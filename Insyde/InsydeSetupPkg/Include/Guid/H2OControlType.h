/** @file
  Defination GUID of H2O Control Type

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _H2O_CONTROL_TYPE_H_
#define _H2O_CONTROL_TYPE_H_

//
// Action Button
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_ACTION_BUTTON_GUID \
  { 0x40a3c3d9, 0x6dae, 0x416b, 0xbe, 0x03, 0xc3, 0xd3, 0x09, 0xa0, 0x3b, 0x87 }

//
// Button
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_BUTTON_GUID \
  { 0x305cb457, 0x900b, 0x4c6e, 0x8e, 0x3a, 0xb0, 0x79, 0x17, 0xff, 0x2c, 0x24 }

//
// Checkbox Popup
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_CHECKBOX_POPUP_GUID \
  { 0x2f74a202, 0x2df1, 0x4120, 0xa6, 0xab, 0x56, 0x85, 0x5d, 0x7d, 0x2d, 0xd5 }

//
// Checkbox Image
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_CHECKBOX_IMAGE_GUID \
  { 0xc4a63d26, 0x3394, 0x44cb, 0xa7, 0x19, 0x20, 0xac, 0xa4, 0x8f, 0x31, 0xa3 }

//
// Date Popup
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_DATE_POPUP_GUID \
  { 0x76c35de4, 0xff83, 0x49a5, 0xbb, 0x31, 0x05, 0x44, 0x6a, 0x6e, 0x8c, 0x24 }

//
// Date Roller
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_DATE_ROLLER_GUID \
  { 0xbe4ad8cc, 0x0937, 0x4dca, 0xb0, 0x4a, 0x38, 0x2e, 0x47, 0x1c, 0x5f, 0xe5 }

//
// Date Formatted
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_DATE_FORMATTED_GUID \
  { 0xf64e170f, 0x4d35, 0x4666, 0xa2, 0x4c, 0xce, 0xa5, 0xe9, 0xb1, 0xe5, 0x3f }

//
// Edit Box
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_EDIT_BOX_GUID \
  { 0xf5de46f8, 0x8dcb, 0x47d5, 0xa7, 0x8c, 0x34, 0x23, 0xe2, 0x5e, 0xa8, 0x1b }

//
// Group
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_GROUP_GUID \
  { 0x070b8791, 0x6c97, 0x4350, 0xaa, 0x82, 0xfd, 0xd1, 0xf5, 0x27, 0x2e, 0x16 }

//
// Hotkey
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_HOTKEY_GUID \
  { 0xbc971b32, 0xb613, 0x492a, 0x9b, 0xa0, 0x7f, 0x5a, 0xb7, 0x25, 0x86, 0x02 }

//
// Numeric Input
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_NUMERIC_INPUT_GUID \
  { 0x4dc06241, 0xfec6, 0x4d23, 0x9a, 0xe5, 0x7d, 0xfd, 0xcc, 0x69, 0x65, 0x4c }

//
// Numeric Popup
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_NUMERIC_POPUP_GUID \
  { 0x548b85c9, 0x4b05, 0x47bc, 0x9b, 0x48, 0xd4, 0x5c, 0x66, 0xe3, 0xef, 0xd6 }

//
// One Of Popup
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_ONE_OF_POPUP_GUID \
  { 0xb09a5059, 0x82c4, 0x4342, 0x8f, 0xcb, 0xd1, 0xb3, 0xb1, 0x3c, 0x3a, 0xa4 }

//
// One Of Option List
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_ONE_OF_OPTION_LIST_GUID \
  { 0x1800dfc3, 0x05b0, 0x4529, 0x8b, 0xfc, 0xba, 0x8c, 0xeb, 0xa8, 0xfd, 0x1f }

//
// Ordered List Popup
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_ORDERED_LIST_POPUP_GUID \
  { 0x03f7c4ab, 0xaea4, 0x4c71, 0x8f, 0xe6, 0x1e, 0xef, 0xa4, 0x7a, 0x09, 0x34 }

//
// Ordered List Option List
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_ORDERED_LIST_OPTION_LIST_GUID \
  { 0x1a1d3c9d, 0x2cd6, 0x4ee9, 0x9e, 0x93, 0xbb, 0x2d, 0x4c, 0x12, 0xca, 0x32 }

//
// Password Input
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_PASSWORD_INPUT_GUID \
  { 0xc3b9ce80, 0x84c7, 0x4cf2, 0x9f, 0x5a, 0x38, 0x84, 0xfb, 0x65, 0x35, 0xc0 }

//
// Password Popup
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_PASSWORD_POPUP_GUID \
  { 0xb60f89cf, 0xe0f6, 0x4283, 0xa8, 0x12, 0x29, 0xff, 0x19, 0xfe, 0x12, 0x3c }

//
// String Input
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_STRING_INPUT_GUID \
  { 0x88ca8bb3, 0xe26d, 0x409f, 0x8b, 0x45, 0x5a, 0x38, 0x47, 0xe6, 0x39, 0x36 }

//
// String Popup
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_STRING_POPUP_GUID \
  { 0x12bbedd4, 0x8cc3, 0x4f7a, 0x96, 0x24, 0xdd, 0x89, 0x26, 0x73, 0x9d, 0x0f }

//
// Subtitle
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_SUBTITLE_GUID \
  { 0x6b2abb00, 0x74fd, 0x4b4e, 0xbb, 0x06, 0xa4, 0x7f, 0x6f, 0x38, 0x83, 0x91 }

//
// Text
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_TEXT_GUID \
  { 0x64e6a5a1, 0x0575, 0x4881, 0x98, 0xbc, 0x7c, 0x11, 0xbb, 0xfa, 0x88, 0x5a }

//
// Time Popup
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_TIME_POPUP_GUID \
  { 0x18a79549, 0x78aa, 0x4f5b, 0xaa, 0xda, 0xb6, 0xc4, 0x07, 0x77, 0xc6, 0x8c }

//
// Time Roller
//
#define H2O_CONTROL_TYPE_SETUP_LMDE_TIME_ROLLER_GUID \
  { 0x0abe5558, 0xa9c9, 0x4cba, 0x9a, 0x3c, 0x4f, 0xbe, 0x0f, 0x7a, 0x10, 0x90 }

extern EFI_GUID gH2OSetupLmdeActionButtonGuid;
extern EFI_GUID gH2OSetupLmdeButtonGuid;
extern EFI_GUID gH2OSetupLmdeCheckboxPopupGuid;
extern EFI_GUID gH2OSetupLmdeCheckboxImageGuid;
extern EFI_GUID gH2OSetupLmdeDatePopupGuid;
extern EFI_GUID gH2OSetupLmdeDateRollerGuid;
extern EFI_GUID gH2OSetupLmdeDateFormattedGuid;
extern EFI_GUID gH2OSetupLmdeEditBoxGuid;
extern EFI_GUID gH2OSetupLmdeGroupGuid;
extern EFI_GUID gH2OSetupLmdeHotkeyGuid;
extern EFI_GUID gH2OSetupLmdeNumericInputGuid;
extern EFI_GUID gH2OSetupLmdeNumericPopupGuid;
extern EFI_GUID gH2OSetupLmdeOneOfPopupGuid;
extern EFI_GUID gH2OSetupLmdeOptionListGuid;
extern EFI_GUID gH2OSetupLmdeOrderedListPopupGuid;
extern EFI_GUID gH2OSetupLmdeOrderedListOptionListGuid;
extern EFI_GUID gH2OSetupLmdePasswordInputGuid;
extern EFI_GUID gH2OSetupLmdePasswordPopupGuid;
extern EFI_GUID gH2OSetupLmdeStringInputGuid;
extern EFI_GUID gH2OSetupLmdeStringPopupGuid;
extern EFI_GUID gH2OSetupLmdeSubtitleGuid;
extern EFI_GUID gH2OSetupLmdeTextGuid;
extern EFI_GUID gH2OSetupLmdeTimePopupGuid;
extern EFI_GUID gH2OSetupLmdeTimeRollerGuid;

#endif
