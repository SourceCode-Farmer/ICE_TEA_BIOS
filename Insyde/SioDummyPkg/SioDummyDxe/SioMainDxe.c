/** @file

SIO main code, it include create SCU, install device protocol, init in DXE stage

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "SioInitDxe.h"
SIO_DEVICE_LIST_TABLE *mTablePtr;
extern EFI_SIO_TABLE mSioTable[];

//[-start-140116-IB12970054-modify]//
extern UINT16 mSioCfgPortListSize;
//[-end-140116-IB12970054-modify]//

extern EFI_INSTALL_DEVICE_FUNCTION mDeviceFunction[];
extern EFI_SIO_RESOURCE_FUNCTION mSioResourceFunction[];

/**
  The entry point of the SIO driver.  

  @param [in] ImageHandle            A handle for the image that is initializing this driver
  @param [in] SystemTable            A pointer to the EFI system table

  @retval EFI_SUCCESS           Function complete successfully. 
**/
EFI_STATUS
EFIAPI
SioDriverEntryPoint (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
  )
{
  EFI_STATUS                        Status;

  mTablePtr = (SIO_DEVICE_LIST_TABLE *)PcdGetPtr (PcdSioDummyCfg);

  //
  // Get SIO from PCD, if it fail, we don't install SIO DXE driver
  //
  Status = CheckDevice ();  
  if (Status != EFI_SUCCESS) {
    return Status;
  }

  InstallEnabledDeviceProtocol ();

  return Status;
}

/**
  To get correct SIO data. 

  @retval EFI_SUCCESS           Found SIO data. 
  @retval EFI_NOT_FOUND         Not found. 
**/
EFI_STATUS
CheckDevice (
  VOID
  ) 
{
  SIO_DEVICE_LIST_TABLE             *PcdPointer;

  PcdPointer = mTablePtr;

  //
  // Calculate the number of non-zero entries in the table
  //
  while (!((PcdPointer->TypeH == NONE_ID) && (PcdPointer->TypeL == NONE_ID))) {
    if (PcdPointer->TypeH == SIO_ID1) {
      return EFI_SUCCESS;          
    } 
    PcdPointer++;
  }
  
  return EFI_NOT_FOUND;
}

/**
  Transfer from SIO_DEVICE_LIST_TABLE to EFI_ISA_DEVICE_RESOURCE for using of device protocol

  @param[in]  *DeviceList            SIO_DEVICE_LIST_TABLE structure.
  @param[in]  *DeviceResource        EFI_ISA_DEVICE_RESOURCE structure.

  @retval EFI_SUCCESS                The function completed successfully.
**/
EFI_STATUS
DeviceListToDeviceResource (
  IN SIO_DEVICE_LIST_TABLE        *DeviceList,
  IN EFI_ISA_DEVICE_RESOURCE      *DeviceResource
  )
{
  DeviceResource->IoPort = DeviceList->DeviceBaseAdr;
  DeviceResource->IrqNumber = DeviceList->DeviceIrq;
  DeviceResource->DmaChannel = DeviceList->DeviceDma;

  return EFI_SUCCESS;
}

/**
  Install Device protocol from PCD structure.
**/
VOID
InstallEnabledDeviceProtocol (
  VOID
  )
{
  EFI_STATUS                         Status;
  UINT8                              Index;
  SIO_DEVICE_LIST_TABLE              *PcdPointer;

  Status       = EFI_NOT_FOUND;
  
  Index = 0;
  while (mDeviceFunction[Index].Device != NULL_ID) {
    PcdPointer   = mTablePtr;
    while (!((PcdPointer->TypeH == NONE_ID) && (PcdPointer->TypeL == NONE_ID))) {
      if ((PcdPointer->TypeH == SIO_ID1) && (PcdPointer->Device == mDeviceFunction[Index].Device) 
         && (PcdPointer->DeviceInstance == mDeviceFunction[Index].Instance)) {   
        //
        // if the device is enable, then install it
        //
        if (PcdPointer->DeviceEnable != FALSE) {
          if (mDeviceFunction[Index].InstallDeviceProtocol != NULL) {
            Status = mDeviceFunction[Index].InstallDeviceProtocol (
                       PcdPointer
                       ); 
          }
        }
        break;
      } 
      PcdPointer++;
    }
    Index++;
  }  
}
