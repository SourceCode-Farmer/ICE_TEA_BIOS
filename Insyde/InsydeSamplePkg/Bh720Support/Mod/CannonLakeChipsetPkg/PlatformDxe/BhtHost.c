/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "BhtHost.h"

int           g_deviceId        = 0;
BOOLEAN       mRegistered       = FALSE;
EFI_HANDLE    mControllerHandle = NULL;

/**
  BH EMMC init done handler.

  @param[in]  Event        Event whose notification function is being invoked
  @param[in]  Context      Pointer to the notification function's context

**/
VOID
EFIAPI
BhtHostInitializeDoneHandler (
  IN EFI_EVENT  Event,
  IN VOID       *Context
  )
{
  EFI_STATUS    Status;
  VOID          *Interface;

  Status = gBS->HandleProtocol (
                mControllerHandle,
                &gEfiSdMmcPassThruProtocolGuid,
                (VOID **) &Interface
                );
  if (EFI_ERROR (Status)) {
    return;
  }

  gBS->CloseEvent (Event);
  //
  // Stall for stable.
  //
  gBS->Stall (6000000);
}

/**

  Override function for SDHCI capability bits

  @param[in]      ControllerHandle      The EFI_HANDLE of the controller.
  @param[in]      Slot                  The 0 based slot index.
  @param[in,out]  SdMmcHcSlotCapability The SDHCI capability structure.

  @retval EFI_SUCCESS           The override function completed successfully.
  @retval EFI_NOT_FOUND         The specified controller or slot does not exist.
  @retval EFI_INVALID_PARAMETER SdMmcHcSlotCapability is NULL

**/
EFI_STATUS
EFIAPI
BhtHostOverrideCapability (
  IN      EFI_HANDLE                      ControllerHandle,
  IN      UINT8                           Slot,
  IN  OUT VOID                            *SdMmcHcSlotCapability
  )
{
  EFI_STATUS                              Status;
  EFI_PCI_IO_PROTOCOL                     *PciIo;
//  SD_MMC_HC_SLOT_CAP                      *Cap;
  UINT16                                  EmmcDtrVar;
  UINTN                                   EmmcDtrVarSize;

  Status = gBS->HandleProtocol (
                ControllerHandle,
                &gEfiPciIoProtocolGuid,
                (VOID **) &PciIo
                );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (BhtHostPciSupport (PciIo)) {
//    Cap = (SD_MMC_HC_SLOT_CAP *)SdMmcHcSlotCapability;
//    //
//    // WA: Disable 8-bit bus width support
//    //
//    Cap->BusWidth8 = 0;
//    Cap->Hs400     = 0;
//    Cap->Sdr104    = 0;
//    Cap->Ddr50     = 0;
    //
    // Set DTR to 4-bit buswidth + HS200
    //
    EmmcDtrVar = 0x0417;
    EmmcDtrVarSize = sizeof (EmmcDtrVar);
    Status = gRT->SetVariable (
                    L"EMMC_DTR",
                    &gEfiGenericVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                    EmmcDtrVarSize,
                    &EmmcDtrVar
                    );
    DEBUG((EFI_D_ERROR, "BhtHostOverrideCapability: Set EMMC_DTR Variable (%r) - DeviceType=0x%x, BusWidth=0x%x\n", Status, 0x17, 0x04));
  }

  return EFI_SUCCESS;
}

/**

  Override function for SDHCI controller operations

  @param[in]      ControllerHandle      The EFI_HANDLE of the controller.
  @param[in]      Slot                  The 0 based slot index.
  @param[in]      PhaseType             The type of operation and whether the
                                        hook is invoked right before (pre) or
                                        right after (post)

  @retval EFI_SUCCESS           The override function completed successfully.
  @retval EFI_NOT_FOUND         The specified controller or slot does not exist.
  @retval EFI_INVALID_PARAMETER PhaseType is invalid

**/
EFI_STATUS
EFIAPI
BhtHostOverrideNotifyPhase (
  IN      EFI_HANDLE                      ControllerHandle,
  IN      UINT8                           Slot,
  IN      EDKII_SD_MMC_PHASE_TYPE         PhaseType
  )
{
  EFI_STATUS                              Status;
  EFI_PCI_IO_PROTOCOL                     *PciIo;
  UINT8                                   Value;
  VOID                                    *Registration;

  Status = gBS->HandleProtocol (
                ControllerHandle,
                &gEfiPciIoProtocolGuid,
                (VOID **) &PciIo
                );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  switch (PhaseType) {
  case EdkiiSdMmcInitHostPre:
    if (BhtHostPciSupport (PciIo)) {
      /* FET on */
      PciBhtOr32 (PciIo, 0xEC, 0x3);
      /* Led on */
      PciBhtAnd32 (PciIo, 0x334, (UINT32)~BIT13);
      PciBhtOr32 (PciIo, 0xD4, BIT6);
      /* Set 1.8v emmc signaling flag */
      PciBhtOr32 (PciIo, 0x308, BIT4);
      /* Set 200MBaseClock */
      PciBhtAnd32 (PciIo, 0x304,0x0000FFFF);
      PciBhtOr32 (PciIo, 0x304,0x25100000);	
      PciBhtOr32 (PciIo, 0x3E4, BIT22);
      //enable internal clk
      PciIo->Mem.Read (PciIo,EfiPciIoWidthUint8, 0, 0x2C, 1, &Value);
      Value |= BIT0;
      PciIo->Mem.Write (PciIo,EfiPciIoWidthUint8, 0, 0x2C, 1, &Value);
      //wait BaseClk stable 0x1CC bit14
      PciIo->Mem.Read (PciIo,EfiPciIoWidthUint8, 0, 0x1CD, 1, &Value);
      while(!(Value & BIT6)) {		
        PciIo->Mem.Read (PciIo,EfiPciIoWidthUint8, 0, 0x1CD, 1, &Value);
        DbgMsg (L"0x1CD=0x%x\n", Value);
      }
      //enable hardware tuning
      Value = (UINT8)(~0x10);
      PciIo->Mem.Write (PciIo,EfiPciIoWidthUint8, 0, 0x110, 1, &Value);
    }
    break;
  case EdkiiSdMmcInitHostPost:
    if (BhtHostPciSupport (PciIo)) {
      //
      // Enable 1.8V signaling
      //
      PciIo->Mem.Read (PciIo,EfiPciIoWidthUint8, 0, 0x3E, 1, &Value);
      Value |= BIT3;
      PciIo->Mem.Write (PciIo,EfiPciIoWidthUint8, 0, 0x3E, 1, &Value);
      gBS->Stall (5000);
      if (!mRegistered) {
        EfiCreateProtocolNotifyEvent (
          &gEfiSdMmcPassThruProtocolGuid,
          TPL_CALLBACK,
          BhtHostInitializeDoneHandler,
          NULL,
          (VOID **)&Registration
          );
        mRegistered= TRUE;
        mControllerHandle = ControllerHandle;
      }
    }

  default:
    break;
  }
  return EFI_SUCCESS;
}

BOOLEAN BhtHostPciSupport(EFI_PCI_IO_PROTOCOL *PciIo)
{
	PCI_TYPE00		Pci;

	PciIo->Pci.Read (PciIo, EfiPciIoWidthUint32,		
				  0, sizeof Pci / sizeof (UINT32), &Pci);

	DEBUG ((DEBUG_INFO, "check device %04x:%04x\n", Pci.Hdr.VendorId, Pci.Hdr.DeviceId));

	if (Pci.Hdr.VendorId != 0x1217)
		goto end;

	switch (Pci.Hdr.DeviceId)
	{
		case 0x8420:	//PCI_DEV_ID_SDS0
		case 0x8421:	//PCI_DEV_ID_SDS1
		case 0x8520:	//PCI_DEV_ID_FJ2
		case 0x8620:	//PCI_DEV_ID_SB0
		case 0x8621:	//PCI_DEV_ID_SB1
			g_deviceId = Pci.Hdr.DeviceId;
			return 1;
		default:
			break;
	}

	end:
	return 0;
}

void DbgNull(IN CONST CHAR16 * fmt, ...)
{
}

UINT32 bht_readl(EFI_PCI_IO_PROTOCOL *PciIo, UINT32 offset)
{
	UINT32 arg;
	PciIo->Mem.Read(PciIo,EfiPciIoWidthUint32,1,offset,1,&arg);
	return arg;
}

void bht_writel(EFI_PCI_IO_PROTOCOL *PciIo, UINT32 offset, UINT32 value)
{
	PciIo->Mem.Write(PciIo,EfiPciIoWidthUint32,1,offset,1,&value);
}


UINT32 PciBhtRead32(EFI_PCI_IO_PROTOCOL *PciIo, UINT32 offset)
{
	UINT32 i = 0;
	UINT32 tmp[3] = {0};

	if((g_deviceId == PCI_DEV_ID_SDS0) ||
			(g_deviceId == PCI_DEV_ID_SDS1) ||
			(g_deviceId == PCI_DEV_ID_FJ2) ||
			(g_deviceId == PCI_DEV_ID_SB0) ||
			(g_deviceId == PCI_DEV_ID_SB1))
	{
		// For Sandstorm, HW implement a mapping method by memory space reg to access PCI reg.
		// Enable mapping
	
		// Check function conflict
		if((g_deviceId == PCI_DEV_ID_SDS0) ||
				(g_deviceId == PCI_DEV_ID_FJ2) ||
				(g_deviceId == PCI_DEV_ID_SB0) ||
				(g_deviceId == PCI_DEV_ID_SB1))
		{
			i = 0;
			bht_writel(PciIo, BHT_PCIRMappingEn, 0x40000000);
			while((bht_readl(PciIo, BHT_PCIRMappingEn) & 0x40000000) == 0)
			{
				if(i == 5)
				{
					//DbgMsg((DRIVERNAME " - %s() function 0 can't lock!\n", __FUNCTION__));
					goto RD_DIS_MAPPING;
				}
						gBS->Stall(1000);
				i++;
					bht_writel(PciIo, BHT_PCIRMappingEn, 0x40000000);
	
			}
		}
		else if(g_deviceId == PCI_DEV_ID_SDS1)
		{
			i = 0;
			bht_writel(PciIo, BHT_PCIRMappingEn, 0x20000000);
			while((bht_readl(PciIo, BHT_PCIRMappingEn) & 0x20000000) == 0)
			{
				if(i == 5)
				{
					//DbgErr((DRIVERNAME " - %s() function 1 can't lock!\n", __FUNCTION__));
					goto RD_DIS_MAPPING;
				}
				gBS->Stall(1000);
				i++;
				bht_writel(PciIo, BHT_PCIRMappingEn, 0x20000000);
			}
		}
	
		// Check last operation is complete
		i = 0;
		while(bht_readl(PciIo, BHT_PCIRMappingCtl) & 0xc0000000)
		{
			if(i == 5)
			{
				//DbgErr((DRIVERNAME " - [204] = 0x%x\n", RegisterRead32(ELN_dPCIRMappingCtl)));
				//DbgErr((DRIVERNAME " - [208] = 0x%x\n", RegisterRead32(ELN_dPCIRMappingEn)));
				//DbgErr((DRIVERNAME " - %s() check last operation complete timeout!!!\n", __FUNCTION__));
				goto RD_DIS_MAPPING;
			}
			gBS->Stall(1000);
			i += 1;
		}
	
		// Set register address
		tmp[0] |= 0x40000000;
		tmp[0] |= offset;
		bht_writel(PciIo, BHT_PCIRMappingCtl, tmp[0]);
	
		// Check read is complete
		i = 0;
		while(bht_readl(PciIo, BHT_PCIRMappingCtl) & 0x40000000)
		{
			if(i == 5)
			{
				//DbgErr((DRIVERNAME " - %s() check read operation complete timeout!!!\n", __FUNCTION__));
				goto RD_DIS_MAPPING;
			}
			gBS->Stall(1000);
			i += 1;
		}
	
		// Get PCIR value
		tmp[1] = bht_readl(PciIo, BHT_PCIRMappingVal);
	
RD_DIS_MAPPING:
		// Disable mapping
		bht_writel(PciIo, BHT_PCIRMappingEn, 0x80000000);
	
		//DbgDebug(L"%s offset=%x Value:%x\n", __FUNCTION__, offset, tmp[1]);
		return tmp[1];
	}
	
	//DbgDebug(L"%s offset=%x Value:%x\n", __FUNCTION__, offset, tmp[0]);
	return tmp[0];	
}

void PciBhtWrite32(EFI_PCI_IO_PROTOCOL *PciIo, UINT32 offset, UINT32 value)
{
	UINT32 tmp = 0;
    UINT32 i = 0;

	if((g_deviceId == PCI_DEV_ID_SDS0) ||
			(g_deviceId == PCI_DEV_ID_SDS1) ||
			(g_deviceId == PCI_DEV_ID_FJ2) ||
			(g_deviceId == PCI_DEV_ID_SB0) ||
			(g_deviceId == PCI_DEV_ID_SB1))
    {
        // For Sandstorm, HW implement a mapping method by memory space reg to access PCI reg.
        // Upper caller doesn't need to set 0xD0.

        // Enable mapping

        // Check function conflict
		if((g_deviceId == PCI_DEV_ID_SDS0) ||
				(g_deviceId == PCI_DEV_ID_FJ2) ||
				(g_deviceId == PCI_DEV_ID_SB0) ||
				(g_deviceId == PCI_DEV_ID_SB1))
        {
            i = 0;
            bht_writel(PciIo, BHT_PCIRMappingEn, 0x40000000);
            while((bht_readl(PciIo, BHT_PCIRMappingEn) & 0x40000000) == 0)
            {
                if(i == 5)
                {
                    //DbgErr((DRIVERNAME " - %s() function 0 can't lock!\n", __FUNCTION__));
                    goto WR_DIS_MAPPING;
                }

                gBS->Stall(1000);
                i++;
                bht_writel(PciIo, BHT_PCIRMappingEn, 0x40000000);
            }
        }
        else if(g_deviceId == PCI_DEV_ID_SDS1)
        {
            i = 0;
            bht_writel(PciIo, BHT_PCIRMappingEn, 0x20000000);

            while((bht_readl(PciIo, BHT_PCIRMappingEn) & 0x20000000) == 0)
            {
                if(i == 5)
                {
                    //DbgErr((DRIVERNAME " - %s() function 0 can't lock!\n", __FUNCTION__));
                    goto WR_DIS_MAPPING;
                }

                gBS->Stall(1000);
                i++;
                bht_writel(PciIo, BHT_PCIRMappingEn, 0x20000000);
            }
        }

        // Enable MEM access
        bht_writel(PciIo, BHT_PCIRMappingVal, 0x80000000);
        bht_writel(PciIo, BHT_PCIRMappingCtl, 0x800000D0);

        // Check last operation is complete
        i = 0;
        while(bht_readl(PciIo, BHT_PCIRMappingCtl) & 0xc0000000)
        {
            if(i == 5)
            {
                //DbgErr((DRIVERNAME " - %s() check last operation complete timeout!!!\n", __FUNCTION__));
                goto WR_DIS_MAPPING;
            }
            gBS->Stall(1000);
            i += 1;
        }

        // Set write value
        bht_writel(PciIo, BHT_PCIRMappingVal, value);
        // Set register address
        tmp |= 0x80000000;
        tmp |= offset;
        bht_writel(PciIo, BHT_PCIRMappingCtl, tmp);

        // Check write is complete
        i = 0;
        while(bht_readl(PciIo, BHT_PCIRMappingCtl) & 0x80000000)
        {
            if(i == 5)
            {
                //DbgErr((DRIVERNAME " - %s() check write operation complete timeout!!!\n", __FUNCTION__));
                goto WR_DIS_MAPPING;
            }
            gBS->Stall(1000);
            i += 1;
        }

WR_DIS_MAPPING:
        // Disable MEM access
        bht_writel(PciIo, BHT_PCIRMappingVal, 0x80000001);
        bht_writel(PciIo, BHT_PCIRMappingCtl, 0x800000D0);

        // Check last operation is complete
        i = 0;
        while(bht_readl(PciIo, BHT_PCIRMappingCtl) & 0xc0000000)
        {
            if(i == 5)
            {
                //DbgErr((DRIVERNAME " - %s() check last operation complete timeout!!!\n", __FUNCTION__));
                break;
            }
            gBS->Stall(1000);
            i += 1;
        }

        // Disable function conflict

        // Disable mapping
        bht_writel(PciIo, BHT_PCIRMappingEn, 0x80000000);
    }
}

void PciBhtOr32(EFI_PCI_IO_PROTOCOL *PciIo, UINT32 offset, UINT32 value)
{
	UINT32 arg;
	arg = PciBhtRead32(PciIo, offset);
	PciBhtWrite32(PciIo, offset, value | arg);
}

void PciBhtAnd32(EFI_PCI_IO_PROTOCOL *PciIo, UINT32 offset, UINT32 value)
{
	UINT32 arg;
	arg = PciBhtRead32(PciIo, offset);
	PciBhtWrite32(PciIo, offset, value & arg);
}


