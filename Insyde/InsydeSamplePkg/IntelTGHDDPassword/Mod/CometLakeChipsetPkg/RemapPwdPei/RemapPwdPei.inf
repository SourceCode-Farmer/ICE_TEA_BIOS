## @file
#  This is a Opal Password PEI driver.
#
# Copyright (c) 2016 - 2018, Intel Corporation. All rights reserved.<BR>
# This program and the accompanying materials
# are licensed and made available under the terms and conditions of the BSD License
# which accompanies this distribution. The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#
##

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = RemapPwdPei
  FILE_GUID                      = 4EFA14DD-2A34-491c-B81E-7D05B210FCFA
  MODULE_TYPE                    = PEIM
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = RemapPwdPeiInit

#
# The following information is for reference only and not required by the build tools.
#
#  VALID_ARCHITECTURES           = IA32 X64
#

[Sources]
  RemapPwdPei.c
  OpalPasswordPei.h
  OpalPasswordCommon.h
  OpalAhciMode.c
  OpalAhciMode.h
  OpalNvmeMode.c
  OpalNvmeMode.h
  OpalNvmeReg.h

[Packages]
  InsydeModulePkg/InsydeModulePkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  SecurityPkg/SecurityPkg.dec
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  $(PROJECT_PKG)/Project.dec


[LibraryClasses]
  PeimEntryPoint
  PeiServicesLib
  DebugLib
  IoLib
  PciLib
  BaseLib
  BaseMemoryLib
  MemoryAllocationLib
  TimerLib
  HobLib
  LockBoxLib
  TcgStorageOpalLib
  Tcg2PhysicalPresenceLib
  VariableLib
  TimerLib
  PchInfoLib
  SataLib
  
[Guids]
  gSaveHddPasswordGuid

  gSaveIntelRemapDevInfoGuid
  
[Ppis]
  gEdkiiIoMmuPpiGuid                            ## SOMETIMES_CONSUMES
  gEfiEndOfPeiSignalPpiGuid                     ## NOTIFY
  gPeiOpalDeviceInfoPpiGuid                     ## SOMETIMES_CONSUMES # RPPO-CNL-0102: RoyalParkOverrideContent

[Pcd]
  gInsydeTokenSpaceGuid.PcdAhciPcieMemBaseAddress
  
[Depex]
  gEfiPeiMasterBootModePpiGuid
