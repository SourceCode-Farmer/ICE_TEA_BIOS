/** @file
 This function offers an interface to Hook IHISI Sub function AH=1Fh,function "FbtsApHookForBios"

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/SmmOemSvcKernelLib.h>
//[-start-211014-BAIN000051-add]//
#if defined(LCFC_SUPPORT)
#include <Library/LfcEcLib.h>
#endif
//[-end-211014-BAIN000051-add]//

/**
 This function provides a hook for IHISI sub-function AH=1Fh and should be implemented to do AP Hook for BIOS.
 Please refer to Insyde H2O internal Soft-SMI interface (IHISI) Specification.

 @param[in]         ApState             denote the start of AP process.
                                        ApState = 0x00, AP terminate. (Before IHISI 0x16)
                                        ApState = 0x01, AP start. (After IHISI 0x10)
                                        ApState = 0x02, Start to read ROM. (Before IHISI 0x14)
                                        ApState = 0x03, Start to write ROM. (Before IHISI 0x15)
                                        ApState = 0x04, Start to write EC. (Before IHISI 0x20)
                                        ApState = 0x05, Before dialog popup.
                                        ApState = 0x06, After dialog close and continue running.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcIhisiS1FHookFbtsApHookForBios (
  IN UINT8          ApState
  )
{
/*++
  Todo:
  Add project specific code in here.
--*/
//[-start-211014-BAIN000051-modify]//
//#if defined(LCFC_SUPPORT)
//#if defined(C970_BSH_SUPPORT)
//[-start-21119-TAMT000032-modify]//
//[-start-211203-QINGLIN0125-modify]//
//[-start-211206-OWENWU0029-modify]//
//[-start-211214-Ching000017-modify]//
#if defined(C970_BSH_SUPPORT) || defined(C770_BSH_SUPPORT) || defined(S77014_BSH_SUPPORT) || defined(S370_BSH_SUPPORT) || defined(S570_BSH_SUPPORT) || defined(S77013_BSH_SUPPORT) || defined(S77014IAH_BSH_SUPPORT)
  if (ApState == BeforeWriteRom) {
    //
    // [Lenovo BIOS Self-Healing Version 1.6]
    // 2.2 Detect
    //     When BIOS flash happened, before erase/write BIOS should notify EC to set flag (Wdt Flag should be keeped
    //     even EC power lost, such as stored in the EC EEPROM) to enable WDT.
    //
    LfcEcLibSetWDTFlagForSelfHealing();
  }

  return EFI_UNSUPPORTED;
#else
  return EFI_UNSUPPORTED;
#endif
//[-end-211214-Ching000017-modify]//
//[-end-211206-OWENWU0029-modify]//
//[-end-211203-QINGLIN0125-modify]//
//[-end-21119-TAMT000032-modify]//
//[-end-211014-BAIN000051-modify]//
}
