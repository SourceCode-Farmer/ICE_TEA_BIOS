## @file
#  Component Description for CapsuleLoaderTriggerDxe Driver
#
#******************************************************************************
#* Copyright (c) 2012 - 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = CapsuleLoaderTriggerDxe
  FILE_GUID                      = C62CEB80-FB40-4A46-A5E5-C1D997C36DFC
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = CapsuleLoaderTriggerEntryPoint

[Sources]
  CapsuleLoaderTriggerDxe.c
  CapsuleLoaderTriggerDxe.h

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
#[-start-210616-YUNLEI0102-add]
!if $(LCFC_SUPPORT_ENABLE) == YES
  LfcPkg/LfcPkg.dec  
  $(PROJECT_PKG)/Project.dec
!endif
#[-end-210616-YUNLEI0102-add]
#[-start-211220-BAIN000070-add]#
!if $(LCFC_SUPPORT_ENABLE) == YES
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
!endif
#[-end-211220-BAIN000070-add]#

[LibraryClasses]
  BaseLib
  UefiLib
  UefiDriverEntryPoint
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  MemoryAllocationLib
  PrintLib
  CapsuleLib
  SeamlessRecoveryLib
  VariableLib
  StdLib
  H2OCpLib
#[-start-210616-YUNLEI0102-add]
!if $(LCFC_SUPPORT_ENABLE) == YES
  LfcEcLib
!endif
#[-end-210616-YUNLEI0102-add]

[Guids]
  gEfiFileInfoGuid
  gEfiGenericVariableGuid
  gH2OBdsCpReadyToBootBeforeGuid
  gH2OBdsCpEndOfDxeBeforeGuid
  gWindowsUxCapsuleGuid
#[-start-210616-YUNLEI0102-add]
!if $(LCFC_SUPPORT_ENABLE) == YES
  gLfcVariableGuid
!endif
#[-end-210616-YUNLEI0102-add]

[Protocols]
  gEfiSimpleFileSystemProtocolGuid
#[-start-210616-YUNLEI0102-add]
!if $(LCFC_SUPPORT_ENABLE) == YES
  gEfiEcFlashProtocolGuid

[Pcd]
  gInsydeTokenSpaceGuid.PcdH2OEsrtSystemFirmwareGuid
#[-start-211220-BAIN000070-add]#
  gChipsetPkgTokenSpaceGuid.PcdWindowsTbtRetimer1FirmwareCapsuleGuid
#[-end-211220-BAIN000070-add]#
!endif
#[-end-210616-YUNLEI0102-add]

[FeaturePcd]
  gInsydeTokenSpaceGuid.PcdH2OBdsCpReadyToBootBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpEndOfDxeBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceEnabled
#[-start-210616-YUNLEI0102-add]
!if $(LCFC_SUPPORT_ENABLE) == YES
  gProjectPkgTokenSpaceGuid.PcdLcfcFlashEcSupportEnable
!endif
#[-end-210616-YUNLEI0102-add]

[Depex]
  gEfiVariableArchProtocolGuid   AND
  gEfiVariableWriteArchProtocolGuid

[FixedPcd]
  gInsydeTokenSpaceGuid.PcdFirmwareResourceMaximum