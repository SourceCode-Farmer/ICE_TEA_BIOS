/** @file
  Dummy implementation for SMM related functions

;******************************************************************************
;* Copyright (c) 2012 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include "InternalH2OIhisiLib.h"

VOID
IhisiLibGenericSmi (
  IN OUT  H2O_IHISI_PARAMS      *ParamsBuffer
  )
{

}
