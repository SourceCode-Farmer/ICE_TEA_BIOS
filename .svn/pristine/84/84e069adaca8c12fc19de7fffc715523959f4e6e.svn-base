/** @file
  Provides function to keep Setup Item after SCU load default

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SetupUtility.h>
#include <L05ChipsetNameList.h>

UINT8                                   mBackupSataInterfaceMode;
UINT8                                   mBackupL05SecureBoot;
UINT8                                   mBackupL05OsOptimizedDefault;
UINT8                                   mBackupVmdEnable;
UINT8                                   mBackupL05DeviceGuard;
UINT8                                   mBackupL05Language;

#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
UINT8                                   mBackupL05IsNvmePresence;
UINT8                                   mBackupRstPcieRemapEnabled[PCH_MAX_PCIE_ROOT_PORTS];
UINT8                                   mBackupSataRstOptaneMemory;
#endif

//[-start-210618-YUNLEI0103-add]//
#ifdef LCFC_SUPPORT
#ifdef L05_MODERN_PRELOAD_SUPPORT
  UINT8         mBackupL05TpmLockDone;
#endif
#endif
//[-end-210618-YUNLEI0103-add]//

/**
  Save and Restore Setup Item.

  @param  SaveRestoreFlag               TRUE:  Restore setup item.
                                        FALSE: Save setup item.
  @param  MyIfrNVData                   A pointer to CHIPSET_CONFIGURATION.
  SETUP_DATA                            A pointer to SETUP_DATA.
  @param  MySaIfrNVData                 A pointer to SA_SETUP.
  @param  MyMeIfrNVData                 A pointer to ME_SETUP.
  @param  MyCpuIfrNVData                A pointer to CPU_SETUP.
  @param  MyPchIfrNVData                A pointer to PCH_SETUP.
  @param  MyMeStorageIfrNVData          A pointer to ME_SETUP_STORAGE.
  @param  MySiIfrNVData                 A pointer to SI_SETUP.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
*/
EFI_STATUS
L05SaveRestoreSetupItem (
  BOOLEAN                               SaveRestoreFlag,
  CHIPSET_CONFIGURATION                 *MyIfrNVData,
  SETUP_DATA                            *MySetupDataIfrNVData,
  SA_SETUP                              *MySaIfrNVData,
  ME_SETUP                              *MyMeIfrNVData,
  CPU_SETUP                             *MyCpuIfrNVData,
  PCH_SETUP                             *MyPchIfrNVData,
  ME_SETUP_STORAGE                      *MyMeStorageIfrNVData
  )
{
  SYSTEM_CONFIGURATION                  *SetupNvData;

  SetupNvData = (SYSTEM_CONFIGURATION *) MyIfrNVData;

  //
  // According to "Lenovo BIOS Setup Design Guide V2.3" -
  // The "Storage Controller Mode" cannot be changed back to default, it only allows user to manually change.
  //
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  SaveRestoreFlag ? (MyPchIfrNVData->SataInterfaceMode = mBackupSataInterfaceMode) : (mBackupSataInterfaceMode = MyPchIfrNVData->SataInterfaceMode);
#else
  SaveRestoreFlag ? (MyIfrNVData->SataCnfigure = mBackupSataInterfaceMode) : (mBackupSataInterfaceMode = MyIfrNVData->SataCnfigure);
#endif

  //
  // According to "Lenovo BIOS Setup Design Guide V2.7" -
  // The "Language" cannot be changed back to default, it only allows user to manually change.
  //
  SaveRestoreFlag ? (SetupNvData->Language = mBackupL05Language) : (mBackupL05Language = SetupNvData->Language);

#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
  //
  // [Lenovo BIOS Optane Support Version 1.4 Page 13]
  //   OS Optimized Default
  //   When RST Mode is selected, BIOS keeps "UEFI/Legacy mode" "EFI only"
  //   even when "Optimized Default" is disabled.
  //   RST Mode relative items need to keeped.
  //
  SaveRestoreFlag ? (SetupNvData->L05IsNvmePresence = mBackupL05IsNvmePresence) : (mBackupL05IsNvmePresence = SetupNvData->L05IsNvmePresence);
  SaveRestoreFlag ? CopyMem (MyPchIfrNVData->RstPcieRemapEnabled, mBackupRstPcieRemapEnabled, PCH_MAX_PCIE_ROOT_PORTS) : CopyMem (mBackupRstPcieRemapEnabled, MyPchIfrNVData->RstPcieRemapEnabled, PCH_MAX_PCIE_ROOT_PORTS);
  SaveRestoreFlag ? (MyPchIfrNVData->SataRstOptaneMemory = mBackupSataRstOptaneMemory) : (mBackupSataRstOptaneMemory = MyPchIfrNVData->SataRstOptaneMemory);
#endif
#endif

  //
  // [Lenovo BIOS Setup Design Guide V2.8]
  //   Load Default Behavior
  //     Rule 2 
  //     If some BIOS cannot keep all settings, we need to keep some items' setting,
  //     for example: Storage Controller Mode, Boot Sequeence, Secure Boot, Boot Mode, Password Setting, OS optimized default, Intel VMD controller, Language, Device guard...
  //
  //     Note: Boot Mode should be load default with OS Optimized Defaults.
  //
  SaveRestoreFlag ? (SetupNvData->L05SecureBoot = mBackupL05SecureBoot) : (mBackupL05SecureBoot = SetupNvData->L05SecureBoot);
  SaveRestoreFlag ? (SetupNvData->L05OsOptimizedDefault = mBackupL05OsOptimizedDefault) : (mBackupL05OsOptimizedDefault = SetupNvData->L05OsOptimizedDefault);
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
  SaveRestoreFlag ? (MySaIfrNVData->VmdEnable = mBackupVmdEnable) : (mBackupVmdEnable = MySaIfrNVData->VmdEnable);
#endif
  SaveRestoreFlag ? (SetupNvData->L05DeviceGuard = mBackupL05DeviceGuard) : (mBackupL05DeviceGuard = SetupNvData->L05DeviceGuard);


//[-start-210618-YUNLEI0103-add]//
#ifdef LCFC_SUPPORT
#ifdef L05_MODERN_PRELOAD_SUPPORT
  SaveRestoreFlag ? (SetupNvData->L05TpmLockDone = mBackupL05TpmLockDone) : (mBackupL05TpmLockDone = SetupNvData->L05TpmLockDone);
#endif
#endif
//[-end-210618-YUNLEI0103-add]//

  return EFI_SUCCESS;
}
