## @file
#  Component description file for PeiOemSvcKernelLib instance.
#
#******************************************************************************
#* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PeiOemSvcKernelLib
  FILE_GUID                      = BFCAACD2-EAC7-479b-800D-850E4D185893
  MODULE_TYPE                    = PEIM
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = PeiOemSvcKernelLib|PEI_CORE PEIM

[Sources]
  OemSvcChipsetModifyClockGenInfo.c
  OemSvcDetectRecoveryRequest.c
  OemSvcGetVerbTable.c
#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  OemSvcHookWhenRecoveryFail.c
#_End_L05_BIOS_SELF_HEALING_SUPPORT_
#_Start_L05_NOVO_BUTTON_MENU_
  OemSvcInitPlatformStage1.c
#_End_L05_NOVO_BUTTON_MENU_
#_Start_L05_LEGACY_TO_UEFI_
  OemSvcInitPlatformStage2.c
#_End_L05_LEGACY_TO_UEFI_
  OemSvcIsBootWithNoChange.c
#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  OemSvcSetRecoveryRequest.c
#_End_L05_BIOS_SELF_HEALING_SUPPORT_
  OemSvcGetWaitTimerAfterHdaInit.c
#_Start_L05_FEATURE_
  OemSvcPeiCrisisRecoveryReset.c
  OemSvcGetProtectTable.c
#_End_L05_FEATURE_
  OemSvcGetBoardId.c
#_Start_L05_NOVO_BUTTON_MENU_
  L05Hook/GetNovoButtonStatus.c
#_End_L05_NOVO_BUTTON_MENU_

[Packages]
#_Start_L05_FEATURE_
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#_End_L05_FEATURE_
  MdePkg/MdePkg.dec
  PerformancePkg/PerformancePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeOemServicesPkg/InsydeOemServicesPkg.dec
  MdeModulePkg/MdeModulePkg.dec
  $(PROJECT_PKG)/Project.dec
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec

[LibraryClasses]
  PcdLib
  CmosLib
  PeiServicesLib
  IoLib
  DebugLib
  HobLib
  PeiServicesTablePointerLib
#  PchPlatformLib
   PchCycleDecodingLib
  GpioLib
  PchCycleDecodingLib
  PeiInsydeChipsetLib
  PchPcrLib
  EcMiscLib
  EcLib
  EcMiscLib
  PmcLib
#_Start_L05_FEATURE_
  PeiOemSvcFeatureLibDefault
  FlashProtectRegionLib
  CmosLib
  BaseMemoryLib
#_End_L05_FEATURE_
#_Start_L05_PREFIX_CHIPSET_ReInstallStallPpi_
  TimerLib
#_End_L05_PREFIX_CHIPSET_ReInstallStallPpi_

[Guids]
  gEfiGenericVariableGuid
  gChasmfallsCrisisRecoveryGuid
#_Start_L05_LEGACY_TO_UEFI_
  gL05StopLegacyToEfiProcessGuid
#_End_L05_LEGACY_TO_UEFI_
#_Start_L05_FEATURE_
  gL05NovoKeyInfoHobGuid
#_End_L05_FEATURE_

[Ppis]
  gEfiPeiReadOnlyVariable2PpiGuid
#_Start_L05_PREFIX_CHIPSET_ReInstallStallPpi_
  gEfiPeiStallPpiGuid
#_Start_L05_PREFIX_CHIPSET_ReInstallStallPpi_

[Pcd]
  gChipsetPkgTokenSpaceGuid.PcdFlashFvBackupBase        ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdFlashFvBackupSize        ## CONSUMES

  gPerformancePkgTokenSpaceGuid.PcdPerfPkgAcpiIoPortBaseAddress
  gChipsetPkgTokenSpaceGuid.PcdPchGpioBaseAddress
  gChipsetPkgTokenSpaceGuid.PcdSetupConfigSize
  gInsydeTokenSpaceGuid.PcdH2OBoardId
  gChipsetPkgTokenSpaceGuid.PcdCrbBoard
  gBoardModuleTokenSpaceGuid.PcdBoardId
  gChipsetPkgTokenSpaceGuid.PcdCrbSkuId
  gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport
#_Start_L05_CRISIS_ENABLE_
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVersionString
  gInsydeTokenSpaceGuid.PcdPeiRecoveryFile
#_End_L05_CRISIS_ENABLE_

#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  gInsydeTokenSpaceGuid.PcdPeiRecoveryFile
  gL05ServicesTokenSpaceGuid.PcdL05SelfRecoveryFolder
  gL05ServicesTokenSpaceGuid.PcdL05SelfRecoveryFile
  gL05ServicesTokenSpaceGuid.PcdL05TopSwapEnable
  gL05ServicesTokenSpaceGuid.PcdL05BiosRecoveryHotkeyFlag
  gL05ServicesTokenSpaceGuid.PcdL05BiosSelfHealingEnable
#_End_L05_BIOS_SELF_HEALING_SUPPORT_

[FeaturePcd]
#[-start-190606-IB16990031-add]#
  gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag
#[-end-190606-IB16990031-add]#

[FixedPcd]
  gBoardModuleTokenSpaceGuid.PcdSioBaseAddress
  gInsydeTokenSpaceGuid.PcdFlashAreaBaseAddress
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMsdmDataBase
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMsdmDataSize
#_Start_L05_BIOS_SELF_HEALING_SUPPORT_
  gL05ServicesTokenSpaceGuid.PcdL05ChipsetName
#_End_L05_BIOS_SELF_HEALING_SUPPORT_
