/** @file
@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Uefi.h>
#include <Uefi/UefiBaseType.h>
#include <Pi/PiFirmwareFile.h>
#include <Library/BaseLib.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/TimerLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DxeServicesLib.h>
#include <Library/PciSegmentLib.h>

#include <Protocol/SndwBeepProtocol.h>
#include <Protocol/SndwAccessProtocol.h>

VOID
PrintAllCodecsInfo (
  SNDW_ACCESS_PROTOCOL    *SndwAccess
  )
{
  SNDW_CODEC_INFO         *CodecInfo;
  SNDW_CODEC_INFO         *NextCodecInfo;

  SndwAccess->GetFirstCodec (SndwAccess, &NextCodecInfo);

  while (NextCodecInfo != NULL) {
    DEBUG ((DEBUG_INFO, "------- SNDW CODEC -------\n"));

    DEBUG ((DEBUG_INFO, "SndwLinkIndex:        %d\n", NextCodecInfo->SndwLinkIndex));
    DEBUG ((DEBUG_INFO, "Codec ID:\n"));
    DEBUG ((DEBUG_INFO, "  Version:            0x%02x\n", NextCodecInfo->CodecId.Encoding.Version));
    DEBUG ((DEBUG_INFO, "  ManufacturerID[0]:  0x%02x\n", NextCodecInfo->CodecId.Encoding.ManufacturerID[0]));
    DEBUG ((DEBUG_INFO, "  ManufacturerID[1]:  0x%02x\n", NextCodecInfo->CodecId.Encoding.ManufacturerID[1]));
    DEBUG ((DEBUG_INFO, "  PartId[0]:          0x%02x\n", NextCodecInfo->CodecId.Encoding.PartId[0]));
    DEBUG ((DEBUG_INFO, "  PartId[1]:          0x%02x\n", NextCodecInfo->CodecId.Encoding.PartId[1]));
    DEBUG ((DEBUG_INFO, "  Class:              0x%02x\n", NextCodecInfo->CodecId.Encoding.Class));

    DEBUG ((DEBUG_INFO, "----------------------\n"));

    CodecInfo = NextCodecInfo;
    NextCodecInfo = NULL;
    SndwAccess->GetNextCodec (SndwAccess, CodecInfo, &NextCodecInfo);
    FreePool (CodecInfo);
  }

  return;
}

/**

  @param  ImageHandle       The firmware allocated handle for the EFI image.
  @param  SystemTable       A pointer to the EFI System Table.

  @retval EFI_SUCCESS         The function completed successfully.
  @retval EFI_DEVICE_ERROR    Not able to enable DSP memory space.
  @retval EFI_UNSUPPORTED     Requested Sndw link not exists.
**/
EFI_STATUS
EFIAPI
DxeSndwBeepExampleEntryPoint (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS              Status;
  SNDW_BEEP_PROTOCOL      *SndwBeep;
  SNDW_ACCESS_PROTOCOL    *SndwAccess;

  DEBUG ((DEBUG_INFO, "%a () Start.\n", __FUNCTION__));

  ///
  ///  Locate the Sndw Beep Protocol.
  ///
  Status = gBS->LocateProtocol (
                  &gSndwBeepProtocolGuid,
                  NULL,
                  (VOID **) &SndwBeep
                  );
  ASSERT_EFI_ERROR (Status);

  ///
  ///  Locate the Sndw Access Protocol.
  ///
  Status = gBS->LocateProtocol (
                  &gSndwAccessProtocolGuid,
                  NULL,
                  (VOID **) &SndwAccess
                  );
  ASSERT_EFI_ERROR (Status);

  Status = SndwBeep->Enable (SndwBeep);

  Status = SndwBeep->Beep (SndwBeep, 250, 0, 2, 1000, 500);

  PrintAllCodecsInfo (SndwAccess);

  Status = SndwBeep->Disable (SndwBeep);

  DEBUG ((DEBUG_INFO, "%a () - End. Status = %r.\n", __FUNCTION__, Status));
  return Status;
}
