## @file
#  Component description file for DxeOemSvcKernelLib instance.
#
#******************************************************************************
#* Copyright (c) 2012 - 2021, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = DxeOemSvcKernelLib
  FILE_GUID                      = 9AC5355F-90C2-4f08-B458-82791C55AE27
  MODULE_TYPE                    = UEFI_DRIVER
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = DxeOemSvcKernelLib|DXE_CORE DXE_DRIVER DXE_RUNTIME_DRIVER DXE_SAL_DRIVER DXE_SMM_DRIVER UEFI_APPLICATION UEFI_DRIVER SMM_CORE

[Sources]
  OemSvcCsm16ReferSwitch.c
  OemSvcGetHotplugBridgeInfo.c
  OemSvcInstallLegacyBiosOemSlp.c
  OemSvcInstallOptionRomTable.c
  OemSvcInstallPciSkipTable.c
  OemSvcInstallPostKeyTable.c
  OemSvcLogoResolution.c
  OemSvcUpdateFormLen.c
  OemSvcPrepareInstallMpTable.c
  OemSvcVariableForReclaimFailTable.c
  OemSvcDxeUpdateSmbiosRecord.c
  OemSvcUpdateAcpiFacsHardwareSignature.c
  OemSvcUpdateSsidSvidInfo.c
  OemSvcAdjustSetupMenu.c
  OemSvcChangeVbiosBootDisplay.c
#_Start_L05_SETUP_MENU_
  OemSvcAdjustNavigationMenu.c
#_End_L05_SETUP_MENU_
#_Start_L05_ACPI_
  OemSvcGetSlp20PubkeyAndMarkerRom.c
  OemSvcGetOa30MsdmData.c
  OemSvcUpdateAcpiFacsHardwareSignature.c
#_End_L05_ACPI_
#_Start_L05_CUSTOMIZE_MULTI_LOGO_
  OemSvcChangeDefaultLogoImage.c
#_End_L05_CUSTOMIZE_MULTI_LOGO_
#_Start_L05_TPM_
  OemSvcTpmUserConfirmDialog.c
#_End_L05_TPM_
#_Start_L05_ACPI_HARDWARE_SIGNATURE_
  L05Hook/UpdateHardwareSignature.c
  L05Hook/UpdateHardwareSignature.h
#_End_L05_ACPI_HARDWARE_SIGNATURE_
#_Start_L05_SPECIFIC_VARIABLE_SERVICE_
  L05Hook/GetOA3MsdmSpecificVariableServiceData.c
  L05Hook/GetOA3MsdmSpecificVariableServiceData.h
#_End_L05_SPECIFIC_VARIABLE_SERVICE_

[Packages]
#_Start_L05_FEATURE_
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
#_End_L05_FEATURE_
  AlderLakeBoardPkg/BoardPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeOemServicesPkg/InsydeOemServicesPkg.dec
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  $(PROJECT_PKG)/Project.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-start-210623-YUNLEI0104-add]
!if $(LCFC_SUPPORT_ENABLE) == YES
  LfcPkg/LfcPkg.dec
!endif
#[-end-210623-YUNLEI0104-add]
[LibraryClasses]
  MemoryAllocationLib
  UefiRuntimeServicesTableLib
  HobLib
  BaseMemoryLib
  UefiBootServicesTableLib
  PcdLib
  BaseLib
  DebugLib
  CommonPciLib
  PciExpressLib
  DxeMeLib
  S3BootScriptLib
  HobLib
  #
  # NOTE!!!:Please not use HiiExLib, HiiLib in DxeOemSvcKernelLib,
  # It will cause this library include UefiHiiServicesLib. UefiHiiServicesLib depends on
  # gEfiHiiStringProtocolGuid, gEfiHiiDatabaseProtocolGuid and gEfiHiiConfigRoutingProtocolGuid
  # and will make all of driver include DxeOemSvcKernelLib library need wait for these HII related
  # protocol are installed.
  #
  UefiLib
  VariableLib
  MeTypeLib
  PchInfoLib
  PchPcieRpLib
  CpuPcieRpLib
  H2OCpLib
  DevicePathLib
#_Start_L05_ACPI_SLP_20_
  DxeServicesLib
#_End_L05_ACPI_SLP_20_
#_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT_
  UefiBootServicesTableLib
  FileHandleLib
  PrintLib
#_End_L05_BIOS_POST_LOGO_DIY_SUPPORT_
#[-start-210723-QINGLIN0001-add]#
#[-start-210805-QINGLIN0008-modify]#
#[-start-210802-SHAONN0003-modify]#
#[-start-210909-TAMT000007-add]#
!if ($(S570_SUPPORT_ENABLE) == YES) OR ($(S370_SUPPORT_ENABLE) == YES) OR ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  GpioLib
!endif
#[-end-210909-TAMT000007-add]#
#[-end-210802-SHAONN0003-modify]#
#[-end-210805-QINGLIN0008-modify]//
#[-end-210723-QINGLIN0001-add]#
#[start-210802-STORM1103-modify]
#[-start-210805-QINGLIN0008-modify]#
#[-start-210802-SHAONN0003-modify]#
#[-start-210909-TAMT000007-add]#
#[-start-211025-QINGLIN0103-modify]#
!if ($(C770_SUPPORT_ENABLE) == YES) OR ($(S370_SUPPORT_ENABLE) == YES) OR ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S570_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  LfcProjectPeiLib
!endif
#[-end-211025-QINGLIN0103-modify]#
#[-end-210909-TAMT000007-add]#
#[-end-210802-SHAONN0003-modify]#
#[-end-210805-QINGLIN0008-modify]#
#[end-210802-STORM1103-modify]
#[-start-211110-QINGLIN0111-add]#
!if ($(S370_SUPPORT_ENABLE) == YES)
  IoLib
!endif
#[-end-211110-QINGLIN0111-add]#

[Protocols]
  gEfiSetupUtilityProtocolGuid
  gEfiGraphicsOutputProtocolGuid
  gEfiOEMBadgingSupportProtocolGuid
  gEfiBmpDecoderProtocolGuid
  gEfiJpegDecoderProtocolGuid
  gEfiTgaDecoderProtocolGuid
  gEfiGifDecoderProtocolGuid
  gEfiPcxDecoderProtocolGuid
  gEfiHiiDatabaseProtocolGuid
  gMebxProtocolGuid
  gEfiChangeVbiosBootDisplayProtocolGuid
  gITbtPolicyProtocolGuid
  gDxeDTbtPolicyProtocolGuid
#[-start-180420-IB11270199-add]#
  gAmtWrapperProtocolGuid
#[-end-180420-IB11270199-add]#
  gTrustedDeviceSetupMainProtocolGuid
#_Start_L05_ACPI_SLP_20_
  gEfiL05VariableProtocolGuid
#_End_L05_ACPI_SLP_20_
#_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT_
  gEfiSimpleFileSystemProtocolGuid
#_End_L05_BIOS_POST_LOGO_DIY_SUPPORT_
#[-start-210623-YUNLEI0104-add]
!if $(LCFC_SUPPORT_ENABLE) == YES
  gLenovoVariableProtocolGuid
!endif
#[-end-210623-YUNLEI0104-add]
[Guids]
  gEfiGenericVariableGuid                      ## CONSUMES  ## Variable:L"VBIOS"
  gMeBiosExtensionSetupGuid
#  gSetupVariableHobGuid
  gMeBiosPayloadHobGuid
  gPchSetupVariableGuid
  gSystemConfigurationGuid
  gSetupVariableGuid
  gTbtInfoHobGuid
  gDxeITbtConfigGuid
  gDTbtInfoHobGuid
  gSaSetupVariableGuid
  gH2ODxeCpPciHpcGetResourcePaddingGuid
#_Start_L05_NOVO_BUTTON
  gL05NovoKeyInfoHobGuid
#_End_L05_NOVO_BUTTON
#_Start_L05_ACPI_SLP_20_
  gL05OA2AreaDataGuid
  gL05OA2SlicMakerGuid
  gL05H2OFlashMapRegionSlp20Guid
#_End_L05_ACPI_SLP_20_
#_Start_L05_CUSTOMIZE_MULTI_LOGO_
  gL05CustomizeMultiLogoGuid
  gL05H2OFlashMapRegionCustomizeMultiLogoGuid
#_End_L05_CUSTOMIZE_MULTI_LOGO_
#_Start_L05_SPECIFIC_VARIABLE_SERVICE_
  gL05OA3MsdmDataGuid
#_End_L05_SPECIFIC_VARIABLE_SERVICE_
#_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT_
  gEfiL05EspCustomizePostLogoInfoVariableGuid
  gEfiL05EspCustomizePostLogoVcmVariableGuid
  gEfiPartTypeSystemPartGuid
#_End_L05_BIOS_POST_LOGO_DIY_SUPPORT_

[FeaturePcd]
  gInsydeTokenSpaceGuid.PcdFrontPageSupported
  gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag

[PCD]
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVersionString
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareReleaseDateString
  gInsydeTokenSpaceGuid.PcdFlashAreaSize
  gInsydeTokenSpaceGuid.PcdH2ORotateScreenSupported
  gInsydeTokenSpaceGuid.PcdH2ORotateScreenRotateLogo
  gSiPkgTokenSpaceGuid.PcdITbtEnable
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable
  gBoardModuleTokenSpaceGuid.PcdEcPresent
  gBoardModuleTokenSpaceGuid.PcdEcMajorRevision
  gBoardModuleTokenSpaceGuid.PcdEcMinorRevision
  gInsydeTokenSpaceGuid.PcdH2ODxeCpPciHpcGetResourcePaddingSupported
#_Start_L05_BIOS_POST_LOGO_DIY_SUPPORT_
  gL05ServicesTokenSpaceGuid.PcdL05CustomizeLogoFromEspFlag
#_End_L05_BIOS_POST_LOGO_DIY_SUPPORT_

[FixedPcd]
  gSiPkgTokenSpaceGuid.PcdAmtEnable
#_Start_L05_SETUP_MENU_
  gL05ServicesTokenSpaceGuid.PcdL05ChipsetName
#_End_L05_SETUP_MENU_
#[-start-211025-QINGLIN0103-add]#
  gChipsetPkgTokenSpaceGuid.PcdHgPegBridgeBus
  gChipsetPkgTokenSpaceGuid.PcdHgPegBridgeDevice
  gChipsetPkgTokenSpaceGuid.PcdHgPegBridgeFunction
#[-end-211025-QINGLIN0103-add]#
