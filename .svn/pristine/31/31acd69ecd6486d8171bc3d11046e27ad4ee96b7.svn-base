## @file
#  AlderLake S RVP GPIO definition table for Pre-Memory Initialization
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

###
### !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
### Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
###
###

# mGpioTablePreMemAdlSDdr4UDimm2DCrb
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr4UDimm2DCrb]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  { GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,   GpioTermNone }}, // PEG_1 RTD3 Reset
  { GPIO_VER4_S_GPP_E3,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,   GpioTermNone }}, // PEG_2 RTD3 Reset Sinai DR0 (Rework)
  { GPIO_VER4_S_GPP_F11,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,   GpioTermNone }}, // PCIe SLOT_1 RTD3 Reset MIPI60 (Rework)
  { GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,   GpioTermNone }}, // PCIe SLOT_2 RTD3 Reset MIPI60 (Rework)
  { GPIO_VER4_S_GPP_F13,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,   GpioTermNone }}, // PCIe SLOT_3 RTD3 Reset MIPI60 (Rework)
  { 0x0 }  // terminator
})}

# mGpioTablePreMemAdlSDdr5UDimm1DCrb
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DCrb]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//CPU PEG Slot1 Reset
  {GPIO_VER4_S_GPP_F11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCIE SLOT 1 - X4 CONNECTOR Reset
  {GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//PCIE SLOT 2 - X4 CONNECTOR Reset
  {GPIO_VER4_S_GPP_F13,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCIE SLOT 3 - X2 CONNECTOR Reset
  {0x0}  // terminator
})}

# mGpioTablePreMemAdlSDdr5UDimm1DAep
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DAep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// PEG SLOT RST_N
  {GPIO_VER4_S_GPP_K4,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_RST_N
  {GPIO_VER4_S_GPP_K3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_RST_N
  {0x0}  // terminator
})}

# mGpioTablePreMemmAdlSSbgaDdr5SODimmErb
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr5SODimmErb]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//CPU PEG Slot Reset
  {GPIO_VER4_S_GPP_E17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCIE SLOT x4 Reset
  {GPIO_VER4_S_GPP_F18,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 CPU SSD reset
  {GPIO_VER4_S_GPP_C10,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD Reset
  {0x0}  // terminator
})}

# mGpioTablePreMemmAdlSSbgaDdr4SODimmCrb
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr4SODimmCrb]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // CPU PEG Slot Reset
  {GPIO_VER4_S_GPP_F22,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCIESLOT_1_RST_N
  {GPIO_VER4_S_GPP_C10,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH SSD Reset
  {GPIO_VER4_S_GPP_F11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // CPU SSD reset
  {0x0}  // terminator
})}

# mGpioTablePreMemAdlSAdpSSbgaDdr5SODimmAep
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr5SODimmAep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//CPU PEG Slot Reset
  {GPIO_VER4_S_GPP_F18,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD1 reset
  {GPIO_VER4_S_GPP_C10,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD2 Reset
  {0x0}  // terminator
})}

