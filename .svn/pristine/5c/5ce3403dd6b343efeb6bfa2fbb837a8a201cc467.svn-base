/** @file
  This file contains functions that implement PCI config access via REGISTER_ACCESS API.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/PciSegmentRegAccessLib.h>
#include <Library/PeiPcieSipInitLib.h>
#include <Library/PciSegmentLib.h>
#include <RegisterAccess.h>

/**
  Reads a 8-bit PCI configuration register

  @param  Access      Pointer to the REGISTER_ACCESS
  @param  Offset      Offset of the register within the PCI config

  @return The 8-bit PCI configuration register specified by Address
**/
UINT8
PcieCfgRead8 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentRead8 (Dev->PciSegmentBase + Offset);
}

/**
  Writes a 8-bit PCI configuration register

  @param  Access      Pointer to the REGISTER_ACCESS
  @param  Offset      Offset of the register within the PCI config
  @param  Value       The value to write

  @return The parameter of Value
**/
UINT8
PcieCfgWrite8 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT8            Value
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentWrite8 (Dev->PciSegmentBase + Offset, Value);
}

/**
  Performs a bitwise OR of a 8-bit PCI configuration register with a 8-bit value

  @param  Access      Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  OrData    The value to OR with the PCI configuration register

  @return The value written to the PCI configuration register
**/
UINT8
PcieCfgOr8 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT8            OrData
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentOr8 (Dev->PciSegmentBase + Offset, OrData);
}

/**
  Performs a bitwise AND of a 8-bit PCI configuration register with a 8-bit value

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register
  @param  AndData   The value to AND with the PCI configuration register

  @return The value written to the PCI configuration register
**/
UINT8
PcieCfgAnd8 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT8            AndData
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentAnd8 (Dev->PciSegmentBase + Offset, AndData);
}

/**
  Performs a bitwise AND of a 8-bit PCI configuration register with a 8-bit value,
  followed a  bitwise OR with another 8-bit value

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  AndData   The value to AND with the PCI configuration register
  @param  OrData    The value to OR with the PCI configuration register

  @return The value written to the PCI configuration register
**/
UINT8
PcieCfgAndThenOr8 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT8            AndData,
  IN UINT8            OrData
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentAndThenOr8 (Dev->PciSegmentBase + Offset, AndData, OrData);
}

/**
  Reads a 16-bit PCI configuration register

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config

  @return The 32-bit PCI configuration register specified by Address
**/
UINT16
PcieCfgRead16 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentRead16 (Dev->PciSegmentBase + Offset);
}

/**
  Writes a 16-bit PCI configuration register

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  Value     The value to write

  @return The parameter of Value
**/
UINT16
PcieCfgWrite16 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT16           Value
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentWrite16 (Dev->PciSegmentBase + Offset, Value);
}

/**
  Performs a bitwise OR of a 16-bit PCI configuration register with a 16-bit value

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  OrData    The value to OR with the PCI configuration register

  @return The value written to the PCI configuration register
**/
UINT16
PcieCfgOr16 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT16           OrData
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentOr16 (Dev->PciSegmentBase + Offset, OrData);
}

/**
  Performs a bitwise AND of a 16-bit PCI configuration register with a 16-bit value

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register
  @param  AndData   The value to AND with the PCI configuration register

  @return The value written to the PCI configuration register
**/
UINT16
PcieCfgAnd16 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT16           AndData
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentAnd16 (Dev->PciSegmentBase + Offset, AndData);
}

/**
  Performs a bitwise AND of a 16-bit PCI configuration register with a 16-bit value,
  followed a  bitwise OR with another 16-bit value

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  AndData   The value to AND with the PCI configuration register
  @param  OrData    The value to OR with the PCI configuration register

  @return The value written to the PCI configuration register
**/
UINT16
PcieCfgAndThenOr16 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT16           AndData,
  IN UINT16           OrData
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentAndThenOr16 (Dev->PciSegmentBase + Offset, AndData, OrData);
}

/**
  Reads a 32-bit PCI configuration register

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config

  @return The 32-bit PCI configuration register specified by Address
**/
UINT32
PcieCfgRead32 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32                    Offset
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentRead32 (Dev->PciSegmentBase + Offset);
}

/**
  Writes a 32-bit PCI configuration register

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  Value     The value to write

  @return The parameter of Value
**/
UINT32
PcieCfgWrite32 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT32           Value
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentWrite32 (Dev->PciSegmentBase + Offset, Value);
}

/**
  Performs a bitwise OR of a 32-bit PCI configuration register with a 32-bit value

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  OrData    The value to OR with the PCI configuration register

  @return The value written to the PCI configuration register
**/
UINT32
PcieCfgOr32 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT32           OrData
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentOr32 (Dev->PciSegmentBase + Offset, OrData);
}

/**
  Performs a bitwise AND of a 32-bit PCI configuration register with a 32-bit value

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register
  @param  AndData   The value to AND with the PCI configuration register

  @return The value written to the PCI configuration register
**/
UINT32
PcieCfgAnd32 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT32           AndData
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentAnd32 (Dev->PciSegmentBase + Offset, AndData);
}

/**
  Performs a bitwise AND of a 32-bit PCI configuration register with a 32-bit value,
  followed a  bitwise OR with another 32-bit value

  @param  Access    Pointer to the REGISTER_ACCESS
  @param  Offset    Offset of the register within the PCI config
  @param  AndData   The value to AND with the PCI configuration register
  @param  OrData    The value to OR with the PCI configuration register

  @return The value written to the PCI configuration register
**/
UINT32
PcieCfgAndThenOr32 (
  IN REGISTER_ACCESS  *Access,
  IN UINT32           Offset,
  IN UINT32           AndData,
  IN UINT32           OrData
  )
{
  PCI_SEGMENT_REG_ACCESS  *Dev;

  Dev = (PCI_SEGMENT_REG_ACCESS*) Access;

  return PciSegmentAndThenOr32 (Dev->PciSegmentBase + Offset, AndData, OrData);
}

/**
  Initializes PCI_SEGMENT_REG_ACCESS based on the PciSegmentBase address.

  @param[in]  PciSegmentBase  PciSegmentBase address. Must be in PciSegmentLib format
  @param[out] PciSegmentRegAccess  Structure to initialize

  @retval TRUE   Initialization successful
  @retval FALSE  Failed to initialize
**/
BOOLEAN
BuildPciSegmentAccess (
  IN UINT64                   PciSegmentBase,
  OUT PCI_SEGMENT_REG_ACCESS  *PciSegmentRegAccess
  )
{
  if (PciSegmentBase == 0 || PciSegmentRegAccess == NULL) {
    return FALSE;
  }
  PciSegmentRegAccess->RegAccess.Read8 = PcieCfgRead8;
  PciSegmentRegAccess->RegAccess.Write8 = PcieCfgWrite8;
  PciSegmentRegAccess->RegAccess.Or8 = PcieCfgOr8;
  PciSegmentRegAccess->RegAccess.And8 = PcieCfgAnd8;
  PciSegmentRegAccess->RegAccess.AndThenOr8 = PcieCfgAndThenOr8;
  PciSegmentRegAccess->RegAccess.Read16 = PcieCfgRead16;
  PciSegmentRegAccess->RegAccess.Write16 = PcieCfgWrite16;
  PciSegmentRegAccess->RegAccess.Or16 = PcieCfgOr16;
  PciSegmentRegAccess->RegAccess.And16 = PcieCfgAnd16;
  PciSegmentRegAccess->RegAccess.AndThenOr16 = PcieCfgAndThenOr16;
  PciSegmentRegAccess->RegAccess.Read32 = PcieCfgRead32;
  PciSegmentRegAccess->RegAccess.Write32 = PcieCfgWrite32;
  PciSegmentRegAccess->RegAccess.Or32 = PcieCfgOr32;
  PciSegmentRegAccess->RegAccess.And32 = PcieCfgAnd32;
  PciSegmentRegAccess->RegAccess.AndThenOr32 = PcieCfgAndThenOr32;
  PciSegmentRegAccess->PciSegmentBase = PciSegmentBase;

  return TRUE;
}
