## @file
#
#******************************************************************************
#* Copyright (c) 2019 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##
### @file
# Component information file for the Platform Init PEI module.
#
# Copyright (c) 2017 - 2019, Intel Corporation. All rights reserved.<BR>
#
# SPDX-License-Identifier: BSD-2-Clause-Patent
#
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = PlatformInitPostMem
  FILE_GUID                      = 59ADD62D-A1C0-44C5-A90F-A1168770468C
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = PEIM
  ENTRY_POINT                    = PlatformInitPostMemEntryPoint

[LibraryClasses]
  PeimEntryPoint
  DebugLib
  IoLib
  MemoryAllocationLib
  BaseMemoryLib
  HobLib
  PeiServicesLib
  BoardInitLib
  TestPointCheckLib
  SetCacheMtrrLib
#[-start-190829-IB15410298-add]#
  PchInfoLib
#[-end-190829-IB15410298-add]#

[Packages]
  MinPlatformPkg/MinPlatformPkg.dec
  MdeModulePkg/MdeModulePkg.dec
  MdePkg/MdePkg.dec
#[-start-190829-IB15410298-add]#
  ClientOneSiliconPkg/SiPkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#[-end-190829-IB15410298-add]#
#[-start-191015-IB1463PowerOn-add]#
  InsydeModulePkg/InsydeModulePkg.dec
#[-end-191015-IB1463PowerOn-add]#
#[-start-201105-IB09480110-add]#
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-end-201105-IB09480110-add]#

[Sources]
  PlatformInitPostMem.c

[Pcd]
#[-start-210113-IB05660151-add]#
  gChipsetPkgTokenSpaceGuid.PcdCrbSkuId
#[-end-210113-IB05660151-add]#  

[Ppis]
  gEfiEndOfPeiSignalPpiGuid                     ## CONSUMES
  gEfiPeiReadOnlyVariable2PpiGuid
#[-start-190829-IB15410298-add]#
  gHybridGraphicsReadyForPowerSequenceInit      ## SOMETIMES_PRODUCES
#[-end-190829-IB15410298-add]#

[Protocols]

[Guids]
#[-start-191015-IB1463PowerOn-add]#
  gEfiSmmPeiSmramMemoryReserveGuid              ## CONSUMES
#[-end-191015-IB1463PowerOn-add]#

[Depex]
  gEfiPeiMemoryDiscoveredPpiGuid

#[-start-190829-IB15410298-add]#
[FeaturePcd]
  gChipsetPkgTokenSpaceGuid.PcdHybridGraphicsSupported
#[-end-190829-IB15410298-add]#
