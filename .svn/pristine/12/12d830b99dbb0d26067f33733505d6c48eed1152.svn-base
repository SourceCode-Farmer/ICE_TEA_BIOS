/** @file
  Support for ME Enahnced Debug mode.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/HeciPpi.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiMeLib.h>
#include <Library/HobLib.h>
#include <Library/DebugLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/ChipsetInitLib.h>
#include <MeEnhancedDebugHob.h>
#include <Register/HeciRegs.h>
#include <Library/PeiServicesLib.h>

/**
  This function retrieves Chipset Init Table header from MeEDebugHob.

  @param[out] CsmeChipsetInitVerInfo  Version Info of Chipset Init Table held by CSME

  @retval EFI_SUCCESS                 Function completed successfully
  @retval EFI_INVALID_PARAMETER       Data Buffer is NULL
  @retval EFI_NOT_FOUND               MeEDebugHob not found
**/
EFI_STATUS
GetEDebugModeChipsetInitHeader (
  OUT PCH_HSIO_VER_INFO *CsmeChipsetInitVerInfo
  )
{
  EFI_PEI_HOB_POINTERS HobPtr;
  ME_E_DEBUG_HOB       *MeEDebugHob;

  if (CsmeChipsetInitVerInfo == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  HobPtr.Guid = GetFirstGuidHob (&gMeEDebugHobGuid);
  ASSERT (HobPtr.Guid != NULL);
  if (HobPtr.Guid != NULL) {
    MeEDebugHob = (ME_E_DEBUG_HOB *) GET_GUID_HOB_DATA (HobPtr.Guid);
  } else {
    return EFI_NOT_FOUND;
  }

  CopyMem (CsmeChipsetInitVerInfo, &MeEDebugHob->CsmeChipsetInitVerInfo, sizeof (PCH_HSIO_VER_INFO));

  return EFI_SUCCESS;
}

/**
  Check if Firmware runs in EDebug Mode

  @retval TRUE            Firmware runs in EDebug Mode.
  @retval FALSE           Firmware is not running in EDebug Mode.
**/
BOOLEAN
IsEDebugMode (
  VOID
  )
{
  EFI_STATUS Status;
  HECI_PPI   *HeciPpi;
  UINT32     MeMode;

  Status = PeiServicesLocatePpi (
             &gHeciPpiGuid,
             0,
             NULL,
             (VOID **) &HeciPpi
             );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  Status = HeciPpi->GetMeMode (&MeMode);
  if (!EFI_ERROR (Status) && (MeMode == ME_MODE_E_DEBUG)) {
    return TRUE;
  }

  return FALSE;
}

/**
  Check if Firmware requests the platform to continue booting in EDebug Mode

  @retval TRUE            EDebug mode requested.
  @retval FALSE           EDebug mode not requested.
**/
BOOLEAN
IsInvokeEDebugModeRequested (
  VOID
  )
{
  HECI_FWS_REGISTER   MeFirmwareStatus;
  UINT64              HeciBaseAddress;

  HeciBaseAddress = PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, 0);
  if (PciSegmentRead16 (HeciBaseAddress + PCI_DEVICE_ID_OFFSET) == 0xFFFF) {
    return FALSE;
  }

  MeFirmwareStatus.ul = PciSegmentRead32 (HeciBaseAddress + R_ME_HFS);

  if (MeFirmwareStatus.r.InvokeEDebugMode) {
    return TRUE;
  }

  return FALSE;
}

/**
  This function will check if the platform should continue the boot and transfer
  from EDebugMode to Normal Mode. The following outcome is possible:
    - transfer to Normal Mode and continue the boot
    - remain in Debug Mode and disable HECI1 interface

  @param[in] TablesMatched   Informs if CSME holds the same Chipset Init Tables as BIOS
**/
VOID
ConfigureEDebugMode (
  IN BOOLEAN TablesMatched
  )
{
  if (!IsEDebugMode ()) {
    return;
  }

  if (!TablesMatched || MeIsEnforceEDebugModeEnabled () || IsInvokeEDebugModeRequested ()) {
    PeiHeciSetEDebugModeState (TRUE);
    return;
  }

  // Platform will transfer to Debug Mode now, we need to disable HECI Interface
  PeiHeciSetEDebugModeState (FALSE);
  HeciDisable ();
}

/**
  Init and install Enhanced Debug Hob.
**/
VOID
InstallEDebugHob (
  VOID
  )
{
  ME_E_DEBUG_HOB *MeEDebugHob;
  ME_E_DEBUG_HOB MeEDebugHobStruct;

  if (!IsEDebugMode ()) {
    return;
  }

  //
  // For Enhanced Debug Mode we need to get the version stored by CSME
  //
  PeiHeciReadFromMphy ((UINT8 *)&MeEDebugHobStruct.CsmeChipsetInitVerInfo, sizeof (PCH_HSIO_VER_INFO));

  MeEDebugHob = BuildGuidDataHob (&gMeEDebugHobGuid, &MeEDebugHobStruct, sizeof (ME_E_DEBUG_HOB));

  if (MeEDebugHob == NULL) {
    DEBUG ((DEBUG_ERROR, "MeEDebugHob could not be created\n"));
    ASSERT (FALSE);
    return;
  }

  DEBUG ((DEBUG_INFO, "ME EDebug HOB installed\n"));
}

