/** @file
  This function handle the register/unregister of PCH PCIe specific SMI events.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "PchSmmHelpers.h"
#include <Register/PmcRegs.h>
#include <Register/PchPcieRpRegs.h>
#include <Register/PcieSipRegs.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/CpuPcieRpLib.h>
#include <CpuPcieInfo.h>
#include <Library/PchPcieRpLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPciBdfLib.h>

extern UINT32  mNumOfRootPorts;

//
// PcieRpHotPlug srcdesc
//
GLOBAL_REMOVE_IF_UNREFERENCED PCH_SMM_SOURCE_DESC PchPcieSmiRpHotPlugTemplate = {
  PCH_SMM_NO_FLAGS,
  {
    {
      {
        PCIE_ADDR_TYPE,
        {R_PCH_PCIE_CFG_MPC}
      },
      S_PCH_PCIE_CFG_MPC,
      N_PCH_PCIE_CFG_MPC_HPME
    },
    NULL_BIT_DESC_INITIALIZER
  },
  {
    {
      {
        PCIE_ADDR_TYPE,
        {R_PCH_PCIE_CFG_SMSCS}
      },
      S_PCH_PCIE_CFG_SMSCS,
      N_PCH_PCIE_CFG_SMSCS_HPPDM
    }
  },
  {
    {
      ACPI_ADDR_TYPE,
      {R_ACPI_IO_SMI_STS}
    },
    S_ACPI_IO_SMI_STS,
    N_ACPI_IO_SMI_STS_PCI_EXP
  }
};

//
// PcieRpLinkActive srcdesc
//
GLOBAL_REMOVE_IF_UNREFERENCED PCH_SMM_SOURCE_DESC PchPcieSmiRpLinkActiveTemplate = {
  PCH_SMM_NO_FLAGS,
  {
    {
      {
        PCIE_ADDR_TYPE,
        {R_PCH_PCIE_CFG_MPC}
      },
      S_PCH_PCIE_CFG_MPC,
      N_PCH_PCIE_CFG_MPC_HPME
    },
    NULL_BIT_DESC_INITIALIZER
  },
  {
    {
      {
        PCIE_ADDR_TYPE,
        {R_PCH_PCIE_CFG_SMSCS}
      },
      S_PCH_PCIE_CFG_SMSCS,
      N_PCH_PCIE_CFG_SMSCS_HPLAS
    }
  },
  {
    {
      ACPI_ADDR_TYPE,
      {R_ACPI_IO_SMI_STS}
    },
    S_ACPI_IO_SMI_STS,
    N_ACPI_IO_SMI_STS_PCI_EXP
  }
};

//
// PcieRpLinkEq srcdesc
//
GLOBAL_REMOVE_IF_UNREFERENCED PCH_SMM_SOURCE_DESC PchPcieSmiRpLinkEqTemplate = {
  PCH_SMM_NO_FLAGS,
  {
    {
      {
        PCIE_ADDR_TYPE,
        {R_PCIE_CFG_EQCFG1}
      },
      S_PCIE_CFG_EQCFG1,
      N_PCIE_CFG_EQCFG1_LERSMIE
    },
    NULL_BIT_DESC_INITIALIZER
  },
  {
    {
      {
        PCIE_ADDR_TYPE,
        {R_PCH_PCIE_CFG_SMSCS}
      },
      S_PCH_PCIE_CFG_SMSCS,
      N_PCH_PCIE_CFG_SMSCS_LERSMIS
    }
  },
  {
    {
      ACPI_ADDR_TYPE,
      {R_ACPI_IO_SMI_STS}
    },
    S_ACPI_IO_SMI_STS,
    N_ACPI_IO_SMI_STS_PCI_EXP
  }
};

/**
  Get Root Port physical Number by CPU or PCH Pcie Root Port Device and Function Number

  @param[in]  RpDev                 Root port device number.
  @param[in]  RpFun                 Root port function number.
  @param[out] RpNumber              Return corresponding physical Root Port index (0-based)
**/
VOID
GetPcieRpNumber (
  IN  UINTN   RpDev,
  IN  UINTN   RpFun,
  OUT UINTN   *RpNumber
  )
{
  GetPchPcieRpNumber (RpDev, RpFun, RpNumber);
}

/**
  Get CPU or PCH Pcie Root Port Device and Function Number by Root Port physical Number

  @param[in]  RpNumber              Root port physical number. (0-based)
  @param[out] RpDev                 Return corresponding root port device number.
  @param[out] RpFun                 Return corresponding root port function number.
**/
VOID
GetPcieRpDevFun (
  IN  UINTN   RpIndex,
  OUT UINTN   *RpDev,
  OUT UINTN   *RpFun
  )
{
  if (RpIndex >= CpuRpIndex0 && RpIndex <= CpuRpIndex3) {
    GetCpuPcieRpDevFun ((RpIndex - CpuRpIndex0), RpDev, RpFun);
  } else {
    *RpDev = PchPcieRpDevNumber (RpIndex);
    *RpFun = PchPcieRpFuncNumber (RpIndex);
  }
}

/**
  For each CPU PCIE RP clear PME SCI status
**/
VOID
ClearPcieSciForCpuRp (
  VOID
  )
{
  UINT32 MaxPorts;
  UINT32 RpIndex;
  UINT64 RpBase;
  MaxPorts = GetMaxCpuPciePortNum ();
  for (RpIndex = 0; RpIndex < MaxPorts; RpIndex++) {
    RpBase = CpuPcieBase (RpIndex);
    if (PciSegmentRead16 (RpBase + PCI_VENDOR_ID_OFFSET) != 0xFFFF) {
      PciSegmentAnd8 ((RpBase + R_PCIE_CFG_MPC + 3), (UINT8)~((UINT8)(B_PCIE_CFG_MPC_PMCE >> 24)));
      PciSegmentWrite32 (RpBase + R_PCIE_CFG_SMSCS, B_PCIE_CFG_SMSCS_PMCS);
    }
  }
}

/**
  For each PCIE RP clear PME SCI status and disable SCI, then PCIEXP_WAKE_STS from PMC.
  This prevents platform from waking more than one time due to a single PCIE wake event.
  Normally it's up to OS to clear SCI statuses. But in a scenario where platform wakes
  and goes to S5 instead of booting to OS, the SCI status would remain set and would trigger another wake.
**/
VOID
ClearPcieSci (
  VOID
  )
{
  UINT32 RpIndex;
  UINT64 RpBase;

  for (RpIndex = 0; RpIndex < mNumOfRootPorts; RpIndex++) {
    RpBase = PchPcieRpPciCfgBase (RpIndex);
    if (PciSegmentRead16 (RpBase + PCI_VENDOR_ID_OFFSET) != 0xFFFF) {
      PciSegmentAnd8 ((RpBase + R_PCH_PCIE_CFG_MPC + 3), (UINT8)~((UINT8)(B_PCH_PCIE_CFG_MPC_PMCE >> 24)));
      PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_SMSCS, B_PCH_PCIE_CFG_SMSCS_PMCS);
    }
  }
  IoWrite16 (mAcpiBaseAddr + R_ACPI_IO_PM1_STS, B_ACPI_IO_PM1_STS_PCIEXP_WAKE_STS);
}
