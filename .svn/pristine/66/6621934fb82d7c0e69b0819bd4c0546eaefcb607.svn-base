/** @file

;******************************************************************************
;* Copyright (c) 2013 - 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_CHIPSET_NAME_LIST_H_
#define _L05_CHIPSET_NAME_LIST_H_

//
// L05 Chipset Name Format: 0x(xxxxyzzz)
//   xxxx - Chipset Vendor ID
//      y - Platform Type
//    zzz - Chipset ID from feature team define
//
// Chipset Vendor ID (xxxx):
//   Intel - 8086
//   Amd   - 1022
//
// Platform Type (y):
//   Nottbook - 1
//   Tablet, Slate - 2
//
// Chipset ID from feature team define (zzz):
//   001 - SharkBay
//   002 - BayTrail-M
//   003 - BayTrail-T
//   004 - BroadWell
//   005 - CarrizoLite
//   006 - Carrizo
//   007 - Skylake
//   008 - Braswell
//   009 - CherryTrail
//   00A - Stoney
//   00B - Bristol
//   00C - ApolloLake
//   00D - KabyLake
//   00E - Mullins
//   00F - RavenRidge
//   010 - GeminiLake
//   011 - CannonLake
//   012 - IceLake
//   013 - Picasso
//   014 - CometLake
//
#define L05_CHIPSET_NAME_SHARKBAY       0x80861001
#define L05_CHIPSET_NAME_BAYTRAIL_M     0x80861002
#define L05_CHIPSET_NAME_BAYTRAIL_T     0x80862003
#define L05_CHIPSET_NAME_BROADWELL      0x80861004
#define L05_CHIPSET_NAME_CARRIZOLITE    0x10221005
#define L05_CHIPSET_NAME_CARRIZO        0x10221006
#define L05_CHIPSET_NAME_SKYLAKE        0x80861007
#define L05_CHIPSET_NAME_BRASWELL       0x80861008
#define L05_CHIPSET_NAME_CHERRYTRAIL    0x80862009
#define L05_CHIPSET_NAME_STONEY         0x1022100A
#define L05_CHIPSET_NAME_BRISTOL        0x1022100B
#define L05_CHIPSET_NAME_APOLLOLAKE     0x8086100C
#define L05_CHIPSET_NAME_KABYLAKE       0x8086100D
#define L05_CHIPSET_NAME_MULLINLS       0x1022100E
#define L05_CHIPSET_NAME_RAVENRIDGE     0x1022100F
#define L05_CHIPSET_NAME_GEMINILAKE     0x80861010
#define L05_CHIPSET_NAME_CANNONLAKE     0x80861011
#define L05_CHIPSET_NAME_ICELAKE        0x80861012
#define L05_CHIPSET_NAME_PICASSO        0x10221013
#define L05_CHIPSET_NAME_COMETLAKE      0x80861014
#define L05_CHIPSET_NAME_TIGERLAKE      0x80861015
#define L05_CHIPSET_NAME_RENOIR         0x10221016
#define L05_CHIPSET_NAME_CEZANNE        0x10221017
#define L05_CHIPSET_NAME_JASPERLAKE     0x80861018
#define L05_CHIPSET_NAME_ALDERLAKE      0x80861019
#define L05_CHIPSET_NAME_REMBRANDT      0x1022101A

#define L05_CHIPSET_VENDOR_ID            (FixedPcdGet32 (PcdL05ChipsetName) >> 16)
#define L05_INTEL_VENDOR_ID              0x8086
#define L05_AMD_VENDOR_ID                0x1022

#if (FixedPcdGet32 (PcdL05ChipsetName) == 0)
  #error [L05 Feature code error notify]: gL05ServicesTokenSpaceGuid.PcdL05ChipsetName is not be defined, please check the setting in InsydeL05PlatformPkg/Package.dsc or INF Files.
#endif

#endif
