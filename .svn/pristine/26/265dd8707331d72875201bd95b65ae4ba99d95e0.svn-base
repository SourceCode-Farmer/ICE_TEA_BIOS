/** @file
  This file contains functions that initializes Dekel

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/PeiCpuPcieDekelInitLib.h>
#include <CpuSbInfo.h>
#include <Library/DebugLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/CpuDmiInfoLib.h>
#include <Library/CpuPlatformLib.h>
#include <CpuDmiPreMemConfig.h>

/**
  Read DEKEL FW download status

  @param[in]  CpuSbDevicePid   CPU SB Device Port ID

  @retval     BOOLEAN          Fw Download Completed TRUE or FALSE
**/

BOOLEAN
EFIAPI
DmiDekelFwStatus (
  IN  CPU_SB_DEVICE_PID  CpuSbDevicePid
)
{
  ///
  ///
  if ((CpuRegbarRead32(CpuSbDevicePid, R_DEKEL_FW_STATUS) & (UINT32)BIT15) != BIT15) {
    if (!IsSimicsEnvironment()) {
      switch (CpuSbDevicePid) {
        case CPU_SB_PID_DEKEL_DMIIO:
          DEBUG ((DEBUG_ERROR, "ERROR: DEKEL FW download failed for DMI PHY !!!\n"));
          ASSERT(FALSE);
          break;
        default:
          DEBUG ((DEBUG_ERROR, "ERROR: DEKEL FW download failed!!!\n"));
          break;
      }
    }
    return FALSE;
  }
  return TRUE;
}

/**
  Initialize the Dekel in PEI

  @param[in]  CpuSbDevicePid   CPU SB Device Port ID
**/

VOID
EFIAPI
DmiDekelInitB0 (
  IN  CPU_SB_DEVICE_PID  CpuSbDevicePid
)
{
  UINT8     Index;

  DEBUG ((DEBUG_INFO, "DmiDekelInitB0 \n"));
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD0, 0xFC90FC5);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD1, 0xFFFFFFFE);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD2, 0x0);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD28, 0x80018532);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD3, 0x074B0743);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD4, 0x0005007F);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD5, 0x00001FBF);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD6, 0x074F0747);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD7, 0x00001FBF);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD8, 0x0000C3C3);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD9, 0x092B0753);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD26, 0x0000C3C3);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD38, 0x0000D6C0);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD43, 0x0003208C);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD44, 0x1F710937);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD51, 0x0F21C0F2);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD52, 0x80018532);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD53, 0xD0F);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD54, 0x3a2022a0);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_LANE_OFFSET, 0x200B400);
  CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_LANE2_OFFSET, 0x200B400);
  for (Index = 0; Index < GetMaxDmiLanes(); Index++) {
    CpuRegbarWrite32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_PCS_DWORD3, 0x3A00407);
  }

}

/**
  Initialize the Dekel in PEI

  @param[in]  CpuDmiPreMemConfig     Instance of CPU_DMI_PREMEM_CONFIG
**/

VOID
EFIAPI
DmiDekelInit (
  IN  CPU_DMI_PREMEM_CONFIG        *CpuDmiPreMemConfig
  )
{
  CPU_SB_DEVICE_PID  CpuSbDevicePid;
  CPU_GENERATION     CpuGeneration;

  CpuSbDevicePid     = CPU_SB_PID_DEKEL_DMIIO;
  CpuGeneration      = GetCpuGeneration ();

  if (((CpuGeneration == EnumAdlCpu) && (GetCpuStepping () >= EnumAdlB0))
      ) {
    DmiDekelInitB0 (CpuSbDevicePid);
    //
    // If CpuDmiPreMemConfig->DmiNewFom is set, Enable New FOM scheme 0x1FB0[2]
    //
    if (CpuDmiPreMemConfig->DmiNewFom) {
      for (UINT32 Index = 0; Index < GetMaxDmiLanes (); Index++) {
      CpuRegbarOr32 (CpuSbDevicePid, (Index * LANE_MULTIPLIER) + R_DEKEL_CMN_DIG_DWORD44, (UINT32)(B_DEKEL_CMN_DIG_DWORD44));
      }
    }
  } else {
    UINT8     Index;

    DEBUG ((DEBUG_INFO, "DmiDekelInit \n"));
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD0, 0x0FC906C7);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD1, 0xFFFFFFFF);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD2, 0x0);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD28, 0x80018532);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD3, 0x074B0743);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD4, 0x0005007F);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD5, 0x00001FBF);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD6, 0x074F0747);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD7, 0x00001FBF);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD8, 0x0000C3C3);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_ANA_DWORD9, 0x092B0753);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD26, 0x0000C3C3);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD38, 0x0000D6C0);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD43, 0x0003208C);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD44, 0x027F0937);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD51, 0x0F21C0F2);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD52, 0x00310310);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD53, 0x0000086B);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD54, 0x00000027);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD56, 0x40200);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_LANE_OFFSET, 0x200B400);
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_LANE2_OFFSET, 0x200B400);
    for (Index = 0; Index < GetMaxDmiLanes(); Index++) {
      CpuRegbarWrite32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_PCS_DWORD3, 0x3A00407);
    }
    for (Index = 0; Index < GetMaxDmiLanes(); Index++) {
      //
      //  Program data lane
      //
      CpuRegbarWrite32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_CDR_L0S_CONFIG, 0x00000A73);
      CpuRegbarWrite32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_CDR_SAVE_RESTORE0, 0x276CE3FB);
      CpuRegbarWrite32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_CFG13, 0x2E00017C);
    }
    for (Index = 0; Index < GetMaxDmiLanes(); Index++) {
      CpuRegbarWrite32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_CALIB_ENABLE_DISABLE, 0x00000001);
    }
    CpuRegbarWrite32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD6, 0x00000001);
    DEBUG ((DEBUG_INFO, "R_DEKEL_CL_SUS_CLK_CRWELL_CNTRL_OFFSET = %x \n",(CpuRegbarRead32 (CpuSbDevicePid, R_DEKEL_CL_SUS_CLK_CRWELL_CNTRL_OFFSET))));
    DEBUG ((DEBUG_INFO, "R_DEKEL_CMN_LANE_OFFSET = %x \n",(CpuRegbarRead32 (CpuSbDevicePid, R_DEKEL_CMN_LANE_OFFSET))));
    DEBUG ((DEBUG_INFO, "R_DEKEL_CMN_LANE2_OFFSET = %x \n",(CpuRegbarRead32 (CpuSbDevicePid, R_DEKEL_CMN_LANE2_OFFSET))));
    for (Index = 0; Index < GetMaxDmiLanes(); Index++) {
      //
      //  Program data lane
      //
      DEBUG ((DEBUG_INFO, "Index = %x R_DEKEL_REDO_RX_CALIB_EQ_TRAIN_OFFSET = %x \n",Index,(CpuRegbarRead32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_REDO_RX_CALIB_EQ_TRAIN_OFFSET))));
      DEBUG ((DEBUG_INFO, "Index = %x R_DEKEL_CFG13 = %x \n",Index,(CpuRegbarRead32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_CFG13))));
      DEBUG ((DEBUG_INFO, "Index = %x R_DEKEL_CDR_FAST_FLT_CFG = %x \n",Index,(CpuRegbarRead32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_CDR_FAST_FLT_CFG))));
      DEBUG ((DEBUG_INFO, "Index = %x R_DEKEL_CDR_SAVE_RESTORE0 = %x \n",Index,(CpuRegbarRead32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_CDR_SAVE_RESTORE0))));
      DEBUG ((DEBUG_INFO, "Index = %x R_DEKEL_CFG3 = %x \n",Index,(CpuRegbarRead32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_CFG3))));
      DEBUG ((DEBUG_INFO, "Index = %x R_DEKEL_PCS_DWORD3 = %x \n",Index,(CpuRegbarRead32 (CpuSbDevicePid, Index * LANE_MULTIPLIER + R_DEKEL_PCS_DWORD3))));
    }
  }
}

/**
  Read DEKEL Firmware Version

  @param[in]  CpuSbDevicePid   CPU SB Device Port ID

  @retval     UINT32         Dekel Firmware Version
**/

UINT32
EFIAPI
DmiGetDekelFwVersion (
  IN  CPU_SB_DEVICE_PID  CpuSbDevicePid
)
{
  UINT32       DekelFwVersion;

  DekelFwVersion = CpuRegbarRead32(CpuSbDevicePid, R_DEKEL_FW_VERSION_OFFSET);
  DEBUG ((DEBUG_INFO, "DekelFwVersion %x\n", DekelFwVersion));
  return DekelFwVersion;
}