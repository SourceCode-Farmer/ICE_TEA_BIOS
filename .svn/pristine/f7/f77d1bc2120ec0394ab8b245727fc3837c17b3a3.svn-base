//;******************************************************************************
/** @file
  This driver is for processing the Legacy to Efi Callback Functions

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _LEGACY_TO_EFI_SMM_H_
#define _LEGACY_TO_EFI_SMM_H_

#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/DevicePathLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PrintLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/IoLib.h>

//
// Consumed Protocols
//
#include <Protocol/SmmVariable.h>
#include <Protocol/SmmBase2.h>
#include <Protocol/SmmInt15Service.h>
#include <Protocol/LoadedImage.h>
#include <Protocol/NovoRecovery.h>  // L05_NOVO_RECOVERY_SYSTEM_NAME
#include <Protocol/L05HddSpindown.h>  // EFI_L05_HDD_SPINDOWN_PROTOCOL

//
// Produced Protocols
//

//
// Guids
//
#include <Guid/DebugMask.h>
#include <Guid/GlobalVariable.h>

//
// INT15 HOOK NUMBER
//
#define INT19_HOOK_POINTS               0x9998

#define INT19_ENTRY                     0x0
#define BEFORE_BOOT_ENTRY               0x1
#define LEGACY_BOOT_FAILURE             0x2
#define ALL_LEGACY_BOOT_FAILURE         0x3
#define BEFORE_BOOT_TO_OS               0x4

#define ATTEMPED_BOOT                   0x02
#define BDA(a)                          (*(UINT16*)((UINTN)0x400 +(a)))
#define EBDA(a)                         (*(UINT16*)(UINTN)(((*(UINT16*)(UINTN)0x40e) << 4) + (a)))

//
// Reset Generator I/O Port
//
#define IO_RST_CNT                      0xCF9
#define IO_RST_CNT_FULL_RST             BIT3
#define IO_RST_CNT_RST_CPU              BIT2
#define IO_RST_CNT_SYS_RST              BIT1
#define IO_RST_CNT_FULLRESET            0x0E
#define IO_RST_CNT_HARDRESET            0x06
#define IO_RST_CNT_SOFTRESET            0x04
#define IO_RST_CNT_HARDSTARTSTATE       0x02
#define IO_RST_CNT_SOFTSTARTSTATE       0x00

#endif
