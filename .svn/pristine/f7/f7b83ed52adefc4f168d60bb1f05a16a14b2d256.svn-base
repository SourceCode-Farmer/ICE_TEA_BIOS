## @file
#  RC Package Flash Description File
#
#******************************************************************************
#* Copyright (c) 2012 - 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
[FV.BACKUPFV]

[FV.RECOVERYFV0]

[FV.RECOVERYFV]

  INF IntelFsp2WrapperPkg/FspmWrapperPeim/FspmWrapperPeim.inf

#[-start-200601-IB05310158-remove]#
#[-start-200220-IB10189038-add]#
# !if gSiPkgTokenSpaceGuid.PcdPeiDisplayEnable == TRUE
#   FILE PEIM = 76ed893a-b2f9-4c7d-a05f-1ea170ecf6cd {
#   SECTION COMPRESS {
#   !if $(TARGET) == DEBUG
#     SECTION Align = Auto PE32 = $(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Graphics/IntelGraphicsPeim/Binaries/IntelGraphicsPeimDebug.efi
#   !else
#     SECTION Align = Auto PE32 = $(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Graphics/IntelGraphicsPeim/Binaries/IntelGraphicsPeim.efi
#   !endif
#     SECTION PEI_DEPEX_EXP = {gEfiPeiStallPpiGuid AND gEnablePeiGraphicsPpiGuid}
#     SECTION UI = "IntelGraphicsPeim"
#   }
# }
# !endif # PcdPeiDisplayEnable
#[-end-200220-IB10189038-add]#
#[-end-200601-IB05310158-remove]#

[FV.RECOVERYFV2]


[FV.DXEFV]
#
# Common
#
  INF  $(C1S_PRODUCT_PATH)/SiInit/Dxe/SiInitDxe.inf
  INF  $(PLATFORM_SI_PACKAGE)/Pch/PchInit/Dxe/PchInitDxeTgl.inf
  INF  $(PLATFORM_SI_PACKAGE)/Pch/SmmControl/RuntimeDxe/SmmControl.inf

!if gSiPkgTokenSpaceGuid.PcdSmmVariableEnable == TRUE
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Spi/Smm/SpiSmm.inf
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Spi/Dxe/SpiSmmStub.inf
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Spi/Dxe/SpiSmmDxe.inf
!else
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Spi/RuntimeDxe/SpiRuntime.inf
!endif
#[-start-190116-IB0672-add]#
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Spi/RuntimeDxe/SpiRuntime.inf
#[-end-190116-IB0672-add]#
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Smbus/Smm/SmbusSmm.inf
#[-start-160112-IB10870324-add]#
  #
  # Do not remove this module or Legacy Boot mode/Dual Boot Type mode will not work!
  #
#[-end-160112-IB10870324-add]#
!if gSiPkgTokenSpaceGuid.PcdThcEnable == TRUE
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Thc/ThcDriver/Thc.inf
!endif
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Wdt/Dxe/WdtDxe.inf
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Smbus/Dxe/SmbusDxe.inf
  INF  $(PLATFORM_SI_PACKAGE)/Pch/PchSmiDispatcher/Smm/PchSmiDispatcher.inf
  INF  $(PLATFORM_SI_PACKAGE)/Pch/PchInit/Smm/PchInitSmm.inf

#[-start-170418-IB07340000-V79-PO-FSP-modify]#
  #
  # Comment out below line if ASPM Init After Oprom was not supported
  #
  INF MdeModulePkg/Bus/Pci/PciHostBridgeDxe/PciHostBridgeDxe.inf
#[-end-170418-IB07340000-V79-PO-FSP-modify]#

!if gSiPkgTokenSpaceGuid.PcdHgEnable == TRUE
!if gSiPkgTokenSpaceGuid.PcdAcpiEnable == TRUE
INF  RuleOverride = ACPITABLE $(PLATFORM_SI_PACKAGE)/IpBlock/HybridGraphics/AcpiTables/Peg/HgAcpiTables.inf
INF  RuleOverride = ACPITABLE $(PLATFORM_SI_PACKAGE)/IpBlock/HybridGraphics/AcpiTables/Pch/HgAcpiTablesPch.inf
!endif
!endif

#[-start-201222-IB09480120-modify]#
!if gSiPkgTokenSpaceGuid.PcdAcpiEnable == TRUE
INF RuleOverride = ACPITABLE $(PLATFORM_SI_PACKAGE)/Fru/AdlPch/Include/AcpiTables/Soc/SocGpeSsdt.inf
INF RuleOverride = ACPITABLE $(PLATFORM_SI_PACKAGE)/Fru/AdlPch/Include/AcpiTables/Soc/SocCommonSsdt.inf
!endif
#[-end-201222-IB09480120-modify]#

!if gSiPkgTokenSpaceGuid.PcdAcpiEnable
  INF  RuleOverride = ACPITABLE $(PLATFORM_SI_PACKAGE)/IpBlock/Vtd/AcpiTables/DmarAcpiTablesVer1.inf
  INF  RuleOverride = ACPITABLE $(PLATFORM_SI_PACKAGE)/SystemAgent/AcpiTables/SaSsdt/SaSsdt.inf
  INF  RuleOverride = ACPITABLE $(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/AcpiTables/IgfxSsdt.inf
#[-start-210824-IB09480150-add]#
!if gSiPkgTokenSpaceGuid.PcdIpuEnable == TRUE
  INF  RuleOverride = ACPITABLE $(PLATFORM_SI_PACKAGE)/IpBlock/Ipu/AcpiTables/IpuSsdt.inf
!endif
#[-end-210824-IB09480150-add]#
!if gSiPkgTokenSpaceGuid.PcdITbtEnable == TRUE
  INF  RuleOverride = ACPITABLE $(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/AcpiTables/TcssSsdt/TcssSsdt.inf
!endif
!endif

  #
  # PROJECT_CPU_DIR
  #
  INF  $(PLATFORM_SI_PACKAGE)/Cpu/CpuInit/Dxe/CpuInitDxe.inf
#[-start-201021-IB11790397-modify]#
#!if gSiPkgTokenSpaceGuid.PcdBiosGuardEnable == TRUE
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/BiosGuard/Smm/BiosGuardServices.inf
#!endif
#[-end-201021-IB11790397-modify]#
!if gSiPkgTokenSpaceGuid.PcdPpmEnable == TRUE
  INF  $(PLATFORM_SI_PACKAGE)/Cpu/PowerManagement/Dxe/PowerMgmtDxe.inf
!endif
!if gSiPkgTokenSpaceGuid.PcdTxtEnable
#[-start-140829-IB10820525-modify]#
  INF  $(PLATFORM_SI_PACKAGE)/Cpu/TxtInit/Dxe/TxtDxe.inf
#[-end-140829-IB10820525-modify]#
!endif
  INF RuleOverride = ACPITABLE $(PLATFORM_SI_PACKAGE)/Cpu/AcpiTables/CpuAcpiTables.inf

  #
  # SystemAgent
  #
  INF  $(PLATFORM_SI_PACKAGE)/SystemAgent/SaInit/Dxe/SaInitDxe.inf
  INF  $(PLATFORM_SI_PACKAGE)/SystemAgent/BdatAccessHandler/Dxe/BdatAccessHandler.inf

  #
  # Me
  #
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Me/HeciInit/Dxe/HeciInit.inf
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Me/MeFwDowngrade/Dxe/MeFwDowngrade.inf

!if gSiPkgTokenSpaceGuid.PcdJhiEnable
  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Me/Jhi/Dxe/JhiDxe.inf
!endif

#[-start-201223-IB19010009-remove]#
  #
  # 5MB
  #
#!if gSiPkgTokenSpaceGuid.PcdAmtEnable
#  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Amt/BiosExtensionLoader/Dxe/BiosExtensionLoader.inf
#  INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Amt/Sol/Dxe/SerialOverLan.inf
#!endif
#[-end-201223-IB19010009-remove]#
#[-start-200220-IB10189038-add]#
!if gSiPkgTokenSpaceGuid.PcdSmbiosEnable == TRUE
  INF $(PLATFORM_SI_PACKAGE)/IpBlock/Me/MeSmbiosDxe/MeSmbiosDxe.inf
!endif

!if gPlatformModuleTokenSpaceGuid.PcdPiI2cStackEnable == TRUE
  INF $(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/I2c/I2cDriver/SerialIoI2cDriver.inf
!endif

INF $(PLATFORM_SI_PACKAGE)/IpBlock/Me/Asf/Dxe/AsfDxe.inf
INF $(PLATFORM_SI_PACKAGE)/IpBlock/Me/HwAsset/Dxe/HwAssetDxe.inf
INF $(PLATFORM_SI_PACKAGE)/SystemAgent/SaInit/Smm/SaLateInitSmm.inf

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
  INF $(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Uart/UartDriver/SerialIoUartDriver.inf
!endif
#[-start-201223-IB19010009-remove]#
#!if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE
#  INF $(PLATFORM_SI_PACKAGE)/IpBlock/Amt/AmtInit/Dxe/AmtInitDxe.inf
#!endif
#[-end-201223-IB19010009-remove]#
#[-end-200220-IB10189038-add]#
