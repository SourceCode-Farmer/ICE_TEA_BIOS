/** @file

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*
    == Readme ==
    For NVOP support, SBIOS has to adjust below code to meet platform:
    - Modify the method path of \_SB.PCI0.PGON and \_SB.PCI0.PGOF to Intel RC dGPU ON/OFF method
    - Modify enable the GPI event for illustrating the GPE event from HPD to notify 0x81
      to dGPU for HPD handling when dGPU is Off. 
*/

//
// Note: Following sample code is added into Gpe.asl (_L61 event) for CRB.
//       Please based on your project design to add this notification into
//       corresponding event.
//
// IBV should be to follow the below defines to support HPD notification.
// Example:
// HDMI or DP HotPlug evnet GPIO pin
//   1. Connect to PCH
//     GPI event number: _L61
//     #define HPD_L_EVENT_NUMBER _L61
//     #define EVENT_DEVICE_PATH \_GPE
//
//   2. Connect to EC
//     GPI evennt enumber: Q21
//     #define HPD_L_EVENT_NUMBER Q21
//     #define EVENT_DEVICE_PATH \_SB.PCI0.SBRG.EC0
//
// DGPU device path: \_SB.PCI0.PEG0.PEGP
//   #define DGPU_DEVICE_PATH \_SB.PCI0.PEG0.PEGP

//#ifdef HPD_L_EVENT_NUMBER
//    External (EVENT_DEVICE_PATH, DeviceObj)
//    Scope (EVENT_DEVICE_PATH)
//    {
//        Method (HPD_L_EVENT_NUMBER, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
//        {
//            Notify (DGPU_DEVICE_PATH, 0x81) // Information Change
//        }
//    }
//#endif

Scope (DGPU_SCOPE)
{
    Name (VGAB, Buffer(0xFB)
    {
        0x00
    })

    Method (_PS0, 0)
    {
        If (LNotEqual (DGPS, Zero))
        {
            DGPU_BRIDGE_SCOPE.PXP._ON ()
            If (LNotEqual (GPRF, One))
            {
                Store (VGAB, VGAR)
            }
            Store (Zero, DGPS)
        }
    }

    Method (_PS3, 0)
    {
        If (LEqual (OMPR, 0x3))
        {
            If (LNotEqual (GPRF, One))
            {
                Store (VGAR, VGAB)
            }
            DGPU_BRIDGE_SCOPE.PXP._OFF ()
            Store (One, DGPS)
            Store (0x2, OMPR)
        }
    }

    Name (DGPS, Zero)
    Name (OMPR, 0x2) // Optimus MXM Power-Control Ready
    Name (GPRF, Zero)
    Name (DPST, One)

    Method (NVOP, 4, Serialized)
    {
        Store ("------- NV OPTIMUS DSM --------", Debug)
        // Only Interface Revision 0x0100 is supported
        If (LNotEqual (Arg1, 0x100))
        {
            Return (STATUS_ERROR_UNSPECIFIED)
        }
        // (Arg2) Sub-Function
        Switch (ToInteger (Arg2))
        {
            //
            // Function 0: NVOP_FUNC_SUPPORT - Bit list of supported functions.
            //
            case (NVOP_FUNC_SUPPORT)
            {
                Store ("   NVOP fun0 NVOP_FUNC_SUPPORT", Debug)
                Return (Buffer(0x04)
                {
                    // sub-func: 0,26,27 supported
                    0x01, 0x00, 0x00, 0x0C
                })
            }

            //
            // Function 26: NVOP_FUNC_OPTIMUSCAPS - Optimus Capabilities.
            //
            case (NVOP_FUNC_OPTIMUSCAPS)
            {
                Store ("   NVOP fun26 NVOP_FUNC_OPTIMUSCAPS", Debug)
                CreateField (Arg3,  0, 1, FLCH)
                CreateField (Arg3,  1, 1, DVSR) // Modify Optimus DSM 0x1A for GC6 TDR support.
                CreateField (Arg3,  2, 1, DVSC) // Modify Optimus DSM 0x1A for GC6 TDR support.
                CreateField (Arg3, 24, 2, OPCE)

                If (LAnd (ToInteger (FLCH), LNotEqual (ToInteger (OPCE), OMPR)))
                {
                    Store (ToInteger (OPCE), OMPR) // Optimus Power Control Enable - From DD
                }

                // Definition of return buffer.
                // bit 0     - Optimus Enabled
                //             0 : Optimus Graphics Disabled
                //             1 : Optimus Graphics Enabled (default)
                // bit 4:3   - Current GPU Control Status
                //             0 : GPU is powered off
                //             3 : GPU power has stabilized (default)
                // bit 6     - Shared discrete GPU Hot-Plug Capabilities
                //             1 : There are discrete GPU Display Hot-Plug signals co-connected to the platform
                // bit 8     - PCIe Configuration Space Owner Actual
                //             0 : SBIOS
                //             1 : GPU Driver
                // bit 26:24 - Optimus Capabilities
                //             0 : No special platform capabilities
                //             1 : Platform has dynamic GPU power control
                // bit 27:28 - Optimus HD Audio Codec Capabilities
                //             0 : No audio codec-related capabilities
                //             1 : Platform does not use HD audio
                //             2 : Platform supports Optimus dynamic codec control
                Store (Buffer(4) {0, 0, 0, 0}, Local0)
                CreateField (Local0,  0, 1, OPEN)
                CreateField (Local0,  3, 2, CGCS)
                CreateField (Local0,  6, 1, SHPC) // Shared discrete GPU Hot-Plug Capabilities
                CreateField (Local0,  8, 1, SNSR) // Modify Optimus DSM 0x1A for GC6 TDR support.
                CreateField (Local0, 24, 3, DGPC) // Optimus Power Capabilities
                CreateField (Local0, 27, 2, OHAC) // Optimus HD Audio Codec Capabilities

                Store (One, OPEN) // Optimus Enabled
                Store (One, SHPC) // Set '1' indicates there are discrete GPU Display Hot-Plug signals co-connected to the platform
                Store (One, DGPC) // Optimus Power Capabilities
                                  // 0: No special platform capabilities
                                  // 1: Dynamic GPU Power Control
                Store (0x3, OHAC) // Optimus HD Audio Codec Capabilities
                                  // 0: No audio codec-related capabilities
                                  // 1: Platform does not use HD audio (SBIOS will always disable audio codecs)
                                  // 2: Optimus dynamic codec control
                                  // 3: Dynamic power state reporting

                If (ToInteger (DVSC))
                {
                    If (ToInteger (DVSR))
                    {
                        Store (One, GPRF)
                    }
                    Else
                    {
                        Store (Zero, GPRF)
                    }
                }
                Store (GPRF, SNSR)

                If (LEqual (DGPS, Zero))
                {
                    Store (0x3, CGCS) // Current GPU Control status: On
                }
                ELSE
                {
                    Store (0x0, CGCS) // Current GPU Control status: Off
                }
                Return (Local0)
            }

            //
            // Function 27: NVOP_FUNC_OPTIMUSFLAGS - Optimus State flags.
            //
            case (NVOP_FUNC_OPTIMUSFLAGS)
            {
                Store ("   NVOP fun27 NVOP_FUNC_OPTIMUSFLAGS", Debug)
                CreateField (Arg3, 0x00, 1, OACC) // Optimus Audio Codec Control
                CreateField (Arg3, 0x01, 1, UOAC) // Update Optimus Audio Codec Control
                CreateField (Arg3, 0x02, 8, OPDA) // Count of the number of applications running on the dGPU
                CreateField (Arg3, 0x0A, 1, OPDE) // Indicate bit 9:2 valid or not

                Store (Zero, Local1)
                Store (DGPU_SCOPE.HDAE, Local1) // Returns the currnet status of the audio device
                Return (Local1)
            }

            default
            {
                //
                // FunctionCode or SubFunctionCode not supported
                //
                Return (STATUS_ERROR_UNSUPPORTED)
            }
        }
    }
}