## @file
#  FDF file of Platform.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2018 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

#=================================================================================#
# 16 M BIOS - for FSP wrapper
#=================================================================================#
DEFINE FLASH_BASE                                                   = 0xFF000000  #
DEFINE FLASH_SIZE                                                   = 0x01000000  #
DEFINE FLASH_BLOCK_SIZE                                             = 0x00010000  #
DEFINE FLASH_NUM_BLOCKS                                             = 0x00000100  #
DEFINE ACM_ALIGNMENT_ON_FV_BASE                                     = 256K        #
#=================================================================================#

!if $(TARGET) == DEBUG
#=================================================================================#
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageOffset             = 0x00000000  # Flash addr (0xFF000000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageSize               = 0x00060000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageVariableOffset     = 0x00000000  # Flash addr (0xFF000000)
SET gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableSize      = 0x0002E000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingOffset   = 0x0002E000  # Flash addr (0xFF02E000)
SET gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingSize    = 0x00002000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareOffset     = 0x00030000  # Flash addr (0xFF030000)
SET gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareSize      = 0x00030000  #
#
# OBB - Begin
# (Note : OBB here is referenced for capsule update solution. BIOS_INFO defines OBB for Secure Boot)
#
SET gBoardModuleTokenSpaceGuid.PcdFlashObbOffset                      = 0x00060000  # Flash addr (0xFF060000)
SET gBoardModuleTokenSpaceGuid.PcdFlashObbSize                        = 0x007A0000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvAdvancedOffset            = 0x000E0000  # Flash addr (0xFF0E0000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvAdvancedSize              = 0x00170000  #
SET gBoardModuleTokenSpaceGuid.PcdFlashFvOptionalOffset               = 0x00250000  # Flash addr (0xFF250000)
SET gBoardModuleTokenSpaceGuid.PcdFlashFvOptionalSize                 = 0x00360000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvSecurityOffset            = 0x005B0000  # Flash addr (0xFF5B0000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvSecuritySize              = 0x00080000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvOsBootOffset              = 0x00630000  # Flash addr (0xFF630000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvOsBootSize                = 0x00060000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvUefiBootOffset            = 0x00690000  # Flash addr (0xFF690000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvUefiBootSize              = 0x00170000  #
#
# OBB - End
#
#
# IBBR - Begin
# (Note : IBB here is referenced for capsule update solution. BIOS_INFO defines IBB for Secure Boot)
#
SET gBoardModuleTokenSpaceGuid.PcdFlashIbbROffset                     = 0x00800000  # Flash addr (0xFF800000)
SET gBoardModuleTokenSpaceGuid.PcdFlashIbbRSize                       = 0x00400000  #
#
# IBBR - End
#
#
# IBB - Begin
# (Note : IBB here is referenced for capsule update solution. BIOS_INFO defines IBB for Secure Boot)
#
SET gBoardModuleTokenSpaceGuid.PcdFlashIbbOffset                      = 0x00C00000  # Flash addr (0xFFC00000)
SET gBoardModuleTokenSpaceGuid.PcdFlashIbbSize                        = 0x00400000  #
SET gBoardModuleTokenSpaceGuid.PcdFlashFvRsvdOffset                   = 0x00C00000  # Flash addr (0xFFC00000)
SET gBoardModuleTokenSpaceGuid.PcdFlashFvRsvdSize                     = 0x00000000  #
## Firmware binaries FV absolute address requires (256KB align - ACM_ALIGNMENT_ON_FV_BASE).
## Today's ADL uses ACM_ALIGNMENT_ON_FV_BASE = 256KB for an effective use of flash space based on the FV layout.
## so set Firmware binaries FV Offset in 256KB aligned i.e. 0x00N00000,0x00N40000,0x00N80000,or 0x00NC0000.
## Build script checks the requirement.
SET gBoardModuleTokenSpaceGuid.PcdFlashFvFirmwareBinariesOffset       = 0x00C00000  # Flash addr (0xFFC00000)
SET gBoardModuleTokenSpaceGuid.PcdFlashFvFirmwareBinariesSize         = 0x00080000  # Keep 0x80000 or larger
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeOffset           = 0x00C80000  # Flash addr (0xFFC80000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeSize             = 0x00050000  # only can have one microcode in resiliency bios consider debug recovery and resiliency support
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPostMemoryOffset          = 0x00CD0000  # Flash addr (0xFFCD0000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPostMemorySize            = 0x00040000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspSOffset                = 0x00D10000  # Flash addr (0xFFD10000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspSSize                  = 0x000B0000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspMOffset                = 0x00DC0000  # Flash addr (0xFFDC0000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspMSize                  = 0x00140000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspTOffset                = 0x00F00000  # Flash addr (0xFFF00000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspTSize                  = 0x00010000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPreMemoryOffset           = 0x00F10000  # Flash addr (0xFFF10000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPreMemorySize             = 0x000F0000  #
#
# IBB - End
#
#=================================================================================#
!else
#=================================================================================#
#=================================================================================#
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageOffset             = 0x00000000  # Flash addr (0xFF000000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageSize               = 0x00060000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageVariableOffset     = 0x00000000  # Flash addr (0xFF000000)
SET gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableSize      = 0x0002E000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingOffset   = 0x0002E000  # Flash addr (0xFF02E000)
SET gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingSize    = 0x00002000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareOffset     = 0x00030000  # Flash addr (0xFF030000)
SET gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareSize      = 0x00030000  #
#
# OBB - Begin
# (Note : OBB here is referenced for capsule update solution. BIOS_INFO defines OBB for Secure Boot)
#
SET gBoardModuleTokenSpaceGuid.PcdFlashObbOffset                      = 0x00060000  # Flash addr (0xFF060000)
SET gBoardModuleTokenSpaceGuid.PcdFlashObbSize                        = 0x007A0000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvAdvancedOffset            = 0x000E0000  # Flash addr (0xFF0E0000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvAdvancedSize              = 0x001F0000  #
SET gBoardModuleTokenSpaceGuid.PcdFlashFvOptionalOffset               = 0x002D0000  # Flash addr (0xFF2D0000)
SET gBoardModuleTokenSpaceGuid.PcdFlashFvOptionalSize                 = 0x00300000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvSecurityOffset            = 0x005D0000  # Flash addr (0xFF5D0000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvSecuritySize              = 0x00080000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvOsBootOffset              = 0x00650000  # Flash addr (0xFF650000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvOsBootSize                = 0x00060000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvUefiBootOffset            = 0x006B0000  # Flash addr (0xFF6B0000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvUefiBootSize              = 0x00150000  #
#
# OBB - End
#
#
# IBBR - Begin
# (Note : IBB here is referenced for capsule update solution. BIOS_INFO defines IBB for Secure Boot)
#
SET gBoardModuleTokenSpaceGuid.PcdFlashIbbROffset                     = 0x00800000  # Flash addr (0xFF800000)
SET gBoardModuleTokenSpaceGuid.PcdFlashIbbRSize                       = 0x00400000  #
#
# IBBR - End
#
#
# IBB - Begin
# (Note : IBB here is referenced for capsule update solution. BIOS_INFO defines IBB for Secure Boot)
#
SET gBoardModuleTokenSpaceGuid.PcdFlashIbbOffset                      = 0x00C00000  # Flash addr (0xFFC00000)
SET gBoardModuleTokenSpaceGuid.PcdFlashIbbSize                        = 0x00400000  #
SET gBoardModuleTokenSpaceGuid.PcdFlashFvRsvdOffset                   = 0x00C00000  # Flash addr (0xFFC00000)
SET gBoardModuleTokenSpaceGuid.PcdFlashFvRsvdSize                     = 0x00000000  #
## Firmware binaries FV absolute address requires (256KB align - ACM_ALIGNMENT_ON_FV_BASE).
## Today's ADL uses ACM_ALIGNMENT_ON_FV_BASE = 256KB for an effective use of flash space based on the FV layout.
## so set Firmware binaries FV Offset in 256KB aligned i.e. 0x00N00000,0x00N40000,0x00N80000,or 0x00NC0000.
## Build script checks the requirement.
SET gBoardModuleTokenSpaceGuid.PcdFlashFvFirmwareBinariesOffset       = 0x00C00000  # Flash addr (0xFFC00000)
SET gBoardModuleTokenSpaceGuid.PcdFlashFvFirmwareBinariesSize         = 0x00080000  # Keep 0x80000 or larger
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeOffset           = 0x00C80000  # Flash addr (0xFFC80000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeSize             = 0x00180000  # set to 0x180000 for 6 microcode
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPostMemoryOffset          = 0x00E00000  # Flash addr (0xFFE00000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPostMemorySize            = 0x00030000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspSOffset                = 0x00E30000  # Flash addr (0xFFE30000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspSSize                  = 0x00070000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspMOffset                = 0x00EA0000  # Flash addr (0xFFEA0000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspMSize                  = 0x000B0000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspTOffset                = 0x00F50000  # Flash addr (0xFFF50000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspTSize                  = 0x00010000  #
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPreMemoryOffset           = 0x00F60000  # Flash addr (0xFFF60000)
SET gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPreMemorySize             = 0x000A0000  #
#
# IBB - End
#
!endif
