/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2020 , Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*++

Module Name:

 RestoreCmosPei.c

--*/

#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/CmosLib.h>
#include <Library/PeiServicesLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Guid/CmosInVariable.h>
#include <ChipsetCmos.h>

VOID
DumpCmosInVariableValue (
  UINT8   *CmosInVariable
  )
{
  UINTN   Index;

  DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "CMOS value save in variable:" ) );
  for (Index = 0; Index < CMOS_TOTAL_LENGTH; Index += 16) {
    DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x", \
      CmosInVariable[Index], CmosInVariable[Index+1], \
      CmosInVariable[Index+2], CmosInVariable[Index+3], \
      CmosInVariable[Index+4], CmosInVariable[Index+5], \
      CmosInVariable[Index+6], CmosInVariable[Index+7], \
      CmosInVariable[Index+8], CmosInVariable[Index+9], \
      CmosInVariable[Index+10], CmosInVariable[Index+11], \
      CmosInVariable[Index+12], CmosInVariable[Index+13], \
      CmosInVariable[Index+14], CmosInVariable[Index+15] \
      ) );
  }

  return;
}

VOID
DumpCmosValue (
//[-start-190610-IB16990033-add]//
  VOID
//[-end-190610-IB16990033-add]//
  )
{
  UINT8   Index;

  DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "CMOS Bank0 table:" ) );
  for (Index = 0; Index < (CMOS_TOTAL_LENGTH/2); Index += 16) {
    DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x", \
      ReadCmos8 (Index), ReadCmos8 (Index+1), \
      ReadCmos8 (Index+2), ReadCmos8 (Index+3), \
      ReadCmos8 (Index+4), ReadCmos8 (Index+5), \
      ReadCmos8 (Index+6), ReadCmos8 (Index+7), \
      ReadCmos8 (Index+8), ReadCmos8 (Index+9), \
      ReadCmos8 (Index+10), ReadCmos8 (Index+11), \
      ReadCmos8 (Index+12), ReadCmos8 (Index+13), \
      ReadCmos8 (Index+14), ReadCmos8 (Index+15) \
      ) );
  }

  DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "CMOS Bank1 table:" ) );
  for (Index = 0; Index < (CMOS_TOTAL_LENGTH/2); Index += 16) {
    DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x", \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+1), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+2), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+3), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+4), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+5), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+6), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+7), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+8), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+9), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+10), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+11), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+12), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+13), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+14), \
      ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Index+15)
      ) );
  }

  return;
}

/**

  This feature will check if cmos data is destoried and check if gCmosInVariableGuid is exist.
  When Both are meet. Code will restore cmos value from gCmosInVariableGuid variable.
  
  @param[in] FileHandle     Handle of the file being invoked.
  @param[in] PeiServices    It's a general purpose services available to every PEIM.

  @retval EFI_SUCCESS       RestoreCmos feature is executed.
  @retval EFI_UNSUPPORTED   RestoreCmos feature is not executded.

**/
EFI_STATUS
RestoreCmosEntry (
  IN EFI_PEI_FILE_HANDLE        FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS                            Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI       *VariableServices;
  UINT8                                 CmosInVariable[CMOS_TOTAL_LENGTH];
  UINT8                                 Index;
  UINT8                                 Address;
  UINTN                                 Size;

  Status = ValidateCmosChecksum();
  if (Status == EFI_SUCCESS) {
    return EFI_UNSUPPORTED;
  }
  DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "CMOS checksum is error" ) );
  //
  // Locate Variable Ppi
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);

  //
  // Get CmosInVariable value
  //
  Size = CMOS_TOTAL_LENGTH;
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"CmosVariable",
                               &gCmosInVariableGuid,
                               NULL,
                               &Size,
                               CmosInVariable
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "gCmosInVariableGuid is not exist" ) );
    DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "RestoreCmos feature is not executed" ) );
    return EFI_UNSUPPORTED;
  }

  DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "RestoreCmos feature is executed" ) );
  //
  // Restore cmos value from variable.
  //
  DumpCmosInVariableValue (CmosInVariable);
  DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "CMOS value before restoring" ) );
  DumpCmosValue ();

  for (Address = CMOS_SAVE_START_OFFSET, Index = CMOS_SAVE_START_OFFSET; Address < (CMOS_TOTAL_LENGTH/2); Address++, Index++) {
    WriteCmos8(Address, CmosInVariable[Index]);
  }
  for (Address = 0x00; Address < (CMOS_TOTAL_LENGTH/2); Address++, Index++) {
    WriteExtCmos8(R_XCMOS_INDEX, R_XCMOS_DATA, Address, CmosInVariable[Index]);
  }

  DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "CMOS value after restoring" ) );
  DumpCmosValue ();

  return EFI_SUCCESS;
}
