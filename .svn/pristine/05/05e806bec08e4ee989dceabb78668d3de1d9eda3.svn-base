### @file
#  Component information file for ME Resiliency driver.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = MeResiliencyDxe
  FILE_GUID                      = 89908199-9126-4EA2-849B-06A75C245D0B
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = DXE_DRIVER
  UEFI_SPECIFICATION_VERSION     = 2.00
  ENTRY_POINT                    = MeResiliencyDxeEntryPoint

[LibraryClasses]
  DebugLib
  BaseLib
  BaseMemoryLib
  UefiRuntimeServicesTableLib
  UefiBootServicesTableLib
  UefiDriverEntryPoint
  PciSegmentLib
  SeamlessRecoverySupportLib
  HobLib
  MeFwStsLib
  SpiAccessLib

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec

[Sources]
  MeResiliencyDxe.c

[Guids]
  gPlatformBeforeEndOfDxeEventGroupGuid                ## CONSUMES

[Depex]
  gEfiVariableWriteArchProtocolGuid AND
  gEfiVariableArchProtocolGuid