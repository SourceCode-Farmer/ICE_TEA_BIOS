## @file
#  Component description file for Capsule Processor Dxe module.
#
#******************************************************************************
#* Copyright (c) 2012 - 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PdtCapsuleDxe
  FILE_GUID                      = 19342d99-ff4e-4798-a3e0-fa36c961ccfe
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = ChipsetPdtFWUEntryPoint

[sources.common]
  PdtCapsuleDxe.c
  PdtCapsuleDxe.h

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec

[LibraryClasses]
  BaseLib
  UefiLib
  PcdLib
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  UefiDriverEntryPoint
  BaseMemoryLib
  MemoryAllocationLib
  DevicePathLib
  DebugLib
  ChipsetCapsuleLib
  ChipsetSignatureLib
  BvdtLib
  OemGraphicsLib
  VariableLib
  BdsCpLib


[Protocols]
  gEfiFirmwareManagementProtocolGuid

[Guids]
  gEfiSystemResourceTableGuid
  gEfiFmpCapsuleGuid
  gEfiCertX509Guid
  gSecureFlashInfoGuid
  gWindowsUxCapsuleGuid
  gEfiEndOfDxeEventGroupGuid
  gIshUpdVerGuid
  gPdtUpdCountGuid
  gH2OBdsCpDisplayBeforeProtocolGuid

[Pcd]
  gInsydeTokenSpaceGuid.PcdCapsuleMaxResult
  gInsydeTokenSpaceGuid.PcdSecureFlashCertificateFile
  gInsydeTokenSpaceGuid.PcdOsIndicationsSupported
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedPdtVersion
  gChipsetPkgTokenSpaceGuid.PcdWindowsPdtFirmwareCapsuleGuid

[FixedPcd]
  gInsydeTokenSpaceGuid.PcdFirmwareResourceMaximum

[Depex]
  gEfiVariableArchProtocolGuid

[BuildOptions]
  MSFT:*_VS2015_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_VS2015x86_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_VS2017_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_VS2019_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_DEVTLSxVC14_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_DEVTLSxVC16_*_DLINK_FLAGS = /LTCG:OFF