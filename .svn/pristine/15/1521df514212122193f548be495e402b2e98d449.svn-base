/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/=#
#include "Standard.uni"

#string STR_MISC_BIOS_VERSION          #language en-US  "ADLP.05.44.02.0035"

#string STR_MISC_SYSTEM_PRODUCT_NAME   #language en-US  "AlderLake"

//
// The STR_CCB_VERSION is for Insyde internal code sync only.
// DO NOT modify it.
//
#string STR_CCB_VERSION                #language en-US  "05.44.02"

//
// Please following the following format to change this string: MM/DD/YYYY
//
#string STR_MISC_BIOS_RELEASE_DATE     #language en-US  "01/13/2022"

//
// System Firmware Version in the ESRT table
//
#string STR_ESRT_VERSION                   #language en-US  "71024035"
#string STR_ESRT_LOWEST_SUPPORTED_VERSION  #language en-US  "52430000"
//
// System Firmware GUID, each project must have a distinct System Firmware GUID from others
//
// ESRT FIRMWARE GUID of CRBs will all be 12B99262-648C-4365-BAFD-EB869FB7EB47
// WARNING!! OEM project need to use different GUID.
#string STR_ESRT_FIRMWARE_GUID         #language en-US  "12B99262-648C-4365-BAFD-EB869FB7EB47"

#include "Standard.uni"
// String that is used by XXXOemSvcChipsetLib put on here
// Copy this file to XXXBoardpkg and add modified POST string on here
// #string STR_OEM_BADGING_STR_ESC         #language en-US  "Press Esc for boot options"

//
//  This ME Version setting is for H2OABT Tool test. It won't affect build code process.
//
#string STR_MISC_ME_VERSION                 #language en-US  "ME_Consumer_16.0.15.1545v2"
#string STR_DEVIDE_PACKAGE_VERSION          #language en-US  "Alder Lake-P 6+82+8 Cons & Corp (BKC) (Win11 64 bit ) WW44-2021"
//
// BDS Hot Key Description Strings
//
#string STR_OEM_BADGING_STR_ESC           #language en-US  "Press Esc for boot options"
#string STR_OEM_BADGING_STR_ESC_SELECT    #language en-US  "Esc is pressed. Go to boot options."

#string STR_REMOTE_ASSISTANCE_KEY         #language en-US  "F9"
#string STR_REMOTE_ASSISTANCE_BEFORE_STR  #language en-US  "Press F9 Go to ME Remote Assistance."
#string STR_REMOTE_ASSISTANCE_AFTER_STR   #language en-US  "F9 is pressed. Go to ME Remote Assistance."

#string STR_MEBX_KEY                      #language en-US  "F10"
#string STR_MEBX_BEFORE_STR               #language en-US  "Press F10 go to MEBx"
#string STR_MEBX_AFTER_STR                #language en-US  "F10 is pressed. Go to MEBx."
