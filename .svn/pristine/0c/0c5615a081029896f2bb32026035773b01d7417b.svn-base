/** @file
  Header file for overclocking definitions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _OC_DEFINES_H_
#define _OC_DEFINES_H_

//
//  OC Mailbox MSR
//
#define MSR_OC_MAILBOX        0x00000150

//
//  OC Mailbox commands
//
#define MAILBOX_OC_CMD_GET_OC_CAPABILITIES              0x01
#define MAILBOX_OC_CMD_GET_PER_CORE_RATIO_LIMIT         0x02
#define MAILBOX_OC_CMD_GET_DDR_CAPABILITIES             0x03
#define MAILBOX_OC_CMD_GET_VR_TOPOLOGY                  0x04
#define MAILBOX_OC_CMD_GET_FUSED_P0_RATIO_VOLTAGE       0x07
#define MAILBOX_OC_CMD_GET_VOLTAGE_FREQUENCY            0x10
#define MAILBOX_OC_CMD_SET_VOLTAGE_FREQUENCY            0x11
#define MAILBOX_OC_CMD_GET_MISC_GLOBAL_CONFIG           0x14
#define MAILBOX_OC_CMD_SET_MISC_GLOBAL_CONFIG           0x15
#define MAILBOX_OC_CMD_GET_ICCMAX                       0x16
#define MAILBOX_OC_CMD_SET_ICCMAX                       0x17
#define MAILBOX_OC_CMD_GET_MISC_TURBO_CONTROL           0x18
#define MAILBOX_OC_CMD_SET_MISC_TURBO_CONTROL           0x19
#define MAILBOX_OC_CMD_GET_AVX_RATIO_OFFSET             0x1A
#define MAILBOX_OC_CMD_SET_AVX_RATIO_OFFSET             0x1B
#define MAILBOX_OC_CMD_GET_AVX_VOLTAGE_GUARDBAND        0x20
#define MAILBOX_OC_CMD_SET_AVX_VOLTAGE_GUARDBAND        0x21
#define MAILBOX_OC_CMD_GET_OC_TVB_CONFIG                0x24
#define MAILBOX_OC_CMD_SET_OC_TVB_CONFIG                0x25

//
// PCODE Mailbox OC Interface 0x37 command set
//
#define MAILBOX_OC_CMD_OC_INTERFACE                            0x00000037
#define MAILBOX_OC_SUBCMD_READ_OC_MISC_CONFIG                  0x00000000
#define MAILBOX_OC_SUBCMD_WRITE_OC_MISC_CONFIG                 0x00000001
#define MAILBOX_OC_SUBCMD_READ_OC_PERSISTENT_OVERRIDES         0x00000002
#define MAILBOX_OC_SUBCMD_WRITE_OC_PERSISTENT_OVERRIDES        0x00000003
#define MAILBOX_OC_SUBCMD_READ_TJ_MAX_OFFSET                   0x00000004
#define MAILBOX_OC_SUBCMD_WRITE_TJ_MAX_OFFSET                  0x00000005
#define MAILBOX_OC_SUBCMD_READ_PLL_VCC_TRIM_OFFSET             0x00000006
#define MAILBOX_OC_SUBCMD_WRITE_PLL_VCC_TRIM_OFFSET            0x00000007
#define MAILBOX_OC_SUBCMD_READ_PVD_RATIO_THRESHOLD_OVERRIDE    0x00000008
#define MAILBOX_OC_SUBCMD_WRITE_PVD_RATIO_THRESHOLD_OVERRIDE   0x00000009
#define MAILBOX_OC_SUBCMD_READ_DISABLED_IA_CORES_MASK          0x0000000E
#define MAILBOX_OC_SUBCMD_WRITE_DISABLED_IA_CORES_MASK         0x0000000F
#define MAILBOX_OC_SUBCMD_READ_PLL_MAX_BANDING_RATIO_OVERRIDE  0x00000010
#define MAILBOX_OC_SUBCMD_WRITE_PLL_MAX_BANDING_RATIO_OVERRIDE 0x00000011

//
// OC Interface (0x37) Sub-Command definitions
//
#define PLL_MAX_BANDING_RATIO_MINIMUM               1
#define PLL_MAX_BANDING_RATIO_MAXIMUM               120
#define PLL_MAX_BANDING_RATIO_MASK                  0x000000FF

//
//  OC Mailbox completion codes
//
#define MAILBOX_OC_COMPLETION_CODE_SUCCESS              0x00
#define MAILBOX_OC_COMPLETION_CODE_OC_LOCKED            0x01
#define MAILBOX_OC_COMPLETION_CODE_INVALID_DOMAIN       0x02
#define MAILBOX_OC_COMPLETION_CODE_MAX_RATIO_EXCEEDED   0x03
#define MAILBOX_OC_COMPLETION_CODE_MAX_VOLTAGE_EXCEEDED 0x04
#define MAILBOX_OC_COMPLETION_CODE_OC_NOT_SUPPORTED     0x05
#define MAILBOX_OC_COMPLETION_CODE_WRITE_FAILED         0x06
#define MAILBOX_OC_COMPLETION_CODE_READ_FAILED          0x07

//
// Domain ID definitions
//
#define MAILBOX_OC_DOMAIN_ID_DDR                0x00
#define MAILBOX_OC_DOMAIN_ID_IA_CORE            0x00
#define MAILBOX_OC_DOMAIN_ID_GT                 0x01
#define MAILBOX_OC_DOMAIN_ID_RING               0x02
#define MAILBOX_OC_DOMAIN_ID_RESERVED           0x03
#define MAILBOX_OC_DOMAIN_ID_SYSTEM_AGENT       0x04
#define MAILBOX_OC_DOMAIN_ID_L2_ATOM            0x05
#define MAILBOX_OC_DOMAIN_ID_MEMORY_CONTROLLER  0x06

//
// BCLK Source
//
#define SOC_BCLK                            0x00
#define CPU_BCLK                            0x01
#define PCH_BCLK                            0x02

//
// BCLK Select
//
#define SOC_BCLK_SELECT                      0x0
#define CPU_BCLK_SELECT                      0x2

//
// PVD and FLL override definitions
//
#define PVD_OVERRIDES         0
#define FLL_OVERRIDES         1

//
// Bit 10 is the S11.0.10V sign bit
//
#define FIXED_POINT_SIGN_BIT_MASK 0x0400
#define INT16_SIGN_BIT_MASK       0x8000

//
// Voltage Conversion defines
//
#define MILLIVOLTS_PER_VOLT  1000
#define MAX_TARGET_MV      4095
#define MAX_OFFSET_MV      500

#define CONVERT_TO_FIXED_POINT_VOLTS  0
#define CONVERT_TO_BINARY_MILLIVOLT   1

//
// Masks and offsets
//
#define BIT0_MASK             0x1
#define MAX_RATIO_MASK        0x000000FF
#define VOLTAGE_TARGET_MASK   0x000FFF00
#define VOLTAGE_TARGET_OFFSET 8
#define VOLTAGE_MODE_MASK     0x00100000
#define VOLTAGE_MODE_OFFSET   20
#define VOLTAGE_OFFSET_MASK   0xFFE00000
#define VOLTAGE_OFFSET_OFFSET 21
#define QCLK_RATIO_MASK         0x000000FF
#define MC_REF_CLK_MASK         0x00000100
#define MC_REF_CLK_OFFSET       8
#define NUM_DDR_CHANNELS_MASK   0x00000C00
#define NUM_DDR_CHANNELS_OFFSET 10

#define OC_CAPS_MAX_RATIO_MASK              0x000000FF
#define OC_CAPS_RATIO_SUPPORT_MASK          0x00000100
#define OC_CAPS_RATIO_SUPPORT_OFFSET        8
#define OC_CAPS_OVERRIDE_SUPPORT_MASK       0x00000200
#define OC_CAPS_OVERRIDE_SUPPORT_OFFSET     9
#define OC_CAPS_OFFSET_SUPPORT_MASK         0x00000400
#define OC_CAPS_OFFSET_SUPPORT_OFFSET       10
#define AVX2_RATIO_OFFSET_MASK              0x000003E0
#define AVX2_RATIO_OFFSET_OFFSET            5
#define MISC_TURBO_RING_DOWNBIN_MASK        0x00000002
#define MISC_TURBO_RING_DOWNBIN_OFFSET      1
#define MISC_TURBO_TVB_RATIO_MASK           0x00000004
#define MISC_TURBO_TVB_RATIO_OFFSET         2
#define MISC_TURBO_TVB_VOLTAGE_MASK         0x00000008
#define MISC_TURBO_TVB_VOLTAGE_OFFSET       3
#define MISC_TURBO_ETVB_MASK                0x00000010
#define MISC_TURBO_ETVB_OFFSET              4
#define FUSED_P0_RATIO_MASK                 0x000000FF
#define FUSED_P0_VOLTAGE_MASK               0x000FFF00
#define FUSED_P0_VOLTAGE_OFFSET             8
#define MISC_GLOBAL_PER_CORE_VOLTAGE_MASK   0x00000008
#define MISC_GLOBAL_PER_CORE_VOLTAGE_OFFSET 3
#define MISC_GLOBAL_BCLK_ADAPTIVE_OFFSET    2
#define MISC_GLOBAL_CFG_COMMAND_MASK        0x0000000F
#define OC_MISC_CFG_MEM_REALTIME_CHG_MASK   0x00000002

// VR Type
#define SVID_VR           0
#define NON_SVID_VR       1

#endif
