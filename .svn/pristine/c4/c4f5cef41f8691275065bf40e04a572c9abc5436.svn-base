/** @file

;******************************************************************************
;* Copyright (c) 2019 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _NVIDIA_COMMON_ASI_
#define _NVIDIA_COMMON_ASI_

//
// ASL code common define about device
//
#define PCI_SCOPE                             \_SB.PC00
#define IGPU_SCOPE                            PCI_SCOPE.GFX0
#define EC_SCOPE                              PCI_SCOPE.LPCB.H_EC

//
// nVIDIA GPS and Ventura feature usage define
//
#define CPU0_SCOPE                            \_SB.PR00
#define CPU1_SCOPE                            \_SB.PR01
#define CPU2_SCOPE                            \_SB.PR02
#define CPU3_SCOPE                            \_SB.PR03
#define CPU4_SCOPE                            \_SB.PR04
#define CPU5_SCOPE                            \_SB.PR05
#define CPU6_SCOPE                            \_SB.PR06
#define CPU7_SCOPE                            \_SB.PR07
#define CPU8_SCOPE                            \_SB.PR08
#define CPU9_SCOPE                            \_SB.PR09
#define CPU10_SCOPE                           \_SB.PR10
#define CPU11_SCOPE                           \_SB.PR11
#define CPU12_SCOPE                           \_SB.PR12
#define CPU13_SCOPE                           \_SB.PR13
#define CPU14_SCOPE                           \_SB.PR14
#define CPU15_SCOPE                           \_SB.PR15
#define CPU16_SCOPE                           \_SB.PR16
#define CPU17_SCOPE                           \_SB.PR17
#define CPU18_SCOPE                           \_SB.PR18
#define CPU19_SCOPE                           \_SB.PR19
#define CPU20_SCOPE                           \_SB.PR20
#define CPU21_SCOPE                           \_SB.PR21
#define CPU22_SCOPE                           \_SB.PR22
#define CPU23_SCOPE                           \_SB.PR23

//
// nVIDIA device's DID list
//
#define N13E_GS1_DID                          0x1212
#define N13E_GE_DID                           0x0FD4
#define N13E_GTX_DID                          0x11A0

//
// License define
//
#define TEST_LICENSE_VECTOR                   0x25, 0x1B, 0xA2, 0x44, 0x17, 0x62, 0x17, 0x46, 0xBF, 0xB7, 0x41, 0x51, 0x4C, 0xEA, 0xC2, 0x41
#define N13E_GS1_LICENSE_VECTOR               TEST_LICENSE_VECTOR
#define N13E_GE_LICENSE_VECTOR                TEST_LICENSE_VECTOR
#define N13E_GTX_LICENSE_VECTOR               TEST_LICENSE_VECTOR

//
// nVIDIA return status code
//
#define STATUS_SUCCESS                        0x00000000 // Generic Success
#define STATUS_ERROR_UNSPECIFIED              0x80000001 // Generic unspecified error code
#define STATUS_ERROR_UNSUPPORTED              0x80000002 // Sub-Function not supported

//
// nVIDIA Optimus feature related function define
//
#define NVOP_FUNC_SUPPORT                     0x00
#define NVOP_FUNC_DISPLAYSTATUS               0x05
#define NVOP_FUNC_MDTL                        0x06
#define NVOP_FUNC_GETOBJBYTYPE                0x10
#define NVOP_FUNC_OPTIMUSCAPS                 0x1A
#define NVOP_FUNC_OPTIMUSFLAGS                0x1B

//
// nVIDIA GPS feature related function define
//
#define GPS_FUNC_SUPPORT                      0x00
#define GPS_FUNC_GETCALLBACKS                 0x13
#define GPS_FUNC_PCONTROL                     0x1C
#define GPS_FUNC_PSHARESTATUS                 0x20
#define GPS_FUNC_GETPSS                       0x21
#define GPS_FUNC_SETPPC                       0x22
#define GPS_FUNC_GETPPC                       0x23
#define GPS_FUNC_PSHAREPARAMS                 0x2A

//
// nVIDIA GC6 feature related function define
//
#define JT_REVISION_ID                        0x00000100
#define JT_FUNC_SUPPORT                       0x00000000
#define JT_FUNC_CAPS                          0x00000001
#define JT_FUNC_POLICYSELECT                  0x00000002
#define JT_FUNC_POWERCONTROL                  0x00000003
#define JT_FUNC_PLATPOLICY                    0x00000004
#define JT_FUNC_DISPLAYSTATUS                 0x00000005
#define JT_FUNC_MDTL                          0x00000006

#define PID_PSTH                              0x89
#define R_PCH_PCR_PSTH_TRPREG0                0x1E80 // IO Tarp 0 register
#define R_PCH_PCR_PSTH_TRPREG1                0x1E88 // IO Tarp 1 register
#define R_PCH_PCR_PSTH_TRPREG2                0x1E90 // IO Tarp 2 register
#define R_PCH_PCR_PSTH_TRPREG3                0x1E98 // IO Tarp 3 register

#define PID_DMI                               0xEF
#define R_PCH_PCR_DMI_IOT1                    0x2750 // I/O Trap Register 1
#define R_PCH_PCR_DMI_IOT2                    0x2758 // I/O Trap Register 2
#define R_PCH_PCR_DMI_IOT3                    0x2760 // I/O Trap Register 3
#define R_PCH_PCR_DMI_IOT4                    0x2768 // I/O Trap Register 4

#define PID_ICC                               0xDC
#define R_PCH_PCR_ICC_MSKCKRQ                 0x100C // Mask Control CLKREQ

//
// nVIDIA NBCI feature related function define
//
#define NBCI_FUNC_SUPPORT                     0x00
#define NBCI_FUNC_PLATCAPS                    0x01
#define NBCI_FUNC_GETOBJBYTYPE                0x10
#define NBCI_FUNC_GETBACKLIGHT                0x14
#define NBCI_FUNC_GETLICENSE                  0x16

//
// nVIDIA PCF feature related function define
//
#define NVPCF_FUNC_GET_SUPPORTED              0x00
#define NVPCF_FUNC_GET_STATIC_CONFIG_TABLES   0x01
#define NVPCF_FUNC_UPDATE_DYNAMIC_PARAMS      0x02
//[-start-211229-GEORGE0053-modify]//
//[-start-211012-GEORGE0013-modify]//
//[-start-211201-JEPLIUT201-modify]// or S570 
#if defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211201-JEPLIUT201-modify]// 
#define NVPCF_FUNC_GET_WM2_TBAND_TABLES       0x03
#define NVPCF_FUNC_GET_WM2_SL_MAP_TABLES      0x04
#define NVPCF_FUNC_GET_WM2_DYNAMIC_PARAMS     0x05
#define NVPCF_FUNC_CPU_CONTROL                0x06
#define NVPCF_FUNC_GPU_INFO                   0x07
#define NVPCF_FUNC_GET_DC_SYSTEM_POWER_LIMITS_TABLE  0x08
#define NVPCF_FUNC_CPU_TDP_CONTROL                   0x09
#else
#define NVPCF_FUNC_GET_SET_CONTROL            0x03
#endif
//[-end-211012-GEORGE0013-modify]//
//[-end-211229-GEORGE0053-modify]//

//
// nVIDIA Ventura feature related function define
//
#define SPB_VEN_THERMAL_BUDGET                0x88B8
#define SPB_FUNC_SUPPORT                      0x00
#define SPB_FUNC_VENTURASTATUS                0x20
#define SPB_FUNC_GETPSS                       0x21
#define SPB_FUNC_SETPPC                       0x22
#define SPB_FUNC_GETPPC                       0x23
#define SPB_FUNC_CALLBACK                     0x24
#define SPB_FUNC_SYSPARAMS                    0x2A
#define VEN_SENSOR_HEADER_STRUC               0x0
#define VEN_SENSOR_CPU_STRUC                  0x1
#define VEN_SENSOR_GPU_STRUC                  0x2
#define VEN_SENSOR_PARAM_STRUC                0x3
#define VEN_VERSION_HEADER                    0x00010000
#define VEN_NUM_SENSORS                       0x02
#define VEN_VERSION_CPU                       0x00010001
#define VEN_CPU_PARAM_A                       0x3E8
#define VEN_CPU_PARAM_C                       0x258
#define VEN_CPU_PARAM_D                       0x258
#define VEN_CPU_PARAM_E                       0x258
#define VEN_CPU_PARAM_G                       0x2CF
#define VEN_CPU_PARAM_H                       0x311
#define VEN_CPU_PARAM_X                       0x136
#define VEN_CPU_PARAM_Y                       0x118
#define VEN_CPU_PARAM_Z                       0x19A
#define VEN_CPU_PARAM_K                       0x001
#define VEN_CPU_PARAM_M                       0x001
#define VEN_CPU_PARAM_N                       0x001
#define VEN_CPU_PARAM_AL                      0x36B
#define VEN_CPU_PARAM_BE                      0x13C
#define VEN_CPU_PARAM_GA                      0x019
#define VEN_CPU_PARAM_P                       0x000
#define VEN_CPU_PARAM_DEL                     0x001
#define VEN_VERSION_GPU                       0x00010000
#define VEN_GPU_PARAM_W                       0x3E8
#define VEN_GPU_PARAM_P                       0x2EE
#define VEN_GPU_PARAM_Q                       0x2EE
#define VEN_GPU_PARAM_R                       0x2EE
#define VEN_GPU_PARAM_A                       0x001
#define VEN_GPU_PARAM_B                       0x3E8
#define VEN_GPU_PARAM_C                       0x001
#define VEN_GPU_PARAM_D                       0x001
#define VEN_GPU_PARAM_DE                      0x000
#define VENSNS_CPU_SENSOR_TYPE                0x00
#define VENSNS_CPU_I2C_PORT                   0x01
#define VENSNS_CPU_I2C_ADDR                   0x80
#define VENSNS_CPU_INA219_CFG_LOC             0x00
#define VENSNS_CPU_INA219_CFG_VALUE           0x27FF
#define VENSNS_CPU_INA219_CALIB_LOC           0x05
#define VENSNS_CPU_INA219_CALIB_VALUE         0xA000
#define VENSNS_CPU_INA219_POWER_LOC           0x03
#define VENSNS_CPU_PMU_POLLING_FREQ           0x0F
#define VENSNS_CPU_SENSE_RESISTOR             0x04
#define VENSNS_GPU_SENSOR_TYPE                0x00
#define VENSNS_GPU_I2C_PORT                   0x01
#define VENSNS_GPU_I2C_ADDR                   0x8A
#define VENSNS_GPU_INA219_CFG_LOC             0x00
#define VENSNS_GPU_INA219_CFG_VALUE           0x27FF
#define VENSNS_GPU_INA219_CALIB_LOC           0x05
#define VENSNS_GPU_INA219_CALIB_VALUE         0xA000
#define VENSNS_GPU_INA219_POWER_LOC           0x03
#define VENSNS_GPU_PMU_POLLING_FREQ           0x0F
#define VENSNS_GPU_SENSE_RESISTOR             0x04

//
// MXM Function define
//
#define MXM_FUNC_MXSS                         0x00
#define MXM_FUNC_MXDP                         0x05
#define MXM_FUNC_MDTL                         0x06
#define MXM_FUNC_MXMS                         0x10
#define MXM_FUNC_MXMI                         0x18
#define MXM_FUNC_MXCB                         0x19
#endif
