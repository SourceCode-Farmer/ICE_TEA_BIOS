/** @file
;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Definition for SysFwUpdateProgress Variable and Capsule backup filenames
  used for Seamless Recovery support

@copyright
  INTEL CONFIDENTIAL
    Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification
**/

#ifndef __SYSTEM_FIRMWARE_UPDATE_PROGRESS_H__
#define __SYSTEM_FIRMWARE_UPDATE_PROGRESS_H__

#define SYSFW_UPDATE_PROGRESS_GUID \
  { \
    0xf92b8157, 0xc647, 0x44d7, { 0x8d, 0x94, 0x81, 0x7d, 0x18, 0xa2, 0x76, 0xdc } \
  }

#define SYSFW_UPDATE_PROGRESS_VARIABLE_NAME              L"SysFwUpdateProgress"
#define SYSFW_UPDATE_CAPSULE_DIGEST_VARIABLE_NAME        L"CapDigest"
#define CAPSULE_LAST_BACKUP_VARIABLE_NAME                L"CapLastBackup"

#define SYSFW_BIOS_DEFERRED_SVN_VARIABLE_NAME            L"SysFwBiosDeferredSvn"
#define SYSFW_BIOS_LSV_SVN_VARIABLE_NAME                 L"SysFwBiosLsv"
#define SYSFW_MICROCODE_DEFERRED_SVN_VARIABLE_NAME       L"SysFwMicroCodeDeferredSvn"
#define SYSFW_MICROCODE_LSV_SVN_VARIABLE_NAME            L"SysFwMicroCodeLsv"
#define SYSFW_BTGACM_DEFERRED_SVN_VARIABLE_NAME          L"SysFwBtGAcmDeferredSvn"
#define SYSFW_BTGACM_LSV_SVN_VARIABLE_NAME               L"SysFwBtGAcmLsv"
#define SYSFW_EC_LSV_SVN_VARIABLE_NAME                   L"SysFwEcLsv"
#define SYSFW_ME_LSV_SVN_VARIABLE_NAME                   L"SysFwMeLsv"
#define SYSFW_ISHPDT_LSV_SVN_VARIABLE_NAME               L"SysFwIshPdtLsv"

#define SYSFW_UPDATE_CURRENT_OBB_BACKUP_FILE_NAME        L"Obb.bin"
#define SYSFW_UPDATE_NEW_OBB_BACKUP_FILE_NAME            L"ObbN.bin"


#define SYSFW_UPDATE_CAPSULE_BACKUP_FILE_NAME            L"Capsule.cap"
#define SYSFW_UPDATE_WINDOWS_UX_CAPSULE_FILE_NAME        L"UxCap.cap"
#define SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME            L"MeRecov.Cap"
#define SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME_NEW        L"MeRecovN.Cap"

#define SYSFW_UPDATE_RESILIENCY_PENDING_ACTION_EC_RESET  BIT0
//[-start-201030-IB16810136-add]//
#define SYSBIOS_NEW_CAPSULE_DIGEST_VARIABLE_NAMEN        L"BiosDigestN"

#define SYSFW_UPDATE_CURRENT_FVOBB_BACKUP_FILE_NAME      L"FvObb.fv"
#define SYSFW_UPDATE_NEW_FVOBB_BACKUP_FILE_NAME          L"FvObbN.fv"
//[-end-201030-IB16810136-add]//

//[-start-201112-IB16810138-add]//
#define SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME            L"MeRecov.Cap"
#define SYSFW_UPDATE_ME_RECOVERY_CAPSULE_NAME_NEW        L"MeRecovN.Cap"
#define CAPSULE_IMAGE_FULL_PATH                          L"EFI\\UpdateCapsule\\CapsuleUpdateFile1000.bin"
#define UX_IMAGE_FULL_PATH                               L"EFI\\UpdateCapsule\\CapsuleUpdateFile0001.bin"
#define CAPSULE_IMAGE_NAME                               L"CapsuleUpdateFile1000.bin"
#define UX_IMAGE_NAME                                    L"CapsuleUpdateFile0001.bin"
//[-start-210514-IB16810155-add]//
#define SECURE_FLASH_IMAGE_PATH                          L"EFI\\Insyde\\isflash.bin"
//[-end-210514-IB16810155-add]//
#define FW_UPDATEINFO_VARIABLE_NAME                      L"FwUpdateInfo"
#define ME_CAPSULE_DIGEST_VARIABLE_NAME                  L"MeCapDigest"
#define ME_CAPSULE_DIGEST_TEMP_VARIABLE_NAME             L"MeCapDigestTemp"
//[-end-201112-IB16810138-add]//

typedef enum {
  NotStarted              = 0,
  UpdatingMe              = 1,
  UpdatingIshPdt          = 2,
  UpdatingBios            = 3,
  UpdatingEc              = 4,
  UpdatingBtGAcm          = 7,
  UpdatinguCode           = 8,
  UpdatingResiliency      = 9, // Post-BIOS update phase. Indicates BIOS backup requirement.
  UpdatingMeResiliency    = 10,
  UpdatingTypeMax
} SYSTEM_FIRMWARE_COMPONENT;

typedef enum {
  BiosIbbR     = 0,
  BiosIbb,
  BiosObb,
  BiosMax
} BIOS_UPDATE_PROGRESS;

typedef enum {
  EcMainImage        = 0,
  EcRecoveryImage    = 1,
  EcMax
} EC_UPDATE_PROGRESS;

#pragma pack(1)

typedef struct {
  SYSTEM_FIRMWARE_COMPONENT  Component;
  UINT32                     Progress;
} SYSTEM_FIRMWARE_UPDATE_PROGRESS;

#pragma pack()

extern EFI_GUID gSysFwUpdateProgressGuid;

#endif
