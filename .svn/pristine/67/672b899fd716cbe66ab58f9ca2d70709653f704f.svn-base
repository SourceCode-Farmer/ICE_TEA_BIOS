*****************************************************************************
*
*
* Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd. 
* And/or its affiliates. All rights reserved. 
* Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
* Use is subject to license terms.
* 
*****************************************************************************
ECFLASH Utility Release Notification

[Release Date]:
04-27-2015

[Version]:
1.02

[Owner]:
Kenneth Zheng

[Notification]: 
 None.

[Usages]: 
  EcFlash -r|w -b:EcRomSize(Unit: KByte) FileName
  Example:
  Reading EC ROM to file with 128K Bytes:
  EcFlash.efi -r -b:128 EC.ROM
  Written the EC ROM from the file:
  EcFlash.efi -w EC.ROM
  
Fixed:
 None.

Add:
 1. Upgraded the EC ROM in the next BIOS boot life phase when the BIOS don't enable the Secure Flash BIOS function.
 2. The EC Flash driver will identifer automatically the EC ROM signature for the ITE 8586 and 8386 chips. 
    Please reference below codes to select your EC ROM:
 project.fdf:
 gEfiLfcPkgTokenSpaceGuid.PcdFlashEcHeaderBase|gEfiLfcPkgTokenSpaceGuid.PcdFlashEcHeaderSize
 DATA = {
  0x45, 0x43, 0x52, 0x4F, 0x4D, 0x00, 0x00, 0x00, # "ECROM" 
  0x01, 0x01, 0x00, 0x00,                         # Major, Minor,Reserved, Reserverd
  0x00, 0xF8, 0x02, 0x00,                         # EC ROM Size. It must equal to $(FLASH_REGION_EC_SIZE)
  
  #ITE Chip Signature
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, # EC ROM ID Offset
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00  # EC ROM Signature
}

Modified:
 None.

[Notes]:
 None.
   
**************************************************************************
ECFLASH Utility Release Notification

[Release Date]:
05-12-2014

[Version]:
1.01

[Owner]:
Kenneth Zheng

[Notification]: 
 None.

[Usages]: 
  EcFlash -r|w -b:EcRomSize(Unit: KByte) FileName
  Example:
  Reading EC ROM to file with 128K Bytes:
  EcFlash.efi -r -b:128 EC.ROM
  Written the EC ROM from the file:
  EcFlash.efi -w EC.ROM
  
Fixed:
 None.

Add:
 1. Added EC ROM signature to the EC ROM header. Please reference below codes to select your EC ROM:
 project.fdf:
 gEfiLfcPkgTokenSpaceGuid.PcdFlashEcHeaderBase|gEfiLfcPkgTokenSpaceGuid.PcdFlashEcHeaderSize
 DATA = {
  0x45, 0x43, 0x52, 0x4F, 0x4D, 0x00, 0x00, 0x00, # "ECROM" 
  0x01, 0x01, 0x00, 0x00,                         # Major, Minor,Reserved, Reserverd
  0x00, 0xF8, 0x02, 0x00,                         # EC ROM Size. It must equal to $(FLASH_REGION_EC_SIZE)
  
  # 8386 ITE Chip
  0x90, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, # EC ROM ID Offset (Offset = 0x90)
  0x49, 0x54, 0x45, 0x38, 0x33, 0x38, 0x30, 0x2D  # EC ROM Signature ('I','T','E','8','3','8','0','-')
  
  #8586 ITE Chip
  #0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, # EC ROM ID Offset (0ffset = 0x50)
  #0x49, 0x54, 0x45, 0x20, 0x45, 0x43, 0x2D, 0x56  # EC ROM Signature ('I','T','E',' ','E','C','-','V')
}

Modified:
 None.

[Notes]:
 None.
   
**************************************************************************
ECFLASH Utility Release Notification

[Release Date]:
04-22-2014

[Version]:
1.00

[Owner]:
Kenneth Zheng

[Notification]: 
 None.

[Usages]: 
  EcFlash -r|w -b:EcRomSize(Unit: KByte) FileName
  Example:
  Reading EC ROM to file with 128K Bytes:
  EcFlash.efi -r -b:128 EC.ROM
  Written the EC ROM from the file:
  EcFlash.efi -w EC.ROM
  
Fixed:
 None.

Add:
 1. Formal release.

Modified:
 None.

[Notes]:
 None.
   
**************************************************************************
ECFLASH Utility Release Notification

[Release Date]:
04-17-2014

[Version]:
0.06

[Owner]:
Kenneth Zheng

[Notification]: 
 None.

[Usages]: 
  EcFlash -r|w -b:EcRomSize(Unit: KByte) FileName
  Example:
  Reading EC ROM to file with 128K Bytes:
  EcFlash.efi -r -b:128 EC.ROM
  Written the EC ROM from the file:
  EcFlash.efi -w EC.ROM
  
Fixed:
 None.

Add:
 1. Supported EFI_ECFLASH_PROTOCOL DXE and SMM combined driver for upgrading the EC ROM.
 2. Integrated the EcFlash Utility into LFC Package.

Modified:
 None.

[Notes]:
 None.
   
**************************************************************************
ECREAD Utility Release Notification

[Release Date]:
02-21-2014

[Version]:
0.05

[Owner]:
Kenneth Zheng

[Notification]: 
 None.

[Usages]: 
  EcRead -r|w -b:EcRomSize(Unit: KByte) FileName
  Example:
  Reading EC ROM to file with 128K Bytes:
  EcRead.efi -r -b:128 EC.ROM
  Written the EC ROM from the file:
  EcRead.efi -w EC.ROM
  
Fixed:
 None.

Add:
 1. Support to upgrade EC ROM in the BIOS and OS environment even if the 
    BIOS don't enable the secure flash feature.

Modified:
 None.

[Notes]:
 None.
   
**************************************************************************
ECREAD Utility Release Notification

[Release Date]:
02-11-2014

[Version]:
0.04

[Owner]:
Kenneth Zheng

[Notification]: 
 None.

[Usages]: 
  EcRead -r|w -b:EcRomSize(Unit: KByte) FileName
  Example:
  Reading EC ROM to file with 128K Bytes:
  EcRead.efi -r -b:128 EC.ROM
  Written the EC ROM from the file:
  EcRead.efi -w EC.ROM
  
Fixed:
 1. Try to program EC ROM within retry count times. The default retry count time is 3.
 2. Stopping the machine when program EC ROM failed.

Add:
 1. None.

Modified:
 None.

[Notes]:
 None.
   
**************************************************************************
ECREAD Utility Release Notification

[Release Date]:
12-10-2013

[Version]:
0.03

[Owner]:
Kenneth Zheng

[Notification]: 
 None.

[Usages]: 
  EcRead -r|w -b:EcRomSize(Unit: KByte) FileName
  Example:
  Reading EC ROM to file with 128K Bytes:
  EcRead.efi -r -b:128 EC.ROM
  Written the EC ROM from the file:
  EcRead.efi -w EC.ROM
  
Fixed:
 1. Based on 1K byte unit command to upgrade EC ROM.

Add:
 1. Supported the programming bar during read and write the EC ROM.

Modified:
 None.

[Notes]:
 None.
   
**************************************************************************
ECREAD Utility Release Notification

[Release Date]:
11-17-2013

[Version]:
0.02

[Owner]:
Kenneth Zheng

[Notification]: 
 None.

[Usages]: 
  EcRead -r|w -b:BankCount FileName
  Example:
  Reading EC ROM to FileName based on BankCount:
  EcRead.efi -r -b:3 EC.ROM
  Written the EC ROM with the FileName:
  EcRead.efi -w EC.ROM\n
  
Fixed:
 1. Fixed that the tool will upgrade any files onto EC ROM issue.
 2. Changed the command input parameters.
 

Add:
 1. Supported "-b" parameter to use to adjust the bank count with the specfically EC ROM.

Modified:
 None.

[Notes]:
 None.
   
**************************************************************************
ECREAD Utility Release Notification

[Release Date]:
10-26-2013

[Version]:
0.01

[Owner]:
Kenneth Zheng

[Notification]: 
 None.

Fixed:
 None.

Add:
 1. Initialized the first verison tool for ITE EC Chip read and write functions.

Modified:
 None.

[Notes]:
 None.
   
**************************************************************************