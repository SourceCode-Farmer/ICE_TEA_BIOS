## @file
# Component information file for TigerLake Multi-Board Initialization in DXE phase.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2018 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = DxeAdlPSimicsMultiBoardInitLib
  FILE_GUID                      = E7D21F79-1126-4B45-987C-41FF30AC0EC2
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = NULL
  CONSTRUCTOR                    = DxeAdlPSimicsMultiBoardInitLibConstructor

[LibraryClasses]
  BaseLib
  DebugLib
  PcdLib
  BaseMemoryLib
  MemoryAllocationLib
  HobLib
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  DxeUpdatePlatformInfoLib
  BoardConfigLib
  PchPcieRpLib
  PciSegmentLib

[Packages]
  MinPlatformPkg/MinPlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec

[Sources]
  DxeInitLib.c
  DxeMultiBoardInitLib.c

[Protocols]

[Guids]
  gSiMemoryInfoDataGuid                         ## CONSUMES
  gSetupVariableGuid                            ## CONSUMES
  gSaSetupVariableGuid                          ## CONSUMES
  gCpuSetupVariableGuid                         ## CONSUMES
  gPchSetupVariableGuid                         ## CONSUMES
  gMeSetupVariableGuid                          ## CONSUMES
  gSiSetupVariableGuid                          ## CONSUMES

[Pcd]
  # Board Init Table List
  gBoardModuleTokenSpaceGuid.PcdXhciAcpiTableSignature
  gBoardModuleTokenSpaceGuid.PcdPreferredPmProfile

  gBoardModuleTokenSpaceGuid.PcdFingerPrintIrqGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanelIrqGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpadIrqGpio
  gBoardModuleTokenSpaceGuid.PcdHdaI2sCodecIrqGpio
  gBoardModuleTokenSpaceGuid.PcdHdaI2sCodecI2cBusNumber

  gBoardModuleTokenSpaceGuid.PcdWwanModemBaseBandResetGpio
  gBoardModuleTokenSpaceGuid.PcdDiscreteBtModule
  gBoardModuleTokenSpaceGuid.PcdBtRfKillGpio
  gBoardModuleTokenSpaceGuid.PcdBtIrqGpio

  gBoardModuleTokenSpaceGuid.PcdBoardRtd3TableSignature

  gBoardModuleTokenSpaceGuid.PcdEcSmiGpio
  gBoardModuleTokenSpaceGuid.PcdEcLowPowerExitGpio

  gBoardModuleTokenSpaceGuid.PcdSpdAddressOverride

  gBoardModuleTokenSpaceGuid.PcdBatteryPresent
  gBoardModuleTokenSpaceGuid.PcdRealBattery1Control
  gBoardModuleTokenSpaceGuid.PcdRealBattery2Control

  gBoardModuleTokenSpaceGuid.PcdMipiCamSensor
  gBoardModuleTokenSpaceGuid.PcdH8S2113SIO
  gBoardModuleTokenSpaceGuid.PcdNCT6776FCOM
  gBoardModuleTokenSpaceGuid.PcdNCT6776FSIO
  gBoardModuleTokenSpaceGuid.PcdNCT6776FHWMON
  gBoardModuleTokenSpaceGuid.PcdZPoddConfig

  gBoardModuleTokenSpaceGuid.PcdSmcRuntimeSciPin
  gBoardModuleTokenSpaceGuid.PcdConvertableDockSupport

  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF3Support
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF4Support
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF5Support
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF6Support
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF7Support
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF8Support

  gBoardModuleTokenSpaceGuid.PcdVirtualButtonVolumeUpSupport
  gBoardModuleTokenSpaceGuid.PcdVirtualButtonVolumeDownSupport
  gBoardModuleTokenSpaceGuid.PcdVirtualButtonHomeButtonSupport
  gBoardModuleTokenSpaceGuid.PcdVirtualButtonRotationLockSupport

  gBoardModuleTokenSpaceGuid.PcdSlateModeSwitchSupport
  gBoardModuleTokenSpaceGuid.PcdVirtualGpioButtonSupport
  gBoardModuleTokenSpaceGuid.PcdAcDcAutoSwitchSupport
  gBoardModuleTokenSpaceGuid.PcdPmPowerButtonGpioPin
  gBoardModuleTokenSpaceGuid.PcdAcpiEnableAllButtonSupport
  gBoardModuleTokenSpaceGuid.PcdAcpiHidDriverButtonSupport

  gBoardModuleTokenSpaceGuid.PcdSmbiosFabBoardName
  gBoardModuleTokenSpaceGuid.PcdBoardName
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardId                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardBomId
  gBoardModuleTokenSpaceGuid.PcdSkuType

  gBoardModuleTokenSpaceGuid.PcdFuncBoardHookPlatformSetupOverride  ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdGpioTier2WakeEnable                 ## PRODUCES

  gBoardModuleTokenSpaceGuid.PcdPssReadSN
  gBoardModuleTokenSpaceGuid.PcdPssI2cBusNumber
  gBoardModuleTokenSpaceGuid.PcdPssI2cSlaveAddress

  gBoardModuleTokenSpaceGuid.PcdGpioTier2WakeEnable
  gBoardModuleTokenSpaceGuid.PcdDimmPopulationError

  #
  # Retimer Device For Capsule Update Support
  #
  gBoardModuleTokenSpaceGuid.PcdBoardRetimerDataTablePtr            ## PRODUCE
  gBoardModuleTokenSpaceGuid.PcdBoardRetimerDataTableEntryCount     ## PRODUCE