/** @file
  The DGR specific Header file for SMM SPA.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _SMM_SPA_H_
#define _SMM_SPA_H_

#include <Protocol/LoadedImage.h>
#include <Library/DevicePathLib.h>
#include <Library/PrintLib.h>
#include <Protocol/SmmBase2.h>

/**
  SMM Policy Analyzer (SPA) callback function at SMM Ready to Lock.
  Initializes the SPA Context structure with reserved memory for SPA.

  @param[in] Protocol   Points to the protocol's unique identifier
  @param[in] Interface  Points to the interface instance
  @param[in] Handle     The handle on which the interface was installed

  @retval EFI_SUCCESS   Notification handler runs successfully.
**/
EFI_STATUS
EFIAPI
SpaSmmReadyToLockCallback (
  IN CONST EFI_GUID  *Protocol,
  IN VOID            *Interface,
  IN EFI_HANDLE      Handle
  );

//
// Definition help catch error at build time.
//

#pragma pack (push)
#pragma pack (1)

#define C_ASSERT(e)  typedef char ___C_ASSERT___[e?1:-1]

#define NAME_STRING_LENGTH         35
#define SPA_PAGE_TABLE_POOL_SIZE   0x100000     // 1 MB
#define MAX_PROTOCOL_ENTRIES       2            // Define maximum of only 2 protocol entries

typedef struct {
  EFI_GUID FileGuid;
  UINTN    EntryPoint;
  UINTN    ImageBase;
  UINTN    ImageSize;
  UINTN    LoadedImageBase;
  CHAR16   NameString [NAME_STRING_LENGTH + 1];
} IMAGE_STRUCT;

typedef struct _IMAGE_HANDLE_DB {
  UINTN        ImageStructCountMax;
  UINTN        ImageStructCount;
  IMAGE_STRUCT ImageHandles [1];
} IMAGE_HANDLE_DB;

typedef struct _SPA_CTXT {
  UINT8               Valid;
  UINT8               RSVD1;
  UINT16              RSVD2;
  UINTN               ImageHandleDb;                  // Address to Image Handle Db Memory.
  UINTN               UsermodePage;                   // Address to user mode page for logging.
  UINTN               SpaPageTablePool;               // Address of page pool for SPA page fault handler.
  UINT32              SpaPageTablePoolSize;
  UINTN               SpaLogOutputCallback;           // SPA_LOG_OUTPUT_CB function pointers.
} SPA_CTXT;

// Checking structure size with actual number of bytes
C_ASSERT (sizeof (SPA_CTXT) == 40);


#pragma pack (pop)
#endif
