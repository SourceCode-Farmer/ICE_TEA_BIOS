/** @file
  This module configures the memory controller address decoder.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "MrcAddressDecodeConfiguration.h"

/**
  This function configures the zone configuration registers MAD-CR and MAD-ZR-CR.

  @param[in, out] MrcData     - Include all MRC global data.
  @param[in]      Controller  - Controller to configure.

  @retval Nothing.
**/
void
ZoneConfiguration (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               Controller
  )
{
  static const UINT8  ChWidDecode[MAX_MRC_DDR_TYPE] = {2, 1, 0, 0, 0xFF};
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  MrcDebug            *Debug;
  MrcControllerOut    *ControllerOut;
  MrcChannelOut       *ChannelOut;
  INT64               ChLMap;
  INT64               EnhancedChMode;
  INT64               ChSSize;
  INT64               DramType;
  INT64               HashMode;
  INT64               LsbMaskBit;
  INT64               HashMask;
  INT64               McChWidth;
  UINT32              ChannelSizeMin;
  UINT32              ChannelSize[MAX_CHANNEL];
  UINT32              CapacityDivisor;
  UINT32              Channel;
  UINT32              Dimm;
  UINT32              SystemChannel;
  UINT32              SmallChIdx;
  BOOLEAN             FullLpChPop;
  MC0_CHANNEL_EHASH_STRUCT  ChannelEHash;

  Inputs              = &MrcData->Inputs;
  Outputs             = &MrcData->Outputs;
  Debug               = &Outputs->Debug;
  ControllerOut       = &Outputs->Controller[Controller];
  CapacityDivisor     = 512;
  EnhancedChMode      = Outputs->EnhancedChannelMode;
  DramType            = Outputs->DdrType;
  HashMode            = 0;
  SystemChannel       = (EnhancedChMode) ? ctChannel2 : ctChannel1;

  // Add up the amount of memory in each channel.
  FullLpChPop = TRUE;   // Assume all LPDDR channels (4x16) are populated in the controller.
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    ChannelOut = &ControllerOut->Channel[Channel];
    ChannelSize[Channel] = 0;
    if (ChannelOut->Status == CHANNEL_PRESENT) {
      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        if (ChannelOut->Dimm[Dimm].Status == DIMM_PRESENT) {
          ChannelSize[Channel] += ChannelOut->Dimm[Dimm].DimmCapacity;
        }
      }
    } else {
      // If any channel is not present in the controller, set the flag to false for LPDDR.
      FullLpChPop = FALSE;
    }
  }

  if (ChannelSize[SystemChannel] <= ChannelSize[ctChannel0]) {
    ChLMap = 0;
    ChannelSizeMin = ChannelSize[SystemChannel];
    SmallChIdx = SystemChannel;
  } else {
    //  ChannelSize0 < ChannelSize1
    ChLMap = 1;
    ChannelSizeMin = ChannelSize[ctChannel0];
    SmallChIdx = ctChannel0;
  }
  // Need to add the capacity of the second LP Channel in Enhanced Channel Mode
  // Channels must be homogeneous
  if (Outputs->EnhancedChannelMode) {
    SmallChIdx++;
    if (MrcChannelExist (MrcData, Controller, SmallChIdx)) {
      ChannelSizeMin += ChannelSize[SmallChIdx];
    }
  }
  ChSSize = ChannelSizeMin / CapacityDivisor;
    // Interleaved mode
    // Check for any Channel hash support
    if (Inputs->ChHashOverride) {
      // Take values from input parameters
      if (Inputs->ChHashEnable) {
        HashMask   = Inputs->ChHashMask;
        LsbMaskBit = Inputs->ChHashInterleaveBit;
        HashMode   = 1;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel HASH Enabled using override values\n");
      }
    } else { // Auto
      if (DramType == MRC_DDR_TYPE_DDR4) {
        HashMask   = 0;
        LsbMaskBit = 0;
        HashMode   = 0;
      } else if (DramType == MRC_DDR_TYPE_DDR5) {
        HashMask   = 0x30c8;
        LsbMaskBit = 3;
        HashMode   = 1;
      } else {
        HashMask   = 0x30cc;
        LsbMaskBit = 2;
        HashMode   = 1;
      }
      MrcGetSetMc (MrcData, Controller, GsmMccHashMask,   WriteNoCache | PrintValue, &HashMask);
      MrcGetSetMc (MrcData, Controller, GsmMccLsbMaskBit, WriteNoCache | PrintValue, &LsbMaskBit);
    }

  MrcGetSetMc (MrcData, Controller, GsmMccHashMode, WriteNoCache | PrintValue, &HashMode);

  ChannelEHash.Data = 0;
  if (EnhancedChMode && FullLpChPop) {
    // EHASH can only be enabled if all LPDDR channels are populated on the controller.
    // If RI is off, EHASH must be off.
    ChannelEHash.Bits.EHASH_MODE = 1;
    ChannelEHash.Bits.EHASH_LSB_MASK_BIT = 2;
    ChannelEHash.Bits.EHASH_MASK = 0x830;
  }
  MrcWriteCR (MrcData, OFFSET_CALC_CH (MC0_CHANNEL_EHASH_REG, MC1_CHANNEL_EHASH_REG, Controller), ChannelEHash.Data);

  McChWidth   = ChWidDecode[Outputs->DdrType];

  MrcGetSetMc (MrcData, Controller, GsmMccAddrDecodeDdrType,  ForceWriteCached | PrintValue, &DramType);
  MrcGetSetMc (MrcData, Controller, GsmMccChWidth,            WriteToCache     | PrintValue, &McChWidth);
  MrcGetSetMc (MrcData, Controller, GsmMccSChannelSize,       WriteToCache     | PrintValue, &ChSSize);
  MrcGetSetMc (MrcData, Controller, GsmMccLChannelMap,        WriteToCache     | PrintValue, &ChLMap);
  MrcFlushRegisterCachedData (MrcData);
}

/**
  This function checks the possiblity of enabling EBH

  @param[in] MrcData    - Include all MRC global data.
  @param[in] Controller - MC Controller ID.

  @retval True/False    - True: enable EBH, False: disable EBH.
**/

BOOLEAN
MrcEnableExtendedBankHashing (
  IN MrcParameters *const MrcData,
  IN     UINT32           Controller
  )
{
  MrcChannelOut     *ChannelOut;
  MrcControllerOut  *ControllerOut;
  MrcDimmOut        *DimmOut;
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MrcDebug          *Debug;
  UINT32            Channel;
  UINT32            Dimm;
  UINT32            DimmCapacity;
  UINT8             Ranks;
  UINT8             SdramWidth;
  BOOLEAN           FirstDimm;

  Inputs        = &MrcData->Inputs;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  ControllerOut = &Outputs->Controller[Controller];
  FirstDimm     = TRUE;
  DimmCapacity  = 0;
  Ranks         = 0;
  SdramWidth    = 0;

  if (!Inputs->ExtendedBankHashing) {
    return FALSE;
  }

  // Can be only enabled when EIM
  if (!Inputs->EnhancedInterleave) {
    return FALSE;
  }

  // Check symmetrical DRAM configs, if same rank occupancy, rank size and device width
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    ChannelOut = &ControllerOut->Channel[Channel];
    for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
      DimmOut = &ChannelOut->Dimm[Dimm];
      if (DimmOut->Status != DIMM_PRESENT) {
        continue;
      }
      if (FirstDimm) {
        DimmCapacity  = DimmOut->DimmCapacity;
        Ranks         = DimmOut->RankInDimm;
        SdramWidth    = DimmOut->SdramWidth;
        FirstDimm     = FALSE;
      } else {
        if ((DimmCapacity != DimmOut->DimmCapacity) ||
            (Ranks        != DimmOut->RankInDimm)   ||
            (SdramWidth   != DimmOut->SdramWidth)) {
          return FALSE;
        }
      }
    } // Dimm
  } // Channel

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Extended Bank Hashing Enabled on Mc %u\n", Controller);

  return TRUE;
}

/**
  This function configures the zone configuration registers MAD_SLICE.

  @param[in, out] MrcData - Include all MRC global data.

  @retval Nothing.
**/
void
ControllerZoneConfiguration (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MrcDebug          *Debug;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *ChannelOut;
  INT64             MsLMap;
  INT64             StkdMode;
  INT64             StkdModeMs1;
  INT64             StkdModeMsHashBits;
  INT64             MsSSize;
  INT64             LsbMaskBit;
  INT64             HashMask;
  INT64             McOrg;
  INT64             SliceDisable;
  UINT32            ControllerSizeMin;
  UINT32            ControllerSize[MAX_CONTROLLER];
  UINT32            CapacityDivisor;
  UINT8             Channel;
  UINT8             Controller;
  UINT8             Dimm;
  UINT32            Offset;
  UINT32            McHashEnabled;
  UINT32            Data32;
  UINT64            Data64;
  MC0_MAD_MC_HASH_STRUCT  MadMcHash;

  Inputs              = &MrcData->Inputs;
  Outputs             = &MrcData->Outputs;
  Debug               = &Outputs->Debug;
  CapacityDivisor     = 512;
  StkdModeMsHashBits  = 0;
  StkdMode            = 0;
  StkdModeMs1         = 0;
  McHashEnabled       = 0;
  ControllerSize[cCONTROLLER0] = 0;
  ControllerSize[cCONTROLLER1] = 0;
  SliceDisable = 1;

  LsbMaskBit = (Outputs->Lpddr) ? 2 : 3;  // Hash_LSB for MC hashing: 2 for LP4/5, 3 for DDR4/5

  // Add up the amount of memory in each controller.
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    ControllerSize[Controller] = 0;
    if (ControllerOut->Status == CONTROLLER_PRESENT) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        ChannelOut            = &ControllerOut->Channel[Channel];
        if (ChannelOut->Status == CHANNEL_PRESENT) {
          if (ChannelOut->ValidSubChBitMask != 3) {
          }
          for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
            if (ChannelOut->Dimm[Dimm].Status == DIMM_PRESENT) {
              ControllerSize[Controller] += ChannelOut->Dimm[Dimm].DimmCapacity;
            }
          }
        }
      }
    } else {
      // Disable this MC slice in CMF
      MrcGetSetMc (MrcData, Controller, GsmCmiSliceDisable, WriteNoCache | PrintValue, &SliceDisable);
    }
  }

  if (ControllerSize[cCONTROLLER1] <= ControllerSize[cCONTROLLER0]) {
    MsLMap = 0;
    ControllerSizeMin = ControllerSize[cCONTROLLER1];
  } else {
    // MemorySliceSize0 < MemorySliceSize1
    MsLMap = 1;
    ControllerSizeMin = ControllerSize[cCONTROLLER0];
  }
  MsSSize = ControllerSizeMin / CapacityDivisor;

  if ((ControllerSize[cCONTROLLER1] != 0) && (ControllerSize[cCONTROLLER0] != 0)) {
    Outputs->MemoryMapData.ILMem = ControllerSizeMin * 2;
    McHashEnabled = Inputs->MsHashEnable;      // MC hash is disabled if only one MC is populated; otherwise it is controlled by input parameter
    if (ControllerSize[cCONTROLLER0] == ControllerSize[cCONTROLLER1]) {
      McOrg = 4;                      // Dual MC, no L-shape
    } else {
      McOrg = (MsLMap == 0) ? 2 : 3;  // Dual MC, L-shape
    }
  } else {
    Outputs->MemoryMapData.ILMem = 0;
    McOrg = (ControllerSize[cCONTROLLER1] == 0) ? 0 : 1; // Single MC
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Interleaved Memory between controllers %d Megabytes\n", Outputs->MemoryMapData.ILMem);
    // Interleaved mode
    // Check for any Memory Slice hash support
    if (McHashEnabled) {
      HashMask = Inputs->MsHashMask;
      MrcGetSetNoScope (MrcData, GsmCmiHashMask,   WriteNoCache | PrintValue, &HashMask);
      MrcGetSetNoScope (MrcData, GsmCmiLsbMaskBit, WriteNoCache | PrintValue, &LsbMaskBit);
    }

  // @todo_adl program new fields in CMI_MEMORY_SLICE_HASH
  // - L_shape_dec_mask - used in 2LM only ?
  // - L_shape_boundary - used in 2LM only ?

  MrcGetSetNoScope (MrcData, GsmCmiMcOrg,             WriteToCache | PrintValue, &McOrg);
  MrcGetSetNoScope (MrcData, GsmCmiStackedMode,       WriteToCache | PrintValue, &StkdMode);
  MrcGetSetNoScope (MrcData, GsmCmiStackMsMap,        WriteToCache | PrintValue, &StkdModeMs1);
  MrcGetSetNoScope (MrcData, GsmCmiStackedMsHash,     WriteToCache | PrintValue, &StkdModeMsHashBits);
  MrcGetSetNoScope (MrcData, GsmCmiSMadSliceSize,     WriteToCache | PrintValue, &MsSSize);
  MrcGetSetNoScope (MrcData, GsmCmiLMadSliceMap,      WriteToCache | PrintValue, &MsLMap);
  MrcFlushRegisterCachedData (MrcData);

  // Program copies of CMF registers into IOP and DIP blocks
  Data32 = MrcReadCR (MrcData, CMI_MAD_SLICE_REG);
  MrcWriteCR (MrcData, IOP_MAD_SLICE_REG, Data32);
  MrcWriteCR (MrcData, DIP_MAD_SLICE_REG, Data32);

  Data64 = MrcReadCR (MrcData, CMI_MEMORY_SLICE_HASH_REG);
  MrcWriteCR64 (MrcData, IOP_MEMORY_SLICE_HASH_REG, Data64);
  MrcWriteCR64 (MrcData, DIP_MEMORY_SLICE_HASH_REG, Data64);

  // Program copy of CMF_TOPOLOGY_GLOBAL_CFG_0 from CMF_Fast into CMF_Slow0 / CMF_Slow1, and into IOP and DIP blocks
  Data32 = MrcReadCR (MrcData, CMF_TOPOLOGY_GLOBAL_CFG_0_REG);
  MrcWriteCR (MrcData, CMF_0_TOPOLOGY_GLOBAL_CFG_0_REG, Data32);
  MrcWriteCR (MrcData, CMF_1_TOPOLOGY_GLOBAL_CFG_0_REG, Data32);
  MrcWriteCR (MrcData, IOP_TOPOLOGY_GLOBAL_CFG_0_REG, Data32);
  MrcWriteCR (MrcData, DIP_TOPOLOGY_GLOBAL_CFG_0_REG, Data32);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    if (MrcControllerExist (MrcData, Controller)) {
      MadMcHash.Data = 0;
      MadMcHash.Bits.Hash_enabled = McHashEnabled;
      MadMcHash.Bits.Hash_LSB     = (UINT32) LsbMaskBit;
      MadMcHash.Bits.Zone1_start  = ControllerSizeMin / 512;    // Twice the size of Zone_1 (Slice S), in 1 GB units
      MadMcHash.Bits.Stacked_Mode = 0;
      Offset = OFFSET_CALC_CH (MC0_MAD_MC_HASH_REG, MC1_MAD_MC_HASH_REG, Controller);
      MrcWriteCR (MrcData, Offset, MadMcHash.Data);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u_MAD_MC_HASH: 0x%08X\n", Controller, MadMcHash.Data);
    }
  }
}

/**
  This function configures the MAD_DIMM_CH0/1 register.

  @param[in] MrcData    - Include all MRC global data.
  @param[in] Controller - Controller to configure.
  @param[in] Channel    - Channel to configure.

  @retval Nothing.
**/
void
ChannelAddressDecodeConfiguration (
  IN MrcParameters *const MrcData,
  IN const UINT32         Controller,
  IN const UINT32         Channel
  )
{
  const MrcInput      *Inputs;
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcControllerOut    *ControllerOut;
  MrcDimmOut          *Dimm0Out;
  MrcDimmOut          *DimmL;
  MrcDimmOut          *DimmS;
  MrcDimmOut          *LpOrDdrDimm1;
  MrcDdrType          DdrType;
  INT64               GetSetVal;
  INT64               DimmLMap;
  INT64               DLNOR;
  INT64               DSNOR;
  INT64               Ddr5Device8GbDimmL;
  INT64               Ddr5Device8GbDimmS;
  INT64               EnhancedInterleave;
  INT64               EccMode;
  INT64               DimmLSize;
  INT64               DimmSSize;
  UINT32              DimmLDramWidth;
  UINT32              DimmSDramWidth;
  UINT32              Dimm0Capacity;
  UINT32              Dimm1Capacity;
  UINT32              Offset;
  UINT32              CapacityDivisor;
  UINT32              IpChannel;
  UINT32              DimmSize;
  BOOLEAN             Lpddr;
  MC0_CH0_CR_DDR_MR_PARAMS_STRUCT    DdrMrParams;

  Inputs            = &MrcData->Inputs;
  Outputs           = &MrcData->Outputs;
  ControllerOut     = &Outputs->Controller[Controller];
  ChannelOut        = &ControllerOut->Channel[Channel];
  DdrType           = Outputs->DdrType;
  DdrMrParams.Data  = 0;
  CapacityDivisor   = 512;
  DimmLDramWidth    = 0;
  DimmSDramWidth    = 0;
  DimmLSize         = 0;
  DimmSSize         = 0;
  DLNOR             = 0;
  DSNOR             = 0;

  // If we are in EnhancedChannelMode treat LPDDR Ch 1/3 as the second DIMM in the MC Channel.
  // Otherwise use DIMM1 in the same struct.
  LpOrDdrDimm1  = (Outputs->EnhancedChannelMode) ? &ControllerOut->Channel[Channel + 1].Dimm[dDIMM0] : &ChannelOut->Dimm[dDIMM1];
  Dimm0Out      = &ChannelOut->Dimm[dDIMM0];
  DdrType       = MrcData->Outputs.DdrType;
  Lpddr         = Outputs->Lpddr;
  IpChannel     = LP_IP_CH (Lpddr, Channel);

  if (Dimm0Out->Status == DIMM_PRESENT) {
    Dimm0Capacity = Dimm0Out->DimmCapacity;
  } else {
    Dimm0Capacity = 0;
  }

  if (LpOrDdrDimm1->Status == DIMM_PRESENT) {
    Dimm1Capacity = (MAX_DIMMS_IN_CHANNEL > 1) ? LpOrDdrDimm1->DimmCapacity : 0;
  } else {
    Dimm1Capacity = 0;
  }

  // larger dimm will be located to Dimm L and small dimm will be located to dimm S
  if (Dimm1Capacity <= Dimm0Capacity) {
    DimmL = &ChannelOut->Dimm[dDIMM0];
    DimmS = LpOrDdrDimm1;
    // larger DIMM in capacity 0 - DIMM 0 or 1 - DIMM 1
    DimmLMap = 0;
  } else {
    DimmL = LpOrDdrDimm1;
    DimmS = &ChannelOut->Dimm[dDIMM0];
    // larger DIMM in capacity 0 - DIMM 0 or 1 - DIMM 1
    DimmLMap = 1;
  }

  // Limit DIMM_L_SIZE and DIMM_S_SIZE based on DDRSZ.
  DimmL->DimmCapacity = MIN (DimmL->DimmCapacity, Outputs->MrcTotalChannelLimit);
  DimmS->DimmCapacity = MIN (DimmS->DimmCapacity, Outputs->MrcTotalChannelLimit - DimmL->DimmCapacity);

  // ddr5_device_8Gb:   0 - DDR5 capacity is more than 8Gb
  //                    1 - DDR5 capacity is 8Gb (default value)
  Ddr5Device8GbDimmL = ((SdramCapacityTable[DimmL->DensityIndex] * 8) > 8192) ? 0 : 1;
  Ddr5Device8GbDimmS = ((SdramCapacityTable[DimmS->DensityIndex] * 8) > 8192) ? 0 : 1;
  if (DimmS->DimmCapacity == 0) {
    Ddr5Device8GbDimmS = Ddr5Device8GbDimmL;
  }

  // Dimm L
  if ((0 < DimmL->RankInDimm) && (DimmL->Status == DIMM_PRESENT)) {
    DimmLSize = DimmL->DimmCapacity / CapacityDivisor;
    if (Lpddr) {
      // For non-power of two device sizes we need to program DIMM_L/S_SIZE to the nearest power of two.
      DimmSize = (UINT32) DimmLSize;
      if (MrcCountBitsEqOne (DimmSize) == 2) { // 3, 6, 12, 24 etc.
        DimmLSize = (DimmSize * 4) / 3;        // Round up to the nearest power of two
      }
    }
    // RankInDimm must be 1 or 2, we test the case that the value is 0
    DLNOR = DimmL->RankInDimm - 1;
    // SDRAM width
    switch (DimmL->SdramWidth) {
      case 8:
        DimmLDramWidth = 0;
        break;

      case 16:
        DimmLDramWidth = 1;
        break;

      case 32:
      case 64:
        DimmLDramWidth = 2;
        break;
    }
  }
  // Dimm S
  if ((MAX_DIMMS_IN_CHANNEL > 1) && (0 < DimmS->RankInDimm) && (DimmS->Status == DIMM_PRESENT)) {
    DimmSSize = DimmS->DimmCapacity / CapacityDivisor;
    if (Lpddr) {
      // For non-power of two device sizes we need to program DIMM_L/S_SIZE to the nearest power of two.
      DimmSize = (UINT32) DimmSSize;
      if (MrcCountBitsEqOne (DimmSize) == 2) { // 3, 6, 12, 24 etc.
        DimmSSize = (DimmSize * 4) / 3;        // Round up to the nearest power of two
      }
    }
    // RankInDimm must be 1 or 2, we test the case that this value is 0.
    DSNOR = DimmS->RankInDimm - 1;
    // SDRAM width
    switch (DimmS->SdramWidth) {
      case 8:
        DimmSDramWidth = 0;
        break;

      case 16:
        DimmSDramWidth = 1;
        break;

      case 32:
      case 64:
        DimmSDramWidth = 2;
        break;
    }
  }

  EnhancedInterleave = (Inputs->EnhancedInterleave) ? 1 : 0;

  if (Outputs->EccSupport) {
    // Set to '01' - ECC is active in IO, ECC logic is not active.
    EccMode = emEccIoActive;
  } else {
    EccMode = 0;
  }

  // DIMM Config
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccLDimmSize,      WriteToCache | PrintValue, &DimmLSize);
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccLDimmRankCnt,   WriteToCache | PrintValue, &DLNOR);
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccSDimmSize,      WriteToCache | PrintValue, &DimmSSize);
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccSDimmRankCnt,   WriteToCache | PrintValue, &DSNOR);
  GetSetVal = DimmLDramWidth;
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccLDimmDramWidth, WriteToCache | PrintValue, &GetSetVal);
  GetSetVal = DimmSDramWidth;
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccSDimmDramWidth, WriteToCache | PrintValue, &GetSetVal);

  // EBH can only be enabled in fully symmetrical config
  GetSetVal = MrcEnableExtendedBankHashing (MrcData, Controller) ? 3 : 0;
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccExtendedBankHashing, WriteToCache | PrintValue, &GetSetVal);

  if (DdrType == MRC_DDR_TYPE_DDR5) {
    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccDdr5Device8GbDimmL, WriteToCache | PrintValue, &Ddr5Device8GbDimmL);
    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccDdr5Device8GbDimmS, WriteToCache | PrintValue, &Ddr5Device8GbDimmS);
  }

  //IntraChannel Config
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccLDimmMap,           WriteToCache | PrintValue, &DimmLMap);
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccEnhancedInterleave, WriteToCache | PrintValue, &EnhancedInterleave);
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccEccMode,            WriteToCache | PrintValue, &EccMode);

  MrcFlushRegisterCachedData (MrcData);

  DdrMrParams.Bits.MR4_PERIOD = 0x200D;

  if (DdrType == MRC_DDR_TYPE_DDR4) {
    DdrMrParams.Bits.DDR4_TS_readout_en = 1;
  }
  if (DimmLMap == 0) {
    DdrMrParams.Bits.Rank_0_width = DimmLDramWidth;
    DdrMrParams.Bits.Rank_1_width = DimmLDramWidth;
    DdrMrParams.Bits.Rank_2_width = DimmSDramWidth;
    DdrMrParams.Bits.Rank_3_width = DimmSDramWidth;
  } else {
    DdrMrParams.Bits.Rank_0_width = DimmSDramWidth;
    DdrMrParams.Bits.Rank_1_width = DimmSDramWidth;
    DdrMrParams.Bits.Rank_2_width = DimmLDramWidth;
    DdrMrParams.Bits.Rank_3_width = DimmLDramWidth;
  }

  Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR_MR_PARAMS_REG, MC1_CH0_CR_DDR_MR_PARAMS_REG, Controller, MC0_CH1_CR_DDR_MR_PARAMS_REG, IpChannel);
  MrcWriteCR (MrcData, Offset, DdrMrParams.Data);

}

/**
  Initialize the MARS feature (Memory AwaRe Scrubber)

  @param[in] MrcData - Include all MRC global data.

  @retval none
**/
void
MrcMarsConfig (
  IN MrcParameters *const MrcData
  )
{
  MrcInput                       *Inputs;
  MrcOutput                      *Outputs;
  MrcDebug                       *Debug;
  UINT32                         Controller;
  UINT32                         Index;
  UINT32                         Data32;
  UINT32                         McOffset;
  UINT32                         IdpOffset;
  UINT32                         IdpMcOffset;
  CCF_IDP0_CR_MARS_ENABLE_STRUCT MarsEnable;
  CCF_IDP0_CR_SLICE_HASH_STRUCT  SliceHash;
  CMI_MAD_SLICE_STRUCT           MadSlice;
  static const struct {
    UINT16 McReg;
    UINT16 IdpReg;
  } RegTable[] = {
    { MC0_MAD_INTER_CHANNEL_REG, CCF_IDP0_CR_MAD_INTER_CHANNEL_REG },
    { MC0_MAD_INTRA_CH0_REG,     CCF_IDP0_CR_MAD_INTRA_CH0_REG     },
    { MC0_MAD_INTRA_CH1_REG,     CCF_IDP0_CR_MAD_INTRA_CH1_REG     },
    { MC0_MAD_DIMM_CH0_REG,      CCF_IDP0_CR_MAD_DIMM_CH0_REG      },
    { MC0_MAD_DIMM_CH1_REG,      CCF_IDP0_CR_MAD_DIMM_CH1_REG      },
    { MC0_CHANNEL_HASH_REG,      CCF_IDP0_CR_CHANNEL_HASH_REG      },
    { MC0_CHANNEL_EHASH_REG,     CCF_IDP0_CR_CHANNEL_EHASH_REG     },
  };

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  if (Inputs->Mars == 0) {
    return;
  }

  // Enable MARS only if all of the below is true:
  // - DIMM (sub-channel) sizes are equal, or only one exists
  // - Channel sizes are equal, or only one exists
  // - MC/Slice sizes are equal, or only one exists

  if (!MrcIsSymmetric(MrcData)) {
    return;
  }

  McOffset    = MC1_MAD_INTER_CHANNEL_REG - MC0_MAD_INTER_CHANNEL_REG;
  IdpOffset   = CCF_IDP1_CR_MAD_INTER_CHANNEL_REG - CCF_IDP0_CR_MAD_INTER_CHANNEL_REG;
  IdpMcOffset = CCF_IDP0_CR_MAD_INTER_CHANNEL_MC1_REG - CCF_IDP0_CR_MAD_INTER_CHANNEL_REG;

  // Copy from MC address decoding registers to CCF IDP register.
  // They have exactly the same format, so can copy as is.
  SliceHash.Data = MrcReadCR (MrcData, CMI_MEMORY_SLICE_HASH_REG);
  SliceHash.Bits.HASH_MODE = 1;                                     // CMI_MEMORY_SLICE_HASH doesn't have HASH_MODE bit, so need to set it here
  MrcWriteCR (MrcData, CCF_IDP0_CR_SLICE_HASH_REG, SliceHash.Data);
  MrcWriteCR (MrcData, CCF_IDP1_CR_SLICE_HASH_REG, SliceHash.Data);

  MadSlice.Data = MrcReadCR (MrcData, CMI_MAD_SLICE_REG);
  if (Inputs->A0 && (MadSlice.Bits.MS_S_SIZE > 0xFF)) {
    return;
  }
  MrcWriteCR (MrcData, CCF_IDP0_CR_MAD_INTER_SLICE_REG, MadSlice.Data);
  MrcWriteCR (MrcData, CCF_IDP1_CR_MAD_INTER_SLICE_REG, MadSlice.Data);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++)  {
    if (MrcControllerExist (MrcData, Controller)) {
      for (Index = 0; Index < ARRAY_COUNT (RegTable); Index++) {
        Data32 = MrcReadCR (MrcData, RegTable[Index].McReg + McOffset * Controller);
        // Copy to both IDP0 and IDP1
        MrcWriteCR (MrcData, RegTable[Index].IdpReg + IdpMcOffset * Controller,             Data32);
        MrcWriteCR (MrcData, RegTable[Index].IdpReg + IdpMcOffset * Controller + IdpOffset, Data32);
      }
    }
  }

  MarsEnable.Data = 0;
  MarsEnable.Bits.MARS_Enable = 1;
  MrcWriteCR (MrcData, CCF_IDP0_CR_MARS_ENABLE_REG, MarsEnable.Data);
  MrcWriteCR (MrcData, CCF_IDP1_CR_MARS_ENABLE_REG, MarsEnable.Data);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MARS Enabled\n");
}

/**
  This function is the main address decoding configuration function.

  @param[in] MrcData - Include all MRC global data.

  @retval Nothing.
**/
void
MrcAdConfiguration (
  IN MrcParameters *const MrcData
  )
{
  UINT32      Controller;
  UINT32      Channel;
  BOOLEAN     Lpddr;

  Lpddr = MrcData->Outputs.Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (!MrcControllerExist (MrcData, Controller)) {
      continue;
    }
    ZoneConfiguration (MrcData, Controller);
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        // For LPDDR4/5, only program register on even channels.
        continue;
      }
      ChannelAddressDecodeConfiguration (MrcData, Controller, Channel);
    } // for Channel
  } // for Controller

  // Initialize the MARS feature (Memory AwaRe Scrubber)
  // This has to be done once all the MC Address Decode register are programmed
  MrcMarsConfig (MrcData);

  return;
}
