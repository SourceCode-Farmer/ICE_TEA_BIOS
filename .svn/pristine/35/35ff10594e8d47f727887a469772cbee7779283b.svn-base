/** @file
  Header file for PeiTsnFvFile

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PEI_TSN_FV_LIB_H_
#define _PEI_TSN_FV_LIB_H_

typedef struct {
  UINT32  Type;
  UINT32  Version;
  UINT64  DataOffset;
  UINT32  DataSize;
  UINT64  SignatureOffset;
  UINT32  SignatureType;
} TSN_SUB_REGION_HEADER;

typedef struct {
  UINT32 Seg     : 8;
  UINT32 Bus     : 8;
  UINT32 Dev     : 5;
  UINT32 Func    : 3;
  UINT32 PcieCap : 8;
} SBDF;

typedef struct {
  SBDF    Bdf;
  UINT32  PortLength;
  UINT32  MacAddressHigh;
  UINT32  MacAddressLow;
} PCH_TSN_SUB_REGION_PORT;

typedef struct {
  PCH_TSN_SUB_REGION_PORT  Port;
  UINT32 Signature;
} TSN_SUB_REGION;

/**
  Install Base and Size Info Ppi for Siip Firmware Volume.

  @retval     EFI_SUCCESS  Always returns success.

**/
EFI_STATUS
EFIAPI
InstallTsnFv (
  VOID
  );

/**
  Install Base and Size Info Ppi for TSN Firmware Volume.

  @retval     EFI_SUCCESS  Always returns success.

**/
EFI_STATUS
EFIAPI
LoadFlashTsnFv (
  VOID
  );

/*
  Reads TSN Sub region.

  Reads TSN sub region data from FV. Returns pointer to data.

  @param[out]  SubRegion Pointer to sub region data.

  @retval     EFI_SUCCESS  Function has completed successfully.
  @retval     Others       TSN region was not found
*/
EFI_STATUS
GetTsnSubRegion (
  OUT TSN_SUB_REGION** SubRegion
  );

#endif
