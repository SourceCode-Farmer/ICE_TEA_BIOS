/** @file
  Initializes both PCH Host and Device USB Controllers.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "PchUsb.h"
#include <Ppi/Spi.h>
#include <Library/PeiHsioLib.h>
#include <Library/UsbHostControllerInitLib.h>
#include <Library/UsbDeviceControllerInitLib.h>
#include <Library/PostCodeLib.h>
#include <Library/UsbLib.h>
#include <Library/Usb2PhyLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchInfoLib.h>
#include <Library/FiaSocLib.h>
#include <Library/PmcSocLib.h>
#include <Library/PsfLib.h>
#include <Library/PchFiaLib.h>
#include <Library/DciPrivateLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/P2SbSocLib.h>
#include <Library/PcdLib.h>
#include <Register/PchRegs.h>
#include <Register/UsbRegs.h>
#include <UsbController.h>
#include "PchInitPei.h"

/**
  Disables USB Host Controller

  @param[in]  UsbHandle         Pointer to USB_HANDLE instance
**/
STATIC
VOID
UsbHostControllerDisable (
  IN    USB_HANDLE        *UsbHandle
  )
{
  //
  // Disable xHCI function.
  // Default value=0, When set, transactions targeting this PCI function
  // will not be positively decoded.
  //
  PsfDisableXhciDevice ();
  //
  // Disable xHCI VTIO Phantom device
  //
  PsfDisableXhciVtioDevice ();

  //
  // Disable xHCI in PMC
  //
  PmcDisableXhci ();
}

/**
  Disables USB Device Controller

  @param[in]  UsbHandle         Pointer to USB_HANDLE instance
**/
STATIC
VOID
UsbDeviceControllerDisable (
  IN    USB_HANDLE        *UsbHandle
  )
{
  //
  // Disable xDCI function.
  //
  PsfDisableXdciDevice ();

  //
  // Disable XDCI Controller in PMC
  //
  PmcDisableXdci ();
}

/**
  Configures HHPIO pins for USB OverCurrent detection

  @param[in]  UsbHandle         Pointer to USB_HANDLE instance
  @param[in]  OvercurrentPin    Index of OverCurrent Pin to be enabled
**/
STATIC
VOID
UsbEnableOvercurrentPin (
  IN    USB_HANDLE        *UsbHandle,
  IN    UINT8             OverCurrentPin
  )
{
  GpioEnableUsbOverCurrent (OverCurrentPin);
}

/**
  Checks in FIA is given lane is USB owned

  @param[in]  UsbHandle         Pointer to USB_HANDLE instance
  @param[in]  LaneIndex         Index of lane to check
**/
BOOLEAN
IsFiaLaneOwnedByUsb (
  IN    USB_HANDLE        *UsbHandle,
  IN    UINT8             LaneNumber
  )
{
  return PchFiaIsUsb3PortConnected (LaneNumber);
}

/**
  Creates USB Host controller handle to pass to xHCI library with all necessary
  data for proper controller initialization

  @param[out]   UsbController       Instance of UsbController structure that
                                    will describe PCH USB Host Controller
**/
STATIC
VOID
CreateUsbHostController (
  OUT   USB_CONTROLLER    *UsbController
  )
{
  ZeroMem (UsbController, sizeof (USB_CONTROLLER));

  //
  // Controller location details
  //
  UsbController->Segment        = DEFAULT_PCI_SEGMENT_NUMBER_PCH;
  UsbController->Bus            = DEFAULT_PCI_BUS_NUMBER_PCH;
  UsbController->Device         = PchXhciDevNumber ();
  UsbController->Function       = PchXhciFuncNumber ();
  UsbController->PciCfgBaseAddr = PchXhciPciCfgBase ();

  //
  // Controller capabilities
  //
  UsbController->Usb2PortCount  = GetPchXhciMaxUsb2PortNum ();
  UsbController->UsbrPortCount  = GetPchXhciMaxUsb2PortNum () - GetPchUsb2MaxPhysicalPortNum ();
  UsbController->Usb3LanesCount = GetPchXhciMaxUsb3PortNum ();
}

/**
  Creates USB Device controller handle to pass to xDCI library with all necessary
  data for proper controller initialization

  @param[out]   UsbController       Instance of UsbController structure that
                                    will describe PCH USB Device Controller
**/
STATIC
VOID
CreateUsbDeviceController (
  OUT   USB_CONTROLLER    *UsbController
  )
{
  ZeroMem (UsbController, sizeof (USB_CONTROLLER));

  UsbController->Segment        = DEFAULT_PCI_SEGMENT_NUMBER_PCH;
  UsbController->Bus            = DEFAULT_PCI_BUS_NUMBER_PCH;
  UsbController->Device         = PchXdciDevNumber ();
  UsbController->Function       = PchXdciFuncNumber ();
  UsbController->PciCfgBaseAddr = PchXdciPciCfgBase ();
}

/**
  Initializes USB controllers handle for both Host and Device controllers
  along with required callbacks for cross IP dependencies

  @param[in]  SiPolicy            The Silicon Policy PPI instance
  @param[in]  Usb2SbAccess        P2SB Register Access to USB2
  @param[in]  DciSbAccess         P2SB Register Access to DCI
  @param[in]  XhciSbAccess        P2SB Register Access to XHCI
  @param[out] P2SbController      P2SB controller pointer
  @param[out] UsbHandle           USB_HANDLE structure
  @param[out] UsbPrivateConfig    USB_PRIVATE_CONFIG structure
  @param[out] UsbHostController   USB_CONTROLLER structure for Host Controller
  @param[out] UsbDeviceController USB_CONTROLLER structure for Device Controller
  @param[out] UsbCallback         USB_CALLBACK structure
**/
STATIC
VOID
CreateUsbControllerHandle (
  IN      SI_POLICY_PPI                  *SiPolicy,
  IN      P2SB_SIDEBAND_REGISTER_ACCESS  *Usb2SbAccess,
  IN      P2SB_SIDEBAND_REGISTER_ACCESS  *DciSbAccess,
  IN      P2SB_SIDEBAND_REGISTER_ACCESS  *XhciSbAccess,
  OUT     P2SB_CONTROLLER                *P2SbController,
  IN OUT  USB_HANDLE                     *UsbHandle,
  IN OUT  USB_PRIVATE_CONFIG             *UsbPrivateConfig,
  IN OUT  USB_CONTROLLER                 *UsbHostController,
  IN OUT  USB_CONTROLLER                 *UsbDeviceController,
  IN OUT  USB_CALLBACK                   *UsbCallback
  )
{
  EFI_STATUS        Status;
  USB_CONFIG        *UsbConfig;
  USB2_PHY_CONFIG   *Usb2PhyConfig;

  //
  // Create private config values
  //
  UsbPrivateConfig->IsPortResetMessagingSupported = TRUE;
  UsbPrivateConfig->IsSimulationEnvironment = (IsSimicsEnvironment () || IsHfpgaEnvironment ());
  UsbPrivateConfig->EnableHcResetIsolationFlow = IsUsb2DbcDebugEnabled ();

  UsbPrivateConfig->RxStandbySupport = TRUE;
  UsbPrivateConfig->LaneDeassertInPs3Support = TRUE;

  UsbPrivateConfig->Usb2PhyRefFreq = USB_FREQ_19_2;

  if (IsPchS ()) {
    UsbPrivateConfig->IpVersion = V19_0;

    UsbPrivateConfig->RxStandbySupport = FALSE;
    UsbPrivateConfig->LaneDeassertInPs3Support = FALSE;

    UsbPrivateConfig->CpxProgramming = TRUE;
    //
    // CP13_DEEMPH
    //
    UsbPrivateConfig->Cp13Deemph.Field.Czero = 0x16;
    UsbPrivateConfig->Cp13Deemph.Field.CminusOnePrecursor = 0x02;
    //
    // CP14_DEEMPH
    //
    UsbPrivateConfig->Cp14Deemph.Field.CplusOnePrecursor = 0x03;
    UsbPrivateConfig->Cp14Deemph.Field.Czero = 0x15;
    //
    // CP15_DEEMPH
    //
    UsbPrivateConfig->Cp15Deemph.Field.CplusOnePrecursor = 0x03;
    UsbPrivateConfig->Cp15Deemph.Field.Czero = 0x13;
    UsbPrivateConfig->Cp15Deemph.Field.CminusOnePrecursor = 0x02;
    //
    // CP16_DEEMPH
    //
    UsbPrivateConfig->Cp16Deemph.Field.Czero = 0x18;

    UsbPrivateConfig->LtrHigh = 0x01200120;
    UsbPrivateConfig->LtrMid  = 0x00800080;
    UsbPrivateConfig->LtrLow  = 0x00260026;
  } else {
    UsbPrivateConfig->IpVersion = V18_0;

    UsbPrivateConfig->LtrHigh = 0x00050002;
    UsbPrivateConfig->LtrMid  = 0x00050002;
    UsbPrivateConfig->LtrLow  = 0x00050002;
  }

  UsbPrivateConfig->Location = Standalone;
  switch (PchStepping ()) {
    case PCH_B1:
      UsbPrivateConfig->Stepping = B1;
      break;
    case PCH_B0:
      UsbPrivateConfig->Stepping = B0;
      break;
    case PCH_A1:
      UsbPrivateConfig->Stepping = A1;
      break;
    case PCH_A0:
    default:
      UsbPrivateConfig->Stepping = A0;
      break;
  }
  UsbHandle->PrivateConfig = UsbPrivateConfig;

  //
  // Populate callbacks pointers
  //
  UsbCallback->UsbEnableOvercurrentPin = UsbEnableOvercurrentPin;
  UsbCallback->UsbHostControllerDisable = UsbHostControllerDisable;
  UsbCallback->UsbDeviceControllerDisable = UsbDeviceControllerDisable;
  UsbCallback->UsbIsLaneOwned = IsFiaLaneOwnedByUsb;
  UsbHandle->Callback = UsbCallback;

  //
  // Populate both host and device controllers data
  //
  CreateUsbHostController (UsbHostController);
  CreateUsbDeviceController (UsbDeviceController);
  UsbHandle->HostController = UsbHostController;
  UsbHandle->DeviceController = UsbDeviceController;

  UsbHandle->Mmio = PcdGet32 (PcdSiliconInitTempMemBaseAddr);

  //
  // Set config pointer
  //
  Status = GetConfigBlock ((VOID *) SiPolicy, &gUsbConfigGuid, (VOID *) &UsbConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a - cannot get USB config block.\n", __FUNCTION__));
    UsbConfig = NULL;
  }
  UsbHandle->UsbConfig = UsbConfig;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gUsb2PhyConfigGuid, (VOID *) &Usb2PhyConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a - cannot get USB2 PHY config block.\n", __FUNCTION__));
  }
  UsbHandle->Usb2PhyConfig = Usb2PhyConfig;

  Status = GetP2SbController (P2SbController);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  BuildP2SbSidebandAccess (
      P2SbController,
      PID_USB2,
      0,
      P2SbPrivateConfig,
      P2SbMsgAccess,
      TRUE,
      Usb2SbAccess
      );
  UsbHandle->Usb2SbAccessPrivateControl = (REGISTER_ACCESS *) Usb2SbAccess;

  BuildP2SbSidebandAccess (
      P2SbController,
      PID_DCI,
      0,
      P2SbMemory,
      P2SbMmioAccess,
      FALSE,
      DciSbAccess
      );
  UsbHandle->DciSbAccessMmio = (REGISTER_ACCESS *) DciSbAccess;

  BuildP2SbSidebandAccess (
      P2SbController,
      PID_XHCI,
      0,
      P2SbMemory,
      P2SbMmioAccess,
      FALSE,
      XhciSbAccess
      );
  UsbHandle->XhciSbAccessMmio = (REGISTER_ACCESS *) XhciSbAccess;
}

/**
  Clears D3HE bit in xHCI PCE register

  @param[in] UsbHandle           USB_HANDLE structure
**/
STATIC
VOID
ClearXhciD3he (
  USB_HANDLE    *UsbHandle
  )
{
  PciSegmentAnd16 (UsbHandle->HostController->PciCfgBaseAddr + R_XHCI_CFG_PCE, (UINT16)~B_XHCI_CFG_PCE_D3HE);
}

/**
  Sets D3HE bit in xHCI PCE register

  @param[in] UsbHandle           USB_HANDLE structure
**/
STATIC
VOID
SetXhciD3he (
  USB_HANDLE    *UsbHandle
  )
{
  PciSegmentOr16 (UsbHandle->HostController->PciCfgBaseAddr + R_XHCI_CFG_PCE, (UINT16) B_XHCI_CFG_PCE_D3HE);
}

/*
  Performs PCH USB Controllers initialization

  @param[in] SiPolicy     The Silicon Policy PPI instance
*/
VOID
PchUsbConfigure (
  IN    SI_POLICY_PPI     *SiPolicy
  )
{
  UINT8                          InterruptPin;
  UINT8                          Irq;
  UINT32                         Data32Or;
  UINT32                         Data32And;
  USB_CONTROLLER                 HostController;
  USB_CONTROLLER                 DeviceController;
  USB_PRIVATE_CONFIG             PrivateConfig;
  USB_CALLBACK                   UsbCallback;
  USB_HANDLE                     UsbHandle;
  HSIO_HANDLE                    HsioHandle;
  HSIO_PRIVATE_CONFIG            HsioPrivate;
  HSIO_LANE                      HsioLane;
  P2SB_CONTROLLER                P2SbController;
  P2SB_SIDEBAND_REGISTER_ACCESS  SbAccessHsio;
  P2SB_SIDEBAND_REGISTER_ACCESS  SbAccessUsb2;
  P2SB_SIDEBAND_REGISTER_ACCESS  SbAccessDci;
  P2SB_SIDEBAND_REGISTER_ACCESS  SbAccessXhci;
  UINT8                          LaneIndex;

  //
  // Clear structures data
  //
  ZeroMem (&HostController, sizeof (USB_CONTROLLER));
  ZeroMem (&DeviceController, sizeof (USB_CONTROLLER));
  ZeroMem (&PrivateConfig, sizeof (USB_PRIVATE_CONFIG));
  ZeroMem (&UsbCallback, sizeof (USB_CALLBACK));
  ZeroMem (&UsbHandle, sizeof (USB_HANDLE));

  //
  // Initialize PCH USB Handle
  //
  CreateUsbControllerHandle (
    SiPolicy, &SbAccessUsb2, &SbAccessDci, &SbAccessXhci, &P2SbController, &UsbHandle, &PrivateConfig, &HostController, &DeviceController, &UsbCallback
    );
  if (UsbHandle.UsbConfig == NULL) {
    DEBUG ((DEBUG_ERROR, "%a - USB config block not available. Skipping initialization.\n", __FUNCTION__));
    return;
  }

  if (IsPchChipsetInitSyncSupported ()) {
    //
    // Tune the USB 2.0 high-speed signals quality.
    //
    PostCode (0xB04);
    if (UsbHandle.Usb2PhyConfig == NULL) {
      DEBUG ((DEBUG_ERROR, "%a - cannot get USB2 PHY config block. Skipping initialization.\n", __FUNCTION__));
    } else {
      if (IsPchLp ()) {
        PmcUsb2CorePhyPowerGatingDisable (PmcGetPwrmBase ());
      }
      Usb2Phy4p4Programming (&UsbHandle);
    }

    //
    // Configure USB3 ModPHY turning.
    //
    PostCode (0xB05);
    for (LaneIndex = 0; LaneIndex < HostController.Usb3LanesCount; LaneIndex++) {
      if (IsFiaLaneOwnedByUsb (&UsbHandle, LaneIndex)) {
        PchHsioHandleInit (SiPolicy, NULL, LaneIndex, (UINT32)-1, &HsioHandle, &HsioPrivate, &HsioLane, &P2SbController, &SbAccessHsio);
        XhciUsb3HsioTuning (&HsioHandle, LaneIndex);
      }
    }
  }

  //
  // It is required to clear xHCI D3HE in case of PCR access during init
  //
  ClearXhciD3he (&UsbHandle);

  //
  // Configure PCH xHCI
  //
  PostCode (0xB06);
  XhciConfigure (&UsbHandle);
  if (PciSegmentRead16 (HostController.PciCfgBaseAddr + PCI_VENDOR_ID_OFFSET) != 0xFFFF) {
    //
    // Configure xHCI interrupt
    //
    InterruptPin = ItssGetDevIntPin (SiPolicy, PchXhciDevNumber (), PchXhciFuncNumber ());

    PciSegmentWrite8 (HostController.PciCfgBaseAddr + PCI_INT_PIN_OFFSET, InterruptPin);

    //
    // After initialization is done D3HE can be reenabled
    //
    SetXhciD3he (&UsbHandle);

    //
    // Set all necessary lock bits in xHCI controller
    //
    XhciLockConfiguration (&UsbHandle);
  }

  //
  // Configure PCH OTG (xDCI)
  //
  PostCode (0xB08);
  XdciConfigure (&UsbHandle);
  if (UsbHandle.UsbConfig->XdciConfig.Enable) {
    //
    // Configure xDCI interrupt
    //
    ItssGetDevIntConfig (
      SiPolicy,
      DeviceController.Device,
      DeviceController.Function,
      &InterruptPin,
      &Irq
      );

    //
    // Set Interrupt Pin and IRQ number
    //
    Data32Or = (UINT32) ((InterruptPin << N_OTG_PCR_PCICFGCTRL_INT_PIN) |
                          (Irq << N_OTG_PCR_PCICFGCTRL_PCI_IRQ));
    Data32And =~(UINT32) (B_OTG_PCR_PCICFGCTRL_PCI_IRQ | B_OTG_PCR_PCICFGCTRL_ACPI_IRQ | B_OTG_PCR_PCICFGCTRL_INT_PIN | B_OTG_PCR_PCICFGCTRL_ACPI_INT_EN);

    PchPcrAndThenOr32 (PID_OTG, R_OTG_PCR_PCICFGCTRL1, Data32And, Data32Or);
  }
}
