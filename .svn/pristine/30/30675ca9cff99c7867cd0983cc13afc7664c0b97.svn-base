/** @file
  Main Funcion file.

;******************************************************************************
;* Copyright (c) 2014, Lenovo Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Uefi.h>
#include <SetupConfig.h>
#include "LenovoChargingLogo.h"
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/AcpiSupport.h>
#include <Lfc.h>
#include <Library/LfcEcLib.h>
#include <Protocol/LfcNvsArea.h>
#include <Protocol/LenovoVariable.h>
#include <Protocol/StartOfBdsDiagnostics.h>

EFI_STATUS
LCL_ClearScreen(
  VOID
  )
{
  gST->ConOut->ClearScreen (gST->ConOut);  

  return EFI_SUCCESS;
}


EFI_STATUS
LCL_InitializeGUILogo(
  VOID
)
{
	EFI_STATUS Status = EFI_SUCCESS;  
    
  Status = InitializeGUI();
  if (EFI_ERROR(Status)) {
     DebugL(L"InitializeGUI Error.\r\n");
     return Status;
  }

	return EFI_SUCCESS;
}

EFI_STATUS
LCL_Draw_Battery(
  UINTN Percent,
  UINT8 ACStatus,
  UINT8 BattStatus
)
{
	EFI_STATUS Status = EFI_SUCCESS;  

  DrawBatteryInfo(Percent,ACStatus,BattStatus);

  DrawBattery(Percent);

	return Status;
}

EFI_STATUS
LfcEcPowerStateIsAcOrAnykey (
  OUT BOOLEAN                        *IsAcStatus,
  OUT BOOLEAN                        *IsAnykeyStatus  
  )
{
  EFI_STATUS        Status;
  UINT8             AcorAnykeyStatus = TRUE;
	
  //if (!mLcfcEcLibSupport) {	
  //return EFI_UNSUPPORTED;
  //}
  //
  // Get AC State connect or press any key by EC ram register 0x05 
  //
  Status = LfcEcLibEcRamRead (0x05, &AcorAnykeyStatus);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  
  // Check if press any key ?
  if ((AcorAnykeyStatus & 0x01) == 0) {
    *IsAnykeyStatus = FALSE;    
  } else {
    *IsAnykeyStatus = TRUE;
  }  

  // Check if plug in AC ?
  if ((AcorAnykeyStatus & 0x02) == 0) {
    *IsAcStatus = FALSE;    
  } else {
    *IsAcStatus = TRUE;
  }
  
  return Status;	
}

EFI_STATUS
LenovoChargingLogoCallBack (
  IN EFI_EVENT                           Event,
  IN VOID                                *Context
  )
{
  EFI_STATUS                  Status = EFI_SUCCESS;
  UINTN                       DisplayTime = 0;  //seconds
  UINT8                       Percent = 0;
  UINT8                       Percent_Bak = 0xFF;

  UINT8                       ACStatus = 0;
  UINT8                       ACStatus_Bak = 0xFF;
  
  UINT8                       BattStatus = 0;
  UINT8                       BattStatus_Bak = 0xFF;

  UINT8                       IndexPort;
  UINTN                                     VarSize;
  SYSTEM_CONFIGURATION                      SetupVariable;
  
  VarSize = sizeof (SYSTEM_CONFIGURATION);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSystemConfigurationGuid,
                  NULL,
                  &VarSize,
                  &SetupVariable
                  );
  if (EFI_ERROR (Status)) {
    //
    // Setup menu is default setting.
    //
    return Status;
  }

  if (SetupVariable.L05ChargingLogoShow == 0) {
    return Status;	
  }
  LCL_ClearScreen();
  Status = LCL_InitializeGUILogo();
  if (EFI_ERROR (Status)) {
    /*Reset system.*/
    gRT->ResetSystem (EfiResetWarm, EFI_SUCCESS, 0, NULL);
  }

  IndexPort = 0x4E;
  IoWrite8 (IndexPort, 0x87);
  IoWrite8 (IndexPort, 0x01);
  IoWrite8 (IndexPort, 0x55);
  IoWrite8 (IndexPort, 0x55);
  
  while(DisplayTime++ < DISPLAY_TIME_SECONDS)
  {
    gBS->Stall(1000000); //wait for 1 second.

    /*get battery percent.*/
    //GetBatteryPercentage(&Percent);	
    LfcEcLibGetBatteryPercentage(&Percent);
	
    /*get AC Status.*/
    //GetACStatus(&ACStatus);
	LfcEcLibPowerStateIsAc(&ACStatus);

    /*get Battery Status.*/
    GetBattStatus(&BattStatus);

    if((Percent_Bak != Percent) ||
      (ACStatus_Bak != ACStatus) ||
      (BattStatus_Bak != BattStatus) ){

      Percent_Bak = Percent;
      ACStatus_Bak = ACStatus;
      BattStatus_Bak = BattStatus;

      LCL_ClearScreen();
      LCL_Draw_Battery((UINTN)Percent, ACStatus, BattStatus);
    }
  }

  //Show charging logo for 30s, then shutdown.
  gRT->ResetSystem (EfiResetShutdown, EFI_SUCCESS, 0, NULL);

  return Status;
}

/**
  The user Entry Point for Application. The user code starts with this function
  as the real entry point for the application.

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.  
  @param[in] SystemTable    A pointer to the EFI System Table.
  
  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurs when executing this entry point.

**/
EFI_STATUS
EFIAPI
LenovoChargingLogoEntry (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS                    Status;
  EFI_EVENT                     Event;
  VOID                          *Registration;
  BOOLEAN                       IsAcPowerOn;
  BOOLEAN                       IsAnykeyPowerOn;
  
  Status = LfcEcPowerStateIsAcOrAnykey(&IsAcPowerOn, &IsAnykeyPowerOn);
  
  if ((IsAcPowerOn == TRUE) || (IsAnykeyPowerOn == TRUE)) {
  // Install notify to callback
  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  LenovoChargingLogoCallBack,
                  NULL,
                  &Event
                  );
  if (!EFI_ERROR (Status)) {
    Status = gBS->RegisterProtocolNotify (
                    &gEfiStartOfBdsDiagnosticsProtocolGuid,
                    Event,
                    &Registration
                    );
  }
  }
  
  return EFI_SUCCESS;  
}
