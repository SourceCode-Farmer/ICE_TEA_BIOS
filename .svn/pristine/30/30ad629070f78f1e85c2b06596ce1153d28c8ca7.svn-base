/** @file
  Provides an opportunity for OEM to decide what SSID/SVID to use.

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeOemSvcKernelLib.h>
//[-start-210909-TAMT000007-add]//
//[-start-211217-Ching000019-modify]//
#ifdef S77014_SUPPORT
#include <Library/OemSvcLfcPeiGetBoardID.h>
#include <Library/PciExpressLib.h>
#include <Library/S3BootScriptLib.h>
#include <Library/PcdLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <SetupVariable.h>
#include <Uefi.h>
#include <Library/GpioLib.h>
#include <Pins/GpioPinsVer2Lp.h>

#define INTEL_VGA_VID                       0x8086
#define INTEL_VGA_DID_46A0                  0x46A0
#define INTEL_VGA_DID_46A6                  0x46A6

#define NVIDIA_VID                          0x10DE
#define NVIDIA_GN18S_DID                    0x1F9F
#define NVIDIA_GN20_S5_DID                  0x25A6
#define NVIDIA_GN20_S7_DID                  0x25A9

#define S77014_INTEL_VGA_UMA_ONLY_SSIDSVID_46A0  0x380217AA
#define S77014_INTEL_VGA_UMA_ONLY_SSIDSVID_46A6  0x380717AA

#define S77014_GN18S_SSIDSVID               0x3AF417AA
#define S77014_GN20_S5_SSIDSVID             0x3AF317AA
#define S77014_GN20_S7_SSIDSVID             0x3B2E17AA
#endif
//[-end-211217-Ching000019-modify]//
//[-end-210909-TAMT000007-add]//

//[-start-220318-Ching000036-modify]//
#ifdef S77014IAH_SUPPORT
#include <Library/OemSvcLfcPeiGetBoardID.h>
#include <Library/PciExpressLib.h>
#include <Library/S3BootScriptLib.h>
#include <Library/PcdLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <SetupVariable.h>
#include <Uefi.h>
#include <Library/GpioLib.h>
#include <Pins/GpioPinsVer2Lp.h>

#define INTEL_VGA_VID                       0x8086
#define INTEL_VGA_DID_46A3                  0x46A3
#define INTEL_VGA_DID_46A6                  0x46A6

#define NVIDIA_VID                          0x10DE
#define NVIDIA_GN18_S5_DID                  0x1F9F
#define NVIDIA_GN20_S7_DID                  0x25A9

#define S77014IAH_INTEL_VGA_UMA_ONLY_SSIDSVID  0x3B3717AA

#define S77014IAH_GN18_S5_SSIDSVID             0x3B3817AA
#define S77014IAH_GN20_S7_SSIDSVID             0x3B3917AA
#endif
//[-end-220318-Ching000036-modify]//

//[-start-210623-Dongxu0003-Modify]
//[-start-210611-Dongxu0001-Modify]
//[-start-210925-TAMT000014-midofy]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT)
//[-end-210925-TAMT000014-modify]//
// Sample code for OEM project.
// The code is for BIOS to configure SSID&SSVID for NVIDA/AMD add-in card.
// The address of SSID&SSVID is different with normal PCIE device's address 0x2C.
#include <Library/PciExpressLib.h>
#include <Library/S3BootScriptLib.h>
//[start-210802-STORM1103-modify]
#include <Library/OemSvcLfcPeiGetBoardID.h>
//[end-210802-STORM1103-modify]

// [VID]
#define INTEL_VID                       0x8086        
#define REALTEK_VID                     0x10EC
// [DID]
//[start-210802-STORM1103-modify]
#define INTEL_DG2_DID                   0x5693
#define INTEL_DID_46A0                  0x46A0
#define INTEL_DID_46A1                  0x46A1
#define INTEL_DID_46A6                  0x46A6
#define INTEL_DID_46A8                  0x46A8
#define INTEL_IGPU_UMA                  0x3AE417AA
#define INTEL_IGPU_DIS                  0x3AE317AA
//[end-210802-STORM1103-modify]

// Ssid Table Struct
typedef struct {
  UINT16 Vid;
  UINT16 Did;
  UINT8  Bus;
  UINT8  Dev;
  UINT8  Fun;
  UINT32 SsidSvid;
} SSID_TABLE_STRU;
#ifdef C970_SUPPORT 
SSID_TABLE_STRU	YogaC970_SsidTable[] = {
  // Vid         Did      Bus    Dev    Fun   SsidSvid
  { INTEL_VID,   0x46A0,  0x00,  0x02,  0x00,  0x380417AA }, //Graphics Device
  //{ INTEL_VID,   0x464D,  0x00,  0x06,  0x00,  0x380117AA }, //Intel Corporation PCI-to-PCI Bridge (PCIE)
  //{ INTEL_VID,   0x466E,  0x00,  0x07,  0x00,  0x380117AA }, //Intel Corporation PCI-to-PCI Bridge (PCIE)
  //{ INTEL_VID,   0x463F,  0x00,  0x07,  0x01,  0x380117AA }, //Intel Corporation PCI-to-PCI Bridge (PCIE)
  //{ INTEL_VID,   0x462F,  0x00,  0x07,  0x02,  0x380117AA }, //Intel Corporation PCI-to-PCI Bridge (PCIE)
 
  //  Unknow Device    ,
  { 0xFFFF,    0xFFFF,  0xFF,  0xFF,  0xFF,    0xFFFFFFFF  }  
};

#endif
//[-start-210925-TAMT000014-add]//
//[-start-220215-Ching000029-modify]//
//[-start-220412-DABING0050-modify]//
#ifdef S77013_SUPPORT 
SSID_TABLE_STRU	S77013_SsidTable[] = {
  // Vid         Did      Bus    Dev    Fun   SsidSvid
  { 0x8086,   0x46A0,  0x00,  0x02,  0x00,  0x380917AA }, //Graphics Device
  { 0x8086,   0x46A6,  0x00,  0x02,  0x00,  0x380D17AA }, //Graphics Device
  //  Unknow Device    ,
  { 0xFFFF,    0xFFFF,  0xFF,  0xFF,  0xFF,    0xFFFFFFFF  }  
};

#endif
//[-end-220412-DABING0050-modify]//
//[-end-220215-Ching000029-modify]//
//[-end-210925-TAMT000014-add]//

//[start-210720-STORM1100-modify]//
//[start-210802-STORM1103-modify]
//#ifdef C770_SUPPORT 
//SSID_TABLE_STRU  YogaC770_SsidTable[] = {
//  // Vid         Did      Bus    Dev    Fun   SsidSvid
//  { INTEL_VID,   0x46A0,  0x00,  0x02,  0x00,  0x380417AA }, //Graphics Device
//  { INTEL_VID,   0x46A0,  0x00,  0x02,  0x00,  0x380417AA }, //Graphics Device
//  { INTEL_VID,   0x46A0,  0x00,  0x02,  0x00,  0x380417AA }, //Graphics Device
//  { INTEL_VID,   0x46A0,  0x00,  0x02,  0x00,  0x380417AA }, //Graphics Device
//
//  //  Unknow Device    ,
//  { 0xFFFF,    0xFFFF,  0xFF,  0xFF,  0xFF,    0xFFFFFFFF  }  
//};
//
//#endif
//[end-210720-STORM1100-modify]//
//[end-210802-STORM1103-modify]

EFI_STATUS
ProgramSsidSvid0xFFFF1002 (
  IN     UINT8    Bus,
  IN     UINT8    Dev,
  IN     UINT8    Func,
  IN OUT UINT32  *SsidSvid
  )
{
  UINT64 BootScriptPciAddress;
  //
  // Program SSID / SSVID
  //
  PciExpressWrite32 (PCI_EXPRESS_LIB_ADDRESS(Bus, Dev, Func, 0x4C), *SsidSvid);

  BootScriptPciAddress = S3_BOOT_SCRIPT_LIB_PCI_ADDRESS (Bus, Dev, Func, 0x4C);
  S3BootScriptSavePciCfgWrite (
      S3BootScriptWidthUint32, 
      BootScriptPciAddress,
      1, 
      SsidSvid);
  
  return EFI_SUCCESS;
}

EFI_STATUS
ProgramSsidSvid0xFFFF10DE (
  IN     UINT8    Bus,
  IN     UINT8    Dev,
  IN     UINT8    Func,
  IN OUT UINT32  *SsidSvid
  )
{
  UINT64     BootScriptPciAddress;
  //
  // Program SSID / SSVID
  //
//[start-210802-STORM1103-modify]
#ifdef C770_SUPPORT
    PciExpressWrite32 (PCI_EXPRESS_LIB_ADDRESS(Bus, Dev, Func, 0x2C), *SsidSvid);
    BootScriptPciAddress = S3_BOOT_SCRIPT_LIB_PCI_ADDRESS (Bus, Dev, Func, 0x2C);
#else
//[end-210802-STORM1103-modify]
  PciExpressWrite32 (PCI_EXPRESS_LIB_ADDRESS(Bus, Dev, Func, 0x40), *SsidSvid);

  BootScriptPciAddress = S3_BOOT_SCRIPT_LIB_PCI_ADDRESS (Bus, Dev, Func, 0x40);
//[start-210802-STORM1103-modify]
#endif
//[end-210802-STORM1103-modify]
  S3BootScriptSavePciCfgWrite (
      S3BootScriptWidthUint32, 
      BootScriptPciAddress,
      1, 
      SsidSvid);

  return EFI_SUCCESS;
}

STATIC OEM_SSID_SVID_TABLE SsidTable[] = {
  0x10DE, DEVICE_ID_DONT_CARE, ProgramSsidSvid0xFFFF10DE,
  0x1002, DEVICE_ID_DONT_CARE, ProgramSsidSvid0xFFFF1002,
  DEVICE_ID_DONT_CARE, DEVICE_ID_DONT_CARE, NULL
};
#endif

//[-start-210723-QINGLIN0001-add]//
#if defined(S570_SUPPORT)
#include <Library/PciExpressLib.h>
#include <Library/S3BootScriptLib.h>
#include <SetupVariable.h>
#include <Uefi.h>
#include <Library/GpioLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Pins/GpioPinsVer2Lp.h>
//[-start-211025-QINGLIN0103-add]//
#include <CommonDefinitions.h>
#include <Library/OemSvcLfcPeiGetBoardID.h>
//[-end-211025-QINGLIN0103-add]//

// [VID]
#define INTEL_VID                       0x8086
#define REALTEK_VID                     0x10EC
#define NVIDIA_VID                      0x10DE
// [DID]
//[-start-211015-QINGLIN0100-modify]//
//[-start-211025-QINGLIN0103-modify]//
#define INTEL_VGA_DID_46A0  0x46A0  //U28
#define INTEL_VGA_DID_46A6  0x46A6  //U28
#define INTEL_VGA_DID_46A8  0x46A8  //U15
//[-start-220407-JEPLIUT220-add]//
#define INTEL_VGA_DID_46B3       0x46B3 
#define INTEL_VGA_DID_4628       0x4628
#define INTEL_VGA_DID_4626       0x4626
//[-end-220407-JEPLIUT220-add]//
#define NVIDIA_N18S5_DID      0x1F9F
#define NVIDIA_N20S5_DID      0x25A6
#define NVIDIA_N20S7_DID      0x25A9

#define INTEL_IGPU_UMA          0x3AF217AA
#define INTEL_IGPU_DIS_N18S5    0x3E7E17AA
#define INTEL_IGPU_DIS_N20S5    0x3E7D17AA
#define INTEL_IGPU_DIS_N20S7    0x3E7917AA
//[-end-211025-QINGLIN0103-modify]//
//[-end-211015-QINGLIN0100-modify]//

EFI_STATUS
ProgramSsidSvid0xFFFF10DE (
  IN     UINT8    Bus,
  IN     UINT8    Dev,
  IN     UINT8    Func,
  IN OUT UINT32  *SsidSvid
  )
{
  UINT64     BootScriptPciAddress;
  //
  // Program SSID / SSVID
  //
#ifdef S570_SUPPORT
  PciExpressWrite32 (PCI_EXPRESS_LIB_ADDRESS(Bus, Dev, Func, 0x2C), *SsidSvid);
  BootScriptPciAddress = S3_BOOT_SCRIPT_LIB_PCI_ADDRESS (Bus, Dev, Func, 0x2C);
#else
  PciExpressWrite32 (PCI_EXPRESS_LIB_ADDRESS(Bus, Dev, Func, 0x40), *SsidSvid);

  BootScriptPciAddress = S3_BOOT_SCRIPT_LIB_PCI_ADDRESS (Bus, Dev, Func, 0x40);
#endif
  S3BootScriptSavePciCfgWrite (
      S3BootScriptWidthUint32, 
      BootScriptPciAddress,
      1, 
      SsidSvid);

  return EFI_SUCCESS;
}

//[-start-211025-QINGLIN0103-add]//
EFI_STATUS
LfcGetSSIDbyGraphicDeviceId (
  IN OUT UINT32 *SsidSvid
)
{
  UINTN                                       PciAddress;
  UINT8                                       DgpuBridgeBus;
  UINT8                                       DgpuBridgeDevice;
  UINT8                                       DgpuBridgeFunction;
  UINT8                                       MasterDgpuBus;
  UINT16                                      MasterDgpuVendorId;
  UINT16                                      MasterDgpuDeviceId;

  DgpuBridgeBus      = PcdGet8 (PcdHgPegBridgeBus);
  DgpuBridgeDevice   = PcdGet8 (PcdHgPegBridgeDevice);
  DgpuBridgeFunction = PcdGet8 (PcdHgPegBridgeFunction);

  //
  // Read Master discrete GPU Bus and Venodr ID.
  //
  PciAddress = PCI_EXPRESS_LIB_ADDRESS (
                 DgpuBridgeBus,
                 DgpuBridgeDevice,
                 DgpuBridgeFunction,
                 PCI_SBUS
                 );
  MasterDgpuBus = PciExpressRead8 (PciAddress);
  
  PciAddress = PCI_EXPRESS_LIB_ADDRESS (
                 MasterDgpuBus,
                 0x00,
                 0x00,
                 PCI_VID
                 );
  MasterDgpuVendorId = PciExpressRead16 (PciAddress);
  
  PciAddress = PCI_EXPRESS_LIB_ADDRESS (
                 MasterDgpuBus,
                 0x00,
                 0x00,
                 PCI_DID
                 );
  MasterDgpuDeviceId = PciExpressRead16 (PciAddress);

  if (MasterDgpuVendorId == NVIDIA_VID) {
    switch(MasterDgpuDeviceId) {
      case NVIDIA_N18S5_DID:  //GN18-S5
        *SsidSvid = INTEL_IGPU_DIS_N18S5;
        break;
      case NVIDIA_N20S5_DID:  //GN20-S5
        *SsidSvid = INTEL_IGPU_DIS_N20S5;
        break;
      case NVIDIA_N20S7_DID:  //GN20-S7
        *SsidSvid = INTEL_IGPU_DIS_N20S7;
        break;
      default:
        break;
    }
  }

  return EFI_SUCCESS;
}
//[-end-211025-QINGLIN0103-add]//

#endif
//[-end-210723-QINGLIN0001-add]//

//[-start-210804-QINGLIN0008-add]//
#if defined(S370_SUPPORT)
#include <Library/PciExpressLib.h>
#include <Library/S3BootScriptLib.h>
#include <SetupVariable.h>
#include <Uefi.h>
#include <Library/GpioLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/OemSvcLfcPeiGetBoardID.h>
//[-start-211110-QINGLIN0111-add]//
#include <Library/DebugLib.h>
#include <Library/IoLib.h>                     // For MmioWrite32
//[-end-211110-QINGLIN0111-add]//
//[-start-211110-QINGLIN0112-add]//
#include <SetupVariable.h>
#include <Library/VariableLib.h>
#include <SetupConfig.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
//[-end-211110-QINGLIN0112-add]//
//[-start-220216-QINGLIN0156-add]//
#include <Library/OemSvcLfcPeiGetBoardID.h>
//[-end-220216-QINGLIN0156-add]//

//[-start-211019-Xinwei003-Modify]//
// [VID]
#define INTEL_VID                       0x8086
#define REALTEK_VID                     0x10EC
// [DID]
#define INTEL_VGA_DID_46B3                  0x46B3 //U15 i3/Pentium/Celeron core + Single/Dual memory chanel
#define INTEL_VGA_DID_46A8                  0x46A8 //U15 i7/i5 core + Dual memory chanel
//[-start-220125-QINGLIN0152-add]//
#define INTEL_VGA_DID_4628                  0x4628 //U15 i7/i5 core + Single memory chanel 
//[-end-220125-QINGLIN0152-add]//
#define INTEL_DG2_SKU6_DID                  0x5695 //DG2 SKU6

// SSID
//[-start-220217-QINGLIN0159-modify]//
#define INTEL_VGA_UMA_SSID                  0x3B3A17AA
#define INTEL_VGA_DIS_SSID                  0x3B3B17AA
//[-end-220217-QINGLIN0159-modify]//

//[-start-211110-QINGLIN0111-add]//
#if defined(BAYHUB_CARD_READER_SUPPORT) || defined(GENESYS_LOGIC_CARD_READER_SUPPORT)
#define CARDREADER_ROOTBRIDGE_BUS 0    // Card Reader Root Bridge Bus
#define CARDREADER_ROOTBRIDGE_DEV 0x1D // Card Reader Root Bridge Device
#define CARDREADER_ROOTBRIDGE_FUN 1    // Card Reader Root Bridge Function

//---------------------------------------------------------------------------
// Structure Defination
//---------------------------------------------------------------------------
typedef struct _PCI_REG_TABLE{
    UINT32  Offset;
    UINT32  AndMask;
    UINT32  OrValue;
} PCI_REG_TABLE;

VOID*
GetPciExpressBaseAddress (
  VOID
  );
#endif

#if defined(BAYHUB_CARD_READER_SUPPORT)
//---------------------------------------------------------------------------
// Device Information
//---------------------------------------------------------------------------
#define BayHub_Card_Reader_VID         0x1217  // BayHub Card Reader VID
#define BayHub_Card_Reader_DID         0x8621  // BayHub Card Reader DID

#define LTRMaxLatencyValueScale        0x10031003      // 3ms
#define L12ThresholdValueScale         0x406E000F      // 110us

#define LENOVO_OZ711LV2_SSVID_SSID     0x387417AA

PCI_REG_TABLE OZ621PciRegTable[] = {  
  { 0xD0,  0x7FFFFFFF,  0x00000000 }, // Unlock write protect.
  //
  // Program O2SD Subsystem Vendor/Device ID
  //
  { 0x2C,  0x00000000,  LENOVO_OZ711LV2_SSVID_SSID }, // Programing SSID/SVID.
  //
  // Disable O2SD function if it is necessary - If L1 Substate function is supported by the platform
  //
  { 0xE0,  0x0FFFFFFF,  0x30000000 }, // Set the PCI PM L1 Entrance timer to 16us.
//[-start-220129-QINGLIN0154-modify]//
  { 0x3E4, 0xFFF7FFFF,  0x00080000 }, // Set the IDDQ function. 
//[-end-220129-QINGLIN0154-modify]//
  { 0x248, 0x00000000,  L12ThresholdValueScale }, // 110us - L1.2 Threshold scale register set to 010b, L1.2 Threshold value set to  00 0110 1110b, Enable PCI-PM L1 Sub state and ASPM L1 Sub state enable
  { 0x90,  0xFFFFFFFC,  0x00000003 }, // Enable L0s=bit0, L1=bit1.
  { 0x90,  0xFFFFFEFF,  0x00000100 }, // Enable using CLK_REQ# to save power.
  //
  // Reverse CD# and WP polarity if it is necessary - Skip now
  //
  //
  // Disable RTD3 (Run Time D3) function if it is necessary - Skip now, default is Enable
  //
  //{ 0x3E0, 0xDFFFFFFF,  0x00000000 }, // Enable RTD3 function.
  //
  // Enable LTR (Latency Tolerant Reporting) feature if it is necessary
  //
  { 0xA8,  0xFFFFFBFF,  0x00000400 }, // LTR Mechanism Enable; if the application does not support LTR Bit[10] set to 0b.
  { 0x234, 0x00000000,  LTRMaxLatencyValueScale }, // Set max no-snoop latency and max snoop latency -> 3ms, Thinkpad Intel not use this
  //
  // L1 Substate function related
  //
  { 0x248, 0x00000000,  L12ThresholdValueScale }, // 110us - L1.2 Threshold scale register set to 010b, L1.2 Threshold value set to  00 0110 1110b, Enable PCI-PM L1 Sub state and ASPM L1 Sub state enable
  //
  // Enable Power saving mode if it is necessary. (PCI Express Capability Register, Link Control register.) 
  //
  { 0x90,  0xFFFFFFFC,  0x00000003 }, // Enable L0s=bit0, L1=bit1.
  { 0x90,  0xFFFFFEFF,  0x00000100 }, // Enable using CLK_REQ# to save power.
  //
  // PART B
  //
  //
  // Set the L1 Entrance Timer
  //
  { 0xE0,  0x0FFFFFFF,  0x30000000 }, // Set the PCI PM L1 Entrance timer to 16us.
  { 0xFC,  0xFFF0FFFF,  0x00080000 }, // Set the ASPM L1 Entrance timer to 512us.
  //
  // Set the IDDQ function for more power saving
  //
//[-start-220129-QINGLIN0154-modify]//
  { 0x3E4, 0xFFF7FFFF,  0x00080000 }, // Set the IDDQ function. 
//[-end-220129-QINGLIN0154-modify]//
  //
  // Set the tuning pass window threshold
  //
  { 0x300, 0xFFFFFF00,  0x00000055 }, // Set the tuning pass window threshold for SDR104 and SDR50 to 5.
  //
  // Set DLL default sample clock phase of SDR50 if all phases tuning pass
  //
  { 0x300, 0xFFFF0FFF,  0x00006000 }, // Set DLL default sample clock phase of SDR50 if all phases tuning pass. -> Thinkpad Intel not use this
  //
  // Set drive strength for 1.8V&3.3V signal
  //
  { 0x304, 0xFFFFFF81,  0x00000044 }, // Set Card Reader driving strength. For EMC request.
  { 0xD0,  0xFFFFFFFF,  0x80000000 }, // Lock write protect.  
};
#endif

#if defined(GENESYS_LOGIC_CARD_READER_SUPPORT)
//---------------------------------------------------------------------------
// Device Information
//---------------------------------------------------------------------------
#define GenesysLogic_Card_Reader_VID   0x17A0  // GenesysLogic Card Reader VID
#define GenesysLogic_Card_Reader_DID   0x9750  // GenesysLogic Card Reader DID

#define LENOVO_GL9750_OIYL4_SSVID_SSID 0x384A17AA

PCI_REG_TABLE GL9750PciRegTable[] = {  
  { 0x800,  0xFFFFFFFE,  0x00000001                     }, // Unlock write protect
//[-start-220129-QINGLIN0154-remove]//
//  { 0x844,  0x7FFFFFFF,  0x80000000                     },// Set the IDDQ function.
//[-end-220129-QINGLIN0154-remove]//
  { 0x848,  0xFFFF8FC7,  0x00006030                     }, // Adjust L1 Exit Latency if to enable ASPM
  { 0x848,  0xFFFFFFBF,  0x00000000                     }, // Disable ASPM L0s
  { 0x860,  0x0FFFFFFF,  0x00800000                     }, // Ennable DFx for Modern Standby
  { 0x878,  0xFDFFFFFF,  0x02000000                     }, // Disable wake up system by card insertion or removal during S3/S4
  { 0x87C,  0x00000000,  LENOVO_GL9750_OIYL4_SSVID_SSID }, // Programing SSID/SVID
  { 0x800,  0xFFFFFFFE,  0x00000000                     }, // Lock write protect
};
#endif

#if defined(BAYHUB_CARD_READER_SUPPORT)
//[-start-211110-QINGLIN0112-add]//
BOOLEAN
OemSvcLfcIsSetupWlanConfigDisable (
  VOID
  )
{
  SYSTEM_CONFIGURATION        *SetupNvData;
  UINTN                       BufferSize;
  BOOLEAN                     IsWlanDisable;

  IsWlanDisable = FALSE;

  CommonGetVariableDataAndSize (
    L"Setup",
    &gSystemConfigurationGuid,
    &BufferSize,
    (VOID **) &SetupNvData
    );

  if (SetupNvData != NULL) {
    if (SetupNvData->L05WirelessLan == 0) { //disable
      IsWlanDisable = TRUE;
    }
    FreePool (SetupNvData);
  }

  return IsWlanDisable;
}
//[-end-211110-QINGLIN0112-add]//

EFI_STATUS
EFIAPI
CheckAndProgramBayHubCardReader (
  IN     UINT16   VendorId,
  IN     UINT16   DeviceId,
  IN     UINT8    Bus,
  IN     UINT8    Dev,
  IN     UINT8    Func
  )
{
  UINTN                MemBaseAddr;
  UINT32               Temp32;
  UINT8                Index;
//[-start-211110-QINGLIN0112-add]//
  UINT8                BrgBusNum;
  UINT8                BrgDevNum;
  UINT8                BrgFunNum;
//[-end-211110-QINGLIN0112-add]//


  DEBUG((DEBUG_INFO, __FUNCTION__" Entry\n"));
  DEBUG ((DEBUG_INFO, "CardReader Vid = 0x%04X, Did = 0x%04X\n", VendorId, DeviceId));

  //
  // Determine device by Vendor ID and Device ID.
  //
  if ((VendorId != BayHub_Card_Reader_VID) || (DeviceId != BayHub_Card_Reader_DID)) {
    return EFI_UNSUPPORTED;
  }
  DEBUG ((DEBUG_INFO, "Find Bayhub Card Reader(B:%d/D:%d:/F:%d)\n", Bus, Dev, Func));

  MemBaseAddr = (UINTN) GetPciExpressBaseAddress () + PCI_EXPRESS_LIB_ADDRESS(Bus, Dev, Func, 0);
  DEBUG((DEBUG_ERROR, "MemBaseAddr = 0x%X\n", MemBaseAddr));

  //
  // Write Value To Crc Register
  //
  for(Index = 0 ; Index < sizeof(OZ621PciRegTable)/sizeof(PCI_REG_TABLE) ; Index ++) {
    if(OZ621PciRegTable[Index].Offset == 0xFFFFFFFF) break;

    //
    // Get Current setting.
    //
    Temp32 = MmioRead32((UINTN) (MemBaseAddr+ OZ621PciRegTable[Index].Offset));
    DEBUG ((DEBUG_INFO, "Read Bayhub Card Reader Register Offset %.4X: %.8X\n", OZ621PciRegTable[Index].Offset, Temp32));
    //
    // Prepare OEM Setting.
    //
    Temp32 = (Temp32 & OZ621PciRegTable[Index].AndMask) | OZ621PciRegTable[Index].OrValue;

    //
    // Write OEM Setting.
    //
    MmioWrite32((UINTN) (MemBaseAddr + OZ621PciRegTable[Index].Offset), Temp32);
    DEBUG ((DEBUG_INFO, "Write Bayhub Card Reader Register Offset %.4X: %.8X\n", OZ621PciRegTable[Index].Offset, Temp32));
  }

//[-start-211110-QINGLIN0112-modify]//
  BrgBusNum = CARDREADER_ROOTBRIDGE_BUS;
  BrgDevNum = CARDREADER_ROOTBRIDGE_DEV;
  BrgFunNum = CARDREADER_ROOTBRIDGE_FUN;

  if (OemSvcLfcIsSetupWlanConfigDisable()) {
    BrgFunNum = 0;
  }

  // Bayhub PCI Bridge
  MemBaseAddr = PCI_EXPRESS_LIB_ADDRESS (BrgBusNum, BrgDevNum, BrgFunNum, 0);
//[-end-211110-QINGLIN0112-modify]//

  //
  // Card Reader Pcie root Port L1 Sub-States Control 1 (L1SCTL1) Offset 208h
  //
  // Program root port Threshold scale & Threshold value same as Device (L12ThresholdValueScale)
  // Program root port  PCI-PM L1 Substate and ASPM L1 Substate
  //
  Temp32 = L12ThresholdValueScale;
  PciExpressWrite32((UINTN) (MemBaseAddr + 0x208), Temp32);

  DEBUG((DEBUG_INFO, __FUNCTION__" End\n"));
  return EFI_SUCCESS;
}
#endif

#if defined(GENESYS_LOGIC_CARD_READER_SUPPORT)
EFI_STATUS
EFIAPI
CheckAndProgramGenesysLogicCardReader (
  IN     UINT16   VendorId,
  IN     UINT16   DeviceId,
  IN     UINT8    Bus,
  IN     UINT8    Dev,
  IN     UINT8    Func
  )
{
  UINTN                MemBaseAddr;
  UINT32               Temp32;
  UINT8                Index;

  DEBUG ((DEBUG_INFO, __FUNCTION__" Entry\n"));
  DEBUG ((DEBUG_INFO, "CardReader Vid = 0x%04X, Did = 0x%04X\n", VendorId, DeviceId));
  //
  // Determine device by Vendor ID and Device ID.
  //
  if ((VendorId != GenesysLogic_Card_Reader_VID) || (DeviceId != GenesysLogic_Card_Reader_DID)) {
    return EFI_UNSUPPORTED;
  }
  DEBUG ((DEBUG_INFO, "Find GenesysLogic Card Reader(B:%d/D:%d:/F:%d)\n", Bus, Dev, Func));

  MemBaseAddr = (UINTN) GetPciExpressBaseAddress () + PCI_EXPRESS_LIB_ADDRESS(Bus, Dev, Func, 0);
  DEBUG ((DEBUG_ERROR, "MemBaseAddr = 0x%X\n", MemBaseAddr));

  //
  // Write Value To Crc Register
  //
  for(Index = 0 ; Index < sizeof(GL9750PciRegTable)/sizeof(PCI_REG_TABLE) ; Index ++) {
    if(GL9750PciRegTable[Index].Offset == 0xFFFFFFFF) break;

    //
    // Get Current setting.
    //
    Temp32 = MmioRead32((UINTN) (MemBaseAddr+ GL9750PciRegTable[Index].Offset));
    DEBUG ((DEBUG_INFO, "Read Genesys Logic Card Reader Register Offset %.4X: %.8X\n", GL9750PciRegTable[Index].Offset, Temp32));
    //
    // Prepare OEM Setting.
    //
    Temp32 = (Temp32 & GL9750PciRegTable[Index].AndMask) | GL9750PciRegTable[Index].OrValue;

    //
    // Write OEM Setting.
    //
    MmioWrite32((UINTN) (MemBaseAddr + GL9750PciRegTable[Index].Offset), Temp32);
    DEBUG ((DEBUG_INFO, "Write Genesys Logic Card Reader Register Offset %.4X: %.8X\n", GL9750PciRegTable[Index].Offset, Temp32));
  }

  DEBUG((DEBUG_INFO, __FUNCTION__" End\n"));
  return EFI_SUCCESS;
}
#endif
//[-end-211110-QINGLIN0111-add]//
#endif
//[-end-210804-QINGLIN0008-add]//

//[-start-210909-TAMT000007-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
EFI_STATUS
ProgramSsidSvid0xFFFF10DE (
  IN     UINT8    Bus,
  IN     UINT8    Dev,
  IN     UINT8    Func,
  IN OUT UINT32  *SsidSvid
  )
{
  UINT64     BootScriptPciAddress;
  //
  // Program SSID / SSVID
  //

  PciExpressWrite32 (PCI_EXPRESS_LIB_ADDRESS(Bus, Dev, Func, 0x2C), *SsidSvid);
  BootScriptPciAddress = S3_BOOT_SCRIPT_LIB_PCI_ADDRESS (Bus, Dev, Func, 0x2C);
  S3BootScriptSavePciCfgWrite (
      S3BootScriptWidthUint32, 
      BootScriptPciAddress,
      1, 
      SsidSvid);

  return EFI_SUCCESS;
}
#endif
//[-end-210909-TAMT000007-add]//

/**
  The OemSvc is used to update SSID/SVID value by OEM.

  @param[in]      Bus                   Bus number.
  @param[in]      Dev                   Device number.
  @param[in]      Func             	    Function number. 
  @param[in]      VendorID              Vendor ID.
  @param[in]      DeviceID              Device ID.
  @param[in]      ClassCode             Class Code.
  @param[in out]  SsidSvid              Pointer to SSID/SVID.

  @retval     EFI_UNSUPPORTED       Returns unsupported by default.
  @retval     EFI_SUCCESS           OEM handled SSID/SVID programming on this PCI device. Skip default kernel
                                    programming mechanism. 
  @retval     EFI_MEDIA_CHANGED     Updated SsidSvid value and returned this value for default programming mechanism.
**/
EFI_STATUS
OemSvcUpdateSsidSvidInfo (
  IN     UINT8     Bus,
  IN     UINT8     Dev,
  IN     UINT8     Func,
  IN     UINT16    VendorId,
  IN     UINT16    DeviceId,
  IN     UINT16    ClassCode,
  IN OUT UINT32   *SsidSvid
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
#if 0
// Sample code for OEM project.
// The code is for BIOS to configure SSID&SSVID for NVIDA/AMD add-in card.
// The address of SSID&SSVID is different with normal PCIE device's address 0x2C.
  UINT8       Index;
  EFI_STATUS  Status;

  Status = EFI_UNSUPPORTED;
  for (Index = 0; SsidTable[Index].SpecialSsidSvidFunction != NULL; Index++ ) {
    if (SsidTable[Index].VendorId == VendorId) {
      if ((SsidTable[Index].DeviceId == DEVICE_ID_DONT_CARE)
        || (SsidTable[Index].DeviceId == DeviceId)){
        Status = SsidTable[Index].SpecialSsidSvidFunction (Bus, Dev, Func, SsidSvid);
        break;
      }
    }
  }

  return Status;
#endif

#if defined(C970_SUPPORT)
  //EFI_STATUS  Status = EFI_MEDIA_CHANGED;
  UINT16       Count              = 0;
  UINT16       OemSsidTableSize   = 0;
  SSID_TABLE_STRU   *OemSsidTable = NULL;

  OemSsidTableSize = sizeof(YogaC970_SsidTable)/sizeof(SSID_TABLE_STRU);
  OemSsidTable     = YogaC970_SsidTable;
  for (Count = 0; Count < OemSsidTableSize; Count++) {
    if ((Dev == OemSsidTable[Count].Dev) && (Func == OemSsidTable[Count].Fun)) {
      *SsidSvid = OemSsidTable[Count].SsidSvid;
      
    }
    /*
    if (VendorId == OemSsidTable[Count].Vid) {
      if (DeviceId == OemSsidTable[Count].Did) {
        if (VendorId != INTEL_VID){
          *SsidSvid = OemSsidTable[Count].SsidSvid;
           break;
        }
        if ((Dev == OemSsidTable[Count].Dev) && (Func == OemSsidTable[Count].Fun)) {
          *SsidSvid = OemSsidTable[Count].SsidSvid;
          return EFI_MEDIA_CHANGED;
        }
      }  
    }
    */
  }    
  return EFI_MEDIA_CHANGED;

#endif
//[start-210802-STORM1103-modify]
#if defined(C770_SUPPORT)
    EFI_STATUS  Status = EFI_MEDIA_CHANGED;
//    UINT16       Count              = 0;
//    UINT16       OemSsidTableSize   = 0;
//    SSID_TABLE_STRU   *OemSsidTable = NULL;
    UINT8               UmaAndDisType;

    if (VendorId == INTEL_VID) {
      if ((DeviceId == INTEL_DID_46A0) || (DeviceId == INTEL_DID_46A1) || (DeviceId == INTEL_DID_46A6) || (DeviceId == INTEL_DID_46A8)) {
        OemSvcLfcGetBoardID (GPU_ID, &UmaAndDisType);
        if(GPU_ID_UMA_ONLY == UmaAndDisType) {
          *SsidSvid = INTEL_IGPU_UMA;
        } else {
          *SsidSvid = INTEL_IGPU_DIS;
        }
        return EFI_MEDIA_CHANGED;
      }
    }

//    OemSsidTableSize = sizeof(YogaC770_SsidTable)/sizeof(SSID_TABLE_STRU);
//    OemSsidTable     = YogaC770_SsidTable;
//    for (Count = 0; Count < OemSsidTableSize; CounMMB  ++) {
//      if ((Dev == OemSsidTable[Count].Dev) && (Func == OemSsidTable[Count].Fun)) {
//        *SsidSvid = OemSsidTable[Count].SsidSvid;
//        
//      }
//      if (VendorId == OemSsidTable[Count].Vid) {
//        if (DeviceId == OemSsidTable[Count].Did) {
//          if (VendorId != INTEL_VID){
//            *SsidSvid = OemSsidTable[Count].SsidSvid;
//             break;
//          }
//          if ((Dev == OemSsidTable[Count].Dev) && (Func == OemSsidTable[Count].Fun)) {
//            *SsidSvid = OemSsidTable[Count].SsidSvid;
//            return EFI_MEDIA_CHANGED;
//          }
//        }  
//      }
//    }

    if((VendorId == INTEL_VID) && (DeviceId == INTEL_DG2_DID)) {
      *SsidSvid = INTEL_IGPU_DIS;
      Status = ProgramSsidSvid0xFFFF10DE(Bus, Dev, Func, SsidSvid);
    }

    return EFI_MEDIA_CHANGED;

#endif
//[end-210802-STORM1103-modify]

//[-start-210817-DABING0002-modify]//
//[-start-210909-TAMT000007-add]//
//[-start-211217-Ching000019-modify]//
#ifdef S77014_SUPPORT
//LCFC Todo for S770-14
  EFI_STATUS                    Status;
  SA_SETUP                      SaSetup;  
  UINTN                         VariableSize;
  UINT32                        BoardID12;
  UINT32	                    BoardID13;

  GpioGetInputValue (GPIO_VER2_LP_GPP_F15, &BoardID12);
  GpioGetInputValue (GPIO_VER2_LP_GPP_F16, &BoardID13);

  if((VendorId == NVIDIA_VID) || (DeviceId == INTEL_VGA_DID_46A0) || (DeviceId == INTEL_VGA_DID_46A6)){ 
    VariableSize = sizeof (SA_SETUP);
    //
    // Get setup variable
    //
    Status = gRT->GetVariable (
                   L"SaSetup",
                   &gSaSetupVariableGuid,
                   NULL,
                   &VariableSize,
                   &SaSetup
                   );

    if ((BoardID12 == 0) && (BoardID13 == 1) && (SaSetup.PrimaryDisplay == 4)) {    //Dis GN18S
      *SsidSvid = S77014_GN18S_SSIDSVID;
    } else if ((BoardID12 == 1) && (SaSetup.PrimaryDisplay == 4)) {    //Dis GN20S
        if (BoardID13 == 0) {    //GN20-S5
          *SsidSvid = S77014_GN20_S5_SSIDSVID;
        } else if (BoardID13 == 1) {    //GN20-S7
            *SsidSvid = S77014_GN20_S7_SSIDSVID;
        }
    } else {
        if (DeviceId == INTEL_VGA_DID_46A0) {    //UMA-46A0
          *SsidSvid = S77014_INTEL_VGA_UMA_ONLY_SSIDSVID_46A0;
        } else if (DeviceId == INTEL_VGA_DID_46A6) {    //UMA-46A6
            *SsidSvid = S77014_INTEL_VGA_UMA_ONLY_SSIDSVID_46A6;
        }
    }
    
    if(((VendorId == NVIDIA_VID) && (DeviceId == NVIDIA_GN18S_DID)) || ((VendorId == NVIDIA_VID) && (DeviceId == NVIDIA_GN20_S5_DID)) || ((VendorId == NVIDIA_VID) && (DeviceId == NVIDIA_GN20_S7_DID))) {
      Status = ProgramSsidSvid0xFFFF10DE(Bus, Dev, Func, SsidSvid); 
    }
  }  
  return EFI_MEDIA_CHANGED;
#endif
//[-end-211217-Ching000019-modify]//
//[-end-210909-TAMT000007-add]//
//[-end-210817-DABING0002-modify]//

//[-start-220318-Ching000036-modify]//
#ifdef S77014IAH_SUPPORT
//LCFC Todo for S770-14IAH
  EFI_STATUS                    Status;
  SA_SETUP                      SaSetup;  
  UINTN                         VariableSize;
  UINT32                        BoardID12;
  UINT32	                    BoardID13;

  GpioGetInputValue (GPIO_VER2_LP_GPP_F15, &BoardID12);
  GpioGetInputValue (GPIO_VER2_LP_GPP_F16, &BoardID13);

  if((VendorId == NVIDIA_VID) || (DeviceId == INTEL_VGA_DID_46A3) || (DeviceId == INTEL_VGA_DID_46A6)){ 
    VariableSize = sizeof (SA_SETUP);
    //
    // Get setup variable
    //
    Status = gRT->GetVariable (
                   L"SaSetup",
                   &gSaSetupVariableGuid,
                   NULL,
                   &VariableSize,
                   &SaSetup
                   );

    if ((BoardID12 == 1) && (BoardID13 == 0) && (SaSetup.PrimaryDisplay == 4)) {    //Dis GN18-S5
      *SsidSvid = S77014IAH_GN18_S5_SSIDSVID;
    } else if ((BoardID12 == 1) && (BoardID13 == 1) && (SaSetup.PrimaryDisplay == 4)) {    //Dis GN20-S7
        *SsidSvid = S77014IAH_GN20_S7_SSIDSVID;
    } else {    //UMA 46A3 & 46A6
        *SsidSvid = S77014IAH_INTEL_VGA_UMA_ONLY_SSIDSVID;
    }
    
    if(((VendorId == NVIDIA_VID) && (DeviceId == NVIDIA_GN18_S5_DID)) || ((VendorId == NVIDIA_VID) && (DeviceId == NVIDIA_GN20_S7_DID))) {
      Status = ProgramSsidSvid0xFFFF10DE(Bus, Dev, Func, SsidSvid); 
    }
  }  
  return EFI_MEDIA_CHANGED;
#endif
//[-end-220318-Ching000036-modify]//

//[-start-210914-DABING0006-modify]//
//[-start-210925-TAMT000014-add]//
#if defined(S77013_SUPPORT)
//LCFC Todo for S770-13
  UINT16       Count              = 0;
  UINT16       OemSsidTableSize   = 0;
  SSID_TABLE_STRU   *OemSsidTable = NULL;

  OemSsidTableSize = sizeof(S77013_SsidTable)/sizeof(SSID_TABLE_STRU);
  OemSsidTable     = S77013_SsidTable;
  for (Count = 0; Count < OemSsidTableSize; Count++) {
    if ((Dev == OemSsidTable[Count].Dev) && (Func == OemSsidTable[Count].Fun)) {
      *SsidSvid = OemSsidTable[Count].SsidSvid;
      
    }
  }
  return EFI_MEDIA_CHANGED;
//[-end-210925-TAMT000014-add]//
#endif
//[-end-210914-DABING0006-modify]//

//[-start-210723-QINGLIN0001-add]//
//[-start-211015-QINGLIN0100-modify]//
//[-start-211025-QINGLIN0103-modify]//
#if defined(S570_SUPPORT)
  EFI_STATUS          Status;
  SA_SETUP            SaSetup;  
  UINTN               VariableSize;
  UINT8               UmaAndDisType;

  Status  = EFI_UNSUPPORTED;

  if ((DeviceId == INTEL_VGA_DID_46A0) || (DeviceId == INTEL_VGA_DID_46A6)) { //U28 CPU only have UMA SKU
    *SsidSvid = INTEL_IGPU_UMA;
    return EFI_MEDIA_CHANGED;
  } 
//[-start-220413-JEPLIUT220-modify]//  Add I5s I3s CPU DCL
//  if((VendorId == NVIDIA_VID) || (DeviceId == INTEL_VGA_DID_46A8)) {
  	if ((VendorId == NVIDIA_VID) || (DeviceId == INTEL_VGA_DID_46A8) || (DeviceId == INTEL_VGA_DID_4626) || (DeviceId == INTEL_VGA_DID_4628) || (DeviceId == INTEL_VGA_DID_46B3)) {
//[-start-220413-JEPLIUT220-modify]//      
    OemSvcLfcGetBoardID (GPU_ID, &UmaAndDisType);
    
    VariableSize = sizeof (SA_SETUP);
    //
    // Get setup variable
    //
    Status = gRT->GetVariable (
                  L"SaSetup",
                  &gSaSetupVariableGuid,
                  NULL,
                  &VariableSize,
                  &SaSetup
                  );
    if ((UmaAndDisType != GPU_ID_UMA_ONLY) && (SaSetup.PrimaryDisplay == 4)) {  //DIS SKU and switchable config
      LfcGetSSIDbyGraphicDeviceId (SsidSvid);
    } else {
      *SsidSvid = INTEL_IGPU_UMA;
    }

    Status = EFI_MEDIA_CHANGED;

    if(VendorId == NVIDIA_VID) {
      Status = ProgramSsidSvid0xFFFF10DE(Bus, Dev, Func, SsidSvid);
    }
  }

  return Status;
#endif
//[-end-211025-QINGLIN0103-modify]//
//[-end-211015-QINGLIN0100-modify]//
//[-end-210723-QINGLIN0001-add]//

//[-start-210804-QINGLIN0008-add]//
#if defined(S370_SUPPORT)
  EFI_STATUS          Status;
  UINT8               UmaAndDisType;

  Status  = EFI_UNSUPPORTED;
//[-start-211019-Xinwei003-Modify]//
//[-start-220217-QINGLIN0159-modify]//
  if(VendorId == INTEL_VID) {
//[-start-220125-QINGLIN0152-modify]//
    if ((DeviceId == INTEL_VGA_DID_46B3) || (DeviceId == INTEL_VGA_DID_46A8)  || (DeviceId == INTEL_VGA_DID_4628) || (DeviceId == INTEL_DG2_SKU6_DID)) {
//[-end-220125-QINGLIN0152-modify]//
      OemSvcLfcGetBoardID (GPU_ID, &UmaAndDisType);
      if (GPU_ID_UMA_ONLY == UmaAndDisType) { //UMA
        *SsidSvid = INTEL_VGA_UMA_SSID;
        Status = EFI_MEDIA_CHANGED;
      } else { //DIS
        *SsidSvid = INTEL_VGA_DIS_SSID;
        Status = EFI_MEDIA_CHANGED;
      }
    }
  }
//[-end-220217-QINGLIN0159-modify]//
//[-end-211019-Xinwei003-Modify]//

//[-start-211110-QINGLIN0111-add]//
#if defined(BAYHUB_CARD_READER_SUPPORT)
  if ((VendorId == BayHub_Card_Reader_VID) && (DeviceId == BayHub_Card_Reader_DID)) {
      Status = CheckAndProgramBayHubCardReader (VendorId, DeviceId, Bus, Dev, Func);
  }
#endif

#if defined(GENESYS_LOGIC_CARD_READER_SUPPORT)
  if ((VendorId == GenesysLogic_Card_Reader_VID) && (DeviceId == GenesysLogic_Card_Reader_DID)) {
      Status = CheckAndProgramGenesysLogicCardReader (VendorId, DeviceId, Bus, Dev, Func);
  }
#endif
//[-end-211110-QINGLIN0111-add]//

  return Status;
#endif
//[-end-210804-QINGLIN0008-add]//


  //return Status;
//[-end-210610-Dongxu0001-Modify]
//[-end-210623-Dongxu0003-Modify]
}
