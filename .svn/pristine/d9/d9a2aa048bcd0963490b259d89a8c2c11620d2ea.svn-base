/** @file

;******************************************************************************
;* Copyright (c) 2017 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CHIPSET_SMI_TABLE_DEFINE_H_
#define _CHIPSET_SMI_TABLE_DEFINE_H_


typedef enum {
  //
  // Kernel must use 0x01~0x4F as SW_SMI command number. Register on SmiTable.h
  //
  //
  // OEM must use 0xc0~0xDF as SW_SMI command number. Register on OemSwSmi.h
  //
  //
  // 0xE0 reserved for ODM Usage
  //
  INT15_HOOK                                      = 0x15,
  //
  // Write PDR Command
  //
  Write_PDR_SW_SMI                                = 0x51,
  //
  // Initial Cipher for S3
  //
  Re_Init_Cipher                                  = 0x52,
  Return_PDR0_Data                                = 0x53,

  SMMBASE_SW_SMI                                  = 0x55,


  ASF_SECURE_BOOT_SMI                             = 0x56,

  //
  EFI_PEP_BCCD_SW_SMI                             = 0x58,
  //  XTU SMI
  //
  EFI_PERF_TUNE_SW_SMI                            = 0x72,
  INT10_HOOK_FOR_INTEL_VBIOS                      = 0x74,
  //
  // Nvidia Optimus _ON/_OFF save/restore SMI
  //
  OPTIMUS_SAVE_PEG_REGISTER                       = 0x7A,
  OPTIMUS_RESTORE_PEG_REGISTER                    = 0x7B,
  OPTIMUS_SET_DISPLAY_MODE                        = 0x7C,
  //
  // OS_COMMAND
  //
  PPM_OS_COMMAND                                  = 0x80,
  //
  // INIT_COMMAND
  //
  PPM_INIT_COMMAND                                = 0x81, //no function
  //
  // APP_COMMAND
  //
  PPM_APP_COMMAND                                 = 0x82,
  //
  // OS_REQUEST
  //
  PPM_OS_REQUEST                                  = 0x83, //no function
  //
  // CSTATE_COMMAND
  //
  PPM_CSTATE_COMMAND                              = 0x85, //no function
  //
  // IST_INIT_OS_TRANSITION
  //
  PPM_IST_INIT_OS_TRANSITION                      = 0x86,
//
// SEG Feature - Remove H2OUVE relevant source codes
//
//  //
//  //  For Variable Editor Tool
//  //
//  IVE_SW_SMI                                      = 0x97,

  PPM_INTERNAL_SW_SMI                             = 0xE4,
  S3_RESTORE_MSR_SW_SMI                           = 0xE5,
  POWER_STATE_SWITCH_SMI                          = 0xE6,
  ENABLE_C_STATE_IO_REDIRECTION_SMI               = 0xE7,
  DISABLE_C_STATE_IO_REDIRECTION_SMI              = 0xE8,
  ENABLE_P_STATE_HARDWARE_COORDINATION_SMI        = 0xE9,
  DISABLE_P_STATE_HARDWARE_COORDINATION_SMI       = 0xEA,
  BIOS_LOCK_SW_SMI                                = 0xEB,
  //
  // Thunderbolt SMI Handler
  //
  THUNDERBOLT_SW_SMI                              = 0xF1,
  AMT_FLASH_SW_SMI                                = 0xF2,
  AMT_FLASH_WRITE_PROTECT_POST_COMPLETE_SW_SMI    = 0xF3,

  //
  // Smm platform
  //
  PCIE_HOT_PLUG_SMI                               = 0xF4,
  ME_LOCK_SW_SMI                                  = 0xF5,

  I_TBT_ENUMRATE                                  = 0xF6, //Reserved for PcdSwSmiITbtEnumerate
  D_TBT_ENUMRATE                                  = 0xF7  //Reserved for PcdSwSmiDTbtEnumerate

}CHIPSET_SW_SMI_PORT_TABLE;

#endif

