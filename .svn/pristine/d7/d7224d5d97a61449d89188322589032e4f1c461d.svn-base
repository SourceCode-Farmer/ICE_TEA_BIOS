/** @file
  USB4 router management library header.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_ROUTER_LIB_H_
#define _USB4_ROUTER_LIB_H_

#include <Uefi.h>
#include <Usb4ConfigLayer.h>
#include <Usb4CsIo.h>
#include <Usb4RouterDefs.h>
#include <Usb4Capabilities.h>
#include <Usb4DomainDefs.h>

/**
  Generic USB4 router instance creation

  @param[in]  TopologyId     - Pointer to Router Topology ID.
  @param[in]  Depth          - Router depth.
  @param[in]  DomainContext  - USB4 domain context.
  @param[out] RouterInstance - Pointer of pointer to the created USB4 Router instance.

  @retval EFI_SUCCESS           - Create USB4 router instance successfully.
  @retval EFI_OUT_OF_RESOURCES  - Insufficient resources to create USB4 router instance.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4RtCreate (
  IN  PTOPOLOGY_ID          TopologyId,
  IN  UINT8                 Depth,
  IN  USB4_DOMAIN_CONTEXT   DomainContext,
  OUT USB4_ROUTER           **RouterInstance
  );

/**
  Generic USB4 router destroy.

  @param[in] Router - Pointer to USB4 Router instance.

  @retval EFI_SUCCESS     - USB4 Router destroy successfully.
  @retval EFI_UNSUPPORTED - Downstream router is still present.
**/
EFI_STATUS
Usb4RtDestroy (
  IN PUSB4_ROUTER   Router
  );

/**
  Enumerate USB4 device router and initialize the router instance based on Topology ID.

  @param[in,out] Router  - Pointer to Router instance.

  @retval EFI_SUCCESS           - Router initialization success.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval Errors in config space access
**/
EFI_STATUS
Usb4DevRtInit (
  IN OUT PUSB4_ROUTER    Router
  );

/**
  Enumerate the Router.
  Write Topology ID and Topology ID Valid bit to Router Configuration Space.
  Router will enter enumerated state.

  @param[in] Router - Pointer to Router instance.

  @retval EFI_SUCCESS           - Write Topology ID and valid bit successfully.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval Errors in config space access
**/
EFI_STATUS
Usb4RtEnumerate (
  IN PUSB4_ROUTER  Router
  );

/**
  Scan router adapters and find the connectivity between Lane adapter and PCIe/USB3 adapter.

  @param[in] Router  - Pointer to Router instance.

  @retval EFI_SUCCESS           - Scan router adapters successfully.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval Errors in config space access
**/
EFI_STATUS
Usb4RtScanAdapters (
  IN OUT PUSB4_ROUTER    Router
  );

/**
  Enumerate downstream Routers of a specific Router and add to Domain Topology

  @param[in] Router      - Pointer to a Router instance that needs downstream enumeration

  @retval EFI_SUCCESS           - Enumerate and add downstream Routers success
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval Errors in config space access.
**/
EFI_STATUS
Usb4RtEnumAllDsRouters (
  IN USB4_ROUTER     *Router
  );

/**
  Enumerate the downstream Router and all of its downstream Routers at the specified Lane adapter.

  @param[in] Router      - Pointer to a Router instance that needs downstream enumeration
  @param[in] AdpNum      - Lane Adapter number for enumerating the downstream Router

  @retval EFI_SUCCESS           - Enumerate and add downstream Routers success.
  @retval EFI_UNSUPPORTED       - Downstream router enumeration can't be supported.
  @retval EFI_NOT_FOUND         - Downstream router is not found at the specified adapter.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval Errors in config space access.
**/
EFI_STATUS
Usb4RtEnumDsRouter (
  IN PUSB4_ROUTER    Router,
  IN UINT8           AdpNum
  );

/**
  Disconnect the downstream Routers connected at the given adapter from domain.

  @param[in] Router - Pointer to USB4 router instance.
  @param[in] AdpNum - Adapter number.

  @retval EFI_SUCCESS           - Disconnect and remove the downstream routers from domain successfully.
  @retval EFI_UNSUPPORTED       - Disconnect the downstream router failure.
  @retval EFI_INVALID_PARAMETER - Disconnect and remove downstream Routers from Domain Topology failure.
**/
EFI_STATUS
Usb4RtDsDisconnect (
  IN PUSB4_ROUTER    Router,
  IN UINT8           AdpNum
  );

/**
  Set Router attributes in Router CS 5

  @param[in] Router       - Pointer to Router instance.
  @param[in] Attrib       - Router attribute to be set.
  @param[in] Enable       - Indicate attribute enable/disable.

  @retval EFI_SUCCESS           - Set Router attributes successfully.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
  @retval Errors in config space access.
**/
EFI_STATUS
Usb4RtSetAttributes (
  IN PUSB4_ROUTER    Router,
  IN UINT32          Attrib,
  IN BOOLEAN         Enable
  );

/**
  Wait for Router status bits of Router CS 6

  @param[in] Router     - Pointer to Router instance.
  @param[in] WaitFlag   - Router status flags for waiting.

  @retval EFI_SUCCESS   - All specified status bits are set.
  @retval EFI_TIMEOUT   - Not all specified status bits are set after a timeout period.
  @retval Errors in config space access.
**/
EFI_STATUS
Usb4RtWaitForStatus (
  IN PUSB4_ROUTER    Router,
  IN UINT32          WaitFlags
  );

/**
  Check if a Router is in a Router tree starting from the specified root.

  @param[in] Router - Pointer to the downstream Router.
  @param[in] Root   - Pointer to the tree root

  @retval TRUE  - Router is in the tree.
  @retval FALSE - Router is not in the tree.
**/
BOOLEAN
Usb4RtInRtTree (
  IN PUSB4_ROUTER     Router,
  IN PUSB4_ROUTER     Root
  );

/**
  Dump DP-IN adapter capability.

  @param[in] DpInCap - Pointer to DP-IN adapter capability.
**/
VOID
DumpAdpDpInCap (
  IN PUSB4_ADP_CAP_DP_IN    DpInCap
  );

/**
  Dump DP-OUT adapter capability.

  @param[in] DpOutCap - Pointer to DP-OUT adapter capability.
**/
VOID
DumpAdpDpOutCap (
  IN PUSB4_ADP_CAP_DP_OUT    DpOutCap
  );

/**
  Dump path entry

  @param[in] PathEntry - Pointer to the path entry
**/
VOID
DumpPathEntry (
  IN PUSB4_PATH_CS_ENTRY  PathEntry
  );

#endif
