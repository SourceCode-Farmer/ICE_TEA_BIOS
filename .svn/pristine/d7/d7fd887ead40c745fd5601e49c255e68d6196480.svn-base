/** @file
  CPU/Chipset/Platform Initial depends on project characteristic.

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <ChipsetSetupConfig.h>
#include <Library/PeiOemSvcKernelLib.h>
#include <Library/PeiInsydeChipsetLib.h>
//_Start_L05_LEGACY_TO_UEFI_
#include <Guid/L05StopLegacyToEfiProcess.h>
#include <Library/PeiServicesLib.h>
#include <Library/FeatureLib/OemSvcStopLegacyToEfiProcess.h>
#include <Library/HobLib.h>
//_End_L05_LEGACY_TO_UEFI_
//[-start-220224-TAMT000046-add]//
#ifdef S77013_SUPPORT
#ifdef L05_ALL_FEATURE_ENABLE
#include <L05Hook/GetNovoButtonStatus.h>
#endif
#endif
//[-end-220224-TAMT000046-add]//
//_Start_L05_PREFIX_CHIPSET_ReInstallStallPpi_
#include <Ppi/Stall.h>
#include <Library/TimerLib.h>
#define PEI_STALL_RESOLUTION   1

/**
  This function provides a blocking stall for reset at least the given number of microseconds
  stipulated in the final argument.

  @param  PeiServices General purpose services available to every PEIM.
  @param  this Pointer to the local data for the interface.
  @param  Microseconds number of microseconds for which to stall.

  @retval  EFI_SUCCESS the function provided at least the required stall.
**/
EFI_STATUS
EFIAPI
Stall (
  IN CONST EFI_PEI_SERVICES             **PeiServices,
  IN CONST EFI_PEI_STALL_PPI            *This,
  IN UINTN                              Microseconds
  );


EFI_PEI_STALL_PPI                       mStallPpi = {
  PEI_STALL_RESOLUTION,
  Stall
};

EFI_PEI_PPI_DESCRIPTOR                  mPeiInstallStallPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiStallPpiGuid,
  &mStallPpi
};

EFI_STATUS
EFIAPI
Stall (
  IN CONST EFI_PEI_SERVICES             **PeiServices,
  IN CONST EFI_PEI_STALL_PPI            *This,
  IN UINTN                              Microseconds
  )
{
  MicroSecondDelay (Microseconds);
  return EFI_SUCCESS;
}

/**
  This function will reinstall the StallPpi.

  @retval EFI_SUCCESS                   If StallPpi is reinstalled successfully.
**/
EFI_STATUS
EFIAPI
ReInstallStallPpi (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_PEI_PPI_DESCRIPTOR                *StallPeiPpiDescriptor;
  EFI_PEI_STALL_PPI                     *StallPpi;

  Status = PeiServicesLocatePpi (
             &gEfiPeiStallPpiGuid,
             0,
             &StallPeiPpiDescriptor,
             (VOID **) &StallPpi
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = PeiServicesReInstallPpi (
             StallPeiPpiDescriptor,
             &mPeiInstallStallPpi
             );

  return Status;
}
//_End_L05_PREFIX_CHIPSET_ReInstallStallPpi_

/**
  CPU/Chipset/Platform Initial depends on project characteristic.

  @param[in]  *Buffer               A pointer to CHIPSET_CONFIGURATION.
  @param[in]  SetupVariableExist    Setup variable be found in variable storage or not.

  @retval     EFI_UNSUPPORTED       Returns unsupported by default.
  @retval     EFI_SUCCESS           The service is customized in the project.
  @retval     EFI_MEDIA_CHANGED     The value of IN OUT parameter is changed. 
  @retval     Others                Depends on customization.
**/
EFI_STATUS
OemSvcInitPlatformStage2 (
  IN  VOID                                 *Buffer,
  IN  BOOLEAN                              SetupVariableExist
  )
{
  /*++
    Tips for programmer at Project layer:
  
//  SYSTEM_CONFIGURATION                  *SystemConfiguration;
//  SystemConfiguration = (SYSTEM_CONFIGURATION *)Buffer;

    Todo:
      Add project specific code in here.
  --*/
  
//_Start_L05_LEGACY_TO_UEFI_
#ifdef L05_ALL_FEATURE_ENABLE
  EFI_STATUS                            Status;
  EFI_BOOT_MODE                         BootMode;
#endif
//_End_L05_LEGACY_TO_UEFI_

//[-start-220224-TAMT000046-add]//
#ifdef S77013_SUPPORT
#ifdef L05_ALL_FEATURE_ENABLE
  GetNovoButtonStatus ();
#endif
#endif
//[-end-220224-TAMT000046-add]//

//_Start_L05_LEGACY_TO_UEFI_
#ifdef L05_ALL_FEATURE_ENABLE
  Status = PeiServicesGetBootMode (&BootMode);

  //
  // Skip during S3 Resume for reducing S3 resume time
  // Skip in Recovery Mode for unnecessary
  //
  if (BootMode != BOOT_ON_S3_RESUME && BootMode != BOOT_IN_RECOVERY_MODE) {

    Status = OemSvcStopLegacyToEfiProcess ();

    if (Status == EFI_MEDIA_CHANGED) {

      //
      // Set stop LegacyToEfi requirement to DXE phase
      //
      BuildGuidDataHob ((CONST EFI_GUID *) &gL05StopLegacyToEfiProcessGuid, NULL, 0);
    }
  }
#endif
//_End_L05_LEGACY_TO_UEFI_

//_Start_L05_PREFIX_CHIPSET_ReInstallStallPpi_
  ReInstallStallPpi ();
//_End_L05_PREFIX_CHIPSET_ReInstallStallPpi_

  return EFI_SUCCESS;
}

