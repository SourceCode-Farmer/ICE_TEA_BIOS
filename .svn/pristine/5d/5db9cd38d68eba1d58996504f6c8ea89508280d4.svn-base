/** @file
  This Library will install CRVfrConfigLib for reference.

;******************************************************************************
;* Copyright (c) 2019 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
 Intel PEI CPU Policy update by board configuration

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include "PeiPolicyBoardConfig.h"
#include <Library/Tpm2CommandLib.h>
#include <Library/Tpm12CommandLib.h>
#include <PolicyUpdateMacro.h>
//[-start-181025-IB15590141-add]//
#include <Library/H2OCpLib.h>
#include <Guid/H2OCpChipset.h>
#include <Library/PcdLib.h>
//[-end-181025-IB15590141-add]//
//[-start-201021-IB11790397-add]//
#include <Library/MemoryAllocationLib.h>
//[-end-201021-IB11790397-add]//

/**
  This function is for platform code to provide EC Commands since different BIOS might have different EC.
  Platform code need to provide a function for CPU code to call to communicate with EC.

  @param[in] EcCmdType      - EC Command Type.
  @param[in] EcCmd          - EC Command Byte to send.
  @param[in] SendData       - EC Data Byte to send.
  @param[in] ReceiveData    - EC Data Byte received.

  @retval EFI_SUCCESS          Command Read/ Write Success
  @retval EFI_DEVICE_ERROR     Command Read/ Write Error
  @retval EFI_OUT_OF_RESOURCES No enough resources (such as out of memory).
**/
EFI_STATUS
PlatformSendEcCommand (
  IN EC_COMMAND_TYPE             EcCmdType,
  IN UINT8                       EcCmd,
  IN UINT8                       SendData,
  IN OUT UINT8                   *ReceiveData
  )
{
  EFI_STATUS Status;

  switch (EcCmdType) {
  case SendEcCmds:
    ///
    /// Sends command to Embedded controller.
    ///
    Status = SendEcCommand(EcCmd);
    break;
  case SendEcValue:
    ///
    /// Sends data to Embedded controller.
    ///
    Status = SendEcData(SendData);
    break;
  case ReceiveEcValue:
    ///
    /// Receives data from Embedded controller.
    ///
    Status = ReceiveEcData(ReceiveData);
    break;
  default:
    return EFI_DEVICE_ERROR;
  }

  return Status;
}

//[-start-201021-IB11790397-add]//
VOID
DumpBiosGuardModule (
  IN  BIOSGUARD_HOB               *BiosGuardHobPtr
  )
{
 UINT32                           Index;

  DEBUG ((DEBUG_INFO, "-------------- Checkpoint Updated Result --------------\n"));
  DEBUG ((DEBUG_INFO, "------------------ BIOS Guard Module ------------------\n"));
  DEBUG ((DEBUG_INFO, " BIOS Guard Module:: Product ID : 0x%X\n", *(UINT16*)(UINTN)(BiosGuardHobPtr->BiosGuardModulePtr + BIOSGUARD_PRODUCT_ID_OFFSET)));
  DEBUG ((DEBUG_INFO, " BIOS Guard Module:: Build Number : 0x%X\n", *(UINT16*)(UINTN)(BiosGuardHobPtr->BiosGuardModulePtr + BIOSGUARD_BUILD_NUMBER_OFFSET)));

  DEBUG ((DEBUG_INFO, "------------------ BIOS GUARD Config ------------------\n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : Version : 0x%X\n", BiosGuardHobPtr->BgupHeader.Version));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : PlatId[16] :\n"));
  for (Index = 0; Index < 16; Index++) {
      DEBUG ((DEBUG_INFO, " 0x%X", BiosGuardHobPtr->BgupHeader.PlatId[Index]));
  }

  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : PkgAttributes : 0x%X\n", BiosGuardHobPtr->BgupHeader.PkgAttributes));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : PslMajorVer : 0x%X\n", BiosGuardHobPtr->BgupHeader.PslMajorVer));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : PslMinorVer : 0x%X\n", BiosGuardHobPtr->BgupHeader.PslMinorVer));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : ScriptSectionSize : 0x%X\n", BiosGuardHobPtr->BgupHeader.ScriptSectionSize));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : DataSectionSize : 0x%X\n", BiosGuardHobPtr->BgupHeader.DataSectionSize));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : BiosSvn : 0x%X\n", BiosGuardHobPtr->BgupHeader.BiosSvn));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : EcSvn : 0x%X\n", BiosGuardHobPtr->BgupHeader.EcSvn));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : VendorSpecific : 0x%X\n", BiosGuardHobPtr->BgupHeader.VendorSpecific));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BgpdtSize : 0x%X\n", BiosGuardHobPtr->Bgpdt.BgpdtSize));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BgpdtMajVer : 0x%X\n", BiosGuardHobPtr->Bgpdt.BgpdtMajVer));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BgpdtMinVer : 0x%X\n", BiosGuardHobPtr->Bgpdt.BgpdtMinVer));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : PlatId[16] :\n"));
  for (Index = 0; Index < 16; Index++) {
      DEBUG ((DEBUG_INFO, " 0x%X", BiosGuardHobPtr->Bgpdt.PlatId[Index]));
  }

  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : PkeySlot0[32] :\n"));
  for (Index = 0; Index < 32; Index++) {
    if ((Index == 15) || (Index == 31)) {
      DEBUG ((DEBUG_INFO, " 0x%X\n", BiosGuardHobPtr->Bgpdt.PkeySlot0[Index]));
    } else {
      DEBUG ((DEBUG_INFO, " 0x%X", BiosGuardHobPtr->Bgpdt.PkeySlot0[Index]));
    }
  }

  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : PkeySlot1[32] :\n"));
  for (Index = 0; Index < 32; Index++) {
    if ((Index == 15) || (Index == 31)) {
      DEBUG ((DEBUG_INFO, " 0x%X\n", BiosGuardHobPtr->Bgpdt.PkeySlot1[Index]));
    } else {
      DEBUG ((DEBUG_INFO, " 0x%X", BiosGuardHobPtr->Bgpdt.PkeySlot1[Index]));
    }
  }

  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : PkeySlot2[32] :\n"));
  for (Index = 0; Index < 32; Index++) {
    if ((Index == 15) || (Index == 31)) {
      DEBUG ((DEBUG_INFO, " 0x%X\n", BiosGuardHobPtr->Bgpdt.PkeySlot2[Index]));
    } else {
      DEBUG ((DEBUG_INFO, " 0x%X", BiosGuardHobPtr->Bgpdt.PkeySlot2[Index]));
    }
  }

  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BgModSvn : 0x%X\n", BiosGuardHobPtr->Bgpdt.BgModSvn));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BiosSvn : 0x%X\n", BiosGuardHobPtr->Bgpdt.BiosSvn));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : ExecLim : 0x%X\n", BiosGuardHobPtr->Bgpdt.ExecLim));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BiosGuardAttr : 0x%X\n", BiosGuardHobPtr->Bgpdt.BiosGuardAttr));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcCmd : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcCmd));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcData : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcData));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcCmdGetSvn : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcCmdGetSvn));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcCmdOpen : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcCmdOpen));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcCmdClose : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcCmdClose));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcCmdPortTest : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcCmdPortTest));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : LastSfam : 0x%X\n", BiosGuardHobPtr->Bgpdt.LastSfam));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : SfamData[64] :\n"));

  //
  // Change the array size according to MAX_SFAM_COUNT
  //
  for (Index = 0; Index <= BiosGuardHobPtr->Bgpdt.LastSfam; Index++) {
    if ((Index == 15) || (Index == 31) || (Index == 47) || (Index == 63) ) {
      DEBUG ((DEBUG_INFO, " 0x%X 0x%X\n", BiosGuardHobPtr->Bgpdt.SfamData[Index].FirstByte,
                                          BiosGuardHobPtr->Bgpdt.SfamData[Index].LastByte));
    } else {
      DEBUG ((DEBUG_INFO, " 0x%X 0x%X,", BiosGuardHobPtr->Bgpdt.SfamData[Index].FirstByte,
                                         BiosGuardHobPtr->Bgpdt.SfamData[Index].LastByte));
    }
  }
  DEBUG ((DEBUG_INFO, " \n"));
}
//[-end-201021-IB11790397-add]//

/**
  This function performs PEI CPU Policy update by board configuration.

  @retval EFI_SUCCESS             The SI Policy is successfully updated.
  @retval Others                  The SI Policy is not successfully updated.
**/
EFI_STATUS
EFIAPI
UpdatePeiCpuPolicyBoardConfig (
  VOID
  )
{
  EFI_STATUS                      Status;
  VOID                            *Sha256Context;
  VOID                            *Data;
  UINTN                           DataSize;
  UINT8                           HashValue[SHA256_DIGEST_SIZE];
  BIOSGUARD_HOB                   *BiosGuardHobPtr;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                            *FspmUpd;
  VOID                            *FspsUpd;
#else
  SI_PREMEM_POLICY_PPI            *SiPreMemPolicyPpi;
  SI_POLICY_PPI                   *SiPolicyPpi;
  SA_MISC_PEI_PREMEM_CONFIG       *MiscPeiPreMemConfig;
  BIOS_GUARD_CONFIG               *BiosGuardConfig;
  CPU_SECURITY_PREMEM_CONFIG      *CpuSecurityPreMemConfig;
#endif

  DEBUG ((DEBUG_INFO, "Updating CPU Policy by board config in Post-Mem\n"));

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = NULL;
  FspsUpd = NULL;
#else
  SiPolicyPpi             = NULL;
  BiosGuardConfig         = NULL;
  SiPreMemPolicyPpi       = NULL;
  MiscPeiPreMemConfig     = NULL;
  CpuSecurityPreMemConfig = NULL;
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = (FSPM_UPD *) PcdGet32 (PcdFspmUpdDataAddress);
  ASSERT (FspmUpd != NULL);

  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR(Status);

  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicyPpi);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *)SiPreMemPolicyPpi, &gCpuSecurityPreMemConfigGuid, (VOID *) &CpuSecurityPreMemConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *)SiPreMemPolicyPpi, &gSaMiscPeiPreMemConfigGuid, (VOID *) &MiscPeiPreMemConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *)SiPolicyPpi, &gBiosGuardConfigGuid, (VOID *) &BiosGuardConfig);
  ASSERT_EFI_ERROR(Status);
#endif

  //
  // Make sure ReadOnlyVariablePpi is available
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR(Status);

#if FixedPcdGet8(PcdFspModeSelection) == 1
  if (((FSPM_UPD *) FspmUpd)->FspmConfig.BiosGuard == TRUE) {
#else
  if (CpuSecurityPreMemConfig->BiosGuard == TRUE) {
#endif
    BiosGuardHobPtr = GetFirstGuidHob (&gBiosGuardHobGuid);
    if (BiosGuardHobPtr == NULL) {
      DEBUG ((DEBUG_ERROR, "BIOS Guard HOB not available\n"));
      return EFI_NOT_FOUND;
    }

//[-start-201021-IB11790397-add]//
    if (FeaturePcdGet(PcdUseCrbEcFlag)) {
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EcCmdProvisionEav,   BiosGuardConfig->EcCmdProvisionEav, FixedPcdGet8 (PcdBiosGuardConfigEcCmdProvisionEav));
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EcCmdLock,           BiosGuardConfig->EcCmdLock,         FixedPcdGet8 (PcdBiosGuardConfigEcCmdLock));
    }

    if (FeaturePcdGet (PcdH2OPeiCpBiosGuardEcSupported) || FeaturePcdGet (PcdH2OPeiCpBiosGuardUpdateBgpdt)) {
      BGPDT        *CpBgpdt;
    
      CpBgpdt = AllocateZeroPool(sizeof(BGPDT));
      if (CpBgpdt != NULL) {
        CopyMem (CpBgpdt, &(BiosGuardHobPtr->Bgpdt), sizeof(BGPDT));

        if (PcdGetBool (PcdEcBiosGuardEnable)) { 
          H2O_PEI_CP_BIOSGUARD_EC_SUPPORT       PeiCpBiosGuardEcData;

          PeiCpBiosGuardEcData.Size                    = sizeof (H2O_PEI_CP_BIOSGUARD_EC_SUPPORT);
          PeiCpBiosGuardEcData.Status                  = H2O_CP_TASK_NORMAL;
          PeiCpBiosGuardEcData.Bgpdt                   = CpBgpdt;
 
          DEBUG_CP ((DEBUG_INFO, "Checkpoint Trigger: %g\n", &gH2OPeiCpBiosGuardEcSupportGuid));
          Status = H2OCpTrigger (&gH2OPeiCpBiosGuardEcSupportGuid, &PeiCpBiosGuardEcData);
          DEBUG_CP ((DEBUG_INFO, "Checkpoint Result: %r\n", PeiCpBiosGuardEcData.Status));

          if (PeiCpBiosGuardEcData.Status != H2O_CP_TASK_NORMAL) {
            CopyMem (&(BiosGuardHobPtr->Bgpdt), CpBgpdt, sizeof(BGPDT));
            UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EcCmdProvisionEav, BiosGuardConfig->EcCmdProvisionEav, PeiCpBiosGuardEcData.EcCmdProvisionEav);
            UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EcCmdLock,         BiosGuardConfig->EcCmdLock,         PeiCpBiosGuardEcData.EcCmdLock);

            if ((PeiCpBiosGuardEcData.Status == H2O_CP_TASK_SKIP) || (PcdGetBool (PcdEcPresent) == FALSE)) {
              BiosGuardHobPtr->Bgpdt.BiosGuardAttr &= ~(EnumEcPresent | EnumEcBiosGuardProtected);
              BiosGuardHobPtr->Bgpdt.EcCmd          = 0x0;
              BiosGuardHobPtr->Bgpdt.EcData         = 0x0;
              BiosGuardHobPtr->Bgpdt.EcCmdGetSvn    = 0x0;
              BiosGuardHobPtr->Bgpdt.EcCmdOpen      = 0x0;
              BiosGuardHobPtr->Bgpdt.EcCmdClose     = 0x0;
              BiosGuardHobPtr->Bgpdt.EcCmdPortTest  = 0x0;
              BiosGuardConfig->EcCmdProvisionEav    = 0x0;
              BiosGuardConfig->EcCmdLock            = 0x0;
              DumpBiosGuardModule (BiosGuardHobPtr);
            }
           }
        } else {
          H2O_PEI_CP_BIOSGUARD_UPDATE_BGPDT    PeiCpBiosGuardBgpdtData;
  
          PeiCpBiosGuardBgpdtData.Size                = sizeof (H2O_PEI_CP_BIOSGUARD_UPDATE_BGPDT);
          PeiCpBiosGuardBgpdtData.Status              = H2O_CP_TASK_NORMAL;
          PeiCpBiosGuardBgpdtData.Bgpdt               = CpBgpdt;
  
          DEBUG_CP ((DEBUG_INFO, "Checkpoint Trigger: %g\n", &gH2OPeiCpBiosGuardUpdateBgpdtGuid));
          Status = H2OCpTrigger (&gH2OPeiCpBiosGuardUpdateBgpdtGuid, &PeiCpBiosGuardBgpdtData);
          DEBUG_CP ((DEBUG_INFO, "Checkpoint Result: %r\n", PeiCpBiosGuardBgpdtData.Status));

          if (PeiCpBiosGuardBgpdtData.Status == H2O_CP_TASK_UPDATE) {
            CopyMem (&(BiosGuardHobPtr->Bgpdt), CpBgpdt, sizeof(BGPDT));

            BiosGuardHobPtr->Bgpdt.BiosGuardAttr &= ~(EnumEcPresent | EnumEcBiosGuardProtected);
            BiosGuardHobPtr->Bgpdt.EcCmd          = 0x0;
            BiosGuardHobPtr->Bgpdt.EcData         = 0x0;
            BiosGuardHobPtr->Bgpdt.EcCmdGetSvn    = 0x0;
            BiosGuardHobPtr->Bgpdt.EcCmdOpen      = 0x0;
            BiosGuardHobPtr->Bgpdt.EcCmdClose     = 0x0;
            BiosGuardHobPtr->Bgpdt.EcCmdPortTest  = 0x0;
            DumpBiosGuardModule (BiosGuardHobPtr);
          }
        }
        FreePool(CpBgpdt);
      } else {
        DEBUG ((DEBUG_INFO, "Allocate pool for Bios Gaurd checkpoint failed..\n"));
      }
    }

    if (!FeaturePcdGet(PcdSecureFlashSupported)) {
      BiosGuardHobPtr->Bgpdt.BiosGuardAttr &= ~EnumFlashwearoutProtection;
      DEBUG ((DEBUG_INFO, "FlashWearOut Protection bit had been Disabled.\n"));
    }
//[-end-201021-IB11790397-add]//

    ///
    /// Cpu platform send EC command function
    ///
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SendEcCmd,           BiosGuardConfig->SendEcCmd,         (EFI_PHYSICAL_ADDRESS)(UINTN)PlatformSendEcCommand);

    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.BiosGuardAttr,       BiosGuardConfig->BiosGuardAttr,      BiosGuardHobPtr->Bgpdt.BiosGuardAttr);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.BiosGuardModulePtr,  BiosGuardConfig->BiosGuardModulePtr, BiosGuardHobPtr->BiosGuardModulePtr;);

    DataSize = Sha256GetContextSize ();
    Status   = PeiServicesAllocatePool (DataSize, &Sha256Context);
    ASSERT_EFI_ERROR (Status);
    DataSize = BiosGuardHobPtr->Bgpdt.BgpdtSize;
    Data     = (VOID *) &BiosGuardHobPtr->Bgpdt;
    Sha256Init   (Sha256Context);
    Sha256Update (Sha256Context, Data, DataSize);
    Sha256Final  (Sha256Context, HashValue);
    COPY_POLICY (&((FSPS_UPD *) FspsUpd)->FspsConfig.BgpdtHash[0], &BiosGuardConfig->BgpdtHash[0], &HashValue[0], 32);
  }

  return Status;
}
