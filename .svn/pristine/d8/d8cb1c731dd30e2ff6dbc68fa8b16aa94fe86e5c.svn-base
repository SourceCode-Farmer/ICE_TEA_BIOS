/** @file
  This file provides services for Overclocking PEI policy function

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Library/SiConfigBlockLib.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <OverclockingConfig.h>
#include <Library/CpuPlatformLib.h>

/**
  Print OVERCLOCKING_PREMEM_CONFIG settings.

  @param[in] SiPolicyPreMemPpi  Instance of SI_PREMEM_POLICY_PPI
**/
VOID
OcPreMemPrintConfig (
  IN CONST SI_PREMEM_POLICY_PPI *SiPolicyPreMemPpi
  )
{
  EFI_STATUS                    Status;
  OVERCLOCKING_PREMEM_CONFIG    *OverClockingPreMemConfig;
  CPU_CONFIG_LIB_PREMEM_CONFIG  *CpuConfigLibPreMemConfig;
  UINTN                         Index;

  Status = GetConfigBlock ((VOID *) SiPolicyPreMemPpi, &gOverclockingPreMemConfigGuid, (VOID *) &OverClockingPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPreMemPpi, &gCpuConfigLibPreMemConfigGuid, (VOID *) &CpuConfigLibPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "------------------ OVERCLOCKING_PREMEM_CONFIG Begin ------------------\n"));
  DEBUG ((DEBUG_INFO, " OcSupport : 0x%X\n", OverClockingPreMemConfig->OcSupport));
  DEBUG ((DEBUG_INFO, " OcLock : 0x%X\n", OverClockingPreMemConfig->OcLock));
  DEBUG ((DEBUG_INFO, " CoreVoltageMode : 0x%X\n", OverClockingPreMemConfig->CoreVoltageMode));
  DEBUG ((DEBUG_INFO, " CoreRatioExtensionMode : 0x%X\n", OverClockingPreMemConfig->CoreRatioExtensionMode));
  DEBUG ((DEBUG_INFO, " CorePllVoltageOffset : 0x%X\n", OverClockingPreMemConfig->CorePllVoltageOffset));
  DEBUG ((DEBUG_INFO, " Avx2RatioOffset : 0x%X\n", OverClockingPreMemConfig->Avx2RatioOffset));
  DEBUG ((DEBUG_INFO, " BclkAdaptiveVoltage  : 0x%X\n", OverClockingPreMemConfig->BclkAdaptiveVoltage));
  DEBUG ((DEBUG_INFO, " RingDownBin     : 0x%X\n", OverClockingPreMemConfig->RingDownBin));
  DEBUG ((DEBUG_INFO, " RealtimeMemoryTiming : 0x%X\n", OverClockingPreMemConfig->RealtimeMemoryTiming));
  DEBUG ((DEBUG_INFO, " CoreVfPointOffsetMode : 0x%X\n", OverClockingPreMemConfig->CoreVfPointOffsetMode));
  DEBUG ((DEBUG_INFO, " CoreVfConfigScope : 0x%X\n", OverClockingPreMemConfig->CoreVfConfigScope));
  DEBUG ((DEBUG_INFO, " PerCoreRatioOverride : 0x%X\n", OverClockingPreMemConfig->PerCoreRatioOverride));
  DEBUG ((DEBUG_INFO, " CoreMaxOcRatio  : 0x%X\n", OverClockingPreMemConfig->CoreMaxOcRatio));
  DEBUG ((DEBUG_INFO, " GtMaxOcRatio  : 0x%X\n", OverClockingPreMemConfig->GtMaxOcRatio));
  DEBUG ((DEBUG_INFO, " RingMaxOcRatio  : 0x%X\n", OverClockingPreMemConfig->RingMaxOcRatio));
  DEBUG ((DEBUG_INFO, " CoreVoltageOverride : 0x%X\n", OverClockingPreMemConfig->CoreVoltageOverride));
  DEBUG ((DEBUG_INFO, " CoreVoltageAdaptive : 0x%X\n", OverClockingPreMemConfig->CoreVoltageAdaptive));
  DEBUG ((DEBUG_INFO, " RingVoltageMode : 0x%X\n", OverClockingPreMemConfig->RingVoltageMode));
  DEBUG ((DEBUG_INFO, " RingVoltageOverride : 0x%X\n", OverClockingPreMemConfig->RingVoltageOverride));
  DEBUG ((DEBUG_INFO, " RingVoltageAdaptive : 0x%X\n", OverClockingPreMemConfig->RingVoltageAdaptive));
  DEBUG ((DEBUG_INFO, " RingVoltageOffset : 0x%X\n", OverClockingPreMemConfig->RingVoltageOffset));
  DEBUG ((DEBUG_INFO, " RingVoltageOffset : 0x%X\n", OverClockingPreMemConfig->RingVoltageOffset));
  DEBUG ((DEBUG_INFO, " GtVoltageMode : 0x%X\n", OverClockingPreMemConfig->GtVoltageMode));
  DEBUG ((DEBUG_INFO, " GtVoltageOffset : 0x%X\n", OverClockingPreMemConfig->GtVoltageOffset));
  DEBUG ((DEBUG_INFO, " GtVoltageOverride : 0x%X\n", OverClockingPreMemConfig->GtVoltageOverride));
  DEBUG ((DEBUG_INFO, " GtExtraTurboVoltage : 0x%X\n", OverClockingPreMemConfig->GtExtraTurboVoltage));
  DEBUG ((DEBUG_INFO, " SaVoltageMode : 0x%X\n", OverClockingPreMemConfig->SaVoltageMode));
  DEBUG ((DEBUG_INFO, " SaVoltageOverride : 0x%X\n", OverClockingPreMemConfig->SaVoltageOverride));
  DEBUG ((DEBUG_INFO, " SaExtraTurboVoltage : 0x%X\n", OverClockingPreMemConfig->SaExtraTurboVoltage));
  DEBUG ((DEBUG_INFO, " SaVoltageOffset : 0x%X\n", OverClockingPreMemConfig->SaVoltageOffset));
  DEBUG ((DEBUG_INFO, " GtPllVoltageOffset   : 0x%X\n", OverClockingPreMemConfig->GtPllVoltageOffset));
  DEBUG ((DEBUG_INFO, " RingPllVoltageOffset : 0x%X\n", OverClockingPreMemConfig->RingPllVoltageOffset));
  DEBUG ((DEBUG_INFO, " SaPllVoltageOffset   : 0x%X\n", OverClockingPreMemConfig->SaPllVoltageOffset));
  DEBUG ((DEBUG_INFO, " AtomPllVoltageOffset : 0x%X\n", OverClockingPreMemConfig->AtomPllVoltageOffset));
  DEBUG ((DEBUG_INFO, " McPllVoltageOffset   : 0x%X\n", OverClockingPreMemConfig->McPllVoltageOffset));
  DEBUG ((DEBUG_INFO, " TjMaxOffset          : 0x%X\n", OverClockingPreMemConfig->TjMaxOffset));
  DEBUG ((DEBUG_INFO, " TvbRatioClipping     : 0x%X\n", OverClockingPreMemConfig->TvbRatioClipping));
  DEBUG ((DEBUG_INFO, " TvbVoltageOptimization : 0x%X\n", OverClockingPreMemConfig->TvbVoltageOptimization));
  DEBUG ((DEBUG_INFO, " PerCoreHtDisable : 0x%X\n", OverClockingPreMemConfig->PerCoreHtDisable));
  DEBUG ((DEBUG_INFO, " Avx2VoltageScaleFactor   : 0x%X\n", OverClockingPreMemConfig->Avx2VoltageScaleFactor));
  DEBUG ((DEBUG_INFO, " CoreRatioExtensionMode   : 0x%X\n", OverClockingPreMemConfig->CoreRatioExtensionMode));
  DEBUG ((DEBUG_INFO, " CoreVfPointCount : 0x%X\n", OverClockingPreMemConfig->CoreVfPointCount));
  DEBUG ((DEBUG_INFO, " DlvrBypassModeEnable : 0x%X\n", OverClockingPreMemConfig->DlvrBypassModeEnable));
  DEBUG ((DEBUG_INFO, " SaPllFreqOverride : 0x%X\n", OverClockingPreMemConfig->SaPllFreqOverride));
  DEBUG ((DEBUG_INFO, " TscDisableHwFixup : 0x%X\n", OverClockingPreMemConfig->TscDisableHwFixup));
  if (OverClockingPreMemConfig->CoreVfPointOffsetMode == 0) {
    if (OverClockingPreMemConfig->CoreVfConfigScope == 0) {
      DEBUG ((DEBUG_INFO, " CoreVoltageOffset : 0x%X\n", OverClockingPreMemConfig->CoreVoltageOffset));
    } else {
      for (Index = 0; Index < CpuConfigLibPreMemConfig->ActiveCoreCount1; Index++) {
        DEBUG ((DEBUG_INFO, " PerCoreVoltageOffset[%d] : 0x%X\n", Index, OverClockingPreMemConfig->PerCoreVoltageOffset1[Index]));
      }
    }
  } else {
    for (Index = 0; Index < OverClockingPreMemConfig->CoreVfPointCount; Index++) {
      DEBUG ((DEBUG_INFO, " CoreVfPointOffset[%d] : 0x%X\n", Index, OverClockingPreMemConfig->CoreVfPointOffset[Index]));
    }
  }
  for (Index = 0; Index < CpuConfigLibPreMemConfig->ActiveCoreCount1; Index++) {
    DEBUG ((DEBUG_INFO, " PerCoreRatio[%d] : 0x%X\n", Index, OverClockingPreMemConfig->PerCoreRatio[Index]));
  }
  DEBUG ((DEBUG_INFO, " AtomL2VoltageMode : 0x%X\n", OverClockingPreMemConfig->AtomL2VoltageMode));
  DEBUG ((DEBUG_INFO, " AtomL2VoltageOverride : 0x%X\n", OverClockingPreMemConfig->AtomL2VoltageOverride));
  DEBUG ((DEBUG_INFO, " AtomL2VoltageAdaptive : 0x%X\n", OverClockingPreMemConfig->AtomL2VoltageAdaptive));
  DEBUG ((DEBUG_INFO, " AtomL2VoltageOffset : 0x%X\n", OverClockingPreMemConfig->AtomL2VoltageOffset));
  for (Index = 0; Index < (CpuConfigLibPreMemConfig->ActiveSmallCoreCount/4); Index++) {
    DEBUG ((DEBUG_INFO, " PerAtomClusterVoltageOffset[%d] : 0x%X\n", Index, OverClockingPreMemConfig->PerAtomClusterVoltageOffset[Index]));
  }

  DEBUG ((DEBUG_INFO, " RingVfPointOffsetMode : 0x%X\n", OverClockingPreMemConfig->RingVfPointOffsetMode));
  if (OverClockingPreMemConfig->RingVfPointOffsetMode == 0) {
    DEBUG ((DEBUG_INFO, " RingVoltageOffset : 0x%X\n", OverClockingPreMemConfig->RingVoltageOffset));
  } else {
    DEBUG ((DEBUG_INFO, " RingVfPointCount[%d] : 0x%X\n", Index, OverClockingPreMemConfig->RingVfPointCount));
    for (Index = 0; Index < OverClockingPreMemConfig->RingVfPointCount; Index++) {
      DEBUG ((DEBUG_INFO, " RingVfPointOffset[%d] : 0x%X\n", Index, OverClockingPreMemConfig->RingVfPointOffset[Index]));
   }
  }
  DEBUG ((DEBUG_INFO, " CpuBclkOcFrequency : 0x%X\n", OverClockingPreMemConfig->CpuBclkOcFrequency));
  DEBUG ((DEBUG_INFO, " SocBclkOcFrequency : 0x%X\n", OverClockingPreMemConfig->SocBclkOcFrequency));
  DEBUG ((DEBUG_INFO, " PvdRatioThreshold  : 0x%X\n", OverClockingPreMemConfig->PvdRatioThreshold));
  DEBUG ((DEBUG_INFO, " PvdMode            : 0x%X\n", OverClockingPreMemConfig->PvdMode));
  DEBUG ((DEBUG_INFO, " FllOcModeEn        : 0x%X\n", OverClockingPreMemConfig->FllOcModeEn));
  DEBUG ((DEBUG_INFO, " FllOverclockMode   : 0x%X\n", OverClockingPreMemConfig->FllOverclockMode));
  DEBUG ((DEBUG_INFO, " IaIccUnlimitedMode : 0x%X\n", OverClockingPreMemConfig->IaIccUnlimitedMode));
  DEBUG ((DEBUG_INFO, " IaIccMax           : 0x%X\n", OverClockingPreMemConfig->IaIccMax));
  DEBUG ((DEBUG_INFO, " GtIccUnlimitedMode : 0x%X\n", OverClockingPreMemConfig->GtIccUnlimitedMode));
  DEBUG ((DEBUG_INFO, " GtIccMax           : 0x%X\n", OverClockingPreMemConfig->GtIccMax));
  DEBUG ((DEBUG_INFO, " TvbTempThreshold0  : 0x%X\n", OverClockingPreMemConfig->TvbTempThreshold0));
  DEBUG ((DEBUG_INFO, " TvbTempThreshold1  : 0x%X\n", OverClockingPreMemConfig->TvbTempThreshold1));
  DEBUG ((DEBUG_INFO, " TvbDownBinsTempThreshold0 : 0x%X\n", OverClockingPreMemConfig->TvbDownBinsTempThreshold0));
  DEBUG ((DEBUG_INFO, " TvbDownBinsTempThreshold1 : 0x%X\n", OverClockingPreMemConfig->TvbDownBinsTempThreshold1));
  DEBUG ((DEBUG_INFO, " eTVB                      : 0x%X\n", OverClockingPreMemConfig->Etvb));
  DEBUG ((DEBUG_INFO, "------------------ OVERCLOCKING_PREMEM_CONFIG End ------------------\n"));

}

/**
  Load Config block default

  @param[in] ConfigBlockPointer  Pointer to config block
**/
VOID
OcLoadConfigDefault (
  IN VOID *ConfigBlockPointer
  )
{
  OVERCLOCKING_PREMEM_CONFIG *OverClockingPreMemConfig;

  OverClockingPreMemConfig = ConfigBlockPointer;

  DEBUG ((DEBUG_INFO, "OverClockingPreMemConfig->Header.GuidHob.Name = %g\n", &OverClockingPreMemConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "OverClockingPreMemConfig->Header.GuidHob.Header.HobLength = 0x%x\n", OverClockingPreMemConfig->Header.GuidHob.Header.HobLength));

  /********************************
    Overclocking configuration.
  ********************************/
  OverClockingPreMemConfig->RingDownBin = 1;
  OverClockingPreMemConfig->OcLock      = 1; // Default enable OC Lock bit

  //
  // Default enable IA & GT & SA Current Excursion Protection, IA SoC Iccmax Reactive Protector and ITD Throttle.
  //
  OverClockingPreMemConfig->IaCepEnable       = 1;
  OverClockingPreMemConfig->GtCepEnable       = 1;
  OverClockingPreMemConfig->SaCepEnable       = 1;
  OverClockingPreMemConfig->IaSirpEnable      = 1;
  OverClockingPreMemConfig->ItdThrottleEnable = 1;
  OverClockingPreMemConfig->IaIccMax          = 4;
  OverClockingPreMemConfig->GtIccMax          = 4;
  //
  // Default settings for TVB parameters
  //
  OverClockingPreMemConfig->TvbDownBinsTempThreshold0 = 1;
  OverClockingPreMemConfig->TvbTempThreshold0         = 70;
  OverClockingPreMemConfig->TvbTempThreshold0         = 100;
  OverClockingPreMemConfig->TvbDownBinsTempThreshold1 = 2;
  OverClockingPreMemConfig->Etvb                      = 1;
}

STATIC COMPONENT_BLOCK_ENTRY mOverclockingBlock = {
  &gOverclockingPreMemConfigGuid,
  sizeof (OVERCLOCKING_PREMEM_CONFIG),
  OVERCLOCKING_PREMEM_CONFIG_REVISION,
  OcLoadConfigDefault
};

/**
  Get Overclocking config block table size.

  @retval Size of config block
**/
UINT16
OcPreMemGetConfigBlockTotalSize (
  VOID
  )
{
  return mOverclockingBlock.Size;
}

/**
  Add Overclocking ConfigBlock.

  @param[in] ConfigBlockTableAddress The pointer to config block table

  @retval EFI_SUCCESS                The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES       Insufficient resources to create buffer
**/
EFI_STATUS
OcPreMemAddConfigBlock (
  IN VOID *ConfigBlockTableAddress
  )
{
  EFI_STATUS Status;

  Status = AddComponentConfigBlocks (ConfigBlockTableAddress, &mOverclockingBlock, 1);
  return Status;
}
