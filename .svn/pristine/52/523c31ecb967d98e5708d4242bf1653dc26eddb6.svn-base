## @file
#
#******************************************************************************
#* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##

### @file
# Component description file for BootModeInformation module.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2018 - 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials,either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = PeiBootModeLib
  FILE_GUID                      = 31978A04-C7C2-4803-B304-499D562A5F49
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = PEIM
  LIBRARY_CLASS                  = PeiBootModeLib

[LibraryClasses]
  BaseLib
  PeiServicesLib
  DebugLib
  IoLib
  HobLib
  PcdLib
#[-start-190723-IB11270244-remove]#
#  CmosAccessLib
#[-end-190723-IB11270244-remove]#
  GpioLib
#[-start-190723-IB11270244-remove]#
#  PlatformNvRamHookLib
#[-end-190723-IB11270244-remove]#
  PeiBootStateLib
#[-start-190724-IB11270244-add]#
  BaseMemoryLib
  FlashRegionLib
  H2OCpLib
  PeiCapsuleLib
  PeiOemSvcKernelLibDefault
#[-end-190724-IB11270244-add]#
#[-start-190829-IB17040083-add]#
  SeamlessRecoveryLib
#[-end-190829-IB17040083-add]#
#[-start-201030-IB16810136-add]#
  SpiAccessLib
  PciSegmentLib
  PchPciBdfLib
#[-end-201030-IB16810136-add]#
#[-start-210505-IB16810152-add]#
  TopSwapLib
  ResetSystemLib
  CmosLib
#[-end-210505-IB16810152-add]#
#[-start-211116-IB16560281-add]#
  VariableLib
#[-end-211116-IB16560281-add]#
#[-start-210728-BAIN000025-add]#
!if $(LCFC_SUPPORT_ENABLE) == YES
  TimerLib
!endif
#[-end-210728-BAIN000025-add]#

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
#[-start-200420-IB17800056-add]#
  # ADL PO Temporary add
  IntelFsp2WrapperPkg/IntelFsp2WrapperPkg.dec
#[-end-200420-IB17800056-add]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
#[-start-190724-IB11270244-add]#
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeOemServicesPkg/InsydeOemServicesPkg.dec
  AlderLakeFspPkg/AlderLakeFspPkg.dec
#[-end-190724-IB11270244-add]#
#[-start-211014-BAIN000051-add]#
!if $(LCFC_SUPPORT_ENABLE) == YES
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
!endif
#[-end-211014-BAIN000051-add]#

[Pcd]
  gSiPkgTokenSpaceGuid.PcdTcoBaseAddress                  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcPresent                 ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdSetupEnable               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdRecoveryModeGpio          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardType                 ## CONSUMES
  gEfiMdeModulePkgTokenSpaceGuid.PcdVpdBaseAddress64      ## CONSUMES
#[-start-190829-IB17040083-add]#
  gInsydeTokenSpaceGuid.PcdPeiRecoveryFile
#[-end-190829-IB17040083-add]#
#[-start-200917-IB06462144-add]#
  gInsydeTokenSpaceGuid.PcdCapsuleImageFolder
#[-end-200917-IB06462144-add]#
#[-start-201021-IB11790397-add]#
  gSiPkgTokenSpaceGuid.PcdBiosGuardEnable
#[-end-201021-IB11790397-add]#
#[-start-210415-IB19010024-add]#
  gPlatformModuleTokenSpaceGuid.PcdExtendedBiosRegionSupport   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvExtendedPostMemoryBase  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvExtendedPostMemorySize  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvExtendedAdvancedBase    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvExtendedAdvancedSize    ## CONSUMES
#[-end-210415-IB19010024-add]#
#[-start-210505-IB16810152-add]#
  gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport
#[-end-210505-IB16810152-add]#
#[-start-210720-IB16810158-add]#
  gBoardModuleTokenSpaceGuid.PcdFlashFvOptionalBase
  gBoardModuleTokenSpaceGuid.PcdFlashFvOptionalSize 
#[-end-210720-IB16810158-add]#
#[-start-211014-BAIN000051-add]#
!if $(LCFC_SUPPORT_ENABLE) == YES
  gL05ServicesTokenSpaceGuid.PcdL05BiosRecoveryHotkeyFlag
!endif
#[-end-211014-BAIN000051-add]#

[FixedPcd]
#[-start-190814-IB11270244-add]#
  gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection                      ## CONSUMES
#[-end-190814-IB11270244-add]#
#[-start-190829-IB17040083-add]#
  gInsydeTokenSpaceGuid.PcdFirmwareResourceMaximum
#[-end-190829-IB17040083-add]#

#[-start-190814-IB11270244-add]#
[FeaturePcd]
  gInsydeTokenSpaceGuid.PcdH2OPeiCpCrisisRecoveryPublishFvSupported
  gInsydeTokenSpaceGuid.PcdH2OPeiCpSetBootModeBeforeSupported
#[-end-190814-IB11270244-add]#
#[-start-210506-IB16810153-add]#
  gChipsetPkgTokenSpaceGuid.PcdUcodeCapsuleUpdateSupported
#[-end-210506-IB16810153-add]#

[Sources]
  PeiBootModeLib.c
  PeiBootModeLibInternal.h

[Ppis]
  gPeiBootInNonS3ModePpiGuid                    ## PRODUCES
#[-start-201030-IB16810136-modify]#
  gEfiPeiCapsulePpiGuid
#  gPeiCapsulePpiGuid                            ## CONSUMES
#[-end-201030-IB16810136-modify]#
  gEfiPeiMasterBootModePpiGuid                  ## PRODUCES
  gEfiPeiBootInRecoveryModePpiGuid              ## PRODUCES
#[-start-210415-IB19010024-add]#  
  gExtendedBiosDecodeReadyPpiGuid               ## CONSUMES
#[-end-210415-IB19010024-add]#
#[-start-210720-IB16810158-add]#
  gPeiFvCnvDispatchFlagPpiGuid
#[-end-210720-IB16810158-add]#

[Guids]
  gFastBootExceptionInfoHobGuid                 ## CONSUMES
  gCpuSetupVariableGuid                         ## CONSUMES
  gMeSetupVariableGuid                          ## CONSUMES
  gSaSetupVariableGuid                          ## CONSUMES
  gPchSetupVariableGuid                         ## CONSUMES
  gBootStateGuid                                ## CONSUMES
  gEfiGlobalVariableGuid                        ## CONSUMES
  gSetupVariableGuid                            ## CONSUMES
  gChassisIntrudeDetHobGuid                     ## CONSUMES
  gVpdFfsGuid                                   ## CONSUMES
#[-start-190814-IB11270244-add]#
  gH2OPeiCpSetBootModeBeforeGuid
  gH2OFlashMapRegionFvGuid
  gH2OFlashMapRegionVarGuid
  gH2OFlashMapRegionFtwStateGuid
  gH2OFlashMapRegionFtwBackupGuid
  gH2OFlashMapRegionVarDefaultGuid
  gH2OFlashMapRegionFactoryCopyGuid
  gH2OPeiCpCrisisRecoveryPublishFvGuid
  gFspSiliconFvGuid
#[-end-190814-IB11270244-add]#
#[-start-201030-IB16810136-add]#
  gSysFwUpdateProgressGuid
  gEfiGlobalVariableGuid
  gFmpCapsuleInfoGuid
  gSysFwUpdateDigiestGuid
  gH2OFlashMapRegionPeiFvGuid
  gFwRecovery2FvGuid
  gH2OFlashMapRegionPeiFvGuid
#[-end-201030-IB16810136-add]#
#[-start-201021-IB11790397-add]#
  gFwBinaryFvGuid
#[-end-201021-IB11790397-add]#
#[-start-210506-IB16810153-add]#
  gH2OFlashMapRegionMicrocodeGuid
#[-end-210506-IB16810153-add]#
#[-start-210720-IB16810158-add]#
  gBootStateAfterCapsuleGuid
#[-end-210720-IB16810158-add]#
#[-start-211116-IB16560281-add]#
  gArbSvnInfoGuid
  gArbSvnInfoHobGuid
#[-end-211116-IB16560281-add]#

