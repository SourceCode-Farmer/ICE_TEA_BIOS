/** @file
    This function offers an interface to dynamically modify GPIO table.

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcChipsetLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <PlatformGpioConfig.h>
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableTglUDdr4ProjectBoard[] =
{
  // CPU M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD PWREN
  {GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_B15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_C22, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WLAN_RST_N
  {GPIO_VER2_LP_GPP_C23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //UART_BT_WAKE_N : Not default POR
  {GPIO_VER2_LP_GPP_A10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,  GpioOutDefault,GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M.2 BT

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_A14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_C13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_RESET_N
  {GPIO_VER2_LP_GPP_F5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_DGPU_SEL
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,  GpioOutDefault,GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_DGPU_PWROK

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_A23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TC_RETIMER_FORCE_PWR
  {GPIO_VER2_LP_GPD7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,  GpioIntDis,GpioDswReset,  GpioTermNone}},  //TCP_RETIMER_PERST_N

  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  {GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntSci,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  // SATA Direct Connect
  {GPIO_VER2_LP_GPP_B4,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //SATA_DIRECT_PWREN

  // FPS
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //FPS_RST_N
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //FPS_INT

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_B16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N


  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_PWREN - CAM1
  {GPIO_VER2_LP_GPP_C15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WF_CAM_RST_N  - CAM1

  {GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_PWREN/BIOS_REC
  {GPIO_VER2_LP_GPP_H12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_RST_N

  {GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM3_PWREN
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM3_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE_1
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_PRIVACY_LED_1
  {GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WF_CAM_CLK_EN

  // Audio
  {GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //SPKR_PD_N
  {GPIO_VER2_LP_GPP_C12, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  // CODEC_INT_N

  // Touch Pad
  // Touch Pad and Touch Panel 2 share the same Power Enable, default is Touch pad
  {GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_LS_EN - PWR_En
  {GPIO_VER2_LP_GPP_C8,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //TCH_PAD_INT_N

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSmi,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_E8,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

  // SPI TPM, derived from ICL
  {GPIO_VER2_LP_GPP_C14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

  // TypeC BIAS : Not used by default in RVP, derived from ICL
  {GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_P_BIAS_GPIO
  {GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO

  // LAN : Not used by Default in RVP

  // X1 Pcie Slot
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //X1 Slot PWREN
  {GPIO_VER2_LP_GPP_F4,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //X1 Slot WAKE
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //X1 Slot RESET
  {0x0}
};


static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableTglUDdr4SimicsProjectBoard[] =
{
  // CPU M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD PWREN
  {GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_B15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_C22, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WLAN_RST_N
  {GPIO_VER2_LP_GPP_A10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,  GpioOutDefault,GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M.2 BT
//  All SCI GPIO's are not programmed due to HFPGA issue : 1505908653
//  {GPIO_VER2_LP_GPP_C23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //WIFI_WAKE_N
//  {GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //UART_BT_WAKE_N : Not default POR

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_A14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_C13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_RESET_N
//  {GPIO_VER2_LP_GPP_B3,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_DGPU_SEL
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,  GpioOutDefault,GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_DGPU_PWROK

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_A23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TC_RETIMER_FORCE_PWR
  {GPIO_VER2_LP_GPD7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,  GpioIntDis,GpioDswReset,  GpioTermNone}},  //TCP_RETIMER_PERST_N

  // Battery Charger Vmin to PCH PROCHOT, derived from TGL
//  {GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntSci,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  // SATA Direct Connect
  {GPIO_VER2_LP_GPP_B4,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //SATA_DIRECT_PWREN

  // FPS
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //FPS_RST_N
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //FPS_INT

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_B16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_PWREN - CAM1
  {GPIO_VER2_LP_GPP_C15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WF_CAM_RST_N  - CAM1

  {GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_PWREN/BIOS_REC
  {GPIO_VER2_LP_GPP_H12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_RST_N

  {GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM3_PWREN
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM3_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE_1
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_PRIVACY_LED_1
  {GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WF_CAM_CLK_EN

  // Audio
  {GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //SPKR_PD_N
  {GPIO_VER2_LP_GPP_C12, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  // CODEC_INT_N

  // Touch Pad
  // Touch Pad and Touch Panel 2 share the same Power Enable, default is Touch pad
  {GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_LS_EN - PWR_En
  {GPIO_VER2_LP_GPP_C8,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //TCH_PAD_INT_N

  // EC
 // {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSmi,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_E8,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

  // SPI TPM, derived from TGL
 // {GPIO_VER2_LP_GPP_C14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

  // TypeC BIAS : Not used by default in RVP, derived from TGL
  {GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_P_BIAS_GPIO
  {GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO

  // LAN : Not used by Default in RVP
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //LAN_RST_N

  // X1 Pcie Slot
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //X1 Slot PWREN
  {GPIO_VER2_LP_GPP_F4,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //X1 Slot WAKE
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //X1 Slot RESET
  {0x0}
};


static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mTglTouchPanel1GpioTableProjectBoard[] =
{
  // Touch Panel 1, Same pins shared between THC and I2C based Panel,
  {GPIO_VER2_LP_GPP_H14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},  //TCH_PNL_PWR_EN
  {GPIO_VER2_LP_GPP_E6,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},  //THC0_RST
  {GPIO_VER2_LP_GPP_E17, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //THC0_INT
};

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mTglTouchPanel2GpioTableProjectBoard[] =
{
  // Touch Panel 2, Not used by default in RVP; Applicable for Converge Mobility RVP SKU only;
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},  //TCH_PNL2_PWR_EN
  {GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},  //THC1_SPI2_RSTB
  {GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //THC1_SPI2_INTB
};


/**
 This function offers an interface to dynamically modify GPIO table.

 @param[in, out]    GpioTable           On entry, points to a structure that specifies the GPIO setting.
                                        On exit, points to the updated structure.
 @param[in, out]    GpioTableCount      On entry, points to a value that the length of GPIO table .
                                        On exit, points to the updated value.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcModifyGpioSettingTable (
  IN OUT  GPIO_INIT_CONFIG                 **GpioTable,
  IN OUT  UINT16                           *GpioTableCount
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
// *GpioTable = mGpioTableTglUDdr4Simics;
//
// *GpioTableCount = 38;

  return EFI_UNSUPPORTED;
}
