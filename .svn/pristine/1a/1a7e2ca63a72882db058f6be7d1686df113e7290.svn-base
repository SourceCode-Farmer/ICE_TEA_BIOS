#******************************************************************************
#* Copyright (c) 2020 - 2021, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
# Config file template to describe BIOS FV image address and target flash address used for BIOS update.
# This file will be patched with the correct address by flashmap and BGUP file when building capsule.
# For Chasm Falls Gen2
#
#@copyright
#  INTEL CONFIDENTIAL
#    Copyright 2019 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Head]
NumOfUpdate = 2
ImageFileGuid  = 5C245293-53B5-4455-BA02-2279EA55BB68  # gCapsuleBiosImageFileGuid
HelperFileGuid = 801776F0-26F9-4934-81ED-7241E29669B0  # gCapsuleBiosBgupFileGuid

Update0 = BiosIbb
Update1 = BiosObb

[BiosObb]
BaseAddress  = 0x00080000    # Spi flash address
Length       = 0x00756000    # Same as PcdFlashSbbSize
ImageOffset  = 0x00080000    # Image address (NV_COMMON_STORE_SUBREGION_NV_BVDT ~ FV_RESERVED)
HelperOffset = 0x00000000    # Image offset within file of HelperFileGuid used to update this region via BiosGuard
HelperLength = 0x00000000    # Image lenght within file of HelperFileGuid used to update this region via BiosGuard

[BiosIbb]
BaseAddress  = 0x00C00000    # Spi flash address
Length       = 0x00400000    # Same as PcdFlashPbbSize
ImageOffset  = 0x00C00000    # Image address (FV_PEI_RESERVED ~ FV_RECOVERY0)
HelperOffset = 0x00000000    # Image offset within file of HelperFileGuid used to update this region via BiosGuard
HelperLength = 0x00000000    # Image lenght within file of HelperFileGuid used to update this region via BiosGuard
