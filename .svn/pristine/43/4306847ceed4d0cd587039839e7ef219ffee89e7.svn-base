/** @file

;******************************************************************************
;* Copyright (c) 2019 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _NVIDIA_DISCRETE_ASI_
#define _NVIDIA_DISCRETE_ASI_

//
// ASL code common define about device and bridge
//
#define PCI_SCOPE                             \_SB.PC00
#define DGPU_BRIDGE_SCOPE                     PCI_SCOPE.PEG1
#define DGPU_DEVICE                           PEGP
#define DGPU_SCOPE                            DGPU_BRIDGE_SCOPE.DGPU_DEVICE
#define DGPU2_BRIDGE_SCOPE                    PCI_SCOPE.PEG2
#define DGPU2_DEVICE                          PEGP
#define DGPU2_SCOPE                           DGPU2_BRIDGE_SCOPE.DGPU_DEVICE
#define IGPU_SCOPE                            PCI_SCOPE.GFX0
#define EC_SCOPE                              PCI_SCOPE.LPCB.H_EC

//
// nVIDIA GPS and Ventura feature usage define
//
#define CPU0_SCOPE                            \_SB.PR00
#define CPU1_SCOPE                            \_SB.PR01
#define CPU2_SCOPE                            \_SB.PR02
#define CPU3_SCOPE                            \_SB.PR03
#define CPU4_SCOPE                            \_SB.PR04
#define CPU5_SCOPE                            \_SB.PR05
#define CPU6_SCOPE                            \_SB.PR06
#define CPU7_SCOPE                            \_SB.PR07

//
// nVIDIA device's DID list
//
#define N13E_GS1_DID                          0x1212
#define N13E_GE_DID                           0x0FD4
#define N13E_GTX_DID                          0x11A0

//
// ACPI define in dGPU SCOPE _DOD method
//
#define ACPI_ID_LCD                           0x00000110
#define ACPI_ID_CRT                           0x80000100
#define ACPI_ID_HDMI                          0x80008320
#define ACPI_ID_TMDS                          0x80008340
#define ACPI_ID_DP_B                          0x80006330
#define ACPI_ID_DP_C                          0x80006340
#define ACPI_ID_EDP                           0x8000A450

//
// License define
//
#define TEST_LICENSE_VECTOR                   0x25, 0x1B, 0xA2, 0x44, 0x17, 0x62, 0x17, 0x46, 0xBF, 0xB7, 0x41, 0x51, 0x4C, 0xEA, 0xC2, 0x41
#define N13E_GS1_LICENSE_VECTOR               TEST_LICENSE_VECTOR
#define N13E_GE_LICENSE_VECTOR                TEST_LICENSE_VECTOR
#define N13E_GTX_LICENSE_VECTOR               TEST_LICENSE_VECTOR

//
// nVIDIA return status code
//
#define STATUS_SUCCESS                        0x00000000 // Generic Success
#define STATUS_ERROR_UNSPECIFIED              0x80000001 // Generic unspecified error code
#define STATUS_ERROR_UNSUPPORTED              0x80000002 // Sub-Function not supported

//
// nVIDIA GPS feature related function define
//
#define GPS_FUNC_SUPPORT                      0x00
#define GPS_FUNC_GETCALLBACKS                 0x13
#define GPS_FUNC_PCONTROL                     0x1C
#define GPS_FUNC_PSHARESTATUS                 0x20
#define GPS_FUNC_GETPSS                       0x21
#define GPS_FUNC_SETPPC                       0x22
#define GPS_FUNC_GETPPC                       0x23
#define GPS_FUNC_PSHAREPARAMS                 0x2A

//
// nVIDIA GC6 feature related function define
//
#define JT_REVISION_ID                        0x00000100
#define JT_FUNC_SUPPORT                       0x00000000
#define JT_FUNC_CAPS                          0x00000001
#define JT_FUNC_POLICYSELECT                  0x00000002
#define JT_FUNC_POWERCONTROL                  0x00000003
#define JT_FUNC_PLATPOLICY                    0x00000004
#define JT_FUNC_DISPLAYSTATUS                 0x00000005
#define JT_FUNC_MDTL                          0x00000006

#define PID_PSTH                              0x89
#define R_PCH_PCR_PSTH_TRPREG0                0x1E80                ///< IO Tarp 0 register
#define R_PCH_PCR_PSTH_TRPREG1                0x1E88                ///< IO Tarp 1 register
#define R_PCH_PCR_PSTH_TRPREG2                0x1E90                ///< IO Tarp 2 register
#define R_PCH_PCR_PSTH_TRPREG3                0x1E98                ///< IO Tarp 3 register

#define PID_DMI                               0xEF
#define R_PCH_PCR_DMI_IOT1                    0x2750                ///< I/O Trap Register 1
#define R_PCH_PCR_DMI_IOT2                    0x2758                ///< I/O Trap Register 2
#define R_PCH_PCR_DMI_IOT3                    0x2760                ///< I/O Trap Register 3
#define R_PCH_PCR_DMI_IOT4                    0x2768                ///< I/O Trap Register 4

#define PID_ICC                               0xDC
#define R_PCH_PCR_ICC_MSKCKRQ                 0x100C                ///< Mask Control CLKREQ

//
// nVIDIA NBCI feature related function define
//
#define NBCI_FUNC_SUPPORT                     0x00
#define NBCI_FUNC_PLATCAPS                    0x01
#define NBCI_FUNC_GETOBJBYTYPE                0x10
#define NBCI_FUNC_GETBACKLIGHT                0x14
#define NBCI_FUNC_GETLICENSE                  0x16

//
// MXM Function define
//
#define MXM_FUNC_MXSS                         0x00
#define NVOP_FUNC_DISPLAYSTATUS               0x05
#define NVOP_FUNC_MDTL                        0x06
#define MXM_FUNC_MXMS                         0x10
#define MXM_FUNC_MXMI                         0x18
#define MXM_FUNC_MDTL                         0x19
#endif
