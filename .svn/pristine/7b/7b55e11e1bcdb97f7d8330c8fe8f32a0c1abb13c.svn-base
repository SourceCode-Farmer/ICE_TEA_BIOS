## @file
#
#******************************************************************************
#* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#++
#  This file contains a 'Sample Driver' and is licensed as such
#  under the terms of your license agreement with Intel or your
#  vendor.  This file may be modified by the user, subject to
#  the additional terms of the license agreement
#--
## @file
#  Wireless Connection Manager.
#
#  This wireless connection manager enables wifi configuration for users.
#
# Copyright (c) 2013 - 2017, Intel Corporation. All rights reserved.<BR>
# This software and associated documentation (if any) is furnished
# under a license and may only be used or copied in accordance
# with the terms of the license. Except as permitted by such
# license, no part of this software or documentation may be
# reproduced, stored in a retrieval system, or transmitted in any
# form or by any means without the express written consent of
# Intel Corporation.
#
##

[Defines]
  INF_VERSION               = 0x00010005
  BASE_NAME                 = WifiConnectionManagerDxe
  FILE_GUID                 = c6df98f2-5ec0-4a94-8c11-9a9828ef03f2
  MODULE_TYPE               = DXE_DRIVER
  VERSION_STRING            = 0.1
  ENTRY_POINT               = WifiMgrDxeDriverEntryPoint
  UNLOAD_IMAGE              = WifiMgrDxeUnload

[Sources]
  WifiConnectionMgrDxe.h
  WifiConnectionMgrDriverBinding.h
  WifiConnectionMgrConfig.h
  WifiConnectionMgrMisc.h
  WifiConnectionMgrImpl.h
  WifiConnectionMgrConfigNVDataStruct.h
  WifiConnectionMgrHiiConfigAccess.h
  WifiConnectionMgrComponentName.h
  WifiConnectionMgrFileUtil.h
  WifiConnectionMgrDriver.c
  WifiConnectionMgrComponentName.c
  WifiConnectionMgrMisc.c
  WifiConnectionMgrHiiConfigAccess.c
  WifiConnectionMgrImpl.c
  WifiConnectionMgrFileUtil.c
  WifiConnectionMgrFileExplorer.c
  WifiConnectionManagerDxeStrings.uni
  WifiConnectionManagerDxe.vfr
  EapContext.h
  WifiConnectionMgrConfigHii.h

  Kms/KmsService.h
  Kms/KmsService.c
  Kms/KmsImpl.h
  Kms/KmsImpl.c
  Kms/KmsProtocol.h
  Kms/KmsProtocol.c
  Kms/KmsServiceSample.h

  Kms/VariableEx/VariableEx.c
  Kms/VariableEx/VariableEx.h
  Kms/VariableEx/KeyLib.h
  Kms/VariableEx/KeyLib.c

[Packages]
#_Start_L05_NOTEBOOK_CLOUD_BOOT_WIFI_
  $(PROJECT_PKG)/Project.dec
  CryptoPkg/CryptoPkg.dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
!if $(L05_CHIPSET_VENDOR) == INTEL
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec  ## PchLimits.h
!endif
#_End_L05_NOTEBOOK_CLOUD_BOOT_WIFI_
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeNetworkPkg/InsydeNetworkPkg.dec
  NetworkPkg/NetworkPkg.dec
#_Start_L05_NOTEBOOK_CLOUD_BOOT_WIFI_
#  CryptoPkg/CryptoPkg.dec
#_End_L05_NOTEBOOK_CLOUD_BOOT_WIFI_

[LibraryClasses]
  UefiDriverEntryPoint
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  MemoryAllocationLib
  BaseMemoryLib
  BaseCryptLib
  BaseLib
  UefiLib
  DevicePathLib
  DebugLib
  HiiLib
  PrintLib
  UefiHiiServicesLib
  NetLib
  FileExplorerLib
#_Start_L05_NOTEBOOK_CLOUD_BOOT_WIFI_
  VariableLib
#_End_L05_NOTEBOOK_CLOUD_BOOT_WIFI_

[Protocols]
  gEfiHiiConfigAccessProtocolGuid               ## PRODUCES
  gEfiWiFi2ProtocolGuid                         ## TO_START
  gEfiAdapterInformationProtocolGuid            ## SOMETIMES_CONSUMES
  gEfiSupplicantProtocolGuid                    ## SOMETIMES_CONSUMES
  gEfiEapConfigurationProtocolGuid              ## SOMETIMES_CONSUMES
  gEfiSimpleFileSystemProtocolGuid              ## CONSUMES

  gCryptoServicesProtocolGuid                   ## CONSUMES use CRYPTO_SERVICES_PBKDF2_CREATE_KEY from CryptoServicesProtocol
  gH2ODialogProtocolGuid
  gEfiFormBrowser2ProtocolGuid
  gEfiFirmwareVolume2ProtocolGuid
  gEfiDevicePathProtocolGuid
  gEfiBlockIoProtocolGuid

[Guids]
  gEfiIfrTianoGuid                              ## CONSUMES  ## GUID (Extended IFR Guid Opcode)
  gEfiAdapterInfoMediaStateGuid                 ## SOMETIMES_CONSUMES  ## GUID  # Indicate the current media state status
  gEfiCertX509Guid                              ## CONSUMES  ## GUID  # Indicate the cert type
  gEfiEndOfDxeEventGroupGuid                    ## CONSUMES  ## Event
  gEfiFileInfoGuid
#_Start_L05_NOTEBOOK_CLOUD_BOOT_WIFI_
  gSystemConfigurationGuid
#_End_L05_NOTEBOOK_CLOUD_BOOT_WIFI_

[FeaturePcd]
  #gInsydeTokenSpaceGuid.PcdH2OFormBrowserSupported


[Pcd]
#_Start_L05_NOTEBOOK_CLOUD_BOOT_WIFI_
  gL05ServicesTokenSpaceGuid.PcdL05InputWifiPassword
  gL05ServicesTokenSpaceGuid.PcdL05WifiAutoConnect
#_End_L05_NOTEBOOK_CLOUD_BOOT_WIFI_

[Depex]
  gEfiHiiConfigRoutingProtocolGuid AND
  gEfiRealTimeClockArchProtocolGuid AND
  gCryptoServicesProtocolGuid
