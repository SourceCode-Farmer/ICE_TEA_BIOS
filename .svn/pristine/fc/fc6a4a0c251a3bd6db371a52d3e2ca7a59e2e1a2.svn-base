/** @file
  USB4 host interface core libary header.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_HI_CORE_LIB_H_
#define _USB4_HI_CORE_LIB_H_

#include <Uefi.h>
#include <Usb4ConfigLayer.h>
#include <Usb4HiCoreDefs.h>
#include <Usb4CsIo.h>
#include <Usb4HrInst.h>

/**
  Create and initialize USB4 Host Interface for Tx/Rx Ring access.

  @param[in] Usb4Hr - Pointer to USB4 host router instance.

  @retval EFI_SUCCESS           - Initialize USB4 host interface successfully.
  @retval EFI_UNSUPPORTED       - Fail to initialize USB4 host interface.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4HiCoreCreate (
  IN  USB4_HR_INSTANCE    *Usb4Hr
  );

/**
  Destroy USB4 host interface

  @param[in] HiCore - Pointer to USB4 host interface core instance.
**/
VOID
Usb4HiCoreDestroy (
  IN USB4_HI_CORE    *HiCore
  );

/**
  Start USB4 host interface core.
  - Create Ring buffer and enable Tx/Rx Ring0 and release.

  @param[in] HiCore - Pointer to USB4 host interface core instance.
**/
EFI_STATUS
Usb4HiCoreStart (
  IN USB4_HI_CORE    *HiCore
  );

/**
  Query USB4_CS_IO associated with USB4 host interface core.

  @param[in]  HiCore   - Pointer to USB4 host interface core instance.
  @param[out] Usb4CsIo - Pointer of pointer to USB4_CS_IO.

  @retval EFI_SUCCESS           - Query USB4_CS_IO successfully.
  @retval EFI_NOT_FOUND         - USB4_CS_IO is not available.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4HiQueryCsIo (
  IN  USB4_HI_CORE    *HiCore,
  OUT USB4_CS_IO      **Usb4CsIo
  );

/**
  Query Rx Ring for Rx event.

  @param[in]  HiCore     - Pointer to USB4 host interface core instance.
  @param[out] RxEvent    - Pointer to the received Rx Event.

  @retval EFI_SUCCESS           - A Rx event is received.
  @retval EFI_NOT_FOUND         - No Rx event is received.
  @retval EFI_UNSUPPORTED       - Fail to query Rx event for unexpected error.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
EFI_STATUS
Usb4HiQueryRxEvent (
  IN  USB4_HI_CORE    *HiCore,
  OUT RX_EVENT        *RxEvent
  );

#endif
