## @file
#  Platform Package Flash Description File
#
#******************************************************************************
#* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##

[FV.RECOVERYFV0]
BlockSize          = 0x10000
FvAlignment        = 16
ERASE_POLARITY     = 1
MEMORY_MAPPED      = TRUE
STICKY_WRITE       = TRUE
LOCK_CAP           = TRUE
LOCK_STATUS        = TRUE
WRITE_DISABLED_CAP = TRUE
WRITE_ENABLED_CAP  = TRUE
WRITE_STATUS       = TRUE
WRITE_LOCK_CAP     = TRUE
WRITE_LOCK_STATUS  = TRUE
READ_DISABLED_CAP  = TRUE
READ_ENABLED_CAP   = TRUE
READ_STATUS        = TRUE
READ_LOCK_CAP      = TRUE
READ_LOCK_STATUS   = TRUE
FvNameGuid = gH2OFlashMapRegionPeiFv0Guid

APRIORI PEI {
}

#
# Move drivers in  MdeModulePag to RECOVERYFV0
#
!if (gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == FALSE) || (gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1) || (gMinPlatformPkgTokenSpaceGuid.PcdFspDispatchModeUseFspPeiMain == FALSE)
INF MdeModulePkg/Core/Pei/PeiMain.inf
!endif
INF APRIORI=0xA001 MdeModulePkg/Universal/PCD/Pei/Pcd.inf
INF APRIORI=0xA001 InsydeModulePkg/Universal/ReportStatusCodeRouter/Pei/ReportStatusCodeRouterPei.inf
!if gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial || gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseMemory
INF APRIORI MdeModulePkg/Universal/StatusCodeHandler/Pei/StatusCodeHandlerPei.inf
!endif

#
# Move drivers in  InsydeModulePkg to RECOVERYFV0
#
!if gInsydeTokenSpaceGuid.PcdH2ODdtSupported
INF APRIORI=0xC000 $(CHIPSET_PKG)/PchDdtUsbPei/PchDdtUsbPei.inf
!if $(H2O_DDT_DEBUG_IO) == Com
INF APRIORI=0xC000 $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/H2ODebug/ComDebugIoPei/ComDebugIoPei.inf
!else
INF $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/H2ODebug/XhcDebugIoPei/XhcDebugIoPei.inf
!endif
INF APRIORI=0xC000 $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/H2ODebug/DebugEnginePei/DebugEnginePei.inf
!if gInsydeTokenSpaceGuid.PcdStatusCodeUseDdt
INF APRIORI InsydeModulePkg/Universal/StatusCode/DdtStatusCodePei/DdtStatusCodePei.inf
!endif
!endif
!if gEfiTraceHubTokenSpaceGuid.PcdStatusCodeUseTraceHub
INF APRIORI=0xC000 $(CHIPSET_PKG)/TraceHubPostCodeHandler/Pei/TraceHubPostCodeHandlerPei.inf
!endif
!if gInsydeTokenSpaceGuid.PcdStatusCodeUseUsb
!if gInsydeTokenSpaceGuid.PcdStatusCodeUseXhc
INF APRIORI InsydeModulePkg/Universal/StatusCode/XhcStatusCodePei/XhcStatusCodePei.inf
!else
INF APRIORI InsydeModulePkg/Universal/StatusCode/UsbStatusCodePei/UsbStatusCodePei.inf
!endif
!endif
!if gInsydeTokenSpaceGuid.PcdStatusCodeUseCmos
INF APRIORI InsydeModulePkg/Universal/StatusCode/CmosStatusCodePei/CmosStatusCodePei.inf
!endif
INF APRIORI=0xC001 $(CHIPSET_PKG)/InitSerialPortPei/InitSerialPortPei.inf
INF $(CHIPSET_PKG)/InsydeReportFvPei/InsydeReportFvPei.inf

[FV.RECOVERYFV]
#
# Move drivers in  MdeModulePag to RECOVERYFV0
#
!disable MdeModulePkg/Core/Pei/PeiMain.inf
!disable MdeModulePkg/Universal/PCD/Pei/Pcd.inf
!disable InsydeModulePkg/Universal/ReportStatusCodeRouter/Pei/ReportStatusCodeRouterPei.inf
!if gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial || gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseMemory
!disable MdeModulePkg/Universal/StatusCodeHandler/Pei/StatusCodeHandlerPei.inf
!endif

#
# Move drivers in  InsydeModulePkg to RECOVERYFV0
#
!if gInsydeTokenSpaceGuid.PcdH2ODdtSupported
!disable InsydeModulePkg/H2ODebug/$(H2O_DDT_DEBUG_IO)DebugIoPei/$(H2O_DDT_DEBUG_IO)DebugIoPei.inf
!disable InsydeModulePkg/H2ODebug/DebugEnginePei/DebugEnginePei.inf
!if gInsydeTokenSpaceGuid.PcdStatusCodeUseDdt
!disable InsydeModulePkg/Universal/StatusCode/DdtStatusCodePei/DdtStatusCodePei.inf
!endif
!endif

!if gInsydeTokenSpaceGuid.PcdStatusCodeUseUsb
!if gInsydeTokenSpaceGuid.PcdStatusCodeUseXhc
!disable InsydeModulePkg/Universal/StatusCode/XhcStatusCodePei/XhcStatusCodePei.inf
!else
!disable InsydeModulePkg/Universal/StatusCode/UsbStatusCodePei/UsbStatusCodePei.inf
!endif
!endif

!if gInsydeTokenSpaceGuid.PcdStatusCodeUseCmos
!disable InsydeModulePkg/Universal/StatusCode/CmosStatusCodePei/CmosStatusCodePei.inf
!endif

INF PRIORITY=0x9000 $(CHIPSET_PKG)/ChipsetSvcPei/ChipsetSvcPei.inf

!if gChipsetPkgTokenSpaceGuid.PcdRestoreCmosfromVariableFlag
INF $(CHIPSET_PKG)/SaveRestoreCmos/RestoreCmosPei/RestoreCmosPei.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdHybridGraphicsSupported
INF $(CHIPSET_PKG)/HybridGraphicsPei/HybridGraphicsPei.inf
!endif

# Init Board Config PCD
INF MdeModulePkg/Universal/ResetSystemPei/ResetSystemPei.inf

!if gInsydeTokenSpaceGuid.PcdH2OPeiTimerSupported
INF $(CHIPSET_PKG)/8259InterruptControllerPei/8259.inf
INF $(CHIPSET_PKG)/CpuArchPei/CpuArchPei.inf
INF $(CHIPSET_PKG)/SmartTimer/Pei/SmartTimer.inf
!endif

################################################################################
#
# FV.RECOVERYFV Override
#
################################################################################
#
# Intel platform do not need ProgClkGenPeim.inf, Because Intel clock gen is setting on ME by Fitc.exe
#
!disable InsydeModulePkg/Bus/Smbus/ProgClkGenPeim/ProgClkGenPeim.inf

INF APRIORI $(CHIPSET_PKG)/EarlyProgramGpioPei/EarlyProgramGpioPei.inf
#[-start-201221-IB16740127-remove]# The judgment was removed in RC1515
#!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == FALSE
!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == TRUE
INF RuleOverride = LzmaCompress UefiCpuPkg/CpuMpPei/CpuMpPei.inf
!endif
#
# CpuMp PEIM for MpService PPI
#
#INF RuleOverride = LzmaCompress UefiCpuPkg/CpuFeatures/CpuFeaturesPei.inf
#!endif
#[-end-201221-IB16740127-remove]#

#
# Replace PlatformStage1Pei with PlatformInitPreMem
#
!disable InsydeModulePkg/Universal/CommonPolicy/PlatformStage1Pei/PlatformStage1Pei.inf
!disable InsydeModulePkg/Universal/CommonPolicy/PlatformStage2Pei/PlatformStage2Pei.inf

!if gInsydeTokenSpaceGuid.PcdH2OFdmChainOfTrustSupported
INF $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Universal/Security/H2OVerifyRegion/H2OVerifyRegionPei/H2OVerifyRegionPei.inf
!endif
#[-start-190702-16990089-add]#
!if gSiPkgTokenSpaceGuid.PcdBootGuardEnable
INF $(CHIPSET_PKG)/BootGuardRecoveryHookPei/BootGuardRecoveryHookPei.inf
!endif
#[-end-190702-16990089-add]#
#
# Unmark to launch PPI to output some status code from reserved memory for verification.
#
#!if gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseMemory == 1
#INF $(CHIPSET_PKG)/StatusCodeHandlerTestPei/StatusCodeHandlerTestPei.inf
#!endif
!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport != 0
INF  $(CHIPSET_PKG)/TopSwapRestorePei/TopSwapRestorePei.inf
!endif

[FV.BACKUPFV]
!if gInsydeTokenSpaceGuid.PcdCrisisRecoverySupported
INF RuleOverride=LzmaCompress $(CHIPSET_PKG)/CommonChipset/SpeakerPei/LegacySpeakerPei.inf
INF RuleOverride=LzmaCompress $(CHIPSET_PKG)/PchAhciPei/PchAhciPei.inf
!endif
#[-start-190909-IB11270246-add]#
!if gInsydeTokenSpaceGuid.PcdH2OUsbPeiSupported
INF RuleOverride=LzmaCompress $(CHIPSET_PKG)/PchXhciPei/PchXhciPei.inf
!endif
#[-end-190909-IB11270246-add]#

[FV.RECOVERYFV2]
BlockSize          = 0x10000
FvAlignment        = 16         #FV alignment and FV attributes setting.
ERASE_POLARITY     = 1
MEMORY_MAPPED      = TRUE
STICKY_WRITE       = TRUE
LOCK_CAP           = TRUE
LOCK_STATUS        = TRUE
WRITE_DISABLED_CAP = TRUE
WRITE_ENABLED_CAP  = TRUE
WRITE_STATUS       = TRUE
WRITE_LOCK_CAP     = TRUE
WRITE_LOCK_STATUS  = TRUE
READ_DISABLED_CAP  = TRUE
READ_ENABLED_CAP   = TRUE
READ_STATUS        = TRUE
READ_LOCK_CAP      = TRUE
READ_LOCK_STATUS   = TRUE
FvNameGuid         = 8579D1CA-45E8-4f1c-A789-FFA770672099

#[-start-201221-IB16740127-remove]// The judgment was removed in RC1513
#!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
#[-end-201221-IB16740127-remove]//
!if gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceResiliencyEnabled == TRUE
INF RuleOverride = LzmaCompress IntelFsp2WrapperPkg/FspsWrapperPeim/FspsWrapperPeim.inf
!else
INF IntelFsp2WrapperPkg/FspsWrapperPeim/FspsWrapperPeim.inf
!endif
#[-start-201221-IB16740127-remove]// The judgment was removed in RC1513
#!endif
#[-end-201221-IB16740127-remove]//
FILE FREEFORM = 7BB28B99-61BB-11D5-9A5D-0090273FC14D { #gTianoLogoGuid
  SECTION RAW = MdeModulePkg/Logo/Logo.bmp
}

# !if 0 #gPlatformModuleTokenSpaceGuid.PcdTdsEnable == TRUE
[FV.FvTrustedDeviceSetupUnCompact]
BlockSize          = 0x10000
FvForceRebase      = FALSE
FvAlignment        = 16
ERASE_POLARITY     = 1
MEMORY_MAPPED      = TRUE
STICKY_WRITE       = TRUE
LOCK_CAP           = TRUE
LOCK_STATUS        = TRUE
WRITE_DISABLED_CAP = TRUE
WRITE_ENABLED_CAP  = TRUE
WRITE_STATUS       = TRUE
WRITE_LOCK_CAP     = TRUE
WRITE_LOCK_STATUS  = TRUE
READ_DISABLED_CAP  = TRUE
READ_ENABLED_CAP   = TRUE
READ_STATUS        = TRUE
READ_LOCK_CAP      = TRUE
READ_LOCK_STATUS   = TRUE
FvNameGuid         = 30F01DDB-2B1E-49EF-8581-516D98B8DE27
# !endif  # gPlatformModuleTokenSpaceGuid.PcdTdsEnable == TRUE

[FV.FvWifiDxeUnCompact]
!if gPlatformModuleTokenSpaceGuid.PcdNetworkEnable == TRUE

  #
  # WifiConnectionManagerDxe Related
  #
  !if gChipsetPkgTokenSpaceGuid.PcdUefiWirelessCnvtEnable == TRUE
  INF $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Universal/WifiConnectionManagerDxe/WifiConnectionManagerDxe.inf
  !endif

!endif

[FV.FvBlueToothDxeUnCompact]

[FV.FvNetworkDxeUnCompact]

[FV.FW_BINARIES_FV]
BlockSize          = 0x00001000
FvAlignment        = 16
ERASE_POLARITY     = 1
MEMORY_MAPPED      = TRUE
STICKY_WRITE       = TRUE
LOCK_CAP           = TRUE
LOCK_STATUS        = TRUE
WRITE_DISABLED_CAP = TRUE
WRITE_ENABLED_CAP  = TRUE
WRITE_STATUS       = TRUE
WRITE_LOCK_CAP     = TRUE
WRITE_LOCK_STATUS  = TRUE
READ_DISABLED_CAP  = TRUE
READ_ENABLED_CAP   = TRUE
READ_STATUS        = TRUE
READ_LOCK_CAP      = TRUE
READ_LOCK_STATUS   = TRUE
WEAK_ALIGNMENT     = TRUE
FvNameGuid         = 7bbb3e42-5a6a-4080-8077-cf05e6cf8d2c

[FV.DXEFV]
#!disable INF InsydeModulePkg/Csm/8259InterruptControllerDxe/8259.inf
#
# Override H2ODebug/DebugConfig.exe
#
!if gInsydeTokenSpaceGuid.PcdH2ODdtSupported
!disable InsydeModulePkg/H2ODebug/$(H2O_DDT_DEBUG_IO)DebugIoDxe/$(H2O_DDT_DEBUG_IO)DebugIoDxe.inf
!disable InsydeModulePkg/H2ODebug/DebugEngineDxe/DebugEngineDxe.inf
INF  APRIORI=0xC000 $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/H2ODebug/$(H2O_DDT_DEBUG_IO)DebugIoDxe/$(H2O_DDT_DEBUG_IO)DebugIoDxe.inf
INF  APRIORI=0xC000 $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/H2ODebug/DebugEngineDxe/DebugEngineDxe.inf
!endif
!disable InsydeModulePkg/Universal/ReportStatusCodeRouter/Smm/ReportStatusCodeRouterSmm.inf
INF APRIORI=0xC000 InsydeModulePkg/Universal/ReportStatusCodeRouter/Smm/ReportStatusCodeRouterSmm.inf

!if gInsydeTokenSpaceGuid.PcdH2OPeiTimerSupported
INF APRIORI $(CHIPSET_PKG)/ExtendPeiTimer/ExtendPeiTimer.inf
!endif
!if gEfiTraceHubTokenSpaceGuid.PcdStatusCodeUseTraceHub
INF APRIORI=0xC000 $(CHIPSET_PKG)/TraceHubPostCodeHandler/Dxe/TraceHubPostCodeHandlerDxe.inf
!endif
INF $(CHIPSET_PKG)/AsfSecureBootSmm/AsfSecureBootSmm.inf
INF $(CHIPSET_PKG)/AsfSecureBootDxe/AsfSecureBootDxe.inf
INF $(CHIPSET_PKG)/Universal/SmbiosProcessorDxe/SmbiosProcessorDxe.inf # RPPO-KBL-0036
INF $(CHIPSET_PKG)/RestoreMtrrDxe/RestoreMtrrDxe.inf
INF $(CHIPSET_PKG)/UsbLegacyControlSmm/UsbLegacyControlSmm.inf
INF $(CHIPSET_PKG)/UefiSetupUtilityDxe/SetupUtilityDxe.inf
INF $(CHIPSET_PKG)/FrontPageDxe/FrontPageDxe.inf
INF $(CHIPSET_PKG)/SataDevSleepDxe/SataDevSleepDxe.inf
#[-start-200420-IB17800056-5-modify]#
INF $(CHIPSET_PKG)/Platform/SmmPlatform/Smm/SmmPlatform.inf
#[-end-200420-IB17800056-5-modify]#
INF $(CHIPSET_PKG)/BbstableHookDxe/BbstableHookDxe.inf

!if gInsydeTokenSpaceGuid.PcdH2OCsmSupported == TRUE
FILE FREEFORM = PCD (gChipsetPkgTokenSpaceGuid.PcdPtOpRomFile) {
  SECTION RAW = $(CHIPSET_PKG)/CsmInt10BlockDxe/PTOpRom/PTOpRom.bin
}
INF $(CHIPSET_PKG)/CsmInt10BlockDxe/CsmInt10BlockDxe.inf
FILE FREEFORM = PCD (gChipsetPkgTokenSpaceGuid.PcdVbiosOpRomFile) {
  SECTION RAW = $(CHIPSET_PKG)/CsmInt10HookSmm/VbiosWaOpRom/VbwOpRom.bin
}
INF $(CHIPSET_PKG)/CsmInt10HookSmm/CsmInt10HookSmm.inf
INF $(CHIPSET_PKG)/CsmInt15HookSmm/CsmInt15HookSmm.inf
!endif

INF $(CHIPSET_PKG)/BiosRegionLockDxe/BiosRegionLockDxe.inf
!if gSiPkgTokenSpaceGuid.PcdAmtEnable
  INF $(CHIPSET_PKG)/Features/AMT/AmtLockPs2ConInDxe/AmtLockPs2ConInDxe.inf
  INF $(CHIPSET_PKG)/Features/AMT/AmtLockI2cConInDxe/AmtLockI2cConInDxe.inf
!if gInsydeTokenSpaceGuid.PcdH2OUsbSupported
  INF $(CHIPSET_PKG)/Features/AMT/AmtLockUsbConInDxe/AmtLockUsbConInDxe.inf
!endif
!endif

!if gInsydeTokenSpaceGuid.PcdDynamicHotKeySupported
INF $(CHIPSET_PKG)/DynamicHotKeyDxe/DynamicHotKeyDxe.inf
!endif
#[-start-201221-IB16740127-remove]# The judgment was removed in RC1515
#!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == TRUE
INF  IntelFsp2WrapperPkg/FspWrapperNotifyDxe/FspWrapperNotifyDxe.inf
!endif
#!endif
#[-end-201221-IB16740127-remove]#

INF $(CHIPSET_PKG)/ChipsetSvcDxe/ChipsetSvcDxe.inf
#[-start-190909-IB11270246-add]#
INF $(CHIPSET_PKG)/ChipsetSvcSmm/ChipsetSvcSmm.inf
#[-end-190909-IB11270246-add]#
INF $(CHIPSET_PKG)/OemBadgingSupportDxe/OEMBadgingSupportDxe.inf
#
#  I2C Platform Specific drvier.
#
INF $(CHIPSET_PKG)/Features/I2cPlatformSpecificDxe/I2cPlatformSpecificDxe.inf
#
#  I2C Master drvier
#
INF $(CHIPSET_PKG)/I2cMaster/Dxe/I2cMasterDxe.inf
INF $(CHIPSET_PKG)/CommonChipset/SetSsidSvidDxe/SetSsidSvidDxe.inf
INF $(CHIPSET_PKG)/CommonChipset/SpeakerDxe/LegacySpeakerDxe.inf
!if gSiPkgTokenSpaceGuid.PcdBiosGuardEnable
INF $(CHIPSET_PKG)/Platform/MsdmUpdateSmm/MsdmUpdateSmm.inf
!endif
INF $(CHIPSET_PKG)/PolicyInit/InsydeChipsetPolicy/GopPolicyDxe/GopPolicyDxe.inf
INF EDK2/PcAtChipsetPkg/HpetTimerDxe/HpetTimerDxe.inf

!if gSiPkgTokenSpaceGuid.PcdITbtEnable == TRUE
INF  $(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/ITbtInit/Dxe/ITbtDxe.inf
!endif
!if gPlatformModuleTokenSpaceGuid.PcdDTbtEnable == TRUE
INF RuleOverride = DRIVER_ACPITABLE $(PLATFORM_FEATURES_PATH)/Tbt/TbtInit/Dxe/DTbtDxe.inf
!endif

INF $(CHIPSET_PKG)/OemModifyOpRegionDxe/OemModifyOpRegionDxe.inf
INF APRIORI $(CHIPSET_PKG)/OemAcpiPlatformDxe/OemAcpiPlatformDxe.inf
INF $(CHIPSET_PKG)/UpdateDsdtByAcpiSdtDxe/UpdateDsdtByAcpiSdtDxe.inf
INF $(CHIPSET_PKG)/MemInfoDxe/MemInfoDxe.inf
INF $(CHIPSET_PKG)/VbiosHookSmm/VbiosHookSmm.inf
INF $(CHIPSET_PKG)/IhisiSmm/IhisiSmm.inf

!if gChipsetPkgTokenSpaceGuid.PcdMeCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdIshCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdEcCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdPdtCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdIomCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdMgPhyCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdTbtCapsuleUpdateSupported == TRUE
!if gInsydeTokenSpaceGuid.PcdH2OIhisiFmtsSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleSmm/MeCapsuleSmm.inf
!endif
!endif
!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport != 0
INF $(CHIPSET_PKG)/SwapAddressRangeDxe/SwapAddressRangeDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdHybridGraphicsSupported
INF $(CHIPSET_PKG)/HybridGraphicsDxe/HybridGraphicsDxe.inf
INF $(CHIPSET_PKG)/HybridGraphicsSmm/HybridGraphicsSmm.inf
#[-start-210722-QINGLIN0002-modify]#
!if $(LCFC_SUPPORT_ENABLE) == NO
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/AmdDiscreteSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/AmdPowerXpressSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/AmdUltPowerXpressSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N17/NvidiaDiscreteSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N17/NvidiaOptimusSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N17/NvidiaUltDiscreteSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N17/NvidiaUltOptimusSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaDiscreteSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaOptimusSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaUltDiscreteSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaUltOptimusSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaDiscreteSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaOptimusSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaUltDiscreteSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaUltOptimusSsdt.inf
!endif
#[-start-210918-QINGLIN0068-modify]#
!if $(S570_SUPPORT_ENABLE) == YES
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaOptimusSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaOptimusSsdt.inf
!endif
#[-end-210918-QINGLIN0068-modify]#
#[-end-210722-QINGLIN0002-modify]#
#[-start-210902-GEORGE0003-add]#
#[-start-210915-GEORGE0004-modify]#
!if ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaOptimusSsdt.inf
INF RuleOverride = ACPITABLE $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaOptimusSsdt.inf
!endif
#[-end-210915-GEORGE0004-modify]#
#[-end-210902-GEORGE0003-add]#
!endif

!if gChipsetPkgTokenSpaceGuid.PcdMeCapsuleUpdateSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleDxe/MeCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdIshCapsuleUpdateSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleIshDxe/IshCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdPdtCapsuleUpdateSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsulePdtDxe/PdtCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdEcCapsuleUpdateSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleEcDxe/EcCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport != 0

!if gChipsetPkgTokenSpaceGuid.PcdMonolithicCapsuleUpdateSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleMonolithicDxe/MonolithicCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2
!if gChipsetPkgTokenSpaceGuid.PcdUcodeCapsuleUpdateSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleUcodeDxe/UcodeCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdBtGAcmCapsuleUpdateSupported == TRUE
INF  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleBtGAcmDxe/BtGAcmCapsuleDxe.inf
!endif
!endif

!endif

!if gChipsetPkgTokenSpaceGuid.PcdRetimerCapsuleUpdateSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtRetimer1Dxe/TbtRetimerCapsule1Dxe.inf
#[-start-211109-BAIN000054-modify]#
!if ($(C970_SUPPORT_ENABLE) == YES) OR ($(C770_SUPPORT_ENABLE) == YES) OR ($(S77013_SUPPORT_ENABLE) == YES) OR ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)

!else
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtRetimer2Dxe/TbtRetimerCapsule2Dxe.inf
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtRetimer3Dxe/TbtRetimerCapsule3Dxe.inf
!endif
#[-end-211109-BAIN000054-modify]#

!endif
!if gChipsetPkgTokenSpaceGuid.PcdTcssPartialUpdateEnable == TRUE

!if gChipsetPkgTokenSpaceGuid.PcdIomCapsuleUpdateSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleIomDxe/IomCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdMgPhyCapsuleUpdateSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleMgPhyDxe/MgPhyCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdTbtCapsuleUpdateSupported == TRUE
INF $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtDxe/TbtCapsuleDxe.inf
!endif

!endif

!if gPlatformModuleTokenSpaceGuid.PcdMeResiliencyEnable == TRUE
INF $(CHIPSET_PKG)/Recovery/MeUpdateFaultToleranceDxe/MeUpdateFaultToleranceDxe.inf
!endif

INF $(CHIPSET_PKG)/SmbiosUpdateDxe/SmbiosUpdateDxe.inf

!if gChipsetPkgTokenSpaceGuid.PcdRestoreCmosfromVariableFlag
INF $(CHIPSET_PKG)/SaveRestoreCmos/SaveCmosDxe/SaveCmosDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdSupportUnLockedBarHandle
INF $(CHIPSET_PKG)/UnLockedBarHandleSmm/UnLockedBarHandleSmm.inf
!endif
#
# Unmark to launch DXE/SMM to output some status code from reserved memory for verification.
#
#!if gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseMemory == 1
#INF $(CHIPSET_PKG)/StatusCodeHandlerTestDxe/StatusCodeHandlerTestDxe.inf
#INF $(CHIPSET_PKG)/StatusCodeHandlerTestSmm/StatusCodeHandlerTestSmm.inf
#!endif

!if gChipsetPkgTokenSpaceGuid.PcdTdsEnable == TRUE
  INF $(PLATFORMSAMPLE_PACKAGE)/Features/TrustedDeviceSetup/TrustedDeviceSetup.inf
!endif

# Trusted Device Setup (TDS)

!if gPlatformModuleTokenSpaceGuid.PcdTdsEnable == TRUE
FILE APPLICATION = 658D56F0-4364-4721-B70E-732DDC8A2771 {
    SECTION PE32 = $(PLATFORMSAMPLE_PACKAGE)/Features/TrustedDeviceSetup/Intel_TDS_Extension.efi
    SECTION UI =  "TrustedDeviceSetupApp"
}
!endif

#
# SmBios Table Type.222
#
#[-start-200420-IB17800056-modify]#
#  INF $(CLIENT_COMMON_PACKAGE)/Universal/IsvtCheckpointDxe/IsvtCheckpointDxe.inf
#[-end-200420-IB17800056-modify]#

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
#
# Note: When integrated RC please check PostMem.fdf. Because Insyde kernel cannot use "!include" in fdf file.
#
  # !Include UfsFeaturePkg/Include/PostMem.fdf
  INF MdeModulePkg/Bus/Pci/UfsPciHcDxe/UfsPciHcDxe.inf
  INF MdeModulePkg/Bus/Ufs/UfsPassThruDxe/UfsPassThruDxe.inf
  INF UfsFeaturePkg/UfsPlatform/UfsPlatform.inf
!endif

################################################################################
#
# FV.DXEFV Override
#
################################################################################
#[-start-190612-IB16990056-add]#
# #
# #  int 15 hook override
# #
# !if gInsydeTokenSpaceGuid.PcdH2OCsmSupported
# FILE FREEFORM = PCD (gInsydeTokenSpaceGuid.PcdInt15ServiceSmmRomFile) {
#     !disable  SECTION RAW = InsydeModulePkg/Csm/BiosThunk/Int15ServiceSmm/Int15Rom/Int15Rom.bin
#     SECTION RAW = $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Csm/BiosThunk/Int15ServiceSmm/Int15Rom/Int15Rom.bin
# }
# !endif
#[-end-190612-IB16990056-add]#

#
# The override should be removed after the DDT debug drivers are ready for SKL from kernel code.
#
!if gSiPkgTokenSpaceGuid.PcdAmtEnable
INF InsydeModulePkg/Universal/StatusCode/DatahubStatusCodeHandlerDxe/DatahubStatusCodeHandlerDxe.inf
!endif

INF  APRIORI=0xC000 $(CHIPSET_PKG)/SmmConfidentialMem/SmmConfidentialMem.inf

!disable InsydeModulePkg/Universal/Security/HstiDxe/HstiDxe.inf
#[-start-200724-IB17040135-add]#
##
## Add from RC BoardPkg.fdf
##
################################################################################
#
# Rules are use with the [FV] section's module INF type to define
# how an FFS file is created for a given INF file. The following Rule are the default
# rules for the different module type. User can add the customized rules to define the
# content of the FFS file.
#
################################################################################

[Rule.Common.DXE_DRIVER.DRIVER_ACPITABLE]
  FILE DRIVER = $(NAMED_GUID) {
    DXE_DEPEX DXE_DEPEX Optional       $(INF_OUTPUT)/$(MODULE_NAME).depex
    PE32      PE32                     $(INF_OUTPUT)/$(MODULE_NAME).efi
    RAW ACPI  Optional                |.acpi
    RAW ASL   Optional                |.aml
    UI        STRING="$(MODULE_NAME)" Optional
    VERSION   STRING="$(INF_VERSION)" Optional BUILD_NUM=$(BUILD_NUMBER)
  }
#[-end-200724-IB17040135-add]#