/** @file
  Intel ACPI Sample Code for Discrete Connectivity module

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

OperationRegion(RPXX, SystemMemory, GMIO(PCIE_ROOT_PORT._ADR, PCIE_ROOT_PORT.PXSX._ADR), 0x30)
Field(RPXX, AnyAcc, NoLock, Preserve)
{
  Offset(0),     // Vendor-Device ID
  VDID, 32,
  Offset(0x2C),  // SVID
  SVID, 16,
}

OperationRegion (FLDR, SystemMemory, Add(GMIO(PCIE_ROOT_PORT._ADR, PCIE_ROOT_PORT.PXSX._ADR), 0x44), 0x06)
Field (FLDR, ByteAcc, NoLock, Preserve)
{
  DCAP, 32,
  DCTR, 16,
}

// WIST (WiFi Device Presence Status)
//
// Check if a WiFi Device is present on the RootPort.
//
// Arguments: (0)
//   None
// Return Value:
//    0 - if a device is not present.
//    1 - if a device is present.
//
Method(WIST,0,Serialized)
{
  // check Vendor-Device ID for supported devices
  If(CondRefOf(VDID)){
    Switch(ToInteger(VDID)){
      Case(0x25268086){Return(1)} // Intel Wireless-AC 9260(Thunder Peak 2)
      Case(0x271B8086){Return(1)} // Intel Wireless-AC 9260(Thunder Peak 1)
      Case(0x27238086){Return(1)} // Cyclone Peak
      Case(0x27258086){Return(1)} // Typhoon Peak
//[start-210821-STORM1109-modify]
//[-start-210906-QINGLIN0041-modify]//
#ifdef LCFC_SUPPORT
      Case(0x885210EC){Return(1)} // Realtek RTL8852AE
      Case(0xB85210EC){Return(1)} // Realtek RTL8852BE
      Case(0xC82210EC){Return(1)} // Realtek RTL8822CE
#endif
//[-end-210906-QINGLIN0041-modify]//
//[end-210821-STORM1109-modify]
      Default{
        Return(0)                 // no supported device
      }
    }
  }Else{
    Return(0)
  }
}

// WWST (WWAN Device Presence Status)
//
// Check if a WWAN Device is present on the RootPort.
//
// Arguments: (0)
//   None
// Return Value:
//    0 - if a device is not present.
//    1 - if a device is present.
//
Method(WWST,0,Serialized)
{
  // check Vendor-Device ID for supported devices
  If(CondRefOf(VDID)){
    Switch(ToInteger(VDID)){
      Case(0x73608086){Return(1)} // XMM7360
      Case(0x75608086){Return(1)} // XMM7560
      Case(0x4D7514C3){Return(1)} // M80
      Default{Return(0)}          // no supported device
    }
  }Else{
    Return(0)
  }
}

//
// Load Wifi/BT/WiGig tables only the Vendor ID, Device ID matches
//
If (WIST())
{
  Include("Wifi.asl")
}

//
// Load WWAN tables only the Vendor ID, Device ID matches
//
If(LAnd(LNotEqual(WWEN, 0), LEqual(WWRP, SLOT)))
{
  Include("Wwan.asl")
}

//
// Load Platform-level device reset tables only the Vendor ID, Device ID matches
//
If (LOr(WIST(), LAnd(LNotEqual(WWEN, 0), LEqual(WWRP, SLOT))))
{
  Include("DiscreteConnectivityReset.asl")
  Include("DiscreteConnectivityDsm.asl")
}