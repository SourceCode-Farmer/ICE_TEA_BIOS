/** @file
  The PEI Library Implements ME Init.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include "MbpData.h"
#include <Library/DebugLib.h>
#include <Register/MeRegs.h>
#include <Library/PeiServicesLib.h>
#include <Library/HobLib.h>
#include <MeBiosPayloadHob.h>
#include <Library/PeiMeLib.h>
#include <MeFwHob.h>
#include <Library/PciSegmentLib.h>
#include <Library/HeciInitLib.h>
#include <Library/MeShowBufferLib.h>
#include <Library/BaseMemoryLib.h>
#include <AmtConfig.h>
#include <Library/MeFwStsLib.h>
#include <Library/PsfSocLib.h>
#include <Library/PchSbiAccessLib.h>
#include <Library/PeiItssLib.h>
#include <Ppi/SiPolicy.h>
#include <Register/HeciRegs.h>
#include <Library/TimerLib.h>
#include "Library/MeTypeLib.h"
#include "MeInitPostMem.h"

/**
  Disable ME Devices when needed (Client version)

  @param[in] IsHeci3Supported    Determines if HECI3 is supported and should be enabled

  @retval EFI_STATUS             Operation status
**/
EFI_STATUS
MeDeviceConfigure (
  IN BOOLEAN                      IsHeci3Supported
  )
{
  EFI_STATUS                      Status;
  SI_PREMEM_POLICY_PPI            *SiPreMemPolicyPpi;
  ME_PEI_PREMEM_CONFIG            *MePeiPreMemConfig;
  ME_BIOS_BOOT_PATH               MeBiosPath;
  ME_DEV_EXPOSURE                 MeDeviceExposure;
  UINT64                          DevicePciCfgBase;
#if FixedPcdGetBool(PcdAmtEnable) == 1
  SI_POLICY_PPI                   *SiPolicy;
  AMT_PEI_CONFIG                  *AmtPeiConfig;
#endif

  DEBUG ((DEBUG_INFO, "MeDeviceConfigure () - Start\n"));

  ///
  /// Step 1. Determine which devices should be hidden depending on specific Me-Boot Path
  ///
  MeDeviceExposure = GetBootPathMeDevHidePolicy ();

  ///
  /// Step 2. Perform device disabling based on FW capability and feature state
  ///         in all boot mode if it is not listed by ME Bios Boot path.
  ///         Policy can only control device disabling when the device is not
  ///         specified to be disabled by any non MeNormalBiosPath.
  ///

  ///
  /// 2-a. Disable HECI2 with CSME in all boot mode - already in MeBiosPath.
  ///
  //
  // Disable Heci2 if and only if policy dictates
  //
  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gMePeiPreMemConfigGuid, (VOID *) &MePeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  if (!EFI_ERROR (Status)) {
    if (!MeHeci2Enabled ()) {
      MeDeviceExposure |= HIDE_MEI2;
    }
  }

  ///
  /// 2-b. Disable Heci3 when no support needed
  ///
  if (!IsHeci3Supported) {
    MeDeviceExposure |= HIDE_MEI3;
  }

#if FixedPcdGetBool(PcdAmtEnable) == 1
  if (MeTypeIsClient ()) {
    Status = PeiServicesLocatePpi (
               &gSiPolicyPpiGuid,
               0,
               NULL,
               (VOID **) &SiPolicy
              );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Get SiPolicy fail\n"));
      return Status;
    }
    Status = GetConfigBlock ((VOID *) SiPolicy, &gAmtPeiConfigGuid, (VOID *) &AmtPeiConfig);
    ASSERT_EFI_ERROR (Status);
    //
    // Since P2SB lock in EndOfPei phase, move all SOL disable check here.
    //
    if (AmtPeiConfig->AmtSolEnabled == 0) {
      MeDeviceExposure |= HIDE_SOL;
    }
  }
#else
  MeDeviceExposure |= HIDE_SOL;
#endif

  ///
  /// Step 3. Perform checking for specific boot mode with ME Bios Boot Path,
  ///         like MeDisableSecoverMeiMsgBiosPath. HECI1 shall be disabled
  ///         after HMRFPO_DISABLE_MSG sent in previous POST boot path
  ///
  if (GetBootModeHob () == BOOT_ON_S3_RESUME) {
    MeBiosPath = CheckMeBootPath ();
    if ((MeBiosPath == SecoverMeiMsgPath) || (MeBiosPath == SwTempDisablePath) || (MeBiosPath == SecoverJmprPath)) {
      MeDeviceExposure |= HIDE_MEI1;
    }
  }

  ///
  /// Save MEI FWSTS registers set before disable it. The MEI device access right will be removed
  /// by late POST, hence save current FWSTS before disable them for reference without further enabling
  /// MEI steps required. The HOB data might be updated if gets failure when send EOP message in PEI
  /// phase, then FWSTS registers will be updated to reflect the last status before disable rest MEI devices
  ///
  SaveFwStsToHob ();

  if ((MeDeviceExposure & HIDE_MEI1)) {
    HeciDisable ();
  }
  if ((MeDeviceExposure & HIDE_MEI2)) {
    Heci2Disable ();
  }
  if ((MeDeviceExposure & HIDE_MEI3)) {
    Heci3Disable ();
  }
  if ((MeDeviceExposure & HIDE_SOL)) {
    SolDisable ();
  }

  ///
  /// Always disable IDE-r with CSME in all boot mode.
  ///
  IderDisable ();

  ///
  /// HECI4 interface is not used and should be disabled by following below steps:
  ///  1. Temporary initialize HECI4 memory (program BAR, set MSE)
  ///  2. Set D0i3 bit in HECI4 memory space
  ///  3. Clear BAR and Command register
  ///  4. Disable device on PSF
  ///
  MeDeviceInit (HECI4, PcdGet32 (PcdSiliconInitTempMemBaseAddr), 0);
  SetD0I3Bit ((UINT32) HECI4);
  ///
  /// Clear BAR and CMD register
  ///
  DevicePciCfgBase = PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI4, 0);
  PciSegmentAnd8 (DevicePciCfgBase + PCI_COMMAND_OFFSET, (UINT8)~(EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_BUS_MASTER));
  PciSegmentWrite32 (DevicePciCfgBase + PCI_BASE_ADDRESSREG_OFFSET, 0);
  PsfDisableDevice (PsfHeciPort (4));
  DEBUG ((DEBUG_INFO, "Disabling CSME device 0:22:%d\n", (UINT8) HECI4));

  return EFI_SUCCESS;
}

/**
  Configure HECI devices on End Of Pei

  @param[in]  PeiServices        Pointer to PEI Services Table.
  @param[in]  NotifyDescriptor   Pointer to the descriptor for the Notification event that caused this function to execute.
  @param[in]  Ppi                Pointer to the PPI data associated with this function.

  @retval     EFI_SUCCESS        The function completes successfully

**/
EFI_STATUS
EFIAPI
MeOnEndOfPei (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
#if FixedPcdGetBool(PcdFspBinaryEnable) == 0
  UINT32               HeciBars[PCI_MAX_BAR];
  UINT8                MeDevFunction;
#endif

  DEBUG ((DEBUG_INFO, "%a() enter\n", __FUNCTION__));

  if (GetBootModeHob () == BOOT_ON_S3_RESUME) {
#if FixedPcdGetBool(PcdFspBinaryEnable) == 0
      if (MeHeciD0I3Enabled () == FALSE) {
        return EFI_SUCCESS;
      }
      //
      // Set D0I3 bits if resuming from S3 for EDK2.
      // For FSP case, it will be set in MeEndOfFirmwareEvent.
      // We have to make sure the HECI BARs are 32-bit here after restoring S3 boot script:
      //  1. Save MMIO BAR addresses for all HECI devices.
      //  2. Set to default (32-bit) BAR addresses for all HECI devices.
      //  3. Set D0i3 bit
      //  4. Restore MMIO BAR addresses for all HECI devices.
      //
      DEBUG ((DEBUG_INFO, "Setting D0I3 bits for HECI devices on S3 resume path\n"));
      for (MeDevFunction = HECI1; MeDevFunction <= HECI4; MeDevFunction++) {
        if (!IsHeciDeviceFunction (MeDevFunction)) {
          continue;
        }
        MeSaveBars (MeDevFunction, HeciBars);
        MeDeviceInit (MeDevFunction, MeGetHeciBarAddress (MeDevFunction), 0);
        SetD0I3Bit (MeDevFunction);
        MeRestoreBars (MeDevFunction, HeciBars);
      }
#endif
  } else {
    if (MeTypeIsClient ()) {
      //
      // Send optional HECI command
      //
      SendOptionalHeciCommand ();

      CreateMkhiVersionHob ();
    }

    //
    // Save ME/AMT policies in PEI phase to HOB for using in DXE.
    //
    SavePolicyToHob ();
  }

  DEBUG ((DEBUG_INFO, "%a() done.\n", __FUNCTION__));
  return EFI_SUCCESS;
}

/**
  Build Sps Guid Data Hob

  @param[in] SiPreMemPolicyPpi     Pointer to PreMem policy PPI
**/
VOID
BuildSpsGuidDataHob (
  IN SI_PREMEM_POLICY_PPI *SiPreMemPolicyPpi
  )
{
  return;
}

//
// This constant should be at least equal to GetPchMaxPciePortNum () + 1
// (PCIe Root Ports and PEG Graphics)
//
#define MAX_MCTP_TARGET_TABLE_ENTRIES 30
/**
  Configure MCTP when needed

  @param[in] MbpHob                   Pointer to MbpHob
  @param[in] MePeiConfig              Pointer to ME PEI Config
**/
VOID
MctpConfigure (
  IN ME_BIOS_PAYLOAD_HOB *MbpHob,
  IN ME_PEI_CONFIG       *MePeiConfig
  )
{
  PSF_PORT_DEST_ID  MctpTargetIdTable[MAX_MCTP_TARGET_TABLE_ENTRIES];
  UINT32            MctpTargetIdTableSize;

  if (IsMctpConfigurationSupported (MbpHob, MePeiConfig)) {
    MctpTargetIdTableSize = MctpTargetsTable (MctpTargetIdTable,
                                              MAX_MCTP_TARGET_TABLE_ENTRIES,
                                              PsfGetSegmentTable (),
                                              PsfGetRootPciePortTable ());
    PsfConfigureMctpCycle (MctpTargetIdTable,
                           MctpTargetIdTableSize,
                           PsfGetSegmentTable (),
                           PsfGetMctpRegDataTable (),
                           PsfGetRcOwner ());
  }
}

/**
  Notify other silicon components that DRAM_INIT_DONE message can be sent
  and all pre-DramInitDone action are completed
  Used in Server implementation
**/
VOID
NotifyMeReadyForSendingDramInitDone (
  VOID
  )
{

}
