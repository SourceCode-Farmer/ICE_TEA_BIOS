/** @file
  For I2C I2cPlatform Specific Dxe Driver

;******************************************************************************
;* Copyright (c) 2019 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "I2cPlatformSpecificDxe.h"
#include <Library/PchPciBdfLib.h>

/**
  Get the amount of I2c bus(bridge) on your platform.

  This routine must be called at or below TPL_NOTIFY.


  @param[out]   TotalControllerNumber  The max number of I2c bus.

  @retval EFI_SUCCESS       The function completed successfully.
  @retval EFI_ERROR         The function operation failed.

**/
EFI_STATUS
EFIAPI
GetMaxI2cControllerNumber(
  OUT UINT8              *TotalControllerNumber
 )
{
  EFI_STATUS             Status;

  Status = EFI_SUCCESS;

  *TotalControllerNumber = GetPchMaxSerialIoI2cControllersNum();
  DEBUG ((DEBUG_INFO, "I2cPlatformSpecificDxe - GetPchMaxSerialIoI2cControllersNum : %x \n", *TotalControllerNumber));

  return Status;
}


VOID
ChipInit (
  IN  I2C_BUS_INSTANCE   *I2cBusInstance
  )
{
  UINT32   OriRegValue;
  UINT32   NewRegValue;
  UINT64   PciCfgBase;
  //
  // Prepare I2c controller for use: put in D0 and get out of reset.
  //
  DEBUG ((DEBUG_INFO, "I2cPlatformSpecificDxe - Prepare I2c controller for using: Put in D0.\n"));
  PciCfgBase = PCI_SEGMENT_LIB_ADDRESS (
                 DEFAULT_PCI_SEGMENT_NUMBER_PCH,
                 (UINT8)I2cBusInstance->PciI2c.Bus,
                 (UINT8)I2cBusInstance->PciI2c.Dev,
                 (UINT8)I2cBusInstance->PciI2c.Func,
                 0
                 );
  SerialIoSetD0 (PciCfgBase);

  OriRegValue = I2cBusInstance->I2cHcRead32(I2cBusInstance, R_SERIAL_IO_MEM_PPR_RESETS);
  if ((OriRegValue & (B_SERIAL_IO_MEM_PPR_RESETS_FUNC | B_SERIAL_IO_MEM_PPR_RESETS_APB | B_SERIAL_IO_MEM_PPR_RESETS_IDMA)) != 0x07) {
    NewRegValue = OriRegValue | (B_SERIAL_IO_MEM_PPR_RESETS_FUNC | B_SERIAL_IO_MEM_PPR_RESETS_APB | B_SERIAL_IO_MEM_PPR_RESETS_IDMA);
    I2cBusInstance->I2cHcWrite32(I2cBusInstance, R_SERIAL_IO_MEM_PPR_RESETS, NewRegValue);
    DEBUG ((DEBUG_INFO, "  -  Get out of reset.\n"));
  }
}

UINTN
GetDevMode (
  IN I2C_BUS_INSTANCE      *I2cBusInstance
  )
{
  return I2C_DEVICE_MODE_PCI;
}

VOID
SwitchDevMode (
  IN I2C_BUS_INSTANCE      *I2cBusInstance,
  IN UINT32                DeviceMode
  )
{
  return;
}

BOOLEAN
GpioEventArrived(
  IN  I2C_BUS_INSTANCE   *I2cBusInstance,
  IN  UINT8              IntGpioController,
  IN  UINT8              PadNumber,
  IN  UINT8              ActiveLevel
  )
{
  UINT32                 GpioTableLength;
  UINT32                 Gplr;
  CONST GPIO_GROUP_INFO  *GpioGroupInfo;

  GpioGroupInfo = GpioGetGroupInfoTable (&GpioTableLength);

  if (IntGpioController  >= GpioTableLength) {
    return FALSE;
  }

  //Gplr = MmioRead32 ((UINTN)((GpioGroupInfo[IntGpioController].Community<<16)|(mGpioPadCfgOffset * GpioIntPin + GpioGroupInfo[IntGpioController].Offset)|0xFD000000));
  // Pad Configuration DW0
  Gplr = MmioRead32 (PCH_PCR_ADDRESS (GpioGroupInfo[IntGpioController].Community, GpioGroupInfo[IntGpioController].PadCfgOffset + PadNumber * S_GPIO_PCR_PADCFG));

  if (((Gplr & 2) >> 1) == ActiveLevel) {
    return TRUE;
  }

  return FALSE;
}

VOID
GetOemConfigI2cSclSda (
  IN OUT  I2C_BUS_INSTANCE    *I2cBusInstance
  )
{
  I2cBusInstance->Info.SpeedClockInfo[0].SclHcnt = PcdGet16(I2cIcSsSclHcnt);
  I2cBusInstance->Info.SpeedClockInfo[0].SclLcnt = PcdGet16(I2cIcSsSclLcnt);
  I2cBusInstance->Info.SpeedClockInfo[0].SdaHold = PcdGet8(I2cIcSsSdaHold);
  I2cBusInstance->Info.SpeedClockInfo[1].SclHcnt = PcdGet16(I2cIcFsSclHcnt);
  I2cBusInstance->Info.SpeedClockInfo[1].SclLcnt = PcdGet16(I2cIcFsSclLcnt);
  I2cBusInstance->Info.SpeedClockInfo[1].SdaHold = PcdGet8(I2cIcFsSdaHold);
  I2cBusInstance->Info.SpeedClockInfo[2].SclHcnt = PcdGet16(I2cIcHsSclHcnt);
  I2cBusInstance->Info.SpeedClockInfo[2].SclLcnt = PcdGet16(I2cIcHsSclLcnt);
  I2cBusInstance->Info.SpeedClockInfo[2].SdaHold = PcdGet8(I2cIcHsSdaHold);

  return;
}


/**
  Get the bus(bridge) data on your platform.

  This routine must be called at or below TPL_NOTIFY.


  @param[in]     This       Pointer to an EFI_I2C_PLATFORM_SPECIFIC_PROTOCOL structure.

  @retval EFI_SUCCESS       The function completed successfully.
  @retval EFI_ERROR         The function operation failed.

**/
EFI_STATUS
EFIAPI
GetI2cConfigData(
  IN       EFI_I2C_PLATFORM_SPECIFIC_PROTOCOL   *This,
  IN OUT   I2C_BUS_INSTANCE                     *I2cBusInstance,
  IN       UINTN                                ControllerNum,
  OUT     UINT8                                *I2cHcType
 )
{
  EFI_STATUS               Status;
  UINTN                    PciCfgBase;
  // UINT64                   BaseAddress;

  Status = EFI_SUCCESS;

  // BaseAddress = PCI_SEGMENT_LIB_ADDRESS (DEFAULT_PCI_SEGMENT_NUMBER_PCH, (UINT8)I2cBusInstance->PciI2c.Bus, (UINT8)I2cBusInstance->PciI2c.Dev, (UINT8)I2cBusInstance->PciI2c.Func, 0);
  PciCfgBase  = (UINTN) GetSerialIoI2cPciCfg ((UINT8)ControllerNum);
#if (FixedPcdGetBool(PcdAdlLpSupport) == 1)
  if (IsPchH ()) {
    if (mI2cBusInfoPchH[ControllerNum].I2cPciDevId != PciSegmentRead16 (PciCfgBase + PCI_DEVICE_ID_OFFSET)) {
      return EFI_DEVICE_ERROR;
    } else {
      CopyMem(&I2cBusInstance->Info, &(mI2cBusInfoPchH[ControllerNum]), sizeof(I2C_INFO));
    }
  } else if (IsPchP ()) {
    if (mI2cBusInfoPchP[ControllerNum].I2cPciDevId != PciSegmentRead16 (PciCfgBase + PCI_DEVICE_ID_OFFSET)) {
      return EFI_DEVICE_ERROR;
    } else {
      CopyMem(&I2cBusInstance->Info, &(mI2cBusInfoPchP[ControllerNum]), sizeof(I2C_INFO));
    }
  } else if (IsPchN ()) {
    if (mI2cBusInfoPchN[ControllerNum].I2cPciDevId != PciSegmentRead16 (PciCfgBase + PCI_DEVICE_ID_OFFSET)) {
      return EFI_DEVICE_ERROR;
    } else {
      CopyMem(&I2cBusInstance->Info, &(mI2cBusInfoPchN[ControllerNum]), sizeof(I2C_INFO));
    }
  } else {
    if (mI2cBusInfoPchLP[ControllerNum].I2cPciDevId != PciSegmentRead16 (PciCfgBase + PCI_DEVICE_ID_OFFSET)) {
      return EFI_DEVICE_ERROR;
    } else {
      CopyMem(&I2cBusInstance->Info, &(mI2cBusInfoPchLP[ControllerNum]), sizeof(I2C_INFO));
    }
  }
#else
  if (IsPchS ()) {
    if (mI2cBusInfoPchS[ControllerNum].I2cPciDevId != PciSegmentRead16 (PciCfgBase + PCI_DEVICE_ID_OFFSET)) {
      return EFI_DEVICE_ERROR;
    } else {
      CopyMem(&I2cBusInstance->Info, &(mI2cBusInfoPchS[ControllerNum]), sizeof(I2C_INFO));
    }
  }
#endif

  I2cBusInstance->PciI2c.Seg  = DEFAULT_PCI_SEGMENT_NUMBER_PCH;
  I2cBusInstance->PciI2c.Bus  = DEFAULT_PCI_BUS_NUMBER_PCH;
  I2cBusInstance->PciI2c.Dev  = (UINTN)SerialIoI2cDevNumber ((UINT8)ControllerNum);
  I2cBusInstance->PciI2c.Func = (UINTN)SerialIoI2cFuncNumber ((UINT8)ControllerNum);

  // I2cBusInstance->I2cBase     = 0xFE020000 + (UINT64)(0x1000 * ControllerNum * 2);
  I2cBusInstance->I2cBase     = GetSerialIoBar(PciCfgBase);
  I2cBusInstance->I2cBaseLen  = 0x1000;

  //
  //  For CFL/WHL/CML,
  //  Read GPIO value, MmioRead32 (PCH_PCR_ADDRESS (GpioGroupInfo[GroupIndex].Community, RegOffset));
  //
  I2cBusInstance->GpioBase    = 0xFD000000;

  *I2cHcType = I2C_HC_TYPE_PCI;

  if (PcdGetBool(OemHookI2cSclSdaEnable)) {
    GetOemConfigI2cSclSda (I2cBusInstance);
  }

  DEBUG ((DEBUG_INFO, "--- Get I2C Device DID:0x%04x(SEG:0x%02x/BUS:0x%02x/DEV:0x%02x/FUN:0x%02x) ConfigData --- \n", I2cBusInstance->Info.I2cPciDevId, I2cBusInstance->PciI2c.Seg, I2cBusInstance->PciI2c.Bus, I2cBusInstance->PciI2c.Dev, I2cBusInstance->PciI2c.Func));
  DEBUG ((DEBUG_INFO, "--- I2C Bus Bar0:0x%08x, Bar0 length:0x%08x, GpioBase:0x%08x\n", I2cBusInstance->I2cBase, I2cBusInstance->I2cBaseLen, I2cBusInstance->I2cBaseLen));
  DEBUG ((DEBUG_INFO, "--- I2C Bus Type (PCI:0/MMIO:1):%01x\n", *I2cHcType));
  DEBUG ((DEBUG_INFO, "--- SS I2C Clock SCL High Count: 0x%04x\n", I2cBusInstance->Info.SpeedClockInfo[0].SclHcnt));
  DEBUG ((DEBUG_INFO, "--- SS I2C Clock SCL Low Count : 0x%04x\n", I2cBusInstance->Info.SpeedClockInfo[0].SclLcnt));
  DEBUG ((DEBUG_INFO, "--- SS I2C Clock SDA Hold Time : 0x%04x\n", I2cBusInstance->Info.SpeedClockInfo[0].SdaHold));
  DEBUG ((DEBUG_INFO, "--- FS I2C Clock SCL High Count: 0x%04x\n", I2cBusInstance->Info.SpeedClockInfo[1].SclHcnt));
  DEBUG ((DEBUG_INFO, "--- FS I2C Clock SCL Low Count : 0x%04x\n", I2cBusInstance->Info.SpeedClockInfo[1].SclLcnt));
  DEBUG ((DEBUG_INFO, "--- FS I2C Clock SDA Hold Time : 0x%04x\n", I2cBusInstance->Info.SpeedClockInfo[1].SdaHold));
  DEBUG ((DEBUG_INFO, "--- HS I2C Clock SCL High Count: 0x%04x\n", I2cBusInstance->Info.SpeedClockInfo[2].SclHcnt));
  DEBUG ((DEBUG_INFO, "--- HS I2C Clock SCL Low Count : 0x%04x\n", I2cBusInstance->Info.SpeedClockInfo[2].SclLcnt));
  DEBUG ((DEBUG_INFO, "--- HS I2C Clock SDA Hold Time : 0x%04x\n", I2cBusInstance->Info.SpeedClockInfo[2].SdaHold));

  return Status;
}

/**
 Unloads an image.

 @param[in]         ImageHandle           Handle that identifies the image to be unloaded.

 @retval            EFI_SUCCESS           The image has been unloaded.
 @retval            EFI_INVALID_PARAMETER One of the protocol interfaces was not previously installed on ImageHandle.
**/
EFI_STATUS
EFIAPI
I2cPlatformSpecificDxeUnload (
  IN EFI_HANDLE  ImageHandle
  )
{
  EFI_STATUS  Status;

  //
  // Uninstall Driver Supported EFI Version Protocol from the image handle.
  //
  Status = gBS->UninstallProtocolInterface (
                ImageHandle,
                &gEfiI2cPlatformSpecificProtocolGuid,
                NULL
                );
  DEBUG ((DEBUG_INFO, "UninstallProtocolInterface gEfiI2cPlatformSpecificProtocolGuid : %x \n" , Status));

  return Status;
}


//
// The protocol interface is used to describe how to control or get platform specific data for I2C devices, controllers or bus.
//
EFI_STATUS
EFIAPI
I2cPlatformSpecificDxeEntry (
  IN      EFI_HANDLE            ImageHandle,
  IN      EFI_SYSTEM_TABLE      *SystemTable
  )
{
  EFI_STATUS                          Status;
  EFI_I2C_PLATFORM_SPECIFIC_PROTOCOL  *I2cPlatformSpecific;

  DEBUG ((DEBUG_INFO, "I2cPlatformSpecificDxeEntry Start. \n"));

  I2cPlatformSpecific = AllocateZeroPool(sizeof(EFI_I2C_PLATFORM_SPECIFIC_PROTOCOL));

  if (I2cPlatformSpecific == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    ASSERT_EFI_ERROR (Status);
    return Status;
  }
  
  I2cPlatformSpecific->Revision                  = I2C_PLATFORM_SPECIFIC_VERSION;
  I2cPlatformSpecific->GetMaxI2cControllerNumber = GetMaxI2cControllerNumber;
  I2cPlatformSpecific->GetI2cConfigData          = GetI2cConfigData;

  Status = gBS->InstallProtocolInterface (
                &ImageHandle,
                &gEfiI2cPlatformSpecificProtocolGuid,
                EFI_NATIVE_INTERFACE,
                I2cPlatformSpecific
                );

  DEBUG ((DEBUG_INFO, "I2cPlatformSpecificDxeEntry End. \n"));
  return Status;
}
