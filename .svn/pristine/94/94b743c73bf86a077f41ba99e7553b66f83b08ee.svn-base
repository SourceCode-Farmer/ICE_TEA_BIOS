## @file
#******************************************************************************
#* Copyright (c) 2015 - 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************#
### @file
# Component information file for SMM Platform module
#
# {CB73C3D9-2F01-4342-AE67-04DDE5264092}
# 0xcb73c3d9, 0x2f01, 0x4342, 0xae, 0x67, 0x4, 0xdd, 0xe5, 0x26, 0x40, 0x92)
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2018 - 2019 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = SmmPlatform
  FILE_GUID                      = 504f2f1f-e7eb-46f9-aa07-0606f311aa1a
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = DXE_SMM_DRIVER
  PI_SPECIFICATION_VERSION       = 1.20
  ENTRY_POINT                    = InitializeInsydePlatformSmm

[LibraryClasses]
  UefiDriverEntryPoint
  BaseMemoryLib
  BasePciLibPciExpress
  DebugLib
  ReportStatusCodeLib
  EcMiscLib
  SmmServicesTableLib
  PmcLib
  UefiRuntimeServicesTableLib
  PciSegmentLib
  GpioLib
  TimerLib
  PostCodeLib
  EcTcssLib
  ItssLib
  TopSwapLib
  PchPciBdfLib
  BaseOemSvcChipsetLibDefault
  CmosLib
  H2OCpLib
  H2OLib
  HobLib
  PcdLib
  ResetSystemLib
  SataLib
  S3BootScriptLib
  SmmOemSvcChipsetLibDefault
  SmmOemSvcKernelLibDefault
  VariableLib

[Packages]
  $(PROJECT_PKG)/Project.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeOemServicesPkg/InsydeOemServicesPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec # new add

[Pcd]
  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress                ## CONSUMES
  # Progress Code for S3 Suspend start.
  # PROGRESS_CODE_S3_SUSPEND_START = (EFI_SOFTWARE_SMM_DRIVER | (EFI_OEM_SPECIFIC | 0x00000000))    = 0x03078000
  gEfiMdeModulePkgTokenSpaceGuid.PcdProgressCodeS3SuspendStart
  gPlatformModuleTokenSpaceGuid.PcdAcpiEnableSwSmi                 ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdAcpiDisableSwSmi                ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdPcieDockBridgeResourcePatchSmi  ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdSmcExtSmiBitPosition            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor
  gBoardModuleTokenSpaceGuid.PcdEcPresent
  gBoardModuleTokenSpaceGuid.PcdUsbcEcPdNegotiation                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFingerPrintIrqGpio                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardPmcPdEnable                   ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdS3Enable                                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdGpioTier2WakeEnable                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdGpioTier2WakeEnable                ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable                   ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdTopSwapEnableSwSmi              ## PRODUCES
  gPlatformModuleTokenSpaceGuid.PcdTopSwapDisableSwSmi             ## PRODUCES
  gChipsetPkgTokenSpaceGuid.PcdRestoreCmosfromVariableFlag         ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdSaveCmosFieldCompareList            ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdSecureFlashWakeFromS3Time           ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdSmiNewCenturySupport                ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdSmmInt10Enable                      ## CONSUMES

[FixedPcd]
  gSiPkgTokenSpaceGuid.PcdEmbeddedEnable

[FeaturePcd]
  gChipsetPkgTokenSpaceGuid.PcdH2OSmmCpEcSendEspiClearSupported
  gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag
  gChipsetPkgTokenSpaceGuid.PcdNvidiaOptimusSupported
  gChipsetPkgTokenSpaceGuid.PcdHgNvidiaDdsFeatureSupport
  gChipsetPkgTokenSpaceGuid.PcdH2OS3SaveRestoreLevelEdgeSupported
  gChipsetPkgTokenSpaceGuid.PcdH2OS3SaveRestoreEDPRegSupported
  gChipsetPkgTokenSpaceGuid.PcdH2OS3SaveRestorePicSupported
  gChipsetPkgTokenSpaceGuid.PcdH2OS3DmaInitSupported

[Sources]
  SmmPlatform.h
  SmmPlatform.c
  EcSmi.h
  EcSmi.c
  PcieDockSmi.h
  PcieDockSmi.c
  HgS3S4Callback.c
  SaveResFuncs.c
  SaveCmosInVariable.c
  CapsuleWakeup.c
  DeviceRegTable.c

[Protocols]
  gPlatformNvsAreaProtocolGuid                  ## CONSUMES
  gEfiS3SmmSaveStateProtocolGuid                ## CONSUMES
  gEfiSmmBase2ProtocolGuid                      ## CONSUMES
  gEfiSmmSwDispatch2ProtocolGuid                ## CONSUMES
  gEfiSmmSxDispatch2ProtocolGuid                ## CONSUMES
  gEfiSmmPowerButtonDispatch2ProtocolGuid       ## CONSUMES
  gEfiSmmVariableProtocolGuid                   ## CONSUMES
  gPchEspiSmiDispatchProtocolGuid               ## CONSUMES
  gPchTcoSmiDispatchProtocolGuid                ## CONSUMES
  gEfiSmmCpuProtocolGuid                        ## CONSUMES
  gEfiSmmEndOfDxeProtocolGuid                   ## CONSUMES
  gAcpiDisableCallbackStartProtocolGuid         ## CONSUMES
  gAcpiEnableCallbackStartProtocolGuid          ## CONSUMES
  gAcpiRestoreCallbackDoneProtocolGuid          ## CONSUMES
  gAcpiRestoreCallbackStartProtocolGuid         ## CONSUMES
  gEfiHgNvsAreaProtocolGuid                     ## CONSUMES
  gEfiOSResetPolicyProtocolGuid                 ## CONSUMES
  gEfiOverrideAspmProtocolGuid                  ## CONSUMES
  gEfiProgramSsidSvidProtocolGuid               ## CONSUMES
  gEfiSmmInt15ServiceProtocolGuid               ## CONSUMES
  gSmmThunkProtocolGuid                         ## CONSUMES

[Guids]
  gSetupVariableGuid                            ## PRODUCES
  gCpuSetupVariableGuid                         ## CONSUMES
  gPchSetupVariableGuid                         ## CONSUMES
  gSkipBiosLockForSysFwUpdateGuid               ## CONSUMES
  gCmosInVariableGuid                           ## CONSUMES ## SOMETIMES_PRODUCES
  gEfiSmmInt15ServiceProtocol2Guid              ## PRODUCES
  gH2OHybridGraphicsVariableGuid                ## CONSUMES ## SOMETIMES_PRODUCES
  gH2OSmmCpEcSendEspiClearGuid                  ## CONSUMES
  gSaSetupVariableGuid                          ## CONSUMES ## SOMETIMES_PRODUCES
  gSecureFlashInfoGuid                          ## CONSUMES
  gSystemConfigurationGuid                      ## CONSUMES

[Depex]
  gEfiSmmBase2ProtocolGuid                 AND
  gEfiSmmPowerButtonDispatch2ProtocolGuid  AND
  gEfiSmmSxDispatch2ProtocolGuid           AND
  gEfiSmmSwDispatch2ProtocolGuid           AND
  gPlatformNvsAreaProtocolGuid             AND
  gPchEspiSmiDispatchProtocolGuid          AND
  gPchTcoSmiDispatchProtocolGuid
