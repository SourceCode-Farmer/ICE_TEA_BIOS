/** @file
  Chipset Setup Configuration Definitions

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CHIPSET_SETUP_CONFIG_H_
#define _CHIPSET_SETUP_CONFIG_H_
#ifndef VFRCOMPILE
#include <Uefi/UefiInternalFormRepresentation.h>
#endif
#include <KernelSetupConfig.h>
#include <IccSetupData.h>
// #include <PlatformBoardId.h> // gino: temp remove
#include <Setup.h>
#include <SetupId.h>
#include <SetupVariable.h>
#include <OemSetup.h>

#define DVMT_PREALLOCATE_LABEL                  0x1060
#define KEY_TXT                                 0x20B4
#define IDE_UPDATE_LABEL                        0x1005
#define AZALIA_LABEL                            0x1006
#define CORE_RATIO_LIMIT_LABEL                  0x1051
#define SYSTEM_HEALTH_LABEL                     0x1013
#define DUAL_VGA_SUPPORT_START_LABEL            0x1cfe
#define DUAL_VGA_SUPPORT_END_LABEL              0x1cff
#define PLUG_IN_DISPLAY_SELECTION_START_LABEL   0x1d00
#define PLUG_IN_DISPLAY_SELECTION_END_LABEL     0x1d01
#define IGD_DISPLAY_SELECTION_START_LABEL       0x1d02
#define IGD_DISPLAY_SELECTION_END_LABEL         0x1d03
#define APCI_DEBUG_ADDRESS_LABEL                0x1052
// #define PTT_INFO_LABEL                          0x1055
// #define PTT_ENABLE_LABEL                        0x660
// #define PTT_REVOKE_TRUST_LABEL                  0x1670
#define ME_UNCONFIG_ON_RTC_LABEL                0x671

#define KEY_SERIAL_PORTA                        0x2030
#define KEY_SERIAL_PORTA_BASE_IO                0x2031
#define KEY_SERIAL_PORTA_INTERRUPT              0x2032
#define KEY_SERIAL_PORTB                        0x2033
#define KEY_SERIAL_PORTB_BASE_IO                0x2034
#define KEY_SERIAL_PORTB_INTERRUPT              0x2035
#define KEY_PCI_SLOT3_IRQ_SET                   0x2040
#define KEY_PCI_SLOT4_IRQ_SET                   0x2041
#define KEY_PEG_FORCE_X1                        0x2050
#define KEY_PCIE_COMPLIANCE_MODE                0x2051
#define KEY_CHIPSET_EXTENDED_CONFIG             0x2060
#define KEY_CHIPSET_SDRAM_TIME_CTRL             0x2061
#define KEY_SETUP_REFRESH                       0x2062
#define KEY_PCI_IRQ_SET                         0x2063
#define KEY_AHCI_OPROM_CONFIG                   0x2065
#define KEY_SATA_CNFIGURE_MODE                  0x2090
#define KEY_DUAL_VGA_SUPPORT                    0x2066
#define KEY_PLUG_IN_DISPLAY_SELECTION1          0x21d0
#define KEY_PLUG_IN_DISPLAY_SELECTION2          0x21d1
#define KEY_IGD_PRIMARY_DISPLAY_SELECTION       0x21e0
#define KEY_IGD_SECONDARY_DISPLAY_SELECTION     0x21e1
#define ME_IMAGE_CONSUMER_SKU_FW      0x03
#define ME_IMAGE_CORPORATE_SKU_FW     0x04

#define KEY_DVMT_PREALLOCATE                    0xA00
#define KEY_APERTURE_SIZE                       0xA04
#define KEY_WAKE_ON_PME                         0x402

#define KEY_DPTF                                0x3610
#define KEY_PROCESSOR_THERMAL                   0x3620
#define KEY_XHCI_PREBOOT_SUPPORT                0x600
#define KEY_IFR_UPDATE                          0x610
#define KEY_DDR3LV_OPTION                       0x620
#define KEY_MAX_TOLUD_OPTION          0x630
#define IDE_MODE                                0
#define AHCI_MODE                               (SATA_MODE_AHCI + 1)
#define RAID_MODE                               (SATA_MODE_RAID + 1)
//#define LOOPBACK_TEST_MODE                      3
#define KEY_PCI_EXPRESS_CONFIGURATION_CHANGE    0x61
#define KEY_CPU_CONTROL                         0x652
#define KEY_ME_UNCONFIG_ON_RTC                  0x670
#define LABEL_CPU_RATIO                         0x683
#define SERIAL_ATA_PORT0_FORM_ID                0x230
#define SERIAL_ATA_PORT1_FORM_ID                0x231
#define SERIAL_ATA_PORT2_FORM_ID                0x232
#define SERIAL_ATA_PORT3_FORM_ID                0x233
#define SERIAL_ATA_PORT4_FORM_ID                0x234
#define SERIAL_ATA_PORT5_FORM_ID                0x235
#define SERIAL_ATA_PORT6_FORM_ID                0x236
#define SERIAL_ATA_PORT7_FORM_ID                0x237
#define KEY_OTHER_VFR_GOTO_BASE                 0x3000
#define SERIAL_IO_FORM_ID                       0x3700
#define SERIAL_IO_I2C0_FORM_ID                  0x3701
#define SERIAL_IO_I2C1_FORM_ID                  0x3702
#define SERIAL_IO_I2C2_FORM_ID                  0x3703
#define SERIAL_IO_I2C3_FORM_ID                  0x3704
#define SERIAL_IO_I2C4_FORM_ID                  0x3705
#define SERIAL_IO_I2C5_FORM_ID                  0x3706
#define SERIAL_IO_SPI0_FORM_ID                  0x3707
#define SERIAL_IO_SPI1_FORM_ID                  0x3708
#define SERIAL_IO_SPI2_FORM_ID                  0x3709
#define SERIAL_IO_UART0_FORM_ID                 0x370A
#define SERIAL_IO_UART1_FORM_ID                 0x370B
#define SERIAL_IO_UART2_FORM_ID                 0x370C
#define SERIAL_IO_GPIO_FORM_ID                  0x370D
#define SERIAL_IO_TIMING_FORM_ID                0x370E
#define SCS_FORM_ID                             0x3720
#define ISH_FORM_ID                             0x3730
#define HD_AUDIO_FORM_ID                        0x3710
#define HD_AUDIO_ADVANCED_CONFIG_FORM_ID        0x3711
#define HD_AUDIO_DSP_FEATURE_CONFIGFORM_ID      0x3712
#define SKYCAM_FORM_ID                          0x3750
#define SKYCAM_CONTROL_lOGIC_0_FORM_ID          0x3751
#define SKYCAM_CONTROL_lOGIC_1_FORM_ID          0x3752
#define SKYCAM_CONTROL_lOGIC_2_FORM_ID          0x3753
#define SKYCAM_CONTROL_lOGIC_3_FORM_ID          0x3754
#define SKYCAM_LINK_0_FORM_ID                   0x3755
#define SKYCAM_LINK_1_FORM_ID                   0x3756
#define SKYCAM_LINK_2_FORM_ID                   0x3757
#define SKYCAM_LINK_3_FORM_ID                   0x3758
#define MAX_PCI_EXPRESS_ROOT_PORTS              6

//
// Express Card Support
//
#define EXPRESS_CARD_ROOT_PORT_BUS              0x0
#define EXPRESS_CARD_ROOT_PORT_DEV              PCI_DEVICE_NUMBER_ICH_PCIEXP
#define EXPRESS_CARD_ROOT_PORT_FUN              PCI_FUNCTION_NUMBER_ICH_PCIEXP2

//
// Local Flat Panel Backlight Control Mode , refer to VBIOS Int15 sub:5F49h
//
#define BKLT_SEL_PWM_INVERTED                   0
#define BKLT_SEL_PWM_NORMAL                     2
#define BKLT_SEL_GMBUS_INVERTED                 1
#define BKLT_SEL_GMBUS_NORMAL                   3

#define B_C1_AUTO_DEMOTION                      BIT0
#define B_C3_AUTO_DEMOTION                      BIT1

#define B_C1_UNDEMOTION                         BIT0
#define B_C3_UNDEMOTION                         BIT1

#define B_TS_ON_DIMM_SLOT0                      BIT0
#define B_TS_ON_DIMM_SLOT1                      BIT1

#define DUAL_VGA_CONTROLLER_ENABLE              1
#define DUAL_VGA_CONTROLLER_DISABLE             0
//
// DisplayMode
//
#define DISPLAY_MODE_IGPU               0x0
#define DISPLAY_MODE_DGPU               0x1
#define DISPLAY_MODE_PCI                0x2
#define DISPLAY_MODE_AUTO               0x3
#define DISPLAY_MODE_HYBRID             0x4
#define DUAL_VGA_CONTROLLER_ENABLE      1
#define DUAL_VGA_CONTROLLER_DISABLE     0
#define SCU_IGD_BOOT_TYPE_DISABLE       0x00
#define SCU_IGD_BOOT_TYPE_VBIOS_DEFAULT 0x00
#define SCU_IGD_BOOT_TYPE_CRT           0x01
#define SCU_IGD_BOOT_TYPE_LFP           0x08
#define SCU_IGD_BOOT_TYPE_EFP           0x04
#define SCU_IGD_BOOT_TYPE_EFP3          0x20
#define SCU_IGD_BOOT_TYPE_EFP2          0x40
#define SCU_IGD_BOOT_TYPE_LFP2          0x80


#define IGD_ENABLE                              1
#define IGD_DISABLE                             0
#define IGD_AUTO                                2
#define ALWAYS_ENABLE_PEG                       1
#define DONT_ALWAYS_ENABLE_PEG                  1
#define SG_ENABLE                               1
#define SG_DISABLE                              0
#define MUXED_FIXED                             1
#define MUXLESS_FIXED                           5
#define MUXLESS_DYNAMIC                         6
#define MUXLESS_FIXED_DYNAMIC                   7
#define SCU_IGD_BOOT_TYPE_DISABLE               0x00
#define SCU_IGD_BOOT_TYPE_VBIOS_DEFAULT         0x00
#define SCU_IGD_BOOT_TYPE_CRT                   0x01
#define SCU_IGD_BOOT_TYPE_LFP                   0x08
#define SCU_IGD_BOOT_TYPE_EFP                   0x04
#define SCU_IGD_BOOT_TYPE_EFP3                  0x20
#define SCU_IGD_BOOT_TYPE_EFP2                  0x40
#define SCU_IGD_BOOT_TYPE_LFP2                  0x80
//
// The order is for SCU
//
#define SCU_IGD_INDEX_DISABLE           0x00
#define SCU_IGD_INDEX_VBIOS_DEFAULT     0x00
#define SCU_IGD_INDEX_CRT               0x01
#define SCU_IGD_INDEX_LFP               0x02
#define SCU_IGD_INDEX_EFP               0x03
#define SCU_IGD_INDEX_EFP2              0x04
#define SCU_IGD_INDEX_EFP3              0x05
#define SCU_IGD_INDEX_LFP2              0x06
#define FAST_RECLAIM_COUNT                      35
#define ADVANCE_VARSTORE_ID                     0x1233
#define CONFIGURATION_VARSTORE_ID               0x1234

#define MAX_HII_HANDLES                         0x10
#define INVALID_HII_HANDLE                      0

#define TPM2_DISABLE                    0
#define TPM2_ENABLE                     1
#define PTT_DISABLE                     0
#define PTT_ENABLE                      1
#define LPC_PCH_TYPE_LP                 1
#define LPC_PCH_TYPE_H                  0
#define XMP_MEM_AUTO                    0


#define SGX_FACTORY_PRESET_EPOCH            0
#define SGX_RANDOM_GENERATED_EPOCH          1
#define SGX_USER_MANUAL_EPOCH               2
//
// SkylakeSiPkg\SystemAgent\Include\Ppi\GraphicsConfig.h
// GT Aperture Size
//
#define GTAPERTURESIZE128MB   0
#define GTAPERTURESIZE256MB   1
#define GTAPERTURESIZE512MB   3
#define GTAPERTURESIZE1024MB  7
#define GTAPERTURESIZE2048MB  15
#define GTAPERTURESIZE4096MB  31


#define OFFSET_0  0x0
#define OFFSET_1  0x1
#define OFFSET_2  0x2
#define OFFSET_3  0x3
#define OFFSET_4  0x4
#define OFFSET_5  0x5
#define OFFSET_6  0x6
#define OFFSET_7  0x7
#define OFFSET_8  0x8
#define OFFSET_9  0x9
#define OFFSET_10  0xa
#define OFFSET_11  0xb
#define OFFSET_12  0xc
#define OFFSET_13  0xd
#define OFFSET_14  0xe
#define OFFSET_15  0xf
#define OFFSET_16  0x10
#define OFFSET_17  0x11
#define OFFSET_18  0x12
#define OFFSET_19  0x13
#define OFFSET_20  0x14
#define OFFSET_21  0x15
#define OFFSET_22  0x16
#define OFFSET_23  0x17
#define OFFSET_24  0x18
#define OFFSET_25  0x19
#define OFFSET_26  0x1a
#define OFFSET_27  0x1b
#define OFFSET_28  0x1c
#define OFFSET_29  0x1d
#define OFFSET_30  0x1e
#define OFFSET_31  0x1f
#define OFFSET_32  0x20
#define OFFSET_33  0x21
#define OFFSET_34  0x22
#define OFFSET_35  0x23
#define OFFSET_36  0x24
#define OFFSET_37  0x25
#define OFFSET_38  0x26
#define OFFSET_39  0x27
#define MAIN_FORM_SET_CLASS                     0x1
#define ADVANCED_FORM_SET_CLASS                 0x2

#define ICC_CLOCK_SETTING_TYPE_CURRENT          0
#define ICC_CLOCK_SETTING_TYPE_BOOT             1
#define ICC_CLOCK_SETTING_TYPE_PRE_BOOT         2
#define ICC_CLOCK_SETTING_TYPE_OEM              3

extern EFI_GUID  gChipsetSetupDafultGuid;

extern SETUP_DATA                mSetupData;
extern SA_SETUP                  mSaSetup;
extern ME_SETUP                  mMeSetup;
extern CPU_SETUP                 mCpuSetup;
extern PCH_SETUP                 mPchSetup;
extern SETUP_VOLATILE_DATA       mSetupVolatileData;

#pragma pack(1)
typedef struct {
  UINT8         IdeDevice0;
  UINT8         IdeDevice1;
  UINT8         IdeDevice2;
  UINT8         IdeDevice3;
  UINT8         IdeDevice4;
  UINT8         IdeDevice5;
  UINT8         IdeDevice6;
  UINT8         IdeDevice7;
} ADVANCE_CONFIGURATION;
#pragma pack()

#pragma pack(1)
//
//  Setup Utility Structure
//
//-----------------------------------------------------------------------------------------------------------------
// Important!!! The following setup utility structure should be syncronize with OperationRegion MBOX in mailbox.asi.
// If you do NOT follow it, you may face on unexpected issue. The total size are 1200 bytes.
// (Kernel 230 bytes + OEM 70 bytes + ODM 100 bytes + Chipset 800 bytes)
//-----------------------------------------------------------------------------------------------------------------

typedef struct {
  //
  // Kernel system configuration (offset 0~229, total 230 bytes)
  //
  #define _IMPORT_KERNEL_SETUP_
  #include <KernelSetupData.h>
  #undef _IMPORT_KERNEL_SETUP_

  // OEM_Start
  // Offset 230~299, total 70 bytes;     // This area must sync to SetupConfig.h
  UINT8         OEMRSV[70];              // Dummy area. Reserve for OEM team, really structure will fill in SetupConfig.h
  // OEM_End

  // ODM_Start
  // Offset 300~399, total 100 bytes;    // This are must sync to SetupConfig.h
  UINT8         ODMRSV[100];             // Dummy area. Reserve for ODM, really structure will fill in SetupConfig.h
  // ODM_End

  //
  // Chipset system configuration (offset 400~1199, total 800 bytes)
  //
  #define _IMPORT_CHIPSET_SPECIFIC_SETUP_
  #include <ChipsetSpecificSetupData.h>
  #undef _IMPORT_CHIPSET_SPECIFIC_SETUP_

} CHIPSET_CONFIGURATION;
#pragma pack()

#endif
