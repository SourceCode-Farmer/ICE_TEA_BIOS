#******************************************************************************
#* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
#  FmpDeviceLib instance to support uCode update
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2019 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = FmpDeviceLibuCode
  FILE_GUID                      = 09EE5BD9-E78F-4534-AED6-A1852BD706A8
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = FmpDeviceLib|DXE_DRIVER
  CONSTRUCTOR                    = FmpDeviceLibuCodeConstructor

#
# The following information is for reference only and not required by the build tools.
#
#  VALID_ARCHITECTURES           = IA32 X64
#

[Sources]
  FmpDeviceLibuCode.c
  MicrocodeUpdate.c
  MicrocodeUpdate.h

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  FmpDevicePkg/FmpDevicePkg.dec
  IntelSiliconPkg/IntelSiliconPkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
#[-start-201130-IB16810140-modify]#
#  ClientOneSiliconPkg/SiPkg.dec
#  AlderLakePlatSamplePkg/PlatformPkg.dec
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec
  $(PLATFORM_SAMPLE_CODE_DEC_NAME)/PlatformPkg.dec
  IntelFsp2WrapperPkg/IntelFsp2WrapperPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  MinPlatformPkg/MinPlatformPkg.dec
  FmpDevicePkg/FmpDevicePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#[-end-201130-IB16810140-modify]#

[LibraryClasses]
  DebugLib
  BaseLib
  BaseMemoryLib
  MemoryAllocationLib
  UefiRuntimeServicesTableLib
  PcdLib
  PlatformFlashAccessLib
  SeamlessRecoverySupportLib
#[-start-201130-IB16810140-remove]#
#  DisplayUpdateProgressLib
#[-end-201130-IB16810140-remove]#
  SpiAccessLib
  HobLib
#[-start-201130-IB16810140-remove]#
#  CapsuleUpdateResetLib
#[-end-201130-IB16810140-remove]#
  IoLib
  BiosUpdateHelpersLib

[Pcd]
  gPlatformModuleTokenSpaceGuid.PcdSystemFirmwareFmpLowestSupportedVersion   ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdSystemFirmwareFmpSupportedMode            ## CONSUMES ## SOMETIMES_PRODUCES
#[-start-201130-IB16810140-remove]#
#  gSiPkgTokenSpaceGuid.PcdFlashMicrocodeFvOffset                             ## CONSUMES
#  gSiPkgTokenSpaceGuid.PcdFlashMicrocodeFvSize                               ## CONSUMES
#  gSiPkgTokenSpaceGuid.PcdFlashMicrocodeFvBase                               ## CONSUMES
#[-end-201130-IB16810140-remove]#
  gSiPkgTokenSpaceGuid.PcdBiosAreaBaseAddress                                ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcduCodeSelectionPolicy                      ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable                          ## CONSUMES
#[-start-201130-IB16810140-remove]#
#  gFmpDevicePkgTokenSpaceGuid.PcdFmpDeviceProgressColor                      ## CONSUMES
#[-end-201130-IB16810140-remove]#
  gPlatformModuleTokenSpaceGuid.PcdTopSwapEnableSwSmi                        ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdTopSwapDisableSwSmi                       ## CONSUMES
#[-start-201130-IB16810140-add]#
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMicrocodeBase
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMicrocodeSize
  gFmpDevicePkgTokenSpaceGuid.PcdFmpDeviceBuildTimeLowestSupportedVersion    ## CONSUME
  gInsydeTokenSpaceGuid.PcdFirmwareResourceMaximum
  gInsydeTokenSpaceGuid.PcdOsIndicationsSupported
  gInsydeTokenSpaceGuid.PcdCapsuleMaxResult 
  gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceResiliencyEnabled
  gChipsetPkgTokenSpaceGuid.PcdWindowsUcodeFirmwareCapsuleGuid
#[-end-201130-IB16810140-add]#

[Protocols]
  gEfiFirmwareManagementProtocolGuid            ## PRODUCES
  gEfiMpServiceProtocolGuid                     ## CONSUMES
  gEfiFirmwareVolumeBlockProtocolGuid           ## CONSUMES
  gEfiFirmwareVolume2ProtocolGuid               ## CONSUMES

[Guids]
  gFmpDevicePlatformuCodeGuid
  gIntelMicrocodeVersionFfsFileGuid
  gIntelMicrocodeArrayFfsFileGuid
#[-start-201130-IB16810140-add]#
  gSysFwUpdateProgressGuid
  gFmpCapsuleInfoGuid
  gSysFwMicrocodeDeferredSVNGuid
  gH2OSeamlessRecoveryGuid
  gEfiSystemResourceTableGuid
#[-end-201130-IB16810140-add]#
#[-start-210506-IB16810153-add]#
  gEfiGlobalVariableGuid
#[-end-210506-IB16810153-add]#

[Depex]
  gEfiVariableArchProtocolGuid AND
  gEfiVariableWriteArchProtocolGuid AND
  gEfiMpServiceProtocolGuid
