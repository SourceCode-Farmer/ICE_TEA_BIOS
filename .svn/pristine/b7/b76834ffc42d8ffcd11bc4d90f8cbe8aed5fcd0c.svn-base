/** @file
  Silicon pre-mem policy PEIM.

Copyright (c) 2017, Intel Corporation. All rights reserved.<BR>
SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#include <PiPei.h>
#include <Library/DebugLib.h>
#include <Library/SiliconPolicyInitLib.h>
#include <Library/SiliconPolicyUpdateLib.h>
//[-start-201027-IB17800106-add]//
#include <Library/PeiOemSvcChipsetLib.h>
//[-end-201027-IB17800106-add]//


/**
  Silicon Policy Init before memory PEI module entry point

  @param[in]  FileHandle           Not used.
  @param[in]  PeiServices          General purpose services available to every PEIM.

  @retval     EFI_SUCCESS          The function completes successfully
**/
EFI_STATUS
EFIAPI
SiliconPolicyPeiPreMemEntryPoint (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  VOID  *Policy;
//[-start-201027-IB17800106-add]//
  EFI_STATUS                   Status;
//[-end-201027-IB17800106-add]//

  Policy = SiliconPolicyInitPreMem (NULL);
  SiliconPolicyUpdatePreMem (Policy);

//[-start-201027-IB17800106-add]//
  //
  // OemServices
  //
  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcUpdatePeiPolicyInitPreMem \n"));
  Status = OemSvcUpdatePeiPolicyInitPreMem (Policy);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcUpdatePeiPolicyInitPreMem Status: %r\n", Status));
  ASSERT ((Status == EFI_SUCCESS) || (Status == EFI_UNSUPPORTED) || (Status == EFI_MEDIA_CHANGED));
//[-end-201027-IB17800106-add]//

  SiliconPolicyDonePreMem (Policy);

  return EFI_SUCCESS;
}