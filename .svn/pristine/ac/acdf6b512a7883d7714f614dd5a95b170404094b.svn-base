/** @file
  The Vfr component for Exit menu

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

//_Start_L05_SETUP_MENU_
//#include "KernelSetupConfig.h"
#include "SetupConfig.h"
//_End_L05_SETUP_MENU_

formset
  guid     = FORMSET_ID_GUID_EXIT,
  title    = STRING_TOKEN(STR_EXIT_TITLE),
  help     = STRING_TOKEN(STR_BLANK_STRING),
  classguid = SETUP_UTILITY_FORMSET_CLASS_GUID,
  class    = SETUP_UTILITY_CLASS,
  subclass = SETUP_UTILITY_SUBCLASS,
#if defined(SETUP_IMAGE_SUPPORT) && FeaturePcdGet(PcdH2OFormBrowserLocalMetroDESupported)
  image     = IMAGE_TOKEN(IMAGE_EXIT);
#endif

//_Start_L05_SETUP_MENU_
//  varstore KERNEL_CONFIGURATION,            // This is the data structure type
  varstore SYSTEM_CONFIGURATION,            // This is the data structure type
//_End_L05_SETUP_MENU_
    varid = CONFIGURATION_VARSTORE_ID,      // Optional VarStore ID
    name  = SystemConfig,                    // Define referenced name in vfr
    guid  = SYSTEM_CONFIGURATION_GUID;      // GUID of this buffer storage

  form
    formid = ROOT_FORM_ID,

    title = STRING_TOKEN(STR_EXIT_TITLE);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);

//_Start_L05_SETUP_MENU_
    grayoutif
      FALSE;
//      ideqval SystemConfig.UserAccessLevel == 2
//      AND
//      ideqval SystemConfig.SetUserPass == 1;
      text
        help   = STRING_TOKEN(STR_EXIT_SAVING_CHANGES_HELP),
        text   = STRING_TOKEN(STR_EXIT_SAVING_CHANGES_STRING),
        text   = STRING_TOKEN(STR_BLANK_STRING),
        flags  = INTERACTIVE,
        key    = KEY_SAVE_EXIT;
    endif;
//_End_L05_SETUP_MENU_

//_Start_L05_SETUP_MENU_
#if 0

    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      text
        help   = STRING_TOKEN(STR_SAVE_CHANGE_WITHOUT_EXIT_HELP),
        text   = STRING_TOKEN(STR_SAVE_CHANGE_WITHOUT_EXIT_STRING),
        text   = STRING_TOKEN(STR_BLANK_STRING),
        flags  = INTERACTIVE,
        key    = KEY_SAVE_WITHOUT_EXIT;
    endif;
#endif
//_End_L05_SETUP_MENU_

    text
      help   = STRING_TOKEN(STR_EXIT_DISCARDING_CHANGES_HELP),
      text   = STRING_TOKEN(STR_EXIT_DISCARDING_CHANGES_STRING),
      text   = STRING_TOKEN(STR_BLANK_STRING),
      flags  = INTERACTIVE,
      key    = KEY_EXIT_DISCARD;

//_Start_L05_SETUP_MENU_
#if 0
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;

      text
        help   = STRING_TOKEN(STR_LOAD_OPTIMAL_DEFAULTS_HELP),
        text   = STRING_TOKEN(STR_LOAD_OPTIMAL_DEFAULTS_STRING),
        text   = STRING_TOKEN(STR_BLANK_STRING),
        flags  = INTERACTIVE,
        key    = KEY_LOAD_OPTIMAL;

      text
        help   = STRING_TOKEN(STR_LOAD_CUSTOM_DEFAULTS_HELP),
        text   = STRING_TOKEN(STR_LOAD_CUSTOM_DEFAULTS_STRING),
        text   = STRING_TOKEN(STR_BLANK_STRING),
        flags  = INTERACTIVE,
        key    = KEY_LOAD_CUSTOM;
    endif;

    grayoutif
      ideqval SystemConfig.UserAccessLevel == 2
      AND
      ideqval SystemConfig.SetUserPass == 1;

      text
        help   = STRING_TOKEN(STR_SAVE_CUSTOM_DEFAULTS_HELP),
        text   = STRING_TOKEN(STR_SAVE_CUSTOM_DEFAULTS_STRING),
        text   = STRING_TOKEN(STR_BLANK_STRING),
        flags  = INTERACTIVE,
        key    = KEY_SAVE_CUSTOM;
    endif;

#endif
//_End_L05_SETUP_MENU_

//_Start_L05_SETUP_MENU_
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
//_End_L05_SETUP_MENU_
    text
      help   = STRING_TOKEN(STR_DISCARD_CHANGES_HELP),
      text   = STRING_TOKEN(STR_DISCARD_CHANGES_STRING),
      text   = STRING_TOKEN(STR_BLANK_STRING),
      flags  = INTERACTIVE,
      key    = KEY_DISCARD_CHANGE;

//_Start_L05_SETUP_MENU_
    text
      help   = STRING_TOKEN(L05_STR_SAVE_CHANGE_HELP),
      text   = STRING_TOKEN(L05_STR_SAVE_CHANGE_STRING),
      text   = STRING_TOKEN(STR_BLANK_STRING),
      flags  = INTERACTIVE,
      key    = KEY_SAVE_WITHOUT_EXIT;

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);

    text
      help   = STRING_TOKEN(L05_STR_LOAD_OPTIMAL_DEFAULTS_HELP),
      text   = STRING_TOKEN(L05_STR_LOAD_OPTIMAL_DEFAULTS_STRING),
      text   = STRING_TOKEN(STR_BLANK_STRING),
      flags  = INTERACTIVE,
      key    = KEY_LOAD_OPTIMAL;

#ifndef L05_LEGACY_SUPPORT
    suppressif
      TRUE;
#endif
    grayoutif
      ideqval SystemConfig.L05DeviceGuard == 1;  // 0:Disabled, 1:Enabled
      oneof
        varid       = SystemConfig.L05OsOptimizedDefault,
        prompt      = STRING_TOKEN(L05_STR_OS_OPTIMIZED_DEFAULTS_STRING),
        help        = STRING_TOKEN(L05_STR_OS_OPTIMIZED_DEFAULTS_HELP),
        option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 0, flags = DEFAULT;
        option text = STRING_TOKEN(STR_DISABLED_TEXT), value = 1, flags = 0;
      endoneof;
    endif;

    suppressif
      ideqvallist SystemConfig.L05DeviceGuard == 0;  // 0:Disabled, 1:Enabled
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_DEVICE_GUARD_UNSELECTABLE_STRING),
        text   = STRING_TOKEN(STR_BLANK_STRING);
    endif;
#ifndef L05_LEGACY_SUPPORT
    endif;
#endif

    endif;
//_End_L05_SETUP_MENU_
    label TRIGGER_BROWSER_REFRESH_LABEL;

    link;

  endform;

endformset;

