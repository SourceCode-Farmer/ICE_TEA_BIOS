## @file
# Module Information for Dxe Timer Library which uses the Time Stamp Counter in the processor.
#
# Note: There will be 1ms penalty to get TSC frequency every time
# by waiting for 3579 clocks of the ACPI timer, or 1ms.
# 
# A version of the Timer Library using the processor's TSC.
# The time stamp counter in newer processors may support an enhancement, referred to as invariant TSC.
# The invariant TSC runs at a constant rate in all ACPI P-, C-. and T-states.
# This is the architectural behavior moving forward.
# TSC reads are much more efficient and do not incur the overhead associated with a ring transition or
# access to a platform resource.
# 
# @copyright
# Copyright (c) 2011 - 2015 Intel Corporation. All rights reserved
# This software and associated documentation (if any) is furnished
# under a license and may only be used or copied in accordance
# with the terms of the license. Except as permitted by the
# license, no part of this software or documentation may be
# reproduced, stored in a retrieval system, or transmitted in any
# form or by any means without the express written consent of
# Intel Corporation.
# This file contains 'Framework Code' and is licensed as such
# under the terms of your license agreement with Intel or your
# vendor. This file may not be modified, except as allowed by
# additional terms of your license agreement.
# 
# @par Specification Reference:
#
##


[Defines]
INF_VERSION = 0x00010017
BASE_NAME = DxeTscAcpiTimerLib
FILE_GUID = CD510996-7160-4845-9074-7BF2F794B41D
VERSION_STRING = 1.0
MODULE_TYPE = DXE_DRIVER
LIBRARY_CLASS = TimerLib|DXE_CORE DXE_DRIVER DXE_RUNTIME_DRIVER DXE_SMM_DRIVER UEFI_APPLICATION UEFI_DRIVER SMM_CORE
CONSTRUCTOR = DxeTscTimerLibConstructor
# 
# The following information is for reference only and not required by the build tools.
# 
# VALID_ARCHITECTURES = IA32 X64
# 



[LibraryClasses]
UefiBootServicesTableLib
PcdLib
IoLib
BaseLib
UefiLib
DebugLib
#BasePlatformTimerLib


[Packages]
MdePkg/MdePkg.dec
#[-start-200721-IB17040134-add]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#[-end-200721-IB17040134-add]#
PerformancePkg/PerformancePkg.dec


[Pcd]
#[-start-200721-IB17040134-modify]#
##
## This PCD is defined in gClientCommonModuleTokenSpaceGuid.
## RC removed this pkg and no longer use these PCDs.
## But our "ChipsetPkg\Override\EDK2\PcAtChipsetPkg\Library\TscAcpiTimerLib" still use these.
## Redefine to chipset PCD.
##
## This PCD specifies the ACPI IO base address.
#gClientCommonModuleTokenSpaceGuid.PcdAcpiIoBaseAddress
gChipsetPkgTokenSpaceGuid.PcdAcpiIoBaseAddress
#gClientCommonModuleTokenSpaceGuid.AcpiTimerLength
gChipsetPkgTokenSpaceGuid.AcpiTimerLength
#[-end-200721-IB17040134-modify]#


[Sources]
TscTimerLibShare.c
DxeTscTimerLib.c
TscTimerLibInternal.h


[Guids]
gEfiTscFrequencyGuid ## CONSUMES # System Configuration Table
