## @file
# Instance of Layout Package Library using DXE protocols and services.
#
#******************************************************************************
#* Copyright (c) 2013 - 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = HiiLayoutPkgDxe
  FILE_GUID                      = 6AD37609-A9F9-4587-9023-7820E10F5B7A
  MODULE_TYPE                    = UEFI_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = HiiLayoutPkgDxeEntryPoint

#
#  VALID_ARCHITECTURES           = IA32 X64 EBC
#

[Sources]
  HiiLayoutPkg.c
  HiiLayoutPkgStr.uni
  HotKeyStr.uni
  Image/Image.idf

  Project.vfcf
  Tool.hfcf
  BootMaint.hfcf
  BootManager.hfcf
  DeviceManager.hfcf
  FrontPage.hfcf
  SecureBootMgr.hfcf
  SetupUtility.hfcf
  VfrApp.hfcf
  MEBX.hfcf

[Packages]
#_Start_L05_FEATURE_
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#_End_L05_FEATURE_
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeSetupPkg/InsydeSetupPkg.dec

[LibraryClasses]
  UefiDriverEntryPoint
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  BaseLib
  MemoryAllocationLib
  DevicePathLib
  BaseMemoryLib
  UefiLib
  DebugLib
  HiiDbLib
  HiiStringLib
  HiiConfigAccessLib
  UefiHiiServicesLib

[Guids]
  gH2OHiiLayoutFileGuid

[Protocols]
  gEfiFirmwareVolume2ProtocolGuid
  gEfiHiiDatabaseProtocolGuid
  gLayoutDatabaseProtocolGuid

[FeaturePcd]
  gInsydeTokenSpaceGuid.PcdH2OBootMgrChangeBootOrderSupported
#_Start_L05_GRAPHIC_UI_
  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported
  gL05ServicesTokenSpaceGuid.PcdL05GamingUiSupported
#_End_L05_GRAPHIC_UI_

#_Start_L05_SETUP_MENU_
[FixedPcd]
  gL05ServicesTokenSpaceGuid.PcdL05ChipsetName
#_End_L05_SETUP_MENU_
