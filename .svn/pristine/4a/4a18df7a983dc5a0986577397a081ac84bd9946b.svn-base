/** @file
  Provides WMI Setup under OS set setup item

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <L05WmiSetupItemLibInternal.h>

//
// Select Value List
// [Convert VFR value to L05 setup value]
//

//
// VFR "Disable" value = 0, mDisableEnableValueMap[0]
// VFR "Enable"  value = 1, mDisableEnableValueMap[1]
//
UINT8                                   mDisableEnableValueMap[] = {
  0x01, 0x00
};

//
// VFR "Enable"  value = 0, mDisableEnableValueMap[0]
// VFR "Disable" value = 1, mDisableEnableValueMap[1]
//
UINT8                                   mEnableDisableValueMap[] = {
  0x00, 0x01
};

//
// VFR "Disable" value = 1, mDisableEnableValueMap[1]
// VFR "Enable"  value = 2, mDisableEnableValueMap[2]
//
UINT8                                   mNullDisableEnableValueMap[] = {
  0xFF, 0x01, 0x00
};

//
// VFR "Enable"  value = 1, mDisableEnableValueMap[1]
// VFR "Disable" value = 2, mDisableEnableValueMap[2]
//
UINT8                                   mNullEnableDisableValueMap[] = {
  0xFF, 0x00, 0x01
};

#ifdef L05_SHOW_RAID_OPTION_ENABLE
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
//
// VFR "AHCI" value = 0, mDisableEnableValueMap[0]
// VFR "RAID" value = 1, mDisableEnableValueMap[1]
//
UINT8                                   mSATAControllerValueMap[] = {
  0x00, 0x01
};
#else
//
// VFR "RAID" value = 1, mDisableEnableValueMap[1]
// VFR "AHCI" value = 2, mDisableEnableValueMap[2]
//
UINT8                                   mSATAControllerValueMap[] = {
  0xFF, 0x01, 0x00
};
#endif

#else
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
//
// VFR "AHCI" value = 0, mDisableEnableValueMap[0]
//
UINT8                                   mSATAControllerValueMap[] = {
  0x00
};
#else
//
// VFR "AHCI" value = 2, mDisableEnableValueMap[2]
//
UINT8                                   mSATAControllerValueMap[] = {
  0xFF, 0xFF, 0x00
};
#endif
#endif

//
// VFR "UMA Graphics"        value = 0, mDisableEnableValueMap[0]
// VFR "Switchable Graphics" value = 4, mDisableEnableValueMap[4]
//
UINT8                                   mGraphicsDeviceValueMap[] = {
  0x01, 0xFF, 0xFF, 0xFF, 0x00
};

//
// VFR "Disable"             value = 0, mDisableEnableValueMap[0]
// VFR "Enable"              value = 1, mDisableEnableValueMap[1]
// VFR "Software Controlled" value = 2, mDisableEnableValueMap[2]
//
UINT8                                   mIntelSgxValueMap[] = {
  0x02, 0x01, 0x00
};

//
// VFR "User Mode"  value = 0, mDisableEnableValueMap[0]
// VFR "Setup Mode" value = 1, mDisableEnableValueMap[1]
//
UINT8                                   mPlatformModeValueMap[] = {
  0x00, 0x01
};

//
// VFR "Standard"  value = 0, mDisableEnableValueMap[0]
// VFR "Custom"    value = 1, mDisableEnableValueMap[1]
//
UINT8                                   mSecureBootModeValueMap[] = {
  0x00, 0x01
};

//
// VFR "Legacy Support" value = 0, mDisableEnableValueMap[0]
// VFR "UEFI"           value = 2, mDisableEnableValueMap[2]
//
UINT8                                   mBootModeValueMap[] = {
  0x01, 0xFF, 0x00
};

//
// VFR "UEFI First"   value = 0, mDisableEnableValueMap[0]
// VFR "Legacy First" value = 1, mDisableEnableValueMap[1]
//
UINT8                                   mBootPriorityValueMap[] = {
  0x00, 0x01
};

//
// WMI Setup Item Sync Map
//
L05_WMI_SETUP_ITEM_SYNC_MAP             mL05WmiSetupItemSyncMap[] = {
// L05WmiSetupItemValid,                                 L05WmiSetupItem,                                 SetupNvDataMap,                              SaveSetupNvData,         SetupDataType
  //
  // Configuration Page
  //
#ifdef L05_NOTEBOOK_CLOUD_BOOT_ENABLE
  {&mL05WmiSetupItem.L05LenovoCloudServicesValid,        &mL05WmiSetupItem.L05LenovoCloudServices,        &mSetupNvData.L05LenovoCloudServices,        &mSaveSetupNvData,       DisableEnableValueType    },
#endif
#ifdef L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE
  {&mL05WmiSetupItem.L05UefiWifiNetworkBootValid,        &mL05WmiSetupItem.L05UefiWifiNetworkBoot,        &mSetupNvData.L05UefiWifiNetworkBoot,        &mSaveSetupNvData,       DisableEnableValueType    },
#endif
  {&mL05WmiSetupItem.L05UsbLegacyValid,                  &mL05WmiSetupItem.L05UsbLegacy,                  &mSetupNvData.LegacyUsbSupport,              &mSaveSetupNvData,       NullEnableDisableValueType},
  {&mL05WmiSetupItem.L05WirelessLanValid,                &mL05WmiSetupItem.L05WirelessLan,                &mSetupNvData.L05WirelessLan,                &mSaveSetupNvData,       DisableEnableValueType    },
#ifndef L05_BIOS_OPTANE_SUPPORT_ENABLE
  {&mL05WmiSetupItem.L05SataControllerModeValid,         &mL05WmiSetupItem.L05SataControllerMode,         &mPchSetupNvData.SataInterfaceMode,          &mSavePchSetupNvData,    SATAControllerValueType   },
#endif
  {&mL05WmiSetupItem.L05VmdControllerValid,              &mL05WmiSetupItem.L05VmdController,              &mSaSetupNvData.VmdEnable,                   &mSaveSaSetupNvData,     DisableEnableValueType    },
  {&mL05WmiSetupItem.L05GraphicsDeviceValid,             &mL05WmiSetupItem.L05GraphicsDevice,             &mSaSetupNvData.PrimaryDisplay,              &mSaveSaSetupNvData,     GraphicsDeviceValueType   },
  {&mL05WmiSetupItem.L05IntelVtValid,                    &mL05WmiSetupItem.L05IntelVt,                    &mCpuSetupNvData.VT,                         &mSaveCpuSetupNvData,    DisableEnableValueType    },
  {&mL05WmiSetupItem.L05IntelVtdValid,                   &mL05WmiSetupItem.L05IntelVtd,                   &mSaSetupNvData.EnableVtd,                   &mSaveSaSetupNvData,     DisableEnableValueType    },
  {&mL05WmiSetupItem.L05AmdVTechnologyValid,             &mL05WmiSetupItem.L05AmdVTechnology,             NULL,                                        NULL,                    SetupValueTypeMax         },
  {&mL05WmiSetupItem.L05IntelHyperThreadingValid,        &mL05WmiSetupItem.L05IntelHyperThreading,        &mCpuSetupNvData.HyperThreading,             &mSaveCpuSetupNvData,    DisableEnableValueType    },
  {&mL05WmiSetupItem.L05BiosBackFlashValid,              &mL05WmiSetupItem.L05BiosBackFlash,              &mSetupNvData.L05BiosBackFlash,              &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05HotKeyModeValid,                 &mL05WmiSetupItem.L05HotKeyMode,                 &mSetupNvData.L05HotKeyMode,                 &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05FoolProofFnCtrlValid,            &mL05WmiSetupItem.L05FoolProofFnCtrl,            &mSetupNvData.L05FoolProofFnCtrl,            &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05AlwaysOnUsbValid,                &mL05WmiSetupItem.L05AlwaysOnUsb,                &mSetupNvData.L05AlwaysOnUsb,                &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05ChargeInBatteryModeValid,        &mL05WmiSetupItem.L05ChargeInBatteryMode,        &mSetupNvData.L05ChargeInBatteryMode,        &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05RestoreDefaultOverclockingValid, &mL05WmiSetupItem.L05RestoreDefaultOverclocking, &mSetupNvData.L05RestoreDefaultOverclocking, &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05WakeOnVoiceValid,                &mL05WmiSetupItem.L05WakeOnVoice,                &mSetupNvData.L05WakeOnVoice,                &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05UltraQuietModeValid,             &mL05WmiSetupItem.L05UltraQuietMode,             &mSetupNvData.L05UltraQuietMode,             &mSaveSetupNvData,       DisableEnableValueType    },

  //
  // Security Page
  //
  {&mL05WmiSetupItem.L05PowerOnPasswordValid,            &mL05WmiSetupItem.L05PowerOnPassword,            &mSetupNvData.PowerOnPassword,               &mSaveSetupNvData,       NullDisableEnableValueType},
  {&mL05WmiSetupItem.L05ClearUserPasswordValid,          &mL05WmiSetupItem.L05ClearUserPassword,          NULL,                                        NULL,                    ClearUserPasswordType     },
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  {&mL05WmiSetupItem.L05IntelPttValid,                   &mL05WmiSetupItem.L05IntelPtt,                   &mSetupNvData.L05FtpmEnable,                 &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05ClearIntelPttKeyValid,           &mL05WmiSetupItem.L05ClearIntelPttKey,           NULL,                                        NULL,                    ClearTpmKeyType           },
#else
  {&mL05WmiSetupItem.L05AmdPspValid,                     &mL05WmiSetupItem.L05AmdPsp,                     &mSetupNvData.L05FtpmEnable,                 &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05ClearAmdPspKeyValid,             &mL05WmiSetupItem.L05ClearAmdPspKey,             NULL,                                        NULL,                    ClearTpmKeyType           },
#endif
  {&mL05WmiSetupItem.L05SecurityChipValid,               &mL05WmiSetupItem.L05SecurityChip,               &mSetupNvData.TpmHide,                       &mSaveSetupNvData,       EnableDisableValueType    },
  {&mL05WmiSetupItem.L05ClearSecurityChipKeyValid,       &mL05WmiSetupItem.L05ClearSecurityChipKey,       NULL,                                        NULL,                    ClearTpmKeyType           },
  {&mL05WmiSetupItem.L05DeviceGuardValid,                &mL05WmiSetupItem.L05DeviceGuard,                &mSetupNvData.L05DeviceGuard,                &mSaveSetupNvData,       DeviceGuardValueType      },
  //
  // I/O Port Access Submenu
  //
  {&mL05WmiSetupItem.L05EthernetLanValid,                &mL05WmiSetupItem.L05EthernetLan,                &mSetupNvData.L05EthernetLan,                &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05WirelessWanValid,                &mL05WmiSetupItem.L05WirelessWan,                &mSetupNvData.L05WirelessWan,                &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05BluetoothValid,                  &mL05WmiSetupItem.L05Bluetooth,                  &mSetupNvData.L05Bluetooth,                  &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05UsbPortValid,                    &mL05WmiSetupItem.L05UsbPort,                    &mSetupNvData.L05UsbPort,                    &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05MemoryCardSlotValid,             &mL05WmiSetupItem.L05MemoryCardSlot,             &mSetupNvData.L05MemoryCardSlot,             &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05SmartCardSlotValid,              &mL05WmiSetupItem.L05SmartCardSlot,              &mSetupNvData.L05SmartCardSlot,              &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05IntegratedCameraValid,           &mL05WmiSetupItem.L05IntegratedCamera,           &mSetupNvData.L05IntegratedCamera,           &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05MicrophoneValid,                 &mL05WmiSetupItem.L05Microphone,                 &mSetupNvData.L05Microphone,                 &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05FingerprintReaderValid,          &mL05WmiSetupItem.L05FingerprintReader,          &mSetupNvData.L05FingerprintReader,          &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05ThunderboltValid,                &mL05WmiSetupItem.L05Thunderbolt,                &mSetupNvData.L05Thunderbolt,                &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05NfcDeviceValid,                  &mL05WmiSetupItem.L05NfcDevice,                  &mSetupNvData.L05NfcDevice,                  &mSaveSetupNvData,       DisableEnableValueType    },
  //
  // Intel (R) SGX Submenu
  //
  {&mL05WmiSetupItem.L05IntelSgxValid,                   &mL05WmiSetupItem.L05IntelSgx,                   NULL,                                        NULL,                    SetupValueTypeMax         },
  //
  // Security Page
  //
  {&mL05WmiSetupItem.L05SecureBootValid,                 &mL05WmiSetupItem.L05SecureBoot,                 &mSetupNvData.L05SecureBoot,                 &mSaveSetupNvData,       SecureBootValueType       },
  {&mL05WmiSetupItem.L05SecureBootStatusValid,           &mL05WmiSetupItem.L05SecureBootStatus,           NULL,                                        NULL,                    SetupValueTypeMax         },
  {&mL05WmiSetupItem.L05PlatformModeValid,               &mL05WmiSetupItem.L05PlatformMode,               &mSetupNvData.L05PlatformMode,               &mSaveSetupNvData,       PlatformModeValueType     },
  {&mL05WmiSetupItem.L05SecureBootModeValid,             &mL05WmiSetupItem.L05SecureBootMode,             &mSetupNvData.L05SecureBootMode,             &mSaveSetupNvData,       SecureBootModeValueType   },
  {&mL05WmiSetupItem.L05ResetToSetupModeValid,           &mL05WmiSetupItem.L05ResetToSetupMode,           NULL,                                        NULL,                    ResetToSetupModeType      },
  {&mL05WmiSetupItem.L05RestoreFactoryKeysValid,         &mL05WmiSetupItem.L05RestoreFactoryKeys,         NULL,                                        NULL,                    RestoreFactoryKeysType    },

  //
  // Boot Page
  //
  {&mL05WmiSetupItem.L05BootModeValid,                   &mL05WmiSetupItem.L05BootMode,                   &mSetupNvData.BootType,                      &mSaveSetupNvData,       BootModeValueType         },
  {&mL05WmiSetupItem.L05BootPriorityValid,               &mL05WmiSetupItem.L05BootPriority,               &mSetupNvData.BootNormalPriority,            &mSaveSetupNvData,       BootPriorityValueType     },
  {&mL05WmiSetupItem.L05FastBootValid,                   &mL05WmiSetupItem.L05FastBoot,                   &mSetupNvData.L05FastBoot,                   &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05UsbBootValid,                    &mL05WmiSetupItem.L05UsbBoot,                    &mSetupNvData.UsbBoot,                       &mSaveSetupNvData,       EnableDisableValueType    },
  {&mL05WmiSetupItem.L05PxeBootToLanValid,               &mL05WmiSetupItem.L05PxeBootToLan,               &mSetupNvData.PxeBootToLan,                  &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05PxeIpv4FirstValid,               &mL05WmiSetupItem.L05PxeIpv4First,               &mSetupNvData.L05PxeIpv4First,               &mSaveSetupNvData,       DisableEnableValueType    },
  {&mL05WmiSetupItem.L05EfiBootOrderValid,               NULL,                                            NULL,                                        &mSavePhysicalBootOrder, EfiBootOrderType          },
  {&mL05WmiSetupItem.L05LegacyBootOrderValid,            NULL,                                            NULL,                                        &mSavePhysicalBootOrder, LegacyBootOrderType       },

  //
  // Exit Page
  //
  {&mL05WmiSetupItem.L05OsOptimizedDefaultValid,         &mL05WmiSetupItem.L05OsOptimizedDefault,         &mSetupNvData.L05OsOptimizedDefault,         &mSaveSetupNvData,       EnableDisableValueType    },
};

//
// WMI Select Value List Map
//
WMI_SELECT_VALUE_LIST_MAP               mWmiSelectValueListMap[] = {
// SetupDataType,              SelectValueList,            SelectValueCount
  {DisableEnableValueType,     mDisableEnableValueMap,     sizeof (mDisableEnableValueMap)    },
  {EnableDisableValueType,     mEnableDisableValueMap,     sizeof (mEnableDisableValueMap)    },
  {NullDisableEnableValueType, mNullDisableEnableValueMap, sizeof (mNullDisableEnableValueMap)},
  {NullEnableDisableValueType, mNullEnableDisableValueMap, sizeof (mNullEnableDisableValueMap)},
  {SATAControllerValueType,    mSATAControllerValueMap,    sizeof (mSATAControllerValueMap)   },
  {GraphicsDeviceValueType,    mGraphicsDeviceValueMap,    sizeof (mGraphicsDeviceValueMap)   },
  {DeviceGuardValueType,       mDisableEnableValueMap,     sizeof (mDisableEnableValueMap)    },
  {IntelSgxValueType,          mIntelSgxValueMap,          sizeof (mIntelSgxValueMap)         },
  {SecureBootValueType,        mDisableEnableValueMap,     sizeof (mDisableEnableValueMap)    },
  {PlatformModeValueType,      mPlatformModeValueMap,      sizeof (mPlatformModeValueMap)     },
  {SecureBootModeValueType,    mSecureBootModeValueMap,    sizeof (mSecureBootModeValueMap)   },
  {BootModeValueType,          mBootModeValueMap,          sizeof (mBootModeValueMap)         },
  {BootPriorityValueType,      mBootPriorityValueMap,      sizeof (mBootPriorityValueMap)     },
};

L05_WMI_ODM_FUNCTION_SUPPORT_SYNC_MAP   mL05OdmFunctionSupportSyncMap[] = {
// L05OdmFunctionSupport,                              L05WmiSetupItemValid
  {&mSetupNvData.L05OdmWirelessLanImplement,           &mL05WmiSetupItem.L05WirelessLanValid               },
  {&mSetupNvData.L05VmdControllerSupport,              &mL05WmiSetupItem.L05VmdControllerValid             },
  {&mSetupNvData.L05GraphicsDeviceSupport,             &mL05WmiSetupItem.L05GraphicsDeviceValid            },
  {&mSetupNvData.L05VtSupport,                         &mL05WmiSetupItem.L05IntelVtValid                   },
  {&mSetupNvData.L05VtdSupport,                        &mL05WmiSetupItem.L05IntelVtdValid                  },
  {&mSetupNvData.L05VtSupport,                         &mL05WmiSetupItem.L05AmdVTechnologyValid            },
  {&mSetupNvData.L05HyperThreadingSupport,             &mL05WmiSetupItem.L05IntelHyperThreadingValid       },
  {&mSetupNvData.L05HotKeyModeSupport,                 &mL05WmiSetupItem.L05HotKeyModeValid                },
  {&mSetupNvData.L05FoolProofFnCtrlSupport,            &mL05WmiSetupItem.L05FoolProofFnCtrlValid           },
  {&mSetupNvData.L05AlwaysOnUsbSupport,                &mL05WmiSetupItem.L05AlwaysOnUsbValid               },
  {&mSetupNvData.L05ChargeInBatteryModeSupport,        &mL05WmiSetupItem.L05ChargeInBatteryModeValid       },
  {&mSetupNvData.L05RestoreDefaultOverclockingSupport, &mL05WmiSetupItem.L05RestoreDefaultOverclockingValid},
  {&mSetupNvData.L05WakeOnVoiceSupport,                &mL05WmiSetupItem.L05WakeOnVoiceValid               },
  {&mSetupNvData.L05UltraQuietModeSupport,             &mL05WmiSetupItem.L05UltraQuietModeValid            },
  {&mSetupNvData.L05OdmTpmImplement,                   &mL05WmiSetupItem.L05IntelPttValid                  },
  {&mSetupNvData.L05OdmTpmImplement,                   &mL05WmiSetupItem.L05ClearIntelPttKeyValid          },
  {&mSetupNvData.L05OdmTpmImplement,                   &mL05WmiSetupItem.L05AmdPspValid                    },
  {&mSetupNvData.L05OdmTpmImplement,                   &mL05WmiSetupItem.L05ClearAmdPspKeyValid            },
  {&mSetupNvData.L05OdmTpmImplement,                   &mL05WmiSetupItem.L05SecurityChipValid              },
  {&mSetupNvData.L05OdmTpmImplement,                   &mL05WmiSetupItem.L05ClearSecurityChipKeyValid      },
  {&mSetupNvData.L05DeviceGuardSupport,                &mL05WmiSetupItem.L05DeviceGuardValid               },
  {&mSetupNvData.L05FastBootImplement,                 &mL05WmiSetupItem.L05FastBootValid                  },
  {&mSetupNvData.L05OdmEthernetLanImplement,           &mL05WmiSetupItem.L05EthernetLanValid               },
  {&mSetupNvData.L05OdmWirelessWanImplement,           &mL05WmiSetupItem.L05WirelessWanValid               },
  {&mSetupNvData.L05OdmBluetoothImplement,             &mL05WmiSetupItem.L05BluetoothValid                 },
  {&mSetupNvData.L05OdmUsbPortImplement,               &mL05WmiSetupItem.L05UsbPortValid                   },
  {&mSetupNvData.L05OdmMemoryCardSlotImplement,        &mL05WmiSetupItem.L05MemoryCardSlotValid            },
  {&mSetupNvData.L05OdmSmartCardSlotImplement,         &mL05WmiSetupItem.L05SmartCardSlotValid             },
  {&mSetupNvData.L05OdmIntegratedCameraImplement,      &mL05WmiSetupItem.L05IntegratedCameraValid          },
  {&mSetupNvData.L05OdmMicrophoneImplement,            &mL05WmiSetupItem.L05MicrophoneValid                },
  {&mSetupNvData.L05OdmFingerprintReaderImplement,     &mL05WmiSetupItem.L05FingerprintReaderValid         },
  {&mSetupNvData.L05OdmThunderboltImplement,           &mL05WmiSetupItem.L05ThunderboltValid               },
  {&mSetupNvData.L05OdmNfcDeviceImplement,             &mL05WmiSetupItem.L05NfcDeviceValid                 },
};

//
// Map List Count
//
UINT16                                  mL05WmiSetupItemSyncMapListCount        = sizeof (mL05WmiSetupItemSyncMap)       / sizeof (L05_WMI_SETUP_ITEM_SYNC_MAP);
UINT16                                  mWmiSelectValueListMapListCount         = sizeof (mWmiSelectValueListMap)        / sizeof (WMI_SELECT_VALUE_LIST_MAP);
UINT16                                  mL05OdmFunctionSupportSyncMapListCount  = sizeof (mL05OdmFunctionSupportSyncMap) / sizeof (L05_WMI_ODM_FUNCTION_SUPPORT_SYNC_MAP);


