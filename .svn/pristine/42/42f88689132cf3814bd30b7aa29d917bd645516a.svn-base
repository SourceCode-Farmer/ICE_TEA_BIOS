/** @file
;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Platform flash device access library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#ifndef __PLATFORM_FLASH_ACCESS_LIB_H__
#define __PLATFORM_FLASH_ACCESS_LIB_H__

#include <Protocol/FirmwareManagement.h>
#include <Protocol/Spi.h>
//
//  General datastructure to pass a single Flash ACCESS request to different flash storage media.
//  Request can be Flash Read or Write
//  Use this structure to provide stream level I/O operation.
//  The caller such as FMP instance driver should not care about underlying hardware devices.
//
//  1) To descrbe each flash region in Flash Descriptor mode, config FlashRegionType. It is now only useful for NOR SPI flash.
//  2) To describe source/target flash address, configue FlashAddress. Address range is implementation specific.
//  3) To describe Input/Output data buffer, configure Data/DataSize
//  4) To support BiosGuard Update, set BgupImage/BgupImageSize
//

typedef struct {
  //
  // Flash Read/Update Address.
  // It is not required in BiosGuard Update mode. Script itself carries address carries
  //
  //FLASH_MEDIA_TYPE   MediaType;           //  Media type of the Flash. It is a reserved field for future extension
  FLASH_REGION_TYPE  FlashRegionType;       //  Flash Region type for flash cycle which is listed in the Descriptor. Now only used by SPI flash
  UINTN              FlashAddress;          //  Flash Address
                                            //     For SPI flash. The Flash Linear Address accessed by CPU
                                            //     For other storage, address is interpreted by storage
  //
  // For Flash Write/Update, describe input update image
  // For Flash Read, describe output data buffer
  //
  UINT8              *Data;
  UINTN              DataSize;

  //
  // Signed BgupImage Script for update. Only required with a write request when BiosGuard is enabled
  //
  UINT8              *BgupImage;
  UINTN              BgupImageSize;

} FLASH_ACCESS_REQUEST;

/*
  Get and return the pointer of SPI protocol instance.

  @param[in]    VOID

  @retval       PCH_SPI_PROTOCOL    Pointer point to the SPI procotol.
*/
PCH_SPI_PROTOCOL*
EFIAPI
GetPchSpiProtocol (
  VOID
  );

/**
  Generic API for Gen3 capsule update, Read data from SPI flash.

  @param[in]  FLASH_ACCESS_REQUEST     Request strucutre for read.

  @retval     EFI_SUCCESS.             Operation is successful.
  @retval     EFI_OUT_OF_RESOURCES     Failed to allocate needed memory buffer.
  @retval     EFI_VOLUME_CORRUPTED     The block is not updated as expected.
  @retval     Others                   If there is any device errors.
**/
//[-start-201030-IB16810136-modify]//
EFI_STATUS
EFIAPI
FlashReadAdl (
  IN FLASH_ACCESS_REQUEST  *ReadReq
  );
//[-end-201030-IB16810136-modify]//
/**
  Generic API for Gen3 capsule update, Write content with SPI flash.

  @param[in]  WriteReq                 Request information for update flash.

  @retval     EFI_SUCCESS.             Operation is successful.
  @retval     EFI_OUT_OF_RESOURCES     Failed to allocate needed memory buffer.
  @retval     EFI_VOLUME_CORRUPTED     The block is not updated as expected.
  @retval     Others                   If there is any device errors.
**/
EFI_STATUS
EFIAPI
FlashUpdate (
  IN FLASH_ACCESS_REQUEST                           *WriteReq,
  IN EFI_FIRMWARE_MANAGEMENT_UPDATE_IMAGE_PROGRESS  Progress,        OPTIONAL
  IN UINTN                                          StartPercentage,
  IN UINTN                                          EndPercentage
  );
#endif
