## @file
# Component description file for One Click Recovery Setup feature DXE Driver.
#
#******************************************************************************
#* Copyright (c) 2021 , Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
# @copyright
#   INTEL CONFIDENTIAL
#   Copyright 2020 - 2021 Intel Corporation.
#
#   The source code contained or described herein and all documents related to the
#   source code ("Material") are owned by Intel Corporation or its suppliers or
#   licensors. Title to the Material remains with Intel Corporation or its suppliers
#   and licensors. The Material may contain trade secrets and proprietary and
#   confidential information of Intel Corporation and its suppliers and licensors,
#   and is protected by worldwide copyright and trade secret laws and treaty
#   provisions. No part of the Material may be used, copied, reproduced, modified,
#   published, uploaded, posted, transmitted, distributed, or disclosed in any way
#   without Intel's prior express written permission.
#
#   No license under any patent, copyright, trade secret or other intellectual
#   property right is granted to or conferred upon you by disclosure or delivery
#   of the Materials, either expressly, by implication, inducement, estoppel or
#   otherwise. Any license under such intellectual property rights must be
#   express and approved by Intel in writing.
#
#   Unless otherwise agreed by Intel in writing, you may not remove or alter
#   this notice or any other notice embedded in Materials by Intel or
#   Intel's suppliers or licensors in any way.
#
#   This file contains a 'Sample Driver' and is licensed as such under the terms
#   of your license agreement with Intel or your vendor. This file may be modified
#   by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
##

[Defines]
  INF_VERSION = 0x00010017
  BASE_NAME = OneClickRecovery
  FILE_GUID = 86997AE6-D443-4939-B729-A3D0F32FB772
  VERSION_STRING = 1.0
  MODULE_TYPE = DXE_DRIVER
  UEFI_SPECIFICATION_VERSION = 2.00
  ENTRY_POINT = OneClickRecoveryEntryPoint

#
# The following information is for reference only and not required by the build tools.
#
# VALID_ARCHITECTURES = IA32 X64 IPF
#

[LibraryClasses]
  BaseLib
  IoLib
  DebugLib
  UefiDriverEntryPoint
  UefiRuntimeServicesTableLib
  UefiLib
  UefiBootManagerLib
#[-start-210114-IB16560236-remove]#
#  PlatformBootManagerLib
#[-end-210114-IB16560236-remove]#
  DevicePathLib
  DxeAsfHeciLib
  DxeAsfLib
  NetLib
  DxeAmtHeciLib
  HobLib
  PcdLib
  HttpLib

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  NetworkPkg/NetworkPkg.dec
  SecurityPkg/SecurityPkg.dec
#[-start-211111-IB09480171-add]#
  InsydeModulePkg/InsydeModulePkg.dec
#[-end-211111-IB09480171-add]#

#[-start-211111-IB09480171-add]#
[Sources.X64]
  X64/CallSmmAsfSecureBoot.asm      |MSFT
  X64/CallSmmAsfSecureBoot.nasm    |GCC
#[-end-211111-IB09480171-add]#

[Sources]
  OneClickRecovery.c
  OneClickRecoverySupport.h
  OneClickRecoveryHttpsSupport.c
  OneClickRecoveryFileSupport.c
  OemOneClickRecovery.c

[Guids]
  gSetupVariableGuid                           ## CONSUMES
  gEfiTlsCaCertificateGuid                     ## SOMETIMES_PRODUCES
  gEfiCertX509Guid                             ## SOMETIMES_CONSUMES
  gEfiFileInfoGuid                             ## SOMETIMES_CONSUMES
  gMeSetupVariableGuid                         ## CONSUMES
  gEfiSecureBootEnableDisableGuid              ## SOMETIMES_CONSUMES
  gEfiEventExitBootServicesGuid                ## CONSUMES
#[-start-211111-IB09480171-add]#
  gEfiGenericVariableGuid
#[-end-211111-IB09480171-add]#
#[-start-211215-IB09480180-add]#
  gBootOrderHookEnableGuid
  gBootOrderHookDisableGuid
#[-end-211215-IB09480180-add]#

[Protocols]
  gOneClickRecoveryProtocolGuid                ## PRODUCES
  gEfiBlockIoProtocolGuid                      ## SOMETIMES_CONSUMES
  gEfiDevicePathProtocolGuid                   ## SOMETIMES_CONSUMES
  gEfiSimpleFileSystemProtocolGuid             ## SOMETIMES_CONSUMES
  gEfiSimpleNetworkProtocolGuid                ## SOMETIMES_CONSUMES
  gWiFiProfileSyncProtocolGuid                 ## SOMETIMES_CONSUMES
  gEfiWiFi2ProtocolGuid                        ## SOMETIMES_CONSUMES
  gEfiPciIoProtocolGuid                        ## SOMETIMES_CONSUMES
  gEfiHttpBootCallbackProtocolGuid             ## SOMETIMES_CONSUMES
  gEfiLoadFileProtocolGuid                     ## CONSUMES
  gEdkiiHttpCallbackProtocolGuid               ## SOMETIMES_PRODUCES
  gEfiRscHandlerProtocolGuid                   ## SOMETIMES_PRODUCES

[Pcd]
  gPlatformModuleTokenSpaceGuid.PcdWifiProfileSyncEnable   ## CONSUMES
  gEfiNetworkPkgTokenSpaceGuid.PcdHttpIoTimeout            ## PRODUCES
#[-start-211111-IB09480171-add]#
  gInsydeTokenSpaceGuid.PcdSoftwareSmiPort
#[-end-211111-IB09480171-add]#

#[-start-211215-IB09480180-add]#
[FeaturePcd]
  gInsydeTokenSpaceGuid.PcdAutoCreateDummyBootOption
#[-end-211215-IB09480180-add]#

[Depex]
  gHeciProtocolGuid
