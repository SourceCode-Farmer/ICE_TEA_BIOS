//*****************************************************************************
// Copyright (c) 2012 - 2021, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.         
//******************************************************************************
/*++
Abstract:
This MOF file is used for Lenovo LBG product MFG and MBV tools WMI service interface.

History:
Date          Name          Version    Change Notes
2020.02.23    Steven Wang   V1.00      Initial release.

Module Name:
LfcWmiService.mof
--*/

//#PRAGMA AUTORECOVER 
//[abstract]
//
// Wmi internal classes, for event notify
//
class WMIEvent : __ExtrinsicEvent
{
};

//
// Customized classes
//
class Lfc_Common_Wmi_Service
{
};

[WMI,
 Locale("MS\\0x409"),
 Description("This class contains the definition of the package used in other classes"),
 guid("{7D728687-50AE-44C4-AAB2-D1F1AACAC9AA}")
]

class LfcPackage
{
    [WmiDataId(1),
     read, write,
     Description("16 bytes of data")
    ] uint8 Bytes[16];
};


[WMI, Dynamic: ToInstance, Provider("WMIProv"), WmiExpense(1),
 Description("Lfc Tools WMI Service"),
 GUID("{7B0D0CF0-5D6C-439E-86A5-A3619B623492}"),
 Locale("MS\\0x409")
]
class Lfc_Tools_Wmi_Service : Lfc_Common_Wmi_Service
{
    [key, read] string InstanceName;
    [read] boolean Active;
    [Implemented, WmiMethodId(1), Description("EC control function")]
        void LfcWmiEcControlUlong([in] uint32 parameter, [out] uint32 return);	
		
};

[WMI,
 Dynamic,
 Provider("WmiProv"),
 Description("Class containing event generated ULong data"),
 Locale("MS\\0x409"),
 guid("{83D7DB01-3C6D-42DC-BA1B-56DAA597F2B2}")
]

class Lfc_EventULong : WmiEvent
{
    [key, read]
     string InstanceName;
    [read] boolean Active;

    [WmiDataId(1),
     read, write,
     Description("Notification value")
    ] uint32 ULong;
};


