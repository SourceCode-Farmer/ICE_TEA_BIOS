/** @file

;******************************************************************************
;* Copyright (c) 1983 - 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "PlatformInfo.h"
#include <Protocol/CpuInfo.h>

CPU_INFO_DISPLAY_TABLE        mDisplayCpuInfoFunction[]       = {CPU_INFO_DISPLAY_TABLE_LIST};
CPU_CAPABILITY_DISPLAY_TABLE  mDisplayCpuCapabilityFunction[] = {CPU_CAPABILITY_DISPLAY_TABLE_LIST};

#define IS_DATA_RECORD_FOR_CPU_INFO     (DataHeader->RecordType == ProcessorEnabledCoreCountRecordType) || \
                                        (DataHeader->RecordType == ProcessorThreadCountRecordType)      || \
                                        (DataHeader->RecordType == CpuUcodeRevisionDataRecordType)      || \
                                        (DataHeader->RecordType == ProcessorIdRecordType)

EFI_STATUS
GetCpuMiscInfo (
  IN   VOID                       *OpCodeHandle,
  IN   EFI_HII_HANDLE             MainHiiHandle,
  IN   EFI_HII_HANDLE             AdvanceHiiHandle,
  IN   CHAR16                     *StringBuffer
  )
{
  EFI_STATUS                    Status;
  UINTN                         Index;
  UINT64                        MonotonicCount;
  CPUID_VERSION_INFO_ECX        Ecx;
  EFI_SMBIOS_HANDLE             SmbiosHandle;
  EFI_SMBIOS_TYPE               RecordType;
  EFI_SMBIOS_PROTOCOL           *mSmbios;
  EFI_SMBIOS_TABLE_HEADER       *Buffer;
  SMBIOS_TABLE_TYPE4            *Record;

  Status         = EFI_SUCCESS;
  mSmbios        = NULL;
  Record         = NULL;

  MonotonicCount = 0;

  Status = gBS->LocateProtocol (
                  &gEfiSmbiosProtocolGuid,
                  NULL,
                  (VOID **)&mSmbios
                  );
  if (EFI_ERROR (Status) || mSmbios == NULL) {
    goto ExecuteCpuCapability;
  }

  //
  // Get Processor info from DataHub
  //
  SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;
  RecordType = EFI_SMBIOS_TYPE_PROCESSOR_INFORMATION;
  //
  // Get SMBIOS type 4 data
  //
  Status = mSmbios->GetNext (
                      mSmbios,
                      &SmbiosHandle,
                      &RecordType,
                      &Buffer,
                      NULL
                      );
  Record = (SMBIOS_TABLE_TYPE4 *) Buffer;

  for (Index = 0; mDisplayCpuInfoFunction[Index].DisplayCpuInfoFunction != NULL; Index++) {
    if (mDisplayCpuInfoFunction[Index].Option == DISPLAY_ENABLE) {
      ZeroMem(StringBuffer, 0x100);
      mDisplayCpuInfoFunction[Index].DisplayCpuInfoFunction (OpCodeHandle, MainHiiHandle, AdvanceHiiHandle, StringBuffer, Record);
    }
  }
ExecuteCpuCapability:
  //
  // Check if the processor supports VT-x, VT-x and TXT.
  //
  AsmCpuid (CPUID_VERSION_INFO, NULL, NULL, &Ecx.Uint32, NULL);
//[-start-190613-IB16990066-add]//
  for (Index = 0; Index < (sizeof (mDisplayCpuCapabilityFunction) / sizeof (CPU_CAPABILITY_DISPLAY_TABLE)) && mDisplayCpuCapabilityFunction[Index].DisplayCpuCapabilityFunction != NULL; Index++) {
//[-end-190613-IB16990066-add]//
	if (mDisplayCpuCapabilityFunction[Index].Option == DISPLAY_ENABLE) {
      ZeroMem(StringBuffer, 0x100);
      mDisplayCpuCapabilityFunction[Index].DisplayCpuCapabilityFunction (OpCodeHandle, MainHiiHandle, AdvanceHiiHandle, StringBuffer, Ecx);
    }
  }

  return Status;
}


EFI_STATUS
GetCpuIdFunc (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS         Status;
  EFI_STRING_ID      CpuIdText;
  EFI_STRING_ID      CpuIdString;
  UINTN              ProcessorSignature;
  CHAR16             *ProcessorString;

  Status             = EFI_UNSUPPORTED;
  ProcessorSignature = 0x00;
  ProcessorString    = NULL;

  CopyMem (&ProcessorSignature, &(Record->ProcessorId.Signature), sizeof (EFI_PROCESSOR_SIGNATURE));

  ProcessorString = HiiGetString (AdvanceHiiHandle, STRING_TOKEN (STR_PROCESSOR_VALUE), NULL);
  UnicodeSPrint (StringBuffer, 0x100, L"0x%x (%s)", ProcessorSignature, ProcessorString);

  CpuIdString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);
  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_CPUID_TEXT),
    MainHiiHandle,
    &CpuIdText
    );

  HiiCreateTextOpCode (OpCodeHandle, CpuIdText, 0, CpuIdString);

  return Status;
}


EFI_STATUS
GetCpuSpeedFunc (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS         Status = EFI_UNSUPPORTED;
  EFI_STRING_ID      CpuSpeedText;
  EFI_STRING_ID      CpuSpeedString;
  UINTN              CpuSpeed;
  CPU_INFO_PROTOCOL  *DxeCpuInfo;

  Status = gBS->LocateProtocol (
                  &gCpuInfoProtocolGuid, 
                  NULL, 
                  (VOID **) &DxeCpuInfo
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR,"Failed to locate DxeCpuInfo Protocol\n"));
    return Status;
  }

  if (DxeCpuInfo->CpuInfo[BIG_CORE].NumCores != 0) {
    CpuSpeed = (UINTN) DxeCpuInfo->CpuInfo[BIG_CORE].IntendedFreq;
  }
  else {
    CpuSpeed = (UINTN) DxeCpuInfo->CpuInfo[SMALL_CORE].IntendedFreq;
  }

  UnicodeSPrint (StringBuffer, 0x100, L"%d MHz", CpuSpeed);

  CpuSpeedString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_CPU_SPEED_TEXT),
    MainHiiHandle,
    &CpuSpeedText
    );

  HiiCreateTextOpCode (OpCodeHandle, CpuSpeedText, 0, CpuSpeedString);

  return Status;
}

EFI_STATUS
GetL1DataCache (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS     Status = EFI_UNSUPPORTED;
  EFI_STRING_ID  DataCacheText;
  EFI_STRING_ID  DataCacheString;
  CHAR16         *L1DataCacheValue;
  EFI_STRING_ID             CoreInfoTitleString;
  EFI_STRING_ID             CoreInfoHelp;
  EFI_STRING_ID             BlankString;

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_CORE_INFO_FORM_TITLE),
    MainHiiHandle,
    &CoreInfoTitleString
    );

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_CORE_INFO_FORM_HELP),
    MainHiiHandle,
    &CoreInfoHelp
    );

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_BLANK_STRING),
    MainHiiHandle,
    &BlankString
    );

  HiiCreateTextOpCode (OpCodeHandle,CoreInfoTitleString, CoreInfoHelp, BlankString );

  L1DataCacheValue = HiiGetString (
                       AdvanceHiiHandle,
                       STRING_TOKEN (STR_CORE_L1_DATA_CACHE_VALUE),
                       NULL
                       );
  UnicodeSPrint (StringBuffer, 0x100, L"%s", L1DataCacheValue);

  DataCacheString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_DATA_CACHE_TEXT),
    MainHiiHandle,
    &DataCacheText
    );

  HiiCreateTextOpCode (OpCodeHandle, DataCacheText, 0, DataCacheString);

  return Status;
}

EFI_STATUS
GetL1Insruction (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS     Status = EFI_UNSUPPORTED;
  EFI_STRING_ID  InstructionCacheText;
  EFI_STRING_ID  InstructionString;
  CHAR16         *L1InstructionCacheValue;

  L1InstructionCacheValue = HiiGetString (
                              AdvanceHiiHandle,
                              STRING_TOKEN (STR_CORE_L1_INSTR_CACHE_VALUE),
                              NULL
                              );
  UnicodeSPrint (StringBuffer, 0x100, L"%s", L1InstructionCacheValue);

  InstructionString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_INSTRUCTION_CACHE_TEXT),
    MainHiiHandle,
    &InstructionCacheText
    );

  HiiCreateTextOpCode (OpCodeHandle, InstructionCacheText, 0, InstructionString);

  return Status;
}


EFI_STATUS
GetL2Cache (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS     Status = EFI_UNSUPPORTED;
  EFI_STRING_ID  L2CacheText;
  EFI_STRING_ID  L2CacheString;
  CHAR16         *L2CacheValue;

  L2CacheValue = HiiGetString (
                   AdvanceHiiHandle,
                   STRING_TOKEN (STR_CORE_L2_CACHE_VALUE),
                   NULL
                   );
  UnicodeSPrint (StringBuffer, 0x100, L"%s", L2CacheValue);

  L2CacheString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_L2_CACHE_TEXT),
    MainHiiHandle,
    &L2CacheText
    );

  HiiCreateTextOpCode (OpCodeHandle, L2CacheText, 0, L2CacheString);

  return Status;
}

EFI_STATUS
GetL3Cache (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS     Status = EFI_UNSUPPORTED;
  EFI_STRING_ID  L3CacheText;
  EFI_STRING_ID  L3CacheString;
  CHAR16         *L3CacheValue;

  L3CacheValue = HiiGetString (
                   AdvanceHiiHandle,
                   STRING_TOKEN (STR_CORE_L3_CACHE_VALUE),
                   NULL
                   );
  UnicodeSPrint (StringBuffer, 0x100, L"%s", L3CacheValue);

  L3CacheString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_L3_CACHE_TEXT),
    MainHiiHandle,
    &L3CacheText
    );

  HiiCreateTextOpCode (OpCodeHandle, L3CacheText, 0, L3CacheString);

  return Status;
}

EFI_STATUS
GetL4Cache (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS     Status = EFI_UNSUPPORTED;
  EFI_STRING_ID  L4CacheText;
  EFI_STRING_ID  L4CacheString;
  CHAR16         *L4CacheValue;

  L4CacheValue = HiiGetString (
                   AdvanceHiiHandle,
                   STRING_TOKEN (STR_CORE_L4_CACHE_VALUE),
                   NULL
                   );
  UnicodeSPrint (StringBuffer, 0x100, L"%s", L4CacheValue);

  L4CacheString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_L4_CACHE_TEXT),
    MainHiiHandle,
    &L4CacheText
    );

  HiiCreateTextOpCode (OpCodeHandle, L4CacheText, 0, L4CacheString);

  return Status;
}

EFI_STATUS
GetL1DataAtomCache (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS         Status = EFI_UNSUPPORTED;
  EFI_STRING_ID      DataCacheText;
  EFI_STRING_ID      DataCacheString;
  CHAR16             *L1DataCacheValue;
  EFI_STRING_ID      AtomCoreInfoTitleString;
  EFI_STRING_ID      AtomCoreInfoHelp;
  EFI_STRING_ID      BlankString;
  CPU_INFO_PROTOCOL  *DxeCpuInfo;

  Status = gBS->LocateProtocol (
                  &gCpuInfoProtocolGuid, 
                  NULL, 
                  (VOID **) &DxeCpuInfo
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR,"Failed to locate DxeCpuInfo Protocol\n"));
    return Status;
  }

  if (DxeCpuInfo->CpuInfo[SMALL_CORE].NumCores != 0) {  
    NewStringToHandle (
      AdvanceHiiHandle,
      STRING_TOKEN (STR_ATOM_CORE_INFO_FORM_TITLE),
      MainHiiHandle,
      &AtomCoreInfoTitleString
      );

    NewStringToHandle (
      AdvanceHiiHandle,
      STRING_TOKEN (STR_ATOM_CORE_INFO_FORM_HELP),
      MainHiiHandle,
      &AtomCoreInfoHelp
      );

    NewStringToHandle (
      AdvanceHiiHandle,
      STRING_TOKEN (STR_BLANK_STRING),
      MainHiiHandle,
      &BlankString
      );

    HiiCreateTextOpCode (OpCodeHandle,AtomCoreInfoTitleString, AtomCoreInfoHelp, BlankString); 

    L1DataCacheValue = HiiGetString (
                         AdvanceHiiHandle,
                         STRING_TOKEN (STR_ATOM_CORE_L1_DATA_CACHE_VALUE),
                         NULL
                         );
    UnicodeSPrint (StringBuffer, 0x100, L"%s", L1DataCacheValue);

    DataCacheString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

    NewStringToHandle (
      AdvanceHiiHandle,
      STRING_TOKEN (STR_ATOM_DATA_CACHE_TEXT),
      MainHiiHandle,
      &DataCacheText
      );

    HiiCreateTextOpCode (OpCodeHandle, DataCacheText, 0, DataCacheString);
  }
  return Status;
}

EFI_STATUS
GetL1AtomInsruction (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS         Status = EFI_UNSUPPORTED;
  EFI_STRING_ID      InstructionCacheText;
  EFI_STRING_ID      InstructionString;
  CHAR16             *L1InstructionCacheValue;
  CPU_INFO_PROTOCOL  *DxeCpuInfo;

  Status = gBS->LocateProtocol (
                  &gCpuInfoProtocolGuid, 
                  NULL, 
                  (VOID **) &DxeCpuInfo
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR,"Failed to locate DxeCpuInfo Protocol\n"));
    return Status;
  }

  if (DxeCpuInfo->CpuInfo[SMALL_CORE].NumCores != 0) {
    L1InstructionCacheValue = HiiGetString (
                                AdvanceHiiHandle,
                                STRING_TOKEN (STR_ATOM_CORE_L1_INSTR_CACHE_VALUE),
                                NULL
                                );
    UnicodeSPrint (StringBuffer, 0x100, L"%s", L1InstructionCacheValue);

    InstructionString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

    NewStringToHandle (
      AdvanceHiiHandle,
      STRING_TOKEN (STR_ATOM_INSTRUCTION_CACHE_TEXT),
      MainHiiHandle,
      &InstructionCacheText
      );

    HiiCreateTextOpCode (OpCodeHandle, InstructionCacheText, 0, InstructionString);
  }
  return Status;
}


EFI_STATUS
GetL2AtomCache (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS         Status = EFI_UNSUPPORTED;
  EFI_STRING_ID      L2CacheText;
  EFI_STRING_ID      L2CacheString;
  CHAR16             *L2CacheValue;
  CPU_INFO_PROTOCOL  *DxeCpuInfo;

  Status = gBS->LocateProtocol (
                  &gCpuInfoProtocolGuid, 
                  NULL, 
                  (VOID **) &DxeCpuInfo
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR,"Failed to locate DxeCpuInfo Protocol\n"));
    return Status;
  }

  if (DxeCpuInfo->CpuInfo[SMALL_CORE].NumCores != 0) {
    L2CacheValue = HiiGetString (
                     AdvanceHiiHandle,
                     STRING_TOKEN (STR_ATOM_CORE_L2_CACHE_VALUE),
                     NULL
                     );
    UnicodeSPrint (StringBuffer, 0x100, L"%s", L2CacheValue);

    L2CacheString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

    NewStringToHandle (
      AdvanceHiiHandle,
      STRING_TOKEN (STR_ATOM_L2_CACHE_TEXT),
      MainHiiHandle,
      &L2CacheText
      );

    HiiCreateTextOpCode (OpCodeHandle, L2CacheText, 0, L2CacheString);
  }
  return Status;
}

EFI_STATUS
GetL3AtomCache (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS         Status = EFI_UNSUPPORTED;
  EFI_STRING_ID      L3CacheText;
  EFI_STRING_ID      L3CacheString;
  CHAR16             *L3CacheValue;
  CPU_INFO_PROTOCOL  *DxeCpuInfo;

  Status = gBS->LocateProtocol (
                  &gCpuInfoProtocolGuid, 
                  NULL, 
                  (VOID **) &DxeCpuInfo
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR,"Failed to locate DxeCpuInfo Protocol\n"));
    return Status;
  }

  if (DxeCpuInfo->CpuInfo[SMALL_CORE].NumCores != 0) {
    L3CacheValue = HiiGetString (
                     AdvanceHiiHandle,
                     STRING_TOKEN (STR_ATOM_CORE_L3_CACHE_VALUE),
                     NULL
                     );
   UnicodeSPrint (StringBuffer, 0x100, L"%s", L3CacheValue);

    L3CacheString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

    NewStringToHandle (
      AdvanceHiiHandle,
      STRING_TOKEN (STR_ATOM_L3_CACHE_TEXT),
      MainHiiHandle,
      &L3CacheText
      );

    HiiCreateTextOpCode (OpCodeHandle, L3CacheText, 0, L3CacheString);
  }
  return Status;
}

EFI_STATUS
GetL4AtomCache (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS         Status = EFI_UNSUPPORTED;
  EFI_STRING_ID      L4CacheText;
  EFI_STRING_ID      L4CacheString;
  CHAR16             *L4CacheValue;
  CPU_INFO_PROTOCOL  *DxeCpuInfo;

  Status = gBS->LocateProtocol (
                  &gCpuInfoProtocolGuid, 
                  NULL, 
                  (VOID **) &DxeCpuInfo
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR,"Failed to locate DxeCpuInfo Protocol\n"));
    return Status;
  }

  if (DxeCpuInfo->CpuInfo[SMALL_CORE].NumCores != 0) {
    L4CacheValue = HiiGetString (
                     AdvanceHiiHandle,
                     STRING_TOKEN (STR_ATOM_CORE_L4_CACHE_VALUE),
                     NULL
                     );
    UnicodeSPrint (StringBuffer, 0x100, L"%s", L4CacheValue);

    L4CacheString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

    NewStringToHandle (
      AdvanceHiiHandle,
      STRING_TOKEN (STR_ATOM_L4_CACHE_TEXT),
      MainHiiHandle,
      &L4CacheText
      );

    HiiCreateTextOpCode (OpCodeHandle, L4CacheText, 0, L4CacheString);
  }
  return Status;
}

EFI_STATUS
GetCoreThreadNumFunc (
  IN     VOID                       *OpCodeHandle,
  IN     EFI_HII_HANDLE             MainHiiHandle,
  IN     EFI_HII_HANDLE             AdvanceHiiHandle,
  IN     CHAR16                     *StringBuffer,
  IN     SMBIOS_TABLE_TYPE4         *Record
)
{
  EFI_STATUS     Status = EFI_UNSUPPORTED;
  UINT8          NumOfTotalCore;
  UINT8          NumOfAtomCore;
  UINT8          NumOfCore;
  EFI_STRING_ID  CpuNumText;
  EFI_STRING_ID  CpuNumString;
  UINT8          NumOfTotalThread;
  UINT8          NumOfAtomThread;
  UINT8          NumOfThread;
  CPU_INFO_PROTOCOL  *DxeCpuInfo;

  NumOfTotalCore = Record->EnabledCoreCount;
  NumOfTotalThread = Record->ThreadCount;

  UnicodeSPrint (StringBuffer, 0x100, L"%2d Core(s) / %2d Thread(s)", (UINTN)NumOfTotalCore, (UINTN)NumOfTotalThread);

  CpuNumString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_NUM_OF_TOTAL_CORE_THREAD_TEXT),
    MainHiiHandle,
    &CpuNumText
    );

  HiiCreateTextOpCode (OpCodeHandle, CpuNumText, 0, CpuNumString);

  Status = gBS->LocateProtocol (
                  &gCpuInfoProtocolGuid, 
                  NULL, 
                  (VOID **) &DxeCpuInfo
                  );
  if (EFI_ERROR (Status) || DxeCpuInfo == NULL) {
    goto Exit;
  }

  NumOfAtomCore = DxeCpuInfo->CpuInfo[SMALL_CORE].NumCores;
  NumOfAtomThread = DxeCpuInfo->CpuInfo[SMALL_CORE].NumCores * DxeCpuInfo->CpuInfo[SMALL_CORE].NumHts;

  UnicodeSPrint (StringBuffer, 0x100, L"%2d Core(s) / %2d Thread(s)", (UINTN)NumOfAtomCore, (UINTN)NumOfAtomThread);

  CpuNumString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_NUM_OF_ATOM_CORE_THREAD_TEXT),
    MainHiiHandle,
    &CpuNumText
    );

  HiiCreateTextOpCode (OpCodeHandle, CpuNumText, 0, CpuNumString);

  NumOfCore = DxeCpuInfo->CpuInfo[BIG_CORE].NumCores;
  NumOfThread = DxeCpuInfo->CpuInfo[BIG_CORE].NumCores * DxeCpuInfo->CpuInfo[BIG_CORE].NumHts;

  UnicodeSPrint (StringBuffer, 0x100, L"%2d Core(s) / %2d Thread(s)", (UINTN)NumOfCore, (UINTN)NumOfThread);

  CpuNumString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_NUM_OF_CORE_THREAD_TEXT),
    MainHiiHandle,
    &CpuNumText
    );

  HiiCreateTextOpCode (OpCodeHandle, CpuNumText, 0, CpuNumString);

Exit:
  return Status;
}


EFI_STATUS
GetMicrocodeVersion (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS          Status;
  UINT32              MicroCodeVersion;
  EFI_STRING_ID       MicroCodeVersionText;
  EFI_STRING_ID       MicroCodeVersionString;

  Status              = EFI_UNSUPPORTED;
  MicroCodeVersion    = 0x00;

  MicroCodeVersion = GetCpuUcodeRevision ();
  UnicodeSPrint (StringBuffer, 0x100, L"%08x", (UINTN)MicroCodeVersion);

  MicroCodeVersionString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  Status = NewStringToHandle (
             AdvanceHiiHandle,
             STRING_TOKEN (STR_MICROCODE_UPD_VERSION_TEXT),
             MainHiiHandle,
             &MicroCodeVersionText
             );

  if (!EFI_ERROR(Status)) {
    if (NULL == HiiCreateTextOpCode (OpCodeHandle, MicroCodeVersionText, 0, MicroCodeVersionString)) {
      return EFI_BUFFER_TOO_SMALL;
    }
  }

  return Status;
}


EFI_STATUS
GetTxtCapability (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    CPUID_VERSION_INFO_ECX    VersionInfoEcx
  )
{
  EFI_STATUS                      Status = EFI_UNSUPPORTED;
  EFI_STRING_ID                   TxtCapabilityText;
  EFI_STRING_ID                   TxtCapabilityString;
  CPUID_VERSION_INFO_ECX          Ecx;

  Ecx.Uint32 = VersionInfoEcx.Uint32;

  if (Ecx.Bits.SMX == SUPPORTED) {
    //
    // TXT is supported.
    //
    UnicodeSPrint (StringBuffer, 0x100, L"Supported");
  } else {
    //
    // TXT isn't supported.
    //
    UnicodeSPrint (StringBuffer, 0x100, L"Un-Supported");
  }

  TxtCapabilityString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);
  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_SMX_TEXT),
    MainHiiHandle,
    &TxtCapabilityText
    );

  HiiCreateTextOpCode (OpCodeHandle, TxtCapabilityText, 0, TxtCapabilityString);

  return Status;
}


EFI_STATUS
GetVtdCapability (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer
  )
{
  EFI_STATUS     Status = EFI_UNSUPPORTED;
  EFI_STRING_ID  VtdCapabilityText;
  EFI_STRING_ID  VtdCapabilityString;

  //
  // Please refer to SNB SA v1.1.1 Ch14.1 on how to determine if SA supported VT-d
  //
  if ((McD0PciCfg32 (R_SA_MC_CAPID0_A_OFFSET) & BIT23) == 0) {
    //
    // VT-d is supported.
    //
    UnicodeSPrint (StringBuffer, 0x100, L"Supported");
  } else {
    //
    // VT-d isn't supported.
    //
    UnicodeSPrint (StringBuffer, 0x100, L"Un-Supported");
  }

  VtdCapabilityString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);
  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_VTD_TEXT),
    MainHiiHandle,
    &VtdCapabilityText
    );

  HiiCreateTextOpCode (OpCodeHandle, VtdCapabilityText, 0, VtdCapabilityString);

  return Status;
}


EFI_STATUS
GetVtxCapability (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    CPUID_VERSION_INFO_ECX    VersionInfoEcx
  )
{
  EFI_STATUS                      Status = EFI_UNSUPPORTED;
  EFI_STRING_ID                   VtxCapabilityText;
  EFI_STRING_ID                   VtxCapabilityString;
  CPUID_VERSION_INFO_ECX          Ecx;

  Ecx.Uint32 = VersionInfoEcx.Uint32;

  if (Ecx.Bits.VMX == SUPPORTED) {
    //
    // VT-x is supported.
    //
    UnicodeSPrint (StringBuffer, 0x100, L"Supported");
  } else {
    //
    // VT-x isn't supported.
    //
    UnicodeSPrint (StringBuffer, 0x100, L"Un-Supported");
  }

  VtxCapabilityString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);
  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_VMX_TEXT),
    MainHiiHandle,
    &VtxCapabilityText
    );

  HiiCreateTextOpCode (OpCodeHandle, VtxCapabilityText, 0, VtxCapabilityString);

  return Status;
}

EFI_STATUS
GetCpuSteppingFunc (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS          Status;
  EFI_STRING_ID       CpuSteppingText;
  EFI_STRING_ID       CpuSteppingIdString;
  UINTN               ProcessorSignature;
  UINT32              Cpuid;
  CHAR16              *CpuSteppingString;

  Status            = EFI_UNSUPPORTED;
  CpuSteppingString = NULL;

  CopyMem (&ProcessorSignature, &(Record->ProcessorId.Signature), sizeof (EFI_PROCESSOR_SIGNATURE));
  Cpuid = ProcessorSignature & CPUID_FULL_FAMILY_MODEL_STEPPING;
  CpuSteppingString = HiiGetString (AdvanceHiiHandle, STRING_TOKEN (STR_PROCESSOR_STEPPING_VALUE), NULL);

  if (CpuSteppingString != NULL) {
    UnicodeSPrint (StringBuffer, 0x100, L"%02x (%s Stepping)", Cpuid, CpuSteppingString);
  } else {
    UnicodeSPrint (StringBuffer, 0x100, L"%02x (Unknown)", Cpuid);
  }

  CpuSteppingIdString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  Status = NewStringToHandle (
             AdvanceHiiHandle,
             STRING_TOKEN (STR_CPU_STEPPING_TEXT),
             MainHiiHandle,
             &CpuSteppingText
             );

  if (!EFI_ERROR(Status)) {
    if (NULL == HiiCreateTextOpCode (OpCodeHandle, CpuSteppingText, 0, CpuSteppingIdString)) {
      return EFI_BUFFER_TOO_SMALL;
    }
  }

  return Status;
}

//[-start-190709-16990078-modify]//
EFI_STATUS
GetGTInfo (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer,
  IN    SMBIOS_TABLE_TYPE4        *Record
  )
{
  EFI_STATUS         Status;
  EFI_STRING_ID      GTInfoText;
  EFI_STRING_ID      GTInfoString;

  Status             = EFI_UNSUPPORTED;

  Status = NewStringToHandle (
             AdvanceHiiHandle,
             STRING_TOKEN (STR_GT_INFO_TEXT),
             MainHiiHandle,
             &GTInfoText
             );

  Status = NewStringToHandle (
             AdvanceHiiHandle,
             STRING_TOKEN (STR_PROCESSOR_GT_VALUE),
             MainHiiHandle,
             &GTInfoString
             );

  if (!EFI_ERROR(Status)) {
    if (NULL == HiiCreateTextOpCode (OpCodeHandle, GTInfoText, 0, GTInfoString)) {
      return EFI_BUFFER_TOO_SMALL;
    }
  }

  return Status;
}
//[-end-190709-16990078-modify]//
