## @file
# 
#******************************************************************************
#* Copyright 2021 Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corp.
#*
#******************************************************************************
### @file
#  Component information file for AdvancedAcpiDxe module
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2013 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = AdvancedAcpiDxe
  FILE_GUID                      = C3E69EB2-0429-4BD6-AE4A-8CA02FBACC2E
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = InstallAcpiPlatform

[Sources.common]
  AcpiPlatform.h
  AcpiPlatform.c
  GenSsdtLib.c
  GenSsdtLib.h
  AcpiByteStream.h

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
#[-start-190620-IB11270237-add]#
  InsydeModulePkg/InsydeModulePkg.dec
#[-end-190620-IB11270237-add]#
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-start-190620-IB11270237-add]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec
#[-end-190620-IB11270237-add]#
  ClientOneSiliconPkg/SiPkg.dec
#[-start-190620-IB11270237-add]#
  PerformancePkg/PerformancePkg.dec
  $(PROJECT_PKG)/Project.dec
#[-start-190620-IB11270237-add]#
  AlderLakeBoardPkg/BoardPkg.dec
  IntelSiliconPkg/IntelSiliconPkg.dec

[LibraryClasses]
  UefiDriverEntryPoint
  BaseLib
  DebugLib
#[-start-190620-IB11270237-add]#
  UefiLib
#[-end-190620-IB11270237-add]#
  IoLib
  PcdLib
  EcMiscLib
  AslUpdateLib
  DxeAcpiGnvsInitLib
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  BaseMemoryLib
  MemoryAllocationLib
  S3BootScriptLib
  PchCycleDecodingLib
  PchInfoLib
  PchPcieRpLib
  PciSegmentLib
  HobLib
  SerialIoAccessLib
  GpioLib
  EspiLib
  CpuPlatformLib
  GbeLib
  IpuLib
  SataSocLib
  SataLib
  SpiAccessLib
  GraphicsInfoLib
  PchPciBdfLib
  GnaInfoLib
  VmdInfoLib
  TsnLib
  LocalApicLib
#[-start-190620-IB11270237-add]#
  AcpiPlatformLib
  BaseOemSvcChipsetLibDefault
  DxeOemSvcChipsetLibDefault
#[-end-190620-IB11270237-add]#

[Pcd]
  gSiPkgTokenSpaceGuid.PcdPciExpressRegionLength                    ## CONSUMES
#[-start-190620-IB11270237-add]#
  gPerformancePkgTokenSpaceGuid.PcdPerfPkgAcpiIoPortBaseAddress
#[-end-190620-IB11270237-add]#
  gBoardModuleTokenSpaceGuid.PcdRealBattery1Control                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdRealBattery2Control                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMipiCamSensor                       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdNCT6776FCOM                         ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdNCT6776FSIO                         ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdNCT6776FHWMON                       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdH8S2113SIO                          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdH8S2113UAR                          ## CONSUMES
  #gBoardModuleTokenSpaceGuid.PcdZPODD                              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdRGBCameraAdr                        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdDepthCameraAdr                      ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdSmcRuntimeSciPin                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdConvertableDockSupport              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF3Support                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF4Support                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF5Support                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF6Support                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF7Support                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF8Support                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdVirtualButtonVolumeUpSupport        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdVirtualButtonVolumeDownSupport      ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdVirtualButtonHomeButtonSupport      ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdVirtualButtonRotationLockSupport    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdSlateModeSwitchSupport              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdAcDcAutoSwitchSupport               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPmPowerButtonGpioPin                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdAcpiEnableAllButtonSupport          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdAcpiHidDriverButtonSupport          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTsOnDimmTemperature                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBatteryPresent                      ## CONSUMES
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemTableId           ## CONSUMES
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemRevision
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultCreatorId
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultCreatorRevision
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemId

  # DXE PCD
  gBoardModuleTokenSpaceGuid.PcdXhciAcpiTableSignature       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPreferredPmProfile           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdSpecificIoExpanderBus        ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdFingerPrintSleepGpio         ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFingerPrintIrqGpio           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdWwanModemBaseBandResetGpio   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBtRfKillGpio                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBtIrqGpio                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpadIrqGpio              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanelIrqGpio            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcLowPowerExitGpio           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcSmiGpio                    ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdUsbTypeCSupport              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCEcLess               ## CONSUMES

  # PD PS_ON GPIO
  gBoardModuleTokenSpaceGuid.PcdUsbCPsonOverrideN            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPsonS0ixEntryReq         ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPsonS0ixEntryAck         ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdDpMuxGpio                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardRtd3TableSignature ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdEnableVoltageMargining  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPlatformGeneration      ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardId                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardRev                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardType               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdCvfUsbPort              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPlatformType            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcPresent               ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdBoardUcmcGpio1          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardUcmcGpio2          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardUcmcGpio3          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardUcmcGpio4          ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdHdaI2sCodecIrqGpio
  gBoardModuleTokenSpaceGuid.PcdHdaI2sCodecI2cBusNumber


  # WWAN GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdWwanFullCardPowerOffGpio            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdWwanFullCardPowerOffGpioPolarity    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdWwanBbrstGpio                       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdWwanBbrstGpioPolarity               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdWwanPerstGpio                       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdWwanPerstGpioPolarity               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdWwanWakeGpio                        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdWwanSourceClock                     ## CONWUMES
  gBoardModuleTokenSpaceGuid.PcdWwanRootPortNumber                  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieWwanEnable                      ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardWwanTOn2ResDelayMs             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardWwanTOnRes2PerDelayMs          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardWwanTOnPer2PdsDelayMs          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardWwanTRes2OffDelayMs            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardWwanTOffDisDelayMs             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardWwanTResTogDelayMs             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardWwanTRes2PdsDelayMs            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardWwanTPer2ResDelayMs            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieDeviceOnWwanSlot                ## CONSUMES

  # PCIE Slot1 (x4 Connector) GPIO configuration  PCDs
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1WakeGpioPin             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1RootPort                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioPolarity     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioNo         ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioPolarity   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdDg1VramSRGpio                    ## CONSUMES

  # CPU PCIe x4 M.2 SSD RTD3
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2PwrEnableGpioNo       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2PwrEnableGpioPolarity ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioPolarity       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSsd3PwrEnableGpioNo       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSsd3RstGpioNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSsd3PwrEnableGpioPolarity ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSsd3RstGpioPolarity       ## CONSUMES

  # CPU PCIe x8 DG RTD3
  gBoardModuleTokenSpaceGuid.PcdPcieDG2PwrEnableGpioNo       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieDG2RstGpioNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieDG2PwrEnableGpioPolarity  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieDG2RstGpioPolarity       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieDG2WakeGpioPin           ## CONSUMES

  # PCIE SLOT 2 - X4 CONNECTOR GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2PwrEnableGpioNo        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2PwrEnableGpioPolarity  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2RstGpioNo              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2RstGpioPolarity        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot2WakeGpioPin               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot2RootPort                  ## CONSUMES

  # PCIE SLOT 3 - X2 CONNECTOR GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot3PwrEnableGpioNo        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot3PwrEnableGpioPolarity  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot3RstGpioNo              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot3RstGpioPolarity        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot3WakeGpioPin               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot3RootPort                  ## CONSUMES

  # PCH M.2 SSD GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdPchSsd1PwrEnableGpioNo            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSsd1PwrEnableGpioPolarity      ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSsd1RstGpioNo                  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSsd1RstGpioPolarity            ## CONSUMES

  # PCH M.2 SSD2 GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdPchSsd2PwrEnableGpioNo            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSsd2PwrEnableGpioPolarity      ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSsd2RstGpioNo                  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSsd2RstGpioPolarity            ## CONSUMES

  # PCH M.2 SSD3 GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdPchSsd3PwrEnableGpioNo            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSsd3PwrEnableGpioPolarity      ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSsd3RstGpioNo                  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSsd3RstGpioPolarity            ## CONSUMES

  # Onboard MR 1 GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1PowerEnableGpioNo        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1PowerEnableGpioPolarity  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1RstGpioNo                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1RstGpioPolarity          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1WakeGpioPin              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1RootPort                 ## CONSUMES

  # Onboard MR 2 GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr2PowerEnableGpioNo        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr2PowerEnableGpioPolarity  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr2RstGpioNo                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr2RstGpioPolarity          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr2WakeGpioPin              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr2RootPort                 ## CONSUMES

  # PCH SATA port GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdPchSataPortPwrEnableGpioNo       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSataPortPwrEnableGpioPolarity ## CONSUMES

  # CPU PEG slot1 GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdPegSlot1PwrEnableGpioNo          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPegSlot1PwrEnableGpioPolarity    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPegSlot1RstGpioNo                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPegSlot1RstGpioPolarity          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPegSlot1WakeGpioPin              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPegSlot1RootPort                 ## CONSUMES

  # CPU PEG slot2 GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdPegSlot2PwrEnableGpioNo          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPegSlot2PwrEnableGpioPolarity    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPegSlot2RstGpioNo                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPegSlot2RstGpioPolarity          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPegSlot2WakeGpioPin              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPegSlot2RootPort                 ## CONSUMES

  #Foxville GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdFoxLanWakeGpio                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFoxLanDisableNGpio               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFoxLanDisableNGpioPolarity       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFoxLanResetGpio                  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFoxLanResetGpioPolarity          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFoxlanRootPortNumber             ## CONSUMES

  # I2C Touch Panel 0 & 1 GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdTouchpanelPwrEnableGpio          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanelRstGpio                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanelPwrEnableGpioPolarity  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanelRstGpioPolarity        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpadIrqGpioPolarity          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanelIrqGpioPolarity        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpadIrqGpio                  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanelIrqGpio                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1PwrEnableGpio         ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1RstGpio               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1IrqGpio               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1PwrEnableGpioPolarity ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1RstGpioPolarity       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1IrqGpioPolarity       ## CONSUMES
  # WLAN GPIO configuration PCDs
  gBoardModuleTokenSpaceGuid.PcdWlanWakeGpio
  gBoardModuleTokenSpaceGuid.PcdWlanRootPortNumber

  # LID SWITCH
  gBoardModuleTokenSpaceGuid.PcdLidSwitchWakeGpio

  # ACPI configuration file data
  gBoardModuleTokenSpaceGuid.PcdBoardAcpiData         ## CONSUMES


  gBoardModuleTokenSpaceGuid.PcdClwlI2cController
  gBoardModuleTokenSpaceGuid.PcdClwlI2cSlaveAddress

  # eDP Display Mux GPIO
  gBoardModuleTokenSpaceGuid.PcdDisplayMuxGpioNo                 ## CONSUMES

#[-start-210913-IB05660178-add]#
  gChipsetPkgTokenSpaceGuid.PcdCrbSkuId
#[-end-210913-IB05660178-add]#

[FixedPcd]
  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress
  gSiPkgTokenSpaceGuid.PcdAcpiBaseAddress
#[-start-190620-IB11270237-add]#
#  gPlatformModuleTokenSpaceGuid.PcdS3AcpiReservedMemorySize         ## CONSUMES
#[-end-190620-IB11270237-add]#
  gSiPkgTokenSpaceGuid.PcdITbtEnable                          ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable                 ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdS3AcpiReservedMemorySize
  gSiPkgTokenSpaceGuid.PcdEmbeddedEnable
  gSiPkgTokenSpaceGuid.PcdAdlLpSupport                        ## CONSUMES
#[-start-201124-IB09480117-add]#
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuHoldRstActive                   ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuHoldRstGpioNo                   ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuPwrEnableActive                 ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuPwrEnableGpioNo                 ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuWakeGpioNo                      ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdHgPcieRootPortIndex                   ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdIntelDg1VramSrGpioNo                  ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdUseCrbHgDefaultSettings               ## CONSUMES
#[-end-201124-IB09480117-add]#
#[-start-210913-IB05660178-add]#
  gChipsetPkgTokenSpaceGuid.PcdHgPegBridgeDevice
  gChipsetPkgTokenSpaceGuid.PcdHgPegBridgeFunction
#[-end-210913-IB05660178-add]#

#[-start-190620-IB11270237-add]#
[FeaturePcd]
  gChipsetPkgTokenSpaceGuid.PcdEnableEconFlag
#[-end-190620-IB11270237-add]#
#[-start-201124-IB09480117-add]#
  gChipsetPkgTokenSpaceGuid.PcdHybridGraphicsSupported               ## CONSUMES
#[-end-201124-IB09480117-add]#
#[-start-210913-IB05660178-add]#
  gChipsetPkgTokenSpaceGuid.PcdNvidiaOptimusSupported
  gChipsetPkgTokenSpaceGuid.PcdAmdPowerXpressSupported
#[-end-210913-IB05660178-add]#

[Protocols]
  gEfiAcpiTableProtocolGuid                     ## CONSUMES
  gEfiFirmwareVolume2ProtocolGuid               ## CONSUMES
  gEfiPciRootBridgeIoProtocolGuid               ## CONSUMES
  gEfiMpServiceProtocolGuid                     ## CONSUMES
  gEfiPciIoProtocolGuid                         ## CONSUMES
  gIgdOpRegionProtocolGuid                      ## CONSUMES
  gPchSerialIoUartDebugInfoProtocolGuid         ## CONSUMES
  gEfiCpuIo2ProtocolGuid                        ## CONSUMES
  gEfiSimpleFileSystemProtocolGuid              ## CONSUMES
  gPlatformNvsAreaProtocolGuid                  ## PRODUCES
  gEdkiiVariableLockProtocolGuid                ## CONSUMES

[Guids]
  gAcpiTableStorageGuid                         ## CONSUMES
#[-start-190620-IB11270237-remove]#
#  gRcAcpiTableStorageGuid                       ## CONSUMES
#  gOcAcpiTableStorageGuid                       ## CONSUMES
#[-end-190620-IB11270237-remove]#
  gEfiGlobalVariableGuid                        ## CONSUMES
  gEfiHobListGuid                               ## CONSUMES
  gEfiFileInfoGuid                              ## CONSUMES
  gSetupVariableGuid                            ## SOMETIMES_PRODUCES
  gSaSetupVariableGuid                          ## CONSUMES
  gMeSetupVariableGuid                          ## CONSUMES
  gCpuSetupVariableGuid                         ## CONSUMES
  gPchSetupVariableGuid                         ## CONSUMES
  gMeBiosPayloadHobGuid                         ## CONSUMES
  gEfiEndOfDxeEventGroupGuid                    ## CONSUMES
  gS3MemoryVariableGuid                         ## CONSUMES
  gTcssHobGuid                                  ## CONSUMES
  gSocGpeSsdtAcpiTableStorageGuid               ## CONSUMES
  gSocCmnSsdtAcpiTableStorageGuid               ## CONSUMES
  gUsb4PlatformHobGuid                          ## CONSUMES
#[-start-190620-IB11270237-add]#
  gSystemConfigurationGuid
#[-end-190620-IB11270237-add]#

[Depex]
  gEfiAcpiTableProtocolGuid           AND
  gEfiFirmwareVolume2ProtocolGuid     AND
  gEfiCpuIo2ProtocolGuid              AND
  gEfiMpServiceProtocolGuid           AND
#[-start-190613-IB11270237-remove]#
#  gEfiPciRootBridgeIoProtocolGuid     AND
#[-end-190613-IB11270237-remove]#
  gEfiVariableArchProtocolGuid        AND
  gEfiSmbusHcProtocolGuid             AND
  gEfiVariableWriteArchProtocolGuid
