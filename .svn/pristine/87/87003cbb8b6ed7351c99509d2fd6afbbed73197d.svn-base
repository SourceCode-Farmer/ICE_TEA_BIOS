## @file
#  Component description file for the AlderLake SiPkg PEI libraries.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2016 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
#@par Specification Reference:
#
##

#
# Silicon Init Pei Library
#
!include $(PLATFORM_SI_PACKAGE)/UniversalPeiLib.dsc

#
# FRUs
#
!include $(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/PeiLib.dsc
!include $(PLATFORM_SI_PACKAGE)/Fru/AdlPch/PeiLib.dsc

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  PchInitLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/PeiPchInitLib/PeiPchInitLibTgl.inf
!else
  PchInitLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/PeiPchInitLib/PeiPchInitLibAdl.inf
!endif

 SiPolicyLib|$(PLATFORM_SI_PACKAGE)/Product/AlderLake/Library/PeiSiPolicyLib/PeiSiPolicyLib.inf
 SiConfigBlockLib|$(PLATFORM_SI_PACKAGE)/Library/BaseSiConfigBlockLib/BaseSiConfigBlockLib.inf
 SiFviInitLib|$(PLATFORM_SI_PACKAGE)/LibraryPrivate/PeiSiFviInitLib/PeiSiFviInitLib.inf
 StallPpiLib|$(PLATFORM_SI_PACKAGE)/Library/PeiInstallStallPpiLib/PeiStallPpiLib.inf
 SiPolicyOverrideLib|$(PLATFORM_SI_PACKAGE)/LibraryPrivate/PeiPolicyOverrideLib/PeiSiPolicyOverrideLib.inf
 PeiSiSsidLib|$(PLATFORM_SI_PACKAGE)/LibraryPrivate/PeiSiSsidLib/PeiSiSsidLib.inf
 SiMtrrLib|$(PLATFORM_SI_PACKAGE)/Library/SiMtrrLib/SiMtrrLib.inf
 CacheAsRamLib|$(PLATFORM_SI_PACKAGE)/Library/BaseCacheAsRamLib/BaseCacheAsRamLib.inf
 PeiFiaPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Sps/Library/PeiPchFiaPolicyNullLib/PeiFiaPolicyNullLib.inf
 PeiMeServerPreMemPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Sps/Library/PeiMeServerPreMemPolicyLib/PeiMeServerPreMemPolicyNullLib.inf

#
# Pch
#

 PchPolicyLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/PeiPchPolicyLib/PeiPchPolicyLib.inf
!if gSiPkgTokenSpaceGuid.PcdS3Enable == TRUE
 PchSmmControlLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/PeiPchSmmControlLib/PeiPchSmmControlLib.inf
!else
 PchSmmControlLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/PeiPchSmmControlLibNull/PeiPchSmmControlLibNull.inf
!endif
 SerialIoI2cMasterLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/I2c/Library/PeiSerialIoI2cMasterLib/PeiSerialIoI2cMasterLib.inf
 PeiHybridStorageLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Rst/LibraryPrivate/PeiHybridStorageLib/PeiHybridStorageLib.inf
 PeiHybridStoragePolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Rst/LibraryPrivate/PeiHybridStoragePolicyLib/PeiHybridStoragePolicyLib.inf
 PeiRstPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Rst/LibraryPrivate/PeiRstPrivateLib/PeiRstPrivateLib.inf
#
# Cpu
#
 CpuCommonLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiDxeSmmCpuCommonLib/PeiDxeSmmCpuCommonLib.inf
 CpuInitLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiCpuInitLib/PeiCpuInitLib.inf
!if gSiPkgTokenSpaceGuid.PcdBiosGuardEnable == TRUE
 BiosGuardLib|$(PLATFORM_SI_PACKAGE)/IpBlock/BiosGuard/LibraryPrivate/PeiBiosGuardLib/PeiBiosGuardLib.inf
!else
 BiosGuardLib|$(PLATFORM_SI_PACKAGE)/IpBlock/BiosGuard/LibraryPrivate/PeiBiosGuardLibNull/PeiBiosGuardLibNull.inf
!endif
 CpuPowerMgmtLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiCpuPowerMgmtLib/PeiCpuPowerMgmtLib.inf
!if gSiPkgTokenSpaceGuid.PcdTxtEnable == TRUE
 PeiTxtLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiTxtLib/PeiTxtLib.inf
!else
 PeiTxtLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiTxtLibNull/PeiTxtLibNull.inf
!endif
!if gSiPkgTokenSpaceGuid.PcdCpuPowerOnConfigEnable == TRUE
 CpuPowerOnConfigLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiCpuPowerOnConfigLib/PeiCpuPowerOnConfigLib.inf
!else
 CpuPowerOnConfigLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiCpuPowerOnConfigLibDisable/PeiCpuPowerOnConfigLibDisable.inf
!endif
!if gSiPkgTokenSpaceGuid.PcdSmbiosEnable == TRUE
 SmbiosCpuLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiSmbiosCpuLib/PeiSmbiosCpuLib.inf
!else
 SmbiosCpuLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiSmbiosCpuLibNull/PeiSmbiosCpuLibNull.inf
!endif

 SaInitLib|$(PLATFORM_SI_PACKAGE)/SystemAgent/LibraryPrivate/PeiSaInitLib/PeiSaInitLib.inf

 #
 # Host Bridge
 #
 PeiHostBridgeInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HostBridge/LibraryPrivate/PeiHostBridgeInitLib/PeiHostBridgeInitLib.inf
 PeiHostBridgePolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HostBridge/LibraryPrivate/PeiHostBridgePolicyLib/PeiHostBridgePolicyLibVer1.inf
 PeiHostBridgeInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/PeiHostBridgeInitFruLib/PeiHostBridgeInitFruLib.inf

!if gSiPkgTokenSpaceGuid.PcdSaDmiEnable == TRUE
 #
 # Dmi IpBlock
 #
  PeiCpuDmiInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuDmi/LibraryPrivate/PeiCpuDmiInitLibVer2/PeiCpuDmiInitLib.inf
!endif

!if gSiPkgTokenSpaceGuid.PcdITbtEnable == TRUE
  PeiTcssPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssPolicyLib/PeiTcssPolicyLib.inf
  PeiTcssInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssInitLib/PeiTcssInitLib.inf
  ItbtPcieRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiItbtPcieRpInitLib/ItbtPcieRpInitLib.inf
  TcssPmcLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssPmcLib/PeiTcssPmcLib.inf
!else
  PeiTcssPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssPolicyLibNull/PeiTcssPolicyLibNull.inf
  PeiTcssInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssInitLibNull/PeiTcssInitLibNull.inf
  ItbtPcieRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiItbtPcieRpInitLibNull/ItbtPcieRpInitLibNull.inf
  TcssPmcLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssPmcLibNull/PeiTcssPmcLibNull.inf
!endif
  PeiDpInPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiDpInPolicyLibNull/PeiDpInPolicyLibNull.inf

  PeiSaPolicyLib|$(PLATFORM_SI_PACKAGE)/SystemAgent/Library/PeiSaPolicyLib/PeiSaPolicyLib.inf
  PeiMemPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/MemoryInit/Adl/Library/PeiMemPolicyLib/PeiMemPolicyLib.inf


#
# TBT
#
  PeiITbtPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/Library/PeiITbtPolicyLib/PeiITbtPolicyLib.inf
  PeiTbtTaskDispatchLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/Library/PeiTbtTaskDispatchLib/PeiTbtTaskDispatchLib.inf
!if gSiPkgTokenSpaceGuid.PcdITbtEnable == TRUE
  PeiTbtTaskDispatchLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/Library/PeiTbtTaskDispatchLib/PeiTbtTaskDispatchLib.inf
  PeiITbtInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/LibraryPrivate/PeiITbtInitLib/PeiITbtInitLib.inf
!else
  PeiITbtInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/LibraryPrivate/PeiITbtInitLibNull/PeiITbtInitLibNull.inf
!endif

#
# Cpu
#
  CpuPolicyLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/PeiCpuPolicyLib/PeiCpuPolicyLib.inf


