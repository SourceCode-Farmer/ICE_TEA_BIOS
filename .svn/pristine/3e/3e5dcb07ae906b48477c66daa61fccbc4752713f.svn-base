/**@file
  Touch Host Controller ACPI

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#define THC0_ADR                           0x00100006
#define THC0_MODE                          TMD0
#define THC0_WAKE_INT                      TIN0
#define THC0_RST_PAD                       T010
#define THC0_RST_TRIGGER                   T020
#define THC0_CONNECTION_SPEED              T030
#define THC0_INPUT_REPORT_HEADER_ADDRESS   T040
#define THC0_INPUT_REPORT_BODY_ADDRESS     T050
#define THC0_OUTPUT_REPORT_ADDRESS         T060
#define THC0_READ_OPCODE                   T070
#define THC0_WRITE_OPCODE                  T080
#define THC0_FLAGS                         T090
#define THC0_ACTIVE_LTR                    T0A0
#define THC0_IDLE_LTR                      T0B0
#define THC0_LIMIT_PACKET_SIZE             T0C0
#define THC0_PERFORMANCE_LIMITATION        T0D0

#define THC1_ADR                           0x00100007
#define THC1_MODE                          TMD1
#define THC1_WAKE_INT                      TIN1
#define THC1_RST_PAD                       T011
#define THC1_RST_TRIGGER                   T021
#define THC1_CONNECTION_SPEED              T031
#define THC1_INPUT_REPORT_HEADER_ADDRESS   T041
#define THC1_INPUT_REPORT_BODY_ADDRESS     T051
#define THC1_OUTPUT_REPORT_ADDRESS         T061
#define THC1_READ_OPCODE                   T071
#define THC1_WRITE_OPCODE                  T081
#define THC1_FLAGS                         T091
#define THC1_ACTIVE_LTR                    T0A1
#define THC1_IDLE_LTR                      T0B1
#define THC1_LIMIT_PACKET_SIZE             T0C1
#define THC1_PERFORMANCE_LIMITATION        T0D1

Scope (\_SB.PC00) {
Include ("ThcCommon.asl")

#define THC_ADR                         THC0_ADR
#define THC_MODE                        THC0_MODE
#define THC_WAKE_INT                    THC0_WAKE_INT
#define THC_RST_PAD                     THC0_RST_PAD
#define THC_RST_TRIGGER                 THC0_RST_TRIGGER
#define THC_CONNECTION_SPEED            THC0_CONNECTION_SPEED
#define THC_LIMIT_PACKET_SIZE           THC0_LIMIT_PACKET_SIZE
#define THC_PERFORMANCE_LIMITATION      THC0_PERFORMANCE_LIMITATION
#define THC_INPUT_REPORT_HEADER_ADDRESS THC0_INPUT_REPORT_HEADER_ADDRESS
#define THC_INPUT_REPORT_BODY_ADDRESS   THC0_INPUT_REPORT_BODY_ADDRESS
#define THC_OUTPUT_REPORT_ADDRESS       THC0_OUTPUT_REPORT_ADDRESS
#define THC_READ_OPCODE                 THC0_READ_OPCODE
#define THC_WRITE_OPCODE                THC0_WRITE_OPCODE
#define THC_FLAGS                       THC0_FLAGS
#define THC_ACTIVE_LTR                  THC0_ACTIVE_LTR
#define THC_IDLE_LTR                    THC0_IDLE_LTR
Device (THC0) {
Include ("ThcController.asl")
}
#undef THC_ADR
#undef THC_MODE
#undef THC_WAKE_INT
#undef THC_RST_PAD
#undef THC_RST_TRIGGER
#undef THC_CONNECTION_SPEED
#undef THC_LIMIT_PACKET_SIZE
#undef THC_PERFORMANCE_LIMITATION
#undef THC_INPUT_REPORT_HEADER_ADDRESS
#undef THC_INPUT_REPORT_BODY_ADDRESS
#undef THC_OUTPUT_REPORT_ADDRESS
#undef THC_READ_OPCODE
#undef THC_WRITE_OPCODE
#undef THC_FLAGS
#undef THC_ACTIVE_LTR
#undef THC_IDLE_LTR

#define THC_ADR                         THC1_ADR
#define THC_MODE                        THC1_MODE
#define THC_WAKE_INT                    THC1_WAKE_INT
#define THC_RST_PAD                     THC1_RST_PAD
#define THC_RST_TRIGGER                 THC1_RST_TRIGGER
#define THC_CONNECTION_SPEED            THC1_CONNECTION_SPEED
#define THC_LIMIT_PACKET_SIZE           THC1_LIMIT_PACKET_SIZE
#define THC_PERFORMANCE_LIMITATION      THC1_PERFORMANCE_LIMITATION
#define THC_INPUT_REPORT_HEADER_ADDRESS THC1_INPUT_REPORT_HEADER_ADDRESS
#define THC_INPUT_REPORT_BODY_ADDRESS   THC1_INPUT_REPORT_BODY_ADDRESS
#define THC_OUTPUT_REPORT_ADDRESS       THC1_OUTPUT_REPORT_ADDRESS
#define THC_READ_OPCODE                 THC1_READ_OPCODE
#define THC_WRITE_OPCODE                THC1_WRITE_OPCODE
#define THC_FLAGS                       THC1_FLAGS
#define THC_ACTIVE_LTR                  THC1_ACTIVE_LTR
#define THC_IDLE_LTR                    THC1_IDLE_LTR
Device (THC1) {
Include ("ThcController.asl")
}
#undef THC_ADR
#undef THC_MODE
#undef THC_WAKE_INT
#undef THC_RST_PAD
#undef THC_RST_TRIGGER
#undef THC_CONNECTION_SPEED
#undef THC_LIMIT_PACKET_SIZE
#undef THC_PERFORMANCE_LIMITATION
#undef THC_INPUT_REPORT_HEADER_ADDRESS
#undef THC_INPUT_REPORT_BODY_ADDRESS
#undef THC_OUTPUT_REPORT_ADDRESS
#undef THC_READ_OPCODE
#undef THC_WRITE_OPCODE
#undef THC_FLAGS
#undef THC_ACTIVE_LTR
#undef THC_IDLE_LTR

} //End scope

