## @file
#  ADL P DQS configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR4 RVP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 0, 1 }, { 1, 0 }, { 0, 1 }, { 0, 1 },  // Controller 0
   { 0, 1 }, { 1, 0 }, { 1, 0 }, { 0, 1 }}  // Controller 1
})}
[PcdsDynamicExVpd.common.SkuIdAdlPLp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR5 RVP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 },  // Controller 0
   { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }}  // Controller 1
})}

#[-start-210519-KEBIN00001-modify]
gBoardModuleTokenSpaceGuid.YogaC9VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR5 RVP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 },  // Controller 0
   { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }}  // Controller 1
})}
#[-end-210519-KEBIN00001-modify]#

#[start-210720-STORM1100-modify]#
# 16"
gBoardModuleTokenSpaceGuid.YogaC716VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR5 RVP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 1, 0 }, { 0, 1 }, { 1, 0 }, { 0, 1 },  // Controller 0
   { 1, 0 }, { 0, 1 }, { 0, 1 }, { 1, 0 }}  // Controller 1
})}
#[end-210720-STORM1100-modify]#

#[start-210720-STORM1100-modify]#
# 14"
gBoardModuleTokenSpaceGuid.YogaC714VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR5 RVP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 0, 1 }, { 1, 0 }, { 0, 1 }, { 1, 0 },  // Controller 0
   { 0, 1 }, { 1, 0 }, { 0, 1 }, { 1, 0 }}  // Controller 1
})}
#[end-210720-STORM1100-modify]#

#[-start-210817-DABING0002-modify]#
#[-start-210831-TAMT000005-modify]#
gBoardModuleTokenSpaceGuid.S77014VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR5 RVP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 1, 0 }, { 0, 1 }, { 1, 0 }, { 0, 1 },  // Controller 0
   { 1, 0 }, { 0, 1 }, { 0, 1 }, { 1, 0 }}  // Controller 1
})}
#[-end-210831-TAMT000005-modify]#
#[-end-210817-DABING0002-modify]#

#[-start-210914-DABING0006-modify]#
#[-start-210918-DABING0008-modify]#
#[-start-210922-DABING0008-A-modify]#
gBoardModuleTokenSpaceGuid.S77013VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR5 RVP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 },  // Controller 0
   { 0, 1 }, { 0, 1 }, { 1, 0 }, { 0, 1 }}  // Controller 1
})}
#[-end-210922-DABING0008-A-modify]#
#[-end-210918-DABING0008-modify]#
#[-end-210914-DABING0006-modify]#

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Bep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR4 Type-4 BEP+ DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 1, 0 }, { 1, 0 }, { 0, 1 }, { 0, 1 },  // Controller 0
   { 0, 1 }, { 1, 0 }, { 1, 0 }, { 0, 1 }}  // Controller 1
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR4 Type-4 RVP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 0, 1 }, { 1, 0 }, { 0, 1 }, { 1, 0 },  // Controller 0
   { 1, 0 }, { 1, 0 }, { 1, 0 }, { 0, 1 }}  // Controller 1
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Dg128Aep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR5 AEP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 },  // Controller 0
   { 0, 1 }, { 0, 1 }, { 1, 0 }, { 0, 1 }}  // Controller 1
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Gcs]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR5 GCS DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 0, 1 }, { 1, 0 }, { 0, 1 }, { 1, 0 },  // Controller 0
   { 1, 0 }, { 1, 0 }, { 1, 0 }, { 0, 1 }}  // Controller 1
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5MbAep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR5 MB AEP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 0, 1 }, { 1, 0 }, { 0, 1 }, { 1, 0 },  // Controller 0
   { 1, 0 }, { 1, 0 }, { 1, 0 }, { 0, 1 }}  // Controller 1
})}

[PcdsDynamicExVpd.common.SkuIdAdlPT3Lp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram|*|{CODE({
//
// Adl-P LPDDR5 T3 RVP DQS byte swizzling between CPU and DRAM
//
  // Ch 0     1         2         3
  {{ 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 },  // Controller 0
   { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }}  // Controller 1
})}