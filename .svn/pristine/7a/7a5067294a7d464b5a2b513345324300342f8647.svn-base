## @file
#  Platform Configuration Switches
#
#******************************************************************************
#* Copyright (c) 2014 - 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  DEFINE  FIRMWARE_PERFORMANCE                    = NO
  DEFINE  H2O_PORT_80_DEBUG                       = YES
  DEFINE  EFI_PORT_80_DEBUG                       = NO
  DEFINE  INSYDE_DEBUGGER                         = NO
  DEFINE  EFI_DEBUG                               = NO
  DEFINE  USB_DEBUG_SUPPORT                       = NO
  DEFINE  EC_SHARED_FLASH_SUPPORT                 = NO
  DEFINE  CRISIS_RECOVERY_SUPPORT                 = YES
  DEFINE  USE_FAST_CRISIS_RECOVERY                = YES
  DEFINE  SYS_PASSWORD_IN_CMOS                    = YES
  DEFINE  SUPPORT_USER_PASSWORD                   = NO
  DEFINE  TEXT_MODE_FULL_SCREEN_SUPPORT           = NO
  DEFINE  SMM_INT10_ENABLE                        = NO
  DEFINE  MULTI_SUPER_IO_SUPPORT                  = NO
  DEFINE  MULTI_CONFIG_SUPPORT                    = NO
  DEFINE  DYNAMIC_HOTKEY_SUPPORT                  = NO

#
# Warning!!!
# Intel CRB H8 EC Support. OEM engineer should turn this off when you prepare the power on code base.
#
  DEFINE  USE_INTEL_CRB_H8_EC                     = YES
  DEFINE  ENABLE_ASL_ECON                         = YES
  DEFINE  EC_IDLE_PER_WRITE_BLOCK                 = NO
  DEFINE  FRONTPAGE_SUPPORT                       = YES
  DEFINE  BACKUP_SECURE_BOOT_SETTINGS_SUPPORT     = NO
  DEFINE  SUPPORT_64BITS_AML                      = YES
  DEFINE  Q2LSERVICE_SUPPORT                      = NO
  DEFINE  TXT_SUPPORT                             = NO
  DEFINE  MEMORY_SPD_PROTECTION                   = NO
  DEFINE  HYBRID_GRAPHICS_SUPPORT                 = NO
  DEFINE  NVIDIA_OPTIMUS_SUPPORT                  = NO
  DEFINE  AMD_POWERXPRESS_SUPPORT                 = NO
  DEFINE  HG_ASLCODE_FOR_WPT_LYNXPOINTLP          = YES  # SET NO When your platform need HG and use LynxPh
  DEFINE  SECURE_FLASH_SUPPORT                    = YES
  DEFINE  UNSIGNED_FV_SUPPORT                     = NO
  DEFINE  UEFI_PAUSE_KEY_FUNCTION_SUPPORT         = NO
  DEFINE  AMT_ENABLE                              = NO
  DEFINE  SNAPSCREEN_SUPPORT                      = NO

#
# If you use PCH UART2 and run com port DDT
# Please folllow below steps to modify code:
# 1. Change H2O_DDT_DEBUG_IO = Com
# 2. Change DEBUG_USE_PCH_COMPORT = YES
# 3. Please make sure ComDebugIoDxe\ComDebugIoDxe.efi is come from AlderLakeChipsetPkg\Override\InsydeModulePkg\H2ODebug
# 4. Modify the setting in AlderLakeChipsetPkg\Override\InsydeModulePkg\H2ODebug\DebugConfig.exe (Com port Type: MMIO8, Address: FE042000)
#
  DEFINE  H2O_DDT_DEBUG_IO                        = Xhc
  DEFINE  DEBUG_USE_PCH_COMPORT                   = NO  # SET YES When your platform use PCH UARTx to send message or DDT

#
# This complier switch is used for 1-chip platform if ULT_SUPPORT is set to YES.
# Note: This switch has not been testable when it is set to "YES" at current code base (Tag 08).
#
#
# Set "BOOT GUARD SUPPORT" to YES to enable BOOT GUARD BIOS related code
# Set "BOOT GUARD SUPPORT" to NO to disable BOOT GUARD BIOS related code
#
  DEFINE  BOOT_GUARD_SUPPORT                      = YES
  DEFINE  BOOTGUARD_FEATURE_ENABLE                = $(BOOT_GUARD_SUPPORT)

#
# Intel BIOS Guard Technology
#
# Note: SecurityFlash1.00.44.01 modify sign BIOS Guard (PFAT) image method,
#       When "BIOS_GUARD_SUPPORT" set YES, please set "SECURE_FLASH_SUPPORT" as YES too.
#       More sign Bios guard image details, please refer to SecurityFlash Tool ReleaseNotes.txt.
#
  DEFINE  BIOS_GUARD_SUPPORT                      = YES
  DEFINE  PTT_SUPPORT                             = YES
  DEFINE  OVERCLOCK_ENABLE                        = NO
  DEFINE  SOFTWARE_GUARD_ENABLE                   = YES

#
# When PRODUCTION_SIGNED_ACM is set to "YES", production-signed ACMs(BIOS Guard, Boot Guard and Intel TXT) will be used.
# It will be applied on production processors and chipsets.
#
  DEFINE  PRODUCTION_SIGNED_ACM                   = NO
  DEFINE  USE_H2O_IDE_GPIO_EDITOR                 = NO

#
# Project parameter for GenBvdt tool
#
# DEFINE GEN_BVDT_PROJECT_PARAMETER = -p $(WORKSPACE)/$(PROJECT_REL_PATH)/$(PROJECT_PKG)/ProjectProtectAreas.txt
  DEFINE GEN_BVDT_PROJECT_PARAMETER =

#
# FSP
#
  DEFINE  FSP_WRAPPER_SUPPORT                     = YES

#
# GOP
#
  DEFINE  VIDEO_UEFI_DRIVER_VERSION               = 17.0.1073

#
# Integrated Lan PXE ROM
#
  DEFINE  LAN_UEFI_DRIVER_VERSION                 = 26.2

#
# BIOS Guard ACM
#
  DEFINE  BIOS_GUARD_ACM_VERSION                  = 2.0.5021

#
# Power On
#
  DEFINE  POWER_ON_FLAG                           = NO


