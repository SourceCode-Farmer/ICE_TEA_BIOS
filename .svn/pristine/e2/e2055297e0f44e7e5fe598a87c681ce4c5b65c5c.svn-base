 /** @file
  This file contains Cpu Information for specific generation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _CPU_GEN_INFO_FRU_LIB_H_
#define _CPU_GEN_INFO_FRU_LIB_H_
#include <CpuRegs.h>
#include <CpuGenInfo.h>
#include <Register/CommonMsr.h>

///
/// Used to identify the CPU used for programming with the VR override table
///
typedef enum {
  EnumUnknownCpuId              = 0,
  EnumMinCpuId                  = 1,

  ///
  /// ADL S
  ///
  EnumAdlS35Watt881fCpuId       = 0x1,
  EnumAdlS65Watt881fCpuId       = 0x3,
  EnumAdlS125Watt881fCpuId      = 0x4,
  EnumAdlS150Watt881fCpuId      = 0x5,
  EnumAdlS35Watt841fCpuId       = 0x6,
  EnumAdlS65Watt841fCpuId       = 0x8,
  EnumAdlS125Watt841fCpuId      = 0x9,
  EnumAdlS35Watt641fCpuId       = 0xA,
  EnumAdlS65Watt641fCpuId       = 0xB,
  EnumAdlS125Watt641fCpuId      = 0xC,
  EnumAdlS35Watt681fCpuId       = 0xD,
  EnumAdlS35Watt801fCpuId       = 0xF,
  EnumAdlS65Watt801fCpuId       = 0x10,
  EnumAdlS35Watt401fCpuId       = 0x12,
  EnumAdlS65Watt401fCpuId       = 0x13,
  EnumAdlS35Watt601fCpuId       = 0x15,
  EnumAdlS65Watt601fCpuId       = 0x16,
  EnumAdlS35Watt201fCpuId       = 0x17,
  EnumAdlS46Watt201fCpuId       = 0x18,
  EnumAdlS65Watt201fCpuId       = 0x19,
  EnumAdlS58Watt401fCpuId       = 0x1A,
  EnumAdlS60Watt401fCpuId       = 0x1B,
  EnumAdlS63Watt401fCpuId       = 0x1C,
  EnumAdlS65WattBga881fCpuId    = 0x1D,
  EnumAdlS65WattBga841fCpuId    = 0x1E,
  EnumAdlS65WattBga441fCpuId    = 0x1F,
  EnumAdlS65WattBga601fCpuId    = 0x20,
  EnumAdlS65WattBga681fCpuId    = 0x21,
  EnumAdlS65WattBga401fCpuId    = 0x22,
  EnumAdlS55WattHBga881fCpuId   = 0x23,
  EnumAdlS55WattHBga681fCpuId   = 0x24,
  EnumAdlS55WattHBga481fCpuId   = 0x25,
  EnumAdlS55WattHBga441fCpuId   = 0x26,
  EnumAdlSMaxCpuId              = EnumAdlS55WattHBga441fCpuId,

  ///
  /// ADL P
  ///
  EnumAdlP15Watt282fCpuId       = 0x30,
  EnumAdlP28Watt482fCpuId       = 0x31,
  EnumAdlP28Watt682fCpuId       = 0x32,
  EnumAdlP45Watt682fCpuId       = 0x35,
  EnumAdlP15Watt142fCpuId       = 0x36,
  EnumAdlP15Watt242fCpuId       = 0x37,
  EnumAdlP45Watt482fCpuId       = 0x38,
  EnumAdlP45Watt442fCpuId       = 0x39,
  EnumAdlP28Watt442fCpuId       = 0x3A,
  EnumAdlP28Watt282fCpuId       = 0x3B,
  EnumAdlP28Watt242fCpuId       = 0x3C,
  EnumAdlP28Watt142fCpuId       = 0x3D,
  EnumAdlP45Watt242fCpuId       = 0x3E,
  EnumAdlP28Watt182fCpuId       = 0x3F,
  EnumAdlP28Watt662fCpuId       = 0x40,
  EnumAdlP28Watt642fCpuId       = 0x41,
  EnumAdlP45Watt642fCpuId       = 0x42,
  EnumAdlPMaxCpuId              = EnumAdlP45Watt642fCpuId,

  ///
  /// ADL M
  ///
  EnumAdlM7Watt182fPmicCpuId         = 0x50,
  EnumAdlM5Watt182fPmicCpuId         = 0x51,
  EnumAdlM5Watt142fPmicCpuId         = 0x52,
  EnumAdlM7Watt142fPmicCpuId         = 0x53,
  EnumAdlM7Watt242fHybDiscreteCpuId  = 0x54,
  EnumAdlM7Watt282fHybDiscreteCpuId  = 0x55,
  EnumAdlM7Watt182fHybDiscreteCpuId  = 0x56,
  EnumAdlM9Watt282fHybDiscreteCpuId  = 0x57,
  EnumAdlM9Watt142fHybDiscreteCpuId  = 0x58,
  EnumAdlM9Watt242fHybDiscreteCpuId  = 0x59,
  EnumAdlM12Watt142fHybDiscreteCpuId = 0x60,
  EnumAdlM12Watt242fHybDiscreteCpuId = 0x61,
  EnumAdlM12Watt182fHybDiscreteCpuId = 0x62,
  EnumAdlM12Watt282fHybDiscreteCpuId = 0x63,
  EnumAdlMMaxCpuId                  = 0x6F,



} CPU_IDENTIFIER;

/**
  Return CPU Identifier used to identify various CPU types

  @param[in]  CPU_SKU                       CpuSku
  @param[in]  CPU_STEPPING                  CpuStepping
  @param[in]  SelectedCtdpLevel             Ctdp Level Selected in BIOS

  @retval     CPU_IDENTIFIER                CPU Identifier
**/
CPU_IDENTIFIER
EFIAPI
GetCpuSkuIdentifier (
  IN  CPU_SKU         CpuSku,
  IN  CPU_STEPPING    CpuStepping,
  IN  UINT16          SelectedCtdpLevel
  );
#endif // _CPU_GEN_INFO_FRU_LIB_H_
