/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification
**/

form formid = AUTO_ID(AMT_FORM_ID),
  title     = STRING_TOKEN(STR_AMT_FORM);

  //
  // Invisible text to indicate entry into AMT Submenu and triggering form open action.
  //
  suppressif TRUE;
   text
         help  = STRING_TOKEN(STR_NONE),
         text  = STRING_TOKEN(STR_NONE),
         flags = INTERACTIVE,
         key   = AMT_FORM_ACTION_KEY;
  endif;

  oneof varid   = ME_SETUP.UsbProvision,
    prompt      = STRING_TOKEN(STR_AMT_USB_PROVISION_PROMPT),
    help        = STRING_TOKEN(STR_AMT_USB_PROVISION_HELP),
    option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
    option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = RESET_REQUIRED;
  endoneof;

  oneof varid   = ME_SETUP.MacPassThrough,
    prompt      = STRING_TOKEN(STR_AMT_MAC_PASS_THROUGH_PROMPT),
    help        = STRING_TOKEN(STR_AMT_MAC_PASS_THROUGH_HELP),
    option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
    option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = RESET_REQUIRED;
  endoneof;



  checkbox varid = ME_SETUP.AmtCiraRequest,
    prompt       = STRING_TOKEN(STR_AMT_CIRA_REQUEST_PROMPT),
    help         = STRING_TOKEN(STR_AMT_CIRA_REQUEST_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value. CHECKBOX_DEFAULT = ON, 0 = off
    flags        = 0 | RESET_REQUIRED,
    key          = 0,
  endcheckbox;

   checkbox varid = ME_SETUP.UnConfigureMe,
    prompt       = STRING_TOKEN(STR_MEBX_UNCONFIGURE_ME_2_PROMPT),
    help         = STRING_TOKEN(STR_MEBX_UNCONFIGURE_ME_2_HELP),
    flags        = 0 | RESET_REQUIRED,
    key          = 0,
  endcheckbox;


  goto ASF_CONFIGURATION_FORM_ID,
    prompt = STRING_TOKEN(STR_ASF_CONFIGURATION_FORM),
    help   = STRING_TOKEN(STR_ASF_CONFIGURATION_FORM_HELP);

  goto SECURE_ERASE_FORM_ID,
    prompt = STRING_TOKEN(STR_ASF_SECURE_ERASE_FORM),
    help   = STRING_TOKEN(STR_ASF_SECURE_ERASE_FORM_HELP);

#if FixedPcdGet8 (PcdOneClickRecoveryEnable) == 0x1
  goto ONE_CLICK_RECOVERY_FORM_ID,
    prompt = STRING_TOKEN(STR_ONE_CLICK_RECOVERY_FORM),
    help   = STRING_TOKEN(STR_ONE_CLICK_RECOVERY_FORM_HELP);
#endif

#if FixedPcdGet8 (PcdRemotePlatformEraseSupport) == 0x1
  goto REMOTE_PLATFORM_ERASE_FORM_ID,
    prompt = STRING_TOKEN(STR_REMOTE_PLATFORM_ERASE_FORM_TITLE),
    help   = STRING_TOKEN(STR_REMOTE_PLATFORM_ERASE_FORM_TITLE_HELP);
#endif

endform; // AMT_FORM_ID


form formid = AUTO_ID(ASF_CONFIGURATION_FORM_ID),
  title     = STRING_TOKEN(STR_ASF_CONFIGURATION_FORM);

      oneof varid   = ME_SETUP.FwProgress,
        prompt      = STRING_TOKEN(STR_ASF_PET_PROGRESS_PROMPT),
        help        = STRING_TOKEN(STR_ASF_PET_PROGRESS_HELP),
        option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = RESET_REQUIRED;
        option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
      endoneof;

      oneof varid   = ME_SETUP.WatchDogEnabled,
        prompt      = STRING_TOKEN(STR_ASF_WATCHDOG_PROMPT),
        help        = STRING_TOKEN(STR_ASF_WATCHDOG_HELP),
        option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
        option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = RESET_REQUIRED;
      endoneof;

    grayoutif ideqval ME_SETUP.WatchDogEnabled == 0;
      numeric varid = ME_SETUP.WatchDogTimerOs,
        prompt      = STRING_TOKEN (STR_ASF_WATCHDOG_TIMER_OS_PROMPT),
        help        = STRING_TOKEN (STR_ASF_WATCHDOG_TIMER_OS_HELP),
        minimum     = 0,
        maximum     = 65535,
        step        = 1,
        default     = 0,
      endnumeric;

      numeric varid = ME_SETUP.WatchDogTimerBios,
        prompt      = STRING_TOKEN (STR_ASF_WATCHDOG_TIMER_BIOS_PROMPT),
        help        = STRING_TOKEN (STR_ASF_WATCHDOG_TIMER_BIOS_HELP),
        minimum     = 0,
        maximum     = 65535,
        step        = 1,
        default     = 0,
      endnumeric;
    endif; // grayoutif ideqval ME_SETUP.WatchDogEnabled == 0;

    oneof varid   = ME_SETUP.AsfSensorsTable,
      prompt      = STRING_TOKEN(STR_ASF_SENSORS_TABLE_PROMPT),
      help        = STRING_TOKEN(STR_ASF_SENSORS_TABLE_HELP),
      option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
      option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = RESET_REQUIRED;
    endoneof;

endform; // ASF_CONFIGURATION_FORM_ID

form formid = SECURE_ERASE_FORM_ID,
  title = STRING_TOKEN(STR_ASF_SECURE_ERASE_FORM);

  oneof varid   = SETUP_DATA.SecureEraseModeRealMode,
    prompt      = STRING_TOKEN(STR_SECURE_ERASE_MODE_PROMPT),
    help        = STRING_TOKEN(STR_SECURE_ERASE_MODE_HELP),
    option text = STRING_TOKEN(STR_SECURE_ERASE_MODE_SIMULATED), value = 0, flags = DEFAULT;
    option text = STRING_TOKEN(STR_SECURE_ERASE_MODE_REAL),      value = 1, flags = 0;
  endoneof;

  oneof varid   = SETUP_DATA.ForceSecureErase,
    prompt      = STRING_TOKEN(STR_SECURE_ERASE_FORCE_PROMPT),
    help        = STRING_TOKEN(STR_SECURE_ERASE_FORCE_HELP),
    option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = DEFAULT | RESET_REQUIRED;
    option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = RESET_REQUIRED;
  endoneof;

endform;//SECURE_ERASE_FORM_ID


form formid = AUTO_ID(ONE_CLICK_RECOVERY_FORM_ID),
  title     = STRING_TOKEN(STR_ONE_CLICK_RECOVERY_FORM);

  suppressif ideqval SETUP_VOLATILE_DATA.OcrBootHttpsSupported == 0;
    oneof varid   = ME_SETUP.OcrBootHttps,
      prompt      = STRING_TOKEN(STR_OCR_BOOT_HTTPS_PROMPT),
      help        = STRING_TOKEN(STR_OCR_BOOT_HTTPS_HELP),
      option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = RESET_REQUIRED;
      option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
    endoneof;
  endif;

  suppressif ideqval SETUP_VOLATILE_DATA.OcrBootPbaSupported == 0;
    oneof varid   = ME_SETUP.OcrBootPba,
      prompt      = STRING_TOKEN(STR_OCR_BOOT_PBA_PROMPT),
      help        = STRING_TOKEN(STR_OCR_BOOT_PBA_HELP),
      option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = RESET_REQUIRED;
      option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
    endoneof;
  endif;

  suppressif ideqval SETUP_VOLATILE_DATA.OcrBootWinReSupported == 0;
    oneof varid   = ME_SETUP.OcrBootWinRe,
      prompt      = STRING_TOKEN(STR_OCR_BOOT_WIN_RE_PROMPT),
      help        = STRING_TOKEN(STR_OCR_BOOT_WIN_RE_HELP),
      option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = RESET_REQUIRED;
      option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
    endoneof;
  endif;

  suppressif ideqval SETUP_VOLATILE_DATA.OcrAmtDisSecBootSupported == 0;
    oneof varid   = ME_SETUP.OcrAmtDisSecBoot,
      prompt      = STRING_TOKEN(STR_OCR_AMT_DIS_SEC_BOOT_PROMPT),
      help        = STRING_TOKEN(STR_OCR_AMT_DIS_SEC_BOOT_HELP),
      option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = RESET_REQUIRED;
      option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
    endoneof;
  endif;

endform; // ONE_CLICK_RECOVERY_FORM_ID

//
// Define Remote Platform Erase Setup Form.
//
form formid = REMOTE_PLATFORM_ERASE_FORM_ID,
title = STRING_TOKEN(STR_REMOTE_PLATFORM_ERASE_FORM_TITLE);

  oneof varid   = ME_SETUP.EnableRpe,
    prompt      = STRING_TOKEN(STR_ENABLE_REMOTE_PLATFORM_ERASE_PROMPT),
    help        = STRING_TOKEN(STR_ENABLE_REMOTE_PLATFORM_ERASE_HELP),
    option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = RESET_REQUIRED;
    option text = STRING_TOKEN(STR_ENABLED),  value = 1, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
  endoneof;

  grayoutif ideqval ME_SETUP.EnableRpe == 0;
    oneof varid   = ME_SETUP.RpeSsdEraseRealMode,
      prompt      = STRING_TOKEN(STR_RPE_SSD_ERASE_MODE_PROMPT),
      help        = STRING_TOKEN(STR_RPE_SSD_ERASE_MODE_PROMPT_HELP),
      option text = STRING_TOKEN(STR_RPE_SSD_ERASE_SIMULATED_MODE), value = 0, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
      option text = STRING_TOKEN(STR_RPE_SSD_ERASE_REAL_MODE), value = 1, flags = RESET_REQUIRED;
    endoneof;
  endif; // grayoutif

endform; // End of REMOTE_PLATFORM_ERASE_FORM_ID
