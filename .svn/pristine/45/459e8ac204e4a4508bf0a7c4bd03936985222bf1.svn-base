## @file
#
#******************************************************************************
#* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
#  Provides platform policy services used during a capsule update.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2019 - 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION     = 0x00010005
  BASE_NAME       = CapsuleUpdatePolicyLib
  MODULE_UNI_FILE = CapsuleUpdatePolicyLib.uni
  FILE_GUID       = 93FAE102-694E-45CD-A5EE-4294E18D8EA6
  MODULE_TYPE     = BASE
  VERSION_STRING  = 1.0
  LIBRARY_CLASS   = CapsuleUpdatePolicyLib

#
#  VALID_ARCHITECTURES           = IA32 X64 ARM AARCH64
#

[Sources]
  CapsuleUpdatePolicyLib.c

[Packages]
  MdePkg/MdePkg.dec
  FmpDevicePkg/FmpDevicePkg.dec
#[-start-201030-IB16810136-remove]#
#  ClientOneSiliconPkg/SiPkg.dec
#[-end-201030-IB16810136-remove]#
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-start-201030-IB16810136-remove]#
#  AlderLakeBoardPkg/BoardPkg.dec
#[-end-201030-IB16810136-remove]#
#[-start-201030-IB16810136-add]#
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#[-end-201030-IB16810136-add]#

[LibraryClasses]
  DebugLib
  PcdLib
  EcMiscLib

[Pcd]
#[-start-201030-IB16810136-remove]#
#  gPlatformModuleTokenSpaceGuid.PcdLowBatteryCheck                       ## CONSUMES
#[-end-201030-IB16810136-remove]#

[Guids]
  gSetupVariableGuid                                                     ## CONSUMES
