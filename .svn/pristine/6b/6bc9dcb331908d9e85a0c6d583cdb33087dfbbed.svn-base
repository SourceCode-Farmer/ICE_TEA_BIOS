## @file
# Component information file for PEI AlderLake Simics Board Init Pre-Mem Library
#
# Copyright (c) 2017 - 2018, Intel Corporation. All rights reserved.<BR>
#
# This program and the accompanying materials are licensed and made available under
# the terms and conditions of the BSD License which accompanies this distribution.
# The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
#
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#
##

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PeiAlderLakeSimicsMultiBoardInitPreMemLib
  FILE_GUID                      = EA05BD43-136F-45EE-BBBA-27D75817574F
  MODULE_TYPE                    = BASE
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = NULL
  CONSTRUCTOR                    = PeiAlderLakeSimicsMultiBoardInitPreMemLibConstructor

[LibraryClasses]
  BaseLib
  DebugLib
  BaseMemoryLib
  MemoryAllocationLib
  PcdLib
  SiliconInitLib
  MultiBoardInitSupportLib
  PeiLib
  EcMiscLib
  MmioInitLib

[Packages]
  MinPlatformPkg/MinPlatformPkg.dec
  AlderLakeOpenBoardPkg/OpenBoardPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  IntelFsp2Pkg/IntelFsp2Pkg.dec
  ClientOneSiliconPkg/SiPkg.dec

[Sources]
  PeiInitPreMemLib.c
  PeiMultiBoardInitPreMemLib.c
  PeiDetect.c

[Pcd]
  gBoardModuleTokenSpaceGuid.PcdLpcSioConfigDefaultPort

  # SA Misc Config
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData00
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData01
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData10
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData11
  gBoardModuleTokenSpaceGuid.PcdMrcSpdDataSize

  # SPD Address Table
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable0
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable1
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable2
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable3

  # USB 2.0 Port Over Current Pin
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort0
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort1
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort2
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort3
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort4
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort5
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort6
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort7
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort8
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort9
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort10
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort11
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort12
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort13

  gBoardModuleTokenSpaceGuid.PcdCpuRatio
  gBoardModuleTokenSpaceGuid.PcdBiosGuard

  gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamBase              ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamSize              ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdFspTemporaryRamSize           ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdFspReservedBufferSize         ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdStackBase
  gBoardModuleTokenSpaceGuid.PcdStackSize
  gBoardModuleTokenSpaceGuid.PcdNvsBufferPtr
  gBoardModuleTokenSpaceGuid.PcdCleanMemory

[Guids]
  gFspNonVolatileStorageHobGuid
  gEfiMemoryOverwriteControlDataGuid
