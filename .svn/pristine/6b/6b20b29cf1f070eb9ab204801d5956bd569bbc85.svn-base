## @file
#  AmtLockI2cConInDxe
#
#******************************************************************************
#* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = AmtLockI2cConInDxe
  FILE_GUID                      = 0702269D-380C-4873-BFEB-32F5BC44DD16
  MODULE_TYPE                    = UEFI_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = AmtLockI2cConInDxeEntryPoint
#
# The following information is for reference only and not required by the build tools.
#
# VALID_ARCHITECTURES = IA32 X64 EBC
#

[LibraryClasses]
  UefiDriverEntryPoint
  #DxeAmtLib
  PrintLib
  DevicePathLib
  DxeServicesTableLib
  MemoryAllocationLib
  BaseMemoryLib
  UefiRuntimeServicesTableLib
  UefiBootServicesTableLib
  UefiBootManagerLib
  PchInfoLib
  ConfigBlockLib
  DxeAsfLib

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
#[-start-200420-IB17800056-modify]#
  $(PLATFORMSAMPLE_PACKAGE)/PlatformPkg.dec
#[-end-200420-IB17800056-modify]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  #ClientSiliconPkg/ClientSiliconPkg.dec
  InsydeModulePkg/InsydeModulePkg.dec

[Pcd]
  gChipsetPkgTokenSpaceGuid.PcdTDSBlockConInEnable

[Sources]
  AmtLockI2cConInDxe.c

[Protocols]
  gAlertStandardFormatProtocolGuid
  gAmtWrapperProtocolGuid
  gEfiUsbCoreProtocolGuid
  gEfiUsbIoProtocolGuid
  gEfiI2cIoProtocolGuid
  gEfiIsaIoProtocolGuid
  gEfiDevicePathProtocolGuid                    # PROTOCOL TO_START
  gEfiLegacyBiosProtocolGuid
  gTDSBlockConInProtocolGuid

[Guids]
  gI2cHidDeviceInfoGuid

[Depex]
  gAmtWrapperProtocolGuid
