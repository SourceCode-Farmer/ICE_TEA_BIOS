/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/=#
#include "Standard.uni"

//_Start_L05_SCU_BIOS_VERSION_DEFINE_
// BIOS Name:
// xxCNmmWW or xxCNmmCL
//
// xx -> BIOS ID provided by Lenovo, should match the BIOS ID number
// mm -> version of the driver package
// WW -> for world wide products
// CL -> for China only products
//
//#string STR_MISC_BIOS_VERSION          #language en-US  "ADLM.05.44.02.0035"
#string STR_MISC_BIOS_VERSION          #language en-US  "xxCN35WW"
//_End_L05_SCU_BIOS_VERSION_DEFINE_

#string STR_MISC_SYSTEM_PRODUCT_NAME   #language en-US  "AlderLake"

//
// The STR_CCB_VERSION is for Insyde internal code sync only.
// DO NOT modify it.
//
#string STR_CCB_VERSION                #language en-US  "05.44.02"

//
// Please following the following format to change this string: MM/DD/YYYY
//
#string STR_MISC_BIOS_RELEASE_DATE     #language en-US  "01/13/2022"

//
// System Firmware Version in the ESRT table
//
#string STR_ESRT_VERSION                   #language en-US  "71024035"
#string STR_ESRT_LOWEST_SUPPORTED_VERSION  #language en-US  "52430000"
//
// System Firmware GUID, each project must have a distinct System Firmware GUID from others
//
// ESRT FIRMWARE GUID of CRBs will all be 12B99262-648C-4365-BAFD-EB869FB7EB47
// WARNING!! OEM project need to use different GUID.
//_Start_L05_ESRT_FIRMWARE_GUID_DEFINE_
//#string STR_ESRT_FIRMWARE_GUID         #language en-US  "12B99262-648C-4365-BAFD-EB869FB7EB47"
#string STR_ESRT_FIRMWARE_GUID         #language en-US  "BE248459-CAF2-4B09-AF02-69B6A49974C1"
//_End_L05_ESRT_FIRMWARE_GUID_DEFINE_

#include "Standard.uni"
// String that is used by XXXOemSvcChipsetLib put on here
// Copy this file to XXXBoardpkg and add modified POST string on here
// #string STR_OEM_BADGING_STR_ESC         #language en-US  "Press Esc for boot options"

//
//  This ME Version setting is for H2OABT Tool test. It won't affect build code process.
//
#string STR_MISC_ME_VERSION                 #language en-US  "ME_Consumer_16.0.0.1341v1.2"
//_Start_L05_ABT_TOOL_DEFINE_DRIVER_NAME_
//#string STR_DEVIDE_PACKAGE_VERSION          #language en-US  "Alder Lake-P 6+84+82+8 Corp and Cons (BKC) (OCT2021) WW23-2021"
#string STR_DEVIDE_PACKAGE_VERSION          #language en-US  "Alder Lake-P 6+84+82+8 Corp and Cons (BKC) (OCT2021) WW23-2021 Lenovo"
//_End_L05_ABT_TOOL_DEFINE_DRIVER_NAME_
//
// BDS Hot Key Description Strings
//
#string STR_OEM_BADGING_STR_ESC           #language en-US  "Press Esc for boot options"
#string STR_OEM_BADGING_STR_ESC_SELECT    #language en-US  "Esc is pressed. Go to boot options."

#string STR_REMOTE_ASSISTANCE_KEY         #language en-US  "F9"
#string STR_REMOTE_ASSISTANCE_BEFORE_STR  #language en-US  "Press F9 Go to ME Remote Assistance."
#string STR_REMOTE_ASSISTANCE_AFTER_STR   #language en-US  "F9 is pressed. Go to ME Remote Assistance."

#string STR_MEBX_KEY                      #language en-US  "F10"
#string STR_MEBX_BEFORE_STR               #language en-US  "Press F10 go to MEBx"
#string STR_MEBX_AFTER_STR                #language en-US  "F10 is pressed. Go to MEBx."

//_Start_L05_SCU_LANGUAGE_DEFINE_
//
// Include L05Languages.uni here to avoid L05 #langdef being overwritten by Standard.uni
//
#include "InsydeL05ModulePkg/Include/Message/L05Languages.uni"
//_End__L05_SCU_LANGUAGE_DEFINE_