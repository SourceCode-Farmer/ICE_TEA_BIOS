## @file
#
#******************************************************************************
#* Copyright (c) 2018 - 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##
### @file
# Module Information file for the PolicyInit Advanced DXE driver.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2013 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = PolicyInitAdvancedDxe
  FILE_GUID                      = 490D0119-4448-440D-8F5C-F58FB53EE057
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = DXE_DRIVER
  ENTRY_POINT                    = PolicyInitAdvancedDxeEntryPoint

[LibraryClasses]
  BaseLib
  BaseMemoryLib
  CpuPlatformLib
  DebugLib
  DxeServicesTableLib
  IoLib
  MemoryAllocationLib
  DxeMePolicyLib
  DxeSaPolicyLib
  DxeAmtPolicyLib
  DxePchPolicyLib
  DxeITbtPolicyLib
  DxeDTbtPolicyLib
  PcdLib
  DxePolicyBoardConfigLib
  DxePolicyUpdateLib
  PostCodeLib
  UefiBootServicesTableLib
  UefiDriverEntryPoint
  UefiLib
  UefiRuntimeServicesTableLib
  ConfigBlockLib
  DevicePathLib
#[-start-200420-IB17800056-remove]#
#  EcMiscLib
#[-end-200420-IB17800056-remove]#
  PreSiliconEnvDetectLib
  GraphicsInfoLib
#[-start-200420-IB17800056-modify]#
  DxeOemSvcChipsetLibDefault
  BaseOemSvcChipsetLibDefault
  DxeInsydeChipsetLib
  HobLib
#[-start-190722-IB17700055-add]#
  H2OLib
#[-end-190722-IB17700055-add]#
#[-end-200420-IB17800056-modify]#

[Packages]
  MdePkg/MdePkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  IntelSiliconPkg/IntelSiliconPkg.dec
  MdeModulePkg/MdeModulePkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
#[-start-200420-IB17800056-modify]#
  InsydeModulePkg/InsydeModulePkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  $(PROJECT_PKG)/Project.dec
#[-end-200420-IB17800056-modify]#

[Pcd]
  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress                     ## CONSUMES
  gEfiMdeModulePkgTokenSpaceGuid.PcdResetOnMemoryTypeInformationChange
  gSiPkgTokenSpaceGuid.PcdAmtEnable                                     ## CONSUMES
#[-start-200420-IB17800056-remove]# 
#  gBoardModuleTokenSpaceGuid.PcdIntelGopEnable
#[-end-200420-IB17800056-remove]#
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor
  gBoardModuleTokenSpaceGuid.PcdPlatformType
  gBoardModuleTokenSpaceGuid.PcdEcPresent
#[-start-200420-IB17800056-remove]#
#  gBoardModuleTokenSpaceGuid.PcdVbtMipiGuid
#  gIntelSiliconPkgTokenSpaceGuid.PcdIntelGraphicsVbtFileGuid
#[-end-200420-IB17800056-remove]#
  gPlatformModuleTokenSpaceGuid.PcdSmbiosOemTypeFirmwareVersionInfo     ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable
  gSiPkgTokenSpaceGuid.PcdITbtEnable                                    ## CONSUMES
#[-start-190606-IB16990032-add]#
  gChipsetPkgTokenSpaceGuid.PcdSaGlobalNvsDeviceId                      ## CONSUMES
#[-end-190606-IB16990032-add]#

[FixedPcd]

[Sources]
  AmtPolicyInitDxe.c
  MePolicyInitDxe.c
  PolicyInitAdvancedDxe.c
  SaPolicyInitDxe.c
  SiliconPolicyInitDxe.c
#[-start-200420-IB17800056-remove]#
#  GopPolicyInitDxe.c
#[-end-200420-IB17800056-remove]#
  PchPolicyInitDxe.c
  CpuPolicyInitDxe.c
  TbtPolicyInitDxe.c
  TbtPolicyInitDxe.h
#[-start-200103-IB16740000-modify]#
  SaNotifyEventDxe.c
#[-end-200103-IB16740000-modify]#

[Protocols]
  gEfiFirmwareVolume2ProtocolGuid               ## CONSUMES
  gDxeMePolicyGuid                              ## PRODUCES
  gDxeAmtPolicyGuid                             ## PRODUCES
  gSaPolicyProtocolGuid                         ## CONSUMES
  gPchPolicyProtocolGuid                        ## CONSUMES
  gDxeSiPolicyProtocolGuid                      ## PRODUCES
  gGopPolicyProtocolGuid                        ## PRODUCES
  gDxeCpuPolicyProtocolGuid                     ## PRODUCES
  gITbtPolicyProtocolGuid                       ## CONSUMES
#[-start-200420-IB17800056-modify]#
  gSaNvsAreaProtocolGuid
  gEfiSetupUtilityProtocolGuid
  gEfiVariableArchProtocolGuid
#[-end-200420-IB17800056-modify]#

[Guids]
#[-start-200420-IB17800056-remove]#
#  gSetupVariableGuid                            ## CONSUMES
#  gSaSetupVariableGuid                          ## CONSUMES
#[-end-200420-IB17800056-remove]#
  gPchSetupVariableGuid                         ## CONSUMES
  gCpuSmmGuid                                   ## CONSUMES
#[-start-200420-IB17800056-modify]#
  gSystemConfigurationGuid
  gSiMemoryS3DataGuid
#[-end-200420-IB17800056-modify]#

#[-start-200420-IB17800056-modify]#
[Depex]
  gEfiReadOnlyVariableProtocolGuid OR
  gEfiVariableArchProtocolGuid
#[-end-200420-IB17800056-modify]#
