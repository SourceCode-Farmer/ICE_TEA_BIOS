/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/**@file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2016 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

//[-start-200215-IB06462109-remove]//
//#include <SetupPrivate.h>
//[-end-200215-IB06462109-remove]//
#include <Library/PchInfoLib.h>
#include <ConfigBlock.h>
#include <Ppi/SiPolicy.h>
#include <TraceHubConfig.h>
//[-start-201005-IB10181010-add]//
#include <DebugTokenDataHob.h>
//[-end-201005-IB10181010-add]//
#include <Library/CpuPlatformLib.h>
//[-start-200215-IB06462109-add]//
#include <ChipsetSetupConfig.h>
#include <SetupUtility.h>

SA_SETUP                    mSaSetup;
PCH_SETUP                   mPchSetup;
//[-end-200215-IB06462109-add]//
/*
  Get Debug Token Data

  @param[out]  DebugToken       Pointer to Token data

  @retval      TRUE             Debug Token is existed
  @retval      FALSE            Debug Token is not existed
*/
BOOLEAN
GetDebugTokenData (
  OUT DEBUG_TOKEN_KNOB_DATA     *DebugToken
  )
{
  DEBUG_TOKEN_DATA_HOB             *DebugTokenHob;
  EFI_HOB_GUID_TYPE                *GuidHob;

  GuidHob = GetFirstGuidHob (&gDebugTokenDataHobGuid);
  if (GuidHob == NULL) {
    return FALSE;
  }
  DebugTokenHob = (DEBUG_TOKEN_DATA_HOB *) GET_GUID_HOB_DATA (GuidHob);
  DebugToken->Data = DebugTokenHob->DebugTokenData;
  DEBUG ((DEBUG_INFO, "DebugToken->Data = %x\n", DebugToken->Data));
  return TRUE;
}


/*
  Create and update DebugSetupVolatile Data
*/
VOID
InitDebugSetupVolatileData (
  VOID
  )
{
  EFI_STATUS                      Status;
  DEBUG_SETUP_VOLATILE_DATA       DebugSetupVolData;
  UINT32                          DebugSetupVolVarAttr;
  UINTN                           VariableSize;
  DEBUG_TOKEN_KNOB_DATA           DebugToken;
  BOOLEAN                         DebugTokenExisted;

  ZeroMem (&DebugSetupVolData, sizeof (DebugSetupVolData));
  DebugSetupVolVarAttr = EFI_VARIABLE_BOOTSERVICE_ACCESS;
  VariableSize = sizeof (DEBUG_SETUP_VOLATILE_DATA);
  Status = gRT->GetVariable (
                  L"DebugSetupVolatileData",
                  &gSetupVariableGuid,
                  &DebugSetupVolVarAttr,
                  &VariableSize,
                  &DebugSetupVolData
                  );
  if (Status == EFI_NOT_FOUND) {
    DebugTokenExisted = GetDebugTokenData (&DebugToken);
    if (DebugTokenExisted) {
      if (DebugToken.Fields.StreamingTraceSink != SetupMenuDefault) {
        DebugSetupVolData.StreamingTraceSink = 1;
      }
      if (DebugToken.Fields.JtagC10PgDis == TRUE) {
        DebugSetupVolData.JtagC10PgDis = 1;
      }
      if (DebugToken.Fields.UsbOverCurrentOvrd == TRUE) {
        DebugSetupVolData.UsbOverCurrentOvrd = 1;
      }
      if (DebugToken.Fields.TraceHubForceOn == TRUE) {
        DebugSetupVolData.TraceHubForceOn = 1;
      }
    }
    Status = gRT->SetVariable (
                    L"DebugSetupVolatileData",
                    &gSetupVariableGuid,
                    DebugSetupVolVarAttr,
                    sizeof (DebugSetupVolData),
                    &DebugSetupVolData
                    );
    ASSERT_EFI_ERROR (Status);
  }
}

/**
  Initialize Debug strings.

  @param[in] EFI_HII_HANDLE   HiiHandle
  @param[in] UINT16           Class
**/
VOID
InitDebugStrings (
  EFI_HII_HANDLE HiiHandle,
  UINT16         Class
  )
{
  SI_SETUP                    SiSetup;
  UINTN                       SiVarSize;
  UINT32                      SiVarAttr;
  UINTN                       SaVarSize;
  UINT32                      SaVarAttr;
  UINTN                       PchVarSize;
  UINT32                      PchVarAttr;
  UINTN                       CpuVarSize;
  UINT32                      CpuVarAttr;
  EFI_STATUS                  Status;
  DEBUG_TOKEN_KNOB_DATA       DebugToken;
  BOOLEAN                     DebugTokenExisted;

  DebugTokenExisted = GetDebugTokenData (&DebugToken);

  SiVarSize = sizeof (SI_SETUP);
  Status = gRT->GetVariable (L"SiSetup", &gSiSetupVariableGuid, &SiVarAttr, &SiVarSize, &SiSetup);
  if (EFI_ERROR (Status)) DEBUG ((DEBUG_ERROR, "Get SiSetup Fail. Status = %r\n", Status));

  PchVarSize = sizeof (PCH_SETUP);
  Status = gRT->GetVariable (L"PchSetup", &gPchSetupVariableGuid, &PchVarAttr, &PchVarSize, &mPchSetup);
  if (EFI_ERROR (Status)) DEBUG ((DEBUG_ERROR, "Get PchSetup Fail. Status = %r\n", Status));

  SaVarSize = sizeof (SA_SETUP);
  Status = gRT->GetVariable (L"SaSetup", &gSaSetupVariableGuid, &SaVarAttr, &SaVarSize, &mSaSetup);
  if (EFI_ERROR (Status)) DEBUG ((DEBUG_ERROR, "Get SaSetup Fail. Status = %r\n", Status));

  CpuVarSize = sizeof (CPU_SETUP);
  Status = gRT->GetVariable (L"CpuSetup", &gCpuSetupVariableGuid, &CpuVarAttr, &CpuVarSize, &mCpuSetup);
  if (EFI_ERROR (Status)) DEBUG ((DEBUG_ERROR, "Get CpuSetup Fail. Status = %r\n", Status));

  if (DebugTokenExisted) {
    InitString(
      HiiHandle, STRING_TOKEN(STR_DEBUG_TOKEN_PRESENCE_VALUE),
      L"Yes"
    );

    if (DebugToken.Fields.JtagC10PgDis == TRUE) {
      mCpuSetup.JtagC10PowerGateDisable = TRUE;
    }

    if (DebugToken.Fields.UsbOverCurrentOvrd == TRUE) {
      mPchSetup.PchEnableDbcObs = TRUE;
    }

    if (DebugToken.Fields.StreamingTraceSink != SetupMenuDefault) {
      switch (DebugToken.Fields.StreamingTraceSink) {
        case EnabledAll:
          SiSetup.PlatformDebugConsent = ProbeTypeDciOob;
          break;

        case EnabledLowPower:
          SiSetup.PlatformDebugConsent = ProbeType2WireDciOob;
          break;

        case ForceDisabled:
          SiSetup.PlatformDebugConsent = ProbeTypeDisabled;
          break;
      }
    }
  } else {
    InitString(
      HiiHandle, STRING_TOKEN(STR_DEBUG_TOKEN_PRESENCE_VALUE),
      L"No"
    );
  }

  //
  // Do NOT update options for PDC Manual
  //
  if (SiSetup.PlatformDebugConsent == ProbeTypeManual) {
    return;
  }

  if (Class == ADVANCED_FORM_SET_CLASS) {
    DEBUG ((DEBUG_INFO, "Init Debug defaults ()\n"));

    switch (SiSetup.PlatformDebugConsent) {
      case ProbeTypeDisabled:
        mPchSetup.DciEn                    = FALSE;
        mSaSetup.CpuTraceHubMode           = TraceHubModeDisabled;
        mSaSetup.CpuTraceHubMemReg0Size    = TraceBufferNone;
        mSaSetup.CpuTraceHubMemReg1Size    = TraceBufferNone;
        mPchSetup.PchTraceHubMode          = TraceHubModeDisabled;
        mPchSetup.PchTraceHubMemReg0Size   = TraceBufferNone;
        mPchSetup.PchTraceHubMemReg1Size   = TraceBufferNone;
        break;

      case ProbeType2WireDciOob:
        mPchSetup.DciEn                    = TRUE;
        mSaSetup.CpuTraceHubMode           = TraceHubModeDisabled;
        mSaSetup.CpuTraceHubMemReg0Size    = TraceBufferNone;
        mSaSetup.CpuTraceHubMemReg1Size    = TraceBufferNone;
        mPchSetup.PchTraceHubMode          = TraceHubModeDisabled;
        mPchSetup.PchTraceHubMemReg0Size   = TraceBufferNone;
        mPchSetup.PchTraceHubMemReg1Size   = TraceBufferNone;
        break;

      case ProbeTypeDciOob:
        mPchSetup.DciEn                    = TRUE;
        mSaSetup.CpuTraceHubMode           = TraceHubModeHostDebugger;
        mSaSetup.CpuTraceHubMemReg0Size    = TraceBuffer8M;
        mSaSetup.CpuTraceHubMemReg1Size    = TraceBuffer8M;
        mPchSetup.PchTraceHubMode          = TraceHubModeHostDebugger;
        mPchSetup.PchTraceHubMemReg0Size   = TraceBuffer8M;
        mPchSetup.PchTraceHubMemReg1Size   = TraceBuffer8M;
        break;

      default:
        break;
    }

    if (DebugTokenExisted && (DebugToken.Fields.TraceHubForceOn == TRUE)) {
      mPchSetup.PchTraceHubMode = TraceHubModeHostDebugger;
      mPchSetup.PchTraceHubMemReg0Size   = TraceBuffer8M;
      mPchSetup.PchTraceHubMemReg1Size   = TraceBuffer8M;
    }

    Status = gRT->SetVariable (L"SaSetup", &gSaSetupVariableGuid, SaVarAttr, SaVarSize, &mSaSetup);
    if (EFI_ERROR (Status)) DEBUG ((DEBUG_ERROR, "Set SaSetup Fail. Status = %r\n", Status));

    Status = gRT->SetVariable (L"PchSetup", &gPchSetupVariableGuid, PchVarAttr, PchVarSize, &mPchSetup);
    if (EFI_ERROR (Status)) DEBUG ((DEBUG_ERROR, "Set PchSetup Fail. Status = %r\n", Status));

    Status = gRT->SetVariable (L"CpuSetup", &gCpuSetupVariableGuid, CpuVarAttr, CpuVarSize, &mCpuSetup);
    if (EFI_ERROR (Status)) DEBUG ((DEBUG_ERROR, "Set CpuSetup Fail. Status = %r\n", Status));

    Status = gRT->SetVariable (L"SiSetup", &gSiSetupVariableGuid, SiVarAttr, SiVarSize, &SiSetup);
    if (EFI_ERROR (Status)) DEBUG ((DEBUG_ERROR, "Set SiSetup Fail. Status = %r\n", Status));
  }
}

/**
  Setup callback executed when user selects a Debug Enable from the BIOS UI.
  Changes visibility and settings of other options.

  @param[in] This            Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param[in] Action          Specifies the type of action taken by the browser.
  @param[in] KeyValue        A unique value which is sent to the original exporting driver
                             so that it can identify the type of data to expect.
  @param[in] Type            The type of value for the question.
  @param[in] Value           A pointer to the data being sent to the original exporting driver.
  @param[out] ActionRequest  On return, points to the action requested by the callback function.

  @retval EFI_SUCCESS        The callback successfully handled the action
  @retval EFI_UNSUPPORTED    The specified action is not supported by the callback
**/
EFI_STATUS
EFIAPI
DebugFormCallBackFunction(
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  SI_SETUP                SiSetup;
  UINTN                   SiVarSize;
  EFI_STRING              SiRequestString;
  PCH_SETUP               PchSetup;
  UINTN                   PchVarSize;
  EFI_STRING              PchRequestString;
  SA_SETUP                SaSetup;
  UINTN                   SaVarSize;
  EFI_STRING              SaRequestString;

  PchRequestString       = NULL;
  SaRequestString        = NULL;
  SiRequestString        = NULL;

//[-start-200215-IB06462109-add]//
  EFI_STATUS              Status;
  SI_SETUP                *MySiIfrNVData;
  SA_SETUP                *MySaIfrNVData;
  PCH_SETUP               *MyPchIfrNVData;
  BOOLEAN                 ActionStatus = FALSE;

  Status                 = EFI_SUCCESS;
//[-end-200215-IB06462109-add]//
  switch (Action) {
    case EFI_BROWSER_ACTION_CHANGED:
    case EFI_BROWSER_ACTION_CHANGING:
      break;

    default:
      return EFI_UNSUPPORTED;
      break;
  }
  DEBUG ((DEBUG_INFO, "DebugFormCallBackFunction()\n"));

  SiVarSize = sizeof (SI_SETUP);
//[-start-200215-IB06462109-modify]//
  MySiIfrNVData = (SI_SETUP *)gRcSUBrowser->SiSUBrowserData;
  CopyMem (&SiSetup, MySiIfrNVData, SiVarSize);
  ActionStatus = HiiGetBrowserData (&gSiSetupVariableGuid, L"SiSetup", SiVarSize, (UINT8 *) &SiSetup);
  if (ActionStatus == FALSE) {
    Status = EFI_ABORTED;
    ASSERT_EFI_ERROR (Status);
  } else {
    Status = EFI_SUCCESS;
  }
//[-end-200215-IB06462109-modify]//
  //
  // Do NOT update options for PDC Manual
  //
  if (SiSetup.PlatformDebugConsent == ProbeTypeManual) {
    return EFI_SUCCESS;
  }

  PchVarSize = sizeof (PCH_SETUP);
//[-start-200215-IB06462109-modify]//
  MyPchIfrNVData = (PCH_SETUP *)gRcSUBrowser->PchSUBrowserData;
  CopyMem (&PchSetup, MyPchIfrNVData, PchVarSize);
  ActionStatus = HiiGetBrowserData (&gPchSetupVariableGuid, L"PchSetup", PchVarSize, (UINT8 *) &PchSetup);
  if (ActionStatus == FALSE) {
    Status = EFI_ABORTED;
    ASSERT_EFI_ERROR(Status);
  } else {
    Status = EFI_SUCCESS;
  }
//[-end-200215-IB06462109-modify]//

  SaVarSize = sizeof (SA_SETUP);
//[-start-200215-IB06462109-modify]//
  MySaIfrNVData = (SA_SETUP *)gRcSUBrowser->SaSUBrowserData;
  CopyMem (&SaSetup, MySaIfrNVData, SaVarSize);
  ActionStatus = HiiGetBrowserData (&gSaSetupVariableGuid, L"SaSetup", SaVarSize, (UINT8 *) &SaSetup);
  if (ActionStatus == FALSE) {
    Status = EFI_ABORTED;
    ASSERT_EFI_ERROR(Status);
  } else {
    Status = EFI_SUCCESS;
  }
//[-end-200215-IB06462109-modify]//

  switch (KeyValue) {
    case PLATFORM_DEBUG_CONSENT_KEY:
      switch (SiSetup.PlatformDebugConsent) {
        case ProbeTypeDisabled:
          PchSetup.DciEn                    = FALSE;
          SaSetup.CpuTraceHubMode           = TraceHubModeDisabled;
          SaSetup.CpuTraceHubMemReg0Size    = TraceBufferNone;
          SaSetup.CpuTraceHubMemReg1Size    = TraceBufferNone;
          PchSetup.PchTraceHubMode          = TraceHubModeDisabled;
          PchSetup.PchTraceHubMemReg0Size   = TraceBufferNone;
          PchSetup.PchTraceHubMemReg1Size   = TraceBufferNone;
          break;

        case ProbeType2WireDciOob:
          PchSetup.DciEn                    = TRUE;
          SaSetup.CpuTraceHubMode           = TraceHubModeDisabled;
          SaSetup.CpuTraceHubMemReg0Size    = TraceBufferNone;
          SaSetup.CpuTraceHubMemReg1Size    = TraceBufferNone;
          PchSetup.PchTraceHubMode          = TraceHubModeDisabled;
          PchSetup.PchTraceHubMemReg0Size   = TraceBufferNone;
          PchSetup.PchTraceHubMemReg1Size   = TraceBufferNone;
          break;

        case ProbeTypeDciOob:
          PchSetup.DciEn                    = TRUE;
          SaSetup.CpuTraceHubMode           = TraceHubModeHostDebugger;
          SaSetup.CpuTraceHubMemReg0Size    = TraceBuffer8M;
          SaSetup.CpuTraceHubMemReg1Size    = TraceBuffer8M;
          PchSetup.PchTraceHubMode          = TraceHubModeHostDebugger;
          PchSetup.PchTraceHubMemReg0Size   = TraceBuffer8M;
          PchSetup.PchTraceHubMemReg1Size   = TraceBuffer8M;
          break;

        default:
          break;
      }
      break;

    default:
      break;
  }
  SiRequestString = HiiConstructRequestString (SiRequestString, OFFSET_OF (SI_SETUP, PlatformDebugConsent), sizeof (SiSetup.PlatformDebugConsent));
  if (SiRequestString != NULL) {
    if (!HiiSetBrowserData (&gSiSetupVariableGuid, L"SiSetup", SiVarSize, (UINT8 *) &SiSetup, SiRequestString)) {
      ASSERT (FALSE);
    }
    FreePool (SiRequestString);
  }
//[-start-200215-IB06462109-add]//
#if 0
//[-end-200215-IB06462109-add]//
  PchRequestString = HiiConstructRequestString (PchRequestString, OFFSET_OF (PCH_SETUP, DciEn), sizeof (PchSetup.DciEn));
  PchRequestString = HiiConstructRequestString (PchRequestString, OFFSET_OF (PCH_SETUP, PchTraceHubMode), sizeof (PchSetup.PchTraceHubMode));
  PchRequestString = HiiConstructRequestString (PchRequestString, OFFSET_OF (PCH_SETUP, PchTraceHubMemReg0Size), sizeof (PchSetup.PchTraceHubMemReg0Size));
  PchRequestString = HiiConstructRequestString (PchRequestString, OFFSET_OF (PCH_SETUP, PchTraceHubMemReg1Size), sizeof (PchSetup.PchTraceHubMemReg1Size));
  if (PchRequestString != NULL) {
    if (!HiiSetBrowserData (&gPchSetupVariableGuid, L"PchSetup", PchVarSize, (UINT8 *) &PchSetup, PchRequestString)) {
      ASSERT (FALSE);
    }
    FreePool (PchRequestString);
  }
  SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, CpuTraceHubMode), sizeof (SaSetup.CpuTraceHubMode));
  SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, CpuTraceHubMemReg0Size), sizeof (SaSetup.CpuTraceHubMemReg0Size));
  SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, CpuTraceHubMemReg1Size), sizeof (SaSetup.CpuTraceHubMemReg1Size));
  if (SaRequestString != NULL) {
    if (!HiiSetBrowserData (&gSaSetupVariableGuid, L"SaSetup", SaVarSize, (UINT8 *) &SaSetup, SaRequestString)) {
      ASSERT (FALSE);
    }
    FreePool (SaRequestString);
  }
//[-start-200215-IB06462109-add]//
#endif

  PchVarSize = sizeof (PCH_SETUP);
  CopyMem (MyPchIfrNVData, &PchSetup, PchVarSize);

  SaVarSize = sizeof (SA_SETUP);
  CopyMem (MySaIfrNVData, &SaSetup, SaVarSize);
//[-end-200215-IB06462109-add]//

  return EFI_SUCCESS;
}