;;******************************************************************************
;;* Copyright (c) 2014 - 2021, Insyde Software Corporation. All Rights Reserved.
;;*
;;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;;* transmit, broadcast, present, recite, release, license or otherwise exploit
;;* any part of this publication in any form, by any means, without the prior
;;* written permission of Insyde Software Corporation.
;;*
;;******************************************************************************
;;
;; Module Name:
;;
;;   FactoryDefaultConfig.ini
;;
;; Abstract:
;;
;;   The information file for FactoryCopy.bin
;;


[HeaderSetting]
  ;;
  ;; Here are two ways to provide NvStoreBlockSize, NvStoreBlockNum and FactoryDefaultSize information.
  ;; 1. Use NvStoreBlockSize, NvStoreBlockNum and NvStoreBlockNum definition in HeaderSetting section.
  ;; 2. Use FlashMapFile defintion in HeaderSetting section, and then utility will get these settings from FlashMap file.
  ;; If user wants to generate FactoryCopy.bin while building BIOS, the suggested method is 2.
  ;; If user wants use tool to generate FactoryCopy.bin (user doesn't have FlashMap file), the suggested method is 1.
  ;; Note: If user uses these two methods at the same time, tool will use FlashMap to get these information. (method 2).
  ;;
;  NvStoreBlockSize    = 0x1000
;  NvStoreBlockNum     = 0x34
;  FactoryDefaultSize  = 0x2000
  FlashMapFile        = FlashMap.h

  ;;
  ;; Define the default owner GUID.
  ;; If certification doesn't specify owner GUID, it will use default owner GUID.
  ;; If certification specifies its owner GUID, it will use its owner GUID.
  ;; Note: If user doesn't specify default owner GUID, the default owner GUID is 00000000-0000-0000-0000-000000000000
  ;;
;;_Start_L05_IdeaPad_Signature_Owner_GUID
;;  DefaultOwnerGUID    = 77fa9abd-0359-4d32-bd60-28f4e78f784b  ;; MS owner GUID
  DefaultOwnerGUID    =    E04FD794-033E-46a0-81D2-048E8DA1432E  ;; Ideapad owner GUID
;;_End_L05_IdeaPad_Signature_Owner_GUID
  ;;
  ;; TimeStamp statement to specify time stamp and this statement is optional.
  ;; If user doesn't write this statement, tool will use current time as time stamp.
  ;; The format of time stamp is AAAA/BB/CC DD:EE:FF.  Ex: 2012/12/01 15:33:20, 0000/00/00 00:00:00....
  ;; Fromat Definition :
  ;; AAAA : Year      BB   : Month      CC   : Day
  ;; DD   : Hour      EE   : Minute     FF   : Second
  ;;
  TimeStamp           = 0000/00/00 00:00:00

;;
;; Certification format in PlatformKey, KeyExchangeKey, AuthorizedSignatureDatabase and
;; ForbiddenSignatureDatabase section:
;; FileName         [OWNER_GUID = xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx]
;;
;; Support two different type file name:
;; 1. .cer file is a X509 format file.
;; 2. .auth file is a EFI_VARIABLE_AUTHENTICATION_2 format file which may contain multiple signature list.
;;
[PlatformKey]
;;_Start_L05_IdeaPad_Signature_Owner_GUID
;;  TestPk.cer              OWNER_GUID = 55555555-5555-5555-5555-555555555555
  ideapadpk.cer              OWNER_GUID = E04FD794-033E-46a0-81D2-048E8DA1432E
;;_End_L05_IdeaPad_Signature_Owner_GUID

[KeyExchangeKey]
;;_Start_L05_IdeaPad_Signature_Owner_GUID
  KEK_MSFTproductionKekCA.cer    OWNER_GUID = 77fa9abd-0359-4d32-bd60-28f4e78f784b
;;_End_L05_IdeaPad_Signature_Owner_GUID

[AuthorizedSignatureDatabase]
;;_Start_L05_IdeaPad_Signature_Owner_GUID
  db_MSFTproductionWindowsSigningCA2011.cer    OWNER_GUID = 77fa9abd-0359-4d32-bd60-28f4e78f784b
  db_MSFTproductionUEFIsigningCA.cer           OWNER_GUID = 77fa9abd-0359-4d32-bd60-28f4e78f784b
;;_End_L05_IdeaPad_Signature_Owner_GUID
;;  SecureFlash.cer
  SecureFlash.cer          OWNER_GUID = E04FD794-033E-46a0-81D2-048E8DA1432E
;;[-start-210613-BAIN000011-add];;
;;  Mocca.cer                OWNER_GUID = E04FD794-033E-46a0-81D2-048E8DA1432E
;;[-end-210613-BAIN000011-add];;
[ForbiddenSignatureDatabase]
  DbxUpdate.auth
  ;;
  ;; dbxupdate_x64.auth, dbxupdate_arm64.auth and dbxupdate_x86.auth are used to fix GRUB2 
  ;; security issue for individual architecture. User can add them to [ForbiddenSignatureDatabase] 
  ;; (for example: DXE is x64 so we need add dbxupdate_x64.auth to fix this issue.). However, 
  ;; adding it will cause older version  Linux (before 2021/04/29) cannot boot in secure boot 
  ;; enabled environment. Kernel default setting doesn't add it and project can decide to add it or 
  ;; not.
  ;;  
  
[CapsuleSignatureDatabase]
  SecureFlash.cer
;;[-start-210613-BAIN000011-modify];;
;;  TestRoot.cer
;;  Mocca.cer
;;[-end-210613-BAIN000011-modify];;
