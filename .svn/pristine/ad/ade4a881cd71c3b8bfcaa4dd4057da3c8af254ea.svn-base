/** @file
  The SATA controller SoC specific implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiRstPrivateLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PeiSataLib.h>
#include <Library/PchFiaLib.h>
#include <Library/PmcSocLib.h>
#include <Library/PsfLib.h>
#include <Library/PeiHsioLib.h>
#include <Library/HsioSocLib.h>
#include <Library/GpioPrivateLib.h>
#include <Library/GpioNativePads.h>
#include <Library/PchFiaLib.h>
#include <Library/FiaSocLib.h>
#include <Library/SataSocLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchPolicyLib.h>
#include <Library/PeiItssLib.h>
#include <Library/SataLib.h>
#include <Library/PeiRstPolicyLib.h>
#include <Library/GpioHelpersLib.h>
#include <SataSocIntegration.h>
#include "PchInitPei.h"

/**
  Checks if SATA controller has lanes connected

  @param[in]  SataController    Pointer to SATA Controller structure
  @param[in]  Port              Port number (0 based) to be examined

  @retval TRUE if SATA controller has lanes assigned, otherwise FALSE
**/
BOOLEAN
SocIsSataPortConnected (
  IN  SATA_CONTROLLER   *SataController,
  IN  UINT8             Port
  )
{
  BOOLEAN PhyConnected;

  if (SataController == NULL) {
    DEBUG ((DEBUG_ERROR, "%a: NULL pointer detected!\n", __FUNCTION__));
    return FALSE;
  }

  PhyConnected = PchFiaIsSataPortConnected (SataController->SataCtrlIndex, Port);

  DEBUG ((DEBUG_INFO, "%a: Port: %d PHY %a connected\n", __FUNCTION__, Port, (PhyConnected) ? "" : "NOT"));

  return PhyConnected;
}

/**
  Configures GPIO pins for SATA DevSlp

  @param[in]  SataController    Pointer to SATA Controller structure
  @param[in]  Port              Port number (0 based) for Dev Slp enable
  @param[in]  ResetType         GPIO reset type (see GPIO_RESET_CONFIG)
  @param[in]  DevSlpGpioPinMux  Pin Mux GPIO pad value.

  @retval EFI_STATUS      Status returned by worker function
**/
EFI_STATUS
SocSataDevSlpEnable (
  IN  SATA_CONTROLLER          *SataController,
  IN  UINT8                    Port,
  IN  UINT32                   ResetType,
  IN  GPIO_PAD                 DevSlpGpioPinMux
  )
{
  EFI_STATUS         Status;
  GPIO_NATIVE_PAD   GpioNativePad;

  if (SataController == NULL) {
    DEBUG ((DEBUG_ERROR, "%a: NULL pointer detected!\n", __FUNCTION__));
    return EFI_INVALID_PARAMETER;
  }

  //
  // Configure GPIO pins for SATA DevSlp
  //
  Status = GpioSetNativePadByFunction (GPIO_FUNCTION_SATA_DEVSLP (SataController->SataCtrlIndex, Port), DevSlpGpioPinMux);
  if (Status == EFI_SUCCESS && ! GpioOverrideLevel1Enabled ()) {
    GpioNativePad = GpioGetNativePadByFunctionAndPinMux (GPIO_FUNCTION_SATA_DEVSLP (SataController->SataCtrlIndex, Port), DevSlpGpioPinMux);
    GpioSetPadResetConfig (GpioNativePad, ResetType);
  }

  DEBUG ((DEBUG_INFO, "%a: Status: %r\n", __FUNCTION__, Status));

  return Status;
}

/**
  Configures GPIO pins for SATA Hot Plug

  @param[in]  SataController    Pointer to SATA Controller structure
  @param[in]  Port              Port number (0 based) for Hot Plug enable

  @retval EFI_STATUS      Status returned by worker function
**/
EFI_STATUS
SocSataHotPlugEnable (
  IN  SATA_CONTROLLER    *SataController,
  IN  UINT8              Port
  )
{
  EFI_STATUS Status;

  if (SataController == NULL) {
    DEBUG ((DEBUG_ERROR, "%a: NULL pointer detected!\n", __FUNCTION__));
    return EFI_INVALID_PARAMETER;
  }

  Status = GpioEnableSataGpPin (SataController->SataCtrlIndex, Port);

  DEBUG ((DEBUG_INFO, "%a: Status: %r\n", __FUNCTION__, Status));

  return Status;
}

/**
  Configures GPIO pins for SATA Serial GPIO

  @param[in]  SataController     Pointer to SATA Controller structure

  @retval EFI_STATUS      Status returned by worker function
**/
EFI_STATUS
SocSataSgpioEnable (
  IN  SATA_CONTROLLER    *SataController
  )
{
  return EFI_SUCCESS;
}

/**
  Configures GPIO pins for SATA LED

  @param[in]  SataController     Pointer to SATA Controller structure

  @retval EFI_STATUS      Status returned by worker function
**/
EFI_STATUS
SocSataLedEnable (
  IN  SATA_CONTROLLER    *SataController
  )
{
  EFI_STATUS Status;

  if (SataController == NULL) {
    DEBUG ((DEBUG_ERROR, "%a: NULL pointer detected!\n", __FUNCTION__));
    return EFI_INVALID_PARAMETER;
  }

  Status =  GpioEnableSataLed (SataController->SataCtrlIndex);

  DEBUG ((DEBUG_INFO, "%a: Status: %r\n", __FUNCTION__, Status));

  return Status;
}

/**
  Disables SATA controller in power controller and fabric

  @param[in]  SataController     Pointer to SATA Controller structure

**/
VOID
SocSataCtrlDisable (
  IN  SATA_CONTROLLER    *SataController
  )
{

  if (SataController == NULL) {
    DEBUG ((DEBUG_ERROR, "%a: NULL pointer detected!\n", __FUNCTION__));
    return;
  }

  PsfDisableSataDevice (SataController->SataCtrlIndex);
  PmcDisableSata (SataController->SataCtrlIndex);
}

/**
  Enables SATA test mode in PHY

  @param[in]  SataController     Pointer to SATA Controller structure

**/
VOID
SocSataTestModeEnable (
  IN  SATA_CONTROLLER    *SataController
  )
{
  HSIO_HANDLE                        HsioHandle;
  HSIO_PRIVATE_CONFIG                HsioPrivate;
  HSIO_LANE                          HsioLane;
  P2SB_CONTROLLER                    P2SbController;
  P2SB_SIDEBAND_REGISTER_ACCESS      SbAccess;
  UINT32                             Index;
  UINT8                              LaneNum;

  for (Index = 0; Index < MaxSataPortNum (SataController->SataCtrlIndex); Index++) {
    if (PchFiaGetSataLaneNum (SataController->SataCtrlIndex, Index, &LaneNum)) {
      PchHsioHandleInit (NULL, NULL, LaneNum, SataController->SataCtrlIndex, &HsioHandle, &HsioPrivate, &HsioLane, &P2SbController, &SbAccess);
      HsioSataTestModeEnable (&HsioLane);
    }
  }
}


/**
  The function programs HSIO Rx\Tx Eq policy registers for SATA lanes.

  @param[in]  SiPreMemPolicyPpi  The SI PreMem Policy PPI instance
**/
VOID
PchHsioRxTxEqPolicySataProg (
  IN SI_PREMEM_POLICY_PPI  *SiPreMemPolicyPpi
  )
{
  HSIO_HANDLE                        HsioHandle;
  HSIO_PRIVATE_CONFIG                HsioPrivate;
  HSIO_LANE                          HsioLane;
  P2SB_CONTROLLER                    P2SbController;
  P2SB_SIDEBAND_REGISTER_ACCESS      SbAccess;
  UINT8                              MaxSataPorts;
  UINT32                             SataPortIndex;
  UINT32                             SataCtrlIndex;
  UINT8                              LaneNum;

  for (SataCtrlIndex = 0; SataCtrlIndex < MaxSataControllerNum (); SataCtrlIndex++) {
    MaxSataPorts = MaxSataPortNum (SataCtrlIndex);
    for (SataPortIndex = 0; SataPortIndex < MaxSataPorts; SataPortIndex++) {
      if (PchFiaGetSataLaneNum (SataCtrlIndex, SataPortIndex, &LaneNum)) {
        PchHsioHandleInit (NULL, SiPreMemPolicyPpi, LaneNum, SataCtrlIndex, &HsioHandle, &HsioPrivate, &HsioLane, &P2SbController, &SbAccess);
        HsioRxTxEqPolicySataProg (&HsioHandle, SataPortIndex);
      }
    }
  }
}

/**
  Load default values to SATA structures
  - Initialize data structures to zero
  - Initialize function pointers to NULL
  - Initialize pointes for SATA_HANDLE substructures

  @param[in out]  Sata            Pointer to SATA handle structure
  @param[in out]  SataPrivate     Pointer to SATA privare configuration structure
  @param[in out]  SataController  Pointer to SATA controller identification structure
  @param[in out]  SataCallback    Pointer to SATA callback structure
**/
VOID
SataHandleLoadDefaults (
  IN OUT  SATA_HANDLE         *Sata,
  IN OUT  SATA_PRIVATE_CONFIG *SataPrivate,
  IN OUT  SATA_CONTROLLER     *SataController,
  IN OUT  SATA_CALLBACK       *SataCallback
  )
{
  ZeroMem (SataPrivate, sizeof (SATA_PRIVATE_CONFIG));
  Sata->PrivateConfig = SataPrivate;

  ZeroMem (SataCallback, sizeof (SATA_CALLBACK));
  Sata->Callback = SataCallback;

  ZeroMem (SataController, sizeof (SATA_CONTROLLER));
  Sata->Controller = SataController;

  Sata->SataConfig = NULL;
}

/**
  Initialize SATA structures
  This should be done according SATA IP integration in SoC

  @param[in]      SiPolicy        Pointer to Silicon Policy
  @param[in]      SataCtrlIndex   SATA controller index in SoC
  @param[in out]  Sata            Pointer to SATA handle structure
  @param[in out]  SataPrivate     Pointer to SATA privare configuration structure
  @param[in out]  SataController  Pointer to SATA controller identification structure
  @param[in out]  SataCallback    Pointer to SATA callback structure
**/
VOID
SataHandleInit (
  IN      SI_POLICY_PPI                  *SiPolicy,
  IN      PCH_UPSTREAM_COMPONENT_CONFIG  *PchConfig,
  IN      UINT32                         SataCtrlIndex,
  IN OUT  SATA_HANDLE                    *Sata,
  IN OUT  SATA_PRIVATE_CONFIG            *SataPrivate,
  IN OUT  SATA_CONTROLLER                *SataController,
  IN OUT  SATA_CALLBACK                  *SataCallback
  )
{
  EFI_STATUS            Status;
  PCH_PM_CONFIG         *PmConfig;

  //
  // Initialize: data with defaults, function pointers to NULL, substructures pointers
  //
  SataHandleLoadDefaults (Sata, SataPrivate, SataController, SataCallback);

  Status = GetConfigBlock ((VOID *) SiPolicy, &gPmConfigGuid, (VOID *) &PmConfig);
  ASSERT_EFI_ERROR (Status);

  //
  // Initialize SATA Private Configuration
  //
  SataPrivate->RemappingSupported           = FALSE;  //TODO: Update when PchInit library moved to proper SOC
  SataPrivate->StorageRemappingEnabled      = RstIsPcieStorageRemapEnabled (SiPolicy);
  SataPrivate->SataSramParityCheckDisable   = TRUE;
  SataPrivate->SirC4Programming             = FALSE;  //TODO: Update when PchInit library moved to proper SOC
  SataPrivate->SataPowerGatingSupported     = TRUE;
  SataPrivate->SataSkipPortClockDisable   = TRUE;  //TODO: Update when PchInit library moved to proper SOC
  if (IsPchH () && PmConfig->PsOnEnable) {
    SataPrivate->SataPsOnSupported          = TRUE;
  } else {
    SataPrivate->SataPsOnSupported          = FALSE;
  }
  SataPrivate->SataSupportedRstMode         = PchGetSupportedRstMode ();
  SataPrivate->SataOscClkFreq               = SataSosc100Mhz;
  SataPrivate->InterruptPin                 = ItssGetDevIntPin (SiPolicy, SataDevNumber (SataCtrlIndex), SataFuncNumber (SataCtrlIndex));

  SataPrivate->SataSnoopLatVal              = PchConfig->SnoopLtrReqUs;
  SataPrivate->SataSnoopLatScale            = Scale1024ns;

  //
  // Initialize SATA callback pointers
  //
  SataCallback->SataIsPhyConnected    = SocIsSataPortConnected;
  SataCallback->SataTestModeEnable    = SocSataTestModeEnable;
  SataCallback->SataDevSleepEnable    = SocSataDevSlpEnable;
  SataCallback->SataSgpioEnable       = NULL;   // SGPIO is not required for Client projects
  SataCallback->SataLedEnable         = SocSataLedEnable;
  SataCallback->SataHotPlugEnable     = SocSataHotPlugEnable;
  SataCallback->SataControllerDisable = SocSataCtrlDisable;

  //
  // Initialize SATA controller data
  //
  Status = SataGetController (SataCtrlIndex, SataController);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a: ERROR: Cannot get SATA Controller!\n", __FUNCTION__));
    ASSERT (FALSE);
  }

  //
  // Set pointer to SATA Config Block
  //
  Sata->SataConfig = GetPchSataConfig (SiPolicy, SataCtrlIndex);
  ASSERT (Sata->SataConfig != NULL);

  Sata->Mmio           = PcdGet32 (PcdSiliconInitTempMemBaseAddr);
}

/**
  Initializes SATA Controller

  @param[in] SiPolicy   The SI Policy PPI instance
  @param[in] PchConfig  PCH upstream die config
**/
VOID
PchSataInit (
  IN  SI_POLICY_PPI                  *SiPolicy,
  IN  PCH_UPSTREAM_COMPONENT_CONFIG  *PchConfig
  )
{
  SATA_HANDLE           Sata;
  SATA_PRIVATE_CONFIG   SataPrivate;
  SATA_CONTROLLER       SataController;
  SATA_CALLBACK         SataCallback;

  SataHandleInit (SiPolicy, PchConfig, SATA_1_CONTROLLER_INDEX, &Sata, &SataPrivate, &SataController, &SataCallback);
  ConfigureSata (&Sata);
}

/**
  Initializes SATA Controller after RST

  @param[in] SiPolicy   The SI Policy PPI instance
  @param[in] PchConfig  PCH upstream die config
**/
VOID
PchSataAfterRstInit (
  IN  SI_POLICY_PPI  *SiPolicy,
  IN  PCH_UPSTREAM_COMPONENT_CONFIG  *PchConfig
  )
{
  SATA_HANDLE           Sata;
  SATA_PRIVATE_CONFIG   SataPrivate;
  SATA_CONTROLLER       SataController;
  SATA_CALLBACK         SataCallback;

  SataHandleInit (SiPolicy, PchConfig, SATA_1_CONTROLLER_INDEX, &Sata, &SataPrivate, &SataController, &SataCallback);
  ConfigureSataAfterRst (&Sata);
}
