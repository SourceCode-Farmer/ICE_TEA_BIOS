/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <PlatformInfo.h>

/**
 To display VBIOS reversion:

 @param        OpCodeHandle
 @param [in]   MainHiiHandle
 @param [in]   AdvanceHiiHandle
 @param [in]   StringBuffer

 @retval EFI_SUCCESS            To display VBIOS OpRom ver. is successful.

**/
EFI_STATUS
GetVbiosVersion (
  IN   VOID                       *OpCodeHandle,
  IN     EFI_HII_HANDLE           MainHiiHandle,
  IN     EFI_HII_HANDLE           AdvanceHiiHandle,
  IN     CHAR16                   *StringBuffer
  )
{
  EFI_STATUS                   Status;
  EFI_STRING_ID                VbiosVersionText;
  EFI_STRING_ID                VbiosVersionString;
  EFI_IA32_REGISTER_SET        RegisterSet;
  EFI_LEGACY_BIOS_PROTOCOL     *LegacyBios;
  CHAR8*                       TempPtr;

  LegacyBios            = NULL;
  TempPtr               = NULL;

  Status = gBS->LocateProtocol (
                  &gEfiLegacyBiosProtocolGuid,
                  NULL,
                  (VOID **)&LegacyBios
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  ZeroMem (&RegisterSet, sizeof (EFI_IA32_REGISTER_SET));

  //
  // Reference : #442269, Intel Graphics Media Accelerator Video BIOS
  // Chpa 8.1, 5F01h - Get Video BIOS Information
  //
  RegisterSet.X.AX = GET_VIDEO_BIOS_INFORMATION;
  LegacyBios->Int86 (LegacyBios, LEGACY_BIOS_INT_10, &RegisterSet);

  //
  // Return Registers:
  // AX = Return Status (function not supported if AL != 5Fh):
  //    = 005Fh, Function supported and successful
  //    = 015Fh, Function supported but failed
  //
  if (RegisterSet.X.AX == 0x005F) {
    // 
    // EBX = 4 bytes Video BIOS Build Number ASCII string 
    //
    TempPtr          = (CHAR8 *)&RegisterSet.E.EBX;
    StringBuffer [0] = (CHAR16)TempPtr[3];
    StringBuffer [1] = (CHAR16)TempPtr[2];
    StringBuffer [2] = (CHAR16)TempPtr[1];
    StringBuffer [3] = (CHAR16)TempPtr[0];
    TempPtr          = NULL;

    //
    // Update VBIOS Version String
    //
    VbiosVersionString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL); 

  } else {
    //
    // Update Default VBIOS Version String
    //
    Status = NewStringToHandle (
               AdvanceHiiHandle,
               STRING_TOKEN (STR_VBIOS_VERSION_STRING),
               MainHiiHandle,
               &VbiosVersionString
               );
  }

  //
  // Update VBIOS Item String
  //
  Status = NewStringToHandle (
             AdvanceHiiHandle,
             STRING_TOKEN (STR_VBIOS_VERSION_TXT),
             MainHiiHandle,
             &VbiosVersionText
             );

  if (!EFI_ERROR(Status)) {
    HiiCreateTextOpCode (OpCodeHandle,VbiosVersionText, 0, VbiosVersionString );
  }

  return Status;
}
