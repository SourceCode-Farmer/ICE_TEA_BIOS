#!/bin/bash

## @file
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2016 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

if [ ! -d $WORKSPACE/RomImages ]; then
  mkdir $WORKSPACE/RomImages
fi

# Build BIOS with Production signed ACM
wine $WORKSPACE_PLATFORM/$PLATFORM_PACKAGE/InternalOnly/Tools/BootGuard/anc_bios_gen.exe \
  -b $BUILD_DIR/FV/CLIENTBIOS.fd -a $WORKSPACE_PLATFORM/$PLATFORM_BOARD_PACKAGE/Binaries/BootGuard/ACM/StartupAcmProd.bin \
  -o $BUILD_DIR/FV/Prod_CLIENTBIOS.fd

#
# Copy BIOS ROM files to destination folder with proper naming basing on build type.
#
ROM_NAME=${BIOS_PREFIX}_FSPWRAPPER_${BIOS_MAJOR_VERSION}_${BIOS_MIN_VERSION}_${BUILD_TYPE}.rom
SIM_ROM_NAME=${BIOS_PREFIX}${IFWI_VERSION}_FSPWRAPPER_${BIOS_MAJOR_VERSION}_${BIOS_MIN_VERSION}_${BUILD_TYPE}_Simics.bin
SIM_ROM_DIR=${TARGET_PLATFORM}Simics
if [ -f $WORKSPACE/$BUILD_DIR/FV/CLIENTBIOS.fd ]; then
  cp -f $WORKSPACE/$BUILD_DIR/FV/CLIENTBIOS.fd $WORKSPACE/RomImages/${ROM_NAME}
else
  echo "No /FV/CLIENTBIOS.fd!!."
  exit 1
fi
if [ -f $WORKSPACE/$BUILD_DIR/FV/Prod_CLIENTBIOS.fd ]; then
  cp -f $WORKSPACE/$BUILD_DIR/FV/Prod_CLIENTBIOS.fd $WORKSPACE/RomImages/Prod_${ROM_NAME}
else
  echo "No /FV/Prod_CLIENTBIOS.fd!!."
  exit 1
fi

echo "Simics image: $WORKSPACE/RomImages/$SIM_ROM_DIR/$SIM_ROM_NAME"
python $WORKSPACE_PLATFORM/$PLATFORM_PACKAGE/Tools/RomImage/IfwiPatcher/IfwiPatcher.py \
  --ifwi $IFWI_DIR/${IFWI_PREFIX}_IFWI.bin \
  --bios $WORKSPACE/$BUILD_DIR/FV/CLIENTBIOS.fd \
  --output $WORKSPACE/RomImages/$SIM_ROM_DIR/$SIM_ROM_NAME
if [ $? -ne 0 ]; then
  echo "Simics image generated failed !"
  exit $?
fi
echo "Simics image generated successfully !"
