## @file
#  AmtLockUsbConInDxe
#
#******************************************************************************
#* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = AmtLockUsbConInDxe
  FILE_GUID                      = 1722EFD4-B7F0-41E8-AD21-0DA8FD6297A3
  MODULE_TYPE                    = COMBINED_SMM_DXE
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = AmtLockUsbConInDxeEntryPoint
#
# The following information is for reference only and not required by the build tools.
#
# VALID_ARCHITECTURES = IA32 X64 EBC
#

[LibraryClasses]
  UefiDriverEntryPoint
  #DxeAmtLib
  PrintLib
  DevicePathLib
  DxeServicesTableLib
  MemoryAllocationLib
  BaseMemoryLib
  UefiRuntimeServicesTableLib
  UefiBootServicesTableLib
  UefiBootManagerLib
  PchInfoLib
  ConfigBlockLib
  DxeAsfLib

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
#[-start-200420-IB17800056-modify]#
  $(PLATFORMSAMPLE_PACKAGE)/PlatformPkg.dec
#[-end-200420-IB17800056-modify]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  #ClientSiliconPkg/ClientSiliconPkg.dec
  #IntelFrameworkModulePkg/IntelFrameworkModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec

[Pcd]
  gChipsetPkgTokenSpaceGuid.PcdTDSBlockConInEnable

[Sources]
  AmtLockUsbConInDxe.c

[Protocols]
  gAlertStandardFormatProtocolGuid
  gAmtWrapperProtocolGuid
  gEfiUsbCoreProtocolGuid
  gEfiUsbIoProtocolGuid
  gEfiI2cIoProtocolGuid
  gEfiIsaIoProtocolGuid
  gEfiDevicePathProtocolGuid                    # PROTOCOL TO_START
  gEfiLegacyBiosProtocolGuid
  gTDSBlockConInProtocolGuid

[Guids]
  gI2cHidDeviceInfoGuid

[Depex]
  gAmtWrapperProtocolGuid AND
  gEfiUsbCoreProtocolGuid
