/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/=#
#include "Standard.uni"
#include "Include/Message/L05Languages.uni"
#include "Include/Message/L05StartupInterruptMenuString.uni"

//
// InsydeModulePkg/Universal/UserInterface/BootManagerDxe/BootManagerStrings.uni
//
#string STR_HELP_FOOTER                   #language en-US  "↑ and ↓ to change option, ENTER to select an option"

//
// InsydeSetupPkg/Drivers/HiiLayoutPkgDxe/HotKeyStr.uni
//
#string STR_HOT_KEY_SELECT_MENU_HELP_STRING                         #language en-US  "Switch Menu Items"
                                                                    #language zh-CN  "\wide切换菜单选项\narrow"
                                                                    #language fr-FR  "Choisir Page"
                                                                    #language es-ES  "Seleccionar pantalla"
                                                                    #language zh-TW  "\wide選擇畫面\narrow"
                                                                    #language ja-JP  "\wideスクリーン選択\narrow"
                                                                    #language it-IT  "Seleziona Schermata"
                                                                    #language de-DE  "Bildschirm Auswahl"
                                                                    #language pt-PT  "Seleccionar tela"

#string STR_HOT_KEY_ENTER_HELP_STRING                               #language en-US  "Select > SubMenu"
                                                                    #language zh-CN  "\wide选择或进入子菜单\narrow"
                                                                    #language fr-FR  "Sélectionner>Sous-menu"
                                                                    #language es-ES  "Seleccionar > Submenú"
                                                                    #language zh-TW  "\wide選取\narrow > \wide子功能表\narrow"
                                                                    #language ja-JP  "\wide選択\narrow > \wideサブメニューを\narrow"
                                                                    #language it-IT  "Seleziona > SottoMenu"
                                                                    #language de-DE  "Untermenüauswahl"
                                                                    #language pt-PT  "Selecionar > Submenu"

#string STR_HOT_KEY_SHOW_HELP_HELP_STRING                           #language en-US  "Help"
                                                                    #language zh-CN  "\wide帮助信息\narrow"
#string STR_HOT_KEY_DISCARD_AND_EXIT_HELP_STRING                    #language en-US  "Exit"
                                                                    #language zh-CN  "\wide退出\narrow"
#string STR_HOT_KEY_SELECT_ITEM_HELP_STRING                         #language en-US  "Select Item"
                                                                    #language zh-CN  "\wide选择项目\narrow"
#string STR_HOT_KEY_MODIFY_VALUE_HELP_STRING                        #language en-US  "Change Values"
                                                                    #language zh-CN  "\wide调整数值\narrow"
#string STR_HOT_KEY_LOAD_DEFAULT_HELP_STRING                        #language en-US  "Setup Defaults"
                                                                    #language zh-CN  "\wide载入默认值\narrow"
#string STR_HOT_KEY_SAVE_AND_EXIT_HELP_STRING                       #language en-US  "Save and Exit"
                                                                    #language zh-CN  "\wide保存退出\narrow"

//
// InsydeModulePkg/Library/SetupUtilityLib/SetupUtilityLibString.uni
//
#string STR_BOOT_TITLE                                              #language en-US  "Boot"
                                                                    #language zh-CN  "\wide启动设置\narrow"
#string STR_BOOT_TYPE_STRING                                        #language en-US  "Boot Mode"
                                                                    #language zh-CN  "\wide启动方式\narrow"
#string STR_BOOT_TYPE_HELP                                          #language en-US  "[UEFI]\nFor any OS that needs pure UEFI.\n[Legacy Support]\nFor any OS that need legacy support."
                                                                    #language zh-CN  "[UEFI]\n\wide系统会从\narrowUEFI\wide启动项启动\narrow。\n[Legacy]\n\wide系统会从\narrowLegacy\wide启动项启动\narrow。"
#string STR_EFI_BOOT_TYPE_TEXT                                      #language en-US  "UEFI"
#string STR_DUAL_BOOT_TYPE_TEXT                                     #language en-US  "Legacy Support"
#string STR_BOOT_OPTIONS                                            #language en-US  "Legacy"
                                                                    #language zh-CN  "Legacy\wide启动顺序\narrow"
#string STR_EFI                                                     #language en-US  "EFI"
                                                                    #language zh-CN  "UEFI\wide启动顺序\narrow"

#string STR_SECURITY_TITLE                                          #language en-US  "Security"
                                                                    #language zh-CN  "\wide安全设置\narrow"
#string STR_SUPERVISOR_PASSWORD_PROMPT                              #language en-US  "Set Administrator Password"
                                                                    #language zh-CN  "\wide设定管理员密码\narrow"
#string STR_SUPERVISOR_PASSWORD_STRING                              #language en-US  "Administrator Password"
                                                                    #language zh-CN  "\wide管理员密码\narrow"
#string STR_PASSWORD_POWERON_STRING                                 #language en-US  "Power on Password"
                                                                    #language zh-CN  "\wide启动时输入开机密码\narrow"
#string STR_SUPERVISOR_PASSWORD_STRING2                             #language en-US  "Not Set"
                                                                    #language zh-CN  "\wide未设定\narrow"
#string STR_USER_PASSWORD_STRING                                    #language en-US  "User Password"
                                                                    #language zh-CN  "\wide用户密码\narrow"
#string STR_USER_PASSWORD_STRING2                                   #language en-US  "Not Set"
                                                                    #language zh-CN  "\wide未设定\narrow"
#string STR_CHECK_PASSWORD_STRING                                   #language en-US  "System Password:"
                                                                    #language zh-CN  "\wide管理员密码:\narrow"
#string STR_INSTALLED_TEXT                                          #language en-US  "Set"
                                                                    #language zh-CN  "\wide已设定\narrow"
#string STR_NOT_INSTALLED_TEXT                                      #language en-US  "Not Set"
                                                                    #language zh-CN  "\wide未设定\narrow"
#string STR_USER_PASSWORD_PROMPT                                    #language en-US  "Set User Password"
                                                                    #language zh-CN  "\wide设定用户密码\narrow"
#string STR_CLEAR_USER_PASSWORD_STRING                              #language en-US  "Clear User Password"
                                                                    #language zh-CN  "\wide清除用户密码\narrow"
#string STR_PASSWORD_POWERON_HELP                                   #language en-US  "[Enabled]\nSystem will prompt for password at POST.\n[Disabled]\nSystem will prompt for password to enter Setup Utility."
                                                                    #language zh-CN  "[\wide是\narrow]\n\wide系统从关机或休眠状态启动后提示用户输入开机密码\narrow。\n[\wide否\narrow]\n\wide系统进入联想配置工具时提示用户输入开机密码\narrow。"
#string STR_PXE_BOOT_TO_LAN_STRING                                  #language en-US  "PXE Boot to LAN"
                                                                    #language zh-CN  "PXE\wide启动\narrow"
#string STR_PXE_BOOT_TO_LAN_HELP                                    #language en-US  "Enable or disable PXE boot to LAN.\n\n[Enabled]\nPXE boot is enabled.\n[Disabled]\nPXE boot is disabled."
                                                                    #language zh-CN  "\wide打开\narrow \wide或\narrow \wide关闭\narrow \wide从\narrowPXE\wide启动\narrow。\n\n[\wide打开\narrow]\n\wide支持系统从\narrowPXE\wide启动\narrow。\n[\wide关闭\narrow]\n\wide关闭系统从\narrowPXE\wide启动\narrow。"
#string STR_MRC_FAST_BOOT_ENABLE_PROMPT                             #language en-US  "Fast Boot"
                                                                    #language zh-CN  "\wide快速启动\narrow"
#string STR_MRC_FAST_BOOT_ENABLE_PROMPT_HELP                        #language en-US  "Enable/Disable fast path thru the MRC"
                                                                    #language zh-CN  "\wide打开\narrow \wide或\narrow \wide关闭\narrow \wide快速启动\narrow。\n\n[\wide打开\narrow]\n\wide减少\narrowBIOS\wide初始化的时间\narrow，BIOS\wide只能设置为从内置存储设备启动\narrow。\wide只支持内置显示器和键盘\narrow，\wide进入到系统后无限制\narrow。\n[\wide关闭\narrow]\n\wide正常启动到系统\narrow。"
#string STR_USB_BOOT_STRING                                         #language en-US  "USB Boot"
                                                                    #language zh-CN  "USB\wide启动\narrow"
//[-start-210915-Dennis0002-modify]//
#string STR_USB_BOOT_HELP                                           #language en-US  "Enable or disable boot for USB device.\n\n[Enabled]\nUSB device boot is enabled.\n[Disabled]\nUSB device boot is disabled."
                                                                    #language zh-CN  "\wide打开\narrow \wide或\narrow \wide关闭\narrow \wide从\narrowUSB\wide启动\narrow。\n\n[\wide打开\narrow]\n\wide支持系统从\narrowUSB\wide启动\narrow。\n[\wide关闭\narrow]\n\wide关闭系统从\narrowUSB\wide启动\narrow。"
//[-end-210915-Dennis0002-modify]//
#string STR_EXIT_TITLE                                              #language en-US  "Exit"
                                                                    #language zh-CN  "\wide退出\narrow"
#string STR_EXIT_SAVING_CHANGES_STRING                              #language en-US  "Exit Saving Changes"
                                                                    #language zh-CN  "\wide保存并退出\narrow"
#string STR_EXIT_SAVING_CHANGES_HELP                                #language en-US  "Exit Setup and save your changes."
                                                                    #language zh-CN  "\wide保存设定后退出。\narrow"
#string STR_EXIT_DISCARDING_CHANGES_STRING                          #language en-US  "Exit Discarding Changes"
                                                                    #language zh-CN  "\wide不保存并退出\narrow"
#string STR_EXIT_DISCARDING_CHANGES_HELP                            #language en-US  "Exit Setup without saving changes."
                                                                    #language zh-CN  "\wide不保存设定并退出。\narrow"
#string STR_DISCARD_CHANGES_STRING                                  #language en-US  "Discard Changes"
                                                                    #language zh-CN  "\wide放弃修改\narrow"
#string STR_DISCARD_CHANGES_HELP                                    #language en-US  "Discard Changes made so far to any of the Setup options." 
                                                                    #language zh-CN  "\wide放弃所有设定的修改\narrow。"
#string STR_EXIT_SAVING_CHANGES_HELP                                #language en-US  "Exit Setup and save your changes."
#string STR_EXIT_DISCARDING_CHANGES_HELP                            #language en-US  "Exit Setup without saving changes."
#string STR_DISCARD_CHANGES_HELP                                    #language en-US  "Discard Changes made so far to any of the Setup options."

#string SCU_STR_YES_TEXT                                            #language en-US  "Yes"
                                                                    #language zh-CN  "\wide是\narrow"
#string SCU_STR_NO_TEXT                                             #language en-US  "No"
                                                                    #language zh-CN  "\wide否\narrow"

//
// InsydeL05PlatformPkg/InsydeSetupPkg/Drivers/H2OFormBrowserDxe/SetupBrowserStr.uni
//
#string SCU_TITLE_STRING                                            #language en-US  "Lenovo Setup Utility"
                                                                    #language zh-CN  "\wide联想配置工具\narrow"
#string STR_ENTER_OLD_PWD                                           #language en-US  "Enter Current Password"
                                                                    #language zh-CN  "\wide输入当前密码\narrow"
#string STR_ENTER_NEW_PWD                                           #language en-US  "Enter New Password"
                                                                    #language zh-CN  "\wide输入新密码\narrow"
#string STR_ENTER_NEW_PWD_AGAIN                                     #language en-US  "Confirm New Password"
                                                                    #language zh-CN  "\wide确认新密码\narrow"
#string STR_HELP_DIALOG_TITLE                                       #language en-US  "[General Help]"
                                                                    #language zh-CN  "[\wide帮助信息\narrow]"
#string PASSWORD_CHANGES_SAVED_STRING                               #language en-US  "Changes have been saved after press "Save and Exit""
                                                                    #language zh-CN  "\wide更改已保存\narrow，\wide按保存并退出\narrow"
#string SCU_STR_EXIT_DISCARDING_CHANGES_STRING                      #language en-US  "Exit Discarding Changes"
                                                                    #language zh-CN  "\wide不保存并退出\narrow"
#string MINI_STRING                                                 #language en-US  "Please enter enough characters" 
                                                                    #language zh-CN  "\wide请输入足够多的字符\narrow。"
#string CONFIRM_ERROR                                               #language en-US  "Passwords are not the same"
                                                                    #language zh-CN  "\wide两次输入的密码不一样\narrow。"
#string PASSWORD_INVALID                                            #language en-US  "Incorrect password"
                                                                    #language zh-CN  "\wide错误的密码\narrow"
#string SCU_STR_OK_TEXT                                             #language en-US  "Ok"
                                                                    #language zh-CN  "\wide确认\narrow"

//
// InsydeL05PlatformPkg/InsydeModulePkg/Universal/Security/HddPassword/Strings.uni
//
#string STR_HDD_TITLE_MSG                                           #language en-US  "Harddisk Security"
                                                                    #language zh-CN  "\wide硬盘安全设置\narrow"
#string STR_HDD_DIALOG_HELP_TITLE                                   #language en-US  "Please input password for "
                                                                    #language zh-CN  "\wide请输入密码到\narrow "

