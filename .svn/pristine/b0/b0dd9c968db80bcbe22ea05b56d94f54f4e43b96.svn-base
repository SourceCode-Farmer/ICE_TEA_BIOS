/** @file
  SpiAccessLib implementation for SPI Flash Type devices

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MmPciLib.h>
#include <Register/PchRegs.h>
#include <Library/SpiAccessLib.h>
#include <PostCode.h>
#include <Library/ChipsetSpiAccessInitLib.h>
#include <Library/FdSupportLib.h>
#include <Library/IrsiRegistrationLib.h>
#include <Library/PcdLib.h>
#include <Library/CacheMaintenanceLib.h>
#include <Register/SpiRegs.h>
#include <Register/FlashRegs.h>
#include <Protocol/Spi.h>
#include <Protocol/BiosGuard.h>
#include <ChipsetInit.h>
#include <Library/FdSupportLib.h>
//[-start-191225-IB16740000-add]// for PCI_DEVICE_NUMBER_PCH_LPC & PCI_FUNCTION_NUMBER_PCH_XHCI define
#include <PchBdfAssignment.h>
//[-end-191225-IB16740000-add]//
#define SECTOR_SIZE_4KB   0x1000      // Common 4kBytes sector size
#define BIOS_GUARD_SECTOR_SIZE   0x10000

//
// SFDP Table
//
#define SFDP_SIGNATURE                  0x50444653
#define SPI_READ_SFDP_OPCODE            0x5A
#define SPI_READ_SFDP_DUMMY_CYCLE_NUM   0

#pragma pack(1)
typedef struct
{
  UINT32  SfdpSignature;
  UINT8   SfdpMinorRevNum;
  UINT8   SfdpMajorRevNum;
  UINT8   ParameterHeaderNum;       // n: n+1 parameter header
  UINT8   Reserve0;
} SPI_SFDP_HEADER;

typedef struct
{
  UINT8  ID;
  UINT8  ParameterTableMinorRev;
  UINT8  ParameterTableMajorRev;
  UINT8  ParameterTableLen;         //in double word
  UINT8  ParameterTablePointer[3];
  UINT8  Reserve0;
} SPI_SFDP_PARAMETER_HEADER;

typedef struct
{
  //
  // 1th dword
  //
  UINT8  BlockEraseSizes : 2;
  UINT8  WriteGranularity : 1;
  UINT8  VolatitleStatusBit : 1;
  UINT8  WriteEnableOpcodeSelect : 1;
  UINT8  Reserve0 : 3;
  UINT8  EraseOpcode;
  UINT8  FastRead112Support : 1;   //112(1-1-2) (x-y-z): x: x pins used for the opcode
  UINT8  AddressBytes : 2;         //                    y: y pins used for the address
  UINT8  DTRSupport : 1;           //                    z: z pins used for the data
  UINT8  FastRead122Support : 1;
  UINT8  FastRead144Support : 1;
  UINT8  FastRead114Support : 1;
  UINT8  Reserve1 : 1;
  UINT8  Reserve2;
  //
  // 2nd dword
  //
  UINT32 FlashMemoryDensity;
  //
  // 3rd dword
  //
  UINT8  FastRead144DummyCycle : 5;
  UINT8  FastRead144ModeBit : 3;
  UINT8  FastRead144OpCode;
  UINT8  FastRead114DummyCycle : 5;
  UINT8  FastRead114ModeBit : 3;
  UINT8  FastRead114OpCode;
  //
  // 4th dword
  //
  UINT8  FastRead112DummyCycle : 5;
  UINT8  FastRead112ModeBit : 3;
  UINT8  FastRead112OpCode;
  UINT8  FastRead122DummyCycle : 5;
  UINT8  FastRead122ModeBit : 3;
  UINT8  FastRead122OpCode;
  //
  // 5th dword
  //
  UINT8  FastRead222Support : 1;
  UINT8  Reserve3 : 3;
  UINT8  FastRead444Support : 1;
  UINT8  Reserve4 : 3;
  UINT8  Reserve5[3];
  //
  // 6th dword
  //
  UINT8  Reserve6[2];
  UINT8  FastRead222DummyCycle : 5;
  UINT8  FastRead222ModeBit : 3;
  UINT8  FastRead222OpCode;
  //
  // 7th dword
  //
  UINT8  Reserve7[2];
  UINT8  FastRead444DummyCycle : 5;
  UINT8  FastRead444ModeBit : 3;
  UINT8  FastRead444OpCode;
  //
  // 8th dword
  //
  UINT8  SectorType1Size;
  UINT8  SectorType1EraseOpcode;
  UINT8  SectorType2Size;
  UINT8  SectorType2EraseOpcode;
  //
  // 9th dword
  //
  UINT8  SectorType3Size;
  UINT8  SectorType3EraseOpcode;
  UINT8  SectorType4Size;
  UINT8  SectorType4EraseOpcode;
} SPI_SFDP_JEDEC_PARAMETER;

typedef struct
{
  SPI_SFDP_HEADER                  Header;
  SPI_SFDP_PARAMETER_HEADER        JedecParameterHeader;
  SPI_SFDP_JEDEC_PARAMETER         JedecParameter;
} SPI_SFDP_TABLE;
#pragma pack()

UINTN                   mPchSpiBar0;
UINTN                   mFlashAreaBaseAddress;
UINTN                   mFlashPartBaseAddress;
UINTN                   mFlashAreaSize;
UINTN                   mFlashAreaOffset;
UINTN                   mFlashECRegionBase;
UINTN                   mFlashECRegionLimit;
UINT8                   mNumberOfComponents;
PCH_SPI_PROTOCOL        *mSpiProtocol;
BIOSGUARD_PROTOCOL      *mlibBiosGuardProtocol;
FLASH_REGION            mPlatformFlashTable[FlashRegionMax + 1];

UINTN ConvertFlashBase(
//[-start-190610-IB16990033-add]//
  VOID
//[-end-190610-IB16990033-add]//
  )
{
  return (GetFlashMode() == FW_FLASH_MODE) ? mFlashPartBaseAddress : mFlashAreaBaseAddress;
}
EFI_STATUS
UpdateDevInfoFromSfdp (
  IN OUT SPI_CONFIG_BLOCK        *SpiConfigBlock
  )
/*++

Routine Description:

  Check flash support SFDP or not.
  If support, update relative device information.

Arguments:

  SpiConfigBlock                 pointer to SPI_CONFIG_BLOCK of FLASH_SPI_DEVICE.

Returns:

  EFI_SUCCESS                    Support SFDP and update relative information.
  EFI_UNSUPPORTED                Not support SFDP.

--*/
{
  EFI_STATUS              Status;
  SPI_SFDP_TABLE          SfdpTable;
  UINT8                   SfdpParameterAddr;
  UINT32                  FlashDeviceSize;

  //
  // Read SFDP header and JEDEC parameter header.
  //
  ZeroMem (&SfdpTable, sizeof (SPI_SFDP_TABLE));
  Status = mSpiProtocol->FlashReadSfdp (
                           mSpiProtocol,
                           0,
                           0,
                           sizeof (SPI_SFDP_HEADER) + sizeof (SPI_SFDP_PARAMETER_HEADER),
                           (UINT8*)(&SfdpTable.Header)
                           );
  if (EFI_ERROR (Status) ||
      (SfdpTable.Header.SfdpSignature != SFDP_SIGNATURE) ||
      (SfdpTable.JedecParameterHeader.ParameterTableLen < 2)) {
    return Status;
  }

  //
  // Read JEDEC parameter tables.
  //
  SfdpParameterAddr = SfdpTable.JedecParameterHeader.ParameterTablePointer[0] |
                      SfdpTable.JedecParameterHeader.ParameterTablePointer[1] << 8 |
                      SfdpTable.JedecParameterHeader.ParameterTablePointer[2] << 16;
  Status = mSpiProtocol->FlashReadSfdp (
                           mSpiProtocol,
                           0,
                           SfdpParameterAddr,
                           MIN (SfdpTable.JedecParameterHeader.ParameterTableLen, 9) * 4,
                           (UINT8*)(&SfdpTable.JedecParameter)
                           );
  if (EFI_ERROR (Status)) {
     return Status;
  }

  //
  // We don't know protect info by SFDP, so clear software protect information
  //
  SpiConfigBlock->GlobalProtect = 0;
  SpiConfigBlock->BlockProtect  = 0;

  //
  // 1th dword jedec parameter
  //

  //
  // 4K erase opcode support
  //
  if (SfdpTable.JedecParameter.BlockEraseSizes == 1 &&
      SfdpTable.JedecParameter.EraseOpcode != 0xFF) {
    //
    //  64K block map
    //
    //SpiConfigBlock->BlockEraseSize = 0x1000;
    SpiConfigBlock->OpCodeMenu[1] = SfdpTable.JedecParameter.EraseOpcode;
  } else {
    return EFI_UNSUPPORTED;
  }

  if (SfdpTable.JedecParameter.WriteGranularity != 1) {
    SpiConfigBlock->ProgramGranularity = 0;
  } else {
    SpiConfigBlock->ProgramGranularity = 1;
  }

  if (SfdpTable.JedecParameter.VolatitleStatusBit != 1) {
    SpiConfigBlock->NVStatusBit = 1;
  } else {
    SpiConfigBlock->NVStatusBit = 0;
  }

  //
  // 2nd dword jedec parameter
  //
  if ((SfdpTable.JedecParameter.FlashMemoryDensity & BIT31) == BIT31) {
    FlashDeviceSize = 1 << (SfdpTable.JedecParameter.FlashMemoryDensity & 0x7FFFFFFF) / 8;
  } else {
    FlashDeviceSize = (SfdpTable.JedecParameter.FlashMemoryDensity + 1) / 8;
  }
  SpiConfigBlock->DeviceSize = FlashDeviceSize;

  return EFI_SUCCESS;
}
/**
  Detect and Initialize SPI flash part OpCode and other parameter through PCH

  @param FlashDevice            pointer to FLASH_DEVICE structure

  @retval EFI_SUCCESS           The SPI device was successfully recognized
  @retval EFI_UNSUPPORTED       The flash device is not supported by this function
  @retval EFI_DEVICE_ERROR      Failed to Recognize the SPI device

**/
EFI_STATUS
EFIAPI
SpiRecognize (
  IN FLASH_DEVICE                       *FlashDevice
  )
{
  EFI_STATUS              Status;
  SPI_CONFIG_BLOCK        *SpiConfigBlock;
  UINT32                  JedecId;

  //
  // PostCode = 0x41, South bridge SPI initial
  //
  POST_CODE (DXE_SB_SPI_INIT);

  Status         = EFI_SUCCESS;
  SpiConfigBlock = (SPI_CONFIG_BLOCK *) FlashDevice->TypeSpecificInfo;
  JedecId        = 0;

  if (FlashDevice->DeviceInfo.Id == ID_COMMON_SPI_DEVICE) {
    Status = UpdateDevInfoFromSfdp (SpiConfigBlock);
    if (!EFI_ERROR (Status)) {
      switch (SpiConfigBlock->DeviceSize) {
        case 128 * SIZE_1KB:
          FlashDevice->DeviceInfo.Size = FLASH_SIZE_128K;
          break;
        case 256 * SIZE_1KB:
          FlashDevice->DeviceInfo.Size = FLASH_SIZE_256K;
          break;
        case 512 * SIZE_1KB:
          FlashDevice->DeviceInfo.Size = FLASH_SIZE_512K;
          break;
        case 1024 * SIZE_1KB:
          FlashDevice->DeviceInfo.Size = FLASH_SIZE_1024K;
          break;
        case 2048 * SIZE_1KB:
          FlashDevice->DeviceInfo.Size = FLASH_SIZE_2048K;
          break;
        case 4096 * SIZE_1KB:
          FlashDevice->DeviceInfo.Size = FLASH_SIZE_4096K;
          break;
        case 8192 * SIZE_1KB:
          FlashDevice->DeviceInfo.Size = FLASH_SIZE_8192K;
          break;
        case 16384 * SIZE_1KB:
          FlashDevice->DeviceInfo.Size = FLASH_SIZE_16384K;
          break;
         case 32768 * SIZE_1KB:
           FlashDevice->DeviceInfo.Size = FLASH_SIZE_32768K;
           break;
        case 65536 * SIZE_1KB:
          FlashDevice->DeviceInfo.Size = FLASH_SIZE_65536K;
          break;
        default:
          FlashDevice->DeviceInfo.Size = 0xff;
          break;
      }
      FlashDevice->DeviceInfo.BlockMap.BlockSize = (UINT16)(SpiConfigBlock->BlockEraseSize / 0x100);
      FlashDevice->DeviceInfo.BlockMap.Multiple = (UINT16)(SpiConfigBlock->DeviceSize / SpiConfigBlock->BlockEraseSize);
    }
    return Status;
  }

  //
  // Read device ID
  //
  mSpiProtocol->FlashReadJedecId (
                  mSpiProtocol,
                  0,
                  SpiConfigBlock->FlashIDSize,
                  (UINT8 *) &JedecId
                  );

  if (JedecId != FlashDevice->DeviceInfo.Id) {
    return EFI_UNSUPPORTED;
  }

  return EFI_SUCCESS;
}

/**
  Enable block protection on the Serial Flash device.

  @retval     EFI_SUCCESS       Operation is successful.
  @retval     EFI_DEVICE_ERROR  If there is any device errors.

**/
EFI_STATUS
EFIAPI
SpiFlashLock (
  VOID
  )
{
  EFI_STATUS  Status;

  Status = EFI_SUCCESS;

  if (mlibBiosGuardProtocol != NULL) {
    Status = mlibBiosGuardProtocol->Execute (
                                   mlibBiosGuardProtocol,
                                   FALSE
                                   );
  }

  return Status;
}

/**
  Write the SPI flash device with given address and size through PCH

  @param FlashDevice            pointer to FLASH_DEVICE structure
  @param FlashAddress           Destination Offset
  @param SrcAddress             Source Offset
  @param SPIBufferSize          The size for programming
  @param LbaWriteAddress        Write Address

  @retval EFI_SUCCESS           The SPI device was successfully recognized
  @retval EFI_UNSUPPORTED       The flash device is not supported by this function
  @retval EFI_DEVICE_ERROR      Failed to erase the target address

**/
EFI_STATUS
EFIAPI
SpiProgram (
  IN  FLASH_DEVICE              *FlashDevice,
  IN  UINT8                     *FlashAddress,
  IN  UINT8                     *SrcAddress,
  IN  UINTN                     *SPIBufferSize,
  IN  UINTN                     LbaWriteAddress
  )
{
  EFI_STATUS     Status;
  UINTN          Offset;
  UINT32         Length;
  UINT32         RemainingBytes;

  Status         = EFI_SUCCESS;
  Offset         = 0x00;
  Length         = 0x00;
  RemainingBytes = (UINT32) *SPIBufferSize;

  ASSERT ((SPIBufferSize != NULL) && (SrcAddress != NULL));

  if ((UINTN) FlashAddress < (mFlashPartBaseAddress - mFlashAreaOffset) || (UINTN) FlashAddress > (mFlashAreaBaseAddress + mFlashAreaSize - 1)) {
    return EFI_INVALID_PARAMETER;
  } else if ((UINTN) FlashAddress >= ConvertFlashBase() && (UINTN) FlashAddress < (ConvertFlashBase() + mFlashAreaSize - 1)) {
    if (mlibBiosGuardProtocol != NULL) {
      //
      // BIOS GUARD enabled Only
      // The BiosGuardProtocolWrite() offset is base on whole SPI size to calculate.
      //
      Offset = (UINTN) FlashAddress - (ConvertFlashBase() - mFlashAreaOffset);
      ASSERT ((*SPIBufferSize + Offset) <= mFlashAreaSize);
      while (RemainingBytes > 0) {
        if (RemainingBytes > BIOS_GUARD_SECTOR_SIZE) {
          Length = BIOS_GUARD_SECTOR_SIZE;
        } else {
          Length = RemainingBytes;
        }
        mlibBiosGuardProtocol->Write (
                              mlibBiosGuardProtocol,
                              (UINT32)(UINTN) Offset,
                                Length,
                              SrcAddress
                              );
        DEBUG ((DEBUG_INFO, "SpiProgram mlibBiosGuardProtocol->Write Finish \n"));
        Status = SpiFlashLock ();
        DEBUG ((DEBUG_INFO, "SpiProgram SpiFlashLock Status %r\n", Status));
        RemainingBytes -= Length;
        Offset += Length;
        SrcAddress += Length;
      }
      WriteBackInvalidateDataCacheRange ((VOID *)(LbaWriteAddress), *SPIBufferSize);

      return Status;
    }
  }
//
// Refer Intel BIOS, offset is set from BIOS region.
//
  Offset = (UINTN) FlashAddress - (ConvertFlashBase() - mFlashAreaOffset);
//  Offset = (UINTN) FlashAddress - mFlashAreaBaseAddress;

  DEBUG ((DEBUG_INFO, "SpiProgram mSpiProtocol->FlashWrite Address: 0x%x, Size: 0x%x\n", (UINTN) FlashAddress, RemainingBytes));
  while (RemainingBytes > 0) {
    if (RemainingBytes > SECTOR_SIZE_4KB) {
      Length = SECTOR_SIZE_4KB;
    } else {
      Length = RemainingBytes;
    }
//
// If flash address over BIOS end address, Let tool skip remain ranges
//
	if (Offset < (mFlashAreaOffset + mFlashAreaSize)) {
      Status = mSpiProtocol->FlashWrite (
                           mSpiProtocol,
                           FlashRegionAll,
                           (UINT32) Offset,
                           Length,
                           SrcAddress
                           );
      if (EFI_ERROR (Status)) {
        break;
      }
    } else {
      Status = EFI_SUCCESS;
    }
    RemainingBytes -= Length;
    Offset += Length;
    SrcAddress += Length;
  }
  WriteBackInvalidateDataCacheRange ((VOID *)(LbaWriteAddress), *SPIBufferSize);

  return Status;
}

/**
  Read the SPI flash device with given address and size through PCH

  @param FlashDevice            pointer to FLASH_DEVICE structure
  @param DstAddress             Destination buffer address
  @param FlashAddress           The flash device address to be read
  @param BufferSize             The size to be read

  @retval EFI_SUCCESS           The SPI device was successfully recognized
  @retval EFI_UNSUPPORTED       The flash device is not supported by this function
  @retval EFI_DEVICE_ERROR      Failed to erase the target address

**/
EFI_STATUS
EFIAPI
SpiRead (
  IN  FLASH_DEVICE              *FlashDevice,
  IN  UINT8                     *DstAddress,
  IN  UINT8                     *FlashAddress,
  IN  UINTN                     BufferSize
  )
{
  EFI_STATUS             Status;
  UINTN                  Offset;
  UINTN                  Index;

  Status                 = EFI_SUCCESS;
  Offset                 = 0x00;

  ASSERT ((FlashAddress != NULL) && (DstAddress != NULL));
  ASSERT (BufferSize <= (mFlashAreaOffset + mFlashAreaSize));

  if ((UINTN) FlashAddress < (mFlashPartBaseAddress - mFlashAreaOffset) || (UINTN) FlashAddress > (mFlashAreaBaseAddress + mFlashAreaSize - 1)) {
    return EFI_INVALID_PARAMETER;
  } else if ((UINTN) FlashAddress >= ConvertFlashBase() && (UINTN) FlashAddress < (ConvertFlashBase() + mFlashAreaSize - 1)) {
    if (PcdGetBool (PcdSpiReadByMemoryMapped)) {
      //
      // This function is implemented specifically for those platforms
      // at which the SPI device is memory mapped for read. So this
      // function just do a memory copy for Spi Flash Read.
      //
      if (GetFlashMode() == FW_FLASH_MODE) {
        FlashAddress = FlashAddress - mFlashPartBaseAddress + mFlashAreaBaseAddress;
      }
      CopyMem ((VOID *) DstAddress, (VOID *) FlashAddress, BufferSize);

      return EFI_SUCCESS;
    }
  }

  Offset = (UINTN) FlashAddress - (ConvertFlashBase() - mFlashAreaOffset);
//  Offset = (UINTN) FlashAddress - mFlashAreaBaseAddress;
//
// If flash address over BIOS end address, Let tool skip remain ranges
//
  if (Offset >= (mFlashAreaOffset + mFlashAreaSize)) {
    for (Index = 0; Index < BufferSize; Index++) {
      *(DstAddress + Index) = 0xFF;
    }
    return EFI_SUCCESS;
  }

  DEBUG ((DEBUG_INFO, "SpiRead mSpiProtocol->FlashRead DestAddress: 0x%x, Address: 0x%x, Size: 0x%x\n", (UINTN) DstAddress, (UINTN) FlashAddress, BufferSize));
  Status = mSpiProtocol->FlashRead (
                           mSpiProtocol,
                           FlashRegionAll,
                           (UINT32) Offset,
                           (UINT32) BufferSize,
                           DstAddress
                           );

  DEBUG ((DEBUG_INFO, "SpiRead mSpiProtocol->FlashRead Status %r\n", Status));

  return Status;
}

/**
  Erase the SPI flash device from LbaWriteAddress through PCH

  @param FlashDevice            pointer to FLASH_DEVICE structure
  @param FlashAddress           Target address to be erased
  @param Size                   The size in bytes to be erased

  @retval EFI_SUCCESS           The SPI device was successfully recognized
  @retval EFI_UNSUPPORTED       The flash device is not supported by this function
  @retval EFI_DEVICE_ERROR      Failed to erase the target address

**/
EFI_STATUS
EFIAPI
SpiErase(
  IN  FLASH_DEVICE       *FlashDevice,
  IN  UINTN              Address,
  IN  UINTN              EraseBlockSize
  )
{
  EFI_STATUS             Status;
  UINTN                  Offset;
  UINTN                  RemainingBytes;

  Status                 = EFI_SUCCESS;
  Offset                 = 0x00;
  RemainingBytes         = EraseBlockSize;

  ASSERT ((EraseBlockSize % SECTOR_SIZE_4KB) == 0);

  if (Address < (mFlashPartBaseAddress - mFlashAreaOffset) || Address > (mFlashAreaBaseAddress + mFlashAreaSize - 1)) {
    return EFI_INVALID_PARAMETER;
  } else if (Address >= ConvertFlashBase() && Address < (ConvertFlashBase() + mFlashAreaSize - 1)) {
    if (mlibBiosGuardProtocol != NULL) {
      //
      // BIOS GUARD enabled Only
      // The BiosGuardProtocolBlockErase() offset is base on whole SPI size to calculate.
      //
      Offset = Address - (ConvertFlashBase() - mFlashAreaOffset);
      ASSERT ((EraseBlockSize + Offset) <= mFlashAreaSize);
      DEBUG ((DEBUG_INFO, "SpiErase mlibBiosGuardProtocol->Erase Offset: 0x%x, Size: 0x%x\n", Offset, EraseBlockSize));
      while (RemainingBytes > 0) {
        mlibBiosGuardProtocol->Erase (
                              mlibBiosGuardProtocol,
                              (UINT32) Offset
                              );
        RemainingBytes -= SECTOR_SIZE_4KB;
        Offset         += SECTOR_SIZE_4KB;
      }
      DEBUG ((DEBUG_INFO, "SpiErase mlibBiosGuardProtocol->Erase Finished.\n"));

      Status = SpiFlashLock ();
      DEBUG ((DEBUG_INFO, "SpiErase SpiFlashLock Status %r\n", Status));
      WriteBackInvalidateDataCacheRange ((VOID *)(Address), EraseBlockSize);

      return Status;
    }
  }
  Offset = Address - (ConvertFlashBase() - mFlashAreaOffset);
  //Offset = Address - mFlashAreaBaseAddress;
  ASSERT ((EraseBlockSize + Offset) <= (mFlashAreaOffset + mFlashAreaSize));
//
// If flash address over BIOS end address, Let tool skip remain ranges
//
  if (Offset >= (mFlashAreaOffset + mFlashAreaSize)) {
    return EFI_SUCCESS;
  }

  DEBUG ((DEBUG_INFO, "SpiErase mSpiProtocol->FlashErase Address: 0x%x, Size: 0x%x\n", Address, EraseBlockSize));
  Status = mSpiProtocol->FlashErase (
                           mSpiProtocol,
                           FlashRegionAll,
                           (UINT32) Offset,
                           (UINT32) RemainingBytes
                           );

  DEBUG ((DEBUG_INFO, "SpiErase mSpiProtocol->FlashErase Status %r\n", Status));
  WriteBackInvalidateDataCacheRange ((VOID *)(Address), EraseBlockSize);

  return Status;
}

/**
  Check whether the flash region is used or not

  @param FlashRegion            Flash Region x Register (x = 0 - 3)

  @retval TRUE                  The region is used
  @retval FALSE                 The region is not used

**/
BOOLEAN
CheckFlashRegionIsValid (
  IN       UINT32    FlashRegion
  )
{
  BOOLEAN         Flag = TRUE;
//[-start-190701-16990084-add]//
  UINT32          RegionBase;
  UINT32          RegionLimit;

  RegionBase = ((FlashRegion & B_SPI_MEM_FREGX_BASE_MASK) >> N_SPI_MEM_FREGX_BASE) << N_SPI_MEM_FREGX_BASE_REPR;
  RegionLimit = ((((FlashRegion & B_SPI_MEM_FREGX_LIMIT_MASK) >> N_SPI_MEM_FREGX_LIMIT)) << N_SPI_MEM_FREGX_LIMIT_REPR);

  if (RegionBase > RegionLimit) {
//[-end-190701-16990084-add]//
    Flag = FALSE;
  }
  return Flag;
}

/**
  Get flash table from platform

  @param DataBuffer             IN: the input buffer address
                                OUT:the flash region table from rom file

  @retval EFI_SUCCESS           Function successfully returned

**/
EFI_STATUS
GetSpiPlatformFlashTable (
  IN OUT   UINT8    *DataBuffer
  )
{
  UINT8           Index;
  FLASH_REGION    *FlashTable;

  Index      = 0;
  FlashTable = NULL;

  FlashTable = (FLASH_REGION *)DataBuffer;

  for (Index = FlashRegionDescriptor; Index < FlashRegionMax; Index++) {
    FlashTable->Type   = mPlatformFlashTable[Index].Type;
    FlashTable->Offset = mPlatformFlashTable[Index].Offset;
    FlashTable->Size   = mPlatformFlashTable[Index].Size;
    FlashTable->Access = mPlatformFlashTable[Index].Access;

    if (FlashTable->Type == FLASH_REGION_TYPE_OF_EOS){
      break;
    }

    FlashTable++;
  }

  FlashTable->Type = FLASH_REGION_TYPE_OF_EOS;
  return EFI_SUCCESS;
}

/**
  Save flash table from platform

  @param FlashTable             IN: the input buffer address
                                OUT:the flash region table from rom file

  @param TableSize             Size of FlashTable

**/
EFI_STATUS
SaveSpiPlatformFlashTable (
  IN OUT   FLASH_REGION    *FlashTable,
  IN       UINT32           TableSize
  )
{
  UINT8           Index;
  UINT32          FlashRegionReg;
  UINT32          ReadAccess;
  UINT32          WriteAccess;
  UINT32          BaseAddress;
  UINT32          Frap;
  UINT8           TableEntryCount;

  TableEntryCount = 0;

  Frap = MmioRead16 (mPchSpiBar0 + R_SPI_MEM_FRAP);

  for (Index = FlashRegionDescriptor; Index < FlashRegionMax; Index++) {

    if (TableEntryCount >= (TableSize/sizeof(FLASH_REGION) - 1)){
      break;
    }

    MmioAndThenOr32 (
      mPchSpiBar0 + R_SPI_MEM_FDOC,
      (UINT32) (~(B_SPI_MEM_FDOC_FDSS_MASK | B_SPI_MEM_FDOC_FDSI_MASK)),
      (UINT32) (V_SPI_MEM_FDOC_FDSS_REGN | Index << 2)
      );

    FlashRegionReg = MmioRead32 (mPchSpiBar0 + R_SPI_MEM_FDOD);

    if (CheckFlashRegionIsValid (FlashRegionReg)) {
      switch (Index){
      case FlashRegionEC:
        FlashTable->Type = EC_REGION;
        break;
      case FlashRegionDer:
        FlashTable->Type = DEVICE_EXPANSION_REGION;
        break;
      default:
        FlashTable->Type = Index;
        break;
      }

      FlashTable->Offset = (FlashRegionReg & B_SPI_MEM_FREGX_BASE_MASK) << N_SPI_MEM_FREGX_BASE_REPR;
      // Refer Silicon code, SpiCommon.c
      BaseAddress = ((FlashRegionReg & B_SPI_MEM_FREGX_BASE_MASK) >> N_SPI_MEM_FREGX_BASE) << N_SPI_MEM_FREGX_BASE_REPR;
      //
      // Region limit address Bits[11:0] are assumed to be FFFh
      //
      FlashTable->Size = ((((FlashRegionReg & B_SPI_MEM_FREGX_LIMIT_MASK) >> N_SPI_MEM_FREGX_LIMIT) + 1) << N_SPI_MEM_FREGX_LIMIT_REPR) - BaseAddress;

      //
      //we can override  BIOS region, ME region, and GBE regiron the permissions
      //in the Flash Descriptor through BMWAG and BMRAG.
      //
      if (Index >= FlashRegionBios && Index <= FlashRegionGbE) {
        ReadAccess = (Frap >> (BIOS_REGION_READ_ACCESS + Index)) & ACCESS_AVAILABLE;
        ReadAccess |= ((Frap >> (BIOS_MASTER_READ_ACCESS_GRANT + Index)) & ACCESS_AVAILABLE);
        WriteAccess = (Frap >> (BIOS_REGION_WRITE_ACCESS + Index)) & ACCESS_AVAILABLE;
        WriteAccess |= ((Frap >> (BIOS_MASTER_WRITE_ACCESS_GRANT + Index)) & ACCESS_AVAILABLE);

      } else if (Index < MAX_FLASH_REGION) {
        ReadAccess = (Frap >> (BIOS_REGION_READ_ACCESS + Index)) & ACCESS_AVAILABLE;
        WriteAccess = (Frap >> (BIOS_REGION_WRITE_ACCESS + Index)) & ACCESS_AVAILABLE;
      } else {
        ReadAccess = ACCESS_AVAILABLE;
        WriteAccess = ACCESS_AVAILABLE;
      }

      if (Index == FlashRegionEC) {
        ReadAccess = ACCESS_AVAILABLE;
        WriteAccess = ACCESS_AVAILABLE;
      }
      if (ReadAccess == ACCESS_AVAILABLE && WriteAccess == ACCESS_AVAILABLE) {
        FlashTable->Access = ACCESS_AVAILABLE;
      } else {
        FlashTable->Access = ACCESS_NOT_AVAILABLE;
      }

      TableEntryCount++;
      FlashTable++;
    }
  }

  FlashTable->Type = FLASH_REGION_TYPE_OF_EOS;
  return EFI_SUCCESS;
}

/**
  Get flash number from SPI Descriptor

  @param FlashNumber            Number of SPI flash devices returned

  @retval EFI_SUCCESS           Function successfully returned
  @retval EFI_UNSUPPORTED       The SPI flash is not in Descriptor mode

**/
EFI_STATUS
GetSpiFlashNumber (
  OUT   UINT8    *FlashNumber
  )
{
  *FlashNumber = mNumberOfComponents + 1;

  return EFI_SUCCESS;
}

/**
  Offset the BIOS address from top of 4G memory address to correct BIOS region
  described in SPI Descriptor

  @param MemoryAddress          BIOS memory mapped address
  @param BiosRegionAddress      Flash ROM start address + BIOS address in flash ROM

  @retval EFI_SUCCESS           Function successfully returned
  @retval EFI_UNSUPPORTED       The SPI flash is not in Descriptor mode

**/
EFI_STATUS
MemoryToBiosRegionAddress (
  IN UINTN       MemoryAddress,
  OUT UINTN      *BiosRegionAddress
  )
{
  UINTN   Offset;

  if (((MmioRead32 (mPchSpiBar0 + R_SPI_MEM_HSFSC) & B_SPI_MEM_HSFSC_FDV) == B_SPI_MEM_HSFSC_FDV) &&
      ((UINTN)MemoryAddress >= (UINTN)mFlashAreaBaseAddress)) {
#if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 0    
    Offset = MemoryAddress - (UINTN)mFlashAreaBaseAddress; // the data offset in BIOS region
    *BiosRegionAddress = PcdGet32 (PcdFlashAreaBaseAddress) + Offset;
#else
    Offset = MemoryAddress - (UINTN)PcdGet32 (PcdFlashAreaBaseAddress);
#endif    
  } else {
    return EFI_UNSUPPORTED;
  }

  return EFI_SUCCESS;
}

/**
  SpiAccessLib Library Class Constructor

  @retval EFI_SUCCESS           Module initialized successfully
  @retval Others                Module initialization failed

**/
EFI_STATUS
EFIAPI
SpiAccessLibInit (
  VOID
  )
{
  EFI_STATUS			      Status;
  UINTN                 PchSpiBase;
  UINT8                 Comp0Density;
  UINT8                 Comp1Density;
  UINT32                FlashSize1;
  UINT32                FlashSize2;
  UINT32                ReadValue;
  UINT32                ReadECValue;

  PchSpiBase            = 0x00;
  Comp0Density          = 0x00;
  Comp1Density          = 0x00;
  FlashSize1            = 0x00;
  FlashSize2            = 0x00;
  mPchSpiBar0           = 0x00;
  mFlashAreaBaseAddress = 0x00;
  mFlashAreaSize        = 0x00;
  mFlashAreaOffset      = 0x00;
  mNumberOfComponents   = 0x00;
  mSpiProtocol          = NULL;
  mlibBiosGuardProtocol = NULL;
  ReadValue = 0x0;
  ReadECValue = 0x0;

#if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 0
  mFlashAreaBaseAddress = (UINTN)PcdGet32 (PcdFlashAreaBaseAddress);
  mFlashAreaSize        = (UINTN)PcdGet32 (PcdFlashAreaSize);
#else
  mFlashAreaBaseAddress = (UINTN)PcdGet32 (PcdFlashAreaBaseAddress) + (UINTN)PcdGet32 (PcdFlashExtendRegionOffset);
  mFlashAreaSize        = 0x1000000 + (UINTN)PcdGet32 (PcdFlashExtendRegionSizeInUse);
#endif

  PchSpiBase = MmPciBase (
                 DEFAULT_PCI_BUS_NUMBER_PCH,
                 PCI_DEVICE_NUMBER_PCH_SPI,
                 PCI_FUNCTION_NUMBER_PCH_SPI
                 );

  mPchSpiBar0 = MmioRead32 (PchSpiBase + R_SPI_CFG_BAR0) & ~(B_SPI_CFG_BAR0_MASK);
  if (mPchSpiBar0 == 0) {
    DEBUG ((DEBUG_ERROR, "ERROR : PchSpiBar0 is invalid!\n"));
    ASSERT (FALSE);
  }

  //
  // Get the Number Of SPI Components
  // Select to Flash Map 0 Register to get the number of flash Component
  //
  MmioAndThenOr32 (
    mPchSpiBar0 + R_SPI_MEM_FDOC,
    (UINT32) (~(B_SPI_MEM_FDOC_FDSS_MASK | B_SPI_MEM_FDOC_FDSI_MASK)),
    (UINT32) (V_SPI_MEM_FDOC_FDSS_FSDM | R_FLASH_FDBAR_FLASH_MAP0)
    );
  //
  // Update Zero based Number Of Components
  //
  mNumberOfComponents = (UINT8) ((MmioRead16 (mPchSpiBar0 + R_SPI_MEM_FDOD) & B_FLASH_FDBAR_NC) >> N_FLASH_FDBAR_NC);

  //
  // Get SPI Component Size from flash component register
  //
  MmioAndThenOr32 (
    mPchSpiBar0 + R_SPI_MEM_FDOC,
    (UINT32) (~(B_SPI_MEM_FDOC_FDSS_MASK | B_SPI_MEM_FDOC_FDSI_MASK)),
    (UINT32) (V_SPI_MEM_FDOC_FDSS_COMP | R_FLASH_FCBA_FLCOMP)
    );
  Comp0Density = (UINT8) MmioRead32 (mPchSpiBar0 + R_SPI_MEM_FDOD) & B_FLASH_FLCOMP_COMP0_MASK;
  Comp1Density = (((UINT8) MmioRead32 (mPchSpiBar0 + R_SPI_MEM_FDOD) & B_FLASH_FLCOMP_COMP1_MASK) >> 4);

  FlashSize1 = (UINT32) (V_FLASH_FLCOMP_COMP_512KB << Comp0Density);
  if (mNumberOfComponents == 1) {
    FlashSize2 = (UINT32) (V_FLASH_FLCOMP_COMP_512KB << Comp1Density);
  }

  //
  // Get Embedded Controller region base & limit from flash region register
  //
  MmioAndThenOr32 (
    mPchSpiBar0 + R_SPI_MEM_FDOC,
    (UINT32) (~(B_SPI_MEM_FDOC_FDSS_MASK | B_SPI_MEM_FDOC_FDSI_MASK)),
    (UINT32) (V_SPI_MEM_FDOC_FDSS_REGN | (B_FLASH_FDBAR_FRBA + 0x20))
    );
//[-start-190611-IB16990046-add]//
  mFlashECRegionBase = ((UINTN) ((MmioRead32 (mPchSpiBar0 + R_SPI_MEM_FDOD) & B_SPI_MEM_FREGX_BASE_MASK) >> N_SPI_MEM_FREGX_BASE) << N_SPI_MEM_FREGX_BASE_REPR);
  mFlashECRegionLimit = ((UINTN) ((MmioRead32 (mPchSpiBar0 + R_SPI_MEM_FDOD) & B_SPI_MEM_FREGX_LIMIT_MASK) >> N_SPI_MEM_FREGX_LIMIT) << N_SPI_MEM_FREGX_LIMIT_REPR);

  //
  //  Flash Part Address representation:
  //
  ReadValue = MmioRead32 (mPchSpiBar0 + (R_SPI_MEM_FREG0_FLASHD + (S_SPI_MEM_FREGX * ((UINT32) FlashRegionBios))));
  mFlashAreaOffset = ((UINTN) ((ReadValue & B_SPI_MEM_FREGX_BASE_MASK) >> N_SPI_MEM_FREGX_BASE) << N_SPI_MEM_FREGX_BASE_REPR);

  //
  //  FFT Tool Address representation:
  //  4G - RomSize + BiosActualOffsetInRom
  //
  mFlashPartBaseAddress = (UINTN) (MEM_EQU_4GB - FlashSize1 - FlashSize2 + mFlashAreaOffset);
#if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 0  
  Status = PcdSet32S (PcdFlashPartBaseAddress, (UINT32)mFlashPartBaseAddress);
#else
  Status = PcdSet32S (PcdFlashPartBaseAddress, PcdGet32 (PcdFlashAreaBaseAddress));
#endif  
  ASSERT_EFI_ERROR (Status);
//[-end-190611-IB16990046-add]//
  //
  // Save Flash Region Table before PCH lock "Flash Descriptor Region"
  //
  ZeroMem (mPlatformFlashTable, sizeof (mPlatformFlashTable));
  SaveSpiPlatformFlashTable (mPlatformFlashTable, sizeof(mPlatformFlashTable));

  SpiAccessInit();

  IrsiAddVirtualPointer ((VOID **) mFlashAreaBaseAddress);
  IrsiAddVirtualPointer ((VOID **) mPchSpiBar0);
  IrsiAddVirtualPointer ((VOID **) &mSpiProtocol);
  return EFI_SUCCESS;
}

/**
  If a  driver exits with an error, it must call this routine
  to free the allocated resource before the exiting.

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.
  @param[in] SystemTable    A pointer to the EFI System Table.

  @retval  EFI_SUCCESS      The Driver Lib shutdown successfully.
**/
EFI_STATUS
EFIAPI
SpiAccessLibDestruct (
  VOID
  )
{
  SpiAccessDestroy ();
  IrsiRemoveVirtualPointer ((VOID **)mFlashAreaBaseAddress);
  IrsiRemoveVirtualPointer ((VOID **)mPchSpiBar0);
  IrsiRemoveVirtualPointer ((VOID **)&mSpiProtocol);
  return EFI_SUCCESS;
}
