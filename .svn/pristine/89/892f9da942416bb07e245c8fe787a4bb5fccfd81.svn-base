## @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*
** @file
# This is a VTIO Dxe driver which is used to support VTIO feature.
#
#@copyright
# Copyright (c) 2019 - 2020, Intel Corporation. All rights reserved.
# This software and associated documentation (if any) is furnished
# under a license and may only be used or copied in accordance
# with the terms of the license. Except as permitted by such
# license, no part of this software or documentation may be
# reproduced, stored in a retrieval system, or transmitted in any
# form or by any means without the express written consent of
# Intel Corporation.
# This file contains a 'Sample Driver' and is licensed as such
# under the terms of your license agreement with Intel or your
# vendor. This file may be modified by the user, subject to
# the additional terms of the license agreement.
#
#@par Specification Reference:
#
##

[Defines]
  INF_VERSION                    = 0x00010007
  BASE_NAME                      = VtioDxe
  FILE_GUID                      = 812BA1E0-6A75-4AE5-AD37-63D030974501
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = VtioDxeEntryPoint

#
# The following information is for reference only and not required by the build tools.
#
#  VALID_ARCHITECTURES           = IA32 X64 IPF EBC
#
[BuildOptions]
#*_*_*_CC_FLAGS = /FAc

[Sources]

  VtioDxe.c

[Packages]
  MdePkg/MdePkg.dec
  #[-start-191111-IB10189001-remove]#
  # CryptoPkg/CryptoPkg.dec
  #[-end-191111-IB10189001-remove]#
  AlderLakePlatSamplePkg/PlatformPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  SecurityPkg/SecurityPkg.dec

[LibraryClasses]
  BaseLib
  BaseCryptLib
  MemoryAllocationLib
  UefiBootServicesTableLib
  UefiDriverEntryPoint
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  BaseMemoryLib
  DebugLib
  PrintLib
  UefiLib
  HobLib
  DxeMeLib
  Tpm2DeviceLib
  RngLib
  PciSegmentLib
  PchPciBdfLib

[Guids]
  gEfiEventExitBootServicesGuid                 ## CONSUMES
  gEfiEventLegacyBootGuid                       ## CONSUMES
  gMeBiosPayloadHobGuid
  gSetupVariableGuid                            ## CONSUMES
  gSaSetupVariableGuid                          ## CONSUMES

[Protocols]
  gHeciProtocolGuid                             ## CONSUMES
  gEfiTcg2ProtocolGuid                          ## CONSUMES
  gEfiAcpiTableProtocolGuid                     ## CONSUMES

[Depex]
  gEfiAcpiTableProtocolGuid AND
  gHeciProtocolGuid

