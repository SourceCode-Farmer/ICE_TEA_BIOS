/** @file
 
;******************************************************************************
;* Copyright 2020 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  This file is SampleCode of the library for Intel PCH PEI Policy initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PeiPchPolicyUpdate.h"
#include <Guid/GlobalVariable.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPcieRpLib.h>
#include <Library/PchPolicyLib.h>
#include <Library/PeiSiPolicyUpdateLib.h>
#include <Library/SataSocLib.h>
#include <Library/SiPolicyLib.h>

#include <PolicyUpdateMacro.h>
//[-start-211102-QINGLIN0108-add]//
#if defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(C770_SUPPORT) || defined(S77014IAH_SUPPORT)
#include <Library/CnviLib.h>
#include <Library/OemSvcLfcPeiGetBoardID.h>
#include <SetupVariable.h>
#include <Library/VariableLib.h>
#include <SetupConfig.h>
#endif
//[-end-211102-QINGLIN0108-add]//
#if FixedPcdGet8(PcdFspModeSelection) == 1
#include <FspmUpd.h>
#endif

#include <Pins/GpioPinsVer2Lp.h>
#include <PchDmiConfig.h>

//[-start-190410-IB16990021-add]//
#include <PortConfig.h>
//[-end-190410-IB16990021-add]//
//
// Sawtooth Peak
// Single SPD EEPROM at 0xA2 serves both C0D0 and C1D0 (LPDDR is 1DPC only)
//
#define DIMM_SMB_SPD_P0C0D0_STP 0xA2
#define DIMM_SMB_SPD_P0C0D1_STP 0xA0
#define DIMM_SMB_SPD_P0C1D0_STP 0xA2
#define DIMM_SMB_SPD_P0C1D1_STP 0xA0

GLOBAL_REMOVE_IF_UNREFERENCED UINT8 mSmbusSTPRsvdAddresses[] = {
  DIMM_SMB_SPD_P0C0D0_STP,
  DIMM_SMB_SPD_P0C0D1_STP,
  DIMM_SMB_SPD_P0C1D0_STP,
  DIMM_SMB_SPD_P0C1D1_STP
};

/**
  Update PCH General pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspsUpm        Pointer to FSPM_UPD
  @param[in] PchSetup       Pointer to PCH_SETUP
  @param[in] SetupData      Pointer to SETUP_DATA
**/
STATIC
VOID
UpdatePchGeneralPreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy,
  IN  VOID                     *FspmUpd,
  IN  PCH_SETUP                *PchSetup,
  IN  SETUP_DATA               *SetupData
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_GENERAL_PREMEM_CONFIG       *PchGeneralPreMemConfig;
  EFI_STATUS                      Status;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gPchGeneralPreMemConfigGuid, (VOID *) &PchGeneralPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchPort80Route, PchGeneralPreMemConfig->Port80Route,  PchSetup->IchPort80Route, NullIndex);
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GpioOverride, PchGeneralPreMemConfig->GpioOverride, 0);

#if FixedPcdGet8(PcdFspModeSelection) == 1
  ((FSPM_UPD *) FspmUpd)->FspmConfig.BoardGpioTablePreMemAddress = (UINT32) PcdGetPtr (PcdBoardGpioTablePreMem);
#endif
}

/**
  Update DCI pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspsUpm        Pointer to FSPM_UPD
  @param[in] PchSetup       Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateDciPreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy,
  IN  VOID                     *FspmUpd,
  IN  PCH_SETUP                *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_DCI_PREMEM_CONFIG           *DciPreMemConfig;
  EFI_STATUS                      Status;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gDciPreMemConfigGuid, (VOID *) &DciPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DciEn,              DciPreMemConfig->DciEn,              PchSetup->DciEn,              NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DciDbcMode,         DciPreMemConfig->DciDbcMode,         PchSetup->DciDbcMode,         NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DciUsb3TypecUfpDbg, DciPreMemConfig->DciUsb3TypecUfpDbg, PchSetup->DciUsb3TypecUfpDbg, NullIndex);
}

/**
  Update Pch TraceHub pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspsUpm        Pointer to FSPM_UPD
  @param[in] PchSetup       Pointer to PCH_SETUP
**/
STATIC
VOID
UpdatePchTraceHubPreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy,
  IN  VOID                     *FspmUpd,
  IN  PCH_SETUP                *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_TRACE_HUB_PREMEM_CONFIG     *PchTraceHubPreMemConfig;
  EFI_STATUS                      Status;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gPchTraceHubPreMemConfigGuid, (VOID *) &PchTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchTraceHubMode,        PchTraceHubPreMemConfig->TraceHub.EnableMode,  PchSetup->PchTraceHubMode,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchTraceHubMemReg0Size, PchTraceHubPreMemConfig->TraceHub.MemReg0Size, PchSetup->PchTraceHubMemReg0Size, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchTraceHubMemReg1Size, PchTraceHubPreMemConfig->TraceHub.MemReg1Size, PchSetup->PchTraceHubMemReg1Size, NullIndex);
}

/**
  Update Smbus pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspsUpm        Pointer to FSPM_UPD
  @param[in] PchSetup       Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateSmbusPreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy,
  IN  VOID                     *FspmUpd,
  IN  PCH_SETUP                *PchSetup
  )
{
  UINT8                           *SmBusReservedTable;
  UINT8                           SmBusReservedNum;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_SMBUS_PREMEM_CONFIG         *SmbusPreMemConfig;
  EFI_STATUS                      Status;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gSmbusPreMemConfigGuid, (VOID *) &SmbusPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SmbusEnable,       SmbusPreMemConfig->Enable,         TRUE);
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchSmbAlertEnable, SmbusPreMemConfig->SmbAlertEnable, PcdGetBool (PcdSmbusAlertEnable));

  SmBusReservedTable = NULL;
  SmBusReservedNum   = 0;
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchSmbusIoBase, SmbusPreMemConfig->SmbusIoBase, PcdGet16 (PcdSmbusBaseAddress));
  SmBusReservedTable = mSmbusSTPRsvdAddresses;
  SmBusReservedNum   = sizeof (mSmbusSTPRsvdAddresses);

  if (SmBusReservedTable != NULL) {
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchNumRsvdSmbusAddresses, SmbusPreMemConfig->NumRsvdSmbusAddresses, SmBusReservedNum);
#if FixedPcdGet8(PcdFspModeSelection) == 1
    ((FSPM_UPD *) FspmUpd)->FspmConfig.RsvdSmbusAddressTablePtr = (UINT32) SmBusReservedTable;
#else
    CopyMem (SmbusPreMemConfig->RsvdSmbusAddressTable, SmBusReservedTable, SmBusReservedNum);
#endif
  }
}

//[-start-211108-IB18410125-add]//
/**
  Update PCH PCIE HSIO pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
**/
STATIC
VOID
UpdateHsioPciePreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  UINTN                           Index;
  EFI_STATUS                      Status;
  PCH_HSIO_PCIE_PREMEM_CONFIG     *HsioPciePreMemConfig;
  UINTN                           MaxPcieRootPorts;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gHsioPciePreMemConfigGuid, (VOID *) &HsioPciePreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  MaxPcieRootPorts = GetPchMaxPciePortNum ();
//[-start-220412-TAMT000048-A-modify]//
//[-start-220805-QINGLIN0172-modify]//
#if defined(S77014IAH_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT)
//[-end-220805-QINGLIN0172-modify]//
  for (Index = 0; Index < MaxPcieRootPorts; Index ++) {
    HsioPciePreMemConfig->Lane[Index].HsioRxSetCtleEnable           = ((UINT8 *)PcdGetPtr (PchPcieHsioRxSetCtleEnable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioRxSetCtle                 = ((UINT8 *)PcdGetPtr (PchPcieHsioRxSetCtle))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen1DownscaleAmpEnable  = 0;
    HsioPciePreMemConfig->Lane[Index].HsioTxGen1DownscaleAmp        = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen1DownscaleAmp))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DownscaleAmpEnable  = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DownscaleAmpEnable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DownscaleAmp        = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DownscaleAmp))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen3DownscaleAmpEnable  = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen3DownscaleAmpEnable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen3DownscaleAmp        = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen3DownscaleAmp))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen1DeEmphEnable        = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen1DeEmphEnable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen1DeEmph              = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen1DeEmph))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DeEmph3p5Enable     = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DeEmph3p5Enable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DeEmph3p5           = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DeEmph3p5))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DeEmph6p0Enable     = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DeEmph6p0Enable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DeEmph6p0           = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DeEmph6p0))[Index];
  }
#else
  for (Index = 0; Index < MaxPcieRootPorts; Index ++) {
    HsioPciePreMemConfig->Lane[Index].HsioRxSetCtleEnable           = ((UINT8 *)PcdGetPtr (PchPcieHsioRxSetCtleEnable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioRxSetCtle                 = ((UINT8 *)PcdGetPtr (PchPcieHsioRxSetCtle))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen1DownscaleAmpEnable  = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen1DownscaleAmpEnable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen1DownscaleAmp        = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen1DownscaleAmp))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DownscaleAmpEnable  = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DownscaleAmpEnable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DownscaleAmp        = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DownscaleAmp))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen3DownscaleAmpEnable  = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen3DownscaleAmpEnable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen3DownscaleAmp        = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen3DownscaleAmp))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen1DeEmphEnable        = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen1DeEmphEnable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen1DeEmph              = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen1DeEmph))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DeEmph3p5Enable     = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DeEmph3p5Enable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DeEmph3p5           = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DeEmph3p5))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DeEmph6p0Enable     = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DeEmph6p0Enable))[Index];
    HsioPciePreMemConfig->Lane[Index].HsioTxGen2DeEmph6p0           = ((UINT8 *)PcdGetPtr (PchPcieHsioTxGen2DeEmph6p0))[Index];
  }
#endif
//[-end-220412-TAMT000048-A-modify]//
#endif
}
//[-end-211108-IB18410125-add]//

/**
  Update Lpc pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspsUpm        Pointer to FSPM_UPD
  @param[in] PchSetup       Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateLpcPreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy,
  IN  VOID                     *FspmUpd,
  IN  PCH_SETUP                *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_LPC_PREMEM_CONFIG           *LpcPreMemConfig;
  EFI_STATUS                      Status;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gLpcPreMemConfigGuid, (VOID *) &LpcPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchLpcEnhancePort8xhDecoding, LpcPreMemConfig->EnhancePort8xhDecoding, PchSetup->EnhancePort8xhDecoding, NullIndex);
}

/**
  Update WatchDog pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspsUpm        Pointer to FSPM_UPD
  @param[in] PchSetup       Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateWatchDogPreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy,
  IN  VOID                     *FspmUpd,
  IN  PCH_SETUP                *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_WDT_PREMEM_CONFIG           *WatchDogPreMemConfig;
  EFI_STATUS                      Status;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gWatchDogPreMemConfigGuid, (VOID *) &WatchDogPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WdtDisableAndLock, WatchDogPreMemConfig->DisableAndLock, (UINTN)(!PchSetup->WdtEnable), NullIndex);
}

VOID
UpdatePcieClockInfo (
  PCH_PCIE_RP_PREMEM_CONFIG  *PcieRpPreMemConfig,
  IN VOID                    *FspmUpd,
  UINTN                      Index,
  UINT64                     Data
  )
{
  PCD64_BLOB Pcd64;

  Pcd64.Blob = Data;
  DEBUG ((DEBUG_INFO, "UpdatePcieClockInfo ClkIndex %x ClkUsage %x, Supported %x\n", Index, Pcd64.PcieClock.ClockUsage, Pcd64.PcieClock.ClkReqSupported));

  UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[Index], PcieRpPreMemConfig->PcieClock[Index].Usage, (UINT8)Pcd64.PcieClock.ClockUsage);
  UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcClkReq[Index], PcieRpPreMemConfig->PcieClock[Index].ClkReq, Pcd64.PcieClock.ClkReqSupported ? (UINT8)Index : 0xFF);
}

//[-start-211102-QINGLIN0108-add]//
//[-start-211103-DABING0020-modify]//
#if defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(C770_SUPPORT) || defined(S77014IAH_SUPPORT)
BOOLEAN
OemSvcLfcIsUmaConfig (
  VOID
  )
{
  UINT8                       UmaAndDisType;
  SA_SETUP                    *SaSetupNvData;
  UINTN                       BufferSize;
  BOOLEAN                     IsUmaConfig;

  IsUmaConfig = TRUE;

  OemSvcLfcGetBoardID(GPU_ID, &UmaAndDisType);
  if (UmaAndDisType != GPU_ID_UMA_ONLY) {
    CommonGetVariableDataAndSize (
      SA_SETUP_VARIABLE_NAME,
      &gSaSetupVariableGuid,
      &BufferSize,
      (VOID **) &SaSetupNvData
      );
    
    if (SaSetupNvData != NULL) {
      if (SaSetupNvData->PrimaryDisplay == 4) { //switchable
        IsUmaConfig =  FALSE;
      }
      FreePool (SaSetupNvData);
    }
  }

  return IsUmaConfig;
}

BOOLEAN
OemSvcLfcIsSetupWlanConfigDisable (
  VOID
  )
{
  SYSTEM_CONFIGURATION        *SetupNvData;
  UINTN                       BufferSize;
  BOOLEAN                     IsWlanDisable;

  IsWlanDisable = FALSE;

  CommonGetVariableDataAndSize (
    L"Setup",
    &gSystemConfigurationGuid,
    &BufferSize,
    (VOID **) &SetupNvData
    );

  if (SetupNvData != NULL) {
    if (SetupNvData->L05WirelessLan == 0) { //disable
      IsWlanDisable = TRUE;
    }
    FreePool (SetupNvData);
  }

  return IsWlanDisable;
}
#endif
//[-end-211102-QINGLIN0108-add]//

#if defined(S77013_SUPPORT)
BOOLEAN
OemSvcLfcIsSetupWlanConfigDisable (
  VOID
  )
{
  SYSTEM_CONFIGURATION        *SetupNvData;
  UINTN                       BufferSize;
  BOOLEAN                     IsWlanDisable;

  IsWlanDisable = FALSE;

  CommonGetVariableDataAndSize (
    L"Setup",
    &gSystemConfigurationGuid,
    &BufferSize,
    (VOID **) &SetupNvData
    );

  if (SetupNvData != NULL) {
    if (SetupNvData->L05WirelessLan == 0) { //disable
      IsWlanDisable = TRUE;
    }
    FreePool (SetupNvData);
  }

  return IsWlanDisable;
}
#endif
//[-end-211103-DABING0020-modify]//

/**
  Update PcieRp pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspsUpm        Pointer to FSPM_UPD
  @param[in] PchSetup       Pointer to PCH_SETUP
**/
STATIC
VOID
UpdatePcieRpPreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy,
  IN  VOID                     *FspmUpd,
  IN  PCH_SETUP                *PchSetup
  )
{
  UINT32                          RpIndex;
  UINT32                          RpEnabledMask;
  PCH_PCIE_RP_PREMEM_CONFIG       *PcieRpPreMemConfig;
  //[-start-190410-IB16990021-add]//
  PORT_CONFIG                     PciePortConfig;
  //[-end-190410-IB16990021-add]//
#if FixedPcdGet8(PcdFspModeSelection) == 0
  EFI_STATUS                      Status;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gPcieRpPreMemConfigGuid, (VOID *) &PcieRpPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#else
  PcieRpPreMemConfig = NULL;
#endif
  //[-start-190410-IB16990021-add]//
  PciePortConfig.Value = PcdGet64 (PcdH2OChipsetPciePortEnable);
  //[-end-190410-IB16990021-add]//
  GET_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PcieRpEnableMask, PcieRpPreMemConfig->RpEnabledMask, RpEnabledMask);

  for (RpIndex = 0; RpIndex < GetPchMaxPciePortNum (); RpIndex ++) {
    //[-start-191111-IB10189001-modify]//
    // if (PchSetup->PcieRootPortEn[RpIndex]) {
    if ((PciePortConfig.Value >> (RpIndex << 1)) & 0x3) {
    //[-end-191111-IB10189001-modify]//
      RpEnabledMask |=  (UINT32) (1 << RpIndex);
    } else {
      RpEnabledMask &= ~(UINT32) (1 << RpIndex);
    }
  }
  // RpEnabledMask value is related with Setup value, Need to check Policy Default
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PcieRpEnableMask, PcieRpPreMemConfig->RpEnabledMask, RpEnabledMask, NullIndex);

  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 0, PcdGet64(PcdPcieClock0));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 1, PcdGet64(PcdPcieClock1));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 2, PcdGet64(PcdPcieClock2));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 3, PcdGet64(PcdPcieClock3));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 4, PcdGet64(PcdPcieClock4));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 5, PcdGet64(PcdPcieClock5));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 6, PcdGet64(PcdPcieClock6));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 7, PcdGet64(PcdPcieClock7));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 8, PcdGet64(PcdPcieClock8));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 9, PcdGet64(PcdPcieClock9));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 10, PcdGet64(PcdPcieClock10));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 11, PcdGet64(PcdPcieClock11));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 12, PcdGet64(PcdPcieClock12));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 13, PcdGet64(PcdPcieClock13));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 14, PcdGet64(PcdPcieClock14));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 15, PcdGet64(PcdPcieClock15));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 16, PcdGet64(PcdPcieClock16));
  UpdatePcieClockInfo (PcieRpPreMemConfig, FspmUpd, 17, PcdGet64(PcdPcieClock17));

//[-start-211102-JAYAN00006-modify]//
//[-start-211102-QINGLIN0108-modify]//
//#ifdef C770_SUPPORT
#if defined(C770_SUPPORT)
//[-end-211102-QINGLIN0108-modify]//
//disable CLKREQ1(wlan) - CLKREQ3(DGPU) 
  for (RpIndex = 0; RpIndex < GetPchMaxPcieClockNum(); RpIndex++) {
   if(((RpIndex == 1) && (CnviIsPresent() || OemSvcLfcIsSetupWlanConfigDisable())) || \
    ((RpIndex == 3) && (OemSvcLfcIsUmaConfig()))){
    UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcClkReq[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReq, (UINT8)PCH_PCIE_NO_SUCH_CLOCK);
    UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageNotUsed);
   } else {
     if(PchSetup->PchPcieClkReqSupport[RpIndex] == 0xFF) {
      UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcClkReq[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReq, (UINT8)PCH_PCIE_NO_SUCH_CLOCK);
     }
     if (PchSetup->PchPcieClockUsageOverride[RpIndex] == 1) {
       UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageUnspecified);
     } else if (PchSetup->PchPcieClockUsageOverride[RpIndex] == 2) {
       UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageNotUsed);
     }
     UPDATE_POLICY(((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkReqGpioMux[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReqGpioPinMux, 0);
   }
  }
//[-start-211102-QINGLIN0108-modify]//
//[-start-211103-DABING0020-modify]//
#elif defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
  for (RpIndex = 0; RpIndex < GetPchMaxPcieClockNum(); RpIndex++) {
    if(((RpIndex == 1) && (CnviIsPresent() || OemSvcLfcIsSetupWlanConfigDisable())) || \
       ((RpIndex == 3) && (OemSvcLfcIsUmaConfig()))) {
     UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcClkReq[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReq, (UINT8)PCH_PCIE_NO_SUCH_CLOCK);
     UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageNotUsed);
    } else {
      if(PchSetup->PchPcieClkReqSupport[RpIndex] == 0xFF) {
       UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcClkReq[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReq, (UINT8)PCH_PCIE_NO_SUCH_CLOCK);
      }
      if (PchSetup->PchPcieClockUsageOverride[RpIndex] == 1) {
        UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageUnspecified);
      } else if (PchSetup->PchPcieClockUsageOverride[RpIndex] == 2) {
        UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageNotUsed);
      }
      UPDATE_POLICY(((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkReqGpioMux[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReqGpioPinMux, 0);
    }
  }
//[-end-211102-QINGLIN0108-modify]//

#elif defined(S77013_SUPPORT)
  for (RpIndex = 0; RpIndex < GetPchMaxPcieClockNum(); RpIndex++) {
    if((RpIndex == 1) && (CnviIsPresent() || OemSvcLfcIsSetupWlanConfigDisable())) {
     UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcClkReq[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReq, (UINT8)PCH_PCIE_NO_SUCH_CLOCK);
     UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageNotUsed);
    } else {
      if(PchSetup->PchPcieClkReqSupport[RpIndex] == 0xFF) {
       UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcClkReq[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReq, (UINT8)PCH_PCIE_NO_SUCH_CLOCK);
      }
      if (PchSetup->PchPcieClockUsageOverride[RpIndex] == 1) {
        UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageUnspecified);
      } else if (PchSetup->PchPcieClockUsageOverride[RpIndex] == 2) {
        UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageNotUsed);
      }
      UPDATE_POLICY(((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkReqGpioMux[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReqGpioPinMux, 0);
    }
  }
//[-end-211103-DABING0020-modify]//  
#else
  for (RpIndex = 0; RpIndex < GetPchMaxPcieClockNum(); RpIndex++) {
    if (PchSetup->PchPcieClkReqSupport[RpIndex] == 0xFF) {
      UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcClkReq[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReq, (UINT8)PCH_PCIE_NO_SUCH_CLOCK);
    }
    if (PchSetup->PchPcieClockUsageOverride[RpIndex] == 1) {
      UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageUnspecified);
    } else if (PchSetup->PchPcieClockUsageOverride[RpIndex] == 2) {
      UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkSrcUsage[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].Usage, PchClockUsageNotUsed);
    }
    UPDATE_POLICY(((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkReqGpioMux[RpIndex], PcieRpPreMemConfig->PcieClock[RpIndex].ClkReqGpioPinMux, 0);
  }
#endif
//[-end-211102-JAYAN00006-modify]//

  //
  // Update Pin Mux details for ClKReq  Gpio
  //
  if (IsPchP ()) {
    UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PcieClkReqGpioMux[9], PcieRpPreMemConfig->PcieClock[9].ClkReqGpioPinMux, GPIO_VER2_P_MUXING_PCIE_CLKREQ_9_E0);
  }
}

/**
  Update HD Audio pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspsUpm        Pointer to FSPM_UPD
  @param[in] PchSetup       Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateHdaPreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy,
  IN  VOID                     *FspmUpd,
  IN  PCH_SETUP                *PchSetup
  )
{
  UINTN                           Index;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  HDAUDIO_PREMEM_CONFIG           *HdaPreMemConfig;
  EFI_STATUS                      Status;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gHdAudioPreMemConfigGuid, (VOID *) &HdaPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaSdiEnable[0], HdaPreMemConfig->AudioLinkHda.SdiEnable[0], TRUE);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaEnable,               HdaPreMemConfig->Enable,               PchSetup->PchHdAudio,                    NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaDspEnable,            HdaPreMemConfig->DspEnable,            PchSetup->PchHdAudioDsp,                 NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaIDispLinkFrequency,   HdaPreMemConfig->IDispLinkFrequency,   PchSetup->PchHdaIDisplayLinkFreq,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaIDispLinkTmode,       HdaPreMemConfig->IDispLinkTmode,       PchSetup->PchHdaIDisplayLinkTmode,       NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaIDispCodecDisconnect, HdaPreMemConfig->IDispCodecDisconnect, PchSetup->PchHdaIDisplayCodecDisconnect, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaDspUaaCompliance,     HdaPreMemConfig->DspUaaCompliance,     PchSetup->PchHdAudioDspUaaCompliance,    NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkHdaEnable,   HdaPreMemConfig->AudioLinkHda.Enable,  PchSetup->PchHdAudioHdaLinkEnable,       NullIndex);
  for (Index = 0; Index < GetPchHdaMaxDmicLinkNum (); Index++) {
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkDmicEnable[Index],      HdaPreMemConfig->AudioLinkDmic[Index].Enable,          PchSetup->PchHdAudioDmicLinkEnable[Index],  Index);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkDmicClockSelect[Index], HdaPreMemConfig->AudioLinkDmic[Index].DmicClockSelect, PchSetup->PchHdAudioDmicClockSelect[Index], Index);
  }
  for (Index = 0; Index < GetPchHdaMaxSspLinkNum (); Index++) {
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkSspEnable[Index],  HdaPreMemConfig->AudioLinkSsp[Index].Enable,  PchSetup->PchHdAudioSspLinkEnable[Index],  Index);
  }
  for (Index = 0; Index < GetPchHdaMaxSndwLinkNum (); Index++) {
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkSndwEnable[Index], HdaPreMemConfig->AudioLinkSndw[Index].Enable, PchSetup->PchHdAudioSndwLinkEnable[Index], Index);
  }

  if (IsAdlPch () && IsPchLp ()) {
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkDmicClkAPinMux[0], HdaPreMemConfig->AudioLinkDmic[0].PinMux.ClkA, GPIO_VER2_LP_MUXING_DMIC0_CLKA_GPP_S2);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkDmicClkBPinMux[0], HdaPreMemConfig->AudioLinkDmic[0].PinMux.ClkB, GPIO_VER2_LP_MUXING_DMIC1_CLKA_GPP_S6);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkDmicDataPinMux[0], HdaPreMemConfig->AudioLinkDmic[0].PinMux.Data, GPIO_VER2_LP_MUXING_DMIC0_DATA_GPP_S3);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkDmicClkAPinMux[1], HdaPreMemConfig->AudioLinkDmic[1].PinMux.ClkA, GPIO_VER2_LP_MUXING_DMIC1_CLKA_GPP_S6);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkDmicClkBPinMux[1], HdaPreMemConfig->AudioLinkDmic[1].PinMux.ClkB, GPIO_VER2_LP_MUXING_DMIC1_CLKB_GPP_S5);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchHdaAudioLinkDmicDataPinMux[1], HdaPreMemConfig->AudioLinkDmic[1].PinMux.Data, GPIO_VER2_LP_MUXING_DMIC1_DATA_GPP_S7);
  }
}

/**
  Update ISH pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspsUpm        Pointer to FSPM_UPD
  @param[in] PchSetup       Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateIshPreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy,
  IN  VOID                     *FspmUpd,
  IN  PCH_SETUP                *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  ISH_PREMEM_CONFIG               *IshPreMemConfig;
  EFI_STATUS                      Status;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gIshPreMemConfigGuid, (VOID *) &IshPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchIshEnable, IshPreMemConfig->Enable, PchSetup->PchIshEnable);
}

/**
  Update CNVi pre mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspsUpm        Pointer to FSPM_UPD
  @param[in] PchSetup       Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateCnviPreMemPolicy (
  IN  SI_PREMEM_POLICY_PPI     *SiPreMemPolicy,
  IN  VOID                     *FspmUpd,
  IN  PCH_SETUP                *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  CNVI_PREMEM_CONFIG           *CnviPreMemConfig;
  EFI_STATUS                   Status;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gCnviPreMemConfigGuid, (VOID *) &CnviPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CnviDdrRfim, CnviPreMemConfig->DdrRfim, PchSetup->CnviDdrRfim, NullIndex);
}

/**
  Update Dmi Pre Mem policies.

  @param[in] SiPreMemPolicy Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspmUpd        Pointer to FSPM_UPD
  @param[in] SetupData      Pointer to SETUP_DATA
  @param[in] PchSetup       Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateDmiPreMemPolicy (
  IN SI_PREMEM_POLICY_PPI *SiPreMemPolicy,
  IN VOID                 *FspmUpd,
  IN SETUP_DATA           *SetupVariables
  )
{
  UINTN                           Index;
  DEBUG ((DEBUG_INFO, "UpdateDmiPreMemPolicy \n"));

#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_DMI_PREMEM_CONFIG           *DmiPreMemConfig;
  EFI_STATUS                      Status;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gPchDmiPreMemConfigGuid, (VOID *) &DmiPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiHweq, DmiPreMemConfig->DmiHweq, SetupVariables->DmiHweq, NullIndex);

  for (Index = 0; Index < PCH_MAX_DMI_LANES; Index++) {
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchDmiHwEqGen3CoeffListCm[Index],   DmiPreMemConfig->DmiHwEqGen3CoeffList[Index].Cm,       SetupVariables->PchDmiGen3RtcoCpre[Index], NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PchDmiHwEqGen3CoeffListCp[Index],   DmiPreMemConfig->DmiHwEqGen3CoeffList[Index].Cp,       SetupVariables->PchDmiGen3RtcoCpo[Index], NullIndex);
  }
}

/**
  This function performs PCH PEI Policy initialization.

  @retval EFI_SUCCESS             The PPI is installed and initialized.
  @retval EFI ERRORS              The PPI is not successfully installed.
  @retval EFI_OUT_OF_RESOURCES    Do not have enough resources to initialize the driver
**/
EFI_STATUS
EFIAPI
UpdatePeiPchPolicyPreMem (
  VOID
  )
{
  EFI_STATUS                      Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  UINTN                           VariableSize;
  PCH_SETUP                       PchSetup;
  SETUP_DATA                      SetupVariables;
  VOID                            *FspmUpd;
  SI_PREMEM_POLICY_PPI            *SiPreMemPolicy;

  DEBUG ((DEBUG_INFO, "Update PeiPchPolicyUpdate Pre-Mem Start\n"));

  FspmUpd        = NULL;
  SiPreMemPolicy = NULL;

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = (FSPM_UPD *) PcdGet32 (PcdFspmUpdDataAddress);
  ASSERT (FspmUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicy);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

  //
  // Retrieve Setup variable
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid, // GUID
             0,                                // INSTANCE
             NULL,                             // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices       // PPI
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SetupVariables
                               );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (PCH_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"PchSetup",
                               &gPchSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &PchSetup
                               );
  ASSERT_EFI_ERROR (Status);

  UpdatePchGeneralPreMemPolicy (SiPreMemPolicy, FspmUpd, &PchSetup, &SetupVariables);
  UpdateDciPreMemPolicy (SiPreMemPolicy, FspmUpd, &PchSetup);
  UpdatePchTraceHubPreMemPolicy (SiPreMemPolicy, FspmUpd, &PchSetup);
  UpdateSmbusPreMemPolicy (SiPreMemPolicy, FspmUpd, &PchSetup);
  UpdateLpcPreMemPolicy (SiPreMemPolicy, FspmUpd, &PchSetup);
  UpdateWatchDogPreMemPolicy (SiPreMemPolicy, FspmUpd, &PchSetup);
  UpdatePcieRpPreMemPolicy (SiPreMemPolicy, FspmUpd, &PchSetup);
  UpdateHdaPreMemPolicy (SiPreMemPolicy, FspmUpd, &PchSetup);
  UpdateIshPreMemPolicy (SiPreMemPolicy, FspmUpd, &PchSetup);
  UpdateCnviPreMemPolicy (SiPreMemPolicy, FspmUpd, &PchSetup);
  UpdateDmiPreMemPolicy (SiPreMemPolicy, FspmUpd, &SetupVariables);
//[-start-211108-IB18410125-add]//
  UpdateHsioPciePreMemPolicy (SiPreMemPolicy);
//[-end-211108-IB18410125-add]//
  return EFI_SUCCESS;
}
