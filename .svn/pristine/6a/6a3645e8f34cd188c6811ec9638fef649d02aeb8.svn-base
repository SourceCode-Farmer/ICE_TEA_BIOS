/** @file
  This function provides an interface to hook GenericRouteConfig.
   
;******************************************************************************
;* Copyright (c) 2015-2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeOemSvcChipsetLib.h>
#include <Guid/RcSetupUtility.h>
#include <SetupVariable.h>
#include <MeSetup.h>

/**
  This function provides an interface to hook GenericRouteConfig.

  @param[in,out]     ScBuffer            A pointer to CHIPSET_CONFIGURATION struct.
  @param[in]         BufferSize          System configuration size.
  @param[in,out]     RcScBuffer          A pointer to RC relative variables struct.
  @param[in]         ScuRecord           The bit mask of the currently SCU record.
                                           Bit 0 = 1 (SCU_ACTION_LOAD_DEFAULT), It indicates system do load default action.

  @retval            EFI_UNSUPPORTED     This function is a pure hook; Chipset code don't care return status.
  @retval            EFI_SUCCESS         This function is a pure hook; Chipset code don't care return status. 
**/
EFI_STATUS
OemSvcHookRouteConfig (
  IN OUT CHIPSET_CONFIGURATION          *ScBuffer,
  IN     UINT32                         BufferSize,
  IN OUT VOID                           *RcScBuffer,
  IN     UINT32                         ScuRecord
  )
{
  RC_SETUP_UTILITY_BROWSER_DATA         *RcSUBData;
  SA_SETUP                              *SaScBuffer;
  UINTN                                 SaScBufferSize;
  ME_SETUP                              *MeScBuffer;
  UINTN                                 MeScBufferSize;
  CPU_SETUP                             *CpuScBuffer;
  UINTN                                 CpuScBufferSize;
  PCH_SETUP                             *PchScBuffer;
  UINTN                                 PchScBufferSize;
  SI_SETUP                             *SiScBuffer;
  UINTN                                 SiScBufferSize;
  ME_SETUP_STORAGE                      *MeStorageScBuffer;
  UINTN                                 MeStorageScBufferSize;

  RcSUBData         = (RC_SETUP_UTILITY_BROWSER_DATA *)RcScBuffer;
  SaScBuffer        = (SA_SETUP *)RcSUBData->SaSUBrowserData;
  MeScBuffer        = (ME_SETUP *)RcSUBData->MeSUBrowserData;
  CpuScBuffer       = (CPU_SETUP *)RcSUBData->CpuSUBrowserData;
  PchScBuffer       = (PCH_SETUP *)RcSUBData->PchSUBrowserData;
  SiScBuffer        = (SI_SETUP *)RcSUBData->SiSUBrowserData;
  MeStorageScBuffer = (ME_SETUP_STORAGE *)RcSUBData->MeStorageSUBrowserData;

  SaScBufferSize        = RcSUBData->SaSUBDataSize;
  MeScBufferSize        = RcSUBData->MeSUBDataSize;
  CpuScBufferSize       = RcSUBData->CpuSUBDataSize;
  PchScBufferSize       = RcSUBData->PchSUBDataSize;
  SiScBufferSize       = RcSUBData->SiSUBDataSize;
  MeStorageScBufferSize = RcSUBData->MeStorageSUBDataSize;

  /*++
    Todo:
      Add project specific code in here.
  --*/

  return EFI_UNSUPPORTED;
}
