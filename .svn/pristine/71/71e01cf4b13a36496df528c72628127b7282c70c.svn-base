//*****************************************************************************
//
// Copyright (c) 2012 - 2017, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.
// 
//******************************************************************************
// Project Dxe Common code here

#include <Uefi.h>
#include <Guid/EventGroup.h>
#include <Guid/LfcVariableGuid.h>
#include <Guid/EfiSystemResourceTable.h>
#include <Library/UefiLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/LfcLib.h>
#include <Library/DebugLib.h>
#include <Library/VariableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DevicePathLib.h>
#include <Library/HidDescriptorLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Protocol/I2cIo.h>
#include <Protocol/LfcNvsArea.h>
#include <Protocol/SimplePointer.h>
#include <Protocol/AdapterInformation.h>
#include <Protocol/EndOfBdsBootSelection.h>
#include <I2cHidDeviceInfo.h>
#include <Bus/I2c/I2cDxe/I2cDxe.h>
#include <Bus/I2c/I2cDxe/I2cEnum.h>
#include <I2cTPGetVidDxe.h>
//[-start-210920-Ching000003-add]//
//[-start-210930-YUNLEI0141-modify]//
//[-start-211015-Ching000014-modify]//
//[-start-220127-Dennis0013-modify]//
//[-start-220209-OWENWU0037-modify]//
#if defined(S77014_SUPPORT) || defined(C770_SUPPORT) || defined(C970_SUPPORT) || defined(S77013_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
#include <Protocol/LfcProjectTouchPadTypeProtocol.h>
#endif
//[-end-220209-OWENWU0037-modify]//
//[-end-220127-Dennis0013-modify]//
//[-end-211015-Ching000014-modify]//
//[-end-210930-YUNLEI0141-modify]//
//[-end-210920-Ching000003-add]//

#define ESRT_LAST_ATTEMPT_VERSION       L"EsrtLastAttemptVersion"
#define ESRT_LAST_ATTEMPT_STATUS        L"EsrtLastAttemptStatus"
#define LFC_MOUSE_CLASS                 0x0102
#define LFC_TOUCH_PANEL_CLASS           0x0d00

//[-start-210920-Ching000003-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT) 
#define ELAN 1
#define SYNA 2

EFI_HANDLE                mTouchPadHandle             = NULL;

/*
This function is to set Touch Pad Type variable data for ELAN firmware issue
*/
EFI_STATUS
SetTouchPadTypeProtocol(
  IN  UINT8 TouchPadType
)
{  
  EFI_STATUS               Status;
  UINT16                   TouchPadPID;
  LFC_NVS_AREA_PROTOCOL    *LfcNvsAreaProtocol;
  TouchPadPID               = 0;

	Status = gBS->LocateProtocol (&gLfcNvsAreaProtocolGuid, NULL, (VOID **)&LfcNvsAreaProtocol);
  if (!EFI_ERROR(Status)) {
    TouchPadPID =	LfcNvsAreaProtocol->Area->TouchPadPID;
	}
  TouchPadPID &= 0XFF ;
	switch(TouchPadType)
  {
    case ELAN:
      //
      // A workaround: Here we install a dummy ELAN handle
      //
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadElanProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
      break;
    case SYNA:
      //
      // A workaround: Here we install a dummy SYNA handle
      //
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadSynaProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
      break;
    default:
      // Do Nothing
      break;
  }
  return Status;
}

//[-start-210930-YUNLEI0141-modify]//
#elif defined(C770_SUPPORT) 
#define ELAN 1
#define SYNA 2

EFI_HANDLE                mTouchPadHandle             = NULL;

/*
This function is to set Touch Pad Type variable data for ELAN firmware issue
*/
EFI_STATUS
SetTouchPadTypeProtocol(
  IN  UINT8 TouchPadType
)
{  
  EFI_STATUS               Status;
  UINT16                   TouchPadPID;
  LFC_NVS_AREA_PROTOCOL    *LfcNvsAreaProtocol;
  TouchPadPID               = 0;

  Status = gBS->LocateProtocol (&gLfcNvsAreaProtocolGuid, NULL, (VOID **)&LfcNvsAreaProtocol);
  if (!EFI_ERROR(Status)) {
    TouchPadPID = LfcNvsAreaProtocol->Area->TouchPadPID;
  }
  TouchPadPID &= 0XFF ;
  switch(TouchPadType)
  {
    case ELAN:
      //
      // A workaround: Here we install a dummy ELAN handle
      //
      if(TouchPadPID == 0x7C) {
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadElanProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
      }
  else{
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadElan1ProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
    }
      break;
    case SYNA:
      //
      // A workaround: Here we install a dummy SYNA handle
      //
      if(TouchPadPID == 0x44) {
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadSynaProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
      }
      else{
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadSyna1ProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
      }
      break;
    default:
      // Do Nothing
      break;
  }
  return Status;
}
#elif defined(C970_SUPPORT) 
#define ELAN 1
#define SYNA 2

EFI_HANDLE                mTouchPadHandle             = NULL;

/*
This function is to set Touch Pad Type variable data for ELAN firmware issue
*/
EFI_STATUS
SetTouchPadTypeProtocol(
  IN  UINT8 TouchPadType
)
{  
  EFI_STATUS               Status;
  UINT16                   TouchPadPID;
  LFC_NVS_AREA_PROTOCOL    *LfcNvsAreaProtocol;
  TouchPadPID               = 0;

  Status = gBS->LocateProtocol (&gLfcNvsAreaProtocolGuid, NULL, (VOID **)&LfcNvsAreaProtocol);
  if (!EFI_ERROR(Status)) {
    TouchPadPID = LfcNvsAreaProtocol->Area->TouchPadPID;
  }
  TouchPadPID &= 0XFF ;
  switch(TouchPadType)
  {
    case ELAN:
      //
      // A workaround: Here we install a dummy ELAN handle
      //
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadElan1ProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
      break;
    case SYNA:
      //
      // A workaround: Here we install a dummy SYNA handle
      //
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadSyna1ProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
      break;
    default:
      // Do Nothing
      break;
  }
  return Status;
}
//[-end-210930-YUNLEI0141-modify]//

//[-start-211015-Ching000014-modify]//
#elif defined(S77013_SUPPORT)
#define ELAN 1
#define SYNA 2

EFI_HANDLE                mTouchPadHandle             = NULL;

/*
This function is to set Touch Pad Type variable data for ELAN firmware issue
*/
EFI_STATUS
SetTouchPadTypeProtocol(
  IN  UINT8 TouchPadType
)
{  
  EFI_STATUS               Status;
  UINT16                   TouchPadPID;
  LFC_NVS_AREA_PROTOCOL    *LfcNvsAreaProtocol;
  TouchPadPID               = 0;

  Status = gBS->LocateProtocol (&gLfcNvsAreaProtocolGuid, NULL, (VOID **)&LfcNvsAreaProtocol);
  if (!EFI_ERROR(Status)) {
    TouchPadPID = LfcNvsAreaProtocol->Area->TouchPadPID;
  }
  TouchPadPID &= 0XFF ;
  switch(TouchPadType)
  {
    case ELAN:
      //
      // A workaround: Here we install a dummy ELAN handle
      //
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadElan2ProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
      break;
    case SYNA:
      //
      // A workaround: Here we install a dummy SYNA handle
      //
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadSyna2ProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
      break;
    default:
      // Do Nothing
      break;
  }
  return Status;
}
//[-start-220127-Dennis0013-modify]//
#elif defined(S370_SUPPORT)
#define ELAN   1
#define SYNA   2
#define CIRQUE 4
#define FOCAL  5

EFI_HANDLE                mTouchPadHandle             = NULL;

/*
This function is to set Touch Pad Type variable data for ELAN firmware issue
*/
EFI_STATUS
SetTouchPadTypeProtocol(
  IN  UINT8 TouchPadType
)
{  
  EFI_STATUS               Status;
  UINT16                   TouchPadPID;
  LFC_NVS_AREA_PROTOCOL    *LfcNvsAreaProtocol;
  TouchPadPID               = 0;

  Status = gBS->LocateProtocol (&gLfcNvsAreaProtocolGuid, NULL, (VOID **)&LfcNvsAreaProtocol);
  if (!EFI_ERROR(Status)) {
    TouchPadPID = LfcNvsAreaProtocol->Area->TouchPadPID;
  }
  TouchPadPID &= 0XFF ;
  switch(TouchPadType)
  {
    case ELAN:
      //
      // A workaround: Here we install a dummy ELAN handle
      //
      if(TouchPadPID == 0xB8) {
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadElan3ProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
        }
//[-start-220224-Dennis0015-modify]//
      else if(TouchPadPID == 0xBE){
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadElan4ProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
      }
      else if(TouchPadPID == 0x48){
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadElan5ProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
      }
      else {  //PID:0x3247
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadElan6ProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
      }
      break;
//[-end-220224-Dennis0015-modify]//
    case SYNA:
      //
      // A workaround: Here we install a dummy SYNA handle
      //
        if(TouchPadPID == 0x2C) {
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadSyna3ProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
          }
        else{
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadSyna4ProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
        }
      break;
    case CIRQUE:
      //
      // A workaround: Here we install a dummy CIRQUE handle
      //
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadCirqueProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
      break;
    case FOCAL:
      //
      // A workaround: Here we install a dummy FOCAL handle
      //
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadFocalProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
      break;
    default:
      // Do Nothing
      break;
  }
  return Status;
}
//[-end-220127-Dennis0013-modify]//
//[-start-220209-OWENWU0037-modify]//
#elif defined(S570_SUPPORT)
#define ELAN 1
#define SYNA 2
#define CIRQUE 4
#define FOCAL  5

EFI_HANDLE                mTouchPadHandle             = NULL;

/*
This function is to set Touch Pad Type variable data for ELAN firmware issue
*/
EFI_STATUS
SetTouchPadTypeProtocol(
  IN  UINT8 TouchPadType
)
{  
  EFI_STATUS               Status;
  UINT16                   TouchPadPID;
  LFC_NVS_AREA_PROTOCOL    *LfcNvsAreaProtocol;
  TouchPadPID               = 0;

  Status = gBS->LocateProtocol (&gLfcNvsAreaProtocolGuid, NULL, (VOID **)&LfcNvsAreaProtocol);
  if (!EFI_ERROR(Status)) {
    TouchPadPID = LfcNvsAreaProtocol->Area->TouchPadPID;
  }
  TouchPadPID &= 0XFF ;
  switch(TouchPadType)
  {
    case ELAN:
      //
      // A workaround: Here we install a dummy ELAN handle
      //
      Status = gBS->InstallProtocolInterface (
                      &mTouchPadHandle,
                      &gLfcProjectTouchPadElan4ProtocolGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
      break;
    case SYNA:
      //
      // A workaround: Here we install a dummy SYNA handle
      //
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadSyna4ProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
      break;
    case CIRQUE:
      //
      // A workaround: Here we install a dummy CIRQUE handle
      //
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadCirqueProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
      break;
    case FOCAL:
      //
      // A workaround: Here we install a dummy FOCAL handle
      //
        Status = gBS->InstallProtocolInterface (
                        &mTouchPadHandle,
                        &gLfcProjectTouchPadFocalProtocolGuid,
                        EFI_NATIVE_INTERFACE,
                        NULL
                        );
      break;
    default:
      // Do Nothing
      break;
  }
  return Status;
}
//[-end-220209-OWENWU0037-modify]//
//[-end-211015-Ching000014-modify]//

#endif
//[-end-210920-Ching000003-add]//

EFI_STATUS
EFIAPI
LfcI2cTPCreateEsrtEntry (
  IN  UINT8  I2cTouchPadIndex,
  IN  UINT16 I2cTouchPadVersionID
  )
{
  EFI_STATUS                    Status;
  EFI_SYSTEM_RESOURCE_TABLE     *Esrt;
  UINTN                         Size;
  UINT32                        TPFwVersion;

  UINT32                        LastAttemptTPVersion;
  ESRT_STATUS                   LastAttemptTPStatus;
//[-start-210920-Ching000003-add]//
//[-start-210930-YUNLEI0141-modify]//
//[-start-211015-Ching000014-modify]//
//[-start-220127-Dennis0013-modify]//
//[-start-220209-OWENWU0037-modify]//
#if defined(S77014_SUPPORT) || defined(C770_SUPPORT) || defined(C970_SUPPORT) || defined(S77013_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
  UINT16                        TouchPadPID;
  LFC_NVS_AREA_PROTOCOL         *LfcNvsAreaProtocol;
#endif
//[-end-220209-OWENWU0037-modify]//
//[-end-220127-Dennis0013-modify]//
//[-end-211015-Ching000014-modify]//
//[-end-210930-YUNLEI0141-modify]//
//[-end-210920-Ching000003-add]//
  
  EFI_GUID LcfcI2cTouchPadGuidTable[] = {
    ZERO_TP_FIRMWARE_GUID, // Non I2c TouchPad Guid
    ZERO_TP_FIRMWARE_GUID  // I2c TouchPad Guid
  };

  //
  // Local Variable Initialize
  //
  Status                     = EFI_SUCCESS;
  Esrt                       = NULL;
  Size                       = 0x00;
  TPFwVersion                = 0x00;
  LastAttemptTPVersion       = 0x00;
  LastAttemptTPStatus        = 0x00;
//[-start-210920-Ching000003-add]//
//[-start-210930-YUNLEI0141-modify]//
//[-start-211015-Ching000014-modify]//
//[-start-220127-Dennis0013-modify]//
//[-start-220209-OWENWU0037-modify]//
#if defined(S77014_SUPPORT) || defined(C770_SUPPORT) || defined(C970_SUPPORT) || defined(S77013_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
  TouchPadPID                = 0;

  SetTouchPadTypeProtocol(I2cTouchPadIndex);

  Status = gBS->LocateProtocol (&gLfcNvsAreaProtocolGuid, NULL, (VOID **)&LfcNvsAreaProtocol);
  if (!EFI_ERROR(Status)) {
    TouchPadPID =  LfcNvsAreaProtocol->Area->TouchPadPID;
  }
  TouchPadPID &= 0XFF;
#endif
//[-end-220209-OWENWU0037-modify]//
//[-end-220127-Dennis0013-modify]//
//[-end-211015-Ching000014-modify]//
//[-end-210930-YUNLEI0141-modify]//
//[-end-210920-Ching000003-add]//
//[-start-210930-YUNLEI0141-modify]//
#if defined(C770_SUPPORT) 
  switch (I2cTouchPadIndex) {
    case 1:  // ELAN
      if(TouchPadPID == 0x7C) {
        CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsELANTPFirmwareCapsuleGuid);
      }
      else{
        CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsELAN1TPFirmwareCapsuleGuid);
      }
      break;
    case 2:  // SYNA
      if(TouchPadPID == 0x44) {
        CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsSYNATPFirmwareCapsuleGuid);
      }else{
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsSYNA1TPFirmwareCapsuleGuid);
      }
      break;
    case 3:  // ALPS
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsALPSTPFirmwareCapsuleGuid);
      break;
    default:
      // Do Nothing
      break;
  }
#elif defined(C970_SUPPORT) 
   switch (I2cTouchPadIndex) {
    case 1:  // ELAN
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsELAN1TPFirmwareCapsuleGuid);
      break;
    case 2:  // SYNA
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsSYNA1TPFirmwareCapsuleGuid);
      break;
    case 3:  // ALPS
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsALPSTPFirmwareCapsuleGuid);
      break;
    default:
      // Do Nothing
      break;
  }
//[-start-211015-Ching000014-modify]//
#elif defined(S77013_SUPPORT) 
  switch (I2cTouchPadIndex) {
    case 1:  // ELAN
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsELAN2TPFirmwareCapsuleGuid);
      break;
    case 2:  // SYNA
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsSYNA2TPFirmwareCapsuleGuid);
      break;
    case 3:  // ALPS
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsALPSTPFirmwareCapsuleGuid);
      break;
    default:
      // Do Nothing
      break;
  }
//[-end-211015-Ching000014-modify]//
//[-start-220127-Dennis0013-modify]//
#elif defined(S370_SUPPORT) 
  switch (I2cTouchPadIndex) {
    case 1:  // ELAN
      if(TouchPadPID == 0xB8) {
        CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsELAN3TPFirmwareCapsuleGuid);
      }
//[-start-220224-Dennis0015-modify]//
      else if(TouchPadPID == 0xBE){
        CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsELAN4TPFirmwareCapsuleGuid);
      }
      else if(TouchPadPID == 0x48){
        CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsELAN5TPFirmwareCapsuleGuid);
      }
      else {
        CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsELAN6TPFirmwareCapsuleGuid);
      }
      break;
//[-end-220224-Dennis0015-modify]//
    case 2:  // SYNA
      if(TouchPadPID == 0x2C) {
        CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsSYNA3TPFirmwareCapsuleGuid);
      }else{
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsSYNA4TPFirmwareCapsuleGuid);
      }
      break;
    case 4:  // CIRQUE
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsCIRQUETPFirmwareCapsuleGuid);
      break;
    case 5:  // FOCAL
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsFOCALTPFirmwareCapsuleGuid);
      break;
    default:
      // Do Nothing
      break;
  }
//[-end-220127-Dennis0013-modify]//
//[-start-220209-OWENWU0037-modify]//
#elif defined(S570_SUPPORT) 
  switch (I2cTouchPadIndex) {
    case 1:  // ELAN
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsELAN4TPFirmwareCapsuleGuid);
      break;
    case 2:  // SYNA
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsSYNA4TPFirmwareCapsuleGuid);
      break;
    case 4:  // CIRQUE
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsCIRQUETPFirmwareCapsuleGuid);
      break;
    case 5:  // FOCAL
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsFOCALTPFirmwareCapsuleGuid);
      break;
    default:
      // Do Nothing
      break;
  }
//[-end-220209-OWENWU0037-modify]//  
#else 
   switch (I2cTouchPadIndex) {
    case 1:  // ELAN
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsELANTPFirmwareCapsuleGuid);
      break;
    case 2:  // SYNA
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsSYNATPFirmwareCapsuleGuid);
      break;
    case 3:  // ALPS
      CopyGuid(&LcfcI2cTouchPadGuidTable[1], &gWindowsALPSTPFirmwareCapsuleGuid);
      break;
    default:
      // Do Nothing
      break;
  }
#endif
//[-end-210930-YUNLEI0141-modify]//

  if(CompareGuid(&LcfcI2cTouchPadGuidTable[0], &LcfcI2cTouchPadGuidTable[1])) {
	  return EFI_UNSUPPORTED;  
  }

  EfiGetSystemConfigurationTable (&gEfiSystemResourceTableGuid, (VOID **)&Esrt);
  if (Esrt == NULL) {
    return EFI_UNSUPPORTED;
  }

  Size = sizeof(UINT32);
  Status = CommonGetVariable (
             ESRT_LAST_ATTEMPT_VERSION,
             &Esrt->FirmwareResources[Esrt->FirmwareResourceCount].FirmwareClass,
             &Size,
             &LastAttemptTPVersion
             );

  Size = sizeof(ESRT_STATUS);
  Status = CommonGetVariable (
             ESRT_LAST_ATTEMPT_STATUS,
             &Esrt->FirmwareResources[Esrt->FirmwareResourceCount].FirmwareClass,
             &Size,
             &LastAttemptTPStatus
             );

  Esrt->FirmwareResources[Esrt->FirmwareResourceCount].FirmwareClass                   = LcfcI2cTouchPadGuidTable[1];
  Esrt->FirmwareResources[Esrt->FirmwareResourceCount].FirmwareType                    = DEVICE_FIRMWARE_TYPE;
  Esrt->FirmwareResources[Esrt->FirmwareResourceCount].FirmwareVersion                 = I2cTouchPadVersionID;
  Esrt->FirmwareResources[Esrt->FirmwareResourceCount].LowestSupportedFirmwareVersion  = TPFwVersion;
  Esrt->FirmwareResources[Esrt->FirmwareResourceCount].CapsuleFlags                    = 0x8010;
  Esrt->FirmwareResources[Esrt->FirmwareResourceCount].LastAttemptVersion              = LastAttemptTPVersion;
  Esrt->FirmwareResources[Esrt->FirmwareResourceCount].LastAttemptStatus               = LastAttemptTPStatus;
  Esrt->FirmwareResourceCount                                                          = Esrt->FirmwareResourceCount + 1;

  //
  // Add an ESRT into configuration table
  //
  Status = gBS->InstallConfigurationTable (&gEfiSystemResourceTableGuid, Esrt);

  return EFI_SUCCESS;
}


VOID
EFIAPI
I2cTPGetVidDxeCallback (
  IN     EFI_EVENT      Event,
  IN     VOID           *Context
  )
{
  EFI_STATUS               Status = EFI_SUCCESS;
  UINTN                    Index;
  UINTN                    HandleCount;
  EFI_HANDLE               *HandleBuffer;
  EFI_I2C_IO_PROTOCOL      *I2cIo;
  I2C_DEVICE_CONTEXT       *I2cDeviceContext;
  H2O_I2C_DEVICE_CONTEXT   *I2cDev;

  UINT16                   VendorID;
  LFC_NVS_AREA_PROTOCOL    *LfcNvsAreaProtocol;
  UINT8                    I2cTouchPadIndex;
  UINT16                   I2cTouchPadVersionID;

  I2cTouchPadIndex         = 0;
  I2cTouchPadVersionID     = 0;
  HandleCount              = 0;
  VendorID                 = 0;
  HandleBuffer             = NULL;
  I2cIo                    = NULL;
  I2cDeviceContext         = NULL;
  I2cDev                   = NULL;

  gBS->CloseEvent (Event);

  Status = gBS->LocateProtocol (&gLfcNvsAreaProtocolGuid, NULL, (VOID **)&LfcNvsAreaProtocol);

  if (EFI_ERROR(Status)) {
    return;
  }

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiI2cIoProtocolGuid,
                  NULL,
                  &HandleCount,
                  &HandleBuffer
                  );

  if (EFI_ERROR(Status)) {
    return;
  }

  for (Index = 0; Index < HandleCount; Index++) {
      Status = gBS->HandleProtocol (
                      HandleBuffer[Index],
                      &gEfiI2cIoProtocolGuid,
                      (VOID **) &I2cIo
                    );
      if (EFI_ERROR (Status)) {
        Status = EFI_UNSUPPORTED;
        continue;
      } 

      I2cDeviceContext = I2C_DEVICE_CONTEXT_FROM_PROTOCOL (I2cIo);
      I2cDev = H2O_I2C_DEVICE_CONTEXT_FROM_I2C_DEVICE (I2cDeviceContext->I2cDevice);

      if ((I2cDev->I2cHidDevice.ClassType != LFC_MOUSE_CLASS) && 
          (I2cDev->I2cHidDevice.ClassType != LFC_TOUCH_PANEL_CLASS)) {
        continue;
      }

      if (!I2cDev->I2cHidDevice.VendorID) {
        continue;
      }

      switch (I2cDev->I2cHidDevice.ClassType) {
        case LFC_MOUSE_CLASS:
            VendorID = (UINT8) (I2cDev->I2cHidDevice.VendorID & 0xFF);
            LfcNvsAreaProtocol->Area->TouchPadPID  = I2cDev->I2cHidDevice.ProductID;
            switch (VendorID) {
              case 0xF3:  // 1: ELAN
                LfcNvsAreaProtocol->Area->TouchPadType = 0x01;
                break;
              case 0xCB:  // 2: SYNA
                LfcNvsAreaProtocol->Area->TouchPadType = 0x02;
                break;
              case 0x4E:  // 3: ALPS
                LfcNvsAreaProtocol->Area->TouchPadType = 0x03;
                break;
//[-start-210916-xinwei0002-add]//
//[-start-210919-QINGLIN0074-modify]//
#if defined(S370_SUPPORT) || defined(S570_SUPPORT)
              case 0x88:  // 4: Cirque
                LfcNvsAreaProtocol->Area->TouchPadType = 0x04;
                break;
              case 0x08:  // 5: Focal
                LfcNvsAreaProtocol->Area->TouchPadType = 0x05;
                break;
#endif
//[-end-210919-QINGLIN0074-modify]//
//[-end-210916-xinwei0002-add]//
              default:
                LfcNvsAreaProtocol->Area->TouchPadType = 0x00;
                LfcNvsAreaProtocol->Area->TouchPadPID  = 0x00;
                break;
            }
            I2cTouchPadIndex     = (UINT8) LfcNvsAreaProtocol->Area->TouchPadType;
            I2cTouchPadVersionID = I2cDev->I2cHidDevice.VersionID;
          break;
        case LFC_TOUCH_PANEL_CLASS:
            VendorID  = (UINT8)(I2cDev->I2cHidDevice.VendorID & 0xFF);
            LfcNvsAreaProtocol->Area->TouchPnLPID = I2cDev->I2cHidDevice.ProductID;
            switch (VendorID) {
              case 0x6A:  // WACOM
                LfcNvsAreaProtocol->Area->TouchPnLType = 0xA1;
                break;
//[-start-210916-xinwei0002-add]// 
//[-start-210919-QINGLIN0074-modify]//
//[-start-210922-GEORGE0005-A-modify]//
//[-start-210922-QINGLIN0078-modify]//
//[-start-211110-GEORGE0020-modify]//
#if defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211110-GEORGE0020-modify]//
//[-end-210922-QINGLIN0078-modify]//
//[-end-210922-GEORGE0005-A-modify]//
              case 0x94:  // BOE
                LfcNvsAreaProtocol->Area->TouchPnLType = 0xA2;
                break;
              case 0xF3:  // IVO/AUO/INX
                LfcNvsAreaProtocol->Area->TouchPnLType = 0xA3;
                break;
#endif
//[-end-210919-QINGLIN0074-modify]//
//[-end-210916-xinwei0002-add]//
              default:
                LfcNvsAreaProtocol->Area->TouchPnLType = 0x00;
                LfcNvsAreaProtocol->Area->TouchPnLPID  = 0x00;    
                break;
            }
          break;
      }

  }

  if (HandleBuffer) {
    gBS->FreePool (HandleBuffer);
    HandleBuffer = NULL;
  }

  if (PcdGetBool (PcdI2cPtpWufuSupport)) {
    Status = LfcI2cTPCreateEsrtEntry(I2cTouchPadIndex, I2cTouchPadVersionID);
  }
}


EFI_STATUS
EFIAPI
I2cTPGetVidDxeEntryPoint (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
  )
{
  EFI_STATUS  Status;
  EFI_EVENT   I2cTPGetVidEvent;
  VOID        *I2cTPGetVidReg;

  Status           = EFI_SUCCESS;
  I2cTPGetVidEvent = NULL;
  I2cTPGetVidReg   = NULL;

  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  I2cTPGetVidDxeCallback,
                  NULL,
                  &I2cTPGetVidEvent
                  );

  if (!EFI_ERROR (Status)) {
    Status = gBS->RegisterProtocolNotify (
                    &gEndOfBdsBootSelectionProtocolGuid,
                    I2cTPGetVidEvent,
                    &I2cTPGetVidReg
                    );
  }

  return Status;
}
