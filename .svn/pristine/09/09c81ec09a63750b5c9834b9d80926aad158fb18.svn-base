/** @file

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
### @file
# Module Information file for PEI PolicyUpdateLib Library
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2017 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = PeiPolicyUpdateLib
  FILE_GUID                      = D42F5BB8-E0CE-47BD-8C52-476C79055FC6
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = PEIM
  LIBRARY_CLASS                  = PeiPolicyUpdateLib|PEIM PEI_CORE SEC

[LibraryClasses]
  HobLib
  BaseCryptLib
  CpuPlatformLib
  IoLib
  EcMiscLib
  PeiSaPolicyLib
  MemoryAllocationLib
  PeiServicesTablePointerLib
  SerialPortParameterLib
  PcdLib
  Tpm2CommandLib
  Tpm12CommandLib
  Tpm2DeviceLib
  Tpm12DeviceLib
  BoardConfigLib
  PciSegmentLib
  CnviLib
  SiPolicyLib
  PeiServicesLib
  MeFwStsLib
  FspCommonLib
  PeiDTbtPolicyLib
  PreSiliconEnvDetectLib
  CpuInitLib
  BiosGuardLib
  FirmwareBootMediaLib
  RstCrLib
  SpiLib
  PeiTsnFvLib
  BmpSupportLib
  SpiAccessLib
  PeiGetFvInfoLib
  TimerLib
  GbeLib
  PeiBootStateLib
  PeiMeLib
  CmosAccessLib
#[-start-191111-IB10189001-add]#
  PeiOemSvcKernelLibDefault
  H2OLib
#[-end-191111-IB10189001-add]#
#[-start-200724-IB17040135-add]#
  BaseOemSvcChipsetLib
#[-end-200724-IB17040135-add]#
#[-start-210528-IB16740141-add]#
  CmosLib
#[-end-210528-IB16740141-add]#

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  IntelFsp2WrapperPkg/IntelFsp2WrapperPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  SecurityPkg/SecurityPkg.dec
  CryptoPkg/CryptoPkg.dec
  IntelSiliconPkg/IntelSiliconPkg.dec
  IntelFsp2Pkg/IntelFsp2Pkg.dec
#[-start-200917-IB06462159-modify]#
  $(PLATFORM_FSP_BIN_PACKAGE)/AlderLakeFspBinPkg.dec
#[-end-200917-IB06462159-modify]#
  AlderLakeBoardPkg/BoardPkg.dec
  MinPlatformPkg/MinPlatformPkg.dec
  BoardModulePkg/BoardModulePkg.dec
#[-start-191111-IB10189001-add]#
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeOemServicesPkg/InsydeOemServicesPkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#[-end-191111-IB10189001-add]#
#[-start-200210-IB14630306-add]#
  SioDummyPkg/SioDummyPkg.dec
#[-end-200210-IB14630306-add]#

[FixedPcd]
  gBoardModuleTokenSpaceGuid.PcdDefaultBoardId              ## CONSUMES
#[-start-201021-IB11790397-modify]#
#  gBoardModuleTokenSpaceGuid.PcdFlashFvFirmwareBinariesBase    ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PcdFlashFirmwareBinariesFvBase
#[-end-201021-IB11790397-modify]#
  gSiPkgTokenSpaceGuid.PcdITbtEnable                        ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdBiosSize                          ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable               ## CONSUMES
#[-start-211109-IB18410136-add]#
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxDownscaleAmp                ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxDeEmph                      ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioCtrlAdaptOffsetCfg            ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioFilterSelN                    ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioFilterSelP                    ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioOlfpsCfgPullUpDwnRes          ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxDeEmphEnable                ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxDownscaleAmpEnable          ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioCtrlAdaptOffsetCfgEnable      ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioFilterSelNEnable              ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioFilterSelPEnable              ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioOlfpsCfgPullUpDwnResEnable    ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxRate3UniqTran               ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxRate2UniqTran               ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxRate1UniqTran               ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxRate0UniqTran               ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxRate3UniqTranEnable         ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxRate2UniqTranEnable         ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxRate1UniqTranEnable         ## CONSUMES
  gChipsetPkgTokenSpaceGuid.PchUsb3HsioTxRate0UniqTranEnable         ## CONSUMES
#[-end-211109-IB18410136-add]#

[Pcd]
  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress     ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdPciExpressRegionLength        ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdMchBaseAddress                ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSmbusBaseAddress              ## CONSUMES
  gEfiSecurityPkgTokenSpaceGuid.PcdTpmInstanceGuid      ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdDmiBaseAddress       ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdEpBaseAddress        ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdGttMmAddress         ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdGmAdrAddress         ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdEdramBaseAddress     ## CONSUMES
  gIntelFsp2WrapperTokenSpaceGuid.PcdFspmUpdDataAddress ## CONSUMES
  gIntelFsp2WrapperTokenSpaceGuid.PcdFspsUpdDataAddress ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdEmbeddedEnable                ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdOverclockEnable               ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdVmdCfgBarBase     ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdVmdMemBar1Base    ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdVmdMemBar2Base    ## CONSUMES

  gSiPkgTokenSpaceGuid.PcdSmbusBaseAddress              ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdHgEnable                      ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdIoExpanderPresent
  gBoardModuleTokenSpaceGuid.PcdSaDdrFreqLimit       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardPmcPdEnable     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardId              ## CONSUMES

  gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamBase              ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamSize              ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdFspTemporaryRamSize           ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdFspReservedBufferSize         ## CONSUMES

  # HDA Verb Table
  gBoardModuleTokenSpaceGuid.PcdHdaVerbTableDatabase ## CONSUMES

  # SA Misc Config
  gBoardModuleTokenSpaceGuid.PcdSaMiscUserBd                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdSaMiscMmioSizeAdjustment       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcRcompResistor               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcRcompTarget                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcDqByteMap                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcDqByteMapSize               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcDqsMapCpu2Dram              ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcDqsMapCpu2DramSize          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcDqMapCpu2Dram               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcDqMapCpu2DramSize           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcDqPinsInterleavedControl    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcDqPinsInterleaved           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdDataSize                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcLp5CccConfig                ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcCmdMirror                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdSaMiscFirstDimmBitMask         ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdSaMiscFirstDimmBitMaskEcc      ## CONSUMES

  # Display DDI
  gBoardModuleTokenSpaceGuid.PcdSaDisplayConfigTable           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdSaDisplayConfigTableSize       ## CONSUMES

  # PCIE RTD3 GPIO
  gBoardModuleTokenSpaceGuid.PcdRootPortDev                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdRootPortFunc                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdRootPortIndex                  ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcieSlot1GpioSupport               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstExpanderNo         ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioPolarity       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableExpanderNo       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioPolarity     ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcie0GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0PwrEnableActive               ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcie1GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1PwrEnableActive               ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcie2GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2PwrEnableActive               ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcie3GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3PwrEnableActive               ## CONSUMES

  # SPD Address Table
  gBoardModuleTokenSpaceGuid.PcdSpdPresent                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable0            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable1            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable2            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable3            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable4            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable5            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable6            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable7            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable8            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable9            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable10           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable11           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable12           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable13           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable14           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable15           ## CONSUMES

  # CA Vref Configuration
  gBoardModuleTokenSpaceGuid.PcdMrcCaVrefConfig                ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdMobileDramPresent              ## CONSUMES

  # PCIe Clock Info
  gBoardModuleTokenSpaceGuid.PcdPcieClock0                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock1                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock2                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock3                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock4                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock5                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock6                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock7                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock8                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock9                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock10                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock11                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock12                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock13                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock14                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock15                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock16                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcieClock17                    ## CONSUMES

  # USB 2.0 PHY Port parameters
  gBoardModuleTokenSpaceGuid.PcdUsb2PhyTuningTable             ## CONSUMES

  # USB 2.0 Port Over Current Pin
  gBoardModuleTokenSpaceGuid.PcdUsb2OverCurrentPinTable        ## CONSUMES

  # USB 3.0 Port Over Current Pin
  gBoardModuleTokenSpaceGuid.PcdUsb3OverCurrentPinTable        ## CONSUMES

  # Pch SerialIo I2c Pads Termination
  gBoardModuleTokenSpaceGuid.PcdPchSerialIoI2c0PadInternalTerm ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSerialIoI2c1PadInternalTerm ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSerialIoI2c2PadInternalTerm ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSerialIoI2c3PadInternalTerm ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSerialIoI2c4PadInternalTerm ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSerialIoI2c5PadInternalTerm ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSerialIoI2c6PadInternalTerm ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchSerialIoI2c7PadInternalTerm ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPlatformType
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor
  gBoardModuleTokenSpaceGuid.PcdBoardType
  gBoardModuleTokenSpaceGuid.PcdBoardBomId
  gBoardModuleTokenSpaceGuid.PcdEcPresent
  gBoardModuleTokenSpaceGuid.PcdSkuType

  gIntelSiliconPkgTokenSpaceGuid.PcdIntelGraphicsVbtFileGuid   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdVbtMipiGuid                    ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdSmbusAlertEnable               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdSataLedEnable                  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdVrAlertEnable                  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPchThermalHotEnable            ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdMemoryThermalSensorGpioCPmsyncEnable  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMemoryThermalSensorGpioDPmsyncEnable  ## CONSUMES
  gEfiMdeModulePkgTokenSpaceGuid.PcdSystemFmpCapsuleImageTypeIdGuid   ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdHgEnable
  gSiPkgTokenSpaceGuid.PcdIpuEnable                                   ## CONSUMES
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMaxLogicalProcessorNumber           ## CONSUMES
#[-start-200210-IB14630306-add]#
  gChipsetPkgTokenSpaceGuid.PcdDefaultSsidSvidPeiTable
  gInsydeTokenSpaceGuid.PcdDefaultSsidSvid
#[-end-200210-IB14630306-add]#
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMicrocodePatchAddress               ## CONSUMES
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMicrocodePatchRegionSize            ## CONSUMES

  gIntelSiliconPkgTokenSpaceGuid.PcdVTdPolicyPropertyMask             ## CONSUMES
  gIntelSiliconPkgTokenSpaceGuid.PcdVTdPeiDmaBufferSize               ## CONSUMES  # The memory size should be larged then 1MB and follow the alignment
  gIntelSiliconPkgTokenSpaceGuid.PcdVTdPeiDmaBufferSizeS3             ## CONSUMES  # The memory size should be larged then 1MB and follow the alignment
  gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable                   ## CONSUMES

  gEfiMdeModulePkgTokenSpaceGuid.PcdPcieResizableBarSupport           ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPolicyCheckIsFirstBoot  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPolicyCheckIsRvpSupport ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPolicyCheckPcdInitDone  ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdUsbTypeCSupport         ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdTypeCPortsSupported     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort1Pch        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPort1Properties     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort2Pch        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPort2Properties     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort3Pch        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPort3Properties     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort4Pch        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPort4Properties     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort5Pch        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPort5Properties     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort6Pch        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPort6Properties     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort7Pch        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPort7Properties     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort8Pch        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPort8Properties     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort9Pch        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPort9Properties     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPortAPch        ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbCPortAProperties     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdCpuUsb30PortEnable      ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcBiosGuardEnable       ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdExtendedBiosRegionSupport         ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashExtendRegionSizeInUse           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTablePreMem                 ## CONSUMES

#[-start-191111-IB10189001-add]#
  gInsydeTokenSpaceGuid.PcdH2OChipsetPciePortEnable
  gInsydeTokenSpaceGuid.PcdH2OChipsetUsbPortEnable
  gInsydeTokenSpaceGuid.PcdH2OChipsetUsb3PortEnable
  gInsydeTokenSpaceGuid.PcdH2OChipsetSataPortEnable
  gChipsetPkgTokenSpaceGuid.PcdCrbBoard
  gChipsetPkgTokenSpaceGuid.PcdDebugUsePchComPort
  gSiPkgTokenSpaceGuid.PcdSerialIoUartNumber
  gPlatformModuleTokenSpaceGuid.PcdStatusCodeFlags
  gInsydeTokenSpaceGuid.PcdMaxSockets
  gChipsetPkgTokenSpaceGuid.PcdHybridGraphicsSupported
  gChipsetPkgTokenSpaceGuid.PcdUseCrbHgDefaultSettings
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuHoldRstActive
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuPwrEnableGpioNo
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuWakeGpioNo
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuGpioSupport
#[-start-201113-IB09480112-add]#
  gChipsetPkgTokenSpaceGuid.PcdHgPcieRootPortIndex
#[-end-201113-IB09480112-add]#
  gInsydeTokenSpaceGuid.PcdPeiRecoveryMinMemorySize
  gInsydeTokenSpaceGuid.PcdH2OPeiMinMemorySize
  gInsydeTokenSpaceGuid.PcdPreserveMemoryTable
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuHoldRstGpioNo
  gChipsetPkgTokenSpaceGuid.PcdHgDgpuPwrEnableActive
#[-end-191111-IB10189001-add]#
#[-start-200303-IB14630338-add]#
#[-start-201104-IB14630426-modify]#
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc0Ch0Dimm0
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc0Ch1Dimm0
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc0Ch2Dimm0
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc0Ch3Dimm0
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc1Ch0Dimm0
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc1Ch1Dimm0
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc1Ch2Dimm0
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc1Ch3Dimm0
#[-end-201104-IB14630426-modify]#
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSupported
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownRcompResistorSupported
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownRcompTargetSupported
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownRcompTarget
  #gChipsetPkgTokenSpaceGuid.PcdH2OMrcDqByteMapSupported
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownDqByteMapSupported
  #gChipsetPkgTokenSpaceGuid.PcdH2OMrcDqByteMap
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownDqByteMap
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownRcompResistor
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownDqPinsInterleaved
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownDqPinsInterleavedSupported
  #gChipsetPkgTokenSpaceGuid.PcdH2OMrcDqsMapCpu2DramSupported
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownDqsMapCpu2DramSupported
  #gChipsetPkgTokenSpaceGuid.PcdH2OMrcDqsMapCpu2Dram
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownDqsMapCpu2Dram
#[-end-200303-IB14630338-add]#
#[-start-201021-IB11790397-add]#
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtBiosSvn
  gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag

  gInsydeTokenSpaceGuid.PcdFlashNvStorageFactoryCopyBase
  gInsydeTokenSpaceGuid.PcdFlashAreaBaseAddress
  gInsydeTokenSpaceGuid.PcdFlashNvStorageFactoryCopySize
  gInsydeTokenSpaceGuid.PcdFlashNvStorageBvdtSize
  gInsydeTokenSpaceGuid.PcdFlashNvStorageDmiBase
  gInsydeTokenSpaceGuid.PcdFlashNvStorageDmiSize
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMsdmDataBase
  gInsydeTokenSpaceGuid.PcdFlashNvStorageVariableDefaultsBase
  gInsydeTokenSpaceGuid.PcdFlashAreaSize

  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtPublicKeySlot0
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtPublicKeySlot1
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtPublicKeySlot2
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtPlatformId

  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigEcCmdDiscovery
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtEcCmdGetSvn
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtEcCmdOpen
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtEcCmdClose
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtEcCmdPortTest
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgupHeaderEcSvn
#[-end-201021-IB11790397-add]#
#[-start-201112-IB16810138-add]#
  gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport
#[-end-201112-IB16810138-add]#
#[-start-210318-IB05660157-add]#
  gChipsetPkgTokenSpaceGuid.PcdCrbSkuId
#[-end-210318-IB05660157-add]#
#[-start-210518-IB05660166-add]#
  gChipsetPkgTokenSpaceGuid.PcdH2OPcieNonCpuPortNumber
#[-end-210518-IB05660166-add]#

[FixedPcd]
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiAcpiReclaimMemorySize    ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiAcpiNvsMemorySize        ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiReservedMemorySize       ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiRtDataMemorySize         ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiRtCodeMemorySize         ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashNvStorageSize                  ## CONSUMES
  gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection                  ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdTsegSize                                     ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable                       ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeBase                ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeSize                ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdMicrocodeOffsetInFv                 ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdBiosGuardEnable                              ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdAdlLpSupport                                 ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdCpuPcieEnable                                ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdVmdEnable                                    ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdPlatformMeEDebugCapsuleImageTypeIdGuid ## CONSUMES

[Sources]
  PeiPchPolicyUpdatePreMem.c
  PeiPchPolicyUpdate.c
  PeiCpuPolicyUpdatePreMem.c
  PeiCpuPolicyUpdate.c
  PeiMePolicyUpdatePreMem.c
  PeiMePolicyUpdate.c
  PeiAmtPolicyUpdate.c
  PeiSaPolicyUpdate.c
  PeiSaPolicyUpdatePreMem.c
  PeiTbtPolicyUpdate.c
  PeiSiPolicyUpdate.c
  PcieWorkaroundOnPolicy.c

[Ppis]
  gEfiPeiReadOnlyVariable2PpiGuid               ## CONSUMES
  gWdtPpiGuid                                   ## CONSUMES
  gPchSpiPpiGuid                                ## CONSUMES
  gSiPolicyPpiGuid                              ## CONSUMES
  gSiPreMemPolicyPpiGuid                        ## CONSUMES
  gPeiDTbtPolicyPpiGuid                         ## CONSUMES
  gFspmArchConfigPpiGuid                        ## PRODUCES
  gReadyForGopConfigPpiGuid                     ## PRODUCES
  gEfiSecPlatformInformationPpiGuid
  gEfiSecPlatformInformation2PpiGuid
  gDebugEventHandlerPpiGuid
  gPeiGraphicsPlatformPpiGuid                   ## CONSUMES
  gPlatformVTdEnableDmaBufferPpiGuid            ## CONSUMES
  gPeiGraphicsFramebufferReadyPpiGuid           ## CONSUMES

[Guids]
  gMeBiosExtensionSetupGuid                     ## CONSUMES
  gAmtForcePushPetPolicyGuid                    ## CONSUMES
  gEfiGlobalVariableGuid                        ## CONSUMES
  gSetupVariableGuid                            ## CONSUMES
  gMemoryConfigVariableGuid                     ## CONSUMES
  gS3MemoryVariableGuid                         ## CONSUMES
  gSiSetupVariableGuid                          ## CONSUMES
  gSaSetupVariableGuid                          ## CONSUMES
  gMeSetupVariableGuid                          ## CONSUMES
  gCpuSetupVariableGuid                         ## CONSUMES
  gPchSetupVariableGuid                         ## CONSUMES
  gEfiCapsuleVendorGuid                         ## CONSUMES
  gBiosGuardModuleGuid                          ## CONSUMES
  gBiosGuardModuleSimGuid                       ## CONSUMES
  gBiosGuardHobGuid                             ## CONSUMES
  gEfiMemoryTypeInformationGuid                 ## CONSUMES
  gEfiMemoryOverwriteControlDataGuid            ## CONSUMES
  gDebugConfigVariableGuid                      ## CONSUMES
  gTianoLogoGuid                                ## CONSUMES
  gTxtApStartupPeiFileGuid                      ## CONSUMES
  gTxtBiosAcmPeiFileGuid                        ## CONSUMES
  gEfiFmpCapsuleGuid                            ## CONSUMES
  gSkipBiosLockForSysFwUpdateGuid               ## SOMETIMES_PRODUCES
  gTcssHobGuid                                  ## CONSUMES
  gDebugConfigHobGuid                           ## CONSUMES
  gCpuPcieRpPrememConfigGuid                    ## CONSUMES
  gRstConfigGuid                                ## CONSUMES
  gPciePreMemConfigGuid                         ## CONSUMES
  gSysFwUpdateProgressGuid                      ## CONSUMES
  gFmpDevicePlatformMonolithicGuid              ## CONSUMES
  gFmpDevicePlatformBiosGuid                    ## CONSUMES
  gFmpDevicePlatformMeGuid                      ## CONSUMES
  gCpuPcieHobGuid                               ## CONSUMES
  gFmpDevicePlatformBtGAcmGuid                  ## CONSUMES
  gFmpDevicePlatformuCodeGuid                   ## CONSUMES
  gCpuPcieHobGuid
  gFusaConfigGuid                               ## CONSUMES
  gPlatformInitFvLocationGuid                   ## CONSUMES
  gHostBridgePeiPreMemConfigGuid                ## CONSUMES
  gHostBridgePeiConfigGuid                      ## CONSUMES
  gCpuDmiPreMemConfigGuid                       ## CONSUMES
  gEfiVmdFeatureVariableGuid                    ## CONSUMES
  gHybridGraphicsConfigGuid                     ## CONSUMES
  gMebxDataGuid                                 ## CONSUMES
  gMeBiosPayloadHobGuid                         ## CONSUMES
  gOemSnpsPhyTableGuid                          ## CONSUMES
  gAdlPchLpChipsetInitTableAxGuid               ## CONSUMES
  gEfiGraphicsInfoHobGuid                       ## CONSUMES
  gSaDataHobGuid                                ## CONSUMES
  gPchDmiPreMemConfigGuid                       ## CONSUMES
#[-start-191111-IB10189001-add]#
  gDimmSmbusAddrHobGuid
#[-end-191111-IB10189001-add]#
#[-start-210318-IB05660157-add]#
  gSystemConfigurationGuid
#[-end-210318-IB05660157-add]#
#[-start-211208-IB16810176-add]#
  gChasmfallsTopSwapStatusGuid
#[-end-211208-IB16810176-add]#