//*****************************************************************************
//
//
// Copyright (c) 2012 - 2015, Hefei LCFC Information Technology Co.Ltd.
// And/or its affiliates. All rights reserved.
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
// Use is subject to license terms.
//
//******************************************************************************
/*
History:
  Date          Name          Version    Change Notes
*/
#include <PiPei.h>
#include <Pi/PiBootMode.h>
#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/OemSvcLfcCardReaderCfg.h>
#include <Library/OemSvcLfcGenesysLogicCardReaderCfg.h>
//[start-211211-STORM1119-modify]
#if defined(C770_SUPPORT)
#include <Library/GpioLib.h>
#define GPIO_VER2_LP_GPP_R6              0x09030006  //Board ID5
#define GPIO_VER2_LP_GPP_R7              0x09030007  //Board ID6

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaC770_GPIO_Table_early_initGLC[] =
{
  {GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5
  {GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6
};

#endif
//[end-211211-STORM1119-modify]

PCI_REG_TABLE GL9750PciRegTable[] = {  
  { 0x800,  0xFFFFFFFE,  0x00000001                     }, // Unlock write protect
//[-start-211110-YUNLEI0153-modify]//
  { 0x844,  0x7FFFFFFF,  0x80000000                     },// Set the IDDQ function.
//[-end-211110-YUNLEI0153-modify]//
  { 0x848,  0xFFFF8FC7,  0x00006030                     }, // Adjust L1 Exit Latency if to enable ASPM
  { 0x848,  0xFFFFFFBF,  0x00000000                     }, // Disable ASPM L0s
  { 0x860,  0x0FFFFFFF,  0x00800000                     }, // Ennable DFx for Modern Standby
  { 0x878,  0xFDFFFFFF,  0x02000000                     }, // Disable wake up system by card insertion or removal during S3/S4
  { 0x87C,  0x00000000,  LENOVO_GL9750_OIYL4_SSVID_SSID }, // Programing SSID/SVID
  { 0x800,  0xFFFFFFFE,  0x00000000                     }, // Lock write protect
};

//[start-211211-STORM1119-modify]
#if defined(C770_SUPPORT)
PCI_REG_TABLE GL9750PciRegTable_size14[] = {  
  { 0x800,  0xFFFFFFFE,  0x00000001                     }, // Unlock write protect
//[-start-211110-YUNLEI0153-modify]//
//  { 0x844,  0x7FFFFFFF,  0x80000000                     },// Set the IDDQ function.
//[-end-211110-YUNLEI0153-modify]//
  { 0x848,  0xFFFF8FC7,  0x00006030                     }, // Adjust L1 Exit Latency if to enable ASPM
  { 0x848,  0xFFFFFFBF,  0x00000000                     }, // Disable ASPM L0s
  { 0x860,  0x0FFFFFFF,  0x00800000                     }, // Ennable DFx for Modern Standby
  { 0x878,  0xFDFFFFFF,  0x02000000                     }, // Disable wake up system by card insertion or removal during S3/S4
  { 0x87C,  0x00000000,  LENOVO_GL9750_OIYL4_SSVID_SSID }, // Programing SSID/SVID
  { 0x800,  0xFFFFFFFE,  0x00000000                     }, // Lock write protect
};


EFI_STATUS
LcfcGpioGetMachineSizeGLC(
    OUT UINT8      *MachineSize
)

{
  UINT32  BoardID5, BoardID6;
  UINT8   Data8 = 0;
  GpioGetInputValue(GPIO_VER2_LP_GPP_R6,&BoardID5);
  GpioGetInputValue(GPIO_VER2_LP_GPP_R7,&BoardID6);

  Data8 |= (UINT8)(BoardID6 << 0);
  Data8 |= (UINT8)(BoardID5 << 1);

  DEBUG ((DEBUG_INFO, "KEBIN dbg Machine Size = 0x%x\n",Data8));
  *MachineSize = Data8;
   DEBUG ((DEBUG_INFO, "KEBIN dbg2 MachineSize = 0x%x\n",*MachineSize));
  return EFI_SUCCESS;
}
#endif
//[end-211211-STORM1119-modify]

// this function will be executed right after end of pei  (project speific code here)
EFI_STATUS
EFIAPI
CheckAndProgramGenesysLogicCardReader (
  IN EFI_PEI_SERVICES          **PeiServices,
  UINT8   BrgBusNum,
  UINT8   BrgDevNum,
  UINT8   BrgFunNum,
  UINT8   BusNum,
  UINTN   MemBaseAddr,
  UINT16  VendorId,
  UINT16  DeviceId
  )
{
  UINT32               Temp32;
  UINT8                Index;
//[start-211211-STORM1119-modify]
#if defined(C770_SUPPORT)
  UINT8                MachineSize = 0;
#endif
//[end-211211-STORM1119-modify]
  DEBUG((DEBUG_INFO, __FUNCTION__" Entry\n"));
  //
  // Determine device by Vendor ID and Device ID.
  //
  if ((VendorId != GenesysLogic_Card_Reader_VID) || (DeviceId != GenesysLogic_Card_Reader_DID)) {
    return EFI_UNSUPPORTED;
  }
  DEBUG ((DEBUG_INFO, "Find Genesys Logic Card Reader(B:%d/D:%d:/F:%d)\n", BusNum, 0, 0));

  //
  // Write Value To Crc Register
  //
//[start-211211-STORM1119-modify]
#if defined(C770_SUPPORT)
GpioConfigurePads (2, YogaC770_GPIO_Table_early_initGLC);
LcfcGpioGetMachineSizeGLC (&MachineSize);

if(2 == MachineSize) {
#endif
//[end-211211-STORM1119-modify]
  for(Index = 0 ; Index < sizeof(GL9750PciRegTable)/sizeof(PCI_REG_TABLE) ; Index ++) {
    if(GL9750PciRegTable[Index].Offset == 0xFFFFFFFF) break;

    //
    // Get Current setting.
    //
    Temp32 = MmioRead32((UINTN) (MemBaseAddr+ GL9750PciRegTable[Index].Offset));
    DEBUG ((DEBUG_INFO, "Read Genesys Logic Card Reader Register Offset %.4X: %.8X\n", GL9750PciRegTable[Index].Offset, Temp32));
    //
    // Prepare OEM Setting.
    //
    Temp32 = (Temp32 & GL9750PciRegTable[Index].AndMask) | GL9750PciRegTable[Index].OrValue;

    //
    // Write OEM Setting.
    //
    MmioWrite32((UINTN) (MemBaseAddr + GL9750PciRegTable[Index].Offset), Temp32);
    DEBUG ((DEBUG_INFO, "Write Genesys Logic Card Reader Register Offset %.4X: %.8X\n", GL9750PciRegTable[Index].Offset, Temp32));
  }
//[start-211211-STORM1119-modify]
#if defined(C770_SUPPORT)
} else {
  for(Index = 0 ; Index < sizeof(GL9750PciRegTable_size14)/sizeof(PCI_REG_TABLE) ; Index ++) {
    if(GL9750PciRegTable_size14[Index].Offset == 0xFFFFFFFF) break;

    //
    // Get Current setting.
    //
    Temp32 = MmioRead32((UINTN) (MemBaseAddr+ GL9750PciRegTable_size14[Index].Offset));
    DEBUG ((DEBUG_INFO, "Read Genesys Logic Card Reader Register Offset %.4X: %.8X\n", GL9750PciRegTable_size14[Index].Offset, Temp32));
    //
    // Prepare OEM Setting.
    //
    Temp32 = (Temp32 & GL9750PciRegTable_size14[Index].AndMask) | GL9750PciRegTable_size14[Index].OrValue;

    //
    // Write OEM Setting.
    //
    MmioWrite32((UINTN) (MemBaseAddr + GL9750PciRegTable_size14[Index].Offset), Temp32);
    DEBUG ((DEBUG_INFO, "Write Genesys Logic Card Reader Register Offset %.4X: %.8X\n", GL9750PciRegTable_size14[Index].Offset, Temp32));
  }
}
#endif
//[end-211211-STORM1119-modify]
  DEBUG((DEBUG_INFO, __FUNCTION__" End\n"));
  return EFI_SUCCESS;
}

