/** @file
  Header file for USB4 configuration space access functions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _USB4_CS_IO_H_
#define _USB4_CS_IO_H_

#include <Uefi.h>
#include <Usb4ConfigLayer.h>
#include <Usb4ConfigSpace.h>

typedef struct _USB4_CS_IO   USB4_CS_IO;

//
// USB4_CS_IO functions
//

/**
  Reads register data with a given address from USB4 config space.
  Operation:
  1. Prepare the read request
  2. Swap bytes
  3. Calculate CRC and swap it too
  4. Read response and swap bytes of response data

  @param[in]  This        - Pointer to USB4_CS_IO function table.
  @param[in]  TopologyId  - Pointer to Topology ID of Router.
  @param[in]  ConfigSpace - Configuration space to be read.
  @param[in]  AdpNum      - Adapter number.
  @param[in]  Offset      - Data offset in the config space.
  @param[in]  Count       - DWORD count to be read from config space.
  @param[out] Data        - Data buffer for Read data.
  @param[out] RspAdpNum   - Pointer to the response adapter number (Required for TBT3 compatible operations).

  @retval EFI_SUCCESS           - USB4 config space register read success.
  @retval EFI_UNSUPPORTED       - Fail to read USB4 config space register.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
typedef
EFI_STATUS
(* USB4_CS_IO_READ) (
  IN  USB4_CS_IO           *This,
  IN  PTOPOLOGY_ID         TopologyId,
  IN  USB4_CONFIG_SPACE    ConfigSpace,
  IN  UINT8                AdpNum,
  IN  UINT16               Offset,
  IN  UINT8                Count,
  OUT UINT32               *Data,
  OUT UINT8                *RspAdpNum
  );

/**
  Write data to a given register in USB4 config space
  Operation:
  1. Prepare the write request
  2. Swap bytes
  3. Calculate CRC and swap it

  @param[in] This         - Pointer to USB4_CS_IO function table.
  @param[in] TopologyId   - Pointer to Topology ID of Router.
  @param[in] ConfigSpace  - Configuration space to be written.
  @param[in] AdpNum       - Adapter number
  @param[in] Offset       - Data offset in the config space.
  @param[in] Count        - DWORD count to be written to config space.
  @param[in] Data         - Data buffer for USB4 config space read.

  @retval EFI_SUCCESS           - Write data to USB4 register successfully.
  @retval EFI_UNSUPPORTED       - Fail to read USB4 config space register.
  @retval EFI_INVALID_PARAMETER - Invalid parameter.
**/
typedef
EFI_STATUS
(* USB4_CS_IO_WRITE) (
  IN USB4_CS_IO           *This,
  IN PTOPOLOGY_ID         TopologyId,
  IN USB4_CONFIG_SPACE    ConfigSpace,
  IN UINT8                AdpNum,
  IN UINT16               Offset,
  IN UINT8                Count,
  IN UINT32               *Data
  );

/**
  Query Upstream Adapter number of Router.
  Used for TBT3 compatible operations.

  @param[in]  This         - Pointer to USB4_CS_IO instance.
  @param[in]  TopologyId   - Pointer to Topology ID of Router.
  @param[out] UpAdpNum     - Pointer to Upstream Adapter number.

  @retval EFI_SUCCESS      - Query Upstream Adapter number success.
  @retval other            - Query Upstream Adapter number failure.
**/
typedef
EFI_STATUS
(* USB4_CS_IO_QUERY_RT_UP_ADAPTER) (
  IN  USB4_CS_IO      *This,
  IN  PTOPOLOGY_ID    TopologyId,
  OUT UINT8           *UpAdpNum
  );

/**
  Increment reference count to host interface core.
  The host interface core can't be released if the reference count is non-zero.

  @param[in] This - Pointer to USB4_CS_IO function table.
**/
typedef
VOID
(* USB4_CS_IO_ADD_REF) (
  IN USB4_CS_IO    *This
  );

/**
  Decrement reference count to host interface core.
  The host interface core can't be released if the reference count is non-zero.

  @param[in] This - Pointer to USB4_CS_IO function table.
**/
typedef
VOID
(* USB4_CS_IO_RELEASE_REF) (
  IN USB4_CS_IO    *This
  );

//
// Function table for USB4 config space access
//
struct _USB4_CS_IO {
  USB4_CS_IO_READ                   CsRead;
  USB4_CS_IO_WRITE                  CsWrite;
  USB4_CS_IO_QUERY_RT_UP_ADAPTER    QueryRtUpAdp;
  USB4_CS_IO_ADD_REF                AddRef;
  USB4_CS_IO_RELEASE_REF            ReleaseRef;
};

#endif
