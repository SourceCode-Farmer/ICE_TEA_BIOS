## @file
#  Component description file for SeamlessRecoveryLib module
#
#******************************************************************************
#* Copyright (c) 2013 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = SeamlessRecoveryLibSysFw
  FILE_GUID                      = C40A39CB-DE16-4925-BC21-8EE63C1FE421
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = SeamlessRecoveryLib
  CONSTRUCTOR                    = SeamlessRecoveryLibConstructor

[Sources]
  SeamlessRecoveryLib.c

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec
  $(PLATFORMSAMPLE_PACKAGE)/PlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec

[LibraryClasses]
  DebugLib
  PcdLib
  BaseMemoryLib
  MemoryAllocationLib
  FlashDevicesLib
  FlashRegionLib
  UefiBootServicesTableLib
  VariableLib
  PciSegmentLib
  PchPciBdfLib
  HobLib

[Guids]
  gH2OFlashMapRegionFtwBackupGuid
  gH2OSeamlessRecoveryGuid
  gSysFwUpdateProgressGuid
  gFmpCapsuleInfoGuid
  gChasmfallsTopSwapStatusGuid

[Protocols]
  gEfiSmmFwBlockServiceProtocolGuid
  gEfiSwapAddressRangeProtocolGuid

[Pcd]
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareBase
  gInsydeTokenSpaceGuid.PcdCrisisRecoverySupported
  gInsydeTokenSpaceGuid.PcdSeamlessRecoverySignature
  gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceResiliencyEnabled
  gInsydeTokenSpaceGuid.PcdFlashPbbRBase
  gInsydeTokenSpaceGuid.PcdFlashPbbBase
  gInsydeTokenSpaceGuid.PcdFlashPbbSize
  gInsydeTokenSpaceGuid.PcdFlashSbbBase
  gInsydeTokenSpaceGuid.PcdFlashSbbSize

