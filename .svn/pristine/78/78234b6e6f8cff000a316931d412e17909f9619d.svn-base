/** @file
;******************************************************************************
;* Copyright (c) 2015 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
**/

    //
    //  Form 0x20: PCI Configuration
    //
    form
      formid = 0x20, title = STRING_TOKEN(STR_PCI_CONFIG_STRING);

      subtitle
        text = STRING_TOKEN(STR_PCI_CONFIG_STRING);

      subtitle
        text = STRING_TOKEN(STR_BLANK_STRING);

      grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;

      oneof
        varid       = SystemConfig.PciSlot3,
        questionid = KEY_PCI_SLOT3_IRQ_SET,
        prompt      = STRING_TOKEN(STR_PCI_SLOT3_STRING),
        help        = STRING_TOKEN(STR_PCI_SLOT_HELP),
        option text = STRING_TOKEN(STR_AUTO_TEXT),      value = 0, flags = INTERACTIVE | DEFAULT;
        option text = STRING_TOKEN(STR_PCI_IRQ3_TEXT),  value = 3, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ4_TEXT),  value = 4, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ5_TEXT),  value = 5, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ7_TEXT),  value = 7, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ9_TEXT),  value = 9, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ10_TEXT), value = 10, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ11_TEXT), value = 11, flags = INTERACTIVE;
      endoneof;

      oneof
        varid       = SystemConfig.PciSlot4,
        questionid = KEY_PCI_SLOT4_IRQ_SET,
        prompt      = STRING_TOKEN(STR_PCI_SLOT4_STRING),
        help        = STRING_TOKEN(STR_PCI_SLOT_HELP),
        option text = STRING_TOKEN(STR_AUTO_TEXT),      value = 0, flags = INTERACTIVE | DEFAULT;
        option text = STRING_TOKEN(STR_PCI_IRQ3_TEXT),  value = 3, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ4_TEXT),  value = 4, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ5_TEXT),  value = 5, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ7_TEXT),  value = 7, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ9_TEXT),  value = 9, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ10_TEXT), value = 10, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_PCI_IRQ11_TEXT), value = 11, flags = INTERACTIVE;
      endoneof;
      endif;
    endform;  // Form 0x20: PCI Configuration End

    //
    //  Form 0x21: Boot Configuration
    //
    form
      formid = 0x21, title = STRING_TOKEN(STR_BOOT_CONFIG_STRING);

      subtitle
        text = STRING_TOKEN(STR_BOOT_CONFIG_STRING);

      subtitle
        text = STRING_TOKEN(STR_BLANK_STRING);

      grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.NumLock,
        prompt      = STRING_TOKEN(STR_NUMLOCK_STRING),
        help        = STRING_TOKEN(STR_NUMLOCK_HELP),
        option text = STRING_TOKEN(STR_OFF_TEXT), value = 0, flags = DEFAULT;
        option text = STRING_TOKEN(STR_ON_TEXT),  value = 1, flags = 0;
      endoneof;

#if !FeaturePcdGet(PcdH2OUsbSupported)
      //
      // Usb Zip Emulation as FDD or HDD
      //
      oneof
        varid       = SystemConfig.UsbZipEmulation,
        prompt      = STRING_TOKEN(STR_ZIP_EMULATION_STRING),
        help        = STRING_TOKEN(STR_ZIP_EMULATION_HELP),
        option text = STRING_TOKEN(STR_ZIP_EMU_FDDTYPE_TEXT),  value = 0, flags = DEFAULT;
        option text = STRING_TOKEN(STR_ZIP_EMU_HDDTYPE_TEXT),  value = 1, flags = 0;
      endoneof;
#endif
      endif;

    endform;  // Form 0x21: Boot Configuration End

    //
    //  Form 0x22: Peripheral Configuration
    //
    form
      formid = 0x22, title = STRING_TOKEN(STR_PERIP_CONFIG_STRING);

      subtitle
        text = STRING_TOKEN(STR_PERIP_CONFIG_STRING);

      subtitle
        text = STRING_TOKEN(STR_BLANK_STRING);

      //
      // If serial port a enable, display Base Io and interrupt,
      // else hidden them.
      //
      grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.ComPortA,
        questionid  = KEY_SERIAL_PORTA,
        prompt      = STRING_TOKEN(STR_SERIAL_PORT_A_STRING),
        help        = STRING_TOKEN(STR_SERIAL_PORT_A_HELP),
        option text = STRING_TOKEN(STR_DISABLED_TEXT), value = 0, flags = INTERACTIVE | DEFAULT;
        option text = STRING_TOKEN(STR_AUTO_TEXT),     value = 1, flags = INTERACTIVE | 0;
        option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 2, flags = INTERACTIVE;
      endoneof;
      endif;

        //
        // hide option when Disable or Auto
        //
        grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
        suppressif
          ideqvallist SystemConfig.ComPortA == 0 1;
          oneof
            varid       = SystemConfig.ComPortABaseIo,
            questionid  = KEY_SERIAL_PORTA_BASE_IO,
            prompt      = STRING_TOKEN(STR_BASEIO_STRING),
            help        = STRING_TOKEN(STR_BLANK_STRING),
            option text = STRING_TOKEN(STR_2E8_TEXT), value = 0x0, flags = INTERACTIVE;
            option text = STRING_TOKEN(STR_2F8_TEXT), value = 0x1, flags = INTERACTIVE;
            option text = STRING_TOKEN(STR_3E8_TEXT), value = 0x2, flags = INTERACTIVE;
            option text = STRING_TOKEN(STR_3F8_TEXT), value = 0x3, flags = INTERACTIVE | DEFAULT;
          endoneof;

          oneof
            varid       = SystemConfig.ComPortAInterrupt,
            questionid  = KEY_SERIAL_PORTA_INTERRUPT,
            prompt      = STRING_TOKEN(STR_INTERRUPT_STRING),
            help        = STRING_TOKEN(STR_BLANK_STRING),
            option text = STRING_TOKEN(STR_PCI_IRQ3_TEXT), value = 3,  flags = INTERACTIVE;
            option text = STRING_TOKEN(STR_PCI_IRQ4_TEXT), value = 4,  flags = INTERACTIVE | DEFAULT;
          endoneof;
        endif;
        endif;

      subtitle
        text = STRING_TOKEN(STR_BLANK_STRING);
      grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.ComPortB,
        questionid  = KEY_SERIAL_PORTB,
        prompt      = STRING_TOKEN(STR_INFRARED_PORT_STRING),
        help        = STRING_TOKEN(STR_INFRARED_PORT_HELP),
        option text = STRING_TOKEN(STR_DISABLED_TEXT), value = 0, flags = INTERACTIVE | DEFAULT;
        option text = STRING_TOKEN(STR_AUTO_TEXT),     value = 1, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 2, flags = INTERACTIVE;
      endoneof;
      endif;
        grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
        suppressif
          ideqval SystemConfig.ComPortB == 0;
          oneof
            varid       = SystemConfig.ComPortBMode,
            prompt      = STRING_TOKEN(STR_MODE_STRING),
            help        = STRING_TOKEN(STR_MODE_HELP),
            option text = STRING_TOKEN(STR_CIR_TEXT),    value = 1, flags = 0;
            option text = STRING_TOKEN(STR_FIR_TEXT),    value = 2, flags = DEFAULT;
          endoneof;
        endif;
        endif;
        //
        // hide option when Disable or Auto
        //
        grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
        suppressif
          ideqvallist SystemConfig.ComPortB == 0 1;
          oneof
            varid       = SystemConfig.ComPortBBaseIo,
            questionid  = KEY_SERIAL_PORTB_BASE_IO,
            prompt      = STRING_TOKEN(STR_BASEIO_STRING),
            help        = STRING_TOKEN(STR_BLANK_STRING),
            option text = STRING_TOKEN(STR_2E8_TEXT), value = 0x0, flags = INTERACTIVE;
            option text = STRING_TOKEN(STR_2F8_TEXT), value = 0x1, flags = INTERACTIVE | DEFAULT;
            option text = STRING_TOKEN(STR_3E8_TEXT), value = 0x2, flags = INTERACTIVE;
            option text = STRING_TOKEN(STR_3F8_TEXT), value = 0x3, flags = INTERACTIVE;
          endoneof;

          oneof
            varid       = SystemConfig.ComPortBInterrupt,
            questionid  = KEY_SERIAL_PORTB_INTERRUPT ,
            prompt      = STRING_TOKEN(STR_INTERRUPT_STRING),
            help        = STRING_TOKEN(STR_BLANK_STRING),
            option text = STRING_TOKEN(STR_PCI_IRQ3_TEXT), value = 3,  flags = INTERACTIVE | DEFAULT;
            option text = STRING_TOKEN(STR_PCI_IRQ4_TEXT), value = 4,  flags = INTERACTIVE;
          endoneof;
        endif;
        endif;

        grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
        suppressif
          ideqvallist SystemConfig.ComPortB == 0 1
            OR
          ideqval SystemConfig.ComPortBMode == 0;
          oneof
          varid       = SystemConfig.ComPortBDma,
          prompt      = STRING_TOKEN(STR_DMA_CHANNEL_STRING),
          help        = STRING_TOKEN(STR_BLANK_STRING),
          option text = STRING_TOKEN(STR_DMA1_TEXT), value = 1,  flags = 0;
          option text = STRING_TOKEN(STR_DMA2_TEXT), value = 2,  flags = 0;
          option text = STRING_TOKEN(STR_DMA3_TEXT), value = 3,  flags = DEFAULT;
          endoneof;
        endif;
        endif;

        grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
        suppressif
          ideqvallist SystemConfig.ComPortB == 0 1
            OR
          ideqval SystemConfig.ComPortBMode == 1;
          oneof
          varid       = SystemConfig.FirHighSpeed,
          prompt      = STRING_TOKEN(STR_HIGHSPEED_STRING),
          help        = STRING_TOKEN(STR_HIGHSPEED_HELP),
          option text = STRING_TOKEN(STR_NORMALSPEED_TEXT), value = 0, flags = DEFAULT;
          option text = STRING_TOKEN(STR_HIGHSPEED_TEXT),     value = 1, flags = 0;
          endoneof;
        endif;
        endif;
    endform;

    //
    //  Form 0x23: SATA Configuration
    //
    form
      formid = 0x23, title = STRING_TOKEN(STR_SATA_CONFIG_STRING);

      subtitle
        text = STRING_TOKEN(STR_SATA_CONFIG_STRING);

      subtitle
        text = STRING_TOKEN(STR_BLANK_STRING);



      grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
         AND
         ideqval SystemConfig.SetUserPass == 1
         OR
         ideqval PchSetup.PchSata == 0;
      suppressif
          TRUE;
        oneof
          varid       = SystemConfig.AhciOptionRomSupport,
          questionid = KEY_AHCI_OPROM_CONFIG,
          prompt      = STRING_TOKEN(STR_AHCI_OPTION_ROM_SUPPORT_STRING),
          help        = STRING_TOKEN(STR_AHCI_OPTION_ROM_SUPPORT_HELP),
          option text = STRING_TOKEN(STR_DISABLED_TEXT),             value = 0x00, flags = INTERACTIVE;
          option text = STRING_TOKEN(STR_ENABLED_TEXT),              value = 0x01, flags = DEFAULT | INTERACTIVE;
        endoneof;
      endif;
      endif;

      subtitle
        text = STRING_TOKEN(STR_BLANK_STRING);
     //
     // IdeDvicex = 2 indicates this platform doesn't support this port
     //
     grayoutif
       TRUE;
     suppressif
       ideqval AdvanceConfig.IdeDevice0 == 2;
         goto SERIAL_ATA_PORT0_FORM_ID,
           prompt = STRING_TOKEN(STR_SERIAL_ATA_PORT0_STRING),
        help = STRING_TOKEN(STR_SERIAL_ATA_PORT0_STRING_HELP);
     endif;
     endif;

     grayoutif
       TRUE;
     suppressif
       ideqval AdvanceConfig.IdeDevice1 == 2;
         goto SERIAL_ATA_PORT1_FORM_ID,
           prompt = STRING_TOKEN(STR_SERIAL_ATA_PORT1_STRING),
        help = STRING_TOKEN(STR_SERIAL_ATA_PORT1_STRING_HELP);
     endif;
     endif;

     grayoutif
       TRUE;
     suppressif
       ideqval AdvanceConfig.IdeDevice2 == 2;
         goto SERIAL_ATA_PORT2_FORM_ID,
           prompt = STRING_TOKEN(STR_SERIAL_ATA_PORT2_STRING),
        help = STRING_TOKEN(STR_SERIAL_ATA_PORT2_STRING_HELP);
     endif;
     endif;

     grayoutif
       TRUE;
     suppressif
       ideqval AdvanceConfig.IdeDevice3 == 2;
         goto SERIAL_ATA_PORT3_FORM_ID,
           prompt = STRING_TOKEN(STR_SERIAL_ATA_PORT3_STRING),
        help = STRING_TOKEN(STR_SERIAL_ATA_PORT3_STRING_HELP);
     endif;
     endif;

     grayoutif
       TRUE;
     suppressif
       ideqval AdvanceConfig.IdeDevice4 == 2
       OR
       ideqval SystemConfig.PchType == LPC_PCH_TYPE_LP;
       goto SERIAL_ATA_PORT4_FORM_ID,
         prompt = STRING_TOKEN(STR_SERIAL_ATA_PORT4_STRING),
        help = STRING_TOKEN(STR_SERIAL_ATA_PORT4_STRING_HELP);
     endif;
     endif;

     grayoutif
       TRUE;
     suppressif
       ideqval AdvanceConfig.IdeDevice5 == 2;
       goto SERIAL_ATA_PORT5_FORM_ID,
         prompt = STRING_TOKEN(STR_SERIAL_ATA_PORT5_STRING),
        help = STRING_TOKEN(STR_SERIAL_ATA_PORT5_STRING_HELP);
     endif;
     endif;

     grayoutif
       TRUE;
     suppressif
       ideqval AdvanceConfig.IdeDevice6 == 2;
       goto SERIAL_ATA_PORT6_FORM_ID,
         prompt = STRING_TOKEN(STR_SERIAL_ATA_PORT6_STRING),
        help = STRING_TOKEN(STR_SERIAL_ATA_PORT6_STRING_HELP);
     endif;
     endif;

     grayoutif
       TRUE;
     suppressif
       ideqval AdvanceConfig.IdeDevice7 == 2;
       goto SERIAL_ATA_PORT7_FORM_ID,
        prompt = STRING_TOKEN(STR_SERIAL_ATA_PORT7_STRING),
        help = STRING_TOKEN(STR_SERIAL_ATA_PORT7_STRING_HELP);
     endif;
     endif;

     text
      help   = STRING_TOKEN(STR_BLANK_STRING),
      text   = STRING_TOKEN(STR_BLANK_STRING);

      label IDE_UPDATE_LABEL;

//
// add these code is just for declaration that we can use these StringToken in C code.
//
      suppressif ideqvallist PchSetup.SataInterfaceMode == SATA_MODE_AHCI SATA_MODE_RAID 2 3 4;
        text
          help   = STRING_TOKEN(STR_SERIAL_ATA_PORT0_MODEL_NAME),
          text   = STRING_TOKEN(STR_SERIAL_ATA_PORT1_MODEL_NAME);
        text
          help   = STRING_TOKEN(STR_SERIAL_ATA_PORT2_MODEL_NAME),
          text   = STRING_TOKEN(STR_SERIAL_ATA_PORT3_MODEL_NAME);
        text
          help   = STRING_TOKEN(STR_SERIAL_ATA_PORT4_MODEL_NAME),
          text   = STRING_TOKEN(STR_SERIAL_ATA_PORT5_MODEL_NAME);
        text
          help   = STRING_TOKEN(STR_SERIAL_ATA_PORT6_MODEL_NAME),
          text   = STRING_TOKEN(STR_SERIAL_ATA_PORT7_MODEL_NAME);
      endif;

    endform;



    //
    //  Form 0x26: USB Configuration
    //
    form
      formid = 0x26, title = STRING_TOKEN(STR_USB_CONFIG_STRING);

      subtitle
        text = STRING_TOKEN(STR_USB_CONFIG_STRING);

      subtitle
        text = STRING_TOKEN(STR_BLANK_STRING);

      grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;

        suppressif ideqvallist SystemConfig.BootType == 0 1;
          oneof
            varid       = SystemConfig.LegacyUsbSupport,
            prompt      = STRING_TOKEN ( STR_USB_SUPPORT_STRING ),
            help        = STRING_TOKEN ( STR_USB_SUPPORT_UEFI_HELP ),
            option text = STRING_TOKEN ( STR_DISABLED_TEXT ),            value = 0, flags = 0;
            option text = STRING_TOKEN ( STR_ENABLED_TEXT ),             value = 1, flags = DEFAULT;
          endoneof;
        endif;

        suppressif ideqval SystemConfig.BootType == 2;
          oneof
            varid       = SystemConfig.LegacyUsbSupport,
            prompt      = STRING_TOKEN ( STR_USB_SUPPORT_STRING ),
            help        = STRING_TOKEN ( STR_USB_SUPPORT_LEGACY_HELP ),
            option text = STRING_TOKEN ( STR_DISABLED_TEXT ),            value = 0, flags = 0;
            option text = STRING_TOKEN ( STR_ENABLED_TEXT ),             value = 1, flags = DEFAULT;
            option text = STRING_TOKEN ( STR_UEFI_ONLY_TEXT ),           value = 2, flags = 0;
          endoneof;
        endif;

      endif;



      grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
      oneof
          varid       = SystemConfig.UsbLegacySmiBitClean,
          prompt      = STRING_TOKEN ( STR_USB_LEGACY_SMI_BIT_CLEAN_STRING ),
          help        = STRING_TOKEN ( STR_USB_LEGACY_SMI_BIT_CLEAN_HELP ),
          option text = STRING_TOKEN ( STR_DISABLED_TEXT ), value = 0, flags = DEFAULT;
          option text = STRING_TOKEN ( STR_ENABLED_TEXT ),  value = 1, flags = 0;
      endoneof;
      endif;
   endform;

    //
    //  Form 0x27: Chipset Configuration
    //
    form
      formid = 0x27, title = STRING_TOKEN(STR_CHIPSET_CONFIG_STRING);

      subtitle text = STRING_TOKEN(STR_CHIPSET_CONFIG_STRING);
      subtitle text = STRING_TOKEN(STR_BLANK_STRING);
      subtitle text = STRING_TOKEN(STR_SETUP_WARNING_TEXT);
      subtitle text = STRING_TOKEN(STR_SETTING_ITEM_STRING);
      subtitle text = STRING_TOKEN(STR_MALFUNCTION_STRING);
      subtitle text = STRING_TOKEN(STR_BLANK_STRING);

      suppressif
        ideqvallist SystemConfig.UserAccessLevel == 1 3 4
        OR
        ideqval SystemConfig.SetUserPass == 0;
        text
          help   = STRING_TOKEN(STR_BLANK_STRING),
          text   = STRING_TOKEN(STR_BLANK_STRING),
          text   = STRING_TOKEN(STR_BLANK_STRING),
          flags  = 0,
          key    = 0;
      endif;

#if FeaturePcdGet(PcdMeUnconfigOnRtcSupported)
      label ME_UNCONFIG_ON_RTC_LABEL;

#endif

#if FeaturePcdGet(PcdPttSupported)
      // grayoutif
      //   TRUE;
      //     label PTT_INFO_LABEL;
      // endif;

      grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;

        oneof
          varid       = SystemConfig.PTTEnable,
          // questionid = PTT_ENABLE_LABEL,
          prompt      = STRING_TOKEN (STR_PTT_STRING),
          help        = STRING_TOKEN (STR_PTT_HELP),
          option text = STRING_TOKEN (STR_ENABLED_TEXT),  value = 1, flags = DEFAULT | RESET_REQUIRED | INTERACTIVE;
          option text = STRING_TOKEN (STR_DISABLED_TEXT), value = 0, flags = RESET_REQUIRED | INTERACTIVE;

        endoneof;
      endif;
#endif

#if FeaturePcdGet (PcdHybridGraphicsSupported) && FeaturePcdGet (PcdNvidiaOptimusSupported) && FeaturePcdGet (PcdHgNvidiaDdsFeatureSupport)
      oneof varid   = SystemConfig.DisplayMode,
        prompt      = STRING_TOKEN (STR_DISPLAY_MODE_STRING),
        help        = STRING_TOKEN (STR_DISPLAY_MODE_HELP),
        option text = STRING_TOKEN (STR_IGD_ONLY),  value = 1, flags = RESET_REQUIRED;
        option text = STRING_TOKEN (STR_DGPU_ONLY), value = 2, flags = RESET_REQUIRED;
        option text = STRING_TOKEN (STR_MSHYBRID),  value = 3, flags = RESET_REQUIRED;
        option text = STRING_TOKEN (STR_DYNAMIC) ,  value = 4, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
      endoneof;
#endif

#if FeaturePcdGet (PcdHybridGraphicsSupported)
      oneof varid   = SystemConfig.HgSlot,   // Pcie Slot Selection
        prompt      = STRING_TOKEN (STR_PCIE_SLOT_HG),
        help        = STRING_TOKEN (STR_PCIE_SLOT_HG_HELP),
        option text = STRING_TOKEN (STR_PEG_STRING), value = 1, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
        option text = STRING_TOKEN (STR_PCH_STRING), value = 0, flags = RESET_REQUIRED;
      endoneof;
#endif

//
// SEG Feature - Remove H2OUVE relevant source codes
//
//      grayoutif
//        ideqval SystemConfig.UserAccessLevel == 2
//        AND
//        ideqval SystemConfig.SetUserPass == 1;
//        oneof
//          varid       = SystemConfig.H2OUVESupport,
//          prompt      = STRING_TOKEN ( STR_H2OUVE_SUPPORT_STRING ),
//          help        = STRING_TOKEN ( STR_H2OUVE_SUPPORT_HELP   ),
//          option text = STRING_TOKEN ( STR_DISABLED_TEXT ), value = 0, flags = DEFAULT;
//          option text = STRING_TOKEN ( STR_ENABLED_TEXT ),  value = 1, flags = 0;
//        endoneof;
//      endif;

    endform;

    //
    //  Form 0x28: ACPI Table/Feature Control
    //
    form
      formid = 0x28, title = STRING_TOKEN(STR_ACPI_TABLE_STRING);

      subtitle  text = STRING_TOKEN(STR_ACPI_TABLE_STRING);
      subtitle  text = STRING_TOKEN(STR_BLANK_STRING);

      goto ACPI_FORM_ID,
        prompt = STRING_TOKEN(STR_ACPI_FORM),
        help = STRING_TOKEN(STR_ACPI_FORM_HELP);

      grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.FacpRTCS4Wakeup,
        prompt      = STRING_TOKEN(STR_FACP_RTC_S4_STRING),
        help        = STRING_TOKEN(STR_FACP_RTC_S4__HELP),
        option text = STRING_TOKEN(STR_DISABLED_TEXT), value = 0, flags = 0;
        option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 1, flags = DEFAULT;
      endoneof;
      endif;

      grayoutif
        ideqval SystemConfig.UserAccessLevel == 2
        AND
        ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.IoApicMode,
        prompt      = STRING_TOKEN(STR_IO_APIC_MODE_STRING),
        help        = STRING_TOKEN(STR_IO_APIC_MODE_HELP),
        option text = STRING_TOKEN(STR_DISABLED_TEXT),  value = 0, flags = 0;
        option text = STRING_TOKEN(STR_ENABLED_TEXT),   value = 1, flags = DEFAULT;
      endoneof;
      endif; 
           
    endform; 