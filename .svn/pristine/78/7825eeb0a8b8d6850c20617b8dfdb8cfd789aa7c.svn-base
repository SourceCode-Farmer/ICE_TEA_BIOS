## @file
#  Platform Package Declaration file
#
#******************************************************************************
#* Copyright (c) 2014 - 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##

[Defines]
  DEC_SPECIFICATION              = 0x00010005
  PACKAGE_NAME                   = AlderLakeMultiBoardPkg
  PACKAGE_GUID                   = 00804A41-A178-4bf3-A7E0-185528586E0D

  PACKAGE_VERSION                = 0.1

[Includes]
  Include
#_Start_L05_FEATURE_
  InsydeL05ModulePkg/Include
  ClientOneSiliconPkg/Pch/Include
  ClientOneSiliconPkg/SystemAgent/Include
#_End_L05_FEATURE_

[Guids]
#[-start-210616-YUNLEI0102-add]
  gProjectPkgTokenSpaceGuid     = { 0x0fb76d37, 0x93bb, 0x4c48, {0xbe, 0xf0, 0xfd, 0xe9, 0x7c, 0x6a, 0x35, 0x28} }
#[-end-210616-YUNLEI0102-add]
[Ppis]
  gPeiOpalDeviceInfoPpiGuid     =  {0x35e19da5, 0xf946, 0x4ec5, {0xb8, 0xcc, 0x97, 0xf5, 0x7f, 0xd8, 0xc9, 0x3c}}

[Protocols]

[PcdsFeatureFlag]
#[-start-210616-YUNLEI0102-add]
  gProjectPkgTokenSpaceGuid.PcdLcfcFlashEcSupportEnable|FALSE|BOOLEAN|0x60000097
#[-end-210616-YUNLEI0102-add]

#[-start-210616-BAIN000013-add]#
  gH2OFlashDeviceEnableGuid.PcdW25Q128JVSpiEnable|TRUE|BOOLEAN|0x001B709D
  gH2OFlashDeviceEnableGuid.PcdW25Q64JVSpiEnable|TRUE|BOOLEAN|0x001C709D
#[-end-210616-BAIN000013-add]#
#[-start-210922-YUNLEI0136-modify]
  gH2OFlashDeviceEnableGuid.PcdMxic77l6450fSpiEnable|TRUE|BOOLEAN|0x0019709D
  gH2OFlashDeviceEnableGuid.PcdMxic25l12872fSpiEnable|TRUE|BOOLEAN|0x001A709D
  gH2OFlashDeviceEnableGuid.PcdXMC25QH128ASpiEnable|TRUE|BOOLEAN|0x001D709D
#[-start-211021-Dennis0006-add]
  gH2OFlashDeviceEnableGuid.PcdXMC25RH64CSpiEnable|TRUE|BOOLEAN|0x001E709D
#[-end-211021-Dennis0006-add]
#[-end-210922-YUNLEI0136-modify]
#[-start-211014-BAIN000051-add]#
  gH2OFlashDeviceEnableGuid.PcdGD25Q127CSpiEnable|TRUE|BOOLEAN|0x001C70AE
  gH2OFlashDeviceEnableGuid.PcdGD25B64CSpiEnable|TRUE|BOOLEAN|0x001C70BE
#[-end-211014-BAIN000051-add]#
#[-start-211208-QINGLIN0127-add]#
  gH2OFlashDeviceEnableGuid.PcdXMC25QH128CSpiEnable|TRUE|BOOLEAN|0x001C70BF
#[-end-211208-QINGLIN0127-add]#

#[-start-211108-Dennis0008-add]#
[PcdsDynamic]
  gProjectPkgTokenSpaceGuid.Pcd14inchSku|0x00|UINT8|0x70000104
  gProjectPkgTokenSpaceGuid.Pcd15inchSku|0x00|UINT8|0x70000105
  gProjectPkgTokenSpaceGuid.Pcd17inchSku|0x00|UINT8|0x70000106
#[-end-211108-Dennis0008-add]#

#[-start-210616-BAIN000013-add]#
  [PcdsDynamicEx]
  #
  # WINBOND   W25Q128JV
  #
  gH2OFlashDeviceGuid.PcdW25Q128JVSpi|{0}|VOID*|0x001B709D
  gH2OFlashDeviceMfrNameGuid.PcdW25Q128JVMfrName|"WINBOND"|VOID*|0x001B709D
  gH2OFlashDevicePartNameGuid.PcdW25Q128JVPartName|"W25Q128JV"|VOID*|0x001B709D
  gH2OFlashDeviceConfigGuid.PcdW25Q128JVConfig|{0}|VOID*|0x001B709D

  #
  #WINBOND W25Q64JV
  #
  gH2OFlashDeviceGuid.PcdW25Q64JVSpi|{0}|VOID*|0x001C709D
  gH2OFlashDeviceMfrNameGuid.PcdW25Q64JVSpiMfrName|"WINBOND"|VOID*|0x001C709D
  gH2OFlashDevicePartNameGuid.PcdW25Q64JVSpiPartName|"W25Q64JV"|VOID*|0x001C709D
  gH2OFlashDeviceConfigGuid.PcdW25Q64JVSpiConfig|{0}|VOID*|0x001C709D
#[-end-210616-BAIN000013-add]#
#[-start-210922-YUNLEI0136-modify]
  #
  # MXIC MX77L6450F
  #
  gH2OFlashDeviceGuid.PcdMxic77l6450fSpi|{0}|VOID*|0x0019709D
  gH2OFlashDeviceMfrNameGuid.PcdMxic77l6450fSpiMfrName|"MXIC"|VOID*|0x0019709D
  gH2OFlashDevicePartNameGuid.PcdMxic77l6450fSpiPartName|"MX77L6450F"|VOID*|0x0019709D
  gH2OFlashDeviceConfigGuid.PcdMxic77l6450fSpiConfig|{0}|VOID*|0x0019709D
  #
  # MXIC 25L12872F
  #
  gH2OFlashDeviceGuid.PcdMxic25l12872fSpi|{0}|VOID*|0x001A709D
  gH2OFlashDeviceMfrNameGuid.PcdMxic25l12872fSpiMfrName|"MXIC"|VOID*|0x001A709D
  gH2OFlashDevicePartNameGuid.PcdMxic25l12872fSpiPartName|"25L12872F"|VOID*|0x001A709D
  gH2OFlashDeviceConfigGuid.PcdMxic25l12872fSpiConfig|{0}|VOID*|0x001A709D
  #
  # XMC  25QH128A
  #
  gH2OFlashDeviceGuid.PcdXMC25QH128ASpi|{0}|VOID*|0x001D709D
  gH2OFlashDeviceMfrNameGuid.PcdXMC25QH128ASpiMfrName|"XMC"|VOID*|0x001D709D
  gH2OFlashDevicePartNameGuid.PcdXMC25QH128ASpiPartName|"25QH128A"|VOID*|0x001D709D
  gH2OFlashDeviceConfigGuid.PcdXMC25QH128ASpiConfig|{0}|VOID*|0x001D709D
#[-start-211021-Dennis0006-add]
  #
  # XMC  XM25RH64C
  #
  gH2OFlashDeviceGuid.PcdXMC25RH64CSpi|{0}|VOID*|0x001E709D
  gH2OFlashDeviceMfrNameGuid.PcdXMC25RH64CSpiMfrName|"XMC"|VOID*|0x001E709D
  gH2OFlashDevicePartNameGuid.PcdXMC25RH64CSpiPartName|"25RH64C"|VOID*|0x001E709D
  gH2OFlashDeviceConfigGuid.PcdXMC25RH64CSpiConfig|{0}|VOID*|0x001E709D
#[-end-211021-Dennis0006-add]
#[-end-210922-YUNLEI0136-modify]

#[-start-211014-BAIN000051-add]#
  #
  # GD25Q127C
  #
  gH2OFlashDeviceGuid.PcdGD25Q127CSpi|{0}|VOID*|0x001C70AE
  gH2OFlashDeviceMfrNameGuid.PcdGD25Q127CMfrName|"GD"|VOID*|0x001C70AE
  gH2OFlashDevicePartNameGuid.PcdGD25Q127CPartName|"25Q127C"|VOID*|0x001C70AE
  gH2OFlashDeviceConfigGuid.PcdGD25Q127CConfig|{0}|VOID*|0x001C70AE

  #
  #GD25B64C
  #
  gH2OFlashDeviceGuid.PcdGD25B64CSpi|{0}|VOID*|0x001C70BE
  gH2OFlashDeviceMfrNameGuid.PcdGD25B64CSpiMfrName|"GD"|VOID*|0x001C70BE
  gH2OFlashDevicePartNameGuid.PcdGD25B64CSpiPartName|"25B64C"|VOID*|0x001C70BE
  gH2OFlashDeviceConfigGuid.PcdGD25B64CSpiConfig|{0}|VOID*|0x001C70BE
#[-end-211014-BAIN000051-add]#

#[-start-211208-QINGLIN0127-add]#
  #
  # XMC  XM25QH128C
  #
  gH2OFlashDeviceGuid.PcdXMC25QH128CSpi|{0}|VOID*|0x001C70BF
  gH2OFlashDeviceMfrNameGuid.PcdXMC25QH128CSpiMfrName|"XMC"|VOID*|0x001C70BF
  gH2OFlashDevicePartNameGuid.PcdXMC25QH128CSpiPartName|"25QH128C"|VOID*|0x001C70BF
  gH2OFlashDeviceConfigGuid.PcdXMC25QH128CSpiConfig|{0}|VOID*|0x001C70BF
#[-end-211208-QINGLIN0127-add]#
  
[PcdsFixedAtBuild]
#[-start-210623-YUNLEI0104-add]
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarSub1Base|0x00000000|UINT32|0x18656001
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarSub1Size|0x00000000|UINT32|0x18656002
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarSub2Base|0x00000000|UINT32|0x18656003
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarSub2Size|0x00000000|UINT32|0x18656004
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarDebugBase|0x00000000|UINT32|0x1865607
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarDebugSize|0x00000000|UINT32|0x1865608
#[-end-210623-YUNLEI0104-add]

[LibraryClasses]
