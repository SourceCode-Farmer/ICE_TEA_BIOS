## @file
# CPU Exception Handler library instance for SMM modules.
#
#******************************************************************************
#* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
## @file
#  CPU Exception Handler library instance for SMM modules.
#
#  Copyright (c) 2013 - 2018, Intel Corporation. All rights reserved.<BR>
#  SPDX-License-Identifier: BSD-2-Clause-Patent
#
##

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = SmmCpuExceptionHandlerLib
  MODULE_UNI_FILE                = SmmCpuExceptionHandlerLib.uni
  FILE_GUID                      = 8D2C439B-3981-42ff-9CE5-1B50ECA502D6
  MODULE_TYPE                    = DXE_SMM_DRIVER
  VERSION_STRING                 = 1.1
  LIBRARY_CLASS                  = CpuExceptionHandlerLib|DXE_SMM_DRIVER MM_STANDALONE MM_CORE_STANDALONE

#
# The following information is for reference only and not required by the build tools.
#
#  VALID_ARCHITECTURES           = IA32 X64
#

[Sources.Ia32]
  Ia32/ExceptionHandlerAsm.nasm
  Ia32/ExceptionTssEntryAsm.nasm
  Ia32/ArchExceptionHandler.c
  Ia32/ArchInterruptDefs.h

[Sources.X64]
  X64/Xcode5ExceptionHandlerAsm.nasm
  X64/ArchExceptionHandler.c
  X64/ArchInterruptDefs.h

[Sources.common]
  CpuExceptionCommon.h
  CpuExceptionCommon.c
  PeiDxeSmmCpuException.c
  SmmException.c

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
  InsydeModulePkg/InsydeModulePkg.dec

[LibraryClasses]
  BaseLib
  SerialPortLib
  PrintLib
  SynchronizationLib
  LocalApicLib
  PeCoffGetEntryPointLib
  DebugLib
  VmgExitLib
  PcdLib
  H2OCpLib
  H2OSecurityEventLib

[Guids]
  gH2OBaseCpCpuExceptionGuid

[FeaturePcd]
  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmStackGuard
  gInsydeTokenSpaceGuid.PcdH2OBaseCpCpuExceptionSupported

[Pcd]
  gInsydeTokenSpaceGuid.PcdH2OCpuExceptionExcludedList
  gInsydeTokenSpaceGuid.PcdH2OBaseCpSecurityEventReportSupported
  gInsydeTokenSpaceGuid.PcdH2OSecurityEventPageFaultSupported
  gInsydeTokenSpaceGuid.PcdH2OSecurityEventGeneralProtectionFaultSupported
