## @file
#  Component description file for Vfr Application
#
#******************************************************************************
#* Copyright (c) 2015 - 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = VfrApp
  FILE_GUID                      = 3935B0A1-A182-4887-BC56-675528E78878
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = VfrAppMain

[Sources]
  VfrAppStrings.uni
  VfrAppVfr.vfr
  SetupPagePanel.hfr
  SetupPagePanel.uni
  HelpTextPanel.hfr
  HelpTextPanel.uni
  HotKeyPanel.hfr
  HotKeyPanel.uni
  VfrAppLegacyBios.hfr
  VfrAppLegacyBios.uni

  VfrApp.c
  VfrAppMisc.c
  VfrAppConOut.c
  VfrAppLegacyBios.c

[Packages]
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeSetupPkg/InsydeSetupPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec

[LibraryClasses]
  UefiDriverEntryPoint
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  UefiLib
  BaseLib
  BaseMemoryLib
  MemoryAllocationLib
  HiiLib
  DebugLib
  PcdLib
  VariableLib
  DevicePathLib

[Guids]
  gDriverSampleFormSetGuid
  gH2ODisplayEngineLocalMetroGuid
  gH2ODisplayEngineLocalTextGuid
  gEfiIfrTianoGuid
  gSystemConfigurationGuid
  gEfiIfrRefreshIdOpGuid

[Protocols]
  gEfiHiiConfigAccessProtocolGuid
  gEfiDevicePathProtocolGuid
  gH2ODialogProtocolGuid
  gH2ODisplayEngineProtocolGuid
  gEfiBdsArchProtocolGuid
  gEfiDevicePathToTextProtocolGuid
  gEfiComponentName2ProtocolGuid
  gEfiLegacyBiosProtocolGuid
  gEfiGraphicsOutputProtocolGuid
  gH2OFormBrowserProtocolGuid
  gEfiSimpleTextInputExProtocolGuid
  gEfiHiiPopupProtocolGuid

[FeaturePcd]
  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalTextDESupported
  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported

[Depex]
  gEfiHiiConfigRoutingProtocolGuid  AND
  gEfiHiiDatabaseProtocolGuid       AND
  gEfiVariableArchProtocolGuid      AND
  gEfiVariableWriteArchProtocolGuid AND
  gEfiBdsArchProtocolGuid           AND
  gEfiSetupUtilityProtocolGuid