/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _ADVANCE_CALLBACK_H
#define _ADVANCE_CALLBACK_H

#include <SetupUtility.h>
#include <DisplaySelection.h>
#include <MeBiosPayloadHob.h>
#include <MeBiosPayloadData.h>
#include <Library/DxeMeLib.h>
#include "IccSetup.h"
#include "PchSetup.h"
#include "AcpiSetup.h"
#include "CpuSetup.h"
#include "MeSetup.h"
#include "SaSetup.h"

#define SYSTEM_MEMORY_512M    0x200
#define SYSTEM_MEMORY_1G      0x400
#define MAXPCISLOT            2

extern ADVANCE_CONFIGURATION            mAdvConfig;
extern  IDE_CONFIG                      *mIdeConfig;
EFI_STATUS
AdvanceCallbackRoutine (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  EFI_BROWSER_ACTION                     Action,
  IN  EFI_QUESTION_ID                        QuestionId,
  IN  UINT8                                  Type,
  IN  EFI_IFR_TYPE_VALUE                     *Value,
  OUT EFI_BROWSER_ACTION_REQUEST             *ActionRequest
  );

EFI_STATUS
AdvanceCallbackRoutineByAction (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  EFI_BROWSER_ACTION                     Action,
  IN  EFI_QUESTION_ID                        QuestionId,
  IN  UINT8                                  Type,
  IN  EFI_IFR_TYPE_VALUE                     *Value,
  OUT EFI_BROWSER_ACTION_REQUEST             *ActionRequest
  );

EFI_STATUS
InstallAdvanceCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  );

EFI_STATUS
UninstallAdvanceCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle
  );

EFI_STATUS
InitAdvanceMenu (
  IN EFI_HII_HANDLE                         HiiHandle
  );
EFI_STATUS
UpdateHDCConfigure (
  IN  EFI_HII_HANDLE                    HiiHandle,
  IN  CHIPSET_CONFIGURATION              *Buffer
  );

EFI_STATUS
GetIdeDevNameString (
  IN OUT CHAR16 *                        *DevNameString
  );

PCI_DEVICE_PATH *
GetPciLocation(
  IN  EFI_DEVICE_PATH_PROTOCOL  *SataDevicePath,
  OUT UINT8                     *SataBus
  );

BOOLEAN
IsOnBoardPciDevice (
  IN UINT32                     Bus,
  IN UINT32                     Device,
  IN UINT32                     Function
  );

EFI_STATUS
InitIdeConfig (
  IDE_CONFIG                             *IdeConfig
  );

BOOLEAN
CheckSioConflict (
  IN  CHIPSET_CONFIGURATION                    *MyIfrNVData
  );

BOOLEAN
CheckSioAndPciSoltConflict (
  IN  CHIPSET_CONFIGURATION                    *MyIfrNVData,
  IN  UINT8                                   *PciIrqData,
  OUT UINT8                                   *DeviceKind
  );

BOOLEAN
CheckPciSioConflict (
  IN  CHIPSET_CONFIGURATION                    *MyIfrNVData,
  IN  UINT8                                   *PciIrqData,
  OUT UINT8                                   *DeviceKind
  );

EFI_STATUS
UpdateACPIDebugInfo (
  IN  EFI_HII_HANDLE                      HiiHandle
  );


EFI_STATUS
InitMeUnconfigOnRtc (
  IN  EFI_HII_HANDLE                      HiiHandle
  );
//[-start-210324-IB16740136-modify]// TbtFormCallBackFunction has changed to DTbtFormCallBackFunction in Rc2117
EFI_STATUS
EFIAPI
DTbtFormCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );
//[-end-210324-IB16740136-modify]//
//[-start-210324-IB16740136-remove]//for build error, DTbtTypeCallBackFunction has removed in RC2117
//EFI_STATUS
//EFIAPI
//DTbtTypeCallBackFunction(
//  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
//  IN EFI_BROWSER_ACTION                   Action,
//  IN EFI_QUESTION_ID                      KeyValue,
//  IN UINT8                                Type,
//  IN EFI_IFR_TYPE_VALUE                   *Value,
//  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
//  );
//[-end-210324-IB16740136-remove]//

EFI_STATUS
EFIAPI
TbtOsSelectorFormCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );

VOID
InitSaStrings (
  EFI_HII_HANDLE                          HiiHandle,
  UINT16                                  Class
  );

VOID
InitBoardStrings (
  EFI_HII_HANDLE                          HiiHandle,
  UINT16                                  Class
  );

VOID
InitMeInfo (
  EFI_HII_HANDLE                          HiiHandle,
  UINT16                                  Class
  );

VOID
InitConnectivityStrings (
  EFI_HII_HANDLE                          HiiHandle,
  UINT16                                  Class
  );

VOID
MeExtractConfig (
  );

VOID
MeRouteConfig (
  );

EFI_STATUS
EFIAPI
MeFormCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );

EFI_STATUS
SyncIccSetupDataWithFormBrowser (
  IN EFI_BROWSER_ACTION                   Action
  );

EFI_STATUS
EFIAPI
DebugFormCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );

EFI_STATUS
EFIAPI
TxtPolicyCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );

EFI_STATUS
EFIAPI
CnvFormCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );

EFI_STATUS
EFIAPI
TBTSecurityCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );

EFI_STATUS
EFIAPI
ThermalFunctionCallback (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );

EFI_STATUS
EFIAPI
VtioFormCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );

EFI_STATUS
EFIAPI
StateAfterG3CallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );

EFI_STATUS
EFIAPI
VmdCallback (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );

EFI_STATUS
EFIAPI
Rtd3SupportCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  );
#endif
