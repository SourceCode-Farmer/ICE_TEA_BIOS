//;******************************************************************************
//;* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************

#include "GamingHome.h"

EFI_CALLBACK_INFO                       *mGamingHomeCallBackInfo;
BOOLEAN                                 mFirstInScu = TRUE;

EFI_STATUS
GamingHomeCallbackRoutineByAction (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  )
{
  EFI_STATUS                            Status;
  EFI_CALLBACK_INFO                     *CallbackInfo;
  UINTN                                 BufferSize;
  EFI_GUID                              VarStoreGuid = SYSTEM_CONFIGURATION_GUID;

  if ((This == NULL) ||
      ((Value == NULL) &&
       (Action != EFI_BROWSER_ACTION_FORM_OPEN) &&
       (Action != EFI_BROWSER_ACTION_FORM_CLOSE)) ||
      (ActionRequest == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  CallbackInfo   = EFI_CALLBACK_INFO_FROM_THIS (This);
  BufferSize     = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemConfig");
  Status         = EFI_UNSUPPORTED;
  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;

  switch (Action) {

  case EFI_BROWSER_ACTION_FORM_OPEN:
    if (QuestionId == 0) {
      if (mFirstInScu) {
        //
        // Record changed question prompt at first time into SCU.
        //
        mFirstInScu = FALSE;
        L05RecordChangedQuestionPrompt ();
      }
    }

    break;

  case EFI_BROWSER_ACTION_FORM_CLOSE:
    if (QuestionId == 0) {
    }

    break;

  case EFI_BROWSER_ACTION_CHANGING:
    Status = EFI_SUCCESS;
    break;

  case EFI_BROWSER_ACTION_DEFAULT_MANUFACTURING:
    if (QuestionId == KEY_SCAN_F9) {
      Status = HotKeyCallBack (
                 This,
                 Action,
                 QuestionId,
                 Type,
                 Value,
                 ActionRequest
                 );
    }

    //
    // avoid GetQuestionDefault execute ExtractConfig
    //
    return EFI_SUCCESS;

  default:
    break;
  }

  return Status;
}

/*++

Routine Description:

  This is the callback function for the Main Menu.

Arguments:

Returns:

--*/
EFI_STATUS
GamingHomeCallbackRoutine (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  )
{
  EFI_STATUS                            Status;
  CHAR16                                *StringPtr;
  SYSTEM_CONFIGURATION                  *MyIfrNvData;
  EFI_HII_HANDLE                        HiiHandle;
  EFI_CALLBACK_INFO                     *CallbackInfo;
  UINTN                                 BufferSize;
  EFI_GUID                              VarStoreGuid = SYSTEM_CONFIGURATION_GUID;

  BufferSize = 0;

  if (Action != EFI_BROWSER_ACTION_CHANGED) {
    return GamingHomeCallbackRoutineByAction (This, Action, QuestionId, Type, Value, ActionRequest);
  }

  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;
  CallbackInfo   = EFI_CALLBACK_INFO_FROM_THIS (This);

  BufferSize     = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemConfig");

  MyIfrNvData    = (SYSTEM_CONFIGURATION *) gSUBrowser->SCBuffer;

  Status         = EFI_SUCCESS;
  StringPtr      = NULL;
  HiiHandle      = CallbackInfo->HiiHandle;

  switch (QuestionId) {

  default:
    Status = HotKeyCallBack (
               This,
               Action,
               QuestionId,
               Type,
               Value,
               ActionRequest
               );
    break;
  }

  return Status;
}

EFI_STATUS
InstallGamingHomeCallbackRoutine (
  IN EFI_HANDLE                         DriverHandle,
  IN EFI_HII_HANDLE                     HiiHandle
  )
{
  EFI_STATUS                            Status;
  EFI_GUID                              FormsetGuid = FORMSET_ID_GUID_GAMING_HOME;

  mGamingHomeCallBackInfo = AllocatePool (sizeof (EFI_CALLBACK_INFO));

  if (mGamingHomeCallBackInfo == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  mGamingHomeCallBackInfo->Signature                    = EFI_CALLBACK_INFO_SIGNATURE;
  mGamingHomeCallBackInfo->DriverCallback.ExtractConfig = gSUBrowser->ExtractConfig;
  mGamingHomeCallBackInfo->DriverCallback.RouteConfig   = gSUBrowser->RouteConfig;
  mGamingHomeCallBackInfo->DriverCallback.Callback      = GamingHomeCallbackRoutine;
  mGamingHomeCallBackInfo->HiiHandle                    = HiiHandle;
  CopyGuid (&mGamingHomeCallBackInfo->FormsetGuid, &FormsetGuid);

  //
  // Install protocol interface
  //
  Status = gBS->InstallProtocolInterface (
                  &DriverHandle,
                  &gEfiHiiConfigAccessProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  &mGamingHomeCallBackInfo->DriverCallback
                  );
  ASSERT_EFI_ERROR (Status);

  return Status;
}

EFI_STATUS
UninstallGamingHomeCallbackRoutine (
  IN EFI_HANDLE                         DriverHandle
  )
{

  EFI_STATUS                            Status;

  if (mGamingHomeCallBackInfo == NULL) {
    return EFI_SUCCESS;
  }

  Status = gBS->UninstallProtocolInterface (
                  DriverHandle,
                  &gEfiHiiConfigAccessProtocolGuid,
                  &mGamingHomeCallBackInfo->DriverCallback
                  );

  ASSERT_EFI_ERROR (Status);
  gBS->FreePool (mGamingHomeCallBackInfo);
  mGamingHomeCallBackInfo = NULL;
  return Status;
}

