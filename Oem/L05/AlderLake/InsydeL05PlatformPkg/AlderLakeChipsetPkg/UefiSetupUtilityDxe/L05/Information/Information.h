//;******************************************************************************
//;* Copyright (c) 1983-2019, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
//;
//; Abstract:
//;

#ifndef _INFORMATION_CALLBACK_H
#define _INFORMATION_CALLBACK_H

#include "SetupUtility.h"
#include "OemInformation.h"
#include "L05ExitDiscardingChanges.h"
//#include <Library/BvdtLib.h>  // move to relative .c to avoid redefining SHA256_DIGEST_SIZE with BootGuardPlatformLib.h [AlderLake only]

//
// InsydeModulePkg\Library\SetupUtilityLib\SetupUtilityLibCommon.h
//
#define PRINTABLE_LANGUAGE_NAME_STRING_ID   0x0001

EFI_STATUS
InformationCallbackRoutine (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  );

EFI_STATUS
InstallInformationCallbackRoutine (
  IN EFI_HANDLE                         DriverHandle,
  IN EFI_HII_HANDLE                     HiiHandle
  );

EFI_STATUS
UninstallInformationCallbackRoutine (
  IN EFI_HANDLE                         DriverHandle
  );

EFI_STATUS
InitInformationMenu (
  IN EFI_HII_HANDLE                     HiiHandle
  );

EFI_STATUS
GetSetupUtilityBrowserData (
  OUT SETUP_UTILITY_BROWSER_DATA        **SuBrowser
  );

#endif