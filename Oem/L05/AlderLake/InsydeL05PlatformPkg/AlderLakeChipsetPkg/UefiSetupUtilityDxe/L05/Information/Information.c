//;******************************************************************************
//;* Copyright (c) 1983-2019, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#include "Information.h"
#include <Library/BvdtLib.h>

EFI_CALLBACK_INFO                       *mInformationCallBackInfo;
#ifndef L05_GAMING_UI_ENABLE
BOOLEAN                                 mFirstInScu = TRUE;
#endif

/**
  Update the language item with all of support languages for main menu use.

  @param  HiiHandle                     Target EFI_HII_HANDLE instance.

  @retval EFI_SUCCESS                   Udpate string token successfully.
  @return Other                         Error occurred during execution.

**/
STATIC
EFI_STATUS
UpdateLanguage (
  IN  EFI_HII_HANDLE                    HiiHandle
  )
{
  EFI_STATUS                            Status;
  KERNEL_CONFIGURATION                  SetupNvData;
  UINT16                                VarOffset;
  UINT16                                *StringBuffer;
  UINTN                                 Index;
  UINTN                                 OptionCount;
  STRING_REF                            Token;
  CHAR8                                 *LanguageString;
  UINTN                                 LangNum;
  VOID                                  *StartOpCodeHandle;
  VOID                                  *OptionsOpCodeHandle;
  EFI_IFR_GUID_LABEL                    *StartLabel;
  SETUP_UTILITY_BROWSER_DATA            *SuBrowser;
  CHAR8                                 *PlatformLangVar;
  UINT8                                 PlatformLangValue;
  CHAR8                                 DefaultLangCode[] = "en-US";
  UINT8                                 DefaultLangValue;

  Status              = EFI_SUCCESS;
  StringBuffer        = NULL;
  LanguageString      = NULL;
  StartOpCodeHandle   = NULL;
  OptionsOpCodeHandle = NULL;
  StartLabel          = NULL;
  SuBrowser           = NULL;
  PlatformLangVar     = NULL;

  Status = GetSetupUtilityBrowserData (&SuBrowser);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  GetLangDatabase (
    &LangNum,
    (UINT8 **) &LanguageString
    );

  PlatformLangVar = CommonGetVariableData (
                      L"PlatformLang",
                      &gEfiGlobalVariableGuid
                      );

  //
  // Init OpCode Handle and Allocate space for creation of UpdateData Buffer
  //
  StartOpCodeHandle   = HiiAllocateOpCodeHandle ();
  OptionsOpCodeHandle = HiiAllocateOpCodeHandle ();

  //
  // Create Hii Extend Label OpCode as the start opcode
  //
  StartLabel               = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (StartOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
  StartLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
  StartLabel->Number       = L05_SETUP_UTILITY_LANG_LABEL;

  OptionCount = 0;

  DefaultLangValue  = (UINT8) LangNum;
  PlatformLangValue = (UINT8) LangNum;

  for (Index = 0; Index < LangNum; Index++) {
    StringBuffer = HiiGetString (
                     HiiHandle,
                     PRINTABLE_LANGUAGE_NAME_STRING_ID,
                     &LanguageString[Index * RFC_3066_ENTRY_SIZE]
                     );
    ASSERT (StringBuffer != NULL);

    Token = HiiSetString (HiiHandle, 0, StringBuffer, NULL);
    FreePool (StringBuffer);

    HiiCreateOneOfOptionOpCode (
      OptionsOpCodeHandle,
      Token,
      OptionCount == 0 ? EFI_IFR_OPTION_DEFAULT : 0,
      EFI_IFR_NUMERIC_SIZE_1,
      (UINT8) OptionCount
      );

    if (PlatformLangVar != NULL && AsciiStrCmp (PlatformLangVar, &LanguageString[Index * RFC_3066_ENTRY_SIZE]) == 0) {
      PlatformLangValue = (UINT8) Index;
    }

    if (AsciiStrCmp (DefaultLangCode, &LanguageString[Index * RFC_3066_ENTRY_SIZE]) == 0) {
      DefaultLangValue = (UINT8) Index;
    }

    OptionCount++;
  }

  VarOffset = (UINT16) ((UINTN) (&SetupNvData.Language) - (UINTN) (&SetupNvData));

  HiiCreateOneOfOpCode (
    StartOpCodeHandle,
    KEY_LANGUAGE_UPDATE,
    CONFIGURATION_VARSTORE_ID,
    VarOffset,
    STRING_TOKEN (L05_STR_LANGUAGE_STRING),
    STRING_TOKEN (L05_STR_LANGUAGE_HELP),
    EFI_IFR_FLAG_CALLBACK,
    EFI_IFR_NUMERIC_SIZE_1,
    OptionsOpCodeHandle,
    NULL
    );

  Status = HiiUpdateForm (
             HiiHandle,
             NULL,
             ROOT_FORM_ID,
             StartOpCodeHandle,
             NULL
             );

  //
  // Make sure that PlatformLang variable is included in language list.
  // Sync language value between PlatformLang variable and language of Setup variable.
  //
  if (PlatformLangValue == LangNum) {
    PlatformLangValue = DefaultLangValue;
    CommonSetVariable (
      L"PlatformLang",
      &gEfiGlobalVariableGuid,
      EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
      sizeof (DefaultLangCode),
      DefaultLangCode
      );
  }

  if (((KERNEL_CONFIGURATION *) SuBrowser->SCBuffer)->Language != PlatformLangValue) {
    ((KERNEL_CONFIGURATION *) SuBrowser->SCBuffer)->Language = PlatformLangValue;
    UpdateStringToken ((KERNEL_CONFIGURATION *) SuBrowser->SCBuffer);
  }

  if (LangNum != 0) {
    FreePool (LanguageString);
  }

  HiiFreeOpCodeHandle (StartOpCodeHandle);
  HiiFreeOpCodeHandle (OptionsOpCodeHandle);

  if (PlatformLangVar != NULL) {
    FreePool (PlatformLangVar);
  }

  return Status;
}

/*++

Routine Description:

  Using BVDT data to update STR_MISC_BIOS_VERSION string token

Arguments:

  HiiHandle - Target EFI_HII_HANDLE instance

Returns:

  EFI_SUCESS - Udpate string token successfule
  Other      - Cannot find HII protocol

--*/
STATIC
EFI_STATUS
UpdateBiosVersionFromBvdt (
  IN EFI_HII_HANDLE                     HiiHandle
  )
{
  UINTN                                 StrSize;
  CHAR16                                Str[BVDT_MAX_STR_SIZE];
  EFI_STATUS                            Status;

  StrSize = BVDT_MAX_STR_SIZE;
  Status = GetBvdtInfo ((BVDT_TYPE) BvdtBiosVer, &StrSize, Str);

  if (!EFI_ERROR (Status)) {
//    Status = IfrLibSetString (HiiHandle, STRING_TOKEN (STR_MISC_BIOS_VERSION), Str);
    Status = HiiSetString (HiiHandle, STRING_TOKEN (STR_MISC_BIOS_VERSION), Str, NULL);
  }

  return Status;
}

EFI_STATUS
InformationCallbackRoutineByAction (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  )
{
  EFI_STATUS                            Status;
  EFI_CALLBACK_INFO                     *CallbackInfo;
  UINTN                                 BufferSize;
  EFI_GUID                              VarStoreGuid = SYSTEM_CONFIGURATION_GUID;

  if ((This == NULL) ||
      ((Value == NULL) &&
       (Action != EFI_BROWSER_ACTION_FORM_OPEN) &&
       (Action != EFI_BROWSER_ACTION_FORM_CLOSE)) ||
      (ActionRequest == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  CallbackInfo   = EFI_CALLBACK_INFO_FROM_THIS (This);
  BufferSize     = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemConfig");
  Status         = EFI_UNSUPPORTED;
  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;

  switch (Action) {

  case EFI_BROWSER_ACTION_FORM_OPEN:
    if (QuestionId == 0) {
      Status = SetupVariableConfig (
                 &VarStoreGuid,
                 L"SystemConfig",
                 BufferSize,
                 (UINT8 *) gSUBrowser->SCBuffer,
                 FALSE
                 );

#ifndef L05_GAMING_UI_ENABLE
      if (mFirstInScu) {
        //
        // Record changed question prompt at first time into SCU.
        //
        mFirstInScu = FALSE;
        L05RecordChangedQuestionPrompt ();
      }
#endif
    }

    break;

  case EFI_BROWSER_ACTION_FORM_CLOSE:
    if (QuestionId == 0) {
      Status = SetupVariableConfig (
                 &VarStoreGuid,
                 L"SystemConfig",
                 BufferSize,
                 (UINT8 *) gSUBrowser->SCBuffer,
                 TRUE
                 );
    }

    break;

  case EFI_BROWSER_ACTION_CHANGING:
    Status = EFI_SUCCESS;
    break;

  case EFI_BROWSER_ACTION_DEFAULT_MANUFACTURING:
    if (QuestionId == KEY_SCAN_F9) {
      Status = HotKeyCallBack (
                 This,
                 Action,
                 QuestionId,
                 Type,
                 Value,
                 ActionRequest
                 );

      SetupVariableConfig (
        &VarStoreGuid,
        L"SystemConfig",
        sizeof (KERNEL_CONFIGURATION),
        (UINT8 *) gSUBrowser->SCBuffer,
        FALSE
        );
    }

    //
    // avoid GetQuestionDefault execute ExtractConfig
    //
    return EFI_SUCCESS;

  default:
    break;
  }

  return Status;
}

/*++

Routine Description:

  This is the callback function for the Information Menu.

Arguments:

Returns:

--*/
EFI_STATUS
InformationCallbackRoutine (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  )
{
  EFI_STATUS                            Status;
  CHAR16                                *StringPtr;
  SYSTEM_CONFIGURATION                  *MyIfrNvData;
  EFI_HII_HANDLE                        HiiHandle;
  EFI_CALLBACK_INFO                     *CallbackInfo;
  UINTN                                 BufferSize;
  EFI_GUID                              VarStoreGuid = SYSTEM_CONFIGURATION_GUID;

  BufferSize = 0;

  if (Action != EFI_BROWSER_ACTION_CHANGED) {
    return InformationCallbackRoutineByAction (This, Action, QuestionId, Type, Value, ActionRequest);
  }

  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;
  CallbackInfo   = EFI_CALLBACK_INFO_FROM_THIS (This);
  BufferSize     = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemConfig");
  MyIfrNvData    = (SYSTEM_CONFIGURATION *) gSUBrowser->SCBuffer;
  Status         = EFI_SUCCESS;
  StringPtr      = NULL;
  HiiHandle      = CallbackInfo->HiiHandle;

  Status = SetupVariableConfig (
             &VarStoreGuid,
             L"SystemConfig",
             BufferSize,
             (UINT8 *) gSUBrowser->SCBuffer,
             TRUE
             );

  switch (QuestionId) {

  case KEY_LANGUAGE_UPDATE:
    UpdateLangItem (This, &(((KERNEL_CONFIGURATION *) gSUBrowser->SCBuffer)->Language));
    UpdateStringToken ((KERNEL_CONFIGURATION *) gSUBrowser->SCBuffer);
    UpdateStorageDeviceModelName (HiiHandle);
    BrowserRefreshFormSet ();
    UpdateSecurityStates (HiiHandle);
    break;

  default:
    Status = HotKeyCallBack (
               This,
               Action,
               QuestionId,
               Type,
               Value,
               ActionRequest
               );
    break;
  }

  BufferSize = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemConfig");
  SetupVariableConfig (
    &VarStoreGuid,
    L"SystemConfig",
    BufferSize,
    (UINT8 *) gSUBrowser->SCBuffer,
    FALSE
    );

  return Status;
}

EFI_STATUS
InstallInformationCallbackRoutine (
  IN EFI_HANDLE                         DriverHandle,
  IN EFI_HII_HANDLE                     HiiHandle
  )
{
  EFI_STATUS                            Status;
  EFI_GUID                              FormsetGuid = FORMSET_ID_GUID_INFORMATION;

  mInformationCallBackInfo = AllocatePool (sizeof (EFI_CALLBACK_INFO));

  if (mInformationCallBackInfo == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  mInformationCallBackInfo->Signature                    = EFI_CALLBACK_INFO_SIGNATURE;
  mInformationCallBackInfo->DriverCallback.ExtractConfig = gSUBrowser->ExtractConfig;
  mInformationCallBackInfo->DriverCallback.RouteConfig   = gSUBrowser->RouteConfig;
  mInformationCallBackInfo->DriverCallback.Callback      = InformationCallbackRoutine;
  mInformationCallBackInfo->HiiHandle                    = HiiHandle;
  CopyGuid (&mInformationCallBackInfo->FormsetGuid, &FormsetGuid);

  //
  // Install protocol interface
  //
  Status = gBS->InstallProtocolInterface (
                  &DriverHandle,
                  &gEfiHiiConfigAccessProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  &mInformationCallBackInfo->DriverCallback
                  );
  ASSERT_EFI_ERROR (Status);

  Status = InitInformationMenu (HiiHandle);

  return Status;
}

EFI_STATUS
UninstallInformationCallbackRoutine (
  IN EFI_HANDLE                         DriverHandle
  )
{

  EFI_STATUS                            Status;

  if (mInformationCallBackInfo == NULL) {
    return EFI_SUCCESS;
  }

  Status = gBS->UninstallProtocolInterface (
                  DriverHandle,
                  &gEfiHiiConfigAccessProtocolGuid,
                  &mInformationCallBackInfo->DriverCallback
                  );

  ASSERT_EFI_ERROR (Status);
  gBS->FreePool (mInformationCallBackInfo);
  mInformationCallBackInfo = NULL;
  return Status;
}

EFI_STATUS
InitInformationMenu (
  IN EFI_HII_HANDLE                     HiiHandle
  )
{
  EFI_STATUS                            Status = EFI_SUCCESS;

  UpdateBiosVersionFromBvdt (HiiHandle);

  //
  // Update System Information
  //
  UpdateOemSystemInfo (HiiHandle);
  UpdateStorageDeviceModelName (HiiHandle);
  UpdateECVersionStringInSCU (HiiHandle);

  //
  // Update information of OA3 Key ID (OA3 Key ID is NOT OA 3.0 Key from ACPI MSDM table!!!)
  //
  UpdateOa3KeyIdInSCU (HiiHandle);

  //
  // Update Security Boot Information
  //
  UpdateSecurityStates (HiiHandle);

  //
  // Update Current Language
  //
  Status = UpdateLanguage (HiiHandle);

  return Status;
}
