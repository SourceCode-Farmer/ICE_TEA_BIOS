//;******************************************************************************
//;* Copyright (c) 1983-2015, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#include "OemInformation.h"
#include <Library/UefiLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/DxeOemSvcKernelLib.h>
#include <Library/DxeServicesLib.h>
#include <Library/FeatureLib/OemSvcUpdateEcVersionStringInScu.h>
#include <Library/FeatureLib/OemSvcSmbiosOverride.h>
#include <Protocol/L05Variable.h>
#include <Guid/L05VariableTool.h>
#include <L05Config.h>
#include <SetupConfig.h>
#include <IndustryStandard/Oa3_0.h>
#include <L05Slp20Config.h>
//[-start-210608-FLINT00003-add]//
#include <Library/LfcEcLib.h>
//[-end-210608-FLINT00003-add]//

//
// In L05PlatformPkg\SharkBayChipsetPkg\UefiSetupUtilityDxe\SetupUtilityLib\Main\SystemInformation.c
//
EFI_STATUS
GetOptionalStringByIndex (
  IN  CHAR8                             *OptionalStrStart,
  IN  UINT8                             Index,
  OUT CHAR16                            **String
  );

//
// In InsydeL05PlatformPkg\InsydeModulePkg\Library\GenericBdsLib\BdsMisc.c
//
EFI_STATUS
GetAllHwBootDeviceInfo (
  OUT UINTN                             *HwBootDeviceInfoCount,
  OUT HARDWARE_BOOT_DEVICE_INFO         **HwBootDeviceInfo
  );

/**
  Get model name string from device name.

  After Kernel 05.42.46 [IB15850250], HwDeviceName will display serial number + model name for all devices.
  So get model name string from device name need ignore serial number.

  @param  HwDeviceName                  The pointer to HW device name.
  @param  ModelNameString               The pointer to model name string.

  @retval EFI_SUCCESS                   Get information successfully.
  @return EFI_INVALID_PARAMETER         Invalid input parameter.
**/
EFI_STATUS
GetModelNameStringFromDeviceName (
  IN  CHAR16                            *HwDeviceName,
  OUT CHAR16                            **ModelNameString
  )
{
  UINTN                                 Index;

  Index = 0;
  *ModelNameString = HwDeviceName;

#if FixedPcdGetBool (PcdH2OCcbVersion) >= 0x05424600
  if (StrLen (HwDeviceName) == 0) {
    return EFI_NOT_FOUND;
  }

  for (Index = 0; Index < StrLen (HwDeviceName); Index++) {
    if (HwDeviceName[Index] == L'-') {
      *ModelNameString = &HwDeviceName[Index + 1];
      break;
    }
  }
#endif

  return EFI_SUCCESS;
}

/**
  Get all Disk device information (BlockIo device path and device name).

  @param  EfiL05Service                 The pointer to EFI L05 Service Protocol.

  @retval EFI_SUCCESS                   Get information successfully.
  @return EFI_INVALID_PARAMETER         Invalid input parameter.
**/
EFI_STATUS
GetDiskDeviceInfo (
  IN  EFI_L05_SERVICE_PROTOCOL          *EfiL05Service
  )
{
  EFI_STATUS                            Status;
  HARDWARE_BOOT_DEVICE_INFO             *HwBootDeviceInfo;
  UINTN                                 HwBootDeviceCount;
  EFI_DEVICE_PATH_PROTOCOL              *TempDevicePath;
  EFI_DEVICE_PATH_PROTOCOL              *MessagingDevicePath;
  UINTN                                 Index;
  EFI_HANDLE                            Handle;
  EFI_BLOCK_IO_PROTOCOL                 *BlkIo;
  EFI_DISK_INFO_PROTOCOL                *DiskInfo;
  ATA_IDENTIFY_DATA                     AtaIdentifyData;
  UINT32                                BufferSize;
  UINT8                                 SataCnfigure;
  SYSTEM_CONFIGURATION                  *SetupVariable;
  CHAR16                                *ModelNameString;
  EFI_LEGACY_BIOS_PROTOCOL              *LegacyBios;
  UINT16                                HddCount;
  HDD_INFO                              *LocalHddInfo;
  UINT16                                BbsTotalCount;
  BBS_TABLE                             *LocalBbsTable;
  UINT8                                 Channel;
  UINT8                                 Device;
  EFI_ATAPI_IDENTIFY_DATA               *IdentifyTable;

  if (EfiL05Service == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  EfiL05Service->NumberOfSataHdd = 0;
  EfiL05Service->NumberOfSataSsd = 0;
  EfiL05Service->NumberOfNvmeSsd = 0;
  EfiL05Service->NumberOfEmmcSsd = 0;
  EfiL05Service->NumberOfSataOdd = 0;
  TempDevicePath                 = NULL;
  MessagingDevicePath            = NULL;

  Status = GetAllHwBootDeviceInfo (&HwBootDeviceCount, &HwBootDeviceInfo);

  for (Index = 0; Index < HwBootDeviceCount; Index++) {

    ModelNameString = NULL;
    Status = GetModelNameStringFromDeviceName (HwBootDeviceInfo[Index].HwDeviceName, &ModelNameString);

    TempDevicePath = HwBootDeviceInfo[Index].BlockIoDevicePath;

    //
    // Get Messaging device path
    //
    while (!IsDevicePathEnd (TempDevicePath)) {

      if (DevicePathType (TempDevicePath) == MESSAGING_DEVICE_PATH) {

        //
        // Found HDD device
        //
        MessagingDevicePath = TempDevicePath;
        break;
      }

      TempDevicePath = NextDevicePathNode (TempDevicePath);
    }

    TempDevicePath = HwBootDeviceInfo[Index].BlockIoDevicePath;

    Status = gBS->LocateDevicePath (&gEfiBlockIoProtocolGuid, &TempDevicePath, &Handle);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    //  Use BlockIo RemovableMedia to check if it is ODD
    //
    Status = gBS->HandleProtocol (
                    Handle,
                    &gEfiBlockIoProtocolGuid,
                    &BlkIo
                    );

    if (!EFI_ERROR (Status)) {
      if ((BlkIo->Media->RemovableMedia) && (MessagingDevicePath != NULL)) {

        switch (DevicePathSubType (MessagingDevicePath)) {

        case MSG_SATA_DP:
        case MSG_ATAPI_DP:
          if (EfiL05Service->NumberOfSataOdd < MAX_SATA_ODD_NUMBER) {

            CopyMem (
              EfiL05Service->SataOddInfo[EfiL05Service->NumberOfSataOdd].DeviceModelName,
              ModelNameString,
              StrSize (ModelNameString)
              );

            CleanSpaceChar (EfiL05Service->SataOddInfo[EfiL05Service->NumberOfSataOdd].DeviceModelName);
            EfiL05Service->NumberOfSataOdd++;
          }

          break;

        default:
          break;
        }

        continue;
      }
    }

    if (MessagingDevicePath != NULL) {

      SetMem(&AtaIdentifyData, sizeof (EFI_ATAPI_IDENTIFY_DATA), 0);
      BufferSize = sizeof (EFI_ATAPI_IDENTIFY_DATA);

      Status = gBS->HandleProtocol (
                      Handle,
                      &gEfiDiskInfoProtocolGuid,
                      (VOID **) &DiskInfo
                      );

      if (!EFI_ERROR(Status)) {
        Status = DiskInfo->Identify (
                            DiskInfo,
                            &AtaIdentifyData,
                            &BufferSize
                            );
      }

      switch (DevicePathSubType (MessagingDevicePath)) {

      case MSG_SATA_DP:
      case MSG_ATAPI_DP:
        if (AtaIdentifyData.nominal_media_rotation_rate == 1) {
          if (EfiL05Service->NumberOfSataSsd < MAX_SATA_SSD_NUMBER) {

            CopyMem (
              EfiL05Service->SataSsdInfo[EfiL05Service->NumberOfSataSsd].DeviceModelName,
              ModelNameString,
              StrSize (ModelNameString)
              );

            CleanSpaceChar (EfiL05Service->SataSsdInfo[EfiL05Service->NumberOfSataSsd].DeviceModelName);
            EfiL05Service->NumberOfSataSsd++;
          }

        } else {
          if (EfiL05Service->NumberOfSataHdd < MAX_SATA_HDD_NUMBER) {

            CopyMem (
              EfiL05Service->SataHddInfo[EfiL05Service->NumberOfSataHdd].DeviceModelName,
              ModelNameString,
              StrSize (ModelNameString)
              );

            CleanSpaceChar (EfiL05Service->SataHddInfo[EfiL05Service->NumberOfSataHdd].DeviceModelName);
            EfiL05Service->NumberOfSataHdd++;
          }
        }
        break;

      case MSG_NVME_NAMESPACE_DP:
        if (EfiL05Service->NumberOfNvmeSsd < MAX_NVME_SSD_NUMBER) {

          CopyMem (
            EfiL05Service->NvmeSsdInfo[EfiL05Service->NumberOfNvmeSsd].DeviceModelName,
            ModelNameString,
            StrSize (ModelNameString)
            );

          CleanSpaceChar (EfiL05Service->NvmeSsdInfo[EfiL05Service->NumberOfNvmeSsd].DeviceModelName);
          EfiL05Service->NumberOfNvmeSsd++;
        }
        break;

      case MSG_EMMC_DP:
        if (EfiL05Service->NumberOfEmmcSsd < MAX_EMMC_SSD_NUMBER) {

          CopyMem (
            EfiL05Service->EmmcSsdInfo[EfiL05Service->NumberOfEmmcSsd].DeviceModelName,
            ModelNameString,
            StrSize (ModelNameString)
            );

          CleanSpaceChar (EfiL05Service->EmmcSsdInfo[EfiL05Service->NumberOfEmmcSsd].DeviceModelName);
          EfiL05Service->NumberOfEmmcSsd++;
        }
        break;

      default:
        break;
      }
    }
  }

  SataCnfigure = gSUBrowser->SUCInfo->PrevSataCnfigure;
  SetupVariable = (SYSTEM_CONFIGURATION *) gSUBrowser->SCBuffer;
  ModelNameString = AllocateZeroPool ((MAX_MODEL_NAME_LEN + 1) * sizeof (CHAR16));

  //
  // When support legacy boot & sata mode is RAID mode,
  // RAID option rom will not install EFI_COMPONENT_NAME2_PROTOCOL to provide device name.
  // So we need get device name from EFI_LEGACY_BIOS_PROTOCOL.
  //
  if ((SetupVariable->BootType != EFI_BOOT_TYPE) && (SataCnfigure == RAID_MODE)) {

    Status = gBS->LocateProtocol (
                    &gEfiLegacyBiosProtocolGuid,
                    NULL,
                    (VOID **) &LegacyBios
                    );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    Status = LegacyBios->GetBbsInfo (
                           LegacyBios,
                           &HddCount,
                           &LocalHddInfo,
                           &BbsTotalCount,
                           &LocalBbsTable
                           );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    for (Channel = 0; Channel < 4; Channel++) {
      for (Device = 0; Device < 2; Device++) {

        IdentifyTable = (EFI_ATAPI_IDENTIFY_DATA *) &LocalHddInfo[Channel].IdentifyDrive[Device];

        if (IdentifyTable->ModelName[0] == 0) {
          continue;
        }

        UpdateAtaString (
          (EFI_ATAPI_IDENTIFY_DATA *) &LocalHddInfo[Channel].IdentifyDrive[Device],
          &ModelNameString
          );

        if ((IdentifyTable->config & BIT7) == BIT7) {
          //
          // Config [BIT7] : Removable media device
          //
          if (EfiL05Service->NumberOfSataOdd < MAX_SATA_ODD_NUMBER) {

            CopyMem (
              EfiL05Service->SataOddInfo[EfiL05Service->NumberOfSataOdd].DeviceModelName,
              ModelNameString,
              StrSize (ModelNameString)
              );

            CleanSpaceChar (EfiL05Service->SataOddInfo[EfiL05Service->NumberOfSataOdd].DeviceModelName);
            EfiL05Service->NumberOfSataOdd++;
          }

          continue;
        }

        if (((ATA_IDENTIFY_DATA *) IdentifyTable)->nominal_media_rotation_rate == 1) {
          if (EfiL05Service->NumberOfSataSsd < MAX_SATA_HDD_NUMBER) {

            CopyMem (
              EfiL05Service->SataSsdInfo[EfiL05Service->NumberOfSataSsd].DeviceModelName,
              ModelNameString,
              StrSize (ModelNameString)
              );
  
            CleanSpaceChar (EfiL05Service->SataSsdInfo[EfiL05Service->NumberOfSataSsd].DeviceModelName);
            EfiL05Service->NumberOfSataSsd++;
          }

        } else {
          if (EfiL05Service->NumberOfSataHdd < MAX_SATA_HDD_NUMBER) {

            CopyMem (
              EfiL05Service->SataHddInfo[EfiL05Service->NumberOfSataHdd].DeviceModelName,
              ModelNameString,
              StrSize (ModelNameString)
              );

            CleanSpaceChar (EfiL05Service->SataHddInfo[EfiL05Service->NumberOfSataHdd].DeviceModelName);
            EfiL05Service->NumberOfSataHdd++;
          }
        }
      }
    }
  }

  FreePool (ModelNameString);

  if (HwBootDeviceCount != 0 && HwBootDeviceInfo != NULL) {
    for (Index = 0; Index < HwBootDeviceCount; Index++) {
      FreePool (HwBootDeviceInfo[Index].HwDeviceName);
    }
    FreePool (HwBootDeviceInfo);
  }

  return Status;
}

/**
  Update System Infomation, ie CPU Type & Speed, Bus Speed, System Memory Speed,
  cache, and RAM.

  @param  HiiHandle                     Hii handle.

  @retval EFI_SUCCESS                   System info update successful.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
UpdateOemSystemInfo (
  IN  EFI_HII_HANDLE                    HiiHandle
  )
{
  EFI_SMBIOS_TABLE_HEADER               *Record;
  EFI_SMBIOS_HANDLE                     SmbiosHandle;
  EFI_SMBIOS_PROTOCOL                   *Smbios;
  EFI_STATUS                            Status;
  UINT8                                 StrIndex;
  CHAR16                                *NewString;
  EFI_STRING_ID                         TokenToUpdate;
  SMBIOS_TABLE_TYPE0                    *Type0Record;
  SMBIOS_TABLE_TYPE1                    *Type1Record;
  SMBIOS_TABLE_TYPE2                    *Type2Record;
  SMBIOS_TABLE_TYPE4                    *Type4Record;
  SMBIOS_TABLE_TYPE19                   *Type19Record;
  UINTN                                 Index;
  BOOLEAN                               GetType[6] = {FALSE};
  CHAR8                                 *L05MtmString;

  //
  // Update Front Page strings
  //
  Status = gBS->LocateProtocol (
                  &gEfiSmbiosProtocolGuid,
                  NULL,
                  (VOID **) &Smbios
                  );
  ASSERT_EFI_ERROR (Status);

  Index = 0;
  SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;

  do {
    Status = Smbios->GetNext (Smbios, &SmbiosHandle, NULL, &Record, NULL);

    if (EFI_ERROR (Status)) {
      break;
    }

    switch (Record->Type) {

    case EFI_SMBIOS_TYPE_BIOS_INFORMATION:
      Type0Record = (SMBIOS_TABLE_TYPE0 *) Record;
      //
      // Bios Version
      //
      StrIndex = Type0Record->BiosVersion;
      Status = GetOptionalStringByIndex ((CHAR8 *) ((UINT8 *) Type0Record + Type0Record->Hdr.Length), StrIndex, &NewString);
      TokenToUpdate = STRING_TOKEN (STR_MISC_BIOS_VERSION);
      HiiSetString (HiiHandle, TokenToUpdate, NewString, NULL);
      gBS->FreePool (NewString);
      GetType[0] = TRUE;
      break;

    case EFI_SMBIOS_TYPE_SYSTEM_INFORMATION:
      //
      // Product Name
      //
      Type1Record = (SMBIOS_TABLE_TYPE1 *) Record;
      StrIndex = Type1Record->Version;
      GetOptionalStringByIndex ((CHAR8 *) ((UINT8 *) Type1Record + Type1Record->Hdr.Length), StrIndex, &NewString);
      TokenToUpdate = STRING_TOKEN (L05_STR_PRODUCT_NAME_STRING2);
      HiiSetString (HiiHandle, TokenToUpdate, NewString, NULL);
      gBS->FreePool (NewString);

      //
      // UUID Number
      //
      NewString = AllocateZeroPool (0x60);
      L05GuidToString (&(Type1Record->Uuid), NewString, (sizeof (EFI_GUID) * sizeof (CHAR16) * 3));
      TokenToUpdate = STRING_TOKEN (L05_STR_UUID_NUMBER_STRING2);
      HiiSetString (HiiHandle, TokenToUpdate, NewString, NULL);
      gBS->FreePool (NewString);
      GetType[1] = TRUE;
      break;

    case EFI_SMBIOS_TYPE_BASEBOARD_INFORMATION:
      //
      // Windows License
      //
      Type2Record = (SMBIOS_TABLE_TYPE2 *) Record;
      StrIndex = Type2Record->Version;
      GetOptionalStringByIndex ((CHAR8 *) ((UINT8 *) Type2Record + Type2Record->Hdr.Length), StrIndex, &NewString);
      CleanSpaceChar (NewString);

      TokenToUpdate = STRING_TOKEN (L05_STR_PREINSTALLED_OS_LICENSE_STRING2);
      HiiSetString (HiiHandle, TokenToUpdate, NewString, NULL);

      gBS->FreePool (NewString);
      GetType[2] = TRUE;

      //
      // Serial Number
      //
      StrIndex = Type2Record->SerialNumber;
      GetOptionalStringByIndex ((CHAR8 *) ((UINT8 *) Type2Record + Type2Record->Hdr.Length), StrIndex, &NewString);
      TokenToUpdate = STRING_TOKEN (L05_STR_SYSTEMSERIAL_NUMBER_STRING2);
      HiiSetString (HiiHandle, TokenToUpdate, NewString, NULL);
      gBS->FreePool (NewString);
      break;

    case EFI_SMBIOS_TYPE_PROCESSOR_INFORMATION:
      //
      // CPU Info
      //
      Type4Record = (SMBIOS_TABLE_TYPE4 *) Record;
      StrIndex = Type4Record->ProcessorVersion;
      //NewString = AllocateZeroPool (0x20);
      Status = GetOptionalStringByIndex ((CHAR8 *) ((UINT8 *) Type4Record + Type4Record->Hdr.Length), StrIndex, &NewString);
      TokenToUpdate = STRING_TOKEN (L05_STR_CPU_INFO_STRING2);
      HiiSetString (HiiHandle, TokenToUpdate, NewString, NULL);
      gBS->FreePool (NewString);
      GetType[3] = TRUE;
      break;

    case EFI_SMBIOS_TYPE_MEMORY_ARRAY_MAPPED_ADDRESS:
      //
      // Total Memory Size
      //
      Type19Record = (SMBIOS_TABLE_TYPE19 *) Record;
      L05ConvertMemorySizeToString (
        (UINT32) (RShiftU64 ((Type19Record->EndingAddress - Type19Record->StartingAddress), 10) + 1),
        &NewString);

      TokenToUpdate = STRING_TOKEN (L05_STR_TOTAL_MEMORY_STRING2);
      HiiSetString (HiiHandle, TokenToUpdate, NewString, NULL);
      gBS->FreePool (NewString);
      GetType[4] = TRUE;
      break;

    case L05_SMBIOS_IDEAPAD_IDENTIFY:
      L05MtmString = (CHAR8 *) PcdGetPtr (PcdL05Type200MTM);

      NewString = AllocateZeroPool ((L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH + 1) * sizeof (CHAR16));
      AsciiStrToUnicodeStrS (L05MtmString, NewString, L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH + 1);
      CleanSpaceChar (NewString);

      TokenToUpdate = STRING_TOKEN (L05_STR_MTM_STRING2);
      HiiSetString (HiiHandle, TokenToUpdate, NewString, NULL);

      gBS->FreePool (NewString);
      GetType[5] = TRUE;
      break;

    default:
      break;
    }
  } while (!(GetType[0] && GetType[1] && GetType[2] && GetType[3] && GetType[4] && GetType [5]));

  return  EFI_SUCCESS;
}

/**
  Show storage device information.

  @param HiiHandle                      Hii handle.
  @param StorageDeviceType              Pointer to string of storage device type.
  @param NumberOfDevice                 Number Of storage device.
  @param StorageDeviceInfo              Pointer to storage device information.
  @param StartOpCodeHandle              Pointer to start opcode handle.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
ShowStorageDeviceInformation (
  IN  EFI_HII_HANDLE                    HiiHandle,
  IN  CHAR16                            *StorageDeviceType,
  IN  UINTN                             NumberOfDevice,
  IN  EFI_L05_STORAGE_DEVICE_INFO       *StorageDeviceInfo,
  IN  VOID                              *StartOpCodeHandle
  )
{
  UINTN                                 Index;
  CHAR16                                *NewString;
  EFI_STRING_ID                         TitleString;
  EFI_STRING_ID                         ModelNameString;

  if ((StorageDeviceType == NULL) || (NumberOfDevice == 0)) {
    return EFI_UNSUPPORTED;
  }

  //
  // Show information for Storage Device
  //
  for (Index = 0; Index < NumberOfDevice; Index++) {

    NewString = CatSPrint (NULL, StorageDeviceType, NULL);

    //
    // Show number for more than one Storage Device
    //
    if (NumberOfDevice > 1) {
      if (StrStr (NewString, L"(") != NULL) {
        NewString = CatSPrint (NewString, L" %d)", Index + 1);
      } else {
        NewString = CatSPrint (NewString, L" %d", Index + 1);
      }
    } else {
      if (StrStr (NewString, L"(") != NULL) {
        NewString = CatSPrint (NewString, L")", NULL);
      }
    }

    TitleString     = HiiSetString (HiiHandle, 0, NewString, NULL);
    ModelNameString = HiiSetString (HiiHandle, 0, StorageDeviceInfo[Index].DeviceModelName, NULL);

    HiiCreateTextOpCode (StartOpCodeHandle, TitleString, 0, ModelNameString);

    if (NewString != NULL) {
      gBS->FreePool (NewString);
    }
  }

  return EFI_SUCCESS;
}

/**
  Update storage device model name.

  @param HiiHandle                      Hii handle

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
UpdateStorageDeviceModelName (
  IN  EFI_HII_HANDLE                    HiiHandle
  )
{
  EFI_STATUS                            Status;
  VOID                                  *StartOpCodeHandle;
  VOID                                  *EndOpCodeHandle; 
  EFI_IFR_GUID_LABEL                    *StartLabel;
  EFI_IFR_GUID_LABEL                    *EndLabel;
  EFI_STRING_ID                         TitleString;
  EFI_STRING_ID                         ModelNameString;
  EFI_L05_SERVICE_PROTOCOL              *EfiL05Service;
  UINTN                                 NumberOfStorageDevice;

  EfiL05Service = NULL;
  NumberOfStorageDevice = 0;

  Status = gBS->LocateProtocol (&gEfiL05ServiceProtocolGuid, NULL, &EfiL05Service);
  ASSERT_EFI_ERROR (Status);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  StartOpCodeHandle        = HiiAllocateOpCodeHandle ();
  StartLabel               = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (StartOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
  StartLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
  StartLabel->Number       = L05_HDD_MODEL_NAME_LABEL;

  EndOpCodeHandle          = HiiAllocateOpCodeHandle ();
  EndLabel                 = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (EndOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
  EndLabel->ExtendOpCode   = EFI_IFR_EXTEND_OP_LABEL;
  EndLabel->Number         = L05_HDD_MODEL_NAME_END_LABEL;

  Status = GetDiskDeviceInfo (EfiL05Service);

  NumberOfStorageDevice = EfiL05Service->NumberOfSataHdd + EfiL05Service->NumberOfSataSsd + EfiL05Service->NumberOfNvmeSsd + EfiL05Service->NumberOfEmmcSsd;

  //
  // Show [Not Detected] without any Hard Disk device
  //
  if (NumberOfStorageDevice == 0) {

    //
    // No NVMe, eMMC or HDD be found
    //
    TitleString     = HiiSetString (HiiHandle, 0, HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_STORAGE_DEVICE_NOT_DETECTED_STRING1), NULL), NULL);
    ModelNameString = HiiSetString (HiiHandle, 0, HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_STORAGE_DEVICE_NOT_DETECTED_STRING2), NULL), NULL);
    HiiCreateTextOpCode (StartOpCodeHandle, TitleString, 0, ModelNameString);
  }

  //
  // [Lenovo China Minimum BIOS Spec 1.39]
  //   [3.11.1 Information menu]
  //     If the system has more than one hard disk, BIOS should show the hard disk index to user, for example,
  //     Hard disk 1: XXXXXX, Hard disk 2: XXXXXXX, SSD 1: XXXXX, SSD 2: XXXX, NVME SSD 1:XXXX, NVME SSD 2: XXXX, EMMC SSD 1:XXXX, EMMC SSD 2: XXXX
  //
  ShowStorageDeviceInformation (HiiHandle, HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_STORAGE_DEVICE_HARD_DISK_STRING), NULL), EfiL05Service->NumberOfSataHdd, EfiL05Service->SataHddInfo, StartOpCodeHandle);
  ShowStorageDeviceInformation (HiiHandle, HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_STORAGE_DEVICE_SSD_STRING), NULL), EfiL05Service->NumberOfSataSsd, EfiL05Service->SataSsdInfo, StartOpCodeHandle);
  ShowStorageDeviceInformation (HiiHandle, HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_STORAGE_DEVICE_NVME_SSD_STRING), NULL), EfiL05Service->NumberOfNvmeSsd, EfiL05Service->NvmeSsdInfo, StartOpCodeHandle);
  ShowStorageDeviceInformation (HiiHandle, HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_STORAGE_DEVICE_EMMC_SSD_STRING), NULL), EfiL05Service->NumberOfEmmcSsd, EfiL05Service->EmmcSsdInfo, StartOpCodeHandle);
  ShowStorageDeviceInformation (HiiHandle, HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_STORAGE_DEVICE_ODD_STRING), NULL), EfiL05Service->NumberOfSataOdd, EfiL05Service->SataOddInfo, StartOpCodeHandle);

  HiiUpdateForm (
    HiiHandle,
    NULL,
    ROOT_FORM_ID,
    StartOpCodeHandle,
    EndOpCodeHandle
    );

  HiiFreeOpCodeHandle (StartOpCodeHandle);
  HiiFreeOpCodeHandle (EndOpCodeHandle);

  return EFI_SUCCESS;
}

/**
  Update EC version String.

  @param  HiiHandle                     Hii handle.

  @retval None.
**/
VOID
UpdateECVersionStringInSCU (
  IN  EFI_HII_HANDLE                    HiiHandle
  )
{
  EFI_STATUS                            Status;
  CHAR16                                *NewString;
  UINT8                                 EcMinorRelease;
  CHAR16                                *BiosVersion;
//[-start-210608-FLINT00003-modify]//
#ifdef LCFC_SUPPORT
  {
    UINT8 EcMajorVersion, EcTestVersion;
    Status = LfcEcLibReadEcVersion(&EcMajorVersion,&EcMinorRelease,&EcTestVersion);   
    BiosVersion = (CHAR16 *) PcdGetPtr (PcdFirmwareVersionString);
    if (EcTestVersion) {
      NewString = CatSPrint (NULL, L"%c%cEC%02d%c%c(T%03d)", BiosVersion[0], BiosVersion[1], EcMinorRelease, BiosVersion[6], BiosVersion[7], EcTestVersion);
    }
    else {
      NewString = CatSPrint (NULL, L"%c%cEC%02d%c%c", BiosVersion[0], BiosVersion[1], EcMinorRelease, BiosVersion[6], BiosVersion[7]);
 //     break;
    }

    HiiSetString (HiiHandle, STRING_TOKEN (L05_STR_EC_VERSION_STRING2), NewString, NULL);

    if (NewString != NULL) {
      FreePool (NewString);
    }
  }
#else
  UINT8                                 StringSize;

  EcMinorRelease = PcdGet8 (PcdL05Type00ECMinorRelease);
  BiosVersion = (CHAR16 *) PcdGetPtr (PcdFirmwareVersionString);

  if (EcMinorRelease != 0) {
    //
    //BIOS Name:
    //xxCNmmWW or xxCNmmCL
    //EC Name:
    //xxECmmWW or xxECmmCL
    //xx BIOS ID provided by Lenovo, should match the BIOS ID number
    //xx in SMBIOS Type 0 structure (ref: 3.4.6)
    //WW for world wide products
    //CL for China only products
    //
    StringSize = 0x40;
    NewString = AllocateZeroPool (StringSize);
    UnicodeSPrint (NewString, StringSize, L"%c%cEC%02d%c%c", BiosVersion[0], BiosVersion[1], EcMinorRelease, BiosVersion[6], BiosVersion[7]);

    Status = OemSvcUpdateEcVersionStringInScu (NewString);

    switch (Status) {

    case EFI_UNSUPPORTED:
      FreePool (NewString);
      NewString = CatSPrint (NULL, L"%c%cEC%02d%c%c", BiosVersion[0], BiosVersion[1], EcMinorRelease, BiosVersion[6], BiosVersion[7]);
      break;
    }

    HiiSetString (HiiHandle, STRING_TOKEN (L05_STR_EC_VERSION_STRING2), NewString, NULL);

    if (NewString != NULL) {
      FreePool (NewString);
    }
  }
#endif
//[-end-210608-FLINT00003-modify]//
}

/**
  Get OA3 Key ID data from EEPROM or others (NOT from ACPI MSDM Table!!!).

  @param  KeyIdPtr                      The pointer to OA3 Key ID.

  @retval TRUE                          Get OA3 Key ID successfully.
  @retval FALSE                         Get OA3 Key ID fail or OA3 Key ID is invalid.
**/
BOOLEAN
GetOa3KeyId (
  CHAR8                                 *KeyIdPtr
  )
{
  EFI_STATUS                            Status;
#if (LATEST_L05_SPEC_VERSION == 135)
EFI_L05_EEPROM_MAP_119                  EepromBuffer;
#else
EFI_L05_EEPROM_MAP_120                  EepromBuffer;
#endif
  UINTN                                 EerpomBase;
  UINT32                                Index;

  Status       = EFI_SUCCESS;
  EerpomBase   = 0;

  ZeroMem (&EepromBuffer, sizeof (EepromBuffer));

  //
  // Get EEPROM data
  //
  Status = OemSvcGetEepromData (&EepromBuffer, sizeof (EepromBuffer));

  if (Status == EFI_UNSUPPORTED) {

    EerpomBase = (UINTN) FdmGetNAtAddr (&gL05H2OFlashMapRegionEepromGuid, 1);
    CopyMem ((VOID *) &EepromBuffer, (VOID *) EerpomBase, sizeof (EepromBuffer));
  }

  //
  // Get OA3 key ID from EEPROM (NOT from ACPI MSDM Table!!!)
  //
//[-start-210623-YUNLEI0104-modify]//
#ifdef LCFC_SUPPORT
  CopyMem ((VOID *) KeyIdPtr, &EepromBuffer.Oa3KeyId, L05_EEPROM_OA3_KEY_ID_LENGTH);
#else  
  CopyMem ((VOID *) KeyIdPtr, &EepromBuffer.Oa3KeyId, L05_EEPROM_OA3_KEY_ID_SIZE_USED);
#endif
  //
  // Check the data of OA3 Key ID are not all 0x00
  //
#ifdef LCFC_SUPPORT
  for (Index = 0; Index < L05_EEPROM_OA3_KEY_ID_LENGTH; Index++) {
#else   
  for (Index = 0; Index < L05_EEPROM_OA3_KEY_ID_SIZE_USED; Index++) {
#endif

    if (KeyIdPtr[Index] != 0x00) {
      break;
    }
  }

#ifdef LCFC_SUPPORT
  if (Index == L05_EEPROM_OA3_KEY_ID_LENGTH) {
#else
  if (Index == L05_EEPROM_OA3_KEY_ID_SIZE_USED) {
#endif
    return FALSE;
  }
//[-end-210623-YUNLEI0104-modify]//
  return TRUE;
}

/**
  Update OA3 Key ID.

  @param HiiHandle                      Hii handle.

  @retval None.
**/
VOID
UpdateOa3KeyIdInSCU (
  IN EFI_HII_HANDLE                     HiiHandle
  )
{
  EFI_STATUS                            Status;
  CHAR16                                *NewString;
//[-start-210623-YUNLEI0104-modify]//
#ifdef LCFC_SUPPORT
  CHAR8                                 Oa3KeyId[L05_EEPROM_OA3_KEY_ID_LENGTH + 1];
#else
  CHAR8                                 Oa3KeyId[L05_EEPROM_OA3_KEY_ID_SIZE_USED];
#endif

  Status = EFI_SUCCESS;
#ifdef LCFC_SUPPORT
  ZeroMem (Oa3KeyId, L05_EEPROM_OA3_KEY_ID_LENGTH + 1);
#else
  ZeroMem (Oa3KeyId, sizeof (Oa3KeyId));
#endif
//[-end-210623-YUNLEI0104-modify]//

  //
  // Get OA3 Key ID from EEPROM or others
  //
  if (GetOa3KeyId (Oa3KeyId)) {
    NewString = AllocateZeroPool (AsciiStrSize (Oa3KeyId) * sizeof (CHAR16));
    AsciiStrToUnicodeStrS (Oa3KeyId, NewString, AsciiStrSize (Oa3KeyId));
    NewString   = CatSPrint (NULL, L"%s", NewString);

  } else {
    NewString   = CatSPrint (NULL, L"0000000000000");
  }

  HiiSetString (HiiHandle, STRING_TOKEN (L05_STR_OA3_ID_STRING2), NewString, NULL);

  if (NewString != NULL) {
    gBS->FreePool (NewString);
  }
}

/**
  Update security states.

  @param  HiiHandle                     Hii handle.

  @retval None.
**/
VOID
UpdateSecurityStates (
  IN EFI_HII_HANDLE                     HiiHandle
  )
{
  UINTN                                 BufferSize;
  UINT8                                 Data;
  EFI_STATUS                            Status;
  EFI_STRING_ID                         TokenToUpdate;
  CHAR16                                *NewString;

  BufferSize = sizeof (UINT8);
  Status = gRT->GetVariable (
                  L"SecureBoot",
                  &gEfiGlobalVariableGuid,
                  NULL,
                  &BufferSize,
                  &Data
                  );

  //
  // SecureBoot = 0(Disabled) / 1(Enabled)
  //
  if ((EFI_SUCCESS == Status) && (1 == Data)) {
    NewString = HiiGetString (HiiHandle, STRING_TOKEN (STR_ENABLED_TEXT), NULL);
    TokenToUpdate = STRING_TOKEN (L05_STR_SECURE_BOOT_VALUE);
    HiiSetString (HiiHandle, TokenToUpdate, NewString, NULL);
    gBS->FreePool (NewString);
  }
}

/**
  Convert Memory Size to a string.

  @param MemorySize                     The size of the memory to process.
  @param String                         Pointer to the string, that is created.

  @retval EFI_SUCCESS                   Convert memory size value to string successful.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
L05ConvertMemorySizeToString (
  IN  UINT32                            MemorySize,
  OUT CHAR16                            **String
  )
{
  CHAR16                                *StringBuffer;

  StringBuffer = AllocateZeroPool (0x20);
  UnicodeValueToStringS (StringBuffer, 0x20, LEFT_JUSTIFY, MemorySize, 6);

  StrCatS (StringBuffer, 0x20 / sizeof (CHAR16), L" MB");

  *String = (CHAR16 *) StringBuffer;

  return EFI_SUCCESS;
}

/**
  VSPrint worker function that prints an EFI_GUID.

  @param  Guid                          Pointer to GUID.
  @param  Buffer                        Pointer to buffer of print GUID.
  @param  BufferSize                    Size of Buffer.

  @retval UINTN                         Number of characters printed.
**/
UINTN
L05GuidToString (
  IN  EFI_GUID                          *Guid,
  IN  CHAR16                            *Buffer,
  IN  UINTN                             BufferSize
  )
{
  UINTN                                 Size;

  Size = UnicodeSPrint (
           Buffer, BufferSize,
           L"%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
           Guid->Data1,
           Guid->Data2,
           Guid->Data3,
           Guid->Data4[0],
           Guid->Data4[1],
           Guid->Data4[2],
           Guid->Data4[3],
           Guid->Data4[4],
           Guid->Data4[5],
           Guid->Data4[6],
           Guid->Data4[7]
           );

  //
  // UnicodeSPrint will null terminate the string. The -1 skips the null
  //
  return Size - 1;
}
