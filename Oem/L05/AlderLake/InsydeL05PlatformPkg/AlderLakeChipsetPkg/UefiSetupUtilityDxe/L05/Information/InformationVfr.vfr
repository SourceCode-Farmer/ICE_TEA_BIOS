//;******************************************************************************
//;* Copyright (c) 1983-2019, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
//;
//; Module Name:
//;
//;   InformationVfr.vfr
//;
//; Abstract:
//;
//;   The Vfr component for Information menu
//;

#include "SetupConfig.h"
//
// Form Information: Information Menu
//
formset
  guid     = FORMSET_ID_GUID_INFORMATION,
  title    = STRING_TOKEN(L05_STR_INFORMATION_TITLE),
  help     = STRING_TOKEN(STR_BLANK_STRING),
  classguid = SETUP_UTILITY_FORMSET_CLASS_GUID,
  class    = SETUP_UTILITY_CLASS,
  subclass = SETUP_UTILITY_SUBCLASS,
#if defined(SETUP_IMAGE_SUPPORT) && FeaturePcdGet(PcdH2OFormBrowserLocalMetroDESupported)
  image     = IMAGE_TOKEN(IMAGE_MAIN);
#endif

  varstore SYSTEM_CONFIGURATION,            // This is the data structure type
    varid = CONFIGURATION_VARSTORE_ID,      // Optional VarStore ID
    name  = SystemConfig,                    // Define referenced name in vfr
    guid  = SYSTEM_CONFIGURATION_GUID;      // GUID of this buffer storage

  form
    formid = ROOT_FORM_ID,
    title = STRING_TOKEN(L05_STR_INFORMATION_TITLE);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);

    grayoutif  TRUE;
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_PRODUCT_NAME_STRING),
        text   = STRING_TOKEN(L05_STR_PRODUCT_NAME_STRING2),
        flags  = 0,
        key    = 0;
  
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_SYSTEM_BIOS_VERSION_STRING),
        text   = STRING_TOKEN(STR_MISC_BIOS_VERSION),
#if !FeaturePcdGet (PcdL05GamingUiSupported)
        flags  = 0,
        key    = 0;
#else
        flags  = INTERACTIVE,
        key    = L05_KEY_SYSTEM_BIOS_VERSION;
#endif
  
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_EC_VERSION_STRING),
        text   = STRING_TOKEN(L05_STR_EC_VERSION_STRING2),
        flags  = 0,
        key    = 0;
  
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_MTM_STRING),
        text   = STRING_TOKEN(L05_STR_MTM_STRING2),
        flags  = 0,
        key    = 0;
  
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_SYSTEMSERIAL_NUMBER_STRING),
        text   = STRING_TOKEN(L05_STR_SYSTEMSERIAL_NUMBER_STRING2),
#if !FeaturePcdGet (PcdL05GamingUiSupported)
        flags  = 0,
        key    = 0;
#else
        flags  = INTERACTIVE,
        key    = L05_KEY_SYSTEMSERIAL_NUMBER;
#endif
  
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_UUID_NUMBER_STRING),
        text   = STRING_TOKEN(L05_STR_UUID_NUMBER_STRING2),
        flags  = 0,
        key    = 0;
  
      subtitle
        text = STRING_TOKEN(STR_BLANK_STRING);
  
  
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_CPU_INFO_STRING),
        text   = STRING_TOKEN(L05_STR_CPU_INFO_STRING2),
#if !FeaturePcdGet (PcdL05GamingUiSupported)
        flags  = 0,
        key    = 0;
#else
        flags  = INTERACTIVE,
        key    = L05_KEY_CPU_INFO;
#endif
  
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_TOTAL_MEMORY_STRING),
        text   = STRING_TOKEN(L05_STR_TOTAL_MEMORY_STRING2),
        flags  = 0,
        key    = 0;
  
      label L05_HDD_MODEL_NAME_LABEL;
      label L05_HDD_MODEL_NAME_END_LABEL;
  
      subtitle
        text = STRING_TOKEN(STR_BLANK_STRING);
  
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_PREINSTALLED_OS_LICENSE_STRING),
        text   = STRING_TOKEN(L05_STR_PREINSTALLED_OS_LICENSE_STRING2),
        flags  = 0,
        key    = 0;
  
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_OA3_ID_STRING),
        text   = STRING_TOKEN(L05_STR_OA3_ID_STRING2),
        flags  = 0,
        key    = 0;
  
      suppressif
        ideqvallist SystemConfig.BootType == 0 1;
        text
          help   = STRING_TOKEN(STR_BLANK_STRING),
          text   = STRING_TOKEN(L05_STR_SECURE_BOOT_STRING),
          text   = STRING_TOKEN(L05_STR_SECURE_BOOT_VALUE);
      endif;
    endif;

  label L05_SETUP_UTILITY_LANG_LABEL;

  endform;

  link;

  endformset;
