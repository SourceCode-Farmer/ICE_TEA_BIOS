//;******************************************************************************
//;* Copyright (c) 1983-2019, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
//;
//; Abstract:
//;

#ifndef _OEM_INFORMATION_H_
#define _OEM_INFORMATION_H_

#include "SetupUtility.h"
#include <Protocol/DataHub.h>
#include <Protocol/L05Service.h>
#include <Protocol/BlockIo.h>
#include <Protocol/Smbios.h>
#include <Guid/DataHubRecords.h>
#include <IndustryStandard/SmBios.h>
#include <SetupConfig.h>

#define L05_SMBIOS_IDEAPAD_IDENTIFY     200

EFI_STATUS
UpdateOemSystemInfo (
  IN  EFI_HII_HANDLE                    HiiHandle
  );

EFI_STATUS
L05ConvertMemorySizeToString (
  IN  UINT32                            MemorySize,
  OUT CHAR16                            **String
  );

UINTN
L05GuidToString (
  IN  EFI_GUID                          *Guid,
  IN  CHAR16                            *Buffer,
  IN  UINTN                             BufferSize
  );

VOID
CleanSpaceChar (
  IN    CHAR16                          *Str
  );

EFI_STATUS
UpdateStorageDeviceModelName (
  IN  EFI_HII_HANDLE                    HiiHandle
  );

VOID
UpdateECVersionStringInSCU (
  IN  EFI_HII_HANDLE                    HiiHandle
  );

VOID
UpdateOa3KeyIdInSCU (
  IN EFI_HII_HANDLE                     HiiHandle
  );

VOID
UpdateSecurityStates (
  IN EFI_HII_HANDLE                     HiiHandle
  );

#endif
