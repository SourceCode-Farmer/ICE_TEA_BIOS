/** @file
  Provides function to keep Setup Item after SCU load default

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SAVE_RESTORE_SETUP_ITEM_H_
#define _SAVE_RESTORE_SETUP_ITEM_H_

EFI_STATUS
L05SaveRestoreSetupItem (
  BOOLEAN                               SaveRestoreFlag,
  CHIPSET_CONFIGURATION                 *MyIfrNVData,
  SETUP_DATA                            *MySetupDataIfrNVData,
  SA_SETUP                              *MySaIfrNVData,
  ME_SETUP                              *MyMeIfrNVData,
  CPU_SETUP                             *MyCpuIfrNVData,
  PCH_SETUP                             *MyPchIfrNVData,
  ME_SETUP_STORAGE                      *MyMeStorageIfrNVData
  );

#endif
