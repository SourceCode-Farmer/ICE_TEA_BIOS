//;******************************************************************************
//;* Copyright (c) 2013 - 2014, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************

#ifndef _CONFIGURATION_MENU_H_
#define _CONFIGURATION_MENU_H_

EFI_STATUS
UpdateConfigurationMenu (
  IN EFI_HII_HANDLE                     HiiHandle
  );
#endif
