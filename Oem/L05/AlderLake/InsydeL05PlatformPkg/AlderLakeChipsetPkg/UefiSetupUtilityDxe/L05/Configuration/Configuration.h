//;******************************************************************************
//;* Copyright (c) 2013 - 2014, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#ifndef _CONFIGURATION_H_
#define _CONFIGURATION_H_

#include "SetupUtility.h"
#include "ConfigurationMenu.h"
#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
#include "LinkIntelRstFormset.h"
#endif
#ifdef L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE
#include "LinkIntelWifiFormset.h"
#endif
#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
#include "GamingOverclock.h"
#endif

EFI_STATUS
ConfigurationCallbackRoutine (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  );

EFI_STATUS
InstallConfigurationCallbackRoutine (
  IN EFI_HANDLE                         DriverHandle,
  IN EFI_HII_HANDLE                     HiiHandle
  );

EFI_STATUS
UninstallConfigurationCallbackRoutine (
  IN EFI_HANDLE                         DriverHandle
  );

EFI_STATUS
InitConfigurationMenu (
  IN EFI_HII_HANDLE                     HiiHandle
  );
#endif