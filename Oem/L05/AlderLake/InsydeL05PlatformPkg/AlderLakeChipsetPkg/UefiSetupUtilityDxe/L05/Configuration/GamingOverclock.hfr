/** @file
  The hfr component for Gaming Overclocking

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <PlatformBoardId.h>

  form
    formid = L05_GAMING_OVERCLOCKING_FORM_ID,
    title = STRING_TOKEN(L05_STR_OVERCLOCKING_TITLE);

    //
    // Cpu Overclocking
    //
    suppressif 
      ideqval SETUP_CPU_FEATURES.CpuSkuOcSupported == 0
#if L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID
      OR
      ideqval SystemConfig.L05CpuUnlock == 0
#endif
      ;
      grayoutif ideqvallist SystemConfig.OverClockingSupport == 0;
        oneof varid   = CPU_SETUP.OverclockingSupport,
          questionid  = L05_KEY_CPU_OVERCLOCKING,
          prompt      = STRING_TOKEN(L05_STR_CPU_OVERCLOCKING),
          help        = STRING_TOKEN(STR_BLANK_STRING),
          option text = STRING_TOKEN(STR_DISABLED_TEXT), value = 0, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED | INTERACTIVE;
          option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 1, flags =  RESET_REQUIRED | INTERACTIVE;
        endoneof;
      endif;
    endif;

    //
    // Gpu Overclocking switch
    //
    suppressif 
      NOT ideqval SystemConfig.L05GpuType == L05_GPU_TYPE_DGPU_NVIDIA
      OR
      ideqval SystemConfig.L05GpuUnlock == 0;
      oneof varid   = SystemConfig.L05GpuOverclocking,
        questionid  = L05_KEY_GPU_OVERCLOCKING,
        prompt      = STRING_TOKEN(L05_STR_GPU_OVERCLOCKING),
        help        = STRING_TOKEN(STR_BLANK_STRING),
        option text = STRING_TOKEN(STR_DISABLED_TEXT), value = 0, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED | INTERACTIVE;
        option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 1, flags =  RESET_REQUIRED | INTERACTIVE;
      endoneof;
    endif;

    //
    // Memory Overclocking switch
    //
    suppressif ideqval SETUP_VOLATILE_DATA.MobileOcUnSupport == 1
            OR ideqval SETUP_VOLATILE_DATA.PlatformFlavor == FlavorUpServer;
      oneof varid = SA_SETUP.RealtimeMemoryTiming,
        questionid  = L05_KEY_MEMORY_OVERCLOCKING,
        prompt  = STRING_TOKEN(L05_STR_MEMORY_OVERCLOCKING),
        help    = STRING_TOKEN(STR_BLANK_STRING),
        option text = STRING_TOKEN(STR_DISABLED_TEXT), value = 0,  flags = DEFAULT | MANUFACTURING | RESET_REQUIRED | INTERACTIVE;
        option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 1,  flags = RESET_REQUIRED | INTERACTIVE;
      endoneof;

      suppressif NOT ideqval SA_SETUP.RealtimeMemoryTiming == 1;
        oneof varid = SA_SETUP.SpdProfileSelected,
          questionid  = L05_KEY_MEMORY_PROFILE,
          prompt      = STRING_TOKEN(L05_STR_MEMORY_PROFILE),
          help        = STRING_TOKEN(L05_STR_MEMORY_PROFILE_HELP),
          flags       = INTERACTIVE,
            option text = STRING_TOKEN (STR_DEFAULT_SPD_PROFILE),  value = 0, flags = DEFAULT | MANUFACTURING | RESET_REQUIRED;
            option text = STRING_TOKEN (STR_CUSTOM_PROFILE),    value = 1, flags = RESET_REQUIRED;
            suppressif cond ((get(SA_SETUP.XmpProfileEnable) & 0x1) == 0 ? 0 : 1);
              option text = STRING_TOKEN (STR_XMP_PROFILE_1),   value = 2, flags = RESET_REQUIRED;
            endif;
            suppressif cond ((get(SA_SETUP.XmpProfileEnable) & 0x2) == 0 ? 0 : 1);
              option text = STRING_TOKEN (STR_XMP_PROFILE_2),   value = 3, flags = RESET_REQUIRED;
            endif;
            suppressif cond ((get(SA_SETUP.XmpProfileEnable) & 0x4) == 0 ? 0 : 1);
              option text = STRING_TOKEN (STR_XMP_PROFILE_3),   value = 4, flags = RESET_REQUIRED;
            endif;
            suppressif cond ((get(SA_SETUP.XmpProfileEnable) & 0x8) == 0 ? 0 : 1);
              option text = STRING_TOKEN (STR_XMP_USER_PROFILE_4),  value = 5, flags = RESET_REQUIRED;
            endif;
            suppressif cond ((get(SA_SETUP.XmpProfileEnable) & 0x10) == 0 ? 0 : 1);
              option text = STRING_TOKEN (STR_XMP_USER_PROFILE_5),  value = 6, flags = RESET_REQUIRED;
            endif;
        endoneof;
      endif;
    endif;

  endform;

  form
    formid = L05_GAMING_OVERCLOCKING_INTERNAL_FORM_ID,
    title = STRING_TOKEN(L05_STR_OVERCLOCKING_INTERNAL_TITLE);

    text
      help   = STRING_TOKEN(L05_STR_OVERCLOCK_BACKUP_SETUP_DATA),
      text   = STRING_TOKEN(L05_STR_OVERCLOCK_BACKUP_SETUP_DATA),
      text   = STRING_TOKEN(L05_STR_ENTER_TEXT),
      flags  = INTERACTIVE,
      key    = L05_KEY_OVERCLOCK_BACKUP_SETUP_DATA;

    text
      help   = STRING_TOKEN(L05_STR_OVERCLOCK_RESTORE_SETUP_DATA),
      text   = STRING_TOKEN(L05_STR_OVERCLOCK_RESTORE_SETUP_DATA),
      text   = STRING_TOKEN(L05_STR_ENTER_TEXT),
      flags  = INTERACTIVE,
      key    = L05_KEY_OVERCLOCK_RESTORE_SETUP_DATA;

    text
      help   = STRING_TOKEN(L05_STR_SET_TO_DEFAULT),
      text   = STRING_TOKEN(L05_STR_SET_TO_DEFAULT),
      text   = STRING_TOKEN(L05_STR_ENTER_TEXT),
      flags  = INTERACTIVE,
      key    = L05_KEY_OVERCLOCK_SET_TO_DEFAULT;

  endform;

