//;******************************************************************************
//;* Copyright (c) 2013 - 2014, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#include "Configuration.h"
#ifdef L05_MAC_ADDRESS_PASS_THROUGH_ENABLE
#include <L05MacAddressPassThroughVariable.h>
#endif
#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
#include "RstConfigVariable.h"
#endif

EFI_CALLBACK_INFO                       *mConfigurationCallBackInfo;

EFI_STATUS
ConfigurationCallbackRoutineByAction (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  )
{
  EFI_STATUS                            Status;
  EFI_CALLBACK_INFO                     *CallbackInfo;
  UINTN                                 BufferSize;
  EFI_GUID                              VarStoreGuid = SYSTEM_CONFIGURATION_GUID;

  if ((This == NULL) ||
      ((Value == NULL) &&
       (Action != EFI_BROWSER_ACTION_FORM_OPEN) &&
       (Action != EFI_BROWSER_ACTION_FORM_CLOSE)) ||
      (ActionRequest == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;
  CallbackInfo   = EFI_CALLBACK_INFO_FROM_THIS (This);
  BufferSize     = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemConfig");
  Status         = EFI_UNSUPPORTED;

  switch (Action) {

  case EFI_BROWSER_ACTION_FORM_OPEN:
    if (QuestionId == 0) {
      Status = SetupVariableConfig (
                 &VarStoreGuid,
                 L"SystemConfig",
                 BufferSize,
                 (UINT8 *) gSUBrowser->SCBuffer,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gSaSetupVariableGuid,
                 SA_SETUP_VARIABLE_NAME,
                 sizeof (SA_SETUP),
                 (UINT8 *) gRcSUBrowser->SaSUBrowserData,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gCpuSetupVariableGuid,
                 CPU_SETUP_VARIABLE_NAME,
                 sizeof (CPU_SETUP),
                 (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gPchSetupVariableGuid,
                 PCH_SETUP_VARIABLE_NAME,
                 sizeof (PCH_SETUP),
                 (UINT8 *) gRcSUBrowser->PchSUBrowserData,
                 FALSE
                 );
    }

    break;

  case EFI_BROWSER_ACTION_FORM_CLOSE:
    if (QuestionId == 0) {
      Status = SetupVariableConfig (
                 &VarStoreGuid,
                 L"SystemConfig",
                 BufferSize,
                 (UINT8 *) gSUBrowser->SCBuffer,
                 TRUE
                 );
      Status = SetupVariableConfig (
                 &gSaSetupVariableGuid,
                 SA_SETUP_VARIABLE_NAME,
                 sizeof (SA_SETUP),
                 (UINT8 *) gRcSUBrowser->SaSUBrowserData,
                 TRUE
                 );
      Status = SetupVariableConfig (
                 &gCpuSetupVariableGuid,
                 CPU_SETUP_VARIABLE_NAME,
                 sizeof (CPU_SETUP),
                 (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
                 TRUE
                 );
      Status = SetupVariableConfig (
                 &gPchSetupVariableGuid,
                 PCH_SETUP_VARIABLE_NAME,
                 sizeof (PCH_SETUP),
                 (UINT8 *) gRcSUBrowser->PchSUBrowserData,
                 TRUE
                 );
    }

    break;

  case EFI_BROWSER_ACTION_CHANGING:
    Status = EFI_SUCCESS;
    break;

  case EFI_BROWSER_ACTION_DEFAULT_MANUFACTURING:
    if (QuestionId == KEY_SCAN_F9) {
      Status = HotKeyCallBack (
                 This,
                 Action,
                 QuestionId,
                 Type,
                 Value,
                 ActionRequest
                 );
      Status = SetupVariableConfig (
                 &VarStoreGuid,
                 L"SystemConfig",
                 BufferSize,
                 (UINT8 *) gSUBrowser->SCBuffer,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gSaSetupVariableGuid,
                 SA_SETUP_VARIABLE_NAME,
                 sizeof (SA_SETUP),
                 (UINT8 *) gRcSUBrowser->SaSUBrowserData,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gCpuSetupVariableGuid,
                 CPU_SETUP_VARIABLE_NAME,
                 sizeof (CPU_SETUP),
                 (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gPchSetupVariableGuid,
                 PCH_SETUP_VARIABLE_NAME,
                 sizeof (PCH_SETUP),
                 (UINT8 *) gRcSUBrowser->PchSUBrowserData,
                 FALSE
                 );
    }

    //
    // avoid GetQuestionDefault execute ExtractConfig
    //
    return EFI_SUCCESS;

  default:
    break;
  }

  return Status;
}

/*++

Routine Description:

  This is the callback function for the Configuration Menu.

Arguments:

Returns:

--*/
EFI_STATUS
ConfigurationCallbackRoutine (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  )
{
  EFI_STATUS                            Status;
  EFI_CALLBACK_INFO                     *CallbackInfo;
  UINTN                                 BufferSize;
  EFI_GUID                              VarStoreGuid = SYSTEM_CONFIGURATION_GUID;
  SYSTEM_CONFIGURATION                  *MyIfrNvData;
  PCH_SETUP                             *MyPchIfrNvData;
  EFI_HII_HANDLE                        HiiHandle;
  CHAR16                                *StringPtr;
  EFI_INPUT_KEY                         Key;
#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
  UINT8                                 TempSataInterfaceMode;
  UINT8                                 SataInterfaceMode;
  RST_CONFIG_VARIABLE                   RstConfigVariable;
#endif
  SETUP_DATA                            *MySetupDataIfrNvData;

  if (Action != EFI_BROWSER_ACTION_CHANGED) {
    return ConfigurationCallbackRoutineByAction (This, Action, QuestionId, Type, Value, ActionRequest);
  }

  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;
  CallbackInfo = EFI_CALLBACK_INFO_FROM_THIS (This);

  if (QuestionId == GET_SETUP_CONFIG || QuestionId == SET_SETUP_CONFIG) {
    BufferSize = sizeof (SYSTEM_CONFIGURATION);
    Status = SetupVariableConfig (
               NULL,
               NULL,
               BufferSize,
               (UINT8 *) gSUBrowser->SCBuffer,
               (BOOLEAN) (QuestionId == GET_SETUP_CONFIG)
               );
    Status = SetupVariableConfig (
               &gSaSetupVariableGuid,
               SA_SETUP_VARIABLE_NAME,
               sizeof (SA_SETUP),
               (UINT8 *) gRcSUBrowser->SaSUBrowserData,
               (BOOLEAN)(QuestionId == GET_SETUP_CONFIG)
               );
    Status = SetupVariableConfig (
               &gCpuSetupVariableGuid,
               CPU_SETUP_VARIABLE_NAME,
               sizeof (CPU_SETUP),
               (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
               (BOOLEAN)(QuestionId == GET_SETUP_CONFIG)
               );
    Status = SetupVariableConfig (
               &gPchSetupVariableGuid,
               PCH_SETUP_VARIABLE_NAME,
               sizeof (PCH_SETUP),
               (UINT8 *) gRcSUBrowser->PchSUBrowserData,
               (BOOLEAN)(QuestionId == GET_SETUP_CONFIG)
               );
    Status = SetupVariableConfig (
               &gSetupVariableGuid,
               PLATFORM_SETUP_VARIABLE_NAME,
               sizeof (SETUP_DATA),
               (UINT8 *) gRcSUBrowser->SetupDataSUBrowserData,
               (BOOLEAN)(QuestionId == GET_SETUP_CONFIG)
               );
    return Status;
  }

#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
  //
  // Backup SataInterfaceMode before running SetupVariableConfig
  //
  TempSataInterfaceMode = ((PCH_SETUP *)gRcSUBrowser->PchSUBrowserData)->SataInterfaceMode;   
#endif

  BufferSize = GetVarStoreSize (
                 CallbackInfo->HiiHandle,
                 &CallbackInfo->FormsetGuid,
                 &VarStoreGuid,
                 "SystemConfig"
                 );

  Status = SetupVariableConfig (
             NULL,
             NULL,
             BufferSize,
             (UINT8 *) gSUBrowser->SCBuffer,
             TRUE
             );
  Status = SetupVariableConfig (
             &gSaSetupVariableGuid,
             SA_SETUP_VARIABLE_NAME,
             sizeof (SA_SETUP),
             (UINT8 *) gRcSUBrowser->SaSUBrowserData,
             TRUE
             );
  Status = SetupVariableConfig (
             &gCpuSetupVariableGuid,
             CPU_SETUP_VARIABLE_NAME,
             sizeof (CPU_SETUP),
             (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
             TRUE
             );
  Status = SetupVariableConfig (
             &gPchSetupVariableGuid,
             PCH_SETUP_VARIABLE_NAME,
             sizeof (PCH_SETUP),
             (UINT8 *) gRcSUBrowser->PchSUBrowserData,
             TRUE
             );
  Status = SetupVariableConfig (
             &gSetupVariableGuid,
             PLATFORM_SETUP_VARIABLE_NAME,
             sizeof (SETUP_DATA),
             (UINT8 *) gRcSUBrowser->SetupDataSUBrowserData,
             TRUE
             );

  MyIfrNvData          = (SYSTEM_CONFIGURATION *) gSUBrowser->SCBuffer;
  MyPchIfrNvData       = (PCH_SETUP *) gRcSUBrowser->PchSUBrowserData;
  MySetupDataIfrNvData = (SETUP_DATA *) gRcSUBrowser->SetupDataSUBrowserData;

  Status    = EFI_SUCCESS;
  StringPtr = NULL;
  HiiHandle = CallbackInfo->HiiHandle;

  switch (QuestionId) {

  case L05_KEY_DISABLE_BUILDING_IN_BATTERY:

    StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_DISABLE_BUILDING_IN_BATTERY_NOTICE), NULL);

    gSUBrowser->H2ODialog->ConfirmDialog (
                             0,
                             FALSE,
                             0,
                             NULL,
                             &Key,
                             StringPtr
                             );

    if (Key.UnicodeChar == CHAR_CARRIAGE_RETURN) {
      OemSvcDisableBuildingInBattery ();
    }

    gBS->FreePool (StringPtr);
    break;

#ifdef L05_MAC_ADDRESS_PASS_THROUGH_ENABLE
  case L05_KEY_MAC_ADDRESS_PASS_THROUGH:
{
    CHAR16                              *CreateNetworkString;
    STRING_REF                          TokenToUpdate;
    L05_MAC_ADDRESS_DATA                *ModifyMacAddrData;

    ModifyMacAddrData = NULL;
    ModifyMacAddrData = CommonGetVariableData (L05_MAC_ADDRESS_PASS_THROUGH_VARIABLE_NAME, &gH2OBdsCpNetworkUpdateMacAddrGuid);

    if (ModifyMacAddrData != NULL) {
      CreateNetworkString = AllocateZeroPool (StrSize (L05_MAC_ADDRESS_PASS_THROUGH_DUMMY_STRING));

      switch (MyIfrNvData->L05MacAddressPassThrough) {

      case SELECT_INTERNAL_MAC_ADDRESS:
        if (ModifyMacAddrData->InternalMacExisted) {
          //
          // Update Internal Mac Address to SCU
          //
          UnicodeSPrint (
            CreateNetworkString,
            StrSize (L05_MAC_ADDRESS_PASS_THROUGH_DUMMY_STRING),
            L05_MAC_ADDRESS_PASS_THROUGH_STRING,
            ModifyMacAddrData->InternalMacAddrrss.Addr[0],
            ModifyMacAddrData->InternalMacAddrrss.Addr[1],
            ModifyMacAddrData->InternalMacAddrrss.Addr[2],
            ModifyMacAddrData->InternalMacAddrrss.Addr[3],
            ModifyMacAddrData->InternalMacAddrrss.Addr[4],
            ModifyMacAddrData->InternalMacAddrrss.Addr[5]
            );
        }
        break;

      case SELECT_SECOND_MAC_ADDRESS:
        if (ModifyMacAddrData->SecondMacExisted) {
          //
          // Update Second Mac Address to SCU
          //
          UnicodeSPrint (
            CreateNetworkString,
            StrSize (L05_MAC_ADDRESS_PASS_THROUGH_DUMMY_STRING),
            L05_MAC_ADDRESS_PASS_THROUGH_STRING,
            ModifyMacAddrData->SecondMacAddrrss.Addr[0],
            ModifyMacAddrData->SecondMacAddrrss.Addr[1],
            ModifyMacAddrData->SecondMacAddrrss.Addr[2],
            ModifyMacAddrData->SecondMacAddrrss.Addr[3],
            ModifyMacAddrData->SecondMacAddrrss.Addr[4],
            ModifyMacAddrData->SecondMacAddrrss.Addr[5]
            );
        }
        break;
      }

      TokenToUpdate = STRING_TOKEN (L05_STR_MAC_ADDRESS_STRING2);
      HiiSetString (HiiHandle, TokenToUpdate, CreateNetworkString, NULL);
      DEBUG ((DEBUG_INFO, "UpdateMacAddrrss Status: %r\n", Status));

      gBS->FreePool (ModifyMacAddrData);
      gBS->FreePool (CreateNetworkString);
    }
    break;
}
#endif

#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
  case L05_KEY_STORAGE_CONTROLLER_MODE:
    SataInterfaceMode = MyPchIfrNvData->SataInterfaceMode;

    if (SataInterfaceMode == TempSataInterfaceMode) {
      break;
    }

    //
    // [Lenovo BIOS Optane Support Version 1.4 Page 5]
    //   BIOS Setup
    //   Need to display Setup Warning when changing "Controller Mode"
    //
    if (!FeaturePcdGet (PcdH2OFormBrowserLocalMetroDESupported)) {
      StringPtr = HiiGetString (
                    HiiHandle,
                    STRING_TOKEN (L05_STR_STORAGE_WARNING_CONFIRM_STRING),
                    NULL
                    );

    } else {
      StringPtr = HiiGetString (
                    HiiHandle,
                    STRING_TOKEN (L05_STR_GRAPHIC_STORAGE_WARNING_CONFIRM_STRING),
                    NULL
                    );
    }

    gSUBrowser->H2ODialog->ConfirmDialog (
                             L05WarningDlgYesNo,
                             FALSE,
                             0,
                             NULL,
                             &Key,
                             StringPtr
                             );

    if (Key.UnicodeChar != CHAR_CARRIAGE_RETURN) {
      MyPchIfrNvData->SataInterfaceMode = TempSataInterfaceMode;
    }

    if (MyPchIfrNvData->SataInterfaceMode == SATA_MODE_RAID) {
      //
      // [Lenovo BIOS Optane Support Version 1.4 Page 2]
      //   BIOS Design -
      //   PCIe Remapping is always enabled when "Intel RST and System Acceleration with Intel Optane Technology" is selected
      //   RST Mode is selected -
      //   PCIe Remapping will be automatically enabled by RstOneClickEnable driver
      //
      BufferSize = sizeof (RST_CONFIG_VARIABLE);
      Status = gRT->GetVariable (
                      RstConfigVariableName,
                      &gRstConfigVariableGuid,
                      NULL,
                      &BufferSize,
                      &RstConfigVariable
                      );

      if (!EFI_ERROR (Status)) {
        RstConfigVariable.RemapEnable = TRUE;

        Status = gRT->SetVariable (
                        RstConfigVariableName,
                        &gRstConfigVariableGuid,
                        (EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS),
                        sizeof (RstConfigVariable),
                        &RstConfigVariable
                        );
      }

      //
      // Enable System Acceleration
      //
      MyPchIfrNvData->SataRstOptaneMemory = 1;

      //
      // [Lenovo BIOS Optane Support Version 1.4 Page 2 & 6]
      //   RST Mode is selected -
      //   Legacy boot is not supported and Legacy boot option will be grayed out for end user
      //   BIOS Setup -
      //   When IRST boot mode is selected, UEFI is automatically selected and grayed out
      //
      MyIfrNvData->BootType = EFI_BOOT_TYPE;

    } else if (MyPchIfrNvData->SataInterfaceMode == SATA_MODE_AHCI) {
      //
      // [Lenovo BIOS Optane Support Version 1.4 Page 2]
      //   AHCI Mode is selected -
      //   System Acceleration is not supported even if Optane is attached
      //
      MyPchIfrNvData->SataRstOptaneMemory = 0;
    }

    gBS->FreePool (StringPtr);  
    break;
#endif

#ifdef L05_BIOS_SELF_HEALING_SUPPORT
  case L05_KEY_BIOS_SELF_HEALING:

    //
    // [Lenovo BIOS Self-Healing Design Guidance Specification v1.9]
    //   2.6 BIOS Setup Option
    //     When disabled, BIOS should do:
    //       a. Show warning message
    //       b. Delete storage BIOS backup file (Do this only when performing operations related to "Save Changes")
    //       c. Notify EC to disable WDT (Do this only when performing operations related to "Save Changes")
    //
    if (MyIfrNvData->L05BiosSelfHealing == 1) {  // STR_ENABLED_TEXT
      break;
    }

    StringPtr = HiiGetString (
                  HiiHandle,
                  STRING_TOKEN (L05_STR_BIOS_SELF_HEALING_DISABLE_WARNING),
                  NULL
                  );

    gSUBrowser->H2ODialog->ConfirmDialog (
                             L05WarningDlgYesNo,
                             FALSE,
                             0,
                             NULL,
                             &Key,
                             StringPtr
                             );

    if (Key.UnicodeChar != CHAR_CARRIAGE_RETURN) {
      MyIfrNvData->L05BiosSelfHealing = 1;  // STR_ENABLED_TEXT
    }

    gBS->FreePool (StringPtr);
    break;
#endif

#ifdef L05_NOTEBOOK_CLOUD_BOOT_ENABLE
  case L05_KEY_LENOVO_CLOUD_SERVICE:
    MyIfrNvData->NetworkProtocol = (MyIfrNvData->L05LenovoCloudServices == 0) ? UEFI_NETWORK_BOOT_OPTION_PXE_BOTH : UEFI_NETWORK_BOOT_OPTION_HTTP_PXE_BOTH;
    break;
#endif

#ifdef L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE
  case L05_KEY_LENOVO_CLOUD_SERVICE_WIFI:
    MySetupDataIfrNvData->PrebootBleEnable = MyIfrNvData->L05UefiWifiNetworkBoot;
    break;
#endif

  default:
#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
{
    EFI_STATUS                          OverclockStatus;

    OverclockStatus = OverclockCallback (
                        This,
                        Action,
                        QuestionId,
                        Type,
                        Value,
                        ActionRequest
                        );

    if (!EFI_ERROR (OverclockStatus)) {
      break;
    }
}
#endif

    Status = HotKeyCallBack (
               This,
               Action,
               QuestionId,
               Type,
               Value,
               ActionRequest
               );
    break;
  }

  BufferSize = sizeof (SYSTEM_CONFIGURATION);
  SetupVariableConfig (
    NULL,
    NULL,
    BufferSize,
    (UINT8 *) gSUBrowser->SCBuffer,
    FALSE
    );
  SetupVariableConfig (
    &gSaSetupVariableGuid,
    SA_SETUP_VARIABLE_NAME,
    sizeof (SA_SETUP),
    (UINT8 *) gRcSUBrowser->SaSUBrowserData,
    FALSE
    );
  SetupVariableConfig (
    &gCpuSetupVariableGuid,
    CPU_SETUP_VARIABLE_NAME,
    sizeof (CPU_SETUP),
    (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
    FALSE
    );
  SetupVariableConfig (
    &gPchSetupVariableGuid,
    PCH_SETUP_VARIABLE_NAME,
    sizeof (PCH_SETUP),
    (UINT8 *) gRcSUBrowser->PchSUBrowserData,
    FALSE
    );
  SetupVariableConfig (
    &gSetupVariableGuid,
    PLATFORM_SETUP_VARIABLE_NAME,
    sizeof (SETUP_DATA),
    (UINT8 *) gRcSUBrowser->SetupDataSUBrowserData,
    FALSE
    );

  return Status;
}

EFI_STATUS
InstallConfigurationCallbackRoutine (
  IN EFI_HANDLE                         DriverHandle,
  IN EFI_HII_HANDLE                     HiiHandle
  )
{
  EFI_STATUS                            Status;
  EFI_GUID                              FormsetGuid = FORMSET_ID_GUID_CONFIGURATION;

  mConfigurationCallBackInfo = AllocatePool (sizeof (EFI_CALLBACK_INFO));

  if (mConfigurationCallBackInfo == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  mConfigurationCallBackInfo->Signature                    = EFI_CALLBACK_INFO_SIGNATURE;
  mConfigurationCallBackInfo->DriverCallback.ExtractConfig = gSUBrowser->ExtractConfig;
  mConfigurationCallBackInfo->DriverCallback.RouteConfig   = gSUBrowser->RouteConfig;
  mConfigurationCallBackInfo->DriverCallback.Callback      = ConfigurationCallbackRoutine;
  mConfigurationCallBackInfo->HiiHandle                    = HiiHandle;
  CopyGuid (&mConfigurationCallBackInfo->FormsetGuid, &FormsetGuid);

  //
  // Install protocol interface
  //
  Status = gBS->InstallProtocolInterface (
                  &DriverHandle,
                  &gEfiHiiConfigAccessProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  &mConfigurationCallBackInfo->DriverCallback
                  );
  ASSERT_EFI_ERROR (Status);

  Status = InitConfigurationMenu (HiiHandle);

  return Status;
}

EFI_STATUS
UninstallConfigurationCallbackRoutine (
  IN EFI_HANDLE                         DriverHandle
  )
{
  EFI_STATUS     Status;

  if (mConfigurationCallBackInfo == NULL) {
    return EFI_SUCCESS;
  }

  Status = gBS->UninstallProtocolInterface (
                  DriverHandle,
                  &gEfiHiiConfigAccessProtocolGuid,
                  &mConfigurationCallBackInfo->DriverCallback
                  );
  ASSERT_EFI_ERROR (Status);
  gBS->FreePool (mConfigurationCallBackInfo);
  mConfigurationCallBackInfo = NULL;
  return Status;
}

EFI_STATUS
InitConfigurationMenu (
  IN EFI_HII_HANDLE                     HiiHandle
  )
{
  EFI_STATUS                            Status = EFI_SUCCESS;
  SYSTEM_CONFIGURATION                  *MyIfrNvData;
  PCH_SETUP                             *MyPchIfrNvData;
#ifdef L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE
  EFI_GUID                              WifiConfigFormSetGuid = L05_WIFI_CONFIG_FORMSET_GUID;
#endif

  MyIfrNvData    = (SYSTEM_CONFIGURATION *) gSUBrowser->SCBuffer;
  MyPchIfrNvData = (PCH_SETUP *) gRcSUBrowser->PchSUBrowserData;

  //
  // Update Information on Configuration Menu
  //
  Status = UpdateConfigurationMenu (HiiHandle);

#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
  //
  // Link external Intel RST formset
  //
  LinkIntelRstFormset (HiiHandle);
#endif

#ifdef L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE
  //
  // Link external Intel WIFI Configuration formset
  //
  LinkIntelWifiFormset (
    HiiHandle,
    &WifiConfigFormSetGuid,
    ROOT_FORM_ID,
    L"Wi-Fi Configuration",
    0,
    L05_KEY_LINK_WIFI_CONFIG_FORMSET_BASE,
    L05_LINK_WIFI_CONFIG_FORMSET_LABEL,
    L05_LINK_WIFI_CONFIG_FORMSET_LABEL_END,
    L05_NETWORK_BOOT_SETTINGS_FORM_ID
    );
#endif

  //
  // Record current SataInterfaceMode
  //
  MyIfrNvData->CurrentSataInterfaceMode = MyPchIfrNvData->SataInterfaceMode;

  return Status;
}
