//;******************************************************************************
//;* Copyright (c) 2013 - 2014, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************

#include <Uefi.h>
#include <SetupConfig.h>
#include <SetupUtility.h>

#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HiiLib.h>  // HiiGetString, HiiSetString
#ifdef L05_MAC_ADDRESS_PASS_THROUGH_ENABLE
#include <L05MacAddressPassThroughVariable.h>
#endif

/*++
  This is a callback function for setting L05 string on Configuration Menu

  @param  HiiHandle                     The HII Handle for current Menu

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_UNSUPPORTED               Cannot get current String from HII database
  @retval Others                        Depend on gRT->GetVariable()
--*/
EFI_STATUS
UpdateConfigurationMenu (
  IN EFI_HII_HANDLE                     HiiHandle
  )
{
  EFI_STATUS                            Status;
  CHAR16                                *RaidStringPtr;
  EFI_HII_HANDLE                        AdvanceHandle;

  Status        = EFI_SUCCESS;
  RaidStringPtr = NULL;
  AdvanceHandle = gSUBrowser->SUCInfo->MapTable[AdvanceHiiHandle].HiiHandle;

#ifdef L05_MAC_ADDRESS_PASS_THROUGH_ENABLE
{
  CHAR16                                *CreateNetworkString;
  STRING_REF                            TokenToUpdate;
  L05_MAC_ADDRESS_DATA                  *ModifyMacAddrData;
  SYSTEM_CONFIGURATION                  *MyIfrNvData;

  //
  // Changed L05_STR_MAC_ADDRESS_STRING2 "MAC Address"
  //
  MyIfrNvData       = NULL;
  ModifyMacAddrData = NULL;

  ModifyMacAddrData = CommonGetVariableData (L05_MAC_ADDRESS_PASS_THROUGH_VARIABLE_NAME, &gH2OBdsCpNetworkUpdateMacAddrGuid);

  if (ModifyMacAddrData != NULL) {
    MyIfrNvData         = (SYSTEM_CONFIGURATION *) gSUBrowser->SCBuffer;
    CreateNetworkString = AllocateZeroPool (StrSize (L05_MAC_ADDRESS_PASS_THROUGH_DUMMY_STRING));

    switch (MyIfrNvData->L05MacAddressPassThrough) {

    case SELECT_INTERNAL_MAC_ADDRESS :
      if (ModifyMacAddrData->InternalMacExisted) {
        //
        // Update Internal Mac Address to SCU
        //
        UnicodeSPrint (
          CreateNetworkString,
          StrSize (L05_MAC_ADDRESS_PASS_THROUGH_DUMMY_STRING),
          L05_MAC_ADDRESS_PASS_THROUGH_STRING,
          ModifyMacAddrData->InternalMacAddrrss.Addr[0],
          ModifyMacAddrData->InternalMacAddrrss.Addr[1],
          ModifyMacAddrData->InternalMacAddrrss.Addr[2],
          ModifyMacAddrData->InternalMacAddrrss.Addr[3],
          ModifyMacAddrData->InternalMacAddrrss.Addr[4],
          ModifyMacAddrData->InternalMacAddrrss.Addr[5]
          );
      }
      break;

    case SELECT_SECOND_MAC_ADDRESS :
      if (ModifyMacAddrData->SecondMacExisted) {
        //
        // Update Second Mac Address to SCU
        //
        UnicodeSPrint (
          CreateNetworkString,
          StrSize (L05_MAC_ADDRESS_PASS_THROUGH_DUMMY_STRING),
          L05_MAC_ADDRESS_PASS_THROUGH_STRING,
          ModifyMacAddrData->SecondMacAddrrss.Addr[0],
          ModifyMacAddrData->SecondMacAddrrss.Addr[1],
          ModifyMacAddrData->SecondMacAddrrss.Addr[2],
          ModifyMacAddrData->SecondMacAddrrss.Addr[3],
          ModifyMacAddrData->SecondMacAddrrss.Addr[4],
          ModifyMacAddrData->SecondMacAddrrss.Addr[5]
          );
      }
      break;
    }

    TokenToUpdate = STRING_TOKEN (L05_STR_MAC_ADDRESS_STRING2);
    HiiSetString (HiiHandle, TokenToUpdate, CreateNetworkString, NULL);
    DEBUG ((DEBUG_INFO, "UpdateMacAddrrss Status: %r\n", Status));

    gBS->FreePool (ModifyMacAddrData);
    gBS->FreePool (CreateNetworkString);
  }
}
#endif

//
//  AlderLake not support RAID mode
//
//  //
//  //  Intel CRB code changed STR_SATA_RAID "RAID" to related RST Mode strings by InitializeRstModeStrings function,
//  //  so feature code sync STR_SATA_RAID to L05_STR_HDC_TYPE_RAID_TEXT.
//  //
//  RaidStringPtr = HiiGetString (AdvanceHandle, STRING_TOKEN (STR_SATA_RAID), NULL);
//
//  if (RaidStringPtr != NULL) {
//    HiiSetString (HiiHandle, STRING_TOKEN (L05_STR_RAID_STRING), RaidStringPtr, NULL);
//    gBS->FreePool (RaidStringPtr);
//
//  } else {
//    Status = EFI_UNSUPPORTED;
//  }

  return  Status;
}
