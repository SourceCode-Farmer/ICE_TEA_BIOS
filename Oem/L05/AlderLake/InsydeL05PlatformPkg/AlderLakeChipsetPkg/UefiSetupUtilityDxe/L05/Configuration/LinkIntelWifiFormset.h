/** @file
  Provide functions to link external Intel WIFI Configuration formset.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _LINK_INTEL_WIFI_FORMSET_H_
#define _LINK_INTEL_WIFI_FORMSET_H_

#include <Library/UefiHiiServicesLib.h>

EFI_STATUS
LinkIntelWifiFormset (
  IN  EFI_HII_HANDLE                        HiiHandle,
  IN  EFI_GUID                              *RefFormSetId,
  IN  EFI_FORM_ID                           RefFormId,
  IN  EFI_STRING                            DefaultFormSetTitle,
  IN  UINT8                                 QuestionFlags,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT16                                UpdateLabel,
  IN  UINT16                                UpdateEndLabel,
  IN  EFI_FORM_ID                           UpdateFormId
  );
#endif
