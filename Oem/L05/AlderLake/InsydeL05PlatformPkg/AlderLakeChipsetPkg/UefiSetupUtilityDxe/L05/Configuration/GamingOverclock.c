//;******************************************************************************
//;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************

#include "Configuration.h"
#include "L05OverclockUi.h"
#include <L05ChipsetNameList.h>
#include <Uefi/UefiInternalFormRepresentation.h>  // EFI_IFR_TYPE_NUM_SIZE_#
#include <Library/BaseMemoryLib.h>

//_Start_L05_AMD_PLATFORM_
#if (FixedPcdGetBool (PcdL05AmdSetupSupported))
//
// PBS Setup
//
#include <Dxe/AmdPbsSetupDxe/AmdPbsConfig.h>

//
// CBS Setup
//
#include <Guid/AmdCbsConfig.h>
#include <Build/ResourceRMB/AmdCbsVariable.h>
#include <Build/ResourceRMB/AmdCbsFormID.h>
#include <Library\Family\0x19\RMB\External\CbsSetupLib\CbsCustomCorePstates.h>

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_REMBRANDT)
//
// AOD Setup
//
#include <Features/AODv2/Include/AodConfigRmb.h>  // AOD_CONFIG
#endif
#endif
//_End_L05_AMD_PLATFORM_

#if (FixedPcdGetBool (PcdL05AmdSetupSupported))
extern UINT8                            *mL05PbsSetupIfrData;
extern UINT8                            *mL05CbsSetupIfrData;
extern UINT8                            *mL05AodSetupIfrData;
#endif

SYSTEM_CONFIGURATION                    mBackupSetupNvData;
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
PCH_SETUP                               mBackupPchSetupNvData;
SA_SETUP                                mBackupSaSetupNvData;
CPU_SETUP                               mBackupCpuSetupNvData;
#endif

#if (L05_CHIPSET_VENDOR_ID == L05_AMD_VENDOR_ID)
AMD_PBS_SETUP_OPTION                    mBackupPbsSetupIfrData;
CBS_CONFIG                              mBackupCbsSetupIfrData;
AOD_CONFIG                              mBackupAodSetupIfrData;
#endif

/**
  Get page data according to specific Form ID.

  @param[in]  This                      A pointer to the H2O_FORM_BROWSER_PROTOCOL instance.
  @param[in]  FormId                    Specific input Form ID.
  @param[out] PageInfo                  A pointer to point H2O_FORM_BROWSER_P instance.

  @retval EFI_SUCCESS                   Get page data successful.
  @retval EFI_INVALID_PARAMETER         This is NULL, or PageInfo is NULL.
  @retval EFI_NOT_FOUND                 Cannot find specific input page ID.
  @retval EFI_OUT_OF_RESOURCES          Could not allocate resources for page.
**/
EFI_STATUS
L05GetPInfoByFormId (
  IN  H2O_FORM_BROWSER_PROTOCOL         *This,
  IN  H2O_FORM_ID                       FormId,
  OUT H2O_FORM_BROWSER_P                **PageInfo
  )
{
  EFI_STATUS                            Status;
  UINT32                                PageIdCount;
  H2O_PAGE_ID                           *PageIdList;
  UINTN                                 PageIndex;
  H2O_PAGE_ID                           PageId;

  if (This == NULL || FormId == 0 || PageInfo == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  PageIdCount = 0;
  PageIdList  = NULL;

  Status = This->GetPAll (This, &PageIdCount, &PageIdList);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  for (PageIndex = 0; PageIndex < PageIdCount; PageIndex++) {
    PageId = PageIdList[PageIndex];

    if ((PageId & 0xFFFF) == FormId) {
      break;
    }
  }

  FreePool (PageIdList);

  if (PageIndex == PageIdCount) {
    return EFI_NOT_FOUND;
  }

  Status = This->GetPInfo (This, PageId, PageInfo);

  return Status;
}

/**
  Overclock Set Setup Item Default.

  @param This                           Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param Action                         Specifies the type of action taken by the browser.
  @param Type                           The type of value for the question.
  @param ActionRequest                  On return, points to the action requested by the callback function. Type
                                        EFI_BROWSER_ACTION_REQUEST is specified in SendForm() in the Form Browser Protocol.
  @param Statement                      A pointer to H2O_FORM_BROWSER_S.
  @param SetupBuffer                    A pointer to Setup Buffer.
  @param SetupDefault                   A pointer to Setup Default Buffer.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
VOID
OverclockSetSetupItemDefault (
  IN     CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN     EFI_BROWSER_ACTION                   Action,
  IN     UINT8                                Type,
  OUT    EFI_BROWSER_ACTION_REQUEST           *ActionRequest,
  IN     H2O_FORM_BROWSER_S                   *Statement,
  IN OUT UINT8                                *SetupBuffer,
  IN     UINT8                                *SetupDefault
  )
{
  BOOLEAN                               IsUpdate;
  UINT8                                 *SetupValue;
  UINT8                                 *SetupDefaultValue;
  EFI_IFR_TYPE_VALUE                    Value;

  if (Statement == NULL || SetupBuffer == NULL || SetupDefault == NULL) {
    return;
  }

  IsUpdate = FALSE;
  SetupValue = (UINT8 *)((UINTN) SetupBuffer + Statement->VariableOffset);
  SetupDefaultValue = (UINT8 *)((UINTN) SetupDefault + Statement->VariableOffset);
  ZeroMem (&Value, sizeof (Value));

  switch (Statement->HiiValue.Type) {

  case EFI_IFR_TYPE_NUM_SIZE_8:
    if (*(UINT8 *) SetupValue != *(UINT8 *) SetupDefaultValue) {
      *(UINT8 *) SetupValue = *(UINT8 *) SetupDefaultValue;
      Value.u8 = *(UINT8 *) SetupValue;
      IsUpdate = TRUE;
    }
    break;

  case EFI_IFR_TYPE_NUM_SIZE_16:
    if (*(UINT16 *) SetupValue != *(UINT16 *) SetupDefaultValue) {
      *(UINT16 *) SetupValue = *(UINT16 *) SetupDefaultValue;
      Value.u16 = *(UINT16 *) SetupValue;
      IsUpdate = TRUE;
    }
    break;

  case EFI_IFR_TYPE_NUM_SIZE_32:
    if (*(UINT32 *) SetupValue != *(UINT32 *) SetupDefaultValue) {
      *(UINT32 *) SetupValue = *(UINT32 *) SetupDefaultValue;
      Value.u32 = *(UINT32 *) SetupValue;
      IsUpdate = TRUE;
    }
    break;

  case EFI_IFR_TYPE_NUM_SIZE_64:
    if (*(UINT64 *) SetupValue != *(UINT64 *) SetupDefaultValue) {
      *(UINT64 *) SetupValue = *(UINT64 *) SetupDefaultValue;
      Value.u64 = *(UINT64 *) SetupValue;
      IsUpdate = TRUE;
    }
    break;
  }

  if (IsUpdate) {
    SetupVariableConfig (
      NULL,
      NULL,
      sizeof (SYSTEM_CONFIGURATION),
      (UINT8 *) gSUBrowser->SCBuffer,
      FALSE
      );

#if (FixedPcdGetBool (PcdL05PchSetupSupported))
    SetupVariableConfig (
      &gSaSetupVariableGuid,
      SA_SETUP_VARIABLE_NAME,
      sizeof (SA_SETUP),
      (UINT8 *) gRcSUBrowser->SaSUBrowserData,
      FALSE
      );
    SetupVariableConfig (
      &gCpuSetupVariableGuid,
      CPU_SETUP_VARIABLE_NAME,
      sizeof (CPU_SETUP),
      (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
      FALSE
      );
    SetupVariableConfig (
      &gPchSetupVariableGuid,
      PCH_SETUP_VARIABLE_NAME,
      sizeof (PCH_SETUP),
      (UINT8 *) gRcSUBrowser->PchSUBrowserData,
      FALSE
      );
#endif

    ConfigurationCallbackRoutine (This, Action, Statement->QuestionId, Type, &Value, ActionRequest);
  }

  return;
}

/**
  Overclock Load Default by QuestionId.

  @param This                           Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param Action                         Specifies the type of action taken by the browser.
  @param QuestionId                     A unique value which is sent to the original exporting driver so that it can identify the
                                        type of data to expect. The format of the data tends to vary based on the opcode that
                                        generated the callback.
  @param Type                           The type of value for the question.
  @param ActionRequest                  On return, points to the action requested by the callback function. Type
                                        EFI_BROWSER_ACTION_REQUEST is specified in SendForm() in the Form Browser Protocol.
  @param SetupBuffer                    A pointer to SYSTEM_CONFIGURATION Buffer.
  @param PchSetupBuffer                 A pointer to PCH_SETUP Buffer.
  @param CpuSetupBuffer                 A pointer to CPU_SETUP Buffer.
  @param SaSetupBuffer                  A pointer to SA_SETUP Buffer.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
OverclockLoadDefault (
  IN     CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN     EFI_BROWSER_ACTION                   Action,
  IN     EFI_QUESTION_ID                      QuestionId,
  IN     UINT8                                Type,
  OUT    EFI_BROWSER_ACTION_REQUEST           *ActionRequest,
  IN OUT UINT8                                *SetupBuffer,
  IN OUT UINT8                                *PchSetupBuffer,
  IN OUT UINT8                                *CpuSetupBuffer,
  IN OUT UINT8                                *SaSetupBuffer
  )
{
  EFI_STATUS                            Status;
  EFI_STATUS                            GetSetupStatus;
  EFI_STATUS                            GetSaSetupStatus;
  EFI_STATUS                            GetMeSetupStatus;
  EFI_STATUS                            GetCpuSetupStatus;
  EFI_STATUS                            GetPchSetupStatus;
  EFI_STATUS                            GetMeSetupStorageStatus;
  EFI_STATUS                            GetSetupDataStatus;
  EFI_STATUS                            GetSiSetupStatus;
  EFI_CALLBACK_INFO                     *CallbackInfo;
  UINTN                                 BufferSize;
  CHIPSET_CONFIGURATION                 *SetupNvDataDefault;
  UINTN                                 SaBufferSize;
  SA_SETUP                              *SaSetupNvDataDefault;
  UINTN                                 MeBufferSize;
  ME_SETUP                              *MeSetupNvDataDefault;
  UINTN                                 CpuBufferSize;
  CPU_SETUP                             *CpuSetupNvDataDefault;
  UINTN                                 PchBufferSize;
  PCH_SETUP                             *PchSetupNvDataDefault;
  UINTN                                 MeStorageBufferSize;
  ME_SETUP_STORAGE                      *MeSetupStorageNvDataDefault;
  UINTN                                 SetupDataBufferSize;
  SETUP_DATA                            *SetupDataNvDataDefault;
  UINTN                                 SiBufferSize;
  SI_SETUP                              *SiSetupNvDataDefault;
  H2O_FORM_BROWSER_PROTOCOL             *FBProtocol;
  L05_OVERCLOCK_SETUP_DATA              *L05OverclockSetupDataTable;
  UINTN                                 L05OverclockSetupDataTableCount;
  UINT32                                DefaultGroup;
  H2O_FORM_BROWSER_P                    *CurrentP;
  CHAR16                                *StringPtr;
  H2O_FORM_BROWSER_P                    *GamingOverclockingP;
  UINTN                                 TableIndex;
  UINTN                                 Index;
  H2O_FORM_BROWSER_S                    *Statement;
  H2O_FORM_BROWSER_VS                   *VarStoreData;

  GetSetupStatus          = EFI_NOT_FOUND;
  GetSaSetupStatus        = EFI_NOT_FOUND;
  GetMeSetupStatus        = EFI_NOT_FOUND;
  GetCpuSetupStatus       = EFI_NOT_FOUND;
  GetPchSetupStatus       = EFI_NOT_FOUND;
  GetMeSetupStorageStatus = EFI_NOT_FOUND;
  GetSetupDataStatus      = EFI_NOT_FOUND;
  GetSiSetupStatus        = EFI_NOT_FOUND;

  CallbackInfo                    = NULL;
  FBProtocol                      = NULL;
  L05OverclockSetupDataTable      = NULL;
  L05OverclockSetupDataTableCount = 0;
  DefaultGroup                    = 0;
  CurrentP                        = NULL;
  StringPtr                       = NULL;
  GamingOverclockingP             = NULL;
  Statement                       = NULL;
  VarStoreData                    = NULL;

  if (SetupBuffer == NULL) {
    return EFI_UNSUPPORTED;
  }

#if (FixedPcdGetBool (PcdL05PchSetupSupported))
  if ((PchSetupBuffer == NULL) || (CpuSetupBuffer == NULL) || (SaSetupBuffer == NULL)) {
    return EFI_UNSUPPORTED;
  }
#endif

  CallbackInfo = EFI_CALLBACK_INFO_FROM_THIS (This);

  Status = gBS->LocateProtocol (
                  &gH2OFormBrowserProtocolGuid,
                  NULL,
                  (VOID **) &FBProtocol
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Check the setup variable was create or not, if not then create default setup variable.
  //
  BufferSize = PcdGet32 (PcdSetupConfigSize);
  Status = gBS->AllocatePool(
                  EfiACPIMemoryNVS,
                  BufferSize,
                  (VOID **)&SetupNvDataDefault
                  );
  if (EFI_ERROR(Status)) {
    return Status;
  }
  ZeroMem (SetupNvDataDefault, BufferSize);

  {
    //============================================//
    // Allocate a memory to put SA setup variable //
    //============================================//
    SaBufferSize = sizeof (SA_SETUP);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    SaBufferSize,
                    (VOID **)&SaSetupNvDataDefault
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (SaSetupNvDataDefault, SaBufferSize);
    //============================================//
    // Allocate a memory to put ME setup variable //
    //============================================//
    MeBufferSize = sizeof (ME_SETUP);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    MeBufferSize,
                    (VOID **)&MeSetupNvDataDefault
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (MeSetupNvDataDefault, MeBufferSize);
    //=============================================//
    // Allocate a memory to put CPU setup variable //
    //=============================================//
    CpuBufferSize = sizeof (CPU_SETUP);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    CpuBufferSize,
                    (VOID **)&CpuSetupNvDataDefault
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (CpuSetupNvDataDefault, CpuBufferSize);
    //=============================================//
    // Allocate a memory to put PCH setup variable //
    //=============================================//
    PchBufferSize = sizeof (PCH_SETUP);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    PchBufferSize,
                    (VOID **)&PchSetupNvDataDefault
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (PchSetupNvDataDefault, PchBufferSize);
    //====================================================//
    // Allocate a memory to put ME setup storage variable //
    //====================================================//
    MeStorageBufferSize = sizeof (ME_SETUP_STORAGE);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    MeStorageBufferSize,
                    (VOID **)&MeSetupStorageNvDataDefault
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (MeSetupStorageNvDataDefault, MeStorageBufferSize);
    //==============================================//
    // Allocate a memory to put SETUP DATA variable //
    //==============================================//
    SetupDataBufferSize = sizeof (SETUP_DATA);
    Status = gBS->AllocatePool (
                    EfiACPIMemoryNVS,
                    SetupDataBufferSize,
                    (VOID **)&SetupDataNvDataDefault
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (SetupDataNvDataDefault, SetupDataBufferSize);
    //=============================================//
    // Allocate a memory to put SI setup variable //
    //=============================================//
    SiBufferSize = sizeof (SI_SETUP);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    SiBufferSize,
                    (VOID **)&SiSetupNvDataDefault
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (SiSetupNvDataDefault, SiBufferSize);
  }

  //
  // Get Setup Default
  //
  DefaultSetup (SetupNvDataDefault);
  DefaultSaSetup (&SaSetupNvDataDefault, EfiACPIMemoryNVS);
  DefaultMeSetup (&MeSetupNvDataDefault, EfiACPIMemoryNVS);
  DefaultCpuSetup (&CpuSetupNvDataDefault, EfiACPIMemoryNVS);
  DefaultPchSetup (&PchSetupNvDataDefault, EfiACPIMemoryNVS);
  DefaultSetupData (&SetupDataNvDataDefault, EfiACPIMemoryNVS);
  DefaultSiSetup (&SiSetupNvDataDefault, EfiACPIMemoryNVS);
  DefaultMeSetupStorage (&MeSetupStorageNvDataDefault, EfiACPIMemoryNVS);

  SetupRuntimeDetermination (
    SetupNvDataDefault, GetSetupStatus,
    SaSetupNvDataDefault, GetSaSetupStatus,
    MeSetupNvDataDefault, GetMeSetupStatus,
    CpuSetupNvDataDefault, GetCpuSetupStatus,
    PchSetupNvDataDefault, GetPchSetupStatus,
    MeSetupStorageNvDataDefault, GetMeSetupStorageStatus,
    SetupDataNvDataDefault, GetSetupDataStatus,
    SiSetupNvDataDefault, GetSiSetupStatus
    );

#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  L05DefaultSetup ((UINT8 *) SetupNvDataDefault, (UINT8 *) PchSetupNvDataDefault, (UINT8 *) CpuSetupNvDataDefault, (UINT8 *) SaSetupNvDataDefault);
#else
  L05DefaultSetup ((UINT8 *) SetupNvDataDefault, NULL, NULL, NULL);
#endif

  //
  // Set Default Group
  //
  switch (QuestionId) {

  case L05_KEY_OVERCLOCK_CPU_UNLOCK:
    DefaultGroup = BITS (CpuControl) | BITS (CpuAdvanced) | BITS (CpuAdvancedGroup);
    break;

  case L05_KEY_CPU_OVERCLOCKING:
    DefaultGroup = BITS (CpuAdvanced) | BITS (CpuAdvancedGroup);
    break;

  case L05_KEY_CPU_ADVANCED_OVERCLOCKING:
    DefaultGroup = BITS (CpuAdvancedGroup);
    break;

  case L05_KEY_OVERCLOCK_GPU_UNLOCK:
    DefaultGroup = BITS (GpuControl) | BITS (GpuAdvanced) | BITS (GpuAdvancedGroup);
    break;

  case L05_KEY_GPU_OVERCLOCKING:
    DefaultGroup = BITS (GpuAdvanced) | BITS (GpuAdvancedGroup);
    break;

  case L05_KEY_GPU_ADVANCED_OVERCLOCKING:
    DefaultGroup = BITS (GpuAdvancedGroup);
    break;

  case L05_KEY_OVERCLOCK_SET_TO_DEFAULT:
    DefaultGroup = (UINT32) (-1);
    break;

  default:
    return EFI_UNSUPPORTED;
  }

  //
  // Get Overclock Setup Data Table
  //
  L05OverclockSetupDataTable = (L05_OVERCLOCK_SETUP_DATA *) PcdGet64 (PcdL05GamingOverClockSetupDataTable);
  L05OverclockSetupDataTableCount = PcdGet32 (PcdL05GamingOverClockSetupDataTableCount);

  if (L05OverclockSetupDataTable == NULL || L05OverclockSetupDataTableCount == 0) {
    return EFI_UNSUPPORTED;
  }

  //
  // When CurrentP is Configuration Page, update CurrentP to Gaming Overclocking Page for get statement of Overclocking Items.
  //
  CurrentP = FBProtocol->CurrentP;
  StringPtr = HiiGetString (CallbackInfo->HiiHandle, STRING_TOKEN (STR_CONFIGURATION_TITLE), NULL);
  if (StrCmp (StringPtr, CurrentP->PageTitle) == 0) {
    Status = L05GetPInfoByFormId (FBProtocol, L05_GAMING_OVERCLOCKING_FORM_ID, &GamingOverclockingP);
    if (!EFI_ERROR (Status) && GamingOverclockingP != NULL) {
      CurrentP = GamingOverclockingP;
    }
  }

  for (TableIndex = 0; TableIndex < L05OverclockSetupDataTableCount; TableIndex++) {

    if ((DefaultGroup & BITS (L05OverclockSetupDataTable[TableIndex].ItemGroup)) == 0) {
      continue;
    }

    //
    // Get Statement of QuestionId
    //
    for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
      Status = FBProtocol->GetSInfo (FBProtocol, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

      if (EFI_ERROR (Status)) {
        continue;
      }

      if (Statement->QuestionId != L05OverclockSetupDataTable[TableIndex].QuestionId) {
        FreePool (Statement);
        continue;
      }

      break;
    }

    if (Index == CurrentP->NumberOfStatementIds) {
      continue;
    }

    //
    // Get Variable Store Guid by GetVSInfo ()
    //
    Status = FBProtocol->GetVSInfo (FBProtocol, Statement->PageId, Statement->VarStoreId, &VarStoreData);

    if (CompareGuid (&gSystemConfigurationGuid, &VarStoreData->Guid)) {
      OverclockSetSetupItemDefault (This, Action, Type, ActionRequest, Statement, SetupBuffer, (UINT8 *) SetupNvDataDefault);

#if (FixedPcdGetBool (PcdL05PchSetupSupported))
    } else if (CompareGuid (&gPchSetupVariableGuid, &VarStoreData->Guid)) {
      OverclockSetSetupItemDefault (This, Action, Type, ActionRequest, Statement, PchSetupBuffer, (UINT8 *) PchSetupNvDataDefault);

    } else if (CompareGuid (&gCpuSetupVariableGuid, &VarStoreData->Guid)) {
      OverclockSetSetupItemDefault (This, Action, Type, ActionRequest, Statement, CpuSetupBuffer, (UINT8 *) CpuSetupNvDataDefault);

    } else if (CompareGuid (&gSaSetupVariableGuid, &VarStoreData->Guid)) {
      OverclockSetSetupItemDefault (This, Action, Type, ActionRequest, Statement, SaSetupBuffer, (UINT8 *) SaSetupNvDataDefault);
#endif
    }

    FreePool (Statement);
  }

  if (GamingOverclockingP != NULL) {
    FreePool (GamingOverclockingP);
  }

  FreePool (SetupNvDataDefault);
  FreePool (SaSetupNvDataDefault);
  FreePool (MeSetupNvDataDefault);
  FreePool (CpuSetupNvDataDefault);
  FreePool (PchSetupNvDataDefault);
  FreePool (MeSetupStorageNvDataDefault);
  FreePool (SetupDataNvDataDefault);
  FreePool (SiSetupNvDataDefault);

  return Status;
}

/**
  Proccess Overclock Question Action.

  @param This                           Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param Action                         Specifies the type of action taken by the browser.
  @param QuestionId                     A unique value which is sent to the original exporting driver so that it can identify the
                                        type of data to expect. The format of the data tends to vary based on the opcode that
                                        generated the callback.
  @param Type                           The type of value for the question.
  @param Value                          A pointer to the data being sent to the original exporting driver. The type is specified
                                        by Type. Type EFI_IFR_TYPE_VALUE is defined in EFI_IFR_ONE_OF_OPTION.
  @param ActionRequest                  On return, points to the action requested by the callback function. Type
                                        EFI_BROWSER_ACTION_REQUEST is specified in SendForm() in the Form Browser Protocol.

  @retval EFI_SUCCESS                   The callback successfully handled the action.
  @retval EFI_OUT_OF_RESOURCES          Not enough storage is available to hold the variable and its data.
  @retval EFI_DEVICE_ERROR              The variable could not be saved.
  @retval EFI_UNSUPPORTED               The specified Action is not supported by the callback.
**/
EFI_STATUS
OverclockCallback (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  )
{
  EFI_STATUS                            Status;

  Status = EFI_SUCCESS;

  switch (QuestionId) {

  case L05_KEY_CPU_OVERCLOCKING:
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
    AdvanceCallbackRoutine (This, Action, KEY_OC_ENABLE_DEPENDENCY, Type, Value, ActionRequest);
#endif

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_REMBRANDT)
    //
    // Update CPU OC relate items.
    //
    if (Value->u8 == 0x02) { // ENABLED
      ((AOD_CONFIG *) mL05AodSetupIfrData)->AodNbioPBOScalarCtl = 0x01; // MANUAL
      ((AOD_CONFIG *) mL05AodSetupIfrData)->AodCpuBoostClock = 0x01; // POSITIVE_SIGN
      ((AOD_CONFIG *) mL05AodSetupIfrData)->AodCurveOptimizer = 0x01; // ALL_CORES
      ((AOD_CONFIG *) mL05AodSetupIfrData)->AodAllCoreCurveOptimizerSign = 0x00; // POSITIVE_SIGN

    } else if (Value->u8 == 0) { // DISABLED
      ((AOD_CONFIG *) mL05AodSetupIfrData)->AodNbioPBOScalarCtl = 0x01; // MANUAL
      ((AOD_CONFIG *) mL05AodSetupIfrData)->AodCpuBoostClock = 0x01; // POSITIVE_SIGN
      ((AOD_CONFIG *) mL05AodSetupIfrData)->AodCurveOptimizer = 0x01; // ALL_CORES
      ((AOD_CONFIG *) mL05AodSetupIfrData)->AodAllCoreCurveOptimizerSign = 0x00; // POSITIVE_SIGN
    }
#endif

  case L05_KEY_OVERCLOCK_CPU_UNLOCK:
  case L05_KEY_OVERCLOCK_GPU_UNLOCK:
  case L05_KEY_CPU_ADVANCED_OVERCLOCKING:
  case L05_KEY_GPU_OVERCLOCKING:
  case L05_KEY_GPU_ADVANCED_OVERCLOCKING:
  case L05_KEY_OVERCLOCK_SET_TO_DEFAULT:
    if (Value->u8 == 0) {
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
      OverclockLoadDefault (
        This,
        Action,
        QuestionId,
        Type,
        ActionRequest,
        (UINT8 *) gSUBrowser->SCBuffer,
        (UINT8 *) gRcSUBrowser->PchSUBrowserData,
        (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
        (UINT8 *) gRcSUBrowser->SaSUBrowserData
        );
#endif
#if (L05_CHIPSET_VENDOR_ID == L05_AMD_VENDOR_ID)
      OverclockLoadDefault (
        This,
        Action,
        QuestionId,
        Type,
        ActionRequest,
        (UINT8 *) gSUBrowser->SCBuffer,
        (UINT8 *) NULL,
        (UINT8 *) NULL,
        (UINT8 *) NULL
        );
#endif
    }
    break;

#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  case L05_KEY_MEMORY_PROFILE:
    AdvanceCallbackRoutine (This, Action, KEY_MEMINFO_PROFILE, Type, Value, ActionRequest);
    break;
#endif

#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE)
  case KEY_RatioLimit1:
  case KEY_RatioLimit2:
  case KEY_RatioLimit3:
  case KEY_RatioLimit4:
  case KEY_RatioLimit5:
  case KEY_RatioLimit6:
  case KEY_RatioLimit7:
  case KEY_RatioLimit8:
  case KEY_RING_MAX_OC_RATIO_LIMIT:
#endif
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
  case KEY_OC_ENABLE_DEPENDENCY:
#endif
  case KEY_MEMINFO_PROFILE:
    AdvanceCallbackRoutine (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
#endif

  case L05_KEY_OVERCLOCK_BACKUP_SETUP_DATA:
    ZeroMem (&mBackupSetupNvData,    sizeof (mBackupSetupNvData));
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
    ZeroMem (&mBackupPchSetupNvData, sizeof (mBackupPchSetupNvData));
    ZeroMem (&mBackupSaSetupNvData,  sizeof (mBackupSaSetupNvData));
    ZeroMem (&mBackupCpuSetupNvData, sizeof (mBackupCpuSetupNvData));
#endif
#if (L05_CHIPSET_VENDOR_ID == L05_AMD_VENDOR_ID)
    ZeroMem (&mBackupPbsSetupIfrData, sizeof (mBackupPbsSetupIfrData));
    ZeroMem (&mBackupCbsSetupIfrData, sizeof (mBackupCbsSetupIfrData));
    ZeroMem (&mBackupAodSetupIfrData, sizeof (mBackupAodSetupIfrData));
#endif

    CopyMem ((UINT8 *) &mBackupSetupNvData,    (UINT8 *) gSUBrowser->SCBuffer,           sizeof (mBackupSetupNvData));
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
    CopyMem ((UINT8 *) &mBackupPchSetupNvData, (UINT8 *) gRcSUBrowser->PchSUBrowserData, sizeof (mBackupPchSetupNvData));
    CopyMem ((UINT8 *) &mBackupSaSetupNvData,  (UINT8 *) gRcSUBrowser->SaSUBrowserData,  sizeof (mBackupSaSetupNvData));
    CopyMem ((UINT8 *) &mBackupCpuSetupNvData, (UINT8 *) gRcSUBrowser->CpuSUBrowserData, sizeof (mBackupCpuSetupNvData));
#endif

#if (L05_CHIPSET_VENDOR_ID == L05_AMD_VENDOR_ID)
    if (mL05PbsSetupIfrData != NULL) {
      CopyMem ((UINT8 *) &mBackupPbsSetupIfrData, (UINT8 *) mL05PbsSetupIfrData, sizeof (mBackupPbsSetupIfrData));
    }
    if (mL05CbsSetupIfrData != NULL) {
      CopyMem ((UINT8 *) &mBackupCbsSetupIfrData, (UINT8 *) mL05CbsSetupIfrData, sizeof (mBackupCbsSetupIfrData));
    }
    if (mL05AodSetupIfrData != NULL) {
      CopyMem ((UINT8 *) &mBackupAodSetupIfrData, (UINT8 *) mL05AodSetupIfrData, sizeof (mBackupAodSetupIfrData));
    }
#endif

    break;

  case L05_KEY_OVERCLOCK_RESTORE_SETUP_DATA:
    CopyMem ((UINT8 *) gSUBrowser->SCBuffer,           (UINT8 *) &mBackupSetupNvData,    sizeof (mBackupSetupNvData));
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
    CopyMem ((UINT8 *) gRcSUBrowser->PchSUBrowserData, (UINT8 *) &mBackupPchSetupNvData, sizeof (mBackupPchSetupNvData));
    CopyMem ((UINT8 *) gRcSUBrowser->SaSUBrowserData,  (UINT8 *) &mBackupSaSetupNvData,  sizeof (mBackupSaSetupNvData));
    CopyMem ((UINT8 *) gRcSUBrowser->CpuSUBrowserData, (UINT8 *) &mBackupCpuSetupNvData, sizeof (mBackupCpuSetupNvData));
#endif

#if (L05_CHIPSET_VENDOR_ID == L05_AMD_VENDOR_ID)
    if (mL05PbsSetupIfrData != NULL) {
      CopyMem ((UINT8 *) mL05PbsSetupIfrData, (UINT8 *) &mBackupPbsSetupIfrData, sizeof (mBackupPbsSetupIfrData));
    }
    if (mL05CbsSetupIfrData != NULL) {
      CopyMem ((UINT8 *) mL05CbsSetupIfrData, (UINT8 *) &mBackupCbsSetupIfrData, sizeof (mBackupCbsSetupIfrData));
    }
    if (mL05AodSetupIfrData != NULL) {
      CopyMem ((UINT8 *) mL05AodSetupIfrData, (UINT8 *) &mBackupAodSetupIfrData, sizeof (mBackupAodSetupIfrData));
    }
#endif

    break;

  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}

