/** @file
  Provide functions to link external Intel RST formset

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <SetupConfig.h>
#include <SetupUtility.h>

#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HiiLib.h>

#include <Guid/HiiPlatformSetupFormset.h>

/**
  Extract the displayed formset for given HII handle and class guid.

  @param  Handle                            The HII handle.
  @param  SetupClassGuid                    The class guid specifies which form set will be displayed.
  @param  FormSetTitle                      Formset title string.
  @param  FormSetHelp                       Formset help string.

  @retval TRUE                              The formset for given HII handle will be displayed.
  @return FALSE                             The formset for given HII handle will not be displayed.

**/
BOOLEAN
ExtractDisplayedHiiFormFromHiiHandle (
  IN  EFI_HII_DATABASE_PROTOCOL             *HiiDatabase,
  IN  EFI_HII_HANDLE                        Handle,
  IN  EFI_GUID                              *SetupClassGuid,
  OUT EFI_STRING_ID                         *FormSetTitle,
  OUT EFI_STRING_ID                         *FormSetHelp,
  OUT EFI_GUID                              *FormSetGuid
  )
{
  EFI_STATUS                                Status;
  UINTN                                     BufferSize;
  EFI_HII_PACKAGE_LIST_HEADER               *HiiPackageList;
  UINT8                                     *Package;
  UINT8                                     *OpCodeData;
  UINT32                                    Offset;
  UINT32                                    Offset2;
  UINT32                                    PackageListLength;
  EFI_HII_PACKAGE_HEADER                    PackageHeader;
  EFI_GUID                                  *ClassGuid;
  UINT8                                     ClassGuidNum;

  ASSERT (Handle != NULL);
  ASSERT (SetupClassGuid != NULL);
  ASSERT (FormSetTitle != NULL);
  ASSERT (FormSetHelp != NULL);

  *FormSetTitle = 0;
  *FormSetHelp  = 0;
  ClassGuidNum  = 0;
  ClassGuid     = NULL;

  //
  // Get HII PackageList
  //
  BufferSize = 0;
  HiiPackageList = NULL;
  Status = HiiDatabase->ExportPackageLists (HiiDatabase, Handle, &BufferSize, HiiPackageList);
  //
  // Handle is a invalid handle. Check if Handle is corrupted.
  //
  ASSERT (Status != EFI_NOT_FOUND);
  //
  // The return status should always be EFI_BUFFER_TOO_SMALL as input buffer's size is 0.
  //
  ASSERT (Status == EFI_BUFFER_TOO_SMALL);

  HiiPackageList = AllocatePool (BufferSize);
  ASSERT (HiiPackageList != NULL);

  if (HiiPackageList == NULL) {
    return FALSE;
  }

  Status = HiiDatabase->ExportPackageLists (HiiDatabase, Handle, &BufferSize, HiiPackageList);

  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  //
  // Get Form package from this HII package List
  //
  Offset = sizeof (EFI_HII_PACKAGE_LIST_HEADER);
  Offset2 = 0;
  PackageListLength = ReadUnaligned32 (&HiiPackageList->PackageLength);

  if (PackageListLength > BufferSize) {
    PackageListLength = (UINT32) BufferSize;
  }

  while (Offset < PackageListLength) {
    Package = ((UINT8 *) HiiPackageList) + Offset;
    CopyMem (&PackageHeader, Package, sizeof (EFI_HII_PACKAGE_HEADER));

    if (PackageHeader.Type == EFI_HII_PACKAGE_FORMS) {
      //
      // Search FormSet Opcode in this Form Package
      //
      Offset2 = sizeof (EFI_HII_PACKAGE_HEADER);

      while (Offset2 < PackageHeader.Length) {
        OpCodeData = Package + Offset2;

        if (((EFI_IFR_OP_HEADER *) OpCodeData)->OpCode == EFI_IFR_FORM_SET_OP) {
          if (((EFI_IFR_OP_HEADER *) OpCodeData)->Length > OFFSET_OF (EFI_IFR_FORM_SET, Flags)) {
            //
            // Find FormSet OpCode
            //
            ClassGuidNum = (UINT8) (((EFI_IFR_FORM_SET *) OpCodeData)->Flags & 0x3);
            ClassGuid = (EFI_GUID *) (VOID *) (OpCodeData + sizeof (EFI_IFR_FORM_SET));

            while (ClassGuidNum-- > 0) {
              if (CompareGuid (SetupClassGuid, ClassGuid)) {
                CopyMem (FormSetTitle, &((EFI_IFR_FORM_SET *) OpCodeData)->FormSetTitle, sizeof (EFI_STRING_ID));
                CopyMem (FormSetHelp, &((EFI_IFR_FORM_SET *) OpCodeData)->Help, sizeof (EFI_STRING_ID));
                CopyMem (FormSetGuid, &((EFI_IFR_FORM_SET *) OpCodeData)->Guid, sizeof (EFI_GUID));
                FreePool (HiiPackageList);
                return TRUE;
              }

              ClassGuid ++;
            }

          } else {
            CopyMem (FormSetTitle, &((EFI_IFR_FORM_SET *) OpCodeData)->FormSetTitle, sizeof (EFI_STRING_ID));
            CopyMem (FormSetHelp, &((EFI_IFR_FORM_SET *) OpCodeData)->Help, sizeof (EFI_STRING_ID));
            CopyMem (FormSetGuid, &((EFI_IFR_FORM_SET *) OpCodeData)->Guid, sizeof (EFI_GUID));
            FreePool (HiiPackageList);
            return TRUE;
          }
        }

        //
        // Go to next opcode
        //
        Offset2 += ((EFI_IFR_OP_HEADER *) OpCodeData)->Length;
        ASSERT ((((EFI_IFR_OP_HEADER *) OpCodeData)->Length) >= sizeof (EFI_IFR_OP_HEADER));

        if (((EFI_IFR_OP_HEADER *) OpCodeData)->Length < sizeof (EFI_IFR_OP_HEADER)) {
          break;
        }
      }
    }

    //
    // Go to next package
    //
    Offset += PackageHeader.Length;
    ASSERT (PackageHeader.Length != 0);

    if (PackageHeader.Length == 0) {
      break;
    }
  }

  FreePool (HiiPackageList);

  return FALSE;
}

/**
  Provide to Link external Intel RST formset.

  @param  HiiHandle                         The HII Handle for current Menu.

  @retval EFI_SUCCESS                       The operation completed successfully.
  @retval Others                            Error occurred during execution.

**/
EFI_STATUS
LinkIntelRstFormset (
  IN  EFI_HII_HANDLE                        HiiHandle
  )
{
  EFI_STATUS                                Status;
  EFI_HII_DATABASE_PROTOCOL                 *HiiDatabase;
  UINTN                                     Index;
  EFI_STRING                                String;
  EFI_STRING_ID                             Token;
  EFI_STRING_ID                             TokenHelp;
  EFI_HII_HANDLE                            *HiiHandles;
  EFI_STRING_ID                             FormSetTitle;
  EFI_STRING_ID                             FormSetHelp;
  EFI_GUID                                  FormSetGuid;
  VOID                                      *StartOpCodeHandle;
  VOID                                      *EndOpCodeHandle;
  EFI_IFR_GUID_LABEL                        *StartLabel;
  EFI_GUID                                  IntelRstFormSetGuid = FORMSET_ID_GUID_INTEL_RST;

  HiiDatabase       = NULL;
  String            = NULL;
  Token             = 0;
  TokenHelp         = 0;
  HiiHandles        = NULL;
  FormSetTitle      = 0;
  FormSetHelp       = 0;
  StartOpCodeHandle = NULL;
  EndOpCodeHandle   = NULL;
  StartLabel        = NULL;

  Status = gBS->LocateProtocol (&gEfiHiiDatabaseProtocolGuid, NULL, (VOID **) &HiiDatabase);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  StartOpCodeHandle = HiiAllocateOpCodeHandle ();

  if (StartOpCodeHandle == NULL) {
    return EFI_ABORTED;
  }

  StartLabel = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (StartOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));

  if (StartLabel == NULL) {
    return EFI_ABORTED;
  }

  StartLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
  StartLabel->Number       = L05_LINK_INTEL_RST_FORMSET_LABEL;

  HiiHandles = HiiGetHiiHandles (NULL);

  if (HiiHandles == NULL) {
    return EFI_NOT_FOUND;
  }

  //
  // Search for formset of each class type
  //
  for (Index = 0; HiiHandles[Index] != NULL; Index++) {
    if (!ExtractDisplayedHiiFormFromHiiHandle (HiiDatabase, HiiHandles[Index], &gEfiHiiPlatformSetupFormsetGuid, &FormSetTitle, &FormSetHelp, &FormSetGuid)) {
      continue;
    }

    if (CompareGuid (&IntelRstFormSetGuid, &FormSetGuid)) {
      //
      // Intel RST formset is found
      //
      String = HiiGetString (HiiHandles[Index], FormSetTitle, NULL);

      if (String == NULL) {
        Token = HiiSetString (HiiHandle, 0, L"Intel(R) Rapid Storage Technology", NULL);

      } else {
        Token = HiiSetString (HiiHandle, 0, String, NULL);
        FreePool (String);
      }

      String = HiiGetString (HiiHandles[Index], FormSetHelp, NULL);

      if (String != NULL) {
        TokenHelp = HiiSetString (HiiHandle, 0, String, NULL);
        FreePool (String);
      }

      HiiCreateGotoExOpCode (
        StartOpCodeHandle,
        1,
        Token,
        TokenHelp,
        0,
        (EFI_QUESTION_ID) L05_KEY_LINK_INTEL_RST_FORMSET_BASE,
        0,
        &FormSetGuid,
        0
        );
      break;
    }
  }

  HiiUpdateForm (
    HiiHandle,
    NULL,
    L05_STORAGE_FORM_ID,
    StartOpCodeHandle,
    NULL
    );

  HiiFreeOpCodeHandle (StartOpCodeHandle);
  FreePool (HiiHandles);
  return EFI_SUCCESS;
}