/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <PlatformInfo.h>

EFI_STATUS
GetMeVersion (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer
  )
{
  EFI_STATUS                        Status;
  UINTN                             VariableSize;
  STRING_REF                        MeFwVersionString;
  STRING_REF                        MeVersionString;
  UINTN                             Index;
  ME_INFO_SETUP_DATA                MeInfoSetupData;
  ME_SETUP                          MeSetup;
  STEPPING_STRING_DEFINITION          MeFwSkuString[] = {
                                      {IntelMeConsumerFw, L"CONSUMER"},
                                      {IntelMeCorporateFw,   L"CORPORATE"},
                                      {0xFF, L""}
                                      };

  Status = EFI_UNSUPPORTED;

  VariableSize = sizeof(ME_INFO_SETUP_DATA);
  Status = gRT->GetVariable (
                  L"MeInfoSetup",
                  &gMeInfoSetupGuid,
                  NULL,
                  &VariableSize,
                  &MeInfoSetupData
                  );
  if (EFI_ERROR(Status)) {
    return Status;
  }

  VariableSize = sizeof (ME_SETUP);
  Status = gRT->GetVariable (
                  L"MeSetup",
                  &gMeSetupVariableGuid,
                  NULL,
                  &VariableSize,
                  &MeSetup
                  );
  if (EFI_ERROR(Status)) {
    return Status;
  }

  if (!EFI_ERROR (Status)) {
    for (Index = 0; MeFwSkuString[Index].ReversionValue != 0xFF; Index++) {
      if (MeSetup.MeImageType == MeFwSkuString[Index].ReversionValue) {
        break;
      }
    }
    UnicodeSPrint (
      StringBuffer,
      0x100,
      L"%d.%d.%d.%d / %s",
      (UINTN) (UINT16) MeInfoSetupData.MeMajor,
      (UINTN) (UINT16) MeInfoSetupData.MeMinor,
      (UINTN) (UINT16) MeInfoSetupData.MeHotFix,
      (UINTN) (UINT16) MeInfoSetupData.MeBuildNo,
      MeFwSkuString[Index].SteppingString
      );
    } else {
    StringBuffer=HiiGetString (AdvanceHiiHandle, STRING_TOKEN (STR_ME_VERSION), NULL);
  }

  MeVersionString=HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);
  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_ME_FW_VERSION_STRING),
    MainHiiHandle,
    &MeFwVersionString
    );

  HiiCreateTextOpCode (OpCodeHandle,MeFwVersionString, 0, MeVersionString );

  return Status;
}

