/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SetupUtility.h>
#include <Protocol/CpuIo2.h>
#include <Protocol/FormBrowserEx2.h>
#include <Library/PttHeciLib.h>
#include <CpuRegs.h>
#include <Protocol/MemInfo.h>

#include <Library/DxeMeLib.h>
#include <Library/VariableSupportLib.h>
#include <Protocol/H2OFormBrowser.h>
#include <Library/DxeOemSvcChipsetLib.h>
#include <PchResetPlatformSpecific.h>
// #include <SgxSetupData.h>
#include <Guid/H2OCp.h>
#include <Library/H2OCpLib.h>
#include <Library/PchInfoLib.h>
#include <HybridGraphicsDefine.h>
//_Start_L05_SETUP_MENU_
#include <Library/BaseLib.h>
#include <Protocol/L05SecureBoot.h>
#include <Protocol/L05BootOption.h>
#include "L05/Configuration/ConfigurationMenu.h"
#include "L05/SaveRestoreSetupItem.h"
#include "L05ExitDiscardingChanges.h"
//_End_L05_SETUP_MENU_
//_Start_L05_ONE_KEY_RECOVERY_ENABLE_
#include "L05Config.h"
#include <Protocol/NovoRecovery.h>
//_End_L05_ONE_KEY_RECOVERY_ENABLE_
//[-start-210629-FLINT00009-add]//
#include <Library/LfcEcLib.h>
//[-end-210629-FLINT00009-add]//

//[-start-210701-KEBIN00030-modify]//
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#endif
//[-end-210701-KEBIN00030-modify]//
//[-start-220321-QINGLIN0166-add]//
#ifdef S370_SUPPORT
#include <Library/OemSvcLfcPeiGetBoardID.h>
#endif
//[-end-220321-QINGLIN0166-add]//

//_Start_L05_SETUP_MENU_
BOOLEAN                         mL05ResetToSetupMode = FALSE;
BOOLEAN                         mL05RestoreFactoryKeys = FALSE;
BOOLEAN                         mL05EfiDefault = TRUE;
//_End_L05_SETUP_MENU_

//_Start_L05_SETUP_MENU_
EFI_STATUS
L05UpdateBootMenu (
  IN EFI_HII_HANDLE             HiiHandle
  );

EFI_STATUS
EFIAPI
BootCallbackRoutine (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  EFI_BROWSER_ACTION                     Action,
  IN  EFI_QUESTION_ID                        QuestionId,
  IN  UINT8                                  Type,
  IN  EFI_IFR_TYPE_VALUE                     *Value,
  OUT EFI_BROWSER_ACTION_REQUEST             *ActionRequest
  );
//_End_L05_SETUP_MENU_

UINT16                          gSCUSystemHealth;
EFI_EVENT                       gSCUTimerEvent;
BOOLEAN                         mPopLoadDefaultDialog = TRUE;
STATIC H2O_CP_HANDLE            mSendFormAfterCpHandle = NULL;
//_Start_L05_WORKAROUND_HDD_PASSWORDS_UNSYNC_[Kernel]
BOOLEAN                         mIsSaveWithoutExit;
//_End_L05_WORKAROUND_HDD_PASSWORDS_UNSYNC_[Kernel]

UINT32                          mScuRecord = 0;
extern BOOLEAN                  mMeReset;

EFI_STATUS
DiscardSetupDataChange (
  VOID
  );

EFI_STATUS
DiscardSaChange (
  VOID
  );

EFI_STATUS
DiscardMeChange (
  VOID
  );

EFI_STATUS
DiscardCpuChange (
  VOID
  );

EFI_STATUS
DiscardPchChange (
  VOID
  );

EFI_STATUS
DiscardSiChange (
  VOID
  );

EFI_STATUS
DiscardIccChange (
  VOID
  );

EFI_STATUS
DiscardMeStorageChange (
  VOID
  );

EFI_STATUS
LoadCustomRcOption (
  VOID
  );

EFI_STATUS
SaveCustomRcOption (
  VOID
  );


EFI_STATUS
EFIAPI
SendSubmitExitNotify (
  VOID
  )
{
  EFI_STATUS                         Status;
  H2O_DISPLAY_ENGINE_EVT_SUBMIT_EXIT SubmitExitNotify;
  H2O_FORM_BROWSER_PROTOCOL          *FBProtocol;

  Status = gBS->LocateProtocol (
                   &gH2OFormBrowserProtocolGuid,
                   NULL,
                   (VOID **)&FBProtocol
                   );
  if (!EFI_ERROR (Status)){
    ZeroMem (&SubmitExitNotify, sizeof (SubmitExitNotify));

    SubmitExitNotify.Hdr.Size   = sizeof (H2O_DISPLAY_ENGINE_EVT_SUBMIT_EXIT);
    SubmitExitNotify.Hdr.Type   = H2O_DISPLAY_ENGINE_EVT_TYPE_SUBMIT_EXIT;
    SubmitExitNotify.Hdr.Target = H2O_DISPLAY_ENGINE_EVT_TARGET_FORM_BROWSER;

    Status = FBProtocol->Notify (FBProtocol, &SubmitExitNotify.Hdr);
  }

  return Status;
}

//[-start-190701-16990083-add]//
EFI_STATUS
EFIAPI
JumpToFirstQuestion (
  VOID
  )
{
  EFI_STATUS                                  Status;
  H2O_FORM_BROWSER_PROTOCOL                   *FBProtocol;
  H2O_DISPLAY_ENGINE_EVT_SELECT_Q             SelectQNotify;
  H2O_FORM_BROWSER_S                          *Statement;
  UINTN                                       Index;
  H2O_FORM_BROWSER_P                          *CurrentP;
  EFI_QUESTION_ID                             QuestionId;

  Status = gBS->LocateProtocol (&gH2OFormBrowserProtocolGuid, NULL, (VOID **) &FBProtocol);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  QuestionId = 0;
  CurrentP   = FBProtocol->CurrentP;
  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Statement = NULL;
    Status = FBProtocol->GetSInfo (FBProtocol, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);
    if (EFI_ERROR (Status)) {
      continue;
    }

    if (!Statement->Selectable) {
      continue;
    }

    QuestionId = Statement->QuestionId;
    FreePool (Statement);

    if (QuestionId != 0) {
      break;
    }
  }

  ZeroMem (&SelectQNotify, sizeof (H2O_DISPLAY_ENGINE_EVT_SELECT_Q));
  SelectQNotify.Hdr.Size   = sizeof (H2O_DISPLAY_ENGINE_EVT_SELECT_Q);
  SelectQNotify.Hdr.Type   = H2O_DISPLAY_ENGINE_EVT_TYPE_SELECT_Q;
  SelectQNotify.Hdr.Target = H2O_DISPLAY_ENGINE_EVT_TARGET_FORM_BROWSER;
  SelectQNotify.PageId     = FBProtocol->CurrentP->PageId;
  SelectQNotify.QuestionId = QuestionId;
  SelectQNotify.IfrOpCode  = 0;

  return FBProtocol->Notify (FBProtocol, &SelectQNotify.Hdr);
}
//[-end-190701-16990083-add]//

STATIC
EFI_STATUS
FullReset (
  IN BOOLEAN                 GlobalReset
  )
{
  PCH_RESET_DATA             ResetData;

  if (GlobalReset) {
    //
    // Because gRT->ResetSystem() producer supports PCH global reset, call it directly.
    //
    CopyMem (
      &ResetData.Guid,
      &gPchGlobalResetGuid,
      sizeof (EFI_GUID)
      );
    StrCpyS (
      ResetData.Description,
      PCH_RESET_DATA_STRING_MAX_LENGTH,
      PCH_PLATFORM_SPECIFIC_RESET_STRING
      );
    gRT->ResetSystem (EfiResetPlatformSpecific, EFI_SUCCESS, sizeof (ResetData), (VOID *) &ResetData);

    //
    // Should NOT reach here.
    //
    CpuDeadLoop();
  }

  return EFI_SUCCESS;
}

/**
 Do full reset when exit setup utility at send form after checkpoint.

 @param[in] Event                           The Event this notify function registered to.
 @param[in] Handle                          The handle associated with a previously registered checkpoint handler.
**/
STATIC
VOID
EFIAPI
BdsCpSendFormAfterHandler (
  IN EFI_EVENT                              Event,
  IN H2O_CP_HANDLE                          Handle
  )
{
  EFI_STATUS                                Status;

  Status = FullReset (TRUE);
  ASSERT_EFI_ERROR (Status);

  H2OCpUnregisterHandler (Handle);
  mSendFormAfterCpHandle = NULL;
}

/**
  Execute form browser action by EDK2 form browser extension 2 protocol

  @param[in] Action     Execute the request action.
  @param[in] DefaultId  The default Id info when need to load default value.

  @retval EFI_SUCCESS   Successfully execute form browser action
  @retval Other         Locate protocol fail
**/
EFI_STATUS
ExecuteFormBrowserAction (
  IN UINT32                                    Action,
  IN UINT16                                    DefaultId
  )
{
  EFI_STATUS                                   Status;
  EDKII_FORM_BROWSER_EXTENSION2_PROTOCOL       *FormBrowserEx2;

  Status = gBS->LocateProtocol (
                  &gEdkiiFormBrowserEx2ProtocolGuid,
                  NULL,
                  (VOID **)&FormBrowserEx2
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  return FormBrowserEx2->ExecuteAction (Action, DefaultId);
}
EFI_STATUS
HotKeyCallBackByAction (
  IN CONST  EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  )
{
  EFI_STATUS                     Status;

  Status = EFI_UNSUPPORTED;

  switch (Action) {

  case EFI_BROWSER_ACTION_CHANGING:
    return EFI_SUCCESS;

  case EFI_BROWSER_ACTION_DEFAULT_MANUFACTURING:
    if (QuestionId == KEY_SCAN_F9) {
      mPopLoadDefaultDialog = FALSE;
      Status = HotKeyCallBack (
                 This,
                 EFI_BROWSER_ACTION_CHANGED,
                 QuestionId,
                 Type,
                 Value,
                 ActionRequest
                 );
      mPopLoadDefaultDialog = TRUE;

//_Start_L05_SETUP_MENU_
{
      EFI_IFR_TYPE_VALUE                BootTypeValue;

      //
      // Save Setup variable before call BootCallbackRoutine with Action KEY_BOOT_MODE_TYPE
      // to avoid default data is overriden by BootCallbackRoutine.
      //
      Status = BootCallbackRoutine(
                 This,
                 EFI_BROWSER_ACTION_FORM_OPEN,
                 0,
                 0,
                 NULL,
                 ActionRequest
                 );

      //
      // When load default switch BootType, there is no corresponding behavior KEY_BOOT_MODE_TYPE.
      // Call it to sync relative setup item.
      //
      BootTypeValue.u8 = ((CHIPSET_CONFIGURATION *) gSUBrowser->SCBuffer)->BootType;
      Status = BootCallbackRoutine(
                 This,
                 EFI_BROWSER_ACTION_CHANGED,
                 KEY_BOOT_MODE_TYPE,
                 0,
                 &BootTypeValue,
                 ActionRequest
                 );
}
//_End_L05_SETUP_MENU_

      BrowserRefreshFormSet ();
    }
    //
    // avoid GetQuestionDefault execute ExtractConfig, so always return success.
    //
    return EFI_SUCCESS;

  default:
    break;
  }

  return Status;
}

/**
 Proccess HotKey function

 @param [in]   This
 @param [in]   HiiHandle
 @param [in]   Action
 @param [in]   QuestionId
 @param [in]   Type
 @param [in]   Value
 @param [out]  ActionRequest


**/
EFI_STATUS
HotKeyCallBack (
  IN CONST  EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  )
{
  CHIPSET_CONFIGURATION                     *MyIfrNVData;
  CHAR16                                    *StringPtr;
  SETUP_UTILITY_CONFIGURATION               *SUCInfo;
  EFI_INPUT_KEY                             Key;
  EFI_STATUS                                Status;
  UINT16                                    *BootOrderList;
  UINTN                                     BufferSize;
  UINT8                                     CmosPlatformSetting;
  CHIPSET_CONFIGURATION                     *NVData;
  UINTN                                     VariableSize;
  EFI_GUID                                  SetupVariableGuidId = SYSTEM_CONFIGURATION_GUID;
  EFI_HII_HANDLE                            HiiHandle;
  UINTN                                     FullResetSize = 0;
  UINT8                                     *FullResetValue;
  UINT32                                    MeUnconfigOnRtc;
  SETUP_DATA                                *MySetupDataIfrNVData;
  SETUP_DATA                                *SetupDataNvDataInRom;
  SA_SETUP                                  *MySaIfrNVData;
  SA_SETUP                                  *SaNvDataInRom;
  ME_SETUP                                  *MyMeIfrNVData;
  ME_SETUP                                  *MeNvDataInRom;
  CPU_SETUP                                 *MyCpuIfrNVData;
  CPU_SETUP                                 *CpuNvDataInRom;
  PCH_SETUP                                 *MyPchIfrNVData;
  PCH_SETUP                                 *PchNvDataInRom;
  SI_SETUP                                  *MySiIfrNVData;
  SI_SETUP                                  *SiNvDataInRom;
  ME_SETUP_STORAGE                          *MyMeStorageIfrNVData;
  ME_SETUP_STORAGE                          *MeStorageNvDataInRom;
  EFI_STATUS                                GetSetupStatus;
  EFI_STATUS                                GetSetupDataStatus;
  EFI_STATUS                                GetSiSetupStatus;
  EFI_STATUS                                GetSaSetupStatus;
  EFI_STATUS                                GetMeSetupStatus;
  EFI_STATUS                                GetCpuSetupStatus;
  EFI_STATUS                                GetPchSetupStatus;
  EFI_STATUS                                GetMeSetupStorageStatus;
//_Start_L05_SETUP_MENU_
  EFI_L05_BOOT_OPTION_PROTOCOL              *L05BootOrderPtr;
  SYSTEM_CONFIGURATION                      *L05SetupData;
  UINTN                                     L05VariableSize;
  UINT8                                     BootType;
  CHAR16                                    *Buffer;
  H2O_FORM_BROWSER_PROTOCOL                 *FBProtocol;
  EFI_GUID                                  IntelRstFormSetGuid = FORMSET_ID_GUID_INTEL_RST;
//_End_L05_SETUP_MENU_

  if (Action != EFI_BROWSER_ACTION_CHANGED) {
    return HotKeyCallBackByAction (This, Action, QuestionId, Type, Value, ActionRequest);
  }

  BufferSize           = 0;
  VariableSize         = 0;
  BootOrderList        = NULL;
  SUCInfo              = gSUBrowser->SUCInfo;
  MyIfrNVData          = (CHIPSET_CONFIGURATION *) gSUBrowser->SCBuffer;
  CmosPlatformSetting  = 0;
  HiiHandle            = mDriverHiiHandle;
  *ActionRequest       = EFI_BROWSER_ACTION_REQUEST_NONE;
  FullResetValue       = NULL;
  MeUnconfigOnRtc      = 0;
  MySetupDataIfrNVData = (SETUP_DATA *)gRcSUBrowser->SetupDataSUBrowserData;
  MySaIfrNVData        = (SA_SETUP *)gRcSUBrowser->SaSUBrowserData;
  MyMeIfrNVData        = (ME_SETUP *)gRcSUBrowser->MeSUBrowserData;
  MyCpuIfrNVData       = (CPU_SETUP *)gRcSUBrowser->CpuSUBrowserData;
  MyPchIfrNVData       = (PCH_SETUP *)gRcSUBrowser->PchSUBrowserData;
  MySiIfrNVData        = (SI_SETUP *)gRcSUBrowser->SiSUBrowserData;
  MyMeStorageIfrNVData = (ME_SETUP_STORAGE *)gRcSUBrowser->MeStorageSUBrowserData;
//_Start_L05_SETUP_MENU_
  L05BootOrderPtr      = NULL;
  L05SetupData         = NULL;
  L05VariableSize      = 0;
  BootType             = EFI_BOOT_TYPE;
  Buffer               = NULL;
  FBProtocol           = NULL;
//_End_L05_SETUP_MENU_

//_Start_L05_SETUP_MENU_
  Status = gBS->LocateProtocol (
                  &gH2OFormBrowserProtocolGuid,
                  NULL,
                  (VOID **) &FBProtocol
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }
//_End_L05_SETUP_MENU_

  NVData = GetVariableAndSize (
             L"Setup",
             &SetupVariableGuidId,
             &VariableSize
             );
  ASSERT (NVData != NULL);
  if (NVData == NULL) {
    return EFI_NOT_FOUND;
  }

  {
    //===========================//
    // Get SETUP DATA from NV    //
    //===========================//
    SetupDataNvDataInRom = GetVariableAndSize (
                             PLATFORM_SETUP_VARIABLE_NAME,
                             &gSetupVariableGuid,
                             &VariableSize
                             );
    ASSERT (SetupDataNvDataInRom != NULL);
    if (SetupDataNvDataInRom == NULL) {
      return EFI_NOT_FOUND;
    }
    //===========================//
    // Get SA setup data from NV //
    //===========================//
    SaNvDataInRom = GetVariableAndSize (
                      SA_SETUP_VARIABLE_NAME,
                      &gSaSetupVariableGuid,
                      &VariableSize
                      );
    ASSERT (SaNvDataInRom != NULL);
    if (SaNvDataInRom == NULL) {
      return EFI_NOT_FOUND;
    }
    //===========================//
    // Get ME setup data from NV //
    //===========================//
    MeNvDataInRom = GetVariableAndSize (
                      ME_SETUP_VARIABLE_NAME,
                      &gMeSetupVariableGuid,
                      &VariableSize
                      );
    ASSERT (MeNvDataInRom != NULL);
    if (MeNvDataInRom == NULL) {
      return EFI_NOT_FOUND;
    }
    //===========================//
    // Get CPU setup data from NV //
    //===========================//
    CpuNvDataInRom = GetVariableAndSize (
                       CPU_SETUP_VARIABLE_NAME,
                       &gCpuSetupVariableGuid,
                       &VariableSize
                       );
    ASSERT (CpuNvDataInRom != NULL);
    if (CpuNvDataInRom == NULL) {
      return EFI_NOT_FOUND;
    }
    //===========================//
    // Get PCH setup data from NV //
    //===========================//
    PchNvDataInRom = GetVariableAndSize (
                       PCH_SETUP_VARIABLE_NAME,
                       &gPchSetupVariableGuid,
                       &VariableSize
                       );
    ASSERT (PchNvDataInRom != NULL);
    if (PchNvDataInRom == NULL) {
      return EFI_NOT_FOUND;
    }
    //===========================//
    // Get SI setup data from NV //
    //===========================//
    SiNvDataInRom = GetVariableAndSize (
                       SI_SETUP_VARIABLE_NAME,
                       &gSiSetupVariableGuid,
                       &VariableSize
                       );
    ASSERT (SiNvDataInRom != NULL);
    if (SiNvDataInRom == NULL) {
      return EFI_NOT_FOUND;
    }
    //===================================//
    // Get ME setup storage data from NV //
    //===================================//
    MeStorageNvDataInRom = GetVariableAndSize (
                             ME_SETUP_STORAGE_VARIABLE_NAME,
                             &gMeSetupVariableGuid,
                             &VariableSize
                             );
    ASSERT (MeStorageNvDataInRom != NULL);
    if (MeStorageNvDataInRom == NULL) {
      return EFI_NOT_FOUND;
    }
  }

  if ((SetupDataNvDataInRom != NULL) && (NVData != NULL) && (SaNvDataInRom != NULL) && (MeNvDataInRom != NULL) &&
      (CpuNvDataInRom != NULL) && (PchNvDataInRom != NULL) && (MeStorageNvDataInRom != NULL) && (SiNvDataInRom != NULL)) {
    if ((MyIfrNVData->BootType                    != NVData->BootType                        ) ||
        (MyIfrNVData->TpmDevice                   != NVData->TpmDevice                       ) ||
        (MySetupDataIfrNVData->EnablePcieTunnelingOverUsb4     != SetupDataNvDataInRom->EnablePcieTunnelingOverUsb4 ) ||
        (MySetupDataIfrNVData->EnableCrashLog                   != SetupDataNvDataInRom->EnableCrashLog               ) ||
        (MySetupDataIfrNVData->CrashLogOnAllReset               != SetupDataNvDataInRom->CrashLogOnAllReset)) {
      if (!mFullResetFlag) {
        mFullResetFlag = 1;
        Status = gRT->GetVariable (
                        L"FullReset",
                        &gEfiGenericVariableGuid,
                        NULL,
                        &FullResetSize,
                        NULL
                        );

        if (Status == EFI_BUFFER_TOO_SMALL) {
          FullResetValue = AllocateZeroPool (FullResetSize);
          ASSERT (FullResetValue != NULL);

          if (FullResetValue == NULL) {
            return EFI_OUT_OF_RESOURCES;
          } else {
            Status = gRT->GetVariable (
                            L"FullReset",
                            &gEfiGenericVariableGuid,
                            NULL,
                            &FullResetSize,
                            FullResetValue
                            );

            if (!EFI_ERROR(Status)) {
              *FullResetValue += mFullResetFlag;

              Status = gRT->SetVariable (
                                L"FullReset",
                                &gEfiGenericVariableGuid,
                                EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                                sizeof (UINT8),
                                FullResetValue
                                );
              if (EFI_ERROR (Status)) {
                return Status;
              }
            }
          }
        }
      }
    } else {
       mFullResetFlag = 0;
    }

    gBS->FreePool (NVData);
    gBS->FreePool (SetupDataNvDataInRom);
    gBS->FreePool (SaNvDataInRom);
    gBS->FreePool (MeNvDataInRom);
    gBS->FreePool (CpuNvDataInRom);
    gBS->FreePool (PchNvDataInRom);
    gBS->FreePool (MeStorageNvDataInRom);
    gBS->FreePool (SiNvDataInRom);
  }

//_Start_L05_WORKAROUND_
  Status = EFI_SUCCESS;
//_End_L05_WORKAROUND_

  switch (QuestionId) {
  //==============//
  // KEY_SCAN_ESC //
  //==============//
  case KEY_SCAN_ESC :
    //
    // Discard setup and exit
    //

//_Start_L05_SETUP_MENU_
    //
    // [Lenovo BIOS Setup Design Guide V2.4]
    //   BIOS Setup UI Definition - Exit Discarding Changes
    //     If no any change, don't show warning box, exit and continue to boot;
    //     If changed, show warning box, behavior as:
    //     Yes: Save and exit (reboot).
    //     No:  Discard changes continue to boot.
    //
    StringPtr = NULL;
    Key.UnicodeChar = L'N';
    if ((PcdGetBool (PcdL05SecureSuiteFlag) == FALSE) &&
        !(CompareGuid (&FBProtocol->CurrentQ->FormsetGuid, &IntelRstFormSetGuid)) &&
        (L05IsSetupItemChanged (MyIfrNVData) || L05IsBootOrderChanged () || L05IsPasswordChanged ())) {
//_End_L05_SETUP_MENU_

    StringPtr = HiiGetString (
                  HiiHandle,
//_Start_L05_SETUP_MENU_
//                  STRING_TOKEN (STR_EXIT_DISCARDING_CHANGES_STRING),
                  STRING_TOKEN (L05_STR_EXIT_DISCARDING_CHANGES_DIALOG),
//_End_L05_SETUP_MENU_
                  NULL
                  );
    gSUBrowser->H2ODialog->ConfirmDialog (
//_Start_L05_SETUP_MENU_
//                             0,
                             PcdGetBool (PcdL05GamingSetupExitDialogFlag) ? L05GamingExitDiscardDlgYesNoCancel : L05ExitDiscardWarningDlgYesNo,
//_End_L05_SETUP_MENU_
                             FALSE,
                             0,
                             NULL,
                             &Key,
                             StringPtr
                             );

//_Start_L05_SETUP_MENU_
    }
//_End_L05_SETUP_MENU_

//_Start_L05_SETUP_MENU_
    PcdSetBoolS (PcdL05GamingSetupExitDialogFlag, FALSE);
    if (CompareGuid (&FBProtocol->CurrentQ->FormsetGuid, &IntelRstFormSetGuid)) {
      //
      // In this case, the behavior of ESC should be to return to the previous page instead of leaving SCU
      //
      Key.UnicodeChar = CHAR_NULL;
    }

//    if (Key.UnicodeChar == CHAR_CARRIAGE_RETURN) {
    if (Key.UnicodeChar == L'N') {
      //
      // [Lenovo BIOS Setup Design Guide V2.4]
      //   BIOS Setup UI Definition - Exit Discarding Changes
      //     No:  Discard changes continue to boot.
      //
      if ( mFullResetFlag == 2 ) {
        Status = FullReset (TRUE);
        ASSERT_EFI_ERROR (Status);
      }
      CheckLanguage ();
      *ActionRequest = EFI_BROWSER_ACTION_REQUEST_EXIT;
      PcdSetBoolS (PcdL05SetupDiscardAndExitFlag, TRUE);

    } else if (Key.UnicodeChar == L'Y') {
      //
      // [Lenovo BIOS Setup Design Guide V2.4]
      //   BIOS Setup UI Definition - Exit Discarding Changes
      //     Yes: Save and exit (reboot).
      //
      GenericRouteConfig ((EFI_HII_CONFIG_ACCESS_PROTOCOL *) This, NULL, NULL);

      //
      // set the request action to save all value and then exit
      //
      gBS->RaiseTPL (TPL_NOTIFY);
      gRT->ResetSystem (EfiResetCold, EFI_SUCCESS, 0, NULL);
    }
//_End_L05_SETUP_MENU_

    gBS->FreePool (StringPtr);
    break;
  //=============//
  // KEY_SCAN_F9 //
  //=============//
  case KEY_SCAN_F9 :
    //
    // Load Optimal
    //
    // If user Accesslevel = USER_PASSWORD_VIEW_ONLY(0x02) or USER_PASSWORD_LIMITED(0x03)
    // and use user password into SCU,the user don't use load optimal function.
    if ((MyIfrNVData->SetUserPass == TRUE) &&
        ((MyIfrNVData->UserAccessLevel == 2) ||
        (MyIfrNVData->UserAccessLevel == 3))) {
      return EFI_ABORTED;
    }

    EventTimerControl(0);


    //
    // Load optimal setup.
    //
    Key.UnicodeChar = CHAR_CARRIAGE_RETURN;
    if (mPopLoadDefaultDialog) {
      StringPtr = HiiGetString (
                    HiiHandle,
//_Start_L05_SETUP_MENU_
//                    STRING_TOKEN (STR_LOAD_OPTIMAL_DEFAULTS_STRING),
                    STRING_TOKEN (L05_STR_LOAD_DEFAULTS_DIALOG),
//_End_L05_SETUP_MENU_
                    NULL
                    );
      gSUBrowser->H2ODialog->ConfirmDialog (
                               0,
                               FALSE,
                               0,
                               NULL,
                               &Key,
                               StringPtr
                               );
      gBS->FreePool (StringPtr);
      if (Key.UnicodeChar != CHAR_CARRIAGE_RETURN) {
        EventTimerControl(TIMEOUT_OF_EVENT);
        return EFI_UNSUPPORTED;
      }

      return ExecuteFormBrowserAction (BROWSER_ACTION_DEFAULT, EFI_HII_DEFAULT_CLASS_STANDARD);
    }
    if (Key.UnicodeChar == CHAR_CARRIAGE_RETURN) {
      mScuRecord |= SCU_ACTION_LOAD_DEFAULT;

//_Start_L05_SETUP_MENU_
      //
      // According to "Lenovo BIOS Setup Design Guide V2.7" -
      // The "Language" cannot be changed back to default, it only allows user to manually change.
      //
#if 0
      Status = gRT->SetVariable (
                      L"PlatformLang",
                      &gEfiGlobalVariableGuid,
                      EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                      AsciiStrSize ((CHAR8 *) PcdGetPtr (PcdUefiVariableDefaultPlatformLang)),
                      (VOID *) PcdGetPtr (PcdUefiVariableDefaultPlatformLang)
                      );
      if (!EFI_ERROR (Status)) {
        GetLangIndex ((CHAR8 *) PcdGetPtr (PcdUefiVariableDefaultPlatformLang), &MyIfrNVData->Language);
      }
#endif
//_End_L05_SETUP_MENU_

      BrowserRefreshFormSet ();

      //
      //Remove this condition, because no loneger use Changelanguage Flag to determine chagelanguage
      //
      UpdatePasswordState (gSUBrowser->SUCInfo->MapTable[SecurityHiiHandle].HiiHandle);

      BootOrderList = GetVariableAndSize (
                        L"BootOrder",
                        &gEfiGlobalVariableGuid,
                        &BufferSize
                        );

      if (BufferSize != 0) {
         gBS->FreePool (SUCInfo->BootOrder);
         SUCInfo->BootOrder = BootOrderList;
         SUCInfo->AdvBootDeviceNum  = (UINT16) (BufferSize / sizeof(UINT16));

//_Start_L05_SETUP_MENU_
         gBS->LocateProtocol (&gEfiL05BootOptionProtocolGuid, NULL, &L05BootOrderPtr);
//_End_L05_SETUP_MENU_

#ifdef L05_ONE_KEY_RECOVERY_ENABLE
        if (L05BootOrderPtr != NULL) {
          L05BootOrderPtr->UpdateBootOrderForNovoRecovery (BootOrderList, &SUCInfo->AdvBootDeviceNum, TRUE);
        }
#endif
      }

      MyIfrNVData->BootMenuType         = NORMAL_MENU;
      MyIfrNVData->BootNormalPriority   = EFI_FIRST;
      MyIfrNVData->LegacyNormalMenuType = NORMAL_MENU;

//_Start_L05_SETUP_MENU_
      //
      // Use Setup variable to check which type of Boot order need to be sorted.
      //
      L05SetupData = GetVariableAndSize (
                       L"Setup",
                       &mFormSetGuid,
                       &L05VariableSize
                       );

      if (L05SetupData != NULL) {
        BootType = L05SetupData->BootType;
        gBS->FreePool (L05SetupData);
      }

      if (L05BootOrderPtr != NULL) {
        //
        // BUGBUG: This will only load default for UEFI Boot Type if the Setting of Setup Menu is Hybrid mode by End user.
        //
        L05BootOrderPtr->SortBootOrderByDefault (
                           SUCInfo->BootOrder,
                           SUCInfo->AdvBootDeviceNum,
                           BootType
                           );
      }
//_End_L05_SETUP_MENU_

      //
      //  reset the flag of showing from.
      //
//_Start_L05_SETUP_MENU_
      L05SaveRestoreSetupItem (
        FALSE,
        MyIfrNVData,
        MySetupDataIfrNVData,
        MySaIfrNVData,
        MyMeIfrNVData,
        MyCpuIfrNVData,
        MyPchIfrNVData,
        MyMeStorageIfrNVData
        );
//_End_L05_SETUP_MENU_

      DefaultSetup (MyIfrNVData);
      DefaultSetupData (&MySetupDataIfrNVData, EfiACPIMemoryNVS);
      DefaultSaSetup (&MySaIfrNVData, EfiACPIMemoryNVS);
      DefaultMeSetup (&MyMeIfrNVData, EfiACPIMemoryNVS);
      DefaultCpuSetup (&MyCpuIfrNVData, EfiACPIMemoryNVS);
      DefaultPchSetup (&MyPchIfrNVData, EfiACPIMemoryNVS);
      DefaultSiSetup (&MySiIfrNVData, EfiACPIMemoryNVS);
//[-start-201127-IB17510127-add]//
      DefaultMeSetupStorage (&MyMeStorageIfrNVData, EfiACPIMemoryNVS);
//[-end-201127-IB17510127-add]//
      DefaultIccSetup ();
      //SetupRuntimeDetermination (MyIfrNVData, MySaIfrNVData, MyMeIfrNVData, MyCpuIfrNVData, MyPchIfrNVData, MyMeStorageIfrNVData);
      GetSetupStatus = GetSetupDataStatus = GetSiSetupStatus =GetSaSetupStatus = GetMeSetupStatus = GetCpuSetupStatus = GetPchSetupStatus = GetMeSetupStorageStatus = EFI_MEDIA_CHANGED;
      SetupRuntimeDetermination (
        MyIfrNVData, GetSetupStatus,
        MySaIfrNVData, GetSaSetupStatus,
        MyMeIfrNVData, GetMeSetupStatus,
        MyCpuIfrNVData, GetCpuSetupStatus,
        MyPchIfrNVData, GetPchSetupStatus,
        MyMeStorageIfrNVData, GetMeSetupStorageStatus,
        MySetupDataIfrNVData, GetSetupDataStatus,
        MySiIfrNVData, GetSiSetupStatus
        );

//_Start_L05_SETUP_MENU_
      L05SaveRestoreSetupItem (
        TRUE,
        MyIfrNVData,
        MySetupDataIfrNVData,
        MySaIfrNVData,
        MyMeIfrNVData,
        MyCpuIfrNVData,
        MyPchIfrNVData,
        MyMeStorageIfrNVData
        );
//[-start-211229-YUNLEI0155-add]//
#if defined(C770_SUPPORT) 
       L05Default14WifiSarTable(MySetupDataIfrNVData);
#endif
//[-end-211229-YUNLEI0155-add]//
//[-start-220321-QINGLIN0166-add]//
#if defined(S370_SUPPORT)
      {
        UINT8    Project_Type = 0;
        OemSvcLfcGetBoardID (PROJECT_ID, &Project_Type);
        if (Project_Type == PROJECT_ID_S170) { //S170
          MySetupDataIfrNVData->TcssUcmDevice = 0;
        }
      }
#endif
//[-end-220321-QINGLIN0166-add]//
      L05DefaultSetup ((UINT8 *) MyIfrNVData, (UINT8 *) MyPchIfrNVData, (UINT8 *) MyCpuIfrNVData, (UINT8 *) MySaIfrNVData);
//_End_L05_SETUP_MENU_

      UpdateHDCConfigure (
        gSUBrowser->SUCInfo->MapTable[AdvanceHiiHandle].HiiHandle,
        MyIfrNVData
        );
      UpdateStringToken ((KERNEL_CONFIGURATION *) MyIfrNVData);

      SetupUtilityLibLoadDefault ();

      SaNvDataInRom = GetVariableAndSize (
                        SA_SETUP_VARIABLE_NAME,
                        &gSaSetupVariableGuid,
                        &VariableSize
                        );
      ASSERT (SaNvDataInRom != NULL);
      if (SaNvDataInRom == NULL) {
        return EFI_OUT_OF_RESOURCES;
      }

      if ((MySaIfrNVData->IgdDvmt50PreAlloc != SaNvDataInRom->IgdDvmt50PreAlloc ) ||
          (MySaIfrNVData->ApertureSize != SaNvDataInRom->ApertureSize)) {
        CmosPlatformSetting = ReadExtCmos8 ( R_XCMOS_INDEX, R_XCMOS_DATA, PlatformSettingFlag );
        CmosPlatformSetting = CmosPlatformSetting | B_SETTING_MEM_REFRESH_FLAG;
        WriteExtCmos8 ( R_XCMOS_INDEX, R_XCMOS_DATA, PlatformSettingFlag, CmosPlatformSetting );
      }

      FreePool (SaNvDataInRom);
//[-start-190701-16990083-add]//
	  JumpToFirstQuestion ();
//[-end-190701-16990083-add]//
    }
    EventTimerControl(TIMEOUT_OF_EVENT);
    break;
  //==============//
  // KEY_SCAN_F10 //
  //==============//
  case KEY_SCAN_F10 :
//_Start_L05_SETUP_MENU_
//
// When login SCU as User, the "Save and Exit" function should still work.
//
//    //
//    // If user Accesslevel = USER_PASSWORD_VIEW_ONLY(0x02) or USER_PASSWORD_LIMITED(0x03)
//    // and use user password into SCU,the user don't use load optimal function.
//    //
//    if ((MyIfrNVData->SetUserPass == TRUE) &&
//        (MyIfrNVData->UserAccessLevel == 2)) {
//      return EFI_ABORTED;
//    }
//_End_L05_SETUP_MENU_

    EventTimerControl (0);
//[-start-211021-GEORGE0015-modify]//
//[-start-210701-KEBIN00030-modify]//
#ifdef LCFC_SUPPORT
    if (((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05SystemPerformanceMode == 1) {
      Status = LfcEcLibEcRamWrite (0x1D, 0x01); // Performance Mode
      Status = LfcEcLibEcRamWrite (0x20, 0x01); // Performance Mode
    } else if (((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05SystemPerformanceMode == 2) {
      Status = LfcEcLibEcRamWrite (0x1D, 0x02); // Battery Mode
      Status = LfcEcLibEcRamWrite (0x20, 0x02); // Battery Mode
    } else {
      Status = LfcEcLibEcRamWrite (0x1D, 0x00); // Intelligent Mode
      Status = LfcEcLibEcRamWrite (0x20, 0x00); // Intelligent Mode
    }

#endif
//[-end-210701-KEBIN00030-modify]//
//[-end-211021-GEORGE0015-modify]//

    SendSubmitExitNotify ();
    break;

  //====================//
  // KEY_BOOT_MODE_TYPE //
  //====================//
  case KEY_BOOT_MODE_TYPE:
    //
    // For IGD display selection:
    // if user switch mode, suppress the display selection in this boot.
    // due to the display selection is decided by LegacyBiosProtocol.
    //
    MySaIfrNVData->IgdBootType = SCU_IGD_BOOT_TYPE_VBIOS_DEFAULT;
    MyIfrNVData->IGDBootTypeSecondary = SCU_IGD_BOOT_TYPE_DISABLE;
    ClearFormDataFromLabel (HiiHandle, IGFX_FORM_ID, IGD_DISPLAY_SELECTION_START_LABEL);
    //
    // For Plug-in display selection:
    //
    ClearFormDataFromLabel (HiiHandle, IGFX_FORM_ID, PLUG_IN_DISPLAY_SELECTION_START_LABEL);

//_Start_L05_SETUP_MENU_
    //
    //  By L05 Minimum BIOS Spec v1.26 10 Secure boot and Boot mode
    //  When boot mode set to legacy, disable Secure Boot.
    //
    if (MyIfrNVData->BootType == 0) {   //  0:Dual Boot, 2:EFI Boot
      ((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05SecureBoot = 0; //  0:Disable,   1:Enable
    }
//_End_L05_SETUP_MENU_

    //
    // For dual vga controller supported:
    // It only supported the Uefi mode.
    //
    if (Value->u8 != EFI_BOOT_TYPE && MyIfrNVData->UefiDualVgaControllers == DUAL_VGA_CONTROLLER_ENABLE) {
      MyIfrNVData->UefiDualVgaControllers = DUAL_VGA_CONTROLLER_DISABLE;
      MySaIfrNVData->PrimaryDisplay = DISPLAY_MODE_AUTO;
      ClearFormDataFromLabel (HiiHandle, IGFX_FORM_ID, DUAL_VGA_SUPPORT_START_LABEL);
    }
    if (Value->u8 != EFI_BOOT_TYPE) {
      MySaIfrNVData->EnableAbove4GBMmio = 0;
    }

    switch (Value->u8) {

      case EFI_BOOT_TYPE:
        break;

      case LEGACY_BOOT_TYPE:
        MySetupDataIfrNVData->LowPowerS0Idle = 0;
        break;

      case DUAL_BOOT_TYPE:
        MySetupDataIfrNVData->LowPowerS0Idle = 0;
        break;
    }
    break;
  //=======================//
  // KEY_SAVE_WITHOUT_EXIT //
  //=======================//
  case KEY_SAVE_WITHOUT_EXIT:
    StringPtr = HiiGetString (
                  HiiHandle,
//_Start_L05_SETUP_MENU_
//                  STRING_TOKEN (STR_SAVE_CHANGE_WITHOUT_EXIT_STRING),
                  STRING_TOKEN (L05_STR_SAVE_CHANGE_DIALOG),
//_End_L05_SETUP_MENU_
                  NULL
                  );
    gSUBrowser->H2ODialog->ConfirmDialog (
                             0,
                             FALSE,
                             0,
                             NULL,
                             &Key,
                             StringPtr
                             );
    gBS->FreePool (StringPtr);
    if (Key.UnicodeChar == CHAR_CARRIAGE_RETURN) {
//_Start_L05_WORKAROUND_HDD_PASSWORDS_UNSYNC_[Kernel]
      mIsSaveWithoutExit = TRUE;
//_End_L05_WORKAROUND_HDD_PASSWORDS_UNSYNC_[Kernel]
      if (mFullResetFlag == 1) {
        mFullResetFlag = 2;
      }
      ExecuteFormBrowserAction (BROWSER_ACTION_SUBMIT, 0);

//_Start_L05_SETUP_MENU_
      //
      // Record changed question prompt when save without exit.
      //
      L05RecordChangedQuestionPrompt ();
//_End_L05_SETUP_MENU_
    }
    break;
  //====================//
  // KEY_DISCARD_CHANGE //
  //====================//
  case KEY_DISCARD_CHANGE:
    //
    // discard setup change.
    //
    StringPtr = HiiGetString (
                  HiiHandle,
//_Start_L05_SETUP_MENU_
//                  STRING_TOKEN (STR_DISCARD_CHANGES_STRING),
                  STRING_TOKEN (L05_STR_DISCARD_CHANGES_DIALOG),
//_End_L05_SETUP_MENU_
                  NULL
                  );
    gSUBrowser->H2ODialog->ConfirmDialog (
                             0,
                             FALSE,
                             0,
                             NULL,
                             &Key,
                             StringPtr
                             );
    if (Key.UnicodeChar == CHAR_CARRIAGE_RETURN) {
      mScuRecord &= (~SCU_ACTION_LOAD_DEFAULT);
      BrowserRefreshFormSet ();
      ExecuteFormBrowserAction (BROWSER_ACTION_DISCARD, 0);
      if (mFullResetFlag == 1) {
        mFullResetFlag = 0;
      }
      Status = DiscardChange (This);
      Status = DiscardSetupDataChange ();
      Status = DiscardSaChange ();
      Status = DiscardMeChange ();
      Status = DiscardCpuChange ();
      Status = DiscardPchChange ();
      Status = DiscardSiChange ();
      Status = DiscardIccChange ();
      Status = DiscardMeStorageChange ();
//_Start_L05_SETUP_MENU_
      Status = UpdateStorageDeviceModelName (gSUBrowser->SUCInfo->MapTable[InformationHiiHandle].HiiHandle);
      UpdateSecurityStates (gSUBrowser->SUCInfo->MapTable[InformationHiiHandle].HiiHandle);
//_End_L05_SETUP_MENU_

      UpdateHDCConfigure (
        gSUBrowser->SUCInfo->MapTable[AdvanceHiiHandle].HiiHandle,
        MyIfrNVData
        );
      SUCInfo->DoRefresh = TRUE;
//[-start-190701-16990083-add]//
	  JumpToFirstQuestion ();
//[-end-190701-16990083-add]//
    }

    gBS->FreePool (StringPtr);
    break;
  //=================//
  // KEY_LOAD_CUSTOM //
  //=================//
  case KEY_LOAD_CUSTOM:
    LoadCustomRcOption ();
//[-start-190701-16990083-add]//
	JumpToFirstQuestion ();
//[-end-190701-16990083-add]//
    break;
  //=================//
  // KEY_SAVE_CUSTOM //
  //=================//
  case KEY_SAVE_CUSTOM:
    SaveCustomRcOption ();
    break;

//_Start_L05_SETUP_MENU_
  case L05_KEY_SECURE_BOOT:
#ifdef L05_NOTEBOOK_CLOUD_BOOT_ENABLE
    if (((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05SecureBoot == 0) {
      ((SYSTEM_CONFIGURATION *) MyIfrNVData)->NetworkProtocol = UEFI_NETWORK_BOOT_OPTION_PXE_BOTH;
      ((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05LenovoCloudServices = 0;
#ifdef L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE
      ((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05UefiWifiNetworkBoot = 0;
#endif
    }
#endif
    break;

  case L05_KEY_RESET_TO_SETUP_MODE:
    //
    //  By L05 Minimum BIOS Spec v1.27  10 Secure boot and Boot mode
    //  When user select Reset to Setup Mode, this cause system workingin Setup mode
    //  and user may customize the secure boot database
    //
    StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_RESET_TO_SETUP_MODE_STRING), NULL);

    Buffer = AllocateZeroPool (StrSize (StringPtr) + sizeof (CHAR16));
    if (Buffer != NULL) {
      UnicodeSPrint (Buffer, (StrSize (StringPtr) + sizeof (CHAR16)), L"%s?", StringPtr);
    }

    gSUBrowser->H2ODialog->ConfirmDialog (
                             0,
                             FALSE,
                             0,
                             NULL,
                             &Key,
                             Buffer
                             );

    if (Key.UnicodeChar == CHAR_CARRIAGE_RETURN) {
      mL05ResetToSetupMode = TRUE;
      mL05RestoreFactoryKeys = FALSE;
      ((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05PlatformMode   = 1; //0:User Mode, 1:Setup Mode
      ((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05SecureBootMode = 1; //0:Standard,  1:Custom
      ((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05SecureBoot     = 0; //0:Disable,   1:Enable

    }

    gBS->FreePool (StringPtr);
    gBS->FreePool (Buffer);
    break;

  case L05_KEY_RESTORE_FACTORY_KEYS:
    //
    //  By L05 Minimum BIOS Spec v1.27  10 Secure boot and Boot mode
    //  When user select Restore Factory Keys, BIOS must display standard mode to user,
    //  change platform status to User mode and enable secure boot
    //
    StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_RESTORE_FACTORY_KEYS_STRING), NULL);

    Buffer = AllocateZeroPool (StrSize (StringPtr) + sizeof (CHAR16));
    if (Buffer != NULL) {
      UnicodeSPrint (Buffer, (StrSize (StringPtr) + sizeof (CHAR16)), L"%s?", StringPtr);
    }

    gSUBrowser->H2ODialog->ConfirmDialog (
                             0,
                             FALSE,
                             0,
                             NULL,
                             &Key,
                             Buffer
                             );

    if (Key.UnicodeChar == CHAR_CARRIAGE_RETURN) {
      mL05ResetToSetupMode = FALSE;
      mL05RestoreFactoryKeys = TRUE;
      ((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05PlatformMode   = 0; //0:User Mode, 1:Setup Mode
      ((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05SecureBootMode = 0; //0:Standard,  1:Custom
      ((SYSTEM_CONFIGURATION *) MyIfrNVData)->L05SecureBoot     = 1; //0:Disable,   1:Enable
    }

    gBS->FreePool (StringPtr);
    gBS->FreePool (Buffer);
    break;

//[-start-210701-FLINT00010-add]//
#ifdef LCFC_SUPPORT
  case L05_KEY_ALWAYS_USB :
    if (((SYSTEM_CONFIGURATION*) MyIfrNVData)->L05AlwaysOnUsb==1 ){
      ((SYSTEM_CONFIGURATION*) MyIfrNVData)->L05ChargeInBatteryMode=1;
    }else {
      ((SYSTEM_CONFIGURATION*) MyIfrNVData)->L05ChargeInBatteryMode=0;
    }
    break;
#endif
//[-end-210629-FLINT00009-add]//

//[-start-210629-FLINT00008-add]//
#ifdef LCFC_SUPPORT
{
   case L05_FLTP_TO_BOOT :
     UINT8 Data;
     LfcEcLibEcRamRead(0x8D, &Data);
     if (((SYSTEM_CONFIGURATION*) MyIfrNVData)->L05FlipToBoot ==1 ){
       Data |= BIT0;
//       Status = LfcEcLibEcRamWrite(0x8D, Data);// Enable Flip to Boot
     } else {
       Data &= ~BIT0;
//       Status = LfcEcLibEcRamWrite(0x8D, Data);// Disable Flip to Boot
     }
   break;
}
#endif
//[-end-210701-FLINT00010-add]//
//_End_L05_SETUP_MENU_

  //=========//
  // default //
  //=========//
  default :
    break;
  }

  return EFI_SUCCESS;
}


/**
 This function allows a caller to extract the current configuration for one
 or more named elements from the target driver.

 @param [in]   This             Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
 @param [in]   Request          A null-terminated Unicode string in <ConfigRequest> format.
 @param [out]  Progress         On return, points to a character in the Request string.
                                Points to the string's null terminator if request was successful.
                                Points to the most recent '&' before the first failing name/value
                                pair (or the beginning of the string if the failure is in the
                                first name/value pair) if the request was not successful.
 @param [out]  Results          A null-terminated Unicode string in <ConfigAltResp> format which
                                has all values filled in for the names in the Request string.
                                String to be allocated by the called function.

 @retval EFI_SUCCESS            The Results is filled with the requested values.
 @retval EFI_OUT_OF_RESOURCES   Not enough memory to store the results.
 @retval EFI_INVALID_PARAMETER  Request is NULL, illegal syntax, or unknown name.
 @retval EFI_NOT_FOUND          Routing data doesn't match any storage in this driver.

**/
EFI_STATUS
EFIAPI
GenericExtractConfigHook (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  CONST EFI_STRING                       Request,
  OUT EFI_STRING                             *Progress,
  OUT EFI_STRING                             *Results
  )
{
  EFI_STATUS                                 Status;
  UINTN                                      BufferSize;
  EFI_STRING                                 ConfigRequest;
  BOOLEAN                                    AllocatedRequest;
  UINTN                                      Size;
  CHAR16                                     *StrPointer;
  UINT8                                      *RcSetupData;
  ICC_SETUP_DATA                             IccSetup;
  CPU_SETUP_VOLATILE_DATA                    CpuSetupVolData;
  SETUP_CPU_FEATURES                         SetupCpuFeaturesData;
  TBT_SETUP_VOLATILE_DATA                    TbtSetupVolatileData;

  if (This == NULL || Progress == NULL || Results == NULL) {
    return EFI_INVALID_PARAMETER;
  }
//[-start-190611-IB16990047-add]//
  RcSetupData = NULL;
//[-end-190611-IB16990047-add]//
  if (HiiIsConfigHdrMatch (Request, &gSetupVariableGuid, PLATFORM_SETUP_VARIABLE_NAME)) {
    BufferSize = sizeof (SETUP_DATA);
    RcSetupData = gRcSUBrowser->SetupDataSUBrowserData;
  } else if (HiiIsConfigHdrMatch (Request, &gSaSetupVariableGuid, SA_SETUP_VARIABLE_NAME)) {
    BufferSize = sizeof (SA_SETUP);
    RcSetupData = gRcSUBrowser->SaSUBrowserData;
  } else if (HiiIsConfigHdrMatch (Request, &gMeSetupVariableGuid, ME_SETUP_VARIABLE_NAME)) {
    BufferSize = sizeof (ME_SETUP);
    RcSetupData = gRcSUBrowser->MeSUBrowserData;
  } else if (HiiIsConfigHdrMatch (Request, &gCpuSetupVariableGuid, CPU_SETUP_VARIABLE_NAME)) {
    BufferSize = sizeof (CPU_SETUP);
    RcSetupData = gRcSUBrowser->CpuSUBrowserData;
  } else if (HiiIsConfigHdrMatch (Request, &gPchSetupVariableGuid, PCH_SETUP_VARIABLE_NAME)) {
    BufferSize = sizeof (PCH_SETUP);
//[-start-210902-05660177-modify]//
    RcSetupData = gRcSUBrowser->PchSUBrowserData;
  } else if (HiiIsConfigHdrMatch (Request, &gSiSetupVariableGuid, SI_SETUP_VARIABLE_NAME)) {
    BufferSize = sizeof (SI_SETUP);
    RcSetupData = gRcSUBrowser->SiSUBrowserData;
//[-end-210902-05660177-modify]//
  } else if (HiiIsConfigHdrMatch (Request, &gSetupVariableGuid, SETUP_VOLATILE_DATA_VARIABLE_NAME)) {
    BufferSize = sizeof (mSetupVolatileData);
    RcSetupData = (UINT8 *) &mSetupVolatileData;
  } else if (HiiIsConfigHdrMatch (Request, &gIccGuid, ICC_SETUP_DATA_C_NAME)) {
    BufferSize = sizeof (ICC_SETUP_DATA);
    Status = gRT->GetVariable (ICC_SETUP_DATA_C_NAME, &gIccGuid, NULL, &BufferSize, &IccSetup);
    ASSERT_EFI_ERROR (Status);
    RcSetupData = (UINT8 *) &IccSetup;
  } else if (HiiIsConfigHdrMatch (Request, &gCpuSetupVariableGuid, CPU_SETUP_VOLATILE_DATA_VARIABLE_NAME)) {
    BufferSize = sizeof (CPU_SETUP_VOLATILE_DATA);
    Status = gRT->GetVariable (CPU_SETUP_VOLATILE_DATA_VARIABLE_NAME, &gCpuSetupVariableGuid, NULL, &BufferSize, &CpuSetupVolData);
    ASSERT_EFI_ERROR (Status);
    RcSetupData = (UINT8 *) &CpuSetupVolData;
  } else if (HiiIsConfigHdrMatch (Request, &gSetupVariableGuid,SETUP_CPU_FEATURES_NAME)) {
    BufferSize = sizeof (SETUP_CPU_FEATURES);
    Status = gRT->GetVariable (SETUP_CPU_FEATURES_NAME, &gSetupVariableGuid, NULL, &BufferSize, &SetupCpuFeaturesData);
    ASSERT_EFI_ERROR(Status);
    RcSetupData = (UINT8 *) &SetupCpuFeaturesData;
  } else if (HiiIsConfigHdrMatch (Request, &gMeSetupVariableGuid, ME_SETUP_STORAGE_VARIABLE_NAME)) {
    BufferSize = sizeof (ME_SETUP_STORAGE);
    RcSetupData = gRcSUBrowser->MeStorageSUBrowserData;
  } else if (HiiIsConfigHdrMatch (Request, &gSetupVariableGuid,TBT_SETUP_VOLATILLE_VARIABLE_NAME)) {
    BufferSize = sizeof (TBT_SETUP_VOLATILE_DATA);
    Status = gRT->GetVariable (TBT_SETUP_VOLATILLE_VARIABLE_NAME, &gSetupVariableGuid, NULL, &BufferSize, &TbtSetupVolatileData);
    ASSERT_EFI_ERROR(Status);
    RcSetupData = (UINT8 *) &TbtSetupVolatileData;
  } else {
    Status = GenericExtractConfig (This, Request, Progress, Results);
    return Status;
  }

  *Progress        = Request;
  ConfigRequest    = NULL;
  AllocatedRequest = FALSE;
  Size             = 0;

  //
  // Set Request to the unified request string.
  //
  ConfigRequest = Request;
  //
  // Check whether Request includes Request Element.
  //
  if (StrStr (Request, L"OFFSET") == NULL) {
    //
    // Check Request Element does exist in Reques String
    //
    StrPointer = StrStr (Request, L"PATH");
    if (StrPointer == NULL) {
      return EFI_INVALID_PARAMETER;
    }
    if (StrStr (StrPointer, L"&") == NULL) {
      Size = (StrLen (Request) + 32 + 1) * sizeof (CHAR16);
      ConfigRequest = AllocateZeroPool (Size);
      ASSERT (ConfigRequest != NULL);
      AllocatedRequest = TRUE;
      UnicodeSPrint (ConfigRequest, Size, L"%s&OFFSET=0&WIDTH=%016LX", Request, (UINT64) BufferSize);
    }
  }

  if (StrStr (ConfigRequest, L"OFFSET") == NULL) {
    //
    // If requesting Name/Value storage, return not found.
    //
    return EFI_NOT_FOUND;
  }

  //
  // Convert buffer data to <ConfigResp> by helper function BlockToConfig()
  //
  Status = gSUBrowser->HiiConfigRouting->BlockToConfig (
                                           gSUBrowser->HiiConfigRouting,
                                           ConfigRequest,
                                           RcSetupData,
                                           BufferSize,
                                           Results,
                                           Progress
                                           );

  //
  // Free the allocated config request string.
  //
  if (AllocatedRequest) {
    gBS->FreePool (ConfigRequest);
    ConfigRequest = NULL;
  }
  //
  // Set Progress string to the original request string.
  //
  if (Request == NULL) {
    *Progress = NULL;
  } else if (StrStr (Request, L"OFFSET") == NULL) {
    *Progress = Request + StrLen (Request);
  }
  return Status;
}


/**
 This function allows a caller to extract the current configuration for one
 or more named elements from the target driver.

 @param [in]   This             Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
 @param [in]   Request          A null-terminated Unicode string in <ConfigRequest> format.
 @param [out]  Progress         On return, points to a character in the Request string.
                                Points to the string's null terminator if request was successful.
                                Points to the most recent '&' before the first failing name/value
                                pair (or the beginning of the string if the failure is in the
                                first name/value pair) if the request was not successful.
 @param [out]  Results          A null-terminated Unicode string in <ConfigAltResp> format which
                                has all values filled in for the names in the Request string.
                                String to be allocated by the called function.

 @retval EFI_SUCCESS            The Results is filled with the requested values.
 @retval EFI_OUT_OF_RESOURCES   Not enough memory to store the results.
 @retval EFI_INVALID_PARAMETER  Request is NULL, illegal syntax, or unknown name.
 @retval EFI_NOT_FOUND          Routing data doesn't match any storage in this driver.

**/
EFI_STATUS
EFIAPI
GenericExtractConfig (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  CONST EFI_STRING                       Request,
  OUT EFI_STRING                             *Progress,
  OUT EFI_STRING                             *Results
  )
{
  EFI_STATUS                    Status;
  UINTN                         BufferSize;
  EFI_STRING                    ConfigRequestHdr;
  EFI_STRING                    ConfigRequest;
  BOOLEAN                       AllocatedRequest;
  UINTN                         Size;
  CHAR16                        *StrPointer;
  EFI_CALLBACK_INFO             *CallbackInfo;
  EFI_HANDLE                    DriverHandle;
  EFI_GUID                      VarStoreGuid = SYSTEM_CONFIGURATION_GUID;

  if (This == NULL || Progress == NULL || Results == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  *Progress        = Request;
  CallbackInfo     = EFI_CALLBACK_INFO_FROM_THIS (This);
  BufferSize       = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemConfig");
  ConfigRequestHdr = NULL;
  ConfigRequest    = NULL;
  AllocatedRequest = FALSE;
  Size             = 0;

  if (Request == NULL) {
    //
    // Request is set to NULL, construct full request string.
    //
    //
    // Allocate and fill a buffer large enough to hold the <ConfigHdr> template
    // followed by "&OFFSET=0&WIDTH=WWWWWWWWWWWWWWWW" followed by a Null-terminator
    //
    Status = gSUBrowser->HiiDatabase->GetPackageListHandle (gSUBrowser->HiiDatabase, CallbackInfo->HiiHandle, &DriverHandle);
    if (EFI_ERROR (Status)) {
      return EFI_NOT_FOUND;
    }
    ConfigRequestHdr = HiiConstructConfigHdr (&mFormSetGuid, mVariableName, DriverHandle);
    if (ConfigRequestHdr == NULL) {
      return EFI_NOT_FOUND;
    }
    Size = (StrLen (ConfigRequestHdr) + 32 + 1) * sizeof (CHAR16);
    ConfigRequest = AllocateZeroPool (Size);
    ASSERT (ConfigRequest != NULL);
    AllocatedRequest = TRUE;
    UnicodeSPrint (ConfigRequest, Size, L"%s&OFFSET=0&WIDTH=%016LX", ConfigRequestHdr, (UINT64) BufferSize);
    FreePool (ConfigRequestHdr);
    ConfigRequestHdr = NULL;
  } else {
    //
    // Check routing data in <ConfigHdr>.
    // Note: if only one Storage is used, then this checking could be skipped.
    //
    if (!HiiIsConfigHdrMatch (Request, &mFormSetGuid, mVariableName)) {
      return EFI_NOT_FOUND;
    }
    //
    // Set Request to the unified request string.
    //
    ConfigRequest = Request;
    //
    // Check whether Request includes Request Element.
    //
    if (StrStr (Request, L"OFFSET") == NULL) {
      //
      // Check Request Element does exist in Reques String
      //
      StrPointer = StrStr (Request, L"PATH");
      if (StrPointer == NULL) {
        return EFI_INVALID_PARAMETER;
      }
      if (StrStr (StrPointer, L"&") == NULL) {
        Size = (StrLen (Request) + 32 + 1) * sizeof (CHAR16);
        ConfigRequest = AllocateZeroPool (Size);
        ASSERT (ConfigRequest != NULL);
        AllocatedRequest = TRUE;
        UnicodeSPrint (ConfigRequest, Size, L"%s&OFFSET=0&WIDTH=%016LX", Request, (UINT64) BufferSize);
      }
    }
  }

  if (StrStr (ConfigRequest, L"OFFSET") == NULL) {
    //
    // If requesting Name/Value storage, return not found.
    //
    return EFI_NOT_FOUND;
  }

  //
  // Convert buffer data to <ConfigResp> by helper function BlockToConfig()
  //
  Status = gSUBrowser->HiiConfigRouting->BlockToConfig (
                                           gSUBrowser->HiiConfigRouting,
                                           ConfigRequest,
                                           (UINT8 *) gSUBrowser->SCBuffer,
                                           BufferSize,
                                           Results,
                                           Progress
                                           );

  //
  // Free the allocated config request string.
  //
  if (AllocatedRequest) {
    gBS->FreePool (ConfigRequest);
    ConfigRequest = NULL;
  }
  //
  // Set Progress string to the original request string.
  //
  if (Request == NULL) {
    *Progress = NULL;
  } else if (StrStr (Request, L"OFFSET") == NULL) {
    *Progress = Request + StrLen (Request);
  }

  return Status;
}


/**
 This function processes the results of changes in configuration.

 @param [in]   This             Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
 @param [in]   Configuration    A null-terminated Unicode string in <ConfigResp> format.
 @param [out]  Progress         A pointer to a string filled in with the offset of the most
                                recent '&' before the first failing name/value pair (or the
                                beginning of the string if the failure is in the first
                                name/value pair) or the terminating NULL if all was successful.

 @retval EFI_SUCCESS            The Results is processed successfully.
 @retval EFI_INVALID_PARAMETER  Configuration is NULL.
 @retval EFI_NOT_FOUND          Routing data doesn't match any storage in this driver.

**/
EFI_STATUS
EFIAPI
GenericRouteConfig (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  CONST EFI_STRING                       Configuration,
  OUT EFI_STRING                             *Progress
  )
{
  EFI_STATUS                                Status;
  VOID                                      *SetupNvData;
  UINTN                                     BufferSize;
  UINTN                                     SetupDataBufferSize;
  UINTN                                     SaBufferSize;
  UINTN                                     MeBufferSize;
  UINTN                                     CpuBufferSize;
  UINTN                                     PchBufferSize;
  UINTN                                     SiBufferSize;
  UINTN                                     MeStorageBufferSize;
  CPU_SETUP                                 *CpuSetup;
//   SGX_SETUP_DATA                            *SgxSetupData;
//   UINTN                                     SgxSetupDataSize;

  EFI_GUID                                  VarStoreGuid = SYSTEM_CONFIGURATION_GUID;
  UINT8                                     *Lang;
//  UINTN                                     VariableSize;
//   UINT32                                    CpuSetupSgxEpochVarAttr;
//   CPU_SETUP_SGX_EPOCH_DATA                  SgxEpochSetupData;

//_Start_L05_SETUP_MENU_
//[-start-210817-DABING0002-modify]//
#ifndef LCFC_SUPPORT
{
#endif
//[-end-210817-DABING0002-modify]//
  SYSTEM_CONFIGURATION                     *TempSetupNvData;
  UINTN                                    TempBufferSize;
  BOOLEAN                                  L05SecureBootUpdateNeeded;
  EFI_L05_SECURE_BOOT_DATA                 L05SecureBootData;
  EFI_L05_SECURE_BOOT_PROTOCOL             *L05SecureBootPtr;


  TempSetupNvData           = NULL;
  TempBufferSize            = 0;
  L05SecureBootUpdateNeeded = FALSE;
  L05SecureBootPtr          = NULL;

  ZeroMem (&L05SecureBootData, sizeof (EFI_L05_SECURE_BOOT_DATA));

  TempBufferSize = sizeof (SYSTEM_CONFIGURATION);
  TempSetupNvData = GetVariableAndSize (
                      L"Setup",
                      &mFormSetGuid,
                      &TempBufferSize
                      );

  if (((SYSTEM_CONFIGURATION*) gSUBrowser->SCBuffer)->L05SecureBoot != TempSetupNvData->L05SecureBoot) {
    if (((SYSTEM_CONFIGURATION*) gSUBrowser->SCBuffer)->L05SecureBoot == 1) {  // 0:Disable, 1:Enable
      L05SecureBootData.Action = L05_SECURE_BOOT_ENABLE;
    } else {
      L05SecureBootData.Action = L05_SECURE_BOOT_DISABLE;
    }
    L05SecureBootUpdateNeeded = TRUE;
  }

  if (mL05ResetToSetupMode) {
    L05SecureBootData.ResetToSystemMode = L05_SECURE_BOOT_ENABLE;
    L05SecureBootUpdateNeeded = TRUE;
  }

  if (mL05RestoreFactoryKeys) {
    L05SecureBootData.RestoreFactoryKeys = L05_SECURE_BOOT_ENABLE;
    L05SecureBootUpdateNeeded = TRUE;
  }

  if (L05SecureBootUpdateNeeded) {
    Status = gBS->LocateProtocol (
                    &gEfiL05SecureBootProtocolGuid,
                    NULL,
                    &L05SecureBootPtr
                    );
    if (!EFI_ERROR (Status)) {
      Status = L05SecureBootPtr->L05SecureBootCallback (&L05SecureBootData);
    }
  }
//[-start-210817-DABING0002-modify]//
#ifndef LCFC_SUPPORT
}
#endif
//[-end-210817-DABING0002-modify]//
//_End_L05_SETUP_MENU_

  if (Configuration != NULL && !HiiIsConfigHdrMatch (Configuration, &mFormSetGuid, mVariableName)) {
    return EFI_SUCCESS;
  }
  BufferSize = PcdGet32 (PcdSetupConfigSize);
  SetupVariableConfig (
    &VarStoreGuid,
    L"SystemConfig",
    BufferSize,
    (UINT8 *) gSUBrowser->SCBuffer,
    TRUE
    );

  {
    SetupDataBufferSize = sizeof (SETUP_DATA);
    SetupVariableConfig (
      &gSetupVariableGuid,
      PLATFORM_SETUP_VARIABLE_NAME,
      SetupDataBufferSize,
      (UINT8 *) gRcSUBrowser->SetupDataSUBrowserData,
      TRUE
      );
    SaBufferSize = sizeof (SA_SETUP);
    SetupVariableConfig (
      &gSaSetupVariableGuid,
      SA_SETUP_VARIABLE_NAME,
      SaBufferSize,
      (UINT8 *) gRcSUBrowser->SaSUBrowserData,
      TRUE
      );
    MeBufferSize = sizeof (ME_SETUP);
    SetupVariableConfig (
      &gMeSetupVariableGuid,
      ME_SETUP_VARIABLE_NAME,
      MeBufferSize,
      (UINT8 *) gRcSUBrowser->MeSUBrowserData,
      TRUE
      );
    CpuBufferSize = sizeof (CPU_SETUP);
    SetupVariableConfig (
      &gCpuSetupVariableGuid,
      CPU_SETUP_VARIABLE_NAME,
      CpuBufferSize,
      (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
      TRUE
      );
    PchBufferSize = sizeof (PCH_SETUP);
    SetupVariableConfig (
      &gPchSetupVariableGuid,
      PCH_SETUP_VARIABLE_NAME,
      PchBufferSize,
      (UINT8 *) gRcSUBrowser->PchSUBrowserData,
      TRUE
      );
    SiBufferSize = sizeof (SI_SETUP);
    SetupVariableConfig (
      &gSiSetupVariableGuid,
      SI_SETUP_VARIABLE_NAME,
      SiBufferSize,
      (UINT8 *) gRcSUBrowser->SiSUBrowserData,
      TRUE
      );
    MeStorageBufferSize = sizeof (ME_SETUP_STORAGE);
    SetupVariableConfig (
      &gMeSetupVariableGuid,
      ME_SETUP_STORAGE_VARIABLE_NAME,
      MeStorageBufferSize,
      (UINT8 *) gRcSUBrowser->MeStorageSUBrowserData,
      TRUE
      );
  }

//
// Oem hook when F10 or Exit Saving Changes or Save Change Without Exit submit.
// And system will set current setuputility setting to browser.
//
  DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices Call: OemSvcHookRouteConfig \n"));
  Status = OemSvcHookRouteConfig (
    (VOID *)gSUBrowser->SCBuffer,
    (UINT32)BufferSize,
    (VOID *)gRcSUBrowser,
    mScuRecord
    );
  DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices OemSvcHookRouteConfig Status: %r\n", Status));

  SetupVariableConfig (
    &VarStoreGuid,
    L"SystemConfig",
    BufferSize,
    (UINT8 *) gSUBrowser->SCBuffer,
    FALSE
    );

  {
    //========================================================//
    // Save SETUP DATA variable and update SETUP_DATA to form //
    //========================================================//
    SetupVariableConfig (
      &gSetupVariableGuid,
      PLATFORM_SETUP_VARIABLE_NAME,
      SetupDataBufferSize,
      (UINT8 *) gRcSUBrowser->SetupDataSUBrowserData,
      FALSE
      );

    Status = SaveSetupConfig (
               PLATFORM_SETUP_VARIABLE_NAME,
               &gSetupVariableGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               SetupDataBufferSize,
               (VOID *)gRcSUBrowser->SetupDataSUBrowserData
               );
    //===================================================//
    // Save SA setup variable and update SA data to form //
    //===================================================//
    SetupVariableConfig (
      &gSaSetupVariableGuid,
      SA_SETUP_VARIABLE_NAME,
      SaBufferSize,
      (UINT8 *) gRcSUBrowser->SaSUBrowserData,
      FALSE
      );

    Status = SaveSetupConfig (
               SA_SETUP_VARIABLE_NAME,
               &gSaSetupVariableGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               SaBufferSize,
               (VOID *)gRcSUBrowser->SaSUBrowserData
               );
    //===================================================//
    // Save ME setup variable and update ME data to form //
    //===================================================//
    SetupVariableConfig (
      &gMeSetupVariableGuid,
      ME_SETUP_VARIABLE_NAME,
      MeBufferSize,
      (UINT8 *) gRcSUBrowser->MeSUBrowserData,
      FALSE
      );

    Status = SaveSetupConfig (
               ME_SETUP_VARIABLE_NAME,
               &gMeSetupVariableGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               MeBufferSize,
               (VOID *)gRcSUBrowser->MeSUBrowserData
               );
    //=====================================================//
    // Save CPU setup variable and update CPU data to form //
    //=====================================================//
    SetupVariableConfig (
      &gCpuSetupVariableGuid,
      CPU_SETUP_VARIABLE_NAME,
      CpuBufferSize,
      (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
      FALSE
      );
    Status = SaveSetupConfig (
               CPU_SETUP_VARIABLE_NAME,
               &gCpuSetupVariableGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               CpuBufferSize,
               (VOID *)gRcSUBrowser->CpuSUBrowserData
               );
    //=====================================================//
    // Save PCH setup variable and update PCH data to form //
    //=====================================================//
    SetupVariableConfig (
      &gPchSetupVariableGuid,
      PCH_SETUP_VARIABLE_NAME,
      PchBufferSize,
      (UINT8 *) gRcSUBrowser->PchSUBrowserData,
      FALSE
      );
    Status = SaveSetupConfig (
               PCH_SETUP_VARIABLE_NAME,
               &gPchSetupVariableGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               PchBufferSize,
               (VOID *)gRcSUBrowser->PchSUBrowserData
               );
    //=====================================================//
    // Save SI setup variable and update SI data to form //
    //=====================================================//
    SetupVariableConfig (
      &gSiSetupVariableGuid,
      SI_SETUP_VARIABLE_NAME,
      SiBufferSize,
      (UINT8 *) gRcSUBrowser->SiSUBrowserData,
      FALSE
      );
    Status = SaveSetupConfig (
               SI_SETUP_VARIABLE_NAME,
               &gSiSetupVariableGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               SiBufferSize,
               (VOID *)gRcSUBrowser->SiSUBrowserData
               );
    //===========================================================//
    // Save ME setup storage variable and update ME data to form //
    //===========================================================//
    SetupVariableConfig (
      &gMeSetupVariableGuid,
      ME_SETUP_STORAGE_VARIABLE_NAME,
      MeStorageBufferSize,
      (UINT8 *) gRcSUBrowser->MeStorageSUBrowserData,
      FALSE
      );

    Status = SaveSetupConfig (
               ME_SETUP_STORAGE_VARIABLE_NAME,
               &gMeSetupVariableGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               MeStorageBufferSize,
               (VOID *)gRcSUBrowser->MeStorageSUBrowserData
               );
//[-start-210817-DABING0002-modify]//
#ifdef LCFC_SUPPORT
    OemSvcSwitchAlwaysOnUsb (RouteConfig,TempSetupNvData->L05AlwaysOnUsb);

    OemSvcSwitchChargeInBatteryMode(RouteConfig,TempSetupNvData->L05ChargeInBatteryMode);
#endif
//[-end-210817-DABING0002-modify]//
    //=======================================================================//
    // Run RC relatvie route config. functions after variable saving is done //
    //=======================================================================//
    IccRouteConfig();
    MeRouteConfig ();
    if (mMeReset == TRUE) {
      mFullResetFlag++;
    }
  }

  BufferSize   = PcdGet32 (PcdSetupConfigSize);
  if (BufferSize == 0) {
    Status = RETURN_BAD_BUFFER_SIZE;
    ASSERT_EFI_ERROR (Status);
    return Status;
  }
  SetupNvData  = AllocateZeroPool (BufferSize);
  if (SetupNvData == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    ASSERT_EFI_ERROR (Status);
    return Status;
  }
  CopyMem (SetupNvData, gSUBrowser->SCBuffer, BufferSize);

  Lang = GetVariableAndSize (L"PlatformLang", &gEfiGlobalVariableGuid, &BufferSize);
  Status = gRT->SetVariable (
                  L"BackupPlatformLang",
                  &gEfiGenericVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  (Lang == NULL) ? 0 : BufferSize,
                  Lang
                  );
  if (Lang != NULL) {
    FreePool (Lang);
  }
//[-start-190701-16990083-add]//
  SetupUtilityLibRouteConfig ();
//[-end-190701-16990083-add]//

  SetSecurityStatus ();
  BufferSize = PcdGet32 (PcdSetupConfigSize);
  Status = SaveSetupConfig (
             L"Setup",
             &mFormSetGuid,
             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
             BufferSize,
             SetupNvData
             );

//_Start_L05_SETUP_MENU_
  UpdateConfigurationMenu (gSUBrowser->SUCInfo->MapTable[ConfigurationHiiHandle].HiiHandle);
  L05UpdateBootMenu (gSUBrowser->SUCInfo->MapTable[BootHiiHandle].HiiHandle);

  //
  // OEM Switch
  //
  OemSvcSwitchWirelessLan                (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05WirelessLan);
  OemSvcSwitchHotKeyMode                 (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05HotKeyMode);
  OemSvcSwitchFoolProofFnCtrl            (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05FoolProofFnCtrl);
  OemSvcSwitchAlwaysOnUsb                (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05AlwaysOnUsb);
  OemSvcSwitchChargeInBatteryMode        (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05ChargeInBatteryMode);
#ifndef L05_GAMING_UI_ENABLE
  OemSvcSwitchSystemPerformanceMode      (RouteConfig, ((SYSTEM_CONFIGURATION*) SetupNvData)->L05SystemPerformanceMode);
#else
  OemSvcSwitchThermalMode                (RouteConfig, ((SYSTEM_CONFIGURATION*) SetupNvData)->L05ThermalMode);
  OemSvcSwitchRestoreDefaultOverclocking (RouteConfig, ((SYSTEM_CONFIGURATION*) SetupNvData)->L05RestoreDefaultOverclocking);
#endif
  OemSvcSwitchFlipToBoot                 (RouteConfig, ((SYSTEM_CONFIGURATION*) SetupNvData)->L05FlipToBoot);
#ifdef L05_ONE_KEY_BATTERY_ENABLE
  OemSvcSwitchOneKeyBattery              (RouteConfig, ((SYSTEM_CONFIGURATION*) SetupNvData)->L05OneKeyBattery);
#endif
  OemSvcSwitchWakeOnVoice                (RouteConfig, ((SYSTEM_CONFIGURATION*) SetupNvData)->L05WakeOnVoice);
  OemSvcSwitchUltraQuietMode             (RouteConfig, ((SYSTEM_CONFIGURATION*) SetupNvData)->L05UltraQuietMode);
  OemSvcSwitchInstantBoot                (RouteConfig, ((SYSTEM_CONFIGURATION*) SetupNvData)->L05InstantBoot);

  //
  // I/O Port Access
  //
  OemSvcSwitchEthernetLan         (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05EthernetLan);
  OemSvcSwitchWirelessWan         (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05WirelessWan);
  OemSvcSwitchBluetooth           (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05Bluetooth);
  OemSvcSwitchUsbPort             (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05UsbPort);
  OemSvcSwitchMemoryCardSlot      (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05MemoryCardSlot);
  OemSvcSwitchSmartCardSlot       (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05SmartCardSlot);
  OemSvcSwitchIntegratedCamera    (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05IntegratedCamera);
  OemSvcSwitchMicrophone          (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05Microphone);
  OemSvcSwitchFingerprintReader   (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05FingerprintReader);
  OemSvcSwitchThunderbolt         (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05Thunderbolt);
  OemSvcSwitchNfcDevice           (RouteConfig, ((SYSTEM_CONFIGURATION *) SetupNvData)->L05NfcDevice);
//_End_L05_SETUP_MENU_

  PlugInVgaDisplaySelectionSave ();

  if ((FeaturePcdGet (PcdHybridGraphicsSupported)) &&
      (FeaturePcdGet (PcdNvidiaOptimusSupported)) &&
      (FeaturePcdGet (PcdHgNvidiaDdsFeatureSupport))) {
    //
    // Sync "DisplayMode" setting to "PrimaryDisplay".
    //
    switch (((CHIPSET_CONFIGURATION *)SetupNvData)->DisplayMode) {
      case IgfxOnly:
        ((SA_SETUP *)gRcSUBrowser->SaSUBrowserData)->PrimaryDisplay = DisplayModeIgpu;
        break;
      case DgpuOnly:
        if (IsPchH ()) {
          ((SA_SETUP *)gRcSUBrowser->SaSUBrowserData)->PrimaryDisplay = DisplayModeDgpu;
        } else {
          ((SA_SETUP *)gRcSUBrowser->SaSUBrowserData)->PrimaryDisplay = DisplayModePci;
        }
        break;
      case MsHybrid:
        ((SA_SETUP *)gRcSUBrowser->SaSUBrowserData)->PrimaryDisplay = DisplayModeHg;
        break;
      case Dynamic:
        ((SA_SETUP *)gRcSUBrowser->SaSUBrowserData)->PrimaryDisplay = DisplayModeHg;
        break;
      default:
        break;
    }
    Status = SaveSetupConfig (
               SA_SETUP_VARIABLE_NAME,
               &gSaSetupVariableGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               SaBufferSize,
               (VOID *)gRcSUBrowser->SaSUBrowserData
               );
    if (EFI_ERROR(Status)) {
      DEBUG ((EFI_D_ERROR | EFI_D_INFO, "Sync DisplayMode with PrimaryDisplay fail. Status = %x\n", Status));
    }
  }

  WriteExtCmos8 (
    R_XCMOS_INDEX,
    R_XCMOS_DATA,
    S5LongRunTestFlag,
    ((CHIPSET_CONFIGURATION *)SetupNvData)->S5LongRunTest
    );

  CpuSetup = (CPU_SETUP *)(VOID *)gRcSUBrowser->CpuSUBrowserData;
  // SgxSetupDataSize = sizeof (SGX_SETUP_DATA);
  // SgxSetupData = AllocateZeroPool (SgxSetupDataSize);
  // if (SgxSetupData == NULL) {
  //   Status = EFI_OUT_OF_RESOURCES;
  //   ASSERT_EFI_ERROR (Status);
  //   return Status;
  // }
  // VariableSize = sizeof (CPU_SETUP_SGX_EPOCH_DATA);
  // Status = gRT->GetVariable (
  //                 L"CpuSetupSgxEpochData",
  //                 &gCpuSetupVariableGuid,
  //                 &CpuSetupSgxEpochVarAttr,
  //                 &VariableSize,
  //                 &SgxEpochSetupData
  //                 );

  // SgxSetupData->EnableC6Dram = CpuSetup->EnableC6Dram;
  // SgxSetupData->EnableSgx = CpuSetup->EnableSgx;
  // SgxSetupData->EpochUpdate = CpuSetup->EpochUpdate;
  // SgxSetupData->ShowEpoch = CpuSetup->ShowEpoch;
  // SgxSetupData->MaxPrmrrSize = CpuSetup->MaxPrmrrSize;
  // SgxSetupData->PrmrrSize = CpuSetup->PrmrrSize;
  // SgxSetupData->SgxEpoch0 = SgxEpochSetupData.SgxEpoch0;
  // SgxSetupData->SgxEpoch1 = SgxEpochSetupData.SgxEpoch1;

  // Status = SaveSetupConfig (
  //            SGX_SETUP_VARIABLE_NAME,
  //            &gSgxSetupVariableGuid,
  //            EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
  //            SgxSetupDataSize,
  //            (VOID *)SgxSetupData
  //            );
  // if (EFI_ERROR(Status)) {
  //   DEBUG ((DEBUG_ERROR | DEBUG_INFO, "Save SGX variable fail. Status = %x\n", Status));
  // }

  if (mFullResetFlag != 0) {
    if (FeaturePcdGet (PcdH2OBdsCpSendFormAfterSupported)) {
      if (mSendFormAfterCpHandle == NULL) {
        Status = H2OCpRegisterHandler (&gH2OBdsCpSendFormAfterGuid , BdsCpSendFormAfterHandler , H2O_CP_MEDIUM, &mSendFormAfterCpHandle);
        if (EFI_ERROR (Status)) {
          DEBUG_CP ((DEBUG_ERROR, "Checkpoint Register Fail: %g (%r)\n", &gH2OBdsCpSendFormAfterGuid, Status));
          return Status;
        }
        DEBUG_CP ((DEBUG_INFO, "Checkpoint Registered: %g (%r)\n", &gH2OBdsCpSendFormAfterGuid, Status));
      }
    }
  }

//_Start_L05_WORKAROUND_HDD_PASSWORDS_UNSYNC_[Kernel]
  mIsSaveWithoutExit = FALSE;
//_End_L05_WORKAROUND_HDD_PASSWORDS_UNSYNC_[Kernel]

  // FreePool (SgxSetupData);
  // FreePool (SetupNvData);
  return  Status;
}

#if 0
/**
 This function processes the memory overclocking setting when the profile is set to custom profile.

 @param [in] [out]  MyIfrNvData    SCU chipset configuration data.

 @retval None

**/
VOID
ConfigXmpDimmProfile (
  IN OUT SA_SETUP                    *MySaIfrNVData
  )
{
  MEM_INFO_PROTOCOL      *MemInfo;
  EFI_STATUS             Status;

  MemInfo = NULL;

  if (MySaIfrNVData->SpdProfileSelected != USER_PROFILE) {
    return;
  }

  Status = gBS->LocateProtocol (&gMemInfoProtocolGuid, NULL, (VOID **)&MemInfo);
  ASSERT_EFI_ERROR (Status);

  if (MemInfo == NULL) {
    return;
  }

  if (MySaIfrNVData->DdrRefClk == XMP_MEM_AUTO) {
    MySaIfrNVData->DdrRefClk = MemInfo->MemInfoData.RefClk;
  }

  if (MySaIfrNVData->tCL == XMP_MEM_AUTO) {
    MySaIfrNVData->tCL = (UINT8) MemInfo->MemInfoData.Timing[0].tCL;
  }

  if (MySaIfrNVData->tRCDtRP == XMP_MEM_AUTO) {
    MySaIfrNVData->tRCDtRP = (UINT8) MemInfo->MemInfoData.Timing[0].tRCDtRP;
  }

  if (MySaIfrNVData->tRAS == XMP_MEM_AUTO) {
    MySaIfrNVData->tRAS = (UINT8) MemInfo->MemInfoData.Timing[0].tRAS;
  }

  if (MySaIfrNVData->tCWL == XMP_MEM_AUTO) {
    MySaIfrNVData->tCWL = (UINT8) MemInfo->MemInfoData.Timing[0].tCWL;
  }

  if (MySaIfrNVData->tFAW == XMP_MEM_AUTO) {
    MySaIfrNVData->tFAW = (UINT8) MemInfo->MemInfoData.Timing[0].tFAW;
  }

  if (MySaIfrNVData->tREFI == XMP_MEM_AUTO) {
    MySaIfrNVData->tREFI = MemInfo->MemInfoData.Timing[0].tREFI;
  }

  if (MySaIfrNVData->tRFC == XMP_MEM_AUTO) {
    MySaIfrNVData->tRFC = MemInfo->MemInfoData.Timing[0].tRFC;
  }

  if (MySaIfrNVData->tRRD == XMP_MEM_AUTO) {
    MySaIfrNVData->tRRD = (UINT8) MemInfo->MemInfoData.Timing[0].tRRD;
  }

  if (MySaIfrNVData->tRTP == XMP_MEM_AUTO) {
    MySaIfrNVData->tRTP = (UINT8) MemInfo->MemInfoData.Timing[0].tRTP;
  }

  if (MySaIfrNVData->tWR == XMP_MEM_AUTO) {
    MySaIfrNVData->tWR = (UINT8) MemInfo->MemInfoData.Timing[0].tWR;
  }

  if (MySaIfrNVData->tWTR == XMP_MEM_AUTO) {
    MySaIfrNVData->tWTR = (UINT8) MemInfo->MemInfoData.Timing[0].tWTR;
  }

  if (MySaIfrNVData->NModeSupport == XMP_MEM_AUTO) {
    MySaIfrNVData->NModeSupport = (UINT8) MemInfo->MemInfoData.Timing[0].NMode;
  }
}
#endif

/**
  To discard user changed Setup Data utility setting in this boot.

  @param  VOID

  @retval EFI_SUCCESS            Function has completed successfully.
  @return Other                  Cannot get SetupUtility browser data or language data.

**/
EFI_STATUS
DiscardSetupDataChange (
  VOID
  )
{
  EFI_STATUS                                Status;
  SETUP_DATA                                *MyOrgIfrNVData;
  UINTN                                     SetupDataBufferSize;

  Status = CommonGetVariableDataAndSize (
             PLATFORM_SETUP_VARIABLE_NAME,
             &gSetupVariableGuid,
             &SetupDataBufferSize,
             (VOID **) &MyOrgIfrNVData
             );
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  if (SetupDataBufferSize != 0) {
    CopyMem (
      gRcSUBrowser->SetupDataSUBrowserData,
      MyOrgIfrNVData,
      SetupDataBufferSize
      );
  } else {
    return EFI_NOT_FOUND;
  }

  gBS->FreePool (MyOrgIfrNVData);
  return EFI_SUCCESS;
}


/**
  To discard user changed SA setup utility setting in this boot.

  @param  VOID

  @retval EFI_SUCCESS            Function has completed successfully.
  @return Other                  Cannot get SetupUtility browser data or language data.

**/
EFI_STATUS
DiscardSaChange (
  VOID
  )
{
  EFI_STATUS                                Status;
  SA_SETUP                                  *MyOrgIfrNVData;
  UINTN                                     SaBufferSize;

  Status = CommonGetVariableDataAndSize (
             SA_SETUP_VARIABLE_NAME,
             &gSaSetupVariableGuid,
             &SaBufferSize,
             (VOID **) &MyOrgIfrNVData
             );
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  if (SaBufferSize != 0) {
    CopyMem (
      gRcSUBrowser->SaSUBrowserData,
      MyOrgIfrNVData,
      SaBufferSize
      );
  } else {
    return EFI_NOT_FOUND;
  }

  gBS->FreePool (MyOrgIfrNVData);
  return EFI_SUCCESS;
}


/**
  To discard user changed ME setup utility setting in this boot.

  @param  VOID

  @retval EFI_SUCCESS            Function has completed successfully.
  @return Other                  Cannot get SetupUtility browser data or language data.

**/
EFI_STATUS
DiscardMeChange (
  VOID
  )
{
  EFI_STATUS                                Status;
  ME_SETUP                                  *MyOrgIfrNVData;
  UINTN                                     MeBufferSize;

  Status = CommonGetVariableDataAndSize (
             ME_SETUP_VARIABLE_NAME,
             &gMeSetupVariableGuid,
             &MeBufferSize,
             (VOID **) &MyOrgIfrNVData
             );
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  if (MeBufferSize != 0) {
    CopyMem (
      gRcSUBrowser->MeSUBrowserData,
      MyOrgIfrNVData,
      MeBufferSize
      );
  } else {
    return EFI_NOT_FOUND;
  }

  gBS->FreePool (MyOrgIfrNVData);
  return EFI_SUCCESS;
}


/**
  To discard user changed CPU setup utility setting in this boot.

  @param  VOID

  @retval EFI_SUCCESS            Function has completed successfully.
  @return Other                  Cannot get SetupUtility browser data or language data.

**/
EFI_STATUS
DiscardCpuChange (
  VOID
  )
{
  EFI_STATUS                                Status;
  CPU_SETUP                                 *MyOrgIfrNVData;
  UINTN                                     CpuBufferSize;

  Status = CommonGetVariableDataAndSize (
             CPU_SETUP_VARIABLE_NAME,
             &gCpuSetupVariableGuid,
             &CpuBufferSize,
             (VOID **) &MyOrgIfrNVData
             );
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  if (CpuBufferSize != 0) {
    CopyMem (
      gRcSUBrowser->CpuSUBrowserData,
      MyOrgIfrNVData,
      CpuBufferSize
      );
  } else {
    return EFI_NOT_FOUND;
  }

  gBS->FreePool (MyOrgIfrNVData);
  return EFI_SUCCESS;
}


/**
  To discard user changed PCH setup utility setting in this boot.

  @param  VOID

  @retval EFI_SUCCESS            Function has completed successfully.
  @return Other                  Cannot get SetupUtility browser data or language data.

**/
EFI_STATUS
DiscardPchChange (
  VOID
  )
{
  EFI_STATUS                                Status;
  PCH_SETUP                                 *MyOrgIfrNVData;
  UINTN                                     PchBufferSize;

  Status = CommonGetVariableDataAndSize (
             PCH_SETUP_VARIABLE_NAME,
             &gPchSetupVariableGuid,
             &PchBufferSize,
             (VOID **) &MyOrgIfrNVData
             );
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  if (PchBufferSize != 0) {
    CopyMem (
      gRcSUBrowser->PchSUBrowserData,
      MyOrgIfrNVData,
      PchBufferSize
      );
  } else {
    return EFI_NOT_FOUND;
  }

  gBS->FreePool (MyOrgIfrNVData);
  return EFI_SUCCESS;
}
/**
  To discard user changed SI setup utility setting in this boot.

  @param  VOID

  @retval EFI_SUCCESS            Function has completed successfully.
  @return Other                  Cannot get SetupUtility browser data or language data.

**/
EFI_STATUS
DiscardSiChange (
  VOID
  )
{
  EFI_STATUS                                Status;
  SI_SETUP                                 *MyOrgIfrNVData;
  UINTN                                     SiBufferSize;

  Status = CommonGetVariableDataAndSize (
             SI_SETUP_VARIABLE_NAME,
             &gSiSetupVariableGuid,
             &SiBufferSize,
             (VOID **) &MyOrgIfrNVData
             );
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  if (SiBufferSize != 0) {
    CopyMem (
      gRcSUBrowser->SiSUBrowserData,
      MyOrgIfrNVData,
      SiBufferSize
      );
  } else {
    return EFI_NOT_FOUND;
  }

  gBS->FreePool (MyOrgIfrNVData);
  return EFI_SUCCESS;
}



EFI_STATUS
DiscardIccChange (
  VOID
  )
{
  DefaultIccSetup ();
  return EFI_SUCCESS;
}


/**
  To discard user changed ME setup storage utility setting in this boot.

  @param  VOID

  @retval EFI_SUCCESS            Function has completed successfully.
  @return Other                  Cannot get SetupUtility browser data or language data.

**/
EFI_STATUS
DiscardMeStorageChange (
  VOID
  )
{
  EFI_STATUS                                Status;
  ME_SETUP_STORAGE                          *MyStorageOrgIfrNVData;
  UINTN                                     MeStorageBufferSize;

  Status = CommonGetVariableDataAndSize (
             ME_SETUP_STORAGE_VARIABLE_NAME,
             &gMeSetupVariableGuid,
             &MeStorageBufferSize,
             (VOID **) &MyStorageOrgIfrNVData
             );
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  if (MeStorageBufferSize != 0) {
    CopyMem (
      gRcSUBrowser->MeStorageSUBrowserData,
      MyStorageOrgIfrNVData,
      MeStorageBufferSize
      );
  } else {
    return EFI_NOT_FOUND;
  }

  gBS->FreePool (MyStorageOrgIfrNVData);
  return EFI_SUCCESS;
}


/**
  To restore RC setup utility setting to user custom setting

  @param  None

  @retval EFI_SUCCESS            Function has completed successfully.
  @return Other                  Cannot get SetupUtility browser data.

**/
EFI_STATUS
LoadCustomRcOption (
  VOID
  )
{
  EFI_STATUS                                Status;
  UINT8                                     *RcSetupData = NULL;
  UINTN                                     RcBufferSize;

  //====================================//
  // Restore custom SETUP DATA variable //
  //====================================//
  RcBufferSize = sizeof (SETUP_DATA);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  L"Custom",
                  &gSetupVariableGuid,
                  NULL,
                  &RcBufferSize,
                  (VOID *)RcSetupData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupData);
    return Status;
  }
  CopyMem (gRcSUBrowser->SetupDataSUBrowserData, RcSetupData, RcBufferSize);
  FreePool (RcSetupData);
  //==================================//
  // Restore custom SA setup variable //
  //==================================//
  RcBufferSize = sizeof (SA_SETUP);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  L"Custom",
                  &gSaSetupVariableGuid,
                  NULL,
                  &RcBufferSize,
                  (VOID *)RcSetupData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupData);
    return Status;
  }
  CopyMem (gRcSUBrowser->SaSUBrowserData, RcSetupData, RcBufferSize);
  FreePool (RcSetupData);
  //==================================//
  // Restore custom ME setup variable //
  //==================================//
  RcBufferSize = sizeof (ME_SETUP);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  L"Custom",
                  &gMeSetupVariableGuid,
                  NULL,
                  &RcBufferSize,
                  (VOID *)RcSetupData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupData);
    return Status;
  }
  CopyMem (gRcSUBrowser->MeSUBrowserData, RcSetupData, RcBufferSize);
  FreePool (RcSetupData);
  //===================================//
  // Restore custom CPU setup variable //
  //===================================//
  RcBufferSize = sizeof (CPU_SETUP);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  L"Custom",
                  &gCpuSetupVariableGuid,
                  NULL,
                  &RcBufferSize,
                  (VOID *)RcSetupData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupData);
    return Status;
  }
  CopyMem (gRcSUBrowser->CpuSUBrowserData, RcSetupData, RcBufferSize);
  FreePool (RcSetupData);
  //===================================//
  // Restore custom PCH setup variable //
  //===================================//
  RcBufferSize = sizeof (PCH_SETUP);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  L"Custom",
                  &gPchSetupVariableGuid,
                  NULL,
                  &RcBufferSize,
                  (VOID *)RcSetupData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupData);
    return Status;
  }
  CopyMem (gRcSUBrowser->PchSUBrowserData, RcSetupData, RcBufferSize);
  FreePool (RcSetupData);
  //===================================//
  // Restore custom SI setup variable //
  //===================================//
  RcBufferSize = sizeof (SI_SETUP);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  L"Custom",
                  &gSiSetupVariableGuid,
                  NULL,
                  &RcBufferSize,
                  (VOID *)RcSetupData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupData);
    return Status;
  }
  CopyMem (gRcSUBrowser->SiSUBrowserData, RcSetupData, RcBufferSize);
  FreePool (RcSetupData);
  //==========================================//
  // Restore custom ME setup storage variable //
  //==========================================//
  RcBufferSize = sizeof (ME_SETUP_STORAGE);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  L"MeSetupStorageCustom",
                  &gMeSetupVariableGuid,
                  NULL,
                  &RcBufferSize,
                  (VOID *)RcSetupData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupData);
    return Status;
  }
  CopyMem (gRcSUBrowser->MeStorageSUBrowserData, RcSetupData, RcBufferSize);
  FreePool (RcSetupData);
  return EFI_SUCCESS;
}


/**
  To save current RC setup utility setting to user custom setting

  @param  None

  @retval EFI_SUCCESS            Function has completed successfully.
  @return Other                  Cannot save RC SetupUtility browser data.
**/
EFI_STATUS
SaveCustomRcOption (
  VOID
  )
{
  EFI_STATUS                                Status;
  UINT8                                     *RcSetupData = NULL;
  UINTN                                     RcBufferSize;

  //=================================//
  // Save custom SETUP DATA variable //
  //=================================//
  RcBufferSize = sizeof (SETUP_DATA);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  CopyMem (RcSetupData, gRcSUBrowser->SetupDataSUBrowserData, RcBufferSize);
  Status = gRT->SetVariable (
                  L"Custom",
                  &gSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  RcBufferSize,
                  (VOID *)RcSetupData
                  );
  FreePool (RcSetupData);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //===============================//
  // Save custom SA setup variable //
  //===============================//
  RcBufferSize = sizeof (SA_SETUP);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  CopyMem (RcSetupData, gRcSUBrowser->SaSUBrowserData, RcBufferSize);
  Status = gRT->SetVariable (
                  L"Custom",
                  &gSaSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  RcBufferSize,
                  (VOID *)RcSetupData
                  );
  FreePool (RcSetupData);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //===============================//
  // Save custom ME setup variable //
  //===============================//
  RcBufferSize = sizeof (ME_SETUP);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  CopyMem (RcSetupData, gRcSUBrowser->MeSUBrowserData, RcBufferSize);
  Status = gRT->SetVariable (
                  L"Custom",
                  &gMeSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  RcBufferSize,
                  (VOID *)RcSetupData
                  );
  FreePool (RcSetupData);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //================================//
  // Save custom CPU setup variable //
  //================================//
  RcBufferSize = sizeof (CPU_SETUP);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  CopyMem (RcSetupData, gRcSUBrowser->CpuSUBrowserData, RcBufferSize);
  Status = gRT->SetVariable (
                  L"Custom",
                  &gCpuSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  RcBufferSize,
                  (VOID *)RcSetupData
                  );
  FreePool (RcSetupData);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //================================//
  // Save custom PCH setup variable //
  //================================//
  RcBufferSize = sizeof (PCH_SETUP);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  CopyMem (RcSetupData, gRcSUBrowser->PchSUBrowserData, RcBufferSize);
  Status = gRT->SetVariable (
                  L"Custom",
                  &gPchSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  RcBufferSize,
                  (VOID *)RcSetupData
                  );
  FreePool (RcSetupData);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //================================//
  // Save custom SI setup variable //
  //================================//
  RcBufferSize = sizeof (SI_SETUP);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  CopyMem (RcSetupData, gRcSUBrowser->SiSUBrowserData, RcBufferSize);
  Status = gRT->SetVariable (
                  L"Custom",
                  &gSiSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  RcBufferSize,
                  (VOID *)RcSetupData
                  );
  FreePool (RcSetupData);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //=======================================//
  // Save custom ME setup storage variable //
  //=======================================//
  RcBufferSize = sizeof (ME_SETUP_STORAGE);
  RcSetupData = AllocateZeroPool (RcBufferSize);
  if (RcSetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  CopyMem (RcSetupData, gRcSUBrowser->MeStorageSUBrowserData, RcBufferSize);
  Status = gRT->SetVariable (
                  L"MeSetupStorageCustom",
                  &gMeSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  RcBufferSize,
                  (VOID *)RcSetupData
                  );
  FreePool (RcSetupData);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  return EFI_SUCCESS;
}
