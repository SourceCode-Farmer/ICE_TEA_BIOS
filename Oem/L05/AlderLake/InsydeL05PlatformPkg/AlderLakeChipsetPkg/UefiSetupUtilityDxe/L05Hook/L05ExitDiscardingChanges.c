/** @file
  Source file for L05Hook for Exit Discarding Changes.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SetupUtility.h>
#include <Library/BaseLib.h>
#include <Protocol/H2OFormBrowser.h>
#include <Protocol/L05BootOption.h>

EFI_L05_RECORD_CHANGED_QUESTION_DATA    *mL05RecordChangedQuestionData = NULL;
UINT32                                  mChangedQuestionCount = 0;

/**
  Frees record changed question data.

  @param  None.

  @retval None.
**/
VOID
L05FreeRecordChangedQuestionData (
  VOID
  )
{
  UINTN                                 Index;

  if (mL05RecordChangedQuestionData != NULL && mChangedQuestionCount != 0) {
    for (Index = 0; Index < mChangedQuestionCount; Index++) {
      if (mL05RecordChangedQuestionData[Index].Prompt == NULL) {
        continue;
      }

      FreePool (mL05RecordChangedQuestionData[Index].Prompt);
    }

      FreePool (mL05RecordChangedQuestionData);
      mL05RecordChangedQuestionData = NULL;
      mChangedQuestionCount = 0;
  }

  return;
}

/**
  Record changed question prompt for first time into SCU.

  @param  None.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
L05RecordChangedQuestionPrompt (
  VOID
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_PROTOCOL             *FBProtocol;
  UINT32                                ChangedQuestionCount;
  H2O_FORM_BROWSER_Q                    *ChangedQuestionBuffer;
  UINTN                                 Index;

  //
  // Frees record changed question data.
  //
  L05FreeRecordChangedQuestionData ();

  Status = gBS->LocateProtocol (
                  &gH2OFormBrowserProtocolGuid,
                  NULL,
                  (VOID **) &FBProtocol
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = FBProtocol->GetChangedQuestions (FBProtocol, &ChangedQuestionCount, &ChangedQuestionBuffer);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  mL05RecordChangedQuestionData = AllocateZeroPool (ChangedQuestionCount * sizeof (EFI_L05_RECORD_CHANGED_QUESTION_DATA));
  mChangedQuestionCount = ChangedQuestionCount;

  for (Index = 0; Index < ChangedQuestionCount; Index++) {
    if (ChangedQuestionBuffer[Index].Prompt == NULL) {
      continue;
    }

    mL05RecordChangedQuestionData[Index].Prompt = AllocateCopyPool (StrSize (ChangedQuestionBuffer[Index].Prompt), ChangedQuestionBuffer[Index].Prompt);
    CopyMem (&mL05RecordChangedQuestionData[Index].HiiValue, &ChangedQuestionBuffer[Index].HiiValue, sizeof (EFI_HII_VALUE));
  }

  return Status;
}

/**
  Is Setup item changed.

  @param  MyIfrNVData                   A pointer to CHIPSET_CONFIGURATION.

  @retval TRUE                          Setup item has changed.
  @retval FALSE                         Setup item has no changed.
**/
BOOLEAN
L05IsSetupItemChanged (
  CHIPSET_CONFIGURATION                 *MyIfrNVData
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_PROTOCOL             *FBProtocol;
  UINT32                                ChangedQuestionCount;
  H2O_FORM_BROWSER_Q                    *ChangedQuestionBuffer;
  UINTN                                 Index;
  UINTN                                 Index2;
  BOOLEAN                               SkipPrompt;

  Status = gBS->LocateProtocol (
                  &gH2OFormBrowserProtocolGuid,
                  NULL,
                  (VOID **) &FBProtocol
                  );

  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  Status = FBProtocol->GetChangedQuestions (FBProtocol, &ChangedQuestionCount, &ChangedQuestionBuffer);

  //
  // Compare count of changed question with first time record.
  //
  if (mChangedQuestionCount != ChangedQuestionCount) {
    return TRUE;
  }

  if (Status == EFI_NOT_FOUND) {
    return FALSE;
  }

  for (Index = 0; Index < ChangedQuestionCount; Index++) {

    if ((mL05RecordChangedQuestionData != NULL) && (ChangedQuestionBuffer[Index].Prompt != NULL)) {
      //
      // Compare this prompt with first time record.
      //
      SkipPrompt = FALSE;

      for (Index2 = 0; Index2 < mChangedQuestionCount; Index2++) {
        if ((mL05RecordChangedQuestionData[Index].Prompt != NULL) &&
            (StrCmp (ChangedQuestionBuffer[Index].Prompt, mL05RecordChangedQuestionData[Index2].Prompt) == 0) &&
            (CompareMem (&ChangedQuestionBuffer[Index].HiiValue, &mL05RecordChangedQuestionData[Index2].HiiValue, sizeof (EFI_HII_VALUE)) == 0)) {
          SkipPrompt = TRUE;
          break;
        }
      }

      if (SkipPrompt) {
        //
        // Skip this prompt when changed question prompt the same as first time record.
        //
        continue;
      }
    }

    if ((ChangedQuestionBuffer[Index].Prompt != NULL) && (StrLen (ChangedQuestionBuffer[Index].Prompt) != 0)) {
      return TRUE;
    }
  }

  //
  // Check TPM clear changed.
  //
  if ((MyIfrNVData->TpmClear != 0) || (MyIfrNVData->Tpm2Operation != 0)) {
    return TRUE;
  }

  return FALSE;
}

/**
  Is Boot Order changed.

  @param  None.

  @retval TRUE                          Boot Order has changed.
  @retval FALSE                         Boot Order has no changed.
**/
BOOLEAN
L05IsBootOrderChanged (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINT16                                *BootOrder;
  UINTN                                 BootOrderSize;
  UINT16                                BootDeviceNum;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;

  BootOrder     = NULL;
  BootOrderSize = 0;
  SUCInfo       = NULL;

  Status = CommonGetVariableDataAndSize (
             L"BootOrder",
             &gEfiGlobalVariableGuid,
             &BootOrderSize,
             (VOID **) &BootOrder
             );

  if (EFI_ERROR (Status) || (BootOrderSize == 0)) {
    return FALSE;
  }

  BootDeviceNum = (UINT16) (BootOrderSize / sizeof (UINT16));

#ifdef L05_ONE_KEY_RECOVERY_ENABLE
{
  EFI_L05_BOOT_OPTION_PROTOCOL          *L05BootOrderPtr;

  L05BootOrderPtr = NULL;

  Status = gBS->LocateProtocol (&gEfiL05BootOptionProtocolGuid, NULL, &L05BootOrderPtr);

  if (L05BootOrderPtr != NULL) {
    Status = L05BootOrderPtr->UpdateBootOrderForNovoRecovery (BootOrder, &BootDeviceNum, TRUE);
  }
}
#endif

  SUCInfo = gSUBrowser->SUCInfo;

  if (CompareMem (SUCInfo->BootOrder, BootOrder, BootDeviceNum * sizeof (UINT16)) == 0) {
    return FALSE;
  }

  return TRUE;
}

/**
  Is Password changed.

  @param  None.

  @retval TRUE                          Password has changed.
  @retval FALSE                         Password has no changed.
**/
BOOLEAN
L05IsPasswordChanged (
  VOID
  )
{
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;

  SUCInfo = NULL;

  SUCInfo = gSUBrowser->SUCInfo;

  if ((SUCInfo->SupervisorPassword->Flag != NO_ACCESS_PASSWORD) ||
      (SUCInfo->UserPassword->Flag != NO_ACCESS_PASSWORD) ||
      ((SUCInfo->HddPasswordScuData->MasterFlag != NO_ACCESS_PASSWORD) && (SUCInfo->HddPasswordScuData->MasterFlag != ENABLE_PASSWORD) && (SUCInfo->HddPasswordScuData->MasterFlag != 0)) ||
      ((SUCInfo->HddPasswordScuData->Flag != NO_ACCESS_PASSWORD) && (SUCInfo->HddPasswordScuData->Flag != ENABLE_PASSWORD) && (SUCInfo->HddPasswordScuData->Flag != 0))) {
    return TRUE;
  }

  return FALSE;
}

