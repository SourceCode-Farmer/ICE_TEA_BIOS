/** @file
  Source file for L05Hook for Exit Discarding Changes.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_EXIT_DISCARDING_CHANGES_H_
#define _L05_EXIT_DISCARDING_CHANGES_H_

#include <Protocol/H2OFormBrowser.h>

typedef struct {
  CHAR16                                *Prompt;
  EFI_HII_VALUE                         HiiValue;
} EFI_L05_RECORD_CHANGED_QUESTION_DATA;

EFI_STATUS
L05RecordChangedQuestionPrompt (
  VOID
  );

BOOLEAN
L05IsSetupItemChanged (
  CHIPSET_CONFIGURATION                 *MyIfrNVData
  );

BOOLEAN
L05IsBootOrderChanged (
  VOID
  );

BOOLEAN
L05IsPasswordChanged (
  VOID
  );

#endif
