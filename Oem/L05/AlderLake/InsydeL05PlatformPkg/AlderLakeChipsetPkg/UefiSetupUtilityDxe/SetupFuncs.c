/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2022, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SetupFuncs.h>
#include <Library/DxeOemSvcKernelLib.h>
#include <MeBiosPayloadData.h>
#include <Guid/HobList.h>
#include <Guid/TpmInstance.h>
#include <CpuRegs.h>
//[-start-200907-IB16740115-remove]// CpuAccess.h has removed in RC1362.
//#include <CpuAccess.h>
//[-end-200907-IB16740115-remove]//
#include <Library/BaseSetupDefaultLib.h>
#include <TxtInfoHob.h>
#include <PcieRegs.h>
#include <Library/PchInfoLib.h>
#include <Library/MultiConfigBaseLib.h>
#include <SetupConfig.h>
#include <Library/BaseRcSetupDefaultLib.h>
#include <Library/MmPciLib.h>
#include <Register/Cpuid.h>
#include <Library/DxeOemSvcChipsetLib.h>
#include <Register/PchRegs.h>
#include <Register/PchPcieRpRegs.h>

#if FixedPcdGetBool(PcdITbtEnable) == 1
#include <TcssDataHob.h>
#endif

#include <Library/HobLib.h>
#include <VmdInfoHob.h>
#include <Library/VmdInfoLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#ifdef L05_ONE_KEY_RECOVERY_ENABLE
#include <Library/CmosLib.h>
#include <OemCmos.h>
#endif
//[-start-211229-YUNLEI0155-add]//
#if defined(C770_SUPPORT) 
#include <Library/OemSvcLfcPeiGetBoardID.h>
#endif
//[-end-211229-YUNLEI0155-add]//

//extern BOOLEAN mGetSetupNvDataFailed;
//extern EFI_GUID gSetupDefaultHobGuid;

/**
 Given a token, return the string.

 @param [in]   HiiHandle        the handle the token is located
 @param [in]   Token            the string reference
 @param [in]   LanguageString   indicate what language string we want to get. if this is a
                                NULL pointer, using the current language setting to get string

 @retval *CHAR16                Returns the string corresponding to the token
 @retval NULL                   Cannot get string from Hii database

**/
CHAR16 *
GetTokenStringByLanguage (
  IN EFI_HII_HANDLE                             HiiHandle,
  IN EFI_STRING_ID                              Token,
  IN CHAR8                                     *LanguageString
  )
{
  CHAR16                                      *Buffer;
  UINTN                                       BufferLength;
  EFI_STATUS                                  Status;
  EFI_HII_STRING_PROTOCOL                     *HiiString;

  HiiString = gSUBrowser->HiiString;
  //
  // Set default string size assumption at no more than 256 bytes
  //
  BufferLength = 0x100;

  Status = gBS->AllocatePool (
                  EfiBootServicesData,
                  BufferLength,
                  (VOID **)&Buffer
                  );
  if (EFI_ERROR (Status)) {
    return NULL;
  }
  ZeroMem (Buffer, BufferLength);
  Status = HiiString->GetString (
                        HiiString,
                        LanguageString,
                        HiiHandle,
                        Token,
                        Buffer,
                        &BufferLength,
                        NULL
                        );
  if (EFI_ERROR (Status)) {
    if (Status == EFI_BUFFER_TOO_SMALL) {
      //
      // Free the old pool
      //
      gBS->FreePool (Buffer);

      //
      // Allocate new pool with correct value
      //
      gBS->AllocatePool (
             EfiBootServicesData,
             BufferLength,
             (VOID **)&Buffer
             );
      Status = HiiString->GetString (
                            HiiString,
                            LanguageString,
                            HiiHandle,
                            Token,
                            Buffer,
                            &BufferLength,
                            NULL
                            );
      if (!EFI_ERROR (Status)) {
        //
        // return searched string
        //
        return Buffer;
      }
    }
    //
    // Cannot find string, free buffer and return NULL pointer
    //
    gBS->FreePool (Buffer);
    Buffer = NULL;
    return Buffer;
  }
  //
  // return searched string
  //
  return Buffer;
}


EFI_STATUS
CheckLanguage (
  VOID
  )
{
  EFI_STATUS                            Status;
  CHAR8                                 *LangCode;
  UINT8                                 *BackupLang;
  UINTN                                 BufferSize;
  UINTN                                 BackupBufferSize;


  LangCode   = GetVariableAndSize (L"PlatformLang", &gEfiGlobalVariableGuid, &BufferSize);
  BackupLang = GetVariableAndSize (L"BackupPlatformLang", &gEfiGenericVariableGuid, &BackupBufferSize);
  if (LangCode == NULL || !SetupUtilityLibIsLangCodeSupport (LangCode)) {
    //
    // if cannot find current language, set default language as english
    //
    if (BackupLang == NULL || !SetupUtilityLibIsLangCodeSupport (BackupLang)) {
      Status = gRT->SetVariable (
                      L"PlatformLang",
                      &gEfiGlobalVariableGuid,
                      EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                      AsciiStrSize ((CHAR8 *) PcdGetPtr (PcdUefiVariableDefaultPlatformLang)),
                      (VOID *) PcdGetPtr (PcdUefiVariableDefaultPlatformLang)
                      );
    } else {
      Status = gRT->SetVariable (
                      L"PlatformLang",
                      &gEfiGlobalVariableGuid,
                      EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                      BackupBufferSize,
                      BackupLang
                      );
    }

    gRT->SetVariable (
           L"BackupPlatformLang",
           &gEfiGenericVariableGuid,
           EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
           0,
           NULL
           );
    if (LangCode != NULL) {
      gBS->FreePool (LangCode);
    }
    if (BackupLang != NULL) {
      gBS->FreePool (BackupLang);
    }
    return Status;
  }

  if (BackupLang != NULL) {
    if (BackupBufferSize != BufferSize || CompareMem (BackupLang, LangCode, BufferSize)) {
      Status = gRT->SetVariable (
                      L"PlatformLang",
                      &gEfiGlobalVariableGuid,
                      EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                      BackupBufferSize,
                      BackupLang
                      );
    }

    Status = gRT->SetVariable (
                    L"BackupPlatformLang",
                    &gEfiGenericVariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    0,
                    NULL
                    );
    gBS->FreePool (BackupLang);
  }
  gBS->FreePool (LangCode);
  return EFI_SUCCESS;
}

/**
 Read the EFI variable (VendorGuid/Name) and return a dynamically allocated
 buffer, and the size of the buffer. If failure return NULL.

 Name           String part of EFI variable name
 VendorGuid     GUID part of EFI variable name
 VariableSize   Returns the size of the EFI variable that was read

 @return Dynamically allocated memory that contains a copy of the EFI variable.
 @return Caller is responsible freeing the buffer.
 @retval NULL                   Variable was not read

**/
VOID *
GetVariableAndSize (
  IN  CHAR16              *Name,
  IN  EFI_GUID            *VendorGuid,
  OUT UINTN               *VariableSize
  )
{
  EFI_STATUS  Status;
  UINTN       BufferSize;
  VOID        *Buffer;

  Buffer = NULL;

  //
  // Pass in a zero size buffer to find the required buffer size.
  //
  BufferSize  = 0;

  Status      = gRT->GetVariable (Name, VendorGuid, NULL, &BufferSize, Buffer);
  if (Status == EFI_BUFFER_TOO_SMALL) {
    //
    // Allocate the buffer to return
    //

    Buffer = AllocateZeroPool (BufferSize);
    if (Buffer == NULL) {
      return NULL;
    }

    //
    // Read variable into the allocated buffer.
    //

    Status = gRT->GetVariable (Name, VendorGuid, NULL, &BufferSize, Buffer);
    if (EFI_ERROR (Status)) {
      BufferSize = 0;
      gBS->FreePool (Buffer);
      Buffer = NULL;
    }

  }

  *VariableSize = BufferSize;

  return Buffer;
}

/**
 Function to update the ATA strings into Model Name -- Size

 @param [in]        IdentifyDriveInfo
 @param [in, out]   NewString

 @return Will return model name and size (or ATAPI if nonATA)

**/
EFI_STATUS
UpdateAtaString (
  IN      EFI_ATAPI_IDENTIFY_DATA     *IdentifyDriveInfo,
  IN OUT  CHAR16                      **NewString
  )
{
  CHAR8                       *TempString;
  UINT16                      Index;
  CHAR8                       Temp8;

  TempString = AllocateZeroPool (0x100);
  ASSERT (TempString != NULL);
  if (TempString == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  CopyMem (TempString, IdentifyDriveInfo->ModelName, sizeof (IdentifyDriveInfo->ModelName));

  //
  // Swap the IDE string since Identify Drive format is inverted
  //
  Index = 0;
  while (TempString[Index] != 0 && TempString[Index+1] != 0) {
    Temp8 = TempString[Index];
    TempString[Index] = TempString[Index+1];
    TempString[Index+1] = Temp8;
    Index +=2;
  }
  AsciiToUnicode (TempString, *NewString);

  if (TempString != NULL) {
    gBS->FreePool(TempString);
  }

  return EFI_SUCCESS;
}

/**
 Function to convert ASCII string to Unicode

 @param [in]   AsciiString
 @param [in]   UnicodeString


**/
EFI_STATUS
AsciiToUnicode (
  IN    CHAR8     *AsciiString,
  IN    CHAR16    *UnicodeString
  )
{
  UINT8           Index;

  Index = 0;
  while (AsciiString[Index] != 0) {
    UnicodeString[Index] = (CHAR16)AsciiString[Index];
    Index++;
  }
  UnicodeString[Index] = 0;

  return EFI_SUCCESS;
}

/**
 Control Event Timer start and end.

 @param [in]   Timeout          Event time, Zero is Stop the timer

 @retval EFI_SUCCESS            Control Event Timer is success.

**/
EFI_STATUS
EventTimerControl (
  IN UINT64                     Timeout
  )
{

  //
  // If the system health event is create for this Key value ,we can stop event
  //
  if (gSCUSystemHealth == HAVE_CREATE_SYSTEM_HEALTH_EVENT) {
    //
    // Set the timer event
    //
    gBS->SetTimer (
              gSCUTimerEvent,
              TimerPeriodic,
              Timeout
              );
  }

  return EFI_SUCCESS;

}

/**
 According Bus, Device, Function, PrimarySecondary, SlaveMaster to get corresponding
 SATA port number

 @param [in]   Bus              PCI bus number
 @param [in]   Device           PCI device number
 @param [in]   Function         PCI function number
 @param [in]   PrimarySecondary  primary or scondary
 @param [in]   SlaveMaster      slave or master
 @param [in, out] PortNum       An address to save SATA port number.

 @retval EFI_SUCCESS            get corresponding port number successfully
 @retval EFI_INVALID_PARAMETER  input parameter is invalid
 @retval EFI_NOT_FOUND          can't get corresponding port number

**/
EFI_STATUS
SearchMatchedPortNum (
  IN     UINT32                              Bus,
  IN     UINT32                              Device,
  IN     UINT32                              Function,
  IN     UINT8                               PrimarySecondary,
  IN     UINT8                               SlaveMaster,
  IN OUT UINTN                               *PortNum
  )
{
  UINTN                 Index;
  PORT_NUMBER_MAP       *PortMappingTable;
  UINTN                 NoPorts;
  PORT_NUMBER_MAP                   EndEntry;

  PortMappingTable      = NULL;

  ZeroMem (&EndEntry, sizeof (PORT_NUMBER_MAP));

  PortMappingTable = (PORT_NUMBER_MAP *)PcdGetPtr (PcdPortNumberMapTable);

  NoPorts = 0;
  while (CompareMem (&EndEntry, &PortMappingTable[NoPorts], sizeof (PORT_NUMBER_MAP)) != 0) {
    NoPorts++;
  }

  if (NoPorts == 0) {
    return EFI_NOT_FOUND;
  }

  for (Index = 0; Index < NoPorts; Index++) {
    if ((PortMappingTable[Index].Bus == Bus) &&
        (PortMappingTable[Index].Device == Device) &&
        (PortMappingTable[Index].Function == Function) &&
        (PortMappingTable[Index].PrimarySecondary == PrimarySecondary) &&
        (PortMappingTable[Index].SlaveMaster == SlaveMaster)) {
      *PortNum = PortMappingTable[Index].PortNum;
      return EFI_SUCCESS;

    }
  }
  return EFI_NOT_FOUND;
}

/**
 Check this SATA port number is whther support

 @param [in]   PortNum          SATA port number

 @retval EFI_SUCCESS            platform supports this SATA port number
 @retval EFI_UNSUPPORTED        platform unsupports this SATA port number

**/
EFI_STATUS
CheckSataPort (
  IN UINTN                                  PortNum
)
{
  UINTN                 Index;
  PORT_NUMBER_MAP       *PortMappingTable;
  UINTN                 NoPorts;
  PORT_NUMBER_MAP                   EndEntry;

  PortMappingTable      = NULL;

  ZeroMem (&EndEntry, sizeof (PORT_NUMBER_MAP));

  PortMappingTable = (PORT_NUMBER_MAP *)PcdGetPtr (PcdPortNumberMapTable);

  NoPorts = 0;
  while (CompareMem (&EndEntry, &PortMappingTable[NoPorts], sizeof (PORT_NUMBER_MAP)) != 0) {
    NoPorts++;
  }
  if (NoPorts == 0) {
    return EFI_UNSUPPORTED;;
  }

  for (Index = 0; Index < NoPorts; Index++) {
    if (PortMappingTable[Index].PortNum == PortNum) {
      return EFI_SUCCESS;
    }
  }
  return EFI_UNSUPPORTED;
}


/**
 Save Setup Configuration to NV storage and call KERNEL_CALCULATE_WRITE_CMOS_CHECKSUM
 OEM service

 @param [in]   VariableName     The name of variable to load
 @param [in]   VendorGuid       It's vendor GUID
 @param [in]   Attributes       attributes of the data to be stored
 @param [in]   DataSize         The size of the data
 @param [in]   Buffer           Space to store data to be written to NVRam

 @retval EFI_SUCCESS            Save Setup Configuration successful
 @retval EFI_INVALID_PARAMETER  The input parameter is invalid
 @return Other                  Some other error occured

**/
EFI_STATUS
SaveSetupConfig (
  IN     CHAR16             *VariableName,
  IN     EFI_GUID           *VendorGuid,
  IN     UINT32             Attributes,
  IN     UINTN              DataSize,
  IN     VOID               *Buffer
  )
{
  EFI_STATUS                Status;


  if (Buffer == NULL || VariableName == NULL || VendorGuid == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = SetVariableToSensitiveVariable (
                  VariableName,
                  VendorGuid,
                  Attributes,
                  DataSize,
                  Buffer
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  //Status = OemServices->Funcs[KERNEL_CALCULATE_WRITE_CMOS_CHECKSUM] (
  //Status = OemServices->Funcs[COMMON_CALCULATE_WRITE_CMOS_CHECKSUM] (
  //                        OemServices,
  //                        COMMON_CALCULATE_WRITE_CMOS_CHECKSUM_ARG_COUNT
  //                        );
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcCalculateWriteCmosChecksum \n"));
  Status = OemSvcCalculateWriteCmosChecksum ();
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcCalculateWriteCmosChecksum Status: %r\n", Status));

  return EFI_SUCCESS;
}

/**
 Get string from input HII handle and add this string

 @param [in]   InputHiiHandle
 @param [in]   OutputHiiHandle
 @param [in]   InputToken
 @param [out]  OutputToken

 @retval EFI_SUCCESS            Update system bus speed success

**/
EFI_STATUS
AddNewString (
  IN   EFI_HII_HANDLE           InputHiiHandle,
  IN   EFI_HII_HANDLE           OutputHiiHandle,
  IN   EFI_STRING_ID            InputToken,
  OUT  EFI_STRING_ID            *OutputToken
  )
{
  UINTN                      LanguageCount;
  UINTN                      TotalLanguageCount;
  CHAR8                     *LanguageString;
  CHAR16                     *TokenString;
  EFI_STATUS                 Status;
  EFI_STRING_ID              AddedToken;


  //
  // Get all of supported language
  //
  Status = GetLangDatabase (&TotalLanguageCount, &LanguageString);
    if (EFI_ERROR (Status)) {
    return Status;
  }

  AddedToken = 0;
  //
  // Create new string token for this string
  //
  TokenString=HiiGetString (InputHiiHandle, InputToken, NULL);
  ASSERT (TokenString != NULL);
  AddedToken=HiiSetString (OutputHiiHandle, 0, TokenString, NULL);
  gBS->FreePool (TokenString);
  //
  // according to different language to get string token from input HII handle and
  // add this string to oupt string hanlde
  //
  for (LanguageCount = 0; LanguageCount < TotalLanguageCount; LanguageCount++) {
    TokenString = GetTokenStringByLanguage (
                    InputHiiHandle,
                    InputToken,
                    &LanguageString[LanguageCount * RFC_3066_ENTRY_SIZE]
                    );
    if (TokenString != NULL) {
      Status = gSUBrowser->HiiString->SetString (
                                        gSUBrowser->HiiString,
                                        OutputHiiHandle,
                                        AddedToken,
                                        &LanguageString[LanguageCount * RFC_3066_ENTRY_SIZE],
                                        TokenString,
                                        NULL
                                        );
      gBS->FreePool (TokenString);
    }
  }
  gBS->FreePool (LanguageString);
  *OutputToken = AddedToken;

  return EFI_SUCCESS;
}

#if 0
// Kernel Chipset coding here
// We write at RcSetupDefaultLib.c
/**
  Extract default SA Setup variable data from VFR forms

  @param[in,out]  SaSetupData                   A pointer to the SA Setup variable data buffer
  @param[in]      SaSetupDataSize               Data size in bytes of the SA Setup variable
*/
VOID
ExtractSaSetupDefault (
  IN OUT UINT8                                  *SaSetupData,
  IN     UINTN                                  SaSetupDataSize
  )
{
  UINT8                  SkuId;
  EFI_STATUS             Status;

  SkuId = (UINT8) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             SA_SETUP_VARIABLE_NAME,
             &gSaSetupVariableGuid,
             SkuId,
             NULL,
             &SaSetupDataSize,
             SaSetupData
             );
  if (Status != EFI_SUCCESS) {
    Status = CommonGetDefaultVariable (
               SA_SETUP_VARIABLE_NAME,
               &gSaSetupVariableGuid,
               0,
               NULL,
               &SaSetupDataSize,
               SaSetupData
               );
  }
  ASSERT (Status == EFI_SUCCESS);
}

/**
  Extract default ME Setup variable data from VFR forms

  @param[in,out]  MeSetupData                   A pointer to the ME Setup variable data buffer
  @param[in]      MeSetupDataSize               Data size in bytes of the ME Setup variable
*/
VOID
ExtractMeSetupDefault (
  IN OUT UINT8                                  *MeSetupData,
  IN     UINTN                                  MeSetupDataSize
  )
{
  UINT8                  SkuId;
  EFI_STATUS             Status;

  SkuId = (UINT8) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             ME_SETUP_VARIABLE_NAME,
             &gMeSetupVariableGuid,
             SkuId,
             NULL,
             &MeSetupDataSize,
             MeSetupData
             );
  if (Status != EFI_SUCCESS) {
    Status = CommonGetDefaultVariable (
               ME_SETUP_VARIABLE_NAME,
               &gMeSetupVariableGuid,
               0,
               NULL,
               &MeSetupDataSize,
               MeSetupData
               );
  }
  ASSERT (Status == EFI_SUCCESS);
}

/**
  Extract default CPU Setup variable data from VFR forms

  @param[in,out]  CpuSetupData                  A pointer to the CPU Setup variable data buffer
  @param[in]      CpuSetupDataSize              Data size in bytes of the CPU Setup variable
*/
VOID
ExtractCpuSetupDefault (
  IN OUT UINT8                                  *CpuSetupData,
  IN     UINTN                                  CpuSetupDataSize
  )
{
  UINT8                  SkuId;
  EFI_STATUS             Status;

  SkuId = (UINT8) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             CPU_SETUP_VARIABLE_NAME,
             &gCpuSetupVariableGuid,
             SkuId,
             NULL,
             &CpuSetupDataSize,
             CpuSetupData
             );
  if (Status != EFI_SUCCESS) {
    Status = CommonGetDefaultVariable (
               CPU_SETUP_VARIABLE_NAME,
               &gCpuSetupVariableGuid,
               0,
               NULL,
               &CpuSetupDataSize,
               CpuSetupData
               );
  }
  ASSERT (Status == EFI_SUCCESS);
}

/**
  Extract default PCH Setup variable data from VFR forms

  @param[in,out]  PchSetupData                  A pointer to the PCH Setup variable data buffer
  @param[in]      PchSetupDataSize              Data size in bytes of the PCH Setup variable
*/
VOID
ExtractPchSetupDefault (
  IN OUT UINT8                                  *PchSetupData,
  IN     UINTN                                  PchSetupDataSize
  )
{
  UINT8                  SkuId;
  EFI_STATUS             Status;

  SkuId = (UINT8) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             PCH_SETUP_VARIABLE_NAME,
             &gPchSetupVariableGuid,
             SkuId,
             NULL,
             &PchSetupDataSize,
             PchSetupData
             );
  if (Status != EFI_SUCCESS) {
    Status = CommonGetDefaultVariable (
               PCH_SETUP_VARIABLE_NAME,
               &gPchSetupVariableGuid,
               0,
               NULL,
               &PchSetupDataSize,
               PchSetupData
               );
  }
  ASSERT (Status == EFI_SUCCESS);
}

/**
  Extract default ME Setup storage variable data from VFR forms

  @param[in,out]  MeSetupStorageData            A pointer to the ME Setup storage variable data buffer
  @param[in]      MeSetupStorageDataSize        Data size in bytes of the ME Setup storage variable
*/
VOID
ExtractMeSetupStorageDefault (
  IN OUT UINT8                                  *MeSetupStorageData,
  IN     UINTN                                  MeSetupStorageDataSize
  )
{
  UINT8                  SkuId;
  EFI_STATUS             Status;

  SkuId = (UINT8) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             ME_SETUP_STORAGE_VARIABLE_NAME,
             &gMeSetupVariableGuid,
             SkuId,
             NULL,
             &MeSetupStorageDataSize,
             MeSetupStorageData
             );
  if (Status != EFI_SUCCESS) {
    Status = CommonGetDefaultVariable (
               ME_SETUP_STORAGE_VARIABLE_NAME,
               &gMeSetupVariableGuid,
               0,
               NULL,
               &MeSetupStorageDataSize,
               MeSetupStorageData
               );
  }
  ASSERT (Status == EFI_SUCCESS);
}
#endif

//_Start_L05_WORKAROUND_SETUP_DEFAULT_
VOID
RollbackSetupNvData (
  IN  SYSTEM_CONFIGURATION              *BackupSetupNvData,
  OUT SYSTEM_CONFIGURATION              *SetupNvData
  )
{
  //
  // Tpm
  //
  SetupNvData->TpmDeviceOk     = BackupSetupNvData->TpmDeviceOk;
  SetupNvData->Tpm2DeviceOk    = BackupSetupNvData->Tpm2DeviceOk;

  //
  // Password
  //
  SetupNvData->SupervisorFlag  = BackupSetupNvData->SupervisorFlag;
  SetupNvData->UserFlag        = BackupSetupNvData->UserFlag;
  SetupNvData->SetUserPass     = BackupSetupNvData->SetUserPass;

  //
  // Other
  //
  SetupNvData->DeviceExist     = BackupSetupNvData->DeviceExist;
  SetupNvData->L05MemorySizeGb = BackupSetupNvData->L05MemorySizeGb;

  //
  // Keep L05SetupVariableValid setting will not be modified
  //
  SetupNvData->L05SetupVariableValid = BackupSetupNvData->L05SetupVariableValid;

  //
  // L05 Secure Boot related items
  //
  SetupNvData->L05PlatformMode   = BackupSetupNvData->L05PlatformMode;
  SetupNvData->L05SecureBootMode = BackupSetupNvData->L05SecureBootMode;

  //
  // L05 setup related items
  //
  SetupNvData->L05SetHddPasswordFlag = BackupSetupNvData->L05SetHddPasswordFlag;
  SetupNvData->L05TpmLockDone        = BackupSetupNvData->L05TpmLockDone;
}
//_End_L05_WORKAROUND_SETUP_DEFAULT_

/**
 Build a default variable value an save to a buffer according to platform requirement

 @param [out]  SetupNvData

 @retval EFI_SUCCESS            Build default setup successful.
 @retval EFI_INVALID_PARAMETER  Input value is invalid.

**/
EFI_STATUS
DefaultSetup (
  OUT CHIPSET_CONFIGURATION            *SetupNvData
  )
{
  EFI_GUID                             SetupVariableGuidId = SYSTEM_CONFIGURATION_GUID;
//_Start_L05_SETUP_MENU_
  UINT8                                L05OsDefaultType;
//_End_L05_SETUP_MENU_
//_Start_L05_WORKAROUND_SETUP_DEFAULT_
  SYSTEM_CONFIGURATION                 *BackupSetupNvData;
//_End_L05_WORKAROUND_SETUP_DEFAULT_

  if (SetupNvData == NULL) {
    return EFI_INVALID_PARAMETER;
  }

//_Start_L05_SETUP_MENU_
  //
  //Save L05OsOptimizedDefault before load default
  //
  L05OsDefaultType = ((SYSTEM_CONFIGURATION *) SetupNvData)->L05OsOptimizedDefault;
//_End_L05_SETUP_MENU_

//_Start_L05_WORKAROUND_SETUP_DEFAULT_
  BackupSetupNvData = AllocateZeroPool (sizeof (SYSTEM_CONFIGURATION));
  CopyMem (BackupSetupNvData, SetupNvData, sizeof (SYSTEM_CONFIGURATION));
//_End_L05_WORKAROUND_SETUP_DEFAULT_

  ExtractSetupDefault ((UINT8 *) SetupNvData );
  if (FeaturePcdGet (PcdMultiConfigSupported)) {
    SetSCUDataFromMC (L"Setup", &SetupVariableGuidId, SETUP_FOR_LOAD_DEFAULT, (VOID *)SetupNvData, sizeof (SYSTEM_CONFIGURATION));
  }

//_Start_L05_WORKAROUND_SETUP_DEFAULT_
  RollbackSetupNvData (BackupSetupNvData, (SYSTEM_CONFIGURATION *) SetupNvData);
  gBS->FreePool (BackupSetupNvData);
//_End_L05_WORKAROUND_SETUP_DEFAULT_

  SetupNvData->DefaultBootType   = DEFAULT_BOOT_FLAG;
  SetupNvData->Timeout = (UINT16) PcdGet16 (PcdPlatformBootTimeOut);
  GetLangIndex ((CHAR8 *) PcdGetPtr (PcdUefiVariableDefaultPlatformLang), &SetupNvData->Language);
//  SetupNvData->NumCores   = (UINT8) RShiftU64 (AsmReadMsr64 (MSR_CORE_THREAD_COUNT), N_CORE_COUNT_OFFSET);

//_Start_L05_SETUP_MENU_
  //
  //Restore L05OsOptimizedDefault when do load default
  //
  ((SYSTEM_CONFIGURATION *) SetupNvData)->L05OsOptimizedDefault = L05OsDefaultType;
//_End_L05_SETUP_MENU_

  return EFI_SUCCESS;
}

/**
 Build a default SETUP_DATA variable value an save to a buffer according to platform requirement

 @param[out]  SetupDataNvData          A pointer of a pointer to SETUP_DATA variable default value
                                       for DXE recovery between different size of SETUP_DATA.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultSetupData (
  OUT SETUP_DATA                         **SetupDataNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  )
{
  if (*SetupDataNvData == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ExtractSetupDataDefault ((UINT8 **) SetupDataNvData, sizeof (SETUP_DATA), PoolType);
  if (FeaturePcdGet (PcdMultiConfigSupported)) {
    SetSCUDataFromMC (PLATFORM_SETUP_VARIABLE_NAME, &gSetupVariableGuid, SETUP_FOR_LOAD_DEFAULT, (VOID *)(*SetupDataNvData), sizeof (SETUP_DATA));
  }

  return EFI_SUCCESS;
}

/**
 Build a default SA variable value an save to a buffer according to platform requirement

 @param[out]  SaSetupNvData            A pointer of a pointer to SA_SETUP variable default value
                                       for DXE recovery between different size of SA_SETUP.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultSaSetup (
  OUT SA_SETUP                           **SaSetupNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  )
{

  if (*SaSetupNvData == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ExtractSaSetupDefault ((UINT8 **) SaSetupNvData, sizeof (SA_SETUP), PoolType);
  if (FeaturePcdGet (PcdMultiConfigSupported)) {
    SetSCUDataFromMC (SA_SETUP_VARIABLE_NAME, &gSaSetupVariableGuid, SETUP_FOR_LOAD_DEFAULT, (VOID *)(*SaSetupNvData), sizeof (SA_SETUP));
  }

  return EFI_SUCCESS;
}


/**
 Build a default ME variable value an save to a buffer according to platform requirement

 @param[out]  MeSetupNvData            A pointer of a pointer to ME_SETUP variable default value
                                       for DXE recovery between different size of ME_SETUP.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultMeSetup (
  OUT ME_SETUP                           **MeSetupNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  )
{

  if (*MeSetupNvData == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ExtractMeSetupDefault ((UINT8 **) MeSetupNvData, sizeof (ME_SETUP), PoolType);
  if (FeaturePcdGet (PcdMultiConfigSupported)) {
    SetSCUDataFromMC (ME_SETUP_VARIABLE_NAME, &gMeSetupVariableGuid, SETUP_FOR_LOAD_DEFAULT, (VOID *)(*MeSetupNvData), sizeof (ME_SETUP));
  }

  return EFI_SUCCESS;
}

/**
 Build a default CPU variable value an save to a buffer according to platform requirement

 @param[out]  CpuSetupNvData           A pointer of a pointer to CPU_SETUP variable default value
                                       for DXE recovery between different size of CPU_SETUP.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultCpuSetup (
  OUT CPU_SETUP                          **CpuSetupNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  )
{
  if (*CpuSetupNvData == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ExtractCpuSetupDefault ((UINT8 **) CpuSetupNvData, sizeof (CPU_SETUP), PoolType);
  if (FeaturePcdGet (PcdMultiConfigSupported)) {
    SetSCUDataFromMC (CPU_SETUP_VARIABLE_NAME, &gCpuSetupVariableGuid, SETUP_FOR_LOAD_DEFAULT, (VOID *)(*CpuSetupNvData), sizeof (CPU_SETUP));
  }
  return EFI_SUCCESS;
}


/**
 Build a default PCH variable value an save to a buffer according to platform requirement

 @param[out]  PchSetupNvData           A pointer of a pointer to PCH_SETUP variable default value
                                       for DXE recovery between different size of PCH_SETUP.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultPchSetup (
  OUT PCH_SETUP                          **PchSetupNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  )
{

  if (*PchSetupNvData == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ExtractPchSetupDefault ((UINT8 **) PchSetupNvData, sizeof (PCH_SETUP), PoolType);
  if (FeaturePcdGet (PcdMultiConfigSupported)) {
    SetSCUDataFromMC (PCH_SETUP_VARIABLE_NAME, &gPchSetupVariableGuid, SETUP_FOR_LOAD_DEFAULT, (VOID *)(*PchSetupNvData), sizeof (PCH_SETUP));
  }

  return EFI_SUCCESS;
}

/**
 Build a default SI variable value an save to a buffer according to platform requirement

 @param[out]  SiSetupNvData            A pointer of a pointer to SI_SETUP variable default value
                                       for DXE recovery between different size of SI_SETUP.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultSiSetup (
  OUT SI_SETUP                        **SiSetupNvData,
  IN  EFI_MEMORY_TYPE                 PoolType
  )
{
  if (*SiSetupNvData == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ExtractSiSetupDefault ((UINT8 **) SiSetupNvData, sizeof (SI_SETUP), PoolType);
  if (FeaturePcdGet (PcdMultiConfigSupported)) {
    SetSCUDataFromMC (SI_SETUP_VARIABLE_NAME, &gSiSetupVariableGuid, SETUP_FOR_LOAD_DEFAULT, (VOID *)(*SiSetupNvData), sizeof (SI_SETUP));
  }

  return EFI_SUCCESS;
}

//[-start-201127-IB17510127-add]//
/**
 Build a default ME_SETUP_STORAGE variable value an save to a buffer according to platform requirement

 @param[out]  MeSetupStorageNvData     A pointer of a pointer to ME_SETUP_STORAGE variable default value
                                       for DXE recovery between different size of ME_SETUP_STORAGE.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultMeSetupStorage (
  OUT ME_SETUP_STORAGE                **MeSetupStorageNvData,
  IN  EFI_MEMORY_TYPE                 PoolType
  )
{
  if (*MeSetupStorageNvData == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ExtractMeSetupStorageDefault ((UINT8 **) MeSetupStorageNvData, sizeof (ME_SETUP_STORAGE), PoolType);
  if (FeaturePcdGet (PcdMultiConfigSupported)) {
    SetSCUDataFromMC (ME_SETUP_STORAGE_VARIABLE_NAME, &gMeSetupVariableGuid, SETUP_FOR_LOAD_DEFAULT, (VOID *)(*MeSetupStorageNvData), sizeof (ME_SETUP_STORAGE));
  }

  return EFI_SUCCESS;
}
//[-end-201127-IB17510127-add]//

/**
 This function only updates the VMD volatile variable..
 @param [in,out]  SaSetupNvData          A pointer to SA setup NV data

 @retval          EFI_SUCCESS            Build default setup successful.
**/

EFI_STATUS
UpdateVMDVolatilevariables (
  IN OUT  SA_SETUP                     *SaSetupNvData
  )
{
  UINT32                                    Data32;
  VMD_INFO_HOB                              *VmdInfoHob;
  UINT8                                     Index;

  VmdInfoHob = NULL;
  // VMD: Reading CapId0_B for VMD support
  Data32 = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_CAPID0_B));
  DEBUG ((EFI_D_INFO, "CAPID0_B value is 0x%x\n", Data32));
  if (!(Data32 & V_SA_MC_CAPID0_B_VMD_DIS)) { // VMD is enabled

    SaSetupNvData->VmdSupported = 1;
    //
    // Update VMD Volatile variables
    //
    VmdInfoHob = (VMD_INFO_HOB *) GetFirstGuidHob (&gVmdInfoHobGuid);
    if (VmdInfoHob != NULL) {
      for (Index=0; Index < VMD_MAX_DEVICES; ++Index) {
#if FixedPcdGet8(PcdModifyVmdPortConfigViaScuDefault)
        if (SaSetupNvData->VmdPortDev[Index] != 0 ){
          SaSetupNvData->VmdPortPresent[Index] = 1;
        }
#else
        SaSetupNvData->VmdPortPresent[Index] = VmdInfoHob->VmdPortInfo.PortInfo[Index].DeviceDetected;
        SaSetupNvData->VmdPort[Index]        = VmdInfoHob->VmdPortInfo.PortInfo[Index].PortEn;
        // Also update the device and func
        SaSetupNvData->VmdPortDev[Index]     = VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev;
        SaSetupNvData->VmdPortFunc[Index]    = VmdInfoHob->VmdPortInfo.PortInfo[Index].RpFunc;
#endif
      }
      DEBUG ((EFI_D_INFO, "Vmd: Printing updated setup info\n"));
      for (Index=0; Index < VMD_MAX_DEVICES; ++Index) {
        if (SaSetupNvData->VmdPortPresent[Index]) { //print only if device is discovered
          DEBUG ((EFI_D_INFO, "Vmd For Rp %d VmdPortPresent is %d  VmdPort-En is %d  VmdPortDev is %d  VmdPortFunc is %d\n",
            Index, SaSetupNvData->VmdPortPresent[Index], SaSetupNvData->VmdPort[Index], SaSetupNvData->VmdPortDev[Index], SaSetupNvData->VmdPortFunc[Index] ));
        }
      }
    } else {
      DEBUG ((EFI_D_INFO, "Vmd Info Hob not found\n"));
    }
  }
  return EFI_SUCCESS;
}

/**
 This function only be entered once loading default required.

 @param [in,out]  SetupNvData            A pointer to Chipset configuration setup NV data
 @param [in,out]  SaSetupNvData          A pointer to SA setup NV data
 @param [in,out]  MeSetupNvData          A pointer to ME setup NV data
 @param [in,out]  CpuSetupNvData         A pointer to CPU setup NV data
 @param [in,out]  PchSetupNvData         A pointer to PCH setup NV data
 @param [in,out]  MeSetupStorageNvData   A pointer to ME setup storage NV data

 @retval          EFI_SUCCESS            Build default setup successful.
**/

EFI_STATUS
SetupRuntimeDetermination (
  IN OUT  CHIPSET_CONFIGURATION        *SetupNvData,
  IN EFI_STATUS                        GetSetupStatus,
  IN OUT  SA_SETUP                     *SaSetupNvData,
  IN EFI_STATUS                        GetSaSetupStatus,
  IN OUT  ME_SETUP                     *MeSetupNvData,
  IN EFI_STATUS                        GetMeSetupStatus,
  IN OUT  CPU_SETUP                    *CpuSetupNvData,
  IN EFI_STATUS                        GetCpuSetupStatus,
  IN OUT  PCH_SETUP                    *PchSetupNvData,
  IN EFI_STATUS                        GetPchSetupStatus,
  IN OUT  ME_SETUP_STORAGE             *MeSetupStorageNvData,
  IN EFI_STATUS                        GetMeSetupStorageStatus,
  IN OUT  SETUP_DATA                   *SetupDataNvData,
  IN EFI_STATUS                        GetSetupDataStatus,
  IN OUT  SI_SETUP                     *SiSetupNvData,
  IN EFI_STATUS                        GetSiSetupStatus
  )
{
  EFI_STATUS                           Status;
  MEM_INFO_PROTOCOL                    *MemoryInfo;
  UINT8                                     Index;
  //[-start-201127-IB17510127-add]//
  MEFWCAPS_SKU                         CurrentFeatures;
  AMT_WRAPPER_PROTOCOL                 *AmtWrapper;
  UINT32                               MsrValue;
  HECI_PROTOCOL                        *Heci;
  MEFWCAPS_SKU                         UserCapabilities;

  AmtWrapper   = NULL;
  Heci         = NULL;
//[-end-201127-IB17510127-add]//

  //=====================================================//
  // For SetupNvData                                     //
  //=====================================================//
  if (EFI_ERROR (GetSetupStatus)) {

    MeSetupNvData->MeImageType = MeFwTypeDetect ();

    if (IsPchLp()) {
      SetupNvData->PchType = LPC_PCH_TYPE_LP;
    } else {
      SetupNvData->PchType = LPC_PCH_TYPE_H;
    }

  }
  //=====================================================//
  // For SetupDataNvData                                   //
  //=====================================================//
  if (EFI_ERROR (GetSetupDataStatus)) {
    if (SetupDataNvData->Rtd3Support == 1) {
//[-start-211227-QINGLIN0135-modify]//
#ifdef LCFC_SUPPORT
      SetupDataNvData->StorageRtd3Support = 1; // D3Hot
#else
      SetupDataNvData->StorageRtd3Support = 2; // D3Cold
#endif
//[-end-211227-QINGLIN0135-modify]//
    } else {
      SetupDataNvData->StorageRtd3Support = 1; // D3Hot
    }
  }

  //=====================================================//
  // For SiSetupNvData                                   //
  //=====================================================//
  if (EFI_ERROR (GetSiSetupStatus)) {

  }
  //=====================================================//
  // For SaSetupNvData                                   //
  //=====================================================//
  if (EFI_ERROR (GetSaSetupStatus)) {

    Status = gBS->LocateProtocol (&gMemInfoProtocolGuid, NULL, (VOID **)&MemoryInfo);
    if (EFI_ERROR (Status)) {
      SaSetupNvData->DdrRefClk = 0;
      DEBUG ( ( DEBUG_ERROR | DEBUG_INFO, "Locate gMemInfoProtocolGuid Fail") );
    } else {
      SaSetupNvData->DdrRefClk = MemoryInfo->MemInfoData.RefClk;
    }

    SaSetupNvData->GtDid = MmioRead16 (MmPciBase (SA_MC_BUS, 2, 0) + 0x2);
    UpdateVMDVolatilevariables(SaSetupNvData);

    for (Index = 0; Index < GetMaxCpuPciePortNum(); Index++) {
      SaSetupNvData->PcieRootPortMultiVcSupported[Index] = (UINT8) IsRpMultiVc(Index);
    }

  //  AsmCpuid (1, &RegEAX, &RegEBX, &RegECX, &RegEDX);
  //  SetupNvData->CpuFamilyModel         = (UINT16) RegEAX & 0x3FF0;
  //  SetupNvData->CpuExtendedFamilyModel = (UINT16) ((RegEAX >> 16) & 0x0FFF);
  //  CpuStepping = GetCpuStepping ();
  //  SetupNvData->EdramTestModeEnable = 0;
  //  if((((UINT32)((SetupNvData->CpuExtendedFamilyModel << 16) | SetupNvData->CpuFamilyModel) == EnumCpuSklUltUlx) &&
  //     (CpuStepping == EnumSklK0)) ||
  //     (((UINT32)((SetupNvData->CpuExtendedFamilyModel << 16) | SetupNvData->CpuFamilyModel) == EnumCpuSklDtHalo) &&
  //     (CpuStepping == EnumSklM0) || (CpuStepping == EnumSklN0))){
  //    SetupNvData->EdramTestModeEnable = 1;
  //  }

  }
  //=====================================================//
  // For MeSetupNvData                                   //
  //=====================================================//
  if (EFI_ERROR (GetMeSetupStatus)) {

  }
  //=====================================================//
  // For CpuSetupNvData                                  //
  //=====================================================//
  if (EFI_ERROR (GetCpuSetupStatus)) {

  }
  //=====================================================//
  // For PchSetupNvData                                  //
  //=====================================================//
  if (EFI_ERROR (GetPchSetupStatus)) {
    PchSetupNvData->RootPortDid = (UINT16) MmioRead16 (MmPciBase (0, 0, 0) + 2); // @todo: fix RP DID.
  }
  //=====================================================//
  // For MeSetupStorageNvData                            //
  //=====================================================//
  if (EFI_ERROR (GetMeSetupStorageStatus)) {
//[-start-201127-IB17510127-add]//
//[-start-201209-IB17510132-add]//
    HeciGetDamState (&MeSetupStorageNvData->DelayedAuthenticationMode);
//[-end-201209-IB17510132-add]//
    if (IsBootGuardSupported ()) {
      MeSetupStorageNvData->BootGuardSupport = 1;
    }

    MsrValue = (UINT32) AsmReadMsr64 (MSR_BOOT_GUARD_SACM_INFO);
    if (MsrValue & B_BOOT_GUARD_SACM_INFO_MEASURED_BOOT) {
      MeSetupStorageNvData->MeasureBoot = 1;
    }

    if (MeIsAfterEndOfPost ()) {
      MeSetupStorageNvData->AfterEoP = 1;
    } else {
      MeSetupStorageNvData->AfterEoP = 0;
    }

    Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
    if (EFI_ERROR (Status)) {
      MeSetupStorageNvData->AfterEoP = 1;
      goto Done;
    }

    if (MeSetupStorageNvData->AfterEoP) {
      goto Done;
    }

    Status = gBS->LocateProtocol (&gAmtWrapperProtocolGuid, NULL, (VOID **) &AmtWrapper);
    if (!EFI_ERROR (Status)) {
      MeSetupStorageNvData->RemoteSessionActive = AmtWrapper->IsStorageRedirectionEnabled () ||
                                                  AmtWrapper->IsSolEnabled () ||
                                                  AmtWrapper->IsKvmEnabled ();
    }

    Status = HeciGetFwFeatureStateMsg (&CurrentFeatures);
    if (!EFI_ERROR (Status)) {
      MeSetupStorageNvData->PttState   = !!CurrentFeatures.Fields.PTT;
    }

   Status = HeciGetUserCapabilitiesState (&UserCapabilities);
   if (!EFI_ERROR (Status)) {
     MeSetupStorageNvData->AmtState   = !!UserCapabilities.Fields.Amt;
   }

    HeciGetPlatIdSupportedState (&MeSetupStorageNvData->UpidSupport);

Done:
    DEBUG ((DEBUG_INFO, "%a MeExtractConfig %d %d %d %d %d %d %d %d %d %d %d %d %d",
      __FUNCTION__,
      MeSetupStorageNvData->MngState,
      MeSetupStorageNvData->FwUpdEnabled,
      MeSetupStorageNvData->MeStateControl,
      MeSetupStorageNvData->AfterEoP,
      MeSetupStorageNvData->RemoteSessionActive,
      MeSetupStorageNvData->PttState,
      MeSetupStorageNvData->TpmDeactivate,
      MeSetupStorageNvData->BootGuardSupport,
      MeSetupStorageNvData->MeasureBoot,
      MeSetupStorageNvData->DelayedAuthenticationMode,
      MeSetupStorageNvData->FipsModeSelect,
      MeSetupStorageNvData->InvokeArbSvnCommit,
      MeSetupStorageNvData->AmtState
      ));
    DEBUG ((DEBUG_INFO, " %d %d %d\n",
      MeSetupStorageNvData->UpidSupport,
      MeSetupStorageNvData->UpidState,
      MeSetupStorageNvData->UpidOsControlState
      ));
//[-end-201127-IB17510127-add]//
  }

  SetupNvData->OverClockingSupport = FeaturePcdGet(PcdOverclockEnable);
  return EFI_SUCCESS;
}

/**
 This function will be entered "in every boot" >> for updating related variables.
 (EX: memory size change, other items should be updated at the same time)

 @param [in,out]  SetupNvData            A pointer to Chipset configuration setup NV data
 @param [in,out]  SaSetupNvData          A pointer to SA setup NV data
 @param [in,out]  MeSetupNvData          A pointer to ME setup NV data
 @param [in,out]  CpuSetupNvData         A pointer to CPU setup NV data
 @param [in,out]  PchSetupNvData         A pointer to PCH setup NV data
 @param [in,out]  MeSetupStorageNvData   A pointer to ME setup storage NV data

 @retval          EFI_SUCCESS            Build default setup successful.
**/
EFI_STATUS
SetupRuntimeUpdateEveryBoot (
  IN OUT  CHIPSET_CONFIGURATION        *SetupNvData,
  IN OUT  SETUP_DATA                   *SetupDataNvData,
  IN OUT  SA_SETUP                     *SaSetupNvData,
  IN OUT  ME_SETUP                     *MeSetupNvData,
  IN OUT  CPU_SETUP                    *CpuSetupNvData,
  IN OUT  PCH_SETUP                    *PchSetupNvData,
  IN OUT  SI_SETUP                     *SiSetupNvData,
  IN OUT  ME_SETUP_STORAGE             *MeSetupStorageNvData
  )
{
  MEM_INFO_PROTOCOL         *MemoryInfo;
  EFI_STATUS                Status;
  UINTN                           Size;
  UINT32                          TbtSetupVolatileDataAttr;
  TBT_SETUP_VOLATILE_DATA         TbtSetupVolatileVar;
  TBT_SETUP_VOLATILE_DATA         *pTbtSetupVolatileVar;

#if FixedPcdGetBool(PcdITbtEnable) == 1
  UINT8                           Index;
  UINT32                          VariableAttributes;
  BOOLEAN                         SetupItbtDataModified;
  TCSS_DATA_HOB                   *TcssHob;
  UINT8                           TcssCpuXhciPortSupportMap;
#endif
  
  MemoryInfo            = NULL;
  ZeroMem (&TbtSetupVolatileVar, sizeof(TbtSetupVolatileVar));
  pTbtSetupVolatileVar = (TBT_SETUP_VOLATILE_DATA *)&TbtSetupVolatileVar;

#if FixedPcdGetBool(PcdITbtEnable) == 1
  TcssHob = NULL;
  TcssCpuXhciPortSupportMap = PcdGet8 (PcdCpuXhciPortSupportMap); // Get Board Capability Map set Cpu Xhci Port.
#endif

  TbtSetupVolatileDataAttr = EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS;
  Size = sizeof(TBT_SETUP_VOLATILE_DATA);
  Status = gRT->GetVariable(
                  L"TbtSetupVolatileData",
                  &gSetupVariableGuid,
                  &TbtSetupVolatileDataAttr,
                  &Size,
                  &TbtSetupVolatileVar
                  );
  if (Status == EFI_NOT_FOUND) {
    Status = gRT->SetVariable(
                    L"TbtSetupVolatileData",
                    &gSetupVariableGuid,
                    TbtSetupVolatileDataAttr,
                    sizeof(TbtSetupVolatileVar),
                    &TbtSetupVolatileVar
                    );
     ASSERT_EFI_ERROR(Status);
  }

  //
  // SharkBay Platform Performance Tuning Guide Revision 1.1 page 77
  // 9.3.7 It is expected that BIOS will program the MSR (0x601), bits 12:0 with the maximum desired current.
  // For overclock-able parts only, this register may be overridden to a value above the maximum VR supported Icc-max.
  //

  Status = gBS->LocateProtocol ( &gMemInfoProtocolGuid, NULL, (VOID **)&MemoryInfo );
  ASSERT_EFI_ERROR ( Status );

  if ( MemoryInfo->MemInfoData.memSize >= 256 ) {
    SetupNvData->MemoryLess256MB = 0;
  } else {
    SetupNvData->MemoryLess256MB = 1;
  }

  //
  // If memory size <= 512 MB, only pre-allocate 32 MB for DVMT.
  //
  if ( MemoryInfo->MemInfoData.memSize <= 512 ) {
    SaSetupNvData->IgdDvmt50PreAlloc = 1;
  }
  UpdateVMDVolatilevariables(SaSetupNvData);
  if (FeaturePcdGet (PcdTXTSupported)) {
    CpuSetupNvData->DprSize = UpdateTxtDprMemorySize();
    DEBUG ( ( DEBUG_ERROR | DEBUG_INFO, "!! DprSize = %x !!\n", CpuSetupNvData->DprSize));
  } else {
    CpuSetupNvData->DprSize = 0;
  }

  //
  // Check Setup Variable.
  // Setup Variable should always exist, because post build will create the default setup variable.
  //
#if FixedPcdGetBool(PcdITbtEnable) == 1
  Size = sizeof (mSetupData);
  Status = gRT->GetVariable(L"Setup", &gSetupVariableGuid, &VariableAttributes, &Size, &mSetupData);
  ASSERT_EFI_ERROR (Status);
  if (!EFI_ERROR(Status)) {
      SetupItbtDataModified = FALSE;
      //
      // Get status of each iTBT PCIe RP is enabled or not.
      //
      TcssHob = (TCSS_DATA_HOB *)GetFirstGuidHob(&gTcssHobGuid);
      if (TcssHob != NULL) {
        for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
          //
          // Check each iTBT PCIe RP is enabled or not.
          //
          if (mSetupData.ITbtPcieRootPortSupported[Index] != TcssHob->TcssData.ItbtPcieRpEn[Index]) {
            mSetupData.ITbtPcieRootPortSupported[Index] = TcssHob->TcssData.ItbtPcieRpEn[Index];
            SetupItbtDataModified = TRUE;
          }
        }
      }

      if (SetupItbtDataModified) {
          gRT->SetVariable(
              L"Setup",
              &gSetupVariableGuid,
              VariableAttributes,
              sizeof(mSetupData),
              &mSetupData
              );
      }
  }
#endif

  pTbtSetupVolatileVar->ITbtRootPortsNumber = PcdGet8 (PcdITbtRootPortNumber);
  pTbtSetupVolatileVar->DTbtContollersNumber = PcdGet8 (PcdDTbtControllerNumber);

#if FixedPcdGetBool(PcdITbtEnable) == 1
  for (Index = 0; Index < MAX_TCSS_USB3_PORTS; Index++) {
    pTbtSetupVolatileVar->TcssCpuXhciPortSupport[Index] = ((TcssCpuXhciPortSupportMap >> Index) & BIT0);
  }
#endif

  Status = gRT->SetVariable(
                  L"TbtSetupVolatileData",
                  &gSetupVariableGuid,
                  TbtSetupVolatileDataAttr,
                  sizeof(TbtSetupVolatileVar),
                  &TbtSetupVolatileVar
                  );

  if (SetupDataNvData->DiscreteTbtSupport) {
    DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices Call: OemSvcInitTbtFunc \n"));
    Status = OemSvcInitTbtFunc (SetupNvData, SetupDataNvData, SaSetupNvData, PchSetupNvData);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices OemSvcInitTbtFunc Status: %r\n", Status));
  }

  return EFI_SUCCESS;
}

UINT8
MeFwTypeDetect (
//[-start-190610-IB16990033-add]//
  VOID
//[-end-190610-IB16990033-add]//
  )
{
  ME_BIOS_PAYLOAD_HOB             *MbpHob;

  MbpHob              = NULL;
  //
  // Get the MBP Data.
  //
  MbpHob = GetFirstGuidHob (&gMeBiosPayloadHobGuid);

//[-start-220110-IB16810181-modify]//
  if (MbpHob == NULL) {
    return 0;
  } else {
    return ( UINT8 )( MbpHob->MeBiosPayload.FwPlatType.RuleData.Fields.IntelMeFwImageType );
  }
//[-end-220110-IB16810181-modify]//
}

UINT8
UpdateTxtDprMemorySize (
//[-start-190610-IB16990033-add]//
  VOID
//[-end-190610-IB16990033-add]//
  )
{
  VOID           *HobList;
  TXT_INFO_HOB   *TxtInfoHob;
  UINT8          TxtDprMemorySize;

  HobList          = NULL;
  TxtInfoHob       = NULL;
  TxtDprMemorySize = 0;

  HobList = GetFirstGuidHob (&gTxtInfoHobGuid);
  if (HobList == NULL) {
    return TxtDprMemorySize;
  } else {
    TxtInfoHob              = (TXT_INFO_HOB *) HobList;
    TxtDprMemorySize = (UINT8) RShiftU64(TxtInfoHob->Data.TxtDprMemorySize, 20);
    return TxtDprMemorySize;
  }
}
UINT8 *
EFIAPI
ScuCreateGrayoutIfOpCode (
  IN VOID                 *OpCodeHandle
  )
{
  EFI_IFR_SUPPRESS_IF     OpCode;

  OpCode.Header.OpCode = EFI_IFR_GRAYOUT_IF_OP;
  OpCode.Header.Length = sizeof (EFI_IFR_GRAYOUT_IF_OP);
  OpCode.Header.Scope  = 1;
  return HiiCreateRawOpCodes (OpCodeHandle, (UINT8 *) &OpCode, sizeof (EFI_IFR_GRAYOUT_IF_OP));
}

UINT8 *
EFIAPI
ScuCreateFalseOpCode (
  IN VOID                 *OpCodeHandle
  )
{
  EFI_IFR_FALSE           OpCode;

  OpCode.Header.OpCode = EFI_IFR_FALSE_OP;
  OpCode.Header.Length = sizeof (EFI_IFR_FALSE);
  OpCode.Header.Scope  = 0;

  return HiiCreateRawOpCodes (OpCodeHandle, (UINT8 *) &OpCode, sizeof (EFI_IFR_FALSE));
}

UINT8 *
EFIAPI
ScuCreateTrueOpCode (
  IN VOID                 *OpCodeHandle
  )
{
  EFI_IFR_FALSE           OpCode;

  OpCode.Header.OpCode = EFI_IFR_TRUE_OP;
  OpCode.Header.Length = sizeof (EFI_IFR_TRUE);
  OpCode.Header.Scope  = 0;

  return HiiCreateRawOpCodes (OpCodeHandle, (UINT8 *) &OpCode, sizeof (EFI_IFR_TRUE));
}

//_Start_L05_SETUP_MENU_
/**
 Build L05 default variable value an save to a buffer according to platform requirement

 @param [out]  SetupNvData              A pointer to SYSTEM_CONFIGURATION variable default value
 @param [out]  PchSetupNvData           A pointer to PCH_SETUP variable default value
 @param [out]  CpuSetupNvData           A pointer to CPU_SETUP variable default value
 @param [out]  SaSetupNvData            A pointer to SA_SETUP variable default value

 @retval EFI_SUCCESS                    Build default setup successful.
 @retval EFI_INVALID_PARAMETER          Input value is invalid.

**/
EFI_STATUS
L05DefaultSetup (
  IN OUT UINT8                          *SetupNvData,
  IN OUT UINT8                          *PchSetupNvData,
  IN OUT UINT8                          *CpuSetupNvData,
  IN OUT UINT8                          *SaSetupNvData
  )
{
  EFI_STATUS                            Status;
  SYSTEM_CONFIGURATION                  *SystemConfiguration;
#if (FixedPcdGetBool (PcdL05PchSetupSupported))
  PCH_SETUP                             *PchSetup;
  CPU_SETUP                             *CpuSetup;
  SA_SETUP                              *SaSetup;
#endif
  EFI_L05_SETUP_MENU_PROTOCOL           *L05SetupMenu;
  UINT8                                 L05OsOptimizedDefault;

  if (SetupNvData == NULL) {
    return RETURN_INVALID_PARAMETER;
  }

#if (FixedPcdGetBool (PcdL05PchSetupSupported))
  if ((PchSetupNvData == NULL) || (CpuSetupNvData == NULL) || (SaSetupNvData == NULL)) {
    return RETURN_INVALID_PARAMETER;
  }
#endif

  SystemConfiguration = (SYSTEM_CONFIGURATION *) SetupNvData;
#if (FixedPcdGetBool (PcdL05PchSetupSupported))
  PchSetup            = (PCH_SETUP *) PchSetupNvData;
  CpuSetup            = (CPU_SETUP *) CpuSetupNvData;
  SaSetup            =  (SA_SETUP *)  SaSetupNvData;
#endif

  L05SetupMenu = NULL;
  //
  //Save L05OsOptimizedDefault before load default
  //
  L05OsOptimizedDefault = SystemConfiguration->L05OsOptimizedDefault;

#if (FixedPcdGetBool (PcdL05PchSetupSupported))
  Status = OemSvcOverrideDefaultSetupSetting (SetupNvData, PchSetupNvData, CpuSetupNvData, SaSetupNvData);
#else
  Status = OemSvcOverrideDefaultSetupSetting (SetupNvData, NULL, NULL, NULL);
#endif

  switch (Status) {

  case EFI_SUCCESS:
    break;

  case EFI_MEDIA_CHANGED:
  case EFI_UNSUPPORTED:
  default:
    Status = gBS->LocateProtocol (
                    &gEfiL05SetupMenuProtocolGuid,
                    NULL,
                    &L05SetupMenu
                    );
    break;
  }

  if (L05SetupMenu != NULL) {
    //
    //To avoid ODM modify L05OsOptimizedDefault data and it need to restore when do load default
    //
    SystemConfiguration->L05OsOptimizedDefault = L05OsOptimizedDefault;

#if (FixedPcdGetBool (PcdL05PchSetupSupported))
    L05SetupMenu->OverrideDefaultSetupSetting (
                    (UINT8*)(VOID*) SetupNvData,
                    (UINT8*)(VOID*) PchSetup,
                    (UINT8*)(VOID*) CpuSetup,
                    (UINT8*)(VOID*) SaSetup
                    ); 
    L05SetupMenu->HideDisableProjectDependencyItem (
                    (UINT8*)(VOID*) SetupNvData,
                    (UINT8*)(VOID*) PchSetup,
                    (UINT8*)(VOID*) CpuSetup,
                    (UINT8*)(VOID*) SaSetup
                    ); 

#else
    L05SetupMenu->OverrideDefaultSetupSetting (
                    (UINT8*)(VOID*) SetupNvData,
                    (UINT8*)(VOID*) NULL,
                    (UINT8*)(VOID*) NULL,
                    (UINT8*)(VOID*) NULL
                    ); 
    L05SetupMenu->HideDisableProjectDependencyItem (
                    (UINT8*)(VOID*) SetupNvData,
                    (UINT8*)(VOID*) NULL,
                    (UINT8*)(VOID*) NULL,
                    (UINT8*)(VOID*) NULL
                    ); 
#endif
  }

  return EFI_SUCCESS;
}
//[-start-211229-YUNLEI0155-add]//
//_Start_L05_SETUP_MENU_
/**
 Build L05 default variable value for Intel WIFI SAR Table

 @param [out]  SetupNvData              A pointer to SYSTEM_CONFIGURATION variable default value


**/
#if defined(C770_SUPPORT) 
VOID 
L05Default14WifiSarTable (
  IN OUT   SETUP_DATA                            *MySetupDataIfrNVData                      
  )
{
  UINT8    Panel_Size;
  OemSvcLfcGetBoardID(PANEL_SIZE, &Panel_Size);
  if(Panel_Size==PANEL_SIZE_14)
  {
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit1=0x7C;  
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit2=0x5C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit3=0x5C;   
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit4=0x5C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit5=0x5C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit6=0x4C;  
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit7=0x4C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit8=0x4C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit9=0x4C;  
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit10=0x4C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit11=0x4C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit12=0x7C;  
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit13=0x5C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit14=0x5C;   
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit15=0x5C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit16=0x5C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit17=0x4C;  
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit18=0x4C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit19=0x4C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit20=0x4C;  
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit21=0x4C; 
    MySetupDataIfrNVData->WrdsWiFiSarTxPowerSet1Limit22=0x4C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit1=0x7C;  
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit2=0x5C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit3=0x5C;   
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit4=0x5C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit5=0x5C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit6=0x4C;  
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit7=0x4C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit8=0x4C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit9=0x4C;  
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit10=0x4C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit11=0x4C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit12=0x7C;  
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit13=0x5C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit14=0x5C;   
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit15=0x5C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit16=0x5C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit17=0x4C;  
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit18=0x4C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit19=0x4C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit20=0x4C;  
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit21=0x4C; 
    MySetupDataIfrNVData->WrdsCdbWiFiSarTxPowerSet1Limit22=0x4C;
  }


}
#endif
//[-end-211229-YUNLEI0155-add]//
//_End_L05_SETUP_MENU_

