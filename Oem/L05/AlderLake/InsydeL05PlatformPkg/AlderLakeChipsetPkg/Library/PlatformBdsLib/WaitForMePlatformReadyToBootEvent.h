/** @file
  A header file for WaitForMePlatformReadyToBootEvent.c.

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _WAIT_FOR_ME_PLATFORM_READY_TO_BOOT_EVENT_H_
#define _WAIT_FOR_ME_PLATFORM_READY_TO_BOOT_EVENT_H_
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>

VOID
CreateWaitForMePlatformReadyToBootEvent (
  VOID
);
#endif