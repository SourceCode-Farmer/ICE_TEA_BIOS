; @file
;  Intel Silicon View Technology (ISVT) support
;
;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************

DEFAULT REL
SECTION .text

extern   ASM_PFX(PcdGet16 (PcdIsvtCheckPointIoReadPort))

;------------------------------------------------------------------------------
;UINT8
;IsvtCall (
;  IN UINT8 CheckPoint       // rcx
;  );
;
;Routine Description:
;
;   Intel Silicon View Technology (ISVT) IO Reading port 0x84 with different AH values.
;
;Arguments:
;
;   CheckPoint - Put into AH.
;                AH = 1:  End of MRC State
;                AH = 2:  End of DXE State
;                AH = 3:  Before INT 19h boot (Ready to boot)
;
;Returns:
;
;   Data in IO port 0x99.
;
;------------------------------------------------------------------------------
global ASM_PFX(IsvtCall)
ASM_PFX(IsvtCall):
    xor   eax, eax
    mov   ah,  cl
    mov   dx,  [ASM_PFX(PcdGet16 (PcdIsvtCheckPointIoReadPort))]
    in    al,  dx
    ret

