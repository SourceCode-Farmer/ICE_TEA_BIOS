/** @file
  PlatformBdsLib

;******************************************************************************
;* Copyright (c) 2017 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "OemHotKey.h"
//_Start_L05_NOVO_BUTTON_MENU_
#include <Protocol/L05Service.h>
#include <Protocol/NovoRecovery.h>
#include <L05Config.h>
#include <Guid/L05NovoKeyInfoHob.h>
#include <Pi/PiHob.h>
#include <Protocol/L05Variable.h>
#include <SetupConfig.h>  // SYSTEM_CONFIGURATION
//_End_L05_NOVO_BUTTON_MENU_
//_Start_L05_VARIABLE_SERVICE_
#include <Guid/L05OneKeyRecoveryBiosData.h>
//_End_L05_VARIABLE_SERVICE_
//[-start-211013-TAMT000028-add]//
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#endif
//[-end-211013-TAMT000028-add]//
#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
typedef enum {
  L05InterruptMenuNormalBoot,
  L05InterruptMenuSetup,
  L05InterruptMenuRegulatoryInfo,
  L05InterruptMenuDiagnostics,
  L05InterruptMenuOneKey,
  L05InterruptMenuBootManager,
} L05_INTERRUPT_MENU_OPTION;
//_End_L05_INTERRUPT_MENU_
#endif

//_Start_L05_HDD_PASSWORD_ENABLE_
/**
  Checking all HDD security status is LOCK or not.

  @param  None.

  @retval TRUE                          Found harddisk is locked.
  @retval FALSE                         Could not found any harddisk is locked.
**/
BOOLEAN
L05CheckHddLock (
  )
{
  EFI_STATUS                            Status;
  EFI_HDD_PASSWORD_SERVICE_PROTOCOL     *HddPasswordService;
  HDD_PASSWORD_HDD_INFO                 *HddInfoArray;
  UINTN                                 NumOfHdd;
  UINTN                                 Index;
  BOOLEAN                               IsHddLock;

  HddPasswordService = NULL;
  HddInfoArray       = NULL;
  NumOfHdd           = 0;
  IsHddLock          = FALSE;

  Status = gBS->LocateProtocol (
                  &gEfiHddPasswordServiceProtocolGuid,
                  NULL,
                  &HddPasswordService
                  );

  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  //
  // Get HDD info
  //
  Status = HddPasswordService->GetHddInfo (
                                 HddPasswordService,
                                 &HddInfoArray,
                                 &NumOfHdd
                                 );

  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  if (NumOfHdd == 0) {
    return FALSE;
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    if ((HddInfoArray[Index].HddSecurityStatus & HDD_LOCKED_BIT) == HDD_LOCKED_BIT) {
      IsHddLock = TRUE;
      break;
    }
  }

  if (HddInfoArray != NULL) {
    gBS->FreePool (HddInfoArray);
  }

  return IsHddLock;
}
//_End_L05_HDD_PASSWORD_ENABLE_

/**
  Platform Oem HotKey Callback Function

  @param  Selection       HotKey Selection
  @param  Timeout
  @param  BootMode
  @param  NoBootDevices

  @retval EFI_SUCCESS

**/
EFI_STATUS
OemHotKeyCallback (
  IN UINT16                                    Selection,
  IN UINT16                                    Timeout,
  IN EFI_BOOT_MODE                             BootMode,
  IN BOOLEAN                                   NoBootDevices
  )
{
  EFI_STATUS                            Status;
//_Start_L05_NOVO_BUTTON_MENU_
  UINTN                                 DataSize;
  OKR_BIOS_DATA                         Data;
  BOOLEAN                               LvarEnable;
  EFI_PEI_HOB_POINTERS                  GuidHob;
  BOOLEAN                               NovoButtonPressed;
  EFI_HANDLE                            NovoHandle;
  EFI_L05_VARIABLE_PROTOCOL             *EfiL05VariablePtr;
  EFI_L05_SERVICE_PROTOCOL              *EfiL05ServicePtr;
  UINTN                                 L05NovoButtonSelection;
  EFI_STATUS                            L05Status;
  BOOLEAN                               EnterOneKey;
  EFI_HANDLE                            L05Handle;
  SYSTEM_CONFIGURATION                  SystemConfiguration;
  LIST_ENTRY                            BdsBootOptionList;
//_End_L05_NOVO_BUTTON_MENU_
#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
#ifdef L05_GRAPHIC_UI_ENABLE
  H2O_DIALOG_PROTOCOL                   *H2ODialog;
  UINT8                                 L05InterrupMenuSelection;
#endif
//_End_L05_INTERRUPT_MENU_
#endif

//_Start_L05_NOVO_BUTTON_MENU_
  EfiL05ServicePtr  = NULL;
  EnterOneKey       = FALSE;
  L05Handle         = NULL;
  NovoHandle        = NULL;
  EfiL05VariablePtr = NULL;
  LvarEnable        = FALSE;
  NovoButtonPressed = FALSE;

  GuidHob.Raw = GetHobList ();
  GuidHob.Raw = GetNextGuidHob (&gL05NovoKeyInfoHobGuid, GuidHob.Raw);

  if (GuidHob.Raw != NULL) {
    NovoButtonPressed = TRUE;
  }

  DataSize = sizeof (SYSTEM_CONFIGURATION);
  ZeroMem (&SystemConfiguration, DataSize);

  //
  // Get System Configuration settings
  //
  Status = gRT->GetVariable (
                  SETUP_VARIABLE_NAME,
                  &gSystemConfigurationGuid,
                  NULL,
                  &DataSize,
                  &SystemConfiguration
                  );

  DataSize = sizeof (OKR_BIOS_DATA);
  ZeroMem (&Data, DataSize);

  //
  // We must record lvar data before run OneKeyRecovery.efi driver.
  // Becuase we need to check it in OemHotKey and BdsEntry.
  //
  Status = gBS->LocateProtocol (&gEfiL05VariableProtocolGuid, NULL, (VOID **) &EfiL05VariablePtr);

  if (!EFI_ERROR (Status)) {

    Status = EfiL05VariablePtr->GetVariable (
                                  EfiL05VariablePtr,
                                  &gEfiL05OneKeyRecoveryBiosDataGuid,
                                  (UINT32 *) &DataSize,
                                  &Data
                                  );

    if (!EFI_ERROR (Status)) {
      //
      // 0x02 - trigger by application
      //
      if (Data.OkrStatus == 0x02) {
        LvarEnable = TRUE;
        DataSize = sizeof (LvarEnable);
        Status = gRT->SetVariable (
                        L"EnterOneKey",
                        &gEfiL05OneKeyRecoveryFromNovoButtonGuid,
                        EFI_VARIABLE_BOOTSERVICE_ACCESS,
                        DataSize,
                        &LvarEnable
                        );
      }
    }
  }
//_End_L05_NOVO_BUTTON_MENU_

//_Start_L05_HOTKEY_
//  if ((IsHotKeyDetected () || DoesOsIndicateBootToFwUI ()) && BdsLibIsWin8FastBootActive ()) {
  if ((IsHotKeyDetected () || DoesOsIndicateBootToFwUI ()) && BdsLibIsWin8FastBootActive () ||
      LvarEnable || (NovoButtonPressed && SystemConfiguration.UsbHotKeySupport == 1)) {  // UsbHotKeySupport, 0:Disable, 1:Enable
//_End_L05_HOTKEY_
    BdsLibConnectUsbHID ();
    BdsLibConnectAll ();
    if (H2OGetBootType () != LEGACY_BOOT_TYPE) {
      BdsLibRestoreBootOrderFromPhysicalBootOrder ();
    }

//_Start_L05_NOVO_BUTTON_MENU_
    InitializeListHead (&BdsBootOptionList);
    BdsLibEnumerateAllBootOption (TRUE, &BdsBootOptionList);
//_End_L05_NOVO_BUTTON_MENU_
  }

//_Start_L05_ONE_KEY_RECOVERY_ENABLE_
  Status = gBS->InstallProtocolInterface (
                  &NovoHandle,
                  &gEfiInstallOneKeyRecoveryGuid,
                  EFI_NATIVE_INTERFACE,
                  NULL
                  );
  Status = gDS->Dispatch ();
//_End_L05_ONE_KEY_RECOVERY_ENABLE_
//[-start-211013-TAMT000028-add]//
#ifdef LCFC_SUPPORT
  Status = LfcCleanUpKbcAndEcBuffer ();
#endif
//[-end-211013-TAMT000028-add]//
//_Start_L05_NOVO_BUTTON_MENU_
  if (NovoButtonPressed) {
    L05Status = gBS->LocateProtocol (&gEfiL05ServiceProtocolGuid, NULL, &EfiL05ServicePtr);

    if (!EFI_ERROR (L05Status)) {

      if (!FeaturePcdGet (PcdH2OFormBrowserLocalMetroDESupported)) {
        //
        // It should not switch to TEXT Mode when Graphics Mode enabled
        //
        DisableQuietBoot ();
      }

      gST->ConOut->EnableCursor (gST->ConOut, FALSE);
      gST->ConOut->ClearScreen (gST->ConOut);

      L05Status = EfiL05ServicePtr->L05NovoButtonMenu (&L05NovoButtonSelection);

      if ((L05NovoButtonSelection != NovoButtonMenuNormalBoot) &&
          (NovoButtonPressed && SystemConfiguration.UsbHotKeySupport == 0)) {  // UsbHotKeySupport, 0:Disable, 1:Enable
        BdsLibConnectUsbHID ();
        BdsLibConnectAll ();
        if (H2OGetBootType () != LEGACY_BOOT_TYPE) {
          BdsLibRestoreBootOrderFromPhysicalBootOrder ();
        }

        InitializeListHead (&BdsBootOptionList);
        BdsLibEnumerateAllBootOption (TRUE, &BdsBootOptionList);
      }

      if (!EFI_ERROR (L05Status)) {
        switch (L05NovoButtonSelection) {

        case NovoButtonMenuSetup:
          //
          //  Enter Setup Menu
          //
          Selection = L05_SETUP_HOT_KEY;
          break;

        case NovoButtonMenuBootManager:
          //
          //  Enter Boot Manager
          //
          Selection = L05_BOOT_MANAGER_HOT_KEY;
          break;

        case NovoButtonMenuOneKey:
          EnterOneKey = TRUE;
          Selection = NO_OPERATION;
          break;

        case NovoButtonMenuDiagnostics:
          Selection = L05_DIAGNOSTICAL_HOT_KEY;
          break;

        case NovoButtonMenuNormalBoot:
        default:
          Selection = NO_OPERATION;
          break;
        }

      }
    }
  }

#ifdef L05_SMB_BIOS_ENABLE
  if ((Selection == L05_RECOVERY_HOT_KEY)) {
    EnterOneKey = TRUE;
  }

//_Start_L05_INTERRUPT_MENU_
#ifdef L05_GRAPHIC_UI_ENABLE
  if (!NovoButtonPressed) {
    if (Selection == L05_STARTUP_INTERRUPT_MENU_KEY) {
      Status = gBS->LocateProtocol (
                      &gH2ODialogProtocolGuid,
                      NULL,
                      (VOID **) &H2ODialog
                      );

      if (!EFI_ERROR (Status)) {
        H2ODialog->L05InterrupMenuDialog (&L05InterrupMenuSelection);

        switch (L05InterrupMenuSelection) {

        case L05InterruptMenuNormalBoot:
          Selection = NO_OPERATION;
          break;

        case L05InterruptMenuSetup:
          Selection = L05_SETUP_HOT_KEY;
          break;

        case L05InterruptMenuRegulatoryInfo:
          Selection = NO_OPERATION;
          break;

        case L05InterruptMenuDiagnostics:
          Selection = L05_DIAGNOSTICAL_HOT_KEY;
          break;

        case L05InterruptMenuOneKey:
          EnterOneKey = TRUE;
          Selection = NO_OPERATION;
          break;

        case L05InterruptMenuBootManager:
          Selection = L05_BOOT_MANAGER_HOT_KEY;
          break;
        }
      }
    }
  }
#endif
//_End_L05_INTERRUPT_MENU_
#endif

  if (EnterOneKey) {
    DataSize = sizeof (EnterOneKey);
    Status = gRT->SetVariable (
                    L"EnterOneKey",
                    &gEfiL05OneKeyRecoveryFromNovoButtonGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS,
                    DataSize,
                    &EnterOneKey
                    );

#ifdef L05_ONE_KEY_RECOVERY_ENABLE
    //
    //  By L05 Dual mode BIOS spec v0.1a, Notify Event when select One Key Recovery.
    //
    L05Status = gBS->InstallProtocolInterface (
                       &L05Handle,
                       &gEfiOneKeyRecoveryEvent2Guid,
                       EFI_NATIVE_INTERFACE,
                       NULL
                       );
#endif
  }
//_End_L05_NOVO_BUTTON_MENU_

  Status = EFI_SUCCESS;
  Status = BdsLibStartSetupUtility (TRUE);

//_Start_L05_HDD_PASSWORD_ENABLE_
  //
  // [Lenovo China Minimum BIOS Spec 1.39]
  //   [3.4.2 Rules for Hard disk password setting]
  //     6. When hard disk password enabled, the user must input
  //        correct hard disk password while entering CMOS setup.
  //
  if ((Selection == L05_SETUP_HOT_KEY) || DoesOsIndicateBootToFwUI ()) {
    if (L05CheckHddLock ()) {
      //
      // Enter SCU by press "F2" hotkey at POST.
      // - Clear hot key selection.
      //
      Selection = NO_OPERATION;

      //
      // Enter SCU by OS indications.
      //  - Clear EFI_OS_INDICATIONS_BOOT_TO_FW_UI bit of OS indications.
      //
      ClearOsIndicationsBits ((UINT64) EFI_OS_INDICATIONS_BOOT_TO_FW_UI);
    }
  }
//_End_L05_HDD_PASSWORD_ENABLE_

  switch (Selection) {

//_Start_L05_HOTKEY_
//  case SETUP_HOT_KEY:
//    //
//    // Display SetupUtility
//    //
//    BdsLibStartSetupUtility (FALSE);
//    break;
//
//  case DEVICE_MANAGER_HOT_KEY:
//    LaunchBootOptionByDevicePath ((EFI_DEVICE_PATH_PROTOCOL *) &gH2ODeviceManagerDevicePath);
//    break;
//
//  case BOOT_MANAGER_HOT_KEY:
//    //
//    // User chose to run the Boot Manager
//    //
//    LaunchBootOptionByDevicePath ((EFI_DEVICE_PATH_PROTOCOL *) &gH2OBootManagerDevicePath);
//    break;
//
//  case BOOT_MAINTAIN_HOT_KEY:
//    if (H2OGetBootType () != LEGACY_BOOT_TYPE) {
//      //
//      // Display the Boot Maintenance Manager
//      //
//      LaunchBootOptionByDevicePath ((EFI_DEVICE_PATH_PROTOCOL *) &gH2OBootMaintenanceDevicePath);
//    } else {
//      return EFI_UNSUPPORTED;
//    }
//    break;
//
//  case SECURE_BOOT_HOT_KEY:
//    if (H2OGetBootType () != LEGACY_BOOT_TYPE) {
//      //
//      // Display SetupUtility
//      //
//      LaunchBootOptionByDevicePath ((EFI_DEVICE_PATH_PROTOCOL *) &gH2OSecureBootMgrDevicePath);
//    } else {
//      return EFI_UNSUPPORTED;
//    }
//    break;
//
//  case FRONT_PAGE_HOT_KEY:
//    if (FeaturePcdGet(PcdFrontPageSupported)) {
//      Timeout = 0xFFFF;
//      BdsLibEnumerateAllBootOption (TRUE, NULL);
//      LaunchBootOptionByDevicePath ((EFI_DEVICE_PATH_PROTOCOL *) &gH2OFrontPageDevicePath);
//    }
//    break;
//
//  case MEBX_HOT_KEY:
//    if (FeaturePcdGet (PcdAmtEnable)) {
//      DisableQuietBoot ();
//      gST->ConOut->EnableCursor (gST->ConOut, TRUE);
//      BdsLibStartSetupUtility (TRUE);
//      InvokeMebxHotKey ();
//    }
//    break;
//
//  case REMOTE_ASST_HOT_KEY:
//    if (FeaturePcdGet (PcdAmtEnable)) {
//      DisableQuietBoot ();
//      gST->ConOut->EnableCursor (gST->ConOut, TRUE);
//      BdsLibStartSetupUtility (TRUE);
//      InvokeRemoteAsstHotKey ();
//    }
//    break;
//_End_L05_HOTKEY_

//_Start_L05_HOTKEY_
  case L05_SETUP_HOT_KEY:
    //
    // Display SetupUtility
    //
    if (!FeaturePcdGet (PcdH2OFormBrowserLocalMetroDESupported)) {
      //
      // It should not switch to TEXT Mode when Graphics Mode enabled
      //
      DisableQuietBoot ();
    }

    BdsLibStartSetupUtility (FALSE);

    break;

  case L05_BOOT_MANAGER_HOT_KEY:
    //
    // User chose to run the Boot Manager
    //
    LaunchBootOptionByDevicePath ((EFI_DEVICE_PATH_PROTOCOL *) &gH2OBootManagerDevicePath);
    break;
//[-start-211122-BAIN000060-modify]//
#ifdef LCFC_SUPPORT
#if defined(L05_SMB_BIOS_ENABLE) || defined(L05_DIAGNOSTICS_SUPPORT)
  case L05_DIAGNOSTICAL_HOT_KEY:
    gBS->LocateProtocol (&gEfiL05ServiceProtocolGuid, NULL, &EfiL05ServicePtr);

    if (EfiL05ServicePtr != NULL) {
      EfiL05ServicePtr->L05LoadEfiDriverFromFv (&gL05UefiDiagnosticsAppGuid);
    }
    break;
#endif
#else
#if defined(L05_SMB_BIOS_ENABLE) && defined(L05_DIAGNOSTICS_SUPPORT)
  case L05_DIAGNOSTICAL_HOT_KEY:
    gBS->LocateProtocol (&gEfiL05ServiceProtocolGuid, NULL, &EfiL05ServicePtr);

    if (EfiL05ServicePtr != NULL) {
      EfiL05ServicePtr->L05LoadEfiDriverFromFv (&gL05UefiDiagnosticsAppGuid);
    }
    break;
#endif
#endif
//[-end-211122-BAIN000060-modify]//
//_End_L05_HOTKEY_
  }

  return EFI_SUCCESS;
}

