/** @file

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  ASF BDS Support include file

@copyright
 Copyright (c) 2005 - 2014 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains a 'Sample Driver' and is licensed as such
 under the terms of your license agreement with Intel or your
 vendor. This file may be modified by the user, subject to
 the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _ASF_SUPPORT_H_
#define _ASF_SUPPORT_H_

#include <Uefi/UefiBaseType.h>
#include <Guid/GlobalVariable.h>
#include <IndustryStandard/Pci22.h>
#include <Protocol/LoadFile.h>
#include <Protocol/LoadedImage.h>
#include <Protocol/LegacyBios.h>
#include <Protocol/AcpiS3Save.h>
#include <Protocol/BlockIo.h>
#include <Protocol/DiskInfo.h>
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/SimpleNetwork.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PrintLib.h>
#include <Library/DevicePathLib.h>

//#include <Amt.h>
#include <Protocol/AsfProtocol.h>
#include <Library/DxeAmtHeciLib.h>
#include <Register/MeRegs.h>
//#include <Protocol/AlertStandardFormatProtocol.h>
#include <Protocol/HeciProtocol.h>
#include "BdsPlatform.h"
#include <Library/PchInfoLib.h>

#include <Guid/CrConfigHii.h>

#define USBR_STOR_PORT_NUM            0xB
#define USBR_BOOT_DEVICE_SHIFT        8
#define USBR_BOOT_DEVICE_MASK         0x1

#define SECURE_BOOT_ENABLED               1
#define SECURE_BOOT_DISABLED              0

#define RESTORE_SECURE_BOOT_NONE          0
#define RESTORE_SECURE_BOOT_ENABLED       1

//#549522 ME BIOS Specification
//Table 5-11. BAE Queue Value Definitions 
#define BAE_NETWORK_DEVICE    0x1
#define BAE_HDD_DEVICE        0x2
#define BAE_REMOVABLE_DEVICE  0x3
#define BAE_EMPTY_QUEUE       0x7F

#define RESTORE_SECURE_BOOT_GUID \
  { \
    0x118b3c6f, 0x98d6, 0x4d05, 0x96, 0xb2, 0x90, 0xe4, 0xcb, 0xb7, 0x40, 0x34 \
  }

#define EFI_SECURE_BOOT_ENABLE_DISABLE \
  { 0xf0a30bc7, 0xaf08, 0x4556, { 0x99, 0xc4, 0x0, 0x10, 0x9, 0xc9, 0x3a, 0x44 } }

typedef struct {
  ACPI_HID_DEVICE_PATH      PciRootBridge;
  PCI_DEVICE_PATH           Xhci;
  USB_DEVICE_PATH           Usbr;
  EFI_DEVICE_PATH_PROTOCOL  End;
} STORAGE_REDIRECTION_DEVICE_PATH;


typedef struct {
  UINT8 SubCommand;      // 0x00
  UINT8 VersionNumber;   // 0x01
  UINT8 EventSensorType; // 0x02
  UINT8 EventType;       // 0x03
  UINT8 EventOffset;     // 0x04
  UINT8 EventSourceType; // 0x05
  UINT8 EventSeverity;   // 0x06
  UINT8 SensorDevice;    // 0x07
  UINT8 Sensornumber;    // 0x08
  UINT8 Entity;          // 0x09
  UINT8 EntityInstance;  // 0x0A
  UINT8 EventData1;      // 0x0B
  UINT8 EventData2;      // 0x0C
  UINT8 EventData3;      // 0x0D
  UINT8 EventData4;      // 0x0E
  UINT8 EventData5;      // 0x0F
} BOOT_AUDIT_ENTRY;

typedef struct {
  UINT8            Command;
  UINT8            ByteCount;
  BOOT_AUDIT_ENTRY Data;
} BOOT_AUDIT_ENTRY_PACK;

//typedef struct _HECI_ASF_PUSH_PROGRESS_CODE {
//  UINT8           Command;
//  UINT8           ByteCount;
//  ASF_MESSAGE     AsfMessage;
//  UINT8           EventData[6];
//} HECI_ASF_PUSH_PROGRESS_CODE;

VOID
ConnectAllDriversToAllControllers (
  VOID
  );

extern
VOID
PrintBbsTable (
  IN BBS_TABLE                      *LocalBbsTable,
  IN UINT16                         BbsCount
  );

EFI_STATUS
ManageSecureBootState(
  IN VOID
  )
;

EFI_STATUS
BdsAsfInitialization (
  IN  VOID
  )
/*++

Routine Description:

  Retrieve the ASF boot options previously recorded by the ASF driver.

Arguments:

  None.

Returns:

  Initialize Boot Options global variable and AMT protocol

--*/
;

EFI_STATUS
BdsBootViaAsf (
  IN  VOID
  )
/*++

Routine Description:

  Process ASF boot options and if available, attempt the boot

Arguments:

  None.

Returns:

  EFI_SUCCESS - The command completed successfully
  Other       - Error!!

--*/
;

BOOLEAN
BdsCheckAsfBootCmd (
  IN  VOID
  );
  
VOID
UpdateSolTerminalTypeByCrConfig(
  IN PLATFORM_ISA_SERIAL_OVER_LAN_DEVICE_PATH *FullDevicePath
  );

#endif
