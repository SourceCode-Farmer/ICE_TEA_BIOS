/** @file
  IntelRemapPwd

;******************************************************************************
;* Copyright (c) 2019 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _INTEL_REMAP_PWD_H_
#define _INTEL_REMAP_PWD_H_

#include <Uefi.h>
#include <Protocol/UefiRaidOpromReady.h>
#include <Protocol/LoadedImage.h>
#include <Protocol/DiskInfo.h>

#include <IndustryStandard/Pci30.h>

#include <Guid/IntelRemapDevInfoVariable.h>
#include <Guid/H2OBdsCheckPoint.h>

#include <Library/H2OCpLib.h>
#include <Library/PchInfoLib.h>
#include <Library/VariableLib.h>
#include <Library/H2OHddPasswordTableLib.h>

#include <BdsPlatform.h>
#include <Library/SataSocLib.h>

#define bit(a)   (1 << (a))
#define HDD_SECURITY_SUPPORT bit(0)
#define HDD_SECURITY_ENABLE  bit(1)
#define HDD_SECURITY_LOCK    bit(2)
#define HDD_SECURITY_FROZEN  bit(3)
#define HDD_SECURITY_EXPIRED bit(4)

#define SATA_1_CONTROLLER_INDEX             0
#define SATA_2_CONTROLLER_INDEX             1
#define SATA_3_CONTROLLER_INDEX             2

typedef struct {
  EFI_GUID                                FileGuid;
} AGENT_FILE_GUID_LIST;

/**
  To support HddPassword for Intel Remapped device, need to dispatch Intel RST driver
  before reaching H2OBdsCpConOutAfter checkpoint.  This is where HddPassword driver
  collect all supported devices

 **/
VOID
DispatchIntelRstBeforeConOutAfter (
  );


/**
  Collect remap and Raid drive information for s3 auto unlock in Pei phase

  @param[in]        Event               Pointer to this event
  @param[in]        Handle              The handle associated with a previously registered checkpoint handler.

**/
VOID
H2OBdsCpEnumerteAfterCallback (
  IN EFI_EVENT                              Event,
  IN H2O_CP_HANDLE                          Handle
  );


/**
  To check if Intel Raid devices have being security enabled, if Yes, reconnect the controller.
  This is due to Intel Rst Driver can't produce BlockIo, after unlock a device, until re-enumerate device again for security reason.

  @param[in]        Event               Pointer to this event
  @param[in]        Handle              The handle associated with a previously registered checkpoint handler.

**/
VOID
H2OBdsCpEnumerteBeforeCallback (
  IN EFI_EVENT                              Event,
  IN H2O_CP_HANDLE                          Handle
  );

/**
 Find all agent handles that support HDD Password feature.
 @param[in]        AgentFileGuidList         Pointer to AGENT_FILE_GUID_LIST.
 @param[in]        size                      size of the AGENT_FILE_GUID_LIST.

 @retval *EFI_HANDLE  A list of agent handles that discovered from file Guids
**/
EFI_HANDLE*
FindAgentHandleListByFileGUID (
IN AGENT_FILE_GUID_LIST*   AgentFileGuidList,
IN UINTN                   Size
  );

#endif
