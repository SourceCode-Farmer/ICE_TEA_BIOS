;;******************************************************************************
;;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;;*
;;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;;* transmit, broadcast, present, recite, release, license or otherwise exploit
;;* any part of this publication in any form, by any means, without the prior
;;* written permission of Insyde Software Corporation.
;;*
;;******************************************************************************
;;
;; Module Name:
;;
;;   CallSmmIhisiBiosGuard.nasm
;;
;; Abstract:
;;
;;   64 bit Sent SMI to call IHISI flash ROM part
;;
SECTION .text 
%define IHISI_SIGNATURE 0x2448324F

;------------------------------------------------------------------------------
;  UINT8
;  BiosGuardCommunicate (
;    IN UINT8                 *Buffer,      // rcx
;    IN UINT32                FlashSize,    // rdx
;    IN UINT32                FlashAddress  // r8
;    IN UINT16                SmiPort       // r9
;    );
;------------------------------------------------------------------------------
global ASM_PFX(BiosGuardCommunicate)
ASM_PFX(BiosGuardCommunicate):
  push    rbx
  push    rdi
  push    rsi
  push    r8

  mov     ebx, IHISI_SIGNATURE
  mov     ax,  0x41EF
  mov     rdx, r9
  out     dx,  al

  ;AL Fun ret state

  pop     r8
  pop     rsi
  pop     rdi
  pop     rbx
  ret

;------------------------------------------------------------------------------
;  UINT8
;  BiosGuardWrite (
;    IN UINT8                *Buffer,      // rcx
;    IN UINT32               FlashSize,    // rdx
;    IN UINT32               FlashAddress  // r8
;    IN UINT16               SmiPort       // r9
;    );
;------------------------------------------------------------------------------
global ASM_PFX(BiosGuardWrite)
ASM_PFX(BiosGuardWrite):
  push    rbx
  push    rdi
  push    rsi
  push    r8

  mov     rsi, rcx    ;Arg1 rcx -> Write buffer to RSI
  mov     rdi, rdx    ;Arg2 rdx -> Write size to RDI
  mov     rcx, 0xEF
  mov     ebx, IHISI_SIGNATURE
  mov     ax,  0x42EF
  mov     rdx, r9
  out     dx,  al

  ;AL Fun ret state

  pop     r8
  pop     rsi
  pop     rdi
  pop     rbx
  ret

