;;******************************************************************************
;;* Copyright (c) 2016, Insyde Software Corp. All Rights Reserved.
;;*
;;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;;* transmit, broadcast, present, recite, release, license or otherwise exploit
;;* any part of this publication in any form, by any means, without the prior
;;* written permission of Insyde Software Corporation.
;;*
;;******************************************************************************
;;
;; Module Name:
;;
;;   CallSmmAsfSecureBoot.asm
;;
;; Abstract:
;;
;;   64 bit Sent SMI to call ASF_SECURE_BOOT_SMI
;;

TITLE   CallSmmAsfSecureBoot.asm

text  SEGMENT

H2O_SIGNATURE EQU           02448324Fh

;------------------------------------------------------------------------------
;  UINT8
;  AsfSecureBootChangedSmiCall (
;    IN     UINT8                 Action,   // rcx
;    IN     UINT16                SmiPort   // rdx
;    );
;------------------------------------------------------------------------------
AsfSecureBootChangedSmiCall PROC       PUBLIC

  push    rbx
  push    rdi
  push    rsi
  push    r8

  mov     ebx, H2O_SIGNATURE
  mov     al,  cl
  mov     ah,  al
  mov     al,  56h ; ASF_SECURE_BOOT_SMI
  out     dx,  al

  ;AL Fun ret state
  pop     r8
  pop     rsi
  pop     rdi
  pop     rbx
  ret

AsfSecureBootChangedSmiCall  ENDP
text  ENDS
END

