/** @file
  Source file for L05Hook for Security Erase.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_SECURITY_ERASE_H_
#define _L05_SECURITY_ERASE_H_

#define MAX_HDD_PASSWORD_LENGTH         32

//
// Bit0 - IDENTIFIER 0=Compare User password
//                   1=Compare Master password
//
#define ATA_SECURITY_ERASE_UNIT_COMPARE_USER_PASSWORD         0
#define ATA_SECURITY_DISABLE_PASSWORD_COMPARE_ADMIN_PASSWORD  BIT0
//
// Bit0 - ERASE MODE 0=Normal Erase mode
//                   1=Enhanced Erase mode
//
#define ATA_SECURITY_ERASE_UNIT_NORMAL_MODE                   0
#define ATA_SECURITY_ERASE_UNIT_ENHANCED_MODE                 BIT1

#define NVME_GENERIC_TIMEOUT            5000000    ///< 5s
#define NVME_FORMAT_NVM_CMD_TIMEOUT     3600000000 ///< 1h

#define NVME_CONTROLLER_ID              0
#define NVME_ALL_NAMESPACES             0xFFFFFFFF

#define STONY_BEACH_V1_DEVICE_ID        0x2522
#define INTEL_VENDOR_ID                 0x8086
#define STONY_BEACH_V1_MIN_SSID         0x3806
#define STONY_BEACH_V1_MAX_SSID         0x3807

//
// Optional Admin Command Support
//
#define FORMAT_NVM_SUPPORTED            BIT1
//
// Format NVM Attributes
//
#define CRYPTOGRAPHIC_ERASE_SUPPORTED   BIT2

//
// NvmExpress Admin Format NVM Command
//
#define FORMAT_NVM_USER_DATA_ERASE      BIT0
#define FORMAT_NVM_CRYPTOGRAPHIC_ERASE  BIT1

typedef enum {
  NoEraseMode,
  UserDataEraseMode,
  FullEraseMode
} SECURE_ERASE_MODE;

#pragma pack(1)
typedef struct {
  UINT16                                EraseControldata;
  UINT8                                 Password[MAX_HDD_PASSWORD_LENGTH];
  UINT16                                Reserved[256 - 1 - (MAX_HDD_PASSWORD_LENGTH / 2)];
} ATA_SECURE_ERASE_DATA_BLOCK;
#pragma pack()

#ifdef L05_SECURITY_ERASE_ENABLE
EFI_STATUS
L05SecurityErase (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  SETUP_UTILITY_BROWSER_DATA            *SuBrowser,
  IN  EFI_HII_HANDLE                        HiiHandle,
  IN  UINT16                                HddIndex
  );

EFI_STATUS
L05UpdateSecurityEraseSupported (
  IN  SETUP_UTILITY_BROWSER_DATA        *SuBrowser,
  IN  UINT16                            HddIndex,
  OUT UINT8                             *SecurityEraseSupported
  );

VOID
L05UpdateSecurityEraseItemString (
  IN  EFI_HII_HANDLE                    HiiHandle,
  IN  HDD_PASSWORD_SCU_DATA             *HddPasswordScuData
  );

EFI_STATUS
EFIAPI
L05InitSecurityEraseItem (
  IN  EFI_HII_HANDLE                    HiiHandle
  );
#endif

#endif
