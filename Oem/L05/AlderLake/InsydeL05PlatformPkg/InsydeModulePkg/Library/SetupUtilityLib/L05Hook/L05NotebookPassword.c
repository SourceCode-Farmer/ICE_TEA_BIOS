/** @file
  Source file for L05Hook for Lenovo Notebook Password.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "Security.h"

#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
BOOLEAN                                 mIsL05InitHddPasswordItem = FALSE;

/**
  Update set HDD Password flag.

  @param  SetupBuffer                   Points to the SYSTEM_CONFIGURATION.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
L05UpdateSetHddPasswordFlag (
  SYSTEM_CONFIGURATION                  *SetupBuffer
  )
{
  UINTN                                 Index;

  if (SetupBuffer == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  for (Index = 0; Index < mL05HddPasswordConfig->NumOfHdd; Index++) {
    if (mL05HddPasswordConfig->SetHddPasswordFlag[Index] == TRUE) {
      SetupBuffer->L05SetHddPasswordFlag = TRUE;
      break;
    }
  }

  if (Index == mL05HddPasswordConfig->NumOfHdd) {
    SetupBuffer->L05SetHddPasswordFlag = FALSE;
  }

  return EFI_SUCCESS;
}

/**
  L05 set HDD password

  @param  FormCallback                  Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param  HiiHandle                     Specific HII handle for Security menu.
  @param  ActionRequest                 On return, points to the action requested by the callback function. Type
                                        EFI_BROWSER_ACTION_REQUEST is specified in SendForm() in the Form Browser Protocol.
  @param  PState                        Password access is success or not, if access success then return TRUE.
  @param  HddPasswordScuData            Pointer to the Harddisk information array.
  @param  HddIndex                      Index of Harddisk.
  @param  MasterPasswordInput           Pointer to the input master password.
  @param  UserPasswordInput             Pointer to the input user password.

  @retval EFI_SUCCESS                   Callback success.
  @retval EFI_NOT_AVAILABLE_YET         Callback is not supported.
  @retval EFI_ACCESS_DENIED             User password can not disable password function.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
L05SetHddPassword (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *FormCallback,
  IN  EFI_HII_HANDLE                        HiiHandle,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest,
  OUT BOOLEAN                               *PState,
  IN  HDD_PASSWORD_SCU_DATA                 *HddPasswordScuData,
  IN  UINT16                                HddIndex,
  CHAR16                                    *MasterPasswordInput,
  CHAR16                                    *UserPasswordInput
  )
{
  EFI_STATUS                            Status;
  STATIC CHAR16                         MasterPassword[HDD_PASSWORD_MAX_NUMBER + 1];
  STATIC CHAR16                         UserPassword[HDD_PASSWORD_MAX_NUMBER + 1];
  SETUP_UTILITY_BROWSER_DATA            *SuBrowser;

  SuBrowser = NULL;

  Status = GetSetupUtilityBrowserData (&SuBrowser);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  *PState = FALSE;

  ZeroMem (MasterPassword, (HDD_PASSWORD_MAX_NUMBER + 1) * sizeof (CHAR16));
  ZeroMem (UserPassword,   (HDD_PASSWORD_MAX_NUMBER + 1) * sizeof (CHAR16));
  CopyMem (MasterPassword, MasterPasswordInput, StrSize (MasterPasswordInput));
  CopyMem (UserPassword,   UserPasswordInput,   StrSize (UserPasswordInput));

  Status = SetStoragePassword (
             HiiHandle,
             HddIndex,
             MASTER_PSW,
             MasterPassword
             );

  Status = SetStoragePassword (
             HiiHandle,
             HddIndex,
             USER_PSW,
             UserPassword
             );

  *PState = TRUE;
  mL05HddPasswordConfig->SetHddPasswordFlag[HddIndex] = TRUE;
  L05UpdateSetHddPasswordFlag ((SYSTEM_CONFIGURATION *) SuBrowser->SCBuffer);

  ZeroMem (
    HddPasswordScuData[HddIndex].DisableAllInputString,
    (HDD_PASSWORD_MAX_NUMBER + 1) * sizeof (CHAR16)
    );

  StrCpyS (
    HddPasswordScuData[HddIndex].DisableAllInputString,
    HDD_PASSWORD_MAX_NUMBER + 1,
    MasterPassword
    );

  HddPasswordScuData[HddIndex].DisableStringLength = (HDD_PASSWORD_MAX_NUMBER + 1);
  HddPasswordScuData[HddIndex].DisableAllType = MASTER_PSW;

  return Status;
}

/**
  L05 set HDD password Callback

  @param  This                          Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param  HiiHandle                     Specific HII handle for Security menu.
  @param  Type                          Return string token of device status.
  @param  ActionRequest                 On return, points to the action requested by the callback function. Type
                                        EFI_BROWSER_ACTION_REQUEST is specified in SendForm() in the Form Browser Protocol.
  @param  PState                        Password access is success or not, if access success then return TRUE.
  @param  HddPasswordScuData            Pointer to the Harddisk information array.
  @param  HddIndex                      Index of Harddisk.

  @retval EFI_SUCCESS                   Callback success.
  @retval EFI_NOT_AVAILABLE_YET         Callback is not supported.
  @retval EFI_ACCESS_DENIED             User password can not disable password function.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
L05SetHddPasswordCallback (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL    *This,
  IN  EFI_HII_HANDLE                          HiiHandle,
  IN  UINT8                                   Type,
  OUT EFI_BROWSER_ACTION_REQUEST              *ActionRequest,
  OUT BOOLEAN                                 *PState,
  IN  HDD_PASSWORD_SCU_DATA                   *HddPasswordScuData,
  IN  UINT16                                  HddIndex
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_BROWSER_DATA            *SuBrowser;
  CHAR16                                *StringPtr;
  EFI_INPUT_KEY                         Key;
  CHAR16                                MasterPasswordInput[FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1];
  CHAR16                                UserPasswordInput[FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1];

  ZeroMem (MasterPasswordInput,   sizeof (MasterPasswordInput));
  ZeroMem (UserPasswordInput,   sizeof (UserPasswordInput));

  Status = GetSetupUtilityBrowserData (&SuBrowser);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Enter Master HDD Password.
  //
  StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_SET_HDD_PASSWORDS_NOTICE_1), NULL);
  SuBrowser->H2ODialog->ConfirmDialog (DlgOk, FALSE, 0, NULL, &Key, StringPtr);
  gBS->FreePool (StringPtr);

  if (Key.ScanCode == SCAN_ESC) {
    return EFI_ABORTED;
  }

  StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_ENTER_MASTER_HDD_PASSWORD_PROMPT), NULL);
  Status = SuBrowser->H2ODialog->L05HddPasswordDialog (
                                   0,
                                   FALSE,
                                   FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1,
                                   MasterPasswordInput,
                                   &Key,
                                   StringPtr
                                   );
  gBS->FreePool (StringPtr);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Enter User HDD Password.
  //
  StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_SET_HDD_PASSWORDS_NOTICE_2), NULL);
  SuBrowser->H2ODialog->ConfirmDialog (DlgOk, FALSE, 0, NULL, &Key, StringPtr);
  gBS->FreePool (StringPtr);

  if (Key.ScanCode == SCAN_ESC) {
    return EFI_ABORTED;
  }

  StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_ENTER_USER_HDD_PASSWORD_PROMPT), NULL);
  Status = SuBrowser->H2ODialog->L05HddPasswordDialog (
                                   0,
                                   FALSE,
                                   FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1,
                                   UserPasswordInput,
                                   &Key,
                                   StringPtr
                                   );
  gBS->FreePool (StringPtr);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = L05SetHddPassword (
             This,
             HiiHandle,
             ActionRequest,
             PState,
             HddPasswordScuData,
             HddIndex,
             MasterPasswordInput,
             UserPasswordInput
             );

  return Status;
}

/**
 Update string of HDD password item in Security menu

  @param  HiiHandle                     Specific HII handle for Security menu.
  @param  HddPasswordScuData            Pointer to the Harddisk information array.
**/
VOID
L05UpdateHddPasswordItemString (
  IN  EFI_HII_HANDLE                    HiiHandle,
  IN  HDD_PASSWORD_SCU_DATA             *HddPasswordScuData
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_BROWSER_DATA            *SuBrowser;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  UINTN                                 NumOfHdd;
  UINTN                                 NumberLength;
  CHAR8                                 *PlatformLangVar;
  HDD_PASSWORD_HDD_INFO                 *HddInfo;
  UINTN                                 Index;
  CHAR16                                HddModelString[DEVICE_MODEL_NAME_STRING_LENGTH + 1];
  UINTN                                 StringIndex;

  SuBrowser       = NULL;
  PlatformLangVar = NULL;
  HddInfo         = NULL;

  Status = GetSetupUtilityBrowserData (&SuBrowser);

  if (Status != EFI_SUCCESS || SuBrowser == NULL || !mIsL05InitHddPasswordItem) {
    return;
  }

  SUCInfo  = SuBrowser->SUCInfo;
  NumOfHdd = SUCInfo->NumOfHdd;
  L05_GET_NUMBER_LENGTH (NumOfHdd, NumberLength);

  PlatformLangVar = CommonGetVariableData (
                      L"PlatformLang",
                      &gEfiGlobalVariableGuid
                      );

  for (Index = 0; Index < NumOfHdd; Index++) {
    HddInfo = HddPasswordScuData[Index].HddInfo;

    //
    // Copy Model String
    //
    ZeroMem (HddModelString, (DEVICE_MODEL_NAME_STRING_SIZE + sizeof (CHAR16)));
    CopyMem (HddModelString, HddInfo->HddModelString, DEVICE_MODEL_NAME_STRING_SIZE);

    //
    // Clean space from tail
    //
    StringIndex = DEVICE_MODEL_NAME_STRING_LENGTH - 1;

    while (HddModelString[StringIndex] == 0x0020) {
      SetMem (&HddModelString[StringIndex], sizeof (CHAR16), 0x0000);
      StringIndex--;
    }

    //
    // Update "HDD # password ststus" prompt string.
    //
    L05HiiSetStringByFormatString (
      HiiHandle,
      mL05HddPasswordConfig->StatusPromptStringId[Index],
      STRING_TOKEN (L05_STR_HDD_NUMBER_PASSWORD_STATUS_PROMPT),
      (NumberLength * sizeof (CHAR16)),
      (Index + 1)
      );

    //
    // Update HDD # password ststus string.
    //
    L05UpdateHddPasswordStatus (&HddPasswordScuData[Index], mL05HddPasswordConfig->StatusStringId[Index]);

    //
    // Update "Set HDD # password" prompt string.
    //
    L05HiiSetStringByFormatString (
      HiiHandle,
      mL05HddPasswordConfig->SetHddPromptStringId[Index],
      STRING_TOKEN (L05_STR_SET_HDD_NUMBER_PASSWORDS_PROMPT),
      (NumberLength * sizeof (CHAR16)),
      (Index + 1)
      );

    //
    // Update "Set HDD # password" help string
    //
    L05HiiSetStringByFormatString (
      HiiHandle,
      mL05HddPasswordConfig->SetHddHelpStringId[Index],
      STRING_TOKEN (L05_STR_SET_HDD_NUMBER_PASSWORDS_HELP),
      ((NumberLength * sizeof (CHAR16)) + StrSize (HddModelString)),
      (Index + 1),
      HddModelString
      );

    //
    // Update "Change Master Password of HD #" prompt string.
    //
    L05HiiSetStringByFormatString (
      HiiHandle,
      mL05HddPasswordConfig->ChangeMasterPromptStringId[Index],
      STRING_TOKEN (L05_STR_CHG_MASTER_HDD_NUMBER_PASSWORD_PROMPT),
      (NumberLength * sizeof (CHAR16)),
      (Index + 1)
      );

    //
    // Update "Change Master Password or disable Master and User Password for HD %d %s." prompt string.
    //
    if (AsciiStrCmp (PlatformLangVar, L05_RFC_3066_SIMPLIFIED_CHINESE_CODE) != 0) {
      L05HiiSetStringByFormatString (
        HiiHandle,
        mL05HddPasswordConfig->ChangeMasterHelpStringId[Index],
        STRING_TOKEN (L05_STR_CHG_MASTER_HDD_NUMBER_PASSWORD_HELP),
        ((NumberLength * sizeof (CHAR16)) + StrSize (HddModelString)),
        (Index + 1),
        HddModelString
        );

    } else {
      L05HiiSetStringByFormatString (
        HiiHandle,
        mL05HddPasswordConfig->ChangeMasterHelpStringId[Index],
        STRING_TOKEN (L05_STR_CHG_MASTER_HDD_NUMBER_PASSWORD_HELP),
        (((NumberLength * sizeof (CHAR16)) + StrSize (HddModelString)) * 2),
        (Index + 1),
        HddModelString,
        (Index + 1),
        HddModelString
        );
    }

    //
    // Update "Change User Password of HD #" prompt string.
    //
    L05HiiSetStringByFormatString (
      HiiHandle,
      mL05HddPasswordConfig->ChangeUserPromptStringId[Index],
      STRING_TOKEN (L05_STR_CHG_HDD_NUMBER_PASSWORD_PROMPT),
      (NumberLength * sizeof (CHAR16)),
      (Index + 1)
      );

    //
    // Update "Change User Password only for HD %d %s." prompt string.
    //
    if (AsciiStrCmp (PlatformLangVar, L05_RFC_3066_SIMPLIFIED_CHINESE_CODE) != 0) {
      L05HiiSetStringByFormatString (
        HiiHandle,
        mL05HddPasswordConfig->ChangeUserHelpStringId[Index],
        STRING_TOKEN (L05_STR_CHG_HDD_NUMBER_PASSWORD_HELP),
        ((NumberLength * sizeof (CHAR16)) + StrSize (HddModelString)),
        (Index + 1),
        HddModelString
        );

    } else {
      L05HiiSetStringByFormatString (
        HiiHandle,
        mL05HddPasswordConfig->ChangeUserHelpStringId[Index],
        STRING_TOKEN (L05_STR_CHG_HDD_NUMBER_PASSWORD_HELP),
        (((NumberLength * sizeof (CHAR16)) + StrSize (HddModelString)) * 2),
        (Index + 1),
        HddModelString,
        (Index + 1),
        HddModelString
        );
    }
  }

  if (PlatformLangVar != NULL) {
    FreePool (PlatformLangVar);
  }

  return;
}

/**
  Use format string to call HiiSetString function.

  @param  HiiHandle                     A handle that was previously registered in the HII Database.
  @param  UpdateStringId                If zero, then a new string is created in the String Package
                                        associated with HiiHandle.
                                        If non-zero, then the string specified by StringId is updated
                                        in the String Package associated with HiiHandle.
  @param  FormatStringId                String ID for the Null-terminated Unicode format string.
  @param  ArgumentExtraSize             Extra string size for Arguments.
  @param  ...                           Variable argument list whose contents are accessed based on the
                                        format string specified by FormatString.

  @retval 0                             The string could not be added or updated in the String Package.
  @retval Other                         The EFI_STRING_ID of the newly added or updated string.
**/
EFI_STRING_ID
L05HiiSetStringByFormatString (
  IN EFI_HII_HANDLE                     HiiHandle,
  IN EFI_STRING_ID                      UpdateStringId,
  IN EFI_STRING_ID                      FormatStringId,
  IN UINTN                              ArgumentExtraSize,
  ...
  )
{
  CHAR16                                *FormatString;
  UINTN                                 StringSize;
  CHAR16                                *TempString;
  VA_LIST                               Marker;
  EFI_STRING_ID                         SetStringId;

  FormatString = HiiGetString (HiiHandle, FormatStringId, NULL);
  StringSize = StrSize (FormatString) + ArgumentExtraSize;
  TempString = AllocateZeroPool (StringSize);
  VA_START (Marker, ArgumentExtraSize);
  UnicodeVSPrint (TempString, StringSize, FormatString, Marker);
  VA_END (Marker);
  SetStringId = HiiSetString (HiiHandle, UpdateStringId, (EFI_STRING) TempString, NULL);
  FreePool (FormatString);
  FreePool (TempString);

  return SetStringId;
}

/**
 Initialize HDD password item in Security menu
   [Lenovo Notebook Password Design Spec V1.1]
     3.1.1. HDP Configuration Item

  @param  HiiHandle                     Specific HII handle for Security menu.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
L05InitHddPasswordItem (
  IN  EFI_HII_HANDLE                    HiiHandle
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  SETUP_UTILITY_BROWSER_DATA            *SuBrowser;
  CHAR8                                 *PlatformLangVar;
  VOID                                  *StatusStartOpCodeHandle;
  VOID                                  *StatusEndOpCodeHandle;
  VOID                                  *OptionStartOpCodeHandle;
  VOID                                  *OptionEndOpCodeHandle;
  EFI_IFR_GUID_LABEL                    *StatusStartLabel;
  EFI_IFR_GUID_LABEL                    *StatusEndLabel;
  EFI_IFR_GUID_LABEL                    *OptionStartLabel;
  EFI_IFR_GUID_LABEL                    *OptionEndLabel;
  EFI_HDD_PASSWORD_SERVICE_PROTOCOL     *HddPasswordService;
  HDD_PASSWORD_SCU_DATA                 *HddPasswordScuData;
  UINTN                                 NumOfHdd;
  UINTN                                 NumberLength;
  HDD_PASSWORD_HDD_INFO                 *HddInfo;
  CHAR16                                HddModelString[DEVICE_MODEL_NAME_STRING_LENGTH + 1];
  UINTN                                 StringIndex;
  CHAR16                                *TempString;
  EFI_IFR_OP_HEADER                     *IfrOpHeader;

  SuBrowser       = NULL;
  IfrOpHeader     = NULL;
  PlatformLangVar = NULL;

  Status = GetSetupUtilityBrowserData (&SuBrowser);

  if (Status != EFI_SUCCESS || SuBrowser == NULL) {
    return Status;
  }

  SUCInfo            = SuBrowser->SUCInfo;
  HddPasswordService = SUCInfo->HddPasswordService;
  HddPasswordScuData = SUCInfo->HddPasswordScuData;
  NumOfHdd           = SUCInfo->NumOfHdd;
  L05_GET_NUMBER_LENGTH (NumOfHdd, NumberLength);

  if (HddPasswordService == NULL || HddPasswordScuData == NULL || NumOfHdd == 0) {
    return EFI_UNSUPPORTED;
  }

  PlatformLangVar = CommonGetVariableData (
                      L"PlatformLang",
                      &gEfiGlobalVariableGuid
                      );

  //
  // Allocate space for creation of Update Status Data Buffer
  //
  StatusStartOpCodeHandle = HiiAllocateOpCodeHandle ();
  ASSERT (StatusStartOpCodeHandle != NULL);

  StatusEndOpCodeHandle = HiiAllocateOpCodeHandle ();
  ASSERT (StatusEndOpCodeHandle != NULL);

  //
  // Allocate space for creation of Update Option Data Buffer
  //
  OptionStartOpCodeHandle = HiiAllocateOpCodeHandle ();
  ASSERT (OptionStartOpCodeHandle != NULL);

  OptionEndOpCodeHandle = HiiAllocateOpCodeHandle ();
  ASSERT (OptionEndOpCodeHandle != NULL);

  //
  // Create Hii Extend Label OpCode as the start opcode for HDD Password Status
  //
  StatusStartLabel = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (StatusStartOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
  StatusStartLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
  StatusStartLabel->Number       = L05_LABEL_HDD_PASSWORD_STATUS;

  //
  // Create Hii Extend Label OpCode as the end opcode for HDD Password Status
  //
  StatusEndLabel = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (StatusEndOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
  StatusEndLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
  StatusEndLabel->Number       = L05_LABEL_HDD_PASSWORD_STATUS_END;

  //
  // Create Hii Extend Label OpCode as the start opcode for HDD Password Option
  //
  OptionStartLabel = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (OptionStartOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
  OptionStartLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
  OptionStartLabel->Number       = L05_LABEL_HDD_PASSWORD_OPTION;

  //
  // Create Hii Extend Label OpCode as the end opcode for HDD Password Option
  //
  OptionEndLabel = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (OptionEndOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
  OptionEndLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
  OptionEndLabel->Number       = L05_LABEL_HDD_PASSWORD_OPTION_END;

  //
  // grayoutif  TRUE;
  //
  HiiCreateGrayOutIfOpCodeEx (StatusStartOpCodeHandle, 1);
  HiiCreateTrueOpCode (StatusStartOpCodeHandle);

  //
  // Show HDD Password Status
  // - "Hard Disk # Password"
  //
  for (Index = 0; Index < NumOfHdd; Index++) {
//_Start_L05_HDD_PASSWORD_ENABLE_
    //
    // eMMC does not support all functions of HDD password.
    //
    HddInfo = HddPasswordScuData[Index].HddInfo;

    if (HddInfo->ControllerMode == eMMC_MODE) {
      continue;
    }
//_End_L05_HDD_PASSWORD_ENABLE_
    
    mL05HddPasswordConfig->StatusPromptStringId[Index] = L05HiiSetStringByFormatString (
                                                           HiiHandle,
                                                           0,
                                                           STRING_TOKEN (L05_STR_HDD_NUMBER_PASSWORD_STATUS_PROMPT),
                                                           (NumberLength * sizeof (CHAR16)),
                                                           (Index + 1)
                                                           );

    TempString = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_HDD_PASSWORD_STATUS_STRING), NULL);
    mL05HddPasswordConfig->StatusStringId[Index] = HiiSetString (HiiHandle, 0, (EFI_STRING) TempString, NULL);
    FreePool (TempString);

    HiiCreateTextOpCode (
      StatusStartOpCodeHandle,
      mL05HddPasswordConfig->StatusPromptStringId[Index],
      STRING_TOKEN (STR_BLANK_STRING),
      mL05HddPasswordConfig->StatusStringId[Index]
      );

  }

  //
  // endif;
  //
  HiiCreateEndOpCode (StatusStartOpCodeHandle);

  //
  // Show HDD Password Option
  // - "Set Hard Disk # Passwords"
  // - "Change Master Password of HD #"
  // - "Change User Password of HD #"
  //
  for (Index = 0; Index < NumOfHdd; Index++) {
    HddInfo = HddPasswordScuData[Index].HddInfo;

//_Start_L05_HDD_PASSWORD_ENABLE_
    //
    // eMMC does not support all functions of HDD password.
    //
    if (HddInfo->ControllerMode == eMMC_MODE) {
      continue;
    }
//_End_L05_HDD_PASSWORD_ENABLE_

    //
    // Copy Model String
    //
    ZeroMem (HddModelString, (DEVICE_MODEL_NAME_STRING_SIZE + sizeof (CHAR16)));
    CopyMem (HddModelString, HddInfo->HddModelString, DEVICE_MODEL_NAME_STRING_SIZE);

    //
    // Clean space from tail
    //
    StringIndex = DEVICE_MODEL_NAME_STRING_LENGTH - 1;

    while (HddModelString[StringIndex] == 0x0020) {
      SetMem (&HddModelString[StringIndex], sizeof (CHAR16), 0x0000);
      StringIndex--;
    }

    mL05HddPasswordConfig->SetHddPromptStringId[Index] = L05HiiSetStringByFormatString (
                                                           HiiHandle,
                                                           0,
                                                           STRING_TOKEN (L05_STR_SET_HDD_NUMBER_PASSWORDS_PROMPT),
                                                           (NumberLength * sizeof (CHAR16)),
                                                           (Index + 1)
                                                           );

    mL05HddPasswordConfig->SetHddHelpStringId[Index] = L05HiiSetStringByFormatString (
                                                          HiiHandle,
                                                          0,
                                                          STRING_TOKEN (L05_STR_SET_HDD_NUMBER_PASSWORDS_HELP),
                                                          ((NumberLength * sizeof (CHAR16)) + StrSize (HddModelString)),
                                                          (Index + 1),
                                                          HddModelString
                                                          );

    mL05HddPasswordConfig->ChangeMasterPromptStringId[Index] = L05HiiSetStringByFormatString (
                                                                 HiiHandle,
                                                                 0,
                                                                 STRING_TOKEN (L05_STR_CHG_MASTER_HDD_NUMBER_PASSWORD_PROMPT),
                                                                 (NumberLength * sizeof (CHAR16)),
                                                                 (Index + 1)
                                                                 );

    if (AsciiStrCmp (PlatformLangVar, L05_RFC_3066_SIMPLIFIED_CHINESE_CODE) != 0) {
      mL05HddPasswordConfig->ChangeMasterHelpStringId[Index] = L05HiiSetStringByFormatString (
                                                                 HiiHandle,
                                                                 0,
                                                                 STRING_TOKEN (L05_STR_CHG_MASTER_HDD_NUMBER_PASSWORD_HELP),
                                                                 ((NumberLength * sizeof (CHAR16)) + StrSize (HddModelString)),
                                                                 (Index + 1),
                                                                 HddModelString
                                                                 );

    } else {
      mL05HddPasswordConfig->ChangeMasterHelpStringId[Index] = L05HiiSetStringByFormatString (
                                                                 HiiHandle,
                                                                 0,
                                                                 STRING_TOKEN (L05_STR_CHG_MASTER_HDD_NUMBER_PASSWORD_HELP),
                                                                 (((NumberLength * sizeof (CHAR16)) + StrSize (HddModelString)) * 2),
                                                                 (Index + 1),
                                                                 HddModelString,
                                                                 (Index + 1),
                                                                 HddModelString
                                                                 );
    }

    mL05HddPasswordConfig->ChangeUserPromptStringId[Index] = L05HiiSetStringByFormatString (
                                                               HiiHandle,
                                                               0,
                                                               STRING_TOKEN (L05_STR_CHG_HDD_NUMBER_PASSWORD_PROMPT),
                                                               (NumberLength * sizeof (CHAR16)),
                                                               (Index + 1)
                                                               );

    if (AsciiStrCmp (PlatformLangVar, L05_RFC_3066_SIMPLIFIED_CHINESE_CODE) != 0) {
      mL05HddPasswordConfig->ChangeUserHelpStringId[Index] = L05HiiSetStringByFormatString (
                                                               HiiHandle,
                                                               0,
                                                               STRING_TOKEN (L05_STR_CHG_HDD_NUMBER_PASSWORD_HELP),
                                                               ((NumberLength * sizeof (CHAR16)) + StrSize (HddModelString)),
                                                               (Index + 1),
                                                               HddModelString
                                                               );

    } else {
      mL05HddPasswordConfig->ChangeUserHelpStringId[Index] = L05HiiSetStringByFormatString (
                                                               HiiHandle,
                                                               0,
                                                               STRING_TOKEN (L05_STR_CHG_HDD_NUMBER_PASSWORD_HELP),
                                                               (((NumberLength * sizeof (CHAR16)) + StrSize (HddModelString)) * 2),
                                                               (Index + 1),
                                                               HddModelString,
                                                               (Index + 1),
                                                               HddModelString
                                                               );
    }

    //
    // grayoutif
    //   ideqval SystemConfig.SetUserPass == 1
    //   OR
    //   ideqval L05HddPasswordCongig.SetHddPasswordFlag[Index] == 1;
    //
    HiiCreateGrayOutIfOpCodeEx (OptionStartOpCodeHandle, 1);
    IfrOpHeader = (EFI_IFR_OP_HEADER *) HiiCreateIdEqualOpCodeEx (
                                          OptionStartOpCodeHandle,
                                          (EFI_QUESTION_ID) (L05_KEY_SET_USER_PASS_FLAG),
                                          1
                                          );
    IfrOpHeader->Scope = 1;
    HiiCreateIdEqualOpCodeEx (
      OptionStartOpCodeHandle,
      (EFI_QUESTION_ID) (Index + L05_KEY_SET_HDD_PASSWORD_FLAG),
      1
      );
    HiiCreateOrOpCode (OptionStartOpCodeHandle);
    HiiCreateEndOpCode (OptionStartOpCodeHandle);

    HiiCreateActionOpCode (
      OptionStartOpCodeHandle,
      (UINT16) (L05_KEY_SET_HDD_PASSWORD_OPTION + Index),
      mL05HddPasswordConfig->SetHddPromptStringId[Index],
      mL05HddPasswordConfig->SetHddHelpStringId[Index],
      EFI_IFR_FLAG_CALLBACK,
      0
      );

    //
    // endif;
    //
    HiiCreateEndOpCode (OptionStartOpCodeHandle);

    //
    // suppressif L05HddPasswordCongig.SetHddPasswordFlag[Index] == 0;
    //
    HiiCreateSuppressIfOpCodeEx (OptionStartOpCodeHandle, 1);
    HiiCreateIdEqualOpCodeEx (
      OptionStartOpCodeHandle,
      (EFI_QUESTION_ID) (Index + L05_KEY_SET_HDD_PASSWORD_FLAG),
      0
      );

    //
    // grayoutif
    //   ideqval SystemConfig.SetUserPass == 1
    //   OR
    //   ideqval L05HddPasswordCongig.SetHddPasswordFlag[Index] == 0;
    //
    HiiCreateGrayOutIfOpCodeEx (OptionStartOpCodeHandle, 1);
    IfrOpHeader = (EFI_IFR_OP_HEADER *) HiiCreateIdEqualOpCodeEx (
                                          OptionStartOpCodeHandle,
                                          (EFI_QUESTION_ID) (L05_KEY_SET_USER_PASS_FLAG),
                                          1
                                          );
    IfrOpHeader->Scope = 1;
    HiiCreateIdEqualOpCodeEx (
      OptionStartOpCodeHandle,
      (EFI_QUESTION_ID) (Index + L05_KEY_SET_HDD_PASSWORD_FLAG),
      0
      );
    HiiCreateOrOpCode (OptionStartOpCodeHandle);
    HiiCreateEndOpCode (OptionStartOpCodeHandle);

    HiiCreatePasswordOpCodeEx (
      OptionStartOpCodeHandle,
      (UINT16) (L05_KEY_CHANGE_MASTER_PASSWORD_OPTION + Index),
      PASSWORD_NAME_VALUE_VARSTORE_ID,
      STRING_TOKEN (STR_MASTER_HDD_PASSWORD_VAR_NAME),
      mL05HddPasswordConfig->ChangeMasterPromptStringId[Index],
      mL05HddPasswordConfig->ChangeMasterHelpStringId[Index],
      0x0,
      EFI_IFR_FLAG_CALLBACK,
      PcdGet16 (PcdH2OHddPasswordMinLength),
#ifndef L05_NOTEBOOK_PASSWORD_ENABLE
      MIN (PcdGet16 (PcdH2OHddPasswordMaxLength), HddInfo->MaxPasswordLengthSupport)
#else
      PcdGet16 (PcdH2OHddPasswordMaxLength)
#endif
      );

    //
    // endif;
    //
    HiiCreateEndOpCode (OptionStartOpCodeHandle);

    //
    // grayoutif
    //   ideqval L05HddPasswordCongig.SetHddPasswordFlag[Index] == 0;
    //
    HiiCreateGrayOutIfOpCodeEx (OptionStartOpCodeHandle, 1);
    HiiCreateIdEqualOpCodeEx (
      OptionStartOpCodeHandle,
      (EFI_QUESTION_ID) (Index + L05_KEY_SET_HDD_PASSWORD_FLAG),
      0
      );

    HiiCreatePasswordOpCodeEx (
      OptionStartOpCodeHandle,
      (UINT16) (L05_KEY_CHANGE_USER_PASSWORD_OPTION + Index),
      PASSWORD_NAME_VALUE_VARSTORE_ID,
      STRING_TOKEN (STR_HDD_PASSWORD_VAR_NAME),
      mL05HddPasswordConfig->ChangeUserPromptStringId[Index],
      mL05HddPasswordConfig->ChangeUserHelpStringId[Index],
      0x0,
      EFI_IFR_FLAG_CALLBACK,
      PcdGet16 (PcdH2OHddPasswordMinLength),
#ifndef L05_NOTEBOOK_PASSWORD_ENABLE
      MIN (PcdGet16 (PcdH2OHddPasswordMaxLength), HddInfo->MaxPasswordLengthSupport)
#else
      PcdGet16 (PcdH2OHddPasswordMaxLength)
#endif
      );

    //
    // endif;
    //
    HiiCreateEndOpCode (OptionStartOpCodeHandle);

    //
    // endif;
    //
    HiiCreateEndOpCode (OptionStartOpCodeHandle);
  }

  //
  // suppressif  TRUE;
  //
  HiiCreateSuppressIfOpCodeEx (OptionStartOpCodeHandle, 1);
  HiiCreateTrueOpCode (OptionStartOpCodeHandle);

  for (Index = 0; Index < NumOfHdd; Index++) {
    //
    // Assign questionid for SetHddPasswordFlag
    //
    HiiCreateNumericOpCode (
      OptionStartOpCodeHandle,
      (EFI_QUESTION_ID) (Index + L05_KEY_SET_HDD_PASSWORD_FLAG),
      L05_HDD_PASSWORD_CONFIGURATION_VARSTORE_ID,
      (UINT16) OFFSET_OF (L05_HDD_PASSWORD_CONFIGURATION, SetHddPasswordFlag[Index]),
      STRING_TOKEN (STR_BLANK_STRING),
      STRING_TOKEN (STR_BLANK_STRING),
      EFI_IFR_FLAG_READ_ONLY,
      EFI_IFR_NUMERIC_SIZE_1 | EFI_IFR_DISPLAY_UINT_HEX,
      0,
      0xFF,
      0,
      NULL
      );
  }

  //
  // Assign questionid for SetUserPass
  //
  HiiCreateNumericOpCode (
    OptionStartOpCodeHandle,
    (EFI_QUESTION_ID) (L05_KEY_SET_USER_PASS_FLAG),
    CONFIGURATION_VARSTORE_ID,
    (UINT16) OFFSET_OF (SYSTEM_CONFIGURATION, SetUserPass),
    STRING_TOKEN (STR_BLANK_STRING),
    STRING_TOKEN (STR_BLANK_STRING),
    EFI_IFR_FLAG_READ_ONLY,
    EFI_IFR_NUMERIC_SIZE_1 | EFI_IFR_DISPLAY_UINT_HEX,
    0,
    0xFF,
    0,
    NULL
    );

  //
  // endif;
  //
  HiiCreateEndOpCode (OptionStartOpCodeHandle);

  HiiUpdateForm (
    HiiHandle,
    NULL,
    ROOT_FORM_ID,
    StatusStartOpCodeHandle,
    StatusEndOpCodeHandle
    );

  HiiUpdateForm (
    HiiHandle,
    NULL,
    ROOT_FORM_ID,
    OptionStartOpCodeHandle,
    OptionEndOpCodeHandle
    );

  if (PlatformLangVar != NULL) {
    FreePool (PlatformLangVar);
  }

  HiiFreeOpCodeHandle (StatusStartOpCodeHandle);
  HiiFreeOpCodeHandle (StatusEndOpCodeHandle);
  HiiFreeOpCodeHandle (OptionStartOpCodeHandle);
  HiiFreeOpCodeHandle (OptionEndOpCodeHandle);

  mIsL05InitHddPasswordItem = TRUE;

  return EFI_SUCCESS;
}
#endif

