/** @file
  Source file for L05Hook for HDD Password.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "Security.h"
#include <Guid/L05PasswordVariable.h>

/**
  Is checked system password by HDD password dialog.

  @param  None.

  @retval TRUE                          The system password has been checked by HDD password dialog.
  @retval FALSE                         The system password was not checked by HDD password dialog.
**/
BOOLEAN
L05IsCheckedSystemPassword (
  VOID
  )
{
  EFI_STATUS                            Status;
  CHAR16                                L05PasswordStringPtr[FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1];
  UINTN                                 DataSize;

  Status   = EFI_NOT_FOUND;
  DataSize = sizeof (L05PasswordStringPtr);

  ZeroMem (L05PasswordStringPtr, sizeof (L05PasswordStringPtr));
  Status = gRT->GetVariable (
                  L05_PASSWORD_VARIABLE_NAME,
                  &gL05PasswordVariableGuid,
                  NULL,
                  &DataSize,
                  (VOID *) L05PasswordStringPtr
                  );

  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  return TRUE;
}

/**
  L05 set all HDD password

  @param  FormCallback                  Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param  HiiHandle                     Specific HII handle for Security menu.
  @param  ActionRequest                 On return, points to the action requested by the callback function. Type
                                        EFI_BROWSER_ACTION_REQUEST is specified in SendForm() in the Form Browser Protocol.
  @param  PState                        Password access is success or not, if access success then return TRUE.
  @param  HddPasswordScuData            Pointer to the Harddisk information array.
  @param  NumOfHdd                      Number of Harddisk.
  @param  MasterPasswordInput           Pointer to the input master password.
  @param  UserPasswordInput             Pointer to the input user password.

  @retval EFI_SUCCESS                   Callback success.
  @retval EFI_NOT_AVAILABLE_YET         Callback is not supported.
  @retval EFI_ACCESS_DENIED             User password can not disable password function.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
L05SetAllHddPassword (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *FormCallback,
  IN  EFI_HII_HANDLE                        HiiHandle,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest,
  OUT BOOLEAN                               *PState,
  IN  HDD_PASSWORD_SCU_DATA                 *HddPasswordScuData,
  IN  UINTN                                 NumOfHdd,
  CHAR16                                    *MasterPasswordInput,
  CHAR16                                    *UserPasswordInput
  )
{
  EFI_STATUS                            Status;
  STATIC CHAR16                         MasterPassword[HDD_PASSWORD_MAX_NUMBER + 1];
  STATIC CHAR16                         UserPassword[HDD_PASSWORD_MAX_NUMBER + 1];
  SETUP_UTILITY_BROWSER_DATA            *SuBrowser;
  UINTN                                 Index;

  SuBrowser = NULL;

  Status = GetSetupUtilityBrowserData (&SuBrowser);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  *PState = FALSE;

  ZeroMem (MasterPassword, (HDD_PASSWORD_MAX_NUMBER + 1) * sizeof (CHAR16));
  ZeroMem (UserPassword,   (HDD_PASSWORD_MAX_NUMBER + 1) * sizeof (CHAR16));
  CopyMem (MasterPassword, MasterPasswordInput, StrSize (MasterPasswordInput));
  CopyMem (UserPassword,   UserPasswordInput,   StrSize (UserPasswordInput));

  Status = SetAllHddPassword (
             HiiHandle,
             HddPasswordScuData,
             MASTER_PSW,
             NumOfHdd,
             MasterPassword
             );

  Status = SetAllHddPassword (
             HiiHandle,
             HddPasswordScuData,
             USER_PSW,
             NumOfHdd,
             UserPassword
             );

  *PState = TRUE;
  ((SYSTEM_CONFIGURATION *) SuBrowser->SCBuffer)->L05SetHddPasswordFlag = TRUE;

  for (Index = 0; Index < NumOfHdd; Index++) {
    ZeroMem (
      HddPasswordScuData[Index].DisableAllInputString,
      (HDD_PASSWORD_MAX_NUMBER + 1) * sizeof (CHAR16)
      );

    StrCpyS (
      HddPasswordScuData[Index].DisableAllInputString,
      HDD_PASSWORD_MAX_NUMBER + 1,
      MasterPassword
      );

    HddPasswordScuData[Index].DisableStringLength = (HDD_PASSWORD_MAX_NUMBER + 1);
    HddPasswordScuData[Index].DisableAllType = MASTER_PSW;
  }

  return Status;
}

/**
  L05 set all HDD password Callback

  @param  This                          Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param  HiiHandle                     Specific HII handle for Security menu.
  @param  Type                          Return string token of device status.
  @param  ActionRequest                 On return, points to the action requested by the callback function. Type
                                        EFI_BROWSER_ACTION_REQUEST is specified in SendForm() in the Form Browser Protocol.
  @param  PState                        Password access is success or not, if access success then return TRUE.
  @param  HddPasswordScuData            Pointer to the Harddisk information array.
  @param  NumOfHdd                      Number of Harddisk.

  @retval EFI_SUCCESS                   Callback success.
  @retval EFI_NOT_AVAILABLE_YET         Callback is not supported.
  @retval EFI_ACCESS_DENIED             User password can not disable password function.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
L05SetAllHddPasswordCallback (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL    *This,
  IN  EFI_HII_HANDLE                          HiiHandle,
  IN  UINT8                                   Type,
  OUT EFI_BROWSER_ACTION_REQUEST              *ActionRequest,
  OUT BOOLEAN                                 *PState,
  IN  HDD_PASSWORD_SCU_DATA                   *HddPasswordScuData,
  IN  UINTN                                   NumOfHdd
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_BROWSER_DATA            *SuBrowser;
  CHAR16                                *StringPtr;
  EFI_INPUT_KEY                         Key;
  CHAR16                                MasterPasswordInput[FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1];
  CHAR16                                UserPasswordInput[FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1];

  ZeroMem (MasterPasswordInput,   sizeof (MasterPasswordInput));
  ZeroMem (UserPasswordInput,   sizeof (UserPasswordInput));

  Status = GetSetupUtilityBrowserData (&SuBrowser);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Enter Master HDD Password.
  //
  StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_SET_HDD_PASSWORDS_NOTICE_1), NULL);
  SuBrowser->H2ODialog->ConfirmDialog (DlgOk, FALSE, 0, NULL, &Key, StringPtr);
  gBS->FreePool (StringPtr);

  if (Key.ScanCode == SCAN_ESC) {
    return EFI_ABORTED;
  }

  StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_SET_MASTER_HDD_PASSWORD_PROMPT), NULL);
  Status = SuBrowser->H2ODialog->L05HddPasswordDialog (
                                   0,
                                   FALSE,
                                   FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1,
                                   MasterPasswordInput,
                                   &Key,
                                   StringPtr
                                   );
  gBS->FreePool (StringPtr);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Enter User HDD Password.
  //
  StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_SET_HDD_PASSWORDS_NOTICE_2), NULL);
  SuBrowser->H2ODialog->ConfirmDialog (DlgOk, FALSE, 0, NULL, &Key, StringPtr);
  gBS->FreePool (StringPtr);

  if (Key.ScanCode == SCAN_ESC) {
    return EFI_ABORTED;
  }

  StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_SET_USER_HDD_PASSWORD_PROMPT), NULL);
  Status = SuBrowser->H2ODialog->L05HddPasswordDialog (
                                   0,
                                   FALSE,
                                   FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1,
                                   UserPasswordInput,
                                   &Key,
                                   StringPtr
                                   );
  gBS->FreePool (StringPtr);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = L05SetAllHddPassword (
             This,
             HiiHandle,
             ActionRequest,
             PState,
             HddPasswordScuData,
             NumOfHdd,
             MasterPasswordInput,
             UserPasswordInput
             );

  return Status;
}

/**
 Update string of HDD password status in Security menu

  @param  HddPasswordScuData            Pointer to the Harddisk information array.
  @param  UpdateStringId                Update string id.
**/
VOID
L05UpdateHddPasswordStatus (
  IN  HDD_PASSWORD_SCU_DATA             *HddPasswordScuData,
  IN  EFI_STRING_ID                     UpdateStringId
  )
{
  EFI_STRING_ID                         Token;
  CHAR16                                *NewString;
  EFI_STATUS                            Status;
  SETUP_UTILITY_BROWSER_DATA            *SuBrowser;

  Token             = 0;
  NewString         = NULL;

  Status = GetSetupUtilityBrowserData (&SuBrowser);

  if (EFI_ERROR (Status)) {
    return;
  }

  //
  // Update Information
  //
  switch (HddPasswordScuData->Flag) {

  case CHANGE_PASSWORD:
  case ENABLE_PASSWORD:
    Token = STRING_TOKEN (STR_INSTALLED_TEXT);
    break;

  default:
    Token = STRING_TOKEN (STR_NOT_INSTALLED_TEXT);
    break;
  }

  //
  // BUGBUG:
  // Status will not update with the following steps.
  // So check MasterFlag.
  //
  // 1. Enter SCU with no hdd password function.
  // 2. Set Master Hdd password (Update MasterFlag as CHANGE_PASSWORD)  <== Problem !!
  //
  if (HddPasswordScuData->MasterFlag == CHANGE_PASSWORD) {
    Token = STRING_TOKEN (STR_INSTALLED_TEXT);
  }

  if (Token != 0) {

    NewString = HiiGetString (SuBrowser->SUCInfo->MapTable[SecurityHiiHandle].HiiHandle, Token, NULL);

    if (NewString != NULL) {
      HiiSetString (SuBrowser->SUCInfo->MapTable[SecurityHiiHandle].HiiHandle, UpdateStringId, NewString, NULL);
      gBS->FreePool (NewString);
    }
  }

  return;
}

/**
 Update status of HDD password item in Security menu

  @param  HiiHandle                     Specific HII handle for Security menu.
  @param  HddPasswordScuData            Pointer to the Harddisk information array.
**/
VOID
L05UpdateHddPasswordStatusInSecurityPage (
  IN  EFI_HII_HANDLE                    HiiHandle,
  IN  HDD_PASSWORD_SCU_DATA             *HddPasswordScuData
  )
{

  //
  // Update Set all HDD password ststus
  //
  L05UpdateHddPasswordStatus (&HddPasswordScuData[0], STRING_TOKEN (L05_STR_HDD_PASSWORD_STATUS_STRING));

#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
  //
  // Update HDD password Item
  //
  L05UpdateHddPasswordItemString (HiiHandle, HddPasswordScuData);
#endif

#ifdef L05_SECURITY_ERASE_ENABLE
  L05UpdateSecurityEraseItemString (HiiHandle, HddPasswordScuData);
#endif

  return;
}

