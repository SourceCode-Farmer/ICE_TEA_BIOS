/** @file
  Source file for L05Hook for HDD Password.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_HDD_PASSWORD_H_
#define _L05_HDD_PASSWORD_H_

BOOLEAN
L05IsCheckedSystemPassword (
  VOID
  );

EFI_STATUS
L05SetAllHddPassword (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *FormCallback,
  IN  EFI_HII_HANDLE                        HiiHandle,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest,
  OUT BOOLEAN                               *PState,
  IN  HDD_PASSWORD_SCU_DATA                 *HddPasswordScuData,
  IN  UINTN                                 NumOfHdd,
  CHAR16                                    *MasterPasswordInput,
  CHAR16                                    *UserPasswordInput
  );

EFI_STATUS
L05SetAllHddPasswordCallback (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_HII_HANDLE                        HiiHandle,
  IN  UINT8                                 Type,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest,
  OUT BOOLEAN                               *PState,
  IN  HDD_PASSWORD_SCU_DATA                 *HddPasswordScuData,
  IN  UINTN                                 NumOfHdd
  );

VOID
L05UpdateHddPasswordStatus (
  IN  HDD_PASSWORD_SCU_DATA             *HddPasswordScuData,
  IN  EFI_STRING_ID                     UpdateStringId
  );

VOID
L05UpdateHddPasswordStatusInSecurityPage (
  IN  EFI_HII_HANDLE                    HiiHandle,
  IN  HDD_PASSWORD_SCU_DATA             *HddPasswordDataInfo
  );

#endif
