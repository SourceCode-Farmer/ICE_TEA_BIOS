/** @file
  Source file for L05Hook for Security Erase.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "Security.h"
#include <IndustryStandard/Atapi.h>
#include <IndustryStandard/Nvme.h>
#include <IndustryStandard/Pci30.h>
#include <Protocol/AtaPassThru.h>
#include <Protocol/NvmExpressPassthru.h>
#include <Protocol/PciIo.h>

#ifdef L05_SECURITY_ERASE_ENABLE
BOOLEAN                                 mIsL05InitSecurityEraseItem = FALSE;

/**
 Wrap original FreePool call in order to decrease code length
 (with setting back Buffer to NULL).

 @param Buffer                          Pointer to the allocated memory address.

**/
VOID
SafeFreePool (
  IN OUT VOID                           **Buffer
  )
{
  if (Buffer != NULL && *Buffer != NULL) {
    FreePool (*Buffer);
    *Buffer = NULL;
  }
}

/**
 Secure Erase HDP Authenticate.

  @param  SuBrowser                     Points to SETUP_UTILITY_BROWSER_DATA.
  @param  HiiHandle                     Specific HII handle for Security menu.
  @param  HddIndex                      HDD Index.
  @param  PasswordType                  The pointer to Password type.
  @param  PasswordToHdd                 The pointer to Encode password address.
  @param  PasswordToHddLength           The pointer to Encode string length.
  @param  EncodeData                    The pointer to Encode data.
  @param  EncodeDataSize                Encode data size.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
SecureEraseHdpAuthenticate (
  IN  SETUP_UTILITY_BROWSER_DATA        *SuBrowser,
  IN  EFI_HII_HANDLE                    HiiHandle,
  IN  UINT16                            HddIndex,
  OUT UINT8                             *PasswordType,
  OUT UINT8                             *PasswordToHdd,
  OUT UINTN                             *PasswordToHddLength,
  OUT UINT8                             **EncodeData,
  OUT UINTN                             *EncodeDataSize
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  HDD_PASSWORD_HDD_INFO                 *HddInfo;
  UINTN                                 NumberLength;
  EFI_INPUT_KEY                         Key;
  CHAR16                                UnicodePasswordString[HDD_PASSWORD_MAX_NUMBER + 1];
  CHAR16                                *FormatString;
  UINTN                                 BufferSize;
  CHAR16                                *StringPtr;
  CHAR16                                *F1String;
  UINTN                                 StringSize;
  CHAR8                                 *PlatformLangVar;

  SUCInfo         = SuBrowser->SUCInfo;
  HddInfo         = SUCInfo->HddPasswordScuData[HddIndex].HddInfo;
  FormatString    = NULL;
  StringPtr       = NULL;
  F1String        = NULL;
  PlatformLangVar = NULL;

  *PasswordType = MASTER_PSW;
  L05_GET_NUMBER_LENGTH (HddIndex, NumberLength);

  while (1) {
    ZeroMem (&Key, sizeof (Key));
    ZeroMem (UnicodePasswordString, (HDD_PASSWORD_MAX_NUMBER + 1) * sizeof (CHAR16));

    if (*PasswordType == MASTER_PSW) {
      FormatString = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_ENTER_HDD_MASTER_PASSWORD_STRING), NULL);

    } else {
      FormatString = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_ENTER_HDD_USER_PASSWORD_STRING), NULL);
    }

    BufferSize = StrSize (FormatString) + (NumberLength * sizeof (CHAR16));
    StringPtr = AllocateZeroPool (BufferSize);
    UnicodeSPrint (StringPtr, BufferSize, FormatString, (HddIndex + 1));
    SafeFreePool (&FormatString);

    //
    // Update L05_STR_PRESS_F1_SWITCH_MASTER_AND_USER_PASSWORD_MSG
    //
    FormatString = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_PRESS_F1_SWITCH_MASTER_AND_USER_PASSWORD_MSG), NULL);
    StringSize = StrSize (FormatString) + (NumberLength * sizeof (CHAR16));
    F1String = AllocateZeroPool (StringSize);

    if (F1String == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }

    PlatformLangVar = CommonGetVariableData (
                        L"PlatformLang",
                        &gEfiGlobalVariableGuid
                        );

    if (AsciiStrCmp (PlatformLangVar, L05_RFC_3066_SIMPLIFIED_CHINESE_CODE) != 0) {
      StrCpyS (F1String, (StringSize / sizeof (CHAR16)), FormatString);
    } else {
      UnicodeSPrint (F1String, StringSize, FormatString, (HddIndex + 1));
    }

    PcdSetPtrS (PcdL05NotebookPasswordDesignDialogF1String, &StringSize, (VOID *) F1String);
    SafeFreePool (&FormatString);
    SafeFreePool (&F1String);
    SafeFreePool (&PlatformLangVar);

    PcdSetBoolS (PcdL05NotebookPasswordDesignDialogF1Flag, TRUE);
    Status = SuBrowser->H2ODialog->PasswordDialog (
                                     0,
                                     FALSE,
                                     (PcdGet16 (PcdH2OHddPasswordMaxLength) + 1),
                                     UnicodePasswordString,
                                     &Key,
                                     StringPtr
                                     );
    PcdSetBoolS (PcdL05NotebookPasswordDesignDialogF1Flag, FALSE);
    SafeFreePool (&StringPtr);

    if (EFI_ERROR (Status)) {
      return Status;
    }

    if (Key.ScanCode != SCAN_F1) {
      break;
    }

    *PasswordType = (*PasswordType == MASTER_PSW) ? USER_PSW : MASTER_PSW;
  }

  Status = ValidateStoragePassword (
             HiiHandle,
             HddIndex,
             *PasswordType,
             UnicodePasswordString
             );

  if (EFI_ERROR (Status)) {
    SuBrowser->H2ODialog->L05ShowPwdStatusMessage (0, Status);
    return Status;
  }

  ZeroMem (PasswordToHdd, (HDD_PASSWORD_MAX_NUMBER + 1));
  *PasswordToHddLength = 0;

  Status = SUCInfo->HddPasswordService->PasswordStringProcess (
                                          SUCInfo->HddPasswordService,
                                          *PasswordType,
                                          UnicodePasswordString,
                                          StrLen (UnicodePasswordString),
                                          (VOID **) PasswordToHdd,
                                          PasswordToHddLength
                                          );

  *EncodeDataSize = SHA256_DIGEST_SIZE;
  *EncodeData = AllocateZeroPool (*EncodeDataSize);

  if (*EncodeData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  Status = HddPasswordEncode (
             PasswordToHdd,
             *PasswordToHddLength,
             HddInfo->HddModelNumber,
             sizeof (HddInfo->HddModelNumber),
             HddInfo->HddSerialNumber,
             sizeof (HddInfo->HddSerialNumber),
             *EncodeData,
             *EncodeDataSize
             );

  return Status;
}

/**
 Secure Erase set HDD password.

  @param  SuBrowser                     Points to SETUP_UTILITY_BROWSER_DATA.
  @param  HddIndex                      HDD Index.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
SecureEraseSetHddPassword (
  IN  SETUP_UTILITY_BROWSER_DATA        *SuBrowser,
  IN  UINT16                            HddIndex
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  UINT8                                 PasswordToHdd[HDD_PASSWORD_MAX_NUMBER + 1];
  UINTN                                 PasswordToHddLength;

  SUCInfo = SuBrowser->SUCInfo;

  if (SUCInfo->HddPasswordScuData[HddIndex].MasterFlag == CHANGE_PASSWORD) {
    ZeroMem (PasswordToHdd, (HDD_PASSWORD_MAX_NUMBER + 1));
    PasswordToHddLength = 0;

    Status = SUCInfo->HddPasswordService->PasswordStringProcess (
                                            SUCInfo->HddPasswordService,
                                            MASTER_PSW,
                                            SUCInfo->HddPasswordScuData[HddIndex].MasterInputString,
                                            StrLen (SUCInfo->HddPasswordScuData[HddIndex].MasterInputString),
                                            (VOID **) &PasswordToHdd,
                                            &PasswordToHddLength
                                            );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    Status = SUCInfo->HddPasswordService->SetHddPassword (
                                            SUCInfo->HddPasswordService,
                                            SUCInfo->HddPasswordScuData[HddIndex].HddInfo,
                                            MASTER_PSW,
                                            PasswordToHdd,
                                            PasswordToHddLength
                                            );

    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  if (SUCInfo->HddPasswordScuData[HddIndex].Flag == CHANGE_PASSWORD) {
    ZeroMem (PasswordToHdd, (HDD_PASSWORD_MAX_NUMBER + 1));
    PasswordToHddLength = 0;

    Status = SUCInfo->HddPasswordService->PasswordStringProcess (
                                            SUCInfo->HddPasswordService,
                                            USER_PSW,
                                            SUCInfo->HddPasswordScuData[HddIndex].InputString,
                                            StrLen (SUCInfo->HddPasswordScuData[HddIndex].InputString),
                                            (VOID **) &PasswordToHdd,
                                            &PasswordToHddLength
                                            );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    Status = SUCInfo->HddPasswordService->SetHddPassword (
                                            SUCInfo->HddPasswordService,
                                            SUCInfo->HddPasswordScuData[HddIndex].HddInfo,
                                            USER_PSW,
                                            PasswordToHdd,
                                            PasswordToHddLength
                                            );

    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  return Status;
}

/**
  Check whether it is an NVMe device by device path.

  @param  DevicePath                    Point to device path.

  @retval TRUE                          It is an NVMe device.
  @retval FALSE                         It is not an NVMe device.
**/
BOOLEAN
IsNvmeDevice (
  IN  EFI_DEVICE_PATH_PROTOCOL          *DevicePath
  )
{
  EFI_DEVICE_PATH_PROTOCOL              *TempDevicePath;

  TempDevicePath = DevicePath;

  while (!IsDevicePathEndType (TempDevicePath)) {
    if (DevicePathType (TempDevicePath) == MESSAGING_DEVICE_PATH &&
        DevicePathSubType (TempDevicePath) == MSG_NVME_NAMESPACE_DP) {
      return TRUE;
    }
    TempDevicePath = NextDevicePathNode (TempDevicePath);
  }

  return FALSE;
}

/**
  Get device pass thru.

  @param  SuBrowser                     Points to SETUP_UTILITY_BROWSER_DATA.
  @param  HddIndex                      HDD Index.
  @param  IsAtaPassThru                 Is ATA pass thru.
  @param  ControllerHandle              Controller Handle.
  @param  AtaDevice                     The pointer to the EFI_ATA_PASS_THRU_PROTOCOL.
  @param  NvmeDevice                    The pointer to the NVME_PASS_THRU_DEVICE data structure.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/

EFI_STATUS
GetDevicePassThru (
  IN  SETUP_UTILITY_BROWSER_DATA         *SuBrowser,
  IN  UINT16                             HddIndex,
  OUT BOOLEAN                            *IsAtaPassThru,
  OUT EFI_HANDLE                         *ControllerHandle,
  OUT EFI_ATA_PASS_THRU_PROTOCOL         **AtaPassThru,
  OUT EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL **NvmePassThru
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  HDD_PASSWORD_HDD_INFO                 *HddInfo;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePath;

  SUCInfo    = SuBrowser->SUCInfo;
  HddInfo    = SUCInfo->HddPasswordScuData[HddIndex].HddInfo;
  DevicePath = NULL;

  Status = gBS->HandleProtocol (
                  HddInfo->DeviceHandleInDxe,
                  &gEfiDevicePathProtocolGuid,
                  (VOID *) &DevicePath
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Check Device Type
  //
  *IsAtaPassThru = IsNvmeDevice (DevicePath) ? FALSE : TRUE;

  //
  // Locate PCI IO Protocol to get Controller Handle
  //
  *ControllerHandle = NULL;
  Status = gBS->LocateDevicePath (&gEfiPciIoProtocolGuid, &DevicePath, ControllerHandle);

  if (EFI_ERROR (Status) || *ControllerHandle == NULL) {
    return Status;
  }

  //
  // Get PassThru Protocol
  //
  if (*IsAtaPassThru) {
    Status = gBS->HandleProtocol (
                    *ControllerHandle,
                    &gEfiAtaPassThruProtocolGuid,
                    AtaPassThru
                    );
  } else {
    Status = gBS->HandleProtocol (
                    *ControllerHandle,
                    &gEfiNvmExpressPassThruProtocolGuid,
                    NvmePassThru
                    );
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }

  return Status;
}

/**
  Check the device has been properly erased.

  @param  HddSecurityStatus             HDD Security Status.

  @retval EFI_SUCCESS                   Secure Erase successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
VerifySecureErase (
  IN UINT16                             HddSecurityStatus
  )
{
  if ((HddSecurityStatus & HDD_ENABLE_BIT) == 0) {
    return EFI_SUCCESS;
  }

  return EFI_DEVICE_ERROR;
}

/**
  Sends prepare secure erase ATA command

  @param  AtaDevice                     The pointer to the EFI_ATA_PASS_THRU_PROTOCOL.
  @param  Port                          Port number on the ATA controller
  @param  PortMultiplierPort            Port multiplier port number on the ATA controller

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval EFI_BAD_BUFFER_SIZE           The ATA command was not executed. The number of bytes that could be transferred
                                        is returned in InTransferLength. For write and bi-directional commands,
                                        OutTransferLength bytes were transferred by OutDataBuffer.
  @retval EFI_NOT_READY                 The ATA command could not be sent because there are too many ATA commands
                                        already queued. The caller may retry again later.
  @retval EFI_DEVICE_ERROR              A device error occurred while attempting to send the ATA command.
  @retval EFI_INVALID_PARAMETER         Port, PortMultiplierPort, or the contents of Acb are invalid. The ATA
                                        command was not sent, so no additional status information is available.
**/
EFI_STATUS
PrepareSecureErase (
  IN EFI_ATA_PASS_THRU_PROTOCOL         *AtaDevice,
  IN UINT16                             Port,
  IN UINT16                             PortMultiplierPort
  )
{
  EFI_STATUS                            Status;
  EFI_ATA_PASS_THRU_COMMAND_PACKET      Packet;
  EFI_ATA_COMMAND_BLOCK                 Acb;
  EFI_ATA_STATUS_BLOCK                  Asb;

  ZeroMem (&Packet, sizeof (EFI_ATA_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Acb, sizeof (EFI_ATA_COMMAND_BLOCK));
  ZeroMem (&Asb, sizeof (EFI_ATA_STATUS_BLOCK));

  Acb.AtaCommand  = ATA_CMD_SECURITY_ERASE_PREPARE;
  Packet.Acb      = &Acb;
  Packet.Asb      = &Asb;
  Packet.Protocol = EFI_ATA_PASS_THRU_PROTOCOL_ATA_NON_DATA;
  Packet.Length   = EFI_ATA_PASS_THRU_LENGTH_BYTES | EFI_ATA_PASS_THRU_LENGTH_SECTOR_COUNT;
  Packet.Timeout  = EFI_TIMER_PERIOD_SECONDS (3);

  Status = AtaDevice->PassThru (
                        AtaDevice,
                        Port,
                        PortMultiplierPort,
                        &Packet,
                        NULL
                        );

  return Status;
}

/**
  Send ATA secure erase command and wait for completion for a specified timeout
  @param           AtaDevice            The pointer to the EFI_ATA_PASS_THRU_PROTOCOL.
  @param           Port                 Port number on the ATA controller
  @param           PortMultiplierPort   Port multiplier port number on the ATA controller
  @param           Password             Disk password
  @param           Timeout              Time to wait in 100ns units

  @retval          EFI_SUCCESS          This function execute successfully.
  @retval          EFI_TIMEOUT          Secure erase command takes longer to complete
                                        then the specified timeout
  @retval          EFI_DEVICE_ERROR     The device has failed to complete the command
  @retval          EFI_OUT_OF_RESOURCES There was not enough resources to complete
                                        the call
**/
EFI_STATUS
SendAtaEraseCommand (
  IN EFI_ATA_PASS_THRU_PROTOCOL         *AtaDevice,
  IN UINT16                             Port,
  IN UINT16                             PortMultiplierPort,
  IN ATA_IDENTIFY_DATA                  *AtaIdentifyData,
  IN BOOLEAN                            UserOrMaster,
  IN UINT8                              *Password,
  IN UINTN                              PasswordLength,
  IN UINT64                             Timeout
  )
{
  EFI_STATUS                            Status;
  EFI_ATA_PASS_THRU_COMMAND_PACKET      Packet;
  EFI_ATA_COMMAND_BLOCK                 Acb;
  EFI_ATA_STATUS_BLOCK                  Asb;
  ATA_SECURE_ERASE_DATA_BLOCK           SecurityEraseUnit;
  BOOLEAN                               IsEnhancedSecurityEraseSupported;

  ZeroMem (&Packet, sizeof (EFI_ATA_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Acb, sizeof (EFI_ATA_COMMAND_BLOCK));
  ZeroMem (&Asb, sizeof (EFI_ATA_STATUS_BLOCK));
  ZeroMem (&SecurityEraseUnit, sizeof (ATA_SECURE_ERASE_DATA_BLOCK));

  IsEnhancedSecurityEraseSupported = (AtaIdentifyData->security_status & HDD_ENHANCED_SECURITY_ERASE_BIT) == HDD_ENHANCED_SECURITY_ERASE_BIT;

  SecurityEraseUnit.EraseControldata = (IsEnhancedSecurityEraseSupported ? ATA_SECURITY_ERASE_UNIT_ENHANCED_MODE : ATA_SECURITY_ERASE_UNIT_NORMAL_MODE) | UserOrMaster;
  CopyMem (SecurityEraseUnit.Password, Password, MIN (sizeof (SecurityEraseUnit.Password), PasswordLength));
  Acb.AtaCommand           = ATA_CMD_SECURITY_ERASE_UNIT;

  Packet.Protocol          = EFI_ATA_PASS_THRU_PROTOCOL_PIO_DATA_OUT;
  Packet.Acb               = &Acb;
  Packet.Asb               = &Asb;
  Packet.OutDataBuffer     = &SecurityEraseUnit;
  Packet.OutTransferLength = sizeof (ATA_SECURE_ERASE_DATA_BLOCK);
  Packet.Length            = EFI_ATA_PASS_THRU_LENGTH_BYTES | EFI_ATA_PASS_THRU_LENGTH_SECTOR_COUNT;
  Packet.Timeout           = Timeout;

  Status = AtaDevice->PassThru (
                        AtaDevice,
                        Port,
                        PortMultiplierPort,
                        &Packet,
                        NULL
                        );

  ZeroMem (&SecurityEraseUnit, sizeof (ATA_SECURE_ERASE_DATA_BLOCK));

  return Status;
}

/**
 ATA Security Erase.

  @param  SuBrowser                     Points to SETUP_UTILITY_BROWSER_DATA.
  @param  HddIndex                      HDD Index.
  @param  PasswordType                  User or Master.
  @param  AtaPassThru                   The pointer to the EFI_ATA_PASS_THRU_PROTOCOL.
  @param  EncodeData                    The pointer to Encode data.
  @param  EncodeDataSize                Encode data size.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
AtaSecureErase (
  IN  SETUP_UTILITY_BROWSER_DATA        *SuBrowser,
  IN  UINT16                            HddIndex,
  IN  UINT8                             PasswordType,
  EFI_ATA_PASS_THRU_PROTOCOL            *AtaPassThru,
  IN  UINT8                             *EncodeData,
  IN  UINTN                             EncodeDataSize
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  HDD_PASSWORD_HDD_INFO                 *HddInfo;
  ATA_IDENTIFY_DATA                     AtaIdentifyData;

  SUCInfo = SuBrowser->SUCInfo;
  HddInfo = SUCInfo->HddPasswordScuData[HddIndex].HddInfo;
  ZeroMem (&AtaIdentifyData, sizeof (ATA_IDENTIFY_DATA));

  Status = SUCInfo->HddPasswordService->HddIdentify (
                                          SUCInfo->HddPasswordService,
                                          HddInfo,
                                          (UINT16 *) &AtaIdentifyData
                                          );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = PrepareSecureErase (
             AtaPassThru,
             HddInfo->PortNumber,
             HddInfo->PortMulNumber
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = SendAtaEraseCommand (
             AtaPassThru,
             HddInfo->PortNumber,
             HddInfo->PortMulNumber,
             &AtaIdentifyData,
             PasswordType,
             EncodeData,
             EncodeDataSize,
             EFI_TIMER_PERIOD_SECONDS (HddInfo->SecurityEraseTime * 2 * 60)
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = SUCInfo->HddPasswordService->ResetSecurityStatus (
                                          SUCInfo->HddPasswordService,
                                          HddInfo
                                          );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = SUCInfo->HddPasswordService->HddIdentify (
                                          SUCInfo->HddPasswordService,
                                          HddInfo,
                                          (UINT16 *) &AtaIdentifyData
                                          );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = VerifySecureErase (AtaIdentifyData.security_status);

  return Status;
}

/**
  Function checks which Secure Erase operations should be conducted.

  @retval NoEraseMode                   Secure Erase should not be conducted at all.
  @retval UserDataEraseMode             Only User Data Erase should be conducted.
  @retval FullEraseMode                 Both User Data Erase and Cryptographic Erase
                                        should be conducted.

  @return FullEraseMode                 Full Erase Mode.
  @return NoEraseMode                   No Erase Mode.
**/
SECURE_ERASE_MODE
GetSecureEraseMode (
  IN  EFI_HANDLE                        Handle,
  IN  NVME_ADMIN_CONTROLLER_DATA        *ControllerData
  )
{
  EFI_STATUS                            Status;
  EFI_PCI_IO_PROTOCOL                   *PciIo;
  UINT16                                DeviceId;
  UINT16                                VendorId;
  UINT16                                SubsystemId;
  UINT16                                SubsystemVendorId;

  Status = gBS->HandleProtocol (
                  Handle,
                  &gEfiPciIoProtocolGuid,
                  (VOID **) &PciIo
                  );

  if (!EFI_ERROR (Status)) {

    PciIo->Pci.Read (PciIo, EfiPciIoWidthUint16, PCI_VENDOR_ID_OFFSET, 1, &VendorId);
    PciIo->Pci.Read (PciIo, EfiPciIoWidthUint16, PCI_DEVICE_ID_OFFSET, 1, &DeviceId);

    if (!(ControllerData->Oacs & FORMAT_NVM_SUPPORTED)) {
      return NoEraseMode;

    } else {
      // Cryptographic Erase may only be skipped for Stony Beach v1. For other drives,
      // which does not support Cryptographic Erase, Secure Erase should fail.
      if (VendorId == INTEL_VENDOR_ID && DeviceId == STONY_BEACH_V1_DEVICE_ID) {
        PciIo->Pci.Read (PciIo, EfiPciIoWidthUint16, PCI_SUBSYSTEM_VENDOR_ID_OFFSET, 1, &SubsystemVendorId);
        PciIo->Pci.Read (PciIo, EfiPciIoWidthUint16, PCI_SUBSYSTEM_ID_OFFSET, 1, &SubsystemId);

        if (SubsystemVendorId == INTEL_VENDOR_ID &&
            (SubsystemId == STONY_BEACH_V1_MIN_SSID || SubsystemId == STONY_BEACH_V1_MAX_SSID)) {
          return UserDataEraseMode;
        }
      }

      if (!(ControllerData->Fna & CRYPTOGRAPHIC_ERASE_SUPPORTED)) {
        return UserDataEraseMode;
      }
    }

    return FullEraseMode;
  }

  return NoEraseMode;
}

/**
  Get identify controller data.

  @param  NvmeDevice                    The pointer to the NVME_PASS_THRU_DEVICE data structure.
  @param  NamespaceId                   NamespaceId for an NVM Express namespace present on the NVM Express controller
  @param  Buffer                        The buffer used to store the identify controller data.

  @return EFI_SUCCESS                   Successfully get the identify controller data.
  @return EFI_DEVICE_ERROR              Fail to get the identify controller data.
**/
EFI_STATUS
NvmeIdentifyController (
  IN  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL    *NvmeDevice,
  IN  UINT32                                NamespaceId,
  OUT VOID                                  *Buffer
  )
{
  EFI_STATUS                                Status;
  EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET  CommandPacket;
  EFI_NVM_EXPRESS_COMMAND                   Command;
  EFI_NVM_EXPRESS_COMPLETION                Completion;

  ZeroMem (&CommandPacket, sizeof (EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Command, sizeof (EFI_NVM_EXPRESS_COMMAND));
  ZeroMem (&Completion, sizeof (EFI_NVM_EXPRESS_COMPLETION));

  Command.Cdw0.Opcode          = NVME_ADMIN_IDENTIFY_CMD;

  Command.Nsid                 = NamespaceId;

  CommandPacket.NvmeCmd        = &Command;
  CommandPacket.NvmeCompletion = &Completion;
  CommandPacket.TransferBuffer = Buffer;
  CommandPacket.TransferLength = sizeof (NVME_ADMIN_CONTROLLER_DATA);
  CommandPacket.CommandTimeout = NVME_GENERIC_TIMEOUT;
  CommandPacket.QueueType      = NVME_ADMIN_QUEUE;
  //
  // Set bit 0 (Cns bit) to 1 to identify a controller
  //
  Command.Cdw10                = 1;
  Command.Flags                = CDW10_VALID;

  Status = NvmeDevice->PassThru (NvmeDevice, NamespaceId, &CommandPacket, NULL);

  return Status;
}


/**
  Perform Nvme User Data Erase.

  @param]  NvmeDevice                   The pointer to the NVME_PASS_THRU_DEVICE data structure.
  @param]  NamespaceId                  NamespaceId for an NVM Express namespace present on the NVM Express controller
  @param]  SecureEraseSettings          Type of the Secure Erase operation

  @retval EFI_SUCCESS                   Successfully performed secure erase operation.
  @retval EFI_NOT_READY                 The NVM Express Command Packet could not be sent because the controller is not ready.
  @retval EFI_DEVICE_ERROR              A device error occurred while attempting to send the NVM Express Command Packet.
  @retval EFI_INVALID_PARAMETER         NamespaceId or the contents of EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET are invalid.
  @retval EFI_UNSUPPORTED               The command described by the NVM Express Command Packet is not supported by the NVM Express
  @retval EFI_TIMEOUT                   A timeout occurred while waiting for the NVM Express Command Packet to execute.
**/
EFI_STATUS
NvmeFormatNvmCommand (
  IN EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL    *NvmeDevice,
  IN UINT32                                NamespaceId,
  IN UINT32                                SecureEraseSettings
  )
{
  EFI_STATUS                               Status;
  EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET CommandPacket;
  EFI_NVM_EXPRESS_COMMAND                  Command;
  EFI_NVM_EXPRESS_COMPLETION               Completion;
  NVME_ADMIN_FORMAT_NVM                    FormatNvm;

  ZeroMem (&CommandPacket, sizeof (EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Command, sizeof (EFI_NVM_EXPRESS_COMMAND));
  ZeroMem (&Completion, sizeof (EFI_NVM_EXPRESS_COMPLETION));
  ZeroMem (&FormatNvm, sizeof (NVME_ADMIN_FORMAT_NVM));

  CommandPacket.NvmeCmd        = &Command;
  CommandPacket.NvmeCompletion = &Completion;

  Command.Nsid                 = NamespaceId;
  Command.Cdw0.Opcode          = NVME_ADMIN_FORMAT_NVM_CMD;
  CommandPacket.CommandTimeout = NVME_FORMAT_NVM_CMD_TIMEOUT;
  CommandPacket.QueueType      = NVME_ADMIN_QUEUE;
  FormatNvm.Ses                = SecureEraseSettings;
  CopyMem (&CommandPacket.NvmeCmd->Cdw10, &FormatNvm, sizeof (NVME_ADMIN_FORMAT_NVM));
  CommandPacket.NvmeCmd->Flags = CDW10_VALID;

  Status = NvmeDevice->PassThru (NvmeDevice, Command.Nsid, &CommandPacket, NULL);

  return Status;
}

/**
  Attempts to erase a NVMe device specified by a given device path protocol

  @param  NvmeDevice                    The pointer to the EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL.
  @param  NamespaceId                   NamespaceId for an NVM Express namespace present on the NVM Express controller
  @param  SecureEraseMode               Secure Erase Mode defining Secure Erase Operations to be conducted

  @retval EFI_SUCCESS                   Secure Erase Commands were processed successfully.
  @retval EFI_NOT_READY                 The NVM Express Command Packet could not be sent because the controller is not ready.
  @retval EFI_DEVICE_ERROR              A device error occurred while attempting to send the NVM Express Command Packet.
  @retval EFI_INVALID_PARAMETER         NamespaceId or the contents of EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET are invalid.
  @retval EFI_UNSUPPORTED               The command described by the NVM Express Command Packet is not supported by the NVM Express
  @retval EFI_TIMEOUT                   A timeout occurred while waiting for the NVM Express Command Packet to execute.
  @retval Others                        Secure Erase has failed
**/
EFI_STATUS
SendNvmeSecureEraseCommands (
  IN EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL *NvmeDevice,
  IN UINT32                             NamespaceId,
  IN SECURE_ERASE_MODE                  SecureEraseMode
  )
{

  EFI_STATUS                            Status;

  if (SecureEraseMode == NoEraseMode) {
    return EFI_UNSUPPORTED;
  }

  if (SecureEraseMode != UserDataEraseMode) {
    Status = NvmeFormatNvmCommand (NvmeDevice, NamespaceId, FORMAT_NVM_CRYPTOGRAPHIC_ERASE);
  }

  Status = NvmeFormatNvmCommand (NvmeDevice, NamespaceId, FORMAT_NVM_USER_DATA_ERASE);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  return Status ;
}

/**
 NVMe Security Erase.

  @param  ControllerHandle              Controller Handle.
  @param  SuBrowser                     Points to EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
NvmeSecureErase (
  EFI_HANDLE                            ControllerHandle,
  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL    *NvmeDevice
  )
{
  EFI_STATUS                            Status;
  NVME_ADMIN_CONTROLLER_DATA            ControllerData;
  UINT32                                NamespaceId;
  SECURE_ERASE_MODE                     SecureEraseMode;

  NamespaceId = NVME_ALL_NAMESPACES;

  while (TRUE) {
    Status = NvmeDevice->GetNextNamespace (NvmeDevice, &NamespaceId);

    if (EFI_ERROR (Status)) {
      if (Status == EFI_NOT_FOUND) {
        Status = EFI_SUCCESS;
        break;

      } else {
        return Status;
      }
    }

    // Due to the fact that RAID Driver expects other value of NamespaceId parameter than AHCI driver,
    // need to send the Identify Controller command twice - once with the NamespaceId retrieved
    // using GetNextNamespace. If it fails we need to set NamespaceId = 0.
    ZeroMem (&ControllerData, sizeof (NVME_ADMIN_CONTROLLER_DATA));
    Status = NvmeIdentifyController (NvmeDevice, NamespaceId, (VOID *) &ControllerData);

    if (EFI_ERROR (Status)) {
      Status = NvmeIdentifyController (NvmeDevice, NVME_CONTROLLER_ID, (VOID *) &ControllerData);

      if (EFI_ERROR (Status)) {
        return Status;
      }
    }

    SecureEraseMode = GetSecureEraseMode (ControllerHandle, &ControllerData);

    Status = SendNvmeSecureEraseCommands (NvmeDevice, NamespaceId, SecureEraseMode);

    if (EFI_ERROR (Status)) {
      return Status;
    }

  }

  return Status;
}

/**
 L05 Security Erase.

  @param  This                          Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param  SuBrowser                     Points to SETUP_UTILITY_BROWSER_DATA.
  @param  HiiHandle                     Specific HII handle for Security menu.
  @param  HddIndex                      HDD Index.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
L05SecurityErase (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  SETUP_UTILITY_BROWSER_DATA            *SuBrowser,
  IN  EFI_HII_HANDLE                        HiiHandle,
  IN  UINT16                                HddIndex
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  HDD_PASSWORD_HDD_INFO                 *HddInfo;
  UINTN                                 NumOfHdd;
  CHAR16                                *StringPtr;
  CHAR16                                *String2Ptr;
  EFI_INPUT_KEY                         Key;
  UINT8                                 PasswordType;
  UINT8                                 PasswordToHdd[HDD_PASSWORD_MAX_NUMBER + 1];
  UINTN                                 PasswordToHddLength;
  UINT8                                 *EncodeData;
  UINTN                                 EncodeDataSize;
  BOOLEAN                               IsAtaPassThru;
  EFI_HANDLE                            ControllerHandle;
  EFI_ATA_PASS_THRU_PROTOCOL            *AtaPassThru;
  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL    *NvmePassThru;
  BOOLEAN                               IsTryAgain;
  EFI_STATUS                            ConfirmStatus;

  SUCInfo          = SuBrowser->SUCInfo;
  HddInfo          = SUCInfo->HddPasswordScuData[HddIndex].HddInfo;
  NumOfHdd         = SUCInfo->NumOfHdd;
  StringPtr        = NULL;
  String2Ptr       = NULL;
  EncodeData       = NULL;
  ControllerHandle = NULL;
  AtaPassThru      = NULL;
  NvmePassThru     = NULL;

  StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_SECURITY_ERASE_CONFIRM), NULL);
  SuBrowser->H2ODialog->ConfirmDialog (
                          L05WarningDlgYesNo,
                          FALSE,
                          0,
                          NULL,
                          &Key,
                          StringPtr
                          );

  SafeFreePool (&StringPtr);

  if (Key.UnicodeChar == CHAR_CARRIAGE_RETURN) {
    //
    // HDP authentication
    //
    Status = SecureEraseHdpAuthenticate (
               SuBrowser,
               HiiHandle,
               HddIndex,
               &PasswordType,
               PasswordToHdd,
               &PasswordToHddLength,
               &EncodeData,
               &EncodeDataSize
               );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    Status = SecureEraseSetHddPassword (SuBrowser, HddIndex);

    if (EFI_ERROR (Status)) {
      return Status;
    }

    Status = GetDevicePassThru (
               SuBrowser,
               HddIndex,
               &IsAtaPassThru,
               &ControllerHandle,
               &AtaPassThru,
               &NvmePassThru
               );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    do {
      IsTryAgain = FALSE;

      //
      // Prepare Security Erase Processing Dialog
      //
      StringPtr  = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_SECURITY_ERASE_PROCESSING_MSG_1), NULL);
      String2Ptr = HiiGetString (HiiHandle, STRING_TOKEN (L05_STR_SECURITY_ERASE_PROCESSING_MSG_2), NULL);
      SuBrowser->H2ODialog->L05SecurityEraseProcessingDialog (
                              FALSE,
                              HddInfo->SecurityEraseTime,
                              StringPtr,
                              String2Ptr
                              );

      if (IsAtaPassThru) {
        Status = AtaSecureErase (
                   SuBrowser,
                   HddIndex,
                   PasswordType,
                   AtaPassThru,
                   EncodeData,
                   EncodeDataSize
                   );

      } else {
        Status = NvmeSecureErase (ControllerHandle, NvmePassThru);

        //
        // Password will be clear after erase sucessfully.
        // NVMe need clear HDD Password.
        //
        if (!EFI_ERROR (Status)) {
          Status = SUCInfo->HddPasswordService->DisableHddPassword (
                                                  SUCInfo->HddPasswordService,
                                                  HddInfo,
                                                  PasswordType,
                                                  PasswordToHdd,
                                                  PasswordToHddLength
                                                  );
        }
      }

      if (!EFI_ERROR (Status)) {
        mL05HddPasswordConfig->SetHddPasswordFlag[HddIndex] = FALSE;
        SUCInfo->HddPasswordScuData[HddIndex].Flag = DISABLE_PASSWORD;
        SUCInfo->HddPasswordScuData[HddIndex].MasterFlag = DISABLE_PASSWORD;
        SUCInfo->HddPasswordScuData[HddIndex].DisableAllType = NO_ACCESS_PSW;
        L05UpdateSetHddPasswordFlag ((SYSTEM_CONFIGURATION *) SuBrowser->SCBuffer);

        ZeroMem (
          SUCInfo->HddPasswordScuData[HddIndex].InputString,
          sizeof (SUCInfo->HddPasswordScuData[HddIndex].InputString)
          );
        ZeroMem (
          SUCInfo->HddPasswordScuData[HddIndex].MasterInputString,
          sizeof (SUCInfo->HddPasswordScuData[HddIndex].MasterInputString)
          );

        UpdateAllHddPasswordFlag (
          HiiHandle,
          SUCInfo->HddPasswordScuData,
          NumOfHdd
          );
      }

      SuBrowser->H2ODialog->L05SecurityEraseProcessingDialog (
                              TRUE,
                              HddInfo->SecurityEraseTime,
                              StringPtr,
                              String2Ptr
                              );

      SafeFreePool (&StringPtr);
      SafeFreePool (&String2Ptr);

      StringPtr = HiiGetString (HiiHandle, EFI_ERROR (Status) ? STRING_TOKEN (L05_STR_SECURITY_ERASE_FAILED_MSG) : STRING_TOKEN (L05_STR_SECURITY_ERASE_SUCCESSFULLY_MSG), NULL);
      ConfirmStatus = SuBrowser->H2ODialog->ConfirmDialog (
                                              EFI_ERROR (Status) ? L05SecurityEraseFailedDlg : L05SecurityEraseSuccessfullyDlg,
                                              FALSE,
                                              0,
                                              NULL,
                                              &Key,
                                              StringPtr
                                              );

      SafeFreePool (&StringPtr);

      if (!EFI_ERROR (ConfirmStatus)) {
        if (Key.ScanCode == SCAN_ESC) {
          gRT->ResetSystem (EfiResetCold, EFI_SUCCESS, 0, NULL);
        }

        if (EFI_ERROR (Status) && (Key.UnicodeChar == CHAR_CARRIAGE_RETURN)) {
          IsTryAgain = TRUE;
        }
      }
    } while (IsTryAgain);

  }

  SafeFreePool (&StringPtr);
  SafeFreePool (&String2Ptr);

  return EFI_SUCCESS;
}

/**
 Update Security Erase supported.

  @param  SuBrowser                     Points to SETUP_UTILITY_BROWSER_DATA.
  @param  HddIndex                      HDD Index.
  @param  SecurityEraseSupported        Points to Security Erase supported.
  
  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
L05UpdateSecurityEraseSupported (
  IN  SETUP_UTILITY_BROWSER_DATA        *SuBrowser,
  IN  UINT16                            HddIndex,
  OUT UINT8                             *SecurityEraseSupported
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  HDD_PASSWORD_HDD_INFO                 *HddInfo;
  UINTN                                 NumOfHdd;
  BOOLEAN                               IsAtaPassThru;
  EFI_HANDLE                            ControllerHandle;
  EFI_ATA_PASS_THRU_PROTOCOL            *AtaPassThru;
  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL    *NvmePassThru;
  NVME_ADMIN_CONTROLLER_DATA            ControllerData;
  UINT32                                NamespaceId;

  SUCInfo          = SuBrowser->SUCInfo;
  HddInfo          = SUCInfo->HddPasswordScuData[HddIndex].HddInfo;
  NumOfHdd         = SUCInfo->NumOfHdd;
  ControllerHandle = NULL;
  AtaPassThru      = NULL;
  NvmePassThru     = NULL;

  Status = GetDevicePassThru (
             SuBrowser,
             HddIndex,
             &IsAtaPassThru,
             &ControllerHandle,
             &AtaPassThru,
             &NvmePassThru
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (IsAtaPassThru) {
    //
    // ATA Command Set - 4 (ACS-4) 
    //   4.17 Security feature set
    //     4.17.6 Commands
    //       A device that supports the Security feature set shall implement the following commands:
    //       a) SECURITY SET PASSWORD (see 7.38);
    //       b) SECURITY UNLOCK (see 7.39);
    //       c) SECURITY ERASE PREPARE (see 7.35);
    //       d) SECURITY ERASE UNIT (see 7.36);
    //       e) SECURITY FREEZE LOCK (see 7.37); and
    //       f) SECURITY DISABLE PASSWORD (see 7.34).
    //
    //   9.11.8.3.7 Security enabled (SECURITY ENABLED bit)
    //     The IDENTIFY DEVICE data contains a copy of the SECURITY ENABLED bit (see IDENTIFY DEVICE data word 128 in table 44).
    //
    if ((HddInfo->HddSecurityStatus & HDD_SUPPORT_BIT) == HDD_SUPPORT_BIT) {
      *SecurityEraseSupported = TRUE;
    }

  } else {
    NamespaceId = NVME_ALL_NAMESPACES;

    while (TRUE) {
      Status = NvmePassThru->GetNextNamespace (NvmePassThru, &NamespaceId);

      if (EFI_ERROR (Status)) {
        if (Status == EFI_NOT_FOUND) {
          Status = EFI_SUCCESS;
          break;

        } else {
          return Status;
        }
      }

      // Due to the fact that RAID Driver expects other value of NamespaceId parameter than AHCI driver,
      // need to send the Identify Controller command twice - once with the NamespaceId retrieved
      // using GetNextNamespace. If it fails we need to set NamespaceId = 0.
      ZeroMem (&ControllerData, sizeof (NVME_ADMIN_CONTROLLER_DATA));
      Status = NvmeIdentifyController (NvmePassThru, NamespaceId, (VOID *) &ControllerData);

      if (EFI_ERROR (Status)) {
        Status = NvmeIdentifyController (NvmePassThru, NVME_CONTROLLER_ID, (VOID *) &ControllerData);

        if (EFI_ERROR (Status)) {
          return Status;
        }
      }

      if ((ControllerData.Oacs & FORMAT_NVM_SUPPORTED) == FORMAT_NVM_SUPPORTED) {
        *SecurityEraseSupported = TRUE;
        break;
      }
    }
  }

  return EFI_SUCCESS;
}

/**
 Update string of Security Erase item in Security menu

  @param  HiiHandle                     Specific HII handle for Security menu.
  @param  HddPasswordScuData            Pointer to the Harddisk information array.
**/
VOID
L05UpdateSecurityEraseItemString (
  IN  EFI_HII_HANDLE                    HiiHandle,
  IN  HDD_PASSWORD_SCU_DATA             *HddPasswordScuData
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_BROWSER_DATA            *SuBrowser;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  UINTN                                 NumOfHdd;
  UINTN                                 NumberLength;
  UINTN                                 Index;

  SuBrowser = NULL;
  Status = GetSetupUtilityBrowserData (&SuBrowser);

  if (Status != EFI_SUCCESS || SuBrowser == NULL || !mIsL05InitSecurityEraseItem) {
    return;
  }

  SUCInfo  = SuBrowser->SUCInfo;
  NumOfHdd = SUCInfo->NumOfHdd;
  L05_GET_NUMBER_LENGTH (NumOfHdd, NumberLength);

  for (Index = 0; Index < NumOfHdd; Index++) {
    //
    // Update "Security Erase HDD # Data" prompt string.
    //
    L05HiiSetStringByFormatString (
      HiiHandle,
      mL05HddPasswordConfig->SecurityErasePromptStringId[Index],
      STRING_TOKEN (L05_STR_SECURITY_ERASE_HDD_DATA_PROMPT),
      (NumberLength * sizeof (CHAR16)),
      (Index + 1)
      );

    //
    // Update "Security Erase HDD # Data" help string.
    //
    L05HiiSetStringByFormatString (
      HiiHandle,
      mL05HddPasswordConfig->SecurityEraseHelpStringId[Index],
      STRING_TOKEN (L05_STR_SECURITY_ERASE_HDD_DATAD_HELP),
      (NumberLength * sizeof (CHAR16)),
      (Index + 1)
      );
  }

  return;
}

/**
 Initialize Security Erase HDD Data item in Security menu
   [enovo BIOS Setup Design Guide V2.9]
     Security page - Security Erase HDD # Data

  @param  HiiHandle                     Specific HII handle for Security menu.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Orther                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
L05InitSecurityEraseItem (
  IN  EFI_HII_HANDLE                    HiiHandle
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  SETUP_UTILITY_BROWSER_DATA            *SuBrowser;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  VOID                                  *StartOpCodeHandle;
  VOID                                  *EndOpCodeHandle;
  EFI_IFR_GUID_LABEL                    *OptionStartLabel;
  EFI_IFR_GUID_LABEL                    *OptionEndLabel;
  UINTN                                 NumOfHdd;
  UINTN                                 NumberLength;
  EFI_IFR_OP_HEADER                     *IfrOpHeader;

  IfrOpHeader = NULL;

  SuBrowser = NULL;
  Status = GetSetupUtilityBrowserData (&SuBrowser);

  if (Status != EFI_SUCCESS || SuBrowser == NULL) {
    return Status;
  }

  SUCInfo            = SuBrowser->SUCInfo;
  NumOfHdd           = SUCInfo->NumOfHdd;
  L05_GET_NUMBER_LENGTH (NumOfHdd, NumberLength);

  if (NumOfHdd == 0) {
    return EFI_UNSUPPORTED;
  }

  //
  // Allocate space for creation of Update Data Buffer
  //
  StartOpCodeHandle = HiiAllocateOpCodeHandle ();
  ASSERT (StartOpCodeHandle != NULL);

  EndOpCodeHandle = HiiAllocateOpCodeHandle ();
  ASSERT (EndOpCodeHandle != NULL);

  //
  // Create Hii Extend Label OpCode as the start opcode for Security Erase HDD Data
  //
  OptionStartLabel = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (StartOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
  OptionStartLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
  OptionStartLabel->Number       = L05_LABEL_SECURITY_ERASE_HDD_DATA;

  //
  // Create Hii Extend Label OpCode as the end opcode for Security Erase HDD Data
  //
  OptionEndLabel = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (EndOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
  OptionEndLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
  OptionEndLabel->Number       = L05_LABEL_SECURITY_ERASE_HDD_DATA_END;

  //
  // Show Security Erase HDD Data
  // - "Security Erase HDD # Data"
  //
  for (Index = 0; Index < NumOfHdd; Index++) {
//_Start_L05_HDD_PASSWORD_ENABLE_
    //
    // eMMC does not support all functions of HDD password.
    //
    if (SUCInfo->HddPasswordScuData[Index].HddInfo->ControllerMode == eMMC_MODE) {
      continue;
    }
//_End_L05_HDD_PASSWORD_ENABLE_
    
    mL05HddPasswordConfig->SecurityErasePromptStringId[Index] = L05HiiSetStringByFormatString (
                                                                  HiiHandle,
                                                                  0,
                                                                  STRING_TOKEN (L05_STR_SECURITY_ERASE_HDD_DATA_PROMPT),
                                                                  (NumberLength * sizeof (CHAR16)),
                                                                  (Index + 1)
                                                                  );

    mL05HddPasswordConfig->SecurityEraseHelpStringId[Index] = L05HiiSetStringByFormatString (
                                                                HiiHandle,
                                                                0,
                                                                STRING_TOKEN (L05_STR_SECURITY_ERASE_HDD_DATAD_HELP),
                                                                (NumberLength * sizeof (CHAR16)),
                                                                (Index + 1)
                                                                );

    //
    // suppressif L05HddPasswordCongig.SecurityEraseSupported[Index] == 0;
    //
    HiiCreateSuppressIfOpCodeEx (StartOpCodeHandle, 1);
    HiiCreateIdEqualOpCodeEx (
      StartOpCodeHandle,
      (EFI_QUESTION_ID) (Index + L05_KEY_SECURITY_ERASE_FLAG),
      0
      );

    //
    // grayoutif
    //   ideqval SystemConfig.SetUserPass == 1
    //   OR
    //   ideqval L05HddPasswordCongig.SetHddPasswordFlag[Index] == 0;
    //
    HiiCreateGrayOutIfOpCodeEx (StartOpCodeHandle, 1);
    IfrOpHeader = (EFI_IFR_OP_HEADER *) HiiCreateIdEqualOpCodeEx (
                                          StartOpCodeHandle,
                                          (EFI_QUESTION_ID) (L05_KEY_SET_USER_PASS_FLAG),
                                          1
                                          );
    IfrOpHeader->Scope = 1;
    HiiCreateIdEqualOpCodeEx (
      StartOpCodeHandle,
      (EFI_QUESTION_ID) (Index + L05_KEY_SET_HDD_PASSWORD_FLAG),
      0
      );
    HiiCreateOrOpCode (StartOpCodeHandle);
    HiiCreateEndOpCode (StartOpCodeHandle);

    HiiCreateActionOpCode (
      StartOpCodeHandle,
      (UINT16) (L05_KEY_SECURITY_ERASE_HDD_DATA + Index),
      mL05HddPasswordConfig->SecurityErasePromptStringId[Index],
      mL05HddPasswordConfig->SecurityEraseHelpStringId[Index],
      EFI_IFR_FLAG_CALLBACK,
      0
      );

    //
    // endif;
    //
    HiiCreateEndOpCode (StartOpCodeHandle);

    //
    // endif;
    //
    HiiCreateEndOpCode (StartOpCodeHandle);
  }

  //
  // suppressif  TRUE;
  //
  HiiCreateSuppressIfOpCodeEx (StartOpCodeHandle, 1);
  HiiCreateTrueOpCode (StartOpCodeHandle);

  for (Index = 0; Index < NumOfHdd; Index++) {
    //
    // Assign questionid for SecurityEraseSupported
    //
    HiiCreateNumericOpCode (
      StartOpCodeHandle,
      (EFI_QUESTION_ID) (Index + L05_KEY_SECURITY_ERASE_FLAG),
      L05_HDD_PASSWORD_CONFIGURATION_VARSTORE_ID,
      (UINT16) OFFSET_OF (L05_HDD_PASSWORD_CONFIGURATION, SecurityEraseSupported[Index]),
      STRING_TOKEN (STR_BLANK_STRING),
      STRING_TOKEN (STR_BLANK_STRING),
      EFI_IFR_FLAG_READ_ONLY,
      EFI_IFR_NUMERIC_SIZE_1 | EFI_IFR_DISPLAY_UINT_HEX,
      0,
      0xFF,
      0,
      NULL
      );
  }

  //
  // endif;
  //
  HiiCreateEndOpCode (StartOpCodeHandle);

  HiiUpdateForm (
    HiiHandle,
    NULL,
    ROOT_FORM_ID,
    StartOpCodeHandle,
    EndOpCodeHandle
    );

  HiiFreeOpCodeHandle (StartOpCodeHandle);
  HiiFreeOpCodeHandle (EndOpCodeHandle);

  mIsL05InitSecurityEraseItem = TRUE;

  return EFI_SUCCESS;
}
#endif

