/** @file
  Source file for L05Hook for Lenovo Notebook Password.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_NOTEBOOK_PASSWORD_H_
#define _L05_NOTEBOOK_PASSWORD_H_

EFI_STATUS
L05UpdateSetHddPasswordFlag (
  SYSTEM_CONFIGURATION                  *SetupBuffer
  );

EFI_STATUS
L05SetHddPassword (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *FormCallback,
  IN  EFI_HII_HANDLE                        HiiHandle,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest,
  OUT BOOLEAN                               *PState,
  IN  HDD_PASSWORD_SCU_DATA                 *HddPasswordScuData,
  IN  UINT16                                HddIndex,
  CHAR16                                    *MasterPasswordInput,
  CHAR16                                    *UserPasswordInput
  );

EFI_STATUS
L05SetHddPasswordCallback (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_HII_HANDLE                        HiiHandle,
  IN  UINT8                                 Type,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest,
  OUT BOOLEAN                               *PState,
  IN  HDD_PASSWORD_SCU_DATA                 *HddPasswordScuData,
  IN  UINT16                                HddIndex
  );

VOID
L05UpdateHddPasswordItemString (
  IN  EFI_HII_HANDLE                    HiiHandle,
  IN  HDD_PASSWORD_SCU_DATA             *HddPasswordScuData
  );

EFI_STRING_ID
L05HiiSetStringByFormatString (
  IN EFI_HII_HANDLE                     HiiHandle,
  IN EFI_STRING_ID                      UpdateStringId,
  IN EFI_STRING_ID                      FormatStringId,
  IN UINTN                              ExtraSize,
  ...
  );

EFI_STATUS
EFIAPI
L05InitHddPasswordItem (
  IN  EFI_HII_HANDLE                    HiiHandle
  );

#endif
