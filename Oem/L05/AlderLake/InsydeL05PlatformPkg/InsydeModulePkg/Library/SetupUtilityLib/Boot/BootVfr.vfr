/** @file
  The Vfr component for Boot menu

;******************************************************************************
;* Copyright (c) 2012 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

//_Start_L05_SETUP_MENU_
//#include "KernelSetupConfig.h"
#include "SetupConfig.h"
//_End_L05_SETUP_MENU_

formset
  guid     = FORMSET_ID_GUID_BOOT,
  title    = STRING_TOKEN(STR_BOOT_TITLE),
  help     = STRING_TOKEN(STR_BLANK_STRING),
  classguid = SETUP_UTILITY_FORMSET_CLASS_GUID,
  class    = SETUP_UTILITY_CLASS,
  subclass = SETUP_UTILITY_SUBCLASS,
#if defined(SETUP_IMAGE_SUPPORT) && FeaturePcdGet(PcdH2OFormBrowserLocalMetroDESupported)
  image     = IMAGE_TOKEN(IMAGE_BOOT);
#endif

//_Start_L05_SETUP_MENU_
//  varstore KERNEL_CONFIGURATION,            // This is the data structure type
  varstore SYSTEM_CONFIGURATION,            // This is the data structure type
//_End_L05_SETUP_MENU_
    varid = CONFIGURATION_VARSTORE_ID,      // Optional VarStore ID
    name  = SystemConfig,                   // Define referenced name in vfr
    guid  = SYSTEM_CONFIGURATION_GUID;      // GUID of this buffer storage

  varstore BOOT_CONFIGURATION,              // This is the data structure type
    varid = BOOT_VARSTORE_ID,               // Optional VarStore ID
    name  = BootConfig,                     // Define referenced name in vfr
    guid  = SYSTEM_CONFIGURATION_GUID;      // GUID of this buffer storage

  varstore NETWORK_CONFIGURATION,           // This is the data structure type
    varid = NETWORK_VARSTORE_ID,            // Optional VarStore ID
    name  = NetworkConfig,                  // Define referenced name in vfr
    guid  = SYSTEM_CONFIGURATION_GUID;      // GUID of this buffer storage

//_Start_L05_SETUP_MENU_
  PCH_SETUP_VARSTORE
//_End_L05_SETUP_MENU_

  form
    formid = ROOT_FORM_ID,

    title = STRING_TOKEN(STR_BOOT_TITLE);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
#if FeaturePcdGet(PcdH2OCsmSupported)

#ifndef L05_LEGACY_SUPPORT
    suppressif
      TRUE;
#endif

//_Start_L05_SETUP_MENU_
      grayoutif
#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
        //
        // [Lenovo BIOS Optane Support Version 1.4 Page 2 & 6]
        //   RST Mode is selected 
        //   Legacy boot is not supported and Legacy boot option will be grayed out for end user
        //   BIOS Setup
        //   When IRST boot mode is selected, UEFI is automatically selected and grayed out
        //
        ideqval PCH_SETUP.SataInterfaceMode == SATA_MODE_RAID
        OR
#endif
        ideqval SystemConfig.L05DeviceGuard == 1;  // 0:Disabled, 1:Enabled
//_End_L05_SETUP_MENU_

      oneof
        varid       = SystemConfig.BootType,
        questionid  = KEY_BOOT_MODE_TYPE,
        prompt      = STRING_TOKEN(STR_BOOT_TYPE_STRING),
        help        = STRING_TOKEN(STR_BOOT_TYPE_HELP),
//_Start_L05_SETUP_MENU_
//        option text = STRING_TOKEN(STR_DUAL_BOOT_TYPE_TEXT),   value = 0, flags = 0|INTERACTIVE;
//        option text = STRING_TOKEN(STR_LEGACY_BOOT_TYPE_TEXT), value = 1, flags = 0|INTERACTIVE;
//        option text = STRING_TOKEN(STR_EFI_BOOT_TYPE_TEXT),    value = 2, flags = DEFAULT|INTERACTIVE;
        option text = STRING_TOKEN(STR_EFI_BOOT_TYPE_TEXT),    value = 2, flags = DEFAULT|INTERACTIVE;
        option text = STRING_TOKEN(STR_DUAL_BOOT_TYPE_TEXT),   value = 0, flags = 0|INTERACTIVE;
//_End_L05_SETUP_MENU_
      endoneof;

//_Start_L05_SETUP_MENU_
      endif;
//_End_L05_SETUP_MENU_

//_Start_L05_SETUP_MENU_
    suppressif
      ideqvallist SystemConfig.L05DeviceGuard == 0;  // 0:Disabled, 1:Enabled
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_DEVICE_GUARD_UNSELECTABLE_STRING),
        text   = STRING_TOKEN(STR_BLANK_STRING);
    endif;
//_End_L05_SETUP_MENU_

#ifdef L05_GRAPHIC_UI_ENABLE
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(STR_BLANK_STRING);
#endif

#ifndef L05_LEGACY_SUPPORT
    endif;
#endif

//_Start_L05_SETUP_MENU_
    suppressif
      ideqval SystemConfig.BootType == 2;
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.BootNormalPriority,
        questionid  = KEY_NORMAL_BOOT_PRIORITY,
        prompt      = STRING_TOKEN(L05_STR_BOOT_PRIORITY_STRING),
        help        = STRING_TOKEN(L05_STR_BOOT_PRIORITY_HELP),
        option text = STRING_TOKEN(L05_STR_UEFI_FIRST_TEXT),    value = 0, flags = DEFAULT | INTERACTIVE;
        option text = STRING_TOKEN(L05_STR_LEGACY_FIRST_TEXT),  value = 1, flags = 0 | INTERACTIVE;
      endoneof;
    endif;
    endif;
//_End_L05_SETUP_MENU_

#else
      suppressif TRUE;
        oneof
          varid       = SystemConfig.BootType,
          prompt      = STRING_TOKEN(0),
          help        = STRING_TOKEN(0),
          option text = STRING_TOKEN(0),    value = 2, flags = DEFAULT;
        endoneof;
      endif;
#endif

//_Start_L05_SETUP_MENU_
suppressif
  TRUE;
//_End_L05_SETUP_MENU_

      oneof
        varid       = SystemConfig.QuickBoot,
        questionid  = KEY_QUICK_BOOT,
        prompt      = STRING_TOKEN(STR_QUICK_BOOT_STRING),
        help        = STRING_TOKEN(STR_QUICK_BOOT_HELP),
        option text = STRING_TOKEN(STR_ENABLED_TEXT),   value = 1, flags = DEFAULT;
        option text = STRING_TOKEN(STR_DISABLED_TEXT),  value = 0, flags = 0;
      endoneof;

      oneof
        varid       = SystemConfig.QuietBoot,
        questionid  = KEY_QUIET_BOOT,
        prompt      = STRING_TOKEN(STR_QUIET_BOOT_STRING),
        help        = STRING_TOKEN(STR_QUIET_BOOT_HELP),
        option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 1, flags = DEFAULT;
        option text = STRING_TOKEN(STR_DISABLED_TEXT),   value = 0, flags = 0;
      endoneof;

#if FeaturePcdGet(PcdH2ONetworkSupported)
    suppressif
      ideqvallist SystemConfig.BootType == 1;
    oneof
      varid       = SystemConfig.PxeBootToLan,
      questionid  = KEY_PXE_BOOT_TO_LAN,
      prompt      = STRING_TOKEN(NETWORK_STACK_CONFIG_STRING),
      help        = STRING_TOKEN(NETWORK_STACK_CONFIG_HELP),
      option text = STRING_TOKEN(STR_DISABLED_TEXT),  value = 0, flags = DEFAULT|INTERACTIVE;
      option text = STRING_TOKEN(STR_ENABLED_TEXT),   value = 1, flags = 0;
    endoneof;
    endif;

    suppressif
      ideqvallist SystemConfig.BootType == 0 2;
    oneof
      varid       = SystemConfig.PxeBootToLan,
      questionid  = KEY_PXE_BOOT_TO_LAN_IN_DUAL_LEGACY,
      prompt      = STRING_TOKEN(STR_PXE_BOOT_TO_LAN_STRING),
      help        = STRING_TOKEN(STR_PXE_BOOT_TO_LAN_HELP),
      option text = STRING_TOKEN(STR_DISABLED_TEXT),  value = 0, flags = DEFAULT;
      option text = STRING_TOKEN(STR_ENABLED_TEXT),   value = 1, flags = 0;
    endoneof;
    endif;

    suppressif
    ideqvallist SystemConfig.BootType == 1;

    grayoutif
    ideqvallist SystemConfig.PxeBootToLan == 0;
      oneof
      varid       = SystemConfig.NetworkProtocol,
      questionid  = KEY_NETWORK_PROTOCOL,
      prompt      = STRING_TOKEN(STR_IP_CONFIG_STRING),
      help        = STRING_TOKEN(STR_IP_CONFIG_HELP_V2),
      option text = STRING_TOKEN(STR_DISABLED_TEXT),   value = UEFI_NETWORK_BOOT_OPTION_DISABLE, flags = DEFAULT; // UEFI PXE Disable, But support Network Stack
      suppressif
      ideqvallist SystemConfig.BootType == 1
      OR
      ideqvallist SystemConfig.PxeBootToLan == 0;
        //
        // This section for IPv4
        //
        option text = STRING_TOKEN(STR_PXE_IPV4_TEXT),          value = UEFI_NETWORK_BOOT_OPTION_PXE_IPV4,      flags = 0;
#if FeaturePcdGet(PcdH2ONetworkHttpSupported)
        option text = STRING_TOKEN(STR_HTTP_IPV4_TEXT),         value = UEFI_NETWORK_BOOT_OPTION_HTTP_IPV4,     flags = 0;
        option text = STRING_TOKEN(STR_HTTP_PXE_IPV4_TEXT),     value = UEFI_NETWORK_BOOT_OPTION_HTTP_PXE_IPV4, flags = 0;
#endif
        //
        // This section for IPv6
        //
#if FeaturePcdGet(PcdH2ONetworkIpv6Supported)
        option text = STRING_TOKEN(STR_PXE_IPV6_TEXT),          value = UEFI_NETWORK_BOOT_OPTION_PXE_IPV6,      flags = 0;
  #if FeaturePcdGet(PcdH2ONetworkHttpSupported)
        option text = STRING_TOKEN(STR_HTTP_IPV6_TEXT),         value = UEFI_NETWORK_BOOT_OPTION_HTTP_IPV6,     flags = 0;
        option text = STRING_TOKEN(STR_HTTP_PXE_IPV6_TEXT),     value = UEFI_NETWORK_BOOT_OPTION_HTTP_PXE_IPV6, flags = 0;
  #endif
        //
        // This section for IPv4 and IPv6 at the same time.
        //
        option text = STRING_TOKEN(STR_PXE_BOTH_TEXT),          value = UEFI_NETWORK_BOOT_OPTION_PXE_BOTH,      flags = 0;
  #if FeaturePcdGet(PcdH2ONetworkHttpSupported)
        option text = STRING_TOKEN(STR_HTTP_BOTH_TEXT),         value = UEFI_NETWORK_BOOT_OPTION_HTTP_BOTH,     flags = 0;
        option text = STRING_TOKEN(STR_HTTP_PXE_BOTH_TEXT),     value = UEFI_NETWORK_BOOT_OPTION_HTTP_PXE_BOTH, flags = 0;
  #endif
#endif
      endif
      suppressif
      ideqvallist SystemConfig.BootType == 2;
        option text = STRING_TOKEN(STR_LEGACY),        value = 3, flags = 0;       // Legacy PXE OPROM, not support Network Stack
      endif
      endoneof;
    endif;
    endif;

    suppressif
    ideqvallist SystemConfig.NetworkProtocol == UEFI_NETWORK_BOOT_OPTION_DISABLE;
      numeric
        varid       = NetworkConfig.NetworkProtocolRetryTime,
        questionid  = KEY_NETWORK_PROTOCOL_RETRY_POLICY,
        prompt      = STRING_TOKEN(STR_IP_RETRY_STRING),
        help        = STRING_TOKEN(STR_IP_RETRY_HELP),
        minimum     = 0,
        maximum     = H2O_NETWORK_BOOT_INFINITELY_RETRY,
        step        = 1,
      endnumeric;
    endif;
#endif


    oneof
      varid       = SystemConfig.PUISEnable,
      questionid  = KEY_PUIS_ENABLE,
      prompt      = STRING_TOKEN(STR_PUIS_SUPPORT_STRING),
      help        = STRING_TOKEN(STR_PUIS_SUPPORT_HELP),
      option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 1, flags = 0;
      option text = STRING_TOKEN(STR_DISABLED_TEXT),   value = 0, flags = DEFAULT;
    endoneof;

    oneof
      varid       = SystemConfig.StorageOpromAccessRight,
      questionid  = KEY_STORAGE_OPROM_ACCESS_ENABLE,
      prompt      = STRING_TOKEN(STR_STORAGE_OPROM_ACCESS_SUPPORT_STRING),
      help        = STRING_TOKEN(STR_STORAGE_OPROM_ACCESS_SUPPORT_HELP),
      option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 1, flags = DEFAULT;
      option text = STRING_TOKEN(STR_DISABLED_TEXT),   value = 0, flags = 0;
    endoneof;
#if FeaturePcdGet(PcdH2OEsataDriveBootPolicy)
    oneof
      varid       = SystemConfig.EsataDriveBootControl,
      questionid  = KEY_ESATA_BOOT_ACCESS_ENABLE,
      prompt      = STRING_TOKEN(STR_ESATA_BOOT_ACCESS_SUPPORT_STRING),
      help        = STRING_TOKEN(STR_ESATA_BOOT_ACCESS_SUPPORT_HELP),
      option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 1, flags = DEFAULT;
      option text = STRING_TOKEN(STR_DISABLED_TEXT),   value = 0, flags = 0;
    endoneof;
#endif    

      oneof
        varid      = SystemConfig.NewPositionPolicy,
        questionid = KEY_NEW_POSITION_POLICY,
        prompt     = STRING_TOKEN(STR_ADDPOSITION_TEXT),
        help       = STRING_TOKEN(STR_ADDPOSITION_TEXT_HELP),
        option text = STRING_TOKEN(STR_ADDPOSITION_FIRST), value = 0, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_ADDPOSITION_LAST), value = 1, flags = INTERACTIVE;
        option text = STRING_TOKEN(STR_ADDPOSITION_AUTO), value = 2, flags = DEFAULT | INTERACTIVE;
      endoneof;

//_Start_L05_SETUP_MENU_
endif;
//_End_L05_SETUP_MENU_

    endif;

//_Start_L05_SETUP_MENU_
suppressif
  TRUE;
//_End_L05_SETUP_MENU_

#if FeaturePcdGet(Pcd64BitAmlSupported)
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.AcpiVer,
        questionid  = KEY_ACPI_VER,
        prompt      = STRING_TOKEN(STR_ACPIVER_BOOT_STRING),
        help        = STRING_TOKEN(STR_ACPIVER_BOOT_HELP),
        option text = STRING_TOKEN(STR_ACPI40_TEXT), value = 2, flags = 0;
        option text = STRING_TOKEN(STR_ACPI50_TEXT), value = 3, flags = DEFAULT;
        option text = STRING_TOKEN(STR_ACPI60_TEXT), value = 4, flags = 0;
        option text = STRING_TOKEN(STR_ACPI61_TEXT), value = 5, flags = 0;
        option text = STRING_TOKEN(STR_ACPI62_TEXT), value = 6, flags = 0;
        option text = STRING_TOKEN(STR_ACPI63_TEXT), value = 7, flags = 0;
      endoneof;
    endif;
#else
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.AcpiVer,
        questionid  = KEY_ACPI_VER,
        prompt      = STRING_TOKEN(STR_ACPIVER_BOOT_STRING),
        help        = STRING_TOKEN(STR_ACPIVER_BOOT_HELP),
        option text = STRING_TOKEN(STR_ACPI30_TEXT), value = 1, flags = 0;
        option text = STRING_TOKEN(STR_ACPI40_TEXT), value = 2, flags = 0;
        option text = STRING_TOKEN(STR_ACPI50_TEXT), value = 3, flags = DEFAULT;
        option text = STRING_TOKEN(STR_ACPI60_TEXT), value = 4, flags = 0;
        option text = STRING_TOKEN(STR_ACPI61_TEXT), value = 5, flags = 0;
        option text = STRING_TOKEN(STR_ACPI62_TEXT), value = 6, flags = 0;
        option text = STRING_TOKEN(STR_ACPI63_TEXT), value = 7, flags = 0;
      endoneof;
    endif;
#endif

//_Start_L05_SETUP_MENU_
endif;
//_End_L05_SETUP_MENU_

//_Start_L05_SETUP_MENU_
    suppressif
      ideqval SystemConfig.L05FastBootImplement == 0;
    grayoutif
      ideqval SystemConfig.UserAccessLevel == 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.L05FastBoot,
        questionid  = L05_KEY_FAST_BOOT,
        prompt      = STRING_TOKEN(L05_STR_FASTBOOT_SUPPORT_STRING),
        help        = STRING_TOKEN(L05_STR_FASTBOOT_SUPPORT_HELP),
        option text = STRING_TOKEN(STR_ENABLED_TEXT),   value = 1, flags = DEFAULT;
        option text = STRING_TOKEN(STR_DISABLED_TEXT),  value = 0, flags = 0;
      endoneof;
    endif;
    endif;
//_End_L05_SETUP_MENU_

    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid   = SystemConfig.UsbBoot,
        questionid  = KEY_USB_BOOT,
        prompt      = STRING_TOKEN(STR_USB_BOOT_STRING),
        help        = STRING_TOKEN(STR_USB_BOOT_HELP),
        option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 0, flags = DEFAULT;
        option text = STRING_TOKEN(STR_DISABLED_TEXT), value = 1, flags = 0;
      endoneof;

//_Start_L05_SETUP_MENU_
#if 0
      label OPROM_STORAGE_DEVICE_BOOT_LABEL;
#endif
//_End_L05_SETUP_MENU_
    endif;

//_Start_L05_SETUP_MENU_
    grayoutif
      ideqval SystemConfig.UserAccessLevel == 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.PxeBootToLan,
        questionid  = L05_KEY_PXE_BOOT_TO_LAN,
        prompt      = STRING_TOKEN(STR_PXE_BOOT_TO_LAN_STRING),
        help        = STRING_TOKEN(STR_PXE_BOOT_TO_LAN_HELP),
        option text = STRING_TOKEN(STR_ENABLED_TEXT),   value = 1, flags = 0;
        option text = STRING_TOKEN(STR_DISABLED_TEXT),  value = 0, flags = DEFAULT;
      endoneof;
    endif;

    grayoutif
      ideqval SystemConfig.UserAccessLevel == 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
    suppressif
      ideqvallist SystemConfig.PxeBootToLan == 0;
      oneof
        varid       = SystemConfig.L05PxeIpv4First,
        questionid  = L05_KEY_PXE_IPV4_FIRST,
        prompt      = STRING_TOKEN(L05_PXE_IPV4_FIRST_STRING),
        help        = STRING_TOKEN(L05_PXE_IPV4_FIRST_HELP),
        option text = STRING_TOKEN(STR_ENABLED_TEXT),    value = 1, flags = DEFAULT;
        option text = STRING_TOKEN(STR_DISABLED_TEXT),   value = 0, flags = 0;
        endoneof;
    endif;
    endif;
//_End_L05_SETUP_MENU_

#ifdef L05_SECURE_SUITE_SUPPORT
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      text
        help   = STRING_TOKEN(L05_STR_WIPE_STORAGE_DEVICE_HELP),
        text   = STRING_TOKEN(L05_STR_WIPE_STORAGE_DEVICE_STRING),
        text   = STRING_TOKEN(L05_STR_ENTER_TEXT),
        flags  = INTERACTIVE,
        key    = L05_KEY_SECURE_SUITE_APP;
    endif;
#endif

//_Start_L05_SETUP_MENU_
#if 0
#if FeaturePcdGet(PcdH2OCsmSupported)
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.BootNormalPriority,
        questionid  = KEY_NORMAL_BOOT_PRIORITY,
        prompt      = STRING_TOKEN(STR_EFI_DEVICE_FIRST_STRING),
        help        = STRING_TOKEN(STR_EFI_DEVICE_FIRST_HELP),
        option text = STRING_TOKEN(STR_EFI_DEVICE_FIRST_DISABLED),  value = 1, flags = 0 | INTERACTIVE;
        option text = STRING_TOKEN(STR_EFI_DEVICE_FIRST_ENABLED),   value = 0, flags = DEFAULT | INTERACTIVE;
      endoneof;
    endif;
#else
    suppressif TRUE;
      oneof
        varid       = SystemConfig.BootNormalPriority,
        questionid  = KEY_NORMAL_BOOT_PRIORITY,
        prompt      = STRING_TOKEN(STR_EFI_DEVICE_FIRST_STRING),
        help        = STRING_TOKEN(STR_EFI_DEVICE_FIRST_HELP),
        option text = STRING_TOKEN(STR_ENABLED_TEXT),   value = 0, flags = DEFAULT | INTERACTIVE;
      endoneof;
    endif;
#endif
#endif
//_End_L05_SETUP_MENU_

//_Start_L05_SETUP_MENU_
suppressif
  TRUE;
//_End_L05_SETUP_MENU_

    suppressif
      ideqvallist SystemConfig.BootType == 0 1;
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.Win8FastBoot,
        questionid  = KEY_WIN8_FAST_BOOT,
        prompt      = STRING_TOKEN(STR_UEFI_OS_FAST_BOOT_STRING),
        help        = STRING_TOKEN(STR_UEFI_OS_FAST_BOOT_HELP),
        option text = STRING_TOKEN(STR_ENABLED_TEXT),  value = 0, flags = DEFAULT;
        option text = STRING_TOKEN(STR_DISABLED_TEXT), value = 1, flags = 0;
      endoneof;
    endif;
    endif;

    suppressif
      ideqvallist SystemConfig.BootType == 0 1
      OR
      ideqval SystemConfig.Win8FastBoot == 1;
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.UsbHotKeySupport,
        questionid  = KEY_USB_HOT_KEY_SUPPORT,
        prompt      = STRING_TOKEN(STR_USB_HOT_KEY_SUPPORT_STRING),
        help        = STRING_TOKEN(STR_USB_HOT_KEY_SUPPORT_HELP),
        option text = STRING_TOKEN(STR_DISABLED_TEXT),  value = 0, flags = DEFAULT;
        option text = STRING_TOKEN(STR_ENABLED_TEXT),   value = 1, flags = 0;
      endoneof;
    endif;
    endif;

    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      numeric
        varid      = SystemConfig.Timeout,
        questionid = KEY_BOOT_TIMEOUT,
        prompt     = STRING_TOKEN(STR_BOOT_TIMEOUT_STRING),
        help       = STRING_TOKEN(STR_BOOT_TIMEOUT_HELP),
        minimum    = 0,
        maximum    = PcdGet16 (PcdPlatformBootTimeOutMax),
        step       = 1,
        default    = PcdGet16 (PcdPlatformBootTimeOut),
      endnumeric;
    endif;

    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.AutoFailover,
        questionid  = KEY_AUTO_FAILOVER,
        prompt      = STRING_TOKEN(STR_AUTO_FAILOVER_STRING),
        help        = STRING_TOKEN(STR_AUTO_FAILOVER_HELP),
        option text = STRING_TOKEN(STR_DISABLED_TEXT),  value = 0, flags = 0;
        option text = STRING_TOKEN(STR_ENABLED_TEXT),   value = 1, flags = DEFAULT;
      endoneof;
    endif;

//_Start_L05_SETUP_MENU_
endif;
//_End_L05_SETUP_MENU_

    subtitle
     text = STRING_TOKEN(STR_BLANK_STRING);

//_Start_L05_SETUP_MENU_
suppressif
  TRUE;
//_End_L05_SETUP_MENU_

    suppressif
      ideqval SystemConfig.BootType == 1
      OR
      ideqval BootConfig.HaveEfiBootDev == 0;
      goto BOOT_DEVICE_EFI_FORM_ID,
        questionid = KEY_BOOT_DEVICE_EFI_FORM,
        prompt = STRING_TOKEN(STR_EFI),
        help   = STRING_TOKEN(STR_EFI_HELP);
    endif;

    suppressif
      ideqval SystemConfig.BootType == 2
      OR
      ideqval BootConfig.HaveLegacyBootDev == 0;
      goto BOOT_OPTION_FORM_ID,
        questionid = KEY_BOOT_OPTION_FORM,
        prompt = STRING_TOKEN(STR_LEGACY),
        help   = STRING_TOKEN(STR_LEGACY_HELP);
    endif;

//_Start_L05_SETUP_MENU_
endif;
//_End_L05_SETUP_MENU_

//_Start_L05_SETUP_MENU_
    //
    // By L05 spec 1.29, user can modify boot sequence too.
    //
    suppressif
      ideqval SystemConfig.BootType == 1;
#if FeaturePcdGet(PcdH2OFormBrowserLocalMetroDESupported)
      text
        help = STRING_TOKEN(L05_STR_GUI_BOOT_PRIORITY_ORDER_HELP),
        text = STRING_TOKEN(STR_EFI);
#endif
      subtitle
        text = STRING_TOKEN(STR_EFI);
    grayoutif
      ideqval SystemConfig.L05DeviceGuard == 1  // 0:Disabled, 1:Enabled
      OR
      //
      // [Natural File Guard Design Guide V1.03]
      //   2.1 BIOS Setup
      //     After enabled, boot device will be limited to internal storage.
      //
      // Boot order need locked when natural file guard enabled.
      //
      ideqval SystemConfig.L05NaturalFileGuard == 1;  // 0:Disabled, 1:Enabled
        label EFI_BOOT_DEVICE_LABEL;
    endif;
    endif;

    suppressif
      ideqval SystemConfig.BootType == 2;
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(STR_BLANK_STRING);
#if FeaturePcdGet(PcdH2OFormBrowserLocalMetroDESupported)
      text
        help = STRING_TOKEN(L05_STR_GUI_BOOT_PRIORITY_ORDER_HELP),
        text = STRING_TOKEN(STR_LEGACY);
#endif
      subtitle
        text = STRING_TOKEN(STR_BOOT_OPTIONS);
    grayoutif
      ideqval SystemConfig.L05DeviceGuard == 1  // 0:Disabled, 1:Enabled
      OR
      //
      // [Natural File Guard Design Guide V1.03]
      //   2.1 BIOS Setup
      //     After enabled, boot device will be limited to internal storage.
      //
      // Boot order need locked when natural file guard enabled.
      //
      ideqval SystemConfig.L05NaturalFileGuard == 1;  // 0:Disabled, 1:Enabled
        label BOOT_LEGACY_ADV_BOOT_LABEL;
    endif;
    endif;

    suppressif
      ideqvallist SystemConfig.L05DeviceGuard == 0;  // 0:Disabled, 1:Enabled
      text
        help   = STRING_TOKEN(STR_BLANK_STRING),
        text   = STRING_TOKEN(L05_STR_DEVICE_GUARD_UNSELECTABLE_STRING),
        text   = STRING_TOKEN(STR_BLANK_STRING);
    endif;
//_End_L05_SETUP_MENU_

    link;

  endform;

  //
  //  Sub Form BOOT_DEVICE_EFI_FORM_ID
  //
  form
    formid = BOOT_DEVICE_EFI_FORM_ID, title = STRING_TOKEN(STR_EFI);

    subtitle
      text = STRING_TOKEN(STR_EFI);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);
    grayoutif
      (ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1)
      OR
      ideqval SystemConfig.NewPositionPolicy == 2;
      label EFI_BOOT_DEVICE_LABEL;
    endif;
  endform;

  form
    formid = BOOT_HIDDEN_BOOT_TYPE_ORDER_ID, title = STRING_TOKEN(STR_BOOT_TYPE_ORDER_OPTIONS);

    label HIDDEN_BOOT_TYPE_ORDER_LABEL;
  endform;

  //
  //  Sub Form BOOT_OPTION_FORM_ID
  //
  form
    formid = BOOT_OPTION_FORM_ID, title = STRING_TOKEN(STR_LEGACY);

    subtitle
      text = STRING_TOKEN(STR_BOOT_OPTIONS);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);

    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      oneof
        varid       = SystemConfig.LegacyNormalMenuType,
        questionid  = KEY_LEGACY_NORMAL_BOOT_MENU,
        prompt      = STRING_TOKEN(STR_LEGACY_NORMAL_OPTIONS),
        help        = STRING_TOKEN(STR_LEGACY_NORMAL_HELP),
        option text = STRING_TOKEN(STR_NORMAL),     value = 0, flags = INTERACTIVE | DEFAULT;
        option text = STRING_TOKEN(STR_ADVANCE),    value = 1, flags = INTERACTIVE | 0;
      endoneof;
    endif;

    subtitle
         text = STRING_TOKEN(STR_BLANK_STRING);

    suppressif
      ideqval SystemConfig.LegacyNormalMenuType == 1
      OR
      ideqval BootConfig.HaveLegacyBootDevTypeOrder == 0;
      goto BOOT_DEVICE_LEG_NOR_BOOT_ID,
        questionid = KEY_BOOT_DEVICE_LEG_NOR_BOOT,
        prompt = STRING_TOKEN(STR_BOOT_TYPE_ORDER_OPTIONS),
        help   = STRING_TOKEN(STR_BOOT_TYPE_ORDER_HELP);
    endif;

    suppressif
      ideqval SystemConfig.LegacyNormalMenuType == 1
      OR
      ideqval BootConfig.NoBootDevs[0] == 0;
      goto BOOT_DEVICE_FDD_FORM_ID,
        questionid = KEY_BOOT_DEVICE_FDD_FORM,
        prompt = STRING_TOKEN(STR_FLOPPY_BOOT),
        help   = STRING_TOKEN(STR_FLOPPY_BOOT_HELP);
    endif;

    suppressif
      ideqval SystemConfig.LegacyNormalMenuType == 1
      OR
      ideqval BootConfig.NoBootDevs[1] == 0;
      goto BOOT_DEVICE_HDD_FORM_ID,
        questionid = KEY_BOOT_DEVICE_HDD_FORM,
        prompt = STRING_TOKEN(STR_HDD_BOOT),
        help   = STRING_TOKEN(STR_HDD_BOOT_HELP);
    endif;

    suppressif
      ideqval SystemConfig.LegacyNormalMenuType == 1
      OR
      ideqval BootConfig.NoBootDevs[2] == 0;
      goto BOOT_DEVICE_CD_FORM_ID,
        questionid = KEY_BOOT_DEVICE_CD_FORM,
        prompt = STRING_TOKEN(STR_CD_BOOT),
        help   = STRING_TOKEN(STR_CD_BOOT_HELP);
    endif;

    suppressif
      ideqval SystemConfig.LegacyNormalMenuType == 1
      OR
      ideqval BootConfig.NoBootDevs[3] == 0;
      goto BOOT_DEVICE_PCMCIA_FORM_ID,
        questionid = KEY_BOOT_DEVICE_PCMCIA_FORM,
        prompt = STRING_TOKEN(STR_PCMCIA_BOOT),
        help   = STRING_TOKEN(STR_PCMCIA_BOOT_HELP);
    endif;

    suppressif
      ideqval SystemConfig.LegacyNormalMenuType == 1
      OR
      ideqval BootConfig.NoBootDevs[4] == 0;
      goto BOOT_DEVICE_USB_FORM_ID,
        questionid = KEY_BOOT_DEVICE_USB_FORM,
        prompt = STRING_TOKEN(STR_USB_BOOT),
        help   = STRING_TOKEN(STR_USB_BOOT_ORDER_HELP);
    endif;

    suppressif
      ideqval SystemConfig.LegacyNormalMenuType == 1
      OR
      ideqval BootConfig.NoBootDevs[5] == 0;
      goto BOOT_EMBED_NETWORK_FORM_ID,
        questionid = KEY_BOOT_EMBED_NETWORK_FORM,
        prompt = STRING_TOKEN(STR_EMBED_NETWORK_BOOT),
        help   = STRING_TOKEN(STR_EMBED_NETWORK_BOOT_HELP);
    endif;

    suppressif
      ideqval SystemConfig.LegacyNormalMenuType == 1
      OR
      ideqval BootConfig.NoBootDevs[6] == 0;
      goto BOOT_DEVICE_BEV_FORM_ID,
        questionid = KEY_BOOT_DEVICE_BEV_FORM,
        prompt = STRING_TOKEN(STR_BEV_BOOT),
        help   = STRING_TOKEN(STR_BEV_BOOT_HELP);
    endif;

    suppressif
      ideqval SystemConfig.LegacyNormalMenuType == 1
      OR
      ideqval BootConfig.NoBootDevs[7] == 0;
      goto BOOT_DEVICE_OTHER_FORM_ID,
        questionid = KEY_BOOT_DEVICE_OTHER_FORM,
        prompt = STRING_TOKEN(STR_OTHER_DRIVE_BOOT),
        help   = STRING_TOKEN(STR_OTHER_DRIVE_BOOT_HELP);
    endif;

    suppressif
      ideqval SystemConfig.LegacyNormalMenuType == 0
      OR
      ideqval BootConfig.HaveLegacyBootDev == 0;

      label BOOT_LEGACY_ADV_BOOT_LABEL;
    endif;
  endform;

  //
  //  Sub Form BOOT_DEVICE_LEG_NOR_BOOT_ID
  //
  form
    formid = BOOT_DEVICE_LEG_NOR_BOOT_ID, title = STRING_TOKEN(STR_BOOT_TYPE_ORDER_OPTIONS);

    subtitle
      text = STRING_TOKEN(STR_BOOT_TYPE_ORDER_OPTIONS);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);

    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      label BOOT_ORDER_LABEL;
    endif;
  endform;


  //
  //  Sub Form BOOT_DEVICE_HDD_FORM_ID
  //
  form
    formid = BOOT_DEVICE_HDD_FORM_ID, title = STRING_TOKEN(STR_HDD_BOOT);

    subtitle
      text = STRING_TOKEN(STR_HDD_BOOT);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      label HDD_BOOT_DEVICE_LABEL;
    endif;
  endform;

  //
  //  Sub Form BOOT_DEVICE_CD_FORM_ID
  //
  form
    formid = BOOT_DEVICE_CD_FORM_ID, title = STRING_TOKEN(STR_CD_BOOT);

    subtitle
      text = STRING_TOKEN(STR_CD_BOOT);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      label CD_BOOT_DEVICE_LABEL;
    endif;
  endform;

  //
  //  Sub Form BOOT_DEVICE_FDD_FORM_ID
  //
  form
    formid = BOOT_DEVICE_FDD_FORM_ID, title = STRING_TOKEN(STR_FLOPPY_BOOT);

    subtitle
      text = STRING_TOKEN(STR_FLOPPY_BOOT);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      label FDD_BOOT_DEVICE_LABEL;
    endif;
  endform;

  //
  //  Sub Form BOOT_DEVICE_OTHER_FORM_ID
  //
  form
    formid = BOOT_DEVICE_OTHER_FORM_ID, title = STRING_TOKEN(STR_OTHER_DRIVE_BOOT);

    subtitle
      text = STRING_TOKEN(STR_OTHER_DRIVE_BOOT);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      label OTHER_BOOT_DEVICE_LABEL;
    endif;
  endform;

  //
  //  Sub Form BOOT_DEVICE_PCMCIA_FORM_ID
  //
  form
    formid = BOOT_DEVICE_PCMCIA_FORM_ID, title = STRING_TOKEN(STR_PCMCIA_BOOT);

    subtitle
      text = STRING_TOKEN(STR_PCMCIA_BOOT);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      label PCMCIA_BOOT_DEVICE_LABEL;
    endif;
  endform;

  //
  //  Sub Form BOOT_DEVICE_USB_FORM_ID
  //
  form
    formid = BOOT_DEVICE_USB_FORM_ID, title = STRING_TOKEN(STR_USB_BOOT);

    subtitle
      text = STRING_TOKEN(STR_USB_BOOT);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      label USB_BOOT_DEVICE_LABEL;
    endif;
  endform;

  //
  //  Sub Form BOOT_EMBED_NETWORK_FORM_ID
  //
  form
    formid = BOOT_EMBED_NETWORK_FORM_ID, title = STRING_TOKEN(STR_EMBED_NETWORK_BOOT);

    subtitle
      text = STRING_TOKEN(STR_EMBED_NETWORK_BOOT);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      label EMBED_NETWORK_BOOT_DEVICE_LABEL;
    endif;
  endform;

  //
  //  Sub Form BOOT_DEVICE_BEV_FORM_ID
  //
  form
    formid = BOOT_DEVICE_BEV_FORM_ID, title = STRING_TOKEN(STR_BEV_BOOT);

    subtitle
      text = STRING_TOKEN(STR_BEV_BOOT);

    subtitle
      text = STRING_TOKEN(STR_BLANK_STRING);
    grayoutif
      ideqvallist SystemConfig.UserAccessLevel == 2 3
      AND
      ideqval SystemConfig.SetUserPass == 1;
      label BEV_BOOT_DEVICE_LABEL;
    endif;
  endform;

endformset;
