/** @file
  Function for determine to Show Boot Fail Confirm Dialog

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

//
// Libraries
//
#include <Library/DevicePathLib.h>  // IsDevicePathEnd, DevicePathType, DevicePathSubType, NextDevicePathNode
#include <Library/FeatureLib/OemSvcSkipPxeBootFailConfirmDialog.h>

//
// Consumed Protocols
//
#include <Protocol/DevicePath.h>  // EFI_DEVICE_PATH_PROTOCOL

//
// Device Path Check List
//
UINT8                                   mDevicePathCheckList[][2] = {
  {MESSAGING_DEVICE_PATH, MSG_IPv4_DP                   },  // PXE, IPv4 Device Path
  {MESSAGING_DEVICE_PATH, MSG_IPv6_DP                   },  // PXE, IPv6 Device Path
  {END_DEVICE_PATH_TYPE,  END_ENTIRE_DEVICE_PATH_SUBTYPE}   // End list
  };

/**
  Show Boot Fail Confirm Dialog or not

  @param  DevicePath                    Points to Boot fail Device Path

  @retval TRUE                          Show Confirm Dialog by default
  @retval FALSE                         Do not show Confirm Dialog
**/
BOOLEAN
ShowBootFailConfirmDialog (
  IN EFI_DEVICE_PATH_PROTOCOL           *DevicePath
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  BOOLEAN                               MatchedDevicePath;
  BOOLEAN                               ShowConfirmDialog;

  MatchedDevicePath = FALSE;
  ShowConfirmDialog = TRUE;

  //
  // Find the matched device (USB storage, SATA HDD)
  //
  while (!IsDevicePathEnd (DevicePath)) {

    for (Index = 0; mDevicePathCheckList[Index][0] != END_DEVICE_PATH_TYPE; Index++) {

      if (DevicePathType (DevicePath)    == mDevicePathCheckList[Index][0] &&
          DevicePathSubType (DevicePath) == mDevicePathCheckList[Index][1]) {

        MatchedDevicePath = TRUE;
        break;
      }
    }

    DevicePath = NextDevicePathNode (DevicePath);
  }

  if (MatchedDevicePath) {

    Status = OemSvcSkipPxeBootFailConfirmDialog ();

    switch (Status) {

    case EFI_MEDIA_CHANGED:
      ShowConfirmDialog = FALSE;
      break;

    case EFI_UNSUPPORTED:
    default:
      ShowConfirmDialog = TRUE;
      break;
    }
  }

  return ShowConfirmDialog;
}
