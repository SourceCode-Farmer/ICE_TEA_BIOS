/** @file
  Function for determine to Show Boot Fail Confirm Dialog

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SHOW_BOOT_FAIL_CONFIRM_DIALOG_H_
#define _SHOW_BOOT_FAIL_CONFIRM_DIALOG_H_

BOOLEAN
ShowBootFailConfirmDialog (
  IN EFI_DEVICE_PATH_PROTOCOL           *DevicePath
  );
#endif
