/** @file
  Feature Hook for Crisis Recovery Log.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CRISIS_RECOVERY_LOG_H_
#define _CRISIS_RECOVERY_LOG_H_

#ifdef L05_CRISIS_RECOVERY_LOG_ENABLE

#include <FatPeim.h>
#include <Ppi/FileAccessPei.h>
#include <Library/PrintLib.h>
#include <Library/BvdtLib.h>

#define L05_FAT_MAX_VOLUME_LABLE_LENGTH     11
#define L05_FAT_8_3_FILE_NAME_LENGTH        12
#define L05_CRISIS_LOG_SIZE                 108
#define L05_CRISIS_LOG_BIOS_VERSION_LENGTH  6

typedef enum {
  L05NotFoundCrisisFile = 0x00,
  L05ValidateCrisisFileFail,
  L05CrisisProcessFail,
  L05CrisisProcessSuccess,
  L05NotFoundBackupSbbFile,
  L05LoadBackupSbbFileSuccess,
} L05_CRISIS_LOG_TYPE;

typedef struct {
  L05_CRISIS_LOG_TYPE                   CrisisLogType;
  CHAR8                                 *CrisisLogString;
} L05_CRISIS_LOG_MAP;

VOID
L05RecordCrisisLog (
  L05_CRISIS_LOG_TYPE                   CrisisLogType,
  UINTN                                 Parameter1,
  UINTN                                 Parameter2
  );
#endif

#endif
