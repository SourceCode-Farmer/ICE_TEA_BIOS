/** @file
  Feature Hook for Crisis Recovery Log.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "CrisisRecovery.h"

#ifdef L05_CRISIS_RECOVERY_LOG_ENABLE
//
// Crisis Log Map
//
L05_CRISIS_LOG_MAP mL05CrisisLogMap[] = {
// CrisisLogType,             CrisisLogString
  {L05NotFoundCrisisFile,        "%s\nFind Crisis File: [Fail], Cannot found file"},
  {L05ValidateCrisisFileFail,    "%s\nFind Crisis File: [Fail], validate file fail"},
  {L05CrisisProcessFail,         "%s\nFind Crisis File: [Success]\n\nCrisis Process: [Fail], update address 0x%08x fail"},
  {L05CrisisProcessSuccess,      "%s\nFind Crisis File: [Success]\n\nCrisis Process: [Success]"},
  {L05NotFoundBackupSbbFile,     "%s\nFind Crisis File: [Fail], Cannot found backup SBB file"},
  {L05LoadBackupSbbFileSuccess,  "%s\nFind Crisis File: [Success], Load backup SBB file successfully"},
};

UINTN                                   mL05CrisisLogMapCount = sizeof (mL05CrisisLogMap) / sizeof (L05_CRISIS_LOG_MAP);

STATIC UINT8 BvdtSig[] = {'$', 'B', 'V', 'D', 'T'};

//
// Copy from BvdtLibAscii2Unicode () in BvdtLib.c.
//
CHAR16 *
Ascii2Unicode (
  OUT CHAR16         *UnicodeStr,
  IN  CHAR8          *AsciiStr
  )
{
  CHAR16  *Str;

  Str = UnicodeStr;

  while (TRUE) {
    *(UnicodeStr++) = (CHAR16) *AsciiStr;
    if (*(AsciiStr++) == '\0') {
      return Str;
    }
  }
}

/**
  Get crisis log index by crisis log type.

  @param  CrisisLogType                 Crisis log type.
  @param  CrisisLogIndex                A pointer to the crisis log index.

  @retval EFI_SUCCESS                   Get crisis log index successfully.
  @retval EFI_NOT_FOUND                 Can't find crisis log index.
**/
EFI_STATUS
GetCrisisLogIndexByType (
  IN  L05_CRISIS_LOG_TYPE               CrisisLogType,
  OUT UINTN                             *CrisisLogIndex
  )
{
  UINTN                                 Index;

  if (CrisisLogIndex == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  *CrisisLogIndex = (UINTN) -1;

  for (Index = 0; Index < mL05CrisisLogMapCount; Index++) {
    if (CrisisLogType == mL05CrisisLogMap[Index].CrisisLogType) {
      *CrisisLogIndex = Index;
      return EFI_SUCCESS;
    }
  }

  return EFI_NOT_FOUND;
}

/**
  Get USB device volume index.

  @param  FileAccess                    A pointer to the PEI_FILE_ACCESS_PPI.
  @param  VolumeIndex                   A pointer to the volume index.

  @retval EFI_SUCCESS                   Get volume index successfully.
  @retval EFI_NOT_FOUND                 Can't find volume index.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
GetUsbDeviceVolumeIndex (
  IN  PEI_FILE_ACCESS_PPI               *FileAccess,
  IN OUT UINTN                          *VolumeIndex
  )
{
  EFI_STATUS                            Status;
  UINTN                                 VolumeCount;
  UINTN                                 Index;
  UINT8                                 VolumeInfoBuffer[sizeof (PEI_VOLUME_INFO) + (sizeof (CHAR16) * L05_FAT_MAX_VOLUME_LABLE_LENGTH)];
  PEI_VOLUME_INFO                       *VolumeInfo;
  UINTN                                 VolumeInfoSize;
  UINTN                                 BufferSize;

  if (FileAccess == NULL || VolumeIndex == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  VolumeCount = 0;
  VolumeInfo  = NULL;

  VolumeCount = FileAccess->GetNumberOfVolumes (FileAccess);

  if (VolumeCount == 0) {
    return EFI_NOT_FOUND;
  }

  VolumeInfo = (PEI_VOLUME_INFO*) VolumeInfoBuffer;
  VolumeInfoSize = sizeof (VolumeInfoBuffer);

  for (Index = 0; Index < VolumeCount; Index++) {
    ZeroMem (VolumeInfo, VolumeInfoSize);
    BufferSize = VolumeInfoSize;

    Status = FileAccess->GetVolumeInfo (FileAccess, Index, &BufferSize, VolumeInfo);

    if (!EFI_ERROR (Status) && (VolumeInfo->DevType == UsbMassStorage)) {
      *VolumeIndex = Index;
      return EFI_SUCCESS;
    }
  }

  return EFI_NOT_FOUND;
}

/**
  Record log to file.

  @param  FileAccess                    A pointer to the PEI_FILE_ACCESS_PPI.
  @param  FileName                      A pointer to the file name.
  @param  VolumeIndex                   Volume index.
  @param  LogBuffer                     A pointer to the log buffer.
  @param  LogBufferSize                 Size of log buffer.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
RecordLogToFile (
  IN  PEI_FILE_ACCESS_PPI               *FileAccess,
  IN  CHAR16                            *FileName,
  IN  UINTN                             VolumeIndex,
  IN  UINT8                             *LogBuffer,
  IN  UINTN                             LogBufferSize
  )
{
  EFI_STATUS                            Status;
  PEI_FILE_HANDLE                       Handle;

  if (FileAccess == NULL || FileName == NULL || LogBuffer  == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = FileAccess->OpenFile (
                         FileAccess,
                         FileName,
                         &Handle,
                         EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE | EFI_FILE_MODE_CREATE,
                         EFI_FILE_ARCHIVE,
                         VolumeIndex
                         );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = FileAccess->WriteFile (
                         FileAccess,
                         Handle,
                         &LogBufferSize,
                         LogBuffer
                         );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = FileAccess->CloseFile (FileAccess, Handle);

  return Status;
}

/**
  Get crisis log file name with crisis log number.

  @param  FileAccess                    A pointer to the PEI_FILE_ACCESS_PPI.
  @param  VolumeIndex                   Volume index.
  @param  CrisisLogNumFileName          A pointer to the crisis log file name with crisis log number.
  @param  CrisisLogNumFileNameSize      Size of crisis log file name with crisis log number.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
GetCrisisLogNumFileName (
  IN  PEI_FILE_ACCESS_PPI               *FileAccess,
  IN  UINTN                             VolumeIndex,
  IN OUT CHAR16                         *CrisisLogNumFileName,
  IN  UINTN                             CrisisLogNumFileNameSize
  )
{
  EFI_STATUS                            Status;
  CHAR16                                FileName[L05_FAT_8_3_FILE_NAME_LENGTH + 1];
  CHAR16                                CrisisBiosVersion[L05_CRISIS_LOG_BIOS_VERSION_LENGTH + 1];
  PEI_FILE_HANDLE                       Handle;
  UINT8                                 LogNumber;
  UINTN                                 NumberSize;

  if (FileAccess == NULL || CrisisLogNumFileName == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  LogNumber = 0;
  NumberSize = sizeof (LogNumber);

  ZeroMem (FileName, sizeof (FileName));
  StrCpyS (FileName, sizeof (FileName), (CHAR16 *) PcdGetPtr (PcdL05CrisisRecoveryLogNumFile));

  ZeroMem (CrisisBiosVersion, sizeof (CrisisBiosVersion));
  CopyMem (CrisisBiosVersion, PcdGetPtr (PcdFirmwareVersionString), (L05_CRISIS_LOG_BIOS_VERSION_LENGTH * sizeof (CHAR16)));
  CopyMem (FileName, CrisisBiosVersion, (L05_CRISIS_LOG_BIOS_VERSION_LENGTH * sizeof (CHAR16)));

  Status = FileAccess->OpenFile (
                         FileAccess,
                         FileName,
                         &Handle,
                         EFI_FILE_MODE_READ,
                         EFI_FILE_ARCHIVE,
                         VolumeIndex
                         );

  if (!EFI_ERROR (Status)) {
    Status = FileAccess->ReadFile (
                           FileAccess,
                           Handle,
                           ReadData,
                           &NumberSize,
                           &LogNumber
                           );

    Status = FileAccess->CloseFile (FileAccess, Handle);
  }

  LogNumber++;

  //
  // Record log number to file.
  //
  Status = RecordLogToFile (FileAccess, FileName, VolumeIndex, &LogNumber, NumberSize);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  ZeroMem (CrisisLogNumFileName, CrisisLogNumFileNameSize);
  UnicodeSPrint (
    CrisisLogNumFileName,
    CrisisLogNumFileNameSize,
    (CHAR16 *) PcdGetPtr (PcdL05CrisisRecoveryLogFileFormat),
    CrisisBiosVersion,
    LogNumber
    );

  return Status;
}

/**
  Get BVDT from BIOS image.

  @param  BiosImage                     A pointer to the BIOS image.
  @param  BiosImageSize                 BIOS image size.

  @retval UINT8 *                       Return point of BIOS image BVDT.
**/
UINT8 *
GetImageBvdtPtr (
  UINT8                                 *BiosImage,
  UINTN                                 BiosImageSize
  )
{
  UINT8                                 *ImageBvdtPtr;
  UINTN                                 Index;

  if (BiosImage == NULL) {
    return NULL;
  }

  ImageBvdtPtr = NULL;

  for (Index = 0; Index < BiosImageSize; Index++) {
    ImageBvdtPtr = (UINT8 *)BiosImage + Index;
    if (CompareMem (ImageBvdtPtr, BvdtSig, sizeof (BvdtSig)) == 0) {
      //
      // $BVDT found
      //
      return ImageBvdtPtr;
    }
  }

  return NULL;
}

/**
  Get firmware version of BIOS image.

  @param  BiosImage                     A pointer to the BIOS image.
  @param  BiosImageSize                 BIOS image size.
  @param  FirmwareVersionString         A pointer to the firmware version string.
  @param  FirmwareVersionStringSize     Firmware version string size.

  @retval EFI_SUCCESS                   Get firmware version string successfully.
  @retval EFI_NOT_FOUND                 Can't find firmware version string.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
GetImageFirmwareVersion (
  UINT8                                 *BiosImage,
  UINTN                                 BiosImageSize,
  CHAR16                                *FirmwareVersionString,
  UINTN                                 FirmwareVersionStringSize
  )
{
  UINT64                                BvdtAddr;
  UINT8                                 *ImageBvdtPtr;
  CHAR16                                BvdtStr[BVDT_MAX_STR_SIZE];

  if (BiosImage == NULL || FirmwareVersionString == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  BvdtAddr = 0;
  ImageBvdtPtr = NULL;
  ZeroMem (BvdtStr, sizeof (BvdtStr));

  BvdtAddr = FdmGetNAtAddr (&gH2OFlashMapRegionBvdtGuid, 1);
  ImageBvdtPtr = (CHAR8 *) ((UINTN) BiosImage + ((UINTN) BvdtAddr - (UINTN) FdmGetBaseAddr ()));

  if (CompareMem (ImageBvdtPtr, BvdtSig, sizeof (BvdtSig)) != 0) {
    //
    // Flash map changed, can't found $BVDT.
    // Get BVDT from BIOS image
    //
    ImageBvdtPtr = NULL;
    ImageBvdtPtr = GetImageBvdtPtr (BiosImage, BiosImageSize);
    if (ImageBvdtPtr == NULL) {
      return EFI_NOT_FOUND;
    }
  }

  Ascii2Unicode (BvdtStr, (CHAR8 *) (ImageBvdtPtr + BIOS_VERSION_OFFSET));
  ZeroMem (FirmwareVersionString, FirmwareVersionStringSize);
  StrnCpyS (FirmwareVersionString, FirmwareVersionStringSize, BvdtStr, L05_CRISIS_LOG_BIOS_VERSION_LENGTH);

  return EFI_SUCCESS;
}

/**
  Record crisis log.

  @param  CrisisLogType                 Crisis log type.
  @param  Parameter1                    Parameter1 of crisis log.
  @param  Parameter2                    Parameter2 of crisis log.
**/
VOID
L05RecordCrisisLog (
  L05_CRISIS_LOG_TYPE                   CrisisLogType,
  UINTN                                 Parameter1,
  UINTN                                 Parameter2
  )
{
  EFI_STATUS                            Status;
  PEI_FILE_ACCESS_PPI                   *FileAccess;
  UINTN                                 VolumeIndex;
  UINTN                                 CrisisLogIndex;
  CHAR16                                FirmwareVersionString[L05_CRISIS_LOG_BIOS_VERSION_LENGTH + 1];
  UINT8                                 LogBuffer[L05_CRISIS_LOG_SIZE];
  CHAR16                                CrisisLogFileName[L05_FAT_8_3_FILE_NAME_LENGTH + 1];

  FileAccess = NULL;
  CrisisLogIndex = (UINTN) -1;

  ZeroMem (FirmwareVersionString, sizeof (FirmwareVersionString));
  ZeroMem (LogBuffer, sizeof (LogBuffer));
  ZeroMem (CrisisLogFileName, sizeof (CrisisLogFileName));

  Status = PeiServicesLocatePpi (
             &gPeiFileAccessPpiGuid,
             0,
             NULL,
             (VOID **) &FileAccess
             );

  if (EFI_ERROR (Status)) {
    return;
  }

  Status = GetUsbDeviceVolumeIndex (FileAccess, &VolumeIndex);

  if (EFI_ERROR (Status)) {
    return;
  }

  Status = GetCrisisLogIndexByType (CrisisLogType, &CrisisLogIndex);

  if (EFI_ERROR (Status) || (CrisisLogIndex == (UINTN) -1)) {
    return;
  }

  //
  // Get firmware version for log.
  //
  StrnCpyS (FirmwareVersionString, sizeof (FirmwareVersionString), (CHAR16 *) PcdGetPtr (PcdFirmwareVersionString), L05_CRISIS_LOG_BIOS_VERSION_LENGTH);

  if (CrisisLogType == L05CrisisProcessSuccess) {
    GetImageFirmwareVersion ((UINT8 *) Parameter1, Parameter2, FirmwareVersionString, sizeof (FirmwareVersionString));
  }

  //
  // Print log to buffer.
  //
  switch (CrisisLogType) {

  case L05CrisisProcessFail:
    AsciiSPrint (LogBuffer, L05_CRISIS_LOG_SIZE, mL05CrisisLogMap[CrisisLogIndex].CrisisLogString, FirmwareVersionString, Parameter1);
    break;

  default:
    AsciiSPrint (LogBuffer, L05_CRISIS_LOG_SIZE, mL05CrisisLogMap[CrisisLogIndex].CrisisLogString, FirmwareVersionString);
  }

  //
  // Record crisis recovery Log # to file.
  //
  GetCrisisLogNumFileName (FileAccess, VolumeIndex, CrisisLogFileName, sizeof (CrisisLogFileName));

  Status = RecordLogToFile (
             FileAccess,
             CrisisLogFileName,
             VolumeIndex,
             LogBuffer,
             L05_CRISIS_LOG_SIZE
             );

  if (EFI_ERROR (Status)) {
    return;
  }

  //
  // Record last crisis recovery Log to file.
  //
  ZeroMem (CrisisLogFileName, sizeof (CrisisLogFileName));
  StrCpyS (CrisisLogFileName, sizeof (CrisisLogFileName), (CHAR16 *) PcdGetPtr (PcdL05CrisisRecoveryLastLogFile));

  Status = RecordLogToFile (
             FileAccess,
             CrisisLogFileName,
             VolumeIndex,
             LogBuffer,
             L05_CRISIS_LOG_SIZE
             );

  if (EFI_ERROR (Status)) {
    return;
  }

  return;
}

#endif

