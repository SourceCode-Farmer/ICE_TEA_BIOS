/** @file
  Feature Hook for Natural File Guard.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _NATURAL_FILE_GUARD_H_
#define _NATURAL_FILE_GUARD_H_

#ifdef L05_NATURAL_FILE_GUARD_ENABLE

BOOLEAN
NaturalFileGuardCheckHddSecurityDisable (
  IN HDD_PASSWORD_HDD_INFO              *HddInfoArray,
  IN UINTN                              NumOfHdd
  );

EFI_STATUS
NaturalFileGuardResetAllSecurityStatus (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *HddPasswordDialog
  );

EFI_STATUS
NaturalFileGuardUnlockStoredUhdp (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService
  );

EFI_STATUS
NaturalFileGuardUnlockAuthenticate (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *HddPasswordDialog,
  IN  BOOLEAN                           IsOnlyUnlockUhdp,
  OUT UINT8                             *UserOrMaster
  );

EFI_STATUS
ShowUhdpInvalidMessage (
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *HddPasswordDialog
  );

EFI_STATUS
ReadyToBootLockSecureKey (
  VOID
  );
#endif

#endif
