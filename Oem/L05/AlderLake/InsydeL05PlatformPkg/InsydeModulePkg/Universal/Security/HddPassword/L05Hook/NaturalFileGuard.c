/** @file
  Feature Hook for Natural File Guard.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
//[-start-220125-BAIN000092-modify]//
#ifdef LCFC_SUPPORT
#ifdef L05_NATURAL_FILE_GUARD_ENABLE
#include "HddPassword.h"

EFI_STATUS
GetHddInfoArray (
  IN EFI_HDD_PASSWORD_SERVICE_PROTOCOL  *This,
  IN OUT HDD_PASSWORD_HDD_INFO          **HddInfoArray,
  IN OUT UINTN                          *NumOfHdd
  );

EFI_STATUS
ShowErrorCountExpiredMessageAndSystemStop (
  IN EFI_HDD_PASSWORD_DIALOG_PROTOCOL   *HddPasswordDialog
  );

UINT16
GetExtDataSize (
  VOID
  );

VOID
ZeroMemByPolicy (
  IN  HDD_PASSWORD_TABLE                *HddPasswordTable,
  IN  UINTN                             HddPasswordTableSize
  );

VOID *
CreateHddPasswordTable (
  IN     UINTN                          NumOfHdd,
  IN     HDD_PASSWORD_HDD_INFO          *HddInfoArray,
  OUT    UINTN                          *TableSize
  );

VOID
UpdateExtData (
  IN  HDD_PASSWORD_TABLE                *HddPasswordTable,
  IN  UINT8                              PasswordType
  );

/**
  Natural File Guard checking all HDD security status is Disabled or not.

  @param[in]        HddInfoArray        The array of HDD information used in HDD Password.
  @param[in]        NumOfHdd            Number of HDD.

  @retval           TRUE                Found one of HDD is security Enabled.
  @retval           FALSE               Could not found any HDD is  security Enabled.
**/
BOOLEAN
NaturalFileGuardCheckHddSecurityDisable (
  IN HDD_PASSWORD_HDD_INFO              *HddInfoArray,
  IN UINTN                              NumOfHdd
  )
{
  UINTN                                 Index;

  if (HddInfoArray == NULL) {
    return FALSE;
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    if ((HddInfoArray[Index].HddSecurityStatus & HDD_ENABLE_BIT) != HDD_ENABLE_BIT) {
       return TRUE;
    }
  }
  return FALSE;
}

/**
  Natural File Guard reset all security status.

  @param  HddPasswordService            A pointer to the EFI_HDD_PASSWORD_SERVICE_PROTOCOL.
  @param  HddPasswordDialog             A pointer to the EFI_HDD_PASSWORD_DIALOG_PROTOCOL.

  @retval EFI_SUCCESS                   The security of HDDs is DISABLE.
                                        No any HDD having to be unlock
  @retval EFI_NOT_READY                 the security of HDDs is ENABLE.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardResetAllSecurityStatus (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *HddPasswordDialog
  )
{
  EFI_STATUS                            Status;
  HDD_PASSWORD_HDD_INFO                 *HddInfoArray;
  UINTN                                 NumOfHdd;

  //
  // Check for invalid input parameters
  //
  if (HddPasswordService == NULL || HddPasswordDialog == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  HddInfoArray = NULL;
  NumOfHdd     = 0;

  Status = GetHddInfoArray (
             HddPasswordService,
             &HddInfoArray,
             &NumOfHdd
             );

  if (NumOfHdd == 0) {
    return EFI_SUCCESS;
  }

  if (Status != EFI_SUCCESS) {
    return Status;
  }

  Status = HddPasswordDialog->ResetAllSecurityStatus (
                                HddPasswordDialog,
                                HddInfoArray,
                                NumOfHdd
                                );

  if (HddInfoArray != NULL) {
    FreePool (HddInfoArray);
  }

  return Status;
}

/**
  Natural File Guard unlock all HDD.

  @param  HddPasswordService            A pointer to the EFI_HDD_PASSWORD_SERVICE_PROTOCOL.
  @param  PasswordPtr                   A pointer to the HDD password.
  @param  PasswordLength                Length of HDD password.
  @param  IsOnlyUnlockUhdp              Is only unlock Uhdp.
  @param  UserOrMaster                  A pointer to User or Master flag.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardUnlockAllHdd (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN  UINT8                             *PasswordPtr,
  IN  UINTN                             PasswordLength,
  IN  BOOLEAN                           IsOnlyUnlockUhdp,
  OUT UINT8                             *UserOrMaster
  )
{
  EFI_STATUS                            Status;
  EFI_STATUS                            UnlockHddStatus;
  HDD_PASSWORD_PRIVATE                  *HddPasswordPrivate;
  UINTN                                 Index;
  UINTN                                 NumOfHdd;
  HDD_PASSWORD_HDD_INFO                 *HddInfoArray;

  //
  // Check for invalid input parameters
  //
  if ((HddPasswordService == NULL) ||
      (PasswordPtr == NULL || PasswordLength > HDD_PASSWORD_MAX_NUMBER)) {
    return EFI_INVALID_PARAMETER;
  }

  Status             = EFI_SUCCESS;
  UnlockHddStatus    = EFI_SUCCESS;
  HddPasswordPrivate = NULL;
  NumOfHdd           = 0;
  HddInfoArray       = NULL;
  *UserOrMaster      = 0xFF;

  HddPasswordPrivate = GET_PRIVATE_FROM_HDD_PASSWORD (HddPasswordService);

  Status = GetHddInfoArray (
             HddPasswordService,
             &HddInfoArray,
             &NumOfHdd
             );

  if (NumOfHdd == 0) {
    return EFI_SUCCESS;
  }

  if (Status != EFI_SUCCESS) {
    return Status;
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    if ((HddInfoArray[Index].HddSecurityStatus & HDD_ENABLE_BIT) != HDD_ENABLE_BIT) {
      continue;
    }

    UnlockHddStatus = HddPasswordService->UnlockHddPassword (
                                            HddPasswordService,
                                            &HddInfoArray[Index],
                                            USER_PSW,
                                            PasswordPtr,
                                            PasswordLength
                                            );

    if (!EFI_ERROR (UnlockHddStatus) && !((HddInfoArray[Index].HddSecurityStatus & HDD_LOCKED_BIT) == HDD_LOCKED_BIT)) {
      *UserOrMaster = USER_PSW;

    } else if (!IsOnlyUnlockUhdp) {

      UnlockHddStatus = HddPasswordService->UnlockHddPassword (
                                              HddPasswordService,
                                              &HddInfoArray[Index],
                                              MASTER_PSW,
                                              PasswordPtr,
                                              PasswordLength
                                              );
  
      if (!EFI_ERROR (UnlockHddStatus) && !((HddInfoArray[Index].HddSecurityStatus & HDD_LOCKED_BIT) == HDD_LOCKED_BIT)) {
        *UserOrMaster = MASTER_PSW;
      }
    }

    if (EFI_ERROR (UnlockHddStatus)) {
      Status = UnlockHddStatus;
    }
  }

  if (HddInfoArray != NULL) {
    FreePool (HddInfoArray);
  }

  return Status;
}

/**
  Natural File Guard unlock by stored UHDP.

  @param  HddPasswordService            A pointer to the EFI_HDD_PASSWORD_SERVICE_PROTOCOL.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardUnlockStoredUhdp (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *EncodeData;
  UINTN                                 EncodeDataSize;
  UINT8                                 UserOrMaster;

  //
  // Check for invalid input parameters
  //
  if (HddPasswordService == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  EncodeData     = NULL;
  EncodeDataSize = SHA256_DIGEST_SIZE;
  EncodeData     = AllocateZeroPool (EncodeDataSize);

  if (EncodeData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  Status = UhdpDecryption (EncodeData, &EncodeDataSize);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  PcdSetBoolS (PcdL05NaturalFileGuardUnlockStoredUhdpFlag, TRUE);
  Status = NaturalFileGuardUnlockAllHdd (HddPasswordService, EncodeData, EncodeDataSize, TRUE, &UserOrMaster);
  PcdSetBoolS (PcdL05NaturalFileGuardUnlockStoredUhdpFlag, FALSE);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  FreePool (EncodeData);

  return Status;
}

/**
  Natural File Guard update HDD password variable.

  @param  HddPasswordService            A pointer to the EFI_HDD_PASSWORD_SERVICE_PROTOCOL.
  @param  UnicodePasswordString         A pointer to the unicode password string.
  @param  UserOrMaster                  User or Master flag.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardUpdateHddPasswordVariable (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN  CHAR16                            *UnicodePasswordString,
  IN  UINT8                             UserOrMaster
  )
{
  EFI_STATUS                            Status;
  UINTN                                 NumOfHdd;
  HDD_PASSWORD_HDD_INFO                 *HddInfoArray;
  HDD_PASSWORD_TABLE                    *HddPasswordTable;
  UINTN                                 HddPasswordTableSize;
  UINTN                                 Index;

  HddInfoArray     = NULL;
  HddPasswordTable = NULL;

  //
  // Get HDD information
  //
  Status = GetHddInfoArray (
             HddPasswordService,
             &HddInfoArray,
             &NumOfHdd
             );

  //
  // Get save HDD passwird Variable
  //
  Status = CommonGetVariableDataAndSize (
             SAVE_HDD_PASSWORD_VARIABLE_NAME,
             &gSaveHddPasswordGuid,
             &HddPasswordTableSize,
             (VOID **) &HddPasswordTable
             );

  if (Status == EFI_SUCCESS) {
    //
    // May get old table
    //

    if ((HddPasswordTableSize / (sizeof (HDD_PASSWORD_TABLE) + GetExtDataSize ())) != (NumOfHdd + 1)) {
      Status = EFI_NOT_FOUND;
      gBS->FreePool (HddPasswordTable);
    } else {
      ZeroMemByPolicy (HddPasswordTable, HddPasswordTableSize);
    }
  }

  if (Status != EFI_SUCCESS) {
    //
    // Create new table
    //
    HddPasswordTable = CreateHddPasswordTable (NumOfHdd, HddInfoArray, &HddPasswordTableSize);
    if (HddPasswordTable == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    CopyMem (
      HddPasswordTable[Index].PasswordStr,
      UnicodePasswordString,
      StrSize (UnicodePasswordString)
      );
    HddPasswordTable[Index].PasswordType     = UserOrMaster;
    HddPasswordTable[Index].ControllerNumber = HddInfoArray[Index].ControllerNumber;
    HddPasswordTable[Index].PortNumber       = HddInfoArray[Index].PortNumber;
    HddPasswordTable[Index].PortMulNumber    = HddInfoArray[Index].PortMulNumber;

    UpdateExtData (&HddPasswordTable[Index], HddPasswordTable[Index].PasswordType);
  }

  Status = CommonSetVariable (
             SAVE_HDD_PASSWORD_VARIABLE_NAME,
             &gSaveHddPasswordGuid,
             EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
             HddPasswordTableSize,
             (VOID *) HddPasswordTable
             );

  L05SetEncodeHddPasswordTable (HddPasswordService, HddPasswordTable, (UINT32) NumOfHdd);

  return EFI_SUCCESS;
}

/**
  Natural File Guard unlock authenticate.

  @param  HddPasswordService            A pointer to the EFI_HDD_PASSWORD_SERVICE_PROTOCOL.
  @param  HddPasswordDialog             A pointer to the EFI_HDD_PASSWORD_DIALOG_PROTOCOL.
  @param  IsOnlyUnlockUhdp              Is only unlock Uhdp.
  @param  UserOrMaster                  A pointer to User or Master flag.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardUnlockAuthenticate (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *HddPasswordDialog,
  IN  BOOLEAN                           IsOnlyUnlockUhdp,
  OUT UINT8                             *UserOrMaster
  )
{
  EFI_STATUS                            Status;
  HDD_PASSWORD_POST_DIALOG_PRIVATE      *HddPasswordPostDialogPrivate;
  H2O_DIALOG_PROTOCOL                   *H2oDialogProtocol;
  CHAR16                                *StringPtr;
  EFI_INPUT_KEY                         Key;
  CHAR16                                UnicodePasswordString[HDD_PASSWORD_MAX_NUMBER + 1];
  UINT8                                 PasswordToHdd[HDD_PASSWORD_MAX_NUMBER + 1];
  UINTN                                 PasswordToHddLength;
  UINTN                                 RetryCount;
  UINTN                                 Index;

  //
  // Check for invalid input parameters
  //
  if (HddPasswordService == NULL || HddPasswordDialog == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  HddPasswordPostDialogPrivate = NULL;
  H2oDialogProtocol            = NULL;
  StringPtr                    = NULL;

  HddPasswordPostDialogPrivate = GET_PRIVATE_FROM_HDD_PASSWORD_POST_DIALOG (HddPasswordDialog);
  H2oDialogProtocol = HddPasswordPostDialogPrivate->H2oDialogProtocol;

  Key.UnicodeChar = CHAR_CARRIAGE_RETURN;
  ZeroMem (UnicodePasswordString, (HDD_PASSWORD_MAX_NUMBER + 1) * sizeof (CHAR16));

  if (!IsOnlyUnlockUhdp) {
    //
    // Prompt for HDP & Authenticate
    //
    StringPtr = GetStringById (STRING_TOKEN (L05_STR_ENTER_HDD_PASSWORD_STRING));

    //
    // fail & not reach max retry counter
    //
    RetryCount = PcdGet8 (PcdH2OHddPasswordMaxCheckPasswordCount);

  } else {
    //
    // Authenticate UHDP
    //
    StringPtr  = GetStringById (STRING_TOKEN (L05_STR_ENTER_USER_HDD_PASSWORD_STRING));
    RetryCount = 1;

    //
    // Prompt for UHDP or ESC
    //
    PcdSetBoolS (PcdL05NaturalFileGuardPressEscFlag, TRUE);
  }

  for (Index = 0; Index < RetryCount; Index++) {
    PcdSetBoolS (PcdL05NaturalFileGuardDialogFlag, TRUE);
    Status = H2oDialogProtocol->PasswordDialog (
                                  0,
                                  FALSE,
                                  (PcdGet16 (PcdH2OHddPasswordMaxLength) + 1),
                                  UnicodePasswordString,
                                  &Key,
                                  StringPtr
                                  );
    PcdSetBoolS (PcdL05NaturalFileGuardDialogFlag, FALSE);
    ZeroMem (PasswordToHdd, (HDD_PASSWORD_MAX_NUMBER + 1));
    PasswordToHddLength = 0;

    Status = HddPasswordService->PasswordStringProcess (
                                   HddPasswordService,
                                   0,
                                   UnicodePasswordString,
                                   StrLen (UnicodePasswordString),
                                   (VOID **) &PasswordToHdd,
                                   &PasswordToHddLength
                                   );

    Status = NaturalFileGuardUnlockAllHdd (HddPasswordService, PasswordToHdd, PasswordToHddLength, IsOnlyUnlockUhdp, UserOrMaster);

    if (!EFI_ERROR (Status)) {
      break;
    }
  }

  PcdSetBoolS (PcdL05NaturalFileGuardPressEscFlag, FALSE);
  FreePool (StringPtr);

  if (Index == PcdGet8 (PcdH2OHddPasswordMaxCheckPasswordCount)) {
    //
    // Fail reach max retry counter
    // Hang with error prompt
    //
    ShowErrorCountExpiredMessageAndSystemStop (HddPasswordDialog);
  }

  //
  // Update HDD Password Variable
  //
  if (!EFI_ERROR (Status)) {
    NaturalFileGuardUpdateHddPasswordVariable (HddPasswordService, UnicodePasswordString, *UserOrMaster);
  }

  if (!EFI_ERROR (Status) && *UserOrMaster == USER_PSW) {
    //
    // Safe store UHDP
    //
    Status = UhdpEncryption (PasswordToHdd, PasswordToHddLength);
  }

  return Status;
}

/**
  Show UHDP invalid message.

  @param  HddPasswordDialog             A pointer to the EFI_HDD_PASSWORD_DIALOG_PROTOCOL.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
ShowUhdpInvalidMessage (
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *HddPasswordDialog
  )
{
  EFI_STATUS                            Status;
  HDD_PASSWORD_POST_DIALOG_PRIVATE      *HddPasswordPostDialogPrivate;
  H2O_DIALOG_PROTOCOL                   *H2oDialogProtocol;
  CHAR16                                *ErrorStatusMsg;
  CHAR16                                *PromptMsg1;
  CHAR16                                *PromptMsg2;
  CHAR16                                *PromptMsg3;

  //
  // Check for invalid input parameters
  //
  if (HddPasswordDialog == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  HddPasswordPostDialogPrivate = NULL;
  H2oDialogProtocol            = NULL;
  ErrorStatusMsg               = NULL;
  PromptMsg1                   = NULL;
  PromptMsg2                   = NULL;
  PromptMsg3                   = NULL;

  HddPasswordPostDialogPrivate = GET_PRIVATE_FROM_HDD_PASSWORD_POST_DIALOG (HddPasswordDialog);
  H2oDialogProtocol = HddPasswordPostDialogPrivate->H2oDialogProtocol;

  ErrorStatusMsg = GetStringById (STRING_TOKEN (STR_HDD_DIALOG_ERROR_STATUS));

  if (FeaturePcdGet (PcdH2OFormBrowserLocalMetroDESupported)) {
    PromptMsg1 = GetStringById (STRING_TOKEN (L05_STR_GRAPHIC_UHDP_INVALID_OR_ESC_PRESSED_MSG));
    PromptMsg2 = GetStringById (STRING_TOKEN (L05_STR_GRAPHIC_NATURAL_FILE_GUARD_AUTO_DISABLED_MSG));
    PromptMsg3 = GetStringById (STRING_TOKEN (L05_STR_GRAPHIC_PRESS_ENTER_OR_ESC_TO_CONTINUE_MSG));

  } else {
    PromptMsg1 = GetStringById (STRING_TOKEN (L05_STR_UHDP_INVALID_OR_ESC_PRESSED_MSG));
    PromptMsg2 = GetStringById (STRING_TOKEN (L05_STR_NATURAL_FILE_GUARD_AUTO_DISABLED_MSG));
    PromptMsg3 = GetStringById (STRING_TOKEN (L05_STR_PRESS_ENTER_OR_ESC_TO_CONTINUE_MSG));
  }

  PcdSetBoolS (PcdL05NaturalFileGuardDialogFlag, TRUE);
  Status = H2oDialogProtocol->CreateMsgPopUp (
                                50,
                                4,
                                ErrorStatusMsg,
                                PromptMsg1,
                                PromptMsg2,
                                PromptMsg3
                                );
  PcdSetBoolS (PcdL05NaturalFileGuardDialogFlag, FALSE);

  FreePool (PromptMsg1);
  FreePool (PromptMsg2);

  return Status;
}

/**
  Lock secure key during Ready To Boot Event.

  @param Event                          ReadyToBootEvent
  @param Context                        Context
**/
VOID
EFIAPI
LockSecureKeyNotify (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  )
{
  gBS->CloseEvent (Event);

  OemSvcLockSecureKey ();

  return;
}

/**
  On the Ready to boot event, BIOS should issue command to EC to lock the secure key data area in
  the EC and prevent Read/Write access.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
ReadyToBootLockSecureKey (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_EVENT                             ReadyToBootEvent;

  ReadyToBootEvent = NULL;

  //
  // Register Ready to Boot Event for lock secure key
  //
  Status = EfiCreateEventReadyToBootEx (
             TPL_CALLBACK,
             LockSecureKeyNotify,
             NULL,
             &ReadyToBootEvent
             );

  return Status;
}

#endif

#else

#include "HddPassword.h"

#ifdef L05_NATURAL_FILE_GUARD_ENABLE

EFI_STATUS
GetHddInfoArray (
  IN EFI_HDD_PASSWORD_SERVICE_PROTOCOL  *This,
  IN OUT HDD_PASSWORD_HDD_INFO          **HddInfoArray,
  IN OUT UINTN                          *NumOfHdd
  );

EFI_STATUS
ShowErrorCountExpiredMessageAndSystemStop (
  IN EFI_HDD_PASSWORD_DIALOG_PROTOCOL   *HddPasswordDialog
  );

UINT16
GetExtDataSize (
  VOID
  );

VOID
ZeroMemByPolicy (
  IN  HDD_PASSWORD_TABLE                *HddPasswordTable,
  IN  UINTN                             HddPasswordTableSize
  );

VOID *
CreateHddPasswordTable (
  IN     UINTN                          NumOfHdd,
  IN     HDD_PASSWORD_HDD_INFO          *HddInfoArray,
  OUT    UINTN                          *TableSize
  );

VOID
UpdateExtData (
  IN  HDD_PASSWORD_TABLE                *HddPasswordTable,
  IN  UINT8                              PasswordType
  );

/**
  Natural File Guard set password dialog F1 string.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardSetDialogF1String (
  VOID
  )
{
  CHAR16                                *F1String;
  UINTN                                 StringSize;

  F1String = NULL;

  F1String = GetStringById (STRING_TOKEN (L05_STR_NFG_PRESS_F1_SWITCH_MASTER_AND_USER_PASSWORD_MSG));

  if (F1String == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  StringSize = StrSize (F1String);
  PcdSetPtrS (PcdL05NotebookPasswordDesignDialogF1String, &StringSize, (VOID *) F1String);

  FreePool (F1String);

  return EFI_SUCCESS;
}

/**
  Natural File Guard checking all HDD security status is Disabled or not.

  @param[in]        HddInfoArray        The array of HDD information used in HDD Password.
  @param[in]        NumOfHdd            Number of HDD.

  @retval           TRUE                Found one of HDD is security Enabled.
  @retval           FALSE               Could not found any HDD is  security Enabled.
**/
BOOLEAN
NaturalFileGuardCheckHddSecurityDisable (
  IN HDD_PASSWORD_HDD_INFO              *HddInfoArray,
  IN UINTN                              NumOfHdd
  )
{
  UINTN                                 Index;

  if (HddInfoArray == NULL) {
    return FALSE;
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    if ((HddInfoArray[Index].HddSecurityStatus & HDD_ENABLE_BIT) != HDD_ENABLE_BIT) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
  Natural File Guard reset all security status.

  @param  HddPasswordService            A pointer to the EFI_HDD_PASSWORD_SERVICE_PROTOCOL.
  @param  HddPasswordDialog             A pointer to the EFI_HDD_PASSWORD_DIALOG_PROTOCOL.

  @retval EFI_SUCCESS                   The security of HDDs is DISABLE.
                                        No any HDD having to be unlock
  @retval EFI_NOT_READY                 the security of HDDs is ENABLE.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardResetAllSecurityStatus (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *HddPasswordDialog
  )
{
  EFI_STATUS                            Status;
  HDD_PASSWORD_HDD_INFO                 *HddInfoArray;
  UINTN                                 NumOfHdd;

  //
  // Check for invalid input parameters
  //
  if (HddPasswordService == NULL || HddPasswordDialog == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  HddInfoArray = NULL;
  NumOfHdd     = 0;

  Status = GetHddInfoArray (
             HddPasswordService,
             &HddInfoArray,
             &NumOfHdd
             );

  if (NumOfHdd == 0) {
    return EFI_SUCCESS;
  }

  if (Status != EFI_SUCCESS) {
    return Status;
  }

  Status = HddPasswordDialog->ResetAllSecurityStatus (
                                HddPasswordDialog,
                                HddInfoArray,
                                NumOfHdd
                                );

  if (HddInfoArray != NULL) {
    FreePool (HddInfoArray);
  }

  return Status;
}

/**
  Natural File Guard unlock all HDD.

  @param  HddPasswordService            A pointer to the EFI_HDD_PASSWORD_SERVICE_PROTOCOL.
  @param  PasswordType                  User or Master flag.
  @param  PasswordPtr                   A pointer to the HDD password.
  @param  PasswordLength                Length of HDD password.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardUnlockAllHdd (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN  UINT8                             PasswordType,
  IN  UINT8                             *PasswordPtr,
  IN  UINTN                             PasswordLength
  )
{
  EFI_STATUS                            Status;
  EFI_STATUS                            UnlockHddStatus;
  HDD_PASSWORD_PRIVATE                  *HddPasswordPrivate;
  UINTN                                 Index;
  UINTN                                 NumOfHdd;
  HDD_PASSWORD_HDD_INFO                 *HddInfoArray;

  //
  // Check for invalid input parameters
  //
  if ((HddPasswordService == NULL) ||
      (PasswordPtr == NULL || PasswordLength > HDD_PASSWORD_MAX_NUMBER)) {
    return EFI_INVALID_PARAMETER;
  }

  Status             = EFI_SUCCESS;
  UnlockHddStatus    = EFI_SUCCESS;
  HddPasswordPrivate = NULL;
  NumOfHdd           = 0;
  HddInfoArray       = NULL;

  HddPasswordPrivate = GET_PRIVATE_FROM_HDD_PASSWORD (HddPasswordService);

  Status = GetHddInfoArray (
             HddPasswordService,
             &HddInfoArray,
             &NumOfHdd
             );

  if (NumOfHdd == 0) {
    return EFI_SUCCESS;
  }

  if (Status != EFI_SUCCESS) {
    return Status;
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    if ((HddInfoArray[Index].HddSecurityStatus & HDD_ENABLE_BIT) != HDD_ENABLE_BIT) {
      continue;
    }

    UnlockHddStatus = HddPasswordService->UnlockHddPassword (
                                            HddPasswordService,
                                            &HddInfoArray[Index],
                                            PasswordType,
                                            PasswordPtr,
                                            PasswordLength
                                            );

    if (EFI_ERROR (UnlockHddStatus)) {
      Status = UnlockHddStatus;
    }
  }

  if (HddInfoArray != NULL) {
    FreePool (HddInfoArray);
  }

  return Status;
}

/**
  Natural File Guard unlock by stored UHDP.

  @param  HddPasswordService            A pointer to the EFI_HDD_PASSWORD_SERVICE_PROTOCOL.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardUnlockStoredUhdp (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService
  )
{
  EFI_STATUS                            Status;
  UINT8                                 *EncodeData;
  UINTN                                 EncodeDataSize;

  //
  // Check for invalid input parameters
  //
  if (HddPasswordService == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  EncodeData     = NULL;
  EncodeDataSize = SHA256_DIGEST_SIZE;
  EncodeData     = AllocateZeroPool (EncodeDataSize);

  if (EncodeData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  Status = UhdpDecryption (EncodeData, &EncodeDataSize);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  PcdSetBoolS (PcdL05NaturalFileGuardUnlockStoredUhdpFlag, TRUE);
  Status = NaturalFileGuardUnlockAllHdd (HddPasswordService, USER_PSW, EncodeData, EncodeDataSize);
  PcdSetBoolS (PcdL05NaturalFileGuardUnlockStoredUhdpFlag, FALSE);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  FreePool (EncodeData);

  return Status;
}

/**
  Natural File Guard update HDD password variable.

  @param  HddPasswordService            A pointer to the EFI_HDD_PASSWORD_SERVICE_PROTOCOL.
  @param  UnicodePasswordString         A pointer to the unicode password string.
  @param  PasswordType                  User or Master flag.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardUpdateHddPasswordVariable (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN  CHAR16                            *UnicodePasswordString,
  IN  UINT8                             PasswordType
  )
{
  EFI_STATUS                            Status;
  UINTN                                 NumOfHdd;
  HDD_PASSWORD_HDD_INFO                 *HddInfoArray;
  HDD_PASSWORD_TABLE                    *HddPasswordTable;
  UINTN                                 HddPasswordTableSize;
  UINTN                                 Index;

  HddInfoArray     = NULL;
  HddPasswordTable = NULL;

  //
  // Get HDD information
  //
  Status = GetHddInfoArray (
             HddPasswordService,
             &HddInfoArray,
             &NumOfHdd
             );

  //
  // Get save HDD passwird Variable
  //
  Status = CommonGetVariableDataAndSize (
             SAVE_HDD_PASSWORD_VARIABLE_NAME,
             &gSaveHddPasswordGuid,
             &HddPasswordTableSize,
             (VOID **) &HddPasswordTable
             );

  if (Status == EFI_SUCCESS) {
    //
    // May get old table
    //

    if ((HddPasswordTableSize / (sizeof (HDD_PASSWORD_TABLE) + GetExtDataSize ())) != (NumOfHdd + 1)) {
      Status = EFI_NOT_FOUND;
      gBS->FreePool (HddPasswordTable);
    } else {
      ZeroMemByPolicy (HddPasswordTable, HddPasswordTableSize);
    }
  }

  if (Status != EFI_SUCCESS) {
    //
    // Create new table
    //
    HddPasswordTable = CreateHddPasswordTable (NumOfHdd, HddInfoArray, &HddPasswordTableSize);
    if (HddPasswordTable == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    CopyMem (
      HddPasswordTable[Index].PasswordStr,
      UnicodePasswordString,
      StrSize (UnicodePasswordString)
      );
    HddPasswordTable[Index].PasswordType     = PasswordType;
    HddPasswordTable[Index].ControllerNumber = HddInfoArray[Index].ControllerNumber;
    HddPasswordTable[Index].PortNumber       = HddInfoArray[Index].PortNumber;
    HddPasswordTable[Index].PortMulNumber    = HddInfoArray[Index].PortMulNumber;

    UpdateExtData (&HddPasswordTable[Index], HddPasswordTable[Index].PasswordType);
  }

  Status = CommonSetVariable (
             SAVE_HDD_PASSWORD_VARIABLE_NAME,
             &gSaveHddPasswordGuid,
             EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
             HddPasswordTableSize,
             (VOID *) HddPasswordTable
             );

  L05SetEncodeHddPasswordTable (HddPasswordService, HddPasswordTable, (UINT32) NumOfHdd);

  return EFI_SUCCESS;
}

/**
  Natural File Guard unlock authenticate.

  @param  HddPasswordService            A pointer to the EFI_HDD_PASSWORD_SERVICE_PROTOCOL.
  @param  HddPasswordDialog             A pointer to the EFI_HDD_PASSWORD_DIALOG_PROTOCOL.
  @param  IsOnlyUnlockUhdp              Is only unlock Uhdp.
  @param  PasswordType                  A pointer to User or Master flag.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
NaturalFileGuardUnlockAuthenticate (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *HddPasswordDialog,
  IN  BOOLEAN                           IsOnlyUnlockUhdp,
  OUT UINT8                             *PasswordType
  )
{
  EFI_STATUS                            Status;
  HDD_PASSWORD_POST_DIALOG_PRIVATE      *HddPasswordPostDialogPrivate;
  H2O_DIALOG_PROTOCOL                   *H2oDialogProtocol;
  CHAR16                                *StringPtr;
  EFI_INPUT_KEY                         Key;
  CHAR16                                UnicodePasswordString[HDD_PASSWORD_MAX_NUMBER + 1];
  UINT8                                 PasswordToHdd[HDD_PASSWORD_MAX_NUMBER + 1];
  UINTN                                 PasswordToHddLength;
  UINTN                                 RetryCount;
  UINTN                                 Index;

  //
  // Check for invalid input parameters
  //
  if (HddPasswordService == NULL || HddPasswordDialog == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  HddPasswordPostDialogPrivate = NULL;
  H2oDialogProtocol            = NULL;
  StringPtr                    = NULL;

  HddPasswordPostDialogPrivate = GET_PRIVATE_FROM_HDD_PASSWORD_POST_DIALOG (HddPasswordDialog);
  H2oDialogProtocol = HddPasswordPostDialogPrivate->H2oDialogProtocol;

  Key.UnicodeChar = CHAR_CARRIAGE_RETURN;
  ZeroMem (UnicodePasswordString, (HDD_PASSWORD_MAX_NUMBER + 1) * sizeof (CHAR16));

  //
  // fail & not reach max retry counter
  //
  RetryCount = PcdGet8 (PcdH2OHddPasswordMaxCheckPasswordCount);

  *PasswordType = MASTER_PSW;

  if (IsOnlyUnlockUhdp) {
    *PasswordType = USER_PSW;
    RetryCount = 1;

    //
    // Prompt for UHDP or ESC
    //
    PcdSetBoolS (PcdL05NaturalFileGuardPressEscFlag, TRUE);
  }

  NaturalFileGuardSetDialogF1String ();

  for (Index = 0; Index < RetryCount; Index++) {

    do {
      Key.ScanCode    = SCAN_NULL;
      Key.UnicodeChar = CHAR_NULL;

      if (IsOnlyUnlockUhdp) {
        StringPtr = GetStringById (STRING_TOKEN (L05_STR_ENTER_UHDP_ACTIVATE_NFG_STRING));

      } else {
        if (*PasswordType == MASTER_PSW) {
          StringPtr = GetStringById (STRING_TOKEN (L05_STR_ENTER_MASTER_HDD_PASSWORD_STRING));
        } else {
          StringPtr = GetStringById (STRING_TOKEN (L05_STR_ENTER_USER_HDD_PASSWORD_STRING));
        }
      }

      if (!IsOnlyUnlockUhdp) {
        PcdSetBoolS (PcdL05NotebookPasswordDesignDialogF1Flag, TRUE);
      }

      PcdSetBoolS (PcdL05NaturalFileGuardDialogFlag, TRUE);
      Status = H2oDialogProtocol->PasswordDialog (
                                    0,
                                    FALSE,
                                    (PcdGet16 (PcdH2OHddPasswordMaxLength) + 1),
                                    UnicodePasswordString,
                                    &Key,
                                    StringPtr
                                    );
      PcdSetBoolS (PcdL05NaturalFileGuardDialogFlag, FALSE);
      FreePool (StringPtr);

      if (!IsOnlyUnlockUhdp) {
        PcdSetBoolS (PcdL05NotebookPasswordDesignDialogF1Flag, FALSE);
      }

      ZeroMem (PasswordToHdd, (HDD_PASSWORD_MAX_NUMBER + 1));
      PasswordToHddLength = 0;

      Status = HddPasswordService->PasswordStringProcess (
                                     HddPasswordService,
                                     0,
                                     UnicodePasswordString,
                                     StrLen (UnicodePasswordString),
                                     (VOID **) &PasswordToHdd,
                                     &PasswordToHddLength
                                     );

      if (IsOnlyUnlockUhdp) {
        break;
      }

      //
      // User can press F1 to switch Master Password and User Password.
      //
      if (Key.ScanCode != SCAN_F1) {
        break;
      }

      *PasswordType = (*PasswordType == MASTER_PSW) ? USER_PSW : MASTER_PSW;
    } while (!IsOnlyUnlockUhdp);

    Status = NaturalFileGuardUnlockAllHdd (HddPasswordService, *PasswordType, PasswordToHdd, PasswordToHddLength);

    if (!EFI_ERROR (Status)) {
      break;
    }
  }

  PcdSetBoolS (PcdL05NaturalFileGuardPressEscFlag, FALSE);

  if (Index == PcdGet8 (PcdH2OHddPasswordMaxCheckPasswordCount)) {
    //
    // Fail reach max retry counter
    // Hang with error prompt
    //
    ShowErrorCountExpiredMessageAndSystemStop (HddPasswordDialog);
  }

  //
  // Update HDD Password Variable
  //
  if (!EFI_ERROR (Status)) {
    NaturalFileGuardUpdateHddPasswordVariable (HddPasswordService, UnicodePasswordString, *PasswordType);
  }

  if (!EFI_ERROR (Status) && *PasswordType == USER_PSW) {
    //
    // Safe store UHDP
    //
    Status = UhdpEncryption (PasswordToHdd, PasswordToHddLength);
  }

  return Status;
}

/**
  Show UHDP invalid message.

  @param  HddPasswordDialog             A pointer to the EFI_HDD_PASSWORD_DIALOG_PROTOCOL.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         One or more parameters are invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
ShowUhdpInvalidMessage (
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *HddPasswordDialog
  )
{
  EFI_STATUS                            Status;
  HDD_PASSWORD_POST_DIALOG_PRIVATE      *HddPasswordPostDialogPrivate;
  H2O_DIALOG_PROTOCOL                   *H2oDialogProtocol;
  CHAR16                                *ErrorStatusMsg;
  CHAR16                                *PromptMsg1;
  CHAR16                                *PromptMsg2;
  CHAR16                                *PromptMsg3;

  //
  // Check for invalid input parameters
  //
  if (HddPasswordDialog == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  HddPasswordPostDialogPrivate = NULL;
  H2oDialogProtocol            = NULL;
  ErrorStatusMsg               = NULL;
  PromptMsg1                   = NULL;
  PromptMsg2                   = NULL;
  PromptMsg3                   = NULL;

  HddPasswordPostDialogPrivate = GET_PRIVATE_FROM_HDD_PASSWORD_POST_DIALOG (HddPasswordDialog);
  H2oDialogProtocol = HddPasswordPostDialogPrivate->H2oDialogProtocol;

  ErrorStatusMsg = GetStringById (STRING_TOKEN (STR_HDD_DIALOG_ERROR_STATUS));

  if (FeaturePcdGet (PcdH2OFormBrowserLocalMetroDESupported)) {
    PromptMsg1 = GetStringById (STRING_TOKEN (L05_STR_GRAPHIC_UHDP_INVALID_OR_ESC_PRESSED_MSG));
    PromptMsg2 = GetStringById (STRING_TOKEN (L05_STR_GRAPHIC_NATURAL_FILE_GUARD_AUTO_DISABLED_MSG));
    PromptMsg3 = GetStringById (STRING_TOKEN (L05_STR_GRAPHIC_PRESS_ENTER_OR_ESC_TO_CONTINUE_MSG));

  } else {
    PromptMsg1 = GetStringById (STRING_TOKEN (L05_STR_UHDP_INVALID_OR_ESC_PRESSED_MSG));
    PromptMsg2 = GetStringById (STRING_TOKEN (L05_STR_NATURAL_FILE_GUARD_AUTO_DISABLED_MSG));
    PromptMsg3 = GetStringById (STRING_TOKEN (L05_STR_PRESS_ENTER_OR_ESC_TO_CONTINUE_MSG));
  }

  PcdSetBoolS (PcdL05NaturalFileGuardDialogFlag, TRUE);
  Status = H2oDialogProtocol->CreateMsgPopUp (
                                50,
                                4,
                                ErrorStatusMsg,
                                PromptMsg1,
                                PromptMsg2,
                                PromptMsg3
                                );
  PcdSetBoolS (PcdL05NaturalFileGuardDialogFlag, FALSE);

  FreePool (PromptMsg1);
  FreePool (PromptMsg2);

  return Status;
}

/**
  Lock secure key during Ready To Boot Event.

  @param Event                          ReadyToBootEvent
  @param Context                        Context
**/
VOID
EFIAPI
LockSecureKeyNotify (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  )
{
  gBS->CloseEvent (Event);

  OemSvcLockSecureKey ();

  return;
}

/**
  On the Ready to boot event, BIOS should issue command to EC to lock the secure key data area in
  the EC and prevent Read/Write access.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
ReadyToBootLockSecureKey (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_EVENT                             ReadyToBootEvent;

  ReadyToBootEvent = NULL;

  //
  // Register Ready to Boot Event for lock secure key
  //
  Status = EfiCreateEventReadyToBootEx (
             TPL_CALLBACK,
             LockSecureKeyNotify,
             NULL,
             &ReadyToBootEvent
             );

  return Status;
}

#endif
#endif
//[-end-220125-BAIN000092-modify]//
