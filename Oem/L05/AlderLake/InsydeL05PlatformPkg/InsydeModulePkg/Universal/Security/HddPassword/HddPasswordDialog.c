/** @file
  String processing for HDD Password Protocol

;******************************************************************************
;* Copyright (c) 2012 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "HddPassword.h"


HDD_PASSWORD_POST_DIALOG_PRIVATE *mHddPasswordPostDialogPrivate;



EFI_HII_HANDLE                           gStringPackHandle;

#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
UINT8                            mPasswordType;
#endif

//
// HII specific Vendor Device Path definition.
//
typedef struct {
  VENDOR_DEVICE_PATH        VendorDevicePath;
  EFI_DEVICE_PATH_PROTOCOL  End;
} HII_VENDOR_DEVICE_PATH;

HII_VENDOR_DEVICE_PATH  mHiiVendorDevicePath = {
  {
    {
      HARDWARE_DEVICE_PATH,
      HW_VENDOR_DP,
      {
        (UINT8) (sizeof (VENDOR_DEVICE_PATH)),
        (UINT8) ((sizeof (VENDOR_DEVICE_PATH)) >> 8)
      }
    },
    {0xad2e3474, 0x93e6, 0x488b, {0x93, 0x19, 0x64, 0x88, 0xfc, 0x68, 0x1f, 0x16}}
  },
  {
    END_DEVICE_PATH_TYPE,
    END_ENTIRE_DEVICE_PATH_SUBTYPE,
    {
      (UINT8) (END_DEVICE_PATH_LENGTH),
      (UINT8) ((END_DEVICE_PATH_LENGTH) >> 8)
    }
  }
};

extern UINT8                            HddPasswordStrings[];

EFI_GUID gHddPasswordStringPackGuid = { 0xac6f7313, 0xcea, 0x461d, {0x9a, 0xbc, 0x64, 0xf0, 0x77, 0x7a, 0x70, 0x4b} };

//_Start_L05_HDD_PASSWORD_ENABLE_
VOID
ResetSystemHook (
  IN EFI_RESET_TYPE                     ResetType,
  IN EFI_STATUS                         ResetStatus,
  IN UINTN                              DataSize,
  IN CHAR16                             *ResetData OPTIONAL
  )
{
}

/**
  The function use to calculate the length of Harddisk model name

  @param  String                        The content pointer to Harddisk model name

  @retval Length                        The characters length of Harddisk model name
**/
UINTN
EFIAPI
StrLenForModelName (
  IN      CHAR16                        *String
  )
{
  UINTN                                 Length;
  CHAR16                                *TempString;

  if (String != NULL) {
    TempString = String;
  } else {
    TempString = NULL;
    return MODEL_NUMBER_LENGTH;
  }
  //
  // Due to the length of Model name is 40,
  // it will search the valid characters (not space) from the end of Harddisk model name
  //
  for (Length = MODEL_NUMBER_LENGTH;  Length > 0; Length--) {

    if (*TempString != CHAR_SPACE) {
      break;
    }

    TempString--;
  }

  if (TempString != NULL) {
    TempString = NULL;
  }
  return Length;
}

EFI_STATUS
L05CheckSystemPassword (
  BOOLEAN                               IntoScu
  )
{
  EFI_STATUS                            Status;
  EFI_STATUS                            SupervisorStatus;
  EFI_STATUS                            UserStatus;
  EFI_SYS_PASSWORD_SERVICE_PROTOCOL     *PasswordInstance;
  EFI_SETUP_UTILITY_PROTOCOL            *SetupUtility;
  SYSTEM_CONFIGURATION                  *SetupVariable;
  H2O_DIALOG_PROTOCOL                   *H2oDialog;
  UINT8                                 ErrorCount;
  EFI_INPUT_KEY                         Key;
//_Start_L05_SETUP_MENU_
  CHAR16                                PasswordString[FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1];
//_End_L05_SETUP_MENU_
  CHAR16                                *TitleString;
  CHAR16                                *ErrorStatusMsg;
  CHAR16                                *MaximumIncorrectMsg;
  CHAR16                                *SysInvalidPassword;
//[-start-220225-BAIN000098-add]//
#ifdef LCFC_SUPPORT
  HOTKEY_TYPE                           LfcSelection = KEY_NONE;
#endif
//[-end-220225-BAIN000098-add]//

  Status              = EFI_SUCCESS;
  SupervisorStatus    = EFI_SUCCESS;
  UserStatus          = EFI_SUCCESS;
  PasswordInstance    = NULL;
  SetupUtility        = NULL;
  SetupVariable       = NULL;
  H2oDialog           = NULL;
  ErrorCount          = 0;
  TitleString         = NULL;
  ErrorStatusMsg      = NULL;
  MaximumIncorrectMsg = NULL;
  SysInvalidPassword  = NULL;

  SetupVariable = CommonGetVariableData (SETUP_VARIABLE_NAME, &gSystemConfigurationGuid);

//[-start-220225-BAIN000098-add]//
#ifdef LCFC_SUPPORT
  LfcEcLibGetHotkeyFromEc(&LfcSelection);
  if(LfcSelection == KEY_F2){
    IntoScu = TRUE;
  }
#endif
//[-end-220225-BAIN000098-add]//

//[-start-211013-TAMT000028-add]//
#ifdef LCFC_SUPPORT
  Status = LfcCleanUpKbcAndEcBuffer ();
#endif
//[-end-211013-TAMT000028-add]//
  if (SetupVariable == NULL) {
    return EFI_NOT_FOUND;
  }


  Status = gBS->LocateProtocol (
                  &gEfiSysPasswordServiceProtocolGuid,
                  NULL,
                  &PasswordInstance
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  SupervisorStatus = PasswordInstance->GetStatus (PasswordInstance, SystemSupervisor);
  UserStatus = PasswordInstance->GetStatus (PasswordInstance, SystemUser);

  if (EFI_ERROR (SupervisorStatus) && EFI_ERROR (UserStatus)) {
    return EFI_NOT_FOUND;
  }

  TitleString         = GetStringById (STRING_TOKEN (L05_STR_SYSTEM_PASSWORD_TITLET_MSG));
  ErrorStatusMsg      = GetStringById (STRING_TOKEN (L05_STR_SYS_DIALOG_ERROR_STATUS));
  MaximumIncorrectMsg = GetStringById (STRING_TOKEN (L05_STR_SYS_DIALOG_MAXIMUM_ERRORS_MSG));
  SysInvalidPassword  = GetStringById (STRING_TOKEN (L05_STR_PASSWORD_ERROR));

  //
  //  Only need to ask system password when Power on password enable or boot into SCU
  //
  if ((SetupVariable->PowerOnPassword == POWER_ON_PASSWORD) || IntoScu) {
//    PrintAt (0, 0, L"Input System Password !!!");

    while (1)
    {

      Status = gBS->LocateProtocol (
                      &gH2ODialogProtocolGuid,
                      NULL,
                      &H2oDialog
                      );

      if (EFI_ERROR (Status)) {
        return Status;
      }

      H2oDialog->PasswordDialog (
                   0,
                   FALSE,
//_Start_L05_SETUP_MENU_
                   FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1,
//_End_L05_SETUP_MENU_
                   PasswordString,
                   &Key,
                   TitleString
                   );

      Status = PasswordInstance->GetStatus (
                                   PasswordInstance,
                                   SystemSupervisor
                                   );

      if (Status == EFI_SUCCESS) {
        Status = PasswordInstance->CheckPassword (
                                     PasswordInstance,
                                     PasswordString,
//_Start_L05_SETUP_MENU_
                                     (FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1) * sizeof (CHAR16),
//_End_L05_SETUP_MENU_
                                     SystemSupervisor
                                     );

        if (Status == EFI_SUCCESS) {
          Status = gRT->SetVariable (
                          L05_PASSWORD_VARIABLE_NAME,
                          &gL05PasswordVariableGuid,
                          EFI_VARIABLE_BOOTSERVICE_ACCESS,
                          sizeof (PasswordString),
                          PasswordString
                          );

          if (!EFI_ERROR (Status)) {
            SetupVariable->SetUserPass = FALSE;
          }

          return EFI_SUCCESS;
        }
      }

      Status = PasswordInstance->GetStatus (
                                   PasswordInstance,
                                   SystemUser
                                   );

      if (Status == EFI_SUCCESS) {
        Status = PasswordInstance->CheckPassword (
                                     PasswordInstance,
                                     PasswordString,
//_Start_L05_SETUP_MENU_
                                     (FixedPcdGet16 (PcdDefaultSysPasswordMaxLength) + 1) * sizeof (CHAR16),
//_End_L05_SETUP_MENU_
                                     SystemUser
                                     );

        if (Status == EFI_SUCCESS) {
          Status = gRT->SetVariable (
                          L05_PASSWORD_VARIABLE_NAME,
                          &gL05PasswordVariableGuid,
                          EFI_VARIABLE_BOOTSERVICE_ACCESS,
                          sizeof (PasswordString),
                          PasswordString
                          );

          if (!EFI_ERROR (Status)) {
            SetupVariable->SetUserPass = TRUE;
          }

          return EFI_SUCCESS;
        }
      }

      ErrorCount++;

      if (ErrorCount >= EFI_L05_SYSTEM_PASSWORD_MAX_RETRY) {
        //
        //  Replace the original ResetSystem function to prevent USB keyboard reset command
        //
        gRT->ResetSystem = ResetSystemHook;

        gST->ConOut->ClearScreen (gST->ConOut);

        PcdSetBoolS (PcdL05PasswordErrorFlag, TRUE);
        H2oDialog->CreateMsgPopUp (
                     50,
                     2,
                     ErrorStatusMsg,
                     MaximumIncorrectMsg
                     );
      }

      H2oDialog->ConfirmDialog (
                   L05WarningDlgContinueDlg,
                   FALSE,
                   40,
                   NULL,
                   &Key,
                   SysInvalidPassword
                   );

    }
  }

  return EFI_NOT_FOUND;
}

UINTN
L05GetHddInfoIndex (
  IN EFI_HDD_PASSWORD_DIALOG_PROTOCOL   *This,
  IN HDD_PASSWORD_HDD_INFO              *HddInfo
  )
{
  EFI_STATUS                            Status;
  EFI_HDD_PASSWORD_SERVICE_PROTOCOL     *HddPasswordService;
  HDD_PASSWORD_POST_DIALOG_PRIVATE      *HddPasswordPostDialogPrivate;
  HDD_PASSWORD_HDD_INFO                 *HddInfoArray;
  UINTN                                 NumOfHdd;
  UINTN                                 Index;
  UINTN                                 HddInfoIndex;

  HddPasswordService           = NULL;
  HddPasswordPostDialogPrivate = NULL;
  HddInfoArray                 = NULL;
  HddInfoIndex                 = (UINTN) -1;

  HddPasswordPostDialogPrivate = GET_PRIVATE_FROM_HDD_PASSWORD_POST_DIALOG (This);
  HddPasswordService = HddPasswordPostDialogPrivate->HddPasswordService;

  Status = HddPasswordService->GetHddInfo (
                                 HddPasswordService,
                                 &HddInfoArray,
                                 &NumOfHdd
                                 );

  if (NumOfHdd == 0) {
    return (UINTN) -1;
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    if (CompareMem (HddInfo, &HddInfoArray[Index], sizeof (HDD_PASSWORD_HDD_INFO)) == 0) {
      HddInfoIndex = Index;
      break;
    }
  }

  if (HddInfoArray != NULL) {
    FreePool (HddInfoArray);
  }

  if (Index == NumOfHdd) {
    return (UINTN) -1;
  }

  return HddInfoIndex;
}
//_End_L05_HDD_PASSWORD_ENABLE_

/**
  The HII driver handle passed in for HiiDatabase.NewPackageList() requires
  that there should be DevicePath Protocol installed on it.
  This routine create a virtual Driver Handle by installing a vendor device
  path on it, so as to use it to invoke HiiDatabase.NewPackageList().

  @param[out]       DriverHandle         Handle to be returned

  @retval           EFI_SUCCESS          Handle destroy success.
  @retval           EFI_OUT_OF_RESOURCES Not enough memory.

**/
EFI_STATUS
CreateHiiDriverHandle (
  OUT EFI_HANDLE                         *DriverHandle
  )
{
  EFI_STATUS                             Status;

  *DriverHandle = NULL;
  Status = gBS->InstallMultipleProtocolInterfaces (
                  DriverHandle,
                  &gEfiDevicePathProtocolGuid,
                  &mHiiVendorDevicePath,
                  NULL
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  return EFI_SUCCESS;
}

/**
  Get string by string id from HII Interface

  @param[in]        Id                  String ID

  @retval                               String from ID

**/
CHAR16 *
GetStringById (
  IN EFI_STRING_ID                      Id
  )
{
  CHAR16                                *String;

  String = HiiGetString (
             gStringPackHandle,
             Id,
             NULL
             );
  return String;
}

/**
  Initialize HII global accessor for string support

  @param

  @retval

**/
EFI_STATUS
InitializeStringSupport (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_HANDLE                            DriverHandle;

  Status = CreateHiiDriverHandle (&DriverHandle);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  gStringPackHandle = HiiAddPackages (
                        &gHddPasswordStringPackGuid,
                        DriverHandle,
                        HddPasswordStrings,
                        NULL
                        );

  return Status;
}

/**
  Init. StringTokenArray for HddPassword

  @param

  @retval
**/
EFI_STATUS
InitDialogStringTokenArray (
  VOID
  )
{
  CHAR16                                **StrTokenArray;

  StrTokenArray = AllocatePool(STR_TOKEN_NUMBERS * sizeof (CHAR16 *));
  if (StrTokenArray == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  StrTokenArray [STR_HDD_ESC_SKIP_MSG_INDEX]        = GetStringById (STRING_TOKEN (STR_HDD_ESC_SKIP_MSG));
  StrTokenArray [STR_HDD_TITLE_MSG_INDEX]           = GetStringById (STRING_TOKEN (STR_HDD_TITLE_MSG));
  StrTokenArray [STR_HDD_DIALOG_THREE_ERRORS_INDEX] = GetStringById (STRING_TOKEN (STR_HDD_DIALOG_THREE_ERRORS));
  StrTokenArray [STR_HDD_DIALOG_CONTINUE_MSG_INDEX] = GetStringById (STRING_TOKEN (STR_HDD_DIALOG_CONTINUE_MSG));
  StrTokenArray [STR_HDD_DIALOG_ENTER_MSG_INDEX]    = GetStringById (STRING_TOKEN (STR_HDD_DIALOG_ENTER_MSG));
//_Start_L05_HDD_PASSWORD_ENABLE_
//  StrTokenArray [STR_HDD_DIALOG_ERROR_STATUS_INDEX] = GetStringById (STRING_TOKEN (STR_HDD_DIALOG_ERROR_STATUS));
  StrTokenArray [STR_HDD_DIALOG_ERROR_STATUS_INDEX] = GetStringById (STRING_TOKEN (L05_STR_SYS_DIALOG_ERROR_STATUS));
//_End_L05_HDD_PASSWORD_ENABLE_
  StrTokenArray [STR_HDD_ENTER_MANY_ERRORS_INDEX]   = GetStringById (STRING_TOKEN (STR_HDD_ENTER_MANY_ERRORS));
  StrTokenArray [STR_SECURITY_COUNT_EXPIRED_INDEX]  = GetStringById (STRING_TOKEN (STR_SECURITY_COUNT_EXPIRED));
  StrTokenArray [STR_HDD_DIALOG_COLDBOOT_MSG_INDEX] = GetStringById (STRING_TOKEN (STR_HDD_DIALOG_COLDBOOT_MSG));
  StrTokenArray [STR_HDD_DIALOG_HELP_TITLE_INDEX]   = GetStringById (STRING_TOKEN (STR_HDD_DIALOG_HELP_TITLE));
//_Start_L05_HDD_PASSWORD_ENABLE_
  StrTokenArray [L05_STR_SYSTEM_PASSWORD_TITLET_MSG_INDEX]    = GetStringById (STRING_TOKEN (L05_STR_SYSTEM_PASSWORD_TITLET_MSG));
  StrTokenArray [L05_STR_HDD_DIALOG_MASTER_TITLE_MSG_INDEX]   = GetStringById (STRING_TOKEN (L05_STR_HDD_DIALOG_MASTER_TITLE));
  StrTokenArray [L05_STR_HDD_DIALOG_USER_TITLE_MSG_INDEX]     = GetStringById (STRING_TOKEN (L05_STR_HDD_DIALOG_USER_TITLE));
  StrTokenArray [L05_STR_HDD_DIALOG_PASSWORD_ERROR_MSG_INDEX] = GetStringById (STRING_TOKEN (L05_STR_PASSWORD_ERROR));
//_End_L05_HDD_PASSWORD_ENABLE_
//[-start-220214-BAIN000094-add]//
#ifdef LCFC_SUPPORT
#ifndef L05_GRAPHIC_UI_ENABLE
  StrTokenArray [L05_STR_HDD_DIALOG_MASTER_TITLE_MSG_INDEX_TXT]   = GetStringById (STRING_TOKEN (L05_STR_HDD_DIALOG_MASTER_TITLE_TXT));
  StrTokenArray [L05_STR_HDD_DIALOG_USER_TITLE_MSG_INDEX_TXT]     = GetStringById (STRING_TOKEN (L05_STR_HDD_DIALOG_USER_TITLE_TXT));
#endif
#endif
//[-end-220214-BAIN000094-add]//

  mDriverInstallInfo->StrTokenInfo.StrTokenArray    = StrTokenArray;

  return EFI_SUCCESS;
}

/**
  Get StringTokenArray for HddPassword

  @param[out]       StrTokenArray       StringToken array pointer

  @retval           EFI_SUCCESS
**/
EFI_STATUS
GetStringTokenArray (
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *This,
  OUT CHAR16                            **StrTokenArray
  )
{
  CHAR16                                **StrTokenArrayPtr;

  if (This == NULL || StrTokenArray == NULL) {
    return EFI_INVALID_PARAMETER;
  }
  StrTokenArrayPtr = mDriverInstallInfo->StrTokenInfo.StrTokenArray;

  *StrTokenArray = (VOID *)StrTokenArrayPtr;

  return EFI_SUCCESS;
}

/**
  Free SteingTokenArray memory buffer

  @param

  @retval           EFI_SUCCESS

**/
EFI_STATUS
ReleaseStringTokenArray (
  VOID
  )
{
  UINTN                                 Index;
  CHAR16                                **StrTokenArray;

  Index            = 0;
  StrTokenArray    = mDriverInstallInfo->StrTokenInfo.StrTokenArray;

  for (Index = 0; Index < STR_TOKEN_NUMBERS; Index++) {
    if (StrTokenArray[Index] != NULL) {
      FreePool (StrTokenArray[Index]);
    }
  }

  FreePool ((VOID *)StrTokenArray);

  mDriverInstallInfo->StrTokenInfo.StrTokenArray = NULL;

  return EFI_SUCCESS;
}

/**
  To check is SecureBoot reset

  @param

  @retval           EFI_SUCCESS
**/
BOOLEAN
IsAdmiSecureBootReset (
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *This
  )
{
  UINT8                                 AdmiSecureBoot;
  EFI_STATUS                            Status;
  BOOLEAN                               AdmiSecureBootReset;
  UINTN                                 BufferSize;

  AdmiSecureBootReset = FALSE;

  BufferSize = sizeof (UINT8);

  Status = gRT->GetVariable (
                   EFI_ADMINISTER_SECURE_BOOT_NAME,
                   &gEfiGenericVariableGuid,
                   NULL,
                   &BufferSize,
                   &AdmiSecureBoot
                   );

  if (!EFI_ERROR (Status) && AdmiSecureBoot == 1) {
    AdmiSecureBootReset = TRUE;
  }

  return AdmiSecureBootReset;

}


/**
  Copy SourceBufferPtr string to DescBufferPtr.

  @param[in]        DescBufferPtr       Destination buffer address.
  @param[in]        SourceBufferPtr     Source buffer addess.

  @retval           EFI_SUCCESS
**/
EFI_STATUS
GetHddModelNumber (
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *This,
  IN VOID                               *DescBufferPtr,
  IN VOID                               *SourceBufferPtr
  )
{
  if (This == NULL || DescBufferPtr == NULL || SourceBufferPtr == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  return GetModelNumber (DescBufferPtr, SourceBufferPtr) ;
}

VOID
HexToString (
  CHAR16  *String,
  UINTN   Value,
  UINTN   Digits
  )
{
  CHAR16                          HexDigit[17] = L"0123456789ABCDEF";
  for (; Digits > 0; Digits--, String++) {
    *String = HexDigit[((Value >> (4*(Digits-1))) & 0x0f)];
  }
}


/**
  Fill in HDD information string to buffer.

  @param[in]        HddInfoArray            The array of HDD information used in HDD Password.
  @param[in]        NumOfHdd                Number of HDD.
  @param[in, out]   HddDialogItemInfoString String buffer array for HDD password unlock dialog.

  @retval EFI_SUCCESS
**/
EFI_STATUS
PrepareHddDialogItems (
  IN EFI_HDD_PASSWORD_DIALOG_PROTOCOL   *This,
  IN HDD_PASSWORD_HDD_INFO              *HddInfoArray,
  IN UINTN                              NumOfHdd,
  OUT ITEM_INFO_IN_DIALOG               *HddDialogItemInfoString
  )
{
  UINT32                                    Index;
  CHAR16                                    *PortStatusString;
  CHAR16                                    PortNumberString[7];
  ITEM_INFO_IN_DIALOG                       DialogItemInfoTempBuffer;
  UINT16                                    HddPort;
  UINTN                                     StringLength;
  UINTN                                     ModelLength;
//_Start_L05_HDD_PASSWORD_ENABLE_
//  UINTN                                     StartIndex;
//  UINTN                                     EndIndex;
  UINTN                                     TotalStringLength;
//_End_L05_HDD_PASSWORD_ENABLE_
  EFI_STATUS                                Status;  
  EFI_DEVICE_PATH_PROTOCOL                  *DevicePath;
  EFI_DEVICE_PATH_PROTOCOL                  *DevicePathNode;
  NVME_NAMESPACE_DEVICE_PATH                *NVMeDevicePath;

//_Start_L05_HDD_PASSWORD_ENABLE_
  TotalStringLength = 0;
//_End_L05_HDD_PASSWORD_ENABLE_

  if (This == NULL || HddInfoArray == NULL || HddDialogItemInfoString == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if (NumOfHdd == 0) {
    return EFI_UNSUPPORTED;
  }

  StringLength = 0;

  for (Index = 0; Index < NumOfHdd; Index++) {
    SetMem16 ((VOID *)&(DialogItemInfoTempBuffer.ItemInfoString), sizeof (ITEM_INFO_IN_DIALOG), CHAR_SPACE);

//_Start_L05_HDD_PASSWORD_ENABLE_
    //
    //  Copy HDD status to string
    //
    if ((HddInfoArray[Index].HddSecurityStatus & HDD_LOCKED_BIT) != HDD_LOCKED_BIT) {
      PortStatusString = GetStringById (STRING_TOKEN (STR_HDD_PSW_UNLOCK));

    } else {
      PortStatusString = GetStringById (STRING_TOKEN (STR_HDD_PSW_LOCK));
    }

    StrCpyS (
      &(DialogItemInfoTempBuffer.ItemInfoString[0]),
      sizeof(DialogItemInfoTempBuffer.ItemInfoString) / sizeof(CHAR16),
      PortStatusString
      );

    StringLength = StrLen (PortStatusString);

    if (DialogItemInfoTempBuffer.ItemInfoString[StringLength] == CHAR_NULL) {
      DialogItemInfoTempBuffer.ItemInfoString[StringLength] = CHAR_SPACE;
    }

    //
    //  Allocate by GetStringById
    //
    FreePool (PortStatusString);

    //
    // Now TotalStringLength = HDD status
    //
    TotalStringLength += StringLength;
//_End_L05_HDD_PASSWORD_ENABLE_

    SetMem16 ((VOID *)&(PortNumberString), sizeof(CHAR16) * 6, CHAR_SPACE);
//_Start_L05_PREFIX_KERNEL_
    PortNumberString[(sizeof(PortNumberString) / sizeof(CHAR16)) - 1] = CHAR_NULL;
//_End_L05_PREFIX_KERNEL_
    
    PortNumberString[6] = '\0';

    Status = gBS->HandleProtocol (
                    HddInfoArray[Index].DeviceHandleInDxe,
                    &gEfiDevicePathProtocolGuid,
                    (VOID *) &DevicePath
                    );

    NVMeDevicePath = NULL;

    if (Status == EFI_SUCCESS) {
      DevicePathNode = DevicePath;
      while (!IsDevicePathEnd (DevicePathNode)) {
        if ((DevicePathType (DevicePathNode) == MESSAGING_DEVICE_PATH) &&
            (DevicePathSubType (DevicePathNode) == MSG_NVME_NAMESPACE_DP)) {
              NVMeDevicePath = (NVME_NAMESPACE_DEVICE_PATH *) DevicePathNode;
              break;
        }
        DevicePathNode = NextDevicePathNode (DevicePathNode);
      }
    }

    if (HddInfoArray[Index].ControllerMode == ATA_IDE_MODE) {
      HddPort = HddInfoArray[Index].MappedPort;
    } else {
      HddPort = HddInfoArray[Index].PortNumber;
    }

    if (HddInfoArray[Index].ControllerMode < NVMe_MODE && NVMeDevicePath == NULL) {
      PortStatusString = GetStringById (STRING_TOKEN (STR_SATA));
    } else {
       PortStatusString = (NVMeDevicePath != NULL) ? GetStringById (STRING_TOKEN (STR_NVME)) : GetStringById (STRING_TOKEN (STR_EMMC));
    }


    //
    //  Copy which port to string
    //
    StrCpyS (
//_Start_L05_HDD_PASSWORD_ENABLE_
//      &(DialogItemInfoTempBuffer.ItemInfoString[0]),
      &(DialogItemInfoTempBuffer.ItemInfoString[TotalStringLength]),
//_End_L05_HDD_PASSWORD_ENABLE_
      sizeof(DialogItemInfoTempBuffer.ItemInfoString) / sizeof(CHAR16),
      PortStatusString
      );
    if (PortStatusString == NULL) {
      return EFI_INVALID_PARAMETER;
    }
    StringLength = StrLen (PortStatusString);
//_Start_L05_HDD_PASSWORD_ENABLE_
    //
    // Now TotalStringLength = HDD status + Port Status
    //
    TotalStringLength += StringLength;
//_End_L05_HDD_PASSWORD_ENABLE_

//_Start_L05_HDD_PASSWORD_ENABLE_
//    if (DialogItemInfoTempBuffer.ItemInfoString[StringLength] == CHAR_NULL) {
//      DialogItemInfoTempBuffer.ItemInfoString[StringLength] = CHAR_SPACE;
    if (DialogItemInfoTempBuffer.ItemInfoString[TotalStringLength] == CHAR_NULL) {
      DialogItemInfoTempBuffer.ItemInfoString[TotalStringLength] = CHAR_SPACE;
    }

    TotalStringLength++;  // Add CHAR_SPACE
//_End_L05_HDD_PASSWORD_ENABLE_

    //
    //  Allocate by GetStringById
    //
    FreePool (PortStatusString);

    if (HddInfoArray[Index].ControllerMode < NVMe_MODE) {
//_Start_L05_HDD_PASSWORD_ENABLE_
//      HexToString(PortNumberString, HddPort, 2);
      HexToString(PortNumberString, HddInfoArray[Index].PortNumber, 1);
//_End_L05_HDD_PASSWORD_ENABLE_
      StrCpyS (
//_Start_L05_HDD_PASSWORD_ENABLE_
//        &(DialogItemInfoTempBuffer.ItemInfoString[StringLength]),
        &(DialogItemInfoTempBuffer.ItemInfoString[TotalStringLength]),
//_End_L05_HDD_PASSWORD_ENABLE_
        sizeof(DialogItemInfoTempBuffer.ItemInfoString) / sizeof(CHAR16),
        PortNumberString
        );

//_Start_L05_HDD_PASSWORD_ENABLE_
//      StringLength += StrLen (PortNumberString);
      //
      // Now TotalStringLength = HDD status + Port Status + Port Number
      //
      TotalStringLength +=  StrLen (PortNumberString);
//_End_L05_HDD_PASSWORD_ENABLE_

//_Start_L05_HDD_PASSWORD_ENABLE_
//      if (DialogItemInfoTempBuffer.ItemInfoString[StringLength] == CHAR_NULL) {
//        DialogItemInfoTempBuffer.ItemInfoString[StringLength] = CHAR_SPACE;
      if (DialogItemInfoTempBuffer.ItemInfoString[TotalStringLength] == CHAR_NULL) {
        DialogItemInfoTempBuffer.ItemInfoString[TotalStringLength] = CHAR_SPACE;
//_End_L05_HDD_PASSWORD_ENABLE_
      }
    }

    //
    //  Copy model string to string
    //
//_Start_L05_HDD_PASSWORD_ENABLE_
//    if (PcdGetBool (PcdH2OFormBrowserLocalMetroDESupported)) {
//      //
//      //  For GUI mode
//      //
//      ModelLength = StrLen (HddInfoArray[Index].HddModelString);
//      CopyMem (
//        &(DialogItemInfoTempBuffer.ItemInfoString[StringLength]),
//        (CHAR16*)HddInfoArray[Index].HddModelString,
//        MODEL_NUMBER_LENGTH * sizeof (CHAR16)
//        );
//      while (DialogItemInfoTempBuffer.ItemInfoString[StringLength + ModelLength - 1] == CHAR_SPACE) {
//        ModelLength--;
//      }
//      
//      //
//      //  Copy HDD status to string
//      //
//      if ((HddInfoArray[Index].HddSecurityStatus & HDD_LOCKED_BIT) != HDD_LOCKED_BIT) {
//        PortStatusString = GetStringById (STRING_TOKEN (STR_HDD_PSW_UNLOCK));
//      } else {
//        PortStatusString = GetStringById (STRING_TOKEN (STR_HDD_PSW_LOCK));
//      }
//
//      //
//      //  change PortStatusString form in GUI mode
//      //
//      StartIndex = 0;
//      if (PortStatusString == NULL) {
//        return EFI_INVALID_PARAMETER;
//      }      
//      EndIndex = StrLen (PortStatusString) - 1;
//      while (StartIndex <= EndIndex) {
//        if (PortStatusString[StartIndex] == CHAR_SPACE) {
//          StartIndex++;
//        } else if (PortStatusString[EndIndex] == CHAR_SPACE) {
//          PortStatusString[EndIndex] = 0;
//          EndIndex--;
//        } else {
//          break;
//        }
//      }
//
//      DialogItemInfoTempBuffer.ItemInfoString[StringLength + ModelLength - 1] = '(';
//      StrCpyS (
//        &(DialogItemInfoTempBuffer.ItemInfoString[StringLength + ModelLength]),
//        (sizeof(DialogItemInfoTempBuffer.ItemInfoString) / sizeof(CHAR16)) - (StringLength + ModelLength),
//        &PortStatusString[StartIndex]
//        );
//      DialogItemInfoTempBuffer.ItemInfoString[StrLen (DialogItemInfoTempBuffer.ItemInfoString)] = ')';
//      
//      //
//      //  Allocate by GetStringById
//      //
//      FreePool (PortStatusString);
//    } else {
//      //
//      //  For text mode
//      //
//      ModelLength = StrLen (HddInfoArray[Index].HddModelString);
//      CopyMem (
//        &(DialogItemInfoTempBuffer.ItemInfoString[StringLength]),
//        (CHAR16*)HddInfoArray[Index].HddModelString,
//        MODEL_NUMBER_LENGTH * sizeof (CHAR16)
//        );
//      while (DialogItemInfoTempBuffer.ItemInfoString[StringLength + ModelLength - 1] == CHAR_SPACE) {
//        ModelLength--;
//      }
//
//      //
//      //  Copy HDD status to string
//      //
//      if ((HddInfoArray[Index].HddSecurityStatus & HDD_LOCKED_BIT) != HDD_LOCKED_BIT) {
//        PortStatusString = GetStringById (STRING_TOKEN (STR_HDD_PSW_UNLOCK));
//      } else {
//        PortStatusString = GetStringById (STRING_TOKEN (STR_HDD_PSW_LOCK));
//      }
//
//      //
//      //  change PortStatusString form in GUI mode
//      //
//      StartIndex = 0;
//      if (PortStatusString == NULL) {
//        return EFI_INVALID_PARAMETER;
//      }      
//      EndIndex = StrLen (PortStatusString) - 1;
//      while (StartIndex <= EndIndex) {
//        if (PortStatusString[StartIndex] == CHAR_SPACE) {
//          StartIndex++;
//        } else if (PortStatusString[EndIndex] == CHAR_SPACE) {
//          PortStatusString[EndIndex] = 0;
//          EndIndex--;
//        } else {
//          break;
//        }
//      }
//
//      DialogItemInfoTempBuffer.ItemInfoString[StringLength + ModelLength - 1] = '(';
//      StrCpyS (
//        &(DialogItemInfoTempBuffer.ItemInfoString[StringLength + ModelLength]),
//        (sizeof(DialogItemInfoTempBuffer.ItemInfoString) / sizeof(CHAR16)) - (StringLength + ModelLength),
//        &PortStatusString[StartIndex]
//        );
//      DialogItemInfoTempBuffer.ItemInfoString[StrLen (DialogItemInfoTempBuffer.ItemInfoString)] = ')';
//      //
//      //  Allocate by GetStringById
//      //
//      FreePool (PortStatusString);
//    }
    ModelLength = StrLen (HddInfoArray[Index].HddModelString);

    //
    // Dispaly info will be HDD status + Port info + Model name
    //
    CopyMem (
      &(DialogItemInfoTempBuffer.ItemInfoString[TotalStringLength]),
      (CHAR16*)HddInfoArray[Index].HddModelString,
      MODEL_NUMBER_LENGTH * sizeof (CHAR16)
      );

    while (DialogItemInfoTempBuffer.ItemInfoString[TotalStringLength + ModelLength - 1] == CHAR_SPACE) {
      ModelLength--;
    }

    //
    // Clear string length for the next dialog info
    //
    TotalStringLength = 0;
//_End_L05_HDD_PASSWORD_ENABLE_

    DialogItemInfoTempBuffer.ItemInfoString[OPTION_ICON_MAX_STR_SIZE - 1] = CHAR_NULL;
    CopyMem (
      HddDialogItemInfoString[Index].ItemInfoString,
      DialogItemInfoTempBuffer.ItemInfoString,
      (OPTION_ICON_MAX_STR_SIZE * sizeof(CHAR16))
      );
  }

  return EFI_SUCCESS;
}

/**
  Checking all HDD security status is LOCK or not.

  @param[in]        HddInfoArray        The array of HDD information used in HDD Password.
  @param[in]        NumOfHdd            Number of HDD.

  @retval           TRUE                Found harddisk is locked.
  @retval           FALSE               Could not found any harddisk is locked.
**/
BOOLEAN
CheckHddLock (
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *This,
  IN HDD_PASSWORD_HDD_INFO              *HddInfoArray,
  IN UINTN                              NumOfHdd
  )
{
  UINTN                                 Index;

  if (This == NULL || HddInfoArray == NULL) {
    return FALSE;
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    if ((HddInfoArray[Index].HddSecurityStatus & HDD_LOCKED_BIT) == HDD_LOCKED_BIT) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
  Checking all HDD security status is Enabled or not.

  @param[in]        HddInfoArray        The array of HDD information used in HDD Password.
  @param[in]        NumOfHdd            Number of HDD.

  @retval           TRUE                Found one of HDD is security Enabled.
  @retval           FALSE               Could not found any HDD is  security Enabled.
**/
BOOLEAN
CheckHddSecurityEnable (
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *This,
  IN HDD_PASSWORD_HDD_INFO              *HddInfoArray,
  IN UINTN                              NumOfHdd
  )
{
  UINTN                                 Index;

  if (This == NULL || HddInfoArray == NULL) {
    return FALSE;
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    if ((HddInfoArray[Index].HddSecurityStatus & HDD_ENABLE_BIT) == HDD_ENABLE_BIT) {
       return TRUE;
    }
  }
  return FALSE;
}

EFI_STATUS
ResetAllSecurityStatus (
  IN EFI_HDD_PASSWORD_DIALOG_PROTOCOL   *This,
  IN HDD_PASSWORD_HDD_INFO              *HddInfoArray,
  IN UINTN                              NumOfHdd
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  UINT8                                 SkipCounter;
  EFI_HDD_PASSWORD_SERVICE_PROTOCOL     *HddPasswordService;
  HDD_PASSWORD_POST_DIALOG_PRIVATE      *HddPasswordPostDialogPrivate;

  if (This == NULL || HddInfoArray == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  HddPasswordPostDialogPrivate = GET_PRIVATE_FROM_HDD_PASSWORD_POST_DIALOG (This);

  HddPasswordService = HddPasswordPostDialogPrivate->HddPasswordService;

  SkipCounter = 0;


  for (Index = 0; Index < NumOfHdd; Index++) {
    if (((HddInfoArray[Index].HddSecurityStatus & HDD_LOCKED_BIT) != HDD_LOCKED_BIT) &&
        ((HddInfoArray[Index].HddSecurityStatus & HDD_ENABLE_BIT) == HDD_ENABLE_BIT)) {

        Status = HddPasswordService->ResetSecurityStatus (
                                       HddPasswordService,
                                       &(HddInfoArray[Index])
                                       );

    }

    if (((HddInfoArray[Index].HddSecurityStatus & HDD_LOCKED_BIT) != HDD_LOCKED_BIT) ||
        ((HddInfoArray[Index].HddSecurityStatus & HDD_FROZEN_BIT) == HDD_FROZEN_BIT)) {
      SkipCounter++;
    }
    if (SkipCounter == NumOfHdd) {
      //
      //  No any HDD having to be unlock
      //
      return EFI_SUCCESS;
    }
  }

  return EFI_NOT_READY;
}

EFI_STATUS
SelectedHddPrepareTitleString (
  IN EFI_HDD_PASSWORD_DIALOG_PROTOCOL   *This,
  IN HDD_PASSWORD_HDD_INFO              *HddInfo,
  OUT CHAR16                            **HddTitleString
  )
{
  EFI_STATUS                            Status;
  CHAR16                                **StrTokenArray;
  CHAR16                                *HddTitleStringPtr;
//[-start-220214-BAIN000094-add]//
#ifdef LCFC_SUPPORT
#ifndef L05_GRAPHIC_UI_ENABLE
  CHAR16                                SelectedHddString[DEVICE_MODEL_NAME_STRING_LENGTH + 1];
  UINTN                                 StringIndex = 0;
#endif
#endif
//[-end-220214-BAIN000094-add]//

  if (This == NULL || HddInfo == NULL || HddTitleString == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status            = EFI_SUCCESS;
  StrTokenArray     = NULL;
  HddTitleStringPtr = (CHAR16 *)HddTitleString;

  Status = This->GetStringTokenArray (
                   This,
                   (VOID *)&StrTokenArray
                   );
  if (Status != EFI_SUCCESS) {
    return Status;
  }

  SetMem16 (HddTitleStringPtr, HDD_TITLE_STRING_LENGTH * sizeof (CHAR16), CHAR_SPACE);
  StrCpyS (HddTitleStringPtr, HDD_TITLE_STRING_LENGTH, StrTokenArray[STR_HDD_DIALOG_HELP_TITLE_INDEX]);
  CopyMem (&(HddTitleStringPtr[StrLen(StrTokenArray[STR_HDD_DIALOG_HELP_TITLE_INDEX])]), HddInfo->HddModelString, MODEL_NUMBER_LENGTH * sizeof (CHAR16));

//[-start-220214-BAIN000094-modify]//
#ifdef LCFC_SUPPORT
#ifndef L05_GRAPHIC_UI_ENABLE
    //
    // Copy Model String
    //
    ZeroMem (SelectedHddString, (DEVICE_MODEL_NAME_STRING_LENGTH + sizeof (CHAR16)));
    CopyMem (SelectedHddString, HddInfo->HddModelString, DEVICE_MODEL_NAME_STRING_LENGTH * sizeof (CHAR16));
    //
    // Clean space from tail
    //
    StringIndex = DEVICE_MODEL_NAME_STRING_LENGTH - 1;
    
    while (SelectedHddString[StringIndex] == 0x0020) {
      SetMem (&SelectedHddString[StringIndex], sizeof (CHAR16), 0x0000);
      StringIndex--;
    }
#endif
#endif
//[-end-220214-BAIN000094-modify]//

#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
//
// [Lenovo Notebook Password Design Spec V1.2]
//   4.1.1. Check HDP
//     User can press F1 to switch Master Password and User Password.
//     Figure 4-1 Check Master Password
//       Title - Enter HD 1 Master Password
//     Figure 4-2 Check User Password
//       Title - Enter HD 1 User Password
//
{
  CHAR16                                *FormatString;
  UINTN                                 HddInfoIndex;

  ZeroMem (HddTitleStringPtr, (HDD_TITLE_STRING_LENGTH * sizeof (CHAR16)));
  HddInfoIndex = L05GetHddInfoIndex (This, HddInfo);
  HddInfoIndex = (HddInfoIndex == (UINTN)-1) ? 0 : HddInfoIndex + 1;

//[-start-220214-BAIN000094-modify]//
#ifdef LCFC_SUPPORT
#ifndef L05_GRAPHIC_UI_ENABLE
    if (mPasswordType == MASTER_PSW) {
      FormatString = StrTokenArray[L05_STR_HDD_DIALOG_MASTER_TITLE_MSG_INDEX_TXT];
      } else {
      FormatString = StrTokenArray[L05_STR_HDD_DIALOG_USER_TITLE_MSG_INDEX_TXT];
    }
#else
    if (mPasswordType == MASTER_PSW) {
      FormatString = StrTokenArray[L05_STR_HDD_DIALOG_MASTER_TITLE_MSG_INDEX];
      } else {
      FormatString = StrTokenArray[L05_STR_HDD_DIALOG_USER_TITLE_MSG_INDEX];
    }
#endif
#else
  if (mPasswordType == MASTER_PSW) {
    FormatString = StrTokenArray[L05_STR_HDD_DIALOG_MASTER_TITLE_MSG_INDEX];
    } else {
    FormatString = StrTokenArray[L05_STR_HDD_DIALOG_USER_TITLE_MSG_INDEX];
  }
#endif
//[-end-220214-BAIN000094-modify]//

//[-start-220214-BAIN000094-add]//
#ifdef LCFC_SUPPORT
#ifndef L05_GRAPHIC_UI_ENABLE
//[-start-220225-BAIN000098-modify]//
  UnicodeSPrint (HddTitleStringPtr, ((HDD_TITLE_STRING_LENGTH - 1) * sizeof (CHAR16)), FormatString, HddInfoIndex ,SelectedHddString);
//[-end-220225-BAIN000098-modify]//
#else
  UnicodeSPrint (HddTitleStringPtr, (HDD_TITLE_STRING_LENGTH * sizeof (CHAR16)), FormatString, HddInfoIndex);
#endif
#else
  UnicodeSPrint (HddTitleStringPtr, (HDD_TITLE_STRING_LENGTH * sizeof (CHAR16)), FormatString, HddInfoIndex);
#endif
//[-end-220214-BAIN000094-add]//
}
#endif

//[-start-220225-BAIN000098-modify]//
#if defined(S370_SUPPORT)

#else
  HddTitleStringPtr[StrLen (StrTokenArray[STR_HDD_DIALOG_HELP_TITLE_INDEX]) + MODEL_NUMBER_LENGTH] = CHAR_NULL;
#endif
//[-end-220225-BAIN000098-modify]//

  return Status;
}

EFI_STATUS
ShowErrorCountExpiredMessageAndSystemStop (
  IN EFI_HDD_PASSWORD_DIALOG_PROTOCOL   *HddPasswordDialog
  )
{
  CHAR16                                **StrTokenArray;
  HDD_PASSWORD_POST_DIALOG_PRIVATE      *HddPasswordPostDialogPrivate;
  H2O_DIALOG_PROTOCOL                   *H2oDialogProtocol;


  if (HddPasswordDialog == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  HddPasswordPostDialogPrivate = GET_PRIVATE_FROM_HDD_PASSWORD_POST_DIALOG (HddPasswordDialog);

  H2oDialogProtocol = HddPasswordPostDialogPrivate->H2oDialogProtocol;

  HddPasswordDialog->GetStringTokenArray (
                       HddPasswordDialog,
                       (VOID *)&StrTokenArray
                       );

//_Start_L05_HDD_PASSWORD_ENABLE_
  //
  //  Follow L05 Spec 1.15
  //    BIOS must lock system after enter two error Hdd password
  //
  gRT->ResetSystem = ResetSystemHook;
  PcdSetBoolS (PcdL05PasswordErrorFlag, TRUE);
//_End_L05_HDD_PASSWORD_ENABLE_

//_Start_L05_HDD_PASSWORD_ENABLE_
//_Start_L05_HDD_PASSWORD_ENABLE_
//[-start-211202-Dennis0010-modify]//
#if defined(S370_SUPPORT)
  H2oDialogProtocol->CreateMsgPopUp (
                       50,
                       2,
                       StrTokenArray[STR_HDD_DIALOG_ERROR_STATUS_INDEX],
                       GetStringById (STRING_TOKEN (L05_STR_HDD_DIALOG_CHECK_FAIL_MORE_THAN_3_TIMES_MSG))
                       );
#else
//  H2oDialogProtocol->CreateMsgPopUp (
//                       HDD_TITLE_STRING_LENGTH,
//                       4,
//                       StrTokenArray[STR_HDD_DIALOG_ERROR_STATUS_INDEX],
//                       StrTokenArray[STR_HDD_ENTER_MANY_ERRORS_INDEX],
//                       StrTokenArray[STR_SECURITY_COUNT_EXPIRED_INDEX],
//                       StrTokenArray[STR_HDD_DIALOG_COLDBOOT_MSG_INDEX]
//                       );
  //
  // [Lenovo Notebook Password Design Spec V1.2]
  //   4.1.1. Check HDP
  //     Figure 4-4 Check fail more than 3 times
  //
  PcdSetBoolS (PcdL05SetupErrorDialogFlag, TRUE);
  PcdSetBoolS (PcdL05ShowDialogTitleStringFlag, TRUE);
  PcdSetBoolS (PcdL05SetupAlignmentCenterFlag, TRUE);
  H2oDialogProtocol->CreateMsgPopUp (
                       50,
                       2,
                       StrTokenArray[STR_HDD_DIALOG_ERROR_STATUS_INDEX],
                       GetStringById (STRING_TOKEN (L05_STR_HDD_DIALOG_CHECK_FAIL_MORE_THAN_3_TIMES_MSG))
                       );
#endif
//[-end-211202-Dennis0010-modify]//
//_End_L05_HDD_PASSWORD_ENABLE_

//_Start_L05_HDD_PASSWORD_ENABLE_
//  CpuDeadLoop ();
//_End_L05_HDD_PASSWORD_ENABLE_

  return EFI_SUCCESS;
}

/**
  To show the selected HDD unlock dialog and to unlock HDD by inputing HDD password.

  @param[in]        HddPasswordService  EFI_HDD_PASSWORD_SERVICE_PROTOCOL instance.
  @param[in]        HddInfo             The HDD information used in HDD Password.
  @param[in]        OemFormBrowserPtr   EFI_OEM_FORM_BROWSER_PROTOCOL instance.
  @param[in]        HddInfoIndex        HDD information index.

  @retval EFI_SUCCESS

**/
EFI_STATUS
UnlockSelectedHdd (
  IN EFI_HDD_PASSWORD_DIALOG_PROTOCOL   *This,
  IN HDD_PASSWORD_HDD_INFO              *HddInfo,
  OUT PASSWORD_INFORMATION              *PasswordInfo
  )
{
  EFI_STATUS                            Status;
  EFI_STATUS                            UnlockHddStatus;
  CHAR16                                HddTitleString[HDD_TITLE_STRING_LENGTH];
  CHAR16                                UnicodePasswordString[HDD_PASSWORD_MAX_NUMBER + 1];
  UINTN                                 UnicodePasswordLength;
  EFI_INPUT_KEY                         Key;
  CHAR16                                **StrTokenArray;
  UINT8                                 PasswordToHdd[HDD_PASSWORD_MAX_NUMBER + 1];
  UINTN                                 PasswordToHddLength;
  HDD_PASSWORD_POST_DIALOG_PRIVATE      *HddPasswordPostDialogPrivate;
  H2O_DIALOG_PROTOCOL                   *H2oDialogProtocol;
  EFI_HDD_PASSWORD_SERVICE_PROTOCOL     *HddPasswordService;

  if (This == NULL || HddInfo == NULL || PasswordInfo == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if ((HddInfo->HddSecurityStatus & HDD_LOCKED_BIT) != HDD_LOCKED_BIT) {
    return EFI_SUCCESS;
  }

  HddPasswordPostDialogPrivate = GET_PRIVATE_FROM_HDD_PASSWORD_POST_DIALOG (This);

  HddPasswordService = HddPasswordPostDialogPrivate->HddPasswordService;
  H2oDialogProtocol = HddPasswordPostDialogPrivate->H2oDialogProtocol;

  StrTokenArray = NULL;
#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
  //
  // [Lenovo Notebook Password Design Spec V1.1]
  //   4.1.1. Check HDP
  //     Lenovo BIOS require Master Password at first.
  //     User can press F1 to switch Master Password and User Password.
  //
  mPasswordType = MASTER_PSW;
  while (1) {
#endif

  Key.ScanCode    = SCAN_NULL;
  Key.UnicodeChar = CHAR_NULL;

  UnlockHddStatus = EFI_SUCCESS;
  UnicodePasswordLength = 0;

  This->GetStringTokenArray (
          This,
          (VOID *)&StrTokenArray
          );

  Status = SelectedHddPrepareTitleString (
             This,
             HddInfo,
             (CHAR16 **)&HddTitleString
             );

  ZeroMem (UnicodePasswordString, (HDD_PASSWORD_MAX_NUMBER + 1) * sizeof(CHAR16));

#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
{
  UINTN                                 HddInfoIndex;
  UINTN                                 NumberLength;
  CHAR16                                *FormatString;
  CHAR16                                *F1String;
  UINTN                                 StringSize;
  CHAR8                                 *PlatformLangVar;

  F1String        = NULL;
  PlatformLangVar = NULL;

  HddInfoIndex = L05GetHddInfoIndex (This, HddInfo);
  HddInfoIndex = (HddInfoIndex == (UINTN)-1) ? 0 : HddInfoIndex + 1;

  L05_GET_NUMBER_LENGTH (HddInfoIndex, NumberLength);

  FormatString = GetStringById (STRING_TOKEN (L05_STR_PRESS_F1_SWITCH_MASTER_AND_USER_PASSWORD_MSG));
  StringSize = StrSize (FormatString) + (NumberLength * sizeof (CHAR16));
  F1String = AllocateZeroPool (StringSize);

  if (F1String == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  PlatformLangVar = CommonGetVariableData (
                      L"PlatformLang",
                      &gEfiGlobalVariableGuid
                      );

  if (PlatformLangVar == NULL) {
    PlatformLangVar = L05_RFC_3066_ENGLISH_CODE;
  }

  if (AsciiStrCmp (PlatformLangVar, L05_RFC_3066_SIMPLIFIED_CHINESE_CODE) != 0) {
    StrCpyS (F1String, (StringSize / sizeof (CHAR16)), FormatString);
  } else {
    UnicodeSPrint (F1String, StringSize, FormatString, (HddInfoIndex));
  }

  PcdSetPtrS (PcdL05NotebookPasswordDesignDialogF1String, &StringSize, (VOID *) F1String);
  FreePool (FormatString);
  FreePool (F1String);
  FreePool (PlatformLangVar);

  PcdSetBoolS (PcdL05NotebookPasswordDesignDialogF1Flag, TRUE);
}
#endif
//[-start-2200405-BAIN000100-add]//
#if defined(S370_SUPPORT)
  PcdSetBoolS(PcdHddStrWAFlag1,TRUE);
#endif
//[-end-2200405-BAIN000100-add]//

  Status = H2oDialogProtocol->PasswordDialog (
                                0,
                                FALSE,
#ifndef L05_NOTEBOOK_PASSWORD_ENABLE
                                (MIN (PcdGet16 (PcdH2OHddPasswordMaxLength), HddInfo->MaxPasswordLengthSupport) + 1),
#else
                                (PcdGet16 (PcdH2OHddPasswordMaxLength) + 1),
#endif
                                UnicodePasswordString,
                                &Key,
                                HddTitleString
                                );

#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
  PcdSetBoolS (PcdL05NotebookPasswordDesignDialogF1Flag, FALSE);
#endif

  if (EFI_ERROR (Status)) {
    return Status;
  }

#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
  //
  // [Lenovo Notebook Password Design Spec V1.1]
  //   4.1.1. Check HDP
  //     User can press F1 to switch Master Password and User Password.
  //
  if (Key.ScanCode != SCAN_F1) {
    break;
  } 

  mPasswordType = (mPasswordType == MASTER_PSW) ? USER_PSW : MASTER_PSW;
  }
#endif

  UnicodePasswordLength = StrLen (UnicodePasswordString);

  ZeroMem (PasswordToHdd, (HDD_PASSWORD_MAX_NUMBER + 1));
  Status = HddPasswordService->PasswordStringProcess (
                                 HddPasswordService,
#ifndef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
                                 HddInfo->StorageTcgSecuritySupported ? MASTER_PSW : USER_PSW,
#else
                                 mPasswordType,
#endif
                                 UnicodePasswordString,
                                 UnicodePasswordLength,
                                 (VOID **)&PasswordToHdd,
                                 &PasswordToHddLength
                                 );

  if (PasswordToHddLength == 0) {
    return Status;
  }

  UnlockHddStatus = HddPasswordService->UnlockHddPassword (
                                          HddPasswordService,
                                          HddInfo,
#ifndef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
                                          HddInfo->StorageTcgSecuritySupported ? MASTER_PSW : USER_PSW,
#else
                                          mPasswordType,
#endif
                                          PasswordToHdd,
                                          PasswordToHddLength
                                          );
  if (!EFI_ERROR (UnlockHddStatus) && !((HddInfo->HddSecurityStatus & HDD_LOCKED_BIT) == HDD_LOCKED_BIT)) {
    CopyMem (
      PasswordInfo->PasswordStr,
      UnicodePasswordString,
      (UnicodePasswordLength * sizeof (CHAR16))
      );

#ifndef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
    PasswordInfo->PasswordType = HddInfo->StorageTcgSecuritySupported ? MASTER_PSW : USER_PSW;
#else
    PasswordInfo->PasswordType = mPasswordType;
#endif

  } else {
    //
    //To check the Identify word 128 Security count expired bit
    //It is set to one if enter incorrect password too many times.
    //
    if ((HddInfo->HddSecurityStatus & HDD_EXPIRED_BIT) == HDD_EXPIRED_BIT) {
      ShowErrorCountExpiredMessageAndSystemStop (This);
    }

#ifndef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
    UnlockHddStatus = HddPasswordService->UnlockHddPassword (
                                            HddPasswordService,
                                            HddInfo,
                                            HddInfo->StorageTcgSecuritySupported ? USER_PSW : MASTER_PSW,
                                            PasswordToHdd,
                                            PasswordToHddLength
                                            );
    if (!EFI_ERROR (UnlockHddStatus) && !((HddInfo->HddSecurityStatus & HDD_LOCKED_BIT) == HDD_LOCKED_BIT)) {
      CopyMem (
        PasswordInfo->PasswordStr,
        UnicodePasswordString,
        (UnicodePasswordLength * sizeof (CHAR16))
        );
      PasswordInfo->PasswordType = HddInfo->StorageTcgSecuritySupported ? USER_PSW : MASTER_PSW;
    }
#endif

    if (HddInfo->StorageTcgSecuritySupported) {
      if (PasswordToHddLength > 0) {
        ZeroMem (PasswordToHdd, PasswordToHddLength);
      }
    }
    return UnlockHddStatus;
  }

  //
  // zeroing the PWD content in memory after it's being used
  //
  if (HddInfo->StorageTcgSecuritySupported) {
    if (UnicodePasswordLength > 0) {
      ZeroMem (UnicodePasswordString, (UnicodePasswordLength * sizeof (CHAR16)));
    }

    if (PasswordToHddLength > 0) {
      ZeroMem (PasswordToHdd, PasswordToHddLength);
    }
  }



  return UnlockHddStatus;
}

/**
  Check the valid skip Hot-Key

  @param[in]        SkipDialogKey       Check the key is skip dialog Hot-Key or not

  @retval           EFI_NOT_FOUND       SkipDialogKey is not Hot-Key
  @retval           EFI_SUCCESS         Checking is successful and SkipDialogKey is Hot-Key
**/
EFI_STATUS
CheckSkipDialogKey (
  IN  EFI_HDD_PASSWORD_DIALOG_PROTOCOL  *This,
  IN  VOID                              *SkipDialogKey
  )
{
  EFI_INPUT_KEY                         HotKey;

  HotKey.ScanCode    = SCAN_ESC;
  HotKey.UnicodeChar = CHAR_NULL;


  if (SkipDialogKey == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if (((EFI_INPUT_KEY *)SkipDialogKey)->ScanCode == SCAN_NULL &&
      ((EFI_INPUT_KEY *)SkipDialogKey)->UnicodeChar == CHAR_NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if (((EFI_INPUT_KEY *)SkipDialogKey)->ScanCode == HotKey.ScanCode &&
      ((EFI_INPUT_KEY *)SkipDialogKey)->UnicodeChar == HotKey.UnicodeChar) {
    return EFI_SUCCESS;
  }

  return EFI_NOT_FOUND;
}



/**
  Update extend data area.

  @param[in]        HddPasswordTable     Pointer to HddPasswordTable
  @param[in]        PasswordType         Password type

**/
VOID
UpdateExtData (
  IN  HDD_PASSWORD_TABLE                *HddPasswordTable,
  IN  UINT8                              PasswordType
  )
{
  TCG_OPAL_EXT_DATA                     *TcgExtDataPtr;

  TcgExtDataPtr = (TCG_OPAL_EXT_DATA *)(HddPasswordTable + 1);
  if (TcgExtDataPtr->Signature == TCG_OPAL_EXT_DATA_SIGNATURE) {
    if (PasswordType == MASTER_PSW) {
      //
      // Record as SID password
      //
      CopyMem (
        TcgExtDataPtr->SIDPasswordStr,
        HddPasswordTable->PasswordStr,
        StrSize (HddPasswordTable->PasswordStr)
        );
      //
      // Record as Admin1 password
      //
      CopyMem (
        TcgExtDataPtr->Admin1PasswordStr,
        HddPasswordTable->PasswordStr,
        StrSize (HddPasswordTable->PasswordStr)
        );
    } else { // USER_PSW
      CopyMem (
        TcgExtDataPtr->User1PasswordStr,
        HddPasswordTable->PasswordStr,
        StrSize (HddPasswordTable->PasswordStr)
        );
    }
  }
}


/**
  Create new HDD password table.

  @param[in]        NumOfHdd     The number of HDDs.
  @param[in]        TableSize    The size of table allocated.

  @retval           HddPasswordTable pointer

**/
VOID *
CreateHddPasswordTable (
  IN     UINTN                          NumOfHdd,
  IN     HDD_PASSWORD_HDD_INFO          *HddInfoArray,
  OUT    UINTN                          *TableSize
  )
{
  HDD_PASSWORD_TABLE                    *HddPasswordTable;
  UINTN                                 TempHddPasswordTableSize;
  UINTN                                 OldHddPasswordTableSize;
  UINTN                                 NewHddPasswordTableSize;  
  
  HDD_PASSWORD_TABLE                    *HddPasswordTablePtr;
  TCG_OPAL_EXT_DATA                     *TcgExtDataPtr;
  UINTN                                 Index;
  EFI_STATUS                            Status;
  VOID                                  *TempPtr;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePath;  
  
  OldHddPasswordTableSize = 0;
  TempHddPasswordTableSize = 0;
  NewHddPasswordTableSize = 0;
  HddPasswordTable = NULL;

  //
  // Fill out the TCG Opal extend data size and sig and devicepath
  //
  for (Index = 0; Index < NumOfHdd; Index++) {
    Status = gBS->HandleProtocol (
                    HddInfoArray[Index].DeviceHandleInDxe,
                    &gVmdPhySataDevicePathGuid,
                    (VOID *) &DevicePath
                    );
    if (EFI_ERROR(Status)) {
      Status = gBS->HandleProtocol (
                      HddInfoArray[Index].DeviceHandleInDxe,
                      &gEfiDevicePathProtocolGuid,
                      (VOID *) &DevicePath
                      );
    }
    OldHddPasswordTableSize = NewHddPasswordTableSize; 
    TempHddPasswordTableSize = GetDevicePathSize(DevicePath) + GetExtDataSize () + sizeof(HDD_PASSWORD_TABLE);
    NewHddPasswordTableSize = TempHddPasswordTableSize + OldHddPasswordTableSize;
    HddPasswordTable = (HDD_PASSWORD_TABLE*)ReallocatePool (
                                              OldHddPasswordTableSize,
                                              NewHddPasswordTableSize,
                                              (VOID *)HddPasswordTable);
    //
    // get the table entry with index
    //
    HddPasswordTablePtr = GetTableEntryWithIndex (HddPasswordTable, Index);
    if (HddPasswordTablePtr == NULL) {
      return NULL;
    }
    HddPasswordTablePtr->ExtDataSize = GetExtDataSize ();
    TcgExtDataPtr = (TCG_OPAL_EXT_DATA *)(HddPasswordTablePtr + 1);
    if (PcdGetBool (PcdH2OHddPasswordTcgOpalSupported)) {
      TcgExtDataPtr->Signature = TCG_OPAL_EXT_DATA_SIGNATURE;
    }
    HddPasswordTablePtr->DevicePathSize = (UINT16)GetDevicePathSize(DevicePath);
    TempPtr = (VOID*)((UINTN)TcgExtDataPtr + GetExtDataSize ());
    CopyMem (TempPtr, DevicePath, HddPasswordTablePtr->DevicePathSize);
  }

  *TableSize = NewHddPasswordTableSize;
  return (VOID *)HddPasswordTable;
}




/**
  Check if the HDD Password table is out of date

  @param[in]        NumOfHdd     The number of HDDs.
  @param[in]        TableSize    The size of table allocated.
  @param[in]        HddPasswordTableSize  The size of HddPassword table 
  @param[in]        HddInfoArray          Hdd password info pointer 

  @retval           TRUE                This Hddpassword is valid.
  @retval           FALSE               This Hddpassword is not valid.
**/
BOOLEAN
IsTableOutOfDate (
  IN     UINTN                          NumOfHdd,
  IN     HDD_PASSWORD_TABLE             *HddPasswordTable,
  IN     UINTN                          HddPasswordTableSize,
  IN     HDD_PASSWORD_HDD_INFO          *HddInfoArray
  )
{
  UINTN                                 OldHddPasswordTableSize;
  HDD_PASSWORD_TABLE                    *HddPasswordTablePtr;
  UINTN                                 Index;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePathFromHddInfoArray;  
  EFI_DEVICE_PATH_PROTOCOL              *DevicePathFromHddPasswordTable;    
  TCG_OPAL_EXT_DATA                     *TcgExtDataPtr;
  EFI_STATUS                            Status;
  
  HddPasswordTablePtr = NULL;
  OldHddPasswordTableSize = 0;
  for (Index = 0; Index < NumOfHdd; Index++) {
    //
    // Start with second table
    //
    Status = gBS->HandleProtocol (
                    HddInfoArray[Index].DeviceHandleInDxe,
                    &gVmdPhySataDevicePathGuid,
                    (VOID *) &DevicePathFromHddInfoArray
                    );
    if (EFI_ERROR(Status)) {
      Status = gBS->HandleProtocol (
                      HddInfoArray[Index].DeviceHandleInDxe,
                      &gEfiDevicePathProtocolGuid,
                      (VOID *) &DevicePathFromHddInfoArray
                      );
    }
    
    HddPasswordTablePtr = GetTableEntryWithIndex(HddPasswordTable, Index);
    if (HddPasswordTablePtr ==  NULL) {
      return FALSE;
    }
    TcgExtDataPtr = (TCG_OPAL_EXT_DATA *)(HddPasswordTablePtr + 1);
    DevicePathFromHddPasswordTable  = (EFI_DEVICE_PATH_PROTOCOL*)((UINTN)TcgExtDataPtr + GetExtDataSize ());
    //
    // If found any devicepath from HddpasswordTable that is different from devicepath in HddInfoArray, should re-build HddPasswordTable
    //
    if (CompareMem(DevicePathFromHddInfoArray, DevicePathFromHddPasswordTable, HddPasswordTablePtr->DevicePathSize)) {
      return FALSE;
    }
    OldHddPasswordTableSize += HddPasswordTablePtr->DevicePathSize;
    OldHddPasswordTableSize += GetExtDataSize ();
    OldHddPasswordTableSize += sizeof(HDD_PASSWORD_TABLE);
  }
  
  if (OldHddPasswordTableSize != HddPasswordTableSize) {
    return FALSE;
  }
  return TRUE;
}

/**
  To show the hdd check dialog.

  @param[in]        HddPasswordProtocol EFI_HDD_PASSWORD_SERVICE_PROTOCOL instance.

  @retval           EFI_SUCCESS

**/
EFI_STATUS
HddUnlockDialog (
  IN EFI_HDD_PASSWORD_DIALOG_PROTOCOL   *HddPasswordDialog
  )
{
  UINTN                                 Index;
  UINT8                                 *ErrorCount;
  UINTN                                 NumOfHdd;
  UINT8                                 SkipCounter;
  EFI_STATUS                            Status;
  H2O_DIALOG_PROTOCOL                   *H2oDialogProtocol;
  ITEM_INFO_IN_DIALOG                   *HddDialogItemInfoString;
  CHAR16                                **HddDialogItemInfoStringPtr;
  CHAR16                                **StrTokenArray;
  EFI_INPUT_KEY                         Key;
  EFI_INPUT_KEY                         KeyList[] = {{SCAN_UP,   CHAR_NULL},
                                                     {SCAN_DOWN, CHAR_NULL},
//_Start_L05_HDD_PASSWORD_
//                                                     {SCAN_ESC,  CHAR_NULL},
//_End_L05_HDD_PASSWORD_
                                                     {SCAN_NULL, CHAR_CARRIAGE_RETURN},
                                                     {SCAN_NULL, CHAR_NULL}
                                                     };
  HDD_PASSWORD_TABLE                    *HddPasswordTable;
  UINTN                                 HddPasswordTableSize;
  HDD_PASSWORD_HDD_INFO                 *HddInfoArray;
  HDD_PASSWORD_POST_DIALOG_PRIVATE      *HddPasswordPostDialogPrivate;
  EFI_HDD_PASSWORD_SERVICE_PROTOCOL     *HddPasswordService;
  PASSWORD_INFORMATION                  PasswordInfo;
  HDD_PASSWORD_TABLE                    *HddPasswordTablePtr;
  UINTN                                 Count;
  UINTN                                 OrgMode;
  UINT32                                 HddPasswordCount;
//_Start_L05_HDD_PASSWORD_
  UINTN                                 *L05HddIndexMapTable;
  UINTN                                 L05HddIndex;
  UINTN                                 L05NumOfHdd;
//_End_L05_HDD_PASSWORD_

  StrTokenArray = NULL;
  SkipCounter  = 0;

  HddInfoArray = NULL;
  NumOfHdd = 0;
  HddPasswordTable = NULL;

  ErrorCount = NULL;
  HddPasswordTableSize = 0;
  HddDialogItemInfoString = NULL;
    
//_Start_L05_HDD_PASSWORD_
  L05HddIndexMapTable = NULL;
  L05HddIndex = 0;
  L05NumOfHdd = 0;
//_End_L05_HDD_PASSWORD_

  HddPasswordPostDialogPrivate = GET_PRIVATE_FROM_HDD_PASSWORD_POST_DIALOG (HddPasswordDialog);

  HddPasswordService = HddPasswordPostDialogPrivate->HddPasswordService;
  H2oDialogProtocol = HddPasswordPostDialogPrivate->H2oDialogProtocol;

  Status = HddPasswordService->GetHddInfo (
                                 HddPasswordService,
                                 &HddInfoArray,
                                 &NumOfHdd
                                 );

  if (NumOfHdd == 0) {
    Status = EFI_SUCCESS;
    goto EndOfDialog;
  }

  Status = HddPasswordDialog->ResetAllSecurityStatus (
                                HddPasswordDialog,
                                HddInfoArray,
                                NumOfHdd
                                );

  if (Status == EFI_SUCCESS) {
    //
    //  No HDD having to be unlock
    //

    goto EndOfDialog;
  } else if (Status != EFI_NOT_READY) {
    goto EndOfDialog;
  }
 
  HddPasswordCount = (UINT32)NumOfHdd;
  if (FeaturePcdGet (PcdH2OBaseCpHddPasswordGetTableSupported)) {  
    Status = TriggerCpHddPasswordGetTable(&HddPasswordTable, &HddPasswordCount);
    if (Status == EFI_WARN_STALE_DATA) {
      if (FeaturePcdGet (PcdH2OBaseCpHddPasswordSetTableSupported) ) {
        TriggerCpHddPasswordSetTable(HddPasswordTable, HddPasswordCount); 
      }
    } 
  }
  
  if (Status == EFI_SUCCESS) {
    //
    // May get old table
    //
    HddPasswordTableSize = GetHddPasswordTableSize(HddPasswordCount, HddPasswordTable);
    if (!IsTableOutOfDate (NumOfHdd, HddPasswordTable, HddPasswordTableSize, HddInfoArray)) {
      Status = EFI_NOT_FOUND;
      FreePool (HddPasswordTable);
    } else {
      ZeroMemByPolicy (HddPasswordTable, HddPasswordTableSize);
    }
  }

  if (Status != EFI_SUCCESS) {
    //
    // Create new table
    //
    HddPasswordTable = CreateHddPasswordTable (NumOfHdd, HddInfoArray ,&HddPasswordTableSize);
    if (HddPasswordTable == NULL) {
      goto EndOfDialog;
    }
  }

//_Start_L05_HDD_PASSWORD_
  //
  // eMMC does not support all functions of HDD password.
  // Map HDD index to skip eMMC.
  //
  L05HddIndexMapTable = AllocateZeroPool (NumOfHdd * sizeof(UINTN));
  for (Index = 0; Index < NumOfHdd; Index++) {
    if (HddInfoArray[Index].ControllerMode != eMMC_MODE) {
      L05HddIndexMapTable[L05HddIndex] = Index;
      L05HddIndex++;
    }
  }

  L05NumOfHdd = L05HddIndex;
//_End_L05_HDD_PASSWORD_

  HddDialogItemInfoString    = AllocatePool (NumOfHdd * sizeof(ITEM_INFO_IN_DIALOG));
  if (HddDialogItemInfoString == NULL) {
    goto EndOfDialog;
  }
  HddDialogItemInfoStringPtr = AllocatePool ( (NumOfHdd + 1) * sizeof(CHAR16 *));
  if (HddDialogItemInfoStringPtr == NULL) {
    goto EndOfDialog;
  }

//_Start_L05_HDD_PASSWORD_
  L05HddIndex = 0;
//_End_L05_HDD_PASSWORD_

  for (Index = 0; Index < NumOfHdd; Index++) {
//_Start_L05_HDD_PASSWORD_
//    HddDialogItemInfoStringPtr[Index] = (CHAR16 *)&(HddDialogItemInfoString[Index].ItemInfoString);
    //
    // Hidden eMMC at Harddisk Security.
    //
    if (HddInfoArray[Index].ControllerMode == eMMC_MODE) {
      continue;
    }

    HddDialogItemInfoStringPtr[L05HddIndex] = (CHAR16 *)&(HddDialogItemInfoString[Index].ItemInfoString);
    L05HddIndex++;
//_End_L05_HDD_PASSWORD_
  }

//_Start_L05_HDD_PASSWORD_
//  HddDialogItemInfoStringPtr[Index] = NULL;
  HddDialogItemInfoStringPtr[L05HddIndex] = NULL;
//_End_L05_HDD_PASSWORD_

  Key.ScanCode    = SCAN_NULL;
  Key.UnicodeChar = CHAR_NULL;

  ErrorCount = (UINT8 *)AllocateZeroPool (NumOfHdd * sizeof(UINT8));

  if (ErrorCount == NULL) {
    goto EndOfDialog;
  }

  Index = 0;
//_Start_L05_HDD_PASSWORD_
  L05HddIndex = 0;
//_End_L05_HDD_PASSWORD_

  HddPasswordDialog->GetStringTokenArray (
                       HddPasswordDialog,
                       (VOID *)&StrTokenArray
                       );

  OrgMode = (UINTN) gST->ConOut->Mode->Mode;
  DisableQuietBoot ();

  gST->ConOut->ClearScreen (gST->ConOut);

//_Start_L05_HDD_PASSWORD_ENABLE_
{
  EFI_L05_HOT_KEY_SERVICE_PROTOCOL      *L05HotKeyServicePtr;
  BOOLEAN                               IntoScu;
  UINT16                                FunctionKey;

  L05HotKeyServicePtr = NULL;
  IntoScu             = FALSE;
  //
  //  For L05 password sequence, we must check System password before HDD password.
  //  But we should ask system password only when power on password enable on boot into SCU.
  //  Thus we need to wait here to be sure if user is boot to SCU.
  //
  Status = gBS->LocateProtocol (&gEfiL05HotKeyServiceProtocolGuid, NULL, &L05HotKeyServicePtr);
  if (!EFI_ERROR (Status)) {
    //
    //  Wait for user input
    //
    gBS->Stall(1000000);

    Status = L05HotKeyServicePtr->StopHotKey ();
    Status = L05HotKeyServicePtr->GetHotKey (&FunctionKey);

    if (!EFI_ERROR (Status) && (FunctionKey == L05_SETUP_HOT_KEY)) {
      IntoScu = TRUE;
    }
    
    //
    //  Clear the HotKey Message
    //
    gST->ConOut->ClearScreen (gST->ConOut);
  }

  //
  // For OS Indicate boot to SCU
  //
  if (DoesOsIndicateBootToFwUI ()) {
    IntoScu = TRUE;
    gST->ConOut->ClearScreen (gST->ConOut);
  }
  
  L05CheckSystemPassword (IntoScu);
}
//_End_L05_HDD_PASSWORD_ENABLE_
#ifdef LCFC_SUPPORT
    Status = LfcCleanUpKbcAndEcBuffer ();
#endif

  while (HddPasswordDialog->CheckHddLock (HddPasswordDialog, HddInfoArray, NumOfHdd)) {
//_Start_L05_HDD_PASSWORD_ENABLE_
    //
    // Hook HDD Index to skip eMMC.
    //
    Index = L05HddIndex;
//_End_L05_HDD_PASSWORD_

    gST->ConOut->SetCursorPosition (
                   gST->ConOut,
                   0,
                   0
                   );

//_Start_L05_HDD_PASSWORD_ENABLE_
//    Print (L"%s", StrTokenArray[STR_HDD_ESC_SKIP_MSG_INDEX]);
//_End_L05_HDD_PASSWORD_ENABLE_

    HddPasswordDialog->PrepareHddDialogItems (
                         HddPasswordDialog,
                         HddInfoArray,
                         NumOfHdd,
                         HddDialogItemInfoString
                         );

    H2oDialogProtocol->OneOfOptionDialog (
//_Start_L05_HDD_PASSWORD_ENABLE_
//                         (UINT32)NumOfHdd,
                         //
                         // Hook HDD number to skip eMMC.
                         //
                         (UINT32)L05NumOfHdd,
//_End_L05_HDD_PASSWORD_
                         TRUE,
                         KeyList,
                         &Key,
                         OPTION_ICON_MAX_STR_SIZE,
                         StrTokenArray[STR_HDD_TITLE_MSG_INDEX],
                         (UINT32 *)&Index,
                         HddDialogItemInfoStringPtr,
                         0
                         );

    Status = HddPasswordDialog->CheckSkipDialogKey (
                                  HddPasswordDialog,
                                  (VOID *)&Key
                                  );

    if (Status != EFI_SUCCESS && (Key.UnicodeChar != CHAR_CARRIAGE_RETURN)) {
      switch (Key.ScanCode) {
      case SCAN_UP:
           if (Index == 0) {
             Index = NumOfHdd - 1;
           } else {
             Index--;
           }
           break;
      case SCAN_DOWN:
           if (Index == (UINTN) (NumOfHdd - 1)) {
             Index = 0;
           } else {
             Index++;
           }
           break;
      default:
           break;
      }
      continue;
    }
    if (Status == EFI_SUCCESS)  {
      //
      //  Skip POST unlock dialog Hot Key is pressed and skip unlock behavior
      //
      break;
    }

//_Start_L05_HDD_PASSWORD_ENABLE_
    //
    // Hook HDD Index to skip eMMC.
    //
    L05HddIndex = Index;
    Index = L05HddIndexMapTable[Index];
//_End_L05_HDD_PASSWORD_ENABLE_

    ZeroMem (&PasswordInfo, sizeof(PASSWORD_INFORMATION));
    Status = UnlockSelectedHdd (
               HddPasswordDialog,
               &HddInfoArray[Index],
               &PasswordInfo
               );

    if ((Status == EFI_SUCCESS) && (StrLen(PasswordInfo.PasswordStr) > 0)) {
      //
      // Find corresponding HddPasswordTable according to Index
      //
      HddPasswordTablePtr = (HDD_PASSWORD_TABLE *)HddPasswordTable;
      for (Count = 0; Count < Index; Count++) {
        HddPasswordTablePtr = GetNextTableEntry (HddPasswordTablePtr);
      }

      CopyMem (
        HddPasswordTablePtr->PasswordStr,
        PasswordInfo.PasswordStr,
        StrSize (PasswordInfo.PasswordStr)
        );
      HddPasswordTablePtr->PasswordType     = PasswordInfo.PasswordType;
      HddPasswordTablePtr->ControllerNumber = HddInfoArray[Index].ControllerNumber;
      HddPasswordTablePtr->PortNumber       = HddInfoArray[Index].PortNumber;
      HddPasswordTablePtr->PortMulNumber    = HddInfoArray[Index].PortMulNumber;
      UpdateExtData (HddPasswordTablePtr, HddPasswordTablePtr->PasswordType);
      //
      // zeroing the PWD content in memory after it's being used
      //
      if (HddInfoArray->StorageTcgSecuritySupported) {
        ZeroMem (&PasswordInfo, sizeof(PASSWORD_INFORMATION));
      }

    } else if (Status == EFI_DEVICE_ERROR) {
      ErrorCount[Index]++;
      if (ErrorCount[Index] == PcdGet8(PcdH2OHddPasswordMaxCheckPasswordCount)) {
        ShowErrorCountExpiredMessageAndSystemStop (HddPasswordDialog);

//_Start_L05_HDD_PASSWORD_ENABLE_
      } else {
        //
        // error count < 3
        //
#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
        //
        // [Lenovo Notebook Password Design Spec V1.2]
        //   4.1.1. Check HDP
        //     Figure 4-3 Password error
        //
        PcdSetBoolS (PcdL05SetupWarningDialogFlag, TRUE);
        PcdSetBoolS (PcdL05SetupAlignmentCenterFlag, TRUE);
#endif

        H2oDialogProtocol->ConfirmDialog (
                             L05WarningDlgContinueDlg,
                             FALSE,
                             40,
                             NULL,
                             &Key,
#ifndef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
                             L"Invalid password"
#else
                             StrTokenArray[L05_STR_HDD_DIALOG_PASSWORD_ERROR_MSG_INDEX]
#endif
                             );
//_End_L05_HDD_PASSWORD_ENABLE_

      }
    }

  }

  if (OrgMode != (UINTN) gST->ConOut->Mode->Mode) {
    gST->ConOut->SetMode (gST->ConOut, OrgMode);
  }

  //
  // Get updated count
  //
  HddPasswordCount = NumOfHddPasswordTable(HddPasswordTable, HddPasswordTableSize);
  if (FeaturePcdGet (PcdH2OBaseCpHddPasswordSetTableSupported) ) {
    Status = TriggerCpHddPasswordSetTable(HddPasswordTable, HddPasswordCount); 
#ifdef L05_NOTEBOOK_PASSWORD_ENABLE
    //
    // [Lenovo Notebook Password Design Spec V1.0]
    //   2 Hard Disk Password (HDP)
    //
    L05SetEncodeHddPasswordTable (HddPasswordService, HddPasswordTable, HddPasswordCount);
#endif
  }

  FreePool (HddDialogItemInfoStringPtr);

  //
  // zeroing the PWD content in memory after it's being used
  //
  if (HddInfoArray->StorageTcgSecuritySupported) {
    ZeroMem (HddPasswordTable,HddPasswordTableSize);
  }


EndOfDialog:

  if (HddPasswordTable != NULL) {
    FreePool (HddPasswordTable);
  }

  if (HddInfoArray != NULL) {
    FreePool (HddInfoArray);
  }

  if (ErrorCount != NULL) {
    FreePool (ErrorCount);
  }

  if (HddDialogItemInfoString != NULL) {
    FreePool (HddDialogItemInfoString);
  }
  
//_Start_L05_HDD_PASSWORD_ENABLE_
  if (L05HddIndexMapTable != NULL) {
    FreePool (L05HddIndexMapTable);
  }
//_End_L05_HDD_PASSWORD_ENABLE_

  if (Status != EFI_SUCCESS) {
    return Status;
  }

  return EFI_SUCCESS;

}

EFI_STATUS
HddPasswordUnlocked (
  IN EFI_HDD_PASSWORD_DIALOG_PROTOCOL   *This
  )
{
  HDD_PASSWORD_POST_DIALOG_PRIVATE *HddPasswordPostDialogPrivate;

  HddPasswordPostDialogPrivate = GET_PRIVATE_FROM_HDD_PASSWORD_POST_DIALOG (This);

  HddPasswordPostDialogPrivate->PostUnlocked = TRUE;

  return EFI_SUCCESS;
}

EFI_STATUS
HddUnlockDialogInit (
  IN EFI_HDD_PASSWORD_SERVICE_PROTOCOL  *HddPasswordService
  )
{
  EFI_STATUS                            Status;
  EFI_HANDLE                            Handle;
  VOID                                  *HobList;
  BOOLEAN                               UnlockHddByDialogFlag;
  HDD_PASSWORD_PRIVATE                  *HddPasswordPrivate;
  H2O_DIALOG_PROTOCOL                   *H2oDialogProtocol;

  Handle = NULL;
  //
  // Get Hob list to check boot mode
  //
  HobList           = NULL;
  UnlockHddByDialogFlag = TRUE;



  mHddPasswordPostDialogPrivate = (HDD_PASSWORD_POST_DIALOG_PRIVATE *)AllocateZeroPool (sizeof (HDD_PASSWORD_POST_DIALOG_PRIVATE));
  if (mHddPasswordPostDialogPrivate == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  mHddPasswordPostDialogPrivate->Signature = HDD_PASSWORD_POST_DIALOG_SIGNATURE;



  Status = gBS->LocateProtocol (
                  &gH2ODialogProtocolGuid,
                  NULL,
                  (VOID **)&H2oDialogProtocol
                  );
  if (Status != EFI_SUCCESS) {
    return Status;
  }



  mHddPasswordPostDialogPrivate->H2oDialogProtocol = H2oDialogProtocol;
  mHddPasswordPostDialogPrivate->HddPasswordService = HddPasswordService;

  HddPasswordPrivate = GET_PRIVATE_FROM_HDD_PASSWORD (HddPasswordService);


  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.GetStringTokenArray           = GetStringTokenArray;
  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.CheckSkipDialogKey            = CheckSkipDialogKey;
  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.ResetAllSecurityStatus        = ResetAllSecurityStatus;
  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.CheckHddLock                  = CheckHddLock;
  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.CheckHddSecurityEnable        = CheckHddSecurityEnable;
  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.PrepareHddDialogItems         = PrepareHddDialogItems;
  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.GetModelNumber                = GetHddModelNumber;
  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.UnlockSelectedHdd             = UnlockSelectedHdd;
  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.SelectedHddPrepareTitleString = SelectedHddPrepareTitleString;
  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.IsAdmiSecureBootReset         = IsAdmiSecureBootReset;
  mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol.HddPasswordUnlocked           = HddPasswordUnlocked;

  //
  //  Init String for HDD Password Dialog
  //
  InitDialogStringTokenArray ();

  //
  // Install HddPasswordDialogProtocol
  //
  Status = gBS->InstallProtocolInterface (
                  &Handle,
                  &gEfiHddPasswordDialogProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  &(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol)
                  );
  if (Status != EFI_SUCCESS) {
    return Status;
  }

  if (mHddPasswordPostDialogPrivate->PostUnlocked != TRUE) {

//_Start_L05_HDD_PASSWORD_ENABLE_
//    Status = EfiGetSystemConfigurationTable (&gEfiHobListGuid, &HobList);
//    if (Status == EFI_SUCCESS) {
{
    BOOLEAN                             IsLegacyToEfi;
    UINTN                               VarSize;

    IsLegacyToEfi         = FALSE;
    VarSize               = 0;

    VarSize = sizeof (IsLegacyToEfi);
    Status = gRT->GetVariable (
                    L"IsLegacyToEfi",
                    &gEfiGenericVariableGuid,
                    NULL,
                    &VarSize,
                    &IsLegacyToEfi
                    );

    if (EFI_ERROR (Status)) {
      IsLegacyToEfi = FALSE;
    }

//      if (GetBootModeHob () == BOOT_ON_S4_RESUME || IsAdmiSecureBootReset (&(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol))) {
      if (IsLegacyToEfi) {

        HddPasswordService->UnlockAllHdd (
          HddPasswordService
          );

        UnlockHddByDialogFlag = FALSE;
      }
//    }
}
//_End_L05_HDD_PASSWORD_ENABLE_
//[-start-220125-BAIN000092-modify]//
#ifdef LCFC_SUPPORT
#ifdef L05_NATURAL_FILE_GUARD_ENABLE
{
    HDD_PASSWORD_HDD_INFO               *HddInfoArray;
    UINTN                               NumOfHdd;
    UINT8                               UserOrMaster;

    HddInfoArray = NULL;
    NumOfHdd     = 0;
    UserOrMaster = 0xFF;

    Status = HddPasswordService->GetHddInfo (
                                   HddPasswordService,
                                   &HddInfoArray,
                                   &NumOfHdd
                                   );

    //
    // [Natural File Guard Design Guide V1.01]
    //   2.2.1 Secure Key Generation
    //     Procedure:
    //       2. On the Ready to boot event, BIOS should issue command to EC to lock the secure key data area in
    //          the EC and prevent Read/Write access.
    //
    Status = ReadyToBootLockSecureKey ();

    //
    // Disable Natural File Guard.
    // 1. HDD not have HDP.
    // 2. Platform not install HDD.
    // 3. The system has more than 1 HDD with Notebook Password Design Spec v1.1.
    //
    if (IsNaturalFileGuardEnable () &&
        (NaturalFileGuardCheckHddSecurityDisable (HddInfoArray, NumOfHdd) ||
#ifndef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
         (NumOfHdd == 0))) {
#else
         (NumOfHdd != 1))) {
#endif
      ClearStoredUhdp ();
      DisableNaturalFileGuard ();
    }

    //
    // [Natural File Guard Design Guide V1.01]
    //   2.3 Boot Flow
    //
    if (IsNaturalFileGuardEnable () && (NumOfHdd != 0)) {
      UnlockHddByDialogFlag = FALSE;
      Status = EFI_NOT_FOUND;

      NaturalFileGuardResetAllSecurityStatus (
        HddPasswordService,
        &(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol)
        );

      if (IsFoundStoredUhdp ()) {
        Status = NaturalFileGuardUnlockStoredUhdp (HddPasswordService);

        if (EFI_ERROR (Status)) {
          ClearStoredUhdp ();
        }
      }

      if (EFI_ERROR (Status)) {
        Status = NaturalFileGuardUnlockAuthenticate (
                   HddPasswordService,
                   &(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol),
                   FALSE,
                   &UserOrMaster
                   );
      }

      if (!EFI_ERROR (Status) && UserOrMaster == MASTER_PSW) {
        Status = NaturalFileGuardUnlockAuthenticate (
                   HddPasswordService,
                   &(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol),
                   TRUE,
                   &UserOrMaster
                   );
      }

      if (EFI_ERROR (Status)) {
        DisableNaturalFileGuard ();
        ShowUhdpInvalidMessage (&(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol));
      }
    }
}
#endif

#else

#ifdef L05_NATURAL_FILE_GUARD_ENABLE
{
    HDD_PASSWORD_HDD_INFO               *HddInfoArray;
    UINTN                               NumOfHdd;
    UINT8                               PasswordType;

    HddInfoArray = NULL;
    NumOfHdd     = 0;
    PasswordType = 0xFF;

    Status = HddPasswordService->GetHddInfo (
                                   HddPasswordService,
                                   &HddInfoArray,
                                   &NumOfHdd
                                   );

    //
    // [Natural File Guard Design Guide V1.01]
    //   2.2.1 Secure Key Generation
    //     Procedure:
    //       2. On the Ready to boot event, BIOS should issue command to EC to lock the secure key data area in
    //          the EC and prevent Read/Write access.
    //
    Status = ReadyToBootLockSecureKey ();

    //
    // Disable Natural File Guard.
    // 1. HDD not have HDP.
    // 2. Platform not install HDD.
    // 3. The system has more than 1 HDD with Notebook Password Design Spec v1.1.
    //
    if (IsNaturalFileGuardEnable () &&
        (NaturalFileGuardCheckHddSecurityDisable (HddInfoArray, NumOfHdd) ||
#ifndef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
         (NumOfHdd == 0)
#else
         (NumOfHdd != 1)
#endif
        )) {
      ClearStoredUhdp ();
      DisableNaturalFileGuard ();
    }

    //
    // [Natural File Guard Design Guide V1.01]
    //   2.3 Boot Flow
    //
    if (IsNaturalFileGuardEnable () && (NumOfHdd != 0)) {
      UnlockHddByDialogFlag = FALSE;
      Status = EFI_NOT_FOUND;

      NaturalFileGuardResetAllSecurityStatus (
        HddPasswordService,
        &(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol)
        );

      if (IsFoundStoredUhdp ()) {
        Status = NaturalFileGuardUnlockStoredUhdp (HddPasswordService);

        if (EFI_ERROR (Status)) {
          ClearStoredUhdp ();
        }
      }

      if (EFI_ERROR (Status)) {
        Status = NaturalFileGuardUnlockAuthenticate (
                   HddPasswordService,
                   &(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol),
                   FALSE,
                   &PasswordType
                   );
      }

      if (!EFI_ERROR (Status) && PasswordType == MASTER_PSW) {
        Status = NaturalFileGuardUnlockAuthenticate (
                   HddPasswordService,
                   &(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol),
                   TRUE,
                   &PasswordType
                   );
      }

      if (EFI_ERROR (Status)) {
        DisableNaturalFileGuard ();
        ShowUhdpInvalidMessage (&(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol));
      }
    }
}
#endif
#endif
//[-end-220125-BAIN000092-modify]//

    if (UnlockHddByDialogFlag) {
      HddUnlockDialog (
        &(mHddPasswordPostDialogPrivate->HddPasswordDialogProtocol)
        );
    }
  }


  //
  //  Release String for HDD Password Dialog
  //
  ReleaseStringTokenArray ();

  return EFI_SUCCESS;

}

