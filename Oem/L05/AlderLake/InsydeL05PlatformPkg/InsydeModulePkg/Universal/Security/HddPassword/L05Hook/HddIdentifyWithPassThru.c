/** @file
  Hdd Identify with PassThru Protocol.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "HddPassword.h"
#include <IndustryStandard/Atapi.h>
#include <Protocol/NvmExpressPassthru.h>
#include <Protocol/AtaPassThru.h>

#define NVME_CONTROLLER_ID              0
#define NVME_ALL_NAMESPACES             0xFFFFFFFF
#define NVME_GENERIC_TIMEOUT            EFI_TIMER_PERIOD_SECONDS (5)
#define HDD_DEFAULT_HEAD_VALUE          0xE0

/**
  Get identify controller data.

  @param  NvmePassThru                  A pointer to the NvmePassThru protocol instance.
  @param  NamespaceId                   NamespaceId for an NVM Express namespace present on the NVM Express controller.
  @param  Buffer                        The buffer used to store the identify controller data.

  @return EFI_SUCCESS                   Successfully get the identify controller data.
  @return EFI_DEVICE_ERROR              Fail to get the identify controller data.
**/
EFI_STATUS
NvmeIdentifyController (
  IN     EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL *NvmePassThru,
  IN     UINT32                             NamespaceId,
  IN OUT VOID                               *Buffer
  )
{
  EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET  CommandPacket;
  EFI_NVM_EXPRESS_COMMAND                   Command;
  EFI_NVM_EXPRESS_COMPLETION                Completion;
  EFI_STATUS                                Status;

  ZeroMem (&CommandPacket, sizeof(EFI_NVM_EXPRESS_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Command, sizeof(EFI_NVM_EXPRESS_COMMAND));
  ZeroMem (&Completion, sizeof(EFI_NVM_EXPRESS_COMPLETION));

  Command.Cdw0.Opcode = NVME_ADMIN_IDENTIFY_CMD;
  //
  // According to Nvm Express 1.1 spec Figure 38, When not used, the field shall be cleared to 0h.
  // For the Identify command, the Namespace Identifier is only used for the Namespace data structure.
  //
  Command.Nsid        = NamespaceId;

  CommandPacket.NvmeCmd        = &Command;
  CommandPacket.NvmeCompletion = &Completion;
  CommandPacket.TransferBuffer = Buffer;
  CommandPacket.TransferLength = sizeof (NVME_ADMIN_CONTROLLER_DATA);
  CommandPacket.CommandTimeout = NVME_GENERIC_TIMEOUT;
  CommandPacket.QueueType      = NVME_ADMIN_QUEUE;
  //
  // Set bit 0 (Cns bit) to 1 to identify a controller
  //
  Command.Cdw10                = 1;
  Command.Flags                = CDW10_VALID;

  Status = NvmePassThru->PassThru (
                           NvmePassThru,
                           NamespaceId,
                           &CommandPacket,
                           NULL
                           );

  return Status;
}

/**
  Get NVMe namespace ID and controller data.

  @param  NvmePassThru                  A pointer to the NvmePassThru protocol instance.
  @param  NamespaceId                   A pointer to the NVMe namespace ID.
  @param  NvmeAdminControllerData       A pointer to the NVMe admin controller data.

  @retval EFI_SUCCESS                   Get the correct namespace ID and admin controller data.
  @retval Other                         An unexpected error occurred.
**/
EFI_STATUS 
NvmeGetNamespaceIdAndControllerData (
  IN      EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL *NvmePassThru,
  IN OUT UINT32                              *NamespaceId,
  IN OUT NVME_ADMIN_CONTROLLER_DATA          *NvmeAdminControllerData
  )
{
  EFI_STATUS                            Status;

  *NamespaceId = NVME_ALL_NAMESPACES;

  Status = NvmePassThru->GetNextNamespace (NvmePassThru, NamespaceId);
  //
  // Due to the fact that RAID Driver expects other value of NamespaceId parameter than AHCI driver,
  // need to send the Identify Controller command twice - once with the NamespaceId retrieved
  // using GetNextNamespace. If it fails we need to set NamespaceId = 0.
  //
  if (!EFI_ERROR (Status)) {
    Status = NvmeIdentifyController (NvmePassThru, *NamespaceId, NvmeAdminControllerData);
    if (EFI_ERROR (Status)) {
      *NamespaceId = NVME_CONTROLLER_ID;
      Status = NvmeIdentifyController (NvmePassThru, *NamespaceId, NvmeAdminControllerData);
    }
  }

  return Status;
}

/**
  Get HDD device identify data.

  @param  AtaPassThru                   A pointer to the AtaPassThru protocol instance.
  @param  PortNumber                    Port of this device.
  @param  PortMulNumber                 PortMultiplierPort of this device.
  @param  IdentifyData                  A pointer to the IdentifyData buffer.

  @return EFI_SUCCESS                   Successfully get the identify data.
  @retval Other                         An unexpected error occurred.
**/
EFI_STATUS
AtaIdentifyCommand (
  IN     EFI_ATA_PASS_THRU_PROTOCOL     *AtaPassThru,
  IN     UINT16                         PortNumber,
  IN     UINT16                         PortMulNumber,
  IN OUT ATA_IDENTIFY_DATA              *IdentifyData
  )
{
  EFI_STATUS                            Status;
  EFI_ATA_PASS_THRU_COMMAND_PACKET      AtaPassThruCmdPacket;
  EFI_ATA_COMMAND_BLOCK                 Acb;
  EFI_ATA_STATUS_BLOCK                  Asb;

  ZeroMem (&AtaPassThruCmdPacket, sizeof (EFI_ATA_PASS_THRU_COMMAND_PACKET));
  ZeroMem (&Acb, sizeof (EFI_ATA_COMMAND_BLOCK));
  ZeroMem (&Asb, sizeof (EFI_ATA_STATUS_BLOCK));
  ZeroMem (IdentifyData, sizeof (ATA_IDENTIFY_DATA));

  AtaPassThruCmdPacket.Acb                = &Acb;
  AtaPassThruCmdPacket.Asb                = &Asb;
  //
  // Set "IDENTIFY DEVICE" command for ATA Pass Through
  //
  AtaPassThruCmdPacket.Acb->AtaCommand    = ATA_CMD_IDENTIFY_DRIVE;
  AtaPassThruCmdPacket.Acb->AtaDeviceHead = (UINT8) ((PortMulNumber << 4) | HDD_DEFAULT_HEAD_VALUE);
  AtaPassThruCmdPacket.InDataBuffer       = (VOID *) IdentifyData;
  AtaPassThruCmdPacket.InTransferLength   = sizeof (ATA_IDENTIFY_DATA);
  AtaPassThruCmdPacket.Protocol           = EFI_ATA_PASS_THRU_PROTOCOL_PIO_DATA_IN;
  AtaPassThruCmdPacket.Length             = EFI_ATA_PASS_THRU_LENGTH_BYTES;

  Status = AtaPassThru->PassThru (
                          AtaPassThru,
                          PortNumber,
                          PortMulNumber,
                          &AtaPassThruCmdPacket,
                          0
                          );

  return Status;
}

/**
  Return limited HDD device identify data.

  @param  ControllerHandle              Controller handle of the device.
  @param  DevicePath                    The device path of a SSCP device.
  @param  InstalledByInsydeDriver       A flag indicating whether device related protocol is installed by Insyde driver.
  @param  Buffer                        A pointer to the IdentifyData buffer.

  @return EFI_SUCCESS                   Successfully get the identify data.
  @retval Other                         An unexpected error occurred.
**/
EFI_STATUS
LimitedHddIdentifyWithPassThru (
  IN     EFI_HANDLE                     ControllerHandle,
  IN     EFI_DEVICE_PATH_PROTOCOL       *DevicePath,
  IN     BOOLEAN                        InstalledByInsydeDriver,
  IN OUT VOID                           *Buffer
  )
{
  EFI_STATUS                            Status;
  EFI_DEVICE_PATH_PROTOCOL              *TmpDevicePath;
  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL    *NvmePassThru;
  UINT32                                NamespaceId;
  NVME_ADMIN_CONTROLLER_DATA            *NvmeAdminControllerData;
  EFI_ATA_PASS_THRU_PROTOCOL            *AtaPassThru;
  UINT16                                PortNumber;
  UINT16                                PortMulNumber;
  ATA_IDENTIFY_DATA                     *TmpIdentifyData;
  ATA_IDENTIFY_DATA                     *IdentifyData;

  Status                  = EFI_UNSUPPORTED;
  TmpDevicePath           = DevicePath;
  NvmePassThru            = NULL;
  NamespaceId             = 0;
  NvmeAdminControllerData = NULL;
  AtaPassThru             = NULL;
  PortNumber              = 0;
  PortMulNumber           = 0;
  TmpIdentifyData         = NULL;
  IdentifyData            = (ATA_IDENTIFY_DATA *) Buffer;

  if (FindNvmeDevicePath (TmpDevicePath) != NULL) {
    //
    // Identify with NvmExpressPassThru
    //
    NvmeAdminControllerData = AllocateZeroPool (sizeof (NVME_ADMIN_CONTROLLER_DATA));

    if (NvmeAdminControllerData == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }

    Status = gBS->HandleProtocol (
                    ControllerHandle,
                    &gEfiNvmExpressPassThruProtocolGuid,
                    (VOID **) &NvmePassThru
                    );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    Status = NvmeGetNamespaceIdAndControllerData (NvmePassThru, &NamespaceId, NvmeAdminControllerData);

    if (EFI_ERROR (Status)) {
      FreePool (NvmeAdminControllerData);
      return Status;
    }

    CopyMem (IdentifyData->ModelName, NvmeAdminControllerData->Mn, sizeof (IdentifyData->ModelName));
    CopyMem (IdentifyData->SerialNo, NvmeAdminControllerData->Sn, sizeof (IdentifyData->SerialNo));

    //
    // Due to ModelName & SerialNo string returned by AlderLake VMD driver's NvmExpressPassThru is in Big-Endian format.
    // However, ModelName & SerialNo string returned by Insyde driver's NvmExpressPassThru is returned in ATA string format.
    // So need to do byte swap action for future conversion function.
    //
    L05SwapEntries (IdentifyData->ModelName, sizeof (IdentifyData->ModelName));
    L05SwapEntries (IdentifyData->SerialNo, sizeof (IdentifyData->SerialNo));

    FreePool (NvmeAdminControllerData);
  } else if (FindSataDevicePath (TmpDevicePath) != NULL) {
    //
    // Identify with AtaPassThru
    //
    TmpIdentifyData = AllocateZeroPool (sizeof (ATA_IDENTIFY_DATA));

    if (TmpIdentifyData == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }

    Status = gBS->HandleProtocol (
                    ControllerHandle,
                    &gEfiAtaPassThruProtocolGuid,
                    (VOID **) &AtaPassThru
                    );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    PortNumber    = FindSataDevicePortNumber (TmpDevicePath, InstalledByInsydeDriver);
    PortMulNumber = FindSataDevicePortMultiplierNumber (TmpDevicePath);

    Status = AtaIdentifyCommand (AtaPassThru, PortNumber, PortMulNumber, TmpIdentifyData);

    if (EFI_ERROR (Status)) {
      FreePool (TmpIdentifyData);
      return Status;
    }

    CopyMem (IdentifyData->ModelName, TmpIdentifyData->ModelName, sizeof (IdentifyData->ModelName));
    CopyMem (IdentifyData->SerialNo, TmpIdentifyData->SerialNo, sizeof (IdentifyData->SerialNo));

    FreePool (TmpIdentifyData);
  }

  return Status;
}