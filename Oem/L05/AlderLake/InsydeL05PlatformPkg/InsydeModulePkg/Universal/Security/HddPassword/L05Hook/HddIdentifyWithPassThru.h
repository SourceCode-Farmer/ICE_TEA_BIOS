/** @file
  Hdd Identify with PassThru Protocol.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _HDD_IDENTIFY_WITH_PASS_THRU_H_
#define _HDD_IDENTIFY_WITH_PASS_THRU_H_

EFI_STATUS
LimitedHddIdentifyWithPassThru (
  IN     EFI_HANDLE                     ControllerHandle,
  IN     EFI_DEVICE_PATH_PROTOCOL       *DevicePath,
  IN     BOOLEAN                        InstalledByInsydeDriver,
  IN OUT VOID                           *Buffer
  );

#endif
