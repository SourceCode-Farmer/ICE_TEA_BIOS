/** @file
  Function for Install L05 Before Ready To Boot Protocol

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _INSTALL_BEFORE_READY_TO_BOOT_PROTOCOL_H_
#define _INSTALL_BEFORE_READY_TO_BOOT_PROTOCOL_H_

VOID
InstallBeforeReadyToBootProtocol (
  );
#endif
