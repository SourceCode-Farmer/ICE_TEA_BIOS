/** @file
  Function for Install L05 Before Ready To Boot Protocol

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>

//
// Produced Protocols
//
#include <Protocol/L05BeforeReadyToBoot.h>

BOOLEAN                                 mBeInstalled = FALSE;

/**
  Install L05 Before Ready To Boot Protocol
**/
VOID
InstallBeforeReadyToBootProtocol (
  )
{
  EFI_STATUS                            Status;
  EFI_HANDLE                            Handle;

  Handle = NULL;

  if (mBeInstalled) {
    return;
  }

  Status = gBS->InstallProtocolInterface (
                  &Handle,
                  &gEfiL05BeforeReadyToBootProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  NULL
                  );

  if (!EFI_ERROR (Status)) {
    mBeInstalled = TRUE;
  }

  return;
}
