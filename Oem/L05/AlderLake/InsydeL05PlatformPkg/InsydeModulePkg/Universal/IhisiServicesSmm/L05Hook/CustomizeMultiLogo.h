/** @file
  Feature Hook for update customize multi-logo

;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CUSTOMIZE_MULTI_LOGO_H_
#define _CUSTOMIZE_MULTI_LOGO_H_

#ifdef L05_CUSTOMIZE_MULTI_LOGO_SUPPORT
EFI_STATUS
L05CheckCustomizeLogo (
  IN     AP_COMMUNICATION_DATA_TABLE    *ApCommDataBuffer,
  IN OUT BIOS_COMMUNICATION_DATA_TABLE  *BiosCommDataBuffer
  );

EFI_STATUS
L05WriteCustomizeLogo (
  );
#endif

EFI_STATUS
L05IhisiS41T04CustomizeMultiLogo (
  VOID
  );

EFI_STATUS
L05IhisiS42T04CustomizeMultiLogo (
  VOID
  );
#endif
