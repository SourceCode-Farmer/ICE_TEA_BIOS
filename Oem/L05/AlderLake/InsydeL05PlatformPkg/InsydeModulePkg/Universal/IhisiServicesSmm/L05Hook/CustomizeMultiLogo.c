/** @file
  Feature Hook for update customize multi-logo

;******************************************************************************
;* Copyright (c) 2014 - 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

#include <H2OIhisi.h>  // L05CustomizeLogoMultiSupport
#include <FlashRegionLayout.h>

#include <Library/BaseMemoryLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/BaseLib.h>  // StrnCmp, StrLen, StrStr, StrDecimalToUintn, StrSize
#include <Library/FlashRegionLib.h>

#include <Guid/L05CustomizeMultiLogo.h>

#include <Protocol/H2OIhisi.h>  // gH2OIhisiProtocolGuid
#include <Protocol/SmmFwBlockService.h>
#include <Protocol/SmmCpu.h>  // EFI_SMM_SAVE_STATE_REGISTER_RSI

STATIC UINT8                            mOemExtraDataType;

UINT32
EFIAPI
IhisiProtReadCpuReg32 (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum
  );

EFI_STATUS
EFIAPI
SetApandBiosCommDataBuffer (
  AP_COMMUNICATION_DATA_TABLE           *ApCommDataBuffer,
  BIOS_COMMUNICATION_DATA_TABLE         *BiosCommDataBuffer
  );

BOOLEAN
EFIAPI
IhisiProtBufferInCmdBuffer (
  IN  VOID                              *Buffer,
  IN  UINTN                             BufferSize
  );

#ifdef L05_CUSTOMIZE_MULTI_LOGO_SUPPORT
//
// The command definition of -edt4s:"" argument
//
typedef enum {
  AddNewLogo = 0,
  EraseAllLogo,
  JpegFormt1,
  JpegFormt2,
  TgaFormat,
  GifFormat,
  Delimiter,
  MaxType,
} L05_EDT4_PARAM_TYPE;

typedef struct {
  CHAR16                                *Command;
  L05_EDT4_PARAM_TYPE                   CommandType;
} L05_EDT4_PARAM_ITEM;

L05_EDT4_PARAM_ITEM                     mEdt4ParamList[] = {
  L"add:",  AddNewLogo,
  L"erase", EraseAllLogo,
  L"jpeg:", JpegFormt1,
  L"jpg:",  JpegFormt2,
  L"tga:",  TgaFormat,
  L"gif:",  GifFormat,
  L"x",     Delimiter,
  NULL,     MaxType
};

L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER  mCurrentLogoInfo = {0};
UINT8                                      *mCurrentFlashAddr = NULL;

/**
  Get Start Location for adding new logo

  @param  StartAddr                     Pointer to start address
  @param  TotalRegionSize               The available total space of Logo Region
  @param  InputDataSize                 The size of Logo which be added

  @retval EFI_SUCCESS                   Search the start address successful
  @retval EFI_OUT_OF_RESOURCES          No available space for new logo in Logo Region
**/
EFI_STATUS
GetStartLocation (
  IN OUT UINT8                          **StartAddr,
  IN  UINT32                            TotalRegionSize,
  IN  UINT32                            InputDataSize
  )
{
  EFI_STATUS                                 Status;
  L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER  *RegionHeader;
  UINT32                                     UsedSize;

  Status   = EFI_OUT_OF_RESOURCES;
  UsedSize = 0;

  //
  // Search the start address
  //
  do {

    RegionHeader = (L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER *) *StartAddr;

    if (CompareMem (&(RegionHeader->Guid), &gL05CustomizeMultiLogoGuid, sizeof (EFI_GUID)) == 0 &&
        RegionHeader->Signature == L05_CUSTOM_MULTI_LOGO_SIGNATURE &&
        RegionHeader->SupportedResolutionX != 0 &&
        RegionHeader->SupportedResolutionY != 0 &&
        RegionHeader->ImageSize != 0) {

      //
      // Move point to next Logo
      //
      *StartAddr += sizeof (L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER) - sizeof (UINT8) + RegionHeader->ImageSize;

      //
      // Record remaining size
      //
      UsedSize += sizeof (L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER) - sizeof (UINT8) + RegionHeader->ImageSize;

    } else {

      if (TotalRegionSize > (UsedSize + sizeof (L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER) - sizeof (UINT8) + InputDataSize)) {
        Status = EFI_SUCCESS;
      }

      break;
    }
  } while (TRUE);

  return Status;
}

/**
  Parsing input command from -edt4s:"" argument

  @param  Command                       Pointer to Input Command (Unicode)
  @param  CommandLength                 The length of input Command (Unicode)

  @retval EFI_SUCCESS                   Parsing successful
  @retval EFI_UNSUPPORTED               Input command is not legal
**/
EFI_STATUS
ParsingInputCommand (
  IN CHAR16                             *Command,
  IN UINT32                             CommandLength
  )
{
  EFI_STATUS                                 Status;
  UINT32                                     Index;
  L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER  LogoInfoBuf;
  CHAR16                                     *DelimiterPtr;
  UINTN                                      BufSize;
  CHAR16                                     *StringBuf;
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL          *SmmFwBlockService;
  UINTN                                      LoopCount;

  Status = EFI_UNSUPPORTED;
  ZeroMem (&LogoInfoBuf, sizeof (L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER));

  //
  // Lowercase all char.
  //
  for (Index = 0; Command[Index] != L'\0'; Index++) {
    if (Command[Index] >= L'A' && Command[Index] <= L'Z') {
      Command[Index] = Command[Index] + 0x20;
    }
  }

  //
  // Check Add command Type
  //
  if (StrnCmp (Command, mEdt4ParamList[AddNewLogo].Command, StrLen (mEdt4ParamList[AddNewLogo].Command)) == 0) {

    //
    // Get input Image Format
    //
    Command += StrLen (mEdt4ParamList[AddNewLogo].Command);

    if (StrnCmp (Command, mEdt4ParamList[JpegFormt1].Command, StrLen (mEdt4ParamList[JpegFormt1].Command)) == 0) {

      Command += StrLen (mEdt4ParamList[JpegFormt1].Command);
      LogoInfoBuf.ImageFormat = 0x01;  // EfiBadgingSupportFormatJPEG

    } else if (StrnCmp (Command, mEdt4ParamList[JpegFormt2].Command, StrLen (mEdt4ParamList[JpegFormt2].Command)) == 0) {

      Command += StrLen (mEdt4ParamList[JpegFormt2].Command);
      LogoInfoBuf.ImageFormat = 0x01;  // EfiBadgingSupportFormatJPEG

    } else if (StrnCmp (Command, mEdt4ParamList[TgaFormat].Command, StrLen (mEdt4ParamList[TgaFormat].Command)) == 0) {

      Command += StrLen (mEdt4ParamList[TgaFormat].Command);
      LogoInfoBuf.ImageFormat = 0x05;  // EfiBadgingSupportFormatTGA

    } else if (StrnCmp (Command, mEdt4ParamList[GifFormat].Command, StrLen (mEdt4ParamList[GifFormat].Command)) == 0) {

      Command += StrLen (mEdt4ParamList[GifFormat].Command);
      LogoInfoBuf.ImageFormat = 0x03;  // EfiBadgingSupportFormatGIF

    } else {

      return EFI_UNSUPPORTED;
    }

    //
    // Get input supported resolution X
    //
    DelimiterPtr = StrStr (Command, mEdt4ParamList[Delimiter].Command);

    if (DelimiterPtr == NULL) {
      return EFI_UNSUPPORTED;
    }

    BufSize = (UINTN) (VOID *) DelimiterPtr - (UINTN) (VOID *) Command;

    Status = gSmst->SmmAllocatePool (
                      EfiRuntimeServicesData,
                      BufSize,
                      (VOID **) &StringBuf
                      );

    if (EFI_ERROR (Status)) {
      return EFI_UNSUPPORTED;
    }

    CopyMem (StringBuf, Command, BufSize);
    LogoInfoBuf.SupportedResolutionX = (UINT32) StrDecimalToUintn (StringBuf);
    gSmst->SmmFreePool (StringBuf);

    //
    // Skip delimiter L"x" then get input supported resolution Y
    //
    Command = DelimiterPtr;
    Command += 1;

    BufSize = StrSize (DelimiterPtr);

    Status = gSmst->SmmAllocatePool (
                      EfiRuntimeServicesData,
                      BufSize,
                      (VOID **) &StringBuf
                      );

    if (EFI_ERROR (Status)) {
      return EFI_UNSUPPORTED;
    }

    CopyMem (StringBuf, Command, BufSize);
    LogoInfoBuf.SupportedResolutionY = (UINT32) StrDecimalToUintn (StringBuf);
    gSmst->SmmFreePool (StringBuf);

    //
    // Check the setting of resolution X, Y are valid
    //
    if (LogoInfoBuf.SupportedResolutionX == 0 ||
        LogoInfoBuf.SupportedResolutionX == 0) {

      return EFI_UNSUPPORTED;
    }

    //
    // Fill the temporary Logo Information Region
    //
    CopyMem (&mCurrentLogoInfo, &LogoInfoBuf, sizeof (L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER));

    CopyMem ((VOID *) &(mCurrentLogoInfo.Guid), (VOID *) &gL05CustomizeMultiLogoGuid, sizeof (EFI_GUID));
    mCurrentLogoInfo.Signature = L05_CUSTOM_MULTI_LOGO_SIGNATURE;

    return EFI_SUCCESS;
  }

  //
  // Check Erase command Type
  //
  if (StrnCmp (Command, mEdt4ParamList[EraseAllLogo].Command, StrLen (mEdt4ParamList[EraseAllLogo].Command)) == 0) {


    Status = gSmst->SmmLocateProtocol (
                      &gEfiSmmFwBlockServiceProtocolGuid,
                      NULL,
                      &SmmFwBlockService
                      );

    if (EFI_ERROR (Status)) {
      return EFI_UNSUPPORTED;
    }

    LoopCount = 0;

    while (LoopCount < 3) {

      BufSize = (UINTN) FdmGetNAtSize (&gL05H2OFlashMapRegionCustomizeMultiLogoGuid, 1);

      Status = SmmFwBlockService->EraseBlocks (
                                    SmmFwBlockService,
                                    (UINTN) FdmGetNAtAddr (&gL05H2OFlashMapRegionCustomizeMultiLogoGuid, 1),
                                    &BufSize
                                    );

      if (!EFI_ERROR (Status)) {

        break;

      } else {

        LoopCount++;
      }
    }

    return EFI_SUCCESS;
  }

  //
  // No match commands
  //
  return EFI_UNSUPPORTED;
}

/**
  Flash input data from start offset

  @param  StartAddr                     Pointer to start address
  @param  FlashData                     Pointer to Flash Data
  @param  FlashDataSize                 The size of data which be flashed

  @retval EFI_SUCCESS                   Flash successful
  @retval EFI_UNSUPPORTED               No Smm Fw Block Service Protocol could use or flash data fail
**/
EFI_STATUS
FlashIntoSpiRom (
  IN UINT8                              *StartAddr,
  IN UINT8                              *FlashData,
  IN UINT32                             FlashDataSize
  )
{
  EFI_STATUS                            Status;
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwBlockService;
  UINTN                                 LoopCount;
  UINTN                                 WriteSize;

  Status    = EFI_SUCCESS;
  WriteSize = 0;

  //
  // Locate Smm FW Block Service Protocol
  //
  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmFwBlockServiceProtocolGuid,
                    NULL,
                    &SmmFwBlockService
                    );

  if (EFI_ERROR (Status)) {

    return EFI_UNSUPPORTED;
  }

  //
  // Start to flash input data.
  //
  *(SmmFwBlockService->FlashMode) = SMM_FW_FLASH_MODE;

  LoopCount = 0;

  while (LoopCount < 3) {

    WriteSize = (UINTN) FlashDataSize;

    Status = SmmFwBlockService->Write (
                                  SmmFwBlockService,
                                  (UINTN) StartAddr,
                                  &WriteSize,
                                  FlashData
                                  );

    if (!EFI_ERROR (Status)) {

      break;

    } else {

      LoopCount++;
    }
  }

  *(SmmFwBlockService->FlashMode) = SMM_FW_DEFAULT_MODE;

  if (LoopCount == 3) {
    Status = EFI_UNSUPPORTED;
  }

  return Status;
}

/**
  AH=41h, User Define Communication type 04h to update customize logo.

  @param  ApCommDataBuffer              Pointer to AP communication data buffer.
  @param  BiosCommDataBuffer            Pointer to BIOS communication data buffer.

  @retval EFI_SUCCESS                   Check customize logo successful.
  @retval EFI_UNSUPPORTED               User Define Communication type is not 04h.
**/
EFI_STATUS
L05CheckCustomizeLogo (
  IN     AP_COMMUNICATION_DATA_TABLE    *ApCommDataBuffer,
  IN OUT BIOS_COMMUNICATION_DATA_TABLE  *BiosCommDataBuffer
  )
{
  EFI_STATUS                            Status;

  BiosCommDataBuffer->BlockSize = OemExtraMaximunBlockSize;
  BiosCommDataBuffer->ErrorReturn = 0;

  //
  // If the first -edt4s:"" argument is valid before, then get the start address
  //
  if (mCurrentLogoInfo.SupportedResolutionX != 0 &&
      mCurrentLogoInfo.SupportedResolutionX != 0) {

    mCurrentFlashAddr = (UINT8 *) (UINTN) FdmGetNAtAddr (&gL05H2OFlashMapRegionCustomizeMultiLogoGuid, 1);

    //
    // Get Start Location for adding new logo
    //
    Status = GetStartLocation (
               &mCurrentFlashAddr,
               (UINT32) FdmGetNAtSize (&gL05H2OFlashMapRegionCustomizeMultiLogoGuid, 1),
               ApCommDataBuffer->PhysicalDataSize
               );

    if (!EFI_ERROR (Status)) {

      mCurrentLogoInfo.ImageSize = ApCommDataBuffer->PhysicalDataSize;

    } else {

      ZeroMem ((VOID *) &mCurrentLogoInfo, sizeof (L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER));
      return EFI_UNSUPPORTED;
    }
  }

  return EFI_SUCCESS;
}

/**
  AH=42h, User Define Communication type 04h to write customize logo into PcdFlashFvL05CustomizeMultiLogoBase region.

  @retval EFI_SUCCESS                   Write customize logo successful.
  @retval EFI_UNSUPPORTED               Write customize logo fail.
**/
EFI_STATUS
L05WriteCustomizeLogo (
  )
{
  EFI_STATUS                            Status;
  H2O_IHISI_PROTOCOL                    *H2OIhisiPtr;
  UINT8                                 *InputDataBuffer;
  UINT32                                InputDataSize;
  UINT32                                InputDataOffset;
  UINT8                                 ApOperation;

  H2OIhisiPtr     = NULL;

  Status = gSmst->SmmLocateProtocol (
                    &gH2OIhisiProtocolGuid,
                    NULL,
                    (VOID **) &H2OIhisiPtr
                    );

  if (EFI_ERROR (Status)) {

    return EFI_UNSUPPORTED;
  }

  //
  // Get input buffer and size
  //
  InputDataBuffer = (UINT8 *) (UINTN) H2OIhisiPtr->ReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RSI);  // ESI
  InputDataSize   = H2OIhisiPtr->ReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RDI);  // EDI
  InputDataOffset = H2OIhisiPtr->ReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RCX);  // CX
  ApOperation     = (UINT8) InputDataOffset;  // CL
  InputDataOffset = (InputDataOffset >> 8) && 0xFF; //CH

  //
  // Overlap security check
  //
  if (H2OIhisiPtr->BufferOverlapSmram ((VOID *) InputDataBuffer, InputDataSize)) {

    return IHISI_BUFFER_RANGE_ERROR;
  }

  //
  // Check the first -edt4s:"" argument is valid or not
  //
  if (mCurrentLogoInfo.SupportedResolutionX == 0 ||
      mCurrentLogoInfo.SupportedResolutionX == 0) {

    //
    // BUGBUG: Input Data Size is not match Input Data Buffer for inputing ASCII string then H2OFFT record as Unicode string
    //
    Status = ParsingInputCommand ((CHAR16 *) InputDataBuffer, InputDataSize);

    return Status;
  }

  //
  // For second -edt4f:"" argument to flash logo into SPI Rom
  //
  if (InputDataOffset == 0x00) {

    //
    // Flash Custom logo header first
    //
    Status = FlashIntoSpiRom (
               mCurrentFlashAddr,
               (UINT8 *) &mCurrentLogoInfo,
               sizeof (L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER) - sizeof (UINT8)
               );

    if (!EFI_ERROR (Status)) {

      mCurrentFlashAddr += (sizeof (L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER) - sizeof (UINT8));

    } else {

      return Status;
    }
  }

  //
  // Flash Input logo data
  //
  Status = FlashIntoSpiRom (
             mCurrentFlashAddr,
             InputDataBuffer,
             InputDataSize
             );

  if (!EFI_ERROR (Status)) {

    mCurrentFlashAddr += InputDataSize;

  } else {

    return Status;
  }

  //
  // Check it is the last input logo data
  //
  if (ApOperation == DoNothing && InputDataSize <= 0x10000) {

    //
    // Clean Custom logo header for next command -edt4s:""
    //
    ZeroMem ((VOID *) &mCurrentLogoInfo, sizeof (L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER));
  }

  return EFI_SUCCESS;
}
#endif


/**
  AH=41h, OEM Extra Data Communication type.
  
  Data type 04h = L05CustomizeMultiLogo
  
  @retval EFI_SUCCESS                   Process OEM extra data communication successful.
  @return Other                         Process OEM extra data communication failed.
**/
EFI_STATUS
EFIAPI
L05IhisiS41T04CustomizeMultiLogo (
  VOID
  )
{
  EFI_STATUS                            Status;
  BIOS_COMMUNICATION_DATA_TABLE         *BiosCommDataBuffer;
  AP_COMMUNICATION_DATA_TABLE           *ApCommDataBuffer;

  ApCommDataBuffer = (AP_COMMUNICATION_DATA_TABLE *) (UINTN) IhisiProtReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RCX);

  Status = gSmst->SmmAllocatePool (
                    EfiRuntimeServicesData,
                    sizeof (BIOS_COMMUNICATION_DATA_TABLE),
                    (VOID **) &BiosCommDataBuffer
                    );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = SetApandBiosCommDataBuffer (ApCommDataBuffer, BiosCommDataBuffer);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  mOemExtraDataType = ApCommDataBuffer->DataType;

  Status = IHISI_SUCCESS;

#ifdef L05_CUSTOMIZE_MULTI_LOGO_SUPPORT
  //
  // Execute Feature function directly to make sure the CPU register, and function return Status are all correct for AP and IHISI Services
  //
  if (ApCommDataBuffer->DataType == L05CustomizeMultiLogoSupport) {

    Status = L05CheckCustomizeLogo (ApCommDataBuffer, BiosCommDataBuffer);

    if (!EFI_ERROR (Status)) {
      //
      // Follow default behavior to exit this function as follow
      //
      CopyMem ((VOID *) (UINTN) IhisiProtReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RCX), BiosCommDataBuffer, sizeof (BIOS_COMMUNICATION_DATA_TABLE));

      Status = IHISI_END_FUNCTION_CHAIN;
    }
  }
#endif

  if (BiosCommDataBuffer != NULL) {
    gSmst->SmmFreePool (BiosCommDataBuffer);
  }

  return Status;
}

/**
  AH=42h, OEM Extra Data Write.

  Data type 04h = L05CustomizeMultiLogo
  
  @retval EFI_SUCCESS                   OEM Extra Data Write successful.
  @return Other                         OEM Extra Data Write failed.
**/
EFI_STATUS
EFIAPI
L05IhisiS42T04CustomizeMultiLogo (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINTN                                 WriteSize;
  UINT8                                 ShutdownMode;
  UINTN                                 RomBaseAddress;
  UINT8                                 *WriteDataBuffer;

  Status = IHISI_SUCCESS;
  WriteDataBuffer = (UINT8 *) (UINTN) IhisiProtReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RSI);
  WriteSize       = (UINTN) IhisiProtReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RDI);
  RomBaseAddress  = (UINTN) (IhisiProtReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RCX) >> 8);
  ShutdownMode    = (UINT8) IhisiProtReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RCX);

  if (!IhisiProtBufferInCmdBuffer ((VOID *) WriteDataBuffer, WriteSize)) {
    return IHISI_BUFFER_RANGE_ERROR;
  }

#ifdef L05_CUSTOMIZE_MULTI_LOGO_SUPPORT
  //
  // Execute Feature function directly to make sure the CPU register, and function return Status are all correct for AP and IHISI Services
  //
  switch (mOemExtraDataType) {

  case L05CustomizeMultiLogoSupport:

    Status = L05WriteCustomizeLogo ();

    if (!EFI_ERROR (Status)) {

      //
      // Follow default behavior to exit this function as follow
      //
      return IHISI_END_FUNCTION_CHAIN;
    }

    break;

  default:
    break;
  }
#endif

  return Status;
}
