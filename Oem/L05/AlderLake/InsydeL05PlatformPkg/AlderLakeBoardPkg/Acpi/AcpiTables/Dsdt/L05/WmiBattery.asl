Scope(\_SB)
{
    Mutex(MWMI, 0)

  //
  // WMI-to-ACPI mapper device.
  //
  Device(WMI4)
  {
    // PNP0C14 is Plug and Play ID assigned to WMI mapper
    Name(_HID, EISAID("PNP0C14"))
      Name(_UID, "WBAT")

      //
      // _WDG evaluates to a data structure that specifies the data
      // blocks supported by the ACPI device.
      //
      Name(_WDG, Buffer() {
           // -------- Method execution for Lenovo_BatteryInformation
           0x76, 0x37, 0xa0, 0xc3, 0xac, 0x51, 0xaa, 0x49, 0xad, 0x0f,
           0xf2, 0xf7, 0xd6, 0x2c, 0x3f, 0x3c,
           65, 68,        // Object ID (AD)
           6,             // Instance Count  >>> Should be 3 for Single Battery System.
           0x05,          // Flags WMIACPI_REGFLAG_EXPENSIVE & STRING
           // -------- MOF data
           0x21, 0x12, 0x90, 0x05, 0x66, 0xd5, 0xd1, 0x11, 0xb2, 0xf0,
           0x00, 0xa0, 0xc9, 0x06, 0x29, 0x10,
           66, 68,        // Object ID (BD)
           1,             // Instance Count
           0x00,          // Flags
           })

    //
    //  Items
    //
    //    Battery ID, Fucntion, Item name
    //
    Name(ITEM, Package() {
         Package() {0, 0, "BAT0 BatMaker"},
         Package() {0, 1, "BAT0 HwId    "},
         Package() {0, 2, "BAT0 MfgDate "},
         Package() {1, 0, "BAT1 BatMaker"},
         Package() {1, 1, "BAT1 HwId "},
         Package() {1, 2, "BAT1 MfgDate "},
         })

    //
    // WQAD() - Data block query for Lenovo_BatteryInfomation
    //
    // This method returns the instance data for the specified
    // instance index.
    //
    //     Arg0: Instance index of data block
    //
    //     Return: Error string.
    //
    Method(WQAD, 1) {
      Acquire(\_SB.MWMI, 0xffff)       // Serialize buffer access
      Store( PSAG(Arg0), Local0)
      Store(DerefOf(Index(ITEM, Local0)), Local1)
      Store(DerefOf(Index(Local1, 0)), Local2) // Local1: BAT ID
      Store(DerefOf(Index(Local1, 1)), Local3) // Local2: Function ID
      Store(DerefOf(Index(Local1, 2)), Local4) // Local3: Item name
      Store (BATD (Local2, Local3), Local5)
      Concatenate(Local4, ",", Local6)
      Concatenate(Local6, Local5, Local7)
      Release(\_SB.MWMI)
      Return(Local7)
    }
    Method(PSAG, 1) {
      Return(Arg0)
    }

    //
    // BATD() - Return Battery Information
    //
    // This method returns the Battery Information.
    //
    //     Arg0: Battery ID
    //              0 - Primary battery
    //              1 - Seconday battery
    //
    //     Arg1: Requred Item
    //              0 - BatMaker
    //              1 - HwId
    //              2 - MfgDate
    //
    //     Return: Requrend Information.
    //
    Method(BATD, 2) {
      If (LEqual(Arg0, 0)) {
        If (LEqual(Arg1, 0)) {
          //
          // Return Primary Battery Marker
          // 0x37 Byte 0 & 1
          //
          Store("0001", Local0)
        }
        If (LEqual(Arg1, 1)) {
          //
          // Return Primary Battery  ID
          // 0x37 Byte 2 & 3
          //
          Store("0002", Local0)
        }
        If (LEqual(Arg1, 2)) {
          //
          // Return Primary Battery MfgDate
          // 0x27
          //
          Store("XXXXXSSSYYY", Local0)
        }
      }
      If (LEqual(Arg0, 1)) {
        If (LEqual(Arg1, 0)) {
          //
          // Return Secondary Battery Marker
          // 0x37 Byte 0 & 1
          //
          Store("0003", Local0)
        }
        If (LEqual(Arg1, 1)) {
          //
          // Return Secondary Battery ID
          // 0x37 Byte 2 & 3
          //
          Store("0004", Local0)
        }
        If (LEqual(Arg1, 2)) {
          //
          // Return Secondary Battery MfgDate
          // 0x27
          //
          Store("XXXXXSSSYYY", Local0)
        }
      }
      Return(Local0)
    }

    // -------- Compiled version of "Associated MOF File" below --------
    // Memo: generated by mofcomp.exe version 6.1.7600.16385

    Name(WQBD, Buffer() {
         0x46,0x4F,0x4D,0x42,0x01,0x00,0x00,0x00,0x65,0x02,0x00,0x00,0xF8,0x05,0x00,0x00,
         0x44,0x53,0x00,0x01,0x1A,0x7D,0xDA,0x54,0x18,0xD1,0x82,0x00,0x01,0x06,0x18,0x42,
         0x10,0x05,0x10,0x8A,0x0D,0x21,0x02,0x0B,0x83,0x50,0x50,0x18,0x14,0xA0,0x45,0x41,
         0xC8,0x05,0x14,0x95,0x02,0x21,0xC3,0x02,0x14,0x0B,0x70,0x2E,0x40,0xBA,0x00,0xE5,
         0x28,0x72,0x0C,0x22,0x02,0xF7,0xEF,0x0F,0x31,0xD0,0x18,0xA8,0x50,0x08,0x89,0x00,
         0xA6,0x42,0xE0,0x08,0x41,0xBF,0x02,0x10,0x3A,0x14,0x20,0x53,0x80,0x41,0x01,0x4E,
         0x11,0x44,0x10,0xA5,0x65,0x01,0xBA,0x05,0xF8,0x16,0xA0,0x1D,0x42,0x68,0x91,0x9A,
         0x9F,0x04,0x81,0x6A,0x5B,0x80,0x45,0x01,0xB2,0x41,0x08,0xA0,0xC7,0xC1,0x44,0x0E,
         0x02,0x25,0x66,0x10,0x28,0x9D,0x73,0x90,0x4D,0x60,0xE1,0x9F,0x4C,0x94,0xF3,0x88,
         0x92,0xE0,0xA8,0x0E,0x22,0x42,0xF0,0x72,0x05,0x48,0x9E,0x80,0x34,0x4F,0x4C,0xD6,
         0x07,0xA1,0x21,0xB0,0x11,0xF0,0x88,0x12,0x40,0x58,0xA0,0x75,0x2A,0x14,0x0C,0xCA,
         0x03,0x88,0xE4,0x8C,0x15,0x05,0x6C,0xAF,0x13,0x91,0xC9,0x81,0x52,0x49,0x70,0xA8,
         0x61,0x5A,0xE2,0xEC,0x34,0xB2,0x13,0x39,0xB6,0xA6,0x87,0x2C,0x48,0x26,0x6D,0x28,
         0xA8,0xB1,0x7B,0x5A,0x27,0xE5,0x99,0x46,0x3C,0x28,0xC3,0x24,0xF0,0x28,0x18,0x1A,
         0x27,0x28,0x0B,0x42,0x0E,0x06,0x8A,0x02,0x3C,0x09,0xCF,0xB1,0x78,0x01,0xC2,0x67,
         0x4C,0xA6,0x1D,0x23,0x81,0xCF,0x04,0x1E,0xE6,0x31,0x63,0x47,0x14,0x2E,0xE0,0xF9,
         0x1C,0x43,0xE4,0xB8,0x87,0x1A,0xE3,0x28,0x22,0x3F,0x08,0x60,0x05,0x1D,0x04,0x90,
         0x38,0xFF,0xFF,0xE3,0x89,0x76,0xDA,0xC1,0x42,0xC7,0x39,0xBF,0xD0,0x18,0xD1,0xE3,
         0x40,0xC9,0x80,0x90,0x47,0x01,0x56,0x61,0x35,0x91,0x04,0xBE,0x07,0x74,0x76,0x12,
         0xD0,0xA5,0x21,0x46,0x6F,0x08,0xD2,0x26,0xC0,0x96,0x00,0x6B,0x02,0x8C,0xDD,0x06,
         0x08,0xCA,0xD1,0x36,0x87,0x22,0x84,0x28,0x21,0xE2,0x86,0xAC,0x11,0x45,0x10,0x95,
         0x41,0x08,0x35,0x50,0xD8,0x28,0xF1,0x8D,0x13,0x22,0x48,0x02,0x8F,0x1C,0x77,0x04,
         0xF0,0xD8,0x0E,0xE8,0x04,0x4F,0xE9,0x71,0xC1,0x04,0x9E,0xF7,0xC1,0x1D,0xEA,0x21,
         0x1C,0x70,0xD4,0x18,0xC7,0xF1,0x4C,0x40,0x16,0x2E,0x0D,0x20,0x8A,0x04,0x8F,0x3A,
         0x32,0xF8,0x70,0xE0,0x41,0x7A,0x9E,0x9E,0x40,0x90,0x43,0x38,0x82,0xC7,0x86,0xA7,
         0x02,0x8F,0x81,0x5D,0x17,0x7C,0x0E,0xF0,0x31,0x01,0xEF,0x1A,0x50,0xA3,0x7E,0x3A,
         0x60,0x93,0x0E,0x87,0x19,0xAE,0x87,0x1D,0xEE,0x04,0x1E,0x0E,0x1E,0x33,0xF8,0x91,
         0xC3,0x83,0xC3,0xCD,0xF0,0x64,0x8E,0xAC,0x54,0x01,0x66,0x4F,0x08,0x3A,0x4D,0xF8,
         0xCC,0xC1,0x6E,0x00,0xE7,0xD3,0x33,0x24,0x91,0x3F,0x08,0xD4,0xC8,0x0C,0xED,0x69,
         0xBF,0x7A,0x18,0xF2,0xA1,0xE0,0xB0,0x98,0xD8,0xB3,0x07,0x1D,0x0F,0xF8,0xAF,0x24,
         0x0F,0x1B,0x9E,0xBE,0xE7,0x6B,0x82,0x91,0x07,0x8E,0x1E,0x88,0xA1,0x9F,0x38,0x0E,
         0xE3,0x34,0x7C,0x09,0xF1,0x39,0xE0,0xFF,0x1F,0x24,0xC6,0x31,0x79,0x70,0x3C,0xD8,
         0xC8,0xE9,0x51,0xC5,0x47,0x0A,0x7E,0xBE,0xF0,0x91,0x82,0x5D,0x10,0x9E,0x1C,0x0C,
         0x71,0x38,0x67,0xE5,0x13,0x85,0x0F,0x2A,0xB8,0x13,0x05,0x5C,0x85,0xE8,0xE4,0x36,
         0x61,0xB4,0x67,0x81,0xC7,0x09,0x98,0x07,0x01,0xF0,0x8D,0xDF,0x07,0x19,0xB0,0x4D,
         0x09,0x3B,0x24,0x78,0x47,0x19,0xE0,0x71,0x32,0xC1,0x1D,0x27,0x3C,0x04,0x3E,0x80,
         0x87,0x90,0x93,0xB4,0xD2,0xA9,0x21,0xCF,0x3C,0x60,0x1B,0x06,0x57,0x68,0xD3,0xA7,
         0x46,0xA3,0x56,0x0D,0xCA,0xD4,0x28,0xD3,0xA0,0x56,0x9F,0x4A,0x8D,0x19,0xFB,0xE1,
         0x58,0xDC,0xBB,0x40,0x07,0x03,0x0B,0x7B,0x21,0xE8,0x88,0xE0,0x58,0x20,0x34,0x08,
         0x9D,0x40,0xFC,0xFF,0x07,
    })
  }

}
