## @file
#
#******************************************************************************
#* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##
### @file
#  Component information file for the ACPI tables
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 1999 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# Defines Section - statements that will be processed to create a Makefile.
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = AcpiTables
  FILE_GUID                      = 7E374E25-8E01-4FEE-87F2-390C23C606CD
  MODULE_TYPE                    = USER_DEFINED
  VERSION_STRING                 = 1.0

[Sources]
  Fadt/Fadt6.3.act
  Facs/Facs.act
  Hpet/Hpet.act
  Madt/Madt.act
  Madt/MadtX2Apic.act
  Mcfg/Mcfg.act
  Ecdt/Ecdt.act
  Dsdt/Dsdt.asl
!if $(S570_SUPPORT_ENABLE) == YES
  
!else
  SsdtRvp/Thermal.asl
  SsdtRtd3/AdlSAepRtd3.asl
  SsdtRtd3/AdlSAepTbtRtd3.asl
  SsdtRtd3/AdlSRvpRtd3.asl
  SsdtRtd3/AdlSRvpTbtRtd3.asl
  SsdtRtd3/AdlSBRvpRtd3.asl
  SsdtRtd3/AdlSBRvpTbtRtd3.asl
  SsdtRtd3/AdlSBRvp3Rtd3.asl
  SsdtRtd3/AdlSBRvp3TbtRtd3.asl
!endif  
  SsdtRtd3/AdlSBAepRtd3.asl
  SsdtRtd3/AdlPRvpRtd3.asl
  SsdtRtd3/AdlPHsioRvpRtd3.asl
  SsdtRtd3/AdlMRvpRtd3.asl
  SsdtRtd3/AdlSS8RvpRtd3.asl
  SsdtRtd3/AdlSS8RvpTbtRtd3.asl
  SsdtRtd3/AdlMRvp2aRtd3.asl
  SsdtRtd3/AdlPDdr5MrDTbtRvpRtd3.asl
  SsdtXhci/UsbPortXhciAdlS03Rvp.asl
  SsdtXhci/UsbPortXhciAdlS08Rvp.asl
  SsdtXhci/UsbPortXhciAdlS10Rvp.asl
  SsdtXhci/UsbPortXhciAdlLp4Rvp.asl
  SsdtXhci/UsbPortXhciAdlMLp4Rvp.asl
  SsdtXhci/UsbPortXhciAdlSSbgaRvp.asl
  SsdtXhci/UsbPortXhciAdlSSbgaRvp3.asl
  SsdtXhci/UsbPortXhciAdlpDdr5MrRvp.asl

################################################################################
#
# Package Dependency Section - list of Package files that are required for
#                              this module.
#
################################################################################
[Packages]
#_Start_L05_FEATURE_
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
#_End_L05_FEATURE_
  MdePkg/MdePkg.dec
#[-start-190724-IB17700055-add]#
  MdeModulePkg/MdeModulePkg.dec
#[-end-190724-IB17700055-add]#
  AlderLakePlatSamplePkg/PlatformPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  PcAtChipsetPkg/PcAtChipsetPkg.dec
#[-start-190723-IB15410291-add]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#[-end-190723-IB15410291-add]#
#[-start-210520-KEBIN00003-modify]#
!if $(LCFC_SUPPORT_ENABLE) == YES
  LfcPkg/LfcPkg.dec
!endif
#[end-210520-KEBIN00003-modify]#

################################################################################
#
# Library Class Section - list of Library Classes that are required for
#                         this module.
#
################################################################################

[LibraryClasses]

[Pcd]
  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress             ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdPciExpressRegionLength                ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdAcpiDebugEnableFlag                   ## CONSUMES
  gPcAtChipsetPkgTokenSpaceGuid.PcdRealTimeClockUpdateTimeout   ## CONSUMES
#[-start-190723-IB15410291-add]#
  gChipsetPkgTokenSpaceGuid.PcdDebugSizeMask
  gChipsetPkgTokenSpaceGuid.PcdDebugSize
#[-end-190723-IB15410291-add]#
#[-start-190722-IB17700055-add]#
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiS3Enable
#[-end-190722-IB17700055-add]#

[FixedPcd]
  gSiPkgTokenSpaceGuid.PcdAcpiBaseAddress               ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdTcoBaseAddress                ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdApicLocalAddress     ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdApicIoAddress        ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdAcpiEnableSwSmi      ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdAcpiDisableSwSmi     ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdApicIoIdPch          ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdVmdEnable                     ## CONSUMES
#[-start-201005-IB10181010-add]#
  gSiPkgTokenSpaceGuid.PcdEmbeddedEnable                ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdAdlLpSupport
#[-end-201005-IB10181010-add]#
  gBoardModuleTokenSpaceGuid.PcdEcEnable                ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdAdlLpSupport                  ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdITbtEnable                    ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable           ## CONSUMES

#[-start-190723-IB15410291-add]#
[FeaturePcd]
  gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag
#[-start-181022-IB15590139-add]#
  gChipsetPkgTokenSpaceGuid.PcdComPortDdt
  gChipsetPkgTokenSpaceGuid.PcdDebugUsePchComPort
#[-start-181022-IB15590139-add]#
#[-end-190723-IB15410291-add]#
#[-start-201124-IB09480117-add]#
  gChipsetPkgTokenSpaceGuid.PcdHybridGraphicsSupported
#[-start-201124-IB09480117-add]#
#[-start-201127-IB09480119-add]#
#[-start-211206-IB05660188-add]#
  gChipsetPkgTokenSpaceGuid.PcdNvidiaOptimusSupported
  gChipsetPkgTokenSpaceGuid.PcdAmdPowerXpressSupported
#[-end-211206-IB05660188-add]#
#[-end-201127-IB09480119-add]#

################################################################################
#
# Protocol C Name Section - list of Protocol and Protocol Notify C Names
#                           that this module uses or produces.
#
################################################################################
[Protocols]

[PPIs]

[Guids]

[Depex]
