## @file
#  Platform Package Description file
#
#******************************************************************************
#* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  !include $(PROJECT_PKG)/OemConfig.env
  !include InsydeL05ModulePkg/Package.env

[LibraryClasses]
#_Start_L05_HOTKEY_
!if $(L05_ALL_FEATURE_ENABLE) == YES
  PlatformBdsLib|$(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(CHIPSET_PKG)/Library/PlatformBdsLib/PlatformBdsLib.inf
!endif
#_End_L05_HOTKEY_

!if $(L05_HDD_PASSWORD_ENABLE) == YES
  H2OHddPasswordTableLib|$(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Library/H2OHddPasswordTableLib/H2OHddPasswordTableLib.inf
!endif

[LibraryClasses.common.PEI_CORE]

[LibraryClasses.common.PEIM]
!if $(L05_CRISIS_RECOVERY_PROGRESS_BAR_ENABLE) == YES
  L05PeiProgressBarLib|$(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_NAME)Feature/Library/L05PeiProgressBarLib/L05PeiProgressBarLib.inf
!endif

[LibraryClasses.common.UEFI_DRIVER]

[LibraryClasses.common.DXE_DRIVER]

[LibraryClasses.common.DXE_SMM_DRIVER]
!if $(L05_SETUP_UNDER_OS_SUPPORT) == YES
  L05WmiSetupItemLib|$(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_NAME)Feature/Library/L05WmiSetupItemLib/L05WmiSetupItemLib.inf
!endif

[LibraryClasses.common.COMBINED_SMM_DXE]

[LibraryClasses.common.SMM_CORE]

[LibraryClasses.common.UEFI_APPLICATION]

[PcdsFeatureFlag]
!if $(L05_SMB_BIOS_ENABLE) == YES
#_Start_L05_INTERRUPT_MENU_
  gInsydeTokenSpaceGuid.PcdDisplayOemHotkeyString|TRUE
#_End_L05_INTERRUPT_MENU_

!if $(L05_MAC_ADDRESS_PASS_THROUGH_ENABLE) == YES
  gInsydeTokenSpaceGuid.PcdH2OBdsCpNetworkUpdateMacAddrSupported|TRUE
!endif
!endif

!if $(L05_BIOS_UPDATE_UI_ENABLE) == YES
  gInsydeTokenSpaceGuid.PcdH2OBdsCpCapsuleUpdateProgressSupported|TRUE
!endif

  #
  # Limited to internal storage
  #
  gInsydeTokenSpaceGuid.PcdH2OBdsCpBootDeviceEnumAfterSupported|TRUE

!if $(L05_BIOS_SELF_HEALING_SUPPORT) == YES
  gInsydeTokenSpaceGuid.PcdH2OBaseCpVerifyFvSupported|YES
  gInsydeTokenSpaceGuid.PcdH2OPeiCpDxeFvCorruptedSupported|YES
!endif

[PcdsFixedAtBuild]
  gL05ServicesTokenSpaceGuid.PcdOemModulePkgVersion|$(OEM_MODULE_PKG_VERSION)
  gL05ServicesTokenSpaceGuid.PcdL05ChipsetName|0x80861019  # L05_CHIPSET_NAME_ALDERLAKE
#Start_BIOS_FLASH_UPDATE_
  gInsydeTokenSpaceGuid.PcdIhisiFbtsVendorId|0x17AA
  gInsydeTokenSpaceGuid.PcdIhisiFbtsBatteryLowBound|30
#End_BIOS_FLASH_UPDATE_

#_Start_L05_HDD_PASSWORD_
  #
  # [Lenovo China Minimum BIOS Spec V 1.39]
  #   3.4 BIOS Security Passwords
  #     Lenovo require BIOS must lock system after user input wrong password twice.
  #
  gInsydeTokenSpaceGuid.PcdH2OHddPasswordMaxCheckPasswordCount|0x02

!if $(L05_NOTEBOOK_PASSWORD_V1_1_ENABLE) == YES
#
# [Lenovo Notebook Password Design Spec v1.1]
#   4.1.1. Check HDP
#     If password check, total number of different password attempts (Master Password and User Password),
#     failed exceeds three times, the prompt should be displayed as Figure 4-4.
#
  gInsydeTokenSpaceGuid.PcdH2OHddPasswordMaxCheckPasswordCount|0x03
!endif
#_End_L05_HDD_PASSWORD_

#_Start_L05_SETUP_MENU_
  #
  # Date and time initial values used if the RTC values are invalid during driver initialization.
  #
  gInsydeTokenSpaceGuid.PcdRealTimeClockInitYear|2020
  gInsydeTokenSpaceGuid.PcdRealTimeClockInitMonth|1
  gInsydeTokenSpaceGuid.PcdRealTimeClockInitDay|1
  gInsydeTokenSpaceGuid.PcdRealTimeClockInitHour|0
  gInsydeTokenSpaceGuid.PcdRealTimeClockInitMinute|0
  gInsydeTokenSpaceGuid.PcdRealTimeClockInitSecond|0
#_End_L05_SETUP_MENU_

#_Start_L05_HDD_PASSWORD_ENABLE_
  #
  # Temporarily Disable TCG Opal Supported.
  #
  gInsydeTokenSpaceGuid.PcdH2OHddPasswordTcgOpalSupported|FALSE
#_End_L05_HDD_PASSWORD_ENABLE_

[PcdsDynamicExDefault]
!if $(L05_ONE_KEY_RECOVERY_ENABLE) == YES
  gH2OBdsDefaultBootListGenericOsTokenSpaceGuid.L05OneKeyRecvoeryOS     |L"\\EFI\\Microsoft\\Boot\\LrsBootMgr.efi\tLenovo Recovery System"
!endif

!if $(L05_SMB_BIOS_ENABLE) == YES
#_Start_L05_INTERRUPT_MENU_
  #
  # [Lenovo SMB BIOS Special Requirements V1.1]
  #   2.2 Hotkey Definition
  #     Startup Interrupt Menu - Enter
  #
  # BDS Hot Key default PCD. These PCDs will format as
  #   typedef struct _PCD_H2O_HOT_KEY {
  #     EFI_GUID HotKeyProtocolGuid;
  #     UINT32   Id;
  #   //CHAR8    HotKeys[];
  #   //CHAR8    Options[];
  #   //CHAR8    DevicePath[];
  #   } PCD_H2O_HOT_KEY;
  #
  gH2OBdsHotKeyGuid.PcdH2OHotKeyInterruptMenu|{GUID("00000000-0000-0000-0000-000000000000"), UINT32(0), "enter", "", ""}
  #
  # BDS Hot Key Description default PCD for text mode. These PCDs will format as
  #   {STRING_TOKEN(BEFORE_STR_ID),STRING_TOKEN(AFTER_STR_ID), <"keyword=value;">*}
  #
  gH2OBdsHotKeyDescGuid.PcdH2OHotKeyInterruptMenuDesc|{STRING_TOKEN(L05_STR_OEM_BADGING_STR_INTERRUPT_MENU), STRING_TOKEN(L05_STR_OEM_BADGING_STR_INTERRUPT_MENU_SELECT),"x=0em;y=0em;xorigin=left;yorigin=bottom;"}
  #
  # BDS Hot Key Quiet Description default PCD for graphics mode. These PCDs will format as
  #   {STRING_TOKEN(BEFORE_STR_ID),STRING_TOKEN(AFTER_STR_ID), <"keyword=value;">*}
  #
  gH2OBdsHotKeyDescQuietGuid.PcdH2OHotKeyInterruptMenuDescQuiet|{STRING_TOKEN(L05_STR_OEM_BADGING_STR_INTERRUPT_MENU), STRING_TOKEN(L05_STR_OEM_BADGING_STR_INTERRUPT_MENU_SELECT),"x=0em;y=0em;xorigin=center;yorigin=top(70%);"}
#_End_L05_INTERRUPT_MENU_
!endif

[Components]

[Components.$(PEI_ARCH)]
#_Start_L05_BIOS_FLASH_UPDATE_
!if $(L05_ALL_FEATURE_ENABLE) == YES
!disable InsydeModulePkg/Universal/Recovery/CrisisRecoveryPei/CrisisRecoveryPei.inf
  InsydeModulePkg/Universal/Recovery/CrisisRecoveryPei/CrisisRecoveryPei.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Recovery/CrisisRecoveryPei
  }
!endif
#_End_L05_BIOS_FLASH_UPDATE_

#_Start_L05_PREFIX_KERNEL_PEI_VOLUME_INFO_DevType_
  !disable InsydeModulePkg/Universal/FileAccess/FileAccessPei/FileAccessPei.inf
  InsydeModulePkg/Universal/FileAccess/FileAccessPei/FileAccessPei.inf {
    <SOURCE_OVERRIDE_PATH>
    $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/FileAccess/FileAccessPei
  }
#_End_L05_PREFIX_KERNEL_PEI_VOLUME_INFO_DevType_

!if $(L05_HDD_PASSWORD_ENABLE) == YES
  !if gInsydeTokenSpaceGuid.PcdH2OHddPasswordPeiSupported
    !disable InsydeModulePkg/Universal/Security/HddPassword/HddPasswordPei.inf
      InsydeModulePkg/Universal/Security/HddPassword/HddPasswordPei.inf {
      <LibraryClasses>
        TcgStorageOpalLib|SecurityPkg/Library/TcgStorageOpalLib/TcgStorageOpalLib.inf
        Tcg2PhysicalPresenceLib|SecurityPkg/Library/PeiTcg2PhysicalPresenceLib/PeiTcg2PhysicalPresenceLib.inf
      <SOURCE_OVERRIDE_PATH>
        $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Security/HddPassword
      }
  !endif
!endif

[Components.$(DXE_ARCH)]
#_Start_L05_CHANGE_SYSTEM_PASSWORD_STORE_
!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable InsydeModulePkg/Universal/Security/SysPasswordDxe/SysPasswordDxe.inf
  InsydeModulePkg/Universal/Security/SysPasswordDxe/SysPasswordDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Security/SysPasswordDxe
  }
!endif
#_End_L05_CHANGE_SYSTEM_PASSWORD_STORE_

!if $(L05_HDD_PASSWORD_ENABLE) == YES
  !if gInsydeTokenSpaceGuid.PcdH2OHddPasswordSupported
    !disable InsydeModulePkg/Universal/Security/HddPassword/HddPassword.inf
    InsydeModulePkg/Universal/Security/HddPassword/HddPassword.inf {
      <SOURCE_OVERRIDE_PATH>
        $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Security/HddPassword
    }
  !endif
!endif

#_Start_L05_SETUP_MENU_
!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable $(CHIPSET_PKG)/UefiSetupUtilityDxe/SetupUtilityDxe.inf
  $(CHIPSET_PKG)/UefiSetupUtilityDxe/SetupUtilityDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(CHIPSET_PKG)/UefiSetupUtilityDxe
  }
!endif
#_End_L05_SETUP_MENU_

!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable InsydeModulePkg/Universal/BdsDxe/BdsDxe.inf
  InsydeModulePkg/Universal/BdsDxe/BdsDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/BdsDxe
  }
!endif

!if $(L05_SETUP_MOUSE_ENABLE) == NO
  !disable InsydeModulePkg/Universal/UserInterface/SetupMouseDxe/SetupMouseDxe.inf
!endif

!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable InsydeModulePkg/Universal/UserInterface/BootManagerDxe/BootManagerDxe.inf
  InsydeModulePkg/Universal/UserInterface/BootManagerDxe/BootManagerDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/UserInterface/BootManagerDxe
  }
!endif

#_Start_L05_SMBIOS_
!if $(L05_ALL_FEATURE_ENABLE) == YES
  !if gInsydeTokenSpaceGuid.PcdH2OIrsiSupported
    !disable InsydeModulePkg/Universal/Smbios/PnpRuntimeDxe/PnpRuntimeDxe.inf
    InsydeModulePkg/Universal/Smbios/PnpRuntimeDxe/PnpRuntimeDxe.inf {
      <SOURCE_OVERRIDE_PATH>
        $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Smbios/PnpRuntimeDxe
    }
  !endif
!endif

!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable InsydeModulePkg/Universal/Smbios/PnpSmm/PnpSmm.inf
  InsydeModulePkg/Universal/Smbios/PnpSmm/PnpSmm.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Smbios/PnpSmm
  }
!endif

!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable InsydeModulePkg/Universal/Smbios/SmbiosDxe/SmbiosDxe.inf
  InsydeModulePkg/Universal/Smbios/SmbiosDxe/SmbiosDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Smbios/SmbiosDxe
  }
!endif
#_End_L05_SMBIOS_

!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable InsydeSetupPkg/Drivers/H2OFormBrowserDxe/H2OFormBrowserDxe.inf
  InsydeSetupPkg/Drivers/H2OFormBrowserDxe/H2OFormBrowserDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeSetupPkg/Drivers/H2OFormBrowserDxe
  }
  
  !disable InsydeSetupPkg/Drivers/HiiLayoutPkgDxe/HiiLayoutPkgDxe.inf
  InsydeSetupPkg/Drivers/HiiLayoutPkgDxe/HiiLayoutPkgDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeSetupPkg/Drivers/HiiLayoutPkgDxe
  }
  
  !if gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalTextDESupported
    !disable InsydeSetupPkg/Drivers/H2ODisplayEngineLocalTextDxe/H2ODisplayEngineLocalTextDxe.inf
    InsydeSetupPkg/Drivers/H2ODisplayEngineLocalTextDxe/H2ODisplayEngineLocalTextDxe.inf {
      <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeSetupPkg/Drivers/H2ODisplayEngineLocalTextDxe
    }
  !endif
  
  !if gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported
    !disable InsydeSetupPkg/Drivers/H2ODisplayEngineLocalMetroDxe/H2ODisplayEngineLocalMetroDxe.inf
    InsydeSetupPkg/Drivers/H2ODisplayEngineLocalMetroDxe/H2ODisplayEngineLocalMetroDxe.inf {
      <SOURCE_OVERRIDE_PATH>
        $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeSetupPkg/Drivers/H2ODisplayEngineLocalMetroDxe
    }
  !endif
!endif

#_Start_L05_BIOS_FLASH_UPDATE_
!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable InsydeModulePkg/Universal/IhisiServicesSmm/IhisiServicesSmm.inf
  InsydeModulePkg/Universal/IhisiServicesSmm/IhisiServicesSmm.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/IhisiServicesSmm
  }
!endif
#_End_L05_BIOS_FLASH_UPDATE_

#_Start_L05_TPM_REPORTING_
!if $(L05_TPM_REPORTING_ENABLE) == YES
  !disable InsydeModulePkg/Universal/Security/Tcg/TcgDxe/TcgDxe.inf
  InsydeModulePkg/Universal/Security/Tcg/TcgDxe/TcgDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Security/Tcg/TcgDxe
  }
!endif

!if $(L05_TPM_REPORTING_ENABLE) == YES
  !disable InsydeModulePkg/Universal/Security/Tcg/Tcg2Dxe/Tcg2Dxe.inf
  InsydeModulePkg/Universal/Security/Tcg/Tcg2Dxe/Tcg2Dxe.inf {
    <LibraryClasses>
      NULL|SecurityPkg/Library/HashInstanceLibSha1/HashInstanceLibSha1.inf
      NULL|SecurityPkg/Library/HashInstanceLibSha256/HashInstanceLibSha256.inf
      NULL|SecurityPkg/Library/HashInstanceLibSha384/HashInstanceLibSha384.inf
      NULL|SecurityPkg/Library/HashInstanceLibSha512/HashInstanceLibSha512.inf
      NULL|InsydeModulePkg/Library/HashInstanceLibSm3/HashInstanceLibSm3.inf
      PcdLib|MdePkg/Library/DxePcdLib/DxePcdLib.inf
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Security/Tcg/Tcg2Dxe
  }
!endif
#_End_L05_TPM_REPORTING_

#_Start_L05_WMI_BATTERY_
!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable $(PLATFORM_BOARD_PACKAGE)/Acpi/AcpiTables/AcpiTables.inf
  $(PLATFORM_BOARD_PACKAGE)/Acpi/AcpiTables/AcpiTables.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(PLATFORM_BOARD_PACKAGE)/Acpi/AcpiTables
  }
!endif
#_End_L05_WMI_BATTERY_

#_Start_L05_DUAL_MODE_PXE_
!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable InsydeNetworkPkg/Drivers/NetworkLockerDxe/NetworkLockerDxe.inf
  InsydeNetworkPkg/Drivers/NetworkLockerDxe/NetworkLockerDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeNetworkPkg/Drivers/NetworkLockerDxe
  }
!endif
#_End_L05_DUAL_MODE_PXE_

#_Start_L05_SETUP_MENU_
!if $(L05_ALL_FEATURE_ENABLE) == YES
  !disable InsydeModulePkg/Universal/Console/GraphicsConsoleDxe/GraphicsConsoleDxe.inf
  InsydeModulePkg/Universal/Console/GraphicsConsoleDxe/GraphicsConsoleDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Console/GraphicsConsoleDxe
  }
!endif
#_End_L05_SETUP_MENU_

!if $(L05_BIOS_POST_LOGO_DIY_SUPPORT) == YES
  !if gInsydeTokenSpaceGuid.PcdH2OAcpiBgrtSupported
    !disable InsydeModulePkg/Universal/Acpi/BootGraphicsResourceTableDxe/BootGraphicsResourceTableDxe.inf
    InsydeModulePkg/Universal/Acpi/BootGraphicsResourceTableDxe/BootGraphicsResourceTableDxe.inf {
      <SOURCE_OVERRIDE_PATH>
        $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Acpi/BootGraphicsResourceTableDxe
    }
  !endif
!endif

!if $(L05_NOTEBOOK_CLOUD_BOOT_ENABLE) == YES
  !if gInsydeTokenSpaceGuid.PcdH2ONetworkHttpSupported
    !disable InsydeNetworkPkg/Drivers/HttpDxe/HttpDxe.inf
    InsydeNetworkPkg/Drivers/HttpDxe/HttpDxe.inf {
      <SOURCE_OVERRIDE_PATH>
        $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeNetworkPkg/Drivers/HttpDxe
    }

    !disable InsydeNetworkPkg/Drivers/HttpBootDxe/HttpBootDxe.inf
    InsydeNetworkPkg/Drivers/HttpBootDxe/HttpBootDxe.inf {
      <LibraryClasses>
        UefiBootManagerLib|MdeModulePkg/Library/UefiBootManagerLib/UefiBootManagerLib.inf
      <SOURCE_OVERRIDE_PATH>
        $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeNetworkPkg/Drivers/HttpBootDxe
    }

    !if gInsydeTokenSpaceGuid.PcdH2ONetworkTlsSupported
      !disable InsydeNetworkPkg/Drivers/TlsDxe/TlsDxe.inf
      InsydeNetworkPkg/Drivers/TlsDxe/TlsDxe.inf {
        <SOURCE_OVERRIDE_PATH>
          $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeNetworkPkg/Drivers/TlsDxe
      }
    !endif
  !endif
!endif

!if $(L05_BIOS_SELF_HEALING_SUPPORT) == YES
  !if gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceEnabled
    !disable InsydeModulePkg/Universal/Recovery/BiosUpdateFaultToleranceDxe/BiosUpdateFaultToleranceDxe.inf
    InsydeModulePkg/Universal/Recovery/BiosUpdateFaultToleranceDxe/BiosUpdateFaultToleranceDxe.inf {
      <SOURCE_OVERRIDE_PATH>
        $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeModulePkg/Universal/Recovery/BiosUpdateFaultToleranceDxe
    }
  !endif
!endif

!if $(L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE) == YES
  !if gChipsetPkgTokenSpaceGuid.PcdUefiWirelessCnvtEnable
    !disable $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Universal/WifiConnectionManagerDxe/WifiConnectionManagerDxe.inf
    $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Universal/WifiConnectionManagerDxe/WifiConnectionManagerDxe.inf {
      <SOURCE_OVERRIDE_PATH>
        $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Universal/WifiConnectionManagerDxe
    }
  !endif

  !if gInsydeTokenSpaceGuid.PcdH2ONetworkSupported
    !disable InsydeNetworkPkg/Drivers/PxeDummyDxe/PxeDummyDxe.inf
    InsydeNetworkPkg/Drivers/PxeDummyDxe/PxeDummyDxe.inf {
      <SOURCE_OVERRIDE_PATH>
        $(OEM_FEATURE_OVERRIDE_CORE_CODE)/InsydeNetworkPkg/Drivers/PxeDummyDxe
    }
  !endif
!endif
