/** @file
  UI ordered list control

;******************************************************************************
;* Copyright (c) 2014 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2ODisplayEngineLocalMetro.h"
#include "UiControls.h"
#include "MetroUi.h"
#include "MetroDialog.h"
#include <Library/ConsoleLib.h>

extern H2O_FORM_BROWSER_D       *mFbDialog;

//_Start_L05_GRAPHIC_UI_
extern BOOLEAN                  mIsSendForm;

CHAR16 *mPopupDialogWithButtonChilds = L""
  L"<VerticalLayout padding='10,0,0,10' name='PopUpDialog' background-color='0xFFFFFFFF' height='324' width='817'>"
    L"<Control name='DialogShadow' background-color='0xFFE5E5E5'/>"
    L"<VerticalLayout name='DialogBorder' padding='2,2,2,2' background-color='0xFF3E8DDD' height='314' width='807' float='true'>"
      L"<VerticalLayout name='BodyLayout' padding='66,0,30,72' background-color='0xFFFFFFFF' min-height='310' height='wrap_content' width='803'>"
        L"<Label textcolor='0xFF3E8DDD' font-size='32' name='DialogTitle' height='32'/>"
        L"<Control height='34'/>"
        L"<HorizontalLayout min-height='45' height='wrap_content' width='match_parent'>"
          L"<Texture name='DialogImage' height='45' width='45' background-color='0xFFFFFFFF'/>"
          L"<Control width='15'/>"
          L"<VerticalLayout min-height='92' height='wrap_content' width='671'>"
            L"<Label visibility='true' text-align='left' padding='10,0,8,0' textcolor='0xFF000000' font-size='24' name='DialogText' min-height='34' height='wrap_content' width='match_parent'/>"
            L"<Label visibility='false' text-align='left' padding='10,0,8,0' textcolor='0xFF000000' font-size='24' name='QuestionText' height='34'/>"
          L"</VerticalLayout>"
        L"</HorizontalLayout>"
        L"<Control/>"
        L"<HorizontalLayout padding='24,0,0,0' child-padding='30' height='wrap_content' name='DialogButtonList'/>"
      L"</VerticalLayout>"
    L"</VerticalLayout>"
  L"</VerticalLayout>";

CHAR16 *mPopupDialogWithoutButtonChilds = L""
  L"<VerticalLayout padding='10,0,0,10' name='PopUpDialog' background-color='0xFFFFFFFF' height='284' width='817'>"
    L"<Control name='DialogShadow' background-color='0xFFE5E5E5'/>"
    L"<VerticalLayout name='DialogBorder' padding='2,2,2,2' background-color='0xFF3E8DDD' height='274' width='807' float='true'>"
      L"<VerticalLayout name='BodyLayout' padding='66,0,30,72' background-color='0xFFFFFFFF' min-height='270' height='wrap_content' width='803'>"
        L"<Label textcolor='0xFF3E8DDD' font-size='32' name='DialogTitle' height='32'/>"
        L"<Control height='15'/>"
        L"<HorizontalLayout min-height='45' height='wrap_content' width='match_parent'>"
          L"<Texture name='DialogImage' height='45' width='45' background-color='0xFFFFFFFF'/>"
          L"<Control width='15'/>"
          L"<VerticalLayout min-height='92' height='wrap_content' width='671'>"
            L"<Label visibility='true' text-align='left' padding='10,0,8,0' textcolor='0xFF000000' font-size='24' name='DialogText' min-height='34' height='wrap_content' width='match_parent'/>"
            L"<Label visibility='true' text-align='left' padding='10,0,8,0' textcolor='0xFF000000' font-size='24' name='QuestionText' height='34' text='Please press any key to continue...'/>"
          L"</VerticalLayout>"
        L"</HorizontalLayout>"
      L"</VerticalLayout>"
    L"</VerticalLayout>"
  L"</VerticalLayout>";
//_End_L05_GRAPHIC_UI_

EFI_STATUS
PopupDialogInit (
  IN UI_DIALOG                             *Dialog
  )
{
  EFI_STATUS                               Status;
  UI_CONTROL                               *Control;
  UI_CONTROL                               *DialogControl;
  UI_CONTROL                               *FocusControl;
  UINT32                                   Index;
  CHAR16                                   ButtonWidthStr[20];
  CHAR16                                   *BodyString;
  INTN                                     Result;
//_Start_L05_GRAPHIC_UI_
  UI_CONTROL                               *Child;
  CHAR16                                   AttributeStr[100];
//_End_L05_GRAPHIC_UI_

  if (mFbDialog->TitleString != NULL) {
    Control = UiFindChildByName (Dialog, L"DialogTitle");
    UiSetAttribute (Control, L"text", mFbDialog->TitleString);
  }

//_Start_L05_GRAPHIC_UI_
  if (!mIsSendForm) {
    Control = UiFindChildByName (Dialog, L"PopUpDialog");
    if (Control != NULL) {
      UiApplyAttributeList (Control, L"padding='0,0,0,0' background-color='0xFF000000'");
      Child = UiFindChildByName (Dialog, L"DialogBorder");
      if (Child != NULL) {
        UnicodeSPrint (
          AttributeStr,
          sizeof (AttributeStr),
          L"%d",
          Child->FixedSize.cx
          );
        UiSetAttribute (Control, L"width", AttributeStr);
      }
    }
    Control = UiFindChildByName (Dialog, L"DialogShadow");
    if (Control != NULL) {
      UiSetAttribute (Control, L"visibility", L"false");
    }
  }

  if (Dialog->XmlBuffer == mPopupDialogWithButtonChilds) {
    if (PcdGetBool (PcdL05SetupConfirmDialogFlag) || PcdGetBool (PcdL05SetupNoticeDialogFlag)) {
      Control = UiFindChildByName (Dialog, L"DialogImage");
      UiSetAttribute (Control, L"background-image", L"@L05DialogQuestion");
    } else if (PcdGetBool (PcdL05SetupWarningDialogFlag) || PcdGetBool (PcdL05SetupErrorDialogFlag) || PcdGetBool (PcdL05SetupFailedDialogFlag)) {
      Control = UiFindChildByName (Dialog, L"DialogImage");
      UiSetAttribute (Control, L"background-image", L"@L05Failed");
    } else {
      Control = UiFindChildByName (Dialog, L"DialogImage");
      UiSetAttribute (Control, L"background-image", L"@L05DialogQuestion");

      //
      // Other pop-up dialogs with buttons will show QuestionText's content
      //
      Control = UiFindChildByName (Dialog, L"QuestionText");
      UiSetAttribute (Control, L"visibility", L"true");
    }
  } else if (Dialog->XmlBuffer == mPopupDialogWithoutButtonChilds) {
    if (PcdGetBool (PcdL05SetupSucessDialogFlag)) {
      Control = UiFindChildByName (Dialog, L"DialogImage");
      UiSetAttribute (Control, L"background-image", L"@L05Success");
    } else if (PcdGetBool (PcdL05SetupWarningDialogFlag) || PcdGetBool (PcdL05SetupErrorDialogFlag) || PcdGetBool (PcdL05SetupFailedDialogFlag)) {
      Control = UiFindChildByName (Dialog, L"DialogImage");
      UiSetAttribute (Control, L"background-image", L"@L05Failed");
    }
    Control = UiFindChildByName (Dialog, L"QuestionText");
    UnicodeSPrint (
      AttributeStr,
      sizeof (AttributeStr),
      L"text='%s'",
      HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_SETUP_CONTINUE_STRING), NULL)
    );
    UiApplyAttributeList (Control, AttributeStr);

    if (FeaturePcdGet (PcdL05HiddenDialogPressContinueString)) {
      UiSetAttribute (Control, L"visibility", L"false");
    }
  }
//_End_L05_GRAPHIC_UI_

  if (mFbDialog->BodyStringArray != NULL) {
    Control = UiFindChildByName (Dialog, L"DialogText");
    BodyString = CatStringArray (mFbDialog->BodyStringCount, (CONST CHAR16 **) mFbDialog->BodyStringArray);
    if (BodyString != NULL) {
      UiSetAttribute (Control, L"text", BodyString);
      FreePool (BodyString);
    }
  }

  if (mFbDialog->ButtonCount == 0) {
    return EFI_SUCCESS;
  }

  DialogControl = UiFindChildByName (Dialog, L"DialogButtonList");

  Control = CreateControl (L"Control", DialogControl);
  CONTROL_CLASS(DialogControl)->AddChild (DialogControl, Control);

  UnicodeSPrint (ButtonWidthStr, sizeof (ButtonWidthStr), L"%d", GetButtonWidthByStr ());
  FocusControl = NULL;
  for (Index = 0; Index < mFbDialog->ButtonCount; Index++) {
    if (mFbDialog->ButtonStringArray[Index] == NULL) {
      continue;
    }

    Control = CreateControl (L"Button", DialogControl);
    UiSetAttribute (Control, L"text",  mFbDialog->ButtonStringArray[Index]);
    SetWindowLongPtr (Control->Wnd, GWLP_USERDATA, (INTN) Index);

//_Start_L05_GRAPHIC_UI_
//    UiApplyAttributeList (Control, L"name='Button' height='30' font-size='19' textcolor='0xFFFFFFFF' text-align='center' text-align='singleline' background-color='0xFFCCCCCC' focusbkcolor='@menulightcolor'");
//    if (PcdGet32(PcdH2OLmdeMultiLayout) == 1) {
//      UiApplyAttributeList (Control, L"background-color='0x0' focustextcolor='0xFF404040'");
//    }
//    UiSetAttribute (Control, L"width", ButtonWidthStr);
    UiApplyAttributeList (Control, L"name='Button' height='42' font-size='22' textcolor='0xFF000000' text-align='center|singleline' background-color='0xFFE5E5E5' focusbkcolor='0xFF3E8DDD'");
    UiSetAttribute (Control, L"width", L"130");
//_End_L05_GRAPHIC_UI_

    Status = CompareHiiValue (&mFbDialog->ButtonHiiValueArray[Index], &mFbDialog->ConfirmHiiValue, &Result);
    if (!EFI_ERROR(Status) && Result == 0) {
      FocusControl = Control;
    }
    CONTROL_CLASS(DialogControl)->AddChild (DialogControl, Control);
  }
  if (FocusControl != NULL) {
    SetFocus (FocusControl->Wnd);
  }

  Control = CreateControl (L"Control", DialogControl);
//_Start_L05_GRAPHIC_UI_
  UiSetAttribute (Control, L"width", L"42"); // 72 - 30(ChildPadding) = 42
//_End_L05_GRAPHIC_UI_
  CONTROL_CLASS(DialogControl)->AddChild (DialogControl, Control);
  return EFI_SUCCESS;
}

INTN
H2OPopupDialogProc (
  HWND         Wnd,
  UINT         Msg,
  WPARAM       WParam,
  LPARAM       lParam
  )
{
  UI_DIALOG                                *Dialog;
  UI_CONTROL                               *Control;
  UINTN                                    Index;
//_Start_L05_GRAPHIC_UI_
  SIZE                                     ContentSize;
  CHAR16                                   Str[20];
  RECT                                     ScreenRect;
  UI_CONTROL                               *PopDialog;
  SIZE                                     DialogSize;
//_End_L05_GRAPHIC_UI_

  Dialog = (UI_DIALOG *) GetWindowLongPtr (Wnd, 0);

  switch (Msg) {

  case UI_NOTIFY_WINDOWINIT:
//_Start_L05_GAMING_UI_ENABLE_
    if (PcdGetBool (PcdL05GamingSetupExitDialogFlag)) {
      L05GamingExitDialogInit (Dialog);
    } else {
//_End_L05_GAMING_UI_ENABLE_
    PopupDialogInit (Dialog);
//_Start_L05_GAMING_UI_ENABLE_
    }
//_End_L05_GAMING_UI_ENABLE_

//_Start_L05_GRAPHIC_UI_
    //
    // Adjust dialog's height dynamically
    //
    if (!PcdGetBool (PcdL05GamingSetupExitDialogFlag)) {
      Control = UiFindChildByName (Dialog, L"BodyLayout");
      if (Control != NULL) {
        ContentSize.cx = Control->FixedSize.cx;
        ContentSize.cy = 9999;
        ContentSize = CONTROL_CLASS (Control)->EstimateSize (Control, ContentSize);
        ContentSize.cy = MAX (ContentSize.cy, Control->MinSize.cy);

        Control = UiFindChildByName (Dialog, L"PopUpDialog");
        if (Control != NULL) {
          UnicodeSPrint (Str, sizeof (Str), L"%d", ContentSize.cy + (mIsSendForm ? 14 : 4));
          UiSetAttribute (Control, L"height", Str);
        }
        Control = UiFindChildByName (Dialog, L"DialogBorder");
        if (Control != NULL) {
          UnicodeSPrint (Str, sizeof (Str), L"%d", ContentSize.cy + 4);
          UiSetAttribute (Control, L"height", Str);
        }
      }
    }

    //
    // Move window position
    //
    GetWindowRect (gWnd, &ScreenRect);
    PopDialog = UiFindChildByName (Dialog, L"PopUpDialog");
    DialogSize = PopDialog->FixedSize;
    SetWindowPos (
      Wnd,
      HWND_TOP,
      (ScreenRect.right  / 2) - (DialogSize.cx / 2) - 1,
      (ScreenRect.bottom / 2) - (DialogSize.cy / 2),
      DialogSize.cx,
      DialogSize.cy,
      0
      );
//_End_L05_GRAPHIC_UI_
    break;

  case UI_NOTIFY_CLICK:
    Control = (UI_CONTROL *) WParam;
    Index = (UINTN) GetWindowLongPtr (Control->Wnd, GWLP_USERDATA);
    SendChangeQNotify (0, 0, &mFbDialog->ButtonHiiValueArray[Index]);
    break;

//_Start_L05_GRAPHIC_UI_
  case WM_KEYDOWN:
    if (Dialog->XmlBuffer == mPopupDialogWithoutButtonChilds) {
      SendShutDNotify ();
      return 0;
    }
    break;
//_End_L05_GRAPHIC_UI_

  case WM_HOTKEY:
    if (HIWORD(lParam) == VK_ESCAPE) {
      SendShutDNotify ();
      return 0;
    }
    return 1;

  case WM_DESTROY:
    FreeDialogEvent (&mFbDialog);
    return 0;

  default:
    return 0;
  }

  return 1;
}
