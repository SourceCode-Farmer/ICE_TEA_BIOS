/** @file
  Entry point and initial functions for H2O local Metro display engine driver

;******************************************************************************
;* Copyright (c) 2013 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "H2ODisplayEngineLocalMetro.h"
#include "UiControls.h"
#include "MetroDialog.h"
#include "MetroUi.h"
#include <Library/ConsoleLib.h>
//_Start_L05_SETUP_MENU_
#include <L05Config.h>
#include <Protocol/L05SetupMenu.h>
#include <Library/FeatureLib/OemSvcGenerateBeep.h>
#include <Library/FeatureLib/OemSvcOverridePasswordKeyboardBeep.h>
//_End_L05_SETUP_MENU_

//_Start_L05_SETUP_MENU_
BOOLEAN                                 mL05GraphicPasswordInput = FALSE;
#define L05_INTERRUPT_MENU_TIMER_ID     1
#define L05_SECURITY_ERASE_TIMER_ID     2
#define L05_PROGRESS_RING_TIMER_ID      3

UINT64                                  mSecurityEraseProcessingMicroseconds;
UINTN                                   mPercentage = 0;
CHAR16                                  *mCountFormatString = L05_SECURITY_ERASE_PERCENTAGE_FORMAT_STRING;
CHAR16                                  *mCountString = NULL;
UINTN                                   mCountStringSize = 0;
//_End_L05_SETUP_MENU_

#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
#define L05_INTERRUPT_MENU_COUNT_DOWN_VALUE 15
#define FORCE_REFRESH_PANEL               TRUE
#define NO_FORCE_REFRESH_PANEL            FALSE

typedef enum {
  L05InterruptMenuNormalBoot,
  L05InterruptMenuSetup,
  L05InterruptMenuRegulatoryInfo,
  L05InterruptMenuDiagnostics,
  L05InterruptMenuOneKey,
  L05InterruptMenuBootManager,
} L05_INTERRUPT_MENU_OPTION;

BOOLEAN  mPauseMenuCountdown = FALSE;

CHAR16 *mL05InterruptMenuPopupDialogChilds = L""
  L"<VerticalLayout padding='10,0,0,10' name='PopUpDialog' background-color='0xFF000000' height='324' width='677'>"
    L"<Control background-color='0xFFE5E5E5' name='DialogShadow'/>"
    L"<VerticalLayout name='DialogBorder' padding='2,2,2,2' background-color='0xFF3E8DDD' height='314' width='667' float='true'>"
      L"<VerticalLayout name='BodyLayout' padding='66,0,30,72' background-color='0xFFFFFFFF' min-height='310' height='wrap_content' width='663'>"
        L"<Label textcolor='0xFF3E8DDD' font-size='32' name='DialogTitle' height='32'/>"
        L"<Control height='34'  name='L05InterruptMenu'/>"
        L"<HorizontalLayout min-height='45' height='wrap_content' width='591'>"
          L"<VerticalLayout min-height='0' height='wrap_content' width='531'>"
            L"<Label visibility='true' text-align='left' padding='10,0,0,0' textcolor='0xFF000000' font-size='24' name='DialogText' min-height='34' height='wrap_content' width='match_parent' show_html_text='true'/>"
            L"<VerticalLayout name='Draw15SecsCountDownLayout' width='531' height='24'>"
              L"<HorizontalLayout width='match_parent' background-color='0xFFBFBFBF' float='true'>"
                L"<HorizontalLayout name='Draw15SecsCountDownRunningBar' width='0' background-color='0xFF4F81BD' visibility='false'/>"
                L"<HorizontalLayout/>"
              L"</HorizontalLayout>"
              L"<HorizontalLayout width='match_parent' float='true'>"
                L"<HorizontalLayout/>"
                L"<Label textcolor='0xFF000000' font-size='24' name='Draw15SecsCountDownValue' width='30'/>"
                L"<HorizontalLayout width='10'/>"
              L"</HorizontalLayout>"
            L"</VerticalLayout>"
          L"</VerticalLayout>"
        L"</HorizontalLayout>"
        L"<Control/>"
        L"<HorizontalLayout padding='24,82,0,0' child-padding='30' height='wrap_content' name='DialogButtonList'/>"
      L"</VerticalLayout>"
    L"</VerticalLayout>"
  L"</VerticalLayout>";

EFI_STATUS
UpdateOwnerDrawText (
  IN UI_CONTROL                            *PanelControl,
  IN CHAR16                                *ControlName,
  IN CHAR16                                *TextStr
  );
//_End_L05_INTERRUPT_MENU_
#endif

//_Start_L05_SETUP_MENU_
CHAR16 *mL05SecurityEraseChilds = L""
  L"<VerticalLayout padding='10,0,0,10' name='PopUpDialog' background-color='0xFFFFFFFF' height='284' width='817'>"
    L"<Control name='DialogShadow' background-color='0xFFE5E5E5'/>"
    L"<VerticalLayout name='DialogBorder' padding='2,2,2,2' background-color='0xFF3E8DDD' height='274' width='807' float='true'>"
      L"<VerticalLayout name='BodyLayout' padding='66,0,30,72' background-color='0xFFFFFFFF' min-height='270' height='wrap_content' width='803'>"
        L"<Label textcolor='0xFF3E8DDD' font-size='32' name='DialogTitle' height='32'/>"
        L"<Control height='15'/>"
        L"<HorizontalLayout min-height='45' height='wrap_content' width='match_parent'>"
          L"<Texture name='DialogImage' height='45' width='45' background-color='0xFFFFFFFF'/>"
          L"<Control width='15'/>"
          L"<VerticalLayout min-height='92' height='wrap_content' width='671'>"
            L"<Label visibility='true' text-align='left' padding='10,0,8,0' textcolor='0xFF000000' font-size='24' name='DialogText' min-height='34' height='100' width='match_parent'/>"
            L"<HorizontalLayout height='34' width='match_parent'>"
              L"<Label width='200'/>"
              L"<Texture name='ProgressRing' height='32' width='32' background-image='@ProgressRing1'/>"
              L"<Label visibility='true' text-align='left' padding='4,0,4,0' textcolor='0xFF000000' font-size='24' name='DialogText2' height='34'/>"
            L"</HorizontalLayout>"
            L"<Label visibility='true' text-align='left' padding='10,0,8,0' textcolor='0xFF000000' font-size='24' name='QuestionText' height='34'/>"
          L"</VerticalLayout>"
          L"<Control width='15'/>"
        L"</HorizontalLayout>"
      L"</VerticalLayout>"
    L"</VerticalLayout>"
  L"</VerticalLayout>";

CHAR16                                  *mProgressRingMetroList[] = {
  L"@ProgressRing1",
  L"@ProgressRing2",
  L"@ProgressRing3",
  L"@ProgressRing4",
};

UINTN                                   mProgressRingMetroListCount = sizeof (mProgressRingMetroList) / sizeof (CHAR16 *);
UINTN                                   mProgressRingMetroIndex = 0;
//_End_L05_SETUP_MENU_

extern H2O_DISPLAY_ENGINE_METRO_PRIVATE_DATA      *mMetroPrivate;
extern HWND                                       gWnd;
extern EFI_ABSOLUTE_POINTER_STATE                 mPreviousAbsPtrState;

HWND                            mDialogWnd = NULL;
HWND                            mOverlayWnd = NULL;
H2O_FORM_BROWSER_D              *mFbDialog;
BOOLEAN                         mTitleVisible;
BOOLEAN                         mIsSendForm;

//_Start_L05_SETUP_MENU_
VOID
EFIAPI
L05SecurityEraseMetroPercentageTimerEvent (
  HWND                                  Wnd
  )
{
  UI_DIALOG                             *Dialog;
  UI_CONTROL                            *Control;

  if (mPercentage <= 100) {
    Dialog = (UI_DIALOG *) GetWindowLongPtr (Wnd, 0);
    Control = UiFindChildByName (Dialog, L"DialogText2");
    UnicodeSPrint (mCountString, mCountStringSize, mCountFormatString, mPercentage);
    UiSetAttribute (Control, L"text", mCountString);

    RegisterTimerEvent (L05_SECURITY_ERASE_TIMER_ID, EFI_TIMER_PERIOD_MICROSECONDS (mSecurityEraseProcessingMicroseconds));
    mPercentage++;

  } else {
    mPercentage = 0;
    FreePool (mCountString);
    SendShutDNotify ();
  }

  return;
}

VOID
EFIAPI
L05SecurityEraseMetroProgressRingTimerEvent (
  HWND                                  Wnd
  )
{
  UI_DIALOG                             *Dialog;
  UI_CONTROL                            *Control;

  if (mProgressRingMetroIndex == mProgressRingMetroListCount) {
    mProgressRingMetroIndex = 0;
  }

  if (mPercentage <= 100) {
    Dialog = (UI_DIALOG *) GetWindowLongPtr (Wnd, 0);
    Control = UiFindChildByName (Dialog, L"ProgressRing");
    UiSetAttribute (Control, L"background-image", mProgressRingMetroList[mProgressRingMetroIndex]);
    RegisterTimerEvent (L05_PROGRESS_RING_TIMER_ID, EFI_TIMER_PERIOD_MILLISECONDS (L05_SECURITY_ERASE_PROGRESS_RING_MICROSECOND));
    mProgressRingMetroIndex++;
  }

  return;
}
//_End_L05_SETUP_MENU_

VOID
H2OCommonDialogWithHelpOnSetState (
  UI_CONTROL                    *Control,
  UI_STATE                      SetState,
  UI_STATE                      ClearState
  )
{
  UI_CONTROL                    *Child;


  if (SetState & UISTATE_FOCUSED) {
    Child = UiFindChildByName (Control, L"DialogText");
    UiSetAttribute (Child, L"textcolor", L"0xFFFFFFFF");
  }
  if (ClearState & UISTATE_FOCUSED) {
    Child = UiFindChildByName (Control, L"DialogText");
    UiSetAttribute (Child, L"textcolor", L"0xFFA8D2DF");
  }
}

#if FixedPcdGet32(PcdH2OLmdeMultiLayout) == 0
CHAR16 *mCommonDialogChilds = L""
  L"<VerticalLayout padding='40,30,40,30' background-color='@menucolor' name='PopUpDialog'>"
    L"<VerticalLayout name='TitleLayout'>"
      L"<Label padding='0,0,30,0' textcolor='0xFFFFFFFF' font-size='29' name='DialogTitle' height='110'/>"
      L"<Label padding='0,0,45,0' textcolor='0xFFA8D2DF' font-size='19' name='DialogText'/>"
    L"</VerticalLayout>"
    L"<Control background-image='@DialogSeparator' background-color='0x0' name='DialogSeparator' height='10'/>"
    L"<VerticalLayout>"
      L"<HorizontalLayout padding='10,0,0,0' visibility='false' name='DialogPasswordInput' height='51'>"
        L"<Label name='ConfirmNewPasswordLabel' height='41' width='119' font-size='16' background-color='0xFFF2F2F2' textcolor='0xFF4D4D4D'/>"
        L"<VerticalLayout padding='6,8,6,8' background-color='0xFFF2F2F2' height='41'>"
          L"<Control padding='1,1,1,1' background-color='0xFF999999' height='29'>"
            L"<UiEdit name='ConfirmPasswordInput' focusbkcolor='@menulightcolor' height='27' padding='7,3,0,3' background-color='0xFFF2F2F2' password='true'/>"
          L"</Control>"
        L"</VerticalLayout>"
      L"</HorizontalLayout>"
      L"<HorizontalLayout padding='10,0,0,0' child-padding='2' name='DialogButtonList'/>"
    L"</VerticalLayout>"
  L"</VerticalLayout>";

CHAR16 *mCommonDialogWithHelpChilds = L""
  L"<VerticalLayout padding='40,30,40,30' width='-1' float='true' name='HelpMenu' height='-1' background-color='@menucolor' name='Dialog'>"
    L"<VerticalLayout name='TitleLayout'>"
      L"<HorizontalLayout padding='0,0,8,0' min-height='50' height='wrap_content'>"
        L"<Label width='200' textcolor='0xFFFFFFFF' font-size='29' name='DialogTitle' height='wrap_content'/>"
        L"<Control padding='0,25,0,25' height='50'>"
          L"<Texture width='50' height='50' name='DialogImage' background-image-style='stretch|light'/>"
        L"</Control>"
      L"</HorizontalLayout>"
      L"<ScrollView taborder='20' name='DialogTextScrollView' vscrollbar='true' scale9grid='1,13,1,13'>"
        L"<Label name='DialogText' height='wrap_content' width='match_parent' font-size='19' textcolor='0xFFA8D2DF'/>"
      L"</ScrollView>"
    L"</VerticalLayout>"
    L"<VerticalLayout>"
      L"<Control background-image='@DialogSeparator' background-color='0x0' height='2'/>"
      L"<Control height='5'/>"
      L"<Control name='Content'/>"
    L"</VerticalLayout>"
  L"</VerticalLayout>";

CHAR16 *mDialogWithoutSendFormChilds = L""
  L"<VerticalLayout name='DialogWithoutSendForm'>"
    L"<Control/>"
    L"<Control background-color='@menucolor' name='parent' height='wrap_content'>"
      L"<VerticalLayout padding='20,30,20,30' background-color='@menucolor' height='wrap_content'>"
        L"<VerticalLayout name='TitleLayout' height='wrap_content'>"
          L"<Label name='DialogTitle' text-align='center' height='40' visibility='false' font-size='19' textcolor='0xFFFFFFFF'/>"
          L"<Label width='match_parent' textcolor='0xFFFFFFFF' font-size='19' name='DialogText' height='wrap_content'/>"
          L"<Control height='15'/>"
          L"<Control height='10' background-image='@DialogSeparator' background-color='0x0' background-image-style='center'/>"
          L"<Control height='15'/>"
        L"</VerticalLayout>"
        L"<VerticalLayout padding='0,0,10,0' visibility='false' min-height='51' name='DialogPasswordInput' height='wrap_content'>"
          L"<Label name='ConfirmNewPasswordLabel' height='wrap_content' padding='0,0,2,0' width='match_parent' font-size='18' textcolor='0xFFFFFFFF'/>"
          L"<Control padding='2,2,2,2' background-color='0xFFCCCCCC' height='wrap_content'>"
            L"<UiEdit name='ConfirmPasswordInput' focusbkcolor='0xFFFFFFFF' tabstop='true' height='27' padding='7,3,0,3' taborder='1' background-color='@menulightcolor' password='true'/>"
          L"</Control>"
        L"</VerticalLayout>"
        L"<HorizontalLayout padding='1,0,0,0' child-padding='2' visibility='false' name='DialogButtonList' height='32'/>"
      L"</VerticalLayout>"
      L"<Texture name='FormHalo' float='true' height='-1' width='-1' background-image='@FormHalo' scale9grid='23,26,22,31'/>"
    L"</Control>"
    L"<Control/>"
  L"</VerticalLayout>";
#endif

//_Start_L05_GRAPHIC_UI_
VOID
MoveDialogPosition (
  HWND                          Wnd,
  UI_DIALOG                     *Dialog
  )
{
  UI_CONTROL                    *Control;
  SIZE                          ContentSize;
  CHAR16                        Str[20];
  RECT                          ScreenRect;
  UI_CONTROL                    *PopDialog;
  SIZE                          DialogSize;

  //
  // Adjust dialog's height dynamically
  //
  Control = UiFindChildByName (Dialog, L"BodyLayout");
  if (Control != NULL) {
    ContentSize.cx = Control->FixedSize.cx;
    ContentSize.cy = 9999;
    ContentSize = CONTROL_CLASS (Control)->EstimateSize (Control, ContentSize);
    ContentSize.cy = MAX (ContentSize.cy, Control->MinSize.cy);

    Control = UiFindChildByName (Dialog, L"PopUpDialog");
    if (Control != NULL) {
      UnicodeSPrint (Str, sizeof (Str), L"%d", ContentSize.cy + (mIsSendForm ? 14 : 4));
      UiSetAttribute (Control, L"height", Str);
    }
    Control = UiFindChildByName (Dialog, L"DialogBorder");
    if (Control != NULL) {
      UnicodeSPrint (Str, sizeof (Str), L"%d", ContentSize.cy + 4);
      UiSetAttribute (Control, L"height", Str);
    }
  }

  //
  // Move window position
  //
  GetWindowRect (gWnd, &ScreenRect);
  PopDialog = UiFindChildByName (Dialog, L"PopUpDialog");
  DialogSize = PopDialog->FixedSize;
  SetWindowPos (
    Wnd,
    HWND_TOP,
    (ScreenRect.right  / 2) - (DialogSize.cx / 2) - 1,
    (ScreenRect.bottom / 2) - (DialogSize.cy / 2),
    DialogSize.cx,
    DialogSize.cy,
    0
    );
}

#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
VOID
Draw15SecsCountDownFunc (
  IN UI_CONTROL                            *PanelControl,
  IN BOOLEAN                               ForceRefresh
  )
{
  CHAR16                                   String[3];
  STATIC UINT8                             LeftSeconds = L05_INTERRUPT_MENU_COUNT_DOWN_VALUE;
  UI_CONTROL                               *Child;
  UINT32                                   Value;

  if (!mPauseMenuCountdown) {

    if (LeftSeconds < L05_INTERRUPT_MENU_COUNT_DOWN_VALUE) {
      Value = 0;
      Child = UiFindChildByName (PanelControl, L"Draw15SecsCountDownLayout");

      if (Child != NULL) {
        Value = (UINT32) Child->FixedSize.cx;
      }

      Child = UiFindChildByName (PanelControl, L"Draw15SecsCountDownRunningBar");

      if (Value > 0 && Child != NULL) {
        UiSetAttribute (Child, L"visibility", L"true");
        Value /= L05_INTERRUPT_MENU_COUNT_DOWN_VALUE;
        Value *= (L05_INTERRUPT_MENU_COUNT_DOWN_VALUE - LeftSeconds);
        UiSetAttributeEx (Child, L"width", L"%d", Value);
      }
    }

    if (LeftSeconds > 0) {
      UnicodeSPrint (String, sizeof (String), L"%02d", LeftSeconds);
      UpdateOwnerDrawText (PanelControl, L"Draw15SecsCountDownValue", String);
      LeftSeconds--;

    } else if (LeftSeconds == 0) {
      SendShutDNotify ();
    }
  }
}
//_End_L05_INTERRUPT_MENU_
#endif
//_End_L05_GRAPHIC_UI_

VOID
FreeDialogEvent (
  IN H2O_FORM_BROWSER_D         **DialogEvt
  )
{
  H2O_FORM_BROWSER_D                       *Event;

  if (DialogEvt == NULL || *DialogEvt == NULL) {
    return;
  }

  Event = *DialogEvt;

  if (Event->TitleString != NULL) {
    FreePool (Event->TitleString);
  }

  if (Event->BodyStringArray != NULL && Event->BodyStringCount != 0) {
    FreeStringArray (Event->BodyStringArray, Event->BodyStringCount);
  }

  if (Event->BodyInputStringArray != NULL && Event->BodyInputCount != 0) {
    FreeStringArray (Event->BodyInputStringArray, Event->BodyInputCount);
  }

  if (Event->ButtonStringArray != NULL && Event->ButtonCount != 0) {
    FreeStringArray (Event->ButtonStringArray, Event->ButtonCount);
  }

  if (Event->BodyHiiValueArray != NULL && Event->BodyStringCount != 0) {
    FreeHiiValueArray (Event->BodyHiiValueArray, Event->BodyStringCount);
  }

  if (Event->ButtonHiiValueArray != NULL && Event->ButtonCount != 0) {
    FreeHiiValueArray (Event->ButtonHiiValueArray, Event->ButtonCount);
  }

  if (Event->ConfirmHiiValue.Type == EFI_IFR_TYPE_BUFFER && Event->ConfirmHiiValue.Buffer != NULL && Event->ConfirmHiiValue.BufferLen != 0) {
    FreePool (Event->ConfirmHiiValue.Buffer);
  }

  FreePool (Event);

  *DialogEvt = NULL;
}

H2O_FORM_BROWSER_D *
CopyDialogEvent (
  IN H2O_FORM_BROWSER_D                    *SrcDialogEvt
  )
{
  H2O_FORM_BROWSER_D                       *DialogEvt;

  if (SrcDialogEvt == NULL) {
    return NULL;
  }

  DialogEvt = AllocateCopyPool (sizeof (H2O_FORM_BROWSER_D), SrcDialogEvt);
  if (DialogEvt == NULL) {
    return NULL;
  }

  if (SrcDialogEvt->TitleString != NULL) {
    DialogEvt->TitleString = AllocateCopyPool (StrSize (SrcDialogEvt->TitleString), SrcDialogEvt->TitleString);
    if (DialogEvt->TitleString == NULL) {
      goto Error;
    }
  }

  if (SrcDialogEvt->BodyStringArray != NULL && SrcDialogEvt->BodyStringCount != 0) {
    DialogEvt->BodyStringArray = CopyStringArray (SrcDialogEvt->BodyStringArray, SrcDialogEvt->BodyStringCount);
    if (DialogEvt->BodyStringArray == NULL) {
      goto Error;
    }
  }

  if (SrcDialogEvt->BodyInputStringArray != NULL && SrcDialogEvt->BodyInputCount != 0) {
    DialogEvt->BodyInputStringArray = CopyStringArray (SrcDialogEvt->BodyInputStringArray, SrcDialogEvt->BodyInputCount);
    if (DialogEvt->BodyInputStringArray == NULL) {
      goto Error;
    }
  }

  if (SrcDialogEvt->ButtonStringArray != NULL && SrcDialogEvt->ButtonCount != 0) {
    DialogEvt->ButtonStringArray = CopyStringArray (SrcDialogEvt->ButtonStringArray, SrcDialogEvt->ButtonCount);
    if (DialogEvt->ButtonStringArray == NULL) {
      goto Error;
    }
  }

  if (SrcDialogEvt->BodyHiiValueArray != NULL && SrcDialogEvt->BodyStringCount != 0) {
    DialogEvt->BodyHiiValueArray = CopyHiiValueArray (SrcDialogEvt->BodyHiiValueArray, SrcDialogEvt->BodyStringCount);
    if (DialogEvt->BodyHiiValueArray == NULL) {
      goto Error;
    }
  }

  if (SrcDialogEvt->ButtonHiiValueArray != NULL && SrcDialogEvt->ButtonCount != 0) {
    DialogEvt->ButtonHiiValueArray = CopyHiiValueArray (SrcDialogEvt->ButtonHiiValueArray, SrcDialogEvt->ButtonCount);
    if (DialogEvt->ButtonHiiValueArray == NULL) {
      goto Error;
    }
  }

  if (SrcDialogEvt->ConfirmHiiValue.Type == EFI_IFR_TYPE_BUFFER && SrcDialogEvt->ConfirmHiiValue.Buffer != NULL && SrcDialogEvt->ConfirmHiiValue.BufferLen != 0) {
    DialogEvt->ConfirmHiiValue.Buffer = AllocateCopyPool (SrcDialogEvt->ConfirmHiiValue.BufferLen, SrcDialogEvt->ConfirmHiiValue.Buffer);
    if (DialogEvt->ConfirmHiiValue.Buffer == NULL) {
      goto Error;
    }
  }

  return DialogEvt;

Error:
  FreeDialogEvent (&DialogEvt);
  return NULL;
}

HWND
CreateModalDialog (
  HINSTANCE                     Instance,
  CHAR16                        *XmlBuffer,
  HWND                          ParentWnd,
  WNDPROC                       DialogProc,
  LPARAM                        Param,
  INT32                         X,
  INT32                         Y,
  INT32                         Width,
  INT32                         Height,
  BOOLEAN                       CloseDlgWhenTouchOutside
  )
{
  UI_DIALOG                     *DialogData;
  HWND                          Dlg;
  UI_CONTROL                    *Control;
  UI_MANAGER                    *Manager;

  DialogData = AllocateZeroPool (sizeof (UI_DIALOG));
  if (DialogData == NULL) {
    return NULL;
  }
  DialogData->Instance          = Instance;
  DialogData->ParentWnd         = ParentWnd;
  DialogData->Proc              = DialogProc;
  DialogData->Param             = Param;
  DialogData->XmlBuffer         = XmlBuffer;
  DialogData->Running           = TRUE;
  DialogData->CloseDlgWhenTouchOutside = CloseDlgWhenTouchOutside;

  Dlg = CreateWindowEx (
          WS_EX_NOACTIVATE, L"DIALOG", L"", WS_VISIBLE | WS_POPUP,
          X, Y, Width, Height, ParentWnd, NULL, Instance, DialogData
          );
//  ASSERT (Dlg != NULL);
  if (Dlg == NULL) {
    return NULL;
  }

  Control = (UI_CONTROL *) (UINTN) GetWindowLongPtr (Dlg, 0);
  Manager = Control->Manager;

  SendMessage (Manager->MainWnd, UI_NOTIFY_WINDOWINIT, (WPARAM)Manager->Root, 0);

  return Dlg;
}

BOOLEAN
ClickOnOutsideOfDlgRegion (
  IN EFI_ABSOLUTE_POINTER_STATE               *AbsPtrState
  )
{
  RECT                                        DlgRect;
  POINT                                       Pt;

  ASSERT (mDialogWnd != NULL);
  ASSERT (AbsPtrState != NULL);

  if (GetCapture () != NULL) {
    return FALSE;
  }

  if (AbsPtrState->ActiveButtons == 0) {
    return FALSE;
  }

  Pt.x = (INT32) AbsPtrState->CurrentX;
  Pt.y = (INT32) AbsPtrState->CurrentY;

  GetWindowRect (mDialogWnd, &DlgRect);
  if (PtInRect (&DlgRect, Pt)) {
    return FALSE;
  }

  return TRUE;
}

UINT32
GetButtonWidthByStr (
  VOID
  )
{
  UINT32                                   Index;
  UINT32                                   ButtonWidth;

  ButtonWidth = 0;
  for (Index = 0; Index < mFbDialog->ButtonCount; Index++) {
    if (mFbDialog->ButtonStringArray[Index] != NULL) {
      ButtonWidth = (UINT32) MAX (GetStringWidth (mFbDialog->ButtonStringArray[Index]), ButtonWidth);
    }
  }

  return (ButtonWidth * 5 + 4);
}

VOID
EnableTitle (
  HWND          Wnd,
  BOOLEAN       Enable
  )
{
  CHAR16           *Str;
  UI_DIALOG        *Dialog;
  UI_CONTROL       *Control;
  HWND             FocusedWnd;

  //
  // Prevent from button location change to cause invalid button press, NOT do
  // enable/disable password during this period.
  //
  FocusedWnd = GetFocus ();
  Control  = (UI_CONTROL *) GetWindowLongPtr (FocusedWnd, 0);
  if (Control != NULL && (StrCmp (Control->Name, L"Ok") == 0 ||
      StrCmp (Control->Name, L"Cancel") == 0) &&
      (CONTROL_CLASS_GET_STATE(Control) & UISTATE_PRESSED)) {
    return;
  }

  Dialog  = (UI_DIALOG *) GetWindowLongPtr (Wnd, 0);
  Str = Enable ? L"true" : L"false";

  Control = UiFindChildByName (Dialog, L"TitleLayout");
//_Start_L05_Graphic_UI_
//  UiSetAttribute (Control, L"visibility", Str);
  if (Control != NULL) {
    UiSetAttribute (Control, L"visibility", Str);
  }
//_End_L05_Graphic_UI_
  mTitleVisible = Enable;
  CONTROL_CLASS_INVALIDATE (Dialog);
}


LRESULT
WINAPI
DialogCallback (
  HWND Wnd,
  UINT Msg,
  WPARAM WParam,
  LPARAM LParam
  )
{
  KEYBOARD_ATTRIBUTES  KeyboardAttributes;

  if (mSetupMouse == NULL) {
    return 0;
  }

  mSetupMouse->GetKeyboardAttributes (mSetupMouse, &KeyboardAttributes);
  if (KeyboardAttributes.IsStart && mTitleVisible) {
    EnableTitle  (Wnd, FALSE);
  } else if (!KeyboardAttributes.IsStart && !mTitleVisible) {
    EnableTitle  (Wnd, TRUE);
  }
  return 0;
}


EFI_STATUS
GetDialogRegion (
  IN  CHAR16                               *PanelName,
  OUT RECT                                 *DlgRect
  )
{
  EFI_STATUS                               Status;
  RECT                                     PanelRect;
  UINT32                                   PanelWidth;
  UINT32                                   PanelHeight;
  UINT32                                   DlgWidth;
  UINT32                                   DlgHeight;

  ASSERT (PanelName != NULL);
  ASSERT (DlgRect != NULL);

  Status = GetRectByName (gWnd, PanelName, &PanelRect);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  PanelWidth  = PanelRect.right  - PanelRect.left;
  PanelHeight = PanelRect.bottom - PanelRect.top;

  if ((mFbDialog->DialogType & H2O_FORM_BROWSER_D_TYPE_QUESTIONS) == 0) {
    if ((mFbDialog->DialogType & H2O_FORM_BROWSER_D_TYPE_SELECTION) == H2O_FORM_BROWSER_D_TYPE_SELECTION) {
      DlgWidth  = 360;
      DlgHeight = PanelHeight;
    } else if ((mFbDialog->DialogType & H2O_FORM_BROWSER_D_TYPE_MSG) == H2O_FORM_BROWSER_D_TYPE_MSG) {
      DlgWidth  = 360;
      DlgHeight = PanelHeight;
    } else {
      return EFI_UNSUPPORTED;
    }
  } else {
    switch (GetOpCodeByDialogType (mFbDialog->DialogType)) {

    case EFI_IFR_ONE_OF_OP:
      DlgWidth  = PanelWidth;
      DlgHeight = PanelHeight;
      break;

    case EFI_IFR_NUMERIC_OP:
      DlgWidth  = 360;
      DlgHeight = 200;
      break;

    case EFI_IFR_PASSWORD_OP:
      DlgWidth  = 400;
      DlgHeight = PanelHeight;
      break;

    default:
      return EFI_UNSUPPORTED;
      break;
    }
  }

  DlgRect->left   = PanelRect.left + ((PanelWidth - DlgWidth) / 2);
  DlgRect->top    = PanelRect.top  + ((PanelHeight - DlgHeight) / 2);
  DlgRect->right  = DlgRect->left + DlgWidth;
  DlgRect->bottom = DlgRect->top + DlgHeight;

  return EFI_SUCCESS;
}

EFI_STATUS
SetDialogText (
  IN UI_DIALOG                             *Dialog
  )
{
  EFI_STATUS                               Status;
  UINTN                                    Index;
  UI_CONTROL                               *Control;
  UI_CONTROL                               *ListControl;
  UI_CONTROL                               *FocusControl;
  CHAR16                                   *BodyString;
  INTN                                     Result;
//_Start_L05_GRAPHIC_UI_
  UI_CONTROL                               *Child;
  CHAR16                                   AttributeStr[100];
//_End_L05_GRAPHIC_UI_

  if ((mFbDialog->DialogType & H2O_FORM_BROWSER_D_TYPE_QUESTIONS) != 0) {
    return EFI_UNSUPPORTED;
  }

  FocusControl = NULL;

//_Start_L05_GRAPHIC_UI_
  if (!mIsSendForm) {
    Control = UiFindChildByName (Dialog, L"PopUpDialog");
    if (Control != NULL) {
      UiApplyAttributeList (Control, L"padding='0,0,0,0' background-color='0xFF000000'");
      Child = UiFindChildByName (Dialog, L"DialogBorder");
      if (Child != NULL) {
        UnicodeSPrint (
          AttributeStr,
          sizeof (AttributeStr),
          L"%d",
          Child->FixedSize.cx
          );
        UiSetAttribute (Control, L"width", AttributeStr);
      }
    }
    Control = UiFindChildByName (Dialog, L"DialogShadow");
    if (Control != NULL) {
      UiSetAttribute (Control, L"visibility", L"false");
    }
  }

  if (Dialog->XmlBuffer == mPopupDialogWithButtonChilds) {
    if (PcdGetBool (PcdL05SetupConfirmDialogFlag)) {
      Control = UiFindChildByName (Dialog, L"DialogImage");
      UiSetAttribute (Control, L"background-image", L"@L05DialogQuestion");
    } else if (PcdGetBool (PcdL05SetupWarningDialogFlag) || PcdGetBool (PcdL05SetupErrorDialogFlag) || PcdGetBool (PcdL05SetupFailedDialogFlag)) {
      Control = UiFindChildByName (Dialog, L"DialogImage");
      UiSetAttribute (Control, L"background-image", L"@L05Failed");
    } else {
      Control = UiFindChildByName (Dialog, L"DialogImage");
      UiSetAttribute (Control, L"background-image", L"@L05DialogQuestion");

      //
      // Warning Dialog will Show QuestionText's content
      //
      Control = UiFindChildByName (Dialog, L"QuestionText");
      UiSetAttribute (Control, L"visibility", L"true");
    }
  } else if (Dialog->XmlBuffer == mPopupDialogWithoutButtonChilds) {
    if (PcdGetBool (PcdL05SetupSucessDialogFlag)) {
      Control = UiFindChildByName (Dialog, L"DialogImage");
      UiSetAttribute (Control, L"background-image", L"@L05Success");
    } else if (PcdGetBool (PcdL05SetupWarningDialogFlag) || PcdGetBool (PcdL05SetupErrorDialogFlag) || PcdGetBool (PcdL05SetupFailedDialogFlag)) {
      Control = UiFindChildByName (Dialog, L"DialogImage");
      UiSetAttribute (Control, L"background-image", L"@L05Failed");
    }
  }

  if (mFbDialog->TitleString != NULL) {
    Control = UiFindChildByName (Dialog, L"DialogTitle");
    UiSetAttribute (Control, L"text", mFbDialog->TitleString);
  }
//_End_L05_GRAPHIC_UI_

  if ((mFbDialog->DialogType & H2O_FORM_BROWSER_D_TYPE_SELECTION) == H2O_FORM_BROWSER_D_TYPE_SELECTION) {
    if (mFbDialog->BodyStringArray != NULL) {
      Control = UiFindChildByName (Dialog, L"DialogText");
      BodyString = CatStringArray (mFbDialog->BodyStringCount, (CONST CHAR16 **) mFbDialog->BodyStringArray);
      if (BodyString != NULL) {
        UiSetAttribute (Control, L"text", BodyString);
        if (GetSystemMetrics(SM_CYSCREEN) < 600) {
          UiSetAttribute (Control, L"font-size", L"16");
        }
        FreePool (BodyString);
      }
    }

//_Start_L05_GRAPHIC_UI_
    if (mFbDialog->ButtonCount == 0) {
      return EFI_SUCCESS;
    }
//_End_L05_GRAPHIC_UI_

    ListControl = UiFindChildByName (Dialog, L"DialogButtonList");
    UiSetAttribute (ListControl, L"visibility", L"true");

    Control = CreateControl (L"Control", ListControl);
    CONTROL_CLASS(ListControl)->AddChild (ListControl, Control);

    for (Index = 0; Index < mFbDialog->ButtonCount; Index++) {
      if (mFbDialog->ButtonStringArray[Index] == NULL) {
        continue;
      }

      Control = CreateControl (L"Button", ListControl);
      UiSetAttribute (Control, L"text",  mFbDialog->ButtonStringArray[Index]);
      SetWindowLongPtr (Control->Wnd, GWLP_USERDATA, (INTN) Index);
//_Start_L05_GRAPHIC_UI_
//      UiApplyAttributeList (Control, L"name='Button' height='30' width='75' font-size='19' textcolor='0xFFFFFFFF' text-align='center' text-align='singleline' background-color='0xFFCCCCCC' focusbkcolor='@menulightcolor'");
//      if (PcdGet32(PcdH2OLmdeMultiLayout) == 1) {
//        UiApplyAttributeList (Control, L"background-color='0x0' focustextcolor='0xFF404040'");
//      }
      UiApplyAttributeList (Control, L"name='Button' height='42' width='130' font-size='22' textcolor='0xFF000000' text-align='center|singleline' background-color='0xFFE5E5E5' focusbkcolor='0xFF3E8DDD'");
//_End_L05_GRAPHIC_UI_
      CONTROL_CLASS(ListControl)->AddChild (ListControl, Control);

      Status = CompareHiiValue (&mFbDialog->ButtonHiiValueArray[Index], &mFbDialog->ConfirmHiiValue, &Result);
      if (!EFI_ERROR(Status) && Result == 0) {
        FocusControl = Control;
      }
    }

    Control = CreateControl (L"Control", ListControl);

//_Start_L05_GRAPHIC_UI_
    UiSetAttribute (Control, L"width", L"42"); // 72 - 30(ChildPadding) = 42
//_End_L05_GRAPHIC_UI_

#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
    if (Dialog->XmlBuffer == mL05InterruptMenuPopupDialogChilds) {
      UiSetAttribute (Control, L"width", L"0");
    }
//_End_L05_INTERRUPT_MENU_
#endif

    CONTROL_CLASS(ListControl)->AddChild (ListControl, Control);

    if (FocusControl != NULL) {
      SetFocus (FocusControl->Wnd);
    }
  }

  return EFI_SUCCESS;
}


LRESULT
DialogWithoutSendFormProc (
  HWND                          Wnd,
  UINT                          Msg,
  WPARAM                        WParam,
  LPARAM                        LParam
  )
{
  UI_DIALOG                     *Dialog;
  UINTN                         Index;
  UI_CONTROL                    *Control;
#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
  EFI_HII_VALUE                 HiiValue;
//_End_L05_INTERRUPT_MENU_
#endif

  Dialog  = (UI_DIALOG *) GetWindowLongPtr (Wnd, 0);

  switch (Msg) {

  case UI_NOTIFY_WINDOWINIT:
    SetDialogText (Dialog);

//_Start_L05_GRAPHIC_UI_
    MoveDialogPosition (Wnd, Dialog);

#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
    if (Dialog->XmlBuffer == mL05InterruptMenuPopupDialogChilds) {
      Control = (UI_CONTROL *) GetWindowLongPtr (Wnd, 0);
      KillTimer (Wnd, L05_INTERRUPT_MENU_TIMER_ID);
      SetTimer (Wnd, L05_INTERRUPT_MENU_TIMER_ID, 1000, NULL);
      Draw15SecsCountDownFunc (Control, FORCE_REFRESH_PANEL);
      RegisterTimerEvent (H2O_METRO_DE_TIMER_ID_PERIODIC_TIMER, H2O_METRO_DE_TIMER_PERIODIC_TIME);
      UnregisterHotKey (Wnd, 2);
      UnregisterHotKey (Wnd, 3);
      UnregisterHotKey (Wnd, 4);
      UnregisterHotKey (Wnd, 5);
      UnregisterHotKey (Wnd, 6);
      UnregisterHotKey (Wnd, 7);
      MwRegisterHotKey (Wnd, 2,  0, VK_RETURN);
      MwRegisterHotKey (Wnd, 3,  0, VK_F1);
      MwRegisterHotKey (Wnd, 4,  0, VK_F9);
      MwRegisterHotKey (Wnd, 5,  0, VK_F10);
      MwRegisterHotKey (Wnd, 6,  0, VK_F11);
      MwRegisterHotKey (Wnd, 7,  0, VK_F12);
    }
//_End_L05_INTERRUPT_MENU_
#endif
//_End_L05_GRAPHIC_UI_
    break;

  case UI_NOTIFY_CLICK:
    Control = (UI_CONTROL *)WParam;

    if ((mFbDialog->DialogType & H2O_FORM_BROWSER_D_TYPE_SELECTION) == H2O_FORM_BROWSER_D_TYPE_SELECTION) {
      Index = (UINTN) GetWindowLongPtr (Control->Wnd, GWLP_USERDATA);
      SendChangeQNotify (0, 0, &mFbDialog->ButtonHiiValueArray[Index]);
    }
    break;

  case WM_DESTROY:
#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
    UnregisterHotKey (Wnd, 2);
    UnregisterHotKey (Wnd, 3);
    UnregisterHotKey (Wnd, 4);
    UnregisterHotKey (Wnd, 5);
    UnregisterHotKey (Wnd, 6);
    UnregisterHotKey (Wnd, 7);
//_End_L05_INTERRUPT_MENU_
#endif

    FreeDialogEvent (&mFbDialog);
    return 0;

#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
  case WM_TIMER:
    Control = UiFindChildByName (Dialog, L"L05InterruptMenu");

    if (Control != NULL) {
      Control = (UI_CONTROL *) GetWindowLongPtr (Wnd, 0);

      if (WParam == L05_INTERRUPT_MENU_TIMER_ID) {
        Draw15SecsCountDownFunc (Control, FORCE_REFRESH_PANEL);
      }
    }
    break;

  case WM_HOTKEY:
    Control = UiFindChildByName (Dialog, L"L05InterruptMenu");

    if (Control != NULL) {
      ZeroMem (&HiiValue, sizeof (HiiValue));

      switch (HIWORD (LParam)) {

      case VK_RETURN:
        mPauseMenuCountdown = !mPauseMenuCountdown;
        break;

      case VK_F1:
        HiiValue.Value.u8 = L05InterruptMenuSetup;
        SendChangeQNotify (0, 0, &HiiValue);
        break;

      case VK_F9:
        HiiValue.Value.u8 = L05InterruptMenuRegulatoryInfo;
        SendChangeQNotify (0, 0, &HiiValue);
        break;

      case VK_F10:
        HiiValue.Value.u8 = L05InterruptMenuDiagnostics;
        SendChangeQNotify (0, 0, &HiiValue);
        break;

      case VK_F11:
        HiiValue.Value.u8 = L05InterruptMenuOneKey;
        SendChangeQNotify (0, 0, &HiiValue);
        break;

      case VK_F12:
        HiiValue.Value.u8 = L05InterruptMenuBootManager;
        SendChangeQNotify (0, 0, &HiiValue);
        break;

      case VK_ESCAPE:
        HiiValue.Value.u8 = L05InterruptMenuNormalBoot;
        SendChangeQNotify (0, 0, &HiiValue);
        break;
      }
    }
    break;
//_End_L05_INTERRUPT_MENU_
#endif

  default:
    return 0;
  }

  return 1;
}


LRESULT
MsgPopUpProc (
  HWND                          Wnd,
  UINT                          Msg,
  WPARAM                        WParam,
  LPARAM                        LParam
  )
{
  UI_DIALOG                     *Dialog;
  UI_CONTROL                    *Control;
  UI_CONTROL                    *ListControl;
  UI_CONTROL                    *FocusControl;
  CHAR16                        *BodyString;
  CHAR16                        ButtonWidthStr[20];
  UINTN                         Index;
  INTN                          Result;
  EFI_STATUS                    Status;
//_Start_L05_GRAPHIC_UI_
  UI_CONTROL                    *Child;
  CHAR16                        AttributeStr[100];
//_End_L05_GRAPHIC_UI_

  Dialog  = (UI_DIALOG *) GetWindowLongPtr (Wnd, 0);

  switch (Msg) {

  case UI_NOTIFY_WINDOWINIT:

//_Start_L05_GRAPHIC_UI_
    if (!mIsSendForm) {
      Control = UiFindChildByName (Dialog, L"PopUpDialog");
      if (Control != NULL) {
        UiApplyAttributeList (Control, L"padding='0,0,0,0' background-color='0xFF000000'");
        Child = UiFindChildByName (Dialog, L"DialogBorder");
        if (Child != NULL) {
          UnicodeSPrint (
            AttributeStr,
            sizeof (AttributeStr),
            L"%d",
            Child->FixedSize.cx
            );
          UiSetAttribute (Control, L"width", AttributeStr);
        }
      }
      Control = UiFindChildByName (Dialog, L"DialogShadow");
      if (Control != NULL) {
        UiSetAttribute (Control, L"visibility", L"false");
      }
    }

    if (Dialog->XmlBuffer == mPopupDialogWithButtonChilds) {
      if (PcdGetBool (PcdL05SetupConfirmDialogFlag)) {
        Control = UiFindChildByName (Dialog, L"DialogImage");
        UiSetAttribute (Control, L"background-image", L"@L05DialogQuestion");
      } else if (PcdGetBool (PcdL05SetupWarningDialogFlag) || PcdGetBool (PcdL05SetupErrorDialogFlag) || PcdGetBool (PcdL05SetupFailedDialogFlag)) {
        Control = UiFindChildByName (Dialog, L"DialogImage");
        UiSetAttribute (Control, L"background-image", L"@L05Failed");
      } else {
        Control = UiFindChildByName (Dialog, L"DialogImage");
        UiSetAttribute (Control, L"background-image", L"@L05DialogQuestion");

        //
        // Warning Dialog will Show QuestionText's content
        //
        Control = UiFindChildByName (Dialog, L"QuestionText");
        UiSetAttribute (Control, L"visibility", L"true");
      }
    } else if (Dialog->XmlBuffer == mPopupDialogWithoutButtonChilds) {
      if (PcdGetBool (PcdL05SetupSucessDialogFlag)) {
        Control = UiFindChildByName (Dialog, L"DialogImage");
        UiSetAttribute (Control, L"background-image", L"@L05Success");
      } else if (PcdGetBool (PcdL05SetupWarningDialogFlag) || PcdGetBool (PcdL05SetupErrorDialogFlag) || PcdGetBool (PcdL05SetupFailedDialogFlag)) {
        Control = UiFindChildByName (Dialog, L"DialogImage");
        UiSetAttribute (Control, L"background-image", L"@L05Failed");
      }
      if (PcdGetBool (PcdL05PasswordErrorFlag)) {
        Control = UiFindChildByName (Dialog, L"QuestionText");
        UiSetAttribute (Control, L"visibility", L"false");
        Control = UiFindChildByName (Dialog, L"DialogImage");
        UiSetAttribute (Control, L"background-image", L"@L05Failed");
      }

//_Start_L05_NATURAL_FILE_GUARD_ENABLE_
      if (PcdGetBool (PcdL05NaturalFileGuardDialogFlag)) {
        //
        // [Natural File Guard Design Guide V1.01]
        //   2.3 Boot Flow
        //     2. Display prompt
        //
        Control = UiFindChildByName (Dialog, L"QuestionText");
        UiSetAttribute (Control, L"visibility", L"false");
        Control = UiFindChildByName (Dialog, L"DialogImage");
        UiSetAttribute (Control, L"background-image", L"@L05Failed");
      }
//_End_L05_NATURAL_FILE_GUARD_ENABLE_
    }
//_End_L05_GRAPHIC_UI_

    if (mFbDialog->TitleString != NULL) {
      Control = UiFindChildByName (Dialog, L"DialogTitle");
      UiSetAttribute (Control, L"text", mFbDialog->TitleString);
      UiSetAttribute (Control, L"visibility", L"true");
    }

    if (mFbDialog->BodyStringArray != NULL) {
      Control = UiFindChildByName (Dialog, L"DialogText");
      BodyString = CatStringArray (mFbDialog->BodyStringCount, (CONST CHAR16 **) mFbDialog->BodyStringArray);
      if (BodyString != NULL) {
        UiSetAttribute (Control, L"text", BodyString);
        FreePool (BodyString);
      }
    }

#ifdef L05_SECURITY_ERASE_ENABLE
{
    UINTN                               NumberLength;

    if (PcdGetBool (PcdL05SecurityEraseProcessingDialogFlag)) {
      L05_GET_NUMBER_LENGTH (100, NumberLength);

      mSecurityEraseProcessingMicroseconds = PcdGet64 (PcdL05SecurityEraseTimeMicrosecond);
      mSecurityEraseProcessingMicroseconds /= 100; // Percentage
  
      if (mSecurityEraseProcessingMicroseconds == 0) {
        mSecurityEraseProcessingMicroseconds = L05_SECURITY_ERASE_DEFAULT_TIME_SECOND * 1000 * 1000;
        mSecurityEraseProcessingMicroseconds /= 100; // Percentage
      }

      mCountStringSize = StrSize (mCountFormatString) + (NumberLength * sizeof (CHAR16));
      mCountString = AllocateZeroPool (mCountStringSize);
      StrCpyS (mCountString, mCountStringSize, mCountFormatString);

      L05SecurityEraseMetroPercentageTimerEvent (Wnd);
      L05SecurityEraseMetroProgressRingTimerEvent (Wnd);
    }
}
#endif

//_Start_L05_GRAPHIC_UI_
    if (mFbDialog->ButtonCount == 0) {
      MoveDialogPosition (Wnd, Dialog);
      break;
    }
//_End_L05_GRAPHIC_UI_

    ListControl = UiFindChildByName (Dialog, L"DialogButtonList");
    UiSetAttribute (ListControl, L"visibility", L"true");

    Control = CreateControl (L"Control", ListControl);
    CONTROL_CLASS(ListControl)->AddChild (ListControl, Control);

    UnicodeSPrint (ButtonWidthStr, sizeof (ButtonWidthStr), L"%d", GetButtonWidthByStr ());
    FocusControl = NULL;
    for (Index = 0; Index < mFbDialog->ButtonCount; Index++) {
      if (mFbDialog->ButtonStringArray[Index] == NULL) {
        continue;
      }

      Control = CreateControl (L"Button", ListControl);
      SetWindowLongPtr (Control->Wnd, GWLP_USERDATA, (INTN) Index);

//_Start_L05_GRAPHIC_UI_
//      UiApplyAttributeList (Control, L"name='Button' height='30' font-size='19' textcolor='0xFFFFFFFF' text-align='center' text-align='singleline' background-color='0xFFCCCCCC' focusbkcolor='@menulightcolor'");
//      if (PcdGet32(PcdH2OLmdeMultiLayout) == 1) {
//        UiApplyAttributeList (Control, L"background-color='0x0' focustextcolor='0xFF404040'");
//      }
//      UiSetAttribute (Control, L"text",  mFbDialog->ButtonStringArray[Index]);
//      UiSetAttribute (Control, L"width", ButtonWidthStr);
      UiApplyAttributeList (Control, L"name='Button' height='42' font-size='22' textcolor='0xFF000000' text-align='center|singleline' background-color='0xFFE5E5E5' focusbkcolor='0xFF3E8DDD'");
      UiSetAttribute (Control, L"text",  mFbDialog->ButtonStringArray[Index]);
      UiSetAttribute (Control, L"width", L"130");
//_End_L05_GRAPHIC_UI_

      Status = CompareHiiValue (&mFbDialog->ButtonHiiValueArray[Index], &mFbDialog->ConfirmHiiValue, &Result);
      if (!EFI_ERROR(Status) && Result == 0) {
        FocusControl = Control;
      }
      CONTROL_CLASS(ListControl)->AddChild (ListControl, Control);
    }
    if (FocusControl != NULL) {
      SetFocus (FocusControl->Wnd);
    }

    Control = CreateControl (L"Control", ListControl);
//_Start_L05_GRAPHIC_UI_
    UiSetAttribute (Control, L"width", L"42"); // 72 - 30(ChildPadding) = 42
//_End_L05_GRAPHIC_UI_
    CONTROL_CLASS(ListControl)->AddChild (ListControl, Control);

//_Start_L05_GRAPHIC_UI_
    MoveDialogPosition (Wnd, Dialog);
//_End_L05_GRAPHIC_UI_
    break;

  case UI_NOTIFY_CLICK:
    Control = (UI_CONTROL *) WParam;
    Index = (UINTN) GetWindowLongPtr (Control->Wnd, GWLP_USERDATA);
    SendChangeQNotify (0, 0, &mFbDialog->ButtonHiiValueArray[Index]);
    break;

  case WM_HOTKEY:
    if (HIWORD(LParam) == VK_ESCAPE) {
      SendShutDNotify ();
      return 0;
    }
    return 1;

  case WM_DESTROY:
    FreeDialogEvent (&mFbDialog);
    return 0;

//_Start_L05_NATURAL_FILE_GUARD_ENABLE_
  //
  // [Natural File Guard Design Guide V1.01]
  //   2.3 Boot Flow
  //     2. Display prompt
  //
  // Press Enter or ESC to continue
  //
  case WM_CHAR:
    if (PcdGetBool (PcdL05NaturalFileGuardDialogFlag)) {
      if (LOWORD (WParam) == 0x0D) { /* enter */
        SendShutDNotify ();
      }
    }
    return 0;
//_End_L05_NATURAL_FILE_GUARD_ENABLE_

  default:
    return 0;
  }

  return 1;
}


HWND
FindOverlayDialog (
  )
{
  UI_CONTROL                    *Control;
  UI_CONTROL                    *Overlay;
  UI_MANAGER                    *Manager;


  Control = GetUiControl (gWnd);
  Manager = Control->Manager;
  Overlay = Manager->FindControlByName (Manager, L"overlay");
  if (Overlay != NULL) {
    CONTROL_CLASS(Overlay)->SetAttribute (Overlay, L"visibility", L"true");
    return Overlay->Wnd;
  }

  return NULL;
}


HWND
LocalMetroCreateDialog (
  IN CONST H2O_DISPLAY_ENGINE_EVT          *Notify
  )
{
  HWND                          Wnd;
  CHAR16                        *XmlBuffer;
  H2O_DISPLAY_ENGINE_EVT_OPEN_D *OpenDNotify;
  H2O_FORM_BROWSER_D            *Dialog;
  WNDPROC                       DialogProc;
  BOOLEAN                       CloseOnTouchOutside;
  UINT8                         OpCode;
  HWND                          ParentWnd;
  RECT                          DlgRect;
  EFI_STATUS                    Status;
  INT32                         Id;

  OpenDNotify = (H2O_DISPLAY_ENGINE_EVT_OPEN_D *) Notify;
  Dialog      = &OpenDNotify->Dialog;

  Wnd = NULL;
  XmlBuffer = NULL;
  DialogProc = NULL;
  if (gFB->CurrentP != NULL) {
    CloseOnTouchOutside = TRUE;
    mIsSendForm         = TRUE;
  } else {
    CloseOnTouchOutside = FALSE;
    mIsSendForm         = FALSE;
    ZeroMem (&mPreviousAbsPtrState, sizeof (EFI_ABSOLUTE_POINTER_STATE));
  }

  SetRectEmpty (&DlgRect);
  ParentWnd = GetFocus();
  mFbDialog = Dialog;

  if ((Dialog->DialogType & H2O_FORM_BROWSER_D_TYPE_QUESTIONS) == 0) {
    if ((Dialog->DialogType & H2O_FORM_BROWSER_D_TYPE_SHOW_HELP) == H2O_FORM_BROWSER_D_TYPE_SHOW_HELP) {
      Status = GetInformationField (&DlgRect);
      ASSERT (!EFI_ERROR (Status));
      XmlBuffer = mHelpDialogChilds;
      DialogProc = H2OHelpDialogProc;
    } else if ((Dialog->DialogType & H2O_FORM_BROWSER_D_TYPE_SHOW_CONFIRM_PAGE) == H2O_FORM_BROWSER_D_TYPE_SHOW_CONFIRM_PAGE) {
      Status = GetInformationField (&DlgRect);
      if (EFI_ERROR (Status)) {
        DEBUG ((EFI_D_INFO, "GetInformationField(): %r", Status));
      }

      XmlBuffer = mConfirmPageDialogChilds;
      DialogProc = H2OConfirmPageDialogProc;

    } else if ((Dialog->DialogType & H2O_FORM_BROWSER_D_TYPE_SELECTION) == H2O_FORM_BROWSER_D_TYPE_SELECTION) {
      mFbDialog = CopyDialogEvent (Dialog);
      if (mFbDialog == NULL) {
        return NULL;
      }

      if (mIsSendForm) {
        Status = GetRectByName (gWnd, L"H2OHelpTextPanel", &DlgRect);
        ASSERT (!EFI_ERROR (Status));
//_Start_L05_GRAPHIC_UI_
//        XmlBuffer = mCommonDialogChilds;
        if (mFbDialog->ButtonCount == 0) {
          XmlBuffer = mPopupDialogWithoutButtonChilds;
        } else {
          if (PcdGetBool (PcdL05GamingSetupExitDialogFlag)) {
            XmlBuffer = mL05GamingExitPopupDialog;
          } else {
            XmlBuffer = mPopupDialogWithButtonChilds;
          }
        }
//End_L05_GRAPHIC_UI_

        DialogProc = H2OPopupDialogProc;

#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
        if (mL05OverclockDialogWnd != NULL) {
          XmlBuffer = mL05OverclockApplyDialog;
          DialogProc = L05OverclockApplyDialogProc;
        }
#endif

      } else {
        Status = GetDialogRegion (L"DialogPanel", &DlgRect);
//      ASSERT_EFI_ERROR (Status);
//_Start_L05_GRAPHIC_UI_
//        XmlBuffer = mDialogWithoutSendFormChilds;
        if (mFbDialog->ButtonCount == 0) {
          XmlBuffer = mPopupDialogWithoutButtonChilds;
        } else {
          XmlBuffer = mPopupDialogWithButtonChilds;          
#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
          if (!StrCmp (mFbDialog->TitleString, L"Startup Interrupt Menu")) {
            XmlBuffer = mL05InterruptMenuPopupDialogChilds;
          }
//_End_L05_INTERRUPT_MENU_
#endif
        }
//End_L05_GRAPHIC_UI_
        DialogProc = DialogWithoutSendFormProc;
      }
    } else if ((Dialog->DialogType & H2O_FORM_BROWSER_D_TYPE_MSG) == H2O_FORM_BROWSER_D_TYPE_MSG) {
      if (mIsSendForm) {
        Status = GetRectByName (gWnd, L"H2OHelpTextPanel", &DlgRect);
        ASSERT_EFI_ERROR (Status);
//_Start_L05_GRAPHIC_UI_
//        XmlBuffer = mCommonDialogChilds;
        XmlBuffer = mPopupDialogWithoutButtonChilds;
//End_L05_GRAPHIC_UI_
      } else {
        Status = GetDialogRegion (L"DialogPanel", &DlgRect);
        ASSERT_EFI_ERROR (Status);
//_Start_L05_GRAPHIC_UI_
//        XmlBuffer = mDialogWithoutSendFormChilds;
        if (mFbDialog->ButtonCount == 0) {
          XmlBuffer = mPopupDialogWithoutButtonChilds;
        } else {
          XmlBuffer = mPopupDialogWithButtonChilds;          
        }
//End_L05_GRAPHIC_UI_
      }

//_Start_L05_SETUP_MENU_
      if (PcdGetBool (PcdL05SecurityEraseProcessingDialogFlag)) {
        XmlBuffer = mL05SecurityEraseChilds;
      }
//_End_L05_SETUP_MENU_

      DialogProc = MsgPopUpProc;
      mFbDialog = CopyDialogEvent (Dialog);
      if (mFbDialog == NULL) {
        return NULL;
      }
    } else {
      return NULL;
    }
  } else {
    OpCode = GetOpCodeByDialogType (Dialog->DialogType);

    switch (OpCode) {

    case EFI_IFR_ONE_OF_OP:
      mFbDialog = CopyDialogEvent (Dialog);
      if (mFbDialog == NULL) {
        return NULL;
      }

      if (mIsSendForm) {
        Status = GetRectByName (gWnd, L"H2OHelpTextPanel", &DlgRect);
//_Start_L05_GRAPHIC_UI_
//        XmlBuffer = mCommonDialogWithHelpChilds;
        XmlBuffer = mOneOfDialogWithSendFormChilds;
//_End_L05_GRAPHIC_UI_
      } else {
        Status = GetDialogRegion (L"DialogPanel", &DlgRect);
        XmlBuffer = mOneOfDialogWithoutSendFormChilds;
      }
      DialogProc = H2OOneOfInputProc;
      ASSERT (!EFI_ERROR (Status));
      break;

    case EFI_IFR_NUMERIC_OP:
      //
      // TODO: Need receive and sending H2O_DISPLAY_ENGINE_EVT_TYPE_CHANGING_Q to synchronize
      // with other display engine.
      //
      if (mIsSendForm) {
        Status = GetRectByName (gWnd, L"H2OHelpTextPanel", &DlgRect);
        ASSERT (!EFI_ERROR (Status));
        XmlBuffer = mCommonDialogWithHelpChilds;
        DialogProc = H2ONumericDialogProc;
      } else {
        Status = GetDialogRegion (L"DialogPanel", &DlgRect);
        ASSERT_EFI_ERROR (Status);
        OffsetRect (&DlgRect, 0, -30);
        XmlBuffer = mNumericDialogWithoutSendFormChilds;
        DialogProc = H2ONumericDialogWithoutSendFormProc;
      }
      CloseOnTouchOutside = FALSE;
      break;

    case EFI_IFR_ORDERED_LIST_OP:
      Status = GetRectByName (gWnd, L"H2OHelpTextPanel", &DlgRect);
      ASSERT (!EFI_ERROR (Status));
      XmlBuffer = mCommonDialogWithHelpChilds;
      DialogProc = H2OOrderedListInputProc;
      CloseOnTouchOutside = FALSE;
      break;

    case EFI_IFR_CHECKBOX_OP:
    case EFI_IFR_PASSWORD_OP:
      //
      // TODO: Need receive and sending H2O_DISPLAY_ENGINE_EVT_TYPE_CHANGING_Q to synchronize
      // with other display engine.
      //
      if (mIsSendForm) {
        Status = GetRectByName (gWnd, L"H2OHelpTextPanel", &DlgRect);
        ASSERT (!EFI_ERROR (Status));
//_Start_L05_GRAPHIC_UI_
//        XmlBuffer = mCommonDialogWithHelpChilds;
        XmlBuffer = mPasswordDialogChilds;
//End_L05_GRAPHIC_UI_
        DialogProc = H2OPasswordDialogProc;
      } else {
        Status = GetDialogRegion (L"DialogPanel", &DlgRect);
        ASSERT_EFI_ERROR (Status);
        XmlBuffer = mConfirmPasswordDialogChilds;
        DialogProc = H2OConfirmPasswordProc;
      }
//[-start-220125-BAIN000092-modify]#
#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
//#ifdef L05_NOTEBOOK_PASSWORD_ENABLE
//[-end-220125-BAIN000092-modify]#
      //
      // [Lenovo Notebook Password Design Spec V1.1]
      //   4.1.1. Check HDP
      //     User can press F1 to switch Master Password and User Password.
      //     Figure 4-1 Check Master Password
      //     Figure 4-2 Check User Password
      //     prompt - "Press F1 to switch between Master Password and User Password"
      //
      if (PcdGetBool (PcdL05NotebookPasswordDesignDialogF1Flag)) {
        XmlBuffer = mConfirmHddPasswordDialogChilds;
        DialogProc = H2OConfirmPasswordProc;
      }
#endif

//_Start_L05_SETUP_MENU_
      mL05GraphicPasswordInput = TRUE;
//_End_L05_SETUP_MENU_
      CloseOnTouchOutside = FALSE;
      break;

    case EFI_IFR_STRING_OP:
      //
      // TODO: Need receive and sending H2O_DISPLAY_ENGINE_EVT_TYPE_CHANGING_Q to synchronize
      // with other display engine.
      //
      Status = GetRectByName (gWnd, L"H2OHelpTextPanel", &DlgRect);
      ASSERT (!EFI_ERROR (Status));
      XmlBuffer = mCommonDialogWithHelpChilds;
      DialogProc = H2OStringDialogProc;
      CloseOnTouchOutside = FALSE;
      break;

    case EFI_IFR_DATE_OP:
    case EFI_IFR_TIME_OP:
      Status = GetRectByName (gWnd, L"H2OHelpTextPanel", &DlgRect);
      ASSERT (!EFI_ERROR (Status));
//_Start_L05_GRAPHIC_UI_
//      XmlBuffer  = mCommonDialogWithHelpChilds;
      if (OpCode == EFI_IFR_DATE_OP) {
        XmlBuffer = mDateDialogChilds;     
      } else {
        XmlBuffer = mTimeDialogChilds;
      }
//End_L05_GRAPHIC_UI_
      DialogProc = H2ODateTimeDialogProc;
      break;

    default:
      ASSERT (FALSE);
    }
  }

  Id = 1;
  while (TRUE) {
    if (!UnregisterHotKey (Wnd, Id++)) {
      break;
    }
  }

  Wnd = CreateModalDialog (
          NULL,
          XmlBuffer,
          ParentWnd,
          DialogProc,
          0,
          DlgRect.left,
          DlgRect.top,
          DlgRect.right - DlgRect.left,
          DlgRect.bottom - DlgRect.top,
          CloseOnTouchOutside
          );

#ifdef L05_SECURITY_ERASE_ENABLE
  if (!PcdGetBool (PcdL05SecurityEraseProcessingDialogFlag)) {
#endif

  MwRegisterHotKey (Wnd, 1,  0, VK_ESCAPE);

#ifdef L05_SECURITY_ERASE_ENABLE
  }
#endif

  return Wnd;
}

VOID
LocalMetroRegisterHotkey (
  HWND                          Wnd
  );

HWND
GetNextTabItemByTabOrder (
  IN  HWND                      Dialog,
  IN  UI_CONTROL                *Current,
  IN  BOOL                      Previous
  )
{
  INTN                          Style;
  INTN                          ExStyle;
  HWND                          Child;
  HWND                          Node;
  UI_CONTROL                    *Candidate;
  UI_CONTROL                    *Next;
  UI_CONTROL                    *Control;

  Next      = NULL;
  Candidate = NULL;

  Node = GetWindow (Dialog, GW_CHILD);
  while (Node != NULL && Node != Dialog) {

    Child = GetWindow (Node, GW_CHILD);
    Style = GetWindowLongPtr (Node, GWL_STYLE);
    ExStyle = GetWindowLongPtr (Node, GWL_EXSTYLE);

    do {
      if (!((Style & WS_VISIBLE) && !(Style & WS_DISABLED))) {
        Child = NULL;
        break;
      }

      if (!(ExStyle & WS_EX_CONTROLPARENT)) {
        Child = NULL;
        break;
      }

      if (Style & WS_TABSTOP) {
        Child = NULL;
        Control = GetUiControl (Node);
        ASSERT (Control != NULL);
        if (Control == NULL) {
          break;
        }
        if (Control->TabOrder == (Current->TabOrder + 1)) {
          return Control->Wnd;
        }
        if ((Control->TabOrder > Current->TabOrder) &&
            ((Next == NULL) || (Next->TabOrder > Control->TabOrder))) {
          Next = Control;
        }
        if ((Control->TabOrder < Current->TabOrder) &&
            ((Candidate == NULL) || (Candidate->TabOrder > Control->TabOrder))) {
          Candidate = Control;
        }
      }
    } while (0);

    if (Child != NULL) {
      Node = Child;
    } else if (GetWindow (Node, GW_HWNDNEXT) != NULL) {
      Node = GetWindow (Node, GW_HWNDNEXT);
    } else {
      Node = GetParent (Node);
      while (Node != NULL && Node != Dialog) {
        if (GetWindow (Node, GW_HWNDNEXT) != NULL) {
          Node = GetWindow (Node, GW_HWNDNEXT);
          break;
        } else {
          Node = GetParent (Node);
        }
      }
    }
  }

  if (Next == NULL) {
    Next = Candidate;
  }

  if (Next != NULL) {
    return Next->Wnd;
  }
  return NULL;
}

HWND
GetNextTabItem (
  IN  HWND MainWnd,
  IN  HWND DlgWnd,
  IN  HWND CtrlWnd,
  IN  BOOL Previous
  )
{
  INTN                          Style;
  INTN                          ExStyle;
  UINT32                        WndSearch;
  HWND                          ChildFirst;
  HWND                          RetWnd;

  WndSearch = Previous ? GW_HWNDPREV : GW_HWNDNEXT;

  if (DlgWnd == CtrlWnd) {
    CtrlWnd = NULL;
  }

  ChildFirst = NULL;
  RetWnd = NULL;

  if (!CtrlWnd) {
    ChildFirst = GetWindow (DlgWnd, GW_CHILD);
    if (Previous) {
      ChildFirst = GetWindow (ChildFirst, GW_HWNDLAST);
    }
  } else if (IsChild (MainWnd, CtrlWnd)) {
    ChildFirst = GetWindow (CtrlWnd, WndSearch);
    if (!ChildFirst) {
      if (GetParent (CtrlWnd) != MainWnd) {
        ChildFirst = GetWindow (GetParent (CtrlWnd), WndSearch);
      } else {
        ChildFirst = GetWindow (CtrlWnd, Previous ? GW_HWNDLAST : GW_HWNDFIRST);
      }
    }
  }

  while (ChildFirst != NULL) {
    Style = GetWindowLongPtr (ChildFirst, GWL_STYLE);
    ExStyle = GetWindowLongPtr (ChildFirst, GWL_EXSTYLE);

    if ((Style & WS_TABSTOP) && (Style & WS_VISIBLE) && !(Style & WS_DISABLED)) {
      return ChildFirst;
    } else if ((ExStyle & WS_EX_CONTROLPARENT) && (Style & WS_VISIBLE) && !(Style &WS_DISABLED)) {
      RetWnd = GetNextTabItem (MainWnd, ChildFirst, NULL, Previous);
      if (RetWnd != NULL) {
        return RetWnd;
      }
    }

    ChildFirst = GetWindow (ChildFirst, WndSearch);
  }

  if (CtrlWnd != NULL) {
    HWND Parent;

    Parent = GetParent (CtrlWnd);
    while (Parent != NULL) {
      if (Parent == MainWnd) {
        break;
      }
      RetWnd = GetNextTabItem (MainWnd, GetParent (Parent), Parent, Previous);
      if (RetWnd != NULL) {
        break;
      }
    }
    if(RetWnd == NULL) {
      RetWnd = GetNextTabItem (MainWnd, MainWnd, NULL, Previous);
    }
  }

  return RetWnd ? RetWnd : CtrlWnd;
}



BOOLEAN
IsUiDialogMessage (
  HWND                          DlgWnd,
  LPMSG                         Msg
  )
{
  HWND                          Focus;
  HWND                          Next;
  LRESULT                       DlgCode;

  if (Msg->message == WM_TIMER) {
    return FALSE;
  }

  if ((DlgWnd != Msg->hwnd) && !IsChild(DlgWnd, Msg->hwnd)) {
    return FALSE;
  }

  switch(Msg->message)
  {

  case WM_KEYDOWN:
    DlgCode = SendMessage (Msg->hwnd, WM_GETDLGCODE, Msg->wParam, (LPARAM) Msg);
    if (DlgCode & (DLGC_WANTMESSAGE)) break;

    switch (Msg->wParam) {

    case VK_TAB:
      if (DlgCode & DLGC_WANTTAB) break;

      Focus = GetFocus ();

      Next = GetNextTabItemByTabOrder (DlgWnd, GetUiControl (Focus), FALSE);
      if (Next == NULL) {
        Next = GetNextTabItem (DlgWnd, DlgWnd, Focus == DlgWnd ? NULL : Focus, FALSE);
      }
      if (Next != NULL) {
        SetFocus (Next);
        return TRUE;
      }
      break;
    }
  }

  TranslateMessage (Msg);
  DispatchMessage (Msg);
  return TRUE;
}

EFI_STATUS
EFIAPI
LocalMetroDialogNotify (
  IN       H2O_DISPLAY_ENGINE_PROTOCOL     *This,
  IN CONST H2O_DISPLAY_ENGINE_EVT          *Notify
  )
{
  MSG                                 Msg;
  H2O_DISPLAY_ENGINE_EVT_ABS_PTR_MOVE *AbsPtr;
  H2O_DISPLAY_ENGINE_EVT_KEYPRESS     *KeyPress;
  UI_DIALOG                           *Dialog;

  switch (Notify->Type) {

  case H2O_DISPLAY_ENGINE_EVT_TYPE_OPEN_D:
    //
    // don't allow re-entry
    //
    ASSERT (mDialogWnd == NULL);
    if (mDialogWnd != NULL) {
      return EFI_ALREADY_STARTED;
    }
    EnableWindow (gWnd, FALSE);
    EnableWindow (GetDesktopWindow(), FALSE);
    mDialogWnd = LocalMetroCreateDialog (Notify);
    if (mDialogWnd == NULL) {
      SendShutDNotify ();
    }
//_Start_L05_GRAPHIC_UI_
//    GrayOutBackground (gWnd, TRUE);
//_End_L05_GRAPHIC_UI_
//_Start_L05_SETUP_MENU_
    if (mL05GraphicPasswordInput) {
      OemSvcOverridePasswordKeyboardBeep (TRUE);
    }
//_End_L05_SETUP_MENU_
    break;

  case H2O_DISPLAY_ENGINE_EVT_TYPE_SHUT_D:
    if (mDialogWnd != NULL) {
//_Start_L05_NATURAL_FILE_GUARD_ENABLE_
      //
      // [Natural File Guard Design Guide V1.01]
      //   2.3 Boot Flow
      //     2. Display prompt
      //
      // Need DestroyWindow & set mDialogWnd to NULL for next dialog can H2O_DISPLAY_ENGINE_EVT_TYPE_OPEN_D.
      //
//      if (!(mFbDialog->DialogType  == (H2O_FORM_BROWSER_D_TYPE_MSG|H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG))) {
      if (!(mFbDialog->DialogType  == (H2O_FORM_BROWSER_D_TYPE_MSG|H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG)) ||
          ((mFbDialog->DialogType  == (H2O_FORM_BROWSER_D_TYPE_MSG|H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG)) && PcdGetBool (PcdL05NaturalFileGuardDialogFlag)) ||
          ((mFbDialog->DialogType  == (H2O_FORM_BROWSER_D_TYPE_MSG|H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG)) && PcdGetBool (PcdL05SecurityEraseProcessingDialogFlag))) {
//_End_L05_NATURAL_FILE_GUARD_ENABLE_
        DestroyWindow(mDialogWnd);
        mDialogWnd = NULL;
      }
    }
    This->Notify = LocalMetroNotify;
    EnableWindow (gWnd, TRUE);
//_Start_L05_GRAPHIC_UI_
//    GrayOutBackground (gWnd, FALSE);
//_End_L05_GRAPHIC_UI_
//_Start_L05_SETUP_MENU_
    if (mL05GraphicPasswordInput) {
      OemSvcOverridePasswordKeyboardBeep (FALSE);
      mL05GraphicPasswordInput = FALSE;
    }
//_End_L05_SETUP_MENU_
    SetFocus (gWnd);
    LocalMetroRegisterHotkey (gWnd);
    break;

  case H2O_DISPLAY_ENGINE_EVT_TYPE_ABS_PTR_MOVE:
    AbsPtr = (H2O_DISPLAY_ENGINE_EVT_ABS_PTR_MOVE *) Notify;
    Dialog = (UI_DIALOG *) GetWindowLongPtr (mDialogWnd, 0);

    if (Dialog->CloseDlgWhenTouchOutside && ClickOnOutsideOfDlgRegion (&AbsPtr->AbsPtrState) && mPreviousAbsPtrState.ActiveButtons == 0) {
      CopyMem (&mPreviousAbsPtrState, &AbsPtr->AbsPtrState, sizeof (EFI_ABSOLUTE_POINTER_STATE));
      SendShutDNotify ();
      break;
    }

    CopyMem (&mPreviousAbsPtrState, &AbsPtr->AbsPtrState, sizeof (EFI_ABSOLUTE_POINTER_STATE));
    GdAddPointerData (NULL, &AbsPtr->AbsPtrState);
    break;

  case H2O_DISPLAY_ENGINE_EVT_TYPE_KEYPRESS:
    KeyPress = (H2O_DISPLAY_ENGINE_EVT_KEYPRESS *) Notify;
//_Start_L05_SETUP_MENU_
    if (mL05GraphicPasswordInput) {
      EFI_STATUS                     Status;
      EFI_L05_SETUP_MENU_PROTOCOL    *L05SetupMenu;
      UI_CONTROL                     *FocusedControl;

      L05SetupMenu = NULL;
      Status = gBS->LocateProtocol (
                      &gEfiL05SetupMenuProtocolGuid,
                      NULL,
                      &L05SetupMenu
                      );
      FocusedControl = GetUiControl (GetFocus ());

      if (L05SetupMenu != NULL && StrCmp (FocusedControl->Class->ClassName, L"UiEdit") == 0) {
        //
        // Store Key data for L05SetupMenuPtr->IsValidKeyData() using.
        //
        CopyMem (&(L05SetupMenu->CurrentKeyData), &(KeyPress->KeyData), sizeof (EFI_KEY_DATA));
        if (!L05SetupMenu->IsValidKeyData (L05SetupMenu)) {
          OemSvcGenerateBeep (PASSWORD_INVALID_KEY_BEEP);
          break;
        }
      }
    }
//_End_L05_SETUP_MENU_

//[-start-220125-BAIN000092-modify]//
#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
//#ifdef L05_NOTEBOOK_PASSWORD_ENABLE
//[-end-220125-BAIN000092-modify]#
    //
    // [Lenovo Notebook Password Design Spec V1.1]
    //   4.1.1. Check HDP
    //     User can press F1 to switch Master Password and User Password.
    //
    if (PcdGetBool (PcdL05NotebookPasswordDesignPressF1Flag)) {
      KeyPress->KeyData.Key.ScanCode = SCAN_ESC;
    }
#endif

    GdAddEfiKeyData (&KeyPress->KeyData);
    break;

  case H2O_DISPLAY_ENGINE_EVT_TYPE_TIMER:
    if (((H2O_DISPLAY_ENGINE_EVT_TIMER *) Notify)->TimerId == H2O_METRO_DE_TIMER_ID_PERIODIC_TIMER) {
      RegisterTimerEvent (H2O_METRO_DE_TIMER_ID_PERIODIC_TIMER, H2O_METRO_DE_TIMER_PERIODIC_TIME);
    }

#ifdef L05_SECURITY_ERASE_ENABLE
    if (((H2O_DISPLAY_ENGINE_EVT_TIMER *) Notify)->TimerId == L05_SECURITY_ERASE_TIMER_ID) {
      L05SecurityEraseMetroPercentageTimerEvent (mDialogWnd);
    }

    if (((H2O_DISPLAY_ENGINE_EVT_TIMER *) Notify)->TimerId == L05_PROGRESS_RING_TIMER_ID) {
      L05SecurityEraseMetroProgressRingTimerEvent (mDialogWnd);
    }
#endif
    break;

  case H2O_DISPLAY_ENGINE_EVT_TYPE_REFRESH_Q:
    break;

  case H2O_DISPLAY_ENGINE_EVT_TYPE_CHANGING_Q:
    break;

  case H2O_DISPLAY_ENGINE_EVT_TYPE_SUBMIT_EXIT:
    break;

  default:
    ASSERT (FALSE);
  }

  while (PeekMessage (&Msg, NULL, 0, 0, PM_REMOVE)) {
    if (Msg.message == WM_RBUTTONUP && GetCapture () == NULL) {
      SendMessage (mDialogWnd, WM_HOTKEY, 0, MAKELPARAM(0, VK_ESCAPE));
    } else if (mDialogWnd == NULL || !IsUiDialogMessage (mDialogWnd, &Msg)) {
      TranslateMessage (&Msg);
      DispatchMessage (&Msg);
    }
  }

  return EFI_SUCCESS;
}

