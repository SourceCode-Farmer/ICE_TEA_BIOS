/** @file
  UI ordered list control

;******************************************************************************
;* Copyright (c) 2014 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2ODisplayEngineLocalMetro.h"
#include "UiControls.h"
#include "MetroUi.h"
#include "MetroDialog.h"
#include <Library/ConsoleLib.h>

//_Start_L05_GRAPHIC_UI_
//#if FixedPcdGet32(PcdH2OLmdeMultiLayout) == 0
//CHAR16 *mConfirmPasswordDialogChilds = L""
//  L"<VerticalLayout name='DialogWithoutSendForm'>"
//    L"<Control/>"
//    L"<Control background-color='@menucolor' name='parent' height='wrap_content'>"
//      L"<VerticalLayout padding='20,30,20,30' background-color='@menucolor' height='wrap_content'>"
//        L"<VerticalLayout name='TitleLayout' height='wrap_content'>"
//          L"<Label name='DialogTitle' text-align='center' height='40' visibility='false' font-size='19' textcolor='0xFFFFFFFF'/>"
//          L"<Label width='match_parent' textcolor='0xFFFFFFFF' font-size='19' name='DialogText' height='wrap_content'/>"
//          L"<Control height='15'/>"
//          L"<Control height='10' background-image='@DialogSeparator' background-color='0x0' background-image-style='center'/>"
//          L"<Control height='15'/>"
//        L"</VerticalLayout>"
//        L"<VerticalLayout padding='0,0,10,0' min-height='51' name='DialogPasswordInput' height='wrap_content'>"
//          L"<Label name='ConfirmNewPasswordLabel' height='wrap_content' padding='0,0,2,0' width='match_parent' font-size='18' textcolor='0xFFFFFFFF'/>"
//          L"<Control padding='2,2,2,2' background-color='0xFFCCCCCC' height='wrap_content'>"
//            L"<UiEdit name='ConfirmPasswordInput' focusbkcolor='0xFFFFFFFF' tabstop='true' height='27' padding='7,3,0,3' taborder='1' background-color='@menulightcolor' password='true'/>"
//          L"</Control>"
//        L"</VerticalLayout>"
//      L"</VerticalLayout>"
//      L"<Texture name='FormHalo' float='true' height='-1' width='-1' background-image='@FormHalo' scale9grid='23,26,22,31'/>"
//    L"</Control>"
//    L"<Control/>"
//  L"</VerticalLayout>";
//#endif
CHAR16 *mConfirmPasswordDialogChilds = L""
  L"<VerticalLayout padding='10,0,0,10' name='DialogWithoutSendForm' background-color='0x0' height='270' width='968'>"
    L"<VerticalLayout padding='2,2,2,2' background-color='0xFF3E8DDD' height='270' width='968' float='true'>"
      L"<VerticalLayout name='BodyLayout' padding='40,0,0,32' background-color='0xFFFFFFFF' height='266' width='964'>"
        L"<Label textcolor='0xFF3E8DDD' font-size='32' name='DialogText' height='wrap_content' width='match_parent'/>"
        L"<Control/>"
        L"<HorizontalLayout padding='0,0,0,52' background-color='0xFFFFFFFF' name='DialogPasswordInput' height='wrap_content'>"
          L"<VerticalLayout width='800' height='wrap_content'>"
            L"<Control padding='0,0,0,0' width='800' background-color='0xFFEFEFEF' height='50'>"
              L"<UiEdit name='ConfirmPasswordInput' focusbkcolor='0xFFCFE3F2' tabstop='true' height='50' padding='13,13,13,13' font-size='24' width='800' taborder='1' background-color='0xFFEFEFEF' password='true'/>"
            L"</Control>"
          L"</VerticalLayout>"
        L"</HorizontalLayout>"
        L"<Control/>"
      L"</VerticalLayout>"
    L"</VerticalLayout>"
  L"</VerticalLayout>";

//_Start_L05_NOTEBOOK_PASSWORD_V1_1_ENABLE_
//
// [Lenovo Notebook Password Design Spec V1.1]
//   4.1.1. Check HDP
//     User can press F1 to switch Master Password and User Password.
//     Figure 4-1 Check Master Password
//     Figure 4-2 Check User Password
//     prompt - "Press F1 to switch between Master Password and User Password"
//
CHAR16 *mConfirmHddPasswordDialogChilds = L""
  L"<VerticalLayout padding='10,0,0,10' name='DialogWithoutSendForm' background-color='0x0' height='300' width='968'>"
    L"<VerticalLayout padding='2,2,2,2' background-color='0xFF3E8DDD' height='300' width='968' float='true'>"
      L"<VerticalLayout name='BodyLayout' padding='40,0,0,32' background-color='0xFFFFFFFF' height='296' width='964'>"
        L"<Label textcolor='0xFF3E8DDD' font-size='32' name='DialogText' height='wrap_content' width='match_parent'/>"
        L"<Control/>"
        L"<HorizontalLayout padding='0,0,0,52' background-color='0xFFFFFFFF' name='DialogPasswordInput' height='wrap_content'>"
          L"<VerticalLayout width='800' height='wrap_content'>"
            L"<Control height='15'/>"
            L"<Control padding='0,0,0,0' width='800' background-color='0xFFEFEFEF' height='50'>"
              L"<UiEdit name='ConfirmPasswordInput' focusbkcolor='0xFFCFE3F2' tabstop='true' height='50' padding='13,13,13,13' font-size='24' width='800' taborder='1' background-color='0xFFEFEFEF' password='true'/>"
            L"</Control>"
            L"<Control height='32'/>"
            L"<Label name='ConfirmNewPasswordLabel' height='wrap_content' width='match_parent' font-size='22' textcolor='0xFF000000'/>"
          L"</VerticalLayout>"
        L"</HorizontalLayout>"
        L"<Control/>"
      L"</VerticalLayout>"
    L"</VerticalLayout>"
  L"</VerticalLayout>";
//_End_L05_NOTEBOOK_PASSWORD_V1_1_ENABLE_
//_End_L05_GRAPHIC_UI_


EFI_STATUS
SendConfirmPassword (
  IN  UI_DIALOG                 *Dialog
  )
{
  UI_CONTROL                    *Control;
  CHAR16                        *PasswordStr;
  EFI_HII_VALUE                 HiiValue;

  Control = UiFindChildByName (Dialog, L"ConfirmPasswordInput");
  PasswordStr = ((UI_LABEL *) Control)->Text;

  HiiValue.BufferLen = (UINT16) StrSize (PasswordStr);
  HiiValue.Buffer    = AllocatePool (HiiValue.BufferLen);
  ASSERT (HiiValue.Buffer != NULL);
  CopyMem (HiiValue.Buffer, PasswordStr, HiiValue.BufferLen);
  SendChangeQNotify (0, 0, &HiiValue);

  return EFI_SUCCESS;
}


LRESULT
H2OConfirmPasswordProc (
  HWND                          Wnd,
  UINT                          Msg,
  WPARAM                        WParam,
  LPARAM                        LParam
  )
{
  UI_DIALOG                     *Dialog;
  UI_CONTROL                    *Control;
  CHAR16                        Str[20];
//_Start_L05_GRAPHIC_UI_
  RECT                          ScreenRect;
  UI_CONTROL                    *PasswordDialog;
  SIZE                          DialogSize;
//_End_L05_GRAPHIC_UI_

  Dialog  = (UI_DIALOG *) GetWindowLongPtr (Wnd, 0);

  switch (Msg) {

  case UI_NOTIFY_WINDOWINIT:
//_Start_L05_GRAPHIC_UI_
//    if (gFB->CurrentP == NULL) {
//      Control = UiFindChildByName (Dialog, L"TitleLayout");
//      UiSetAttribute (Control, L"visibility", L"false");
//    }
//_End_L05_GRAPHIC_UI_

    if (mFbDialog->TitleString != NULL) {
      Control = UiFindChildByName (Dialog, L"DialogText");
      UiSetAttribute (Control, L"text", mFbDialog->TitleString);
//_Start_L05_GRAPHIC_UI_
//      Control = UiFindChildByName (Dialog, L"ConfirmNewPasswordLabel");
//      UiSetAttribute (Control, L"text", mFbDialog->TitleString);
//_End_L05_GRAPHIC_UI_
    }

//[-start-220125-BAIN000092-modify]//
#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
//_Start_L05_NOTEBOOK_PASSWORD_V1_1_ENABLE_
//#ifdef L05_NOTEBOOK_PASSWORD_ENABLE
////_Start_L05_NOTEBOOK_PASSWORD_ENABLE_
//[-end-220125-BAIN000092-modify]//
    //
    // [Lenovo Notebook Password Design Spec V1.2]
    //   4.1.1. Check HDP
    //     User can press F1 to switch Master Password and User Password.
    //     Figure 4-1 Check Master Password
    //     Figure 4-2 Check User Password
    //     prompt - "Press F1 to switch between Master Password and User Password"
    //
    if (PcdGetBool (PcdL05NotebookPasswordDesignDialogF1Flag)) {
      Control = UiFindChildByName (Dialog, L"ConfirmNewPasswordLabel");
      if (Control != NULL) {
        UiSetAttribute (Control, L"text", PcdGetPtr (PcdL05NotebookPasswordDesignDialogF1String));
      }
    }
#endif

    if (mFbDialog->BodyInputCount != 0) {
      UnicodeSPrint (Str, sizeof (Str), L"%d", (mFbDialog->ConfirmHiiValue.BufferLen / sizeof (CHAR16) - 1));
      Control = UiFindChildByName (Dialog, L"ConfirmPasswordInput");
      UiSetAttribute (Control, L"maxlength", Str);
    }

    if (gFB->CurrentP != NULL) {
      mTitleVisible = TRUE;
      SetTimer (Wnd, 0, 1, DialogCallback);
    }

    Control = UiFindChildByName (Dialog, L"ConfirmPasswordInput");
    SetFocus (Control->Wnd);

//_Start_L05_GRAPHIC_UI_
    //
    // Move window position
    //
    GetWindowRect (gWnd, &ScreenRect);
    PasswordDialog = UiFindChildByName (Dialog, L"DialogWithoutSendForm");
    DialogSize = PasswordDialog->FixedSize;
    SetWindowPos (
      Wnd,
      HWND_TOP,
      (ScreenRect.right  / 2) - (DialogSize.cx / 2) - 1,
      (ScreenRect.bottom / 2) - (DialogSize.cy / 2),
      DialogSize.cx,
      DialogSize.cy,
      0
      );
//_End_L05_GRAPHIC_UI_
    break;

  case UI_NOTIFY_CLICK:
  case UI_NOTIFY_CARRIAGE_RETURN:
    //
    // click ok button or ConfirmPasswordInput passowrd input
    //
    SendConfirmPassword (Dialog);
    return 0;
    break;

  case WM_DESTROY:
    if (gFB->CurrentP != NULL) {
      KillTimer (Wnd, 0);
    }
    return 0;

#ifdef L05_NOTEBOOK_PASSWORD_ENABLE
  //
  // [Lenovo Notebook Password Design Spec V1.1]
  //   4.1.1. Check HDP
  //     User can press F1 to switch Master Password and User Password.
  //
  //
  // [Lenovo Natural File Guard Design Guide V1.01]
  //   2.3 Boot Flow
  //     1. Prompt for UHDP or ESC:
  //        Enter UHDP for 'Show Hard Disk Information' to activate 'Natural File Guard', or press ESC to disable it
  //
  case WM_HOTKEY:
    if (HIWORD (LParam) == VK_ESCAPE) {
      if (PcdGetBool (PcdL05NotebookPasswordDesignPressF1Flag) ||
          PcdGetBool (PcdL05NaturalFileGuardPressEscFlag)) {
        SendShutDNotify ();
      }
    }
    return 0;
#endif

  default:
    return 0;
  }

  return 1;
}
