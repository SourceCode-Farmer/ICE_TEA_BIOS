/** @file
  UI ordered list control

;******************************************************************************
;* Copyright (c) 2014 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2ODisplayEngineLocalMetro.h"
#include "UiControls.h"
#include "MetroUi.h"
#include "MetroDialog.h"
#include <Library/ConsoleLib.h>

extern H2O_FORM_BROWSER_D       *mFbDialog;

//_Start_L05_GRAPHIC_UI_
//#if FixedPcdGet32(PcdH2OLmdeMultiLayout) == 0
//CHAR16 *mHelpDialogChilds = L""
//  L"<VerticalLayout padding='20,20,0,20' background-color='@menucolor' name='HelpDialog'>"
//    L"<VerticalLayout padding='0,0,20,0' vscrollbar='false'>"
//      L"<Label textcolor='0xFFFFFFFF' font-size='19' name='DialogText'/>"
//    L"</VerticalLayout>"
//    L"<HorizontalLayout height='50'>"
//      L"<Control/>"
//      L"<Button name='DialogButton' focusbkcolor='@menulightcolor' text='OK' text-align='singleline|center' height='30' width='55' font-size='27' background-color='0xFFCCCCCC' textcolor='0xFFFFFFFF'/>"
//      L"<Control/>"
//    L"</HorizontalLayout>"
//  L"</VerticalLayout>";
//#endif
CHAR16 *mHelpDialogChilds = L""
  L"<VerticalLayout padding='10,0,0,10' background-color='0xFFFFFFFF' name='HelpDialog' height='764' width='1004'>"
    L"<Control background-color='0xFFE5E5E5'/>"
    L"<VerticalLayout name='DialogBorder' padding='2,2,2,2' background-color='0xFF3E8DDD' height='754' width='994' float='true'>"
      L"<VerticalLayout padding='20,20,0,20' background-color='0xFFFFFFFF' name='BodyLayout' min-height='500' height='wrap_content' width='990'>"
        L"<VerticalLayout vscrollbar='false' height='wrap_content' width='match_parent'>"
          L"<Label textcolor='0xFF000000' font-size='24' name='DialogTitle' height='wrap_content' width='match_parent' text-align='center'/>"
          L"<Label textcolor='0xFF000000' font-size='24' name='DialogText' height='wrap_content' width='match_parent'/>"
          L"<HorizontalLayout padding='65,0,51,0' height='wrap_content' width='match_parent'>"
            L"<Control/>"
            L"<Texture name='CopyrightQRcode' width='69' height='69' background-image='@L05Copyright'/>"
            L"<Control/>"
          L"</HorizontalLayout>"
        L"</VerticalLayout>"
        L"<HorizontalLayout height='72'>"
          L"<Control/>"
          L"<Button name='DialogButton' focusbkcolor='0xFF3E8DDD' text='OK' text-align='singleline|center' height='42' width='130' font-size='22' background-color='0xFFE5E5E5' textcolor='0xFF000000' text-align='center|singleline'/>"
          L"<Control/>"
        L"</HorizontalLayout>"
      L"</VerticalLayout>"
    L"</VerticalLayout>"
  L"</VerticalLayout>";
//_End_L05_GRAPHIC_UI_

LRESULT
H2OHelpDialogProc (
  HWND                          Wnd,
  UINT                          Msg,
  WPARAM                        WParam,
  LPARAM                        LParam
  )
{
  UI_DIALOG                     *Dialog;
  UI_CONTROL                    *Control;
  CHAR16                        *BodyString;
//_Start_L05_GRAPHIC_UI_
  RECT                          ScreenRect;
  UI_CONTROL                    *HelpDialog;
  SIZE                          DialogSize;
  SIZE                          ContentSize;
  CHAR16                        Str[20];
//_End_L05_GRAPHIC_UI_

  Dialog  = (UI_DIALOG *) GetWindowLongPtr (Wnd, 0);

  switch (Msg) {

  case UI_NOTIFY_WINDOWINIT:
//_Start_L05_GRAPHIC_UI_
    Control = UiFindChildByName (Dialog, L"DialogTitle");
    if (mFbDialog->TitleString!= NULL) {
      UiSetAttribute (Control, L"text", mFbDialog->TitleString);
    }
//_End_L05_GRAPHIC_UI_
    Control = UiFindChildByName (Dialog, L"DialogText");
    BodyString = CatStringArray (mFbDialog->BodyStringCount, (CONST CHAR16 **) mFbDialog->BodyStringArray);
    if (BodyString != NULL) {
      UiSetAttribute (Control, L"text", BodyString);
      FreePool (BodyString);
    }

//_Start_L05_GRAPHIC_UI_
    //
    // Adjust dialog's height dynamically
    //
    Control = UiFindChildByName (Dialog, L"BodyLayout");
    if (Control != NULL) {
      ContentSize.cx = Control->FixedSize.cx;
      ContentSize.cy = 9999;
      ContentSize = CONTROL_CLASS (Control)->EstimateSize (Control, ContentSize);
      ContentSize.cy = MAX (ContentSize.cy, Control->MinSize.cy);

      UnicodeSPrint (Str, sizeof (Str), L"%d", ContentSize.cy + 14);
      Control = UiFindChildByName (Dialog, L"HelpDialog");
      if (Control != NULL) {
        UiSetAttribute (Control, L"height", Str);
      }
      UnicodeSPrint (Str, sizeof (Str), L"%d", ContentSize.cy + 4);
      Control = UiFindChildByName (Dialog, L"DialogBorder");
      if (Control != NULL) {
        UiSetAttribute (Control, L"height", Str);
      }
    }

    //
    // Move window position
    //
    GetWindowRect (gWnd, &ScreenRect);
    HelpDialog = UiFindChildByName (Dialog, L"HelpDialog");
    DialogSize = HelpDialog->FixedSize;
    SetWindowPos (
      Wnd,
      HWND_TOP,
      (ScreenRect.right  / 2) - (DialogSize.cx / 2) - 1,
      (ScreenRect.bottom / 2) - (DialogSize.cy / 2),
      DialogSize.cx,
      DialogSize.cy,
      0
      );
//_End_L05_GRAPHIC_UI_

    Control = UiFindChildByName (Dialog, L"DialogButton");
    SetFocus (Control->Wnd);
    break;

  case UI_NOTIFY_CLICK:
    SendShutDNotify ();
    break;

  case WM_HOTKEY:
    if (HIWORD(LParam) == VK_ESCAPE) {
      SendShutDNotify ();
      return 0;
    }
    return 1;

  default:
    return 0;
  }
  return 1;
}

