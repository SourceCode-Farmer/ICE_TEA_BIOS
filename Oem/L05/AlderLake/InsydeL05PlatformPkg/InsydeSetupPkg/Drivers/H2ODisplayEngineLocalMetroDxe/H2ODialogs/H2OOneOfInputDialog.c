/** @file
  UI One Of Input Dialog

;******************************************************************************
;* Copyright (c) 2014 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2ODisplayEngineLocalMetro.h"
#include "UiControls.h"
#include "MetroUi.h"
#include <Library/ConsoleLib.h>

extern H2O_FORM_BROWSER_D       *mFbDialog;
extern BOOLEAN                  mIsSendForm;
extern EFI_HII_HANDLE           mHiiHandle;

VOID
FreeDialogEvent (
  IN H2O_FORM_BROWSER_D                    **DialogEvt
  );

//_Start_L05_GRAPHIC_UI_
//#if FixedPcdGet32(PcdH2OLmdeMultiLayout) == 0
//_End_L05_GRAPHIC_UI_
//_Start_L05_GRAPHIC_UI_
//CHAR16 *mOneOfDialogChilds = L""
//  L"<ListView name='OneOfList' vscrollbar='true' tabstop='true' padding='15,0,0,0' child-padding='2' taborder='1'/>";
CHAR16 *mOneOfDialogChilds = L""
  L"<ListView name='OneOfList' vscrollbar='true' tabstop='true' taborder='1' width='match_parent' height='wrap_content'/>";
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
CHAR16 *mOneOfDialogWithSendFormChilds = L""
  L"<VerticalLayout padding='0,2,2,2' background-color='0xFF000000' name='OneOfDialog' width='match_parent' height='wrap_content'>"
    L"<ListView name='OneOfList' vscrollbar='true' width='match_parent' height='wrap_content' background-color='0xFFFFFFFF'/>"
  L"</VerticalLayout>";
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
//CHAR16 *mOneOfDialogWithoutSendFormChilds = L""
//  L"<VerticalLayout name='OneOfDialog'>"
//    L"<Control/>"
//    L"<HorizontalLayout height='wrap_content'>"
//      L"<Control/>"
//      L"<Control width='wrap_content' height='wrap_content'>"
//        L"<VerticalLayout name='Content' width='wrap_content' height='wrap_content' padding='20,30,20,30' child-padding='15' background-color='@menucolor'>"
//          L"<Label name='DialogTitle' text-align='center' height='wrap_content' width='match_parent' font-size='19' textcolor='0xFFFFFFFF'/>"
//          L"<Control height='10' background-image='@DialogSeparator' background-color='0x0' background-image-style='center'/>"
//          L"<ListView name='OneOfList' vscrollbar='true' width='wrap_content' height='wrap_content' padding='0,0,0,0' child-padding='2'/>"
//        L"</VerticalLayout>"
//        L"<Texture name='FormHalo' float='true' height='-1' width='-1' background-image='@FormHalo' scale9grid='23,26,22,31'/>"
//      L"</Control>"
//      L"<Control/>"
//    L"</HorizontalLayout>"
//    L"<Control/>"
//  L"</VerticalLayout>";
CHAR16 *mOneOfDialogWithoutSendFormChilds = L""
  L"<VerticalLayout name='OneOfDialog'>"
    L"<Control/>"
    L"<HorizontalLayout height='wrap_content'>"
      L"<Control/>"
      L"<VerticalLayout padding='2,2,2,2' name='Content' background-color='0xFF3E8DDD' height='545' width='828'>"
        L"<VerticalLayout padding='26,0,26,35' name='body' background-color='0xFFFFFFFF' height='541' width='824'>"
          L"<Label name='DialogTitle' textcolor='0xFF3E8DDD' font-size='24' height='24' text='Please select boot device:'/>"
          L"<Control height='45'/>"
          L"<ListView name='OneOfList' vscrollbar='true' width='wrap_content' padding='0,0,0,0'/>"
          L"<Control height='45'/>"
          L"<Label name='help' text-align='singleline|center' textcolor='0xFF3E8DDD' font-size='24' height='24' text=' '/>"
        L"</VerticalLayout>"
      L"</VerticalLayout>"
      L"<Control/>"
    L"</HorizontalLayout>"
    L"<Control/>"
  L"</VerticalLayout>";
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
//CHAR16 *mOneOfOptionChilds = L""
//  L"<HorizontalLayout height='40'>"
//    L"<Label name='Option' padding='0,20,0,20' text-align='singleline|center' height='40' font-size='19' background-color='0xFFE6E6E6' textcolor='0xFF404040' text-overflow='ellipsis'/>"
//    L"<Texture name='OptionSelectedIcon' float='true' pos='260,12,281,29' visibility='false' background-image='@DialogSelectedIcon' background-color='0x0'/>"
//  L"</HorizontalLayout>";
CHAR16 *mOneOfOptionChilds = L""
  L"<HorizontalLayout height='40'>"
    L"<Label name='Option' padding='8,0,0,8' text-align='left' height='40' font-size='24' background-color='0xFFFFFFFF' textcolor='0xFF000000' text-overflow='ellipsis'/>"
  L"</HorizontalLayout>";
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
//CHAR16 *mOneOfOptionChildsWithoutSendForm = L""
//  L"<VerticalLayout height='40'>"
//    L"<Label name='Option' text-align='singleline' height='40' padding='0,10,0,10' width='match_parent' font-size='19' background-color='0xFFE6E6E6' textcolor='0xFF404040' text-overflow='ellipsis'/>"
//  L"</VerticalLayout>";
CHAR16 *mOneOfOptionChildsWithoutSendForm = L""
  L"<VerticalLayout height='36' width='762'>"
    L"<Label name='Option' text-align='singleline|left' height='36' width='match_parent' padding='6,0,6,3' font-size='24' background-color='0xFFFFFFFF' textcolor='0xFF000000' text-overflow='ellipsis'/>"
  L"</VerticalLayout>";
//_End_L05_GRAPHIC_UI_
//_Start_L05_GRAPHIC_UI_
//#endif
//_End_L05_GRAPHIC_UI_

VOID
AdjustImage (
  UI_CONTROL                    *Control,
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL *Src,
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL *Dst
  )
{
  INT16                         HueDiff;
  INT8                          SaturationDiff;
  INT8                          ValueDiff;
  CHAR16                        Str[20];
  HSV_VALUE                     SrcHsv;
  HSV_VALUE                     DstHsv;

  RGB2HSV(Src, &SrcHsv);
  RGB2HSV(Dst, &DstHsv);

  if (DstHsv.Hue == 0 && DstHsv.Saturation == 0 && DstHsv.Value == 0) {
    return ;
  }
  HueDiff        = (INT16) (DstHsv.Hue - SrcHsv.Hue);
  SaturationDiff = (INT8)  (DstHsv.Saturation - SrcHsv.Saturation);
  ValueDiff      = (INT8)  (DstHsv.Value - SrcHsv.Value);
  UnicodeSPrint (Str, sizeof (Str), L"%d,%d,%d",  HueDiff, SaturationDiff, ValueDiff);
  UiSetAttribute (Control, L"hsvadjust", Str);
}



VOID
H2OOneOfOptionOnSetState (
  UI_CONTROL                    *Control,
  UI_STATE                      SetState,
  UI_STATE                      ClearState
  )
{
  UI_CONTROL                    *Child;
//_Start_L05_GRAPHIC_UI_
//  COLORREF                      Color;
//  EFI_IMAGE_INPUT               *Image;
  UINTN                         Index;
  INTN                          Result;
  EFI_STATUS                    Status;
//_End_L05_GRAPHIC_UI_

  if (!((SetState & UISTATE_SELECTED) || (ClearState & UISTATE_SELECTED))) {
    return ;
  }

  if (SetState & UISTATE_SELECTED) {
    Child = UiFindChildByName (Control, L"Option");
//_Start_L05_GRAPHIC_UI_
//  UiSetAttribute (Child, L"marquee", L"true");
//  UiApplyAttributeList (Child, L"textcolor='0xFFFFFFFF' background-color='@menulightcolor' ");
//    if (PcdGet32(PcdH2OLmdeMultiLayout) == 1) {
//      UiApplyAttributeList (Child, L"textcolor='0xFF404040'");
//    }
//    if (mIsSendForm) {
//      Child = UiFindChildByName (Control, L"OptionSelectedIcon");
//      ASSERT (Child != NULL);
//      if (Child == NULL) {
//        return;
//      }
//
//      if (IsWindowVisible (Child->Wnd)) {
//        Color = GetColorValue (L"0xFFFFFFFF");
//        Image = GetImageByString (Child->BkImage);
//        if (Image != NULL) {
//          AdjustImage (Child, Image->Bitmap, (EFI_GRAPHICS_OUTPUT_BLT_PIXEL *)&Color);
//        }
//      }
//    }
    if (gFB->CurrentP != NULL) {
      UiApplyAttributeList (Child, L"textcolor='0xFFFFFFFF' background-color='0xFF0F75BC' ");
    } else {
      UiApplyAttributeList (Child, L"textcolor='0xFF000000' background-color='0xFFCFE3F2' ");
    }
//_End_L05_GRAPHIC_UI_

  } else {
    Child = UiFindChildByName (Control, L"Option");
//_Start_L05_GRAPHIC_UI_
//    UiSetAttribute (Child, L"marquee", L"false");
//    UiApplyAttributeList (Child, L"textcolor='0xFF404040' background-color='0xFFE6E6E6' ");
//    if (PcdGet32(PcdH2OLmdeMultiLayout) == 1) {
//      UiApplyAttributeList (Child, L"textcolor='0xFFFFFFFF' background-color='0x0'");
//    }
//    if (mIsSendForm) {
//      Child = UiFindChildByName (Control, L"OptionSelectedIcon");
//      ASSERT (Child != NULL);
//      if (Child == NULL) {
//        return;
//      }
//
//      if (IsWindowVisible (Child->Wnd)) {
//        Color = GetColorValue (L"@menulightcolor");
//        Image = GetImageByString (Child->BkImage);
//        if (Image != NULL) {
//          AdjustImage (Child, Image->Bitmap, (EFI_GRAPHICS_OUTPUT_BLT_PIXEL *)&Color);
//        }
//      }
//    }
    for (Index = 0, Result = 1; Index < mFbDialog->BodyStringCount; Index++) {
      Status = CompareHiiValue (&mFbDialog->BodyHiiValueArray[Index], &mFbDialog->ConfirmHiiValue, &Result);
      if (!EFI_ERROR (Status) && Result == 0) {
        if (StrCmp (((UI_LABEL *) Child)->Text, mFbDialog->BodyStringArray[Index]) == 0 && gFB->CurrentP != NULL) {
          UiApplyAttributeList (Child, L"textcolor='0xFF0F75BC' background-color='0xFFFFFFFF' ");
        } else {
          UiApplyAttributeList (Child, L"textcolor='0xFF000000' background-color='0xFFFFFFFF' ");
        }
        break;
      }
    }
//_End_L05_GRAPHIC_UI_
  }
}

VOID
H2OOneOfOptionOnItemClick (
  UI_LIST_VIEW                  *This,
  UI_CONTROL                    *Item,
  UINT32                        Index
  )
{
  Index = (UINT32) GetWindowLongPtr (Item->Wnd, GWLP_USERDATA);
  SendChangeQNotify (0, 0, &mFbDialog->BodyHiiValueArray[Index]);
}

LRESULT
H2OOneOfInputProc (
  HWND                          Wnd,
  UINT                          Msg,
  WPARAM                        WParam,
  LPARAM                        LParam
  )
{
  UI_CONTROL                    *Layout;
  UI_DIALOG                     *Dialog;
  UI_MANAGER                    *Manager;
  UINTN                         Index;
  UI_CONTROL                    *Control;
//_Start_L05_GRAPHIC_UI_
//  CHAR16                        Str[20];
//  EFI_IMAGE_INPUT               *FormsetImage;
//_End_L05_GRAPHIC_UI_
  UI_CONTROL                    *CreatedControl;
  INTN                          Result;
  EFI_STATUS                    Status;
  INT32                         MaxWidth;
  SIZE                          Size;
  HDC                           Hdc;
  INT32                         Padding;
  UI_CONTROL                    *Content;
  SIZE                          ContentSize;
//_Start_L05_GRAPHIC_UI_
  UI_LIST_VIEW                  *ListView;
  INT32                         CurIndex;
  UI_CONTROL                    *SelectedSetupPageItem;
  UI_CONTROL                    *OptionControl;
  RECT                          OptionControlRect;
  UI_CONTROL                    *OneOfDialog;
  UI_CONTROL                    *OptionValue;
  CHAR16                        Str[40];
  EFI_STRING                    StringPtr;
//_End_L05_GRAPHIC_UI_

  Dialog  = (UI_DIALOG *) GetWindowLongPtr (Wnd, 0);
  Manager = (UI_MANAGER *)((UI_CONTROL *)Dialog)->Manager;

  switch (Msg) {

  case UI_NOTIFY_WINDOWINIT:
    if (mFbDialog->TitleString != NULL) {
      Control = UiFindChildByName (Dialog, L"DialogTitle");
//_Start_L05_GRAPHIC_UI_
      if (Control != NULL) {
        UiSetAttribute (Control, L"text", mFbDialog->TitleString);
      }
//_End_L05_GRAPHIC_UI_
    }

    Control = UiFindChildByName (Dialog, L"Content");
    if (Control != NULL &&  gFB->CurrentQ != NULL) {
      XmlCreateControl (mOneOfDialogChilds, Control);
    }

    Control = UiFindChildByName (Dialog, L"DialogTextScrollView");
    if (Control != NULL) {
      Control->OnSetState = H2OCommonDialogWithHelpOnSetState;
    }

    if ((mFbDialog->DialogType & H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG) == 0 && gFB->CurrentQ != NULL && gFB->CurrentQ->Help != NULL) {
      Control = UiFindChildByName (Dialog, L"DialogText");
//_Start_L05_GRAPHIC_UI_
      if (Control != NULL) {
        UiSetAttribute (Control, L"text", gFB->CurrentQ->Help);
      }
//_End_L05_GRAPHIC_UI_
    }

//_Start_L05_GRAPHIC_UI_
    if (Dialog->XmlBuffer == mOneOfDialogWithoutSendFormChilds) {
      StringPtr = HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_MOVE_SELECTION_STRING), NULL);
      Control = UiFindChildByName (Dialog, L"help");
      if (Control != NULL) {
        UnicodeSPrint (
        Str,
        sizeof (Str),
        StringPtr,
        0x2191,  // Upwards Arrow symbol
        0x2193   // Downwards Arrow symbol
        );
        UiSetAttribute (Control, L"text", Str);
      }
      FreePool (StringPtr);
    }
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
//    FormsetImage = GetCurrentFormSetImage ();
//    if (FormsetImage != NULL) {
//      Control = UiFindChildByName (Dialog, L"DialogImage");
//      UnicodeSPrint (Str, sizeof (Str), L"0x%p", FormsetImage);
//      UiSetAttribute (Control, L"background-image", Str);
//      UiSetAttribute (Control, L"visibility", L"true");
//    }
//_End_L05_GRAPHIC_UI_

    Layout = UiFindChildByName (Dialog, L"OneOfList");
    if (Layout->VScrollBar != NULL) {
      UiSetAttribute (Layout->VScrollBar, L"displayscrollbar", L"false");
      CONTROL_CLASS_INVALIDATE (Layout);
    }

    CreatedControl = NULL;
    ((UI_LIST_VIEW *)Layout)->OnItemClick = H2OOneOfOptionOnItemClick;
    for (Index = 0, Result = 1; Index < mFbDialog->BodyStringCount; Index++) {
      if (gFB->CurrentP != NULL) {
        CreatedControl = XmlCreateControl (mOneOfOptionChilds, Layout);
      } else {
        CreatedControl = XmlCreateControl (mOneOfOptionChildsWithoutSendForm, Layout);
      }
      if (CreatedControl == NULL) {
        continue;
      }

      SetWindowLongPtr (CreatedControl->Wnd, GWLP_USERDATA, (INTN) Index);
      CreatedControl->OnSetState = H2OOneOfOptionOnSetState;

      Control = UiFindChildByName (CreatedControl, L"Option");
      UiSetAttribute (Control, L"text", mFbDialog->BodyStringArray[Index]);
      Status = CompareHiiValue (&mFbDialog->BodyHiiValueArray[Index], &mFbDialog->ConfirmHiiValue, &Result);
      if (!EFI_ERROR (Status) && Result == 0) {
//_Start_L05_GRAPHIC_UI_
//        if (gFB->CurrentP != NULL) {
//          UiSetAttribute (Control, L"padding", L"0,45,0,45");
//        }
//        Control = UiFindChildByName (CreatedControl, L"OptionSelectedIcon");
//        if (Control != NULL) {
//          UiSetAttribute (Control, L"visibility", L"true");
//        }
//_End_L05_GRAPHIC_UI_

        LIST_VIEW_CLASS (Layout)->SetSelection (
                                  (UI_LIST_VIEW *)Layout,
                                  (INT32)Index,
                                  TRUE
                                  );
      }
    }

    if (gFB->CurrentP == NULL && CreatedControl != NULL) {
      //
      // calculate maximum width of option
      //
//_Start_L05_GRAPHIC_UI_
//      MaxWidth = 360;
      MaxWidth = 762;
//_End_L05_GRAPHIC_UI_
      Control = UiFindChildByName (CreatedControl, L"Option");
      Padding = Control->Padding.left + Control->Padding.right;
      Hdc = CreateCompatibleDC(NULL);
      Hdc->font->FontSize = ((UI_LABEL *)Control)->FontSize;
      for (Index = 0; Index < mFbDialog->BodyStringCount; Index++) {
        GetTextExtentPoint32 (Hdc, mFbDialog->BodyStringArray[Index], -1, &Size);
        MaxWidth = MAX (MaxWidth, Size.cx + Padding);
      }
      DeleteDC (Hdc);
      UnicodeSPrint (Str, sizeof (Str), L"%d", MaxWidth);
      UiSetAttribute (Layout, L"width", Str);

      //
      // calculate content width for estimate dialog title size correctly.
      //
      Content = UiFindChildByName (Dialog, L"Content");
      ContentSize.cx = 9999;
      ContentSize.cy = WRAP_CONTENT;
      ContentSize = CONTROL_CLASS (Content)->EstimateSize (Content, ContentSize);
      UnicodeSPrint (Str, sizeof (Str), L"%d", ContentSize.cx);
      UiSetAttribute (Content, L"width", Str);
    }

//_Start_L05_GRAPHIC_UI_
    //
    // Move window position
    //
    if (gFB->CurrentP != NULL) {
      ListView = (UI_LIST_VIEW *) UiFindChildByName (
                                    (UI_CONTROL *) GetWindowLongPtr (gWnd, 0),
                                    L"H2OSetupPagePanel"
                                    );
      CurIndex = ListView->CurSel;
      SelectedSetupPageItem = ((UI_CONTROL *) ListView)->Items[CurIndex];
      
      OptionControl = UiFindChildByName (SelectedSetupPageItem, L"OptionContainer");
      if (OptionControl != NULL) {
        GetWindowRect (OptionControl->Wnd, &OptionControlRect);
        ContentSize.cx = OptionControlRect.right - OptionControlRect.left;
        ContentSize.cy = WRAP_CONTENT;
        OneOfDialog = UiFindChildByName (Dialog, L"OneOfDialog");
        ContentSize = CONTROL_CLASS (OneOfDialog)->EstimateSize ((UI_CONTROL *)OneOfDialog, ContentSize);
        SetWindowPos (
          Wnd,
          HWND_TOP,
          OptionControlRect.left,
          OptionControlRect.bottom,
          OptionControlRect.right - OptionControlRect.left,
          ContentSize.cy,
          0
          );
        
        OptionValue = UiFindChildByName (SelectedSetupPageItem, L"OptionValue");
        UiSetAttribute (OptionValue, L"textcolor", L"0xFF0F75BC");
        Status = EFI_SUCCESS;
      }
    }
//_End_L05_GRAPHIC_UI_
    SetFocus (Layout->Wnd);
    break;

  case WM_HOTKEY:
    if (HIWORD(LParam) == VK_ESCAPE) {
      SendShutDNotify ();
      return 0;
    }
    return 1;

  case WM_DESTROY:
    FreeDialogEvent (&mFbDialog);
    return 0;

  default:
    return 0;
  }


  return 1;
}
