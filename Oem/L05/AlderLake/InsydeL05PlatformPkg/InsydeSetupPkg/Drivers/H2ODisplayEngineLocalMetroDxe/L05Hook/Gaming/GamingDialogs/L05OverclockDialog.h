/** @file
  Provide functions for Gaming Dialog

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_OVERCLOCK_DIALOGS_H_
#define _L05_OVERCLOCK_DIALOGS_H_

#include "H2ODisplayEngineLocalMetro.h"
#include "L05OverclockUi.h"

EFI_STATUS
GetListViewCurSel (
  IN  UI_CONTROL                        *Control,
  IN  HWND                              ItemWnd,
  OUT INT32                             *CurSel
  );

VOID
OverclockSetFocus (
  IN  HWND                              SetFocusWnd,
  IN  INT32                             *Selection
  );

VOID
OverclockSetItemActionFocus (
  IN  HWND                              SetFocusWnd,
  IN  UINTN                             KeyCode
  );

VOID
OverclockUpdateFocusedWnd (
  IN  HWND                              *FocusedWnd
  );

LRESULT
OverclockSettingDialogProc (
  HWND                                  Wnd,
  UINT                                  Msg,
  WPARAM                                WParam,
  LPARAM                                LParam
  );

LRESULT
L05OverclockApplyDialogProc (
  HWND                          Wnd,
  UINT                          Msg,
  WPARAM                        WParam,
  LPARAM                        LParam
  );

extern CHAR16                           *mL05OverclockApplyDialog;

#endif
