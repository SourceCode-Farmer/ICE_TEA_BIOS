/** @file
  UI Gaming Overclock Controls

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2ODisplayEngineLocalMetro.h"
#include "H2OPanels.h"
#include "MetroUi.h"
#include <KernelSetupConfig.h>
#include <L05Config.h>
#include <L05ChipsetNameList.h>
#include "L05OverclockUi.h"
#include <IndustryStandard/SmBios.h>
#include <Guid/L05H2OSetup.h>
#include <Protocol/SetupUtilityBrowser.h>
#include <Protocol/H2ODialog.h>
#include <Protocol/Smbios.h>
#include <Library/UefiLib.h>

HWND                                    mL05OverclockDialogWnd = NULL;

extern HWND                             gWnd;
extern L05_OVERCLOCK_SETUP_DATA         mL05OverclockSetupDataTable[];
extern UINTN                            mL05OverclockSetupDataTableCount;

L05_OVERCLOCK_CONTROL_ITEM_DATA         mL05OverclockControlItemDataTable[] = {
// ItemName,    StringId,                      QuestionId,                     ItemValue, CurrentSpeed, TurboSpeed, ItemUnit, ItemUnitBase, SwitchVisibility, SwitchValue
  {L"Cpu",      STRING_TOKEN (L05_STR_CPU),    L05_KEY_CPU_OVERCLOCKING,       0,         0,            0,          L"Ghz",   1000,         FALSE,            FALSE},
  {L"Gpu",      STRING_TOKEN (L05_STR_GPU),    L05_KEY_GPU_OVERCLOCKING,       0,         0,            0,          L"Ghz",   1000,         FALSE,            FALSE},
  {L"Memory",   STRING_TOKEN (L05_STR_MEMORY), L05_KEY_MEMORY_OVERCLOCKING,    0,         0,            0,          L"Mhz",   1,            FALSE,            FALSE},
  {L"MemoryLP", STRING_TOKEN (L05_STR_MEMORY), L05_KEY_MEMORY_OVERCLOCKING_LP, 0,         0,            0,          L"Mhz",   1,            FALSE,            FALSE},
};

UINTN                                   mL05OverclockControlItemDataTableCount = sizeof (mL05OverclockControlItemDataTable) / sizeof (L05_OVERCLOCK_CONTROL_ITEM_DATA);

EFI_STATUS
L05InitPopupOverclockDialog (
  )
{
  EFI_STATUS                    Status;
  H2O_DIALOG_PROTOCOL           *H2ODialogData;

  Status = gBS->LocateProtocol (&gH2ODialogProtocolGuid, NULL, &H2ODialogData);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  mL05OverclockDialogWnd = L05PopupOverclockDialog ();
  if (mL05OverclockDialogWnd == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  mDialogWnd = mL05OverclockDialogWnd;
  PcdSetBoolS (PcdL05GamingOverClockDialogFlag, TRUE);

  return Status;
}

EFI_STATUS
GetSwitchValueByQuestionId (
  IN EFI_QUESTION_ID                    QuestionId,
  BOOLEAN                               *SwitchValue
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_P                    *CurrentP;
  H2O_FORM_BROWSER_S                    *Statement;
  UINTN                                 Index;

  CurrentP = gFB->CurrentP;
  
  //
  // Get Statement of QuestionId
  //
  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // import formset[FORMSET_ID_GUID_CONFIGURATION].form[0x01].question[QuestionId];
    //
    if (Statement->QuestionId != QuestionId) {
      continue;
    }

    *SwitchValue = (BOOLEAN) Statement->HiiValue.Value.u8;
    break;
  }

  if (Index == CurrentP->NumberOfStatementIds) {
    return EFI_NOT_FOUND;
  }

#if (L05_CHIPSET_VENDOR_ID == L05_AMD_VENDOR_ID)
  //
  // Special Init IsOn
  //
  if (Statement->QuestionId == L05_KEY_CPU_OVERCLOCKING) { // 5:Enable, 0:Normal Operation
    *SwitchValue = (Statement->HiiValue.Value.u8 == 5) ? TRUE : FALSE;
  }

  if ((Statement->QuestionId == L05_KEY_MEMORY_OVERCLOCKING) || (Statement->QuestionId == L05_KEY_MEMORY_OVERCLOCKING_LP)) { // 1:Enable, 0xFF:Auto
    *SwitchValue = (Statement->HiiValue.Value.u8 == 1) ? TRUE : FALSE;
  }
#endif

  return EFI_SUCCESS;
}

EFI_STATUS
OverclockGetControlItemIndex (
  EFI_QUESTION_ID                       QuestionId,
  UINTN                                 *ItemIndex
  )
{
  UINTN                                 Index;

  *ItemIndex = 0xFF;

  for (Index = 0; Index < mL05OverclockControlItemDataTableCount; Index++) {
    if (QuestionId == mL05OverclockControlItemDataTable[Index].QuestionId) {
      break;
    }

    if (Index == mL05OverclockControlItemDataTableCount) {
      return EFI_NOT_FOUND;
    }
  }

  *ItemIndex = Index;

  return EFI_SUCCESS;
}

VOID
UpdateOverclockData (
  IN UI_CONTROL                         *Control,
  L05_OVERCLOCK_CONTROL_ITEM_DATA       *OverclockControlItemData
  )
{
  UI_CONTROL                            *TextChild;
  UI_CONTROL                            *ContentChild;
  UI_CONTROL                            *StatusLayoutChild;
  UI_CONTROL                            *StatusImageChild;
  CHAR16                                String[40];

  TextChild         = NULL;
  ContentChild      = NULL;
  StatusLayoutChild = NULL;
  StatusImageChild  = NULL;

  UnicodeSPrint (String, sizeof (String), L"Overclock%sText", OverclockControlItemData->ItemName);
  TextChild = UiFindChildByName (Control, String);
  if (TextChild == NULL) {
    return;
  }

  UnicodeSPrint (String, sizeof (String), L"Overclock%sContent", OverclockControlItemData->ItemName);
  ContentChild = UiFindChildByName (Control, String);
  if (ContentChild == NULL) {
    return;
  }

  UnicodeSPrint (String, sizeof (String), L"Overclock%sStatusLayout", OverclockControlItemData->ItemName);
  StatusLayoutChild = UiFindChildByName (Control, String);
  if (StatusLayoutChild == NULL) {
    return;
  }

  UnicodeSPrint (String, sizeof (String), L"Overclock%sStatusImage", OverclockControlItemData->ItemName);
  StatusImageChild = UiFindChildByName (Control, String);
  if (StatusImageChild == NULL) {
    return;
  }

  UiSetAttribute (TextChild, L"text", HiiGetString (mHiiHandle, OverclockControlItemData->StringId, NULL));

  if (OverclockControlItemData->SwitchVisibility) {
    SetWindowLongPtr (ContentChild->Wnd, GWLP_USERDATA, (INTN) OverclockControlItemData->ItemValue);
    if (OverclockControlItemData->ItemUnitBase > 1) {
      UnicodeSPrint (
        String,
        sizeof (String),
        L"%d.%d%s",
        OverclockControlItemData->ItemValue / OverclockControlItemData->ItemUnitBase,
        (OverclockControlItemData->ItemValue % OverclockControlItemData->ItemUnitBase) / (OverclockControlItemData->ItemUnitBase / 10),
        OverclockControlItemData->ItemUnit
        );
    } else {
      UnicodeSPrint (
        String,
        sizeof (String),
        L"%d%s",
        OverclockControlItemData->ItemValue,
        OverclockControlItemData->ItemUnit
        );
    }
    UiSetAttribute (ContentChild, L"text", String);

    SetWindowLongPtr (StatusImageChild->Wnd, GWLP_USERDATA, (INTN) OverclockControlItemData->SwitchValue);
    UiSetAttribute (StatusImageChild, L"background-image", OverclockControlItemData->SwitchValue ? L"@L05OverclockSwitchOn" : L"@L05OverclockSwitchOff");
    UiSetAttribute (StatusLayoutChild, L"width", L"50");
    UiSetAttribute (StatusImageChild, L"visibility", L"true");

  } else {
    UiSetAttribute (ContentChild, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_OC_UNAVAILABLE), NULL));
    UiSetAttribute (StatusLayoutChild, L"width", L"1");
    UiSetAttribute (StatusImageChild, L"visibility", L"false");
  }

  return;
}

VOID
HiddenOverclockTemplateLayoutByName (
  IN UI_CONTROL                         *Control,
  CHAR16                                *ItemName
  )
{
  UI_CONTROL                            *LayoutControl;
  CHAR16                                String[40];

  LayoutControl = NULL;

  UnicodeSPrint (String, sizeof (String), L"OverclockTemplate%sLayout", ItemName);
  LayoutControl = UiFindChildByName (Control, String);
  if (LayoutControl == NULL) {
    return;
  }

  UiSetAttribute (LayoutControl, L"visibility", L"false");

  return;
}

VOID
UpdateMemoryLayout (
  IN UI_CONTROL                         *Control
  )
{
  UINTN                                 MemorySpeedIndex;
  UINTN                                 MemorySpeedLpIndex;

  OverclockGetControlItemIndex (L05_KEY_MEMORY_OVERCLOCKING, &MemorySpeedIndex);
  OverclockGetControlItemIndex (L05_KEY_MEMORY_OVERCLOCKING_LP, &MemorySpeedLpIndex);

  if (!mL05OverclockControlItemDataTable[MemorySpeedLpIndex].SwitchVisibility) {
    HiddenOverclockTemplateLayoutByName (Control, mL05OverclockControlItemDataTable[MemorySpeedLpIndex].ItemName);

  } else {
    HiddenOverclockTemplateLayoutByName (Control, mL05OverclockControlItemDataTable[MemorySpeedIndex].ItemName);
  }

}

VOID
OverclockUpdateLayoutData (
  IN UI_CONTROL                         *Control
  )
{
  EFI_STATUS                            Status;
  UINTN                                 CpuSpeedIndex;
  UINTN                                 GpuSpeedIndex;
  UINTN                                 MemorySpeedIndex;
  UINTN                                 MemorySpeedLpIndex;
  UINTN                                 ItemValue;
  UINTN                                 TableIndex;
  UI_CONTROL                            *Child;
  CHAR16                                Str[40];

  if (Control == NULL) {
    return;
  }

  OverclockGetControlItemIndex (L05_KEY_CPU_OVERCLOCKING, &CpuSpeedIndex);
  OverclockGetControlItemIndex (L05_KEY_GPU_OVERCLOCKING, &GpuSpeedIndex);
  OverclockGetControlItemIndex (L05_KEY_MEMORY_OVERCLOCKING, &MemorySpeedIndex);
  OverclockGetControlItemIndex (L05_KEY_MEMORY_OVERCLOCKING_LP, &MemorySpeedLpIndex);

  for (TableIndex = 0; TableIndex < mL05OverclockControlItemDataTableCount; TableIndex++) {
    ItemValue = 0;
    mL05OverclockControlItemDataTable[TableIndex].SwitchVisibility = FALSE;

    Status = GetSwitchValueByQuestionId (mL05OverclockControlItemDataTable[TableIndex].QuestionId, &mL05OverclockControlItemDataTable[TableIndex].SwitchValue);
    if (!EFI_ERROR (Status)) {
      mL05OverclockControlItemDataTable[TableIndex].SwitchVisibility = TRUE;
    }

    ItemValue = mL05OverclockControlItemDataTable[TableIndex].SwitchValue ? mL05OverclockControlItemDataTable[TableIndex].TurboSpeed : mL05OverclockControlItemDataTable[TableIndex].CurrentSpeed;
    mL05OverclockControlItemDataTable[TableIndex].ItemValue = ItemValue;

    UpdateOverclockData (
      Control,
      &mL05OverclockControlItemDataTable[TableIndex]
      );
  }

  UpdateMemoryLayout (Control);

  Child = UiFindChildByName (Control, L"OverclockImage");
  if (Child != NULL) {
    if (mL05OverclockControlItemDataTable[MemorySpeedLpIndex].SwitchVisibility) {
      MemorySpeedIndex = MemorySpeedLpIndex;
    }

    UnicodeSPrint (
      Str,
      sizeof (Str),
      L"@L05OverclockBRY%x%x%x",
      mL05OverclockControlItemDataTable[CpuSpeedIndex].SwitchValue,
      mL05OverclockControlItemDataTable[GpuSpeedIndex].SwitchValue,
      mL05OverclockControlItemDataTable[MemorySpeedIndex].SwitchValue
      );
    UiSetAttribute (Child, L"background-image", Str);
  }

  return;
}

VOID
OverclockInitLayoutData (
  IN  H2O_OWNER_DRAW_PANEL              *This
  )
{
  EFI_STATUS                            Status;
  UINTN                                 ItemIndex;
  EFI_SMBIOS_HANDLE                     SmbiosHandle;
  EFI_SMBIOS_TYPE                       RecordType;
  EFI_SMBIOS_PROTOCOL                   *Smbios;
  EFI_SMBIOS_TABLE_HEADER               *Record;
  SMBIOS_TABLE_TYPE4                    *Type4Record;
  SMBIOS_TABLE_TYPE17                   *Type17Record;
  H2O_FORM_BROWSER_P                    *CurrentP;
  H2O_FORM_BROWSER_S                    *Statement;
  UINTN                                 Index;
  BOOLEAN                               ValuePrefix;
  UINT16                                ValueRecord;

  Smbios       = NULL;
  Record       = NULL;
  Type4Record  = NULL;
  Type17Record = NULL;
  CurrentP     = gFB->CurrentP;
  Statement    = NULL;
  ValuePrefix  = 0;
  ValueRecord  = 0;

  PcdSet64S (PcdL05GamingOverClockSetupDataTable, (UINT64)(UINTN) mL05OverclockSetupDataTable);
  PcdSet32S (PcdL05GamingOverClockSetupDataTableCount, (UINT32) mL05OverclockSetupDataTableCount);

  Status = gBS->LocateProtocol (
                  &gEfiSmbiosProtocolGuid,
                  NULL,
                  (VOID **) &Smbios
                  );

  if (EFI_ERROR (Status) || Smbios == NULL) {
    return;
  }

  //
  // Get CPU Speed from SMBIOS type 4 data
  //
  OverclockGetControlItemIndex (L05_KEY_CPU_OVERCLOCKING, &ItemIndex);
  SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;
  RecordType   = EFI_SMBIOS_TYPE_PROCESSOR_INFORMATION;

  Status = Smbios->GetNext (
                     Smbios,
                     &SmbiosHandle,
                     &RecordType,
                     &Record,
                     NULL
                     );

  if (!EFI_ERROR (Status) && Record != NULL) {
    Type4Record = (SMBIOS_TABLE_TYPE4 *) Record;

    mL05OverclockControlItemDataTable[ItemIndex].CurrentSpeed = Type4Record->CurrentSpeed;
    mL05OverclockControlItemDataTable[ItemIndex].TurboSpeed   = Type4Record->MaxSpeed;
  }

  //
  // Update PcdL05GamingOverClockGpuTurboSpeed
  //
  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    if (Statement->QuestionId != L05_KEY_GPU_CORE_CLOCK_OFFSET_PREFIX) {
      continue;
    }

    ValuePrefix = (BOOLEAN) Statement->HiiValue.Value.u8; // 0:VOLTAGE_POSITIVE_SIGN(+), 1:VOLTAGE_NEGATIVE_SIGN(-)
    break;
  }

  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    if (Statement->QuestionId != L05_KEY_GPU_CORE_CLOCK_OFFSET) {
      continue;
    }

    ValueRecord = Statement->HiiValue.Value.u16;
    break;
  }

  PcdSet16S (PcdL05GamingOverClockGpuTurboSpeed, ValuePrefix ? PcdGet16 (PcdL05GamingOverClockGpuCurrentSpeed) - ValueRecord : PcdGet16 (PcdL05GamingOverClockGpuCurrentSpeed) + ValueRecord); // 0:VOLTAGE_POSITIVE_SIGN(+), 1:VOLTAGE_NEGATIVE_SIGN(-)

  //
  // Get GPU Speed from PCD
  //
  OverclockGetControlItemIndex (L05_KEY_GPU_OVERCLOCKING, &ItemIndex);
  mL05OverclockControlItemDataTable[ItemIndex].CurrentSpeed = PcdGet16 (PcdL05GamingOverClockGpuCurrentSpeed);
  mL05OverclockControlItemDataTable[ItemIndex].TurboSpeed = PcdGet16 (PcdL05GamingOverClockGpuTurboSpeed);

  //
  // Get Memory Speed from MBIOS type 17 data
  //
  SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;
  RecordType   = EFI_SMBIOS_TYPE_MEMORY_DEVICE;
  Status       = EFI_SUCCESS;

  while (!EFI_ERROR (Status)) {
    Status = Smbios->GetNext (
                       Smbios,
                       &SmbiosHandle,
                       &RecordType,
                       &Record,
                       NULL
                       );

    if (EFI_ERROR (Status)) {
      break;
    }

    Type17Record = (SMBIOS_TABLE_TYPE17 *) Record;

    if (Type17Record->ConfiguredMemoryClockSpeed != 0) {
      OverclockGetControlItemIndex (L05_KEY_MEMORY_OVERCLOCKING, &ItemIndex);
      mL05OverclockControlItemDataTable[ItemIndex].CurrentSpeed = Type17Record->ConfiguredMemoryClockSpeed;
      mL05OverclockControlItemDataTable[ItemIndex].TurboSpeed   = Type17Record->ConfiguredMemoryClockSpeed;

      OverclockGetControlItemIndex (L05_KEY_MEMORY_OVERCLOCKING_LP, &ItemIndex);
      mL05OverclockControlItemDataTable[ItemIndex].CurrentSpeed = Type17Record->ConfiguredMemoryClockSpeed;
      mL05OverclockControlItemDataTable[ItemIndex].TurboSpeed   = Type17Record->ConfiguredMemoryClockSpeed;
    }
  }

  return;
}

