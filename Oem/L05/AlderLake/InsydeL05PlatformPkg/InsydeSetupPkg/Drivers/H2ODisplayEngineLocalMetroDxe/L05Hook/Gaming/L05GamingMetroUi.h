/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_GAMING_METRO_UI_H_
#define _L05_GAMING_METRO_UI_H_

#pragma pack(1)
typedef struct _GAMING_FOCUSABLE_WINDOW_DATA {
  HWND                          FocusWnd;
  HWND                          UpWnd;
  HWND                          DownWnd;
  HWND                          LeftWnd;
  HWND                          RightWnd;
} GAMING_FOCUSABLE_WINDOW_DATA;

typedef struct {
  CHAR16                                *ItemName;
  EFI_STRING_ID                         StringId;
  EFI_QUESTION_ID                       QuestionId;
  UINTN                                 ItemValue;
  INT16                                 CurrentSpeed;
  INT16                                 TurboSpeed;
  CHAR16                                *ItemUnit;
  UINTN                                 ItemUnitBase;
  BOOLEAN                               SwitchVisibility;
  BOOLEAN                               SwitchValue;
} L05_OVERCLOCK_CONTROL_ITEM_DATA;
#pragma pack()

typedef enum {
  MoreSettingWnd = 0,
  ExitButtonWnd,
  BootDeviceWnd,
  GraphicDeviceWnd,
  OverclockWnd,
  MaxGamingMainPageWnd
} GAMING_FOCUSABLE_WINDOW_INDEX;

BOOLEAN
IsL05GamingHomePage (
  VOID
  );

VOID
L05GamingSwitchSettingMode (
  VOID
  );

EFI_STATUS
L05GamingMetroProcessUiNotify (
  IN  UI_CONTROL                        *Sender
  );

extern HWND                             mDialogWnd;
extern HWND                             mL05OverclockDialogWnd;

#endif

