/** @file
  Get display resolution for GUI.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_GET_RESOLUTION_H_
#define _L05_GET_RESOLUTION_H_

#include "H2ODisplayEngineLocalMetro.h"

#define L05_GUI_RESOLUTION_BASE_X          1920
#define L05_GUI_RESOLUTION_BASE_Y          1080
#define SCREEN_RATIO(H,V)                  ((H * 100) / V)

//
// Panel's layout define
//
#define L05_GUI_SETUP_MENU_BASE_WIDTH      440

#define L05_GUI_SETUP_PAGE_PADDING_BOTTOM  5

#define L05_GUI_HOTKEY_BASE_HEIGHT         130
#define L05_GUI_HOTKEY_PADDING_TOP         10
#define L05_GUI_HOTKEY_PADDING_BOTTOM      40
#define L05_GUI_HOTKEY_PADDING_LEFT        70
#define L05_GUI_HOTKEY_PADDING_RIGHT       70
#define L05_GUI_HOTKEY_ITEM_BASE_WIDTH     300
#define L05_GUI_HOTKEY_ITEM_BASE_HEIGHT    30

#define L05_GUI_BLUE_BAR_WIDTH             4

//_Start_L05_GAMING_UI_ENABLE_
//
// InformationLayout define
//
#define  INFORMATION_LAYOUT_BASE_WIDTH     600
#define  INFORMATION_LAYOUT_BASE_HEIGHT    280
#define  INFORMATION_LAYOUT_BASE_COORD_X   1250
#define  INFORMATION_LAYOUT_BASE_COORD_Y   340

//
// GamingHotkeyLayout define
//
#define  GAMING_HOTKEY_LAYOUT_BASE_WIDTH   240
#define  GAMING_HOTKEY_LAYOUT_BASE_HEIGHT  400

//
// SetupLayout define
//
#ifndef L05_GAMING_OVERCLOCK_UI_ENABLE
#define  SETUP_LAYOUT_BASE_WIDTH           1400
#else
#define  SETUP_LAYOUT_BASE_WIDTH           1550
#endif
#define  SETUP_LAYOUT_BASE_HEIGHT          420
#ifndef L05_GAMING_OVERCLOCK_UI_ENABLE
#define  SETUP_LAYOUT_BASE_COORD_X         410
#else
#define  SETUP_LAYOUT_BASE_COORD_X         260
#endif
#define  SETUP_LAYOUT_BASE_COORD_Y         660
#define  SETUP_LAYOUT_BASE_TOP_PADDING     110

#define  PERFORMANCE_SECTION_BASE_WIDTH    300

#ifndef L05_GAMING_OVERCLOCK_UI_ENABLE
#define  BOOT_DEVICE_SECTION_BASE_WIDTH    374
#else
#define  BOOT_DEVICE_SECTION_BASE_WIDTH    340
#endif
#define  BOOT_DEVICE_ITEM_BASE_HEIGHT      44
#define  BOOT_DEVICE_CHILD_PADDING         10
#define  MIN_HEIGHT_LEGACY_SETTING         55

#ifndef L05_GAMING_OVERCLOCK_UI_ENABLE
#define  GRAPHIC_DEVICE_SECTION_BASE_WIDTH 364
#else
#define  GRAPHIC_DEVICE_SECTION_BASE_WIDTH 300
#endif

#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
#define  OVERCLOCK_LAYOUT_BASE_WIDTH       390
#endif
//_End_L05_GAMING_UI_ENABLE_

EFI_STATUS
L05GetBestResolution (
  IN     EFI_GRAPHICS_OUTPUT_PROTOCOL    *GraphicsOut, 
  IN OUT UINT32                          *HorizontalResolution, 
  IN OUT UINT32                          *VerticalResolution
  );

#endif

