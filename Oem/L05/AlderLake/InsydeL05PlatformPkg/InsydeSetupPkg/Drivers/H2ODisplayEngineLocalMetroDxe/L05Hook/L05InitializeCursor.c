/** @file
  Initialize L05 cursor image.

;******************************************************************************
;* Copyright (c) 2019 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "L05InitializeCursor.h"
#include <SetupMouse.h>  // Need to include SetupMouse.h here to avoid definition conflict at InvalidateRect()

/**
 Initialize L05 cursor image, reference from SetupMouseCursor.c.

 @param  Private                        None.

 @retval EFI_SUCCESS                    Initialize success.
**/
EFI_STATUS
L05InitializeCursor (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_IMAGE_INPUT                       Image;
  EFI_SETUP_MOUSE_PROTOCOL              *SetupMouse;  
  PRIVATE_MOUSE_DATA                    *Private;

  Status = gBS->LocateProtocol (
                  &gSetupMouseProtocolGuid,
                  NULL,
                  (VOID **) &SetupMouse
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Private = SETUP_MOUSE_DEV_FROM_PROTOCOL (SetupMouse);

  Status = EfiAcquireLockOrFail (&Private->SetupMouseLock);
  ASSERT_EFI_ERROR (Status);

  if (mImageHiiHandle == NULL) {
    AddHiiImagePackage ();
  }

  Status = gHiiImage->GetImage (
                        gHiiImage,
                        mImageHiiHandle,
                        IMAGE_TOKEN (L05_IMAGE_ARROW),
                        &Image
                        );
  if (EFI_ERROR (Status) || Image.Bitmap == NULL) {
    EfiReleaseLock (&Private->SetupMouseLock);
    return Status;
  }

  if ((Image.Flags & H2O_IMAGE_ALPHA_CHANNEL) != H2O_IMAGE_ALPHA_CHANNEL) {
    ConvertToAlphaChannelImage (&Image);
  }

  SetRect (&Private->Cursor.ImageRc, 0, 0, Image.Width, Image.Height);
  Private->Cursor.Image = Image.Bitmap;

  OffsetRect (
    &Private->Cursor.ImageRc,
    (INT32) Private->SaveCursorX,
    (INT32) Private->SaveCursorY
    );

  EfiReleaseLock (&Private->SetupMouseLock);

  return EFI_SUCCESS;
}

/**
  Check if setup mouse is started or not.

  @param  Private                       None.

  @retval TRUE                          Setup mouse started
  @retval FALSE                         Setup mouse closed
**/
BOOLEAN
IsSetupMouseStart (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_SETUP_MOUSE_PROTOCOL              *SetupMouse;  
  PRIVATE_MOUSE_DATA                    *Private;

  Status = gBS->LocateProtocol (
                  &gSetupMouseProtocolGuid,
                  NULL,
                  (VOID **) &SetupMouse
                  );
  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  Private = SETUP_MOUSE_DEV_FROM_PROTOCOL (SetupMouse);

  return Private->IsStart;
}

