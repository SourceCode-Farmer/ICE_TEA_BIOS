/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _GAMING_SETUP_MENU_PANEL_H_
#define _GAMING_SETUP_MENU_PANEL_H_

extern CHAR16                           *mH2OL05GamingSetupMenuTopChilds;
extern CHAR16                           *mH2OL05GamingSetupMenuBottomChilds;

#endif

