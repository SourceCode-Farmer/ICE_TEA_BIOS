/** @file
  UI Gaming Controls

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2ODisplayEngineLocalMetro.h"
#include "H2OPanels.h"
#include "MetroUi.h"
#include "L05Hook/L05GetResolution.h"

#include <L05Config.h>
#include <KernelSetupConfig.h>
#include <Guid/L05H2OSetup.h>
#include <Protocol/SetupUtilityBrowser.h>
#include <Library/FeatureLib/OemSvcGetPerformanceMode.h>
#include <Library/FeatureLib/OemSvcGetGpuInformation.h>
#include <Library/UefiLib.h>

extern HWND                             gLastFocus;
UI_CONTROL                              *gLastHoverControl = NULL;


GAMING_FOCUSABLE_WINDOW_DATA mGamingFocusableWnd[MaxGamingMainPageWnd] = {0};

CHAR16  *mL05GamingOwnerDrawPanelChilds = L""
  L"<Texture name='OwnerDrawBackground' float='true' height='-1' width='-1' background-image='@L05GamingBackground' background-image-style='stretch'/>"

  L"<HorizontalLayout name='ExitButtonLayout' height='80' padding='40,40,0,0'>"
    L"<Label/>"
    L"<HorizontalLayout height='40' width='110'>"
      L"<Button name='ExitButton' text='Exit' textcolor='0xFFFBFDFF' font-size='16' padding='13,0,0,50' normalimage='@L05ButtonNormal' focusedimage='@L05ButtonHover' hoverimage='@L05ButtonHover' pushimage='@L05ButtonHover'  scale9grid='24,20,24,19'/>"
      L"<Texture name='ExitButtonImage' height='20' width='20' background-image='@L05Exit' float='true' pos='20,10,40,30'/>"
    L"</HorizontalLayout>"
  L"</HorizontalLayout>"

  L"<VerticalLayout name='InformationLayout' height='wrap_content' width='600' padding='0,0,0,0'>"
    L"<Texture name='BrandingLogo' height='100' width='match_parent' background-image='@GamingLegion'/>"
    L"<Label height='44'/>"
    L"<VerticalLayout height='wrap_content' width='match_parent'>"
      L"<HorizontalLayout height='wrap_content' width='match_parent' min-height='50'>"
        L"<HorizontalLayout height='wrap_content' min-height='50'>"
          L"<Texture height='30' width='30' background-image='@L05CpuIcon'/>"
          L"<Label name='Cpu' height='20' width='90' textcolor='0xFFFBFDFF' font-size='16' padding='6,0,0,10' text='CPU'/>"
          L"<Label name='CpuInfo' min-height='50' textcolor='0x99F8FBFD' font-size='16' padding='6,7,4,10' text='Invalid'/>"
        L"</HorizontalLayout>"
        L"<HorizontalLayout height='wrap_content' min-height='50'>"
          L"<Texture height='30' width='30' background-image='@L05VersionIcon'/>"
          L"<Label name='BiosVersion' height='20' width='90' textcolor='0xFFFBFDFF' font-size='16' padding='6,7,0,10' text='Bios Version'/>"
          L"<Label name='BiosInfo' min-height='50' textcolor='0x99F8FBFD' font-size='16' padding='6,7,4,10' text='Invalid'/>"
        L"</HorizontalLayout>"
      L"</HorizontalLayout>"
      L"<HorizontalLayout height='wrap_content' width='match_parent' min-height='50'>"
        L"<HorizontalLayout height='wrap_content' min-height='50'>"
          L"<Texture height='30' width='30' background-image='@L05GpuIcon'/>"
          L"<Label name='Gpu' height='20' width='90' textcolor='0xFFFBFDFF' font-size='16' padding='6,0,0,10' text='GPU'/>"
          L"<Label name='GpuInfo' min-height='50' textcolor='0x99F8FBFD' font-size='16' padding='6,7,4,10' text='Invalid'/>"
        L"</HorizontalLayout>"
        L"<HorizontalLayout height='wrap_content' min-height='50'>"
          L"<Texture height='30' width='30' background-image='@L05SnIcon'/>"
          L"<Label name='LenovoSn' height='20' width='90' textcolor='0xFFFBFDFF' font-size='16' padding='6,7,0,10' text='Lenovo SN'/>"
          L"<Label name='LenovoInfo' min-height='50' textcolor='0x99F8FBFD' font-size='16' padding='6,7,4,10' text='Invalid'/>"
        L"</HorizontalLayout>"
      L"</HorizontalLayout>"
    L"</VerticalLayout>"
  L"</VerticalLayout>"

  L"<VerticalLayout name='GamingHotkeyLayout' height='400' width='240' padding='0,0,40,40'>"
    L"<VerticalLayout height='60' width='match_parent' padding='11,0,9,0'>"
      L"<Label name='Arrows' height='20' textcolor='0xFF1381FF' font-size='14' padding='2,0,0,0' text='Arrows'/>"
      L"<Label name='SelectItem' height='20' textcolor='0x99FFFFFF' font-size='14' padding='2,0,0,0' text='Select Item'/>"
    L"</VerticalLayout>"
    L"<VerticalLayout height='60' width='match_parent' padding='11,0,9,0'>"
      L"<Label name='Enter' height='20' textcolor='0xFF1381FF' font-size='14' padding='2,0,0,0' text='Enter'/>"
      L"<Label name='OK' height='20' textcolor='0x99FFFFFF' font-size='14' padding='2,0,0,0' text='OK'/>"
    L"</VerticalLayout>"
    L"<VerticalLayout height='60' width='match_parent' padding='11,0,9,0'>"
      L"<Label name='Esc' height='20' textcolor='0xFF1381FF' font-size='14' padding='2,0,0,0' text='Esc'/>"
      L"<Label name='LeaveBios' height='20' textcolor='0x99FFFFFF' font-size='14' padding='2,0,0,0' text='Leave Bios'/>"
    L"</VerticalLayout>"
    L"<VerticalLayout height='60' width='match_parent' padding='11,0,9,0'>"
      L"<Label name='F2DuringBoot' height='20' textcolor='0xFF1381FF' font-size='14' padding='2,0,0,0' text='F2 During Boot'/>"
      L"<Label name='BiosSetup' height='20' textcolor='0x99FFFFFF' font-size='14' padding='2,0,0,0' text='BIOS Setup'/>"
    L"</VerticalLayout>"
    L"<VerticalLayout height='60' width='match_parent' padding='11,0,9,0'>"
      L"<Label name='F12DuringBoot' height='20' textcolor='0xFF1381FF' font-size='14' padding='2,0,0,0' text='F12 During Boot'/>"
      L"<Label name='BiosDevice' height='20' textcolor='0x99FFFFFF' font-size='14' padding='2,0,0,0' text='BIOS Device'/>"
    L"</VerticalLayout>"
    L"<VerticalLayout height='60' width='match_parent' padding='11,0,9,0'>"
      L"<HorizontalLayout name='Star_en' height='20' width='match_parent'>"
        L"<Texture height='20' width='20' background-image='@L05SuperStar'/>"
        L"<Label name='DuringBoot' height='20' textcolor='0xFF1381FF' font-size='14' padding='4,0,0,0' text='During Boot'/>"
      L"</HorizontalLayout>"
      L"<HorizontalLayout name='Star_cn' height='20' width='match_parent'>"
        L"<Label name='DuringBoot_1' height='20' width='wrap_content' textcolor='0xFF1381FF' font-size='14' padding='4,0,0,0' text='During Boot'/>"
        L"<Texture height='20' width='20' background-image='@L05SuperStar'/>"
        L"<Label name='DuringBoot_2' height='20' width='wrap_content' textcolor='0xFF1381FF' font-size='14' padding='4,0,0,0' text='During Boot'/>"
      L"</HorizontalLayout>"
      L"<Label name='NovoMenu' height='20' textcolor='0x99FFFFFF' font-size='14' padding='2,0,0,0' text='Novo Menu'/>"
    L"</VerticalLayout>"
  L"</VerticalLayout>"

#ifndef L05_GAMING_OVERCLOCK_UI_ENABLE
  L"<HorizontalLayout name='SetupLayout' height='420' width='1400' padding='110,20,40,0'>"
#else
  L"<HorizontalLayout name='SetupLayout' height='420' width='1550' padding='110,20,40,0'>"
#endif
    L"<Label/>"
    L"<Label/>"
    L"<VerticalLayout name='PerformanceSection' height='match_parent' width='300' padding='0,0,0,0'>"
      L"<Label name='PerformanceTitle' height='40' text-align='center' textcolor='0xFFFBFDFF' font-size='28' padding='6,0,0,0' text='Performance Mode'/>"
      L"<Label height='10'/>"
      L"<Label/>"
      L"<HorizontalLayout height='100'>"
        L"<Label/>"
        L"<Texture name='PerformanceImage' height='100' width='100' background-image='@L05Its40ExtremeMode'/>"
        L"<Label/>"
      L"</HorizontalLayout>"
      L"<Label height='10'/>"
      L"<Label name='PerformanceMode' height='20' text-align='center' textcolor='0xFFFF634B' font-size='20' padding='2,0,0,0' text='Extreme Performance'/>"
      L"<Label height='5'/>"
      L"<Label name='Fn+QInWindowsToSwitch' height='20' text-align='center' textcolor='0x99FBFDFF' font-size='14' padding='2,0,0,0' text='Press Fn+Q in Windows to Switch'/>"
      L"<Label/>"
      L"<Label/>"
    L"</VerticalLayout>"
    L"<Label/>"
    L"<Label/>"

#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
    L"<VerticalLayout name='OverclockLayout' width='390' padding='0,0,0,0'>"
      L"<Label name='OverclockingTitle' height='40' text-align='left' textcolor='0xFFFBFDFF' font-size='28' padding='6,0,0,0'/>"
      L"<HorizontalLayout height='20'/>"

      L"<ListView name='OverclockList' height='164' width='match_parent'>"
        L"<HorizontalLayout name='border' height='164' width='match_parent' background-color='0xFF404556' padding='2,2,2,2'>"
          L"<HorizontalLayout name='ItemLayout' height='160' background-color='0xFF242B3F' padding='20,20,20,20'>"

            L"<Texture name='OverclockImage' height='120' width='120' background-image='@L05OverclockBRY000'/>"

            L"<HorizontalLayout height='1' width='10'/>"

            L"<VerticalLayout height='120'>"
              L"<HorizontalLayout height='0'/>"
              L05_OVERCLOCK_DEVICE_TEMPLATE ("@L05OverclockBulletBlue100", "Cpu", "0.0Ghz", "@L05OverclockSwitchOff")
              L"<HorizontalLayout height='0'/>"
              L05_OVERCLOCK_DEVICE_TEMPLATE ("@L05OverclockBulletRed010", "Gpu", "0.0Ghz", "@L05OverclockSwitchOff")
              L"<HorizontalLayout height='0'/>"
              L05_OVERCLOCK_DEVICE_TEMPLATE ("@L05OverclockBulletYellow001", "Memory", "0.0Ghz", "@L05OverclockSwitchOff")
              L05_OVERCLOCK_DEVICE_TEMPLATE ("@L05OverclockBulletYellow001", "MemoryLP", "0.0Ghz", "@L05OverclockSwitchOff")
              L"<HorizontalLayout height='0'/>"
            L"</VerticalLayout>"

          L"</HorizontalLayout>"
        L"</HorizontalLayout>"
      L"</ListView>"
    L"</VerticalLayout>"

    L"<Label/>"
    L"<Label/>"
#endif

#ifndef L05_GAMING_OVERCLOCK_UI_ENABLE
    L"<VerticalLayout name='BootDeviceSection' height='match_parent' width='374' padding='0,0,0,0'>"
      L"<Label name='BootDevice' height='40' width='match_parent' textcolor='0xFFFBFDFF' font-size='28' padding='6,0,0,0' text='Boot Device'/>"
      L"<Label name='BootDeviceTitleSeparator' height='20'/>"
      L"<ListView name='BootDeviceList' height='wrap_content' width='match_parent' vscrollbar='true' child-padding='10' background-color='0x0'/>"
      L"<HorizontalLayout name='LegacySetting' width='match_parent' padding='5,0,0,2'>"
        L"<Button name='LegacySettingButton' height='24' width='75' padding='4,0,0,0' textcolor='0xFF1381FF' font-size='16' text='LEGACY >'/>"
      L"</HorizontalLayout>"
    L"</VerticalLayout>"
#else
    L"<VerticalLayout name='BootDeviceSection' height='match_parent' width='300' padding='0,0,0,0'>"
      L"<Label name='BootDevice' height='40' width='match_parent' textcolor='0xFFFBFDFF' font-size='28' padding='6,0,0,0' text='Boot Device'/>"
      L"<Label name='BootDeviceTitleSeparator' height='20'/>"
      L"<ListView name='BootDeviceList' height='wrap_content' width='match_parent' vscrollbar='true' child-padding='10' background-color='0x0'/>"
      L"<HorizontalLayout name='LegacySetting' width='match_parent' padding='5,0,0,2'>"
        L"<Button name='LegacySettingButton' height='24' width='75' padding='4,0,0,0' textcolor='0xFF1381FF' font-size='16' text='LEGACY >'/>"
      L"</HorizontalLayout>"
    L"</VerticalLayout>"
#endif

    L"<Label/>"

#ifndef L05_GAMING_OVERCLOCK_UI_ENABLE
    L"<VerticalLayout name='GraphicDeviceSection' height='match_parent' width='364' padding='0,0,0,0'>"
#else
    L"<VerticalLayout name='GraphicDeviceSection' height='match_parent' width='200' padding='0,0,0,0'>"
#endif
      L"<Label name='GraphicDevice' height='40' textcolor='0xFFFBFDFF' font-size='28' padding='6,0,0,0' text='Graphic Device'/>"
      L"<Label height='20'/>"
      L"<ListView name='GraphicOptionList' height='wrap_content' width='match_parent' child-padding='10'/>"
      L"<Label height='20'/>"
      L"<Label/>"
      L"<HorizontalLayout height='40'>"
        L"<Label/>"
        L"<Button name='MoreSetting' height='40' width='200' text-align='center' padding='12,0,10,0' textcolor='0xFFFBFDFF' font-size='16' text='More Setting >' normalimage='@L05ButtonNormal' focusedimage='@L05ButtonHover' hoverimage='@L05ButtonHover' pushimage='@L05ButtonHover' scale9grid='24,20,24,19'/>"
      L"</HorizontalLayout>"
    L"</VerticalLayout>"
    L"<Label/>"
  L"</HorizontalLayout>";

CHAR16  *mGamingOwnerDrawBootDeviceItemChilds = L""
#ifndef L05_GAMING_OVERCLOCK_UI_ENABLE
  L"<HorizontalLayout name='border' height='44' width='match_parent' background-color='0xFF404556' padding='2,2,2,2'>"
    L"<HorizontalLayout name='ItemLayout' height='40' background-color='0xFF242B3F'>"
#else
  L"<HorizontalLayout name='border' height='44' width='match_parent' background-color='0xFF404556' padding='2,2,2,2'>"
    L"<HorizontalLayout name='ItemLayout' height='40' background-color='0xFF242B3F'>"
#endif
      L"<Label name='DeviceName' text-align='singleline' textcolor='0xFFFBFDFF' font-size='16' padding='12,0,8,10'/>"
      L"<Button name='UpButton' height='40' width='40' normalimage='@L05UpArrow' focusedimage='@L05UpArrow' hoverimage='@L05UpArrowHover' pushimage='@L05UpArrowHover' disabledimage='@L05UpArrowDisable'/>"
      L"<Button name='DownButton' height='40' width='40' normalimage='@L05DownArrow' focusedimage='@L05DownArrow' hoverimage='@L05DownArrowHover' pushimage='@L05DownArrowHover' disabledimage='@L05DownArrowDisable'/>"
    L"</HorizontalLayout>"
  L"</HorizontalLayout>";

CHAR16  *mGamingOwnerDrawGraphicDeviceItemChilds = L""
#ifndef L05_GAMING_OVERCLOCK_UI_ENABLE
  L"<HorizontalLayout name='border' height='44' width='match_parent' background-color='0xFF404556' padding='2,2,2,2'>"
    L"<HorizontalLayout name='ItemLayout' height='40' background-color='0xFF242B3F'>"
#else
  L"<HorizontalLayout name='border' height='44' width='match_parent' background-color='0xFF404556' padding='2,2,2,2'>"
    L"<HorizontalLayout name='ItemLayout' height='40' background-color='0xFF242B3F'>"
#endif
      L"<Texture name='RaidoButton' height='40' width='40' background-image='@L05RadioOff' background-image-style='stretch' float='true'/>"
      L"<Label name='OptionName' text-align='singleline' textcolor='0xFFFBFDFF' font-size='16' padding='12,0,8,40'/>"
    L"</HorizontalLayout>"
  L"</HorizontalLayout>";

/**
  Determine whether to show Legacy setting button.

  @param  This                          The pointer of the panel.

  @retval None
**/
VOID
ShowLegacySettingButton (
  IN  H2O_OWNER_DRAW_PANEL              *This
  )
{
  EFI_STATUS                            Status;         
  EFI_SETUP_UTILITY_BROWSER_PROTOCOL    *Interface;
  KERNEL_CONFIGURATION                  *MyIfrNvData;
  UI_CONTROL                            *ChildControl;
  BOOLEAN                               ShowButton;
  UI_CONTROL                            *LayoutControl;
  GDICOORD                              MaxListHeight;
  GDICOORD                              ListHeight;
  GDICOORD                              ValueOfHeight;
  SIZE                                  ContentSize;

  Interface      = NULL;
  MyIfrNvData    = NULL;
  ChildControl   = NULL;
  ShowButton     = FALSE;
  LayoutControl  = NULL;
  MaxListHeight  = 0;
  ListHeight     = 0;
  ValueOfHeight  = 0;
  ContentSize.cx = 0;
  ContentSize.cy = 0;

  //
  // Need to show Legacy Setting Button or not.
  //
  if (FeaturePcdGet (PcdH2OCsmSupported)) {
    Status = gBS->LocateProtocol (
                 &gEfiSetupUtilityBrowserProtocolGuid,
                 NULL,
                 (VOID **) &Interface
                 );
    if (!EFI_ERROR (Status)) {
      MyIfrNvData = (KERNEL_CONFIGURATION *) Interface->SCBuffer;

      if (MyIfrNvData->BootType == DUAL_BOOT_TYPE) {
        ShowButton = TRUE;
      }
    }
  }

  ChildControl = UiFindChildByName (This, L"LegacySetting");
  UiSetAttribute (ChildControl, L"visibility", ShowButton ? L"true" : L"false");

  //
  // Adjust the height of Boot Device list
  //
  LayoutControl = UiFindChildByName (This, L"SetupLayout");
  MaxListHeight = LayoutControl->FixedSize.cy - LayoutControl->Padding.top - LayoutControl->Padding.bottom;
  ChildControl  = UiFindChildByName (This, L"BootDevice");
  MaxListHeight -= ChildControl->FixedSize.cy;
  ChildControl  = UiFindChildByName (This, L"BootDeviceTitleSeparator");
  MaxListHeight -= ChildControl->FixedSize.cy;
  MaxListHeight -= ShowButton ? MIN_HEIGHT_LEGACY_SETTING : 0;
  //
  // Do not show half item.
  // 54 = 44(BaseItemHeight) + 10(ChildPadding)
  //
  ValueOfHeight = (BOOT_DEVICE_ITEM_BASE_HEIGHT + BOOT_DEVICE_CHILD_PADDING);
  if ((MaxListHeight % ValueOfHeight) >= BOOT_DEVICE_ITEM_BASE_HEIGHT) {
    //
    // Can show one more item.
    //
    MaxListHeight = ((MaxListHeight / ValueOfHeight) * ValueOfHeight) + BOOT_DEVICE_ITEM_BASE_HEIGHT;
  } else {
    MaxListHeight = ((MaxListHeight / ValueOfHeight) * ValueOfHeight) - BOOT_DEVICE_CHILD_PADDING;
  }

  ChildControl   = UiFindChildByName (This, L"BootDeviceSection");
  ContentSize.cx = ChildControl->FixedSize.cx;
  ContentSize.cy = 9999;
  ChildControl   = UiFindChildByName (This, L"BootDeviceList");
  ChildControl->FixedSize.cy = WRAP_CONTENT;
  ContentSize    = CONTROL_CLASS (ChildControl)->EstimateSize (ChildControl, ContentSize);

  ListHeight = MIN (ContentSize.cy, MaxListHeight);
  ChildControl->FixedSize.cy = ListHeight;

  return;
}

/**
  Change the attribute when the state of the item changes.

  @param  Control                       The pointer of the item.
  @param  SetState                      The state adds to the control.
  @param  ClearState                    The state removes from the control.

  @retval None
**/
VOID
GamingOwnerDrawPanelItemSetState (
  IN  UI_CONTROL                        *Control,
  IN  UI_STATE                          SetState,
  IN  UI_STATE                          ClearState
  )
{
  UI_CONTROL                            *Child;

  Child = NULL;

  if ((SetState & UISTATE_FOCUSED) == UISTATE_FOCUSED || (SetState & UISTATE_HOVER) == UISTATE_HOVER) {
    if ((SetState & UISTATE_HOVER) == UISTATE_HOVER) {
      gLastHoverControl = Control;
    }
    UiSetAttribute (Control, L"background-color", L"0xFF0160D6");
    Child = (UI_CONTROL *)UiFindChildByName (Control, L"ItemLayout");
    if (Child != NULL) {
      UiSetAttribute (Child, L"background-color", L"0xFF414553");
    }
  } else if ((ClearState & UISTATE_FOCUSED) == UISTATE_FOCUSED || (ClearState & UISTATE_HOVER) == UISTATE_HOVER) {
    if (!(((CONTROL_CLASS_GET_STATE(Control) & UISTATE_HOVER) == UISTATE_HOVER && (ClearState & UISTATE_FOCUSED) == UISTATE_FOCUSED) ||
          ((CONTROL_CLASS_GET_STATE(Control) & UISTATE_FOCUSED) == UISTATE_FOCUSED && (ClearState & UISTATE_HOVER) == UISTATE_HOVER))) {
      UiSetAttribute (Control, L"background-color", L"0xFF404556");
      Child = (UI_CONTROL *)UiFindChildByName (Control, L"ItemLayout");
      if (Child != NULL) {
        UiSetAttribute (Child, L"background-color", L"0xFF242B3F");
      }
    }
  }

  return;
}

/**
  Process the event when clicking the item from the list view.

  @param  This                          The pointer of the list view.
  @param  Item                          The pointer of the item clicked from the list.
  @param  Index                         The index of Item in the list.

  @retval None
**/
VOID
GamingItemClick (
  IN  UI_LIST_VIEW                      *This,
  IN  UI_CONTROL                        *Item,
  IN  UINT32                            Index
  )
{
  UI_CONTROL                            *Control;
  H2O_FORM_BROWSER_S                    *Statement;
  H2O_STATEMENT_ID                      StatementId;
  EFI_STATUS                            Status;
  EFI_HII_VALUE                         HiiValue;
  UI_CONTROL                            *RadioButton;
  UINTN                                 Count;

  Control     = (UI_CONTROL *)This;
  Statement   = NULL;
  StatementId = 0;
  RadioButton = NULL;

  gLastFocus = Control->Wnd;
  if (gLastFocus == mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd) {
    StatementId = (H2O_STATEMENT_ID) GetWindowLongPtr (Control->Wnd, GWLP_USERDATA);
#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
  } else if (gLastFocus == mGamingFocusableWnd[OverclockWnd].FocusWnd) {
    L05InitPopupOverclockDialog ();
    return;
#endif
  } else {
    StatementId = (H2O_STATEMENT_ID) GetWindowLongPtr (Item->Wnd, GWLP_USERDATA);
  }
  Status = gFB->GetSInfo (gFB, gFB->CurrentP->PageId, StatementId, &Statement);

  if (EFI_ERROR (Status)) {
    return;
  }

  if (Statement->Selectable) {
    SendSelectQNotify (Statement->PageId, Statement->QuestionId, Statement->IfrOpCode);

    if (Statement->Operand == EFI_IFR_ONE_OF_OP) {
      for (Count = 0; Count < Control->ItemCount; Count++) {
        RadioButton = UiFindChildByName (Control->Items[Count], L"RaidoButton");
        UiSetAttribute (RadioButton, L"background-image", L"@L05RadioOff");
      }

      RadioButton = UiFindChildByName (Item, L"RaidoButton");
      UiSetAttribute (RadioButton, L"background-image", L"@L05RadioOn");
      CopyMem (&HiiValue, &Statement->HiiValue, sizeof (EFI_HII_VALUE));
      HiiValue.Value.u8 = (UINT8) GetWindowLongPtr (Item->Wnd, GWLP_USERDATA);
      SendChangeQNotify (Statement->PageId, Statement->QuestionId, &HiiValue);
    }
  }

  FreePool (Statement);
}

/**
  Process the event when selecting the item from the list view.

  @param  This                          The pointer of the list view.
  @param  Item                          The pointer of the item selected from the list.
  @param  Index                         The index of Item in the list.

  @retval None
**/
VOID
GamingItemSelected (
  IN  UI_LIST_VIEW                      *This,
  IN  UI_CONTROL                        *Item,
  IN  UINT32                            Index
  )
{
  UI_CONTROL                            *Control;
  H2O_FORM_BROWSER_S                    *Statement;
  H2O_STATEMENT_ID                      StatementId;
  EFI_STATUS                            Status;

  Control     = (UI_CONTROL *)This;
  Statement   = NULL;
  StatementId = 0;

  if (gLastFocus != mGamingFocusableWnd[BootDeviceWnd].FocusWnd) {
    StatementId = (H2O_STATEMENT_ID) GetWindowLongPtr (Control->Wnd, GWLP_USERDATA);
  } else {
    StatementId = (H2O_STATEMENT_ID) GetWindowLongPtr (Item->Wnd, GWLP_USERDATA);
  }
  Status = gFB->GetSInfo (gFB, gFB->CurrentP->PageId, StatementId, &Statement);

  if (EFI_ERROR (Status)) {
    return;
  }

  if (Statement->Selectable) {
    SendSelectQNotify (Statement->PageId, Statement->QuestionId, Statement->IfrOpCode);
  }

  FreePool (Statement);
}

/**
  Process the hotkey event of Gaming Home Page.

  @param  Control                       The pointer of UI control.
  @param  KeyCode                       The data of the hotkey.

  @retval None
**/
VOID
L05GamingHotkey (
  IN  UI_CONTROL                        *Control,
  IN  UINTN                             KeyCode
  )
{
  HWND                                  FocusedWnd;
  HWND                                  NextFocusWnd;
  UINTN                                 WndIndex;
  BOOLEAN                               IsGamingHomePgaeWnd;
  UI_LIST_VIEW                          *ListControl;
  UINTN                                 ListItemCount;

  FocusedWnd          = NULL;
  NextFocusWnd        = NULL;
  IsGamingHomePgaeWnd = FALSE;
  ListControl         = NULL;
  ListItemCount       = 0;

  switch (KeyCode) {
  case VK_UP:
  case VK_DOWN:
  case VK_LEFT:
  case VK_RIGHT:
    FocusedWnd = GetFocus ();
    NextFocusWnd = NULL;
    IsGamingHomePgaeWnd = FALSE;

    for (WndIndex = 0; WndIndex < MaxGamingMainPageWnd; WndIndex++) {
      if (FocusedWnd == mGamingFocusableWnd[WndIndex].FocusWnd) {
        IsGamingHomePgaeWnd = TRUE;
        break;
      }
    }

    //
    // Find next focus window.
    //
    if (IsGamingHomePgaeWnd) {
      if (FocusedWnd == mGamingFocusableWnd[BootDeviceWnd].FocusWnd ||
          FocusedWnd == mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd) {
        if (KeyCode == VK_UP || KeyCode == VK_DOWN) {
          ListControl = (UI_LIST_VIEW *) GetWindowLongPtr (FocusedWnd, 0);
          ListItemCount = ((UI_CONTROL*)ListControl)->ItemCount;
          if ((KeyCode == VK_UP && ListControl->CurSel != 0) || 
              (KeyCode == VK_DOWN && ListControl->CurSel != (ListItemCount - 1))) {
            SendMessage (FocusedWnd, WM_KEYDOWN, KeyCode, 0);
            return;
          }
        }
      }

      if (KeyCode == VK_UP) {
        NextFocusWnd = mGamingFocusableWnd[WndIndex].UpWnd;
      } else if (KeyCode == VK_DOWN) {
        NextFocusWnd = mGamingFocusableWnd[WndIndex].DownWnd;
      } else if (KeyCode == VK_LEFT) {
        NextFocusWnd = mGamingFocusableWnd[WndIndex].LeftWnd;
      } else if (KeyCode == VK_RIGHT) {
        NextFocusWnd = mGamingFocusableWnd[WndIndex].RightWnd;
      }
    } else {
      if (gLastFocus != NULL) {
        NextFocusWnd = gLastFocus;
      } else {
        NextFocusWnd = mGamingFocusableWnd[MoreSettingWnd].FocusWnd;
      }
    }

    if (NextFocusWnd != NULL) {
      if (NextFocusWnd == mGamingFocusableWnd[BootDeviceWnd].FocusWnd ||
#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
          NextFocusWnd == mGamingFocusableWnd[OverclockWnd].FocusWnd ||
#endif
          NextFocusWnd == mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd) {
        ListControl = (UI_LIST_VIEW *) GetWindowLongPtr (NextFocusWnd, 0);
        LIST_VIEW_CLASS (ListControl)->SetSelection (ListControl, (ListControl->CurSel >= 0) ? ListControl->CurSel : 0, TRUE);
      }
      SetFocus (NextFocusWnd);
      gLastFocus = NextFocusWnd;

#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
      mL05OverclockDialogWnd = NULL;
#endif
    }
    break;

  default:
    break;
  }

  return;
}

/**
  Auto adjust the layout of Gaming Home Page.

  @param  This                          The pointer of the panel.

  @retval EFI_SUCCESS                   Auto adjust successfully.
  @retval Other                         Auto adjust failed.
**/
EFI_STATUS
GamingAutoAdjustLayout (
  IN  H2O_OWNER_DRAW_PANEL              *This
  )
{
  UI_MANAGER                            *Manager;
  CHAR16                                String[40];
  UI_CONTROL                            *LayoutControl;
  UI_CONTROL                            *ChildControl;
  BOOLEAN                               ExpandX;
  BOOLEAN                               ExpandY;
  UINT32                                BaseX;
  UINT32                                BaseY;
  UINT32                                ScreenX;
  UINT32                                ScreenY;
  UINT32                                ScaleRateX;
  UINT32                                ScaleRateY;
  UINT32                                Width;
  UINT32                                Height;
  UINT32                                CoordX;
  UINT32                                CoordY;
  UINT32                                Padding;

  Manager       = ((UI_CONTROL*) This)->Manager;
  LayoutControl = NULL;
  ChildControl  = NULL;
  ExpandX       = FALSE;
  ExpandY       = FALSE;
  BaseX         = L05_GUI_RESOLUTION_BASE_X;
  BaseY         = L05_GUI_RESOLUTION_BASE_Y;
  ScreenX       = (UINTN) GetSystemMetrics (SM_CXSCREEN);
  ScreenY       = (UINTN) GetSystemMetrics (SM_CYSCREEN);
  ScaleRateY    = 0;
  ScaleRateX    = 0;
  Width         = 0;
  Height        = 0;
  CoordX        = 0;
  CoordY        = 0;
  Padding       = 0;

  //
  // Calculate the scale rate.
  //
  if (ScreenX > BaseX) {
    ExpandX    = TRUE;
    ScaleRateX = (ScreenX * 1000) / BaseX;
  } else if (ScreenX < BaseX) {
    ScaleRateX = (BaseX * 1000) / ScreenX;
  }
  if (ScreenY > BaseY) {
    ExpandY    = TRUE;
    ScaleRateY = (ScreenY * 1000) / BaseY;
  } else if (ScreenY < BaseY) {
    ScaleRateY = (BaseY * 1000) / ScreenY;
  }

  //
  // Adjust Information layout
  //
  LayoutControl = Manager->FindControlByName (Manager, L"InformationLayout");
  Width  = INFORMATION_LAYOUT_BASE_WIDTH;
  Height = INFORMATION_LAYOUT_BASE_HEIGHT;
  CoordX = INFORMATION_LAYOUT_BASE_COORD_X;
  CoordY = INFORMATION_LAYOUT_BASE_COORD_Y;
  if (ScaleRateX > 0) {
    if (ExpandX) {
      CoordX = (CoordX * ScaleRateX) / 1000;
      Width  = (Width  * ScaleRateX) / 1000;
    } else {
      CoordX = (CoordX * 1000) / ScaleRateX;
      Width  = (Width  * 1000) / ScaleRateX;
    }
  }
  if (ScaleRateY > 0) {
    if (ExpandY) {
      CoordY = (CoordY * ScaleRateY) / 1000;
      Height = (Height * ScaleRateY) / 1000;
    } else {
      CoordY = (CoordY * 1000) / ScaleRateY;
      Height = (Height * 1000) / ScaleRateY;
    }
  }
  UnicodeSPrint (
    String,
    sizeof (String),
    L"%d,%d,%d,%d",
    CoordX,
    CoordY,
    CoordX + Width,
    CoordY + Height
    );
  UiSetAttribute (LayoutControl, L"pos", String);

  //
  // Adjust Gaming Hotkey layout
  //
  LayoutControl = Manager->FindControlByName (Manager, L"GamingHotkeyLayout");
  Width  = GAMING_HOTKEY_LAYOUT_BASE_WIDTH;
  Height = GAMING_HOTKEY_LAYOUT_BASE_HEIGHT;
  CoordX = 0;
  CoordY = ScreenY - Height;
  UnicodeSPrint (
    String,
    sizeof (String),
    L"%d,%d,%d,%d",
    CoordX,
    CoordY,
    CoordX + Width,
    CoordY + Height
    );
  UiSetAttribute (LayoutControl, L"pos", String);

  //
  // Adjust Setup layout
  //
  LayoutControl = Manager->FindControlByName (Manager, L"SetupLayout");
  Width  = SETUP_LAYOUT_BASE_WIDTH;
  Height = SETUP_LAYOUT_BASE_HEIGHT;
  CoordX = SETUP_LAYOUT_BASE_COORD_X;
  CoordY = SETUP_LAYOUT_BASE_COORD_Y;
  if (ScaleRateX > 0) {
    if (ExpandX) {
      CoordX = (CoordX * ScaleRateX) / 1000;
      Width  = (Width  * ScaleRateX) / 1000;
    } else {
      CoordX = (CoordX * 1000) / ScaleRateX;
      Width  = (Width  * 1000) / ScaleRateX;
    }
  }
  if (ScaleRateY > 0) {
    if (ExpandY) {
      CoordY = (CoordY * ScaleRateY) / 1000;
      Height = (Height * ScaleRateY) / 1000;
    } else {
      CoordY = (CoordY * 1000) / ScaleRateY;
      Height = (Height * 1000) / ScaleRateY;
    }
  }
  UnicodeSPrint (
    String,
    sizeof (String),
    L"%d,%d,%d,%d",
    CoordX,
    CoordY,
    CoordX + Width,
    CoordY + Height
    );
  UiSetAttribute (LayoutControl, L"pos", String);

  Padding = SETUP_LAYOUT_BASE_TOP_PADDING;
  if (ScaleRateY > 0) {
    if (ExpandY) {
      Padding = (Padding * ScaleRateY) / 1000;
    } else {
      Padding = (Padding * 1000) / ScaleRateY;
    }
  }
  UnicodeSPrint (
    String,
    sizeof (String),
    L"%d,20,40,0",
    Padding
    );
  UiSetAttribute (LayoutControl, L"padding", String);

  ChildControl = UiFindChildByName (LayoutControl, L"PerformanceSection");
  Width = PERFORMANCE_SECTION_BASE_WIDTH;
  if (ScaleRateX > 0) {
    if (ExpandX) {
      Width = (Width * ScaleRateX) / 1000;
    } else {
      Width = (Width * 1000) / ScaleRateX;
    }
  }
  ChildControl->FixedSize.cx = Width;

  ChildControl = UiFindChildByName (LayoutControl, L"BootDeviceSection");
  Width = BOOT_DEVICE_SECTION_BASE_WIDTH;
  if (ScaleRateX > 0) {
    if (ExpandX) {
      Width = (Width * ScaleRateX) / 1000;
    } else {
      Width = (Width * 1000) / ScaleRateX;
    }
  }
  ChildControl->FixedSize.cx = Width;

  ChildControl = UiFindChildByName (LayoutControl, L"GraphicDeviceSection");
  Width = GRAPHIC_DEVICE_SECTION_BASE_WIDTH;
  if (ScaleRateX > 0) {
    if (ExpandX) {
      Width = (Width * ScaleRateX) / 1000;
    } else {
      Width = (Width * 1000) / ScaleRateX;
    }
  }
  ChildControl->FixedSize.cx = Width;

#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
  ChildControl = UiFindChildByName (LayoutControl, L"OverclockLayout");
  Width = OVERCLOCK_LAYOUT_BASE_WIDTH;
  if (ScaleRateX > 0) {
    if (ExpandX) {
      Width = (Width * ScaleRateX) / 1000;
    } else {
      Width = (Width * 1000) / ScaleRateX;
    }
  }
  ChildControl->FixedSize.cx = Width;
#endif

  return EFI_SUCCESS;
}

/**
  Init function for Gaming Home Page.

  @param  This                          The pointer of the panel.

  @retval None
**/
VOID
L05GamingOwnerDrawPanelCreate (
  IN  H2O_OWNER_DRAW_PANEL              *This
  )
{
  EFI_STATUS                            Status;
  UI_CONTROL                            *Control;
  UI_CONTROL                            *Child;
  UI_MANAGER                            *Manager;
  H2O_FORM_BROWSER_P                    *CurrentP;
  H2O_FORM_BROWSER_S                    *Statement;
  UI_CONTROL                            *LayoutControl;
  UI_CONTROL                            *ChildControl;
  UINTN                                 Index;
  CHAR16                                *GpuInfoStr;

  Control       = (UI_CONTROL*) This;
  Child         = NULL;
  Manager       = Control->Manager;
  CurrentP      = gFB->CurrentP;
  Statement     = NULL;
  LayoutControl = NULL;
  ChildControl  = NULL;
  GpuInfoStr    = NULL;

  gLastFocus = NULL;

  XmlCreateControl (mL05GamingOwnerDrawPanelChilds, Control);

  //
  // Auto adjust the layout.
  //
  Status = GamingAutoAdjustLayout (This);

  //
  // Init Imformation section
  //
  LayoutControl = Manager->FindControlByName (Manager, L"InformationLayout");

  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // import formset[FORMSET_ID_GUID_INFORMATION].form[0x01].question[L05_KEY_SYSTEM_BIOS_VERSION];
    //
    if (Statement->Operand != EFI_IFR_ACTION_OP || Statement->QuestionId != L05_KEY_SYSTEM_BIOS_VERSION) {
      continue;
    }

    ChildControl = UiFindChildByName (LayoutControl, L"BiosInfo");
    UiSetAttribute (ChildControl, L"text", Statement->TextTwo);

    break;
  }

  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // import formset[FORMSET_ID_GUID_INFORMATION].form[0x01].question[L05_KEY_SYSTEMSERIAL_NUMBER];
    //
    if (Statement->Operand != EFI_IFR_ACTION_OP || Statement->QuestionId != L05_KEY_SYSTEMSERIAL_NUMBER) {
      continue;
    }

    ChildControl = UiFindChildByName (LayoutControl, L"LenovoInfo");
    UiSetAttribute (ChildControl, L"text", Statement->TextTwo);

    break;
  }

  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // import formset[FORMSET_ID_GUID_INFORMATION].form[0x01].question[L05_KEY_CPU_INFO];
    //
    if (Statement->Operand != EFI_IFR_ACTION_OP || Statement->QuestionId != L05_KEY_CPU_INFO) {
      continue;
    }

    ChildControl = UiFindChildByName (LayoutControl, L"CpuInfo");
    UiSetAttribute (ChildControl, L"text", Statement->TextTwo);

    break;
  }


  ChildControl = UiFindChildByName (LayoutControl, L"GpuInfo");
  Status = OemSvcGetGpuInformation(&GpuInfoStr);
  if (!EFI_ERROR (Status)) {
    if (GpuInfoStr != NULL) {
      UiSetAttribute (ChildControl, L"text", GpuInfoStr);
      FreePool (GpuInfoStr);
    }
  }

  //
  // Init Setup section
  //
  LayoutControl = Manager->FindControlByName (Manager, L"SetupLayout");

  //
  // Init Gaphic Device list window
  //
  ChildControl = UiFindChildByName (LayoutControl, L"GraphicOptionList");
  mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd = ChildControl->Wnd;
  ((UI_LIST_VIEW *) ChildControl)->OnItemClick = GamingItemClick;

  //
  // Init Boot Device list window
  //
  ChildControl = UiFindChildByName (LayoutControl, L"BootDeviceList");
  mGamingFocusableWnd[BootDeviceWnd].FocusWnd = ChildControl->Wnd;
  ((UI_LIST_VIEW *) ChildControl)->OnItemClick = GamingItemClick;
  ((UI_LIST_VIEW *) ChildControl)->OnItemSelected = GamingItemSelected;

  //
  // Init gaming home page button
  //
  ChildControl = UiFindChildByName (LayoutControl, L"MoreSetting");
  mGamingFocusableWnd[MoreSettingWnd].FocusWnd = ChildControl->Wnd;
  ChildControl = Manager->FindControlByName (Manager, L"ExitButton");
  mGamingFocusableWnd[ExitButtonWnd].FocusWnd = ChildControl->Wnd;
  ChildControl = Manager->FindControlByName (Manager, L"ExitButtonImage");
  ChildControl->Wnd->owner = mGamingFocusableWnd[ExitButtonWnd].FocusWnd;

#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
  //
  // Init Over Clocking list window
  //
  ChildControl = UiFindChildByName (LayoutControl, L"OverclockList");
  mGamingFocusableWnd[OverclockWnd].FocusWnd = ChildControl->Wnd;
  ((UI_LIST_VIEW *) ChildControl)->OnItemClick = GamingItemClick;
  SetWindowLongPtr (
    ChildControl->Wnd,
    GWL_EXSTYLE, GetWindowLongPtr (ChildControl->Wnd, GWL_EXSTYLE) & ~WS_EX_NOACTIVATE
    );

  Child = UiFindChildByName (ChildControl, L"border");
  if (Child != NULL) {
    Child->OnSetState = GamingOwnerDrawPanelItemSetState;
  }

  OverclockInitLayoutData (This);
  OverclockUpdateLayoutData (ChildControl);
#endif

  //
  // Init window direction
  //
  mGamingFocusableWnd[MoreSettingWnd].UpWnd     = mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd;
  mGamingFocusableWnd[ExitButtonWnd].DownWnd    = mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd;
  mGamingFocusableWnd[BootDeviceWnd].RightWnd   = mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd;
  mGamingFocusableWnd[GraphicDeviceWnd].UpWnd   = mGamingFocusableWnd[ExitButtonWnd].FocusWnd;
  mGamingFocusableWnd[GraphicDeviceWnd].DownWnd = mGamingFocusableWnd[MoreSettingWnd].FocusWnd;
  mGamingFocusableWnd[GraphicDeviceWnd].LeftWnd = mGamingFocusableWnd[BootDeviceWnd].FocusWnd;
#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
  mGamingFocusableWnd[OverclockWnd].RightWnd = mGamingFocusableWnd[BootDeviceWnd].FocusWnd;
  mGamingFocusableWnd[BootDeviceWnd].LeftWnd    = mGamingFocusableWnd[OverclockWnd].FocusWnd;
#endif

  SetWindowLongPtr (
    Control->Wnd,
    GWL_EXSTYLE, GetWindowLongPtr (Control->Wnd, GWL_EXSTYLE) & ~WS_EX_NOACTIVATE
    );

  return;
}

/**
  Update string for Gaming Home Page.

  @param  This                          The pointer of the panel.

  @retval None
**/
VOID
UpdateGamingOwnerDrawPanel (
  IN  H2O_OWNER_DRAW_PANEL              *This
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_P                    *CurrentP;
  UI_MANAGER                            *Manager;
  UI_CONTROL                            *LayoutControl;
  UI_CONTROL                            *ChildControl;
  H2O_FORM_BROWSER_S                    *Statement;
  UINTN                                 Index;
  L05_PERFORMANCE_MODE                  Mode;
  CHAR8                                 *SCULanguage;
  CHAR8                                 *SCUDefaultLanguage = "en-US";

  CurrentP      = gFB->CurrentP;
  Manager       = ((UI_CONTROL*) This)->Manager;
  LayoutControl = NULL;
  ChildControl  = NULL;
  Statement     = NULL;
  Mode          = MaxPerformanceMode;

  //
  // Get SCU language.
  //
  GetEfiGlobalVariable2 (L"PlatformLang", (VOID **)&SCULanguage, NULL);
  if (SCULanguage == NULL) {
    return ;
  }

  //
  // Update ExitButtonLayout item.
  //
  LayoutControl = Manager->FindControlByName (Manager, L"ExitButtonLayout");
  UiSetAttribute (UiFindChildByName (LayoutControl, L"ExitButton"),  L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_EXIT), NULL));

  //
  // Update InformationLayout item.
  //
  LayoutControl = Manager->FindControlByName (Manager, L"InformationLayout");
  UiSetAttribute (UiFindChildByName (LayoutControl, L"Cpu"),         L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_CPU_INFO), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"BiosVersion"), L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_BIOS_VERSION), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"Gpu"),         L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_GPU_INFO), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"LenovoSn"),    L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_LENOVO_SN), NULL));

  //
  // Update GamingHotkeyLayout item.
  //
  LayoutControl = Manager->FindControlByName (Manager, L"GamingHotkeyLayout");
  UiSetAttribute (UiFindChildByName (LayoutControl, L"Arrows"),        L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_ARROWS), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"SelectItem"),    L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_SELECT_ITEM), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"Enter"),         L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_ENTER), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"OK"),            L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_OK), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"Esc"),           L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_ESC), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"LeaveBios"),     L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_LEAVE_BIOS), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"F2DuringBoot"),  L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_F2_DURING_BOOT), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"BiosSetup"),     L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_BIOS_SETUP), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"F12DuringBoot"), L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_F12_DURING_BOOT), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"BiosDevice"),    L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_BOOT_DEVICE), NULL));

  if (AsciiStrCmp (SCULanguage, SCUDefaultLanguage) == 0) {
    ChildControl = UiFindChildByName (LayoutControl, L"Star_en");
    UiSetAttribute (ChildControl, L"visibility", L"true");
    UiSetAttribute (UiFindChildByName (ChildControl, L"DuringBoot"),    L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_DURING_BOOT), NULL));
    ChildControl = UiFindChildByName (LayoutControl, L"Star_cn");
    UiSetAttribute (ChildControl, L"visibility", L"false");
  } else {
    ChildControl = UiFindChildByName (LayoutControl, L"Star_cn");
    UiSetAttribute (ChildControl, L"visibility", L"true");  
    UiSetAttribute (UiFindChildByName (LayoutControl, L"DuringBoot_1"),  L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_DURING_BOOT_1), NULL));
    UiSetAttribute (UiFindChildByName (LayoutControl, L"DuringBoot_2"),  L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_DURING_BOOT_2), NULL));
    ChildControl = UiFindChildByName (LayoutControl, L"Star_en");
    UiSetAttribute (ChildControl, L"visibility", L"false");
  }
  UiSetAttribute (UiFindChildByName (LayoutControl, L"NovoMenu"),      L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_NOVO_MENU), NULL));

  //
  // Update SetupLayout item.
  //
  LayoutControl = Manager->FindControlByName (Manager, L"SetupLayout");

  //
  // Update Performance Mode from SystemConfig.L05ThermalMode
  //
  ChildControl = UiFindChildByName (LayoutControl, L"PerformanceTitle");

  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // import formset[FORMSET_ID_GUID_CONFIGURATION].form[0x01].question[L05_KEY_THERMAL_MODE];
    //
    if (Statement->Operand != EFI_IFR_ONE_OF_OP || Statement->QuestionId != L05_KEY_THERMAL_MODE) {
      continue;
    }

    switch (Statement->HiiValue.Value.u8) {

    case 0: // Balance
      Mode = Its30BalanceMode;
      break;

    case 1: // Performance
      Mode = Its30PerformanceMode;
      break;

    case 2: // Quiet
      Mode = Its30QuietMode;
      break;

    default:
      Mode = MaxPerformanceMode;
    }

    break;
  }

  Status = OemSvcGetPerformanceMode (Statement->HiiValue.Value.u8, &Mode);

  if (Mode < MaxPerformanceMode) {
    if (Mode >= Its40IntelligentMode && Mode <= Its40SavingMode) {
      UiSetAttribute (ChildControl, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_PERFORMANCE_MODE), NULL));
    } else {
      UiSetAttribute (ChildControl, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_THERMAL_MODE), NULL));
    }
  
    switch (Mode) {
  
    //
    // ITS 4.0
    //
    case Its40ExtremeMode:
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceImage");
      UiSetAttribute (ChildControl, L"background-image", L"@L05Its40ExtremeMode");
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceMode");
      UiSetAttribute (ChildControl, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_EXTREME_PERFORMANCE), NULL));
      UiSetAttribute (ChildControl, L"textcolor", L"0xFFFF634B");
      break;
  
    case Its40IntelligentMode:
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceImage");
      UiSetAttribute (ChildControl, L"background-image", L"@L05Its40IntelligentMode");
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceMode");
      UiSetAttribute (ChildControl, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_INTELLIGENT_COOLING), NULL));
      UiSetAttribute (ChildControl, L"textcolor", L"0xFF257EFF");
      break;
  
    case Its40SavingMode:
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceImage");
      UiSetAttribute (ChildControl, L"background-image", L"@L05Its40SavingMode");
      ChildControl = UiFindChildByName (LayoutControl, HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_BETTERY_SAVING), NULL));
      UiSetAttribute (ChildControl, L"text", L"Battery Saving");
      UiSetAttribute (ChildControl, L"textcolor", L"0xFF32D489");
      break;
  
    //
    // ITS 3.0
    //
    case Its30PerformanceMode:
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceImage");
      UiSetAttribute (ChildControl, L"background-image", L"@L05Its30Performance");
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceMode");
      UiSetAttribute (ChildControl, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_PERFORMANCE), NULL));
      UiSetAttribute (ChildControl, L"textcolor", L"0xFFFF634B");
      break;
  
    case Its30BalanceMode:
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceImage");
      UiSetAttribute (ChildControl, L"background-image", L"@L05Its30Balance");
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceMode");
      UiSetAttribute (ChildControl, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_BALANCE), NULL));
      UiSetAttribute (ChildControl, L"textcolor", L"0xFF257EFF");
      break;
  
    case Its30QuietMode:
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceImage");
      UiSetAttribute (ChildControl, L"background-image", L"@L05Its30Quiet");
      ChildControl = UiFindChildByName (LayoutControl, L"PerformanceMode");
      UiSetAttribute (ChildControl, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_QUIET), NULL));
      UiSetAttribute (ChildControl, L"textcolor", L"0xFF32D489");
      break;
  
    default:
      break;
    }
  }

  UiSetAttribute (UiFindChildByName (LayoutControl, L"Fn+QInWindowsToSwitch"), L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_PRESS_FN_Q_IN_WINDOWS_TO_SWITCH), NULL));
#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
  UiSetAttribute (UiFindChildByName (LayoutControl, L"OverclockingTitle"),     L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_OVERCLOCKING_TITLE), NULL));
#endif
  UiSetAttribute (UiFindChildByName (LayoutControl, L"BootDevice"),            L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_BOOT_DEVICE), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"GraphicDevice"),         L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_GRAPHIC_DEVICE), NULL));
  UiSetAttribute (UiFindChildByName (LayoutControl, L"MoreSetting"),           L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_MORE_SETTINGS), NULL));

  return;
}

/**
  Refresh the lists of Gaming Home Page.

  @param  This                          The pointer of the panel.

  @retval None
**/
VOID
L05GamingRefreshList (
  IN  H2O_OWNER_DRAW_PANEL              *This
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_P                    *CurrentP;
  H2O_FORM_BROWSER_S                    *Statement;
  UI_CONTROL                            *ListControl;
  UI_CONTROL                            *OptionControl;
  UINTN                                 Index;
  UINTN                                 BootDeviceCount;
  BOOLEAN                               HaveGraphicDevice;
  BOOLEAN                               HaveBootDevice;

  CurrentP          = gFB->CurrentP;
  Statement         = NULL;
  ListControl       = NULL;
  OptionControl     = NULL;
  BootDeviceCount   = 0;
  HaveGraphicDevice = FALSE;
  HaveBootDevice    = FALSE;
  gLastHoverControl = NULL;

  //
  // Refresh Gaphic Device list option
  //
  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // import formset[FORMSET_ID_GUID_CONFIGURATION].form[0x01].question[L05_KEY_SPECIAL_VGA_FEATURE];
    //
    if (Statement->Operand != EFI_IFR_ONE_OF_OP || Statement->QuestionId != L05_KEY_SPECIAL_VGA_FEATURE) {
      continue;
    }

    break;
  }

  ListControl = (UI_CONTROL*) GetWindowLongPtr (mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd, 0);
  CONTROL_CLASS(ListControl)->RemoveAllChild ((UI_CONTROL *)ListControl);
  if (Statement->Operand == EFI_IFR_ONE_OF_OP && Statement->QuestionId == L05_KEY_SPECIAL_VGA_FEATURE) {
    HaveGraphicDevice = TRUE;
    SetWindowLongPtr (ListControl->Wnd, GWLP_USERDATA, Statement->StatementId);
    for (Index = 0; Index < Statement->NumberOfOptions; Index++) {
      OptionControl = XmlCreateControl (mGamingOwnerDrawGraphicDeviceItemChilds, ListControl);
      UiSetAttribute (UiFindChildByName (OptionControl, L"OptionName"), L"text", Statement->Options[Index].Text);
      OptionControl->OnSetState = GamingOwnerDrawPanelItemSetState;
      SetWindowLongPtr (OptionControl->Wnd, GWLP_USERDATA, Statement->Options[Index].HiiValue.Value.u8);

      if (Statement->HiiValue.Value.u8 == Statement->Options[Index].HiiValue.Value.u8) {
        UiSetAttribute (UiFindChildByName (OptionControl, L"RaidoButton"), L"background-image", L"@L05RadioOn");
      }
    }
  } else {
    //
    // Show UMA Graphic item
    //
    HaveGraphicDevice = TRUE;
    OptionControl = XmlCreateControl (mGamingOwnerDrawGraphicDeviceItemChilds, ListControl);
    UiSetAttribute (UiFindChildByName (OptionControl, L"OptionName"), L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_GAMING_UMA_GRAPHICS), NULL));
    OptionControl->OnSetState = GamingOwnerDrawPanelItemSetState;
    UiSetAttribute (UiFindChildByName (OptionControl, L"RaidoButton"), L"background-image", L"@L05RadioOn");
  }
  UiSetAttribute (ListControl, L"visibility", HaveGraphicDevice ? L"true" : L"false");

  //
  // Refresh Boot Device list option
  //
  ListControl = (UI_CONTROL*) GetWindowLongPtr (mGamingFocusableWnd[BootDeviceWnd].FocusWnd, 0);
  LIST_VIEW_CLASS (ListControl)->SetSelection ((UI_LIST_VIEW *)ListControl, -1, TRUE); // Clear the selection
  CONTROL_CLASS(ListControl)->RemoveAllChild ((UI_CONTROL *)ListControl);
  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    if (Statement->Operand != EFI_IFR_ACTION_OP  || Statement->QuestionId < KEY_EFI_BOOT_DEVICE_BASE || Statement->QuestionId >= KEY_ADV_LEGACY_BOOT_BASE) {
      continue;
    }
    OptionControl = XmlCreateControl (mGamingOwnerDrawBootDeviceItemChilds, ListControl);
    UiSetAttribute (UiFindChildByName (OptionControl, L"DeviceName"), L"text", Statement->Prompt);
    OptionControl->OnSetState = GamingOwnerDrawPanelItemSetState;
    SetWindowLongPtr (OptionControl->Wnd, GWLP_USERDATA, Statement->StatementId);
    SetWindowLongPtr ((UiFindChildByName (OptionControl, L"UpButton"))->Wnd, GWLP_USERDATA, Statement->StatementId);
    SetWindowLongPtr ((UiFindChildByName (OptionControl, L"DownButton"))->Wnd, GWLP_USERDATA, Statement->StatementId);

    if (Statement->StatementId == gFB->CurrentQ->StatementId) {
      LIST_VIEW_CLASS (ListControl)->SetSelection ((UI_LIST_VIEW *)ListControl, (INT32)BootDeviceCount, TRUE);
    }
    if (!Statement->Selectable) {
      UiSetAttribute (UiFindChildByName (OptionControl, L"DeviceName"), L"textcolor", L"0xFF515151");
      CONTROL_CLASS_SET_STATE(UiFindChildByName (OptionControl, L"UpButton"), UISTATE_DISABLED, 0);
      CONTROL_CLASS_SET_STATE(UiFindChildByName (OptionControl, L"DownButton"), UISTATE_DISABLED, 0);
    }
    BootDeviceCount++;
  }
  if (BootDeviceCount > 0) {
    HaveBootDevice = TRUE;
    CONTROL_CLASS_SET_STATE(UiFindChildByName (ListControl->Items[0], L"UpButton"), UISTATE_DISABLED, 0);
    CONTROL_CLASS_SET_STATE(UiFindChildByName (ListControl->Items[ListControl->ItemCount - 1], L"DownButton"), UISTATE_DISABLED, 0);
  } else {
    HaveBootDevice = FALSE;
  }
  UiSetAttribute (ListControl, L"visibility", HaveBootDevice ? L"true" : L"false");

  //
  // Refresh Legacy Setting button
  //
  ShowLegacySettingButton (This);

  //
  // Update string for Gaming Home Page.
  //
  UpdateGamingOwnerDrawPanel (This);

#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
  //
  // Update Overclock Data.
  //
  ListControl = UiFindChildByName (This, L"OverclockList");
  OverclockUpdateLayoutData (ListControl);
#endif

  //
  // Redirection window
  //
  if (HaveGraphicDevice && HaveBootDevice) {
    mGamingFocusableWnd[MoreSettingWnd].UpWnd     = mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd;
    mGamingFocusableWnd[ExitButtonWnd].DownWnd    = mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd;
    mGamingFocusableWnd[BootDeviceWnd].RightWnd   = mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd;
#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
    mGamingFocusableWnd[BootDeviceWnd].LeftWnd    = mGamingFocusableWnd[OverclockWnd].FocusWnd;
#endif
    mGamingFocusableWnd[BootDeviceWnd].UpWnd      = NULL;
    mGamingFocusableWnd[BootDeviceWnd].DownWnd    = NULL;
    mGamingFocusableWnd[GraphicDeviceWnd].UpWnd   = mGamingFocusableWnd[ExitButtonWnd].FocusWnd;
    mGamingFocusableWnd[GraphicDeviceWnd].DownWnd = mGamingFocusableWnd[MoreSettingWnd].FocusWnd;
    mGamingFocusableWnd[GraphicDeviceWnd].LeftWnd = mGamingFocusableWnd[BootDeviceWnd].FocusWnd;
#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
    mGamingFocusableWnd[OverclockWnd].RightWnd = mGamingFocusableWnd[BootDeviceWnd].FocusWnd;
#endif
  } else if (HaveGraphicDevice && !HaveBootDevice) {
    mGamingFocusableWnd[MoreSettingWnd].UpWnd     = mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd;
    mGamingFocusableWnd[ExitButtonWnd].DownWnd    = mGamingFocusableWnd[GraphicDeviceWnd].FocusWnd;
    mGamingFocusableWnd[GraphicDeviceWnd].UpWnd   = mGamingFocusableWnd[ExitButtonWnd].FocusWnd;
    mGamingFocusableWnd[GraphicDeviceWnd].DownWnd = mGamingFocusableWnd[MoreSettingWnd].FocusWnd;
#ifndef L05_GAMING_OVERCLOCK_UI_ENABLE
    mGamingFocusableWnd[GraphicDeviceWnd].LeftWnd = NULL;
#else
    mGamingFocusableWnd[GraphicDeviceWnd].LeftWnd = mGamingFocusableWnd[OverclockWnd].FocusWnd;;
    mGamingFocusableWnd[OverclockWnd].LeftWnd  = NULL;
#endif
  } else if (!HaveGraphicDevice && HaveBootDevice) {
    mGamingFocusableWnd[MoreSettingWnd].UpWnd     = mGamingFocusableWnd[BootDeviceWnd].FocusWnd;
    mGamingFocusableWnd[ExitButtonWnd].DownWnd    = mGamingFocusableWnd[BootDeviceWnd].FocusWnd;
    mGamingFocusableWnd[BootDeviceWnd].UpWnd      = mGamingFocusableWnd[ExitButtonWnd].FocusWnd;
    mGamingFocusableWnd[BootDeviceWnd].DownWnd    = mGamingFocusableWnd[MoreSettingWnd].FocusWnd;
    mGamingFocusableWnd[BootDeviceWnd].RightWnd   = NULL;
#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
    mGamingFocusableWnd[BootDeviceWnd].LeftWnd    = mGamingFocusableWnd[OverclockWnd].FocusWnd;
    mGamingFocusableWnd[OverclockWnd].RightWnd = mGamingFocusableWnd[BootDeviceWnd].FocusWnd;
#endif
  } else {
    mGamingFocusableWnd[MoreSettingWnd].UpWnd     = mGamingFocusableWnd[ExitButtonWnd].FocusWnd;
    mGamingFocusableWnd[ExitButtonWnd].DownWnd    = mGamingFocusableWnd[MoreSettingWnd].FocusWnd;
  }

  return;
}

