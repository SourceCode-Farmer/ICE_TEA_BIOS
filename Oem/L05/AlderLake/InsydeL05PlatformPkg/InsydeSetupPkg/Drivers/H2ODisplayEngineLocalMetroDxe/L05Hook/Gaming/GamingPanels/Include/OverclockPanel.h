/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OVERCLOCK_PANEL_H_
#define _OVERCLOCK_PANEL_H_

#define L05_OVERCLOCK_DEVICE_TEMPLATE(IMAGE, NAME, TEXT, STATUS) \
  L"<HorizontalLayout name='OverclockTemplate" L##NAME L"Layout' height='wrap_content'>" \
    L"<Texture name='Overclock" L##NAME L"BulletImage' height='12' width='12' background-image='" L##IMAGE L"' background-image-style='stretch' padding='6,0,0,0'/>" \
    L"<HorizontalLayout width='5'/>" \
    L"<HorizontalLayout padding='0,0,0,0'>" \
      L"<Label name='Overclock" L##NAME L"Text' height='24' width='60' textcolor='0xFFFBFDFF' font-size='16' text='" L##NAME L"'/>" \
      L"<HorizontalLayout width='10'/>" \
      L"<Label name='Overclock" L##NAME L"Content' height='24' textcolor='0xFFFBFDFF' font-size='16' text='" L##TEXT L"'/>" \
    L"</HorizontalLayout>" \
    L"<HorizontalLayout name='Overclock" L##NAME L"StatusLayout' height='24' width='50'>" \
      L"<Texture name='Overclock" L##NAME L"StatusImage' height='24' width='50' background-image='" L##STATUS L"' background-image-style='stretch'/>" \
    L"</HorizontalLayout>" \
  L"</HorizontalLayout>"

EFI_STATUS
L05InitPopupOverclockDialog (
  );

VOID
OverclockInitLayoutData (
  IN  H2O_OWNER_DRAW_PANEL              *This
  );

VOID
OverclockUpdateLayoutData (
  IN UI_CONTROL                         *Control
  );

HWND
L05PopupOverclockDialog (
  );

#endif

