/** @file
  Provide function of Gaming Popup Dialog

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2ODisplayEngineLocalMetro.h"
#include "MetroUi.h"
#include "MetroDialog.h"

extern H2O_FORM_BROWSER_D               *mFbDialog;

CHAR16 *mL05GamingExitPopupDialog = L""
  L"<VerticalLayout name='PopUpDialog' height='270' width='500' background-image='@L05DialogBackground' scale9grid='5,5,5,5'>"
    L"<VerticalLayout name='BodyLayout' padding='30,40,0,40' background-color='0x0' height='match_parent' width='match_parent'>"
      L"<Label textcolor='0xFF194A65' font-size='28' name='DialogTitle' height='40' padding='10,0,0,0'/>"
      L"<Label height='20'/>"
      L"<VerticalLayout child-padding='20' height='wrap_content' name='DialogButtonList'/>"
    L"</VerticalLayout>"
  L"</VerticalLayout>";

/**
  Init function for Gaming Exit Dialog.

  @param  UI_DIALOG                     Point to Dialog.

  @retval EFI_SUCCESS                   This function execute successfully.
**/
EFI_STATUS
L05GamingExitDialogInit (
  IN  UI_DIALOG                         *Dialog
  )
{
  EFI_STATUS                            Status;
  UI_CONTROL                            *Control;
  UI_CONTROL                            *DialogControl;
  UI_CONTROL                            *FocusControl;
  UINT32                                Index;
  INTN                                  Result;

  Control       = NULL;
  DialogControl = NULL;
  FocusControl  = NULL;
  Result        = 0;

  if (mFbDialog->TitleString != NULL) {
    Control = UiFindChildByName (Dialog, L"DialogTitle");
    UiSetAttribute (Control, L"text", mFbDialog->TitleString);
  }

  if (mFbDialog->ButtonCount == 0) {
    return EFI_SUCCESS;
  }

  DialogControl = UiFindChildByName (Dialog, L"DialogButtonList");

  FocusControl = NULL;
  for (Index = 0; Index < mFbDialog->ButtonCount; Index++) {
    if (mFbDialog->ButtonStringArray[Index] == NULL) {
      continue;
    }

    Control = CreateControl (L"Button", DialogControl);
    UiSetAttribute (Control, L"text",  mFbDialog->ButtonStringArray[Index]);
    SetWindowLongPtr (Control->Wnd, GWLP_USERDATA, (INTN) Index);

    UiApplyAttributeList (Control, L"name='Button' height='40' font-size='22' textcolor='0xFFFBFDFF' text-align='center|singleline' normalimage='@L05ButtonNormal2' focusedimage='@L05ButtonHover2' hoverimage='@L05ButtonHover2' pushimage='@L05ButtonHover2' scale9grid='24,20,24,19'");
    UiSetAttribute (Control, L"width", L"420");

    if (Index == 0) {
      UiSetAttribute (Control, L"normalimage", L"@L05ButtonNormal");
    }

    Status = CompareHiiValue (&mFbDialog->ButtonHiiValueArray[Index], &mFbDialog->ConfirmHiiValue, &Result);
    if (!EFI_ERROR(Status) && Result == 0) {
      FocusControl = Control;
    }
    CONTROL_CLASS(DialogControl)->AddChild (DialogControl, Control);
  }

  if (FocusControl != NULL) {
    SetFocus (FocusControl->Wnd);
  }

  return EFI_SUCCESS;
}

