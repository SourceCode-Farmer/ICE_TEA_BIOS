/** @file
  UI Gaming Controls

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2ODisplayEngineLocalMetro.h"
#include "H2OPanels.h"

CHAR16 *mH2OL05GamingSetupMenuTopChilds = L""
  L"<VerticalLayout padding='20,0,0,20' height='70'>"
    L"<HorizontalLayout width='200'>"
      L"<Button name='BackButton' height='30' textcolor='0xFF257EFF' font-size='18' padding='2,0,0,30'/>"
      L"<HorizontalLayout width='30' height='30' float='true'>"
        L"<Texture height='21' width='21' background-image='@L05Back'/>"
      L"</HorizontalLayout>"
    L"</HorizontalLayout>"
  L"</VerticalLayout>";

CHAR16 *mH2OL05GamingSetupMenuBottomChilds = L""
  L"<VerticalLayout background-color='0xFFFFFFFF' padding='0,0,0,70'>"
    L"<Control />"
#ifndef L05_SMB_BIOS_ENABLE
    L"<Texture name='BrandingLogo' width='315' height='85' background-image='@Yoga'/>"
#else
    L"<Texture name='BrandingLogo' width='315' height='109' background-image='@LenovoBranding'/>"
#endif
    L"<Control height='42' />"
  L"</VerticalLayout>";
