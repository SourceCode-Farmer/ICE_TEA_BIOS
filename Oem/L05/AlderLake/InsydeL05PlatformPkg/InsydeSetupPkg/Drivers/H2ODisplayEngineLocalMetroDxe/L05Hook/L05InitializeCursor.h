/** @file
  Initialize L05 cursor image.

;******************************************************************************
;* Copyright (c) 2019 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_SETUP_MOUSE_H_
#define _L05_SETUP_MOUSE_H_

#include <InternalFormRepresentation.h>
#include <Library/HiiLib.h>
#include <Library/UefiHiiServicesLib.h>
#include <Library/BitBltLib.h>
#include <Library/RectLib.h>

#define SETUP_MOUSE_DEV_FROM_PROTOCOL(a) \
  CR(a, PRIVATE_MOUSE_DATA, SetupMouse, SETUP_MOUSE_SIGNATURE)

//
// Refer to InsydeSetupPkg\Drivers\H2ODisplayEngineLocalMetroDxe\MetroUi.h
//
VOID
AddHiiImagePackage (
  VOID
  );

//
// Public Functions
//
EFI_STATUS
L05InitializeCursor (
  VOID
  );

BOOLEAN
IsSetupMouseStart (
  VOID
  );

extern EFI_HII_HANDLE                   mImageHiiHandle;

#endif
