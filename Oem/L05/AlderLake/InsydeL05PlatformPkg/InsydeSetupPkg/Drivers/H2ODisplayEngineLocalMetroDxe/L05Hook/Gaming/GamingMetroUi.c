/** @file
  Initial functions for Gaming Home page

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2ODisplayEngineLocalMetro.h"
#include "MetroUi.h"

#include <KernelSetupConfig.h>
#include <L05SetupConfig.h>

extern HWND                             gLastFocus;
extern UI_CONTROL                       *gLastHoverControl;

/**
  Switch to Boot page.

  @param None

  @retval None
**/
VOID
GamingSelectBootPage (
  VOID
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_SM                   *SetupMenuData;
  UINTN                                 Index;

  SetupMenuData = NULL;

  ASSERT (gFB->CurrentP != NULL);
  gLastFocus = NULL;

  Status = gFB->GetSMInfo (gFB, &SetupMenuData);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR(Status)) {
    return;
  }

  for (Index = 0; Index < SetupMenuData->NumberOfSetupMenus; Index++) {
    if (StrCmp (SetupMenuData->SetupMenuInfoList[Index].PageTitle, L"Boot") == 0) { // STR_BOOT_TITLE
      SendSelectPNotify (SetupMenuData->SetupMenuInfoList[Index].PageId);
    }
  }

  return;
}

/**
  Check if the current page is L05 gaming home page.

  @param  None

  @retval TRUE                          Current page is L05 gaming home page.
  @retval FALSE                         Current page is NOT L05 gaming home page.
**/
BOOLEAN
IsL05GamingHomePage (
  VOID
  )
{
  H2O_FORM_BROWSER_SM                   *SetupMenuData;
  EFI_STATUS                            Status;
  EFI_GUID                              L05FrontPage = FORMSET_ID_GUID_GAMING_HOME;

  SetupMenuData = NULL;

  ASSERT (gFB != NULL);

  if (gFB->CurrentP == NULL) {
    return FALSE;
  }

  Status = gFB->GetSMInfo (gFB, &SetupMenuData);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  if (CompareGuid (&SetupMenuData->FormSetGuid, &L05FrontPage)) {
    FreeSetupMenuData (SetupMenuData);
    return TRUE;
  }

  FreeSetupMenuData (SetupMenuData);
  return FALSE;
}

/**
  Switch setting interface mode between gaming UI and common UI.

  @param  None

  @retval None
**/
VOID
L05GamingSwitchSettingMode (
  VOID
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_SM                   *SetupMenuData;

  SetupMenuData = NULL;

  ASSERT (gFB->CurrentP != NULL);
  gLastFocus = NULL;

  Status = gFB->GetSMInfo (gFB, &SetupMenuData);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR(Status)) {
    return;
  }

  if (IsL05GamingHomePage ()) {
    SendSelectPNotify (SetupMenuData->SetupMenuInfoList[1].PageId);
  } else {
    SendSelectPNotify (SetupMenuData->SetupMenuInfoList[0].PageId);
  }

  return;
}

/**
  Process relevant UI events when user activates it.

  @param  Sender                        The pointer of activated UI control.

  @retval EFI_SUCCESS                   The event is processed successfully.
  @retval EFI_UNSUPPORTED               Cannot find the relevant event to the Sender.
  @retval others                        The event is failed to process.
**/
EFI_STATUS
L05GamingMetroProcessUiNotify (
  UI_CONTROL                            *Sender
  )
{
  EFI_STATUS                            Status;
  H2O_STATEMENT_ID                      StatementId;
  H2O_FORM_BROWSER_S                    *Statement;
  HOT_KEY_INFO                          *HotkeyInfo;
  EFI_GUID                              ExitFormsetGuid = FORMSET_ID_GUID_EXIT;
  EFI_GUID                              BootFormsetGuid = FORMSET_ID_GUID_BOOT;

  StatementId = 0;
  Statement   = NULL;
  HotkeyInfo  = NULL;

  if (StrCmp (Sender->Name, L"MoreSetting") == 0 || StrCmp (Sender->Name, L"BackButton") == 0) {
    L05GamingSwitchSettingMode ();
    return EFI_SUCCESS;
  }

  if (StrCmp (Sender->Name, L"ExitButton") == 0) {
    HotkeyInfo = AllocateZeroPool (sizeof (HOT_KEY_INFO));
    CopyMem (&HotkeyInfo->HotKeyTargetFormSetGuid, &ExitFormsetGuid, sizeof (EFI_GUID));
    HotkeyInfo->KeyData.Key.ScanCode = SCAN_ESC;
    HotkeyInfo->KeyData.Key.UnicodeChar = CHAR_NULL;
    HotkeyInfo->HotKeyTargetQuestionId =  0xF0D1; // KEY_SCAN_ESC
    HotkeyInfo->HotKeyAction = HotKeyCallback;
    HotKeyFunc (HotkeyInfo);
    FreePool (HotkeyInfo);
    return EFI_SUCCESS;
  }

  if (StrCmp (Sender->Name, L"UpButton") == 0) {
    if ((CONTROL_CLASS_GET_STATE (Sender) & UISTATE_DISABLED) != 0) {
      return EFI_SUCCESS;
    }
    gLastHoverControl = NULL;
    StatementId = (H2O_STATEMENT_ID) GetWindowLongPtr (Sender->Wnd, GWLP_USERDATA);
    Status = gFB->GetSInfo (gFB, gFB->CurrentP->PageId, StatementId, &Statement);
    ASSERT_EFI_ERROR (Status);
    if (EFI_ERROR (Status)) {
      return Status;
    }
    SendSelectQNotify (Statement->PageId, Statement->QuestionId, Statement->IfrOpCode);

    HotkeyInfo = AllocateZeroPool (sizeof (HOT_KEY_INFO));
    CopyMem (&HotkeyInfo->HotKeyTargetFormSetGuid, &BootFormsetGuid, sizeof (EFI_GUID));
    HotkeyInfo->HotKeyTargetQuestionId =  0x0000;
    HotkeyInfo->HotKeyAction = HotKeyCallback;
    HotkeyInfo->HotKeyHiiValue.Type = 2;
    HotkeyInfo->HotKeyHiiValue.Value.b = 1; // KEY_UP_SHIFT
    HotKeyFunc (HotkeyInfo);
    FreePool (HotkeyInfo);
    return EFI_SUCCESS;
  }

  if (StrCmp (Sender->Name, L"DownButton") == 0) {
    if ((CONTROL_CLASS_GET_STATE (Sender) & UISTATE_DISABLED) != 0) {
      return EFI_SUCCESS;
    }
    gLastHoverControl = NULL;
    StatementId = (H2O_STATEMENT_ID) GetWindowLongPtr (Sender->Wnd, GWLP_USERDATA);
    Status = gFB->GetSInfo (gFB, gFB->CurrentP->PageId, StatementId, &Statement);
    ASSERT_EFI_ERROR (Status);
    if (EFI_ERROR (Status)) {
      return Status;
    }
    SendSelectQNotify (Statement->PageId, Statement->QuestionId, Statement->IfrOpCode);

    HotkeyInfo = AllocateZeroPool (sizeof (HOT_KEY_INFO));
    CopyMem (&HotkeyInfo->HotKeyTargetFormSetGuid, &BootFormsetGuid, sizeof (EFI_GUID));
    HotkeyInfo->HotKeyTargetQuestionId =  0x0000;
    HotkeyInfo->HotKeyAction = HotKeyCallback;
    HotkeyInfo->HotKeyHiiValue.Type = 2;
    HotkeyInfo->HotKeyHiiValue.Value.b = 0; // KEY_DOWN_SHIFT
    HotKeyFunc (HotkeyInfo);
    FreePool (HotkeyInfo);
    return EFI_SUCCESS;
  }

  if (StrCmp (Sender->Name, L"LegacySettingButton") == 0) {
    GamingSelectBootPage ();
    return EFI_SUCCESS;
  }

  return EFI_UNSUPPORTED;
}

