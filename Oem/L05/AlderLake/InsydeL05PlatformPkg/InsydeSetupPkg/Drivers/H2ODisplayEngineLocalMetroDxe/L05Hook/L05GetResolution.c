/** @file
  Get display resolution for GUI.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "L05GetResolution.h"

/**
  Get the best resolution between Gop modes.

  The priority of best resolution from high to low.
  1. The lowest resolution between high resolutions & horizontal equal to HorizontalResolution
  2. The lowest resolution between high resolutions & horizontal NOT equal to HorizontalResolution
  3. The highest resolution between low resolutions & horizontal equal to HorizontalResolution
  4. The highest resolution between low resolutions & horizontal NOT equal to HorizontalResolution

  @param GraphicsOut                     A pointer to the EFI_GRAPHICS_OUTPUT_PROTOCOL instance.
  @param HorizontalResolution            [in]  The base value of horizontal resolution for searching
                                         [out] The best value for horizontal resolution
  @param VerticalResolution              [in]  The base value of vertical resolution for searching
                                         [out] The best value for vertical resolution

  @retval EFI_SUCCESS                    The best resolution is found
  @retval EFI_NOT_FOUND                  Cannot find resolution
**/
EFI_STATUS
L05GetBestResolution (
  IN     EFI_GRAPHICS_OUTPUT_PROTOCOL    *GraphicsOut, 
  IN OUT UINT32                          *HorizontalResolution, 
  IN OUT UINT32                          *VerticalResolution
  )
{
  EFI_STATUS                             Status;
  UINT32                                 Mode;
  UINTN                                  SizeOfInfo;
  EFI_GRAPHICS_OUTPUT_MODE_INFORMATION   *Info;
  UINT32                                 HorizontalBase;
  UINT32                                 VerticalBase;
  UINT32                                 BaseRatio;
  UINT32                                 Index;
  UINT32                                 *Candidate;
  UINT32                                 CandidateCount;
  UINT32                                 *SecondCandidate;
  UINT32                                 SecondCandidateCount;
  UINT32                                 NewQueryH;
  UINT32                                 NewQueryV;
  UINT32                                 BestHorizontal;
  UINT32                                 BestVertical;
  BOOLEAN                                HighResDetected;
  UINT32                                 Value1;
  UINT32                                 Value2;

  HorizontalBase = *HorizontalResolution;
  VerticalBase   = *VerticalResolution;
  BaseRatio      = SCREEN_RATIO (HorizontalBase, VerticalBase);
  CandidateCount       = 0;
  SecondCandidateCount = 0;
  NewQueryH = 0;
  NewQueryV = 0;
  BestHorizontal = 0;
  BestVertical   = 0;
  HighResDetected = FALSE;

  if (GraphicsOut->Mode->MaxMode <= 0) {
    return EFI_NOT_FOUND;
  }
  Candidate = AllocateZeroPool (sizeof (UINT32) * GraphicsOut->Mode->MaxMode);
  SecondCandidate = AllocateZeroPool (sizeof (UINT32) * GraphicsOut->Mode->MaxMode);

  //
  // Find the best resolution with the horizontal same as HorizontalBase.
  //
  for (Mode = 0; Mode < GraphicsOut->Mode->MaxMode; Mode++) {
    Status = GraphicsOut->QueryMode (GraphicsOut, Mode, &SizeOfInfo, &Info);
    if (EFI_ERROR (Status)) {
      continue;
    }
    NewQueryH = Info->HorizontalResolution;
    NewQueryV = Info->VerticalResolution;
    FreePool (Info);
    
    if (NewQueryH >= HorizontalBase && NewQueryV >= VerticalBase) {
      HighResDetected = TRUE;
      if (NewQueryH == HorizontalBase) {
        Candidate[CandidateCount] = NewQueryV;
        CandidateCount++;
      }
    } else if (NewQueryH == HorizontalBase && NewQueryV < VerticalBase){
      SecondCandidate[SecondCandidateCount] = NewQueryV;
      SecondCandidateCount++;
    }
  }

  if (CandidateCount > 0) {
    BestVertical   = Candidate[0];
    for (Index = 1; Index < CandidateCount; Index++) {
      if (Candidate[Index] < BestVertical) {
        BestVertical = Candidate[Index];
      }
    }
  }

  if (SecondCandidateCount > 0 && !HighResDetected) {
    BestVertical   = SecondCandidate[0];
    for (Index = 1; Index < SecondCandidateCount; Index++) {
      if (SecondCandidate[Index] > BestVertical) {
        BestVertical = SecondCandidate[Index];
      }
    }
  }

  if (BestVertical != 0) {
    *VerticalResolution = BestVertical;
    FreePool (Candidate);
    FreePool (SecondCandidate);
    return EFI_SUCCESS;
  }

  //
  // Find the best resolution that close to the base resolution.
  //
  for (Mode = 0; Mode < GraphicsOut->Mode->MaxMode; Mode++) {
    Status = GraphicsOut->QueryMode (GraphicsOut, Mode, &SizeOfInfo, &Info);
    if (EFI_ERROR (Status)) {
      continue;
    }
    NewQueryH = Info->HorizontalResolution;
    NewQueryV = Info->VerticalResolution;
    FreePool (Info);
    
    if (HighResDetected) {
      if (NewQueryH < HorizontalBase || NewQueryV < VerticalBase) {
        continue;
      }
    }

    if (BestHorizontal == 0) {
      BestHorizontal = NewQueryH;
      BestVertical   = NewQueryV;
      continue;
    }

    if ((NewQueryH > BestHorizontal && NewQueryV < BestVertical) ||
        (NewQueryH < BestHorizontal && NewQueryV > BestVertical)) {
      //
      // For special screen, find the resolution that closes to base screen ratio.
      //
      Value1 = abs (SCREEN_RATIO (NewQueryH, NewQueryV) - BaseRatio);
      Value2 = abs (SCREEN_RATIO (BestHorizontal, BestVertical) - BaseRatio);
      if (Value1 < Value2) {
        BestHorizontal = NewQueryH;
        BestVertical   = NewQueryV;
        continue;
      }
    }

    if (HighResDetected) {
      if ((NewQueryH * NewQueryV) < (BestHorizontal * BestVertical)) {
        BestHorizontal = NewQueryH;
        BestVertical   = NewQueryV;
      }
    } else {
      if ((NewQueryH * NewQueryV) > (BestHorizontal * BestVertical)) {
        BestHorizontal = NewQueryH;
        BestVertical   = NewQueryV;
      }
    }
  }

  if (BestHorizontal == 0 || BestVertical == 0) {
    return EFI_NOT_FOUND;
  }

  *HorizontalResolution = BestHorizontal;
  *VerticalResolution   = BestVertical;
  return EFI_SUCCESS;
}
