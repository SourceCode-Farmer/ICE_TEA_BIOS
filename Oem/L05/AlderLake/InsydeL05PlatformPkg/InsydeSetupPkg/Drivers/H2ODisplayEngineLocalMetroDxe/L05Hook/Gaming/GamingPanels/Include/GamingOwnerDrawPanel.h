/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _GAMING_OWNER_DRAW_PANEL_H_
#define _GAMING_OWNER_DRAW_PANEL_H_

extern UI_CONTROL                       *gLastHoverControl;

VOID
L05GamingHotkey (
  IN  UI_CONTROL                        *Control,
  IN  UINTN                             KeyCode
  );

VOID
L05GamingOwnerDrawPanelCreate (
  IN  H2O_OWNER_DRAW_PANEL              *This
  );

VOID
L05GamingRefreshList (
  IN  H2O_OWNER_DRAW_PANEL              *This
  );

#endif

