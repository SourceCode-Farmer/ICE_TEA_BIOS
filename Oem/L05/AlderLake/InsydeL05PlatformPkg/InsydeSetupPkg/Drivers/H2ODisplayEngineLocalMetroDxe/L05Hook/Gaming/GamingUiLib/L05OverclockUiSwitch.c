/** @file
  UI Common Controls

;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "UiControls.h"
#include "H2ODisplayEngineLocalMetro.h"
#include "MetroUi.h"

CHAR16 *mL05GamingSwitchChilds = L""
  L"<Label float='true' padding='3,5,4,5' background-image='@L05CheckBoxBkg' name='CheckboxBkg' scale9grid='14,1,14,1'/>"
  L"<HorizontalLayout float='true' padding='15,10,15,10'>"
    L"<Label name='CheckboxBkgLeft' padding='0,0,0,0' background-color='0xFF257EFF' scale9grid='0,0,0,0'/>"
    L"<Label name='CheckboxBkgRight' padding='0,0,0,0' background-color='0xFF667481' scale9grid='0,0,0,0'/>"
  L"</HorizontalLayout>"
  L"<Label textcolor='0xFFFFFFFF' float='true' font-size='17' name='CheckboxText'/>"
  L"<HorizontalLayout float='true' name='CheckboxButton'>"
    L"<Label name='Button' background-image='@L05CheckBoxBullet'/>"
  L"</HorizontalLayout>";

INT32
L05UiSwitchValueModeUpdateThumbPos (
  IN UI_SWITCH                          *This
  )
{
  INT32                                 ThumbPos;

  if (This == NULL || This->L05ValueModeNumber == 0 || This->ThumbRange == 0) {
    This->L05UpdateThumbPosByRecord = FALSE;
    return 0;
  }

  if (!This->L05UpdateThumbPosByRecord) {
    This->L05ValueModeNumberRecord = (This->ThumbPos * L05_THUMB_SEPARATE_RATIO * This->L05ValueModeNumber) / (This->ThumbRange * L05_THUMB_SEPARATE_RATIO);
  }

  This->L05UpdateThumbPosByRecord = FALSE;

  if (This->L05ValueModeNumberRecord <= 0) {
    This->L05ValueModeNumberRecord = 0;
  }

  if (This->L05ValueModeNumberRecord > This->L05ValueModeNumber) {
    This->L05ValueModeNumberRecord = This->L05ValueModeNumber;
  }

  if (This->L05ValueModeNumberRecord == This->L05ValueModeNumber) {
    ThumbPos = This->ThumbRange;

  } else {
    ThumbPos = This->L05ValueModeNumberRecord * ((This->ThumbRange * L05_THUMB_SEPARATE_RATIO) / This->L05ValueModeNumber) / L05_THUMB_SEPARATE_RATIO;
  }

  return ThumbPos;
}

VOID
L05UpdateSwitch (
  UI_CONTROL                            *Control,
  CONST RECT                            *Rc
  )
{
  UI_SWITCH                             *This;
  UI_CONTROL                            *Child;
  CHAR16                                Str[20];
  INT32                                 Width;
  INT32                                 Height;
  INT32                                 ThumbWidth;
  INT32                                 ThumbHeight;
  INT32                                 FreeWidth;

  This = (UI_SWITCH *) Control;
  Width  = Rc->right   -  Rc->left;
  Height =  Rc->bottom -  Rc->top;
  ThumbWidth  = Height - (Height / 4) * 2;
  ThumbHeight = ThumbWidth;

  Child = UiFindChildByName (Control, L"CheckboxButton");
  UnicodeSPrint (
    Str,
    sizeof (Str), 
    L"%d,%d,%d,%d", 
    Height / 4, 
    Width - ThumbWidth - L05_SWITCH_PADDING_WIDTH - This->ThumbPos, 
    Height / 4, 
    This->ThumbPos + L05_SWITCH_PADDING_WIDTH
    );
  UiSetAttribute (Child, L"padding", Str);

  Child = UiFindChildByName (Control, L"CheckboxBkgRight");
  if (Child != NULL) {
    UiSetAttributeEx (Child, L"width", L"%d", Rc->right - This->ThumbPos - ThumbWidth * 2 - L05_SWITCH_PADDING_WIDTH * 2);
  }

  if (This->ThumbPos < This->ThumbRange / 2) {
    Child = UiFindChildByName (Control, L"CheckboxText");
    if (Child != NULL) {
      UiSetAttribute (Child, L"text", L"Disabled");
    }

    FreeWidth = Width - SWITCH_TEXT_LENGTH - ThumbWidth - SWITCH_BORDER_WIDTH * 2;
    UnicodeSPrint (Str, sizeof (Str), L"%d,4,4,%d", (Height - 20) / 2, ThumbWidth + SWITCH_BORDER_WIDTH + FreeWidth / 2);
    UiSetAttribute (Child, L"padding", Str);

  } else {
    Child = UiFindChildByName (Control, L"CheckboxText");
    if (Child != NULL) {
      UiSetAttribute (Child, L"text", L"Enabled");
    }

    FreeWidth = Width - SWITCH_TEXT_LENGTH - ThumbWidth - SWITCH_BORDER_WIDTH * 2;
    UnicodeSPrint (Str, sizeof (Str), L"%d,4,4,%d", (Height - 20) / 2, SWITCH_BORDER_WIDTH + FreeWidth / 2);
    UiSetAttribute (Child, L"padding", Str);
  }
}
