/** @file
  Provide functions for Gaming Dialog

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_GAMING_DIALOGS_H_
#define _L05_GAMING_DIALOGS_H_

#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
#include "L05OverclockDialog.h"
#endif

extern CHAR16                           *mL05GamingExitPopupDialog;

EFI_STATUS
L05GamingExitDialogInit (
  IN  UI_DIALOG                         *Dialog
  );

#endif
