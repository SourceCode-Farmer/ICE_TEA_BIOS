/** @file
  Provide function of Gaming Popup Dialog

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2ODisplayEngineLocalMetro.h"
#include "MetroUi.h"
#include "MetroDialog.h"
#include <L05Config.h>
#include <L05ChipsetNameList.h>
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
#include <SetupId.h>
#endif
#include <Uefi/UefiInternalFormRepresentation.h>
#include <Guid/L05H2OSetup.h>

#include <SetupConfig.h>    // SYSTEM_CONFIGURATION
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
#include <SetupVariable.h>  // CPU_SETUP, SA_SETUP, PCH_SETUP
#endif
#include <Library/FeatureLib/OemSvcOverrideDefaultSetupSetting.h>
#include <Library/VariableLib.h>
#include <Protocol/L05SetupMenu.h>
#include <Protocol/H2ODialog.h>

#define VOLTAGE_POSITIVE_SIGN           0  // +
#define VOLTAGE_NEGATIVE_SIGN           1  // -

extern H2O_FORM_BROWSER_D               *mFbDialog;
extern HWND                             gLastFocus;
extern UI_CONTROL                       *gLastHoverControl;
extern GAMING_FOCUSABLE_WINDOW_DATA     mGamingFocusableWnd[];
extern L05_OVERCLOCK_CONTROL_ITEM_DATA  mL05OverclockControlItemDataTable[];
extern UINTN                            mL05OverclockControlItemDataTableCount;

HWND                                    mOverclockLayoutWnd = NULL;
HWND                                    mSetToDefaultWnd = NULL;
HWND                                    mApplyWnd = NULL;
HWND                                    mCancelWnd = NULL;
HWND                                    mApplyDialogWnd = NULL;
BOOLEAN                                 mIsDialogInit = FALSE;

L05_OVERCLOCK_FOCUS_TABLE               mL05OverclockFocusTypeTable[] = {
// FocusType,                Wnd
  {ApplyFocusType,           &mApplyWnd},
  {CancelFocusType,          &mCancelWnd},
  {SetToDefaultFocusType,    &mSetToDefaultWnd},
  {OverclockLayoutFocusType, &mOverclockLayoutWnd} // Need at bottom.
};

UINTN                                   mL05OverclockFocusTypeTableCount = sizeof (mL05OverclockFocusTypeTable) / sizeof (L05_OVERCLOCK_FOCUS_TABLE);

L05_OVERCLOCK_SETUP_DATA                mL05OverclockSetupDataTable[] = {
// ItemName,                                   ItemType,         ItemGroup,        QuestionId,                                          StatementId, Wnd,  Value, ValueBase, BaseOffset, Visibility
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  {L"CpuOverclocking",                         ToggleButtonType, CpuControl,       L05_KEY_CPU_OVERCLOCKING,                            0,           NULL, 0,     1,         0,          TRUE },
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE)
  {L"CpuAdvancedOverclocking",                 ToggleButtonType, CpuAdvanced,      L05_KEY_CPU_ADVANCED_OVERCLOCKING,                   0,           NULL, 0,     1,         0,          TRUE },
  {L"1ActiveCoreRatio",                        ButtonBarType,    CpuAdvancedGroup, KEY_RatioLimit1,                                     0,           NULL, 0,     1,         0,          TRUE },
  {L"2ActiveCoreRatio",                        ButtonBarType,    CpuAdvancedGroup, KEY_RatioLimit2,                                     0,           NULL, 0,     1,         0,          TRUE },
  {L"3ActiveCoreRatio",                        ButtonBarType,    CpuAdvancedGroup, KEY_RatioLimit3,                                     0,           NULL, 0,     1,         0,          TRUE },
  {L"4ActiveCoreRatio",                        ButtonBarType,    CpuAdvancedGroup, KEY_RatioLimit4,                                     0,           NULL, 0,     1,         0,          TRUE },
  {L"5ActiveCoreRatio",                        ButtonBarType,    CpuAdvancedGroup, KEY_RatioLimit5,                                     0,           NULL, 0,     1,         0,          TRUE },
  {L"6ActiveCoreRatio",                        ButtonBarType,    CpuAdvancedGroup, KEY_RatioLimit6,                                     0,           NULL, 0,     1,         0,          TRUE },
  {L"7ActiveCoreRatio",                        ButtonBarType,    CpuAdvancedGroup, KEY_RatioLimit7,                                     0,           NULL, 0,     1,         0,          TRUE },
  {L"8ActiveCoreRatio",                        ButtonBarType,    CpuAdvancedGroup, KEY_RatioLimit8,                                     0,           NULL, 0,     1,         0,          TRUE },
  {L"CoreVoltageOffsetPrefix",                 OneOfListType,    CpuAdvancedGroup, L05_KEY_CPU_VOLTAGE_OFFSET_PREFIX,                   0,           NULL, 0,     1,         0,          FALSE},
  {L"CoreVoltageOffset",                       ButtonBarType,    CpuAdvancedGroup, L05_KEY_CPU_VOLTAGE_OFFSET,                          0,           NULL, 0,     1,         500,        TRUE },
  {L"AVXRatioOffset",                          ButtonBarType,    CpuAdvancedGroup, L05_KEY_AVX_RATIO_OFFSET,                            0,           NULL, 0,     1,         0,          TRUE },
  {L"CacheRatio",                              ButtonBarType,    CpuAdvancedGroup, KEY_RING_MAX_OC_RATIO_LIMIT,                         0,           NULL, 0,     1,         0,          TRUE },
  {L"CacheVoltageOffsetPrefix",                OneOfListType,    CpuAdvancedGroup, L05_KEY_CACHE_VOLTAG_OFFSET_PREFIX,                  0,           NULL, 0,     1,         0,          FALSE},
  {L"CacheVoltageOffset",                      ButtonBarType,    CpuAdvancedGroup, L05_KEY_CACHE_VOLTAG_OFFSET,                         0,           NULL, 0,     1,         500,        TRUE },
#endif
  {L"GpuOverclocking",                         ToggleButtonType, GpuControl,       L05_KEY_GPU_OVERCLOCKING,                            0,           NULL, 0,     1,         0,          TRUE },
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE)
  {L"GpuAdvancedOverclocking",                 ToggleButtonType, GpuAdvanced,      L05_KEY_GPU_ADVANCED_OVERCLOCKING,                   0,           NULL, 0,     1,         0,          TRUE },
  {L"CoreClockOffsetPrefix",                   OneOfListType,    GpuAdvancedGroup, L05_KEY_GPU_CORE_CLOCK_OFFSET_PREFIX,                0,           NULL, 0,     1,         0,          FALSE},
  {L"CoreClockOffset",                         ButtonBarType,    GpuAdvancedGroup, L05_KEY_GPU_CORE_CLOCK_OFFSET,                       0,           NULL, 0,     1,         200,        TRUE },
  {L"VRAMClockOffsetPrefix",                   OneOfListType,    GpuAdvancedGroup, L05_KEY_GPU_VRAM_CLOCK_OFFSET_PREFIX,                0,           NULL, 0,     1,         0,          FALSE},
  {L"VRAMClockOffset",                         ButtonBarType,    GpuAdvancedGroup, L05_KEY_GPU_VRAM_CLOCK_OFFSET,                       0,           NULL, 0,     1,         200,        TRUE },
#endif
  {L"MemoryOverclocking",                      ToggleButtonType, MemoryControl,    L05_KEY_MEMORY_OVERCLOCKING,                         0,           NULL, 0,     1,         0,          TRUE },
  {L"MemoryOverclockingLP",                    ToggleButtonType, MemoryControl,    L05_KEY_MEMORY_OVERCLOCKING_LP,                      0,           NULL, 0,     1,         0,          TRUE },
  {L"MemoryProfile",                           OneOfListType,    MemoryGroup,      L05_KEY_MEMORY_PROFILE,                              0,           NULL, 0,     1,         0,          TRUE },
#endif
#if (L05_CHIPSET_VENDOR_ID == L05_AMD_VENDOR_ID)
  {L"CpuOverclocking",                         ToggleButtonType, CpuControl,       L05_KEY_CPU_OVERCLOCKING,                            0,           NULL, 0,     1,         0,          TRUE },
  {L"CpuAdvancedOverclocking",                 ToggleButtonType, CpuAdvanced,      L05_KEY_CPU_ADVANCED_OVERCLOCKING,                   0,           NULL, 0,     1,         0,          TRUE },
  {L"PrecisionBoostOverdriveScalar",           ButtonBarType,    CpuAdvancedGroup, L05_KEY_PRECISION_BOOST_OVERDRIVE_SCALAR,            0,           NULL, 0,     100,       0,          TRUE },
  {L"MaxCPUBoostClockOverride",                ButtonBarType,    CpuAdvancedGroup, L05_KEY_MAX_CPU_BOOST_CLOCK_OVERRIDE,                0,           NULL, 0,     25,        0,          TRUE },
  {L"AllCoreCurveOptimizerMagnitudePrefix",    OneOfListType,    CpuAdvancedGroup, L05_KEY_ALL_CORE_CURVE_OPTIMIZER_MAGNITUDE_PREFIX,   0,           NULL, 0,     1,         0,          FALSE},
  {L"AllCoreCurveOptimizerMagnitude",          ButtonBarType,    CpuAdvancedGroup, L05_KEY_ALL_CORE_CURVE_OPTIMIZER_MAGNITUDE,          0,           NULL, 0,     1,         30,         TRUE },
  {L"GpuOverclocking",                         ToggleButtonType, GpuControl,       L05_KEY_GPU_OVERCLOCKING,                            0,           NULL, 0,     1,         0,          TRUE },
  {L"GpuAdvancedOverclocking",                 ToggleButtonType, GpuAdvanced,      L05_KEY_GPU_ADVANCED_OVERCLOCKING,                   0,           NULL, 0,     1,         0,          TRUE },
  {L"CoreClockOffsetPrefix",                   OneOfListType,    GpuAdvancedGroup, L05_KEY_GPU_CORE_CLOCK_OFFSET_PREFIX,                0,           NULL, 0,     1,         0,          FALSE},
  {L"CoreClockOffset",                         ButtonBarType,    GpuAdvancedGroup, L05_KEY_GPU_CORE_CLOCK_OFFSET,                       0,           NULL, 0,     1,         200,        TRUE },
  {L"VRAMClockOffsetPrefix",                   OneOfListType,    GpuAdvancedGroup, L05_KEY_GPU_VRAM_CLOCK_OFFSET_PREFIX,                0,           NULL, 0,     1,         0,          FALSE},
  {L"VRAMClockOffset",                         ButtonBarType,    GpuAdvancedGroup, L05_KEY_GPU_VRAM_CLOCK_OFFSET,                       0,           NULL, 0,     1,         200,        TRUE },
  {L"MemoryOverclocking",                      ToggleButtonType, MemoryControl,    L05_KEY_MEMORY_OVERCLOCKING,                         0,           NULL, 0,     1,         0,          TRUE },
  {L"MemoryOverclockingLP",                    ToggleButtonType, MemoryControl,    L05_KEY_MEMORY_OVERCLOCKING_LP,                      0,           NULL, 0,     1,         0,          TRUE },
#endif
};

UINTN                                   mL05OverclockSetupDataTableCount = sizeof (mL05OverclockSetupDataTable) / sizeof (L05_OVERCLOCK_SETUP_DATA);

#define L05_OVERCLOCK_DIALOG_DEVICE_TEMPLATE(IMAGE, NAME, TEXT) \
  L"<HorizontalLayout name='OverclockTemplate" L##NAME L"Layout' height='16'>" \
    L"<Texture name='Overclock" L##NAME L"BulletImage' height='12' width='12' background-image='" L##IMAGE L"' background-image-style='stretch' padding='6,0,0,0'/>" \
    L"<HorizontalLayout width='5'/>" \
    L"<HorizontalLayout padding='0,0,0,0'>" \
      L"<Label name='Overclock" L##NAME L"Text' height='16' width='60' textcolor='0xFFFBFDFF' font-size='16' text='" L##NAME L"'/>" \
      L"<HorizontalLayout width='10'/>" \
      L"<Label name='Overclock" L##NAME L"Content' height='16' width='200' textcolor='0xFFFBFDFF' font-size='16' text='" L##TEXT L"'/>" \
      L"<HorizontalLayout/>" \
    L"</HorizontalLayout>" \
  L"</HorizontalLayout>"

CHAR16                                  *mOverclockSettingDialog = L""
  L"<VerticalLayout name='PopUpDialog' height='800' width='800' background-color='0xFF1F2A41'>"

    L"<HorizontalLayout height='50' background-color='0xFF11192C' padding='15,20,15,20'>"
      L"<Label name='DialogTitle' height='16' width='wrap_content' textcolor='0xFFFFFFFF' font-size='16' padding='1,0,1,0'/>"
      L"<HorizontalLayout/>"
      L"<Button name='CloseButton' height='20' width='20' normalimage='@L05CloseIconWhite' focusedimage='@L05CloseIconWhite' hoverimage='@L05CloseIconWhite' pushimage='@L05CloseIconWhite'/>"
    L"</HorizontalLayout>"

    L"<ListView name='OverclockLayout' height='640' width='match_parent' padding='10,10,10,10' vscrollbar='true'>"
    L"</ListView>"

    L"<HorizontalLayout name='BottomLayout' height='110' width='0' background-color='0xFF242B3F' padding='0,20,0,20'>"

      L"<VerticalLayout width='wrap_content'>"
        L"<VerticalLayout/>"
        L"<Texture name='OverclockImage' height='79' width='79' background-image='@L05OverclockBRY000' background-image-style='stretch'/>"
        L"<VerticalLayout/>"
      L"</VerticalLayout>"

      L"<HorizontalLayout height='1' width='10'/>"

      L"<VerticalLayout width='300'>"
        L"<HorizontalLayout height='0'/>"
        L05_OVERCLOCK_DIALOG_DEVICE_TEMPLATE ("@L05OverclockBulletBlue100", "Cpu", "0.0Ghz -> 0.0Ghz")
        L"<HorizontalLayout height='0'/>"
        L05_OVERCLOCK_DIALOG_DEVICE_TEMPLATE ("@L05OverclockBulletRed010", "Gpu", "0.0Ghz -> 0.0Ghz")
        L"<HorizontalLayout height='0'/>"
        L05_OVERCLOCK_DIALOG_DEVICE_TEMPLATE ("@L05OverclockBulletYellow001", "Memory", "0000Mhz")
        L05_OVERCLOCK_DIALOG_DEVICE_TEMPLATE ("@L05OverclockBulletYellow001", "MemoryLP", "0000Mhz")
        L"<HorizontalLayout height='0'/>"
      L"</VerticalLayout>"

      L"<HorizontalLayout/>"

      L"<VerticalLayout width='300'>"
        L"<HorizontalLayout height='wrap_content' width='match_parent'>"
          L"<HorizontalLayout/>"
          L"<ListView name='SetToDefaultLayoutList' height='wrap_content' width='wrap_content'>"
            L"<HorizontalLayout name='SetToDefaultLayout' height='wrap_content' width='wrap_content'>"
              L"<HorizontalLayout height='wrap_content' width='wrap_content' name='ItemLayout' background-color='0xFF242B3F' padding='2,2,2,2'>"
                L"<HorizontalLayout/>"
                L"<Texture height='20' width='20' background-image='@L05ResetIcon'/>"
                L"<Label name='SetToDefault' textcolor='0xFFFFFFFF' width='wrap_content'/>"
              L"</HorizontalLayout>"
            L"</HorizontalLayout>"
          L"</ListView>"
        L"</HorizontalLayout>"

        L"<HorizontalLayout height='10'/>"

        L"<HorizontalLayout height='40'>"
          L"<HorizontalLayout/>"
          L"<Button name='Apply' height='40' width='113' text-align='center' padding='12,0,10,0' textcolor='0xFFFBFDFF' font-size='16' normalimage='@L05ButtonNormal' focusedimage='@L05ButtonHover' hoverimage='@L05ButtonHover' pushimage='@L05ButtonHover' scale9grid='24,20,24,19'/>"
          L"<HorizontalLayout width='10'/>"
          L"<Button name='Cancel' height='40' width='113' text-align='center' padding='12,0,10,0' textcolor='0xFFFBFDFF' font-size='16' normalimage='@L05ButtonNormal' focusedimage='@L05ButtonHover' hoverimage='@L05ButtonHover' pushimage='@L05ButtonHover' scale9grid='24,20,24,19'/>"
        L"</HorizontalLayout>"
      L"</VerticalLayout>"

    L"</HorizontalLayout>"

  L"</VerticalLayout>";

CHAR16                                  *mL05OverclockToggleButton = L""
  L"<HorizontalLayout name='OverclockToggleButton'  height='84' background-color='0xFF404556' padding='2,2,2,2'>"
    L"<HorizontalLayout name='ItemLayout' height='80' width='0' background-color='0xFF242B3F' padding='0,20,0,20'>"
      L"<Label name='ToggleButtonPrompt' textcolor='0xFFFFFFFF' font-size='28'/>"
      L"<HorizontalLayout/>"
      L"<Button name='ToggleButton' width='50' normalimage='@L05ToggleOn' focusedimage='@L05ToggleOn' hoverimage='@L05ToggleOn' pushimage='@L05ToggleOn'/>"
    L"</HorizontalLayout>"
  L"</HorizontalLayout>";

CHAR16                                  *mL05OverclockOneOfList = L""
  L"<HorizontalLayout name='OverclockOneOfList' height='wrap_content' background-color='0xFF404556' padding='2,2,2,2'>"
    L"<HorizontalLayout name='ItemLayout' height='wrap_content' background-color='0xFF242B3F' padding='0,20,0,20'>"
      L"<Label name='ListPrompt' textcolor='0xFFFFFFFF' font-size='28'/>"
      L"<HorizontalLayout/>"
      L"<VerticalLayout name='ListLayout' height='wrap_content' width='wrap_content' padding='0,0,0,0'>"
        L"<ListView name='OneOfList' height='wrap_content' width='wrap_content' child-padding='10'/>"
      L"</VerticalLayout>"
    L"</HorizontalLayout>"
  L"</HorizontalLayout>";

CHAR16                                  *mL05OverclockButtonBar = L""
  L"<HorizontalLayout name='OverclockButtonBar' height='164' background-color='0xFF404556' padding='2,2,2,2'>"
    L"<VerticalLayout name='ItemLayout' height='160' width='0' background-color='0xFF242B3F' padding='0,20,0,20'>"
      L"<HorizontalLayout height='40'>"
        L"<Label name='ButtonBarPrompt' width='wrap_content' height='40' textcolor='0xFFFFFFFF' font-size='20' padding='10,10,10,10'/>"
        L"<HorizontalLayout/>"
        L"<Label name='ButtonBarValue' width='140' height='40' textcolor='0xFF257EFF' font-size='20' padding='10,10,10,10'/>"
      L"</HorizontalLayout>"
      L"<HorizontalLayout name='ButtonBar' height='40' width='match_parent' padding='10,10,10,10'>"
        L"<Button name='MinusButton' width='20' height='20' normalimage='@L05MinusIcon' focusedimage='@L05MinusIcon' hoverimage='@L05MinusIcon' pushimage='@L05MinusIcon'/>"
        L"<Switch name='ButtonScalar' width='0' height='40'/>"
        L"<Button name='PlusButton' width='20' height='20' normalimage='@L05PlusIcon' focusedimage='@L05PlusIcon' hoverimage='@L05PlusIcon' pushimage='@L05PlusIcon'/>"
      L"</HorizontalLayout>"
      L"<Label name='ButtonBarHelp' width='0' height='wrap_content' textcolor='0xFFFFFFFF' font-size='16' padding='10,10,10,10'/>"
    L"</VerticalLayout>"
  L"</HorizontalLayout>";

CHAR16                                  *mL05OverclockApplyDialog = L""
  L"<VerticalLayout name='PopUpDialog' height='393' width='500' background-color='0xFFFFFFFF'>"
    L"<HorizontalLayout height='40' padding='10,20,10,20'>"
      L"<HorizontalLayout/>"
      L"<Button name='CloseButton' height='20' width='20' normalimage='@L05CloseIconBlue' focusedimage='@L05CloseIconBlue' hoverimage='@L05CloseIconBlue' pushimage='@L05CloseIconBlue'/>"
    L"</HorizontalLayout>"
    L"<VerticalLayout height='250' padding='0,40,0,40'>"
      L"<Label name='PopWindowsTitle' textcolor='0xFF194A65' font-size='25' height='wrap_content' width='match_parent' padding='0,0,0,0'/>"
      L"<Label name='WaringText' textcolor='0xFF194A65' font-size='16' height='wrap_content' width='match_parent' padding='20,0,20,0' show_html_text='true'/>"
    L"</VerticalLayout>"
    L"<VerticalLayout height='10' width='match_parent' padding='4,40,0,40'>"
      L"<HorizontalLayout height='2' width='match_parent' background-color='0xFFE4E7EB' padding='0,0,0,0'/>"
    L"</VerticalLayout>"
    L"<HorizontalLayout height='61' width='match_parent' padding='21,40,0,40'>"
      L"<Button name='Apply' height='40' width='113' text-align='center' padding='12,0,10,0' textcolor='0xFFFBFDFF' font-size='16' normalimage='@L05ButtonNormal' focusedimage='@L05ButtonHover' hoverimage='@L05ButtonHover' pushimage='@L05ButtonHover' scale9grid='24,20,24,19'/>"
      L"<HorizontalLayout width='20'/>"
      L"<Button name='Cancel' height='40' width='113' text-align='center' padding='12,0,10,0' textcolor='0xFFFBFDFF' font-size='16' normalimage='@L05ButtonNormal' focusedimage='@L05ButtonHover' hoverimage='@L05ButtonHover' pushimage='@L05ButtonHover' scale9grid='24,20,24,19'/>"
    L"</HorizontalLayout>"
  L"</VerticalLayout>";

CHAR16                                  *mOverclockDeviceItemChilds = L""
  L"<HorizontalLayout name='border' height='wrap_content' width='wrap_content' background-color='0xFF404556' padding='2,2,2,2'>"
    L"<HorizontalLayout name='ItemLayout' height='40' width='200' background-color='0xFF242B3F'>"
      L"<Texture name='RaidoButton' height='40' width='40' background-image='@L05RadioOff' background-image-style='stretch' float='true'/>"
      L"<Label name='OptionName' text-align='singleline' textcolor='0xFFFBFDFF' font-size='16' padding='12,0,8,40'/>"
    L"</HorizontalLayout>"
  L"</HorizontalLayout>";

VOID
GamingOwnerDrawPanelItemSetState (
  IN  UI_CONTROL                        *Control,
  IN  UI_STATE                          SetState,
  IN  UI_STATE                          ClearState
  );

VOID
UpdateMemoryLayout (
  IN UI_CONTROL                         *Control
  );

HWND
CreateOverclockDialog (
  IN  HINSTANCE                         Instance,
  IN  CHAR16                            *XmlBuffer,
  IN  HWND                              ParentWnd,
  IN  WNDPROC                           DialogProc,
  IN  LPARAM                            Param,
  IN  INT32                             X,
  IN  INT32                             Y,
  IN  INT32                             Width,
  IN  INT32                             Height,
  IN  BOOLEAN                           CloseDlgWhenTouchOutside
  )
{
  UI_DIALOG                             *DialogData;
  HWND                                  Dlg;
  UI_CONTROL                            *Control;
  UI_MANAGER                            *Manager;

  DialogData = NULL;
  Dlg        = NULL;
  Control    = NULL;
  Manager    = NULL;

  DialogData = AllocateZeroPool (sizeof (UI_DIALOG));
  if (DialogData == NULL) {
    return NULL;
  }

  DialogData->Instance                 = Instance;
  DialogData->ParentWnd                = ParentWnd;
  DialogData->Proc                     = DialogProc;
  DialogData->Param                    = Param;
  DialogData->XmlBuffer                = XmlBuffer;
  DialogData->Running                  = TRUE;
  DialogData->CloseDlgWhenTouchOutside = CloseDlgWhenTouchOutside;

  Dlg = CreateWindowEx (
          WS_EX_NOACTIVATE, // dwExStyle
          L"DIALOG", // lpClassName
          L"", // lpWindowName
          WS_VISIBLE | WS_POPUP, // dwStyle
          X, // x
          Y, // y
          Width, // nWidth
          Height, // nHeight
          ParentWnd, // hwndParent
          NULL, // hMenu
          Instance, // hInstance
          DialogData // lpParam
          );

  if (Dlg == NULL) {
    return NULL;
  }

  Control = (UI_CONTROL *) (UINTN) GetUiControl (Dlg);
  Manager = Control->Manager;

  SendMessage (Manager->MainWnd, UI_NOTIFY_WINDOWINIT, (WPARAM) Manager->Root, 0);

  return Dlg;
}

HWND
L05PopupOverclockDialog (
  )
{
  RECT                                  Rect;
  HWND                                  CurrentFocus;
  HWND                                  Wnd;

  GetWindowRect (gWnd, &Rect);

  EnableWindow (gWnd, FALSE);
  EnableWindow (GetDesktopWindow (), FALSE);

  CurrentFocus = GetFocus ();

  Wnd = CreateOverclockDialog (
          NULL,  // Instance
          mOverclockSettingDialog, // XmlBuffer,
          CurrentFocus, // ParentWnd,
          OverclockSettingDialogProc, // DialogProc,
          0, // 0,
          Rect.left, // X,
          Rect.bottom, // Y,
          Rect.right - Rect.left, // Width,
          Rect.bottom - Rect.top, // Height,
          TRUE // IsCloseOnTouchOutside
          );

  if (Wnd == NULL) {
    return 0;
  }

  return Wnd;
}

UI_CONTROL *
GetParentControlByControlName (
  IN  UI_CONTROL                        *InputControl,
  IN  CHAR16                            *ControlName
  )
{
  HWND                                  ParentWnd;
  HWND                                  Wnd;
  UI_CONTROL                            *ParentControl;

  ParentWnd     = NULL;
  Wnd           = NULL;
  ParentControl = NULL;

  if (InputControl == NULL || InputControl->Wnd == NULL || ControlName == NULL) {
    return NULL;
  }

  Wnd = InputControl->Wnd;
  do {
    ParentWnd = GetParent (Wnd);
    if (ParentWnd == NULL) {
      break;
    }
    ParentControl = GetUiControl (ParentWnd);
    if (ParentControl == NULL) {
      break;
    }
    if (ParentControl->Name != NULL &&
        StrCmp (ParentControl->Name, ControlName) == 0) {
      return ParentControl;
    }
    Wnd = ParentWnd;
  } while (ParentWnd != NULL);

  return NULL;
}

HWND
GetParentWndByClassName (
  IN  HWND                              Wnd,
  IN  CHAR16                            *ClassName
  )
{
  HWND                                  ParentWnd;
  UI_CONTROL                            *ParentControl;

  if (Wnd == NULL || ClassName == NULL) {
    return NULL;
  }

  do {
    ParentWnd = GetParent (Wnd);
    if (ParentWnd == NULL) {
      break;
    }

    ParentControl = GetUiControl (ParentWnd);
    if (ParentControl == NULL) {
      break;
    }

    if (ParentControl->Class != NULL &&
        ParentControl->Class->ClassName != NULL &&
        StrCmp (ParentControl->Class->ClassName, ClassName) == 0) {
      return ParentWnd;
    }

    Wnd = ParentWnd;
  } while (ParentWnd != NULL);

  return NULL;
}

EFI_STATUS
OverclockGetSetupItemIndexByQuestionId (
  EFI_QUESTION_ID                       QuestionId,
  UINTN                                 *ItemIndex
  )
{
  UINTN                                 Index;

  if (ItemIndex == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  *ItemIndex = mL05OverclockSetupDataTableCount;

  for (Index = 0; Index < mL05OverclockSetupDataTableCount; Index++) {
    if (QuestionId == mL05OverclockSetupDataTable[Index].QuestionId) {
      break;
    }
  }

  if (Index == mL05OverclockSetupDataTableCount) {
    return EFI_NOT_FOUND;
  }

  *ItemIndex = Index;

  return EFI_SUCCESS;
}

EFI_STATUS
OverclockGetSetupItemIndexByWnd (
  HWND                                  Wnd,
  UINTN                                 *ItemIndex
  )
{
  UINTN                                 Index;

  if (ItemIndex == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  *ItemIndex = mL05OverclockSetupDataTableCount;

  for (Index = 0; Index < mL05OverclockSetupDataTableCount; Index++) {
    if (Wnd == mL05OverclockSetupDataTable[Index].Wnd) {
      break;
    }

  }

  if (Index == mL05OverclockSetupDataTableCount) {
    return EFI_NOT_FOUND;
  }

  *ItemIndex = Index;

  return EFI_SUCCESS;
}

HWND
OverclockGetSetupItemWndByWnd (
  IN  HWND                              Wnd
  )
{
  EFI_STATUS                            Status;
  HWND                                  ParentWnd;
  UI_CONTROL                            *ParentControl;
  UINTN                                 ItemIndex;

  if (Wnd == NULL) {
    return NULL;
  }

  do {
    Status = OverclockGetSetupItemIndexByWnd (Wnd, &ItemIndex);

    if (!EFI_ERROR (Status) && ItemIndex < mL05OverclockSetupDataTableCount) {
      return mL05OverclockSetupDataTable[ItemIndex].Wnd;
    }

    ParentWnd = GetParent (Wnd);
    if (ParentWnd == NULL) {
      break;
    }

    ParentControl = GetUiControl (ParentWnd);
    if (ParentControl == NULL) {
      break;
    }

    Wnd = ParentWnd;
  } while (ParentWnd != NULL);

  return NULL;
}

EFI_STATUS
OverclockGetCurrentFocusedIndex (
  OUT UINTN                             *FocusedTypeIndex,
  OUT UINTN                             *FocusedItemIndex
  )
{
  EFI_STATUS                            Status;
  HWND                                  FocusedWnd;
  UINTN                                 Index;

  FocusedWnd = gLastFocus;

  for (Index = 0; Index < mL05OverclockFocusTypeTableCount; Index++) {
    if (FocusedWnd == *mL05OverclockFocusTypeTable[Index].Wnd) {
      break;
    }
  }

  *FocusedTypeIndex = Index;

  if (*FocusedTypeIndex >= OverclockLayoutFocusType) {
    *FocusedTypeIndex = OverclockLayoutFocusType;
    FocusedWnd = OverclockGetSetupItemWndByWnd (FocusedWnd);

    if (FocusedWnd != NULL) {
      Status = OverclockGetSetupItemIndexByWnd (FocusedWnd, FocusedItemIndex);

      if (EFI_ERROR (Status)) {
        FocusedWnd = NULL;
      }
    }

    if (FocusedWnd == NULL) {
      *FocusedTypeIndex = OVERCLOCK_INVALID_VALUE;
      *FocusedItemIndex = OVERCLOCK_INVALID_VALUE;
    }
  }

  return EFI_SUCCESS;
}

BOOLEAN
OverclockIsItemVisibility (
  EFI_QUESTION_ID                       QuestionId
  )
{
  BOOLEAN                               IsItemVisibility;
  UINTN                                 Index;

  IsItemVisibility = TRUE;

  for (Index = 0; Index < mL05OverclockSetupDataTableCount; Index++) {

    if (QuestionId == mL05OverclockSetupDataTable[Index].QuestionId) {
      IsItemVisibility = mL05OverclockSetupDataTable[Index].Visibility;
      break;
    }

  }

  return IsItemVisibility;
}

VOID
SetSetupDataToFormBrowser (
  H2O_FORM_BROWSER_S                    *Statement,
  UINTN                                 Value
  )
{
  EFI_HII_VALUE                         HiiValue;

  CopyMem (&HiiValue, &Statement->HiiValue, sizeof (EFI_HII_VALUE));

  switch (Statement->HiiValue.Type) {

  case EFI_IFR_TYPE_NUM_SIZE_8:
    HiiValue.Value.u8 = (UINT8) Value;
    break;

  case EFI_IFR_TYPE_NUM_SIZE_16:
    HiiValue.Value.u16 = (UINT16) Value;
    break;

  case EFI_IFR_TYPE_NUM_SIZE_32:
    HiiValue.Value.u32 = (UINT32) Value;
    break;

  case EFI_IFR_TYPE_NUM_SIZE_64:
    HiiValue.Value.u64 = (UINT64) Value;
    break;

  default:
    HiiValue.Value.u8 = (UINT8) Value;
  }

#if (L05_CHIPSET_VENDOR_ID == L05_AMD_VENDOR_ID)
  //
  // Special Init
  //
  if (Statement->QuestionId == L05_KEY_CPU_OVERCLOCKING) { // 2:ADVANCE, 0:AUTO
    HiiValue.Value.u8 = (Value == 1) ? 2 : 0xFF;
  }

  if ((Statement->QuestionId == L05_KEY_MEMORY_OVERCLOCKING) || (Statement->QuestionId == L05_KEY_MEMORY_OVERCLOCKING_LP)) { // 1:Enable, 0xFF:Auto
    HiiValue.Value.u8 = (Value == 1) ? 1 : 0xFF;
  }
#endif

  SendSelectQNotify (Statement->PageId, Statement->QuestionId, Statement->IfrOpCode);
  SendChangeQNotify (Statement->PageId, Statement->QuestionId, &HiiValue);

  return;
}

EFI_STATUS
OverclockUpdateSetupData (
  L05_OVERCLOCK_ITEM_TYPE               ItemType,
  UI_CONTROL                            *Control,
  H2O_FORM_BROWSER_S                    *Statement
  )
{
  UINTN                                 Index;

  if (Control == NULL || Statement == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  for (Index = 0; Index < mL05OverclockSetupDataTableCount; Index++) {
    if (Statement->QuestionId == mL05OverclockSetupDataTable[Index].QuestionId &&
        mL05OverclockSetupDataTable[Index].ItemType == ItemType) {
      mL05OverclockSetupDataTable[Index].StatementId = Statement->StatementId;
      mL05OverclockSetupDataTable[Index].Wnd = Control->Wnd;

      switch (Statement->HiiValue.Type) {

      case EFI_IFR_TYPE_NUM_SIZE_8:
        mL05OverclockSetupDataTable[Index].Value = (UINTN) Statement->HiiValue.Value.u8;
        break;

      case EFI_IFR_TYPE_NUM_SIZE_16:
        mL05OverclockSetupDataTable[Index].Value = (UINTN) Statement->HiiValue.Value.u16;
        break;

      case EFI_IFR_TYPE_NUM_SIZE_32:
        mL05OverclockSetupDataTable[Index].Value = (UINTN) Statement->HiiValue.Value.u32;
        break;

      case EFI_IFR_TYPE_NUM_SIZE_64:
        mL05OverclockSetupDataTable[Index].Value = (UINTN) Statement->HiiValue.Value.u64;
        break;

      default:
        mL05OverclockSetupDataTable[Index].Value = (UINTN) Statement->HiiValue.Value.u8;
      }

      break;
    }
  }

#if (L05_CHIPSET_VENDOR_ID == L05_AMD_VENDOR_ID)
  //
  // Special Init IsOn
  //
  if (Statement->QuestionId == L05_KEY_CPU_OVERCLOCKING) { // 2:ADVANCE, 0:AUTO
    mL05OverclockSetupDataTable[Index].Value = (Statement->HiiValue.Value.u8 == 2) ? TRUE : FALSE;
  }

  if ((Statement->QuestionId == L05_KEY_MEMORY_OVERCLOCKING) || (Statement->QuestionId == L05_KEY_MEMORY_OVERCLOCKING_LP)) { // 1:Enable, 0xFF:Auto
    mL05OverclockSetupDataTable[Index].Value = (Statement->HiiValue.Value.u8 == 1) ? TRUE : FALSE;
  }
#endif

  return EFI_SUCCESS;
}

EFI_STATUS
OverclockSetSetupData (
  H2O_FORM_BROWSER_S                    *Statement,
  L05_OVERCLOCK_ITEM_TYPE               ItemType,
  UINTN                                 Value,
  BOOLEAN                               SetSetupData
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  BOOLEAN                               ValuePrefix;
  H2O_FORM_BROWSER_S                    *PrefixStatement;

  PrefixStatement = NULL;

  if (Statement == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  for (Index = 0; Index < mL05OverclockSetupDataTableCount; Index++) {
    if (Statement->QuestionId == mL05OverclockSetupDataTable[Index].QuestionId &&
        ItemType == mL05OverclockSetupDataTable[Index].ItemType) {
      mL05OverclockSetupDataTable[Index].Value = Value;

      //
      // Init Prefix Value
      //
      if (mL05OverclockSetupDataTable[Index].BaseOffset != 0) {
        if (Value >= mL05OverclockSetupDataTable[Index].BaseOffset) {
          mL05OverclockSetupDataTable[Index].Value = Value - mL05OverclockSetupDataTable[Index].BaseOffset;
          ValuePrefix = VOLTAGE_POSITIVE_SIGN; // 0:VOLTAGE_POSITIVE_SIGN(+), 1:VOLTAGE_NEGATIVE_SIGN(-)
        } else {
          mL05OverclockSetupDataTable[Index].Value = mL05OverclockSetupDataTable[Index].BaseOffset - Value;
          ValuePrefix = VOLTAGE_NEGATIVE_SIGN; // 0:VOLTAGE_POSITIVE_SIGN(+), 1:VOLTAGE_NEGATIVE_SIGN(-)
        }

        if (mL05OverclockSetupDataTable[Index - 1].Value != ValuePrefix) {
          mL05OverclockSetupDataTable[Index - 1].Value = ValuePrefix;
        }
      }
      break;
    }

    if (Index == mL05OverclockSetupDataTableCount) {
      return EFI_NOT_FOUND;
    }
  }


  if (!mIsDialogInit && SetSetupData) {
    //
    // Set Prefix Value
    //
    if (mL05OverclockSetupDataTable[Index].BaseOffset != 0) {
      Status = gFB->GetSInfo (gFB, gFB->CurrentP->PageId, mL05OverclockSetupDataTable[Index - 1].StatementId, &PrefixStatement);

      if (!EFI_ERROR (Status)) {
        SetSetupDataToFormBrowser (PrefixStatement, (UINTN) mL05OverclockSetupDataTable[Index - 1].Value);
        FreePool (PrefixStatement);
      }
    }

    SetSetupDataToFormBrowser (Statement, (UINTN) mL05OverclockSetupDataTable[Index].Value);
  }

  return EFI_SUCCESS;
}

VOID
OverclockQuestionInteractive (
  EFI_QUESTION_ID                       QuestionId
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_P                    *CurrentP;
  H2O_FORM_BROWSER_S                    *Statement;
  UINTN                                 Index;

  CurrentP  = gFB->CurrentP;
  Statement = NULL;

  //
  // Get Statement of QuestionId
  //
  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // import formset[FORMSET_ID_GUID_CONFIGURATION].form[0x01].question[QuestionId];
    //
    if (Statement->QuestionId != QuestionId) {
      FreePool (Statement);
      continue;
    }

    SetSetupDataToFormBrowser (Statement, 0);
    FreePool (Statement);
    break;
  }

  return;
}

VOID
OverclockBackupSetupData (
  )
{
  OverclockQuestionInteractive (L05_KEY_OVERCLOCK_BACKUP_SETUP_DATA);

  return;
}

VOID
OverclockRestoreSetupData (
  )
{
  OverclockQuestionInteractive (L05_KEY_OVERCLOCK_RESTORE_SETUP_DATA);

  return;
}

VOID
OverclockSetItemCurSel (
  IN  HWND                              ItemWnd,
  IN  INT32                             FocusedCurSel
  )
{

  EFI_STATUS                            Status;
  UI_CONTROL                            *ItemControl;
  UINTN                                 ItemIndex;
  UI_CONTROL                            *Child;
  INT32                                 Selection;

  ItemControl = NULL;
  Child       = NULL;

  if (ItemWnd == NULL || FocusedCurSel == OVERCLOCK_INVALID_VALUE) {
    return;
  }

  
  Selection = FocusedCurSel;

  Status = OverclockGetSetupItemIndexByWnd (ItemWnd, &ItemIndex);
  if (EFI_ERROR (Status)) {
    return;
  }

  ItemControl = (UI_CONTROL *) GetUiControl (ItemWnd);

  switch (mL05OverclockSetupDataTable[ItemIndex].ItemType) {

  case ToggleButtonType:
    break;

  case OneOfListType:
    Child = UiFindChildByName (ItemControl, L"OneOfList");
    OverclockSetFocus (Child->Wnd, &Selection);
    break;

  case ButtonBarType:
    break;
  }

  return;
}

VOID
OverclockSetItemFocus (
  IN  HWND                              SetFocusWnd
  )
{
  HWND                                  FocusWnd;

  if (SetFocusWnd == NULL) {
    return;
  }

  FocusWnd = SetFocusWnd;

  OverclockUpdateFocusedWnd (&FocusWnd);
  OverclockSetFocus (FocusWnd, NULL);
  OverclockSetItemActionFocus (FocusWnd, VK_NULL);

  return;
}

EFI_STATUS
OverclockInitDeviceTemplate (
  IN UI_DIALOG                          *Dialog
  )
{
  EFI_STATUS                            Status;
  UINTN                                 TableIndex;
  UI_CONTROL                            *Child;
  UI_CONTROL                            *LayoutChild;
  UINTN                                 ItemIndex;
  BOOLEAN                               IsTurboSpeed;
  CHAR16                                Str[40];

  Child       = NULL;
  LayoutChild = NULL;

  for (TableIndex = 0; TableIndex < mL05OverclockControlItemDataTableCount; TableIndex++) {
    UnicodeSPrint (Str, sizeof (Str), L"Overclock%sText", mL05OverclockControlItemDataTable[TableIndex].ItemName);
    Child = UiFindChildByName (Dialog, Str);
    if (Child == NULL) {
      continue;
    }

    LayoutChild = GetUiControl (GetParent (GetParent (Child->Wnd)));
    if (LayoutChild == NULL) {
      continue;
    }

    UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, mL05OverclockControlItemDataTable[TableIndex].StringId, NULL));

    UnicodeSPrint (Str, sizeof (Str), L"Overclock%sContent", mL05OverclockControlItemDataTable[TableIndex].ItemName);
    Child = UiFindChildByName (Dialog, Str);
    if (Child == NULL) {
      continue;
    }

    if (mL05OverclockControlItemDataTable[TableIndex].SwitchVisibility == FALSE) {
      UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_OC_UNAVAILABLE), NULL));

      if (mL05OverclockControlItemDataTable[TableIndex].QuestionId == L05_KEY_MEMORY_OVERCLOCKING) {
        UiSetAttribute (LayoutChild, L"visibility", L"false");
      }

      continue;
    }

    IsTurboSpeed = FALSE;
    Status = OverclockGetSetupItemIndexByQuestionId (mL05OverclockControlItemDataTable[TableIndex].QuestionId, &ItemIndex);
    if (!EFI_ERROR (Status)) {
      IsTurboSpeed = (BOOLEAN) mL05OverclockSetupDataTable[ItemIndex].Value;
    }

    if (mL05OverclockControlItemDataTable[TableIndex].QuestionId == L05_KEY_GPU_OVERCLOCKING) {
      mL05OverclockControlItemDataTable[TableIndex].CurrentSpeed = PcdGet16 (PcdL05GamingOverClockGpuCurrentSpeed);
      mL05OverclockControlItemDataTable[TableIndex].TurboSpeed = PcdGet16 (PcdL05GamingOverClockGpuTurboSpeed);
    }

    if (mL05OverclockControlItemDataTable[TableIndex].ItemUnitBase > 1) {
      if (mL05OverclockControlItemDataTable[TableIndex].CurrentSpeed != mL05OverclockControlItemDataTable[TableIndex].TurboSpeed && IsTurboSpeed) {
        UnicodeSPrint (
          Str,
          sizeof (Str),
          L"%d.%d%s -> %d.%d%s",
          (mL05OverclockControlItemDataTable[TableIndex].CurrentSpeed / mL05OverclockControlItemDataTable[TableIndex].ItemUnitBase),
          ((mL05OverclockControlItemDataTable[TableIndex].CurrentSpeed % mL05OverclockControlItemDataTable[TableIndex].ItemUnitBase) / (mL05OverclockControlItemDataTable[TableIndex].ItemUnitBase / 10)),
          mL05OverclockControlItemDataTable[TableIndex].ItemUnit,
          (mL05OverclockControlItemDataTable[TableIndex].TurboSpeed / mL05OverclockControlItemDataTable[TableIndex].ItemUnitBase),
          ((mL05OverclockControlItemDataTable[TableIndex].TurboSpeed % mL05OverclockControlItemDataTable[TableIndex].ItemUnitBase) / (mL05OverclockControlItemDataTable[TableIndex].ItemUnitBase / 10)),
          mL05OverclockControlItemDataTable[TableIndex].ItemUnit
          );
        UiSetAttribute (Child, L"text", Str);
      } else {
        UnicodeSPrint (
          Str,
          sizeof (Str),
          L"%d.%d%s",
          (mL05OverclockControlItemDataTable[TableIndex].CurrentSpeed / mL05OverclockControlItemDataTable[TableIndex].ItemUnitBase),
          ((mL05OverclockControlItemDataTable[TableIndex].CurrentSpeed % mL05OverclockControlItemDataTable[TableIndex].ItemUnitBase) / (mL05OverclockControlItemDataTable[TableIndex].ItemUnitBase / 10)),
          mL05OverclockControlItemDataTable[TableIndex].ItemUnit
          );
        UiSetAttribute (Child, L"text", Str);
      }

    } else {
      if (mL05OverclockControlItemDataTable[TableIndex].CurrentSpeed != mL05OverclockControlItemDataTable[TableIndex].TurboSpeed && IsTurboSpeed) {
        UnicodeSPrint (
          Str,
          sizeof (Str),
          L"%d%s -> %d%s",
          mL05OverclockControlItemDataTable[TableIndex].CurrentSpeed,
          mL05OverclockControlItemDataTable[TableIndex].ItemUnit,
          mL05OverclockControlItemDataTable[TableIndex].TurboSpeed,
          mL05OverclockControlItemDataTable[TableIndex].ItemUnit
          );
        UiSetAttribute (Child, L"text", Str);
      } else {
        UnicodeSPrint (
          Str,
          sizeof (Str)
          , L"%d%s",
          mL05OverclockControlItemDataTable[TableIndex].CurrentSpeed,
          mL05OverclockControlItemDataTable[TableIndex].ItemUnit
          );
        UiSetAttribute (Child, L"text", Str);
      }
    }
  }

  UpdateMemoryLayout ((UI_CONTROL *) Dialog);

  return EFI_SUCCESS;
}

EFI_STATUS
OverclockInitImage (
  IN UI_DIALOG                          *Dialog
  )
{
  EFI_STATUS                            Status;
  UI_CONTROL                            *Child;
  UINTN                                 ItemIndex;
  BOOLEAN                               CpuOverclockingSwitch;
  BOOLEAN                               GpuOverclockingSwitch;
  BOOLEAN                               MemoryOverclockingSwitch;
  CHAR16                                Str[40];

  CpuOverclockingSwitch    = FALSE;
  GpuOverclockingSwitch    = FALSE;
  MemoryOverclockingSwitch = FALSE;

  Child = UiFindChildByName (Dialog, L"OverclockImage");
  if (Child != NULL) {
    Status = OverclockGetSetupItemIndexByQuestionId (L05_KEY_CPU_OVERCLOCKING, &ItemIndex);
    if (!EFI_ERROR (Status)) {
      CpuOverclockingSwitch = (BOOLEAN) mL05OverclockSetupDataTable[ItemIndex].Value;
    }

    Status = OverclockGetSetupItemIndexByQuestionId (L05_KEY_GPU_OVERCLOCKING, &ItemIndex);
    if (!EFI_ERROR (Status)) {
      GpuOverclockingSwitch = (BOOLEAN) mL05OverclockSetupDataTable[ItemIndex].Value;
    }

    Status = OverclockGetSetupItemIndexByQuestionId (L05_KEY_MEMORY_OVERCLOCKING, &ItemIndex);
    if (!EFI_ERROR (Status)) {
      MemoryOverclockingSwitch = (BOOLEAN) mL05OverclockSetupDataTable[ItemIndex].Value;
    }

    Status = OverclockGetSetupItemIndexByQuestionId (L05_KEY_MEMORY_OVERCLOCKING_LP, &ItemIndex);
    if (!EFI_ERROR (Status)) {
      MemoryOverclockingSwitch = (BOOLEAN) mL05OverclockSetupDataTable[ItemIndex].Value;
    }

    UnicodeSPrint (Str, sizeof (Str), L"@L05OverclockBRY%x%x%x", CpuOverclockingSwitch, GpuOverclockingSwitch, MemoryOverclockingSwitch);
    UiSetAttribute (Child, L"background-image", Str);
  }

  return EFI_SUCCESS;
}

EFI_STATUS
OverclockDialogSetToggleButton (
  IN UI_CONTROL                         *Control,
  IN BOOLEAN                            IsOn
  )
{
  HWND                                  ItemWnd;
  UI_CONTROL                            *ItemControl;
  H2O_FORM_BROWSER_S                    *Statement;
  H2O_STATEMENT_ID                      StatementId;
  EFI_STATUS                            Status;
  CHAR16                                Str[20];
  UINTN                                 ItemIndex;

  ItemControl = NULL;
  Statement   = NULL;
  StatementId = 0;

  if (Control == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ItemWnd = GetParent (Control->Wnd);
  ItemWnd = GetParent (ItemWnd);
  ItemControl = GetUiControl (ItemWnd);

  if (ItemControl == NULL) {
    return EFI_NOT_FOUND;
  }


  StatementId = (H2O_STATEMENT_ID) GetWindowLongPtr (ItemControl->Wnd, GWLP_USERDATA);
  Status = gFB->GetSInfo (gFB, gFB->CurrentP->PageId, StatementId, &Statement);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  SetWindowLongPtr (Control->Wnd, GWLP_USERDATA, IsOn);
  UnicodeSPrint (Str, sizeof (Str), L"%s", IsOn ? L"@L05ToggleOn" : L"@L05ToggleOff");
  UiSetAttribute (Control, L"normalimage", Str);
  UiSetAttribute (Control, L"focusedimage", Str);
  UiSetAttribute (Control, L"hoverimage", Str);
  UiSetAttribute (Control, L"pushimage", Str);

  Status = OverclockGetSetupItemIndexByQuestionId (Statement->QuestionId, &ItemIndex);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  OverclockSetSetupData (Statement, ToggleButtonType, (UINTN) IsOn, TRUE);
  OverclockSetItemFocus (ItemWnd);

  FreePool (Statement);

  return EFI_SUCCESS;
}

VOID
OverclockInitToggleButtonItemSetState (
  IN  UI_CONTROL                        *Control,
  IN  UI_STATE                          SetState,
  IN  UI_STATE                          ClearState
  )
{

  HWND                                  ItemWnd;
  UI_CONTROL                            *ItemControl;

  ItemControl = NULL;

  if (Control == NULL) {
    return;
  }

  ItemWnd = GetParent (Control->Wnd);
  ItemWnd = GetParent (ItemWnd);
  ItemControl = GetUiControl (ItemWnd);

  if (ItemControl == NULL) {
    return;
  }

  GamingOwnerDrawPanelItemSetState (ItemControl, SetState, ClearState);

  return;
}

EFI_STATUS
OverclockInitToggleButtonItemValue (
  IN UI_DIALOG                          *Dialog,
  IN CHAR16                             *ItemName,
  H2O_FORM_BROWSER_S                    *Statement
  )
{
  EFI_STATUS                            Status;
  UI_CONTROL                            *ItemControl;
  UI_CONTROL                            *Child;
  UINTN                                 ItemIndex;

  ItemControl = NULL;

  if (Dialog == NULL || ItemName == NULL || Statement == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Init Toggle Button
  //
  ItemControl = UiFindChildByName (Dialog, ItemName);
  if (ItemControl == NULL) {
    return EFI_NOT_FOUND;
  }

  Child = UiFindChildByName (ItemControl, L"ToggleButton");
  if (Child == NULL) {
    return EFI_NOT_FOUND;
  }

  Status = OverclockGetSetupItemIndexByQuestionId (Statement->QuestionId, &ItemIndex);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = OverclockDialogSetToggleButton (Child, (BOOLEAN) mL05OverclockSetupDataTable[ItemIndex].Value);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  return EFI_SUCCESS;
}

EFI_STATUS
OverclockInitToggleButtonItem (
  IN UI_DIALOG                          *Dialog,
  IN CHAR16                             *ItemName,
  IN EFI_QUESTION_ID                    QuestionId
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_P                    *CurrentP;
  UI_CONTROL                            *ItemControl;
  UI_CONTROL                            *Child;
  H2O_FORM_BROWSER_S                    *Statement;
  UINTN                                 Index;
  UINTN                                 ItemIndex;

  CurrentP    = gFB->CurrentP;
  ItemControl = NULL;
  Statement   = NULL;

  if (Dialog == NULL || ItemName == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Init Toggle Button
  //
  ItemControl = UiFindChildByName (Dialog, ItemName);
  if (ItemControl == NULL) {
    return EFI_NOT_FOUND;
  }

  Child = UiFindChildByName (ItemControl, L"ToggleButton");
  if (Child == NULL) {
    return EFI_NOT_FOUND;
  }

  ItemControl->OnSetState = GamingOwnerDrawPanelItemSetState;
  Child->OnSetState =  OverclockInitToggleButtonItemSetState;

  //
  // Get Statement of QuestionId
  //
  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // import formset[FORMSET_ID_GUID_CONFIGURATION].form[0x01].question[QuestionId];
    //
    if (Statement->QuestionId != QuestionId) {
      FreePool (Statement);
      continue;
    }

    SetWindowLongPtr (ItemControl->Wnd, GWLP_USERDATA, Statement->StatementId);

    Status = OverclockGetSetupItemIndexByQuestionId (Statement->QuestionId, &ItemIndex);
    if (EFI_ERROR (Status)) {
      return Status;
    }

    //
    // Update Toggle Button prompt
    //
    UiSetAttribute (UiFindChildByName (ItemControl, L"ToggleButtonPrompt"), L"text", Statement->Prompt);

    OverclockUpdateSetupData (ToggleButtonType, ItemControl, Statement);

    OverclockInitToggleButtonItemValue (Dialog, ItemName, Statement);

    FreePool (Statement);

    break;
  }

  if ((Index == CurrentP->NumberOfStatementIds) || (!mL05OverclockSetupDataTable[ItemIndex].Visibility)) {
    UiSetAttribute (ItemControl, L"visibility", L"false");
  }

  return EFI_SUCCESS;
}

VOID
OverclockItemClick (
  IN  UI_LIST_VIEW                      *This,
  IN  UI_CONTROL                        *Item,
  IN  UINT32                            Index
  )
{
  HWND                                  ItemWnd;
  UI_CONTROL                            *ItemControl;
  UI_CONTROL                            *Control;
  H2O_FORM_BROWSER_S                    *Statement;
  H2O_STATEMENT_ID                      StatementId;
  EFI_STATUS                            Status;
  UI_CONTROL                            *RadioButton;
  UINTN                                 Count;
  INT32                                 Selection;

  ItemControl = NULL;
  Control     = (UI_CONTROL *) This;
  Statement   = NULL;
  StatementId = 0;
  RadioButton = NULL;
  Selection   = OVERCLOCK_INVALID_VALUE;

  if (This == NULL || Item == NULL) {
    return;
  }

  ItemWnd = GetParent (Control->Wnd);
  ItemWnd = GetParent (ItemWnd);
  ItemWnd = GetParent (ItemWnd);
  ItemControl = GetUiControl (ItemWnd);

  if (ItemControl == NULL) {
    return;
  }

  StatementId = (H2O_STATEMENT_ID) GetWindowLongPtr (ItemControl->Wnd, GWLP_USERDATA);

  Status = gFB->GetSInfo (gFB, gFB->CurrentP->PageId, StatementId, &Statement);

  if (EFI_ERROR (Status)) {
    return;
  }

  if (Statement->Selectable) {
    if (Statement->Operand == EFI_IFR_ONE_OF_OP) {
      for (Count = 0; Count < Control->ItemCount; Count++) {
        RadioButton = UiFindChildByName (Control->Items[Count], L"RaidoButton");
        UiSetAttribute (RadioButton, L"background-image", L"@L05RadioOff");
      }

      RadioButton = UiFindChildByName (Item, L"RaidoButton");
      UiSetAttribute (RadioButton, L"background-image", L"@L05RadioOn");
    }
  }

  OverclockSetSetupData (Statement, OneOfListType, (UINTN) GetWindowLongPtr (Item->Wnd, GWLP_USERDATA), TRUE);
  GetListViewCurSel (Control, Item->Wnd, &Selection);
  OverclockSetItemCurSel (ItemWnd, Selection);
  OverclockSetItemFocus (ItemWnd);

  FreePool (Statement);
}

EFI_STATUS
OverclockInitOneOfListItemValue (
  IN UI_DIALOG                          *Dialog,
  IN CHAR16                             *ItemName,
  H2O_FORM_BROWSER_S                    *Statement
  )
{
  EFI_STATUS                            Status;
  UI_CONTROL                            *ItemControl;
  UI_CONTROL                            *Child;
  UINTN                                 Index;
  UINTN                                 ItemIndex;

  ItemControl = NULL;

  if (Dialog == NULL || ItemName == NULL || Statement == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Init One Of List
  //
  ItemControl = UiFindChildByName (Dialog, ItemName);
  if (ItemControl == NULL) {
    return EFI_NOT_FOUND;
  }

  Child = UiFindChildByName (ItemControl, L"OneOfList");
  if (Child == NULL) {
    return EFI_NOT_FOUND;
  }

  Status = OverclockGetSetupItemIndexByQuestionId (Statement->QuestionId, &ItemIndex);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  for (Index = 0; Index < Child->ItemCount; Index++) {
    if (mL05OverclockSetupDataTable[ItemIndex].Value == Statement->Options[Index].HiiValue.Value.u8) {
      break;
    }
  }

  if (Index == Child->ItemCount) {
    return EFI_NOT_FOUND;
  }

  OverclockItemClick ((UI_LIST_VIEW *) Child, Child->Items[Index], (UINT32) Index);

  return EFI_SUCCESS;
}

EFI_STATUS
OverclockInitOneOfListItem (
  IN UI_DIALOG                          *Dialog,
  IN CHAR16                             *ItemName,
  IN EFI_QUESTION_ID                    QuestionId
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_P                    *CurrentP;
  UI_CONTROL                            *ItemControl;
  UI_CONTROL                            *ListControl;
  UINTN                                 Index;
  H2O_FORM_BROWSER_S                    *Statement;
  UINTN                                 ItemIndex;
  UI_CONTROL                            *OptionControl;
  BOOLEAN                               HaveQuestionIdSupport;

  CurrentP      = gFB->CurrentP;
  ItemControl   = NULL;
  ListControl   = NULL;
  Statement     = NULL;
  OptionControl = NULL;

  if (Dialog == NULL || ItemName == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Init List window
  //
  HaveQuestionIdSupport = FALSE;
  ItemControl = UiFindChildByName (Dialog, ItemName);
  if (ItemControl == NULL) {
    return EFI_NOT_FOUND;
  }

  ListControl = UiFindChildByName (ItemControl, L"OneOfList");
  if (ListControl == NULL) {
    return EFI_NOT_FOUND;
  }

  ((UI_LIST_VIEW *) ListControl)->OnItemClick = OverclockItemClick;

  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // import formset[FORMSET_ID_GUID_CONFIGURATION].form[0x01].question[QuestionId];
    //
    if (Statement->Operand != EFI_IFR_ONE_OF_OP || Statement->QuestionId != QuestionId) {
      FreePool (Statement);
      continue;
    }

    break;
  }

  CONTROL_CLASS (ListControl)->RemoveAllChild ((UI_CONTROL *) ListControl);

  if (Index < CurrentP->NumberOfStatementIds &&
      Statement->Operand == EFI_IFR_ONE_OF_OP
      && Statement->QuestionId == QuestionId) {
    HaveQuestionIdSupport = TRUE;
    SetWindowLongPtr (ItemControl->Wnd, GWLP_USERDATA, Statement->StatementId);

    Status = OverclockGetSetupItemIndexByQuestionId (Statement->QuestionId, &ItemIndex);
    if (EFI_ERROR (Status)) {
      return Status;
    }

    //
    // Update Label text
    //
    UiSetAttribute (UiFindChildByName (ItemControl, L"ListPrompt"), L"text", Statement->Prompt);

    for (Index = 0; Index < Statement->NumberOfOptions; Index++) {
      OptionControl = XmlCreateControl (mOverclockDeviceItemChilds, ListControl);
      UiSetAttribute (UiFindChildByName (OptionControl, L"OptionName"), L"text", Statement->Options[Index].Text);
      OptionControl->OnSetState = GamingOwnerDrawPanelItemSetState;
      SetWindowLongPtr (OptionControl->Wnd, GWLP_USERDATA, Statement->Options[Index].HiiValue.Value.u8);
    }

    OverclockUpdateSetupData (OneOfListType, ItemControl, Statement);
    OverclockInitOneOfListItemValue (Dialog, ItemName, Statement);

    FreePool (Statement);
  }

  if (!HaveQuestionIdSupport || (!mL05OverclockSetupDataTable[ItemIndex].Visibility)) {
    UiSetAttribute (ItemControl, L"visibility", L"false");
  }

  return EFI_SUCCESS;
}

VOID
OverclockBarWmLbuttondownRecord (
  HWND                                  Hwnd,
  UINT32                                Msg,
  WPARAM                                WParam,
  LPARAM                                LParam
  )
{
  EFI_STATUS                            Status;
  UI_SWITCH                             *This;
  HWND                                  ItemWnd;
  UI_CONTROL                            *ItemControl;
  UI_CONTROL                            *Parent;
  H2O_FORM_BROWSER_S                    *Statement;
  UINTN                                 ItemIndex;
  H2O_STATEMENT_ID                      StatementId;

  ItemControl  = NULL;
  Parent       = NULL;
  Statement    = NULL;

  This = (UI_SWITCH *) GetUiControl (Hwnd);

  ItemWnd = GetParent (Hwnd);
  ItemWnd = GetParent (ItemWnd);
  ItemWnd = GetParent (ItemWnd);
  ItemControl = GetUiControl (ItemWnd);

  if (ItemControl == NULL) {
    return;
  }

  Parent = GetParentControlByControlName (ItemControl, L"PopUpDialog");
  if (Parent == NULL) {
    return;
  }

  StatementId = (H2O_STATEMENT_ID) GetWindowLongPtr (ItemControl->Wnd, GWLP_USERDATA);
  Status = gFB->GetSInfo (gFB, gFB->CurrentP->PageId, StatementId, &Statement);

  if (EFI_ERROR (Status)) {
    return;
  }

  Status = OverclockGetSetupItemIndexByQuestionId (Statement->QuestionId, &ItemIndex);
  if (EFI_ERROR (Status)) {
    return;
  }

  OverclockSetItemFocus (mL05OverclockSetupDataTable[ItemIndex].Wnd);

  FreePool (Statement);

  return;
}

VOID
OverclockBarWmNclbuttonupRecord (
  HWND                                  Hwnd,
  UINT32                                Msg,
  WPARAM                                WParam,
  LPARAM                                LParam
  )
{
  EFI_STATUS                            Status;
  UI_SWITCH                             *This;
  HWND                                  ItemWnd;
  UI_CONTROL                            *ItemControl;
  UI_CONTROL                            *Parent;
  H2O_FORM_BROWSER_S                    *Statement;
  UINTN                                 ItemIndex;
  H2O_STATEMENT_ID                      StatementId;
  UI_CONTROL                            *Child;
  CHAR16                                Str[20];
  BOOLEAN                               SetSetupData;
  CHAR16                                UnitStr[10];

  ItemControl  = NULL;
  Parent       = NULL;
  Statement    = NULL;
  Child        = NULL;
  SetSetupData = FALSE;

  This = (UI_SWITCH *) GetUiControl (Hwnd);

  ItemWnd = GetParent (Hwnd);
  ItemWnd = GetParent (ItemWnd);
  ItemWnd = GetParent (ItemWnd);
  ItemControl = GetUiControl (ItemWnd);

  if (ItemControl == NULL) {
    return;
  }

  Parent = GetParentControlByControlName (ItemControl, L"PopUpDialog");
  if (Parent == NULL) {
    return;
  }

  StatementId = (H2O_STATEMENT_ID) GetWindowLongPtr (ItemControl->Wnd, GWLP_USERDATA);
  Status = gFB->GetSInfo (gFB, gFB->CurrentP->PageId, StatementId, &Statement);

  if (EFI_ERROR (Status)) {
    return;
  }

  Status = OverclockGetSetupItemIndexByQuestionId (Statement->QuestionId, &ItemIndex);
  if (EFI_ERROR (Status)) {
    return;
  }

  Child = UiFindChildByName (ItemControl, L"ButtonBarValue");
  if (Child != NULL) {
    UnicodeSPrint (Str, sizeof (Str), L"%d", (This->L05ValueModeNumberRecord * mL05OverclockSetupDataTable[ItemIndex].ValueBase));

    //
    // Init Prefix Value
    //
    if (mL05OverclockSetupDataTable[ItemIndex].BaseOffset != 0) {
      if (Statement->QuestionId == L05_KEY_CPU_VOLTAGE_OFFSET ||
          Statement->QuestionId == L05_KEY_CACHE_VOLTAG_OFFSET) {
        StrCpyS (UnitStr, (sizeof (UnitStr) / sizeof (CHAR16)), L"mV");
      }

      if (Statement->QuestionId == L05_KEY_GPU_CORE_CLOCK_OFFSET ||
          Statement->QuestionId == L05_KEY_GPU_VRAM_CLOCK_OFFSET) {
        StrCpyS (UnitStr, (sizeof (UnitStr) / sizeof (CHAR16)), L"Mhz");
      }

      if (This->L05ValueModeNumberRecord >= mL05OverclockSetupDataTable[ItemIndex].BaseOffset) {
        UnicodeSPrint (Str, sizeof (Str), L"+%d %s", (This->L05ValueModeNumberRecord - mL05OverclockSetupDataTable[ItemIndex].BaseOffset), UnitStr);
        if (Statement->QuestionId == L05_KEY_GPU_CORE_CLOCK_OFFSET) {
          PcdSet16S (PcdL05GamingOverClockGpuTurboSpeed, PcdGet16 (PcdL05GamingOverClockGpuCurrentSpeed) + (UINT16)(This->L05ValueModeNumberRecord - mL05OverclockSetupDataTable[ItemIndex].BaseOffset));
        }
      } else {
        UnicodeSPrint (Str, sizeof (Str), L"-%d %s", (mL05OverclockSetupDataTable[ItemIndex].BaseOffset - This->L05ValueModeNumberRecord), UnitStr);
        if (Statement->QuestionId == L05_KEY_GPU_CORE_CLOCK_OFFSET) {
          PcdSet16S (PcdL05GamingOverClockGpuTurboSpeed, PcdGet16 (PcdL05GamingOverClockGpuCurrentSpeed) - (UINT16)(mL05OverclockSetupDataTable[ItemIndex].BaseOffset - This->L05ValueModeNumberRecord));
        }
      }
    }

#if (L05_CHIPSET_VENDOR_ID == L05_AMD_VENDOR_ID)
    if (Statement->QuestionId == L05_KEY_PRECISION_BOOST_OVERDRIVE_SCALAR) {
      if (This->L05ValueModeNumberRecord < 1) {
        This->L05ValueModeNumberRecord = 1;  // Minimum is 1X
      }

      UnicodeSPrint (Str, sizeof (Str), L"%dX", This->L05ValueModeNumberRecord);
    }

    if (Statement->QuestionId == L05_KEY_MAX_CPU_BOOST_CLOCK_OVERRIDE) {
      UnicodeSPrint (Str, sizeof (Str), L"%d Mhz", (This->L05ValueModeNumberRecord * mL05OverclockSetupDataTable[ItemIndex].ValueBase));
    }
#endif

    UiSetAttribute (Child, L"text", Str);

    SetSetupData = (Msg == WM_NCLBUTTONUP) ? TRUE : FALSE;
    OverclockSetSetupData (Statement, ButtonBarType, (UINTN)(This->L05ValueModeNumberRecord * mL05OverclockSetupDataTable[ItemIndex].ValueBase), SetSetupData);
    OverclockSetItemFocus (ItemWnd);

    //
    // Update Device Template
    //
    OverclockInitDeviceTemplate ((UI_DIALOG *) GetUiControl (Parent->Wnd));
  }

  FreePool (Statement);

  return;
}

EFI_STATUS
OverclockInitButtonBarItemValue (
  IN UI_DIALOG                          *Dialog,
  IN CHAR16                             *ItemName,
  H2O_FORM_BROWSER_S                    *Statement
  )
{
  EFI_STATUS                            Status;
  UI_CONTROL                            *ItemControl;
  UI_CONTROL                            *Child;
  UI_SWITCH                             *Switch;
  UINTN                                 ItemIndex;
  BOOLEAN                               ValuePrefix;
  UINTN                                 ValueRecord;

  ItemControl = NULL;
  Child       = NULL;
  Switch      = NULL;

  if (Dialog == NULL || ItemName == NULL || Statement == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Init Button Bar
  //
  ItemControl = UiFindChildByName (Dialog, ItemName);
  if (ItemControl == NULL) {
    return EFI_NOT_FOUND;
  }

  Child = UiFindChildByName (ItemControl, L"ButtonScalar");
  if (Child == NULL) {
    return EFI_NOT_FOUND;
  }

  Switch = (UI_SWITCH*) Child;

  Status = OverclockGetSetupItemIndexByQuestionId (Statement->QuestionId, &ItemIndex);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  UiSetAttributeEx (Child, L"L05-value-mode-number", L"%d", (Statement->Maximum / mL05OverclockSetupDataTable[ItemIndex].ValueBase));
  UiSetAttributeEx (Child, L"L05-value-mode-number-record", L"%d", (mL05OverclockSetupDataTable[ItemIndex].Value / mL05OverclockSetupDataTable[ItemIndex].ValueBase));

  //
  // Init Prefix Value
  //
  if (mL05OverclockSetupDataTable[ItemIndex].BaseOffset != 0) {
    ValuePrefix = (BOOLEAN) mL05OverclockSetupDataTable[ItemIndex - 1].Value;
    UiSetAttributeEx (Child, L"L05-value-mode-number", L"%d", (mL05OverclockSetupDataTable[ItemIndex].BaseOffset * 2));

    if (ValuePrefix) { // 0:VOLTAGE_POSITIVE_SIGN(+), 1:VOLTAGE_NEGATIVE_SIGN(-)
      ValueRecord = mL05OverclockSetupDataTable[ItemIndex].BaseOffset - mL05OverclockSetupDataTable[ItemIndex].Value;
    } else {
      ValueRecord = mL05OverclockSetupDataTable[ItemIndex].BaseOffset + mL05OverclockSetupDataTable[ItemIndex].Value;
    }

    UiSetAttributeEx (Child, L"L05-value-mode-number-record", L"%d", ValueRecord);
  }

  Switch->MoveThumb = TRUE;
  Switch->L05UpdateThumbPosByRecord = TRUE;
  SendMessage (((UI_CONTROL *) Switch)->Wnd, WM_NCLBUTTONUP, 0, 0);

  return EFI_SUCCESS;
}

EFI_STATUS
OverclockInitButtonBarItem (
  IN UI_DIALOG                          *Dialog,
  IN CHAR16                             *ItemName,
  IN EFI_QUESTION_ID                    QuestionId
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_P                    *CurrentP;
  UI_CONTROL                            *ItemControl;
  UINTN                                 Index;
  H2O_FORM_BROWSER_S                    *Statement;
  UINTN                                 ItemIndex;
  UI_CONTROL                            *Child;

  CurrentP    = gFB->CurrentP;
  ItemControl = NULL;
  Statement   = NULL;
  Child       = NULL;

  if (Dialog == NULL || ItemName == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Init Button Bar
  //
  ItemControl = UiFindChildByName (Dialog, ItemName);
  if (ItemControl == NULL) {
    return EFI_NOT_FOUND;
  }

  ItemControl->OnSetState = GamingOwnerDrawPanelItemSetState;

  for (Index = 0; Index < CurrentP->NumberOfStatementIds; Index++) {
    Status = gFB->GetSInfo (gFB, CurrentP->PageId, CurrentP->StatementIds[Index], &Statement);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // import formset[FORMSET_ID_GUID_CONFIGURATION].form[0x01].question[QuestionId];
    //
    if (Statement->QuestionId != QuestionId) {
      FreePool (Statement);
      continue;
    }

    SetWindowLongPtr (ItemControl->Wnd, GWLP_USERDATA, Statement->StatementId);
    Status = OverclockGetSetupItemIndexByQuestionId (Statement->QuestionId, &ItemIndex);
    if (EFI_ERROR (Status)) {
      return Status;
    }

    //
    // Update Prompt & Help text
    //
    UiSetAttribute (UiFindChildByName (ItemControl, L"ButtonBarPrompt"), L"text", Statement->Prompt);
    UiSetAttribute (UiFindChildByName (ItemControl, L"ButtonBarHelp"), L"text", Statement->Help);

    Child = UiFindChildByName (ItemControl, L"ButtonScalar");
    if (Child == NULL) {
      FreePool (Statement);
      continue;
    }

    ((UI_SWITCH *) Child)->L05UiSwitchWmLbuttondownRecord = OverclockBarWmLbuttondownRecord;
    ((UI_SWITCH *) Child)->L05UiSwitchWmNclbuttonupRecord = OverclockBarWmNclbuttonupRecord;

    UiSetAttribute (Child, L"L05-value-mode", L"true");
    UiSetAttribute (Child, L"L05-show-text", L"false");

    OverclockUpdateSetupData (ButtonBarType, ItemControl, Statement);
    OverclockInitButtonBarItemValue (Dialog, ItemName, Statement);
    FreePool (Statement);
    break;
  }

  if ((Index == CurrentP->NumberOfStatementIds) || (!mL05OverclockSetupDataTable[ItemIndex].Visibility)) {
    UiSetAttribute (ItemControl, L"visibility", L"false");
  }

  return EFI_SUCCESS;
}

VOID
SetToDefaultLayoutListItemClick (
  IN  UI_LIST_VIEW                      *This,
  IN  UI_CONTROL                        *Item,
  IN  UINT32                            Index
  )
{
  UI_CONTROL                            *Parent;

  Parent = NULL;

  if (This == NULL || Item == NULL) {
    return;
  }

  Parent = GetParentControlByControlName ((UI_CONTROL *) This, L"PopUpDialog");
  if (Parent == NULL) {
    return;
  }

  OverclockQuestionInteractive (L05_KEY_OVERCLOCK_SET_TO_DEFAULT);

  OverclockSetItemFocus (mSetToDefaultWnd);

  return;
}

VOID
OverclockDialogApplyCancelOnSetState (
  UI_CONTROL                            *Control,
  UI_STATE                              SetState,
  UI_STATE                              ClearState
  )
{
  if (Control == NULL) {
    return;
  }

  if ((SetState & UISTATE_HOVER) == UISTATE_HOVER) {
    gLastHoverControl = Control;
  }

  if (SetState == UISTATE_FOCUSED) {
    UiSetAttribute (Control, L"textcolor", L"0xFF257EFF");
  }

  if (ClearState == UISTATE_FOCUSED) {
    UiSetAttribute (Control, L"textcolor", L"0xFFFFFFFF");
  }

  return;
}

EFI_STATUS
L05OverclockSettingDialogInit (
  IN UI_DIALOG                          *Dialog
  )
{
  UI_CONTROL                            *Child;
  UI_CONTROL                            *ListControl;
  UI_CONTROL                            *ItemControl;
  UINTN                                 Index;
  HWND                                  FocusedWnd;
  UINTN                                 FocusedTypeIndex;
  UINTN                                 FocusedItemIndex;

  Child            = NULL;
  ListControl      = NULL;
  ItemControl      = NULL;
  FocusedWnd       = NULL;
  FocusedTypeIndex = OVERCLOCK_INVALID_VALUE;
  FocusedItemIndex = OVERCLOCK_INVALID_VALUE;

  if (Dialog == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if (mL05OverclockDialogWnd == NULL) {
    OverclockBackupSetupData ();

  } else {
    //
    // Skip invisibility item to avoid screen update abnormal.
    //
    if (!OverclockIsItemVisibility (gFB->CurrentQ->QuestionId)) {
      return EFI_ALREADY_STARTED;
    }

    OverclockGetCurrentFocusedIndex (&FocusedTypeIndex, &FocusedItemIndex);
  }

  //
  // Init Dialog String
  //
  Child = UiFindChildByName (Dialog, L"DialogTitle");
  UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_OVERCLOCKING_TITLE), NULL));

  Child = UiFindChildByName (Dialog, L"SetToDefault");
  UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_SET_TO_DEFAULT), NULL));

  Child = UiFindChildByName (Dialog, L"Apply");
  UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_APPLY), NULL));

  Child = UiFindChildByName (Dialog, L"Cancel");
  UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_CANCEL), NULL));

  ListControl = UiFindChildByName (Dialog, L"OverclockLayout");
  if (ListControl == NULL) {
    return EFI_NOT_FOUND;
  }

  CONTROL_CLASS (ListControl)->RemoveAllChild ((UI_CONTROL *) ListControl);

  //
  // Init Overclock Setup Data
  //
  mIsDialogInit = TRUE;

  for (Index = 0; Index < mL05OverclockSetupDataTableCount; Index++) {

    mL05OverclockSetupDataTable[Index].Wnd = NULL;
    mL05OverclockSetupDataTable[Index].StatementId = 0;

    switch (mL05OverclockSetupDataTable[Index].ItemType) {

    case ToggleButtonType:
      //
      // Init Toggle Button
      //
      ItemControl = XmlCreateControl (mL05OverclockToggleButton, ListControl);
      UiSetAttribute (ItemControl, L"name", mL05OverclockSetupDataTable[Index].ItemName);
      OverclockInitToggleButtonItem (Dialog, mL05OverclockSetupDataTable[Index].ItemName, mL05OverclockSetupDataTable[Index].QuestionId);
      break;

    case OneOfListType:
      //
      // Init One Of List window
      //
      ItemControl = XmlCreateControl (mL05OverclockOneOfList, ListControl);
      UiSetAttribute (ItemControl, L"name", mL05OverclockSetupDataTable[Index].ItemName);
      OverclockInitOneOfListItem (Dialog, mL05OverclockSetupDataTable[Index].ItemName, mL05OverclockSetupDataTable[Index].QuestionId);
      break;

    case ButtonBarType:
      //
      // Init Button Bar
      //
      ItemControl = XmlCreateControl (mL05OverclockButtonBar, ListControl);
      UiSetAttribute (ItemControl, L"name", mL05OverclockSetupDataTable[Index].ItemName);
      OverclockInitButtonBarItem (Dialog, mL05OverclockSetupDataTable[Index].ItemName, mL05OverclockSetupDataTable[Index].QuestionId);
      break;
    }
  }

  OverclockInitDeviceTemplate (Dialog);
  OverclockInitImage (Dialog);

  Child = UiFindChildByName (Dialog, L"SetToDefaultLayoutList");
  if (Child != NULL) {
    ((UI_LIST_VIEW *) Child)->OnItemClick = SetToDefaultLayoutListItemClick;
    SetWindowLongPtr (
      Child->Wnd,
      GWL_EXSTYLE, GetWindowLongPtr (Child->Wnd, GWL_EXSTYLE) & ~WS_EX_NOACTIVATE
      );
    mSetToDefaultWnd = Child->Wnd;
  }
  
  Child = UiFindChildByName (Child, L"SetToDefaultLayout");
  if (Child != NULL) {
    Child->OnSetState = GamingOwnerDrawPanelItemSetState;
  }

  Child = UiFindChildByName (Dialog, L"Cancel");
  if (Child != NULL) {
    Child->OnSetState = OverclockDialogApplyCancelOnSetState;
    mCancelWnd = Child->Wnd;

    if (mL05OverclockDialogWnd == NULL) {
      SetFocus (Child->Wnd);
      gLastFocus = Child->Wnd;
    }
  }

  Child = UiFindChildByName (Dialog, L"Apply");
  if (Child != NULL) {
    Child->OnSetState = OverclockDialogApplyCancelOnSetState;
    mApplyWnd = Child->Wnd;
  }

  Child = UiFindChildByName (Dialog, L"OverclockLayout");
  if (Child != NULL) {
    mOverclockLayoutWnd = Child->Wnd;
  }

  if ((mL05OverclockDialogWnd != NULL) &&
      ((FocusedTypeIndex != OVERCLOCK_INVALID_VALUE) || (FocusedItemIndex != OVERCLOCK_INVALID_VALUE))) {
    if (FocusedTypeIndex < OverclockLayoutFocusType) {
      FocusedWnd = *mL05OverclockFocusTypeTable[FocusedTypeIndex].Wnd;
    } else {
      FocusedWnd = mL05OverclockSetupDataTable[FocusedItemIndex].Wnd;
    }

    OverclockSetItemFocus (FocusedWnd);
  }

  mIsDialogInit = FALSE;

  return EFI_SUCCESS;
}

VOID
OverclockRegisterHotkey (
  HWND                                  Wnd
  )
{
  INT32                                 Id;

  if (Wnd == NULL) {
    return;
  }

  Id = 1;
  while (UnregisterHotKey (Wnd, Id)) {
    Id++;
  }

  Id = 1;
  MwRegisterHotKey (Wnd, Id++,  0, VK_ESCAPE);
  MwRegisterHotKey (Wnd, Id++,  0, VK_UP);
  MwRegisterHotKey (Wnd, Id++,  0, VK_DOWN);
  MwRegisterHotKey (Wnd, Id++,  0, VK_OEM_PLUS);
  MwRegisterHotKey (Wnd, Id++,  0, VK_OEM_MINUS);

  return;
}

EFI_STATUS
OverclockPopupApplyDialog (
  BOOLEAN                               *IsApply
  )
{
  EFI_STATUS                            Status;
  H2O_DIALOG_PROTOCOL                   *H2ODialogData;
  CHAR16                                *StringPtr;
  EFI_INPUT_KEY                         Key;

  H2ODialogData = NULL;
  StringPtr     = NULL;

  if (IsApply == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  *IsApply = FALSE;

  Status = gBS->LocateProtocol (&gH2ODialogProtocolGuid, NULL, &H2ODialogData);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  StringPtr = AllocateCopyPool (StrSize (L"Apply Settings?"), L"Apply Settings?");
  if (StringPtr == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  mDialogWnd = NULL;

  Status = H2ODialogData->ConfirmDialog (
                            L05OverclockDlgApplyCancel,
                            FALSE,
                            0,
                            NULL,
                            &Key,
                            StringPtr
                            );

  EnableWindow (gWnd, FALSE);
  EnableWindow (GetDesktopWindow (), FALSE);

  *IsApply = (Key.UnicodeChar == L'Y') ? TRUE : FALSE;

  mDialogWnd = mL05OverclockDialogWnd;
  OverclockRegisterHotkey (mL05OverclockDialogWnd);

  FreePool (StringPtr);

  return Status;
}

EFI_STATUS
GetListViewCurSel (
  IN  UI_CONTROL                        *Control,
  IN  HWND                              ItemWnd,
  OUT INT32                             *CurSel
  )
{
  UINTN                                 Index;

  if (Control == NULL || ItemWnd == NULL || CurSel == NULL) {
  }

  for (Index = 0; Index < Control->ItemCount; Index++) {
    if (Control->Items[Index]->Wnd == ItemWnd) {
      break;
    }
  }

  if (Index == Control->ItemCount) {
    return EFI_NOT_FOUND;
  }

  *CurSel = (INT32) Index;

  return EFI_SUCCESS;
}

VOID
OverclockHotkeyListView (
  IN  UINTN                             KeyCode,
  IN  UI_CONTROL                        *ListControl,
  IN  UI_CONTROL                        *NextControl,
  IN  HWND                              FocusedWnd,
  IN  UINT32                            *NextSelection
  )
{
  UI_LIST_VIEW                          *ListView;
  UINTN                                 ListItemCount;

  ListView            = NULL;
  ListItemCount       = 0;

  if (ListControl == NULL || FocusedWnd == NULL || NextSelection == NULL) {
    return;
  }

  if (KeyCode == VK_UP || KeyCode == VK_DOWN) {
    ListView = (UI_LIST_VIEW *) ListControl;
    ListItemCount = ListControl->ItemCount;

    if (ListControl != NextControl) {
      *NextSelection = LIST_VIEW_CLASS (ListView)->FindNextSelection (ListView, ListView->CurSel, KeyCode);
    } else {
      *NextSelection = ListView->CurSel;
    }

    if (KeyCode == VK_UP && ListView->CurSel == 0) {
      return;
    }

    if (KeyCode == VK_DOWN &&
        (ListView->CurSel == (ListItemCount - 1) ||
         *NextSelection == 0xffffffff)) {
      return;
    }

    if (ListControl != NextControl) {
      SendMessage (FocusedWnd, WM_KEYDOWN, KeyCode, 0);
    }
  }

  return;
}

VOID
OverclockSetFocus (
  IN  HWND                              SetFocusWnd,
  IN  INT32                             *Selection
  )
{
  UI_CONTROL                            *ListControl;
  UI_LIST_VIEW                          *ListView;
  INT32                                 ListViewCurSel;

  ListControl = NULL;
  ListView    = NULL;

  if (SetFocusWnd == NULL) {
    return;
  }

  SetFocus (SetFocusWnd);
  gLastFocus = SetFocusWnd;

  ListControl = (UI_CONTROL *) GetUiControl (SetFocusWnd);
  if (StrCmp (ListControl->Class->ClassName, L"ListView") == 0) {
    ListView = (UI_LIST_VIEW *) ListControl;
    ListViewCurSel = (Selection != NULL) ? *Selection : ListView->CurSel;
    LIST_VIEW_CLASS (ListView)->SetSelection (ListView, (ListViewCurSel >= 0) ? ListViewCurSel : 0, TRUE);
  }

  return;
}

EFI_STATUS
OverclockSetPlusButton (
  IN UI_CONTROL                         *Control
  )
{
  HWND                                  ItemWnd;
  UI_CONTROL                            *ItemControl;
  UI_SWITCH                             *Switch;

  ItemControl = NULL;
  Switch      = NULL;

  if (Control == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ItemWnd = GetParent (Control->Wnd);
  ItemWnd = GetParent (ItemWnd);
  ItemWnd = GetParent (ItemWnd);
  ItemControl = GetUiControl (ItemWnd);

  if (ItemControl == NULL) {
    return EFI_NOT_FOUND;
  }

  Switch = (UI_SWITCH *) UiFindChildByName (ItemControl, L"ButtonScalar");
  if (Switch != NULL && Switch->L05ValueModeNumberRecord < Switch->L05ValueModeNumber) {
    UiSetAttributeEx (Switch, L"L05-value-mode-number-record", L"%d", Switch->L05ValueModeNumberRecord + 1);
    Switch->MoveThumb = TRUE;
    Switch->L05UpdateThumbPosByRecord = TRUE;
    SendMessage (((UI_CONTROL *) Switch)->Wnd, WM_NCLBUTTONUP, 0, 0);
  }

  return EFI_SUCCESS;
}

EFI_STATUS
OverclockSetMinusButton (
  IN UI_CONTROL                         *Control
  )
{
  HWND                                  ItemWnd;
  UI_CONTROL                            *ItemControl;
  UI_SWITCH                             *Switch;

  ItemControl = NULL;
  Switch      = NULL;

  if (Control == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ItemWnd = GetParent (Control->Wnd);
  ItemWnd = GetParent (ItemWnd);
  ItemWnd = GetParent (ItemWnd);
  ItemControl = GetUiControl (ItemWnd);

  if (ItemControl == NULL) {
    return EFI_NOT_FOUND;
  }

  Switch = (UI_SWITCH *) UiFindChildByName (ItemControl, L"ButtonScalar");
  if (Switch != NULL && Switch->L05ValueModeNumberRecord > 0) {
    UiSetAttributeEx (Switch, L"L05-value-mode-number-record", L"%d", Switch->L05ValueModeNumberRecord - 1);
    Switch->MoveThumb = TRUE;
    Switch->L05UpdateThumbPosByRecord = TRUE;
    SendMessage (((UI_CONTROL *) Switch)->Wnd, WM_NCLBUTTONUP, 0, 0);
  }

  return EFI_SUCCESS;
}

VOID
OverclockSetItemActionFocus (
  IN  HWND                              SetFocusWnd,
  IN  UINTN                             KeyCode
  )
{
  EFI_STATUS                            Status;
  UI_CONTROL                            *ListControl;
  UI_LIST_VIEW                          *ListView;
  UINTN                                 ItemIndex;
  UI_CONTROL                            *ItemControl;
  UI_CONTROL                            *Child;

  ListControl = NULL;
  ListView    = NULL;
  ItemControl = NULL;
  Child       = NULL;

  if (SetFocusWnd == NULL) {
    return;
  }

  ListControl = (UI_CONTROL *) GetUiControl (SetFocusWnd);

  if (StrCmp (ListControl->Class->ClassName, L"ListView") == 0) {
    ListView = (UI_LIST_VIEW *) ListControl;
  
    if (ListView->CurSel < ListControl->ItemCount) {
      Status = OverclockGetSetupItemIndexByWnd (ListControl->Items[ListView->CurSel]->Wnd, &ItemIndex);
      if (EFI_ERROR (Status)) {
        return;
      }
  
      ItemControl = (UI_CONTROL *) GetUiControl (ListControl->Items[ListView->CurSel]->Wnd);
  
      switch (mL05OverclockSetupDataTable[ItemIndex].ItemType) {
  
      case ToggleButtonType:
        Child = UiFindChildByName (ItemControl, L"ToggleButton");
        break;

      case OneOfListType:
        Child = UiFindChildByName (ItemControl, L"OneOfList");
        OverclockSetFocus (Child->Wnd, NULL);
        Child = NULL;
        break;

      case ButtonBarType:
        if (KeyCode == VK_OEM_PLUS) {
          Child = UiFindChildByName (ItemControl, L"PlusButton");
          OverclockSetPlusButton (Child);
          Child = NULL;

        } else if (KeyCode == VK_OEM_MINUS) {
          Child = UiFindChildByName (ItemControl, L"MinusButton");
          OverclockSetMinusButton (Child);
          Child = NULL;

        } else {
          Child = UiFindChildByName (ItemControl, L"ButtonScalar");
        }

        break;
      }

      if (Child != NULL) {
        SetFocus (Child->Wnd);
        gLastFocus = Child->Wnd;
      }
    }
  }

  return;
}

VOID
OverclockUpdateFocusedWnd (
  IN  HWND                              *FocusedWnd
  )
{
  EFI_STATUS                            Status;
  UI_CONTROL                            *FocusedControl;
  HWND                                  SetupItemWnd;
  INT32                                 Selection;

  SetupItemWnd   = NULL;
  FocusedControl = NULL;

  if (FocusedWnd == NULL || *FocusedWnd == NULL) {
    return;
  }

  FocusedControl = GetUiControl (*FocusedWnd);

  if (FocusedControl == NULL) {
    return;
  }

  if ((*FocusedWnd != mApplyWnd &&
       *FocusedWnd != mCancelWnd &&
       *FocusedWnd != mSetToDefaultWnd &&
       *FocusedWnd != mOverclockLayoutWnd &&
       StrCmp (FocusedControl->Class->ClassName, L"ListView") != 0)) {

    SetupItemWnd = OverclockGetSetupItemWndByWnd (*FocusedWnd);
    *FocusedWnd = GetParentWndByClassName (SetupItemWnd, L"ListView");
    if (*FocusedWnd == NULL) {
      return;
    }

    FocusedControl = GetUiControl (*FocusedWnd);
    if (FocusedControl == NULL) {
      return;
    }
    
    Status = GetListViewCurSel (FocusedControl, SetupItemWnd, &Selection);
    if (EFI_ERROR (Status)) {
      *FocusedWnd = NULL;
      return;
    }

    OverclockSetFocus (*FocusedWnd, &Selection);
  }

  return;
}

/**
  Process the hotkey event of Gaming Overclock Page.

  @param  Control                       The pointer of UI control.
  @param  KeyCode                       The data of the hotkey.

  @retval None
**/
VOID
OverclockHotkey (
  IN  UI_CONTROL                        *Control,
  IN  UINTN                             KeyCode
  )
{
  HWND                                  FocusedWnd;
  HWND                                  NextFocusWnd;
  UI_CONTROL                            *FocusedControl;
  UI_CONTROL                            *NextControl;
  UI_CONTROL                            *ListControl;
  UINT32                                NextSelection;
  UINTN                                 ItemIndex;

  FocusedWnd          = NULL;
  NextFocusWnd        = NULL;
  FocusedControl      = NULL;
  NextControl         = NULL;
  ListControl         = NULL;
  NextSelection       = 0;
  ItemIndex           = mL05OverclockSetupDataTableCount;

  FocusedWnd = GetFocus ();
  NextFocusWnd = NULL;

  FocusedControl = GetUiControl (FocusedWnd);

  if (FocusedControl == NULL) {
    return;
  }

  switch (KeyCode) {
  case VK_UP:
  case VK_DOWN:
    FocusedControl = NULL;
    OverclockUpdateFocusedWnd (&FocusedWnd);
    FocusedControl = GetUiControl (FocusedWnd);

    if (FocusedWnd == NULL || FocusedControl == NULL) {
      return;
    }

    if (FocusedWnd == mApplyWnd || FocusedWnd == mCancelWnd) {
      if (KeyCode == VK_DOWN) {
        break;
      }

      NextFocusWnd = mSetToDefaultWnd;
      break;
    }

    if (FocusedWnd == mSetToDefaultWnd) {
      if (KeyCode == VK_DOWN) {
        NextFocusWnd = mCancelWnd;
        break;

      } else if (KeyCode == VK_UP) {
        NextFocusWnd = mOverclockLayoutWnd;
        NextControl = (UI_CONTROL *) GetUiControl (NextFocusWnd);
      }
    }

    ListControl = (NextControl != NULL) ? NextControl : FocusedControl;

    if (StrCmp (ListControl->Class->ClassName, L"ListView") == 0) {
      if (ListControl->Wnd != mOverclockLayoutWnd) {
        OverclockHotkeyListView (KeyCode, ListControl, NextControl, FocusedWnd, &NextSelection);
        if (NextSelection == 0xffffffff) {
          FocusedWnd = mOverclockLayoutWnd;
          NextFocusWnd = mOverclockLayoutWnd;
          ListControl = (UI_CONTROL *) GetUiControl (FocusedWnd);
          OverclockSetFocus (FocusedWnd, NULL);
        } else {
          break;
        }
      }

      if (KeyCode == VK_UP || KeyCode == VK_DOWN) {
        OverclockHotkeyListView (KeyCode, ListControl, NextControl, FocusedWnd, &NextSelection);
        OverclockSetItemActionFocus (ListControl->Wnd, KeyCode);
        if (NextSelection == 0xffffffff) {
          if (KeyCode == VK_DOWN) {
            NextFocusWnd = mSetToDefaultWnd;
          }
          break;
        }

        if (ListControl != NextControl) {
          return;
        }
      }
    }
    break;

  case VK_OEM_PLUS:
  case VK_OEM_MINUS:
    FocusedControl = NULL;
    OverclockUpdateFocusedWnd (&FocusedWnd);
    FocusedControl = GetUiControl (FocusedWnd);

    if (FocusedWnd == NULL || FocusedControl == NULL) {
      return;
    }

    OverclockSetItemActionFocus (FocusedWnd, KeyCode);
    break;

  default:
    break;
  }

  OverclockSetFocus (NextFocusWnd, NULL);
  OverclockSetItemActionFocus (NextFocusWnd, KeyCode);

  return;
}

/**
  Init function for Gaming Exit Dialog.

  @param  UI_DIALOG                     Point to Dialog.

  @retval EFI_SUCCESS                   This function execute successfully.
**/
LRESULT
OverclockSettingDialogProc (
  HWND                                  Wnd,
  UINT                                  Msg,
  WPARAM                                WParam,
  LPARAM                                LParam
  )
{
  UI_DIALOG                             *Dialog;
  HWND                                  CurrentFocus;
  UI_CONTROL                            *CurrentFocusControl;
  UI_CONTROL                            *Control;
  UI_CONTROL                            *Child;
  UINTN                                 Index;
  RECT                                  Rect;
  SIZE                                  Size;
  BOOLEAN                               IsOn;
  UINTN                                 ItemIndex;
  BOOLEAN                               IsApply;

  Dialog = (UI_DIALOG *) GetUiControl (Wnd);
  CurrentFocus = GetFocus ();
  CurrentFocusControl = GetUiControl (CurrentFocus);

  switch (Msg) {

  case UI_NOTIFY_WINDOWINIT:
    L05OverclockSettingDialogInit (Dialog);
    OverclockRegisterHotkey (Wnd);

    //
    // Move window position
    //
    GetWindowRect (gWnd, &Rect);
    Child = UiFindChildByName (Dialog, L"PopUpDialog");
    if (Child != NULL) {
      Size = Child->FixedSize;
      SetWindowPos (
        Wnd,
        HWND_TOP,
        (Rect.right  / 2) - (Size.cx / 2) - 1,
        (Rect.bottom / 2) - (Size.cy / 2),
        Size.cx,
        Size.cy,
        0
        );
      SetWindowLongPtr (
        Child->Wnd,
        GWL_EXSTYLE, GetWindowLongPtr (Child->Wnd, GWL_EXSTYLE) & ~WS_EX_NOACTIVATE
        );
    }
    break;

  case UI_NOTIFY_CLICK:
    Control = (UI_CONTROL *) WParam;
    Index = (UINTN) GetWindowLongPtr (Control->Wnd, GWLP_USERDATA);
    if (StrCmp (Control->Name, L"Apply") == 0) {
      OverclockPopupApplyDialog (&IsApply);
      SetFocus (Control->Wnd);
      gLastFocus = Control->Wnd;

      if (IsApply) {
        SendShutDNotify ();
      }

      break;

    } else if (StrCmp (Control->Name, L"ToggleButton") == 0) {
      IsOn = (BOOLEAN) GetWindowLongPtr (Control->Wnd, GWLP_USERDATA);
      IsOn = IsOn == 1 ? 0 : 1;
      OverclockGetSetupItemIndexByWnd (OverclockGetSetupItemWndByWnd (Control->Wnd), &ItemIndex);
      if (IsOn &&
          ItemIndex < mL05OverclockSetupDataTableCount &&
          (mL05OverclockSetupDataTable[ItemIndex].ItemGroup == CpuAdvanced ||
           mL05OverclockSetupDataTable[ItemIndex].ItemGroup == GpuAdvanced)) {
        mApplyDialogWnd = Control->Wnd;
        OverclockPopupApplyDialog (&IsApply);
        SetFocus (Control->Wnd);
        gLastFocus = Control->Wnd;

        if (!IsApply) {
          break;
        }
      }

      OverclockDialogSetToggleButton (Control, IsOn);

    } else if (StrCmp (Control->Name, L"Cancel") == 0 ||
               StrCmp (Control->Name, L"CloseButton") == 0) {
      OverclockRestoreSetupData ();
      SendShutDNotify ();

    } else if (StrCmp (Control->Name, L"PlusButton") == 0) {
      OverclockSetPlusButton (Control);

    } else if (StrCmp (Control->Name, L"MinusButton") == 0) {
      OverclockSetMinusButton (Control);
    }

    break;

  case WM_HOTKEY:
    if (HIWORD (LParam) == VK_ESCAPE) {
      OverclockRestoreSetupData ();
      SendShutDNotify ();
      return 0;
    }

    Control = (UI_CONTROL *) WParam;
    OverclockHotkey (Control, (UINTN) HIWORD (LParam));

    return 1;

  case WM_DESTROY:
    SetFocus (mGamingFocusableWnd[OverclockWnd].FocusWnd);
    gLastFocus = mGamingFocusableWnd[OverclockWnd].FocusWnd;
    gLastHoverControl = NULL;
    mL05OverclockDialogWnd = NULL;
    PcdSetBoolS (PcdL05GamingOverClockDialogFlag, FALSE);
    SendMessage (gWnd, FB_NOTIFY_REPAINT, 0, 0);
    return 0;

  default:
    return 0;
  }

  return 1;
}

VOID
OverclockApplyDialogApplyCancelOnSetState (
  UI_CONTROL                            *Control,
  UI_STATE                              SetState,
  UI_STATE                              ClearState
  )
{
  if (Control == NULL) {
    return;
  }

  if (SetState == UISTATE_FOCUSED) {
    UiSetAttribute (Control, L"textcolor", L"0xFF257EFF");
  }

  if (ClearState == UISTATE_FOCUSED) {
    UiSetAttribute (Control, L"textcolor", L"0xFFFFFFFF");
  }

  return;
}

EFI_STATUS
OverclockApplyDialogInit (
  IN UI_DIALOG                          *Dialog
  )
{
  UI_CONTROL                            *Child;

  if (Dialog == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Init Dialog String
  //
  Child = UiFindChildByName (Dialog, L"PopWindowsTitle");
  UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_APPLY_SETTINGS_TITLE), NULL));

  Child = UiFindChildByName (Dialog, L"WaringText");
  if (Dialog->ParentWnd == mApplyWnd) {
    UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_OVERCLOCKING_WARNING_ON_EXIT), NULL));
  } else {
    UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_OVERCLOCKING_WARNING_ON_ENTRY), NULL));
  }

  Child = UiFindChildByName (Dialog, L"Apply");
  UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_APPLY), NULL));

  Child = UiFindChildByName (Dialog, L"Cancel");
  UiSetAttribute (Child, L"text", HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_CANCEL), NULL));

  //
  // Init Button
  //
  Child = UiFindChildByName (Dialog, L"Cancel");
  if (Child != NULL) {
    Child->OnSetState = OverclockApplyDialogApplyCancelOnSetState;
    //
    // ButtonStringArray[1] = GetString (STRING_TOKEN (L05_STR_CANCEL), mHiiHandle);
    //
    SetWindowLongPtr (Child->Wnd, GWLP_USERDATA, (INTN) 0x01);
    SetFocus (Child->Wnd);
    gLastFocus = Child->Wnd;
  }

  Child = UiFindChildByName (Dialog, L"Apply");
  if (Child != NULL) {
    Child->OnSetState = OverclockApplyDialogApplyCancelOnSetState;
    //
    // ButtonStringArray[0] = GetString (STRING_TOKEN (L05_STR_APPLY), mHiiHandle);
    //
    SetWindowLongPtr (Child->Wnd, GWLP_USERDATA, (INTN) 0x00);
  }

  return EFI_SUCCESS;
}

LRESULT
L05OverclockApplyDialogProc (
  HWND                                  Wnd,
  UINT                                  Msg,
  WPARAM                                WParam,
  LPARAM                                LParam
  )
{
  UI_DIALOG                             *Dialog;
  HWND                                  CurrentFocus;
  UI_CONTROL                            *Control;
  UI_CONTROL                            *Child;
  UINTN                                 Index;
  RECT                                  Rect;
  SIZE                                  Size;

  Dialog = (UI_DIALOG *) GetUiControl (Wnd);
  CurrentFocus = GetFocus ();

  switch (Msg) {

  case UI_NOTIFY_WINDOWINIT:
    OverclockApplyDialogInit (Dialog);
    //
    // Move window position
    //
    GetWindowRect (gWnd, &Rect);
    Child = UiFindChildByName (Dialog, L"PopUpDialog");
    if (Child != NULL) {
      Size = Child->FixedSize;
      SetWindowPos (
        Wnd,
        HWND_TOP,
        (Rect.right  / 2) - (Size.cx / 2) - 1,
        (Rect.bottom / 2) - (Size.cy / 2),
        Size.cx,
        Size.cy,
        0
        );
      SetWindowLongPtr (
        Child->Wnd,
        GWL_EXSTYLE, GetWindowLongPtr (Child->Wnd, GWL_EXSTYLE) & ~WS_EX_NOACTIVATE
        );
    }
    break;

  case UI_NOTIFY_CLICK:
    Control = (UI_CONTROL *) WParam;
    Index = (UINTN) GetWindowLongPtr (Control->Wnd, GWLP_USERDATA);
    SendChangeQNotify (0, 0, &mFbDialog->ButtonHiiValueArray[Index]);
    break;

  case WM_HOTKEY:
    if (HIWORD (LParam) == VK_ESCAPE) {
      SendShutDNotify ();
      SetFocus (Dialog->ParentWnd);
      gLastFocus = Dialog->ParentWnd;
      return 0;
    }
    return 1;

  case WM_DESTROY:
    if (CurrentFocus != mCancelWnd) {
      SetFocus (Dialog->ParentWnd);
      gLastFocus = Dialog->ParentWnd;
    }
    return 0;

  default:
    return 0;
  }

  return 1;
}
