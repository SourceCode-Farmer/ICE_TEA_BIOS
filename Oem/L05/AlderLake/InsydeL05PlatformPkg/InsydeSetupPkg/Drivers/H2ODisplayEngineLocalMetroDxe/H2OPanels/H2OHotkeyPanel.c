/** @file
  UI Common Controls

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "H2ODisplayEngineLocalMetro.h"
#include "H2OPanels.h"
#include "MetroUi.h"
//_Start_L05_GRAPHIC_UI_
#include "L05Hook/L05GetResolution.h"
//_End_L05_GRAPHIC_UI_

STATIC H2O_HOTKEY_PANEL_CLASS            *mH2OHotkeyPanelClass = NULL;
#define CURRENT_CLASS                    mH2OHotkeyPanelClass

//_Start_L05_GRAPHIC_UI_
CHAR16 *mHotkeyPanelChilds = L""
  L"<VerticalLayout width='match_parent' height='match_parent' background-color='0x0' padding='0,70,0,70'>"
    L"<Control height='10' />"
    L"<ListView name='Container' />"
  L"</VerticalLayout>";
//_End_L05_GRAPHIC_UI_

EFI_STATUS
RefreshHotkeyList (
  H2O_HOTKEY_PANEL              *This
  )
{
  UINTN                         Index;
  HOT_KEY_INFO                  *HotkeyInfo;
  HOT_KEY_INFO                  *CurrentHotkey;
  UI_CONTROL                    *HotkeyControl;
//_Start_L05_GRAPHIC_UI_
//  CHAR16                        Str[20];
//_End_L05_GRAPHIC_UI_
  UINTN                         GroupIdIndex;
  UINT8                         GroupId[HOT_KEY_INFO_GROUP_ID_MAX];
  BOOLEAN                       NewGroup;
  BOOLEAN                       OldGroup;
//_Start_L05_GRAPHIC_UI_
  CHAR16                        Str[100];
  UI_CONTROL                    *Container;
  UI_CONTROL                    *PanelControl;
  UINTN                         Padding;
  UINTN                         CellWidth;
  UINTN                         CellHeight;
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
  Container    = UiFindChildByName (This, L"Container");
  PanelControl = (UI_CONTROL*) This;
  Padding      = 0;
  CellWidth    = 0;
  CellHeight   = 0;
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
  //
  // cellwidth = (PanelWidth - Padding(left & right) - 4 Hoykey in Row(Base hotkey width x 4)) / 3
  // ex. (1920 - (70 + 70) - (300 x 4)) / 3 = 193
  //
  Padding = L05_GUI_HOTKEY_PADDING_LEFT + L05_GUI_HOTKEY_PADDING_RIGHT;
  if (PanelControl->FixedSize.cx >= (Padding + (L05_GUI_HOTKEY_ITEM_BASE_WIDTH * 4))) {
    CellWidth = (PanelControl->FixedSize.cx - Padding - (L05_GUI_HOTKEY_ITEM_BASE_WIDTH * 4)) / 3;
  }

  //
  // Panel's height must not smaller than L05_GUI_HOTKEY_BASE_HEIGHT
  //
  // CellHeight = (PanelHeight - Padding(top + bottom) - 2 Hoykey in Column(Base hotkey height x 2)) / 2
  // ex. (130 - (10 + 40) - (30 x 2)) / 2 = 10
  //
  Padding = L05_GUI_HOTKEY_PADDING_TOP + L05_GUI_HOTKEY_PADDING_BOTTOM;
  if (PanelControl->FixedSize.cy >= L05_GUI_HOTKEY_BASE_HEIGHT) {
    CellHeight = (PanelControl->FixedSize.cy - Padding - (L05_GUI_HOTKEY_ITEM_BASE_HEIGHT * 2)) / 2;
  }

  UnicodeSPrint (Str, sizeof (Str), L"layout='cellspnanning' cellwidth='%d' cellheight='%d'", CellWidth, CellHeight);
  UiApplyAttributeList ((UI_CONTROL*) Container, Str);
//_End_L05_GRAPHIC_UI_

  //
  // remove old items
  //
//_Start_L05_GRAPHIC_UI_
//  CONTROL_CLASS (This)->RemoveAllChild ((UI_CONTROL *)This);
  CONTROL_CLASS (Container)->RemoveAllChild ((UI_CONTROL *)Container);
//_End_L05_GRAPHIC_UI_

  //
  // Create hotkey items
  //
  ASSERT (gFB->CurrentP != NULL);
  ASSERT (gFB->CurrentP->HotKeyInfo != NULL);
  HotkeyInfo = gFB->CurrentP->HotKeyInfo;

  for (GroupIdIndex = 0; GroupIdIndex < HOT_KEY_INFO_GROUP_ID_MAX; GroupIdIndex ++) {
    GroupId[GroupIdIndex] = HOT_KEY_INFO_GROUP_ID_NONE;
  }

  for (Index = 0; !IS_END_OF_HOT_KEY_INFO(&HotkeyInfo[Index]); Index++) {
    CurrentHotkey = &HotkeyInfo[Index];
    if (!CurrentHotkey->Display) {
      continue;
    }

    NewGroup = FALSE;
    OldGroup = FALSE;
    if (CurrentHotkey->GroupId != HOT_KEY_INFO_GROUP_ID_NONE) {
      for (GroupIdIndex = 0; GroupIdIndex < HOT_KEY_INFO_GROUP_ID_MAX; GroupIdIndex ++) {
        if (GroupId[GroupIdIndex] == HOT_KEY_INFO_GROUP_ID_NONE) {
          //
          // New GroupId
          //
          NewGroup = TRUE;
          GroupId[GroupIdIndex] = CurrentHotkey->GroupId;
          break;
        } else if (GroupId[GroupIdIndex] == CurrentHotkey->GroupId) {
          //
          // Same GroupId
          //
          OldGroup = TRUE;
          UnicodeSPrint (Str, sizeof (Str), L"Hotkey_Group%02x", CurrentHotkey->GroupId);
//_Start_L05_GRAPHIC_UI_
//          HotkeyControl = UiFindChildByName (This, Str);
          HotkeyControl = UiFindChildByName (Container, Str);
//_End_L05_GRAPHIC_UI_
          UnicodeSPrint (Str, sizeof (Str), L"0x%p", CurrentHotkey);
          UiSetAttribute (HotkeyControl, L"hotkey", Str);
          break;
        }
      }
    }

    if (OldGroup) {
      continue;
    }

    //
    // Create new HotkeyItem
    //
//_Start_L05_GRAPHIC_UI_
//    HotkeyControl = CreateControl (L"HotkeyItem", (UI_CONTROL*) This);
    HotkeyControl = CreateControl (L"HotkeyItem", (UI_CONTROL*) Container);
//_End_L05_GRAPHIC_UI_
    UnicodeSPrint (Str, sizeof (Str), L"0x%p", CurrentHotkey);
    UiSetAttribute (HotkeyControl, L"hotkey", Str);

    if (NewGroup) {
      UnicodeSPrint (Str, sizeof (Str), L"Hotkey_Group%02x", CurrentHotkey->GroupId);
      UiSetAttribute (HotkeyControl, L"name", Str);
    } else {
      UiSetAttribute (HotkeyControl, L"name", L"Hotkey");
    }
//_Start_L05_GRAPHIC_UI_
//    CONTROL_CLASS(This)->AddChild ((UI_CONTROL *)This, HotkeyControl);
    UiSetAttribute (HotkeyControl, L"width", L"300");
    UiSetAttribute (HotkeyControl, L"height", L"30");

    CONTROL_CLASS(Container)->AddChild ((UI_CONTROL *)Container, HotkeyControl);
//_End_L05_GRAPHIC_UI_
  }

  return EFI_SUCCESS;
}

LRESULT
EFIAPI
H2OHotkeyPanelProc (
  HWND   Wnd,
  UINT32 Msg,
  WPARAM WParam,
  LPARAM LParam
  )
{
  H2O_HOTKEY_PANEL              *This;
  UI_CONTROL                    *Control;


  This = (H2O_HOTKEY_PANEL *) GetWindowLongPtr (Wnd, 0);
  if (This == NULL && Msg != WM_CREATE && Msg != WM_NCCALCSIZE) {
    ASSERT (FALSE);
    return 0;
  }
  Control = (UI_CONTROL *)This;

  switch (Msg) {

  case WM_CREATE:
    This = (H2O_HOTKEY_PANEL *) AllocateZeroPool (sizeof (H2O_HOTKEY_PANEL));
    if (This != NULL) {
      CONTROL_CLASS (This) = (UI_CONTROL_CLASS *) GetClassLongPtr (Wnd, 0);
      SetWindowLongPtr (Wnd, 0, (INTN)This);
      SendMessage (Wnd, UI_NOTIFY_CREATE, WParam, LParam);
    }
    break;

  case UI_NOTIFY_CREATE:
    PARENT_CLASS_WNDPROC (CURRENT_CLASS, Wnd, UI_NOTIFY_CREATE, WParam, LParam);
//_Start_L05_GRAPHIC_UI_
    XmlCreateControl (mHotkeyPanelChilds, Control);
//_End_L05_GRAPHIC_UI_
    break;

  case FB_NOTIFY_REPAINT:
    RefreshHotkeyList (This);
    break;


  default:
    return PARENT_CLASS_WNDPROC (CURRENT_CLASS, Wnd, Msg, WParam, LParam);

  }
  return 0;

}

H2O_HOTKEY_PANEL_CLASS *
EFIAPI
GetH2OHotkeyPanelClass (
  VOID
  )
{
  if (CURRENT_CLASS != NULL) {
    return CURRENT_CLASS;
  }

//_Start_L05_GRAPHIC_UI_
//  InitUiClass ((UI_CONTROL_CLASS **)&CURRENT_CLASS, sizeof (*CURRENT_CLASS), L"H2OHotkeyPanel", (UI_CONTROL_CLASS *)GetHorizontalLayoutClass ());
  InitUiClass ((UI_CONTROL_CLASS **)&CURRENT_CLASS, sizeof (*CURRENT_CLASS), L"H2OHotkeyPanel", (UI_CONTROL_CLASS *) GetControlClass());
//_End_L05_GRAPHIC_UI_
  if (CURRENT_CLASS == NULL) {
    return NULL;
  }
  ((UI_CONTROL_CLASS *)CURRENT_CLASS)->WndProc = H2OHotkeyPanelProc;

  return CURRENT_CLASS;
}

