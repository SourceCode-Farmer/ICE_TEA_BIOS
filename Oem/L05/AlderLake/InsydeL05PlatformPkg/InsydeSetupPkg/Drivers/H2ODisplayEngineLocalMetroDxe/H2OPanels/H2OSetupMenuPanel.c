/** @file
  UI Common Controls

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "H2ODisplayEngineLocalMetro.h"
#include "UiControls.h"
#include "H2OPanels.h"
#include "MetroUi.h"

STATIC H2O_SETUP_MENU_PANEL_CLASS        *mH2OSetupMenuPanelClass = NULL;
#define CURRENT_CLASS                    mH2OSetupMenuPanelClass

extern HWND                     gLastFocus;

//_Start_L05_GRAPHIC_UI_
CHAR16 *mH2OSetupMenuTopChilds = L""
  L"<VerticalLayout name='ProductImg' background-color='0xFFFFFFFF' padding='0,0,0,70' height='245'>"
#ifndef L05_SMB_BIOS_ENABLE
    L"<Texture name='BrandingLogo' width='315' height='85' background-image='@Yoga'/>"
#else
    L"<Texture name='BrandingLogo' width='315' height='109' background-image='@LenovoBranding'/>"
#endif
    L"<Control />"
    L"<Label name='StartMenu' text='Start Menu' text-align='left' height='38'  font-size='38' textcolor='0xFF0F75BC' />"
    L"<Control height='26' />"
  L"</VerticalLayout>";

CHAR16 *mH2OSetupMenuBottomChilds = L""
  L"<VerticalLayout background-color='0xFFFFFFFF' padding='0,0,0,70'>"
    L"<Control />"
#ifndef L05_SMB_BIOS_ENABLE
    L"<Texture name='LenovoLogo' width='260' height='88' background-image='@LenovoColor'/>"
#else
    L"<Texture name='LenovoLogo' width='260' height='88'/>"
#endif
    L"<Control height='42' />"
  L"</VerticalLayout>";

//
// Need to be aligned with the enum order of L05_BRANDING_LOGO_TYPE
//
CHAR16 *mBrandingLogoImageStr[] = {
  L"@Yoga",
  L"@Ideapad",
  L"@Legion",
  L"@XiaoXin",
//[-start-220410-Ching000042-modify]//
//[-start-220412-Ching000044-modify]//
//[-start-220422-Ching000046-modify]//
#if defined(S77013_SUPPORT) || defined(S77014_SUPPORT)
  L"@LenovoNA",
#endif
#if defined(S77014IAH_SUPPORT) || defined(S77014_SUPPORT)
  L"@XiaoXinIAH",
#endif
//[-end-220422-Ching000046-modify]//
//[-end-220412-Ching000044-modify]//
//[-end-220410-Ching000042-modify]//
#ifdef L05_SMB_BIOS_ENABLE
  L"@ThinkBook",
#endif
};
//_End_L05_GRAPHIC_UI_

VOID
SetupMenuItemSelected (
  UI_LIST_VIEW                  *This,
  UI_CONTROL                    *Child,
  UINT32                        Index
  )
{
  EFI_STATUS                    Status;
  H2O_FORM_BROWSER_SM           *SetupMenuData;
  SETUP_MENU_INFO               CurrentSetupMenuInfo;

  ASSERT (gFB->CurrentP != NULL);


  Status = gFB->GetSMInfo (gFB, &SetupMenuData);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR(Status)) {
    return ;
  }

//_Start_L05_GRAPHIC_UI_
//  ASSERT (Index < SetupMenuData->NumberOfSetupMenus);
//  if (Index >= SetupMenuData->NumberOfSetupMenus) {
  //
  // Skip top and bottom logo item
  //
  if (Index == 0 || Index >= (SetupMenuData->NumberOfSetupMenus + 1)) {
    goto Done;
  }
//_End_L05_GRAPHIC_UI_

//_Start_L05_GAMING_UI_ENABLE_
  if (FeaturePcdGet (PcdL05GamingUiSupported)) {
    //
    // Skip gaming home page
    //
    Index++;
  }
//_End_L05_GAMING_UI_ENABLE_

  Status = GetSetupMenuInfoByPage (gFB->CurrentP, &CurrentSetupMenuInfo);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    goto Done;
  }

//_Start_L05_GRAPHIC_UI_
//  if (SetupMenuData->SetupMenuInfoList[Index].PageId == CurrentSetupMenuInfo.PageId) {
  if (SetupMenuData->SetupMenuInfoList[Index - 1].PageId == CurrentSetupMenuInfo.PageId) {
//_End_L05_GRAPHIC_UI_
    goto Done;
  }

//_Start_L05_GRAPHIC_UI_
//  SendSelectPNotify (SetupMenuData->SetupMenuInfoList[Index].PageId);
  SendSelectPNotify (SetupMenuData->SetupMenuInfoList[Index - 1].PageId);
//_End_L05_GRAPHIC_UI_

Done:
  FreeSetupMenuData (SetupMenuData);
}

VOID
SetupMenuItemClick (
  UI_LIST_VIEW                  *This,
  UI_CONTROL                    *Item,
  UINT32                        Index
  )
{
  EFI_STATUS                    Status;
  H2O_FORM_BROWSER_SM           *SetupMenuData;
  SETUP_MENU_INFO               CurrentSetupMenuInfo;

  ASSERT (gFB->CurrentP != NULL);


  Status = gFB->GetSMInfo (gFB, &SetupMenuData);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR(Status)) {
    return ;
  }

//_Start_L05_GRAPHIC_UI_
//  ASSERT (Index < SetupMenuData->NumberOfSetupMenus);
//  if (Index >= SetupMenuData->NumberOfSetupMenus) {
  //
  // Skip top and bottom logo item
  //
  if (Index == 0 || Index >= (SetupMenuData->NumberOfSetupMenus + 1)) {
    goto Done;
  }
//_End_L05_GRAPHIC_UI_

//_Start_L05_GAMING_UI_ENABLE_
  if (FeaturePcdGet (PcdL05GamingUiSupported)) {
    //
    // Skip gaming home page
    //
    Index++;
  }
//_End_L05_GAMING_UI_ENABLE_

  Status = GetSetupMenuInfoByPage (gFB->CurrentP, &CurrentSetupMenuInfo);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    goto Done;
  }

//_Start_L05_GRAPHIC_UI_
//  if (SetupMenuData->SetupMenuInfoList[Index].PageId == CurrentSetupMenuInfo.PageId) {
  if (SetupMenuData->SetupMenuInfoList[Index - 1].PageId == CurrentSetupMenuInfo.PageId) {
//_End_L05_GRAPHIC_UI_
    goto Done;
  }

//_Start_L05_GRAPHIC_UI_
//  SendSelectPNotify (SetupMenuData->SetupMenuInfoList[Index].PageId);
  SendSelectPNotify (SetupMenuData->SetupMenuInfoList[Index - 1].PageId);
//_End_L05_GRAPHIC_UI_

Done:
  FreeSetupMenuData (SetupMenuData);
}

EFI_STATUS
RefreshSetupMenuList (
  H2O_SETUP_MENU_PANEL          *This
  )
{
  UINTN                         Index;
  EFI_STATUS                    Status;
  H2O_FORM_BROWSER_SM           *SetupMenuData;
  SETUP_MENU_INFO               *SetupMenuInfo;
  SETUP_MENU_INFO               CurrentSetupMenuInfo;
  UI_CONTROL                    *MenuControl;
  CHAR16                        Str[20];
//_Start_L05_GRAPHIC_UI_
  EFI_STATUS                    OemSvcStatus;
  L05_BRANDING_LOGO_TYPE        BrandingLogoType;
  BOOLEAN                       IsConsumer;
  CHAR16                        AttributeStr[100];
  UI_CONTROL                    *TopMenuControl;
  UI_CONTROL                    *BottomMenuControl;
  UI_CONTROL                    *ChildControl;
  UI_CONTROL                    *LogoControl;
  EFI_IMAGE_INPUT               *LogoImage;
//_End_L05_GRAPHIC_UI_


  if (!NeedShowSetupMenu ()) {
    UiSetAttribute (This, L"visibility", L"false");
    return EFI_SUCCESS;
//_Start_L05_GAMING_UI_ENABLE_
  } else if (IsL05GamingHomePage ()) {
    return EFI_SUCCESS;
//_End_L05_GAMING_UI_ENABLE_
  } else {
    UiSetAttribute (This, L"visibility", L"true");
  }

  //
  // remove old items
  //
  CONTROL_CLASS (This)->RemoveAllChild ((UI_CONTROL *)This);

  ((UI_LIST_VIEW *)This)->OnItemClick = SetupMenuItemClick;
  ((UI_LIST_VIEW *)This)->OnItemSelected = SetupMenuItemSelected;


  Status = gFB->GetSMInfo (gFB, &SetupMenuData);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (SetupMenuData->NumberOfSetupMenus == 0) {
    FreePool (SetupMenuData);
    return EFI_UNSUPPORTED;
  }

//_Start_L05_GRAPHIC_UI_
  //
  // Oem service for :
  // 1. Customize branding logo by BrandingLogoType
  // 2. Identify whether it is a consumer product
  //
  OemSvcStatus     = EFI_UNSUPPORTED;
  BrandingLogoType = MaxBrandingLogoType;
  IsConsumer       = TRUE;

  OemSvcStatus = OemSvcGetProductLogoType (&BrandingLogoType, &IsConsumer);

  if (OemSvcStatus == EFI_MEDIA_CHANGED && BrandingLogoType >= MaxBrandingLogoType) {
    OemSvcStatus = EFI_UNSUPPORTED;
  }

  //
  // Top child item
  //
  if (FeaturePcdGet (PcdL05GamingUiSupported)) {
    TopMenuControl = XmlCreateControl (mH2OL05GamingSetupMenuTopChilds, (UI_CONTROL *) This);
    ChildControl = UiFindChildByName (TopMenuControl, L"BackButton");
    UnicodeSPrint (
      AttributeStr,
      sizeof (AttributeStr),
      L"text='%s'",
      HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_BACK_BUTTON_TITLE), NULL)
      );
    UiApplyAttributeList (ChildControl, AttributeStr);
  } else {
    TopMenuControl = XmlCreateControl (mH2OSetupMenuTopChilds, (UI_CONTROL *) This);
    ChildControl = UiFindChildByName (TopMenuControl, L"StartMenu");
    UnicodeSPrint (
      AttributeStr,
      sizeof (AttributeStr),
      L"text='%s'",
      HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_START_MENU_TITLE), NULL)
      );
    UiApplyAttributeList (ChildControl, AttributeStr);

  }
  CONTROL_CLASS_SET_STATE (TopMenuControl, UISTATE_DISABLED, 0);
//_End_L05_GRAPHIC_UI_

  Status = GetSetupMenuInfoByPage (gFB->CurrentP, &CurrentSetupMenuInfo);
  if (EFI_ERROR (Status)) {
    ZeroMem (&CurrentSetupMenuInfo, sizeof (SETUP_MENU_INFO));
  }

  for (Index = 0; Index < SetupMenuData->NumberOfSetupMenus; Index++) {

//_Start_L05_GAMING_UI_ENABLE_
    if (FeaturePcdGet (PcdL05GamingUiSupported) && Index == 0) {
      //
      // Skip gaming home page
      //
      continue;
    }
//_End_L05_GAMING_UI_ENABLE_

    SetupMenuInfo = &SetupMenuData->SetupMenuInfoList[Index];

    MenuControl = CreateControl (L"SetupMenuItem", (UI_CONTROL*) This);
    UnicodeSPrint (Str, sizeof (Str), L"0x%p", SetupMenuInfo);
    UiSetAttribute (MenuControl, L"setupmenuinfo", Str);

    CONTROL_CLASS(This)->AddChild ((UI_CONTROL *)This, MenuControl);

    //
    // performItemClick to select
    //
    if (SetupMenuData->SetupMenuInfoList[Index].PageId == CurrentSetupMenuInfo.PageId) {
      LIST_VIEW_CLASS (This)->SetSelection (
                                (UI_LIST_VIEW *)This,
//_Start_L05_GRAPHIC_UI_
//                                (INT32)Index,
                                FeaturePcdGet (PcdL05GamingUiSupported) ? (INT32)Index : (INT32)Index + 1,
//_End_L05_GRAPHIC_UI_
                                TRUE
                                );
    }
  }

//_Start_L05_GRAPHIC_UI_
  //
  // Bottom child item
  //
  if (FeaturePcdGet (PcdL05GamingUiSupported)) {
    BottomMenuControl = XmlCreateControl (mH2OL05GamingSetupMenuBottomChilds, (UI_CONTROL *) This);
  } else {
    BottomMenuControl = XmlCreateControl (mH2OSetupMenuBottomChilds, (UI_CONTROL *) This);
  }
  CONTROL_CLASS_SET_STATE (BottomMenuControl, UISTATE_DISABLED, 0);

  LogoControl = UiFindChildByName (BottomMenuControl, L"LenovoLogo");
  if (LogoControl != NULL && OemSvcStatus == EFI_MEDIA_CHANGED) {
    //
    // Lenovo Logo of BIOS Setup will not be displayed in the following cases:
    // 1. Enable Gaming UI
    // 2. [Lenovo SMB BIOS Special Requirements V1.1]
    //      2.3 BIOS Setup Design
    //        Remove Lenovo Logo in SMB other products (not ThinkBook and ThinkPad E)
    //
#ifdef L05_SMB_BIOS_ENABLE
    if (BrandingLogoType == ThinkBook) {
#endif
    if (IsConsumer) {
      UiSetAttribute (LogoControl, L"background-image", L"@LenovoColor");
    } else {
      UiSetAttribute (LogoControl, L"background-image", L"@Lenovo");
    }
#ifdef L05_SMB_BIOS_ENABLE
    }
#endif
  }

  //
  // Branding logo
  //
  if (OemSvcStatus == EFI_MEDIA_CHANGED) {
    MenuControl = FeaturePcdGet (PcdL05GamingUiSupported) ? BottomMenuControl : TopMenuControl;
    LogoControl = UiFindChildByName (MenuControl, L"BrandingLogo");
    LogoImage   = GetImageByString (mBrandingLogoImageStr[BrandingLogoType]);
    UnicodeSPrint (
      AttributeStr,
      sizeof (AttributeStr),
      L"height='%d' width='%d' background-image='%s'",
      LogoImage->Height,
      LogoImage->Width,
      mBrandingLogoImageStr[BrandingLogoType]
      );
    UiApplyAttributeList (LogoControl, AttributeStr);
  }
//_End_L05_GRAPHIC_UI_

  FreeSetupMenuData (SetupMenuData);
  return EFI_SUCCESS;
}

LRESULT
EFIAPI
H2OSetupMenuPanelProc (
  HWND   Wnd,
  UINT32 Msg,
  WPARAM WParam,
  LPARAM LParam
  )
{
  H2O_SETUP_MENU_PANEL          *This;
  UI_CONTROL                    *Control;


  This = (H2O_SETUP_MENU_PANEL *) GetWindowLongPtr (Wnd, 0);
  if (This == NULL && Msg != WM_CREATE && Msg != WM_NCCALCSIZE) {
    ASSERT (FALSE);
    return 0;
  }
  Control = (UI_CONTROL *)This;

  switch (Msg) {

  case WM_CREATE:
    This = (H2O_SETUP_MENU_PANEL *) AllocateZeroPool (sizeof (H2O_SETUP_MENU_PANEL));
    if (This != NULL) {
      CONTROL_CLASS (This) = (UI_CONTROL_CLASS *) GetClassLongPtr (Wnd, 0);
      SetWindowLongPtr (Wnd, 0, (INTN)This);
      SendMessage (Wnd, UI_NOTIFY_CREATE, WParam, LParam);
    }
    break;

  case UI_NOTIFY_CREATE:
    PARENT_CLASS_WNDPROC (CURRENT_CLASS, Wnd, UI_NOTIFY_CREATE, WParam, LParam);
//_Start_L05_GRAPHIC_UI_
    UiSetAttribute (This, L"background-color", L"0x0");
//_End_L05_GRAPHIC_UI_
    break;

  case FB_NOTIFY_REPAINT:
    RefreshSetupMenuList (This);
    break;

  case WM_SETFOCUS:
    gLastFocus = Wnd;
    PARENT_CLASS_WNDPROC (CURRENT_CLASS, Wnd, Msg, WParam, LParam);
    break;

  default:
    return PARENT_CLASS_WNDPROC (CURRENT_CLASS, Wnd, Msg, WParam, LParam);

  }
  return 0;

}


H2O_SETUP_MENU_PANEL_CLASS *
EFIAPI
GetH2OSetupMenuPanelClass (
  VOID
  )
{
  if (CURRENT_CLASS != NULL) {
    return CURRENT_CLASS;
  }

  InitUiClass ((UI_CONTROL_CLASS **)&CURRENT_CLASS, sizeof (*CURRENT_CLASS), L"H2OSetupMenuPanel", (UI_CONTROL_CLASS *) GetListViewClass());
  if (CURRENT_CLASS == NULL) {
    return NULL;
  }
  ((UI_CONTROL_CLASS *)CURRENT_CLASS)->WndProc = H2OSetupMenuPanelProc;

  return CURRENT_CLASS;
}

