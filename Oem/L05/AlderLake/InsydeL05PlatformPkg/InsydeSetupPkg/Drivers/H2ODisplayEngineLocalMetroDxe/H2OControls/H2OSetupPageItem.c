/** @file
  UI Common Controls

;******************************************************************************
;* Copyright (c) 2014 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2OControls.h"
#include "H2ODisplayEngineLocalMetro.h"
#include "MetroUi.h"
//_Start_L05_GRAPHIC_UI_
#include <L05SetupConfig.h>
//_End_L05_GRAPHIC_UI_

STATIC H2O_SETUP_PAGE_ITEM_CLASS    *mSetupPageItemClass = NULL;
#define CURRENT_CLASS              mSetupPageItemClass

//_Start_L05_GRAPHIC_UI_
//#if FixedPcdGet32(PcdH2OLmdeMultiLayout) == 0
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
//CHAR16 *mSubtitleChilds = L""
//  L"<VerticalLayout height='35' width='match_parent'>"
//    L"<Label name='OptionPrompt' text-align='singleline' padding='0,0,0,20' font-size='20' textcolor='0xFF4D4D4D' text-overflow='ellipsis'/>"
//    L"<Control height='1' width='match_parent' background-image='@OptionBkg' background-image-style='stretch'/>"
//  L"</VerticalLayout>";
CHAR16 *mSubtitleChilds = L""
  L"<VerticalLayout height='35' width='match_parent'>"
    L"<Label name='OptionPrompt' background-color='0xFFFFFFFF' text-align='singleline' padding='0,0,0,20' font-size='24' textcolor='0xFF0F75BC' text-overflow='ellipsis'/>"
  L"</VerticalLayout>";
//_End_L05_GRAPHIC_UI_

CHAR16 *mH2OCheckBoxOpChilds = L""
  L"<Texture float='true' background-image='@OptionBkg' name='OptionBkg' background-image-style='stretch'/>"
  L"<HorizontalLayout height='wrap_content'>"
    L"<Control width='60' name='OptionImagePadding'>"
      L"<Texture name='OptionImage' background-image-style='stretch'/>"
    L"</Control>"
    L"<Label text-align='singleline' textcolor='0xFF666666' font-size='21' name='OptionPrompt' text-overflow='ellipsis'/>"
    L"<HorizontalLayout padding='17,0,17,0' width='120' height='70'>"
      L"<Switch switchcolor='@menucolor' name='CheckBox'/>"
    L"</HorizontalLayout>"
    L"<Label width='30' />"
  L"</HorizontalLayout>";

//_Start_L05_GRAPHIC_UI_
//CHAR16 *mStatementChilds = L""
//  L"<Texture float='true' background-image='@OptionBkg' name='OptionBkg' background-image-style='stretch'/>"
//  L"<HorizontalLayout>"
//    L"<Control width='60' name='OptionImagePadding'>"
//      L"<Texture name='OptionImage' background-image-style='stretch'/>"
//    L"</Control>"
//    L"<HorizontalLayout name='OptionLayout'>"
//      L"<Label text-align='singleline|left' textcolor='0xFF666666' font-size='21' name='OptionPrompt' text-overflow='ellipsis'/>"
//      L"<Label text-align='singleline' name='OptionSparator' width='10'/>"
//      L"<Label text-align='singleline|right' width='160' textcolor='0xFF666666' font-size='21' name='OptionValue' text-overflow='ellipsis'/>"
//      L"<Label name='OptionEnd' text='>' text-align='singleline|right' text-align='center' width='20' font-size='21' textcolor='0xFF666666'/>"
//      L"<Label text-align='singleline' name='EndSparator' width='20'/>"
//    L"</HorizontalLayout>"
//  L"</HorizontalLayout>";
CHAR16 *mStatementChilds = L""
  L"<HorizontalLayout width='match_parent' min-height='24' height='wrap_content'>"
    L"<VerticalLayout height='wrap_content' width='777' padding='0,50,0,50' min-height='24' name='ItemContainer'>"
      L"<VerticalLayout height='wrap_content' background-color='0x0' name='item-board' padding='0,20,0,20'>"
        L"<Label text-align='left' textcolor='0xFF0F75BC' font-size='24' height='wrap_content' name='OptionPrompt' width='match_parent'/>"
        L"<Control height='20' name='help-separator'/>"
        L"<Label text-align='left' name='help' padding='0,0,8,0' textcolor='0xFF515151' min-height='35'  height='wrap_content'  width='match_parent'  font-size='24' text='1' />"
      L"</VerticalLayout>"
      L"<Control height='40' name='item-separator'/>"
    L"</VerticalLayout>"
    L"<Control name='Option-separator' width='65'/>"
    L"<VerticalLayout width='459' height='44' padding='2,2,2,2' background-color='0xFF000000' name='OptionContainer' >"
      L"<HorizontalLayout background-color='0xFFFFFFFF' >"
        L"<Label text-align='singleline|left' textcolor='0xFF000000' font-size='24' padding='0,0,0,8' name='OptionValue' text-overflow='ellipsis'/>"
        L"<Texture width='40' height='40' background-image='@L05DropdownButton' />"
      L"</HorizontalLayout>"
    L"</VerticalLayout>"
  L"</HorizontalLayout>";
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
//CHAR16 *mH2OTextOpChilds = L""
//  L"<HorizontalLayout child-padding='2' width='match_parent' min-height='35' height='wrap_content'>"
//    L"<HorizontalLayout child-padding='2' float='true' width='match_parent' height='match_parent'>"
//      L"<Texture scale9grid='1,1,1,1' background-image='@SetupMenuTextOpBkg' name='OptionPromptBackground' width='258' height='match_parent'/>"
//      L"<Texture scale9grid='1,1,1,1' background-image='@SetupMenuTextOpBkg' name='OptionValueBackground' height='match_parent'/>"
//    L"</HorizontalLayout>"
//    L"<Label name='OptionPrompt' min-height='35' height='wrap_content' padding='7,0,7,60' width='258' font-size='20' textcolor='0xFF4D4D4D'/>"
//    L"<Label name='OptionValue' text-align='center' min-height='35' height='wrap_content' padding='7,0,7,0' width='258' font-size='20' textcolor='@menucolor'/>"
//    L"<Texture float='true' background-color='@menucolor' pos='30,11,38,19'/>"
//  L"</HorizontalLayout>";
CHAR16 *mH2OTextOpChilds = L""
  L"<HorizontalLayout width='match_parent' min-height='28' height='wrap_content'>"
    L"<VerticalLayout name='ItemContainer' height='wrap_content' width='777' padding='0,50,0,50' min-height='28'>"
      L"<VerticalLayout height='wrap_content' background-color='0x0' name='item-board' padding='0,20,0,20'>"
        L"<Label name='OptionPrompt' text-align='left' height='wrap_content' width='match_parent' font-size='24' textcolor='0xFF0F75BC'/>"
        L"<Label text-align='left' name='help' visibility='false' textcolor='0xFF515151' min-height='24'  height='wrap_content'  width='match_parent'  font-size='24' text='1' />"
      L"</VerticalLayout>"
      L"<Control height='20' name='item-separator'/>"
    L"</VerticalLayout>"
    L"<Control name='Option-separator' width='65'/>"
    L"<Label name='OptionValue'  min-height='24' height='wrap_content' font-size='24' textcolor='0xFF000000'/>"
  L"</HorizontalLayout>";
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
CHAR16 *mDateStatementChilds = L""
  L"<VerticalLayout height='wrap_content' width='777' padding='0,50,0,50' min-height='132'>"
    L"<VerticalLayout height='wrap_content' background-color='0x0' name='item-board' padding='0,20,0,20'>"
      L"<Label text-align='singleline|left' textcolor='0xFF0F75BC' font-size='24' height='24' name='OptionPrompt' width='match_parent' text-overflow='ellipsis'/>"
      L"<Control height='20'/>"
      L"<HorizontalLayout width='wrap_content' height='52'>"
        L"<Label width='110' text-align='singleline|center' textcolor='0xFF000000' background-color='0xFFF3F3F3' font-size='36' padding='8,0,8,0' name='Month' text-overflow='ellipsis' text='Month'/>"
        L"<Control width='6'/>"
        L"<Label width='56' text-align='singleline|center' textcolor='0xFF000000' background-color='0xFFF3F3F3' font-size='36' padding='8,0,8,0' name='Day' text-overflow='ellipsis' text='12'/>"
        L"<Control width='6'/>"
        L"<VerticalLayout height='match_parent' width='80'>"
          L"<Label height='24' text-align='singleline|center' textcolor='0xFF000000' background-color='0xFFF3F3F3' font-size='20' padding='2,0,2,0' name='Weekday' text-overflow='ellipsis' text='mon'/>"
          L"<Control height='4'/>"
          L"<Label height='24' text-align='singleline|center' textcolor='0xFF000000' background-color='0xFFF3F3F3' font-size='20' padding='2,0,2,0' name='Year' text-overflow='ellipsis' text='1900'/>"
        L"</VerticalLayout>"
      L"</HorizontalLayout>"
      L"<Control height='20'/>"
      L"<Label text-align='left' name='help' textcolor='0xFF515151' min-height='35'  height='wrap_content'  width='match_parent'  font-size='24' text='1' />"
    L"</VerticalLayout>"
    L"<Control height='40'/>"
  L"</VerticalLayout>";

CHAR16 *mTimeStatementChilds = L""
  L"<VerticalLayout height='wrap_content' width='777' padding='0,50,0,50' min-height='132'>"
    L"<VerticalLayout height='wrap_content' background-color='0x0' name='item-board' padding='0,20,0,20'>"
      L"<Label text-align='singleline|left' textcolor='0xFF0F75BC' font-size='24' height='24' name='OptionPrompt' width='match_parent' text-overflow='ellipsis'/>"
      L"<Control height='20'/>"
      L"<HorizontalLayout width='wrap_content' height='52' padding='0,0,0,14'>"
        L"<Label width='55' text-align='singleline|center' textcolor='0xFF000000' background-color='0xFFF3F3F3' font-size='36' padding='8,0,8,0' name='Hour' text-overflow='ellipsis' text='00'/>"
        L"<Label width='24' text-align='singleline|center' textcolor='0xFF000000' font-size='36' padding='8,0,8,0' text=':'/>"
        L"<Label width='55' text-align='singleline|center' textcolor='0xFF000000' background-color='0xFFF3F3F3' font-size='36' padding='8,0,8,0' name='Minute' text-overflow='ellipsis' text='00'/>"
        L"<Control width='4'/>"
        L"<Label width='wrap_content' text-align='bottom' textcolor='0xFF000000' font-size='20' padding='24,0,8,0' name='Second' text='00'/>"
      L"</HorizontalLayout>"
      L"<Control height='4'/>"
      L"<HorizontalLayout width='wrap_content' height='18' padding='0,0,0,14'>"
        L"<Label width='55' text-align='singleline|center' textcolor='0xFF000000' font-size='18' name='Hrs' text-overflow='ellipsis' text='HRS'/>"
        L"<Control width='24'/>"
        L"<Label width='55' text-align='singleline|center' textcolor='0xFF000000' font-size='18' name='Mins' text-overflow='ellipsis' text='MINS'/>"
      L"</HorizontalLayout>"
      L"<Control height='20'/>"
      L"<Label text-align='left' name='help' textcolor='0xFF515151' min-height='35'  height='wrap_content'  width='match_parent'  font-size='24' text='1' />"
    L"</VerticalLayout>"
    L"<Control height='40'/>"
  L"</VerticalLayout>";
//_End_L05_GRAPHIC_UI_

INT32
GetStatementHeight (
  IN VOID                                 *Statement
  )
{
  H2O_FORM_BROWSER_S                      *St;

  if (Statement == NULL) {
    return 35;
  }

  St = (H2O_FORM_BROWSER_S *)Statement;
  if (St->Operand == EFI_IFR_SUBTITLE_OP ||
    St->Operand == EFI_IFR_TEXT_OP) {
    return 35;
  }

  return 70;
}

//_Start_L05_GRAPHIC_UI_
//#endif
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
CHAR16 *mL05MonthStr[12]  = {0};
CHAR16 *mL05WeekdayStr[7] = {0};
//_End_L05_GRAPHIC_UI_

#define  OPTION_FONT_SIZE         21
#define  OPTION_IMAGE_SIZE        25

#define  OPTION_LEFT_PADDING      60
#define  OPTION_MIN_PROMPT_WIDTH  200
#define  OPTION_VALUE_WIDTH       105
#define  OPTION_RIGHT_PADDING     30

//_Start_L05_GRAPHIC_UI_
CHAR16 *
GetWeekdayStrByHiiDate (
  IN EFI_HII_DATE                         Date
  )
{
  INTN                                    Adjustment;
  INTN                                    Month;
  INTN                                    Year;
  INTN                                    Weekday;

  Adjustment = (14 - Date.Month) / 12;
  Month      = Date.Month + 12 * Adjustment - 2;
  Year       = Date.Year - Adjustment;

  mL05WeekdayStr[0] = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_WEEKDAY_SUN_STRING), NULL);
  mL05WeekdayStr[1] = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_WEEKDAY_MON_STRING), NULL);
  mL05WeekdayStr[2] = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_WEEKDAY_TUE_STRING), NULL);
  mL05WeekdayStr[3] = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_WEEKDAY_WED_STRING), NULL);
  mL05WeekdayStr[4] = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_WEEKDAY_THU_STRING), NULL);
  mL05WeekdayStr[5] = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_WEEKDAY_FRI_STRING), NULL);
  mL05WeekdayStr[6] = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_WEEKDAY_SAT_STRING), NULL);

  Weekday = (Date.Day + (13 * Month - 1) / 5 + Year + Year / 4 - Year / 100 + Year / 400) % 7;

  return mL05WeekdayStr[Weekday];
}
//_End_L05_GRAPHIC_UI_

EFI_STATUS
MarqueeByName (
  IN UI_CONTROL             *Control,
  IN CHAR16                 *ChildName,
  IN CHAR16                 *Value
  )
{
  UI_CONTROL                *Child;

  Child = UiFindChildByName (Control, ChildName);
  if (Child == NULL) {
    return EFI_NOT_FOUND;
  }

  UiSetAttribute (Child, L"marquee", Value);
  return EFI_SUCCESS;
}

VOID
UpdateSetupPageItem (
  UI_CONTROL                    *Control
  )
{
  H2O_SETUP_PAGE_ITEM           *This;
  H2O_FORM_BROWSER_S            *Statement;
//_Start_L05_GRAPHIC_UI_
//  CHAR16                        Str[20];
  CHAR16                        Str[60];
//_End_L05_GRAPHIC_UI_
  UI_CONTROL                    *Child;
  UINT32                        Index;
  INTN                          Result;
  EFI_STATUS                    Status;
  INT32                         Height;
  UI_CONTROL                    *CheckBox;
  UI_LABEL                      *Label;
  CHAR16                        ValueTextColor[20];
//_Start_L05_GRAPHIC_UI_
  EFI_GUID                      InformationFormSetGuid = FORMSET_ID_GUID_INFORMATION;
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
  Height = 70;
//_End_L05_GRAPHIC_UI_

  This = (H2O_SETUP_PAGE_ITEM *) Control;
  if (This->Statement == NULL) {
    return ;
  }

  Statement = This->Statement;
  ASSERT (Statement != NULL);
  if (Statement == NULL) {
    return;
  }

//_Start_L05_GRAPHIC_UI_
  //
  // Adjust item for Information page
  //
  if (CompareGuid(&This->Statement->FormsetGuid, &InformationFormSetGuid)) {
    Child = UiFindChildByName (Control, L"ItemContainer");
    if (Child != NULL) {
      UiSetAttribute (Child, L"width", L"495");
    }
    Child = UiFindChildByName (Control, L"Option-separator");
    if (Child != NULL) {
      UiSetAttribute (Child, L"width", L"135");
    }
  }
//_End_L05_GRAPHIC_UI_

  //
  // Get Value Text color
  //
  Label = (UI_LABEL *)UiFindChildByName (This, L"ValueText");
  UnicodeSPrint (ValueTextColor, sizeof (ValueTextColor), L"%s", L"@menucolor");
  if (Label != NULL) {
    if (Label->TextColor == 0) {
      UnicodeSPrint (ValueTextColor, sizeof (ValueTextColor), L"%s", L"@menulightcolor");
    } else {
      UnicodeSPrint (ValueTextColor, sizeof (ValueTextColor), L"0x%08x", Label->TextColor);
    }
  }

//_Start_L05_GRAPHIC_UI_
  mL05MonthStr[0]  = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_JAN_STRING), NULL);
  mL05MonthStr[1]  = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_FEB_STRING), NULL);
  mL05MonthStr[2]  = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_MAR_STRING), NULL);
  mL05MonthStr[3]  = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_APR_STRING), NULL);
  mL05MonthStr[4]  = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_MAY_STRING), NULL);
  mL05MonthStr[5]  = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_JUN_STRING), NULL);
  mL05MonthStr[6]  = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_JUL_STRING), NULL);
  mL05MonthStr[7]  = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_AUG_STRING), NULL);
  mL05MonthStr[8]  = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_SEP_STRING), NULL);
  mL05MonthStr[9]  = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_OCT_STRING), NULL);
  mL05MonthStr[10] = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_NOV_STRING), NULL);
  mL05MonthStr[11] = HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MONTH_DEC_STRING), NULL);
//_End_L05_GRAPHIC_UI_

  //
  // Add Statement image if need
  //
  Child = UiFindChildByName (This, L"OptionImage");
  if (Child != NULL && Statement->Image != NULL) {
//_Start_L05_GRAPHIC_UI_
//    Height = GetStatementHeight (Statement);
//_End_L05_GRAPHIC_UI_
    UnicodeSPrint (Str, sizeof (Str), L"0x%p", Statement->Image);
    UiSetAttribute (Child, L"background-image", Str);
    UnicodeSPrint (
      Str,
      sizeof (Str),
      L"%d,%d,%d,%d",
      (Height - OPTION_IMAGE_SIZE) / 2 + (Height - OPTION_IMAGE_SIZE) % 2,
      17,
      (Height - OPTION_IMAGE_SIZE) / 2,
      18
      );
    Child = UiFindChildByName (This, L"OptionImagePadding");
//_Start_L05_GRAPHIC_UI_
//    UiSetAttribute (Child, L"padding", Str);
    if (Child != NULL) {
      UiSetAttribute (Child, L"padding", Str);
    }
//_End_L05_GRAPHIC_UI_
  }

//_Start_L05_GRAPHIC_UI_
  if (Statement->Help != NULL) {
    Child = UiFindChildByName (This, L"help");
    if (Child != NULL) {
      UiSetAttribute (Child, L"text", Statement->Help);
    }
  }
//_End_L05_GRAPHIC_UI_

  Child = UiFindChildByName (This, L"RefImage");
  if (Child != NULL && Statement->Operand == EFI_IFR_REF_OP) {
    UiSetAttribute (Child, L"visibility", L"true");
  }

  //
  // Adjust text and text color
  //
  if (Statement->Prompt != NULL) {
    Child = UiFindChildByName (This, L"OptionPrompt");
    UiSetAttribute (Child, L"text", Statement->Prompt);
  }

  if (Statement->Operand == EFI_IFR_CHECKBOX_OP) {
    CheckBox = UiFindChildByName (This, L"CheckBox");

    if (Statement->HiiValue.Value.b) {
      UiSetAttribute (CheckBox, L"checkboxvalue", L"true");
    } else {
      UiSetAttribute (CheckBox, L"checkboxvalue", L"false");
    }

    if (Statement->Selectable) {
      UiSetAttribute (CheckBox, L"disabled", L"false");
      if (Statement->HiiValue.Value.b) {
        UiSetAttribute (CheckBox, L"switchcolor", L"@menucolor");
        UiSetAttribute (CheckBox, L"switch-textcolor", L"0xFFFFFFFF");
      } else {
        UiSetAttribute (CheckBox, L"switchcolor", L"gray");
        UiSetAttribute (CheckBox, L"switch-textcolor", L"@menucolor");
        if (PcdGet32(PcdH2OLmdeMultiLayout) == 1) {
          UiSetAttribute (CheckBox, L"switch-textcolor", L"0xFF008000");
        }
      }
    } else {
      UiSetAttribute (CheckBox, L"switchcolor", L"gray");
      UiSetAttribute (CheckBox, L"switch-textcolor", L"0xFF666666");
      if (PcdGet32(PcdH2OLmdeMultiLayout) == 1) {
        UiSetAttribute (CheckBox, L"switch-textcolor", L"0xFF333333");
      }
      UiSetAttribute (CheckBox, L"disabled", L"true");
    }

  } else if (Statement->Operand == EFI_IFR_TEXT_OP
#ifdef L05_GAMING_UI_ENABLE
             || This->Statement->QuestionId == L05_KEY_SYSTEM_BIOS_VERSION
             || This->Statement->QuestionId == L05_KEY_SYSTEMSERIAL_NUMBER
             || This->Statement->QuestionId == L05_KEY_CPU_INFO
#endif
            ) {
    if (Statement->TextTwo != NULL && Statement->TextTwo[0] != '\0') {
      Child = UiFindChildByName (This, L"OptionValue");
      UiSetAttribute (Child, L"text", Statement->TextTwo);
//_Start_L05_GRAPHIC_UI_
      if (Statement->GrayedOut) {
        UiSetAttribute (Child, L"textcolor", L"0xFF515151");
      }
//_End_L05_GRAPHIC_UI_
    } else {
//_Start_L05_GRAPHIC_UI_
//      Child = UiFindChildByName (This, L"OptionPrompt");
//      UiSetAttribute (Child, L"width", L"0");
//      UiSetAttribute (Child, L"height", L"0");
//      UiSetAttribute (Child, L"text-align", L"singleline|left");
//      UiSetAttribute (Child, L"text-overflow", L"ellipsis");
//_End_L05_GRAPHIC_UI_

      Child = UiFindChildByName (This, L"OptionValue");
      UiSetAttribute (Child, L"visibility", L"false");
//_Start_L05_GRAPHIC_UI_
//      Child = UiFindChildByName (This, L"OptionPromptBackground");
//      UiSetAttribute (Child, L"width", L"0");
//
//      Child = UiFindChildByName (This, L"OptionValueBackground");
//      UiSetAttribute (Child, L"visibility", L"false");
      Child = UiFindChildByName (This, L"item-separator");
      UiSetAttribute (Child, L"visibility", L"false");
      if (Statement->Help != NULL) {
        Child = UiFindChildByName (This, L"help");
        if (Child != NULL && (StrCmp (Statement->Help, L" ") != 0) && (StrCmp (Statement->Help, L"\0") != 0)) {
          UiSetAttribute (Child, L"visibility", L"true");
        }
      }
//_End_L05_GRAPHIC_UI_
    }
//_Start_L05_GRAPHIC_UI_
    if (Statement->GrayedOut) {
      Child = UiFindChildByName (This, L"OptionPrompt");
      UiSetAttribute (Child, L"textcolor", L"0xFF515151");
    }
//_End_L05_GRAPHIC_UI_
  } else if (Statement->NumberOfOptions != 0) {
    for (Index = 0; Index < Statement->NumberOfOptions; Index++) {
      Status = CompareHiiValue (&Statement->Options[Index].HiiValue, &Statement->HiiValue, &Result);
      if (!EFI_ERROR (Status) && Result == 0) {
        Child = UiFindChildByName (This, L"OptionValue");
        UiSetAttribute (Child, L"text", Statement->Options[Index].Text);
        if (Statement->Selectable) {
//_Start_L05_GRAPHIC_UI_
//          UiSetAttribute (Child, L"textcolor", ValueTextColor);
//          Child = UiFindChildByName (This, L"OptionEnd");
//          UiSetAttribute (Child, L"textcolor", ValueTextColor);
          UiSetAttribute (Child, L"textcolor", L"0xFF000000");
//_End_L05_GRAPHIC_UI_
        }
        break;
      }
    }
//_Start_L05_GRAPHIC_UI_
    if (!Statement->Selectable) {
      Child = UiFindChildByName (This, L"OptionPrompt");
      UiSetAttribute (Child, L"textcolor", L"0xFF515151");
      Child = UiFindChildByName (This, L"OptionValue");
      UiSetAttribute (Child, L"textcolor", L"0xFF515151");
    }
//_End_L05_GRAPHIC_UI_
  } else if (Statement->Operand == EFI_IFR_NUMERIC_OP) {
    Child = UiFindChildByName (This, L"OptionValue");
    if ((Statement->Flags & EFI_IFR_DISPLAY) == EFI_IFR_DISPLAY_UINT_HEX) {
      UnicodeSPrint (Str, sizeof (Str), L"0x%lX", Statement->HiiValue.Value.u64);
    } else {
      UnicodeSPrint (Str, sizeof (Str), L"%ld", Statement->HiiValue.Value.u64);
    }
    UiSetAttribute (Child, L"text", Str);

//_Start_L05_GRAPHIC_UI_
//    if (Statement->Selectable) {
//      UiSetAttribute (Child, L"textcolor", ValueTextColor);
//      Child = UiFindChildByName (This, L"OptionEnd");
//      UiSetAttribute (Child, L"textcolor", ValueTextColor);
//    }
//_End_L05_GRAPHIC_UI_
  } else if (Statement->Operand == EFI_IFR_ACTION_OP) {
    Child = UiFindChildByName (This, L"OptionValue");
    if (Child != NULL) {
      if (Statement->TextTwo != NULL && Statement->TextTwo[0] != '\0') {
        UiSetAttribute (Child, L"text", Statement->TextTwo);
//_Start_L05_GRAPHIC_UI_
//        if (Statement->Selectable) {
//          UiSetAttribute (Child, L"textcolor", ValueTextColor);
//        } else {
//          UiSetAttribute (Child, L"textcolor", L"0xFF666666");
//        }
        if (!Statement->Selectable) {
          UiSetAttribute (Child, L"textcolor", L"0xFF515151");
        }
        if (StrCmp (Statement->TextTwo, L" ") == 0) {
          Child = UiFindChildByName (This, L"OptionContainer");
          if (Child != NULL) {
            UiSetAttribute (Child, L"visibility", L"false");
          }
        }
//_End_L05_GRAPHIC_UI_
//_Start_L05_GRAPHIC_UI_
      } else if (Statement->QuestionId == L05_KEY_SET_MASTER_USER_HDD_SECURITY ||
                 (Statement->QuestionId >= L05_KEY_SET_HDD_PASSWORD_OPTION && Statement->QuestionId <= (L05_KEY_SET_HDD_PASSWORD_OPTION_END)) ||
                 (Statement->QuestionId >= L05_KEY_SECURITY_ERASE_HDD_DATA && Statement->QuestionId <= (L05_KEY_SECURITY_ERASE_HDD_DATA_END))) {
        UiSetAttribute (Child, L"visibility", L"false");

        Child = UiFindChildByName (This, L"OptionContainer");
        if (Child != NULL) {
          UiSetAttribute (Child, L"visibility", L"false");
        }
//_End_L05_GRAPHIC_UI_
      } else {
        UiSetAttribute (Child, L"visibility", L"false");
//_Start_L05_GRAPHIC_UI_
        Child = UiFindChildByName (This, L"OptionContainer");
        if (Child != NULL) {
          UiSetAttribute (Child, L"visibility", L"false");
        }
        Child = UiFindChildByName (This, L"help");
        if (Child != NULL) {
          UiSetAttribute (Child, L"visibility", L"false");
        }
        Child = UiFindChildByName (This, L"item-separator");
        if (Child != NULL) {
          UiSetAttribute (Child, L"visibility", L"false");
        }
        Child = UiFindChildByName (This, L"help-separator");
        if (Child != NULL) {
          UiSetAttribute (Child, L"visibility", L"false");
        }
        Child = UiFindChildByName (This, L"OptionPrompt");
        UiSetAttribute (Child, L"textcolor", L"0xFF000000");

        if (IsBootManagerPage ()) {
          UiApplyAttributeList (Child, L"text-align='singleline' text-overflow='ellipsis' marquee='false' padding='6,0,6,0'"); // OptionPrompt

          Child = UiFindChildByName (This, L"item-board");
          UiSetAttribute (Child, L"padding", L"0,0,0,3");
          Child = UiFindChildByName (This, L"ItemContainer");
          UiApplyAttributeList (Child, L"width='762' padding='0,0,0,0'");
        } else if (IsNovoMenuPage ()) {
          UiApplyAttributeList (Child, L"height='36' padding='6,0,6,30'"); // OptionPrompt

          Child = UiFindChildByName (This, L"item-board");
          UiSetAttribute (Child, L"padding", L"0,0,0,0");
          Child = UiFindChildByName (This, L"ItemContainer");
          UiApplyAttributeList (Child, L"width='466' padding='0,0,0,0'");
        }
//_End_L05_GRAPHIC_UI_
      }
//_Start_L05_GRAPHIC_UI_
//      Child = UiFindChildByName (This, L"OptionEnd");
//      UiSetAttribute (Child, L"visibility", L"false");
//_End_L05_GRAPHIC_UI_
    }
//_Start_L05_GRAPHIC_UI_
    Child = UiFindChildByName (This, L"OptionPrompt");
    if (!Statement->Selectable) {
      UiSetAttribute (Child, L"textcolor", L"0xFF515151");
    }
//_End_L05_GRAPHIC_UI_
  } else if (Statement->Operand == EFI_IFR_STRING_OP) {
    Child = UiFindChildByName (This, L"OptionValue");
    if (Child != NULL && Statement->HiiValue.Buffer != NULL) {
      UiSetAttribute (Child, L"text", (CHAR16 *)Statement->HiiValue.Buffer);
    }

    if (Statement->Selectable) {
      if (Child != NULL) {
        UiSetAttribute (Child, L"textcolor", ValueTextColor);
      }
//_Start_L05_GRAPHIC_UI_
//      Child = UiFindChildByName (This, L"OptionEnd");
//      UiSetAttribute (Child, L"textcolor", ValueTextColor);
//_End_L05_GRAPHIC_UI_
    }
//_Start_L05_GRAPHIC_UI_
  } else if (Statement->Operand == EFI_IFR_TIME_OP) {
    Child = UiFindChildByName (This, L"Hour");
    if (Child != NULL) {
      UnicodeSPrint (Str, sizeof (Str), L"%02d", Statement->HiiValue.Value.time.Hour);
      UiSetAttribute (Child, L"text", Str);
    }
    Child = UiFindChildByName (This, L"Minute");
    if (Child != NULL) {
      UnicodeSPrint (Str, sizeof (Str), L"%02d", Statement->HiiValue.Value.time.Minute);
      UiSetAttribute (Child, L"text", Str);
    }
    Child = UiFindChildByName (This, L"Second");
    if (Child != NULL) {
      UnicodeSPrint (Str, sizeof (Str), L"%02d", Statement->HiiValue.Value.time.Second);
      UiSetAttribute (Child, L"text", Str);
    }
  } else if (Statement->Operand == EFI_IFR_DATE_OP) {
    Child = UiFindChildByName (This, L"Weekday");
    if (Child != NULL) {
      UnicodeSPrint (
        Str,
        sizeof (Str),
        L"%s",
        GetWeekdayStrByHiiDate (Statement->HiiValue.Value.date)
        );
      UiSetAttribute (Child, L"text", Str);
    }
    Child = UiFindChildByName (This, L"Month");
    if (Child != NULL) {
      UnicodeSPrint (
        Str,
        sizeof (Str),
        L"%s",
        mL05MonthStr[Statement->HiiValue.Value.date.Month - 1]
        );
      UiSetAttribute (Child, L"text", Str);
    }
    Child = UiFindChildByName (This, L"Day");
    if (Child != NULL) {
      UnicodeSPrint (
        Str,
        sizeof (Str),
        L"%02d",
        Statement->HiiValue.Value.date.Day
        );
      UiSetAttribute (Child, L"text", Str);
    }
    Child = UiFindChildByName (This, L"Year");
    if (Child != NULL) {
      UnicodeSPrint (
        Str,
        sizeof (Str),
        L"%04d",
        Statement->HiiValue.Value.date.Year
        );
      UiSetAttribute (Child, L"text", Str);
    }
  } else if (Statement->Operand == EFI_IFR_PASSWORD_OP) {
    Child = UiFindChildByName (This, L"OptionValue");
    if (Child != NULL) {
      UiSetAttribute (Child, L"visibility", L"false");

      Child = UiFindChildByName (This, L"OptionContainer");
      if (Child != NULL) {
        UiSetAttribute (Child, L"visibility", L"false");
      }
    }
  } else if (Statement->Operand == EFI_IFR_REF_OP) {
    if (StrCmp (Statement->Help, L" ") == 0) {
      Child = UiFindChildByName (This, L"help");
      if (Child != NULL) {
        UiSetAttribute (Child, L"visibility", L"false");
      }
      Child = UiFindChildByName (This, L"help-separator");
      if (Child != NULL) {
        UiSetAttribute (Child, L"visibility", L"false");
      }
      Child = UiFindChildByName (This, L"OptionContainer");
      if (Child != NULL) {
        UiSetAttribute (Child, L"visibility", L"false");
      }
    }

    Child = UiFindChildByName (This, L"OptionPrompt");
    UnicodeSPrint (Str, sizeof (Str), L"+%s", Statement->Prompt);
    UiSetAttribute (Child, L"text", Str);

#ifdef L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE
    if (Statement->TextTwo == NULL) {
      Child = UiFindChildByName (This, L"OptionContainer");
      if (Child != NULL) {
        UiSetAttribute (Child, L"visibility", L"false");
      }
    }
#endif
//_End_L05_GRAPHIC_UI_
  } else {
    Child = UiFindChildByName (This, L"OptionValue");
    if (Child != NULL) {
      UiSetAttribute (Child, L"visibility", L"false");
//_Start_L05_GRAPHIC_UI_
//      Child = UiFindChildByName (This, L"OptionEnd");
//      UiSetAttribute (Child, L"visibility", L"false");
      Child = UiFindChildByName (This, L"OptionContainer");
      if (Child != NULL) {
        UiSetAttribute (Child, L"visibility", L"false");
      }
//_End_L05_GRAPHIC_UI_
    }
//_Start_L05_GRAPHIC_UI_
    Child = UiFindChildByName (This, L"help");
    if (Child != NULL) {
      UiSetAttribute (Child, L"visibility", L"false");
    }
//_End_L05_GRAPHIC_UI_
  }
}

BOOLEAN
EFIAPI
H2OSetupPageItemSetAttribute (
  UI_CONTROL             *Control,
  CHAR16                 *Name,
  CHAR16                 *Value
  )
{
  H2O_SETUP_PAGE_ITEM    *This;
  EFI_STATUS             Status;
  UI_CONTROL             *Switch;
//_Start_L05_GRAPHIC_UI_
  CHAR16                 *Ch;
  UI_CONTROL             *ChildControl;
  CHAR16                 AttributeStr[100];
//_End_L05_GRAPHIC_UI_

  This = (H2O_SETUP_PAGE_ITEM *) Control;

  if (StrCmp (Name, L"statement") == 0) {
    CONTROL_CLASS (This)->RemoveAllChild (Control);

    This->Statement = (H2O_FORM_BROWSER_S *)(UINTN) StrToUInt (Value, 16, &Status);
    if (This->Statement->Operand == EFI_IFR_SUBTITLE_OP) {
      CONTROL_CLASS_SET_STATE(Control, UISTATE_DISABLED, 0);
      XmlCreateControl (mSubtitleChilds, Control);
//_Start_L05_GRAPHIC_UI_
      //
      // Hidden emtpy subtitle
      //
      Ch = This->Statement->Prompt;
      while (*Ch == ' ') {
        Ch++;
      };
      if (Ch != CHAR_NULL) {
        UiSetAttribute (Control, L"visibility", L"false");
      }
//_End_L05_GRAPHIC_UI_
    } else if (This->Statement->Operand == EFI_IFR_CHECKBOX_OP) {
      XmlCreateControl (mH2OCheckBoxOpChilds, Control);
      Switch = UiFindChildByName (This, L"CheckBox");
      SetWindowLongPtr (Switch->Wnd, GWLP_USERDATA, This->Statement->StatementId);
    } else if (This->Statement->Operand == EFI_IFR_TEXT_OP
#ifdef L05_GAMING_UI_ENABLE
               || This->Statement->QuestionId == L05_KEY_SYSTEM_BIOS_VERSION
               || This->Statement->QuestionId == L05_KEY_SYSTEMSERIAL_NUMBER
               || This->Statement->QuestionId == L05_KEY_CPU_INFO
#endif
               ) {
      XmlCreateControl (mH2OTextOpChilds, Control);
//_Start_L05_GRAPHIC_UI_
      UiApplyAttributeList (Control, L"min-height='24'");
      if (This->Statement->TextTwo == NULL) {
        CONTROL_CLASS_SET_STATE(Control, UISTATE_DISABLED, 0);
      }
//_End_L05_GRAPHIC_UI_
    } else {
      switch (This->Statement->Operand) {

      case EFI_IFR_REF_OP:
      case EFI_IFR_ACTION_OP:
      case EFI_IFR_PASSWORD_OP:
      case EFI_IFR_NUMERIC_OP:
      case EFI_IFR_ONE_OF_OP:
//_Start_L05_GRAPHIC_UI_
//      case EFI_IFR_TIME_OP:
//      case EFI_IFR_DATE_OP:
//_End_L05_GRAPHIC_UI_
      case EFI_IFR_ORDERED_LIST_OP:
      case EFI_IFR_RESET_BUTTON_OP:
      case EFI_IFR_STRING_OP:
        XmlCreateControl (mStatementChilds, Control);
//_Start_L05_GRAPHIC_UI_
        UiApplyAttributeList (Control, L"min-height='24'");
//_End_L05_GRAPHIC_UI_
        break;

//_Start_L05_GRAPHIC_UI_
      case EFI_IFR_TIME_OP:
        XmlCreateControl (mTimeStatementChilds, Control);
        ChildControl = UiFindChildByName (Control, L"Hrs");
        UnicodeSPrint (
          AttributeStr,
          sizeof (AttributeStr),
          L"text='%s'",
          HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_HRS_PROMPT), NULL)
        );
        UiApplyAttributeList (ChildControl, AttributeStr);

        ChildControl = UiFindChildByName (Control, L"Mins");
        UnicodeSPrint (
          AttributeStr,
          sizeof (AttributeStr),
          L"text='%s'",
          HiiGetString(mHiiHandle, STRING_TOKEN (L05_STR_MINS_PROMPT), NULL)
        );
        UiApplyAttributeList (ChildControl, AttributeStr);

        UiApplyAttributeList (Control, L"min-height='100'");
        break;

      case EFI_IFR_DATE_OP:
        XmlCreateControl (mDateStatementChilds, Control);
        UiApplyAttributeList (Control, L"min-height='120'");
        break;
//_End_L05_GRAPHIC_UI_

      default:
        DEBUG ((EFI_D_ERROR, "Unsupported opcode : %d\n", This->Statement->Operand));
        This->Statement = NULL;
        ASSERT (FALSE);
        break;
      }
    }
//_Start_L05_GRAPHIC_UI_
//    Control->MinSize.cy = GetStatementHeight (This->Statement);
//_End_L05_GRAPHIC_UI_
    UpdateSetupPageItem (Control);
    CONTROL_CLASS_INVALIDATE (This);
    return TRUE;
  }

  return PARENT_CLASS_SET_ATTRIBUTE (CURRENT_CLASS, Control, Name, Value);

}

VOID
EFIAPI
H2OSetupPageItemSetState (
  UI_CONTROL                    *Control,
  UI_STATE                      SetState,
  UI_STATE                      ClearState
  )
{
  STATIC UI_CONTROL             *OldFocusedControl = NULL;
  UI_CONTROL                    *Child;
//_Start_L05_GRAPHIC_UI_
  H2O_SETUP_PAGE_ITEM           *This;
//_End_L05_GRAPHIC_UI_

  PARENT_CONTROL_CLASS(CURRENT_CLASS)->SetState (Control, SetState, ClearState);

  if ((SetState & UISTATE_FOCUSED) == UISTATE_FOCUSED) {
    if (Control != OldFocusedControl) {
//_Start_L05_GRAPHIC_UI_
      if (!IsBootManagerPage ()) {
//_End_L05_GRAPHIC_UI_
      MarqueeByName (Control, L"OptionPrompt", L"true");
      MarqueeByName (Control, L"OptionValue",  L"true");
//_Start_L05_GRAPHIC_UI_
      }
//_End_L05_GRAPHIC_UI_
      OldFocusedControl = Control;
//_Start_L05_GRAPHIC_UI_
      Child = (UI_CONTROL *)UiFindChildByName (Control, L"item-board");
      if (Child != NULL) {
        UiSetAttribute (Child, L"background-color", L"0xFFCFE3F2");
      }
      This = (H2O_SETUP_PAGE_ITEM *) Control;
      if (This->Statement->Operand == EFI_IFR_ONE_OF_OP && This->Statement->Selectable) {
        Child = (UI_CONTROL *)UiFindChildByName (Control, L"OptionValue");
        if (Child != NULL) {
          UiSetAttribute (Child, L"textcolor", L"0xFF000000");
        }
      }
//_End_L05_GRAPHIC_UI_

      Child = UiFindChildByName (Control, L"HotBkg");
      if (Child != NULL) {
        UiSetAttribute (Child, L"visibility", L"true");
      }
    }
  } else if ((ClearState & UISTATE_FOCUSED) == UISTATE_FOCUSED) {
    if (Control == OldFocusedControl) {
      MarqueeByName (Control, L"OptionPrompt", L"false");
      MarqueeByName (Control, L"OptionValue",  L"false");
      OldFocusedControl = NULL;
//_Start_L05_GRAPHIC_UI_
      Child = (UI_CONTROL *)UiFindChildByName (Control, L"item-board");
      if (Child != NULL) {
        UiSetAttribute (Child, L"background-color", L"0x0");
      }
//_End_L05_GRAPHIC_UI_

      Child = UiFindChildByName (Control, L"HotBkg");
      if (Child != NULL) {
        UiSetAttribute (Child, L"visibility", L"false");
      }
    }
  }
}

LRESULT
EFIAPI
H2OSetupPageItemProc (
  HWND   Hwnd,
  UINT32 Msg,
  WPARAM WParam,
  LPARAM LParam
  )
{
  H2O_SETUP_PAGE_ITEM      *This;
  UI_CONTROL               *Control;

  This = (H2O_SETUP_PAGE_ITEM *) GetWindowLongPtr (Hwnd, 0);
  if (This == NULL && Msg != WM_CREATE && Msg != WM_NCCALCSIZE) {
    ASSERT (FALSE);
    return 0;
  }
  Control = (UI_CONTROL *)This;

  switch (Msg) {

  case WM_CREATE:
    This = (H2O_SETUP_PAGE_ITEM *) AllocateZeroPool (sizeof (H2O_SETUP_PAGE_ITEM));
    if (This != NULL) {
      CONTROL_CLASS (This) = (UI_CONTROL_CLASS *) GetClassLongPtr (Hwnd, 0);
      SetWindowLongPtr (Hwnd, 0, (INTN)This);
      SendMessage (Hwnd, UI_NOTIFY_CREATE, WParam, LParam);
    }
    break;

  case UI_NOTIFY_CREATE:
    PARENT_CLASS_WNDPROC (CURRENT_CLASS, Hwnd, UI_NOTIFY_CREATE, WParam, LParam);
    Control->FixedSize.cy = WRAP_CONTENT;
    Control->MinSize.cy = 35;
    break;

  case UI_NOTIFY_PAINT:
    if (WParam == PAINT_BKCOLOR || WParam == PAINT_STATUSIMAGE) {
      break;
    }
    PARENT_CLASS_WNDPROC (CURRENT_CLASS, Hwnd, Msg, WParam, LParam);

//_Start_L05_GRAPHIC_UI_
//    if (WParam == PAINT_ALL &&
//        (CONTROL_CLASS_GET_STATE (This) & (UISTATE_SELECTED | UISTATE_FOCUSED)) == (UISTATE_SELECTED | UISTATE_FOCUSED)) {
//      COLORREF                  Color;
//      UI_MANAGER                *Manager;
//      RECT                      Rc;
//      HDC                       Hdc;
//
//      if (PcdGet32(PcdH2OLmdeMultiLayout) == 0) {
//        Color = 0xFFFF0000;
//      } else {
//        Color = 0xFFFFFFFF;
//      }
//      Manager = Control->Manager;
//      Hdc     = Manager->PaintDC;
//
//      Manager->GetControlRect (Manager, Control, &Rc);
//
//      SelectObject (Hdc, GetStockObject (PS_NULL));
//      SelectObject (Hdc, GetStockObject (DC_PEN));
//      SetDCPenColor (Hdc, Color);
//      Rectangle (Hdc, Rc.left, Rc.top, Rc.right, Rc.bottom);
//   }
//_End_L05_GRAPHIC_UI_
   break;

  case WM_NCHITTEST:
    return HTTRANSPARENT;

  default:
    return PARENT_CLASS_WNDPROC (CURRENT_CLASS, Hwnd, Msg, WParam, LParam);
  }

  return 0;
}

H2O_SETUP_PAGE_ITEM_CLASS *
EFIAPI
GetSetupPageItemClass (
  VOID
  )
{
  if (CURRENT_CLASS != NULL) {
    return CURRENT_CLASS;
  }

  InitUiClass ((UI_CONTROL_CLASS **)&CURRENT_CLASS, sizeof (*CURRENT_CLASS), L"SetupPageItem", (UI_CONTROL_CLASS *) GetControlClass());
  if (CURRENT_CLASS == NULL) {
    return NULL;
  }
  ((UI_CONTROL_CLASS *)CURRENT_CLASS)->WndProc      = H2OSetupPageItemProc;
  ((UI_CONTROL_CLASS *)CURRENT_CLASS)->SetAttribute = H2OSetupPageItemSetAttribute;
  ((UI_CONTROL_CLASS *)CURRENT_CLASS)->SetState     = H2OSetupPageItemSetState;

  return CURRENT_CLASS;
}


