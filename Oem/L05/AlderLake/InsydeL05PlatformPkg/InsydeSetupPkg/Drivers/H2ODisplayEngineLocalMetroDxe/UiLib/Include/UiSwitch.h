/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _UI_SWITCH_H
#define _UI_SWITCH_H

typedef struct _UI_SWITCH UI_SWITCH;
typedef struct _UI_SWITCH_CLASS UI_SWITCH_CLASS;

#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
#include "L05OverclockUiSwitch.h"
#endif

UI_SWITCH_CLASS *
EFIAPI
GetSwitchClass (
  VOID
  );

#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
typedef
VOID
(EFIAPI *L05_UI_SWITCH_WM_LBUTTONDOWN_RECORD) (
  HWND     Hwnd,
  UINT32   Msg,
  WPARAM   WParam,
  LPARAM   LParam
  );

typedef
VOID
(EFIAPI *L05_UI_SWITCH_WM_NCLBUTTONUP_RECORD) (
  HWND     Hwnd,
  UINT32   Msg,
  WPARAM   WParam,
  LPARAM   LParam
  );
#endif

struct _UI_SWITCH {
  UI_BUTTON                     Button;
  BOOLEAN                       CheckBoxValue;
  RECT                          ThumbRc;
  INT32                         ThumbPos;
  INT32                         ThumbRange;
  BOOLEAN                       InitThumb;
  BOOLEAN                       IsHitThumb;
  BOOLEAN                       MoveThumb;
  BOOLEAN                       Disabled;
#ifdef L05_GAMING_OVERCLOCK_UI_ENABLE
  BOOLEAN                       IsL05ValueMode;
  INT32                         L05ValueModeNumber;
  INT32                         L05ValueModeNumberRecord;
  BOOLEAN                       L05UpdateThumbPosByRecord;
  L05_UI_SWITCH_WM_LBUTTONDOWN_RECORD L05UiSwitchWmLbuttondownRecord;
  L05_UI_SWITCH_WM_NCLBUTTONUP_RECORD L05UiSwitchWmNclbuttonupRecord;
#endif
};

struct _UI_SWITCH_CLASS {
  UI_BUTTON_CLASS               ParentClass;
};


#endif
