/** @file
  XML parser

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "UiControls.h"

#define TAG 1
#define CONTENT 2
#define QUOTE   3

#define MAX_ATTRIBS 128

//_Start_L05_GRAPHIC_UI_
#define MAX_TAG_COUNT 64
#define DEBUG_MAX_STRLEN 128
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
VOID
EFIAPI
DumpXmlBuffer (
  CHAR16                       *XMLBuffer
  )
{
  UINTN                        StringLen;
  CHAR16                       Buffer[DEBUG_MAX_STRLEN + 1];

  DEBUG ((EFI_D_ERROR, "=== XML string data ===\n"));

  StringLen = StrLen (XMLBuffer);
  while (StringLen >= DEBUG_MAX_STRLEN) {

    StrnCpyS (Buffer, DEBUG_MAX_STRLEN + 1, XMLBuffer, DEBUG_MAX_STRLEN);
    Buffer[DEBUG_MAX_STRLEN] = '\0';

    DEBUG((EFI_D_ERROR, "%s", Buffer));

    XMLBuffer += DEBUG_MAX_STRLEN;
    StringLen -= DEBUG_MAX_STRLEN;
  };

  if (StringLen != 0) {
    DEBUG((EFI_D_ERROR, "%s", XMLBuffer));
  }
  DEBUG ((EFI_D_ERROR, "\n=== XML string data ===\n"));
}
//_End_L05_GRAPHIC_UI_

STATIC
BOOLEAN
IsSpace (
  INT32 c
  )
{
  return ((c<=' ' && (c==' ' || (c<=13 && c>=9))));
}

STATIC
VOID
ParserContent (
  CHAR16 *Str,
  SAX_PARSER_CONTENT_CALLBACK ContentCb,
  VOID   *Data
  )
{
  while (*Str != '\0' && IsSpace (*Str)) {
    Str++;
  }

  if (*Str == '\0') {
    return ;
  }

  if (ContentCb != NULL) {
    (*ContentCb)(Data, Str);
  }
}


//_Start_L05_GRAPHIC_UI_
//STATIC
//VOID
//ParseElement (
//  IN OUT CHAR16                *Str,
//  IN SAX_PARSER_START_CALLBACK StartCb,
//  IN SAX_PARSER_END_CALLBACK   EndCb,
//  IN OUT VOID                  *Data
//  )
STATIC
EFI_STATUS
ParseElement (
  IN OUT CHAR16                *Str,
  IN SAX_PARSER_START_CALLBACK StartCb,
  IN SAX_PARSER_END_CALLBACK   EndCb,
  IN OUT VOID                  *Data,
  IN CHAR16                    *XMLBuffer,
  IN CHAR16                    **TagNameStack,
  IN UINTN                     *TagCount
  )
//_End_L05_GRAPHIC_UI_
{
  CHAR16       *AttrNames[MAX_ATTRIBS];
  CHAR16       *AttrValues[MAX_ATTRIBS];
  UINTN        AttrNum;
  CHAR16       *TagName;
  BOOLEAN      IsStart;
  BOOLEAN      IsEnd;
  CHAR16       QuoteChar;

  AttrNum = 0;
  TagName = NULL;
  IsStart = FALSE;
  IsEnd   = FALSE;

  //
  // trim white space after the '<'
  //
  while (*Str != '\0' && IsSpace (*Str)) {
    Str++;
  }

  //
  // start tag or end tag
  //
  if (*Str == L'/') {
    Str++;
    IsEnd = TRUE;
  } else {
    IsStart = TRUE;
  }

  //
  // commonts or preprocessor
  //
  if (*Str == '\0' || *Str == L'?' || *Str == '!') {
//_Start_L05_GRAPHIC_UI_
//    return ;
    return EFI_SUCCESS;
//_End_L05_GRAPHIC_UI_
  }


  TagName = Str;
  while (*Str !='\0' && !IsSpace (*Str) && *Str != '/') {
    Str++;
  }

  if (*Str != '\0') {
    if (*Str == '/') {
      IsEnd = TRUE;
    }
    *Str++ = L'\0';
  }

  while (!IsEnd && *Str != '\0' && AttrNum < (MAX_ATTRIBS - 1)) {

    while (*Str != '\0' && IsSpace (*Str)) {
      Str++;
    }

    if (*Str == '\0') {
      break;
    }

    if (*Str == '/') {
      IsEnd = TRUE;
      break;
    }

    AttrNames[AttrNum] = Str++;
    AttrValues[AttrNum] = NULL;
    while (*Str !='\0' && !IsSpace (*Str) && *Str != '=') {
      Str++;
    }

    if (*Str != '\0') {
      *Str++ = '\0';
    }

    //
    // beginning of the value
    //
    while (*Str != '\0' && *Str != '\"' && *Str != '\'') {
      Str++;
    }

    if (*Str == '\0') {
      break;
    }

    QuoteChar = *Str;
    Str++;
    AttrValues[AttrNum++] = Str;
    while (*Str != '\0' && *Str != QuoteChar) {
      if (*Str == '\\' && *(Str + 1) == QuoteChar) {
        Str += 2;
        continue;
      }
      Str++;
    }
    if (*Str != '\0') {
      *Str++ = '\0';
    }
  }
  AttrNames[AttrNum] = NULL;
  AttrValues[AttrNum++] = NULL;

//_Start_L05_GRAPHIC_UI_
//  if (IsStart && StartCb) {
//    (*StartCb)(Data, TagName, AttrNames, AttrValues);
//  }
//  if (IsEnd && EndCb) {
//    (*EndCb)(Data, TagName);
//  }
  if (IsStart) {
    if (*TagCount >= MAX_TAG_COUNT) {
      return EFI_OUT_OF_RESOURCES;
    }
    TagNameStack[*TagCount] = TagName;
    *TagCount = *TagCount + 1;
    if (StartCb) {
      (*StartCb)(Data, TagName, AttrNames, AttrValues);
    }
  }
  if (IsEnd) {
    if (*TagCount < 1) {
      DEBUG((EFI_D_ERROR, "The parser found the start of an element after the end of the root element.!!!\n"));
      DEBUG((EFI_D_ERROR, "\"%s\"\n", TagName));
      DumpXmlBuffer (XMLBuffer);
      ASSERT (FALSE);
    }

    if (StrCmp (TagName, TagNameStack[*TagCount - 1]) == 0) {
      TagNameStack[*TagCount] = NULL;
      *TagCount = *TagCount - 1;
    } else {
      DEBUG((EFI_D_ERROR, "The start and end tag names of an element did not match!!!"));
      DEBUG((EFI_D_ERROR, "Maybe the element is empty and no matching end-tag, but no slash before > chracter, add a slash like <Contorl />"));

      DEBUG((EFI_D_ERROR, "\"%s\" doesn't match \"%s\"\n", TagName, TagNameStack[*TagCount - 1]));
      DumpXmlBuffer (XMLBuffer);
      ASSERT (FALSE);
      return EFI_INVALID_PARAMETER;
    }
    if (EndCb) {
      (*EndCb)(Data, TagName);
    }
  }

  return EFI_SUCCESS;
//_End_L05_GRAPHIC_UI_
}

BOOLEAN
EFIAPI
SaxParser (
  CHAR16           *XMLBuffer,
  SAX_PARSER_START_CALLBACK   StartCb,
  SAX_PARSER_CONTENT_CALLBACK ContentCb,
  SAX_PARSER_END_CALLBACK     EndCb,
  VOID             *Data
  )
{
//_Start_L05_GRAPHIC_UI_
  CHAR16                      *BackupStr;
  CHAR16                      *Str;
  CHAR16                      *Mark;
  UINTN                       State;
  CHAR16                      QuoteChar;
  BOOLEAN                     Result;
  CHAR16                      *TagNameStack[MAX_TAG_COUNT];
  UINTN                       TagCount;
  UINTN                       Index;
//_End_L05_GRAPHIC_UI_

//_Start_L05_GRAPHIC_UI_
//  Str   = XMLBuffer;
  BackupStr = AllocateCopyPool (StrSize (XMLBuffer), XMLBuffer);
  if (BackupStr == NULL) {
    return FALSE;
  }

  Str    = BackupStr;
  Mark   = Str;
  State  = CONTENT;
  QuoteChar = 0;
  Result = TRUE;
  TagCount = 0;
//_End_L05_GRAPHIC_UI_

  while (*Str != '\0') {

    if (*Str == '<' && State == CONTENT) {
      *Str++ = '\0';
      ParserContent (Mark, ContentCb, Data);
      Mark = Str;
      State = TAG;
    } else if (*Str == '\\' && State == QUOTE) {
      if (*(Str + 1) == QuoteChar) {
        Str += 2;
        continue;
      }
      Str++;
    } else if (*Str == '\"' || *Str == '\'') {
      if (State == TAG) {
        State = QUOTE;
        QuoteChar = *Str;
      } else if (State == QUOTE && *Str == QuoteChar) {
        State = TAG;
      }
      Str++;
    } else if (*Str == '>' && State == TAG) {
      *Str++ = '\0';
//_Start_L05_GRAPHIC_UI_
//      ParseElement (Mark, StartCb, EndCb, Data);
      ParseElement (Mark, StartCb, EndCb, Data, XMLBuffer, TagNameStack, &TagCount);
//_End_L05_GRAPHIC_UI_
      Mark = Str;
      State = CONTENT;
    } else {
      Str++;
    }
  }

//_Start_L05_GRAPHIC_UI_
//  return TRUE;
  if (State != CONTENT) {
    if (State == QUOTE) {
      DEBUG ((EFI_D_ERROR, "SaxParser: The parser found an invalid attribute format, not paired \'!!\n"));
      DEBUG ((EFI_D_ERROR, "%s\n", Mark));
    } else if (State == TAG) {
      DEBUG ((EFI_D_ERROR, "SaxParser: The parser an invalid start of an element, not paired \">\"!!\n"));
      DEBUG ((EFI_D_ERROR, "%s\n", Mark));
    }
    DumpXmlBuffer (XMLBuffer);
    ASSERT (State == CONTENT);
    Result = FALSE;
  }

  if (TagCount != 0) {
    DEBUG ((EFI_D_ERROR, "SaxParser: The parser found the no paired start statement!!\n"));
    for (Index = 0; Index < TagCount; Index++) {
      DEBUG ((EFI_D_ERROR, "\"%s\"\n", TagNameStack[Index]));
    }
    DumpXmlBuffer (XMLBuffer);
    ASSERT (TagCount == 0);
  }

  FreePool (BackupStr);
  return Result;
//_End_L05_GRAPHIC_UI_
}


