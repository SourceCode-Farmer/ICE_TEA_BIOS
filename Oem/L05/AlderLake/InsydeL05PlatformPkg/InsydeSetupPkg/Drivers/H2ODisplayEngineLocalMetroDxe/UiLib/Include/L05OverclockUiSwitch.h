/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_OVERCLOCK_UI_SWITCH_H
#define _L05_OVERCLOCK_UI_SWITCH_H

#define SWITCH_TEXT_LENGTH       60
#define SWITCH_BORDER_WIDTH      4

#define L05_SWITCH_PADDING_WIDTH 8
#define L05_THUMB_SEPARATE_RATIO 100

INT32
L05UiSwitchValueModeUpdateThumbPos (
  IN UI_SWITCH                          *This
  );

VOID
L05UpdateSwitch (
  UI_CONTROL                            *Control,
  CONST RECT                            *Rc
  );

extern CHAR16                           *mL05GamingSwitchChilds;

#endif
