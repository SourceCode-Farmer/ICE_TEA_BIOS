/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "UiRender.h"
#include "UiControls.h"
#include "UiManager.h"
#include <InternalFormRepresentation.h>
#include <Library/HiiLib.h>
#include <Library/UefiHiiServicesLib.h>
#include <Library/BitBltLib.h>

EFI_HII_HANDLE                  mImageHiiHandle = NULL;

typedef struct {
  CHAR16                        *Name;
  EFI_IMAGE_ID                  ImageId;
  EFI_IMAGE_INPUT               *ImageIn;       ///< Cache Image Input data
} IMAGE_RESOURCE;

IMAGE_RESOURCE mImageResource[] = {
  {L"FormHalo",              IMAGE_TOKEN (IMAGE_FORM_HALO               ), NULL},
  {L"MenuBackgroundNormal",  IMAGE_TOKEN (IMAGE_MENU_BACKGROUND_NORMAL  ), NULL},
  {L"MenuBackgroundHot",     IMAGE_TOKEN (IMAGE_MENU_BACKGROUND_HOT     ), NULL},
  {L"HotkeyShowHelp",        IMAGE_TOKEN (IMAGE_HOTKEY_SHOW_HELP        ), NULL},
  {L"HotkeySelectItemUp",    IMAGE_TOKEN (IMAGE_HOTKEY_SELECT_ITEM_UP   ), NULL},
  {L"HotkeySelectItemDown",  IMAGE_TOKEN (IMAGE_HOTKEY_SELECT_ITEM_DOWN ), NULL},
  {L"HotkeySelectMenuUp",    IMAGE_TOKEN (IMAGE_HOTKEY_SELECT_MENU_UP   ), NULL},
  {L"HotkeySelectMenuDown",  IMAGE_TOKEN (IMAGE_HOTKEY_SELECT_MENU_DOWN ), NULL},
  {L"HotKeyModifyValueUp",   IMAGE_TOKEN (IMAGE_HOTKEY_MODIFY_VALUE_UP  ), NULL},
  {L"HotKeyModifyValueDown", IMAGE_TOKEN (IMAGE_HOTKEY_MODIFY_VALUE_DOWN), NULL},
  {L"HotKeyLoadDefault",     IMAGE_TOKEN (IMAGE_HOTKEY_LOAD_DEFAULT     ), NULL},
  {L"HotKeySaveAndExit",     IMAGE_TOKEN (IMAGE_HOTKEY_SAVE_AND_EXIT    ), NULL},
  {L"HotkeyEnter",           IMAGE_TOKEN (IMAGE_HOTKEY_ENTER            ), NULL},
  {L"HotKeyDiscardExit",     IMAGE_TOKEN (IMAGE_HOTKEY_DISCARD_EXIT     ), NULL},
  {L"OwnerDrawInsyde",       IMAGE_TOKEN (IMAGE_OWNER_DRAW_INSYDE       ), NULL},
  {L"OwnerDrawInsydeH2O",    IMAGE_TOKEN (IMAGE_OWNER_DRAW_INSYDE_H2O   ), NULL},
  {L"OwnerDrawSeparator",    IMAGE_TOKEN (IMAGE_OWNER_DRAW_SEPARATOR    ), NULL},
  {L"OwnerDrawTemperature",  IMAGE_TOKEN (IMAGE_OWNER_DRAW_TEMPERATURE  ), NULL},
  {L"OwnerDrawTime",         IMAGE_TOKEN (IMAGE_OWNER_DRAW_TIME         ), NULL},
  {L"DialogSeparator",       IMAGE_TOKEN (IMAGE_DIALOG_SEPARATOR        ), NULL},
  {L"DialogButtonDown",      IMAGE_TOKEN (IMAGE_DIALOG_BUTTON_DOWN      ), NULL},
  {L"DialogButtonUp",        IMAGE_TOKEN (IMAGE_DIALOG_BUTTON_UP        ), NULL},
  {L"DialogSelectedIcon",    IMAGE_TOKEN (IMAGE_DIALOG_SELECTED_ICON    ), NULL},
  {L"DialogSortIcon",        IMAGE_TOKEN (IMAGE_DIALOG_SORT_ICON        ), NULL},
  {L"ScrollbarThumb",        IMAGE_TOKEN (IMAGE_SCROLLBAR_THUMB         ), NULL},
  {L"ScrollbarBackground",   IMAGE_TOKEN (IMAGE_SCROLLBAR_BACKGROUND    ), NULL},
  {L"SetupMenuTextOpBkg",    IMAGE_TOKEN (IMAGE_SETUP_MENU_TEXT_OP_BKG  ), NULL},
  {L"FormTitleBkg",          IMAGE_TOKEN (IMAGE_FORM_TITLE_BKG          ), NULL},
  {L"OptionBkg",             IMAGE_TOKEN (IMAGE_OPTION_BKG              ), NULL},
  {L"OwnerDrawBkg",          IMAGE_TOKEN (IMAGE_OWNER_DRAW_BKG          ), NULL},
  {L"CheckBoxBkg",           IMAGE_TOKEN (IMAGE_CHECK_BOX_BKG           ), NULL},
  {L"CheckBoxBorder",        IMAGE_TOKEN (IMAGE_CHECK_BOX_BORDER        ), NULL},
  {L"CheckBoxButton",        IMAGE_TOKEN (IMAGE_CHECK_BOX_BUTTON        ), NULL},
#if FixedPcdGet32(PcdH2OLmdeMultiLayout) == 1
  {L"SpTriangle",            IMAGE_TOKEN (IMAGE_SP_TRIANGLE             ), NULL},
  {L"Bkg",                   IMAGE_TOKEN (IMAGE_BKG                     ), NULL},
  {L"DialogBkg",             IMAGE_TOKEN (IMAGE_DIALOG_BKG              ), NULL},
  {L"FtBorder",              IMAGE_TOKEN (IMAGE_FT_BORDER               ), NULL},
  {L"HtBorder",              IMAGE_TOKEN (IMAGE_HT_BORDER               ), NULL},
  {L"HtSepartor",            IMAGE_TOKEN (IMAGE_HT_SEPARATOR            ), NULL},
  {L"SmBorder",              IMAGE_TOKEN (IMAGE_SM_BORDER               ), NULL},
  {L"SpSelectedBkg",         IMAGE_TOKEN (IMAGE_SP_SELECTED_BKG         ), NULL},
  {L"SpTriangle",            IMAGE_TOKEN (IMAGE_SP_TRIANGLE             ), NULL},
#endif
//_Start_L05_GRAPHIC_UI_
  {L"Ideapad",               IMAGE_TOKEN (L05_IMAGE_IDEAPAD             ), NULL},
  {L"Legion",                IMAGE_TOKEN (L05_IMAGE_LEGION              ), NULL},
  {L"XiaoXin",               IMAGE_TOKEN (L05_IMAGE_XIAOXIN             ), NULL},
  {L"Yoga",                  IMAGE_TOKEN (L05_IMAGE_YOGA                ), NULL},
#ifdef L05_SMB_BIOS_ENABLE
  {L"ThinkBook",             IMAGE_TOKEN (L05_IMAGE_THINKBOOK           ), NULL},
  {L"LenovoBranding",        IMAGE_TOKEN (L05_IMAGE_LENOVO_BRANDING     ), NULL},
#endif
  {L"Lenovo",                IMAGE_TOKEN (L05_IMAGE_LENOVO_BLACK        ), NULL},
//[-start-220410-Ching000042-modify]//
//[-start-220412-Ching000044-modify]//
//[-start-220422-Ching000046-modify]//
#if defined(S77013_SUPPORT) || defined(S77014_SUPPORT)
  {L"LenovoNA",              IMAGE_TOKEN (L05_IMAGE_LENOVO_NA           ), NULL},
#endif
#if defined(S77014IAH_SUPPORT) || defined(S77014_SUPPORT)
  {L"XiaoXinIAH",            IMAGE_TOKEN (L05_IMAGE_XIAOXIN_IAH         ), NULL},
#endif
//[-end-220422-Ching000046-modify]//
//[-end-220412-Ching000044-modify]//
//[-end-220410-Ching000042-modify]//
  {L"LenovoColor",           IMAGE_TOKEN (L05_IMAGE_LENOVO_COLOR        ), NULL},
  {L"L05Return",             IMAGE_TOKEN (L05_IMAGE_RETURN              ), NULL},
  {L"L05DropdownButton",     IMAGE_TOKEN (L05_IMAGE_DROPDOWN_BUTTON     ), NULL},
  {L"L05DialogQuestion",     IMAGE_TOKEN (L05_IMAGE_DIALOG_QUESTION     ), NULL},
  {L"L05Success",            IMAGE_TOKEN (L05_IMAGE_DIALOG_SUCCESS      ), NULL},
  {L"L05Failed",             IMAGE_TOKEN (L05_IMAGE_DIALOG_FAILED       ), NULL},
  {L"L05DialogButtonDown",   IMAGE_TOKEN (L05_IMAGE_DIALOG_BUTTON_DOWN  ), NULL},
  {L"L05DialogButtonUp",     IMAGE_TOKEN (L05_IMAGE_DIALOG_BUTTON_UP    ), NULL},
  {L"L05Copyright",          IMAGE_TOKEN (L05_IMAGE_COPYRIGHT           ), NULL},
  {L"ProgressRing1",         IMAGE_TOKEN (L05_IMAGE_PROGRESS_RING_1     ), NULL},
  {L"ProgressRing2",         IMAGE_TOKEN (L05_IMAGE_PROGRESS_RING_2     ), NULL},
  {L"ProgressRing3",         IMAGE_TOKEN (L05_IMAGE_PROGRESS_RING_3     ), NULL},
  {L"ProgressRing4",         IMAGE_TOKEN (L05_IMAGE_PROGRESS_RING_4     ), NULL},
#ifdef L05_GAMING_UI_ENABLE
  {L"GamingLegion",          IMAGE_TOKEN (L05_IMAGE_LEGION_LOGO         ), NULL},
  {L"L05GamingBackground",   IMAGE_TOKEN (L05_IMAGE_GAMING_BACKGROUND   ), NULL},
  {L"L05CommonBackground",   IMAGE_TOKEN (L05_IMAGE_COMMON_BACKGROUND   ), NULL},
  {L"L05Exit",               IMAGE_TOKEN (L05_IMAGE_EXIT                ), NULL},
  {L"L05CpuIcon",            IMAGE_TOKEN (L05_IMAGE_CPU_ICON            ), NULL},
  {L"L05GpuIcon",            IMAGE_TOKEN (L05_IMAGE_GPU_ICON            ), NULL},
  {L"L05SnIcon",             IMAGE_TOKEN (L05_IMAGE_SN_ICON             ), NULL},
  {L"L05VersionIcon",        IMAGE_TOKEN (L05_IMAGE_VERSION_ICON        ), NULL},
  {L"L05SuperStar",          IMAGE_TOKEN (L05_IMAGE_SUPER_STAR_ICON     ), NULL},
  {L"L05DownArrow",          IMAGE_TOKEN (L05_IMAGE_DOWN_ARROW          ), NULL},
  {L"L05DownArrowDisable",   IMAGE_TOKEN (L05_IMAGE_DOWN_ARROW_DISABLE  ), NULL},
  {L"L05DownArrowHover",     IMAGE_TOKEN (L05_IMAGE_DOWN_ARROW_HOVER    ), NULL},
  {L"L05UpArrow",            IMAGE_TOKEN (L05_IMAGE_UP_ARROW            ), NULL},
  {L"L05UpArrowDisable",     IMAGE_TOKEN (L05_IMAGE_UP_ARROW_DISABLE    ), NULL},
  {L"L05UpArrowHover",       IMAGE_TOKEN (L05_IMAGE_UP_ARROW_HOVER      ), NULL},
  {L"L05Its40ExtremeMode",     IMAGE_TOKEN (L05_IMAGE_ITS40_EXTREME     ), NULL},
  {L"L05Its40IntelligentMode", IMAGE_TOKEN (L05_IMAGE_ITS40_INTELLIGENT ), NULL},
  {L"L05Its40SavingMode",      IMAGE_TOKEN (L05_IMAGE_ITS40_SAVING      ), NULL},
  {L"L05Its30Performance",   IMAGE_TOKEN (L05_IMAGE_ITS30_PERFORMANCE   ), NULL},
  {L"L05Its30Balance",       IMAGE_TOKEN (L05_IMAGE_ITS30_BALANCE       ), NULL},
  {L"L05Its30Quiet",         IMAGE_TOKEN (L05_IMAGE_ITS30_QUIET         ), NULL},
  {L"L05RadioOn",            IMAGE_TOKEN (L05_IMAGE_RADIO_ON            ), NULL},
  {L"L05RadioOff",           IMAGE_TOKEN (L05_IMAGE_RADIO_OFF           ), NULL},
  {L"L05Back",               IMAGE_TOKEN (L05_IMAGE_BACK                ), NULL},
  {L"L05ButtonNormal",       IMAGE_TOKEN (L05_IMAGE_BUTTON_NORMAL       ), NULL},
  {L"L05ButtonNormal2",      IMAGE_TOKEN (L05_IMAGE_BUTTON_NORMAL2      ), NULL},
  {L"L05ButtonHover",        IMAGE_TOKEN (L05_IMAGE_BUTTON_HOVER        ), NULL},
  {L"L05ButtonHover2",       IMAGE_TOKEN (L05_IMAGE_BUTTON_HOVER2       ), NULL},
  {L"L05DialogBackground",   IMAGE_TOKEN (L05_IMAGE_DIALOG_BACKGROUND   ), NULL},
//_Start_L05_OVERCLOCK_UI_
  {L"L05OverclockBRY000",    IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BRY_000), NULL},
  {L"L05OverclockBRY001",    IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BRY_001), NULL},
  {L"L05OverclockBRY010",    IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BRY_010), NULL},
  {L"L05OverclockBRY011",    IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BRY_011), NULL},
  {L"L05OverclockBRY100",    IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BRY_100), NULL},
  {L"L05OverclockBRY101",    IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BRY_101), NULL},
  {L"L05OverclockBRY110",    IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BRY_110), NULL},
  {L"L05OverclockBRY111",    IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BRY_111), NULL},
  {L"L05OverclockBulletBlue100", IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BULLET_BLUE_100), NULL},
  {L"L05OverclockBulletRed010", IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BULLET_RED_010), NULL},
  {L"L05OverclockBulletYellow001", IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_BULLET_YELLOW_001), NULL},
  {L"L05OverclockSwitchOff", IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_SWITCH_OFF), NULL},
  {L"L05OverclockSwitchOn",  IMAGE_TOKEN (L05_IMAGE_OVERCLOCKING_SWITCH_ON), NULL},
  {L"L05ToggleOn",           IMAGE_TOKEN (L05_IMAGE_TOGGLE_ON), NULL},
  {L"L05ToggleOff",          IMAGE_TOKEN (L05_IMAGE_TOGGLE_OFF), NULL},
  {L"L05ResetIcon",          IMAGE_TOKEN (L05_IMAGE_RESET_ICON), NULL},
  {L"L05CloseIconWhite",     IMAGE_TOKEN (L05_IMAGE_CLOSE_ICON_WHITE), NULL},
  {L"L05CloseIconBlue",      IMAGE_TOKEN (L05_IMAGE_CLOSE_ICON_BLUE), NULL},
  {L"L05MinusIcon",          IMAGE_TOKEN (L05_IMAGE_MINUS_ICON), NULL},
  {L"L05PlusIcon",           IMAGE_TOKEN (L05_IMAGE_PLUS_ICON), NULL},
  {L"L05CheckBoxBkg",        IMAGE_TOKEN (L05_IMAGE_CHECK_BOX_BKG), NULL},
  {L"L05CheckBoxBullet",     IMAGE_TOKEN (L05_IMAGE_CHECK_BOX_BULLET), NULL},
//_End_L05_OVERCLOCK_UI_
#endif
//_End_L05_GRAPHIC_UI_
  {NULL, 0, NULL}
  };

EFI_IMAGE_INPUT *
GetImageByString (
  CHAR16                        *Name
  )
{
  UINTN                         Index;
  EFI_STATUS                    Status;
  EFI_IMAGE_INPUT               *ImagePtr;

  ImagePtr = NULL;
  if (Name[0] == '@') {
    Name++;
    for (Index = 0; mImageResource[Index].Name != NULL; Index++) {
      if (StrCmp (mImageResource[Index].Name, Name) == 0) {
        break;
      }
    }
    ASSERT (mImageResource[Index].Name != NULL);
    if (mImageResource[Index].Name == NULL) {
      return NULL;
    }

    if (mImageResource[Index].ImageIn != NULL) {
      ImagePtr = mImageResource[Index].ImageIn;
    } else {
      ImagePtr = AllocateZeroPool (sizeof (EFI_IMAGE_INPUT));
      Status = gHiiImage->GetImage (
                            gHiiImage,
                            mImageHiiHandle,
                            mImageResource[Index].ImageId,
                            ImagePtr
                            );
      ASSERT_EFI_ERROR (Status);
      if (EFI_ERROR (Status)){
        FreePool (ImagePtr);
        return NULL;
      }
      mImageResource[Index].ImageIn = ImagePtr;
    }
  } else {
    ImagePtr = (EFI_IMAGE_INPUT *)(UINTN) StrToUInt (Name, 16, &Status);
    ASSERT_EFI_ERROR (Status);
    ASSERT (ImagePtr != NULL);
  }

  if (ImagePtr != NULL) {
    //
    // We only convert transparent image once.
    //
    if ((ImagePtr->Flags & EFI_IMAGE_TRANSPARENT) == EFI_IMAGE_TRANSPARENT) {
      ConvertToAlphaChannelImage (ImagePtr);
      ImagePtr->Flags &= ~EFI_IMAGE_TRANSPARENT;
    }
  }

  return ImagePtr;

}

VOID
AddHiiImagePackage (
  VOID
  )
{
  if (mImageHiiHandle == NULL) {
    mImageHiiHandle = HiiAddPackages (&gEfiCallerIdGuid, NULL, IMAGE_ARRAY_NAME, NULL);
    ASSERT (mImageHiiHandle != NULL);
  }
}
