/** @file
  Entry point and initial functions for H2O local Metro display engine driver

;******************************************************************************
;* Copyright (c) 2013 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "H2ODisplayEngineLocalMetro.h"
#include "UiControls.h"
#include "H2OControls.h"
#include "H2OPanels.h"
#include "MetroUi.h"
#include <Guid/ZeroGuid.h>
//_Start_L05_GRAPHIC_UI_
#include "L05Hook/L05InitializeCursor.h"
#include "L05Hook/L05GetResolution.h"
//_End_L05_GRAPHIC_UI_

extern H2O_DISPLAY_ENGINE_METRO_PRIVATE_DATA      *mMetroPrivate;
extern HWND                                       gLastFocus;
H2O_LAYOUT_INFO                                   *mLayoutInfo = NULL;
EFI_INPUT_KEY                                     mPrivateHotKeyList[] = {{SCAN_NULL , CHAR_CARRIAGE_RETURN},
                                                                          {SCAN_UP   , CHAR_NULL},
                                                                          {SCAN_DOWN , CHAR_NULL},
                                                                          {SCAN_RIGHT, CHAR_NULL},
                                                                          {SCAN_LEFT , CHAR_NULL},
//_Start_L05_GRAPHIC_UI_
//                                                                          {SCAN_ESC  , CHAR_NULL},
//_End_L05_GRAPHIC_UI_
                                                                          };

typedef struct _DISPLAY_ENGINE_FRAME DISPLAY_ENGINE_FRAME;

struct _DISPLAY_ENGINE_FRAME {
  UI_FRAME                      Frame;

  UI_CONTROL                    *H2OSetupMenuPanel;
  UI_CONTROL                    *H2OSetupPagePanel;
  UI_CONTROL                    *H2OFormTitlePanel;
  UI_CONTROL                    *H2OHelpTextPanel;
  UI_CONTROL                    *H2OHotkeyPanel;
  UI_CONTROL                    *H2OOwnerDrawPanel;
};

typedef struct _PANEL_VFCF_DATA {
  CHAR16                        *PanelName;
  UINT32                        PanelId;
} PANEL_VFCF_DATA;

PANEL_VFCF_DATA                 mPanelVfcfData[] = {
                                  {L"H2OOwnerDrawPanel", OWNER_DRAW_PANEL_ID},
                                  {L"H2OSetupMenuPanel", SETUP_MENU_PANEL_ID},
                                  {L"H2OFormTitlePanel", TITLE_PANEL_ID},
                                  {L"H2OSetupPagePanel", SETUP_PAGE_PANEL_ID},
                                  {L"H2OHelpTextPanel" , HELP_TEXT_PANEL_ID},
                                  {L"H2OHotkeyPanel"   , HOTKEY_PANEL_ID},
                                  {NULL, 0},
                                  };

typedef struct _DISPLAY_ENGINE_FRAME_CLASS {
  UI_FRAME_CLASS                ParentClass;
} DISPLAY_ENGINE_FRAME_CLASS;

STATIC DISPLAY_ENGINE_FRAME_CLASS    *mDisplayEngineFrameClass = NULL;
#define CURRENT_CLASS                mDisplayEngineFrameClass

DISPLAY_ENGINE_FRAME_CLASS *
EFIAPI
GetDisplayEngineFrameClass (
  VOID
  );

//_Start_L05_GRAPHIC_UI_
//#if FixedPcdGet32(PcdH2OLmdeMultiLayout) == 0
//_End_L05_GRAPHIC_UI_
CHAR16 *mDialogPanelChilds = L""
  L"<VerticalLayout name='DialogPanel' background-color='0xFF000000'/>";

//_Start_L05_GRAPHIC_UI_
//CHAR16 *mScreenPanelChilds = L""
//  L"<Control name='background' background-color='0xFF000000'>"
//    L"<Texture float='true' name='ContextBkg'     background-color='0xFFE8E8E8'/>"
//    L"<H2OOwnerDrawPanel name='H2OOwnerDrawPanel' background-color='0xFF000000' RefreshInterval='100'/>"
//    L"<H2OSetupMenuPanel name='H2OSetupMenuPanel' background-color='0xFF000000'/>"
//    L"<H2OSetupPagePanel name='H2OSetupPagePanel' background-color='0x0'/>"
//    L"<H2OFormTitlePanel name='H2OFormTitlePanel' background-color='0x0'/>"
//    L"<H2OHelpTextPanel  name='H2OHelpTextPanel'  background-color='0x0'/>"
//    L"<H2OHotkeyPanel    name='H2OHotkeyPanel'    background-color='0xFF333333' background-image='@OwnerDrawBkg' background-image-style='stretch' padding='5,5,5,5'/>"
//    //L"<Texture background-image='@FormHalo' float='true' name='FormHalo' scale9grid='23,26,22,31'/>"
//    L"<Texture name='overlay' float='true' visibility='false' height='-1' width='-1' background-color='0x80C8C8C8'/>"
//  L"</Control>";
CHAR16 *mScreenPanelChilds = L""
  L"<Control name='background' background-color='0xFF000000'>"
    L"<H2OOwnerDrawPanel RefreshInterval='400' name='H2OOwnerDrawPanel'/>"
    L"<H2OSetupMenuPanel name='H2OSetupMenuPanel'/>"
    L"<H2OFormTitlePanel border-color='0xff123456' name='H2OFormTitlePanel'/>"
    L"<H2OSetupPagePanel name='H2OSetupPagePanel'/>"
    L"<H2OHelpTextPanel border-color='0xff123456' name='H2OHelpTextPanel'/>"
    L"<H2OHotkeyPanel name='H2OHotkeyPanel'/>"
    L"<Texture background-image='@FormHalo' float='true' name='FormHalo' scale9grid='23,26,22,31'/>"
    L"<Texture name='overlay' float='true' visibility='false' height='-1' width='-1' background-color='0x80C8C8C8'/>"
    //
    // blue bar between setup menu and setup page and hotkey panel
    //
    L"<Control background-color='0xFF0F75BC' float='true' name='bluebarV' pos='440,75,444,946' />"
    L"<Control background-color='0xFF0F75BC' float='true' name='bluebarH' pos='70,946,1850,950' />"
  L"</Control>";

CHAR16 *mBootManagerPageChilds = L""
  L"<Control name='background' background-color='0xFFFFFFFF'>"
    L"<VerticalLayout padding='10,0,0,10' name='DialogPage' background-color='0xFFFFFFFF' height='555' width='838'>"
      L"<Control background-color='0xFFE5E5E5' height='545' width='828'/>"
      L"<VerticalLayout padding='2,2,2,2' background-color='0xFF3E8DDD' height='545' width='828' float='true'>"
        L"<VerticalLayout padding='26,0,26,35' name='body' background-color='0xFFFFFFFF' height='541' width='824'>"
          L"<Label textcolor='0xFF3E8DDD' font-size='24' height='24' text='Please select boot device:'/>"
          L"<Control height='45'/>"
          L"<H2OOwnerDrawPanel name='H2OOwnerDrawPanel'/>"
          L"<H2OSetupMenuPanel name='H2OSetupMenuPanel'/>"
          L"<H2OFormTitlePanel border-color='0xff123456' name='H2OFormTitlePanel'/>"
          L"<H2OSetupPagePanel name='H2OSetupPagePanel'/>"
          L"<H2OHelpTextPanel border-color='0xff123456' name='H2OHelpTextPanel'/>"
          L"<H2OHotkeyPanel name='H2OHotkeyPanel'/>"
          L"<Texture background-image='@FormHalo' float='true' name='FormHalo' scale9grid='23,26,22,31'/>"
          L"<Texture name='overlay' float='true' visibility='false' height='-1' width='-1' background-color='0x80C8C8C8'/>"
          L"<Control height='45'/>"
          L"<Label name='help' text-align='singleline|center' textcolor='0xFF3E8DDD' font-size='24' height='24' text=' '/>"
        L"</VerticalLayout>"
      L"</VerticalLayout>"
    L"</VerticalLayout>"
  L"</Control>";

CHAR16 *mNovoMenuPageChilds = L""
  L"<Control name='background' background-color='0xFFFFFFFF'>"
    L"<VerticalLayout padding='10,0,0,10' name='DialogPage' background-color='0xFFFFFFFF' height='350' width='520'>"
      L"<Control background-color='0xFFE5E5E5' height='340' width='510'/>"
      L"<VerticalLayout padding='2,2,2,2' background-color='0xFF3E8DDD' height='340' width='510' float='true'>"
        L"<VerticalLayout padding='26,0,0,20' name='body' background-color='0xFFFFFFFF' height='336' width='506'>"
          L"<Label textcolor='0xFF3E8DDD' padding='0,0,0,20' font-size='32' height='32' text='Novo Button Menu'/>"
          L"<Control height='45'/>"
          L"<H2OOwnerDrawPanel name='H2OOwnerDrawPanel'/>"
          L"<H2OSetupMenuPanel name='H2OSetupMenuPanel'/>"
          L"<H2OFormTitlePanel border-color='0xff123456' name='H2OFormTitlePanel'/>"
          L"<H2OSetupPagePanel name='H2OSetupPagePanel'/>"
          L"<H2OHelpTextPanel border-color='0xff123456' name='H2OHelpTextPanel'/>"
          L"<H2OHotkeyPanel name='H2OHotkeyPanel'/>"
          L"<Texture background-image='@FormHalo' float='true' name='FormHalo' scale9grid='23,26,22,31'/>"
          L"<Texture name='overlay' float='true' visibility='false' height='-1' width='-1' background-color='0x80C8C8C8'/>"
        L"</VerticalLayout>"
      L"</VerticalLayout>"
    L"</VerticalLayout>"
  L"</Control>";
//_End_L05_GRAPHIC_UI_
//_Start_L05_GRAPHIC_UI_
//#endif
//_End_L05_GRAPHIC_UI_

EFI_STATUS
GetInformationField (
  OUT RECT                                    *InfoField
  )
{
  EFI_STATUS                                  Status;
  RECT                                        SetupPagePanelField;
  RECT                                        HelpTextPanelField;

  Status = GetRectByName (gWnd, L"H2OSetupPagePanel", &SetupPagePanelField);
  if (EFI_ERROR (Status)) {
    SetRectEmpty (&SetupPagePanelField);
  }

  Status = GetRectByName (gWnd, L"H2OHelpTextPanel", &HelpTextPanelField);
  if (EFI_ERROR (Status)) {
    SetRectEmpty (&HelpTextPanelField);
  }

  if (SetupPagePanelField.top    == HelpTextPanelField.top &&
      SetupPagePanelField.bottom == HelpTextPanelField.bottom) {
    if (SetupPagePanelField.right == HelpTextPanelField.left ||
        SetupPagePanelField.left  == HelpTextPanelField.right) {
      UnionRect (InfoField, &SetupPagePanelField, &HelpTextPanelField);
      return EFI_SUCCESS;
    }
  } else if (SetupPagePanelField.left  == HelpTextPanelField.left &&
             SetupPagePanelField.right == HelpTextPanelField.right) {
    if (SetupPagePanelField.bottom == HelpTextPanelField.top ||
        SetupPagePanelField.top    == HelpTextPanelField.bottom) {
      UnionRect (InfoField, &SetupPagePanelField, &HelpTextPanelField);
      return EFI_SUCCESS;
    }
  }

  CopyRect (InfoField, &SetupPagePanelField);
  return EFI_SUCCESS;
}

EFI_STATUS
DisableHalo (
  IN DISPLAY_ENGINE_FRAME                    *This
  )
{
  UI_MANAGER                               *Manager;
  UI_CONTROL                               *Control;

  Manager = ((UI_CONTROL *) This)->Manager;
  Control = Manager->FindControlByName (Manager, L"FormHalo");
  if (Control == NULL) {
    return EFI_NOT_FOUND;
  }
  UiSetAttribute (Control, L"visibility", L"false");

  return EFI_SUCCESS;
}

//_Start_L05_GRAPHIC_UI_
/**
  Auto adjust all panel's layout.

  @param  PanelName                     The name of the panel that need to adjust.
  @param  ScreenField                   The Field of the screen.
  @param  PanelField                    The Field of the Panel.

  @retval EFI_SUCCESS                   Auto adjust successfully.
  @retval Other                         Auto adjust failed.
**/
EFI_STATUS
AutoAdjustPanelLayout (
  IN     CHAR16                         *PanelName,
  IN     RECT                           ScreenField,
  IN OUT RECT                           *PanelField
  )
{
  BOOLEAN                               AdjustWidth;
  BOOLEAN                               AdjustHeight;
  RECT                                  TempField;

  AdjustWidth  = FALSE;
  AdjustHeight = FALSE;
  CopyRect (&TempField, PanelField);

  if (ScreenField.right != L05_GUI_RESOLUTION_BASE_X) {
    AdjustWidth = TRUE;
  }
  if (ScreenField.bottom != L05_GUI_RESOLUTION_BASE_Y) {
    AdjustHeight = TRUE;
  }

  if (!AdjustWidth && !AdjustHeight) {
    return EFI_SUCCESS;
  }

  if (StrCmp (PanelName, L"H2OSetupMenuPanel") == 0) {
    if (AdjustWidth) {
      TempField.right = (ScreenField.right * 229) / 1000; // 22.9%
      if (((ScreenField.right * 229) % 1000) != 0) {
        TempField.right++;
      }
      if (TempField.right < L05_GUI_SETUP_MENU_BASE_WIDTH) {
        TempField.right = L05_GUI_SETUP_MENU_BASE_WIDTH;
      }
    }

    if (AdjustHeight) {
      TempField.bottom = (ScreenField.bottom * 880) / 1000;
      if (ScreenField.bottom - TempField.bottom < L05_GUI_HOTKEY_BASE_HEIGHT) {
        TempField.bottom = ScreenField.bottom - L05_GUI_HOTKEY_BASE_HEIGHT;
      }
      TempField.bottom -= L05_GUI_BLUE_BAR_WIDTH;
    }
  }

  if (StrCmp (PanelName, L"H2OSetupPagePanel") == 0) {
    if (AdjustWidth) {
      TempField.left = (ScreenField.right * 229) / 1000; // 22.9%
      if (((ScreenField.right * 229) % 1000) != 0) {
        TempField.left++;
      }
      if (TempField.left < L05_GUI_SETUP_MENU_BASE_WIDTH) {
        TempField.left = L05_GUI_SETUP_MENU_BASE_WIDTH;
      }
      TempField.left += (L05_GUI_BLUE_BAR_WIDTH);
    }

    if (AdjustHeight) {
      TempField.bottom = (ScreenField.bottom * 880) / 1000; // 88%
      if (ScreenField.bottom - TempField.bottom < L05_GUI_HOTKEY_BASE_HEIGHT) {
        TempField.bottom = ScreenField.bottom - L05_GUI_HOTKEY_BASE_HEIGHT;
      }
      TempField.bottom -= (L05_GUI_BLUE_BAR_WIDTH + L05_GUI_SETUP_PAGE_PADDING_BOTTOM);
    }
  }

  if (StrCmp (PanelName, L"H2OHotkeyPanel") == 0) {
    if (AdjustHeight) {
      TempField.top = (ScreenField.bottom * 880) / 1000; // 88%
      if (((ScreenField.bottom * 880) % 1000) != 0) {
        TempField.top++;
      }
      if (TempField.bottom - TempField.top < L05_GUI_HOTKEY_BASE_HEIGHT) {
        TempField.top = TempField.bottom - L05_GUI_HOTKEY_BASE_HEIGHT;
      }
    }
  }

  CopyRect (PanelField, &TempField);

  return EFI_SUCCESS;
}
//_End_L05_GRAPHIC_UI_

EFI_STATUS
SetPanelPosFromVfcf (
  IN DISPLAY_ENGINE_FRAME       *This
  )
{
  EFI_STATUS                    Status;
  H2O_LAYOUT_INFO               *LayoutInfo;
  RECT                          ScreenField;
  RECT                          PanelField;
  UI_MANAGER                    *Manager;
  UI_CONTROL                    *Control;
  CHAR16                        String[20];
  PANEL_VFCF_DATA               *PanelVfcfDataPtr;
  H2O_PANEL_INFO                *PanelInfo;
  BOOLEAN                       IsVisible;
  UINT32                        LayoutId;
  EFI_GUID                      ScuFormSetGuid = {0x9f85453e, 0x2f03, 0x4989, 0xad, 0x3b, 0x4a, 0x84, 0x07, 0x91, 0xaf, 0x3a};
  H2O_FORM_BROWSER_SM           *SetupMenuData;
  EFI_GUID                      FormSetGuid;

  Manager = ((UI_CONTROL *) This)->Manager;

  Status = gFB->GetSMInfo (gFB, &SetupMenuData);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  CopyGuid (&FormSetGuid, &SetupMenuData->FormSetGuid);
  FreeSetupMenuData (SetupMenuData);

  LayoutInfo = NULL;

  LayoutId = 0;
  Status = EFI_NOT_FOUND;
  if (NeedShowSetupMenu ()) {
    Status = GetLayoutIdByGuid (&ScuFormSetGuid, &LayoutId);
  }
  if (EFI_ERROR (Status)) {
    Status = GetLayoutIdByGuid (&FormSetGuid, &LayoutId);
    if (EFI_ERROR (Status)) {
      Status = GetLayoutIdByGuid (&gZeroGuid, &LayoutId);
    }

    ASSERT_EFI_ERROR (Status);
  }
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = GetLayoutById (LayoutId, &mMetroPrivate->DisplayEngine.Id, &LayoutInfo);
  if (EFI_ERROR (Status)) {
    Status = GetLayoutIdByGuid (&gZeroGuid, &LayoutId);
    ASSERT_EFI_ERROR (Status);
    Status = GetLayoutById (LayoutId, &mMetroPrivate->DisplayEngine.Id, &LayoutInfo);
    ASSERT_EFI_ERROR (Status);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  mLayoutInfo = LayoutInfo;

//_Start_L05_GRAPHIC_UI_
  //DisableHalo (This);
  DisableHalo (This);
//_End_L05_GRAPHIC_UI_

  ScreenField.left   = 0;
  ScreenField.top    = 0;
  ScreenField.right  = (INT32) GetSystemMetrics (SM_CXSCREEN);
  ScreenField.bottom = (INT32) GetSystemMetrics (SM_CYSCREEN);

  PanelVfcfDataPtr = mPanelVfcfData;
  while (PanelVfcfDataPtr->PanelName != NULL) {
    IsVisible = FALSE;
    SetRectEmpty (&PanelField);

    PanelInfo = GetPanelInfo (LayoutInfo, PanelVfcfDataPtr->PanelId);
    if (PanelInfo != NULL) {
      GetPanelField (PanelInfo, NULL, H2O_IFR_STYLE_TYPE_PANEL, H2O_STYLE_PSEUDO_CLASS_NORMAL, &ScreenField, &PanelField);
      IsVisible = IsVisibility (PanelInfo, NULL, H2O_IFR_STYLE_TYPE_PANEL, H2O_STYLE_PSEUDO_CLASS_NORMAL);
    }

    Control = Manager->FindControlByName (Manager, PanelVfcfDataPtr->PanelName);

//_Start_L05_GRAPHIC_UI_
    if (!IsFrontPage ()) {
      //
      // Ajust the layout if resolution is not base resolution (1920 * 1080).
      //
      if (ScreenField.right != L05_GUI_RESOLUTION_BASE_X || ScreenField.bottom != L05_GUI_RESOLUTION_BASE_Y) {
        Status = AutoAdjustPanelLayout (PanelVfcfDataPtr->PanelName, ScreenField, &PanelField);
      }

    }
//_End_L05_GRAPHIC_UI_

    UnicodeSPrint (String, sizeof (String), L"%d,%d,%d,%d", PanelField.left, PanelField.top, PanelField.right, PanelField.bottom);
    UiSetAttribute (Control, L"pos", String);
    UiSetAttribute (Control, L"visibility", IsVisible ? L"true" : L"false");

    PanelVfcfDataPtr++;
  }

//_Start_L05_GRAPHIC_UI_
  Control = UiFindChildByName (This, L"bluebarV");
  if (Control != NULL) {
    UnicodeSPrint (String, sizeof (String), L"%d,%d,%d,%d",
      This->H2OSetupMenuPanel->FixedXY.x + This->H2OSetupMenuPanel->FixedSize.cx,
      This->H2OSetupMenuPanel->FixedXY.y,
      This->H2OSetupMenuPanel->FixedXY.x + This->H2OSetupMenuPanel->FixedSize.cx + L05_GUI_BLUE_BAR_WIDTH,
      This->H2OSetupMenuPanel->FixedXY.y + This->H2OSetupMenuPanel->FixedSize.cy
      );
    UiSetAttribute (Control, L"pos", String);
    UiSetAttribute (Control, L"visibility", (IsFrontPage () || IsL05GamingHomePage ()) ? L"false" : L"true");
  }
  Control = UiFindChildByName (This, L"bluebarH");
  if (Control != NULL) {
    UnicodeSPrint (String, sizeof (String), L"%d,%d,%d,%d",
      This->H2OHotkeyPanel->FixedXY.x + 70,
      This->H2OSetupMenuPanel->FixedXY.y + This->H2OSetupMenuPanel->FixedSize.cy,
      This->H2OHotkeyPanel->FixedXY.x + This->H2OHotkeyPanel->FixedSize.cx - 70,
      This->H2OSetupMenuPanel->FixedXY.y + This->H2OSetupMenuPanel->FixedSize.cy + L05_GUI_BLUE_BAR_WIDTH
      );
    UiSetAttribute (Control, L"pos", String);
    UiSetAttribute (Control, L"visibility", (IsFrontPage () || IsL05GamingHomePage ()) ? L"false" : L"true");
  }
//_End_L05_GRAPHIC_UI_

  return Status;
}

VOID
LocalMetroSetComponent (
  DISPLAY_ENGINE_FRAME            *This
  )
{
  UI_MANAGER  *Manager;

  Manager = ((UI_CONTROL *) This)->Manager;

  This->H2OSetupMenuPanel = Manager->FindControlByName (Manager, L"H2OSetupMenuPanel");
  This->H2OSetupPagePanel = Manager->FindControlByName (Manager, L"H2OSetupPagePanel");
  This->H2OFormTitlePanel = Manager->FindControlByName (Manager, L"H2OFormTitlePanel");
  This->H2OHelpTextPanel  = Manager->FindControlByName (Manager, L"H2OHelpTextPanel");
  This->H2OHotkeyPanel    = Manager->FindControlByName (Manager, L"H2OHotkeyPanel");
  This->H2OOwnerDrawPanel = Manager->FindControlByName (Manager, L"H2OOwnerDrawPanel");
}

VOID
LocalMetroProcessUiNotify (
  DISPLAY_ENGINE_FRAME            *This,
  UINT                          Msg,
  UI_CONTROL                    *Sender
  )
{
  H2O_STATEMENT_ID              StatementId;
  H2O_FORM_BROWSER_S            *Statement;
  HOT_KEY_INFO                  *HotkeyInfo;
  EFI_STATUS                    Status;
  EFI_HII_VALUE                 HiiValue;

  switch (Msg) {

  case UI_NOTIFY_CLICK:
  case UI_NOTIFY_CARRIAGE_RETURN:
    if (StrnCmp (Sender->Name, L"Hotkey", StrLen (L"Hotkey")) == 0) {
      HotkeyInfo = (HOT_KEY_INFO *) GetWindowLongPtr (Sender->Wnd, GWLP_USERDATA);
      HotKeyFunc (HotkeyInfo);
      break;
    }

    if (StrCmp (Sender->Name, L"CheckBox") == 0) {
      StatementId = (H2O_PAGE_ID) GetWindowLongPtr (Sender->Wnd, GWLP_USERDATA);
      Status = gFB->GetSInfo (gFB, gFB->CurrentP->PageId, StatementId, &Statement);
      ASSERT_EFI_ERROR (Status);
      if (EFI_ERROR (Status)) {
        break;
      }
      SendSelectQNotify (Statement->PageId, Statement->QuestionId, Statement->IfrOpCode);
      CopyMem (&HiiValue, &Statement->HiiValue, sizeof (EFI_HII_VALUE));
      HiiValue.Value.b = !HiiValue.Value.b;
      SendChangeQNotify (Statement->PageId, Statement->QuestionId, &HiiValue);
      break;
    }

//_Start_L05_GAMING_UI_ENABLE_
    if (FeaturePcdGet (PcdL05GamingUiSupported)) {
      Status = L05GamingMetroProcessUiNotify (Sender);
      if (!EFI_ERROR (Status)) {
        break;
      }
    }
//_End_L05_GAMING_UI_ENABLE_

    DEBUG ((EFI_D_INFO, "Unsuppoert item click: %s", Sender->Name));
    ASSERT (FALSE);
    break;
  }
}

STATIC
VOID
UpdateHaloColor (
  IN DISPLAY_ENGINE_FRAME            *This
  )
{
  UI_CONTROL                    *Control;
  HSV_VALUE                     MenuHsv;
  HSV_VALUE                     HaloHsv;
  INT16                         HueDiff;
  INT8                          SaturationDiff;
  INT8                          ValueDiff;
  CHAR16                        Str[20];

  ASSERT (This != NULL);
  GetCurrentHaloHsv (&HaloHsv);
  GetCurrentMenuHsv (&MenuHsv);
  if (MenuHsv.Hue == 0 && MenuHsv.Saturation == 0 && MenuHsv.Value == 0) {
    return;
  }
  HueDiff        = (INT16) (MenuHsv.Hue - HaloHsv.Hue);
  SaturationDiff = (INT8)  (MenuHsv.Saturation - HaloHsv.Saturation);
  ValueDiff      = (INT8)  (MenuHsv.Value - HaloHsv.Value);
  UnicodeSPrint (Str, sizeof (Str), L"%d,%d,%d",  HueDiff, SaturationDiff, ValueDiff);
  Control = UiFindChildByName (This, L"FormHalo");
  UiSetAttribute (Control, L"hsvadjust", Str);
}

EFI_STATUS
UpdateHaloPos (
  IN DISPLAY_ENGINE_FRAME                    *This
  )
{
  EFI_STATUS                               Status;
  RECT                                     Field;
  UI_MANAGER                               *Manager;
  UI_CONTROL                               *Control;
  CHAR16                                   String[20];

  Status = GetInformationField (&Field);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Manager = ((UI_CONTROL *) This)->Manager;

  Control = Manager->FindControlByName (Manager, L"FormHalo");
  if (Control == NULL) {
    return EFI_NOT_FOUND;
  }

  UnicodeSPrint (String, sizeof (String), L"%d,%d,%d,%d", Field.left, Field.top, Field.right, Field.bottom);
  UiSetAttribute (Control, L"pos", String);

  return EFI_SUCCESS;
}

STATIC
EFI_STATUS
GetContextField (
  OUT RECT                                    *ContextField
  )
{
  EFI_STATUS                                  Status;
  RECT                                        SetupMenuPanelField;
  RECT                                        FormTitlePanelField;
  RECT                                        SetupPagePanelField;
  RECT                                        HelpTextPanelField;
  RECT                                        HotkeyPanelField;

  Status = GetRectByName (gWnd, L"H2OSetupMenuPanel", &SetupMenuPanelField);
  if (EFI_ERROR (Status)) {
    SetRectEmpty (&SetupMenuPanelField);
  }

  Status = GetRectByName (gWnd, L"H2OFormTitlePanel", &FormTitlePanelField);
  if (EFI_ERROR (Status)) {
    SetRectEmpty (&FormTitlePanelField);
  }

  Status = GetRectByName (gWnd, L"H2OSetupPagePanel", &SetupPagePanelField);
  if (EFI_ERROR (Status)) {
    SetRectEmpty (&SetupPagePanelField);
  }

  Status = GetRectByName (gWnd, L"H2OHelpTextPanel", &HelpTextPanelField);
  if (EFI_ERROR (Status)) {
    SetRectEmpty (&HelpTextPanelField);
  }

  Status = GetRectByName (gWnd, L"H2OHotkeyPanel", &HotkeyPanelField);
  if (EFI_ERROR (Status)) {
    SetRectEmpty (&HotkeyPanelField);
  }

  UnionRect (ContextField, &SetupMenuPanelField, &FormTitlePanelField);
  UnionRect (ContextField, ContextField, &SetupPagePanelField);
  UnionRect (ContextField, ContextField, &HelpTextPanelField);
  UnionRect (ContextField, ContextField, &HotkeyPanelField);

  return EFI_SUCCESS;
}

STATIC
EFI_STATUS
UpdateContextPos (
  IN VOID                                  *Frame
  )
{
  EFI_STATUS                               Status;
  RECT                                     Field;
  UI_MANAGER                               *Manager;
  UI_CONTROL                               *Control;
  CHAR16                                   String[20];

  Status = GetContextField (&Field);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Manager = ((UI_CONTROL *) Frame)->Manager;

  Control = Manager->FindControlByName (Manager, L"ContextBkg");
  if (Control == NULL) {
    return EFI_NOT_FOUND;
  }

  UnicodeSPrint (String, sizeof (String), L"%d,%d,%d,%d", Field.left, Field.top, Field.right, Field.bottom);
  UiSetAttribute (Control, L"pos", String);

  return EFI_SUCCESS;
}

BOOLEAN
IsMetroPrivateHotKey (
  IN EFI_KEY_DATA               *KeyData
  )
{
  UINTN                         Index;

  if (KeyData == NULL) {
    return FALSE;
  }

  for (Index = 0; Index < sizeof (mPrivateHotKeyList) / sizeof (EFI_INPUT_KEY); Index++) {
    if (KeyData->Key.ScanCode    == mPrivateHotKeyList[Index].ScanCode &&
        KeyData->Key.UnicodeChar == mPrivateHotKeyList[Index].UnicodeChar &&
        KeyData->KeyState.KeyShiftState == EFI_SHIFT_STATE_VALID) {
      return TRUE;
    }
  }
//_Start_L05_GAMING_UI_ENABLE_
  if (FeaturePcdGet (PcdL05GamingUiSupported)) {
    //
    // When pressing ESC in any setup menu root page, it should go back to gaming main page.
    // Add private key here to prevent calling HotKeyFunc().
    //
    if (!IsL05GamingHomePage ()) {
      if (KeyData->Key.ScanCode    == SCAN_ESC &&
          KeyData->Key.UnicodeChar == CHAR_NULL &&
          KeyData->KeyState.KeyShiftState == EFI_SHIFT_STATE_VALID) {
        return TRUE;
      }
    }
  }
//_End_L05_GAMING_UI_ENABLE_

  return FALSE;
}

VOID
LocalMetroRegisterHotkey (
  HWND                          Wnd
  )
{
  INT32                         Id;

  Id = 1;
  while (UnregisterHotKey (Wnd, Id)) {
    Id++;
  }

  Id = 1;
  MwRegisterHotKey (Wnd, Id++,  0, VK_ESCAPE);
//_Start_L05_GAMING_UI_ENABLE_
  if (IsL05GamingHomePage ()) {
    MwRegisterHotKey (Wnd, Id++,  0, VK_UP);
    MwRegisterHotKey (Wnd, Id++,  0, VK_DOWN);
    MwRegisterHotKey (Wnd, Id++,  0, VK_RIGHT);
    MwRegisterHotKey (Wnd, Id++,  0, VK_LEFT);
  }
//_End_L05_GAMING_UI_ENABLE_
}

LRESULT
CALLBACK
DisplayEngineProc (
  HWND Wnd,
  UINT Msg,
  WPARAM WParam,
  LPARAM LParam
  )
{
  DISPLAY_ENGINE_FRAME            *This;
  UI_MANAGER                    *Manager;
  UI_CONTROL                    *Control;
  UINTN                         VKCode;
  HOT_KEY_INFO                  HotKeyInfo;
//_Start_L05_GRAPHIC_UI_
  CHAR16                        String[40];
  UI_CONTROL                    *ChildControl = NULL;
  SIZE                          WinSize;
  EFI_STRING                    StringPtr;
//_End_L05_GRAPHIC_UI_

  This    = (DISPLAY_ENGINE_FRAME *) GetWindowLongPtr (Wnd, 0);
  if (This == NULL && Msg != WM_CREATE && Msg != WM_NCCALCSIZE) {
    ASSERT (FALSE);
    return 0;
  }
  Control = (UI_CONTROL *) This;

  switch ( Msg ) {

  case WM_CREATE:
    This = (DISPLAY_ENGINE_FRAME *) AllocateZeroPool (sizeof (DISPLAY_ENGINE_FRAME));
    if (This != NULL) {
      CONTROL_CLASS(This) = (UI_CONTROL_CLASS *) GetClassLongPtr (Wnd, 0);
      SetWindowLongPtr (Wnd, 0, (INTN)This);
      SendMessage (Wnd, UI_NOTIFY_CREATE, WParam, LParam);
    }
    break;

  case UI_NOTIFY_CREATE:
    PARENT_CLASS_WNDPROC (CURRENT_CLASS, Wnd, Msg, WParam,LParam);
    //
    // create UiContainer as root
    //
    Manager = Control->Manager;
    SetWindowLongPtr (Wnd, GWL_EXSTYLE, GetWindowLongPtr (Control->Wnd, GWL_EXSTYLE) & (~WS_EX_NOACTIVATE));
    if (gFB->CurrentP != NULL) {
//_Start_L05_GRAPHIC_UI_
//      XmlCreateControl (mScreenPanelChilds, Control);
      if (IsBootManagerPage ()) {
        XmlCreateControl (mBootManagerPageChilds, Control);
      } else if (IsNovoMenuPage ()) {
        XmlCreateControl (mNovoMenuPageChilds, Control);
      } else {
        XmlCreateControl (mScreenPanelChilds, Control);
      }
//_End_L05_GRAPHIC_UI_
      LocalMetroSetComponent (This);
      SetPanelPosFromVfcf (This);
//_Start_L05_GRAPHIC_UI_
      if (IsBootManagerPage () || IsNovoMenuPage ()) {
        UiSetAttribute (This->H2OSetupPagePanel, L"pos", L"0,0,0,0");
        //
        // Update help string if "help" control exist.
        //
        StringPtr = HiiGetString (mHiiHandle, STRING_TOKEN (L05_STR_MOVE_SELECTION_STRING), NULL);
        ChildControl = UiFindChildByName (Control, L"help");
        if (ChildControl != NULL) {
          UnicodeSPrint (
            String,
            sizeof (String),
            StringPtr,
            0x2191,  // Upwards Arrow symbol
            0x2193   // Downwards Arrow symbol
            );
          UiSetAttribute (ChildControl, L"text", String);
        }
        FreePool(StringPtr);

        //
        // Move the window position to center of the screen
        //
        ChildControl = UiFindChildByName (Control, L"DialogPage");
        WinSize = ChildControl->FixedSize;
        UnicodeSPrint (
          String,
          sizeof (String),
          L"%d,%d,%d,%d",
          (GetSystemMetrics (SM_CXSCREEN) / 2) - (WinSize.cx / 2) - 1,
          (GetSystemMetrics (SM_CYSCREEN) / 2) - (WinSize.cy / 2),
          (GetSystemMetrics (SM_CXSCREEN) / 2) - (WinSize.cx / 2) - 1 + WinSize.cx,
          (GetSystemMetrics (SM_CYSCREEN) / 2) - (WinSize.cy / 2) + WinSize.cy
          );
        UiSetAttribute (ChildControl, L"pos", String);
      }
//_End_L05_GRAPHIC_UI_
    } else {
      XmlCreateControl (mDialogPanelChilds, Control);
    }
    break;

  case WM_SETFOCUS:
    if (gFB->CurrentP == NULL) {
      break;
    }
//_Start_L05_GAMING_UI_ENABLE_
    if (FeaturePcdGet (PcdL05GamingUiSupported)) {
      if (IsL05GamingHomePage ()) {
        SetFocus (This->H2OOwnerDrawPanel->Wnd);
        break;
      }
    }
//_End_L05_GAMING_UI_ENABLE_
    if (gLastFocus != NULL) {
      if (gLastFocus == This->H2OSetupMenuPanel->Wnd && IsWindowVisible (This->H2OSetupMenuPanel->Wnd)) {
        SetFocus (gLastFocus);
        break;
      } else if (gLastFocus == This->H2OSetupPagePanel->Wnd) {
        SetFocus (gLastFocus);
        break;
      }
    }
    if (IsWindowVisible(This->H2OSetupMenuPanel->Wnd)) {
      SetFocus (This->H2OSetupMenuPanel->Wnd);
    } else {
      SetFocus (This->H2OSetupPagePanel->Wnd);
    }
    break;

  case WM_HOTKEY:
    if (GetWindowLongPtr (Wnd, GWL_STYLE) & WS_DISABLED) {
      break;
    }

//_Start_L05_GAMING_UI_ENABLE_
    if (IsL05GamingHomePage ()) {
      This->H2OOwnerDrawPanel->Class->WndProc (This->H2OOwnerDrawPanel->Wnd, Msg, WParam, LParam);
      break;
    }
//_End_L05_GAMING_UI_ENABLE_

    VKCode = (UINTN) HIWORD(LParam);

    switch (VKCode) {

    case VK_ESCAPE:
//_Start_L05_GAMING_UI_ENABLE_
      if (FeaturePcdGet (PcdL05GamingUiSupported)) {
        //
        // When pressing ESC in any setup menu root page, it should go back to gaming main page.
        //
        if (IsRootPage ()) {
          L05GamingSwitchSettingMode ();
          break;
        }
      }
//_End_L05_GAMING_UI_ENABLE_
      ZeroMem (&HotKeyInfo, sizeof (HOT_KEY_INFO));
      HotKeyInfo.HotKeyAction = HotKeyDiscardAndExit;
      HotKeyFunc (&HotKeyInfo);
      break;
    }
    break;

  case FB_NOTIFY_SELECT_Q:
    if (gFB->CurrentP != NULL) {
      This->H2OSetupMenuPanel->Class->WndProc (This->H2OSetupMenuPanel->Wnd, Msg, WParam, LParam);
      This->H2OSetupPagePanel->Class->WndProc (This->H2OSetupPagePanel->Wnd, Msg, WParam, LParam);
      This->H2OFormTitlePanel->Class->WndProc (This->H2OFormTitlePanel->Wnd, Msg, WParam, LParam);
      This->H2OHelpTextPanel->Class->WndProc (This->H2OHelpTextPanel->Wnd, Msg, WParam, LParam);
      This->H2OHotkeyPanel->Class->WndProc (This->H2OHotkeyPanel->Wnd, Msg, WParam, LParam);
      This->H2OOwnerDrawPanel->Class->WndProc (This->H2OOwnerDrawPanel->Wnd, Msg, WParam, LParam);
    }
    break;

  case FB_NOTIFY_REPAINT:
    if (gFB->CurrentP != NULL) {
//_Start_L05_GAMING_UI_ENABLE_
      if (FeaturePcdGet (PcdL05GamingUiSupported) && !(IsFrontPage () || IsBootManagerPage () || IsNovoMenuPage ())) {
        if (IsL05GamingHomePage ()) {
          UiSetAttribute (This->H2OSetupPagePanel, L"visibility", L"false");
          UiSetAttribute (This->H2OSetupMenuPanel, L"visibility", L"false");
          UiSetAttribute (This->H2OFormTitlePanel, L"visibility", L"false");
          UiSetAttribute (This->H2OHelpTextPanel , L"visibility", L"false");
          UiSetAttribute (This->H2OHotkeyPanel   , L"visibility", L"false");
          UiSetAttribute (This->H2OOwnerDrawPanel, L"visibility", L"true");
          UnicodeSPrint (
            String,
            sizeof (String),
            L"%d,%d,%d,%d",
            0,
            0,
            GetSystemMetrics (SM_CXSCREEN),
            GetSystemMetrics (SM_CYSCREEN)
            );
          UiSetAttribute (This->H2OOwnerDrawPanel, L"pos", String);
          UiSetAttribute (UiFindChildByName (This, L"bluebarV"), L"visibility", L"false");
          UiSetAttribute (UiFindChildByName (This, L"bluebarH"), L"visibility", L"false");
        } else {
          UiSetAttribute (UiFindChildByName (This, L"background"), L"background-color", L"0xFFFFFFFF");
          UiSetAttribute (This->H2OSetupPagePanel, L"visibility", L"true");
          UiSetAttribute (This->H2OSetupMenuPanel, L"visibility", L"true");
          UiSetAttribute (This->H2OFormTitlePanel, L"visibility", L"false");
          UiSetAttribute (This->H2OHelpTextPanel , L"visibility", L"false");
          UiSetAttribute (This->H2OHotkeyPanel   , L"visibility", L"true");
          UiSetAttribute (This->H2OOwnerDrawPanel, L"visibility", L"false");
          UiSetAttribute (UiFindChildByName (This, L"bluebarV"), L"visibility", L"true");
          UiSetAttribute (UiFindChildByName (This, L"bluebarH"), L"visibility", L"true");
        }
      } else {
        UiSetAttribute (UiFindChildByName (This, L"background"), L"background-color", L"0xFFFFFFFF");
      }
//_End_L05_GAMING_UI_ENABLE_
      This->H2OSetupMenuPanel->Class->WndProc (This->H2OSetupMenuPanel->Wnd, Msg, WParam, LParam);
      This->H2OSetupPagePanel->Class->WndProc (This->H2OSetupPagePanel->Wnd, Msg, WParam, LParam);
      This->H2OFormTitlePanel->Class->WndProc (This->H2OFormTitlePanel->Wnd, Msg, WParam, LParam);
      This->H2OHelpTextPanel->Class->WndProc (This->H2OHelpTextPanel->Wnd, Msg, WParam, LParam);
      This->H2OHotkeyPanel->Class->WndProc (This->H2OHotkeyPanel->Wnd, Msg, WParam, LParam);
      This->H2OOwnerDrawPanel->Class->WndProc (This->H2OOwnerDrawPanel->Wnd, Msg, WParam, LParam);
      //UpdateHaloPos (This);
      //UpdateHaloColor (This);
      UpdateContextPos (This);
      LocalMetroRegisterHotkey (Wnd);
      SetFocus (Wnd);
    }
    break;

  case FB_NOTIFY_REFRESH_Q:
    if (gFB->CurrentP != NULL) {
      This->H2OSetupPagePanel->Class->WndProc (This->H2OSetupPagePanel->Wnd, Msg, WParam, LParam);
    }
    break;

  case UI_NOTIFY_CLICK:
  case UI_NOTIFY_CARRIAGE_RETURN:
    LocalMetroProcessUiNotify (This, Msg, (UI_CONTROL *)WParam);
    break;

  case WM_DESTROY:
    PARENT_CLASS_WNDPROC (CURRENT_CLASS, Wnd, Msg, WParam,LParam);
    break;

  case WM_CLOSE:
    DestroyWindow (Wnd);
    break;

  default:
    return PARENT_CLASS_WNDPROC (CURRENT_CLASS, Wnd, Msg, WParam,LParam);
  }
  return 0;
}

DISPLAY_ENGINE_FRAME_CLASS *
GetDisplayEngineFrameClass (
  VOID
  )
{
  if (CURRENT_CLASS != NULL) {
    return CURRENT_CLASS;
  }

  InitUiClass ((UI_CONTROL_CLASS **)&CURRENT_CLASS, sizeof (*CURRENT_CLASS), DISPLAY_ENGINE_CLASS_NAME, (UI_CONTROL_CLASS *)GetFrameClass());
  if (CURRENT_CLASS == NULL) {
    return NULL;
  }
  ((UI_CONTROL_CLASS *)CURRENT_CLASS)->WndProc = DisplayEngineProc;

  return CURRENT_CLASS;
}



UI_GET_CLASS mGetClassTable[] = {
               (UI_GET_CLASS) GetControlClass          ,
               (UI_GET_CLASS) GetTextureClass          ,

               (UI_GET_CLASS) GetLabelClass            ,
               (UI_GET_CLASS) GetButtonClass           ,
               (UI_GET_CLASS) GetSwitchClass           ,

               (UI_GET_CLASS) GetEditClass             ,
               (UI_GET_CLASS) GetScrollBarClass        ,
               (UI_GET_CLASS) GetVerticalLayoutClass   ,
               (UI_GET_CLASS) GetHorizontalLayoutClass ,

               (UI_GET_CLASS) GetListViewClass,
               (UI_GET_CLASS) GetNumberPickerClass     ,

               (UI_GET_CLASS) GetDialogClass           ,
               (UI_GET_CLASS) GetFrameClass            ,


               (UI_GET_CLASS) GetFrontPageItemClass    ,
               (UI_GET_CLASS) GetSetupPageItemClass    ,
               (UI_GET_CLASS) GetSetupMenuItemClass    ,
               (UI_GET_CLASS) GetDateItemClass         ,
               (UI_GET_CLASS) GetTimeItemClass         ,
               (UI_GET_CLASS) GetHotkeyItemClass       ,

               (UI_GET_CLASS) GetDisplayEngineFrameClass,

               (UI_GET_CLASS) GetH2OFormTitlePanelClass,
               (UI_GET_CLASS) GetH2OHotkeyPanelClass,
               (UI_GET_CLASS) GetH2OHelpTextPanelClass,
               (UI_GET_CLASS) GetH2OOwnerDrawPanelClass,
               (UI_GET_CLASS) GetH2OSetupMenuPanelClass,
               (UI_GET_CLASS) GetH2OSetupPagePanelClass,

               (UI_GET_CLASS) GetScrollViewClass,

               NULL,
            };


EFI_STATUS
InitializeGUI (
  EFI_GRAPHICS_OUTPUT_PROTOCOL  *GraphicsOutput
  )
{
  EFI_STATUS                    Status;
//_Start_L05_GRAPHIC_UI_
  BOOLEAN                       L05InitCursor;
  EFI_TPL                       Tpl;
//_End_L05_GRAPHIC_UI_

  Status = gBS->LocateProtocol (
                  &gSetupMouseProtocolGuid,
                  NULL,
                  (VOID **) &mSetupMouse
                  );
  if (EFI_ERROR (Status)) {
    mSetupMouse = NULL;
  }

  AddHiiImagePackage ();

  GdAddGopDevice (GraphicsOutput);

//_Start_L05_GRAPHIC_UI_
  //
  // SetupMouse will be started and the mouse icon will become the original icon when MwOpen() is running.
  // Raise TPL to prevent drawing the original mouse icon, and restore TPL after L05 mouse icon is initialized.
  //
  Tpl = gBS->RaiseTPL (TPL_NOTIFY);
  L05InitCursor = FALSE;
  if (!IsSetupMouseStart ()) {
    L05InitCursor = TRUE;
  }
//_End_L05_GRAPHIC_UI_

  if (MwOpen() < 0) {
    return EFI_INVALID_PARAMETER;
  }

//_Start_L05_GRAPHIC_UI_
  if (L05InitCursor) {
    L05InitCursor = FALSE;
    L05InitializeCursor ();
  }
  gBS->RestoreTPL (Tpl);
//_End_L05_GRAPHIC_UI_

  RegisterClassTable (mGetClassTable);

  return EFI_SUCCESS;
}

EFI_STATUS
InitializeWindows (
  VOID
  )
{
  MSG                                      Msg;

  if (gWnd != NULL) {
    return EFI_ALREADY_STARTED;
  }

  //
  // CreateWindow
  //
  gWnd = CreateWindowEx (
           0,
           DISPLAY_ENGINE_CLASS_NAME,
           L"Insyde H2O",
           WS_OVERLAPPED | WS_VISIBLE,
           0, 0, GetSystemMetrics (SM_CXSCREEN), GetSystemMetrics (SM_CYSCREEN),
           NULL,
           NULL,
           NULL,
           NULL
           );
  //
  // Process Message
  //
  while (PeekMessage (&Msg, NULL, 0, 0, PM_REMOVE)) {
    TranslateMessage (&Msg);
    DispatchMessage (&Msg);
  }

  return EFI_SUCCESS;
}


