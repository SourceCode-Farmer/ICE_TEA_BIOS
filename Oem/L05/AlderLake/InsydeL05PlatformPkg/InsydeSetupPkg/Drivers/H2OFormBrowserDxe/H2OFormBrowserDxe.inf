## @file
#  Component description file for H2O Form Browser driver.
#
#******************************************************************************
#* Copyright (c) 2013 - 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = H2OFormBrowserDxe
  FILE_GUID                      = 9E5DAEB4-4B91-4466-9EBE-81C7E4401E6D
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = FBEntryPoint
  STRGATHER_RESERVE_SIZE         = 0x1000

#
# The following information is for reference only and not required by the build tools.
#
#  VALID_ARCHITECTURES           = IA32 X64 EBC
#

[Sources]
  SetupBrowserStr.uni
  Setup.c
  Setup.h
  IfrParse.c
  Expression.c
  Presentation.c
  Colors.h
  H2OFormBrowser.c
  FBConsole.c
  FBEvent.c
  FBDialog.c
  FBProcessVfcf.c
  H2ODialog.c
  Value.c
  Page.c
  Statement.c
  Option.c
  FBHotPlug.c
  FBConSplitter.c
  FBTargetInfo.c
  String.c
  Uefi.c
  Link.c
  Import.c
  HiiPopup.c

[Packages]
#_Start_L05_FEATURE_
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec  ## ChipsetSetupConfig.h
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec  ## CpuLimits.h
#_End_L05_FEATURE_
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeOemServicesPkg/InsydeOemServicesPkg.dec
  InsydeSetupPkg/InsydeSetupPkg.dec

[LibraryClasses]
  UefiDriverEntryPoint
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  MemoryAllocationLib
  BaseMemoryLib
  PrintLib
  UefiLib
  DebugLib
  HiiDbLib
  HiiStringLib
  HiiConfigAccessLib
  UefiHiiServicesLib
  DxeOemSvcKernelLibDefault
  LayoutLib
  ReportStatusCodeLib
  H2OCpLib
  PcdLib
  H2ODisplayEngineLib
  CollationLib
#_Start_L05_SETUP_MENU_
  DxeOemSvcFeatureLibDefault
#_End_L05_SETUP_MENU_
#[-start-211208-JOYID00007-add]#
#[-start-211213-TAMT000038-modify]#
!if ($(C970_SUPPORT_ENABLE) == YES) OR ($(C770_SUPPORT_ENABLE) == YES) OR ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  IoLib
  GpioLib
!endif
#[-end-211213-TAMT000038-modify]#
#[-end-211208-JOYID00007-add]#

[Guids]
  gH2ODisplayEngineLocalTextGuid
  gH2ODisplayEngineRemoteTextGuid
  gH2ODisplayEngineRemoteBrowserGuid
  gH2ODisplayEngineLocalCommandLineGuid
  gH2ODisplayEngineLocalMetroGuid
  gH2ODisplayEngineNullTypeGuid
  gH2ODisplayEngineAllTypeGuid
  gH2ODisplayTypePrimaryDisplayGuid
  gEfiHiiPlatformSetupFormsetGuid
  gEfiHiiStandardFormGuid
  gEfiIfrTianoGuid
  gEfiUsbEnumerationGuid
  gFrontPageFormSetGuid
  gEfiConsoleInDeviceGuid
  gEfiConsoleOutDeviceGuid
  gReturnFromImageGuid
  gH2OStopHotKeyGuid
  gH2OSetupChangeStatusCodeGuid
  gH2OBdsCpSendFormAfterGuid
  gH2OBdsCpSendFormBeforeGuid
  gH2OBdsCpFormBrowserIdleAfterGuid
  gH2OBdsCpFormBrowserIdleBeforeGuid
  gH2OBdsCpFormBrowserUserInputGuid
  gH2OBdsCpFormBrowserCallbackAfterGuid
  gH2OBdsCpFormBrowserCallbackBeforeGuid
  gH2OBdsCpFormBrowserExtractConfigAfterGuid
  gH2OBdsCpFormBrowserExtractConfigBeforeGuid
  gH2OBdsCpFormBrowserRouteConfigAfterGuid
  gH2OBdsCpFormBrowserRouteConfigBeforeGuid
  gH2OBdsCpFormBrowserCheckPasswordGuid
  gH2OBdsCpFormBrowserUpdatePasswordDialogMessageGuid
  gZeroGuid
#_Start_L05_SETUP_MENU_
  gSystemConfigurationGuid
#_End_L05_SETUP_MENU_

[Protocols]
  gEfiDevicePathToTextProtocolGuid
  gEfiSimpleTextInProtocolGuid
  gEfiSimpleTextInputExProtocolGuid
  gEfiSimplePointerProtocolGuid
  gEfiAbsolutePointerProtocolGuid
  gEfiFormBrowser2ProtocolGuid
  gEdkiiFormBrowserEx2ProtocolGuid              ## PRODUCES
  gEfiHiiConfigAccessProtocolGuid
  gEfiUserManagerProtocolGuid
  gEfiDevicePathFromTextProtocolGuid
  gH2ODisplayEngineProtocolGuid
  gH2OFormBrowserProtocolGuid
  gH2OKeyDescProtocolGuid
  gH2ODialogProtocolGuid
  gSetupMouseProtocolGuid
  gEfiCpuArchProtocolGuid
  gH2OSubmitSvcProtocolGuid
  gH2OBdsServicesProtocolGuid
  gEfiHiiPopupProtocolGuid
#_Start_L05_SETUP_MENU_
  gEfiL05HotKeyServiceProtocolGuid
#_End_L05_SETUP_MENU_
#[-start-211208-JOYID00007-add]#
#[-start-211213-TAMT000038-modify]#
!if ($(C970_SUPPORT_ENABLE) == YES) OR ($(C770_SUPPORT_ENABLE) == YES) OR ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  gGopDisplayBrightnessProtocoGuid
!endif
#[-end-211213-TAMT000038-modify]#
#[-end-211208-JOYID00007-add]#

#_Start_L05_SETUP_MENU_
[Pcd]
  gL05ServicesTokenSpaceGuid.PcdL05PasswordErrorFlag
  #
  # BIOS Setup UI Definition
  #
  gL05ServicesTokenSpaceGuid.PcdL05BackgroundBrightFlag
  gL05ServicesTokenSpaceGuid.PcdL05SetupConfirmDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05SetupWarningDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05SetupWarningContinueDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05SetupErrorDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05SetupNoticeDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05SetupDiscardAndExitFlag
  gL05ServicesTokenSpaceGuid.PcdL05SetupAlignmentCenterFlag
#_End_L05_SETUP_MENU_
#_Start_L05_GRAPHIC_UI_
  gL05ServicesTokenSpaceGuid.PcdL05SetupSucessDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05SetupFailedDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05GamingSetupExitDialogFlag
#_End_L05_GRAPHIC_UI_
#_Start_L05_OVERCLOCK_UI_
  gL05ServicesTokenSpaceGuid.PcdL05GamingOverClockDialogFlag
#_End_L05_OVERCLOCK_UI_
#_Start_L05_SECURE_SUITE_
  gL05ServicesTokenSpaceGuid.PcdL05SetupNoticeDialogGrayFlag
#_End_L05_SECURE_SUITE_
#_Start_L05_NATURAL_FILE_GUARD_ENABLE_
  gL05ServicesTokenSpaceGuid.PcdL05NaturalFileGuardDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05NaturalFileGuardPressEscFlag
#_End_L05_NATURAL_FILE_GUARD_ENABLE_
#_Start_L05_NOTEBOOK_PASSWORD_V1_1_ENABLE_
  gL05ServicesTokenSpaceGuid.PcdL05NotebookPasswordDesignDialogF1Flag
  gL05ServicesTokenSpaceGuid.PcdL05NotebookPasswordDesignPressF1Flag
  gL05ServicesTokenSpaceGuid.PcdL05NotebookPasswordDesignDialogF1String
  gL05ServicesTokenSpaceGuid.PcdL05DialogNoTitleStringFlag
  gL05ServicesTokenSpaceGuid.PcdL05ShowDialogTitleStringFlag
  gL05ServicesTokenSpaceGuid.PcdL05HddPaswordTitleString
#_End_L05_NOTEBOOK_PASSWORD_V1_1_ENABLE_
#_Start_L05_SECURITY_ERASE_ENABLE_
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseProcessingDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseProcessingDialogExit
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseSuccessfullyDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseFailedDialogFlag
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseTimeMicrosecond
#_End_L05_SECURITY_ERASE_ENABLE_

[FeaturePcd]
  gEfiMdeModulePkgTokenSpaceGuid.PcdConOutGopSupport
  gEfiMdeModulePkgTokenSpaceGuid.PcdConOutUgaSupport
  gEfiMdePkgTokenSpaceGuid.PcdUgaConsumeSupport
  gInsydeTokenSpaceGuid.PcdH2OSetupChangeDisplaySupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpSendFormAfterSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpSendFormBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserIdleAfterSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserIdleBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserUserInputSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserCallbackAfterSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserCallbackBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserExtractConfigAfterSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserExtractConfigBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserRouteConfigAfterSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserRouteConfigBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserCheckPasswordSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpFormBrowserUpdatePasswordDialogMessageSupported
#_Start_L05_GRAPHIC_UI_
  gInsydeTokenSpaceGuid.PcdH2OFormBrowserLocalMetroDESupported
  gL05ServicesTokenSpaceGuid.PcdL05GamingUiSupported
#_End_L05_GRAPHIC_UI_

[FixedPcd]
  gH2OSetupTokenSpaceGuid.PcdDisplayEngineIgfx
  gH2OSetupTokenSpaceGuid.PcdDisplayEnginePeg
  gH2OSetupTokenSpaceGuid.PcdDisplayEnginePci
#_Start_L05_SETUP_MENU_
  gInsydeTokenSpaceGuid.PcdH2OHddPasswordMinLength
#_End_L05_SETUP_MENU_

[Depex]
  gEfiHiiDatabaseProtocolGuid AND gEfiHiiConfigRoutingProtocolGuid

