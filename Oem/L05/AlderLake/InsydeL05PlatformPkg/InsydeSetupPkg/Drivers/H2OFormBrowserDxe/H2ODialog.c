/** @file
 The H2O_DIALOG_PROTOCOL is the interface to the EFI Configuration Driver.

;******************************************************************************
;* Copyright (c) 2013 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************

*/
#include "InternalH2OFormBrowser.h"

//_Start_L05_SETUP_MENU_
extern UINT16  mL05HddPasswordDialogFlag;
//_End_L05_SETUP_MENU_

/**
 Display Dialog for User Requirement which dilag type is Yes/No, Yes/No/Cancel, Ok or Ok/Cancel

 @param[in]  DialogOperation           Dialog type (Yes/No, Yes/No/Cancel, Ok or Ok/Cancel)
 @param[in]  HotKey                    HotKey information
 @param[in]  MaximumStringSize         Maximum string length
 @param[out] StringBuffer              String buffer
 @param[out] KeyValue               Ptr to returned structure that indicates the key the user selected.
 @param[in]  String                 Ptr to null-terminated string that specifies the dialog prompt

 @retval EFI_SUCCESS                Process successfully.
**/
EFI_STATUS
EFIAPI
FBConfirmDialog (
  IN  UINT32                           DialogOperation,
  IN  BOOLEAN                          HotKey,
  IN  UINT32                           MaximumStringSize,
  OUT CHAR16                          *StringBuffer,
  OUT EFI_INPUT_KEY                   *KeyValue,
  IN  CHAR16                          *String,
  ...
  )
{
  EFI_STATUS                          Status;

//ButtonStringArray and ButtonHiiValueArray use {YES,NO,CANCEL,NULL}
  EFI_STRING                          ButtonStringArray[4];
  EFI_HII_VALUE                       ButtonHiiValueArray[4];
  H2O_FORM_BROWSER_D                  Dialog;
  UINT32                              ButtonCount;
  BOOLEAN                             ConsoleInitComplete;
  H2O_FORM_BROWSER_PRIVATE_DATA       *Private;
  UINTN                               Index;

  ZeroMem (&Dialog, sizeof (H2O_FORM_BROWSER_D));

  ConsoleInitComplete = FALSE;
  Private = NULL;
  FBIsConsoleInit (&ConsoleInitComplete, &Private);
  if (!ConsoleInitComplete) {
    Status = FBInitConsoles (Private);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  Status = EFI_SUCCESS;

  switch (DialogOperation) {

//_Start_L05_SETUP_MENU_
  case L05WarningDlgYesNo:
    PcdSetBoolS (PcdL05SetupWarningDialogFlag, TRUE);

  case L05ExitDiscardWarningDlgYesNo:
  case L05SecurityEraseSuccessfullyDlg:
  case L05SecurityEraseFailedDlg:
//_End_L05_SETUP_MENU_
  case L05OverclockDlgApplyCancel:

  case DlgYesNo:
    ButtonCount          = 2;

//_Start_L05_SETUP_MENU_
    switch (DialogOperation) {
    case L05SecurityEraseSuccessfullyDlg:
      PcdSetBoolS (PcdL05SecurityEraseDialogFlag, TRUE);
      PcdSetBoolS (PcdL05SecurityEraseSuccessfullyDialogFlag, TRUE);
      ButtonStringArray[0] = GetString (STRING_TOKEN (L05_STR_RETURN_STRING), mHiiHandle);
      ButtonStringArray[1] = GetString (STRING_TOKEN (L05_STR_REBOOT_STRING), mHiiHandle);
      break;

    case L05SecurityEraseFailedDlg:
      PcdSetBoolS (PcdL05SecurityEraseDialogFlag, TRUE);
      PcdSetBoolS (PcdL05SecurityEraseFailedDialogFlag, TRUE);
      ButtonStringArray[0] = GetString (STRING_TOKEN (L05_STR_TRY_AGAIN_STRING), mHiiHandle);
      ButtonStringArray[1] = GetString (STRING_TOKEN (L05_STR_REBOOT_STRING), mHiiHandle);
      break;

    case L05OverclockDlgApplyCancel:
      ButtonStringArray[0] = GetString (STRING_TOKEN (L05_STR_APPLY), mHiiHandle);
      ButtonStringArray[1] = GetString (STRING_TOKEN (L05_STR_CANCEL), mHiiHandle);
      break;

    default:
//_End_L05_SETUP_MENU_
    ButtonStringArray[0] = GetString (STRING_TOKEN (SCU_STR_YES_TEXT), mHiiHandle);
    ButtonStringArray[1] = GetString (STRING_TOKEN (SCU_STR_NO_TEXT), mHiiHandle);
//_Start_L05_SETUP_MENU_
    }
//_End_L05_SETUP_MENU_

    CreateValueAsUint64 (ButtonHiiValueArray    , ButtonActionYes);
    CreateValueAsUint64 (ButtonHiiValueArray + 1, ButtonActionNo);

//_Start_L05_SETUP_MENU_
    if (!PcdGetBool (PcdL05SetupWarningDialogFlag)) {
      PcdSetBoolS (PcdL05SetupConfirmDialogFlag, TRUE);
    }
//_End_L05_SETUP_MENU_
    break;

  case DlgYesNoCancel:
//_Start_L05_GAMING_UI_ENABLE_
  case L05GamingExitDiscardDlgYesNoCancel:
//_End_L05_GAMING_UI_ENABLE_
    ButtonCount          = 3;
//_Start_L05_GAMING_UI_ENABLE_
    if (DialogOperation == L05GamingExitDiscardDlgYesNoCancel) {
      ButtonStringArray[0] = GetString (STRING_TOKEN (L05_STR_GAMING_EXIT_DIALOG_SAVING), mHiiHandle);
      ButtonStringArray[1] = GetString (STRING_TOKEN (L05_STR_GAMING_EXIT_DIALOG_DISCARDING), mHiiHandle);
    } else {
//_End_L05_GAMING_UI_ENABLE_
    ButtonStringArray[0] = GetString (STRING_TOKEN (SCU_STR_YES_TEXT), mHiiHandle);
    ButtonStringArray[1] = GetString (STRING_TOKEN (SCU_STR_NO_TEXT), mHiiHandle);
//_Start_L05_GAMING_UI_ENABLE_
    }
//_End_L05_GAMING_UI_ENABLE_
    ButtonStringArray[2] = GetString (STRING_TOKEN (SCU_STR_CANCEL_TEXT), mHiiHandle);
    CreateValueAsUint64 (ButtonHiiValueArray    , ButtonActionYes);
    CreateValueAsUint64 (ButtonHiiValueArray + 1, ButtonActionNo);
    CreateValueAsUint64 (ButtonHiiValueArray + 2, ButtonActionCancel);
    break;

//_Start_L05_SETUP_MENU_
  case L05WarningDlgContinueDlg:
    ButtonCount          = 1;
    ButtonStringArray[0] = GetString (STRING_TOKEN (L05_STR_CONTINUE), mHiiHandle);
    CreateValueAsUint64 (ButtonHiiValueArray, ButtonActionYes);
    PcdSetBoolS (PcdL05SetupWarningDialogFlag, TRUE);
    break;
//_End_L05_SETUP_MENU_

  case DlgOk:
    ButtonCount          = 1;
    ButtonStringArray[0] = GetString (STRING_TOKEN (SCU_STR_OK_TEXT), mHiiHandle);
    CreateValueAsUint64 (ButtonHiiValueArray, ButtonActionYes);

//_Start_L05_SETUP_MENU_
//[-start-220125-BAIN000092-modify]#
#ifndef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
    PcdSetBoolS (PcdL05SetupNoticeDialogFlag, TRUE);
#else
//#ifdef L05_NOTEBOOK_PASSWORD_ENABLE
//[-end-220125-BAIN000092-modify]#
    //
    // [Lenovo Notebook Password Design Spec V1.1]
    //   3.1.2. HDP Set ting Flow
    //     Figure 3-5 First, set Master Password
    //     Figure 3-7 Second , set User Password
    //
    PcdSetBoolS (PcdL05SetupConfirmDialogFlag, TRUE);
#endif
//_End_L05_SETUP_MENU_
    break;

  case DlgOkCancel:
    ButtonCount          = 2;
    ButtonStringArray[0] = GetString (STRING_TOKEN (SCU_STR_OK_TEXT), mHiiHandle);
    ButtonStringArray[1] = GetString (STRING_TOKEN (SCU_STR_CANCEL_TEXT), mHiiHandle);
    CreateValueAsUint64 (ButtonHiiValueArray    , ButtonActionYes);
    CreateValueAsUint64 (ButtonHiiValueArray + 1, ButtonActionCancel);
    break;

  default:
    DEBUG ((EFI_D_INFO, "DialogOperation is error"));
    ASSERT (FALSE);
    return EFI_UNSUPPORTED;
  }
  CreateValueAsUint64 (&Dialog.ConfirmHiiValue, ButtonActionYes);

  Status = CreateNewDialog (
             H2O_FORM_BROWSER_D_TYPE_SELECTION | H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG,
             0,
             NULL,
             1,
             0,
             ButtonCount,
             &String,
             NULL,
             ButtonStringArray,
             NULL,
             ButtonHiiValueArray,
             &Dialog
             );
  if (!EFI_ERROR (Status) && Dialog.ConfirmHiiValue.Value.u8 == ButtonActionYes) {
    KeyValue->ScanCode    = SCAN_NULL;
    KeyValue->UnicodeChar = CHAR_CARRIAGE_RETURN;
  } else {
    KeyValue->ScanCode    = SCAN_ESC;
    KeyValue->UnicodeChar = CHAR_NULL;
  }

//_Start_L05_SETUP_MENU_
  if (DialogOperation == L05ExitDiscardWarningDlgYesNo || 
      DialogOperation == L05GamingExitDiscardDlgYesNoCancel || 
      DialogOperation == L05OverclockDlgApplyCancel) {
    if (!EFI_ERROR (Status) && Dialog.ConfirmHiiValue.Value.u8 == ButtonActionYes) {
      KeyValue->ScanCode    = SCAN_NULL;
      KeyValue->UnicodeChar = L'Y';

    } else if (!EFI_ERROR (Status) && Dialog.ConfirmHiiValue.Value.u8 == ButtonActionNo) {
      KeyValue->ScanCode    = SCAN_NULL;
      KeyValue->UnicodeChar = L'N';

    } else {
      KeyValue->ScanCode    = SCAN_ESC;
      KeyValue->UnicodeChar = CHAR_NULL;
    }
  }
//_End_L05_SETUP_MENU_

  if (!ConsoleInitComplete) {
    FBDetachConsoles (Private);
  }

  for (Index = 0; Index < ButtonCount; Index++) {
    FreePool (ButtonStringArray[Index]);
  }

  return Status;
}

/**
 Display Dialog for User Requirement

 @param[in]  HotKey                 HotKey information
 @param[in]  MaximumStringSize      Maximum string size
 @param[out] UserInputStringBuffer  User input string buffer
 @param[out] KeyValue               Output key value which stand for user selection
 @param[in]  String                 Dialog question string

 @retval EFI_SUCCESS                Process successfully.
**/
EFI_STATUS
EFIAPI
FBPasswordDialog (
  IN  UINT32                                   NumberOfLines,
  IN  BOOLEAN                                 HotKey,
  IN  UINT32                                  MaximumStringSize,
  OUT CHAR16                                  *UserInputStringBuffer,
  OUT EFI_INPUT_KEY                           *KeyValue,
  IN  CHAR16                                  *TitleString,
  ...
  )
{
  EFI_STATUS                                  Status;
  H2O_FORM_BROWSER_PRIVATE_DATA               *Private;
  BOOLEAN                                     ConsoleInitComplete;
  H2O_FORM_BROWSER_D                          Dialog;
  CHAR16                                      **BodyInputStringArray;
//_Start_L05_SETUP_MENU_
  UINT32                                      BodyStringCount;
  CHAR16                                      **BodyStrArray;
//_End_L05_SETUP_MENU_

  if ((MaximumStringSize == 0) || (UserInputStringBuffer == NULL)) {
    return EFI_SUCCESS;
  }

  Private = &mFBPrivate;

  ConsoleInitComplete = Private->ConsoleInitComplete;
  if (!ConsoleInitComplete) {
    Status = FBInitConsoles (Private);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  ZeroMem (&Dialog, sizeof (H2O_FORM_BROWSER_D));

//_Start_L05_SETUP_MENU_
  BodyStringCount = 1;
//[-start-220125-BAIN000092-modify]#
#ifdef L05_NOTEBOOK_PASSWORD_V1_1_ENABLE
//#ifdef L05_NOTEBOOK_PASSWORD_ENABLE
//[-end-220125-BAIN000092-modify]#
  //
  // [Lenovo Notebook Password Design Spec V1.1]
  //   4.1.1. Check HDP
  //     User can press F1 to switch Master Password and User Password.
  //     Figure 4-1 Check Master Password
  //     Figure 4-2 Check User Password
  //     prompt - "Press F1 to switch between Master Password and User Password"
  //
  if (FeaturePcdGet (PcdL05NotebookPasswordDesignDialogF1Flag)) {
    BodyStringCount = 2;
  }
#endif

  BodyStrArray = AllocatePool (sizeof (CHAR16 *) * 2);
  if (BodyStrArray == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  BodyStrArray[0] = L"";
  BodyStrArray[1] = L"";
//_End_L05_SETUP_MENU_

  BodyInputStringArray = AllocatePool (sizeof (CHAR16 *));
  if (BodyInputStringArray == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  BodyInputStringArray[0] = L"";

  CreateValueAsString (&Dialog.ConfirmHiiValue, (UINT16) MaximumStringSize * sizeof (CHAR16), (UINT8 *)L"");

  Status = CreateNewDialog (
             (H2O_FORM_BROWSER_D_TYPE_PASSWORD << 16) | H2O_FORM_BROWSER_D_TYPE_SELECTION | H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG,
             0,
             TitleString,
//_Start_L05_SETUP_MENU_
//             0,
             BodyStringCount,
//_End_L05_SETUP_MENU_
             1,
             0,
//_Start_L05_SETUP_MENU_
//             NULL,
             BodyStrArray,
//_End_L05_SETUP_MENU_
             BodyInputStringArray,
             NULL,
             NULL,
             NULL,
             &Dialog
             );
  if (!EFI_ERROR (Status)) {
    ZeroMem (UserInputStringBuffer, sizeof (CHAR16) * MaximumStringSize);
    CopyMem (UserInputStringBuffer, Dialog.ConfirmHiiValue.Buffer, StrSize ((CHAR16 *) Dialog.ConfirmHiiValue.Buffer));
    FreePool (Dialog.ConfirmHiiValue.Buffer);
    KeyValue->ScanCode    = SCAN_NULL;
    KeyValue->UnicodeChar = CHAR_CARRIAGE_RETURN;
  } else if (Status == EFI_NOT_READY) {
    //
    // Shut Dialog
    //
    KeyValue->ScanCode    = SCAN_ESC;
    KeyValue->UnicodeChar = CHAR_NULL;
    Status = EFI_SUCCESS;
  }

//_Start_L05_NOTEBOOK_PASSWORD_V1_1_ENABLE_
  //
  // [Lenovo Notebook Password Design Spec V1.1]
  //   4.1.1. Check HDP
  //     User can press F1 to switch Master Password and User Password.
  //
  if (PcdGetBool (PcdL05NotebookPasswordDesignPressF1Flag)) {
    KeyValue->ScanCode    = SCAN_F1;
    KeyValue->UnicodeChar = CHAR_NULL;

    PcdSetBoolS (PcdL05NotebookPasswordDesignPressF1Flag, FALSE);

    Status = EFI_SUCCESS;
  }
//_End_L05_NOTEBOOK_PASSWORD_V1_1_ENABLE_

  if (!ConsoleInitComplete) {
    FBDetachConsoles (Private);
  }

  return Status;
}
/**
 Draw select dialog and wait user select item.

 @param[in]  NumberOfLines          Number of display item
 @param[in]  HotKey                 If TRUE need check assign key list
                                    If FALSE use function default key event
 @param[in]  KeyList                Assign key list
 @param[out] EventKey               User input string buffer
 @param[in]  MaximumStringSize      Maximum string length
 @param[in]  TitleString            Title string
 @param[in]  SelectIndex            The select index number
 @param[in]  String                 The address array of string
 @param[in]  Color                  Set display color

 @retval EFI_SUCCESS                Process successfully.
**/
EFI_STATUS
EFIAPI
FBOneOfOptionDialog (
  IN  UINT32                           NumberOfLines,
  IN  BOOLEAN                         HotKey,
  IN  CONST EFI_INPUT_KEY             *KeyList,
  OUT EFI_INPUT_KEY                   *EventKey,
  IN  UINT32                           MaximumStringSize,
  IN  CHAR16                          *TitleString,
  OUT UINT32                          *SelectIndex,
  IN  CHAR16                          **String,
  IN  UINT32                           Color
  )
{
  EFI_STATUS                          Status;
  H2O_FORM_BROWSER_D                  Dialog;
  UINT32                               Index;
  EFI_HII_VALUE                       *HiiValueArray;
  H2O_FORM_BROWSER_PRIVATE_DATA       *Private;
  BOOLEAN                             ConsoleInitComplete;


  Private = &mFBPrivate;

  ZeroMem (&Dialog, sizeof (H2O_FORM_BROWSER_D));

  if (NumberOfLines == 0) {
    return EFI_INVALID_PARAMETER;
  }

  ConsoleInitComplete = Private->ConsoleInitComplete;
  if (!ConsoleInitComplete) {
    Status = FBInitConsoles (Private);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }


  HiiValueArray = AllocateZeroPool (sizeof (EFI_HII_VALUE) * NumberOfLines);
  if (HiiValueArray == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  for (Index = 0; Index < NumberOfLines; Index++) {

    CreateValueAsUint64 (HiiValueArray+Index, Index);
  }

  Index = (*SelectIndex < NumberOfLines) ? *SelectIndex : 0;
  CopyMem (&Dialog.ConfirmHiiValue, &HiiValueArray[Index], sizeof (EFI_HII_VALUE));
  Status = CreateNewDialog (
             (H2O_FORM_BROWSER_D_TYPE_ONE_OF << 16) | H2O_FORM_BROWSER_D_TYPE_BODY_SELECTABLE | H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG,
             0,
             TitleString,
             (UINT32) NumberOfLines,
             0,
             0,
             String,
             NULL,
             NULL,
             HiiValueArray,
             NULL,
             &Dialog
             );

  if (!EFI_ERROR (Status)) {
    *SelectIndex = (UINT32) (UINTN)Dialog.ConfirmHiiValue.Value.u64;
    EventKey->ScanCode    = SCAN_NULL;
    EventKey->UnicodeChar = CHAR_CARRIAGE_RETURN;
  } else {
    EventKey->ScanCode    = SCAN_ESC;
    EventKey->UnicodeChar = CHAR_NULL;
  }

  if (!ConsoleInitComplete) {
    FBDetachConsoles (Private);
  }

  FreePool (HiiValueArray);

  return EFI_SUCCESS;
}
/**
 To show message dialog with title.

 @param[in] RequestedWidth      Dialog Width.
 @param[in] NumberOfLines       Number of item.
 @param[in] ArrayOfStrings      Message strings.

 @retval  EFI_SUCCESS           The function completed successfully.
**/
EFI_STATUS
EFIAPI
FBCreateMsgPopUp (
  IN  UINT32                       RequestedWidth,
  IN  UINT32                       NumberOfLines,
  IN  CHAR16                      *ArrayOfStrings,
  ...
  )
{
  H2O_FORM_BROWSER_D Dialog;
  EFI_STATUS         Status;
  CHAR16                              **MsgString;
  H2O_FORM_BROWSER_PRIVATE_DATA       *Private;
  BOOLEAN                             ConsoleInitComplete;


  Private = &mFBPrivate;
  MsgString = &ArrayOfStrings;

  ConsoleInitComplete = Private->ConsoleInitComplete;
  if (!ConsoleInitComplete) {
    Status = FBInitConsoles (Private);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  ZeroMem (&Dialog, sizeof (H2O_FORM_BROWSER_D));
  Status = CreateSimpleDialog (
             H2O_FORM_BROWSER_D_TYPE_MSG | H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG,
             0,
             MsgString[0],
             (UINT32)(NumberOfLines - 1),
             &(MsgString[1]),
             0,
             &Dialog
             );

//_Start_L05_SETUP_MENU_
  if (PcdGetBool (PcdL05PasswordErrorFlag)) {
    gBS->RaiseTPL (TPL_HIGH_LEVEL);
  }
//_End_L05_SETUP_MENU_

//_Start_L05_NATURAL_FILE_GUARD_ENABLE_
  if (!PcdGetBool (PcdL05NaturalFileGuardDialogFlag)) {
  //
  // [Natural File Guard Design Guide V1.01]
  //   2.3 Boot Flow
  //     2. Display prompt
  //
//_End_L05_NATURAL_FILE_GUARD_ENABLE_

  CpuDeadLoop ();

//_Start_L05_NATURAL_FILE_GUARD_ENABLE_
  }
//_End_L05_NATURAL_FILE_GUARD_ENABLE_

  if (!ConsoleInitComplete) {
    FBDetachConsoles (Private);
  }

  return EFI_SUCCESS;
}

/**
 Display page information

 @param[in] TitleString         Ptr to the dialog title string
 @param[in] TokenHandle         HII handle that specifies the information string handle
 @param[in] NumOfInfoStrings    Number of the information strings
 @param[in] InfoStrings         Unsigned integer that specifies the number of information strings in InfoStrings
 @return EFI_INVALID_PARAMETER  Invalid parameter
 @return EFI_OUT_OF_RESOURCES   BodyStringArray resource is not enough
 @return EFI_SUCCESS            Display page information success
**/
EFI_STATUS
EFIAPI
FBShowPageInfo (
  IN   CHAR16          *TitleString,
  IN   CHAR16          *InfoStrings
  )
{
  EFI_STATUS                                  Status;
  UINT32                                      DialogType;
  UINT32                                      ButtonCount;
  H2O_FORM_BROWSER_D                          Dialog;
  UINT32                                      Attribute;
  H2O_FORM_BROWSER_PRIVATE_DATA               *Private;
  BOOLEAN                                     ConsoleInitComplete;

  if (TitleString == NULL || InfoStrings == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Private = &mFBPrivate;
  ConsoleInitComplete = Private->ConsoleInitComplete;
  if (!ConsoleInitComplete) {
    Status = FBInitConsoles (Private);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  DialogType = H2O_FORM_BROWSER_D_TYPE_MSG | H2O_FORM_BROWSER_D_TYPE_SHOW_HELP;
  ButtonCount = 0;
  Attribute = 0;
  ZeroMem (&Dialog, sizeof (H2O_FORM_BROWSER_D));

  Status = CreateSimpleDialog (
             DialogType,
             Attribute,
             TitleString,
             1,
             &InfoStrings,
             ButtonCount,
             &Dialog
             );

  if (!ConsoleInitComplete) {
    FBDetachConsoles (Private);
  }

  return Status;
}

/**
 Display numeric dialog for user input numeric value

 @param[in]  TitleString         The dialog title string
 @param[in]  Minimum             Minimum value of output numeric value
 @param[in]  Maximum             Maximum value of output numeric value
 @param[in]  Step                Step value of numeric value
 @param[in]  IsHex               Flag to determine if the value is hexadecimal or not
 @param[out] Step                Output numeric value

 @retval EFI_SUCCESS             Get user input value successfully
 @retval EFI_INVALID_PARAMETER   TitleString or NumericValue is NULL
 @retval EFI_OUT_OF_RESOURCES    Allocate memory fail
**/
EFI_STATUS
EFIAPI
FBNumericDialog (
  IN  CHAR16                          *TitleString,
  IN  UINT64                          Minimum,
  IN  UINT64                          Maximum,
  IN  UINT64                          Step,
  IN  BOOLEAN                         IsHex,
  OUT UINT64                          *NumericValue
  )
{
  EFI_STATUS                          Status;
  H2O_FORM_BROWSER_D                  Dialog;
  CHAR16                              *BodyInputString;
  CHAR16                              **ButtonStringArray;
  EFI_HII_VALUE                       *ButtonHiiValueArray;
  H2O_FORM_BROWSER_S                  H2OStatement;
  H2O_FORM_BROWSER_PRIVATE_DATA       *Private;
  BOOLEAN                             ConsoleInitComplete;

  if (TitleString == NULL || NumericValue == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Private = &mFBPrivate;
  ConsoleInitComplete = Private->ConsoleInitComplete;
  if (!ConsoleInitComplete) {
    Status = FBInitConsoles (Private);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  ZeroMem (&Dialog, sizeof (H2O_FORM_BROWSER_D));
  ZeroMem (&H2OStatement, sizeof (H2OStatement));

  H2OStatement.Minimum = Minimum;
  H2OStatement.Maximum = Maximum;
  H2OStatement.Step    = Step;
  H2OStatement.Flags   = IsHex ? EFI_IFR_DISPLAY_UINT_HEX : EFI_IFR_DISPLAY_UINT_DEC;

  Dialog.H2OStatement         = &H2OStatement;
  Dialog.ConfirmHiiValue.Type = EFI_IFR_TYPE_NUM_SIZE_64;

  ButtonStringArray = AllocatePool (sizeof (CHAR16 *) * 2);
  if (ButtonStringArray == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  ButtonStringArray[0] = GetString (STRING_TOKEN (SCU_STR_YES_TEXT), mHiiHandle);
  ButtonStringArray[1] = GetString (STRING_TOKEN (SCU_STR_NO_TEXT), mHiiHandle);

  ButtonHiiValueArray = AllocateZeroPool (sizeof (EFI_HII_VALUE) * 2);
  if (ButtonHiiValueArray == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  CreateValueAsBoolean (ButtonHiiValueArray    , TRUE);
  CreateValueAsBoolean (ButtonHiiValueArray + 1, FALSE);

  BodyInputString = L"";

  Status = CreateNewDialog (
             (H2O_FORM_BROWSER_D_TYPE_NUMERIC << 16) | H2O_FORM_BROWSER_D_TYPE_SELECTION | H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG,
             0,
             TitleString,
             0,
             1,
             2,
             NULL,
             &BodyInputString,
             ButtonStringArray,
             NULL,
             ButtonHiiValueArray,
             &Dialog
             );
  if (!EFI_ERROR (Status)) {
    *NumericValue = Dialog.ConfirmHiiValue.Value.u64;
   } else {
    Status = EFI_ABORTED;
  }

  FreePool (ButtonStringArray[0]);
  FreePool (ButtonStringArray[1]);
  FreePool (ButtonStringArray);
  FreePool (ButtonHiiValueArray);

  if (!ConsoleInitComplete) {
    FBDetachConsoles (Private);
  }

  return Status;
}

/**
 Display page Dialog for User Requirement which dialog type are Yes/No, Yes/No/Cancel, Ok or Ok/Cancel

 @param[in]  DialogOperation     Dialog type (Yes/No, Yes/No/Cancel, Ok or Ok/Cancel)
 @param[in]  TitleString         Ptr to the dialog title string.
 @param[in]  ConfirmStrings      Ptr to the confirm message for dialog type are Yes/No, Yes/No/Cancel, Ok or Ok/Cancel.
 @param[in]  ShowStringBuffer    Ptr to the string buffer which will be shown in the page.
 @param[out] KeyValue            Ptr to returned structure that indicates the key the user selected.

 @return EFI_INVALID_PARAMETER  Invalid parameter
 @return EFI_OUT_OF_RESOURCES   BodyStringArray resource is not enough
 @return EFI_SUCCESS            Display page information success
**/
EFI_STATUS
EFIAPI
FBConfirmPageDialog (
  IN   UINT32          DialogOperation,
  IN   CHAR16          *TitleString,
  IN   CHAR16          *ConfirmStrings,
  IN   CHAR16          *ShowStringBuffer,
  OUT  EFI_INPUT_KEY   *KeyValue
  )
{
  EFI_STATUS                          Status;
  H2O_FORM_BROWSER_D                  Dialog;
  UINT32                              DialogType;
  UINT32                              ButtonCount;
  EFI_STRING                          ButtonStringArray[4];
  EFI_HII_VALUE                       ButtonHiiValueArray[4];
  H2O_FORM_BROWSER_PRIVATE_DATA       *Private;
  BOOLEAN                             ConsoleInitComplete;
  UINTN                               Index;
  CHAR16                              *BodyStringArray[2];

  ZeroMem (&Dialog, sizeof (H2O_FORM_BROWSER_D));

  ConsoleInitComplete = FALSE;
  Private = NULL;
  FBIsConsoleInit (&ConsoleInitComplete, &Private);
  if (!ConsoleInitComplete) {
    Status = FBInitConsoles (Private);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  BodyStringArray[0] = ConfirmStrings;
  BodyStringArray[1] = ShowStringBuffer;

  switch (DialogOperation) {

  case DlgYesNo:
    ButtonCount          = 2;
    ButtonStringArray[0] = GetString (STRING_TOKEN (SCU_STR_YES_TEXT), mHiiHandle);
    ButtonStringArray[1] = GetString (STRING_TOKEN (SCU_STR_NO_TEXT), mHiiHandle);

    CreateValueAsUint64 (ButtonHiiValueArray    , ButtonActionYes);
    CreateValueAsUint64 (ButtonHiiValueArray + 1, ButtonActionNo);
    break;

  case DlgYesNoCancel:
    ButtonCount          = 3;
    ButtonStringArray[0] = GetString (STRING_TOKEN (SCU_STR_YES_TEXT), mHiiHandle);
    ButtonStringArray[1] = GetString (STRING_TOKEN (SCU_STR_NO_TEXT), mHiiHandle);
    ButtonStringArray[2] = GetString (STRING_TOKEN (SCU_STR_CANCEL_TEXT), mHiiHandle);
    CreateValueAsUint64 (ButtonHiiValueArray    , ButtonActionYes);
    CreateValueAsUint64 (ButtonHiiValueArray + 1, ButtonActionNo);
    CreateValueAsUint64 (ButtonHiiValueArray + 2, ButtonActionCancel);
    break;

  case DlgOk:
    ButtonCount          = 1;
    ButtonStringArray[0] = GetString (STRING_TOKEN (SCU_STR_OK_TEXT), mHiiHandle);
    CreateValueAsUint64 (ButtonHiiValueArray, ButtonActionYes);
    break;

  case DlgOkCancel:
    ButtonCount          = 2;
    ButtonStringArray[0] = GetString (STRING_TOKEN (SCU_STR_OK_TEXT), mHiiHandle);
    ButtonStringArray[1] = GetString (STRING_TOKEN (SCU_STR_CANCEL_TEXT), mHiiHandle);
    CreateValueAsUint64 (ButtonHiiValueArray    , ButtonActionYes);
    CreateValueAsUint64 (ButtonHiiValueArray + 1, ButtonActionCancel);
    break;

  default:
    DEBUG ((EFI_D_INFO, "DialogOperation is error"));
    ASSERT (FALSE);
    return EFI_UNSUPPORTED;
  }

  CreateValueAsUint64 (&Dialog.ConfirmHiiValue, ButtonActionYes);

  DialogType = H2O_FORM_BROWSER_D_TYPE_SHOW_CONFIRM_PAGE | H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG;
  Status = CreateNewDialog (
             DialogType,
             0,
             TitleString,
             2,
             0,
             ButtonCount,
             (CHAR16 **)&BodyStringArray,
             NULL,
             ButtonStringArray,
             NULL,
             ButtonHiiValueArray,
             &Dialog
             );
  if (!EFI_ERROR (Status) && Dialog.ConfirmHiiValue.Value.u8 == ButtonActionYes) {
    KeyValue->ScanCode    = SCAN_NULL;
    KeyValue->UnicodeChar = CHAR_CARRIAGE_RETURN;
  } else {
    KeyValue->ScanCode    = SCAN_ESC;
    KeyValue->UnicodeChar = CHAR_NULL;
  }

  if (!ConsoleInitComplete) {
    FBDetachConsoles (Private);
  }

  for (Index = 0; Index < ButtonCount; Index++) {
    FreePool (ButtonStringArray[Index]);
  }

  return Status;

}

//_Start_L05_SETUP_MENU_
/**
 Display L05 Set HDD Password Dialog
 (Refer by FBPasswordDialog () & ShowPwdDialog ())

 @param[in]  HotKey                 HotKey information
 @param[in]  MaximumStringSize      Maximum string size
 @param[out] UserInputStringBuffer  User input string buffer
 @param[out] KeyValue               Output key value which stand for user selection
 @param[in]  String                 Dialog question string

 @retval EFI_SUCCESS                Process successfully.
**/
EFI_STATUS
EFIAPI
FBL05HddPasswordDialog (
  IN  UINT32                                   NumberOfLines,
  IN  BOOLEAN                                 HotKey,
  IN  UINT32                                  MaximumStringSize,
  OUT CHAR16                                  *UserInputStringBuffer,
  OUT EFI_INPUT_KEY                           *KeyValue,
  IN  CHAR16                                  *TitleString,
  ...
  )
{
  EFI_STATUS                                  Status;
  H2O_FORM_BROWSER_PRIVATE_DATA               *Private;
  BOOLEAN                                     ConsoleInitComplete;
  H2O_FORM_BROWSER_D                          Dialog;
  CHAR16                                      *BodyStrArray[2];
  CHAR16                                      *BodyInputStringArray[2];
  UINT32                                      Index;
  CHAR16                                      *NewPasswordStr;
  CHAR16                                      *ConfirmNewPasswordStr;
  UINTN                                       NewPasswordStrSize;
  EFI_STRING                                  ButtonStringArray[2];
  EFI_HII_VALUE                               ButtonHiiValueArray[2];

  mL05HddPasswordDialogFlag = 0;

  if ((MaximumStringSize == 0) || (UserInputStringBuffer == NULL)) {
    return EFI_SUCCESS;
  }

  Private = &mFBPrivate;

  ConsoleInitComplete = Private->ConsoleInitComplete;
  if (!ConsoleInitComplete) {
    Status = FBInitConsoles (Private);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  ZeroMem (&Dialog, sizeof (H2O_FORM_BROWSER_D));
  ZeroMem (BodyStrArray, sizeof (BodyStrArray));
  ZeroMem (BodyInputStringArray, sizeof (BodyInputStringArray));

  BodyStrArray[0] = GetString (STRING_TOKEN (L05_STR_HDD_ENTER_NEW_PASSWORD)  , mHiiHandle);
  BodyStrArray[1] = GetString (STRING_TOKEN (L05_STR_HDD_CONFIRM_NEW_PASSWORD), mHiiHandle);

  for (Index = 0; Index < sizeof (BodyInputStringArray) / sizeof (CHAR16 *); Index++) {
    BodyInputStringArray[Index] = gEmptyString;
  }

  CreateValueAsString (&Dialog.ConfirmHiiValue, (UINT16) MaximumStringSize * sizeof (CHAR16), (UINT8 *)L"");

  if (!FeaturePcdGet (PcdH2OFormBrowserLocalMetroDESupported)) {
    Status = CreateNewDialog (
               (H2O_FORM_BROWSER_D_TYPE_PASSWORD << 16) | H2O_FORM_BROWSER_D_TYPE_SELECTION | H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG,
               0,
               TitleString,
               2,
               2,
               0,
               BodyStrArray,
               BodyInputStringArray,
               NULL,
               NULL,
               NULL,
               &Dialog
               );
  } else {
    ButtonStringArray[0] = GetString (STRING_TOKEN (SCU_STR_YES_TEXT), mHiiHandle);
    ButtonStringArray[1] = GetString (STRING_TOKEN (SCU_STR_NO_TEXT), mHiiHandle);

    CreateValueAsBoolean (ButtonHiiValueArray ,TRUE);
    CreateValueAsBoolean (ButtonHiiValueArray+1 ,FALSE);
  
    Status = CreateNewDialog (
               (H2O_FORM_BROWSER_D_TYPE_PASSWORD << 16) | H2O_FORM_BROWSER_D_TYPE_SELECTION | H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG,
               0,
               TitleString,
               2,
               2,
               2,
               BodyStrArray,
               BodyInputStringArray,
               ButtonStringArray,
               NULL,
               ButtonHiiValueArray,
               &Dialog
               );
  }

  if (!EFI_ERROR (Status)) {
    NewPasswordStr        = (CHAR16 *) (Dialog.ConfirmHiiValue.Buffer);
    NewPasswordStrSize    = StrSize (NewPasswordStr);
    ConfirmNewPasswordStr = (CHAR16 *) (Dialog.ConfirmHiiValue.Buffer + NewPasswordStrSize);

    if (NewPasswordStr [0] == 0x00 || ConfirmNewPasswordStr [0] == 0x00) {
      if (NewPasswordStr [0] == 0x00) {
        mL05HddPasswordDialogFlag = mL05HddPasswordDialogFlag | L05_HDD_PASSWORD_NEW_EMPTY;
      }
      
      if (ConfirmNewPasswordStr [0] == 0x00) {
        mL05HddPasswordDialogFlag = mL05HddPasswordDialogFlag | L05_HDD_PASSWORD_CONFIRM_EMPTY;
      }

    } else if (StrCmp (NewPasswordStr, ConfirmNewPasswordStr) != 0) {
      mL05HddPasswordDialogFlag = mL05HddPasswordDialogFlag | L05_HDD_PASSWORD_CONFIRM_ERROR;
    }

    if (StrLen (NewPasswordStr) < PcdGet16 (PcdH2OHddPasswordMinLength)) {
      Status = EFI_ABORTED;
    }

    ZeroMem (UserInputStringBuffer, sizeof (CHAR16) * MaximumStringSize);
    CopyMem (UserInputStringBuffer, NewPasswordStr, StrSize (NewPasswordStr));
    FreePool (Dialog.ConfirmHiiValue.Buffer);
    
    KeyValue->ScanCode    = SCAN_NULL;
    KeyValue->UnicodeChar = CHAR_CARRIAGE_RETURN;
  }

  if (mL05HddPasswordDialogFlag != 0) {
    Status = EFI_NOT_FOUND;
  }

  if (Status == EFI_NOT_READY) {
    //
    // Shut Dialog
    //
    KeyValue->ScanCode    = SCAN_ESC;
    KeyValue->UnicodeChar = CHAR_NULL;
  }

  if (EFI_ERROR (Status) && (Status != EFI_NOT_READY)) {
    ShowPwdStatusMessage (0, Status);
  }

  if (!ConsoleInitComplete) {
    FBDetachConsoles (Private);
  }

  if (FeaturePcdGet (PcdH2OFormBrowserLocalMetroDESupported)) {
    FreePool (ButtonStringArray[0]);
    FreePool (ButtonStringArray[1]);
  }

  return Status;
}

#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
/**
  Display L05 interrup menu dialog.

  @param[out] L05InterrupMenuSelection Output value which stand for user selection

  @retval EFI_SUCCESS                   Process successfully.
**/
EFI_STATUS
EFIAPI
FBL05InterrupMenuDialog (
  OUT UINT8                             *L05InterrupMenuSelection
  )
{
  EFI_STATUS                            Status;
  H2O_FORM_BROWSER_D                    Dialog;
  CHAR16                                *TitleString;
  UINT32                                DialogType;
  CHAR16                                *BodyStrArray[9];
  BOOLEAN                               ConsoleInitComplete;
  H2O_FORM_BROWSER_PRIVATE_DATA         *Private;
  UINT32                                BodyStringCount;
  UINT32                                Index;
  EFI_STRING                            ButtonStringArray[1];
  EFI_HII_VALUE                         ButtonHiiValueArray[1];
  UINT32                                ButtonCount;

  *L05InterrupMenuSelection = 0;
  TitleString               = NULL;
  ConsoleInitComplete       = FALSE;
  Private                   = NULL;
  BodyStringCount           = sizeof (BodyStrArray) / sizeof (CHAR16 *);
  ButtonCount               = sizeof (ButtonStringArray) / sizeof (EFI_STRING);

  FBIsConsoleInit (&ConsoleInitComplete, &Private);
  if (!ConsoleInitComplete) {
    Status = FBInitConsoles (Private);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  ZeroMem (&Dialog, sizeof (H2O_FORM_BROWSER_D));
  DialogType   = H2O_FORM_BROWSER_D_TYPE_SELECTION | H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG;
  TitleString = GetString (STRING_TOKEN (L05_STR_STARTUP_INTERRUPT_MENU_TITLE), mHiiHandle);
  ZeroMem (BodyStrArray, sizeof (BodyStrArray));

  BodyStrArray[0] = GetString (STRING_TOKEN (L05_STR_STARTUP_INTERRUPT_MENU_STR1), mHiiHandle);
  BodyStrArray[1] = GetString (STRING_TOKEN (L05_STR_STARTUP_INTERRUPT_MENU_ESC), mHiiHandle);
  BodyStrArray[2] = GetString (STRING_TOKEN (L05_STR_STARTUP_INTERRUPT_MENU_F1), mHiiHandle);
  BodyStrArray[3] = GetString (STRING_TOKEN (L05_STR_STARTUP_INTERRUPT_MENU_F9), mHiiHandle);
  BodyStrArray[4] = GetString (STRING_TOKEN (L05_STR_STARTUP_INTERRUPT_MENU_F10), mHiiHandle);
  BodyStrArray[5] = GetString (STRING_TOKEN (L05_STR_STARTUP_INTERRUPT_MENU_F11), mHiiHandle);
  BodyStrArray[6] = GetString (STRING_TOKEN (L05_STR_STARTUP_INTERRUPT_MENU_F12), mHiiHandle);
  BodyStrArray[7] = GetString (STRING_TOKEN (L05_EMPTY_STR), mHiiHandle);
  BodyStrArray[8] = GetString (STRING_TOKEN (L05_STR_STARTUP_INTERRUPT_MENU_STR2), mHiiHandle);

  ButtonStringArray[0] = GetString (STRING_TOKEN (L05_STR_CLOSE), mHiiHandle);
  CreateValueAsUint64 (ButtonHiiValueArray, ButtonActionCancel);
  CreateValueAsUint64 (&Dialog.ConfirmHiiValue, ButtonActionCancel);

  Status = CreateNewDialog (
             DialogType,
             0,
             TitleString,
             9,
             0,
             ButtonCount,
             BodyStrArray,
             NULL,
             ButtonStringArray,
             NULL,
             ButtonHiiValueArray,
             &Dialog
             );

  if (!EFI_ERROR (Status)) {
    *L05InterrupMenuSelection = Dialog.ConfirmHiiValue.Value.u8;
  }

  if (!ConsoleInitComplete) {
    FBDetachConsoles (Private);
  }

  for (Index = 0; Index < BodyStringCount; Index++) {
    FreePool (BodyStrArray[Index]);
  }

  FreePool (TitleString);

  return EFI_SUCCESS;
}
//_End_L05_INTERRUPT_MENU_
#endif

#ifdef L05_SECURITY_ERASE_ENABLE
/**
  Display L05 Security Erase Processing Dialog

  @param[in] IsExitDialog               Is exit Dialog.
  @param[in] SecurityEraseTime          Time of Security Erase.
  @param[in] ArrayOfStrings             Message strings.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
FBL05SecurityEraseProcessingDialog (
  IN  BOOLEAN                           IsExitDialog,
  IN  UINT16                            SecurityEraseTime,
  IN  CHAR16                            *ArrayOfStrings,
  ...
  )
{
  EFI_STATUS                            Status;
  CHAR16                                **MsgString;
  BOOLEAN                               ConsoleInitComplete;
  H2O_FORM_BROWSER_PRIVATE_DATA         *Private;
  H2O_FORM_BROWSER_D                    Dialog;
  UINT32                                DialogType;
  UINT32                                Attribute;
  UINT32                                ButtonCount;

  MsgString = &ArrayOfStrings;

  ConsoleInitComplete = FALSE;
  Private = NULL;
  FBIsConsoleInit (&ConsoleInitComplete, &Private);
  if (!ConsoleInitComplete) {
    Status = FBInitConsoles (Private);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  Status      = EFI_SUCCESS;
  DialogType  = H2O_FORM_BROWSER_D_TYPE_MSG | H2O_FORM_BROWSER_D_TYPE_FROM_H2O_DIALOG;
  Attribute   = EFI_BLACK | EFI_BACKGROUND_LIGHTGRAY;
  ButtonCount = 0;

  PcdSetBoolS (PcdL05SecurityEraseProcessingDialogFlag, TRUE);
  PcdSetBoolS (PcdL05SecurityEraseProcessingDialogExit, IsExitDialog);

  //
  // ATA Command Set - 4 (ACS-4)
  // 9.11.8.5 Time required for a Normal Erase mode SECURITY ERASE UNIT command (NORMAL SECURITY ERASE TIME field)
  // (Value X 2) minutes
  //
  PcdSet64S (PcdL05SecurityEraseTimeMicrosecond, ((UINT64) SecurityEraseTime) * 2 * 60 * 1000 * 1000);

  ZeroMem (&Dialog, sizeof (H2O_FORM_BROWSER_D));
  Status = CreateSimpleDialog (
             DialogType,
             Attribute,
             NULL,
             2,
             MsgString,
             ButtonCount,
             &Dialog
             );


  if (!ConsoleInitComplete) {
    FBDetachConsoles (Private);
  }

  return Status;
}
#endif
//_End_L05_SETUP_MENU_

/**
 This is the routine which install the H2O dialog protocol.

 @retval  EFI_SUCCESS            The function completed successfully.
 @retval  EFI_OUT_OF_RESOURCES   Allocate memory fail.
 @retval  otherwise              Install protocol interface fail
**/
EFI_STATUS
EFIAPI
InstallH2ODialogProtocol (
  EFI_HANDLE ImageHandle
  )
{
  EFI_STATUS                     Status;
  H2O_DIALOG_PROTOCOL           *H2ODialogData;

  H2ODialogData = AllocateZeroPool (sizeof (H2O_DIALOG_PROTOCOL));
  if (H2ODialogData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }


  H2ODialogData->ConfirmDialog     = FBConfirmDialog;
  H2ODialogData->PasswordDialog    = FBPasswordDialog;
  H2ODialogData->OneOfOptionDialog = FBOneOfOptionDialog;
  H2ODialogData->CreateMsgPopUp    = FBCreateMsgPopUp;
  H2ODialogData->ShowPageInfo      = FBShowPageInfo;
  H2ODialogData->NumericDialog     = FBNumericDialog;
  H2ODialogData->ConfirmPageDialog = FBConfirmPageDialog;
//_Start_L05_SETUP_MENU_
  H2ODialogData->L05HddPasswordDialog = FBL05HddPasswordDialog;
  H2ODialogData->L05ShowPwdStatusMessage = ShowPwdStatusMessage;
#ifdef L05_SMB_BIOS_ENABLE
//_Start_L05_INTERRUPT_MENU_
  H2ODialogData->L05InterrupMenuDialog = FBL05InterrupMenuDialog;
//_End_L05_INTERRUPT_MENU_
#endif
#ifdef L05_SECURITY_ERASE_ENABLE
  H2ODialogData->L05SecurityEraseProcessingDialog = FBL05SecurityEraseProcessingDialog;
#endif
//_End_L05_SETUP_MENU_

  Status = gBS->InstallProtocolInterface (
                  &ImageHandle,
                  &gH2ODialogProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  H2ODialogData
                  );
  DEBUG ((EFI_D_INFO, "Install H2ODialogProtocol is fail"));
  ASSERT_EFI_ERROR (Status);

  return Status;
}

