/** @file
  Draw Progress Bar

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_PEI_PROGRESS_BAR_LIB_H_
#define _L05_PEI_PROGRESS_BAR_LIB_H_

#define PROGRESS_ACCURACY               100

EFI_STATUS
ProgressBarExit (
  VOID
  );

EFI_STATUS
ProgressBarInit (
  VOID
  );

VOID
PaintProgressBar (
  IN  UINTN                             Completion
  );

#endif
