/** @file
  Provide L05 WMI Setup Item related opearting functions

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_WMI_SETUP_ITEM_LIB_INTERNAL_H_
#define _L05_WMI_SETUP_ITEM_LIB_INTERNAL_H_

#include <L05ChipsetNameList.h>
#include <Uefi.h>
#include <SetupConfig.h>    // SYSTEM_CONFIGURATION
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
#include <SetupVariable.h>  // CPU_SETUP
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE)
#include <SgxSetupData.h>   // SGX_SETUP_DATA
#endif
#include <CpuRegs.h>        // CPU_FEATURE_DISABLE
#else
#include <Dxe/AmdPbsSetupDxe/AmdPbsConfig.h>  // AMD_PBS_SETUP_OPTION
#endif
#include <L05Config.h>
#include <L05WmiSetupUnderOsDefinition.h>
#include <Guid/GlobalVariable.h>
#include <Guid/DebugMask.h>  // gEfiGenericVariableGuid
#include <Guid/L05WmiSetupUnderOsSetupVariable.h>  // gL05WmiSetupUnderOsSetupVariableGuid
#include <Protocol/SmmVariable.h>
#include <Protocol/BootOptionPolicy.h>
#include <Protocol/L05SecureBoot.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PrintLib.h>
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
#include <Private/Library/SoftwareGuardLib.h>  // IsSgxFeatureCtrlSet()
#endif
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE)
#include <Library/SoftwareGuardLib.h>  // IsSgxFeatureCtrlSet()
#endif

#pragma pack (1)

typedef enum {
  DisableEnableValueType = 0x00,
  EnableDisableValueType,
  NullDisableEnableValueType,
  NullEnableDisableValueType,
  SATAControllerValueType,
  GraphicsDeviceValueType,
  DeviceGuardValueType,
  IntelSgxValueType,
  SecureBootValueType,
  PlatformModeValueType,
  SecureBootModeValueType,
  BootModeValueType,
  BootPriorityValueType,
  SetupValueTypeMax,
  ClearUserPasswordType,
  ClearTpmKeyType,
  ResetToSetupModeType,
  RestoreFactoryKeysType,
  EfiBootOrderType,
  LegacyBootOrderType,
} SETUP_DATA_TYPE;

typedef struct {
  BOOLEAN                               *L05WmiSetupItemValid;
  UINT8                                 *L05WmiSetupItem;
  UINT8                                 *SetupNvDataMap;
  BOOLEAN                               *SaveSetupNvData;
  SETUP_DATA_TYPE                       SetupDataType;
} L05_WMI_SETUP_ITEM_SYNC_MAP;

typedef struct {
  UINT8                                 *L05OdmFunctionSupport;
  BOOLEAN                               *L05WmiSetupItemValid;
} L05_WMI_ODM_FUNCTION_SUPPORT_SYNC_MAP;


typedef struct {
  SETUP_DATA_TYPE                       SetupDataType;
  UINT8                                 *SelectValueList;
  UINT16                                SelectValueCount;
} WMI_SELECT_VALUE_LIST_MAP;
#pragma pack ()

extern L05_WMI_SETUP_ITEM_SYNC_MAP           mL05WmiSetupItemSyncMap[];
extern WMI_SELECT_VALUE_LIST_MAP             mWmiSelectValueListMap[];
extern L05_WMI_ODM_FUNCTION_SUPPORT_SYNC_MAP mL05OdmFunctionSupportSyncMap[];
extern L05_WMI_SETUP_ITEM                    mL05WmiSetupItem;
extern SYSTEM_CONFIGURATION                  mSetupNvData;
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
extern PCH_SETUP                             mPchSetupNvData;
extern SA_SETUP                              mSaSetupNvData;
extern CPU_SETUP                             mCpuSetupNvData;
#else
extern AMD_PBS_SETUP_OPTION                  mAmdPbsSetupNvData;
#endif
extern BOOLEAN                               mSaveSetupNvData;
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
extern BOOLEAN                               mSavePchSetupNvData;
extern BOOLEAN                               mSaveSaSetupNvData;
extern BOOLEAN                               mSaveCpuSetupNvData;
#else
extern BOOLEAN                               mSaveAmdPbsSetupNvData;
#endif
extern BOOLEAN                               mSavePhysicalBootOrder;
extern UINT16                                mL05WmiSetupItemSyncMapListCount;
extern UINT16                                mWmiSelectValueListMapListCount;
extern UINT16                                mL05OdmFunctionSupportSyncMapListCount;

#endif
