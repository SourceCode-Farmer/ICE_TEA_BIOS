/** @file
  Provide L05 WMI Setup Item related opearting functions

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <L05WmiSetupItemLibInternal.h>

//
// L05 related setup item
//
L05_WMI_SETUP_ITEM                      mL05WmiSetupItem;
UINT8                                   mSystemL05SecureBoot         = 0;
EFI_L05_SECURE_BOOT_DATA                mL05SecureBootData;
BOOLEAN                                 mSaveL05SecureBootData       = FALSE;
BOOLEAN                                 mLoadDefaultSetupData        = FALSE;

//
// Chipset & Kernel related setup item
//
EFI_SMM_VARIABLE_PROTOCOL               *mSmmVariable;
SYSTEM_CONFIGURATION                    mSetupNvData;
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
PCH_SETUP                               mPchSetupNvData;
SA_SETUP                                mSaSetupNvData;
CPU_SETUP                               mCpuSetupNvData;
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
SGX_SETUP_DATA                          mSgxSetupNvData;
#endif
#else
AMD_PBS_SETUP_OPTION                    mAmdPbsSetupNvData;
#endif
UINT16                                  *mPhysicalBootOrder          = NULL;
UINTN                                   mPhysicalBootOrderSize       = 0;
UINT16                                  *mEfiBootOrder               = NULL;
UINTN                                   mEfiBootOrderSize            = 0;
UINT16                                  *mLegacyBootOrder            = NULL;
UINTN                                   mLegacyBootOrderSize         = 0;
BOOLEAN                                 mSaveSetupNvData             = FALSE;
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
BOOLEAN                                 mSavePchSetupNvData          = FALSE;
BOOLEAN                                 mSaveSaSetupNvData           = FALSE;
BOOLEAN                                 mSaveCpuSetupNvData          = FALSE;
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
BOOLEAN                                 mSaveSgxSetupNvData          = FALSE;
#endif
#else
BOOLEAN                                 mSaveAmdPbsSetupNvData       = FALSE;
#endif
BOOLEAN                                 mSavePhysicalBootOrder       = FALSE;
BOOLEAN                                 mSystemL05BootPriority       = FALSE;

//
// Backup Chipset & Kernel related setup item for Discard function
//
BOOLEAN                                 mAlreadyBackupSetup          = FALSE;
SYSTEM_CONFIGURATION                    mBackupSetupNvData;
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
PCH_SETUP                               mBackupPchSetupNvData;
SA_SETUP                                mBackupSaSetupNvData;
CPU_SETUP                               mBackupCpuSetupNvData;
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
SGX_SETUP_DATA                          mBackupSgxSetupNvData;
#endif
#else
AMD_PBS_SETUP_OPTION                    mBackupAmdPbsSetupNvData;
#endif
UINT16                                  *mBackupPhysicalBootOrder    = NULL;
UINTN                                   mBackupPhysicalBootOrderSize = 0;

/**
  This function is for getting variable data and size in SMM mode.

  @param  Name                          A pointer to the variable string name.
  @param  VendorGuid                    A pointer to the variable guid.
  @param  VariableSize                  A pointer to the Variable data size.

  @retval VOID                          Return point of variable data.
**/
VOID *
EFIAPI
SmmGetVariableAndSize (
  IN  CHAR16                            *Name,
  IN  EFI_GUID                          *VendorGuid,
  OUT UINTN                             *VariableSize
  )
{
  EFI_STATUS  Status;
  UINTN       BufferSize;
  VOID        *Buffer;

  //
  // Initialization
  //
  Buffer = NULL;

  //
  // Pass in a zero size buffer to find the required buffer size.
  //
  BufferSize  = 0;
  Status      = mSmmVariable->SmmGetVariable (
                                Name,
                                VendorGuid,
                                NULL,
                                &BufferSize,
                                Buffer
                                );

  if (Status == EFI_BUFFER_TOO_SMALL) {

    //
    // Allocate the buffer to return
    //
    Buffer = AllocateRuntimeZeroPool (BufferSize);

    if (Buffer == NULL) {
      return NULL;
    }

    //
    // Read variable into the allocated buffer.
    //
    Status = mSmmVariable->SmmGetVariable (Name, VendorGuid, NULL, &BufferSize, Buffer);

    if (EFI_ERROR (Status)) {

      BufferSize = 0;
      FreePool (Buffer);
      Buffer = NULL;
    }
  }

  *VariableSize = BufferSize;

  return Buffer;
}


/**
  Internal function to check the input device path is whether a legacy boot option device path.

  @param  DevicePath                    A pointer to a device path data structure.

  @retval TRUE                          This is a legacy boot option.
  @retval FALSE                         This isn't a legacy boot option.
**/
STATIC
BOOLEAN
IsLegacyBootOption (
  IN  CONST EFI_DEVICE_PATH_PROTOCOL  *DevicePath
  )
{
  if (DevicePath == NULL) {
    return FALSE;
  }

  if ((BBS_DEVICE_PATH == DevicePath->Type) && (BBS_BBS_DP == DevicePath->SubType)) {
    return TRUE;
  }

  return FALSE;
}

/**
  WMI get boot description.

  @param  BootOptionType                Type of boot option.
  @param  BootOrder                     Assign a pointer to the boot order.
  @param  BootOrderSize                 A pointer to the boot order size.
  @param  BootOrderCount                A pointer to the boot order count.

  @retval L05_WMI_BOOT_DESCRIPTION *    Return point of WMI boot description buffer.
**/
L05_WMI_BOOT_DESCRIPTION *
WmiGetBootDescription (
  IN  UINTN                             BootOptionType,
  OUT UINT16                            **BootOrder,
  OUT UINTN                             *BootOrderSize,
  OUT UINT16                            *BootOrderCount
  )
{
  UINT16                                PhysicalBootOrderCount;
  L05_WMI_BOOT_DESCRIPTION              *WmiBootDescriptionBuffer;
  UINTN                                 Index;
  UINT8                                 *Variable;
  UINTN                                 VariableSize;
  UINT8                                 *TempPtr;
  CHAR16                                OptionName[10];
  CHAR16                                *BootDescription;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePath;
  UINTN                                 BufferSize;
  UINTN                                 DescriptionLength;

  //
  // Initialization
  //
  WmiBootDescriptionBuffer = NULL;
  *BootOrderCount = 0;

  if (mPhysicalBootOrder == NULL) {
    return WmiBootDescriptionBuffer;
  }

  //
  // Count physical boot order
  //
  PhysicalBootOrderCount = (UINT16) mPhysicalBootOrderSize / sizeof (UINT16);

  //
  // Allocate WMI boot description buffer
  //
  BufferSize = sizeof (L05_WMI_BOOT_DESCRIPTION) * PhysicalBootOrderCount;
  WmiBootDescriptionBuffer = AllocateRuntimeZeroPool (BufferSize);

  //
  // Get boot description by each boot option
  //
  for (Index = 0; Index < PhysicalBootOrderCount; Index++) {
    SetMem (OptionName, sizeof (OptionName), 0);
    UnicodeSPrint (OptionName, sizeof (OptionName), L"Boot%04x", mPhysicalBootOrder[Index]);
    Variable = SmmGetVariableAndSize (
                 OptionName,
                 &gEfiGlobalVariableGuid,
                 &VariableSize
                 );

    if (Variable == NULL) {
      continue;
    }

    TempPtr  = Variable;
    TempPtr += sizeof (UINT32); // Size of the option attribute
    TempPtr += sizeof (UINT16); // Size of the option's device path size
    BootDescription = (CHAR16 *) TempPtr; // Pointer to boot description
    TempPtr += StrSize ((CHAR16 *) TempPtr); // Size of the option's description string
    DevicePath = (EFI_DEVICE_PATH_PROTOCOL *) TempPtr;

    if (BootOptionType == IsLegacyBootOption (DevicePath)) {
      //
      // Put BootOption & BootDescription to L05_WMI_BOOT_DESCRIPTION by BootOptionType
      //
      DescriptionLength = StrLen ((CHAR16 *) BootDescription);

      WmiBootDescriptionBuffer[*BootOrderCount].BootOption = mPhysicalBootOrder[Index];
      WmiBootDescriptionBuffer[*BootOrderCount].BootDescription = AllocateRuntimeZeroPool (DescriptionLength + 1);
      UnicodeStrToAsciiStrS (BootDescription, WmiBootDescriptionBuffer[*BootOrderCount].BootDescription, DescriptionLength + 1);
      (*BootOrderCount)++;
    }

    FreePool (Variable);
  }

  //
  // Allocate boot order buffer
  //
  *BootOrderSize = sizeof (UINT16) * (*BootOrderCount);
  *BootOrder = AllocateRuntimeZeroPool (*BootOrderSize);

  //
  // Put boot order from WMI boot description buffer
  //
  for (Index = 0; Index < *BootOrderCount; Index++) {
    (*BootOrder)[Index] = WmiBootDescriptionBuffer[Index].BootOption;
  }

  return WmiBootDescriptionBuffer;
}

/**
  Get platform setup variable.

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
GetPlatformSetupVariable (
  )
{
  EFI_STATUS                            Status;
  UINTN                                 BufferSize;

  //
  // Get "Setup" variable and save to mSetupNvData
  //
  BufferSize = sizeof (SYSTEM_CONFIGURATION);
  Status = mSmmVariable->SmmGetVariable (
                           L"Setup",
                           &gSystemConfigurationGuid,
                           NULL,
                           &BufferSize,
                           &mSetupNvData
                           );

  if (EFI_ERROR (Status)) {
    return Status;
  }

#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  //
  // Get "PchSetup" variable and save to mPchSetupNvData
  //
  BufferSize = sizeof (PCH_SETUP);
  Status = mSmmVariable->SmmGetVariable (
                           PCH_SETUP_VARIABLE_NAME,
                           &gPchSetupVariableGuid,
                           NULL,
                           &BufferSize,
                           &mPchSetupNvData
                           );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get "SaSetup" variable and save to mSaSetupNvData
  //
  BufferSize = sizeof (SA_SETUP);
  Status = mSmmVariable->SmmGetVariable (
                           SA_SETUP_VARIABLE_NAME,
                           &gSaSetupVariableGuid,
                           NULL,
                           &BufferSize,
                           &mSaSetupNvData
                           );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get "CpuSetup" variable and save to mCpuSetupNvData
  //
  BufferSize = sizeof (CPU_SETUP);
  Status = mSmmVariable->SmmGetVariable (
                           CPU_SETUP_VARIABLE_NAME,
                           &gCpuSetupVariableGuid,
                           NULL,
                           &BufferSize,
                           &mCpuSetupNvData
                           );

  if (EFI_ERROR (Status)) {
    return Status;
  }

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
  //
  // Get "SgxSetupVariable" variable and save to mSgxSetupNvData
  //
  BufferSize = sizeof (SGX_SETUP_DATA);
  Status = mSmmVariable->SmmGetVariable (
                           SGX_SETUP_VARIABLE_NAME,
                           &gSgxSetupVariableGuid,
                           NULL,
                           &BufferSize,
                           &mSgxSetupNvData
                           );

  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

#else
  //
  // Get "AMD_PBS_SETUP" variable and save to mAmdPbsSetupNvData
  //
  BufferSize = sizeof (AMD_PBS_SETUP_OPTION);
  Status = mSmmVariable->SmmGetVariable (
                           AMD_PBS_SETUP_VARIABLE_NAME,
                           &gAmdPbsSystemConfigurationGuid,
                           NULL,
                           &BufferSize,
                           &mAmdPbsSetupNvData
                           );

  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

  //
  // Get "PhysicalBootOrder" variable and save to mPhysicalBootOrder
  //
  mPhysicalBootOrder = SmmGetVariableAndSize (
                         L"PhysicalBootOrder",
                         &gEfiGenericVariableGuid,
                         &mPhysicalBootOrderSize
                         );

  mBackupPhysicalBootOrder = AllocateRuntimeCopyPool (mPhysicalBootOrderSize, mPhysicalBootOrder);

  return Status;
}

/**
  Set platform setup variable.

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SetPlatformSetupVariable (
  )
{
  EFI_STATUS                            Status;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;

  //
  // Set "Setup" variable
  //
  if (mSaveSetupNvData) {
    Status = mSmmVariable->SmmSetVariable (
                             L"Setup",
                             &gSystemConfigurationGuid,
                             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                             sizeof (SYSTEM_CONFIGURATION),
                             &mSetupNvData
                             );
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }

#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  //
  // Set "PchSetup" variable
  //
  if (mSavePchSetupNvData) {
    Status = mSmmVariable->SmmSetVariable (
                             PCH_SETUP_VARIABLE_NAME,
                             &gPchSetupVariableGuid,
                             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                             sizeof (PCH_SETUP),
                             &mPchSetupNvData
                             );
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Set "SaSetup" variable
  //
  if (mSaveSaSetupNvData) {
    Status = mSmmVariable->SmmSetVariable (
                             SA_SETUP_VARIABLE_NAME,
                             &gSaSetupVariableGuid,
                             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                             sizeof (SA_SETUP),
                             &mSaSetupNvData
                             );
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Set "CpuSetup" variable
  //
  if (mSaveCpuSetupNvData) {
    Status = mSmmVariable->SmmSetVariable (
                             CPU_SETUP_VARIABLE_NAME,
                             &gCpuSetupVariableGuid,
                             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                             sizeof (CPU_SETUP),
                             &mCpuSetupNvData
                             );
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
  //
  // Set "SgxSetupVariable" variable
  //
  if (mSaveSgxSetupNvData) {
    Status = mSmmVariable->SmmSetVariable (
                             SGX_SETUP_VARIABLE_NAME,
                             &gSgxSetupVariableGuid,
                             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                             sizeof (SGX_SETUP_DATA),
                             &mSgxSetupNvData
                             );
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

#else
  //
  // Set "AMD_PBS_SETUP" variable
  //
  if (mSaveAmdPbsSetupNvData) {
    Status = mSmmVariable->SmmSetVariable (
                             AMD_PBS_SETUP_VARIABLE_NAME,
                             &gAmdPbsSystemConfigurationGuid,
                             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                             sizeof (AMD_PBS_SETUP_OPTION),
                             &mAmdPbsSetupNvData
                             );
  }

  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

  return Status;
}

/**
  Set secure boot data.

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SetSecureBootData (
  )
{
  EFI_STATUS                            Status;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;

  //
  // Secure Boot check & setting
  //
  if (mSystemL05SecureBoot != mSetupNvData.L05SecureBoot) {

    if (mSetupNvData.L05SecureBoot == 1) {  // 0:Disable, 1:Enable
      mL05SecureBootData.Action = L05_SECURE_BOOT_ENABLE;

    } else {
      mL05SecureBootData.Action = L05_SECURE_BOOT_DISABLE;
    }

    mSaveL05SecureBootData = TRUE;
  }

  if (mSaveL05SecureBootData) {

    mSaveL05SecureBootData = FALSE;

    Status = mSmmVariable->SmmSetVariable (
                             L05_WMI_SECURE_BOOT_DATA_VARIABLE_NAME,
                             &gL05WmiSetupUnderOsSetupVariableGuid,
                             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                             sizeof (EFI_L05_SECURE_BOOT_DATA),
                             (VOID *) &mL05SecureBootData
                             );
  }

  return Status;
}

/**
  Set boot order.

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SetBootOrder (
  )
{
  EFI_STATUS                            Status;
  UINT16                                *TempBootOrder;
  UINTN                                 TempBootOrderSize;

  Status            = EFI_SUCCESS;
  TempBootOrderSize = 0;

  //
  // When boot priority change, need save physical boot order
  //
  if ((mSystemL05BootPriority != mL05WmiSetupItem.L05BootPriority) && (mL05WmiSetupItem.L05BootMode == L05_WMI_BOOT_MODE_LEGACY_SUPPORT)) {
    mSavePhysicalBootOrder = TRUE;
  }

  if (!mSavePhysicalBootOrder) {
    return EFI_SUCCESS;
  }

  mSavePhysicalBootOrder = FALSE;

  if (mL05WmiSetupItem.L05BootMode == L05_WMI_BOOT_MODE_UEFI) {
    TempBootOrderSize = mEfiBootOrderSize;
    TempBootOrder = mEfiBootOrder;

  } else {
    TempBootOrderSize = mEfiBootOrderSize + mLegacyBootOrderSize;

    TempBootOrder = AllocateRuntimeZeroPool (TempBootOrderSize);
  
    if (mL05WmiSetupItem.L05BootPriority == 0x00) {  // 0x00 : [UEFI First], 0x01 : [Legacy First]
      CopyMem (TempBootOrder, mEfiBootOrder, mEfiBootOrderSize);
      CopyMem (&TempBootOrder[mL05WmiSetupItem.L05EfiBootOrderCount], mLegacyBootOrder, mLegacyBootOrderSize);
  
    } else {
      CopyMem (TempBootOrder, mLegacyBootOrder, mLegacyBootOrderSize);
      CopyMem (&TempBootOrder[mL05WmiSetupItem.L05LegacyBootOrderCount], mEfiBootOrder, mEfiBootOrderSize);
    }
  }

  Status = mSmmVariable->SmmSetVariable (
                           L"PhysicalBootOrder",
                           &gEfiGenericVariableGuid,
                           EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                           TempBootOrderSize,
                           (VOID *) TempBootOrder
                           );

  Status = mSmmVariable->SmmSetVariable (
                           L"BootOrder",
                           &gEfiGlobalVariableGuid,
                           EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                           TempBootOrderSize,
                           (VOID *) TempBootOrder
                           );

  mPhysicalBootOrderSize = TempBootOrderSize;
  CopyMem (mPhysicalBootOrder, TempBootOrder,  mPhysicalBootOrderSize);

  //
  // If Boot Order is changed, Win 8 fast boot should not be active at next boot.
  //
  Status = mSmmVariable->SmmSetVariable (
                           L"TargetHddDevPath",
                           &gEfiGenericVariableGuid,
                           EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                           0,
                           NULL
                           );

  FreePool (TempBootOrder);

  return EFI_SUCCESS;
}

/**
  WMI get boot order and description.

  @param  BootOptionType                Type of boot option.
  @param  BootOrder                     Assign a pointer to the boot order.
  @param  L05SetupItemBootOrder         Assign a pointer to the L05 setup item boot order.
  @param  BootOrderSize                 A pointer to the boot order size.
  @param  BootOrderCount                A pointer to the boot order count.
  @param  L05WmiBootDescription         Assign a pointer to the WMI boot description buffer.

  @retval None
**/
VOID
WmiGetBootOrderAndDescription (
  IN  UINTN                             BootOptionType,
  OUT UINT16                            **BootOrder,
  OUT UINT16                            **L05SetupItemBootOrder,
  OUT UINTN                             *BootOrderSize,
  OUT UINT16                            *BootOrderCount,
  OUT L05_WMI_BOOT_DESCRIPTION          **L05WmiBootDescription
  )
{
  UINTN                                 Index;

  if (*BootOrder != NULL) {
    FreePool (*BootOrder);
    *BootOrder = NULL;
  }

  if (*L05SetupItemBootOrder != NULL) {
    FreePool (*L05SetupItemBootOrder);
    *L05SetupItemBootOrder = NULL;
  }

  for (Index = 0; Index < *BootOrderCount; Index++) {
    FreePool ((*L05WmiBootDescription)[Index].BootDescription);
    (*L05WmiBootDescription)[Index].BootDescription = NULL;
  }

  if ((*L05WmiBootDescription) != NULL) {
    FreePool ((*L05WmiBootDescription));
    (*L05WmiBootDescription) = NULL;
  }

  (*L05WmiBootDescription) = WmiGetBootDescription (
                               BootOptionType,
                               BootOrder,
                               BootOrderSize,
                               BootOrderCount
                               );

  *L05SetupItemBootOrder = AllocateRuntimeZeroPool ((UINTN) (*BootOrderSize));
  CopyMem (*L05SetupItemBootOrder, *BootOrder, *BootOrderSize);
}

/**
  WMI get EFI boot order and description.

  @param  None

  @retval None
**/
VOID
WmiGetEfiBootOrderAndDescription (
  VOID
  )
{
  UINT16                                *BootOrder;
  UINT16                                *L05SetupItemBootOrder;
  UINTN                                 BootOrderSize;
  UINT16                                BootOrderCount;
  L05_WMI_BOOT_DESCRIPTION              *L05WmiBootDescription;

  BootOrder             = mEfiBootOrder;
  L05SetupItemBootOrder = mL05WmiSetupItem.L05EfiBootOrder;
  BootOrderSize         = mEfiBootOrderSize;
  BootOrderCount        = mL05WmiSetupItem.L05EfiBootOrderCount;
  L05WmiBootDescription = mL05WmiSetupItem.L05WmiEfiBootDescription;

  WmiGetBootOrderAndDescription (
    EFI_BOOT_DEV,
    &BootOrder,
    &L05SetupItemBootOrder,
    &BootOrderSize,
    &BootOrderCount,
    &L05WmiBootDescription
    );

  mEfiBootOrder                             = BootOrder;
  mL05WmiSetupItem.L05EfiBootOrder          = L05SetupItemBootOrder;
  mEfiBootOrderSize                         = BootOrderSize;
  mL05WmiSetupItem.L05EfiBootOrderCount     = BootOrderCount;
  mL05WmiSetupItem.L05WmiEfiBootDescription = L05WmiBootDescription;
}

/**
  WMI get legacy boot order and description.

  @param  None

  @retval None
**/
VOID
WmiGetLegacyBootOrderAndDescription (
  VOID
  )
{
  UINT16                                *BootOrder;
  UINT16                                *L05SetupItemBootOrder;
  UINTN                                 BootOrderSize;
  UINT16                                BootOrderCount;
  L05_WMI_BOOT_DESCRIPTION              *L05WmiBootDescription;

  //
  // Boot Order & Description
  //
  BootOrder             = mLegacyBootOrder;
  L05SetupItemBootOrder = mL05WmiSetupItem.L05LegacyBootOrder;
  BootOrderSize         = mLegacyBootOrderSize;
  BootOrderCount        = mL05WmiSetupItem.L05LegacyBootOrderCount;
  L05WmiBootDescription = mL05WmiSetupItem.L05WmiLegacyBootDescription;

  WmiGetBootOrderAndDescription (
    LEGACY_BOOT_DEV,
    &BootOrder,
    &L05SetupItemBootOrder,
    &BootOrderSize,
    &BootOrderCount,
    &L05WmiBootDescription
    );

  mLegacyBootOrder                             = BootOrder;
  mL05WmiSetupItem.L05LegacyBootOrder          = L05SetupItemBootOrder;
  mLegacyBootOrderSize                         = BootOrderSize;
  mL05WmiSetupItem.L05LegacyBootOrderCount     = BootOrderCount;
  mL05WmiSetupItem.L05WmiLegacyBootDescription = L05WmiBootDescription;
}

/**
  Init L05 WMI Setup Item.

  @param  L05WmiSetupItemBuffer         A pointer to the L05 WMI Setup Item buffer.
  @param  GetPlatformData               Is need get platform data.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
InitL05WmiSetupItem (
  IN OUT L05_WMI_SETUP_ITEM             *L05WmiSetupItemBuffer,
  IN     BOOLEAN                        GetPlatformData
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  UINTN                                 SelectValueListMapIndex;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;

  //
  // Clean L05 WMI Setup Item
  //
  ZeroMem (&mL05WmiSetupItem, sizeof (L05_WMI_SETUP_ITEM));
  ZeroMem (&mL05SecureBootData, sizeof (EFI_L05_SECURE_BOOT_DATA));

  //
  // Get Platform Setup Variable
  //
  if (GetPlatformData) {
    Status = GetPlatformSetupVariable ();
  }

  //
  // Sync L05 WMI Setup Item by Platform Setup Value
  //
  for (Index = 0; Index < mL05WmiSetupItemSyncMapListCount; Index++) {

    if (mL05WmiSetupItemSyncMap[Index].SetupDataType < SetupValueTypeMax) {
      //
      // Setup value type to init L05 WMI Setup Item
      //
      if (mL05WmiSetupItemSyncMap[Index].SetupNvDataMap == NULL) {
        continue;
      }

      if (mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem == NULL) {
        continue;
      }

      //
      // Select Value List Map
      //
      for (SelectValueListMapIndex = 0; SelectValueListMapIndex < mWmiSelectValueListMapListCount; SelectValueListMapIndex++) {
        if (mL05WmiSetupItemSyncMap[Index].SetupDataType == mWmiSelectValueListMap[SelectValueListMapIndex].SetupDataType) {
          break;
        }
      }

      //
      // Transform Platform Setup Value to L05 WMI Setup Item Value by Select Value List
      //
      if (mWmiSelectValueListMap[SelectValueListMapIndex].SelectValueList[*mL05WmiSetupItemSyncMap[Index].SetupNvDataMap] != 0xFF) {
        *mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem = mWmiSelectValueListMap[SelectValueListMapIndex].SelectValueList[*mL05WmiSetupItemSyncMap[Index].SetupNvDataMap];
        *mL05WmiSetupItemSyncMap[Index].L05WmiSetupItemValid = TRUE;
      }

    } else {
      //
      // Special type to init L05 WMI Setup Item
      //
      switch (mL05WmiSetupItemSyncMap[Index].SetupDataType) {

      case ClearTpmKeyType:
      case ResetToSetupModeType:
      case RestoreFactoryKeysType:
        *mL05WmiSetupItemSyncMap[Index].L05WmiSetupItemValid = TRUE;
        *mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem = FALSE;
        break;

      case EfiBootOrderType:
        //
        // Boot Order & Description
        //
        WmiGetEfiBootOrderAndDescription ();

        if ((mL05WmiSetupItem.L05WmiEfiBootDescription != NULL) && (mL05WmiSetupItem.L05EfiBootOrderCount != 0)) {
          *mL05WmiSetupItemSyncMap[Index].L05WmiSetupItemValid = TRUE;
        }

        break;

      case LegacyBootOrderType:

        if (mL05WmiSetupItem.L05BootMode == L05_WMI_BOOT_MODE_UEFI) {
        
          *mL05WmiSetupItemSyncMap[Index].L05WmiSetupItemValid = FALSE;
          mLegacyBootOrderSize = 0;

          if (mLegacyBootOrder != NULL) {
            FreePool (mLegacyBootOrder);
            mLegacyBootOrder = NULL;
          }
          
          break;
        }

        //
        // Boot Order & Description
        //
        WmiGetLegacyBootOrderAndDescription ();

        if ((mL05WmiSetupItem.L05WmiLegacyBootDescription != NULL) && (mL05WmiSetupItem.L05LegacyBootOrderCount != 0)) {
          *mL05WmiSetupItemSyncMap[Index].L05WmiSetupItemValid = TRUE;
        }

        break;

      }
    }
  }

  //
  // Determine TPM type.
  //
  if (mSetupNvData.L05TpmInterfaceType == 0x0001) {  // B_TPM_INTERFACE_CRB
    //
    // B_TPM_INTERFACE_CRB for Firmware TPM usage.
    // Disable Security Chip.
    //
    mL05WmiSetupItem.L05SecurityChipValid         = FALSE;
    mL05WmiSetupItem.L05ClearSecurityChipKeyValid = FALSE;

  } else {
    //
    // Disable Firmware TPM.
    //
    mL05WmiSetupItem.L05IntelPttValid         = FALSE;
    mL05WmiSetupItem.L05ClearIntelPttKeyValid = FALSE;
    mL05WmiSetupItem.L05AmdPspValid           = FALSE;
    mL05WmiSetupItem.L05ClearAmdPspKeyValid   = FALSE;
  }

  //
  // Sync L05 ODM Function Support to disable unsupport items.
  //
  for (Index = 0; Index < mL05OdmFunctionSupportSyncMapListCount; Index++) {
    if (*mL05OdmFunctionSupportSyncMap[Index].L05OdmFunctionSupport == 0) {
      *mL05OdmFunctionSupportSyncMap[Index].L05WmiSetupItemValid = FALSE;
    }
  }

  CopyMem (L05WmiSetupItemBuffer, &mL05WmiSetupItem, sizeof (L05_WMI_SETUP_ITEM));

  //
  // Update system backup item
  //
  mSystemL05SecureBoot   = mSetupNvData.L05SecureBoot;
  mSystemL05BootPriority = mL05WmiSetupItem.L05BootPriority;

  return Status;
}

/**
  Delete platform setup variable.

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
DeletePlatformSetupVariable (
  VOID
  )
{
  //
  // Delete "Setup" variable
  //
  mSmmVariable->SmmSetVariable (
                  L"Setup",
                  &gSystemConfigurationGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  0,
                  NULL
                  );

#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  //
  // Delete "PchSetup" variable
  //
  mSmmVariable->SmmSetVariable (
                  PCH_SETUP_VARIABLE_NAME,
                  &gPchSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  0,
                  NULL
                  );

  //
  // Delete "SaSetup" variable
  //
  mSmmVariable->SmmSetVariable (
                  SA_SETUP_VARIABLE_NAME,
                  &gSaSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  0,
                  NULL
                  );

  //
  // Delete "CpuSetup" variable
  //
  mSmmVariable->SmmSetVariable (
                  CPU_SETUP_VARIABLE_NAME,
                  &gCpuSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  0,
                  NULL
                  );

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
    //
    // Delete "SgxSetupVariable" variable
    //
    mSmmVariable->SmmSetVariable (
                    SGX_SETUP_VARIABLE_NAME,
                    &gSgxSetupVariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    0,
                    NULL
                    );
#endif

#else
  //
  // Delete "AMD_PBS_SETUP" variable
  //
  mSmmVariable->SmmSetVariable (
                  AMD_PBS_SETUP_VARIABLE_NAME,
                  &gAmdPbsSystemConfigurationGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  0,
                  NULL
                  );
#endif

  return EFI_SUCCESS;
}

/**
  Extend Setting for Save Setup Item.

  @param  None.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.

**/
EFI_STATUS
SaveSetupItemExtendsetting (
  VOID
  )
{
  EFI_STATUS                            Status;

  Status = EFI_SUCCESS;

#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  //
  // Update Intel SGX relate data
  //
  if (mCpuSetupNvData.EnableSgx != mBackupCpuSetupNvData.EnableSgx) {
    //
    // By SgxFormCallBackFunction () [at CpuSetup.c]
    // If EnableSgx is changed to Enable, then PRMRR size will be set to maximum supported size in MB
    // If EnableSgx is changed to Disabled or to Software Controlled, PRMRR size is change to 0.
    // Software Controlled it means the the size is controlled by the software and not by BIOS
    //
    if (mCpuSetupNvData.EnableSgx == 1) {
      mCpuSetupNvData.PrmrrSize = SIZE_128MB;

    } else {
      mCpuSetupNvData.PrmrrSize = CPU_FEATURE_DISABLE;
    }

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
    //
    // By GenericRouteConfig ()
    // Save SGX setup variable
    //
    mSgxSetupNvData.EnableSgx = mCpuSetupNvData.EnableSgx;
    mSgxSetupNvData.PrmrrSize = mCpuSetupNvData.PrmrrSize;

    //
    // Save "SgxSetupVariable" variable
    //
    mSaveSgxSetupNvData = TRUE;
#endif

  }
#endif

  return Status;
}

/**
  Save L05 WMI Setup Item.

  @param  L05WmiSetupItemBuffer         A pointer to the L05 WMI Setup Item buffer.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SaveL05WmiSetupItem (
  IN OUT L05_WMI_SETUP_ITEM             *L05WmiSetupItemBuffer
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  UINTN                                 Index2;
  UINTN                                 SelectValueListMapIndex;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;

  //
  //Load Default Setup Data
  //
  if (mLoadDefaultSetupData) {
    Status = DeletePlatformSetupVariable ();

    return Status;
  }

  //
  // Copy L05 WMI Setup Item by L05 WMI Setup Item Buffer from WmiSetupUnderOsSmm driver
  //
  CopyMem (&mL05WmiSetupItem, L05WmiSetupItemBuffer, sizeof (L05_WMI_SETUP_ITEM));

  //
  // Sync L05 WMI Setup Item to Platform Setup Value
  //
  for (Index = 0; Index < mL05WmiSetupItemSyncMapListCount; Index++) {

    //
    // When setup item is not valid will skip save L05 WMI Setup Item.
    // Without "Secure Boot" item.
    //   [10.1 Secure boot]
    //     When boot mode set to legacy support, the Secure boot option must be disabled and hidden
    //
    if ((*mL05WmiSetupItemSyncMap[Index].L05WmiSetupItemValid == FALSE) && 
        (mL05WmiSetupItemSyncMap[Index].SetupDataType != SecureBootValueType)) {
      continue;
    }

    if (mL05WmiSetupItemSyncMap[Index].SetupDataType < SetupValueTypeMax) {
      //
      // Setup value type to save L05 WMI Setup Item
      //
      if (mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem == NULL) {
        continue;
      }

      if (mL05WmiSetupItemSyncMap[Index].SetupNvDataMap == NULL) {
        continue;
      }

      //
      // Select Value List Map
      //
      for (SelectValueListMapIndex = 0; SelectValueListMapIndex < mWmiSelectValueListMapListCount; SelectValueListMapIndex++) {
        if (mL05WmiSetupItemSyncMap[Index].SetupDataType == mWmiSelectValueListMap[SelectValueListMapIndex].SetupDataType) {
          break;
        }
      }

      //
      // Transform L05 WMI Setup Item Value to Platform Setup Value by Select Value List
      //
      for (Index2 = 0; Index2 < mWmiSelectValueListMap[SelectValueListMapIndex].SelectValueCount; Index2++) {
        if (*mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem == mWmiSelectValueListMap[SelectValueListMapIndex].SelectValueList[Index2]) {
          break;
        }
      }

      if (*mL05WmiSetupItemSyncMap[Index].SetupNvDataMap != (UINT8) Index2) {
        *mL05WmiSetupItemSyncMap[Index].SetupNvDataMap = (UINT8) Index2;
        *mL05WmiSetupItemSyncMap[Index].SaveSetupNvData = TRUE;
      }

    } else {
      //
      // Special type to init L05 WMI Setup Item
      //
      switch (mL05WmiSetupItemSyncMap[Index].SetupDataType) {

      case ClearTpmKeyType:
        if (*mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem) {

          switch (mSetupNvData.TpmDevice) {

          case TPM_DEVICE_1_2:
            mSetupNvData.TpmClear = 1;
            mSaveSetupNvData = TRUE;
            break;

          case TPM_DEVICE_2_0:
            mSetupNvData.Tpm2Operation = 1;
            mSaveSetupNvData = TRUE;
            break;

          default:
            break;
          }

          *mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem = FALSE;
        }

        break;

      case ResetToSetupModeType:
        if (*mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem) {
          mL05SecureBootData.ResetToSystemMode = L05_SECURE_BOOT_ENABLE;
          mL05SecureBootData.RestoreFactoryKeys = L05_SECURE_BOOT_NO_ACTION;
          mSetupNvData.L05PlatformMode   = 1; //0:User Mode, 1:Setup Mode
          mSetupNvData.L05SecureBootMode = 1; //0:Standard,  1:Custom
          mSetupNvData.L05SecureBoot     = 0; //0:Disable,   1:Enable
          mSaveL05SecureBootData = TRUE;
          mSaveSetupNvData = TRUE;

          *mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem = FALSE;
        }

        break;

      case RestoreFactoryKeysType:
        if (*mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem) {
          mL05SecureBootData.ResetToSystemMode  = L05_SECURE_BOOT_NO_ACTION;
          mL05SecureBootData.RestoreFactoryKeys = L05_SECURE_BOOT_ENABLE;
          mSetupNvData.L05PlatformMode   = 0; //0:User Mode, 1:Setup Mode
          mSetupNvData.L05SecureBootMode = 0; //0:Standard,  1:Custom
          mSetupNvData.L05SecureBoot     = 1; //0:Disable,   1:Enable
          mSaveL05SecureBootData = TRUE;
          mSaveSetupNvData = TRUE;

          *mL05WmiSetupItemSyncMap[Index].L05WmiSetupItem = FALSE;
        }

        break;

      case EfiBootOrderType:

        if (mEfiBootOrder == NULL) {
          break;
        }

        if (CompareMem (mL05WmiSetupItem.L05EfiBootOrder, mEfiBootOrder, mEfiBootOrderSize) != 0) {
          mSavePhysicalBootOrder = TRUE;
        }

        //
        // Set Boot Order
        //
        CopyMem (mEfiBootOrder, mL05WmiSetupItem.L05EfiBootOrder, mEfiBootOrderSize);
        SetBootOrder ();

        //
        // Re-get Boot Order & Description
        //
        WmiGetEfiBootOrderAndDescription ();
        break;

      case LegacyBootOrderType:
        if (mLegacyBootOrder == NULL) {
          break;
        }

        if (CompareMem (mL05WmiSetupItem.L05LegacyBootOrder, mLegacyBootOrder, mLegacyBootOrderSize) != 0) {
          mSavePhysicalBootOrder = TRUE;
        }

        //
        // Set Boot Order
        //
        CopyMem (mLegacyBootOrder, mL05WmiSetupItem.L05LegacyBootOrder, mLegacyBootOrderSize);
        SetBootOrder ();

        //
        // Re-get Boot Order & Description
        //
        WmiGetLegacyBootOrderAndDescription ();
        break;
      }
    }
  }

  //
  // Extend Setting for Save Setup Item.
  //
  Status = SaveSetupItemExtendsetting ();

  //
  // Set Platform Setup Variable
  //
  Status = SetPlatformSetupVariable ();

  //
  // Set Secure Boot Data
  //
  Status = SetSecureBootData ();

  return Status;
}

/**
  Backup platform setup variable.

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
BackupPlatformSetupVariable (
  VOID
  )
{
  EFI_STATUS                            Status;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;

  if (mAlreadyBackupSetup) {
    return Status;
  }

  //
  // Backup platform setup variable data
  //
  CopyMem (&mBackupSetupNvData,       &mSetupNvData,       sizeof (SYSTEM_CONFIGURATION));
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  CopyMem (&mBackupPchSetupNvData,    &mPchSetupNvData,    sizeof (PCH_SETUP));
  CopyMem (&mBackupSaSetupNvData,     &mSaSetupNvData,     sizeof (SA_SETUP));
  CopyMem (&mBackupCpuSetupNvData,    &mCpuSetupNvData,    sizeof (CPU_SETUP));
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
  CopyMem (&mBackupSgxSetupNvData,    &mSgxSetupNvData,    sizeof (SGX_SETUP_DATA));
#endif
#else
  CopyMem (&mBackupAmdPbsSetupNvData, &mAmdPbsSetupNvData, sizeof (AMD_PBS_SETUP_OPTION));
#endif
  CopyMem (mBackupPhysicalBootOrder,  mPhysicalBootOrder,  mPhysicalBootOrderSize);

  mAlreadyBackupSetup = TRUE;

  return Status;
}

/**
  Discard L05 WMI Setup Item.

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
DiscardL05WmiSetupItem (
  VOID
  )
{
  EFI_STATUS                            Status;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;

  //
  // Restore platform setup variable data
  //
  CopyMem (&mSetupNvData,       &mBackupSetupNvData,       sizeof (SYSTEM_CONFIGURATION));
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  CopyMem (&mPchSetupNvData,    &mBackupPchSetupNvData,    sizeof (PCH_SETUP));
  CopyMem (&mSaSetupNvData,     &mBackupSaSetupNvData,     sizeof (SA_SETUP));
  CopyMem (&mCpuSetupNvData,    &mBackupCpuSetupNvData,    sizeof (CPU_SETUP));
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
  CopyMem (&mSgxSetupNvData,    &mBackupSgxSetupNvData,    sizeof (SGX_SETUP_DATA));
#endif
#else
  CopyMem (&mAmdPbsSetupNvData, &mBackupAmdPbsSetupNvData, sizeof (AMD_PBS_SETUP_OPTION));
#endif
  CopyMem (mPhysicalBootOrder,  mBackupPhysicalBootOrder,  mPhysicalBootOrderSize);

  //
  // Set Save Flag
  //
  mSaveSetupNvData       = TRUE;
#if (L05_CHIPSET_VENDOR_ID == L05_INTEL_VENDOR_ID)
  mSavePchSetupNvData    = TRUE;
  mSaveSaSetupNvData     = TRUE;
  mSaveCpuSetupNvData    = TRUE;
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
  mSaveSgxSetupNvData    = TRUE;
#endif
#else
  mSaveAmdPbsSetupNvData = TRUE;
#endif
  mSavePhysicalBootOrder = TRUE;

  //
  // Delete WMI Secure Boot change
  //
  mSmmVariable->SmmSetVariable (
                  L05_WMI_SECURE_BOOT_DATA_VARIABLE_NAME,
                  &gL05WmiSetupUnderOsSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  0,
                  NULL
                  );

  return Status;
}

/**
  L05 WMI Setup Item load default .

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
LoadDefaultL05WmiSetupItem (
  VOID
  )
{
  EFI_STATUS                            Status;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;

  mLoadDefaultSetupData = TRUE;

  return Status;
}

/**
  Return TRUE if SGX in Feature Control MSR was set.

  @param  None

  @retval TRUE                          If SGX in Feature Control MSR was set.
          FALSE                         If SGX in Feature Control MSR was not set.
**/
BOOLEAN
L05WmiIsSgxFeatureCtrlSet (
  VOID
  )
{
  BOOLEAN                               SgxState;

  SgxState = FALSE;

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
  SgxState = IsSgxFeatureCtrlSet ();
#endif

  return SgxState;
}

/**
  Provides WMI Setup under OS set Setup Item.
  It will be called in one cases as below,
  L05WmiSetupItemInit, L05WmiSetupItemSave, L05WmiSetupItemDiscard & L05WmiSetupItemLoadDefault.

  @param  *SmmVariable                  A pointer to the SMM variable protocol.
  @param  SetupItemAction               Setup item action.
  @param  *L05WmiSetupItemBuffer        A pointer to the L05 WMI Setup Item buffer.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
L05WmiSetupItemFunc (
  IN EFI_SMM_VARIABLE_PROTOCOL          *SmmVariable,
  IN L05_WMI_SETUP_ITEM_ACTION          SetupItemAction,
  IN OUT L05_WMI_SETUP_ITEM             *L05WmiSetupItemBuffer
  )
{

  //
  // Set SMM variable protocol
  //
  mSmmVariable = SmmVariable;

  switch (SetupItemAction) {

  case L05WmiSetupItemInit:
    InitL05WmiSetupItem (L05WmiSetupItemBuffer, TRUE);
    BackupPlatformSetupVariable ();
    break;

  case L05WmiSetupItemSave:
    SaveL05WmiSetupItem (L05WmiSetupItemBuffer);
    InitL05WmiSetupItem (L05WmiSetupItemBuffer, FALSE);
    break;

  case L05WmiSetupItemDiscard:
    DiscardL05WmiSetupItem ();
    InitL05WmiSetupItem (L05WmiSetupItemBuffer, FALSE);
    break;

  case L05WmiSetupItemLoadDefault:
    LoadDefaultL05WmiSetupItem ();
    break;
  }

  return EFI_SUCCESS;
}
