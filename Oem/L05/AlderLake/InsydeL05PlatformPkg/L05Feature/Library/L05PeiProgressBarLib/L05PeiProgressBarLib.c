/** @file
  Draw Progress Bar

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "L05PeiProgressBarLibInternal.h"

EFI_PEI_GRAPHICS_INFO_HOB               *mGraphicsInfo     = NULL;
EFI_GRAPHICS_OUTPUT_BLT_PIXEL           *mScalingBarBlt    = NULL;
EFI_GRAPHICS_OUTPUT_BLT_PIXEL           mBarColor          = {0xFF, 0xFF, 0x00, 0x00};
UINTN                                   mCurrentCompletion = 0;
BOOLEAN                                 mGraphicsIsReady   = FALSE;

/**
  Initialize the graphics device, graphics information and the buffer before paint the progress bar.

  @retval  EFI_SUCCESS                  Initialize successfully.
  @retval  Others                       Initialize failed.
**/
EFI_STATUS
ProgressBarInit (
  VOID
  )
{
  EFI_STATUS                            Status;
  SI_PREMEM_POLICY_PPI                  *SiPreMemPolicy;
  GRAPHICS_PEI_PREMEM_CONFIG            *GtPreMemConfig;
  UINTN                                 PciCfgBase;
  LARGE_INTEGER                         GmAdrValue;
  VOID                                  *HobStart;
  UINTN                                 Border;
  UINTN                                 Width;
  UINTN                                 Height;
  UINTN                                 ScalingBarBltSize;
  UINTN                                 X;
  UINTN                                 Y;

  DEBUG ((DEBUG_INFO, "%a: Start\n", __FUNCTION__));

  SiPreMemPolicy    = NULL;
  GtPreMemConfig    = NULL;
  PciCfgBase        = 0;
  HobStart          = NULL;
  Border            = 0;
  Width             = 0;
  Height            = 0;
  ScalingBarBltSize = 0;
  X                 = 0;
  Y                 = 0;

  //
  // Get Si PreMem Policy settings through the SiPreMemPolicy PPI
  //
  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicy
             );
  if (EFI_ERROR (Status)) {
    ASSERT (FALSE);
    return Status;
  }

  //
  // Initialize Gttmmadr and Gmadr
  //
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gGraphicsPeiPreMemConfigGuid, (VOID *) &GtPreMemConfig);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  DEBUG ((DEBUG_INFO, "GttMmAdr: %x\n", GtPreMemConfig->GttMmAdr));
  DEBUG ((DEBUG_INFO, "GmAdr64: %x\n", GtPreMemConfig->GmAdr64));

  PciCfgBase = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);

  PciSegmentWrite32 (PciCfgBase + R_SA_IGD_GTTMMADR, GtPreMemConfig->GttMmAdr);

  GmAdrValue.Data = GtPreMemConfig->GmAdr64;
  PciSegmentWrite32 (PciCfgBase + R_SA_IGD_GMADR, GmAdrValue.Data32.Low);
  PciSegmentWrite32 (PciCfgBase + R_SA_IGD_GMADR + 4, GmAdrValue.Data32.High);

  PciSegmentWrite32 (PciCfgBase + PCI_COMMAND_OFFSET, (UINT32) (EFI_PCI_COMMAND_BUS_MASTER | EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_IO_SPACE));

  //
  // Get Graphics Information from gEfiGraphicsInfoHobGuid HOB
  //
  HobStart = GetFirstGuidHob (&gEfiGraphicsInfoHobGuid);

  if ((HobStart == NULL) || (GET_GUID_HOB_DATA_SIZE (HobStart) < sizeof (EFI_PEI_GRAPHICS_INFO_HOB))) {
    DEBUG ((DEBUG_ERROR, "gEfiGraphicsInfoHobGuid HOB doesn't exist.\n"));
    return EFI_UNSUPPORTED;
  }

  mGraphicsInfo = (EFI_PEI_GRAPHICS_INFO_HOB *) (GET_GUID_HOB_DATA (HobStart));

  if (mGraphicsInfo == NULL) {
    DEBUG ((DEBUG_ERROR, "Failed to retrieve gEfiGraphicsInfoHobGuid HOB data.\n"));
    return EFI_UNSUPPORTED;
  }

  DEBUG ((DEBUG_INFO, "FrameBufferBase = 0x%x\n", mGraphicsInfo->FrameBufferBase));

  //
  // Initialize progress bar
  //
  mCurrentCompletion = PROGRESS_ACCURACY + 1;
  Border             = PROGRESS_BAR_DEFAULT_BORDER;
  Width              = PROGRESS_BAR_DEFAULT_WIDTH;
  Height             = PROGRESS_BAR_DEFAULT_HEIGHT;
  ScalingBarBltSize  = (Width * Height) * sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL);

  mScalingBarBlt     = AllocateZeroPool (ScalingBarBltSize);

  if (mScalingBarBlt == NULL) {
    DEBUG ((DEBUG_ERROR, "Failed to allocate resources for mScalingBarBlt.\n"));
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Fill the border of progress bar
  //
  for (Y = 0; Y < Height; Y++) {
    for (X = 0; X < Width; X++) {
      if ((X < Border) ||
          (Y < Border) ||
          (X > (Width - Border - 1)) ||
          (Y > (Height - Border - 1))) {
        mScalingBarBlt[X + (Y * Width)] = mBarColor;
      } 
    }
  }

  //
  // Set up Ready flag
  //
  mGraphicsIsReady = TRUE;

  DEBUG ((DEBUG_INFO, "%a: End\n", __FUNCTION__));

  return EFI_SUCCESS;
}

/**
  The exit work after the painting is done.

  @retval  EFI_SUCCESS                  Exit successfully.
**/
EFI_STATUS
ProgressBarExit (
  VOID
  )
{
  UINTN                                 PciCfgBase;

  DEBUG ((DEBUG_INFO, "%a: Start\n", __FUNCTION__));

  //
  // Disable Gttmmadr and Gmadr MMIO after we are done with display
  //
  PciCfgBase = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  MmioWrite32 (PciCfgBase + PCI_COMMAND_OFFSET, 0);

  //
  // Release resources
  //
  mGraphicsInfo = NULL;

  if (mScalingBarBlt != NULL) {
    FreePool (mScalingBarBlt);
  }

  mGraphicsIsReady = FALSE;

  DEBUG ((DEBUG_INFO, "%a: End\n", __FUNCTION__));

  return EFI_SUCCESS;
}

/**
  Fill the graphics frame buffer and show the progress bar by completion.

  @param  Completion                    The completion of the progress.
**/
VOID
PaintProgressBar (
  IN  UINTN                             Completion
  )
{
  UINTN                                 Width;
  UINTN                                 Height;
  UINTN                                 CompletionWidth;
  UINTN                                 X;
  UINTN                                 Y;
  UINTN                                 LogoDestX;
  UINTN                                 LogoDestY;
  UINTN                                 BytesPerScanLine;
  UINTN                                 DstY;
  UINTN                                 SrcY;
  UINT8                                 *DstAddressT;
  UINT8                                 *SrcAddressT;

  DEBUG ((DEBUG_INFO, "%a: Start\n", __FUNCTION__));

  Width            = 0;
  Height           = 0;
  CompletionWidth  = 0;
  X                = 0;
  Y                = 0;
  LogoDestX        = 0;
  LogoDestY        = 0;
  BytesPerScanLine = 0;
  DstY             = 0;
  SrcY             = 0;
  DstAddressT      = NULL;
  SrcAddressT      = NULL;

  if (!mGraphicsIsReady) {
    DEBUG ((DEBUG_ERROR, "The previous initialization was not completed.\n"));
    return;
  }

  if (mGraphicsInfo == NULL || mScalingBarBlt == NULL ) {
    DEBUG ((DEBUG_ERROR, "mGraphicsInfo or mScalingBarBlt is NULL.\n"));
    return;
  }

  if (Completion == mCurrentCompletion) {
    //
    // Do nothing if the progress completion is the same
    //
    return;
  }

  mCurrentCompletion = Completion;

  Width  = PROGRESS_BAR_DEFAULT_WIDTH;
  Height = PROGRESS_BAR_DEFAULT_HEIGHT;
  CompletionWidth = Completion * Width / PROGRESS_ACCURACY;
  LogoDestX = (mGraphicsInfo->GraphicsMode.HorizontalResolution - Width) / 2;
  LogoDestY = (mGraphicsInfo->GraphicsMode.VerticalResolution - Height) / 2;
  BytesPerScanLine = mGraphicsInfo->GraphicsMode.PixelsPerScanLine * sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL);  
  
  for (Y = 0; Y < Height; Y++) {
    for (X = 0; X < CompletionWidth; X++) {
      mScalingBarBlt[X + (Y * Width)] = mBarColor;
    }
  }

  //
  // Fill frame buffer with the logo line by line
  //
  for (SrcY = 0, DstY = LogoDestY; DstY < (LogoDestY + Height); SrcY++, DstY++) {
    DstAddressT = (UINT8 *) (UINTN) (mGraphicsInfo->FrameBufferBase + DstY * BytesPerScanLine + LogoDestX * sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL));
    SrcAddressT = (UINT8 *) ((UINT8 *) mScalingBarBlt + (SrcY * Width * sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL)));
    CopyMem (DstAddressT, SrcAddressT, Width * sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL));
  }

  DEBUG ((DEBUG_INFO, "%a: End\n", __FUNCTION__));

  return;
}
