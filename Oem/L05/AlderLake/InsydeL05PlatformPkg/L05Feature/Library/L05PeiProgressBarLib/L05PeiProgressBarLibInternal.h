/** @file
  Draw Progress Bar

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_PEI_PROGRESS_BAR_LIB_INTERNAL_H_
#define _L05_PEI_PROGRESS_BAR_LIB_INTERNAL_H_

#include <Uefi.h>
#include <IndustryStandard/Pci22.h>
#include <Core/Pei/PeiMain.h>
#include <Guid/GraphicsInfoHob.h>
#include <Library/BaseMemoryLib.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/PeiSaPolicyLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/L05PeiProgressBarLib.h>
#include <Protocol/GraphicsOutput.h>
#include <Fru/TglCpu/IncludePrivate/Register/IgdRegs.h>

#define PROGRESS_BAR_DEFAULT_WIDTH      400
#define PROGRESS_BAR_DEFAULT_HEIGHT     35
#define PROGRESS_BAR_DEFAULT_BORDER     3

#pragma pack(1)

typedef union {
  struct {
    UINT32  Low;
    UINT32  High;
  } Data32;
  UINT64 Data;
} LARGE_INTEGER;

#pragma pack()

#endif
