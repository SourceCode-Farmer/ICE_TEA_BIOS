/** @file
  Cloud Boot DXE Driver.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CLOUD_BOOT_DXE_H_
#define _CLOUD_BOOT_DXE_H_

#include <Uefi.h>
#include <SetupConfig.h>

//
// Libraries
//
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/DevicePathLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/PrintLib.h>
#include <Library/VariableLib.h>
#include <Library/H2OCpLib.h>
#include <Library/GenericBdsLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>

//
// Consumed Protocols
//
#include <Protocol/BlockIo.h>
#include <Protocol/DevicePath.h>

//
// Produced Protocols
//
#include <Protocol/L05CloudBoot.h>

//
// Guids
//

//
// Include files with function prototypes
//
#define TIMER_EVENT_ONE_MILLISEC                             10000
#define MILLISEC                                             1000
#define WIFI_WAITING_CONNECT_TIMEOUT_SEC                     30

typedef struct {
  UINT8                                 NetwortType;   // Record Ipv4 or Ipv6
  UINT16                                BootOrderList; // The Number is BootOption's number. ex: BootOrderList[0]= 0001, and 0001 is the number.
} TEMP_SAVE_NETWORK_INDEX;

#endif
