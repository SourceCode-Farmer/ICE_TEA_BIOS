/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _LIMITED_TO_INTERNAL_STORAGE_H_
#define _LIMITED_TO_INTERNAL_STORAGE_H_

BOOLEAN
L05IsLimitedToInternalStorage (
  VOID
  );

EFI_STATUS
LimitedToInternalStorage (
  VOID
  );

#endif
