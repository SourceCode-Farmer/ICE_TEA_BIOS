//;******************************************************************************
//;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#ifndef _EFI_BOOT_DEVICE_INFO_H_
#define _EFI_BOOT_DEVICE_INFO_H_

#include <Protocol/LegacyBios.h>
#include <IndustryStandard/Pci22.h>
#include "L05Config.h"

#define EFI_L05_SATA_LOCATION_TABLE_MAX    0x10

#define EFI_L05_CSM_SIGNATURE              SIGNATURE_32 ('I', 'F', 'E', '$')
#define EFI_L05_CSM_REGION_START           0x0FE000
#define EFI_L05_CSM_REGION_END             0x100000
#define EFI_L05_CSM_VERSION_FOR_USB_CLASS  0x1452

BOOLEAN
L05IsUsbDevice (
  IN BBS_TABLE                          *CurrentBbsTable
  );

BOOLEAN
L05IsThisInternalHddInfo (
  IN  BBS_TABLE                         *CurrentBbsTable,
  IN  UINTN                             TableIndex
  );

BOOLEAN
L05IsThisSecondHddInfo (
  IN  BBS_TABLE                         *CurrentBbsTable,
  IN  UINTN                             TableIndex
  );

BOOLEAN
L05IsThisSataOddInfo (
  IN  BBS_TABLE                         *CurrentBbsTable,
  IN  UINTN                             TableIndex
  );

UINT16
L05DistinguishSataHddType (
  IN  BBS_TABLE                         *CurrentBbsTable,
  IN  UINTN                             TableIndex
  );

UINT16
L05DistinguishEfiSataHddType (
  IN  UINTN                             PortNumber
  );

#endif // _EFI_L05_BOOT_DEVICE_INFO_H_
