/** @file


;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <BootOrderUpdateForNovoRecovery.h>
#include <L05Config.h>
#include <Library/PrintLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseLib.h>
#include <Protocol/NovoRecovery.h>
#include <Guid/GlobalVariable.h>
//#include <Uefi/UefiSpec.h>

/**
  This function use boot option number to get the device name

  @param   BootOptionNo                 It mean boot option number
  @param   DeviceName                   Points to the spece of saving boot option description

  @retval  EFI_SUCCESS                  The Results is processed successfully.
  @retval  OTHERS                       The Results mean function doesn't find the correspond data.
**/
EFI_STATUS
L05BootOptionNoToName (
  IN     UINT16                         BootOptionNo,
  OUT    CHAR16                         *DeviceName
  )
{
  EFI_STATUS                            Status;
  CHAR16                                BootString[10];
  UINT8                                 *Buffer;
  UINTN                                 BufferSize;
  UINT8                                 *Ptr;
  UINTN                                 DeviceNameSize;

  Status         = EFI_SUCCESS;
  Buffer         = NULL;
  BufferSize     = 0;
  Ptr            = NULL;
  DeviceNameSize = 0;

  //
  //  Get "Boot####" variable.
  //
  CopyMem (BootString, L"Boot", 4 * sizeof (CHAR16));

  UnicodeValueToStringS (&(BootString[4]), sizeof (BootString) - (4 * sizeof (CHAR16)), PREFIX_ZERO | RADIX_HEX, BootOptionNo, 4);
  Status = gRT->GetVariable (BootString, &gEfiGlobalVariableGuid, NULL, &BufferSize, Buffer);

  if (Status == EFI_BUFFER_TOO_SMALL) {
    Buffer = AllocateZeroPool (BufferSize);

    if (Buffer == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }

    Status = gRT->GetVariable (BootString, &gEfiGlobalVariableGuid, NULL, &BufferSize, Buffer);

    if (EFI_ERROR (Status)) {
      gBS->FreePool (Buffer);
      return Status;
    }
  }

  //
  //  Parse the data in "Boot####" variable to get Device type
  //
  Ptr = Buffer;
  Ptr += sizeof (UINT32);   // skip Load Option Attributes
  Ptr += sizeof (UINT16);   // skip DevicePathSize
  DeviceNameSize = StrSize ((UINT16 *) Ptr);

  CopyMem (DeviceName, Ptr, DeviceNameSize);

  FreePool (Buffer);
  return EFI_SUCCESS;
}

/**
  Update Boot Order for Novo Recovery.

  @param   BootOrder                    Points to the description of device
  @param   BootDeviceNum Points         to the BbsTable of device.
  @param   IsGetBootList                This parameter mean current index of boot option

  @retval  EFI_SUCCESS                  The Results is processed successfully.
  @retval  EFI_UNSUPPORTED              The Results mean function doesn't find the correspond data.
**/
EFI_STATUS
L05UpdateBootOrderForNovoRecovery (
  IN OUT UINT16                         *BootOrder,
  IN OUT UINT16                         *BootDeviceNum,
  IN     BOOLEAN                        IsGetBootList
  )
{
  EFI_STATUS                            Status;
  UINT16                                Index;
  UINT16                                TempIndex;
  CHAR16                                TempString[0x50];
  CHAR16                                *NovoRecoveryString;
  UINT16                                L05NovoRecoverySystemNumber;

  Index              = 0;
  TempIndex          = 0;
  NovoRecoveryString = L05_NOVO_RECOVERY_SYSTEM_NAME;

  L05NovoRecoverySystemNumber = (UINT16) L05_INVALID_VALUE;

  if (IsGetBootList) {
    for (Index = 0; Index < *BootDeviceNum; Index++) {
      ZeroMem (TempString, 0x50 * sizeof (CHAR16));
      Status = L05BootOptionNoToName (BootOrder[Index], TempString);

      if (CompareMem (NovoRecoveryString, TempString, StrSize (NovoRecoveryString)) == 0x0) {
        TempIndex = Index;
        L05NovoRecoverySystemNumber = BootOrder[Index];
        break;
      }
    }

    if (L05NovoRecoverySystemNumber != (UINT16) L05_INVALID_VALUE) {
      for (Index = TempIndex; Index < (*BootDeviceNum - 1); Index++) {
        BootOrder[Index] = BootOrder[Index + 1];
      }

      *BootDeviceNum = *BootDeviceNum - 1;
    }

  } else {
    //
    // If we have hidden Novo Recovery System, we need to add it to last of Boot Order.
    //
    if (L05NovoRecoverySystemNumber != (UINT16) L05_INVALID_VALUE) {
      BootOrder[*BootDeviceNum] = L05NovoRecoverySystemNumber;
      *BootDeviceNum = *BootDeviceNum + 1;
    }
  }

  return EFI_SUCCESS;
}

