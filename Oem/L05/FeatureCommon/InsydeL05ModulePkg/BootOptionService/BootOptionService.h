//;******************************************************************************
//;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#ifndef _BOOT_OPTION_SERVICE_H_
#define _BOOT_OPTION_SERVICE_H_

#include <Uefi.h>

//
// Libraries
//
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/DevicePathLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/PrintLib.h>
#include <Library/FeatureLib/OemSvcSetBootTypeOrder.h>
#include <Library/VariableLib.h>
#include <Library/GenericBdsLib.h>

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//

//
// Produced Protocols
//

//
// Guids
//

//
// Driver Version
//

//
// Protocol instances
//
#include <Protocol/LegacyBios.h>
#include <Protocol/SetupUtility.h>
#include <Protocol/NovoRecovery.h>
#include <Protocol/L05BootOption.h>
#include <Protocol/NovoRecovery.h>
#include <Protocol/SimpleFileSystem.h>

//
// Include files with function prototypes
//
#include "BootDeviceInfo.h"
#include "LimitedToInternalStorage.h"
#include <SetupConfig.h>
#include <L05Config.h>
#include <Protocol/BlockIo.h>
#include <Protocol/UsbIo.h>
#include <Protocol/DiskInfo.h>
#include <Protocol/UsbMassTransport.h>
#include <Library/UefiLib.h>
#include <Protocol/UsbCore.h>
#include "BootDeviceTitleString.h"
#include <Library/DebugLib.h>
#include <BootOrderUpdateForNovoRecovery.h>
#include <Protocol/DevicePath.h>
#include <Protocol/L05CloudBoot.h>

#define L05_MAX_DEVICE_NAME_SIZE        100
#define L05_MAX_BOOT_ORDER              100
#define L05_BOOT_ORDER_NOT_FOUND        0xFFFF
#define L05_UNKNOWN_DEVICE_TYPE         0xFFFF
#define L05_EFI_TYPE_MASK               0xE000

extern EFI_GUID gEfiGlobalVariableGuid;

#define  USB_MASS_SIGNATURE             SIGNATURE_32 ('U', 's', 'b', 'K')
#define USB_MASS_DEVICE_FROM_BLOCKIO(a) \
        CR (a, USB_MASS_DEVICE, BlockIo, USB_MASS_SIGNATURE)

//
// Copy form Insyde\InsydeModulePkg\Include\Library\GenericBdsLib.h
//
#define IS_USB_SHORT_FORM_DEVICE_PATH(DevPath)  (BOOLEAN) ((DevPath) != NULL && \
                                                           ((EFI_DEVICE_PATH_PROTOCOL *)(DevPath))->Type == MESSAGING_DEVICE_PATH && \
                                                           ((EFI_DEVICE_PATH_PROTOCOL *)(DevPath))->SubType == MSG_USB_WWID_DP)

typedef struct _USB_MASS_DEVICE         USB_MASS_DEVICE;

typedef struct {
  UINT8                                 Pdt;            // Peripheral Device Type (low 5 bits)
  UINT8                                 Removable;      // Removable Media (highest bit)
  UINT8                                 Reserved0[2];
  UINT8                                 AddLen;         // Additional length
  UINT8                                 Reserved1[3];
  UINT8                                 VendorID[8];
  UINT8                                 ProductID[16];
  UINT8                                 ProductRevision[4];
} USB_BOOT_INQUIRY_DATA;

struct _USB_MASS_DEVICE {
  UINT32                                Signature;
  EFI_HANDLE                            Controller;
  EFI_USB_IO_PROTOCOL                   *UsbIo;
  EFI_BLOCK_IO_PROTOCOL                 BlockIo;
  EFI_BLOCK_IO_MEDIA                    BlockIoMedia;
  EFI_DISK_INFO_PROTOCOL                DiskInfo;
  UINT8                                 OpticalStorage : 1;
  UINT8                                 ScsiInterface : 1;
  UINT8                                 Reserved : 6;
  UINT8                                 Lun;          // Logical Unit Number
  UINT8                                 Pdt;          // Peripheral Device Type
  UINT8                                 MediumType;   // MediumType for FDD 720/1.44
  USB_MASS_TRANSPORT_PROTOCOL           *Transport;   // USB mass storage transport protocol
  VOID                                  *Context;     // Opaque storage for mass transport
  USB_BOOT_INQUIRY_DATA                 InquiryData;
  EFI_UNICODE_STRING_TABLE              *DeviceNameTable;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePath;
  EFI_USB_CORE_PROTOCOL                 *UsbCore;
};

typedef struct {
  UINT8                                 NetwortType;   // Record Ipv4 or Ipv6
  UINT16                                BootOrderList; // The Number is BootOption's number. ex: BootOrderList[0]= 0001, and 0001 is the number.
} TEMP_SAVE_NETWORK_INDEX;

EFI_STATUS
L05SortPxeOfBootOrder (
  IN OUT UINT16                         *BootOrderList,
  IN OUT UINTN                          BootOrderNumber
  );

CHAR16 *
L05Ascii2Unicode (
  OUT CHAR16                            *UnicodeStr,
  IN  CHAR8                             *AsciiStr
  );

BOOLEAN
L05IsUsbMemoryDevice (
  IN  EFI_DEVICE_PATH_PROTOCOL          *DevicePathPtr
  );

EFI_STATUS
L05CheckLegacyDeviceType (
  IN      BBS_TABLE                     *BbsItem,
  IN OUT  UINT16                        *DeviceType,
  IN      CHAR16                        *DeviceName,
  IN      UINTN                         Index
  );

BOOLEAN
L05IsEfiDeviceType (
  IN  UINT16                            DeviceType
  );

EFI_STATUS
L05DevicePathToBootType (
  IN  EFI_DEVICE_PATH_PROTOCOL          *DevicePathPtr,
  OUT UINT16                            *BootType
  );

EFI_STATUS
L05ModifyBootOrder (
  IN UINT8                              BootNormalPriority,
  IN UINT16                             NewPositionPolicy,
  IN BBS_TABLE                          *CurrentBbsItem,
  IN UINTN                              BbsTableIndex,
  IN OUT UINT16                         *BootOrderList,
  IN OUT UINTN                          *BootOrderListSize
  );

EFI_STATUS
L05ModifyBootOptionPosition (
  IN UINT8                              BootNormalPriority,
  IN UINT16                             *BootOrderList,
  IN UINTN                              BootOrderNum,
  IN EFI_DEVICE_PATH_PROTOCOL           *DevicePath,
  OUT UINTN                             *NewPosition
  );

EFI_STATUS
L05SortBootOrderByDefault (
  IN OUT UINT16                         *BootOrderList,
  IN     UINTN                          BootOrderNumber,
  IN     UINT8                          BootType
  );

EFI_STATUS
L05RemoveDeviceTypeFromBootOrder (
  IN OUT UINT16                         *BootOrderList,
  IN OUT UINT16                         *BootOrderNumber,
  IN     UINT16                         DeviceType
  );

EFI_STATUS
L05MoveHddToFrontOfBootOrder (
  IN OUT UINT16                         *BootOrderList,
  IN OUT UINTN                          BootOrderNumber
  );

EFI_STATUS
InstallProjectModifyBootOrderHook (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  );

#endif
