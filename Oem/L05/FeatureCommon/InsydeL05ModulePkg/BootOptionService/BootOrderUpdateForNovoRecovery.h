/** @file


;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_BOOT_ORDER_UPDATE_FOR_NOVORECOVERY_H
#define _L05_BOOT_ORDER_UPDATE_FOR_NOVORECOVERY_H

EFI_STATUS
L05BootOptionNoToName (
  IN     UINT16                         BootOptionNo,
  OUT    CHAR16                         *DeviceName
  );

EFI_STATUS
L05UpdateBootOrderForNovoRecovery (
  IN OUT UINT16                         *BootOrder,
  IN OUT UINT16                         *BootDeviceNum,
  IN     BOOLEAN                        IsGetBootList
  );

#endif
