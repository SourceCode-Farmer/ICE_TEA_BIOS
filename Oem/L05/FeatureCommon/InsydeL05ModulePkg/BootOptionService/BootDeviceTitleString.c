/** @file
  Boot Device title string relative functions

;******************************************************************************
;* Copyright (c) 2012 - 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <L05Config.h>
#include <L05Project.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Protocol/LegacyBios.h>
#include <Protocol/L05BootOption.h>
#include "BootDeviceTitleString.h"
#include "BootDeviceInfo.h"

//
// At Insyde\InsydeModulePkg\Library\GenericBdsLib\BdsMisc.c
//
VOID
CleanSpaceChar (
  IN  CHAR16                            *Str
  );

/**
  This function modify description of Legacy boot device.

  @param  Desc                          Points to the description of device.
  @param  CurrentBbsTable               Points to the BbsTable of device.
  @param  TableIndex                    This parameter mean current index of boot option.

  @retval EFI_SUCCESS                   The Results is processed successfully.
  @retval EFI_UNSUPPORTED               The Results mean function doesn't find the correspond data.
  @retval EFI_OUT_OF_RESOURCES          The Results mean function allocate pool fail.
**/
EFI_STATUS
L05ChangeDeviceType (
  IN OUT CHAR16                         **Desc,
  IN     BBS_TABLE                      *CurrentBbsTable,
  IN     UINTN                          TableIndex
  )
{
  UINT16                                DeviceType;
  CHAR16                                TempString[L05_MAX_TITLE_STRING_LENGTH];
  EFI_STATUS                            Status;
  CHAR16                                *NewString;
  UINTN                                 NewStringSize;

  DeviceType    = 0;
  NewString     = NULL;
  Status        = EFI_UNSUPPORTED;
  NewStringSize = 0;

  if (*Desc == NULL || CurrentBbsTable == NULL) {
    return EFI_UNSUPPORTED;
  }


  switch (CurrentBbsTable->DeviceType) {

  case BBS_HARDDISK:
    if (L05IsUsbDevice (CurrentBbsTable)) {
      DeviceType = L05_BOOT_TYPE_BBS_USB_HDD;

    } else {
      DeviceType = L05DistinguishSataHddType (CurrentBbsTable, TableIndex);
    }

    break;

  case BBS_CDROM:
    if (L05IsUsbDevice (CurrentBbsTable)) {
      DeviceType = L05_BOOT_TYPE_BBS_USB_CDROM;

    } else {
      DeviceType = BBS_CDROM;
    }

    break;

  case BBS_FLOPPY:
    if (L05IsUsbDevice (CurrentBbsTable)) {
      DeviceType = L05_BOOT_TYPE_BBS_USB_FLOPPY;

    } else {
      DeviceType = BBS_FLOPPY;
    }

    break;

  case BBS_BEV_DEVICE:
    DeviceType = BBS_BEV_DEVICE;
    break;

  default:
    return EFI_UNSUPPORTED;
  }

  ZeroMem (TempString, sizeof (TempString));
  Status = L05GetDeviceTypeTitle (DeviceType, TempString);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  CleanSpaceChar (*Desc);

  NewStringSize = (StrLen (TempString) + StrLen (*Desc) + 1) * sizeof (CHAR16);
  NewString = (CHAR16 *) AllocateZeroPool (NewStringSize);

  if (NewString == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  StrCpyS (NewString, NewStringSize / sizeof (CHAR16), TempString);
  StrCatS (NewString, NewStringSize / sizeof (CHAR16), *Desc);
  FreePool (*Desc);
  *Desc = NewString;

  return Status;
}

/**
  This function change device title string by device type.

  @param  DeviceType                    Points to the description of device.
  @param  DeviceTitleString             Points to the string buffer.

  @retval EFI_SUCCESS                   The Results is processed successfully.
  @retval EFI_UNSUPPORTED               There are no correspond device type in EFI_L05_BOOT_DEVICE_NAME_LIST.
**/
EFI_STATUS
L05GetDeviceTypeTitle (
  IN       UINT16                       DeviceType,
  IN OUT   CHAR16                       *DeviceTitleString
  )
{
  UINTN                                 Index;
  EFI_L05_BOOT_DEVICE_NAME              BootDeviceNameList[] = {EFI_L05_BOOT_DEVICE_NAME_LIST};

  Index = 0;

  if (BootDeviceNameList == NULL) {
    return EFI_UNSUPPORTED;
  }

  //
  //  1. Compare DeviceType(Input) and the boot device list of Lenovo
  //  2. Change the device titile string by device type
  //
  for (Index = 0; Index < EFI_L05_BOOT_DEVICE_NAME_TABLE_MAX; Index++) {
    if (BootDeviceNameList[Index].DeviceType == L05_BOOT_TYPE_END_OF_LIST) {
      break;

    } else if (BootDeviceNameList[Index].DeviceType == DeviceType) {
      StrCpyS (DeviceTitleString, L05_MAX_TITLE_STRING_LENGTH, BootDeviceNameList[Index].NameString);
      return EFI_SUCCESS;
    }
  }

  return EFI_UNSUPPORTED;
}
