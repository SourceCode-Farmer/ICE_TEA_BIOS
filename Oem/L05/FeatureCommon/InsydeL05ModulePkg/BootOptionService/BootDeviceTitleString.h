/** @file
  Boot device title string relative functions

;******************************************************************************
;* Copyright (c) 2012 - 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_BOOTMENU_FUNC_H
#define _L05_BOOTMENU_FUNC_H

#define L05_MAX_TITLE_STRING_LENGTH     64

EFI_STATUS
L05GetDeviceTypeTitle (
  IN       UINT16                       DeviceType,
  IN OUT   CHAR16                       *DeviceTitleString
  );

EFI_STATUS
L05ChangeDeviceType (
  IN OUT CHAR16                         **Desc,
  IN     BBS_TABLE                      *CurrentBbsTable,
  IN     UINTN                          TableIndex
  );

#endif
