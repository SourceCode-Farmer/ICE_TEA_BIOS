/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_SPECIFIC_VARIABLE_SERVICE_DXE_H_
#define _L05_SPECIFIC_VARIABLE_SERVICE_DXE_H_

#include <FlashRegionLayout.h>

#include <Library/BaseLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PcdLib.h>
#include <Library/FlashRegionLib.h>

#include <Protocol/L05Variable.h>
#include <Protocol/SmmFwBlockService.h>

#if (FixedPcdGet32 (PcdFlashFvL05Variable1Size) == 0 || FixedPcdGet32 (PcdFlashFvL05Variable2Size) == 0)
  #error "The size of VARIABLE_SUBREGION_1 and 2 must not be 0."
#endif
#if (FixedPcdGet32 (PcdFlashFvL05Variable1Size) != FixedPcdGet32 (PcdFlashFvL05Variable2Size))
  #error "VARIABLE_SUBREGION_1 and 2 must be the same size. Please check your flash map."
#endif
#if (FixedPcdGet32 (PcdFlashFvL05Variable1Size) % EFI_PAGE_SIZE != 0)
  #error "FLASH_REGION_LENOVO_VARIABLE_REGION_SUBREGION_1_SIZE must be multiple of 4K. Please check your flash map."
#endif

//
//  Function
//
EFI_STATUS
L05GetVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN OUT  UINT32                        *DataSize,
  OUT     VOID                          *Data
  );

EFI_STATUS
L05SetVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      UINT32                        DataSize,
  IN      VOID                          *Data
  );

EFI_STATUS
L05LockVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName
  );

EFI_STATUS
L05UnlockVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      CHAR16                        *Password
  );

EFI_STATUS
L05VariableServiceDxeEntry (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  );

#endif
