/** @file
  Variable Runtime Protection by hooking gRT->SetVariable()

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _VARIABLE_RUNTIME_PROTECTION_DXE_H_
#define _VARIABLE_RUNTIME_PROTECTION_DXE_H_

#include <Uefi.h>
#include <KernelSetupConfig.h>  // SETUP_VARIABLE_NAME, gSystemConfigurationGuid

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseMemoryLib.h>  // CompareGuid
#include <Library/UefiLib.h>

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//

//
// Produced Protocols
//

//
// Guids
//
#include <Guid/ReturnFromImage.h>  // gReturnFromImageGuid
#include <Guid/EventGroup.h>  // gEfiEventVirtualAddressChangeGuid
#include <Guid/L05WirelessDeviceSupportVariable.h>  // gL05WirelessDeviceSupportVariableGuid
#include <Guid\DebugMask.h>  // gEfiGenericVariableGuid

//
// Driver Version
//

//
// Protocol instances
//

//
// Include files with function prototypes
//

//
// Structure of Variable Protection Information
//
typedef struct {
  EFI_GUID                              VariableGuid;
  CHAR16                                *VariableName;
} EFI_L05_PROTECTION_VARIABLE_INFO;

#endif
