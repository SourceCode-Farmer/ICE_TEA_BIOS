/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_VARIABLE_SERVICE_SMM_H_
#define _L05_VARIABLE_SERVICE_SMM_H_

#include <Protocol/SmmVariable.h>
#include <Protocol/SmmCpu.h>
#include <Library/BaseLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Protocol/SmmSwDispatch2.h>
#include <OemSwSmi.h>
#include <Protocol/L05Variable.h>

#define EFI_L05_VARIABLE_FUNCTION       (UINT16)(0x5300 + (UINT8)EFI_L05_VARIABLE_CALLBACK)
#define EFI_L05_VARIABLE_EAX_MASK       0xFFFF00FF
#define EFI_L05_VARIABLE_NO_ERROR       0x0000CE00
#define EFI_L05_VARIABLE_ERROR          0x0000B600

//
// According to Lenovo's requirements,
// BIOS needs to reserve 8*4K memory buffer for LVAR tool and memory type should be EfiReservedMemoryType
//
#define LVAR_RESERVED_MEMORY_SIZE       0x00008000

//
//  Function
//
EFI_STATUS
L05SmmGetVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN OUT  UINT32                        *DataSize,
  OUT     VOID                          *Data
  );

EFI_STATUS
L05SmmSetVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      UINT32                        DataSize,
  IN      VOID                          *Data
  );

EFI_STATUS
L05SmmLockVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName
  );

EFI_STATUS
L05SmmUnlockVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      CHAR16                        *Password
  );

EFI_STATUS
L05SmmVariableEntry (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  );

EFI_STATUS
EFIAPI
L05SmmVariableCallback (
  IN  EFI_HANDLE                        DispatchHandle,
  IN  CONST  VOID                       *DispatchContext,
  IN  OUT  VOID                         *CommBuffer,
  IN  OUT  UINTN                        *CommBufferSize
  );

EFI_STATUS
SmmGetCpuSaveState (
  );

EFI_STATUS
ReadDwordRegister (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             Width,
  IN  UINT8                             CpuNum,
  OUT VOID                              *RegisterData
  );

EFI_STATUS
WriteDwordRegister (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             Width,
  IN  UINT8                             CpuNum,
  OUT VOID                              *RegisterData
  );
#endif
