/** @file
  Library for Feature Report Print function

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifdef OK_FEATURE_REPORT_ENABLE
#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>

//
// Consumed Protocols
//
#include <Protocol/OkFeatureReportInterface.h>

extern EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL  *mOkFeatureReportInterface;

/**
  Feature Report Print function

  @param  Format                        The point to the Format string
  @param  ...                           Variable argument list whose contents are accessed based on the format string specified by Format.
**/
VOID
EFIAPI
FeatureReportPrint (
  IN CONST CHAR8                        *Format,
  ...
  )
{
  EFI_STATUS                            Status;
  VA_LIST                               Marker;

  if (Format == NULL) {
    return;
  }

  if (mOkFeatureReportInterface == NULL) {

    Status = gBS->LocateProtocol (
                    &gEfiOkFeatureReportInterfaceProtocolGuid,
                    NULL,
                    &mOkFeatureReportInterface
                    );

    if (EFI_ERROR (Status)) {
      return;
    }
  }

  VA_START (Marker, Format);
  Status = mOkFeatureReportInterface->RecordMessage (Format, Marker);
  VA_END (Marker);

  return;
}
#endif
