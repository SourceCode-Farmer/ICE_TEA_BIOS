/** @file
  Library for Feature Report Print function

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifdef OK_FEATURE_REPORT_ENABLE
#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>

//
// Consumed Protocols
//
#include <Protocol/OkFeatureReportInterface.h>

extern EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL  *mOkFeatureReportInterface;

/**
  Feature Report Print HEX function

  @param  Location                      The point to the Format string
  @param  Length                        Variable argument list whose contents are accessed based on the format string specified by Format.
**/
VOID
EFIAPI
FeatureReportPrintHex (
  IN VOID                               *Location,
  IN UINTN                              Length
  )
{
  EFI_STATUS                            Status;

  if (mOkFeatureReportInterface == NULL) {

    Status = gBS->LocateProtocol (
                    &gEfiOkFeatureReportInterfaceProtocolGuid,
                    NULL,
                    &mOkFeatureReportInterface
                    );

    if (EFI_ERROR (Status)) {
      return;
    }
  }

  Status = mOkFeatureReportInterface->RecordHexData (Location, Length);

  return;
}
#endif
