/** @file
  Instance of Print String to Image Services Library.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <PiDxe.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/PcdLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PrintLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeServicesLib.h>
#include <Protocol/HiiFont.h>
#include <Protocol/GraphicsOutput.h>

/**
 Print string at specific position.

 @param  Y                              Position X value.
 @param  Y                              Position Y value.
 @param  String                         Pointer to display string.
 @param  FontSize                       Font Size.
 @param  ForegroundColorValue           Value of foreground color.
 @param  BackgroundColorValue           Value of background color.
 @param  FontStyle                      Font style.

 @retval EFI_SUCCESS                    Print string successfully.
 @retval EFI_INVALID_PARAMETER          Input paramter in NULL.
 @retval Other                          Locate graphics output protocol failed or display function return failed.
**/
EFI_STATUS
EFIAPI
L05PrintStrToImage (
  IN UINTN                              X,
  IN UINTN                              Y,
  IN CHAR16                             *String,
  IN UINTN                              FontSize,
  IN UINTN                              ForegroundColorValue,
  IN UINTN                              BackgroundColorValue,
  EFI_HII_FONT_STYLE                    FontStyle
  )
{
  EFI_STATUS                            Status;
  EFI_HII_FONT_PROTOCOL                 *HiiFont;
  EFI_GRAPHICS_OUTPUT_PROTOCOL          *GraphicsOutput;
  UINT32                                HorizontalResolution;
  UINT32                                VerticalResolution;
  EFI_FONT_DISPLAY_INFO                 *FontDisplayInfo;
  CHAR16                                FontName[] = L"ttf";
  UINTN                                 FontNameStrLen;
  UINTN                                 ColorValue;
  EFI_IMAGE_OUTPUT                      *Blt;
  EFI_HII_ROW_INFO                      *RowInfoArray;
  UINTN                                 RowInfoArraySize;

  FontDisplayInfo = NULL;

  if (String == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = gBS->LocateProtocol (&gEfiHiiFontProtocolGuid, NULL, (VOID **) &HiiFont);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = gBS->HandleProtocol (gST->ConsoleOutHandle, &gEfiGraphicsOutputProtocolGuid, (VOID **) &GraphicsOutput);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  HorizontalResolution = GraphicsOutput->Mode->Info->HorizontalResolution;
  VerticalResolution   = GraphicsOutput->Mode->Info->VerticalResolution;

  FontNameStrLen = StrLen (FontName);
  FontDisplayInfo = AllocateZeroPool (sizeof (EFI_FONT_DISPLAY_INFO) + (FontNameStrLen * sizeof (CHAR16)));

  if (FontDisplayInfo == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  ColorValue = ForegroundColorValue;
  FontDisplayInfo->ForegroundColor.Blue     = (UINT8) (ColorValue         & 0xFF);
  FontDisplayInfo->ForegroundColor.Green    = (UINT8) ((ColorValue >>  8) & 0xFF);
  FontDisplayInfo->ForegroundColor.Red      = (UINT8) ((ColorValue >> 16) & 0xFF);
  FontDisplayInfo->ForegroundColor.Reserved = (UINT8) ((ColorValue >> 24) & 0xFF);

  ColorValue = BackgroundColorValue;
  FontDisplayInfo->BackgroundColor.Blue     = (UINT8) (ColorValue         & 0xFF);
  FontDisplayInfo->BackgroundColor.Green    = (UINT8) ((ColorValue >>  8) & 0xFF);
  FontDisplayInfo->BackgroundColor.Red      = (UINT8) ((ColorValue >> 16) & 0xFF);
  FontDisplayInfo->BackgroundColor.Reserved = (UINT8) ((ColorValue >> 24) & 0xFF);

  FontDisplayInfo->FontInfoMask = 0;
  FontDisplayInfo->FontInfo.FontSize  = (UINT16) FontSize;
  FontDisplayInfo->FontInfo.FontStyle = FontStyle;

  StrCpyS (FontDisplayInfo->FontInfo.FontName, (FontNameStrLen + 1), FontName);

  Blt = (EFI_IMAGE_OUTPUT *) AllocateZeroPool (sizeof (EFI_IMAGE_OUTPUT));

  if (Blt == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  Blt->Width        = (UINT16) (HorizontalResolution);
  Blt->Height       = (UINT16) (VerticalResolution);
  Blt->Image.Screen = GraphicsOutput;
  RowInfoArray      = NULL;
  RowInfoArraySize  = 0;
  Status = HiiFont->StringToImage (
                      HiiFont,
                      EFI_HII_IGNORE_IF_NO_GLYPH | EFI_HII_OUT_FLAG_CLIP |
                      EFI_HII_OUT_FLAG_CLIP_CLEAN_X | EFI_HII_OUT_FLAG_CLIP_CLEAN_Y | EFI_HII_DIRECT_TO_SCREEN,
                      String,
                      FontDisplayInfo,
                      &Blt,
                      X,
                      Y,
                      &RowInfoArray,
                      &RowInfoArraySize,
                      NULL
                      );

  if (FontDisplayInfo != NULL) {
    FreePool (FontDisplayInfo);
  }

  FreePool (Blt);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (RowInfoArray != NULL) {
    FreePool (RowInfoArray);
  }

  return EFI_SUCCESS;
}

