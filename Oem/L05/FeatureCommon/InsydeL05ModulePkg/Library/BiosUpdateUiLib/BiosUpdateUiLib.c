/** @file
  Instance of BIOS Update UI Services Library.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <PiDxe.h>
#include <L05Config.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/PcdLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PrintLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeServicesLib.h>
#include <Library/PrintStrToImageLib.h>
#include <Library/ProgressBarLib.h>
#include <Library/BiosUpdateUiLib.h>
#include <Protocol/PngDecoder.h>

/**
  Dummy function for SetMode() of EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL

  @param  This                          The protocol instance pointer.
  @param  ModeNumber                    The mode number to set.

  @retval EFI_SUCCESS                   The requested text mode was set.
**/
STATIC
EFI_STATUS
EFIAPI
DummySetMode (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL    *This,
  IN UINTN                              ModeNumber
  )
{
  return EFI_SUCCESS;
}

/**
  Dummy function for SetMode() of EFI_GRAPHICS_OUTPUT_PROTOCOL

  @param  This                          The protocol instance pointer.
  @param  ModeNumber                    The mode number to set.

  @retval EFI_SUCCESS                   The requested text mode was set.
**/
STATIC
EFI_STATUS
EFIAPI
DummyGraphicsSetMode (
  IN EFI_GRAPHICS_OUTPUT_PROTOCOL       *This,
  IN UINT32                             ModeNumber
  )
{
  return EFI_SUCCESS;
}

/**
  Dummy function for ClearScreen() of EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL

  @param  This                          The protocol instance pointer.

  @retval  EFI_SUCCESS                  The operation completed successfully.
**/
STATIC
EFI_STATUS
EFIAPI
DummyClearScreen (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL    *This
  )
{
  return EFI_SUCCESS;
}

/**
  Dummy function for OutputString() of EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL

  @param  This                          The protocol instance pointer.
  @param  String                        The NULL-terminated string to be displayed

  @retval  EFI_SUCCESS                  The operation completed successfully.
**/
STATIC
EFI_STATUS
EFIAPI
DummyOutputString (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL    *This,
  IN CHAR16                             *String
  )
{
  return EFI_SUCCESS;
}

/**
  Disable the display of firmware update utility, the display will be controlled
  by BIOS Update UI image.

  @retval  EFI_SUCCESS                  The operation completed successfully.

**/
EFI_STATUS
EFIAPI
L05DisableVendorUserInterface (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_GRAPHICS_OUTPUT_PROTOCOL          *GraphicsOutput;

  gST->ConOut->SetMode      = DummySetMode;
  gST->ConOut->ClearScreen  = DummyClearScreen;
  gST->ConOut->OutputString = DummyOutputString;

  Status = gBS->HandleProtocol (
                  gST->ConsoleOutHandle,
                  &gEfiGraphicsOutputProtocolGuid,
                  (VOID **) &GraphicsOutput
                  );

  if (Status == EFI_SUCCESS) {
    GraphicsOutput->SetMode = DummyGraphicsSetMode;
  }

  return EFI_SUCCESS;
}

/**
  Show warning image.

  @param  GraphicsOutput                Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
ShowWarningImage (
  EFI_GRAPHICS_OUTPUT_PROTOCOL          *GraphicsOutput
  )
{
  EFI_STATUS                            Status;
  UINT32                                HorizontalResolution;
  UINT32                                VerticalResolution;
  EFI_PNG_DECODER_PROTOCOL              *PngDecoder;
  EFI_GUID                              WarningImageGuid = {0x8fb9ae7f, 0xdad8, 0x42e9, {0xb3, 0xde, 0x16, 0xe, 0x48, 0x33, 0xee, 0xbe}};
  UINT8                                 *PngImage;
  UINTN                                 PngImageSize;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL         *Blt;
  UINTN                                 BltSize;
  UINTN                                 Height;
  UINTN                                 Width;
  UINT32                                ImagePercent;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL         *BltScaling;
  UINTN                                 BltScalingSize;
  UINTN                                 BltWidth;
  UINTN                                 BltHeight;

  if (GraphicsOutput == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  PngDecoder   = NULL;
  PngImage     = NULL;
  PngImageSize = 0;
  Blt          = NULL;
  BltScaling   = NULL;

  HorizontalResolution = GraphicsOutput->Mode->Info->HorizontalResolution;
  VerticalResolution   = GraphicsOutput->Mode->Info->VerticalResolution;

  Status = gBS->LocateProtocol (
                  &gEfiPngDecoderProtocolGuid,
                  NULL,
                  &PngDecoder
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = GetSectionFromAnyFv (
             &WarningImageGuid,
             EFI_SECTION_RAW,
             0,
             (VOID **) &PngImage,
             &PngImageSize
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = PngDecoder->DecodeImage (
                         PngDecoder,
                         PngImage,
                         PngImageSize,
                         (UINT8 **) &Blt,
                         &BltSize,
                         &Height,
                         &Width
                         );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  ImagePercent = HorizontalResolution * PROCESS_PERCENTAGE_VALUE / FULL_HD_HORIZONTAL_VALUE;
  ImagePercent = ImagePercent * PcdGet32 (PcdL05BiosUpdateWarningImageSizePercent) / PROCESS_PERCENTAGE_VALUE;

  BltWidth = Width * ImagePercent / PROCESS_PERCENTAGE_VALUE;
  BltHeight = Height * ImagePercent / PROCESS_PERCENTAGE_VALUE;

  //
  // Allocate blt scaling Buffer.
  //
  BltScalingSize = BltHeight * BltWidth * sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL);
  BltScaling = AllocateZeroPool (BltScalingSize);

  if (BltScaling == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Scaling image
  //
  Status = L05ImageScaling (
             BltScaling,
             BltScalingSize,
             Blt,
             BltSize,
             BltWidth,
             BltHeight,
             Width,
             Height,
             FALSE
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = GraphicsOutput->Blt (
                             GraphicsOutput,
                             BltScaling,
                             EfiBltBufferToVideo,
                             0,
                             0,
                             (HorizontalResolution * PcdGet32 (PcdL05BiosUpdateWarningImageHorizontalPercent) / 100),
                             (VerticalResolution * PcdGet32 (PcdL05BiosUpdateWarningImageVerticalPercent) / 100),
                             BltWidth,
                             BltHeight,
                             BltWidth * sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL)
                             );

  if (PngImage != NULL) {
    FreePool (PngImage);
  }

  if (Blt != NULL) {
    FreePool (Blt);
  }

  if (BltScaling != NULL) {
    FreePool (BltScaling);
  }

  return Status;
}

/**
  Paint BIOS update warning message.

  @param  DisableVendorUi               A flag indicates whether to mask vendor user interface.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
L05BiosUpdateWarningMsg (
  IN  BOOLEAN                           DisableVendorUi
  )
{
  EFI_STATUS                            Status;
  EFI_GRAPHICS_OUTPUT_PROTOCOL          *GraphicsOutput;
  UINT32                                HorizontalResolution;
  UINT32                                VerticalResolution;
  UINTN                                 StringX;
  UINTN                                 StringY;
  UINTN                                 FontSize;

  GraphicsOutput  = NULL;

  Status = gBS->HandleProtocol (gST->ConsoleOutHandle, &gEfiGraphicsOutputProtocolGuid, (VOID **) &GraphicsOutput);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  HorizontalResolution = GraphicsOutput->Mode->Info->HorizontalResolution;
  VerticalResolution   = GraphicsOutput->Mode->Info->VerticalResolution;
  FontSize = VerticalResolution * PcdGet32 (PcdL05BiosUpdateUiStringFontSizePercent) / 100;
  StringX = HorizontalResolution * PcdGet32 (PcdL05BiosUpdateUiStringHorizontalPercent) / 100;
  StringY = VerticalResolution * PcdGet32 (PcdL05BiosUpdateUiStringVerticalPercent) / 100;

  ShowWarningImage (GraphicsOutput);

  L05PrintStrToImage (
    StringX,
    StringY,
    PcdGetPtr (PcdL05BiosUpdateUiStringEn1),
    FontSize,
    PcdGet32 (PcdL05BiosUpdateUiStringForegroundColorValue),
    PcdGet32 (PcdL05BiosUpdateUiStringBackgroundColorValue),
    EFI_HII_FONT_STYLE_BOLD
    );

  StringY += FontSize;

  L05PrintStrToImage (
    StringX,
    StringY,
    PcdGetPtr (PcdL05BiosUpdateUiStringEn2),
    FontSize,
    PcdGet32 (PcdL05BiosUpdateUiStringForegroundColorValue),
    PcdGet32 (PcdL05BiosUpdateUiStringBackgroundColorValue),
    EFI_HII_FONT_STYLE_BOLD
    );

  StringY += FontSize * 2;

  L05PrintStrToImage (
    StringX,
    StringY,
    PcdGetPtr (PcdL05BiosUpdateUiStringZh),
    FontSize,
    PcdGet32 (PcdL05BiosUpdateUiStringForegroundColorValue),
    PcdGet32 (PcdL05BiosUpdateUiStringBackgroundColorValue),
    EFI_HII_FONT_STYLE_BOLD
    );

  if (DisableVendorUi) {
    L05DisableVendorUserInterface ();
  }

  return Status;
}

/**
  Process H2OFFT error message.
  Paint BIOS update error message.

  @param[in]  H2OFftStatus              Status of H2OFFT.
  @param[in]  ErrorString               A pointer to the error string of H2OFFT.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
L05ErrorProcess (
  IN EFI_STATUS                         H2OFftStatus,
  IN CHAR16                             *ErrorString
  )
{
  EFI_STATUS                            Status;
  EFI_GRAPHICS_OUTPUT_PROTOCOL          *GraphicsOutput;
  UINT32                                HorizontalResolution;
  UINT32                                VerticalResolution;
  UINTN                                 StringX;
  UINTN                                 StringY;
  UINTN                                 FontSize;
  UINTN                                 ErrorCode;
  UINTN                                 ErrorCodeLength;
  CHAR16                                *ErrorCodeString;
  UINTN                                 StringSize;

  GraphicsOutput  = NULL;
  ErrorCodeString = NULL;
  StringSize      = 0;

  if (!EFI_ERROR (H2OFftStatus)) {
    return H2OFftStatus;
  }

  ErrorCode = H2OFftStatus & (~MAX_BIT);
  L05_GET_HEX_NUMBER_LENGTH (ErrorCode, ErrorCodeLength);

  Status = gBS->HandleProtocol (gST->ConsoleOutHandle, &gEfiGraphicsOutputProtocolGuid, (VOID **) &GraphicsOutput);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  HorizontalResolution = GraphicsOutput->Mode->Info->HorizontalResolution;
  VerticalResolution   = GraphicsOutput->Mode->Info->VerticalResolution;
  FontSize = VerticalResolution * PcdGet32 (PcdL05BiosUpdateUiErrorStringFontSizePercent) / 100;
  StringX = HorizontalResolution * PcdGet32 (PcdL05BiosUpdateUiErrorStringHorizontalPercent) / 100;
  StringY = VerticalResolution * PcdGet32 (PcdL05BiosUpdateUiErrorStringVerticalPercent) / 100;

  StringSize = StrSize (PcdGetPtr (PcdL05BiosUpdateUiErrorCodeString)) + (ErrorCodeLength * sizeof (CHAR16));
  ErrorCodeString = AllocateZeroPool (StringSize);
  UnicodeSPrint (ErrorCodeString, StringSize, PcdGetPtr (PcdL05BiosUpdateUiErrorCodeString), ErrorCode);

  L05PrintStrToImage (
    StringX,
    StringY,
    ErrorCodeString,
    FontSize,
    PcdGet32 (PcdL05BiosUpdateUiErrorStringForegroundColorValue),
    PcdGet32 (PcdL05BiosUpdateUiErrorStringBackgroundColorValue),
    EFI_HII_FONT_STYLE_NORMAL
    );

  FreePool (ErrorCodeString);

  StringY += FontSize;

  L05PrintStrToImage (
    StringX,
    StringY,
    ErrorString,
    FontSize,
    PcdGet32 (PcdL05BiosUpdateUiErrorStringForegroundColorValue),
    PcdGet32 (PcdL05BiosUpdateUiErrorStringBackgroundColorValue),
    EFI_HII_FONT_STYLE_NORMAL
    );

  return Status;
}

