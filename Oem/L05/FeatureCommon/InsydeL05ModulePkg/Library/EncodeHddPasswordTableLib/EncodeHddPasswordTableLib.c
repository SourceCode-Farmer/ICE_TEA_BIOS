/** @file
  Instance of Encode HDD Password Table Library.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <PiDxe.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/VariableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseCryptLib.h>
#include <Library/H2OHddPasswordTableLib.h>
#include <Library/FeatureLib/OemSvcSecurityPassword.h>
#include <Library/EncodeHddPasswordTableLib.h>

/**
  Encode HDD Password Table and set to L05_SAVE_ENCODE_HDD_PASSWORD_VARIABLE_NAME variable.

  @param  HddPasswordService            A pointer to the HDD password service.
  @param  HddPasswordTable              A pointer to the HDD password table.
  @param  HddPasswordCount              Count of HDD password.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
L05SetEncodeHddPasswordTable (
  IN EFI_HDD_PASSWORD_SERVICE_PROTOCOL  *HddPasswordService,
  IN HDD_PASSWORD_TABLE                 *HddPasswordTable,
  IN OUT  UINT32                        HddPasswordCount
  )
{
  EFI_STATUS                            Status;
  HDD_PASSWORD_HDD_INFO                 *HddInfoArray;
  UINTN                                 NumOfHdd;
  HDD_PASSWORD_TABLE                    *HddPasswordTablePtr;
  HDD_PASSWORD_TABLE                    *EncodeHddPasswordTable;
  HDD_PASSWORD_TABLE                    *EncodeHddPasswordTablePtr;
  UINT32                                HddPasswordTableSize;
  UINT8                                 PasswordToHdd[HDD_PASSWORD_MAX_NUMBER + 1];
  UINTN                                 PasswordToHddLength;
  UINT8                                 *EncodeData;
  UINTN                                 EncodeDataSize;
  UINTN                                 Index;

  HddInfoArray           = NULL;
  NumOfHdd               = 0;
  HddPasswordTablePtr    = NULL;
  EncodeHddPasswordTable = NULL;
  EncodeData             = NULL;

  if (HddPasswordService == NULL || HddPasswordTable == NULL || HddPasswordCount == 0) {
    return EFI_INVALID_PARAMETER;
  }

  Status = HddPasswordService->GetHddInfo (
                                 HddPasswordService,
                                 &HddInfoArray,
                                 &NumOfHdd
                                 );

  if (Status != EFI_SUCCESS || NumOfHdd == 0) {
    return Status;
  }

  HddPasswordTableSize = GetHddPasswordTableSize (HddPasswordCount, HddPasswordTable);
  EncodeHddPasswordTable = AllocateCopyPool (HddPasswordTableSize, HddPasswordTable);
  if (EncodeHddPasswordTable == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  EncodeDataSize = SHA256_DIGEST_SIZE;
  EncodeData = AllocateZeroPool (EncodeDataSize);
  if (EncodeData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  for (Index = 0; Index < NumOfHdd; Index++) {
    //
    // get the table entry with index
    //
    HddPasswordTablePtr = GetTableEntryWithIndex (HddPasswordTable, Index);
    if (HddPasswordTablePtr == NULL) {
      continue;
    }

    EncodeDataSize = SHA256_DIGEST_SIZE;
    ZeroMem (EncodeData, EncodeDataSize);

    ZeroMem (PasswordToHdd, sizeof (PasswordToHdd));
    PasswordToHddLength = 0;

    Status = HddPasswordService->PasswordStringProcess (
                                   HddPasswordService,
                                   HddPasswordTablePtr->PasswordType,
                                   HddPasswordTablePtr->PasswordStr,
                                   StrLen (HddPasswordTablePtr->PasswordStr),
                                   (VOID **)&PasswordToHdd,
                                   &PasswordToHddLength
                                   );

    Status = HddPasswordEncode (
               PasswordToHdd,
               PasswordToHddLength,
               HddInfoArray[Index].HddModelNumber,
               sizeof (HddInfoArray[Index].HddModelNumber),
               HddInfoArray[Index].HddSerialNumber,
               sizeof (HddInfoArray[Index].HddSerialNumber),
               EncodeData,
               EncodeDataSize
               );

    EncodeHddPasswordTablePtr = GetTableEntryWithIndex (EncodeHddPasswordTable, Index);
    CopyMem (EncodeHddPasswordTablePtr->PasswordStr, (VOID *)EncodeData, EncodeDataSize);
  }

  Status = CommonSetVariable (
             L05_SAVE_ENCODE_HDD_PASSWORD_VARIABLE_NAME,
             &gSaveHddPasswordGuid,
             EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
             HddPasswordTableSize,
             EncodeHddPasswordTable
             );

  FreePool (EncodeHddPasswordTable);
  FreePool (EncodeData);

  return Status;
}

/**
  Delete Encode HDD Password Table variable.

  @param  none.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
L05DeleteEncodeHddPasswordTable (
  VOID
  )
{
  EFI_STATUS                            Status;

  Status = CommonSetVariable (
             L05_SAVE_ENCODE_HDD_PASSWORD_VARIABLE_NAME,
             &gSaveHddPasswordGuid,
             EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
             0,
             NULL
             );

  return Status;
}

