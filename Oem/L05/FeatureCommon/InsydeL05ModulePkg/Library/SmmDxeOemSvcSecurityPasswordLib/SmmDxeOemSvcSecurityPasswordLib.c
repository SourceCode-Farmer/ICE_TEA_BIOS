/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcSecurityPasswordLib.h>

/**
  The constructor function

  @param[in]  ImageHandle               The firmware allocated handle for the EFI image.
  @param[in]  SystemTable               A pointer to the EFI System Table.

  @retval     EFI_SUCCESS               The constructor completed successfully.
**/
EFI_STATUS
EFIAPI
OemSvcSecurityPasswordLibConstructor (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_SUCCESS;
}

