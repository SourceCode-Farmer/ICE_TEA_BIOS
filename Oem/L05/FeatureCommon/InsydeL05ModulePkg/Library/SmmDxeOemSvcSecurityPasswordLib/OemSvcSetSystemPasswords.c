/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmDxeOemSvcSecurityPasswordLib.h>

/**
  Provides an opportunity for
  (Lenovo Sepcification: Lenovo China Minimum BIOS Spec 139.pdf - CH.3.4.1 Rules for BIOS CMOS password)


  @param  SystemPasswordType            System Supervisor password or System User password
  @param  DataLength                    Password length
  @param  SystemPasswordBuffer          Password pointer

  @retval EFI_UNSUPPORTED               Returns unsupported for to do default setting
  @retval EFI_MEDIA_CHANGED             change load password buffer data
  @retval EFI_SCUESS                    skip default behavior
**/
EFI_STATUS
OemSvcSetSystemPasswords (
  IN     PASSWORD_TYPE                  SystemPasswordType,
  IN     UINTN                          DataLength,
  IN OUT UINT8                          *SystemPasswordBuffer
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}

