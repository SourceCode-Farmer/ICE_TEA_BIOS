/** @file
  Provides an interface to notify EC to set WDT flag.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
//[-start-211014-BAIN000051-add]//
#if defined(LCFC_SUPPORT)
#include <Library/LfcEcLib.h>
#endif
//[-end-211014-BAIN000051-add]//

/**
  Provides an interface to notify EC to set WDT flag.

  @param  None

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcNotifyEcToSetWdtFlag (
  VOID
  )
{
  //
  // [Lenovo BIOS Self-Healing Design Guidance Specification v1.9]
  //   2.2 Detect
  //     When BIOS flash happened, before erase/write BIOS should notify EC to set flag (Wdt Flag should be keeped
  //     even EC power lost, such as stored in the EC EEPROM) to enable WDT.
  //

  //
  // Todo:
  //   Add project specific code in here.
  //
//[-start-211014-BAIN000051-modify]//
//#if defined(LCFC_SUPPORT)
//#if defined(C970_BSH_SUPPORT)
//[-start-21119-TAMT000032-modify]//
//[-start-211203-QINGLIN0125-modify]//
//[-start-211206-OWENWU0029-modify]//
//[-start-211214-Ching000017-modify]//
#if defined(C970_BSH_SUPPORT) || defined(C770_BSH_SUPPORT) || defined(S77014_BSH_SUPPORT) || defined(S370_BSH_SUPPORT) || defined(S570_BSH_SUPPORT) || defined(S77013_BSH_SUPPORT) || defined(S77014IAH_BSH_SUPPORT)
  LfcEcLibSetWDTFlagForSelfHealing();

  return EFI_UNSUPPORTED;
#endif
//[-end-211214-Ching000017-modify]//
//[-end-211206-OWENWU0029-modify]//
//[-end-211203-QINGLIN0125-modify]//
//[-end-21119-TAMT000032-modify]//
//[-end-211014-BAIN000051-modify]//
}
