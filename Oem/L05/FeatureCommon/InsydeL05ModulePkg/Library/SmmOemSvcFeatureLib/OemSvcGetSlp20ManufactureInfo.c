/** @file

;******************************************************************************
;* Copyright (c) 2013 - 2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Slp20Config.h>
#include <FlashRegionLayout.h>

#include <Library/BaseMemoryLib.h>
#include <Library/FlashRegionLib.h>

EFI_STATUS
OemSvcGetSlp20ManufactureInfo (
  IN OUT EFI_L05_SLP20_MFG_STATE        *EfiL05Slp20MfgState
  )
{
  UINTN                                 L05Slp20RegionBase;
  UINTN                                 L05Slp20RegionSize;
  EFI_L05_SLP20_AREA                    L05Slp20Area;

  L05Slp20RegionBase = (UINTN) FdmGetNAtAddr (&gL05H2OFlashMapRegionSlp20Guid, 1);
  L05Slp20RegionSize = (UINTN) FdmGetNAtSize (&gL05H2OFlashMapRegionSlp20Guid, 1);
  CopyMem (&L05Slp20Area, (VOID *) L05Slp20RegionBase, sizeof (EFI_L05_SLP20_AREA));

  *EfiL05Slp20MfgState = (EFI_L05_SLP20_MFG_STATE) L05Slp20Area.Manufacture;

  return EFI_SUCCESS;
}
