/** @file

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/BaseMemoryLib.h>
#include <Library/FlashRegionLib.h>
#include <L05Slp20Config.h>
#include <FlashRegionLayout.h>

EFI_STATUS
OemSvcGetSlp20RandomNumber (
  IN OUT EFI_L05_SLP20_RANDOM_INFO      *EfiL05Slp20RandomInfo
  )
{
  UINTN                                 L05Slp20RegionBase;
  UINTN                                 L05Slp20RegionSize;
  EFI_L05_SLP20_AREA                    L05Slp20Area;

  L05Slp20RegionBase = (UINTN) FdmGetNAtAddr (&gL05H2OFlashMapRegionSlp20Guid, 1);
  L05Slp20RegionSize = (UINTN) FdmGetNAtSize (&gL05H2OFlashMapRegionSlp20Guid, 1);

  CopyMem (&L05Slp20Area, (VOID *) L05Slp20RegionBase, sizeof (EFI_L05_SLP20_AREA));
  CopyMem (EfiL05Slp20RandomInfo, &L05Slp20Area.RandomInfo, sizeof (EFI_L05_SLP20_RANDOM_INFO));

  return EFI_SUCCESS;
}
