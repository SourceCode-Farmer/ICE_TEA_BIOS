/** @file
  Provides an interface for notify EC to switch Flip to Boot in S4/S5.

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <SetupConfig.h>
#include <Library/SmmServicesTableLib.h>
#include <Protocol/SmmVariable.h>
#include <Guid/L05SwitchFlipToBoot.h>
//[-start-210817-DABING0002-modify]//
#ifdef LCFC_SUPPORT
#include <Library/DebugLib.h>
#include <Library/FeatureLib/OemSvcSwitchFlipToBoot.h>
#include <Library/LfcEcLib.h>
#endif
//[-end-210817-DABING0002-modify]//
//[-start-211208-TAMT000035-add]//
#ifdef S77013_SUPPORT
#include <SetupVariable.h>
#endif
//[-end-211208-TAMT000035-add]//
/**
  Provides an interface for notify EC to switch Flip to Boot in S4/S5.

  @param  None

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSwitchFlipToBootS4S5 (
  VOID
  )
{
  //
  // Todo:
  //   Add project specific code in here.
  //
//[-start-210701-FLINT00010-add]//
//[-start-210721-QINGLIN0001-modify]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//[-start-210817-DABING0002-modify]//
//#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT)
#ifdef LCFC_SUPPORT
  EFI_STATUS                               Status;
  UINTN                                    FbswifSize;
  LENOVO_FLIP_TO_BOOT_SW_INTERFACE         FtbFun;
  EFI_STATUS                               FlipToBootStatus;
  EFI_SMM_VARIABLE_PROTOCOL                *SmmVariable;

  EFI_STATUS                               LfcSetupStatus;
  UINTN                                    LfcSetupSize;
  SYSTEM_CONFIGURATION                     LfcSetup;  

  Status = gSmst->SmmLocateProtocol (&gEfiSmmVariableProtocolGuid, NULL, &SmmVariable);
  ASSERT_EFI_ERROR (Status);
  LfcSetupSize = sizeof(SYSTEM_CONFIGURATION);
  LfcSetupStatus = SmmVariable->SmmGetVariable (
                 L"Setup",
                 &gSystemConfigurationGuid,
                 NULL,
                 &LfcSetupSize,
                 &LfcSetup
                 );

  FbswifSize = sizeof(LENOVO_FLIP_TO_BOOT_SW_INTERFACE);
  FlipToBootStatus = SmmVariable->SmmGetVariable (
                   L"FBSWIF",
                   &gLenovoFlipToBootSwInterfaceVariableGuid,
                   NULL,
                   &FbswifSize,
                   &FtbFun
                   );

  if (LfcSetup.L05FlipToBoot != FtbFun.FlipToBootEn) {
        LfcSetup.L05FlipToBoot = FtbFun.FlipToBootEn;
        Status = SmmVariable->SmmSetVariable (
                  L"Setup",
                  &gSystemConfigurationGuid,
                  EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                  LfcSetupSize,
                  &LfcSetup
                  );
  }
  LfcEcLibSetFlipToBootMode(FtbFun.FlipToBootEn);
//[-end-210817-DABING0002-modify]//  
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
#endif
//[-end-210721-QINGLIN0001-modify]//
//[-end-210701-FLINT00010-add]//
//[-start-211208-TAMT000035-add]//
#ifdef S77013_SUPPORT
  SETUP_DATA                        SetupData;
  EFI_STATUS                        SetupStatus;
  UINTN                             SetupSize;
  UINT8                             Data = 0;
  UINT32                            Temp32 = 0;

  Status = gSmst->SmmLocateProtocol (&gEfiSmmVariableProtocolGuid, NULL, &SmmVariable);

  ASSERT_EFI_ERROR (Status);

  SetupSize = sizeof(SETUP_DATA);

  SetupStatus = SmmVariable->SmmGetVariable (
                 L"Setup",
                 &gSetupVariableGuid,
                 NULL,
                 &SetupSize,
                 &SetupData
                 );
  Status = LfcEcLibEcRamRead (0x23, &Data);
  if(SetupData.iRPMMode == 0){
    if (Data & BIT1) {
      Data &= (UINT8)~BIT1;
      Status = LfcEcLibEcRamWrite (0x23, Data);
     }
   } else {
     if ((Data & BIT1) != BIT1) {;
       Data |= BIT1;
       Status = LfcEcLibEcRamWrite (0x23, Data);
     }
   }

   Temp32 = MmioRead32(0xFD6D0850);//E3_PWR_EN_3V:0xFD6D0850	
   Temp32 = Temp32 & 0xFFFFFFFE;  //Bit0 to 0	
   MmioWrite32(0xFD6D0850, Temp32);
#endif
//[-end-211208-TAMT000035-add]//
  return EFI_UNSUPPORTED;
}
