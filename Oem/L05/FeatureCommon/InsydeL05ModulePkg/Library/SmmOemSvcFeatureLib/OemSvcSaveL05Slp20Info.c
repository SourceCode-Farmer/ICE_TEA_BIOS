/** @file

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/BaseMemoryLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/FlashRegionLib.h>
#include <Protocol/SmmFwBlockService.h>
#include <L05Slp20Config.h>
#include <FlashRegionLayout.h>

EFI_STATUS
OemSvcSaveL05Slp20Info (
  IN EFI_L05_SLP20_AREA                 *L05Slp20Area
  )
{
  UINTN                                 L05Slp20RegionBase;
  UINTN                                 L05Slp20RegionSize;
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwBlockService;
  EFI_STATUS                            Status;
  UINTN                                 DataLength;

  SmmFwBlockService = NULL;
  Status            = EFI_SUCCESS;
  DataLength        = 0;

  L05Slp20RegionBase = (UINTN) FdmGetNAtAddr (&gL05H2OFlashMapRegionSlp20Guid, 1);
  L05Slp20RegionSize = (UINTN) FdmGetNAtSize (&gL05H2OFlashMapRegionSlp20Guid, 1);

  Status = gSmst->SmmLocateProtocol (&gEfiSmmFwBlockServiceProtocolGuid, NULL, &SmmFwBlockService);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  DataLength = MIN (L05Slp20RegionSize, sizeof (EFI_L05_SLP20_AREA));

  Status = SmmFwBlockService->EraseBlocks (SmmFwBlockService, L05Slp20RegionBase, &L05Slp20RegionSize);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  return SmmFwBlockService->Write (SmmFwBlockService, L05Slp20RegionBase, &DataLength, (UINT8 *) L05Slp20Area);
}
