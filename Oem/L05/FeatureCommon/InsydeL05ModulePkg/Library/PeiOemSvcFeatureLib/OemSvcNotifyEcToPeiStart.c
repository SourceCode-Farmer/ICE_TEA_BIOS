/** @file
  Provides an interface to notify EC at which PEI phase.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
//[-start-211231-JOYID00010-add]
//[-start-220117-QINGLIN0146-modify]//
//[-start-220119-OWENWU0036-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
#include <Library/LfcEcLib.h>
#endif
//[-end-220119-OWENWU0036-modify]//
//[-end-220117-QINGLIN0146-modify]//
//[-end-211231-JOYID00010-add]
/**
  Provides a function to notify EC at which PEI phase.

  @param  PhaseFlag                     TRUE : [Stage1] PEI start (before memory training).
                                        FALSE: [Stage2] Memory ready.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcNotifyEcToPeiStart (
  IN  BOOLEAN                           PhaseFlag
  )
{
  //
  // [Lenovo BIOS Self-Healing Design Guidance Specification v1.9]
  //   2.1 Overview
  //     Notify EC that PEI has started
  //
  // [Self-Healing EC WDT timer optimize]
  //   Split into two stages to notify EC - "PEI start (before memory training)" and "Memory ready"
  //   Before memory ready, EC will reset timer 3 times to extend WDT timer if Port 80 keep changing
  //

  //
  // Todo:
  //   Add project specific code in here.
  //
//[-start-211231-JOYID00010-add]
//[-start-220117-QINGLIN0146-modify]//
//[-start-220119-OWENWU0036-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
  if (PhaseFlag == 1) {

    LfcEcLibStopWDT();
    return EFI_MEDIA_CHANGED;

  } else if (PhaseFlag == 0) {

    LfcEcLibStartWDT();
    return EFI_UNSUPPORTED;

  }
#endif
//[-end-220119-OWENWU0036-modify]//
//[-end-220117-QINGLIN0146-modify]//
//[-end-211231-JOYID00010-add]
  return EFI_UNSUPPORTED;
}
