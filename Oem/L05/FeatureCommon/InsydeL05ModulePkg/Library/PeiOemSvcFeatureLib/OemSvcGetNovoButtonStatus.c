/** @file
  Provides an opportunity for Get Novo Button Status

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcGetNovoButtonStatus.h>

/**
 This function offers an interface to Crisis Led state

 @retval            EFI_UNSUPPORTED     Novo Button is not pressed
 @retval            EFI_MEDIA_CHANGED   Novo Button is pressed
*/
EFI_STATUS
OemSvcGetNovoButtonStatus (
  VOID
  )
{
  EFI_STATUS                            Status;

  Status = EFI_UNSUPPORTED;

  return Status;
}
