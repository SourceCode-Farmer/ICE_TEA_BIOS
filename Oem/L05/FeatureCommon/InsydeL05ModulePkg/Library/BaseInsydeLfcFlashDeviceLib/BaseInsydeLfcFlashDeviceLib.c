//******************************************************************************
//* Copyright (c) 2012 - 2014, Insyde Software Corp. All Rights Reserved.
//*
//* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//* transmit, broadcast, present, recite, release, license or otherwise exploit
//* any part of this publication in any form, by any means, without the prior
//* written permission of Insyde Software Corporation.
//*
//******************************************************************************

#include <Uefi.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/FdSupportLib.h>

/*
  Read NumBytes from PAddress (flash device) to Buffer
*/
EFI_STATUS
EFIAPI
LfcFdLibRead (
  IN     UINTN                          PAddress,
  IN OUT UINTN                          *NumBytes,
     OUT UINT8                          *Buffer
  )
{
  return FlashRead (Buffer, (UINT8 *) PAddress, *NumBytes);
}

/*
  Write NumBytes of Butter to PAddress (flash device)
*/
EFI_STATUS
EFIAPI
LfcFdLibWrite (
  IN      UINTN                         PAddress,
  IN OUT  UINTN                         *NumBytes,
  IN      UINT8                         *Buffer
  )
{
  return FlashProgram ((UINT8 *) PAddress, Buffer, NumBytes, (PAddress & ~(0xFFFF)));
}

/*
  Erase LbaLength from PAddress (flash device)
*/
EFI_STATUS
EFIAPI
LfcFdLibBlockErase (
  IN      UINTN                         PAddress,
  IN      UINTN                         LbaLength
  )
{
  return FlashErase (PAddress, LbaLength);
}

