/** @file
  Instance of Flash Protect Region Library.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05ChipsetNameList.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/FlashProtectRegionLib.h>
#include <Library/FeatureLib/OemSvcFeatureGetProtectTable.h>

EFI_L05_FLASH_PROTECT_ENTRY             mDefaultProtectTable[] = {
// SupportProtectStage,              LinearAddress,                                           Size
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashNvStorageMsdmDataBase),           FixedPcdGet32 (PcdFlashNvStorageMsdmDataSize)}}, 
#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvL05Variable1Base),              FixedPcdGet32 (PcdFlashFvL05Variable1Size)}}, 
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvL05Variable2Base),              FixedPcdGet32 (PcdFlashFvL05Variable2Size)}},
#else
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvEepromBase),                    FixedPcdGet32 (PcdFlashFvEepromSize)}},
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvSystemSupervisorPasswordBase),  FixedPcdGet32 (PcdFlashFvSystemSupervisorPasswordSize)}},
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvSystemUserPasswordBase),        FixedPcdGet32 (PcdFlashFvSystemUserPasswordSize)}},
#endif
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdL05FlashFvSlp20Base),                  FixedPcdGet32 (PcdL05FlashFvSlp20Size)}},
#ifdef L05_COMPUTRACE_ENABLE
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvL05ComputraceRegionBase),       FixedPcdGet32 (PcdFlashFvL05ComputraceRegionSize)}},
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvL05ComputraceFvBase),           FixedPcdGet32 (PcdFlashFvL05ComputraceFvSize)}},
#endif
#ifdef L05_CUSTOMIZE_MULTI_LOGO_SUPPORT
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvL05CustomizeMultiLogoBase),     FixedPcdGet32 (PcdFlashFvL05CustomizeMultiLogoSize)}},
#endif
#ifdef L05_BIOS_SELF_HEALING_SUPPORT
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE)
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvL05BackupIbbBase),              FixedPcdGet32 (PcdFlashFvL05BackupIbbSize)}},
#endif
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CEZANNE)
#ifdef CZN_COMBO_SUPPORT
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashCznPspDirLv2BBase),               FixedPcdGet32 (PcdFlashCznPspDirLv2BSize)}},
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashCznBiosDirLv2BBase),              FixedPcdGet32 (PcdFlashCznBiosDirLv2BSize)}},
#endif
#ifdef LCN_COMBO_SUPPORT
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashRnPspDirLv2BBase),                FixedPcdGet32 (PcdFlashRnPspDirLv2BSize)}},
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashRnBiosDirLv2BBase),               FixedPcdGet32 (PcdFlashRnBiosDirLv2BSize)}},
#endif
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvRecoveryPadBase),               FixedPcdGet32 (PcdFlashFvRecoveryPadSize)}},
#endif
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_REMBRANDT)
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashPspDirBBase),                     FixedPcdGet32 (PcdFlashPspDirBSize)}},
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashBiosDirBBase),                    FixedPcdGet32 (PcdFlashBiosDirBSize)}},
  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashPeiFlashBBase),                   FixedPcdGet32 (PcdFlashPeiFlashBSize)}},
#endif
  // According to Lenovo's request, Self-Healing recovery should not change user settings and status
  {(SelfHealing),                   {FixedPcdGet32 (PcdFlashNvStorageVariableBase),           FixedPcdGet32 (PcdFlashNvStorageVariableSize)}},
  {(SelfHealing),                   {FixedPcdGet32 (PcdFlashNvStorageFtwWorkingBase),         FixedPcdGet32 (PcdFlashNvStorageFtwWorkingSize)}},
  {(SelfHealing),                   {FixedPcdGet32 (PcdFlashNvStorageFtwSpareBase),           FixedPcdGet32 (PcdFlashNvStorageFtwSpareSize)}},
#endif
//[-start-211117-Dongxu0031-add]//
#ifdef LCFC_SUPPORT
    {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvLvarSub1Base),                FixedPcdGet32 (PcdFlashFvLvarSub1Size)}},
    {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvLvarSub2Base),                FixedPcdGet32 (PcdFlashFvLvarSub2Size)}},
    {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvLvarDebugBase),               FixedPcdGet32 (PcdFlashFvLvarDebugSize)}},
#ifdef L05_COMPUTRACE_ENABLE    
    {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvL05ComputraceRegionBase),     FixedPcdGet32 (PcdFlashFvL05ComputraceRegionSize)}},
#endif    
#endif
//[-end-211117-Dongxu0031-add]//
};

/**
  Check if the region should be protected when updating BIOS.

  @param  Region                        Point to region data.
  @param  FlashStage                    Flash stage.

  @retval TRUE                          Region needs protection.
  @retval FALSE                         Region does not need protection.
**/
BOOLEAN
NeedToProtect (
  IN  EFI_L05_FLASH_PROTECT_ENTRY       *Region,
  IN  EFI_L05_FLASH_STAGE               FlashStage
  )
{
  return ((Region->SupportProtectStage & FlashStage) == FlashStage) ? TRUE : FALSE;
}

/**
  Get flash protect table.

  @param  ProtectTable                  Point to protect table.
                                        The caller is responsible to free the memory.
  @param  Count                         Table count.
  @param  FlashStage                    Flash stage.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
GetFlashProtectTable (
  OUT EFI_L05_FLASH_PROTECT_REGION      **ProtectTable,
  OUT UINTN                             *Count,
  IN  EFI_L05_FLASH_STAGE               FlashStage
  )
{
  EFI_STATUS                            Status;
  EFI_L05_FLASH_PROTECT_ENTRY           *TempBuff;
  UINTN                                 TempCount;
  EFI_L05_FLASH_PROTECT_ENTRY           *EntryPtr;
  UINTN                                 EntryCount;
  UINTN                                 Index;
  UINTN                                 Index2;

  *ProtectTable = NULL;
  *Count        = 0;
  TempBuff      = NULL;
  TempCount     = 0;
  EntryPtr      = NULL;
  EntryCount    = 0;

  TempBuff = AllocateZeroPool (sizeof (EFI_L05_FLASH_PROTECT_ENTRY) * L05_DEFAULT_PROTECT_TABLE_SIZE);
  if (TempBuff == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  CopyMem (TempBuff, mDefaultProtectTable, sizeof (mDefaultProtectTable));
  TempCount = sizeof (mDefaultProtectTable) / sizeof (EFI_L05_FLASH_PROTECT_ENTRY);

  Status = OemSvcFeatureGetProtectTable (&TempBuff, &TempCount);

  if ((Status != EFI_MEDIA_CHANGED) ||
      ((Status == EFI_MEDIA_CHANGED) && ((TempBuff == NULL) || (TempCount == 0)))) {
    EntryPtr   = mDefaultProtectTable;
    EntryCount = sizeof (mDefaultProtectTable) / sizeof (EFI_L05_FLASH_PROTECT_ENTRY);
  } else {
    EntryPtr   = TempBuff;
    EntryCount = TempCount;
  }

  for (Index = 0; Index < EntryCount; Index++) {
    if (NeedToProtect (&EntryPtr[Index], FlashStage)) {
      (*Count)++;
    }
  }

  if (*Count == 0) {
    FreePool (TempBuff);
    return EFI_NOT_FOUND;
  }

  *ProtectTable = AllocateZeroPool (sizeof (EFI_L05_FLASH_PROTECT_REGION) * *Count);
  if (*ProtectTable == NULL) {
    *Count = 0;
    FreePool (TempBuff);
    return EFI_OUT_OF_RESOURCES;
  }

  for (Index = 0, Index2 = 0; Index < EntryCount; Index++) {
    if (NeedToProtect (&EntryPtr[Index], FlashStage)) {
      (*ProtectTable)[Index2].LinearAddress = EntryPtr[Index].Region.LinearAddress;
      (*ProtectTable)[Index2].Size          = EntryPtr[Index].Region.Size;
      Index2++;
    }
  }

  FreePool (TempBuff);

  return EFI_SUCCESS;
}
