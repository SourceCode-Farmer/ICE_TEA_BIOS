## @file
# Instance of Flash Protect Region Library.
#
#******************************************************************************
#* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = FlashProtectRegionLib
  FILE_GUID                      = A06B6898-C340-4D23-82AB-FA005949BF87
  MODULE_TYPE                    = BASE
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = FlashProtectRegionLib

[Sources]
  FlashProtectRegionLib.c

[Packages]
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
!if $(L05_CHIPSET_VENDOR) == AMD
  AmdPspFeaturePkg/AmdPspFeaturePkg.dec
!endif
  InsydeModulePkg/InsydeModulePkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
#//[-start-211117-Dongxu0031-add]//
!if $(LCFC_SUPPORT_ENABLE) == YES
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  LfcPkg/LfcPkg.dec
!endif
#//[-end-211117-Dongxu0031-add]//

[LibraryClasses]
  MemoryAllocationLib
  BaseMemoryLib
  BaseOemSvcFeatureLibDefault

[Guids]

[Pcd]
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableBase
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableSize
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingBase
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingSize
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareBase
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareSize
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMsdmDataBase
  gInsydeTokenSpaceGuid.PcdFlashNvStorageMsdmDataSize
  gL05ServicesTokenSpaceGuid.PcdL05ChipsetName
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05Variable1Base
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05Variable1Size
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05Variable2Base
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05Variable2Size
  gL05ServicesTokenSpaceGuid.PcdFlashFvEepromBase
  gL05ServicesTokenSpaceGuid.PcdFlashFvEepromSize
  gL05ServicesTokenSpaceGuid.PcdFlashFvSystemSupervisorPasswordBase
  gL05ServicesTokenSpaceGuid.PcdFlashFvSystemSupervisorPasswordSize
  gL05ServicesTokenSpaceGuid.PcdFlashFvSystemUserPasswordBase
  gL05ServicesTokenSpaceGuid.PcdFlashFvSystemUserPasswordSize
  gL05ServicesTokenSpaceGuid.PcdL05FlashFvSlp20Base
  gL05ServicesTokenSpaceGuid.PcdL05FlashFvSlp20Size
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05ComputraceRegionBase
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05ComputraceRegionSize
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05ComputraceFvBase
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05ComputraceFvSize
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05CustomizeMultiLogoBase
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05CustomizeMultiLogoSize
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05BackupIbbBase
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05BackupIbbSize
!if $(CHIPSET_PKG) == Fp6ChipsetPkg and $(AB_RECOVERY) == YES
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashCznPspDirLv2BBase
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashCznPspDirLv2BSize
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashCznBiosDirLv2BBase
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashCznBiosDirLv2BSize
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashRnPspDirLv2BBase
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashRnPspDirLv2BSize
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashRnBiosDirLv2BBase
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashRnBiosDirLv2BSize
  gPlatformPkgTokenSpaceGuid.PcdFlashFvRecoveryPadBase
  gPlatformPkgTokenSpaceGuid.PcdFlashFvRecoveryPadSize
!endif
!if $(CHIPSET_PKG) == Fp7ChipsetPkg
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashPspDirBBase
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashPspDirBSize
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashBiosDirBBase
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashBiosDirBSize
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashPeiFlashBBase
  gAmdPspFeaturePkgTokenSpaceGuid.PcdFlashPeiFlashBSize
!endif
#//[-start-211117-Dongxu0031-add]//
!if $(LCFC_SUPPORT_ENABLE) == YES
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarSub1Base
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarSub1Size
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarSub2Base
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarSub2Size
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarDebugBase
  gEfiLfcPkgTokenSpaceGuid.PcdFlashFvLvarDebugSize
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05ComputraceRegionBase
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05ComputraceRegionSize 
!endif
//[-end-211117-Dongxu0031-add]//
[BuildOptions]
!if $(INSYDE_DEBUGGER) == YES
  MSFT:*_*_*_CC_FLAGS = /Od
!endif