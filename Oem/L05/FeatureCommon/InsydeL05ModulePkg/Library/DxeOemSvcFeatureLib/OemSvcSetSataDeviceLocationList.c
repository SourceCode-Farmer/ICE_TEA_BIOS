/** @file
  Provides an opportunity for ODM to get EC major and minor version.

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcSetSataDeviceLocationList.h>
#include <Protocol/LegacyBios.h>
#include <L05Config.h>

EFI_L05_SATA_DEVICE_LOCATION            mSataDeviceLocation [] = {
//                    Boot Device Type,  Bus, Device, Function, PortNumber (0 base)

  {L05_BOOT_TYPE_BBS_FIRST_SATA_HDD,    0x00,   0xFF,     0xFF,      0xFF},   /*    The location of the first HDD     */  \
  {L05_BOOT_TYPE_BBS_SECOND_SATA_HDD,   0x00,   0xFF,     0xFF,      0xFF},   /*    The location of the second HDD    */  \
  {L05_BOOT_TYPE_END_OF_LIST,           0xFF,   0xFF,     0xFF,      0xFF}    /* End of List. Please keep this entry. */
};

/**
  Provides an opportunity for ODM set SMBIOS Type01 - Offset 19.
  (Lenovo Sepcification: Lenovo China Minimum BIOS Spec 128.doc - CH.3.4.6 System Management BIOS (SMBIOS))


  @param  MTMExist                      Return Boolean for MTM type exist or not.
  @param  Buffer                        Points to LENOVO_MT_XXXXX or LENOVO_BI_IDEAPADXX,
                                        Please only return value XXXXX or XX (ASCII).

  @retval EFI_UNSUPPORTED               Returns unsupported.
  @retval EFI_MEDIA_CHANGED             Filled out Sata Device Location finished.
**/
EFI_STATUS
OemSvcSetSataDeviceLocationList (
  OUT EFI_L05_SATA_DEVICE_LOCATION      **Buffer
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}

