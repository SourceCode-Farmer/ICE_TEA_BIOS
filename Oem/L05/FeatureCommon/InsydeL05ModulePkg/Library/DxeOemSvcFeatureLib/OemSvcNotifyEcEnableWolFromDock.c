/** @file
  Provides an interface to notify EC to enable wake on LAN from dock.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>


/**
  Provides an interface to notify EC to enable wake on LAN from dock.

  @param  EnableDockWol                 TRUE  - WOL from Dock is enable.
                                        FALSE - WOL from Dock is disable.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcNotifyEcEnableWolFromDock (
  BOOLEAN                               EnableDockWol
  )
{

  //
  // Todo:
  //   Add project specific code in here.
  //

  return EFI_UNSUPPORTED;
}
