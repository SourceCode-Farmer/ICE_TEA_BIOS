/** @file
  Provides an interface for changing PS2 Keyboard HID in ACPI DSDT Table.

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

/**
  Provides an interface for changing PS2 Keyboard HID(Hardware ID) in ACPI DSDT Table.

  @param  OldKeyBoardHid                The old HID for Feature to search
  @param  NewKeyBoardHid                The new HID for Feature to override old HID
                                        Hardware ID list:
                                          IDEA0100 - Matrix 12/13/14 inch without Numpad
                                          IDEA0101 - Matrix 12/13/14 inch without Numpad for Brazilian Portuguese
                                          IDEA0102 - Matrix 15/17 inch with Numpad
                                          IDEA0103 - Matrix 15/17 inch with Numpad for Brazilian Portuguese
                                          IDEA0104 - Matrix 10/11 inch without Numpad
                                          IDEA0105 - Matrix 10/11 inch without Numpad for Brazilian Portuguese

  @retval EFI_UNSUPPORTED               Returns unsupported by default for do not override HID.
  @retval EFI_MEDIA_CHANGED             Returns changed for Feature override PS2 Keyboard HID in ACPI DSDT Table
**/
EFI_STATUS
OemSvcUpdatePs2KeyBoardHid (
  IN OUT  UINT64                        *OldKeyBoardHid,
  IN OUT  UINT64                        *NewKeyBoardHid
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//  *OldKeyBoardHid = SIGNATURE_64 ('M', 'S', 'F', 'T', '0', '0', '0', '1');
//  *NewKeyBoardHid = SIGNATURE_64 ('I', 'D', 'E', 'A', '0', '1', '0', '0');
  return EFI_UNSUPPORTED;
}

