/** @file
  For Project to Skip Confirm Dialog when PXE Boot Fail.

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

/**
  Provide an interface for Project to Skip Confirm Dialog when PXE Boot Fail.

  Only for UEFI IPv4, IPv6 PXE(EFI PXE Network) boot fail

  @retval EFI_UNSUPPORTED               Returns unsupported to continue normal procedure to show Confirm Dialog
  @retval EFI_MEDIA_CHANGED             Returns changed to skip Confirm Dialog when PXE Boot Fail
**/
EFI_STATUS
OemSvcSkipPxeBootFailConfirmDialog (
  )
{
  EFI_STATUS                            Status;

  Status = EFI_UNSUPPORTED;

  /*++
    Todo:
      Add project specific code in here.

  --*/

  return Status;
}
