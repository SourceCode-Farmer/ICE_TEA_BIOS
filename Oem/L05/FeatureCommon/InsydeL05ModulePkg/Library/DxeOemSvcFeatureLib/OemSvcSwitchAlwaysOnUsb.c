/** @file
  Provides an interface for switch USB Charge for external device function.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>
//[-start-210701-FLINT00010-add]//
//[-start-210721-QINGLIN0001-modify]//
//#if defined(C970_SUPPORT) || defined(C770_SUPPORT)
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#endif
//[-end-210721-QINGLIN0001-modify]//
//[-end-210701-FLINT00010-add]//
//[-start-210908-QINGLIN0053-add]//
#ifdef S370_SUPPORT
#include <Library/OemSvcLfcPeiGetBoardID.h>
#endif
//[-end-210908-QINGLIN0053-add]//

/**
  Provides an interface for switch USB Charge for external device function.

  @param  TriggerPoint                  Trigger point for project reference.
  @param  L05AlwaysOnUsb                The switch setting of USB Charge function.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSwitchAlwaysOnUsb (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05AlwaysOnUsb
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
//[-start-210701-FLINT00010-modify]//
#ifdef LCFC_SUPPORT
//[-start-211013-QINGLIN0095-modify]//
#if defined(S370_SUPPORT)
  return EFI_UNSUPPORTED;
#else
  EFI_STATUS Status;

  Status = LfcEcLibSetAlwaysOnUsbMode(L05AlwaysOnUsb);
  return Status;
#endif
//[-end-211013-QINGLIN0095-modify]//
#else
  return EFI_UNSUPPORTED;
#endif
//[-end-210701-FLINT00010-modify]//
}

