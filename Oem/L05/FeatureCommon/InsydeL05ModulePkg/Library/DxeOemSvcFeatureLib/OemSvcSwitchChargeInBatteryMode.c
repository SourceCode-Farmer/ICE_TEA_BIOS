/** @file
  Provides an interface for switch ChargeInBatteryMode.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>
//[-start-210701-FLINT00010-add]//
//[-start-210721-QINGLIN0001-modify]//
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#endif
//[-end-210721-QINGLIN0001-modify]//
//[-end-210701-FLINT00010-add]//
//[-start-210908-QINGLIN0053-add]//
#ifdef S370_SUPPORT
#include <Library/OemSvcLfcPeiGetBoardID.h>
#endif
//[-end-210908-QINGLIN0053-add]//

/**
  Provides an interface for switch ChargeInBatteryMode.

  @param  TriggerPoint                  Trigger point for project reference.
  @param  L05ChargeInBatteryMode        The switch setting of Charge In Battery Mode.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSwitchChargeInBatteryMode (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05ChargeInBatteryMode
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//[-start-210701-FLINT00010-modify]//
#ifdef LCFC_SUPPORT
//[-start-211013-QINGLIN0095-modify]//
#if defined(S370_SUPPORT)
  return EFI_UNSUPPORTED;
#else
  EFI_STATUS Status; 

  Status = LfcEcLibSetChargeInBatteryMode (L05ChargeInBatteryMode);
  return Status;
#endif
//[-end-211013-QINGLIN0095-modify]//
#else
  return EFI_UNSUPPORTED;
#endif
//[-start-210701-FLINT00010-modify]//
}

