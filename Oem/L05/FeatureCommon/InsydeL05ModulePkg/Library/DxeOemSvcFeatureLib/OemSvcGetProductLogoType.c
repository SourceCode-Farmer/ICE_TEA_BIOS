/** @file
  Provides an opportunity for OEM to change Branding logo and Lenovo logo
  in setup interface.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcGetProductLogoType.h>
//[-start-210906-QINGLIN0043-add]//
//[-start-220410-Ching000042-modify]//
//[-start-220412-Ching000044-modify]//
#if defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
#include <Library/OemSvcLfcEarlyDxe.h>
#endif
//[-end-220412-Ching000044-modify]//
//[-end-220410-Ching000042-modify]//
//[-end-210906-QINGLIN0043-add]//

/**
  Provides an opportunity for OEM to change Branding logo and Lenovo logo in setup interface.

  @param  BrandingLogoType              The type of the branding logo.
  @param  IsConsumer                    Whether the product type is consumer or others.
                                        Effect on Lenovo logo; TRUE = colorful style, FLASE = black style.

  @retval EFI_UNSUPPORTED               To do default rule.
  @retval EFI_MEDIA_CHANGED             Change the branding logo and Lenovo logo.
**/
EFI_STATUS
OemSvcGetProductLogoType (
  OUT L05_BRANDING_LOGO_TYPE            *BrandingLogoType,
  OUT BOOLEAN                           *IsConsumer
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  //
  // e.g.
  // *BrandingLogoType = Yoga;  The main logo will be Yoga.
  // *IsConsumer       = TRUE;  Lenovo logo will be a colorful style.
  //
//[-start-210906-QINGLIN0043-modify]//
#if defined(S570_SUPPORT)
  if (OemSvcLfcGetProjectType() == 1) //Xiaoxin
    *BrandingLogoType = XiaoXin;
  else
    *BrandingLogoType = Ideapad;
  *IsConsumer       = TRUE;
  return EFI_MEDIA_CHANGED;
#elif defined(S370_SUPPORT)
  *BrandingLogoType = Ideapad;
  *IsConsumer       = TRUE;
  return EFI_MEDIA_CHANGED;
//[-start-220410-Ching000042-modify]//
//[-start-220412-Ching000044-modify]//
#elif defined(S77013_SUPPORT)
  if (OemSvcLfcGetProjectType() == 1) //LenovoNA
    *BrandingLogoType = LenovoNA;
  else
    *BrandingLogoType = Yoga;
  *IsConsumer       = TRUE;
  return EFI_MEDIA_CHANGED;
//[-end-220410-Ching000042-modify]//
#elif defined(S77014IAH_SUPPORT)
  if (OemSvcLfcGetProjectType() == 1) //XiaoXinIAH
    *BrandingLogoType = XiaoXinIAH;
  else
    *BrandingLogoType = Yoga;
  *IsConsumer       = TRUE;
  return EFI_MEDIA_CHANGED;
//[-end-220412-Ching000044-modify]//
//[-start-220422-Ching000046-modify]//
#elif defined(S77014_SUPPORT)
  if (OemSvcLfcGetProjectType() == 2) //LenovoNA
    *BrandingLogoType = LenovoNA;
  else if (OemSvcLfcGetProjectType() == 1) //XiaoXin
    *BrandingLogoType = XiaoXinIAH;
  else
    *BrandingLogoType = Yoga;
  *IsConsumer 	  = TRUE;
  return EFI_MEDIA_CHANGED;
//[-end-220422-Ching000046-modify]//
#else
  return EFI_UNSUPPORTED;
#endif
//[-end-210906-QINGLIN0043-modify]//
}
