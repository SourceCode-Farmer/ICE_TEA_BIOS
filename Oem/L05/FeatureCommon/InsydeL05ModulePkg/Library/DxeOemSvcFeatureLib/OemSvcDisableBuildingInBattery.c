/** @file
  Provides an interface for Disable Building-in Battery.

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

/**
  Provides an interface for Disable Building-in Battery.

  @param  None

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcDisableBuildingInBattery (
  VOID
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
  return EFI_UNSUPPORTED;
}

