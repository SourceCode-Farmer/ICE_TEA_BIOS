/** @file
  Provides an interface for changing PS2 Touch Pad HID, CID in ACPI DSDT Table.

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

/**
  Provides an interface for changing PS2 Touch Pad HID(Hardware ID), CID(Compatible ID) in ACPI DSDT Table.

  @param  OldTouchPadHid                The old HID for Feature to search
  @param  NewTouchPadHid                The new HID for Feature to override old HID
  @param  NewTouchPadCid                The new CID for Feature to override
  @param  OffsetCid                     The offset of old CID begin at Device (PS2M) in ACPI DSDT table
                                        If OffsetCid = 0, the CID will not be updated even if return EFI_MEDIA_CHANGED

  @retval EFI_UNSUPPORTED               Returns unsupported by default for do not override HID, CID.
  @retval EFI_MEDIA_CHANGED             Returns changed for Feature override PS2 Touch Pad HID, CID in ACPI DSDT Table
**/
EFI_STATUS
OemSvcUpdatePs2TouchPadHidCid (
  IN OUT  UINT64                        *OldTouchPadHid,
  IN OUT  UINT64                        *NewTouchPadHid,
  IN OUT  UINT16                        *NewTouchPadCid,
  IN OUT  UINT8                         *OffsetCid
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//  *OldTouchPadHid = SIGNATURE_64 ('M', 'S', 'F', 'T', '0', '0', '0', '3');
//  *NewTouchPadHid = SIGNATURE_64 ('I', 'D', 'E', 'A', '0', '1', '1', '0');
//  *OffsetCid = 27;
//  *NewTouchPadCid = 0x1565;

  return EFI_UNSUPPORTED;
}

