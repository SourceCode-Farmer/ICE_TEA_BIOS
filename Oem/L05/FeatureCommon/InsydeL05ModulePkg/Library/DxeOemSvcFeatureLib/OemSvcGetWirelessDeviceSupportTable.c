/** @file
  OEM Service to get Wireless Device Support Table for Wireless Device Support feature

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05WirelessDeviceSupportConfig.h>

//
// Please implemnt PCI location which do you want to detect
//
EFI_L05_PCI_LOCATION                    mPciLocationLList [] = {
  //
  // End of List (DO NOT Remove)
  //
  {0xFF,   0xFF,     0xFF}
};

//
// Please implemnt USB location which do you want to detect
//
EFI_L05_USB_LOCATION                    mUsbLocationList [] = {
  //
  // End of List (DO NOT Remove)
  //
  {0xFF, 0xFF, 0xFF}
};

//
// Please implement Supported wireless device to list.
//
EFI_L05_SUPPORTED_WIRELESS_DEVICE_INFO  mSupportedDeviceList[] = {
  //
  // End of List (DO NOT Remove)
  //
  {0xFF, 0xFF, 0xFFFF, 0xFF, 0, NULL}
};

/**
  OEM Service to get Wireless Device Support Table for Wireless Device Support feature

  @param  PciDeivceLocationList         Return Bus, Device, Function list  (if PciDeivceLocationList == NULL, Feature will not check PCI Interface)
  @param  UsbDeivceLocationList         Return Device, Function, Port list (if UsbDeivceLocationList == NULL, Feature will not check USB Interface)
  @param  SupportedWirelessDeviceList   Return Supported wireless device list (if SupportedWirelessDeviceList == NULL, Feature will not execute Wireless Device Support function)

  @retval EFI_MEDIA_CHANGED             Returns media changed, L05 Feature will check valid Supported wireless device list then execute Wireless Device Support function
  @retval EFI_UNSUPPORTED               Returns unsupported by default, L05 Feature will not execute Wireless Device Support function.
  @retval EFI_SUCCESS                   Returns success, L05 Feature will not execute Wireless Device Support function, Project need porting this feature function by itself.
**/
EFI_STATUS
OemSvcGetWirelessDeviceSupportTable (
  OUT EFI_L05_PCI_LOCATION                    **PciDeivceLocationList,
  OUT EFI_L05_USB_LOCATION                    **UsbDeivceLocationList,
  OUT EFI_L05_SUPPORTED_WIRELESS_DEVICE_INFO  **SupportedWirelessDeviceList
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}
