/** @file
  Provides an interface for EC to keep power for wake on LAN during S3, S4, and S5 when only has battery or with AC.
;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

/**
  Provides an interface for EC to keep power for wake on LAN during S3, S4, and S5 when only has battery or with AC.

  @param  AcOnly                        TRUE means wake on LAN only when AC connected;
                                        FALSE means system can wake on LAN with battery connected.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcEcKeepWolPowerForAcOnly (
  IN  UINT8                             AcOnly
  )
{

  //
  // Todo:
  //   Add project specific code in here.
  //

  return EFI_UNSUPPORTED;
}
