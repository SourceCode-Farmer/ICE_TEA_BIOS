/** @file
  Provide an interface for EC to get wake up type.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <IndustryStandard/SmBios.h>

/**
  Provide an interface for EC to get wake up type

  @param  WakeUpType                    Points to UINT8 that specifies to get wake up type
                                        SystemWakeupTypeReserved         = 0x00,
                                        SystemWakeupTypeOther            = 0x01,
                                        SystemWakeupTypeUnknown          = 0x02,
                                        SystemWakeupTypeApmTimer         = 0x03,
                                        SystemWakeupTypeModemRing        = 0x04,
                                        SystemWakeupTypeLanRemote        = 0x05,
                                        SystemWakeupTypePowerSwitch      = 0x06,
                                        SystemWakeupTypePciPme           = 0x07,
                                        SystemWakeupTypeAcPowerRestored  = 0x08

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_MEDIA_CHANGED             Get wake up type success.
**/
EFI_STATUS
OemSvcEcGetWakeUpType (
  OUT UINT8                             *WakeUpType
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}

