/** @file
  Provide an interface for Project to modify ACPI Table (ex: OEM ID, OEM Table ID).

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <IndustryStandard/Acpi.h>

/**
  Provide an interface for Project to modify ACPI Table (ex: OEM ID, OEM Table ID).

  @param  Table                         The point to the ACPI Table.

  @retval EFI_UNSUPPORTED               Returns unsupported for L05 feature override ACPI OEM ID, OEM Table ID, and OEM Revision
  @retval EFI_MEDIA_CHANGED             Returns changed by default for reserve default ACPI Table.
**/
EFI_STATUS
OemSvcAcpiTableUpdate (
  IN OUT  VOID                          *Table
  )
{
  EFI_STATUS                            Status;
  EFI_ACPI_DESCRIPTION_HEADER           *TableHeader;

  Status      = EFI_UNSUPPORTED;
  TableHeader = (EFI_ACPI_DESCRIPTION_HEADER *) Table;

  /*++
    Todo:
      Add project specific code in here.

  --*/

  return Status;
}

