/** @file
  Provide an interface in DXE phase.
  For Project to stop process of Legacy To EFI when unexpected shutdown system before in DXE phase.

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

/**
  Provide an interface in DXE for Project to stop process of Legacy To EFI when unexpected shutdown system before.

  According to feature will do reboot then boot EFI device when all legacy device boot fail.
  And in case system be shutdown by End User unexpected operation after all legacy device boot fail.

  Project neeed check System not Wake from cold boot(ex: power button, WOL, RTC wake..., etc).

  This OEM SVC only be executed when Feature do next reboot when all legacy device boot fail.

  @retval EFI_UNSUPPORTED               Returns unsupported to continue process of Legacy To EFI by default
  @retval EFI_MEDIA_CHANGED             Returns changed to stop process of Legacy To EFI
**/
EFI_STATUS
OemSvcStopLegacyToEfiProcess (
  )
{
  EFI_STATUS                            Status;

  Status = EFI_UNSUPPORTED;

  /*++
    Todo:
      Add project specific code in here.

  --*/

  return Status;
}
