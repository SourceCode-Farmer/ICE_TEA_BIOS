/** @file
  Provides an Override Default Setup Setting Function

;******************************************************************************
;* Copyright (c) 2013 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <SetupConfig.h>
#include <L05ChipsetNameList.h>
//[-start-210722-QINGLIN0002-add]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210903-LIUJIE0005-modify]//
//[-start-210902-GEORGE0003-modify]//
#if defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(C770_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
#include <Library/OemSvcLfcPeiGetBoardID.h>
#endif
//[-start-211118-YUNLEI0155-add]//
#if defined(C770_SUPPORT)
#include <Library/LfcEcLib.h>
#endif
//[-end-211118-YUNLEI0155-add]//

//[-end-210902-GEORGE0003-modify]//
//[-end-210903-LIUJIE0005-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210722-QINGLIN0002-add]//

/**
  Provides an Override Default Setup Setting Function

  It will be called in two cases as below,
    1. Variable L"Setup" not exist during POST
    2. End user do "Load Default" in Setup Menu

  @param  SetupBuffer                   Points to System Configuration Structure
  @param  PchSetupBuffer                Points to PCH_SETUP Structure
  @param  CpuSetupBuffer                Points to CPU_SETUP Structure
  @param  SaSetupBuffer                 Points to SA_SETUP Structure

  @retval EFI_UNSUPPORTED               It will not override L05 Feature default setting. (Default)
  @retval EFI_MEDIA_CHANGED             It will not override L05 Feature default setting.
  @retval EFI_SUCCESS                   It will override L05 Feature default setting.
**/
EFI_STATUS
OemSvcOverrideDefaultSetupSetting (
  IN OUT UINT8                          *SetupBuffer,
  IN OUT UINT8                          *PchSetupBuffer,
  IN OUT UINT8                          *CpuSetupBuffer,
  IN OUT UINT8                          *SaSetupBuffer
  )
{
  SYSTEM_CONFIGURATION                  *SetupVariable;
//[-start-210722-QINGLIN0002-add]//
//[-start-210804-QINGLIN0008-modify]//
//[-start-210903-LIUJIE0005-modify]//
//[-start-210902-GEORGE0003-modify]//
#if defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(C770_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    UINT8                               GpuConfig = 0xFF;
#endif
//[-end-210902-GEORGE0003-modify]//
//[-end-210903-LIUJIE0005-modify]//
//[-end-210804-QINGLIN0008-modify]//
//[-end-210722-QINGLIN0002-add]//
//[-start-210803-QINGLIN0008-modify]//
#if defined(S370_SUPPORT)
    UINT8                               Project_Type = 0;
    UINT8                               Panel_Size   = 0;
#endif
//[-end-210803-QINGLIN0008-modify]//

#if (FixedPcdGetBool (PcdL05PchSetupSupported))
  PCH_SETUP                             *PchSetup;
  CPU_SETUP                             *CpuSetup;
  SA_SETUP                              *SaSetup;
#endif

  if (SetupBuffer == NULL) {
    return EFI_UNSUPPORTED;
  }

#if (FixedPcdGetBool (PcdL05PchSetupSupported))
  if ((PchSetupBuffer == NULL) || (CpuSetupBuffer == NULL) || (SaSetupBuffer == NULL)) {
    return EFI_UNSUPPORTED;
  }
#endif

  SetupVariable = (SYSTEM_CONFIGURATION *) SetupBuffer;
#if (FixedPcdGetBool (PcdL05PchSetupSupported))
  PchSetup            = (PCH_SETUP *) PchSetupBuffer;
  CpuSetup            = (CPU_SETUP *) CpuSetupBuffer;
  SaSetup             = (SA_SETUP *)  SaSetupBuffer;
#endif

  //
  // Todo:
  //   Add project specific code in here.
  //
//_Start_L05_SETUP_MENU_
  //
  // Configuration
  //
  SetupVariable->L05OdmWirelessLanImplement           = 1;
//[-start-210617-Dongxu0002-modify]#  
//[-start-210721-QINGLIN0001-modify]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210923-Ching000004-A-modify]//
#if defined(C970_SUPPORT) || defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210923-Ching000004-A-modify]//
  SetupVariable->L05WlanSyncToChipsetWlan             = 1;
#else
  SetupVariable->L05WlanSyncToChipsetWlan             = 0;
#endif
//[-end-210803-QINGLIN0008-modify]//
//[-end-210721-QINGLIN0001-modify]//
//[-end-210617-Dongxu0002-modify]#
//[-start-210909-YUNLEI0132-modify]//
#if defined(C770_SUPPORT) 
//[-start-211118-YUNLEI0155-modify]//
//[-start-211124-YUNLEI0156-modify]//
    UINT8        PTdpCpuIdentifier;
    PchSetup->PchUsbSsPort[3]=0;
    OemSvcLfcGetCpuIdentifier(&PTdpCpuIdentifier);  
    if(PTdpCpuIdentifier==0x2D)//CPU H45 set PL1=45,PL2=95 fail when modify one Custom. 
    {
      CpuSetup->Custom1PowerLimit1Power=45000;
      CpuSetup->Custom2PowerLimit1Power=45000;
      CpuSetup->Custom3PowerLimit1Power=45000;
      CpuSetup->Custom1PowerLimit2Power=95000;
      CpuSetup->Custom2PowerLimit2Power=95000;
      CpuSetup->Custom3PowerLimit2Power=95000;
      CpuSetup->PowerLimit4=215000;//Power team requirement CPU H45 set PL4=215w
    }
    else if(PTdpCpuIdentifier==0x1C)//Power team requirement CPU U28 set PL4=140w
    {
      CpuSetup->PowerLimit4=140000;
    }
    else{
//[-start-220210-YUNLEI0161-modify]//
      CpuSetup->Custom1PowerLimit2Power=55000;//CPU U15 set PL2=55w
      CpuSetup->Custom2PowerLimit2Power=55000;
      CpuSetup->Custom3PowerLimit2Power=55000;
//[-end-220210-YUNLEI0161-modify]//
      CpuSetup->PowerLimit4=123000;//Power team requirement CPU U15 set PL4=123w
    }
//[-end-211124-YUNLEI0156-modify]//
//[-end-211118-YUNLEI0155-modify]//
#endif
//[-end-210909-YUNLEI0132-modify]//
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
//[-start-211118-KEBIN00067-modify]//
//[-start-211125-SHAONN0020-modify]//
//[-start-211126-OWENWU0026-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)    
  SetupVariable->L05VmdControllerSupport			  = 0;

#else
  SetupVariable->L05VmdControllerSupport              = 1;
#endif
#else
  SetupVariable->L05VmdControllerSupport              = 0;
#endif

#if (FixedPcdGetBool (PcdL05SwitchableGraphicsSupported))
  SetupVariable->L05GraphicsDeviceSupport             = 1;
#else
  SetupVariable->L05GraphicsDeviceSupport             = 0;
#endif

#if (FixedPcdGetBool (PcdL05AmdSetupSupported))
  SetupVariable->L05UmaFrameBufferSizeSupport         = 1;
#else
  SetupVariable->L05UmaFrameBufferSizeSupport         = 0;
#endif

  SetupVariable->L05VtSupport                         = 1;

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_KABYLAKE   || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_JASPERLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
  SetupVariable->L05VtdSupport                        = 1;
#else
  SetupVariable->L05VtdSupport                        = 0;
#endif

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_KABYLAKE   || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE    || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_JASPERLAKE || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
  SetupVariable->L05HyperThreadingSupport             = 1;
#else
  SetupVariable->L05HyperThreadingSupport             = 0;
#endif

  SetupVariable->L05HotKeyModeSupport                 = 1;
  SetupVariable->L05FoolProofFnCtrlSupport            = 1;
//[-end-211126-OWENWU0026-modify]//  
 //[-start-211125-SHAONN0020-modify]//
//[-start-211118-KEBIN00067-modify]// 
#ifdef S370_SUPPORT
  SetupVariable->L05AlwaysOnUsbSupport                = 0;
#else
  SetupVariable->L05AlwaysOnUsbSupport                = 1;
#endif
//[-end-211118-KEBIN00067-modify]//
//[-start-211203-SHAONN0022-modify]//
#ifdef S370_SUPPORT
  SetupVariable->L05ChargeInBatteryModeSupport        = 0;
#else
  SetupVariable->L05ChargeInBatteryModeSupport        = 1;
#endif
//[-end-211203-SHAONN0022-modify]//

  SetupVariable->L05DisableBuildingInBatterySupport   = 1;
  SetupVariable->L05SystemPerformanceModeSupport      = 1;
  SetupVariable->L05ThermalModeSupport                = 0;
  SetupVariable->L05RestoreDefaultOverclockingSupport = 0;
  SetupVariable->L05FlipToBootSupport                 = 1;
  SetupVariable->L05OneKeyBatterySupport              = 1;
//[-start-211118-KEBIN00067-modify]//  
#ifdef LCFC_SUPPORT
  SetupVariable->L05WakeOnVoiceSupport                = 0;
//[-start-211217-KEBIN00071-add]//
//[-start-220113-YUNLEI0158-modify]//
//[-start-220117-SHAONN0027-modify]//
#if defined(C970_SUPPORT) || defined(S370_SUPPORT)
  SetupVariable->L05UltraQuietModeSupport             = 1;
 #else
  SetupVariable->L05UltraQuietModeSupport             = 0;
 #endif
//[-end-220117-SHAONN0027-modify]//
//[-end-220113-YUNLEI0158-modify]//
//[-start-211217-KEBIN00071-add]// 
  SetupVariable->L05InstantBootSupport                = 0;
#else
  SetupVariable->L05WakeOnVoiceSupport                = 1;
  SetupVariable->L05UltraQuietModeSupport             = 1;
  SetupVariable->L05InstantBootSupport                = 1;
#endif 
//[-end-211118-KEBIN00067-modify]// 

  SetupVariable->L05EnhanceMemorySupport              = 0;
  SetupVariable->L05ActiveAtomCoresSupport            = 0;

  //
  // Security
  //
  SetupVariable->L05OdmTpmImplement                   = 1;
  SetupVariable->L05DeviceGuardSupport                = 1;
  SetupVariable->L05ShowComputraceHelpStringInScu     = 1;
  //
  // I/O Port Access
  //
  SetupVariable->L05OdmEthernetLanImplement           = 1;
  SetupVariable->L05OdmWirelessWanImplement           = 1;
  SetupVariable->L05OdmBluetoothImplement             = 1;
  SetupVariable->L05OdmUsbPortImplement               = 1;
  SetupVariable->L05OdmMemoryCardSlotImplement        = 1;
  SetupVariable->L05OdmSmartCardSlotImplement         = 1;
  SetupVariable->L05OdmIntegratedCameraImplement      = 1;
  SetupVariable->L05OdmMicrophoneImplement            = 1;
  SetupVariable->L05OdmFingerprintReaderImplement     = 1;
  SetupVariable->L05OdmThunderboltImplement           = 1;
  SetupVariable->L05OdmNfcDeviceImplement             = 1;

  //
  // Boot
  //
  SetupVariable->L05FastBootImplement                 = 1;

  //
  // Configuration switch update
  //
#ifdef L05_GAMING_UI_ENABLE
  SetupVariable->L05SystemPerformanceModeSupport      = 0;
  SetupVariable->L05ThermalModeSupport                = 1;
  SetupVariable->L05RestoreDefaultOverclockingSupport = 1;

#if FixedPcdGetBool(PcdL05ActiveSmallCoreCountSupported)
  SetupVariable->L05ActiveAtomCoresSupport            = 1;
#endif

#if FixedPcdGetBool(PcdL05SaGvSupported)
  SetupVariable->L05EnhanceMemorySupport              = 1;
#endif
#endif
#if defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(C770_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    OemSvcLfcGetBoardID(GPU_ID,&GpuConfig);
    if(GPU_ID_UMA_ONLY == GpuConfig) {
      SetupVariable->L05GraphicsDeviceSupport = 0;
//[-start-210910-QINGLIN0057-add]//
      SaSetup->PrimaryDisplay                 = 0;
//[-end-210910-QINGLIN0057-add]//
    } else {
      SetupVariable->L05GraphicsDeviceSupport = 1;
    }
#endif
//[-end-210902-GEORGE0003-modify]//
//[-end-210903-LIUJIE0005-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210722-QINGLIN0002-add]//

//[-start-210803-QINGLIN0008-add]//
#if defined(S370_SUPPORT)
    OemSvcLfcGetBoardID(PROJECT_ID, &Project_Type);
    OemSvcLfcGetBoardID(PANEL_SIZE, &Panel_Size);

    if (Project_Type == PROJECT_ID_S370) { //S370
      SetupVariable->L05FlipToBoot                     = 1;
    } else if (Project_Type == PROJECT_ID_S1415) { //S1415
      SetupVariable->L05FlipToBoot                     = 0;
    } else if (Project_Type == PROJECT_ID_V141517) { //V141517
      if (Panel_Size == PANEL_SIZE_17) { //17'
        SetupVariable->L05FlipToBoot                   = 1;
      } else { // 14 or 15'
        SetupVariable->L05FlipToBoot                   = 0;
      }
//[-start-210908-QINGLIN0049-add]//
    } else { //S170
      SetupVariable->L05FlipToBoot                     = 1;
    }
//[-end-210908-QINGLIN0049-add]//
#endif
//[-end-210803-QINGLIN0008-add]//

#ifdef L05_SMB_BIOS_ENABLE
  SetupVariable->L05FlipToBootSupport                 = 0;
#endif
//_End_L05_SETUP_MENU_

  return EFI_UNSUPPORTED;
}
