/** @file
  Provides an opportunity for ODM to remove Smbios type.

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcSmbiosOverride.h>

/**
  Provides an opportunity for OEM to remove unused SMBIOS Type.

  @param  RemoveSmbiosTypeList          Points to uint8 that specifies the Remove Smbios List
                                        List Max size is 0xFF

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_MEDIA_CHANGED             Add Remove list success.
**/
EFI_STATUS
OemSvcRemoveSmbiosType (
  UINT8                                 *RemoveSmbiosTypeList
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
  //
  //Example:
  //Remove Smbios Type 5
  //
  //RemoveSmbiosTypeList [5] = DISABLE_SMBIOS_TYPE_REQUIRED;

  return EFI_UNSUPPORTED;
}

