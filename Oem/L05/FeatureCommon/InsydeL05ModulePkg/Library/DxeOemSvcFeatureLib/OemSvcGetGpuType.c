/** @file
  Provides an opportunity for ODM to Get GPU Type.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05SetupConfig.h>

/**
  Provides an opportunity for ODM to Get GPU Type.

  @param  GpuType                       L05_GPU_TYPE_IGD.
                                        L05_GPU_TYPE_DGPU_NVIDIA.
                                        L05_GPU_TYPE_DGPU_AMD.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcGetGpuType (
  IN OUT UINT8                          *GpuType
  )
{

  *GpuType = L05_GPU_TYPE_DGPU_NVIDIA;

  return EFI_UNSUPPORTED;
}
