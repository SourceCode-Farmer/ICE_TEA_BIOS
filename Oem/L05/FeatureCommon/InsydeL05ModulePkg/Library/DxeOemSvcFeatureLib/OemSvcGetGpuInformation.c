/** @file
  Provides an opportunity for OEM to show GPU information in Gaming setup interface.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcGetGpuInformation.h>

/**
  Provides an opportunity for OEM to show GPU information.

  @param  GpuInfo                       Return the buffer of the string, it will be freed after usage.

  @retval EFI_SUCCESS                   Update GPU information.
  @retval Other                         To do default rule.
**/
EFI_STATUS
OemSvcGetGpuInformation (
  OUT CHAR16                            **GpuInfo
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  //
  // e.g.
  // CHAR16 *String = L"GPU information";
  //
  // *GpuInfo = AllocateCopyPool (StrSize(String), String);
  //

  return EFI_NOT_FOUND;
}
