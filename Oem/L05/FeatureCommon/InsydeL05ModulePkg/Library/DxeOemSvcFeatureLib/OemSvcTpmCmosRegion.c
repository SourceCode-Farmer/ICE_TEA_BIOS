/** @file
  Provide an interface for Project to add Oem CMOS Bank0 offset to do Tpm calculation (Oem CMOS Region: 0x48 ~ 0x4F, 0x6C ~ 0x7F).

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

//
// 0x48 ~ 0x4F : OEM Reserve
// 0x6C ~ 0x7F : OEM Reserve
// This is sample code.
// Please fill out it by project requirement
//
UINT8 ProtectBank0[] = { 0x48, 0x49, 0x4A,
                         0x6C, 0x6D
                       };

//
// 0x00 ~ 0x0F : OEM Reserve
// 0x40 ~ 0x57 : OEM Reserve
// 0x60 ~ 0x7F : OEM Reserve
// This is sample code.
// Please fill out it by project requirement.
//
UINT8 ProtectBank1[] = { 0x0,  0x01,
                         0x40, 0x41,
                         0x60, 0x61
                       };

/**
  Provide an interface for Project to add Oem CMOS Bank0 offset to do Tpm calculation (Oem CMOS Region: 0x48 ~ 0x4F, 0x6C ~ 0x7F).
                                          Oem CMOS Bank0 offset to do Tpm calculation (Oem CMOS Region: 0x00 ~ 0x0F, 0x40 ~ 0x5F, 0x60 ~ 0x7F).

  @param  Table                         The point to the UINT8 Aarray (Size: 0xFF, default value = 0x0).

  @retval EFI_UNSUPPORTED               Returns unsupported for L05 feature default behavior.
  @retval EFI_MEDIA_CHANGED             Returns changed to add Oem Cmos regions for tpm calcaulation.
**/
EFI_STATUS
OemSvcTpmCmosRegion (
  IN   UINT8                            CmosBank,
  OUT  UINT8                            **ProtectCmosTable,
  OUT  UINT8                            *ProtectCmosTableSize
  )
{
  EFI_STATUS                            Status;

  Status = EFI_UNSUPPORTED;

  switch (CmosBank) {

  case 0:
    *ProtectCmosTable = ProtectBank0;
    *ProtectCmosTableSize = sizeof (ProtectBank0);
//      Status = EFI_MEDIA_CHANGED;
    break;

  case 1:
    *ProtectCmosTable = ProtectBank1;
    *ProtectCmosTableSize = sizeof (ProtectBank1);
//      Status = EFI_MEDIA_CHANGED;
    break;

  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}
