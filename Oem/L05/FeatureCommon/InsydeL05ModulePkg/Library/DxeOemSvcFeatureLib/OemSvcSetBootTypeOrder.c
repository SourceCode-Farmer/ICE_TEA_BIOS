/** @file
  Provides an opportunity for ODM to set Boot Type Order.

;******************************************************************************
;* Copyright (c) 2012-2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcSetBootTypeOrder.h>
#include <Protocol/LegacyBios.h>
#include <L05Config.h>

//UINT16 mL05DefaultLegacyBootTypeOrder[] = {
//                                            L05_BOOT_TYPE_BBS_FIRST_SATA_HDD,
//                                            L05_BOOT_TYPE_BBS_SECOND_SATA_HDD,
//                                            BBS_HARDDISK,
//                                            BBS_USB,
//                                            L05_BOOT_TYPE_BBS_USB_MEMORY,
//                                            BBS_EMBED_NETWORK,
//                                            BBS_FLOPPY,
//                                            BBS_CDROM,
//                                            BBS_BEV_DEVICE,
//                                            L05_BOOT_TYPE_BBS_USB_FLOPPY,
//                                            L05_BOOT_TYPE_BBS_USB_CDROM,
//                                            L05_BOOT_TYPE_BBS_USB_HDD
//                                          };
//
//UINT16 mL05DefaultEfiBootTypeOrder[] = {
//                                         L05_BOOT_TYPE_EFI_PORTABLE_DEVICE,
//                                         L05_BOOT_TYPE_EFI_FIRST_SATA_HDD,
//                                         L05_BOOT_TYPE_EFI_SECOND_SATA_HDD,
//                                         L05_BOOT_TYPE_EFI_HARDDISK,
//                                         L05_BOOT_TYPE_EFI_FLOPPY,
//                                         L05_BOOT_TYPE_EFI_CDROM,
//                                         L05_BOOT_TYPE_EFI_USB_MEMORY,
//                                         L05_BOOT_TYPE_EFI_NETWORK,
//                                         L05_BOOT_TYPE_EFI_USB_FLOPPY,
//                                         L05_BOOT_TYPE_EFI_USB_CDROM,
//                                         L05_BOOT_TYPE_EFI_USB_HDD
//                                       };


/**
  Provides an opportunity for ODM to set Boot Type Order.

  @param  BootTypeOrder                 Boot Type Order pointer
  @param  BootTypeOrderLength           Boot Type Order Length
  @param  EfiBootTypeOrder              Efi Boot Type Order
  @param  EfiBootTypeOrderLength        Efi Boot Type Order Length

  @retval EFI_UNSUPPORTED               To do default rule
  @retval EFI_MEDIA_CHANGED             Change Boot Type Order and EFI Boot Type Order
**/
EFI_STATUS
OemSvcSetBootTypeOrder (
  OUT UINT16                            **BootTypeOrder,
  OUT UINTN                             *BootTypeOrderLength,
  OUT UINT16                            **EfiBootTypeOrder,
  OUT UINTN                             *EfiBootTypeOrderLength
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//  *BootTypeOrder          = mL05BootTypeOrder;
//  *BootTypeOrderLength    = sizeof (mL05BootTypeOrder) / sizeof (UINT16);
//  *EfiBootTypeOrder       = mL05EfiBootTypeOrder;
//  *EfiBootTypeOrderLength = sizeof (mL05EfiBootTypeOrder) / sizeof (UINT16);

  return EFI_UNSUPPORTED;
}
