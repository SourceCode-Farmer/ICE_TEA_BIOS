/** @file
  Provides an interface for changing EC version string in SCU.

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

/**
  Provides an interface for changing EC version string in SCU.

  @param  override                      The default string is xxECmmWW or xxECmmCL (The buffer size is 0x20).

  @retval EFI_UNSUPPORTED               Returns unsupported by default setting.
  @retval EFI_MEDIA_CHANGED             Returns override Ec version string.
**/
EFI_STATUS
OemSvcUpdateEcVersionStringInScu (
  IN OUT  CHAR16                        *EcVersionString
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
  return EFI_UNSUPPORTED;
}
