/** @file
 This function offers an interface to check Comptrace suppported status"

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcDxeComputrace.h>

/**
 This function offers an interface to check Comptrace suppported status

 @param[in, out]    Support             Pointer to default rule

 @retval            EFI_UNSUPPORTED     Used dafault rule (Support Comptrace Function)
 @retval            EFI_MEDIA_CHANGED   Checking return value *Supported to determine support comptrace or not.
*/
EFI_STATUS
OemSvcCheckComputraceSupportStatus (
  IN OUT BOOLEAN                        *Supported
  )
{
  /*++
    Todo:
    Add project specific code in here.
  --*/

  return EFI_UNSUPPORTED;
}
