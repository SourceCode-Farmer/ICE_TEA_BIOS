/** @file
  Provides an interface for Project to implement beep sound

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>
//[-start-210926-QINGLIN0085-add]//
#ifdef LCFC_SUPPORT
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/Speaker.h>
#include <Library/DebugLib.h>
#endif
//[-end-210926-QINGLIN0085-add]//

/**
  Provides an interface for Project to implement beep sound

  @param  BeepType                      The type of beep sound.(ex:system error, invalid password key, etc)

  @retval EFI_UNSUPPORTED               Returns unsupported.
  @retval EFI_SUCCESS                   Set Project beep sound success.
**/
EFI_STATUS
OemSvcGenerateBeep (
  IN L05_SYSTEM_BEEP_TYPE               BeepType
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//[-start-210926-QINGLIN0085-modify]//
#ifdef LCFC_SUPPORT
  EFI_SPEAKER_IF_PROTOCOL         *SpeakerInterface = NULL;
  EFI_STATUS                      Status = EFI_NOT_FOUND;

  Status = gBS->LocateProtocol(&gEfiSpeakerInterfaceProtocolGuid, NULL, &SpeakerInterface);
  if (!EFI_ERROR (Status)) {
    SpeakerInterface->SetSpeakerToneFrequency (SpeakerInterface, 1000);
    SpeakerInterface->GenerateBeep (SpeakerInterface, 1, 100000, 20000);
  }

  return EFI_SUCCESS;
#else
  return EFI_UNSUPPORTED;
#endif
//[-end-210926-QINGLIN0085-modify]//
}