/** @file
  Provides an interface for switch One Key Battery.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>

/**
  Provides an interface for switch One Key Battery.

  @param  TriggerPoint                  Trigger point for project reference.
  @param  L05OneKeyBattery              The switch setting of One Key Battery.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSwitchOneKeyBattery (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05OneKeyBattery
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
  return EFI_UNSUPPORTED;
}

