/** @file
  Provide an interface for logo resolution table

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>

/**
  Provide an interface for logo resolution table.

  @param  LogoResolutionTable           The point to the logo resolution table.
  @param  LogoResolutionCount           Logo resolution table count.

  @retval EFI_UNSUPPORTED               Feature will use LogoResolutionTable only by default.
  @retval EFI_MEDIA_CHANGED             Feature will refer Oem Svc to get LogoResolutionTable.
**/
EFI_STATUS
OemSvcLogoResolutionTable (
  IN OUT  L05_LOGO_RESOLUTION_TABLE     **LogoResolutionTable,
  IN OUT  UINTN                         *LogoResolutionCount
  )
{

  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}

