/** @file
  Provides an interface for switch System Performance Mode.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>
//[-start-210902-Dongxu0014-add]//
#ifdef LCFC_SUPPORT
#include <Library/LfcEcLib.h>
#endif
//[-end-210902-Dongxu0014-add]//

/**
  Provides an interface for switch System Performance Mode.

  @param  TriggerPoint                  Trigger point for project reference.
  @param  L05SystemPerformanceMode      The switch setting of System Performance Mode.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSwitchSystemPerformanceMode (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05SystemPerformanceMode
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
//[-start-210916-Dongxu0017-modify]//
//[-start-210902-Dongxu0014-add]//
#ifdef LCFC_SUPPORT
    EFI_STATUS  Status = EFI_SUCCESS;

if (TriggerPoint == RouteConfig) {
  if (L05SystemPerformanceMode == 1) {
    Status = LfcEcLibEcRamWrite (0x20, 0x01); // Performance Mode
  } else if (L05SystemPerformanceMode == 2) {
    Status = LfcEcLibEcRamWrite (0x20, 0x02); // Battery Mode
  } else {
    Status = LfcEcLibEcRamWrite (0x20, 0x00); // Intelligent Mode
  }  
}

    return EFI_MEDIA_CHANGED;
#else
    return EFI_UNSUPPORTED;
#endif
//[-start-210902-Dongxu0014-add]//
//[-end-210916-Dongxu0017-modify]//
}

