/** @file
  Provide an interface for Project to decide install SLIC Table or not when no valid Marker in SLIC Table.

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <IndustryStandard/SLP2_0.h>

/**
  Provide an interface for Project to decide install SLIC Table or not when no valid Marker in SLIC Table.

  @param  InstallSlicTable              The pointer to let Feature install SLIC Table or not
  @param  Table                         The point to the ACPI SLIC Table.

  @retval EFI_UNSUPPORTED               Returns unsupported for do not install SLIC Table decide by Default
  @retval EFI_MEDIA_CHANGED             Returns changed for Project to decide install SLIC Table or not.
**/
EFI_STATUS
OemSvcInstallSlicTable (
  IN OUT BOOLEAN                        *InstallSlicTable,
  IN OUT VOID                           *Table
  )
{
  EFI_ACPI_SOFTWARE_LICENSING_TABLE     *SlicTable;

  SlicTable         = (EFI_ACPI_SOFTWARE_LICENSING_TABLE *) Table;
  *InstallSlicTable = FALSE;

  /*++
    Todo:
      Add project specific code in here.
  --*/

  return EFI_UNSUPPORTED;
}

