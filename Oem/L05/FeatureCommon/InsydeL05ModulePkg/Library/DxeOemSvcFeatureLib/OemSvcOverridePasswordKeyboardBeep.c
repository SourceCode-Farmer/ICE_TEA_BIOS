/** @file
  Provides an opportunity to override Keyboard Beep Function for invalid password key

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

/**
  Provides an opportunity to override Keyboard Beep Function for illegal password key

  @param  PlatformBeep                  TRUE  - Active platform invalid key beep function.
                                        FALSE - Inactive platform invalid key beep function.

  @retval EFI_UNSUPPORTED               Returns unsupported.
  @retval EFI_SUCCESS                   Override Keyboard Beep function success.
**/
EFI_STATUS
OemSvcOverridePasswordKeyboardBeep (
  IN BOOLEAN                            PlatformBeep
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
  return EFI_UNSUPPORTED;
}