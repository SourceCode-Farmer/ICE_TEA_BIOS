/** @file
  Provides an Restore Setup Setting Function After Flash.

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <SetupConfig.h>

/**
  Provides an Restore Setup Setting Function After Flash.

  @param SetupBuffer                    pointer to SYSTEM_CONFIGURATION Structure.
  @param PchSetupBuffer                 pointer to PCH_SETUP Structure.
  @param CpuSetupBuffer                 pointer to CPU_SETUP Structure.
  @param SaSetupBuffer                  pointer to SA_SETUP Structure.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcRestoreSetupSettingAfterFlash (
  IN OUT UINT8                          *SetupBuffer,
  IN OUT UINT8                          *PchSetupBuffer,
  IN OUT UINT8                          *CpuSetupBuffer,
  IN OUT UINT8                          *SaSetupBuffer
  )
{
  SYSTEM_CONFIGURATION                  *SetupVariable;
#if (FixedPcdGetBool (PcdL05PchSetupSupported))
  PCH_SETUP                             *PchSetupVariable;
  CPU_SETUP                             *CpuSetupVariable;
  SA_SETUP                              *SaSetupVariable;
#endif

  SetupVariable    = (SYSTEM_CONFIGURATION *) SetupBuffer;
#if (FixedPcdGetBool (PcdL05PchSetupSupported))
  PchSetupVariable = (PCH_SETUP *) PchSetupBuffer;
  CpuSetupVariable = (CPU_SETUP *) CpuSetupBuffer;
  SaSetupVariable  = (SA_SETUP *)  SaSetupBuffer;
#endif

  //
  // Todo:
  //   Add project specific code in here.
  //

  return EFI_UNSUPPORTED;
}
