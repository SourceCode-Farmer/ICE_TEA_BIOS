/** @file
  Provides an Override Setup Setting Function During Post

;******************************************************************************
;* Copyright (c) 2013 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <SetupConfig.h>

/**
  Provides an Override Setup Setting Function During Post

  It will be called during every POST (G3/S5 -> S0, S4 -> S0).

  @param  SetupBuffer                   Points to System Configuration Structure

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcOverrideSetupSettingDuringPost (
  IN OUT UINT8                          *SetupBuffer
  )
{
  SYSTEM_CONFIGURATION                  *SetupVariable;

  SetupVariable = (SYSTEM_CONFIGURATION *) SetupBuffer;

  //
  // Todo:
  //   Add project specific code in here.
  //

  return EFI_UNSUPPORTED;
}
