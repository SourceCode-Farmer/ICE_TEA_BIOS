/** @file
  Provides an opportunity for ODM to Detect NVMe Presence

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>

/**
  Provides an opportunity for ODM to if NVMe/Optane is present or not.

  @param  IsNvmePresence                0: Optane does not exist in Optane/WWAN Slot and NVMe does not exist in Main Storage Slot.
                                        1: Optane does not exist in Optane/WWAN Slot and NVMe exists in Main Storage Slot.
                                        2: Optane exists in Optane/WWAN Slot. It's no matter whether Main Storage Slot have NVMe or not.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcDetectNvmePresence (
  IN OUT UINT8                          *IsNvmePresence
  )
{
  // 
  // [Lenovo BIOS Optane Support Version 1.4 Page 12]
  //   BIOS detects NVMe device by using GPIO and ECFW I/F Space 
  //
  // Todo:
  //   Add project specific code in here.
  //
  // The reference setting of IsNvmePresence is as follows -
  // +=================================================================================+
  // |           Main Storage Slot |            |            |            |            |
  // |                             |  SATA HDD  |  SATA SSD  |    NVMe    |    None    |
  // | Optane/WWAN (Cache) Slot    |            |            |            |            |
  // +=================================================================================+
  // | Optane                      |      2     |      2     |      2     |     2      |
  // +---------------------------------------------------------------------------------+
  // | Empty or WWAN               |      0     |      0     |      1     |     0      |
  // +=================================================================================+
  //

  return EFI_UNSUPPORTED;
}
