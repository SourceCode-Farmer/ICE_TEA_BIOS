/** @file
  Provides an opportunity for OEM to change the image of performance mode and
  the string in Gaming setup interface.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/FeatureLib/OemSvcGetPerformanceMode.h>

/**
  Provides an opportunity for OEM to change the image of performance mode and the string in Gaming setup interface.

  @param  L05ThermalMode                Thermal Mode of SYSTEM_CONFIGURATION.
  @param  Mode                          Return performance mode.

  @retval EFI_UNSUPPORTED               To do default rule.
  @retval EFI_MEDIA_CHANGED             Change performance mode.
**/
EFI_STATUS
OemSvcGetPerformanceMode (
  IN  UINT8                             L05ThermalMode,
  OUT L05_PERFORMANCE_MODE              *Mode
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  //
  // e.g.
  // *Mode = SavingMode;  The image and the string of performance mode will change to Battery Saving style.
  //

  return EFI_UNSUPPORTED;
}
