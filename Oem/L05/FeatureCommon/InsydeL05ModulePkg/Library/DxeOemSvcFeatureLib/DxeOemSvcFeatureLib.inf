## @file
#  Component description file for DxeOemSvcFeatureLib instance.
#
#******************************************************************************
#* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = DxeOemSvcFeatureLib
  FILE_GUID                      = D5A88998-936D-4e42-ABF0-3B0943A9E05A
  MODULE_TYPE                    = UEFI_DRIVER
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = DxeOemSvcFeatureLib|DXE_CORE DXE_DRIVER DXE_RUNTIME_DRIVER DXE_SAL_DRIVER DXE_SMM_DRIVER UEFI_APPLICATION UEFI_DRIVER SMM_CORE

[Sources]
  OemSvcGetEepromData.c
  OemSvcGetEcVersion.c
  OemSvcRemoveSmbiosType.c
  OemSvcGetWirelessDeviceInfoForSmbiosType248.c
  OemSvcGetWirelessDeviceSupportTable.c
  OemSvcAcpiTableUpdate.c
  OemSvcSetSataDeviceLocationList.c
  OemSvcSetBootTypeOrder.c
  OemSvcOverrideDefaultSetupSetting.c
  OemSvcOverrideSetupSettingDuringPost.c
  OemSvcSwitchWirelessLan.c
  OemSvcSwitchHotKeyMode.c
  OemSvcSwitchFoolProofFnCtrl.c
  OemSvcSwitchAlwaysOnUsb.c
  OemSvcSwitchChargeInBatteryMode.c
  OemSvcDisableBuildingInBattery.c
  OemSvcSwitchSystemPerformanceMode.c
  OemSvcSwitchThermalMode.c
  OemSvcSwitchFlipToBoot.c
  OemSvcSwitchOneKeyBattery.c
  OemSvcSwitchRestoreDefaultOverclocking.c
  OemSvcSwitchWakeOnVoice.c
  OemSvcSwitchUltraQuietMode.c
  OemSvcSwitchInstantBoot.c
  OemSvcSwitchEthernetLan.c
  OemSvcSwitchWirelessWan.c
  OemSvcSwitchBluetooth.c
  OemSvcSwitchUsbPort.c
  OemSvcSwitchMemoryCardSlot.c
  OemSvcSwitchSmartCardSlot.c
  OemSvcSwitchIntegratedCamera.c
  OemSvcSwitchMicrophone.c
  OemSvcSwitchFingerprintReader.c
  OemSvcSwitchThunderbolt.c
  OemSvcSwitchNfcDevice.c
  OemSvcGetBatterySerial.c
  OemSvcGetSystemSerial.c
  OemSvcCheckComputraceSupportStatus.c
  OemSvcUpdatePs2KeyBoardHid.c
  OemSvcUpdatePs2TouchPadHidCid.c
  OemSvcGenerateBeep.c
  OemSvcOverridePasswordKeyboardBeep.c
  OemSvcSwitchFeatureVersion.c
  OemSvcInstallSlicTable.c
  OemSvcUpdateEcVersionStringInScu.c
  OemSvcStopLegacyToEfiProcess.c
  OemSvcOverrideSetupSettingInSetupMenu.c
  OemSvcTpmCmosRegion.c
  OemSvcLogoResolutionTable.c
  OemSvcDetectNvmePresence.c
  OemSvcRestoreSetupSettingAfterFlash.c
  OemSvcCustomizePostLogoSupportFormat.c
  OemSvcGetProductLogoType.c
  OemSvcGetPerformanceMode.c
  OemSvcGetGpuInformation.c
  OemSvcOneKeyBattery.c
#[-start-210817-DABING0002-modify]#
!if $(LCFC_SUPPORT_ENABLE) == YES
  OemSvcSwitchOneKeyBattery.c
!endif
#[-end-210817-DABING0002-modify]#
  OemSvcNotifyEcToDisableTopSwap.c
  OemSvcNotifyEcToSetWdtFunction.c
!if $(L05_SMB_BIOS_ENABLE) == YES
  OemSvcEcKeepWolPowerForAcOnly.c
  OemSvcNotifyEcEnableWolFromDock.c
  OemSvcEcGetWakeUpType.c
  OemSvcUpdateSecondMacAddress.c
!endif
#[-start-210903-BAIN000038-add]#
!if $(LCFC_SUPPORT_ENABLE) == YES
  OemSvcAcAdapterString.c
!endif
#[-end-210903-BAIN000038-add]#
  OemSvcGetGpuType.c

[Packages]
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec  ## ChipsetSetupConfig.h
!if $(L05_CHIPSET_VENDOR) == INTEL
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec  ## PchLimits.h
!endif
  InsydeModulePkg/InsydeModulePkg.dec
  MdeModulePkg/MdeModulePkg.dec  ## Guid/MdeModuleHii.h
  MdePkg/MdePkg.dec
#[-start-210902-Dongxu0014-add]#
  LfcPkg/LfcPkg.dec
#[-end-210902-Dongxu0014-add]#

[LibraryClasses]
  MemoryAllocationLib
  BaseMemoryLib
  PcdLib
  SpiAccessLib
  FlashDevicesLib
  CmosLib
#[-start-210902-Dongxu0014-add]#
  LfcEcLib
#[-end-210902-Dongxu0014-add]#

[Pcd]

[FixedPcd]
  gL05ServicesTokenSpaceGuid.PcdL05ChipsetName
  gL05ServicesTokenSpaceGuid.PcdL05PchSetupSupported
  gL05ServicesTokenSpaceGuid.PcdL05SaGvSupported
  gL05ServicesTokenSpaceGuid.PcdL05ActiveSmallCoreCountSupported
  gL05ServicesTokenSpaceGuid.PcdL05AmdSetupSupported
  gL05ServicesTokenSpaceGuid.PcdL05SwitchableGraphicsSupported
