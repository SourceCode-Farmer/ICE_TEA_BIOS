/** @file
  Provides an opportunity for ODM to get One Key Battery status.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/FeatureLib/OemSvcOneKeyBattery.h>

/**
  Provides an opportunity for ODM get battery percentage.

  @param  BatteryPercentage             Battery percentage

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_SUCCESS                   Support Get BatteryPercentage.
**/
EFI_STATUS
OemSvcGetBatteryPercentage (
  IN OUT  UINT8                         *BatteryPercentage
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}

/**
  Provides an opportunity for ODM get AC State.

  @param  AcStatus                      AC status.

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_SUCCESS                   Support Get AC Status.
**/
EFI_STATUS
OemSvcGetAcStatus (
  OUT BOOLEAN                           *AcStatus
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}

/**
  Provides an opportunity for ODM get AOU Status.

  @param  AouStatus                     AOU status.

  @retval EFI_UNSUPPORTED               Returns unsupported by default.
  @retval EFI_SUCCESS                   Support Get AOU status.
**/
EFI_STATUS
OemSvcGetAouStatus (
  OUT BOOLEAN                           *AouStatus
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}

/**
  Provides an opportunity for ODM get bootup state.

  @param  BootupState                       BootUp state.

  @retval EFI_UNSUPPORTED                   Returns unsupported by default.
  @retval EFI_SUCCESS                       Support Get BootUp State.
**/
EFI_STATUS
OemSvcGetBootUpState (
  OUT BOOTUP_STATE_FOR_SHOW_ONEKEYBATTERY   *BootupState
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/

  return EFI_UNSUPPORTED;
}
