/** @file
  Provides an interface for switch Ultra Quiet Mode.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <L05Config.h>

/**
  Provides an interface for switch Ultra Quiet Mode.

  @param  TriggerPoint                  Trigger point for project reference.
  @param  L05UltraQuietMode             The switch setting of Ultra Quiet Mode.

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcSwitchUltraQuietMode (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05UltraQuietMode
  )
{
  /*++
    Todo:
      Add project specific code in here.

  --*/
  return EFI_UNSUPPORTED;
}

