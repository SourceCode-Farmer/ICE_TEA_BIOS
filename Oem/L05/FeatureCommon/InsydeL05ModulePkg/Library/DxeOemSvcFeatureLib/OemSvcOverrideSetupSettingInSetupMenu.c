/** @file
  Provides an Override Setup Setting Function in Setup menu

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <SetupConfig.h>

/**
  Provides an Override Setup Setting Function in Setup menu

  It will be called in two cases as below,
    1. End user do "Exit Saving Changes" in Setup Menu
    2. End user do "Save Changes" in Setup Menu

  @param  SetupBuffer                   Points to System Configuration Structure

  @retval EFI_UNSUPPORTED               Returns unsupported by default. The return status will not be referenced.
**/
EFI_STATUS
OemSvcOverrideSetupSettingInSetupMenu (
  IN OUT UINT8                          *SetupBuffer
  )
{
  SYSTEM_CONFIGURATION                  *SetupVariable;

  SetupVariable = (SYSTEM_CONFIGURATION *) SetupBuffer;

  //
  // Todo:
  //   Add project specific code in here.
  //

  return EFI_UNSUPPORTED;
}
