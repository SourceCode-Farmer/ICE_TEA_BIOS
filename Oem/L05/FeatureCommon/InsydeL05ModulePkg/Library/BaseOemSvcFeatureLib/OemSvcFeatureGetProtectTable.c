/** @file
  Provide an interface for customizing the protected region when flash the BIOS.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/BaseMemoryLib.h>
#include <Library/FlashProtectRegionLib.h>

//EFI_L05_FLASH_PROTECT_ENTRY             mOemProtectTable[] = {
//// SupportProtectStage,              LinearAddress,                                           Size
//  {(Ihisi | Crisis | SelfHealing),  {FixedPcdGet32 (PcdFlashFvReservedBase),                  FixedPcdGet32 (PcdFlashFvReservedSize)}},
//};

/**
  Provide an interface for customizing the protected region when flash the BIOS.

  @param  *ProtectTable                 The table defines the protected regions when flash the BIOS.
  @param  *Count                        The number of protected regions are defined.
  
  @retval EFI_UNSUPPORTED               Feature will use the internal protect table by default.
  @retval EFI_MEDIA_CHANGED             Feature will refer Oem Svc to set protect table.
**/
EFI_STATUS
OemSvcFeatureGetProtectTable (
  IN OUT EFI_L05_FLASH_PROTECT_ENTRY    **ProtectTable,
  IN OUT UINTN                          *Count
  )
{
  //
  // Todo:
  //   Add project specific code in here.
  //
//  UINTN                                 OemProtectEntryCount;
//  UINTN                                 OldProtectEntryCount;
//
//  //=============================================================================
//  // Overlap Sample Code: Overlap input buffer
//  //=============================================================================
//  OemProtectEntryCount = sizeof (mOemProtectTable) / sizeof (EFI_L05_FLASH_PROTECT_ENTRY);
//
//  if (OemProtectEntryCount > L05_DEFAULT_PROTECT_TABLE_SIZE) {
//    return EFI_UNSUPPORTED;
//  }
//
//  CopyMem (*ProtectTable, mOemProtectTable, sizeof (mOemProtectTable));
//  *Count = OemProtectEntryCount;
//  //=============================================================================
//  // Overlap Sample Code End 
//  //=============================================================================
//
//  //=============================================================================
//  // Merge Sample Code: Append OEM's protect table to input buffer
//  //=============================================================================
//  OldProtectEntryCount = *Count;
//  OemProtectEntryCount = sizeof (mOemProtectTable) / sizeof (EFI_L05_FLASH_PROTECT_ENTRY);
//
//  if ((OldProtectEntryCount + OemProtectEntryCount) > L05_DEFAULT_PROTECT_TABLE_SIZE) {
//    return EFI_UNSUPPORTED;
//  }
//
//  CopyMem (((*ProtectTable) + OldProtectEntryCount), mOemProtectTable, sizeof (mOemProtectTable));
//  *Count = OldProtectEntryCount + OemProtectEntryCount;
//  //=============================================================================
//  // Merge Sample Code End
//  //=============================================================================
//
//  return EFI_MEDIA_CHANGED;
  return EFI_UNSUPPORTED;
}
