//******************************************************************************
//* Copyright (c) 2012 - 2014, Insyde Software Corp. All Rights Reserved.
//*
//* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//* transmit, broadcast, present, recite, release, license or otherwise exploit
//* any part of this publication in any form, by any means, without the prior
//* written permission of Insyde Software Corporation.
//*
//******************************************************************************

#include <Uefi.h>
#include <Library/PcdLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Protocol/SmmCpu.h>
#include <Protocol/SmmBase2.h>
#include <Library/IoLib.h>


EFI_SMM_CPU_PROTOCOL                    *mSmmCpu = NULL;

EFI_STATUS
GetDwordRegisterByCpuIndex (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             CpuIndex,
  IN  UINTN                             RegisterWidth,
  OUT UINT32                            *RegisterData
  )
{
  EFI_STATUS                            Status;

  if (mSmmCpu == NULL) {
    Status = gSmst->SmmLocateProtocol (
                      &gEfiSmmCpuProtocolGuid,
                      NULL,
                      &mSmmCpu
                      );
    ASSERT_EFI_ERROR (Status);
  }

  return mSmmCpu->ReadSaveState (
                    mSmmCpu,
                    sizeof (UINT32),
                    RegisterNum,
                    CpuIndex,
                    RegisterData
                    );
}

EFI_STATUS
SetDwordRegisterByCpuIndex (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             CpuIndex,
  IN  UINTN                             RegisterWidth,
  IN  UINT32                            *RegisterData
  )
{
  EFI_STATUS                            Status;

  if (mSmmCpu == NULL) {
    Status = gSmst->SmmLocateProtocol (
                      &gEfiSmmCpuProtocolGuid,
                      NULL,
                      &mSmmCpu
                      );
    ASSERT_EFI_ERROR (Status);
  }

  return mSmmCpu->WriteSaveState (
                    mSmmCpu,
                    sizeof (UINT32),
                    RegisterNum,
                    CpuIndex,
                    RegisterData
                    );
}

//Find out which CPU triggered SW SMI
//EaxValue - Typecally the SW SMI number
//EaxValueMask - Typecally 0xff
//CpuIndex - CPU index
EFI_STATUS
IdentifyCpuIndexByEax (
  IN  UINT32                            EaxValue,
  IN  UINT32                            EaxValueMask,
  OUT UINTN                             *CpuIndex
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  UINT32                                Eax;
  UINT32                                Edx;
  UINT16                                SmiPort;

  SmiPort = 0xB2;

  if (mSmmCpu == NULL) {
    Status = gSmst->SmmLocateProtocol (
                      &gEfiSmmCpuProtocolGuid,
                      NULL,
                      &mSmmCpu
                      );
    ASSERT_EFI_ERROR (Status);
  }

  SmiPort = PcdGet16 (PcdSoftwareSmiPort);

  for (Index = 0; Index < gSmst->NumberOfCpus; Index++) {
    Status = GetDwordRegisterByCpuIndex (EFI_SMM_SAVE_STATE_REGISTER_RAX, Index, sizeof (UINT32), &Eax);
    Status = GetDwordRegisterByCpuIndex (EFI_SMM_SAVE_STATE_REGISTER_RDX, Index, sizeof (UINT32), &Edx);

    // Find out which CPU triggered SW SMI
    if (((Eax & EaxValueMask) == EaxValue) && ((Edx & 0xffff) == SmiPort)) {
      // Cpu found!
      break;
    }
  }

  if (Index == gSmst->NumberOfCpus) {
    // Error out due to CPU not found
    return EFI_NOT_FOUND;

  } else {
    *CpuIndex = Index;
    return EFI_SUCCESS;
  }
}

EFI_STATUS
GetSwSmiSubFunctionNumber (
  OUT UINT8                             *SwSmiSubFunctionNumber
  )
{
  UINT16                                SmiDataPort = 0xb3;

  SmiDataPort = PcdGet16 (PcdSoftwareSmiPort) + 1;

  *SwSmiSubFunctionNumber = IoRead8 (SmiDataPort);

  return EFI_SUCCESS;
}

EFI_STATUS
SetSwSmiSubFunctionNumber (
  IN UINT8                              SwSmiSubFunctionNumber
  )
{
  UINT16                                SmiDataPort = 0xb3;

  SmiDataPort = PcdGet16 (PcdSoftwareSmiPort) + 1;

  IoWrite8 (SmiDataPort, SwSmiSubFunctionNumber);

  return EFI_SUCCESS;
}
