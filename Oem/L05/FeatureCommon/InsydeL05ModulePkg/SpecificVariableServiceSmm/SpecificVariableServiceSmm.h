/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_SPECIFIC_VARIABLE_SERVICE_SMM_H_
#define _L05_SPECIFIC_VARIABLE_SERVICE_SMM_H_

#include <OemSwSmi.h>
#include <FlashRegionLayout.h>

#include <Library/BaseLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib/BaseLibInternals.h>
#include <Library/PcdLib.h>
#include <Library/FdSupportLib.h>
#include <Library/FlashRegionLib.h>

#include <Protocol/SmmCpu.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/L05Variable.h>

#if (FixedPcdGet32 (PcdFlashFvL05Variable1Size) == 0 || FixedPcdGet32 (PcdFlashFvL05Variable2Size) == 0)
  #error "The size of VARIABLE_SUBREGION_1 and 2 must not be 0."
#endif
#if (FixedPcdGet32 (PcdFlashFvL05Variable1Size) != FixedPcdGet32 (PcdFlashFvL05Variable2Size))
  #error "VARIABLE_SUBREGION_1 and 2 must be the same size. Please check your flash map."
#endif
#if (FixedPcdGet32 (PcdFlashFvL05Variable1Size) % EFI_PAGE_SIZE != 0)
  #error "FLASH_REGION_LENOVO_VARIABLE_REGION_SUBREGION_1_SIZE must be multiple of 4K. Please check your flash map."
#endif

#define EFI_L05_VARIABLE_FUNCTION       (UINT16)(0x5300 + (UINT8)EFI_L05_VARIABLE_CALLBACK)
#define EFI_L05_VARIABLE_EAX_MASK       0xFFFF00FF
#define EFI_L05_VARIABLE_NO_ERROR       0x0000CE00
#define LVAR_FUNCTION_NUMBER_WRONG      0x0000B100
#define LVAR_BUFFER_TOO_SMALL           0x0000B200
#define LVAR_NOT_FOUND                  0x0000B300
#define LVAR_REGIONE_LOCKED             0x0000B400
#define LVAR_OTHER_ERROR                0x0000B600
#define LVAR_VARIABLE_LOCKED            0x0000B700
#define LVAR_PASSWORD_WRONG             0x0000B800

//
// According to Lenovo's requirements,
// BIOS needs to reserve 8*4K memory buffer for LVAR tool and memory type should be EfiReservedMemoryType
//
#define LVAR_RESERVED_MEMORY_SIZE       0x00008000

//
//  Function
//
EFI_STATUS
L05SmmGetVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN OUT  UINT32                        *DataSize,
  OUT     VOID                          *Data
  );

EFI_STATUS
L05SmmSetVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      UINT32                        DataSize,
  IN      VOID                          *Data
  );

EFI_STATUS
L05SmmLockVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName
  );

EFI_STATUS
L05SmmUnlockVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      CHAR16                        *Password
  );

EFI_STATUS
L05SmmVariableEntry (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  );

EFI_STATUS
EFIAPI
L05SmmVariableCallback (
  IN  EFI_HANDLE                        DispatchHandle,
  IN  CONST  VOID                       *DispatchContext,
  IN  OUT  VOID                         *CommBuffer,
  IN  OUT  UINTN                        *CommBufferSize
  );

EFI_STATUS
SmmGetCpuSaveState (
  );

EFI_STATUS
ReadDwordRegister (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             Width,
  IN  UINT8                             CpuNum,
  OUT VOID                              *RegisterData
  );

EFI_STATUS
WriteDwordRegister (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             Width,
  IN  UINT8                             CpuNum,
  OUT VOID                              *RegisterData
  );
#endif
