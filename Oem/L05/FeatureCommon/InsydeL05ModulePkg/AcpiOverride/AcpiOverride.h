/** @file
  Override ACPI Tables for L05 Feature

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _ACPI_OVERRIDE_H_
#define _ACPI_OVERRIDE_H_

#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DevicePathLib.h>
#include <Library/DebugLib.h>
#include <Library/H2OCpLib.h>
#include <Library/FeatureLib/OemSvcAcpiTableUpdate.h>
#include <Library/FeatureLib/OemSvcUpdatePs2KeyBoardHid.h>
#include <Library/FeatureLib/OemSvcUpdatePs2TouchPadHidCid.h>

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//
#include <Protocol/AcpiSupport.h>
#include <Protocol/AcpiTable.h>
#include <Protocol/L05DxeSlp20.h>

//
// Produced Protocols
//

//
// Guids
//
#include <Guid/H2OCp.h>

//
// Driver Version
//

//
// Protocol instances
//

//
// Include files with function prototypes
//
#include <IndustryStandard/Acpi.h>
#include <IndustryStandard/SmBios.h>
#include "L05Config.h"
#include "L05Project.h"

#pragma pack(1)
typedef struct {
  EFI_ACPI_DESCRIPTION_HEADER           *Table;
  EFI_ACPI_TABLE_VERSION                Version;
  UINTN                                 Handle;
} ACPI_TABLE_INFO;

typedef struct {
  UINT8                                 SmbiosType03Type;
  UINT8                                 PreferredPmProfile;
} PREFERRED_PM_PROFILE_CONVERT_TABLE;

#pragma pack()

#endif
