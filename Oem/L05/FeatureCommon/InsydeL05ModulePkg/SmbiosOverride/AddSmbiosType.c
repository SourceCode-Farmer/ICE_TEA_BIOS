/** @file
  Add SMBIOS Type function

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "SmbiosOverride.h"

//
// Initial data of SMBIOS Type 133
//
L05_SMBIOS_TYPE_133_WIRELESS_DEVICE_SUPPORT_TABLE  mType133Data = {
  {0x85, 0x00, 0x0000},  // Type, Length, Handle
  0x01,                  // String1
  0x00                   // Index 0 of OemDataString
};

//
// Initial data of SMBIOS Type 248
//
L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_SUPPORT_TABLE  mType248Data = {
  {0xF8, 0x12, 0x0000},                          // Type, Length, Handle
  L05_SMBIOS_TYPE_248_SIGNATURE,                 // Signature
  { L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING,
    L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING,
    L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING,
    L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING,
    L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING
  },                                             // L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_SUPPORT_INFO
  0x0000                                         // End Of Table, double-null (0x0000)
};

/**
  Add SMBIOS Type 133(0x85) function

  @param  Context                       Point of input context, optional by Type
  @param  ContextSize                   Size of input context, optional by Type

  @retval EFI_SUCCESS                   Add SMBIOS Type successfully
  @retval EFI_OUT_OF_RESOURCES          Add SMBIOS Type fail for out of resource for allocate memory
  @retval EFI_INVALID_PARAMETER         Add SMBIOS Type fail for optional invalid parameter
  @retval Others                        An unexpected error occurred by referring EFI_SMBIOS_PROTOCOL.Add()
**/
EFI_STATUS
Type133ForWirelessDeviceSupport (
  IN VOID                               *Context,
  IN UINTN                              ContextSize
  )
{
  L05_SMBIOS_TYPE_133_WIRELESS_DEVICE_SUPPORT_TABLE  *Type133Table;
  CHAR8                                              OemDataString[] = L05_SMBIOS_TYPE_133_OEM_DATA_STRING;
  EFI_STATUS                                         Status;
  EFI_SMBIOS_HANDLE                                  SmbiosHandle;

  Type133Table = NULL;

  //
  // Need more 1 byte(UINT8) for double-null (0x0000) on the end of table
  //
  Type133Table = AllocateZeroPool (sizeof (L05_SMBIOS_TYPE_133_WIRELESS_DEVICE_SUPPORT_TABLE) + sizeof (OemDataString) + sizeof (UINT8));

  if (Type133Table == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Fill SMBIOS Type 133 table structure
  //
  CopyMem (Type133Table, &mType133Data, sizeof (L05_SMBIOS_TYPE_133_WIRELESS_DEVICE_SUPPORT_TABLE) - sizeof (UINT8));
  CopyMem (Type133Table->OemDataString, OemDataString, sizeof (OemDataString));

  //
  // Remove 1 byte(UINT8) for not counting double-null (0x0000) on the end of table
  //
  Type133Table->Header.Length = sizeof (L05_SMBIOS_TYPE_133_WIRELESS_DEVICE_SUPPORT_TABLE) - sizeof (UINT8);

  SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;

  Status = mSmbiosProtocol->Add (
                              mSmbiosProtocol,
                              NULL,
                              &SmbiosHandle,
                              (EFI_SMBIOS_TABLE_HEADER *) Type133Table
                              );

  FreePool (Type133Table);

  return Status;
}

/**
  Add SMBIOS Type 248(0xF8) function

  @param  Context                       Point of input context, optional by Type
  @param  ContextSize                   Size of input context, optional by Type

  @retval EFI_SUCCESS                   Add SMBIOS Type successfully
  @retval EFI_OUT_OF_RESOURCES          Add SMBIOS Type fail for out of resource for allocate memory
  @retval EFI_INVALID_PARAMETER         Add SMBIOS Type fail for optional invalid parameter
  @retval Others                        An unexpected error occurred by referring EFI_SMBIOS_PROTOCOL.Add()
**/
EFI_STATUS
Type248ForWirelessDeviceSupport (
  IN VOID                               *Context,
  IN UINTN                              ContextSize
  )
{
  EFI_STATUS                            Status;
  EFI_SMBIOS_HANDLE                     SmbiosHandle;

  if (Context == NULL || ContextSize != sizeof (L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO)) {

    return EFI_INVALID_PARAMETER;
  }

  CopyMem (&mType248Data.WirelessDeviceInfo, Context, sizeof (L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO));

  SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;

  Status = mSmbiosProtocol->Add (
                              mSmbiosProtocol,
                              NULL,
                              &SmbiosHandle,
                              (EFI_SMBIOS_TABLE_HEADER *) &mType248Data
                              );

  return Status;
}

/**
  Add SMBIOS Type function

  @param  Type                          Number of SMBIOS Type
  @param  Context                       Point of input context, optional by Type
  @param  ContextSize                   Size of input context, optional by Type

  @retval EFI_SUCCESS                   Add SMBIOS Type successfully
  @retval EFI_OUT_OF_RESOURCES          Add SMBIOS Type fail for out of resource for allocate memory
  @retval EFI_INVALID_PARAMETER         Add SMBIOS Type fail for optional invalid parameter
  @retval EFI_UNSUPPORTED               Add SMBIOS Type fail for invalid SMBIOS Type
  @retval Others                        An unexpected error occurred by referring EFI_SMBIOS_PROTOCOL.Add()
**/
EFI_STATUS
AddSmbiosType (
  IN UINT8                              Type,
  IN VOID                               *Context,
  IN UINTN                              ContextSize
  )
{
  EFI_STATUS                            Status;

  switch (Type) {

  case 133:  // 0x85

    Status = Type133ForWirelessDeviceSupport (NULL, 0);
    break;

  case 248:  // 0xF8

    Status = Type248ForWirelessDeviceSupport (Context, ContextSize);
    break;

  default:

    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}
