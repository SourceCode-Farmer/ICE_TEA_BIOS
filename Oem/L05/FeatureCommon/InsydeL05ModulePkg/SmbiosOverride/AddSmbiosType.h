/** @file
  Add SMBIOS Type function

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _ADD_SMBIOS_TYPE_H_
#define _ADD_SMBIOS_TYPE_H_

EFI_STATUS
AddSmbiosType (
  IN UINT8                              Type,
  IN VOID                               *Context,
  IN UINTN                              ContextSize
  );
#endif
