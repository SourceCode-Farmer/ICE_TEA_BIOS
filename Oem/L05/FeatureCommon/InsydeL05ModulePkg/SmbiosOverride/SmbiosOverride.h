/** @file
  Feature SMBIOS Override core function

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _EFI_SMBIOS_OVERRIDE_H_
#define _EFI_SMBIOS_OVERRIDE_H_

#include <Uefi.h>
//#include <Uefi/UefiSpec.h>
//#include <Base.h>
#include <IndustryStandard/Smbios.h>  // SMBIOS_STRUCTURE
#include <L05Config.h>
#include <OemSwSmi.h>
#include <SetupConfig.h>
#include <L05WirelessDeviceSupportConfig.h>

//
// Libraries
//
#include <Library/UefiLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/HiiLib.h>
#include <Library/PcdLib.h>
#include <Library/FeatureLib/OemSvcSmbiosOverride.h>
#include <Library/FeatureLib/OemSvcSwitchFeatureVersion.h>
#ifdef L05_SMB_BIOS_ENABLE
#include <Library/FeatureLib/OemSvcEcGetWakeUpType.h>
#endif
#include "Library/CommonSmbiosLib.h"
#include <Library/SetupUtilityLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/FeatureLib/OemSvcSecurityPassword.h>

//
// Consumed Protocols
//
#include <Protocol/Smbios.h>
#include <Protocol/Bds.h>
#include "Protocol/EndOfBdsBootSelection.h"
#include <Protocol/L05BdsEntryServices.h>

//
// Produced Protocols
//
#include <Protocol/L05SmbiosInterface.h>

//
// Guids
//

//
// Include files with function prototypes
//
#include "AddSmbiosType.h"

#define UUID_SIZE                       16
#define MAX_OF_SMBIOS_TYPE              0xFF

#define LC_PRODUCT_MTM_LENGTH           5
#define LI_PRODUCT_MTM_LENGTH           7
#define NEW_MTM_PRODUCT_MTM_LENGTH      10
#define NEW_MTM_PRODUCT_MTM_LENGTH2     4

#define LC_MT_STR_LENGTH                5
#define LI_MT_STR_LENGTH                4
#define NEW_MTM_MT_STR_LENGTH           4

#define SUPPORT_TYPE_24_200_VERSION     132

#pragma pack(1)
typedef struct {
  UINT32                                BiosReserved     :16;
  UINT32                                EnclosureType    :3;   // BIOS Characteristics Bit48:50, see below
  UINT32                                BrandType        :5;   // BIOS Characteristics Bit51:55, see below
  UINT32                                SystemReserved1  :8;
  UINT32                                Reserved         :32;
} EFI_L05_MISC_BIOS_CHARACTERISTICS;

typedef struct {
  UINT8                                 SpecMajorNumber;
  UINT8                                 SpecMinorNumber;
  UINT8                                 EepromSpecMajorNumber;
  UINT8                                 EepromSpecMinorNumber;
  UINT8                                 PmSpecMajorNumber;
  UINT8                                 PmSpecMinorNumber;
  UINT8                                 SmiIOPort;
  UINT8                                 SoftwareSmiValue;
  UINT8                                 L05VarSpecMajorNumber;
  UINT8                                 L05VarSpecMinorNumber;
} L05_PRODUCT_DATA;

typedef struct {
  SMBIOS_STRUCTURE                      Header;
  CHAR8                                 ID;
  CHAR8                                 MTM;
  L05_PRODUCT_DATA                      ProductData;
  CHAR8                                 IdString[1];
} L05_IDEAPAD_IDENTIFY;

typedef struct {
  SMBIOS_STRUCTURE                      Header;
  UINT8                                 String1; // OemDataString
  UINT8                                 OemDataString[1];
} L05_SMBIOS_TYPE_133_WIRELESS_DEVICE_SUPPORT_TABLE;

typedef struct {
  SMBIOS_STRUCTURE                          Header;
  UINT32                                    Signature;
  L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO  WirelessDeviceInfo;
  UINT16                                    EndOfTable;
} L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_SUPPORT_TABLE;
#pragma pack()

typedef
VOID
(EFIAPI *REVISE_SMBIOS_TABLE_DATA) (
  IN EFI_SMBIOS_TABLE_HEADER            *RecordData
  );

typedef
EFI_STATUS
(EFIAPI *ODM_SET_DMI_DATA) (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  );

VOID
ReviseL05SmbiosType0 (
  IN EFI_SMBIOS_TABLE_HEADER            *RecordData
  );

VOID
ReviseL05SmbiosType01 (
  IN EFI_SMBIOS_TABLE_HEADER            *RecordData
  );

VOID
ReviseL05SmbiosType02 (
  IN EFI_SMBIOS_TABLE_HEADER            *RecordData
  );

VOID
ReviseL05SmbiosType03 (
  IN EFI_SMBIOS_TABLE_HEADER            *RecordData
  );

VOID
GetSmbiosHandle (
  IN EFI_SMBIOS_TYPE                    RecordType,
  IN OUT EFI_SMBIOS_TABLE_HEADER        **Record
  );

EFI_STATUS
ODMSetDMIData135 (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  );

EFI_STATUS
ODMSetDMIData138 (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  );

extern EFI_SMBIOS_PROTOCOL  *mSmbiosProtocol;
#endif
