/** @file
  Store Lenovo String Service

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "LenovoStringService.h"

CHAR8                                   mL05String[]  = L05_STRING;
CHAR8                                   mL05String2[] = L05_STRING2;

/**
  This is a core function for store L05 string in the E000:0000 to F000:FFF0 address range
  By following perious setting, fix the address at F000:E810

  @retval EFI_SUCCESS                   The core function completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
StringServiceCore (
  )
{
  EFI_STATUS                            Status;
  EFI_LEGACY_REGION2_PROTOCOL           *LegacyRegion;
  VOID                                  *StringAddress;
  VOID                                  *StringAddress2;
  UINT32                                Granularity;

  Status         = EFI_SUCCESS;
  LegacyRegion   = NULL;
  StringAddress  = (VOID *) (UINTN) L05_STRING_DEDICATED_ADDRESS;
  StringAddress2 = (VOID *) (UINTN) L05_STRING_DEDICATED_ADDRESS2;

  Status = gBS->LocateProtocol (
                  &gEfiLegacyRegion2ProtocolGuid,
                  NULL,
                  &LegacyRegion
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = LegacyRegion->UnLock (
                           LegacyRegion,
                           L05_STRING_MIN_ADDRESS,
                           (L05_STRING_MAX_ADDRESS - L05_STRING_MIN_ADDRESS),
                           &Granularity
                           );

  if (!EFI_ERROR (Status)) {
    CopyMem (StringAddress, (VOID *) mL05String, L05_STRING_STRING_LENGTH);
    CopyMem (StringAddress2, (VOID *) mL05String2, L05_STRING_STRING_LENGTH);
  }

  Status = LegacyRegion->Lock (
                           LegacyRegion,
                           L05_STRING_MIN_ADDRESS,
                           (L05_STRING_MAX_ADDRESS - L05_STRING_MIN_ADDRESS),
                           &Granularity
                           );

  return Status;
}

/**
  This is a callback function for store L05 string

  @param  Event                         The Event for the callback function.
  @param  Context                       The context for the callback function.

  @retval NONE
**/
VOID
EFIAPI
StringServiceCallBack (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  )
{
  EFI_STATUS                            Status;

  Status = StringServiceCore ();

  //
  // There is no necessary to trigger this callback function again in any case.
  //
  if (!EFI_ERROR (Status)) {
    gBS->CloseEvent (Event);
  }

  return;
}

/**
  By following Spec "Lenovo China Minimum BIOS Spec" 3.7 SLP String,
  Storing String "LEGEND Dragon" in the E000:0000 to F000:FFF0 address range.

  @param  ImageHandle                   The firmware allocated handle for the UEFI image.
  @param  SystemTable                   A pointer to the EFI System Table.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
LenovoStringServiceDriverEntryPoint (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  EFI_EVENT                             Event;
  VOID                                  *Registration;
  UINTN                                 BufferSize;
  SYSTEM_CONFIGURATION                  SetupData;
  EFI_LEGACY_BIOS_PROTOCOL              *LegacyBios;

  Status       = EFI_SUCCESS;
  LegacyBios   = NULL;
  Event        = NULL;
  Registration = NULL;
  BufferSize   = sizeof (SYSTEM_CONFIGURATION);

  //
  // Get System Configuration settings
  //
  Status = gRT->GetVariable (
                  SETUP_VARIABLE_NAME,
                  &gSystemConfigurationGuid,
                  NULL,
                  &BufferSize,
                  &SetupData
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Check the Boot Type and POST sequence
  //
  switch (SetupData.BootType) {

  case DUAL_BOOT_TYPE:

    //
    // Register Event for store SLP String if need
    //
    Status = gBS->LocateProtocol (
                    &gEfiLegacyBiosProtocolGuid,
                    NULL,
                    &LegacyBios
                    );

    if (EFI_ERROR (Status)) {
      Status = gBS->CreateEvent (
                      EVT_NOTIFY_SIGNAL,
                      TPL_CALLBACK,
                      StringServiceCallBack,
                      NULL,
                      &Event
                      );

      if (!EFI_ERROR (Status)) {
        Status = gBS->RegisterProtocolNotify (
                        &gEfiLegacyBiosProtocolGuid,
                        Event,
                        &Registration
                        );
      }
    }

    break;

  case EFI_BOOT_TYPE:
    //
    // Do nothing, continue to next step.
    //
    break;

  case LEGACY_BOOT_TYPE:
  default:
    //
    // Exit the function directly, the OemSvcInstallLegacyBiosOemSlp() will store SLP String when boot into legacy O.S.
    //
    return EFI_SUCCESS;
    break;
  }

  //
  // This service is only finctional for UEFI and Dual Boot Type
  //
  Status = StringServiceCore ();

  return Status;
}

