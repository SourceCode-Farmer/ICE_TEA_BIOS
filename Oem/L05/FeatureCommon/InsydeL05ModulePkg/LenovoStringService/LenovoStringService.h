/** @file
  Store Lenovo String Service

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _LENOVO_STRING_SERVICE_H_
#define _LENOVO_STRING_SERVICE_H_

#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DevicePathLib.h>
#include <Library/DebugLib.h>

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//
#include <Protocol/LegacyRegion2.h>
#include <Protocol/LegacyBios.h>

//
// Produced Protocols
//

//
// Guids
//

//
// Driver Version
//

//
// Protocol instances
//

//
// Include files with function prototypes
//
#include <SetupConfig.h>
#include <L05Config.h>

#define L05_STRING_MAX_ADDRESS          0xFFFF0
#define L05_STRING_MIN_ADDRESS          0xE0000
#define L05_STRING_DEDICATED_ADDRESS    0xFE810
#define L05_STRING_DEDICATED_ADDRESS2   0xFE800

//
//                                        0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
//#define L05_STRING                    {'L', 'E', 'G', 'E', 'N', 'D', ' ', 'D', 'r', 'a', 'g', 'o', 'n', 0x00, 0x00, 0x00 }
#define L05_STRING                      L05_SLP10_STRING
//                                      {'L', 'E', 'N', 'O', 'V', 'O',0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
#define L05_STRING2                     L05_SLP10_STRING2

#define L05_STRING_STRING_LENGTH        0x10

#endif
