/** @file
  Functions for storing Feature report into stored file

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "OkFeatureReportInterfaceCoreDxe.h"

CHAR16                                  mStoredFilePathName[MAX_EFI_FILE_NAME_LENGTH + 30];

UINT8                                   mDevicePathAllowList[][2] = {
  {MESSAGING_DEVICE_PATH, MSG_USB_DP                    },  // USB storage
  {MESSAGING_DEVICE_PATH, MSG_USB_WWID_DP               },  // USB storage
  {MEDIA_DEVICE_PATH,     MEDIA_HARDDRIVE_DP            },  // SATA HDD
  {END_DEVICE_PATH_TYPE,  END_ENTIRE_DEVICE_PATH_SUBTYPE}   // End list
};

/**
 Wrap original EFI_FILE_PROTOCOL.Close() call in order to decrease code length (with setting back pointer to NULL).

 @param This                            Pointer to the file protocol.
**/
VOID
ReleaseFilePointer (
  IN EFI_FILE_PROTOCOL                  **This
  )
{
  //
  // Close EFI File Protocol
  //
  if (This != NULL && *This != NULL) {

    (*This)->Close (*This);
    *This = NULL;
  }

  return ;
}

/**
  Set the full folder path with file name into mStoredFilePathName[]
  Style: \\EFI\\Insyde\\$(SchipseName)-$(YYYYMMDD)-$(hhmmss).log
**/
VOID
SetStoredFileName (
  )
{
  EFI_STATUS                            Status;
  EFI_TIME                              CurrentTime;
  UINT32                                YearMonthDayValue;
  UINT32                                HourMinSecValue;
  CHAR16                                YearMonthDayStr[9];
  CHAR16                                HourMinSecStr[7];

  YearMonthDayValue = 0;
  HourMinSecValue   = 0;

  //
  // Get Current date and time
  //
  ZeroMem (&CurrentTime, sizeof (EFI_TIME));

  Status = gRT->GetTime (&CurrentTime, NULL);

  if (!EFI_ERROR (Status)) {

    YearMonthDayValue = CurrentTime.Year  * 10000 +
                        CurrentTime.Month * 100 +
                        CurrentTime.Day;

    HourMinSecValue = CurrentTime.Hour   * 10000 +
                      CurrentTime.Minute * 100 +
                      CurrentTime.Second;
  }

  //
  // Convert to unicode string with Null-terminated
  //
  ZeroMem (YearMonthDayStr, 9 * sizeof (CHAR16));
  UnicodeValueToStringS (YearMonthDayStr, sizeof (YearMonthDayStr), PREFIX_ZERO, (INT64) YearMonthDayValue, 8);

  ZeroMem (HourMinSecStr, 7 * sizeof (CHAR16));
  UnicodeValueToStringS (HourMinSecStr, sizeof (HourMinSecStr), PREFIX_ZERO, (INT64) HourMinSecValue, 6);

  //
  // Fill the full folder path with file name: \\EFI\\Insyde\\$(ChipsetName)-$(YYYYMMDD)-$(hhmmss).log
  //
  ZeroMem (mStoredFilePathName, (MAX_EFI_FILE_NAME_LENGTH + 30) * sizeof (CHAR16));

  StrCatS (mStoredFilePathName, sizeof (mStoredFilePathName), STORED_FILE_LOCATION);        // \\EFI\\Insyde
  StrCatS (mStoredFilePathName, sizeof (mStoredFilePathName), STORED_FILE_CHIPSET_NAME);    // $(ChipsetName)
  StrCatS (mStoredFilePathName, sizeof (mStoredFilePathName), STORED_FILE_DELIMITER);       // -
  StrCatS (mStoredFilePathName, sizeof (mStoredFilePathName), YearMonthDayStr);             // $(YYYYMMDD)
  StrCatS (mStoredFilePathName, sizeof (mStoredFilePathName), STORED_FILE_DELIMITER);       // -
  StrCatS (mStoredFilePathName, sizeof (mStoredFilePathName), HourMinSecStr);               // $(hhmmss)
  StrCatS (mStoredFilePathName, sizeof (mStoredFilePathName), STORED_FILE_EXTENSION_NAME);  // .log

  return;
}

/**
  Check the folder path is exist or not.

  @param SimpleFileSystem               Pointer to the Simple File System Protocol
  @param FolderPath                     Pointer to the folder path.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         The parameters are invalid.
  @retval Others                        The folder path not found by refer OpenVolume(), Open().
**/
EFI_STATUS
CheckFolderPath (
  IN EFI_SIMPLE_FILE_SYSTEM_PROTOCOL    *SimpleFileSystem,
  IN CHAR16                             *FolderPath
  )
{
  EFI_STATUS                            Status;
  EFI_FILE_PROTOCOL                     *RootPtr;
  EFI_FILE_PROTOCOL                     *FilePtr;

  if (SimpleFileSystem == NULL || FolderPath == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status     = EFI_SUCCESS;
  RootPtr    = NULL;
  FilePtr    = NULL;

  //
  // Get the file pointer
  //
  Status = SimpleFileSystem->OpenVolume (
                               SimpleFileSystem,
                               &RootPtr
                               );

  if (!EFI_ERROR (Status)) {

    //
    // Open the folder path
    //
    Status = RootPtr->Open (
                        RootPtr,
                        &FilePtr,
                        FolderPath,
                        EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE,
                        0
                        );
  }

  //
  // Close all pointer
  //
  ReleaseFilePointer (&FilePtr);
  ReleaseFilePointer (&RootPtr);

  return Status;
}

/**
  Set Up Stored File function
    1. Find the Stored File loacted on which interface device (USB storage, SATA HDD) by refer mDevicePathAllowList[]
    2. Check path \EFI\Insyde\ is exist or not
    3. Set the new file name for stored file into mStoredFilePathName[]
    4. Store message from temporary memory into new stored file then fill mStoredFileInfo[] and release temporary memory

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred by refer gBS->LocateHandleBuffer(), CheckFolderPath(), StoreIntoStoredFile().
**/
EFI_STATUS
SetUpStoredFile (
  )
{
  EFI_STATUS                            Status;
  UINTN                                 FileSystemHandleNum;
  EFI_HANDLE                            *FileSystemHandle;
  UINTN                                 Index;
  UINTN                                 Index2;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePath;
  UINTN                                 BeFoundOrder;
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL       *SimpleFileSysPtr;

  Status           = EFI_SUCCESS;
  FileSystemHandle = NULL;
  DevicePath       = NULL;
  SimpleFileSysPtr = NULL;


  //
  // 1. Find the Stored File loacted on which interface device (USB storage, SATA HDD) by refer mDevicePathAllowList[]
  //
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiSimpleFileSystemProtocolGuid,
                  NULL,
                  &FileSystemHandleNum,
                  &FileSystemHandle
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  for (Index = 0; Index < FileSystemHandleNum; Index++) {

    DevicePath = DevicePathFromHandle (FileSystemHandle[Index]);

    if (DevicePath == NULL) {
      continue;
    }

    //
    // Find the current device (USB storage, SATA HDD)
    //
    BeFoundOrder = sizeof (mDevicePathAllowList) / 2 - 1;

    while (!IsDevicePathEnd (DevicePath)) {

      for (Index2 = 0; mDevicePathAllowList[Index2][0] != END_DEVICE_PATH_TYPE; Index2++) {

        if (DevicePathType (DevicePath)    == mDevicePathAllowList[Index2][0] &&
            DevicePathSubType (DevicePath) == mDevicePathAllowList[Index2][1]) {

          if (Index2 < BeFoundOrder) {
            BeFoundOrder = Index2;
          }

          break;
        }
      }

      if (BeFoundOrder == 0) {  // USB storage

        break;

      } else {

        DevicePath = NextDevicePathNode (DevicePath);
      }
    }

    //
    // 2. Check path \EFI\Insyde\ is exist or not
    //
    Status = gBS->HandleProtocol (
                    FileSystemHandle[Index],
                    &gEfiSimpleFileSystemProtocolGuid,
                    (VOID **) &SimpleFileSysPtr
                    );

    if (EFI_ERROR (Status)) {
      continue;
    }

    Status = CheckFolderPath (SimpleFileSysPtr, STORED_FILE_LOCATION);

    if (EFI_ERROR (Status)) {
      continue;
    }

    //
    // 3. Set the new file name for stored file into mStoredFilePathName[]
    //
    SetStoredFileName ();

    //
    // 4. Store message from temporary memory into new stored file then release temporary memory
    //
    mStoredFileInfo->SimpleFileSystem = SimpleFileSysPtr;

    Status = StoreIntoStoredFile (mTemporaryMemInfo->Address, mTemporaryMemInfo->UsedSize);

    if (!EFI_ERROR (Status)) {

      SafeFreePool ((VOID **) &(mTemporaryMemInfo->Address));
      break;
    }

    mStoredFileInfo->SimpleFileSystem = NULL;
  }

  SafeFreePool ((VOID **) &FileSystemHandle);

  return Status;
}

/**
  gEfiSimpleFileSystemProtocolGuid protocol notify function to set up Stored FileL

  @param Event                          Event whose notification function is being invoked.
  @param Context                        Pointer to the notification function's context.
**/
VOID
EFIAPI
SimpleFileSystemProtocolCallback (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  )
{
  EFI_STATUS                            Status;

  Status = SetUpStoredFile ();

  if (!EFI_ERROR (Status)) {

    gBS->CloseEvent (Event);
  }

  return;
}

/**
  Storing Feature report message into stored file.

  @param  Message                       Pointer to the store message
  @param  MessageLength                 The size of store message

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred by refer OpenVolume(), Open(), GetInfo(), SetPosition(), Write().
**/
EFI_STATUS
StoreIntoStoredFile (
  CHAR8                                 *Message,
  UINTN                                 MessageLength
  )
{
  EFI_STATUS                            Status;
  EFI_FILE_PROTOCOL                     *RootPtr;
  EFI_FILE_PROTOCOL                     *FilePtr;
  UINT8                                 Buffer[MAX_FILE_INFO_SIZE];
  UINTN                                 BufferSize;
  EFI_FILE_INFO                         *FileInfo;
  UINT32                                RetryCount;

  if (Message == NULL || MessageLength == 0) {
    return EFI_INVALID_PARAMETER;
  }

  Status     = EFI_SUCCESS;
  RootPtr    = NULL;
  FilePtr    = NULL;
  BufferSize = MAX_FILE_INFO_SIZE;
  FileInfo   = (EFI_FILE_INFO *) Buffer;
  RetryCount = 0;

  ZeroMem (Buffer, MAX_FILE_INFO_SIZE);

  //
  // Get the name for stored file
  //
  Status = mStoredFileInfo->SimpleFileSystem->OpenVolume (
                                                mStoredFileInfo->SimpleFileSystem,
                                                &RootPtr
                                                );

  if (EFI_ERROR (Status)) {
    goto Exit;
  }

  //
  // Check size of current stored file with storing messang is not bigger than MAX. size limitation.
  //
  do {

    if (RetryCount != 0) {

      SafeFreePool ((VOID **) &FileInfo);
      ReleaseFilePointer (&FilePtr);

      //
      // Set new Stored File name
      //
      SetStoredFileName ();

    } else if (RetryCount >= 3) {

      //
      // Retry 3 times for open or create stored file
      //
      goto Exit;
    }

    //
    // Open/Create the stored file
    //
    Status = RootPtr->Open (
                        RootPtr,
                        &FilePtr,
                        mStoredFilePathName,
                        EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE | EFI_FILE_MODE_CREATE,
                        0
                        );

    if (EFI_ERROR (Status)) {
      goto Exit;
    }

    //
    // Get current file size
    //
    Status = FilePtr->GetInfo (FilePtr, &gEfiFileInfoGuid,  &BufferSize, FileInfo);

    if (EFI_ERROR (Status)) {
      goto Exit;
    }

    RetryCount++;

  } while (MessageLength + (UINTN) FileInfo->FileSize > mStoredFileInfo->MaxSize);

  //
  // point to th end of file
  //
  Status = FilePtr->SetPosition (FilePtr, (UINT64) FileInfo->FileSize);

  if (EFI_ERROR (Status)) {
    goto Exit;
  }

  //
  // Store message into file
  //
  BufferSize = MessageLength;
  Status = FilePtr->Write (FilePtr, &BufferSize, (VOID *) Message);

  if (EFI_ERROR (Status)) {
    goto Exit;
  }

Exit:
  //
  // Close file
  //
  SafeFreePool ((VOID **) &FileInfo);

  ReleaseFilePointer (&FilePtr);
  ReleaseFilePointer (&RootPtr);

  return Status;
}

/**
  Stored File Initialize function:
    Register protocol notify function to open/create stored file

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_UNSUPPORTED               Register protocol notify function  failed.
**/
EFI_STATUS
InitializeStoredFile (
  )
{
  EFI_STATUS                            Status;
  EFI_EVENT                             Event;
  VOID                                  *Registration;

  Status       = EFI_SUCCESS;
  Event        = NULL;
  Registration = NULL;

  //
  // Register gEfiSimpleFileSystemProtocolGuid protocol notify function
  //
  Event = EfiCreateProtocolNotifyEvent (
            &gEfiSimpleFileSystemProtocolGuid,
            TPL_CALLBACK,
            SimpleFileSystemProtocolCallback,
            NULL,
            &Registration
            );

  if (Event == NULL) {

    Status = EFI_UNSUPPORTED;
  }

  return Status;
}
