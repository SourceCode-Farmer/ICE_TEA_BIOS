/** @file
  OK Feature Report Interface Core

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OK_FEATURE_REPORT_INTERFACE_CORE_DXE_H_
#define _OK_FEATURE_REPORT_INTERFACE_CORE_DXE_H_

#include <Uefi.h>
#include <L05ChipsetNameList.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>  // gRT->GetTime
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PrintLib.h>  // AsciiVSPrint, UnicodeValueToStringS
#include <Library/UefiLib.h>  // EfiCreateProtocolNotifyEvent
#include <Library/DevicePathLib.h>  // DevicePathFromHandle
#include <Library/OkFeatureReportLib.h>  // FEATURE_REPORT

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//
#include <Protocol/SimpleFileSystem.h>  // gEfiSimpleFileSystemProtocolGuid, EFI_FILE_PROTOCOL
#include <Protocol/DevicePath.h>  // EFI_DEVICE_PATH_PROTOCOL


//
// Produced Protocols
//
#include <Protocol/OkFeatureReportInterface.h>


//
// Guids
//
#include <Guid/FileInfo.h>  // EFI_FILE_INFO, gEfiFileInfoGuid

//
// Driver Version
//

//
// Protocol instances
//

//
// Include files with function prototypes
//
#include "TemporaryMemmoryFunc.h"
#include "StoredFileFunc.h"


//
// Internal Structure definition
//
typedef struct {
  UINTN                                 TotalSize;
  UINTN                                 UsedSize;
  CHAR8                                 *Address;
} TEMPORARY_MEMORY_INFO;

typedef struct {
  UINTN                                 MaxSize;
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL       *SimpleFileSystem;
} STORED_FILE_INFO;


#define MAX_REPORT_MESSAGE_LENGTH       256

#define TEMPORARY_MEMORY_SIZE           256 * 1024 // 256 KB

#define MAX_EFI_FILE_NAME_LENGTH        255
#define MAX_FILE_INFO_SIZE              sizeof (EFI_FILE_INFO) + MAX_EFI_FILE_NAME_LENGTH

#define STORED_FILE_SIZE                256 * 1024 // 256 KB
#define STORED_FILE_LOCATION            L"\\EFI\\Insyde\\"
#define STORED_FILE_DELIMITER           L"-"
#define STORED_FILE_EXTENSION_NAME      L".log"

#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_BAYTRAIL_T)
  #define STORED_FILE_CHIPSET_NAME       L"BayTrail-T"
#elif   (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_BROADWELL)
  #define STORED_FILE_CHIPSET_NAME       L"Broadwell"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CARRIZOLITE)
  #define STORED_FILE_CHIPSET_NAME       L"CarrizoLite"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CARRIZO)
  #define STORED_FILE_CHIPSET_NAME       L"Carrizo"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_SKYLAKE)
  #define STORED_FILE_CHIPSET_NAME       L"SkyLake"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_BRASWELL)
  #define STORED_FILE_CHIPSET_NAME       L"Braswell"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CHERRYTRAIL)
  #define STORED_FILE_CHIPSET_NAME       L"CherryTrail"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_STONEY)
  #define STORED_FILE_CHIPSET_NAME       L"Stoney"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_BRISTOL)
  #define STORED_FILE_CHIPSET_NAME       L"Bristol"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_APOLLOLAKE)
  #define STORED_FILE_CHIPSET_NAME       L"ApolloLake"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_KABYLAKE)
  #define STORED_FILE_CHIPSET_NAME       L"Kabylake"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_MULLINLS)
  #define STORED_FILE_CHIPSET_NAME       L"Mullins"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RAVENRIDGE)
  #define STORED_FILE_CHIPSET_NAME       L"Ravenridge"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_GEMINILAKE)
  #define STORED_FILE_CHIPSET_NAME       L"Geminilake"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CANNONLAKE)
  #define STORED_FILE_CHIPSET_NAME       L"Cannonlake"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ICELAKE)
  #define STORED_FILE_CHIPSET_NAME       L"Icelake"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_PICASSO)
  #define STORED_FILE_CHIPSET_NAME       L"Picasso"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_COMETLAKE)
  #define STORED_FILE_CHIPSET_NAME       L"CometLake"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_TIGERLAKE)
  #define STORED_FILE_CHIPSET_NAME       L"TigerLake"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RENOIR)
  #define STORED_FILE_CHIPSET_NAME       L"Renoir"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CEZANNE)
  #define STORED_FILE_CHIPSET_NAME       L"Cezanne"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_JASPERLAKE)
  #define STORED_FILE_CHIPSET_NAME       L"JasperLake"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_ALDERLAKE)
  #define STORED_FILE_CHIPSET_NAME       L"AlderLake"
#elif (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_REMBRANDT)
  #define STORED_FILE_CHIPSET_NAME       L"Rembrandt"
#else
  #define STORED_FILE_CHIPSET_NAME       L"Unknown"
#endif

//
// Internal function prototypes definition
//
VOID
SafeFreePool (
  IN OUT VOID                           **Buffer
  );

//
// Extension declaration
//
extern TEMPORARY_MEMORY_INFO  mTemporaryMemInfo[];
extern STORED_FILE_INFO       mStoredFileInfo[];

#endif
