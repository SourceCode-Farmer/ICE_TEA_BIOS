/** @file
  OK Feature Report Interface Core

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "OkFeatureReportInterfaceCoreDxe.h"

TEMPORARY_MEMORY_INFO                   mTemporaryMemInfo[] = {
  TEMPORARY_MEMORY_SIZE,
  0,
  NULL
};

STORED_FILE_INFO                        mStoredFileInfo[] = {
  STORED_FILE_SIZE,
  NULL
};

STATIC CONST CHAR8                      Hex[] = "0123456789ABCDEF";

/**
  Wrap original FreePool gBS call in order to decrease code length (with setting back Buffer to NULL).

  @param Buffer                         Pointer to the allocated memory address.
**/
VOID
SafeFreePool (
  IN OUT VOID                           **Buffer
  )
{
  EFI_STATUS                            Status;

  if (Buffer != NULL && *Buffer != NULL) {

    Status = gBS->FreePool (*Buffer);

    if (!EFI_ERROR (Status)) {

      *Buffer = NULL;
    }
  }
}

/**
  Record Feature report message function into stored file or temporary memory

  @param  Format                        A Null-terminated ASCII format string
  @param  Marker                        VA_LIST marker for the variable argument list

  @retval EFI_SUCCESS                   The operation completed successfully
  @retval Others                        An unexpected error occurred by refer StoreIntoStoredFile() or StoreIntoTemporaryMem()
**/
EFI_STATUS
RecordMessage (
  IN CONST CHAR8                        *Format,
  IN       VA_LIST                      Marker
  )
{
  EFI_STATUS                            Status;
  CHAR8                                 Buffer[MAX_REPORT_MESSAGE_LENGTH];
  UINTN                                 MessageLength;

  Status        = EFI_SUCCESS;
  MessageLength = 0;
  ZeroMem (Buffer, sizeof (Buffer));

  //
  // Convert the FEATURE_REPORT() message to an ASCII String
  //
  MessageLength = AsciiVSPrint (Buffer, sizeof (Buffer), Format, Marker);

  //
  // store into file or temporary memory
  //
  if (mStoredFileInfo->SimpleFileSystem != NULL) {

    Status = StoreIntoStoredFile (Buffer, MessageLength);

  } else {

    Status = StoreIntoTemporaryMem (Buffer, MessageLength);
  }

  return Status;
}

/**
  Record Feature report HEX data function by calling macro FEATURE_REPORT ()

  @param  Location                      Memory address of data
  @param  Length                        Byte Size of data

  @retval EFI_SUCCESS                   The operation completed successfully
  @retval EFI_INVALID_PARAMETER         Location is NULL or Length is 0
**/
EFI_STATUS
RecordHexData (
  IN VOID                               *Location,
  IN UINTN                              Length
  )
{
  UINT8                                 *Data;
  CHAR8                                 Val[50];
  CHAR8                                 Str[20];
  UINT8                                 TempByte;
  UINTN                                 Size;
  UINTN                                 Index;

  if (Location == NULL || Length <= 0) {
    return EFI_INVALID_PARAMETER;
  }

  FEATURE_REPORT (("  Memory Address %016LX 0x%X Bytes\n", Location, Length));

  Data = (UINT8 *) Location;

  while (Length != 0) {

    Size = 16;

    if (Size > Length) {
      Size = Length;
    }

    for (Index = 0; Index < Size; Index += 1) {
      TempByte            = Data[Index];
      Val[Index * 3 + 0]  = Hex[TempByte >> 4];
      Val[Index * 3 + 1]  = Hex[TempByte & 0xF];
      Val[Index * 3 + 2]  = (CHAR8) ((Index == 7) ? '-' : ' ');
      Str[Index]          = (CHAR8) ((TempByte < ' ' || TempByte > 'z') ? '.' : TempByte);
    }

    Val[Index * 3]  = 0;
    Str[Index]      = 0;
    FEATURE_REPORT (("  %08LX: %-48a *%a*\n", Data - (UINT8 *) Location, Val, Str));

    Data += Size;
    Length -= Size;
  }

  return EFI_SUCCESS;
}

/**
  Get Feature Report Message from Memory, caller must check the output parameters are valid or not.

  @param  Location                      Pointer to the reserved memory offset 0
  @param  TotalSize                     Pointer to the total size of reserved memory

  @retval EFI_SUCCESS                   The output parameters be set successfully
**/
EFI_STATUS
GetMessageFromMemory (
  OUT CHAR8                             **Location,
  OUT UINTN                             *TotalSize
  )
{
  *Location  = mTemporaryMemInfo->Address;

  if (mTemporaryMemInfo->Address != NULL) {

    *TotalSize = mTemporaryMemInfo->TotalSize;

  } else {

    *TotalSize = 0;
  }

  return EFI_SUCCESS;
}

/**
  This is the declaration of an EFI image entry point. This entry point is
  the same for UEFI Applications, UEFI OS Loaders, and UEFI Drivers including
  both device drivers and bus drivers.

  @param  ImageHandle                   The firmware allocated handle for the UEFI image.
  @param  SystemTable                   A pointer to the EFI System Table.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
OkFeatureReportInterfaceCoreDxeDriverEntryPoint (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                                Status;
  EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL  *OkFeatureReportInterface;

  Status                   = EFI_SUCCESS;
  OkFeatureReportInterface = NULL;

  //
  // Initializ Temporary Memory region for store message
  //
  Status = InitializeTemporaryMem ();

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Initializ stored file related information for store message when system is ready
  //
  Status = InitializeStoredFile ();

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Install gEfiOkFeatureReportInterfaceProtocolGuid for provide Feature Report interface
  //
  OkFeatureReportInterface = AllocateZeroPool (sizeof (EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL));

  if (OkFeatureReportInterface == NULL) {

    SafeFreePool ((VOID **) &(mTemporaryMemInfo->Address));
    return EFI_OUT_OF_RESOURCES;
  }

  OkFeatureReportInterface->RecordMessage        = RecordMessage;
  OkFeatureReportInterface->RecordHexData        = RecordHexData;
  OkFeatureReportInterface->GetMessageFromMemory = GetMessageFromMemory;

  Status = gBS->InstallProtocolInterface (
                  &ImageHandle,
                  &gEfiOkFeatureReportInterfaceProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  OkFeatureReportInterface
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  FEATURE_REPORT (("Start L05 Feature Report\n"));
  FEATURE_REPORT (("[OkFeatureReportInterfaceCoreDxe] Dispatch - %r\n", Status));

  return EFI_SUCCESS;
}
