/** @file
  Functions for storing Feature report into temporary memory

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "OkFeatureReportInterfaceCoreDxe.h"

/**
  Storing Feature report message into temporary memory.

  @param  Message                       Pointer to the store message
  @param  MessageLength                 The size of store message

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_OUT_OF_RESOURCES          Temporary memory is too smaller to storing Feature report message.
**/
EFI_STATUS
StoreIntoTemporaryMem (
  CHAR8                                 *Message,
  UINTN                                 MessageLength
  )
{
  EFI_STATUS                            Status;
  CHAR8                                 *CurrentStoreMemPtr;

  Status             = EFI_SUCCESS;
  CurrentStoreMemPtr = NULL;

  //
  // Point to available space
  //
  CurrentStoreMemPtr = mTemporaryMemInfo->Address;
  CurrentStoreMemPtr += mTemporaryMemInfo->UsedSize;

  //
  // Count the used size of temporary memory
  //
  mTemporaryMemInfo->UsedSize += MessageLength;

  if (mTemporaryMemInfo->UsedSize <= mTemporaryMemInfo->TotalSize) {

    //
    // Store message into memory
    //
    CopyMem ((VOID *) CurrentStoreMemPtr, (VOID *) Message, MessageLength);

  } else {

    Status = EFI_OUT_OF_RESOURCES;
  }

  return Status;
}

/**
  Temporary Memory Initialize function:
    Allocate temporary memory for store Feature report message and fill mTemporaryMemInfo[]

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_OUT_OF_RESOURCES          Allocate temporary memory failed.
**/
EFI_STATUS
InitializeTemporaryMem (
  )
{
  EFI_STATUS                            Status;

  Status = EFI_SUCCESS;

  mTemporaryMemInfo->Address = AllocateZeroPool (mTemporaryMemInfo->TotalSize);

  if (mTemporaryMemInfo->Address == NULL) {

    Status = EFI_OUT_OF_RESOURCES;
  }

  return Status;
}
