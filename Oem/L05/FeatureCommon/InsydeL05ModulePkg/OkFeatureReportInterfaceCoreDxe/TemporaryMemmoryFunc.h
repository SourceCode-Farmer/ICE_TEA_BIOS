/** @file
  Functions for storing Feature report into temporary memory

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _TEMPORARY_MEMMORY_FUNC_H_
#define _TEMPORARY_MEMMORY_FUNC_H_

EFI_STATUS
StoreIntoTemporaryMem (
  CHAR8                                 *Message,
  UINTN                                 MessageLength
  );

EFI_STATUS
InitializeTemporaryMem (
  );

#endif
