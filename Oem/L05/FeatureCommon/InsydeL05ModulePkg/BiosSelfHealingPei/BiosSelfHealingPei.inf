## @file
#  BIOS Self-Healing PEI Module.
#
#******************************************************************************
#* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = BiosSelfHealingPei
  FILE_GUID                      = 76EB2674-B3F2-4489-BB6D-E2448F7EF614
  MODULE_TYPE                    = PEIM
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = BiosSelfHealingPeiEntryPoint

[Packages]
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
!if $(L05_CHIPSET_VENDOR) == INTEL
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
!endif
!if $(L05_CHIPSET_VENDOR) == AMD
  AgesaModulePkg/AgesaModulePspPkg.dec
!endif
  InsydeModulePkg/InsydeModulePkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec

[Sources]
  BiosSelfHealingPei.c
  BiosSelfHealingPei.h

[LibraryClasses]
  PeimEntryPoint
  PeiServicesLib
  BaseLib
  BaseMemoryLib
  BaseCryptLib
  HobLib
  DebugLib
  CmosLib
  H2OCpLib
  VariableLib
  ResetSystemLib  # ResetCold
  BiosSelfHealingLib
  PeiOemSvcFeatureLibDefault

[Ppis]
  gEfiPeiReadOnlyVariable2PpiGuid
  gEfiPeiMemoryDiscoveredPpiGuid
  gFirmwareAuthenticationPpiGuid
  gEfiPeiRecoveryModulePpiGuid
  gEfiEndOfPeiSignalPpiGuid
!if $(L05_CHIPSET_VENDOR) == AMD
  gAmdPspCommonServicePpiGuid
!endif

[Guids]
  gH2OFlashMapRegionDxeFvGuid
  gSystemConfigurationGuid

[FeaturePcd]
  gInsydeTokenSpaceGuid.PcdH2OBaseCpVerifyFvSupported

[Pcd]
  gInsydeTokenSpaceGuid.PcdFlashAreaBaseAddress
  gInsydeTokenSpaceGuid.PcdFlashFvMainBase
  gInsydeTokenSpaceGuid.PcdFlashFvMainSize
  gInsydeTokenSpaceGuid.PcdH2OFlashDeviceMapStart
  gInsydeTokenSpaceGuid.PcdFlashNvStorageVariableDefaultsBase
  gL05ServicesTokenSpaceGuid.PcdL05TopSwapEnable
  gL05ServicesTokenSpaceGuid.PcdL05BiosRecoveryHotkeyFlag
  gL05ServicesTokenSpaceGuid.PcdL05BiosSelfHealingEnable
  gL05ServicesTokenSpaceGuid.PcdL05ChipsetName

[Depex]
  TRUE

[BuildOptions]
!if $(INSYDE_DEBUGGER) == YES
  *_*_*_CC_FLAGS = /Od
!endif
