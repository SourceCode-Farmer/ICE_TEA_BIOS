/** @file
  BIOS Self-Healing PEI Module.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BIOS_SELF_HEALING_PEI_H_
#define _BIOS_SELF_HEALING_PEI_H_

#include <Uefi.h>
#include <OemCmos.h>
#include <SetupConfig.h>
#include <L05ChipsetNameList.h>

//
// Libraries
//
#include <Library/PeiServicesLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseCryptLib.h>
#include <Library/HobLib.h>
#include <Library/DebugLib.h>
#include <Library/CmosLib.h>
#include <Library/VariableLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/ResetSystemLib.h>  // ResetCold
#include <Library/H2OCpLib.h>
#include <Library/BiosSelfHealingLib.h>
#include <Library/FeatureLib/OemSvcNotifyEcToPeiStart.h>
#include <Library/FeatureLib/OemSvcNotifyEcToClearWdt.h>

//
// Consumed Ppis
//
#include <Ppi/FirmwareAuthentication.h>
#include <Ppi/RecoveryModule.h>

//
// Produced Ppis
//

//
// Guids
//
#include <Guid/H2OCp.h>

//
// Include files with function prototypes
//
EFI_STATUS
SelfHealingInitCallback (
  IN  EFI_PEI_SERVICES                  **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR         *NotifyDescriptor,
  IN  VOID                              *Ppi
  );

EFI_STATUS
Stage2EcNotifyCallback (
  IN  EFI_PEI_SERVICES                  **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR         *NotifyDescriptor,
  IN  VOID                              *Ppi
  );

EFI_STATUS
ClearWdtCallback (
  IN  EFI_PEI_SERVICES                  **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR         *NotifyDescriptor,
  IN  VOID                              *Ppi
  );

EFI_STATUS
InstallFirmwareAuthHook (
  IN  EFI_PEI_SERVICES                  **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR         *NotifyDescriptor,
  IN  VOID                              *Ppi
  );

EFI_STATUS
FirmwareAuthHookCallback (
  IN  EFI_PEI_SERVICES                  **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR         *NotifyDescriptor,
  IN  VOID                              *Ppi
  );

EFI_STATUS
VerifyFirmware (
  IN  UINT8                             *FirmwareFileData,
  IN  UINTN                             FirmwareFileSize
  );

#endif
