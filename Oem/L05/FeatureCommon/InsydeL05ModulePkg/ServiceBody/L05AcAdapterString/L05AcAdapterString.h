/** @file
  For show AC adapter string.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_AC_ADAPTER_STRING_H_
#define _L05_AC_ADAPTER_STRING_H_

#include <Library/FeatureLib/OemSvcAcAdapterString.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>

#define NOT_CHARING                     0x00
#define NOMINAL_CHARGING                0x01
#define SLOW_CHARGING                   0x02
#define INCOMPATIBLE_CHARGING           0x03
#define UNKNOW_CHARGING                 0x04

VOID
EFIAPI
L05AcAdapterStringCallBack (
  IN  EFI_EVENT                         Event,
  IN  VOID                              *Context
  );

EFI_STATUS
EFIAPI
ShowAdapterMessage (
  IN  UINT8                             Chargingstatus
  );

#endif
