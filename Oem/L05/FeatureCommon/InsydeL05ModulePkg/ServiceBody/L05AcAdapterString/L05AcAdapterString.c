/** @file
  For show AC adapter string.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "L05AcAdapterString.h"
//[-start-220415-QINGLIN0168-add]//
#ifdef S370_SUPPORT
#include <Library/LfcEcLib.h>
#endif
//[-end-220415-QINGLIN0168-add]//

CHAR16  *mL05AcSlowCharging[]          = {L"The connected AC adapter has a lower wattage than the\n",
                                          L"recommended AC adapter which was shipped with the system.\n",
                                          L"To use AC power, please connect the AC adapter which was\n",
                                          L"shipped with the system.\n",
                                          L"Press Esc to continue."};

CHAR16  *mL05AcIncompatibleCharging[]  = {L"The connected AC adapter has a lower wattage than the\n",
                                          L"recommended AC adapter which was shipped with the system.\n",
                                          L"To use AC power, please connect the AC adapter which was\n",
                                          L"shipped with the system.\n",
                                          L"Press Esc to continue."};

CHAR16  *mL05AcUnknowCharging[]        = {L"An unknown adapter is connected to the system.\n",
                                          L"To use AC power, please connect the AC\n",
                                          L"adapter which was shipped with the system.\n",
                                          L"Press Esc to continue."};


EFI_STATUS
EFIAPI
ShowAdapterMessage (
  IN  UINT8                             Chargingstatus
  )
{
  EFI_INPUT_KEY                         Key;
  UINTN                                 Column;
  UINTN                                 Row;
  UINTN                                 NextRow;
  UINTN                                 StringCounts;
  UINT8                                 Index;
  BOOLEAN                               CheckStatus;

  Column       = 0;
  Row          = 0;
  NextRow      = 0;
  Index        = 0;
  StringCounts = 0;
  CheckStatus  = FALSE;

  ZeroMem (&Key, sizeof (EFI_INPUT_KEY));

  gST->ConOut->ClearScreen (gST->ConOut);  // ClearScreen function for clear Boot logo.

  gST->ConOut->SetCursorPosition (gST->ConOut, Column, Row);

  switch (Chargingstatus) {

  case NOT_CHARING:
    break;
  case NOMINAL_CHARGING:
    break;
  case SLOW_CHARGING:
    CheckStatus = TRUE;
    StringCounts = ARRAY_SIZE (mL05AcSlowCharging);
    for (Index = 0; Index < StringCounts; Index++) {
      gST->ConOut->OutputString (gST->ConOut, mL05AcSlowCharging[Index]);
      NextRow = Index + 1;
      gST->ConOut->SetCursorPosition (gST->ConOut, 0, NextRow);
    }
    break;
  case INCOMPATIBLE_CHARGING:
//[-start-220415-QINGLIN0168-add]//
#ifdef S370_SUPPORT
    LfcCleanUpKbcAndEcBuffer ();
#endif
//[-end-220415-QINGLIN0168-add]//
    CheckStatus = TRUE;
    StringCounts = ARRAY_SIZE (mL05AcIncompatibleCharging);
    for (Index = 0; Index < StringCounts; Index++) {
      gST->ConOut->OutputString (gST->ConOut, mL05AcIncompatibleCharging[Index]);
      NextRow = Index + 1;
      gST->ConOut->SetCursorPosition (gST->ConOut, 0, NextRow);
    }
    break;
  case UNKNOW_CHARGING:
    CheckStatus = TRUE;
    StringCounts = ARRAY_SIZE (mL05AcUnknowCharging);
    for (Index = 0; Index < StringCounts; Index++) {
      gST->ConOut->OutputString (gST->ConOut, mL05AcUnknowCharging[Index]);
      NextRow = Index + 1;
      gST->ConOut->SetCursorPosition (gST->ConOut, 0, NextRow);
    }
    break;
  default:
    break;
  }

  if (CheckStatus) {
    while (TRUE) {
      gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);
      if (Key.ScanCode == SCAN_ESC) {
        gST->ConOut->ClearScreen (gST->ConOut);
        break;
      }
    }
  }

  return EFI_SUCCESS;
}

/**
  This is the event callback function for make sure Ac charging status
  and show specific Message at post time.

  @param  Event                         Pointer to this event
  @param  Context                       Event hanlder private data
**/
VOID
EFIAPI
L05AcAdapterStringCallBack (
  IN  EFI_EVENT                         Event,
  IN  VOID                              *Context
  )
{
  EFI_STATUS                            Status;
  UINT8                                 Chargingstatus;

  Status         = EFI_SUCCESS;
  Chargingstatus = 0x01;

  Status = OemSvcAcAdapterString (&Chargingstatus);

  if (EFI_ERROR (Status)) {
    return;
  }

  Status = ShowAdapterMessage (Chargingstatus);

  return;
}
