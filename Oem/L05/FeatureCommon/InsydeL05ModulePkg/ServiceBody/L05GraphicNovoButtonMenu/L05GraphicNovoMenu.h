/** @file
 L05GraphicNovoMenu.h

;******************************************************************************
;* Copyright (c) 2012-2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_NOVO_MENU_H_
#define _L05_NOVO_MENU_H_

#include "L05DxeServiceBody.h"
#include "L05DxeServiceBodyStrDefs.h"
#include "L05NovoButtonMenuCommon.h"
#include <Guid/MdeModuleHii.h>
#include <Guid/L05H2OSetup.h>
#include <Library/DevicePathLib.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/HiiLib.h>
#include <Protocol/HiiConfigAccess.h>

#define L05_NOVO_MENU_FORM_ID           0x1001

#define LABEL_L05_NOVO_MENU_START       0x00
#define LABEL_L05_NOVO_MENU_END         0x01

extern EFI_GUID  gL05NovoMenuFormSetGuid;

//
// These are the VFR compiler generated data representing our VFR data.
//
extern UINT8  L05GraphicNovoMenuVfrBin[];

#define L05_NOVO_MENU_CALLBACK_DATA_SIGNATURE  SIGNATURE_32 ('N', 'M', 'C', 'B')

#pragma pack(1)
typedef struct {
  UINTN                                 Signature;

  //
  // HII relative handles
  //
  EFI_HII_HANDLE                        HiiHandle;
  EFI_HANDLE                            DriverHandle;

  //
  // Produced protocols
  //
  EFI_HII_CONFIG_ACCESS_PROTOCOL        ConfigAccess;
} L05_NOVO_MENU_CALLBACK_DATA;
#pragma pack()

/**
  This call back function is registered with Boot Manager formset.
  When user selects a boot option, this call back function will
  be triggered. The boot option is saved for later processing.


  @param This                           Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param Action                         Specifies the type of action taken by the browser.
  @param QuestionId                     A unique value which is sent to the original exporting driver
                                        so that it can identify the type of data to expect.
  @param Type                           The type of value for the question.
  @param Value                          A pointer to the data being sent to the original exporting driver.
  @param ActionRequest                  On return, points to the action requested by the callback function.

  @retval  EFI_SUCCESS                  The callback successfully handled the action.
  @retval  EFI_INVALID_PARAMETER        The setup browser call this function with invalid parameters.

**/
EFI_STATUS
EFIAPI
NovoMenuCallback (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  EFI_BROWSER_ACTION                    Action,
  IN  EFI_QUESTION_ID                       QuestionId,
  IN  UINT8                                 Type,
  IN  EFI_IFR_TYPE_VALUE                    *Value,
  OUT EFI_BROWSER_ACTION_REQUEST            *ActionRequest
  );

EFI_STATUS
L05GraphicNovoButtonMenu (
  IN OUT  UINTN                         *L05NovoButtonMenuSelection
  );

EFI_STATUS
EFIAPI
NovoMenuFakeExtractConfig (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  CONST EFI_STRING                      Request,
  OUT EFI_STRING                            *Progress,
  OUT EFI_STRING                            *Results
  );

EFI_STATUS
EFIAPI
NovoMenuFakeRouteConfig (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL  *This,
  IN  CONST EFI_STRING                      Configuration,
  OUT EFI_STRING                            *Progress
  );
#endif
