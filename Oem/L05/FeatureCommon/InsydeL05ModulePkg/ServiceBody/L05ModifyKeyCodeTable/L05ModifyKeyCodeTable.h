//;******************************************************************************
//;* Copyright (c) 1983-2011, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#ifndef _L05_DXE_MODIFY_KEY_CODE_TABLE_H_
#define _L05_DXE_MODIFY_KEY_CODE_TABLE_H_

#include <Protocol/LegacyRegion2.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Protocol/StartOfBdsDiagnostics.h>
//#include EFI_PROTOCOL_DEFINITION (LegacyRegion)
//#include EFI_PROTOCOL_DEFINITION (StartOfBdsDiagnostics)

#define  L05_KEY_CODE_TABLE_ADDRESS_MIN     0x0FF000
#define  L05_KEY_CODE_TABLE_ADDRESS_MAX     0x0FFD00

#define  L05_KEY_CODE_TABLE_IDENTIFY_ENTRY  {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  /* Dummy */ \
                                             0x00, 0x1B, 0x01, 0x1B, 0x01, 0x1B, 0x01, 0xF0, 0x01}  /* Esc   */
                                             
                                           /* !1    #3    $4    %5    &7    *8    (9    )0    +=  */
#define  L05_KEY_MODIFY_CTRL_CODE_TABLE     {0x02, 0x04, 0x05, 0x06, 0x08, 0x09, 0x0A, 0x0B, 0x0D,   \
                                           /* :;    "'    ~`    <,    >.    ?/   J-_   J-yen      */ \
                                             0x27, 0x28, 0x29, 0x33, 0x34, 0x35, 0x73, 0x7D}


#pragma pack(1)

typedef struct {
  UINT8                                 KeyAttr;
  UINT16                                BaseCode;
  UINT16                                ShftCode;
  UINT16                                CtrlCode;
  UINT16                                AltCode;
} L05_RAW_KEY;

#pragma pack()

VOID
L05ModifyKeyCodeTableNotificationFunction (
  IN  EFI_EVENT                         Event,
  IN  VOID                              *Context
  );

EFI_STATUS
InstallModifyKeyCodeTableNotification (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  );

#endif

