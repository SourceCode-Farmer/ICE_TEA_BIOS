/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_DXE_SERVICE_BODY_H_
#define _L05_DXE_SERVICE_BODY_H_

#include <Uefi.h>
#include <L05ChipsetNameList.h>
#include <Guid/AdmiSecureBoot.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BadgingSupportLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Protocol/VariableWrite.h>
#include <Protocol/L05Service.h>

//#include "L05MediaLogo.h"
#include "L05ModifyKeyCodeTable.h"
#include "L05NovoButtonMenu.h"
//_Start_L05_GRAPHIC_UI_ENABLE
#ifdef L05_GRAPHIC_UI_ENABLE
#include "L05GraphicNovoMenu.h"
#endif
//_End_L05_GRAPHIC_UI_ENABLE
#include "L05LoadEfiDriverFromFv.h"
#ifdef L05_SECURE_SUITE_SUPPORT
#include "L05SecureSuite.h"
#endif
#ifdef L05_SMB_BIOS_ENABLE
#include "WakeOnLanFromDock.h"
#endif
#ifdef L05_BIOS_UPDATE_UI_ENABLE
#include "L05BiosUpdateUi.h"
#endif
//[-start-210903-BAIN000038-add]//
#ifdef LCFC_SUPPORT
#include "L05AcAdapterString.h"
#include <Library/H2OCpLib.h>
#endif
//[-end-210903-BAIN000038-add]//
#endif

