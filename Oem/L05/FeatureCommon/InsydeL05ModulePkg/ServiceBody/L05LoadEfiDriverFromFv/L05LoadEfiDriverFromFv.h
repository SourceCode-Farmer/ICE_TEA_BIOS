/** @file
  Load EFI driver from Firmware Volume.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_LOAD_EFI_DRIVER_FROM_FV_H_
#define _L05_LOAD_EFI_DRIVER_FROM_FV_H_

#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>            // EfiInitializeFwVolDevicepathNode
#include <Library/BaseLib.h>
#include <Library/DevicePathLib.h>      // DevicePathFromHandle, AppendDevicePathNode

//
// Consumed Protocols
//
#include <Protocol/FirmwareVolume2.h>   // EFI_FIRMWARE_VOLUME2_PROTOCOL
#include <Protocol/DevicePath.h>        // EFI_DEVICE_PATH_PROTOCOL
#include <Protocol/H2OBdsServices.h>    // H2O_BDS_SERVICES_PROTOCOL

//
// Guids
//

EFI_STATUS
L05LoadEfiDriverFromFv (
  IN  EFI_GUID                          *NameGuid
  );

#endif

