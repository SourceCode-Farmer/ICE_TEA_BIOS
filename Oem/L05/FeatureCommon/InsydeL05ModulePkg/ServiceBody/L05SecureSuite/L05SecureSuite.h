/** @file
  Secure Suite for L05 Feature

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_SECURE_SUITE_H_
#define _L05_SECURE_SUITE_H_

//
// Libraries
//
#include <Library/H2OCpLib.h>

EFI_STATUS
InstallSecureSuiteNotification (
  VOID
  );

#endif
