/** @file
  Secure Suite for L05 Feature

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "L05DxeServiceBody.h"

EFI_HANDLE                              mCpHandleReadyToBoot  = NULL;
EFI_HANDLE                              mCpHandleNoBootDevice = NULL;

/**
  Run Secure Suite App.

  @param  Event                         The Event this notify function registered to.
  @param  CpHandle                      The handle associated with a previously registered checkpoint handler.

  @retval None
**/
VOID
EFIAPI
SecureSuiteCore (
  IN  EFI_EVENT                         Event,
  IN  EFI_HANDLE                        CpHandle
  )
{
  EFI_STATUS                            Status;

  Status = EFI_SUCCESS;

  if (mCpHandleReadyToBoot != NULL) {
    H2OCpUnregisterHandler (mCpHandleReadyToBoot);
  }
  if (mCpHandleNoBootDevice != NULL) {
    H2OCpUnregisterHandler (mCpHandleNoBootDevice);
  }
  
  if (FeaturePcdGet(PcdL05SecureSuiteFlag) == FALSE) {
    return;
  }

  //
  // Clear flag
  //
  PcdSetBoolS (PcdL05SecureSuiteFlag, FALSE);

  //
  // Invoke Secure Suite App
  //
  Status = L05LoadEfiDriverFromFv (&gL05SecureSuiteAppGuid);

  return;
}

/**
  Install Secure Suite Notification.

  @param  None

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
InstallSecureSuiteNotification (
  VOID
  )
{
  EFI_STATUS                            Status;

  Status = EFI_SUCCESS;

  Status = H2OCpRegisterHandler (
             &gH2OBdsCpReadyToBootBeforeGuid,
             SecureSuiteCore,
             H2O_CP_MEDIUM_LOW,
             &mCpHandleReadyToBoot
             );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = H2OCpRegisterHandler (
             &gH2OBdsCpNoBootDeviceGuid,
             SecureSuiteCore,
             H2O_CP_MEDIUM,
             &mCpHandleNoBootDevice
             );

  return Status;
}
