//;******************************************************************************
//;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#ifndef _L05_BIOS_UPDATE_UI_H_
#define _L05_BIOS_UPDATE_UI_H_

#include "L05Config.h"
#include <Guid/H2OCp.h>
#include <Guid/EfiSystemResourceTable.h>
#include <Library/HiiLib.h>
#include <Library/VariableLib.h>
#include <Library/H2OCpLib.h>
#include <Library/OemGraphicsLib.h>
#include <Library/ProgressBarLib.h>
#include <Library/BiosUpdateUiLib.h>
#include <Protocol/FirmwareManagement.h>
#include <Protocol/H2OCapsuleUpdateProgress.h>
#include <Protocol/L05FlashFirmwareToolErrorProcess.h>

EFI_STATUS
L05InitBiosUpdateUi (
  VOID
  );

#endif

