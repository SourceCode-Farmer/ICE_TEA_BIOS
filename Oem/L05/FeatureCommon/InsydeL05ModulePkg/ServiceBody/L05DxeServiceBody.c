/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "L05DxeServiceBody.h"

EFI_L05_SERVICE_PROTOCOL                *mEfiL05ServicePtr = NULL;

EFI_STATUS
L05DxeServiceBodyEntryPoint (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  EFI_STATUS                            FunStatus;
//[-start-210903-BAIN000038-add]//
#ifdef LCFC_SUPPORT
  H2O_CP_HANDLE                         H2OBdsCpConOutAfterHandle;
#endif
//[-end-210903-BAIN000038-add]//

  Status    = EFI_SUCCESS;
  FunStatus = EFI_SUCCESS;

  mEfiL05ServicePtr = AllocateZeroPool (sizeof (EFI_L05_SERVICE_PROTOCOL));

  if (mEfiL05ServicePtr == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

//_Start_L05_GRAPHIC_UI_ENABLE
#ifdef L05_GRAPHIC_UI_ENABLE
  mEfiL05ServicePtr->L05NovoButtonMenu      = L05GraphicNovoButtonMenu;
#else
  mEfiL05ServicePtr->L05NovoButtonMenu      = L05NovoButtonMenu;
#endif
//_End_L05_GRAPHIC_UI_ENABLE
  mEfiL05ServicePtr->L05LoadEfiDriverFromFv = L05LoadEfiDriverFromFv;

  Status = gBS->InstallProtocolInterface (
                  &ImageHandle,
                  &gEfiL05ServiceProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  mEfiL05ServicePtr
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  FunStatus = InstallModifyKeyCodeTableNotification (ImageHandle, SystemTable);

  if (EFI_ERROR (FunStatus)) {
    Status = FunStatus;
  }

#ifdef L05_SECURE_SUITE_SUPPORT
  FunStatus = InstallSecureSuiteNotification ();

  if (EFI_ERROR (FunStatus)) {
    Status = FunStatus;
  }
#endif

#ifdef L05_SMB_BIOS_ENABLE
  FunStatus = UpdateWakeOnLanFromDockSetting ();

  if (EFI_ERROR (FunStatus)) {
    Status = FunStatus;
  }
#endif

#ifdef L05_BIOS_UPDATE_UI_ENABLE
  FunStatus = L05InitBiosUpdateUi ();

  if (EFI_ERROR (FunStatus)) {
    Status = FunStatus;
  }
#endif
//[-start-210903-BAIN000038-add]//
#ifdef LCFC_SUPPORT
  H2OBdsCpConOutAfterHandle = NULL;

//[-start-220415-QINGLIN0168-modify]//
#ifdef S370_SUPPORT
  Status = H2OCpRegisterHandler (
             &gH2OBdsCpDisplayBeforeGuid,
             L05AcAdapterStringCallBack,
             H2O_CP_MEDIUM,
             &H2OBdsCpConOutAfterHandle
             );
#else
  Status = H2OCpRegisterHandler (
             &gH2OBdsCpReadyToBootAfterGuid,
             L05AcAdapterStringCallBack,
             H2O_CP_MEDIUM,
             &H2OBdsCpConOutAfterHandle
             );
#endif
//[-end-220415-QINGLIN0168-modify]//

  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif
//[-end-210903-BAIN000038-add]//

  return Status;
}
