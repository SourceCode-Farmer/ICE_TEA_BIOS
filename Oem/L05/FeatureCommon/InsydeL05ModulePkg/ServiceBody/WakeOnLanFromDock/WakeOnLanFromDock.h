/** @file
  Wake On LAN for L05 Feature

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_WAKE_ON_LAN_H_
#define _L05_WAKE_ON_LAN_H_

#include <Uefi.h>
#include <SetupConfig.h>

//
// Libraries
//
#include <Library/UefiLib.h>  // EfiCreateProtocolNotifyEvent
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/FeatureLib/OemSvcNotifyEcEnableWolFromDock.h>

//
// Protocols
//
#include <Protocol/HddPasswordService.h>  // gEfiHddPasswordDialogProtocolGuid
#include <Protocol/AcpiSupport.h>  // gEfiAcpiSupportProtocolGuid

//
// Include files with function prototypes
//
#include <IndustryStandard/Acpi.h>

EFI_STATUS
UpdateWakeOnLanFromDockSetting (
  VOID
  );

#endif
