/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_DXE_NOVO_BUTTON_MENU_COMMON_H_
#define _L05_DXE_NOVO_BUTTON_MENU_COMMON_H_

#include "L05DxeServiceBody.h"
#include <Guid/Gpt.h>                   // gEfiPartTypeSystemPartGuid
#include <Library/BaseMemoryLib.h>
#include <Library/GenericBdsLib.h>      // BdsLibGetDescriptionFromBootOption ()
#include <Protocol/SimpleFileSystem.h>  // EFI_SIMPLE_FILE_SYSTEM_PROTOCOL
#include <Protocol/NovoRecovery.h>      // L05_NOVO_RECOVERY_SYSTEM_NAME

#define L05_PUSH_BUTTON_RECOVERY_PATH            L"\\EFI\\Lenovo\\Boot\\bootmgfw.efi"
#define L05_WIN10_PUSH_BUTTON_RECOVERY_PATH      L"\\EFI\\Microsoft\\Boot\\bootmgfw.efi"

BOOLEAN
IsPbrSystemRecovery (
  );

BOOLEAN
IsOkrSystemRecovery (
  );

#endif
