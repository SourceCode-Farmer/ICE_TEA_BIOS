/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_DXE_NOVO_BUTTON_MENU_H_
#define _L05_DXE_NOVO_BUTTON_MENU_H_

#include "L05DxeServiceBody.h"
#include "L05NovoButtonMenuCommon.h"
#include <Library/PrintLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/OemGraphicsLib.h>
#include <Protocol/L05Variable.h>

extern  BOOLEAN  mL05EFIEnableLEOS;

#define MAX_STRING_LENGTH                        100
#define NOVO_BUTTON_MENU_STRING_MAX_LENGTH       22

#define NOVO_BUTTON_MENU_TITLE_STRING            L"  Novo Button Menu    "
#define NOVO_BUTTON_MENU_NORMAL_STARTUP_STRING   L"  Normal Startup      "
#define NOVO_BUTTON_MENU_SETUP_STRING            L"  BIOS Setup          "
#define NOVO_BUTTON_MENU_BOOT_MANAGER_STRING     L"  Boot Menu           "
#define NOVO_BUTTON_MENU_ONEKEY_RECOVERY_STRING  L"  System Recovery     "
#define NOVO_BUTTON_MENU_UEFI_DIAGNOSTICS_STRING L"  UEFI Diagnostics    "
#define NOVO_BUTTON_MENU_HELP_STRING             L"Up and Down to change option, ENTER to select an option"

EFI_STATUS
L05NovoButtonMenu (
  IN OUT  UINTN                         *L05NovoButtonMenuSelection
  );

#endif
