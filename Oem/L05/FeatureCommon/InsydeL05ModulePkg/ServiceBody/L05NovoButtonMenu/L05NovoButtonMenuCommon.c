/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "L05NovoButtonMenuCommon.h"

/**
 Wrap original EFI_FILE_PROTOCOL.Close() call in order to decrease code length (with setting back pointer to NULL).

 @param This                            Pointer to the file protocol.
**/
VOID
ReleaseFilePointer (
  IN EFI_FILE_PROTOCOL                  **This
  )
{
  //
  // Close EFI File Protocol
  //
  if (This != NULL && *This != NULL) {

    (*This)->Close (*This);
    *This = NULL;
  }

  return ;
}

/**
  Check the folder path is exist or not.

  @param SimpleFileSystem               Pointer to the Simple File System Protocol
  @param FolderPath                     Pointer to the folder path.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval EFI_INVALID_PARAMETER         The parameters are invalid.
  @retval Others                        The folder path not found by refer OpenVolume(), Open().
**/
EFI_STATUS
CheckFolderPath (
  IN EFI_SIMPLE_FILE_SYSTEM_PROTOCOL    *SimpleFileSystem,
  IN CHAR16                             *FolderPath
  )
{
  EFI_STATUS                            Status;
  EFI_FILE_PROTOCOL                     *RootPtr;
  EFI_FILE_PROTOCOL                     *FilePtr;

  if (SimpleFileSystem == NULL || FolderPath == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status     = EFI_SUCCESS;
  RootPtr    = NULL;
  FilePtr    = NULL;

  //
  // Get the file pointer
  //
  Status = SimpleFileSystem->OpenVolume (
                               SimpleFileSystem,
                               &RootPtr
                               );

  if (!EFI_ERROR (Status)) {

    //
    // Open the folder path
    //
    Status = RootPtr->Open (
                        RootPtr,
                        &FilePtr,
                        FolderPath,
                        EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE,
                        0
                        );
  }

  //
  // Close all pointer
  //
  ReleaseFilePointer (&FilePtr);
  ReleaseFilePointer (&RootPtr);

  return Status;
}

/**
  Check if it is push button recovery environment or not.

  @param  None.

  @retval TRUE                          This is push button recovery environment.
  @retval FALSE                         This isn't push button recovery environment.
**/
BOOLEAN
IsPbrSystemRecovery (
  )
{
  EFI_STATUS                            Status;
  BOOLEAN                               FindPbrSystemRecovery;
  UINT8                                 Index;
  UINT8                                 Index2;
  UINTN                                 EspHandleCount;
  EFI_HANDLE                            *EspHandles;
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL       *SimpleFileSystem;
  CHAR16                                *OsRecovery [] = {
                                          L05_PUSH_BUTTON_RECOVERY_PATH,
                                          L05_WIN10_PUSH_BUTTON_RECOVERY_PATH
                                        };

  FindPbrSystemRecovery = FALSE;
  Index                 = 0;
  Index2                = 0;
  EspHandleCount        = 0;
  EspHandles            = NULL;
  SimpleFileSystem      = NULL;

  //
  // Find ESP Partition
  //
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiPartTypeSystemPartGuid,
                  NULL,
                  &EspHandleCount,
                  &EspHandles
                  );
  
  if(EFI_ERROR(Status)) {
    return FALSE;
  }

  //
  // For each system partition...
  //
  for (Index = 0; Index < EspHandleCount; Index++) {
    Status = gBS->HandleProtocol (
                    EspHandles[Index],
                    &gEfiSimpleFileSystemProtocolGuid,
                    (VOID **) &SimpleFileSystem
                    );
    
    if (EFI_ERROR (Status)) {
      continue;
    }

    for (Index2 = 0; Index2 < (sizeof(OsRecovery) / sizeof(CHAR16 *)); Index2++) {
      Status = CheckFolderPath (SimpleFileSystem, OsRecovery[Index2]);

      if (!EFI_ERROR (Status)) {
        FindPbrSystemRecovery = TRUE;
        break;
      }
    }

    if (FindPbrSystemRecovery) {
      break;
    }
  }

  if (EspHandles != NULL) {
    gBS->FreePool (EspHandles);
  }

  return FindPbrSystemRecovery;
}

/**
  Check if it is one key recovery environment or not.

  @param  None.

  @retval TRUE                          This is one key recovery environment.
  @retval FALSE                         This isn't one key recovery environment.
**/
BOOLEAN
IsOkrSystemRecovery (
  )
{
  UINT16                                *BootOrder;
  UINTN                                 BootOrderSize;
  UINTN                                 BootOrderCount;
  UINTN                                 Index;
  CHAR16                                *Description;
  CHAR16                                *NovoRecoveryString = L05_NOVO_RECOVERY_SYSTEM_NAME;

  BootOrder      = NULL;
  BootOrderSize  = 0;
  BootOrderCount = 0;
  Description    = NULL;

  BootOrder = BdsLibGetVariableAndSize (
                L"BootOrder",
                &gEfiGlobalVariableGuid,
                &BootOrderSize
                );

  if (BootOrder == NULL) {
    BootOrderSize = 0;
    return FALSE;
  }

  BootOrderCount = BootOrderSize / sizeof (UINT16);

  for (Index = 0; Index < BootOrderCount; Index++) {
    Description = BdsLibGetDescriptionFromBootOption (BootOrder[Index]);
    if (CompareMem (Description, NovoRecoveryString, StrSize (NovoRecoveryString)) == 0x0) {
      return TRUE;
    }
  }

  return FALSE;
}
