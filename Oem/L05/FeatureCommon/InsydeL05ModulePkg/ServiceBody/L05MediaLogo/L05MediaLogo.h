//;******************************************************************************
//;* Copyright (c) 1983-2011, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#ifndef _L05_DXE_MEDIA_LOGO_H_
#define _L05_DXE_MEDIA_LOGO_H_

//#include "Tiano.h"
//#include "EfiDriverLib.h"
//#include "OemGraphicsLib.h"
#include "L05DxeServiceBody.h"

//#include EFI_PROTOCOL_DEFINITION (StartOfBdsDiagnostics)
//#include EFI_PROTOCOL_DEFINITION (UgaSplash)
//#include EFI_PROTOCOL_DEFINITION (L05ODMService)
//#include EFI_PROTOCOL_DEFINITION (HdaControllerInit)
//#include EFI_PROTOCOL_DEFINITION (L05LogoPlayer)
//#include EFI_PROTOCOL_DEFINITION (Legacy8259)
#include <Protocol/Legacy8259.h>

VOID
L05PlayMediaLogo (
  IN  EFI_EVENT                         Event,
  IN  VOID                              *Context
  );

EFI_STATUS
InstallMediaLogoNotification (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  );

#endif

