//;******************************************************************************
//;* Copyright (c) 1983-2011, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************

#include "L05MediaLogo.h"

VOID
L05PlayMediaLogo (
  IN  EFI_EVENT                         Event,
  IN  VOID                              *Context
  )
{
  EFI_STATUS                            Status;
  EFI_HDA_CONTROLLER_INIT_PROTOCOL      *HdaControllerInit;
  EFI_L05_ODM_SERVICE_PROTOCOL          *EfiL05ODMService;
  EFI_L05_LOGO_PLAYER_PROTOCOL          *L05LogoPlayer;
  EFI_LEGACY_8259_PROTOCOL              *Legacy8259;
  UINTN                                 CodecAddress;
  UINTN                                 LogoVolume;
  UINT16                                LegMask;
  UINT16                                OldLegMask;

  Status            = EFI_SUCCESS;
  HdaControllerInit = NULL;
  EfiL05ODMService  = NULL;
  L05LogoPlayer     = NULL;
  Legacy8259        = NULL;
  CodecAddress      = 0;
  LogoVolume        = 2;
  LegMask           = 0;
  OldLegMask        = 0;

  Status = gBS->LocateProtocol (&gEfiHdaControllerInitProtocolGuid, NULL, &HdaControllerInit);

  if (EFI_ERROR (Status)) {
    return;
  }

  LogoVolume = ((EFI_L05_SETUP_OEM_AREA *) mOemFeatureToCorecodePtr->Data->SetupOemArea)->LogoVolume;

  Status = gBS->LocateProtocol (&gEfiL05ODMServiceProtocolGuid, NULL, &EfiL05ODMService);

  if (!EFI_ERROR (Status)) {
    CodecAddress = EfiL05ODMService->Data->L05CodecAddress;
  }

  //
  //  Show general logo and badging strings
  //
  Status = mOemFeatureToCorecodePtr->EnableQuietBoot (&gEfiUgaSplashProtocolGuid);

  Status = HdaControllerInit->Init (HdaControllerInit, CodecAddress, LogoVolume);
  Status = gBS->LocateProtocol (&gEfiL05LogoPlayerProtocolGuid, NULL, &L05LogoPlayer);

  if (EFI_ERROR (Status)) {
    return;
  }

  //
  //  For L05 MMLogo spec, disable power button during Media Logo playing.
  //
//  Data8 = R_SB_PM_ACPI_PM1_EVT_BLK_HI;
//  IoOutput8 (SB_PM_INDEX_PORT, Data8);
//  Data8 = IoInput8 (SB_PM_DATA_PORT);
//  PmBase = (UINT16)Data8 << 8;
//  Data8 = R_SB_PM_ACPI_PM1_EVT_BLK_LO;
//  IoOutput8 (SB_PM_INDEX_PORT, Data8);
//  Data8 = IoInput8 (SB_PM_DATA_PORT);
//  PmBase |= Data8;
//  Pm1En = IoInput16 (PmBase + R_SB_ACPI_PM1_ENABLE);
//  IoOutput16 (PmBase + R_SB_ACPI_PM1_ENABLE, Pm1En & ~(B_PWR_BTN_EN));

  //
  //  For L05 MMLogo spec, disable keyboard input during Media Logo playing.
  //    Keys pressed during Logo playing would be catched after Logo end.
  //
  Status = gBS->LocateProtocol (&gEfiLegacy8259ProtocolGuid, NULL, &Legacy8259);

  if (!EFI_ERROR (Status)) {
    Legacy8259->GetMask (Legacy8259, &LegMask, NULL, NULL, NULL);
    OldLegMask = LegMask;
    LegMask = 0xFFFE;
    Legacy8259->SetMask (Legacy8259, &LegMask, NULL, NULL, NULL);
  }

  //
  //  Show Media Logo
  //
  L05LogoPlayer->Play (L05LogoPlayer, LogoPlayStart);

  //
  //  Wait Media Logo ending. This could move to a later position.
  //
  L05LogoPlayer->WaitTillEnd (L05LogoPlayer);

  //
  //  Enable keyboard input, keys pressed during Logo playing would be catched.
  //
  if (!EFI_ERROR (Status)) {
    Legacy8259->SetMask (Legacy8259, &OldLegMask, NULL, NULL, NULL);
  }

  //
  //  Enable power button.
  //
//  IoOutput16 (PmBase + R_SB_ACPI_PM1_ENABLE, Pm1En);

  if (LogoVolume != 0) {
    //
    //  BUGBUG: Playing L05 MediaLogo could destroy Verb Table, which could cause beep sound disappear under DOS.
    //  We might need to reinitial Azalia to fix this symptom.
    //
    // ReInitialiAzalia ();
  }
}

EFI_STATUS
InstallMediaLogoNotification (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  VOID                                  *StartOfBdsDiagnosticsEventRegistration;
  EFI_EVENT                             StartOfBdsDiagnosticsEvent;

  Status                                 = EFI_SUCCESS;
  StartOfBdsDiagnosticsEventRegistration = NULL;
  StartOfBdsDiagnosticsEvent             = NULL;

  Status = gBS->CreateEvent (
                  EFI_EVENT_NOTIFY_SIGNAL,
                  EFI_TPL_CALLBACK,
                  L05PlayMediaLogo,
                  NULL,
                  &StartOfBdsDiagnosticsEvent
                  );

  if (EFI_ERROR (Status)) {
    return EFI_ABORTED;
  }

  Status = gBS->RegisterProtocolNotify (
                  &gEfiStartOfBdsDiagnosticsProtocolGuid,
                  StartOfBdsDiagnosticsEvent,
                  &StartOfBdsDiagnosticsEventRegistration
                  );

  if (EFI_ERROR (Status)) {
    return EFI_ABORTED;
  }

  return Status;
}

