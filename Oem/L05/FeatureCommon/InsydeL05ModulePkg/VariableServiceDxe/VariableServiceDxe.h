/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _EFI_VARIABLE_SERVICE_DXE_H_
#define _EFI_VARIABLE_SERVICE_DXE_H_

#include <Library/BaseLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Protocol/L05Variable.h>

//
//  Function
//
EFI_STATUS
L05GetVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN OUT  UINT32                        *DataSize,
  OUT     VOID                          *Data
  );

EFI_STATUS
L05SetVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      UINT32                        DataSize,
  IN      VOID                          *Data
  );

EFI_STATUS
L05LockVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName
  );

EFI_STATUS
L05UnlockVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      CHAR16                        *Password
  );

EFI_STATUS
L05VariableServiceDxeEntry (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  );

#endif
