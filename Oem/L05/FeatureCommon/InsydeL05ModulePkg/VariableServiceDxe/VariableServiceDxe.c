/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "VariableServiceDxe.h"

EFI_L05_VARIABLE_PROTOCOL               *mEfiL05VariablePtr = NULL;

/*++

Routine Description:

  Using runtime service GetVariable(DXE) or SMM variable protocol(SMM) to get variable.
  Variable name always be the same, we only use GUID to distingue variable.
  Simply return get variable function.

--*/
EFI_STATUS
L05GetVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN OUT  UINT32                        *DataSize,
  OUT     VOID                          *Data
  )
{
  EFI_STATUS                            Status;
  UINTN                                 DataSizeUintn;

  DataSizeUintn = *DataSize;

  Status = gRT->GetVariable (
                  EFI_L05_VARIABLE_NAME,
                  VariableGuidName,
                  NULL,
                  &DataSizeUintn,
                  Data
                  );
  *DataSize = (UINT32) DataSizeUintn;
  return Status;
}

/*++

Routine Description:

  Using runtime service SetVariable(DXE) or SMM variable protocol(SMM) to set variable.
  Variable name always be the same, we only use GUID to distingue variable.
  Simply return set variable function.

--*/
EFI_STATUS
L05SetVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      UINT32                        DataSize,
  IN      VOID                          *Data
  )
{
  UINT32                                Attribute;
  
  Attribute = EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE;

  return gRT->SetVariable (
                EFI_L05_VARIABLE_NAME,
                VariableGuidName,
                Attribute,
                (UINTN) DataSize,
                Data
                );
}

/*++

Routine Description:

  Not implemented.

--*/
EFI_STATUS
L05LockVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName
  )
{
  return EFI_UNSUPPORTED;
}

/*++

Routine Description:

  Not implemented.

--*/
EFI_STATUS
L05UnlockVariable (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      CHAR16                        *Password
  )
{
  return EFI_UNSUPPORTED;
}

EFI_STATUS
L05VariableServiceDxeEntry (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{

  EFI_STATUS                            Status;

  Status             = EFI_SUCCESS;
  mEfiL05VariablePtr = AllocateZeroPool (sizeof (EFI_L05_VARIABLE_PROTOCOL));

  if (mEfiL05VariablePtr == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  mEfiL05VariablePtr->GetVariable    = L05GetVariable;
  mEfiL05VariablePtr->SetVariable    = L05SetVariable;
  mEfiL05VariablePtr->LockVariable   = L05LockVariable;
  mEfiL05VariablePtr->UnlockVariable = L05UnlockVariable;

  Status = gBS->InstallProtocolInterface (
                  &ImageHandle,
                  &gEfiL05VariableProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  mEfiL05VariablePtr
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  return Status;
}
