/** @file
  Set Wireless Device Support Variable

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "WirelessDeviceSupportDxe.h"

L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_NAME_INFO  mWirelessDeviceNameList[] = {
  {BrandName_Broadcom, L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_NAME_BROADCOM},
  {BrandName_Intel,    L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_NAME_INTEL   },
  {BrandName_Qualcomm, L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_NAME_QUALCOMM},
  {BrandName_Realtek,  L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_NAME_REALTEK }
};

/**
  Set Wireless Device Support Variable
**/
VOID
SetWirelessDeviceSupportVariable (
  )
{
  EFI_STATUS                            Status;

  Status = EFI_TIMEOUT;

  //
  // For WLAN Device
  //
  if (mWirelessDeviceInfo[WLAN_DEVICE].State == DeviceFound            &&
      mWirelessDeviceInfo[WLAN_DEVICE].VendorNameId < InvalidBrandName &&
      mWirelessDeviceInfo[WLAN_DEVICE].SizeofSettingValue != 0x00      &&
      mWirelessDeviceInfo[WLAN_DEVICE].SettingValue != NULL) {

    Status = gRT->SetVariable (
                    mWirelessDeviceNameList[mWirelessDeviceInfo[WLAN_DEVICE].VendorNameId].VendorNameStr,
                    &gL05WirelessDeviceSupportVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    mWirelessDeviceInfo[WLAN_DEVICE].SizeofSettingValue,
                    mWirelessDeviceInfo[WLAN_DEVICE].SettingValue
                    );
  }

  //
  // For WWAN Device
  //
  if (mWirelessDeviceInfo[WWAN_DEVICE].State == DeviceFound            &&
      mWirelessDeviceInfo[WWAN_DEVICE].VendorNameId < InvalidBrandName &&
      mWirelessDeviceInfo[WWAN_DEVICE].SizeofSettingValue != 0x00      &&
      mWirelessDeviceInfo[WWAN_DEVICE].SettingValue != NULL) {

    Status = gRT->SetVariable (
                    mWirelessDeviceNameList[mWirelessDeviceInfo[WWAN_DEVICE].VendorNameId].VendorNameStr,
                    &gL05WirelessDeviceSupportVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    mWirelessDeviceInfo[WWAN_DEVICE].SizeofSettingValue,
                    mWirelessDeviceInfo[WWAN_DEVICE].SettingValue
                    );
  }

  return;
}
