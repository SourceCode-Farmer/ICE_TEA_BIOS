/** @file
  Check USB Interface to find supported wireless device.

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "WirelessDeviceSupportDxe.h"

/**
  Check USB Interface to find supported wireless device.

  @retval EFI_SUCEESS                   The operation completed successfully.
  @return Others                        Some error occurs.
**/
EFI_STATUS
CheckUsbInterface (
  )
{
  UINTN                                 BufferSize;
  EFI_HANDLE                            Handle;
  EFI_USB_IO_PROTOCOL                   *UsbIo;
  EFI_USB_DEVICE_DESCRIPTOR             DeviceDescriptor;
  EFI_STATUS                            Status;
  UINTN                                 Index;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePath;
  UINT8                                 *Ptr;
  EFI_DEV_PATH_PTR                      DevPath;
  BOOLEAN                               DevPathFinish;
  BOOLEAN                               CorrectLocation;
  UINT8                                 Device;
  UINT8                                 Function;
  UINT8                                 Port;

  UsbIo           = NULL;
  DevPathFinish   = FALSE;
  CorrectLocation = FALSE;
  Device          = 0;
  Function        = 0;
  Port            = 0;

  BufferSize = sizeof (EFI_HANDLE);

  Status = gBS->LocateHandle (
                  ByRegisterNotify,
                  NULL,
                  mUsbIoEventRegistration,
                  &BufferSize,
                  &Handle
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = gBS->HandleProtocol (
                  Handle,
                  &gEfiDevicePathProtocolGuid,
                  &DevicePath
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Ptr = (UINT8 *) DevicePath;

  while ((*(UINT32 *) Ptr != 0) && DevPathFinish != TRUE) {

    DevPath = *(EFI_DEV_PATH_PTR *) &Ptr;

    switch (DevPath.DevPath->Type) {

    //
    // Check the Usb controller
    //
    case HARDWARE_DEVICE_PATH:

      if (DevPath.DevPath->SubType == HW_PCI_DP) {
        Device = DevPath.Pci->Device;
        Function = DevPath.Pci->Function;
      }

      break;

    //
    // Check the Usb port
    //
    case MESSAGING_DEVICE_PATH:

      if (DevPath.DevPath->SubType == MSG_USB_DP) {
        Port = DevPath.Usb->ParentPortNumber;
      }

      break;

    case END_DEVICE_PATH_TYPE:

      DevPathFinish = TRUE;
      break;

    default:
      break;
    }

    Ptr += DevicePathNodeLength (DevPath.DevPath);
  }

  for (Index = 0 ; Index < EFI_L05_BIOS_LOCK_TABLE_MAX ; Index++) {
    //
    //  If this entry is the end of table, just break.
    //
    if ((mUsbDeivceLocationList[Index].Device == 0xFF) &&
        (mUsbDeivceLocationList[Index].Function == 0xFF) &&
        (mUsbDeivceLocationList[Index].Port == 0xFF)) {
      break;
    }

    if ((Device == mUsbDeivceLocationList[Index].Device) &&
        (Function == mUsbDeivceLocationList[Index].Function) &&
        (Port == mUsbDeivceLocationList[Index].Port)) {

      CorrectLocation = TRUE;
      break;
    }
  }

  if (!CorrectLocation) {
    return EFI_NOT_FOUND;
  }

  Status = gBS->HandleProtocol (
                  Handle,
                  &gEfiUsbIoProtocolGuid,
                  &UsbIo
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = UsbIo->UsbGetDeviceDescriptor (
                    UsbIo,
                    &DeviceDescriptor
                    );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Because this fucntion will execute many times when USB IO Protocol installed.
  //
  for (Index = 0 ; Index < EFI_L05_BIOS_LOCK_TABLE_MAX ; Index++) {
    //
    //  If this entry is the end of table, just break.
    //
    if (mSupportedWirelessDeviceList[Index].VendorId == 0xFFFF) {
      break;
    }


    if (mSupportedWirelessDeviceList[Index].Interface == USB_INTERFACE) {
      if (DeviceDescriptor.IdVendor  == mSupportedWirelessDeviceList[Index].VendorId) {

        switch (mSupportedWirelessDeviceList[Index].DeviceType) {

        case WLAN_DEVICE:

          mWirelessDeviceInfo[WLAN_DEVICE].State              = DeviceFound;
          mWirelessDeviceInfo[WLAN_DEVICE].VendorNameId       = mSupportedWirelessDeviceList[Index].VendorNameId;
          mWirelessDeviceInfo[WLAN_DEVICE].SizeofSettingValue = mSupportedWirelessDeviceList[Index].SizeofSettingValue;
          mWirelessDeviceInfo[WLAN_DEVICE].SettingValue       = mSupportedWirelessDeviceList[Index].SettingValue;
          break;

        case WWAN_DEVICE:

          mWirelessDeviceInfo[WWAN_DEVICE].State              = DeviceFound;
          mWirelessDeviceInfo[WWAN_DEVICE].VendorNameId       = mSupportedWirelessDeviceList[Index].VendorNameId;
          mWirelessDeviceInfo[WWAN_DEVICE].SizeofSettingValue = mSupportedWirelessDeviceList[Index].SizeofSettingValue;
          mWirelessDeviceInfo[WWAN_DEVICE].SettingValue       = mSupportedWirelessDeviceList[Index].SettingValue;
          break;
        }

        break;
      }
    }
  }

  //
  // Feature Interanl Test Sample Code
  //
//{
//  UINT8                                  Index;
//
//  for (Index = 0 ; Index < EFI_L05_BIOS_LOCK_TABLE_MAX ; Index++) {
//
//    if (mSupportedWirelessDeviceList[Index].Interface == USB_INTERFACE) {
//
//      switch (mSupportedWirelessDeviceList[Index].DeviceType) {
//
//      case WLAN_DEVICE:
//
//        mWirelessDeviceInfo[WLAN_DEVICE].State              = DeviceFound;
//        mWirelessDeviceInfo[WLAN_DEVICE].VendorNameId       = mSupportedWirelessDeviceList[Index].VendorNameId;
//        mWirelessDeviceInfo[WLAN_DEVICE].SizeofSettingValue = mSupportedWirelessDeviceList[Index].SizeofSettingValue;
//        mWirelessDeviceInfo[WLAN_DEVICE].SettingValue       = mSupportedWirelessDeviceList[Index].SettingValue;
//        break;
//
//      case WWAN_DEVICE:
//
//        mWirelessDeviceInfo[WWAN_DEVICE].State              = DeviceFound;
//        mWirelessDeviceInfo[WWAN_DEVICE].VendorNameId       = mSupportedWirelessDeviceList[Index].VendorNameId;
//        mWirelessDeviceInfo[WWAN_DEVICE].SizeofSettingValue = mSupportedWirelessDeviceList[Index].SizeofSettingValue;
//        mWirelessDeviceInfo[WWAN_DEVICE].SettingValue       = mSupportedWirelessDeviceList[Index].SettingValue;
//        break;
//      }
//
//      break;
//    }
//  }
//}

  return EFI_SUCCESS;
}
