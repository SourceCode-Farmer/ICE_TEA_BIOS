/** @file
  Wireless Device Support Feature

;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "WirelessDeviceSupportDxe.h"

EFI_L05_PCI_LOCATION                    *mPciDeivceLocationList = NULL;
EFI_L05_USB_LOCATION                    *mUsbDeivceLocationList = NULL;
EFI_L05_SUPPORTED_WIRELESS_DEVICE_INFO  *mSupportedWirelessDeviceList = NULL;

VOID                                    *mUsbIoEventRegistration = NULL;

L05_WIRELESS_DEVICE_INFO                mWirelessDeviceInfo[UnsupportDeviceType] = {
  {DeviceNotFound, 0, 0, NULL},
  {DeviceNotFound, 0, 0, NULL}
};

L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO  mWirelessDeviceInfoForSmbiosType248 = {
  L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING,
  L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING,
  L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING,
  L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING,
  L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING,
};
/**
  Notify function to Verify PCI device for supported wireless device .

  @param Event                          Event whose notification function is being invoked.
  @param Context                        Pointer to the notification function's context.
**/
VOID
EFIAPI
CheckPciInterfaceNotifyFun (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  )
{
  EFI_STATUS                                Status;
  EFI_L05_SMBIOS_INTERFACE_PROTOCOL         *L05SmbiosInterfacePtr;
  L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO  TempBuf;

  //
  // There is no necessary to trigger this notify function again in any case.
  //
  gBS->CloseEvent (Event);

  L05SmbiosInterfacePtr = NULL;

  Status = gBS->LocateProtocol (
                  &gEfiL05SmbiosInterfaceProtocolGuid,
                  NULL,
                  (VOID **) &L05SmbiosInterfacePtr
                  );

  //
  // Check PCI Interface
  //
  CheckPciInterface ();

  //
  // Force to add SMBIOS Type 133(0x85)
  //
  if (L05SmbiosInterfacePtr != NULL) {

    Status = L05SmbiosInterfacePtr->AddSmbiosType (133, NULL, 0);
  }

  //
  // The USB interface should be checked before.
  //
  if (mWirelessDeviceInfo[WLAN_DEVICE].State == DeviceFound ||
      mWirelessDeviceInfo[WWAN_DEVICE].State == DeviceFound) {

    SetWirelessDeviceSupportVariable ();
  }

  //
  // Add SMBIOS Type 248(0xF8)
  //
  if (L05SmbiosInterfacePtr != NULL) {

    ZeroMem (&TempBuf, sizeof (L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO));

    //
    // Get updated wireless device support information from Project
    //
    Status = OemSvcGetWirelessDeviceInfoForSmbiosType248 ((VOID *) &TempBuf);

    switch (Status) {

    case EFI_SUCCESS:
      //
      // SMBIOS Type 248 be added by Project, Feature will not add it twice.
      //
      break;

    case EFI_MEDIA_CHANGED:
      //
      // Add updated wireless device support information into SMBIOS Type 248
      //
      CopyMem (&mWirelessDeviceInfoForSmbiosType248, &TempBuf, sizeof (L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO));
      break;

    case EFI_UNSUPPORTED:
    default:
      //
      // Do nothing
      //
      break;
    }

    //
    // Check SMBIOS Type 248 be added by Project or not
    //
    if (EFI_ERROR (Status)) {

      Status = L05SmbiosInterfacePtr->AddSmbiosType (
                                        248,
                                        &mWirelessDeviceInfoForSmbiosType248,
                                        sizeof (L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO)
                                        );
    }
  }

  return;
}

/**
  Notify function to Verify USB device for supported wireless device .

  @param[in] Event                      Event whose notification function is being invoked.
  @param[in] Context                    Pointer to the notification function's context.
**/
VOID
EFIAPI
CheckUsbInterfaceNotifyFun (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  )
{
  //
  // Check USB Interface
  //
  CheckUsbInterface ();

  //
  // There is no necessary to trigger this notify function again in any case.
  //
  gBS->CloseEvent (Event);

  return;
}

/**
  This is the declaration of an EFI image entry point. This entry point is
  the same for UEFI Applications, UEFI OS Loaders, and UEFI Drivers including
  both device drivers and bus drivers.

  @param  ImageHandle                   The firmware allocated handle for the UEFI image.
  @param  SystemTable                   A pointer to the EFI System Table.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
WirelessDeviceSupportDxeDriverEntryPoint (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  EFI_EVENT                             BeforeReadyToBootEvent;
  VOID                                  *EventRegistration;
  EFI_EVENT                             UsbIoEvent;

  BeforeReadyToBootEvent = NULL;
  EventRegistration      = NULL;
  UsbIoEvent             = NULL;

  //
  // OEM SVC to get Wireless Device Support Table
  //
  Status = OemSvcGetWirelessDeviceSupportTable (&mPciDeivceLocationList, &mUsbDeivceLocationList, &mSupportedWirelessDeviceList);

  if (Status == EFI_SUCCESS || Status == EFI_UNSUPPORTED || Status != EFI_MEDIA_CHANGED) {
    return EFI_UNSUPPORTED;
  }

  //
  // Register L05 "Before" Ready To Boot envet to check PCI Interface
  // To Set Read only runtime variable before Variable Runtime Protection enabled
  //
  if (mPciDeivceLocationList != NULL && mSupportedWirelessDeviceList != NULL) {

    Status = gBS->CreateEvent (
                    EVT_NOTIFY_SIGNAL,
                    TPL_CALLBACK,
                    CheckPciInterfaceNotifyFun,
                    NULL,
                    &BeforeReadyToBootEvent
                    );

    if (!EFI_ERROR (Status)) {

      Status = gBS->RegisterProtocolNotify (
                      &gEfiL05BeforeReadyToBootProtocolGuid,
                      BeforeReadyToBootEvent,
                      &EventRegistration
                      );
    }
  }

  //
  // Register Usb Io Protocol envet to check USB Interface
  //
  if (mUsbDeivceLocationList != NULL && mSupportedWirelessDeviceList != NULL) {

    Status = gBS->CreateEvent (
                    EVT_NOTIFY_SIGNAL,
                    TPL_CALLBACK,
                    CheckUsbInterfaceNotifyFun,
                    NULL,
                    &UsbIoEvent
                    );

    if (!EFI_ERROR (Status)) {

      Status = gBS->RegisterProtocolNotify (
                      &gEfiUsbIoProtocolGuid,
                      UsbIoEvent,
                      &mUsbIoEventRegistration
                      );
    }
  }

  return Status;
}
