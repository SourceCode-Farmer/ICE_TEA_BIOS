/** @file
  Set Wireless Device Support Variable

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SET_WIRELESS_DEVICE_SUPPORT_VARIABLE_H_
#define _SET_WIRELESS_DEVICE_SUPPORT_VARIABLE_H_

VOID
SetWirelessDeviceSupportVariable (
  );
#endif
