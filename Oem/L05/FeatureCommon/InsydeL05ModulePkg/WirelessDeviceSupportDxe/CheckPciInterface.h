/** @file
  Check PCI Interface to find supported wireless device.

;******************************************************************************
;* Copyright (c) 2012 - 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CHECK_PCI_INTERFACE_H_
#define _CHECK_PCI_INTERFACE_H_

EFI_STATUS
CheckPciInterface (
  );
#endif
