/** @file
  Check PCI Interface to find supported wireless device.

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "WirelessDeviceSupportDxe.h"

//[-start-210702-Dongxu0007-add]//
#ifdef LCFC_SUPPORT
#include <L05Config.h>
#include <Library/PciExpressLib.h>
#include <Protocol/LenovoVariable.h>

//[-start-210721-QINGLIN0001-modify]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//[-start-210817-DABING0002-modify]//
//[-start-210914-DABING0006-modify]//
//[-start-211122-OWENWU0024-modify]// 
//[-start-211123-Dennis0009-modify]// 
//[-start-220307-OWENWU0041-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
UINT8                                   mCNIntelSettingValue[] = {
                                          0x00, 0x00, 0x43, 0x4E 
                                        };
UINT8                                   mNotIntelSettingValue[] = {
                                          0x00, 0x00, 0x00, 0x00
                                        };
#elif defined(S570_SUPPORT) 
UINT8                                   mCNIntelSettingValue[] = {
                                          0x00, 0x00, 0x43, 0x4E
                                        };

UINT8                                   mNotIntelSettingValue[] = {
                                          0x88, 0x7C, 0x00, 0x00
                                        };
#elif defined(S370_SUPPORT)
UINT8                                   mCNIntelSettingValue[] = {
                                          0x88, 0x7C, 0x43, 0x4E
                                        };

UINT8                                   mNotIntelSettingValue[] = {
                                          0x88, 0x7C, 0x00, 0x00
                                        };
#endif
//[-end-220307-OWENWU0041-modify]//
//[-end-211123-Dennis0009-modify]// 
//[-end-211122-OWENWU0024-modify]// 
//[-end-210914-DABING0006-modify]//
//[-end-210817-DABING0002-modify]//
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210721-QINGLIN0001-modify]//

EFI_L05_SUPPORTED_WIRELESS_DEVICE_INFO  mIntelSupportedDeviceList []= {
                                          { // Intel CN Setting
                                            WLAN_DEVICE,                     // (UINT8)  DeviceType
                                            PCI_INTERFACE,                   // (UINT8)  Interface
                                            0x8086,                          // (UINT16) VendorId
                                            BrandName_Intel,                 // (UINT8)  VendorNameId
                                            sizeof (mCNIntelSettingValue),   // (UINTN)  SizeofSettingValue
                                            (VOID *) mCNIntelSettingValue    // (VOID *) SettingValue
                                          },
                                          { // Intel NOT CN Setting
                                            WLAN_DEVICE,                     // (UINT8)  DeviceType
                                            PCI_INTERFACE,                   // (UINT8)  Interface
                                            0x8086,                          // (UINT16) VendorId
                                            BrandName_Intel,                 // (UINT8)  VendorNameId
                                            sizeof (mNotIntelSettingValue),  // (UINTN)  SizeofSettingValue
                                            (VOID *) mNotIntelSettingValue   // (VOID *) SettingValue
                                          },
                                          {0xFF, 0xFF, 0xFFFF, 0xFF, 0, NULL}
                                        };

//just for Intel Wireless Device ,need correct MTM for SAR/CN function
//MTM : xxxxxxxxCP/xxxxxxxxCD/xxxxxxxxCH : Intel CN Setting
//MTM : NULL or other  : Intel NOT CN Setting
EFI_STATUS
LfcSetIntelWirelessDeviceInfo (
  UINT32                                  DeviceVenId,		
  L05_WIRELESS_DEVICE_INFO                *pWirelessDeviceInfo,
  EFI_L05_SUPPORTED_WIRELESS_DEVICE_INFO  *pSupportedWirelessDeviceList
)
{
  EFI_STATUS                              Status;
  LENOVO_VARIABLE_PROTOCOL                *LenovoVariable   = NULL;
  EFI_GUID                                MtmNumberGuid     = LVAR_MTM_NUMBER_GUID;
  UINT32                                  MtmDataSize       = L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH;
  UINT8                                   MtmBufferPtr[L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH];
  UINT8                                   Index             = 0;
  CHAR8                                   *mCountry[] = {
                                            "CP","CD","CH",
                                            NULL
                                          };
  if ((DeviceVenId & 0xFFFF) != 0x8086) {
    return EFI_UNSUPPORTED;
  }
  Status = gBS->LocateProtocol (
                  &gLenovoVariableProtocolGuid, 
                  NULL, 
                  &LenovoVariable
                  );
  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }
  Status = LenovoVariable->GetVariable (
                             LenovoVariable, 
                             &MtmNumberGuid, 
                             &MtmDataSize, 
                             MtmBufferPtr
                             );
  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }
  while (mCountry[Index] != NULL) {

    if (MtmBufferPtr[8] == *mCountry[Index] && MtmBufferPtr[9] == *(mCountry[Index] + 1)) {
      (pWirelessDeviceInfo + WLAN_DEVICE)->State              = DeviceFound;
      (pWirelessDeviceInfo + WLAN_DEVICE)->VendorNameId       = (pSupportedWirelessDeviceList + 0)->VendorNameId;
      (pWirelessDeviceInfo + WLAN_DEVICE)->SizeofSettingValue = (pSupportedWirelessDeviceList + 0)->SizeofSettingValue;
      (pWirelessDeviceInfo + WLAN_DEVICE)->SettingValue       = (pSupportedWirelessDeviceList + 0)->SettingValue;
      break;
    }
    Index ++;
  }
  if (mCountry[Index] == NULL) {
    (pWirelessDeviceInfo + WLAN_DEVICE)->State              = DeviceFound;
    (pWirelessDeviceInfo + WLAN_DEVICE)->VendorNameId       = (pSupportedWirelessDeviceList + 1)->VendorNameId;
    (pWirelessDeviceInfo + WLAN_DEVICE)->SizeofSettingValue = (pSupportedWirelessDeviceList + 1)->SizeofSettingValue;
    (pWirelessDeviceInfo + WLAN_DEVICE)->SettingValue       = (pSupportedWirelessDeviceList + 1)->SettingValue;
  }
  return EFI_SUCCESS;
}
#endif
//[-end-210702-Dongxu0007-add]//

/**
  Check PCI Interface to find supported wireless device.

  @retval EFI_SUCEESS                   The operation completed successfully.
  @return Others                        Some error occurs.
**/
EFI_STATUS
CheckPciInterface (
  )
{
  EFI_STATUS                            Status;
  EFI_PCI_ROOT_BRIDGE_IO_PROTOCOL       *PciRootBridgeIo;
  UINT32                                DeviceVenId;
  UINT32                                SubSysVenId;
  UINT16                                ClassCode;
  UINT8                                 SecondaryBusNumber;
  UINT8                                 Index;
  UINT8                                 RootPortIndex;

  PciRootBridgeIo = NULL;

  Status = gBS->LocateProtocol (&gEfiPciRootBridgeIoProtocolGuid, NULL, &PciRootBridgeIo);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  for (RootPortIndex = 0 ; RootPortIndex < EFI_L05_BIOS_LOCK_TABLE_MAX ; RootPortIndex++) {

    //
    // If this entry is the end of table, just break.
    //
    if ((mPciDeivceLocationList[RootPortIndex].Bus      == 0xFF) &&
        (mPciDeivceLocationList[RootPortIndex].Device   == 0xFF) &&
        (mPciDeivceLocationList[RootPortIndex].Function == 0xFF)) {
      break;
    }

//[-start-210702-Dongxu0007-add]//
#ifdef LCFC_SUPPORT
    //Get ClassCode
    ClassCode =0;
    ClassCode = PciExpressRead16 (PCI_EXPRESS_LIB_ADDRESS (
                                  mPciDeivceLocationList[RootPortIndex].Bus,\
                                  mPciDeivceLocationList[RootPortIndex].Device,\
                                  mPciDeivceLocationList[RootPortIndex].Function,\
                                  PCI_CLASSCODE_OFFSET+1
                                  ));
    
    if(ClassCode == (UINT16)((PCI_CLASS_NETWORK << 8) + PCI_CLASS_NETWORK_OTHER)){
      DeviceVenId = 0;
      DeviceVenId = PciExpressRead16 (PCI_EXPRESS_LIB_ADDRESS(
                                      mPciDeivceLocationList[RootPortIndex].Bus,\
                                      mPciDeivceLocationList[RootPortIndex].Device,\
                                      mPciDeivceLocationList[RootPortIndex].Function,\
                                      PCI_VENDOR_ID_OFFSET
                                      ));

      Status =LfcSetIntelWirelessDeviceInfo(DeviceVenId,mWirelessDeviceInfo,mIntelSupportedDeviceList);
      if(!EFI_ERROR(Status)){
        break;
      }
    }
#endif
//[-end-210702-Dongxu0007-add]//


    //
    // Get sub bus number of Pci Express Root prot X. (X = 0, 1, 2, etc)
    //

    //
    // Use the first sub bus number.
    //
    SecondaryBusNumber = 0x00;
    PciRootBridgeIo->Pci.Read (
                           PciRootBridgeIo,
                           EfiPciWidthUint8,
                           EFI_PCI_ADDRESS (mPciDeivceLocationList[RootPortIndex].Bus, \
                                            mPciDeivceLocationList[RootPortIndex].Device, \
                                            mPciDeivceLocationList[RootPortIndex].Function, \
                                            PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET),
                           1,
                           &SecondaryBusNumber
                           );

    if (SecondaryBusNumber == 0xFF) {
      continue;
    }

    DeviceVenId = 0;
    PciRootBridgeIo->Pci.Read (
                           PciRootBridgeIo,
                           EfiPciWidthUint32,
                           EFI_PCI_ADDRESS (SecondaryBusNumber, WIRELESS_DEV_NUM, WIRELESS_FUN_NUM, PCI_VENDOR_ID_OFFSET),
                           1,
                           &DeviceVenId
                           );

    if (DeviceVenId == 0xFFFFFFFF) {
      continue;
    }

    ClassCode = 0;
    PciRootBridgeIo->Pci.Read (
                           PciRootBridgeIo,
                           EfiPciWidthUint16,
                           EFI_PCI_ADDRESS (SecondaryBusNumber, WIRELESS_DEV_NUM, WIRELESS_FUN_NUM, PCI_CLASSCODE_OFFSET + 1),
                           1,
                           &ClassCode
                           );

    if (ClassCode != (UINT16) ((PCI_CLASS_NETWORK << 8) + PCI_CLASS_NETWORK_OTHER)) {
      //
      // Check Wireless device only.
      //
      continue;
    }

    SubSysVenId = 0;
    PciRootBridgeIo->Pci.Read (
                           PciRootBridgeIo,
                           EfiPciWidthUint32,
                           EFI_PCI_ADDRESS (SecondaryBusNumber, WIRELESS_DEV_NUM, WIRELESS_FUN_NUM, PCI_SUBSYSTEM_VENDOR_ID_OFFSET),
                           1,
                           &SubSysVenId
                           );

    for (Index = 0 ; Index < EFI_L05_BIOS_LOCK_TABLE_MAX ; Index++) {

      //
      // If this entry is the end of table, just break.
      //
      if ((mSupportedWirelessDeviceList[Index].VendorId    == 0xFFFF)) {
        break;
      }

      if (mSupportedWirelessDeviceList[Index].Interface == PCI_INTERFACE) {

        if (((DeviceVenId & 0xFFFF)  == mSupportedWirelessDeviceList[Index].VendorId)) {

          switch (mSupportedWirelessDeviceList[Index].DeviceType) {

          case WLAN_DEVICE:

            mWirelessDeviceInfo[WLAN_DEVICE].State              = DeviceFound;
            mWirelessDeviceInfo[WLAN_DEVICE].VendorNameId       = mSupportedWirelessDeviceList[Index].VendorNameId;
            mWirelessDeviceInfo[WLAN_DEVICE].SizeofSettingValue = mSupportedWirelessDeviceList[Index].SizeofSettingValue;
            mWirelessDeviceInfo[WLAN_DEVICE].SettingValue       = mSupportedWirelessDeviceList[Index].SettingValue;
            break;

          case WWAN_DEVICE:

            mWirelessDeviceInfo[WWAN_DEVICE].State              = DeviceFound;
            mWirelessDeviceInfo[WWAN_DEVICE].VendorNameId       = mSupportedWirelessDeviceList[Index].VendorNameId;
            mWirelessDeviceInfo[WWAN_DEVICE].SizeofSettingValue = mSupportedWirelessDeviceList[Index].SizeofSettingValue;
            mWirelessDeviceInfo[WWAN_DEVICE].SettingValue       = mSupportedWirelessDeviceList[Index].SettingValue;
            break;
          }

          break;
        }
      }
    }
  }

  //
  // Feature Interanl Test Sample Code
  //
//{
//  UINT8                                  Index;
//
//  for (Index = 0 ; Index < EFI_L05_BIOS_LOCK_TABLE_MAX ; Index++) {
//
//    if (mSupportedWirelessDeviceList[Index].Interface == PCI_INTERFACE) {
//
//
//      switch (mSupportedWirelessDeviceList[Index].DeviceType) {
//
//      case WLAN_DEVICE:
//        mWirelessDeviceInfo[WLAN_DEVICE].State              = DeviceFound;
//        mWirelessDeviceInfo[WLAN_DEVICE].VendorNameId       = mSupportedWirelessDeviceList[Index].VendorNameId;
//        mWirelessDeviceInfo[WLAN_DEVICE].SizeofSettingValue = mSupportedWirelessDeviceList[Index].SizeofSettingValue;
//        mWirelessDeviceInfo[WLAN_DEVICE].SettingValue       = mSupportedWirelessDeviceList[Index].SettingValue;
//        break;
//
//      case WWAN_DEVICE:
//        mWirelessDeviceInfo[WWAN_DEVICE].State              = DeviceFound;
//        mWirelessDeviceInfo[WWAN_DEVICE].VendorNameId       = mSupportedWirelessDeviceList[Index].VendorNameId;
//        mWirelessDeviceInfo[WWAN_DEVICE].SizeofSettingValue = mSupportedWirelessDeviceList[Index].SizeofSettingValue;
//        mWirelessDeviceInfo[WWAN_DEVICE].SettingValue       = mSupportedWirelessDeviceList[Index].SettingValue;
//        break;
//      }
//
//      break;
//    }
//  }
//}

  return EFI_SUCCESS;
}
