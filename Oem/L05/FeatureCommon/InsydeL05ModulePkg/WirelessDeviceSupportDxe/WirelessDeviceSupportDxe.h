/** @file
  Wireless Device Support Feature

;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _WIRELESS_DEVICE_SUPPORT_DXE_H_
#define _WIRELESS_DEVICE_SUPPORT_DXE_H_

#include <Uefi.h>
#include <IndustryStandard/Pci.h>
#include <L05WirelessDeviceSupportConfig.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DevicePathLib.h>
#include <Library/FeatureLib/OemSvcGetWirelessDeviceSupportTable.h>
#include <Library/FeatureLib/OemSvcGetWirelessDeviceInfoForSmbiosType248.h>

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//
#include <Protocol/L05BeforeReadyToBoot.h>
#include <Protocol/PciRootBridgeIo.h>
#include <Protocol/UsbIo.h>
#include <Protocol/L05SmbiosInterface.h>

//
//
// Produced Protocols
//

//
// Guids
//
#include <Guid/L05WirelessDeviceSupportVariable.h>

//
// Driver Version
//

//
// Protocol instances
//

//
// Include files with function prototypes
//
#include "CheckPciInterface.h"
#include "CheckUsbInterface.h"
#include "SetWirelessDeviceSupportVariable.h"

#define WIRELESS_DEV_NUM                0
#define WIRELESS_FUN_NUM                0

//
// Definition of wireless device state
//
typedef enum {
  DeviceNotFound = 0,
  DeviceFound,
  InvalidState
} L05_WIRELESS_DEVICE_STATE;

//
// Structure of wireless device information
//
typedef struct {
  UINT8                                 State;
  UINT8                                 VendorNameId;
  UINTN                                 SizeofSettingValue;
  VOID                                  *SettingValue;
} L05_WIRELESS_DEVICE_INFO;

//
// Structure of wireless device ID and Name
//
typedef struct {
  UINT8                                 VendorNameId;
  CHAR16                                *VendorNameStr;
} L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_NAME_INFO;

extern EFI_L05_PCI_LOCATION                    *mPciDeivceLocationList;
extern EFI_L05_USB_LOCATION                    *mUsbDeivceLocationList;
extern EFI_L05_SUPPORTED_WIRELESS_DEVICE_INFO  *mSupportedWirelessDeviceList;
extern VOID                                    *mUsbIoEventRegistration;
extern L05_WIRELESS_DEVICE_INFO                mWirelessDeviceInfo[];

#endif
