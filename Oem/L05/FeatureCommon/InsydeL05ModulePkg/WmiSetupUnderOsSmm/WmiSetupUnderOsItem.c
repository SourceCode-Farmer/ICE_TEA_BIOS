/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "WmiSetupUnderOsSmm.h"

//
// Select String List
//
CHAR8 *                                 mEnableDisableStrList[] = {
  "Enable", "Disable"
};

#ifdef L05_SHOW_RAID_OPTION_ENABLE
CHAR8 *                                 mSATAControllerStrList[] = {
  "AHCI", "RAID"
};

#else
CHAR8 *                                 mSATAControllerStrList[] = {
  "AHCI"
};
#endif

CHAR8 *                                 mGraphicsDeviceStrList[] = {
  "Switchable Graphics", "UMA Graphics"
};

CHAR8 *                                 mIntelSgxStrList[] = {
  "Software Controlled", "Enable", "Disable"
};

CHAR8 *                                 mPlatformModeStrList[] = {
  "User Mode", "Setup Mode"
};

CHAR8 *                                 mSecureBootModeStrList[] = {
  "Standard", "Custom"
};

CHAR8 *                                 mBootModeStrList[] = {
  "UEFI", "Legacy Support"
};

CHAR8 *                                 mBootPriorityStrList[] = {
  "UEFI First", "Legacy First"
};

//
// BIOS Config Enumerate Map
//
WMI_BIOS_CONFIG_DATA_MAP                mWmiBiosConfigEnumMap[] = {
// BiosConfigItemName,                       ConfigValue, ScuAccessLevel, ConfigDataType,           L05WmiSetupItemValid,                           L05WmiSetupItem
  //
  // Configuration Page
  //
#ifdef L05_NOTEBOOK_CLOUD_BOOT_ENABLE
  {"LenovoCloudServices",                   NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05LenovoCloudServicesValid,        &mL05WmiSetupItem.L05LenovoCloudServices       },
#endif
#ifdef L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE
  {"WiFiLenovoCloudServices",               NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05UefiWifiNetworkBootValid,        &mL05WmiSetupItem.L05UefiWifiNetworkBoot       },
#endif
  {"USBLegacy",                             NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05UsbLegacyValid,                  &mL05WmiSetupItem.L05UsbLegacy                 },
  {"WirelessLAN",                           NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05WirelessLanValid,                &mL05WmiSetupItem.L05WirelessLan               },
  {"SATAControllerMode",                    NULL,        UserReadOnly,   SATAControllerType,       &mL05WmiSetupItem.L05SataControllerModeValid,         &mL05WmiSetupItem.L05SataControllerMode        },
  {"IntelVMDController",                    NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05VmdControllerValid,              &mL05WmiSetupItem.L05VmdController             },
  {"GraphicsDevice",                        NULL,        UserReadOnly,   GraphicsDeviceType,       &mL05WmiSetupItem.L05GraphicsDeviceValid,             &mL05WmiSetupItem.L05GraphicsDevice            },
  {"Intel(R)VirtualizationTechnology",      NULL,        UserReadOnly,   IntelVtType,              &mL05WmiSetupItem.L05IntelVtValid,                    &mL05WmiSetupItem.L05IntelVt                   },
  {"Intel(R)VT-dFeature",                   NULL,        UserReadOnly,   IntelVtdType,             &mL05WmiSetupItem.L05IntelVtdValid,                   &mL05WmiSetupItem.L05IntelVtd                  },
  {"AMDV(TM)Technology",                    NULL,        UserReadOnly,   AmdVType,                 &mL05WmiSetupItem.L05AmdVTechnologyValid,             &mL05WmiSetupItem.L05AmdVTechnology            },
  {"Intel(R)Hyper-ThreadingTechnology",     NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05IntelHyperThreadingValid,        &mL05WmiSetupItem.L05IntelHyperThreading       },
  {"BIOSBackFlash",                         NULL,        UserReadOnly,   BiosBackFlashType,        &mL05WmiSetupItem.L05BiosBackFlashValid,              &mL05WmiSetupItem.L05BiosBackFlash             },
  {"HotkeyMode",                            NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05HotKeyModeValid,                 &mL05WmiSetupItem.L05HotKeyMode                },
  {"FoolProofFnCtrl",                       NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05FoolProofFnCtrlValid,            &mL05WmiSetupItem.L05FoolProofFnCtrl           },
  {"AlwaysOnUSB",                           NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05AlwaysOnUsbValid,                &mL05WmiSetupItem.L05AlwaysOnUsb               },
  {"ChargeInBatteryMode",                   NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05ChargeInBatteryModeValid,        &mL05WmiSetupItem.L05ChargeInBatteryMode       },
  {"RestoreDefaultOverclocking",            NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05RestoreDefaultOverclockingValid, &mL05WmiSetupItem.L05RestoreDefaultOverclocking},
  {"WakeOnVoice",                           NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05WakeOnVoiceValid,                &mL05WmiSetupItem.L05WakeOnVoice               },
  {"UltraQuietMode",                        NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05UltraQuietModeValid,             &mL05WmiSetupItem.L05UltraQuietMode            },

  //
  // Security Page
  //
  {"PowerOnPassword",                       NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05PowerOnPasswordValid,            &mL05WmiSetupItem.L05PowerOnPassword            },
//  {"ClearUserPassword",                     NULL,        UserReadOnly,   ClearUserPasswordType,    &mL05WmiSetupItem.L05ClearUserPasswordValid,          &mL05WmiSetupItem.L05ClearUserPassword          },
  {"IntelPlatformTrustTechnology",          NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05IntelPttValid,                   &mL05WmiSetupItem.L05IntelPtt                   },
//  {"ClearIntelPTTKey",                      NULL,        UserReadOnly,   ClearIntelPttKeyType,     &mL05WmiSetupItem.L05ClearIntelPttKeyValid,           &mL05WmiSetupItem.L05ClearIntelPttKey           },
  {"AMDPlatformSecurityProcessor",          NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05AmdPspValid,                     &mL05WmiSetupItem.L05AmdPsp                     },
//  {"ClearAMDPSPKey",                        NULL,        UserReadOnly,   ClearAmdPspKeyType,       &mL05WmiSetupItem.L05ClearAmdPspKeyValid,             &mL05WmiSetupItem.L05ClearAmdPspKey             },
  {"SecurityChip",                          NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05SecurityChipValid,               &mL05WmiSetupItem.L05SecurityChip               },
//  {"ClearSecurityChipKey",                  NULL,        UserReadOnly,   ClearSecurityChipKeyType, &mL05WmiSetupItem.L05ClearSecurityChipKeyValid,       &mL05WmiSetupItem.L05ClearSecurityChipKey       },
  {"DeviceGuard",                           NULL,        UserReadOnly,   DeviceGuardType,          &mL05WmiSetupItem.L05DeviceGuardValid,                &mL05WmiSetupItem.L05DeviceGuard                },
  //
  // I/O Port Access Submenu
  //
#ifdef L05_SMB_BIOS_ENABLE
  {"EthernetLAN",                           NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05EthernetLanValid,                &mL05WmiSetupItem.L05EthernetLan                },
  {"WirelessWAN",                           NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05WirelessWanValid,                &mL05WmiSetupItem.L05WirelessWan                },
  {"Bluetooth",                             NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05BluetoothValid,                  &mL05WmiSetupItem.L05Bluetooth                  },
  {"USBPort",                               NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05UsbPortValid,                    &mL05WmiSetupItem.L05UsbPort                    },
  {"MemoryCardSlot",                        NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05MemoryCardSlotValid,             &mL05WmiSetupItem.L05MemoryCardSlot             },
  {"SmartCardSlot",                         NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05SmartCardSlotValid,              &mL05WmiSetupItem.L05SmartCardSlot              },
  {"IntegratedCamera",                      NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05IntegratedCameraValid,           &mL05WmiSetupItem.L05IntegratedCamera           },
  {"Microphone",                            NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05MicrophoneValid,                 &mL05WmiSetupItem.L05Microphone                 },
  {"FingerprintReader",                     NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05FingerprintReaderValid,          &mL05WmiSetupItem.L05FingerprintReader          },
  {"Thunderbolt(TM)",                       NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05ThunderboltValid,                &mL05WmiSetupItem.L05Thunderbolt                },
  {"NFCDevice",                             NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05NfcDeviceValid,                  &mL05WmiSetupItem.L05NfcDevice                  },
#endif
  //
  // Intel (R) SGX Submenu
  //
  {"IntelSGX",                              NULL,        UserReadOnly,   IntelSgxType,             &mL05WmiSetupItem.L05IntelSgxValid,                   &mL05WmiSetupItem.L05IntelSgx                   },
  //
  // Security Page
  //
  {"SecureBoot",                            NULL,        UserReadOnly,   SecureBootType,           &mL05WmiSetupItem.L05SecureBootValid,                 &mL05WmiSetupItem.L05SecureBoot                 },
//  {"SecureBootStatus",                      NULL,        ReadOnly,       EnableDisableType,        &mL05WmiSetupItem.L05SecureBootStatusValid,           &mL05WmiSetupItem.L05SecureBootStatus           },
//  {"PlatformMode",                          NULL,        ReadOnly,       PlatformModeType,         &mL05WmiSetupItem.L05PlatformModeValid,               &mL05WmiSetupItem.L05PlatformMode               },
//  {"SecureBootMode",                        NULL,        ReadOnly,       SecureBootModeType,       &mL05WmiSetupItem.L05SecureBootModeValid,             &mL05WmiSetupItem.L05SecureBootMode             },
//  {"ResetToSetupMode",                      NULL,        UserReadOnly,   ResetToSetupModeType,     &mL05WmiSetupItem.L05ResetToSetupModeValid,           &mL05WmiSetupItem.L05ResetToSetupMode           },
//  {"RestoreFactoryKeys",                    NULL,        UserReadOnly,   RestoreFactoryKeysType,   &mL05WmiSetupItem.L05RestoreFactoryKeysValid,         &mL05WmiSetupItem.L05RestoreFactoryKeys         },

  //
  // Boot Page
  //
#ifdef L05_LEGACY_SUPPORT
  {"BootMode",                              NULL,        UserReadOnly,   BootModeType,             &mL05WmiSetupItem.L05BootModeValid,                   &mL05WmiSetupItem.L05BootMode                   },
#endif
  {"BootPriority",                          NULL,        UserReadOnly,   BootPriorityType,         &mL05WmiSetupItem.L05BootPriorityValid,               &mL05WmiSetupItem.L05BootPriority               },
  {"FastBoot",                              NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05FastBootValid,                   &mL05WmiSetupItem.L05FastBoot                   },
  {"USBBoot",                               NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05UsbBootValid,                    &mL05WmiSetupItem.L05UsbBoot                    },
  {"PXEBootToLAN",                          NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05PxeBootToLanValid,               &mL05WmiSetupItem.L05PxeBootToLan               },
  {"IPV4PXEFirst",                          NULL,        UserReadOnly,   EnableDisableType,        &mL05WmiSetupItem.L05PxeIpv4FirstValid,               &mL05WmiSetupItem.L05PxeIpv4First               },
  {"EFI-BootOrder",                         NULL,        UserReadOnly,   EfiBootOrderType,         &mL05WmiSetupItem.L05EfiBootOrderValid,               NULL                                            },
  {"Legacy-BootOrder",                      NULL,        UserReadOnly,   LegacyBootOrderType,      &mL05WmiSetupItem.L05LegacyBootOrderValid,            NULL                                            },

  //
  // Exit Page
  //
//#ifdef L05_LEGACY_SUPPORT
//  {"OSOptimizedDefaults",                   NULL,        UserReadOnly,   OsOptimizedDefaultsType,  &mL05WmiSetupItem.L05OsOptimizedDefaultValid,         &mL05WmiSetupItem.L05OsOptimizedDefault         },
//#endif
};

//
// Select String List Map
//
WMI_SELECT_STR_LIST_MAP                 mWmiSelectStrListMap[] = {
// ConfigDataType,          SelectStrList,          SelectStrCount
  {EnableDisableType,       mEnableDisableStrList,  (sizeof (mEnableDisableStrList)  / sizeof (CHAR8 *))},
  {SATAControllerType,      mSATAControllerStrList, (sizeof (mSATAControllerStrList) / sizeof (CHAR8 *))},
  {GraphicsDeviceType,      mGraphicsDeviceStrList, (sizeof (mGraphicsDeviceStrList) / sizeof (CHAR8 *))},
  {IntelVtType,             mEnableDisableStrList,  (sizeof (mEnableDisableStrList)  / sizeof (CHAR8 *))},
  {IntelVtdType,            mEnableDisableStrList,  (sizeof (mEnableDisableStrList)  / sizeof (CHAR8 *))},
  {AmdVType,                mEnableDisableStrList,  (sizeof (mEnableDisableStrList)  / sizeof (CHAR8 *))},
  {BiosBackFlashType,       mEnableDisableStrList,  (sizeof (mEnableDisableStrList)  / sizeof (CHAR8 *))},
  {DeviceGuardType,         mEnableDisableStrList,  (sizeof (mEnableDisableStrList)  / sizeof (CHAR8 *))},
  {IntelSgxType,            mIntelSgxStrList,       (sizeof (mIntelSgxStrList)       / sizeof (CHAR8 *))},
  {SecureBootType,          mEnableDisableStrList,  (sizeof (mEnableDisableStrList)  / sizeof (CHAR8 *))},
  {PlatformModeType,        mPlatformModeStrList,   (sizeof (mPlatformModeStrList)   / sizeof (CHAR8 *))},
  {SecureBootModeType,      mSecureBootModeStrList, (sizeof (mSecureBootModeStrList) / sizeof (CHAR8 *))},
  {BootModeType,            mBootModeStrList,       (sizeof (mBootModeStrList)       / sizeof (CHAR8 *))},
  {BootPriorityType,        mBootPriorityStrList,   (sizeof (mBootPriorityStrList)   / sizeof (CHAR8 *))},
  {OsOptimizedDefaultsType, mEnableDisableStrList,  (sizeof (mEnableDisableStrList)  / sizeof (CHAR8 *))},
};

//
// Map List Count
//
UINT16                                  mWmiBiosConfigEnumMapListCount = sizeof (mWmiBiosConfigEnumMap) / sizeof (WMI_BIOS_CONFIG_DATA_MAP);
UINT16                                  mWmiSelectStrListMapListCount  = sizeof (mWmiSelectStrListMap)  / sizeof (WMI_SELECT_STR_LIST_MAP);

