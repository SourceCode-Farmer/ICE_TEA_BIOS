/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <L05WmiSetupUnderOsDefinition.h>
#include <OemSwSmi.h>  // EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI

#define SOFTWARE_SMI_PORT               SSMP

DefinitionBlock (
  "Wmi.aml",
  "SSDT",
  1,
  "Insyde",
  "WmiTable",
  0x1000
  )
{
//  External (P8XH, MethodObj)
//  External (MBGS, MethodObj)
//  External (DB2H, MethodObj)
//  External (DW2H, MethodObj)
//  External (DD2H, MethodObj)
  
  Scope(\_SB)
  {
    Mutex(MUTW, 0)
    
    //
    // WMI-to-ACPI mapper device.
    //
    Device(WMI1)
    {
      // PNP0C14 is Plug and Play ID assigned to WMI mapper
      Name(_HID, EISAID("PNP0C14"))
      Name(_UID, "ISET") // _UID follow Lenovo sample code to set "ISET"
      
      //
      // _WDG evaluates to a data structure that specifies the data
      // blocks supported by the ACPI device.
      //
      Name(_WDG, Buffer()
      {
        // -------- Method execution for Lenovo_BiosSetting : Lenovo_BiosElement
        0x0E, 0x23, 0xF5, 0x51, 0x77, 0x96, 0xCD, 0x46, 0xA1, 0xCF,
        0xC0, 0xB2, 0x3E, 0xE3, 0x4D, 0xB7, 
        0x41, 0x30, // Object ID (A0)
        0,          // Instance Count, Automatic count the Instance Count by UpdateWdgBiosSettingA0InstanceCount()
        0x05,       // Flags WMIACPI_REGFLAG_EXPENSIVE & STRING
        0x64, 0x9A, 0x47, 0x98, 0xF5, 0x33, 0x33, 0x4E, 0xA7, 0x07,
        0x8E, 0x25, 0x1E, 0xBB, 0xC3, 0xA1,
        0x41, 0x31, // Object ID (A1)
        1,          // Instance Count
        0x06,       // Flags WMIACPI_REGFLAG_METHOD & WMIACPI_REGFLAG_STRING
        0xEF, 0x54, 0x4B, 0x6A, 0xED, 0xA5, 0x33, 0x4D, 0x94, 0x55,
        0xB0, 0xD9, 0xB4, 0x8D, 0xF4, 0xB3, 
        0x41, 0x32, // Object ID (A2)
        1,          // Instance Count
        0x06,       // Flags WMIACPI_REGFLAG_METHOD & WMIACPI_REGFLAG_STRING
        0xB6, 0xEB, 0xF1, 0x74, 0x7A, 0x92, 0x7D, 0x4C, 0x95, 0xDF,
        0x69, 0x8E, 0x21, 0xE8, 0x0E, 0xB5,
        0x41, 0x33, // Object ID (A3)
        1,          // Instance Count
        0x06,       // Flags WMIACPI_REGFLAG_METHOD & WMIACPI_REGFLAG_STRING
        0xFF, 0x04, 0xEF, 0x7E, 0x28, 0x43, 0x7C, 0x44, 0xB5, 0xBB,
        0xD4, 0x49, 0x92, 0x5D, 0x53, 0x8D, 
        0x41, 0x34, // Object ID (A4)
        1,          // Instance Count
        0x06,       // Flags WMIACPI_REGFLAG_METHOD & WMIACPI_REGFLAG_STRING
        0x9E, 0x15, 0xDB, 0x8A, 0x32, 0x1E, 0x5C, 0x45, 0xBC, 0x93,
        0x30, 0x8A, 0x7E, 0xD9, 0x82, 0x46,
        0x41, 0x35, // Object ID (A5)
        1,          // Instance Count
        0x01,       // Flags WMIACPI_REGFLAG_EXPENSIVE 
        0xFD, 0xD9, 0x51, 0x26, 0x1C, 0x91, 0x69, 0x4B, 0xB9, 0x4E,
        0xD0, 0xDE, 0xD5, 0x96, 0x3B, 0xD7, 
        0x41, 0x36, // Object ID (A6)
        1,          // Instance Count
        0x06,       // Flags WMIACPI_REGFLAG_METHOD & WMIACPI_REGFLAG_STRING
        0x1A, 0x65, 0x64, 0x73, 0x2F, 0x13, 0xE7, 0x4F, 0xAD, 0xAA,
        0x40, 0xC6, 0xC7, 0xEE, 0x2E, 0x3B,
        0x41, 0x37, // Object ID (A7)
        1,          // Instance Count
        0x06,       // Flags WMIACPI_REGFLAG_METHOD & WMIACPI_REGFLAG_STRING
        0x21, 0x12, 0x90, 0x05, 0x66, 0xD5, 0xD1, 0x11, 0xB2, 0xF0,
        0x00, 0xA0, 0xC9, 0x06, 0x29, 0x10, 
        0x42, 0x41, // Object ID (BA)
        1,          // Instance Count
        0x00        // Flags 
      })
  
      Name(RETN, Package()
      {
        "Success",
        "Not Supported",
        "Invalid Parameter",
        "Access Denied",
        "System Busy"
      })
      
      Name(VSEL, Package()
      {
        Package() {"Disable", "Enable"},
        Package() {"Auto", "On", "Off"}, 
      })
  
      Name(PCFG, Buffer(0x18){})
      Name(IBUF, Buffer(0x0100){})
      Name(ILEN, Zero)
  
      Include ("WmiGlobalNvsData.asl")
      Include ("WQBA.asl")
      
      //
      // WQA0() - Data block query for Lenovo_BiosSetting
      //
      // This method returns the instance data for the specified
      // instance index.
      //
      //     Arg0: Instance index of data block
      //
      //     Return: Error string.
      //
      Method (WQA0, 1, Serialized)
      {
        Acquire(MUTW, 0xFFFF)
        
//        P8XH (1,0xDD)
//        P8XH (0,0xA0)
//        MBGS ("WQA0:")
//        DB2H (Arg0)

        Store(L05_BIOS_SETTING_A0, WOID)
        Store(Ones, WSTA)
        Store(Arg0, WITM)
        Store(0,    LWBL)
        Store(0,    LWBF)
        
        Store (EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI, SOFTWARE_SMI_PORT)

        If (LNotEqual (LWBL, Zero)) {
          Store (ToString (LWBF, LWBL), Local0)
        } Else {
          Store (WMI_SETUP_UNDER_OS_NULL_STR, Local0)
        }

//        MBGS (Local0)
        Release(MUTW)
        Return(Local0)
      }
  
      //
      // WMA1() - Method execution for Lenovo_SetBiosSetting
      //
      //       Arg0 is instance being queried
      //       Arg1 is the method ID
      //       Arg2 is the method data passed
      //
      //       Format: item,setting[,password,encode,kbdlang];
      //
      //       Example:
      //           - "WakeOnLAN,Enable;"
      //           - "WakeOnLAN,Enable,pswd,ascii,us;"
      //           - "WakeOnLAN,Enable,191f1120,scancode;"
      //
      Method(WMA1, 3, Serialized)
      {
        Acquire(MUTW, 0xFFFF)
        
//        P8XH (1,0xDD)
//        P8XH (0,0xA1)
//        MBGS ("WMA1:")
        If (LEqual(SizeOf(Arg2), 0)) {
          Return(WMI_SETUP_UNDER_OS_NULL_STR)
        }

        If (LGreaterEqual(SizeOf(Arg2), 255)) {
          Return(WMI_SETUP_UNDER_OS_NULL_STR)
        }

        Store(L05_SET_BIOS_SETTING_A1, WOID)
        Store(Ones, WSTA)
        Store(Ones, WITM)
        Store(0, LWBF)
        Store(SizeOf(Arg2), LWBL)
        Store(Arg2, LWBF)
        
        Store (EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI, SOFTWARE_SMI_PORT)
        
        Release(MUTW)
        Return(DerefOf(Index(RETN, WSTA)))
      }
      
      //
      // WMA2() - Methods for Lenovo_SaveBiosSettings
      //
      //       Arg0 is instance being queried
      //       Arg1 is the method ID
      //       Arg2 is the method data passed
      //
      //       Format: [password,encode,kbdlang;]
      //
      //       Example:
      //           - ""
      //           - "pswd,ascii,us;"
      //           - "191f1120,scancode;"
      //
      Method(WMA2, 3, Serialized)
      {
        Acquire(MUTW, 0xFFFF)
        
//        P8XH (1,0xDD)
//        P8XH (0,0xA2)
//        MBGS ("WMA2:")

        Store(L05_SAVE_BIOS_SETTINGS_A2, WOID)
        Store(Ones, WSTA)
        Store(Ones, WITM)
        Store(0, LWBF)
        Store(SizeOf(Arg2), LWBL)
        Store(Arg2, LWBF)
        
        Store (EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI, SOFTWARE_SMI_PORT)
        
        Release(MUTW)
        Return(DerefOf(Index(RETN, WSTA)))
      }
      
      //
      // WMA3() - Methods for Lenovo_DiscardBiosSettings
      //
      //       Arg0 is instance being queried
      //       Arg1 is the method ID
      //       Arg2 is the method data passed
      //
      //       Format: [password,encode,kbdlang;]
      //
      //       Example:
      //           - ""
      //           - "pswd,ascii,us;"
      //           - "191f1120,scancode;"
      //
      Method(WMA3, 3, Serialized)
      {
        Acquire(MUTW, 0xFFFF)
        
//        P8XH (1,0xDD)
//        P8XH (0,0xA3)
//        MBGS ("WMA3:")

        Store(L05_DISCARD_BIOS_SETTINGS_A3, WOID)
        Store(Ones, WSTA)
        Store(Ones, WITM)
        Store(0, LWBF)
        Store(SizeOf(Arg2), LWBL)
        Store(Arg2, LWBF)

        Store (EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI, SOFTWARE_SMI_PORT)
        
        Release(MUTW)
        Return(DerefOf(Index(RETN, WSTA)))
      }
      
      //
      // WMA4() - Method for Lenovo_LoadDefaultSettings
      //
      //       Arg0 is instance being queried
      //       Arg1 is the method ID
      //       Arg2 is the method data passed
      //
      //       Format: [password,encode,kbdlang;]
      //
      //       Example:
      //           - ""
      //           - "pswd,ascii,us;"
      //           - "191f1120,scancode;"
      //
      Method(WMA4, 3, Serialized)
      {
        Acquire(MUTW, 0xFFFF)
        
//        P8XH (1,0xDD)
//        P8XH (0,0xA4)
//        MBGS ("WMA4:")

        Store(L05_LOAD_DEFAULT_SETTINGS_A4, WOID)
        Store(Ones, WSTA)
        Store(Ones, WITM)
        Store(0, LWBF)
        Store(SizeOf(Arg2), LWBL)
        Store(Arg2, LWBF)
        
        Store (EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI, SOFTWARE_SMI_PORT)
        
        Release(MUTW)
        Return(DerefOf(Index(RETN, WSTA)))
      }

      //
      // WQA5 - Method for Lenovo_BiosPasswordSettings
      //
      //     Arg0: Instance index of data block
      //
      Method(WQA5, 1, Serialized)
      {
        Acquire(MUTW, 0xFFFF)
        
//        P8XH (1,0xDD)
//        P8XH (0,0xA5)
//        MBGS ("WQA5:")
        
        Store(L05_BIOS_PASSWORD_SETTINGS_A5, WOID)
        Store(Ones, WSTA)
        Store(Ones, WITM)
        
        Store (EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI, SOFTWARE_SMI_PORT)
        
  //      Store(WMIS(0x05, Zero), Local0)
  //      Store(WSPM, Index(PCFG, Zero))
  //      Store(WSPS, Index(PCFG, 0x04))
  //      Store(WSMN, Index(PCFG, 0x08))
  //      Store(WSMX, Index(PCFG, 0x0C))
  //      Store(WSEN, Index(PCFG, 0x10))
  //      Store(WSKB, Index(PCFG, 0x14))
        Release(MUTW)
        Return(PCFG)
      }

      //
      // WMA6() - Method for Lenovo_SetBiosPassword
      //
      //       Arg0 is instance being queried
      //       Arg1 is the method ID
      //       Arg2 is the method data passed
      //
      //       Format: type,current,new,encode,kbdlang;
      //
      //       Example:
      //           - "pap,foo,bar,ascii,us;"
      //           - "pop,foo,bar,ascii,us;"
      //           - "hdd0,foo,bar,ascii,us;"
      //
      Method(WMA6, 3, Serialized)
      {
        Acquire(MUTW, 0xFFFF)
        
//        P8XH (1,0xDD)
//        P8XH (0,0xA6)
//        MBGS ("WMA6:")
        
        Store(L05_SET_BIOS_PASSWORD_A6, WOID)
        Store(Ones, WSTA)
        Store(Ones, WITM)
        Store(0, LWBF)
        Store(SizeOf(Arg2), LWBL)
        Store(Arg2, LWBF)
        
        Store (EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI, SOFTWARE_SMI_PORT)
        
        Release(MUTW)
        Return(DerefOf(Index(RETN, WSTA)))
      }
  
      //
      // WMA7 - Method for Lenovo_GetBiosSelections
      //
      //       Arg0 is instance being queried
      //       Arg1 is the method ID
      //       Arg2 is the method data passed
      //
      //       Format: item
      //
      //       Example:
      //           input:  "WakeOnLAN;"
      //           output: "Disable,ACOnly,ACandBattery,Enable"
      //
      //     Local0 is the package of WMI Buffer.
      //     Local1 is the output string. 
      //
      Method(WMA7, 3, Serialized)
      {
        Acquire(MUTW, 0xFFFF)
        
//        P8XH (1,0xDD)
//        P8XH (0,0xA7)
//        MBGS ("WMA7:")
//        MBGS ("Arg2:")
//        MBGS (Arg2)

        If (LEqual(SizeOf(Arg2), 0)) {
          Return(WMI_SETUP_UNDER_OS_NULL_STR)
        }

        If (LGreaterEqual(SizeOf(Arg2), 255)) {
          Return(WMI_SETUP_UNDER_OS_NULL_STR)
        }

        Store(L05_GET_BIOS_SELECTIONS_A7, WOID)
        Store(Ones, WSTA)
        Store(Ones, WITM)
        Store(0, LWBF)
        Store(SizeOf(Arg2), LWBL)
        Store(Arg2, LWBF)

        Store(EFI_L05_WMI_SETUP_UNDER_OS_SW_SMI, SOFTWARE_SMI_PORT)

        If (LNotEqual (LWBL, Zero)) {
          Store (ToString (LWBF, LWBL), Local0)
        } Else {
          Store (WMI_SETUP_UNDER_OS_NULL_STR, Local0)
        }

//        MBGS (Local0)
        Release(MUTW)
        Return(Local0)
      }
    }
  }
}
