/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "WmiSetupUnderOsSmm.h"

/**
  WMI Method for Get BIOS selections -

    1. Switch config type to get BIOS selections form mWmiSelectStrListMap by L05WmiItem.
    2. Special type to get BIOS selections form special way.

    Mapping to WMI class  : Lenovo_GetBiosSelections
    Mapping to ASL method : WMA7

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
WmiGetBiosSelectionsA7 (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  CHAR8                                 SelectStr[L05_WMI_BUFFER_MAX_SIZE];
  UINT16                                SelectStrLen;
  UINTN                                 SelectStrListIndex;
  UINTN                                 BufferOffset;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;

  //
  // Analyze WMI input buffer
  //
  Status = AnalyzeWmiInputBuffer (SelectStr, &SelectStrLen, &mL05GlobalNVSArea->L05WmiItem, &SelectStrListIndex);

  //
  // Clean WMI buffer & Length
  //
  ZeroMem (mL05GlobalNVSArea->L05WmiBuffer, L05_WMI_BUFFER_MAX_SIZE);
  mL05GlobalNVSArea->L05WmiBufferLen = 0;

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Init buffer offset
  //
  BufferOffset = 0;

  //
  // Put selection to WMI buffer
  //
  if (mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigDataType < SwitchConfigTypeMax) {
    //
    // Switch config type to get BIOS selections form mWmiSelectStrListMap by L05WmiItem
    //
    for (Index = 0; Index < mWmiSelectStrListMap[SelectStrListIndex].SelectStrCount; Index++) {
      AsciiStrCpyS (&mL05GlobalNVSArea->L05WmiBuffer[BufferOffset], (L05_WMI_BUFFER_MAX_SIZE - BufferOffset) * sizeof(UINT8), mWmiSelectStrListMap[SelectStrListIndex].SelectStrList[Index]);
      BufferOffset += AsciiStrLen (mWmiSelectStrListMap[SelectStrListIndex].SelectStrList[Index]);

      //
      // End of items not need add ','
      //
      if (Index < (mWmiSelectStrListMap[SelectStrListIndex].SelectStrCount - (UINTN) 1)) {
        mL05GlobalNVSArea->L05WmiBuffer[BufferOffset] = ',';
        BufferOffset++;
      }
    }

  } else {
    //
    // Special type to get BIOS selections form special way
    //
    Status = EFI_INVALID_PARAMETER;

    switch (mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigDataType) {

    case ClearUserPasswordType:
    case ClearIntelPttKeyType:
    case ClearAmdPspKeyType:
    case ResetToSetupModeType:
    case RestoreFactoryKeysType:
      AsciiStrCpyS (&mL05GlobalNVSArea->L05WmiBuffer[BufferOffset], (L05_WMI_BUFFER_MAX_SIZE - BufferOffset) * sizeof(UINT8), WMI_SETUP_UNDER_OS_ENTER_STR);
      BufferOffset += AsciiStrLen (WMI_SETUP_UNDER_OS_ENTER_STR);
      Status = EFI_SUCCESS;
      break;

    case EfiBootOrderType:
      if (mBootOrderStr == NULL) {
        mBootOrderStr = GetBootOrderStr (mL05WmiSetupItem.L05EfiBootOrder, mL05WmiSetupItem.L05EfiBootOrderCount, mL05WmiSetupItem.L05WmiEfiBootDescription, ',');
      }

      if (mBootOrderStr != NULL) {
        AsciiStrCpyS (&mL05GlobalNVSArea->L05WmiBuffer[BufferOffset], (L05_WMI_BUFFER_MAX_SIZE - BufferOffset) * sizeof(UINT8), mBootOrderStr);
        BufferOffset += AsciiStrLen (mBootOrderStr);
        FreePool (mBootOrderStr);
        mBootOrderStr = NULL;
        Status = EFI_SUCCESS;
      }

      break;

    case LegacyBootOrderType:
      if (mBootOrderStr == NULL) {
        mBootOrderStr = GetBootOrderStr (mL05WmiSetupItem.L05LegacyBootOrder, mL05WmiSetupItem.L05LegacyBootOrderCount, mL05WmiSetupItem.L05WmiLegacyBootDescription, ',');
      }

      if (mBootOrderStr != NULL) {
        AsciiStrCpyS (&mL05GlobalNVSArea->L05WmiBuffer[BufferOffset], (L05_WMI_BUFFER_MAX_SIZE - BufferOffset) * sizeof(UINT8), mBootOrderStr);
        BufferOffset += AsciiStrLen (mBootOrderStr);
        FreePool (mBootOrderStr);
        mBootOrderStr = NULL;
        Status = EFI_SUCCESS;
      }

      break;

    default:
      return EFI_UNSUPPORTED;
    }
  }

  //
  // Set WMI buffer length
  //
  mL05GlobalNVSArea->L05WmiBufferLen = (UINT16) BufferOffset;

  return Status;
}

