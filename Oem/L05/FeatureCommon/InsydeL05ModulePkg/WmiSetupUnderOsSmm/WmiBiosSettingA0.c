/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "WmiSetupUnderOsSmm.h"

/**
  Get config value by mL05GlobalNVSArea->L05WmiItem -

    1. Switch config type to get config value form mWmiBiosConfigEnumMap by L05WmiItem.
    2. Special type to get config value form special way.

  @param  None

  @retval EFI_SUCCESS                   Get config value successfully.
  @retval EFI_UNSUPPORTED               Can't get config value.
**/
EFI_STATUS
GetConfigValue (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINT8                                 TempSetupValue;
  UINTN                                 Index;

  //
  // Initialization
  //
  Status = EFI_UNSUPPORTED;

  //
  // Check L05WmiItem value is valid
  //
  if (mL05GlobalNVSArea->L05WmiItem >= mWmiBiosConfigEnumMapListCount) {
    return EFI_OUT_OF_RESOURCES;
  }

  if (mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigDataType < SwitchConfigTypeMax) {
    //
    // Switch config type to get config value form mWmiBiosConfigEnumMap by L05WmiSetupItem
    //
    TempSetupValue = *mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].L05WmiSetupItem;

    for (Index = 0; Index < mWmiSelectStrListMapListCount; Index++) {
      if (mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigDataType == mWmiSelectStrListMap[Index].ConfigDataType) {
        break;
      }
    }

    if (TempSetupValue >= mWmiSelectStrListMap[Index].SelectStrCount) {
      mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigValue = WMI_SETUP_UNDER_OS_NULL_STR;
      return EFI_UNSUPPORTED;
    }

    mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigValue = mWmiSelectStrListMap[Index].SelectStrList[TempSetupValue];

    Status = EFI_SUCCESS;

  } else {
    //
    // Special type to get config value form special way
    //
    switch (mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigDataType) {

    case ClearUserPasswordType:
    case ClearIntelPttKeyType:
    case ClearAmdPspKeyType:
    case ResetToSetupModeType:
    case RestoreFactoryKeysType:
      mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigValue = WMI_SETUP_UNDER_OS_NULL_STR;
      Status = EFI_SUCCESS;
      break;

    case EfiBootOrderType:
      if (mBootOrderStr == NULL) {
        mBootOrderStr = GetBootOrderStr (mL05WmiSetupItem.L05EfiBootOrder, mL05WmiSetupItem.L05EfiBootOrderCount, mL05WmiSetupItem.L05WmiEfiBootDescription, ':');
      }

      if (mBootOrderStr != NULL) {
        mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigValue = mBootOrderStr;
        Status = EFI_SUCCESS;
      }

      break;

    case LegacyBootOrderType:
      if (mBootOrderStr == NULL) {
        mBootOrderStr = GetBootOrderStr (mL05WmiSetupItem.L05LegacyBootOrder, mL05WmiSetupItem.L05LegacyBootOrderCount, mL05WmiSetupItem.L05WmiLegacyBootDescription, ':');
      }

      if (mBootOrderStr != NULL) {
        mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigValue = mBootOrderStr;
        Status = EFI_SUCCESS;
      }

      break;

    default:
      Status = EFI_UNSUPPORTED;
    }
  }

  return Status;
}

/**
  WMI Method for Query BIOS setting -

    Mapping to WMI class  : Lenovo_BiosSetting
    Mapping to ASL method : WQA0

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
WmiBiosSettingA0 (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINTN                                 BufferOffset;
  UINT16                                StrSize;

  //
  // Initialization
  //
  Status       = EFI_SUCCESS;
  BufferOffset = 0;
  StrSize      = 0;

  //
  // Clean WMI buffer
  //
  ZeroMem (mL05GlobalNVSArea->L05WmiBuffer, L05_WMI_BUFFER_MAX_SIZE);

  //
  // Check L05WmiItem value is valid
  //
  if (mL05GlobalNVSArea->L05WmiItem >= mWmiBiosConfigEnumMapListCount) {
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Check L05 WMI Setup Item is Valid
  //
  if (*mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].L05WmiSetupItemValid == FALSE) {
    mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigValue = WMI_SETUP_UNDER_OS_NULL_STR;
    AsciiStrCpyS (mL05GlobalNVSArea->L05WmiBuffer, L05_WMI_BUFFER_MAX_SIZE, WMI_BIOS_SETTING_A0_RETURN_NULL_STR);
    mL05GlobalNVSArea->L05WmiBufferLen = (UINT16) AsciiStrLen (mL05GlobalNVSArea->L05WmiBuffer);
    return EFI_UNSUPPORTED;
  }

  //
  // Put BIOS config item name to WMI buffer
  //
  BufferOffset = 0;
  AsciiStrCpyS (&mL05GlobalNVSArea->L05WmiBuffer[BufferOffset], L05_WMI_BUFFER_MAX_SIZE - (BufferOffset * sizeof (UINT8)), mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].BiosConfigItemName);
  BufferOffset += AsciiStrLen (mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].BiosConfigItemName);

  mL05GlobalNVSArea->L05WmiBuffer[BufferOffset] = ',';
  BufferOffset++;

  //
  // Get Config Value
  //
  Status = GetConfigValue ();

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Put config value to WMI buffer
  //
  AsciiStrCpyS (&mL05GlobalNVSArea->L05WmiBuffer[BufferOffset], L05_WMI_BUFFER_MAX_SIZE - (BufferOffset * sizeof (UINT8)), mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigValue);
  BufferOffset += AsciiStrLen (mWmiBiosConfigEnumMap[mL05GlobalNVSArea->L05WmiItem].ConfigValue);

  //
  // Set WMI buffer length
  //
  mL05GlobalNVSArea->L05WmiBufferLen = (UINT16) BufferOffset;

  //
  // Free resources
  //
  if (mBootOrderStr != NULL) {
    FreePool (mBootOrderStr);
    mBootOrderStr = NULL;
  }

  return Status;
}

