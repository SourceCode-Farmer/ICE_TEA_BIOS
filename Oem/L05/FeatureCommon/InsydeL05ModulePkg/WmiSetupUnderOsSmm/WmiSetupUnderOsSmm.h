/** @file
  Provide functions for WMI SMM Service

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_WMI_SETUP_UNDER_OS_SMM_H_
#define _L05_WMI_SETUP_UNDER_OS_SMM_H_

#include <OemSwSmi.h>       // EFI_L05_WMI_SETUP_UNDER_OS_CALLBACK
#include <L05Config.h>      // EFI_L05_SYSTEM_PASSWORD_MAX_RETRY
#include <L05WmiSetupUnderOsDefinition.h>
#include <Guid/HddPasswordVariable.h> // HDD_PASSWORD_TABLE
#include <Guid/L05VariableTool.h> // gL05SystemSuperPasswordGuid
#include <Guid/L05WmiSetupUnderOsSetupVariable.h>  // gL05WmiSetupUnderOsSetupVariableGuid
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/SmmVariable.h>
#include <Protocol/AcpiTable.h>
#include <Protocol/SmmFwBlockService.h>  // EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL
#include <Protocol/SysPasswordService.h> // PASSWORD_TYPE
#include <Protocol/L05Variable.h> // EFI_L05_VARIABLE_PROTOCOL
#include <Protocol/HddPasswordService.h>
#include <Library/BaseLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DxeServicesLib.h>
#include <Library/PrintLib.h>
#include <Library/DebugLib.h>
#include <Library/L05WmiSetupItemLib.h>
#include <Library/FlashRegionLib.h>      // FdmGetNAtAddr
#include <Library/FeatureLib/OemSvcSecurityPassword.h>

//
// WDG Instance GUID
//
#define WDG_BIOS_SETTING_A0_GUID \
  {0x51F5230E, 0x9677, 0x46CD, {0xA1, 0xCF, 0xC0, 0xB2, 0x3E, 0xE3, 0x4D, 0xB7}}

//
// Define for WMI BIOS Setting return string
//
#define WMI_BIOS_SETTING_A0_RETURN_NULL_STR       ""
#define WMI_BIOS_SETTING_RETURN_ACCESS_DENIED_STR "Access Denied, "
#define WMI_BIOS_SETTING_RETURN_SYSTEM_BUSY_STR   "System Busy, "

//
// Define for WMI password
//
#define L05_WMI_PASSWORD_MAX_LENGTH               FixedPcdGet32 (PcdDefaultSysPasswordMaxLength)
#define WMI_BIOS_PASSWORD_ADMIN_TYPE              "pap"
#define WMI_BIOS_PASSWORD_USER_TYPE               "POP"
#define WMI_BIOS_PASSWORD_HDD_MASTER_TYPE         "mhdp"
#define WMI_BIOS_PASSWORD_HDD_USER_TYPE           "uhdp"
#define WMI_BIOS_PASSWORD_ASCII_ENCODING          "ascii"
#define WMI_BIOS_PASSWORD_SCANCODE_ENCODING       "scancode"
#define WMI_BIOS_PASSWORD_US_LANGUAGE             "us"
#define WMI_BIOS_PASSWORD_FR_LANGUAGE             "fr"
#define WMI_BIOS_PASSWORD_GR_LANGUAGE             "gr"

//
// Define for WMI L05 PXE string
//
#define WMI_L05_PXE_IPV4_STRING                   "EFI PXE 0 for IPv4"
#define WMI_L05_PXE_IPV6_STRING                   "EFI PXE 0 for IPv6"
#define WMI_L05_PXE_NETWROK_STRING                "EFI PXE Network"

#pragma pack (1)
//
// L05 WMI Setup Under OS NVS Area definition
//
typedef struct {
  UINT8                                 L05WmiObjectId;                        // WOID
  UINT8                                 L05WmiStatus;                          // WSTA
  UINT8                                 L05WmiItem;                            // WITM
  UINT16                                L05WmiBufferLen;                       // LWBL
  UINT8                                 L05WmiBuffer[L05_WMI_BUFFER_MAX_SIZE]; // LWBF
} EFI_L05_GLOBAL_NVS_AREA;

//
// AML decode struct for 32bit Offset & 32bit region length
//
typedef struct {
  UINT8                                 OpRegionOp;
  UINT32                                NameString;
  UINT8                                 RegionSpace;
  UINT8                                 OffsetPrefix;
  UINT32                                RegionOffset;
  UINT8                                 LenPrefix;
  UINT32                                RegionLen;
} AML_OP_REGION_32_32;

//
// AML decode struct for Name Buffer Header
//
typedef struct {
  UINT8                                 NameOp;
  UINT32                                NameString;
  UINT8                                 BufferOp;
} AML_NAME_BUFFER_HEADER;

typedef struct {
  EFI_GUID                              InstanceGuid;
  CHAR8                                 ObjectId[2];
  UINT8                                 InstanceCount;
  UINT8                                 Flags;
} AML_WDG_TABLE;

typedef enum {
  UserNotReadOnly = 0x00,
  UserReadOnly,
  ReadOnly,
} SCU_ACCESS_LEVEL;

typedef enum {
  EnableDisableType = 0x00,
  SATAControllerType,
  GraphicsDeviceType,
  IntelVtType,
  IntelVtdType,
  AmdVType,
  BiosBackFlashType,
  DeviceGuardType,
  IntelSgxType,
  SecureBootType,
  PlatformModeType,
  SecureBootModeType,
  BootModeType,
  BootPriorityType,
  OsOptimizedDefaultsType,
  SwitchConfigTypeMax,
  ClearUserPasswordType,
  ClearIntelPttKeyType,
  ClearAmdPspKeyType,
  ClearSecurityChipKeyType,
  ResetToSetupModeType,
  RestoreFactoryKeysType,
  EfiBootOrderType,
  LegacyBootOrderType,
} CONFIG_DATA_TYPE;

typedef enum {
  SetBiosSettingItem = 0x00,
  SetBiosSettingValue,
  SetBiosSettingPassword,
  SetBiosSettingPasswordEncoding,
  SetBiosSettingPasswordLanguage,
  SetBiosSettingMaxSection,
  SetBiosSettingWithoutPassword = SetBiosSettingPassword,
  SetBiosSettingWithPassword    = SetBiosSettingMaxSection,
} WMI_SET_BIOS_SETTING_SECTION;

typedef enum {
  CheckPassword = 0x00,
  CheckPasswordEncoding,
  CheckPasswordLanguage,
  CheckPasswordMaxSection,
} WMI_CHECK_PASSWORD_SECTION;

typedef enum {
  SetBiosPasswordType = 0x00,
  SetBiosPasswordCurrentPassword,
  SetBiosPasswordNewPassword,
  SetBiosPasswordEncoding,
  SetBiosPasswordLanguage,
  SetBiosPasswordMaxSection,
} WMI_SET_BIOS_PASSWORD_SECTION;

typedef struct {
  CHAR8                                 *BiosConfigItemName;     // BIOS Config Item Name
  CHAR8                                 *ConfigValue;
  SCU_ACCESS_LEVEL                      ScuAccessLevel;
  CONFIG_DATA_TYPE                      ConfigDataType;
  BOOLEAN                               *L05WmiSetupItemValid;
  UINT8                                 *L05WmiSetupItem;
} WMI_BIOS_CONFIG_DATA_MAP;

typedef struct {
  CONFIG_DATA_TYPE                      ConfigDataType;
  CHAR8                                 **SelectStrList;
  UINT16                                SelectStrCount;
} WMI_SELECT_STR_LIST_MAP;

typedef struct {
  CHAR8                                 *Item;
  CHAR8                                 *Value;
} WMI_SET_BIOS_SETTING_PARAMETER;

typedef struct {
  CHAR8                                 *Password;
  CHAR8                                 *PasswordEncodingStr;
  CHAR8                                 *PasswordLanguageStr;
} WMI_CHECK_PASSWORD_PARAMETER;

typedef struct {
  CHAR8                                 AsciiChar;
  UINT8                                 KeyboardScanCode;
} WMI_KEYBOARD_SCANCODE_LIST;

typedef struct {
  WMI_PASSWORD_TYPE                     PasswordType;
  CHAR8                                 *PasswordTypeStr;
} WMI_PASSWORD_TYPE_MAP;

typedef struct {
  WMI_PASSWORD_ENCODING                 PasswordEncoding;
  CHAR8                                 *PasswordEncodingStr;
} WMI_PASSWORD_ENCODING_MAP;

typedef struct {
  WMI_PASSWORD_LANGUAGE                 PasswordLanguage;
  CHAR8                                 *PasswordLanguageStr;
} WMI_PASSWORD_LANGUAGE_MAP;

typedef struct {
  WMI_PASSWORD_TYPE                     PasswordType;
  CHAR8                                 CurrentPassword[((L05_WMI_PASSWORD_MAX_LENGTH > HDD_PASSWORD_MAX_NUMBER) ? L05_WMI_PASSWORD_MAX_LENGTH : HDD_PASSWORD_MAX_NUMBER) + 1];
  CHAR8                                 NewPassword[((L05_WMI_PASSWORD_MAX_LENGTH > HDD_PASSWORD_MAX_NUMBER) ? L05_WMI_PASSWORD_MAX_LENGTH : HDD_PASSWORD_MAX_NUMBER) + 1];
} WMI_PASSWORD_DATA_MAP;

#pragma pack ()

EFI_STATUS
AnalyzeWmiCheckPasswordInputBuffer (
  OUT CHAR8                             **PasswordStr,
  OUT CHAR8                             **PasswordEncodingStr,
  OUT CHAR8                             **PasswordLanguageStr
  );

EFI_STATUS
AnalyzeWmiInputBuffer (
  CHAR8                                 *SelectStr,
  UINT16                                *SelectStrLen,
  UINT8                                 *SelectItemIndex,
  UINTN                                 *SelectStrListIndex
  );

CHAR8 *
GetBootOrderStr (
  UINT16                                *BootOrder,
  UINT16                                BootOrderCount,
  L05_WMI_BOOT_DESCRIPTION              *L05WmiBootDescription,
  CHAR8                                 DelimiterChar
  );

EFI_STATUS
GetConfigValue (
  VOID
  );

BOOLEAN
WmiInputPasswordIsRepeat (
  VOID
  );

BOOLEAN
WmiIsAdminPasswordExist (
  VOID
  );

BOOLEAN
WmiIsUserPasswordExist (
  VOID
  );

BOOLEAN
WmiIsHddPasswordExist (
  VOID
  );

EFI_STATUS
ClearSystemPassword (
  WMI_PASSWORD_TYPE                     PasswordType
  );

EFI_STATUS
SecurityDataInit (
  IN BOOLEAN                            IsAlreadyInit
  );

EFI_STATUS
WmiCheckAdminPassword (
  CHAR8                                 *Password,
  CHAR8                                 *PasswordEncodingStr,
  CHAR8                                 *PasswordLanguageStr
  );

EFI_STATUS
EFIAPI
WmiBiosSettingA0 (
  VOID
  );

EFI_STATUS
EFIAPI
WmiSetBiosSettingA1 (
  VOID
  );

EFI_STATUS
EFIAPI
WmiSaveBiosSettingsA2 (
  VOID
  );

EFI_STATUS
EFIAPI
WmiDiscardBiosSettingsA3 (
  VOID
  );

EFI_STATUS
EFIAPI
WmiLoadDefaultSettingsA4 (
  VOID
  );

EFI_STATUS
EFIAPI
WmiBiosPasswordSettingsA5 (
  VOID
  );

EFI_STATUS
EFIAPI
WmiSetBiosPasswordA6 (
  VOID
  );

EFI_STATUS
EFIAPI
WmiGetBiosSelectionsA7 (
  VOID
  );

extern EFI_L05_GLOBAL_NVS_AREA          *mL05GlobalNVSArea;
extern EFI_SMM_VARIABLE_PROTOCOL        *mSmmVariable;
extern L05_WMI_SETUP_ITEM               mL05WmiSetupItem;
extern BOOLEAN                          mAdminPasswordExist;
extern BOOLEAN                          mUserPasswordExist;
extern BOOLEAN                          mHddPasswordExist;
extern BOOLEAN                          mPasswordChange;
extern UINTN                            mPasswordErrorCount;
extern WMI_PASSWORD_DATA_MAP            mPasswordCheckData;
extern CHAR8                            *mBootOrderStr;
extern WMI_BIOS_CONFIG_DATA_MAP         mWmiBiosConfigEnumMap[];
extern WMI_SELECT_STR_LIST_MAP          mWmiSelectStrListMap[];
extern UINT16                           mWmiBiosConfigEnumMapListCount;
extern UINT16                           mWmiSelectStrListMapListCount;
extern CHAR8*                           mEnableDisableStrList[];
extern CHAR8*                           mNullEnableDisableStrList[];
extern CHAR8*                           mNullDisableEnableStrList[];
extern CHAR8*                           mSATAControllerStrList[];
extern CHAR8*                           mGraphicsDeviceStrList[];
extern CHAR8*                           mIntelSgxStrList[];
extern CHAR8*                           mPlatformModeStrList[];
extern CHAR8*                           mSecureBootModeStrList[];
extern CHAR8*                           mBootModeStrList[];
extern CHAR8*                           mBootPriorityStrList[];
extern BOOLEAN                          mIsSgxFeatureCtrlSet;
#endif
