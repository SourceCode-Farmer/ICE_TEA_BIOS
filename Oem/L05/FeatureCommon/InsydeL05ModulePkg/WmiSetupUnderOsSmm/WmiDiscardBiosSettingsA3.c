/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "WmiSetupUnderOsSmm.h"

/**
  WMI Method for Discard BIOS settings -

    Mapping to WMI class  : Lenovo_DiscardBiosSettings
    Mapping to ASL method : WMA3

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval EFI_ACCESS_DENIED             WMI input password is invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
WmiDiscardBiosSettingsA3 (
  VOID
  )
{
  EFI_STATUS                            Status;
  WMI_CHECK_PASSWORD_PARAMETER          WmiCheckPasswordParameter;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;
  ZeroMem (&WmiCheckPasswordParameter, sizeof (WMI_CHECK_PASSWORD_PARAMETER));

  //
  // When admin password exist need to check password
  //
  if (mAdminPasswordExist) {
    //
    // Process WMI input password
    //
    Status = AnalyzeWmiCheckPasswordInputBuffer (
               &WmiCheckPasswordParameter.Password,
               &WmiCheckPasswordParameter.PasswordEncodingStr,
               &WmiCheckPasswordParameter.PasswordLanguageStr
               );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    //
    // Check admin password is valid
    //
    Status = WmiCheckAdminPassword (
               WmiCheckPasswordParameter.Password,
               WmiCheckPasswordParameter.PasswordEncodingStr,
               WmiCheckPasswordParameter.PasswordLanguageStr
               );

    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  //
  // Platform discard BIOS settings
  //
  Status = L05WmiSetupItemFunc (mSmmVariable, L05WmiSetupItemDiscard, &mL05WmiSetupItem);

  return Status;
}

