/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "WmiSetupUnderOsSmm.h"

/**
  Special item save.
  1. L05ClearUserPassword

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
SpecialItemSave (
  VOID
  )
{
  EFI_STATUS                            Status;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;

  //
  // Special item save
  //
  if (mL05WmiSetupItem.L05ClearUserPassword) {
    Status = ClearSystemPassword (WmiSystemUserType);
    mL05WmiSetupItem.L05ClearUserPassword = FALSE;
  }

  return Status;
}

/**
  WMI Method for Save BIOS settings -

    Mapping to WMI class  : Lenovo_SaveBiosSettings
    Mapping to ASL method : WMA2

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval EFI_ACCESS_DENIED             WMI input password is invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
WmiSaveBiosSettingsA2 (
  VOID
  )
{
  EFI_STATUS                            Status;
  WMI_CHECK_PASSWORD_PARAMETER          WmiCheckPasswordParameter;

  //
  // Initialization
  //
  Status = EFI_SUCCESS;
  ZeroMem (&WmiCheckPasswordParameter, sizeof (WMI_CHECK_PASSWORD_PARAMETER));

  //
  // When admin password exist need to check password
  //
  if (mAdminPasswordExist) {
    //
    // Process WMI input password
    //
    Status = AnalyzeWmiCheckPasswordInputBuffer (
               &WmiCheckPasswordParameter.Password,
               &WmiCheckPasswordParameter.PasswordEncodingStr,
               &WmiCheckPasswordParameter.PasswordLanguageStr
               );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    //
    // Check admin password is valid
    //
    Status = WmiCheckAdminPassword (
               WmiCheckPasswordParameter.Password,
               WmiCheckPasswordParameter.PasswordEncodingStr,
               WmiCheckPasswordParameter.PasswordLanguageStr
               );

    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  //
  // Special item save
  //
  Status = SpecialItemSave ();

  //
  // Platform save BIOS settings
  //
  Status = L05WmiSetupItemFunc (mSmmVariable, L05WmiSetupItemSave, &mL05WmiSetupItem);

  return Status;
}

