/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "WmiSetupUnderOsSmm.h"

/**
  WMI Method for Query BIOS password settings -

    Mapping to WMI class  : 
    Mapping to ASL method : WQA5

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
WmiBiosPasswordSettingsA5 (
  VOID
  )
{
  EFI_STATUS                            Status;

  Status = EFI_UNSUPPORTED;
  return Status;
}

