/** @file
  BIOS Self-Healing SMM Driver.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BIOS_SELF_HEALING_SMM_H_
#define _BIOS_SELF_HEALING_SMM_H_

#include <Uefi.h>
#include <OemSwSmi.h>
#include <FlashRegionLayout.h>
#include <H2OIhisi.h>  // IHISI_REGISTER_TABLE
#include <L05BiosSelfHealingConfig.h>
#include <L05ChipsetNameList.h>

//
// Libraries
//
#include <Library/UefiLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/ResetSystemLib.h>  // ResetCold
#include <Library/BiosSelfHealingLib.h>
#include <Library/SmmDxeBiosSelfHealingLib.h>
#include <Library/FeatureLib/OemSvcNotifyEcToSetWdtFlag.h>

//
// Consumed Protocols
//
#include <Protocol/SmmCpu.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/SmmFwBlockService.h>
#include <Protocol/H2OIhisi.h>
#include <Protocol/L05SmmSwSmiInterface.h>


EFI_STATUS
FbtsApHookForBios (
  VOID
  );

#endif
