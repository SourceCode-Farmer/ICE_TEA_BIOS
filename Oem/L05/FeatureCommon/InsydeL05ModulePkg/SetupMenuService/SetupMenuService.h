/** @file
 Function Service for L05 Setup Menu

;******************************************************************************
;* Copyright (c) 2013 - 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_SETUP_MENU_SERVICE_H_
#define _EFI_SETUP_MENU_SERVICE_H_

#include <Uefi.h>
#include <SetupConfig.h>
#include <L05ChipsetNameList.h>
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RAVENRIDGE  || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_PICASSO     || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_RENOIR      || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_CEZANNE     || \
     FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_REMBRANDT)
#include <Dxe/AmdPbsSetupDxe/AmdPbsConfig.h>  // AMD_PBS_SETUP_OPTION
#endif
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_REMBRANDT)
#include <Addendum/Apcb/Inc/RMB/ApcbV3TokenUid.h>
#include <Addendum/Apcb/Inc/RMB/ApcbV3Priority.h>
#endif

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DevicePathLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/Tpm2PtpCommLib.h>
#include <Library/HobLib.h>
#include <Library/H2OCpLib.h>
#include <Library/OemSvcSwitchLib.h>
#include <Library/FeatureLib/OemSvcDetectNvmePresence.h>
#include <Library/FeatureLib/OemSvcOverrideSetupSettingDuringPost.h>
#include <Library/FeatureLib/OemSvcGetMfgMode.h>
#include <Library/FeatureLib/OemSvcGetGpuType.h>

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//

//
// Produced Protocols
//
#include <Protocol/L05SetupMenu.h>
#include <Protocol/L05SecureBoot.h> // gEfiL05SecureBootProtocolGuid
#if (FixedPcdGet32 (PcdL05ChipsetName) == L05_CHIPSET_NAME_REMBRANDT)
#include <Protocol/AmdApcbProtocol.h>
#endif

//
// Guids
//
#include <Guid/ImageAuthentication.h>  // L"EFI_SETUP_MODE_NAME"
#include <Guid/AdmiSecureBoot.h>  // L"EFI_CUSTOM_SECURITY_NAME"
#include <Guid/DebugMask.h>  // gEfiGenericVariableGuid
#include <Guid/H2OCp.h>  // gH2OBdsCpDisplayStringBeforeGuid
#include <Guid/L05BackupSetupItemVariable.h>  // gEfiL05BackupSetupItemVariableGuid
#include <Guid/L05VariableExist.h>  // gEfiL05VariableExistGuid
#include <Guid/L05SwitchFlipToBoot.h>  // gEfiL05FliptoBootVariableGuid

//
// Driver Version
//

//
// Protocol instances
//

//
// Include files with function prototypes
//

#define EFI_INVALID_KEY_STATE           0x00000000

typedef struct {
  CHAR16                                UnicodeChar;
  UINT16                                KeyboardScanCode;
} EFI_KEYBOARD_SCANCODE_LIST;

#endif
