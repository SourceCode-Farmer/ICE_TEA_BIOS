/** @file
  SMM Interface for the EraseCpt Tool

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_ERASE_CPT_SMM_H_
#define _L05_ERASE_CPT_SMM_H_

#include <Uefi.h>
#include <OemSwSmi.h>
#include <FlashRegionLayout.h>
#include <L05Config.h>

//
// Libraries
//
#include <Library/SmmServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/MemoryAllocationLib.h>

//
// Consumed Protocols
//
#include <Protocol/SmmCpu.h>
#include <Protocol/SmmFwBlockService.h>
#include <Protocol/L05ComputraceInfo.h>
#include <Protocol/L05SmmSwSmiInterface.h>
#include <Protocol/L05Variable.h>

//
// Produced Protocols
//

//
// Guids
//

//
// Interface definition
//
#define L05_COMPUTRACE_ERASE_FUNCTION                       (UINT32)(0x534F8F00 + (UINT8)L05_SECURITY_SW_SMI)
#define L05_COMPUTRACE_SET_ERASE_FLAG                       (UINT32)0x00
#define L05_COMPUTRACE_CHECK_ERASE_FLAG                     (UINT32)0x01
#define L05_COMPUTRACE_CHECK_ERASE_RESULT                   (UINT32)0x02
#define L05_COMPUTRACE_CHECK_ERASE_RESULT                   (UINT32)0x02

//
// Return Code
//
#define L05_COMPUTRACE_SET_ERASE_FLAG_SUCCESS               (UINT32)(1 << 31)
#define L05_COMPUTRACE_SET_ERASE_FLAG_FAILED                (UINT32)0x00
#define L05_COMPUTRACE_CHECK_ERASE_FLAG_SUCCESS             (UINT32)(1 << 31)
#define L05_COMPUTRACE_CHECK_ERASE_FLAG_NOT_SET             (UINT32)0x00
#define L05_COMPUTRACE_CHECK_ERASE_RESULT_SUCCESS           (UINT32)(1 << 31)
#define L05_COMPUTRACE_CHECK_ERASE_RESULT_NOT_ERASED        (UINT32)0x00
#define L05_COMPUTRACE_ERASE_FUNCTION_UNEXPECTED_ERROR      (UINT32)0x00

#endif
