/** @file
  SPEC    : Computrace implement requirement v1.2.doc

;******************************************************************************
;* Copyright (c) 2013 - 2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_COMPUTRACE_DRIVER_H_
#define _L05_COMPUTRACE_DRIVER_H_

#include <Pi/PiFirmwareFile.h>
#include <SmiTable.h>
#include <SetupConfig.h>
#include <OemCmos.h>
#include <L05Config.h>
#include <L05ChipsetNameList.h>
#include <FlashRegionLayout.h>

#include <Library/UefiLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DevicePathLib.h>
#include <Library/CmosLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/FeatureLib/OemSvcDxeComputrace.h>

#include <Protocol/IdeControllerInit.h>
#include <Protocol/L05ComputraceInfo.h>
#include <Protocol/DiskInfo.h>
#include <Protocol/BlockIo.h>
#include <Protocol/FirmwareVolume2.h>
#include <Protocol/L05ComputraceInfo.h>
#include <Protocol/SmmFwBlockService.h>
#include <Protocol/L05Variable.h>

#define  L05_COMPUTRACE_EFI_DRIVER_FILE_GUID \
  { \
    0xF0D046DA, 0x7736, 0x48AD, 0xA8, 0x2E, 0x09, 0xDE, 0xC9, 0x9F, 0x0D, 0xD7 \
  }

#define L05_COMPUTRACE_EFI_WPBT_FILE_GUID \
  { \
    0xCF5F7BBF, 0x7FA4, 0x465E,  0x8D, 0x7F, 0xD4, 0x7E, 0X06, 0x95, 0xFA, 0xC4 \
  }

#define NULL_HANDLE                     ((VOID *) 0)

EFI_STATUS
L05ComputraceEntry (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  );

VOID
EFIAPI
L05ComputraceFunction (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  );

EFI_STATUS
EFIAPI
LoadAndStartImage
(
  IN  EFI_HANDLE                        *ImageHandle,
  IN  EFI_GUID                          *ImageGuid
  );

VOID
EFIAPI
DiksInfoProtocolNotify (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  );

EFI_STATUS
GetSystemSerial (
  VOID
  );

EFI_STATUS
GetBatterySerial (
  VOID
  );

EFI_STATUS
SyncComputraceStateWithSetup (
  VOID
  );

#endif
