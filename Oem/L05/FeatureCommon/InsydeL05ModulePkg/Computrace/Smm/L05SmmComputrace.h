/** @file
  SPEC    : Computrace implement requirement v1.2.doc

;******************************************************************************
;* Copyright (c) 2013 - 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_SMM_COMPUTRACE_DRIVER_H_
#define _L05_SMM_COMPUTRACE_DRIVER_H_

#include <SmiTable.h>
#include <OemSwSmi.h>
#include <L05Config.h>
#include <FlashRegionLayout.h>

#include <Library/SmmServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/BaseLib.h>
#include <Library/FeatureLib/OemSvcSmmComputrace.h>

#include <Protocol/SmmCpu.h>
#include <Protocol/SmmFwBlockService.h>
#include <Protocol/L05ComputraceInfo.h>
#include <Protocol/L05SmmSwSmiInterface.h>


EFI_SMM_CPU_PROTOCOL  *mSmmCpu;

//
// Global variables
//
#define OEM_L05_COMPUTRACE_SET_STATE_ENABLE_FUNCTION    (UINT32)(0x534D8500 + (UINT8)L05_SECURITY_SW_SMI)
#define OEM_L05_COMPUTRACE_SET_STATE_DISABLE_FUNCTION   (UINT32)(0x534D8700 + (UINT8)L05_SECURITY_SW_SMI)
#define OEM_L05_COMPUTRACE_MAX_RETRY                  (UINT32)0x05
#define OEM_L05_COMPUTRACE_SET_STATE_SUCCESS          (UINT32)0x00
#define OEM_L05_COMPUTRACE_SET_STATE_NOT_SUPPORTED    (UINT32)(1 << 31)
#define OEM_L05_COMPUTRACE_SET_STATE_ALREADY_ENABLED  (UINT32)(1 << 30)
#define OEM_L05_COMPUTRACE_SET_STATE_ALREADY_DISABLED (UINT32)(1 << 29)
#define OEM_L05_COMPUTRACE_SET_STATE_KEY_NOT_MATCH    (UINT32)(1 << 28)

#define OEM_L05_COMPUTRACE_GET_STATE_FUNCTION           (UINT32)(0x534D8800 + (UINT8)L05_SECURITY_SW_SMI)
#define OEM_L05_COMPUTRACE_GET_STATE_ENABLED          (UINT32)(1 << 30)
#define OEM_L05_COMPUTRACE_GET_STATE_DISABLED         (UINT32)0x00

EFI_STATUS
L05SmmComputraceEntryPoint (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  );

EFI_STATUS
EFIAPI
L05SmmComputraceGetSetStateCallback (
  IN  UINTN                             CpuNum
  );

EFI_STATUS
ReadDwordRegister (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             Width,
  IN  UINTN                             CpuNum,
  OUT VOID                              *RegisterData
  );

EFI_STATUS
WriteDwordRegister (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             Width,
  IN  UINTN                             CpuNum,
  IN  VOID                              *RegisterData
  );

#endif
