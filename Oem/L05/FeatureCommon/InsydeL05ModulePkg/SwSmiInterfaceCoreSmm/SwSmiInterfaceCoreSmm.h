/** @file
  Software SMI Interface Core for registering more than one callback functions with one SMI port number.

;******************************************************************************
;* Copyright (c) 2013 - 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SW_SMI_INTERFACE_CORE_SMM_H_
#define _SW_SMI_INTERFACE_CORE_SMM_H_

#include <Uefi.h>
#include <OemSwSmi.h>  // EFI_L05_TEST_SW_SMI

//
// Libraries
//
#include <Library/SmmServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseLib.h>  // InitializeListHead

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/SmmCpu.h>  // EFI_SMM_CPU_PROTOCOL

//
// Produced Protocols
//
#include <Protocol/L05SmmSwSmiInterface.h>

//
// Guids
//

//
// Driver Version
//

//
// Protocol instances
//

//
// Include files with function prototypes
//
#include "RegisterCallbackFunction.h"
#include "UnRegisterCallbackFunction.h"

//
// For record Callback Function Information
//
typedef struct {
  LIST_ENTRY                             LinkList;
  L05_CALLBACK_TYPE_PRIORITY             CallbackType;
  UINTN                                  FunctionAddr;
} SW_SMI_CALLBACK_FUNCTION_INFO;

//
// For record SW SMI Interface Instance and Callback Function Information
//
typedef struct {
  LIST_ENTRY                             LinkList;
  UINTN                                  SwSmiNumber;
  EFI_HANDLE                             SwSmiHandle;
  LIST_ENTRY                             FunInfoLinkList;
} SW_SMI_INTERFACE_INSTANCE;

//
// Extern resource for RegisterCallbackFunction.c, UnRegisterCallbackFunction.c
//
extern LIST_ENTRY                        mInstanceList;
extern EFI_SMM_SW_DISPATCH2_PROTOCOL     *mSwDispatch;
extern EFI_SMM_CPU_PROTOCOL              *mSmmCpu;
extern EFI_SMM_SW_REGISTER2              mOrgRegister;
extern EFI_SMM_SW_UNREGISTER2            mOrgUnRegister;

#endif
