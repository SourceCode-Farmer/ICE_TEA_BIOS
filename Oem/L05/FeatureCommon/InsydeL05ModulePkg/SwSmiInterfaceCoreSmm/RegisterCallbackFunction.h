/** @file
  Software SMI Interface Core for
    1. Registering more than one callback functions with one SMI port number.
    2. Hook function for Register() from EFI_SMM_SW_DISPATCH2_PROTOCOL.
    3. Common SW SMI callback function entry point for dispatch all SW SMI Callback function

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _REGISTER_CALLBACK_FUNCTION_H_
#define _REGISTER_CALLBACK_FUNCTION_H_

EFI_STATUS
RegisterHook (
  IN  CONST EFI_SMM_SW_DISPATCH2_PROTOCOL          *This,
  IN        EFI_SMM_HANDLER_ENTRY_POINT2           DispatchFunction,
  IN  OUT   EFI_SMM_SW_REGISTER_CONTEXT            *RegisterContext,
  OUT       EFI_HANDLE                             *DispatchHandle
  );

EFI_STATUS
RegisterCallbackFunction (
  IN EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL         *This,
  IN UINTN                                         SwSmiNum,
  IN L05_CALLBACK_TYPE_PRIORITY                    CallbackType,
  IN L05_SW_SMI_CALLBACK_FUNCTION                  CallbackFunction
  );

#endif
