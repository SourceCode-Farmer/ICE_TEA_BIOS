/** @file
  Software SMI Interface Core for
    1. Registering more than one callback functions with one SMI port number.
    2. Hook function for Register() from EFI_SMM_SW_DISPATCH2_PROTOCOL.
    3. Common SW SMI callback function entry point for dispatch all SW SMI Callback function

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "SwSmiInterfaceCoreSmm.h"

/**
  Common SW SMI callback function entry point for dispatch all SW SMI Callback function

  @param  DispatchHandle                 The unique handle assigned to this handler by SmiHandlerRegister().
  @param  Context                        Points to an optional handler context which was specified when the handler was registered.
  @param  CommBuffer                     A pointer to a collection of data in memory that will be conveyed from a non-SMM environment into an SMM environment.
  @param  CommBufferSize                 The size of the CommBuffer.

  @retval EFI_SUCCESS                    The operation completed successfully.
  @retval Others                         An unexpected error occurred by referring mSmmCpu->ReadSaveState().
**/
EFI_STATUS
SwSmiInterfaceCallbackCore (
  IN EFI_HANDLE                          Handle,
  IN CONST VOID                          *Context,
  IN OUT VOID                            *CommBuffer,
  IN OUT UINTN                           *CommBufferSize
  )
{
  EFI_STATUS                             Status;
  LIST_ENTRY                             *Link;
  LIST_ENTRY                             *Link2;
  SW_SMI_INTERFACE_INSTANCE              *SwSmiInstancePtr;
  SW_SMI_CALLBACK_FUNCTION_INFO          *CallbackFunInfo;
  L05_SW_SMI_CALLBACK_FUNCTION           SwSmiCallbackFunction;
  EFI_SMM_HANDLER_ENTRY_POINT2           SmmHandlerEntryPoint;
  UINTN                                  CpuIndex;
  UINT32                                 Eax;

  Status = EFI_SUCCESS;
  Eax    = 0;

  //
  // Find out which CPU core triggered the SW SMI
  //
  for (CpuIndex = 0; CpuIndex < gSmst->NumberOfCpus; CpuIndex++) {

    Status = mSmmCpu->ReadSaveState (
                        mSmmCpu,
                        sizeof (UINT32),
                        EFI_SMM_SAVE_STATE_REGISTER_RAX,
                        CpuIndex,
                        &Eax
                        );

    if (!EFI_ERROR (Status) &&
        (Eax & 0xFF) == (UINT32) ((EFI_SMM_SW_REGISTER_CONTEXT *) Context)->SwSmiInputValue) {

      //
      // CPU found!
      //
      break;
    }
  }

  //
  // Exit if there is no matched CPU Core
  //
  if (EFI_ERROR (Status) || CpuIndex > gSmst->NumberOfCpus) {
    return Status;
  }

  //
  // Dispatch all registered SW SMI callback function
  //
  for (Link = GetFirstNode (&mInstanceList);
       !IsNull (&mInstanceList, Link);
       Link = GetNextNode (&mInstanceList, Link)) {

    SwSmiInstancePtr = BASE_CR (Link, SW_SMI_INTERFACE_INSTANCE, LinkList);

    //
    // Find matched SW SMI Number
    //
    if (SwSmiInstancePtr->SwSmiNumber == ((EFI_SMM_SW_REGISTER_CONTEXT *) Context)->SwSmiInputValue) {

      //
      // Dispatch callback functions
      //
      for (Link2 = GetFirstNode (&SwSmiInstancePtr->FunInfoLinkList);
           !IsNull (&SwSmiInstancePtr->FunInfoLinkList, Link2);
           Link2 = GetNextNode (&SwSmiInstancePtr->FunInfoLinkList, Link2)) {

        CallbackFunInfo = BASE_CR (Link2, SW_SMI_CALLBACK_FUNCTION_INFO, LinkList);

        //
        // Check the callback function be registered by Caller Type 1 or 2, then execute.
        //
        if (CallbackFunInfo->CallbackType != InvalidCallbackType) {

          SwSmiCallbackFunction = (L05_SW_SMI_CALLBACK_FUNCTION) CallbackFunInfo->FunctionAddr;

          Status = SwSmiCallbackFunction (CpuIndex);

          if (!EFI_ERROR (Status)) {

            //
            // Dispatch Done
            //
            break;
          }
        } else {

          SmmHandlerEntryPoint = (EFI_SMM_HANDLER_ENTRY_POINT2) CallbackFunInfo->FunctionAddr;

          Status = SmmHandlerEntryPoint (
                     Handle,
                     Context,
                     CommBuffer,
                     CommBufferSize
                     );

          if (PcdGetBool (PcdL05KeepSmmSwDispatch2DefaultBehavior)) {
            return Status;
          }
        }
      }

      //
      // Exit SW SMI Number search loop after dispatch callback function done.
      //
      break;
    }
  }

  return Status;
}

/**
  Register Hook Core function from End user view
    Caller Type 1. Register() from EFI_SMM_SW_DISPATCH2_PROTOCOL -> RegisterHook()
    Caller Type 2. RegisterCallbackFunction() from EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL

  @param  This                           Pointer to the EFI_SMM_SW_DISPATCH2_PROTOCOL instance.
  @param  RegisterContext                Pointer to the dispatch function's context.
                                         The caller fills this context in before calling the register function to indicate to the register
                                         function which Software SMI input value the dispatch function should be invoked for.
  @param  CallbackType                   The type from enum L05_CALLBACK_TYPE_PRIORITY
  @param  DispatchFunctionAddr           Function Memory Address to register.
  @param  DispatchHandle                 Handle generated by the dispatcher to track the function instance.

  @retval EFI_SUCCESS                    The operation completed successfully.
  @retval EFI_ALREADY_STARTED            The DispatchFunctionAddr already be registered from Caller Type 2.
  @retval EFI_INVALID_PARAMETER          The RegisterContext already be registered from Caller Type 1.
  @retval EFI_OUT_OF_RESOURCES           Allocate memory resource fail.
  @retval Others                         An unexpected error occurred by referring Register() from EFI_SMM_SW_DISPATCH2_PROTOCOL.
**/
EFI_STATUS
RegisterHookCore (
  IN  CONST EFI_SMM_SW_DISPATCH2_PROTOCOL          *This,
  IN  OUT   EFI_SMM_SW_REGISTER_CONTEXT            *RegisterContext,
  IN        L05_CALLBACK_TYPE_PRIORITY             CallbackType,
  IN        UINTN                                  DispatchFunctionAddr,
  OUT       EFI_HANDLE                             *DispatchHandle
  )
{
  EFI_STATUS                             Status;
  LIST_ENTRY                             *Link;
  LIST_ENTRY                             *Link2;
  SW_SMI_INTERFACE_INSTANCE              *SwSmiInstancePtr;
  SW_SMI_CALLBACK_FUNCTION_INFO          *CallbackFunInfoPtr;
  SW_SMI_INTERFACE_INSTANCE              *SwSmiInstanceData;
  SW_SMI_CALLBACK_FUNCTION_INFO          *CallbackFunInfoData;
  UINTN                                  RegisterSwSmiNumber;
  BOOLEAN                                BeRegistered;

  Status              = EFI_SUCCESS;
  BeRegistered        = FALSE;
  SwSmiInstancePtr    = NULL;
  SwSmiInstanceData   = NULL;
  CallbackFunInfoPtr  = NULL;
  CallbackFunInfoData = NULL;
  RegisterSwSmiNumber = RegisterContext->SwSmiInputValue;

  //
  // 1. Check SMI Number and callback function be registered or not
  //
  for (Link = GetFirstNode (&mInstanceList);
       !IsNull (&mInstanceList, Link);
       Link = GetNextNode (&mInstanceList, Link)) {

    SwSmiInstancePtr = BASE_CR (Link, SW_SMI_INTERFACE_INSTANCE, LinkList);

    if (SwSmiInstancePtr->SwSmiNumber == RegisterSwSmiNumber) {

      for (Link2 = GetFirstNode (&SwSmiInstancePtr->FunInfoLinkList);
           !IsNull (&SwSmiInstancePtr->FunInfoLinkList, Link2);
           Link2 = GetNextNode (&SwSmiInstancePtr->FunInfoLinkList, Link2)) {

        CallbackFunInfoPtr = BASE_CR (Link2, SW_SMI_CALLBACK_FUNCTION_INFO, LinkList);

        //
        // Skip duplicate Function Address from Caller Type 2.
        //
        if (CallbackFunInfoPtr->FunctionAddr == (UINTN) DispatchFunctionAddr) {
          return EFI_ALREADY_STARTED;
        }

        //
        // Skip duplicate SW SMI Number from Caller Type 1.
        //   Reserve original behavior of Register() from EFI_SMM_SW_DISPATCH2_PROTOCOL.
        //   It will depend on different Chipset code.
        //
        if (PcdGetBool (PcdL05KeepSmmSwDispatch2DefaultBehavior)) {
          if (CallbackType == InvalidCallbackType && CallbackFunInfoPtr->CallbackType == InvalidCallbackType) {
              return EFI_INVALID_PARAMETER;
          }
        }
      }

      BeRegistered = TRUE;
      *DispatchHandle = SwSmiInstancePtr->SwSmiHandle;

      break;
    }
  }

  //
  // 2. Prepare Callback funnction information
  //
  CallbackFunInfoData = AllocateZeroPool (sizeof (SW_SMI_CALLBACK_FUNCTION_INFO));

  if (CallbackFunInfoData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  CallbackFunInfoData->CallbackType = CallbackType;
  CallbackFunInfoData->FunctionAddr = DispatchFunctionAddr;

  //
  // 3. Store Callback funnction information into SMI Interface Instance
  //
  if (BeRegistered) {

    //
    // Add Callback funnction data into Link List by order of Callback Type
    //
    for (Link2 = GetFirstNode (&SwSmiInstancePtr->FunInfoLinkList);
         !IsNull (&SwSmiInstancePtr->FunInfoLinkList, Link2);
         Link2 = GetNextNode (&SwSmiInstancePtr->FunInfoLinkList, Link2)) {

      CallbackFunInfoPtr = BASE_CR (Link2, SW_SMI_CALLBACK_FUNCTION_INFO, LinkList);

      if (CallbackFunInfoData->CallbackType < CallbackFunInfoPtr->CallbackType) {

        InsertTailList (Link2, &CallbackFunInfoData->LinkList);
        break;
      }

      if (CallbackFunInfoData->CallbackType >= CallbackFunInfoPtr->CallbackType &&
          IsNodeAtEnd (&SwSmiInstancePtr->FunInfoLinkList, Link2)) {

        InsertHeadList (Link2, &CallbackFunInfoData->LinkList);
        break;
      }
    }
  } else {

    //
    // Register common callback function if not register yet
    //
    Status = mOrgRegister (
               This,
               SwSmiInterfaceCallbackCore,
               RegisterContext,
               DispatchHandle
               );

    if (EFI_ERROR (Status)) {
      FreePool (CallbackFunInfoPtr);
      return Status;
    }

    SwSmiInstanceData = AllocateZeroPool (sizeof (SW_SMI_INTERFACE_INSTANCE));

    if (SwSmiInstanceData == NULL) {

      //
      // Un-register if occurring unexpected error
      //
      Status = mOrgUnRegister (
                 This,
                 DispatchHandle
                 );

      return EFI_OUT_OF_RESOURCES;
    }

    InsertHeadList (&mInstanceList, &SwSmiInstanceData->LinkList);
    SwSmiInstanceData->SwSmiNumber = RegisterContext->SwSmiInputValue;
    SwSmiInstanceData->SwSmiHandle = *DispatchHandle;

    InitializeListHead (&SwSmiInstanceData->FunInfoLinkList);
    InsertHeadList (&SwSmiInstanceData->FunInfoLinkList, &CallbackFunInfoData->LinkList);
  }

  return Status;
}

/**
  Hook function for Register() from EFI_SMM_SW_DISPATCH2_PROTOCOL. (Caller Type 1)

  @param  This                 Pointer to the EFI_SMM_SW_DISPATCH2_PROTOCOL instance.
  @param  DispatchFunction     Function to register for handler when the specified software SMI is generated.
  @param  RegisterContext      Pointer to the dispatch function's context.
                               The caller fills this context in before calling the register function to indicate to the register
                               function which Software SMI input value the dispatch function should be invoked for.
  @param  DispatchHandle       Handle generated by the dispatcher to track the function instance.

  @retval EFI_SUCCESS          The operation completed successfully.
  @retval Others               An unexpected error occurred by referring RegisterHookCore().
**/
EFI_STATUS
RegisterHook (
  IN  CONST EFI_SMM_SW_DISPATCH2_PROTOCOL          *This,
  IN        EFI_SMM_HANDLER_ENTRY_POINT2           DispatchFunction,
  IN  OUT   EFI_SMM_SW_REGISTER_CONTEXT            *RegisterContext,
  OUT       EFI_HANDLE                             *DispatchHandle
  )
{
  EFI_STATUS                             Status;

  //
  // Pass InvalidCallbackType to RegisterHookCore() for determine the request is from Caller Type 1
  //
  Status = RegisterHookCore (
             This,
             RegisterContext,
             InvalidCallbackType,
             (UINTN) DispatchFunction,
             DispatchHandle
             );

  return Status;
}

/**
  Register SW SMI callback function Point by SMI port number and Callback Type. (Caller Type 2)

  @param  This                           Point to EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL
  @param  SwSmiPort                      SMI port number
  @param  CallbackType                   The type from enum L05_CALLBACK_TYPE_PRIORITY
  @param  CallbackFunction               Point to Callback Function

  @retval EFI_SUCCESS                    The operation completed successfully.
  @retval EFI_INVALID_PARAMETER          The CallbackType is not valid or CallbackFunction is a NULL point.
  @retval Others                         An unexpected error occurred by referring RegisterHookCore().
**/
EFI_STATUS
RegisterCallbackFunction (
  IN EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL         *This,
  IN UINTN                                         SwSmiNum,
  IN L05_CALLBACK_TYPE_PRIORITY                    CallbackType,
  IN L05_SW_SMI_CALLBACK_FUNCTION                  CallbackFunction
  )
{
  EFI_STATUS                             Status;
  EFI_SMM_SW_REGISTER_CONTEXT            SwContext;
  EFI_HANDLE                             SwHandle;

  SwHandle = NULL;

  //
  // Check Callback Type is not invalid or Callback function address is not NULL
  //
  if (CallbackType >= InvalidCallbackType || CallbackFunction == NULL) {

    return EFI_INVALID_PARAMETER;
  }

  //
  // Store Software SMI number into register context
  //
  SwContext.SwSmiInputValue = SwSmiNum;

  Status = RegisterHookCore (
             mSwDispatch,
             &SwContext,
             CallbackType,
             (UINTN) CallbackFunction,
             &SwHandle
             );

  return Status;
}
