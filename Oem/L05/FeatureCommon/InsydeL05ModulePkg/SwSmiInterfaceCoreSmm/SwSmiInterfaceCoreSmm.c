/** @file
  Software SMI Interface Core for registering more than one callback functions with one SMI port number.

;******************************************************************************
;* Copyright (c) 2013 - 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "SwSmiInterfaceCoreSmm.h"

//
// Main Link List SW SMI Interface Instance
//
LIST_ENTRY                               mInstanceList;

//
// Reserve service of SMM SW Dispatch2 and SMM CPU Protocol
//
EFI_SMM_SW_DISPATCH2_PROTOCOL            *mSwDispatch = NULL;
EFI_SMM_CPU_PROTOCOL                     *mSmmCpu = NULL;

//
// Reserve function point of hook function Register() and UnRegister()
//
EFI_SMM_SW_REGISTER2                     mOrgRegister = NULL;
EFI_SMM_SW_UNREGISTER2                   mOrgUnRegister = NULL;

/**
  Dummy SW SMI callback function

  @param  DispatchHandle                 The unique handle assigned to this handler by SmiHandlerRegister().
  @param  Context                        Points to an optional handler context which was specified when the handler was registered.
  @param  CommBuffer                     A pointer to a collection of data in memory that will be conveyed from a non-SMM environment into an SMM environment.
  @param  CommBufferSize                 The size of the CommBuffer.

  @retval EFI_SUCCESS                    The operation completed successfully.
**/
EFI_STATUS
EFIAPI
DummyCallback (
  IN EFI_HANDLE                          Handle,
  IN CONST VOID                          *Context,
  IN OUT VOID                            *CommBuffer,
  IN OUT UINTN                           *CommBufferSize
  )
{
  return EFI_SUCCESS;
}

/**
  This is the event callback function for locate SMM CPU protocol

  @param Protocol              Points to the protocol's unique identifier.
  @param Interface             Points to the interface instance.
  @param Handle                The handle on which the interface was installed.

  @retval EFI_SUCCESS          The operation completed successfully.
  @retval Others               An unexpected error occurred by referring gSmst->SmmLocateProtocol().
**/
EFI_STATUS
EFIAPI
SmmCpuProtocolCallBack (
  IN CONST EFI_GUID                      *Protocol,
  IN VOID                                *Interface,
  IN EFI_HANDLE                          Handle
  )
{
  EFI_STATUS                             Status;

  //
  // Locate SMM CPU Protocol which is used later to read CPU Save States
  //
  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmCpuProtocolGuid,
                    NULL,
                    &mSmmCpu
                    );

  if (EFI_ERROR (Status)) {

    return Status;
  }

  return Status;
}

/**
  This is the event callback function for hook SMM SW Dispatch2 protocol

  @param Protocol              Points to the protocol's unique identifier.
  @param Interface             Points to the interface instance.
  @param Handle                The handle on which the interface was installed.

  @retval EFI_SUCCESS          The operation completed successfully.
  @retval Others               An unexpected error occurred by referring gSmst->SmmLocateProtocol().
**/
EFI_STATUS
EFIAPI
HookSmmSwDispatch2CallBack (
  IN CONST EFI_GUID                      *Protocol,
  IN VOID                                *Interface,
  IN EFI_HANDLE                          Handle
  )
{
  EFI_STATUS                             Status;
  EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL  *L05SwSmiPtr;
  EFI_HANDLE                             L05SwSmiHandle;
  EFI_SMM_SW_REGISTER_CONTEXT            SwContext;
  EFI_HANDLE                             SwHandle1;
  EFI_HANDLE                             SwHandle2;
  BOOLEAN                                HookFlag;

  Status         = EFI_SUCCESS;
  L05SwSmiPtr    = NULL;
  L05SwSmiHandle = NULL;
  SwHandle1      = NULL;
  SwHandle2      = NULL;
  HookFlag       = TRUE;

  //
  // Get the Sw Dispatch Protocol, if fail then exit
  //
  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmSwDispatch2ProtocolGuid,
                    NULL,
                    &mSwDispatch
                    );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Check L05 SMM SW SMI Interfadce Protocol be installed, if success then exit
  //
  Status = gSmst->SmmLocateProtocol (
                    &gEfiL05SmmSwSmiInterfaceProtocolGuid,
                    NULL,
                    &L05SwSmiPtr
                    );

  if (!EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Initialize L05 SW SMI Interface Protocol
  //
  L05SwSmiPtr = AllocateZeroPool (sizeof (EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL));

  if (L05SwSmiPtr == NULL) {

    return EFI_OUT_OF_RESOURCES;
  }

  L05SwSmiPtr->RegisterCallbackFunction   = RegisterCallbackFunction;
  L05SwSmiPtr->UnRegisterCallbackFunction = UnRegisterCallbackFunction;

  Status = gSmst->SmmInstallProtocolInterface (
                    &L05SwSmiHandle,
                    &gEfiL05SmmSwSmiInterfaceProtocolGuid,
                    EFI_NATIVE_INTERFACE,
                    L05SwSmiPtr
                    );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Initialize SW SMI Interface Instance
  //
  InitializeListHead (&mInstanceList);

  //
  // Check if the same SW SMI number can be registered repeatedly by original dispatcher
  // If supported repeated registration than no need to hook original Register/UnRegister function
  //
  mOrgRegister   = mSwDispatch->Register;
  mOrgUnRegister = mSwDispatch->UnRegister;

  SwContext.SwSmiInputValue = EFI_L05_TEST_SW_SMI;

  Status = mOrgRegister (
             mSwDispatch,
             DummyCallback,
             &SwContext,
             &SwHandle1
             );

  Status = mOrgRegister (
             mSwDispatch,
             DummyCallback,
             &SwContext,
             &SwHandle2
             );
						  
  if (SwHandle1 != NULL && SwHandle2 != NULL) {
    HookFlag = FALSE;
  }

  if (SwHandle1 != NULL) {
    Status = mOrgUnRegister (
               mSwDispatch,
               SwHandle1
               );
  }

  if (SwHandle2 != NULL) {
    Status = mOrgUnRegister (
               mSwDispatch,
               SwHandle2
               );
  }

  //
  // Hook Register() and UnRegister()
  //
  if (HookFlag) {

    mSwDispatch->Register   = RegisterHook;
    mSwDispatch->UnRegister = UnRegisterHook;
  }

  return EFI_SUCCESS;
}

/**
  This is the declaration of an EFI image entry point. This entry point is
  the same for UEFI Applications, UEFI OS Loaders, and UEFI Drivers including
  both device drivers and bus drivers.

  @param  ImageHandle          The firmware allocated handle for the UEFI image.
  @param  SystemTable          A pointer to the EFI System Table.

  @retval EFI_SUCCESS          The operation completed successfully.
  @retval Others               An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
SwSmiInterfaceCoreSmmDriverEntryPoint (
  IN EFI_HANDLE                          ImageHandle,
  IN EFI_SYSTEM_TABLE                    *SystemTable
  )
{
  EFI_STATUS                             Status;
  VOID                                   *Registration;

  Status       = EFI_SUCCESS;
  Registration = NULL;

  //
  // Register Event for hook SMM SW Dispatch2 protocol
  //
  Status = gSmst->SmmRegisterProtocolNotify (
                    &gEfiSmmSwDispatch2ProtocolGuid,
                    HookSmmSwDispatch2CallBack,
                    &Registration
                    );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Locate SMM CPU Protocol which is used later to read CPU Save States
  //
  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmCpuProtocolGuid,
                    NULL,
                    &mSmmCpu
                    );

  if (EFI_ERROR (Status)) {

    //
    // Register Event for locate SMM CPU Protocol
    //
    Registration = NULL;

    Status = gSmst->SmmRegisterProtocolNotify (
                      &gEfiSmmCpuProtocolGuid,
                      SmmCpuProtocolCallBack,
                      &Registration
                      );

    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  return Status;
}
