/** @file
  Software SMI Interface Core for
    1. Unregistering more than one callback functions with one SMI port number.
    2. Hook function for UnRegister() from EFI_SMM_SW_DISPATCH2_PROTOCOL.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _UN_REGISTER_CALLBACK_FUNCTION_H_
#define _UN_REGISTER_CALLBACK_FUNCTION_H_

EFI_STATUS
UnRegisterHook (
  IN CONST EFI_SMM_SW_DISPATCH2_PROTOCOL           *This,
  IN       EFI_HANDLE                              DispatchHandle
  );

EFI_STATUS
UnRegisterCallbackFunction (
  IN EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL         *This,
  IN UINTN                                         SwSmiNum,
  IN L05_SW_SMI_CALLBACK_FUNCTION                  CallbackFunction
  );

#endif
