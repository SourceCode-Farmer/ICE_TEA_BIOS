/** @file
  Software SMI Interface Core for
    1. Unregistering more than one callback functions with one SMI port number.
    2. Hook function for UnRegister() from EFI_SMM_SW_DISPATCH2_PROTOCOL.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "SwSmiInterfaceCoreSmm.h"

/**
  Un-register SW SMI callback function Point by SMI port number from End user view
    Caller Type 1. UnRegister() from EFI_SMM_SW_DISPATCH2_PROTOCOL -> UnRegisterHook()
    Caller Type 2. UnRegisterCallbackFunction() from EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL

  @param  This                           Point to EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL
  @param  SwSmiPort                      SMI port number
  @param  CallbackFunction               Point to Callback Function
  @param  DispatchHandle                 Handle of dispatch function to deregister.

  @retval EFI_SUCCESS                    The operation completed successfully.
  @retval Others                         An unexpected error occurred by referring UnRegister() from EFI_SMM_SW_DISPATCH2_PROTOCOL.
**/
EFI_STATUS
UnRegisterHookCore (
  IN CONST EFI_SMM_SW_DISPATCH2_PROTOCOL           *This,
  IN       UINTN                                   SwSmiNum,
  IN       L05_SW_SMI_CALLBACK_FUNCTION            CallbackFunction,
  IN       EFI_HANDLE                              DispatchHandle
  )
{
  EFI_STATUS                             Status;
  BOOLEAN                                SearchByHandle;
  LIST_ENTRY                             *Link;
  LIST_ENTRY                             *Link2;
  SW_SMI_INTERFACE_INSTANCE              *SwSmiInstancePtr;
  BOOLEAN                                BeRegistered;
  SW_SMI_CALLBACK_FUNCTION_INFO          *CallbackFunInfo;
  UINT32                                 FunctionCount;
  BOOLEAN                                RemoveLink;
  EFI_HANDLE                             SwHandle;

  Status           = EFI_SUCCESS;
  SearchByHandle   = TRUE;
  BeRegistered     = FALSE;
  SwSmiInstancePtr = NULL;
  FunctionCount    = 0;
  RemoveLink       = FALSE;
  SwHandle         = NULL;

  //
  // 1. Check callback function is not NULL to determine Caller Type
  //
  if (DispatchHandle == NULL) {
    SearchByHandle = FALSE;
  }

  //
  // 2. Check SMI Number be registered, Callback function, Callback Type and dispatch Handle be matched
  //
  for (Link = GetFirstNode (&mInstanceList);
       !IsNull (&mInstanceList, Link);
       Link = GetNextNode (&mInstanceList, Link)) {

    SwSmiInstancePtr = BASE_CR (Link, SW_SMI_INTERFACE_INSTANCE, LinkList);

    if (SearchByHandle) {

      //
      // Caller Type 1 - Check dispatch Handle
      //
      if (SwSmiInstancePtr->SwSmiHandle == DispatchHandle) {
        BeRegistered = TRUE;
      }
    } else {

      //
      // Caller Type 2 - Check SMI Number
      //
      if (SwSmiInstancePtr->SwSmiNumber == SwSmiNum) {
        BeRegistered = TRUE;
      }
    }

    if (BeRegistered) {

      //
      // Caculate Callback function number
      //
      for (Link2 = GetFirstNode (&SwSmiInstancePtr->FunInfoLinkList);
           !IsNull (&SwSmiInstancePtr->FunInfoLinkList, Link2);
           Link2 = GetNextNode (&SwSmiInstancePtr->FunInfoLinkList, Link2)) {

        FunctionCount++;
      }

      //
      // Check Callback Function Information Link List
      //
      for (Link2 = GetFirstNode (&SwSmiInstancePtr->FunInfoLinkList);
           !IsNull (&SwSmiInstancePtr->FunInfoLinkList, Link2);
           Link2 = GetNextNode (&SwSmiInstancePtr->FunInfoLinkList, Link2)) {

        CallbackFunInfo = BASE_CR (Link2, SW_SMI_CALLBACK_FUNCTION_INFO, LinkList);

        if (SearchByHandle) {

          //
          // Caller Type 1 - Check Callback Type
          //
          if (CallbackFunInfo->CallbackType == InvalidCallbackType) {
            RemoveLink = TRUE;
          }
        } else {

          //
          // Caller Type 2 - Check Callback function
          //
          if (CallbackFunInfo->FunctionAddr == (UINTN) CallbackFunction) {
            RemoveLink = TRUE;
          }
        }

        //
        // Remove one Callback function information if matched
        //
        if (RemoveLink) {
          
          if (FunctionCount == 1) {

            //
            // Un-regiter SW SMI Interface Instance if all callback functions be removed
            //
            Status = mOrgUnRegister (
                       This,
                       SwHandle
                       );
            if (EFI_ERROR (Status)) {
              return Status;
            }
            
            SwHandle = SwSmiInstancePtr->SwSmiHandle;
            Link2 = RemoveEntryList (&CallbackFunInfo->LinkList);
            Link2 = Link2->BackLink;
            FreePool (CallbackFunInfo);

            Link = RemoveEntryList (&SwSmiInstancePtr->LinkList);
            Link = Link->BackLink;
            FreePool (SwSmiInstancePtr);
            
            break;
          } else {

            SwHandle = SwSmiInstancePtr->SwSmiHandle;
            Link2 = RemoveEntryList (&CallbackFunInfo->LinkList);
            Link2 = Link2->BackLink;
            FreePool (CallbackFunInfo);
          }

          FunctionCount--;
		  
          //
          // Reset RemoveLink to False and continue to for loop to record totaol number of Callback function
          //
          RemoveLink = FALSE;
        }
      }

      break;
    }
  }

  return EFI_SUCCESS;
}

/**
  Hook function for UnRegister() from EFI_SMM_SW_DISPATCH2_PROTOCOL. (Caller Type 1)

  @param  This                 Pointer to the EFI_SMM_SW_DISPATCH2_PROTOCOL instance.
  @param  DispatchHandle       Handle of dispatch function to deregister.

  @retval EFI_SUCCESS          The operation completed successfully.
  @retval Others               An unexpected error occurred by referring UnRegisterHookCore().
**/
EFI_STATUS
UnRegisterHook (
  IN CONST EFI_SMM_SW_DISPATCH2_PROTOCOL           *This,
  IN       EFI_HANDLE                              DispatchHandle
  )
{
  EFI_STATUS                             Status;

  //
  // Pass SwSmiNum as 0 and CallbackFunction as NULL to UnRegisterHookCore() for determine the request is from Caller Type 1
  //
  Status = UnRegisterHookCore (
             This,
             0x00,
             NULL,
             DispatchHandle
             );

  return Status;
}

/**
  Un-register SW SMI callback function Point by SMI port number. (Caller Type 2)

  @param  This                 Point to EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL
  @param  SwSmiPort            SMI port number
  @param  CallbackFunction     Point to Callback Function

  @retval EFI_SUCCESS          The operation completed successfully.
  @retval Others               An unexpected error occurred by referring UnRegisterHookCore().
**/
EFI_STATUS
UnRegisterCallbackFunction (
  IN EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL         *This,
  IN UINTN                                         SwSmiNum,
  IN L05_SW_SMI_CALLBACK_FUNCTION                  CallbackFunction
  )
{
  EFI_STATUS                             Status;

  //
  // Pass DispatchHandle as NULL to UnRegisterHookCore() for determine the request is from Caller Type 2
  //
  Status = UnRegisterHookCore (
             mSwDispatch,
             SwSmiNum,
             CallbackFunction,
             NULL
             );

  return Status;
}
