## @file
#  Platform Package Declaration file
#
#******************************************************************************
#* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  DEC_VERSION                    = 0x00010005
  PACKAGE_NAME                   = InsydeL05ModulePkg
  PACKAGE_GUID                   = F2D5EC39-7477-46DD-B6B6-2E81D21D2729
  PACKAGE_VERSION                = 0.1

[Includes]
  Include
  Include/Message

[Guids]
#//[-start-210918-Dongxu0019-add]//
  gEfiBIOSBackupSetupKeepVariableGuid    = {0x2AA8F59B, 0x1403, 0x22F5, {0x94, 0xAC, 0x7D, 0x11, 0x12, 0x1F, 0xD0, 0x20}}
#//[-end-210918-Dongxu0019-add]// 
  gL05ServicesTokenSpaceGuid             = {0x8A8E89F8, 0x1403, 0x41F5, {0x94, 0xAC, 0x7D, 0x11, 0x82, 0x1F, 0xDD, 0x20}}
  gL05PasswordVariableGuid               = {0x80C1787B, 0x507B, 0x437A, {0xB2, 0x76, 0x03, 0x93, 0x72, 0x5C, 0x55, 0xEF}}
  gEfiNovoRecoveryProtocolGuid           = {0x18382705, 0x9a01, 0x4883, {0x84, 0x92, 0x62, 0xb0, 0xee, 0x2a, 0xae, 0xe2}}
  gL05NovoKeyInfoHobGuid                 = {0xAE7D8822, 0x9512, 0x4500, {0xAA, 0x4B, 0x9D, 0x04, 0x2E, 0x6A, 0x76, 0x5B}}
  gEfiL05OneKeyRecoveryFromNovoButtonGuid= {0x4f1572c7, 0x8086, 0x42b0, {0xb4, 0xb7, 0x20, 0x76, 0x4b, 0xde, 0x29, 0x29}}
#_Start_L05_GRAPHIC_UI_ENABLE
  gL05NovoMenuFormSetGuid                = {0xA6A8C55C, 0x455C, 0x212E, {0x08, 0xD3, 0x05, 0xA0, 0x5E, 0x81, 0x12, 0x52}}
#_End_L05_GRAPHIC_UI_ENABLE
  gL05UefiDiagnosticsAppGuid             = {0x2AA8F59B, 0x102D, 0x4096, {0x8F, 0xCD, 0xB6, 0x44, 0x22, 0xC6, 0x37, 0xB5}}
  gL05SecureSuiteAppGuid                 = {0xEA8C1DE7, 0x6E73, 0x44CB, {0x9E, 0x1B, 0x9E, 0x4D, 0xA2, 0xE9, 0x6D, 0x52}}
  ## Include/Guid/L05ConfigurationVariable.h
  gEfiL05ConfigurationVariableGuid       = {0x74d69abb, 0x57c3, 0x4d7f, {0xbf, 0xb4, 0x26, 0xa2, 0x54, 0x96, 0x10, 0xf1}}
  gEfiL05OneKeyRecoveryBiosDataGuid      = {0x64448f46, 0x6e23, 0x4288, {0x93, 0x49, 0xfd, 0xd8, 0x87, 0xc4, 0x0d, 0xe1}}
  gEfiL05LenovoBootOptionGuid            = {0x146b234d, 0x4052, 0x4e07, {0xb3, 0x26, 0x11, 0x22, 0x0f, 0x8e, 0x1f, 0xe8}}
  ## Include/Guid/L05WirelessDeviceSupportVariable.h
  gL05WirelessDeviceSupportVariableGuid  = {0x74b00bd9, 0x805a, 0x4d61, {0xb5, 0x1f, 0x43, 0x26, 0x81, 0x23, 0xd1, 0x13}}
  ## Include/Guid/L05StopLegacyToEfiProcess.h
  gL05StopLegacyToEfiProcessGuid         = {0x392cf1de, 0x75fd, 0x11e4, {0x99, 0x76, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}
  ## Include/Guid/L05CustomizeMultiLogo.h
  gL05CustomizeMultiLogoGuid             = {0x9bcf40d7, 0xe817, 0x409a, {0xbc, 0x2b, 0x7b, 0x51, 0x0b, 0x89, 0x82, 0xb3}}
  ## Include/Guid/L05BackupSetupItemVariable.h
  gEfiL05BackupSetupItemVariableGuid     = {0xdeef453b, 0x1ac9, 0x44b6, {0xbc, 0x8a, 0x96, 0x5a, 0x8b, 0x68, 0x86, 0x1d}}
  gEfiL05LegacyToEfiCommunicationGuid    = {0x9fb1db48, 0x0874, 0x4ef9, {0x97, 0xc0, 0xbb, 0x04, 0x35, 0xed, 0xec, 0xe9}}
  ## Include/Guid/L05WmiSetupUnderOsSetupVariable.h
  gL05WmiSetupUnderOsSetupVariableGuid   = {0xa93bb445, 0x9e02, 0x4a13, {0xa7, 0xa9, 0x1c, 0x35, 0xa8, 0x05, 0x25, 0xbf}}
  ## Include/Guid/L05EspCustomizePostLogoInfoVariable.h
  gEfiL05EspCustomizePostLogoInfoVariableGuid  = {0x871455D0, 0x5576, 0x4FB8, {0x98, 0x65, 0xAF, 0x08, 0x24, 0x46, 0x3B, 0x9E}}
  ## Include/Guid/L05EspCustomizePostLogoVcmVariable.h
  gEfiL05EspCustomizePostLogoVcmVariableGuid  = {0x871455D1, 0x5576, 0x4FB8, {0x98, 0x65, 0xAF, 0x08, 0x24, 0x46, 0x3C, 0x9F}}
  ## Include/Guid/L05NaturalFileGuardVariable.h
  gEfiL05NaturalFileGuardVariableGuid    = {0xe2c5a81a, 0x4f05, 0x477c, {0xba, 0x4e, 0x49, 0xbd, 0xd5, 0x75, 0xe9, 0xae}}
  ## Include/Guid/L05VariableExist.h
  gEfiL05VariableExistGuid               = {0x5fda220c, 0x9ddc, 0x4588, {0x9d, 0xa7, 0x34, 0x94, 0x6e, 0x00, 0xbf, 0xad}}
  ## Include/Guid/L05SetupConfig.h
  gEfiL05HddPasswordConfigGuid           = {0xe6560c55, 0x28a7, 0x4ad8, {0xa8, 0x7b, 0xe7, 0x51, 0x87, 0x19, 0x6b, 0x6e}}
  #
  # L05 variable Tool
  #
  gL05ProduceNameGuid                    = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x00}}
  gL05ProjectNameGuid                    = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x01}}
  gL05MtmGuid                            = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x02}}
  gL05SerialNumberGuid                   = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x03}}
  gL05LenovoSNGuid                       = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x04}}
  gL05UUIDGuid                           = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x05}}
  gL05BrandTypeGuid                      = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x06}}
  gL05KeyboardIdGuid                     = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x07}}
  gL05EpaIdGuid                          = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x08}}
  gL05FunctionFlagGuid                   = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x09}}
  gL05CustomerGuid                       = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x0a}}
  gL05FamliyNameGuid                     = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x0b}}
  gL05AssetTagGuid                       = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x0c}}
  gL05SkuNumberGuid                      = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x0d}}
  gL05OsLicenseInfoGuid                  = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x00, 0x0e}}
  gL05OA3MsdmDataGuid                    = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x01, 0x00}}
  gL05OA2AreaDataGuid                    = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x01, 0x01}}
  gL05OA2SlicMakerGuid                   = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x01, 0x02}}
  gL05SystemSuperPasswordGuid            = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x02, 0x00}}
  gL05SystemUserPasswordGuid             = {0xC20E5755, 0x1169, 0x4C56, {0xA4, 0x8A, 0x98, 0x24, 0xAB, 0x43, 0x02, 0x01}}
  gL05LseUpLoadInfoFlagGuid              = {0xc20e5755, 0x1169, 0x4c56, {0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0a, 0x00}}
  gL05LseOnOffFlagGuid                   = {0xc20e5755, 0x1169, 0x4c56, {0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0a, 0x01}}
  gL05TpmLockGuid                        = {0xc20e5755, 0x1169, 0x4c56, {0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0b, 0x00}}
  gL05ComputraceEraseFlagGuid            = {0xc20e5755, 0x1169, 0x4c56, {0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0c, 0x00}}
  gL05BiosSelfHealingFlagGuid            = {0xc20e5755, 0x1169, 0x4c56, {0xa4, 0x8a, 0x98, 0x24, 0xab, 0x43, 0x0d, 0x00}}
  gEfiL05FliptoBootVariableGuid          = {0xd743491e, 0xf484, 0x4952, {0xa8, 0x7d, 0x8d, 0x5d, 0xd1, 0x89, 0xb7, 0x0c}}

[Ppis]

[Protocols]
  gEfiL05LvarCptGuid                     = {0xc2873663, 0xb2cb, 0x4f7a, {0x85, 0x48, 0xa6, 0x4,  0x11, 0xf5, 0xec, 0x86}}
  gEfiL05VariableProtocolGuid            = {0xbfd02359, 0x8dfe, 0x459a, {0x8b, 0x69, 0xa7, 0x3a, 0x6b, 0xaf, 0xad, 0xc0}}
  gEfiL05HotKeyServiceProtocolGuid       = {0xF3471608, 0xD1A3, 0x4A23, {0x90, 0x94, 0x36, 0xCE, 0x6D, 0x31, 0x56, 0xB1}}
  gEfiL05ServiceProtocolGuid             = {0x0697da7e, 0xb2bb, 0x49d0, {0x9d, 0xfa, 0x2,  0xeb, 0x49, 0x9f, 0x9f, 0xfd}}
  gEfiOneKeyRecoveryEvent3Guid           = {0x72eefc3e, 0xbe5e, 0x400e, {0xbc, 0x21, 0x8d, 0xfd, 0x7c, 0x6e, 0xa8, 0xd6}}
  gEfiOneKeyRecoveryEvent2Guid           = {0x9e4ac6ce, 0x4b94, 0x4e59, {0xaa, 0xe7, 0xdf, 0x96, 0x2b, 0x33, 0x9b, 0x7c}}
  gEfiOneKeyRecoveryEvent1Guid           = {0xcd8ea2e1, 0xf6b1, 0x4b92, {0x96, 0x73, 0x77, 0xea, 0xac, 0x74, 0x98, 0x8c}}
  gEfiPartTypeLrsSystemGuid              = {0xbfbfafe7, 0xa34f, 0x448a, {0x9a, 0x5b, 0x62, 0x13, 0xeb, 0x73, 0x6c, 0x22}}
  gEfiInstallOneKeyRecoveryGuid          = {0x3f7500cf, 0x9514, 0x4ca0, { 0x84, 0xa9, 0x8b, 0x53, 0xbc, 0xf5, 0x8, 0xe9}}
  ## Include/Protocol/L05BootOption.h
  gEfiL05BootOptionProtocolGuid          = {0xBEB1A081, 0xE34E, 0x46B9, {0xB6, 0x4D, 0x31, 0x95, 0x1E, 0x10, 0x05, 0x1F}}
  gEfiL05SecureBootProtocolGuid          = {0xC706D63F, 0x6CCE, 0x48AD, {0xA2, 0xB4, 0x72, 0xA5, 0xEF, 0x9E, 0x22, 0x0C}}
#_Start_L05_GRAPHIC_UI_ENABLE
  gL05AllowCrisisPwdResetTagGuid         = {0xD211DEFD, 0x40F5, 0x75F1, {0x43, 0xE3, 0x80, 0xB4, 0x02, 0x2F, 0x34, 0x7A}}
#_End_L05_GRAPHIC_UI_ENABLE
  ## Include/Protocol/L05SetupMenu.h
  gEfiL05SetupMenuProtocolGuid           = {0x51fa828f, 0xc353, 0x11e2, {0xaa, 0x49, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}
  ## Include/Protocol/L05DxeSlp20.h
  gEfiL05DxeSlp20ProtocolGuid            = {0x81c2accf, 0x5bee, 0x11e3, {0xb1, 0x78, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}
  ## Include/Protocol/L05BdsEntryServices.h
  gEfiL05BdsEntryServicesProtocolGuid    = {0xF9D36AC8, 0x2E87, 0x49CB, {0xB9, 0x87, 0x49, 0x1A, 0x39, 0xA2, 0xC9, 0x14}}
  ## Include/Protocol/L05BeforeReadyToBoot.h
  gEfiL05BeforeReadyToBootProtocolGuid   = {0x6a32f2e1, 0x57f7, 0x11e4, {0x85, 0x98, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}
  ## Include/Protocol/L05SmbiosInterface.h
  gEfiL05SmbiosInterfaceProtocolGuid     = {0xcd9fff70, 0xecb8, 0x11e4, {0x93, 0x73, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}
  ## Include/Protocol/PushButtonRecovery.h
  gEfiL05PushButtonRecoveryProtocolGuid  = {0x1427b7a7, 0xb18b, 0x4a39, {0x96, 0x5e, 0xbf, 0x6b, 0x6e, 0x3a, 0x05, 0x1a}}
  ## Include/Protocol/OkFeatureReportInterface.h
  gEfiOkFeatureReportInterfaceProtocolGuid         = {0xf16f3fb0, 0xcc74, 0x11e4, {0xad, 0x0a, 0x44, 0x6d, 0x57, 0x15, 0x53, 0xeb}}
  ## Include/Protocol/L05CapsuleUpdateInfo.h
  gEfiL05CapsuleUpdateInfoProtocolGuid   = {0x82b315e7, 0x62a2, 0x410c, {0xbf, 0x21, 0x0c, 0xce, 0xe2, 0xc9, 0xbb, 0x8c}}
  ## Include/Protocol/L05HddSpindown.h
  gEfiL05HddSpindownProtocolGuid         = {0xeab4ed49, 0x27cd, 0x4904, {0xad, 0x23, 0xaf, 0x1a, 0x4c, 0x81, 0x88, 0x46}}
  gEfiL05HddSpindownSwSmiReadyProtocolGuid         = {0x944f2298, 0x618b, 0x43b3, {0xbb, 0x70, 0xf6, 0x9e, 0x57, 0x75, 0x84, 0x63}}
  ## Include/Protocol/L05EndOfBdsConnect.h
  gL05EndOfBdsConnectProtocolGuid        = {0xf78c70cd, 0xc4f5, 0x4cf6, {0x8d, 0x00, 0x8a, 0xa1, 0x56, 0xd4, 0x00, 0x8e}}
  ## Include/Protocol/L05SmmSwSmiInterface.h
  gEfiL05SmmSwSmiInterfaceProtocolGuid   = {0x51646700, 0x7371, 0x11e3, {0xa8, 0xcc, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}
  ## Include/Protocol/L05BiosSelfHealing.h
  gEfiL05BiosSelfHealingProtocolGuid     = {0x9281745c, 0x25b7, 0x4814, {0x98, 0x71, 0x32, 0x23, 0xa3, 0xae, 0xeb, 0x1c}}
  ## Include/Protocol/L05CloudBoot.h
  gEfiL05CloudBootProtocolGuid           = {0x53e7721d, 0x246e, 0x4d0a, {0xb8, 0x46, 0xaf, 0x9c, 0xee, 0x6d, 0x62, 0xdc}}
  ## Include/Protocol/L05FlashFirmwareToolErrorProcess.h
  gEfiL05FlashFirmwareToolErrorProcessProtocolGuid = {0xfd85ac29, 0xa7b0, 0x4c92, {0x93, 0x1a, 0x56, 0x66, 0x22, 0x21, 0x6f, 0xd2}}
#[-start-220104-BAIN000083-add]#
  ## Include/Protocol/L05EndOfHdpConnect.h
  gL05EndOfHdpConnectProtocolGuid        = {0xf643e412, 0xa3ae, 0x46f1, {0xaf, 0x5a, 0xe7, 0x6e, 0xaa, 0x7c, 0x1d, 0xd3}}
#[-end-220104-BAIN000083-add]#

[PcdsFeatureFlag]
#--------------------------------------------------------------------------------
# [PcdsFeatureFlag] Non-Group: 0x1000####
#--------------------------------------------------------------------------------
  #
  # Ensure SYSTEM_CONFIGURATION.TpmHide status is consistent & correct in POST.
  # If not, reset platform.
  #
  gL05ServicesTokenSpaceGuid.PcdL05EnsureTpmStatusSupported           |TRUE |BOOLEAN|0x10000002

  #
  # Computrace Function
  #
  gL05ServicesTokenSpaceGuid.PcdL05ComputraceEnable                   |FALSE|BOOLEAN|0x10000003

  #
  # Keep SW SMM dispatcher can only register one SW SMI number
  # Or allow the dispatcher to register duplicate SW SMI number
  # This is only for Intel platform
  #
  gL05ServicesTokenSpaceGuid.PcdL05KeepSmmSwDispatch2DefaultBehavior  |TRUE|BOOLEAN|0x10000004

  #
  # BIOS Setup Gaming UI
  #
  gL05ServicesTokenSpaceGuid.PcdL05GamingUiSupported                  |FALSE|BOOLEAN|0x10000005

[PcdsFixedAtBuild]
#--------------------------------------------------------------------------------
# [PcdsFixedAtBuild] Non-Group: 0x2000####
#--------------------------------------------------------------------------------
  gL05ServicesTokenSpaceGuid.PcdOemModulePkgVersion                   |""|VOID*|0x20000001
  gL05ServicesTokenSpaceGuid.PcdL05ChipsetName                        |0x00000000|UINT32|0x20000002

  #
  # Function Support
  #
  # Intel 
  #
  gL05ServicesTokenSpaceGuid.PcdL05PchResetSupported                  |FALSE|BOOLEAN|0x20000011
  gL05ServicesTokenSpaceGuid.PcdL05PchSetupSupported                  |FALSE|BOOLEAN|0x20000012
  gL05ServicesTokenSpaceGuid.PcdL05SaGvSupported                      |FALSE|BOOLEAN|0x20000013
  gL05ServicesTokenSpaceGuid.PcdL05ActiveSmallCoreCountSupported      |FALSE|BOOLEAN|0x20000014

  #
  # AMD
  #
  gL05ServicesTokenSpaceGuid.PcdL05AmdSetupSupported                  |FALSE|BOOLEAN|0x20000015

  #
  # Switchable Graphics
  #
  gL05ServicesTokenSpaceGuid.PcdL05SwitchableGraphicsSupported        |FALSE|BOOLEAN|0x20000016

  # 
  #  [Lenovo Dock BIOS Writer's Guide (BWG) V0.3]
  #    3.2.2 Flowchart
  #      WOL from Dock
  #      - Enabled:  Put Name(WOLD,"_S5WOL_#01EF1700000000#") in ACPI DSDT table
  #      - Disabled: Put Name(WOLD,"_S5WOL_#00EF1700000000#") in ACPI DSDT table
  #
  gL05ServicesTokenSpaceGuid.PcdL05WakeOnLanFromDockEnableWold        |"_S5WOL_#01EF1700000000#"|VOID*|0x20000021
  gL05ServicesTokenSpaceGuid.PcdL05WakeOnLanFromDockDisableWold       |"_S5WOL_#00EF1700000000#"|VOID*|0x20000022

  #
  # Lenovo Cloud Boot
  #
  gL05ServicesTokenSpaceGuid.PcdL05LenovoCloudServiceUrl              |L"https://download.lenovo.com/pccbbs/cdeploy/efi/boot.efi"|VOID*|0x20000031
  gL05ServicesTokenSpaceGuid.PcdL05LenovoCloudDigiCert                |{GUID("0634A602-D0C8-4A70-B55D-B89D1AC84A51")}|VOID*|0x20000032

#--------------------------------------------------------------------------------
# [PcdsFixedAtBuild] FLASH_REGION Group: 0x2001####
#--------------------------------------------------------------------------------
  gL05ServicesTokenSpaceGuid.PcdL05FlashFvSlp20Base                   |0x00000000|UINT32|0x20010000
  gL05ServicesTokenSpaceGuid.PcdL05FlashFvSlp20Size                   |0x00000000|UINT32|0x20010001

#_Start_L05_EEPROM_REGION_
  gL05ServicesTokenSpaceGuid.PcdFlashFvEepromBase                     |0x00000000|UINT32|0x20010002
  gL05ServicesTokenSpaceGuid.PcdFlashFvEepromSize                     |0x00000000|UINT32|0x20010003
#_End_L05_EEPROM_REGION_

#_Start_L05_SYSTEM_PASSWORDS_REGION_
  gL05ServicesTokenSpaceGuid.PcdFlashFvSystemSupervisorPasswordBase   |0x00000000|UINT32|0x20010004
  gL05ServicesTokenSpaceGuid.PcdFlashFvSystemSupervisorPasswordSize   |0x00000000|UINT32|0x20010005
  gL05ServicesTokenSpaceGuid.PcdFlashFvSystemUserPasswordBase         |0x00000000|UINT32|0x20010006
  gL05ServicesTokenSpaceGuid.PcdFlashFvSystemUserPasswordSize         |0x00000000|UINT32|0x20010007
#_End_L05_SYSTEM_PASSWORDS_REGION_

#_Start_L05_COMPUTRACE_REGION_
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05ComputraceRegionBase        |0x00000000|UINT32|0x20010008
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05ComputraceRegionSize        |0x00000000|UINT32|0x20010009
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05ComputraceFvBase            |0x00000000|UINT32|0x2001000A
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05ComputraceFvSize            |0x00000000|UINT32|0x2001000B
#_End_L05_COMPUTRACE_REGION_

#_Start_L05_RESERVED_BIOS_REGION_
  gL05ServicesTokenSpaceGuid.PcdFlashFvReservedBase                   |0x00000000|UINT32|0x2001000C
  gL05ServicesTokenSpaceGuid.PcdFlashFvReservedSize                   |0x00000000|UINT32|0x2001000D
#_End_L05_RESERVED_BIOS_REGION_

#_Start_L05_RESERVED_BIOS_REGION_
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05Variable1Base               |0x00000000|UINT32|0x2001000E
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05Variable1Size               |0x00000000|UINT32|0x2001000F
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05Variable2Base               |0x00000000|UINT32|0x20010010
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05Variable2Size               |0x00000000|UINT32|0x20010011
#_End_L05_SYSTEM_PASSWORDS_REGION_

#_Start_L05_CUSTOMIZE_MULTI_LOGO_
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05CustomizeMultiLogoBase      |0x00000000|UINT32|0x20010012
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05CustomizeMultiLogoSize      |0x00000000|UINT32|0x20010013
#_End_L05_CUSTOMIZE_MULTI_LOGO_

#_Start_L05_BIOS_SELF_HEALING_
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05BackupIbbBase               |0x00000000|UINT32|0x20010014
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05BackupIbbSize               |0x00000000|UINT32|0x20010015
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05BshDataBase                 |0x00000000|UINT32|0x20010016
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05BshDataSize                 |0x00000000|UINT32|0x20010017
#_End_L05_BIOS_SELF_HEALING_

  gL05ServicesTokenSpaceGuid.PcdFlashFvL05FeatureUsedBase             |0x00000000|UINT32|0x20010018
  gL05ServicesTokenSpaceGuid.PcdFlashFvL05FeatureUsedSize             |0x00000000|UINT32|0x20010019

#--------------------------------------------------------------------------------
# [PcdsFixedAtBuild] SMBIOS Group: 0x2002####
#--------------------------------------------------------------------------------
  #
  # 00h = Old L05 PC not supporting the new SMBIOS fields
  # 01h = Desktop
  # 02h = Portable
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type00CharacterExEnclosureType     |0x02|UINT32|0x20020001  #Offset 0x0C

  #
  # Brand type
  #   00h = Reserved
  #   01h = L05 3000
  #   02h = Think
  #   03h = L05 Consumer Notebook
  #   04h - 1Fh = reserved
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type00CharacterExBrandType         |0x03|UINT32|0x20020002  #Offset 0x0C

  #
  # Type 03 - Offset 0x05 Type - "Product enclosure type value" 
  # [Follow SMBIOS V3.0.0 Spec - 7.4.1 System Enclosure or Chassis Types]
  #   01h = Other
  #   02h = Unknown
  #   03h = Desktop
  #   04h = Low Profile Desktop
  #   05h = Pizza Box
  #   06h = Mini Tower
  #   07h = Tower
  #   08h = Portable
  #   09h = Laptop
  #   0Ah = Notebook
  #   0Bh = Hand Held
  #   0Ch = Docking Station
  #   0Dh = All in One
  #   0Eh = Sub Notebook
  #   0Fh = Space-saving
  #   10h = Lunch Box
  #   11h = Main Server Chassis
  #   12h = Expansion Chassis
  #   13h = SubChassis
  #   14h = Bus Expansion Chassis
  #   15h = Peripheral Chassis
  #   16h = RAID Chassis
  #   17h = Rack Mount Chassis
  #   18h = Sealed-case PC
  #   19h = Multi-system chassis
  #   1Ah = Compact PCI
  #   1Bh = Advanced TCA
  #   1Ch = Blade
  #   1Eh = Tablet
  #   1Fh = Convertible
  #   20h = Detachable
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type03Type                         |0x0A|UINT8|0x20020003  #Offset 0x05

#--------------------------------------------------------------------------------
# Novo Menu Group: 0x2003####
#--------------------------------------------------------------------------------
  #
  # Define Advance Setup Layout ID for Novo Menu
  #
  gL05ServicesTokenSpaceGuid.PcdLayoutIdL05NovoMenu                   |0x00000020|UINT32|0x20030001
  gL05ServicesTokenSpaceGuid.PcdLayoutIdL05NovoMenuOnCenter           |0x00000021|UINT32|0x20030002

[PcdsDynamicEx]
#--------------------------------------------------------------------------------
# [PcdsDynamicEx] Non-Group: 0x3000####
#--------------------------------------------------------------------------------
  #
  # Novo Menu Entry Flag
  #
  gL05ServicesTokenSpaceGuid.PcdL05NovoMenuEntryFlag                  |FALSE|BOOLEAN|0x30000001

  #
  # Postpone Reset Type
  #
  gL05ServicesTokenSpaceGuid.PcdL05PostponeResetType                  |0xFF |UINT8  |0x30000002

  #
  # Skip dialog confirm and block (ctrl + alt + delete) reset
  #
  gL05ServicesTokenSpaceGuid.PcdL05PasswordErrorFlag                  |FALSE|BOOLEAN|0x30000003

  #
  # Customize Logo From ESP Flag
  #
  gL05ServicesTokenSpaceGuid.PcdL05CustomizeLogoFromEspFlag           |FALSE|BOOLEAN|0x30000004

  #
  # ACPI Table OEM Revision
  #
  gL05ServicesTokenSpaceGuid.PcdL05AcpiTableOemRevision               |0x00000001|UINT32|0x30000005

  #
  # [Lenovo BIOS Self-Healing Design Guidance Specification v1.9]
  #   BIOS Self-Healing Definition
  #
  gL05ServicesTokenSpaceGuid.PcdL05SelfRecoveryFolder                 |L"EFI\\Lenovo\\BIOS"|VOID*  |0x30000006
  gL05ServicesTokenSpaceGuid.PcdL05SelfRecoveryFile                   |L"SelfHealing.fd"   |VOID*  |0x30000007
  gL05ServicesTokenSpaceGuid.PcdL05TopSwapEnable                      |FALSE               |BOOLEAN|0x30000008
  gL05ServicesTokenSpaceGuid.PcdL05TopSwapDisableSwSmi                |0xFF                |UINT8  |0x30000009
  gL05ServicesTokenSpaceGuid.PcdL05BiosRecoveryHotkeyFlag             |FALSE               |BOOLEAN|0x3000000A
  gL05ServicesTokenSpaceGuid.PcdL05BiosSelfHealingEnable              |FALSE               |BOOLEAN|0x3000000B
#[-start-220118-BAIN000089-add]#
  gL05ServicesTokenSpaceGuid.PcdSBBRecoveryFlag                       |FALSE               |BOOLEAN|0x30000FFC
#[-end-220118-BAIN000089-add]#

  #
  # [Natural File Guard Design Guide V1.01]
  #   Natural File Guard Definition
  #
  gL05ServicesTokenSpaceGuid.PcdL05NaturalFileGuardUnlockStoredUhdpFlag |FALSE|BOOLEAN|0x3000000C

  #
  # Crisis Recovery Log
  #
  gL05ServicesTokenSpaceGuid.PcdL05CrisisRecoveryLogFileFormat        |L"%s%02x.C1S"  |VOID*|0x3000000D
  gL05ServicesTokenSpaceGuid.PcdL05CrisisRecoveryLogNumFile           |L"XXCNXX.num"  |VOID*|0x3000000E
  gL05ServicesTokenSpaceGuid.PcdL05CrisisRecoveryLastLogFile          |L"crisislg.dat"|VOID*|0x3000000F

#--------------------------------------------------------------------------------
# [PcdsDynamicEx] SMBIOS Group: 0x3001####
#--------------------------------------------------------------------------------
  #
  # SMBIOS - Type00
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type00BIOSMajorRelease             |0|UINT8|0x30010001  #Offset 0x14
  gL05ServicesTokenSpaceGuid.PcdL05Type00BIOSMinorRelease             |0|UINT8|0x30010002  #Offset 0x15
  gL05ServicesTokenSpaceGuid.PcdL05Type00ECMajorRelease               |0|UINT8|0x30010003  #Offset 0x16
  gL05ServicesTokenSpaceGuid.PcdL05Type00ECMinorRelease               |0|UINT8|0x30010004  #Offset 0x17

  #
  # SMBIOS - Type01
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type01ProductName                  |"INVALID"                            |VOID*|0x30010011  #Offset 0x05
  gL05ServicesTokenSpaceGuid.PcdL05Type01Version                      |"INVALID"                            |VOID*|0x30010012  #Offset 0x06
  gL05ServicesTokenSpaceGuid.PcdL05Type01SerialNumber                 |"INVALID"                            |VOID*|0x30010013  #Offset 0x07
  gL05ServicesTokenSpaceGuid.PcdL05Type01UUID                         |"INVALID"                            |VOID*|0x30010014  #Offset 0x08
  gL05ServicesTokenSpaceGuid.PcdL05Type01SKUNumber                    |"LENOVO_BI_IDEAPADXX_BU_idea_FM_YYYY"|VOID*|0x30010018  #Offset 0x19
  gL05ServicesTokenSpaceGuid.PcdL05Type01Family                       |"IDEAPAD"                            |VOID*|0x00010016  #Offset 0x1A

  #
  # SMBIOS - Type02
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type02Product                      |"INVALID"     |VOID*|0x30010021     #Offset 0x05
  gL05ServicesTokenSpaceGuid.PcdL05Type02Version                      |"No DPK"      |VOID*|0x30010022     #Offset 0x06
  gL05ServicesTokenSpaceGuid.PcdL05Type02SerialNumber                 |"INVALID"     |VOID*|0x30010023     #Offset 0x07
  gL05ServicesTokenSpaceGuid.PcdL05Type02AssetTag                     |"NO Asset Tag"|VOID*|0x30010024     #Offset 0x08
  gL05ServicesTokenSpaceGuid.PcdL05OSLienceDescriptor                 |"Not Defined" |VOID*|0x30010025     #Offset 0x06

  #
  # SMBIOS - Type03
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type03Version                      |"INVALID"     |VOID*|0x30010031  #Offset 0x06
  gL05ServicesTokenSpaceGuid.PcdL05Type03SerialNumber                 |"INVALID"     |VOID*|0x30010032  #Offset 0x07
  gL05ServicesTokenSpaceGuid.PcdL05Type03AssetTag                     |"NO Asset Tag"|VOID*|0x30010033  #Offset 0x08

  #
  # SMBIOS - Type200
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type200ID                          |"IdeaPad"|VOID*|0x30012001  #Offset 0x04
  gL05ServicesTokenSpaceGuid.PcdL05Type200MTM                         |"INVALID"|VOID*|0x30012002  #Offset 0x05
  #  LENOVO_CHINA_EEPROMContents_and_Tools_Spsc V1.20
  gL05ServicesTokenSpaceGuid.PcdL05Type200EepromSpecMajorNumber       |1        |UINT8|0x30012003  #Offset 0x08
  gL05ServicesTokenSpaceGuid.PcdL05Type200EepromSpecMinorNumber       |20       |UINT8|0x30012004  #Offset 0x09
#[-start-210624-YUNLEI0105-modify]
  #  Lenovo Power Management firmware specification V1.84
  gL05ServicesTokenSpaceGuid.PcdL05Type200PmSpecMajorNumber           |1        |UINT8|0x30012005  #Offset 0x0A
  gL05ServicesTokenSpaceGuid.PcdL05Type200PmSpecMinorNumber           |84       |UINT8|0x30012006  #Offset 0x0B
#[-end-210624-YUNLEI0105-modify]
  #  Lenovo variable(lvar) specification V1.05
  gL05ServicesTokenSpaceGuid.PcdL05Type200L05VarSpecMajorNumber       |1        |UINT8|0x30012007  #Offset 0x0E
#[-start-211112-YUNLEI0154-modify]//
  gL05ServicesTokenSpaceGuid.PcdL05Type200L05VarSpecMinorNumber       |20        |UINT8|0x30012008  #Offset 0x0F
#[-end-211112-YUNLEI0154-modify]//

#--------------------------------------------------------------------------------
# [PcdsDynamicEx] Setup & UI Group: 0x3002####
#--------------------------------------------------------------------------------
  #
  # [Lenovo BIOS Setup Design Guide V2.4]
  #   BIOS Setup UI Definition
  #
  gL05ServicesTokenSpaceGuid.PcdL05BackgroundBrightFlag               |FALSE|BOOLEAN|0x30020001
  gL05ServicesTokenSpaceGuid.PcdL05SetupConfirmDialogFlag             |FALSE|BOOLEAN|0x30020002
  gL05ServicesTokenSpaceGuid.PcdL05SetupWarningDialogFlag             |FALSE|BOOLEAN|0x30020003
  gL05ServicesTokenSpaceGuid.PcdL05SetupErrorDialogFlag               |FALSE|BOOLEAN|0x30020004
  gL05ServicesTokenSpaceGuid.PcdL05SetupNoticeDialogFlag              |FALSE|BOOLEAN|0x30020005
  gL05ServicesTokenSpaceGuid.PcdL05SetupDiscardAndExitFlag            |FALSE|BOOLEAN|0x30020006
  gL05ServicesTokenSpaceGuid.PcdL05SetupAlignmentCenterFlag           |FALSE|BOOLEAN|0x30020007
  gL05ServicesTokenSpaceGuid.PcdL05SetupWarningContinueDialogFlag     |FALSE|BOOLEAN|0x30020008

  #
  # BIOS Setup Graphic UI Definition
  #   PcdL05SetupConfirmDialogFlag and PcdL05SetupWarningDialogFlag
  #   are also used for Graphic UI
  #
  gL05ServicesTokenSpaceGuid.PcdL05SetupSucessDialogFlag              |FALSE|BOOLEAN|0x30020011
  gL05ServicesTokenSpaceGuid.PcdL05SetupFailedDialogFlag              |FALSE|BOOLEAN|0x30020012

  #
  # [Lenovo Secure Suite - Wipe Storage Device v1.4]
  # BIOS Setup UI add new item from BOOT page
  #
  gL05ServicesTokenSpaceGuid.PcdL05SetupNoticeDialogGrayFlag          |FALSE|BOOLEAN|0x30020021
  gL05ServicesTokenSpaceGuid.PcdL05SecureSuiteFlag                    |FALSE|BOOLEAN|0x30020022

  #
  # BIOS Setup Gaming UI Definition
  #
  gL05ServicesTokenSpaceGuid.PcdL05GamingSetupExitDialogFlag          |FALSE|BOOLEAN|0x30020031
  gL05ServicesTokenSpaceGuid.PcdL05GamingOverClockDialogFlag          |FALSE|BOOLEAN|0x30020032
  gL05ServicesTokenSpaceGuid.PcdL05GamingOverClockSetupDataTable      |0    |UINT64 |0x30020033
  gL05ServicesTokenSpaceGuid.PcdL05GamingOverClockSetupDataTableCount |0    |UINT32 |0x30020034
  gL05ServicesTokenSpaceGuid.PcdL05GamingOverClockGpuCurrentSpeed     |0    |UINT16 |0x30020035
  gL05ServicesTokenSpaceGuid.PcdL05GamingOverClockGpuTurboSpeed       |0    |UINT16 |0x30020036

  #
  # [Natural File Guard Design Guide V1.01]
  #   1. Prompt for UHDP or ESC
  #
  gL05ServicesTokenSpaceGuid.PcdL05NaturalFileGuardDialogFlag         |FALSE|BOOLEAN|0x30020041
  gL05ServicesTokenSpaceGuid.PcdL05NaturalFileGuardPressEscFlag       |FALSE|BOOLEAN|0x30020042

  #
  # [Lenovo Notebook Password Design V1.1]
  #   4.1.1. Check HDP
  #     User can press F1 to switch Master P assword and User P assword. After press F1
  #
  gL05ServicesTokenSpaceGuid.PcdL05NotebookPasswordDesignDialogF1Flag   |FALSE|BOOLEAN|0x30020051
  gL05ServicesTokenSpaceGuid.PcdL05NotebookPasswordDesignDialogF1String |L""  |VOID*  |0x30020052
  gL05ServicesTokenSpaceGuid.PcdL05NotebookPasswordDesignPressF1Flag    |FALSE|BOOLEAN|0x30020053
  gL05ServicesTokenSpaceGuid.PcdL05DialogNoTitleStringFlag              |FALSE|BOOLEAN|0x30020054
  gL05ServicesTokenSpaceGuid.PcdL05ShowDialogTitleStringFlag            |FALSE|BOOLEAN|0x30020055
  gL05ServicesTokenSpaceGuid.PcdL05HddPaswordTitleString                |L""  |VOID*  |0x30020056
  gL05ServicesTokenSpaceGuid.PcdL05HiddenDialogPressContinueString      |FALSE|BOOLEAN|0x30020057

  #
  # [Lenovo BIOS Setup Design Guide V2.9]
  #   Security
  #     Security Erase HDD # Data
  #
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseDialogFlag            |FALSE|BOOLEAN|0x30020061
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseProcessingDialogFlag  |FALSE|BOOLEAN|0x30020062
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseProcessingDialogExit  |FALSE|BOOLEAN|0x30020063
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseSuccessfullyDialogFlag|FALSE|BOOLEAN|0x30020064
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseFailedDialogFlag      |FALSE|BOOLEAN|0x30020065
  gL05ServicesTokenSpaceGuid.PcdL05SecurityEraseTimeMicrosecond       |0    |UINT64 |0x30020066

  #
  # Lenovo Cloud Boot
  #
  gL05ServicesTokenSpaceGuid.PcdL05InputWifiPassword                  |FALSE|BOOLEAN|0x30020071
  gL05ServicesTokenSpaceGuid.PcdL05WifiAutoConnect                    |FALSE|BOOLEAN|0x30020072
  gL05ServicesTokenSpaceGuid.PcdL05CloudBootFullScreenSupport         |FALSE|BOOLEAN|0x30020073

  #
  # [BIOS Update UI]
  #
  # Progress Bar Position & Width
  #
  gL05ServicesTokenSpaceGuid.PcdL05ProgressBarHorizontalPercent                     |25      |UINT32 |0x30020081
  gL05ServicesTokenSpaceGuid.PcdL05ProgressBarVerticalPercent                       |55      |UINT32 |0x30020082
  gL05ServicesTokenSpaceGuid.PcdL05ProgressBarWidthPercent                          |50      |UINT32 |0x30020083
  #
  # Progress Completion String Position, Size & Color
  #
  gL05ServicesTokenSpaceGuid.PcdL05ProgressCompletionHorizontalPercent              |25      |UINT32 |0x30020084
  gL05ServicesTokenSpaceGuid.PcdL05ProgressCompletionVerticalPercent                |58      |UINT32 |0x30020085
  gL05ServicesTokenSpaceGuid.PcdL05ProgressCompletionFontSizePercent                |3       |UINT32 |0x30020086
  gL05ServicesTokenSpaceGuid.PcdL05ProgressCompletionForegroundColorValue           |0xFFFFFF|UINT32 |0x30020087
  gL05ServicesTokenSpaceGuid.PcdL05ProgressCompletionBackgroundColorValue           |0x0     |UINT32 |0x30020088
  #
  # BIOS Update UI Error String & Position & Size
  #
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiErrorCodeString                      |L""     |VOID*  |0x30020089
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiErrorStringHorizontalPercent         |35      |UINT32 |0x3002008A
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiErrorStringVerticalPercent           |61      |UINT32 |0x3002008B
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiErrorStringFontSizePercent           |3       |UINT32 |0x3002008C
  #
  # BIOS Update UI Error String Color
  #
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiErrorStringForegroundColorValue      |0xFFFFFF|UINT32 |0x3002008D
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiErrorStringBackgroundColorValue      |0x0     |UINT32 |0x3002008E
  #
  # BIOS Update UI Warning Image Position & Size
  #
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateWarningImageHorizontalPercent          |35      |UINT32 |0x3002008F
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateWarningImageVerticalPercent            |68      |UINT32 |0x30020090
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateWarningImageSizePercent                |100     |UINT32 |0x30020091
  #
  # BIOS Update UI String
  #
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringEn1                            |L""     |VOID*  |0x30020092
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringEn2                            |L""     |VOID*  |0x30020093
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringZh                             |L""     |VOID*  |0x30020094
  #
  # BIOS Update UI String Position & Size
  #
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringHorizontalPercent              |40      |UINT32 |0x30020095
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringVerticalPercent                |68      |UINT32 |0x30020096
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringFontSizePercent                |3       |UINT32 |0x30020097
  #
  # BIOS Update UI String Color
  #
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringForegroundColorValue           |0xFFFFFF|UINT32 |0x30020098
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringBackgroundColorValue           |0x0     |UINT32 |0x30020099
  #
  # BIOS Update UI - Extend FW Update
  #
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiExtendFwUpdateFlag                   |FALSE   |BOOLEAN|0x3002009A
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiExtendFwUpdateUsedPercent            |10      |UINT32 |0x3002009B
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiExtendFwUpdateStartFlag              |FALSE   |BOOLEAN|0x3002009C

#[-start-2200405-BAIN0000-add]#
  gL05ServicesTokenSpaceGuid.PcdHddStrWAFlag1                                       |FALSE   |BOOLEAN|0x300200A0
  gL05ServicesTokenSpaceGuid.PcdHddStrWAFlag2                                       |FALSE   |BOOLEAN|0x300200A1
  gL05ServicesTokenSpaceGuid.PcdHddStrWAFlag3                                       |FALSE   |BOOLEAN|0x300200A2
#[-end-2200405-BAIN0000-add]#

[LibraryClasses]

