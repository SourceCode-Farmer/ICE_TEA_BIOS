/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _EFI_UEFI_DRIVE_IDENTIFICATION_H_
#define _EFI_UEFI_DRIVE_IDENTIFICATION_H_

#include <Uefi.h>
#include <Guid/GlobalVariable.h>
#include <Guid/L05LenovoBootOption.h>
#include <Protocol/DevicePath.h>
#include <Protocol/BlockIo.h>
#include <Protocol/BootOptionPolicy.h>
#include <Library/UefiLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DevicePathLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PrintLib.h>
#include <Library/DebugLib.h>
#include <Library/GenericBdsLib.h>

#define L05_FIXED_DEVICE_TYPE           0x00
#define L05_REMOVABLE_DEVICE_TYPE       0x01
#define L05_OPTICAL_DEVICE_TYPE         0x02
#define L05_NETWORK_DEVICE_TYPE         0x03
#define L05_BEV_TYPE                    0x04
#define L05_SDCARD_TYPE                 0x05
#define L05_NVME_DEVICE_TYPE            0x06
#define L05_UNKNOWN_DEVICE_TYPE         0xFF

#pragma pack(1)
typedef struct {
  UINT32                                Attributes;
  UINT16                                FilePathListLength;
  CHAR16                                *Description;
  EFI_DEVICE_PATH_PROTOCOL              *FilePathList;
  UINT8                                 *OptionalData;
} L05_BOOT_OPTION;

//
// By L05 Minimum BIOS Spec 1.37
// Table 13.3 Lenovo device type device path
//
typedef struct {
  EFI_DEVICE_PATH_PROTOCOL              Header;
  EFI_GUID                              Guid;
  UINT8                                 VendorDefinedData[1];
} L05_VENDOR_DEVICE_PATH_WITH_DATA;
#pragma pack()

VOID
ConvertBootToLBoot (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  );

#endif
