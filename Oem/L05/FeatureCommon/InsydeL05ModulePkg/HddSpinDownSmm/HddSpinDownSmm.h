/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_HDD_SPIN_DOWN_SMM_SMM_H_
#define _L05_HDD_SPIN_DOWN_SMM_SMM_H_

#include <OemSwSmi.h>  // EFI_L05_HDD_SPIN_DOWN_CALLBACK
#include <Library/SmmServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DevicePathLib.h>
#include <Library/PrintLib.h>
#include <Library/DebugLib.h>
#include <Protocol/AtaPassThru.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/L05HddSpindown.h>

//
// ATA controller mode
//
#define ATA_IDE_MODE                    0x00
#define ATA_AHCI_MODE                   0x01
#define ATA_RAID_MODE                   0x02
#define ATA_IDER_MODE                   0x03

//
// ATA command for HDD spin down
//
#define STANDBY_IMMEDIATE               0xE0

#endif
