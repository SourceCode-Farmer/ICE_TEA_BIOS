/** @file
  Instance of BIOS Update UI Services Library.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BIOS_UPDATE_UI_LIB_H_
#define _BIOS_UPDATE_UI_LIB_H_

#define FULL_HD_HORIZONTAL_VALUE        1920

EFI_STATUS
EFIAPI
L05DisableVendorUserInterface (
  VOID
  );

EFI_STATUS
EFIAPI
L05BiosUpdateWarningMsg (
  IN  BOOLEAN                           DisableVendorUi
  );

EFI_STATUS
EFIAPI
L05ErrorProcess (
  IN EFI_STATUS                         H2OFftStatus,
  IN CHAR16                             *ErrorString
  );

#endif
