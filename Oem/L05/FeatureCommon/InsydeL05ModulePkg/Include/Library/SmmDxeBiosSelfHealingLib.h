/** @file
  Instance of SMM/DXE BIOS Self-Healing Services Library.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SMM_DXE_BIOS_SELF_HEALING_LIB_H_
#define _SMM_DXE_BIOS_SELF_HEALING_LIB_H_

VOID
SafeFreePool (
  IN OUT VOID                           **Buffer
  );

BOOLEAN
BiosSelfHealingEnabled (
  VOID
  );

EFI_STATUS
GetBiosSelfHealingFlag (
  IN  UINT8                             *BiosSelfHealingFlag
  );

EFI_STATUS
SetBiosSelfHealingFlag (
  IN  UINT8                             BiosSelfHealingFlag
  );

#endif
