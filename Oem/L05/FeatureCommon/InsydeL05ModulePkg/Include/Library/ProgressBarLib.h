/** @file
  Instance of Progress Bar Services Library.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _PROGRESS_BAR_LIB_H_
#define _PROGRESS_BAR_LIB_H_

#define PROCESS_PERCENTAGE_VALUE        100

EFI_STATUS
EFIAPI
L05ImageScaling (
  IN OUT EFI_GRAPHICS_OUTPUT_BLT_PIXEL  *BltScaling,
  IN     UINTN                          BltScalingSize,
  IN     EFI_GRAPHICS_OUTPUT_BLT_PIXEL  *Blt,
  IN     UINTN                          BltSize,
  IN     UINTN                          BltScalingWidth,
  IN     UINTN                          BltScalingHeight,
  IN     UINTN                          BltWidth,
  IN     UINTN                          BltHeight,
  IN     BOOLEAN                        IsEgdgeProcess
  );

EFI_STATUS
EFIAPI
L05ProgressBar (
  IN UINTN                              Completion
  );

#endif
