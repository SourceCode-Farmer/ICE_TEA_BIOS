/** @file
  Instance of Encode HDD Password Table Library.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _ENCODE_HDD_PASSWORD_TABLE_LIBRARY_H_
#define _ENCODE_HDD_PASSWORD_TABLE_LIBRARY_H_

#include <Guid/HddPasswordVariable.h>
#include <Protocol/HddPasswordService.h>

#define L05_SAVE_ENCODE_HDD_PASSWORD_VARIABLE_NAME  L"LSEHP"

EFI_STATUS
L05SetEncodeHddPasswordTable (
  IN  EFI_HDD_PASSWORD_SERVICE_PROTOCOL *HddPasswordService,
  IN HDD_PASSWORD_TABLE                 *HddPasswordTable,
  IN OUT  UINT32                        HddPasswordCount
  );

EFI_STATUS
L05DeleteEncodeHddPasswordTable (
  VOID
  );

#endif
