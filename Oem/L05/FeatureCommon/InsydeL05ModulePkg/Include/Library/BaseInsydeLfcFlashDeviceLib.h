/*
;******************************************************************************
;* Copyright (c) 2012 - 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BASE_INSYDE_LFC_FLASH_DEVICE_LIB_H_
#define _BASE_INSYDE_LFC_FLASH_DEVICE_LIB_H_

EFI_STATUS
EFIAPI
LfcFdLibRead (
  IN     UINTN                          PAddress,
  IN OUT UINTN                          *NumBytes,
  OUT    UINT8                          *Buffer
  );

EFI_STATUS
EFIAPI
LfcFdLibWrite (
  IN      UINTN                         PAddress,
  IN OUT  UINTN                         *NumBytes,
  IN      UINT8                         *Buffer
  );

EFI_STATUS
EFIAPI
LfcFdLibBlockErase (
  IN      UINTN                         PAddress,
  IN      UINTN                         LbaLength
  );

#endif
