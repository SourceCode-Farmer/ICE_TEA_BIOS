/** @file
  Provides an interface for SMM Computrace

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SMM_COMPUTRACE_H_
#define _OEM_SVC_SMM_COMPUTRACE_H_

EFI_STATUS
OemSvcCheckComputraceSupportStatus (
  IN OUT BOOLEAN                        *Supported
  );

#endif
