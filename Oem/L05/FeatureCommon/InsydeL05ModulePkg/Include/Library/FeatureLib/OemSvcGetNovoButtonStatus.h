/** @file
  SPEC    : Dual Mode System Specification BIOS Portion
  Definition for NOVO Button

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_GET_NOVO_BUTTON_STATUS_H_
#define _OEM_SVC_GET_NOVO_BUTTON_STATUS_H_

EFI_STATUS
OemSvcGetNovoButtonStatus (
  VOID
  );

#endif
