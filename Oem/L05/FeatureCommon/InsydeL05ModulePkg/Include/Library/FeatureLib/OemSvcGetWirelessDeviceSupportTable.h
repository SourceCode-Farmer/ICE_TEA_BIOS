/** @file
  OEM Service to get Wireless Device Support Table for Wireless Device Support feature

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_GET_WIRELESS_DEVICE_SUPPORT_TABLE_H_
#define _OEM_SVC_GET_WIRELESS_DEVICE_SUPPORT_TABLE_H_

EFI_STATUS
OemSvcGetWirelessDeviceSupportTable (
  OUT EFI_L05_PCI_LOCATION                    **PciDeivceLocationList,
  OUT EFI_L05_USB_LOCATION                    **UsbDeivceLocationList,
  OUT EFI_L05_SUPPORTED_WIRELESS_DEVICE_INFO  **SupportedWirelessDeviceList
  );
#endif
