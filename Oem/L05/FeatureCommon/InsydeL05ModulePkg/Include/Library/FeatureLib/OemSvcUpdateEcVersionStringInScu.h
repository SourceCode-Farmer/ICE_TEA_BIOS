/** @file
  Provides an interface for changing EC version string in SCU.

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_UPDATE_EC_VERSION_STRING_IN_SCU_H_
#define _OEM_SVC_UPDATE_EC_VERSION_STRING_IN_SCU_H_

EFI_STATUS
OemSvcUpdateEcVersionStringInScu (
  IN OUT  CHAR16                        *EcVersionString
  );

#endif
