/** @file
  Provides an interface for switch Flip to Boot.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SWITCH_FLIP_TO_BOOT_H_
#define _OEM_SVC_SWITCH_FLIP_TO_BOOT_H_

#include <L05Config.h>

EFI_STATUS
OemSvcSwitchFlipToBoot (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05FlipToBoot
  );

//[-start-210701-FLINT00010-add]//
#ifdef LCFC_SUPPORT
#define LENOVO_FLIP_TO_BOOT_SW_INTERFACE_GUID \
 { 0xd743491e, 0xf484, 0x4952, { 0xa8, 0x7d, 0x8d, 0x5d, 0xd1, 0x89, 0xb7, 0xc } }
 
extern EFI_GUID  gLenovoFlipToBootSwInterfaceVariableGuid;
#endif
//[-end-210701-FLINT00010-add]//

#endif
