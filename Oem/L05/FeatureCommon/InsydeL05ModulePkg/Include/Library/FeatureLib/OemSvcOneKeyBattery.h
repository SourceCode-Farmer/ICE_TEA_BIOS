/** @file
  Definition for OneKeyBattery.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_ONE_KEY_BATTERY_H_
#define _OEM_SVC_ONE_KEY_BATTERY_H_

typedef enum {
  Normal_Bootup_State = 0,
  Keyboard_Bootup_State,
  Attach_AC_Bootup_State,
  Attach_AOU_Bootup_State
} BOOTUP_STATE_FOR_SHOW_ONEKEYBATTERY;

EFI_STATUS
OemSvcGetBatteryPercentage (
  IN OUT  UINT8                         *BatteryPercentage
  );

EFI_STATUS
OemSvcGetAcStatus (
  OUT BOOLEAN                           *AcStatus
  );

EFI_STATUS
OemSvcGetAouStatus (
  OUT BOOLEAN                           *AouStatus
  );

EFI_STATUS
OemSvcGetBootUpState (
  OUT BOOTUP_STATE_FOR_SHOW_ONEKEYBATTERY   *BootupState
  );

//[-start-210702-Dongxu0008-add]//
#ifdef LCFC_SUPPORT
VOID
DisableEcPower (
  VOID
  );
#endif
//[-end-210303-Dongxu0018-add]// 

#endif
