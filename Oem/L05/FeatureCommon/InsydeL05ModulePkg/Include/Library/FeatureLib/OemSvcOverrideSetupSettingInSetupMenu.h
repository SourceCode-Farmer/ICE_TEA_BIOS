/** @file
  Provides an Override Setup Setting Function in Setup menu

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_OVERRIDE_SETUP_SETTING_IN_SETUP_MENU_H_
#define _OEM_SVC_OVERRIDE_SETUP_SETTING_IN_SETUP_MENU_H_

EFI_STATUS
OemSvcOverrideSetupSettingInSetupMenu (
  IN OUT UINT8                          *SetupBuffer
  );

#endif
