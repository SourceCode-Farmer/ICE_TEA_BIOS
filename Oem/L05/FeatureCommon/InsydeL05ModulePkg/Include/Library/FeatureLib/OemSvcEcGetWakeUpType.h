/** @file
  Provide an interface for EC to get wake up type.

;******************************************************************************
;* Copyright (c) 2020 Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_EC_GET_WAKE_UP_TYPE_H_
#define _OEM_SVC_EC_GET_WAKE_UP_TYPE_H_

EFI_STATUS
OemSvcEcGetWakeUpType (
  OUT UINT8                             *WakeUpType
  );

#endif
