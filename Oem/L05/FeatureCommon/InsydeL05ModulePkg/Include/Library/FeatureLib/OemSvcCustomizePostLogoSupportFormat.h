/** @file
  Provide an interface for customize POST lOGO support format.

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/


#ifndef _OEM_SVC_CUSTOMIZE_POST_LOGO_SUPPORT_FORMAT_H_
#define _OEM_SVC_CUSTOMIZE_POST_LOGO_SUPPORT_FORMAT_H_

EFI_STATUS
OemSvcCustomizePostLogoSupportFormat (
  IN OUT  LOGO_SUPPORT_FORMAT           *LogoSupportFormat
  );

#endif
