/** @file
  Provide an interface in PEI and DXE phase.
  For Project to stop process of Legacy To EFI when unexpected shutdown system before.

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_STOP_LEGACY_TO_EFI_PROCESS_H_
#define _OEM_SVC_STOP_LEGACY_TO_EFI_PROCESS_H_

EFI_STATUS
OemSvcStopLegacyToEfiProcess (
  );
#endif
