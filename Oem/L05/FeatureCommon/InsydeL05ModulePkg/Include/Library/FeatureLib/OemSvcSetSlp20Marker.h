/** @file

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SET_SLP20_MARKER_H_
#define _OEM_SVC_SET_SLP20_MARKER_H_

EFI_STATUS
OemSvcSetSlp20Marker (
  IN EFI_ACPI_SLP_MARKER_STRUCTURE      *EfiL05Slp20Marker
  );

#endif
