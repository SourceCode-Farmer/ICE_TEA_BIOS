/** @file
  Provide an interface for customizing the protected region when flash the BIOS.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_FEATURE_GET_PROTECT_TABLE_H_
#define _OEM_SVC_FEATURE_GET_PROTECT_TABLE_H_

EFI_STATUS
OemSvcFeatureGetProtectTable (
  IN OUT EFI_L05_FLASH_PROTECT_ENTRY    **ProtectTable,
  IN OUT UINTN                          *Count
  );

#endif
