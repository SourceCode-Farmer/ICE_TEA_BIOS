/** @file
  SPEC    : Lenovo China Minimum BIOS Spec 1.40
  Chapter : 3.3.5 System Management BIOS (SMBIOS)

  Definition for OemSvcSmbiosOverride.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SMBIOS_OVERRIDE_H_
#define _OEM_SVC_SMBIOS_OVERRIDE_H_

#define DISABLE_SMBIOS_TYPE_REQUIRED    1

EFI_STATUS
OemSvcGetEcVersion (
  OUT UINT8                             *MajorVer,
  OUT UINT8                             *MinorVer
  );

EFI_STATUS
OemSvcGetEepromData (
  IN OUT  VOID                          *BufferPtr,
  IN      UINT16                        EepromSize
  );

EFI_STATUS
OemSvcRemoveSmbiosType (
  UINT8                                 *RemoveSmbiosTypeList
  );

EFI_STATUS
OemSvcGetCountryCode (
  CHAR8                                 *CountryCode,
  UINTN                                 CountryCodeSize
  );
#endif
