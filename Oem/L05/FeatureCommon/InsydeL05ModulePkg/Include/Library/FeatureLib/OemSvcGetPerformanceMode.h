/** @file
  Provides an opportunity for OEM to change the image of performance mode and
  the string in Gaming setup interface.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_GET_PERFORMANCE_MODE_H_
#define _OEM_SVC_GET_PERFORMANCE_MODE_H_

#include <L05Config.h>

EFI_STATUS
OemSvcGetPerformanceMode (
  IN  UINT8                             L05ThermalMode,
  OUT L05_PERFORMANCE_MODE              *Mode
  );

#endif
