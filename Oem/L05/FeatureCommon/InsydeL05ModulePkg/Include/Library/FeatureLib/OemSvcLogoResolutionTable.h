/** @file
  Provide an interface for logo resolution table

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/


#ifndef _OEM_SVC_LOGO_RESOLUTION_TABLE_H_
#define _OEM_SVC_LOGO_RESOLUTION_TABLE_H_

EFI_STATUS
OemSvcLogoResolutionTable (
  IN OUT  L05_LOGO_RESOLUTION_TABLE     **LogoResolutionTable,
  IN OUT  UINTN                         *LogoResolutionCount
  );

#endif
