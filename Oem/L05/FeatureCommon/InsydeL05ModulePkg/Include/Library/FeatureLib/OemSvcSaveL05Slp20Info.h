/** @file

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SAVE_L05_SLP20_INFO_H_
#define _OEM_SVC_SAVE_L05_SLP20_INFO_H_

EFI_STATUS
OemSvcSaveL05Slp20Info (
  IN EFI_L05_SLP20_AREA                 *L05Slp20Area
  );

#endif
