/** @file
  For Project to Skip Confirm Dialog when PXE Boot Fail.

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SKIP_PXE_BOOT_FAIL_CONFIRM_DIALOG_H_
#define _OEM_SVC_SKIP_PXE_BOOT_FAIL_CONFIRM_DIALOG_H_

EFI_STATUS
OemSvcSkipPxeBootFailConfirmDialog (
  );
#endif
