/** @file
  Provides an opportunity for ODM to Get GPU Type.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_GET_GPU_TYPE_H_
#define _OEM_SVC_GET_GPU_TYPE_H_

EFI_STATUS
OemSvcGetGpuType (
  IN OUT UINT8                          *GpuType
  );

#endif
