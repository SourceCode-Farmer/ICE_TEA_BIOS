/** @file
  Provides an interface for switch USB Charge for external device function.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SWITCH_ALWAYS_ON_USB_H_
#define _OEM_SVC_SWITCH_ALWAYS_ON_USB_H_

#include <L05Config.h>

EFI_STATUS
OemSvcSwitchAlwaysOnUsb (
  IN L05_OEM_SWITCH_TRIGGER_POINT_TYPE  TriggerPoint,
  IN UINT8                              L05AlwaysOnUsb
  );

#endif
