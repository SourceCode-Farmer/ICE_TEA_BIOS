/** @file
  Provides an interface for Disable Building-in Battery.

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_DISABLE_BUILDING_IN_BATTERY_H_
#define _OEM_SVC_DISABLE_BUILDING_IN_BATTERY_H_

EFI_STATUS
OemSvcDisableBuildingInBattery (
  VOID
  );

#endif
