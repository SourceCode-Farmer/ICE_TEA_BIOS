/** @file
  SPEC    : Lenovo China Minimum BIOS Spec 128.doc
  Chapter : 3.5 BIOS Security Passwords

  Definition for BIOS Security Password

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SECURITY_PASSWORD_H_
#define _OEM_SVC_SECURITY_PASSWORD_H_

#include "L05Config.h"
#include <Protocol/SysPasswordService.h>

#define L05_SECURITY_SYSTEM_PASSWORD_LENGTH  64
#define L05_SECURITY_HDD_PASSWORD_LENGTH     64

EFI_STATUS
OemSvcGetSystemPasswords (
  IN     PASSWORD_TYPE                  SystemPasswordType,
  IN     UINTN                          DataLength,
  IN OUT UINT8                          *SystemPasswordBuffer
  );

EFI_STATUS
OemSvcSetSystemPasswords (
  IN     PASSWORD_TYPE                  SystemPasswordType,
  IN     UINTN                          DataLength,
  IN OUT UINT8                          *SystemPasswordBuffer
  );

UINT8 *
L05SysPasswordPacket (
  IN  UINT8                             *PasswordPtr,
  IN  UINTN                             PasswordLength
  );

BOOLEAN
L05PasswordCmp (
  UINT8                                 *Password1,
  UINT8                                 *Password2
  );

UINT8 *
L05SysPasswordEncode (
  IN  UINT8                             *PasswordPtr,
  IN  UINTN                             PasswordLength
  );

#ifndef L05_NOTEBOOK_PASSWORD_ENABLE
BOOLEAN
IsSystemPasswordChecksumValid (
  IN UINT8                              *PasswordPtr
  );

#else
BOOLEAN
L05HaveSystemPassword (
  IN UINT8                              *PasswordPtr
  );

EFI_STATUS
SystemPasswordEncode (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize,
  OUT UINT8                             *EncodeData,
  IN OUT UINTN                          EncodeDataSize
  );

VOID
L05SwapEntries (
  IN CHAR8                              *Data,
  IN UINT16                             Size
  );

EFI_STATUS
HddPasswordEncode (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize,
  IN  UINT8                             *ModelNumberPtr,
  IN  UINTN                             ModelNumberSize,
  IN  UINT8                             *SerialNumberPtr,
  IN  UINTN                             SerialNumberSize,
  OUT UINT8                             *EncodeData,
  IN OUT UINTN                          EncodeDataSize
  );

#ifdef L05_NATURAL_FILE_GUARD_ENABLE
EFI_STATUS
OemSvcLockSecureKey (
  VOID
  );

BOOLEAN
IsNaturalFileGuardEnable (
  VOID
  );

BOOLEAN
IsFoundStoredUhdp (
  VOID
  );

EFI_STATUS
ClearStoredUhdp (
  VOID
  );

EFI_STATUS
DisableNaturalFileGuard (
  VOID
  );

EFI_STATUS
UhdpEncryption (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             DataSize
  );

EFI_STATUS
UhdpDecryption (
  IN  UINT8                             *DataPtr,
  IN  UINTN                             *DataSize
  );
#endif
#endif

#endif
