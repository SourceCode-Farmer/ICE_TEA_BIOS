/** @file
  Provides an interface to notify EC to stop WDT.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_NOTIFY_EC_TO_STOP_WDT_H_
#define _OEM_SVC_NOTIFY_EC_TO_STOP_WDT_H_

EFI_STATUS
OemSvcNotifyEcToStopWdt (
  VOID
  );

#endif
