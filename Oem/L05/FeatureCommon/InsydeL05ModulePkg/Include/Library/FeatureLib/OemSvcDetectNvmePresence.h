/** @file
  Provides an opportunity for ODM to Detect NVMe Presence

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_DETECT_NVME_PRESENCE_H_
#define _OEM_SVC_DETECT_NVME_PRESENCE_H_

EFI_STATUS
OemSvcDetectNvmePresence (
  IN OUT UINT8                          *IsNvmePresence
  );

#endif
