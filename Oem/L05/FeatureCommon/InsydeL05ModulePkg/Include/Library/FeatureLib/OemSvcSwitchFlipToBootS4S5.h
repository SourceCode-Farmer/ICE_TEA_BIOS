/** @file
  Provides an interface for notify EC to switch Flip to Boot in S4/S5.

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SWITCH_FLIP_TO_BOOT_S4_S5_H_
#define _OEM_SVC_SWITCH_FLIP_TO_BOOT_S4_S5_H_

EFI_STATUS
OemSvcSwitchFlipToBootS4S5 (
  VOID
  );

#endif
