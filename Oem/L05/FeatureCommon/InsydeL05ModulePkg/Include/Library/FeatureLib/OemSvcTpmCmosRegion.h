/** @file
  Definition for Oem Svc Tpm Cmos Region

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_TPM_CMOS_REGION_H_
#define _OEM_SVC_TPM_CMOS_REGION_H_

EFI_STATUS
OemSvcTpmCmosRegion (
  IN   UINT8                            CmosBank,
  OUT  UINT8                            **ProtectCmosTable,
  OUT  UINT8                            *ProtectCmosTableSize
  );
#endif
