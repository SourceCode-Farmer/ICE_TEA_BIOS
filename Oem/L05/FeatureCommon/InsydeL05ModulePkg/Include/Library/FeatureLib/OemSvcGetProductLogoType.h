/** @file
  Provides an opportunity for OEM to change Branding logo and Lenovo logo
  in setup interface.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_GET_PRODUCT_LOGO_TYPE_H_
#define _OEM_SVC_GET_PRODUCT_LOGO_TYPE_H_

#include <L05Config.h>

EFI_STATUS
OemSvcGetProductLogoType (
  OUT L05_BRANDING_LOGO_TYPE           *BrandingLogoType,
  OUT BOOLEAN                          *IsConsumer
  );

#endif
