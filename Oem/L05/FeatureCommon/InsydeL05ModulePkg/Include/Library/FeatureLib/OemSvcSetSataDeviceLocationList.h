/** @file
  Definition for Sata Device location

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SET_SATA_DEVICE_LOCATION_H_
#define _OEM_SVC_SET_SATA_DEVICE_LOCATION_H_

typedef struct {
  UINT16                                DeviceType;
  UINT8                                 Bus;
  UINT8                                 Device;
  UINT8                                 Function;
  UINT8                                 PortNumber;
} EFI_L05_SATA_DEVICE_LOCATION;

EFI_STATUS
OemSvcSetSataDeviceLocationList (
  OUT EFI_L05_SATA_DEVICE_LOCATION      **Buffer
  );

#endif
