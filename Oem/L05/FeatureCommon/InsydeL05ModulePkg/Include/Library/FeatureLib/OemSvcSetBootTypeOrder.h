/** @file
  Provides an opportunity for ODM to set Boot Type Order.

;******************************************************************************
;* Copyright (c) 2012 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_SET_SVC_SET_BOOT_TYPE_ORDER_H_
#define _OEM_SVC_SET_SVC_SET_BOOT_TYPE_ORDER_H_

EFI_STATUS
OemSvcSetBootTypeOrder (
  OUT UINT16                            **BootTypeOrder,
  OUT UINTN                             *BootTypeOrderLength,
  OUT UINT16                            **EfiBootTypeOrder,
  OUT UINTN                             *EfiBootTypeOrderLength
  );

#endif
