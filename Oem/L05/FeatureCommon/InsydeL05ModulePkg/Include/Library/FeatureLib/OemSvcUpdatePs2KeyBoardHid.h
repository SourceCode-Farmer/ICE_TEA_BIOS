/** @file
  Provides an interface for changing PS2 Keyboard HID in ACPI DSDT Table.

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_UPDATE_PS2_KEYBOARD_HID_H_
#define _OEM_SVC_UPDATE_PS2_KEYBOARD_HID_H_

EFI_STATUS
OemSvcUpdatePs2KeyBoardHid (
  IN OUT  UINT64                        *OldKeyBoardHid,
  IN OUT  UINT64                        *NewKeyBoardHid
  );

#endif
