/** @file
  Definition for Override Default Setup Setting Function

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_OVERRIDE_DEFAULT_SETUP_SETTING_H_
#define _OEM_SVC_OVERRIDE_DEFAULT_SETUP_SETTING_H_

EFI_STATUS
OemSvcOverrideDefaultSetupSetting (
  IN OUT UINT8                          *SetupBuffer,
  IN OUT UINT8                          *PchSetupBuffer,
  IN OUT UINT8                          *CpuSetupBuffer,
  IN OUT UINT8                          *SaSetupBuffer
  );

#endif
