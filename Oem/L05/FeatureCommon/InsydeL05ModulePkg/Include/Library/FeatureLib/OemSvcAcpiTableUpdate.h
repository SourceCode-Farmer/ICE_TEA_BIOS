/** @file
  SPEC    : Lenovo China Minimum BIOS Spec 128.doc
  Chapter : 3.4.8 ACPI Table

  Definition for Oem Svc Acpi Table Update

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_ACPI_TABLE_UPDATE_H_
#define _OEM_SVC_ACPI_TABLE_UPDATE_H_

EFI_STATUS
OemSvcAcpiTableUpdate (
  IN OUT  VOID                          *Table
  );
#endif
