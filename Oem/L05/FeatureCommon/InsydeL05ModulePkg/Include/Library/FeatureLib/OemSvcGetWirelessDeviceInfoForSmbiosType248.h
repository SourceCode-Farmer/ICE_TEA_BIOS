/** @file
  OEM Service to get Wireless Device Information for SMBIOS Type 248

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_GET_WIRELESS_DEVICE_INFO_FOR_SMBIOS_TYPE248_H_
#define _OEM_SVC_GET_WIRELESS_DEVICE_INFO_FOR_SMBIOS_TYPE248_H_

EFI_STATUS
OemSvcGetWirelessDeviceInfoForSmbiosType248 (
  OUT VOID                              *SmbiosType248Info
  );
#endif
