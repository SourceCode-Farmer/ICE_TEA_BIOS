/** @file
  Provides an interface for changing PS2 Touch Pad HID, CID in ACPI DSDT Table.

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_UPDATE_PS2_TOUCH_PAD_HID_CID_H_
#define _OEM_SVC_UPDATE_PS2_TOUCH_PAD_HID_CID_H_

EFI_STATUS
OemSvcUpdatePs2TouchPadHidCid (
  IN OUT  UINT64                        *OldTouchPadHid,
  IN OUT  UINT64                        *NewTouchPadHid,
  IN OUT  UINT16                        *NewTouchPadCid,
  IN OUT  UINT8                         *OffsetCid
  );

#endif
