/** @file
  Provides an interface for EC to keep power for wake on LAN during S3, S4, and S5 when only has battery or with AC.
;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_EC_KEEP_WOL_POWER_FOR_AC_ONLY_H_
#define _OEM_SVC_EC_KEEP_WOL_POWER_FOR_AC_ONLY_H_

EFI_STATUS
OemSvcEcKeepWolPowerForAcOnly (
  IN  UINT8                             AcOnly
  );

#endif
