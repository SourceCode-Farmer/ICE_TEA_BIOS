/** @file
  Provides an interface for modify Second Mac Address

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_UPDATE_SECOND_MAC_ADDRESS_H_
#define _OEM_SVC_UPDATE_SECOND_MAC_ADDRESS_H_

EFI_STATUS
OemSvcUpdateSecondMacAddress (
  IN  OUT EFI_MAC_ADDRESS               *ModifyMacAddrVarData
  );

#endif

