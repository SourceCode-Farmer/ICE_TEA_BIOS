/** @file
  Provides an opportunity for OEM to show GPU information in Gaming setup interface.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_GET_GPU_INFORMATION_H_
#define _OEM_SVC_GET_GPU_INFORMATION_H_

#include <Library/MemoryAllocationLib.h>
#include <Library/BaseLib.h>

EFI_STATUS
OemSvcGetGpuInformation (
  OUT CHAR16                            **GpuInfo
  );

#endif
