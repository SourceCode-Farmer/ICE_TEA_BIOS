/** @file
  Provides an Restore Setup Setting Function After Flash

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_SVC_RESTORE_SETUP_SETTING_AFTER_FLASH_H_
#define _OEM_SVC_RESTORE_SETUP_SETTING_AFTER_FLASH_H_

EFI_STATUS
OemSvcRestoreSetupSettingAfterFlash (
  IN OUT UINT8                          *SetupBuffer,
  IN OUT UINT8                          *PchSetupBuffer,
  IN OUT UINT8                          *CpuSetupBuffer,
  IN OUT UINT8                          *SaSetupBuffer
  );

#endif
