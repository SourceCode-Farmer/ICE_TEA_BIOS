/** @file
  Instance of BIOS Self-Healing Services Library.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BIOS_SELF_HEALING_LIB_H_
#define _BIOS_SELF_HEALING_LIB_H_

UINTN
RetrieveIbbFvInfo (
  IN  UINT32                            **PrimaryFvBase,
  IN  UINT32                            **BackupFvBase,
  IN  UINT32                            **BackupFvSize
  );

EFI_STATUS
PatchDataToBuffer (
  IN  UINT32                            FvBase,
  IN  VOID                              *Buffer
  );

BOOLEAN
IsBackupIbbRegion (
  IN  UINT32                            Address
  );

BOOLEAN
TopSwapStatus (
  VOID
  );

EFI_STATUS
TopSwapSet (
  IN  BOOLEAN                           TopSwapEnable
  );

BOOLEAN
IsManufacturingMode (
  VOID
  );

//[-start-220125-BAIN000092-remove]//
#ifndef LCFC_SUPPORT
BOOLEAN
CheckPbbrSyncFlag (
  VOID
  );
#endif
//[-end-220125-BAIN000092-remove]//

#endif
