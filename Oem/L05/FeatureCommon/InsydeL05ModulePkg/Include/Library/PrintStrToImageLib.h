/** @file
  Instance of Print String to Image Services Library.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _PRINT_STR_TO_IMAGE_LIB_H_
#define _PRINT_STR_TO_IMAGE_LIB_H_

EFI_STATUS
EFIAPI
L05PrintStrToImage (
  IN UINTN                              X,
  IN UINTN                              Y,
  IN CHAR16                             *String,
  IN UINTN                              StringSize,
  IN UINTN                              ForegroundColorValue,
  IN UINTN                              BackgroundColorValue,
  EFI_HII_FONT_STYLE                    FontStyle
  );

#endif
