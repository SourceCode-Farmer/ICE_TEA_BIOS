/** @file
  Library definition for Feature Report function

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OK_FEATURE_REPORT_LIB_H_
#define _OK_FEATURE_REPORT_LIB_H_

//
// Library class public structures/unions
//

//
// Library class public functions
//
#ifdef OK_FEATURE_REPORT_ENABLE
VOID
EFIAPI
FeatureReportPrint (
  IN CONST CHAR8                        *Format,
  ...
  );

VOID
EFIAPI
FeatureReportPrintHex (
  IN VOID                               *Location,
  IN UINTN                              Length
  );
#endif

//
// Library class public defines
//
#ifdef OK_FEATURE_REPORT_ENABLE
  #define FEATURE_REPORT(Expression)      FeatureReportPrint Expression
  #define FEATURE_REPORT_HEX(Expression)  FeatureReportPrintHex Expression
#else
  #define FEATURE_REPORT(Expression)
  #define FEATURE_REPORT_HEX(Expression)
#endif

#endif
