/** @file
  Instance of Flash Protect Region Library.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _FLASH_PROTECT_REGION_LIB_H_
#define _FLASH_PROTECT_REGION_LIB_H_

#define L05_DEFAULT_PROTECT_TABLE_SIZE  40

#pragma pack(1)

typedef enum {
  Ihisi           = 0x01,
  Crisis          = 0x02,
  SelfHealing     = 0x04,
  ProtectStageMax = 0xFF
} EFI_L05_FLASH_STAGE;

typedef struct _EFI_L05_FLASH_PROTECT_REGION {
  UINT32                                LinearAddress;
  UINT32                                Size;
} EFI_L05_FLASH_PROTECT_REGION;

typedef struct _EFI_L05_FLASH_PROTECT_ENTRY {
  EFI_L05_FLASH_STAGE                   SupportProtectStage;
  EFI_L05_FLASH_PROTECT_REGION          Region;
} EFI_L05_FLASH_PROTECT_ENTRY;

#pragma pack()

EFI_STATUS
GetFlashProtectTable (
  OUT EFI_L05_FLASH_PROTECT_REGION      **ProtectTable,
  OUT UINTN                             *Count,
  IN  EFI_L05_FLASH_STAGE               FlashStage
  );

#endif
