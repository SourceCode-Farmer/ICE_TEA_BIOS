/*
;******************************************************************************
;* Copyright (c) 2012 - 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SMM_LFC_SWSMI_CPU_REGISTER_LIB_H_
#define _SMM_LFC_SWSMI_CPU_REGISTER_LIB_H_

EFI_STATUS
GetDwordRegisterByCpuIndex (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             CpuIndex,
  IN  UINTN                             RegisterWidth,
  OUT UINT32                            *RegisterData
  );

EFI_STATUS
SetDwordRegisterByCpuIndex (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             CpuIndex,
  IN  UINTN                             RegisterWidth,
  IN  UINT32                            *RegisterData
  );

EFI_STATUS
IdentifyCpuIndexByEax (
  IN  UINT32                            EaxValue,
  IN  UINT32                            EaxValueMask,
  OUT UINTN                             *CpuIndex
  );

EFI_STATUS
GetSwSmiSubFunctionNumber (
  OUT UINT8                             *SwSmiSubFunctionNumber
  );

EFI_STATUS
SetSwSmiSubFunctionNumber (
  IN UINT8                              SwSmiSubFunctionNumber
  );

#endif
