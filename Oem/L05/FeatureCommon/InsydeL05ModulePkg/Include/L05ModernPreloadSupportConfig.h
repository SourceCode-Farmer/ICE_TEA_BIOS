/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_MODERN_PRELOAD_SUPPORT_CONFIG_H_
#define _L05_MODERN_PRELOAD_SUPPORT_CONFIG_H_

//
// Interface definition
//
#define L05_MODERN_PRELOAD_SUPPORT_FUNCTION     (UINT32)(0x534DA100 + (UINT8)L05_SECURITY_SW_SMI)
#define L05_MODERN_PRELOAD_SET_TPMLOCKD         (UINT32)0x00
#define L05_MODERN_PRELOAD_CHECK_TPMLOCKD       (UINT32)0x01

//
// TPMLOCK state definition
//
#define L05_TPMLOCKD_DONE                       (UINT8)0x02
#define L05_TPMLOCKD_NOT_DONE                   (UINT8)0x03

//
// Modern Preload Support Return Code
//
//[-start-210618-YUNLEI0103-modify]//
#ifdef LCFC_SUPPORT
#define L05_MODERN_PRELOAD_GET_TPMLOCKD_FAILED  (UINT32)0x80000000
#define L05_MODERN_PRELOAD_ERROR_SUCCESS           (UINT32)0x0000000
#define L05_MODERN_PRELOAD_ERROR_SET_FAILED        (UINT32)0x80000000
#define L05_MODERN_PRELOAD_ERROR_CHECK_ACCESS      (UINT32)0x80000001
#define L05_MODERN_PRELOAD_ERROR_TPMLOCK_DONE      (UINT32)0x00000000
#define L05_MODERN_PRELOAD_ERROR_TPMLOCK_NOT_DONE  (UINT32)0x80000002
#else
#define L05_MODERN_PRELOAD_SUCCESS              (UINT32)0x00000000
#define L05_MODERN_PRELOAD_SET_TPMLOCKD_FAILED  (UINT32)0x80000000
#define L05_MODERN_PRELOAD_GET_TPMLOCKD_FAILED  (UINT32)0x80000000
#define L05_MODERN_PRELOAD_TPMLOCK_DONE         (UINT32)0x00000000
#define L05_MODERN_PRELOAD_TPMLOCK_NOT_DONE     (UINT32)0x40000000
#endif
//[-end-210618-YUNLEI0103-modify]//

#pragma pack(1)
typedef union {
  struct
  {
     UINT8                              State   :2;  // BIT0-1
     UINT8                              Reserved:6;  // BIT1-7
  } Bits;
  UINT8                                 Byte;
} EFI_L05_TPM_LOCK_REGISTER;
#pragma pack()

#endif
