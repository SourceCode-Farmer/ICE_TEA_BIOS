/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_WMI_ASSET_TAG_H_
#define _L05_WMI_ASSET_TAG_H_

#define L05_WMI_BUFFER_MAX_SIZE_BIT     8192 // 1024 Byte
#define L05_WMI_BUFFER_MAX_SIZE         (L05_WMI_BUFFER_MAX_SIZE_BIT >> 3) //L05_WMI_BUFFER_MAX_SIZE(Byte) = L05_WMI_BUFFER_MAX_SIZE_BIT / 8

#define WMI_ASSET_TAG_NULL_STR          ""
//
// Object ID
//
#define L05_READ_ASSET_TAG_B0           0xB0
#define L05_WRITE_ASSET_TAG_B1          0xB1

//
// WMI Return Status
//
#define L05_WMI_SUCCESS                 0x00
#define L05_WMI_NOT_SUPPORTED           0x01
#define L05_WMI_INVALID_PARAMETER       0x02
#define L05_WMI_ACCESS_DENIED           0x03
#define L05_WMI_SYSTEM_BUSY             0x04

#endif
