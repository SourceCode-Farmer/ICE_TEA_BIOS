/** @file
  L05 Setup Configuration Definitions

;******************************************************************************
;* Copyright (c) 2014 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_SETUP_CONFIG_H_
#define _L05_SETUP_CONFIG_H_

#include <Guid/L05H2OSetup.h>

//
// L05 GPU Type define
//
#define L05_GPU_TYPE_IGD                0
#define L05_GPU_TYPE_DGPU_NVIDIA        1
#define L05_GPU_TYPE_DGPU_AMD           2

//
//  L05 HDD Password Configuration Structure
//
#define L05_HDD_PASSWORD_CONFIGURATION_VARSTORE_NAME L"L05HddPasswordConfig"
#define L05_HDD_PASSWORD_CONFIGURATION_GUID \
  {0xe6560c55, 0x28a7, 0x4ad8, {0xa8, 0x7b, 0xe7, 0x51, 0x87, 0x19, 0x6b, 0x6e}}

extern EFI_GUID                         gEfiL05HddPasswordConfigGuid;

#pragma pack(1)
typedef struct {
  UINT8                                 NumOfHdd;
  UINT8                                 SetHddPasswordFlag[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         StatusPromptStringId[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         StatusStringId[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         SetHddPromptStringId[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         SetHddHelpStringId[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         ChangeMasterPromptStringId[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         ChangeMasterHelpStringId[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         ChangeUserPromptStringId[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         ChangeUserHelpStringId[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         ChangeHddTitleStringId[L05_MAX_HDD_PASSWORD_NUMBER];
  UINT8                                 SecurityEraseSupported[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         SecurityErasePromptStringId[L05_MAX_HDD_PASSWORD_NUMBER];
  EFI_STRING_ID                         SecurityEraseHelpStringId[L05_MAX_HDD_PASSWORD_NUMBER];
} L05_HDD_PASSWORD_CONFIGURATION;
#pragma pack()
#endif

