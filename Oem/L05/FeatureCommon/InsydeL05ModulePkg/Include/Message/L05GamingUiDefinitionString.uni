/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

//
//   BIOS Setup Gaming UI Definition
//

//
// Gaming Home Page (GamingHomeVfr.vfr)
//
#string STR_GAMING_HOME_TITLE                                   #language en-US  "Home"
                                                                #language zh-TW  "\wide主頁\narrow"
//
// Gaming Common Page hotkey
// InsydeL05ModulePkg/Include/Message/L05HotKeyStr.uni
//
#string L05_STR_GAMING_HOT_KEY_DISCARD_AND_EXIT_HELP_STRING     #language en-US  "Back"
                                                                #language zh-CN  "返回" 

//
// Form [General Help]
// InsydeL05ModulePkg/Include/Message/L05SetupBrowserString.uni
//
#string LENOVO_STR_GAMING_SETUP_DESCRIPTION_STRING              #language en-US  "The Setup Utility is a ROM-based configuration utility that displays the \n"
                                                                                 "system's configuration status and provides users with a tool to set their \n"
                                                                                 "system parameters. Setting incorrect values may cause system boot failure:\n"
                                                                                 "Load Setup default values to recover.\n"
                                                                                 "\n"
                                                                                 "                                                                          \n"
                                                                                 "<←/→>: Switch Menu Items\n"
                                                                                 "<↑/↓>: Select Item\n"
                                                                                 "<Enter>: Select or Enter SubMenu\n"
                                                                                 "<F9>: Load Setup Default\n"
                                                                                 "<F10>: Save and Exit\n"
                                                                                 "<ESC>: Back to Home Page\n"
                                                                                 "<F1>: Displays General Help"
       
//
// Gaming exit dialog option
//
#string L05_STR_GAMING_EXIT_DIALOG_SAVING                       #language en-US  "SAVE & EXIT"
                                                                #language zh-CN  "\wide保存并退出\narrow"
#string L05_STR_GAMING_EXIT_DIALOG_DISCARDING                   #language en-US  "DISCARD CHANGE & EXIT"
                                                                #language zh-CN  "\wide放弃修改并退出\narrow"

//
// Gaming Home Page (GamingOwnerDrawPanel.c)
//
#string L05_STR_GAMING_ARROWS                                   #language en-US  "Arrows"
                                                                #language zh-CN  "\wide方向键\narrow"
#string L05_STR_GAMING_SELECT_ITEM                              #language en-US  "Select Item"
                                                                #language zh-CN  "\wide选择项目\narrow"
#string L05_STR_GAMING_ENTER                                    #language en-US  "Enter"
                                                                #language zh-CN  "Enter\wide键\narrow"
#string L05_STR_GAMING_OK                                       #language en-US  "OK"
                                                                #language zh-CN  "\wide确认\narrow"
#string L05_STR_GAMING_ESC                                      #language en-US  "ESC"
                                                                #language zh-CN  "ESC\wide键\narrow"
#string L05_STR_GAMING_LEAVE_BIOS                               #language en-US  "Leave BIOS"
                                                                #language zh-CN  "\wide退出\narrowBIOS"
#string L05_STR_GAMING_F2_DURING_BOOT                           #language en-US  "F2 During Boot"
                                                                #language zh-CN  "\wide启动中按\narrowF2\wide键\narrow"
#string L05_STR_GAMING_BIOS_SETUP                               #language en-US  "BIOS Setup"
                                                                #language zh-CN  "BIOS\wide设置\narrow"
#string L05_STR_GAMING_F12_DURING_BOOT                          #language en-US  "F12 During Boot"
                                                                #language zh-CN  "\wide启动中按\narrowF12\wide键\narrow"
#string L05_STR_GAMING_BOOT_DEVICE                              #language en-US  "Boot Device"
                                                                #language zh-CN  "\wide启动设备\narrow"
#string L05_STR_GAMING_DURING_BOOT                              #language en-US  "During Boot"
                                                                #language zh-CN  "\wide启动中按键\narrow"
#string L05_STR_GAMING_DURING_BOOT_1                            #language en-US  "During Boot"
                                                                #language zh-CN  "\wide启动中按\narrow"
#string L05_STR_GAMING_DURING_BOOT_2                            #language en-US  "During Boot"
                                                                #language zh-CN  "\wide键\narrow"
#string L05_STR_GAMING_NOVO_MENU                                #language en-US  "Novo Menu"
                                                                #language zh-CN  "Novo\wide菜单\narrow"
#string L05_STR_GAMING_CPU_INFO                                 #language en-US  "CPU"
                                                                #language zh-CN  "\wide处理器信息\narrow"
#string L05_STR_GAMING_BIOS_VERSION                             #language en-US  "BIOS Version"
                                                                #language zh-CN  "BIOS\wide版本\narrow"
#string L05_STR_GAMING_GPU_INFO                                 #language en-US  "GPU"
                                                                #language zh-CN  "\wide显卡信息\narrow"
#string L05_STR_GAMING_LENOVO_SN                                #language en-US  "Lenovo SN"
                                                                #language zh-CN  "\wide主机编号\narrow"
#string L05_STR_GAMING_EXIT                                     #language en-US  "Exit"
                                                                #language zh-CN  "\wide退出\narrow"
#string L05_STR_GAMING_MORE_SETTINGS                            #language en-US  "More Settings"
                                                                #language zh-CN  "\wide详细设置\narrow"
#string L05_STR_GAMING_THERMAL_MODE                             #language en-US  "Thermal Mode"
                                                                #language zh-CN  "\wide散热模式\narrow"
#string L05_STR_GAMING_PERFORMANCE                              #language en-US  "Performance"
                                                                #language zh-CN  "\wide野兽模式\narrow"
#string L05_STR_GAMING_BALANCE                                  #language en-US  "Balance"
                                                                #language zh-CN  "\wide均衡模式\narrow"
#string L05_STR_GAMING_QUIET                                    #language en-US  "Quiet"
                                                                #language zh-CN  "\wide安静模式\narrow"
#string L05_STR_GAMING_PERFORMANCE_MODE                         #language en-US  "Performance Mode"
                                                                #language zh-CN  "\wide性能模式\narrow"
#string L05_STR_GAMING_EXTREME_PERFORMANCE                      #language en-US  "Extreme Performance"
                                                                #language zh-CN  "\wide野兽模式\narrow"
#string L05_STR_GAMING_INTELLIGENT_COOLING                      #language en-US  "Intelligent Cooling"
                                                                #language zh-CN  "\wide智能模式\narrow"
#string L05_STR_GAMING_BETTERY_SAVING                           #language en-US  "Bettery Saving"
                                                                #language zh-CN  "\wide节能模式\narrow"
#string L05_STR_GAMING_PRESS_FN_Q_IN_WINDOWS_TO_SWITCH          #language en-US  "Press Fn+Q in Windows to Switch"
                                                                #language zh-CN  "Windows\wide下快捷键\narrowFn+Q\wide切换模式\narrow"
#string L05_STR_GAMING_BOOT_DEVICE                              #language en-US  "Boot Device"
                                                                #language zh-CN  "\wide启动设备\narrow"
#string L05_STR_GAMING_GRAPHIC_DEVICE                           #language en-US  "Graphic Device"
                                                                #language zh-CN  "\wide显卡模式\narrow"
#string L05_STR_GAMING_UMA_GRAPHICS                             #language en-US  "UMA Graphics"
                                                                #language zh-CN  "\wide内显模式\narrow"
#string L05_STR_GAMING_DYNAMIC_SWITCHABLE_GRAPHICS              #language en-US  "Dynamic/Switchable Graphics"
                                                                #language zh-CN  "\wide混合模式\narrow"
#string L05_STR_GAMING_DISCRETE_GRAPHICS                        #language en-US  "Discrete Graphics"
                                                                #language zh-CN  "\wide独显模式\narrow"

#include "L05GamingOverClockingUiString.uni"
