/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_CONFIG_H_
#define _L05_CONFIG_H_

#pragma pack(1)

//================================================================================
//                         L05 Feature Start
//================================================================================
  //  
  //  Current Spec version: Lenovo China Minimum BIOS Spec V1.39
  //  Please modify the LATEST_L05_SPEC_VERSION when update spec.
  //  
//[-start-211224-Ching000021-modify]//
  #define LATEST_L05_SPEC_VERSION       140
//[-end-211224-Ching000021-modify]//

  #define L05_INVALID_VALUE             (-1)
//================================================================================
//                         L05 Feature End
//================================================================================


//================================================================================
//                         L05 Logo Start
//================================================================================
//_Start_L05_LOGO_
  typedef enum {
    L05BgrtLogo = 0,
    L05MaxLogoType
  } EFI_L05_LOGO_TYPE;

  typedef struct _L05_LOGO_RESOLUTION_TABLE {
    UINT32                              X;// Location X
    UINT32                              Y;// Location Y
    EFI_GUID                            Guid;// File Guid
  } L05_LOGO_RESOLUTION_TABLE;

  #define EFI_OEM_BADGING_FILENAME_CUSTOMER_LOGO_1 \
    { \
      0x9415b75c, 0xe1b8, 0x4e2a, 0x98, 0x8d, 0xdd, 0x39, 0x9b, 0x9a, 0x98, 0x5e \
    }

  #define EFI_OEM_BADGING_FILENAME_CUSTOMER_LOGO_2 \
    { \
      0xc3c89ea3, 0x58a6, 0x43ed, 0xb6, 0x85, 0xe, 0xd5, 0x35, 0xb2, 0xe3, 0x6f  \
    }

  #define EFI_OEM_BADGING_FILENAME_CUSTOMER_LOGO_3 \
    { \
      0x4cd76a24, 0xc25b, 0x4e60, 0x9b, 0x4f, 0x25, 0xe9, 0x6b, 0x9e, 0xbf, 0xf6 \
    }

  #define EFI_OEM_BADGING_FILENAME_ENGYSTAR \
    { \
      0x846192c0, 0x9728, 0x4614, 0xbc, 0x38, 0x0b, 0x53, 0xb6, 0x09, 0x2f, 0xd2 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_768x1024 \
    { \
      0x9DF7F118, 0xBA9F, 0x43a7, 0xAD, 0xE4, 0xB9, 0xA1, 0x1B, 0x6C, 0x3A, 0x02  \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_800x1280 \
    { \
      0xC9AC7314, 0xC250, 0x4AEC, 0x8E, 0x34, 0xB0, 0x49, 0x0A, 0x75, 0x66, 0x3C \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_2160x1440 \
    { \
      0x0F85BFDB, 0xB54B, 0x4C5E, 0x85, 0xE6, 0x6A, 0x94, 0x19, 0xF8, 0x14, 0xF6 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_1024x600 \
    { \
      0x15b009cf, 0xa818, 0x4927, 0xa0, 0xed, 0x2b, 0x4f, 0x5, 0x7e, 0xa5, 0xa8 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_1280x768 \
    { \
      0XEC5A76B5, 0XEB48, 0X4321, 0XB8, 0XDF, 0XD2, 0X43, 0X7F, 0X4F, 0X46, 0X41 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_1280x800 \
    { \
      0x00F5ADCE, 0xFF3A, 0x498B, 0x91, 0xD8, 0x67, 0x92, 0x50, 0xB4, 0x08, 0x49 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_1366x768 \
    { \
      0xFCE82B05, 0x526C, 0x436F, 0xAE, 0x1A, 0x93, 0xAE, 0x75, 0x7F, 0x3E, 0x43 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_1600x900 \
    { \
      0xF333BD43, 0x5C62, 0x4F93, 0xBB, 0x9D, 0x0E, 0x3A, 0xC3, 0x83, 0x91, 0x7B \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1080 \
    { \
      0x156A8FFE, 0x62DB, 0x4FF3, 0x82, 0xAD, 0x2E, 0xBD, 0x8A, 0x3E, 0x3D, 0xF7 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_1920x1200 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xE2 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_2880x1800 \
    { \
      0x7ED15186, 0xB06C, 0x4CEF, 0x92, 0xDA, 0x6E, 0x02, 0x90, 0x43, 0x69, 0xF3 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_3072x1920 \
    { \
      0xC57A67F9, 0xCD1F, 0x4234, 0xBB, 0xE9, 0x7F, 0x2D, 0x9F, 0xED, 0x8C, 0xA4 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_3200x1800 \
    { \
      0x742EC58A, 0x17FD, 0x414E, 0x8E, 0xA3, 0x2A, 0x4F, 0x52, 0xC8, 0x3A, 0xF4 \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_3480x2160 \
    { \
      0x82331078, 0xD0A4, 0x4004, 0x92, 0x32, 0xD7, 0x36, 0x81, 0x9F, 0xF4, 0x7E \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_3840x2160 \
    { \
      0x1F56B2F9, 0x6E6D, 0x4014, 0xBF, 0xD4, 0x37, 0xC9, 0xE5, 0xD3, 0x98, 0xF1 \
    }

//[-start-210702-Dongxu0006-add]//
//[-start-211001-Ching000008-add]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211001-Ching000008-add]//
  #define EFI_OEM_BADGING_FILENAME_JPEG_L05_1920x1200 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xE3 \
    }

  #define EFI_OEM_BADGING_FILENAME_JPEG_L05_2880x1800 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xE4 \
    }
  
  #define EFI_OEM_BADGING_FILENAME_JPEG_L05_3840x2400 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xE5 \
    }
//[-start-210830-STORM1110-modify]//
  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_2240x1400 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xE6 \
    }
  
  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_2560x1600 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xE7 \
    }
#endif
//#[-start-211026-Dongxu0025-add]##  yoga logo for C970
#if defined(C970_SUPPORT)
  #define EFI_OEM_BADGING_YOGA_JPEG_L05_1920x1200 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xE8 \
    }

  #define EFI_OEM_BADGING_YOGA_JPEG_L05_2880x1800 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xE9 \
    }

  #define EFI_OEM_BADGING_YOGA_JPEG_L05_3840x2400 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xEA \
    }
#endif
//#[-end-211026-Dongxu0025-add]##  yoga logo for C970

//#[-start-211027-Dongxu0026-add]## 
#if defined(C770_SUPPORT)
  #define EFI_OEM_BADGING_YOGA_JPEG_L05_2240x1400 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xE8 \
    }

  #define EFI_OEM_BADGING_YOGA_JPEG_L05_2880x1800 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xE9 \
    }

#endif
//#[-end-211027-Dongxu0026-add]##  
//[-start-21111-TAMT000031-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
  #define EFI_OEM_BADGING_YOGA_JPEG_L05_2240x1400 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xEB \
    }

  #define EFI_OEM_BADGING_YOGA_JPEG_L05_2880x1800 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xEC \
    }
#endif
//[-start-220412-Ching000044-modify]//
//[-start-220422-Ching000046-modify]//
#if defined(S77014IAH_SUPPORT) || defined(S77014_SUPPORT)
  #define EFI_OEM_BADGING_FILENAME_XIAOXIN_L05_2240x1400 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xEE \
    }

  #define EFI_OEM_BADGING_FILENAME_XIAOXIN_L05_2880x1800 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xEF \
    }
#endif
//[-end-220422-Ching000046-modify]//
//[-end-220412-Ching000044-modify]//
#if defined(S77013_SUPPORT)
  #define EFI_OEM_BADGING_YOGA_JPEG_L05_2560x1600 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xED \
    }
#endif
//[-end-21111-TAMT000031-add]//
//[-end-210830-STORM1110-modify]//
//[-end-210702-Dongxu0006-add]//

//[-start-210906-QINGLIN0043-add]//
#if defined(S570_SUPPORT)
  #define EFI_OEM_BADGING_FILENAME_XIAOXIN_L05_1920x1080 \
    { \
      0x12050FC0, 0xF57F, 0x4CF4, 0xAB, 0xED, 0xEB, 0xDF, 0xED, 0x4C, 0x31, 0xF8 \
    }
#endif
//[-end-210906-QINGLIN0043-add]//
 
  #define EFI_OEM_BADGING_FILENAME_BGRT_L05_3840x2400 \
    { \
      0x4E4F5299, 0x915C, 0x42CF, 0x8D, 0x12, 0xCB, 0xB0, 0xD9, 0x5C, 0x26, 0x2D \
    }

  #define EFI_OEM_BADGING_FILENAME_BGRT_IP_640x360 \
    { \
      0x72C83F78, 0x0F54, 0x400B, 0xBD, 0x61, 0x24, 0x24, 0x7E, 0xD5, 0x9E, 0x54 \
    }
//_End_L05_LOGO_

//================================================================================
//                         L05 Logo End
//================================================================================


//================================================================================
//                         Acpi Table Id Start
//================================================================================
#ifdef L05_ACPI_TABLE_ID_ENABLE
  #define EFI_L05_ACPI_OEM_ID             SIGNATURE_64 ('L','E','N','O','V','O',' ',' ') // OEMID 6 bytes long
  #define EFI_L05_ACPI_OEM_ID_MASK        0x0000FFFFFFFFFFFF
  #define EFI_L05_ACPI_OEM_TABLE_ID       SIGNATURE_64 ('C','B','-','0','1',' ',' ',' ') // OEM table id 8 bytes long
  #define EFI_L05_ACPI_OEM_TABLE_ID_MASK  0x000000FFFFFFFFFF

#endif // #ifdef L05_ACPI_TABLE_ID_ENABLE
//================================================================================
//                         Acpi Table Id End
//================================================================================


//================================================================================
//                         SLP1.0 Start
//================================================================================
//_Start_L05_SLP_STRING_
  //                                        0    1    2    3    4    5    6    7    8    9    A    B    C     D     E     F
  #define L05_SLP10_STRING              {  'L', 'E', 'G', 'E', 'N', 'D', ' ', 'D', 'r', 'a', 'g', 'o', 'n', 0x00, 0x00, 0x00 }
  #define L05_SLP10_STRING2             {  'L', 'E', 'N', 'O', 'V', 'O', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
//_End_L05_SLP_STRING_
//================================================================================
//                         SLP1.0 End
//================================================================================


//================================================================================
//                         HDD Password Start
//================================================================================
  #define EFI_L05_SYSTEM_PASSWORD_MAX_RETRY  0x03
  #define EFI_L05_HDD_PASSWORD_MAX_RETRY     0x02
//================================================================================
//                         HDD Password End
//================================================================================


//================================================================================
//                         SetupMenu Start
//================================================================================

  typedef struct {
    UINT8                               Bus;
    UINT8                               Device;
    UINT8                               Function;
    UINT8                               PortNumber;
  } EFI_L05_SATA_HDD_ODD_LOCATION;

  #define EFI_L05_BOOT_DEVICE_NAME_MAX_SIZE   0x20
  #define EFI_L05_BOOT_DEVICE_NAME_TABLE_MAX  0x20

  typedef struct _EFI_L05_BOOT_DEVICE_NAME {
    UINT16                              DeviceType;
    CHAR16                              NameString[EFI_L05_BOOT_DEVICE_NAME_MAX_SIZE];
  } EFI_L05_BOOT_DEVICE_NAME;

  #define L05_BOOT_TYPE_BBS_FIRST_SATA_HDD     ((0x01) * 256 + BBS_HARDDISK)
  #define L05_BOOT_TYPE_BBS_SECOND_SATA_HDD    ((0x02) * 256 + BBS_HARDDISK)
  #define L05_BOOT_TYPE_BBS_USB_HDD            ((BBS_HARDDISK) * 256 + BBS_USB)
  #define L05_BOOT_TYPE_BBS_USB_CDROM          ((BBS_CDROM)    * 256 + BBS_USB)
  #define L05_BOOT_TYPE_BBS_USB_FLOPPY         ((BBS_FLOPPY)   * 256 + BBS_USB)
  #define L05_BOOT_TYPE_BBS_USB_MEMORY         0x07

  #define L05_BOOT_TYPE_EFI_FIRST_SATA_HDD     ((0xE0 + 0x01) * 256 + BBS_HARDDISK)
  #define L05_BOOT_TYPE_EFI_SECOND_SATA_HDD    ((0xE0 + 0x02) * 256 + BBS_HARDDISK)
  #define L05_BOOT_TYPE_EFI_HARDDISK           ((0xE0)* 256         + BBS_HARDDISK)
  #define L05_BOOT_TYPE_EFI_USB_HDD            ((0xE0 + BBS_HARDDISK) * 256 + BBS_USB)
  #define L05_BOOT_TYPE_EFI_USB_CDROM          ((0xE0 + BBS_CDROM)    * 256 + BBS_USB)
  #define L05_BOOT_TYPE_EFI_CDROM              ((0xE0)* 256         + BBS_CDROM)
  #define L05_BOOT_TYPE_EFI_FLOPPY             ((0xE0)* 256         + BBS_FLOPPY)
  #define L05_BOOT_TYPE_EFI_USB_FLOPPY         ((0xE0 + BBS_FLOPPY)   * 256 + BBS_USB)
  #define L05_BOOT_TYPE_EFI_USB_MEMORY         ((0xE0 + L05_BOOT_TYPE_BBS_USB_MEMORY) * 256 + BBS_USB)
  #define L05_BOOT_TYPE_EFI_NETWORK            ((0xE0)* 256         + BBS_EMBED_NETWORK)
  #define L05_BOOT_TYPE_EFI_NETWORK_WIFI       ((0xE0 + 0x0A) * 256 + BBS_EMBED_NETWORK)
  #define L05_BOOT_TYPE_EFI_LENOVO_CLOUD       ((0xE0 + 0x08) * 256 + BBS_EMBED_NETWORK)
  #define L05_BOOT_TYPE_EFI_LENOVO_CLOUD_WIFI  ((0xE0 + 0x09) * 256 + BBS_EMBED_NETWORK)
  #define L05_BOOT_TYPE_NOVO_RECOVERY_SYSTEM   ((0xE0 + 0x05) * 256 + BBS_HARDDISK)
  #define L05_BOOT_TYPE_EFI_PORTABLE_DEVICE    ((0xE0 + 0x08) * 256 + BBS_USB)
  #define L05_BOOT_TYPE_EFI_UNKNOWN            0xE0FF
  #define L05_BOOT_TYPE_NON_EFI                0xE0BB

  #define L05_BOOT_TYPE_END_OF_LIST            0xFFFF

  //
  //  For PXE Combine Feature
  //
  //  Please make sure the PXE boot option description string at BuildNetworkBootOption function in BdsBoot.c.
  //  H2O Rev5.2 kernel PXE boot option description string is "EFI PXE %d for IPv%d (%02x-%02x-%02x-%02x-%02x-%02x)"
  //  L05_COMPARE_NETWORK_STRING should be follow kernel PXE boot option description to set to "EFI PXE"
  //
  #define L05_PXE_NETWROK_STRING               L"EFI PXE Network"
  #define L05_NETWORK_OPTION_STRING            L"EFI PXE Network (%02x-%02x-%02x-%02x-%02x-%02x)"
  #define L05_NETWORK_DUMMY_OPTION_STRING      L"EFI PXE Network (88-88-88-88-87-88)"
  #define L05_PXE_IPV4_BOOT_FAIL_STRING        L"EFI PXE 0 for IPv4 (88-88-88-88-87-88)"
  #define L05_PXE_IPV6_BOOT_FAIL_STRING        L"EFI PXE 0 for IPv6 (88-88-88-88-87-88)"
  #define L05_COMPARE_NETWORK_STRING           L"EFI PXE"
  #define L05_MAX_SUPPORT_PXE_DEVICE           3

  //
  // For HTTP Combine Feature
  //
  #define L05_CLOUD_SERVICES_STRING            L"Lenovo Cloud Services"
  #define L05_MAX_SUPPORT_HTTP_DEVICE          3

  //
  // For Display Dialog
  //
  #define L05_DIALOG_PAD_FRONT_SPACE_WIDTH           1
  #define L05_DIALOG_PAD_LAST_SPACE_WIDTH            4
  #define L05_PASSWORD_DIALOG_PAD_FRONT_SPACE_WIDTH  1
  #define L05_PASSWORD_DIALOG_PAD_MIDDLE_SPACE_WIDTH 2
  #define L05_PASSWORD_DIALOG_PAD_LAST_SPACE_WIDTH   4

  //
  // L05_SECURITY_ERASE
  //
  #define L05_SECURITY_ERASE_PERCENTAGE_FORMAT_STRING  L"%3d%%"
  #define L05_SECURITY_ERASE_DEFAULT_TIME_SECOND       30
  #define L05_SECURITY_ERASE_PROGRESS_RING_MICROSECOND 250

  #define L05_GET_NUMBER_LENGTH(Number, NumberSize) do { \
    UINTN TempNumber; \
    TempNumber = Number; \
    NumberSize = 0; \
    while (TempNumber != 0) { \
      TempNumber /= 10; \
      NumberSize++; \
    } \
  } while (0)

  #define L05_GET_HEX_NUMBER_LENGTH(Number, NumberSize) do { \
    UINTN TempNumber; \
    TempNumber = Number; \
    NumberSize = 0; \
    while (TempNumber != 0) { \
      TempNumber /= 0x10; \
      NumberSize++; \
    } \
  } while (0)

  #define L05_RFC_3066_ENGLISH_CODE                    "en-US"
  #define L05_RFC_3066_SIMPLIFIED_CHINESE_CODE         "zh-CN"

//================================================================================
//                         SetupMenu End
//================================================================================

//================================================================================
//                         SMBIOS Start
//================================================================================
  //
  // Please modify this according to actual status
  //    Follow L05 Spec V1.15 3.4.6 SMBIOS,
  //    System BIOS Major Version is always 0 prior to shiping the product.
  //    Always "1" after a products ships unless a major change occurs which then development can increment this number
  //
  #define L05_SMBIOS_TYPE00_BIOS_MAJOR_VERSION  0x00

  //
  //  Follow L05 Spec V1.15 3.4.6 SMBIOS,
  //  These setting may reference to L05Config.h
  //
  #define L05_SMBIOS_TYPE00_ENCLOSURE_TYPE      EFI_L05_SMBIOS_TYPE00_ENCLOSURE_TYPE_PORTABLE
  #define L05_SMBIOS_TYPE00_BRAND_TYPE          EFI_L05_SMBIOS_TYPE00_BRAND_TYPE_L05_CONSUMER_NOTEBOOK

  #define L05_SMBIOS_TYPE11_COUNTRY_CODE_STRING    "Country - XX"
  #define L05_SMBIOS_TYPE11_REGION_CODE_STRING     "Region - %a"
  #define L05_SMBIOS_ISO_3166_ALPHA_2_CODE_STRING  "XX"
  #define L05_SMBIOS_TYPE11_MODERN_PRELOAD_STRING  "Modern Preload"

//================================================================================
//                         SMBIOS End
//================================================================================

//================================================================================
//                         EEPROM Start
//================================================================================

  //
  //  Definition of EEPROM Offset and Length.
  //  Based on LENOVO_CHINA_EEPROMContents_and_Tools_Spsc V1.19 for Lenovo China Minimum BIOS Spec V.135
  //
#if (LATEST_L05_SPEC_VERSION == 135)
  #define L05_EEPROM_PRODUCT_NAME_OFFSET           0x00
  #define L05_EEPROM_PRODUCT_NAME_LENGTH           0x20
  #define L05_EEPROM_RESERVED_1_OFFSET             0x20
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_2_OFFSET             0x30
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_3_OFFSET             0x70
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_4_OFFSET             0x80
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_5_OFFSET             0x90
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_6_OFFSET             0x70
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_SERIAL_NUMBER_OFFSET          0x80
  #define L05_EEPROM_SERIAL_NUMBER_LENGTH          0x20
  #define L05_EEPROM_RESERVED_7_OFFSET             0xA0
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_UUID_OFFSET                   0xB0
  #define L05_EEPROM_UUID_LENGTH                   0x10
  #define L05_EEPROM_RESERVED_8_OFFSET             0xC0
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_9_OFFSET             0xD0
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_PROJECT_NAME_OFFSET           0xE0
  #define L05_EEPROM_PROJECT_NAME_LENGTH           0x20
  #define L05_EEPROM_MISC_ATTRIBUTES_OFFSET        0x100
  #define L05_EEPROM_MISC_ATTRIBUTES_LENGTH        0x10
  #define L05_EEPROM_FAMILY_NAME_OFFSET            0x110
  #define L05_EEPROM_FAMILY_NAME_LENGTH            0x10
  #define L05_EEPROM_ASSET_TAG_OFFSET              0x120
  #define L05_EEPROM_ASSET_TAG_LENGTH              0x10
  #define L05_EEPROM_OS_PN_NUMBER_OFFSET           0x130
  #define L05_EEPROM_OS_PN_NUMBER_LENGTH           0x10
  #define L05_EEPROM_OS_DESCRIPTOR_OFFSET          0x140
  #define L05_EEPROM_OS_DESCRIPTOR_LENGTH          0x20
  #define L05_EEPROM_MACHINE_TYPE_MODEL_OFFSET     0x160
  #define L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH     0x20
  #define L05_EEPROM_OA3_KEY_ID_OFFSET             0x180
  #define L05_EEPROM_OA3_KEY_ID_LENGTH             0x20
  #define L05_EEPROM_OA3_KEY_ID_SIZE_USED          15

#else
  //
  //  Definition of EEPROM Offset and Length.
  //  Based on LENOVO_CHINA_EEPROMContents_and_Tools_Spsc V1.20 for Lenovo China Minimum BIOS Spec V.138
  //
  #define L05_EEPROM_PRODUCT_NAME_OFFSET           0x00
  #define L05_EEPROM_PRODUCT_NAME_LENGTH           0x40
  #define L05_EEPROM_RESERVED_1_OFFSET             0x40
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_2_OFFSET             0x50
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_3_OFFSET             0x60
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_4_OFFSET             0x70
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_5_OFFSET             0x80
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_RESERVED_6_OFFSET             0x90
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_SERIAL_NUMBER_OFFSET          0xA0
  #define L05_EEPROM_SERIAL_NUMBER_LENGTH          0x20
  #define L05_EEPROM_RESERVED_7_OFFSET             0xC0
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_UUID_OFFSET                   0xD0
  #define L05_EEPROM_UUID_LENGTH                   0x10
  #define L05_EEPROM_RESERVED_8_OFFSET             0xE0
  #define L05_EEPROM_RESERVED_LENGTH               0x10
  #define L05_EEPROM_TPM_LOCK_OFFSET               0xF0
  #define L05_EEPROM_TPM_LOCK_LENGTH               0x01
  #define L05_EEPROM_COMPUTRACE_ERASE_FLAG_OFFSET  0xF1
  #define L05_EEPROM_COMPUTRACE_ERASE_FLAG_LENGTH  0x01
  #define L05_EEPROM_BIOS_SELF_HEALING_FLAG_OFFSET 0xF2
  #define L05_EEPROM_BIOS_SELF_HEALING_FLAG_LENGTH 0x01
  #define L05_EEPROM_RESERVED_9_OFFSET             0xF3
  #define L05_EEPROM_RESERVED_9_LENGTH             0x0D
  #define L05_EEPROM_PROJECT_NAME_OFFSET           0x100
  #define L05_EEPROM_PROJECT_NAME_LENGTH           0x40
  #define L05_EEPROM_MISC_ATTRIBUTES_OFFSET        0x140
  #define L05_EEPROM_MISC_ATTRIBUTES_LENGTH        0x10
  #define L05_EEPROM_FAMILY_NAME_OFFSET            0x150
  #define L05_EEPROM_FAMILY_NAME_LENGTH            0x40
  #define L05_EEPROM_ASSET_TAG_OFFSET              0x190
  #define L05_EEPROM_ASSET_TAG_LENGTH              0x10
  #define L05_EEPROM_OS_PN_NUMBER_OFFSET           0x1A0
  #define L05_EEPROM_OS_PN_NUMBER_LENGTH           0x10
  #define L05_EEPROM_OS_DESCRIPTOR_OFFSET          0x1B0
  #define L05_EEPROM_OS_DESCRIPTOR_LENGTH          0x20
  #define L05_EEPROM_MACHINE_TYPE_MODEL_OFFSET     0x1D0
  #define L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH     0x40
  #define L05_EEPROM_OA3_KEY_ID_OFFSET             0x210
  #define L05_EEPROM_OA3_KEY_ID_LENGTH             0x20
  #define L05_EEPROM_OA3_KEY_ID_SIZE_USED          15
#endif 

  //
  //  Definition of EEPROM Spsc V1.20 Map for L05 BIOS Spec V1.38. This is only for reference.
  //
  typedef struct _EFI_L05_MISC_ATTRIBUTES_EEPROM_MAP {
    UINT8                               KeyboardId;
    UINT8                               BrandTypeId;
    UINT8                               EPAFlag;
    UINT8                               FunctionFlag;
    UINT8                               Customer;
    UINT8                               Reserved[11];
  } EFI_L05_MISC_ATTRIBUTES_EEPROM_MAP;

  //
  //  Definition of EEPROM Spsc V1.20 Map for L05 BIOS Spec V1.38. This is only for reference.
  //
  //------+--------------------------------------+
  //|0x0  | Product Name                         |
  //+-----+--------------------------------------+
  //|0x40 | Reserved 1                           |
  //+-----+--------------------------------------+
  //|0x50 | Reserved 2                           |
  //+-----+--------------------------------------+
  //|0x60 | Reserved 3                           |
  //+-----+--------------------------------------+
  //|0x70 | Reserved 4                           |
  //+-----+--------------------------------------+
  //|0x80 | Reserved 5                           |
  //+-----+--------------------------------------+
  //|0x90 | Reserved 6                           |
  //+-----+--------------------------------------+
  //|0xA0 | Lenovo Serial Number (SMBIOS Type02) |
  //+-----+--------------------------------------+
  //|0xC0 | Reserved 7                           |
  //+-----+--------------------------------------+
  //|0xD0 | UUID (DMI TYPE01)                    |
  //+-----+--------------------------------------+
  //|0xE0 | Reserved 8                           |
  //+-----+--------------------------------------+
  //|0xF0 | Reserved 9                           |
  //+-----+--------------------------------------+
  //|0x100| Project Name                         |
  //+-----+--------------------------------------+
  //|0x140| Misc Attributes                      |
  //+-----+--------------------------------------+
  //|0x150| Family Name                          |
  //+-----+--------------------------------------+
  //|0x190| No Asset Tag                         |
  //+-----+--------------------------------------+
  //|0x1A0| OS PN Number                         |
  //+-----+--------------------------------------+
  //|     |                                      |
  //|0x1B0| OS Descriptor                        |
  //|     |                                      |
  //+-----+--------------------------------------+
  //|     |                                      |
  //|0x1D0| MTM (SMBIOS TYPE01 Product Name)     |
  //|     |                                      |
  //+-----+--------------------------------------+
  //|     |                                      |
  //|0x210| OA3 Key ID                           | (NOT the same as OA 3.0 Key in ACPI MSDM Table!!!)
  //|     |                                      |
  //+-----+--------------------------------------+

  typedef struct _EFI_L05_EEPROM_MAP_119 {
    UINT8                               ProductName[L05_EEPROM_PRODUCT_NAME_LENGTH];
    UINT8                               Reserved1[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved2[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved3[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved4[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved5[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved6[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               L05SerialNumber[L05_EEPROM_SERIAL_NUMBER_LENGTH];
    UINT8                               Reserved7[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Uuid[L05_EEPROM_UUID_LENGTH];
    UINT8                               Reserved8[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved9[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               ProjectName[L05_EEPROM_PROJECT_NAME_LENGTH];
    EFI_L05_MISC_ATTRIBUTES_EEPROM_MAP  L05MiscAttributes;
    UINT8                               FamilyName[L05_EEPROM_FAMILY_NAME_LENGTH];
    UINT8                               AssetTag[L05_EEPROM_ASSET_TAG_LENGTH];
    UINT8                               OSPNNumber[L05_EEPROM_OS_PN_NUMBER_LENGTH];
    UINT8                               OSDescriptor[L05_EEPROM_OS_DESCRIPTOR_LENGTH];
    UINT8                               MachineTypeModel[L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH];
    UINT8                               Oa3KeyId[L05_EEPROM_OA3_KEY_ID_LENGTH];
  } EFI_L05_EEPROM_MAP_119;

  typedef struct _EFI_L05_EEPROM_MAP_120 {
    UINT8                               ProductName[L05_EEPROM_PRODUCT_NAME_LENGTH];
    UINT8                               Reserved1[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved2[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved3[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved4[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved5[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Reserved6[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               L05SerialNumber[L05_EEPROM_SERIAL_NUMBER_LENGTH];
    UINT8                               Reserved7[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               Uuid[L05_EEPROM_UUID_LENGTH];
    UINT8                               Reserved8[L05_EEPROM_RESERVED_LENGTH];
    UINT8                               TpmLock[L05_EEPROM_TPM_LOCK_LENGTH];
    UINT8                               ComputraceEraseFlag[L05_EEPROM_COMPUTRACE_ERASE_FLAG_LENGTH];
    UINT8                               BiosSelfHealingFlag[L05_EEPROM_BIOS_SELF_HEALING_FLAG_LENGTH];
    UINT8                               Reserved9[L05_EEPROM_RESERVED_9_LENGTH];
    UINT8                               ProjectName[L05_EEPROM_PROJECT_NAME_LENGTH];
    EFI_L05_MISC_ATTRIBUTES_EEPROM_MAP  L05MiscAttributes;
    UINT8                               FamilyName[L05_EEPROM_FAMILY_NAME_LENGTH];
    UINT8                               AssetTag[L05_EEPROM_ASSET_TAG_LENGTH];
    UINT8                               OSPNNumber[L05_EEPROM_OS_PN_NUMBER_LENGTH];
    UINT8                               OSDescriptor[L05_EEPROM_OS_DESCRIPTOR_LENGTH];
    UINT8                               MachineTypeModel[L05_EEPROM_MACHINE_TYPE_MODEL_LENGTH];
    UINT8                               Oa3KeyId[L05_EEPROM_OA3_KEY_ID_LENGTH];
  } EFI_L05_EEPROM_MAP_120;

//================================================================================
//                         EEPROM End
//================================================================================


//================================================================================
//                         L05 System Beep Start
//================================================================================
typedef enum {
  PASSWORD_INVALID_KEY_BEEP = 0
} L05_SYSTEM_BEEP_TYPE;
//================================================================================
//                         L05 System Beep End
//================================================================================

//================================================================================
//                         L05 Graphic UI Start
//================================================================================
typedef enum {
  Yoga = 0,
  Ideapad,
  Legion,
  XiaoXin,
//[-start-220410-Ching000042-modify]//
//[-start-220412-Ching000044-modify]//
//[-start-220422-Ching000046-modify]//
#if defined(S77013_SUPPORT) || defined(S77014_SUPPORT)
  LenovoNA,
#endif
#if defined(S77014IAH_SUPPORT) || defined(S77014_SUPPORT)
  XiaoXinIAH,
#endif
//[-end-220422-Ching000046-modify]//
//[-end-220412-Ching000044-modify]//
//[-end-220410-Ching000042-modify]//
#ifdef L05_SMB_BIOS_ENABLE
  ThinkBook,
#endif
  MaxBrandingLogoType
} L05_BRANDING_LOGO_TYPE;

typedef enum {
  Its30PerformanceMode = 0,
  Its30BalanceMode,
  Its30QuietMode,
  Its40ExtremeMode,
  Its40IntelligentMode,
  Its40SavingMode,
  MaxPerformanceMode
} L05_PERFORMANCE_MODE;
//================================================================================
//                         L05 Graphic UI End
//================================================================================

//================================================================================
//                         L05 OEM Switch Trigger Point Start
//================================================================================
typedef enum {
  DuringPost = 0,
  RouteConfig,
} L05_OEM_SWITCH_TRIGGER_POINT_TYPE;
//================================================================================
//                         L05 OEM Switch Trigger Point End
//================================================================================

//================================================================================
//                         L05 Crisis Recovery Start
//================================================================================
  #define L05_CRISIS_BIOS_NAME          L"xxCN.bin"
//================================================================================
//                         L05 Crisis Recovery End
//================================================================================

#pragma pack()
#endif // _L05_CONFIG_H_
