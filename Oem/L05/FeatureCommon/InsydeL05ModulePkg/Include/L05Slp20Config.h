/** @file

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_SLP20_CONFIG_H_
#define _L05_SLP20_CONFIG_H_

#include <IndustryStandard/SLP2_0.h>  // EFI_ACPI_SLP_MARKER_STRUCTURE
#include <IndustryStandard/Oa3_0.h>   // EFI_ACPI_MSDM_DATA_STRUCTURE

//================================================================================
//                         L05 SLP2.0 Start
//================================================================================
#define EFI_L05_SLP20_MARKER_TYPE       0x00000001  //  Type : Marker
#define EFI_L05_SLP_MARKER_INVALID      0xFFFFFFFF
#define EFI_L05_SLP20_MARKER_VERSION    0x00020000  //  Ver 2.0

#define EFI_L05_SLP20_PUB_KEY \
  { \
    0xa302f9be, 0x8504, 0x4505, 0x81, 0xa, 0x16, 0x5e, 0x80, 0x78, 0xe8, 0x9b \
  }

#define EFI_L05_SLP20_MFG_STATE_MASK    3

typedef enum {
  EFI_L05_SLP20_MFG_RSV0,
  EFI_L05_SLP20_MFG_RSV1,
  EFI_L05_SLP20_MFG_DONE,
  EFI_L05_SLP20_MFG_NOT_DONE,
  EFI_L05_SLP20_MFG_MAX_STATE = 0xFF
} EFI_L05_SLP20_MFG_STATE;


#pragma pack(1)

typedef struct _EFI_L05_SLP20_RANDOM_INFO {
  UINT8                                 RandomNum0;
  UINT8                                 RandomNum1;
  UINT8                                 RandomNum2;
  UINT8                                 RandomNum3;
  UINT8                                 RandomNum4;
  UINT8                                 RandomNum5;
  UINT8                                 RandomNum6;
  UINT8                                 RandomNum7;
} EFI_L05_SLP20_RANDOM_INFO;

typedef struct _EFI_L05_SLP20_AREA {
  EFI_L05_SLP20_RANDOM_INFO             RandomInfo;
  UINT8                                 Manufacture;
  EFI_ACPI_SLP_MARKER_STRUCTURE         MarkerInfo;
  UINT8                                 SLP20Reserved[0x141];
  EFI_ACPI_MSDM_DATA_STRUCTURE          OA30MsdmData;
  UINT8                                 OA30Reserved[0x31];
} EFI_L05_SLP20_AREA;

#pragma pack()
//================================================================================
//                         L05 SLP2.0 End
//================================================================================
#endif
