/** @file
  Configuration definition for Wireless Device Support

;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_WIRELESS_DEVICE_SUPPORT_CONFIG_H_
#define _L05_WIRELESS_DEVICE_SUPPORT_CONFIG_H_

#define EFI_L05_BIOS_LOCK_TABLE_MAX     0x100

//
// Definition of Support Brand
//
typedef enum {
  BrandName_Broadcom = 0,
  BrandName_Intel,
  BrandName_Qualcomm,
  BrandName_Realtek,
  InvalidBrandName
} L05_WIRELESS_DEVICE_SUPPORT_BRAND;

//
// Definition of Device type
//
typedef enum {
  WLAN_DEVICE = 0,
  WWAN_DEVICE,
  UnsupportDeviceType
} L05_WIRELESS_DEVICE_SUPPORT_TYPE;

//
// Definition of Interface
//
typedef enum {
  PCI_INTERFACE = 0,
  USB_INTERFACE,
  UnsupportInterface
} L05_WIRELESS_DEVICE_SUPPORT_INTERFACE;

//
// Definition of setting for SMBIOS Type 133(0x85)
//
#define L05_SMBIOS_TYPE_133_OEM_DATA_STRING         "KHOIHGIUCCHHII"

//
// Definition of setting for SMBIOS Type 248(0xF8)
//
#define L05_SMBIOS_TYPE_248_SIGNATURE               0x17AAACAC
#define L05_SMBIOS_TYPE_248_COMMON_DEFAULT_SETTING  0x0000

#pragma pack(1)
//
// Structure of PCI Location
//
typedef struct {
  UINT8                                 Bus;
  UINT8                                 Device;
  UINT8                                 Function;
} EFI_L05_PCI_LOCATION;

//
// Structure of USB Location
//
typedef struct {
  UINT8                                 Device;
  UINT8                                 Function;
  UINT8                                 Port;
} EFI_L05_USB_LOCATION;

//
// Structure of Wireless Device Information
//
typedef struct {
  UINT8                                 DeviceType;
  UINT8                                 Interface;
  UINT16                                VendorId;
  UINT8                                 VendorNameId;
  UINTN                                 SizeofSettingValue;
  VOID                                  *SettingValue;
} EFI_L05_SUPPORTED_WIRELESS_DEVICE_INFO;

//
// Structure of Wireless Device Information for SMBIOS Type 248
//
typedef struct {
  UINT16                                CountryCode;  // Country Code
  UINT16                                SarRtl;       // SAR RTL
  UINT16                                SarBrcm;      // SAR BRCM
  UINT16                                SarQca;       // SAR QCA
  UINT16                                SarIntel;     // SAR Intel
} L05_SMBIOS_TYPE_248_WIRELESS_DEVICE_INFO;

#pragma pack()
#endif
