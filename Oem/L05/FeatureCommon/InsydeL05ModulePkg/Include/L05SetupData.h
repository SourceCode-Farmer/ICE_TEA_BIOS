/** @file
  L05 Setup Configuration Data

;******************************************************************************
;* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifdef _IMPORT_L05_SETUP_

//--------------------------------------------------------------------------------------------------------------
// The following area is used by Feature team to modify.
// The total size of variable in this part are fixed (70 bytes). That means if you need to add or remove
// variables, please modify the OEMRSV buffer size as well.
//--------------------------------------------------------------------------------------------------------------
  //Feature_Start
  UINT8         L05SetupVariableValid;
  UINT8         LogoVolume;
  UINT8         L05OdmWirelessLanImplement;
  UINT8         L05WirelessLan;
  UINT8         L05WlanSyncToChipsetWlan;
  UINT8         L05GraphicsDeviceSupport;
  UINT8         L05VtSupport;
  UINT8         L05HyperThreadingSupport;
  UINT8         L05BiosBackFlash;
  UINT8         L05HotKeyModeSupport;
  UINT8         L05HotKeyMode;
  UINT8         L05FoolProofFnCtrlSupport;
  UINT8         L05FoolProofFnCtrl;
  UINT8         L05AlwaysOnUsbSupport;
  UINT8         L05AlwaysOnUsb;
  UINT8         L05ChargeInBatteryModeSupport;
  UINT8         L05ChargeInBatteryMode;
  UINT8         L05DisableBuildingInBatterySupport;
  UINT8         L05IsNvmePresence;
  UINT8         CurrentSataInterfaceMode;
  UINT8         L05SetHddPasswordFlag;
  UINT8         L05OdmTpmImplement;
  UINT8         L05FtpmEnable;
  UINT8         L05TpmInterfaceType;
  UINT8         L05TpmLockDone;
  UINT8         ComputraceState;
  UINT8         L05ShowComputraceHelpStringInScu;
  UINT8         L05SecureBoot;
  UINT8         L05PlatformMode;
  UINT8         L05SecureBootMode;
  UINT8         L05FastBootImplement;
  UINT8         L05FastBoot;
  UINT8         L05PxeIpv4First;
  UINT8         L05OsOptimizedDefault;
  UINT8         L05OdmEthernetLanImplement;
  UINT8         L05EthernetLan;
  UINT8         L05OdmWirelessWanImplement;
  UINT8         L05WirelessWan;
  UINT8         L05OdmBluetoothImplement;
  UINT8         L05Bluetooth;
  UINT8         L05OdmUsbPortImplement;
  UINT8         L05UsbPort;
  UINT8         L05OdmMemoryCardSlotImplement;
  UINT8         L05MemoryCardSlot;
  UINT8         L05OdmSmartCardSlotImplement;
  UINT8         L05SmartCardSlot;
  UINT8         L05OdmIntegratedCameraImplement;
  UINT8         L05IntegratedCamera;
  UINT8         L05OdmMicrophoneImplement;
  UINT8         L05Microphone;
  UINT8         L05OdmFingerprintReaderImplement;
  UINT8         L05FingerprintReader;
  UINT8         L05OdmThunderboltImplement;
  UINT8         L05Thunderbolt;
  UINT8         L05OdmNfcDeviceImplement;
  UINT8         L05NfcDevice;
  UINT8         L05BiosSelfHealing;
  UINT8         L05VtdSupport;
  UINT8         L05DeviceGuardSupport;
  UINT8         L05DeviceGuard;
  UINT8         L05VmdControllerSupport;
  UINT8         L05MemorySizeGb;
  UINT8         L05UmaFrameBufferSizeSupport;
  UINT8         L05NaturalFileGuard;
  UINT8         L05SystemPerformanceModeSupport;
  UINT8         L05SystemPerformanceMode;
  UINT8         L05ThermalModeSupport;
  UINT8         L05ThermalMode;
  UINT8         OEMRSV[1];
  //
  // Notice!! Keep this item "OemAreaEndPoint" at the buttom of OEM area.
  //
  UINT8         OemAreaEndPoint;
  //Feature_End
#endif
