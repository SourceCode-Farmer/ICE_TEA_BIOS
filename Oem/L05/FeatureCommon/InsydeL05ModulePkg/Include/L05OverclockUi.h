/** @file
  Definition for Overclock UI

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_OVERCLOCK_UI_H_
#define _L05_OVERCLOCK_UI_H_

#include "H2OWindow/mwtypes.h"  // uint32_t
#include "H2OWindow/windef.h"  // HWND

#define BITS(a)                         (1 << (a))
#define VK_NULL                         0x00
#define OVERCLOCK_INVALID_VALUE         (UINTN) -1

typedef enum {
  ToggleButtonType = 0x00,
  OneOfListType,
  ButtonBarType,
} L05_OVERCLOCK_ITEM_TYPE;

typedef enum {
  CpuControl = 0x00,
  CpuAdvanced,
  CpuAdvancedGroup,
  GpuControl,
  GpuAdvanced,
  GpuAdvancedGroup,
  MemoryControl,
  MemoryGroup,
  AllOverclockItem,
} L05_OVERCLOCK_ITEM_GROUP;

typedef enum {
  ApplyFocusType = 0x00,
  CancelFocusType,
  SetToDefaultFocusType,
  OverclockLayoutFocusType,
} L05_OVERCLOCK_FOCUS_TYPE;

#pragma pack(1)
typedef struct {
  CHAR16                                *ItemName;
  L05_OVERCLOCK_ITEM_TYPE               ItemType;
  L05_OVERCLOCK_ITEM_GROUP              ItemGroup;
  EFI_QUESTION_ID                       QuestionId;
  H2O_STATEMENT_ID                      StatementId;
  HWND                                  Wnd;
  UINTN                                 Value;
  UINTN                                 ValueBase;
  UINTN                                 BaseOffset;
  BOOLEAN                               Visibility;
} L05_OVERCLOCK_SETUP_DATA;

typedef struct {
  L05_OVERCLOCK_FOCUS_TYPE           FocusType;
  HWND                                  *Wnd;
} L05_OVERCLOCK_FOCUS_TABLE;
#pragma pack()

#endif
