/** @file
  L05 Setup Configuration Data borrow ODMRSV[40]

;******************************************************************************
;* Copyright (c) 2012 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifdef _IMPORT_L05_SETUP_BORROW_ODMRSV_

//--------------------------------------------------------------------------------------------------------------
// The following area is used by Feature team to modify.
// The total size of variable in this part are fixed (40 bytes) from ODMRSV[40]. That means if you need to add
// or remove variables, please modify the OEMRSV buffer size as well.
//--------------------------------------------------------------------------------------------------------------
  //Feature_Start
  UINT8         L05CpuAdvancedOverclocking;
  UINT8         L05GpuOverclocking;
  UINT8         L05GpuAdvancedOverclocking;
  UINT8         L05GpuCoreClockOffset;
  UINT8         L05GpuCoreClockOffsetPrefix;
  UINT8         L05GpuVramClockOffset;
  UINT8         L05GpuVramClockOffsetPrefix;
  UINT8         L05GpuType;
  UINT8         L05CpuUnlock;
  UINT8         L05GpuUnlock;
  UINT8         OEMRSV3[10];
  UINT8         L05FlipToBootSupport;
  UINT8         L05FlipToBoot;
  UINT8         L05OneKeyBatterySupport;
  UINT8         L05OneKeyBattery;
  UINT8         L05RestoreDefaultOverclockingSupport;
  UINT8         L05RestoreDefaultOverclocking;
  UINT8         L05WakeOnLan;
  UINT8         L05WakeOnLanFromDock;
  UINT8         L05MacAddressPassThrough;
  UINT8         L05WakeOnVoiceSupport;
  UINT8         L05WakeOnVoice;
  UINT8         L05UltraQuietModeSupport;
  UINT8         L05UltraQuietMode;
  UINT8         L05LenovoCloudServices;
  UINT8         L05UefiWifiNetworkBoot;
  UINT8         L05InstantBootSupport;
  UINT8         L05InstantBoot;
  UINT8         L05EnhanceMemorySupport;
  UINT8         L05ActiveAtomCoresSupport;
  UINT8         OEMRSV2[1];
  //Feature_End
#endif
