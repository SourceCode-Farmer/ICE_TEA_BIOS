/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_WMI_SETUP_UNDER_OS_H_
#define _L05_WMI_SETUP_UNDER_OS_H_

#define L05_WMI_BUFFER_MAX_SIZE_BIT     8192 // 1024 Byte
#define L05_WMI_BUFFER_MAX_SIZE         (L05_WMI_BUFFER_MAX_SIZE_BIT >> 3) //L05_WMI_BUFFER_MAX_SIZE(Byte) = L05_WMI_BUFFER_MAX_SIZE_BIT / 8

#define L05_WMI_SETUP_ITEM_ENABLE        0x00
#define L05_WMI_SETUP_ITEM_DISABLE       0x01
#define L05_WMI_BOOT_MODE_UEFI           0x00
#define L05_WMI_BOOT_MODE_LEGACY_SUPPORT 0x01

#define WMI_SETUP_UNDER_OS_NULL_STR     ""
#define WMI_SETUP_UNDER_OS_ENTER_STR    "Enter"

//
// Object ID
//
#define L05_BIOS_SETTING_A0             0xA0
#define L05_SET_BIOS_SETTING_A1         0xA1
#define L05_SAVE_BIOS_SETTINGS_A2       0xA2
#define L05_DISCARD_BIOS_SETTINGS_A3    0xA3
#define L05_LOAD_DEFAULT_SETTINGS_A4    0xA4
#define L05_BIOS_PASSWORD_SETTINGS_A5   0xA5
#define L05_SET_BIOS_PASSWORD_A6        0xA6
#define L05_GET_BIOS_SELECTIONS_A7      0xA7

//
// WMI Return Status
//
#define L05_WMI_SUCCESS                 0x00
#define L05_WMI_NOT_SUPPORTED           0x01
#define L05_WMI_INVALID_PARAMETER       0x02
#define L05_WMI_ACCESS_DENIED           0x03
#define L05_WMI_SYSTEM_BUSY             0x04

#pragma pack (1)

typedef enum {
  L05WmiSetupItemInit = 0x00,
  L05WmiSetupItemSave,
  L05WmiSetupItemDiscard,
  L05WmiSetupItemLoadDefault,
} L05_WMI_SETUP_ITEM_ACTION;

//
// Need map PASSWORD_TYPE for SystemAdminType & SystemUserType
//
typedef enum {
  WmiSystemAdminType = 0x00,
  WmiSystemUserType,
  WmiHddUserType,
  WmiHddMasterType,
  WmiHddMaxType,
  WmiPasswoedMaxType,
} WMI_PASSWORD_TYPE;

typedef enum {
  WmiAsciiEncoding = 0x00,
  WmiScancodeEncoding,
  WmiPasswoedMaxEncoding,
} WMI_PASSWORD_ENCODING;

typedef enum {
  WmiUsLanguage = 0x00,
  WmiFrLanguage,
  WmiGrLanguage,
  WmiPasswoedMaxLanguage,
} WMI_PASSWORD_LANGUAGE;

typedef enum {
  WmiHddPasswordCurrentSection = 0x00,
  WmiHddPasswordNewSection,
  WmiHddPasswordMaxSection,
} WMI_HDD_PASSWORD_TABLE_SECTION;

typedef struct {
  UINT16                                BootOption;
  CHAR8                                 *BootDescription;
} L05_WMI_BOOT_DESCRIPTION;

typedef struct {
  //
  // Configuration Page
  //
  BOOLEAN                               L05LenovoCloudServicesValid;
  UINT8                                 L05LenovoCloudServices;
  BOOLEAN                               L05UefiWifiNetworkBootValid;
  UINT8                                 L05UefiWifiNetworkBoot;
  BOOLEAN                               L05UsbLegacyValid;
  UINT8                                 L05UsbLegacy;
  BOOLEAN                               L05WirelessLanValid;
  UINT8                                 L05WirelessLan;
  BOOLEAN                               L05SataControllerModeValid;
  UINT8                                 L05SataControllerMode;
  UINT8                                 L05VmdControllerValid;
  UINT8                                 L05VmdController;
  BOOLEAN                               L05GraphicsDeviceValid;
  UINT8                                 L05GraphicsDevice;
  BOOLEAN                               L05IntelVtValid;
  UINT8                                 L05IntelVt;
  BOOLEAN                               L05IntelVtdValid;
  UINT8                                 L05IntelVtd;
  BOOLEAN                               L05AmdVTechnologyValid;
  UINT8                                 L05AmdVTechnology;
  BOOLEAN                               L05IntelHyperThreadingValid;
  UINT8                                 L05IntelHyperThreading;
  BOOLEAN                               L05BiosBackFlashValid;
  UINT8                                 L05BiosBackFlash;
  BOOLEAN                               L05HotKeyModeValid;
  UINT8                                 L05HotKeyMode;
  BOOLEAN                               L05FoolProofFnCtrlValid;
  UINT8                                 L05FoolProofFnCtrl;
  BOOLEAN                               L05DptfValid;
  UINT8                                 L05Dptf;
  BOOLEAN                               L05IntelRapidStartValid;
  UINT8                                 L05IntelRapidStart;
  BOOLEAN                               L05AlwaysOnUsbValid;
  UINT8                                 L05AlwaysOnUsb;
  BOOLEAN                               L05ChargeInBatteryModeValid;
  UINT8                                 L05ChargeInBatteryMode;
  BOOLEAN                               L05RestoreDefaultOverclockingValid;
  UINT8                                 L05RestoreDefaultOverclocking;
  BOOLEAN                               L05WakeOnVoiceValid;
  UINT8                                 L05WakeOnVoice;
  BOOLEAN                               L05UltraQuietModeValid;
  UINT8                                 L05UltraQuietMode;

  //
  // Security Page
  //
  BOOLEAN                               L05PowerOnPasswordValid;
  UINT8                                 L05PowerOnPassword;
  BOOLEAN                               L05ClearUserPasswordValid;
  UINT8                                 L05ClearUserPassword;
  BOOLEAN                               L05IntelPttValid;
  UINT8                                 L05IntelPtt;
  BOOLEAN                               L05ClearIntelPttKeyValid;
  UINT8                                 L05ClearIntelPttKey;
  BOOLEAN                               L05AmdPspValid;
  UINT8                                 L05AmdPsp;
  BOOLEAN                               L05ClearAmdPspKeyValid;
  UINT8                                 L05ClearAmdPspKey;
  BOOLEAN                               L05SecurityChipValid;
  UINT8                                 L05SecurityChip;
  BOOLEAN                               L05ClearSecurityChipKeyValid;
  UINT8                                 L05ClearSecurityChipKey;
  BOOLEAN                               L05DeviceGuardValid;
  UINT8                                 L05DeviceGuard;
  //
  // I/O Port Access Submenu
  //
  BOOLEAN                               L05EthernetLanValid;
  UINT8                                 L05EthernetLan;
  BOOLEAN                               L05WirelessWanValid;
  UINT8                                 L05WirelessWan;
  BOOLEAN                               L05BluetoothValid;
  UINT8                                 L05Bluetooth;
  BOOLEAN                               L05UsbPortValid;
  UINT8                                 L05UsbPort;
  BOOLEAN                               L05MemoryCardSlotValid;
  UINT8                                 L05MemoryCardSlot;
  BOOLEAN                               L05SmartCardSlotValid;
  UINT8                                 L05SmartCardSlot;
  BOOLEAN                               L05IntegratedCameraValid;
  UINT8                                 L05IntegratedCamera;
  BOOLEAN                               L05MicrophoneValid;
  UINT8                                 L05Microphone;
  BOOLEAN                               L05FingerprintReaderValid;
  UINT8                                 L05FingerprintReader;
  BOOLEAN                               L05ThunderboltValid;
  UINT8                                 L05Thunderbolt;
  BOOLEAN                               L05NfcDeviceValid;
  UINT8                                 L05NfcDevice;
  //
  // Intel (R) SGX Submenu
  //
  BOOLEAN                               L05IntelSgxValid;
  UINT8                                 L05IntelSgx;
  //
  // Security Page
  //
  BOOLEAN                               L05SecureBootValid;
  UINT8                                 L05SecureBoot;
  BOOLEAN                               L05SecureBootStatusValid;
  UINT8                                 L05SecureBootStatus;
  BOOLEAN                               L05PlatformModeValid;
  UINT8                                 L05PlatformMode;
  BOOLEAN                               L05SecureBootModeValid;
  UINT8                                 L05SecureBootMode;
  BOOLEAN                               L05ResetToSetupModeValid;
  UINT8                                 L05ResetToSetupMode;
  BOOLEAN                               L05RestoreFactoryKeysValid;
  UINT8                                 L05RestoreFactoryKeys;

  //
  // Boot Page
  //
  BOOLEAN                               L05BootModeValid;
  UINT8                                 L05BootMode;
  BOOLEAN                               L05BootPriorityValid;
  UINT8                                 L05BootPriority;
  BOOLEAN                               L05FastBootValid;
  UINT8                                 L05FastBoot;
  BOOLEAN                               L05UsbBootValid;
  UINT8                                 L05UsbBoot;
  BOOLEAN                               L05PxeBootToLanValid;
  UINT8                                 L05PxeBootToLan;
  BOOLEAN                               L05PxeIpv4FirstValid;
  UINT8                                 L05PxeIpv4First;
  BOOLEAN                               L05EfiBootOrderValid;
  UINT16                                *L05EfiBootOrder;
  UINT16                                L05EfiBootOrderCount;
  L05_WMI_BOOT_DESCRIPTION              *L05WmiEfiBootDescription;
  BOOLEAN                               L05LegacyBootOrderValid;
  UINT16                                *L05LegacyBootOrder;
  UINT16                                L05LegacyBootOrderCount;
  L05_WMI_BOOT_DESCRIPTION              *L05WmiLegacyBootDescription;

  //
  // Exit Page
  //
  BOOLEAN                               L05OsOptimizedDefaultValid;
  UINT8                                 L05OsOptimizedDefault;
} L05_WMI_SETUP_ITEM;

#pragma pack ()

#endif
