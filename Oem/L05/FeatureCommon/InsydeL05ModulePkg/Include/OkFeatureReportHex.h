/** @file
  Header for Feature Report HEX function

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OK_FEATURE_REPORT_HEX_H_
#define _OK_FEATURE_REPORT_HEX_H_

#ifdef OK_FEATURE_REPORT_ENABLE
#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>

//
// Consumed Protocols
//
#include <Protocol/OkFeatureReportInterface.h>

/**
  Feature Report Print function

  @param  Format     The point to the Format string
  @param  ...        Variable argument list whose contents are accessed based on the format string specified by Format.
**/
static
VOID
FeatureReportPrintHexStyle2 (
  IN VOID                               *Location,
  IN UINTN                              Length
  )
{
  EFI_STATUS                                       Status;
  EFI_GUID                                         OkFeatureReportGuid = EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL_GUID;
  static EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL  *OkFeatureReport = NULL;

  if (OkFeatureReport == NULL) {

    Status = gBS->LocateProtocol (
                    &OkFeatureReportGuid,
                    NULL,
                    &OkFeatureReport
                    );

    if (EFI_ERROR (Status)) {
      return;
    }
  }

  Status = OkFeatureReport->RecordHexData (Location, Length);

  return;
}
#endif

//
// Library class public defines
//
#ifdef OK_FEATURE_REPORT_ENABLE
  #define FEATURE_REPORT_HEX(Expression)  FeatureReportPrintHexStyle2 Expression
#else
  #define FEATURE_REPORT_HEX(Expression)
#endif

#endif
