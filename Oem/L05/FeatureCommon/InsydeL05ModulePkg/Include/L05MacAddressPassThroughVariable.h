/** @file
  Definition for Mac Address Pass Through Varible

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_MAC_ADDRESS_PASS_THROUGH_VARIABLE_H_
#define _L05_MAC_ADDRESS_PASS_THROUGH_VARIABLE_H_

//
// Variable Name
//
#define L05_MAC_ADDRESS_PASS_THROUGH_VARIABLE_NAME   L"L05MacAddrVar"

#define L05_MAC_ADDRESS_PASS_THROUGH_STRING          L"%02x-%02x-%02x-%02x-%02x-%02x"
#define L05_MAC_ADDRESS_PASS_THROUGH_DUMMY_STRING    L"88-88-88-88-87-88"

#define SELECT_NATIVE_MAC_ADDRESS       0
#define SELECT_INTERNAL_MAC_ADDRESS     1
#define SELECT_SECOND_MAC_ADDRESS       2

#pragma pack(1)

typedef struct {
  BOOLEAN                               InternalMacExisted;
  EFI_MAC_ADDRESS                       InternalMacAddrrss;
  BOOLEAN                               SecondMacExisted;
  EFI_MAC_ADDRESS                       SecondMacAddrrss;
} L05_MAC_ADDRESS_DATA;

#pragma pack()

#endif
