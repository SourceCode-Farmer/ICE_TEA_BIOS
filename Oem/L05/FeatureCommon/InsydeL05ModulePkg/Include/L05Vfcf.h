/** @file
  L05 VFCF Definitions

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_VFCF_H_
#define _L05_VFCF_H_

#ifdef L05_GRAPHIC_UI_ENABLE
//
// For Graphic UI
//
#define L05_HOT_HOTKEY_MODIFY_VALUE_BY_QUESTION_ID(a) question questionid = a; \
        hotkey hotkeyid = 0x704; \
          prompt                      = STRING_TOKEN(STR_HOT_KEY_MODIFY_PREVIOUS_VALUE_PROMPT_STRING), \
          help                        = STRING_TOKEN(STR_HOT_KEY_MODIFY_VALUE_HELP_STRING), \
          group                       = 3, \
          action                      = H2O_HOT_KEY_ACTION_MODIFY_PREVIOUS_VALUE, \
          style = .H2OHotKeyModifyPreviousValue; \
        endhotkey; \
        hotkey hotkeyid = 0x705; \
          prompt                      = STRING_TOKEN(STR_HOT_KEY_MODIFY_NEXT_VALUE_PROMPT_STRING), \
          help                        = STRING_TOKEN(STR_HOT_KEY_MODIFY_VALUE_HELP_STRING), \
          group                       = 3, \
          action                      = H2O_HOT_KEY_ACTION_MODIFY_NEXT_VALUE, \
          style = .H2OHotKeyModifyNextValue; \
        endhotkey; \
      endquestion;
#else
#define L05_HOT_HOTKEY_MODIFY_VALUE_BY_QUESTION_ID(a) question questionid = a; \
        hotkey hotkeyid = 0x707; \
          prompt                      = STRING_TOKEN(STR_HOT_KEY_MODIFY_PREVIOUS_VALUE_PROMPT_STRING), \
          help                        = STRING_TOKEN(STR_HOT_KEY_MODIFY_VALUE_HELP_STRING), \
          group                       = 3, \
          action                      = H2O_HOT_KEY_ACTION_MODIFY_PREVIOUS_VALUE, \
          style = .H2OHotKeyModifyPreviousValue; \
        endhotkey; \
        hotkey hotkeyid = 0x708; \
          prompt                      = STRING_TOKEN(STR_HOT_KEY_MODIFY_NEXT_VALUE_PROMPT_STRING), \
          help                        = STRING_TOKEN(STR_HOT_KEY_MODIFY_VALUE_HELP_STRING), \
          group                       = 3, \
          action                      = H2O_HOT_KEY_ACTION_MODIFY_NEXT_VALUE, \
          style = .H2OHotKeyModifyNextValue; \
        endhotkey; \
      endquestion;
#endif

#endif
