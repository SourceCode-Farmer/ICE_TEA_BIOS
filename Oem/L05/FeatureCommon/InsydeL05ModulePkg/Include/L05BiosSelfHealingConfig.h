/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_BIOS_SELF_HEALING_CONFIG_H_
#define _L05_BIOS_SELF_HEALING_CONFIG_H_

#include <Uefi.h>

//
// SMI Interface Definition
//
#define L05_BIOS_SELF_HEALING_FUNCTION                    (UINT32)(0x534EDC00 + (UINT8)L05_SECURITY_SW_SMI)
#define L05_BIOS_SELF_HEALING_CHECK_CAPABILITY            (UINT32)0x0C
#define L05_BIOS_SELF_HEALING_DESTROY_BOOT_BLOCK          (UINT32)0x0B
#define L05_BIOS_SELF_HEALING_DESTROY_FVMAIN              (UINT32)0x0F

//
// SMI Interface Return Code
//
#define L05_BIOS_SELF_HEALING_FUNCTION_ENABLED            (UINT32)(1 << 30)
#define L05_BIOS_SELF_HEALING_FUNCTION_DISABLED           (UINT32)(0 << 30)
#define L05_BIOS_SELF_HEALING_FUNCTION_SUPPORTED          (UINT32)(1 << 31)
#define L05_BIOS_SELF_HEALING_FUNCTION_NOT_SUPPORTED      (UINT32)(0 << 31)
#define L05_BIOS_SELF_HEALING_FUNCTION_FAILED             (UINT32)(1 << 31)
#define L05_BIOS_SELF_HEALING_FUNCTION_SUCCESSFUL         (UINT32)(0 << 31)

//
// EEPROM Flag Definition
//
#define L05_BIOS_SELF_HEALING_FLAG_BACKUP_BLOCK_SYNCED    (UINT8)BIT0
#define L05_BIOS_SELF_HEALING_FLAG_DESTROY_BOOT_BLOCK     (UINT8)BIT1
#define L05_BIOS_SELF_HEALING_FLAG_DESTROY_FVMAIN         (UINT8)BIT2

#pragma pack(1)

#define EFI_L05_BSH_DATA_HEADER_SIGNATURE                 SIGNATURE_32 ('$', 'B', 'S', 'H')

typedef struct _EFI_L05_BSH_DATA_HEADER {
  UINT32                                Signature;
  UINT8                                 PatchCount;
  UINT8                                 Reserved[11];
} EFI_L05_BSH_DATA_HEADER;

typedef struct _EFI_L05_PATCH_DATA_HEADER {
  UINT32                                FvBase;
  UINT32                                Offset;
  UINT32                                Size;
  UINT8                                 Reserved[4];
} EFI_L05_PATCH_DATA_HEADER;

#pragma pack()

#endif
