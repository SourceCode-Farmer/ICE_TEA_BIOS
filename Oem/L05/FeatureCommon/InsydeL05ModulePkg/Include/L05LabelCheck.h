/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_LABEL_CHECK_H_
#define _L05_LABEL_CHECK_H_
//================================================================================
//                         CMOS Index for L05 feature Start
//================================================================================

//================================================================================
//                         CMOS Index for L05 feature End
//================================================================================


//================================================================================
//                         Acpi Table Id Start
//================================================================================
//================================================================================
//                         Acpi Table Id End
//================================================================================


//================================================================================
//                         SMBIOS Start
//================================================================================
//================================================================================
//                         SMBIOS End
//================================================================================


//================================================================================
//                         Setup Menu Start
//================================================================================
//================================================================================
//                         Setup Menu End
//================================================================================


//================================================================================
//                         L05 Logo Start
//================================================================================
//================================================================================
//                         L05 Logo End
//================================================================================


//================================================================================
//                         L05 System Passwords Reset Start
//================================================================================
#ifdef ENABLE_L05_SYSTEM_PASSWORDS_RESET

  #ifdef FLASH_REGION_L05_SYSTEM_PASSWORDS_REGION_BASE

    #if ((FLASH_REGION_L05_SYSTEM_PASSWORDS_REGION_BASE % 0x1000) != 0)
      #error [L05 Feature code error notify]: FLASH_REGION_L05_SYSTEM_PASSWORDS_REGION_BASE is not align on 4K bytes boundary, please check your flash map.
    #endif

    #if (FLASH_REGION_L05_SYSTEM_PASSWORDS_REGION_SIZE != 0x1000)
      #error [L05 Feature code error notify]: FLASH_REGION_L05_SYSTEM_PASSWORDS_REGION_SIZE is not equal to 4K bytes. Please check your flash map.
    #endif

  #endif // #ifdef FLASH_REGION_L05_SYSTEM_PASSWORDS_REGION_SIZE

#endif // #ifdef ENABLE_L05_SYSTEM_PASSWORDS_RESET
//================================================================================
//                         L05 System Passwords Reset End
//================================================================================


//================================================================================
//                         Media Logo Start
//================================================================================
//================================================================================
//                         Media Logo End
//================================================================================


//================================================================================
//                         LEOS Start
//================================================================================
//================================================================================
//                         LEOS End
//================================================================================


//================================================================================
//                         SLP1.0 Start
//================================================================================
//================================================================================
//                         SLP1.0 End
//================================================================================


//================================================================================
//                         L05 SLP2.0 Start
//================================================================================
//_Start_L05_ACPI_SLP_20_
  #if ((FixedPcdGet32 (PcdL05FlashFvSlp20Base) % 0x1000) != 0)
    #error [L05 Feature code error notify]: gL05ServicesTokenSpaceGuid.PcdL05FlashFvSlp20Base is not align on 4K bytes boundary, please check your flash map.
  #endif

  #if (FixedPcdGet32 (PcdL05FlashFvSlp20Size) != 0x1000)
    #error [L05 Feature code error notify]: gL05ServicesTokenSpaceGuid.PcdL05FlashFvSlp20Size is not equal to 4K bytes. Please check your flash map.
  #endif
//_End_L05_ACPI_SLP_20_
//================================================================================
//                         L05 SLP2.0 End
//================================================================================


//================================================================================
//                         WIRELESS LOCK Start
//================================================================================
//================================================================================
//                         WIRELESS LOCK End
//================================================================================

#endif
