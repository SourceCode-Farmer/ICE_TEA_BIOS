/** @file
  Definition for Lenovo UEFI Drive Identification

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_LENOVO_BOOT_OPTION_H_
#define _L05_LENOVO_BOOT_OPTION_H_

//
// Variable Name
//

//
// Variable Guid
//
#define L05_LENOVO_BOOT_OPTION_GUID\
  {0x146b234d, 0x4052, 0x4e07, 0xb3, 0x26, 0x11, 0x22, 0x0f, 0x8e, 0x1f, 0xe8}

extern EFI_GUID  gEfiL05LenovoBootOptionGuid;

#endif
