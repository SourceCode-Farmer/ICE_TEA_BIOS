/** @file
  Definition for Backup Setup Item Variable for Flash BIOS

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_BACKUP_SETUP_ITEM_VARIABLE_H_
#define _L05_BACKUP_SETUP_ITEM_VARIABLE_H_

//
// Variable Name
//
#define L05_BACKUP_SETUP_ITEM_VARIABLE_NAME  L"L05BackupSetupItem"


#ifdef L05_BIOS_OPTANE_SUPPORT_ENABLE
#pragma pack(1)
typedef struct {
  UINT8                                 SataInterfaceMode;
  UINT8                                 RstPcieRemapEnabled[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8                                 SataRstOptaneMemory;
} L05_BACKUP_SETUP_ITEM;
#pragma pack()
#endif

#define EFI_L05_BACKUP_SETUP_ITEM_VARIABLE_GUID \
  {0xdeef453b, 0x1ac9, 0x44b6, {0xbc, 0x8a, 0x96, 0x5a, 0x8b, 0x68, 0x86, 0x1d}}

extern EFI_GUID gEfiL05BackupSetupItemVariableGuid;

#endif
//[-start-210918-Dongxu0019-add]//
#ifdef LCFC_SUPPORT
//
//The name of Backup Variable 
//
#define BIOS_BACKUP_SETUP_KEEP_VARIABLE_NAME  L"BIOSBackupSetupKeep"

#pragma pack(1)
typedef struct {
//Configuration
// Wireless LAN
  UINT8                                 L05WirelessLan;
//INTEL VMD Controller
  UINT8                                 VmdEnable;
//Graphics Device
  UINT8                                 PrimaryDisplay;
//Intel VT
  UINT8                                 VT;
//Intel VT-d Feature
  UINT8                                 EnableVtd;
//Intel Hyper- Threading Technology
  UINT8                                 HyperThreading;
//BIOS Back Flash
  UINT8                                 L05BiosBackFlash;
//HotKey Mode
  UINT8                                 L05HotKeyMode;
//Fool Proof Fn Ctrl
  UINT8                                 L05FoolProofFnCtrl;
//Always On USB
  UINT8                                 L05AlwaysOnUsb;
//Charge In Battery Mode
  UINT8                                 L05ChargeInBatteryMode;
//Disable Building-in Battery
  UINT8                                 L05DisableBuildingInBatterySupport;
//System Performance Mode
  UINT8                                 L05SystemPerformanceMode;
//Thermal Mode
  UINT8                                 L05ThermalMode;
//Flip To Boot
  UINT8                                 L05FlipToBoot;
//One Key Battery
  UINT8                                 L05OneKeyBattery;
//[-start-211014-BAIN000051-add]//
//#ifdef LCFC_SUPPORT
//#if defined(C970_BSH_SUPPORT)
//Bios Self Healing
//  UINT8                                 L05BiosSelfHealing;
//#endif
//[-end-211014-BAIN000051-add]//
//Security
  UINT8                                 L05DeviceGuard;
  UINT8                                 L05SecureBoot;
  UINT8                                 L05PlatformMode;
  UINT8                                 L05SecureBootMode;
  
//Boot
  UINT8                                 L05FastBoot;
  UINT8                                 UsbBoot;
  UINT8                                 PxeBootToLan;
  UINT8                                 L05PxeIpv4First;
//[-start-211018-BAIN000058-modify]//
//[-start-211018-Dongxu0024-add]//
//[-start-211207-SHAONN0023-modify]//
//#if defined(C970_BSH_SUPPORT)
#ifdef LCFC_SUPPORT
//Bios Self Healing
  UINT8                                 L05BiosSelfHealing;
  UINT8                                 L05NaturalFileGuard;
  UINT8                                 L05FtpmEnable;
  UINT8                                 L05PowerOnPassword;
//[-start-211124-kebin000069-modify]//
//[-start-220113-YUNLEI0158-modify]//
//[-start-220117-SHAONN0027-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) 
  UINT8                                 L05UltraQuietMode;
#else
  UINT8                                 OEMRSV2;
#endif
//[-end-220117-SHAONN0027-modify]//
//[-end-220113-YUNLEI0158-modify]//
//[-end-211124-kebin000069-modify]//
//  UINT8                                 OEMRSV3;
  UINT8                                 OEMRSV4;
  UINT8                                 OEMRSV5;
  UINT8                                 OEMRSV6;
  UINT8                                 OEMRSV7;
#endif
//[-end-211207-SHAONN0023-modify]//
//[-end-211018-Dongxu0024-add]//
//[-end-211117-BAIN000058-modify]//
} BIOS_BACKUP_SETUP_KEEP;
#pragma pack()

#define EFI_BIOS_BACKUP_SETUP_KEEP_VARIABLE_GUID \
  {0x2AA8F59B, 0x1403, 0x22F5, {0x94, 0xAC, 0x7D, 0x11, 0x12, 0x1F, 0xD0, 0x20}}

extern EFI_GUID gEfiBIOSBackupSetupKeepVariableGuid;
#endif
//[-end-210918-Dongxu0019-add]//
