/** @file
  Definition for switch Flip to Boot.

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_SWITCH_FLIP_TO_BOOT_H_
#define _L05_SWITCH_FLIP_TO_BOOT_H_

#define L05_EFI_FLIP_TO_BOOT_VARIABLE_NAME  L"FBSWIF"

#define L05_EFI_FLIP_TO_BOOT_VARIABLE_GUID  \
  {0xd743491e, 0xf484, 0x4952, 0xa8, 0x7d, 0x8d, 0x5d, 0xd1, 0x89, 0xb7, 0x0c}

#pragma pack(1)
typedef struct LENOVO_FLIP_TO_BOOT_SW_INTERFACE {
  UINT8                                 FlipToBootEn;
  UINT8                                 Reserved[3];
} LENOVO_FLIP_TO_BOOT_SW_INTERFACE;
#pragma pack()

extern EFI_GUID gEfiL05FliptoBootVariableGuid;

#endif
