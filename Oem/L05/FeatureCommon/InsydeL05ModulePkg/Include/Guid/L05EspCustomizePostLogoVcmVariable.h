/** @file
  Definition for Customize POST LOGO VCM

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef L05_ESP_CUSTOMIZE_POST_LOGO_VCM_VARIABLE_
#define L05_ESP_CUSTOMIZE_POST_LOGO_VCM_VARIABLE_

//
// Variable Name
//
#define L05_BIOS_LOGO_DIY_VERSION_CRC_CONTROL_VARIABLE_NAME  L"LBLDVC"
#define L05_LOGO_DIY_VERSION                                 0X20000
#define L05_CUSTOMIZE_POST_LOGO_CRC32_CALCULATE_NUM          512

#pragma pack(1)
typedef struct {
  UINT32                                Version;
  UINT32                                CRC32;
  UINT8                                 Reserved[32];
} LENOVO_ESP_CUSTOMIZE_POST_LOGO_VCM;
#pragma pack()

#define EFI_L05_ESP_CUSTOMIZE_POST_LOGO_VCM_VARIABLE_GUID  \
  { \
    0x871455D1, 0x5576, 0x4FB8, 0x98, 0x65, 0xAF, 0x08, 0x24, 0x46, 0x3C, 0x9F \
  }

extern EFI_GUID gEfiL05EspCustomizePostLogoVcmVariableGuid;

#endif
