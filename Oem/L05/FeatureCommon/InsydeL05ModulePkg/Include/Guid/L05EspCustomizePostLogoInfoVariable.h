/** @file
  Definition for ESP Customize POST LOGO Info

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef L05_ESP_CUSTOMIZE_POST_LOGO_INFO_VARIABLE_
#define L05_ESP_CUSTOMIZE_POST_LOGO_INFO_VARIABLE_

#include <CommonReg.h>
#include <Protocol/OEMBadgingSupport.h>

//
// Variable Name
//
#define L05_BIOS_LOGO_DIY_ESP_VARIABLE_NAME            L"LBLDESP"
#define L05_BIOS_LOGO_DIY_ESP_FILE_NAME_VARIABLE_NAME  L"LBLDESPFN"

//
// Logo Support Bit
//
#define L05_CUSTOMIZE_POST_LOGO_SUPPORT_FORMAT_JPG     BIT0
#define L05_CUSTOMIZE_POST_LOGO_SUPPORT_FORMAT_TGA     BIT1
#define L05_CUSTOMIZE_POST_LOGO_SUPPORT_FORMAT_PCX     BIT2
#define L05_CUSTOMIZE_POST_LOGO_SUPPORT_FORMAT_GIF     BIT3
#define L05_CUSTOMIZE_POST_LOGO_SUPPORT_FORMAT_BMP     BIT4
#define L05_CUSTOMIZE_POST_LOGO_SUPPORT_FORMAT_PNG     BIT5

//
// Customize POST LOGO define
//
#define L05_BIOS_LOGO_DIY_ESP_FOLDER_PATH              L"EFI\\lenovo\\logo\\"
#define L05_BIOS_LOGO_DIY_ESP_NAME_SIG                 L"mylogo_"

#pragma pack(1)
typedef union {
  struct
  {
    UINT8                               JPG      :1;
    UINT8                               TGA      :1;
    UINT8                               PCX      :1;
    UINT8                               GIF      :1;
    UINT8                               BMP      :1;
    UINT8                               PNG      :1;
    UINT8                               Reserved :2;
  } Bits;
  UINT8                                 Data8;
} LOGO_SUPPORT_FORMAT;

typedef struct {
  BOOLEAN                               LogoDiySupport;
  UINT32                                HorizontalResolution;
  UINT32                                VerticalResolution;
  LOGO_SUPPORT_FORMAT                   LogoSupportFormat;
} LENOVO_ESP_CUSTOMIZE_POST_LOGO_INFO;

typedef struct {
  EFI_BADGING_SUPPORT_FORMAT            Format;
  CHAR16                                FileName[1];
} L05_BIOS_LOGO_DIY_ESP_FILE_NAME_DATA;
#pragma pack()

#define EFI_L05_ESP_CUSTOMIZE_POST_LOGO_INFO_VARIABLE_GUID  \
  { \
    0x871455D0, 0x5576, 0x4FB8, 0x98, 0x65, 0xAF, 0x08, 0x24, 0x46, 0x3B, 0x9E \
  }

extern EFI_GUID gEfiL05EspCustomizePostLogoInfoVariableGuid;

#endif
