/** @file
  Definitions for L05 H2O Setup

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_H2O_SETUP_H_
#define _L05_H2O_SETUP_H_

#define L05_HDD_MODEL_NAME_LABEL                0xE000
#define L05_HDD_MODEL_NAME_END_LABEL            0xE001
#define L05_LINK_INTEL_RST_FORMSET_LABEL        0xE002
#define L05_SETUP_UTILITY_LANG_LABEL            0xE003
#define L05_KEY_DUMMY_OEM                       0xE050
#define L05_KEY_SET_MASTER_USER_HDD_SECURITY    0xE051
#define L05_KEY_CHANGE_ALL_MASTER_HDD_SECURITY  0xE052
#define L05_KEY_CHANGE_ALL_USER_HDD_SECURITY    0xE053
#define L05_KEY_SECURE_BOOT                     0xE060
#define L05_KEY_RESET_TO_SETUP_MODE             0xE061
#define L05_KEY_RESTORE_FACTORY_KEYS            0xE062
#define L05_KEY_STORAGE_CONTROLLER_MODE         0xE063
#define L05_KEY_LINK_INTEL_RST_FORMSET_BASE     0xE064
#define L05_KEY_FAST_BOOT                       0xE065
#define L05_KEY_PXE_BOOT_TO_LAN                 0xE067
#define L05_KEY_PXE_IPV4_FIRST                  0xE068
#define L05_KEY_DISABLE_BUILDING_IN_BATTERY     0xE069
#define L05_KEY_FTPM_ENABLE                     0xE06A
#define L05_KEY_SPECIAL_VGA_FEATURE             0xE06B
#define L05_KEY_SECURE_SUITE_APP                0xE06C
#define L05_KEY_BIOS_SELF_HEALING               0xE06D
#define L05_KEY_DEVICE_GUARD                    0xE06E
#define L05_KEY_NATURAL_FILE_GUARD              0xE06F
#define L05_KEY_THERMAL_MODE                    0xE070
#define L05_KEY_SYSTEM_BIOS_VERSION             0xE071
#define L05_KEY_SYSTEMSERIAL_NUMBER             0xE072
#define L05_KEY_CPU_INFO                        0xE073
#define L05_KEY_MAC_ADDRESS_PASS_THROUGH        0xE074
#define L05_KEY_LENOVO_CLOUD_SERVICE            0xE075
#define L05_KEY_LENOVO_CLOUD_SERVICE_WIFI       0xE076
#define L05_LINK_WIFI_CONFIG_FORMSET_LABEL      0xE077
#define L05_LINK_WIFI_CONFIG_FORMSET_LABEL_END  0xE078
#define L05_KEY_LINK_WIFI_CONFIG_FORMSET_BASE   0xE079

//[-start-210701-FLINT00010-add]//
//[-start-210723-QINGLIN0001-modify]//
//#if defined(C970_SUPPORT) || defined(C770_SUPPORT)
#ifdef LCFC_SUPPORT
//[-end-210723-QINGLIN0001-modify]//
#define L05_FLTP_TO_BOOT                        0xE07A
#define L05_KEY_ALWAYS_USB                      0xE07B
#endif
//[-end-210701-FLINT00010-add]//

#define L05_KEY_OVERCLOCK_BACKUP_SETUP_DATA     0xE0A0
#define L05_KEY_OVERCLOCK_RESTORE_SETUP_DATA    0xE0A1
#define L05_KEY_OVERCLOCK_SET_TO_DEFAULT        0xE0A2
#define L05_KEY_OVERCLOCK_CPU_UNLOCK            0xE0A3
#define L05_KEY_OVERCLOCK_GPU_UNLOCK            0xE0A4
#define L05_KEY_CPU_OVERCLOCKING                0xE0B0
#define L05_KEY_CPU_ADVANCED_OVERCLOCKING       0xE0B1
#define L05_KEY_CPU_VOLTAGE_OFFSET_PREFIX       0xE0B2
#define L05_KEY_CPU_VOLTAGE_OFFSET              0xE0B3
#define L05_KEY_AVX_RATIO_OFFSET                0xE0B4
#define L05_KEY_CACHE_RATIO                     0xE0B5
#define L05_KEY_CACHE_VOLTAG_OFFSET_PREFIX      0xE0B6
#define L05_KEY_CACHE_VOLTAG_OFFSET             0xE0B7
#define L05_KEY_PRECISION_BOOST_OVERDRIVE       0xE0B8
#define L05_KEY_PRECISION_BOOST_OVERDRIVE_SCALAR             0xE0B9
#define L05_KEY_CUSTOMIZED_PRECISION_BOOST_OVERDRIVE_SCALAR  0xE0BA
#define L05_KEY_MAX_CPU_BOOST_CLOCK_OVERRIDE    0xE0BB
#define L05_KEY_ALL_CORE_CURVE_OPTIMIZER_MAGNITUDE_PREFIX    0xE0BC
#define L05_KEY_ALL_CORE_CURVE_OPTIMIZER_MAGNITUDE           0xE0BD
#define L05_KEY_GPU_OVERCLOCKING                0xE0C0
#define L05_KEY_GPU_ADVANCED_OVERCLOCKING       0xE0C1
#define L05_KEY_GPU_CORE_CLOCK_OFFSET_PREFIX    0xE0C2
#define L05_KEY_GPU_CORE_CLOCK_OFFSET           0xE0C3
#define L05_KEY_GPU_VRAM_CLOCK_OFFSET_PREFIX    0xE0C4
#define L05_KEY_GPU_VRAM_CLOCK_OFFSET           0xE0C5
#define L05_KEY_MAX_GPU_BOOST_CLOCK_OVERRIDE    0xE0C6
#define L05_KEY_MEMORY_OVERCLOCKING             0xE0D0
#define L05_KEY_MEMORY_PROFILE                  0xE0D1
#define L05_KEY_MEMORY_OVERCLOCKING_LP          0xE0D2

//
// VFR Form ID
//
#define L05_STORAGE_FORM_ID                      0xE101
#define L05_IO_PORT_ACCESS_FORM_ID               0xE102
#define L05_NETWORK_BOOT_SETTINGS_FORM_ID        0xE103
#define L05_NETWORK_LENOVO_CLOUD_FORM_ID         0xE104
#define L05_GAMING_OVERCLOCKING_FORM_ID          0xE110
#define L05_GAMING_OVERCLOCKING_INTERNAL_FORM_ID 0xE111

//
// VFR Varstore ID
//
#define L05_HDD_PASSWORD_CONFIGURATION_VARSTORE_ID   0xE201

//
// HDD password relative key
//
#define L05_MAX_HDD_PASSWORD_NUMBER                  0x10
#define L05_LABEL_HDD_PASSWORD_STATUS                0xEA00
#define L05_LABEL_HDD_PASSWORD_STATUS_END            0xEA01
#define L05_LABEL_HDD_PASSWORD_OPTION                0xEA02
#define L05_LABEL_HDD_PASSWORD_OPTION_END            0xEA03
#define L05_KEY_SET_USER_PASS_FLAG                   0xEA04
#define L05_LABEL_SECURITY_ERASE_HDD_DATA            0xEA05
#define L05_LABEL_SECURITY_ERASE_HDD_DATA_END        0xEA06
#define L05_KEY_SET_HDD_PASSWORD_FLAG                0xEA10
#define L05_KEY_SET_HDD_PASSWORD_FLAG_END            0xEA1F
#define L05_KEY_SET_HDD_PASSWORD_OPTION              0xEA20
#define L05_KEY_SET_HDD_PASSWORD_OPTION_END          0xEA2F
#define L05_KEY_CHANGE_MASTER_PASSWORD_OPTION        0xEA30
#define L05_KEY_CHANGE_MASTER_PASSWORD_OPTION_END    0xEA3F
#define L05_KEY_CHANGE_USER_PASSWORD_OPTION          0xEA40
#define L05_KEY_CHANGE_USER_PASSWORD_OPTION_END      0xEA4F
#define L05_KEY_SECURITY_ERASE_FLAG                  0xEA50
#define L05_KEY_SECURITY_ERASE_FLAG_END              0xEA5F
#define L05_KEY_SECURITY_ERASE_HDD_DATA              0xEA60
#define L05_KEY_SECURITY_ERASE_HDD_DATA_END          0xEA6F

//
// VFR Form ID GUID
//
#define FORMSET_ID_GUID_GAMING_HOME     {0x4c622579, 0xb559, 0x4602, 0x93, 0xe0, 0x44, 0x73, 0x79, 0x3e, 0xa2, 0x00}
#define FORMSET_ID_GUID_INFORMATION     {0x1d09183d, 0x66a4, 0x489d, 0x9f, 0xca, 0xca, 0x8e, 0x6f, 0xef, 0xf9, 0x71}
#define FORMSET_ID_GUID_CONFIGURATION   {0xf500784d, 0x75b5, 0x41fa, 0xb7, 0xd5, 0xd4, 0x13, 0x7d, 0xae, 0xed, 0xb8}
#define FORMSET_ID_GUID_INTEL_RST       {0xd37bcd57, 0xaba1, 0x44e6, 0xa9, 0x2c, 0x89, 0x8b, 0x15, 0x8f, 0x2f, 0x59}
#define L05_NOVO_MENU_FORMSET_GUID      {0xA6A8C55C, 0x455C, 0x212E, 0x08, 0xD3, 0x05, 0xA0, 0x5E, 0x81, 0x12, 0x52}
#define L05_WIFI_CONFIG_FORMSET_GUID    {0x3441803e, 0x5a88, 0x4941, 0x82, 0xf0, 0x85, 0x8a, 0x10, 0x85, 0x27, 0x6c}
#endif
