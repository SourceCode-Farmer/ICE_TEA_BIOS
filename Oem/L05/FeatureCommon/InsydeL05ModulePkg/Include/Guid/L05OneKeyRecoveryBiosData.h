/** @file
  Definition for BIOS Password Variable

;******************************************************************************
;* Copyright (c) 2012 - 2014 Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_ONE_KEY_RECOVERY_BIOS_DATA_GUID_H_
#define _L05_ONE_KEY_RECOVERY_BIOS_DATA_GUID_H_

//
// Variable Guid
//
#define EFI_ONE_KEY_RECOVERY_BIOS_DATA_GUID \
  { \
    0x64448f46, 0x6e23, 0x4288, 0x93, 0x49, 0xfd, 0xd8, 0x87, 0xc4, 0x0d, 0xe1 \
  }

#define EFI_ONE_KEY_RECOVERY_FROM_NOVO_BUTTON_GUID \
  { \
    0x4f1572c7, 0x8086, 0x42b0, 0xb4, 0xb7, 0x20, 0x76, 0x4b, 0xde, 0x29, 0x29 \
  }

extern EFI_GUID  gEfiL05OneKeyRecoveryBiosDataGuid;
extern EFI_GUID  gEfiL05OneKeyRecoveryFromNovoButtonGuid;
#endif
