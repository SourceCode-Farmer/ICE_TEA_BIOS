/** @file
  Definition for Customize Logo Support

;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_CUSTOMIZE_MULTI_LOGO_H_
#define _L05_CUSTOMIZE_MULTI_LOGO_H_

#define L05_CUSTOM_MULTI_LOGO_SIGNATURE      SIGNATURE_64 ('$', 'L', '0', '5', 'C', 'M', 'L', 'G')
#define L05_CUSTOM_MULTI_LOGO_VARIABLE_NAME  L"L05CustomizeMultiLogo"

#pragma pack(1)
typedef struct {
  UINT32                                ResolutionX;
  UINT32                                ResolutionY;
} L05_LOGO_RESOLUTION_INFO;

typedef struct {
  EFI_GUID                              Guid;
  UINT64                                Signature;
  UINT32                                SupportedResolutionX;
  UINT32                                SupportedResolutionY;
  UINT8                                 ImageFormat;
  UINT32                                ImageSize;
  UINT8                                 ImageData[1];
} L05_CUSTOM_MULTI_LOGO_FLASH_REGION_HEADER;
#pragma pack()

extern EFI_GUID  gL05CustomizeMultiLogoGuid;

#endif
