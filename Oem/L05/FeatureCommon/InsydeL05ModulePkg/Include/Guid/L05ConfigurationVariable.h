/** @file
  Definition for Configuration Variable

;******************************************************************************
;* Copyright (c) 2012 - 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_CONFIGURATION_VARIABLE_H_
#define _L05_CONFIGURATION_VARIABLE_H_

//
// Variable Name
//
#define L05_CONFIGURATION_VARIABLE_NAME  L"L05ConfigVar"
#define L05_CONFIGURATION_VARIABLE_SIZE  0x08


#define EFI_L05CONFIGURATION_VARIABLE_GUID \
  {0x74D69ABB, 0x57C3, 0x4d7f, {0xBF, 0xB4, 0x26, 0xA2, 0x54, 0x96, 0x10, 0xF1}}

extern EFI_GUID  gEfiL05ConfigurationVariableGuid;

#endif
