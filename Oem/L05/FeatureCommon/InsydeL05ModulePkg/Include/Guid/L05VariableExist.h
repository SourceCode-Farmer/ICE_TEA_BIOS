/** @file
  Definition for Variable Exist

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef L05_VARIABLE_EXIST_
#define L05_VARIABLE_EXIST_

//
// Variable Name
//
#define L05_VARIABLE_EXIST_NAME         L"LVE"

#define EFI_L05_VARIABLE_EXIST_GUID  \
  { \
    0X5FDA220C, 0X9DDC, 0X4588, 0X9D, 0XA7, 0X34, 0X94, 0X6E, 0X00, 0XBF, 0XAD \
  }

extern EFI_GUID gEfiL05VariableExistGuid;

#endif
