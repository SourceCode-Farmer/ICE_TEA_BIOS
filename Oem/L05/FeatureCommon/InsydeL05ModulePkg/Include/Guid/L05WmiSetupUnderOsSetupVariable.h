/** @file
  Definition for WMI Setup Under Os Setup Variable

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_WMI_SETUP_UNDER_OS_SETUP_VARIABLE_GUID_H_
#define _L05_WMI_SETUP_UNDER_OS_SETUP_VARIABLE_GUID_H_

//
// Variable Name
//
#define L05_WMI_SECURE_BOOT_DATA_VARIABLE_NAME        L"L05WSBD"
#define L05_WMI_HDD_PASWORD_DATA_VARIABLE_NAME        L"L05WHPD"

//
// Variable Guid
//
#define L05_WMI_SETUP_UNDER_OS_SETUP_VARIABLE_GUID  \
  {0XA93BB445, 0X9E02, 0X4A13, 0XA7, 0XA9, 0X1C, 0X35, 0XA8, 0X05, 0X25, 0XBF}

extern EFI_GUID  gL05WmiSetupUnderOsSetupVariableGuid;

#endif
