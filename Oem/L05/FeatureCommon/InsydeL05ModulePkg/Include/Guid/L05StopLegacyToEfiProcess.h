/** @file
  Guid for Stop LegacyToEfi Process service

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_STOP_LEGACY_TO_EFI_PROCESS_H_
#define _L05_STOP_LEGACY_TO_EFI_PROCESS_H_

#define EFI_L05_STOP_LEGACY_TO_EFI_PROCESS_GUID \
  {0x392cf1de, 0x75fd, 0x11e4, {0x99, 0x76, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}

extern EFI_GUID  gL05StopLegacyToEfiProcessGuid;

#endif
