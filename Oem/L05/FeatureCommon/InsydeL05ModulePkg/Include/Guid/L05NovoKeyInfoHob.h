/** @file
  SPEC : Dual Mode System Specification BIOS Portion

  Definition for NOVO Button

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_NOVO_KEY_INFO_HOB_GUID_H_
#define _L05_NOVO_KEY_INFO_HOB_GUID_H_

#define L05_NOVO_KEY_INFO_HOB_GUID  \
  {0xAE7D8822, 0x9512, 0x4500, 0xAA, 0x4B, 0x9D, 0x04, 0x2E, 0x6A, 0x76, 0x5B}

extern EFI_GUID  gL05NovoKeyInfoHobGuid;

#endif
