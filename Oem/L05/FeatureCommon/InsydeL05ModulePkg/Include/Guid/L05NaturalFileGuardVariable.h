/** @file
  Definition for Natural File Guard

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef L05_NATURAL_FILE_GUARD_VARIABLE_
#define L05_NATURAL_FILE_GUARD_VARIABLE_

#define L05_HDD_PASSWORD_IV_SIZE        16
#define L05_HDD_PASSWORD_ENCRYPT_SIZE   64
#define L05_HDD_PASSWORD_HDD_ID_SIZE    16

//
// Variable Name
//
#define L05_NATURAL_FILE_GUARD_VARIABLE_NAME  L"LNFG"

#pragma pack(1)
typedef struct {
  UINT8                                 Iv[L05_HDD_PASSWORD_IV_SIZE];
  UINT8                                 Ciphertext[L05_HDD_PASSWORD_ENCRYPT_SIZE];
  UINT8                                 HddId[L05_HDD_PASSWORD_HDD_ID_SIZE];
} LENOVO_UHDP_STORE;
#pragma pack()

#define EFI_L05_NATURAL_FILE_GUARD_VARIABLE_GUID  \
  { \
    0xE2C5A81A, 0x4F05, 0x477C, 0xBA, 0x4E, 0x49, 0xBD, 0xD5, 0x75, 0xE9, 0xAE \
  }

extern EFI_GUID gEfiL05NaturalFileGuardVariableGuid;

#endif
