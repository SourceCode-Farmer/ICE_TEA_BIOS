/** @file
  Variable GUID and Name definition for Wireless Device Support

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_H_
#define _L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_H_

#define EFI_L05WIRELESS_DEVICE_SUPPORT_VARIABLE_GUID \
  {0x74b00bd9, 0x805a, 0x4d61, {0xb5, 0x1f, 0x43, 0x26, 0x81, 0x23, 0xd1, 0x13}}

//
// GUID specific defines
//
#define L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_NAME_BROADCOM  L"BRCM-pwrovr"
#define L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_NAME_INTEL     L"Intel-pwrovr"
#define L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_NAME_QUALCOMM  L"QCA-pwrovr"
#define L05_WIRELESS_DEVICE_SUPPORT_VARIABLE_NAME_REALTEK   L"RTL-pwrovr"

extern EFI_GUID  gL05WirelessDeviceSupportVariableGuid;

#endif
