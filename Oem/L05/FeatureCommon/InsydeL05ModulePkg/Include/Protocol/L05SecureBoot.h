//;******************************************************************************
//;* Copyright (c) 1983-2012, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#ifndef _EFI_L05_SECURE_BOOT_H_
#define _EFI_L05_SECURE_BOOT_H_

#define EFI_L05_SECURE_BOOT_PROTOCOL_GUID \
  { \
    0xC706D63F, 0x6CCE, 0x48AD, 0xA2, 0xB4, 0x72, 0xA5, 0xEF, 0x9E, 0x22, 0x0C \
  }

#pragma pack(1)

typedef enum {
  L05_SECURE_BOOT_NO_ACTION,
  L05_SECURE_BOOT_ENABLE,
  L05_SECURE_BOOT_DISABLE,
  L05_SECURE_BOOT_MAX_ACTION
} EFI_L05_SECURE_BOOT_ACTION;

typedef struct {
  UINT8     Action;
  UINT8     ResetToSystemMode;
  UINT8     RestoreFactoryKeys;
} EFI_L05_SECURE_BOOT_DATA;

typedef
EFI_STATUS
(EFIAPI *EFI_L05_SECURE_BOOT_CALLBACK) (
  IN      EFI_L05_SECURE_BOOT_DATA      *SecureBootData
  );

typedef struct _EFI_L05_SECURE_BOOT_PROTOCOL {
  EFI_L05_SECURE_BOOT_CALLBACK          L05SecureBootCallback;
} EFI_L05_SECURE_BOOT_PROTOCOL;

#pragma pack()

extern EFI_GUID  gEfiL05SecureBootProtocolGuid;

#endif
