/** @file
  SPEC    : Lenovo China Minimum BIOS Spec 128.doc
  Chapter : 3.4.4.2 Hotkey support

  Definition for Hot Key service

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/


#ifndef _EFI_L05_HOT_KEY_SERVICE_PROTOCOL_H_
#define _EFI_L05_HOT_KEY_SERVICE_PROTOCOL_H_

#define EFI_L05_HOT_KEY_SERVICE_PROTOCOL_GUID \
   { 0xF3471608, 0xD1A3, 0x4A23, 0x90, 0x94, 0x36, 0xCE, 0x6D, 0x31, 0x56, 0xB1 }

#pragma pack (1)
typedef
EFI_STATUS
(EFIAPI *EFI_L05_HOT_KEY_SERVICE_GET_HOT_KEY) (
  IN OUT  UINT16                        *HotKey
  );

typedef
EFI_STATUS
(EFIAPI *EFI_L05_HOT_KEY_SERVICE_STOP_HOT_KEY) (
  VOID
  );

typedef struct _EFI_L05_HOT_KEY_SERVICE_PROTOCOL {
  EFI_L05_HOT_KEY_SERVICE_GET_HOT_KEY   GetHotKey;
  EFI_L05_HOT_KEY_SERVICE_STOP_HOT_KEY  StopHotKey;
} EFI_L05_HOT_KEY_SERVICE_PROTOCOL;
#pragma pack ()

extern EFI_GUID  gEfiL05HotKeyServiceProtocolGuid;

#endif
