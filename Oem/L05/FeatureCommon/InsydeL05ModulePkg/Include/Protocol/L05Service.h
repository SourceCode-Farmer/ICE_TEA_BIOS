/** @file
  Definition for L05 Service Protocol

;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_L05_SERVICE_PROTOCOL_H_
#define _EFI_L05_SERVICE_PROTOCOL_H_

#define EFI_L05_SERVICE_PROTOCOL_GUID \
   { 0x697da7e, 0xb2bb, 0x49d0, 0x9d, 0xfa, 0x2, 0xeb, 0x49, 0x9f, 0x9f, 0xfd }

#define MAX_SATA_HDD_NUMBER             0x08
#define MAX_SATA_SSD_NUMBER             0x08
#define MAX_NVME_SSD_NUMBER             0x04
#define MAX_EMMC_SSD_NUMBER             0x04
#define MAX_SATA_ODD_NUMBER             0x01
#define MAX_MODEL_NAME_LEN              0x40

//
// Add NovoButtonMenuResetPassword for Advance SCU
//
typedef enum {
  NovoButtonMenuNormalBoot,
  NovoButtonMenuSetup,
  NovoButtonMenuBootManager,
  NovoButtonMenuOneKey,
  NovoButtonMenuDiagnostics,
} NOVO_BUTTON_MENU_OPTION;

#pragma pack (1)

typedef struct _EFI_L05_SATA_HDD_INFO {
  UINT8                                 DeviceIndex;
  CHAR16                                DeviceModelName[MAX_MODEL_NAME_LEN];
} EFI_L05_STORAGE_DEVICE_INFO;

typedef
EFI_STATUS
(EFIAPI *EFI_L05_SERVICE_NOVO_BUTTON_MENU) (
  IN OUT  UINTN                         *L05NovoButtonMenuSelection
  );

typedef
EFI_STATUS
(EFIAPI *EFI_L05_SERVICE_LOAD_EFI_DRIVER_FROM_FV) (
  IN  EFI_GUID                          *NameGuid
  );

//
// Add L05ClearPassword for Advance SCU
//
typedef struct _EFI_L05_SERVICE_PROTOCOL {
  EFI_L05_SERVICE_NOVO_BUTTON_MENU             L05NovoButtonMenu;
  EFI_L05_SERVICE_LOAD_EFI_DRIVER_FROM_FV      L05LoadEfiDriverFromFv;
  UINT8                                        NumberOfSataHdd;
  UINT8                                        NumberOfSataSsd;
  UINT8                                        NumberOfNvmeSsd;
  UINT8                                        NumberOfEmmcSsd;
  UINT8                                        NumberOfSataOdd;
  EFI_L05_STORAGE_DEVICE_INFO                  SataHddInfo[MAX_SATA_HDD_NUMBER];
  EFI_L05_STORAGE_DEVICE_INFO                  SataSsdInfo[MAX_SATA_SSD_NUMBER];
  EFI_L05_STORAGE_DEVICE_INFO                  NvmeSsdInfo[MAX_NVME_SSD_NUMBER];
  EFI_L05_STORAGE_DEVICE_INFO                  EmmcSsdInfo[MAX_EMMC_SSD_NUMBER];
  EFI_L05_STORAGE_DEVICE_INFO                  SataOddInfo[MAX_SATA_ODD_NUMBER];
} EFI_L05_SERVICE_PROTOCOL;

#pragma pack ()

extern EFI_GUID  gEfiL05ServiceProtocolGuid;

#endif
