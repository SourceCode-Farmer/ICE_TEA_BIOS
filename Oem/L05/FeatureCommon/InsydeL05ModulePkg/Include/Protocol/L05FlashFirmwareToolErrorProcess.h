/** @file
  The L05_FLASH_FIRMWARE_TOOL_ERROR_PROCESS_PROTOCOL is the interface to process H2OFFT error message.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_FLASH_FIRMWARE_TOOL_ERROR_PROCESS_PROTOCOL_H_
#define _L05_FLASH_FIRMWARE_TOOL_ERROR_PROCESS_PROTOCOL_H_

//
// {FD85AC29-A7B0-4c92-931A-566622216FD2}
//
#define L05_FLASH_FIRMWARE_TOOL_ERROR_PROCESS_PROTOCOL_GUID \
  { \
    0xfd85ac29, 0xa7b0, 0x4c92, 0x93, 0x1a, 0x56, 0x66, 0x22, 0x21, 0x6f, 0xd2 \
  }

//
// Forward reference for pure ANSI compatability
//
typedef struct _L05_FLASH_FIRMWARE_TOOL_ERROR_PROCESS_PROTOCOL  L05_FLASH_FIRMWARE_TOOL_ERROR_PROCESS_PROTOCOL;

//
// Protocol API definitions
//

/**
  This function to process H2OFFT error message.

  @param[in]  H2OFftStatus              Status of H2OFFT.
  @param[in]  ErrorString               A pointer to the error string of H2OFFT.

  @retval EFI_SUCCESS                   The function completed successfully.
*/
typedef
EFI_STATUS
(EFIAPI *L05_FLASH_FIRMWARE_TOOL_ERROR_PROCESS) (
  IN EFI_STATUS                         H2OFftStatus,
  IN CHAR16                             *ErrorString
);

//
// Protocol declaration
//
typedef struct _L05_FLASH_FIRMWARE_TOOL_ERROR_PROCESS_PROTOCOL {
  L05_FLASH_FIRMWARE_TOOL_ERROR_PROCESS ErrorProcess;
} L05_FLASH_FIRMWARE_TOOL_ERROR_PROCESS_PROTOCOL;

extern EFI_GUID  gEfiL05FlashFirmwareToolErrorProcessProtocolGuid;

#endif
