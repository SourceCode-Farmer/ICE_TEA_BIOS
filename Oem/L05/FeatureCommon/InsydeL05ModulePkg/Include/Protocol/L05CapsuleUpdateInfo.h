/** @file
  Definition for L05 Capsule Update Info Protocol

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_L05_CAPSULE_UPDATE_INFO_PROTOCOL_H_
#define _EFI_L05_CAPSULE_UPDATE_INFO_PROTOCOL_H_

//
// {82B315E7-62A2-410c-BF21-0CCEE2C9BB8C}
//
#define EFI_L05_CAPSULE_UPDATE_INFO_PROTOCOL_GUID \
  {0x82b315e7, 0x62a2, 0x410c, {0xbf, 0x21, 0x0c, 0xce, 0xe2, 0xc9, 0xbb, 0x8c}}

//
// Forward declaration
//
typedef struct _EFI_L05_CAPSULE_UPDATE_INFO_PROTOCOL  EFI_L05_CAPSULE_UPDATE_INFO_PROTOCOL;

//
// Protocol structure
//
typedef struct _EFI_L05_CAPSULE_UPDATE_INFO_PROTOCOL {
  UINT32                                WindowsUxCapsuleImageOffsetX;
  UINT32                                WindowsUxCapsuleImageOffsetY;
  UINTN                                 WindowsUxCapsuleHeight;
  UINTN                                 WindowsUxCapsuleWidth;
} EFI_L05_CAPSULE_UPDATE_INFO_PROTOCOL;

extern EFI_GUID  gEfiL05CapsuleUpdateInfoProtocolGuid;

#endif
