/** @file

;******************************************************************************
;* Copyright (c) 2012, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*++

Module Name:

  L05EndOfBdsConnect.h

Abstract:

  This protocol will be installed on the end of BdsConnect

--*/

#ifndef _L05_END_OF_BDS_CONNECT_PROTOCOL_H_
#define _L05_END_OF_BDS_CONNECT_PROTOCOL_H_

#define L05_END_OF_BDS_CONNECT_PROTOCOL_GUID  \
  {0xf78c70cd, 0xc4f5, 0x4cf6, 0x8d, 0x00, 0x8a, 0xa1, 0x56, 0xd4, 0x00, 0x8e}

extern EFI_GUID gL05EndOfBdsConnectProtocolGuid;

#endif
