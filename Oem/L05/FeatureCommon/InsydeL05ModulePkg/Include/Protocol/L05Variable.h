/** @file
  Definition for Variable Function

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_L05_VARIABLE_PROTOCOL_H_
#define _EFI_L05_VARIABLE_PROTOCOL_H_

//
// GUID definition
//
#define EFI_L05_VARIABLE_PROTOCOL_GUID \
  { \
    0xbfd02359, 0x8dfe, 0x459a, 0x8b, 0x69, 0xa7, 0x3a, 0x6b, 0xaf, 0xad, 0xc0 \
  }

//
//  Protocol definition
//
typedef struct _EFI_L05_VARIABLE_PROTOCOL  EFI_L05_VARIABLE_PROTOCOL;

//
// L05 Variable Definition
//
#define EFI_L05_VARIABLE_NAME              L"lvar"
#define LENV_SIGNATURE                     SIGNATURE_32('L','E','N','V')
#define INSTANCE_FROM_LENV_THIS(a)         CR (a, LENOVO_VARIABLE_PRIVATE_DATA, LenovoVariableInstance, LENV_SIGNATURE)
#define LENOVO_VARIABLE_NO_DATA            0
#define LENOVO_VARIABLE_USING_REGION_1     1
#define LENOVO_VARIABLE_USING_REGION_2     2
  
#define LENOVO_VARIABLE_REGION_NOT_LOCKED  0
#define LENOVO_VARIABLE_REGION_LOCKED      1

#define LENOVO_VARIABLE_UNLOCK_PASSWORD    L"UnlockPassword"
#define LENOVO_VARIABLE_UNLOCK_FAIL_MAX    5  //Define max fail count of unlocking

//If locked, can not delete or modify this variable, just read only
#define LENOVO_VARIABLE_ATTRIBUTE_LOCKED   1  //BIT0

enum IPGSMIFUNCTIONS {
  INVALID_COMMAND    = 0,
  READ_NVRAM,
  WRITE_NVRAM,
  DELETE_NVRAM,
  LOCK_NVRAM         = 6,
  UNLOCK_NVRAM       = 7,
  GET_BUFFER_ADDRESS = 9,
  COMMAND_FAILED     = 0xFE,
  COMMAND_END        = 0xFF
};

typedef
EFI_STATUS
(EFIAPI *EFI_L05_GET_VARIABLE) (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN OUT  UINT32                        *DataSize,
  OUT     VOID                          *Data
  );

typedef
EFI_STATUS
(EFIAPI *EFI_L05_SET_VARIABLE) (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      UINT32                        DataSize,
  IN      VOID                          *Data
  );

typedef
EFI_STATUS
(EFIAPI *EFI_L05_LOCK_VARIABLE) (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName
  );

typedef
EFI_STATUS
(EFIAPI *EFI_L05_UNLOCK_VARIABLE) (
  IN      EFI_L05_VARIABLE_PROTOCOL     *This,
  IN      EFI_GUID                      *VariableGuidName,
  IN      CHAR16                        *Password
  );

#pragma pack (1)
struct _EFI_L05_VARIABLE_PROTOCOL {
  EFI_L05_GET_VARIABLE                  GetVariable;
  EFI_L05_SET_VARIABLE                  SetVariable;
  EFI_L05_LOCK_VARIABLE                 LockVariable;
  EFI_L05_UNLOCK_VARIABLE               UnlockVariable;
};

typedef struct _L05_SMAPI_DATA
{
  IN OUT UINT16                         FunctionCode;
  IN OUT EFI_GUID                       Guid;         // Variable
  IN OUT CHAR16                         Name[5];      // Variable name
  IN OUT UINT32                         DataLength;   // real data length
  IN OUT UINT8                          Data[1];      // Variable data
} L05_SMAPI_DATA;

typedef struct {
  EFI_GUID                              VariableGuidName;
  UINT32                                VariableDataSize;  //Only VariableData size
  UINT32                                VariableAttribute;
  UINT8                                 VariableData[1];   //The length is not fixed
} LENOVO_VARIABLE_VARIABLE;

typedef struct {
  CHAR8                                 Signature[4];
  UINT32                                PriorityFlag;
  UINT32                                VariableCount;
  UINT8                                 LockFlag;
  UINT8                                 RandomNumber;
  UINT16                                Checksum;
} LENOVO_VARIABLE_RIGION_HEADER;

typedef struct {
  UINT32                                Signature;
  UINT8                                 Region;
  UINT8                                 *VariableBuffer;
  UINT8                                 *LastByte;
  EFI_L05_VARIABLE_PROTOCOL             LenovoVariableInstance;
} LENOVO_VARIABLE_PRIVATE_DATA;
#pragma pack ()

//extern EFI_GUID  gEfiL05SpecificVariableProtocolGuid;
extern EFI_GUID  gEfiL05VariableProtocolGuid;

#endif
