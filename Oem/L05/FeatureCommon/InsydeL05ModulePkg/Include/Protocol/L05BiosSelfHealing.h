/** @file
  BIOS Self-Healing Protocol

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_BIOS_SELF_HEALING_PROTOCOL_H_
#define _L05_BIOS_SELF_HEALING_PROTOCOL_H_

#define EFI_L05_BIOS_SELF_HEALING_PROTOCOL_GUID \
  {0x9281745c, 0x25b7, 0x4814, {0x98, 0x71, 0x32, 0x23, 0xa3, 0xae, 0xeb, 0x1c}}

//
// Forward declaration
//
typedef struct _EFI_L05_BIOS_SELF_HEALING_PROTOCOL EFI_L05_BIOS_SELF_HEALING_PROTOCOL;

//
// Definition
//
typedef enum {
  BshRecovery = 0,
  BshRecoveryDone,
  BshRecoveryDoneReset,
  BshBackup,
  MaxBshUiType
} L05_BIOS_SELF_HEALING_UI_TYPE;

typedef enum {
  BshBackupSbbToSbbrStart      = 0,
  BshUpdatePbbToPbbrStart      = 0,
  BshFlashPbbrToPbbStart       = 0,
  BshUpdatePbbToPbbrStartFlash = 20,
  BshFlashPbbrToPbbStartFlash  = 20,
  BshUpdateSbbToSbbrStart      = 50,
  BshFlashSbbrToSbbStart       = 50,
  BshBackupSbbToSbbrEnd        = 100,
  BshUpdateSbbToSbbrEnd        = 100,
  BshFlashSbbrToSbbEnd         = 100,
  MaxBshProgress
} L05_BIOS_SELF_HEALING_PROGRESS;

#define EFI_STALL_SECONDS(Seconds)                 MultU64x32((UINT64)(Seconds), 1000000)

//
// Function prototypes
//
typedef
EFI_STATUS
(EFIAPI *EFI_BIOS_SELF_HEALING_FUNCTION_SWITCH) (
  IN  BOOLEAN                           EnableFlag
  );

typedef
EFI_STATUS
(EFIAPI *EFI_BIOS_SELF_HEALING_DISPLAY_MESSAGE) (
  IN  L05_BIOS_SELF_HEALING_UI_TYPE     UiType,
  IN  BOOLEAN                           DisableVendorUi,
  IN  BOOLEAN                           DisplayLogo
  );

typedef
EFI_STATUS
(EFIAPI *EFI_BIOS_SELF_HEALING_PROGRESS_BAR) (
  IN  UINTN                             Completion
  );

//
// Protocol structure
//
typedef struct _EFI_L05_BIOS_SELF_HEALING_PROTOCOL {
  EFI_BIOS_SELF_HEALING_FUNCTION_SWITCH     FunctionSwitch;
  EFI_BIOS_SELF_HEALING_DISPLAY_MESSAGE     DisplayMessage;
  EFI_BIOS_SELF_HEALING_PROGRESS_BAR        ProgressBar;
} EFI_L05_BIOS_SELF_HEALING_PROTOCOL;

extern EFI_GUID gEfiL05BiosSelfHealingProtocolGuid;

#endif
