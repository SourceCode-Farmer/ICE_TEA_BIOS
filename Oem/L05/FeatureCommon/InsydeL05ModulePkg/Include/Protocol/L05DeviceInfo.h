/** @file
  SPEC    : Lenovo China Minimum BIOS Spec 128.doc
  Chapter : 3.17 Wireless Device Support

  Definition for BIOS Lock Function

;******************************************************************************
;* Copyright (c) 2012 - 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_L05_DEVICE_INFO_PROTOCOL_H_
#define _EFI_L05_DEVICE_INFO_PROTOCOL_H_

#define EFI_L05_DEVICE_INFO_PROTOCOL_GUID \
   { 0x1f126374, 0x6159, 0x46f7, 0xb5, 0x9, 0x4c, 0x7e, 0x86, 0x52, 0xc4, 0x1e }

typedef enum {
  EFI_L05_DEV_NOT_FOUND,
  EFI_L05_DEV_FOUND,
  EFI_L05_ILLEGAL_DEV_FOUND,
  EFI_L05_DEV_MAX
} EFI_L05_DEVICE_STATE;

typedef struct _EFI_L05_WWAN_DEVICE_INFO_PROTOCOL {
  EFI_L05_DEVICE_STATE                  EfiL05WWANState;
  EFI_L05_DEVICE_STATE                  EfiL05WLANState;
} EFI_L05_DEVICE_INFO_PROTOCOL;

extern EFI_GUID  gEfiL05DeviceInfoProtocolGuid;

#endif
