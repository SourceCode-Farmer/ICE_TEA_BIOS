/** @file
  Protocol used to send HDD spin down command.
;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_L05_HDD_SPIN_DONW_PROTOCOL_H_
#define _EFI_L05_HDD_SPIN_DONW_PROTOCOL_H_

#define EFI_L05_HDD_SPINDOWN_PROTOCOL_GUID \
  {0xeab4ed49, 0x27cd, 0x4904, 0xad, 0x23, 0xaf, 0x1a, 0x4c, 0x81, 0x88, 0x46}

#define EFI_L05_HDD_SPINDOWN_SW_SMI_READY_PROTOCOL_GUID \
  {0x944f2298, 0x618b, 0x43b3, 0xbb, 0x70, 0xf6, 0x9e, 0x57, 0x75, 0x84, 0x63}

typedef struct _EFI_L05_HDD_SPINDOWN_PROTOCOL  EFI_L05_HDD_SPINDOWN_PROTOCOL;

typedef
EFI_STATUS
(EFIAPI *HDD_SPINDOWN_ALL_PORT_FUNCTION) (
  );

typedef struct _EFI_L05_HDD_SPINDOWN_PROTOCOL {
  HDD_SPINDOWN_ALL_PORT_FUNCTION    HddSpinDownAllPort;
} EFI_L05_HDD_SPINDOWN_PROTOCOL;

extern EFI_GUID gEfiL05HddSpindownProtocolGuid;
extern EFI_GUID gEfiL05HddSpindownSwSmiReadyProtocolGuid;

#endif

