/** @file
  Definition for L05 DXE SLP 2.0 Protocol

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_DXE_SLP_20_H_
#define _L05_DXE_SLP_20_H_

#define EFI_L05_DXE_SLP_20_PROTOCOL_GUID \
  {0x81c2accf, 0x5bee, 0x11e3, {0xb1, 0x78, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}

//
// Function prototypes
//
typedef
BOOLEAN
(EFIAPI *EFI_L05_IS_VALID_MARKER) (
  IN VOID                               *Table
  );

//
// Protocol structure
//
typedef struct _EFI_L05_DXE_SLP_20_PROTOCOL {
  EFI_L05_IS_VALID_MARKER               IsValidMarker;
} EFI_L05_DXE_SLP_20_PROTOCOL;

extern EFI_GUID  gEfiL05DxeSlp20ProtocolGuid;

#endif
