/** @file
  Cloud Boot Protocol

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_CLOUD_BOOT_PROTOCOL_H_
#define _L05_CLOUD_BOOT_PROTOCOL_H_

#define EFI_L05_CLOUD_BOOT_PROTOCOL_GUID \
  {0x53e7721d, 0x246e, 0x4d0a, {0xb8, 0x46, 0xaf, 0x9c, 0xee, 0x6d, 0x62, 0xdc}}

#define EFI_L05_HTTP_WIFI_ITEM_COUNT_MAX                     3
#define L05_NETWORK_HTTP_WIFI_ITEM_COUNT                     1
#define L05_SHIFT_UP                                         1
#define L05_SHIFT_TIME_INIT                                  0
#define L05_NETWORK_TYPE_NULL                                0xFF
#define L05_NETWORK_TYPE_MASK                                0xF0

//
// PXE definition
//
#define L05_NETWORK_IPV4                                     0x01
#define L05_NETWORK_IPV6                                     0x02
#define L05_NETWORK_MASK                                     0x00

//
// WIFI PXE definition
//
#define L05_NETWORK_WIFI_IPV4                                0x11
#define L05_NETWORK_WIFI_IPV6                                0x12
#define L05_NETWORK_WIFI_MASK                                0x10

//
// HTTP definition
//
#define L05_NETWORK_HTTP_IPV4                                0x21
#define L05_NETWORK_HTTP_IPV6                                0x22
#define L05_NETWORK_HTTP_MASK                                0x20

//
// WIFI HTTP definition
//
#define L05_NETWORK_HTTP_WIFI_IPV4                           0x31
#define L05_NETWORK_HTTP_WIFI_IPV6                           0x32
#define L05_NETWORK_HTTP_WIFI_MASK                           0x30

//
// Forward declaration
//
typedef struct _EFI_L05_CLOUD_BOOT_PROTOCOL EFI_L05_CLOUD_BOOT_PROTOCOL;

//
// Function prototypes
//
typedef
EFI_STATUS
(EFIAPI *EFI_L05_WAIT_WIFI_CONNECTING) (
  VOID
  );

typedef
BOOLEAN
(EFIAPI *EFI_L05_IS_WIFI_SUPPORT_ENABLE_AND_ENTER_WIFI_CONFIG) (
  IN  CONST  EFI_GUID                             *PackageListGuid
  );

//
// Protocol structure
//
typedef struct _EFI_L05_CLOUD_BOOT_PROTOCOL {
  EFI_L05_WAIT_WIFI_CONNECTING                            WaitWifiConnecting;
  EFI_L05_IS_WIFI_SUPPORT_ENABLE_AND_ENTER_WIFI_CONFIG    IsWifiSupportEnableAndEnterWifiConfig;
} EFI_L05_CLOUD_BOOT_PROTOCOL;

extern EFI_GUID gEfiL05CloudBootProtocolGuid;

#endif
