/** @file
  Definition for Modify Boot Order Service

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _EFI_L05_BOOT_OPTION_H_
#define _EFI_L05_BOOT_OPTION_H_

#include <Protocol/LegacyBios.h>

typedef
EFI_STATUS
(EFIAPI *EFI_MODIFY_BOOT_ORDER) (
  IN UINT8                              BootNormalPriority,
  IN UINT16                             NewPositionPolicy,
  IN BBS_TABLE                          *CurrentBbsItem,
  IN UINTN                              BbsTableIndex,
  IN OUT UINT16                         *BootOrderList,
  IN OUT UINTN                          *BootOrderListSize
  );

typedef
EFI_STATUS
(EFIAPI *EFI_MODIFY_BOOT_OPTION_POSITION) (
  IN UINT8                              BootNormalPriority,
  IN UINT16                             *BootOrderList,
  IN UINTN                              BootOrderNum,
  IN EFI_DEVICE_PATH_PROTOCOL           *DevicePath,
  OUT UINTN                             *NewPosition
  );

typedef
EFI_STATUS
(EFIAPI *EFI_SORT_BOOT_ORDER_BY_DEFAULT) (
  IN OUT UINT16                         *BootOrderList,
  IN     UINTN                          BootOrderNumber,
  IN     UINT8                          BootType
  );

typedef
EFI_STATUS
(EFIAPI *EFI_REMOVE_DEVICE_TYPE_FROM_BOOT_ORDER) (
  IN OUT UINT16                         *BootOrderList,
  IN OUT UINT16                         *BootOrderNumber,
  IN     UINT16                         DeviceType
  );

typedef
EFI_STATUS
(EFIAPI *EFI_MOVE_HDD_TO_THE_TOP_OF_BOOT_ORDER) (
  IN OUT UINT16                         *BootOrderList,
  IN OUT UINTN                          BootOrderNumber
  );

typedef
EFI_STATUS
(EFIAPI *EFI_SORT_PXE_NETWORK_OF_BOOT_ORDER) (
  IN OUT UINT16                         *BootOrderList,
  IN OUT UINTN                          BootOrderNumber
  );

typedef
EFI_STATUS
(EFIAPI *EFI_ADD_BOOT_DEVICE_TITLE_STRING) (
  IN OUT CHAR16                         **Desc,
  IN     BBS_TABLE                      *CurrentBbsTable,
  IN     UINTN                          TableIndex
  );

typedef
EFI_STATUS
(EFIAPI *EFI_UPDATE_BOOT_ORDER_FOR_NOVORECOVERY) (
  IN OUT UINT16                         *BootOrder,
  IN OUT UINT16                         *BootDeviceNum,
  IN     BOOLEAN                        IsGetBootList
  );

typedef
BOOLEAN
(EFIAPI *EFI_IS_LIMITED_TO_INTERNAL_STORAGE) (
  VOID
  );

typedef
EFI_STATUS
(EFIAPI *EFI_DELETE_DUMMY_BOOT_OPTIONS) (
  VOID
  );

typedef struct _EFI_L05_BOOT_OPTION_PROTOCOL {
  EFI_MODIFY_BOOT_ORDER                   ModifyBootOrder;
  EFI_MODIFY_BOOT_OPTION_POSITION         ModifyBootOptionPosition;
  EFI_SORT_BOOT_ORDER_BY_DEFAULT          SortBootOrderByDefault;
  EFI_REMOVE_DEVICE_TYPE_FROM_BOOT_ORDER  RemoveDeviceTypeFromBootOrder;
  EFI_MOVE_HDD_TO_THE_TOP_OF_BOOT_ORDER   MoveHddToTheTopOfBootOrder;
  EFI_SORT_PXE_NETWORK_OF_BOOT_ORDER      SortPxeNetworkOfBootOrder;
  EFI_ADD_BOOT_DEVICE_TITLE_STRING        AddBootDeviceTitleString;
  EFI_UPDATE_BOOT_ORDER_FOR_NOVORECOVERY  UpdateBootOrderForNovoRecovery;
  EFI_IS_LIMITED_TO_INTERNAL_STORAGE      IsLimitedToInternalStorage;
} EFI_L05_BOOT_OPTION_PROTOCOL;

extern EFI_GUID  gEfiL05BootOptionProtocolGuid;

#endif
