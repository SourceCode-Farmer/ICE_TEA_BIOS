/** @file
  Definition for L05 BDS Entry Protocol

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_L05_BDS_ENTRY_SERVICES_H_
#define _EFI_L05_BDS_ENTRY_SERVICES_H_

#define EFI_L05_BDS_ENTRY_PROTOCOL_GUID \
  {0xF9D36AC8, 0x2E87, 0x49CB, {0xB9, 0x87, 0x49, 0x1A, 0x39, 0xA2, 0xC9, 0x14}}

extern EFI_GUID  gEfiL05BdsEntryServicesProtocolGuid;

#endif
