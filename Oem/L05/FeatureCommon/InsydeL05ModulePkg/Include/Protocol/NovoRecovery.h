/** @file
  Definition for One key Recovery

;******************************************************************************
;* Copyright (c) 2012, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _NOVO_RECOVERY_H
#define _NOVO_RECOVERY_H

#define L05_NOVO_RECOVERY_SYSTEM_NAME   L"Lenovo Recovery System"
#define L05_NOVO_RECOVERY_SYSTEM_PATH   L"\\EFI\\Microsoft\\Boot\\LrsBootMgr.efi"

//
// GUID definition
//
#define EFI_NOVO_RECOVERY_PROTOCOL_GUID \
  { \
    0x18382705, 0x9a01, 0x4883, 0x84, 0x92, 0x62, 0xb0, 0xee, 0x2a, 0xae, 0xe2 \
  }

#define EFI_ONE_KEY_RECOVERY_EVENT_2_GUID \
  { \
    0x9e4ac6ce, 0x4b94, 0x4e59, 0xaa, 0xe7, 0xdf, 0x96, 0x2b, 0x33, 0x9b, 0x7c \
  }

#define EFI_ONE_KEY_RECOVERY_SYSTEM_STATUS_GUID \
  { \
    0x30c7c508, 0x7bd9, 0x43c9, 0x80, 0x9a, 0xc7, 0xf0, 0x65, 0x48, 0x3f, 0x3e \
  }

#define EFI_PART_TYPE_LRS_STSTEM_GUID \
    { \
      0xbfbfafe7, 0xa34f, 0x448a, 0x9a, 0x5b, 0x62, 0x13, 0xeb, 0x73, 0x6c, 0x22 \
    }

#define EFI_INSTALL_ONE_KEY_RECOVERY_GUID \
   {  \
      0x3f7500cf, 0x9514, 0x4ca0, 0x84, 0xa9, 0x8b, 0x53, 0xbc, 0xf5, 0x8, 0xe9 \
   }

//
//  Data Structure definition
//
#pragma pack (1)

typedef struct {
  UINT8                                 OkrStatus;  // 0x02 - trigger by application
  UINT8                                 Reserve[7]; // reserve 7 bytes
} OKR_BIOS_DATA;

#pragma pack ()

//
//  Protocol definition
//
extern EFI_GUID  gEfiNovoRecoveryProtocolGuid;
extern EFI_GUID  gEfiOneKeyRecoveryEvent2Guid;
extern EFI_GUID  gEfiOneKeyRecoveryBiosDataGuid;
extern EFI_GUID  gEfiPartTypeLrsSystemGuid;
extern EFI_GUID  gEfiInstallOneKeyRecoveryGuid;

#endif
