/** @file
  Provide SMBIOS DXE interface for L05 feature

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_SMBIOS_INTERFACE_H_
#define _L05_SMBIOS_INTERFACE_H_

#define EFI_L05_SMBIOS_INTERFACE_PROTOCOL_GUID \
  {0xcd9fff70, 0xecb8, 0x11e4, {0x93, 0x73, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}

//
// Forward declaration
//
typedef struct _EFI_L05_SMBIOS_INTERFACE_PROTOCOL  EFI_L05_SMBIOS_INTERFACE_PROTOCOL;

//
// Function prototypes
//

/**
  Add SMBIOS Type function

  @param  Type                          Number of SMBIOS Type
  @param  Context                       Point of input context, optional by Type
  @param  ContextSize                   Size of input context, optional by Type

  @retval EFI_SUCCESS                   Add SMBIOS Type successfully
  @retval EFI_OUT_OF_RESOURCES          Add SMBIOS Type fail for out of resource for allocate memory
  @retval EFI_INVALID_PARAMETER         Add SMBIOS Type fail for optional invalid parameter
  @retval EFI_UNSUPPORTED               Add SMBIOS Type fail for invalid SMBIOS Type
  @retval Others                        An unexpected error occurred by referring EFI_SMBIOS_PROTOCOL.Add()
**/
typedef
EFI_STATUS
(EFIAPI *ADD_SMBIOS_TYPE) (
  IN UINT8                              Type,
  IN VOID                               *Context,
  IN UINTN                              ContextSize
  );

//
// Protocol structure
//
typedef struct _EFI_L05_SMBIOS_INTERFACE_PROTOCOL {
  ADD_SMBIOS_TYPE                       AddSmbiosType;
} EFI_L05_SMBIOS_INTERFACE_PROTOCOL;

extern EFI_GUID  gEfiL05SmbiosInterfaceProtocolGuid;

#endif
