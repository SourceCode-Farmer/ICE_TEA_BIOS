/** @file
  OK Feature Report Interface Protocol

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OK_FEATURE_REPORT_INTERFACE_H_
#define _OK_FEATURE_REPORT_INTERFACE_H_

#define EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL_GUID \
  {0xf16f3fb0, 0xcc74, 0x11e4, {0xad, 0x0a, 0x44, 0x6d, 0x57, 0x15, 0x53, 0xeb}}

//
// Forward declaration
//
typedef struct _EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL  EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL;

//
// Function prototypes
//
typedef
EFI_STATUS
(EFIAPI *RECORD_EXPRESSION) (
  IN CONST CHAR8                        *Format,
  IN       VA_LIST                      Marker
  );

typedef
EFI_STATUS
(EFIAPI *RECORD_HEX_DATA) (
  IN VOID                               *Location,
  IN UINTN                              Length
  );

typedef
EFI_STATUS
(EFIAPI *GET_MESSAGE_FROM_MEMORY) (
  OUT CHAR8                             **Location,
  OUT UINTN                             *TotalSize
  );

//
// Protocol structure
//
typedef struct _EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL {
  RECORD_EXPRESSION                     RecordMessage;
  RECORD_HEX_DATA                       RecordHexData;
  GET_MESSAGE_FROM_MEMORY               GetMessageFromMemory;
} EFI_OK_FEATURE_REPORT_INTERFACE_PROTOCOL;

extern EFI_GUID  gEfiOkFeatureReportInterfaceProtocolGuid;

#endif
