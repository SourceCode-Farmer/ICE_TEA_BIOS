/** @file
  Definition for L05 Setup Menu Protocol

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_L05_SETUP_MENU_H_
#define _EFI_L05_SETUP_MENU_H_

#define EFI_L05_SETUP_MENU_PROTOCOL_GUID \
  {0x51fa828f, 0xc353, 0x11e2, {0xaa, 0x49, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}

//
// Forward declaration
//
typedef struct _EFI_L05_SETUP_MENU_PROTOCOL  EFI_L05_SETUP_MENU_PROTOCOL;

//
// Function prototypes
//
typedef
BOOLEAN
(EFIAPI *EFI_IS_VALID_KEY_DATA) (
  IN EFI_L05_SETUP_MENU_PROTOCOL        *This
  );

typedef
EFI_STATUS
(EFIAPI *EFI_CONVERT_UNICODE_CHAR_To_KEYBOARD_SCANCODE) (
  OUT CHAR16                            *KeyData
  );


typedef
EFI_STATUS
(EFIAPI *EFI_HIDE_DISABLE_PROJECT_DEPENDENCY_ITEM) (
  IN OUT UINT8                          *SetupBuffer,
  IN OUT UINT8                          *PchSetupBuffer,
  IN OUT UINT8                          *CpuSetupBuffer,
  IN OUT UINT8                          *SaSetupBuffer
  );

typedef
EFI_STATUS
(EFIAPI *EFI_OVERRIDE_DEFAULT_SETUP_SETTING) (
  IN OUT UINT8                          *SetupBuffer,
  IN OUT UINT8                          *PchSetupBuffer,
  IN OUT UINT8                          *CpuSetupBuffer,
  IN OUT UINT8                          *SaSetupBuffer
  );

typedef
EFI_STATUS
(EFIAPI *EFI_OVERRIDE_SETUP_SETTING_DURING_POST) (
  IN OUT UINT8                          *SetupBuffer
  );

//
// Protocol structure
//
typedef struct _EFI_L05_SETUP_MENU_PROTOCOL {
  EFI_KEY_DATA                                          CurrentKeyData;
  EFI_IS_VALID_KEY_DATA                                 IsValidKeyData;
  EFI_CONVERT_UNICODE_CHAR_To_KEYBOARD_SCANCODE         ConvertUnicodeCharToKeyboardScancode;
  EFI_HIDE_DISABLE_PROJECT_DEPENDENCY_ITEM              HideDisableProjectDependencyItem;
  EFI_OVERRIDE_DEFAULT_SETUP_SETTING                    OverrideDefaultSetupSetting;
  EFI_OVERRIDE_SETUP_SETTING_DURING_POST                OverrideSetupSettingDuringPost;
} EFI_L05_SETUP_MENU_PROTOCOL;

extern EFI_GUID  gEfiL05SetupMenuProtocolGuid;

#endif
