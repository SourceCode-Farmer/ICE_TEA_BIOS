//;******************************************************************************
//;* Copyright (c) 1983-2011, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#ifndef _EFI_L05_COMPUTRACE_INFO_PROTOCOL_H_
#define _EFI_L05_COMPUTRACE_INFO_PROTOCOL_H_

#define EFI_L05_COMPUTRACE_INFO_PROTOCOL_GUID \
   { 0x313A3C66, 0xC86D, 0x47DE, 0xB8, 0xF6, 0xE6, 0xED, 0x84, 0x0D, 0x97, 0x2F }

//
// Erase Flag state definition
//
#define L05_ERASE_FLAG_ENABLE           (UINT8)0x01
#define L05_ERASE_FLAG_DISABLE          (UINT8)0x00

typedef enum {
  EFI_L05_COMPUTRACE_DISABLED      = 0x7FFFFF00,
  EFI_L05_COMPUTRACE_ENABLED       = 0x7FFFFF01,
  EFI_L05_COMPUTRACE_ENABLING      = 0x7F7FFF01,
  EFI_L05_COMPUTRACE_NOT_SUPPORTED = 0xFFFFFFFF
} EFI_L05_COMPUTRACE_STATE;

typedef struct _EFI_L05_COMPUTRACE_INFO_PROTOCOL {
  EFI_L05_COMPUTRACE_STATE              EfiL05ComputraceState;
} EFI_L05_COMPUTRACE_INFO_PROTOCOL;

#define L05_COMPUTRACE_DEFAULT_MASK     0x5ADBA5BD

#pragma pack(1)

#define L05_COMPUTRACE_SERIAL_LENGTH    0x20

typedef struct _EFI_L05_COMPUTRACE_SERIAL_CRC {
  UINT32                                BatterySerialCrc;
  UINT32                                HardDiskSerialCrc;
  UINT32                                SystemSerialCrc;
} EFI_L05_COMPUTRACE_SERIAL_CRC;

typedef struct _EFI_L05_COMPUTRACE_AREA {
  UINT32                                Mask;
  UINT32                                ComputraceState;
  UINT32                                DisableKey;
  EFI_L05_COMPUTRACE_SERIAL_CRC         ComputraceSerialCrc;
} EFI_L05_COMPUTRACE_AREA;

#pragma pack()

extern EFI_GUID  gEfiL05ComputraceInfoProtocolGuid;

#endif
