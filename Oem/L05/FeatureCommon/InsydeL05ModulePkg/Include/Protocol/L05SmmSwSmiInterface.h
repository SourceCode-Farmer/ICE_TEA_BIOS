/** @file
  Software SMI Interface for registering more than one callback functions with one SMI port number.

;******************************************************************************
;* Copyright (c) 2013 - 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_SMM_SW_SMI_INTERFACE_H_
#define _L05_SMM_SW_SMI_INTERFACE_H_

#define EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL_GUID \
  {0x51646700, 0x7371, 0x11e3, {0xa8, 0xcc, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}

//
// Forward declaration
//
typedef struct _EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL;

//
// Definition for Callback Type Priority (High -> Low)
//
typedef enum {
  ProjectCallbackType = 0,
  FeatureCallbackType,
  ChipsetCallbackType,
  KernelCallbackType,
  InvalidCallbackType
} L05_CALLBACK_TYPE_PRIORITY;

//
// Function prototypes
//

/**
  L05 SW SMI callback function entry point

  @param  CpuIndex                       The CPU core number which triggered the SW SMI

  @retval EFI_SUCCESS                    The operation completed successfully.
                                         The system will break the process of dispatch.
  @retval Others                         An unexpected error occurred.
                                         The system will continue to dispatch other callback functions.
**/
typedef
EFI_STATUS
(EFIAPI *L05_SW_SMI_CALLBACK_FUNCTION) (
  IN UINTN                               CpuIndex
  );

/**
  Register SW SMI callback function Point by SMI port number and Callback Type. (Caller Type 2)

  @param  This                           Point to EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL
  @param  SwSmiPort                      SMI port number
  @param  CallbackType                   The type from enum L05_CALLBACK_TYPE_PRIORITY
  @param  CallbackFunction               Point to Callback Function

  @retval EFI_SUCCESS                    The operation completed successfully.
  @retval EFI_INVALID_PARAMETER          The CallbackType is not valid or CallbackFunction is a NULL point.
  @retval Others                         An unexpected error occurred.
**/
typedef
EFI_STATUS
(EFIAPI *EFI_REGISTER_CALLBACK_FUNCTION) (
  IN EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL         *This,
  IN UINTN                                         SwSmiNum,
  IN L05_CALLBACK_TYPE_PRIORITY                    CallbackType,
  IN L05_SW_SMI_CALLBACK_FUNCTION                  CallbackFunction
  );

/**
  Un-register SW SMI callback function Point by SMI port number. (Caller Type 2)

  @param  This                 Point to EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL
  @param  SwSmiPort            SMI port number
  @param  CallbackFunction     Point to Callback Function

  @retval EFI_SUCCESS          The operation completed successfully.
  @retval Others               An unexpected error occurred.
**/
typedef
EFI_STATUS
(EFIAPI *EFI_UNREGISTER_CALLBACK_FUNCTION) (
  IN EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL         *This,
  IN UINTN                                         SwSmiNum,
  IN L05_SW_SMI_CALLBACK_FUNCTION                  CallbackFunction
  );

//
// Protocol structure
//
typedef struct _EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL {
  EFI_REGISTER_CALLBACK_FUNCTION         RegisterCallbackFunction;
  EFI_UNREGISTER_CALLBACK_FUNCTION       UnRegisterCallbackFunction;
} EFI_L05_SMM_SW_SMI_INTERFACE_PROTOCOL;

extern EFI_GUID gEfiL05SmmSwSmiInterfaceProtocolGuid;

#endif
