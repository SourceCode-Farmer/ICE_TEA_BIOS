/** @file
  Definition for L05 Before Ready To Boot

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_BEFORE_READY_TO_BOOT_H__
#define _L05_BEFORE_READY_TO_BOOT_H__

#define EFI_L05_BEFORE_READY_TO_BOOT_PROTOCOL_GUID \
  {0x6a32f2e1, 0x57f7, 0x11e4, {0x85, 0x98, 0x04, 0x7d, 0x7b, 0x99, 0xe0, 0x97}}

extern EFI_GUID  gEfiL05BeforeReadyToBootProtocolGuid;

#endif
