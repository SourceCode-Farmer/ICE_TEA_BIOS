//;******************************************************************************
//;* Copyright (c) 1983-2020, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************

#include "SystemPasswordsReset.h"
//[-start-211229-BAIN000078-add]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
#include <Library/LfcEcLib.h>
#endif
//[-end-211229-BAIN000078-add]//

/**
  System passwords reset callback.

 @param[in] Event                       The Event this notify function registered to.
 @param[in] Handle                      The handle associated with a previously registered checkpoint handler.
**/
VOID
EFIAPI
SystemPasswordsResetCallback (
  IN EFI_EVENT                          Event,
  IN H2O_CP_HANDLE                      Handle
  )
{
  EFI_STATUS                            SupervisorPasswordStatus;
  EFI_STATUS                            UserPasswordStatus;
  UINT8                                 DataBuffer[CLEAR_PASSWORD_LENGTH] = {0};
//[-start-211229-BAIN000078-add]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
  EFI_STATUS                            Status = EFI_SUCCESS;
#endif
//[-end-211229-BAIN000078-add]//

//[-start-211229-BAIN000078-add]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    Status = LfcEcLibDisableWatchDogForMemoryTrainning();
    ASSERT_EFI_ERROR (Status);
#endif
//[-end-211229-BAIN000078-add]//

  SupervisorPasswordStatus  = EFI_NOT_FOUND;
  UserPasswordStatus        = EFI_NOT_FOUND;

  DisableQuietBoot ();

  gST->ConOut->ClearScreen (gST->ConOut);
  gST->ConOut->EnableCursor (gST->ConOut, FALSE);

  //
  // Clear Administrator Password
  //
  ZeroMem (DataBuffer, sizeof (DataBuffer));
  SupervisorPasswordStatus  = OemSvcSetSystemPasswords (SystemSupervisor, CLEAR_PASSWORD_LENGTH, DataBuffer);

  //
  // Clear User Password
  //
  ZeroMem (DataBuffer, sizeof (DataBuffer));
  UserPasswordStatus    = OemSvcSetSystemPasswords (SystemUser, CLEAR_PASSWORD_LENGTH, DataBuffer);

  if (SupervisorPasswordStatus  == EFI_MEDIA_CHANGED) {
    SupervisorPasswordStatus = EFI_SUCCESS;
  }

  if (UserPasswordStatus == EFI_MEDIA_CHANGED) {
    UserPasswordStatus = EFI_SUCCESS;
  }

  if (!EFI_ERROR (SupervisorPasswordStatus) && !EFI_ERROR (UserPasswordStatus)) {
    Print (L"\nAll Passwords are cleared, Please shut down the computer");

  } else {
    Print (L"\nAll Passwords are NOT cleared, Error code : %x, %x", (UINT32) SupervisorPasswordStatus, (UINT32) UserPasswordStatus);
  }

  gBS->RaiseTPL (TPL_HIGH_LEVEL);
  CpuDeadLoop ();

  return;
}

/**
  System passwords reset entry.

  @param  ImageHandle                   The firmware allocated handle for the UEFI image.
  @param  SystemTable                   A pointer to the EFI System Table.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
L05SystemPasswordsResetEntryPoint (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  H2O_CP_HANDLE                         CpHandle;

  //
  // Register callback on H2O_CP_MEDIUM_HIGH of gH2OBdsCpConOutConnectAfterGuid event.
  //
  Status = H2OCpRegisterHandler (
             &gH2OBdsCpConOutConnectAfterGuid,
             SystemPasswordsResetCallback,
             H2O_CP_MEDIUM_HIGH,
             &CpHandle
             );
  
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "Checkpoint Register Fail: %g (%r)\n", &gH2OBdsCpConOutConnectAfterGuid, Status));
    return EFI_ABORTED;
  }

  return EFI_SUCCESS;
}

