//;******************************************************************************
//;* Copyright (c) 1983-2020, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
#ifndef _EFI_L05_SYS_PASSWORDS_RESET_FUNCTION_H_
#define _EFI_L05_SYS_PASSWORDS_RESET_FUNCTION_H_

#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/PrintLib.h>
#include <Library/FeatureLib/OemSvcSecurityPassword.h>
#include <Library/OemGraphicsLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseCryptLib.h>
#include <Library/H2OCpLib.h>

#ifndef L05_NOTEBOOK_PASSWORD_ENABLE
#define CLEAR_PASSWORD_LENGTH           0x01
#else
#define CLEAR_PASSWORD_LENGTH           SHA256_DIGEST_SIZE
#endif

#define DEFAULT_HorizontalResolution    640
#define DEFAULT_VerticalResolution      480
//
//  Record the current VGA Mode from EFI code
//
#define EFI_CURRENT_VGA_MODE_ADDRESS    0x4A3
//
//  Record the current VGA Mode from VGA OpRom
//
#define CURRENT_VGA_MODE_ADDRESS        0x449

#endif
