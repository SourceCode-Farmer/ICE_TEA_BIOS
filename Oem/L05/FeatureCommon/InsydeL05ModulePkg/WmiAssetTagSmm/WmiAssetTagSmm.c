/** @file
  Provide functions for WMI Asset Tag Service

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include "WmiAssetTagSmm.h"

EFI_L05_GLOBAL_NVS_AREA                 *mL05GlobalNVSArea = NULL;

/**
  A callback function for WMI Asset Tag.

  @param  DispatchHandle                Unused.
  @param  DispatchContext               Unused.
  @param  CommBuffer                    Unused.
  @param  CommBufferSize                Unused.

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval EFI_UNSUPPORTED               The feature is not supported.
  @retval EFI_INVALID_PARAMETER         The parameter used for operation is not valid.
  @retval EFI_ACCESS_DENIED             The operation needs password and the password is not correct.
  @retval EFI_UNSUPPORTED               An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
L05SmmWmiAssetTagCallback (
  IN     EFI_HANDLE                     DispatchHandle,
  IN     CONST VOID                     *DispatchContext,
  IN OUT VOID                           *CommBuffer,
  IN OUT UINTN                          *CommBufferSize
  )
{
  EFI_STATUS                            Status;

  //
  // Initialization
  //
  Status = EFI_UNSUPPORTED;

  //
  // Call object's function
  //
  switch (mL05GlobalNVSArea->L05WmiObjectId) {

  case L05_READ_ASSET_TAG_B0:
    Status = WmiReadAssetTagB0 ();
    break;

  case L05_WRITE_ASSET_TAG_B1:
    Status = WmiWriteAssetTagB1 ();
    break;

  default:
    break;
  }

  //
  // Report WMI status
  //
  switch (Status) {

  case EFI_SUCCESS:
    mL05GlobalNVSArea->L05WmiStatus = L05_WMI_SUCCESS;
    break;

  case EFI_UNSUPPORTED:
    mL05GlobalNVSArea->L05WmiStatus = L05_WMI_NOT_SUPPORTED;
    break;

  case EFI_INVALID_PARAMETER:
    mL05GlobalNVSArea->L05WmiStatus = L05_WMI_INVALID_PARAMETER;
    break;

  case EFI_ACCESS_DENIED:
    mL05GlobalNVSArea->L05WmiStatus = L05_WMI_ACCESS_DENIED;
    break;

  default:
    mL05GlobalNVSArea->L05WmiStatus = L05_WMI_NOT_SUPPORTED;
  }

  return Status;
}

/**
  Find the operation region in WMI ACPI table by given Name and Size,
  and initialize it if the region is found.

  @param Table                          The WMI item in ACPI table.
  @param Name                           The name string to find in WMI table.
  @param Size                           The size of the region to find.
  @param Address                        The allocated address for the found region.

  @retval EFI_SUCCESS                   The function completed successfully.
**/
EFI_STATUS
AssignOpRegion (
  IN OUT EFI_ACPI_DESCRIPTION_HEADER    *Table,
  IN     UINT32                         Name,
  IN     UINT16                         Size,
  IN OUT VOID                         **Address OPTIONAL
  )
{
  EFI_STATUS                            Status;
  AML_OP_REGION_32_32                    *OpRegion;
  EFI_PHYSICAL_ADDRESS                  MemoryAddress;

  Status = EFI_NOT_FOUND;
  MemoryAddress = (EFI_PHYSICAL_ADDRESS) (UINTN) * Address;

  //
  // Patch some pointers for the ASL code before loading the SSDT.
  //
  for (OpRegion  = (AML_OP_REGION_32_32 *) (Table + 1);
       OpRegion <= (AML_OP_REGION_32_32 *) ((UINT8 *) Table + Table->Length);
       OpRegion  = (AML_OP_REGION_32_32 *) ((UINT8 *) OpRegion + 1)) {
    if ((OpRegion->OpRegionOp   == AML_EXT_REGION_OP) &&
        (OpRegion->NameString   == Name) &&
        (OpRegion->OffsetPrefix == AML_DWORD_PREFIX) &&
        (OpRegion->LenPrefix    == AML_DWORD_PREFIX)) {

      if (MemoryAddress == 0) {
        MemoryAddress = SIZE_4GB - 1;
        Status = gBS->AllocatePages (AllocateMaxAddress, EfiACPIMemoryNVS, EFI_SIZE_TO_PAGES (Size), &MemoryAddress);

        if (EFI_ERROR (Status)) {
          return Status;
        }

        ZeroMem ((VOID *) (UINTN) MemoryAddress, Size);
      }

      OpRegion->RegionOffset = (UINT32) (UINTN) MemoryAddress;
      OpRegion->RegionLen    = (UINT32) Size;
      Status = EFI_SUCCESS;
      break;
    }
  }

  *Address = (VOID *) (UINTN) MemoryAddress;

  return Status;
}

/**
  Initialize and publish WMI items in ACPI table.

  @param  None

  @retval EFI_SUCCESS                   The WMI ACPI table is published successfully.
  @retval Others                        The WMI ACPI table is not published.
**/
EFI_STATUS
PublishAcpiTable (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_ACPI_TABLE_PROTOCOL               *AcpiTable;
  UINTN                                 TableKey;
  EFI_ACPI_DESCRIPTION_HEADER           *Table;
  UINTN                                 TableSize;

  Status = GetSectionFromFv (
             &gEfiCallerIdGuid,
             EFI_SECTION_RAW,
             0,
             (VOID **) &Table,
             &TableSize
             );
  ASSERT_EFI_ERROR (Status);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  ASSERT (Table->OemTableId == SIGNATURE_64 ('W', 'm', 'i', 'T', 'a', 'b', 'l', '2'));

  Status = AssignOpRegion (Table, SIGNATURE_32 ('L', 'N', 'V', '2'), (UINT16) sizeof (EFI_L05_GLOBAL_NVS_AREA), (VOID **) &mL05GlobalNVSArea);
  ASSERT_EFI_ERROR (Status);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Publish the WMI ACPI table
  //
  Status = gBS->LocateProtocol (&gEfiAcpiTableProtocolGuid, NULL, (VOID **) &AcpiTable);
  ASSERT_EFI_ERROR (Status);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  TableKey = 0;
  Status = AcpiTable->InstallAcpiTable (
                        AcpiTable,
                        Table,
                        TableSize,
                        &TableKey
                        );
  ASSERT_EFI_ERROR (Status);

  return Status;
}

/**
  WMI Asset Tag SMM entry

  @param  ImageHandle                   The firmware allocated handle for the UEFI image.
  @param  SystemTable                   A pointer to the EFI System Table.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
L05WmiAssetTagSmmEntry (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status = EFI_SUCCESS;
  EFI_HANDLE                            SwHandle;
  EFI_SMM_SW_DISPATCH2_PROTOCOL         *SwDispatch;
  EFI_SMM_SW_REGISTER_CONTEXT           SwContext;

  if (!InSmm ()) {
    return EFI_SUCCESS;
  }

  //
  // Publish ACPI table
  //
  Status = PublishAcpiTable ();
  ASSERT_EFI_ERROR (Status);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Get the Sw dispatch protocol
  //
  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmSwDispatch2ProtocolGuid,
                    NULL,
                    &SwDispatch
                    );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Register Software SMI function
  //
  SwContext.SwSmiInputValue = EFI_L05_WMI_ASSET_TAG_CALLBACK;
  Status = SwDispatch->Register (
                         SwDispatch,
                         L05SmmWmiAssetTagCallback,
                         &SwContext,
                         &SwHandle
                         );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  return Status;
}
