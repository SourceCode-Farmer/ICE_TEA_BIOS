/** @file
  Provide functions for WMI Asset Tag SMM Service

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_WMI_ASSEET_TAG_SMM_H_
#define _L05_WMI_ASSEET_TAG_SMM_H_

#include <OemSwSmi.h>
#include <L05Config.h>
#include <L05WmiAssetTagDefinition.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/AcpiTable.h>
#include <Protocol/SmmFwBlockService.h>  // EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL
#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
#include <Protocol/L05Variable.h>
#endif

#include <Library/BaseLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DxeServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/FlashRegionLib.h>      // FdmGetNAtAddr

#define L05_TEMP_BUFFER_SIZE            (sizeof(CHAR8) * 17)

#pragma pack (1)
//
// L05 WMI Asset Tag NVS Area definition
//
typedef struct {
  UINT8                                 L05WmiObjectId;                        // WOID
  UINT8                                 L05WmiStatus;                          // WSTA
  UINT16                                L05WmiBufferLen;                       // LWBL
  UINT8                                 L05WmiBuffer[L05_WMI_BUFFER_MAX_SIZE]; // LWBF
} EFI_L05_GLOBAL_NVS_AREA;

//
// AML decode struct for 32bit Offset & 32bit region length
//
typedef struct {
  UINT8                                 OpRegionOp;
  UINT32                                NameString;
  UINT8                                 RegionSpace;
  UINT8                                 OffsetPrefix;
  UINT32                                RegionOffset;
  UINT8                                 LenPrefix;
  UINT32                                RegionLen;
} AML_OP_REGION_32_32;
#pragma pack ()

EFI_STATUS
EFIAPI
WmiReadAssetTagB0 (
  VOID
  );

EFI_STATUS
EFIAPI
WmiWriteAssetTagB1 (
  VOID
  );

extern EFI_L05_GLOBAL_NVS_AREA          *mL05GlobalNVSArea;
extern CHAR8                            *mTempBuffer;

#endif
