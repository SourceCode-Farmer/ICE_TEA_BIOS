/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <L05WmiAssetTagDefinition.h>
#include <OemSwSmi.h>  // EFI_L05_WMI_ASSET_TAG_SW_SMI

#define SOFTWARE_SMI_PORT               SSMP

DefinitionBlock (
  "WmiAssetTag.aml",
  "SSDT",
  1,
  "Insyde",
  "WmiTabl2",
  0x1000
  )
{
//  External (P8XH, MethodObj)
//  External (MBGS, MethodObj)
//  External (DB2H, MethodObj)
//  External (DW2H, MethodObj)
//  External (DD2H, MethodObj)
  
  Scope(\_SB)
  {
    Mutex(MUT2, 0)
    
    //
    // WMI-to-ACPI mapper device.
    //
    Device(WMI2)
    {
      // PNP0C14 is Plug and Play ID assigned to WMI mapper
      Name(_HID, EISAID("PNP0C14"))
      Name(_UID, "ATAG")

      //
      // _WDG evaluates to a data structure that specifies the data
      // blocks supported by the ACPI device.
      //
      Name(_WDG, Buffer()
      {
        // -------- Method execution for Lenovo_AssetTag : Lenovo_AssetTagElement
        // GUID F9AEC85F-4CBD-4668-B242-D261F1300FFE
        0x5F, 0xC8, 0xAE, 0xF9, 0xBD, 0x4C, 0x68, 0x46, 0xB2, 0x42,
        0xD2, 0x61, 0xF1, 0x30, 0x0F, 0xFE,
        0x42, 0x30, // Object ID (B0)
        1,          // Instance Count
        0x05,       // Flags WMIACPI_REGFLAG_EXPENSIVE & STRING
        // -------- Method execution for Lenovo_AssetTagWrite : Lenovo_AssetTagElement
        // GUID B574A36A-955A-406C-AFBE-63FB365732F8
        0x6A, 0xA3, 0x74, 0xB5, 0x5A, 0x95, 0x6C, 0x40, 0xAF, 0xBE,
        0x63, 0xFB, 0x36, 0x57, 0x32, 0xF8,
        0x42, 0x31, // Object ID (B1)
        1,          // Instance Count
        0x06,       // Flags WMIACPI_REGFLAG_METHOD & WMIACPI_REGFLAG_STRING
        // GUID for returning MOF data
        0x21, 0x12, 0x90, 0x05, 0x66, 0xD5, 0xD1, 0x11, 0xB2, 0xF0,
        0x00, 0xA0, 0xC9, 0x06, 0x29, 0x10, 
        0x42, 0x42, // Object ID (BB)
        1,          // Instance Count
        0x00        // Flags
      })

      Name(RETN, Package()
      {
        "Success",
        "Not Supported",
        "Invalid Parameter",
        "Access Denied",
        "System Busy"
      })

      Include ("WmiGlobalNvsData.asl")
      Include ("WQBB.asl")

      //
      // WQB0() - Execution for Lenovo_AssetTag
      //
      // Read Asset Tag from EEPROM
      //
      //     Arg0 is instance index of data block
      //
      //     Return: Asset Tag
      //
      Method (WQB0, 1, Serialized)
      {
        Acquire(MUT2, 0xFFFF)

//        P8XH (1,0xDD)
//        P8XH (0,0xB0)
//        MBGS ("WQB0:")

        Store(L05_READ_ASSET_TAG_B0, WOID)
        Store(Ones, WSTA)
        Store(0,    LWBL)
        Store(0,    LWBF)

        Store (EFI_L05_WMI_ASSET_TAG_SW_SMI, SOFTWARE_SMI_PORT)

        If (LNotEqual (LWBL, Zero)) {
          Store (ToString (LWBF, LWBL), Local0)
        } Else {
          Store (WMI_ASSET_TAG_NULL_STR, Local0)
        }

//        MBGS (Local0)
        Release(MUT2)
        Return(Local0)
      }

      //
      // WMB1() - Method execution for Lenovo_AssetTagWrite
      //
      // Write Asset Tag to EEPROM - max 16 bytes
      //
      //     Arg0 is instance being queried
      //     Arg1 is the method ID
      //     Arg2 is the method data passed
      //
      //     Return: Error string
      //
      Method(WMB1, 3, Serialized)
      {
        Acquire(MUT2, 0xFFFF)

//        P8XH (1,0xDD)
//        P8XH (0,0xB1)
//        MBGS ("WMB1:")
//        MBGS (Arg2)

        If (LEqual(SizeOf(Arg2), 0)) {
          Return(WMI_ASSET_TAG_NULL_STR)
        }

        If (LGreaterEqual(SizeOf(Arg2), 16)) {
          Return(WMI_ASSET_TAG_NULL_STR)
        }

        Store(L05_WRITE_ASSET_TAG_B1, WOID)
        Store(Ones, WSTA)
        Store(0, LWBF)
        Store(SizeOf(Arg2), LWBL)
        Store(Arg2, LWBF)

        Store (EFI_L05_WMI_ASSET_TAG_SW_SMI, SOFTWARE_SMI_PORT)

        Release(MUT2)
        Return(DerefOf(Index(RETN, WSTA)))
      }
    }
  }
}
