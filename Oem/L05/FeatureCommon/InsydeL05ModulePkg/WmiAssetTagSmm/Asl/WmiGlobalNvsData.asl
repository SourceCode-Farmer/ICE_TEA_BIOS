/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

OperationRegion (LNV2, SystemMemory, 0x55AA55AA, 0x55AA55AA)
Field (LNV2, AnyAcc, NoLock, Preserve)
{
  WOID, 8,    // UINT8       L05WmiObjectId;
  WSTA, 8,    // UINT8       L05WmiStatus;
  LWBL, 16,   // UINT16      L05WmiBufferLen;
  LWBF, L05_WMI_BUFFER_MAX_SIZE_BIT, // UINT8       L05WmiBuffer[L05_WMI_BUFFER_MAX_SIZE];
}

//
// Operational region for Software SMI port access
//
OperationRegion (SMPR, SystemIO, FixedPcdGet16 (PcdSoftwareSmiPort), 1)
Field (SMPR, ByteAcc, NoLock, Preserve)
{
  SSMP, 8
}
