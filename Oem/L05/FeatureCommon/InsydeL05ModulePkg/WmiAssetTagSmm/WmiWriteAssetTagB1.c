/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "WmiAssetTagSmm.h"

/**
  WMI Method for Write Asset Tag -

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval EFI_ACCESS_DENIED             WMI input password is invalid.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
WmiWriteAssetTagB1 (
  VOID
  )
{
  EFI_STATUS                            Status;
#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  EFI_L05_VARIABLE_PROTOCOL             *L05VariablePtr;
  UINT32                                DataLength;
  CHAR8                                 *AssetTag;
#else
  EFI_SMM_FW_BLOCK_SERVICE_PROTOCOL     *SmmFwBlockService;
  UINT8                                 *EepromBuffer;
  EFI_L05_EEPROM_MAP_120                *EepromPtr;
  UINTN                                 EepromBase;
  UINTN                                 EepromSize;
#endif

  //
  // Initialization
  //
  Status = EFI_SUCCESS;

#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
  L05VariablePtr = NULL;

  Status = gSmst->SmmLocateProtocol (&gEfiL05VariableProtocolGuid, NULL, &L05VariablePtr);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  DataLength = L05_EEPROM_ASSET_TAG_LENGTH;
  AssetTag = AllocateZeroPool (DataLength);
  Status = L05VariablePtr->GetVariable (
                             L05VariablePtr,
                             &gL05AssetTagGuid,
                             &DataLength,
                             AssetTag
                             );

  if (EFI_ERROR (Status) && (Status != EFI_NOT_FOUND)) {
    return Status;
  }

  if (mL05GlobalNVSArea->L05WmiBufferLen < L05_EEPROM_ASSET_TAG_LENGTH) {
    Status = L05VariablePtr->SetVariable (
                               L05VariablePtr,
                               &gL05AssetTagGuid,
                               mL05GlobalNVSArea->L05WmiBufferLen,
                               mL05GlobalNVSArea->L05WmiBuffer
                               );
  }
#else
  SmmFwBlockService = NULL;
  EepromBase        = 0;
  EepromSize        = 0;
  Status = gSmst->SmmLocateProtocol (&gEfiSmmFwBlockServiceProtocolGuid, NULL, &SmmFwBlockService);

  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }

  EepromBase = (UINTN) FdmGetNAtAddr (&gL05H2OFlashMapRegionEepromGuid, 1);
  EepromSize = (UINTN) FdmGetNAtSize (&gL05H2OFlashMapRegionEepromGuid, 1);

  if ((EepromBase == 0) || (EepromSize == 0)) {
    return EFI_UNSUPPORTED;
  }

  //
  // Get EEPROM data
  //
  EepromBuffer = AllocatePages (EFI_SIZE_TO_PAGES (EepromSize));

  if (EepromBuffer == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  ZeroMem (EepromBuffer, (EFI_SIZE_TO_PAGES (EepromSize) * EFI_PAGE_SIZE));
  CopyMem ((VOID *) EepromBuffer, (VOID *) EepromBase, EepromSize);
  EepromPtr = (EFI_L05_EEPROM_MAP_120 *) EepromBuffer;

  //
  // Write Asset Tag into Eeprom
  //
  Status = SmmFwBlockService->EraseBlocks (SmmFwBlockService, EepromBase, &EepromSize);

  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (mL05GlobalNVSArea->L05WmiBufferLen < L05_EEPROM_ASSET_TAG_LENGTH) {
    AsciiStrCpyS (&EepromPtr->AssetTag[0], L05_EEPROM_ASSET_TAG_LENGTH, mL05GlobalNVSArea->L05WmiBuffer);
    Status = SmmFwBlockService->Write (SmmFwBlockService, EepromBase, &EepromSize, (VOID *) EepromBuffer);

    //
    // Set WMI buffer length
    //
    mL05GlobalNVSArea->L05WmiBufferLen = (UINT16) AsciiStrLen (&EepromPtr->AssetTag[0]);
  }

  if (EepromBuffer != NULL) {
    FreePool (EepromBuffer);
    EepromBuffer = NULL;
  }
#endif

  return Status;
}
