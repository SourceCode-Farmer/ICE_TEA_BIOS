/** @file
  Secure Boot Service for L05 feature

;******************************************************************************
;* Copyright (c) 2012 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SECURE_BOOT_SERVICE_H_
#define _SECURE_BOOT_SERVICE_H_

#include <Uefi.h>
#include <L05ChipsetNameList.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/VariableLib.h>

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//

//
// Produced Protocols
//

//
// Guids
//
#include <Guid/AdmiSecureBoot.h>  // EFI_SECURE_BOOT_ENFORCE_NAME
#include <Guid/DebugMask.h>       // gEfiGenericVariableGuid
#include <Guid/AuthenticatedVariableFormat.h>  // MAX_VARIABLE_NAME_SIZE, MAX_VARIABLE_SIZE

//
// Driver Version
//

//
// Protocol instances
//
#include <Protocol/L05SecureBoot.h>
#include <Protocol/Variable.h>
#include <Protocol/NonVolatileVariable.h>
#include <Protocol/SmmCommunication.h> // EFI_SMM_COMMUNICATION_PROTOCOL, EFI_SMM_COMMUNICATE_HEADER

//
// Include files with function prototypes
//
#include <SetupConfig.h>
#include <SmiTable.h>
#include <SensitiveVariableFormat.h>  // SMM_VAR_BUFFER, SMM_COMMUNICATE_HEADER_SIZE, SMM_COMMUNICATE_BUFFER_SIZE

#define H2O_CCB_VERSION_OF_ECR_77485    0x05430500

UINT8
SmmSecureBootCall (
  IN  UINT8                             *InPutBuff, // rcx
  IN  UINTN                             DataSize,   // rdx
  IN  UINT8                             SubFunNum,  // r8
  IN  UINT16                            SmiPort     // r9
  );
#endif
