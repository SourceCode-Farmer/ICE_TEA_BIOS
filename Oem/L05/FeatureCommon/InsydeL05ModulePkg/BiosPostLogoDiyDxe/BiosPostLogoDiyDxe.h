/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _BIOS_POST_LOGO_DIY_DXE_H_
#define _BIOS_POST_LOGO_DIY_DXE_H_

#include <Guid/Gpt.h>
#include <Guid/L05EspCustomizePostLogoInfoVariable.h>
#include <Guid/L05EspCustomizePostLogoVcmVariable.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PrintLib.h>
#include <Library/DebugLib.h>
#include <Library/H2OCpLib.h>
#include <Library/OemGraphicsLib.h>
#include <Library/FeatureLib/OemSvcCustomizePostLogoSupportFormat.h>
#include <Protocol/EdidDiscovered.h>
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/L05EndOfBdsConnect.h>

#pragma pack(1)
typedef struct _L05_CUSTOMIZE_POST_LOGO_FORMAT_TABLE {
  UINT8                                 SupportFormat;
  BOOLEAN                               IsSupported;
  CHAR16                                *FileNameExtension;
  EFI_BADGING_SUPPORT_FORMAT            Format;
} L05_CUSTOMIZE_POST_LOGO_FORMAT_TABLE;
#pragma pack()

#endif
