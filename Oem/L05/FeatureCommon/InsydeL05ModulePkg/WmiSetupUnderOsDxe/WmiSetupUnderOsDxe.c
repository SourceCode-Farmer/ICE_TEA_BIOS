/** @file
  Provide functions for WMI DXE Service

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "WmiSetupUnderOsDxe.h"

/**

  WMI Setup under OS DXE entry

  @param  ImageHandle                   The firmware allocated handle for the UEFI image.
  @param  SystemTable                   A pointer to the EFI System Table.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.

**/
EFI_STATUS
L05WmiSetupUnderOsDxeEntry (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
  )
{
  EFI_STATUS                            Status;

  Status = WmiL05SecureBootSyncInit ();
  
#ifdef L05_HDD_PASSWORD_ENABLE
  Status = WmiL05HddPasswordSyncInit ();
#endif

  return Status;
}
