/** @file
  Provide functions for WMI DXE Service

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _WMI_SETUP_UNDER_OS_DXE_H_
#define _WMI_SETUP_UNDER_OS_DXE_H_

#include <L05WmiSetupUnderOsDefinition.h>
#include <SetupConfig.h>  // SYSTEM_CONFIGURATION
#include <L05ChipsetNameList.h>
#include <Guid/L05WmiSetupUnderOsSetupVariable.h>  // gL05WmiSetupUnderOsSetupVariableGuid
#include <Guid/HddPasswordVariable.h> // HDD_PASSWORD_TABLE
#include <Protocol/SmmVariable.h> // gEfiSmmVariableProtocolGuid
#include <Protocol/L05SecureBoot.h> // gEfiL05SecureBootProtocolGuid
#include <Protocol/HddPasswordService.h> // gEfiHddPasswordServiceProtocolGuid
#include <Protocol/H2ODialog.h> // gH2ODialogProtocolGuid
#include <Library/BaseLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/VariableLib.h>
#include <Library/PrintLib.h>
#include <Library/OemGraphicsLib.h>
#ifdef L05_NOTEBOOK_PASSWORD_ENABLE
#include <Library/EncodeHddPasswordTableLib.h>
#endif

EFI_STATUS
WmiL05SecureBootSyncInit (
  VOID
  );

#ifdef L05_HDD_PASSWORD_ENABLE
EFI_STATUS
WmiL05HddPasswordSyncInit (
  VOID
  );
#endif

#endif
