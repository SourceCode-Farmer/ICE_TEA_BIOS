/** @file
  Provide functions for sync WMI Secure Boot status

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "WmiSetupUnderOsDxe.h"

/**

  WMI secure boot sync.

  @param  None

  @retval EFI_SUCCESS                   This function execute successfully.
  @retval Others                        An unexpected error occurred.

**/
EFI_STATUS
WmiL05SecureBootSync (
  VOID
  )
{
  EFI_STATUS                            Status;
  UINTN                                 BufferSize;

  EFI_L05_SECURE_BOOT_DATA              L05SecureBootData;
  EFI_L05_SECURE_BOOT_PROTOCOL          *L05SecureBootProtocol;

  //
  // Get WMI secure boot data
  //
  BufferSize = sizeof (EFI_L05_SECURE_BOOT_DATA);
  Status = gRT->GetVariable (
                  L05_WMI_SECURE_BOOT_DATA_VARIABLE_NAME,
                  &gL05WmiSetupUnderOsSetupVariableGuid,
                  NULL,
                  &BufferSize,
                  &L05SecureBootData
                  );

  if (EFI_ERROR (Status)) {
    //
    // There is no need to update L05 secure boot
    //
    return Status;
  }

  //
  // Clean WMI secure boot data variable
  //
  Status = gRT->SetVariable (
                  L05_WMI_SECURE_BOOT_DATA_VARIABLE_NAME,
                  &gL05WmiSetupUnderOsSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  0,
                  NULL
                  );

  Status = gBS->LocateProtocol (
                  &gEfiL05SecureBootProtocolGuid,
                  NULL,
                  &L05SecureBootProtocol
                  );

  if (!EFI_ERROR (Status)) {
    Status = L05SecureBootProtocol->L05SecureBootCallback (&L05SecureBootData);
  }

  gRT->ResetSystem (EfiResetCold, EFI_SUCCESS, 0, NULL);

  return Status;
}

/**

  Notify function for WMI L05 Secure Boot.

  @param Event                          Event whose notification function is being invoked.
  @param Context                        Pointer to the notification function's context.

  @retval None

**/
VOID
EFIAPI
SmmVariableEventProtocolNotify (
  IN EFI_EVENT                           Event,
  IN VOID                                *Context
  )
{
  //
  // WMI secure boot synchronization
  //
  WmiL05SecureBootSync ();

  //
  // There is no necessary to trigger this notify function again in any case,
  // and this notify function will not useing Registration of RegisterProtocolNotify.
  //
  gBS->CloseEvent (Event);
  return;
}

/**

  WMI HDD password sync init.

  @param  None

  @retval None

**/
EFI_STATUS
WmiL05SecureBootSyncInit (
  VOID
  )
{
  EFI_STATUS                            Status;
  VOID                                  *Interface;
  VOID                                  *Registration;
  EFI_EVENT                             Event;

  //
  // SmmSecureBootCall() need call after gEfiSmmVariableProtocolGuid be install
  //
  Status = gBS->LocateProtocol (
                  &gEfiSmmVariableProtocolGuid,
                  NULL,
                  &Interface
                  );

  if (!EFI_ERROR (Status)) {
    Status = WmiL05SecureBootSync ();

  } else {
    //
    // Create notify event for Smm Variable Protocol
    //
    Registration = NULL;

    Status = gBS->CreateEvent (
                    EVT_NOTIFY_SIGNAL ,
                    TPL_CALLBACK,
                    SmmVariableEventProtocolNotify,
                    NULL,
                    &Event
                    );

    if (EFI_ERROR (Status)) {
      return Status;
    }

    Status = gBS->RegisterProtocolNotify (
                    &gEfiSmmVariableProtocolGuid,
                    Event,
                    &Registration
                    );
  }

  return Status;
}
