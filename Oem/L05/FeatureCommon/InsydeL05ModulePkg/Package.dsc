## @file
#  Platform Package Description file
#
#******************************************************************************
#* Copyright (c) 2013 - 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[Defines]
  !include $(PROJECT_PKG)/OemConfig.env
  !include InsydeL05ModulePkg/Package.env

[PcdsFeatureFlag]

[LibraryClasses]
  BaseOemSvcFeatureLibDefault|$(OEM_FEATURE_COMMON_PATH)/Library/BaseOemSvcFeatureLib/BaseOemSvcFeatureLibDefault.inf
  BaseOemSvcFeatureLib       |$(OEM_FEATURE_COMMON_PATH)/Library/BaseOemSvcFeatureLib/BaseOemSvcFeatureLib.inf
#  LfcFlashDeviceLib          |$(OEM_FEATURE_COMMON_PATH)/Library/BaseInsydeLfcFlashDeviceLib/BaseInsydeLfcFlashDeviceLib.inf
  FlashProtectRegionLib      |$(OEM_FEATURE_COMMON_PATH)/Library/FlashProtectRegionLib/FlashProtectRegionLib.inf
#_Start_L05_BIOS_UPDATE_UI_ENABLE_
  !if $(L05_BIOS_UPDATE_UI_ENABLE) == YES
  BiosUpdateUiLib            |$(OEM_FEATURE_COMMON_PATH)/Library/BiosUpdateUiLib/BiosUpdateUiLib.inf
  !endif
  ProgressBarLib             |$(OEM_FEATURE_COMMON_PATH)/Library/ProgressBarLib/ProgressBarLib.inf
  PrintStrToImageLib         |$(OEM_FEATURE_COMMON_PATH)/Library/PrintStrToImageLib/PrintStrToImageLib.inf
#_End_L05_BIOS_UPDATE_UI_ENABLE_
!if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861015 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x10221017 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861019 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x1022101A)
  #
  # BIOS Self-Healing Supported List: (Please refer L05ChipsetNameList.h)
  #   L05_CHIPSET_NAME_TIGERLAKE
  #   L05_CHIPSET_NAME_CEZANNE
  #   L05_CHIPSET_NAME_ALDERLAKE
  #   L05_CHIPSET_NAME_REMBRANDT
  #
  BiosSelfHealingLib         |$(OEM_FEATURE_COMMON_PATH)/Library/BiosSelfHealingLib/BiosSelfHealingLib.inf
!endif

[LibraryClasses.common.PEI_CORE]
  PeiOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/PeiOemSvcFeatureLib/PeiOemSvcFeatureLibDefault.inf
  PeiOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/PeiOemSvcFeatureLib/PeiOemSvcFeatureLib.inf

[LibraryClasses.common.PEIM]
  PeiOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/PeiOemSvcFeatureLib/PeiOemSvcFeatureLibDefault.inf
  PeiOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/PeiOemSvcFeatureLib/PeiOemSvcFeatureLib.inf

[LibraryClasses.common.DXE_CORE]
  DxeOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLibDefault.inf
  DxeOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf

[LibraryClasses.common.DXE_RUNTIME_DRIVER]
  DxeOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLibDefault.inf
  DxeOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf

[LibraryClasses.common.UEFI_DRIVER]
  DxeOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLibDefault.inf
  DxeOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf

[LibraryClasses.common.DXE_DRIVER]
  DxeOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLibDefault.inf
  DxeOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf
  OkFeatureReportLib         |$(OEM_FEATURE_COMMON_PATH)/Library/OkFeatureReportLib/OkFeatureReportLib.inf
  OemSvcSecurityPasswordLib  |$(OEM_FEATURE_COMMON_PATH)/Library/SmmDxeOemSvcSecurityPasswordLib/SmmDxeOemSvcSecurityPasswordLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  EncodeHddPasswordTableLib  |$(OEM_FEATURE_COMMON_PATH)/Library/EncodeHddPasswordTableLib/EncodeHddPasswordTableLib.inf
!endif
!if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861015 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x10221017 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861019 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x1022101A)
  #
  # BIOS Self-Healing Supported List: (Please refer L05ChipsetNameList.h)
  #   L05_CHIPSET_NAME_TIGERLAKE
  #   L05_CHIPSET_NAME_CEZANNE
  #   L05_CHIPSET_NAME_ALDERLAKE
  #   L05_CHIPSET_NAME_REMBRANDT
  #
  SmmDxeBiosSelfHealingLib   |$(OEM_FEATURE_COMMON_PATH)/Library/BiosSelfHealingLib/SmmDxeBiosSelfHealingLib.inf
!endif

[LibraryClasses.common.DXE_SMM_DRIVER]
  DxeOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLibDefault.inf
  DxeOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf
  SmmOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/SmmOemSvcFeatureLib/SmmOemSvcFeatureLibDefault.inf
  SmmOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/SmmOemSvcFeatureLib/SmmOemSvcFeatureLib.inf
#  LfcSwSmiCpuRegisterLib     |$(OEM_FEATURE_COMMON_PATH)/Library/SmmLfcSwSmiCpuRegisterLib/SmmLfcSwSmiCpuRegisterLib.inf
  OemSvcSecurityPasswordLib  |$(OEM_FEATURE_COMMON_PATH)/Library/SmmDxeOemSvcSecurityPasswordLib/SmmDxeOemSvcSecurityPasswordLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  EncodeHddPasswordTableLib  |$(OEM_FEATURE_COMMON_PATH)/Library/EncodeHddPasswordTableLib/EncodeHddPasswordTableLib.inf
!endif
!if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861015 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x10221017 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861019 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x1022101A)
  #
  # BIOS Self-Healing Supported List: (Please refer L05ChipsetNameList.h)
  #   L05_CHIPSET_NAME_TIGERLAKE
  #   L05_CHIPSET_NAME_CEZANNE
  #   L05_CHIPSET_NAME_ALDERLAKE
  #   L05_CHIPSET_NAME_REMBRANDT
  #
  SmmDxeBiosSelfHealingLib   |$(OEM_FEATURE_COMMON_PATH)/Library/BiosSelfHealingLib/SmmDxeBiosSelfHealingLib.inf
!endif

[LibraryClasses.common.COMBINED_SMM_DXE]
  DxeOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLibDefault.inf
  DxeOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf
  SmmOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/SmmOemSvcFeatureLib/SmmOemSvcFeatureLibDefault.inf
  SmmOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/SmmOemSvcFeatureLib/SmmOemSvcFeatureLib.inf
#  LfcSwSmiCpuRegisterLib     |$(OEM_FEATURE_COMMON_PATH)/Library/SmmLfcSwSmiCpuRegisterLib/SmmLfcSwSmiCpuRegisterLib.inf
  OemSvcSecurityPasswordLib  |$(OEM_FEATURE_COMMON_PATH)/Library/SmmDxeOemSvcSecurityPasswordLib/SmmDxeOemSvcSecurityPasswordLib.inf
!if $(L05_NOTEBOOK_PASSWORD_ENABLE) == YES
  EncodeHddPasswordTableLib  |$(OEM_FEATURE_COMMON_PATH)/Library/EncodeHddPasswordTableLib/EncodeHddPasswordTableLib.inf
!endif

[LibraryClasses.common.SMM_CORE]
  DxeOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLibDefault.inf
  DxeOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf
  SmmOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/SmmOemSvcFeatureLib/SmmOemSvcFeatureLibDefault.inf
  SmmOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/SmmOemSvcFeatureLib/SmmOemSvcFeatureLib.inf

[LibraryClasses.common.UEFI_APPLICATION]
  DxeOemSvcFeatureLibDefault |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLibDefault.inf
  DxeOemSvcFeatureLib        |$(OEM_FEATURE_COMMON_PATH)/Library/DxeOemSvcFeatureLib/DxeOemSvcFeatureLib.inf

[PcdsFeatureFlag]
  gL05ServicesTokenSpaceGuid.PcdL05ComputraceEnable|$(L05_COMPUTRACE_ENABLE)
!if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861011 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861015 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x10221016 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x10221017 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861018 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861019 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x1022101A)
  #
  # Gaming UI Supported List: (Please refer L05ChipsetNameList.h)
  #   L05_CHIPSET_NAME_CANNONLAKE
  #   L05_CHIPSET_NAME_TIGERLAKE
  #   L05_CHIPSET_NAME_RENOIR
  #   L05_CHIPSET_NAME_CEZANNE
  #   L05_CHIPSET_NAME_JASPERLAKE
  #   L05_CHIPSET_NAME_ALDERLAKE
  #   L05_CHIPSET_NAME_REMBRANDT
  #
  gL05ServicesTokenSpaceGuid.PcdL05GamingUiSupported|$(L05_GAMING_UI_ENABLE)
!endif

[PcdsFixedAtBuild]
#_Start_L05_HDD_SPIN_DOWN_
!if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861007 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x8086100D)
  #
  # PCH Reset Supported List: (Please refer L05ChipsetNameList.h)
  #   L05_CHIPSET_NAME_SKYLAKE
  #   L05_CHIPSET_NAME_KABYLAKE
  #
  gL05ServicesTokenSpaceGuid.PcdL05PchResetSupported   |TRUE
!else
  gL05ServicesTokenSpaceGuid.PcdL05PchResetSupported   |FALSE
!endif
#_End_L05_HDD_SPIN_DOWN_

!if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x8086100D || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861011 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861012 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861014 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861015 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861018 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861019)
  #
  # PCH Setup Supported List: (Please refer L05ChipsetNameList.h)
  #   L05_CHIPSET_NAME_KABYLAKE
  #   L05_CHIPSET_NAME_CANNONLAKE
  #   L05_CHIPSET_NAME_ICELAKE
  #   L05_CHIPSET_NAME_COMETLAKE
  #   L05_CHIPSET_NAME_TIGERLAKE
  #   L05_CHIPSET_NAME_JASPERLAKE
  #   L05_CHIPSET_NAME_ALDERLAKE
  #
  gL05ServicesTokenSpaceGuid.PcdL05PchSetupSupported   |TRUE
!else
  gL05ServicesTokenSpaceGuid.PcdL05PchSetupSupported   |FALSE
!endif

!if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x1022100F || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x10221013 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x10221016 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x10221017 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x1022101A)
  #
  # CBS Setup Supported List: (Please refer L05ChipsetNameList.h)
  #   L05_CHIPSET_NAME_RAVENRIDGE
  #   L05_CHIPSET_NAME_PICASSO
  #   L05_CHIPSET_NAME_RENOIR
  #   L05_CHIPSET_NAME_CEZANNE
  #   L05_CHIPSET_NAME_REMBRANDT
  #
  gL05ServicesTokenSpaceGuid.PcdL05AmdSetupSupported   |TRUE
!else
  gL05ServicesTokenSpaceGuid.PcdL05AmdSetupSupported   |FALSE
!endif

#[-start-210726-SHAONN0001-add]#
!if ($(C970_SUPPORT_ENABLE) == YES) OR ($(C770_SUPPORT_ENABLE) == YES)
  gL05ServicesTokenSpaceGuid.PcdL05Type03Type     |0x1F
!else
  gL05ServicesTokenSpaceGuid.PcdL05Type03Type   |0x0A
!endif
#[-end-210726-SHAONN0001-add]#

!if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861011 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861015 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861018 || \
     gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861019)
  #
  # SA GV Supported List: (Please refer L05ChipsetNameList.h)
  #   L05_CHIPSET_NAME_CANNONLAKE
  #   L05_CHIPSET_NAME_TIGERLAKE
  #   L05_CHIPSET_NAME_JASPERLAKE
  #   L05_CHIPSET_NAME_ALDERLAKE
  #
  gL05ServicesTokenSpaceGuid.PcdL05SaGvSupported      |TRUE
!else
  gL05ServicesTokenSpaceGuid.PcdL05SaGvSupported      |FALSE
!endif

!if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861019)
  #
  # Active Small Core Count Supported List: (Please refer L05ChipsetNameList.h)
  #   L05_CHIPSET_NAME_ALDERLAKE
  #
  gL05ServicesTokenSpaceGuid.PcdL05ActiveSmallCoreCountSupported |TRUE
!else
  gL05ServicesTokenSpaceGuid.PcdL05ActiveSmallCoreCountSupported |FALSE
!endif

[PcdsDynamicExDefault]
  #
  #SMBIOS - Type01
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type01ProductName  |"INVALID"                              |VOID*|0x42  #Offset 0x05
  gL05ServicesTokenSpaceGuid.PcdL05Type01Version      |"INVALID"                              |VOID*|0x42  #Offset 0x06
  gL05ServicesTokenSpaceGuid.PcdL05Type01SerialNumber |"INVALID"                              |VOID*|0x22  #Offset 0x07
  gL05ServicesTokenSpaceGuid.PcdL05Type01UUID         |"INVALID"                              |VOID*|0x12  #Offset 0x08
  gL05ServicesTokenSpaceGuid.PcdL05Type01SKUNumber    |"LENOVO_BI_IDEAPADXX_BU_idea_FM_YYYY"  |VOID*|0x42  #Offset 0x19
  gL05ServicesTokenSpaceGuid.PcdL05Type01Family       |"IDEAPAD"                              |VOID*|0x42  #Offset 0x1A
  #
  #SMBIOS - Type02
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type02Product      |"INVALID"              |VOID*|0x42  #Offset 0x05
  gL05ServicesTokenSpaceGuid.PcdL05Type02Version      |"No DPK"               |VOID*|0x42  #Offset 0x06
  gL05ServicesTokenSpaceGuid.PcdL05Type02SerialNumber |"INVALID"              |VOID*|0x22  #Offset 0x07
  gL05ServicesTokenSpaceGuid.PcdL05Type02AssetTag     |"NO Asset Tag"         |VOID*|0x12  #Offset 0x08
  gL05ServicesTokenSpaceGuid.PcdL05OSLienceDescriptor |"Not Defined"          |VOID*|0x22  #Offset 0x06
  #
  #SMBIOS - Type03
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type03Version      |"INVALID"              |VOID*|0x42  #Offset 0x06
  gL05ServicesTokenSpaceGuid.PcdL05Type03SerialNumber |"INVALID"              |VOID*|0x22  #Offset 0x07
  gL05ServicesTokenSpaceGuid.PcdL05Type03AssetTag     |"NO Asset Tag"         |VOID*|0x12  #Offset 0x08
  #
  #SMBIOS - Type200
  #
  gL05ServicesTokenSpaceGuid.PcdL05Type200MTM         |"INVALID"              |VOID*|0x42   #Offset 0x05

  #
  # Lenovo Notebook Password Design V1.1
  #
  gL05ServicesTokenSpaceGuid.PcdL05NotebookPasswordDesignDialogF1String|L""   |VOID*|150
  gL05ServicesTokenSpaceGuid.PcdL05HddPaswordTitleString|L""                  |VOID*|0x60

  #
  # BIOS Update UI
  #
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringEn1|L""                  |VOID*|0x80
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringEn2|L""                  |VOID*|0x80
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiStringZh |L""                  |VOID*|0x80
  gL05ServicesTokenSpaceGuid.PcdL05BiosUpdateUiErrorCodeString |L""           |VOID*|0x80

[Components]

[Components.$(PEI_ARCH)]
!if $(L05_ALL_FEATURE_ENABLE) == YES

  !if $(L05_BIOS_SELF_HEALING_SUPPORT) == YES
  !if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861015 || \
       gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x10221017 || \
       gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861019 || \
       gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x1022101A)
    #
    # BIOS Self-Healing Supported List: (Please refer L05ChipsetNameList.h)
    #   L05_CHIPSET_NAME_TIGERLAKE
    #   L05_CHIPSET_NAME_CEZANNE
    #   L05_CHIPSET_NAME_ALDERLAKE
    #   L05_CHIPSET_NAME_REMBRANDT
    #
    $(OEM_FEATURE_COMMON_PATH)/BiosSelfHealingPei/BiosSelfHealingPei.inf
  !endif
  !endif

!endif

[Components.$(DXE_ARCH)]
!if $(L05_ALL_FEATURE_ENABLE) == YES

  $(OEM_FEATURE_COMMON_PATH)/SwSmiInterfaceCoreSmm/SwSmiInterfaceCoreSmm.inf

  !if $(OK_FEATURE_REPORT_ENABLE) == YES
    $(OEM_FEATURE_COMMON_PATH)/OkFeatureReportInterfaceCoreDxe/OkFeatureReportInterfaceCoreDxe.inf
  !endif

  #_Start_L05_VARIABLE_RUNTIME_PROTECTION_
    $(OEM_FEATURE_COMMON_PATH)/VariableRuntimeProtectionDxe/VariableRuntimeProtectionDxe.inf
  #_End_L05_VARIABLE_RUNTIME_PROTECTION_

  #_Start_L05_SMBIOS_
    $(OEM_FEATURE_COMMON_PATH)/SmbiosOverride/SmbiosOverride.inf
  #_End_L05_SMBIOS_

  #_Start_L05_WIRELESS_DEVICE_SUPPORT_
    $(OEM_FEATURE_COMMON_PATH)/WirelessDeviceSupportDxe/WirelessDeviceSupportDxe.inf
  #_End_L05_WIRELESS_DEVICE_SUPPORT_

  !if $(L05_PASSWORD_RESET_CRISIS_BIOS_ENABLE) == YES
    $(OEM_FEATURE_COMMON_PATH)/SystemPasswordsResetCrisisBios/SystemPasswordsResetDriver/SystemPasswordsReset.inf
  !endif

  #_Start_L05_ONE_KEY_RECOVERY_ENABLE_
    $(OEM_FEATURE_COMMON_PATH)/ServiceBody/L05DxeServiceBody.inf
    $(OEM_FEATURE_COMMON_PATH)/OneKeyRecovery/Efi/OneKeyRecovery.inf
  #_End_L05_ONE_KEY_RECOVERY_ENABLE_

  #_Start_L05_SETUP_MENU_
    $(OEM_FEATURE_COMMON_PATH)/SetupMenuService/SetupMenuService.inf
    $(OEM_FEATURE_COMMON_PATH)/SecureBootService/SecureBootService.inf
  #_End_L05_SETUP_MENU_

  #_Start_L05_ALL_FEATURE_
    $(OEM_FEATURE_COMMON_PATH)/BootOptionService/BootOptionService.inf
  #_End_L05_ALL_FEATURE_

  !if $(L05_ACPI_TABLE_ID_ENABLE) == YES
    $(OEM_FEATURE_COMMON_PATH)/AcpiOverride/AcpiOverride.inf
  !endif

  #_Start_L05_SLP_STRING_
    $(OEM_FEATURE_COMMON_PATH)/LenovoStringService/LenovoStringService.inf
  #_End_L05_SLP_STRING_

  #_Start_L05_ACPI_SLP_20_
    $(OEM_FEATURE_COMMON_PATH)/Slp20ServiceDxe/Slp20ServiceDxe.inf
    $(OEM_FEATURE_COMMON_PATH)/Slp20Service/Smm/L05UpdateSlp20/L05UpdateSlp20.inf
  #_End_L05_ACPI_SLP_20_

  #_Start_L05_CONFIGURATION_VARIABLE_
    $(OEM_FEATURE_COMMON_PATH)/ConfigurationVariable/ConfigurationVariable.inf
  #_End_L05_CONFIGURATION_VARIABLE_

  !if $(L05_COMPUTRACE_ENABLE) == YES
    $(OEM_FEATURE_COMMON_PATH)/Computrace/Dxe/L05Computrace.inf
    $(OEM_FEATURE_COMMON_PATH)/Computrace/Smm/L05SmmComputrace.inf
    $(OEM_FEATURE_COMMON_PATH)/Computrace/EraseCptSmm/EraseCptSmm.inf
  !endif

  #_Start_L05_LEGACY_TO_UEFI_
    $(OEM_FEATURE_COMMON_PATH)/LegacyToEfiDxe/LegacyToEfiDxe.inf
    $(OEM_FEATURE_COMMON_PATH)/LegacyToEfiSmm/LegacyToEfiSmm.inf
  #_End_L05_LEGACY_TO_UEFI_

#[-start-210623-YUNLEI0104-remove]
!if $(LCFC_SUPPORT_ENABLE) != YES
  !if $(L05_SPECIFIC_VARIABLE_SERVICE_ENABLE) == YES
    $(OEM_FEATURE_COMMON_PATH)/SpecificVariableServiceSmm/SpecificVariableServiceSmm.inf
    $(OEM_FEATURE_COMMON_PATH)/SpecificVariableServiceDxe/SpecificVariableServiceDxe.inf
  !else
  #_Start_L05_VARIABLE_SERVICE_
    $(OEM_FEATURE_COMMON_PATH)/VariableServiceSmm/VariableServiceSmm.inf
    $(OEM_FEATURE_COMMON_PATH)/VariableServiceDxe/VariableServiceDxe.inf
  #_End_L05_VARIABLE_SERVICE_
  !endif
!endif
#[-end-210623-YUNLEI0104-remove]
  #_Start_L05_DRIVE_IDENTIFICATION_
    $(OEM_FEATURE_COMMON_PATH)/UefiDriveIdentification/UefiDriveIdentification.inf
  #_End_L05_DRIVE_IDENTIFICATION_

  #_Start_L05_HDD_SPIN_DOWN_
    $(OEM_FEATURE_COMMON_PATH)/HddSpinDownSmm/HddSpinDownSmm.inf
  !if gL05ServicesTokenSpaceGuid.PcdL05PchResetSupported
    $(OEM_FEATURE_COMMON_PATH)/HddSpinDownDxe/HddSpinDownDxePchReset.inf
  !else
    $(OEM_FEATURE_COMMON_PATH)/HddSpinDownDxe/HddSpinDownDxe.inf
  !endif
  #_End_L05_HDD_SPIN_DOWN_

  !if $(L05_SETUP_UNDER_OS_SUPPORT) == YES
    $(OEM_FEATURE_COMMON_PATH)/WmiSetupUnderOsDxe/WmiSetupUnderOsDxe.inf
    $(OEM_FEATURE_COMMON_PATH)/WmiSetupUnderOsSmm/WmiSetupUnderOsSmm.inf
  !endif

  !if $(L05_BIOS_POST_LOGO_DIY_SUPPORT) == YES
    $(OEM_FEATURE_COMMON_PATH)/BiosPostLogoDiyDxe/BiosPostLogoDiyDxe.inf
  !endif
#[-start-210618-YUNLEI0103-modify]

!if $(LCFC_SUPPORT_ENABLE) != YES
  !if $(L05_MODERN_PRELOAD_SUPPORT) == YES
    $(OEM_FEATURE_COMMON_PATH)/ModernPreloadDxe/ModernPreloadDxe.inf
    $(OEM_FEATURE_COMMON_PATH)/ModernPreloadSmm/ModernPreloadSmm.inf
  !endif
!endif
#[-end-210618-YUNLEI0103-modify]

  !if $(L05_ONE_KEY_BATTERY_ENABLE) == YES
    $(OEM_FEATURE_COMMON_PATH)/OneKeyBatteryDxe/OneKeyBatteryDxe.inf
  !endif

  !if $(L05_SECURE_SUITE_SUPPORT) == YES
    $(OEM_FEATURE_COMMON_PATH)/EfiSystemTools/SecureSuiteApp/$(L05_SECURE_SUITE_VERSION)/SecureSuiteApp.inf
  !endif

  !if $(L05_DIAGNOSTICS_SUPPORT) == YES
    $(OEM_FEATURE_COMMON_PATH)/EfiSystemTools/UefiDiagnosticsApp/$(L05_DIAGS_EMBEDDED_VERSION)/UefiDiagnosticsApp.inf
  !endif

  !if $(L05_BIOS_SELF_HEALING_SUPPORT) == YES
  !if (gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861015 || \
       gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x10221017 || \
       gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x80861019 || \
       gL05ServicesTokenSpaceGuid.PcdL05ChipsetName == 0x1022101A)
    #
    # BIOS Self-Healing Supported List: (Please refer L05ChipsetNameList.h)
    #   L05_CHIPSET_NAME_TIGERLAKE
    #   L05_CHIPSET_NAME_CEZANNE
    #   L05_CHIPSET_NAME_ALDERLAKE  
    #   L05_CHIPSET_NAME_REMBRANDT
    #
    $(OEM_FEATURE_COMMON_PATH)/BiosSelfHealingDxe/BiosSelfHealingDxe.inf
    $(OEM_FEATURE_COMMON_PATH)/BiosSelfHealingSmm/BiosSelfHealingSmm.inf
  !endif
  !endif

  !if $(L05_MAC_ADDRESS_PASS_THROUGH_ENABLE) == YES
    $(OEM_FEATURE_COMMON_PATH)/MacAddressPassThroughDxe/MacAddressPassThroughDxe.inf
  !endif
!endif

  !if $(L05_SMB_BIOS_ENABLE) == YES
#_Start_L05_WMI_ASSET_TAG_SUPPORT_
    $(OEM_FEATURE_COMMON_PATH)/WmiAssetTagSmm/WmiAssetTagSmm.inf
#_End_L05_WMI_ASSET_TAG_SUPPORT_
  !endif

  !if $(L05_NOTEBOOK_CLOUD_BOOT_ENABLE) == YES and $(L05_NOTEBOOK_CLOUD_BOOT_WIFI_ENABLE) == YES
    $(OEM_FEATURE_COMMON_PATH)/CloudBoot/CloudBootDxe/CloudBootDxe.inf
  !endif
