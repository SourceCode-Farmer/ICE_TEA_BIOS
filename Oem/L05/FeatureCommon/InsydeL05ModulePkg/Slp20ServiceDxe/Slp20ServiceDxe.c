/** @file
  Provide DXE SLP 2.0 Service Protocol for L05 Feature

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "Slp20ServiceDxe.h"

/**
  Check SLP 2.0 Marker is valid or not

  @param  Table                         A pointer to the ACPI SLIC Table

  @retval TRUE                          SLP 2.0 Marker is valid
  @retval FALSE                         SLP 2.0 Marker is not valid
**/
BOOLEAN
IsValidMarker (
  IN VOID                               *Table
  )
{
  EFI_STATUS                            Status;
  BOOLEAN                               InstallSlicTable;

  InstallSlicTable = FALSE;

  if ((((EFI_ACPI_SOFTWARE_LICENSING_TABLE *) Table)->SlpMarker.Type   != EFI_L05_SLP20_MARKER_TYPE)              || // SLP 20 marker type
      (((EFI_ACPI_SOFTWARE_LICENSING_TABLE *) Table)->SlpMarker.Length != sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE))) {

    //
    // For Project to decide install SLIC Table or not when no valid Marker in SLIC Table.
    //
    Status = OemSvcInstallSlicTable (&InstallSlicTable, Table);

    switch (Status) {

    case EFI_MEDIA_CHANGED:
      if (!InstallSlicTable) {
        return FALSE;
      }

      break;

    case EFI_UNSUPPORTED:
    default:
      //
      // By L05 OA Requirement 009, Section 1.1
      // Feature must not install SLIC Table if no valid Marker injected in BIOS.
      //
      return FALSE;
      break;
    }
  }

  return TRUE;
}

/**
  This is the declaration of an EFI image entry point. This entry point is
  the same for UEFI Applications, UEFI OS Loaders, and UEFI Drivers including
  both device drivers and bus drivers.

  @param  ImageHandle                   The firmware allocated handle for the UEFI image.
  @param  SystemTable                   A pointer to the EFI System Table.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
Slp20ServiceDxeDriverEntryPoint (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  EFI_L05_DXE_SLP_20_PROTOCOL           *L05Slp20Ptr;
  EFI_HANDLE                            Handle;

  Status      = EFI_SUCCESS;
  L05Slp20Ptr = NULL;
  Handle      = NULL;

  L05Slp20Ptr = AllocateZeroPool (sizeof (EFI_L05_DXE_SLP_20_PROTOCOL));

  if (L05Slp20Ptr == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  L05Slp20Ptr->IsValidMarker = IsValidMarker;

  Status = gBS->InstallProtocolInterface (
                  &Handle,
                  &gEfiL05DxeSlp20ProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  L05Slp20Ptr
                  );

  return Status;
}

