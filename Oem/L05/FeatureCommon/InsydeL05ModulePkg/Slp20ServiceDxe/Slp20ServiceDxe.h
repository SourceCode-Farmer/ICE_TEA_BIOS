/** @file
  Provide DXE SLP 2.0 Service Protocol for L05 Feature

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SLP_20_SERVICE_DXE_H_
#define _SLP_20_SERVICE_DXE_H_

#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/FeatureLib/OemSvcInstallSlicTable.h>

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//

//
// Produced Protocols
//
#include <Protocol/L05DxeSlp20.h>

//
// Guids
//

//
// Driver Version
//

//
// Protocol instances
//

//
// Include files with function prototypes
//
#include <IndustryStandard/SLP2_0.h>  // EFI_ACPI_SLP_MARKER_STRUCTURE
#include <L05Slp20Config.h>

#endif
