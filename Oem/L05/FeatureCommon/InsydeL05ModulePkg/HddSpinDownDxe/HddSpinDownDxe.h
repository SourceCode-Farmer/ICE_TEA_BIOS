/** @file

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _L05_HDD_SPIN_DOWN_DXE_H_
#define _L05_HDD_SPIN_DOWN_DXE_H_

#include <OemSwSmi.h>  // EFI_L05_HDD_SPIN_DOWN_CALLBACK
#include <Guid/EventGroup.h>  // gEfiEventVirtualAddressChangeGuid
#include <Library/BaseLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PrintLib.h>
#include <Library/DebugLib.h>
#include <Library/H2OCpLib.h>
#include <Protocol/SmmControl2.h>
#if (FixedPcdGetBool (PcdL05PchResetSupported))
#include <Protocol/PchReset.h>
#endif
#include <Protocol/VariableDefaultUpdate.h>
#include <Protocol/L05HddSpindown.h>

#endif
