/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_MODERN_PRELOAD_DXE_H_
#define _L05_MODERN_PRELOAD_DXE_H_

#include <L05ModernPreloadSupportConfig.h>
#include <L05Config.h>
#include <SetupConfig.h>  // SYSTEM_CONFIGURATION
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>  // EfiCreateProtocolNotifyEvent
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PrintLib.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/FlashRegionLib.h>

#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
#include <Protocol/L05Variable.h>
#endif

#endif
