/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _MAC_ADDRESS_PASS_THROUGH_H_
#define _MAC_ADDRESS_PASS_THROUGH_H_

#include <Uefi.h>
#include <SetupConfig.h>
#include <L05MacAddressPassThroughVariable.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/DevicePathLib.h>
#include <Library/DebugLib.h>
#include <Library/VariableLib.h>
#include <Library/H2OCpLib.h>
#include <Library/PrintLib.h>
#include <Library/FeatureLib/OemSvcUpdateSecondMacAddress.h>
//
// Consumed Protocols
//
#include <Protocol/DevicePath.h>
#include <Protocol/AcpiSupport.h>
#include <Protocol/SimpleNetwork.h>

//
// GUID
//
#include <Guid/H2OCp.h>

#define MACA_NAME_STRING                "_AUXMAC_#%02x%02x%02x%02x%02x%02x#"

#endif
