## @file
#
#******************************************************************************
#* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##

[Defines]
  INF_VERSION               = 0x00010005
  BASE_NAME                 = MacAddressPassThroughDxe
  FILE_GUID                 = 1421d662-67bf-4a23-8b32-607e1c9e3aee
  MODULE_TYPE               = DXE_DRIVER
  VERSION_STRING            = 1.0
  ENTRY_POINT               = MacAddressPassThroughDxeEntryPoint

[Packages]
  $(PROJECT_PKG)/Project.dec
  $(OEM_FEATURE_COMMON_PATH)/$(OEM_FEATURE_COMMON_PATH).dec
  $(OEM_FEATURE_OVERRIDE_CORE_CODE)/$(OEM_FEATURE_OVERRIDE_CORE_CODE).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec  ## ChipsetSetupConfig.h
!if $(L05_CHIPSET_VENDOR) == INTEL
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec  ## PchLimits.h
!endif
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeNetworkPkg/InsydeNetworkPkg.dec

[Sources]
  MacAddressPassThroughDxe.c
  MacAddressPassThroughDxe.h

[LibraryClasses]
  UefiDriverEntryPoint
  UefiBootServicesTableLib
  MemoryAllocationLib
  BaseLib
  UefiLib
  DebugLib
  H2OCpLib
  DevicePathLib
  VariableLib
  PrintLib
  DxeOemSvcFeatureLibDefault

[Protocols]
  gEfiDevicePathProtocolGuid
  gEfiSimpleNetworkProtocolGuid
  gEfiAcpiSupportProtocolGuid

[Guids]
  gH2OBdsCpNetworkUpdateMacAddrGuid
  gH2OBdsCpConnectAllAfterGuid
  gH2OBdsCpReadyToBootBeforeGuid
  gSystemConfigurationGuid

[Pcd]
  gInsydeTokenSpaceGuid.PcdH2OBdsCpNetworkUpdateMacAddrSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpConnectAllAfterSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpReadyToBootBeforeSupported

[Depex]
  TRUE

[BuildOptions]
!if $(INSYDE_DEBUGGER) == YES
  *_*_*_CC_FLAGS = /Od
!endif
