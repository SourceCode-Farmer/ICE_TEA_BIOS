//;******************************************************************************
/** @file
  This driver is for processing the Legacy to Efi Callback Functions

;******************************************************************************
;* Copyright (c) 2012 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _LEGACY_TO_EFI_DXE_H_
#define _LEGACY_TO_EFI_DXE_H_

#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/IoLib.h>
#include <Library/HobLib.h>
#include <Library/FeatureLib/OemSvcStopLegacyToEfiProcess.h>
#include <Library/H2OCpLib.h>

//
// Consumed Protocols
//
#include <Protocol/LegacyBios.h>
#include <Protocol/SmmCommunication.h>

//
// Produced Protocols
//

//
// Guids
//
#include <Guid/L05StopLegacyToEfiProcess.h>
#include <Guid/DebugMask.h>  // gEfiGenericVariableGuid
#include <Guid/GlobalVariable.h>

//
// Size of SMM communicate header, without including the payload
//
#define SMM_COMMUNICATE_HEADER_SIZE  (OFFSET_OF (EFI_SMM_COMMUNICATE_HEADER, Data))

#endif
