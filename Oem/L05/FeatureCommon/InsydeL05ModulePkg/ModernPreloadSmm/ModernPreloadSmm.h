/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_MODERN_PRELOAD_SMM_H_
#define _L05_MODERN_PRELOAD_SMM_H_

#include <L05ModernPreloadSupportConfig.h>
#include <L05Config.h>
#include <OemSwSmi.h>  // L05_SECURITY_SW_SMI
#include <TpmPolicy.h>  // SKIP_TPM_REVOKE_TRUST
#include <SetupConfig.h>  // SYSTEM_CONFIGURATION
#include <Library/SmmServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DevicePathLib.h>
#include <Library/PrintLib.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/SmmCpu.h>
#include <Protocol/SmmFwBlockService.h>
#include <Protocol/SmmVariable.h>
#include <Protocol/L05SmmSwSmiInterface.h>

#ifdef L05_SPECIFIC_VARIABLE_SERVICE_ENABLE
#include <Protocol/L05Variable.h>
#endif

#endif
