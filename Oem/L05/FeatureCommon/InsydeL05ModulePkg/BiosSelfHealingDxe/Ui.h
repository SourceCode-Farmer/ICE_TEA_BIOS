/** @file
  BIOS Self-Healing UI function.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BIOS_SELF_HEALING_UI_H_
#define _BIOS_SELF_HEALING_UI_H_

#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/OemGraphicsLib.h>
#include <Library/PrintLib.h>
#include <Library/HiiLib.h>
#include <Library/PrintStrToImageLib.h>
#include <Library/ProgressBarLib.h>
#include <Library/BiosUpdateUiLib.h>

//
// Consumed Protocols
//
#include <Protocol/GraphicsOutput.h>
#include <Protocol/TgaDecoder.h>

//
// Guids
//

//
// Definitions
//
#define PROGRESS_BOARD                      1
#define PROGRESS_BAR                        2
#define PROCESS_ACCURACY_VALUE              100
#define SELF_HEALING_TARGET_COLUMN          100
#define SELF_HEALING_TARGET_ROW             31
#define SELF_HEALING_RECOVERY_DONE_STRING   L"System has self-healed by restoring BIOS from backup.\n\rPlease check BIOS version and then update BIOS if needed.\n\rPress Esc key to hide this message."

//
// Include files with function prototypes
//
EFI_STATUS
EFIAPI
DrawProgressBar (
  IN  UINTN                             Completion
  );

EFI_STATUS
DisplayBootLogo (
  VOID
  );

EFI_STATUS
DisplayMessage (
  IN  L05_BIOS_SELF_HEALING_UI_TYPE     UiType,
  IN  BOOLEAN                           DisableVendorUi
  );

EFI_STATUS
SetResolutionToTarget (
  IN  BOOLEAN                           Restore
  );

UINTN
PrintAt (
  IN  UINTN                             Column,
  IN  UINTN                             Row,
  IN  CHAR16                            *Fmt,
  ...
  );

#endif
