/** @file
  BIOS Self-Healing DXE Driver.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BIOS_SELF_HEALING_DXE_H_
#define _BIOS_SELF_HEALING_DXE_H_

#include <Uefi.h>
#include <OemCmos.h>
#include <L05Config.h>
#include <L05BiosSelfHealingConfig.h>
#include <L05ChipsetNameList.h>

//
// Libraries
//
#include <Library/UefiLib.h>  // EfiCreateProtocolNotifyEvent
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/CmosLib.h>
#include <Library/BvdtLib.h>
#include <Library/H2OCpLib.h>
#include <Library/VariableLib.h>
#include <Library/FdSupportLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/FlashProtectRegionLib.h>
#include <Library/BiosSelfHealingLib.h>
#include <Library/SmmDxeBiosSelfHealingLib.h>
#include <Library/FeatureLib/OemSvcNotifyEcToDisableTopSwap.h>
#include <Library/FeatureLib/OemSvcNotifyEcToSetWdtFunction.h>
#include <Library/FeatureLib/OemSvcNotifyEcToSetWdtFlag.h>

//
// Consumed Protocols
//
#include <Protocol/SmmControl2.h>
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/L05BiosSelfHealing.h>

//
// Guids
//
#include <Guid/FileInfo.h>

//
// Include files with function prototypes
//
#include "Ui.h"

#define FLASH_SECTOR_SIZE                         0x1000
#define FLASH_FAILURE_RETRY_COUNT                 5

EFI_STATUS
EFIAPI
BiosSelfHealingDisplayMessage (
  IN  L05_BIOS_SELF_HEALING_UI_TYPE     UiType,
  IN  BOOLEAN                           DisableVendorUi,
  IN  BOOLEAN                           DisplayLogo
  );

EFI_STATUS
EFIAPI
BiosSelfHealingProgressBar (
  IN  UINTN                             Completion
  );

#endif
