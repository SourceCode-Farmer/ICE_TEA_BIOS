/** @file

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _L05_UPDATE_SLP20_H_
#define _L05_UPDATE_SLP20_H_

#include <Uefi.h>
#include <L05Config.h>
#include <L05Slp20Config.h>

//
// Libraries
//
#include <Library/SmmServicesTableLib.h>

#include <Library/BaseMemoryLib.h>
#include <Library/CmosLib.h>
#include <Library/DebugLib.h>

#include <Library/FeatureLib/OemSvcGetSlp20ManufactureInfo.h>
#include <Library/FeatureLib/OemSvcGetSlp20Marker.h>
#include <Library/FeatureLib/OemSvcGetSlp20RandomNumber.h>

#include <Library/FeatureLib/OemSvcSetSlp20ManufactureInfo.h>
#include <Library/FeatureLib/OemSvcSetSlp20Marker.h>
#include <Library/FeatureLib/OemSvcSetSlp20RandomNumber.h>

//
// Protocol instances
//
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/SmmCpu.h>

//
// Include files with function prototypes
//
#include <OemSwSmi.h>
#include <IndustryStandard/SLP2_0.h>  // EFI_ACPI_SLP_MARKER_STRUCTURE

#define EFI_L05_UPDATE_SLP20_SIGNATURE       'SM'
#define OEM_SLP20_SAVE_MARKER_FUNCTION       ((EFI_L05_UPDATE_SLP20_SIGNATURE << 16) + (0xEF + 0x80)) // It can not be changed. (L05 Spec. defined)
  #define EFI_L05_SAVE_MARKER_SUCCESS          0
  #define EFI_L05_SAVE_MARKER_NOT_COMPLETED    1
  #define EFI_L05_SAVE_MARKER_BUFFER_TOO_SMALL 2
#define OEM_SLP20_RETRIEVE_MARKER_FUNCTION   ((EFI_L05_UPDATE_SLP20_SIGNATURE << 16) + (0xEF + 0x81)) // It can not be changed. (L05 Spec. defined)
  #define EFI_L05_RETRIEVE_SUCCESS             0
  #define EFI_L05_RETRIEVE_NOT_COMPLETED       1
#define OEM_SLP20_VALIDATE_MARKER_FUNCTION   ((EFI_L05_UPDATE_SLP20_SIGNATURE << 16) + (0xEF + 0x82)) // It can not be changed. (L05 Spec. defined)
  #define EFI_L05_VALIDATE_SUCCESS             0
  #define EFI_L05_VALIDATE_NOT_COMPLETED       1
#define EFI_L05_UPDATE_SLP20_SET_MFG_DONE_FUNCTION ((EFI_L05_UPDATE_SLP20_SIGNATURE << 16) + (0xEF + 0x83)) // It can be changed.
  #define EFI_L05_SET_MFG_DONE_SUCCESS         0
  #define EFI_L05_SET_MFG_DONE_NOT_COMPLETED   1

#define EFI_L05_SLP20_MARKER_WINDOWS_FLAG      SIGNATURE_64 ('W','I','N','D','O','W','S',' ')

#define EFI_L05_SLP20_MFG_STATE_MASK           3

#endif
