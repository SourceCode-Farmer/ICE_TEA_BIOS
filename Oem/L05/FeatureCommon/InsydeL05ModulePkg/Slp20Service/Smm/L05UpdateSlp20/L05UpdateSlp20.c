/** @file

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "L05UpdateSlp20.h"

EFI_SMM_CPU_PROTOCOL                    *mSmmCpu;

EFI_STATUS
ReadDwordRegister (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             Width,
  IN  UINT8                             CpuNum,
  OUT VOID                              *RegisterData
  )
{
  EFI_STATUS                            Status;

  Status = mSmmCpu->ReadSaveState (
                      mSmmCpu,
                      Width,
                      RegisterNum,
                      CpuNum,
                      RegisterData
                      );
  return Status;
}

EFI_STATUS
WriteDwordRegister (
  IN  EFI_SMM_SAVE_STATE_REGISTER       RegisterNum,
  IN  UINTN                             Width,
  IN  UINT8                             CpuNum,
  IN  VOID                              *RegisterData
  )
{
  EFI_STATUS                            Status;

  Status = mSmmCpu->WriteSaveState (
                      mSmmCpu,
                      Width,
                      RegisterNum,
                      CpuNum,
                      RegisterData
                      );
  return Status;
}

EFI_STATUS
L05FoundTriggerCpu (
  IN     UINTN                          SlpFuncNum,
  IN OUT UINT8                          *CpuNum
  )
{
  EFI_STATUS                            Status;
  UINT8                                 Index;
  UINT16                                SmiPort;
  UINT32                                Eax;

  Status  = EFI_NOT_FOUND;
  Eax     = 0;
  SmiPort = PcdGet16 (PcdSoftwareSmiPort);

  for (Index = 0; Index < gSmst->NumberOfCpus; Index++) {

    ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT16), Index, &Eax);

    if (Eax  == (SlpFuncNum & 0xffff)) {
      break;
    }
  }

  *CpuNum = Index;

  if (*CpuNum < gSmst->NumberOfCpus) {
    Status = EFI_SUCCESS;
  }

  return Status;
}

BOOLEAN
IsValidMarker (
  IN  UINT8                             *Marker,
  IN  UINTN                             MarkerSize
  )
{
  EFI_ACPI_SLP_MARKER_STRUCTURE         *SlpMarker;
  BOOLEAN                               AllZero;
  UINTN                                 Index;

  if (MarkerSize != sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE)) {
    return FALSE;
  }

  AllZero = TRUE;

  for (Index = 0 ; Index < sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE) ; Index++) {
    if (Marker[Index] != 0x00) {
      AllZero = FALSE;
      break;
    }
  }

  if (AllZero) {
    return TRUE;
  }

  SlpMarker = (EFI_ACPI_SLP_MARKER_STRUCTURE *) (VOID *) Marker;

  if (SlpMarker->Type != EFI_L05_SLP20_MARKER_TYPE) {
    return FALSE;
  }

  if (SlpMarker->Length != sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE)) {
    return FALSE;
  }

  if (SlpMarker->sWindowsFlag != EFI_L05_SLP20_MARKER_WINDOWS_FLAG) {
    return FALSE;
  }

#ifdef L05_ACPI_TABLE_ID_ENABLE
  if ((*(UINT64 *) (SlpMarker->sOemid) & EFI_L05_ACPI_OEM_ID_MASK) != (EFI_L05_ACPI_OEM_ID & EFI_L05_ACPI_OEM_ID_MASK)) {
    return FALSE;
  }

  if ((SlpMarker->sOemTable & EFI_L05_ACPI_OEM_TABLE_ID_MASK) != (EFI_L05_ACPI_OEM_TABLE_ID & EFI_L05_ACPI_OEM_TABLE_ID_MASK)) {
    return FALSE;
  }
#endif

  return TRUE;

}

EFI_STATUS
ProcessManufactureInfo (
  IN OUT EFI_L05_SLP20_MFG_STATE        *EfiL05ManufactureState
  )
{
  EFI_STATUS                            Status;

  Status = EFI_NOT_FOUND;

  OemSvcGetSlp20ManufactureInfo (EfiL05ManufactureState);

  if (EFI_ERROR (Status) ||
      (((*EfiL05ManufactureState) & EFI_L05_SLP20_MFG_STATE_MASK) < EFI_L05_SLP20_MFG_DONE) ||
      (*EfiL05ManufactureState == EFI_L05_SLP20_MFG_MAX_STATE)) {

    *EfiL05ManufactureState = EFI_L05_SLP20_MFG_NOT_DONE;
    Status = OemSvcSetSlp20ManufactureInfo (EfiL05ManufactureState);
  }

  *EfiL05ManufactureState &= EFI_L05_SLP20_MFG_STATE_MASK;

  return Status;
}

EFI_STATUS
ProcessMarkerkey (
  IN OUT EFI_ACPI_SLP_MARKER_STRUCTURE  *EfiL05Marker
  )
{
  EFI_STATUS                            Status;

  Status = EFI_NOT_FOUND;

  Status = OemSvcGetSlp20Marker (EfiL05Marker);

  if (EFI_ERROR (Status) || (EfiL05Marker->Type == EFI_L05_SLP_MARKER_INVALID)) {

    SetMem (EfiL05Marker, sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE), 0);
    Status = OemSvcSetSlp20Marker (EfiL05Marker);
  }

  return Status;
}

EFI_STATUS
ProcessRandomNumber (
  IN OUT EFI_L05_SLP20_RANDOM_INFO      *EfiL05Slp20RandomInfo
  )
{
  EFI_STATUS                            Status;
  UINT8                                 RandomNumber;

  RandomNumber = 0;

  Status = OemSvcGetSlp20RandomNumber (EfiL05Slp20RandomInfo);

  if (EFI_ERROR (Status) ||
      (EfiL05Slp20RandomInfo->RandomNum0 == 0) ||
      (EfiL05Slp20RandomInfo->RandomNum0 == 0xFF)
      ) {

    SetMem (EfiL05Slp20RandomInfo, sizeof (EFI_L05_SLP20_RANDOM_INFO), 0);

    do {
      RandomNumber = ReadCmos8 (0x80);
    } while (RandomNumber == 0 || RandomNumber == 0xFF);

    EfiL05Slp20RandomInfo->RandomNum0 = RandomNumber;
    Status = OemSvcSetSlp20RandomNumber (EfiL05Slp20RandomInfo);
  }

  return Status;
}

STATIC
EFI_STATUS
OemSlp20SaveMarkerCallback (
  IN EFI_HANDLE                         DispatchHandle,
  IN CONST VOID                         *Context         OPTIONAL,
  IN OUT VOID                           *CommBuffer      OPTIONAL,
  IN OUT UINTN                          *CommBufferSize  OPTIONAL
  )
{
  EFI_STATUS                            Status;
  UINT32                                RegisterBuf;
  UINTN                                 Index;
  UINTN                                 MarkerSize;
  UINTN                                 MergeBufferSize;
  UINT8                                 *Marker;
  UINT8                                 *MergeBuffer;
  UINT8                                 *TempMergeBuffer;
  EFI_L05_SLP20_MFG_STATE               EfiL05Slp20MfgState;
  BOOLEAN                               AllZeroMarkerInFlash;
  EFI_L05_SLP20_RANDOM_INFO             EfiL05Slp20RandomInfo;
  UINT8                                 CpuNum;
  UINTN                                 SlpFuncNum;

  CpuNum          = 0;
  SlpFuncNum      = OEM_SLP20_SAVE_MARKER_FUNCTION;
  RegisterBuf     = 0;
  Marker          = NULL;
  MarkerSize      = 0;
  MergeBuffer     = NULL;
  TempMergeBuffer = NULL;
  MergeBufferSize = 0;

  Status = L05FoundTriggerCpu (SlpFuncNum, &CpuNum);

  if (EFI_ERROR (Status)) {
    return EFI_SUCCESS;
  }

  Status = ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);

  if (!EFI_ERROR (Status) && (RegisterBuf == OEM_SLP20_SAVE_MARKER_FUNCTION)) {
    //
    //  Set input parameter as spec defined
    //
    ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RSI, sizeof (UINT32), CpuNum, &RegisterBuf);
    Marker = (UINT8 *) (UINTN) RegisterBuf;
    ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RCX, sizeof (UINT32), CpuNum, &MarkerSize);

    //
    // By L05 Spec., utility will prepare buffer for used.
    //
    ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RDI, sizeof (UINT32), CpuNum, &RegisterBuf);
    MergeBuffer = (UINT8 *) (UINTN) RegisterBuf;
    ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RDX, sizeof (UINT32), CpuNum, &MergeBufferSize);

    if (MergeBufferSize < sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE)) {

      RegisterBuf = EFI_L05_SAVE_MARKER_BUFFER_TOO_SMALL;
      WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
      RegisterBuf = sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE);
      WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RDX, sizeof (UINT32), CpuNum, &RegisterBuf);
      return EFI_SUCCESS;
    }

    //
    // Check Inputted marker.
    //
    if (!IsValidMarker (Marker, MarkerSize)) {

      RegisterBuf = EFI_L05_SAVE_MARKER_NOT_COMPLETED;
      WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
      return EFI_SUCCESS;
    }

    AllZeroMarkerInFlash = TRUE;

    //
	// Don't process Markerkey with outside buffer to prevent malware from decrypting Markerkey.
    //
    Status = gSmst->SmmAllocatePool (
                      EfiRuntimeServicesData,
                      sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE),
                      &TempMergeBuffer
                      );

    ZeroMem (TempMergeBuffer, sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE));

    Status = ProcessMarkerkey ((EFI_ACPI_SLP_MARKER_STRUCTURE *) TempMergeBuffer);

    if (EFI_ERROR (Status)) {

      RegisterBuf = EFI_L05_SAVE_MARKER_NOT_COMPLETED;
      WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
      gSmst->SmmFreePool (TempMergeBuffer);
      return EFI_SUCCESS;
    }

    //
    //  If original marker is all zero, the new marker can be set
    //
    for (Index = 0 ; Index < sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE) ; Index++) {
      if (TempMergeBuffer[Index] != 0) {
        AllZeroMarkerInFlash = FALSE;
        break;
      }
    }

    Status = ProcessManufactureInfo (&EfiL05Slp20MfgState);

    if (EFI_ERROR (Status)) {

      RegisterBuf = EFI_L05_SAVE_MARKER_NOT_COMPLETED;
      WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
      gSmst->SmmFreePool (TempMergeBuffer);
      return EFI_SUCCESS;
    }

    if (AllZeroMarkerInFlash || (EfiL05Slp20MfgState == EFI_L05_SLP20_MFG_NOT_DONE)) {
      Status = ProcessRandomNumber (&EfiL05Slp20RandomInfo);

      if (EFI_ERROR (Status)) {

        RegisterBuf = EFI_L05_SAVE_MARKER_NOT_COMPLETED;
        WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
        gSmst->SmmFreePool (TempMergeBuffer);
        return EFI_SUCCESS;
      }

      for (Index = 0 ; Index < sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE) ; Index++) {
        TempMergeBuffer[Index] = Marker[Index] ^ EfiL05Slp20RandomInfo.RandomNum0;
      }

      Status = OemSvcSetSlp20Marker ((EFI_ACPI_SLP_MARKER_STRUCTURE *) TempMergeBuffer);

      if (EFI_ERROR (Status)) {

        RegisterBuf = EFI_L05_SAVE_MARKER_NOT_COMPLETED;
        WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
        gSmst->SmmFreePool (TempMergeBuffer);
        return EFI_SUCCESS;
      }

      gSmst->SmmFreePool (TempMergeBuffer);
      RegisterBuf = EFI_L05_SAVE_MARKER_SUCCESS;
      WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
    }
  }

  return EFI_SUCCESS;
}

STATIC
EFI_STATUS
OemSlp20RetrieveMarkerCallback (
  IN EFI_HANDLE                         DispatchHandle,
  IN CONST VOID                         *Context         OPTIONAL,
  IN OUT VOID                           *CommBuffer      OPTIONAL,
  IN OUT UINTN                          *CommBufferSize  OPTIONAL
  )
{
  EFI_STATUS                            Status;
  UINT32                                RegisterBuf;
  UINT8                                 CpuNum;
  UINTN                                 SlpFuncNum;

  SlpFuncNum  = OEM_SLP20_RETRIEVE_MARKER_FUNCTION;
  CpuNum      = 0;
  RegisterBuf = 0;
  Status = L05FoundTriggerCpu (SlpFuncNum, &CpuNum);

  if (EFI_ERROR (Status)) {
    return EFI_SUCCESS;
  }

  Status = ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);

  if (!EFI_ERROR (Status) && (RegisterBuf == OEM_SLP20_RETRIEVE_MARKER_FUNCTION)) {

    //
    // By L05 Spec., this SMI must only be active during POST.
    //
    RegisterBuf = EFI_L05_RETRIEVE_NOT_COMPLETED;
    WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
  }

  return EFI_SUCCESS;

}

STATIC
EFI_STATUS
OemSlp20ValidateMarkerCallback (
  IN EFI_HANDLE                         DispatchHandle,
  IN CONST VOID                         *Context         OPTIONAL,
  IN OUT VOID                           *CommBuffer      OPTIONAL,
  IN OUT UINTN                          *CommBufferSize  OPTIONAL
  )
{
  EFI_STATUS                            Status;
  UINT32                                RegisterBuf;
  UINTN                                 Index;
  UINTN                                 BufferSize;
  UINT8                                 *Marker;
  EFI_ACPI_SLP_MARKER_STRUCTURE         L05MarkerKey;
  UINT8                                 *Buffer;
  EFI_L05_SLP20_RANDOM_INFO             EfiL05Slp20RandomInfo;
  UINT8                                 CpuNum;
  UINTN                                 SlpFuncNum;
  
  SlpFuncNum  = OEM_SLP20_VALIDATE_MARKER_FUNCTION;
  CpuNum      = 0;
  RegisterBuf = 0;
  Buffer      = NULL;
  BufferSize  = 0;
  
  Status= L05FoundTriggerCpu (SlpFuncNum, &CpuNum);

  if (EFI_ERROR (Status)) {
    return EFI_SUCCESS;
  }

  Status = ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);

  if (!EFI_ERROR (Status) && (RegisterBuf == OEM_SLP20_VALIDATE_MARKER_FUNCTION)) {

    //
    // Set input parameter as spec defined
    //
    ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RSI, sizeof (UINT32), CpuNum, &RegisterBuf);
    Buffer = (UINT8 *) (UINTN) RegisterBuf;
    ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RCX, sizeof (UINT32), CpuNum, &BufferSize);

    if (BufferSize < sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE)) {

      RegisterBuf = EFI_L05_VALIDATE_NOT_COMPLETED;
      WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
      return EFI_SUCCESS;
    }

    Status = ProcessMarkerkey (&L05MarkerKey);

    if (EFI_ERROR (Status)) {

      RegisterBuf = EFI_L05_VALIDATE_NOT_COMPLETED;
      WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
      return EFI_SUCCESS;
    }

    Status = ProcessRandomNumber (&EfiL05Slp20RandomInfo);

    if (EFI_ERROR (Status)) {

      RegisterBuf = EFI_L05_VALIDATE_NOT_COMPLETED;
      WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
      return EFI_SUCCESS;
    }

    Marker = (UINT8 *) &L05MarkerKey;

    for (Index = 0 ; Index < sizeof (EFI_ACPI_SLP_MARKER_STRUCTURE) ; Index++) {

      if (Buffer[Index] != (Marker[Index] ^ EfiL05Slp20RandomInfo.RandomNum0)) {

        RegisterBuf = EFI_L05_VALIDATE_NOT_COMPLETED;
        WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
        return EFI_SUCCESS;
      }
    }

    RegisterBuf = EFI_L05_VALIDATE_SUCCESS;
    WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
  }

  return EFI_SUCCESS;
}

STATIC
EFI_STATUS
L05UpdateSlp20SetMfgDoneCallback (
  IN EFI_HANDLE                         DispatchHandle,
  IN CONST VOID                         *Context         OPTIONAL,
  IN OUT VOID                           *CommBuffer      OPTIONAL,
  IN OUT UINTN                          *CommBufferSize  OPTIONAL
  )
{
  EFI_STATUS                            Status;
  UINT32                                RegisterBuf;
  EFI_L05_SLP20_MFG_STATE               EfiL05Slp20MfgState;
  UINT8                                 CpuNum;
  UINTN                                 SlpFuncNum;
  
  RegisterBuf         = 0;
  EfiL05Slp20MfgState = EFI_L05_SLP20_MFG_DONE;
  CpuNum              = 0;
  SlpFuncNum          = EFI_L05_UPDATE_SLP20_SET_MFG_DONE_FUNCTION;

  Status = L05FoundTriggerCpu (SlpFuncNum, &CpuNum);

  if (EFI_ERROR (Status)) {
    return EFI_SUCCESS;
  }

  Status = ReadDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);

  if (!EFI_ERROR (Status) && (RegisterBuf == EFI_L05_UPDATE_SLP20_SET_MFG_DONE_FUNCTION)) {

    Status = OemSvcSetSlp20ManufactureInfo (&EfiL05Slp20MfgState);

    if (EFI_ERROR (Status)) {

      RegisterBuf = EFI_L05_SET_MFG_DONE_NOT_COMPLETED;
      WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
      return EFI_SUCCESS;
    }

    RegisterBuf = EFI_L05_SET_MFG_DONE_SUCCESS;
    WriteDwordRegister (EFI_SMM_SAVE_STATE_REGISTER_RAX, sizeof (UINT32), CpuNum, &RegisterBuf);
  }

  return EFI_SUCCESS;
}

EFI_STATUS
L05UpdateSlp20EntryPoint (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  EFI_HANDLE                            SwHandle;
  EFI_SMM_SW_DISPATCH2_PROTOCOL         *SwDispatch;
  EFI_SMM_SW_REGISTER_CONTEXT           SwContext;

  //
  // Get the Smm CPU protocol
  //
  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmCpuProtocolGuid,
                    NULL,
                    &mSmmCpu
                    );

  ASSERT_EFI_ERROR (Status);

  //
  // Get the Sw dispatch protocol
  //
  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmSwDispatch2ProtocolGuid,
                    NULL,
                    &SwDispatch
                    );

  ASSERT_EFI_ERROR (Status);

  //
  // Register Software SMI function
  //
  SwContext.SwSmiInputValue = OEM_SLP20_SAVE_MARKER_CALLBACK;
  Status = SwDispatch->Register (
                         SwDispatch,
                         OemSlp20SaveMarkerCallback,
                         &SwContext,
                         &SwHandle
                         );
  ASSERT_EFI_ERROR (Status);

  //
  // Register Software SMI function
  // By L05 Spec., this SMI must only be active during POST.
  //
  SwContext.SwSmiInputValue = OEM_SLP20_RETRIEVE_MARKER_CALLBACK;
  Status = SwDispatch->Register (
                         SwDispatch,
                         OemSlp20RetrieveMarkerCallback,
                         &SwContext,
                         &SwHandle
                         );
  ASSERT_EFI_ERROR (Status);

  //
  // Register Software SMI function
  //
  SwContext.SwSmiInputValue = OEM_SLP20_VALIDATE_MARKER_CALLBACK;
  Status = SwDispatch->Register (
                         SwDispatch,
                         OemSlp20ValidateMarkerCallback,
                         &SwContext,
                         &SwHandle
                         );

  //
  // Register Software SMI function
  //
  SwContext.SwSmiInputValue = EFI_L05_UPDATE_SLP20_SET_MANUFACTURE_DONE_CALLBACK;
  Status = SwDispatch->Register (
                         SwDispatch,
                         L05UpdateSlp20SetMfgDoneCallback,
                         &SwContext,
                         &SwHandle
                         );

  ASSERT_EFI_ERROR (Status);

  return Status;

}
