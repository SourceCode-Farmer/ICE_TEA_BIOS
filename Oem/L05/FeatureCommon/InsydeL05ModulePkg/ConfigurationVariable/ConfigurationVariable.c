/** @file
  Set Configuration Variable for L05 Feature

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "ConfigurationVariable.h"

/**
  Set Configuration Variable for L05 Feature

  There are two conditions will reset the value of Configuration Variable(all zero, 64 bits data),
  1. The Configuration Variable is not exist.
  2. The size of Configuration Variable is not equal to 64 bits.

  @param  ImageHandle                   The firmware allocated handle for the UEFI image.
  @param  SystemTable                   A pointer to the EFI System Table.

  @retval EFI_SUCCESS                   The operation completed successfully.
  @retval Others                        An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
ConfigurationVariableDriverEntryPoint (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  UINTN                                 VariableSize;
  UINT8                                 VariableBuf[L05_CONFIGURATION_VARIABLE_SIZE];

  Status       = EFI_SUCCESS;
  VariableSize = L05_CONFIGURATION_VARIABLE_SIZE;

  ZeroMem (VariableBuf, L05_CONFIGURATION_VARIABLE_SIZE);

  //
  // Check the Configuration Variable is exist and the size is equal to 64 bits.
  //
  Status = gRT->GetVariable (
                  L05_CONFIGURATION_VARIABLE_NAME,
                  &gEfiL05ConfigurationVariableGuid,
                  NULL,
                  &VariableSize,
                  VariableBuf
                  );

  if (!EFI_ERROR (Status) && (VariableSize == L05_CONFIGURATION_VARIABLE_SIZE)) {

    return Status;

  } else {

    //
    // Delete the wrong size of Configuration Variable
    //
    Status = gRT->SetVariable (
                    L05_CONFIGURATION_VARIABLE_NAME,
                    &gEfiL05ConfigurationVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                    0,
                    NULL
                    );
  }

  //
  // Set Configuration Variable by default(Default value should be all ZERO).
  //
  ZeroMem (VariableBuf, L05_CONFIGURATION_VARIABLE_SIZE);

  Status = gRT->SetVariable (
                  L05_CONFIGURATION_VARIABLE_NAME,
                  &gEfiL05ConfigurationVariableGuid,
                  EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                  L05_CONFIGURATION_VARIABLE_SIZE,
                  VariableBuf
                  );

  return Status;
}

