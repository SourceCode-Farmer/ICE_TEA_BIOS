/** @file
  Set Configuration Variable for L05 Feature

;******************************************************************************
;* Copyright (c) 2013, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CONFIGURATION_VARIABLE_H_
#define _CONFIGURATION_VARIABLE_H_

#include <Uefi.h>

//
// Libraries
//
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>

//
// UEFI Driver Model Protocols
//

//
// Consumed Protocols
//

//
// Produced Protocols
//

//
// Guids
//
#include <Guid/L05ConfigurationVariable.h>

//
// Driver Version
//

//
// Protocol instances
//

//
// Include files with function prototypes
//

#endif
