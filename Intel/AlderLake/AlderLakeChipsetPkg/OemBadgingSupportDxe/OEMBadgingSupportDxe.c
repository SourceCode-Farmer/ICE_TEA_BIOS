/** @file
  Source file for EFI OEM badging support driver.

;******************************************************************************
;* Copyright (c) 2014 - 2019 , Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <ChipsetSetupConfig.h>
#include <OEMBadgingString.h>
#include <Guid/Pcx.h>
#include <OEMBadgingSupportDxe.h>
#include <Library/BadgingSupportLib.h>
#include <Library/BaseLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeOemSvcChipsetLib.h>
#include <Library/DebugLib.h>
#include <Library/DxeInsydeChipsetLib.h>
#include <Library/MemoryAllocationLib.h>

extern OEM_BADGING_STRING mOemBadgingString[];
extern OEM_BADGING_STRING mOemBadgingStringMe5Mb[];
extern OEM_BADGING_STRING mOemBadgingStringAfterSelect[];
extern OEM_BADGING_STRING mOemBadgingStringAfterSelectMe5Mb[];
extern OEM_BADGING_STRING mOemBadgingStringInTextMode[];
extern OEM_BADGING_STRING mOemBadgingStringInTextModeAfterSelect[];
extern OEM_BADGING_STRING mOemBadgingStringInTextModeMe5Mb[];
extern OEM_BADGING_STRING mOemBadgingStringInTextModeMe5MbAfterSelect[];
extern const UINTN StringCountOfmOemBadgingString;
extern const UINTN StringCountOfmOemBadgingStringMe5Mb;
extern const UINTN StringCountOfmOemBadgingStringAfterSelectMe5Mb;
extern const UINTN StringCountOfmOemBadgingStringInText;
extern const UINTN StringCountOfmOemBadgingStringInText5Mb;

EFI_STATUS
InitializeBadgingSupportProtocol (
  IN    EFI_HANDLE                      ImageHandle,
  IN    EFI_SYSTEM_TABLE                *SystemTable,
  IN    EFI_OEM_BADGING_LOGO_DATA       *BadgingData,
  IN    UINTN                           NumberOfLogo,
  IN    OEM_VIDEO_MODE_SCR_STR_DATA     *OemVidoeModeScreenStringData,
  IN    UINTN                           NumberOfVidoeModeScreenStringDataCount,
  IN    OEM_BADGING_STRING              *OemBadgingString,
  IN    OEM_BADGING_STRING              **OemBadgingStringAfterSelect,
  IN    UINTN                           NumberOfString
);

BOOLEAN
IntelInside (
  VOID
  );

EFI_OEM_BADGING_LOGO_DATA mBadgingData[] = {
  {EFI_DEFAULT_PCX_LOGO_GUID,
   EfiBadgingSupportFormatPCX,
   EfiBadgingSupportDisplayAttributeCenter,
   0,
   0,
   NULL,
   EfiBadgingSupportImageLogo
  },
  //
  // BIOS Vendor Insyde Badge
  //
  {EFI_INSYDE_BOOT_BADGING_GUID,
   EfiBadgingSupportFormatBMP,
   EfiBadgingSupportDisplayAttributeCustomized,
   0,
   0,
   NULL,
   EfiBadgingSupportImageBoot
  },
  {EFI_INSYDE_BOOT_BADGING_GUID,
#ifdef UNSIGNED_FV_SUPPORT
   EfiBadgingSupportFormatJPEG,
#else
   EfiBadgingSupportFormatBMP,
#endif
   EfiBadgingSupportDisplayAttributeCenter,
   0,
   0,
   NULL,
   EfiBadgingSupportImageBadge
  }
};

OEM_VIDEO_MODE_SCR_STR_DATA mOemVidoeModeScreenStringData[] = {
  //Bios Setup String
  {
    640,
    480,
    OemEnterSetupStr,
    400,
    392
  },
  {
    800,
    600,
    OemEnterSetupStr,
    500,
    490
  },
  {
    1024,
    768,
    OemEnterSetupStr,
    640,
    628
  },
  //Port80String
  {
    640,
    480,
    OemPort80CodeStr,
    600,
    440
  },
  {
    800,
    600,
    OemPort80CodeStr,
    750,
    550
  },
  {
    1024,
    768,
    OemPort80CodeStr,
    960,
    704
  },
  //Quality String
  {
    640,
    480,
    OemBuildQualityStr,
    280,
    232
  },
  {
    800,
    600,
    OemBuildQualityStr,
    350,
    290
  },
  {
    1024,
    768,
    OemBuildQualityStr,
    448,
    372
  }
};


/**
  Entry point of EFI OEM Badging Support driver

  @param[in]  ImageHandle - image handle of this driver
  @param[in]  SystemTable - pointer to standard EFI system table

  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurred when executing this entry point.

**/
EFI_STATUS
EFIAPI
OEMBadgingEntryPoint (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS                            Status;
  CHIPSET_CONFIGURATION                  *SetupVariable;
  UINTN                                 StringCount;
  OEM_BADGING_STRING                    *OemBadgingStringAfterSelect;
  EFI_OEM_BADGING_LOGO_DATA             *BadgingDataPointer;
  UINTN                                 BadgingDataSize;
  OEM_BADGING_STRING                    *OemBadgingStringPointer;
  OEM_BADGING_STRING                    *OemBadgingStringInTextModePointer;
  OEM_BADGING_STRING                    *OemBadgingStringAfterSelectWithMePointer;
  OEM_BADGING_STRING                    *OemBadgingStringAfterSelectWithMeInTextModePointer;

//  Status = gBS->LocateProtocol (
//                  &gEfiSetupUtilityProtocolGuid,
//                  NULL,
//                  (VOID **)&EfiSetupUtility
//                  );
//  if (EFI_ERROR (Status)) {
//    return Status;
//  }
//  SetupVariable = (CHIPSET_CONFIGURATION *)(EfiSetupUtility->SetupNvData);
  SetupVariable = NULL;
  SetupVariable = AllocateZeroPool (sizeof (CHIPSET_CONFIGURATION));
  if (SetupVariable == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = GetChipsetSetupVariableDxe (SetupVariable, sizeof (CHIPSET_CONFIGURATION));
  if (EFI_ERROR (Status)) {
    FreePool (SetupVariable);
  ASSERT_EFI_ERROR (Status);
    return Status;
  }

  //
  // Assign OemBadgingString to correct pointer according to ME type (1.5MB or 5.0MB)
  //
  if(FeaturePcdGet (PcdAmtEnable)) {
    //
    // ME = 5.0MB
    //
    OemBadgingStringPointer = mOemBadgingStringMe5Mb;
    OemBadgingStringAfterSelectWithMePointer = mOemBadgingStringAfterSelectMe5Mb;
    OemBadgingStringInTextModePointer = mOemBadgingStringInTextModeMe5Mb;
    OemBadgingStringAfterSelectWithMeInTextModePointer = &mOemBadgingStringInTextModeMe5MbAfterSelect[0];
    StringCount = SetupVariable->QuietBoot ? StringCountOfmOemBadgingStringMe5Mb : StringCountOfmOemBadgingStringInText5Mb;
  } else {
    //
    // ME = 1.5MB
    //
    OemBadgingStringPointer = mOemBadgingString;
    OemBadgingStringAfterSelectWithMePointer = mOemBadgingStringAfterSelect;
    OemBadgingStringInTextModePointer = mOemBadgingStringInTextMode;
    OemBadgingStringAfterSelectWithMeInTextModePointer = &mOemBadgingStringInTextModeAfterSelect[0];
    StringCount =  SetupVariable->QuietBoot ? StringCountOfmOemBadgingString : StringCountOfmOemBadgingStringInText;
  }

  BadgingDataPointer = mBadgingData;
  BadgingDataSize = sizeof (mBadgingData) / sizeof (EFI_OEM_BADGING_LOGO_DATA);

  DEBUG ((DEBUG_INFO, "Dxe OemChipsetServices Call: OemSvcUpdateOemBadgingLogoData \n"));
  Status = OemSvcUpdateOemBadgingLogoData (
             &BadgingDataPointer,
             &BadgingDataSize,
             &OemBadgingStringPointer,
             &OemBadgingStringInTextModePointer,
             &StringCount,
             &OemBadgingStringAfterSelectWithMePointer,
             &OemBadgingStringAfterSelectWithMeInTextModePointer
             );
  DEBUG ((DEBUG_INFO, "Dxe OemChipsetServices OemSvcUpdateOemBadgingLogoData Status: %r\n", Status));
  if (Status == EFI_SUCCESS) {
    if (SetupVariable != NULL) {
      FreePool (SetupVariable);
    }
    return EFI_SUCCESS;
  }

  OemBadgingStringPointer = SetupVariable->QuietBoot ? OemBadgingStringPointer : OemBadgingStringInTextModePointer,
  OemBadgingStringAfterSelect = SetupVariable->QuietBoot ? OemBadgingStringAfterSelectWithMePointer : OemBadgingStringAfterSelectWithMeInTextModePointer;

  Status = InitializeBadgingSupportProtocol (
            ImageHandle,
            SystemTable,
            BadgingDataPointer,
            BadgingDataSize,
            mOemVidoeModeScreenStringData,
            sizeof (mOemVidoeModeScreenStringData) / sizeof (OEM_VIDEO_MODE_SCR_STR_DATA),
            OemBadgingStringPointer,
            (OEM_BADGING_STRING**) OemBadgingStringAfterSelect,
            StringCount
            );
  if (SetupVariable != NULL) {
    FreePool (SetupVariable);
  }
  return Status;
}

BOOLEAN
IntelInside (
  VOID
  )
{
  UINT32 RegEax;
  UINT32 RegEbx;
  UINT32 RegEcx;
  UINT32 RegEdx;

  //
  // Is a GenuineIntel CPU?
  //
  AsmCpuid (0x0, &RegEax, &RegEbx, &RegEcx, &RegEdx);
  if ((RegEbx != 'uneG') || (RegEdx != 'Ieni') || (RegEcx != 'letn')) {
    return FALSE;
  }

  return TRUE;
}
