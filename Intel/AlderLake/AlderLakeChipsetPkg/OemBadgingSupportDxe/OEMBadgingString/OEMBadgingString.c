/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*++
  Module Name:

    OEMBadgingString.c

  Abstract:

    OEM can define badging string in this file.

--*/
#include <ChipsetSetupConfig.h>
#include <OEMBadgingString.h>
#include <Library/HiiDbLib.h>
#include <Library/HiiStringLib.h>
#include <Library/HiiConfigAccessLib.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/PrintLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeInsydeChipsetLib.h>
//
// OEM can modify the background and foreground color of the OEM dadging string through through the below data
// for example:
//   { 50, 280, { 0x00, 0x00, 0x00, 0x00 }, { 0xFF, 0xFF, 0xFF, 0x00 }, STRING_TOKEN ( STR_OEM_BADGING_STR_CPUID ), GetCpuId },
//     { 0x00, 0x00, 0x00, 0x00 } indicate the foreground color { Blue, Green, Red, Reserved }
//     { 0xFF, 0xFF, 0xFF, 0x00 } indicate the background color { Blue, Green, Red, Reserved }
//


//
// Dear all engineer:
//   The following badging string are re-organized to make sure every string has its own function and removed unnecessary string array.
//
// Graphic mode, ME = 1.5MB
//
OEM_BADGING_STRING *mOemBadgingString = NULL;


//
// Graphic mode, ME = 1.5MB, after select
// Note: if mOemBadgingString is [1][b] array, then mOemBadgingStringAfterSelect should be [b][b] array
// For example: if mOemBadgingString is [1][5], mOemBadgingStringAfterSelect should be [5][5]
//
OEM_BADGING_STRING *mOemBadgingStringAfterSelect = NULL;

//
// Graphic mode, ME = 5.0MB
//
OEM_BADGING_STRING *mOemBadgingStringMe5Mb = NULL;

//
// Graphic mode, ME = 5.0MB, after select
// Note: if mOemBadgingStringMe5Mb is [1][b] array, then mOemBadgingStringAfterSelectMe5Mb should be [b][b] array
// For example: if mOemBadgingStringMe5Mb is [1][3], mOemBadgingStringAfterSelectMe5Mb should be [3][3]
// The purpose of this array *AfterSelect* is to show ONLY the select key press and hidden the rest with blank string.
// Therefore, 4 different key shown for user to pressed will have 4 different results.
//
OEM_BADGING_STRING *mOemBadgingStringAfterSelectMe5Mb = NULL;

//
// Taxt mode, ME = 1.5MB
//
OEM_BADGING_STRING mOemBadgingStringInTextMode[] = {
  { OEM_STRING_LOCATION_X,       OEM_STRING1_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_OEM_BADGING_STR_CPUID      ), GetCpuId },
  { OEM_STRING_LOCATION_X,       OEM_STRING2_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING               ), NULL     }
};

OEM_BADGING_STRING mOemBadgingStringInTextModeAfterSelect[2][2] = {
{
  { OEM_STRING_LOCATION_X,       OEM_STRING1_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_OEM_BADGING_STR_CPUID      ), GetCpuId },
  { OEM_STRING_LOCATION_X,       OEM_STRING2_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING               ), NULL     }
},
{
  { OEM_STRING_LOCATION_X,       OEM_STRING1_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING_COVER         ), NULL     },
  { OEM_STRING_LOCATION_X,       OEM_STRING2_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING_COVER         ), NULL     }
}
};

//
// Taxt mode, ME = 5.0MB
//
OEM_BADGING_STRING mOemBadgingStringInTextModeMe5Mb[] = {
  { OEM_STRING_LOCATION_X,       OEM_STRING1_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_OEM_BADGING_STR_CPUID     ), GetCpuId    },
  { OEM_STRING_LOCATION_X,       OEM_STRING2_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING3_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING4_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        }
};

OEM_BADGING_STRING mOemBadgingStringInTextModeMe5MbAfterSelect[4][4]= {
{
  { OEM_STRING_LOCATION_X,       OEM_STRING1_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_OEM_BADGING_STR_CPUID     ), GetCpuId    },
  { OEM_STRING_LOCATION_X,       OEM_STRING2_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING3_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING4_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
},
{
  { OEM_STRING_LOCATION_X,       OEM_STRING1_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_OEM_BADGING_STR_CPUID     ), GetCpuId    },
  { OEM_STRING_LOCATION_X,       OEM_STRING2_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING3_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING4_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
},
{
  { OEM_STRING_LOCATION_X,       OEM_STRING1_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_OEM_BADGING_STR_CPUID     ), GetCpuId    },
  { OEM_STRING_LOCATION_X,       OEM_STRING2_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING3_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING4_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING              ), NULL        },
},
{
  { OEM_STRING_LOCATION_X,       OEM_STRING1_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING_COVER        ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING2_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING_COVER        ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING3_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING_COVER        ), NULL        },
  { OEM_STRING_LOCATION_X,       OEM_STRING4_LOCATION_Y, { 0xFF, 0xFF, 0xFF, 0x00 }, { 0x00, 0x00, 0x00, 0x00 }, STRING_TOKEN ( STR_BLANK_STRING_COVER        ), NULL        },
}
};


//
// The following const is the string count of each OEM badging string.
// These definitions are used in OEMBadgingSupportDxe.c for StringCount assignment.
//
const UINTN StringCountOfmOemBadgingString                  =  0;
const UINTN StringCountOfmOemBadgingStringMe5Mb             =  0;
const UINTN StringCountOfmOemBadgingStringAfterSelectMe5Mb  =  0;
const UINTN StringCountOfmOemBadgingStringInText            =  sizeof (mOemBadgingStringInTextMode) / sizeof (mOemBadgingStringInTextMode[0]);
const UINTN StringCountOfmOemBadgingStringInText5Mb         =  sizeof (mOemBadgingStringInTextModeMe5Mb) / sizeof (mOemBadgingStringInTextModeMe5Mb[0]);

EFI_STATUS
GetId (
  IN OUT UINTN    *CpuId
  )
{
  UINT32 RegEax;
  AsmCpuid (0x01, &RegEax, NULL, NULL, NULL);

  *CpuId = RegEax;

  return EFI_SUCCESS;
}

BOOLEAN
GetCpuId (
  IN OEM_BADGING_STRING  *Structure,
  OUT CHAR16             **StringData
  )
{
  CHAR16                                *TokenStr;
  UINTN                                 TokenStrLen;
  UINTN                                 CpuId;
  EFI_STATUS                            Status;

  Status = GetId (&CpuId);
  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  TokenStr = HiiGetPackageString (&gEfiCallerIdGuid, Structure->StringToken, NULL);
  ASSERT (TokenStr != NULL);
  if (TokenStr == NULL){
    return FALSE;
  }

  TokenStrLen = StrLen (TokenStr);
  *StringData = ReallocatePool (
                  (TokenStrLen + 1) * sizeof (CHAR16),
                  (TokenStrLen + 1) * sizeof (CHAR16) + 100,
                  TokenStr
                  );
  ASSERT (*StringData != NULL);
  if (*StringData == NULL) {
    FreePool (TokenStr);
    return FALSE;
  }

  UnicodeValueToStringS (&((*StringData)[TokenStrLen]), 100, RADIX_HEX, CpuId, 0);
  return TRUE;
}
/**
 Check the platform ME fireware type and decide the F9 / F10 should be displayed or not.

 @param [in]   Structure        Array of Oem Badging String.
 @param [out]  StringData       Hot Key string data.

 @retval TRUE                   The ME fireware type is 5MB, the MEBx relative hot key message should be displayed.
 @retval FALSE                  The ME fireware type is 1.5MB, BIOS will not show any MEBx relative hot key message.

**/
BOOLEAN
GetMeFwType(
  IN OEM_BADGING_STRING  *Structure,
  OUT CHAR16             **StringData
  )
{
  EFI_STATUS                        Status;
  ME_SETUP                          *MeSetup;

  *StringData = HiiGetPackageString (&gEfiCallerIdGuid, Structure->StringToken, NULL);

  MeSetup = NULL;
  MeSetup = AllocateZeroPool (sizeof (ME_SETUP));
  if (MeSetup == NULL) {
    return FALSE;
  }
  Status = GetChipsetMeSetupVariableDxe (MeSetup, sizeof (ME_SETUP));
  if (EFI_ERROR (Status)) {
    FreePool (MeSetup);
    ASSERT_EFI_ERROR (Status);
    return FALSE;
  }

  if (MeSetup->MeImageType == ME_IMAGE_CORPORATE_SKU_FW) {
    if (MeSetup != NULL) {
      FreePool (MeSetup);
    }
    return TRUE;
  }

  if (MeSetup != NULL) {
    FreePool (MeSetup);
  }
  return FALSE;
}
