/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*++ 
  Module Name:
  
    OEMBadgingString.h
  
  Abstract:
  
    Header file for OEM badging support driver
    
--*/
#ifndef _EFI_OEM_BADGING_STRING_H
#define _EFI_OEM_BADGING_STRING_H

#include <Library/BadgingSupportLib.h>
#include <Library/PcdLib.h>

#define ME_IMAGE_CONSUMER_SKU_FW      0x03
#define ME_IMAGE_CORPORATE_SKU_FW     0x04

EFI_STATUS
GetId(
  IN OUT UINTN    *CpuId
);

BOOLEAN
GetCpuId(
  IN OEM_BADGING_STRING  *Structure,
  OUT CHAR16             **StringData
);

BOOLEAN
GetMeFwType(
  IN OEM_BADGING_STRING  *Structure,
  OUT CHAR16             **StringData
);


//
// Badging String Count
//

//
// If set x or y value of OEM badging string to -1, system will auto place string at
// below boot image when BGRT is present or center of screen when BGRT is not exist.
//
//#define OEM_STRING_GRAPHICAL_LOCATION_X                408
//#define OEM_STRING_GRAPHICAL_ESC_SELECT_LOCATION_X     372
#define OEM_STRING_LOCATION_X                           50
#define OEM_STRING_GRAPHICAL_LOCATION_X                (UINTN) -1
#define OEM_STRING_GRAPHICAL_LOCATION_Y                (UINTN) -1
#define OEM_STRING2_GRAPHICAL_LOCATION_Y               OEM_STRING_GRAPHICAL_LOCATION_Y  -1
#define OEM_STRING3_GRAPHICAL_LOCATION_Y               OEM_STRING2_GRAPHICAL_LOCATION_Y -1 
#define OEM_STRING_GRAPHICAL_ESC_SELECT_LOCATION_X     (UINTN) -1
#define OEM_STRING1_LOCATION_Y      500
#define OEM_STRING2_LOCATION_Y      575
#define OEM_STRING3_LOCATION_Y      550
#define OEM_STRING4_LOCATION_Y      625
#define ME_INFORM_STRING_LOCATION_X                50
#define ME_INFORM_STRING1_LOCATION_Y               0

#endif
