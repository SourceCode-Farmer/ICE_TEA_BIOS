/** @file
  AMD discrete only mode SSDT sample.

;******************************************************************************
;* Copyright (c) 2018 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <AmdDiscrete.asi>

DefinitionBlock (
    "AmdDiscrete.aml",
    "SSDT",
    1,
    "Insyde",
    "AmdPegTb",
    0x1000
    )
{
    External (DGPU_BRIDGE_SCOPE, DeviceObj)
    External (DGPU2_BRIDGE_SCOPE, DeviceObj)
//    External (EC_SCOPE.BRTS)
    External (\GPRW, MethodObj)
    External (IGDS, FieldUnitObj)
    External (BRTL, FieldUnitObj)
    External (OSYS) // Note: According to the OS type to change the base brightness levels, Win8 = 10, Win7 = 7.
    External (P8XH, MethodObj)
    External (MBGS, MethodObj)
    External (DD2H, MethodObj)
#if !FeaturePcdGet(PcdHgAslCodeForWptLynxPointLp)
    External (DGPU_SCOPE, DeviceObj)
#else
    Scope (DGPU_BRIDGE_SCOPE)
    {
        Device (DGPU_DEVICE)
        {
            Name (_ADR, 0x00000000)
            Method (_PRW, 0)
            {
                Return (GPRW (0x09, 4)) // can wakeup from S4 state
            }
        }
    }
#endif
#if FeaturePcdGet (PcdUseCrbEcFlag)
    External (EC_SCOPE.LSTE)
    External (EC_SCOPE.ECRD, MethodObj) // EC Read Method
#endif

    #include <../OpRegion/AmdOpRegion.asl>

    Include ("AmdDgpu.asl")
    Include ("AmdDgpu2.asl")
    Include ("AmdPort.asl")
    Include ("AmdAtif.asl")
}
