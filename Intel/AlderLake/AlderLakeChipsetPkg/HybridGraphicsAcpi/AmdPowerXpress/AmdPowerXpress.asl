/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <AmdPowerXpress.asi>

DefinitionBlock (
    "AmdPowerXpress.aml",
    "SSDT",
    1,
    "Insyde",
    "AmdTable",
    0x1000
    )
{
    External (PCI_SCOPE, DeviceObj)
    External (DGPU_BRIDGE_SCOPE, DeviceObj)
    External (DGPU_SCOPE, DeviceObj)
    External (DGPU_SCOPE._ON, MethodObj)
    External (DGPU_SCOPE._OFF, MethodObj)
    External (DGPU_SCOPE.SGPO, MethodObj)
    External (IGPU_SCOPE, DeviceObj)
    External (DGPU_SCOPE._ADR)
    External (IGPU_SCOPE._DOD, MethodObj)
    External (IGPU_SCOPE.AINT, MethodObj)
#if FeaturePcdGet (PcdUseCrbEcFlag)
    External (EC_SCOPE.LSTE)
    External (EC_SCOPE.ECRD, MethodObj) // EC Read Method
#endif
    External (\GPRW, MethodObj)
    External (\ECON, IntObj)
    External (PNHM, IntObj)
    External (P0UB, IntObj)
    External (PCSL, IntObj)
    External (DSEN)
    External (NXD1)
    External (NXD2)
    External (NXD3)
    External (NXD4)
    External (NXD5)
    External (NXD6)
    External (NXD7)
    External (NXD8)
    External (\_SB.OSCO)
    External (P8XH, MethodObj)
    External (MBGS, MethodObj)
    External (DD2H, MethodObj)
    External (OSYS)
    External (SGMD)
    External (SGGP)
    External (TDGS)
//    External (DCKE) // Dock event
//    External (UDCK) // Undock request
    External (SUDK) // Surprise undock event
    External (EGPC) // External GPU PCI Configuration Space Buffer Address
    External (EGPV) // External GPU PCI Configuration Space Valid
    External (TBDT) // Thunderbolt Device Tree Info Buffer Address
    External (WRCM, MethodObj) // Description: Function to write a Byte into CMOS 72/73

    Name (DGPU_SCOPE.GSTP, 0) // GPU switch transition in progress
    Name (DGPU_SCOPE.TGXA, 0) // Target GFX adapter as notified by ATPX function 5
    Name (DGPU_SCOPE.AGXA, 0) // Active GFX adapter as notified by ATPX function 6
    Name (DGPU_SCOPE.DSWR, 0) // Display switch request
    Name (DGPU_SCOPE.EMDR, 0) // Expansion mode change request
    Name (DGPU_SCOPE.PXGS, 0) // PowerXpress graphics switch toggle request state
    Name (DGPU_SCOPE.CACD, 0) // Currently active displays
    Name (DGPU_SCOPE.CCND, 0) // Currently connected displays
    Name (DGPU_SCOPE.NACD, 0) // Next active displays
    Name (DGPU_SCOPE.TLSN, 0) // Toggle list sequence index

    //
    // Include OpRegion.
    // AmdOpRegion.asl must be included at the end because the algorithm to
    // initialize each OpRegions will be ended once AOPR is found.
    // (The above algorithm is in HybridGraphicsDxe.c -> InitializeOpRegion())
    //
    #include <../OpRegion/HgOpRegion.asl>
    #include <../OpRegion/VbiosOpRegion.asl>
    #include <../OpRegion/AmdOpRegion.asl>

    Scope (PCI_SCOPE)
    {
        OperationRegion (PAMD, SystemMemory, PCI_SCOPE.DGDA, 0x1000)  // dGPU
        Field (PAMD, DWordAcc, NoLock, Preserve) {
            Offset(0x4),
            CMDR,   8,
            VGAR,   2008,
        }

        Name (VGAB, Buffer(0xFB)
        {
            0x00
        })
    }

    Include ("Atpx.asl")
    Include ("Atif.asl")
}
