/** @file

;******************************************************************************
;* Copyright (c) 2019 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <NvidiaUltOptimus.asi>

DefinitionBlock (
    "NvidiaUltOptimus.aml",
    "SSDT",
    1,
    "Insyde",
    "NvdTable",
    0x1000
    )
{
    External (CPU0_SCOPE._PSS, MethodObj)
    External (CPU0_SCOPE._PPC, MethodObj)
    External (CPU1_SCOPE._PPC, MethodObj)
    External (CPU2_SCOPE._PPC, MethodObj)
    External (CPU3_SCOPE._PPC, MethodObj)
    External (DGPU_BRIDGE_SCOPE, DeviceObj)
    External (DGPU_SCOPE, DeviceObj)
    External (DGPU_SCOPE._OFF, MethodObj)
    External (DGPU_SCOPE._ON, MethodObj)
    External (IGPU_SCOPE, DeviceObj)
    External (PCI_SCOPE, DeviceObj)
    External (PCI_SCOPE.ELCT, IntObj)
    External (PCI_SCOPE.HGOF, MethodObj)
    External (PCI_SCOPE.HGON, MethodObj)
    External (PCI_SCOPE.LCTR, IntObj)
    External (PCI_SCOPE.LKDN, FieldUnitObj)
    External (PCI_SCOPE.LKSN, FieldUnitObj)
    External (PCI_SCOPE.RTDS, MethodObj)
    External (PCI_SCOPE.RTEN, MethodObj)
    External (PCRR, MethodObj)
    External (PCRW, MethodObj)
    External (PNOT, MethodObj)
    External (\_TZ.TZ01._TMP, MethodObj)

//  From CpuNvs.asl
    External (\_SB.CPPC, FieldUnitObj)
    External (\_SB.HWPV, IntObj)

//  For debug purpose
    External (MBGS, MethodObj)
    External (P8XH, MethodObj)

//  From GpioLib.asl
    External (\_SB.CAGS, MethodObj)
    External (\_SB.GGIV, MethodObj)
    External (\_SB.GPC0, MethodObj)
    External (\_SB.SGOV, MethodObj)
    External (\_SB.SHPO, MethodObj)
    External (\_SB.SPC0, MethodObj)

//  From SaNvs.asl
    External (\DID1)
    External (\DID2)
    External (\DID3)
    External (\DID4)
    External (\DID5)
    External (\DID6)
    External (\DID7)
    External (\EECP)
    External (\SGGP)

    //
    // Include OpRegion.
    // NvidiaOpRegion.asl must be included at the end because the algorithm to
    // initialize each OpRegions will be ended once NOPR is found.
    // (The above algorithm is in HybridGraphicsDxe.c -> InitializeOpRegion())
    //
    #include <../OpRegion/HgOpRegion.asl>
    #include <../OpRegion/VbiosOpRegion.asl>
    #include <../OpRegion/NvidiaOpRegion.asl>

    Include ("Optimus.asl")
    Include ("NvHg.asl")
    Include ("NvHgDsm.asl")
    Include ("NvGps.asl")
    Include ("NvUltGc6_v2.x.asl")
    Include ("NvNbci.asl")
    Include ("NvSpb.asl")
}
