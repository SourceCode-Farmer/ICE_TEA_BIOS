/** @file
  nVIDIA discrete only mode SSDT sample.

;******************************************************************************
;* Copyright (c) 2019 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <NvidiaDiscrete.asi>

DefinitionBlock (
    "NvidiaDiscrete.aml",
    "SSDT",
    1,
    "Insyde",
    "NvdPegTb",
    0x1000
    )
{
    External (CPU0_SCOPE._PSS, MethodObj)
    External (DGPU_BRIDGE_SCOPE, DeviceObj)
    External (DGPU_BRIDGE_SCOPE.LDIS, IntObj)
    External (DGPU2_BRIDGE_SCOPE, DeviceObj)
    External (DGPU_SCOPE, DeviceObj)
    External (PCI_SCOPE, DeviceObj)
//    External (PCI_SCOPE.P0AP, IntObj)
//    External (PCI_SCOPE.P0LS, IntObj)
//    External (PCI_SCOPE.P0RM, IntObj)
    External (PCI_SCOPE.RTDS, MethodObj)
    External (PCI_SCOPE.RTEN, MethodObj)
    External (PCRR, MethodObj)
    External (PCRW, MethodObj)
    External (PNOT, MethodObj)
    External (\_TZ.TZ01._TMP, MethodObj)

//  From CpuNvs.asl
    External (\_SB.CPPC, FieldUnitObj)
    External (\_SB.HWPV, IntObj)

//  For debug purpose
    External (MBGS, MethodObj)
    External (P8XH, MethodObj)

//  From GpioLib.asl
    External (\_SB.CAGS, MethodObj)
    External (\_SB.GGIV, MethodObj)
    External (\_SB.GPC0, MethodObj)
    External (\_SB.SGOV, MethodObj)
    External (\_SB.SHPO, MethodObj)
    External (\_SB.SPC0, MethodObj)

//  From PlatformNvs.asl
    External (OSYS) // Note: According to the OS type to change the base brightness levels, Win8 = 10, Win7 = 7.

//  From SaNvs.asl
    External (\BRTL)
    External (\DID1)
    External (\DID2)
    External (\DID4)
    External (\HGMD)
    External (\IGDS)
    External (\SGGP)

    //
    // Include OpRegion.
    // NvidiaOpRegion.asl must be included at the end because the algorithm to
    // initialize each OpRegions will be ended once NOPR is found.
    // (The above algorithm is in HybridGraphicsDxe.c -> InitializeOpRegion())
    //
    #include <../OpRegion/HgOpRegion.asl>
    #include <../OpRegion/VbiosOpRegion.asl>
    #include <../OpRegion/NvidiaOpRegion.asl>

    Include ("NvDgpu.asl")
//    Include ("NvDgpu2.asl")
    Include ("NvGps.asl")
    Include ("NvGc6_v2.x.asl")
    Include ("NvNbci.asl")
    Include ("NvPort.asl")
    Include ("NvWmi.asl")
}
