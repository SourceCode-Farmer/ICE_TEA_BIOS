/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*
    == Readme ==
    For NBCI support, SBIOS has to adjust below code to meet platform:
    - Fill GSV1, GSV2(if need) and GSDR buffer from NVIDIA AE
    - \_SB.PCI0.PEG0.DEID -> Device ID of Host Bridge
*/

Scope (DGPU_SCOPE)
{
    // NVIDIA NBCI related start
    Name (GSV1, Buffer(0x01)                 // Buffer for VK key#1
    {
        0x00
    })
    Name (GSV2, Buffer(0x01)                 // Buffer for VK key#2 (Optional)
    {
        0x00
    })
    Name (GSDR, Buffer(0x01)                 // Buffer for DR key
    {
        0x00
    })

    Method (NBCI, 4, Serialized)
    {
        If (LNotEqual (Arg1, 0x0102))
        {
            Return (STATUS_ERROR_UNSUPPORTED)
        }
        //
        // Sub-Function 0: NBCI_FUNC_SUPPORT
        //
        If (LEqual (Arg2, NBCI_FUNC_SUPPORT))
        {
            Return (Buffer (0x04)
            {
                // sub-func: 0,1,16,20 supported
                0x03, 0x00, 0x11, 0x00
            })
        }
        //
        // Sub-Function 1: NBCI_FUNC_PLATCAPS
        //
        If (LEqual (Arg2, NBCI_FUNC_PLATCAPS))
        {
            Name (TEMP, Buffer (0x04)
            {
                0x00, 0x00, 0x00, 0x00
            })
            CreateDWordField (TEMP, Zero, STS0)
            Or (STS0, Zero, STS0)
            Return (TEMP)
        }
        //
        // Sub-Function 16: NBCI_FUNC_GETOBJBYTYPE
        //
        If (LEqual (Arg2, NBCI_FUNC_GETOBJBYTYPE))
        {
            CreateWordField (Arg3, 0x02, BFF0)
            If (LEqual (BFF0, 0x564B))                // Check VK object
            {
                // OEM_customize: VK Key is an object per panel/GPU/HostBridge. OEM SBIOS need to check corresponding info then pick correct VK object to return.
//                If (LEqual (DEID, 0x0D04))          // Example: VK for host bridge DID == 0x0D04
//                {
//                    Return (GSV1)
//                }
//                If (LEqual (DEID, 0x0C04))          // Example: VK for host bridge DID == 0x0C04 (if need)
//                {
//                    Return (GSV2)
//                }
            }
            If (LEqual (BFF0, 0x4452)) // Check DR object
            {
                // OEM_customize: DR Key is an data Object which might vary on different eDP panel. OEM SBIOS need to check corresponding info then pick correct VK object to return.
                Return (GSDR)
            }
        }
        //
        // Sub-Function 20: NBCI_FUNC_GETBACKLIGHT
        //
        If (LEqual (Arg2, NBCI_FUNC_GETBACKLIGHT))
        {
            // OEM_customize: Package returned in this interface is panel related. OEM SBIOS need to follow definition in NBSI Spec to reflect real panel feature in this pacakge.
            Return (Package (32)
            {
                0x8001A450, 0x0200, Zero, Zero, 0x05, One, 0xC8, 0x32, 0x03E8, 0x0B,
                0x32, 0x64, 0x96, 0xC8, 0x012C, 0x0190, 0x01FE, 0x0276, 0x02F8, 0x0366,
                0x03E8, Zero, 0x64, 0xC8, 0x012C, 0x0190, 0x01F4, 0x0258, 0x02BC,
                0x0320, 0x0384, 0x03E8
            })
        }
    }
}
