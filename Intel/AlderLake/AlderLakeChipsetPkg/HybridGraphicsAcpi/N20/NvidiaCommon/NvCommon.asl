/** @file

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*
    == Readme ==
    For NVIDIA general support, SBIOS has to set below GPIO config:
    - Please search keyword: IBV_customize and OEM_customize to match platform design
*/

Scope (DGPU_BRIDGE_SCOPE)
{
    OperationRegion (RPCX, SystemMemory, PCI_SCOPE.DGBA, 0x1000) // PEG Root Port.
    Field (RPCX, DWordAcc, NoLock, Preserve)
    {
        Offset (0x04),
        CMDR,  8,       // Command Register
        Offset (0x19),
        PRBN,  8,       // Primary Bus Number
        Offset (0x4A),  // Device Status Register
        CEDR,  1,
        Offset (0x69),  // Device Control2 Register
            ,  2,
        LREN,  1,
        Offset (0xA4),
        D0ST,  2,
    }

    /* GSTA: Get dGPU Power state
    * Arguments: (0)
    *   None
    *
    * Return: Zero: Current Power state is OFF
    *         One: Current Power state is ON
    *
    * Note: OEM_customize need to add PWR_OK pin GPIO number for "PWOK".
    *       Or figure out different approach to get dGPU power state,
    *       like PXE_RST pin (PWR_EN does not recommend due to GC6 no control it).
    */
    Method (GSTA, 0, NotSerialized)
    {
        If(LEqual(\_SB.GGIV(DGPU_SCOPE.PWGD), Zero)) //OEM_customize
        {
            Return (Zero) // retrun dGPU Off
        }
        Else
        {
            Return (One) // retrun dGPU On
        }
    }
}

Scope (DGPU_SCOPE)
{
    Name (LTRE, Zero) // Buffer for LTR Save/Restore

    Method (_STA, 0)
    {
        Return (0x0F)
    }

    OperationRegion (PCIM, SystemMemory, PCI_SCOPE.DGDA, 0x1000) // dGPU
    Field (PCIM, DWordAcc, NoLock, Preserve)
    {
        NVID,  16,
        NDID,  16,
        CMDR,  8,
        VGAR,  2008,
        Offset (0x48B),
            ,  1,
        HDAE,  1
    }

    OperationRegion (DGPU, SystemMemory, PCI_SCOPE.DGDA, 0x100) // dGPU
    Field (DGPU, DWordAcc, NoLock, Preserve)
    {
        Offset (0x40), // Offset(64)
        SSSV, 32
    }

    Method (_DSM, 4, Serialized)
    {
        //
        // Check for Nvidia Optimus _DSM UUID
        //
        // NVOP_DSM_GUID {A486D8F8-0BDA-471B-A72B-6042A6B5BEE0}
        If (LEqual (Arg0, ToUUID ("A486D8F8-0BDA-471B-A72B-6042A6B5BEE0")))
        {
            Return (DGPU_SCOPE.NVOP (Arg0, Arg1, Arg2, Arg3))
        }

        //
        // Check for Nvidia GPS _DSM UUID
        //
        // GPS_DSM_GUID {A3132D01-8CDA-49BA-A52E-BC9D46DF6B81}
        If (LEqual (Arg0, ToUUID ("A3132D01-8CDA-49BA-A52E-BC9D46DF6B81")))
        {
            If (LNotEqual (DGPU_SCOPE.GPSS, Zero))
            {
                Return (DGPU_SCOPE.GPS (Arg0, Arg1, Arg2, Arg3))
            }
        }

        //
        // Check for Nvidia GC6 _DSM UUID
        //
        // GC6_DSM_GUID {CBECA351-067B-4924-9CBD-B46B00B86F34}
        If (LEqual (Arg0, ToUUID ("CBECA351-067B-4924-9CBD-B46B00B86F34")))
        {
            If (LNotEqual (DGPU_SCOPE.GC6S, Zero))
            {
                Return (DGPU_SCOPE.NVJT (Arg0, Arg1, Arg2, Arg3))
            }
        }

        //
        // Check for Nvidia NBCI _DSM UUID
        //
        // NBCI_DSM_GUID {D4A50B75-65C7-46F7-BFB7-41514CEA0244}
        If (LEqual (Arg0, ToUUID ("D4A50B75-65C7-46F7-BFB7-41514CEA0244")))
        {
            If (LNotEqual (DGPU_SCOPE.NBCS, Zero))
            {
                Return (DGPU_SCOPE.NBCI (Arg0, Arg1, Arg2, Arg3))
            }
        }

        //
        // Check for MXM _DSM UUID
        //
//[-start-210917-GEORGE0004-modify]//
//[-start-210918-QINGLIN0068-modify]//
//#ifndef S77014_SUPPORT
#if !defined(S77014_SUPPORT) && !defined(S570_SUPPORT) && !defined(S77014IAH_SUPPORT)
//[-end-210918-QINGLIN0068-modify]//
        If (LEqual (Arg0, ToUUID ("4004A400-917D-4cf2-B89C-79B62FD55665")))
        {
            Return (DGPU_SCOPE.MXM (Arg0, Arg1, Arg2, Arg3))
        }
#endif
//[-end-210917-GEORGE0004-modify]//

        Return (STATUS_ERROR_UNSPECIFIED)
    }
}