/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

Scope (PCI_SCOPE)
{
    Name (OTM, "OTMACPI 2010-Mar-09 12:08:26")  // OTMACPIP build time stamp.
    Device (WMI2)
    {
        Name (_HID, "PNP0C14")
        Name (_UID, "OPTM")
        Name (_WDG,  Buffer (0x28)
        {
            //{ CA4982BF-C230-458E-B12F-6F16475F-351B} WMI GUID
            // 0x4F, 0x50      OBJECT ID (OP)
            // 0x01   Instance count
            // 0x02   Flags (WMIACPI_REFLAG_METHOD)
            /* 0000 */    0xBF, 0x82, 0x49, 0xCA, 0x30, 0xC2, 0x8E, 0x45,
            /* 0008 */    0XB1, 0x2F, 0x6F, 0x16, 0x47, 0x5F, 0x35, 0x1B,
            /* 0010 */    0x4F, 0x50, 0x01, 0x02,
            //{ A486D8F8-0BDA-471B-A72B-6042A6B5-BEE0} _DSM GUID
            // 0x53, 0x4D      OBJECT ID (SM)
            // 0x01   Instance count
            // 0x00   Flags
            /* 0000 */    0xF8, 0xD8, 0x86, 0xA4, 0xDA, 0x0B, 0x1B, 0x47,
            /* 0008 */    0XA7, 0x2B, 0x60, 0x42, 0xA6, 0xB5, 0xBE, 0xE0,
            /* 0010 */    0x53, 0x4D, 0x01, 0x00,
        })
        // WMOP ZPB specific method
        //  Arg0:  Instance being queried.
        //  Arg1:  Method Id
        //  Arg2:  Data
        Method (WMOP, 3, NotSerialized)
        {
            If (LEqual (Arg1, One))
            {
                // Power on DGPU
                DGPU_SCOPE._PS0() // IBV_customize: root port path
                // Send notification to OS thru. GPU parent bridge.
                Notify (DGPU_BRIDGE_SCOPE, 0)
                Return (Zero)
            }
            If (LEqual (Arg1, 2))
            {
                // Returns GPU Power state from OPTIMUSCAPS state[4:3]
                If (LEqual (DGPU_SCOPE.DGPS, Zero)) // IBV_customize: root port path
                {
                    Return (0x10)
                }
                Else
                {
                    Return (0x20)
                }
            }
        }
        Method (WQSM, 1)
        {
            Return (ATSM)
        }
        Name (ATSM, Buffer ()
        {
          /* 0000 */    0x52, 0xAA, 0x89, 0xC5, 0x44, 0xCE, 0xC3, 0x3A, 
          /* 0008 */    0x4B, 0x56, 0xE2, 0x00, 0x00, 0x00, 0x01, 0x00, 
          /* 0010 */    0x32, 0x37, 0x34, 0x35, 0x39, 0x31, 0x32, 0x35, 
          /* 0018 */    0x33, 0x36, 0x38, 0x37, 0x47, 0x65, 0x6E, 0x75, 
          /* 0020 */    0x69, 0x6E, 0x65, 0x20, 0x4E, 0x56, 0x49, 0x44, 
          /* 0028 */    0x49, 0x41, 0x20, 0x43, 0x65, 0x72, 0x74, 0x69, 
          /* 0030 */    0x66, 0x69, 0x65, 0x64, 0x20, 0x4F, 0x70, 0x74, 
          /* 0038 */    0x69, 0x6D, 0x75, 0x73, 0x20, 0x52, 0x65, 0x61, 
          /* 0040 */    0x64, 0x79, 0x20, 0x4D, 0x6F, 0x74, 0x68, 0x65, 
          /* 0048 */    0x72, 0x62, 0x6F, 0x61, 0x72, 0x64, 0x20, 0x66, 
          /* 0050 */    0x6F, 0x72, 0x20, 0x63, 0x6F, 0x6F, 0x6B, 0x69, 
          /* 0058 */    0x65, 0x20, 0x66, 0x6F, 0x72, 0x20, 0x75, 0x6C, 
          /* 0060 */    0x35, 0x30, 0x20, 0x75, 0x73, 0x69, 0x6E, 0x20, 
          /* 0068 */    0x2D, 0x20, 0x5E, 0x57, 0x3C, 0x4A, 0x3D, 0x41, 
          /* 0070 */    0x24, 0x4C, 0x3A, 0x4B, 0x38, 0x32, 0x26, 0x51, 
          /* 0078 */    0x48, 0x35, 0x4C, 0x3E, 0x2B, 0x33, 0x52, 0x2B, 
          /* 0080 */    0x54, 0x35, 0x2A, 0x52, 0x29, 0x3A, 0x5B, 0x4C, 
          /* 0088 */    0x4A, 0x3E, 0x36, 0x48, 0x22, 0x48, 0x41, 0x50, 
          /* 0090 */    0x47, 0x39, 0x5A, 0x39, 0x5E, 0x3E, 0x44, 0x53, 
          /* 0098 */    0x54, 0x3C, 0x20, 0x2D, 0x20, 0x43, 0x6F, 0x70, 
          /* 00A0 */    0x79, 0x72, 0x69, 0x67, 0x68, 0x74, 0x20, 0x32, 
          /* 00A8 */    0x30, 0x30, 0x39, 0x20, 0x4E, 0x56, 0x49, 0x44, 
          /* 00B0 */    0x49, 0x41, 0x20, 0x43, 0x6F, 0x72, 0x70, 0x6F, 
          /* 00B8 */    0x72, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x20, 0x41, 
          /* 00C0 */    0x6C, 0x6C, 0x20, 0x52, 0x69, 0x67, 0x68, 0x74, 
          /* 00C8 */    0x73, 0x20, 0x52, 0x65, 0x73, 0x65, 0x72, 0x76, 
          /* 00D0 */    0x65, 0x64, 0x2D, 0x32, 0x37, 0x34, 0x35, 0x39, 
          /* 00D8 */    0x31, 0x32, 0x35, 0x33, 0x36, 0x38, 0x37, 0x28, 
          /* 00E0 */    0x52, 0x29
        })
    }
    Device (WMI1) // placed within PCI Bus scope parallel to iGPU
    {
        Name (_HID, "PNP0C14")
        Name (_UID, "MXM2")
        //
        // For WMI objects that are exposed by the ACPI WMI mapping driver,
        // the GUID matches the GUID in the ACPI _WDG data block,
        // which is required by the ACPI WMI mapping driver.
        //
        Name (_WDG, Buffer(0xB4) {
            // ======================================================================
            // Methods GUID {F6CB5C3C-9CAE-4ebd-B577-931EA32A2CC0}
            // ======================================================================
//            0x3C, 0x5C, 0xCB, 0xF6, 0xAE, 0x9C, 0xBD, 0x4E, 0xB5, 0x77,
//            0x93, 0x1E, 0xA3, 0x2A, 0x2C, 0xC0, 0x4D, 0x58, 0x01, 0x02,
            // ======================================================================
            // NOTIFY_POLICYCHANGE
            // WMI Notify - MXDP/MXPP Policy Change Request D0
            // GUID {921A2F40-0DC4-402d-AC18-B48444EF9ED2}
            // ======================================================================
//            0x40, 0x2F, 0x1A, 0x92, 0xC4, 0x0D, 0x2D, 0x40, 0xAC, 0x18,
//            0xB4, 0x84, 0x44, 0xEF, 0x9E, 0xD2, 0xD0, 0x00, 0x01, 0x08,
            // ======================================================================
            // NVHG_NOTIFY_POLICYSET
            // WMI Notify D9 - Hybrid Policy Set
            // GUID  {C12AD361-9FA9-4C74-901F-95CB0945CF3E}
            // ======================================================================
//            0x61, 0xD3, 0x2A, 0xC1, 0xA9, 0x9F, 0x74, 0x4C, 0x90, 0x1F,
//            0x95, 0xCB, 0x09, 0x45, 0xCF, 0x3E, 0xD9, 0x00, 0x01, 0x08,
            // ======================================================================
            // NVHG_DISPLAY_SCALING
            // Notify event DB - Display scaling change
            // GUID {42848006-8886-490E-8C72-2BDCA93A8A09}
            // ======================================================================
            0x06, 0x80, 0x84, 0x42, 0x86, 0x88, 0x0E, 0x49, 0x8C, 0x72,
            0x2B, 0xDC, 0xA9, 0x3A, 0x8A, 0x09, 0xDB, 0x00, 0x01, 0x08,
            // ======================================================================
            // DISPLAY_HOTKEY, ACPI_NOTIFY_PANEL_SWITCH GUID
            // Notify event 80 (fixed) - Hot-Key, use _DGS, _DCS .. etc.
            // GUID {E06BDE62-EE75-48F4-A583-B23E69ABF891}
            // ======================================================================
            0x62, 0xDE, 0x6B, 0xE0, 0x75, 0xEE, 0xF4, 0x48, 0xA5, 0x83,
            0xB2, 0x3E, 0x69, 0xAB, 0xF8, 0x91, 0x80, 0x00, 0x01, 0x08,
            // ======================================================================
            // DISPLAY_HOTplug, ACPI_NOTIFY_DEVICE_HOTPLUG
            // Notify event 81 (fixed) - Hot-Plug, query _DCS
            // GUID {3ADEBD0F-0C5F-46ED-AB-2E-04-96-2B-4F-DC-BC}
            // ======================================================================
            0x0F, 0xBD, 0xDE, 0x3A, 0x5F, 0x0C, 0xED, 0x46, 0xAB, 0x2E,
            0x04, 0x96, 0x2B, 0x4F, 0xDC, 0xBC, 0x81, 0x00, 0x01, 0x08,
            // ======================================================================
            // BRIGHTNESS_INC, ACPI_NOTIFY_INC_BRIGHTNESS_HOTKEY
            // Notify event 86 (fixed) - Backlight Increase
            // GUID {1E519311-3E75-4208-B05E-EBE17E3FF41F}
            // ======================================================================
            0x11, 0x93, 0x51, 0x1E, 0x75, 0x3E, 0x08, 0x42, 0xB0, 0x5E,
            0xEB, 0xE1, 0x7E, 0x3F, 0xF4, 0x1F, 0x86, 0x00, 0x01, 0x08,
            // ======================================================================
            // BRIGHTNESS_DEC, ACPI_NOTIFY_DEC_BRIGHTNESS_HOTKEY
            // Notify event 87 (fixed) - Backlight Decrease
            // GUID {37F85341-4418-4F24-8533-38FFC7295542}
            // ======================================================================
            0x41, 0x53, 0xF8, 0x37, 0x18, 0x44, 0x24, 0x4F, 0x85, 0x33,
            0x38, 0xFF, 0xC7, 0x29, 0x55, 0x42, 0x87, 0x00, 0x01, 0x08,
            // ======================================================================
            // MOF data {05901221-D566-11d1-B2F0-00A0C9062910}
            // ======================================================================
//            0x21, 0x12, 0x90, 0x05, 0x66, 0xD5, 0xD1, 0x11, 0xB2, 0xF0,
//            0x00, 0xA0, 0xC9, 0x06, 0x29, 0x10, 0x58, 0x4D, 0x01, 0x00
        })

        Method (WMMX, 3, NotSerialized)
        {
            CreateDwordField (Arg2, 0, FUNC)  // Get the function name
//            If (LEqual (FUNC, 0x4D53445F))  // "_DSM"
//            {
//                If (LNot (LLess (SizeOf (Arg2), 0x1C)))
//                {
//                    CreateField (Arg2, Zero, 0x80, MUID)
//                    CreateDwordField (Arg2, 0x10, REVI)
//                    CreateDwordField (Arg2, 0x14, SFNC)
//                    CreateField (Arg2, 0xE0, 0x20, XRG0)
//                    If (LNot (LEqual (Arg1, 0x10)))
//                    {
//                        Return (IGPU_SCOPE._DSM (MUID, REVI, SFNC, XRG0))
//                    }
//                }
//            }
//            Else
//            {
            If (LEqual (FUNC, 0x584D584D))  // "MXMX"
            {
                CreateDwordField (Arg2, 0x08, XRG1)
                If (LEqual (Arg1, 0x10))
                {
                    Return (IGPU_SCOPE.MXMX (XRG1))
                }
                Else
                {
                    Return (DGPU_SCOPE.MXMX (XRG1))
                }
            }
            Else
            {
                If (LEqual (FUNC, 0x5344584D))  // "MXDS"
                {
                    CreateDwordField (Arg2, 0x08, XRG2)
                    If (LEqual (Arg1, 0x10))
                    {
                        Return (IGPU_SCOPE.MXDS (XRG2))
                    }
                    Else
                    {
                        Return (DGPU_SCOPE.MXDS (XRG2))
                    }
                }
            }
//            }
            Return (Zero)
        }

        Name (WQXM, Buffer (0x029C)
        {
            /* 0000 */    0x46, 0x4F, 0x4D, 0x42, 0x01, 0x00, 0x00, 0x00, 
            /* 0008 */    0x8B, 0x02, 0x00, 0x00, 0x0C, 0x08, 0x00, 0x00, 
            /* 0010 */    0x44, 0x53, 0x00, 0x01, 0x1A, 0x7D, 0xDA, 0x54, 
            /* 0018 */    0x18, 0xD2, 0x83, 0x00, 0x01, 0x06, 0x18, 0x42, 
            /* 0020 */    0x10, 0x05, 0x10, 0x8A, 0xE6, 0x80, 0x42, 0x04, 
            /* 0028 */    0x92, 0x43, 0xA4, 0x30, 0x30, 0x28, 0x0B, 0x20, 
            /* 0030 */    0x86, 0x90, 0x0B, 0x26, 0x26, 0x40, 0x04, 0x84, 
            /* 0038 */    0xBC, 0x0A, 0xB0, 0x29, 0xC0, 0x24, 0x88, 0xFA, 
            /* 0040 */    0xF7, 0x87, 0x28, 0x09, 0x0E, 0x25, 0x04, 0x42, 
            /* 0048 */    0x12, 0x05, 0x98, 0x17, 0xA0, 0x5B, 0x80, 0x61, 
            /* 0050 */    0x01, 0xB6, 0x05, 0x98, 0x16, 0xE0, 0x18, 0x92, 
            /* 0058 */    0x4A, 0x03, 0xA7, 0x04, 0x96, 0x02, 0x21, 0xA1, 
            /* 0060 */    0x02, 0x94, 0x0B, 0xF0, 0x2D, 0x40, 0x3B, 0xA2, 
            /* 0068 */    0x24, 0x0B, 0xB0, 0x0C, 0x23, 0x02, 0x8F, 0x82, 
            /* 0070 */    0xA1, 0x71, 0x68, 0xEC, 0x30, 0x2C, 0x13, 0x4C, 
            /* 0078 */    0x83, 0x38, 0x8C, 0xB2, 0x91, 0x45, 0x60, 0xDC, 
            /* 0080 */    0x4E, 0x05, 0xC8, 0x15, 0x20, 0x4C, 0x80, 0x78, 
            /* 0088 */    0x54, 0x61, 0x34, 0x07, 0x45, 0xE0, 0x42, 0x63, 
            /* 0090 */    0x64, 0x40, 0xC8, 0xA3, 0x00, 0xAB, 0xA3, 0xD0, 
            /* 0098 */    0xA4, 0x12, 0xD8, 0xBD, 0x00, 0x8D, 0x02, 0xB4, 
            /* 00A0 */    0x09, 0x70, 0x28, 0x40, 0xA1, 0x00, 0x6B, 0x18, 
            /* 00A8 */    0x72, 0x06, 0x21, 0x5B, 0xD8, 0xC2, 0x68, 0x50, 
            /* 00B0 */    0x80, 0x45, 0x14, 0x8D, 0xE0, 0x2C, 0x2A, 0x9E, 
            /* 00B8 */    0x93, 0x50, 0x02, 0xDA, 0x1B, 0x82, 0xF0, 0x8C, 
            /* 00C0 */    0xD9, 0x18, 0x9E, 0x10, 0x83, 0x54, 0x86, 0x21, 
            /* 00C8 */    0x88, 0xB8, 0x11, 0x8E, 0xA5, 0xFD, 0x41, 0x10, 
            /* 00D0 */    0xF9, 0xAB, 0xD7, 0xB8, 0x1D, 0x69, 0x34, 0xA8, 
            /* 00D8 */    0xB1, 0x26, 0x38, 0x76, 0x8F, 0xE6, 0x84, 0x3B, 
            /* 00E0 */    0x17, 0x20, 0x7D, 0x6E, 0x02, 0x39, 0xBA, 0xD3, 
            /* 00E8 */    0xA8, 0x73, 0xD0, 0x64, 0x78, 0x0C, 0x2B, 0xC1, 
            /* 00F0 */    0x7F, 0x80, 0x4F, 0x01, 0x78, 0xD7, 0x80, 0x9A, 
            /* 00F8 */    0xFE, 0xC1, 0x33, 0x41, 0x70, 0xA8, 0x21, 0x7A, 
            /* 0100 */    0xD4, 0xE1, 0x4E, 0xE0, 0xBC, 0x8E, 0x84, 0x41, 
            /* 0108 */    0x1C, 0xD1, 0x71, 0x63, 0x67, 0x75, 0x32, 0x07, 
            /* 0110 */    0x5D, 0xAA, 0x00, 0xB3, 0x07, 0x00, 0x0D, 0x2E, 
            /* 0118 */    0xC1, 0x69, 0x9F, 0x49, 0xE8, 0xF7, 0x80, 0xF3, 
            /* 0120 */    0xE9, 0x79, 0x6C, 0x6C, 0x10, 0xA8, 0x91, 0xF9, 
            /* 0128 */    0xFF, 0x0F, 0xED, 0x41, 0x9E, 0x56, 0xCC, 0x90, 
            /* 0130 */    0xCF, 0x02, 0x87, 0xC5, 0xC4, 0x1E, 0x19, 0xE8, 
            /* 0138 */    0x78, 0xC0, 0x7F, 0x00, 0x78, 0x34, 0x88, 0xF0, 
            /* 0140 */    0x66, 0xE0, 0xF9, 0x9A, 0x60, 0x50, 0x08, 0x39, 
            /* 0148 */    0x19, 0x0F, 0x4A, 0xCC, 0xF9, 0x80, 0xCC, 0x25, 
            /* 0150 */    0xC4, 0x43, 0xC0, 0x31, 0xC4, 0x08, 0x7A, 0x46, 
            /* 0158 */    0x45, 0x23, 0x6B, 0x22, 0x3E, 0x03, 0x78, 0xDC, 
            /* 0160 */    0x96, 0x05, 0x42, 0x09, 0x0C, 0xEC, 0x73, 0xC3, 
            /* 0168 */    0x3B, 0x84, 0x61, 0x71, 0xA3, 0x09, 0xEC, 0xF3, 
            /* 0170 */    0x85, 0x05, 0x0E, 0x0A, 0x05, 0xEB, 0xBB, 0x42, 
            /* 0178 */    0xCC, 0xE7, 0x81, 0xE3, 0x3C, 0x60, 0x0B, 0x9F, 
            /* 0180 */    0x28, 0x01, 0x3E, 0x24, 0x8F, 0x06, 0xDE, 0x20, 
            /* 0188 */    0xE1, 0x5B, 0x3F, 0x02, 0x10, 0xE0, 0x27, 0x06, 
            /* 0190 */    0x13, 0x58, 0x1E, 0x30, 0x7A, 0x94, 0xF6, 0x2B, 
            /* 0198 */    0x00, 0x21, 0xF8, 0x8B, 0xC5, 0x53, 0xC0, 0xEB, 
            /* 01A0 */    0x40, 0x84, 0x63, 0x81, 0x29, 0x72, 0x6C, 0x68, 
            /* 01A8 */    0x78, 0x7E, 0x70, 0x88, 0x1E, 0xF5, 0x5C, 0xC2, 
            /* 01B0 */    0x1F, 0x4D, 0x94, 0x53, 0x38, 0x1C, 0x1F, 0x39, 
            /* 01B8 */    0x8C, 0x10, 0xFE, 0x49, 0xE3, 0xC9, 0xC3, 0x9A, 
            /* 01C0 */    0xEF, 0x00, 0x9A, 0xD2, 0x5B, 0xC0, 0xFB, 0x83, 
            /* 01C8 */    0x47, 0x80, 0x11, 0x20, 0xE1, 0x68, 0x82, 0x89, 
            /* 01D0 */    0x7C, 0x3A, 0x01, 0xD5, 0xFF, 0xFF, 0x74, 0x02, 
            /* 01D8 */    0xB8, 0xBA, 0x01, 0x14, 0x37, 0x6A, 0x9D, 0x49, 
            /* 01E0 */    0x7C, 0x2C, 0xF1, 0xAD, 0xE4, 0xBC, 0x43, 0xC5, 
            /* 01E8 */    0x7F, 0x93, 0x78, 0x3A, 0xF1, 0x34, 0x1E, 0x4C, 
            /* 01F0 */    0x42, 0x44, 0x89, 0x18, 0x21, 0xA2, 0xEF, 0x27, 
            /* 01F8 */    0x46, 0x08, 0x15, 0x31, 0x6C, 0xA4, 0x37, 0x80, 
            /* 0200 */    0xE7, 0x13, 0xE3, 0x84, 0x08, 0xF4, 0x74, 0xC2, 
            /* 0208 */    0x42, 0x3E, 0x34, 0xA4, 0xE1, 0x74, 0x02, 0x50, 
            /* 0210 */    0xE0, 0xFF, 0x7F, 0x3A, 0x81, 0x1F, 0xF5, 0x74, 
            /* 0218 */    0x82, 0x1E, 0xAE, 0x4F, 0x19, 0x18, 0xE4, 0x03, 
            /* 0220 */    0xF2, 0xA9, 0xC3, 0xF7, 0x1F, 0x13, 0xF8, 0x78, 
            /* 0228 */    0xC2, 0x45, 0x1D, 0x4F, 0x50, 0xA7, 0x07, 0x1F, 
            /* 0230 */    0x4F, 0xD8, 0x19, 0xE1, 0x2C, 0x1E, 0x03, 0x7C, 
            /* 0238 */    0x3A, 0xC1, 0xDC, 0x13, 0x7C, 0x3A, 0x01, 0xDB, 
            /* 0240 */    0x68, 0x60, 0x1C, 0x4F, 0xC0, 0x77, 0x74, 0xC1, 
            /* 0248 */    0x1D, 0x4F, 0xC0, 0x30, 0x18, 0x18, 0xE7, 0x13, 
            /* 0250 */    0xE0, 0x31, 0x5E, 0xDC, 0x31, 0xC0, 0x43, 0xE0, 
            /* 0258 */    0x03, 0x78, 0xDC, 0x38, 0x3D, 0x2B, 0x9D, 0x14, 
            /* 0260 */    0xF2, 0x24, 0xC2, 0x07, 0x85, 0x39, 0xB0, 0xE0, 
            /* 0268 */    0x14, 0xDA, 0xF4, 0xA9, 0xD1, 0xA8, 0x55, 0x83, 
            /* 0270 */    0x32, 0x35, 0xCA, 0x34, 0xA8, 0xD5, 0xA7, 0x52, 
            /* 0278 */    0x63, 0xC6, 0xCE, 0x19, 0x0E, 0xF8, 0x10, 0xD0, 
            /* 0280 */    0x89, 0xC0, 0xF2, 0x9E, 0x0D, 0x02, 0xB1, 0x0C, 
            /* 0288 */    0x0A, 0x81, 0x58, 0xFA, 0xAB, 0x45, 0x20, 0x0E, 
            /* 0290 */    0x0E, 0xA2, 0xFF, 0x3F, 0x88, 0x23, 0xD2, 0x0A, 
            /* 0298 */    0xC4, 0xFF, 0x7F, 0x7F
        })
    }
}
