/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*
    == Readme ==
    For NVOP support, SBIOS has to adjust below code to meet platform:
    - Modify the method path of \_SB.PCI0.PGON and \_SB.PCI0.PGOF to Intel RC dGPU ON/OFF method
*/

Scope (DGPU_SCOPE)
{
    Name (VGAB, Buffer(0xFB)
    {
        0x00
    })

    Method (_PS0, 0)
    {
        If (LNotEqual (DGPS, Zero))
        {
//[-start-211012-QINGLIN0093-modify]//
//[-start-211013-GEORGE0012-modify]//
#if defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211013-GEORGE0012-modify]//
            DGPU_BRIDGE_SCOPE.PXP._ON() 
#else
            If (CondRefOf (DGPU_BRIDGE_SCOPE.PON)) // Using PON exist or not to decide whether it is PEG case.
            {
                DGPU_BRIDGE_SCOPE.PON () //IBV_customize: call to dGPU ON method
            }
#endif
//[-end-211012-QINGLIN0093-modify]//
            If (CondRefOf (PCI_SCOPE.HGON)) // Using HGON exist or not to decide whether it is PCH case.
            {
                PCI_SCOPE.HGON () //IBV_customize: call to dGPU ON method
            }
            If (LNotEqual (GPRF, One))
            {
                Store (VGAB, VGAR)
            }
            Store (Zero, DGPS)
//            Store (Zero, DGPU_SCOPE.HDAE) // disable dGPU HDA device in _PS0 anyway
        }
    }

    Method (_PS3, 0)
    {
        If (LEqual (OMPR, 0x3))
        {
            If (LNotEqual (GPRF, One))
            {
                Store (VGAR, VGAB)
            }
//[-start-211012-QINGLIN0093-modify]//
//[-start-211013-GEORGE0012-modify]//
#if defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211013-GEORGE0012-modify]//
            DGPU_BRIDGE_SCOPE.PXP._OFF() 
#else
            If (CondRefOf (DGPU_BRIDGE_SCOPE.POFF)) // Using POFF exist or not to decide whether it is PEG case.
            {
                DGPU_BRIDGE_SCOPE.POFF () // IBV_customize: call to dGPU OFF method
            }
#endif
//[-end-211012-QINGLIN0093-modify]//
            If (CondRefOf (PCI_SCOPE.HGOF)) // Using HGOF exist or not to decide whether it is PCH case.
            {
                PCI_SCOPE.HGOF () // IBV_customize: call to dGPU OFF method
            }
            Store (One, DGPS)
            Store (0x2, OMPR)
        }
    }

    Method (GOBT, 1)
    {
        Name (OPVK, Buffer()
        {
        // Key below is *NOT A REAL KEY*, it is for reference
        // Customer need to ask NVIDIA PM to get the key
        // Customer need to put the key in between labels "// key start -" and
        //  "// key end -". Please consult NVIDIA PM if any issues

        // Key start -
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00
        // Key end -
        })

        CreateWordField (Arg0, 2, USRG)  // Object type signature passed in by driver.

        If (LEqual (USRG, 0x564B)) {     // 'VK' for Optimus Validation Key Object.
           Return (OPVK)
        }
        Return (Zero)
    }

    Name (DGPS, Zero)
    Name (OMPR, 0x2) // Optimus MXM Power-Control Ready
    Name (GPRF, Zero)
    Name (DPST, One)

    Method (NVOP, 4, Serialized)
    {
        // Only Interface Revision 0x0100 is supported
        If (LNotEqual (Arg1, 0x100))
        {
            Return (STATUS_ERROR_UNSPECIFIED)
        }
        // (Arg2) Sub-Function
        Switch (ToInteger (Arg2))
        {
            //
            // Function 0: NVOP_FUNC_SUPPORT - Bit list of supported functions.
            //
            case (NVOP_FUNC_SUPPORT)
            {
/*
                Store (Buffer(4) {0, 0, 0, 0}, Local0)

                Divide (NVOP_FUNC_SUPPORT, 8, Local2, Local1) // Function 0
                // Local1 is Quotient, Local2 is Remainder
                ShiftLeft (0x01, Local2, Local2)
                Or (DeRefOf (Index (Local0, Local1)), Local2, Index (Local0, Local1))

                Divide (NVOP_FUNC_OPTIMUSCAPS, 8, Local2, Local1) // Function 0x1A
                ShiftLeft (0x01, Local2, Local2)
                Or (DeRefOf (Index (Local0, Local1)), Local2, Index (Local0, Local1))

                // force IGPU
                Divide (NVOP_FUNC_OPTIMUSFLAGS, 8, Local2, Local1) // Function 0x1B
                ShiftLeft (0x01, Local2, Local2)
                Or( DeRefOf(Index(Local0, Local1)), Local2, Index(Local0, Local1))

                // Force IGPU
                Divide (NVOP_FUNC_GETOBJBYTYPE, 8, Local2, Local1) // Function 0x10
                ShiftLeft (0x01, Local2, Local2)
                Or (DeRefOf (Index(Local0, Local1)), Local2, Index (Local0, Local1))

                Divide (NVOP_FUNC_MDTL, 8, Local2, Local1) // Function 0x06
                ShiftLeft(0x01, Local2, Local2)
                Or (DeRefOf (Index (Local0, Local1)), Local2, Index (Local0, Local1))

                Divide (NVOP_FUNC_DISPLAYSTATUS, 8, Local2, Local1) // Function 0x05
                ShiftLeft (0x01, Local2, Local2)
                Or (DeRefOf (Index (Local0, Local1)), Local2, Index (Local0, Local1))

                Return (Local0)
*/
                Return (Buffer(0x04)
                {
                    // sub-func: 0,26,27 supported
                    0x01, 0x00, 0x00, 0x0C
                })
            }
/*
            //
            // Function 5: NVOP_FUNC_DISPLAYSTATUS - Query the Display Hot-Key.
            //
            case (NVOP_FUNC_DISPLAYSTATUS)
            {
                 CreateField (Arg3, 31,  1, NCSM) // Check the next combination sequence mask bit 31.

                 Store (0, Local0)
                 if (LEqual (ToInteger (NCSM), 0x1))
                 {
                     CreateField (Arg3, 25, 5,NCIN) // Get the next combination sequence number bit 29:25
                     Store (ToInteger (NCIN), DPST) // Store the current valid value from driver
                     Add (DPST, 1, DPST) // Add 1 to it because SBIOS needs to give the next combination sequence
                     Mod (DPST, 18, Local0)
                 }
                 else
                 {
                     if (LEqual (DPST, 0x0))
                     {
                         // SBIOS sequence number should not return index 0
                         Add (DPST, 1, DPST)
                     }
                     // Just for testing, display status is implemented without the logic of attach and active display
                     Mod (DPST, 18, Local0)
                     ShiftLeft (Local0, 0x8, Local0) // Shift left 8 bits because display status is between bit 8:13
                     Add (DPST, 1, DPST)
                 }

                 Return (Local0)
            }

            //
            // Function 6: NVOP_FUNC_MDTL - Query Display Toggle List.
            //
            case (NVOP_FUNC_MDTL)
            {
                // Display Toggle List
                Name (TMP6, Package()
                {
                    ones, 0x2C,        // LVDS
                    ones, 0x2C,        // CRT
                    ones, 0x2C,        // HDMI
                    ones, ones, 0x2C,  // LVDS + CRT
                    ones, ones, 0x2C,  // LVDS + HDMI
                    ones, ones, 0x2C   // CRT  + HDMI
                })

                // Update Display Toggle List
                Store (DID2, Index (TMP6, 0))  // LVDS
                Store (DID1, Index (TMP6, 2))  // CRT
                Store (DID4, Index (TMP6, 4))  // HDMI

                Store (DID2, Index (TMP6, 6))  // LVDS + CRT
                Store (DID1, Index (TMP6, 7))

                Store (DID2, Index (TMP6, 9))  // LVDS + HDMI
                Store (DID4, Index (TMP6, 10))

                Store (DID1, Index (TMP6, 12)) // CRT + HDMI
                Store (DID4, Index (TMP6, 13))

                Return (TMP6)
            }

            //
            // Function 16: NVOP_FUNC_GETOBJBYTYPE - Get Data Object.
            //
            case (NVOP_FUNC_GETOBJBYTYPE)
            {
                Return (DGPU_SCOPE.GOBT (Arg3))
            }
*/
            //
            // Function 26: NVOP_FUNC_OPTIMUSCAPS - Optimus Capabilities.
            //
            case (NVOP_FUNC_OPTIMUSCAPS)
            {
                CreateField (Arg3,  0, 1, FLCH)
                CreateField (Arg3,  1, 1, DVSR) // Modify Optimus DSM 0x1A for GC6 TDR support.
                CreateField (Arg3,  2, 1, DVSC) // Modify Optimus DSM 0x1A for GC6 TDR support.
                CreateField (Arg3, 24, 2, OPCE)

                If (LAnd (ToInteger (FLCH), LNotEqual (ToInteger (OPCE), OMPR)))
                {
                    Store (ToInteger (OPCE), OMPR) // Optimus Power Control Enable - From DD
                }

                // Definition of return buffer.
                // bit 0     - Optimus Enabled
                //             0 : Optimus Graphics Disabled
                //             1 : Optimus Graphics Enabled (default)
                // bit 4:3   - Current GPU Control Status
                //             0 : GPU is powered off
                //             3 : GPU power has stabilized (default)
                // bit 6     - Shared discrete GPU Hot-Plug Capabilities
                //             1 : There are discrete GPU Display Hot-Plug signals co-connected to the platform
                // bit 8     - PCIe Configuration Space Owner Actual
                //             0 : SBIOS
                //             1 : GPU Driver
                // bit 26:24 - Optimus Capabilities
                //             0 : No special platform capabilities
                //             1 : Platform has dynamic GPU power control
                // bit 27:28 - Optimus HD Audio Codec Capabilities
                //             0 : No audio codec-related capabilities
                //             1 : Platform does not use HD audio
                //             2 : Platform supports Optimus dynamic codec control
                Store (Buffer(4) {0, 0, 0, 0}, Local0)
                CreateField (Local0,  0, 1, OPEN)
                CreateField (Local0,  3, 2, CGCS)
                CreateField (Local0,  6, 1, SHPC) // Shared discrete GPU Hot-Plug Capabilities
                CreateField (Local0,  8, 1, SNSR) // Modify Optimus DSM 0x1A for GC6 TDR support.
                CreateField (Local0, 24, 3, DGPC) // Optimus Power Capabilities
                CreateField (Local0, 27, 2, OHAC) // Optimus HD Audio Codec Capabilities

                Store (One, OPEN) // Optimus Enabled
                Store (One, SHPC) // Set '1' indicates there are discrete GPU Display Hot-Plug signals co-connected to the platform
                Store (One, DGPC) // Optimus Power Capabilities
                                  // 0: No special platform capabilities
                                  // 1: Dynamic GPU Power Control
                Store (0x3, OHAC) // Optimus HD Audio Codec Capabilities
                                  // 0: No audio codec-related capabilities
                                  // 1: Platform does not use HD audio (SBIOS will always disable audio codecs)
                                  // 2: Optimus dynamic codec control
                                  // 3: Dynamic power state reporting

                If (ToInteger (DVSC))
                {
                    If (ToInteger (DVSR))
                    {
                        Store (One, GPRF)
                    }
                    Else
                    {
                        Store (Zero, GPRF)
                    }
                }
                Store (GPRF, SNSR)

                If (LEqual (DGPS, Zero))
                {
                    Store (0x3, CGCS) // Current GPU Control status: On
                }
                ELSE
                {
                    Store (0x0, CGCS) // Current GPU Control status: Off
                }
                Return (Local0)
            }

            //
            // Function 27: NVOP_FUNC_OPTIMUSFLAGS - Optimus State flags.
            //
            case (NVOP_FUNC_OPTIMUSFLAGS)
            {
//                Store (Arg3, Local0)
//                CreateField (Local0, 0, 1, OPFL)
//                CreateField (Local0, 1, 1, OPVL)
//                If (ToInteger (OPVL))
//                {
//                    Store (Zero, OPTF)
//                    If (ToInteger (OPFL))
//                    {
//                        Store (One, OPTF)
//                    }
//                }
//                Store (OPTF, Local0)
//                Return (Local0)
                CreateField (Arg3, 0x00, 1, OACC) // Optimus Audio Codec Control
                CreateField (Arg3, 0x01, 1, UOAC) // Update Optimus Audio Codec Control
                CreateField (Arg3, 0x02, 8, OPDA) // Count of the number of applications running on the dGPU
                CreateField (Arg3, 0x0A, 1, OPDE) // Indicate bit 9:2 valid or not

                Store (Zero, Local1)
                Store (DGPU_SCOPE.HDAE, Local1) // Returns the currnet status of the audio device
                Return (Local1)
            }

            default
            {
                //
                // FunctionCode or SubFunctionCode not supported
                //
                Return (STATUS_ERROR_UNSUPPORTED)
            }
        }
    }
}