/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*
    == Readme ==
    For NBCI support, SBIOS has to adjust below code to meet platform:
    - Fill GSV1, GSV2(if need) and GSDR buffer from NVIDIA AE
    - \_SB.PCI0.PEG0.DEID -> Device ID of Host Bridge
    - Customer must update 'GSDR' for G-Sync platform (and correspondng VK key)
    - Please clear 'GSDR' for N17x (and earlier) platform
*/

Scope (DGPU_SCOPE)
{
    // NVIDIA NBCI related start
    Name (GSV1, Buffer(0x01)                 // Buffer for VK key#1
    {
        0x00
    })
    Name (GSV2, Buffer(0x01)                 // Buffer for VK key#2 (Optional)
    {
        0x00
    })
    Name(GSDR, Buffer(0xA1)                  // Buffer for DR key
    {
        0x57, 0x74, 0xDC, 0x86, 0x75, 0x84, 0xEC, 0xE7, 
        0x52, 0x44, 0xA1, 0x00, 0x00, 0x00, 0x00, 0x01, 
        0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00, 
        0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00, 
        0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00, 
        0x51, 0x00, 0x00, 0x00, 0x04, 0x00, 0x4F, 0x00, 
        0x00, 0x00, 0x05, 0x00, 0x4D, 0x00, 0x00, 0x00, 
        0x06, 0x00, 0x4B, 0x00, 0x00, 0x00, 0x07, 0x00, 
        0x49, 0x00, 0x00, 0x00, 0x08, 0x00, 0x47, 0x00, 
        0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C, 
        0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 
        0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00, 
        0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01, 
        0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00, 
        0x00, 0x01, 0x00, 0x00, 0x00, 0x60, 0x68, 0x9E, 
        0x35, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00
    })

    Method (NBCI, 4, Serialized)
    {
        If (LNotEqual (Arg1, 0x0102))
        {
            Return (STATUS_ERROR_UNSUPPORTED)
        }
        //
        // Sub-Function 0: NBCI_FUNC_SUPPORT
        //
        If (LEqual (Arg2, NBCI_FUNC_SUPPORT))
        {
            Return (Buffer (0x04)
            {
                0x03, 0x00, 0x11, 0x00                // sub-func: 0,1,16,20 supported
            })
        }
        //
        // Sub-Function 1: NBCI_FUNC_PLATCAPS
        //
        If (LEqual (Arg2, NBCI_FUNC_PLATCAPS))
        {
            Name (TEMP, Buffer (0x04)
            {
                0x00, 0x00, 0x00, 0x00
            })
            CreateDWordField (TEMP, Zero, STS0)
            Or (STS0, Zero, STS0)
            Return (TEMP)
        }
        //
        // Sub-Function 16: NBCI_FUNC_GETOBJBYTYPE
        //
        If (LEqual (Arg2, NBCI_FUNC_GETOBJBYTYPE))
        {
            CreateWordField (Arg3, 0x02, BFF0)
            If (LEqual (BFF0, 0x564B))                // Check VK object
            {
                // OEM_customize: VK Key is an object per panel/GPU/HostBridge. OEM SBIOS need to check corresponding info then pick correct VK object to return.
//                If (LEqual (DEID, 0x0D04))          // Example: VK for host bridge DID == 0x0D04
//                {
//                    Return (GSV1)
//                }
//                If (LEqual (DEID, 0x0C04))          // Example: VK for host bridge DID == 0x0C04 (if need)
//                {
//                    Return (GSV2)
//                }
            }
            If (LEqual (BFF0, 0x4452)) // Check DR object
            {
                // OEM_customize: DR Key is an data Object which might vary on different eDP panel. OEM SBIOS need to check corresponding info then pick correct DR object to return.
                Return (GSDR)
            }
        }
        //
        // Sub-Function 20: NBCI_FUNC_GETBACKLIGHT
        //
        If (LEqual (Arg2, NBCI_FUNC_GETBACKLIGHT))
        {
//            If (LEqual (EDPE, One))
//            {
//                Return (Package() {
//                            ACPI_ID_EDP,
//                            0x00000200, // PWM + Driver API
//                            0x00000000, // i2c address (n/a)
//                            0x00000000, // controller type (n/a)
//                            0x00000004, // backlight type
//                            0x00000001, // one entry
//                            // first entry
//                            200, // 200 Hz
//                            // Follow AW13
//                            // According to 3D panel SPEC, the minimum duty cycle will be 10%.
//                            // Modify Min Duty Cycle to avoid panel luminance  equal to zero issue.
//                            100, 1000, // 0 - 100.0%
//                            0x9,       // Number of PWM settings N for Luminance curve
//                            // N-array of DutyCycle settings in 1/10%
//                            0, 120, 240, 360, 480, 600, 720, 840, 1000,
//                            // N-array of relative luminance in 1/10%
//                            0, 120, 240, 360, 480, 600, 720, 840, 1000,
//                        })
//            }
//            Else
//            {
//                Return (Package() {
//                            ACPI_ID_LCD,
//                            0x00000200, // PWM + Driver API
//                            0x00000000, // i2c address (n/a)
//                            0x00000000, // controller type (n/a)
//                            0x00000004, // backlight type - CCFL (includes luminance), 
//                                        // 0D continuous backlight, 
//                                        // 1 backlight, Regular PWM
//                            0x00000001, // Number of entries - 1
//                            200,        // PWM Base Frequency in HZ - 200 Hz
//                            //  Follow AW13
//                            //  Modify Min Duty Cycle to avoid panel luminance  equal to zero issue.
//                            60, 1000,   // 0 - 100.0%; Min Duty Cycle - 60.0%;Max Duty Cycle - 100.0%
//                            // Start of luminance curve
//                            0x9,        // Number of PWM settings N for Luminance curve
//                            // N-array of DutyCycle settings in 1/10%
//                            0, 120, 240, 360, 480, 600, 720, 840, 1000,
//                            // N-array of relative luminance in 1/10%
//                            0, 120, 240, 360, 480, 600, 720, 840, 1000,
//                        })
//            }
            // OEM_customize: Package returned in this interface is panel related. OEM SBIOS need to follow definition in NBSI Spec to reflect real panel feature in this pacakge.
            Return (Package (32)
            {
                0x8001A450, 0x0200, Zero, Zero, 0x05, One, 0xC8, 0x32, 0x03E8, 0x0B,
                0x32, 0x64, 0x96, 0xC8, 0x012C, 0x0190, 0x01FE, 0x0276, 0x02F8, 0x0366,
                0x03E8, Zero, 0x64, 0xC8, 0x012C, 0x0190, 0x01F4, 0x0258, 0x02BC,
                0x0320, 0x0384, 0x03E8
            })
        }
//        //
//        // Sub-Function 22: NBCI_FUNC_GETLICENSE
//        //
//        If (LEqual (Arg2, NBCI_FUNC_GETLICENSE))
//        {
//            //
//            // 3D Vision License
//            //
//            If (ToInteger (Arg3))
//            {
//                If (LEqual (PDID, N13E_GS1_DID)) // N13E-GS1
//                {
//                    Return (Package() {N13E_GS1_LICENSE_VECTOR})
//                }
//                ElseIf (LEqual (PDID, N13E_GE_DID)) // N13E-GE
//                {
//                    Return (Package() {N13E_GE_LICENSE_VECTOR})
//                }
//                ElseIf (LEqual (PDID, N13E_GTX_DID)) // N13E-GTX
//                {
//                    Return (Package() {N13E_GTX_LICENSE_VECTOR})
//                }
//                Else
//                {
//                    Return (Package() {TEST_LICENSE_VECTOR})
//                }
//            }
//        }
    }
    // NVIDIA NBCI related end
}
