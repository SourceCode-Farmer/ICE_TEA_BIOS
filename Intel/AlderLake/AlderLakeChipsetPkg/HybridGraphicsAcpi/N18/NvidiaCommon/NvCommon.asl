/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*
    == Readme ==
    For NVIDIA general support, SBIOS has to set below GPIO config:
    - Please search keyword: IBV_customize and OEM_customize to match platform design
*/

Scope (DGPU_BRIDGE_SCOPE)
{
    OperationRegion (RPCX, SystemMemory, PCI_SCOPE.DGBA, 0x1000) // PEG Root Port.
    Field (RPCX, DWordAcc, NoLock, Preserve)
    {
        Offset (0x04),
        CMDR,  8,       // Command Register
        Offset (0x19),
        PRBN,  8,       // Primary Bus Number
        Offset (0x4A),  // Device Status Register
        CEDR,  1,
        Offset (0x69),  // Device Control2 Register
            ,  2,
        LREN,  1,
        Offset (0xA4),
        D0ST,  2,
    }

    Device (NXHC)
    {
        Name (_ADR, 0x02)
        Device (NHUB)
        {
            Name (_ADR, Zero)
            Device (SS01)
            {
                Name (_ADR, One)
                Method (_UPC, 0, Serialized)
                {
                    Name (NUPC, Package(4) {Zero, 0xFF, Zero, Zero}) // byte0: Connectable=no, byte1: Type=Proprietary
                    Return (NUPC)
                }
                Method (_PLD) { Return (NPLD (0, 1)) }
            }
            Device (SS02)
            {
                Name (_ADR, 0x02)
                Method (_UPC, 0, Serialized)
                {
                    Name (NUPC, Package(4) {Zero, 0xFF, Zero, Zero}) // byte0: Connectable=no, byte1: Type=Proprietary
                    Return (NUPC)
                }
                Method (_PLD) { Return (NPLD (0, 2)) }
            }
            Device (SS03)
            {
                Name (_ADR, 0x03)
                Method (_UPC, 0, Serialized)
                {
                    Name (NUPC, Package(4) {Zero, 0xFF, Zero, Zero}) // byte0: Connectable=no, byte1: Type=Proprietary
                    Return (NUPC)
                }
                Method (_PLD) { Return (NPLD (0, 3)) }
            }
            Device (SS04)
            {
                Name (_ADR, 0x04)
                Method (_UPC, 0, Serialized)
                {
                    Name (NUPC, Package(4) {Zero, 0xFF, Zero, Zero}) // byte0: Connectable=no, byte1: Type=Proprietary
                    Return (NUPC)
                }
                Method (_PLD) { Return (NPLD (0, 4)) }
            }
            Device(SS05)
            {
                Name (_ADR, 0x05)
                Method (_UPC, 0, Serialized)
                {
                    Name (NUPC, Package(4) {Zero, 0xFF, Zero, Zero}) // byte0: Connectable=no, byte1: Type=Proprietary
                    Return (NUPC)
                }
                Method (_PLD) { Return (NPLD (0, 5)) }
            }
            Device(SS06)
            {
                Name (_ADR, 0x06)
                Method (_UPC, 0, Serialized)
                {
                    Name (NUPC, Package(4) {Zero, 0xFF, Zero, Zero}) // byte0: Connectable=no, byte1: Type=Proprietary
                    Return (NUPC)
                }
                Method (_PLD) { Return (NPLD (0, 6)) }
            }
        }

        Method (NPLD, 2, Serialized)
        {
            Name (PCKG, Package(1)
            {
                Buffer(0x10){}
            })
            CreateField (DerefOf (Index (PCKG, Zero)), Zero, 0x07, REV)
            Store (One, REV)
            CreateField (DerefOf (Index (PCKG, Zero)), 0x40, One, VISI)
            Store (Arg0, VISI)
            CreateField (DerefOf (Index (PCKG, Zero)), 0x57, 0x08, GPOS)
            Store (Arg1, GPOS)
            Return (PCKG)
        }
    }
}

Scope (DGPU_SCOPE)
{
    Name (LTRE, Zero) // Buffer for LTR Save/Restore

    Method (_STA, 0)
    {
        Return (0x0F)
    }

    OperationRegion (PCIM, SystemMemory, PCI_SCOPE.DGDA, 0x1000) // dGPU
    Field (PCIM, DWordAcc, NoLock, Preserve)
    {
        NVID,  16,
        NDID,  16,
        CMDR,  8,
        VGAR,  2008,
        Offset (0x48B),
            ,  1,
        HDAE,  1
    }

    OperationRegion (DGPU, SystemMemory, PCI_SCOPE.DGDA, 0x100) // dGPU
    Field (DGPU, DWordAcc, NoLock, Preserve)
    {
        Offset (0x40), // Offset(64)
        SSSV, 32
    }

/*
    //  method: _ROM
    //  Arguments:
    //  Arg0: Integer     Offset of the graphics device ROM data
    //  Arg1: Integer     Size of the buffer to fill in (up to 4K)
    //
    //  Return Value:
    //  Returns Buffer    Buffer of requested video ROM bytes
    Method (_ROM, 2)
    {
        Store (Arg0, Local0)
        Store (Arg1, Local1)

        Name (VROM, Buffer(Local1) {0x00}) // Create 4K buffer to return to DD

        If (LGreater (Local1, 0x1000))
        {
            Store (0x1000, Local1) // Return dummy buffer if asking for more than 4K
        }

        If (LGreater (Arg0, RVBS))
        {
            Return (VROM) // Return dummy buffer if asking beyond VBIOS image
        }

        Add (Arg0, Arg1, Local2)

        If (LGreater (Local2, RVBS)) // If requested (BASE + LEN) > VBIOS image size
        {
            Subtract (RVBS, Local0, Local1) // Limit length to the final chunk of VBIOS image
        }

        Divide (Local0, 0x8000, Local3, Local4)  // (Dividend, Divisor, Remainder, Result)

        Switch (Local4)
        {
            Case (0)
            {
                Store (DGPU_SCOPE.VBS1, Local5)
            }
            Case (1)
            {
                Store (DGPU_SCOPE.VBS2, Local5)
            }
            Case (2)
            {
                Store (DGPU_SCOPE.VBS3, Local5)
            }
            Case (3)
            {
                Store (DGPU_SCOPE.VBS4, Local5)
            }
            Case (4)
            {
                Store (DGPU_SCOPE.VBS5, Local5)
            }
            Case (5)
            {
                Store (DGPU_SCOPE.VBS6, Local5)
            }
            Case (6)
            {
                Store (DGPU_SCOPE.VBS7, Local5)
            }
            Case (7)
            {
                Store (DGPU_SCOPE.VBS8, Local5)
            }
        }

        Multiply (0x8000, Local4, Local4) // (Multiplicand, Multiplier, Result)
        Subtract (Local0, Local4, Local0)

        Mid (Local5, Local0, Local1, VROM)

        Return (VROM)
    }
*/

    Method (_DSM, 4, Serialized)
    {
        //
        // Check for Nvidia Optimus _DSM UUID
        //
        // NVOP_DSM_GUID {A486D8F8-0BDA-471B-A72B-6042A6B5BEE0}
        If (LEqual (Arg0, ToUUID ("A486D8F8-0BDA-471B-A72B-6042A6B5BEE0")))
        {
            Return (DGPU_SCOPE.NVOP (Arg0, Arg1, Arg2, Arg3))
        }

        //
        // Check for Nvidia GPS _DSM UUID
        //
        // GPS_DSM_GUID {A3132D01-8CDA-49BA-A52E-BC9D46DF6B81}
        If (LEqual (Arg0, ToUUID ("A3132D01-8CDA-49BA-A52E-BC9D46DF6B81")))
        {
            If (LNotEqual (DGPU_SCOPE.GPSS, Zero))
            {
                Return (DGPU_SCOPE.GPS (Arg0, Arg1, Arg2, Arg3))
            }
        }

        //
        // Check for Nvidia GC6 _DSM UUID
        //
        // GC6_DSM_GUID {CBECA351-067B-4924-9CBD-B46B00B86F34}
        If (LEqual (Arg0, ToUUID ("CBECA351-067B-4924-9CBD-B46B00B86F34")))
        {
            If (LNotEqual (DGPU_SCOPE.GC6S, Zero))
            {
                Return (DGPU_SCOPE.NVJT (Arg0, Arg1, Arg2, Arg3))
            }
        }

        //
        // Check for Nvidia NBCI _DSM UUID
        //
        // NBCI_DSM_GUID {D4A50B75-65C7-46F7-BFB7-41514CEA0244}
        If (LEqual (Arg0, ToUUID ("D4A50B75-65C7-46F7-BFB7-41514CEA0244")))
        {
            If (LNotEqual (DGPU_SCOPE.NBCS, Zero))
            {
                Return (DGPU_SCOPE.NBCI (Arg0, Arg1, Arg2, Arg3))
            }
        }

#if FixedPcdGetBool (PcdHgNvidiaVenturaFeatureSupport)
        //
        // Check for Nvidia SPB _DSM UUID
        //
        // SPB_DSM_GUID {95DB88FD-940A-4253-A446-70CE0504AEDF}
        If (LEqual (Arg0, ToUUID ("95DB88FD-940A-4253-A446-70CE0504AEDF")))
        {
            If (CondRefof (IGPU_SCOPE.SPB))
            {
                Return (IGPU_SCOPE.SPB (Arg0, Arg1, Arg2, Arg3))
            }
        }
#endif

        //
        // Check for MXM _DSM UUID
        //
//[-start-210917-GEORGE0004-modify]//
//[-start-210918-QINGLIN0068-modify]//
//#ifndef S77014_SUPPORT
#if !defined(S77014_SUPPORT) && !defined(S570_SUPPORT) && !defined(S77014IAH_SUPPORT)
//[-end-210918-QINGLIN0068-modify]//
        If (LEqual (Arg0, ToUUID ("4004A400-917D-4cf2-B89C-79B62FD55665")))
        {
            Return (DGPU_SCOPE.MXM (Arg0, Arg1, Arg2, Arg3))
        }
#endif
//[-end-210917-GEORGE0004-modify]//

        Return (STATUS_ERROR_UNSPECIFIED)
    }
}