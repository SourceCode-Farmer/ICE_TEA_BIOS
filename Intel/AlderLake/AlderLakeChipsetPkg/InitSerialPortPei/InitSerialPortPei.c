/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/SerialPortLib.h>

#include <Ppi/PeiCoreFvLocation.h>
#include <Library/PeiServicesLib.h>

EFI_STATUS
EFIAPI
ReportFvInfoForDdt (
  )
{
  EFI_STATUS                            Status;
  EFI_PEI_CORE_FV_LOCATION_PPI          *PeiCoreFvLocationPpi;
  UINT8                                 *PeiFvBase;
  UINT8                                 *SearchBase;

  PeiFvBase = NULL;
  SearchBase = NULL;

  Status = PeiServicesLocatePpi (
             &gEfiPeiCoreFvLocationPpiGuid,
             0,
             NULL,
             (VOID **) &PeiCoreFvLocationPpi
             );
  
  if (PeiCoreFvLocationPpi != NULL) {
    PeiFvBase = (UINT8 *)(UINTN)PeiCoreFvLocationPpi->PeiCoreFvLocation;

    PeiServicesInstallFvInfoPpi (
      &gEfiFirmwareFileSystem2Guid,
      (VOID*) (UINTN) PeiFvBase,
      (UINT32) ((EFI_FIRMWARE_VOLUME_HEADER*)PeiFvBase)->FvLength,
      NULL,
      NULL
      );
  }

  return Status;
}

EFI_STATUS
EFIAPI
InitSerialPortPeiEntry (
  IN       EFI_PEI_FILE_HANDLE          FileHandle,
  IN CONST EFI_PEI_SERVICES             **PeiServices
  )
{
  EFI_STATUS        Status;

  Status = SerialPortInitialize();
  ASSERT_EFI_ERROR (Status);

  //
  // Only FSP dispatch mode need to report PEI core location.
  //
  if (PcdGet8 (PcdFspModeSelection) == 0) {
    ReportFvInfoForDdt ();
  }

  return Status;
}
