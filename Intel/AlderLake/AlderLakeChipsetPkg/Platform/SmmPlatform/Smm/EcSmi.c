/** @file
;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  This file processes hotkey dispatch and callback event.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2018 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "SmmPlatform.h"
#include <Library/EcLib.h>
#include <Library/EcMiscLib.h>
#include <Library/EcCommands.h>
//[-start-181025-IB15590141-add]//
#include <Library/H2OCpLib.h>
#include <Guid/H2OCpChipset.h>
//[-end-181025-IB15590141-add]//

//
// Internal functions
//

/**
  eSPI EC SMI callback function.

  @param[in] DispatchHandle       Not used

**/
VOID
EFIAPI
eSpiEcSmiCallback (
  IN  EFI_HANDLE                              DispatchHandle
  )
{
//[-start-181025-IB15590141-modify]//
  if (FeaturePcdGet (PcdH2OSmmCpEcSendEspiClearSupported)) {
    H2O_SMM_CP_EC_SEND_ESPI_CLEAR_DATA    SmmEspiEcSmiData;
    EFI_STATUS                            Status;
    
    SmmEspiEcSmiData.Size          = sizeof (H2O_SMM_CP_EC_SEND_ESPI_CLEAR_DATA);
    SmmEspiEcSmiData.Status        = H2O_CP_TASK_NORMAL;

    DEBUG ((DEBUG_INFO, "Checkpoint Trigger: %g\n", &gH2OSmmCpEcSendEspiClearGuid));
    Status = H2OCpTrigger (&gH2OSmmCpEcSendEspiClearGuid, &SmmEspiEcSmiData);
    DEBUG ((DEBUG_INFO, "Checkpoint Result: %x\n", SmmEspiEcSmiData.Status));

    if (!EFI_ERROR(Status)&& !EFI_ERROR(SmmEspiEcSmiData.Status)) {
      if (SmmEspiEcSmiData.Status == H2O_CP_TASK_SKIP) {
        goto Skip;
      }
    }
  }

  //
  // This is a dummy handling routine.
  //
  if (FeaturePcdGet(PcdUseCrbEcFlag)) {
    IoWrite8 (EC_C_PORT, EC_C_SMI_QUERY);
    IoRead8 (EC_D_PORT);
  }
  
  Skip:
    return;
//[-start-181025-IB15590141-modify]//
}
