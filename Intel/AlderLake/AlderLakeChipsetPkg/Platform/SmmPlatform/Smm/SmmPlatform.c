/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Smm platform driver.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "SmmPlatform.h"
#include <OemSetup.h>
#include <CpuRegs.h>
#include <PchPolicyCommon.h>
#include "PcieDockSmi.h"
#include "PlatformBoardId.h"
#include "AcpiCommon.h"

#define PROGRESS_CODE_S3_SUSPEND_START _gPcd_FixedAtBuild_PcdProgressCodeS3SuspendStart
#include <Library/PmcLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/GpioLib.h>
#include <Library/ItssLib.h>
#include <Library/TimerLib.h>
#include <Register/MeRegs.h>
#include <Register/HeciRegs.h>
#include <Protocol/PchTcoSmiDispatch.h>
#include <Protocol/SmmVariable.h>
#include <Protocol/SmmCpu.h>
#include <Library/PostCodeLib.h>
#include <Register/PchRegs.h>
#include <Register/PchRegsLpc.h>
#include <Register/PmcRegs.h>
#include <Register/RtcRegs.h>
#if FixedPcdGetBool(PcdCapsuleEnable) == 1
#include <Library/HobLib.h>
#include <Library/TopSwapLib.h>
#endif
//[-start-191225-IB16740000-add]//
#include <PchBdfAssignment.h>
//[-end-191225-IB16740000-add]//
#include <ChipsetAccess.h>
#include <ChipsetSmiTable.h>
#include <HybridGraphicsDefine.h>
#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/PchInfoLib.h>
#include <Library/ResetSystemLib.h>
#include <Library/SmmOemSvcKernelLib.h>
#include <Library/VariableLib.h>
#include <PchBdfAssignment.h>
#include <Protocol/OverrideAspm.h>
#include <Protocol/PchSmmIoTrapControl.h>
#include <Protocol/ProgramSsidSvid.h>
#include <Protocol/SmmGpiDispatch2.h>
#include <Register/UsbRegs.h>
#include <SmiTable.h>

//
// Global variables
//
GLOBAL_REMOVE_IF_UNREFERENCED EFI_SMM_SYSTEM_TABLE2                     *mSmst;

GLOBAL_REMOVE_IF_UNREFERENCED UINT16                                    mAcpiBaseAddr;
GLOBAL_REMOVE_IF_UNREFERENCED PLATFORM_NVS_AREA                         *mPlatformNvsAreaPtr = 0;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_ACPI_SMM_DEV                          mAcpiSmm;
//
// Setup variables
//
GLOBAL_REMOVE_IF_UNREFERENCED CHIPSET_CONFIGURATION                     mChipsetConfigurtion;
GLOBAL_REMOVE_IF_UNREFERENCED SETUP_DATA                                mSystemConfiguration;
GLOBAL_REMOVE_IF_UNREFERENCED SA_SETUP                                  mSaSetupConfiguration;
GLOBAL_REMOVE_IF_UNREFERENCED PCH_SETUP                                 mPchSetup;
//
// HMRFPO lock output parameters
//


GLOBAL_REMOVE_IF_UNREFERENCED UINT8                                     mPlatformFlavor;
GLOBAL_REMOVE_IF_UNREFERENCED UINT8                                     mBoardType;
GLOBAL_REMOVE_IF_UNREFERENCED UINT8                                     mEcPresent;
GLOBAL_REMOVE_IF_UNREFERENCED UINT8                                     mEcHandshakeEnable;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_SMM_CPU_PROTOCOL                      *mSmmCpu;

GLOBAL_REMOVE_IF_UNREFERENCED UINT8                                     mEctoPdControllerNegotiationInSx = 0;
GLOBAL_REMOVE_IF_UNREFERENCED UINT32                                    mFpsWakeGpio = 0;

GLOBAL_REMOVE_IF_UNREFERENCED PCH_SMM_IO_TRAP_CONTROL_PROTOCOL          *mPchSmmIoTrapControl;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_HANDLE                                mOSResetCallbacksHandle = NULL;
GLOBAL_REMOVE_IF_UNREFERENCED UINT32                                    mGpioSmiPad = 0;      // SMC_EXT_SMI GPIO pad
GLOBAL_REMOVE_IF_UNREFERENCED EFI_HANDLE                                mIchIoTrapHandle64;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_SMM_GPI_DISPATCH2_PROTOCOL            *mSmmGpiDispatch;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_SMM_IO_TRAP_REGISTER_CONTEXT          mIchIoTrapContext64;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_SMM_IO_TRAP_DISPATCH2_PROTOCOL        *mSmmIoTrapDispatch;
GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN                                   mGpioTier2WakeSupport;
//[-start-201030-IB16810136-remove]//
//#if FixedPcdGetBool(PcdCapsuleEnable) == 1
//GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN                                   mEndOfDxe = FALSE;
//#endif
//[-end-201030-IB16810136-remove]//
EFI_HG_NVS_AREA_PROTOCOL                *mHgDxeInfoDataProtocol;
EFI_SMM_THUNK_PROTOCOL                  *mSmmThunk;

EFI_STATUS
EFIAPI
OsResetSmi (
  IN  EFI_HANDLE     DispatchHandle,
  IN  CONST  VOID    *DispatchContext,
  IN  OUT  VOID      *CommBuffer,
  IN  OUT  UINTN     *CommBufferSize
  )
{
  EFI_STATUS    Status;

//[-start-190723-IB17700055-modify]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcOsResetCallback \n"));
  Status = OemSvcOsResetCallback ();
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcOsResetCallback Status: %r\n", Status));
//[-end-190723-IB17700055-modify]//
  if (Status == EFI_SUCCESS) {
    return EFI_SUCCESS;
  }

  //
  // Install OsReset SMM Protocol.
  //
  Status = mSmst->SmmInstallProtocolInterface (
                   &mOSResetCallbacksHandle,
                   &gEfiOSResetPolicyProtocolGuid,
                   EFI_NATIVE_INTERFACE,
                   NULL
                   );
  ASSERT_EFI_ERROR (Status);

  //
  // Avoid slave harddisk could not attach problem
  // need to set reset command in IDE controller.
  // But it need to take time.
  //
  do {
    // ResetCold ();
    ResetWarm ();
  } while (1);
}

/**
  SMI handler to update both "DisplayMode" and "PrimaryDisplay" settings when
  OS driver want to set MUX mode through MXDM method.

  @param[in]      DispatchHandle    The unique handle assigned to this handler by SmiHandlerRegister().
  @param[in]      DispatchContext   Points to an optional handler context which was specified when the
                                    handler was registered.
  @param[in, out] CommBuffer        A pointer to a collection of data in memory that will
                                    be conveyed from a non-SMM environment into an SMM environment.
  @param[in, out] CommBufferSize    The size of the CommBuffer.

  @retval EFI_SUCCESS               The interrupt was handled successfully.

**/
EFI_STATUS
SetDisplayMode (
  IN  EFI_HANDLE     DispatchHandle,
  IN  CONST  VOID    *DispatchContext,
  IN  OUT  VOID      *CommBuffer,
  IN  OUT  UINTN     *CommBufferSize
  )
{
  EFI_STATUS    Status;

  switch (mHgDxeInfoDataProtocol->DisplayMode) {
    case IgfxOnly:
      mSaSetupConfiguration.PrimaryDisplay = DisplayModeIgpu;
      mChipsetConfigurtion.DisplayMode = IgfxOnly;
      break;
    case DgpuOnly:
      if (IsPchH ()) {
        mSaSetupConfiguration.PrimaryDisplay = DisplayModeDgpu;
        mChipsetConfigurtion.DisplayMode = DgpuOnly;
      } else {
        mSaSetupConfiguration.PrimaryDisplay = DisplayModePci;
        mChipsetConfigurtion.DisplayMode = DgpuOnly;
      }
      break;
    case MsHybrid:
      mSaSetupConfiguration.PrimaryDisplay = DisplayModeHg;
      mChipsetConfigurtion.DisplayMode = MsHybrid;
      break;
    case Dynamic:
      mSaSetupConfiguration.PrimaryDisplay = DisplayModeHg;
      mChipsetConfigurtion.DisplayMode = Dynamic;
      break;
    default:
      return EFI_SUCCESS;
  }
  Status = CommonSetVariable (
             SA_SETUP_VARIABLE_NAME,
             &gSaSetupVariableGuid,
             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
             sizeof (SA_SETUP),
             &mSaSetupConfiguration
             );
  if (EFI_ERROR (Status)) {
    return Status;
  }
  Status = CommonSetVariable (
             SETUP_VARIABLE_NAME,
             &gSystemConfigurationGuid,
             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
             sizeof (CHIPSET_CONFIGURATION),
             &mChipsetConfigurtion
             );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
SaveRestoreStateCallBack (
  IN CONST EFI_GUID  *Protocol,
  IN VOID            *Interface,
  IN EFI_HANDLE      Handle
  )
{
  DEBUG ((DEBUG_INFO, "Save RestoreStateCallBack\n"));
  SaveRestoreState (FALSE);

  return EFI_SUCCESS;
}
/**
 SMI handler to restore ACPI mode

 @param  Protocol    Points to the protocol's unique identifier.
 @param  Interface   Points to the interface instance.
 @param  Handle      The handle on which the interface was installed.

 @retval EFI_SUCCESS callback runs successfully

**/
EFI_STATUS
EFIAPI
PlatformRestoreAcpiCallbackStart (
  IN CONST EFI_GUID  *Protocol,
  IN VOID            *Interface,
  IN EFI_HANDLE      Handle
  )
{
  SaveRestoreState (TRUE);

  return EFI_SUCCESS;
}

/**
 SMI handler to restore ACPI mode

 @param  Protocol    Points to the protocol's unique identifier.
 @param  Interface   Points to the interface instance.
 @param  Handle      The handle on which the interface was installed.

 @retval EFI_SUCCESS callback runs successfully

**/
EFI_STATUS
EFIAPI
PlatformRestoreAcpiCallbackDone (
  IN CONST EFI_GUID    *Protocol,
  IN VOID              *Interface,
  IN EFI_HANDLE        Handle
  )
{
  EFI_OVERRIDE_ASPM_PROTOCOL        *OverrideAspmProtocol;
  EFI_STATUS                        Status;
  EFI_PROGRAM_SSID_SVID_PROTOCOL    *ProgramSsidSvidProtocol;

  Status = mSmst->SmmLocateProtocol (
                    &gEfiProgramSsidSvidProtocolGuid,
                    NULL,
                    (VOID **)&ProgramSsidSvidProtocol
                    );
  if (!EFI_ERROR (Status)) {
    ProgramSsidSvidProtocol->ProgramSsidSvidFunc ();
  }

  Status = mSmst->SmmLocateProtocol (
                    &gEfiOverrideAspmProtocolGuid,
                    NULL,
                    (VOID **)&OverrideAspmProtocol
                    );
  if (!EFI_ERROR (Status)) {
    OverrideAspmProtocol->OverrideAspmFunc ();
  }

  return Status;
}

EFI_STATUS
EFIAPI
LocateSmmThunkProtocolCallBack(
  IN CONST EFI_GUID                     *Protocol,
  IN VOID                               *Interface,
  IN EFI_HANDLE                         Handle
  )
{
  EFI_STATUS                  Status;

  if (mSmmThunk != NULL) {
    return EFI_SUCCESS;
  }

  Status = mSmst->SmmLocateProtocol (
                    &gSmmThunkProtocolGuid,
                    NULL,
                    (VOID **)&mSmmThunk
                    );

  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
SmmInt15ServiceProtocolCallBack (
  IN CONST EFI_GUID           *Protocol,
  IN VOID                     *Interface,
  IN EFI_HANDLE               Handle
  )
{
  EFI_STATUS        Status;

  Status = mSmst->SmmInstallProtocolInterface (
                      &Handle,
                      &gEfiSmmInt15ServiceProtocol2Guid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );

  return Status;
}

VOID
Y2KRolloverCallBack (
  IN  EFI_HANDLE                        DispatchHandle
  )
{
  UINT8 Data8;
  IoWrite8 (PCAT_RTC_ADDRESS_REGISTER, 0x32 | 0x80);
  Data8 = IoRead8 (PCAT_RTC_DATA_REGISTER);
  if ((Data8 & 0x0F) == 0x9) {
    Data8 = Data8 >> 4;
    Data8++;
    Data8 = Data8 << 4;
  } else {
    Data8++;
  }
  IoWrite8 (PCAT_RTC_DATA_REGISTER, Data8);

  return;
}

/**
  Install smi for PCH NewCentury function

  @retval EFI_SUCCESS             Initialization complete.
**/
EFI_STATUS
EFIAPI
InstallPchTcoSmiNewCentury (
  VOID
  )
{
  EFI_STATUS        Status;
  EFI_HANDLE        Handle;
  PCH_TCO_SMI_DISPATCH_PROTOCOL     *PchTcoSmiDispatchProtocol;

  DEBUG ((DEBUG_INFO, "InstallPchTcoSmiNewCentury()\n"));

  ///
  /// Get the PCH TCO SMM dispatch protocol
  ///
  PchTcoSmiDispatchProtocol = NULL;
  Status = mSmst->SmmLocateProtocol (&gPchTcoSmiDispatchProtocolGuid, NULL, (VOID **) &PchTcoSmiDispatchProtocol);
  ASSERT_EFI_ERROR (Status);

  ///
  /// Register an NewCentury callback function to handle TCO NewCentury SMI
  ///
  Handle = NULL;
  Status = PchTcoSmiDispatchProtocol->NewCenturyRegister (
                                        PchTcoSmiDispatchProtocol,
                                        Y2KRolloverCallBack,
                                        &Handle
                                        );
  ASSERT_EFI_ERROR (Status);

  return EFI_SUCCESS;
}

/**
  Converts Decimal to BCD
  Only for 2 digit BCD.

  @param[in] Dec       - Decimal value to be converted to BCD

  @retval BCD for Dec number
**/
UINT8
DecToBCD (
  UINT8 Dec
  )
{
  UINT8 FirstDigit;
  UINT8 SecondDigit;

  FirstDigit  = Dec % 10;
  SecondDigit = Dec / 10;

  return (SecondDigit << 4) + FirstDigit;
}

/**
  Converts BCD to Dec number
  Only for 2 digit BCD.

  @param[in] BCD       - BCD number which needs to be converted to Dec

  @retval Dec value for given BCD
**/
UINT8
BCDToDec (
  UINT8 BCD
  )
{
  UINT8 FirstDigit;
  UINT8 SecondDigit;
  FirstDigit  = BCD & 0xf;
  SecondDigit = BCD >> 4;

  return SecondDigit * 10 + FirstDigit;
}

/**
  S1 Sleep Entry Call Back.

  @param[in] DispatchHandle     - The handle of this callback, obtained when registering
  @param[in] DispatchContext    - The predefined context which contained sleep type and phase
  @param[in] CommBuffer         - A pointer to a collection of data in memory that will
                                  be conveyed from a non-SMM environment into an SMM environment.
  @param[in] CommBufferSize     - The size of the CommBuffer.

  @retval EFI_SUCCESS           - Operation successfully performed**/
EFI_STATUS
S1SleepEntryCallBack (
  IN  EFI_HANDLE                   DispatchHandle,
  IN  CONST  VOID                  *DispatchContext,
  IN  OUT  VOID                    *CommBuffer,
  IN  OUT  UINTN                   *CommBufferSize
  )
{
  EFI_STATUS      Status;
  /*++
    Todo:
      Add project specific code in here.
  --*/
  POST_CODE (SMM_S1_SLEEP_CALLBACK); //PostCode = 0xA1, Enter S1
//[-start-190723-IB17700055-modify]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcS1Callback \n"));
  Status = OemSvcS1Callback ();
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcS1Callback Status: %r\n", Status));
//[-end-190723-IB17700055-modify]//
  return EFI_SUCCESS;
}

/**

 @param [in]   DispatchHandle   The handle of this callback, obtained when registering
 @param [in]   DispatchContext  The predefined context which contained sleep type and phase

**/
EFI_STATUS
S5SleepAcLossCallBack (
  IN  EFI_HANDLE                   DispatchHandle,
  IN  CONST  VOID                  *DispatchContext,
  IN  OUT  VOID                    *CommBuffer,
  IN  OUT  UINTN                   *CommBufferSize
  )
{
  EFI_STATUS  Status;
//[-start-190723-IB17700055-modify]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcS5AcLossCallback \n"));
  Status = OemSvcS5AcLossCallback ();
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcS5AcLossCallback Status: %r\n", Status));
//[-end-190723-IB17700055-modify]//
  return EFI_SUCCESS;
}

/**
  Initializes the SMM Platform Driver

  @param[in] ImageHandle   - Pointer to the loaded image protocol for this driver
  @param[in] SystemTable   - Pointer to the EFI System Table

  @retval Status           - EFI_SUCCESS
  @retval Assert, otherwise.

**/
EFI_STATUS
EFIAPI
InitializeInsydePlatformSmm (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{

  EFI_STATUS                                Status;
  EFI_HANDLE                                SwHandle;
  EFI_SMM_SW_DISPATCH2_PROTOCOL             *SwDispatch;
  EFI_SMM_SX_DISPATCH2_PROTOCOL             *SxDispatch;
  EFI_SMM_SW_REGISTER_CONTEXT               SwContext;
  EFI_SMM_SX_REGISTER_CONTEXT               EntryDispatchContext;
  UINTN                                     DataSize;
  UINT32                                    VariableAttr;
  EFI_HANDLE                                S1SleepEntryHandle;
  VOID                                      *AcpiRestoreAcpiCallbackStartReg;
  VOID                                      *AcpiRestoreAcpiCallbackDoneReg;
  VOID                                      *SaveRestoreStateCallBackReg;
  VOID                                      *SmmThunkProtocolReg;
  VOID                                      *SmmInt15ServiceProtocolReg;
  UINT16                                    Pm1EnData;
  EFI_HG_NVS_AREA_PROTOCOL                  *HGNvsAreaProtocol;

  POST_CODE (SMM_SMM_PLATFORM_INIT);
  DEBUG_OEM_SVC ((DEBUG_INFO, "InitializeInsydePlatformSmm Start\n"));

  //
  // Great!  We're now in SMM!
  //
  mEctoPdControllerNegotiationInSx = PcdGetBool (PcdUsbcEcPdNegotiation) ? 1 : 0;
  mFpsWakeGpio = PcdGet32(PcdFingerPrintIrqGpio);
  //
  // Locate Platform Info Protocol.
  //
  mPlatformFlavor = PcdGet8 (PcdPlatformFlavor);
  mEcPresent      = PcdGetBool (PcdEcPresent);
  mGpioTier2WakeSupport = PcdGetBool (PcdGpioTier2WakeEnable);

  //
  // Initialize global variables
  //
  mSmst = gSmst;

  //
  // Locate setup variable
  // We assert if it is not found because we have a dependency on AcpiPlatform which is
  // dependent on protocols the setup driver produces.
  //
  DataSize = sizeof (CHIPSET_CONFIGURATION);
  Status = gRT->GetVariable (
                  SETUP_VARIABLE_NAME,
                  &gSystemConfigurationGuid,
                  &VariableAttr,
                  &DataSize,
                  &mChipsetConfigurtion
                  );
  ASSERT_EFI_ERROR (Status);


  DataSize = sizeof (SA_SETUP);
  Status = gRT->GetVariable (
                  SA_SETUP_VARIABLE_NAME,
                  &gSaSetupVariableGuid,
                  NULL,
                  &DataSize,
                  &mSaSetupConfiguration
                  );
  ASSERT_EFI_ERROR (Status);

  //
  //  Locate the ICH SMM SW dispatch protocol
  //
  Status = mSmst->SmmLocateProtocol (&gEfiSmmSwDispatch2ProtocolGuid, NULL, (VOID**)&SwDispatch);
  ASSERT_EFI_ERROR (Status);

  //
  //Register the OS restart set features disable
  //
  SwContext.SwSmiInputValue = SMM_OS_RESET_SMI_VALUE;
  Status = SwDispatch->Register (
    SwDispatch,
    OsResetSmi,
    &SwContext,
    &SwHandle
    );
  ASSERT_EFI_ERROR (Status);

  if (FeaturePcdGet (PcdNvidiaOptimusSupported)) {
    Status = gBS->LocateProtocol (&gEfiHgNvsAreaProtocolGuid, NULL, (VOID **)&HGNvsAreaProtocol);
    ASSERT_EFI_ERROR (Status);
    mHgDxeInfoDataProtocol = HGNvsAreaProtocol;
    if (FeaturePcdGet (PcdHgNvidiaDdsFeatureSupport)) {
      //
      // Register SMI handler (0x7C) to change DisplayMode and PrimaryDisplay settings.
      //
      SwContext.SwSmiInputValue = OPTIMUS_SET_DISPLAY_MODE;
      Status = SwDispatch->Register (
                             SwDispatch,
                             SetDisplayMode,
                             &SwContext,
                             &SwHandle
                             );
      ASSERT_EFI_ERROR (Status);
    }
  }

  //
  // Register ACPI restore handler
  //
  Status = mSmst->SmmRegisterProtocolNotify (
                    &gAcpiRestoreCallbackStartProtocolGuid,
                    PlatformRestoreAcpiCallbackStart,
                    &AcpiRestoreAcpiCallbackStartReg
                    );
  ASSERT_EFI_ERROR (Status);

  Status = mSmst->SmmRegisterProtocolNotify (
                    &gAcpiRestoreCallbackDoneProtocolGuid,
                    PlatformRestoreAcpiCallbackDone,
                    &AcpiRestoreAcpiCallbackDoneReg
                    );
  ASSERT_EFI_ERROR (Status);

  Status = mSmst->SmmRegisterProtocolNotify (
                      &gAcpiEnableCallbackStartProtocolGuid,
                      SaveRestoreStateCallBack,
                      &SaveRestoreStateCallBackReg
                      );
  ASSERT_EFI_ERROR (Status);

  if (H2OGetBootType () != EFI_BOOT_TYPE) {

    if (FeaturePcdGet(PcdSmmInt10Enable)) {
      Status = mSmst->SmmLocateProtocol (&gSmmThunkProtocolGuid, NULL, (VOID **)&mSmmThunk);
      if (EFI_ERROR (Status)) {
        Status = mSmst->SmmRegisterProtocolNotify (
                           &gSmmThunkProtocolGuid,
                           LocateSmmThunkProtocolCallBack,
                           &SmmThunkProtocolReg
                           );
      }
      ASSERT_EFI_ERROR (Status);
    }

    Status = mSmst->SmmRegisterProtocolNotify (
                      &gEfiSmmInt15ServiceProtocolGuid,
                      SmmInt15ServiceProtocolCallBack,
                      &SmmInt15ServiceProtocolReg
                      );
    ASSERT_EFI_ERROR (Status);
  }

  //
  // In Windows 7, Pci-E Wake will set by FACP flag
  // but in Dos, system still can wake up from S5 by Pci-E Lan card, set below to follow SCU setting
  //
  if (!mChipsetConfigurtion.WakeOnPME) {
    Pm1EnData = IoRead16 (mAcpiBaseAddr + R_ACPI_IO_PM1_EN);
    Pm1EnData |= BIT14;  //B_ACPI_IO_PM1_EN_PCIEXPWAK_DIS; ==> PCI Express* Wake Disable(PCIEXPWAK_DIS)
    IoWrite16 (mAcpiBaseAddr + R_ACPI_IO_PM1_EN, Pm1EnData);
  }

  if (FeaturePcdGet (PcdSmiNewCenturySupport)) {
    Status = InstallPchTcoSmiNewCentury();
  }

  //
  // Locate the SmmSxDispatch protocol
  //
  Status = mSmst->SmmLocateProtocol (&gEfiSmmSxDispatch2ProtocolGuid, NULL, (VOID**)&SxDispatch);
  if (EFI_ERROR(Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  //
  // Register S1 entry phase call back function
  //
  EntryDispatchContext.Type  = SxS1;
  EntryDispatchContext.Phase = SxEntry;
  Status = SxDispatch->Register (
                         SxDispatch,
                         S1SleepEntryCallBack,
                         &EntryDispatchContext,
                         &S1SleepEntryHandle
                         );

//  PostCode(PLATFORM_SMM_INIT_EXIT);
  DEBUG_OEM_SVC ((DEBUG_INFO, "InitializeInsydePlatformSmm end\n"));

  return EFI_SUCCESS;
}
