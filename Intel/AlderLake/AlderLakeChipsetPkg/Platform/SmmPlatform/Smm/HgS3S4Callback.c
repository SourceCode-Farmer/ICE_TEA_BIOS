/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
//[-start-190613-IB16990059-add]//
#include "SmmPlatform.h"
#include <ChipsetAccess.h>
#include <HybridGraphicsDefine.h>
#include <Library/BaseLib.h>
#include <Library/IoLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/VariableLib.h>
#include <Guid/HybridGraphicsVariable.h>
//[-end-190613-IB16990059-add]//

BOOLEAN                                       mHdaStatus;
HG_VARIABLE_CONFIGURATION                     mHgData;

/**

  This function will sync the value from global NVS to variable.

  @param[in] PlatformNvsAreaPtr    - A pointer of Platform NVS area.
  @param[in] HGNvsAreaProtocol     - A pointer of HG own operation region.

**/
VOID
SetHgVariable (
  IN PLATFORM_NVS_AREA                        *PlatformNvsAreaPtr,
  IN EFI_HG_NVS_AREA_PROTOCOL                 *HGNvsAreaProtocol
  )
{
  EFI_STATUS                                  Status;
  HG_VARIABLE_CONFIGURATION                   HgData;
  UINTN                                       Size;

  if (PlatformNvsAreaPtr->DgpuVendorID != NVIDIA_VID) {
    return;
  }

  Size = sizeof (HG_VARIABLE_CONFIGURATION);
  Status = CommonGetVariable (
             L"HybridGraphicsVariable",
             &gH2OHybridGraphicsVariableGuid,
             &Size,
             &HgData
             );
  if (EFI_ERROR (Status)) {
    return;
  }

  if ((HgData.OptimusVariable.OptimusFlag != HGNvsAreaProtocol->OptimusFlag) ||
      (HgData.OptimusVariable.NvDgpuGen != HGNvsAreaProtocol->NvDgpuGen)) {
    HgData.OptimusVariable.OptimusFlag = HGNvsAreaProtocol->OptimusFlag;
    HgData.OptimusVariable.NvDgpuGen = HGNvsAreaProtocol->NvDgpuGen;
    Status = CommonSetVariable (
               L"HybridGraphicsVariable",
               &gH2OHybridGraphicsVariableGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               sizeof (HG_VARIABLE_CONFIGURATION),
               &HgData
               );
    if (EFI_ERROR (Status)) {
      return;
    }
  }
}

/**

  This function will enable Nvidia dGPU HDA.

  @param[in] PlatformNvsAreaPtr    - A pointer of Platform NVS area.
  @param[in] HGNvsAreaProtocol     - A pointer of HG own operation region.

**/
VOID
OpenHdAudio (
  IN PLATFORM_NVS_AREA                        *PlatformNvsAreaPtr,
  IN EFI_HG_NVS_AREA_PROTOCOL                 *HGNvsAreaProtocol
  )
{
  EFI_STATUS                                  Status;
  UINT32                                      Reg488;
  UINTN                                       Size;
//[-start-190613-IB16990059-add]//
  UINT64                                      BaseAddress;
//[-end-190613-IB16990059-add]//

  mHdaStatus = TRUE;
  if (PlatformNvsAreaPtr->DgpuVendorID != NVIDIA_VID) {
    return;
  }
  Size = sizeof (HG_VARIABLE_CONFIGURATION);
  Status = CommonGetVariable (
             L"HybridGraphicsVariable",
             &gH2OHybridGraphicsVariableGuid,
             &Size,
             &mHgData
             );
  if (EFI_ERROR (Status)) {
    return;
  }
//[-start-190613-IB16990059-add]//
  BaseAddress = PCI_SEGMENT_LIB_ADDRESS (
                  0,
                  mHgData.OptimusVariable.DgpuBus,
                  DGPU_DEVICE_NUM,
                  DGPU_FUNCTION_NUM,
                  0
                  );
  Reg488 = PciSegmentRead32 (BaseAddress + NVIDIA_DGPU_HDA_REGISTER);
  if ((Reg488 & BIT25) == 0) {
    mHdaStatus = FALSE;
    PciSegmentOr32 (BaseAddress + NVIDIA_DGPU_HDA_REGISTER, BIT25);
  }
//[-end-190613-IB16990059-add]//
}

/**

  This function will disable Nvidia dGPU HDA.

  @param[in] PlatformNvsAreaPtr    - A pointer of Platform NVS area.
  @param[in] HGNvsAreaProtocol     - A pointer of HG own operation region.

**/
VOID
CloseHdAudio (
  IN PLATFORM_NVS_AREA                        *PlatformNvsAreaPtr,
  IN EFI_HG_NVS_AREA_PROTOCOL                 *HGNvsAreaProtocol
  )
{
//[-start-190613-IB16990059-add]//
  UINT64                                      BaseAddress;
  
  if (PlatformNvsAreaPtr->DgpuVendorID != NVIDIA_VID) {
    return;
  }
  if (!mHdaStatus) {
    BaseAddress = PCI_SEGMENT_LIB_ADDRESS (
                    0,
                    mHgData.OptimusVariable.DgpuBus,
                    DGPU_DEVICE_NUM,
                    DGPU_FUNCTION_NUM,
                    0
                    );  
    PciSegmentAnd32 (BaseAddress + NVIDIA_DGPU_HDA_REGISTER, (UINT32) ~BIT25);
  }
//[-end-190613-IB16990059-add]//
}