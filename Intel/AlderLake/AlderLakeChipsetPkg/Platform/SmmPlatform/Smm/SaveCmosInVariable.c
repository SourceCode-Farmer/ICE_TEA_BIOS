/** @file

;******************************************************************************
;* Copyright (c) 2020 , Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/VariableLib.h>
#include <Library/CmosLib.h>
#include <Library/BaseMemoryLib.h>
#include <Guid/CmosInVariable.h>
#include <ChipsetCmos.h>

VOID
EFIAPI
SaveCmosInVariableSmm (
  VOID
  )
{
  EFI_STATUS        Status;
  UINT8             Cmosbuffer[CMOS_TOTAL_LENGTH];
  UINT8             CmosInVariable[CMOS_TOTAL_LENGTH];
  UINT8             Index;
  UINT8             Address;
  UINTN             Size;
  INTN              Saveflag;
  CMOS_ELEMENT      *CmosElement;

  Size     = CMOS_TOTAL_LENGTH;
  Saveflag = 0;

  for (Index = 0, Address = 0; Address < (CMOS_TOTAL_LENGTH / 2); Index++, Address++) {
    Cmosbuffer[Index] = ReadCmos8 (Address);
  }

  for (Address = 0; Address < (CMOS_TOTAL_LENGTH / 2); Index++, Address++) {
    Cmosbuffer[Index] = ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Address);
  }

  Status = CommonGetVariable (
             L"CmosVariable",
             &gCmosInVariableGuid,
             &Size,
             &CmosInVariable
             );
  if (Status == EFI_SUCCESS) {
    CmosElement = (CMOS_ELEMENT *)PcdGetPtr (PcdSaveCmosFieldCompareList);
    Index = 0;
    while (((CmosElement[Index].Start_Offset != 0) && (CmosElement[Index].Length != 0)) && Saveflag == 0) {
      Address = CmosElement[Index].Start_Offset;
      Saveflag = CompareMem (&CmosInVariable[Address], &Cmosbuffer[Address], CmosElement[Index].Length);
      Index++;
    }
  } else {
    Saveflag = 1;
  }

  if (Saveflag != 0) {
    Status = CommonSetVariable (
               L"CmosVariable",
               &gCmosInVariableGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               Size,
               (VOID *)Cmosbuffer
               );
  }
}
