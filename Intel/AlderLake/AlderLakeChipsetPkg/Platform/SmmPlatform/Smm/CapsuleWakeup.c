/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SmmPlatform.h>
#include <Library/BaseLib.h>
#include <SecureFlash.h>
#include <Library/VariableLib.h>
#include <Library/CmosLib.h>
#include <Register/PmcRegs.h>
#include <Register/PchRegsLpc.h>

// TGL add
#include <Register/RtcRegs.h>

/**
  Reads the RTC Index register

  @param[in] Index - Index register

  @retval Value in Index register
**/
UINT8
ReadRtcTime (
  IN UINT8 Index
  )
{
  UINT8           Value;
  UINT8           Addr;

  //
  // Use port RTC alternative ports 74h/75h to prevent from breaking NMI setting
  //
  //
  // Check if Data Time is valid
  //
  if (Index <= 9) {
    do {
      Addr = 0x0A;
      gSmst->SmmIo.Io.Write (
                        &gSmst->SmmIo,
                        SMM_IO_UINT8,
                        R_RTC_IO_INDEX_ALT,
                        1,
                        &Addr
                        );
      gSmst->SmmIo.Io.Read (
                        &gSmst->SmmIo,
                        SMM_IO_UINT8,
                        R_RTC_IO_TARGET_ALT,
                        1,
                        &Value
                        );
    } while (Value & 0x80);
  }

  Addr = Index;
  gSmst->SmmIo.Io.Write (
                    &gSmst->SmmIo,
                    SMM_IO_UINT8,
                    R_RTC_IO_INDEX_ALT,
                    1,
                    &Addr
                    );
  //
  // Read register.
  //
  gSmst->SmmIo.Io.Read (
                    &gSmst->SmmIo,
                    SMM_IO_UINT8,
                    R_RTC_IO_TARGET_ALT,
                    1,
                    &Value
                    );
  // if (Index <= 9) {
  //   Value = BCDToDec (Value);
  // }

  return (UINT8) Value;
}

/**
  Writes to an RTC Index register

  @param[in] Index   - Index to be written
  @param[in] Value   - Value to be written to Index register
**/
VOID
WriteRtcTime (
  IN UINT8 Index,
  IN UINT8 Value
  )
{
  UINT8           Addr;

  //
  // Use port RTC alternative ports 74h/75h to prevent from breaking NMI setting
  //

  Addr = Index;
  gSmst->SmmIo.Io.Write (
                    &gSmst->SmmIo,
                    SMM_IO_UINT8,
                    R_RTC_IO_INDEX_ALT,
                    1,
                    &Addr
                    );
  // if (Index <= 9) {
  //   Value = DecToBCD (Value);
  // }
  //
  // Write Register.
  //
  gSmst->SmmIo.Io.Write (
                    &gSmst->SmmIo,
                    SMM_IO_UINT8,
                    R_RTC_IO_TARGET_ALT,
                    1,
                    &Value
                    );
}

/**

 @param [in]   System wakeup time

 @retval None.

**/
VOID
WakeToProcessPendingCapsule (
  IN UINT16           AcpiBaseAddr,
  IN UINT8            WakeAfter
  )
{
  EFI_STATUS          Status;
  UINTN               Size;
  IMAGE_INFO          ImageInfo;
  UINT8               Reminder;
  UINT8               RtcSecond;
  UINT8               RtcMinute;
  UINT8               RtcHour;
  UINT16              RtcSts;
  UINT16              RtcEn;
  UINT8               AIE;
  BOOLEAN             BcdMode;
  UINT8               Buffer;

  Reminder  = 0;
  RtcSecond = 0;
  RtcMinute = 0;
  RtcHour   = 0;
  AIE       = 0;
  RtcSts    = 0;
  RtcEn     = 0;
  BcdMode   = TRUE;

  //
  // Wake time should locate between 1s ~ 15s
  //
  WakeAfter &= 0x0F;
  if (WakeAfter < 1) {
    WakeAfter = 1;
  }

  //
  // Check RTC mode is BCD or Binary
  //
  Buffer  = ReadCmos8 (RTC_ADDRESS_REGISTER_B);
  BcdMode = ~(Buffer >> 2);
  BcdMode &= BIT0;

  //
  // Check any CapsuleUpdate through S3, and set RTC wakeup
  //
  Size = sizeof (IMAGE_INFO);
  Status = CommonGetVariable (
             SECURE_FLASH_INFORMATION_NAME,
             &gSecureFlashInfoGuid,
             &Size,
             &ImageInfo);
  if ((Status == EFI_SUCCESS) && (ImageInfo.FlashMode)) {

    //
    // Caculate RTC wake Second/Minute/Hour
    //
    if (BcdMode) {
      RtcSecond = ReadRtcTime (RTC_ADDRESS_SECONDS);
      RtcSecond = WakeAfter + BcdToDecimal8 (RtcSecond);
      RtcSecond = DecimalToBcd8 (RtcSecond);
      RtcMinute = ReadRtcTime (RTC_ADDRESS_MINUTES);
      RtcMinute = BcdToDecimal8 (RtcMinute);
      RtcMinute = DecimalToBcd8 (RtcMinute);
      RtcHour   = ReadRtcTime (RTC_ADDRESS_HOURS);
      RtcHour   = BcdToDecimal8 (RtcHour);
      RtcHour   = DecimalToBcd8 (RtcHour);
    } else {
      Reminder  = WakeAfter + ReadRtcTime (RTC_ADDRESS_SECONDS);
      RtcSecond = Reminder % 60;
      Reminder  = Reminder / 60;
      Reminder  = Reminder + ReadRtcTime (RTC_ADDRESS_MINUTES);
      RtcMinute = Reminder % 60;
      Reminder  = Reminder / 60;
      Reminder  = Reminder + ReadRtcTime (RTC_ADDRESS_HOURS);
      RtcHour   = Reminder % 24;
    }

    //
    // Set RTC alarm
    //
    WriteRtcTime (RTC_ADDRESS_SECONDS_ALARM, RtcSecond);
    WriteRtcTime (RTC_ADDRESS_MINUTES_ALARM, RtcMinute);
    WriteRtcTime (RTC_ADDRESS_HOURS_ALARM,   RtcHour);

    //
    // Enable Alarm Interrupt Enable
    //
    AIE = ReadCmos8 (RTC_ADDRESS_REGISTER_B);
    AIE |= ALARM_INTERRUPT_ENABLE;
    WriteCmos8 (RTC_ADDRESS_REGISTER_B, AIE);

    //
    // Clear RTC_STS (PMBase + 0h bit10) and write RTC_EN (PMBase + 2h bit10) to generates a wake event
    //
    RtcSts = IoRead16 (AcpiBaseAddr + R_ACPI_IO_PM1_STS);
    RtcSts |= B_ACPI_IO_PM1_EN_RTC;
    IoWrite16 (AcpiBaseAddr, RtcSts);

    RtcEn = IoRead16 (AcpiBaseAddr + R_ACPI_IO_PM1_EN);
    RtcEn |= B_ACPI_IO_PM1_STS_RTC;
    IoWrite16 (AcpiBaseAddr + R_ACPI_IO_PM1_EN, RtcEn);
  }

  return;
}
