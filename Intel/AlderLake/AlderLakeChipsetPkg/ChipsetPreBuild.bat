@REM
@REM Chipset PreBuild batch file
@REM
@REM ******************************************************************************
@REM * Copyright (c) 2017 - 2020, Insyde Software Corp. All Rights Reserved.
@REM *
@REM * You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM * transmit, broadcast, present, recite, release, license or otherwise exploit
@REM * any part of this publication in any form, by any means, without the prior
@REM * written permission of Insyde Software Corporation.
@REM *
@REM ******************************************************************************

@REM     Add Chispet specific pre-build process here
echo off
  for /f "tokens=4" %%a in ('find "BIOS_GUARD_SUPPORT" Project.env') do if %%a==NO goto End_BiosGuardSvn
  setlocal
  set CHIPSET_PKG_TOOLS_PATH=%WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\Tools\Bin\Win32
  path=%path%;%CHIPSET_PKG_TOOLS_PATH%
  BiosGuardSvn.exe %CHIPSET_PKG% %WORKSPACE%
  @if not errorlevel 0 goto BiosGuardSvnError
  goto End_BiosGuardSvn
:BiosGuardSvnError
  echo =============================================
  echo !!!!!                                   !!!!!
  echo !!!!!     Update BiosGuardSvn Error     !!!!!
  echo !!!!!                                   !!!!!
  echo =============================================
  pause
:End_BiosGuardSvn


REM
REM Process FSP binary in Build folder to let the these file can be removed when clean command is executed.
REM
@for /f "tokens=3" %%a in ('find "TARGET " %WORKSPACE%\Conf\target.txt') do @set TARGET=%%a
@for /f "tokens=3" %%a in ('find "TOOL_CHAIN_TAG" %WORKSPACE%\Conf\target.txt') do @set TOOL_CHAIN_TAG=%%a
SET SPLITED_FSP_FV_FOLDER_PATH=%WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN_TAG%\FV\FSP
if not exist %SPLITED_FSP_FV_FOLDER_PATH% mkdir %SPLITED_FSP_FV_FOLDER_PATH%
@set BIN_TYPE =
@if "%1"=="efidebug" set BIN_TYPE=_Efidebug
@if "%1"=="uefi64ddt" set BIN_TYPE=_Ddt
@if "%1"=="uefi64ddtut" set BIN_TYPE=_Ddt

@if "%NativeFsp%" == "YES" (
  @set BIN_TYPE=
  @if "%PLATFORM_TYPE%"=="AlderLakeS" set PLATFORM_FSP_BIN_PACKAGE=%PLATFORM_FSP_BIN_PACKAGE%\NativeFspRelease\%NativeFspVersion%
  @if "%PLATFORM_TYPE%"=="AlderLakeP" set PLATFORM_FSP_BIN_PACKAGE=%PLATFORM_FSP_BIN_PACKAGE%\NativeFspRelease\%NativeFspVersion%
  @if "%PLATFORM_TYPE%"=="AlderLakeM" set PLATFORM_FSP_BIN_PACKAGE=%PLATFORM_FSP_BIN_PACKAGE%\NativeFspRelease\%NativeFspVersion%
  @if "%BIN_TYPE%" =="_Efidebug" (
    @if "%PLATFORM_TYPE%"=="AlderLakeS" set PLATFORM_FSP_BIN_PACKAGE=%PLATFORM_FSP_BIN_PACKAGE%\NativeFspDebug\%NativeFspVersion%
    @if "%PLATFORM_TYPE%"=="AlderLakeP" set PLATFORM_FSP_BIN_PACKAGE=%PLATFORM_FSP_BIN_PACKAGE%\NativeFspDebug\%NativeFspVersion%
    @if "%PLATFORM_TYPE%"=="AlderLakeM" set PLATFORM_FSP_BIN_PACKAGE=%PLATFORM_FSP_BIN_PACKAGE%\NativeFspDebug\%NativeFspVersion%
  )
) 

copy /y %WORKSPACE%\%CHIPSET_REL_PATH%\%PLATFORM_FSP_BIN_PACKAGE%\Fsp%BIN_TYPE%.fd %SPLITED_FSP_FV_FOLDER_PATH%

:EndSwitchingDEC
@popd
@REM echo ---------------------------Check-CrbBuild-------------------------------
@REM Checking DSC control file attrib and clean content
@if exist CrbExtConfig.dsc (
  @attrib -r CrbExtConfig.dsc
  @echo.>CrbExtConfig.dsc
)

@if "%CrbBuild%" == "YES" (
  @findstr /C:"gChipsetPkgTokenSpaceGuid.PcdCrbBoard|TRUE" %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\CrbExtConfig.dsc >nul
  @if errorlevel 1 (
    @echo gChipsetPkgTokenSpaceGuid.PcdCrbBoard^|TRUE >> %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\CrbExtConfig.dsc
  )
)

@if "%CrbBuild%" == "NO" (
  @findstr /C:"gChipsetPkgTokenSpaceGuid.PcdCrbBoard|FALSE" %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\CrbExtConfig.dsc >nul
  @if errorlevel 1 (
    @echo gChipsetPkgTokenSpaceGuid.PcdCrbBoard^|FALSE >> %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\CrbExtConfig.dsc
  )
)
@REM pause
@REM echo ---------------------------Check-CrbBuild-------------------------------

REM
REM RebaseAndPatchFspBinBaseAddress:
REM   1. Re-base FSP bin file to new address which is parsed from Project.fdf
REM   2. Split FSP bin to FSP-S/M/T segments
REM   3. Patch the PCDs in FspBinFvsBaseAddress.dsc with the re-based FSP-S/M/T address
REM
REM pushd %WORKSPACE%
@REM[-start-200710-IB10181004-add]REM
@set PLATFORM_BOARD_PACKAGE=AlderLakeBoardPkg
@set WORKSPACE_ROOT=%WORKSPACE%
@set WORKSPACE_PLATFORM=%WORKSPACE_ROOT%\Intel\AlderLake
@set WORKSPACE_SILICON=%WORKSPACE_PLATFORM%
@set FLASHMAP_FDF=%WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Include\Fdf\FlashMapInclude.fdf
@set INSYDE_PCD_FILE=%WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN_TAG%\X64\PcdList.txt
@set PYTHON_COMMAND=py -3
@set WORKSPACE_CORE_PLATFORM=%WORKSPACE_ROOT%\Intel\Edk2Platforms\Platform\Intel
@set WORKSPACE_FSP_BIN=%WORKSPACE%\Intel\AlderLake
@REM[-start-200917-IB06462159-remove]REM
REM @set PLATFORM_FSP_BIN_PACKAGE=AlderLakeFspBinPkg
@REM[-end-200917-IB06462159-remove]REM
@REM[-end-200710-IB10181004-add]REM

@if "%WORKSPACE:~-1%"==":" ( pushd %WORKSPACE%\ ) ELSE ( pushd %WORKSPACE% )
build.exe -q --Dynamic-to-DynamicEx %BUILD_TARGET_OPTIONS% %PROJECT_BUILD_OPTIONS% -m %WORKSPACE%\%CHIPSET_REL_PATH%\%CHIPSET_PKG%\DummyPcdPei\DummyPcdPei.inf

@REM[-start-200724-IB06462127-modify]REM
@REM[-start-200723-IB10181005-modify]REM
@REM[-start-200710-IB10181004-modify]REM
@REM[-start-201028-IB17040176-modify]REM
@echo .
call %WORKSPACE_CORE_PLATFORM%\MinPlatformPkg\Tools\Fsp\RebaseAndPatchFspBinBaseAddress.exe %INSYDE_PCD_FILE% %SPLITED_FSP_FV_FOLDER_PATH% Fsp%BIN_TYPE%.fd %WORKSPACE%\%PROJECT_REL_PATH%\%PROJECT_PKG%\Project.dsc %WORKSPACE%\Build\%PROJECT_PKG%\Project.dsc 0x0
@echo .
@REM[-end-201028-IB17040176-modify]REM
@REM[-end-200710-IB10181004-modify]REM
@REM[-end-200723-IB10181005-modify]REM
@REM[-end-200724-IB06462127-modify]REM

popd
@if %ERRORLEVEL% NEQ 0 goto Error_PatchFspBin
@REM[-start-200724-IB06462127-modify]REM
@REM[-start-200710-IB10181004-modify]REM
  copy /y /b %SPLITED_FSP_FV_FOLDER_PATH%\Fsp_Rebased_S.fd+%SPLITED_FSP_FV_FOLDER_PATH%\Fsp_Rebased_M.fd+%SPLITED_FSP_FV_FOLDER_PATH%\Fsp_Rebased_T.fd %SPLITED_FSP_FV_FOLDER_PATH%\Fsp_Rebased.fd
@REM[-end-200710-IB10181004-modify]REM
@REM[-end-200724-IB06462127-modify]REM
  goto End_PatchFspBin
:Error_PatchFspBin
  echo =========================================================
  echo !!!!!                                               !!!!!
  echo !!!!!     RebaseAndPatchFspBinBaseAddress Error     !!!!!
  echo !!!!!                                               !!!!!
  echo =========================================================
  pause
:End_PatchFspBin

  endlocal
echo on
