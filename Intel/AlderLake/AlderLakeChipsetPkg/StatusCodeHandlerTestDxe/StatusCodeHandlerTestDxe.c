/** @file
    Implement the test DXE driver for status code to RAM.

***************************************************************************
* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
*
* You may not reproduce, distribute, publish, display, perform, modify, adapt,
* transmit, broadcast, present, recite, release, license or otherwise exploit
* any part of this publication in any form, by any means, without the prior
* written permission of Insyde Software Corporation.
*
******************************************************************************
*/

#include <Uefi.h>
#include <Pi/PiStatusCode.h>
#include <Guid/MemoryStatusCodeRecord.h>
#include <Library/BaseLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>


RUNTIME_MEMORY_STATUSCODE_HEADER  *mRtMemoryStatusCodeTable;

/**

  @param[in]  ImageHandle     The image handle.
  @param[in]  SystemTable     The system table.

  @retval EFI_SUCCESS            A configuration table matching TableGuid was found.
  @retval EFI_NOT_FOUND          A configuration table matching TableGuid could not be found.  
**/
EFI_STATUS
EFIAPI
StatusCodeHandlerTestDxeEntryPoint (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS                            Status;
  MEMORY_STATUSCODE_RECORD              *Record;
  UINTN                                 Index;
  UINTN                                 TempRecordIndex;


  DEBUG ((EFI_D_INFO ,"StatusCodeHandlerTestDxe: Start.\n"));

  Status = EfiGetSystemConfigurationTable (&gMemoryStatusCodeRecordGuid, (VOID **)&mRtMemoryStatusCodeTable);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO ,"EfiGetSystemConfigurationTable for gMemoryStatusCodeRecordGuid! Status = %r\n", Status));
    return Status;
  }
  DEBUG ((EFI_D_INFO ,"mRtMemoryStatusCodeTable address: 0x%X\n", mRtMemoryStatusCodeTable));
  DEBUG ((EFI_D_INFO ,"mRtMemoryStatusCodeTable->RecordIndex = %d\n", mRtMemoryStatusCodeTable->RecordIndex));
  DEBUG ((EFI_D_INFO ,"mRtMemoryStatusCodeTable->NumberOfRecords = %d\n", mRtMemoryStatusCodeTable->NumberOfRecords));
  DEBUG ((EFI_D_INFO ,"mRtMemoryStatusCodeTable->MaxRecordsNumber = %d\n", mRtMemoryStatusCodeTable->MaxRecordsNumber));
  TempRecordIndex = mRtMemoryStatusCodeTable->RecordIndex;
  DEBUG ((EFI_D_INFO ,"TempRecordIndex = %d\n", TempRecordIndex));

  if (mRtMemoryStatusCodeTable != NULL) {
    Record = (MEMORY_STATUSCODE_RECORD *) (mRtMemoryStatusCodeTable + 1);
    for (Index = 0; Index < TempRecordIndex; Index++) {
      DEBUG ((EFI_D_INFO ,"Dxe[%d] CodeType 0x%X V%08X I%X\n", Index + 1, Record->CodeType, Record->Value, Record->Instance));
    }
  }
  DEBUG ((EFI_D_INFO ,"StatusCodeHandlerTestDxe: End. Status = %r\n", Status));

  return Status;
}