/** @file
  Statements that include other files.

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <PostCode.h>
#include <ChipsetSetupConfig.h>
#include "Ppi/ReadOnlyVariable2.h"
#include <Ppi/SioInitialized.h>
#include <Library/IoLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/PeiOemSvcChipsetLib.h>
#include <Register/SaRegsHostBridge.h>
#include <ChipsetAccess.h>
#include <Library/PeiInsydeChipsetLib.h>
//#include <Library/PchPlatformLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/MmPciLib.h>
#include <SystemAgent/Include/SaAccess.h>
#include <Library/SerialPortLib.h>

static EFI_PEI_PPI_DESCRIPTOR mSioInitializedPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPeiSioInitializedPpiGuid,
  NULL
};

/**

   INIT the SIO. Ported this code and I don't undertand the comments either.

   @param   FfsHeader             FV this PEIM was loaded from.
   @param   PeiServices           General purpose services available to every PEIM.

   @retval  None

**/
EFI_STATUS
EFIAPI
SioInitPeim (
  IN       EFI_PEI_FILE_HANDLE       FileHandle,
  IN CONST EFI_PEI_SERVICES          **PeiServices
  )
{
  UINT16                            ConfigPort;
  UINT16                            IndexPort;
  UINT16                            DataPort;
  UINT8                             Index;
  EFI_STATUS                        Status;
  CHIPSET_CONFIGURATION              SystemConfiguration;
  UINT8                             Data8;
  EFI_SIO_TABLE                     *mSioTable;
  UINTN                             Size;
  EFI_SIO_GPIO_TABLE                *mSioGpioTable;
  UINT16                            DeviceId;
  SA_SETUP                          SaSetup;

  POST_CODE (PEI_SIO_INIT); //PostCode = 0x70, Super I/O initial

  mSioTable=(EFI_SIO_TABLE *)PcdGetPtr (PcdPeiSioTable);
  ASSERT (mSioTable != NULL);

  mSioGpioTable=(EFI_SIO_GPIO_TABLE *)PcdGetPtr (PcdPeiSioGpioTable1);
  ASSERT (mSioGpioTable != NULL);

  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcHookInitSio \n"));
  Status = OemSvcHookInitSio (mSioTable,mSioGpioTable);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcHookInitSio Status: %r\n", Status));
  if ( Status == EFI_SUCCESS ) {
    return EFI_SUCCESS;
  }

  DeviceId = MmioRead16 (MmPciBase (SA_MC_BUS, 0, 0) + R_SA_MC_DEVICE_ID);

  if (IS_SA_DEVICE_ID_MOBILE (DeviceId)) {
    //
    // hard code for sio init
    //
    ConfigPort = PcdGet16(PcdSioConfigPort);
    DataPort   = PcdGet16(PcdSioDataPort);
    IndexPort  = PcdGet16(PcdSioIndexPort);

    //
    // Program and Enable SIO Base Addresses
    //
    PchLpcIoDecodeRangesSet (PcdGet16 (PcdPchLpcDecodeRange));
    PchLpcIoEnableDecodingSet (PcdGet16 (PcdPchLpcEnableList));
    //
    // Enter Config Mode
    //
    IoWrite8 (ConfigPort, 0x55);

    Size = PcdGetSize(PcdPeiSioTable)/sizeof(EFI_SIO_TABLE);

    //
    // Configure SIO
    //
    for (Index = 0; Index < Size; Index++) {
      IoWrite8 (IndexPort, mSioTable[Index].Register);
      IoWrite8 (DataPort, mSioTable[Index].Value);
    }

    //
    // Exit Config Mode
    //
    IoWrite8 (PcdGet16(PcdSioConfigPort), 0xAA);

    Size = PcdGetSize(PcdPeiSioGpioTable1)/sizeof(EFI_SIO_GPIO_TABLE);

    for (Index = 0; Index < Size; Index++) {
      IoWrite8 (PcdGet16(PcdSioBaseAddress) + mSioGpioTable[Index].Register, mSioGpioTable[Index].Value);
    }
    //
    // GPIO 15-17:IN  10-11:IN 12:OUT 13-14:IN  Enable RS232  ref: Page40 of CRB_SCH
    //
    IoWrite8 (PcdGet16(PcdSioBaseAddress) + 0x0c, 0x1f);

    //
    // GPIO 30-37, GPIO37/GPIO31:80 OR GPIO37/GPIO31:02
    //

    // Get setup variable from NVRAM.
    Status = GetChipsetSetupVariablePei (&SystemConfiguration);
    if (EFI_ERROR (Status)) {
      ASSERT_EFI_ERROR (Status);
      return Status;
    }
    // Get SA setup variable from NVRAM.
    Status = GetChipsetSaSetupVariablePei (&SaSetup);
    if (EFI_ERROR (Status)) {
      ASSERT_EFI_ERROR (Status);
      return Status;
    }

      if (!EFI_ERROR (Status)) {
        //
        // modify SIO GPIO_31 & GPIO_37 to select backlight control mode : PWM or GMBUS (Graphic SMBUS, ie. I2C)
        //
        Data8 = IoRead8 (PcdGet16(PcdSioBaseAddress) + 0x0E) & (~0x82);  // clear GPIO_31(bit1), GPIO_37(bit7)
        switch (SaSetup.IgdLcdBlc) {

          case BKLT_SEL_PWM_INVERTED :
          case BKLT_SEL_PWM_NORMAL   :
            IoWrite8 (PcdGet16(PcdSioBaseAddress) + 0x0E, (Data8 | 0x02));       // "PWM" is selected
            break;
          case BKLT_SEL_GMBUS_INVERTED  :
          case BKLT_SEL_GMBUS_NORMAL    :
            IoWrite8 (PcdGet16(PcdSioBaseAddress) + 0x0E, (Data8 | 0x80));       // "GMBus" is selected
            break;
        }
      }
    } else {
      SerialPortInitialize ();
    }


  Status = PeiServicesInstallPpi (&mSioInitializedPpi);

  return Status;
}

