/** @file
   Extend PEI Timer driver definition
;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
**/

#ifndef _THUNK_EVENT_H_
#define _THUNK_EVENT_H_

#include <Library/UefiBootServicesTableLib.h>
#include <Library/Thunk64To32Lib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/HobLib.h>
#include <Library/H2OCpLib.h>
#include <Protocol/Cpu.h>
#include <Protocol/Legacy8259.h>
#include <Guid/PeiTimerDataHob.h>

//
// 549254*100ns = 54.9ms
//
#define DEFAULT_TIMER_TICK_DURATION     549254

//
// Store EFI_PEI_SERVICES** in the 4 bytes
//
#define PEI_POINTER_SIZE  4

//
// BugBug: Non Portable
//
#if defined (__GNUC__)
#define ALIGN_16BYTE_BOUNDRY  __attribute__ ((aligned (16)))
#else
#define ALIGN_16BYTE_BOUNDRY  __declspec (align (16))
#endif

#pragma pack(1)

typedef struct {
  UINT16    Offset15To0;
  UINT16    SegmentSelector;
  UINT16    Attributes;
  UINT16    Offset31To16;
  UINT32    Offset63To32;
  UINT32    Reserved;
} INTERRUPT_GATE_DESCRIPTOR;

#pragma pack()

typedef struct _X64_IDT_TABLE {
  //
  // Reserved 4 bytes preceding PeiService and IdtTable,
  // since IDT base address should be 8-byte alignment.
  //
  UINT32                    Reserved;
  UINT32                    PeiService;
  INTERRUPT_GATE_DESCRIPTOR IdtTable;
} X64_IDT_TABLE;

/**
  Legacy8259Protocol installed callback function.
    
  @param[in]  Event                 Wait Event
  @param[in]  Context               Passed parameter to event handler

**/
VOID
Legacy8259CallbackFunction  (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  );

/**

  CpuArchProtocol installed callback function.
  
  @param[in]  Event                 Wait Event
  @param[in]  Context               Passed parameter to event handler

**/
VOID
CpuCallbackFunction  (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  );

/**

  Change cpu mode to IA-32 and execution PEI function.

  @param[in]  Event                 Wait Event
  @param[in]  Context               Passed parameter to event handler

**/
VOID
TimerCallbackFunction  (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  );

  
#endif