/** @file
  Program GPIO in early phase.

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/PeiOemSvcChipsetLib.h>
#include <Library/GpioLib.h>


/**
 Program GPIO in early phase.

 @param[in]         FileHandle          Handle of the file being invoked.
 @param[in]         PeiServices         Describes the list of possible PEI Services.

 @retval            EFI_SUCCESS         The operation completed successfully.
 @retval            Others              An unexpected error occurred.
*/

EFI_STATUS
EarlyProgramGpioEntryPoint (
  IN EFI_PEI_FILE_HANDLE        FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS                    Status;
  GPIO_INIT_CONFIG              *EarlyGpioTable;
  UINT16                        EarlyGpioTableCount;

  DEBUG((DEBUG_INFO, "EarlyProgramGpioEntryPoint Start\n"));

  EarlyGpioTable = NULL;
  EarlyGpioTableCount = 0;

  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcEarlyGpioSettingTable \n"));
  Status = OemSvcEarlyGpioSettingTable (&EarlyGpioTable, &EarlyGpioTableCount);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcEarlyGpioSettingTable Status: %r\n", Status));
  if (Status == EFI_MEDIA_CHANGED && EarlyGpioTable != NULL) {
    DEBUG ((DEBUG_INFO, "GpioConfigurePads() Start\n"));
    Status = GpioConfigurePads (EarlyGpioTableCount, EarlyGpioTable);
    ASSERT_EFI_ERROR (Status);
    if (EFI_ERROR(Status)) {
      return Status;
    }
    DEBUG ((DEBUG_INFO, "GpioConfigurePads() End\n"));
  }

  DEBUG((DEBUG_INFO, "EarlyProgramGpioEntryPoint End\n"));
  return EFI_SUCCESS;

}
