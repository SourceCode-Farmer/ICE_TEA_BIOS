/** @file
  This driver provides some definitions for Smart Tool

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _IHISI_H2OUVESMI_H_
#define _IHISI_H2OUVESMI_H_

//
// The following codes are the sample. Please customize here.
// The default codes are some dummy function prototypes.
//

#include "IhisiSmm.h"

#define H2OUVE_FUNCTIONS                    \
  { 0x52, H2oUveSmiService  },              \
  { 0x53, H2oUveSmiService  }

EFI_STATUS
H2oUveSmiService (
  VOID
);

EFI_STATUS
H2oUveSmiServiceInit (
  VOID
);


#endif
