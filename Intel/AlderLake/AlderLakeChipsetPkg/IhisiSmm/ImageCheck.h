/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _IHISI_FBTS_IMAGE_CHECK_H_
#define _IHISI_FBTS_IMAGE_CHECK_H_

#include "Fbts.h"

#define FBTS_IMAGE_CHECK_IMAGE_SIZE_64KB                ( 0x10000 )
#define FBTS_IMAGE_CHECK_IMAGE_SIZE_MAX                 ( FBTS_IMAGE_CHECK_IMAGE_SIZE_64KB )

#define FBTS_IMAGE_CHECK_FAILURE_CODE_NONE              ( 0 )
#define FBTS_IMAGE_CHECK_FAILURE_CODE_BOOT_GUARD        ( BIT0 )

typedef struct _LOCATION_INFO {
  UINTN        Address;
  UINTN        Length;
} LOCATION_INFO;

typedef struct _LOCATION_INFO_LIST LOCATION_INFO_LIST;

struct _LOCATION_INFO_LIST {
  LOCATION_INFO             LocationInfo;
  LOCATION_INFO_LIST        *NextPtr;
};

typedef
EFI_STATUS
( *AUTHENTICATION_CHECK_FUNCTION ) (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

typedef struct _AUTHENTICATION_CHECK_TABLE {
  UINT32                               TableCode;
  AUTHENTICATION_CHECK_FUNCTION        *Function;
} AUTHENTICATION_CHECK_TABLE;

EFI_STATUS
ImageCheckFreeLocationInfoList (
  IN OUT  LOCATION_INFO_LIST        *LocationInfoList
  );

IHISI_STATUS_CODE
ImageCheckStatusTranslation (
  IN      EFI_STATUS        Status
  );

EFI_STATUS
ImageCheckBootGuardCheckLegacyBoot (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

EFI_STATUS
ImageCheckBootGuardRequestFITP (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

EFI_STATUS
ImageCheckBootGuardRequestFIT (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

EFI_STATUS
ImageCheckBootGuardValidateFIT (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

EFI_STATUS
ImageCheckBootGuardCollectFITInfo (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

EFI_STATUS
ImageCheckBootGuardRequestBPM (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

EFI_STATUS
ImageCheckBootGuardValidateBPM (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

EFI_STATUS
ImageCheckBootGuardCheckACMSVN (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

EFI_STATUS
ImageCheckBootGuardCheckBPSVN (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

EFI_STATUS
ImageCheckBootGuardFindKM (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

EFI_STATUS
ImageCheckBootGuardCheckKMSVN (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );

//[-start-190701-16990082-add]//
EFI_STATUS
ImageCheckBootGuardRequestACM (
  IN OUT  UINTN          *Address,
  IN OUT  UINTN          *Length,
     OUT  BOOLEAN        *FurtherDataRequired,
     OUT  BOOLEAN        *Repeat
  );
//[-end-190701-16990082-add]//

#endif
