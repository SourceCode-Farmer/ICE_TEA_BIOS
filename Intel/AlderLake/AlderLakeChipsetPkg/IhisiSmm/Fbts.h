/** @file
  This driver provides IHISI interface in SMM mode

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _IHISI_FBTS_H_
#define _IHISI_FBTS_H_
#include "IhisiSmm.h"
#include <H2OIhisi.h>
#include <ChipsetAccess.h>
#include <ChipsetSetupConfig.h>
#include <Library/FdSupportLib.h>
#include <Protocol/SmmFwBlockService.h>

//
// Intel RC remove these define, so redefine value as below.
//

#define B_PCH_SPI_FRAP_BMWAG_MASK           0xFF000000                    ///< Master Write Access Grant MASK
#define B_PCH_SPI_FRAP_BMRAG_MASK           0x00FF0000                    ///< Master Read Access Grant MASK
#define B_PCH_SPI_FREG0_LIMIT_MASK          0x7FFF0000                    ///< Size, [30:16] here represents limit[26:12]
#define B_PCH_SPI_FREG0_BASE_MASK           0x00007FFF                    ///< Base, [14:0]  here represents base [26:12]
#define B_PCH_SPI_FREG1_LIMIT_MASK          0x7FFF0000                    ///< Size, [30:16] here represents limit[26:12]
#define B_PCH_SPI_FREG1_BASE_MASK           0x00007FFF                    ///< Base, [14:0]  here represents base [26:12]
#define B_PCH_SPI_FREG2_LIMIT_MASK          0x7FFF0000                    ///< Size, [30:16] here represents limit[26:12]
#define B_PCH_SPI_FREG2_BASE_MASK           0x00007FFF                    ///< Base, [14:0]  here represents base [26:12]
/**
  Passing information to flash program on whether
  if current BIOS (AT-p capable) can be flashed with
  other BIOS ROM file
  AT-p: (Anti-Theft PC Protection).

  @retval EFI_SUCCESS   Success returns.
**/
EFI_STATUS
FbtsGetATpInformation (
  VOID
  );

/**
  Get platform and Rom file flash descriptor region.

  @retval EFI_SUCCESS   Success returns.
**/
EFI_STATUS
GetRomFileAndPlatformTable (
  VOID
  );

/**
  Get Flash table from Rom file.
  if DescriptorMode is true, the FlashTable will be filled.
  if the descriptor is false,the FlashTable will be filled RegionTypeEos(0xff) directly.

  @param[in]  InputDataBuffer    the pointer to Rom file.
  @param[in]  DataBuffer         IN: the input buffer address.
                                 OUT:the flash region table from rom file.
  @param[in]  DescriptorMode     TRUE is indicate this Rom file is descriptor mode
                                 FALSE is indicate this Rom file is non-descriptor mode

  @retval EFI_SUCCESS            Successfully returns
**/
EFI_STATUS
GetRomFileFlashTable (
  IN       UINT8           *InputDataBuffer,
  IN OUT   FLASH_REGION    *DataBuffer,
  IN       BOOLEAN         DescriptorMode
  );

/**
  Initialize Fbts relative services

  @retval EFI_SUCCESS        Initialize Fbts services successful.
  @return Others             Any error occurred.
**/
EFI_STATUS
FbtsInit (
  VOID
  );

/**
  Initialize Fbts2 relative services

  @retval EFI_SUCCESS        Initialize Fbts services successful.
  @return Others             Any error occurred.
**/
EFI_STATUS
Fbts2Init (
  VOID
  );

EFI_STATUS
ImageCheck (
  VOID
  );

EFI_STATUS
FbtsApHookForBios (
  VOID
  );

EFI_STATUS
GetPlatformInfoHook (
  VOID
  );

/**
  AH=1Eh, Chipset Get whole BIOS ROM map.

  @retval EFI_SUCCESS                 FBTS get BIOS ROM map success.
  @return Others                      FBTS get BIOS ROM map failed.
**/
EFI_STATUS
EFIAPI
FbtsGetWholeBiosRomMap (
  VOID
  );
#endif
