/** @file
  This driver provides some functions for Smart Tool

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "H2oUveSmi.h"
#include <Protocol/H2oUveSmiHandler.h>


H2OUVE_SMI_HANDLER_PROTOCOL     *mH2oUveSmiHandler = NULL;

EFI_STATUS
EFIAPI
H2oUveSmiHandlerCallback (
  IN CONST EFI_GUID                       *Protocol,
  IN VOID                                 *Interface,
  IN EFI_HANDLE                           Handle
)
{
  EFI_STATUS                      Status  = EFI_SUCCESS;
  EFI_EVENT                       H2oUveSmiEvent = NULL;

  if (NULL == gSmst) {
    return EFI_NOT_READY;
  }
  if (NULL == Protocol) {
    return EFI_INVALID_PARAMETER;
  }
  if (NULL == Handle) { // 1st function call w/ Handle = NULL
    Status = gSmst->SmmLocateProtocol ( (EFI_GUID *) Protocol, NULL, (VOID **) &Interface);

    if (EFI_ERROR (Status)) {
      Status = gSmst->SmmRegisterProtocolNotify (
                 (EFI_GUID *) Protocol,
                 H2oUveSmiHandlerCallback,
                 &H2oUveSmiEvent
               );
      Interface = NULL;
    }
  }
  mH2oUveSmiHandler = Interface;

  return Status;
}


EFI_STATUS
EFIAPI
H2oUveSmiServiceInit (
  VOID
)
{
  EFI_STATUS                            Status;

  Status = H2oUveSmiHandlerCallback (&gH2oUveSmiHandlerProtocolGuid, NULL, NULL);
  
  Status = mH2OIhisi->RegisterCommand(0x52, H2oUveSmiService, IhisiNormalPriority);
  ASSERT_EFI_ERROR (Status);

  Status = mH2OIhisi->RegisterCommand(0x53, H2oUveSmiService, IhisiNormalPriority);
  ASSERT_EFI_ERROR (Status);

  return Status;
}


EFI_STATUS
H2oUveSmiService (
  VOID
)
/*++

Routine Description:

  Implement IHISI SPEC. AH=52h, H2O UEFI variable edit SMI service - Confirm the legality of the variables.
  Implement IHISI SPEC. AH=53h, H2O UEFI variable edit SMI service - Boot information-related services.

Arguments:

  None

Returns:

--*/
{
  EFI_STATUS                      Status  = EFI_SUCCESS;
  UINT64                          Rax     = 0;
  UINT64                          Rcx     = 0;
  UINT64                          Rsi     = 0;
  UINT64                          Rdi     = 0;

  if (NULL == gSmst) {
    // The new ihisi architecture does not require it. 
    // IhisiLibErrorCodeHandler (FbtsLibStatusTranslation (EFI_NOT_READY));
    return EFI_NOT_READY;
  }

  if (NULL == mH2oUveSmiHandler) {
    Status = gSmst->SmmLocateProtocol (
               &gH2oUveSmiHandlerProtocolGuid,
               NULL,
               (VOID **) &mH2oUveSmiHandler
             );
    if (EFI_ERROR (Status) || (NULL == mH2oUveSmiHandler)) {
      // The new ihisi architecture does not require it. 
      // IhisiLibErrorCodeHandler (FbtsLibStatusTranslation (EFI_NOT_READY));
      return EFI_NOT_READY;
    }
  }

  Rax = (UINT64) (UINTN) mH2OIhisi->ReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RAX);
  Rcx = (UINT64) (UINTN) mH2OIhisi->ReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RCX);
  Rsi = (UINT64) (UINTN) mH2OIhisi->ReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RSI);
  Rdi = (UINT64) (UINTN) mH2OIhisi->ReadCpuReg32 (EFI_SMM_SAVE_STATE_REGISTER_RDI);

  Status = mH2oUveSmiHandler->Function (Rax, Rcx, Rsi, Rdi);
  // The new ihisi architecture does not require it. 
  // IhisiLibErrorCodeHandler (FbtsLibStatusTranslation (Status));
  return Status;
}

