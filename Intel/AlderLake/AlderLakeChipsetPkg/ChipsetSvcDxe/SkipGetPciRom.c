/** @file
 DXE Chipset Services Library.

 This file contains only one function that is DxeCsSvcSkipGetPciRom().
 The function DxeCsSvcSkipGetPciRom() use chipset services to return 
 a PCI ROM image for the device is represented or not.

;***************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/UefiBootServicesTableLib.h>
#include <Library/PciCf8Lib.h>
#include <Library/PciExpressLib.h>
#include <Protocol/SetupUtility.h>
#include <IndustryStandard/Pci30.h>
#include <ChipsetSetupConfig.h>
#include <Library/DxeInsydeChipsetLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <IndustryStandard/Pci22.h>
/**
 Skip to load PCI ROM for specify device.

 @param[in]         Segment             The PCI segment number for PCI device.
 @param[in]         Bus                 The PCI bus number for PCI device.
 @param[in]         Device              The PCI device number for PCI device.
 @param[in]         Function            The PCI function number for PCI device.
 @param[in]         VendorId            The vendor ID for PCI device.
 @param[in]         DeviceId            The device ID for PCI device.
 @param[in, out]    SkipGetPciRom       If SkipGetPciRom == TRUE means that there is no ROM in this device.

 @retval            EFI_SUCCESS         The operation completed successfully.
 @retval            EFI_PROTOCOL_ERROR  Can't find setup utility protocol.
*/
EFI_STATUS
SkipGetPciRom (
  IN  UINTN        Segment,
  IN  UINTN        Bus,
  IN  UINTN        Device,
  IN  UINTN        Function,
  IN  UINT16       VendorId,
  IN  UINT16       DeviceId,
  OUT BOOLEAN     *SkipGetPciRom
  )
{
  EFI_STATUS                  Status;
  UINT16                      DeviceClass;

  CHIPSET_CONFIGURATION        *SystemConfiguration = NULL;

  //
  //Get NVRAM data
  //
  SystemConfiguration = AllocateZeroPool (sizeof (CHIPSET_CONFIGURATION));
  if (SystemConfiguration == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = GetChipsetSetupVariableDxe (SystemConfiguration, sizeof (CHIPSET_CONFIGURATION));
  if (EFI_ERROR (Status)) {
    FreePool (SystemConfiguration);
    ASSERT_EFI_ERROR (Status);
    return Status;
  }
  DeviceClass = PciExpressRead16 (PCI_EXPRESS_LIB_ADDRESS (Bus, Device, Function, 0x0A));

  //
  // Do not run AHCI Option ROM if AhciOptionRomSupport is disabled
  //
  //
  // Run PXE ROM only if Boot network is enabled
  //
  if ((SystemConfiguration->PxeBootToLan == 0) && (DeviceClass == ((PCI_CLASS_NETWORK << 8) | PCI_CLASS_NETWORK_ETHERNET))
     ) {
    *SkipGetPciRom = TRUE;
  }

  if (SystemConfiguration != NULL) {
    FreePool (SystemConfiguration);
  }
  return EFI_SUCCESS;
}
