/** @file
 DXE Chipset Services Library.

 This file contains only one function that is DxeCsSvcProgramChipsetSsid().
 The function DxeCsSvcProgramChipsetSsid() use chipset services to program subsystem vendor identification.

;***************************************************************************
;* Copyright (c) 2014 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/S3BootScriptLib.h>
#include <Library/IoLib.h>
#include <Protocol/PciRootBridgeIo.h>
#include <IndustryStandard/Pci30.h>
//[-start-190613-IB16990059-add]//
#include <ChipsetAccess.h>
#include <Library/PciSegmentLib.h>
//[-end-190613-IB16990059-add]//

#define DEVICE_ID_DONT_CARE           0xFFFF

typedef
VOID
(*SPECIAL_PROGRAM_MECHANISM) (
  IN     UINT8    Bus,
  IN     UINT8    Dev,
  IN     UINT8    Func,
  IN     UINT32   SsidSvid
  );

typedef struct {
  UINT16                       VendorId;
  UINT16                       DeviceId;
  SPECIAL_PROGRAM_MECHANISM    SpecialSsidSvidFunction;
} SPECIFIC_SSID_SVID_TABLE;

///**
// EHCI SSID/SVID Special Program Function
//
// @param[in]         Bus                 PCI Bus number
// @param[in]         Dev                 PCI Device number
// @param[in]         Func                PCI Function number
// @param[in]         SsidVid             SsidVid
//
// @retval            EFI_SUCCESS         Function returns successfully
//*/
//EFI_STATUS
//EhciSsidSvidSpecialProgramFunction (
//  IN UINT8    Bus,
//  IN UINT8    Dev,
//  IN UINT8    Func,
//  IN UINT32   SsidSvid
//  )
//{
//
//
//  return EFI_SUCCESS;
//}
/**
 XHCI SSID/SVID Special Program Function

 @param[in]         Bus                 PCI Bus number
 @param[in]         Dev                 PCI Device number
 @param[in]         Func                PCI Function number
 @param[in]         SsidVid             SsidVid

 @retval            EFI_SUCCESS         Function returns successfully
*/
EFI_STATUS
XhciSsidSvidSpecialProgramFunction (
  IN UINT8    Bus,
  IN UINT8    Dev,
  IN UINT8    Func,
  IN UINT32   SsidSvid
  )
{
//[-start-190613-IB16990059-add]//
  UINT64				BaseAddress;
  //
  // Program SSID & SVID as DID & VID.
  //
  BaseAddress = PCI_SEGMENT_LIB_ADDRESS (0, Bus, Dev, Func, 0);
  PciSegmentWrite32 (BaseAddress + PCI_SUBSYSTEM_VENDOR_ID_OFFSET, SsidSvid);
//[-end-190613-IB16990059-add]//
  S3BootScriptSavePciCfgWrite (
    S3BootScriptWidthUint32,
    (UINTN) (EFI_PCI_ADDRESS (Bus, Dev, Func, PCI_SUBSYSTEM_VENDOR_ID_OFFSET)),
    1,
    &SsidSvid
    );

  return EFI_SUCCESS;
}

/**
 IGD SSID/SVID Special Program Function

 @param[in]         Bus                 PCI Bus number
 @param[in]         Dev                 PCI Device number
 @param[in]         Func                PCI Function number
 @param[in]         SsidVid             SsidVid

 @retval            EFI_SUCCESS         Function returns successfully
*/
EFI_STATUS
IgdSsidSvidSpecialProgramFunction (
  IN UINT8    Bus,
  IN UINT8    Dev,
  IN UINT8    Func,
  IN UINT32   SsidSvid
  )
{
//[-start-190613-IB16990059-add]//
  UINT64				BaseAddress;
  //
  // Program SSID / SSVID
  //
  BaseAddress = PCI_SEGMENT_LIB_ADDRESS (0, Bus, Dev, Func, 0);  
  PciSegmentWrite32 (BaseAddress + PCI_SUBSYSTEM_VENDOR_ID_OFFSET, SsidSvid);
//[-end-190613-IB16990059-add]//
  S3BootScriptSavePciCfgWrite (
    S3BootScriptWidthUint32,
    (UINTN) (EFI_PCI_ADDRESS (Bus, Dev, Func, PCI_SUBSYSTEM_VENDOR_ID_OFFSET)),
    1,
    &SsidSvid
    );

  return EFI_SUCCESS;
}
SPECIFIC_SSID_SVID_TABLE mSpecificSsidSvidTable[] ={
//  {V_SA_IGD_VID         , V_SA_IGD_DID                    , IgdSsidSvidSpecialProgramFunction},
//  {V_PCH_USB_VENDOR_ID, V_PCH_H_USB_DEVICE_ID_XHCI_1     , XhciSsidSvidSpecialProgramFunction},
//  {V_PCH_USB_VENDOR_ID, V_PCH_LP_USB_DEVICE_ID_XHCI_1    , XhciSsidSvidSpecialProgramFunction},
  {DEVICE_ID_DONT_CARE  , DEVICE_ID_DONT_CARE   , NULL}
};

/**
 Program Chipset SSID

 @param[in]         Bus                 PCI Bus number
 @param[in]         Dev                 PCI Device number
 @param[in]         Func                PCI Function number
 @param[in]         VendorId            Vendor ID
 @param[in]         DeviceId            Device ID
 @param[in]         ClassCode           PCI Class Code
 @param[in]         SsidVid             SsidVid

 @retval            EFI_SUCCESS         Function returns successfully
 @retval            EFI_UNSUPPORTED     1. The specific ID is not find.
                                        2. The specific device can not be set SSID.
*/
EFI_STATUS
ProgramChipsetSsid (
  IN UINT8                               Bus,
  IN UINT8                               Dev,
  IN UINT8                               Func,
  IN UINT16                              VendorId,
  IN UINT16                              DeviceId,
  IN UINT32                              SsidSvid
  )
{
  UINT8                               Index;

  for (Index = 0; mSpecificSsidSvidTable[Index].SpecialSsidSvidFunction != NULL; Index++ ) {
    if (mSpecificSsidSvidTable[Index].VendorId == VendorId) {
      if ((mSpecificSsidSvidTable[Index].DeviceId == DEVICE_ID_DONT_CARE)
        || (mSpecificSsidSvidTable[Index].DeviceId == DeviceId)) {
        mSpecificSsidSvidTable[Index].SpecialSsidSvidFunction (Bus, Dev, Func, SsidSvid);
        return EFI_SUCCESS;
      }
    }
  }
  return EFI_UNSUPPORTED;
}
