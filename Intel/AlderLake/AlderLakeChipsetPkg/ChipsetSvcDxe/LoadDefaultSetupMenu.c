/** @file
 DXE Chipset Services Library.

 This file contains only one function that is LoadDefaultSetupMenu().
 The function LoadDefaultSetupMenu() use chipset services to delete setup
 relative variables and then let system to load variable default.

;***************************************************************************
;* Copyright (c) 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Uefi.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <SetupVariable.h>

#pragma pack(1)
typedef struct {
  CHAR16                     VariableName[100];
  EFI_GUID                   VariableGuid;
} RC_SETUP_VARIABLE_INFO;
#pragma pack()

/**
 Delete setup relative variables and then let system to load variable default.

 @param             VOID

 @retval            EFI_SUCCESS          This function always return successfully.
*/
EFI_STATUS
LoadDefaultSetupMenu (
  VOID
  )
{
  EFI_STATUS                      Status;
  UINTN                           Index;
  UINTN                           MaxIndex;
  UINTN                           VariableSize;
  RC_SETUP_VARIABLE_INFO          RcSetupVariableInfo[] = {
                                     {SA_SETUP_VARIABLE_NAME,            SA_SETUP_GUID},
                                     {ME_SETUP_VARIABLE_NAME,            ME_SETUP_GUID},
                                     {CPU_SETUP_VARIABLE_NAME,           CPU_SETUP_GUID},
                                     {PCH_SETUP_VARIABLE_NAME,           PCH_SETUP_GUID},
                                     {ME_SETUP_STORAGE_VARIABLE_NAME,    ME_SETUP_GUID},
                                     {PLATFORM_SETUP_VARIABLE_NAME,      SETUP_GUID},
                                     {L"SiSetup",                        SI_SETUP_GUID},
                                     {L"DebugConfigData",                DEBUG_CONFIG_GUID}};

  MaxIndex = sizeof (RcSetupVariableInfo) / sizeof (RC_SETUP_VARIABLE_INFO);
  for (Index = 0; Index < MaxIndex; Index++) {
    //
    // Is variable existence?
    //
    VariableSize = 0;
    Status = gRT->GetVariable (
                    RcSetupVariableInfo[Index].VariableName,
                    &RcSetupVariableInfo[Index].VariableGuid,
                    NULL,
                    &VariableSize,
                    NULL
                    );
    if (Status != EFI_BUFFER_TOO_SMALL) {
      //
      // No variable found, so this variable is default now.
      // continue to handle next RC setup variable.
      //
      continue;
    }

    //
    // Delete "RC Setup" variable. It will load default in GetSystemConfigurationVar() of SetupUtility.c
    //
    VariableSize = 0;
    Status = gRT->SetVariable (
                    RcSetupVariableInfo[Index].VariableName,
                    &RcSetupVariableInfo[Index].VariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    VariableSize,
                    NULL
                    );
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  return EFI_SUCCESS;
}
