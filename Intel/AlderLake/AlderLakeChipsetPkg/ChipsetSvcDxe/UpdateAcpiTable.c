/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Advanced ACPI Driver

@copyright
  INTEL CONFIDENTIAL
  Copyright 2011 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Uefi.h>
#include <AcpiCommon.h>
#include <Library/HobLib.h>
#include <Guid/HobList.h>
#include <Protocol/FirmwareVolume2.h>
#include <Protocol/S3SaveState.h>
#include <PlatformBoardId.h>
#include <IndustryStandard/AcpiAml.h>
#include <IndustryStandard/Pci30.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPcieRpLib.h>
#include <Library/SerialIoAccessLib.h>
#include <Library/GpioLib.h>
#include <Library/EspiLib.h>
#include <Library/GbeLib.h>
//[-start-200420-IB17800058-2-modify]//
//ADL RC 1181.1 have new Nhl  Audio Feature, temporary remove.
//#include <Library/DxeInstallNhltTableLib.h>
//[-end-200420-IB17800058-2-modify//
#include <Library/MemoryAllocationLib.h>
#include <MeBiosPayloadHob.h>
#include <PlatformBoardConfig.h>
#include <Library/CpuPlatformLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/DxeOemSvcChipsetLib.h>
#include <Library/AcpiPlatformLib.h>
#include <SetupVariable.h>
#include <..\Acpi\AcpiTables\Mcfg\Mcfg.h>
#include "UpdateAcpiTable.h"
#include <PlatformBoardType.h>
#include <Protocol/VariableLock.h>
#if FixedPcdGetBool(PcdITbtEnable) == 1
#include <TcssDataHob.h>
#endif
#include <Include/Library/UefiLib.h>
#include <Protocol/CRPolicy.h>
#include <Protocol/ConsoleRedirectionService.h>
#include <IndustryStandard/SerialPortConsoleRedirectionTable.h>
#include <Register/MeRegs.h>
#include <Protocol/AcpiSupport.h>
#include <Library/IpuLib.h>
#include <Library/GnaInfoLib.h>
#include <Library/VmdInfoLib.h>
#include <Library/DxeInsydeChipsetLib.h>
#include <ChipsetSetupConfig.h>
#include <Library/VariableLib.h>
//#include <SgxSetupData.h>
#include <Register/PchRegs.h>
#include <Library/SataLib.h>
#include <Register/SataRegs.h>
#include <Register/GbeRegs.h>
#include <Register/IpuRegs.h>
#include <Library/GraphicsInfoLib.h>
#include "GenSsdtLib.h"

//
// Global variables
//
GLOBAL_REMOVE_IF_UNREFERENCED VOID                                      *mGfxNvsAddress;
GLOBAL_REMOVE_IF_UNREFERENCED ME_SETUP                                  mMeSetup;
GLOBAL_REMOVE_IF_UNREFERENCED PCH_SETUP                                 mPchSetup;
GLOBAL_REMOVE_IF_UNREFERENCED SETUP_DATA                                mSystemConfiguration;
GLOBAL_REMOVE_IF_UNREFERENCED CHIPSET_CONFIGURATION                     mChipsetSetupData;
GLOBAL_REMOVE_IF_UNREFERENCED PLATFORM_NVS_AREA_PROTOCOL                mPlatformNvsAreaProtocol;

GLOBAL_REMOVE_IF_UNREFERENCED EFI_CPU_IO2_PROTOCOL                      *mCpuIo;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_CPU_ID_ORDER_MAP                      *mCpuApicIdOrderTable;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_CPU_APIC_ID_REORDER_MAP               *mCpuApicIdReorderTable;
GLOBAL_REMOVE_IF_UNREFERENCED UINTN                                     mNumberOfCpus;
GLOBAL_REMOVE_IF_UNREFERENCED UINTN                                     mNumberOfEnabledCpus;
GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN                                   IsX2ApicMode;

GLOBAL_REMOVE_IF_UNREFERENCED EFI_CPU_PACKAGE_INFO                      mSbspPackageInfo;
#if FixedPcdGetBool(PcdITbtEnable) == 1
GLOBAL_REMOVE_IF_UNREFERENCED TCSS_DATA_HOB                             *mTcssHob;
#endif

BOOLEAN                                                                 mIsInitialized;
// extern EFI_GUID                                                         gEpcBiosDataGuid;
// extern EFI_GUID                                                         gEpcOsDataGuid;
/*
  This function will determine whether Native ASPM is supported on the platform.

  @param[in] SystemConfiguration     System configuration data

  @return TRUE if Native ASPM is supported, FALSE otherwise
*/
BOOLEAN
IsNativeAspmSupported (
//  CONST SETUP_DATA* SystemConfiguration
  )
{
//  if (SystemConfiguration->PciExpNative == 0) {
  if (mSystemConfiguration.PciExpNative == 0) {
    return FALSE;
  }
//  switch (SystemConfiguration->NativeAspmEnable) {
  switch (mSystemConfiguration.NativeAspmEnable) {
  case 0:
    return FALSE;
  case 1:
    return TRUE;
  case 2: /* AUTO */
    return PchIsPcieNativeAspmSupported ();
  default:
    ASSERT (0);
  }
  return TRUE;
}

/**
  This function will provide the EC GPE number for ECDT update according to platform design

  @retval EcGpeNumber     EC GPE number to be reported in ECDT
**/
UINT32
GetEcGpeNumber (
  VOID
  )
{
  UINT32                                EcGpeNumber;

  //
  // EC GPE number assignment should be same as Method _GPE under ACPI EC Device which is used for OS without ECDT support
  //
  if (IsEspiEnabled ()) {
    EcGpeNumber = 0x6E;
  } else {
    EcGpeNumber = 23;
    if (mPlatformNvsAreaProtocol.Area->PcdSmcRuntimeSciPin != 0) {
      GpioGetGpeNumber ((GPIO_PAD) mPlatformNvsAreaProtocol.Area->PcdSmcRuntimeSciPin, &EcGpeNumber);
    }
  }

  DEBUG((DEBUG_INFO, "ACPI EcGpeNumber = 0x%x\n ", EcGpeNumber));

  return EcGpeNumber;
}

/**
  This function will update any runtime platform specific information.
  This currently includes:
    Setting OEM table values, ID, table ID, creator ID and creator revision.
    Enabling the proper processor entries in the APIC tables
  It also indicates with which ACPI table version the table belongs.

  @param[in] Table        The table to update
  @param[in] Version      Where to install this table

  @retval EFI_SUCCESS     Updated tables commplete.
**/
EFI_STATUS
PlatformUpdateTables (
  IN OUT EFI_ACPI_COMMON_HEADER       *Table,
  IN OUT EFI_ACPI_TABLE_VERSION       *Version
  )
{
  EFI_ACPI_DESCRIPTION_HEADER                 *TableHeader;
  UINT8                                       *CurrPtr;
  UINT8                                       *TmpDsdtPointer;
  UINT8                                       *EndPtr;
  EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC_STRUCTURE *ApicPtr;
  EFI_STATUS                                  Status;
  EFI_MP_SERVICES_PROTOCOL                    *MpService;

  UINT8                                       MaximumDsdtPointLength;

  UINT32                                      HpetBaseAdress;
  UINT32                                      LocalApicCounter;
  //UINT32                                      GcsRegister;
  SA_SETUP                                    SaSetup;
  UINTN                                       VariableSize;


  CurrPtr           = NULL;
  EndPtr            = NULL;
  ApicPtr           = NULL;

  LocalApicCounter  = 0;
  MaximumDsdtPointLength = 20;

  VariableSize = sizeof (SA_SETUP);
  Status = gRT->GetVariable (
                  L"SaSetup",
                  &gSaSetupVariableGuid,
                  NULL,
                  &VariableSize,
                  &SaSetup
                  );
  //
  // This will be accurate except for FACS structure
  //
  TableHeader = (EFI_ACPI_DESCRIPTION_HEADER *) Table;

  //
  // Update the OEM and creator information for every table except FACS.
  //
  if (Table->Signature != EFI_ACPI_1_0_FIRMWARE_ACPI_CONTROL_STRUCTURE_SIGNATURE) {
    if ((Table->Signature == EFI_ACPI_1_0_SECONDARY_SYSTEM_DESCRIPTION_TABLE_SIGNATURE) &&
        (CompareMem (&TableHeader->OemId, "Rtd3", 4) == 0)) {
      // For SsdtRtd3 table, "OemId" update is done later
    } else {
      CopyMem (TableHeader->OemId, PcdGetPtr (PcdAcpiDefaultOemId), sizeof (TableHeader->OemId));
    }

    //
    // Skip OEM table ID and creator information for SSDT tables, since these are
    // created by an ASL compiler and the creator information is useful.
    //
    if (Table->Signature != EFI_ACPI_1_0_SECONDARY_SYSTEM_DESCRIPTION_TABLE_SIGNATURE) {
      //
      // Set through patchable PCD value
      //
      TableHeader->OemTableId = PcdGet64 (PcdAcpiDefaultOemTableId);
      TableHeader->OemRevision = PcdGet32(PcdAcpiDefaultOemRevision);

      //
      // Update creator information
      //
       TableHeader->CreatorId        = PcdGet32 (PcdAcpiDefaultCreatorId);
       TableHeader->CreatorRevision  = PcdGet32 (PcdAcpiDefaultCreatorRevision) ;
    }
  }

  //
  // Locate the MP services protocol
  // Find the MP Protocol. This is an MP platform, so MP protocol must be there.
  //
  Status = gBS->LocateProtocol (
                  &gEfiMpServiceProtocolGuid,
                  NULL,
                  (VOID **) &MpService
                  );
  ASSERT_EFI_ERROR (Status);

  //
  // Get supported CPU Interrupt model (xApic or x2Apic)
  //
  IsX2ApicMode = (GetApicMode() == LOCAL_APIC_MODE_X2APIC) ? TRUE : FALSE;

  //
  // By default, a table belongs in all ACPI table versions published.
  // Some tables will override this because they have different versions of the table.
  //
  *Version = EFI_ACPI_TABLE_VERSION_1_0B | EFI_ACPI_TABLE_VERSION_2_0 | EFI_ACPI_TABLE_VERSION_3_0;

  //
  // Update the various table types with the necessary updates
  //
  switch (Table->Signature) {

  case EFI_ACPI_1_0_APIC_SIGNATURE:
    //
    // if not MP and not APIC then don't publish the APIC tables.
    //
      //@todo mSystemConfiguration.ApicEnabled default value is 1. So, the following logic is not executed.
      //if (mSystemConfiguration.ApicEnabled == 0) {
      //  *Version = EFI_ACPI_TABLE_VERSION_NONE;
      //  break;
      //}
    CurrPtr = (UINT8 *) &((EFI_ACPI_DESCRIPTION_HEADER *) Table)[1];
    CurrPtr = CurrPtr + 8;
    //
    // Size of Local APIC Address & Flag
    //
    EndPtr  = (UINT8 *) Table;
    EndPtr  = EndPtr + Table->Length;

    ApicPtr = (EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC_STRUCTURE *) CurrPtr;
    DEBUG((DEBUG_INFO, "ApicPtr Type = %x \n ", ApicPtr->Type));
    if ((IsX2ApicMode && ApicPtr->Type == EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC) ||(!IsX2ApicMode && ApicPtr->Type == EFI_ACPI_6_3_PROCESSOR_LOCAL_X2APIC)) {
      // Lets skip the MADT table in below cases:
      // 1. MADT table with Local APIC records when X2Apic Mode is enabled.
      // 2. MADT table with x2Apic records when xApic Mode is enabled.

      *Version = EFI_ACPI_TABLE_VERSION_NONE;
      break;
    }

    //
    // Call for Local APIC ID Reorder
    //
    SortCpuLocalApicInTable(MpService);

    while (CurrPtr < EndPtr) {
      ApicPtr = (EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC_STRUCTURE *) CurrPtr;

      //
      // Check table entry type
      //
      if ((ApicPtr->Type == EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC) || (ApicPtr->Type == EFI_ACPI_6_3_PROCESSOR_LOCAL_X2APIC)) {
        AppendCpuMapTableEntry (
          ApicPtr,
          LocalApicCounter
          );
        LocalApicCounter++;
      }

      //
      // Go to the next structure in the APIC table
      //
      CurrPtr = CurrPtr + ApicPtr->Length;
    }
    break;

  case EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE_SIGNATURE:
    //
    // Fix up all FACP Table values if configuration requires it.
    // This code fixes up the following Table values:
    // (1) C2/C3/CST Enable FACP values
    // (2) RTC S4 Flag
    //
    {
      EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *FadtPointer;

      FadtPointer = (EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *) Table;

      //
      // Check the version of the table
      //
      *Version = EFI_ACPI_TABLE_VERSION_NONE;
      if (FadtPointer->Header.Revision == EFI_ACPI_1_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_NONE;
      } else if (FadtPointer->Header.Revision == EFI_ACPI_2_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_1_0B | EFI_ACPI_TABLE_VERSION_2_0;
      } else if (FadtPointer->Header.Revision == EFI_ACPI_3_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_1_0B | EFI_ACPI_TABLE_VERSION_3_0;
      } else if (FadtPointer->Header.Revision == EFI_ACPI_5_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_1_0B | EFI_ACPI_TABLE_VERSION_5_0;
      } else if (FadtPointer->Header.Revision == EFI_ACPI_6_1_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_5_0;
      } else if (FadtPointer->Header.Revision == EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_5_0;
      }

      //
      // Modify MSI capability based on setup
      //
      if (mSystemConfiguration.EnableMsiInFadt == 0) {
        FadtPointer->IaPcBootArch |= (1 << 3);
      }

      //
      // Modify Preferred_PM_Profile field based on Board SKU's. Default is set to Mobile
      //
      FadtPointer->PreferredPmProfile = PcdGet8 (PcdPreferredPmProfile);

      //
      // if Native ASPM is disabled, set FACP table to skip Native ASPM
      //
      if (!IsNativeAspmSupported ()) {
        FadtPointer->IaPcBootArch |= 0x10;
      }

      //
      // PME WAKE supported, set PCI_EXP_WAK, BIT14 of Fixed feature flags.
      //
      FadtPointer->Flags |= (EFI_ACPI_6_0_PCI_EXP_WAK);

      if (mSystemConfiguration.LowPowerS0Idle) {
        //
        // The Flags field within the FADT (offset 112)
        //   1) will have a new Low Power S0 Idle Capable ACPI flag (bit offset 21).
        //
        FadtPointer->Flags = (BIT21 | FadtPointer->Flags);
        //
        //Only passive docking available in Conected Standby mode. Clear Docking capability Bit
        //
        FadtPointer->Flags &= ~BIT9;
        //
        // set indicates the power button is handled as a control method device.
        //
        FadtPointer->Flags |= EFI_ACPI_2_0_PWR_BUTTON;
      } else {
        FadtPointer->Flags &= ~(EFI_ACPI_2_0_PWR_BUTTON); // clear indicates the power button is handled as a fixed feature programming model
      }

      //
      // 1. set header revision.
      //
//[-start-21523-IB05660169-remove]//
////      FadtPointer->Header.Revision = EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE_REVISION;
//[-end-21523-IB05660169-remove]//

      //
      // 2. set all GAR register AccessSize to valid value.
      //
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->ResetReg.AccessSize    = EFI_ACPI_6_3_BYTE;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPm1aEvtBlk.AccessSize = EFI_ACPI_6_3_WORD;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPm1bEvtBlk.AccessSize = EFI_ACPI_6_3_WORD;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPm1aCntBlk.AccessSize = EFI_ACPI_6_3_WORD;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPm1bCntBlk.AccessSize = EFI_ACPI_6_3_WORD;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPm2CntBlk.AccessSize  = EFI_ACPI_6_3_BYTE;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPmTmrBlk.AccessSize   = EFI_ACPI_6_3_DWORD;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XGpe0Blk.AccessSize    = EFI_ACPI_6_3_BYTE;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XGpe1Blk.AccessSize    = EFI_ACPI_6_3_BYTE;

      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepControlReg.AddressSpaceId    = 0x1;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepControlReg.RegisterBitWidth  = 0x8;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepControlReg.RegisterBitOffset = 0;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepControlReg.AccessSize        = EFI_ACPI_6_3_DWORD;

        ((EFI_ACPI_5_0_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepControlReg.Address           = FixedPcdGet16 (PcdPerfPkgAcpiIoPortBaseAddress) + 4;

      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepStatusReg.AddressSpaceId     = 0x1;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepStatusReg.RegisterBitWidth   = 0x8;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepStatusReg.RegisterBitOffset  = 0;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepStatusReg.AccessSize         = EFI_ACPI_6_3_DWORD;

        ((EFI_ACPI_5_0_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepStatusReg.Address            = FixedPcdGet16 (PcdPerfPkgAcpiIoPortBaseAddress);

      //
      // Set the S4BiosReq to 0 as we don't support it in the FACS for TGL.
      //
      FadtPointer->S4BiosReq = 0;

      //
      //BIOS should expose either 64 bits or 32 bits address, not both.
      //
      if (FadtPointer->XPm1aEvtBlk.Address != 0) {
        FadtPointer->Pm1aEvtBlk = 0;
        FadtPointer->Pm1aCntBlk = 0;
      }

      if (FadtPointer->XPm2CntBlk.Address != 0) {
        FadtPointer->Pm2CntBlk = 0;
      }

        //@todo RtcS4WakeEnable and PmTimerEnable default value is 1. So, the following logic is not executed.
        ////
        //// Check if RTC S4 is enabled
        ////
        //if (!mSystemConfiguration.RtcS4WakeEnable) {

        //  //
        //  // Clear the S4 RTC flag
        //  //
        //  FadtPointer->Flags &= ~EFI_ACPI_2_0_RTC_S4;
        //}

        ////
        //// Check if PM timer is enabled
        ////
        //if (!mSystemConfiguration.PmTimerEnable) {

        //  //
        //  // Clear the PM timer flag
        //  //
        //  FadtPointer->Flags &= ~EFI_ACPI_3_0_USE_PLATFORM_CLOCK;
        //}
      //
//[-start-200305-IB14630342-modify]//
//[-start-200304-IB14630339-add]//
   	  //  According ACPI spec: FACP - Flag: FIX_RTC (bit 6)
   	  //  A zero indicates the RTC wake status is supported in fixed register space;
   	  //  A one indicates the RTC wake status is not supported in fixed register space.
   	  //  If the SystemTimeAndAlarmSource setting in SCU is set to Legacy RTC (value = 1),
   	  //  then we clear FIX_RTC bit.
      //  If the SystemTimeAndAlarmSource setting in SCU is set to ACPI Time and Alarm Device (value = 0),
      //  then we set FIX_RTC bit.
      //
      if (mSystemConfiguration.SystemTimeAndAlarmSource == 1) {
        FadtPointer->Flags &= ~EFI_ACPI_6_2_FIX_RTC;
      } else {
        FadtPointer->Flags |= EFI_ACPI_6_2_FIX_RTC;
      }
//[-end-200304-IB14630339-add]//
//[-end-200305-IB14630342-modify]//
    }
    break;

  case EFI_ACPI_1_0_PERSISTENT_SYSTEM_DESCRIPTION_TABLE_SIGNATURE:
    //
    // These items only belong in the 1.0 tables.
    //
    *Version = EFI_ACPI_TABLE_VERSION_1_0B;
    break;

  case EFI_ACPI_2_0_DIFFERENTIATED_SYSTEM_DESCRIPTION_TABLE_SIGNATURE:
    //
    // Fix up the AML code in the DSDT affected by end user options.
    // Fix up the following ASL Code:
    // (1)  ACPI Global NVS Memory Base and Size.
    // (2)  ACPI Graphics NVS Memory Base and Size.
    // (3)  SMBus I/O Base.
    // (4)  Thermal Management Methods.
    //
    {
      UINT8   *DsdtPointer;
      UINT32  *Signature;
      UINT8   *Operation;
//      UINT32  *Address;
      UINT8   *Value;
//      UINT16  *Size;
      BOOLEAN EnterDock = FALSE;
      //
      // Loop through the ASL looking for values that we must fix up.
      //
      CurrPtr = (UINT8 *) Table;
      for (DsdtPointer = CurrPtr;
           DsdtPointer <= (CurrPtr + ((EFI_ACPI_COMMON_HEADER *) CurrPtr)->Length);
           DsdtPointer++
          ) {
        Signature = (UINT32 *) DsdtPointer;
        switch (*Signature) {
        //
        // GNVS operation region
        //
        case (SIGNATURE_32 ('G', 'N', 'V', 'S')):
          //
          // Conditional match.  For Region Objects, the Operator will always be the
          // byte immediately before the specific name.  Therefore, subtract 1 to check
          // the Operator.
          //
//          Operation = DsdtPointer - 1;
//          if (*Operation == AML_EXT_REGION_OP) {
//            Address   = (UINT32 *) (DsdtPointer + 6);
//            *Address  = (UINT32) (UINTN) mPlatformNvsAreaProtocol.Area;
//            Size      = (UINT16 *) (DsdtPointer + 11);
//            *Size     = sizeof (PLATFORM_NVS_AREA);
//          }
          if (IsAmlOpRegionObject (DsdtPointer)) {
            SetOpRegion (DsdtPointer, mPlatformNvsAreaProtocol.Area, sizeof (PLATFORM_NVS_AREA));
          }
          break;
        case (SIGNATURE_32 ('O', 'G', 'N', 'S')):
          if (IsAmlOpRegionObject (DsdtPointer)) {
            SetOpRegion (DsdtPointer, mPlatformNvsAreaProtocol.OemArea, sizeof (OEM_PLATFORM_NVS_AREA));
          }
          break;


        case (SIGNATURE_32 ('M', 'D', 'B', 'G')):
          if ((mSystemConfiguration.AcpiDebug == 1) && (*(UINT8*) (DsdtPointer + 5) == 0x0C)) {    // Check if it's OperationRegion(\MDBG,SystemMemory,0x55AA55AA, 0x55AA55AA) in MemDbg.asl
            mSystemConfiguration.AcpiDebugAddress = *(UINT32*) (DsdtPointer + 6); // Get offset value of OperationRegion(\MDBG,SystemMemory,0x55AA55AA, 0x55AA55AA)
            {
              UINTN     VarDataSize;
              VarDataSize = sizeof(SETUP_DATA);
              Status = gRT->SetVariable (
                              L"Setup",
                              &gSetupVariableGuid,
                              EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                              VarDataSize,
                              &mSystemConfiguration
                              );
            }
          }
          break;

        //
        // _AC0 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '0')):
          //
          // Conditional match.  _AC0 is >63 and <4095 bytes, so the package length is 2 bytes.
          // Therefore, subtract 3 to check the Operator.
          //
          Operation = DsdtPointer - 3;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC0 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '0');
            }
          }
          break;

        //
        // _AL0 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '0')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL0 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '0');
            }
          }
          break;

        //
        // _AC1 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '1')):
          //
          // Conditional match.  _AC1 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC1 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '1');
            }
          }
          break;

        //
        // _AL1 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '1')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL1 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '1');
            }
          }
          break;

        //
        // _AC2 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '2')):
          //
          // Conditional match.  _AC2 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC2 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '2');
            }
          }
          break;

        //
        // _AL2 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '2')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL2 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '2');
            }
          }
          break;

        //
        // _AC3 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '3')):
          //
          // Conditional match.  _AC3 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC3 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '3');
            }
          }
          break;

        //
        // _AL3 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '3')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL3 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '3');
            }
          }
          break;

        //
        // _AC4 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '4')):
          //
          // Conditional match.  _AC4 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC4 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '4');
            }
          }
          break;

        //
        // _AL4 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '4')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL4 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '4');
            }
          }
          break;

        //
        // _AC5 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '5')):
          //
          // Conditional match.  _AC5 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC5 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '5');
            }
          }
          break;

        //
        // _AL5 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '5')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL5 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '5');
            }
          }
          break;

        //
        // _AC6 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '6')):
          //
          // Conditional match.  _AC6 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC6 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '6');
            }
          }
          break;

        //
        // _AL6 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '6')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL6 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '6');
            }
          }
          break;

        //
        // _AC7 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '7')):
          //
          // Conditional match.  _AC7 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC7 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '7');
            }
          }
          break;

        //
        // _AL7 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '7')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL7 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '7');
            }
          }
          break;

        //
        // _AC8 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '8')):
          //
          // Conditional match.  _AC8 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC8 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '8');
            }
          }
          break;

        //
        // _AL8 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '8')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL8 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '8');
            }
          }
          break;

        //
        // _AC9 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '9')):
          //
          // Conditional match.  _AC9 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC9 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '9');
            }
          }
          break;

        //
        // _AL9 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '9')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL9 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '9');
            }
          }
          break;

        //
        // _PSL method
        //
        case (SIGNATURE_32 ('_', 'P', 'S', 'L')):
          //
          // Conditional match.  _PSL is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 3;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _PSL enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'P', 'S', 'L');
            }
          }
          break;

        //
        // _PSV method
        //
        case (SIGNATURE_32 ('_', 'P', 'S', 'V')):
          //
          // Conditional match.  _PSV is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 3;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _PSV enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'P', 'S', 'V');
            }
          }
          break;

        //
        // _CRT method
        //
        case (SIGNATURE_32 ('_', 'C', 'R', 'T')):
          //
          // Conditional match.  _CRT is < 256 bytes, so the package length is 1 byte.
          // Subtract 3 to check the Operator for CRB, subract 2 for Harris Beach.
          //
          Operation = DsdtPointer - 3;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _CRT enabled
            //
            if (mSystemConfiguration.DisableCriticalTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'C', 'R', 'T');
            }
          }
          break;

        //
        // _TC1 method
        //
        case (SIGNATURE_32 ('_', 'T', 'C', '1')):
          //
          // Conditional match.  _TC1 is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _TC1 enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'C', '1');
            }
          }
          break;

        //
        // _TC2 method
        //
        case (SIGNATURE_32 ('_', 'T', 'C', '2')):
          //
          // Conditional match.  _TC2 is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _TC2 enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'C', '2');
            }
          }
          break;

        //
        // _TSP method
        //
        case (SIGNATURE_32 ('_', 'T', 'S', 'P')):
          //
          // Conditional match.  _TSP is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _TSP enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'S', 'P');
            }
          }
          break;

        //
        // Update SS3 Name with Setup value
        //
        case (SIGNATURE_32 ('S', 'S', '3', '_')):
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {
            Value   = (UINT8 *) DsdtPointer + 4;
//            *Value  = (UINT8)mSystemConfiguration.AcpiSleepState;
            *Value  = (UINT8)mChipsetSetupData.DisableAcpiS3;
          }
          break;
        //
        // Update SS4 Name with Setup value
        //
        case (SIGNATURE_32 ('S', 'S', '4', '_')):
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {
            Value   = (UINT8 *) DsdtPointer + 4;
            *Value  = mSystemConfiguration.AcpiHibernate;
          }
          break;
        //
        // _EJ0 method
        //
        case (SIGNATURE_32 ('_', 'E', 'J', '0')):
          if (mSystemConfiguration.LowPowerS0Idle) {
            //
            // Remove _EJ0 for SOC
            //
            if (*(DsdtPointer-3) == AML_METHOD_OP) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'E', 'J', '0');
              EnterDock = TRUE;
            }
          }
          break;
        //
        // _STA method for Device (\_SB.PC00.DOCK)
        //
        case (SIGNATURE_32 ('_', 'S', 'T', 'A')):
          if (mSystemConfiguration.LowPowerS0Idle) {
            //
            // Remove _STA in (\_SB.PC00.DOCK) for SOC
            //
            if ((*(DsdtPointer-3) == AML_METHOD_OP) && (EnterDock)) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'S', 'T', 'A');
              EnterDock = FALSE;
            }
          }
          break;
        //
        // _UPC method for Device (\_SB.PC00.XHCI.RHUB)
        //
        case (SIGNATURE_32('H', 'S', '1', '3')):
          for (TmpDsdtPointer = DsdtPointer;
               TmpDsdtPointer <= DsdtPointer + MaximumDsdtPointLength;
               TmpDsdtPointer++){
            Signature = (UINT32 *) TmpDsdtPointer;
            switch (*Signature) {
              case(SIGNATURE_32('U', 'P', 'C', 'P')):
                Value   = (UINT8 *)((UINT32 *)TmpDsdtPointer + 2);
                break;
              default:
                //
                // Do nothing.
                //
                break;
            }
          }
        break;


        //
        // _DCK method
        //
        case (SIGNATURE_32 ('_', 'D', 'C', 'K')):
          if (mSystemConfiguration.LowPowerS0Idle) {
            //
            // Remove _DCK for SOC
            //
            if (*(DsdtPointer-3) == AML_METHOD_OP) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'D', 'C', 'K');
            }
          }
          break;

        //
        // mask _DEP from CPU's scope if CS disabled.
        //
        case (SIGNATURE_32 ('P', 'R', '0', '0')):
        case (SIGNATURE_32 ('P', 'R', '0', '1')):
        case (SIGNATURE_32 ('P', 'R', '0', '2')):
        case (SIGNATURE_32 ('P', 'R', '0', '3')):
        case (SIGNATURE_32 ('P', 'R', '0', '4')):
        case (SIGNATURE_32 ('P', 'R', '0', '5')):
        case (SIGNATURE_32 ('P', 'R', '0', '6')):
        case (SIGNATURE_32 ('P', 'R', '0', '7')):
        case (SIGNATURE_32 ('P', 'R', '0', '8')):
        case (SIGNATURE_32 ('P', 'R', '0', '9')):
        case (SIGNATURE_32 ('P', 'R', '1', '0')):
        case (SIGNATURE_32 ('P', 'R', '1', '1')):
        case (SIGNATURE_32 ('P', 'R', '1', '2')):
        case (SIGNATURE_32 ('P', 'R', '1', '3')):
        case (SIGNATURE_32 ('P', 'R', '1', '4')):
        case (SIGNATURE_32 ('P', 'R', '1', '5')):
          if (mSystemConfiguration.LowPowerS0Idle == 0) {
            for (TmpDsdtPointer = DsdtPointer; TmpDsdtPointer <= DsdtPointer + MaximumDsdtPointLength; TmpDsdtPointer++){
              Signature = (UINT32 *) TmpDsdtPointer;
              switch (*Signature) {
                case(SIGNATURE_32('_', 'D', 'E', 'P')):
                  *(UINT8 *) TmpDsdtPointer = 'X';
                  break;
                default:
                  //
                  // Do nothing.
                  //
                  break;
              }
            }
          }
          break;

        //
        // _EDL name
        //
        case (SIGNATURE_32 ('_', 'E', 'D', 'L')):
          if (mSystemConfiguration.LowPowerS0Idle) {
            //
            // Remove _EDL for SOC
            //
            if (*(DsdtPointer-1) == AML_NAME_OP) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'E', 'D', 'L');
            }
          }
          break;

        default:
          //
          // Do nothing.
          //
          break;
        }
        Status = OemSvcUpdateDsdtAcpiTable (Signature);
      }
    }
//[-start-200204-IB16740083-modify]//
// Debug message moved here because the original place will be printed continuously, and it will reduce the boot performance.
        DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices Call: OemSvcUpdateDsdtAcpiTable \n"));
        DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices OemSvcUpdateDsdtAcpiTable Status: %r\n", Status));
//[-end-200204-IB16740083-modify]//
    break;

  case EFI_ACPI_3_0_HIGH_PRECISION_EVENT_TIMER_TABLE_SIGNATURE:
    //
    // If HPET is disabled in setup, don't publish the table.
    //
//    if (mPchSetup.Hpet == FALSE) {
//      *Version = EFI_ACPI_TABLE_VERSION_NONE;
//    } else {
      //
      // Get HPET base address
      //
      PchHpetBaseGet (&HpetBaseAdress);
      //
      // Adjust HPET Table to correct the Base Address
      //
      ((EFI_ACPI_HIGH_PRECISION_EVENT_TIMER_TABLE_HEADER*) Table)->BaseAddressLower32Bit.Address = HpetBaseAdress;
//    }
    break;

  case EFI_ACPI_3_0_PCI_EXPRESS_MEMORY_MAPPED_CONFIGURATION_SPACE_BASE_ADDRESS_DESCRIPTION_TABLE_SIGNATURE:
    //
    // Update MCFG base and end bus number
    //
    ((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->AllocationStructure[0].BaseAddress
      = PcdGet64 (PcdPciExpressBaseAddress);

    ((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->AllocationStructure[0].EndBusNumber
      = (UINT8)(PcdGet32 (PcdPciExpressRegionLength) / 0x100000) - 1;

    if ((((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->AllocationStructure[0].EndBusNumber > MAX_OS_VISIBLE_BUSES_WITH_VMD)
        && (SaSetup.VmdEnable == 1)) {
      ((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->AllocationStructure[0].EndBusNumber
        = MAX_OS_VISIBLE_BUSES_WITH_VMD;
      DEBUG((DEBUG_INFO,"VMD is enabled so restricting OS visible bus numbers to %d\n", MAX_OS_VISIBLE_BUSES_WITH_VMD));
    }

#if FixedPcdGetBool(PcdITbtEnable) == 1
    mTcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
    if (mTcssHob->TcssData.PcieMultipleSegmentEnabled) {
      ((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->AllocationStructure[1].BaseAddress
      = PcdGet64 (PcdPciExpressBaseAddress) + SIZE_256MB;
    } else
#endif
    {
      ((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->Header.Length -= sizeof (EFI_ACPI_MEMORY_MAPPED_ENHANCED_CONFIGURATION_SPACE_BASE_ADDRESS_ALLOCATION_STRUCTURE);
    }
    break;

  case EFI_ACPI_5_0_EMBEDDED_CONTROLLER_BOOT_RESOURCES_TABLE_SIGNATURE:
    //
    // If EC is not present, don't publish the table.
    //
    if (mPlatformNvsAreaProtocol.Area->EcAvailable == PLATFORM_NVS_DEVICE_DISABLE) {
      *Version = EFI_ACPI_TABLE_VERSION_NONE;
    } else {
      //
      // Update GPE Number
      //
      ((EFI_ACPI_5_0_EMBEDDED_CONTROLLER_BOOT_RESOURCES_TABLE *) Table)->GpeBit = (UINT8) GetEcGpeNumber ();
    }
    break;

/* @todo Need to uncomment once MSFT fully implements uPEP
  case SIGNATURE_32 ('L', 'P', 'I', 'T'):
    //
    // If L0s is disabled in setup, don't publish the table.
    //
    if (mSystemConfiguration.LowPowerS0Idle == 0) {
      *Version = EFI_ACPI_TABLE_VERSION_NONE;
    }
    break;
*/
  case EFI_ACPI_1_0_SECONDARY_SYSTEM_DESCRIPTION_TABLE_SIGNATURE:
  {
    UINT8   *TablePointer;
    UINT32  *Signature;
    UINT8   *Operation;

    //
    // Do not load the xHCI table. It is handled by separate function.
    //
    if (CompareMem (&TableHeader->OemTableId, "xh_", 3) == 0) {
      DEBUG((DEBUG_INFO,"TableHeader->OemTableId = %x\n ", TableHeader->OemTableId));
      *Version = EFI_ACPI_TABLE_VERSION_NONE;
    }

    //
    // Load SSDT tables for the platforms based on boardID; default to RVP tables.
    // Load Ther_Rvp for all others.
    //
    if (CompareMem (&TableHeader->OemTableId, "Ther_Rvp", 8) == 0) {
      CurrPtr = (UINT8 *) Table;
      for (TablePointer = CurrPtr;TablePointer < (CurrPtr + ((EFI_ACPI_COMMON_HEADER *) CurrPtr)->Length - sizeof (UINT32));TablePointer++)
      {
        Signature = (UINT32 *) TablePointer;
        switch (*Signature) {
        //
        // _AC0 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '0')):
          //
          // Conditional match.  _AC0 is >63 and <4095 bytes, so the package length is 2 bytes.
          // Therefore, subtract 3 to check the Operator.
          //
          Operation = TablePointer - 3;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC0 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '0');
            }
          }
          break;
        //
        // _AL0 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '0')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = TablePointer - 1;
          if (*Operation == AML_NAME_OP) {
            //
            // Check if we want _AL0 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '0');
            }
          }
          break;
        //
        // _AC1 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '1')):
          //
          // Conditional match.  _AC1 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC1 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '1');
            }
          }
          break;
        //
        // _AL1 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '1')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = TablePointer - 1;
          if (*Operation == AML_NAME_OP) {
            //
            // Check if we want _AL1 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '1');
            }
          }
          break;
        //
        // _AC2 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '2')):
          //
          // Conditional match.  _AC2 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC2 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '2');
            }
          }
          break;
        //
        // _AL2 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '2')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = TablePointer - 1;
          if (*Operation == AML_NAME_OP) {
            //
            // Check if we want _AL2 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '2');
            }
          }
          break;
        //
        // _AC3 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '3')):
          //
          // Conditional match.  _AC3 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC3 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '3');
            }
          }
          break;
        //
        // _AL3 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '3')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = TablePointer - 1;
          if (*Operation == AML_NAME_OP) {
            //
            // Check if we want _AL3 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '3');
            }
          }
          break;
        //
        // _AC4 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '4')):
          //
          // Conditional match.  _AC4 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC4 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '4');
            }
          }
          break;
        //
        // _AL4 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '4')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = TablePointer - 1;
          if (*Operation == AML_NAME_OP) {
            //
            // Check if we want _AL4 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '4');
            }
          }
          break;
        // _PSL method
        //
        case (SIGNATURE_32 ('_', 'P', 'S', 'L')):
          //
          // Conditional match.  _PSL is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 3;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _PSL enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'P', 'S', 'L');
            }
          }
          break;
        //
        // _PSV method
        //
        case (SIGNATURE_32 ('_', 'P', 'S', 'V')):
          //
          // Conditional match.  _PSV is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 3;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _PSV enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'P', 'S', 'V');
            }
          }
          break;
        //
        // _CRT method
        //
        case (SIGNATURE_32 ('_', 'C', 'R', 'T')):
          //
          // Conditional match.  _CRT is < 256 bytes, so the package length is 1 byte.
          // Subtract 3 to check the Operator for CRB, subract 2 for Harris Beach.
          //
          Operation = TablePointer - 3;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _CRT enabled
            //
            if (mSystemConfiguration.DisableCriticalTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'C', 'R', 'T');
            }
          }
          break;
        //
        // _TC1 method
        //
        case (SIGNATURE_32 ('_', 'T', 'C', '1')):
          //
          // Conditional match.  _TC1 is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _TC1 enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'C', '1');
            }
          }
          break;
        //
        // _TC2 method
        //
        case (SIGNATURE_32 ('_', 'T', 'C', '2')):
          //
          // Conditional match.  _TC2 is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _TC2 enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'C', '2');
            }
          }
          break;
        //
        // _TSP method
        //
        case (SIGNATURE_32 ('_', 'T', 'S', 'P')):
          //
          // Conditional match.  _TSP is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _TSP enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'S', 'P');
            }
          }
          break;
        }
      }
    }

    //
    // Load RTD3 SSDT table for ADL RVP SKUs
    // Note: "OemId" field is used to indentify whether SSDT table is for RTD3 usage
    //
    if ((CompareMem (&TableHeader->OemId,  "Rtd3", 4) == 0)) {
      //
      // OemId should be replaced with PcdAcpiDefaultOemId
      //
      CopyMem (TableHeader->OemId, PcdGetPtr (PcdAcpiDefaultOemId), sizeof (TableHeader->OemId));
      //
      // set default = none
      //
      *Version = EFI_ACPI_TABLE_VERSION_NONE;

      //
      // check RTD3 enabled in setup
      //
      if (mSystemConfiguration.Rtd3Support == 1) {
        //
        // match table ID and BoardId
        //

        if (TableHeader->OemTableId == PcdGet64 (PcdBoardRtd3TableSignature)) {
          DEBUG ((DEBUG_INFO, "Board SsdtRtd3 Table: %x\n", TableHeader->OemTableId));
          *Version = EFI_ACPI_TABLE_VERSION_1_0B | EFI_ACPI_TABLE_VERSION_2_0 | EFI_ACPI_TABLE_VERSION_3_0;
        }
      } // Rtd3Support
    } // Load RTD3 SSDT table
  }
    break;

  default:
    break;
  }
  return EFI_SUCCESS;
}
/**
  Convert string containing GUID in the canonical form:
  "aabbccdd-eeff-gghh-iijj-kkllmmnnoopp"
  where aa - pp are unicode hexadecimal digits
  to the buffer format to be used in ACPI, byte ordering:
  [Byte 0] gg, hh, ee, ff, aa, bb, cc, dd [Byte 7]
  [Byte 8] pp, oo, nn, mm, ll, kk, jj, ii [Byte 16]

  @param[in]  GuidString    - GUID String null terminated (aligned on a 16-bit boundary)
  @param[out] AcpiGuidPart1 - First half of buffer  (bytes 0 - 7)
  @param[out] AcpiGuidPart2 - Second half of buffer (bytes 8 - 16)

  @retval EFI_SUCCESS     - String converted successfully.
  @retval EFI_UNSUPPORTED - Wrong input string format.

**/
EFI_STATUS
GuidStringToAcpiBuffer (
  IN  CHAR16 *GuidString,
  OUT UINT64 *AcpiGuidPart1,
  OUT UINT64 *AcpiGuidPart2
  )
{
  UINT32 GuidTempPart32 = 0;
  UINT16 GuidTempPart16 = 0;
  UINT8  GuidPartIndex;

  DEBUG((DEBUG_INFO,"GuidStringToAcpiBuffer() - GUID = %s\n", GuidString));

  for (GuidPartIndex = 0; GuidPartIndex < 4; GuidPartIndex++) {
    switch (GuidPartIndex){
      case 0:
        GuidTempPart32 = SwapBytes32((UINT32)StrHexToUint64(GuidString));
        *AcpiGuidPart1 = ((UINT64)GuidTempPart32 << 0x20);
        break;
      case 1:
        GuidTempPart16 = SwapBytes16((UINT16)StrHexToUint64(GuidString));
        *AcpiGuidPart1 += ((UINT64)GuidTempPart16 << 0x10);
        break;
      case 2:
        GuidTempPart16 = SwapBytes16((UINT16)StrHexToUint64(GuidString));
        *AcpiGuidPart1 += ((UINT64)GuidTempPart16);
        break;
      case 3:
        GuidTempPart16 = (UINT16)StrHexToUint64(GuidString);
        break;
    }

    while ((*GuidString != L'-') && (*GuidString != L'\0')) {
      GuidString++;
    }

    if (*GuidString == L'-') {
      GuidString++;
    } else {
      return EFI_UNSUPPORTED;
    }
  }

  *AcpiGuidPart2 = ((UINT64)GuidTempPart16 << 0x30) + StrHexToUint64(GuidString);

  // Switch endianess because it will be swapped again in ACPI Buffer object
  *AcpiGuidPart1 = SwapBytes64(*AcpiGuidPart1);
  *AcpiGuidPart2 = SwapBytes64(*AcpiGuidPart2);

  return EFI_SUCCESS;
}

/**
  Save Acpi Setup relatived variables.
**/
EFI_STATUS
SetAcpiSetupVariables (
  IN OUT EFI_ACPI_DESCRIPTION_HEADER      *TableHeader
 )
{
 EFI_STATUS                   Status;
 UINTN                        VariableSize;


 VariableSize = sizeof (SETUP_DATA);

  mSystemConfiguration.AcpiTableRevision = TableHeader->Revision;
  Status = SetVariableToSensitiveVariable (
             L"Setup",
             &gSetupVariableGuid,
             EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
             VariableSize,
             &mSystemConfiguration
             );
 ASSERT_EFI_ERROR(Status);
 DEBUG ((DEBUG_INFO, "Acpi table revision: 0x%x, saved successfully.\n", TableHeader->Revision));

  return Status;
}

/**
  ACPI Platform driver installation function.

  @param[in] ImageHandle     Handle for this drivers loaded image protocol.
  @param[in] SystemTable     EFI system table.

  @retval EFI_SUCCESS        The driver installed without error.
  @retval EFI_ABORTED        The driver encountered an error and could not complete installation of
                             the ACPI tables.

**/
EFI_STATUS
EFIAPI
InstallAcpiPlatform (
  IN OUT EFI_ACPI_DESCRIPTION_HEADER      *TableHeader
  )
{
  EFI_STATUS                    Status;
  EFI_MP_SERVICES_PROTOCOL      *MpService;
//[-start-190620-IB11270237-add]//
  PLATFORM_NVS_AREA_PROTOCOL    *PlatformNvsArea;
  UINTN                         VarDataSize;
//[-end-190620-IB11270237-add]//
  
  mCpuApicIdOrderTable   = NULL;
  mCpuApicIdReorderTable = NULL;

//[-start-190620-IB11270237-add]//
  VarDataSize = sizeof (CHIPSET_CONFIGURATION);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSystemConfigurationGuid,
                  NULL,
                  &VarDataSize,
                  &mChipsetSetupData
                  );

  VarDataSize = sizeof (SETUP_DATA);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  NULL,
                  &VarDataSize,
                  &mSystemConfiguration
                  );

  Status = gBS->LocateProtocol (
                  &gPlatformNvsAreaProtocolGuid,
                  NULL,
                  (VOID **) &PlatformNvsArea
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }
  mPlatformNvsAreaProtocol = *PlatformNvsArea;
//[-end-190620-IB11270237-add]//
  //
  // Locate the MP services protocol
  // Find the MP Protocol. This is an MP platform, so MP protocol must be there.
  //
  Status = gBS->LocateProtocol (
                  &gEfiMpServiceProtocolGuid,
                  NULL,
                  (VOID **) &MpService
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Determine the number of processors
  //
  MpService->GetNumberOfProcessors (
              MpService,
              &mNumberOfCpus,
              &mNumberOfEnabledCpus
              );

  DEBUG ((DEBUG_INFO, "mNumberOfCpus - %x,mNumberOfEnabledCpus - %x\n", mNumberOfCpus, mNumberOfEnabledCpus));
  mCpuApicIdOrderTable   = AllocateZeroPool (mNumberOfCpus * sizeof (EFI_CPU_ID_ORDER_MAP));
  if (mCpuApicIdOrderTable == NULL) {
//[-start-201123-IB17040181-add]//
    DEBUG ((DEBUG_ERROR, "Error! Allocate space for mCpuApicIdOrderTable fail.\n"));
    ASSERT_EFI_ERROR(EFI_UNSUPPORTED);
//[-end-201123-IB17040181-add]//
    return EFI_UNSUPPORTED;
  }
  mCpuApicIdReorderTable = AllocateZeroPool (mNumberOfCpus * sizeof (EFI_CPU_APIC_ID_REORDER_MAP));
  if (mCpuApicIdReorderTable == NULL) {
//[-start-201123-IB17040181-add]//
    DEBUG ((DEBUG_ERROR, "Error! Allocate space for mCpuApicIdReorderTable fail.\n"));
    ASSERT_EFI_ERROR(EFI_UNSUPPORTED);
//[-end-201123-IB17040181-add]//
    return EFI_UNSUPPORTED;
  }
  
  //
  // Save Acpi Setup Variables
  //
  if (TableHeader->Signature == EFI_ACPI_2_0_FIXED_ACPI_DESCRIPTION_TABLE_SIGNATURE) {
    Status = SetAcpiSetupVariables ((EFI_ACPI_DESCRIPTION_HEADER *) TableHeader);
    ASSERT_EFI_ERROR (Status);
  }

  //
  // Finished
  //
  return EFI_SUCCESS;
}

/**
  Sort ordering of CPUs according to the priorities of cores and threads.
  ** ASSUMPTION: 1) single CPU package systsem wide; 2) two threads per core

  @param[in] MpService             The MP servicde protocol (used to retrievel MP info)
  @param[in] NumberOfCPUs          Number of CPUs
  @param[in] NumberOfEnabledCPUs   Number of Enabled CPUs.

  @retval EFI_SUCCESS              The driver installed without error.

**/
EFI_STATUS
SortCpuLocalApicInTable (
  IN  EFI_MP_SERVICES_PROTOCOL          *MpService
  )
{
  EFI_PROCESSOR_INFORMATION                 *MpContext;
  UINT8                                     Index;
  UINTN                                     BspIndex;
  UINT8                                     CurrProcessor;
  EFI_STATUS                                Status;
  UINT8                                     MaxCpuCore;
  UINT8                                     MaxCpuThread;

  MpContext                     = NULL;
  MaxCpuCore                    = 0;
  MaxCpuThread                  = 0;

  Status = MpService->WhoAmI (
                        MpService,
                        &BspIndex
                        );

  //
  // Fill mCpuApicIdOrderTable
  //
  for (CurrProcessor = 0; CurrProcessor < mNumberOfCpus; CurrProcessor++) {

    MpContext = AllocatePool (sizeof(EFI_PROCESSOR_INFORMATION));
    ASSERT (MpContext != NULL);
    Status = MpService->GetProcessorInfo (
                          MpService,
                          CurrProcessor,
                          MpContext
                          );
    ASSERT_EFI_ERROR (Status);

    if (MpContext == NULL) {
      return Status;
    }
    mCpuApicIdOrderTable[CurrProcessor].ApicId  = (UINT32) MpContext->ProcessorId;
    DEBUG((DEBUG_INFO, "The CurrProcessor 0x%x ApicId is 0x%x\n", CurrProcessor, mCpuApicIdOrderTable[CurrProcessor].ApicId));
    mCpuApicIdOrderTable[CurrProcessor].Flags   = (MpContext->StatusFlag & PROCESSOR_ENABLED_BIT)? 1: 0;
    mCpuApicIdOrderTable[CurrProcessor].Package = (UINT8) MpContext->Location.Package;
    mCpuApicIdOrderTable[CurrProcessor].Die     = (UINT8) 0;
    mCpuApicIdOrderTable[CurrProcessor].Core    = (UINT8) MpContext->Location.Core;
    mCpuApicIdOrderTable[CurrProcessor].Thread  = (UINT8) MpContext->Location.Thread;

    if (MaxCpuThread < (UINT8) MpContext->Location.Thread) {
      MaxCpuThread = (UINT8) MpContext->Location.Thread;
    }

    if (MaxCpuCore < (UINT8) MpContext->Location.Core) {
      MaxCpuCore = (UINT8) MpContext->Location.Core;
    }
  }

  //
  // Do some statistics about the SBSP package
  //
  for (CurrProcessor = 0; CurrProcessor < mNumberOfCpus; CurrProcessor++) {
    if (CurrProcessor == BspIndex) {
      mSbspPackageInfo.BspApicId = mCpuApicIdOrderTable[CurrProcessor].ApicId;
      mSbspPackageInfo.PackageNo   = mCpuApicIdOrderTable[CurrProcessor].Package;
      mSbspPackageInfo.TotalThreads = 0;
      mSbspPackageInfo.CoreNo = 0;
      mSbspPackageInfo.LogicalThreadNo = 0;
    }
  }

  for (CurrProcessor = 0; CurrProcessor < mNumberOfCpus; CurrProcessor++) {
    if (mCpuApicIdOrderTable[CurrProcessor].Package == mSbspPackageInfo.PackageNo) {
      mSbspPackageInfo.TotalThreads++;
      if (mCpuApicIdOrderTable[CurrProcessor].Thread == 0) {
        mSbspPackageInfo.CoreNo++;
      } else {
        mSbspPackageInfo.LogicalThreadNo++;
      }
    }
  }

  //
  // Output debug info
  //
  for (Index = 0; Index < mNumberOfCpus; Index++) {
    DEBUG((DEBUG_INFO, "Package = %x,  Die = %x,  Core = %x,  Thread = %x,  ApicId = %x\n", \
      mCpuApicIdOrderTable[Index].Package, \
      mCpuApicIdOrderTable[Index].Die, \
      mCpuApicIdOrderTable[Index].Core, \
      mCpuApicIdOrderTable[Index].Thread, \
      mCpuApicIdOrderTable[Index].ApicId));
  }

  DEBUG((DEBUG_INFO, "MaxCpuCore    = %x\n", MaxCpuCore));
  DEBUG((DEBUG_INFO, "MaxCpuThread    = %x\n\n", MaxCpuThread));
  DEBUG((DEBUG_INFO, "mSbspPackageInfo.BspApicId        = %x\n", mSbspPackageInfo.BspApicId));
  DEBUG((DEBUG_INFO, "mSbspPackageInfo.TotalThreads     = %x\n", mSbspPackageInfo.TotalThreads));
  DEBUG((DEBUG_INFO, "mSbspPackageInfo.PackageNo        = %x\n", mSbspPackageInfo.PackageNo));
  DEBUG((DEBUG_INFO, "mSbspPackageInfo.CoreNo           = %x\n", mSbspPackageInfo.CoreNo));
  DEBUG((DEBUG_INFO, "mSbspPackageInfo.LogicalThreadNo  = %x\n", mSbspPackageInfo.LogicalThreadNo));

  //
  // First entry is always SBSP
  //
  CurrProcessor = 0;
  mCpuApicIdReorderTable[CurrProcessor].ApicId = mSbspPackageInfo.BspApicId;
  mCpuApicIdReorderTable[CurrProcessor].Package = mSbspPackageInfo.PackageNo;
  mCpuApicIdReorderTable[CurrProcessor].Flags = 1;
  CurrProcessor++;

  //
  // Output debug info
  //
  for (Index = 0; Index < mNumberOfCpus; Index++) {
    DEBUG((DEBUG_INFO, "Index = %x,  ApicId = %x,  Package = %x\n", \
      Index, \
      mCpuApicIdOrderTable[Index].ApicId, \
      mCpuApicIdOrderTable[Index].Package));
  }

  FreePool (MpContext);

  return  EFI_SUCCESS;
}


EFI_STATUS
AppendCpuMapTableEntry (
  IN  EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC_STRUCTURE   *AcpiLocalApic,
  IN UINT32                                          LocalApicCounter
  )
{
  EFI_STATUS    Status = EFI_SUCCESS;
  EFI_ACPI_6_3_PROCESSOR_LOCAL_X2APIC_STRUCTURE *LocalX2ApicPtr;

  if (AcpiLocalApic->Type == EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC) {
    if (LocalApicCounter < mNumberOfCpus) {
      AcpiLocalApic->Flags = (UINT8)mCpuApicIdOrderTable[LocalApicCounter].Flags;
      AcpiLocalApic->ApicId = (UINT8)mCpuApicIdOrderTable[LocalApicCounter].ApicId;
      //
      // Make AcpiProcessorIds 0-based
      //
      AcpiLocalApic->AcpiProcessorId = (UINT8)LocalApicCounter;
    } else {
      AcpiLocalApic->Flags = 0;
      AcpiLocalApic->ApicId = 0xFF;
      //
      // Make AcpiProcessorIds 0-based
      //
      AcpiLocalApic->AcpiProcessorId = (UINT8)LocalApicCounter;
      Status = EFI_UNSUPPORTED;
    }

    //
    // Display the results
    //
    DEBUG((DEBUG_INFO, "AcpiLocalApic: AcpiProcessorId=%x, ApicId=%x, Flags=%x\n", \
    AcpiLocalApic->AcpiProcessorId, \
    AcpiLocalApic->ApicId, \
    AcpiLocalApic->Flags));
  } else if (AcpiLocalApic->Type == EFI_ACPI_6_3_PROCESSOR_LOCAL_X2APIC) {
    LocalX2ApicPtr = (EFI_ACPI_6_3_PROCESSOR_LOCAL_X2APIC_STRUCTURE *)AcpiLocalApic;
    if (LocalApicCounter < mNumberOfCpus) {
      LocalX2ApicPtr->Flags = (UINT8)mCpuApicIdOrderTable[LocalApicCounter].Flags;
      LocalX2ApicPtr->X2ApicId = mCpuApicIdOrderTable[LocalApicCounter].ApicId;
      LocalX2ApicPtr->AcpiProcessorUid = LocalApicCounter;
    } else {
      LocalX2ApicPtr->Flags = 0;
      LocalX2ApicPtr->X2ApicId = (UINT32)-1;
      LocalX2ApicPtr->AcpiProcessorUid = LocalApicCounter;
      Status = EFI_UNSUPPORTED;
    }
    //
    // Display the results
    //
    DEBUG((DEBUG_INFO, "AcpiLocalx2Apic: AcpiProcessorId=%x, ApicId=%x, Flags=%x\n", \
    LocalX2ApicPtr->AcpiProcessorUid, \
    LocalX2ApicPtr->X2ApicId, \
    LocalX2ApicPtr->Flags));
  } else {
    Status = EFI_UNSUPPORTED;
  }
  return Status;
}


/**
  I/O work flow of in 8042 Aux data.

  @param Data    Buffer holding return value.

  @retval EFI_SUCCESS Success to excute I/O work flow
  @retval EFI_TIMEOUT Keyboard controller time out.
**/
EFI_STATUS
In8042AuxData (
  IN OUT UINT8 *Data
  )
{
  EFI_STATUS Status;

  //
  // wait for output data
  //
  Status = WaitOutputFull (BAT_TIMEOUT, FALSE);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  *Data = IoRead8 (KBC_DATA_PORT);

  return EFI_SUCCESS;
}

/**
  I/O work flow of outing 8042 Aux command.

  @param Command Aux I/O command

  @retval EFI_SUCCESS Success to excute I/O work flow
  @retval EFI_TIMEOUT Keyboard controller time out.
**/
EFI_STATUS
Out8042AuxCommand (
  IN UINT8                                Command,
  IN BOOLEAN                              Resend
  )
{
  EFI_STATUS  Status;
  UINT8       Data;

  //
  // Wait keyboard controller input buffer empty
  //
  Status = WaitInputEmpty (TIMEOUT);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //
  // Send write to auxiliary device command
  //
  IoWrite8 (KBC_CMD_STS_PORT, WRITE_AUX_DEV);

  Status = WaitInputEmpty (TIMEOUT);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //
  // Send auxiliary device command
  //
  IoWrite8 (KBC_DATA_PORT, Command);

  //
  // Read return code
  //
  Status = In8042AuxData (&Data);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (Data == PS2_ACK) {
    //
    // Receive mouse acknowledge, command send success
    //
    return EFI_SUCCESS;

  } else if (Resend) {
    //
    // Resend fail
    //
    return EFI_DEVICE_ERROR;

  } else if (Data == PS2_RESEND) {
    //
    // Resend command
    //
    Status = Out8042AuxCommand (Command, TRUE);
    if (EFI_ERROR (Status)) {
      return Status;
    }

  } else {
    //
    // Invalid return code
    //
    return EFI_DEVICE_ERROR;

  }

  return EFI_SUCCESS;
}

/**
  Issue command to enable Ps2 mouse.

  @return Status of command issuing.
**/
EFI_STATUS
PS2MouseEnable (
  VOID
  )
{
  //
  // Send auxiliary command to enable mouse
  //
//  return Out8042AuxCommand (ENABLE_CMD);

  EFI_STATUS    Status;

  //
  // Disable AUX and wait output buffer empty
  //
  Out8042Command (DISABLE_AUX);
  Status = WaitOutputEmpty (CLEAR_BUFF_TIMEOUT);
  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }
  //
  // Enable AUX
  //
  Out8042Command (ENABLE_AUX);
  //
  // Send auxiliary command to enable mouse
  //
  Status = Out8042AuxCommand (ENABLE_CMD, FALSE);
  //
  // Check done and disable AUX
  //
  Out8042Command (DISABLE_AUX);
  WaitOutputEmpty (CLEAR_BUFF_TIMEOUT);

  return Status;
}

/**
  Check whether there is Ps/2 mouse device in system

  @retval TRUE      - Keyboard in System.
  @retval FALSE     - Keyboard not in System.

**/
BOOLEAN
IsPs2MouseConnected (
  VOID
  )
{
  EFI_STATUS Status;

  Status = PS2MouseEnable ();

  if (!EFI_ERROR (Status)) {
    return TRUE;
  }

  return FALSE;
}

VOID
EFIAPI
UpdatePs2MouseStatus (
  IN EFI_EVENT            Event,
  IN VOID                 *Context
  )
{
//[-start-190613-IB16990065-add]//
  PLATFORM_NVS_AREA                   *mPlatformNvsArea;

  mPlatformNvsArea = (PLATFORM_NVS_AREA *) Context;
  mPlatformNvsArea->Ps2MouseEnable       = (UINT8) IsPs2MouseConnected ();
//[-end-190613-IB16990065-add]//
}

/**
 Update ACPI table content according to platform specific

 @param[in, out]    TableHeader             Pointer of the table to update
 @param[in, out]    CommonCodeReturnStatus  Return Status from Common Code

*/
VOID
UpdateAcpiTable (
  IN OUT EFI_ACPI_DESCRIPTION_HEADER      *TableHeader,
  IN OUT EFI_STATUS                       *CommonCodeReturnStatus
)
{
  EFI_ACPI_TABLE_VERSION                  Version;
  EFI_STATUS                              Status;

  Status = EFI_UNSUPPORTED;

  if (!mIsInitialized) {
    Status = InstallAcpiPlatform ((EFI_ACPI_DESCRIPTION_HEADER *) TableHeader);
    mIsInitialized = TRUE;
    if (EFI_ERROR (Status)) {
      *CommonCodeReturnStatus = Status;
      DEBUG ((DEBUG_ERROR, "InstallAcpiPlatform() fail. Status: %r\n", Status));
      ASSERT_EFI_ERROR(Status);
    }
  }

  switch ((TableHeader)->Signature) {
    case EFI_ACPI_5_0_MULTIPLE_APIC_DESCRIPTION_TABLE_SIGNATURE:
    case EFI_ACPI_5_0_FIXED_ACPI_DESCRIPTION_TABLE_SIGNATURE:
    case EFI_ACPI_5_0_DIFFERENTIATED_SYSTEM_DESCRIPTION_TABLE_SIGNATURE:
    case EFI_ACPI_5_0_PERSISTENT_SYSTEM_DESCRIPTION_TABLE_SIGNATURE:
    case EFI_ACPI_5_0_HIGH_PRECISION_EVENT_TIMER_TABLE_SIGNATURE:
    case EFI_ACPI_5_0_SECONDARY_SYSTEM_DESCRIPTION_TABLE_SIGNATURE:
    case EFI_ACPI_5_0_FIRMWARE_ACPI_CONTROL_STRUCTURE_SIGNATURE:
    case EFI_ACPI_5_0_EMBEDDED_CONTROLLER_BOOT_RESOURCES_TABLE_SIGNATURE:
    case EFI_ACPI_3_0_PCI_EXPRESS_MEMORY_MAPPED_CONFIGURATION_SPACE_BASE_ADDRESS_DESCRIPTION_TABLE_SIGNATURE:
      //
      // Perform any table specific updates.
      //
      // This function(PlatformUpdateTables) need to sync with Platform code which location at
      // TigerLakePlatSamplePkg\Acpi\AdvancedAcpiDxe\AcpiPlatform.c.
      //
      PlatformUpdateTables ((EFI_ACPI_COMMON_HEADER *) TableHeader, &Version);
      if (Version == EFI_ACPI_TABLE_VERSION_NONE) {
        *CommonCodeReturnStatus = EFI_UNSUPPORTED;
      } else {
        *CommonCodeReturnStatus = EFI_SUCCESS;
      }
      break;
    case EFI_ACPI_3_0_SERIAL_PORT_CONSOLE_REDIRECTION_TABLE_SIGNATURE:
      if (FeaturePcdGet (PcdAmtEnable)) {
        ChipsetSpcrUpdateCommon();
      }
      break;

    default:
      //
      // If there is no any update for other ACPI tables, we keep CommonCodeReturnStatus to original setting.
      // i.e. If SPCR table is disabled by InsydeModulePkg/AcpiPlatform, we keep the setting to prevent overwrite SPCR to be enabled.
      //
      break;
  }
}

EFI_STATUS
ChipsetUpdateSpcrTable (
  IN OUT   EFI_ACPI_COMMON_HEADER                 *Table
  )
{
	EFI_STATUS                                      Status;
	EFI_CONSOLE_REDIRECTION_SERVICE_PROTOCOL        *CRService;
  EFI_CONSOLE_REDIRECTION_INFO                    *CRInfo;
  EFI_ACPI_SERIAL_PORT_CONSOLE_REDIRECTION_TABLE  *SpcrPtr;
  EFI_CR_POLICY_PROTOCOL                          *CRPolicy;
  UINT64                                          PciAddress;
  PCI_TYPE00                                      Pci;
  UINTN                                           Index;
  BOOLEAN                                         MeSolExist;
  UINTN                                           NumHandles;
  EFI_HANDLE                                      *HandleBuffer;
  EFI_PCI_IO_PROTOCOL                             *PciIo;
  UINT64                                          Supports;
  EFI_ACPI_ADDRESS_SPACE_DESCRIPTOR               *Resources;
  UINT16                                          BaseAddress;
  UINTN                                           Bus;
  UINTN                                           Device;
  UINTN                                           Function;
  UINTN                                           SegNum;
  UINTN                                           HandleCount;
  UINT8                                           PciBarCount;
  CHIPSET_CONFIGURATION                           SetupVariable;



  Status = GetChipsetSetupVariableDxe (&SetupVariable, sizeof (CHIPSET_CONFIGURATION));
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Resources = NULL;
  BaseAddress = 0;
  Index = 0;
  MeSolExist = FALSE;
  SpcrPtr  = (EFI_ACPI_SERIAL_PORT_CONSOLE_REDIRECTION_TABLE *)Table;
  Status = gBS->LocateProtocol (&gConsoleRedirectionServiceProtocolGuid, NULL, (VOID **)&CRService);
  ASSERT_EFI_ERROR (Status);

  CRInfo = CRService->CRInfo;
  Status = gBS->LocateProtocol (&gEfiCRPolicyProtocolGuid, NULL, (VOID **)&CRPolicy);
  ASSERT_EFI_ERROR (Status);

  for (Index = 0; Index < CR_MAX_SUPPORT_SERIAL_PORT; Index++) {
    if ((CRPolicy->CRSerialDevData.CRDevice[Index].CrDevAttr.PortEnable == TRUE) &&
       (CRPolicy->CRSerialDevData.CRDevice[Index].Device.PciSerial.Bus == ME_BUS) &&
       (CRPolicy->CRSerialDevData.CRDevice[Index].Device.PciSerial.Device== ME_DEVICE_NUMBER) &&
       (CRPolicy->CRSerialDevData.CRDevice[Index].Device.PciSerial.Function== SOL_FUNCTION_NUMBER)) {
      MeSolExist = TRUE;
      break;
    }
  }
  PciAddress = PCI_SEGMENT_LIB_ADDRESS (0, ME_BUS, ME_DEVICE_NUMBER, SOL_FUNCTION_NUMBER, 0);
  PciSegmentReadBuffer (PciAddress, sizeof (PCI_TYPE00), &Pci);

  Status = gBS->LocateHandleBuffer (
                ByProtocol,
                &gEfiPciIoProtocolGuid,
                NULL,
                &NumHandles,
                &HandleBuffer
                );
  for (HandleCount = 0; HandleCount < NumHandles; HandleCount++) {
    Status = gBS->HandleProtocol (
                    HandleBuffer[HandleCount],
                    &gEfiPciIoProtocolGuid,
                    (VOID **)&PciIo
                    );
    ASSERT_EFI_ERROR (Status);
    PciIo->GetLocation(PciIo, &SegNum, &Bus, &Device, &Function);
    if (Bus == ME_BUS && Device == ME_DEVICE_NUMBER && Function == SOL_FUNCTION_NUMBER) {
      for (PciBarCount = 0; PciBarCount < PCI_MAX_BAR; PciBarCount++) {
        Status  = PciIo->GetBarAttributes (PciIo, PciBarCount, &Supports, (VOID **) &Resources);
        if (!EFI_ERROR(Status)) {
          if (Resources->ResType == ACPI_ADDRESS_SPACE_TYPE_IO) {
            BaseAddress= (UINT16)Resources->AddrRangeMin;
            break;
          }
        }
      }
    }
  }

  if (SetupVariable.ACPISpcr) {
    SpcrPtr->BaseAddress.AddressSpaceId = ACPI_ADDRESS_SPACE_TYPE_IO;
    SpcrPtr->BaseAddress.RegisterBitWidth = 8;
    SpcrPtr->BaseAddress.RegisterBitOffset = 0;
    SpcrPtr->BaseAddress.AccessSize = 0;//reference
    SpcrPtr->BaseAddress.Address = BaseAddress;
    SpcrPtr->InterruptType |= EFI_ACPI_SERIAL_PORT_CONSOLE_REDIRECTION_TABLE_INTERRUPT_TYPE_APIC;
    SpcrPtr->Irq = Pci.Device.InterruptLine;
    SpcrPtr->GlobalSystemInterrupt = Pci.Device.InterruptPin + 15;
    if (MeSolExist) {
      SpcrPtr->PciDeviceId = Pci.Hdr.DeviceId;
      SpcrPtr->PciVendorId = Pci.Hdr.VendorId;
      SpcrPtr->PciBusNumber = CRPolicy->CRSerialDevData.CRDevice[Index].Device.PciSerial.Bus;
      SpcrPtr->PciDeviceNumber = CRPolicy->CRSerialDevData.CRDevice[Index].Device.PciSerial.Device;
      SpcrPtr->PciFunctionNumber = CRPolicy->CRSerialDevData.CRDevice[Index].Device.PciSerial.Function;
      if (CRPolicy->CRSerialDevData.CRDevice[Index].CrDevAttr.UseGlobalSetting == TRUE) {
        SpcrPtr->TerminalType = CRPolicy->CRTerminalType;
        SpcrPtr->BaudRate = CRPolicy->CRBaudRate;
        SpcrPtr->Parity = CRPolicy->CRParity;
        SpcrPtr->StopBits = CRPolicy->CRStopBits;
        SpcrPtr->FlowControl = CRPolicy->CRFlowControl;
      } else {
        SpcrPtr->TerminalType = CRPolicy->CRSerialDevData.CRDevice[Index].CrDevAttr.TerminalType;
        SpcrPtr->BaudRate = CRPolicy->CRSerialDevData.CRDevice[Index].CrDevAttr.BaudRate;
        SpcrPtr->Parity = CRPolicy->CRSerialDevData.CRDevice[Index].CrDevAttr.Parity;
        SpcrPtr->StopBits = CRPolicy->CRSerialDevData.CRDevice[Index].CrDevAttr.StopBits;
        SpcrPtr->FlowControl = CRPolicy->CRSerialDevData.CRDevice[Index].CrDevAttr.FlowControl;
      }
    }
  } else {
    return EFI_UNSUPPORTED;
  }
  return EFI_SUCCESS;
}

EFI_STATUS
ChipsetSpcrUpdateCommon (
  VOID
)
{
  EFI_STATUS          Status;
  EFI_EVENT           ReadyToBootEvent;

  Status = EfiCreateEventReadyToBootEx (
             TPL_CALLBACK,
             ChipsetUpdateSpcrCallback,
             NULL,
             &ReadyToBootEvent
             );
  ASSERT_EFI_ERROR (Status);
  return Status;
}

VOID
EFIAPI
ChipsetUpdateSpcrCallback (
  IN EFI_EVENT   Event,
  IN VOID        *Context
  )
{
  EFI_ACPI_SUPPORT_PROTOCOL                 *AcpiSupport;
  EFI_STATUS                                Status;
  INTN                                      Index;
  UINTN                                     Handle;
  EFI_ACPI_TABLE_VERSION                    Version;
  EFI_ACPI_COMMON_HEADER                       *Table;

  Status = gBS->LocateProtocol (&gEfiAcpiSupportProtocolGuid, NULL, (VOID **)&AcpiSupport);
  ASSERT_EFI_ERROR (Status);
  //
  // Search SPCR table
  //
  Index = 0;
  while (1) {
    Status = AcpiSupport->GetAcpiTable (AcpiSupport, Index, (VOID **)&Table, &Version, &Handle);
    if (Status == EFI_NOT_FOUND) {
      break;
    }

    //
    // Check Signture and update SPCR table
    //
    if (Table->Signature == EFI_ACPI_3_0_SERIAL_PORT_CONSOLE_REDIRECTION_TABLE_SIGNATURE) {
      Status = ChipsetUpdateSpcrTable (Table);
      if (!EFI_ERROR (Status)) {
        Status = AcpiSupport->SetAcpiTable (AcpiSupport, Table, TRUE, Version, &Handle);
      }

      //
      // SPCR table has already updated so free the acpi table.
      //
      gBS->FreePool ( Table );
      return;
    }

    //
    // Not found, get next
    //
    gBS->FreePool ( Table );
    Index++;
  }
}
