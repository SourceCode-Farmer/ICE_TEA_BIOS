/** @file
 DXE Chipset Services Library.

 This file contains only one function that is DxeCsSvcGetBiosProtectTable().
 The function DxeCsSvcGetBiosProtectTable() use chipset services to return
 a Bios Protect Region Table.

;***************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PcdLib.h>
#include <Library/DxeOemSvcChipsetLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Register/SpiRegs.h>
/**
 Provide bios protect table for Kernel.

 @param[out]            *BiosRegionTable    Pointer to BiosRegion Table.
 @param[out]            ProtectRegionNum    The number of Bios protect region instances.

 @retval                EFI_SUCCESS         Provide table for kernel to set protect region and lock flash program registers.
                                            This table will be freed by kernel.
*/
EFI_STATUS
GetBiosProtectTable (
  OUT BIOS_PROTECT_REGION           **BiosRegionTable,
  OUT UINT8                         *ProtectRegionNum
  )
{
  UINT8               BiosProtectRegionNumber;
  UINTN               VariableSize;
  PCH_SETUP           PchSetup;
  EFI_STATUS          Status;

  if (BiosRegionTable == NULL || ProtectRegionNum == NULL) {
    return EFI_INVALID_PARAMETER;
  }
  if (*BiosRegionTable != NULL) {
    //
    // We will allocate a new one, so free it.
    //
    FreePool (*BiosRegionTable);
    *BiosRegionTable = NULL;
  }
  VariableSize = sizeof (PCH_SETUP);
  Status = gRT->GetVariable (
                  PCH_SETUP_VARIABLE_NAME,
                  &gPchSetupVariableGuid,
                  NULL,
                  &VariableSize,
                  &PchSetup
                  );
  ASSERT_EFI_ERROR(Status);

  if (PchSetup.FprrEnable == 0) {
    *ProtectRegionNum = 0;
    DEBUG ((DEBUG_INFO, "FPRR Disable. No BIOS region is protected.\n"));
  } else {
    BiosProtectRegionNumber = S_SPI_MEM_PRX + 1; //S_SPI_MEM_PRX:4 
    *BiosRegionTable = AllocateZeroPool (sizeof (BIOS_PROTECT_REGION) * (BiosProtectRegionNumber));

    if (*BiosRegionTable == NULL) {
      DEBUG ((DEBUG_INFO, "BiosRegionTable allocate memory resource failed.\n"));
      return EFI_OUT_OF_RESOURCES;
    }

//[-start-190611-IB16990043-add]//
  // Set Protected Region in Protected Region Table [0]: NV_FACTORY_COPY
    (*BiosRegionTable)[0].Base = PcdGet32 (PcdFlashNvStorageFactoryCopyBase); //0xFF060000
    (*BiosRegionTable)[0].Size = PcdGet32 (PcdFlashNvStorageFactoryCopySize); //0x00020000
  // Set Protected Region in Protected Region Table [1]: VARIABLE_DEFAULTS ~ FVMAIN
    (*BiosRegionTable)[1].Base = PcdGet32 (PcdFlashNvStorageVariableDefaultsBase); //0xFF09C000
    (*BiosRegionTable)[1].Size = PcdGet32 (PcdFlashNvStorageVariableDefaultsSize) + PcdGet32 (PcdFlashFvMainSize); // 0x00030000 + 0x00536000 = 56 6000
  // Set Protected Region in Protected Region Table [2]: FW_RESILIENCY_RESERVED ~ FV_RECOVERY0
    (*BiosRegionTable)[2].Base = PcdGet32 (PcdFwResiliencyReservedBase);        // if debug = yes, it's 0
    (*BiosRegionTable)[2].Size = PcdGet32 (PcdFwResiliencyReservedSize);        // if debug = yes, it's 0

    (*BiosRegionTable)[3].Base = PcdGet32 (PcdFlashFirmwareBinariesFvBase);     // 0xFF800000
    (*BiosRegionTable)[3].Size = PcdGet32 (PcdFlashFirmwareBinariesFvSize) +    // 0x0005A000
                                 PcdGet32 (PcdFlashNvStorageMicrocodeSize) +    // 0x00080000
                                 PcdGet32 (PcdFlashFvRecovery2Size) +           // 0x0025C000
                                 PcdGet32 (PcdFlashFvRecoverySize) +            // 0x002AA000
                                 PcdGet32 (PcdFlashFvFspSSize) +                // 0x00092000
                                 PcdGet32 (PcdFlashFvFspMSize) +                // 0x00150000
                                 PcdGet32 (PcdFlashFvFspTSize) +                // 0x00010000
                                 (UINT32)(PcdGet64 (PcdH2OFlashDeviceMapSize)) +// 0x00001000
                                 PcdGet32 (PcdFlashFvRecovery0Size);            // 0x0002D000 // TOTAL 0x80 0000
//[-end-190611-IB16990043-add]//
    (*BiosRegionTable)[4].Base = 0;
    (*BiosRegionTable)[4].Size = 0;

    *ProtectRegionNum = BiosProtectRegionNumber;
    DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices Call: OemSvcUpdateBiosProtectTable \n"));
    Status = OemSvcUpdateBiosProtectTable (BiosRegionTable, ProtectRegionNum);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices OemSvcUpdateBiosProtectTable Status: %r\n", Status));
  }
  return EFI_SUCCESS;
}

