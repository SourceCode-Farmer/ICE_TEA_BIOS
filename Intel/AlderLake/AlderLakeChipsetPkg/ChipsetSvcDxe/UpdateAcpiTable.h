/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  This is an implementation of the Advanced ACPI driver.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _ACPI_PLATFORM_H_
#define _ACPI_PLATFORM_H_

//
// Statements that include other header files
//

#include <IndustryStandard/Acpi.h>
#include <Guid/GlobalVariable.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/LocalApicLib.h>
#include <Protocol/CpuIo2.h>
#include <Protocol/AcpiTable.h>
#include <Protocol/PciRootBridgeIo.h>
#include <Protocol/MpService.h>
#include <Register/Cpuid.h>
//[-start-200907-IB16740115-remove]// CpuAccess.h has removed in RC1362.
//#include <CpuAccess.h>
//[-end-200907-IB16740115-remove]//
#include <Protocol/PciIo.h>
#include <Library/PciLib.h>
//#include <Library/PchCycleDecodingLib.h>
#include <Library/S3BootScriptLib.h>
#include <IndustryStandard/Acpi30.h>

#include <IndustryStandard/WatchdogActionTable.h>
#include <IndustryStandard/HighPrecisionEventTimerTable.h>
//#include <IndustryStandard/DebugPort2Table.h>
//#include <Guid/AcpiTableStorage.h>
#include <Library/DxeAcpiGnvsInitLib.h>

#include <Register/SaRegsHostBridge.h>
//
// Produced protocols
//
#include <Protocol/PlatformNvsArea.h>


//#include <Platform.h>
#include <Library/AslUpdateLib.h>
#include <SetupVariable.h>
//#include <Library/EcMiscLib.h>

//#include <Acpi/AcpiTables/Fadt/Fadt.h>
//#include <Acpi/AcpiTables/Dbgp/Dbgp.h>
//#include <PlatformInfo.h>
//#include <TbtBoardInfo.h>
// @todo to port for SKL
#include <Protocol/CpuInfo.h>
#include <Protocol/VariableLock.h>
#include <Protocol/MemInfo.h>
#include <CpuDataStruct.h>
#include <SetupConfig.h>
//#include <Acpi/AcpiTables/Dbgp/Dbgp.h>
#include <IndustryStandard/DebugPortTable.h>
#include <Library/GpioLib.h>

//#define EPC_BIOS    L"EPCBIOS"
//#define EPC_OS_CTRL L"EPCSW"
#include <IndustryStandard/DebugPort2Table.h>
extern EFI_GUID gEfiAcpiTableStorageGuid;

extern EFI_GUID gSinitSvnGuid;

//
// ACPI DBG2 - Debug Device Name Space String
//
#define ACPI_DBG2_DEFAULT_NAME_SPACE            "."
#define ACPI_DBG2_SERIALIO_UART0_NAME_SPACE     "\\_SB.PC00.UA00"
#define ACPI_DBG2_SERIALIO_UART1_NAME_SPACE     "\\_SB.PC00.UA01"
#define ACPI_DBG2_SERIALIO_UART2_NAME_SPACE     "\\_SB.PC00.UA02"
//
// ACPI table information used to initialize tables.
//

#define NAT_CONFIG_INDEX          0x2E
#define NAT_CONFIG_DATA           0x2F
#define WPCN381U_CONFIG_INDEX     0x2E
#define WPCN381U_CONFIG_DATA      0x2F
#define WPCN381U_CHIP_ID          0xF4
#define WDCP376_CHIP_ID           0xF1
#define Smsc1007_CONFIG_INDEX     0x164E
#define SMSC1000_CONFIG_INDEX     0x4E

#define KBC_DATA_PORT             0x60
#define KBC_CMD_STS_PORT          0x64

#define ENABLE_CMD                0xf4
#define WRITE_AUX_DEV             0xd4

#define DISABLE_AUX               0xa7
#define ENABLE_AUX                0xa8
#define CLEAR_BUFF_TIMEOUT        10000

#define PS2_ACK                   0xfa
#define PS2_RESEND                0xfe

#define KBC_INPB                  0x02
#define KBC_OUTB                  0x01
#define KBC_AUXB                  0x20

#define TIMEOUT                   50000
#define BAT_TIMEOUT               5000

#define IO_EXPANDER_DISABLED      0xFF
//
// 10sec Power Button Override Setup value
//
#define TEN_SEC_PB_DISABLE        0
#define TEN_SEC_PB_ENABLE         1
#define TEN_SEC_PB_AUTO           2

///
/// SGX EPC definitions
///
#define EPC_MULTIPLIER            75
#define EPC_SUBTRACT_MB           SIZE_2MB
#define PRM_BOTTOM_LIMIT          4
#define EPC_32_MB                 32
#define EPC_64_MB                 64
#define EPC_128_MB                128
#define EPC_1024_MB               1024
#define EPC_2048_MB               2048

#define EFI_ACPI_TABLE_VERSION_5_0          (1 << 5)
#define EFI_ACPI_TABLE_VERSION_6_0          (1 << 6)

#define SINIT_SVN_SETUP_VARIABLE_NAME   (L"SinitSvnSetup")

typedef struct {
  UINT32  ApicId;
  UINT32  Flags;
  UINT8   Package;
  UINT8   Die;
  UINT8   Core;
  UINT8   Thread;
} EFI_CPU_ID_ORDER_MAP;

typedef struct {
  UINT8   Package;
  UINT32  ApicId;
  UINT32  Flags;
} EFI_CPU_APIC_ID_REORDER_MAP;

typedef struct {
  UINT32  BspApicId;
  UINT8   TotalThreads;
  UINT8   PackageNo;
  UINT8   CoreNo;
  UINT8   LogicalThreadNo;
} EFI_CPU_PACKAGE_INFO;

typedef struct _SINIT_SVN_SETUP {
  ///
  /// Non-zero version indicates the variable is being initialized or valid
  ///
  UINT8  SvnNumber;

} SINIT_SVN_SETUP;

//
// Function definitions
//
EFI_STATUS
LocateSupportProtocol (
  IN     EFI_GUID                      *Protocol,
  IN     EFI_GUID                      gEfiAcpiMultiTableStorageGuid,
  OUT    VOID                          **Instance,
  IN     BOOLEAN                       Type
  );

/**
  Entry point of the ACPI platform driver.

  @param[in] ImageHandle  A handle for the image that is initializing this driver
  @param[in] SystemTable  A pointer to the EFI system table

  @retval    EFI_SUCCESS  The driver installed without error.
  @retval    EFI_ABORTED  The driver encountered an error and could not complete
                          installation of the ACPI tables.
**/
EFI_STATUS
EFIAPI
InstallAcpiPlatform (
//  IN EFI_HANDLE           ImageHandle,
//  IN EFI_SYSTEM_TABLE     *SystemTable
  );

EFI_STATUS
SortCpuLocalApicInTable (
  IN  EFI_MP_SERVICES_PROTOCOL                   *MpService
  );

EFI_STATUS
AppendCpuMapTableEntry (
  IN  EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC_STRUCTURE   *AcpiLocalApic,
  IN  UINT32                                        LocalApicCounter
  );

BOOLEAN
IsPs2MouseConnected (
  VOID
  );

VOID
EFIAPI
UpdatePs2KBStatus (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  );

EFI_STATUS
WaitInputEmpty (
  IN UINTN            Timeout
  );

/**
  I/O work flow to wait output buffer full in given time.
  @param Timeout given time
  @param Keyboard: TRUE is PS2 keyboard, FALSE is PS2 mouse.
  @retval EFI_TIMEOUT  output is not full in given time
  @retval EFI_SUCCESS  output is full in given time.
*/
EFI_STATUS
WaitOutputFull (
  IN UINTN            Timeout,
  IN BOOLEAN          Keyboard
  );

EFI_STATUS
WaitOutputEmpty (
  IN UINTN            Timeout
  );

EFI_STATUS
Out8042Command (
  IN UINT8            Command
  );

VOID
UpdateEpcBiosVariable (
  VOID
  );

VOID
UpdateSgxSvnVariable(
 VOID
 );

VOID
EFIAPI
UpdatePs2MouseStatus (
  IN EFI_EVENT            Event,
  IN VOID                 *Context
  );

VOID
UpdateEpcVariables (
  VOID
  );

VOID
UpdateSgxSvnVariable(
 VOID
 );

EFI_STATUS
ChipsetUpdateSpcrTable (
  IN OUT   EFI_ACPI_COMMON_HEADER          *Table
  );

EFI_STATUS
ChipsetSpcrUpdateCommon (
  VOID
);

VOID
EFIAPI
ChipsetUpdateSpcrCallback (
  IN EFI_EVENT   Event,
  IN VOID        *Context
  );

#endif
