/** @file
  Implement the test PEI PPI for status code to RAM.

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <PiPei.h>
#include <Pi/PiHob.h>
#include <Pi/PiStatusCode.h>
#include <Guid/MemoryStatusCodeRecord.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/HobLib.h>
#include <Ppi/EndOfPeiPhase.h>


EFI_STATUS
EFIAPI
EndOfPeiPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES             **PeiServices,
  IN CONST EFI_PEI_NOTIFY_DESCRIPTOR    *NotifyDescriptor,
  IN VOID                               *Ppi
  );

EFI_PEI_NOTIFY_DESCRIPTOR mNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  EndOfPeiPpiNotifyCallback
};

/**

 @param [in]   FfsHeader
 @param [in]   PeiServices


**/
EFI_STATUS
StatusCodeHandlerTestPeiEntryPoint (
  IN EFI_PEI_FILE_HANDLE        FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS              Status;

  Status = PeiServicesNotifyPpi (
             &mNotifyList
             );

  return Status;
}

/**
 Register notify ppi in the end of PEI.


 @param[in]   PeiServices               Pointer to the PEI Services Table.
 @param[in]   NotifyDescriptor          Pointer to the notify descriptor

 @retval EFI_SUCCESS

**/
EFI_STATUS
EFIAPI
EndOfPeiPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES               **PeiServices,
  IN CONST EFI_PEI_NOTIFY_DESCRIPTOR      *NotifyDescriptor,
  IN VOID                                 *Ppi
  )
{
  EFI_PEI_HOB_POINTERS                  Hob;
  MEMORY_STATUSCODE_PACKET_HEADER       *PacketHeader;
  MEMORY_STATUSCODE_RECORD              *Record;
  UINTN                                 Index;
  UINTN                                 TempRecordIndex;


  DEBUG ((EFI_D_INFO ,"StatusCodeHandlerTestPei: Start.\n"));
  //
  // Find GUID'ed HOBs to locate current record buffer.
  //
  Hob.Raw = GetFirstGuidHob (&gMemoryStatusCodeRecordGuid);
  ASSERT (Hob.Raw != NULL);

  PacketHeader = (MEMORY_STATUSCODE_PACKET_HEADER *) GET_GUID_HOB_DATA (Hob.Guid);
  DEBUG ((EFI_D_INFO ,"PacketHeader address: 0x%X\n", PacketHeader));
  DEBUG ((EFI_D_INFO ,"PacketHeader->PacketIndex = %d\n", PacketHeader->PacketIndex));
  DEBUG ((EFI_D_INFO ,"PacketHeader->RecordIndex = %d\n", PacketHeader->RecordIndex));
  DEBUG ((EFI_D_INFO ,"PacketHeader->MaxRecordsNumber = %d\n", PacketHeader->MaxRecordsNumber));
  TempRecordIndex = PacketHeader->RecordIndex;
  DEBUG ((EFI_D_INFO ,"TempRecordIndex = %d\n", TempRecordIndex));

  if (PacketHeader != NULL) {
    Record = (MEMORY_STATUSCODE_RECORD *) (PacketHeader + 1);
    for (Index = 0; Index < TempRecordIndex; Index++) {
      DEBUG ((EFI_D_INFO ,"Pei[%d] CodeType 0x%X V%08X I%X\n", Index + 1, Record->CodeType, Record->Value, Record->Instance));
    }
  }
  DEBUG ((EFI_D_INFO ,"StatusCodeHandlerTestPei: End.\n"));

  return EFI_SUCCESS;
}
