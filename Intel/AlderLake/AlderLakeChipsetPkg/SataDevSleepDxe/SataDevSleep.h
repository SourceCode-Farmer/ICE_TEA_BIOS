/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SATA_DEVSLEEP_H
#define _SATA_DEVSLEEP_H

#include <Uefi.h>
#include <ChipsetAccess.h>
#include <Register/PchRegs.h>
// gino: remove this since Intel had already removed >>
// #include <Register/PchRegsSata.h>
#include <Register/SataRegs.h>
// gino: remove this since Intel had already removed <<
#include <Protocol/AtaPassThru.h>
#include <Protocol/DevicePath.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DevicePathLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/SataLib.h>

#define ATA_COMMAND_SET_FEATURES             0xEF
#define ATA_COMMAND_SET_FEATURES_ENABLE      0x10
#define ATA_COMMAND_FEATURES_DEVSLEEP        0x09

#define ATA_COMMAND_READ_LOG_EXT             0x2F
#define ATA_COMMAND_IDENTIFY_DEVICE          0xEC
#define ATA_LOG_SIZE                         0x200

//gino: Intel define these in C file >>
#define PCI_DEVICE_NUMBER_PCH_SATA          23
#define PCI_FUNCTION_NUMBER_PCH_SATA        0
//gino: Intel define these in C file <<
typedef struct {
  UINT32 Buffer[128];
} EFI_READ_LOG_EXT_DATA;

typedef struct {
  EFI_DEVICE_PATH_PROTOCOL      Header;
  UINT16                        HBAPortNumber;
  UINT16                        PortMultiplierPortNumber;
  UINT16                        Lun;
} AHCI_SATA_DEVICE_PATH;

typedef struct {
  UINT16  config;             // General Configuration
  UINT16  cylinders;          // Number of Cylinders
  UINT16  reserved_2;
  UINT16  heads;              //Number of logical heads
  UINT16  vendor_data1;
  UINT16  vendor_data2;
  UINT16  sectors_per_track;
  UINT16  vendor_specific_7_9[3];
  CHAR8   SerialNo[20];       // ASCII
  UINT16  vendor_specific_20_21[2];
  UINT16  ecc_bytes_available;
  CHAR8   FirmwareVer[8];     // ASCII
  CHAR8   ModelName[40];      // ASCII
  UINT16  multi_sector_cmd_max_sct_cnt;
  UINT16  reserved_48;
  UINT16  capabilities;
  UINT16  reserved_50;
  UINT16  pio_cycle_timing;
  UINT16  reserved_52;
  UINT16  field_validity;
  UINT16  current_cylinders;
  UINT16  current_heads;
  UINT16  current_sectors;
  UINT16  CurrentCapacityLsb;
  UINT16  CurrentCapacityMsb;
  UINT16  reserved_59;
  UINT16  user_addressable_sectors_lo;
  UINT16  user_addressable_sectors_hi;
  UINT16  reserved_62;
  UINT16  multi_word_dma_mode;
  UINT16  advanced_pio_modes;
  UINT16  min_multi_word_dma_cycle_time;
  UINT16  rec_multi_word_dma_cycle_time;
  UINT16  min_pio_cycle_time_without_flow_control;
  UINT16  min_pio_cycle_time_with_flow_control;
  UINT16  reserved_69_79[11];
  UINT16  major_version_no;
  UINT16  minor_version_no;
  UINT16  command_set_supported_82; // word 82
  UINT16  command_set_supported_83; // word 83
  UINT16  command_set_feature_extn; // word 84
  UINT16  command_set_feature_enb_85; // word 85
  UINT16  command_set_feature_enb_86; // word 86
  UINT16  command_set_feature_default; // word 87
  UINT16  ultra_dma_mode; // word 88
  UINT16  reserved_89_105[17];
  UINT16  phy_logic_sector_support; // word 106
  UINT16  reserved_107_116[10];
  UINT16  logic_sector_size_lo; // word 117
  UINT16  logic_sector_size_hi; // word 118
  UINT16  reserved_119_127[9];
  UINT16  security_status;
  UINT16  vendor_data_129_159[31];
  UINT16  reserved_160_208[49];
  UINT16  alignment_logic_in_phy_blocks; // word 209
  UINT16  reserved_210_255[46];
} EFI_ATA8_IDENTIFY_DATA;

typedef struct {
  UINT8              Valid                 : 1;
  UINT8              SolidStateDrive       : 1;
  UINT8              SetupDevsleepEnable   : 1;
  UINT8              DevsleepCap           : 1;
  UINT8              DevsleepEn            : 1;
  UINT8              DevsleepPwr           : 1;
  UINT8              Reserved              : 2;
} DEV_SATA_DVSLEEP_STATE;

typedef struct {
  DEV_SATA_DVSLEEP_STATE      State;
  UINT16              DETO;
  UINT8               MDAT;
} DEV_SATA_DVSLEEP_DATA;

VOID
EFIAPI
SATADevSleepCallback (
  IN EFI_EVENT                          Event,
  IN VOID                               *Context
  );

EFI_STATUS
GetSATADevsleepTimes (
  IN     EFI_ATA_PASS_THRU_PROTOCOL         *AtaPassThruPtr,
  IN     UINT16                             *PortNumber,
  IN OUT DEV_SATA_DVSLEEP_DATA              *PortData
  );

EFI_STATUS
GetSATAIdentifyDeviceData (
  IN     EFI_ATA_PASS_THRU_PROTOCOL         *AtaPassThruPtr,
  IN     UINT16                             *PortNumber,
  IN OUT DEV_SATA_DVSLEEP_DATA              *PortData
  );


EFI_STATUS
EnableSATADevsleep (
  IN     EFI_ATA_PASS_THRU_PROTOCOL         *AtaPassThruPtr,
  IN     UINT16                             *PortNumber
  );

#endif
