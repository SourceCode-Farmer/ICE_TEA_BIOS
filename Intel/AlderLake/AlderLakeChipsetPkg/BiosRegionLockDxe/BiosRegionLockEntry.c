/** @file
  Provide BIOS region lock protocol to protect BIOS region

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <ChipsetAccess.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/IoLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/S3BootScriptLib.h>
#include "BiosRegionLockInfo.h"
#include <Library/MmPciLib.h>
#include <Register/PchRegs.h>
#include <Register/SpiRegs.h>
//[-start-191225-IB16740000-add]// for PCI_DEVICE_NUMBER_PCH_LPC & PCI_FUNCTION_NUMBER_PCH_XHCI define
#include <PchBdfAssignment.h>
//[-end-191225-IB16740000-add]//
#include <PchReservedResources.h>
#include <Protocol/PciEnumerationComplete.h>

VOID
EFIAPI
RestorePchSpiBar0 (
  IN EFI_EVENT    Event,
  IN VOID         *Context
  )
{
  UINT32        PchSpiBar0;
  UINT64        BootScriptPciAddress;   

  PchSpiBar0 = PCH_SPI_BASE_ADDRESS;
  BootScriptPciAddress = S3_BOOT_SCRIPT_LIB_PCI_ADDRESS (
                           0,
                           31,
                           5,
                           R_SPI_CFG_BAR0
                           );

  S3BootScriptSavePciCfgWrite (
    S3BootScriptWidthUint32,
    BootScriptPciAddress,
    1,
    &PchSpiBar0
    );
   DEBUG ((DEBUG_INFO, "S3BootScriptSavePciCfgWrite RestorePchSpiBar0 BootScriptPciAddress = 0x%08x, PchSpiBar0 = 0x%08x\n", (UINT32)BootScriptPciAddress, (UINT32)PchSpiBar0));    
}


/**
 Entry point for Bios Region Lock driver. Install BIOS region lock protocol to protect BIOS region.

 @param[in]         ImageHandle         Image handle of this driver.
 @param[in]         SystemTable         Global system service table.

 @retval EFI_SUCCESS Initialization complete.
*/
EFI_STATUS
EFIAPI
BiosRegionLockInit (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_HANDLE                            Handle;
  EFI_STATUS                            Status;
  BIOS_REGION_LOCK_INSTANCE            *BiosRegionLockInstance;
  BIOS_REGION_LOCK_PROTOCOL            *BiosRegionLock;
  UINTN                                PchSpiBase;
  UINTN                                PciSpiRegBase;
  VOID                                 *Registration;  

  PciSpiRegBase   = MmPciBase (
                      DEFAULT_PCI_BUS_NUMBER_PCH,
                      PCI_DEVICE_NUMBER_PCH_SPI,
                      PCI_FUNCTION_NUMBER_PCH_SPI
                      );
  PchSpiBase      = MmioRead32 (PciSpiRegBase + R_SPI_CFG_BAR0) &~(B_SPI_CFG_BAR0_MASK);
  if (MmioRead16 (PchSpiBase + R_SPI_MEM_HSFSC) & B_SPI_MEM_HSFSC_FLOCKDN) {
    DEBUG ((DEBUG_ERROR, "SPI has been locked - Access Denied!\n"));
    return EFI_DEVICE_ERROR;
  }

  BiosRegionLockInstance = AllocateZeroPool (sizeof (BIOS_REGION_LOCK_INSTANCE));
  if (BiosRegionLockInstance == NULL) {
    DEBUG ((DEBUG_ERROR, "Allocate Pool Failure!\n"));
    return EFI_OUT_OF_RESOURCES;
  }

  BiosRegionLock = &BiosRegionLockInstance->BiosRegionLock;
  BiosRegionLock->SetRegionByType      = SetRegionByType;
  BiosRegionLock->SetRegionByAddress   = SetRegionByAddress;
  BiosRegionLock->ClearRegionByType    = ClearRegionByType;
  BiosRegionLock->ClearRegionByAddress = ClearRegionByAddress;
  BiosRegionLock->Lock                 = Lock;

  Handle = NULL;
  Status = gBS->InstallProtocolInterface (
                  &Handle,
                  &gEfiBiosRegionLockProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  BiosRegionLock
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Install EfiBiosRegionLockProtocol Failure!\n"));
    gBS->FreePool (BiosRegionLockInstance);
  }
//#ifdef EFI_DEBUG
  else {
    if (PcdGetBool (PcdStatusCodeUseSerial)) {
      DEBUG ((DEBUG_ERROR, "Install EfiBiosRegionLockProtocol Success!\n"));
    }
  }
//#endif
//
// Only for the issue about Native FSP Build that S3script restores wrong Bar0 address during S3, 
// ITS:0081438
//
  if (PcdGetBool(PcdNativeFspBuild)) {
    EfiCreateProtocolNotifyEvent (
      &gEfiPciEnumerationCompleteProtocolGuid,
      TPL_CALLBACK,
      RestorePchSpiBar0,
      NULL,
      &Registration
      );
    DEBUG ((DEBUG_INFO, "BiosRegionLockInit EfiCreateProtocolNotifyEvent by gEfiPciEnumerationCompleteProtocolGuid for RestorePchSpiBar0\n"));
  }
  return Status;
}
