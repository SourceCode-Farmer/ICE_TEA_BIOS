/** @file
 PEI Chipset Services Library.

 This file contains only one function that is PeiCsSvcModifyMemoryRange().
 The function PeiCsSvcModifyMemoryRange() use chipset services to modify
 memory range setting.

;***************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#define PEI_MEMORY_RANGE_OPTION_ROM       UINT32
#define PEI_MEMORY_RANGE_SMRAM            UINT32
#define PEI_MEMORY_RANGE_GRAPHICS_MEMORY  UINT32
#define PEI_MEMORY_RANGE_PCI_MEMORY       UINT32

/**
 To modify memory range setting.


 @param[in, out]    OptionRomMask       A pointer to PEI_MEMORY_RANGE_OPTION_ROM
 @param[in, out]    SmramMask           A pointer to PEI_MEMORY_RANGE_SMRAM
 @param[in, out]    GraphicsMemoryMask  A pointer to PEI_MEMORY_RANGE_GRAPHICS_MEMORY
 @param[in, out]    PciMemoryMask       A pointer to PEI_MEMORY_RANGE_PCI_MEMORY

 @return            EFI_SUCCESS         Always return successfully
*/
EFI_STATUS
ModifyMemoryRange (
  IN OUT  PEI_MEMORY_RANGE_OPTION_ROM           *OptionRomMask,
  IN OUT  PEI_MEMORY_RANGE_SMRAM                *SmramMask,
  IN OUT  PEI_MEMORY_RANGE_GRAPHICS_MEMORY      *GraphicsMemoryMask,
  IN OUT  PEI_MEMORY_RANGE_PCI_MEMORY           *PciMemoryMask
  )
{
  return EFI_SUCCESS;
}
