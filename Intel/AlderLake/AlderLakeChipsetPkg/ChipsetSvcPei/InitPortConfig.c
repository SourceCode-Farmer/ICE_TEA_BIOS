/** @file
 Initialize port config PCDs

;***************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/SataSocLib.h>
#include <Library/PeiInsydeChipsetLib.h>
#include <Library/PchInfoLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <SetupVariable.h>
#include <PortConfig.h>
//[-start-220217-QINGLIN0158-add]//
#if defined(S370_SUPPORT)
#include <Library/CnviLib.h>
#endif
//[-end-220217-QINGLIN0158-add]//

/**
  Initialize the value of port configuration PCDs.

  @retval EFI_SUCCESS           Initialize the value of port configuration PCDs success.
**/
EFI_STATUS
InitPortConfigPcds (
  VOID
  )
{
  PORT_CONFIG                   UsbConfig;
  PORT_CONFIG                   Usb3Config;
  PORT_CONFIG                   PcieConfig;
  PORT_CONFIG                   SataConfig;
  EFI_STATUS                    Status;
  PCH_SETUP                     PchSetup;
  SA_SETUP                      SaSetup;
  UINT8                         PortIndex;
  UINT64                        ConfigMask;
  UINT8                         PcieNonCpuPortNumber;

  //
  // Default Enable all ports
  //
  UsbConfig.Value  = 0xFFFFFFFFFFFFFFFF;
  Usb3Config.Value = 0xFFFFFFFFFFFFFFFF;
  SataConfig.Value = 0xFFFFFFFFFFFFFFFF;
  PcieConfig.Value = 0x5555555555555555;

  PcieNonCpuPortNumber = PcdGet8 (PcdH2OPcieNonCpuPortNumber);

  //
  // Initialize port configurations from Setup variable.
  //
  Status = GetChipsetPchSetupVariablePei (&PchSetup);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Status = GetChipsetSaSetupVariablePei (&SaSetup);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  //
  // USB Port
  //
  for (PortIndex = 0; PortIndex < GetPchXhciMaxUsb2PortNum (); PortIndex++) {
    ConfigMask = LShiftU64 ((UINT64) PORT_ENABLE, PortIndex);
    if (PchSetup.PchUsbHsPort[PortIndex] && ((UsbConfig.Value >> PortIndex) & 0x1)) {
      UsbConfig.Value |= ConfigMask;
    } else {
      UsbConfig.Value &= ~ConfigMask;
    }
  }

  for (PortIndex = 0; PortIndex < GetPchXhciMaxUsb3PortNum (); PortIndex++) {
    ConfigMask = LShiftU64 ((UINT64) PORT_ENABLE, PortIndex);
    if (PchSetup.PchUsbSsPort[PortIndex] && ((Usb3Config.Value >> PortIndex) & 0x1)) {
      Usb3Config.Value |= ConfigMask;
    } else {
      Usb3Config.Value &= ~ConfigMask;
    }
  }

//[-start-220217-QINGLIN0158-add]//
#if defined(S370_SUPPORT)
  if (SaSetup.PrimaryDisplay != 4) { //not switchable, disable PEG2 port
    SaSetup.PcieRootPortEn[2] = 0;
  }
  if (CnviIsPresent()) { //Cnvi Card, disable PCH Root Port 8
    PchSetup.PcieRootPortEn[8] = 0;
  }
#endif
//[-end-220217-QINGLIN0158-add]//

  //
  // PCH PCIe part: Bit [0:55] (Base on the maximum number of PCH root ports on ADL-S).
  //
  for (PortIndex = 0; PortIndex < GetPchMaxPciePortNum (); PortIndex++) {
    if (PchSetup.PcieRootPortEn[PortIndex] && ((PcieConfig.Value >> (PortIndex * 2)) & 0x1)) {
      ConfigMask = LShiftU64 ((UINT64) PORT_ENABLE, (PortIndex * 2));
    } else {
      ConfigMask = LShiftU64 ((UINT64) PORT_DISABLE, (PortIndex * 2));
    }
    PcieConfig.Value &= ~(LShiftU64 ((UINT64) (BIT0 | BIT1), (PortIndex * 2)));
    PcieConfig.Value |= ConfigMask;
  }

  //
  // CPU PCIe part: Bit [56:61] (Base on the maximum number of CPU root ports on ADL-S).
  //
  for (PortIndex = 0; PortIndex < GetMaxCpuPciePortNum (); PortIndex++) {
    if (SaSetup.PcieRootPortEn[PortIndex] && ((PcieConfig.Value >> ((PortIndex + PcieNonCpuPortNumber) * 2)) & 0x1)) {
      ConfigMask = LShiftU64 ((UINT64)PORT_ENABLE, ((PortIndex + PcieNonCpuPortNumber) * 2));
    } else {
      ConfigMask = LShiftU64 ((UINT64)PORT_DISABLE, ((PortIndex + PcieNonCpuPortNumber) * 2));
    }
    PcieConfig.Value &= ~(LShiftU64 ((UINT64)(BIT0 | BIT1), ((PortIndex + PcieNonCpuPortNumber) * 2)));
    PcieConfig.Value |= ConfigMask;
  }

  //
  // SATA
  //
  for (PortIndex = 0; PortIndex < MaxSataPortNum (SATA_1_CONTROLLER_INDEX); PortIndex++) {
    ConfigMask = LShiftU64 ((UINT64) PORT_ENABLE, PortIndex);
    if (PchSetup.SataPort[PortIndex] && ((SataConfig.Value >> PortIndex) & 0x1)) {
      SataConfig.Value |= ConfigMask;
    } else {
      SataConfig.Value &= ~ConfigMask;
    }
  }

  Status = PcdSet64S(PcdH2OChipsetUsbPortEnable , UsbConfig.Value);
  ASSERT_EFI_ERROR(Status);

  Status = PcdSet64S(PcdH2OChipsetUsb3PortEnable, Usb3Config.Value);
  ASSERT_EFI_ERROR(Status);

  Status = PcdSet64S(PcdH2OChipsetSataPortEnable, SataConfig.Value);
  ASSERT_EFI_ERROR(Status);

  Status = PcdSet64S(PcdH2OChipsetPciePortEnable, PcieConfig.Value);
  ASSERT_EFI_ERROR(Status);

  return EFI_SUCCESS;
}


