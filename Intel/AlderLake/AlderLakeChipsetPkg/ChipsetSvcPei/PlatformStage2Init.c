/** @file
 PEI Chipset Services Library.

 This file contains only one function that is PeiCsSvcPlatformStage2Init().
 The function PeiCsSvcPlatformStage2Init() use chipset services to initialization
 chipset in stage 2 of PEi phase.

;***************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Pi/PiPeiCis.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/IoLib.h>
#include <Library/HobLib.h>
#include <Library/CmosLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/BaseMemoryLib.h>
//#include <Ppi/PchProductionFlag.h>
//#include <Ppi/CpuProductionFlag.h>
#include <Ppi/PciCfg2.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Ppi/MemoryDiscovered.h>
#include <Ppi/Stall.h>
#include <Ppi/SiPolicy.h>
#include <Ppi/EcpAtaController.h>
//#include <Ppi/PchUsbPolicy/PchUsbPolicy.h>
//#include <Ppi/PchInit.h>
#include <Ppi/EndOfPeiPhase.h>
#include <Ppi/S3RestoreAcpiCallback.h>
#include <Ppi/Reset.h>
//#include <SaRegs.h>
//#include <Ppi/SaPolicy.h>
// #include <Ppi/PeiBootScriptDone/PeiBootScriptDone.h>
#include <Guid/HybridGraphicsVariable.h>
#include <ChipsetSetupConfig.h>
#include <ChipsetAccess.h>
#include <ChipsetCmos.h>
#include <CpuRegs.h>
//#include <Library/PchPlatformLib.h>
#include <Library/GbeLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/PchInfoLib.h>
#include <HybridGraphicsDefine.h>
#include <Library/MmPciLib.h>
#include <Library/PeiInsydeChipsetLib.h>
#include <Register/Cpuid.h>
#include <Register/Msr.h>
#include <Library/PmcLib.h>









///**
// set VT capability.
//
// @param[in]         PeiServices         Describes the list of possible PEI Services.
// @param[in]         SystemConfiguration A pointer to setup variables.
// @param[in]         IsVtSupport         VT support status
//
// @retval            No Status Return.
//*/
//STATIC
//VOID
//PeiVTExecute (
//  IN CONST  EFI_PEI_SERVICES       **PeiServices,
//  IN        CPU_SETUP              *CpuSetup,
//  IN        BOOLEAN                  IsVtSupport
//  )
//{
//  MSR_IA32_FEATURE_CONTROL_REGISTER Msr;
//  UINT32                        AddressBase;
//
//  AddressBase = PmcGetPwrmBase ();
//
//  Msr.Uint64 = AsmReadMsr64 (MSR_IA32_FEATURE_CONTROL);
//  if (((CpuSetup->VT) && (IsVtSupport)) && (!Msr.Bits.EnableVmxOutsideSmx) \
//      || ((!((CpuSetup->VT) && (IsVtSupport))) & Msr.Bits.EnableVmxOutsideSmx)
//     ) {
//    if (Msr.Bits.Lock == 1) {
//      MmioOr32 (AddressBase + R_PMC_PWRM_ETR3, (UINT32) (B_PMC_PWRM_ETR3_CF9GR));
//      IoWrite32 (R_PCH_IO_RST_CNT, V_PCH_IO_RST_CNT_FULLRESET);
//    } else {
//      if ((CpuSetup->VT) && (IsVtSupport)) {
//        Msr.Bits.EnableVmxOutsideSmx = 1;
//      } else {
//        Msr.Bits.EnableVmxOutsideSmx = 0;
//      }
//      AsmWriteMsr64 (MSR_IA32_FEATURE_CONTROL, Msr.Uint64);
//    }
//  }
//
//  return;
//}

///**
// A callback function is triggered by gPeiS3RestoreAcpiCallbackPpiGuid PPI installation.
//
// @param[in]         PeiServices         General purpose services available to every PEIM.
// @param[in]         NotifyDescriptor    A pointer to notification structure this PEIM registered on install.
// @param[in]         Ppi                 A pointer to S3RestoreAcpiCallback PPI
//
// @retval            EFI_SUCCESS         Procedure complete.
//*/
//STATIC
//EFI_STATUS
//S3RestoreAcpiNotifyCallback (
//  IN EFI_PEI_SERVICES            **PeiServices,
//  IN EFI_PEI_NOTIFY_DESCRIPTOR   *NotifyDescriptor,
//  IN VOID                        *Ppi
//  )
//{
//  EFI_STATUS                     Status;
//
//  if (FeaturePcdGet(PcdTXTSupported)) {
//    Status = PeiServicesInstallPpi (&mPeiBootScriptDonePpi);
//    ASSERT_EFI_ERROR (Status);
//  }
//
//  if (FeaturePcdGet (PcdNvidiaOptimusSupported)) {
//    CloseHdAudio ( (CONST EFI_PEI_SERVICES **)PeiServices);
// }
//
//  return EFI_SUCCESS;
//}

///**
// Close HD Audio device expect S3/S4.
// When S3/S4 resume, check HG variable "OptimusFlag" to on/off HD audio device.
// Bus 1 Dev 0 Fun 0 Reg 488 Bit 25 is HD audio device power enable bit.
//
// @param [in]   PeiServices      General purpose services available to every PEIM.
//
// @retval None.
//
//**/
//STATIC
//VOID
//CloseHdAudio (
//  IN CONST EFI_PEI_SERVICES                   **PeiServices
//  )
//{
//  EFI_BOOT_MODE                               BootMode;
//  EFI_PEI_READ_ONLY_VARIABLE2_PPI             *VariableServices;
//  EFI_STATUS                                  Status;
//  HG_VARIABLE_CONFIGURATION                   HgData;
//  UINT8                                       HdAudioFlag;
//  UINT8                                       PegBus;
//  UINTN                                       VariableSize;
//
//  //
//  // DEVEN register bit 4 is Internal Graphics Engine (D2EN).
//  //
//  if ((MmPci8 (0, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DEVEN) & B_SA_DEVEN_D2EN_MASK) == 0) {
//    return;
//  }
//
//  Status = PeiServicesLocatePpi (
//                             &gEfiPeiReadOnlyVariable2PpiGuid,
//                             0,
//                             NULL,
//                             (VOID **)&VariableServices
//                             );
//  if (EFI_ERROR (Status)) {
//   	return;
//  }
//
//  Status = VariableServices->GetVariable (
//                               VariableServices,
//                               L"HybridGraphicsVariable",
//                               &gH2OHybridGraphicsVariableGuid,
//                               NULL,
//                               &VariableSize,
//                               &HgData
//                               );
//  if (EFI_ERROR (Status)) {
//    return;
//  }
//  PegBus = HgData.OptimusVariable.MasterDgpuBus;
//  if (MmPci16 (0, PegBus, DGPU_DEVICE_NUM, DGPU_FUNCTION_NUM, PCI_VENDOR_ID_OFFSET) == NVIDIA_VID) {
//    Status = PeiServicesGetBootMode (&BootMode);
//    if (EFI_ERROR (Status)) {
//      return;
//    }
//
//    if ((BootMode != BOOT_ON_S3_RESUME) && (BootMode != BOOT_ON_S4_RESUME)) {
//      MmPci32And (0, PegBus, DGPU_DEVICE_NUM, DGPU_FUNCTION_NUM, NVIDIA_DGPU_HDA_REGISTER, ~(BIT25));
//      return;
//    }
//
//    if (BootMode == BOOT_ON_S3_RESUME) {
//      HdAudioFlag = HgData.OptimusVariable.OptimusFlag;
//      if ((HdAudioFlag & BIT0) == 0) {
//        MmPci32And (0, PegBus, DGPU_DEVICE_NUM, DGPU_FUNCTION_NUM, NVIDIA_DGPU_HDA_REGISTER, ~(BIT25));
//      } else {
//        MmPci32Or (0, PegBus, DGPU_DEVICE_NUM, DGPU_FUNCTION_NUM, NVIDIA_DGPU_HDA_REGISTER, BIT25);
//      }
//    }
//  }
//}
//static
//EFI_PEI_NOTIFY_DESCRIPTOR mSetS3RestoreAcpiNotify = {
//  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
//  &gPeiS3RestoreAcpiCallbackPpiGuid,
//  S3RestoreAcpiNotifyCallback
//  };
/**
 Chipset initialization code in stage2 of PEI phase.

 @param[in]         None

 @retval            EFI_SUCCESS         Procedure complete.
*/
EFI_STATUS
PlatformStage2Init (
  VOID
  )
{
//  UINT32                          AddressBase;
//
//  PeiServices            = GetPeiServicesTablePointer ();
//  CmosPlatformSetting    = 0;
//  CmosPlatformConfig     = 0;
//  TempCmosPlatformConfig = 0;
//
//  AddressBase = PmcGetPwrmBase ();
//
//
//  Status = PeiServicesGetBootMode(&BootMode);
//
//  Status = GetChipsetSetupVariablePei (&SystemConfiguration);
//  if (EFI_ERROR (Status)) {
//    ASSERT_EFI_ERROR (Status);
//    return Status;
//  }
//  Status = GetChipsetCpuSetupVariablePei (&CpuSetup);
//  if (EFI_ERROR (Status)) {
//    ASSERT_EFI_ERROR (Status);
//    return Status;
//  }
//  //
//  // Power Failure Consideration PPT BIOS SPEC 19.3
//  //
//  if ((MmioRead32 (AddressBase + R_PMC_PWRM_GEN_PMCON_B) & B_PMC_PWRM_GEN_PMCON_B_RTC_PWR_STS) == B_PMC_PWRM_GEN_PMCON_B_RTC_PWR_STS) {
//    //
//    // When the RTC_PWR_STS is set
//    // software should clear this bit. Then
//    // 2. Set RTC Register 0Ah[6:4] to 110b or 111b
//    // 3. Set RTC Register 0Bh[7]
//    // 4. Set RTC Register 0Ah[6:4] to 010b
//    // 5. Clear RTC Register 0Bh[7]
//    //
//    MmioAnd32 ( AddressBase + R_PMC_PWRM_GEN_PMCON_B, (UINT32) ~B_PMC_PWRM_GEN_PMCON_B_RTC_PWR_STS );
//    RTCReg = ReadCmos8 (R_RTC_IO_REGA);
//    RTCReg &= ~(B_RTC_IO_REGB_PIE | B_RTC_IO_REGB_AIE |B_RTC_IO_REGB_UIE);
//    RTCReg |= B_RTC_IO_REGB_PIE | B_RTC_IO_REGB_AIE |B_RTC_IO_REGB_UIE;
//    WriteCmos8 (R_RTC_IO_REGA, RTCReg);
//
//    RTCReg = ReadCmos8 (R_RTC_IO_REGB);
//    RTCReg |= B_RTC_IO_REGB_SET;
//    WriteCmos8 (R_RTC_IO_REGB, RTCReg);
//
//    RTCReg = ReadCmos8 (R_RTC_IO_REGA);
//    RTCReg &= ~(B_RTC_IO_REGB_PIE | B_RTC_IO_REGB_AIE |B_RTC_IO_REGB_UIE);
//    RTCReg |= B_RTC_IO_REGB_AIE;
//    WriteCmos8 (R_RTC_IO_REGA, RTCReg);
//
//    RTCReg = ReadCmos8 (R_RTC_IO_REGB);
//    RTCReg &= ~B_RTC_IO_REGB_SET;
//    WriteCmos8 (R_RTC_IO_REGB, RTCReg);
//  }
//
//  if (CpuSetup.VT) {
//    CmosVmxSmxFlag = CmosVmxSmxFlag | B_VMX_SETUP_FLAG;
//  }
//
//  IsVtSupport = IsVmxSupported ();
//  if (IsVtSupport) {
//    CmosVmxSmxFlag = CmosVmxSmxFlag | B_VMX_CPU_FLAG;
//  }
//
//  //
//  // TXT feature initialization in PEI.
//  //
//  if (FeaturePcdGet(PcdTXTSupported)) {
//    PeiTxtExecute (PeiServices, &CpuSetup, IsVtSupport, &CmosVmxSmxFlag);
//  } else {
//    PeiVTExecute (PeiServices, &CpuSetup, IsVtSupport);
//    //
//    // Check PCH TXT capability
//    //
//    Data32 = MmioRead32 (PcdGet32 (PcdTxtPublicBase) + 0x10);
//    if (Data32 & BIT0) {
//      ChipsetIsTxtCapable = TRUE;
//    } else {
//      ChipsetIsTxtCapable = FALSE;
//    }
//
//    Status = PeiServicesGetBootMode (&BootMode);
//
//    if ((ChipsetIsTxtCapable == 0) && (BootMode != BOOT_ON_S3_RESUME) && (BootMode != BOOT_ON_S4_RESUME)) {
//      DEBUG ((DEBUG_ERROR | DEBUG_INFO, "Warm boot or S3 !\n"));
//
//      TxtFlag = ReadExtCmos8 ( R_XCMOS_INDEX, R_XCMOS_DATA, VmxSmxFlag);
//      DEBUG ((DEBUG_ERROR | DEBUG_INFO, "Read TXT Flag By CMOS: %x\n", TxtFlag));
//
//      if (TxtFlag & B_SMX_CHIPSET_FLAG){
//        DEBUG ((DEBUG_ERROR | DEBUG_INFO, "ChipsetIsTxtCapable == TURE\n"));
//        ChipsetIsTxtCapable = TRUE;
//      }
//    }
//
//    if (ChipsetIsTxtCapable) {
//      CmosVmxSmxFlag = CmosVmxSmxFlag | B_SMX_CHIPSET_FLAG;
//    }
//
//    TempCmosVmxSmxFlag = ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, VmxSmxFlag);
//    if (CmosVmxSmxFlag != TempCmosVmxSmxFlag) {
//      WriteExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, VmxSmxFlag, CmosVmxSmxFlag);
//    }
//  }
//
//  IoWrite16 (PcdGet16 (PcdPerfPkgAcpiIoPortBaseAddress) + R_ACPI_IO_PM1_EN, 0);
//
//
////#ifdef PCH_CNL
//  IoWrite32 (PcdGet16 (PcdPerfPkgAcpiIoPortBaseAddress) + R_ACPI_IO_GPE0_EN_127_96, 0x00040000);
//
//  MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM0, R_ICL_PCH_LP_GPIO_PCR_GPP_A_GPI_GPE_STS), 0);
//  MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM0, R_ICL_PCH_LP_GPIO_PCR_GPP_B_GPI_GPE_STS), 0);
//  MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM1, R_ICL_PCH_LP_GPIO_PCR_GPP_D_GPI_GPE_STS), 0);
////#else
////  IoWrite32 (PcdGet16 (PcdPerfPkgAcpiIoPortBaseAddress) + R_ACPI_IO_GPE0_EN_127_96, 0x00040000);
////
////  MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM0, R_SKL_PCH_LP_PCR_GPIO_GPP_A_GPI_GPE_STS), 0);
////  MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM0, R_SKL_PCH_LP_PCR_GPIO_GPP_B_GPI_GPE_STS), 0);
////  MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM1, R_SKL_PCH_LP_PCR_GPIO_GPP_D_GPI_GPE_STS), 0);
////#endif
//
//  if (IsPchLp()) {
//    //
//    // GpeGppFSts : Clear status GPI_GPE_STS_GPP_F_0
//    // GpeGppGSts : Clear status GPI_GPE_STS_GPP_G_0
//    //
////#ifdef PCH_CNL
//    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM3, R_ICL_PCH_LP_GPIO_PCR_GPP_F_GPI_GPE_STS), 0);
//    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM3, R_ICL_PCH_LP_GPIO_PCR_GPP_G_GPI_GPE_STS), 0);
////#else
////    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM3, R_SKL_PCH_LP_PCR_GPIO_GPP_F_GPI_GPE_STS), 0);
////    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM3, R_SKL_PCH_LP_PCR_GPIO_GPP_G_GPI_GPE_STS), 0);
////#endif
//  } else {
//    //
//    // GpeGppFSts : Clear status GPI_GPE_STS_GPP_F_0
//    // GpeGppGSts : Clear status GPI_GPE_STS_GPP_G_0
//    // GpeGppHSts : Clear status GPI_GPE_STS_GPP_H_0
//    // GpeGppISts : Clear status GPI_GPE_STS_GPP_I_0
//    //
////#ifdef PCH_CNL
////    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM1, R_CNL_PCH_H_GPIO_PCR_GPP_F_GPI_GPE_STS), 0);
////    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM1, R_CNL_PCH_H_GPIO_PCR_GPP_G_GPI_GPE_STS), 0);
////    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM1, R_CNL_PCH_H_GPIO_PCR_GPP_H_GPI_GPE_STS), 0);
////    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM3, R_CNL_PCH_H_GPIO_PCR_GPP_I_GPI_GPE_STS), 0);
////#else
////    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM1, R_SKL_PCH_H_PCR_GPIO_GPP_F_GPI_GPE_STS), 0);
////    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM1, R_SKL_PCH_H_PCR_GPIO_GPP_G_GPI_GPE_STS), 0);
////    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM1, R_SKL_PCH_H_PCR_GPIO_GPP_H_GPI_GPE_STS), 0);
////    MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM3, R_SKL_PCH_H_PCR_GPIO_GPP_I_GPI_GPE_STS), 0);
////#endif
//  }
//
////#ifdef PCH_CNL
//  MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM2, R_ICL_PCH_LP_GPIO_PCR_GPD_GPI_GPE_STS), 0);
////#else
////  MmioWrite32 (PCH_PCR_ADDRESS (PID_GPIOCOM2, R_SKL_PCH_LP_PCR_GPIO_GPD_GPI_GPE_STS), 0);
////#endif
//
//  SetFlashType (FLASH_DEVICE_TYPE_SPI);
//  SetSensorHubType (SystemConfiguration.SensorHubType);
//
//  if (SystemConfiguration.LowPowerS0Idle && SystemConfiguration.CSNotifyEC && SystemConfiguration.EcLowPowerMode) {
//    OemSvcEcSetLowPowerMode (TRUE);
//  } else {
//    OemSvcEcSetLowPowerMode (FALSE);
//  }
//
//    if ((BootMode == BOOT_ON_S3_RESUME)) {
//      Status = PeiServicesNotifyPpi(&mSetS3RestoreAcpiNotify);
//      ASSERT_EFI_ERROR (Status);
//      return Status;
//    }
  return EFI_SUCCESS;
}

///**
// Provide a callback when the MemoryDiscovered PPI is installed.
//
// @param             PeiServices         The PEI core services table.
// @param             NotifyDescriptor    The descriptor for the notification event.
// @param             Ppi                 Pointer to the PPI in question.
//
// @retval            EFI_SUCCESS         The function is successfully processed.
//
//*/
//EFI_STATUS
//RestoreAcpiVariableHob (
//  IN  EFI_PEI_SERVICES                **PeiServices,
//  IN  EFI_PEI_NOTIFY_DESCRIPTOR       *NotifyDescriptor,
//  IN  VOID                            *Ppi
//  )
//{
//  EFI_BOOT_MODE                         BootMode;
//  EFI_STATUS                            Status;
//  //
//  // ReProduce ACPI S3 hob when boot path is S3
//  //
//  Status = PeiServicesGetBootMode (&BootMode);
//  ASSERT_EFI_ERROR (Status);
//  ASSERT_EFI_ERROR (Status);
//  //
//  // S3 resume and RST resume do this
//  //
//
//  return EFI_SUCCESS;
//}
//EFI_STATUS
//EFIAPI
//SetFlashType (
//  IN UINT32         FlashType
//  )
//{
//  UINTN      PciSpiBase;
//  UINTN      LpcPciBase;
//
//  PciSpiBase = MmPciBase (
//                 DEFAULT_PCI_BUS_NUMBER_PCH,
//                 PCI_DEVICE_NUMBER_PCH_SPI,
//                 PCI_FUNCTION_NUMBER_PCH_SPI
//                 );
//  LpcPciBase = MmPciBase (
//                 DEFAULT_PCI_BUS_NUMBER_PCH,
//                 PCI_DEVICE_NUMBER_PCH_LPC,
//                 PCI_FUNCTION_NUMBER_PCH_LPC
//                 );
//
//  ///
//  /// Set the requested state
//  ///
//  switch (FlashType) {
//
//  case FLASH_DEVICE_TYPE_SPI:
//    //
//    // LPC/eSPI
//    //
//    MmioAndThenOr16 (
//      (UINTN) (LpcPciBase + R_LPC_CFG_BC),
//      (UINT16) ~B_LPC_CFG_BC_BBS,
//      (UINT16) (V_LPC_CFG_BC_BBS_SPI << N_LPC_CFG_BC_BBS)
//      );
//    //
//    // SPI
//    //
//    MmioAndThenOr8 (
//      (UINTN) (PciSpiBase + R_SPI_CFG_BC),
//      (UINT8) ~B_SPI_CFG_BC_BBS,
//      (UINT8) (V_SPI_CFG_BC_BBS_SPI << N_SPI_CFG_BC_BBS)
//      );
//    //
//    // Set PCR[DMI] + 274Ch[10] = 0b for SPI
//    //
//    break;
//
//  case FLASH_DEVICE_TYPE_LPC:
//    //
//    // LPC/eSPI
//    //
//    MmioAndThenOr16 (
//      (UINTN) (LpcPciBase + R_LPC_CFG_BC),
//      (UINT16) ~B_LPC_CFG_BC_BBS,
//      (UINT16) (V_LPC_CFG_BC_BBS_LPC << N_LPC_CFG_BC_BBS)
//      );
//    //
//    // SPI
//    //
//    MmioAndThenOr8 (
//      (UINTN) (PciSpiBase + R_SPI_CFG_BC),
//      (UINT8) ~B_SPI_CFG_BC_BBS,
//      (UINT8) (V_SPI_CFG_BC_BBS_LPC << N_SPI_CFG_BC_BBS)
//      );
//    //
//    // Set PCR[DMI] + 274C[10] = 1b for LPC/eSPI
//    //
//    PchPcrAndThenOr16 (PID_DMI, R_PCH_DMI_PCR_GCS, 0xFFFF, (UINT16) B_PCH_DMI_PCR_BBS);
//    break;
//
//  default:
//    ///
//    /// This is an invalid use of the protocol
//    /// See definition, but caller must call with valid value
//    ///
//    ASSERT (!EFI_UNSUPPORTED);
//    break;
//  }
//
//  return EFI_SUCCESS;
//}

//EFI_STATUS
//SetSensorHubType (
//  IN  UINT8                    SensorHubType
//  )
//{
////  UINT32                       GpioValue;
//
////  GpioValue = IoRead32 (PcdGet16 (PcdPchGpioBaseAddress) + R_PCH_GP_N_CONFIG0 + 46 * 8);
////
////  //
////  // Set GPIO46[31] to enable USB Sensor Hub if SensorHubType is selected to USB in BIOS setup menu.
////  // Clear GPIO46[31] to disable USB Sensor Hub if SensorHubType is selected to I2C or None in BIOS setup menu.
////  //
////  if (SensorHubType == 2) {
////    GpioValue |= BIT31;
////  } else {
////    GpioValue &= ~BIT31;
////  }
////
////  IoWrite32 (PcdGet16 (PcdPchGpioBaseAddress) + R_PCH_GP_N_CONFIG0 + 46 * 8, GpioValue);
//
//  return EFI_SUCCESS;
//}
