/** @file
    This function offers an interface to dynamically modify GPIO table before memory initialization.

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcChipsetLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <PlatformGpioConfig.h>

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTablePreMemTglUDdr4_ProjectBoard[] =
{
  {0x0}
};

//[start-210731-STORM1101-modify]//
#ifdef C770_SUPPORT
//[-start-210908-YUNLEI0131-modify]//
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaC770_DGPU_GPIOTable[] =
{
  {GPIO_VER2_LP_GPP_A15,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,      GpioIntDis,                    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}}, // PEG_RST_M_N
  {GPIO_VER2_LP_GPP_A20,   {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirInInv,   GpioOutDefault,  GpioIntLevel | GpioIntSci,     GpioHostDeepReset, GpioTermNone ,   GpioPadConfigUnlock }}, // GPU_PCIE_WAKE_N
  {GPIO_VER2_LP_GPP_D8,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault,  GpioIntDefault,                GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPU_CLKREQ3_N
  {GPIO_VER2_LP_GPP_E10,   {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,     GpioIntDefault,                GpioPlatformReset, GpioTermNone }}, // DGPU_PWR_CTRL_EN
  {GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 
  {GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}}, //Board ID13
};
//[-end-210908-YUNLEI0131-modify]//

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaC770_14or16BoardID_GPIOTable[] =
{
{GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5
{GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6
};
#endif
//[end-210731-STORM1101-modify]//

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTablePreMemTglUDdr4Simics_ProjectBoard[] =
{
  {GPIO_VER2_LP_GPP_C11, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WWAN_FCP_OFF_N
  {GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WWAN_DISABLE_N
  {GPIO_VER2_LP_GPP_H23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WWAN_PWREN
  {GPIO_VER2_LP_GPP_B17, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WWAN_PERST_N
 // {GPIO_VER2_LP_GPP_C9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //WWAN_WAKE_N
  {GPIO_VER2_LP_GPP_C10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WWAN_RST_N
  {0x0}
};

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableTglUDdr4WwanOnEarlyPreMem_ProjectBoard[] =
{
  {GPIO_VER2_LP_GPP_H23,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_PWREN
  {GPIO_VER2_LP_GPP_C11,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_FCP_OFF_N
  {GPIO_VER2_LP_GPP_C10,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_RST_N
  {GPIO_VER2_LP_GPP_B17,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioPlatformReset,  GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_PERST_N
  {GPIO_VER2_LP_GPP_C9,   { GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock}},    //WWAN_WAKE_N
  {GPIO_VER2_LP_GPP_D15,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_DISABLE_N
};

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableTglUDdr4WwanOffEarlyPreMem_ProjectBoard[] =
{
  {GPIO_VER2_LP_GPP_C10,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutLow,      GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_RST_N
  {GPIO_VER2_LP_GPP_B17,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutLow,      GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_PERST_N
  {GPIO_VER2_LP_GPP_C11,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutLow,      GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_FCP_OFF_N
};

#ifdef S570_SUPPORT
//[-start-210917-QINGLIN0068-modify]//
GPIO_INIT_CONFIG S570_DGPU_GPIOTable[] = {
// ====================================================================================================================================================================================================
// DGPU GPIIO init  
// ====================================================================================================================================================================================================
  {GPIO_VER2_LP_GPP_D8,  {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPU_CLKREQ_N
//  {GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//GPU_EVENT_N
//  {GPIO_VER2_LP_GPP_B8,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//FB_GC6_EN_R
  {GPIO_VER2_LP_GPP_D0,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PXS_RST_N_R
  {GPIO_VER2_LP_GPP_B7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PXS_PWREN
//  {GPIO_VER2_LP_GPP_B16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//NVVDD_PWROK_R
  {GPIO_VER2_LP_GPP_H9,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//VGA_PWRGD
  {GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 
  {GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}} //Board ID13
};
//[-end-210917-QINGLIN0068-modify]//
#endif

//[-start-210803-QINGLIN0008-add]//
//[-start-210908-QINGLIN0055-modify]//
//[-start-210914-SHAONN0007-modify]//
#ifdef S370_SUPPORT
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S370_DGPU_GPIOTable[] =
{
  {GPIO_VER2_LP_GPP_F15,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault,  GpioIntDis,                    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}}, //Board ID12 
  {GPIO_VER2_LP_GPP_F16,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault,  GpioIntDis,                    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}}, //Board ID13
  {GPIO_VER2_LP_GPP_D8,    {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault,  GpioIntDefault,                GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPU_CLKREQ3_N
  {GPIO_VER2_LP_GPP_A15,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,                    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//PEG_RST_N
  {GPIO_VER2_LP_GPP_A20,   {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirInInv,   GpioOutDefault,  GpioIntLevel | GpioIntSci,     GpioHostDeepReset, GpioTermNone ,   GpioPadConfigUnlock }}, //GPU_PCIE_WAKE_N
  {GPIO_VER2_LP_GPP_D14,   {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault,  GpioIntDis,                    GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//VGA_CORE_SAMC_SA_VR_PG_R2
  {GPIO_VER2_LP_GPP_E10,   {GpioPadModeGpio,    GpioHostOwnAcpi,    GpioDirOut,     GpioOutHigh,     GpioIntDis,                    GpioPlatformReset, GpioTermNone}}, // DGPU_PWR_CTRL_EN
};
//[-end-210914-SHAONN0007-modify]//
//[-end-210908-QINGLIN0055-modify]//
#endif
//[-end-210803-QINGLIN0008-add]//

//[-start-210831-GEORGE0001-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S77014_DGPU_GPIOTable[] = 
{
  {GPIO_VER2_LP_GPP_E11,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioPlatformReset, GpioTermNone,    GpioOutputStateUnlock}},//PXS_PWREN
  {GPIO_VER2_LP_GPP_D18,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,       GpioOutDefault, GpioIntDis,    GpioPlatformReset, GpioTermNone,    GpioLockDefault}},//VGA_PWRGD
  {GPIO_VER2_LP_GPP_H13,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioPlatformReset, GpioTermNone,    GpioOutputStateUnlock}},//PXS_RST_R_N
  {GPIO_VER2_LP_GPP_D8,   {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault,  GpioTermDefault, GpioPadLock}},//GPU_CLKREQ_N
  {GPIO_VER2_LP_GPP_F15,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 
  {GPIO_VER2_LP_GPP_F16,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID13
};
#endif
//[-end-210831-GEORGE0001-add]//

/**
 This function offers an interface to dynamically modify GPIO table before memory initialization.

 @param[in, out]    GpioTablePreMem      On entry, points to a structure that specifies the GPIO setting.
                                         On exit, points to the updated structure.
 @param[in, out]    GpioTableCountPreMem On entry, points to a value that the length of GPIO table .
                                         On exit, points to the updated value.

 @retval            EFI_UNSUPPORTED      Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED    Alter the Configuration Parameter.
 @retval            EFI_SUCCESS          The function performs the same operation as caller.
                                         The caller will skip the specified behavior and assuming
                                         that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcModifyGpioSettingTablePreMem (
  IN OUT  GPIO_INIT_CONFIG                 **GpioTablePreMem,
  IN OUT  UINT16                           *GpioTableCountPreMem
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
//
// *GpioTablePreMem = mGpioTablePreMemTglUDdr4Simics;
//
// *GpioTableCountPreMem = 5;
//[-start-210721-QINGLIN0001-modify]//
#ifdef LCFC_SUPPORT
#if defined (S570_SUPPORT)
  *GpioTablePreMem = S570_DGPU_GPIOTable;
  *GpioTableCountPreMem = sizeof(S570_DGPU_GPIOTable)/sizeof(GPIO_INIT_CONFIG);
  return EFI_MEDIA_CHANGED;
//[start-210731-STORM1101-modify]//
#elif defined(C770_SUPPORT)
// storm_bringup can optimise here
  *GpioTablePreMem = YogaC770_DGPU_GPIOTable;
  *GpioTableCountPreMem = sizeof(YogaC770_DGPU_GPIOTable)/sizeof(GPIO_INIT_CONFIG);
  return EFI_MEDIA_CHANGED;
//[end-210731-STORM1101-modify]//

//[-start-210803-QINGLIN0008-add]//
#elif defined(S370_SUPPORT)
  *GpioTablePreMem = S370_DGPU_GPIOTable;
  *GpioTableCountPreMem = sizeof(S370_DGPU_GPIOTable)/sizeof(GPIO_INIT_CONFIG);
  return EFI_MEDIA_CHANGED;
//[-end-210803-QINGLIN0008-add]//

//[-start-210831-GEORGE0001-add]//
#elif defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
  *GpioTablePreMem = S77014_DGPU_GPIOTable;
  *GpioTableCountPreMem = sizeof(S77014_DGPU_GPIOTable)/sizeof(GPIO_INIT_CONFIG);
  return EFI_MEDIA_CHANGED;
//[-end-210831-GEORGE0001-add]//

#else
  return EFI_UNSUPPORTED;
#endif
#else
  return EFI_UNSUPPORTED;
#endif
//[-end-210721-QINGLIN0001-modify]//
}
