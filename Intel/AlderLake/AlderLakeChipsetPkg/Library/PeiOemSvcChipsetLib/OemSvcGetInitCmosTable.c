/** @file
    This function will provide OEM CMOS default table.

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcChipsetLib.h>

//
// Sample
//
//
// CMOS_DEFAULT_DATA mOemCmosDefaultTable[] = {
// //
// // OEM CMOS bank 0
// // 0x48 ~ 0x4F : OEM Reserve
// // 0x6C ~ 0x7F : OEM Reserve
// //
// { 0x48,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
// { 0x49,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
// { 0x4A,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
// { 0x4B,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
// { 0x4C,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
// { 0x4D,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
// { 0x4E,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
// { 0x4F,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
// //
// // OEM CMOS bank 1
// // 0x00 ~ 0x0F : OEM Reserve
// // 0x40 ~ 0x57 : OEM Reserve
// // 0x60 ~ 0x7F : OEM Reserve
// //
// { 0x00,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
// { 0x01,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
// { 0x02,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
// { 0x03,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
// { 0xFF, 0xFF, 0xFF, 0xFF } // end of table
// };

/**
 This function will provide OEM CMOS default table.

 @param[in, out]    CmosDefaultTable    The CMOS default table from OEM.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcGetInitCmosTable (
  OUT CMOS_DEFAULT_DATA       **CmosDefaultTable
  )
{

//
//   *CmosDefaultTable = mOemCmosDefaultTable;
// 
//   return EFI_MEDIA_CHANGED;
//

  return EFI_UNSUPPORTED;
}
