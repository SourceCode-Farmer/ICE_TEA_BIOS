/** @file
    This function offers an interface to config GPIO table at early phase.

;******************************************************************************
;* Copyright (c) 2017 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcChipsetLib.h>

/**
  This function offers an interface to config GPIO table at early phase.

 @param[out]        EarlyGpioTable      On entry, points to a structure that specifies the GPIO setting (default is empty). 
                                        On exit, points to the updated structure. 
 @param[out]        EarlyGpioTableCount On entry, points to a value that the length of GPIO table (default is 0).
                                        On exit, points to the updated value.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcEarlyGpioSettingTable (
  OUT  GPIO_INIT_CONFIG                 **EarlyGpioTable,
  OUT  UINT16                           *EarlyGpioTableCount
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/
  return EFI_UNSUPPORTED;
}
