/** @file
  CPU/Chipset/Platform Initial depends on project characteristic.

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <ChipsetSetupConfig.h>
#include <Library/PeiOemSvcChipsetLib.h>
#include <Library/InsydeChipsetEcLib.h>
#include <Library/PeiInsydeChipsetLib.h>
#include <Library/EcMiscLib.h>
#include <Library/EcLib.h>
/**
  Write Io port to control Ec.

  @param  EnableEcTurboControlMode  To enable Ec Turbo Mode or not.
  @param  EcBrickCap                Value written to data port.
  @param  EcPollingPeriod           Value written to data port.
  @param  EcGuardBandVal            Value written to data port.
  @param  EcAlgorithmSel            Value written to data port.


  @retval EFI_SUCCESS               Always returns success.
**/
EFI_STATUS
EcTurboControlMode (
  IN      BOOLEAN        EnableEcTurboControlMode,
  IN      UINT8          EcBrickCap,
  IN      UINT8          EcPollingPeriod,
  IN      UINT8          EcGuardBandVal,
  IN      UINT8          EcAlgorithmSel
  )
{
  EFI_STATUS        Status;

  Status = EFI_SUCCESS;

  if ( EnableEcTurboControlMode ) {
    Status = SendEcCommand (EC_TURBOCTRL_TESTMODE_ENABLE);
    Status = SendEcData (EcBrickCap);
    Status = SendEcData (EcPollingPeriod);
    Status = SendEcData (EcGuardBandVal);
    Status = SendEcData (EcAlgorithmSel);
  } else {
    Status = SendEcCommand (EC_TURBOCTRL_TESTMODE_DISABLE);
  }

  return EFI_SUCCESS;
}

/**
  CPU/Chipset/Platform Initial depends on project characteristic.

  @param[in]  *Buffer               A pointer to CHIPSET_CONFIGURATION.
  @param[in]  SetupVariableExist    Setup variable be found in variable storage or not.

  @retval     EFI_UNSUPPORTED       Returns unsupported by default.
  @retval     EFI_SUCCESS           The service is customized in the project.
  @retval     EFI_MEDIA_CHANGED     The value of IN OUT parameter is changed. 
  @retval     Others                Depends on customization.
**/
EFI_STATUS
OemSvcInitPlatformInit (
  IN  VOID                                 *Buffer,
  IN  BOOLEAN                              SetupVariableExist
  )
{
  CHIPSET_CONFIGURATION                  *SystemConfiguration;
  EFI_STATUS                             Status;
  CPU_SETUP                              CpuSetup;

  SystemConfiguration = (CHIPSET_CONFIGURATION *)Buffer;

  Status = GetChipsetCpuSetupVariablePei (&CpuSetup);
  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }
  EcTurboControlMode (
    CpuSetup.EcTurboControlMode,
    CpuSetup.AcBrickCapacity,
    CpuSetup.EcPollingPeriod,
    CpuSetup.EcGuardBandValue,
    CpuSetup.EcAlgorithmSel
    );

  return EFI_SUCCESS;
}

