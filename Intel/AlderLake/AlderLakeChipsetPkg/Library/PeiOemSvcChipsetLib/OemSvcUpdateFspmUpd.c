/** @file
  
;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiOemSvcChipsetLib.h>
#include <FspEas.h>
#include <FspmUpd.h>

/**
 This function offers an interface to modify FSPM_UPD data before the FSP-M API be called.

 @param[in,out]     FspUpdDataPtr                 A pointer to the UPD data region data strcture address.
                                                  It must convert to FSPM_UPD structure for update FSPM UPD.

                                                  If you set this pointer to NULL, FSP API will use the
                                                  default FSP-M UPD settings in the binary.

 @retval            EFI_UNSUPPORTED               Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED             Alter the Configuration Parameter.
 @retval            EFI_SUCCESS                   The function performs the same operation as caller.
                                                  The caller will skip the specified behavior and assuming
                                                  that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdateFspmUpd (
  IN OUT VOID                                     **FspUpdDataPtr
  )
{
  FSPM_UPD                                        *FspmUpd;

  FspmUpd = (FSPM_UPD *)(*FspUpdDataPtr);

  //
  // Set this pointer to NULL, use the default FSP-M UPD settings in the binary.
  // BUGBUG: When set the FspUpdDataPtr to NULL, will cause the system to hang in FSP binary,
  //         still can't use the default UPD data in binary to boot successfully.
  //
//  *FspUpdDataPtr = NULL;

  return EFI_UNSUPPORTED;
}
