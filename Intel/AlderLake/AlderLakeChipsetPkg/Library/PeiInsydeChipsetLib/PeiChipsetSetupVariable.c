/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/PeiInsydeChipsetLib.h>
#include <Ppi/ReadOnlyVariable2.h>

/**
 Retrieve setup configuration data

 @param[out] SetupVariable      Pointer to the structure of CHIPSET_CONFIGURATION,
                                this pointer must be allocated with sizeof(CHIPSET_CONFIGURATION)
                                before being called

 @retval EFI_SUCCESS            The Chipset configuration is successfully retrieved
 @retval EFI_INVALID_PARAMETER  NULL pointer for input ChipsetConfig paramater
 @return others                 Failed to retrieve Chipset configuration
**/
EFI_STATUS
EFIAPI
GetChipsetSetupVariablePei (
  OUT CHIPSET_CONFIGURATION             *SetupVariable
  )
{
  EFI_STATUS                       Status;
  UINTN                            Size;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI  *VariablePpi;

  if (SetupVariable == NULL) {
    ASSERT_EFI_ERROR (SetupVariable != NULL);
    return EFI_INVALID_PARAMETER;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariablePpi
           );
  if (EFI_ERROR(Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Size = PcdGet32 (PcdSetupConfigSize);
  Status = VariablePpi->GetVariable (
                          VariablePpi,
                          SETUP_VARIABLE_NAME,
                          &gSystemConfigurationGuid,
                          NULL,
                          &Size,
                          SetupVariable
                          );
  ASSERT_EFI_ERROR (Status);
  return Status;
}