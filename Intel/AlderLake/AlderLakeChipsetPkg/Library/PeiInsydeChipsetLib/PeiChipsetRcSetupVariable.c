/** @file

;******************************************************************************
;* Copyright (c) 2015-2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/PeiInsydeChipsetLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <MeSetup.h>

/**
 Retrieve SETUP DATA.

 @param[out] SetupVariable      Pointer to the structure of SETUP_DATA,
                                this pointer must be allocated with sizeof(SETUP_DATA)
                                before being called

 @retval EFI_SUCCESS            The setup data is successfully retrieved
 @retval EFI_INVALID_PARAMETER  NULL pointer for input SaSetupVariable paramater
 @return others                 Failed to retrieve SA setup data
**/
EFI_STATUS
EFIAPI
GetChipsetSetupDataVariablePei (
  OUT SETUP_DATA                        *SetupVariable
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Size;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI       *VariablePpi;

  if (SetupVariable == NULL) {
    ASSERT_EFI_ERROR (SetupVariable != NULL);
    return EFI_INVALID_PARAMETER;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariablePpi
           );
  if (EFI_ERROR(Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Size = sizeof (SETUP_DATA);
  Status = VariablePpi->GetVariable (
                          VariablePpi,
                          PLATFORM_SETUP_VARIABLE_NAME,
                          &gSetupVariableGuid,
                          NULL,
                          &Size,
                          SetupVariable
                          );
  ASSERT_EFI_ERROR (Status);
  return Status;
}

/**
 Retrieve SA setup configuration data

 @param[out] SaSetupVariable    Pointer to the structure of SA_SETUP,
                                this pointer must be allocated with sizeof(SA_SETUP)
                                before being called

 @retval EFI_SUCCESS            The SA setup data is successfully retrieved
 @retval EFI_INVALID_PARAMETER  NULL pointer for input SaSetupVariable paramater
 @return others                 Failed to retrieve SA setup data
**/
EFI_STATUS
EFIAPI
GetChipsetSaSetupVariablePei (
  OUT SA_SETUP                          *SaSetupVariable
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Size;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI       *VariablePpi;

  if (SaSetupVariable == NULL) {
    ASSERT_EFI_ERROR (SaSetupVariable != NULL);
    return EFI_INVALID_PARAMETER;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariablePpi
           );
  if (EFI_ERROR(Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Size = sizeof (SA_SETUP);
  Status = VariablePpi->GetVariable (
                          VariablePpi,
                          SA_SETUP_VARIABLE_NAME,
                          &gSaSetupVariableGuid,
                          NULL,
                          &Size,
                          SaSetupVariable
                          );
  ASSERT_EFI_ERROR (Status);
  return Status;
}


/**
 Retrieve ME setup configuration data

 @param[out] MeSetupVariable    Pointer to the structure of ME_SETUP,
                                this pointer must be allocated with sizeof(ME_SETUP)
                                before being called

 @retval EFI_SUCCESS            The ME setup data is successfully retrieved
 @retval EFI_INVALID_PARAMETER  NULL pointer for input MeSetupVariable paramater
 @return others                 Failed to retrieve ME setup data
**/
EFI_STATUS
EFIAPI
GetChipsetMeSetupVariablePei (
  OUT ME_SETUP                          *MeSetupVariable
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Size;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI       *VariablePpi;

  if (MeSetupVariable == NULL) {
    ASSERT_EFI_ERROR (MeSetupVariable != NULL);
    return EFI_INVALID_PARAMETER;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariablePpi
           );
  if (EFI_ERROR(Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Size = sizeof (ME_SETUP);
  Status = VariablePpi->GetVariable (
                          VariablePpi,
                          ME_SETUP_VARIABLE_NAME,
                          &gMeSetupVariableGuid,
                          NULL,
                          &Size,
                          MeSetupVariable
                          );
  ASSERT_EFI_ERROR (Status);
  return Status;
}


/**
 Retrieve CPU setup configuration data

 @param[out] CpuSetupVariable   Pointer to the structure of CPU_SETUP,
                                this pointer must be allocated with sizeof(CPU_SETUP)
                                before being called

 @retval EFI_SUCCESS            The CPU setup data is successfully retrieved
 @retval EFI_INVALID_PARAMETER  NULL pointer for input CpuSetupVariable paramater
 @return others                 Failed to retrieve CPU setup data
**/
EFI_STATUS
EFIAPI
GetChipsetCpuSetupVariablePei (
  OUT CPU_SETUP                         *CpuSetupVariable
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Size;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI       *VariablePpi;

  if (CpuSetupVariable == NULL) {
    ASSERT_EFI_ERROR (CpuSetupVariable != NULL);
    return EFI_INVALID_PARAMETER;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariablePpi
           );
  if (EFI_ERROR(Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Size = sizeof (PCH_SETUP);
  Status = VariablePpi->GetVariable (
                          VariablePpi,
                          CPU_SETUP_VARIABLE_NAME,
                          &gCpuSetupVariableGuid,
                          NULL,
                          &Size,
                          CpuSetupVariable
                          );
  ASSERT_EFI_ERROR (Status);
  return Status;
}


/**
 Retrieve PCH setup configuration data

 @param[out] PchSetupVariable   Pointer to the structure of PCH_SETUP,
                                this pointer must be allocated with sizeof(PCH_SETUP)
                                before being called

 @retval EFI_SUCCESS            The PCH setup data is successfully retrieved
 @retval EFI_INVALID_PARAMETER  NULL pointer for input PchSetupVariable paramater
 @return others                 Failed to retrieve PCH setup data
**/
EFI_STATUS
EFIAPI
GetChipsetPchSetupVariablePei (
  OUT PCH_SETUP                         *PchSetupVariable
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Size;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI       *VariablePpi;

  if (PchSetupVariable == NULL) {
    ASSERT_EFI_ERROR (PchSetupVariable != NULL);
    return EFI_INVALID_PARAMETER;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariablePpi
           );
  if (EFI_ERROR(Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Size = sizeof (PCH_SETUP);
  Status = VariablePpi->GetVariable (
                          VariablePpi,
                          PCH_SETUP_VARIABLE_NAME,
                          &gPchSetupVariableGuid,
                          NULL,
                          &Size,
                          PchSetupVariable
                          );
  ASSERT_EFI_ERROR (Status);
  return Status;
}


/**
 Retrieve ME setup storage configuration data

 @param[out] MeSetupVariable    Pointer to the structure of ME_SETUP_STORAGE,
                                this pointer must be allocated with sizeof(ME_SETUP_STORAGE)
                                before being called

 @retval EFI_SUCCESS            The ME setup storage data is successfully retrieved
 @retval EFI_INVALID_PARAMETER  NULL pointer for input MeSetupStorageVariable paramater
 @return others                 Failed to retrieve ME setup storage data
**/
EFI_STATUS
EFIAPI
GetChipsetMeSetupStorageVariablePei (
  OUT ME_SETUP_STORAGE                  *MeSetupStorageVariable
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Size;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI       *VariablePpi;

  if (MeSetupStorageVariable == NULL) {
    ASSERT_EFI_ERROR (MeSetupStorageVariable != NULL);
    return EFI_INVALID_PARAMETER;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariablePpi
           );
  if (EFI_ERROR(Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Size = sizeof (ME_SETUP_STORAGE);
  Status = VariablePpi->GetVariable (
                          VariablePpi,
                          ME_SETUP_STORAGE_VARIABLE_NAME,
                          &gMeSetupVariableGuid,
                          NULL,
                          &Size,
                          MeSetupStorageVariable
                          );
  ASSERT_EFI_ERROR (Status);
  return Status;
}
