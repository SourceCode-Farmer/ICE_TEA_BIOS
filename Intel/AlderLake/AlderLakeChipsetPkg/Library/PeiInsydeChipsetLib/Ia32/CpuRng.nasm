;;@file
;  Generate a random number in 32 bit environment
;
;@copyright
; Copyright (c) 2014 Intel Corporation. All rights reserved
; This software and associated documentation (if any) is furnished
; under a license and may only be used or copied in accordance
; with the terms of the license. Except as permitted by the
; license, no part of this software or documentation may be
; reproduced, stored in a retrieval system, or transmitted in any
; form or by any means without the express written consent of
; Intel Corporation.
; This file contains an 'Intel Peripheral Driver' and is uniquely
; identified as "Intel Reference Module" and is licensed for Intel
; CPUs and chipsets under the terms of your license agreement with
; Intel or your vendor. This file may be modified by the user, subject
; to additional terms of the license agreement.
;
;@par Specification Reference:
;;

SECTION .text

;
; Generate a 16 bit random number
;
global ASM_PFX(GetRandomNumber16)
ASM_PFX(GetRandomNumber16):
        ; rdrand  ax           ; generate a 16 bit RN into eax, CF=1 if RN generated ok, otherwise CF=0
        push  ebp
        mov   ebp, esp
        clc
.0:
        db  0xf,0xc7,0xf0       ; rdrand r16: "0F C7 /6 ModRM:r/m(w)"
        jnc .0                  ; CF will be set if valid number was generated
        leave
        ret

;
; Generate a 32 bit random number
;
global ASM_PFX(GetRandomNumber32)
ASM_PFX(GetRandomNumber32):
        ; rdrand  eax           ; generate a 32 bit RN into eax, CF=1 if RN generated ok, otherwise CF=0
        push  ebp
        mov   ebp, esp
        clc
.1:
        db  0xf,0xc7,0xf0       ; rdrand r32: "0F C7 /6 ModRM:r/m(w)"
        jnc .1                  ; CF will be set if valid number was generated
        leave
        ret

;
; Generate a 64 bit random number
;
global ASM_PFX(GetRandomNumber64)
ASM_PFX(GetRandomNumber64):
        push  ebp
        mov   ebp, esp
        clc
        xor eax, eax            ; RN return 0
        leave
        ret
