/** @file
  SPI Access Init routines

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <PiSmm.h>
#include <Uefi.h>
#include <Guid/EventGroup.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/MmPciLib.h>
#include <Library/PcdLib.h>
#include <Protocol/Spi.h>
#include <Library/UefiRuntimeLib.h>
#include <Protocol/SmmBase2.h>
#include <Protocol/BiosGuard.h>
#include <Library/SmmServicesTableLib.h>

extern PCH_SPI_PROTOCOL       *mSpiProtocol;
extern UINT8                  *mSpiBase;
extern UINTN                  mFlashAreaBaseAddress;
extern UINTN                  mFlashAreaSize;
extern BIOSGUARD_PROTOCOL     *mlibBiosGuardProtocol;

BOOLEAN                mAtRuntime = FALSE;
EFI_EVENT              mExitBootServicesEvent;


/**
  Set AtRuntime flag as TRUE after ExitBootServices.

  @param[in]  Event   The Event that is being processed.
  @param[in]  Context The Event Context.
**/
STATIC
VOID
EFIAPI
ExitBootServicesEvent (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  mAtRuntime = TRUE;
}

/**
  Detect whether the system is at EFI runtime or not

  @param  None

  @retval TRUE                  System is at EFI runtime
  @retval FALSE                 System is not at EFI runtime

**/
BOOLEAN
EFIAPI
AtRuntime (
  VOID
  )
{
  return mAtRuntime;
}

/**
  Detect whether the system is at SMM mode.

  @retval TRUE                  System is at SMM mode.
  @retval FALSE                 System is not at SMM mode.
**/
STATIC
BOOLEAN
IsInSmm (
  VOID
  )
{
  EFI_STATUS                     Status;
  EFI_SMM_BASE2_PROTOCOL         *SmmBase;
  BOOLEAN                        InSmm;

  Status = gBS->LocateProtocol (
                  &gEfiSmmBase2ProtocolGuid,
                  NULL,
                  (VOID **)&SmmBase
                  );
  if (EFI_ERROR (Status)) {
    return FALSE;
  }

  InSmm = FALSE;
  SmmBase->InSmm (SmmBase, &InSmm);
  return InSmm;
}

/**
  Initialization routine for SpiAccessLib

  @param  None

  @retval EFI_SUCCESS           SpiAccessLib successfully initialized
  @return Others                SpiAccessLib initialization failed

**/
EFI_STATUS
EFIAPI
SpiAccessInitDxe (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_GCD_MEMORY_SPACE_DESCRIPTOR       GcdDescriptor;
  EFI_PHYSICAL_ADDRESS                  BaseAddress;
  UINT64                                Length;


  DEBUG ((DEBUG_INFO, "Dxe SpiAccessInit \n"));
#if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 1 
    //
    //  16MB BIOS, 0xFF000000/0x1000000
    //
    BaseAddress = (EFI_PHYSICAL_ADDRESS)(UINTN)(FixedPcdGet32 (PcdFlashAreaBaseAddress) + FixedPcdGet32 (PcdFlashExtendRegionOffset) + FixedPcdGet32 (PcdFlashExtendRegionSizeInUse)) & (~EFI_PAGE_MASK);
    Length      = ((FixedPcdGet32 (PcdFlashAreaSize) - FixedPcdGet32 (PcdFlashExtendRegionOffset) - FixedPcdGet32 (PcdFlashExtendRegionSizeInUse)) + EFI_PAGE_SIZE - 1) & (~EFI_PAGE_MASK);
    Status = gDS->GetMemorySpaceDescriptor (BaseAddress, &GcdDescriptor);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "GetMemorySpaceDescriptor failed to add EFI_MEMORY_RUNTIME attribute to Flash. BaseAddress: 0x%08x\n", (UINT32)BaseAddress));
    }
    Status = gDS->SetMemorySpaceAttributes (
                    BaseAddress,
                    Length,
                    GcdDescriptor.Attributes | EFI_MEMORY_RUNTIME
                    );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "SpiAccessInit failed to add EFI_MEMORY_RUNTIME attribute to Flash. BaseAddress: 0x%08x, Length:0x%08x\n", (UINT32)BaseAddress, (UINT32)Length));
    }

    //
    //  FvExtendedAdvanced, 0xF9F00000/0x00080000
    //
    BaseAddress = (EFI_PHYSICAL_ADDRESS)(UINTN)(FixedPcdGet32 (PcdFlashFvExtendedAdvancedBase)) & (~EFI_PAGE_MASK);
    Length      = ((FixedPcdGet32 (PcdFlashFvExtendedAdvancedSize)) + EFI_PAGE_SIZE - 1) & (~EFI_PAGE_MASK);
    Status = gDS->GetMemorySpaceDescriptor (BaseAddress, &GcdDescriptor);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "GetMemorySpaceDescriptor failed to add EFI_MEMORY_RUNTIME attribute to Flash. BaseAddress: 0x%08x\n", (UINT32)BaseAddress));
    }
    Status = gDS->SetMemorySpaceAttributes (
                    BaseAddress,
                    Length,
                    GcdDescriptor.Attributes | EFI_MEMORY_RUNTIME
                    );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "SpiAccessInit failed to add EFI_MEMORY_RUNTIME attribute to Flash. BaseAddress: 0x%08x, Length:0x%08x\n", (UINT32)BaseAddress, (UINT32)Length));
    }

    //
    //  FvExtendedPostMemory, 0xF9F80000/0x00080000
    //
    BaseAddress = (EFI_PHYSICAL_ADDRESS)(UINTN)(FixedPcdGet32 (PcdFlashFvExtendedPostMemoryBase)) & (~EFI_PAGE_MASK);
    Length      = ((FixedPcdGet32 (PcdFlashFvExtendedPostMemorySize)) + EFI_PAGE_SIZE - 1) & (~EFI_PAGE_MASK);
    Status = gDS->GetMemorySpaceDescriptor (BaseAddress, &GcdDescriptor);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "GetMemorySpaceDescriptor failed to add EFI_MEMORY_RUNTIME attribute to Flash. BaseAddress: 0x%08x\n", (UINT32)BaseAddress));
    }
    Status = gDS->SetMemorySpaceAttributes (
                    BaseAddress,
                    Length,
                    GcdDescriptor.Attributes | EFI_MEMORY_RUNTIME
                    );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "SpiAccessInit failed to add EFI_MEMORY_RUNTIME attribute to Flash. BaseAddress: 0x%08x, Length:0x%08x\n", (UINT32)BaseAddress, (UINT32)Length));
    }
#else

  BaseAddress = mFlashAreaBaseAddress & (~EFI_PAGE_MASK);
  Length      = (mFlashAreaSize + EFI_PAGE_SIZE - 1) & (~EFI_PAGE_MASK);
  //
  // Broadwell: Reserve RCBA space in GCD, which will be reported to OS by E820
  //            It will assert if RCBA Memory Space is not allocated
  //            The caller is responsible for the existence and allocation of the RCBA Memory Spaces
  //            BaseAddress = mSpiBase =(UINTN)(MmioRead32 (PciSpiRegBase + R_PCH_SPI_BAR0) &~(B_PCH_SPI_BAR0_MASK)); 
  // SkyLake: No RCBA, set all bios area "EFI_MEMORY_RUNTIME"
  //
  Status = gDS->GetMemorySpaceDescriptor (BaseAddress, &GcdDescriptor);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "GetMemorySpaceDescriptor failed to add EFI_MEMORY_RUNTIME attribute to Flash.\n"));
  } else {
    Status = gDS->SetMemorySpaceAttributes (
                    BaseAddress,
                    Length,
                    GcdDescriptor.Attributes | EFI_MEMORY_RUNTIME
                    );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "SpiAccessInit failed to add EFI_MEMORY_RUNTIME attribute to Flash.\n"));
    }         
  }
#endif  
  //
  // Locate the SPI protocol.
  //
  Status = gBS->LocateProtocol (
                  &gPchSpiProtocolGuid,
                  NULL,
                  (VOID **)&mSpiProtocol
                  );

  ASSERT_EFI_ERROR (Status);
  //
  //  DxeSpiAccessInitLib will be called by COMBINED_SMM_DXE driver
  //  This event just need to create in runtime driver. 
  //  
//  if (!IsInSmm()) {
    Status = gBS->CreateEventEx (
                    EVT_NOTIFY_SIGNAL,
                    TPL_NOTIFY,
                    ExitBootServicesEvent,
                    NULL,
                    &gEfiEventExitBootServicesGuid,
                    &mExitBootServicesEvent
                    );
    DEBUG ((DEBUG_INFO, "SpiAccessInit CreateEvent ExitBootServicesEven Status: 0x%x \n", Status));
    ASSERT_EFI_ERROR (Status);
//  }
  return Status;
}

/**
  Initialization routine for SpiAccessLib

  @param  None

  @retval EFI_SUCCESS           SpiAccessLib successfully initialized
  @return Others                SpiAccessLib initialization failed

**/
EFI_STATUS
EFIAPI
SpiAccessInitSmm (
  VOID
  )
{
  EFI_STATUS Status;

  DEBUG ((DEBUG_INFO, "SMM SpiAccessInit \n"));

  if (gSmst == NULL) {
    Status = EFI_NOT_READY;
    ASSERT_EFI_ERROR(Status);
  } else {
  //
  // Locate the SMM SPI protocol.
  //
  Status = gSmst->SmmLocateProtocol (
                  &gPchSmmSpiProtocolGuid,
                  NULL,
                  (VOID **)&mSpiProtocol
                  );
  
  ASSERT_EFI_ERROR (Status);

  //
  // Locate the BiosGuard protocol.
  //
    Status = gSmst->SmmLocateProtocol (
               &gSmmBiosGuardProtocolGuid,
               NULL, 
               (VOID **) &mlibBiosGuardProtocol
               );
    ASSERT_EFI_ERROR (Status);
  }
  return Status;
}

/**
  Initialization routine for SpiAccessLib

  @param  None

  @retval EFI_SUCCESS           SpiAccessLib successfully initialized
  @return Others                SpiAccessLib initialization failed

**/
EFI_STATUS
EFIAPI
SpiAccessInit (
  VOID
  )
{
  if (IsInSmm ()) {
    return SpiAccessInitSmm();
  } else {
    return SpiAccessInitDxe();
  }
}

/**
  This routine uses to free the allocated resource by SpiAccessInit ().

  @retval EFI_SUCCESS    Free allocated resource successful.
  @return Others         Free allocated resource failed.
**/
EFI_STATUS
EFIAPI
SpiAccessDestroy (
  VOID
  )
{
  EFI_STATUS        Status;

  if (mExitBootServicesEvent != NULL) {
    Status = gBS->CloseEvent (mExitBootServicesEvent);
    ASSERT_EFI_ERROR (Status);
  }
  return EFI_SUCCESS;
}