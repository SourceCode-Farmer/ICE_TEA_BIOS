/** @file
  SPI Access Init routines

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Ppi/Spi.h>
#include <Library/PeiServicesLib.h>
#include <Ppi/Spi.h>
#include <Library/MmPciLib.h>
#include <Library/IoLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Register/SpiRegs.h>
#include <IndustryStandard/Pci22.h>
#include <Library/SpiCommonLib.h>
#include <Register/PchRegs.h>
#include <Register/SpiRegs.h>
// gino: no this file in ClientOneSilicon path >>
// #include <Register/PchRegsPcr.h>
// gino: no this file in ClientOneSilicon path <<
#include <PchReservedResources.h>
//[-start-191225-IB16740000-add]// for PCI_DEVICE_NUMBER_PCH_LPC & PCI_FUNCTION_NUMBER_PCH_XHCI define
#include <PchBdfAssignment.h>
//[-end-191225-IB16740000-add]//
typedef struct {
  EFI_PEI_PPI_DESCRIPTOR  PpiDescriptor;
  SPI_INSTANCE            SpiInstance;
} PEI_SPI_INSTANCE;

extern PCH_SPI_PROTOCOL       *mSpiProtocol;
/**
  PCI Enumeratuion is not done till later in DXE
  Initlialize SPI BAR0 to a default value till enumeration is done
  also enable memory space decoding for SPI

**/
VOID
InitSpiBar01 (
  VOID
  )
{
  UINTN                                 PchSpiBase;
  PchSpiBase = MmPciBase (
                 DEFAULT_PCI_BUS_NUMBER_PCH,
                 PCI_DEVICE_NUMBER_PCH_SPI,
                 PCI_FUNCTION_NUMBER_PCH_SPI
                 );
  MmioWrite32 (PchSpiBase + R_SPI_CFG_BAR0, PCH_SPI_BASE_ADDRESS);
  MmioOr32 (PchSpiBase + PCI_COMMAND_OFFSET, EFI_PCI_COMMAND_MEMORY_SPACE);
}
/**
  Detect whether the system is at EFI runtime or not

  @param  None

  @retval TRUE                  System is at EFI runtime
  @retval FALSE                 System is not at EFI runtime

**/
BOOLEAN
EFIAPI
AtRuntime (
  VOID
  )
{
  return FALSE;
}

/**
  Initialization routine for SpiAccessLib

  @param  None

  @retval EFI_SUCCESS           SpiAccessLib successfully initialized
  @return Others                SpiAccessLib initialization failed

**/
EFI_STATUS
EFIAPI
SpiAccessInit (
  VOID
  )
{
  EFI_STATUS                  Status;
  PEI_SPI_INSTANCE            *PeiSpiInstance;
  SPI_INSTANCE                *SpiInstance;
  EFI_PEI_PPI_DESCRIPTOR      *PpiDescriptor;

  DEBUG ((DEBUG_INFO, "PEI SpiAccessInit \n"));

  //
  // Original SPI PPI is running on flashrom. Reinsatll SPI PPI on memory to avoid updating PEI area will hang up issue
  //
  DEBUG ((DEBUG_INFO, "ReInstallPchSpi() Start\n"));

  //
  // PCI Enumeratuion is not done till later in DXE
  // Initlialize SPI BAR0 to a default value till enumeration is done
  // also enable memory space decoding for SPI
  //
  InitSpiBar01 ();

  PeiSpiInstance = (PEI_SPI_INSTANCE *) AllocateZeroPool (sizeof (PEI_SPI_INSTANCE));
  if (NULL == PeiSpiInstance) {
    return EFI_OUT_OF_RESOURCES;
  }

  SpiInstance = &(PeiSpiInstance->SpiInstance);
  SpiProtocolConstructor (SpiInstance);

  PeiSpiInstance->PpiDescriptor.Flags = EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST;
  PeiSpiInstance->PpiDescriptor.Guid  = &gPchSpiPpiGuid;
  PeiSpiInstance->PpiDescriptor.Ppi   = &(SpiInstance->SpiProtocol);

  ///
  /// ReInstall the SPI PPI
  ///
  Status = PeiServicesLocatePpi (
             &gPchSpiPpiGuid,
             0,
             &PpiDescriptor,
             NULL
             );
  //
  // Reinstall the Interface using the memory-based descriptor
  //
  if (!EFI_ERROR (Status)) {
    //
    // Only reinstall the "Stall" PPI
    //
    Status = PeiServicesReInstallPpi (
               PpiDescriptor,
               &PeiSpiInstance->PpiDescriptor
               );
  }
  DEBUG ((DEBUG_INFO, "SPI PPI ReInstalled\n"));
  DEBUG ((DEBUG_INFO, "ReInstallPchSpi() End\n"));
  //
  // Locate the SPI protocol.
  //
  Status = PeiServicesLocatePpi (
             &gPchSpiPpiGuid,
             0,
             NULL,
             (VOID **) &mSpiProtocol
             );
  ASSERT_EFI_ERROR (Status);

  return Status;
}
/**
  This routine uses to free the allocated resource by SpiAccessInit ().

  @retval EFI_SUCCESS    Free allocated resource successful.
  @return Others         Free allocated resource failed.
**/
EFI_STATUS
EFIAPI
SpiAccessDestroy (
  VOID
  )
{
  return EFI_SUCCESS;
}
