;;******************************************************************************
;;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;;*
;;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;;* transmit, broadcast, present, recite, release, license or otherwise exploit
;;* any part of this publication in any form, by any means, without the prior
;;* written permission of Insyde Software Corporation.
;;*
;;******************************************************************************
;;
;; Module Name:
;;
;;   CallSmmIhisiBiosGuard.asm
;;
;; Abstract:
;;
;;   32 bit Sent SMI to call IHISI flash ROM part
;;

  SECTION .text

%define IHISI_SIGNATURE 0x2448324F

;------------------------------------------------------------------------------
;  UINT8
;  BiosGuardCommunicate (
;    IN UINT8                 *Buffer,      // ebp+08h
;    IN UINT32                FlashSize,    // ebp+0ch
;    IN UINT32                FlashAddress  // ebp+10h
;    IN UINT16                SmiPort       // ebp+14h
;    );
;------------------------------------------------------------------------------
;BiosGuardCommunicate PROC          PUBLIC Buffer:DWORD, FlashSize:DWORD, FlashAddress:DWORD, SmiPort:WORD
global ASM_PFX(BiosGuardCommunicate)
ASM_PFX(BiosGuardCommunicate):
  push  ebp
  mov   ebp, esp
  push    ebx
  push    ecx
  push    edi
  push    esi

  mov     ebx, IHISI_SIGNATURE
  mov     ecx, [ebp+0x8]
  mov     ax,  0x41EF
  mov     dx, [ebp+0x14]
  out     dx,  al

  ;AL Fun ret state

  pop     esi
  pop     edi
  pop     ecx
  pop     ebx
  leave
  ret

;------------------------------------------------------------------------------
;  UINT8
;  BiosGuardWrite (
;    IN UINT8                 *Buffer,      // ebp+08h
;    IN UINT32                FlashSize,    // ebp+0ch
;    IN UINT32                FlashAddress  // ebp+10h
;    IN UINT16                SmiPort       // ebp+14h
;    );
;------------------------------------------------------------------------------
;BiosGuardWrite PROC      PUBLIC Buffer:DWORD, FlashSize:DWORD, FlashAddress:DWORD, SmiPort:WORD
global ASM_PFX(BiosGuardWrite)
ASM_PFX(BiosGuardWrite):
  push  ebp
  mov   ebp, esp
  push    ebx
  push    ecx
  push    edi
  push    esi

  mov     esi, [ebp+0x8]    ;Arg1 [ebp+08h] -> Write buffer to RSI
  mov     edi, [ebp+0xc] ;Arg2 [ebp+0ch] -> Write size to RDI
  mov     ecx, 0xEF
  mov     ebx, IHISI_SIGNATURE
  mov     ax,  0x42EF
  mov     dx, [ebp+0x14]
  out     dx,  al

  ;AL Fun ret state

  pop     esi
  pop     edi
  pop     ecx
  pop     ebx
  leave
  ret

