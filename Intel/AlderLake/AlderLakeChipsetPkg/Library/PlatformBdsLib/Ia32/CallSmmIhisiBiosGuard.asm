;;******************************************************************************
;;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;;*
;;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;;* transmit, broadcast, present, recite, release, license or otherwise exploit
;;* any part of this publication in any form, by any means, without the prior
;;* written permission of Insyde Software Corporation.
;;*
;;******************************************************************************
;;
;; Module Name:
;;
;;   CallSmmIhisiBiosGuard.asm
;;
;; Abstract:
;;
;;   32 bit Sent SMI to call IHISI flash ROM part
;;

TITLE   CallSmmIhisBiosGuard.asm

  .686
  .MODEL FLAT,C
  .CODE

IHISI_SIGNATURE EQU           02448324Fh

;------------------------------------------------------------------------------
;  UINT8
;  BiosGuardCommunicate (
;    IN UINT8                 *Buffer,      // ebp+08h
;    IN UINT32                FlashSize,    // ebp+0ch
;    IN UINT32                FlashAddress  // ebp+10h
;    IN UINT16                SmiPort       // ebp+14h
;    );
;------------------------------------------------------------------------------
;BiosGuardCommunicate PROC          PUBLIC Buffer:DWORD, FlashSize:DWORD, FlashAddress:DWORD, SmiPort:WORD
BiosGuardCommunicate PROC
  push    ebx
  push    ecx
  push    edi
  push    esi

  mov     ebx, IHISI_SIGNATURE
  mov     ecx, [ebp+08h]
  mov     ax,  41EFh
  mov     dx, [ebp+14h]
  out     dx,  al

  ;AL Fun ret state

  pop     esi
  pop     edi
  pop     ecx
  pop     ebx
  ret
BiosGuardCommunicate  ENDP

;------------------------------------------------------------------------------
;  UINT8
;  BiosGuardWrite (
;    IN UINT8                 *Buffer,      // ebp+08h
;    IN UINT32                FlashSize,    // ebp+0ch
;    IN UINT32                FlashAddress  // ebp+10h
;    IN UINT16                SmiPort       // ebp+14h
;    );
;------------------------------------------------------------------------------
;BiosGuardWrite PROC      PUBLIC Buffer:DWORD, FlashSize:DWORD, FlashAddress:DWORD, SmiPort:WORD
BiosGuardWrite PROC
  push    ebx
  push    ecx
  push    edi
  push    esi

  mov     esi, [ebp+08h]    ;Arg1 [ebp+08h] -> Write buffer to RSI
  mov     edi, [ebp+0ch] ;Arg2 [ebp+0ch] -> Write size to RDI
  mov     ecx, 0EFh
  mov     ebx, IHISI_SIGNATURE
  mov     ax,  42EFh
  mov     dx, [ebp+14h]
  out     dx,  al

  ;AL Fun ret state

  pop     esi
  pop     edi
  pop     ecx
  pop     ebx
  ret
BiosGuardWrite  ENDP

END
