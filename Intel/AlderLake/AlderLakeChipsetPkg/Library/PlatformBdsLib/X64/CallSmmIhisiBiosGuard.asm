;;******************************************************************************
;;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;;*
;;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;;* transmit, broadcast, present, recite, release, license or otherwise exploit
;;* any part of this publication in any form, by any means, without the prior
;;* written permission of Insyde Software Corporation.
;;*
;;******************************************************************************
;;
;; Module Name:
;;
;;   CallSmmIhisiBiosGuard.asm
;;
;; Abstract:
;;
;;   64 bit Sent SMI to call IHISI flash ROM part
;;

TITLE   CallSmmIhisBiosGuard.asm

text  SEGMENT

IHISI_SIGNATURE EQU           02448324Fh

;------------------------------------------------------------------------------
;  UINT8
;  BiosGuardCommunicate (
;    IN UINT8                 *Buffer,      // rcx
;    IN UINT32                FlashSize,    // rdx
;    IN UINT32                FlashAddress  // r8
;    IN UINT16                SmiPort       // r9
;    );
;------------------------------------------------------------------------------
BiosGuardCommunicate PROC          PUBLIC
  push    rbx
  push    rdi
  push    rsi
  push    r8

  mov     ebx, IHISI_SIGNATURE
  mov     ax,  41EFh
  mov     rdx, r9
  out     dx,  al

  ;AL Fun ret state

  pop     r8
  pop     rsi
  pop     rdi
  pop     rbx
  ret
BiosGuardCommunicate  ENDP

;------------------------------------------------------------------------------
;  UINT8
;  BiosGuardWrite (
;    IN UINT8                *Buffer,      // rcx
;    IN UINT32               FlashSize,    // rdx
;    IN UINT32               FlashAddress  // r8
;    IN UINT16               SmiPort       // r9
;    );
;------------------------------------------------------------------------------
BiosGuardWrite PROC      PUBLIC
  push    rbx
  push    rdi
  push    rsi
  push    r8

  mov     rsi, rcx    ;Arg1 rcx -> Write buffer to RSI
  mov     rdi, rdx    ;Arg2 rdx -> Write size to RDI
  mov     rcx, 0EFh
  mov     ebx, IHISI_SIGNATURE
  mov     ax,  42EFh
  mov     rdx, r9
  out     dx,  al

  ;AL Fun ret state

  pop     r8
  pop     rsi
  pop     rdi
  pop     rbx
  ret
BiosGuardWrite  ENDP

text  ENDS
END
