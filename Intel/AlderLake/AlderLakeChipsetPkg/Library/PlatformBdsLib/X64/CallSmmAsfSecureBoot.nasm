;;******************************************************************************
;;* Copyright (c) 2016, Insyde Software Corp. All Rights Reserved.
;;*
;;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;;* transmit, broadcast, present, recite, release, license or otherwise exploit
;;* any part of this publication in any form, by any means, without the prior
;;* written permission of Insyde Software Corporation.
;;*
;;******************************************************************************
;;
;; Module Name:
;;
;;   CallSmmAsfSecureBoot.nasm
;;
;; Abstract:
;;
;;   64 bit Sent SMI to call ASF_SECURE_BOOT_SMI
;;

SECTION .text 
%define H2O_SIGNATURE 0x2448324F

;------------------------------------------------------------------------------
;  UINT8
;  AsfSecureBootChangedSmiCall (
;    IN     UINT8                 Action,   // rcx
;    IN     UINT16                SmiPort   // rdx
;    );
;------------------------------------------------------------------------------
global ASM_PFX(AsfSecureBootChangedSmiCall)
ASM_PFX(AsfSecureBootChangedSmiCall):

  push    rbx
  push    rdi
  push    rsi
  push    r8

  mov     ebx, H2O_SIGNATURE
  mov     al,  cl
  mov     ah,  al
  mov     al,  0x56 ; ASF_SECURE_BOOT_SMI
  out     dx,  al

  ;AL Fun ret state
  pop     r8
  pop     rsi
  pop     rdi
  pop     rbx
  ret

