/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Support routines for ASF boot options in the BDS

@copyright
 Copyright (c) 2005 - 2015 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains a 'Sample Driver' and is licensed as such
 under the terms of your license agreement with Intel or your
 vendor. This file may be modified by the user, subject to
 the additional terms of the license agreement.

@par Specification Reference:
**/

#include <AsfSupport.h>
#include <UefiBootManagerLib.h>
#include <Library/DebugLib.h>
#include <Guid/AdmiSecureBoot.h>
//[-start-190604-IB11270236-remove]//
//#include <IndustryStandardAsf.h>
//[-end-190604-IB11270236-remove]//
#include <Register/UsbRegs.h>
//[-start-191225-IB16740000-add]// for PCI_FUNCTION_NUMBER_PCH_XHCI define
#include <PchBdfAssignment.h>
//[-end-191225-IB16740000-add]//
//[-start-190604-IB11270236-remove]//
//#include <AsfMsgs.h>
//#include <Protocol/AlertStandardFormat.h>
//[-end-190604-IB11270236-remove]//
//#include <Protocol/AsfProtocol.h>
//
//  Refer to ME BWG 5.4.1 Boot Audit Entry (BAE)PET Alert
//
ASF_MESSAGE AsfMessageBootOptionChange = {
    0x15, 0x10, 0x0F, 0x6F, 0x02, 0x68, 0x10, 0xFF, 0xFF, 0x22, 0x00, 0xAA, 0x13
};
ASF_MESSAGE AsfMessageBootOptionExpected = {
    0x16, 0x10, 0x0F, 0x6F, 0x02, 0x68, 0x08, 0xFF, 0xFF, 0x22, 0x00, 0x40, 0x13
};

//
// Global variables
//
//[-start-190604-IB11270236-modify]//
extern GLOBAL_REMOVE_IF_UNREFERENCED ASF_BOOT_OPTIONS      *mAsfBootOptions;
//[-end-190604-IB11270236-modify]//
GLOBAL_REMOVE_IF_UNREFERENCED EFI_GUID              gEfiSecureBootEnableDisableGuid = EFI_SECURE_BOOT_ENABLE_DISABLE;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_GUID              gAsfRestoreBootSettingsGuid     = RESTORE_SECURE_BOOT_GUID;

GLOBAL_REMOVE_IF_UNREFERENCED STORAGE_REDIRECTION_DEVICE_PATH mUsbrDevicePath = {
  gPciRootBridge,
  {
    {
      HARDWARE_DEVICE_PATH,
      HW_PCI_DP,
      {
        (UINT8)(sizeof (PCI_DEVICE_PATH)),
        (UINT8)((sizeof (PCI_DEVICE_PATH)) >> 8)
      }
    },
    PCI_FUNCTION_NUMBER_PCH_XHCI,
    PCI_DEVICE_NUMBER_PCH_XHCI
  },
  {
    {
      MESSAGING_DEVICE_PATH,
      MSG_USB_DP,
      {
        (UINT8)(sizeof (USB_DEVICE_PATH)),
        (UINT8)((sizeof (USB_DEVICE_PATH)) >> 8)
      }
    },
    0,
    0
  },
  gEndEntire
};

UINT8
AsfSecureBootChangedSmiCall (
    IN     UINT8                 Action,   // rcx
    IN     UINT16                SmiPort   // rdx
  );

/**
  Retrieve the ASF boot options previously recorded by the ASF driver.

  @param[in] None.

  @retval EFI_SUCCESS        Initialized Boot Options global variable and AMT protocol
**/
EFI_STATUS
BdsAsfInitialization (
  IN  VOID
  )
{
  EFI_STATUS                          Status;
//[-start-190604-IB11270236-modify]//
  ALERT_STANDARD_FORMAT_PROTOCOL      *Asf;
//[-end-190604-IB11270236-modify]//

  mAsfBootOptions = NULL;
  DEBUG((DEBUG_INFO,"BdsAsfInitialization Enter !!\n"));

  //
  // Get Protocol for ASF
  //
  Status = gBS->LocateProtocol (
                  &gAlertStandardFormatProtocolGuid,
                  NULL,
                  &Asf
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Info : Error getting ASF protocol -> %r\n", Status));
    return Status;
  }

//[-start-181221-IB11270223-modify]//
//  Status = Asf->GetBootOptions (Asf, &mAsfBootOptions);
  mAsfBootOptions = AllocateZeroPool (sizeof (ASF_BOOT_OPTIONS));
  if (mAsfBootOptions != NULL) {
    ///
    /// Get ASF Boot Options
    ///
    CopyMem (mAsfBootOptions, &Asf->AsfBootOptions, sizeof (ASF_BOOT_OPTIONS));
  }
//[-end-181221-IB11270223-modify]//
  if (EFI_ERROR(Status)) {
    DEBUG ((DEBUG_ERROR, "Info : Error getting ASF BootOptions -> %r\n", Status));
    return Status;
  }

  Status = ManageSecureBootState();

  DEBUG((DEBUG_INFO,"BdsAsfInitialization Done !!\n"));
  return Status;
}

/**
  This routine makes necessary Secure Boot & CSM state changes for Storage Redirection boot

  @param[in] None.

  @retval EFI_SUCCESS      Changes applied succesfully
**/
EFI_STATUS
ManageSecureBootState(
  IN VOID
  )
{
  EFI_STATUS Status;
  BOOLEAN    EnforceSecureBoot;
  UINT8      SecureBootState;
  UINT8      UsbrBoot;
  UINTN      VarSize;
  UINT32     VarAttributes;
  UINT8      RestoreBootSettings;
  UINT8      Data1;
  UINT8      Data2;

  DEBUG((DEBUG_INFO,"ManageSecureBootState Enter !!\n"));

  VarSize = sizeof(UINT8);

  //
  // Get boot parameters (Storage Redirection boot?, EnforceSecureBoot flag set?, secure boot enabled?)
  //
  EnforceSecureBoot = AsfIsEnforceSecureBootEnabled();
  UsbrBoot = AsfIsStorageRedirectionEnabled();

  Status = gRT->GetVariable (
                  EFI_SECURE_BOOT_ENFORCE_NAME,
                  &gEfiGenericVariableGuid,
                  &VarAttributes,
                  &VarSize,
                  &Data1
                  );

  Status = gRT->GetVariable (
                  EFI_SECURE_BOOT_MODE_NAME,
                  &gEfiGlobalVariableGuid,
                  NULL,
                  &VarSize,
                  &Data2
                  );

  SecureBootState = Data1 & Data2;

//  if (EFI_ERROR(Status)) {
//    return EFI_SUCCESS;
//  }

  //
  // Check whether we need to restore SecureBootEnable value changed in previous Storage Redirection boot
  //
  Status = gRT->GetVariable(
                  L"RestoreBootSettings",
                  &gAsfRestoreBootSettingsGuid,
                  NULL,
                  &VarSize,
                  &RestoreBootSettings
                  );

  if (Status == EFI_SUCCESS && RestoreBootSettings != RESTORE_SECURE_BOOT_NONE) {
    if (RestoreBootSettings == RESTORE_SECURE_BOOT_ENABLED && SecureBootState == SECURE_BOOT_DISABLED &&
      !(UsbrBoot && !EnforceSecureBoot)) {

      SecureBootState = SECURE_BOOT_ENABLED;

      AsfSecureBootChangedSmiCall(SecureBootState, PcdGet16(PcdSoftwareSmiPort));

      //
      // Delete RestoreBootSettings variable
      //
      Status = gRT->SetVariable(
                      L"RestoreBootSettings",
                      &gAsfRestoreBootSettingsGuid,
                      0,
                      0,
                      NULL
                      );
      ASSERT_EFI_ERROR (Status);

      DEBUG ((DEBUG_INFO, "Secure Boot settings restored after Storage Redirection boot - Cold Reset!\n"));
      gRT->ResetSystem (EfiResetCold, EFI_SUCCESS, 0, NULL);
    }
  }

  Status = EFI_SUCCESS;

  if (UsbrBoot) {
    if (SecureBootState == SECURE_BOOT_ENABLED && !EnforceSecureBoot) {
      //
      // Secure boot needs to be disabled if we're doing Storage Redirection and EnforceSecureBoot not set
      //
      SecureBootState     = SECURE_BOOT_DISABLED;
      RestoreBootSettings = RESTORE_SECURE_BOOT_ENABLED;

      AsfSecureBootChangedSmiCall(SecureBootState, PcdGet16(PcdSoftwareSmiPort));

      //
      // Set variable to restore previous secure boot state
      //
      Status = gRT->SetVariable(
                      L"RestoreBootSettings",
                      &gAsfRestoreBootSettingsGuid,
                      EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                      sizeof(UINT8),
                      &RestoreBootSettings
                      );
      ASSERT_EFI_ERROR (Status);

      DEBUG ((DEBUG_INFO, "Secure Boot disabled for USBr boot - Cold Reset!\n"));
      gRT->ResetSystem (EfiResetCold, EFI_SUCCESS, 0, NULL);
    }
  }
  DEBUG((DEBUG_INFO,"ManageSecureBootState Done !!\n"));
  return Status;
}

/**
  Compare two device paths up to a size of Boot Opion's Device Path

  @param[in] BootOptionDp     Device path acquired from BootXXXX EFI variable
  @param[in] FileSysDp    Device path acquired through EFI_SIMPLE_FILE_SYSTEM_PROTOCOL Handles buffer

  @retval TRUE   Both device paths point to the same device
  @retval FALSE   Device paths point to different devices
**/
BOOLEAN
CompareDevicePaths(
  IN  EFI_DEVICE_PATH_PROTOCOL *BootOptionDp,
  IN  EFI_DEVICE_PATH_PROTOCOL *FileSysDp
)
{
  UINTN BootOptionDpSize;
  UINTN FileSysDpSize;

  if (BootOptionDp == NULL || FileSysDp == NULL) {
    return FALSE;
  }

  BootOptionDpSize = GetDevicePathSize (BootOptionDp) - END_DEVICE_PATH_LENGTH;
  FileSysDpSize    = GetDevicePathSize (FileSysDp) - END_DEVICE_PATH_LENGTH;

  if ((BootOptionDpSize <= FileSysDpSize) && (CompareMem (FileSysDp, BootOptionDp, BootOptionDpSize) == 0)) {
    return TRUE;
  }

  return FALSE;
}

/**
  Get EFI device path through EFI_SIMPLE_FILE_SYSTEM_PROTOCOL Handles buffer. Acquired path must
  point to the same device as argument DevicePath passed to the function.

  @param[in] DevicePath   Device path acquired from BootXXXX EFI variable

  @retval EFI_DEVICE_PATH_PROTOCOL   Device path for booting
**/
EFI_DEVICE_PATH_PROTOCOL *
GetFullBootDevicePath(
  IN EFI_DEVICE_PATH_PROTOCOL *DevicePath
)
{
  EFI_STATUS               Status;
  EFI_DEVICE_PATH_PROTOCOL *TempDevicePath;
  EFI_DEVICE_PATH_PROTOCOL *ReturnDevicePath;
  UINTN                    HandleNum;
  EFI_HANDLE               *HandleBuf;
  UINTN                    Index;

  DEBUG((DEBUG_INFO,"GetFullBootDevicePath Enter !!\n"));
  ReturnDevicePath = NULL;

  if (IS_USB_SHORT_FORM_DEVICE_PATH (DevicePath)) {
    return BdsLibExpandUsbShortFormDevPath (DevicePath);
  }

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiSimpleFileSystemProtocolGuid,
                  NULL,
                  &HandleNum,
                  &HandleBuf
                  );
  if ((EFI_ERROR (Status)) || (HandleBuf == NULL)) {
    return NULL;
  }

  for (Index = 0; Index < HandleNum; Index++) {
    TempDevicePath     = DevicePathFromHandle (HandleBuf[Index]);

    if (CompareDevicePaths(DevicePath, TempDevicePath)) {
      ReturnDevicePath = DuplicateDevicePath(TempDevicePath);
      break;
    }
  }

  DEBUG((DEBUG_INFO,"GetFullBootDevicePath Done !!\n"));
  return ReturnDevicePath;
}

/*++
  Translate ASF request type to BBS or EFI device path type

  @param[in] DeviceType     - ASF request type
  @param[in]  Efi           - Set to TRUE if DeviceType is to be translated
                              to EFI device path type; FALSE if BBS type
  @retval UINTN Translated device type
--*/
UINTN
GetBootDeviceType (
  IN UINTN    DeviceType,
  IN BOOLEAN  Efi
  )
{
  UINTN Type = 0;

  switch (DeviceType) {
    case FORCE_PXE:
      if (Efi) {
        Type = MSG_MAC_ADDR_DP;
      } else {
        Type = BBS_BEV_DEVICE;
      }
      break;
    case FORCE_HARDDRIVE:
    case FORCE_SAFEMODE:
      if (Efi) {
        Type = MEDIA_HARDDRIVE_DP;
      } else {
        Type = BBS_TYPE_HARDDRIVE;
      }
      break;
    case FORCE_DIAGNOSTICS:
      if (Efi) {
        Type = MEDIA_PIWG_FW_FILE_DP;
      }
      break;
    case FORCE_CDDVD:
      if (Efi) {
        Type = MEDIA_CDROM_DP;
      } else {
        Type = BBS_TYPE_CDROM;
      }
      break;
    default:
      break;
  }

  return Type;
}

/**
  Update the BBS table with our required boot device

  @param[in] DeviceIndex   Boot device whose device index
  @param[in] DevType     Boot device whose device type
  @param[in] BbsCount    Number of BBS_TABLE structures
  @param[in] BbsTable    BBS entry
  @param[in] UsbrBoot    set to TRUE if this is USBR boot

  @retval EFI_SUCCESS   BBS table successfully updated
**/
EFI_STATUS
ProcessBbsTable (
  IN UINT16     BbsIndex,
  OUT BOOLEAN   *IsUsbr
  )
{
  EFI_LEGACY_BIOS_PROTOCOL  *LegacyBios;
  BBS_TABLE                 *BbsTable;
  UINT16                    HddCount;
  UINT16                    BbsCount;
  HDD_INFO                  *LocalHddInfo;
  EFI_STATUS                Status;
  UINTN                     Index;
  EFI_DEVICE_PATH_PROTOCOL  *TempDevicePath;

  DEBUG((DEBUG_INFO,"ProcessBbsTable Enter !!\n"));
  HddCount      = 0;
  BbsCount      = 0;
  LocalHddInfo  = NULL;

  //
  // Make sure the Legacy Boot Protocol is available
  //
  Status = gBS->LocateProtocol (&gEfiLegacyBiosProtocolGuid, NULL, &LegacyBios);
  if (LegacyBios == NULL) {
    return Status;
  }

  //
  // Get BBS table instance
  //
  Status = LegacyBios->GetBbsInfo (
                        LegacyBios,
                        &HddCount,
                        &LocalHddInfo,
                        &BbsCount,
                        &BbsTable
                        );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  DEBUG_CODE(PrintBbsTable (BbsTable, BbsCount););

  if (BbsIndex > BbsCount) {
    return EFI_ABORTED;
  }

  //
  // Check if called only to determine whether this is Storage Redirection legacy option
  //
  if (IsUsbr != NULL) {
//    UsbrHandle     = (EFI_HANDLE)((UINTN)BbsTable[BbsIndex].IBV1 | ((UINTN)BbsTable[BbsIndex].IBV2 << 32));
//    TempDevicePath = DevicePathFromHandle (UsbrHandle);
//    *IsUsbr = CompareDevicePaths((EFI_DEVICE_PATH_PROTOCOL*)&mUsbrDevicePath, TempDevicePath);
    for (Index = 0; Index < BbsCount; Index++) {
      if ((BbsTable[Index].BootPriority == BBS_IGNORE_ENTRY) ||
          (BbsTable[Index].BootPriority == BBS_DO_NOT_BOOT_FROM) ||
          (BbsTable[Index].BootPriority == BBS_LOWEST_PRIORITY)
          ) {
        continue;
      }
      TempDevicePath = (EFI_DEVICE_PATH_PROTOCOL *) (UINTN)BbsTable[Index].IBV2;
      *IsUsbr = CompareDevicePaths((EFI_DEVICE_PATH_PROTOCOL*)&mUsbrDevicePath, TempDevicePath);
      if (*IsUsbr) {
        break;
      }
    }
    return EFI_SUCCESS;
  }

  //
  // First set all disks priority to BBS_DO_NOT_BOOT_FROM
  //
  for (Index = 0; Index < BbsCount; Index++) {
    if (BbsTable[Index].BootPriority == BBS_IGNORE_ENTRY) {
      continue;
    }
    BbsTable[Index].BootPriority = BBS_DO_NOT_BOOT_FROM;
  }

  BbsTable[BbsIndex].BootPriority = 0;

  DEBUG_CODE(PrintBbsTable (BbsTable, BbsCount););

  DEBUG((DEBUG_INFO,"ProcessBbsTable Done !!\n"));
  return Status;
}

UINTN
GetBootOptionIndex (
  IN EFI_BOOT_MANAGER_LOAD_OPTION *BootOptions,
  IN UINTN                        OptionCount,
  IN UINT16                       DeviceType,
  IN UINT16                       DeviceIndex,
  IN BOOLEAN                      UsbrBoot,
  IN BOOLEAN                      EfiBoot
  )
/*++

Routine Description:

  Build the Device Path for this boot selection

Arguments:

  DeviceType  - Boot device whose device type
  DeviceIndex - Boot device whose device index
  UsbrBoot    - If UsbrBoot is TRUE then check USBr device
  EfiBoot     - Set to TRUE if this is EFI boot

Returns:

  Device path for booting.

--*/
{
  EFI_DEVICE_PATH_PROTOCOL     *FullDevicePath;
  UINTN                        Index;
  UINTN                        TempIndex;
  EFI_DEVICE_PATH_PROTOCOL     *DevPathNode;
  UINTN                        EfiDeviceType;
  UINTN                        LegacyDeviceType;
  BOOLEAN                      TypeMatched;
  BOOLEAN                      TypeMatchedHdd;
  INTN                         BootDeviceIndex;
  UINTN                        EfiNodeType;

  DEBUG((DEBUG_INFO,"GetBootOptionIndex Enter !!\n"));

  FullDevicePath   = NULL;
  TempIndex        = 1;
  EfiDeviceType    = GetBootDeviceType(DeviceType, TRUE);
  LegacyDeviceType = GetBootDeviceType(DeviceType, FALSE);
  TypeMatched      = FALSE;
  TypeMatchedHdd   = FALSE;
  BootDeviceIndex  = -1;
  EfiNodeType      = (EfiDeviceType == MSG_MAC_ADDR_DP) ? MESSAGING_DEVICE_PATH : MEDIA_DEVICE_PATH;

//[-start-181221-IB11270223-modify]//
  mUsbrDevicePath.Usbr.InterfaceNumber = AsfGetStorageRedirectionBootDevice ();
//            (mAsfBootOptions->SpecialCommandParam >> USBR_BOOT_DEVICE_SHIFT) & USBR_BOOT_DEVICE_MASK;

//[-end-181221-IB11270223-modify]//
  mUsbrDevicePath.Usbr.ParentPortNumber = GetPchUsbrStoragePortNum ();

  for (Index = 0; Index < OptionCount; Index++) {
    if (DevicePathType(BootOptions[Index].FilePath) == BBS_DEVICE_PATH && DevicePathSubType(BootOptions[Index].FilePath) == BBS_BBS_DP) {
      FullDevicePath = DuplicateDevicePath(BootOptions[Index].FilePath);
    } else {
      //
      // If this is EFI boot option, we need to get full device path from EFI_SIMPLE_FILE_SYSTEM_PROTOCOL
      // to determine type of device
      //
      FullDevicePath = GetFullBootDevicePath(BootOptions[Index].FilePath);
      if (FullDevicePath == NULL) {
        //continue;
        FullDevicePath = DuplicateDevicePath(BootOptions[Index].FilePath);
      }
    }

    DevPathNode = FullDevicePath;
    if (DevPathNode == NULL) {
      continue;
    }

    //
    // Check if this is our requested boot device
    //
    while (!IsDevicePathEnd(DevPathNode)) {
      if (UsbrBoot) {
        if (EfiBoot) {
          if (CompareDevicePaths((EFI_DEVICE_PATH_PROTOCOL*)&mUsbrDevicePath, FullDevicePath)) {
            TypeMatched = TRUE;
          }
        } else {
          //
          // USBR legacy boot, lookup BBS table to check if this is Storage Redirection boot option
          //
          ProcessBbsTable((UINT16)*BootOptions[Index].OptionalData, &TypeMatched);
        }
      } else {
        if (DevicePathType(DevPathNode) == BBS_DEVICE_PATH && DevicePathSubType(DevPathNode) == BBS_BBS_DP) {
          //
          // Legacy boot option
          //
          if (((BBS_BBS_DEVICE_PATH *)DevPathNode)->DeviceType == LegacyDeviceType) {
            TypeMatched = TRUE;
          }
        } else {
          //
          // EFI boot option
          //
          if (DevicePathType(DevPathNode) == EfiNodeType && DevicePathSubType(DevPathNode) == EfiDeviceType) {
            if (DeviceType == FORCE_DIAGNOSTICS) {
              //
              // If boot to EFI shell, find shell file by GUID
              //
              if (CompareMem(&((MEDIA_FW_VOL_FILEPATH_DEVICE_PATH*)DevPathNode)->FvFileName, PcdGetPtr(PcdShellFile), sizeof(EFI_GUID))) {
                TypeMatched = TRUE;
              }
            } else {
              TypeMatched = TRUE;
            }
          }
          if (DevicePathType (DevPathNode) == MEDIA_DEVICE_PATH && DevicePathSubType (DevPathNode) == MEDIA_FILEPATH_DP) {
            TypeMatchedHdd = TRUE;
          }
        }
      }

//[-start-190612-IB16990051-remove]//
//	  DevPathNode = NextDevicePathNode (DevPathNode);
//[-end-190612-IB16990051-remove]//

      if (TypeMatched) {
        //
        // Type matched, check for device index
        //
        if (!UsbrBoot && TempIndex < DeviceIndex) {
          TempIndex++;
          TypeMatched = FALSE;
          break;
        }

        if ((DeviceType == FORCE_HARDDRIVE) && (!TypeMatchedHdd)) {
          continue;
        }

        BootDeviceIndex = Index;
        //
        // Refresh BBS table if legacy option
        //
        if (DevicePathType(FullDevicePath) == BBS_DEVICE_PATH && DevicePathSubType(FullDevicePath) == BBS_BBS_DP) {
          ProcessBbsTable((UINT16)*BootOptions[Index].OptionalData, NULL);
        }
        break;
      }

    }

    if (FullDevicePath != NULL) {
      FreePool(FullDevicePath);
      FullDevicePath = NULL;
    }

    if (BootDeviceIndex != -1) {
      break;
    }

    TypeMatched = FALSE;

  }
  DEBUG((DEBUG_INFO,"GetBootOptionIndex Done !!\n"));
  return BootDeviceIndex;
}

/**
  Found out ASF boot options.

  @param[in] EfiBoot      Set to TRUE if this is EFI boot

  @retval EFI_DEVICE_PATH_PROTOCOL     Device path for booting.
**/
INTN
GetAsfBootOptionIndex (
  IN EFI_BOOT_MANAGER_LOAD_OPTION *BootOptions,
  IN UINT8                        DeviceType,
  IN UINTN                        OptionCount,
  IN BOOLEAN                      EfiBoot
  )
{
  INTN BootOptionIndex = -1;

  //
  // First we check ASF boot options Special Command
  //
  switch (DeviceType) {
  //
  // The Special Command Parameter can be used to specify a PXE
  // parameter. When the parameter value is 0, the system default PXE device is booted. All
  // other values for the PXE parameter are reserved for future definition by this specification.
  //
  case FORCE_PXE:
  case FORCE_DIAGNOSTICS:
//[-start-190604-IB11270236-modify]//
    if ((mAsfBootOptions->SpecialCommandParamHighByte != 0) || (mAsfBootOptions->SpecialCommandParamLowByte != 0)) {
//[-end-190604-IB11270236-modify]//
      //
      // ASF spec says 0 currently is the only option
      //
      break;
    }

    //
    // We want everything connected up for PXE or EFI shell
    //
    ConnectAllDriversToAllControllers ();
//[-start-190604-IB11270236-modify]//
     BootOptionIndex = GetBootOptionIndex(BootOptions, OptionCount, DeviceType, mAsfBootOptions->SpecialCommandParamHighByte, FALSE, EfiBoot);
//[-end-190604-IB11270236-modify]//
    break;

  //
  // The Special Command Parameter identifies the boot-media index for
  // the managed client. When the parameter value is 0, the default hard-drive/optical drive is booted, when the
  // parameter value is 1, the primary hard-drive/optical drive is booted; when the value is 2, the secondary
  // hard-drive/optical drive is booted and so on.
  //
  case FORCE_HARDDRIVE:
  case FORCE_SAFEMODE:
  case FORCE_CDDVD:
//[-start-190604-IB11270236-modify]//
    BootOptionIndex = GetBootOptionIndex(BootOptions, OptionCount, DeviceType, mAsfBootOptions->SpecialCommandParamHighByte, FALSE, EfiBoot);
//[-end-190604-IB11270236-modify]//
    break;

  //
  // No additional special command is included; the Special Command Parameter has no
  // meaning.
  //
  case NOP:
  default:
    break;
  }

  return BootOptionIndex;
}

/**
  Check Storage Redirection boot device and Asf boot device

  @param[in] EfiBoot      Set to TRUE if this is EFI boot

  @retval EFI_DEVICE_PATH_PROTOCOL     Device path for booting.
**/
INTN
GetForcedBootOptionIndex (
  IN EFI_BOOT_MANAGER_LOAD_OPTION *BootOptions,
  IN UINTN                        OptionCount,
  IN BOOLEAN                      EfiBoot
  )
{
  INTN BootOptionIndex = -1;

  //
  // OEM command values; the interpretation of the Special Command and associated Special
  // Command Parameters is defined by the entity associated with the Enterprise ID.
  //
  if (AsfIsStorageRedirectionEnabled ()) {
    BootOptionIndex = GetBootOptionIndex ( BootOptions, OptionCount, FORCE_CDDVD, 0, TRUE, EfiBoot );
  } else if (mAsfBootOptions->IanaId != ASF_INDUSTRY_CONVERTED_IANA) {
    BootOptionIndex = GetAsfBootOptionIndex (
                          BootOptions,
                          mAsfBootOptions->SpecialCommand,
                          OptionCount,
                          EfiBoot
                          );
  }

  return BootOptionIndex;
}

/**
  Process ASF boot options and if available, attempt the boot

  @param[in] None.

  @retval EFI_SUCCESS    The command completed successfully
**/
EFI_STATUS
BdsBootViaAsf (
  IN  VOID
  )
{
  EFI_STATUS                   Status;
  EFI_BOOT_MANAGER_LOAD_OPTION *BootOptions;
  UINTN                        OptionCount;
  INTN                         BootOptionIndex;
  BOOLEAN                      EfiBoot;
  EFI_LEGACY_BIOS_PROTOCOL     *LegacyBios;

  DEBUG((DEBUG_INFO,"BdsBootViaAsf Enter !!\n"));
  Status          = EFI_SUCCESS;
  BootOptions     = NULL;
  EfiBoot         = FALSE;

  //
  // Check if this is legacy or efi boot
  //
  Status = gBS->LocateProtocol (&gEfiLegacyBiosProtocolGuid, NULL, &LegacyBios);
  if (LegacyBios == NULL) {
    EfiBoot = TRUE;
  }

  //
  // Check if ASF Boot Options is present.
  //
  if (mAsfBootOptions->SubCommand != ASF_BOOT_OPTIONS_PRESENT) {
    DEBUG((DEBUG_INFO,"ASF Boot Options is not present !!\n"));
    return EFI_NOT_FOUND;
  }

  BootOptions = EfiBootManagerGetLoadOptions(&OptionCount, LoadOptionTypeBoot);
  if (BootOptions == NULL) {
    return EFI_UNSUPPORTED;
  }

  BootOptionIndex = GetForcedBootOptionIndex (BootOptions, OptionCount, EfiBoot);
  //
  // If device path was set, the we have a boot option to use
  //
  if (BootOptionIndex == -1) {
    return EFI_UNSUPPORTED;
  }

 EfiBootManagerBoot (&BootOptions[BootOptionIndex]);

//  //
//  // If this is RCO/Storage Redirection EFI Boot, keep trying unless user cancels
//  //
//  while (!(Key.ScanCode == SCAN_ESC && Key.UnicodeChar == 0)) {
//    EfiBootManagerBoot (&BootOptions[BootOptionIndex]);
//    //
//    // Returning from EfiBootManagerBoot means the boot failed
//    // Display message to user before attempting another RCO/Storage Redirection boot
//    //
//    gST->ConOut->ClearScreen (gST->ConOut);
//    gST->ConOut->OutputString (
//                  gST->ConOut,
//                  L"RCO/USBR boot failed. Press ENTER to try again or ESC to return to regular boot\r\n"
//                  );
//    Key.ScanCode    = 0;
//    Key.UnicodeChar = 0;
//    OldTpl = gBS->RaiseTPL (TPL_HIGH_LEVEL);
//    gBS->RestoreTPL (TPL_APPLICATION);
//    while (!(Key.ScanCode == 0 && Key.UnicodeChar == L'\r')) {
//      gBS->WaitForEvent (1, &(gST->ConIn->WaitForKey), &EventIndex);
//      gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);
//
//      if (Key.ScanCode == SCAN_ESC && Key.UnicodeChar == 0) {
//        break;
//      }
//    }
//
//    if (OldTpl > TPL_APPLICATION) {
//      gBS->RaiseTPL (OldTpl);
//    }
//  }

  EfiBootManagerFreeLoadOptions (BootOptions, OptionCount);
  DEBUG((DEBUG_INFO,"BdsBootViaAsf Done !!\n"));
  return Status;
}

BOOLEAN
BdsCheckAsfBootCmd (
  IN  VOID
  )
{
  if (mAsfBootOptions->SpecialCommand == NOP) {
    return FALSE;
  }

  return TRUE;
}

/**
    Sync CR confing setting(GlobalTerminalType) to SOL Terminal type.

    Below is SCU GlobalTerminalType setting value.
    option text = STRING_TOKEN(STR_CR_TERMINAL_TYPE_VT100),  value = 0, flags = INTERACTIVE | DEFAULT;
    option text = STRING_TOKEN(STR_CR_TERMINAL_TYPE_VT100P), value = 1, flags = INTERACTIVE;
    option text = STRING_TOKEN(STR_CR_TERMINAL_TYPE_VTUTF8), value = 2, flags = INTERACTIVE;
    option text = STRING_TOKEN(STR_CR_TERMINAL_TYPE_ANSI),   value = 3, flags = INTERACTIVE;
    option text = STRING_TOKEN(STR_CR_TERMINAL_TYPE_LOG),    value = 4, flags = INTERACTIVE;
**/

VOID
UpdateSolTerminalTypeByCrConfig(
  IN PLATFORM_ISA_SERIAL_OVER_LAN_DEVICE_PATH *FullDevicePath
  ) 
{
  UINTN                CrConfigSize;
  CR_CONFIGURATION     *CrConfig;

  if (!FeaturePcdGet (PcdAmtEnable)) {
    return;
  }

  if (FullDevicePath == NULL) {
    return;
  }
  DEBUG((DEBUG_INFO,"Update Sol Terminal Type By CrConfig!!\n"));
  CrConfig     = NULL;
  CrConfigSize = 0;

  CrConfig = BdsLibGetVariableAndSize (
                  L"CrConfig",
                  &gH2OCrConfigurationGuid,
                  &CrConfigSize
                  );

  if ((CrConfig == NULL) || (CrConfigSize == 0)) {
    DEBUG((DEBUG_INFO,"Get CrConfig error \n"));
    return;
  }

  DEBUG((DEBUG_INFO,"CrConfig GlobalTerminalType is %d\n",CrConfig->GlobalTerminalType));

  switch (CrConfig->GlobalTerminalType) {
    case 0: //STR_CR_TERMINAL_TYPE_VT100
      CopyMem (&FullDevicePath->TerminalType.Guid, &gEfiVT100Guid, sizeof (EFI_GUID));
      break;

    case 1: //STR_CR_TERMINAL_TYPE_VT100P
      CopyMem (&FullDevicePath->TerminalType.Guid, &gEfiVT100PlusGuid, sizeof (EFI_GUID));
      break;

    case 2: //STR_CR_TERMINAL_TYPE_VTUTF8
      CopyMem (&FullDevicePath->TerminalType.Guid, &gEfiVTUTF8Guid, sizeof (EFI_GUID));
      break;

    case 3: //STR_CR_TERMINAL_TYPE_ANSI
      CopyMem (&FullDevicePath->TerminalType.Guid, &gEfiPcAnsiGuid, sizeof (EFI_GUID));
      break;

    case 4: //STR_CR_TERMINAL_TYPE_LOG
      CopyMem (&FullDevicePath->TerminalType.Guid, &gH2OCrLogTermGuid, sizeof (EFI_GUID));
      break;

    default:  
      break;
  }
}
