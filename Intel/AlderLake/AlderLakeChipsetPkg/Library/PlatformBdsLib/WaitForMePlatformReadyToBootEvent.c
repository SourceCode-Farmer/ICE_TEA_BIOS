/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
**/
#include "WaitForMePlatformReadyToBootEvent.h"
//[-start-180412-IB11270199-add]//
#include <MeBiosPayloadHob.h>
#include <Library/HobLib.h>
//[-end-180412-IB11270199-add]//
#include <AmtConfig.h>

extern EFI_GUID gMePlatformReadyToBootGuid;

VOID
EFIAPI
WaitForMePlatformReadyToBootEventCallback (
  IN EFI_EVENT  Event,
  IN VOID       *Context
  )
{
  DEBUG ((DEBUG_INFO, "WaitForMePlatformReadyToBootEventCallback start.\n"));

  gBS->CloseEvent (Event);

  DEBUG ((DEBUG_INFO, "WaitForMePlatformReadyToBootEventCallback end. Performing a Warm Reset.\n"));

  //
  // WA: Remove Reset until Intel AMT feature ready
  //
//  gRT->ResetSystem (EfiResetWarm, EFI_SUCCESS, 0, NULL);
}

/*
  Create a Event for SOL device
*/
VOID
CreateWaitForMePlatformReadyToBootEvent (
  VOID
)
{
  EFI_EVENT                   WaitForMePlatformReadyToBootEvent;
  EFI_STATUS                  Status;
  EFI_HOB_GUID_TYPE           *GuidHob;
  ME_BIOS_PAYLOAD_HOB         *MbpHob;
  AMT_PEI_CONFIG              *AmtPeiConfig;

  GuidHob       = NULL;
  MbpHob        = NULL;
  AmtPeiConfig  = NULL;

  //
  // Check AMT when Amt Enabled
  // FSP will build AmtPeiConfig and Hob(gAmtPolicyHobGuid)
  //
  GuidHob = GetFirstGuidHob (&gAmtPolicyHobGuid);
  if (GuidHob == NULL) {
    //
    // AMT supported in FSP not ready
    // Need to check or rebuild FSP
    //
    DEBUG((DEBUG_ERROR, "!!! AMT supported in FSP not ready !!!\n"));
    ASSERT_EFI_ERROR (GuidHob != NULL);
    return;
  }

  //
  // Check AMT policy
  //
  AmtPeiConfig = (AMT_PEI_CONFIG *) GET_GUID_HOB_DATA (GuidHob);

  if ((BOOLEAN) AmtPeiConfig->AmtEnabled == FALSE){
    DEBUG((DEBUG_INFO, "AMT not Enabled\n"));
    return;
  }

  //
  // For SOL device
  //
  if (AmtPeiConfig->AmtSolEnabled == FALSE) {
    DEBUG((DEBUG_INFO, "Serial-over-Lan is disabled\n"));
    return;
  }

  //
  // Check ME FW Type
  //
  MbpHob = (ME_BIOS_PAYLOAD_HOB *) GetFirstGuidHob (&gMeBiosPayloadHobGuid);
  if (MbpHob == NULL) {
    //
    // MbpHob == NULL, ME error
    //
    DEBUG((DEBUG_WARN, "ME error\n"));
    return;
  } else if (MbpHob->MeBiosPayload.FwPlatType.RuleData.Fields.IntelMeFwImageType != IntelMeCorporateFw) {
    //
    // ME FW Type error
    //
    DEBUG((DEBUG_INFO, "ME type not Corporate\n"));
    return;
  }

  DEBUG ((DEBUG_INFO, "\nCreate WaitForMePlatformReadyToBootEvent.\n"));
  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  WaitForMePlatformReadyToBootEventCallback,
                  NULL,
                  &gMePlatformReadyToBootGuid,
                  &WaitForMePlatformReadyToBootEvent
                  );

  ASSERT_EFI_ERROR (Status);
}
