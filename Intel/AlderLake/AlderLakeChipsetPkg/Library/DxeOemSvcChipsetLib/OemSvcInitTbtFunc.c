/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeOemSvcChipsetLib.h>

/**
 This function offers an interface to initial Thunderbolt Setup data in SetupRuntimeUpdateEveryBoot.

 @param[in]         *SetupData
 @param[in]         *RcSetupData
 @param[in]         *SaSetup
 @param[in]         *PchSetup
 
 @retval            EFI_UNSUPPORTED      Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED    Alter the Configuration Parameter.
 @retval            EFI_SUCCESS          The function performs the same operation as caller.
                                         The caller will skip the specified behavior and assuming
                                         that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcInitTbtFunc (
  IN CHIPSET_CONFIGURATION            *SetupData,
  IN SETUP_DATA                       *RcSetupData,
  IN SA_SETUP                         *SaSetup,
  IN PCH_SETUP                        *PchSetup
  )
{
  /*++
    Todo:
      Add project specific code in here.
  --*/

  return EFI_UNSUPPORTED;
}
