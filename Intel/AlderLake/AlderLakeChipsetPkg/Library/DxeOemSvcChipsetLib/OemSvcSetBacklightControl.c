/** @file
    This function provides an interface to update IGD OpRegion content.
;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeOemSvcChipsetLib.h>
#if defined(C770_SUPPORT) || defined(C970_SUPPORT)
//[start-220310-STORM1129-modify]
#include <Library/GpioLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <Library/TimerLib.h>
//[end-220310-STORM1129-modify]
#endif

/**

 This function provides an interface to update backlight brightness value.

 @retval            EFI_UNSUPPORTED      Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED    Alter the Configuration Parameter.
 @retval            EFI_SUCCESS          The function performs the same operation as caller.
                                         The caller will skip the specified behavior and assuming
                                         that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcSetBacklightControl(
  VOID
  ) 
{
/*++
  Todo:
  Add project specific code in here.
--*/
#if 0
  EFI_STATUS                      Status;
  GOP_DISPLAY_BRIGHTNESS_PROTOCOL *GopDisplayBrightness;
  EFI_IA32_REGISTER_SET           Reg;
  EFI_LEGACY_BIOS_PROTOCOL        *LegacyBios;
  UINT8                           BrightnessValue;

  GopDisplayBrightness = NULL;
  ZeroMem (&Reg, sizeof (EFI_IA32_REGISTER_SET));
  LegacyBios           = NULL;
  BrightnessValue      = 0x10;
 
  Status = gBS->LocateProtocol (&gEfiLegacyBiosProtocolGuid, NULL, (VOID **)&LegacyBios);
  if (EFI_ERROR (Status)) {
    //
    // Modify Backlight of GOP Setting
    //
    Status = gBS->LocateProtocol (&gGopDisplayBrightnessProtocoGuid, NULL, (VOID **)&GopDisplayBrightness);
    if (EFI_ERROR (Status)) {
       return Status;
    }
    GopDisplayBrightness->SetBrightnessLevel (GopDisplayBrightness, BrightnessValue);
  } else {
    //
    // Modify Backlight of VBIOS Setting
    //
    //
    // VBIOS : Hook to sets the backlight brightness level
    //
    //  Calling Registers:
    //    AX = 5F61h, Local Flat Panel Support function
    //    BH = 08h, Set Backlight Control sub-function
    //    BL = Brightness Level Setting:
    //       = 0 to 255 Range (0 = off, 255 = Full Brightness)
    //  Return Registers: 
    //    AX = Return Status (function not supported if AL != 5Fh): 
    //       = 005Fh, Function supported and successful 
    //       = 015Fh, Function supported but failed 
    //
    Reg.X.AX |= 0x5F61;
    Reg.X.BX &= ~(0xffff);
    Reg.X.BX |= (0x0800 | BrightnessValue);
    Status = LegacyBios->Int86(LegacyBios, 0x10, &Reg);
  }
  return Status;
#endif
#if defined(C770_SUPPORT) || defined(C970_SUPPORT)
//[start-220310-STORM1129-modify]
  GpioSetOutputValue (GPIO_VER2_LP_GPP_D15, 0);  //Set Touch Panel Stop pin Off
  MicroSecondDelay(130000);                      //Delay130ms
  GpioSetOutputValue (GPIO_VER2_LP_GPP_D15, 1);  //Set Touch Panel Stop pin On
//[end-220310-STORM1129-modify]
#endif
  return EFI_UNSUPPORTED;
}

