/** @file
  This function provides an interface to update GlobalNvs table content.
;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Library/DxeOemSvcChipsetLib.h>
/**
 This function provides an interface to update GlobalNvs table content.

 @param[in, out]    mPlatformNvsArea    On entry, points to PLATFORM_NVS_AREA instance.
                                        On exit , points to updated PLATFORM_NVS_AREA instance.
 @param[in, out]    mOemPlatformNvsArea On entry, points to OEM_PLATFORM_NVS_AREA instance.
                                        On exit , points to updated OEM_PLATFORM_NVS_AREA instance.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdatePlatformNvs (
  PLATFORM_NVS_AREA                     *mPlatformNvsArea,
  OEM_PLATFORM_NVS_AREA                 *mOemPlatformNvsArea  
  ) {
/*++
  Todo:
  Add project specific code in here.
--*/

  return EFI_UNSUPPORTED;
}
