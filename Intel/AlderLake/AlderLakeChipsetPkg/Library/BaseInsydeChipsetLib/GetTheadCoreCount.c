/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <CpuRegs.h>
//[-start-200131-IB16740000-add]//
// for build error in RC_1033 (MSR_CORE_THREAD_COUNT: undeclared identifier)
#include <Register/CommonMsr.h>
//[-end-200131-IB16740000-add]//


/**
 get cpu's thread ,Core enable  count

 @param [in, out] ThreadCount   thread count number
 @param [in, out] CoreCount     core count number

 @retval EFI_SUCCESS            The function completed successfully.
 @retval EFI_INVALID_PARAMETER  should input one and not null parmeter

**/
EFI_STATUS
GetTheadCoreCount (
  IN OUT      UINT8   *ThreadCount, OPTIONAL
  IN OUT      UINT8   *CoreCount    OPTIONAL
  )
{
  if (CoreCount != NULL) {
     *CoreCount = (UINT8)(RShiftU64 (AsmReadMsr64 (MSR_CORE_THREAD_COUNT), 16) & 0xFFFF);
  }
  if (ThreadCount != NULL) {
    *ThreadCount = (UINT8)(AsmReadMsr64 (MSR_CORE_THREAD_COUNT) & 0xFFFF);
  }

  if ((CoreCount == NULL) && (ThreadCount == NULL)) {
    return EFI_INVALID_PARAMETER;
  }
  return EFI_SUCCESS;
}


