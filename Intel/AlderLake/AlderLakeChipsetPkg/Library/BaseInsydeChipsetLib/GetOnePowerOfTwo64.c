/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Uefi.h>
#include <Library/BaseLib.h>


UINT64
EFIAPI
GetOnePowerOfTwo64 (
  IN UINT64                         Operand
  )
{
  if (Operand == 0) {
    return 0;
  }

  return LShiftU64 (1, (LowBitSet64 (Operand)));
}

