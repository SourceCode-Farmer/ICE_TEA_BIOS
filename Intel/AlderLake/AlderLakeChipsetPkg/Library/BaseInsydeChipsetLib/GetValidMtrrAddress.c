/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <CpuRegs.h>
#include <CpuDataStruct.h>
#include <Register/Cpuid.h>

UINT64
GetValidMtrrAddress(
  VOID
  )
{
  UINT64                ValidMtrrAddressMask;
  UINT32                Eax;
  
  ValidMtrrAddressMask = LShiftU64((UINT64) 1, 36) & (~(UINT64)0x0FFF);

  AsmCpuid (CPUID_EXTENDED_FUNCTION, &Eax, NULL, NULL, NULL);
  if (Eax >= CPUID_VIR_PHY_ADDRESS_SIZE) {
    AsmCpuid (CPUID_VIR_PHY_ADDRESS_SIZE, &Eax, NULL, NULL, NULL);
    ValidMtrrAddressMask = (LShiftU64((UINT64) 1, Eax & 0xFF) - 1) & (~(UINT64)0x0FFF);
  }

  return ValidMtrrAddressMask;
}

