/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/FitPlatformLib.h>
#include <Library/BaseMemoryLib.h>
/**
 To get FITP.
 Arguments :
 Address - Pointer to FITP, or FITP
 Returns :
 EFI_SUCCESS           - FITP returning
 EFI_INVALID_PARAMETER - Invalid parameter inputted

 @param [in, out] Address       Pointer to FITP, or FITP


**/
EFI_STATUS
EFIAPI
FITPlatformLibGetFITP (
  IN OUT  UINTN        *Address
  )
{
  if ( Address == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  *Address = ( UINTN )( *( UINT32 * )( UINTN )( *Address ) );

  return EFI_SUCCESS;
}

/**
 To validate the address field of FIT Header entry by comparing the signature.
 Arguments :
 FIT - Pointer of the FIT
 Returns :
 EFI_SUCCESS   - Data valid
 EFI_NOT_FOUND - Data invalid

 @param [in]   FIT              Pointer of the FIT


**/
EFI_STATUS
EFIAPI
FITPlatformLibValidateFITHeaderAddress (
  IN      FIT_ENTRY        *FIT
  )
{
  CONST CHAR8        *FITHeaderAddress = V_FIT_ENTRY_FIT_HEADER_ADDRESS;

  return ( CompareMem ( &FIT->Address, FITHeaderAddress, S_FIT_ENTRY_FIT_HEADER_ADDRESS ) == 0 ) ? EFI_SUCCESS : EFI_NOT_FOUND;
}

/**
 To validate the Checksum of FIT.
 Arguments :
 FIT - Pointer of the FIT
 Returns :
 EFI_SUCCESS   - Data valid
 EFI_CRC_ERROR - Data invalid

 @param [in]   FIT              Pointer of the FIT


**/
EFI_STATUS
EFIAPI
FITPlatformLibValidateFITChecksum (
  IN      FIT_ENTRY        *FIT
  )
{
  UINT8        *Buffer;
  UINT8        Checksum;
  UINTN        FITLength;
  UINTN        Index;

  Buffer    = NULL;
  Checksum  = 0;
  FITLength = 0;
  Index     = 0;

  if ( !FIT->C_V ) {
    return EFI_SUCCESS;
  }

  Buffer    = ( UINT8 * )FIT;
  Checksum  = 0;
  FITLength = ( ( FIT->Size[2] << 16 ) | ( FIT->Size[1] << 8 ) | ( FIT->Size[0] << 0 ) ) * V_FIT_ENTRY_FIT_HEADER_SIZE_GRANULARITY;
  for ( Index = 0 ; Index < FITLength ; Index = Index + 1 ) {
    Checksum = ( UINT8 )( Checksum + Buffer[Index] );
  }

  return Checksum ? EFI_CRC_ERROR : EFI_SUCCESS;
}

/**
 To validate FIT.
 Arguments :
 FIT - Pointer of the FIT
 Returns :
 EFI_SUCCESS - Data valid
 Other       - Data invalid

 @param [in]   FIT              Pointer of the FIT


**/
EFI_STATUS
EFIAPI
FITPlatformLibValidateFIT (
  IN      FIT_ENTRY        *FIT
  )
{
  EFI_STATUS        Status;

  Status = FITPlatformLibValidateFITHeaderAddress ( FIT );

  if ( !EFI_ERROR ( Status ) ) {
    Status = FITPlatformLibValidateFITChecksum ( FIT );
  }

  return Status;
}

/**
 To translate the address to the corresponding one of the BIOS image.
 Arguments :
 Image       - BIOS image
 ImageLength - Length of the BIOS image
 Address     - Address
 Returns :
 EFI_SUCCESS           - Address translated
 EFI_INVALID_PARAMETER - Invalid parameter inputted

 @param [in]   Image            BIOS image
 @param [in]   ImageLength      Length of the BIOS image
 @param [in, out] Address       Address


**/
EFI_STATUS
EFIAPI
FITPlatformLibBIOSImageAddressTranslation (
  IN      VOID         *Image,
  IN      UINTN        ImageLength,
  IN OUT  UINTN        *Address
  )
{
  UINTN        BaseAddress;

  BaseAddress = 0;

  if ( Address == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  if ( ( Image == NULL ) || ( ImageLength == 0 ) ) {
    return EFI_SUCCESS;
  }

  BaseAddress = ( UINTN )( MEM_EQU_4GB - ImageLength );
  if ( *Address < BaseAddress ) {
    return EFI_INVALID_PARAMETER;
  }

  *Address = ( UINTN )( ( UINTN )( Image ) + ( *Address - BaseAddress ) );

  return EFI_SUCCESS;
}

/**
 To get FIT.
 Arguments :
 Image       - BIOS image
 ImageLength - Length of the BIOS image
 FIT         - Pointer of the FIT
 Returns :
 EFI_SUCCESS           - FIT returning
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 Other                 - FIT can't be found or is invalid

 @param [in]   Image            BIOS image
 @param [in]   ImageLength      Length of the BIOS image
 @param [out]  FIT              Pointer of the FIT


**/
EFI_STATUS
EFIAPI
FITPlatformLibGetFITEx (
  IN      VOID             *Image,
  IN      UINTN            ImageLength,
     OUT  FIT_ENTRY        **FIT
  )
{
  EFI_STATUS        Status;
  FIT_ENTRY         *FITBuffer;

  FITBuffer = NULL;

  if ( FIT == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  FITBuffer = ( FIT_ENTRY * )( UINTN )R_FITP;
  Status    = FITPlatformLibBIOSImageAddressTranslation ( Image, ImageLength, ( UINTN * )( &FITBuffer ) );
  if ( !EFI_ERROR ( Status ) ) {
    Status = FITPlatformLibGetFITP ( ( UINTN * )( &FITBuffer ) );
  }
  if ( !EFI_ERROR ( Status ) ) {
    Status = FITPlatformLibBIOSImageAddressTranslation ( Image, ImageLength, ( UINTN * )( &FITBuffer ) );
  }

  if ( !EFI_ERROR ( Status ) ) {
    Status = FITPlatformLibValidateFIT ( FITBuffer );
  }

  if ( !EFI_ERROR ( Status ) ) {
    *FIT = FITBuffer;
  }

  return Status;
}

/**
 To get FIT.
 Arguments :
 FIT - Pointer of the FIT
 Returns :
 EFI_SUCCESS           - FIT returning
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 Other                 - FIT can't be found or is invalid

 @param [out]  FIT              Pointer of the FIT


**/
EFI_STATUS
EFIAPI
FITPlatformLibGetFIT (
     OUT  FIT_ENTRY        **FIT
  )
{
  if ( FIT == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  return FITPlatformLibGetFITEx ( NULL, 0, FIT );
}

/**
 To find the next FIT Entry with specific Entry Type.
 Arguments :
 FIT       - Pointer of the FIT
 EntryType - FIT Entry Type used to search
 Entry     - Pointer of specific FIT Entry
 Returns :
 EFI_SUCCESS           - FIT Entry found
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 EFI_NOT_FOUND         - FIT Entry not found
 Other                 - Failed to get FIT

 @param [in]   FIT              Pointer of the FIT
 @param [in]   EntryType        FIT Entry Type used to search
 @param [in, out] Entry         Pointer of specific FIT Entry


**/
EFI_STATUS
EFIAPI
FITPlatformLibGetNextEntry (
  IN      FIT_ENTRY        *FIT,
  IN      UINT8            EntryType,
  IN OUT  FIT_ENTRY        **Entry
  )
{
  EFI_STATUS        Status;
  FIT_ENTRY         *FITBuffer;
  FIT_ENTRY         *CurrentEntry;
  UINTN             FITEntryAmount;
  UINTN             Index;

  FITBuffer      = NULL;
  CurrentEntry   = NULL;
  FITEntryAmount = 0;
  Index          = 0;

  if ( Entry == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  Status    = EFI_SUCCESS;
  FITBuffer = FIT;
  if ( FITBuffer == NULL ) {
    Status = FITPlatformLibGetFIT ( &FITBuffer );
  }
  if ( !EFI_ERROR ( Status ) ) {
    CurrentEntry   = ( *Entry == NULL ) ? FITBuffer : *Entry;
    FITEntryAmount = ( ( FITBuffer->Size[2] << 16 ) | ( FITBuffer->Size[1] << 8 ) | ( FITBuffer->Size[0] << 0 ) ) * V_FIT_ENTRY_FIT_HEADER_SIZE_GRANULARITY / sizeof ( FIT_ENTRY );
    for ( Index = 0 ; Index < FITEntryAmount ; Index = Index + 1 ) {
      if ( &FITBuffer[Index] == CurrentEntry ) {
        break;
      }
    }
    Index = ( *Entry == NULL ) ? Index : ( Index + 1 );
    for ( ; Index < FITEntryAmount ; Index = Index + 1 ) {
      if ( FITBuffer[Index].Type == EntryType ) {
        *Entry = &FITBuffer[Index];
        break;
      }
    }
    if ( Index >= FITEntryAmount ) {
      Status = EFI_NOT_FOUND;
    }
  }

  return Status;
}

/**
 To find the first FIT Entry with specific Entry Type.
 Arguments :
 FIT       - Pointer of the FIT
 EntryType - FIT Entry Type used to search
 Entry     - Pointer of specific FIT Entry
 Returns :
 EFI_SUCCESS           - FIT Entry found
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 Other                 - FIT Entry not found

 @param [in]   FIT              Pointer of the FIT
 @param [in]   EntryType        FIT Entry Type used to search
 @param [out]  Entry            Pointer of specific FIT Entry


**/
EFI_STATUS
EFIAPI
FITPlatformLibGetFirstEntry (
  IN      FIT_ENTRY        *FIT,
  IN      UINT8            EntryType,
     OUT  FIT_ENTRY        **Entry
  )
{
  EFI_STATUS        Status;
  FIT_ENTRY         *CurrentEntry;

  CurrentEntry = NULL;

  if ( Entry == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  CurrentEntry = NULL;
  Status       = FITPlatformLibGetNextEntry ( FIT, EntryType, &CurrentEntry );
  if ( !EFI_ERROR ( Status ) ) {
    *Entry = CurrentEntry;
  }

  return Status;
}
