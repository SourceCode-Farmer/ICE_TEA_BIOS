/** @file
;******************************************************************************
;* Copyright (c) 2015-2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeInsydeChipsetLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/MemoryAllocationLib.h>
#include <SetupVariable.h>
#include <MeSetup.h>
#include <Library/VariableLib.h>

/**
 Retrieve SETUP DATA (default) variable. This function is created for DXE driver
 without normal variable service.

 @param[out] SaSetup            Pointer to the structure of SA_SETUP,
                                this pointer must be allocated with sizeof(SA_SETUP)
                                before being called
 @param[in]  SaSetupSize        SA setup variable size

 @retval EFI_SUCCESS            The SA setup (default) variable is successfully retrieved
 @retval EFI_NOT_FOUND          SA setup (default) variable is not found
 @retval EFI_INVALID_PARAMETER  No enough memory to return data
**/
EFI_STATUS
EFIAPI
GetChipsetSetupDataVariableDxe (
  IN OUT SETUP_DATA                     *SetupData,
  IN     UINTN                          SetupDataSize
  )
{
  UINTN            DataSize;

  DataSize = SetupDataSize;
  return CommonGetVariable (
           PLATFORM_SETUP_VARIABLE_NAME,
           &gSetupVariableGuid,
           &DataSize,
           SetupData
           );
}


/**
 Retrieve SA setup (default) variable. This function is created for DXE driver
 without normal variable service.

 @param[out] SaSetup            Pointer to the structure of SA_SETUP,
                                this pointer must be allocated with sizeof(SA_SETUP)
                                before being called
 @param[in]  SaSetupSize        SA setup variable size

 @retval EFI_SUCCESS            The SA setup (default) variable is successfully retrieved
 @retval EFI_NOT_FOUND          SA setup (default) variable is not found
 @retval EFI_INVALID_PARAMETER  No enough memory to return data
**/
EFI_STATUS
EFIAPI
GetChipsetSaSetupVariableDxe (
  IN OUT SA_SETUP                       *SaSetup,
  IN     UINTN                          SaSetupSize
  )
{
  UINTN            DataSize;

  DataSize = SaSetupSize;
  return CommonGetVariable (
           SA_SETUP_VARIABLE_NAME,
           &gSaSetupVariableGuid,
           &DataSize,
           SaSetup
           );
}


/**
 Retrieve ME setup (default) variable. This function is created for DXE driver
 without normal variable service.

 @param[out] MeSetup            Pointer to the structure of ME_SETUP,
                                this pointer must be allocated with sizeof(ME_SETUP)
                                before being called
 @param[in]  MeSetupSize        ME setup variable size

 @retval EFI_SUCCESS            The ME setup (default) variable is successfully retrieved
 @retval EFI_NOT_FOUND          ME setup (default) variable is not found
 @retval EFI_INVALID_PARAMETER  No enough memory to return data
**/
EFI_STATUS
EFIAPI
GetChipsetMeSetupVariableDxe (
  IN OUT ME_SETUP                       *MeSetup,
  IN     UINTN                          MeSetupSize
  )
{
  UINTN            DataSize;

  DataSize = MeSetupSize;
  return CommonGetVariable (
           ME_SETUP_VARIABLE_NAME,
           &gMeSetupVariableGuid,
           &DataSize,
           MeSetup
           );
}


/**
 Retrieve CPU setup (default) variable. This function is created for DXE driver
 without normal variable service.

 @param[out] CpuSetup           Pointer to the structure of CPU_SETUP,
                                this pointer must be allocated with sizeof(CPU_SETUP)
                                before being called
 @param[in]  CpuSetupSize       CPU setup variable size

 @retval EFI_SUCCESS            The CPU setup (default) variable is successfully retrieved
 @retval EFI_NOT_FOUND          CPU setup (default) variable is not found
 @retval EFI_INVALID_PARAMETER  No enough memory to return data
**/
EFI_STATUS
EFIAPI
GetChipsetCpuSetupVariableDxe (
  IN OUT CPU_SETUP                      *CpuSetup,
  IN     UINTN                          CpuSetupSize
  )
{
  UINTN            DataSize;

  DataSize = CpuSetupSize;
  return CommonGetVariable (
           CPU_SETUP_VARIABLE_NAME,
           &gCpuSetupVariableGuid,
           &DataSize,
           CpuSetup
           );
}


/**
 Retrieve PCH setup (default) variable. This function is created for DXE driver
 without normal variable service.

 @param[out] PchSetup           Pointer to the structure of PCH_SETUP,
                                this pointer must be allocated with sizeof(PCH_SETUP)
                                before being called
 @param[in]  PchSetupSize       PCH setup variable size

 @retval EFI_SUCCESS            The PCH setup (default) variabl is successfully retrieved
 @retval EFI_NOT_FOUND          PCH setup (default) variable is not found
 @retval EFI_INVALID_PARAMETER  No enough memory to return data
**/
EFI_STATUS
EFIAPI
GetChipsetPchSetupVariableDxe (
  IN OUT PCH_SETUP                      *PchSetup,
  IN     UINTN                          PchSetupSize
  )
{
  UINTN            DataSize;

  DataSize = PchSetupSize;
  return CommonGetVariable (
           PCH_SETUP_VARIABLE_NAME,
           &gPchSetupVariableGuid,
           &DataSize,
           PchSetup
           );
}


/**
 Retrieve ME setup storage (default) variable. This function is created for
 DXE driver without normal variable service.

 @param[out] MeSetupStorage     Pointer to the structure of ME_SETUP_STORAGE,
                                this pointer must be allocated with sizeof(ME_SETUP_STORAGE)
                                before being called
 @param[in]  MeSetupStorageSize ME setup storage variable size

 @retval EFI_SUCCESS            The ME setup storage variable data is successfully retrieved
 @retval EFI_NOT_FOUND          ME setup storage (default) variable is not found
 @retval EFI_INVALID_PARAMETER  No enough memory to return data
**/
EFI_STATUS
EFIAPI
GetChipsetMeSetupStorageVariableDxe (
  IN OUT ME_SETUP_STORAGE               *MeSetupStorage,
  IN     UINTN                          MeSetupStorageSize
  )
{
  UINTN            DataSize;

  DataSize = MeSetupStorageSize;
  return CommonGetVariable (
           ME_SETUP_STORAGE_VARIABLE_NAME,
           &gMeSetupVariableGuid,
           &DataSize,
           MeSetupStorage
           );
}