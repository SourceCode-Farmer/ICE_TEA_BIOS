;;@file
;  Generate a random number in 64 bit environment
;
;@copyright
; Copyright (c) 2014 Intel Corporation. All rights reserved
; This software and associated documentation (if any) is furnished
; under a license and may only be used or copied in accordance
; with the terms of the license. Except as permitted by the
; license, no part of this software or documentation may be
; reproduced, stored in a retrieval system, or transmitted in any
; form or by any means without the express written consent of
; Intel Corporation.
; This file contains an 'Intel Peripheral Driver' and is uniquely
; identified as "Intel Reference Module" and is licensed for Intel
; CPUs and chipsets under the terms of your license agreement with
; Intel or your vendor. This file may be modified by the user, subject
; to additional terms of the license agreement.
;
;@par Specification Reference:
;;

.code

;
; Generate a 16 bit random number
;
GetRandomNumber16  PROC
        ; rdrand  ax           ; generate a 16 bit RN into eax, CF=1 if RN generated ok, otherwise CF=0
        clc
@@:
        db  0fh,0c7h,0f0h       ; rdrand r16: "0F C7 /6 ModRM:r/m(w)"
        jnc @b                  ; CF will be set if valid number was generated
        ret
GetRandomNumber16 ENDP

;
; Generate a 32 bit random number
;
GetRandomNumber32  PROC
        ; rdrand  eax           ; generate a 32 bit RN into eax, CF=1 if RN generated ok, otherwise CF=0
        clc
@@:
        db  0fh,0c7h,0f0h       ; rdrand r32: "0F C7 /6 ModRM:r/m(w)"
        jnc @b                  ; CF will be set if valid number was generated
        ret
GetRandomNumber32 ENDP

;
; Generate a 64 bit random number
;
GetRandomNumber64  PROC
        ; rdrand  rax            ; generate a 64 bit RN into rax, CF=1 if RN generated ok, otherwise CF=0
        clc
@@:
        db  048h,0fh,0c7h,0f0h   ; rdrand r64: "REX.W + 0F C7 /6 ModRM:r/m(w)"
        jnc @b
        ret
GetRandomNumber64 ENDP
END
