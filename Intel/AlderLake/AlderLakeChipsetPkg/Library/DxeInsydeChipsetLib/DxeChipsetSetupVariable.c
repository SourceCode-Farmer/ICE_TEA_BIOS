/** @file
;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/DxeInsydeChipsetLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Guid/SetupDefaultHob.h>
#include <Library/VariableLib.h>

/**
 Retrieve setup (default) variable. This function is created for DXE driver
 without normal variable service.

 @param[out] SetupVariable      Pointer to the structure of CHIPSET_CONFIGURATION,
                                this pointer must be allocated with sizeof(CHIPSET_CONFIGURATION)
                                before being called
 @param[in]  SetupVariableSize  Setup variable data Size

 @retval EFI_SUCCESS            The Chipset configuration is successfully retrieved
 @retval EFI_NOT_FOUND          Setup (default) variable is not found
 @retval EFI_INVALID_PARAMETER  Parameter error
**/
EFI_STATUS
EFIAPI
GetChipsetSetupVariableDxe (
  IN OUT CHIPSET_CONFIGURATION          *SetupVariable,
  IN     UINTN                          SetupVariableSize
  )
{
  UINTN            DataSize;

  DataSize = SetupVariableSize;
  return CommonGetVariable (
           L"Setup",
           &gSystemConfigurationGuid,
           &DataSize,
           SetupVariable
           );
}