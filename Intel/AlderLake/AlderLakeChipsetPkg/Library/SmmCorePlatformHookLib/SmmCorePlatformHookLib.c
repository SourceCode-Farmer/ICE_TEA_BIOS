/** @file
  SMM Core Platform Hook Library instance

;******************************************************************************
;* Copyright (c) 2018 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/IoLib.h>
#include <Library/SmmCorePlatformHookLib.h>
#include <Register/PmcRegs.h>
#include <Library/PcdLib.h>
#include <Register/TcoRegs.h>

//
// TRUE:  Watchdog timer is running when entering SMM.
// FALSE: Watchdog timer is stopped when entering SMM.
//
BOOLEAN mTcoTimerHaltFlag = FALSE;


/**
  Determine Watchdog timer Control status when entering SMM.

  @retval  TRUE        Watchdog timer is running when entering SMM.
  @retval  FALSE       Watchdog timer is stopped when entering SMM.

**/
BOOLEAN
CheckTcoTimerHaltStatus (
  VOID
  )
{
  return (BOOLEAN) ((IoRead16 (PcdGet16 (PcdTcoBaseAddress) + R_TCO_IO_TCO1_CNT) & B_TCO_IO_TCO1_CNT_TMR_HLT) == 0);
}


/**
 Prevents system from reset when entering SMM in DDT debug. Halts Watchdog when entering SMM
 and resume it when exiting SMM.

 @param [in]   TcoTmrFlag     Flag to halt or resume Watchdog timer

**/
VOID
ChangeWatchDogTimerStatus (
  BOOLEAN   TcoTmrFlag
  )
{
  if (TcoTmrFlag) {
    //
    // Resume TCO Timer
    //
    IoAnd16 (PcdGet16 (PcdTcoBaseAddress) + R_TCO_IO_TCO1_CNT, (UINT16)~B_TCO_IO_TCO1_CNT_TMR_HLT);
  } else {
    //
    // Halt TCO Timer
    //
    IoOr16 (PcdGet16 (PcdTcoBaseAddress) + R_TCO_IO_TCO1_CNT, (UINT16)B_TCO_IO_TCO1_CNT_TMR_HLT);
  }
}

/**
  Performs platform specific tasks before invoking registered SMI handlers.

  This function performs platform specific tasks before invoking registered SMI handlers.

  @retval EFI_SUCCESS       The platform hook completes successfully.
  @retval Other values      The paltform hook cannot complete due to some error.

**/
EFI_STATUS
EFIAPI
PlatformHookBeforeSmmDispatch (
  VOID
  )
{
  //
  // Check Tco Timer status when entering SMM, if timer is running, stops the timer.
  //
  mTcoTimerHaltFlag = CheckTcoTimerHaltStatus ();
  if (mTcoTimerHaltFlag) {
    ChangeWatchDogTimerStatus (FALSE);
  }

  return EFI_SUCCESS;
}

/**
  Performs platform specific tasks after invoking registered SMI handlers.

  This function performs platform specific tasks after invoking registered SMI handlers.

  @retval EFI_SUCCESS       The platform hook completes successfully.
  @retval Other values      The paltform hook cannot complete due to some error.

**/
EFI_STATUS
EFIAPI
PlatformHookAfterSmmDispatch (
  VOID
  )
{
  //
  // If TCO timer was running when entering SMM, resumes the timer.
  //
  if (mTcoTimerHaltFlag) {
    ChangeWatchDogTimerStatus (TRUE);
  }

  return EFI_SUCCESS;
}
