/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/BootGuardPlatformLib.h>
//[-start-200906-IB16740115-remove]// CpuAccess.h has removed in RC1362.
//#include <CpuAccess.h>
#include <CpuRegs.h>
//[-end-200906-IB16740115-remove]//
#include <Register/MeRegs.h>
#include <Library/MmPciLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/IoLib.h>
#include <Library/PciLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/HobLib.h>
#include <MeState.h>
#include <Register/HeciRegs.h>

//[-start-200721-IB17040134-add]//
//
// This define in D:\Intel "ClientOneSiliconPkg\Include\Register\MeRegs.h"
// But this define RC was no longer used and removed.
//
#define B_TPM_DISCONNECT              0x1000
//[-start-200721-IB17040134-add]//

//
// BUGBUG : Some bits are not yet defined in the RC
//
#define R_MSR_BOOT_GUARD_SACM_INFO                                      ( MSR_BOOT_GUARD_SACM_INFO )
#define B_MSR_BOOT_GUARD_SACM_INFO_BOOT_GUARD_CAPABILITY                ( BIT32 )
#define B_MSR_BOOT_GUARD_SACM_INFO_FACB                                 ( BIT4 )
#define B_MSR_BOOT_GUARD_SACM_INFO_PROFILE_SELECTION                    ( B_BOOT_GUARD_SACM_INFO_VERIFIED_BOOT | B_BOOT_GUARD_SACM_INFO_MEASURED_BOOT | B_MSR_BOOT_GUARD_SACM_INFO_FACB )
#define B_MSR_BOOT_GUARD_SACM_INFO_VERIFICATION_STATUS                  ( B_BOOT_GUARD_SACM_INFO_NEM_ENABLED )
#define V_TXT_PUBLIC_BASE                                               ( TXT_PUBLIC_BASE )
#define R_TXT_PUBLIC_BASE_BOOT_GUARD_ACM_STATUS                         ( R_CPU_BOOT_GUARD_ACM_STATUS )
#define B_TXT_PUBLIC_BASE_BOOT_GUARD_ACM_STATUS_ACM_STARTED             ( BIT15 )

#define B_ME_HFS_4_BOOT_GUARD_FWSTS_VALID                               ( BIT14 )
#define B_ME_HFS_4_ALL_TPMS_DISCONNECTED                                ( B_TPM_DISCONNECT )

#define B_ME_HFS_6_FPF_DISABLE                                          ( BIT29 )
#define B_ME_HFS_6_KMID                                                 ( BIT25 | BIT24 | BIT23 | BIT22 )
#define B_ME_HFS_6_BPMSVN                                               ( BIT21 | BIT20 | BIT19 | BIT18 )
#define B_ME_HFS_6_KMSVN                                                ( BIT17 | BIT16 | BIT15 | BIT14 )
#define B_ME_HFS_6_ACMSVN                                               ( BIT13 | BIT12 | BIT11 | BIT10 )

//[-start-190701-16990082-add]//
///
/// HECI PCI Access Macro
///
//#define HeciPciRead32(Register) PciRead32 (PCI_LIB_ADDRESS (ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, Register))
//[-end-190701-16990082-add]//
#define HeciPciSegmentRead32(Register) PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, Register))

/**
 To determine ME State
**/
EFI_STATUS
BootGuardPlatformLibGetMeStatus (
  OUT UINT32        *MeStatus
  )
{
  HECI_FWS_REGISTER MeFirmwareStatus;
  UINT64            HeciBaseAddress;

  if (MeStatus == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  HeciBaseAddress = PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, 0);
  if (PciSegmentRead16 (HeciBaseAddress + PCI_DEVICE_ID_OFFSET) == 0xFFFF) {
    *MeStatus = ME_NOT_READY;
    return EFI_NOT_FOUND;
  }

  MeFirmwareStatus.ul = PciSegmentRead32 (HeciBaseAddress + R_ME_HFS);

  if (MeFirmwareStatus.r.CurrentState == ME_STATE_NORMAL && MeFirmwareStatus.r.ErrorCode == ME_ERROR_CODE_NO_ERROR) {
    *MeStatus = ME_READY;
  } else if (MeFirmwareStatus.r.CurrentState == ME_STATE_RECOVERY) {
    *MeStatus = ME_IN_RECOVERY_MODE;
  } else if (MeFirmwareStatus.r.CurrentState == ME_STATE_INIT) {
    *MeStatus = ME_INITIALIZING;
  } else if (MeFirmwareStatus.r.CurrentState == ME_STATE_DISABLE_WAIT) {
    *MeStatus = ME_DISABLE_WAIT;
  } else if (MeFirmwareStatus.r.CurrentState == ME_STATE_TRANSITION) {
    *MeStatus = ME_TRANSITION;
  } else {
    *MeStatus = ME_NOT_READY;
  }

  if (MeFirmwareStatus.r.FwUpdateInprogress) {
    *MeStatus |= ME_FW_UPDATES_IN_PROGRESS;
  }

  if (MeFirmwareStatus.r.FwInitComplete == ME_FIRMWARE_COMPLETED) {
    *MeStatus |= ME_FW_INIT_COMPLETE;
  }

  if (MeFirmwareStatus.r.MeBootOptionsPresent == ME_BOOT_OPTIONS_PRESENT) {
    *MeStatus |= ME_FW_BOOT_OPTIONS_PRESENT;
  }

  return EFI_SUCCESS;
}  

/**
 To determine Boot State of Boot Guard.
 Arguments :
 None
 Returns :
 Boot State

 @param None


**/
BOOT_GUARD_BOOT_STATE
BootGuardPlatformLibDetermineBootState (
  VOID
  )
{
  BOOT_GUARD_BOOT_STATE        BootState;

  //
  // Follow Boot Guard BWG to determine current Boot State
  //
  BootState = BootGuardBootStateLegacyBoot;
  if ( IsBootGuardSupported () ) {
    if ( AsmReadMsr64 ( MSR_BOOT_GUARD_SACM_INFO ) & B_MSR_BOOT_GUARD_SACM_INFO_BOOT_GUARD_CAPABILITY ) {
  	  if ( !( HeciPciSegmentRead32 (R_ME_HFS_4) & B_ME_HFS_4_ALL_TPMS_DISCONNECTED ) ) {
          if ( *( UINT16 * )( UINTN )( V_TXT_PUBLIC_BASE + R_TXT_PUBLIC_BASE_BOOT_GUARD_ACM_STATUS ) & B_TXT_PUBLIC_BASE_BOOT_GUARD_ACM_STATUS_ACM_STARTED ) {
            if ( ( AsmReadMsr64 ( MSR_BOOT_GUARD_SACM_INFO ) & B_MSR_BOOT_GUARD_SACM_INFO_PROFILE_SELECTION ) != 0 ) {
              if ( AsmReadMsr64 ( MSR_BOOT_GUARD_SACM_INFO ) & B_MSR_BOOT_GUARD_SACM_INFO_VERIFICATION_STATUS ) {
                BootState = BootGuardBootStateVerificationPassed;
              } else {
                BootState = BootGuardBootStateVerificationFailed;
              }
            }
          }
        } else {
          BootState = BootGuardBootStateVerificationFailed;
        }
    }
  }

  return BootState;
}

/**
 To validate the StructureID field of the BPM Header by comparing the signature.
 Arguments :
 BPMH - Pointer of the BPM Header
 Returns :
 EFI_SUCCESS   - Data valid
 EFI_NOT_FOUND - Data invalid

 @param [in]   BPMH             Pointer of the BPM Header


**/
EFI_STATUS
BootGuardPlatformLibValidateBPMStructureID (
  IN      BOOT_POLICY_MANIFEST_HEADER        *BPMH
  )
{
  CONST CHAR8        *StructureID = V_BOOT_POLICY_MANIFEST_HEADER_STRUCTURE_ID;

//[-start-190701-16990082-add]//
  return ( CompareMem ( &BPMH->StructureId, StructureID, S_BOOT_POLICY_MANIFEST_HEADER_STRUCTURE_ID ) == 0 ) ? EFI_SUCCESS : EFI_NOT_FOUND;
//[-end-190701-16990082-add]//
}
/**
 To validate the StructureID field of the KM by comparing the signature.
 Arguments :
 KM - Pointer of the KM
 Returns :
 EFI_SUCCESS   - Data valid
 EFI_NOT_FOUND - Data invalid

 @param [in]   KM               Pointer of the KM


**/
EFI_STATUS
BootGuardPlatformLibValidateKMStructureID (
  IN      KEY_MANIFEST        *KM
  )
{
  CONST CHAR8        *StructureID = V_KEY_MANIFEST_STRUCTURE_ID;

//[-start-190701-16990082-add]//
  return ( CompareMem ( &KM->StructureId, StructureID, S_KEY_MANIFEST_STRUCTURE_ID ) == 0 ) ? EFI_SUCCESS : EFI_NOT_FOUND;
//[-end-190701-16990082-add]//
}
/**
 To get KMID.
 Arguments :
 KMID - KMID
 Returns :
 EFI_SUCCESS           - KMID returning
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 EFI_UNSUPPORTED       - KMID not available

 @param [out]  KMID             KMID


**/
EFI_STATUS
BootGuardPlatformLibGetKMID (
     OUT  UINT8        *KMID
  )
{
  UINT32                 MeFwSts4;
  UINT32                 MeFwSts6;
  UINT32                 MeStatus;
  EFI_STATUS             Status;

  MeStatus   = ME_NOT_READY;

  if ( KMID == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  Status = BootGuardPlatformLibGetMeStatus (&MeStatus);

  if ((MeStatus & 0x0f) != ME_READY) {
    return EFI_UNSUPPORTED;
  }

  MeFwSts4 = HeciPciSegmentRead32 (R_ME_HFS_4);
  MeFwSts6 = HeciPciSegmentRead32 (R_ME_HFS_6);

  //
  // Check if Boot Guard FWSTS Valid.
  //
  if (!(MeFwSts4 & B_ME_HFS_4_BOOT_GUARD_FWSTS_VALID)) {
    return EFI_UNSUPPORTED;
  }

  //
  // Check if FPF Disable.
  //
  if (MeFwSts6 & B_ME_HFS_6_FPF_DISABLE) {
    return EFI_UNSUPPORTED;
  }

  *KMID = (UINT8)((MeFwSts6 & B_ME_HFS_6_KMID) >> 22);

  return EFI_SUCCESS;
}

/**
 To get the ACM.
 Arguments :
 Image       - BIOS image
 ImageLength - Length of the BIOS image
 FIT         - Pointer of the FIT
 ACMH        - Pointer of the ACM Header
 Returns :
 EFI_SUCCESS           - ACM found
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 Other                 - ACM not found

 @param [in]   Image            BIOS image
 @param [in]   ImageLength      Length of the BIOS image
 @param [in]   FIT              Pointer of the FIT
 @param [out]  ACMH             Pointer of the ACM Header


**/
EFI_STATUS
BootGuardPlatformLibGetACMEx (
  IN      VOID              *Image,
  IN      UINTN             ImageLength,
  IN      FIT_ENTRY         *FIT,
     OUT  ACM_HEADER        **ACMH
  )
{
  EFI_STATUS        Status;
  FIT_ENTRY         *Entry;
  ACM_HEADER        *ACMBuffer;

  Entry     = NULL;
  ACMBuffer = NULL;

  if ( ACMH == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  Status = FITPlatformLibGetFirstEntry ( FIT, V_FIT_ENTRY_TYPE_STARTUP_AC_MODULE, &Entry );
  if ( !EFI_ERROR ( Status ) ) {
    ACMBuffer = ( ACM_HEADER * )( UINTN )Entry->Address;
    Status    = FITPlatformLibBIOSImageAddressTranslation ( Image, ImageLength, ( UINTN * )( &ACMBuffer ) );
  }
  if ( !EFI_ERROR ( Status ) ) {
    *ACMH  = ACMBuffer;
  }

  return Status;
}

/**
 To get the ACM.
 Arguments :
 FIT  - Pointer of the FIT
 ACMH - Pointer of the ACM Header
 Returns :
 EFI_SUCCESS           - ACM found
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 Other                 - ACM not found

 @param [in]   FIT              Pointer of the FIT
 @param [out]  ACMH             Pointer of the ACM Header


**/
EFI_STATUS
BootGuardPlatformLibGetACM (
  IN      FIT_ENTRY         *FIT,
     OUT  ACM_HEADER        **ACMH
  )
{
  if ( ACMH == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  return BootGuardPlatformLibGetACMEx ( NULL, 0, FIT, ACMH );
}

/**
 To get the BPM.
 Arguments :
 Image       - BIOS image
 ImageLength - Length of the BIOS image
 FIT         - Pointer of the FIT
 BPMH        - Pointer of the BPM Header
 Returns :
 EFI_SUCCESS           - BPM found
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 Other                 - BPM not found

 @param [in]   Image            BIOS image
 @param [in]   ImageLength      Length of the BIOS image
 @param [in]   FIT              Pointer of the FIT
 @param [out]  BPMH             Pointer of the BPM Header


**/
EFI_STATUS
BootGuardPlatformLibGetBPMEx (
  IN      VOID                               *Image,
  IN      UINTN                              ImageLength,
  IN      FIT_ENTRY                          *FIT,
     OUT  BOOT_POLICY_MANIFEST_HEADER        **BPMH
  )
{
  EFI_STATUS                         Status;
  FIT_ENTRY                          *Entry;
  BOOT_POLICY_MANIFEST_HEADER        *BPMBuffer;

  Entry     = NULL;
  BPMBuffer = NULL;

  if ( BPMH == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  Status = FITPlatformLibGetFirstEntry ( FIT, V_FIT_ENTRY_TYPE_BOOT_POLICY_MANIFEST, &Entry );
  if ( !EFI_ERROR ( Status ) ) {
    BPMBuffer = ( BOOT_POLICY_MANIFEST_HEADER * )( UINTN )Entry->Address;
    Status    = FITPlatformLibBIOSImageAddressTranslation ( Image, ImageLength, ( UINTN * )( &BPMBuffer ) );
  }
  if ( !EFI_ERROR ( Status ) ) {
    Status = BootGuardPlatformLibValidateBPMStructureID ( BPMBuffer );
  }
  if ( !EFI_ERROR ( Status ) ) {
    *BPMH = BPMBuffer;
  }

  return Status;
}

/**
 To get the BPM.
 Arguments :
 FIT  - Pointer of the FIT
 BPMH - Pointer of the BPM Header
 Returns :
 EFI_SUCCESS           - BPM found
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 Other                 - BPM not found

 @param [in]   FIT              Pointer of the FIT
 @param [out]  BPMH             Pointer of the BPM Header


**/
EFI_STATUS
BootGuardPlatformLibGetBPM (
  IN      FIT_ENTRY                          *FIT,
     OUT  BOOT_POLICY_MANIFEST_HEADER        **BPMH
  )
{
  if ( BPMH == NULL ) {
    return EFI_INVALID_PARAMETER;
  }

  return BootGuardPlatformLibGetBPMEx ( NULL, 0, FIT, BPMH );
}

/**
 To get the KM.
 Arguments :
 Image       - BIOS image
 ImageLength - Length of the BIOS image
 FIT         - Pointer of the FIT
 KM          - Pointer of the KM
 Returns :
 EFI_SUCCESS           - KM found
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 EFI_UNSUPPORTED       - KMID not available
 Other                 - KM not found

 @param [in]   Image            BIOS image
 @param [in]   ImageLength      Length of the BIOS image
 @param [in]   FIT              Pointer of the FIT
 @param [out]  KM               Pointer of the KM


**/
EFI_STATUS
BootGuardPlatformLibGetKMEx (
  IN      VOID                *Image,
  IN      UINTN               ImageLength,
  IN      FIT_ENTRY           *FIT,
     OUT  KEY_MANIFEST        **KM,
  IN      UINT8               *CurrentKMID
  )
{
  EFI_STATUS          Status;
  UINT8               KMID;
  BOOLEAN             EntryFound;
  FIT_ENTRY           *Entry;
  KEY_MANIFEST        *KMBuffer;

  KMID       = 0;
  EntryFound = FALSE;
  Entry      = NULL;
  KMBuffer   = NULL;
  
  if (KM == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = BootGuardPlatformLibGetKMID (&KMID);

  if (EFI_ERROR (Status)) {
    if (CurrentKMID != NULL) {
      KMID = *CurrentKMID;
    } else {
      return EFI_UNSUPPORTED;
    }
  }

  Entry = NULL;
  do {
    EntryFound = FALSE;
    Status     = FITPlatformLibGetNextEntry ( FIT, V_FIT_ENTRY_TYPE_KEY_MANIFEST, &Entry );
    if ( !EFI_ERROR ( Status ) ) {
      EntryFound = TRUE;

      KMBuffer = ( KEY_MANIFEST * )( UINTN )Entry->Address;
      Status   = FITPlatformLibBIOSImageAddressTranslation ( Image, ImageLength, ( UINTN * )( &KMBuffer ) );
    }
    if ( !EFI_ERROR ( Status ) ) {
      Status = BootGuardPlatformLibValidateKMStructureID ( KMBuffer );
    }
    if ( !EFI_ERROR ( Status ) ) {
//[-start-190701-16990082-add]//
	  if ( KMBuffer->KeyManifestId == KMID ) {
//[-end-190701-16990082-add]//
        break;
      }
    }
  } while ( EntryFound );

  if ( !EFI_ERROR ( Status ) ) {
    *KM = KMBuffer;
  }

  return Status;
}

/**
 To get the KM.
 Arguments :
 FIT - Pointer of the FIT
 KM  - Pointer of the KM
 Returns :
 EFI_SUCCESS           - KM found
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 EFI_UNSUPPORTED       - KMID not available
 Other                 - KM not found

 @param [in]   FIT              Pointer of the FIT
 @param [out]  KM               Pointer of the KM


**/
EFI_STATUS
BootGuardPlatformLibGetKM (
  IN      FIT_ENTRY           *FIT,
     OUT  KEY_MANIFEST        **KM
  )
{
  if (KM == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  return BootGuardPlatformLibGetKMEx (NULL, 0, FIT, KM, NULL);
}

/**
 To get revocation values.
 Arguments :
 Image       - BIOS image
 ImageLength - Length of the BIOS image
 Revocation  - Stucture contains revocation values
 Returns :
 EFI_SUCCESS           - Revocation values returning
 EFI_INVALID_PARAMETER - Invalid parameter inputted
 EFI_UNSUPPORTED       - Revocation values not available

 @param [in]   Image            BIOS image
 @param [in]   ImageLength      Length of the BIOS image
 @param [out]  Revocation       Stucture contains revocation values


**/
EFI_STATUS
BootGuardPlatformLibGetRevocationValues (
  IN      VOID                    *Image,
  IN      UINTN                   ImageLength,
     OUT  REVOCATION_VALUE        *Revocation,
  IN      UINT8                   *CurrentKMID
  )
{
  EFI_STATUS                         Status;
  FIT_ENTRY                          *FIT;
  BOOT_POLICY_MANIFEST_HEADER        *BPMH;
  KEY_MANIFEST                       *KM;
  ACM_HEADER                         *ACMH;

  FIT  = NULL;
  BPMH = NULL;
  KM   = NULL;
  ACMH = NULL;

  if (Revocation == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = FITPlatformLibGetFITEx (Image, ImageLength, &FIT);
  if (!EFI_ERROR (Status)) {
    Status = BootGuardPlatformLibGetACMEx (Image, ImageLength, FIT, &ACMH);
  }
  if (!EFI_ERROR (Status)) {
    Status = BootGuardPlatformLibGetBPMEx (Image, ImageLength, FIT, &BPMH);
  }
  if (!EFI_ERROR (Status)) {
    Status = BootGuardPlatformLibGetKMEx (Image, ImageLength, FIT, &KM, CurrentKMID);
  }

  if (!EFI_ERROR (Status)) {
    Revocation->ACMSVN = (UINT8)ACMH->AcmSvn;
    Revocation->BPMSVN = (UINT8)BPMH->BpmRevocation;
    Revocation->KMSVN  = (UINT8)KM->KmSvn;
  }

  return Status;
}
