/** @file
  This header file is for Hybrid Graphics Feature PEI module.

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _HYBRID_GRAPHICS_PEI_H_
#define _HYBRID_GRAPHICS_PEI_H_

#include <Guid/H2OHgInfoHob.h>
#include <Library/GpioLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PeiHybridGraphicsInitLib.h>
#include <Library/PeiOemSvcChipsetLib.h>
#include <Library/PeiSaPolicyLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/TimerLib.h>
#include <Library/SiPolicyLib.h>
#include <Library/BaseMemoryLib.h>
#include <Ppi/HybridGraphics.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <ChipsetSetupConfig.h>
#include <HybridGraphicsDefine.h>
#include <PolicyUpdateMacro.h>
#if FixedPcdGetBool (PcdFspModeSelection) == 1
#include <FspmUpd.h>
#endif

#endif
