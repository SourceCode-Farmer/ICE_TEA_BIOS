/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <PlatformInfo.h>

EFI_STATUS
GetGopVersion (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer
  )
{
  EFI_STATUS                    Status;
  UINT32                        Version[3];
  UINT32                        Index = 0;
  UINT32                        VersionIndex =0;
  CHAR16                        TempStr;
  CHAR16                        *DriverVersion;
  UINT32                        StringLength = 0;
  EFI_LEGACY_BIOS_PROTOCOL      *LegacyBios = NULL;
  GOP_COMPONENT_NAME2_PROTOCOL  *GopComponentName2Protocol = NULL;
  VOID                          *ProtocolPointer;
  EFI_STRING_ID                 GopFwVersionString;
  EFI_STRING_ID                 GopVersionString;

  Status = gBS->LocateProtocol (&gGopComponentName2ProtocolGuid, NULL, (VOID **) &ProtocolPointer);
  if (EFI_SUCCESS != Status) {
    return Status;
  }
  //
  // Set the version default with zero
  //
  for (Index = 0; Index < 3; Index++){
    Version[Index] = 0;
  }

  Status = gBS->LocateProtocol (
                  &gEfiLegacyBiosProtocolGuid,
                  NULL,
                  (VOID **) &LegacyBios
                  );

  if (!LegacyBios) {
    Status = gBS->LocateProtocol (&gGopComponentName2ProtocolGuid, NULL, (VOID **)&GopComponentName2Protocol);
    if (!EFI_ERROR (Status)) {
      Status = GopComponentName2Protocol->GetDriverVersion (
                                            GopComponentName2Protocol,
                                            "en-US",
                                            &DriverVersion
                                            );
      if (!EFI_ERROR (Status)) {
        for (Index = 0; (DriverVersion[Index] != '\0'); Index++) {
        }
        StringLength = (Index+1)*2;
        for (Index = 0 ; Index <= StringLength ; Index++) {
          TempStr = DriverVersion[Index];
          if (TempStr == '.'){
           VersionIndex++;
          } else if ((TempStr <= '9') && (TempStr >= '0')){
            Version[VersionIndex] *= 0x10;
            Version[VersionIndex] += ((UINT32)(TempStr) - '0');
          }
        }

      UnicodeSPrint (
        StringBuffer,
        0x100,
        L"%x.%x.%x",
        (UINTN) Version[0],
        (UINTN) Version[1],
        (UINTN) Version[2]
        );

      }
    }
  }

  GopVersionString=HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_GOP_VERSION_TEXT),
    MainHiiHandle,
    &GopFwVersionString
    );

  HiiCreateTextOpCode (OpCodeHandle,GopFwVersionString, 0, GopVersionString );

  return Status;
}

