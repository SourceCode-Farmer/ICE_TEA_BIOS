//;******************************************************************************
//;* Copyright (c) 1983-2019, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
//[-start-190628-IB16990073-add]//
#include "PlatformInfo.h"

EFI_STATUS
GetLanPhyReversion (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer
  )
{
  EFI_STATUS      Status;
  EFI_STRING_ID   LanPhyReversionText;
  EFI_STRING_ID   LanPhyReversionString;

  Status = EFI_UNSUPPORTED;

  Status = NewStringToHandle (
             AdvanceHiiHandle,
             STRING_TOKEN (STR_LAN_PHY_REV_TEXT),
             MainHiiHandle,
             &LanPhyReversionText
             );

  Status = NewStringToHandle (
             AdvanceHiiHandle,
             STRING_TOKEN (STR_LAN_PHY_REV_VALUE),
             MainHiiHandle,
             &LanPhyReversionString
             );

  if (!EFI_ERROR(Status)) {
    if (NULL == HiiCreateTextOpCode (OpCodeHandle, LanPhyReversionText, 0, LanPhyReversionString)) {
      return EFI_BUFFER_TOO_SMALL;
    }
  }

  return Status;

}
//[-end-190628-IB16990073-add]//