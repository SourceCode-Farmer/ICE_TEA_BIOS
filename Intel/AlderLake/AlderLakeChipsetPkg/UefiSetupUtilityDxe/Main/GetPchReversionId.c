/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <PlatformInfo.h>
#include <Library/PchInfoLib.h>
#include <Register/PchRegs.h>
#include <Register/PchRegsLpc.h>
//[-start-191225-IB16740000-add]// for PCI_DEVICE_NUMBER_PCH_LPC & PCI_FUNCTION_NUMBER_PCH_XHCI define
#include <PchBdfAssignment.h>
//[-end-191225-IB16740000-add]//
EFI_STATUS
GetPchReversionId (
  IN    VOID                      *OpCodeHandle,
  IN    EFI_HII_HANDLE            MainHiiHandle,
  IN    EFI_HII_HANDLE            AdvanceHiiHandle,
  IN    CHAR16                    *StringBuffer
  )
{
  EFI_STATUS          Status;
  UINT8               Data8;
  UINT16              DevId;
  EFI_STRING_ID       PchReversionText;
  EFI_STRING_ID       PchReversionIdString;
  CHAR8               PchStepBuffer[PCH_STEPPING_STR_LENGTH_MAX];
//  CHAR8               PchSkuBuffer[32];
//  CHAR8               PchNameBuffer[32];
//  UINT32              BufferSize;

  Status              = EFI_SUCCESS;

  Data8 = MmioRead8 ( MmPciAddress (0, DEFAULT_PCI_BUS_NUMBER_PCH, PCI_DEVICE_NUMBER_PCH_LPC, PCI_FUNCTION_NUMBER_PCH_LPC, PCI_REVISION_ID_OFFSET));
  DevId = MmioRead16 ( MmPciAddress (0, DEFAULT_PCI_BUS_NUMBER_PCH, PCI_DEVICE_NUMBER_PCH_LPC, PCI_FUNCTION_NUMBER_PCH_LPC, PCI_DEVICE_ID_OFFSET));
//  BufferSize = sizeof (PchNameBuffer);
//  PchGetSeriesStr (PchSeries (), PchNameBuffer, &BufferSize);

//  BufferSize = sizeof (PchSkuBuffer);
//  PchGetSkuStr (DevId, PchSkuBuffer, &BufferSize);

  PchGetSteppingStr (PchStepBuffer, sizeof (PchStepBuffer));
  UnicodeSPrint (StringBuffer, 0x100, L"%02x (%a Stepping) / %a %a", (UINTN)Data8, PchStepBuffer, PchGetSeriesStr (), PchGetSkuStr ());
  PchReversionIdString=HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_PCH_REV_ID_TEXT),
    MainHiiHandle,
    &PchReversionText
  );

  HiiCreateTextOpCode (OpCodeHandle,PchReversionText, 0, PchReversionIdString );

  return Status;
}

