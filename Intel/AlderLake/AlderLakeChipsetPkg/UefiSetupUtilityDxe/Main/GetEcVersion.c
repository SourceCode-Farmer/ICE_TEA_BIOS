/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <PlatformInfo.h>
#include <PlatformBoardId.h>
#include <Library/BaseOemSvcChipsetLib.h>

EC_INFO_DISPLAY_TABLE        mDisplayEcFunction[]       = {EC_INFO_DISPLAY_TABLE_LIST};


EFI_STATUS
GetEcVersion (
  IN     VOID                      *OpCodeHandle,
  IN     EFI_HII_HANDLE            MainHiiHandle,
  IN     EFI_HII_HANDLE            AdvanceHiiHandle,
  IN     CHAR16                    *StringBuffer  )
{
  EFI_STATUS                    Status = EFI_SUCCESS;
  UINTN                         Index;

  for (Index = 0; mDisplayEcFunction[Index].DisplayEcFunction != NULL; Index++) {
    if (mDisplayEcFunction[Index].Option == DISPLAY_ENABLE) {
      ZeroMem(StringBuffer, 0x100);
      Status = mDisplayEcFunction[Index].DisplayEcFunction (OpCodeHandle, MainHiiHandle, AdvanceHiiHandle, StringBuffer);
    }
  }

  return Status;
}


EFI_STATUS
GetEcVerIdFunc (
  IN     VOID                      *OpCodeHandle,
  IN     EFI_HII_HANDLE            MainHiiHandle,
  IN     EFI_HII_HANDLE            AdvanceHiiHandle,
  IN     CHAR16                    *StringBuffer
  )
{
  EFI_STATUS          Status;
  UINT8               MajorNum;
  UINT8               MinorNum;
  EFI_STRING_ID       EcVersionText;
  EFI_STRING_ID       EcVersionString;
  EFI_STATUS          ReadEcVersionStatus;

  Status = EFI_UNSUPPORTED;
  MajorNum = 0;
  MinorNum = 0;
  ReadEcVersionStatus = EFI_UNSUPPORTED;

  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcVersion \n"));
  Status = OemSvcEcVersion (&ReadEcVersionStatus, &MajorNum, &MinorNum);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcVersion Status: %r\n", Status));

  if (!EFI_ERROR(ReadEcVersionStatus)) {
    //
    // Update EC Version to SCU
    //
    UnicodeSPrint (StringBuffer, 0x100, L"%02x.%02x", (UINTN)MajorNum, (UINTN)MinorNum);
    EcVersionString=HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);
  } else {
    //
    // Update Default EC Version to SCU
    //
    NewStringToHandle (
      AdvanceHiiHandle,
      STRING_TOKEN (STR_EC_VERSION_STRING),
      MainHiiHandle,
      &EcVersionString
      );
  }

    NewStringToHandle (
      AdvanceHiiHandle,
      STRING_TOKEN (STR_EC_VERSION_TEXT),
      MainHiiHandle,
      &EcVersionText
      );

  HiiCreateTextOpCode (OpCodeHandle,EcVersionText, 0, EcVersionString );

  return ReadEcVersionStatus;
}


EFI_STATUS
GetBoardFunc (
  IN     VOID                      *OpCodeHandle,
  IN     EFI_HII_HANDLE            MainHiiHandle,
  IN     EFI_HII_HANDLE            AdvanceHiiHandle,
  IN     CHAR16                    *StringBuffer
  )
{
  EFI_STATUS     Status = EFI_SUCCESS;
  EFI_STRING_ID  BoardIdText;
  EFI_STRING_ID  BoardIdString;
  UINT16         BoardFabId;
  CHAR16         *BoardNameString;

  BoardNameString = (CHAR16 *) PcdGetPtr (PcdBoardName);
  BoardFabId = (UINT8)PcdGet64 (PcdH2OBoardId);

  if (BoardFabId < 0x20) {
    UnicodeSPrint (StringBuffer, 0x100, L"%s (0x%02x)", BoardNameString, BoardFabId);
  } else {
    UnicodeSPrint (StringBuffer, 0x100, L"TBD (0x%02x)", BoardFabId);
  }


#ifdef POWER_ON_FLAG

#else

  switch (BoardFabId) {

    case BoardIdAdlPSimics:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-P LP DDR4 RVP (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSTgpHDdr4SODimm1DErb2:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S TGP-H DDR4 ERB2 (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSTgpHDdr4SODimm1DCrb:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S TGP-H DDR4 CRB (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSTgpHDdr4SODimm1DCrbPpv:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S TGP-H DDR4 CRB PPV (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSTgpHDdr5UDimm2DCrb:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S TGP-H DDR5 CRB (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSAdpSDdr4UDimm2DErb1:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S ADP-S DDR4 ERB1 (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S ADP-S DDR4 CRB (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSAdpSDdr4UDimm2DCrbEv:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S ADP-S DDR4 CRB EV (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S ADP-S DDR4 CRB CPV (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S ADP-S DDR5 CRB (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S ADP-S DDR5 CRB PPV (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSAdpSDdr5UDimm2DCrb:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S ADP-S DDR5 CRB (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSAdpSDdr5SODimmCrb:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S ADP-S DDR5 CRB (0x%02x)", BoardFabId);
      break;

    case BoardIdAdlSAdpSDdr5UDimm1DAep:
      UnicodeSPrint (StringBuffer, 0x100, L"AlderLake-S ADP-S DDR5 CRB (0x%02x)", BoardFabId);
      break;

    default:
      UnicodeSPrint (StringBuffer, 0x100, L"TBD (0x%02x)", BoardFabId);
      break;
  }
#endif

  BoardIdString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_BOARD_ID_TEXT),
    MainHiiHandle,
    &BoardIdText
    );

  HiiCreateTextOpCode (OpCodeHandle, BoardIdText, 0, BoardIdString );

  return Status;
}


EFI_STATUS
GetFabFunc (
  IN     VOID                      *OpCodeHandle,
  IN     EFI_HII_HANDLE            MainHiiHandle,
  IN     EFI_HII_HANDLE            AdvanceHiiHandle,
  IN     CHAR16                    *StringBuffer
  )
{
  EFI_STATUS     Status = EFI_SUCCESS;
  EFI_STRING_ID  FabIdText;
  EFI_STRING_ID  FabIdString;
  UINT16          BoardFabId;
  UINT8          FabId = 0;

  BoardFabId = PcdGet16 (PcdBoardRev);

  switch (BoardFabId) {
    case CRB_BOARD_ID_FAB_ID_FAB2:
      FabId = FAB2_ID;
      break;

    case CRB_BOARD_ID_FAB_ID_FAB3:
      FabId = FAB3_ID;
      break;

    case CRB_BOARD_ID_FAB_ID_FAB4:
      FabId = FAB4_ID;
      break;

    default:
      break;
  }

  UnicodeSPrint (StringBuffer, 0x100, L"%x", FabId);

  FabIdString = HiiSetString (MainHiiHandle, 0, StringBuffer, NULL);

  NewStringToHandle (
    AdvanceHiiHandle,
    STRING_TOKEN (STR_FAB_ID_TEXT),
    MainHiiHandle,
    &FabIdText
    );

  HiiCreateTextOpCode (OpCodeHandle, FabIdText, 0, FabIdString);

  return Status;
}
