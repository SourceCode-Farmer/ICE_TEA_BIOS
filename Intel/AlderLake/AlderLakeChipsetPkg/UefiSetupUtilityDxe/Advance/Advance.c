/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <MrcInterface.h>
#include <Advance.h>
#include <Library/PttHeciLib.h>
#include <Protocol/PciRootBridgeIo.h>
#include <OemThunderbolt.h>
#include <IndustryStandard/Pci22.h>
#include <ITbtInfoHob.h>
#include <Include/Guid/HobList.h>
#include <Include/Library/HobLib.h>
#include <Library/MmPciLib.h>
#include <Library/TbtCommonLib.h>
#include <Library/PchPcieRpLib.h>
#include "OverClockSetup.h"
#include "PlatformSetup.h"
#include "SaSetup.h"
#include "EcSetup.h"
#include "MeSetup.h"
#include <Library/ConfigBlockLib.h>
#include <Library/PcdLib.h>

IDE_CONFIG                               *mIdeConfig;
EFI_CALLBACK_INFO                         *mAdvCallBackInfo;
ADVANCE_CONFIGURATION                     mAdvConfig;


/**
 This is the callback function for the Advance Menu.

 @param [in]   This
 @param [in]   Action
 @param [in]   QuestionId
 @param [in]   Type
 @param [in]   Value
 @param [out]  ActionRequest


**/
EFI_STATUS
AdvanceCallbackRoutine (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  EFI_BROWSER_ACTION                     Action,
  IN  EFI_QUESTION_ID                        QuestionId,
  IN  UINT8                                  Type,
  IN  EFI_IFR_TYPE_VALUE                     *Value,
  OUT EFI_BROWSER_ACTION_REQUEST             *ActionRequest
  )
{
  EFI_STATUS                            Status;
  CHAR16                                *StringPtr;
  CHIPSET_CONFIGURATION                 *MyIfrNVData;
  EFI_HII_HANDLE                        HiiHandle;
  EFI_CALLBACK_INFO                     *CallbackInfo;
  EFI_INPUT_KEY                         Key;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo;
  UINT8                                 DeviceKind;
  UINTN                                 BufferSize;
  EFI_GUID                              VarStoreGuid = SYSTEM_CONFIGURATION_GUID;
  UINT8                                 CmosPlatformSetting = 0;
  SETUP_DATA                            *MySetupDataIfrNVData;
  SA_SETUP                              *MySaIfrNVData;
  ME_SETUP                              *MyMeIfrNVData;
  CPU_SETUP                             *MyCpuIfrNVData;
  PCH_SETUP                             *MyPchIfrNVData;
  SI_SETUP                              *MySiIfrNVData;
  ME_SETUP_STORAGE                      *MyMeStorageIfrNVData;

  if (Action != EFI_BROWSER_ACTION_CHANGED) {
    return AdvanceCallbackRoutineByAction (This, Action, QuestionId, Type, Value, ActionRequest);
  }

  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;
  CallbackInfo = EFI_CALLBACK_INFO_FROM_THIS (This);
  BufferSize = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemConfig");

  Status = SetupVariableConfig (
             &VarStoreGuid,
             L"SystemConfig",
             BufferSize,
             (UINT8 *) gSUBrowser->SCBuffer,
             TRUE
             );
  Status = SetupVariableConfig (
             &VarStoreGuid,
             L"AdvanceConfig",
             sizeof (mAdvConfig),
             (UINT8 *) &mAdvConfig,
             TRUE
             );
  Status = SetupVariableConfig (
             &gSetupVariableGuid,
             PLATFORM_SETUP_VARIABLE_NAME,
             sizeof (SETUP_DATA),
             (UINT8 *) gRcSUBrowser->SetupDataSUBrowserData,
             TRUE
             );
  Status = SetupVariableConfig (
             &gSaSetupVariableGuid,
             SA_SETUP_VARIABLE_NAME,
             sizeof (SA_SETUP),
             (UINT8 *) gRcSUBrowser->SaSUBrowserData,
             TRUE
             );
  Status = SetupVariableConfig (
             &gMeSetupVariableGuid,
             ME_SETUP_VARIABLE_NAME,
             sizeof (ME_SETUP),
             (UINT8 *) gRcSUBrowser->MeSUBrowserData,
             TRUE
             );
  Status = SetupVariableConfig (
             &gCpuSetupVariableGuid,
             CPU_SETUP_VARIABLE_NAME,
             sizeof (CPU_SETUP),
             (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
             TRUE
             );
  Status = SetupVariableConfig (
             &gPchSetupVariableGuid,
             PCH_SETUP_VARIABLE_NAME,
             sizeof (PCH_SETUP),
             (UINT8 *) gRcSUBrowser->PchSUBrowserData,
             TRUE
             );
  Status = SetupVariableConfig (
             &gSiSetupVariableGuid,
             SI_SETUP_VARIABLE_NAME,
             sizeof (SI_SETUP),
             (UINT8 *) gRcSUBrowser->SiSUBrowserData,
             TRUE
             );
  Status = SetupVariableConfig (
             &gMeSetupVariableGuid,
             ME_SETUP_STORAGE_VARIABLE_NAME,
             sizeof (ME_SETUP_STORAGE),
             (UINT8 *) gRcSUBrowser->MeStorageSUBrowserData,
             TRUE
             );
  MyIfrNVData = (CHIPSET_CONFIGURATION *)gSUBrowser->SCBuffer;
  Status    = EFI_SUCCESS;
  StringPtr = NULL;
  SUCInfo   = gSUBrowser->SUCInfo;

  HiiHandle = CallbackInfo->HiiHandle;

  MySetupDataIfrNVData = (SETUP_DATA *)gRcSUBrowser->SetupDataSUBrowserData;
  MySaIfrNVData        = (SA_SETUP *)gRcSUBrowser->SaSUBrowserData;
  MyMeIfrNVData        = (ME_SETUP *)gRcSUBrowser->MeSUBrowserData;
  MyCpuIfrNVData       = (CPU_SETUP *)gRcSUBrowser->CpuSUBrowserData;
  MyPchIfrNVData       = (PCH_SETUP *)gRcSUBrowser->PchSUBrowserData;
  MySiIfrNVData        = (SI_SETUP *)gRcSUBrowser->SiSUBrowserData;
  MyMeStorageIfrNVData = (ME_SETUP_STORAGE *)gRcSUBrowser->MeStorageSUBrowserData;

  switch (QuestionId) {
  //=======================//
  // KEY_AHCI_OPROM_CONFIG //
  //=======================//
  case KEY_AHCI_OPROM_CONFIG:
    if (MyPchIfrNVData->SataInterfaceMode == SATA_MODE_AHCI || MyPchIfrNVData->SataInterfaceMode == SATA_MODE_RAID) {
      MyIfrNVData->AhciOptionRomSupport = TRUE;
    }else {
      MyIfrNVData->AhciOptionRomSupport = FALSE;
    }
    break;
  //========================================================================//
  // KEY_SERIAL_PORTA, KEY_SERIAL_PORTA_BASE_IO, KEY_SERIAL_PORTA_INTERRUPT //
  // KEY_SERIAL_PORTB, KEY_SERIAL_PORTB_BASE_IO, KEY_SERIAL_PORTB_INTERRUPT //
  //========================================================================//
  case KEY_SERIAL_PORTA:
  case KEY_SERIAL_PORTA_BASE_IO:
  case KEY_SERIAL_PORTA_INTERRUPT:
  case KEY_SERIAL_PORTB:
  case KEY_SERIAL_PORTB_BASE_IO:
  case KEY_SERIAL_PORTB_INTERRUPT:
    //
    //  Check IRQ conflict between serial port and pci solt.
    //
    if (CheckSioAndPciSoltConflict (MyIfrNVData, (UINT8 *)&(MyIfrNVData->PciSlot3), &DeviceKind)) {
        StringPtr=HiiGetString (HiiHandle, STRING_TOKEN(ISA_AND_PCISOLT_CONFLICT_STRING), NULL);
      gSUBrowser->H2ODialog->ConfirmDialog (2, FALSE, 0, NULL, &Key, StringPtr);
      gBS->FreePool (StringPtr);
      switch (DeviceKind) {

      case 0:
        MyIfrNVData->ComPortA = 0;
        break;

      case 1:
        MyIfrNVData->ComPortB = 0;
        break;
      }
    }
    if (CheckSioConflict (MyIfrNVData)) {
      StringPtr=HiiGetString (HiiHandle, STRING_TOKEN(CONFLICT_STRING), NULL);
      gSUBrowser->H2ODialog->ConfirmDialog (
                               2,
                               FALSE,
                               0,
                               NULL,
                               &Key,
                               StringPtr
                               );
      gBS->FreePool (StringPtr);
    }
    break;
  //==============================================//
  // KEY_PCI_SLOT3_IRQ_SET, KEY_PCI_SLOT4_IRQ_SET //
  //==============================================//
  case KEY_PCI_SLOT3_IRQ_SET:
  case KEY_PCI_SLOT4_IRQ_SET:
    if (CheckPciSioConflict (MyIfrNVData, (UINT8 *)&(MyIfrNVData->PciSlot3), &DeviceKind)) {
      switch (DeviceKind) {

      case 1:
        StringPtr=HiiGetString (HiiHandle, STRING_TOKEN(COMPORTA_CONFLICT_STRING), NULL);
        break;

      case 2:
        StringPtr=HiiGetString (HiiHandle, STRING_TOKEN(COMPORTB_CONFLICT_STRING), NULL);
        break;

      }
      gSUBrowser->H2ODialog->ConfirmDialog (2, FALSE, 0, NULL, &Key, StringPtr);
      gBS->FreePool (StringPtr);
    }
    break;
  //=============================//
  // KEY_CHIPSET_EXTENDED_CONFIG //
  //=============================//
  case KEY_CHIPSET_EXTENDED_CONFIG:
    if (MyIfrNVData->ExtendedConfig == 0) {
      MyIfrNVData->SdRamFrequency    = 0;
      MyIfrNVData->SdRamTimeCtrl     = 0;
      MyIfrNVData->SdRamRasActToPre  = 6;
      MyIfrNVData->SdRamCasLatency   = 25;
      MyIfrNVData->SdRamRasCasDelay  = 3;
      MyIfrNVData->SdRamRasPrecharge = 3;
    }
    break;
  //=============================//
  // KEY_CHIPSET_SDRAM_TIME_CTRL //
  //=============================//
  case KEY_CHIPSET_SDRAM_TIME_CTRL:
    switch (MyIfrNVData->SdRamTimeCtrl) {

    case 0:         // AUTO
      MyIfrNVData->SdRamRasActToPre = 6;
      MyIfrNVData->SdRamCasLatency = 25;
      MyIfrNVData->SdRamRasCasDelay = 3;
      MyIfrNVData->SdRamRasPrecharge = 3;
      break;

    case 1:         // MANUAL_AGRESSIVE
      MyIfrNVData->SdRamRasActToPre  = 5;
      MyIfrNVData->SdRamCasLatency   = 20;
      MyIfrNVData->SdRamRasCasDelay  = 2;
      MyIfrNVData->SdRamRasPrecharge = 2;
      break;
    }
    SUCInfo->DoRefresh = TRUE;
    break;
  //========================//
  // KEY_SATA_CNFIGURE_MODE //
  //========================//
  case KEY_SATA_CNFIGURE_MODE :
    UpdateHDCConfigure (
      HiiHandle,
      MyIfrNVData
      );
    break;
  //===============================================//
  // KEY_ICC_RATIO2, KEY_ICC_FREQ2, KEY_ICC_FREQ3, //
  // KEY_ICC_SPREAD2, KEY_ICC_SPREAD3,             //
  // KEY_BCLK_RFI_FREQ0, KEY_BCLK_RFI_FREQ1,       //
  // KEY_BCLK_RFI_FREQ2, KEY_BCLK_RFI_FREQ3        //
  //===============================================//
  case KEY_ICC_RATIO2:
  case KEY_ICC_FREQ2:
  case KEY_ICC_FREQ3:
  case KEY_ICC_SPREAD2:
  case KEY_ICC_SPREAD3:
  case KEY_BCLK_RFI_FREQ0:
  case KEY_BCLK_RFI_FREQ1:
  case KEY_BCLK_RFI_FREQ2:
  case KEY_BCLK_RFI_FREQ3:
    //
    // 1. Insyde For compatible to old form browser which only use EFI_BROWSER_ACTION_CHANGING action,
    // change action to EFI_BROWSER_ACTION_CHANGED to make it workable.
    // 2. Intel Platofrm code use EFI_BROWSER_ACTION_CHANGING, We need to transfer back temperary.
    //
    if (Action == EFI_BROWSER_ACTION_CHANGED) {
      Action = EFI_BROWSER_ACTION_CHANGING;
    }
    IccCallback (This, Action, QuestionId, Type, Value, ActionRequest);
    if (Action == EFI_BROWSER_ACTION_CHANGING) {
      Action = EFI_BROWSER_ACTION_CHANGED;
    }
    break;
  //=======================//
  // KEY_LOW_POWER_S0_IDLE //
  //=======================//
  case KEY_LOW_POWER_S0_IDLE:
//    if (Action == EFI_BROWSER_ACTION_CHANGED) {
//      Action = EFI_BROWSER_ACTION_CHANGING;
//    }
    LowPowerS0IdleEnableCallback (This, Action, QuestionId, Type, Value, ActionRequest);
//    Status = SetupVariableConfig (
//               &VarStoreGuid,
//               L"SystemConfig",
//               BufferSize,
//               (UINT8 *) gSUBrowser->SCBuffer,
//               TRUE
//               );
//    if (Action == EFI_BROWSER_ACTION_CHANGING) {
//      Action = EFI_BROWSER_ACTION_CHANGED;
//    }
    break;
  //============================================//
  // KEY_XeVoltage, KEY_RING_MAX_OC_RATIO_LIMIT //
  // KEY_TjMaxOffset, KEY_RING_MIN_RATIO_LIMIT  //
  // KEY_RING_MAX_RATIO_LIMIT                   //
  // KEY_CORE_MAX_OC_RATIO_LIMIT                //
  //============================================//
  case KEY_XeVoltage:
  case KEY_RING_MAX_OC_RATIO_LIMIT:
  case KEY_TjMaxOffset:
  case KEY_RING_MIN_RATIO_LIMIT:
  case KEY_RING_MAX_RATIO_LIMIT:
  case KEY_CORE_MAX_OC_RATIO_LIMIT:
    OcFormCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  //=================//
  // KEY_ICC_PROFILE //
  //=================//
  case KEY_ICC_PROFILE:
    //
    // 1. Insyde For compatible to old form browser which only use EFI_BROWSER_ACTION_CHANGING action,
    // change action to EFI_BROWSER_ACTION_CHANGED to make it workable.
    // 2. Intel Platofrm code use EFI_BROWSER_ACTION_CHANGING, We need to transfer back temperary.
    //
    if (Action == EFI_BROWSER_ACTION_CHANGED) {
      Action = EFI_BROWSER_ACTION_CHANGING;
    }
    IccProfileCallback (This, Action, QuestionId, Type, Value, ActionRequest);
    if (Action == EFI_BROWSER_ACTION_CHANGING) {
      Action = EFI_BROWSER_ACTION_CHANGED;
    }
    break;
  //========================================================================//
  // KEY_TBT_SUPPORT, KEY_TBT_BOOT_ON, KEY_TBT_USB_ON, KEY_TBT_HOSTROUTER   //
  // KEY_DTBT_CONTROLLER, KEY_TBT_NATIVE_OS_HOTPLUG, TBT_SECURITY_LEVEL_KEY //
  //========================================================================//
  case KEY_DTBT_SUPPORT:
  case KEY_DTBT_CONTROLLER0_HOSTROUTER:
  case KEY_DTBT_CONTROLLER0:
  case KEY_DTBT_CONTROLLER1_HOSTROUTER:
  case KEY_DTBT_CONTROLLER1:
  case KEY_DTBT_CONTROLLER2:
  case KEY_ITBT_SUPPORT:
//[-start-210330-IB17800121-modify]//
  //=========================================================//
  // KEY_DTBT_CONTROLLER_TYPE_0,KEY_DTBT_CONTROLLER_TYPE_1   //
  // KEY_DTBT_CONTROLLER_TYPE_2, KEY_DTBT_CONTROLLER_TYPE_3  //
  //=========================================================//
  case KEY_DTBT_CONTROLLER_TYPE_0:
  case KEY_DTBT_CONTROLLER_TYPE_1:
  case KEY_DTBT_CONTROLLER_TYPE_2:
  case KEY_DTBT_CONTROLLER_TYPE_3:
    Status = DTbtFormCallBackFunction(
               This,
               Action,
               QuestionId,
               Type,
               Value,
               ActionRequest
               );
//[-end-210330-IB17800121-modify]//
    break;

  case KEY_TBT_OS_SELECTOR:
    Status = TbtOsSelectorFormCallBackFunction(
               This,
               Action,
               QuestionId,
               Type,
               Value,
               ActionRequest
               );
    break;

  //======================//
  // KEY_DUAL_VGA_SUPPORT //
  //======================//
  case KEY_DUAL_VGA_SUPPORT:
    if (MyIfrNVData->UefiDualVgaControllers == DUAL_VGA_CONTROLLER_ENABLE) {
      //
      // Set PrimaryDisplay Mode IGD, and enable the IGD.
      // Always enable PEG detection.
      //
      MySaIfrNVData->PrimaryDisplay = 0;
      MySaIfrNVData->InternalGraphics = 1;
    }

    if (MyIfrNVData->UefiDualVgaControllers == DUAL_VGA_CONTROLLER_DISABLE) {
      //
      // Set PrimaryDisplay Mode AUTO, and set IGD enable in AUTO mode.
      // disable PEG detection.
      //
      MySaIfrNVData->PrimaryDisplay = 3;
      MySaIfrNVData->InternalGraphics = 2;
    }
    break;
  //================================//
  // KEY_PLUG_IN_DISPLAY_SELECTION1 //
  // KEY_PLUG_IN_DISPLAY_SELECTION2 //
  //================================//
  case KEY_PLUG_IN_DISPLAY_SELECTION1:
  case KEY_PLUG_IN_DISPLAY_SELECTION2:
    PlugInVideoDisplaySelectionOption (QuestionId, Value);
    break;
  //===================================//
  // KEY_IGD_PRIMARY_DISPLAY_SELECTION //
  //===================================//
  case KEY_IGD_PRIMARY_DISPLAY_SELECTION:
    if (Value->u8 == SCU_IGD_BOOT_TYPE_VBIOS_DEFAULT) {
      MyIfrNVData->IGDBootTypeSecondary = SCU_IGD_BOOT_TYPE_DISABLE;
    }
    break;
  //================//
  // THERMAL REGION //
  //================//
  case THERMAL_SENSOR_1_KEY:
  case THERMAL_SENSOR_2_KEY:
  case THERMAL_SENSOR_3_KEY:
  case THERMAL_SENSOR_4_KEY:
  case THERMAL_SENSOR_5_KEY:
  case CPU_FAN_KEY :
  case PCH_DTS_TEMP_KEY :
  case TS_ON_DIMM0_TEMP_KEY :
  case TS_ON_DIMM1_TEMP_KEY :
    HhmMobileCallBack (
      HiiHandle,
      ADVANCED_FORM_SET_CLASS,
      0,
      QuestionId
      );
    break;
  //======================//
  // KEY_DVMT_PREALLOCATE //
  //======================//
  case KEY_DVMT_PREALLOCATE:
    CmosPlatformSetting = ReadExtCmos8 ( R_XCMOS_INDEX, R_XCMOS_DATA, PlatformSettingFlag );
    CmosPlatformSetting = CmosPlatformSetting | B_SETTING_MEM_REFRESH_FLAG;
    WriteExtCmos8 ( R_XCMOS_INDEX, R_XCMOS_DATA, PlatformSettingFlag, CmosPlatformSetting );
    break;
  //===================//
  // KEY_APERTURE_SIZE //
  //===================//
  case KEY_APERTURE_SIZE:
    CmosPlatformSetting = ReadExtCmos8 ( R_XCMOS_INDEX, R_XCMOS_DATA, PlatformSettingFlag );
    CmosPlatformSetting = CmosPlatformSetting | B_SETTING_MEM_REFRESH_FLAG;
    WriteExtCmos8 ( R_XCMOS_INDEX, R_XCMOS_DATA, PlatformSettingFlag, CmosPlatformSetting );
    break;
  //============================================//
  // KEY_RatioLimitRatio0~7                     //
  // KEY_RatioLimitNumCore0~7                   //
  // KEY_AtomRatioLimitRatio0~7                 //
  // KEY_AtomRatioLimitNumCore0~7               //
  // KEY_CpuRatioLimit                          //
  //============================================//
  case KEY_RatioLimitRatio0:
  case KEY_RatioLimitRatio1:
  case KEY_RatioLimitRatio2:
  case KEY_RatioLimitRatio3:
  case KEY_RatioLimitRatio4:
  case KEY_RatioLimitRatio5:
  case KEY_RatioLimitRatio6:
  case KEY_RatioLimitRatio7:
  case KEY_RatioLimitNumCore0:
  case KEY_RatioLimitNumCore1:
  case KEY_RatioLimitNumCore2:
  case KEY_RatioLimitNumCore3:
  case KEY_RatioLimitNumCore4:
  case KEY_RatioLimitNumCore5:
  case KEY_RatioLimitNumCore6:
  case KEY_RatioLimitNumCore7:
  case KEY_AtomRatioLimitRatio0:
  case KEY_AtomRatioLimitRatio1:
  case KEY_AtomRatioLimitRatio2:
  case KEY_AtomRatioLimitRatio3:
  case KEY_AtomRatioLimitRatio4:
  case KEY_AtomRatioLimitRatio5:
  case KEY_AtomRatioLimitRatio6:
  case KEY_AtomRatioLimitRatio7:
  case KEY_AtomRatioLimitNumCore0:
  case KEY_AtomRatioLimitNumCore1:
  case KEY_AtomRatioLimitNumCore2:
  case KEY_AtomRatioLimitNumCore3:
  case KEY_AtomRatioLimitNumCore4:
  case KEY_AtomRatioLimitNumCore5:
  case KEY_AtomRatioLimitNumCore6:
  case KEY_AtomRatioLimitNumCore7:
  case KEY_CpuRatioLimit:
    Status = CpuFormCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  //=================//
  // KEY_EpochChange //
  //=================//
//[-start-200420-IB17800056-remove]//
    // Sgx be removed.
#if 0
  case KEY_EpochChange:
    Status = EpochChangeCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  //===============//
  // KEY_SgxEpoch1 //
  //===============//
  case KEY_SgxEpoch1:
    Status = SgxEpoch1CallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  //===============//
  // KEY_SgxChange //
  //===============//
  case KEY_SgxChange:
    Status = SgxFormCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
#endif
//[-end-200420-IB17800056-remove]//
  //==================================//
  // MNG_STATE_KEY                    //
  // PTT_TRIGGER_FORM_OPEN_ACTION_KEY //
  // DAM_STATE_KEY                    //
  //==================================//
  case MNG_STATE_KEY:
  case PTT_TRIGGER_FORM_OPEN_ACTION_KEY:
  case DAM_STATE_KEY:
    Status = MeFormCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
//  //==================//
//  // PTT_ENABLE_LABEL //
//  //==================//
//  case PTT_ENABLE_LABEL :
//    if (FeaturePcdGet(PcdPttSupported)) {
//      if (MyIfrNVData->PTTEnable == PTT_ENABLE) {
//        Status = PttHeciSetState(TRUE);
//      } else {
//        Status = PttHeciSetState(FALSE);
//      }
//    }
//    break;
//  //========================//
//  // PTT_REVOKE_TRUST_LABEL //
//  //========================//
//  case PTT_REVOKE_TRUST_LABEL :
//    StringPtr = HiiGetString (HiiHandle, STRING_TOKEN (STR_PTT_REVOKE_TRUST_WARNING_STRING), NULL);
//    gSUBrowser->H2ODialog->ConfirmDialog (
//                             2,
//                             FALSE,
//                             0,
//                             NULL,
//                             &Key,
//                             StringPtr
//                             );
//    break;
  //============================//
  // PLATFORM_DEBUG_CONSENT_KEY //
  //============================//
  case PLATFORM_DEBUG_CONSENT_KEY:
    Status = DebugFormCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  //====================//
  // KEY_TXT_POLICY_FIT //
  //====================//
  case KEY_TXT_POLICY_FIT:
    Status = TxtPolicyCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  //================================================//
  // KEY_CNV_BT_AUDIO_OFFLOAD, KEY_WIFI_POWER_LIMIT //
  //================================================//
  case KEY_CNV_BT_AUDIO_OFFLOAD:
  case KEY_WIFI_POWER_LIMIT:
    Status = CnvFormCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;

  //=====================//
  // KEY_MEMINFO_PROFILE //
  //=====================//
  case KEY_MEMINFO_PROFILE:
    Status = OcFormMemoryTimingCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  //========================//
  // KEY_ENABLE_THERMAL_FUN //
  //========================//
  case KEY_ENABLE_THERMAL_FUN:
    Status = ThermalFunctionCallback (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  //=================================//
  // KEY_SdevXhciAcpiPathNameDevice1 //
  // KEY_SdevXhciAcpiPathNameDevice2 //
  //=================================//
  case KEY_SdevXhciAcpiPathNameDevice1:
  case KEY_SdevXhciAcpiPathNameDevice2:
    Status = VtioFormCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  //========================//
  // KEY_PCH_STATE_AFTER_G3 //
  //========================//
  case KEY_PCH_STATE_AFTER_G3:
    Status = StateAfterG3CallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;

  case KEY_VrPowerDeliveryChange:
    Status = CpuVrConfigCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;

  case KEY_SA_VMD_GLOBAL_MAPPING:
    Status = VmdCallback (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  case RTD3_SUPPORT_QUESTION_ID:
    Status = Rtd3SupportCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
    break;
  //=========//
  // default //
  //=========//
  default:
    Status = HotKeyCallBack (
               This,
               Action,
               QuestionId,
               Type,
               Value,
               ActionRequest
               );
    break;
  }

  SetupVariableConfig (
    &VarStoreGuid,
    L"SystemConfig",
    BufferSize,
    (UINT8 *) gSUBrowser->SCBuffer,
    FALSE
    );

  SetupVariableConfig (
    &VarStoreGuid,
    L"AdvanceConfig",
    sizeof (mAdvConfig),
    (UINT8 *) &mAdvConfig,
    FALSE
    );
  SetupVariableConfig (
    &gSetupVariableGuid,
    PLATFORM_SETUP_VARIABLE_NAME,
    sizeof (SETUP_DATA),
    (UINT8 *) gRcSUBrowser->SetupDataSUBrowserData,
    FALSE
    );
  SetupVariableConfig (
    &gSaSetupVariableGuid,
    SA_SETUP_VARIABLE_NAME,
    sizeof (SA_SETUP),
    (UINT8 *) gRcSUBrowser->SaSUBrowserData,
    FALSE
    );
  SetupVariableConfig (
    &gMeSetupVariableGuid,
    ME_SETUP_VARIABLE_NAME,
    sizeof (ME_SETUP),
    (UINT8 *) gRcSUBrowser->MeSUBrowserData,
    FALSE
    );
  SetupVariableConfig (
    &gCpuSetupVariableGuid,
    CPU_SETUP_VARIABLE_NAME,
    sizeof (CPU_SETUP),
    (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
    FALSE
    );
  SetupVariableConfig (
    &gPchSetupVariableGuid,
    PCH_SETUP_VARIABLE_NAME,
    sizeof (PCH_SETUP),
    (UINT8 *) gRcSUBrowser->PchSUBrowserData,
    FALSE
    );
  SetupVariableConfig (
    &gSiSetupVariableGuid,
    SI_SETUP_VARIABLE_NAME,
    sizeof (SI_SETUP),
    (UINT8 *) gRcSUBrowser->SiSUBrowserData,
    FALSE
    );
  SetupVariableConfig (
    &gMeSetupVariableGuid,
    ME_SETUP_STORAGE_VARIABLE_NAME,
    sizeof (ME_SETUP_STORAGE),
    (UINT8 *) gRcSUBrowser->MeStorageSUBrowserData,
    FALSE
    );
  return Status;
}

EFI_STATUS
AdvanceCallbackRoutineByAction (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  EFI_BROWSER_ACTION                     Action,
  IN  EFI_QUESTION_ID                        QuestionId,
  IN  UINT8                                  Type,
  IN  EFI_IFR_TYPE_VALUE                     *Value,
  OUT EFI_BROWSER_ACTION_REQUEST             *ActionRequest
  )
{
  EFI_STATUS                            Status;
  EFI_CALLBACK_INFO                     *CallbackInfo;
  UINTN                                 BufferSize;
  EFI_GUID                              VarStoreGuid = SYSTEM_CONFIGURATION_GUID;

  if ((This == NULL) ||
      ((Value == NULL) &&
       (Action != EFI_BROWSER_ACTION_FORM_OPEN) &&
       (Action != EFI_BROWSER_ACTION_FORM_CLOSE))||
      (ActionRequest == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;
  CallbackInfo   = EFI_CALLBACK_INFO_FROM_THIS (This);
  BufferSize     = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemConfig");
  Status         = EFI_UNSUPPORTED;

  switch (Action) {

  case EFI_BROWSER_ACTION_FORM_OPEN:
    if (QuestionId == 0) {
      Status = SetupVariableConfig (
                 &VarStoreGuid,
                 L"SystemConfig",
                 BufferSize,
                 (UINT8 *) gSUBrowser->SCBuffer,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &VarStoreGuid,
                 L"AdvanceConfig",
                 sizeof (mAdvConfig),
                 (UINT8 *) &mAdvConfig,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gSetupVariableGuid,
                 PLATFORM_SETUP_VARIABLE_NAME,
                 sizeof (SETUP_DATA),
                 (UINT8 *) gRcSUBrowser->SetupDataSUBrowserData,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gSaSetupVariableGuid,
                 SA_SETUP_VARIABLE_NAME,
                 sizeof (SA_SETUP),
                 (UINT8 *) gRcSUBrowser->SaSUBrowserData,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gMeSetupVariableGuid,
                 ME_SETUP_VARIABLE_NAME,
                 sizeof (ME_SETUP),
                 (UINT8 *) gRcSUBrowser->MeSUBrowserData,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gCpuSetupVariableGuid,
                 CPU_SETUP_VARIABLE_NAME,
                 sizeof (CPU_SETUP),
                 (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gPchSetupVariableGuid,
                 PCH_SETUP_VARIABLE_NAME,
                 sizeof (PCH_SETUP),
                 (UINT8 *) gRcSUBrowser->PchSUBrowserData,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gSiSetupVariableGuid,
                 SI_SETUP_VARIABLE_NAME,
                 sizeof (SI_SETUP),
                 (UINT8 *) gRcSUBrowser->SiSUBrowserData,
                 FALSE
                 );
      Status = SetupVariableConfig (
                 &gMeSetupVariableGuid,
                 ME_SETUP_STORAGE_VARIABLE_NAME,
                 sizeof (ME_SETUP_STORAGE),
                 (UINT8 *) gRcSUBrowser->MeStorageSUBrowserData,
                 FALSE
                 );
      Status = SyncIccSetupDataWithFormBrowser (Action);
//[-start-200420-IB17800056-remove]//
      // Sgx be removed.
//      Status = SgxFormCallBackFunction (This, Action, QuestionId, Type, Value, ActionRequest);
//[-end-200420-IB17800056-remove]//
    }
    break;

  case EFI_BROWSER_ACTION_FORM_CLOSE:
    if (QuestionId == 0) {
      Status = SetupVariableConfig (
                 &VarStoreGuid,
                 L"SystemConfig",
                 BufferSize,
                 (UINT8 *) gSUBrowser->SCBuffer,
                 TRUE
                 );
      Status = SetupVariableConfig (
                 &VarStoreGuid,
                 L"AdvanceConfig",
                 sizeof (mAdvConfig),
                 (UINT8 *) &mAdvConfig,
                 TRUE
                 );
      Status = SetupVariableConfig (
                 &gSetupVariableGuid,
                 PLATFORM_SETUP_VARIABLE_NAME,
                 sizeof (SETUP_DATA),
                 (UINT8 *) gRcSUBrowser->SetupDataSUBrowserData,
                 TRUE
                 );

      Status = SetupVariableConfig (
                 &gSaSetupVariableGuid,
                 SA_SETUP_VARIABLE_NAME,
                 sizeof (SA_SETUP),
                 (UINT8 *) gRcSUBrowser->SaSUBrowserData,
                 TRUE
                 );
      Status = SetupVariableConfig (
                 &gMeSetupVariableGuid,
                 ME_SETUP_VARIABLE_NAME,
                 sizeof (ME_SETUP),
                 (UINT8 *) gRcSUBrowser->MeSUBrowserData,
                 TRUE
                 );
      Status = SetupVariableConfig (
                 &gCpuSetupVariableGuid,
                 CPU_SETUP_VARIABLE_NAME,
                 sizeof (CPU_SETUP),
                 (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
                 TRUE
                 );
      Status = SetupVariableConfig (
                 &gPchSetupVariableGuid,
                 PCH_SETUP_VARIABLE_NAME,
                 sizeof (PCH_SETUP),
                 (UINT8 *) gRcSUBrowser->PchSUBrowserData,
                 TRUE
                 );
      Status = SetupVariableConfig (
                 &gSiSetupVariableGuid,
                 SI_SETUP_VARIABLE_NAME,
                 sizeof (SI_SETUP),
                 (UINT8 *) gRcSUBrowser->SiSUBrowserData,
                 TRUE
                 );
      Status = SetupVariableConfig (
                 &gMeSetupVariableGuid,
                 ME_SETUP_STORAGE_VARIABLE_NAME,
                 sizeof (ME_SETUP_STORAGE),
                 (UINT8 *) gRcSUBrowser->MeStorageSUBrowserData,
                 TRUE
                 );
      Status = SyncIccSetupDataWithFormBrowser (Action);
    }
    break;

  case EFI_BROWSER_ACTION_CHANGING:
    Status = EFI_SUCCESS;
    break;

  case EFI_BROWSER_ACTION_DEFAULT_MANUFACTURING:
    if (QuestionId == KEY_SCAN_F9) {
      Status = HotKeyCallBack (
                This,
                Action,
                QuestionId,
                Type,
                Value,
                ActionRequest
                );
      SetupVariableConfig (
        &VarStoreGuid,
        L"SystemConfig",
        BufferSize,
        (UINT8 *) gSUBrowser->SCBuffer,
        FALSE
        );
      SetupVariableConfig (
        &gSetupVariableGuid,
        PLATFORM_SETUP_VARIABLE_NAME,
        sizeof (SETUP_DATA),
        (UINT8 *) gRcSUBrowser->SetupDataSUBrowserData,
        FALSE
        );
      SetupVariableConfig (
        &gSaSetupVariableGuid,
        SA_SETUP_VARIABLE_NAME,
        sizeof (SA_SETUP),
        (UINT8 *) gRcSUBrowser->SaSUBrowserData,
        FALSE
        );
      SetupVariableConfig (
        &gMeSetupVariableGuid,
        ME_SETUP_VARIABLE_NAME,
        sizeof (ME_SETUP),
        (UINT8 *) gRcSUBrowser->MeSUBrowserData,
        FALSE
        );
      SetupVariableConfig (
        &gCpuSetupVariableGuid,
        CPU_SETUP_VARIABLE_NAME,
        sizeof (CPU_SETUP),
        (UINT8 *) gRcSUBrowser->CpuSUBrowserData,
        FALSE
        );
      SetupVariableConfig (
        &gPchSetupVariableGuid,
        PCH_SETUP_VARIABLE_NAME,
        sizeof (PCH_SETUP),
        (UINT8 *) gRcSUBrowser->PchSUBrowserData,
        FALSE
        );
      SetupVariableConfig (
        &gSiSetupVariableGuid,
        SI_SETUP_VARIABLE_NAME,
        sizeof (SI_SETUP),
        (UINT8 *) gRcSUBrowser->SiSUBrowserData,
        FALSE
        );
      SetupVariableConfig (
        &gMeSetupVariableGuid,
        ME_SETUP_STORAGE_VARIABLE_NAME,
        sizeof (ME_SETUP_STORAGE),
        (UINT8 *) gRcSUBrowser->MeStorageSUBrowserData,
        FALSE
        );
      Status = SyncIccSetupDataWithFormBrowser (Action);
    }
    //
    // avoid GetQuestionDefault execute ExtractConfig
    //
    return EFI_SUCCESS;

  default:
    break;
  }

  return Status;
}


EFI_STATUS
UpdateAdvanceDevicePathStr (
  IN EFI_HII_HANDLE                         HiiHandle
  )
{
  EFI_STATUS                                Status;
  EFI_HANDLE                                AdvanceDriverHandle;
  EFI_DEVICE_PATH_PROTOCOL                  *AdvanceDevicePath;
  CHAR16                                    *AdvanceDevicePathStr;
  EFI_HANDLE                                PowerHandle;

  AdvanceDriverHandle = gSUBrowser->SUCInfo->MapTable[AdvanceHiiHandle].DriverHandle;
  PowerHandle         = gSUBrowser->SUCInfo->MapTable[PowerHiiHandle].HiiHandle;
  Status              = gBS->HandleProtocol (AdvanceDriverHandle, &gEfiDevicePathProtocolGuid, (VOID **) &AdvanceDevicePath);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  AdvanceDevicePathStr = ConvertDevicePathToText (AdvanceDevicePath, FALSE, FALSE);
  if (AdvanceDevicePathStr == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Update Advance driver device path to Power Hii handle.
  // It will connect Power->Power&Performance->GT-PowerManagementControl->RC6(Rander Standby).
  //
  HiiSetString (PowerHandle, STRING_TOKEN (STR_SCU_GT_MENU_DEVICE_PATH), AdvanceDevicePathStr, NULL);
  FreePool (AdvanceDevicePathStr);

  return EFI_SUCCESS;
}


EFI_STATUS
InstallAdvanceCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  )
{
  EFI_STATUS                                Status;
  EFI_GUID                                  FormsetGuid = FORMSET_ID_GUID_ADVANCE;

  mAdvCallBackInfo = AllocatePool (sizeof(EFI_CALLBACK_INFO));
  if (mAdvCallBackInfo == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  mAdvCallBackInfo->Signature                    = EFI_CALLBACK_INFO_SIGNATURE;
  mAdvCallBackInfo->DriverCallback.ExtractConfig = gSUBrowser->ExtractConfig;
  mAdvCallBackInfo->DriverCallback.RouteConfig   = gSUBrowser->RouteConfig;
  mAdvCallBackInfo->DriverCallback.Callback = AdvanceCallbackRoutine;
  mAdvCallBackInfo->HiiHandle = HiiHandle;
  CopyGuid (&mAdvCallBackInfo->FormsetGuid, &FormsetGuid);

  //
  // Install protocol interface
  //
  Status = gBS->InstallProtocolInterface (
                  &DriverHandle,
                  &gEfiHiiConfigAccessProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  &mAdvCallBackInfo->DriverCallback
                  );
  ASSERT_EFI_ERROR (Status);
  Status = InitAdvanceMenu (HiiHandle);

  //
  // Make text mode SCU can process user go into GT setting in AdvanceVfr from PowerVfr.
  //
  UpdateAdvanceDevicePathStr (HiiHandle);

  return Status;
}

EFI_STATUS
UninstallAdvanceCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle
  )
{
  EFI_STATUS     Status;

  if (mAdvCallBackInfo == NULL) {
    return EFI_SUCCESS;
  }
  Status = gBS->UninstallProtocolInterface (
                  DriverHandle,
                  &gEfiHiiConfigAccessProtocolGuid,
                  &mAdvCallBackInfo->DriverCallback
                  );
  ASSERT_EFI_ERROR (Status);
  gBS->FreePool (mAdvCallBackInfo);
  mAdvCallBackInfo = NULL;
  return Status;
}

//[-start-190709-16990080-modify]//
EFI_STATUS
InitAdvanceMenu (
  IN EFI_HII_HANDLE                         HiiHandle
  )
{
  EFI_STATUS                                Status;
  UINT8                                     SataInterfaceMode;
  UINTN                                     HandleCount;
  EFI_HANDLE                                *HandleBuffer;
  EFI_DISK_INFO_PROTOCOL                    *DiskInfo;
  EFI_DEVICE_PATH_PROTOCOL                  *DevicePath;
  EFI_DEVICE_PATH_PROTOCOL                  *DevicePathNode;
  CHIPSET_CONFIGURATION                     *SetupVariable;
  UINTN                                     Index;
  PCI_DEVICE_PATH                           *PciDevicePath;
  SATA_DEVICE_PATH                          *SataDevicePath;
  SCSI_DEVICE_PATH                          *ScsiDevicePath;
  UINTN                                     PortNum;
  UINT16                                    PortMap;
  UINT16                                    Port;
  BOOLEAN                                   OnBoardCheck;
  UINT8                                     Bus;

  //
  // Check and update IDE configuration.
  //
  SataInterfaceMode = ((PCH_SETUP *)gRcSUBrowser->PchSUBrowserData)->SataInterfaceMode;
  if (SataInterfaceMode == SATA_MODE_AHCI) {
    gSUBrowser->SUCInfo->PrevSataCnfigure = AHCI_MODE;
  } else if (SataInterfaceMode == SATA_MODE_RAID) {
    gSUBrowser->SUCInfo->PrevSataCnfigure = RAID_MODE;
  } else {
    // CRB should not have this
    gSUBrowser->SUCInfo->PrevSataCnfigure = SataInterfaceMode;
  }

    PortMap = 0;
    Port    = 0;
    //
    // Maximum native supported ports without RST remapping.
    //
  PortNum = 32;

  SetupVariable = (CHIPSET_CONFIGURATION *)gSUBrowser->SCBuffer;

  HandleCount = 0;
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiDiskInfoProtocolGuid,
                   NULL,
                  &HandleCount,
                  &HandleBuffer
                  );
  for (Index = 0; Index < HandleCount; Index++) {
    gBS->HandleProtocol (
           HandleBuffer[Index],
           &gEfiDiskInfoProtocolGuid,
           (VOID**)&DiskInfo
           );

    ASSERT_EFI_ERROR (Status);
    if (!(CompareGuid (&DiskInfo->Interface, &gEfiDiskInfoIdeInterfaceGuid) ||
          CompareGuid (&DiskInfo->Interface, &gEfiDiskInfoAhciInterfaceGuid) ||
          CompareGuid (&DiskInfo->Interface, &gEfiDiskInfoNvmeInterfaceGuid))) {
      continue;
    }

    Status = gBS->HandleProtocol (
                    HandleBuffer[Index],
                    &gEfiDevicePathProtocolGuid,
                    (VOID **) &DevicePath
                    );
    if (EFI_ERROR (Status)) {
      continue;
    }

    DevicePathNode = DevicePath;

    SataDevicePath = NULL;
    ScsiDevicePath = NULL;
    OnBoardCheck = FALSE;
    while (!IsDevicePathEnd (DevicePathNode)) {
      if (!OnBoardCheck) {
        PciDevicePath = NULL;
        Bus = 0xFF;
        PciDevicePath = GetPciLocation (DevicePath, &Bus);
        if ((PciDevicePath != NULL) && 
            (Bus != 0xFF) &&
            (!IsOnBoardPciDevice (Bus, PciDevicePath->Device, PciDevicePath->Function))) {
          break;
        }
        OnBoardCheck = TRUE;
      }
      if ((DevicePathType (DevicePathNode) == MESSAGING_DEVICE_PATH) && (DevicePathSubType (DevicePathNode) == MSG_SATA_DP)) {
        SataDevicePath = (SATA_DEVICE_PATH *) DevicePathNode;
        PortNum = SataDevicePath->HBAPortNumber;
        if (gSUBrowser->SUCInfo->PrevSataCnfigure == RAID_MODE) {
          if (SataDevicePath->PortMultiplierPortNumber & SATA_HBA_DIRECT_CONNECT_FLAG) {
            PortMap = (UINT16) PortNum;
            for (Port = 0; PortMap != 0; Port++, PortMap >>= 1);
              PortNum = Port;
          }
        }
        break;
      }
      if ((DevicePathType (DevicePathNode) == MESSAGING_DEVICE_PATH) && (DevicePathSubType (DevicePathNode) == MSG_SCSI_DP)) {
        ScsiDevicePath = (SCSI_DEVICE_PATH *) DevicePathNode;
        PortNum = ScsiDevicePath->Pun;
        break;
      }
      DevicePathNode = NextDevicePathNode (DevicePathNode);
    }
  }
  PortNum = (PortNum < 8) ? 8:PortNum;
  mIdeConfig = AllocateZeroPool (sizeof(IDE_CONFIG)*PortNum);

  InitIdeConfig (mIdeConfig);
  gSUBrowser->IdeConfig = mIdeConfig;
  Status = CheckIde (
             HiiHandle,
             (CHIPSET_CONFIGURATION *)gSUBrowser->SCBuffer,
             FALSE
             );

  InitCPUStrings (HiiHandle, ADVANCED_FORM_SET_CLASS);
  InitSaStrings (HiiHandle, ADVANCED_FORM_SET_CLASS);
  InitSBStrings (HiiHandle, ADVANCED_FORM_SET_CLASS);
  InitAcpiStrings (HiiHandle, ADVANCED_FORM_SET_CLASS);
  InitHhmMobileStrings (HiiHandle, ADVANCED_FORM_SET_CLASS);
  InitMeInfo (HiiHandle, ADVANCED_FORM_SET_CLASS);
  InitIccStrings(HiiHandle, ADVANCED_FORM_SET_CLASS);
  InitOverClockStrings (HiiHandle, ADVANCED_FORM_SET_CLASS);
  InitPlatformStrings (HiiHandle, ADVANCED_FORM_SET_CLASS);
  IccExtractConfig ();
//  UpdateACPIDebugInfo(HiiHandle);
  PlugInVideoDisplaySelectionLabel (HiiHandle);
  InitialDualVgaControllersLabel (HiiHandle);
  InitConnectivityStrings (HiiHandle, ADVANCED_FORM_SET_CLASS);
  InitBoardStrings (HiiHandle, ADVANCED_FORM_SET_CLASS);

  // if (FeaturePcdGet (PcdPttSupported)) {
  //   UpdatePTTInfo (HiiHandle);
  // }

  if (FeaturePcdGet (PcdMeUnconfigOnRtcSupported)) {
    InitMeUnconfigOnRtc(HiiHandle);
  }

  return Status;
}
//[-end-190709-16990080-modify]//

//EFI_STATUS
//UpdateACPIDebugInfo (
//  IN  EFI_HII_HANDLE                      HiiHandle
//  )
//{
//  CHAR16                            *StringBuffer;
//  CHIPSET_CONFIGURATION              *SetupVariable;
//  EFI_STRING_ID                      ACPIDebugAddr;
//  EFI_STRING_ID                      ACPIDebugAddrString;
//  VOID                              *StartOpCodeHandle;
//  EFI_IFR_GUID_LABEL                *StartLabel;
//
//  StartOpCodeHandle = HiiAllocateOpCodeHandle ();
//  ASSERT ( StartOpCodeHandle != NULL );
//
//  StartLabel               = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (StartOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
//  StartLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
//  StartLabel->Number       = APCI_DEBUG_ADDRESS_LABEL;
//
//  SetupVariable = (CHIPSET_CONFIGURATION *)(gSUBrowser->SCBuffer);
//  StringBuffer = AllocateZeroPool (0x100);
//
//  UnicodeSPrint ( StringBuffer, 0x100, L"0x%8x", SetupVariable->AcpiDebugAddr );
//  AcpiDebugAddrString = HiiSetString ( HiiHandle, 0, StringBuffer, NULL );
//
//  StringBuffer = HiiGetString ( HiiHandle, STRING_TOKEN (STR_APCI_DEBUG_ADDRESS_STRING), NULL );
//  AcpiDebugAddr = HiiSetString ( HiiHandle, 0, StringBuffer, NULL );
//
//  HiiCreateTextOpCode ( StartOpCodeHandle, AcpiDebugAddr, 0, AcpiDebugAddrString );
//
//  HiiUpdateForm (
//    HiiHandle,
//    NULL,
//    0x28,
//    StartOpCodeHandle,
//    NULL
//    );
//
//  gBS->FreePool ( StringBuffer );
//  HiiFreeOpCodeHandle ( StartOpCodeHandle );
//  return EFI_SUCCESS;
//}

// EFI_STATUS
// UpdatePTTInfo (
//   IN  EFI_HII_HANDLE                      HiiHandle
//   )
// {
//   EFI_STATUS                        Status;
//   CHAR16                            *StringBuffer;
//   CHIPSET_CONFIGURATION              *SetupVariable;
//   STRING_REF                        PTTCapStsStr;
//   STRING_REF                        PTTCapStsValue;
//   EFI_HII_UPDATE_DATA               *UpdateData;
//   BOOLEAN                           PTTCapSts;
//   UINT8                             PTTCap = 0;
//   UINT8                             PTTSts = 0;
//   VOID                              *StartOpCodeHandle;
//   EFI_IFR_GUID_LABEL                *StartLabel;

//   StartOpCodeHandle = HiiAllocateOpCodeHandle ();
//   ASSERT (StartOpCodeHandle != NULL);

//   StartLabel               = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (StartOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
//   StartLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
//   StartLabel->Number       = PTT_INFO_LABEL;

//   SetupVariable = (CHIPSET_CONFIGURATION *) (gSUBrowser->SCBuffer);
//   StringBuffer = AllocateZeroPool (0x100);
//   UpdateData = AllocateZeroPool (sizeof (EFI_HII_UPDATE_DATA));

//   Status = PttHeciGetCapability (&PTTCapSts);
//   ASSERT_EFI_ERROR (Status);
//   if (PTTCapSts) {
//     PTTCap = 1;
//   } else {
//     PTTCap = 0;
//   }

//   Status = PttHeciGetState (&PTTCapSts);
//   ASSERT_EFI_ERROR (Status);
//   if (PTTCapSts) {
//     PTTSts = 1;
//   } else {
//     PTTSts = 0;
//   }
//   if (!PTTCap ) {
//     SetupVariable->PTTEnable = PTT_DISABLE;
//   }

//   if (SetupVariable->PTTEnable == PTT_ENABLE && PcdGetBool (PcdTpmHide)) {
//     UnicodeSPrint (StringBuffer, 0x100, L"%d / %d (Hidden)",PTTCap, PTTSts);
//   } else {
//     UnicodeSPrint (StringBuffer, 0x100, L"%d / %d",PTTCap, PTTSts);
//   }

//   PTTCapStsValue = HiiSetString (HiiHandle, 0, StringBuffer, NULL);

//   StringBuffer = HiiGetString (HiiHandle, STRING_TOKEN (STR_PTT_CAP_STS_STRING), NULL);
//   ASSERT_EFI_ERROR (Status);

//   PTTCapStsStr = HiiSetString (HiiHandle, 0, StringBuffer, NULL);
//   HiiCreateTextOpCode (StartOpCodeHandle, PTTCapStsStr, 0, PTTCapStsValue);

//   //
//   //  Form 0x27: Chipset Configuration
//   //
//   HiiUpdateForm (HiiHandle, NULL, 0x27, StartOpCodeHandle, NULL);

//   gBS->FreePool (StringBuffer);
//   HiiFreeOpCodeHandle (StartOpCodeHandle);

//   return Status;
// }

EFI_STATUS
InitMeUnconfigOnRtc (
  IN  EFI_HII_HANDLE                      HiiHandle
  )
{
  EFI_STATUS                        Status;
  HECI_PROTOCOL                     *Heci;
  CHIPSET_CONFIGURATION             *SetupVariable;
  VOID                              *StartOpCodeHandle;
  VOID                              *OptionsOpCodeHandle;
  EFI_IFR_GUID_LABEL                *StartLabel;
  ME_POLICY_PROTOCOL                *MEPlatformPolicy;
  UINTN                             OptionCount;
  UINT32                            MeMode;
  UINT32                            MeUnconfigOnRtc;
  UINT16                            VarOffset;
  UINT8                             Flags;
  ME_BIOS_PAYLOAD_HOB             *MbpHob;
  ME_DXE_CONFIG                     *MeDxeConfig;

  MbpHob              = NULL;
  DEBUG ((DEBUG_ERROR | DEBUG_INFO, "InitMeUnconfigOnRtc Start !!\n"));

  Status              = EFI_SUCCESS;
  Heci                = NULL;
  SetupVariable       = NULL;
  StartOpCodeHandle   = NULL;
  OptionsOpCodeHandle = NULL;
  StartLabel          = NULL;
  MEPlatformPolicy    = NULL;
  MeMode              = ME_MODE_FAILED;
  MeUnconfigOnRtc     = 0;
  VarOffset           = 0;
  Flags               = 0;
  SetupVariable       = (CHIPSET_CONFIGURATION *)(gSUBrowser->SCBuffer);

  //
  // Get the MBP Data.
  //
  MbpHob = GetFirstGuidHob (&gMeBiosPayloadHobGuid);
  if (MbpHob == NULL){
    ASSERT (MbpHob != NULL);
    return EFI_NOT_FOUND;
  }

  if (EFI_ERROR(Status))  {
     DEBUG ((DEBUG_ERROR | DEBUG_INFO, "LocateProtocol MeBiosPayload, Status : %r\n", Status ));
  }

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **)&Heci);
  if (EFI_ERROR(Status))  {
     DEBUG ((DEBUG_ERROR | DEBUG_INFO, "LocateProtocol Heci, Status : %r\n", Status ));
  } else{

    Heci->GetMeMode (&MeMode);
    if (MeMode == ME_MODE_NORMAL) {
      Status = HeciGetUnconfigOnRtcClearDisableMsg(&MeUnconfigOnRtc);
      ASSERT_EFI_ERROR (Status);
      SetupVariable->MeUnconfigOnRtc = (UINT8)MeUnconfigOnRtc;
    }
  }

//
//  1. if ME FW un-supported "Unconfig On Rtc", the item will be gray out
//  2. Afer EOP then enter SCU, the item will be gray out
//
  Status = gBS->LocateProtocol ( &gDxeMePolicyGuid, NULL, (VOID **)&MEPlatformPolicy);
  if (EFI_ERROR(Status))  {
     DEBUG ((DEBUG_ERROR | DEBUG_INFO, "LocateProtocol gDxeMePolicyGuid, Status : %r\n", Status ));
  }
  Status = GetConfigBlock ((VOID *) MEPlatformPolicy, &gMeDxeConfigGuid, (VOID *) &MeDxeConfig);
  ASSERT_EFI_ERROR (Status);

  StartOpCodeHandle = HiiAllocateOpCodeHandle ();
  ASSERT (StartOpCodeHandle != NULL);
  OptionsOpCodeHandle = HiiAllocateOpCodeHandle ();
  ASSERT ( OptionsOpCodeHandle != NULL);

  StartLabel = (EFI_IFR_GUID_LABEL *) HiiCreateGuidOpCode (StartOpCodeHandle, &gEfiIfrTianoGuid, NULL, sizeof (EFI_IFR_GUID_LABEL));
  StartLabel->ExtendOpCode = EFI_IFR_EXTEND_OP_LABEL;
  StartLabel->Number       = ME_UNCONFIG_ON_RTC_LABEL;

  ScuCreateGrayoutIfOpCode(StartOpCodeHandle);
  if (MeIsAfterEndOfPost() | (!MbpHob->MeBiosPayload.UnconfigOnRtcClearState.Available)) {
    DEBUG ((DEBUG_ERROR | DEBUG_INFO, "ScuCreateTrueOpCode\n"));
    ScuCreateTrueOpCode(StartOpCodeHandle);
  } else {
    DEBUG ((DEBUG_ERROR | DEBUG_INFO, "ScuCreateFalseOpCode\n"));
    ScuCreateFalseOpCode(StartOpCodeHandle);
  }

  VarOffset = (UINT16)((UINTN)(&SetupVariable->MeUnconfigOnRtc) - (UINTN)SetupVariable);

  OptionCount = 2; // Enable and disable.
  Flags = EFI_IFR_TYPE_NUM_SIZE_8 + EFI_IFR_OPTION_DEFAULT;
  HiiCreateOneOfOptionOpCode (
    OptionsOpCodeHandle,
    STRING_TOKEN(STR_ENABLED_TEXT),
    Flags,
    EFI_IFR_NUMERIC_SIZE_1,
    0
    );

  Flags = EFI_IFR_TYPE_NUM_SIZE_8;
  HiiCreateOneOfOptionOpCode (
    OptionsOpCodeHandle,
    STRING_TOKEN(STR_DISABLED_TEXT),
    Flags,
    EFI_IFR_NUMERIC_SIZE_1,
    1
    );


  HiiCreateOneOfOpCode (
    StartOpCodeHandle,
    KEY_ME_UNCONFIG_ON_RTC,
    CONFIGURATION_VARSTORE_ID,
    VarOffset,
    STRING_TOKEN (STR_ME_UNCONF_RTC_STATE_PROMPT),
    STRING_TOKEN (STR_ME_UNCONF_RTC_STATE_PROMPT_HELP),
    EFI_IFR_FLAG_CALLBACK,
    EFI_IFR_NUMERIC_SIZE_1,
    OptionsOpCodeHandle,
    NULL
    );

  //
  //  Form 0x27: Chipset Configuration
  //
  HiiUpdateForm (HiiHandle, NULL, 0x27, StartOpCodeHandle, NULL);
  HiiFreeOpCodeHandle (StartOpCodeHandle);
  HiiFreeOpCodeHandle (OptionsOpCodeHandle);

  DEBUG ((DEBUG_ERROR | DEBUG_INFO, "MeUnconfigOnRtcAvailable = %x \n", MbpHob->MeBiosPayload.UnconfigOnRtcClearState.Available));
  DEBUG ((DEBUG_ERROR | DEBUG_INFO, "MeUnconfigOnRtc          = %x \n", SetupVariable->MeUnconfigOnRtc));
  DEBUG ((DEBUG_ERROR | DEBUG_INFO, "InitMeUnconfigOnRtc End !!\n"));
  return EFI_SUCCESS;
}


EFI_STATUS
SyncIccSetupDataWithFormBrowser (
  IN EFI_BROWSER_ACTION                   Action
  )
{
  EFI_STATUS                              Status;
  ICC_SETUP_DATA                          IccSetup = {0};
  UINTN                                   Size = sizeof (ICC_SETUP_DATA);

  switch (Action) {

  case EFI_BROWSER_ACTION_FORM_OPEN:
    Status = gRT->GetVariable (
                    ICC_SETUP_DATA_C_NAME_CURRENT,
                    &gIccGuid,
                    NULL,
                    &Size,
                    &IccSetup
                    );
    Status = SetupVariableConfig (
               &gIccGuid,
               ICC_SETUP_DATA_C_NAME,
               sizeof (ICC_SETUP_DATA),
               (UINT8 *) &IccSetup,
               FALSE
               );
    break;

  case EFI_BROWSER_ACTION_FORM_CLOSE:
    Status = SetupVariableConfig (
               &gIccGuid,
               ICC_SETUP_DATA_C_NAME,
               sizeof (ICC_SETUP_DATA),
               (UINT8 *) &IccSetup,
               TRUE
               );
    Status = gRT->SetVariable (
                    ICC_SETUP_DATA_C_NAME_CURRENT,
                    &gIccGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS,
                    sizeof (ICC_SETUP_DATA),
                    &IccSetup
                    );
    break;

  case EFI_BROWSER_ACTION_CHANGING:
    Status = EFI_SUCCESS;
    break;

  case EFI_BROWSER_ACTION_DEFAULT_MANUFACTURING:
    Status = gRT->GetVariable (
                    ICC_SETUP_DATA_C_NAME,
                    &gIccGuid,
                    NULL,
                    &Size,
                    &IccSetup
                    );
    if (Size == sizeof (ICC_SETUP_DATA)) {
      Status = gRT->SetVariable (
                      ICC_SETUP_DATA_C_NAME_CURRENT,
                      &gIccGuid,
                      EFI_VARIABLE_BOOTSERVICE_ACCESS,
                      sizeof(ICC_SETUP_DATA),
                      &IccSetup
                      );
    }
    Status = SetupVariableConfig (
               &gIccGuid,
               ICC_SETUP_DATA_C_NAME,
               sizeof (ICC_SETUP_DATA),
               (UINT8 *) &IccSetup,
               FALSE
               );
    break;

  default:
    Status = EFI_SUCCESS;
    break;
  }
  return Status;
}
