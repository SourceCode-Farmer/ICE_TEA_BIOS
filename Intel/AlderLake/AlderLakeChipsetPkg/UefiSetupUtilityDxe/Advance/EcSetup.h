/** @file

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EC_SETUP_H
#define _EC_SETUP_H

/**

  @param[in] HiiHandle
  @param[in] Class
**/
VOID
InitHhmMobileStrings (
  IN EFI_HII_HANDLE HiiHandle,
  IN UINT16         Class
  );

/**

  @param[in] HiiHandle
  @param[in] Class
  @param[in] SubClass
  @param[in] Key
**/
VOID HhmMobileCallBack(
  IN EFI_HII_HANDLE HiiHandle,
  IN UINT16 Class,
  IN UINT16 SubClass,
  IN UINT16 Key
  );

#endif
