/** @file

;******************************************************************************
;* Copyright (c) 2015 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <SetupUtility.h>
#include <Protocol/SetupMouse.h>
#include <SetupConfig.h>
// #include <SgxSetupData.h>
#include <PlatformSetup.h>
//[-start-190604-IB11270236-remove]//
//#include <Protocol/AlertStandardFormat.h>
//[-end-190604-IB11270236-remove]//
//[-start-190604-IB11270236-remove]//
//#include <AsfMsgs.h>
//[-end-190604-IB11270236-remove]//
//[-start-190902-IB16740050-add]//
#include <VmdInfoHob.h>
//[-end-190902-IB16740050-add]//
#include <Library/PchInfoLib.h>
#include <HybridGraphicsDefine.h>
#include <Library/MsrFruLib.h>
//[-start-201127-IB17510129-add]//
#include <Library/DxeMeLib.h>
#include <PchResetPlatformSpecific.h>
//[-end-201127-IB17510129-add]//

UINT8 mFullResetFlag = 0;
EFI_HII_HANDLE mDriverHiiHandle;
EFI_HII_STRING_PROTOCOL         *gIfrLibHiiString;

extern UINT32 mScuRecord;

#pragma pack(1)
typedef struct {
  VENDOR_DEVICE_PATH             VendorDevicePath;
  UINT32                         Reserved;
  UINT64                         UniqueId;
} HII_VENDOR_DEVICE_PATH_NODE;
#pragma pack()

typedef struct {
  HII_VENDOR_DEVICE_PATH_NODE    Node;
  EFI_DEVICE_PATH_PROTOCOL       End;
} HII_TEMP_DEVICE_PATH;

//
// Hii vendor device path template
//
HII_TEMP_DEVICE_PATH  mHiiVendorDevicePathTemplate = {
  {
    {
      {
        HARDWARE_DEVICE_PATH,
        HW_VENDOR_DP,
        (UINT8) (sizeof (HII_VENDOR_DEVICE_PATH_NODE)),
        (UINT8) ((sizeof (HII_VENDOR_DEVICE_PATH_NODE)) >> 8)
      },
      EFI_IFR_TIANO_GUID,
    },
    0,
    0
  },
  {
    END_DEVICE_PATH_TYPE,
    END_ENTIRE_DEVICE_PATH_SUBTYPE,
    END_DEVICE_PATH_LENGTH,
    0
  }
};


BOOLEAN
IsUserPasswrodBypass(
  VOID
  )
{
//[-start-190604-IB11270236-modify]//
  ALERT_STANDARD_FORMAT_PROTOCOL      *Asf;
//  EFI_ASF_BOOT_OPTIONS                    *AsfBootOptions;
//[-end-190604-IB11270236-modify]//
  EFI_STATUS                          Status;

  Asf            = NULL;
//[-start-190604-IB11270236-remove]//
//  AsfBootOptions = NULL;
//[-end-190604-IB11270236-remove]//

  Status = gBS->LocateProtocol (
//[-start-190604-IB11270236-modify]//
                &gAlertStandardFormatProtocolGuid,
//[-end-190604-IB11270236-modify]//
                NULL,
                (VOID **)&Asf
                );
  if ( !EFI_ERROR(Status) ) {
//[-start-190604-IB11270236-modify]//
//    Status = Asf->GetBootOptions(Asf, &AsfBootOptions);
//    if ( !EFI_ERROR(Status) ) {
//      if ( (AsfBootOptions->BootOptions & USER_PASSWORD_BYPASS) == USER_PASSWORD_BYPASS) {
      if ( (Asf->AsfBootOptions.BootOptionsMaskByte2 & USER_PASSWORD_BYPASS) == USER_PASSWORD_BYPASS) {
        return TRUE;
      } else {
        return FALSE;
      }
//    }
//[-end-190604-IB11270236-modify]//
  }
  return FALSE;
}

/**
 Check the formset in the input HII form packge is supported formset or not.

 @param[in] HiiFormPackage            Pointer to the HII form package

 @retval TRUE                         The formset of input HII form packge is suppoted formset
 @retval FALSE                        The formset of input HII form packge is suppoted formset
**/
STATIC
BOOLEAN
IsSupportedFormSet (
  IN EFI_HII_PACKAGE_LIST_HEADER      *HiiFormPackage
  )
{
  EFI_IFR_FORM_SET                    *FormSetPtr;
  EFI_IFR_GUID_CLASS                  *ClassOpCode;
  EFI_IFR_GUID_SUBCLASS               *SubClassOpCode;
  EFI_GUID                            *ClassGuid;
  UINT8                               ClassGuidNum;
  UINT8                               Index;
  EFI_GUID                            ScuClassGuid = SETUP_UTILITY_FORMSET_CLASS_GUID;

  FormSetPtr = (EFI_IFR_FORM_SET *) ((EFI_HII_FORM_PACKAGE_HDR *) (HiiFormPackage + 1) + 1);

  //
  // If the formset (which class GUID contains HII platform setup) belongs in the device manager,
  // need to check class and subclass to determine it is supported formset or not.
  //
  ClassGuid    = (EFI_GUID *) ((UINT8 *) FormSetPtr + sizeof (EFI_IFR_FORM_SET));
  ClassGuidNum = (UINT8) (FormSetPtr->Flags & 0x3);
  for (Index = 0; Index < ClassGuidNum; Index++, ClassGuid++) {
    if (!CompareGuid (ClassGuid, &ScuClassGuid)) {
      continue;
    }

    ClassOpCode    = (EFI_IFR_GUID_CLASS    *) ((UINT8 *) FormSetPtr  + FormSetPtr->Header.Length);
    SubClassOpCode = (EFI_IFR_GUID_SUBCLASS *) ((UINT8 *) ClassOpCode + ClassOpCode->Header.Length);

    if (ClassOpCode->Header.OpCode    == EFI_IFR_GUID_OP && CompareGuid ((EFI_GUID *)((UINT8 *) ClassOpCode    + sizeof (EFI_IFR_OP_HEADER)), &gEfiIfrTianoGuid) &&
        SubClassOpCode->Header.OpCode == EFI_IFR_GUID_OP && CompareGuid ((EFI_GUID *)((UINT8 *) SubClassOpCode + sizeof (EFI_IFR_OP_HEADER)), &gEfiIfrTianoGuid)) {
      if (ClassOpCode->Class == EFI_NON_DEVICE_CLASS) {
        return FALSE;
      }

      if (SubClassOpCode->SubClass == EFI_USER_ACCESS_THREE &&
          ((CHIPSET_CONFIGURATION *)gSUBrowser->SCBuffer)->SetUserPass == 1 &&
          ((CHIPSET_CONFIGURATION *)gSUBrowser->SCBuffer)->UserAccessLevel == 3) {
        return FALSE;
      }

      if (SubClassOpCode->SubClass == EFI_USER_ACCESS_TWO &&
          ((CHIPSET_CONFIGURATION *)gSUBrowser->SCBuffer)->SetUserPass == 1 &&
          ((CHIPSET_CONFIGURATION *)gSUBrowser->SCBuffer)->UserAccessLevel == 2) {
        return FALSE;
      }
    }
  }

  return TRUE;
}

/**
 The HII driver handle passed in for HiiDatabase.NewPackageList() requires
 that there should be DevicePath Protocol installed on it.
 This routine create a virtual Driver Handle by installing a vendor device
 path on it, so as to use it to invoke HiiDatabase.NewPackageList().

 @param [in]     DriverHandle         Handle to be returned

 @retval EFI_SUCCESS            Handle destroy success.
 @retval EFI_OUT_OF_RESOURCES   Not enough memory.

**/
EFI_STATUS
CreateHiiDriverHandle (
  OUT EFI_HANDLE               *DriverHandle
  )
{
  EFI_STATUS                   Status;
  HII_VENDOR_DEVICE_PATH_NODE  *VendorDevicePath;

  VendorDevicePath = AllocateCopyPool (sizeof (HII_TEMP_DEVICE_PATH), &mHiiVendorDevicePathTemplate);
  if (VendorDevicePath == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Use memory address as unique ID to distinguish from different device paths
  //
  VendorDevicePath->UniqueId = (UINT64) ((UINTN) VendorDevicePath);

  *DriverHandle = NULL;
  Status = gBS->InstallMultipleProtocolInterfaces (
                  DriverHandle,
                  &gEfiDevicePathProtocolGuid,
                  VendorDevicePath,
                  NULL
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  return EFI_SUCCESS;
}
/**
 Destroy the Driver Handle created by CreateHiiDriverHandle().

 @param [in]     DriverHandle   Handle returned by CreateHiiDriverHandle()

 @retval EFI_SUCCESS            Handle destroy success.
 @return other                  Handle destroy fail.

**/
EFI_STATUS
DestroyHiiDriverHandle (
  IN EFI_HANDLE                 DriverHandle
  )
{
  EFI_STATUS                   Status;
  EFI_DEVICE_PATH_PROTOCOL     *DevicePath;

  Status = gBS->HandleProtocol (
                  DriverHandle,
                  &gEfiDevicePathProtocolGuid,
                  (VOID**)&DevicePath
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = gBS->UninstallProtocolInterface (
                  DriverHandle,
                  &gEfiDevicePathProtocolGuid,
                  DevicePath
                  );
  gBS->FreePool (DevicePath);
  return Status;
}

USER_UNINSTALL_CALLBACK_ROUTINE mUninstallCallbackRoutine[] ={
  UninstallExitCallbackRoutine,     UninstallBootCallbackRoutine,
  UninstallPowerCallbackRoutine,    UninstallSecurityCallbackRoutine,
  UninstallAdvanceCallbackRoutine,  UninstallMainCallbackRoutine
  };


EFI_GUID  mFormSetGuid = SYSTEM_CONFIGURATION_GUID;
EFI_GUID  mFormSetClassGuid = SETUP_UTILITY_FORMSET_CLASS_GUID;
CHAR16    mVariableName[] = L"SystemConfig";

EFI_GUID gRcSetupUtilityVariableGuid = RC_SETUP_UTILITY_VARIABLE_GUID;
EFI_GUID gRcSetupUtilityBrowserVariable = RC_SETUP_UTILITY_BROWSER_VARIABLE_GUID;

EFI_STATUS
InitSetupUtilityBrowser (
  IN  EFI_SETUP_UTILITY_PROTOCOL            *This
  );

EFI_STATUS
InitRcSetupUtilityBrowser (
  VOID
  );

SETUP_UTILITY_BROWSER_DATA                   *gSUBrowser;
RC_SETUP_UTILITY_BROWSER_DATA                *gRcSUBrowser = NULL;
UINT16                                       gCallbackKey;

EFI_STATUS
EFIAPI
SetupUtilityInit (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  SETUP_UTILITY_DATA                    *SetupData;
  EFI_EVENT                             Event;
  VOID                                  *Registration;
//[-start-190709-16990077-add]//
  EFI_ME_STATUS_PROTOCOL				*MeStatus;
//[-end-190709-16990077-add]//
  CHIPSET_CONFIGURATION                 *SetupNvData;
  RC_SETUP_UTILITY_DATA                 *RcSetupData;
  UINTN                                 RcSetupDataSize;
  UINT32                                InitSetupVolVarAttr;
  UINT8                                 InitSetupFlag;
  UINTN                                 Size;

  //
  // Initialize the library.
  //

  CheckLanguage ();

  SetupData = AllocatePool (sizeof(SETUP_UTILITY_DATA));
  if (SetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  SetupData->Signature                    = EFI_SETUP_UTILITY_SIGNATURE;
  SetupData->SetupUtility.StartEntry      = NULL;
  SetupData->SetupUtility.PowerOnSecurity = PowerOnSecurity;
  mFullResetFlag = 0;
//[-start-190709-16990077-add]//
  Status = gBS->AllocatePool (
                  EfiBootServicesData,
                  sizeof(EFI_ME_STATUS_PROTOCOL),
                  &MeStatus
                  );
  if (SetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  MeStatus->Status1 = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS(ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, R_ME_HFS));
  MeStatus->Status2 = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS(ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, R_ME_HFS_2));
//[-end-190709-16990077-add]//

  {
    RcSetupData = AllocateZeroPool (sizeof(RC_SETUP_UTILITY_DATA));
    if (RcSetupData == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    RcSetupData->SetupDataNvData      = NULL;
    RcSetupData->SaSetupNvData        = NULL;
    RcSetupData->MeSetupNvData        = NULL;
    RcSetupData->CpuSetupNvData       = NULL;
    RcSetupData->PchSetupNvData       = NULL;
    RcSetupData->SiSetupNvData        = NULL;
    RcSetupData->MeSetupStorageNvData = NULL;
  }

  //
  // The FirstIn flag is for that if there is the first time entering SCU, we should install
  // Vfr of Menu to Hii database.
  // After that, we shouldn't install the Vfr to Hii database again.
  //
  SetupData->SetupUtility.FirstIn         = TRUE;
  Status = GetSystemConfigurationVar (SetupData, RcSetupData);

  SetupNvData = CommonGetVariableData (SETUP_VARIABLE_NAME, &gSystemConfigurationGuid);
  if (SetupNvData == NULL) {
    return EFI_NOT_FOUND;
  }


  Status = PlatformSetupEntry (SetupNvData);
  Status = UpdateSetupVolatileData ();

  { // Save RC Setup Utility
    RcSetupDataSize = sizeof (RC_SETUP_UTILITY_DATA);
    Status = gRT->SetVariable (
                    RC_SETUP_UTILITY_VARIABLE_NAME,
                    &gRcSetupUtilityVariableGuid,
                    EFI_VARIABLE_BOOTSERVICE_ACCESS,
                    RcSetupDataSize,
                    (VOID *)RcSetupData
                    );
    gBS->FreePool (RcSetupData);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  //
  // Install Setup Utility
  //
  SetupData->Handle = NULL;
  Status = gBS->InstallProtocolInterface (
                 &SetupData->Handle,
                 &gEfiSetupUtilityProtocolGuid,
                 EFI_NATIVE_INTERFACE,
                 &SetupData->SetupUtility
                 );

  if (EFI_ERROR (Status)) {
    gBS->FreePool (SetupData);
    return Status;
  }

//[-start-190709-16990077-add]//
  Status = gBS->InstallProtocolInterface (
   				 &ImageHandle,
   				 &gH2OMeStatusProtocolGuid,
   				 EFI_NATIVE_INTERFACE,
   				 MeStatus
  				 );
  if (EFI_ERROR (Status)) {
    gBS->FreePool (MeStatus);
    return Status;
  }
//[-end-190709-16990077-add]//

  //
  // When execute Setup Utility application, install HII data
  //
  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK - 1,
                  SetupUtilityNotifyFn,
                  NULL,
                  &Event
                  );
  if (!EFI_ERROR (Status)) {
    Status = gBS->RegisterProtocolNotify (
                    &gEfiSetupUtilityApplicationProtocolGuid,
                    Event,
                    &Registration
                    );
  }

  //======================//
  // Sync RC setup driver //
  //======================//
  InitCpuMntrDefault();
  Status = InitCpuSetupVolatileData ();

  //
  // Check whether first boot.
  //
  InitSetupVolVarAttr = EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS;
  Size = sizeof (InitSetupFlag);
  Status = gRT->GetVariable (L"InitSetupVariable", &gSystemConfigurationGuid, NULL, &Size, &InitSetupFlag);
  if (Status == EFI_NOT_FOUND) {
    InitSetupFlag = 1;
    Status = gRT->SetVariable (
           L"InitSetupVariable",
           &gSystemConfigurationGuid,
           InitSetupVolVarAttr,
           sizeof (InitSetupFlag),
           &InitSetupFlag
           );
  }

  gBS->FreePool (SetupNvData);

  return Status;
}

EFI_STATUS
CreateScuData (
  BOOLEAN  DoClearScreen
  )
{
  EFI_STATUS                                Status;
  EFI_SETUP_UTILITY_BROWSER_PROTOCOL        *Interface;
  UINTN                                     BufferSize;
  UINT8                                     *Lang = NULL;
  EFI_SETUP_UTILITY_PROTOCOL                *This;

  Status = gBS->LocateProtocol (
                  &gEfiSetupUtilityBrowserProtocolGuid,
                  NULL,
                  (VOID **)&Interface
                  );
  //
  // If there was no error, assume there is an installation and fail to load
  //
  if (EFI_ERROR(Status) ) {
    Status = gBS->LocateProtocol (&gEfiSetupUtilityProtocolGuid, NULL, (VOID **) &This);
    if (EFI_ERROR(Status)) {
      return Status;
    }

    Status = InitSetupUtilityBrowser(This);
    if (EFI_ERROR(Status)) {
      return Status;
    }

    Status = InstallSetupUtilityBrowserProtocol (This);
    if (EFI_ERROR(Status)) {
      return Status;
    }

    Status = InitRcSetupUtilityBrowser ();
    if (EFI_ERROR(Status)) {
      return Status;
    }

    ZeroMem (gSUBrowser->SUCInfo->MapTable, sizeof (HII_HANDLE_VARIABLE_MAP_TABLE) * MAX_HII_HANDLES);
    PlugInVgaUpdateInfo ();
    Status = InstallHiiData ();
    This->FirstIn = FALSE;
    if (EFI_ERROR(Status)) {
      return Status;
    }

    //
    // Load the variable data records
    //
    gSUBrowser->Interface.SCBuffer = (UINT8 *) gSUBrowser->SCBuffer;
  }
  Lang = GetVariableAndSize (
           L"PlatformLang",
           &gEfiGlobalVariableGuid,
           &BufferSize
           );
  if (Lang != NULL) {
    Status = gRT->SetVariable (
                    L"BackupPlatformLang",
                    &gEfiGenericVariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    BufferSize,
                    Lang
                    );
    gBS->FreePool (Lang);
  }

  return Status;
}

EFI_STATUS
DestroyScuData (
  BOOLEAN  DoClearScreen
  )
{
  EFI_STATUS                                Status;
  UINTN                                     BufferSize;
  UINT8                                     *Lang;
  EFI_SYS_PASSWORD_SERVICE_PROTOCOL         *SysPasswordService;
  EFI_SETUP_UTILITY_PROTOCOL                *This;
  RC_SETUP_UTILITY_DATA                     RcSetupData;

  Status = gBS->LocateProtocol (
                  &gEfiSysPasswordServiceProtocolGuid,
                  NULL,
                  (VOID **)&SysPasswordService
                  );
  ASSERT_EFI_ERROR (Status);
  SysPasswordService->LockPassword (SysPasswordService);
  Status = RemoveHiiData (
             SetupUtilityStrings,
             (HII_HANDLE_VARIABLE_MAP_TABLE *) &(gSUBrowser->SUCInfo->MapTable[ExitHiiHandle]),
             (HII_HANDLE_VARIABLE_MAP_TABLE *) &(gSUBrowser->SUCInfo->MapTable[BootHiiHandle]),
             (HII_HANDLE_VARIABLE_MAP_TABLE *) &(gSUBrowser->SUCInfo->MapTable[PowerHiiHandle]),
             (HII_HANDLE_VARIABLE_MAP_TABLE *) &(gSUBrowser->SUCInfo->MapTable[SecurityHiiHandle]),
             (HII_HANDLE_VARIABLE_MAP_TABLE *) &(gSUBrowser->SUCInfo->MapTable[AdvanceHiiHandle]),
             (HII_HANDLE_VARIABLE_MAP_TABLE *) &(gSUBrowser->SUCInfo->MapTable[MainHiiHandle]),
             NULL
             );

  Status = UninstallSetupUtilityBrowserProtocol ();
  Status = gBS->LocateProtocol (&gEfiSetupUtilityProtocolGuid, NULL, (VOID **) &This);
  if (!EFI_ERROR(Status)) {
    This->FirstIn = TRUE;
  }
  gSUBrowser = NULL;
  if (DoClearScreen) {
    if (gST->ConOut != NULL) {
      gST->ConOut->SetAttribute (gST->ConOut, EFI_WHITE | EFI_BACKGROUND_BLACK);
      gST->ConOut->ClearScreen (gST->ConOut);
    }
  }
  Lang = GetVariableAndSize (
           L"BackupPlatformLang",
           &gEfiGenericVariableGuid,
           &BufferSize
           );
  if (Lang != NULL) {
    Status = gRT->SetVariable (
                    L"PlatformLang",
                    &gEfiGlobalVariableGuid,
                    EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                    BufferSize,
                    Lang
                    );
    gBS->FreePool (Lang);

    gRT->SetVariable (
           L"BackupPlatformLang",
           &gEfiGenericVariableGuid,
           EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
           0,
           NULL
           );
  }

  {
    // Remove RC setup utility browser data
    gRT->SetVariable (
           RC_SETUP_UTILITY_BROWSER_VARIABLE_NAME,
           &gRcSetupUtilityBrowserVariable,
           0,
           0,
           NULL
           );
    if (gRcSUBrowser != NULL ) {
      gBS->FreePool (gRcSUBrowser);
    }
    gRcSUBrowser = NULL;

    // Restore NVmemory map of RcSetupData from ROM
    BufferSize = sizeof (RC_SETUP_UTILITY_DATA);
    Status = gRT->GetVariable (
                    RC_SETUP_UTILITY_VARIABLE_NAME,
                    &gRcSetupUtilityVariableGuid,
                    NULL,
                    &BufferSize,
                    &RcSetupData
                    );
    //===============================================================================================//
    // Restore SetupData/SA/ME/CPU/PCH/SI/MEstorage NV data from ROM to RC setup utility data region //
    //===============================================================================================//
    BufferSize = sizeof (SETUP_DATA);
    Status = gRT->GetVariable (
                    PLATFORM_SETUP_VARIABLE_NAME,
                    &gSetupVariableGuid,
                    NULL,
                    &BufferSize,
                    RcSetupData.SetupDataNvData
                    );
    BufferSize = sizeof (SA_SETUP);
    Status = gRT->GetVariable (
                    SA_SETUP_VARIABLE_NAME,
                    &gSaSetupVariableGuid,
                    NULL,
                    &BufferSize,
                    RcSetupData.SaSetupNvData
                    );
    BufferSize = sizeof (ME_SETUP);
    Status = gRT->GetVariable (
                    ME_SETUP_VARIABLE_NAME,
                    &gMeSetupVariableGuid,
                    NULL,
                    &BufferSize,
                    RcSetupData.MeSetupNvData
                    );
    BufferSize = sizeof (CPU_SETUP);
    Status = gRT->GetVariable (
                    CPU_SETUP_VARIABLE_NAME,
                    &gCpuSetupVariableGuid,
                    NULL,
                    &BufferSize,
                    RcSetupData.CpuSetupNvData
                    );
    BufferSize = sizeof (PCH_SETUP);
    Status = gRT->GetVariable (
                    PCH_SETUP_VARIABLE_NAME,
                    &gPchSetupVariableGuid,
                    NULL,
                    &BufferSize,
                    RcSetupData.PchSetupNvData
                    );
    BufferSize = sizeof (SI_SETUP);
    Status = gRT->GetVariable (
                    SI_SETUP_VARIABLE_NAME,
                    &gSiSetupVariableGuid,
                    NULL,
                    &BufferSize,
                    RcSetupData.SiSetupNvData
                    );
    BufferSize = sizeof (ME_SETUP_STORAGE);
    Status = gRT->GetVariable (
                    ME_SETUP_STORAGE_VARIABLE_NAME,
                    &gMeSetupVariableGuid,
                    NULL,
                    &BufferSize,
                    RcSetupData.MeSetupStorageNvData
                    );
  }

  return Status;
}

BOOLEAN
AsfResetSetupData (
  IN  VOID
  )
{
  EFI_STATUS                          Status;
//[-start-190604-IB11270236-modify]//
  ALERT_STANDARD_FORMAT_PROTOCOL      *Asf;
//  EFI_ASF_BOOT_OPTIONS                    *mAsfBootOptions;
//[-end-190604-IB11270236-modify]//

  //
  // Get Protocol for ASF
  //
  Status = gBS->LocateProtocol (
//[-start-190604-IB11270236-modify]//
                      &gAlertStandardFormatProtocolGuid,
//[-end-190604-IB11270236-modify]//
                      NULL,
                      (VOID **)&Asf
                      );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "Error gettings ASF protocol -> %r\n", Status));
    return FALSE;
  }

//[-start-190604-IB11270236-modify]//
//  Status = Asf->GetBootOptions(Asf, &mAsfBootOptions);
//  if (EFI_ERROR (Status)) {
//    return FALSE;
//  }

//  if (mAsfBootOptions->SubCommand != ASF_BOOT_OPTIONS_PRESENT) {
  if (Asf->AsfBootOptions.SubCommand != ASF_BOOT_OPTIONS_PRESENT) {
    return FALSE;
  }

//  if ((mAsfBootOptions->BootOptions & CONFIG_DATA_RESET) == CONFIG_DATA_RESET) {
  if ((Asf->AsfBootOptions.BootOptionsMaskByte2 & CONFIG_DATA_RESET) == CONFIG_DATA_RESET) {
    return TRUE;
  }
//[-end-190604-IB11270236-modify]//

  return FALSE;
}

EFI_STATUS
PowerOnSecurity (
  IN  EFI_SETUP_UTILITY_PROTOCOL        *SetupUtility
  )
{
  EFI_STATUS                            Status;
  EFI_HII_HANDLE                        HiiHandle;
  EFI_GUID                              StringPackGuid = STRING_PACK_GUID;
  SETUP_UTILITY_CONFIGURATION           *SUCInfo = NULL;
  EFI_SETUP_UTILITY_BROWSER_PROTOCOL    *Interface;
  EFI_HANDLE                            DriverHandle;
  VOID                                  *Table;
  UINT8                                 *HobSetupData;
  VOID                                  *HobList = NULL;
  EFI_BOOT_MODE                         BootMode;

  HiiHandle = 0;

  //
  // There will be only one DeviceManagerSetup in the system.
  // If there is another out there, someone is trying to install us
  // again.  We fail in that scenario.
  //
  Status = gBS->LocateProtocol (
                  &gEfiSetupUtilityBrowserProtocolGuid,
                  NULL,
                  (VOID **)&Interface
                  );
  //
  // If there was no error, assume there is an installation and fail to load
  //
  if (!EFI_ERROR(Status) ) {
    Status = UninstallSetupUtilityBrowserProtocol ();
  }

  Status = InitSetupUtilityBrowser (SetupUtility);
  if (EFI_ERROR(Status)) {
    return Status;
  }

  Status = InstallSetupUtilityBrowserProtocol (SetupUtility);
  if (EFI_ERROR(Status)) {
    return Status;
  }
  SUCInfo = gSUBrowser->SUCInfo;
  Status = CreateHiiDriverHandle (&DriverHandle);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  HiiHandle = HiiAddPackages (&StringPackGuid, DriverHandle, SecurityVfrBin ,SetupUtilityLibStrings, NULL);
  ASSERT(HiiHandle != NULL);

  gSUBrowser->SUCInfo->MapTable[SecurityHiiHandle].HiiHandle = HiiHandle;
  gSUBrowser->Interface.SCBuffer = (UINT8 *) gSUBrowser->SCBuffer;

  Status = gBS->LocateProtocol (
                  &gEfiSysPasswordServiceProtocolGuid,
                  NULL,
                  (VOID **)&SUCInfo->SysPasswordService
                  );

  SUCInfo->SupervisorPassword = NULL;
  SUCInfo->UserPassword       = NULL;

  if (!EFI_ERROR(Status)) {

    //
    // Check password
    //
    BootMode = GetBootModeHob();
    if (BootMode == BOOT_IN_RECOVERY_MODE) {
      HobList = GetHobList ();
      Table= GetNextGuidHob (&gEfiPowerOnPwSCUHobGuid, HobList);
      if (Table != NULL) {
        HobSetupData = ((UINT8 *) Table) + sizeof (EFI_HOB_GUID_TYPE);
        ((CHIPSET_CONFIGURATION *)gSUBrowser->SCBuffer)->PowerOnPassword = ((CHIPSET_CONFIGURATION *) HobSetupData)->PowerOnPassword;
      }
    }
    if ((((CHIPSET_CONFIGURATION *) gSUBrowser->SCBuffer)->PowerOnPassword) == POWER_ON_PASSWORD) {
      if(IsUserPasswrodBypass() == FALSE) {
        Status = PasswordCheck (
                   SUCInfo,
                   (KERNEL_CONFIGURATION *) gSUBrowser->SCBuffer
                   );
        ASSERT_EFI_ERROR (Status);
      }
    }
  }

  if (SUCInfo->SupervisorPassword  != NULL) {
    if (SUCInfo->SupervisorPassword->NumOfEntry != 0) {
      gBS->FreePool (SUCInfo->SupervisorPassword->InputString);
      gBS->FreePool (SUCInfo->SupervisorPassword);
      SUCInfo->SupervisorPassword = NULL;
     }
  }

  if (SUCInfo->UserPassword  != NULL) {
    if (SUCInfo->UserPassword->NumOfEntry != 0) {
      gBS->FreePool (SUCInfo->UserPassword->InputString);
      gBS->FreePool (SUCInfo->UserPassword);
      SUCInfo->UserPassword = NULL;
     }
  }

  gSUBrowser->HiiDatabase->RemovePackageList (gSUBrowser->HiiDatabase, HiiHandle);
  DestroyHiiDriverHandle (DriverHandle);
  Status = UninstallSetupUtilityBrowserProtocol ();

  return Status;
}

/**
 Installs the SetupUtilityBrowser protocol including allocating
 storage for variable record data.

 @param [in]   This

 @retval EFI_SUCEESS            Protocol was successfully installed
 @retval EFI_OUT_OF_RESOURCES   Not enough resource to allocate data structures
 @return Other                  Some other error occured

**/
EFI_STATUS
InstallSetupUtilityBrowserProtocol (
  IN  EFI_SETUP_UTILITY_PROTOCOL            *This
  )
{
  EFI_STATUS                                 Status;

  gSUBrowser->Interface.AtRoot        = TRUE;
  gSUBrowser->Interface.Finished      = FALSE;
  gSUBrowser->Interface.Guid          = &mFormSetGuid;
  gSUBrowser->Interface.UseMenus      = FALSE;
  gSUBrowser->Interface.Direction     = NoChange;
  gSUBrowser->Interface.CurRoot       = FALSE;
  gSUBrowser->Interface.MenuItemCount = FALSE;
  gSUBrowser->Interface.Size          = PcdGet32 (PcdSetupConfigSize);
  gSUBrowser->Interface.Firstin       = TRUE;
  gSUBrowser->Interface.Changed       = FALSE;
  gSUBrowser->Interface.JumpToFirstOption = TRUE;

  gSUBrowser->Interface.SCBuffer      = NULL;
  gSUBrowser->Interface.MyIfrNVData = NULL;
  gSUBrowser->Interface.PreviousMenuEntry = 0;
  //
  // Install the Protocol
  //
  gSUBrowser->Handle = NULL;
  Status = gBS->InstallProtocolInterface (
                  &gSUBrowser->Handle,
                  &gEfiSetupUtilityBrowserProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  &gSUBrowser->Interface
                  );

  ASSERT_EFI_ERROR (Status);

  return EFI_SUCCESS;
}

/**
 Uninstalls the DeviceManagerSetup protocol and frees memory
 used for storing variable record data.

 @param None

 @retval EFI_SUCEESS            Protocol was successfully installed
 @retval EFI_ALREADY_STARTED    Protocol was already installed
 @retval EFI_OUT_OF_RESOURCES   Not enough resource to allocate data structures
 @return Other                  Some other error occured

**/
EFI_STATUS
UninstallSetupUtilityBrowserProtocol (
  VOID
  )
{
  EFI_STATUS                                Status;

  Status = gBS->UninstallProtocolInterface (
                  gSUBrowser->Handle,
                  &gEfiSetupUtilityBrowserProtocolGuid,
                  &gSUBrowser->Interface
                  );

  if ( EFI_ERROR(Status) ) {
    return Status;
  }
  if (gSUBrowser->SCBuffer != NULL) {
    gBS->FreePool (gSUBrowser->SCBuffer);
  }


  if (gSUBrowser->SUCInfo->HddPasswordScuData  != NULL) {
    if (gSUBrowser->SUCInfo->HddPasswordScuData->NumOfEntry != 0) {
      if (gSUBrowser->SUCInfo->HddPasswordScuData[0].HddInfo != NULL) {
        gBS->FreePool (gSUBrowser->SUCInfo->HddPasswordScuData[0].HddInfo);
      }
      gBS->FreePool (gSUBrowser->SUCInfo->HddPasswordScuData);
    }
  }
  if (gSUBrowser->SUCInfo->SupervisorPassword  != NULL) {
    if (gSUBrowser->SUCInfo->SupervisorPassword->NumOfEntry != 0) {
      gBS->FreePool (gSUBrowser->SUCInfo->SupervisorPassword->InputString);
      gBS->FreePool (gSUBrowser->SUCInfo->SupervisorPassword);
    }
  }

  if (gSUBrowser->SUCInfo->UserPassword  != NULL) {
    if (gSUBrowser->SUCInfo->UserPassword->NumOfEntry != 0) {
      gBS->FreePool (gSUBrowser->SUCInfo->UserPassword->InputString);
      gBS->FreePool (gSUBrowser->SUCInfo->UserPassword);
    }
  }

  gBS->FreePool (gSUBrowser->SUCInfo);
  gBS->FreePool (gSUBrowser);

  return EFI_SUCCESS;
}

/**
 Call the browser and display the SetupUtility

 @param None

**/
EFI_STATUS
CallSetupUtilityBrowser (
  VOID
  )
{
  EFI_STATUS                                Status;
  UINTN                                     BufferSize;
  EFI_HANDLE                                DispPage;
  BOOLEAN                                   SetupUtilityBrowserEmpty;
  BOOLEAN                                   Continue;
  EFI_BROWSER_ACTION_REQUEST                ResetRequired;
  EFI_STRING_ID                             TempToken;
  UINTN                                     Index;
  EFI_HII_PACKAGE_LIST_HEADER               *Buffer;
  EFI_HII_DATABASE_PROTOCOL                 *HiiDatabase;
  EFI_FORM_BROWSER2_PROTOCOL                 *Browser2;
  EFI_SETUP_MOUSE_PROTOCOL                   *SetupMouse;
  EFI_IFR_FORM_SET                          *FormSetPtr;
  UINT8                                      *TempPtr;


  Status = EFI_SUCCESS;
  SetupUtilityBrowserEmpty = TRUE;
  Buffer = NULL;
  gCallbackKey = 0;
  Continue = TRUE;
  HiiDatabase = gSUBrowser->HiiDatabase;
  Browser2    = gSUBrowser->Browser2;
  SetupMouse  = NULL;

  for (Index = 0; Index < MAX_HII_HANDLES && gSUBrowser->SUCInfo->MapTable[Index].HiiHandle != NULL; Index++) {
    //
    // Am not initializing Buffer since the first thing checked is the size
    // this way I can get the real buffersize in the smallest code size
    //
    BufferSize = 0;
    Status = HiiDatabase->ExportPackageLists (
                            HiiDatabase,
                            gSUBrowser->SUCInfo->MapTable[Index].HiiHandle,
                            &BufferSize,
                            Buffer
                            );

    if (Status == EFI_NOT_FOUND) {
      break;
    }

    //
    // BufferSize should have the real size of the forms now
    //
    Buffer = AllocateZeroPool (BufferSize);
    if (Buffer == NULL) {
      Status = EFI_OUT_OF_RESOURCES;
      ASSERT_EFI_ERROR (Status);
      return Status;
    }
    //
    // Am not initializing Buffer since the first thing checked is the size
    // this way I can get the real buffersize in the smallest code size
    //
    Status = HiiDatabase->ExportPackageLists (
                            HiiDatabase,
                            gSUBrowser->SUCInfo->MapTable[Index].HiiHandle,
                            &BufferSize,
                            Buffer
                            );
    if (!IsSupportedFormSet (Buffer)) {
      gBS->FreePool (Buffer);
      continue;
    }

    //
    // Skips the header, now points to data
    //

    TempPtr = (UINT8 *) (Buffer + 1);
    TempPtr = (UINT8 *) ((EFI_HII_FORM_PACKAGE_HDR *) TempPtr + 1);
    FormSetPtr = (EFI_IFR_FORM_SET *) TempPtr;
    SetupUtilityBrowserEmpty = FALSE;


    if (gSUBrowser->Interface.MenuItemCount < MAX_ITEMS) {
      TempToken = FormSetPtr->FormSetTitle;
      gSUBrowser->Interface.MenuList[gSUBrowser->Interface.MenuItemCount].MenuTitle = TempToken;
      //
      // Set the display page.  Last page found is the first to be displayed.
      //
      gSUBrowser->Interface.MenuList[gSUBrowser->Interface.MenuItemCount].Page = gSUBrowser->SUCInfo->MapTable[Index].HiiHandle;
      //
      // NULL out the string pointer
      //
      gSUBrowser->Interface.MenuList[gSUBrowser->Interface.MenuItemCount].String = NULL;
      //
      // Entry is filled out so update count
      //
      gSUBrowser->Interface.MenuItemCount++;
    }
    gBS->FreePool(Buffer);
  }

  //
  // Drop the TPL level from TPL_APPLICATION+2 to TPL_APPLICATION
  //
  gBS->RestoreTPL (TPL_APPLICATION);

  //
  // If we didn't add anything - don't bother going to device manager
  //
  if (!SetupUtilityBrowserEmpty) {
    Status = gBS->LocateProtocol (
                    &gSetupMouseProtocolGuid,
                    NULL,
                    (VOID **)&(gSUBrowser->Interface.SetupMouse)
                    );
    if (!EFI_ERROR (Status)) {
      gSUBrowser->Interface.SetupMouseFlag = TRUE;
      SetupMouse = (EFI_SETUP_MOUSE_PROTOCOL *) gSUBrowser->Interface.SetupMouse;
      Status = SetupMouse->Start (SetupMouse);
      if (EFI_ERROR (Status)) {
        SetupMouse = NULL;
        gSUBrowser->Interface.SetupMouseFlag = FALSE;
      }
    }
    //
    // Init before root page loop
    //
    gSUBrowser->Interface.AtRoot    = TRUE;
    gSUBrowser->Interface.CurRoot   = gSUBrowser->Interface.MenuItemCount - 1;
    gSUBrowser->Interface.UseMenus  = TRUE;
    gSUBrowser->Interface.Direction = NoChange;
    DispPage                       = gSUBrowser->Interface.MenuList[gSUBrowser->Interface.MenuItemCount - 1].Page;

    //
    // Loop until exit condition is found.  Use direction indicators and
    // the menu list to determine what root page needs to be displayed.
    //
    while (Continue) {
      Status = Browser2->SendForm (
                           Browser2,
                           (EFI_HII_HANDLE *) &DispPage,
                           1,
                           &mFormSetClassGuid,
                           1,
                           NULL,
                           &ResetRequired
                           );

      if (ResetRequired == EFI_BROWSER_ACTION_REQUEST_RESET) {
        gBS->RaiseTPL (TPL_NOTIFY);
        gRT->ResetSystem(EfiResetCold, EFI_SUCCESS, 0, NULL);
      } else if (ResetRequired == EFI_BROWSER_ACTION_REQUEST_EXIT) {
        gSUBrowser->Interface.UseMenus = FALSE;
        break;
      }

      //
      // Force return to Device Manager or Exit if finished
      //
      gSUBrowser->Interface.AtRoot  = TRUE;
      gSUBrowser->Interface.Firstin = FALSE;
      if (gSUBrowser->Interface.Finished) {
        // Need to set an exit at this point
        gSUBrowser->Interface.UseMenus = FALSE;
        Continue = FALSE;
        break;
      }

      //
      // Check for next page or exit states
      //
      switch (gSUBrowser->Interface.Direction) {

     case Right:
        //
        // Update Current Root Index
        //
        if (gSUBrowser->Interface.CurRoot == 0) {
          gSUBrowser->Interface.CurRoot = gSUBrowser->Interface.MenuItemCount - 1;
        } else {
          gSUBrowser->Interface.CurRoot--;
        }
        //
        // Set page to display
        //
        DispPage = gSUBrowser->Interface.MenuList[gSUBrowser->Interface.CurRoot].Page;
        //
        // Update Direction Flag
        //
        gSUBrowser->Interface.Direction = NoChange;
        break;

      case Left:
        //
        // Update Current Root Index
        //
        if (gSUBrowser->Interface.CurRoot == gSUBrowser->Interface.MenuItemCount - 1) {
          gSUBrowser->Interface.CurRoot = 0;
        } else {
          gSUBrowser->Interface.CurRoot++;
        }
        //
        // Set page to display
        //
        DispPage = gSUBrowser->Interface.MenuList[gSUBrowser->Interface.CurRoot].Page;
        //
        // Update Direction Flag
        //
        gSUBrowser->Interface.Direction = NoChange;
        break;

      case Jump:
        //
        // Update Current Root Index
        //
        if (gSUBrowser->Interface.CurRoot <= gSUBrowser->Interface.MenuItemCount - 1) {
          //
          // Set page to display
          //
          DispPage = gSUBrowser->Interface.MenuList[gSUBrowser->Interface.CurRoot].Page;
        }
        gSUBrowser->Interface.Direction = NoChange;
        break;

      default:
        break;
      }

    }
  }
  if ((SetupMouse != NULL) && (gSUBrowser->Interface.SetupMouseFlag == TRUE)) {
    Status = SetupMouse->Close (SetupMouse);
  }
  //
  // We are exiting so clear the screen
  //
  gST->ConOut->SetAttribute (gST->ConOut, EFI_BLACK | EFI_BACKGROUND_BLACK);
  gST->ConOut->ClearScreen (gST->ConOut);

  gBS->RaiseTPL (TPL_APPLICATION+2);   // TPL_APPLICATION+2 = EFI_TPL_DRIVER
  return Status;
}

VOID
EFIAPI
ScuDataInitNotifyFn (
  IN EFI_EVENT                             Event,
  IN VOID                                  *Context
  )
{
  gBS->CloseEvent (Event);
  CreateScuData (FALSE);
  DestroyScuData(FALSE);
}

EFI_STATUS
GetSystemConfigurationVar (
  IN OUT SETUP_UTILITY_DATA                 *SetupData,
  IN OUT RC_SETUP_UTILITY_DATA              *RcSetupData
  )
{
  EFI_STATUS                                Status;
  EFI_STATUS                                GetSetupStatus;
  EFI_STATUS                                GetSaSetupStatus;
  EFI_STATUS                                GetMeSetupStatus;
  EFI_STATUS                                GetCpuSetupStatus;
  EFI_STATUS                                GetPchSetupStatus;
  EFI_STATUS                                GetMeSetupStorageStatus;
  EFI_STATUS                                GetSetupDataStatus;
  EFI_STATUS                                GetSiSetupStatus;
  UINTN                                     BufferSize;
  CHIPSET_CONFIGURATION                     *SetupNvData;
  UINTN                                     Index;
  UINT16                                    Timeout;
  UINTN                                     SaBufferSize;
  SA_SETUP                                  *SaSetupNvData;
  UINTN                                     MeBufferSize;
  ME_SETUP                                  *MeSetupNvData;
  UINTN                                     CpuBufferSize;
  CPU_SETUP                                 *CpuSetupNvData;
  UINTN                                     PchBufferSize;
  PCH_SETUP                                 *PchSetupNvData;
  UINTN                                     MeStorageBufferSize;
  ME_SETUP_STORAGE                          *MeSetupStorageNvData;
  UINTN                                     SetupDataBufferSize;
  SETUP_DATA                                *SetupDataNvData;
  UINTN                                     SiBufferSize;
  SI_SETUP                                 *SiSetupNvData;
  EFI_EVENT                                 Event;
  VOID                                      *Registration;
//  SGX_SETUP_DATA                            *SgxSetupData;
//  UINTN                                     SgxSetupDataSize;
//   UINT32                                    Data32;
//   VMD_INFO_HOB                              *VmdInfoHob;
//
//   VmdInfoHob = NULL;
  BOOLEAN                                   ResetRequired;
//[-start-201127-IB17510129-add]//
  BOOLEAN                                   IsFirstBoot;
  GET_FIPS_MODE_DATA                        FipsModeData;
  PCH_RESET_DATA                            ResetData;
  EFI_BOOT_MODE                             BootMode;
  UINT8                                     MeSetupStorageInitFlag;
  UINTN                                     MeSetupStorageInitFlagSize;
  EFI_STATUS                                MeSetupStorageInitFlagStatus;
  UINT8                                     MeSetupInitFlag;
  UINTN                                     MeSetupInitFlagSize;
  EFI_STATUS                                MeSetupInitFlagStatus;
  UINT8                                     SetupInitFlag;
  UINTN                                     SetupInitFlagSize;
  EFI_STATUS                                SetupInitFlagStatus;

  IsFirstBoot             = FALSE;
//[-end-201127-IB17510129-add]//

  Index = 0;
  GetSetupStatus          = EFI_NOT_FOUND;
  GetSaSetupStatus        = EFI_NOT_FOUND;
  GetMeSetupStatus        = EFI_NOT_FOUND;
  GetCpuSetupStatus       = EFI_NOT_FOUND;
  GetPchSetupStatus       = EFI_NOT_FOUND;
  GetMeSetupStorageStatus = EFI_NOT_FOUND;
  GetSetupDataStatus      = EFI_NOT_FOUND;
  GetSiSetupStatus        = EFI_NOT_FOUND;
  ResetRequired           = FALSE;
  BootMode                = GetBootModeHob ();

  //
  // Check the setup variable was create or not, if not then create default setup variable.
  //
  BufferSize = PcdGet32 (PcdSetupConfigSize);
  Status = gBS->AllocatePool(
                  EfiACPIMemoryNVS,
                  BufferSize,
                  (VOID **)&SetupNvData
                  );
  if (EFI_ERROR(Status)) {
    return Status;
  }
  ZeroMem (SetupNvData, BufferSize);

  {
    //============================================//
    // Allocate a memory to put SA setup variable //
    //============================================//
    SaBufferSize = sizeof (SA_SETUP);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    SaBufferSize,
                    (VOID **)&SaSetupNvData
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (SaSetupNvData, SaBufferSize);
    //============================================//
    // Allocate a memory to put ME setup variable //
    //============================================//
    MeBufferSize = sizeof (ME_SETUP);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    MeBufferSize,
                    (VOID **)&MeSetupNvData
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (MeSetupNvData, MeBufferSize);
    //=============================================//
    // Allocate a memory to put CPU setup variable //
    //=============================================//
    CpuBufferSize = sizeof (CPU_SETUP);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    CpuBufferSize,
                    (VOID **)&CpuSetupNvData
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (CpuSetupNvData, CpuBufferSize);
    //=============================================//
    // Allocate a memory to put PCH setup variable //
    //=============================================//
    PchBufferSize = sizeof (PCH_SETUP);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    PchBufferSize,
                    (VOID **)&PchSetupNvData
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (PchSetupNvData, PchBufferSize);
    //====================================================//
    // Allocate a memory to put ME setup storage variable //
    //====================================================//
    MeStorageBufferSize = sizeof (ME_SETUP_STORAGE);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    MeStorageBufferSize,
                    (VOID **)&MeSetupStorageNvData
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (MeSetupStorageNvData, MeStorageBufferSize);
    //==============================================//
    // Allocate a memory to put SETUP DATA variable //
    //==============================================//
    SetupDataBufferSize = sizeof (SETUP_DATA);
    Status = gBS->AllocatePool (
                    EfiACPIMemoryNVS,
                    SetupDataBufferSize,
                    (VOID **)&SetupDataNvData
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (SetupDataNvData, SetupDataBufferSize);
    //=============================================//
    // Allocate a memory to put SI setup variable //
    //=============================================//
    SiBufferSize = sizeof (SI_SETUP);
    Status = gBS->AllocatePool(
                    EfiACPIMemoryNVS,
                    SiBufferSize,
                    (VOID **)&SiSetupNvData
                    );
    if (EFI_ERROR(Status)) {
      return Status;
    }
    ZeroMem (SiSetupNvData, SiBufferSize);
  }

  //
  // Save default setup by variable
  //
  if (AsfResetSetupData() == FALSE) {
    GetSetupStatus = gRT->GetVariable (
                            L"Setup",
                            &mFormSetGuid,
                            NULL,
                            &BufferSize,
                            (VOID *)SetupNvData
                            );
    if (!EFI_ERROR (GetSetupStatus) &&
        !IsVariableInVariableStoreRegion (L"Setup", &mFormSetGuid)) {
      GetSetupStatus = EFI_NOT_FOUND;
    }
    GetSaSetupStatus = gRT->GetVariable (
                              SA_SETUP_VARIABLE_NAME,
                              &gSaSetupVariableGuid,
                              NULL,
                              &SaBufferSize,
                              (VOID *)SaSetupNvData
                              );
    if (!EFI_ERROR (GetSaSetupStatus) &&
        !IsVariableInVariableStoreRegion (SA_SETUP_VARIABLE_NAME, &gSaSetupVariableGuid)) {
      GetSaSetupStatus = EFI_NOT_FOUND;
    }
    GetMeSetupStatus = gRT->GetVariable (
                              ME_SETUP_VARIABLE_NAME,
                              &gMeSetupVariableGuid,
                              NULL,
                              &MeBufferSize,
                              (VOID *)MeSetupNvData
                              );
    if (!EFI_ERROR (GetMeSetupStatus) &&
        !IsVariableInVariableStoreRegion (ME_SETUP_VARIABLE_NAME, &gMeSetupVariableGuid)) {
      GetMeSetupStatus = EFI_NOT_FOUND;
    } else if (!EFI_ERROR (GetMeSetupStatus)) {
      MeSetupInitFlagSize  = sizeof (MeSetupInitFlag);
      MeSetupInitFlag      = 0;
      MeSetupInitFlagStatus = gRT->GetVariable (
                                       L"MeSetupInitFlag",
                                       &gMeSetupVariableGuid,
                                       NULL,
                                       &MeSetupInitFlagSize,
                                       &MeSetupInitFlag
                                       );
      if (!EFI_ERROR (MeSetupInitFlagStatus) && (MeSetupInitFlag == 1)) {
        GetMeSetupStatus = EFI_ALREADY_STARTED;
      }
    }
    GetCpuSetupStatus = gRT->GetVariable (
                               CPU_SETUP_VARIABLE_NAME,
                               &gCpuSetupVariableGuid,
                               NULL,
                               &CpuBufferSize,
                               (VOID *)CpuSetupNvData
                               );
    if (!EFI_ERROR (GetCpuSetupStatus) &&
        !IsVariableInVariableStoreRegion (CPU_SETUP_VARIABLE_NAME, &gCpuSetupVariableGuid)) {
      GetCpuSetupStatus = EFI_NOT_FOUND;
    }
    GetPchSetupStatus = gRT->GetVariable (
                               PCH_SETUP_VARIABLE_NAME,
                               &gPchSetupVariableGuid,
                               NULL,
                               &PchBufferSize,
                               (VOID *)PchSetupNvData
                               );
    if (!EFI_ERROR (GetPchSetupStatus) &&
        !IsVariableInVariableStoreRegion (PCH_SETUP_VARIABLE_NAME, &gPchSetupVariableGuid)) {
      GetPchSetupStatus = EFI_NOT_FOUND;
    }
    GetMeSetupStorageStatus = gRT->GetVariable (
                                     ME_SETUP_STORAGE_VARIABLE_NAME,
                                     &gMeSetupVariableGuid,
                                     NULL,
                                     &MeStorageBufferSize,
                                     (VOID *)MeSetupStorageNvData
                                     );
    if (!EFI_ERROR (GetMeSetupStorageStatus) &&
        !IsVariableInVariableStoreRegion (ME_SETUP_STORAGE_VARIABLE_NAME, &gMeSetupVariableGuid)) {
//[-start-201127-IB17510129-add]//
      IsFirstBoot = TRUE;
//[-end-201127-IB17510129-add]//
      GetMeSetupStorageStatus = EFI_NOT_FOUND;
    } else if (!EFI_ERROR (GetMeSetupStorageStatus)) {
      MeSetupStorageInitFlagSize  = sizeof (MeSetupStorageInitFlag);
      MeSetupStorageInitFlag      = 0;
      MeSetupStorageInitFlagStatus = gRT->GetVariable (
                                       L"MeSetupStorageInitFlag",
                                       &gMeSetupVariableGuid,
                                       NULL,
                                       &MeSetupStorageInitFlagSize,
                                       &MeSetupStorageInitFlag
                                       );
      if (!EFI_ERROR (GetMeSetupStorageStatus) && (MeSetupStorageInitFlag == 1)) {
        GetMeSetupStorageStatus = EFI_ALREADY_STARTED;
        IsFirstBoot = TRUE;
      }
    }
    
    GetSetupDataStatus = gRT->GetVariable (
                                PLATFORM_SETUP_VARIABLE_NAME,
                                &gSetupVariableGuid,
                                NULL,
                                &SetupDataBufferSize,
                                (VOID *)SetupDataNvData
                                );
    if (!EFI_ERROR (GetSetupDataStatus) &&
        !IsVariableInVariableStoreRegion (PLATFORM_SETUP_VARIABLE_NAME, &gSetupVariableGuid)) {
      GetSetupDataStatus = EFI_NOT_FOUND;
    } else if (!EFI_ERROR (GetSetupDataStatus)) {
      SetupInitFlagSize  = sizeof (SetupInitFlag);
      SetupInitFlag      = 0;
      SetupInitFlagStatus = gRT->GetVariable (
                                       L"SetupInitFlag",
                                       &gSetupVariableGuid,
                                       NULL,
                                       &SetupInitFlagSize,
                                       &SetupInitFlag
                                       );
      if (!EFI_ERROR (SetupInitFlagStatus) && (SetupInitFlag == 1)) {
        GetSetupDataStatus = EFI_ALREADY_STARTED;
      }
    }
    GetSiSetupStatus = gRT->GetVariable (
                               SI_SETUP_VARIABLE_NAME,
                               &gSiSetupVariableGuid,
                               NULL,
                               &SiBufferSize,
                               (VOID *)SiSetupNvData
                               );
    if (!EFI_ERROR (GetSiSetupStatus) &&
        !IsVariableInVariableStoreRegion (SI_SETUP_VARIABLE_NAME, &gSiSetupVariableGuid)) {
      GetSiSetupStatus = EFI_NOT_FOUND;
    }
    if (EFI_ERROR (GetSetupStatus) || EFI_ERROR (GetSaSetupStatus) || EFI_ERROR (GetMeSetupStatus) ||
        EFI_ERROR (GetCpuSetupStatus) || EFI_ERROR (GetPchSetupStatus) || EFI_ERROR (GetMeSetupStorageStatus) ||
        EFI_ERROR(GetSetupDataStatus) || EFI_ERROR (GetSiSetupStatus)) {
      //
      // Only fist boot or setup variable missing, Install HII data and init some variable when BDS connect
      //
      Status = gBS->CreateEvent (
                    EVT_NOTIFY_SIGNAL,
                    TPL_CALLBACK - 1,
                    ScuDataInitNotifyFn,
                    NULL,
                    &Event
                    );
      if (!EFI_ERROR (Status)) {
        Status = gBS->RegisterProtocolNotify (
                        &gBdsAllDriversConnectedProtocolGuid,
                        Event,
                        &Registration
                        );
      }
      Status = EFI_ABORTED;
    }
  } else {
    Status = EFI_NOT_FOUND;
  }

  if (EFI_ERROR (Status)) {
    //
    // "Setup" Variable doesn't exist,so get a buffer with default variable
    //
    if (EFI_ERROR(GetSetupStatus)) {
      DefaultSetup (SetupNvData);
    }
    if (EFI_ERROR(GetSaSetupStatus)) {
      DefaultSaSetup (&SaSetupNvData, EfiACPIMemoryNVS);
    }
    if (EFI_ERROR(GetMeSetupStatus) && (GetMeSetupStorageStatus != EFI_ALREADY_STARTED)) {
      DefaultMeSetup (&MeSetupNvData, EfiACPIMemoryNVS);
    }
    if (EFI_ERROR(GetCpuSetupStatus)) {
      DefaultCpuSetup (&CpuSetupNvData, EfiACPIMemoryNVS);
    }
    if (EFI_ERROR(GetPchSetupStatus)) {
      DefaultPchSetup (&PchSetupNvData, EfiACPIMemoryNVS);
    }
    if (EFI_ERROR(GetSetupDataStatus) && (GetMeSetupStorageStatus != EFI_ALREADY_STARTED)) {
      DefaultSetupData (&SetupDataNvData, EfiACPIMemoryNVS);
    }
    if (EFI_ERROR(GetSiSetupStatus)) {
      DefaultSiSetup (&SiSetupNvData, EfiACPIMemoryNVS);
    }
//[-start-201127-IB17510127-add]//
    if (EFI_ERROR(GetMeSetupStorageStatus) && (GetMeSetupStorageStatus != EFI_ALREADY_STARTED)) {
      DefaultMeSetupStorage (&MeSetupStorageNvData, EfiACPIMemoryNVS);
    }
//[-end-201127-IB17510127-add]//

//     // VMD: Reading CapId0_B for VMD support
//     Data32 = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_CAPID0_B));
//     DEBUG ((EFI_D_INFO, "CAPID0_B value is 0x%x\n", Data32));
//     if (!(Data32 & V_SA_MC_CAPID0_B_VMD_DIS)) { // VMD is enabled
//
//       SaSetupNvData->VmdSupported = 1;
//       //
//       // Update VMD Volatile variables
//       //
//       VmdInfoHob = (VMD_INFO_HOB *) GetFirstGuidHob (&gVmdInfoHobGuid);
//       if (VmdInfoHob != NULL) {
//         SaSetupNvData->VmdPortAPresent = VmdInfoHob->VmdPortInfo.PortInfo[0].DeviceDetected;
//         SaSetupNvData->VmdPortBPresent = VmdInfoHob->VmdPortInfo.PortInfo[1].DeviceDetected;
//         SaSetupNvData->VmdPortCPresent = VmdInfoHob->VmdPortInfo.PortInfo[2].DeviceDetected;
//         SaSetupNvData->VmdPortDPresent = VmdInfoHob->VmdPortInfo.PortInfo[3].DeviceDetected;
//
//         DEBUG ((EFI_D_INFO, "Vmd Info:\n"));
//         DEBUG ((EFI_D_INFO, "SaSetupNvData->VmdPortAPresent = %d  \n", VmdInfoHob->VmdPortInfo.PortInfo[0].DeviceDetected));
//         DEBUG ((EFI_D_INFO, "SaSetupNvData->VmdPortBPresent = %d  \n", VmdInfoHob->VmdPortInfo.PortInfo[1].DeviceDetected));
//         DEBUG ((EFI_D_INFO, "SaSetupNvData->VmdPortCPresent = %d  \n", VmdInfoHob->VmdPortInfo.PortInfo[2].DeviceDetected));
//         DEBUG ((EFI_D_INFO, "SaSetupNvData->VmdPortDPresent = %d  \n", VmdInfoHob->VmdPortInfo.PortInfo[3].DeviceDetected));
//       } else {
//         DEBUG ((EFI_D_INFO, "Vmd Info Hob not found\n"));
//       }
//     }

    gRT->SetVariable (
           L"CustomPlatformLang",
           &gEfiGenericVariableGuid,
           EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
           AsciiStrSize ((CHAR8 *) PcdGetPtr (PcdUefiVariableDefaultPlatformLang)),
           (VOID *) PcdGetPtr (PcdUefiVariableDefaultPlatformLang)
           );

    Timeout = (UINT16) PcdGet16 (PcdPlatformBootTimeOut);
    gRT->SetVariable (
           L"Timeout",
           &gEfiGlobalVariableGuid,
           EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
           sizeof (UINT16),
           (VOID *) &Timeout
           );
    //
    // Load default required
    //
    SetupRuntimeDetermination (
      SetupNvData, GetSetupStatus,
      SaSetupNvData, GetSaSetupStatus,
      MeSetupNvData, GetMeSetupStatus,
      CpuSetupNvData, GetCpuSetupStatus,
      PchSetupNvData, GetPchSetupStatus,
      MeSetupStorageNvData, GetMeSetupStorageStatus,
      SetupDataNvData, GetSetupDataStatus,
      SiSetupNvData, GetSiSetupStatus
      );
  }

//[-start-201127-IB17510129-add]//
  //
  // if FipsModeSelect is disabled, FipsAutoSync must be disabled, either.
  //
  if (MeSetupStorageNvData->FipsModeSelect == FALSE) {
    SetupNvData->FipsAutoSync = FALSE;
  }

  if (IsFirstBoot || SetupNvData->FipsAutoSync) {
    //
    // get FIPS status
    //
    Status = HeciGetFipsMode (&FipsModeData);
    if (!EFI_ERROR(Status) && FipsModeData.FipsMode != MeSetupStorageNvData->FipsModeSelect) {
      //
      // set HECI, then reset
      //
      Status = HeciSetFipsMode(MeSetupStorageNvData->FipsModeSelect);
      if (!EFI_ERROR(Status)) {
        //
        // global reset
        //
        CopyMem (&ResetData.Guid, &gPchGlobalResetGuid, sizeof (EFI_GUID));
        StrCpyS (ResetData.Description, PCH_RESET_DATA_STRING_MAX_LENGTH, PCH_PLATFORM_SPECIFIC_RESET_STRING);
        gRT->ResetSystem (EfiResetPlatformSpecific, EFI_SUCCESS, sizeof (PCH_RESET_DATA), &ResetData);
      }
    } 
  }
//[-end-201127-IB17510129-add]//

  //
  // Update variable in every boot (e.g. MemorySize change and modify SCU settings)
  //
  SetupRuntimeUpdateEveryBoot (SetupNvData, SetupDataNvData, SaSetupNvData, MeSetupNvData, CpuSetupNvData, PchSetupNvData, SiSetupNvData, MeSetupStorageNvData);
  SetupNvData->DefaultBootType = DEFAULT_BOOT_FLAG;

  //
  // Save Setup variable.
  //
//[-start-190702-16990088-add]//
  BufferSize = PcdGet32 (PcdSetupConfigSize);
//[-end-190702-16990088-add]//
  if (BootMode != BOOT_IN_RECOVERY_MODE) {
    Status = SaveSetupConfig (
               L"Setup",
               &mFormSetGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               BufferSize,
               (VOID *) SetupNvData
               );
  }

  {
    if ((FeaturePcdGet (PcdHybridGraphicsSupported)) &&
        (FeaturePcdGet (PcdNvidiaOptimusSupported)) &&
        (FeaturePcdGet (PcdHgNvidiaDdsFeatureSupport))) {
        //
        // Check "PrimaryDisplay" setting need to be changed or not.
        // System need a reset if "PrimaryDisplay" need to be changed.
        // Scenario 1: The default settings of "PrimaryDisplay" and "DisplayMode"
        //             are different during first boot.
        //
        switch (SetupNvData->DisplayMode) {
          case IgfxOnly:
            if (SaSetupNvData->PrimaryDisplay != DisplayModeIgpu) {
              SaSetupNvData->PrimaryDisplay = DisplayModeIgpu;
              ResetRequired = TRUE;
            }
            break;
          case DgpuOnly:
            if (IsPchH ()) {
              if (SaSetupNvData->PrimaryDisplay != DisplayModeDgpu) {
                SaSetupNvData->PrimaryDisplay = DisplayModeDgpu;
                ResetRequired = TRUE;
              }
            } else {
              if (SaSetupNvData->PrimaryDisplay != DisplayModePci) {
                SaSetupNvData->PrimaryDisplay = DisplayModePci;
                ResetRequired = TRUE;
              }
            }
            break;
          case MsHybrid:
            if (SaSetupNvData->PrimaryDisplay != DisplayModeHg) {
              SaSetupNvData->PrimaryDisplay = DisplayModeHg;
              ResetRequired = TRUE;
            }
            break;
          case Dynamic:
            if (SaSetupNvData->PrimaryDisplay != DisplayModeHg) {
              SaSetupNvData->PrimaryDisplay = DisplayModeHg;
              ResetRequired = TRUE;
            }
            break;
          default:
            break;
        }
    }
    //=================================================================================//
    // Save SA setup variable and update variable address to Rc setup utility variable //
    //=================================================================================//
//[-start-190702-16990088-add]//
    SaBufferSize = sizeof (SA_SETUP);
//[-end-190702-16990088-add]//
    if (BootMode != BOOT_IN_RECOVERY_MODE) {
      Status = SaveSetupConfig (
                 SA_SETUP_VARIABLE_NAME,
                 &gSaSetupVariableGuid,
                 EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                 SaBufferSize,
                 (VOID *) SaSetupNvData
                 );
    }
    RcSetupData->SaSetupNvData = (UINT8 *)SaSetupNvData;
    if ((FeaturePcdGet (PcdHybridGraphicsSupported)) &&
        (FeaturePcdGet (PcdNvidiaOptimusSupported)) &&
        (FeaturePcdGet (PcdHgNvidiaDdsFeatureSupport))) {
        if (ResetRequired) {
          gRT->ResetSystem (EfiResetCold, EFI_SUCCESS, 0, NULL);
        }
    }
    //=================================================================================//
    // Save ME setup variable and update variable address to Rc setup utility variable //
    //=================================================================================//
//[-start-190702-16990088-add]//
    MeBufferSize = sizeof (ME_SETUP);
//[-end-190702-16990088-add]//
    if (BootMode != BOOT_IN_RECOVERY_MODE) {
      Status = SaveSetupConfig (
                 ME_SETUP_VARIABLE_NAME,
                 &gMeSetupVariableGuid,
                 EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                 MeBufferSize,
                 (VOID *) MeSetupNvData
                 );
    }
    RcSetupData->MeSetupNvData = (UINT8 *)MeSetupNvData;
    //==================================================================================//
    // Save CPU setup variable and update variable address to Rc setup utility variable //
    //==================================================================================//
//[-start-190702-16990088-add]//
    CpuBufferSize = sizeof (CPU_SETUP);
//[-end-190702-16990088-add]//
    if (BootMode != BOOT_IN_RECOVERY_MODE) {
      Status = SaveSetupConfig (
                 CPU_SETUP_VARIABLE_NAME,
                 &gCpuSetupVariableGuid,
                 EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                 CpuBufferSize,
                 (VOID *) CpuSetupNvData
                 );
    }
    RcSetupData->CpuSetupNvData = (UINT8 *)CpuSetupNvData;
    //==================================================================================//
    // Save PCH setup variable and update variable address to Rc setup utility variable //
    //==================================================================================//
//[-start-190702-16990088-add]//
    PchBufferSize = sizeof (PCH_SETUP);
//[-end-190702-16990088-add]//
    if (BootMode != BOOT_IN_RECOVERY_MODE) {
      Status = SaveSetupConfig (
                 PCH_SETUP_VARIABLE_NAME,
                 &gPchSetupVariableGuid,
                 EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                 PchBufferSize,
                 (VOID *) PchSetupNvData
                 );
    }
    RcSetupData->PchSetupNvData = (UINT8 *)PchSetupNvData;
    //=========================================================================================//
    // Save ME setup storage variable and update variable address to Rc setup utility variable //
    //=========================================================================================//
//[-start-190702-16990088-add]//
    MeStorageBufferSize = sizeof (ME_SETUP_STORAGE);
//[-end-190702-16990088-add]//
    if (BootMode != BOOT_IN_RECOVERY_MODE) {
      Status = SaveSetupConfig (
                 ME_SETUP_STORAGE_VARIABLE_NAME,
                 &gMeSetupVariableGuid,
                 EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                 MeStorageBufferSize,
                 (VOID *) MeSetupStorageNvData
                 );
    }
    RcSetupData->MeSetupStorageNvData = (UINT8 *)MeSetupStorageNvData;
    //===================================================================================//
    // Save SETUP DATA variable and update variable address to Rc setup utility variable //
    //===================================================================================//
//[-start-190702-16990088-add]//
    SetupDataBufferSize = sizeof (SETUP_DATA);
//[-end-190702-16990088-add]//
    if (BootMode != BOOT_IN_RECOVERY_MODE) {
      Status = SaveSetupConfig (
                 PLATFORM_SETUP_VARIABLE_NAME,
                 &gSetupVariableGuid,
                 EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                 SetupDataBufferSize,
                 (VOID *) SetupDataNvData
                 );
    }
    RcSetupData->SetupDataNvData = (UINT8 *)SetupDataNvData;
    //==================================================================================//
    // Save SI setup variable and update variable address to Rc setup utility variable //
    //==================================================================================//
//[-start-190702-16990088-add]//
    SiBufferSize = sizeof (SI_SETUP);
//[-end-190702-16990088-add]//
    if (BootMode != BOOT_IN_RECOVERY_MODE) {
      Status = SaveSetupConfig (
                 SI_SETUP_VARIABLE_NAME,
                 &gSiSetupVariableGuid,
                 EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                 SiBufferSize,
                 (VOID *) SiSetupNvData
                 );
    }
    RcSetupData->SiSetupNvData = (UINT8 *)SiSetupNvData;

  //   //==================================================================================//
  //   // Saving / Restore SGX setup variable                                              //
  //   //==================================================================================//

  //   SgxSetupDataSize = sizeof (SGX_SETUP_DATA);
  //   SgxSetupData = AllocateZeroPool (SgxSetupDataSize);
  //   if (SgxSetupData == NULL) {
  //     Status = EFI_OUT_OF_RESOURCES;
  //     ASSERT_EFI_ERROR (Status);
  //     return Status;
  //   }
  //   Status = gRT->GetVariable (
  //                  SGX_SETUP_VARIABLE_NAME,
  //                  &gSgxSetupVariableGuid,
  //                  NULL,
  //                  &SgxSetupDataSize,
  //                  (VOID *)SgxSetupData
  //                  );

  //   if (EFI_ERROR(Status)) {
  //     //
  //     // Save SGX variable
  //     //
  //     SgxSetupData->EnableSgx = CpuSetupNvData->EnableSgx;
  //     SgxSetupData->EnableC6Dram = CpuSetupNvData->EnableC6Dram;
  //     SgxSetupData->EpochUpdate = CpuSetupNvData->EpochUpdate;
  //     SgxSetupData->ShowEpoch = CpuSetupNvData->ShowEpoch;
  //     SgxSetupData->MaxPrmrrSize = CpuSetupNvData->MaxPrmrrSize;
  //     SgxSetupData->PrmrrSize = CpuSetupNvData->PrmrrSize;
  //     SgxSetupData->SgxEpoch0 = CpuSetupNvData->SgxEpoch0;
  //     SgxSetupData->SgxEpoch1 = CpuSetupNvData->SgxEpoch1;

  //     Status = SaveSetupConfig (
  //                SGX_SETUP_VARIABLE_NAME,
  //                &gSgxSetupVariableGuid,
  //                EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
  //                SgxSetupDataSize,
  //                (VOID *)SgxSetupData
  //                );
  //   } else {
  //     //
  //     // Restore SGX variable
  //     //
  //     CpuSetupNvData->EnableSgx = SgxSetupData->EnableSgx;
  //     CpuSetupNvData->EnableC6Dram = SgxSetupData->EnableC6Dram;
  //     CpuSetupNvData->EpochUpdate = SgxSetupData->EpochUpdate;
  //     CpuSetupNvData->ShowEpoch = SgxSetupData->ShowEpoch;
  //     CpuSetupNvData->MaxPrmrrSize = SgxSetupData->MaxPrmrrSize;
  //     CpuSetupNvData->PrmrrSize = SgxSetupData->PrmrrSize;
  //     CpuSetupNvData->SgxEpoch0 = SgxSetupData->SgxEpoch0;
  //     CpuSetupNvData->SgxEpoch1 = SgxSetupData->SgxEpoch1;

  //     Status = SaveSetupConfig (
  //                CPU_SETUP_VARIABLE_NAME,
  //                &gCpuSetupVariableGuid,
  //                EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
  //                CpuBufferSize,
  //                (VOID *) CpuSetupNvData
  //                );
  //     RcSetupData->CpuSetupNvData = (UINT8 *)CpuSetupNvData;
  //   }

   }

  //
  // Set Quick boot setting to variable, common code can use this setting to
  // do quick boot or not.
  //
  gRT->SetVariable (
         L"QuickBoot",
         &gEfiGenericVariableGuid,
         EFI_VARIABLE_BOOTSERVICE_ACCESS,
         sizeof (SetupNvData->QuickBoot),
         (VOID *) &SetupNvData->QuickBoot
         );

  gBS->FreePool (SetupNvData);
  //
  // Check "Custom" variable is exist or not...
  //
  SetupNvData = AllocateZeroPool (BufferSize);
  ASSERT (SetupNvData != NULL);
  if (SetupNvData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  GetSetupStatus = gRT->GetVariable (
                          L"Custom",
                           &mFormSetGuid,
                          NULL,
                          &BufferSize,
                          (VOID *)SetupNvData
                          );

  {
    //=====================================================//
    // Allocate a memory to put custom SETUP DATA variable //
    //=====================================================//
    SetupDataNvData = AllocateZeroPool (SetupDataBufferSize);
    ASSERT (SetupDataNvData != NULL);
    if (SetupDataNvData == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    GetSetupDataStatus = gRT->GetVariable (
                                L"Custom",
                                &gSetupVariableGuid,
                                NULL,
                                &SetupDataBufferSize,
                                (VOID *)SetupDataNvData
                                );
    //===================================================//
    // Allocate a memory to put custom SA setup variable //
    //===================================================//
    SaSetupNvData = AllocateZeroPool (SaBufferSize);
    ASSERT (SaSetupNvData != NULL);
    if (SaSetupNvData == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    GetSaSetupStatus = gRT->GetVariable (
                              L"Custom",
                              &gSaSetupVariableGuid,
                              NULL,
                              &SaBufferSize,
                              (VOID *)SaSetupNvData
                              );
    //===================================================//
    // Allocate a memory to put custom ME setup variable //
    //===================================================//
    MeSetupNvData = AllocateZeroPool (MeBufferSize);
    ASSERT (MeSetupNvData != NULL);
    if (MeSetupNvData == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    GetMeSetupStatus = gRT->GetVariable (
                              L"Custom",
                              &gMeSetupVariableGuid,
                              NULL,
                              &MeBufferSize,
                              (VOID *)MeSetupNvData
                              );
    //====================================================//
    // Allocate a memory to put custom CPU setup variable //
    //====================================================//
    CpuSetupNvData = AllocateZeroPool (CpuBufferSize);
    ASSERT (CpuSetupNvData != NULL);
    if (CpuSetupNvData == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    GetCpuSetupStatus = gRT->GetVariable (
                               L"Custom",
                                &gCpuSetupVariableGuid,
                               NULL,
                               &CpuBufferSize,
                               (VOID *)CpuSetupNvData
                               );
    //====================================================//
    // Allocate a memory to put custom PCH setup variable //
    //====================================================//
    PchSetupNvData = AllocateZeroPool (PchBufferSize);
    ASSERT (PchSetupNvData != NULL);
    if (PchSetupNvData == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    GetPchSetupStatus = gRT->GetVariable (
                               L"Custom",
                                &gPchSetupVariableGuid,
                               NULL,
                               &PchBufferSize,
                               (VOID *)PchSetupNvData
                               );
    //====================================================//
    // Allocate a memory to put custom SI setup variable //
    //====================================================//
    SiSetupNvData = AllocateZeroPool (SiBufferSize);
    ASSERT (SiSetupNvData != NULL);
    if (SiSetupNvData == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    GetSiSetupStatus = gRT->GetVariable (
                               L"Custom",
                                &gSiSetupVariableGuid,
                               NULL,
                               &SiBufferSize,
                               (VOID *)SiSetupNvData
                               );
    //===========================================================//
    // Allocate a memory to put custom ME setup storage variable //
    //===========================================================//
    MeSetupStorageNvData = AllocateZeroPool (MeStorageBufferSize);
    ASSERT (MeSetupStorageNvData != NULL);
    if (MeSetupStorageNvData == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    GetMeSetupStorageStatus = gRT->GetVariable (
                                     L"MeSetupStorageCustom",
                                     &gMeSetupVariableGuid,
                                     NULL,
                                     &MeStorageBufferSize,
                                     (VOID *)MeSetupStorageNvData
                                     );
  }

  if (EFI_ERROR (GetSetupStatus) || EFI_ERROR (GetSaSetupStatus) || EFI_ERROR (GetMeSetupStatus) ||
      EFI_ERROR (GetCpuSetupStatus) || EFI_ERROR (GetPchSetupStatus) || EFI_ERROR (GetMeSetupStorageStatus) ||
      EFI_ERROR(GetSetupDataStatus) || EFI_ERROR (GetSiSetupStatus)) {
    Status = EFI_ABORTED;
  }

  if (EFI_ERROR (Status)) {
    //
    // "Custom" Variable doesn't exist,so get a buffer with default variable
    //
    if (EFI_ERROR(GetSetupStatus)) {
      DefaultSetup (SetupNvData);
    }
    if (EFI_ERROR(GetSaSetupStatus)) {
      DefaultSaSetup (&SaSetupNvData, EfiBootServicesData);
    }
    if (EFI_ERROR(GetMeSetupStatus)) {
      DefaultMeSetup (&MeSetupNvData, EfiBootServicesData);
    }
    if (EFI_ERROR(GetCpuSetupStatus)) {
      DefaultCpuSetup (&CpuSetupNvData, EfiBootServicesData);
    }
    if (EFI_ERROR(GetPchSetupStatus)) {
      DefaultPchSetup (&PchSetupNvData, EfiBootServicesData);
    }
    if (EFI_ERROR(GetSetupDataStatus)) {
      DefaultSetupData (&SetupDataNvData, EfiBootServicesData);
    }
    if (EFI_ERROR(GetSiSetupStatus)) {
      DefaultSiSetup (&SiSetupNvData, EfiBootServicesData);
    }
//[-start-201127-IB17510127-add]//
    if (EFI_ERROR(GetMeSetupStorageStatus)) {
      DefaultMeSetupStorage (&MeSetupStorageNvData, EfiBootServicesData);
    }
//[-end-201127-IB17510127-add]//

    gRT->SetVariable (
           L"CustomPlatformLang",
           &gEfiGenericVariableGuid,
           EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
           AsciiStrSize ((CHAR8 *) PcdGetPtr (PcdUefiVariableDefaultPlatformLang)),
           (VOID *) PcdGetPtr (PcdUefiVariableDefaultPlatformLang)
           );
    //
    // Load default required
    //
    SetupRuntimeDetermination (
      SetupNvData, GetSetupStatus,
      SaSetupNvData, GetSaSetupStatus,
      MeSetupNvData, GetMeSetupStatus,
      CpuSetupNvData, GetCpuSetupStatus,
      PchSetupNvData, GetPchSetupStatus,
      MeSetupStorageNvData, GetMeSetupStorageStatus,
      SetupDataNvData, GetSetupDataStatus,
      SiSetupNvData, GetSiSetupStatus
      );
  }

  //
  // Update variable in every boot (e.g. MemorySize change and modify SCU settings)
  //
  SetupRuntimeUpdateEveryBoot (SetupNvData, SetupDataNvData, SaSetupNvData, MeSetupNvData, CpuSetupNvData, PchSetupNvData, SiSetupNvData, MeSetupStorageNvData);

  //
  // Save Custom variable.
  //
  Status = gRT->SetVariable (
                  L"Custom",
                  &mFormSetGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  BufferSize,
                  (VOID *)SetupNvData
                  );
  Status = gRT->SetVariable (
                  L"Custom",
                  &gSaSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  SaBufferSize,
                  (VOID *)SaSetupNvData
                  );
  Status = gRT->SetVariable (
                  L"Custom",
                  &gMeSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  MeBufferSize,
                  (VOID *)MeSetupNvData
                  );
  Status = gRT->SetVariable (
                  L"Custom",
                  &gCpuSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  CpuBufferSize,
                  (VOID *)CpuSetupNvData
                  );
  Status = gRT->SetVariable (
                  L"Custom",
                  &gPchSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  PchBufferSize,
                  (VOID *)PchSetupNvData
                  );
  Status = gRT->SetVariable (
                  L"MeSetupStorageCustom",
                  &gMeSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  MeStorageBufferSize,
                  (VOID *)MeSetupStorageNvData
                  );
  Status = gRT->SetVariable (
                  L"Custom",
                  &gSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  SetupDataBufferSize,
                  (VOID *)SetupDataNvData
                  );
  Status = gRT->SetVariable (
                  L"Custom",
                  &gSiSetupVariableGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                  SiBufferSize,
                  (VOID *)SiSetupNvData
                  );

  Status = gRT->SetVariable (
                  L"MeSetupStorageInitFlag",
                  &gMeSetupVariableGuid,
                  0,
                  0,
                  NULL
                  );

  Status = gRT->SetVariable (
                  L"MeSetupInitFlag",
                  &gMeSetupVariableGuid,
                  0,
                  0,
                  NULL
                  );

  Status = gRT->SetVariable (
                  L"SetupInitFlag",
                  &gSetupVariableGuid,
                  0,
                  0,
                  NULL
                  );

  gBS->FreePool (SetupNvData);
  gBS->FreePool (SaSetupNvData);
  gBS->FreePool (MeSetupNvData);
  gBS->FreePool (CpuSetupNvData);
  gBS->FreePool (PchSetupNvData);
  gBS->FreePool (MeSetupStorageNvData);
  gBS->FreePool (SetupDataNvData);
  gBS->FreePool (SiSetupNvData);
  // gBS->FreePool (SgxSetupData);
  return EFI_SUCCESS;
}

/**
 Installs a string and ifr pack set

 @param None

 @retval VOID

**/
EFI_STATUS
InstallHiiData (
  VOID
  )
{
  EFI_HII_HANDLE                 HiiHandle;

  EFI_HANDLE                     DriverHandle;
  UINTN                          Index;
  EFI_STATUS                     Status;
  UINTN                          HandleCnt;
  NEW_PACKAGE_INFO               NewPackageInfo [] =
                                 {{InstallExitCallbackRoutine,     ExitVfrBin,     SetupUtilityLibStrings, SetupUtilityLibImages},
                                  {InstallBootCallbackRoutine,     BootVfrBin,     SetupUtilityLibStrings, SetupUtilityLibImages},
                                  {InstallPowerCallbackRoutine,    PowerVfrBin,    SetupUtilityStrings,    SetupUtilityImages},
                                  {InstallSecurityCallbackRoutine, SecurityVfrBin, SetupUtilityLibStrings, SetupUtilityLibImages},
                                  {InstallAdvanceCallbackRoutine,  AdvancedBin,  SetupUtilityStrings,    SetupUtilityImages},
                                  {InstallMainCallbackRoutine,     MainVfrBin,     SetupUtilityLibStrings, SetupUtilityLibImages}
                                 };

  HandleCnt = sizeof (NewPackageInfo) / sizeof (NEW_PACKAGE_INFO);
  if (HandleCnt > MAX_HII_HANDLES) {
    return EFI_OUT_OF_RESOURCES;
  }

  Status = EFI_SUCCESS;
  for (Index = 0; Index < HandleCnt; Index++) {
    Status = CreateHiiDriverHandle (&DriverHandle);
    if (EFI_ERROR (Status)) {
      break;
    }

    HiiHandle = HiiAddPackages (
                  &mFormSetGuid,
                  DriverHandle,
                  NewPackageInfo[Index].IfrPack,
                  NewPackageInfo[Index].StringPack,
                  NewPackageInfo[Index].ImagePack,
                  NULL
                  );
    ASSERT(HiiHandle != NULL);

    gSUBrowser->SUCInfo->MapTable[Index].HiiHandle = HiiHandle;
    gSUBrowser->SUCInfo->MapTable[Index].DriverHandle = DriverHandle;
    NewPackageInfo[Index].CallbackRoutine (DriverHandle, HiiHandle);

  }

  mDriverHiiHandle = gSUBrowser->SUCInfo->MapTable[AdvanceHiiHandle].HiiHandle;

  UpdateMemoryInfo (gSUBrowser);
  DisplayPlatformInfo (gSUBrowser);

  Status = SyncRcBrowserData ();
  return Status;

}


EFI_STATUS
InitSetupUtilityBrowser(
  IN  EFI_SETUP_UTILITY_PROTOCOL            *This
)
{
  EFI_STATUS                                Status;
  UINT8                                     *Setup = NULL;

  gSUBrowser = AllocateZeroPool (sizeof(SETUP_UTILITY_BROWSER_DATA));
  if (gSUBrowser == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  gSUBrowser->SUCInfo = AllocateZeroPool (sizeof(SETUP_UTILITY_CONFIGURATION));
  if (gSUBrowser->SUCInfo == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Setup = CommonGetVariableData (SETUP_VARIABLE_NAME, &gSystemConfigurationGuid);
  if (Setup == NULL) {
    return EFI_NOT_FOUND;
  }
  gSUBrowser->SCBuffer          = Setup;
  gSUBrowser->SUCInfo->SCBuffer = gSUBrowser->SCBuffer;
  gSUBrowser->Signature         = EFI_SETUP_UTILITY_BROWSER_SIGNATURE;
  gSUBrowser->ExtractConfig     = GenericExtractConfigHook;
  gSUBrowser->RouteConfig       = GenericRouteConfig;
  gSUBrowser->HotKeyCallback    = HotKeyCallBack;
  //
  // There should only be one HII protocol
  //
  Status = gBS->LocateProtocol (
                  &gEfiHiiDatabaseProtocolGuid,
                  NULL,
                  (VOID **)&gSUBrowser->HiiDatabase
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //
  // There should only be one HII protocol
  //
  Status = gBS->LocateProtocol (
                  &gEfiHiiStringProtocolGuid,
                  NULL,
                  (VOID **)&gSUBrowser->HiiString
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //
  // There should only be one HII protocol
  //
  Status = gBS->LocateProtocol (
                  &gEfiHiiConfigRoutingProtocolGuid,
                  NULL,
                  (VOID **)&gSUBrowser->HiiConfigRouting
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }


  //
  // There will be only one FormConfig in the system
  // If there is another out there, someone is trying to install us
  // again.  Fail that scenario.
  //
  Status = gBS->LocateProtocol (
                  &gEfiFormBrowser2ProtocolGuid,
                  NULL,
                  (VOID **)&gSUBrowser->Browser2
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }
  Status = gBS->LocateProtocol (
                 &gH2ODialogProtocolGuid,
                 NULL,
                 (VOID **) &gSUBrowser->H2ODialog
                 );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  return EFI_SUCCESS;
};


EFI_STATUS
InitRcSetupUtilityBrowser (
  VOID
  )
{
  EFI_STATUS                                Status;
  UINTN                                     BufferSize;
  RC_SETUP_UTILITY_DATA                     *RcSetupData = NULL;
  UINT8                                     *SaSetup = NULL;
  UINT8                                     *MeSetup = NULL;
  UINT8                                     *CpuSetup = NULL;
  UINT8                                     *PchSetup = NULL;
  UINT8                                     *MeSetupStorage = NULL;
  UINT8                                     *SetupData = NULL;
  UINT8                                     *SiSetup = NULL;

  Status = EFI_SUCCESS;

  RcSetupData = GetVariableAndSize (
                  RC_SETUP_UTILITY_VARIABLE_NAME,
                  &gRcSetupUtilityVariableGuid,
                  &BufferSize
                  );
  if (RcSetupData == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    goto ErrorHandle;
  }

  //========================================================================//
  // Get SA setup NV data from ROM and copy to RC setup utility data region //
  //========================================================================//
  SaSetup = GetVariableAndSize (
              SA_SETUP_VARIABLE_NAME,
              &gSaSetupVariableGuid,
              &BufferSize
              );
  if (SaSetup == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    goto ErrorHandle;
  }
  CopyMem (RcSetupData->SaSetupNvData, SaSetup, BufferSize);
  //========================================================================//
  // Get ME setup NV data from ROM and copy to RC setup utility data region //
  //========================================================================//
  MeSetup = GetVariableAndSize (
              ME_SETUP_VARIABLE_NAME,
              &gMeSetupVariableGuid,
              &BufferSize
              );
  if (MeSetup == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    goto ErrorHandle;
  }
  CopyMem (RcSetupData->MeSetupNvData, MeSetup, BufferSize);
  //=========================================================================//
  // Get CPU setup NV data from ROM and copy to RC setup utility data region //
  //=========================================================================//
  CpuSetup = GetVariableAndSize (
               CPU_SETUP_VARIABLE_NAME,
               &gCpuSetupVariableGuid,
               &BufferSize
               );
  if (CpuSetup == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    goto ErrorHandle;
  }
  CopyMem (RcSetupData->CpuSetupNvData, CpuSetup, BufferSize);
  //=========================================================================//
  // Get PCH setup NV data from ROM and copy to RC setup utility data region //
  //=========================================================================//
  PchSetup = GetVariableAndSize (
               PCH_SETUP_VARIABLE_NAME,
               &gPchSetupVariableGuid,
               &BufferSize
               );
  if (PchSetup == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    goto ErrorHandle;
  }
  CopyMem (RcSetupData->PchSetupNvData, PchSetup, BufferSize);
  //================================================================================//
  // Get ME setup storage NV data from ROM and copy to RC setup utility data region //
  //================================================================================//
  MeSetupStorage = GetVariableAndSize (
                     ME_SETUP_STORAGE_VARIABLE_NAME,
                     &gMeSetupVariableGuid,
                     &BufferSize
                     );
  if (MeSetupStorage == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    goto ErrorHandle;
  }
  CopyMem (RcSetupData->MeSetupStorageNvData, MeSetupStorage, BufferSize);
  //==========================================================================//
  // Get SETUP DATA NV data from ROM and copy to RC setup utility data region //
  //==========================================================================//
  SetupData = GetVariableAndSize (
                PLATFORM_SETUP_VARIABLE_NAME,
                &gSetupVariableGuid,
                &BufferSize
                );
  if (SetupData == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    goto ErrorHandle;
  }
  CopyMem (RcSetupData->SetupDataNvData, SetupData, BufferSize);
  //=========================================================================//
  // Get SI setup NV data from ROM and copy to RC setup utility data region //
  //=========================================================================//
  SiSetup = GetVariableAndSize (
               SI_SETUP_VARIABLE_NAME,
               &gSiSetupVariableGuid,
               &BufferSize
               );
  if (SiSetup == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    goto ErrorHandle;
  }
  CopyMem (RcSetupData->SiSetupNvData, SiSetup, BufferSize);

  gRcSUBrowser = AllocateZeroPool (sizeof(RC_SETUP_UTILITY_BROWSER_DATA));
  if (gRcSUBrowser == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    goto ErrorHandle;
  }

  gRcSUBrowser->SaSUBrowserData        = RcSetupData->SaSetupNvData;
  gRcSUBrowser->MeSUBrowserData        = RcSetupData->MeSetupNvData;
  gRcSUBrowser->CpuSUBrowserData       = RcSetupData->CpuSetupNvData;
  gRcSUBrowser->PchSUBrowserData       = RcSetupData->PchSetupNvData;
  gRcSUBrowser->MeStorageSUBrowserData = RcSetupData->MeSetupStorageNvData;
  gRcSUBrowser->SetupDataSUBrowserData = RcSetupData->SetupDataNvData;
  gRcSUBrowser->SiSUBrowserData        = RcSetupData->SiSetupNvData;

  gRcSUBrowser->SaSUBDataSize        = sizeof (SA_SETUP);
  gRcSUBrowser->MeSUBDataSize        = sizeof (ME_SETUP);
  gRcSUBrowser->CpuSUBDataSize       = sizeof (CPU_SETUP);
  gRcSUBrowser->PchSUBDataSize       = sizeof (PCH_SETUP);
  gRcSUBrowser->MeStorageSUBDataSize = sizeof (ME_SETUP_STORAGE);
  gRcSUBrowser->SetupDataSUBDataSize = sizeof (SETUP_DATA);
  gRcSUBrowser->SiSUBDataSize        = sizeof (SI_SETUP);

  BufferSize = sizeof (RC_SETUP_UTILITY_BROWSER_DATA);
  Status = gRT->SetVariable (
                  RC_SETUP_UTILITY_BROWSER_VARIABLE_NAME,
                  &gRcSetupUtilityBrowserVariable,
                  EFI_VARIABLE_BOOTSERVICE_ACCESS,
                  BufferSize,
                  (VOID *)gRcSUBrowser
                  );

ErrorHandle:
  if (RcSetupData != NULL) {
    gBS->FreePool (RcSetupData);
  }
  if (SaSetup != NULL) {
    gBS->FreePool (SaSetup);
  }
  if (MeSetup != NULL) {
    gBS->FreePool (MeSetup);
  }
  if (CpuSetup != NULL) {
    gBS->FreePool (CpuSetup);
  }
  if (PchSetup != NULL) {
    gBS->FreePool (PchSetup);
  }
  if (MeSetupStorage != NULL) {
    gBS->FreePool (MeSetupStorage);
  }
  if (SetupData != NULL) {
    gBS->FreePool (SetupData);
  }
  if (SiSetup != NULL) {
    gBS->FreePool (SiSetup);
  }
  return Status;
};


/**
 Installs a string and ifr pack set

 @param [in]   StringPack       string pack to store in database and associate with IfrPack
                                IrfPack - ifr pack to store in database (it will use StringPack data)
 @param        ...

 @retval VOID

**/
EFI_STATUS
RemoveHiiData (
  IN VOID     *StringPack,
  ...
)
{
  VA_LIST                                   args;
  HII_HANDLE_VARIABLE_MAP_TABLE             *MapTable;
  EFI_HII_DATABASE_PROTOCOL                 *HiiDatabase;
  UINTN                                     Index;

  HiiDatabase = gSUBrowser->HiiDatabase;
  VA_START (args, StringPack );

  Index = 0;
  while (TRUE) {
    MapTable = VA_ARG( args, HII_HANDLE_VARIABLE_MAP_TABLE *);
    if (MapTable == NULL) {
      break;
    }
    HiiDatabase->RemovePackageList (HiiDatabase, MapTable->HiiHandle);
    mUninstallCallbackRoutine[Index++] (MapTable->DriverHandle);
    DestroyHiiDriverHandle (MapTable->DriverHandle);
  }

  if (gSUBrowser->IdeConfig != NULL) {
    gBS->FreePool (gSUBrowser->IdeConfig);
    gSUBrowser->IdeConfig = NULL;
  }

  return EFI_SUCCESS;

}

VOID
EFIAPI
SetupUtilityNotifyFn (
  IN EFI_EVENT                             Event,
  IN VOID                                  *Context
  )
{
  EFI_STATUS                               Status;
  EFI_SETUP_UTILITY_APPLICATION_PROTOCOL   *SetupUtilityApp;
  BOOLEAN                                  DoClearScreen;

  Status = gBS->LocateProtocol (
                  &gEfiSetupUtilityApplicationProtocolGuid,
                  NULL,
                  (VOID **) &SetupUtilityApp
                  );
  if (EFI_ERROR(Status)) {
    return;
  }

  if (SetupUtilityApp->VfrDriverState == InitializeSetupUtility) {
    POST_CODE (BDS_ENTER_SETUP); //PostCode = 0x29, Enter Setup Menu
  }

  mScuRecord = 0;

  DoClearScreen = TRUE;

  switch (SetupUtilityApp->VfrDriverState) {

  case InstallSetupUtilityHiiData:
    DoClearScreen = FALSE;
    CreateScuData (DoClearScreen);
    break;

  case UnInstallSetupUtilityHiiData:
    DoClearScreen = FALSE;
    DestroyScuData (DoClearScreen);
    break;

  case InitializeSetupUtility:
    CreateScuData (DoClearScreen);
    break;

  case ShutdownSetupUtility:
    DestroyScuData (DoClearScreen);
    break;

  default:
    break;
  }
}


/**
 This function use to verify Setup Configuration structure size as a build-time.
 It is not be call in BIOS during POST.

**/
VOID
SetupConfigurationSizeVerify (
  VOID
  )
{
  {C_ASSERT((sizeof(SYSTEM_CONFIGURATION)) == FixedPcdGet32 (PcdSetupConfigSize));}
}


VOID InitString(EFI_HII_HANDLE HiiHandle, EFI_STRING_ID StrRef, CHAR16 *sFormat, ...)
{
  CHAR16      StringBuffer[1024];
  VA_LIST     ArgList;
  CHAR8       PlatformLanguage[RFC_3066_ENTRY_SIZE];
  CHAR8       *SupportedLanguages;
  CHAR8       *BestLanguage;
  UINTN       Size;
  EFI_STATUS  Status;

  //
  // Get current language setting
  //
  Size = sizeof (PlatformLanguage);
  Status = gRT->GetVariable (
                  L"PlatformLang",
                  &gEfiGlobalVariableGuid,
                  NULL,
                  &Size,
                  PlatformLanguage
                  );
  if (EFI_ERROR (Status)) {
	  AsciiStrCpyS (PlatformLanguage, Size / sizeof(CHAR8), (CHAR8 *) "en-US");
  }

  SupportedLanguages = HiiGetSupportedLanguages (HiiHandle);
  if (SupportedLanguages == NULL) {
    //
    // No supported language.
    //
    return;
  }
  //
  // Get the best matching language from SupportedLanguages
  //
  BestLanguage = GetBestLanguage (
                   SupportedLanguages,
                   FALSE,                                             // RFC 4646 mode
                   PlatformLanguage,                                  // Highest priority
                   NULL
                   );

  //
  // Construct string value.
  //
  VA_START(ArgList,sFormat);
  UnicodeVSPrint (StringBuffer,sizeof(StringBuffer),sFormat,ArgList);
  VA_END(ArgList);

  //
  // Set string
  //
  if (NULL == gIfrLibHiiString) {
    Status = gBS->LocateProtocol (
              &gEfiHiiStringProtocolGuid,
              NULL,
              &gIfrLibHiiString
              );
    if (EFI_ERROR (Status)) {
      return;
    }
  }

  gIfrLibHiiString->SetString (
                     gIfrLibHiiString,
                     HiiHandle,
                     StrRef,
                     BestLanguage,
                     StringBuffer,
                     NULL
                     );

  if (SupportedLanguages != NULL) {
    FreePool (SupportedLanguages);
  }

  if (BestLanguage != NULL) {
    FreePool (BestLanguage);
  }
}

EFI_STATUS
HiiLibGetString (
  IN  EFI_HII_HANDLE                  PackageList,
  IN  EFI_STRING_ID                   StringId,
  OUT EFI_STRING                      String,
  IN  OUT UINTN                       *StringSize
  )
{
  CHAR8       PlatformLanguage[RFC_3066_ENTRY_SIZE];
  CHAR8       *SupportedLanguages;
  CHAR8       *BestLanguage;
  UINTN       Size;
  EFI_STATUS  Status;

  //
  // Get current language setting
  //
  Size = sizeof (PlatformLanguage);
  Status = gRT->GetVariable (
                  L"PlatformLang",
                  &gEfiGlobalVariableGuid,
                  NULL,
                  &Size,
                  PlatformLanguage
                  );
  if (EFI_ERROR (Status)) {
    AsciiStrCpyS (PlatformLanguage, Size / sizeof (CHAR8),(CHAR8 *) "en-US");
  }

  SupportedLanguages = HiiGetSupportedLanguages (PackageList);
  if (SupportedLanguages == NULL) {
    //
    // No supported language.
    //
    return EFI_NOT_FOUND;
  }

  //
  // Get the best matching language from SupportedLanguages
  //
  BestLanguage = GetBestLanguage (
                   SupportedLanguages,
                   FALSE,                                             // RFC 4646 mode
                   PlatformLanguage,                                  // Highest priority
                   NULL
                   );

  if (NULL == gIfrLibHiiString) {
    Status = gBS->LocateProtocol (
              &gEfiHiiStringProtocolGuid,
              NULL,
              &gIfrLibHiiString
              );
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  Status = gIfrLibHiiString->GetString (
                               gIfrLibHiiString,
                               BestLanguage,
                               PackageList,
                               StringId,
                               String,
                               StringSize,
                               NULL
                               );

  FreePool (SupportedLanguages);
  FreePool (BestLanguage);

  return Status;
}

/**
  Construct Request String (L"&OFFSET=%x&WIDTH=%x") base on the input Offset and Width.
  If the input RequestString is not NULL, new request will be cat at the end of it. The full
  request string will be constructed and return. Caller is responsible to free it.

  @param RequestString   Current request string.
  @param Offset          Offset of data in Storage.
  @param Width           Width of data.

  @return String         Request string with input Offset and Width.
**/
EFI_STRING
EFIAPI
HiiConstructRequestString (
  IN EFI_STRING      RequestString, OPTIONAL
  IN UINTN           Offset,
  IN UINTN           Width
  )
{
  CHAR16             RequestElement[30];
  UINTN              StringLength;
  EFI_STRING         NewString;
  UINTN              NewStringBufferSize;
  StringLength = UnicodeSPrint (
                   RequestElement,
                   sizeof (RequestElement),
                   L"&OFFSET=%x&WIDTH=%x",
                   Offset,
                   Width
                   );
  if (RequestString != NULL) {
    StringLength = StringLength + StrLen (RequestString);
  }
  NewStringBufferSize = (StringLength + 1) * sizeof (CHAR16);
  NewString = AllocateZeroPool (NewStringBufferSize);
  if (NewString == NULL){
    ASSERT (NewString != NULL);
    return NULL;
  }

  if (RequestString != NULL) {
    StrCatS (NewString, NewStringBufferSize, RequestString);
    FreePool (RequestString);
  }
  StrCatS (NewString, NewStringBufferSize, RequestElement);
  return NewString;
}


/**
  Init CPU setup volatile data and set a volatile variable named as L"CpuSetupVolatileData"

  @param  None

  @return EFI_SUCCESS             Init CPU setup volatile data is successful.
  @return Others                  Init CPU setup volatile data is failure.
**/
EFI_STATUS
EFIAPI
InitCpuSetupVolatileData (
  VOID
  )
{
  //=====================================================//
  // Reference to RC platform code in setup driver entry //
  //=====================================================//
  EFI_STATUS                           Status;
  UINTN                                Size;
  UINT32                               CpuSetupVolVarAttr;
  CPU_SETUP_VOLATILE_DATA              CpuSetupVolData = {0};
  CPU_SETUP_VOLATILE_DATA              *pCpuSetupVolData = NULL;
  UINT32                               RegEAX;
  UINT32                               RegEBX;
  UINT32                               RegECX;
  UINT32                               RegEDX;

  CpuSetupVolVarAttr = EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS;
  Size = sizeof (CPU_SETUP_VOLATILE_DATA);
  Status = gRT->GetVariable (
                  CPU_SETUP_VOLATILE_DATA_VARIABLE_NAME,
                  &gCpuSetupVariableGuid,
                  &CpuSetupVolVarAttr,
                  &Size,
                  &CpuSetupVolData
                  );
  if (Status == EFI_NOT_FOUND) {
    Status = gRT->SetVariable (
                    CPU_SETUP_VOLATILE_DATA_VARIABLE_NAME,
                    &gCpuSetupVariableGuid,
                    CpuSetupVolVarAttr,
                    sizeof (CpuSetupVolData),
                    &CpuSetupVolData
                    );
    ASSERT_EFI_ERROR (Status);
  }

  AsmCpuid (1, &RegEAX, &RegEBX, &RegECX, &RegEDX);
  pCpuSetupVolData = (CPU_SETUP_VOLATILE_DATA *) &CpuSetupVolData;

  pCpuSetupVolData->CpuFamilyModel = (UINT16) RegEAX & 0x3FF0;
  pCpuSetupVolData->CpuExtendedFamilyModel = (UINT16) ((RegEAX >> 16) & 0x0FFF);

    pCpuSetupVolData->EdramSupported = IsEdramEnable();

  Status = gRT->SetVariable (
                  CPU_SETUP_VOLATILE_DATA_VARIABLE_NAME,
                  &gCpuSetupVariableGuid,
                  CpuSetupVolVarAttr,
                  sizeof (CpuSetupVolData),
                  &CpuSetupVolData
                  );
  ASSERT_EFI_ERROR (Status);
  return Status;
}


/**
  Sync all RC relative browser data from the newest RC setup variable settings
  before enter SCU

  @param  None

  @return EFI_SUCCESS             Update Rc relative variables to RC Browser data
  @return Others                  Get someone RC relative variable process is failed
**/
EFI_STATUS
SyncRcBrowserData (
  VOID
  )
{
  EFI_STATUS                                Status;
  UINT8                                     *RcSetupNvData;
  UINTN                                     BufferSize;

  RcSetupNvData = NULL;

  //==========================//
  // Sync SETUP DATA variable //
  //==========================//
  BufferSize = sizeof (SETUP_DATA);
  RcSetupNvData = AllocateZeroPool (BufferSize);
  if (RcSetupNvData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  PLATFORM_SETUP_VARIABLE_NAME,
                  &gSetupVariableGuid,
                  NULL,
                  &BufferSize,
                  (VOID *)RcSetupNvData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupNvData);
    return Status;
  }
  CopyMem (gRcSUBrowser->SetupDataSUBrowserData, RcSetupNvData, BufferSize);
  FreePool (RcSetupNvData);
  //========================//
  // Sync SA setup variable //
  //========================//
  BufferSize = sizeof (SA_SETUP);
  RcSetupNvData = AllocateZeroPool (BufferSize);
  if (RcSetupNvData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  SA_SETUP_VARIABLE_NAME,
                  &gSaSetupVariableGuid,
                  NULL,
                  &BufferSize,
                  (VOID *)RcSetupNvData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupNvData);
    return Status;
  }
  CopyMem (gRcSUBrowser->SaSUBrowserData, RcSetupNvData, BufferSize);
  FreePool (RcSetupNvData);
  //========================//
  // Sync ME setup variable //
  //========================//
  BufferSize = sizeof (ME_SETUP);
  RcSetupNvData = AllocateZeroPool (BufferSize);
  if (RcSetupNvData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  ME_SETUP_VARIABLE_NAME,
                  &gMeSetupVariableGuid,
                  NULL,
                  &BufferSize,
                  (VOID *)RcSetupNvData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupNvData);
    return Status;
  }
  CopyMem (gRcSUBrowser->MeSUBrowserData, RcSetupNvData, BufferSize);
  FreePool (RcSetupNvData);
  //=========================//
  // Sync CPU setup variable //
  //=========================//
  BufferSize = sizeof (CPU_SETUP);
  RcSetupNvData = AllocateZeroPool (BufferSize);
  if (RcSetupNvData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  CPU_SETUP_VARIABLE_NAME,
                  &gCpuSetupVariableGuid,
                  NULL,
                  &BufferSize,
                  (VOID *)RcSetupNvData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupNvData);
    return Status;
  }
  CopyMem (gRcSUBrowser->CpuSUBrowserData, RcSetupNvData, BufferSize);
  FreePool (RcSetupNvData);
  //=========================//
  // Sync PCH setup variable //
  //=========================//
  BufferSize = sizeof (PCH_SETUP);
  RcSetupNvData = AllocateZeroPool (BufferSize);
  if (RcSetupNvData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  PCH_SETUP_VARIABLE_NAME,
                  &gPchSetupVariableGuid,
                  NULL,
                  &BufferSize,
                  (VOID *)RcSetupNvData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupNvData);
    return Status;
  }
  CopyMem (gRcSUBrowser->PchSUBrowserData, RcSetupNvData, BufferSize);
  FreePool (RcSetupNvData);
  //=========================//
  // Sync SI setup variable //
  //=========================//
  BufferSize = sizeof (SI_SETUP);
  RcSetupNvData = AllocateZeroPool (BufferSize);
  if (RcSetupNvData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  SI_SETUP_VARIABLE_NAME,
                  &gSiSetupVariableGuid,
                  NULL,
                  &BufferSize,
                  (VOID *)RcSetupNvData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupNvData);
    return Status;
  }
  CopyMem (gRcSUBrowser->SiSUBrowserData, RcSetupNvData, BufferSize);
  FreePool (RcSetupNvData);
  //================================//
  // Sync ME setup storage variable //
  //================================//
  BufferSize = sizeof (ME_SETUP_STORAGE);
  RcSetupNvData = AllocateZeroPool (BufferSize);
  if (RcSetupNvData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                  ME_SETUP_STORAGE_VARIABLE_NAME,
                  &gMeSetupVariableGuid,
                  NULL,
                  &BufferSize,
                  (VOID *)RcSetupNvData
                  );
  if (EFI_ERROR (Status)) {
    FreePool (RcSetupNvData);
    return Status;
  }
  CopyMem (gRcSUBrowser->MeStorageSUBrowserData, RcSetupNvData, BufferSize);
  FreePool (RcSetupNvData);
  //=================================//
  // Sync SetupVolatileData variable //
  //=================================//
  BufferSize = sizeof (SETUP_VOLATILE_DATA);
  Status = gRT->GetVariable (
                  L"SetupVolatileData",
                  &gSetupVariableGuid,
                  NULL,
                  &BufferSize,
                  &mSetupVolatileData
                  );
  ASSERT_EFI_ERROR (Status);
  return Status;
}


/**
  Update Setup volatile data

  @param  None

  @return EFI_SUCCESS             Update setup volatile data is successful.
  @return Others                  Update setup volatile data is failed.
**/
EFI_STATUS
UpdateSetupVolatileData (
  VOID
  )
{
  EFI_STATUS                                Status;
  UINTN                                     VariableSize;
  UINT32                                    SetupVolAttributes;
  SETUP_VOLATILE_DATA                       SetupVolatileData;
  VOID                                      *Hob;
  PCIE_STORAGE_INFO_HOB                     *PcieStorageInfoHob;
//[-start-200826-IB16740113-remove]//
  //UINT32                                    Index;
//[-end-200826-IB16740113-remove]//
  Hob = NULL;
  PcieStorageInfoHob = NULL;
  Hob = GetFirstGuidHob (&gPchPcieStorageDetectHobGuid);
  if (Hob != NULL) {
    PcieStorageInfoHob = (PCIE_STORAGE_INFO_HOB *) GET_GUID_HOB_DATA (Hob);
  }

  //
  // Set SetupVolatileData for Rst remap menu
  //
//[-start-200826-IB16740113-remove]// PcieStorageMap...have removed in RC1341
//  for (Index = 0; Index < PCH_MAX_PCIE_ROOT_PORTS; Index++) {
//    if (PcieStorageInfoHob != NULL) {
//      mSetupVolatileData.PcieStorageMap[Index] = PcieStorageInfoHob->PcieStorageLinkWidth[Index];
//      mSetupVolatileData.PcieStorageProgrammingInterface[Index] = PcieStorageInfoHob->PcieStorageProgrammingInterface[Index];
//    } else {
//      mSetupVolatileData.PcieStorageMap[Index] = 0;
//      mSetupVolatileData.PcieStorageProgrammingInterface[Index] = 0;
//    }
//  }
//
//  for (Index = 0; Index < PCH_MAX_PCIE_CONTROLLERS; Index++) {
//    if (PcieStorageInfoHob != NULL) {
//      mSetupVolatileData.CycleRouterMap[Index] = PcieStorageInfoHob->RstCycleRouterMap[Index];
//      DEBUG ((DEBUG_INFO, "CycleRouterMap[%d] = %d\n", Index, mSetupVolatileData.CycleRouterMap[Index]));
//    } else {
//      mSetupVolatileData.CycleRouterMap[Index] = 0;
//    }
//  }
//[-end-200826-IB16740113-remove]//

  VariableSize = sizeof(SETUP_VOLATILE_DATA);
  Status = gRT->GetVariable (
                  L"SetupVolatileData",
                  &gSetupVariableGuid,
                  &SetupVolAttributes,
                  &VariableSize,
                  &SetupVolatileData
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  VariableSize = sizeof(SETUP_VOLATILE_DATA);
  Status = gRT->SetVariable (
                  L"SetupVolatileData",
                  &gSetupVariableGuid,
                  SetupVolAttributes,
                  VariableSize,
                  &mSetupVolatileData
                  );
  return Status;
}
