/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SETUP_FUNCS_H_
#define _SETUP_FUNCS_H_

#include <SetupUtility.h>
#include <Library/BaseLib.h>

#include <Protocol/MemInfo.h>
#include <Protocol/IdeControllerInit.h>
#include <PortNumberMap.h>
#include <CpuRegs.h>
#include <StdLib/StdCLibSupport.h>
#include <MeSetup.h>
//[-start-201127-IB17510127-add]//
#include <Library/BootGuardLibVer1.h>
#include <Protocol/AmtWrapperProtocol.h>

#define HeciPciSegmentRead32(Register) PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, Register))
//[-end-201127-IB17510127-add]//

///
/// This constant defines the maximum length of the CPU brand string. According to the
/// IA manual, the brand string is in EAX through EDX (thus 16 bytes) after executing
/// the CPUID instructions with EAX as 80000002, 80000003, 80000004.
///
#define MAXIMUM_CPU_BRAND_STRING_LENGTH 48

#define CONFIG_TDP_NUM_LEVELS_MASK        ( BIT34 | BIT33 )

EFI_STATUS
LoadCustomOption (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL        *This
  );

EFI_STATUS
SaveCustomOption (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL        *This
  );

EFI_STATUS
DiscardChange (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL        *This
  );


VOID *
GetVariableAndSize (
  IN  CHAR16              *Name,
  IN  EFI_GUID            *VendorGuid,
  OUT UINTN               *VariableSize
  );

EFI_STATUS
CheckLanguage (
  VOID
  );

EFI_STATUS
UpdateAtaString(
  IN      EFI_ATAPI_IDENTIFY_DATA     *IdentifyDriveInfo,
  IN OUT  CHAR16                      **NewString
  );

EFI_STATUS
AsciiToUnicode (
  IN    CHAR8     *AsciiString,
  IN    CHAR16    *UnicodeString
  );
EFI_STATUS
EventTimerControl (
  IN UINT64                     Timeout
  );

EFI_STATUS
SearchMatchedPortNum (
  IN     UINT32                              Bus,
  IN     UINT32                              Device,
  IN     UINT32                              Function,
  IN     UINT8                               PrimarySecondary,
  IN     UINT8                               SlaveMaster,
  IN OUT UINTN                               *PortNum
  );

EFI_STATUS
CheckSataPort (
  IN UINTN                                  PortNum
  );


CHAR16 *
GetTokenStringByLanguage (
  IN EFI_HII_HANDLE                             HiiHandle,
  IN EFI_STRING_ID                              Token,
  IN CHAR8                                      *LanguageString
  );
EFI_STATUS
SaveSetupConfig (
  IN     CHAR16             *VariableName,
  IN     EFI_GUID           *VendorGuid,
  IN     UINT32             Attributes,
  IN     UINTN              DataSize,
  IN     VOID               *Buffer
  );

EFI_STATUS
AddNewString (
  IN   EFI_HII_HANDLE           InputHiiHandle,
  IN   EFI_HII_HANDLE           OutputHiiHandle,
  IN   EFI_STRING_ID            InputToken,
  OUT  EFI_STRING_ID            *OutputToken
  );

EFI_STATUS
DefaultSetup (
  OUT CHIPSET_CONFIGURATION            *SetupNvData
  );

/**
 Build a default SETUP_DATA variable value an save to a buffer according to platform requirement

 @param[out]  SetupDataNvData          A pointer of a pointer to SETUP_DATA variable default value
                                       for DXE recovery between different size of SETUP_DATA.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultSetupData (
  OUT SETUP_DATA                         **SetupDataNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  );

/**
 Build a default SA variable value an save to a buffer according to platform requirement

 @param[out]  SaSetupNvData            A pointer of a pointer to SA_SETUP variable default value
                                       for DXE recovery between different size of SA_SETUP.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultSaSetup (
  OUT SA_SETUP                           **SaSetupNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  );

/**
 Build a default ME variable value an save to a buffer according to platform requirement

 @param[out]  MeSetupNvData            A pointer of a pointer to ME_SETUP variable default value
                                       for DXE recovery between different size of ME_SETUP.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultMeSetup (
  OUT ME_SETUP                           **MeSetupNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  );

/**
 Build a default CPU variable value an save to a buffer according to platform requirement

 @param[out]  CpuSetupNvData           A pointer of a pointer to CPU_SETUP variable default value
                                       for DXE recovery between different size of CPU_SETUP.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultCpuSetup (
  OUT CPU_SETUP                          **CpuSetupNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  );

/**
 Build a default PCH variable value an save to a buffer according to platform requirement

 @param[out]  PchSetupNvData           A pointer of a pointer to PCH_SETUP variable default value
                                       for DXE recovery between different size of PCH_SETUP.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultPchSetup (
  OUT PCH_SETUP                          **PchSetupNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  );

/**
 Build a default SI variable value an save to a buffer according to platform requirement

 @param[out]  SiSetupNvData            A pointer of a pointer to SI_SETUP variable default value
                                       for DXE recovery between different size of SI_SETUP.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultSiSetup (
  OUT SI_SETUP                           **SiSetupNvData,
  IN  EFI_MEMORY_TYPE                    PoolType
  );

//[-start-201127-IB17510127-add]//
/**
 Build a default ME_SETUP_STORAGE variable value an save to a buffer according to platform requirement

 @param[out]  MeSetupStorageNvData     A pointer of a pointer to ME_SETUP_STORAGE variable default value
                                       for DXE recovery between different size of ME_SETUP_STORAGE.
 @param[in]   PoolType                 The type of pool to allocate

 @retval      EFI_SUCCESS              Build default setup successful.
 @retval      EFI_INVALID_PARAMETER    Input value is invalid.

**/
EFI_STATUS
DefaultMeSetupStorage (
  OUT ME_SETUP_STORAGE                **MeSetupStorageNvData,
  IN  EFI_MEMORY_TYPE                 PoolType
  );
//[-end-201127-IB17510127-add]//

VOID
ClearSetupVariableInvalid (
  VOID
  );

EFI_STATUS
SetupRuntimeDetermination (
  IN OUT  CHIPSET_CONFIGURATION        *SetupNvData,
  IN EFI_STATUS                        GetSetupStatus,
  IN OUT  SA_SETUP                     *SaSetupNvData,
  IN EFI_STATUS                        GetSaSetupStatus,
  IN OUT  ME_SETUP                     *MeSetupNvData,
  IN EFI_STATUS                        GetMeSetupStatus,
  IN OUT  CPU_SETUP                    *CpuSetupNvData,
  IN EFI_STATUS                        GetCpuSetupStatus,
  IN OUT  PCH_SETUP                    *PchSetupNvData,
  IN EFI_STATUS                        GetPchSetupStatus,
  IN OUT  ME_SETUP_STORAGE             *MeSetupStorageNvData,
  IN EFI_STATUS                        GetMeSetupStorageStatus,
  IN OUT  SETUP_DATA                   *SetupDataNvData,
  IN EFI_STATUS                        GetSetupDataStatus,
  IN OUT  SI_SETUP                     *SiSetupNvData,
  IN EFI_STATUS                        GetSiSetupStatus
  );

EFI_STATUS
SetupRuntimeUpdateEveryBoot (
  IN OUT  CHIPSET_CONFIGURATION        *SetupNvData,
  IN OUT  SETUP_DATA                   *SetupDataNvData,
  IN OUT  SA_SETUP                     *SaSetupNvData,
  IN OUT  ME_SETUP                     *MeSetupNvData,
  IN OUT  CPU_SETUP                    *CpuSetupNvData,
  IN OUT  PCH_SETUP                    *PchSetupNvData,
  IN OUT  SI_SETUP                     *SiSetupNvData,
  IN OUT  ME_SETUP_STORAGE             *MeSetupStorageNvData
  );

UINT8
MeFwTypeDetect (
//[-start-190610-IB16990033-add]//
  VOID
//[-end-190610-IB16990033-add]//
  );


UINT8
UpdateTxtDprMemorySize (
//[-start-190610-IB16990033-add]//
  VOID
//[-end-190610-IB16990033-add]//
  );

UINT8 *
EFIAPI
ScuCreateGrayoutIfOpCode (
  IN VOID                 *OpCodeHandle
  );

UINT8 *
EFIAPI
ScuCreateFalseOpCode (
  IN VOID                 *OpCodeHandle
  );

UINT8 *
EFIAPI
ScuCreateTrueOpCode (
  IN VOID                 *OpCodeHandle
  );
#endif
