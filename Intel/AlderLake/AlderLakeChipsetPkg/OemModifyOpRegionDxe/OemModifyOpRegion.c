/** @file
  This driver will initial and update PrePostHotkey Variable.

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/DxeOemSvcChipsetLib.h>
#include <Library/BaseOemSvcChipsetLib.h>

STATIC
VOID
OemSetIgdOpRegion (
  IN EFI_EVENT                Event,
  IN VOID                     *Context
  )
{
  EFI_STATUS                    Status;
  IGD_OPREGION_PROTOCOL         *IgdOpRegion;
  EFI_STATUS                    EcGetLidState;
  BOOLEAN                       LidIsOpen;

  EcGetLidState = EFI_SUCCESS;
  LidIsOpen = TRUE;

  //
  //  Locate the Global NVS Protocol.
  //
  Status = gBS->LocateProtocol (
                  &gIgdOpRegionProtocolGuid,
                  NULL,
                  (VOID **)&IgdOpRegion
                  );
  ASSERT (Status == EFI_SUCCESS);

  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcGetLidState \n"));
  Status = OemSvcEcGetLidState (&EcGetLidState, &LidIsOpen);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcGetLidState Status: %r\n", Status));

  ASSERT (!EFI_ERROR (EcGetLidState));

  //
  // Follow EC.asl to set CLID = 3 (both Integrated LFP and External LFP lid are open) when Lid is open.
  // MBox1.CLID is descripted in "IGD OpRegion/Software SCI for Sandy Bridge/ Ivy Bridge BIOS Specification".
  //
  IgdOpRegion->OpRegion->MBox1.CLID = PcdGet32 ( PcdLidStatus );
  if (!EFI_ERROR (EcGetLidState)) {
    if (!LidIsOpen) {
      //
      // If get lid state from EC successfully and lid is closed.
      //
      IgdOpRegion->OpRegion->MBox1.CLID = 0;
    }
  } else {
    DEBUG ((DEBUG_INFO | DEBUG_ERROR, "EcGetLidState ERROR in OemModifyOpRegion! Status is %r.\n", EcGetLidState));
  }
  DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices Call: OemSvcSetIgdOpRegion \n"));
  Status = OemSvcSetIgdOpRegion( IgdOpRegion );
  DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices OemSvcSetIgdOpRegion Status: %r\n", Status));
}
/**

  Modify Igd OpRegion

  @param     ImageHandle     EFI_HANDLE
  @param     SystemTable     EFI_SYSTEM_TABLE pointer

  @retval     EFI_SUCCESS

**/
EFI_STATUS
EFIAPI
OemModifyOpRegionInit (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
  )
{
  EFI_STATUS                    Status;
  EFI_EVENT                     IddOpRegionEvent;
  VOID                          *NotifyReg;

  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  OemSetIgdOpRegion,
                  NULL,
                  &IddOpRegionEvent
                  );

  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return Status;

  }

  Status = gBS->RegisterProtocolNotify (
                  &gIgdOpRegionProtocolGuid,
                  IddOpRegionEvent,
                  &NotifyReg
                  );
  return EFI_SUCCESS;
}


