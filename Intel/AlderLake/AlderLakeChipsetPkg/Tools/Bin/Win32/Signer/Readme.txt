SignTool is a command-line tool that digitally signs files, verifies the signatures in files, and timestamps files. 
For information about why signing files is important, see Introduction to Code Signing. 
The tool is installed in the \Bin folder of the Microsoft Windows Software Development Kit (SDK) installation path.

SignTool is available as part of the Windows SDK, which you can download from https://developer.microsoft.com/windows/downloads/windows-10-sdk/.