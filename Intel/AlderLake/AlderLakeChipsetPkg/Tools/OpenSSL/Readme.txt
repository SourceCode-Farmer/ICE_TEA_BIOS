OpenSSL is available for Windows* and Linux*.
The latest version at the time of this document is OpenSSL 1.1.1g and 
the version Intel used official is OpenSSL 1.1.0j  20 Nov 2018(TigerLake).

The latest source code is in the git repository in GitHub*.
$git clone https://github.com/openssl/openssl.git

#
# Build OpenSSL in Windows
#
For the Windows operating system, after fetching source files from OpenSSL.org, 
run the commands below to build. 
The source contains all the information for building the binaries on the Windows operating system.

Requirements for the Windows build are Perl* and NMAKE.
Both are free 
(NMAKE via Visual Studio Community Edition https://visualstudio.microsoft.com/downloads/).
1. Download and install Perl at http://strawberryperl.com or ActivePerl.
2. Download and install Visual Studio Community Edition.
3. Install and set path for nasm.exe and rc.exe in the Windows SDK to the system path.
4. Open Visual Studio, select the folder of openssl, then click the Tools Tab.
   a. Tools �X>Command Line �X>Developer Command Prompt
5. Run the commands below.

On Windows, only pick one of the targets for configuration. In Windows, 10 x86 system:
$perl Configure VC-WIN32
$ perl Configure {VC-WIN32 | VC-WIN64A | VC-WIN64I | VC-CE}
$ nmake
$ nmake test
$ nmake install

To use ActivePerl*, you may also need to install the required modules, Strawberry Perl* windows 
version includes all required modules. More detail is in the INSTALL file. When doing the $name 
install, make sure to add user full access of the file folder, or login as administrator authority. 
The default is C:\Program Files (x86)\OpenSSL, and C:\Program Files (x86)\Common Files\SSL. Otherwise, 
you will observe an error to unable to create directory and copy over files.

NOTE: The Windows version OpenSSL binary can also be downloaded from the link below, if
you do not want to build from the source.
https://slproweb.com/products/Win32OpenSSL.html

Minimum tools requirement:
  libcrypto-1_1.dll
  libssl-1_1.dll
  openssl.exe
These files can be put in ChipsetPkg\Tools\OpenSSL or refer to Chapter OpenSSL to set OpenSSL environment.
