import os.path

def ConvertToNormalizeString(strr):
    strrlist=[]
    if(strr.startswith('{')):
        #remove '{' and '}'
        strr = strr.strip('{}')
        # for multi elements that included in '{}' and seperated with ', ' case
        if ", " in strr:
            strr = strr.split(', ')
            return strr # return list[]
        # for multi elements that included in '{}' and seperated with ',' case
        elif "," in strr:
            strr = strr.split(',')
            return strr # return list[]
        # for single element that included in '{}' case 
        else:
            strrlist.append(strr)
            return strrlist # return list[]  
    # for single string which starts with '0x' case
    elif(strr.startswith('0x')):
        strrlist.append(strr)  
        return strrlist # return list[]
    # for single integer value case
    else:
        strrlist.append(strr)
        return strrlist # return list[]

def CheckValue(strr, strr1):
    IsTheSame = True
    strr_len = len(strr)
    strr1_len = len(strr1)
    if(strr_len != strr1_len):
        IsTheSame = False
    if(IsTheSame == True):
        for index in range(strr_len):
            v1 = ConvertHexToDec(strr[index])
            v2 = ConvertHexToDec(strr1[index])
            if(v1 != v2):
                IsTheSame = False
                break
    return IsTheSame

def ConvertHexToDec(value):
    if(value.startswith("0x")):
        return int(value,base=16)
    else:
        return int(value,base=10)
        
ia32_list = []
x64_list = []
cur_file = os.path.abspath(__file__)
cur_dire = os.path.dirname(cur_file)
for cur_dir, next_dir_list, file_list in os.walk(cur_dire):
    if("IA32" in cur_dir):
        ia32_list.append(cur_dir)
    elif("X64" in cur_dir):
        x64_list.append(cur_dir)
ia32_list.extend(x64_list)
        
for index in range(0,len(ia32_list),2):       
    bios_pcd_path = ia32_list[index] + "\\" + "PcdList.txt"
    fsp_pcd_path = ia32_list[index + 1] + "\\" "PcdList.txt"
    result_path = cur_dire + "\\" + "PcdCompareResult_" + ia32_list[index].split("\\")[-1] + ".txt"

    BiosPcdFile = open(bios_pcd_path,'r')
    FspPcdFile = open(fsp_pcd_path,'r')
    ResultFile = open(result_path,'w')
    
    # Get file's entire content
    BiosPcdList = BiosPcdFile.readlines()
    FspPcdList = FspPcdFile.readlines()
    
    ResultFile.write("Detect different Pcd value between Bios and Fsp\n===============================================================\n")
    for FspPcdIndex in range(len(FspPcdList)):
        # Retrieve one line at a time from Fsp's PcdList.txt then split it into CurrentFspPcd = tuple(pcd_token_space, :, pcd_value)
        CurrentFspPcd = FspPcdList[FspPcdIndex].partition(":")
        Fsp_TokenSpace_Pcd = CurrentFspPcd[0]
        Fsp_Pcd_Value = CurrentFspPcd[2]
        for BiosPcdIndex in range(len(BiosPcdList)):
            # Retrieve one line at a time from Bios's PcdList.txt then split it into CurrentFspPcd = tuple(pcd_token_space, :, pcd_value)
            CurrentBiosPcd = BiosPcdList[BiosPcdIndex].partition(":")
            Bios_TokenSpace_Pcd = CurrentBiosPcd[0]
            Bios_Pcd_Value = CurrentBiosPcd[2]
            # Only compare Pcd that exist in Fsp PcdList.txt and Bios PcdList.txt
            if(Fsp_TokenSpace_Pcd == Bios_TokenSpace_Pcd):
                # stripe off ' '(space) and '\n'(newline) in head and tail of pcd_value first
                Fsp_Pcd_Value = Fsp_Pcd_Value.strip('\n ')
                Bios_Pcd_Value = Bios_Pcd_Value.strip('\n ')
                # if case handle string which include ' "..." ' (string) or ' L"..." ' (Unicode string)
                if(Fsp_Pcd_Value.startswith("\"") and Bios_Pcd_Value.startswith("\"") or Fsp_Pcd_Value.startswith("L") and Bios_Pcd_Value.startswith("L")):
                    # compare string directly without further process
                    if(Fsp_Pcd_Value == Bios_Pcd_Value):
                        break
                    else:
                        ResultFile.write("{}\nFsp ->{}\nBios->{}\n\n".format(Fsp_TokenSpace_Pcd,Fsp_Pcd_Value,Bios_Pcd_Value))
                # else case handle string which include ' {...} ' or ' 0x... '    
                else:
                    Fsp_Pcd_Nml_Value = ConvertToNormalizeString(Fsp_Pcd_Value)
                    Bios_Pcd_Nml_Value = ConvertToNormalizeString(Bios_Pcd_Value)                
                    if(CheckValue(Fsp_Pcd_Nml_Value, Bios_Pcd_Nml_Value)):
                        break
                    else:
                        ResultFile.write("{}\nFsp ->{}\nBios->{}\n\n".format(Fsp_TokenSpace_Pcd,Fsp_Pcd_Value,Bios_Pcd_Value))
        
BiosPcdFile.close()
FspPcdFile.close()
ResultFile.close()
