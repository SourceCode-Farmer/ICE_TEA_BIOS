## @file
# The purpose of this tool is to make engineers modify the GPIO values much more
# easily.
# This tool can transfer the GPIO table format in header file (.h) to the format which H2O GPIO feature needs (.csv).
#
# For more information of this tool is described on "Readme.txt".
#
#******************************************************************************
#* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#

import os
import sys
import csv

FieldDescription = [
    'Pad Name',
    'Pad Mode',
    'Pad Enable',
    'GPIO_HOSTSW_OWN',
    'GPIO_DIRECTION',
    'GPIO_OUTPUT_STATE',
    'GPIO_INT_CONFIG',
    'GPIO_RESET_CONFIG',
    'GPIO_ELECTRICAL_CONFIG',
    'GPIO_LOCK_CONFIG',
    'GPIO_OTHER_CONFIG',
    'RsvdBits',
    'PHASE_FLAGS'
]

ValidFieldValue = [
    '',
    'GpioHardwareDefault;GpioPadModeGpio;GpioPadModeNative1;GpioPadModeNative2;GpioPadModeNative3;GpioPadModeNative4;GpioPadModeNative5',
    'Enabled;Disabled',
    'GpioHostOwnDefault;GpioHostOwnAcpi;GpioHostOwnGpio',
    'GpioDirDefault;GpioDirInOut;GpioDirInInvOut;GpioDirIn;GpioDirInInv;GpioDirOut;GpioDirNone',
    'GpioOutDefault;GpioOutLow;GpioOutHigh',
    'GpioIntDefault;GpioIntDis;GpioIntNmi;GpioIntSmi;GpioIntSci;GpioIntApic;GpioIntLevel;GpioIntEdge;GpioIntLvlEdgDis;GpioIntBothEdge',
    'GpioResetDefault;GpioResumeReset;GpioHostDeepReset;GpioPlatformReset;GpioDswReset',
    'GpioTermDefault;GpioTermNone;GpioTermWpd5K;GpioTermWpd20K;GpioTermWpu1K;GpioTermWpu2K;GpioTermWpu5K;GpioTermWpu20K;GpioTermWpu1K2K;GpioTermNative;GpioNoTolerance1v8;GpioTolerance1v8',
    'GpioLockDefault;GpioPadConfigUnlock;GpioPadConfigLock;GpioOutputStateUnlock;GpioPadUnlock;GpioPadLock',
    'GpioRxRaw1Default;GpioRxRaw1Dis;GpioRxRaw1En;GpioDebounceDis',
    '0',
    'PreMem;PostMem'
]

GpioDef = [
    'GPIO_ICL_LP_GPP_A0',
    'GPIO_ICL_LP_GPP_A1',
    'GPIO_ICL_LP_GPP_A2',
    'GPIO_ICL_LP_GPP_A3',
    'GPIO_ICL_LP_GPP_A4',
    'GPIO_ICL_LP_GPP_A5',
    'GPIO_ICL_LP_GPP_A6',
    'GPIO_ICL_LP_GPP_A7',
    'GPIO_ICL_LP_GPP_A8',
    'GPIO_ICL_LP_GPP_A9',
    'GPIO_ICL_LP_GPP_A10',
    'GPIO_ICL_LP_GPP_A11',
    'GPIO_ICL_LP_GPP_A12',
    'GPIO_ICL_LP_GPP_A13',
    'GPIO_ICL_LP_GPP_A14',
    'GPIO_ICL_LP_GPP_A15',
    'GPIO_ICL_LP_GPP_A16',
    'GPIO_ICL_LP_GPP_A17',
    'GPIO_ICL_LP_GPP_A18',
    'GPIO_ICL_LP_GPP_A19',
    'GPIO_ICL_LP_GPP_A20',
    'GPIO_ICL_LP_GPP_A21',
    'GPIO_ICL_LP_GPP_A22',
    'GPIO_ICL_LP_GPP_A23',
    'GPIO_ICL_LP_GPP_B0',
    'GPIO_ICL_LP_GPP_B1',
    'GPIO_ICL_LP_GPP_B2',
    'GPIO_ICL_LP_GPP_B3',
    'GPIO_ICL_LP_GPP_B4',
    'GPIO_ICL_LP_GPP_B5',
    'GPIO_ICL_LP_GPP_B6',
    'GPIO_ICL_LP_GPP_B7',
    'GPIO_ICL_LP_GPP_B8',
    'GPIO_ICL_LP_GPP_B9',
    'GPIO_ICL_LP_GPP_B10',
    'GPIO_ICL_LP_GPP_B11',
    'GPIO_ICL_LP_GPP_B12',
    'GPIO_ICL_LP_GPP_B13',
    'GPIO_ICL_LP_GPP_B14',
    'GPIO_ICL_LP_GPP_B15',
    'GPIO_ICL_LP_GPP_B16',
    'GPIO_ICL_LP_GPP_B17',
    'GPIO_ICL_LP_GPP_B18',
    'GPIO_ICL_LP_GPP_B19',
    'GPIO_ICL_LP_GPP_B20',
    'GPIO_ICL_LP_GPP_B21',
    'GPIO_ICL_LP_GPP_B22',
    'GPIO_ICL_LP_GPP_B23',
    'GPIO_ICL_LP_GPP_C0',
    'GPIO_ICL_LP_GPP_C1',
    'GPIO_ICL_LP_GPP_C2',
    'GPIO_ICL_LP_GPP_C3',
    'GPIO_ICL_LP_GPP_C4',
    'GPIO_ICL_LP_GPP_C5',
    'GPIO_ICL_LP_GPP_C6',
    'GPIO_ICL_LP_GPP_C7',
    'GPIO_ICL_LP_GPP_C8',
    'GPIO_ICL_LP_GPP_C9',
    'GPIO_ICL_LP_GPP_C10',
    'GPIO_ICL_LP_GPP_C11',
    'GPIO_ICL_LP_GPP_C12',
    'GPIO_ICL_LP_GPP_C13',
    'GPIO_ICL_LP_GPP_C14',
    'GPIO_ICL_LP_GPP_C15',
    'GPIO_ICL_LP_GPP_C16',
    'GPIO_ICL_LP_GPP_C17',
    'GPIO_ICL_LP_GPP_C18',
    'GPIO_ICL_LP_GPP_C19',
    'GPIO_ICL_LP_GPP_C20',
    'GPIO_ICL_LP_GPP_C21',
    'GPIO_ICL_LP_GPP_C22',
    'GPIO_ICL_LP_GPP_C23',
    'GPIO_ICL_LP_GPP_D0',
    'GPIO_ICL_LP_GPP_D1',
    'GPIO_ICL_LP_GPP_D2',
    'GPIO_ICL_LP_GPP_D3',
    'GPIO_ICL_LP_GPP_D4',
    'GPIO_ICL_LP_GPP_D5',
    'GPIO_ICL_LP_GPP_D6',
    'GPIO_ICL_LP_GPP_D7',
    'GPIO_ICL_LP_GPP_D8',
    'GPIO_ICL_LP_GPP_D9',
    'GPIO_ICL_LP_GPP_D10',
    'GPIO_ICL_LP_GPP_D11',
    'GPIO_ICL_LP_GPP_D12',
    'GPIO_ICL_LP_GPP_D13',
    'GPIO_ICL_LP_GPP_D14',
    'GPIO_ICL_LP_GPP_D15',
    'GPIO_ICL_LP_GPP_D16',
    'GPIO_ICL_LP_GPP_D17',
    'GPIO_ICL_LP_GPP_D18',
    'GPIO_ICL_LP_GPP_D19',
    'GPIO_ICL_LP_GPP_E0',
    'GPIO_ICL_LP_GPP_E1',
    'GPIO_ICL_LP_GPP_E2',
    'GPIO_ICL_LP_GPP_E3',
    'GPIO_ICL_LP_GPP_E4',
    'GPIO_ICL_LP_GPP_E5',
    'GPIO_ICL_LP_GPP_E6',
    'GPIO_ICL_LP_GPP_E7',
    'GPIO_ICL_LP_GPP_E8',
    'GPIO_ICL_LP_GPP_E9',
    'GPIO_ICL_LP_GPP_E10',
    'GPIO_ICL_LP_GPP_E11',
    'GPIO_ICL_LP_GPP_E12',
    'GPIO_ICL_LP_GPP_E13',
    'GPIO_ICL_LP_GPP_E14',
    'GPIO_ICL_LP_GPP_E15',
    'GPIO_ICL_LP_GPP_E16',
    'GPIO_ICL_LP_GPP_E17',
    'GPIO_ICL_LP_GPP_E18',
    'GPIO_ICL_LP_GPP_E19',
    'GPIO_ICL_LP_GPP_E20',
    'GPIO_ICL_LP_GPP_E21',
    'GPIO_ICL_LP_GPP_E22',
    'GPIO_ICL_LP_GPP_E23',
    'GPIO_ICL_LP_GPP_F0',
    'GPIO_ICL_LP_GPP_F1',
    'GPIO_ICL_LP_GPP_F2',
    'GPIO_ICL_LP_GPP_F3',
    'GPIO_ICL_LP_GPP_F4',
    'GPIO_ICL_LP_GPP_F5',
    'GPIO_ICL_LP_GPP_F6',
    'GPIO_ICL_LP_GPP_F7',
    'GPIO_ICL_LP_GPP_F8',
    'GPIO_ICL_LP_GPP_F9',
    'GPIO_ICL_LP_GPP_F10',
    'GPIO_ICL_LP_GPP_F11',
    'GPIO_ICL_LP_GPP_F12',
    'GPIO_ICL_LP_GPP_F13',
    'GPIO_ICL_LP_GPP_F14',
    'GPIO_ICL_LP_GPP_F15',
    'GPIO_ICL_LP_GPP_F16',
    'GPIO_ICL_LP_GPP_F17',
    'GPIO_ICL_LP_GPP_F18',
    'GPIO_ICL_LP_GPP_F19',
    'GPIO_ICL_LP_GPP_G0',
    'GPIO_ICL_LP_GPP_G1',
    'GPIO_ICL_LP_GPP_G2',
    'GPIO_ICL_LP_GPP_G3',
    'GPIO_ICL_LP_GPP_G4',
    'GPIO_ICL_LP_GPP_G5',
    'GPIO_ICL_LP_GPP_G6',
    'GPIO_ICL_LP_GPP_G7',
    'GPIO_ICL_LP_GPP_H0',
    'GPIO_ICL_LP_GPP_H1',
    'GPIO_ICL_LP_GPP_H2',
    'GPIO_ICL_LP_GPP_H3',
    'GPIO_ICL_LP_GPP_H4',
    'GPIO_ICL_LP_GPP_H5',
    'GPIO_ICL_LP_GPP_H6',
    'GPIO_ICL_LP_GPP_H7',
    'GPIO_ICL_LP_GPP_H8',
    'GPIO_ICL_LP_GPP_H9',
    'GPIO_ICL_LP_GPP_H10',
    'GPIO_ICL_LP_GPP_H11',
    'GPIO_ICL_LP_GPP_H12',
    'GPIO_ICL_LP_GPP_H13',
    'GPIO_ICL_LP_GPP_H14',
    'GPIO_ICL_LP_GPP_H15',
    'GPIO_ICL_LP_GPP_H16',
    'GPIO_ICL_LP_GPP_H17',
    'GPIO_ICL_LP_GPP_H18',
    'GPIO_ICL_LP_GPP_H19',
    'GPIO_ICL_LP_GPP_H20',
    'GPIO_ICL_LP_GPP_H21',
    'GPIO_ICL_LP_GPP_H22',
    'GPIO_ICL_LP_GPP_H23',
    'GPIO_ICL_LP_GPP_R0',
    'GPIO_ICL_LP_GPP_R1',
    'GPIO_ICL_LP_GPP_R2',
    'GPIO_ICL_LP_GPP_R3',
    'GPIO_ICL_LP_GPP_R4',
    'GPIO_ICL_LP_GPP_R5',
    'GPIO_ICL_LP_GPP_R6',
    'GPIO_ICL_LP_GPP_R7',
    'GPIO_ICL_LP_GPP_S0',
    'GPIO_ICL_LP_GPP_S1',
    'GPIO_ICL_LP_GPP_S2',
    'GPIO_ICL_LP_GPP_S3',
    'GPIO_ICL_LP_GPP_S4',
    'GPIO_ICL_LP_GPP_S5',
    'GPIO_ICL_LP_GPP_S6',
    'GPIO_ICL_LP_GPP_S7',
    'GPIO_ICL_LP_GPD0',
    'GPIO_ICL_LP_GPD1',
    'GPIO_ICL_LP_GPD2',
    'GPIO_ICL_LP_GPD3',
    'GPIO_ICL_LP_GPD4',
    'GPIO_ICL_LP_GPD5',
    'GPIO_ICL_LP_GPD6',
    'GPIO_ICL_LP_GPD7',
    'GPIO_ICL_LP_GPD8',
    'GPIO_ICL_LP_GPD9',
    'GPIO_ICL_LP_GPD10',
    'GPIO_ICL_LP_GPD11',
    'GPIO_ICL_H_GPP_A0',
    'GPIO_ICL_H_GPP_A1',
    'GPIO_ICL_H_GPP_A2',
    'GPIO_ICL_H_GPP_A3',
    'GPIO_ICL_H_GPP_A4',
    'GPIO_ICL_H_GPP_A5',
    'GPIO_ICL_H_GPP_A6',
    'GPIO_ICL_H_GPP_A7',
    'GPIO_ICL_H_GPP_A8',
    'GPIO_ICL_H_GPP_A9',
    'GPIO_ICL_H_GPP_A10',
    'GPIO_ICL_H_GPP_A11',
    'GPIO_ICL_H_GPP_A12',
    'GPIO_ICL_H_GPP_A13',
    'GPIO_ICL_H_GPP_A14',
    'GPIO_ICL_H_GPP_A15',
    'GPIO_ICL_H_GPP_A16',
    'GPIO_ICL_H_GPP_A17',
    'GPIO_ICL_H_GPP_A18',
    'GPIO_ICL_H_GPP_A19',
    'GPIO_ICL_H_GPP_A20',
    'GPIO_ICL_H_GPP_A21',
    'GPIO_ICL_H_GPP_A22',
    'GPIO_ICL_H_GPP_A23',
    'GPIO_ICL_H_GPP_B0',
    'GPIO_ICL_H_GPP_B1',
    'GPIO_ICL_H_GPP_B2',
    'GPIO_ICL_H_GPP_B3',
    'GPIO_ICL_H_GPP_B4',
    'GPIO_ICL_H_GPP_B5',
    'GPIO_ICL_H_GPP_B6',
    'GPIO_ICL_H_GPP_B7',
    'GPIO_ICL_H_GPP_B8',
    'GPIO_ICL_H_GPP_B9',
    'GPIO_ICL_H_GPP_B10',
    'GPIO_ICL_H_GPP_B11',
    'GPIO_ICL_H_GPP_B12',
    'GPIO_ICL_H_GPP_B13',
    'GPIO_ICL_H_GPP_B14',
    'GPIO_ICL_H_GPP_B15',
    'GPIO_ICL_H_GPP_B16',
    'GPIO_ICL_H_GPP_B17',
    'GPIO_ICL_H_GPP_B18',
    'GPIO_ICL_H_GPP_B19',
    'GPIO_ICL_H_GPP_B20',
    'GPIO_ICL_H_GPP_B21',
    'GPIO_ICL_H_GPP_B22',
    'GPIO_ICL_H_GPP_B23',
    'GPIO_ICL_H_GPP_C0',
    'GPIO_ICL_H_GPP_C1',
    'GPIO_ICL_H_GPP_C2',
    'GPIO_ICL_H_GPP_C3',
    'GPIO_ICL_H_GPP_C4',
    'GPIO_ICL_H_GPP_C5',
    'GPIO_ICL_H_GPP_C6',
    'GPIO_ICL_H_GPP_C7',
    'GPIO_ICL_H_GPP_C8',
    'GPIO_ICL_H_GPP_C9',
    'GPIO_ICL_H_GPP_C10',
    'GPIO_ICL_H_GPP_C11',
    'GPIO_ICL_H_GPP_C12',
    'GPIO_ICL_H_GPP_C13',
    'GPIO_ICL_H_GPP_C14',
    'GPIO_ICL_H_GPP_C15',
    'GPIO_ICL_H_GPP_C16',
    'GPIO_ICL_H_GPP_C17',
    'GPIO_ICL_H_GPP_C18',
    'GPIO_ICL_H_GPP_C19',
    'GPIO_ICL_H_GPP_C20',
    'GPIO_ICL_H_GPP_C21',
    'GPIO_ICL_H_GPP_C22',
    'GPIO_ICL_H_GPP_C23',
    'GPIO_ICL_H_GPP_D0',
    'GPIO_ICL_H_GPP_D1',
    'GPIO_ICL_H_GPP_D2',
    'GPIO_ICL_H_GPP_D3',
    'GPIO_ICL_H_GPP_D4',
    'GPIO_ICL_H_GPP_D5',
    'GPIO_ICL_H_GPP_D6',
    'GPIO_ICL_H_GPP_D7',
    'GPIO_ICL_H_GPP_D8',
    'GPIO_ICL_H_GPP_D9',
    'GPIO_ICL_H_GPP_D10',
    'GPIO_ICL_H_GPP_D11',
    'GPIO_ICL_H_GPP_D12',
    'GPIO_ICL_H_GPP_D13',
    'GPIO_ICL_H_GPP_D14',
    'GPIO_ICL_H_GPP_D15',
    'GPIO_ICL_H_GPP_E0',
    'GPIO_ICL_H_GPP_E1',
    'GPIO_ICL_H_GPP_E2',
    'GPIO_ICL_H_GPP_E3',
    'GPIO_ICL_H_GPP_E4',
    'GPIO_ICL_H_GPP_E5',
    'GPIO_ICL_H_GPP_E6',
    'GPIO_ICL_H_GPP_E7',
    'GPIO_ICL_H_GPP_E8',
    'GPIO_ICL_H_GPP_E9',
    'GPIO_ICL_H_GPP_E10',
    'GPIO_ICL_H_GPP_E11',
    'GPIO_ICL_H_GPP_E12',
    'GPIO_ICL_H_GPP_F0',
    'GPIO_ICL_H_GPP_F1',
    'GPIO_ICL_H_GPP_F2',
    'GPIO_ICL_H_GPP_F3',
    'GPIO_ICL_H_GPP_F4',
    'GPIO_ICL_H_GPP_F5',
    'GPIO_ICL_H_GPP_F6',
    'GPIO_ICL_H_GPP_F7',
    'GPIO_ICL_H_GPP_F8',
    'GPIO_ICL_H_GPP_F9',
    'GPIO_ICL_H_GPP_F10',
    'GPIO_ICL_H_GPP_F11',
    'GPIO_ICL_H_GPP_F12',
    'GPIO_ICL_H_GPP_F13',
    'GPIO_ICL_H_GPP_F14',
    'GPIO_ICL_H_GPP_F15',
    'GPIO_ICL_H_GPP_F16',
    'GPIO_ICL_H_GPP_F17',
    'GPIO_ICL_H_GPP_F18',
    'GPIO_ICL_H_GPP_F19',
    'GPIO_ICL_H_GPP_F20',
    'GPIO_ICL_H_GPP_F21',
    'GPIO_ICL_H_GPP_F22',
    'GPIO_ICL_H_GPP_F23',
    'GPIO_ICL_H_GPP_G0',
    'GPIO_ICL_H_GPP_G1',
    'GPIO_ICL_H_GPP_G2',
    'GPIO_ICL_H_GPP_G3',
    'GPIO_ICL_H_GPP_G4',
    'GPIO_ICL_H_GPP_G5',
    'GPIO_ICL_H_GPP_G6',
    'GPIO_ICL_H_GPP_G7',
    'GPIO_ICL_H_GPP_G8',
    'GPIO_ICL_H_GPP_G9',
    'GPIO_ICL_H_GPP_G10',
    'GPIO_ICL_H_GPP_G11',
    'GPIO_ICL_H_GPP_G12',
    'GPIO_ICL_H_GPP_G13',
    'GPIO_ICL_H_GPP_G14',
    'GPIO_ICL_H_GPP_G15',
    'GPIO_ICL_H_GPP_H0',
    'GPIO_ICL_H_GPP_H1',
    'GPIO_ICL_H_GPP_H2',
    'GPIO_ICL_H_GPP_H3',
    'GPIO_ICL_H_GPP_H4',
    'GPIO_ICL_H_GPP_H5',
    'GPIO_ICL_H_GPP_H6',
    'GPIO_ICL_H_GPP_H7',
    'GPIO_ICL_H_GPP_H8',
    'GPIO_ICL_H_GPP_H9',
    'GPIO_ICL_H_GPP_H10',
    'GPIO_ICL_H_GPP_H11',
    'GPIO_ICL_H_GPP_H12',
    'GPIO_ICL_H_GPP_H13',
    'GPIO_ICL_H_GPP_H14',
    'GPIO_ICL_H_GPP_H15',
    'GPIO_ICL_H_GPP_H16',
    'GPIO_ICL_H_GPP_H17',
    'GPIO_ICL_H_GPP_H18',
    'GPIO_ICL_H_GPP_H19',
    'GPIO_ICL_H_GPP_H20',
    'GPIO_ICL_H_GPP_H21',
    'GPIO_ICL_H_GPP_H22',
    'GPIO_ICL_H_GPP_H23',
    'GPIO_ICL_H_GPP_I0',
    'GPIO_ICL_H_GPP_I1',
    'GPIO_ICL_H_GPP_I2',
    'GPIO_ICL_H_GPP_I3',
    'GPIO_ICL_H_GPP_I4',
    'GPIO_ICL_H_GPP_I5',
    'GPIO_ICL_H_GPP_I6',
    'GPIO_ICL_H_GPP_I7',
    'GPIO_ICL_H_GPP_I8',
    'GPIO_ICL_H_GPP_I9',
    'GPIO_ICL_H_GPP_I10',
    'GPIO_ICL_H_GPP_I11',
    'GPIO_ICL_H_GPP_I12',
    'GPIO_ICL_H_GPP_I13',
    'GPIO_ICL_H_GPP_I14',
    'GPIO_ICL_H_GPP_J0',
    'GPIO_ICL_H_GPP_J1',
    'GPIO_ICL_H_GPP_J2',
    'GPIO_ICL_H_GPP_J3',
    'GPIO_ICL_H_GPP_J4',
    'GPIO_ICL_H_GPP_J5',
    'GPIO_ICL_H_GPP_J6',
    'GPIO_ICL_H_GPP_J7',
    'GPIO_ICL_H_GPP_J8',
    'GPIO_ICL_H_GPP_J9',
    'GPIO_ICL_H_GPP_K0',
    'GPIO_ICL_H_GPP_K1',
    'GPIO_ICL_H_GPP_K2',
    'GPIO_ICL_H_GPP_K3',
    'GPIO_ICL_H_GPP_K4',
    'GPIO_ICL_H_GPP_K5',
    'GPIO_ICL_H_GPP_K6',
    'GPIO_ICL_H_GPP_K7',
    'GPIO_ICL_H_GPP_K8',
    'GPIO_ICL_H_GPP_K9',
    'GPIO_ICL_H_GPP_K10',
    'GPIO_ICL_H_GPP_K11',
    'GPIO_ICL_H_GPP_R0',
    'GPIO_ICL_H_GPP_R1',
    'GPIO_ICL_H_GPP_R2',
    'GPIO_ICL_H_GPP_R3',
    'GPIO_ICL_H_GPP_R4',
    'GPIO_ICL_H_GPP_R5',
    'GPIO_ICL_H_GPP_R6',
    'GPIO_ICL_H_GPP_R7',
    'GPIO_ICL_H_GPP_S0',
    'GPIO_ICL_H_GPP_S1',
    'GPIO_ICL_H_GPP_S2',
    'GPIO_ICL_H_GPP_S3',
    'GPIO_ICL_H_GPP_S4',
    'GPIO_ICL_H_GPP_S5',
    'GPIO_ICL_H_GPP_S6',
    'GPIO_ICL_H_GPP_S7',
    'GPIO_ICL_H_GPD0',
    'GPIO_ICL_H_GPD1',
    'GPIO_ICL_H_GPD2',
    'GPIO_ICL_H_GPD3',
    'GPIO_ICL_H_GPD4',
    'GPIO_ICL_H_GPD5',
    'GPIO_ICL_H_GPD6',
    'GPIO_ICL_H_GPD7',
    'GPIO_ICL_H_GPD8',
    'GPIO_ICL_H_GPD9',
    'GPIO_ICL_H_GPD10',
    'GPIO_ICL_H_GPD11',
    #
    # PadModeDefinition
    #
    'GpioHardwareDefault',
    'GpioPadModeGpio',
    'GpioPadModeNative1',
    'GpioPadModeNative2',
    'GpioPadModeNative3',
    'GpioPadModeNative4',
    'GpioPadModeNative5',
    'GpioPadModeNative6',
    #
    # HostSoftPadOwnDefinition
    #
    'GpioHostOwnDefault',
    'GpioHostOwnAcpi',
    'GpioHostOwnGpio',
    #
    # DirectionDefinition
    #
    'GpioDirDefault',
    'GpioDirInOut',
    'GpioDirInInvOut',
    'GpioDirIn',
    'GpioDirInInv',
    'GpioDirOut',
    'GpioDirNone',
    #
    # OutputStateDefinition
    #
    'GpioOutDefault',
    'GpioOutLow',
    'GpioOutHigh',
    #
    # InterruptConfigDefinition
    #
    'GpioIntDefault',
    'GpioIntDis',
    'GpioIntNmi',
    'GpioIntSmi',
    'GpioIntSci',
    'GpioIntApic',
    'GpioIntLevel',
    'GpioIntEdge',
    'GpioIntLvlEdgDis',
    'GpioIntBothEdge',
    #
    # PowerConfigDefinition
    #
    'GpioResetDefault',
    'GpioResumeReset',
    'GpioHostDeepReset',
    'GpioPlatformReset',
    'GpioDswReset',
    #
    # ElectricalConfigDefinition
    #
    'GpioTermDefault',
    'GpioTermNone',
    'GpioTermWpd5K',
    'GpioTermWpd20K',
    'GpioTermWpu1K',
    'GpioTermWpu2K',
    'GpioTermWpu5K',
    'GpioTermWpu20K',
    'GpioTermWpu1K2K',
    'GpioTermNative',
    #
    # GPIO LockConfiguration
    #
    'GpioLockDefault',
    'GpioPadConfigUnlock',
    'GpioPadConfigLock',
    'GpioOutputStateUnlock',
    'GpioPadUnlock',
    'GpioPadLock',
    #
    # OtherSettingsDefinition
    #
    'GpioRxRaw1Default',
    'GpioRxRaw1Dis',
    'GpioRxRaw1En'
]

GpioTable = []

def TransferDefinition (Phase, GpioData, writer):
    try:
        Count = 0
        for Setting in GpioData:
            if Setting.find('{') >= 0:
                Setting = Setting.replace('{', '')
            if Setting.find('}') >= 0:
                Setting = Setting.replace('}', '')
            if Setting.find('|') >= 0:
                Count += 1
                GpioTable.append(Setting.split('|')[0].strip() + ' | ' + Setting.split('|')[1].strip())
            if Setting.find('//') >= 0:
                break
            if Setting.strip() in GpioDef:
                Count += 1
                if Count == 3:
                    GpioTable.append('Enabled')
                    GpioTable.append(Setting.strip())
                else:
                    GpioTable.append(Setting.strip())
        Lost = 13 - Count
        while Lost != 0 and Lost != 13:
            if Lost == 5:
                GpioTable.append('GpioLockDefault')
            elif Lost == 4:
                GpioTable.append('GpioRxRaw1Default')
            elif Lost == 3:
                GpioTable.append('0x00')
            elif Lost == 2 and Phase == 'PostMem':
                GpioTable.append('PostMem')
            elif Lost == 2 and Phase == 'PreMem':
                GpioTable.append('PreMem')
            Lost -= 1
        if Lost != 13:
            writer.writerow(GpioTable)
            GpioTable[:] = []
    except:
        print('Processing ERROR!! '+Setting.strip())
        sys.exit(-1)

def GetGpioTableAndTransferFormat (Phase, TableFile, writer):
    """ The function is to get each GPIO setting from the file which user input.
        The program read a single line at once, transfers the format of each line.
        Finally the result will be written into the OutputCsvFile
    """
    Lines = TableFile.readlines()
    LineNum = 0
    try:
        for Data in Lines:
            LineNum += 1
            if Data.find('//') >= 0:
                #
                # If this line is commented out in InputFile, then skip this line.
                #
                CheckComment = Data.strip().split('//')[0]
                if CheckComment == '':
                    continue
                else:
                    GpioData = Data.strip().split(',')
                    TransferDefinition(Phase, GpioData, writer)
            else:
                GpioData = Data.strip().split(',')
                TransferDefinition(Phase, GpioData, writer)
    except:
        print('Processing ERROR! Example file line '+str(LineNum))
        sys.exit(-1)

def TransRcGpioTable (Phase, InputFile, OutputCsvFile):
    """ The main function for this module.
        1. Open the input file.
        2. Get the GPIO settings from the file which user input.
        3. Transfer the GPIO table format in .h file to .csv format which H2O GPIO feature uses and create output file.
    """
    if not os.path.exists(InputFile):
        print ('\"' + InputFile + '\"' + ' does not exist')
        sys.exit(-1)
    try:
        TableFile = open (InputFile, 'r')
        with open(OutputCsvFile, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(FieldDescription)
            writer.writerow(ValidFieldValue)
            GetGpioTableAndTransferFormat(Phase, TableFile, writer)
            print('Transformation Accomplished!\nPlease check the ' + OutputCsvFile + ' at ' + os.getcwd())
    except:
        print ('Read InputFile or write OutputCsvFile ERROR!')
    finally:
        TableFile.close()

if len(sys.argv) < 4:
    print 'This tool can transfer the GPIO table format in header file (.h) to the format which H2O GPIO feature needs (.gpio).'
    print 'Version: 1.00'
    print '[Usage]: TransRcGpioTable.exe argv1 argv2 argv3'
    print 'argv1: The GPIO table you want to transfer is "PreMem" or "PostMem" phase table'
    print 'argv2: The file in the fixed format of your GPIO Table in header file.'
    print 'argv3: The file name which will be generated by this tool.'
    sys.exit(-1)

TransRcGpioTable (sys.argv[1], sys.argv[2], sys.argv[3])
