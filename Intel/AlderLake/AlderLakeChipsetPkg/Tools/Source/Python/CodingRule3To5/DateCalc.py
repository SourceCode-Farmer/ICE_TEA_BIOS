## @file
# 
#******************************************************************************
#* Copyright (c) 2012-2014, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
import datetime

def DateCalculator():
    DateList = str(datetime.date.today()).split('-', 2)
    if int(DateList[1]) > 11:
        TempDate = [str(DateList[0]), str(int(DateList[1]) - 11).zfill(2), '1'.zfill(2)]
    else:
        TempDate = [str(int(DateList[0]) - 1), str(int(DateList[1]) + 1).zfill(2), '1'.zfill(2)]
    Date = TempDate[0][2:] + TempDate[1] + TempDate[2]
    
    return Date
