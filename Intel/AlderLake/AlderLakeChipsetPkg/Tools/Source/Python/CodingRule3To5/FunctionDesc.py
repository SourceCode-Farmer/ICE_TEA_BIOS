## @file
#  Conversion of function description from EDK to EDKII
#
#******************************************************************************
#* Copyright (c) 2012-2014, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#

import os
import sys

def FirstWordIndex (Line):
    FirstWord = Line.strip().split()[0]
    return Line.find(FirstWord)

def GetFunctionPurpose(FuncDesc):
    FuncPurpose = []
    PurposeRegion = False
    for Line in FuncDesc:
        if Line.strip().startswith('--*/'):
            return FuncPurpose
        if Line.strip() == '':
            continue
        if not PurposeRegion:
            if Line.strip().lower().startswith('routine description'):
                PurposeRegion = True
            continue
        if Line.strip().lower().startswith('arguments:') or Line.strip().lower().startswith('argument:') :
            break
        if Line.strip().lower().startswith('returns:') or Line.strip().lower().startswith('return:') :
            break
        if PurposeRegion:
            FuncPurpose.append(Line.strip())
    return FuncPurpose

def GetParameterDesc(FuncDesc, Parameter):
    ArgumentRegion = False
    Found = False
    ParamDesc = []
    for Line in FuncDesc:
        StripLowerCaseLine = Line.strip().lower()
        if Line.strip() == '':
            continue
        if not ArgumentRegion:
            if StripLowerCaseLine.startswith('arguments'):
                ArgumentRegion = True
            continue
        if StripLowerCaseLine.startswith('returns:') or StripLowerCaseLine.startswith('return:') or StripLowerCaseLine.startswith('return :'):
            break
        if not Found and Line.strip().startswith(Parameter + ' '):
            Found = True
            ParamDesc.append(Line.replace(Parameter + ' ',' ',1).replace('-','',1).split(' ',1)[1].strip().lstrip('-').strip())
            continue
        if Found:
            if FirstWordIndex(Line) > 8:
                ParamDesc.append(Line.replace(Parameter,'',1).strip().lstrip('-').strip())
            else:
                break;
    return ParamDesc


def GetParameterList(FuncDesc, OutBuf, OutFunctionStartIndex):
    j = OutFunctionStartIndex;
    ParameterList = []
    while (OutBuf[j].find('(') < 0):
        j += 1
    if OutBuf[j].find('(') > 0 and OutBuf[j].count('(') == OutBuf[j].count(')'):
        return ParameterList
    if OutBuf[j].find(')') >= 0 and OutBuf[j].find('(') < 0:
        return ParameterList
    if OutBuf[j].find('(VOID)'):
        return ParameterList
    j += 1
    if OutBuf[j].strip() == 'VOID':
        ParameterList.append('@param None')
        return ParameterList
    while (OutBuf[j].find(')') < 0):
        Line = OutBuf[j].rstrip().replace(',', ' ').replace(' OPTIONAL', '').replace(' CONST','')
        if Line.strip() == '':
            j += 1
            continue
        Parameter = ''
        if Line.find(' IN ') >= 0 and Line.find(' OUT ') >= 0:
            Parameter = '@param [in, out]'
        elif Line.find(' IN ') >= 0:
            Parameter = '@param [in]'
        elif Line.find(' OUT ') >= 0:
            Parameter = '@param [out]'
        elif FirstWordIndex(Line) < 8:
            Parameter = '@param'
        ParameterName = Line.split()[-1].strip('*')
        Parameter += (13 - len(Parameter))*' ' + ' ' + ParameterName
        ParamDesc = GetParameterDesc(FuncDesc, ParameterName)
        if len(ParamDesc) > 0:
            Parameter += (29 - len(Parameter))*' ' + '  ' + ParamDesc[0]
            ParameterList.append(Parameter)
            for k in range(1,len(ParamDesc)):
                ParameterList.append(31*' ' + ParamDesc[k])
        else:
            ParameterList.append(Parameter)

        j += 1
    return ParameterList

def GetFunctionResult(FuncDesc):
    FuncResult = []
    ResultRegion = False
    for Line in FuncDesc:
        if Line.strip().startswith('--*/') or Line.strip().endswith('*/'):
            return FuncResult
        if Line.strip() == '':
            continue
        Line = Line.replace('-','',1)
        StripLowerCaseLine = Line.strip().lower()
        if not ResultRegion:
            if StripLowerCaseLine.startswith('returns:') or StripLowerCaseLine.startswith('return:') or StripLowerCaseLine.startswith('return :'):
                ResultRegion = True
            continue
        if ResultRegion:
            if FirstWordIndex(Line) < 10:
                LineSplit = Line.split()
                ReturnValue = LineSplit[0]
                if len(LineSplit) > 1:
                    if Line.find(LineSplit[1]) - Line.find(LineSplit[0]) <= len(LineSplit[0]) + 1:
                        FuncResult.append('@return ' + Line.strip())
                        continue
                if ReturnValue.lower().startswith('other'):
                    FuncResult.append('@return ' + Line.strip())
                    continue
                if (len(Line.split(' ',1))>1):
                    ReturnValueDesc = Line.replace(ReturnValue,'',1).strip().strip('-').strip()
                    FuncResult.append('@retval ' + ReturnValue + (21 - len(ReturnValue))*' ' + '  ' + ReturnValueDesc)
                else:
                    FuncResult.append('@retval ' + ReturnValue )
            else:
                FuncResult.append(29*' '+'  '+Line.strip().strip('-').strip())
    return FuncResult


def ConvertDesc(FuncDesc, OutBuf, OutFunctionStartIndex):
    j = OutFunctionStartIndex
    OutBuf.insert(j,'/**\n')
    j += 1
    FuncPurpose = GetFunctionPurpose(FuncDesc)
    for Line in FuncPurpose:
        OutBuf.insert(j, ' ' + Line.rstrip(' ') + '\n')
        j += 1
    OutBuf.insert(j, '\n')
    j += 1

    ParamList = GetParameterList(FuncDesc, OutBuf, OutFunctionStartIndex)
    for Line in ParamList:
        OutBuf.insert(j, ' ' + Line.rstrip(' ') + '\n')
        j += 1
    OutBuf.insert(j, '\n')
    j += 1

    FuncResult = GetFunctionResult(FuncDesc)
    for Line in FuncResult:
        OutBuf.insert(j, ' ' + Line.rstrip(' ') + '\n')
        j += 1
    OutBuf.insert(j, '\n')
    j += 1
    OutBuf.insert(j,'**/\n')

def UpdateSingleFunctionDesc(InBuf, OutBuf, StartDescIndex):
    SkipList = ['EFIAPI','STATIC', 'static', 'EFI_BOOTSERVICE','EFI_BOOTSERVICE11','EFI_RUNTIMESERVICE', 'typedef']
    j = len(OutBuf) - 1
    while (OutBuf[j].find('(') < 0):
        j -= 1
    j -= 1
    while OutBuf[j].strip() in SkipList or OutBuf[j].strip() == '':
        j -= 1

    if OutBuf[j-1].strip() in SkipList:
        j -= 1

    OutFunctionStartIndex = j
    i = StartDescIndex + 1
    while (not InBuf[i].strip().endswith('-*/')):
        i += 1
    while (InBuf[i + 1].lstrip().startswith('//')):
        i += 1
    if InBuf[i + 1].strip() == ';':
        i += 1
        OutBuf[-1] = OutBuf[-1].rstrip() + ';\n'
    EndDescIndex = i
    ConvertDesc(InBuf[StartDescIndex + 1:EndDescIndex], OutBuf, OutFunctionStartIndex)
    return EndDescIndex - StartDescIndex

def UpdateFunctionDesc (OriginalFilePath, NewFilePath):
    OriginalFile = open(OriginalFilePath, 'r')
    InBuf = OriginalFile.readlines()
    NewFile = open(NewFilePath,'w')
    OutBuf = []
    SkipCount = 0
    for i in range(0, len(InBuf)):
        if SkipCount > 0:
            SkipCount -= 1
            continue
        Line = InBuf[i].rstrip(' ')
        if Line.lstrip().startswith('/*++') and InBuf[i-1].rstrip().endswith(')'):
            SkipCount = UpdateSingleFunctionDesc(InBuf, OutBuf, i)
        else:
            OutBuf.append(Line)
    NewFile.writelines(OutBuf)
    OriginalFile.close()
    NewFile.close()
    
def FolderUpdateFunctionDesc (OldFolder, NewFolder):
    print 'Processing ' + OldFolder
    if not os.path.exists(NewFolder):
        os.mkdir(NewFolder)
    DirEntries = os.listdir(OldFolder)
    for DirEntry in DirEntries:
        if os.path.isfile(OldFolder + os.sep + DirEntry):
            Ext = os.path.splitext(DirEntry)[1].lower()
            if Ext == '.c' or Ext == '.h':
                UpdateFunctionDesc(OldFolder + os.sep + DirEntry, NewFolder + os.sep + DirEntry)

def RecursiveUpdateFunctionDesc (OldFolder, NewFolder):
    if not os.path.exists(NewFolder):
        os.mkdir(NewFolder)
    FolderUpdateFunctionDesc (OldFolder, NewFolder)
    DirEntries = os.listdir(OldFolder)
    for DirEntry in DirEntries:
        if (not DirEntry.startswith('.')) and os.path.isdir(OldFolder + os.sep + DirEntry):
            RecursiveUpdateFunctionDesc (OldFolder + os.sep + DirEntry, NewFolder + os.sep + DirEntry)

def main():
    if len(sys.argv) < 3:
        print 'Insyde Function Description Update Utility'
        print 'Usage: UpdateFunctionDesc OriginalFolder NewFolder'
        sys.exit(-1)

    RecursiveUpdateFunctionDesc(sys.argv[1], sys.argv[2])
