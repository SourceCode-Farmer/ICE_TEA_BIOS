## @file
#  Utility to convert H2O 3.x coding style to H2O 5.x coding style
#
#******************************************************************************
#* Copyright (c) 2012-2014, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
import os
import sys
from IbHeader import UpdateHeader
from RemoveIB import RemoveIbNumber
from FunctionDesc import UpdateFunctionDesc

def UpdateCodingRules (OldFilePath, NewFilePath):
    Ext = os.path.splitext(OldFilePath)[1].lower()
    #
    # Print current processing path\file
    #
    print 'Processing ' + OldFilePath
    if Ext == '.c' or Ext == '.h' or Ext == '.asl' or Ext == '.dxs' or Ext == '.vfr' or Ext == '.act':
        Comment = '//'
    elif Ext == '.inf' or Ext == '.dec' or Ext == '.dsc' or Ext == '.fdf' or Ext == '.env':
        Comment = '#'
    elif Ext == '.inc' or Ext == '.asm':
        Comment = ';'
    elif Ext == '.bat':
        Comment = '@REM'
    else:
        return
    TempFilePath1 = '_temp1_'
    TempFilePath2 = '_temp2_'
    UpdateHeader(OldFilePath, TempFilePath1, Comment)
    if Ext == '.c' or Ext == '.h':
        RemoveIbNumber(TempFilePath1, TempFilePath2, Comment)
        UpdateFunctionDesc(TempFilePath2, NewFilePath)
    else:
        RemoveIbNumber(TempFilePath1, NewFilePath, Comment)

    if os.path.exists(TempFilePath1):
        os.remove(TempFilePath1)
    if os.path.exists(TempFilePath2):
        os.remove(TempFilePath2)

def FolderUpdateCodingRules (OldFolder, NewFolder):
    print 'Processing ' + OldFolder
    if not os.path.exists(NewFolder):
        os.mkdir(NewFolder)
    DirEntries = os.listdir(OldFolder)
    for DirEntry in DirEntries:
        if os.path.isfile(OldFolder + os.sep + DirEntry):
            UpdateCodingRules(OldFolder + os.sep + DirEntry, NewFolder + os.sep + DirEntry)

def RecursiveUpdateCodingRules(OldFolder, NewFolder):
    if not os.path.exists(NewFolder):
        os.mkdir(NewFolder)
    FolderUpdateCodingRules (OldFolder, NewFolder)
    DirEntries = os.listdir(OldFolder)
    for DirEntry in DirEntries:
        if (not DirEntry.startswith('.')) and os.path.isdir(OldFolder + os.sep + DirEntry):
            RecursiveUpdateCodingRules (OldFolder + os.sep + DirEntry, NewFolder + os.sep + DirEntry)

if len(sys.argv) < 3:
    print 'Insyde Coding Rule for H2O 5.0 Utility'
    print 'Usage: CodingRule3To5 OriginalFolder NewFolder'
    sys.exit(-1)

RecursiveUpdateCodingRules(sys.argv[1], sys.argv[2])
print 'Processing finished.'
