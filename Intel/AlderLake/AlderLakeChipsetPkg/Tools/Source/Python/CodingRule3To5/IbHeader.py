## @file
#  Insyde copyright header generation  
#
#******************************************************************************
#* Copyright (c) 2012-2014, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#

import os
import sys
IbFileHeader1 = [
    '/** @file\n', 
    ';******************************************************************************\n',
    ';* Copyright (c) 2012 - 2014, Insyde Software Corp. All Rights Reserved.\n',
    ';*\n',
    ';* You may not reproduce, distribute, publish, display, perform, modify, adapt,\n',
    ';* transmit, broadcast, present, recite, release, license or otherwise exploit\n',
    ';* any part of this publication in any form, by any means, without the prior\n',
    ';* written permission of Insyde Software Corporation.\n',
    ';*\n',
    ';******************************************************************************\n',
    '*/\n'
    '\n',
    ]
    
IbFileHeader2 = [    
    '## @file\n',
    '#******************************************************************************\n',
    '#* Copyright (c) 2012 - 2014, Insyde Software Corp. All Rights Reserved.\n',
    '#*\n',
    '#* You may not reproduce, distribute, publish, display, perform, modify, adapt,\n',
    '#* transmit, broadcast, present, recite, release, license or otherwise exploit\n',
    '#* any part of this publication in any form, by any means, without the prior\n',
    '#* written permission of Insyde Software Corporation.\n',
    '#*\n',
    '#******************************************************************************\n',
    '\n'
    ]    
IbFileHeader3 = [
    '; @file\n',
    ';******************************************************************************\n',
    ';* Copyright (c) 2012-2014, Insyde Software Corp. All Rights Reserved.\n',
    ';*\n',
    ';* You may not reproduce, distribute, publish, display, perform, modify, adapt,\n',
    ';* transmit, broadcast, present, recite, release, license or otherwise exploit\n',
    ';* any part of this publication in any form, by any means, without the prior\n',
    ';* written permission of Insyde Software Corporation.\n',
    ';*\n',
    ';******************************************************************************\n',
    '\n'
    ]
def GetFileAbstract (Lines, Comment):
    Abstract = []
    AbstractFlag = False
    for Index in range(0, len(Lines)):
        Line = Lines[Index].strip()
        if (not Line.startswith(Comment)) and (not Line == ''):
            break
        if Line.startswith(Comment):
            Line = Line.lstrip(Comment).strip('*').lstrip(';').strip('-').strip(' ')
            Line = Line.lstrip(Comment).strip('*').lstrip(';').strip('-').strip(' ')
        if Line.startswith('@file'):
            if Abstract == []:
                Line = Lines[Index + 1].lstrip(Comment).lstrip(' ')
                Abstract.append('  ' + Line)
            break
        if AbstractFlag and Line != '':
            Abstract.append('  ' + Line)
            if Comment == '#':
                break;
        else:
            if Line.startswith('Abstract'):
                AbstractFlag = True
    return Abstract    
    
def FirstCopyrightIsInsyde (Lines, Comment):
    Count = 0
    for Line in Lines:
        Count += 1
        if Count > 20:
            return False
        if Line.lower().find('copyright') > 0:
            if Line.lower().find('insyde') > 0:
                return True
            else:
                return False
    return False
    

def UpdateHeader (OriginalFilePath, NewFilePath, Comment):
    HeaderUpdated = False
    OriginalFile = open(OriginalFilePath, 'r')
    InBuf = OriginalFile.readlines()
    if InBuf[0].find('@file') > 0:
        if FirstCopyrightIsInsyde(InBuf, Comment):
            HeaderUpdated = True
    NewFile = open(NewFilePath,'w')
    OutBuf = []         
    NonFileHeaderStart = 0
    if Comment != '@REM' and not HeaderUpdated:
        for Line in InBuf:
            Line = Line.strip()
            if (not Line.startswith(Comment)) and (not Line == ''):
                break
            if Line.lstrip(Comment).lstrip('/*').lstrip('*').lstrip(' ').startswith('@file'):
                break
            NonFileHeaderStart += 1

        Abstract = GetFileAbstract(InBuf, Comment)

        if Comment == '//':
            IbFileHeader = IbFileHeader1
        elif Comment == '#':
            IbFileHeader = IbFileHeader2
        elif Comment == ';':
            IbFileHeader = IbFileHeader3
        OutBuf.append(IbFileHeader[0])
        for Line in Abstract:
            if Comment == '#':
                OutBuf.append('#' + Line + '\n')
            elif Comment == ';':
                OutBuf.append(';' + Line + '\n')
            else:
                OutBuf.append(Line + '\n')
        if Comment == '#':
            OutBuf.append('#\n')
        elif Comment == ';':
            OutBuf.append(';\n')
        else:
            OutBuf.append('\n')
        for i in range(1, len(IbFileHeader)):
            OutBuf.append(IbFileHeader[i])
    for i in range(NonFileHeaderStart, len(InBuf)):
        if (not HeaderUpdated) and (InBuf[i].startswith(Comment) or InBuf[i].startswith('/*')):
            if InBuf[i].find('@file') > 0:
                InBuf[i] = InBuf[i].replace('@file','')
        OutBuf.append(InBuf[i].rstrip(' '))
    NewFile.writelines(OutBuf)
    OriginalFile.close()
    NewFile.close()
    
def FolderUpdateHeader (OldFolder, NewFolder):
    print 'Processing ' + OldFolder
    if not os.path.exists(NewFolder):
        os.mkdir(NewFolder)
    DirEntries = os.listdir(OldFolder)
    for DirEntry in DirEntries:
        if os.path.isfile(OldFolder + os.sep + DirEntry):
            Ext = os.path.splitext(DirEntry)[1].lower()
            if Ext == '.c' or Ext == '.h' or Ext == '.asl' or Ext == '.dxs' or Ext == '.vfr':
                Comment = '//'
            elif Ext == '.inf' or Ext == '.dec' or Ext == '.dsc' or Ext == '.fdf' or Ext == '.env':
                Comment = '#'
            elif Ext == '.inc' or Ext == '.asm':
                Comment = ';'
            else:               
                Comment = ''
            if Comment != '':
                UpdateHeader(OldFolder + os.sep + DirEntry, NewFolder + os.sep + DirEntry, Comment)

def RecursiveUpdateHeader (OldFolder, NewFolder):
    if not os.path.exists(NewFolder):
        os.mkdir(NewFolder)
    FolderUpdateHeader (OldFolder, NewFolder)
    DirEntries = os.listdir(OldFolder)
    for DirEntry in DirEntries:
        if (not DirEntry.startswith('.')) and os.path.isdir(OldFolder + os.sep + DirEntry):
            RecursiveUpdateHeader (OldFolder + os.sep + DirEntry, NewFolder + os.sep + DirEntry)    
      
def main():
    if len(sys.argv) < 3:
        print 'Insyde File Header Update Utility'
        print 'Usage: IbUpdateHeader OriginalFolder NewFolder'
        sys.exit(-1)

    RecursiveUpdateHeader(sys.argv[1], sys.argv[2])
