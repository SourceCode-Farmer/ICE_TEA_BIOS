## @ PatchFspBinBaseAddress.py
#
# Copyright (c) 2015 - 2016, Intel Corporation. All rights reserved.<BR>
# This program and the accompanying materials are licensed and made available under
# the terms and conditions of the BSD License that accompanies this distribution.
# The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php.
#
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#
##

import os
import re
import sys
import struct
from   datetime import date

def GetBaseAddressToStr (binfile, offset):
                BaseAddressStr = ""
                for index in range(3,-1,-1):
                  value = readDataFromFile(binfile, offset + index)
                  str1 = str(hex(value)[2:])
                  str1 = str1.zfill(2)
                  str1 = str1.upper()
                  BaseAddressStr = BaseAddressStr + str1
                return BaseAddressStr

#
#  Read data from file
#
#  param [in]  binfile     Binary file
#  param [in]  offset      Offset
#  param [in]  len         Length
#
#  retval      value       Value
#
def readDataFromFile (binfile, offset, len=1):
      fd     = open(binfile, "rb")
      fsize  = os.path.getsize(binfile)
      offval = offset & 0xFFFFFFFF
      if (offval & 0x80000000):
          offval = fsize - (0xFFFFFFFF - offval + 1)
      fd.seek(offval)
      bytearray = [ord(b) for b in fd.read(len)]
      value = 0
      idx   = len - 1
      while  idx >= 0:
          value = value << 8 | bytearray[idx]
          idx = idx - 1
      fd.close()
      return value

def updateFspFvsBase (binfile, TargetFile):
      ext_file = str(os.path.splitext(TargetFile)[-1]).lower()
      if not os.path.exists(binfile):
        print "WARNING!  " + str(binfile) + " is not found."
        return 1
      if not os.path.exists(TargetFile):
        print "WARNING!  " + str(TargetFile) + " is not found."
        return 1
      if ext_file == ".dsc":
        DscFile        = open(TargetFile, "r")
        DscLines     = DscFile.readlines()
        DscFile.close()
        DscContent = []

        for line in DscLines:
            DscContent.append(line)
        DscFile = open(TargetFile,"w")

        for index in range(len(DscContent)):
              DscLine = DscContent[index]
              Match = re.match("([_a-zA-Z0-9]+).Pcd(Fspt|Fspm|Fsps)BaseAddress",DscLine)
              if Match:
                  DscLine = Match.group(1) + ".Pcd" + Match.group(2) + "BaseAddress|0x"
                  if Match.group(2) == 'Fspt':
                      BaseAddrStr = GetBaseAddressToStr (binfile, 0xFC)
                  elif Match.group(2) == 'Fspm':
                      BaseAddrStr = GetBaseAddressToStr (binfile, 0x100)
                  elif Match.group(2) == 'Fsps':
                      BaseAddrStr = GetBaseAddressToStr (binfile, 0x104)
                  DscLine = DscLine + BaseAddrStr + "\n"
              DscFile.writelines(DscLine)
        DscFile.close()
        return 0

def Main():
    #
    # Parse the options and args
    #
    if len(sys.argv) != 3:
        print "error"
        return 1
    ret = updateFspFvsBase (sys.argv[1], sys.argv[2])

    if ret != 0:
      return 1
    return 0

if __name__ == '__main__':
    sys.exit(Main())
