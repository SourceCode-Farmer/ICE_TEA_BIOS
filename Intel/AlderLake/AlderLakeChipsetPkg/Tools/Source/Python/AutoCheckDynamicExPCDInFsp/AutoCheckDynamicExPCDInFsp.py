## @file
#
#     The purpose of this tool is to make sure the FspPcdListLibNull.inf have
# include all PcdsDynamicEx that have use in FSP to avoid system hang up. It 
# will auto gen a new FspPcdListLibNull.inf to replase old one.
#
#******************************************************************************
#* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#

import os, sys, datetime, shutil

WS                = os.getcwd()+"/"
FspPcdListLibPath = "FspBinPkg/Library/FspPcdListLib"
RootPathList      = []

#
# File Content area.
#
InfContent = """## @file
# Library instance to list all DynamicEx PCD FSP consumes.
#
# @copyright
#  Insyde CONFIDENTIAL
#  Copyright {0} Insyde Corporation.
#
#  Automatic generated inf file.
#
##

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = FspPcdListLibNull
  FILE_GUID                      = C5D4D79E-3D5C-4EB6-899E-6F1563CB0B32
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = BASE
  LIBRARY_CLASS                  = NULL
#
# The following information is for reference only and not required by the build tools.
#
# VALID_ARCHITECTURES = IA32 X64 IPF EBC
#

[LibraryClasses]
  BaseLib

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
  ClientOneSiliconPkg/SiPkg.dec

[Sources]
  FspPcdListLibNull.c

[Pcd]
#
# List all the DynamicEx PCDs that FSP will consume.
# FSP Dispatch mode bootloader will include this INF to ensure all the PCDs are
# built into PCD database.
#
""".format (str(datetime.datetime.now().year))

CFileContent = """
#include <Base.h>
/**
  Automatic generated file, do nothing function.
**/
VOID
FspPcdListLibNull (
  VOID
  )
{
  return;
}
"""

#
#  As finction name
#
def Need2FindSubFileName (S,*EndString):
    Array = map(S.endswith,EndString)
    return True if True in Array else False

#
#  Create FspPcdListLibNull.inf and FspPcdListLibNull.c into code base.
#
def WriteResult(PcdList):
    Data = []
    try:
        DestinaPath = WS+'Intel/'+sys.argv[1]+'/'+sys.argv[1]+FspPcdListLibPath
        os.makedirs(WS+'Intel/'+sys.argv[1]+'/'+sys.argv[1]+FspPcdListLibPath)
        File = open(DestinaPath+"/FspPcdListLibNull.inf",'w')
        File.write(InfContent)
        for PCD in PcdList:
            Data.append(PCD+"\n")
        File.writelines(Data)
        File.close()
        File = open(DestinaPath+"/FspPcdListLibNull.c",'w')
        File.write(CFileContent)
        File.close()
    except:
        print("ResultFile file open error !")

#
#  Search specify file and find all keywoard pcd add into list.
#
def SearchFilesWithLine(KeyWord, *SubFileName):
    TempFile      = "N/A"
    Step          = 1
    PCDList       = []
    for Root,Dirs,Files in os.walk(WS):
        for file in Files:
            #
            # Open the file that you need to search.
            #
            if Need2FindSubFileName(file, SubFileName):
                PreCount  = len(PCDList)
                TempFile  = Root + os.sep + file
                TempFile  = TempFile.replace("\\","\\\\")
                try:
                    File = open(TempFile,'r')
                    if Root[0:Root.rfind(os.sep)+1] not in RootPathList:
                        RootPathList.append(Root[0:Root.rfind(os.sep)+1])
                except:
                    print ("Loop file "+TempFile+"  open error:")
                    continue
                #
                # Start to search DynamicEx in all dec file.
                #
                Step = 1
                for FileLine in File:
                    try:
                        if FileLine.strip()[0] == "#":
                            continue
                    except:
                        continue
                    #
                    #  Step 1: Fine this dec file have DynamicEx PCD or not.
                    #
                    if Step == 1:
                        if KeyWord in FileLine:
                            Step = 2
                            continue
                    #
                    # Step 2: Put all not duplicate DynamicExPcd in PCD list.
                    #
                    if Step == 2:
                        if ('[' in FileLine) and (']' in FileLine) and (KeyWord not in FileLine):
                            Step = 1
                        else:
                            if "|" in FileLine:
                                PCD = FileLine.strip().split("|",1)[0].strip()
                                if PCD not in PCDList:
                                    PCDList.append(PCD)
                #
                # Record above pcd is in which dec file
                #
                if len(PCDList) > PreCount:
                    PCDList.append("# ^^^ Above Pcd is define in "+str(TempFile)+" ^^^")
                File.close()
    return PCDList

#
#  Search all inf that use in fsp, and check they truely use it or not.
#
def FindAllInfFileUseInFsp():
    DscDefine        = []
    IsUseDynamicPcd  = []
    DscDynamicExList = SearchFilesWithLine ("PcdsDynamicExDefault", '.dsc')
    DecDynamicExList = SearchFilesWithLine ("PcdsDynamicEx", '.dec', '.template')
    File = open(WS+"Intel/{0}/{0}FspPkg/{0}FspPkg.dsc".format(sys.argv[1]),'r')
    for FileLine in File:
        try:
            if FileLine.strip()[0] == "#":
                continue
        except:
            continue
        if "DEFINE" in FileLine:
            TemBuf = FileLine.replace("DEFINE","").replace(" ","").replace("\n","")
            if '$' in TemBuf:
                TemBuf = TemBuf.replace("$(","").replace(")","")
                for TmpStr in DscDefine:
                    TmpStr = TmpStr.split("=",1)
                    if TmpStr[0] in TemBuf:
                        TemBuf = TemBuf.replace(TmpStr[0],TmpStr[1])
                        break
                DscDefine.append(TemBuf)
            else:
                DscDefine.append(TemBuf)
        elif ".inf" in FileLine:
            #
            #  Parsing the string line
            #
            if '#' in FileLine:
                FileLine = FileLine.split("#",1)[0]
            elif '{' in FileLine:
                FileLine = FileLine.split("{",1)[0]

            if "|" in FileLine:
                InfPath = FileLine.strip().split("|",1)[1]
            else:
                InfPath = FileLine.strip()
            #
            # Replace variable into correct path
            #
            for Def in DscDefine:
                Def = Def.split("=",1)
                if Def[0] in InfPath:
                    InfPath = InfPath.replace("$("+Def[0]+")", Def[1])
                    break
            for RP in RootPathList:
                if os.path.isfile(RP+InfPath):
                    InfPath = RP+InfPath
                    break
            #
            # Open the inf file to check PCD
            #
            InfFile = open(InfPath)
            FileStr = InfFile.read()
            for PCD in DecDynamicExList:
                if str(PCD) in FileStr:
                    if ((PCD not in IsUseDynamicPcd) and (PCD in DscDynamicExList)):
                        IsUseDynamicPcd.append(PCD)
            InfFile.close()

    WriteResult(IsUseDynamicPcd)
    File.close()

#
# I am the entry !!
#
try:
    if sys.argv[1] == '-h' or sys.argv[1] == '-H' or sys.argv[1] == 'h' or sys.argv[1] == 'H':
        print("\n  [CAT] Usage : Put this python into your work space then type\n  >> py [This tool name].py [Your Platform Name]")
    else:
        print ("Start to Gen Pcd that use in FSP and add into FspPcdListLibNull.inf.")
        if os.path.isdir(WS+'Intel/'+sys.argv[1]+'/'+sys.argv[1]+FspPcdListLibPath):
            shutil.rmtree(WS+'Intel/'+sys.argv[1]+'/'+sys.argv[1]+FspPcdListLibPath)
        FindAllInfFileUseInFsp()
        print ("End of AP :)")
except Error as i:
    print("\n  [CAT] Usage : Put this python into your work space then type\n  >> py [This tool name].py [Your Platform Name]")
    print(i)