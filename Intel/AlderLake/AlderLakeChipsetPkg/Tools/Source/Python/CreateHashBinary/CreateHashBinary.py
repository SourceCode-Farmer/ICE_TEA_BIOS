#;******************************************************************************
#;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
#;*
#;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#;* transmit, broadcast, present, recite, release, license or otherwise exploit
#;* any part of this publication in any form, by any means, without the prior
#;* written permission of Insyde Software Corporation.
#;*
#;******************************************************************************

import os
import sys
import codecs
import re

mFlashAreaBase = 0
mFlashAreaSize = 0
mSbbBase = 0
mVariableFlashReservedBase = 0
mVariableFlashReservedSize = 0
mReservedList = []
mSkipListInStr = []

def Calculate(Base, Data, op):
   if op == '+':
     Value = Base + Data
   elif op == '-':
     Value = Base - Data
   elif op == '*':
     Value = Base * Data
   elif op == '-':
     Value = Base / Data
   return Value

def ConverToSpiAddress(MemoryAddress):
    if (MemoryAddress > mFlashAreaBase):
        return MemoryAddress - mFlashAreaBase    
    else :
        return 0

def IsComment (Data):
    #
    # This function is used to skip comments.
    #    Current it only supports '//', '/*', and '*/'.
    #
    if Data.strip(' ').startswith('//'):
        return 1
    elif Data.strip(' ').startswith('/*'):
        if Data.strip(' ').strip('\n').endswith('*/'):
            return 1
        else:
            return 2
    elif Data.strip(' ').strip('\n').endswith('*/'):
        return 3

    return 0

def Isbrackets(Data):
    if (Data.strip(' ').startswith('{') and Data.strip(' ').strip('\n').endswith('}')) or \
       (Data.strip(' ').startswith('{') and Data.strip(' ').strip('\n').endswith('},')) or \
       (Data.strip(' ').startswith('{') and Data.strip(' ').strip('\n').endswith('}};')) :
      return 1
    elif Data.strip(' ').startswith('{') :
      return 2
    elif Data.strip(' ').strip('\n').endswith('}') or \
         Data.strip(' ').strip('\n').endswith('},') or \
         Data.strip(' ').strip('\n').endswith('}};'):
      return 3
    
    return 0
def StrToInt (Str):
  if Str.startswith('0x'):
    Value = int(Str, 16)
  else:
    Value = int(Str, 10)
  return Value
def FindPcd(File, Pcd):
  Value = 0
  Str = "" 
  LineList = File.readlines()
  for Line in LineList:
    if Line.find(Pcd) > 0:
      Str = Line.split(' ')
      Str = Str[1]
      Value = StrToInt (Str)
      break;
  File.seek(0)  
  return Value
  
def InitValue(File):
  
  Pcd = 'PcdFlashAreaBaseAddress'
  global mFlashAreaBase
  mFlashAreaBase = FindPcd(File, Pcd)
  
  Pcd = 'PcdFlashAreaSize'
  global mFlashAreaSize
  mFlashAreaSize = FindPcd(File, Pcd)
  
  Pcd = 'PcdFlashSbbBase:'
  global mSbbBase
  mSbbBase = FindPcd(File, Pcd)
  
  Pcd = 'VariableFlashReservedBase'
  global mVariableFlashReservedBase
  mVariableFlashReservedBase = FindPcd(File,Pcd);
  
  Pcd = 'VariableFlashReservedSize'
  global mVariableFlashReservedSize
  mVariableFlashReservedSize = FindPcd(File,Pcd);

def PatchReservedFile(ReservedFile):
    FileByLine = ReservedFile.readlines()
    RegionInfo = []
    Temp = '';
    StartRecord = False
    CommentSkipLine = False
    BracketsRange = False

    for Data in FileByLine:
      if (Data.strip(' ').find('FAULTTOLERANCE_SKIP_TABLE') == 0) : 
        StartRecord = True
        
      if StartRecord :
        if CommentSkipLine :
          if IsComment(Data) == 3: # */
            CommentSkipLine = False
            continue
        if IsComment(Data) == 2: # /*
          CommentSkipLine = True
          continue
        elif IsComment(Data) == 1: # //
          continue
        
        if Isbrackets(Data) == 1:
          Temp = '';
          Temp = re.sub("\{|\}|\(|\)|", '', Data)
          Temp = Temp.replace(' ', '').replace(';', '').strip('\n').rstrip(',')
          RegionInfo = Temp.split(',')
          mSkipListInStr.append(RegionInfo)      
        else:
          if BracketsRange:
            Temp = Temp + Data.strip('\n')
            if Isbrackets(Data) == 3:
              BracketsRange = False           
              Temp = re.sub("\{|\}|\(|\)|", '', Temp)
              Temp = Temp.replace(' ', '').replace(';', '').strip('\n').rstrip(',')
              RegionInfo = Temp.split(',')
              mSkipListInStr.append(RegionInfo)
          if Isbrackets(Data) == 2:
            BracketsRange = True
            Temp = '';
            Temp = Data.strip('\n')

    #print('---mSkipListInStr---')
    #for I in mSkipListInStr:
    #  print (I)
    #print('---mSkipListInStr End---')

def GetPcdValueForList(File):
    operate = ['+', '-', '*', '/']

    for ListItem in mSkipListInStr:
      for Index in range(2):
        IsOpExist = False
        Temp = ''
        Operatelist = ['+']
        for op in operate:
          if op in ListItem[Index]:
            IsOpExist = True
        
        if IsOpExist == True :
          Temp = ListItem[Index]
          for c in range(len(Temp)-1):
            for op in operate:
              if Temp[c] == op:
                Operatelist.append(op)
          for op in operate:
            Temp = Temp.replace(op,'+')
          PcdItemCount = 0
          PcdItem = []
          PcdItem = Temp.split('+')
          Value = 0
          for Item in PcdItem:
            if 'FixedPcdGet32' in Item:
              Temp = FindPcd(File, Item.replace('FixedPcdGet32', ''))
            else :
              Temp = StrToInt(Item)
            Value = Calculate(Value, Temp, Operatelist[PcdItemCount])
            PcdItemCount += 1
        else:
          Temp = ListItem[Index]
          if 'FixedPcdGet32' in Temp:
            Value = FindPcd(File, Temp.replace('FixedPcdGet32', ''))
          else :
            Value = StrToInt(Temp)
        mReservedList.append(int(Value))
    #print('---mReservedList---')
    #for I in mReservedList:
    #  print (hex(I))
    #print('---mReservedList End---')
    
    try :
      ReservedListFile = open('ReservedList.txt', 'w')
      Index = 0
      for I in mReservedList:
        if (Index % 2 == 0):
          Temp = hex (ConverToSpiAddress(I))
        else :
          Temp = hex (I)
        Index += 1
        ReservedListFile.write(Temp + '\n')
    except IOError as err:
      print ('Write file fail :' + str(err))
    finally:
      if 'ReservedListFile' in locals():
        ReservedListFile.close()

def IsInSkipList(CurrentLine):
  Index = 0
  Count = len(mReservedList)
  while Index < Count:
    if CurrentLine >= ConverToSpiAddress(mReservedList[Index]) and CurrentLine < ConverToSpiAddress(mReservedList[Index] + mReservedList[Index+1]):
      return True
    Index += 2  
  
  return False  

def IsSkipVariableReservedRegion(CurrentLine):

  if mSbbBase <= mVariableFlashReservedBase:
    #
    # Variable region in SBB
    # 
    if CurrentLine >= ConverToSpiAddress(mVariableFlashReservedBase) and  CurrentLine < ConverToSpiAddress(mVariableFlashReservedBase + mVariableFlashReservedSize):
      return True
   
  return False  


def CreateHashBinary(NewHashBuffer, OrgBuffer):
  Index = 0
  
  while Index < mFlashAreaSize:
    if IsInSkipList(Index) or IsSkipVariableReservedRegion(Index):
      NewHashBuffer.write(codecs.decode('ff', 'hex'))
    else:
      NewHashBuffer.write(bytes([OrgBuffer[Index]]))
    Index += 1
 
#
# main start
#
if len(sys.argv) < 4:
    print('[Usage]: xxx argv1 argv2 argv3 argv4')
    print('argv1: PcdList.txt')
    print('argv2: XXXLake.fd')
    print('argv3: Hash.fd')
    print('argv4: ChasmFallsReservedDefine.h.')
    
# Read file
#
if not os.path.exists(sys.argv[1]):
  print ('Error! ' + sys.argv[1] +' is inexistence!')
  sys.exit(-1)
if not os.path.exists(sys.argv[2]):
  print ('Error! ' + sys.argv[2] +' is inexistence!')
  sys.exit(-1)
if not os.path.exists(sys.argv[4]):
  print ('Error! ' + sys.argv[4] +' is inexistence!')
  sys.exit(-1)
    
try:
  PcdListFile = open(sys.argv[1], 'r')
  OrgBiosFile = open(sys.argv[2], 'rb')   
  NewBiosFile = open(sys.argv[3], 'wb')
  ReservedTableFile = open(sys.argv[4], 'r')
  
  InitValue(PcdListFile)
  PatchReservedFile(ReservedTableFile)
  GetPcdValueForList (PcdListFile)
  
  OrgBiosFile = OrgBiosFile.read()
  OrgBiosData = list(OrgBiosFile)
  
  CreateHashBinary(NewBiosFile, OrgBiosData)
  NewBiosFile.close()

except IOError as err:
  print ('Read file fail :' + str(err))
finally:
  if 'PcdListFile' in locals():
    PcdListFile.close()
  if 'ReservedTableFile' in locals():
    ReservedTableFile.close()  
