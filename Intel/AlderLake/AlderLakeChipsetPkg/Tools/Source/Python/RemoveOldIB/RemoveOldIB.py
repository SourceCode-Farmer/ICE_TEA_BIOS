## @file
# The tool for automatically removing the old IB numbers which are created 11 months ago.
#
#******************************************************************************
#* Copyright (c) 2012-2013, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
import os
import sys
from DateCalc import DateCalculator

def RemoveIbNumber (OriginalFilePath, NewFilePath, Comment):
    OriginalFile = open(OriginalFilePath, 'r')
    NewFile = open(NewFilePath,'w')
    InBuf = OriginalFile.readlines()
    OutBuf = []
    RemoveFlag = False
    for Line in InBuf:
        if Line.lstrip().startswith(Comment):
            if Line.find('[-start-') > 0 or Line.find('[-end-') > 0:
                IBTextList = Line.split('-', 4)
                Date = DateCalculator()
                if int(IBTextList[2]) <= int(Date):
def FolderRemoveIb (OldFolder, NewFolder):
def RecursiveRemoveIb (OldFolder, NewFolder):
if len(sys.argv) < 3:
RecursiveRemoveIb(sys.argv[1], sys.argv[2])
