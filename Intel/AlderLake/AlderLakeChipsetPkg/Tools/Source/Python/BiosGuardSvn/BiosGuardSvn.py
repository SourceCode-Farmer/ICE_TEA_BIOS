## @file
#
# The purpose for this tool is to update BiosGuardSvn depending 
# PCD - PcdBiosGuardConfigBgpdtBiosSvn automatically in the build time.
#
#******************************************************************************
#* Copyright (c) 2012-2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#

import os
import sys
import codecs
WorkSpaces = []

def GetBiosSvnReferenceValue(InputFile, DscDecFile):
    """ Using this function to find and get PcdBiosGuardConfigBgpdtBiosSvn or STR_ESRT_VERSION .
        Return a list named 'Result' to caller.
        Result[0] : IsFound, which is a BOOLEAN type. If the PCD has be found it will be TRUE, otherwise will be FALSE.
        Result[1] : A value which BiosGuardSvn.
    """
    IsFound = False
    LinesList = InputFile.readlines()
    for Line in LinesList:
        if DscDecFile :
            if Line.find('PcdBiosGuardConfigBgpdtBiosSvn') > 0 and (Line.find('PcdBiosGuardConfigBgpdtBiosSvn') < Line.find('#') or Line.find('#') < 0):
                Value = int (Line.split('|')[1][2:], 16)
                IsFound = True
                break
        else :
             if Line.find('STR_ESRT_VERSION') > 0 and (Line.find('STR_ESRT_VERSION') < Line.find('//') or Line.find('//') < 0):
                Value = int(str(Line.split('#')[-1][-11:-3]),16)
                IsFound = True
                break           
    Result = [IsFound]
    if IsFound:
        Result.append(Value)
    return Result

def OpenFile(InputFilePath, BiosGuardSvnValue):
    """ Using this function to open the file which inputed,
        call GetBiosSvnReferenceValue(),
        if Inputfile is DEC or DSC file, get PcdBiosGuardConfigBgpdtBiosSvn value
        if Inputfile is an UNI file, get the STR_ESRT_VERSION value
        Return BiosGuardSvnValue.
    """
    DscDecFile = False
    if InputFilePath[-3:] == 'dsc' or InputFilePath[-3:] == 'dec':
        DscDecFile = True
    if not os.path.exists(InputFilePath):
        print ('BiosGuardSvn.exe error! ' + InputFilePath +' is inexistence!')
        sys.exit(-1)
    try:
        if DscDecFile:
            InputFile = open(InputFilePath,'r')
        else :
            InputFile = codecs.open(InputFilePath, 'rb', 'utf-16-le')
        Result = GetBiosSvnReferenceValue(InputFile,DscDecFile)
        if not Result[0]:
            print ('BiosGuardSvn.exe info ' + InputFilePath +' STR_ESRT_VERSION / PcdBiosGuardConfigBgpdtBiosSvn not found!')
        else:
            BiosGuardSvnValue = Result[1]
    except IOError:
        print ('BiosGuardSvn.exe Read file error! ' + InputFilePath)
    finally:
        InputFile.close()
    return BiosGuardSvnValue

def UpdateBiosSvnOfPcdValue(InputFilePath, BiosGuardSvnValue):
    """ Update PcdBiosGuardConfigBgpdtBiosSvn which is declared in ChipsetPkg DSC file """
    Status = True
    if InputFilePath[-3:] == 'dsc':
        DecFile = False
    if not os.path.exists(InputFilePath):
        print ('BiosGuardSvn.exe error! ' + InputFilePath +' is inexistence!')
        sys.exit(-1)
    try:
        InputFile = open(InputFilePath, 'r')
        FileLinesList = InputFile.readlines()
        LineIndex = 0
        for Line in FileLinesList:
            if Line.find('PcdBiosGuardConfigBgpdtBiosSvn') > 0 and (Line.find('PcdBiosGuardConfigBgpdtBiosSvn') < Line.find('#') or Line.find('#') < 0):
                PcdContent = Line.split('|')
                FileLinesList[LineIndex] = PcdContent[0] + ('|' + str(hex(BiosGuardSvnValue))) + '\n'
                break
            LineIndex += 1
    except IOError:
        print ('BiosGuardSvn.exe file access error! ' + InputFilePath)
        Status = False
    finally:
        InputFile.close()

    try:
        InputFile = open(InputFilePath, 'w+')
        InputFile.writelines(FileLinesList)
    except IOError:
        print ('BiosGuardSvn.exe update PcdBiosGuardConfigBgpdtBiosSvn error! ' + InputFilePath)
        Status = False
    finally:
        InputFile.close()
    return Status

def UpdateBiosGuardSvnValue(InputFilePath, BiosGuardSvnValue):
    """ Update BiosGuardSvnValue to BiosGuardSetting.ini."""
    Status = True
    if not os.path.exists(InputFilePath):
        print ('BiosGuardSvn.exe error! ' + InputFilePath +' is inexistence!')
        sys.exit(-1)
    try:
        InputFile = open(InputFilePath, 'r')
        FileLinesList = InputFile.readlines()
        LineIndex = 0
        for Line in FileLinesList:
            if Line.find('BiosSvn') > 0 and (Line.find('BiosSvn') < Line.find(';') or Line.find(';') < 0):
                BiosSvn = Line.split('=', 1)
                FileLinesList[LineIndex] = BiosSvn[0] + '= ' + str(hex(BiosGuardSvnValue)) + '\n'
                break
            LineIndex += 1
    except IOError:
        print ('BiosGuardSvn.exe file access error! ' + InputFilePath)
        Status = False
    finally:
        InputFile.close()
        
    try:
        InputFile = open(InputFilePath, 'w+')
        InputFile.writelines(FileLinesList)
    except IOError:
        print ('BiosGuardSvn.exe update BiosGuardSetting.ini error! ' + InputFilePath)
        Status = False
    finally:
        InputFile.close()
    return Status

def GetMultipleWorkspacePaths(Workspace):
    WorkSpaces.append (Workspace)
    if os.getenv('PACKAGES_PATH') == None:
        return
    for WorkSpace in os.getenv('PACKAGES_PATH').split(os.pathsep):
        WorkSpaces.append (WorkSpace)
    return

def FindFilePath(File):
    for WorkSpace in WorkSpaces:
        if WorkSpace.endswith(":"):
            WorkSpace = WorkSpace + os.sep
        if os.path.exists (os.path.join (WorkSpace, File)):
            return os.path.join (WorkSpace, File)
    return ""

def BiosGuardSvn(InputChipsetPkg, WorkSpace):
    """ The main funtion for this module.
        1. Get ESRT value in Project.env
        1. Search and update PcdBiosGuardConfigBgpdtBiosSvn in PLATFORMChipsetPkg\Package.dsc file.
        2. Search and get PcdBiosGuardConfigBgpdtBiosSvn in Build\PLATFORMMultiBoardPkg\Project.dsc file.
        3. Update the BiosGuardSvn value in PLATFORMMultiBoardPkg\PlatformConfig\BiosGuardSetting.ini .
    """
    ChipsetPkg = InputChipsetPkg
    WorkSpaceList = WorkSpace.split('\\')
    ProjectPkgList = os.getcwd().split('\\')
    for Index, Item in enumerate(ProjectPkgList):
        if Index > len(WorkSpaceList)-1 or Item != WorkSpaceList[Index]:
            break
    #
    # Assemble the file path: PLATFORMChipsetPkg\Package.dsc, Build\PLATFORMMultiBoardPkg\Project.dsc
    # and PLATFORMMultiBoardPkg\PlatformConfig\BiosGuard\BiosGuardSetting.ini.
    #
    ChipsetPkgDscPathList    = WorkSpaceList + [ChipsetPkg, 'Package.dsc']
    ChipsetPkgDscPath        = '\\'.join(ChipsetPkgDscPathList)
    BuildProjectDscPathList  = WorkSpaceList +['Build'] + ProjectPkgList[Index:] + ['Project.dsc']
    BuildProjectDscPath      = '\\'.join(BuildProjectDscPathList)
    BiosGuardSettingPathList = WorkSpaceList + ProjectPkgList[Index:] + ['PlatformConfig', 'BiosGuard', 'BiosGuardSetting.ini']
    BiosGuardSettingPath     = '\\'.join(BiosGuardSettingPathList)
    EsrtVersionPathList      = WorkSpaceList + ProjectPkgList[Index:] + ['Project.uni']
    EsrtVersionPath      = '\\'.join(EsrtVersionPathList)

    if not os.path.exists (ChipsetPkgDscPath) or not os.path.exists (BiosGuardSettingPath) or not os.path.exists (EsrtVersionPath):	
            #
            # Try to get PLATFORMChipsetPkg.dec and BiosGuardSetting.ini in multiple workspace environment.
            #
            GetMultipleWorkspacePaths(WorkSpace)
            ChipsetPkgDscPath    =  FindFilePath(os.path.join (ChipsetPkg, 'Package.dsc'))
            BuildProjectDscPathList = WorkSpaceList +['Build'] + [os.getenv('PROJECT_PKG')] + ['Project.dsc']
            BuildProjectDscPath  = '\\'.join(BuildProjectDscPathList)
            BiosGuardSettingPath = FindFilePath(os.path.join (os.getenv('PROJECT_PKG'), 'PlatformConfig', 'BiosGuard', 'BiosGuardSetting.ini'))
            EsrtVersionPath      = FindFilePath(os.path.join (os.getenv('PROJECT_PKG'), 'Project.uni'))

    BiosGuardSvnValue = None
    #
    # Search PcdBiosGuardConfigBgpdtBiosSvn in PLATFORMChipsetPkg\Package.dsc file.
    #
    BiosGuardSvnValue = OpenFile (ChipsetPkgDscPath, BiosGuardSvnValue)
    if BiosGuardSvnValue is None:
        print ('BiosGuardSvn.exe error! PcdBiosGuardConfigBgpdtBiosSvn not be declared')
        sys.exit(-1)

    #
    # Search and get STR_ESRT_VERSION in PLATFORMMultiBoardPkg\Project.uni file.
    # update PcdBiosGuardConfigBgpdtBiosSvn in PLATFORMChipsetPkg\Package.dsc file
    #
    BiosGuardSvnValue = OpenFile (EsrtVersionPath, BiosGuardSvnValue)
    ChipsetPkgStatus = UpdateBiosSvnOfPcdValue (ChipsetPkgDscPath, BiosGuardSvnValue)
    if ChipsetPkgStatus :
        print ('PcdBiosGuardConfigBgpdtBiosSvn updated successfully.')

    #
    # Search and get PcdBiosGuardConfigBgpdtBiosSvn in Build\PLATFORMMultiBoardPkg\Project.dsc file.
    # Update the BiosGuardSvn value in PLATFORMMultiBoardPkg\PlatformConfig\BiosGuard\BiosGuardSetting.ini .
    #
    BiosGuardSvnValue = OpenFile (BuildProjectDscPath, BiosGuardSvnValue) 
    Status = UpdateBiosGuardSvnValue(BiosGuardSettingPath, BiosGuardSvnValue)
    if Status:
        print ('BiosGuard Svn updated successfully.')
    else:
        sys.exit(-1)
BiosGuardSvn(sys.argv[1], sys.argv[2])
