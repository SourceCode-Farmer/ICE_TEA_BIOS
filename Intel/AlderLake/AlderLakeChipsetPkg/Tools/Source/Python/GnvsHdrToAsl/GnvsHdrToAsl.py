## @file
# This tool is made to transfer the global nvs area structure from header to asl.
# It contains structure item, offset and comments transferring.
# An file will be output if no error occurs.
#
#******************************************************************************
#* Copyright (c) 2012-2014, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#

import os
import sys
import datetime

mStructItems = []
mAslItems = []
mCStructLineNumStart = 0
mCStructLineNumEnd = 0
mAslStructLineNumStart = 0
mAslStructLineNumEnd = 0
mInsydeCopyRight = '/** @file'                                                                       + '\n' + \
                  ''                                                                                + '\n' + \
                  ';******************************************************************************' + '\n' + \
                  ';* Copyright (c) 2012 - 2014, Insyde Software Corp. All Rights Reserved.'        + '\n' + \
                  ';*'                                                                              + '\n' + \
                  ';* You may not reproduce, distribute, publish, display, perform, modify, adapt,' + '\n' + \
                  ';* transmit, broadcast, present, recite, release, license or otherwise exploit'  + '\n' + \
                  ';* any part of this publication in any form, by any means, without the prior'    + '\n' + \
                  ';* written permission of Insyde Software Corporation.'                           + '\n' + \
                  ';*'                                                                              + '\n' + \
                  ';******************************************************************************' + '\n' + \
                  '*/'


def IsComment (Data):
    #
    # This function is used to skip comments.
    #    Current it only supports '//', '/*', and '*/'.
    #
    if Data.strip(' ').startswith('//'):
        return 1
    elif Data.strip(' ').startswith('/*'):
        if Data.strip(' ').strip('\n').endswith('*/'):
            return 1
        else:
            return 2
    elif Data.strip(' ').strip('\n').endswith('*/'):
        return 3

    return 0

def GetGnvsStruct (StructFile):
    #
    # Read the structure of the header file and stores it to mStructItems.
    #
    # The produced mStructItems has several kinds of data. Briefly described as below:
    #  1. Comments
    #     If a line starts with '//' or /* it is a pure comment, it will be recorded in the mStructItems.
    #     For this item, the array count is 1.
    #  2. Structure members + comments
    #     Structure members will be split into an array as following example shows:
    #     Example:
    #     Array ID:     0                 1                                    2   3     4                                             5
    #               UINT8       SmiFunction;                                ///< 002 [SMIF]  SMI Function Call (ASL to SMI via I/O Trap)
    #     (1) The ";" in the 1st element is removed.
    #     (2) "[" and "]" in the 4th element are removed.
    #     (3) In the array item 0, UINT8 is set as 1. UINT16 is set as 2 and so on.
    #         If the item is an array and it is defined as UINT16, then the item 0 value will be 2 x Array size.
    #     (4) Array item 4 is made for the ASL structure name. The program will fail with an error message if the item does not
    #         start with '[' and end with ']'. This make sure the application knows the corresponding ASL item name.
    #     (5) If a ASL item name between '[' and ']' is '----', it means its a place holder in ASL code. 
    #         In Asl, no item name will be assigned.
    #         It will be something like below:
    #         Offset(30),         , 8,  // Offset(30),    Battery Number Present
    #     (6) For structure members started with Reserved, we assume that it is reserved field and will not validate the array item 4.
    #     (7) Structure item comment should always be "///<". If this symbol is not added after a structure member, error occurs and process fails.
    #
    global mCStructLineNumStart
    global mCStructLineNumEnd
    global mStructItems

    LineNum = 0
    GotStruct = False
    CommentSkipLine = False
    Lines = StructFile.readlines()
    StructItem = []
    StructItemCount = 0
    StructItemCountComment = 7
    StructItemCountFlagStart = 3
    StructItemCountFlagEnd = 4

    for Data in Lines:
        #
        # Skip comments.
        #
        if CommentSkipLine:
            if IsComment(Data) == 3: # */
                CommentSkipLine = False
            LineNum = LineNum + 1
            continue
        if IsComment(Data) == 1: # //
            LineNum = LineNum + 1
            continue
        elif IsComment(Data) == 2: # /*
            CommentSkipLine = True
            LineNum = LineNum + 1
            continue

        #
        # Only comes to here when IsComment returns 0 and CommentSkipLine = False
        # Find the C structure header signature: "typedef struct {", "}", and "PLATFORM_NVS_AREA",
        # so we can know where to store the structure content.
        #
        if not GotStruct and Data.find('typedef struct {') >= 0:
            mCStructLineNumStart = LineNum
            GotStruct = True;
        elif GotStruct:
            if Data.find('}') >= 0:
                if Data.find('PLATFORM_NVS_AREA') >= 0:
                    mCStructLineNumEnd = LineNum
                    break
        LineNum = LineNum + 1

    CommentSkipLine = False
    LineNum = 0

    for StructIndex in range(mCStructLineNumStart + 1, mCStructLineNumEnd):
        #
        # 1. Replace tab key to make sure the indentation is consistent.
        # 2. Skip the empty lines.
        #
        Lines[StructIndex] = Lines[StructIndex].replace('\t', '  ')
        if Lines[StructIndex].isspace():
            LineNum = LineNum + 1
            continue

        #
        # Skip comments.
        #
        if CommentSkipLine:
            if IsComment(Lines[StructIndex]) == 3: # */
                CommentSkipLine = False
            Item = [0]
            Item.append(Lines[StructIndex])
            mStructItems.append(Item);
            LineNum = LineNum + 1
            continue
        if IsComment(Lines[StructIndex]) == 1: # //
            Item = [0]
            Item.append(Lines[StructIndex])
            mStructItems.append(Item);
            LineNum = LineNum + 1
            continue
        elif IsComment(Lines[StructIndex]) == 2: # /*
            CommentSkipLine = True
            Item = [0]
            Item.append(Lines[StructIndex])
            mStructItems.append(Item);
            LineNum = LineNum + 1
            continue

        #
        # Only comes to here when IsComment returns 0 and CommentSkipLine = False
        # Structure member/Comments strings process and error handling.
        #
        StructItem = []
        StructItemCount = 0
        Temp = Lines[StructIndex].strip().split(' ')
        StructItemComment = ''
        StructItemFlag = ''
        for Data in Temp:
            if not (Data == ''):
                StructItemCount = StructItemCount + 1
                if StructItemCount >= StructItemCountComment:
                    if not (StructItemCount == StructItemCountComment):
                        StructItemComment = StructItemComment + ' ' + Data
                    else:
                        StructItemComment = StructItemComment + Data
                elif StructItemCount >= StructItemCountFlagStart and StructItemCount <= StructItemCountFlagEnd:
                    StructItemFlag = StructItemFlag + Data + ' '
                    if StructItemCount == StructItemCountFlagEnd:
                        StructItem.append(StructItemFlag)
                else:
                    StructItem.append(Data)

        if StructItemCount >= StructItemCountComment:
            StructItem.append(StructItemComment.strip('\n'))
        elif StructItemCount < (StructItemCountComment - 1) and not (StructItem[1].startswith('Reserved')):
            print('')
            print('[ERROR] Line: ' + str(LineNum + mCStructLineNumStart + 2) + ', Lack of ACPI name! Tool does not know how to transfer this to ASL!')
            print('The ASL name should be put after the line number and between \'[\' and \']\'')
            return False

        if StructItem[0] == 'UINT8':
            StructItem[0] = 1
        elif StructItem[0] == 'UINT16':
            StructItem[0] = 2
        elif StructItem[0] == 'UINT32':
            StructItem[0] = 4
        elif StructItem[0] == 'UINT64':
            StructItem[0] = 8
        elif StructItem[0] == 'UINTN':
            StructItem[0] = 8

        StructItem[1] = StructItem[1].strip(';')
        ReservedArrayCountStr = ''
        ReservedArrayCountInt = 0
        if StructItem[1].endswith(']'):
            for idx in range(StructItem[1].index('[') + 1, StructItem[1].index(']')):
                ReservedArrayCountStr = ReservedArrayCountStr + StructItem[1][idx]
            ReservedArrayCountInt = int(ReservedArrayCountStr)
            StructItem[0] = StructItem[0] * ReservedArrayCountInt

        if not (StructItem[1].startswith('Reserved')) and not (StructItem[4].startswith('[') and StructItem[4].endswith(']')):
            print('')
            print('[ERROR] Line: ' + str(LineNum + mCStructLineNumStart + 2) + ', Lack of ACPI name! Tool does not know how to transfer this to ASL!')
            print('The ASL name should be put after the line number and between \'[\' and \']\'')
            return False
        elif not (StructItem[1].startswith('Reserved')):
            StructItem[4] = StructItem[4].strip('[').strip(']')
            if StructItem[4].startswith('----'):
                StructItem[4] = '    '

        mStructItems.append(StructItem);
        LineNum = LineNum + 1
        if StructItemCount >= StructItemCountComment:
            if not StructItem[2].startswith('///< Offset '):
                print('')
                print('[ERROR] Line: ' + str(LineNum + mCStructLineNumStart + 2) + ', This struct member should always have the comment, \'///<\', to includ offect and ASL name.')
                return False

    return True

def PatchAsl (AslFile, OutputAslFile):
    #
    # Patch the data of mAslItems into the asl file that needs to be patched.
    # It also check and patches Insyde copyright.
    #
    global mAslItems
    global mAslStructLineNumStart
    global mAslStructLineNumEnd
    global mInsydeCopyRight

    LineNum = 0
    GotAsl = False
    CommentSkipLine = False
    Lines = AslFile.readlines()
    StructItem = []
    StructItemCount = 0
    AslOutFile = []
    idx = 0

    #
    # Check if copyright exists. If it does not, patch one with current year.
    # If it exists but the year is wrong, only patch the line for year.
    #
    InsydeCopyRightLocal = mInsydeCopyRight.replace('2014, Insyde Software Corp.', '{0}, Insyde Software Corp.'.format(int(datetime.date.today().year)), 1)
    CopyRightLines = InsydeCopyRightLocal.split('\n')
    FoundCopyRight = False
    for Data in Lines:
        if Data.find(CopyRightLines[2]) >= 0:
            if Lines[idx + 2].find(CopyRightLines[4]) >= 0 and \
                Lines[idx + 3].find(CopyRightLines[5]) >= 0 and \
                Lines[idx + 4].find(CopyRightLines[6]) >= 0 and \
                Lines[idx + 5].find(CopyRightLines[7]) >= 0 and \
                Lines[idx + 6].find(CopyRightLines[8]) >= 0 and \
                Lines[idx + 7].find(CopyRightLines[9]) >= 0 and \
                Lines[idx + 8].find(CopyRightLines[10]) >= 0 and \
                Lines[idx + 9].find(CopyRightLines[11]) >= 0:
                Lines[idx + 1] = CopyRightLines[3] + '\n'
            FoundCopyRight = True
            break
        idx = idx + 1
    if not FoundCopyRight:
        for Data in CopyRightLines:
            AslOutFile.append(Data + '\n')

    idx = 0
    for Data in Lines:
        #
        # Skip comments.
        #
        if CommentSkipLine:
            if IsComment(Data) == 3: # */
                CommentSkipLine = False
            LineNum = LineNum + 1
            continue
        if IsComment(Data) == 1: # //
            LineNum = LineNum + 1
            continue
        elif IsComment(Data) == 2: # /*
            CommentSkipLine = True
            LineNum = LineNum + 1
            continue

        #
        # Only comes to here when IsComment returns 0 and CommentSkipLine = False
        # Find the asl structure header signature: "Field(GNVS,", "{", and "}" so we can know where to patch its content.
        #
        if not GotAsl and Data.strip(' ').startswith('Field(GNVS,'):
            if Data.strip(' ').strip('\n').endswith('{'):
                mAslStructLineNumStart = LineNum + 1
            GotAsl = True;
        elif GotAsl:
            if Data.strip(' ').strip('\n').endswith('{'):
                mAslStructLineNumStart = LineNum + 1
            if Data.find('}') >= 0:
                mAslStructLineNumEnd = LineNum
                break
        LineNum = LineNum + 1

    #
    # Start to patch asl code.
    #
    for idx in range(0, mAslStructLineNumStart):
        AslOutFile.append(Lines[idx])
    for idx in range(0, len(mAslItems)):
        AslOutFile.append(mAslItems[idx])
    for idx in range(mAslStructLineNumEnd, len(Lines)):
        AslOutFile.append(Lines[idx])

    try:
        #
        # Update results.
        #
        OutputFile = open(OutputAslFile,'w')
        OutputFile.writelines(AslOutFile)
    except IOError as err:
        print ('Read fild error:' + str(err))
    finally:
        if 'OutputFile' in locals():
            OutputFile.close()

def GetSizeForAlphabet (Alphabet):
    CountResult = 0
    if Alphabet.isdigit():
        CountResult = int(Alphabet)
    elif Alphabet.isalpha():
        CountResult = ord(Alphabet) - ord('A') + 10
    return CountResult


def GetStrFromNumber (Number):
    CountResult = 0
    if Number >= 10:
        CountResult = chr(Number - 10 + ord('A'))
    else:
        CountResult = str(Number)
    return CountResult

def TransferCHeaderStructureToAsl ():
    #
    # Transfer the C structure data in mStructItems to asl format, and stores it to mAslItems.
    # 1. Indentation
    #    Any structure element 0 has the value 0 and element 1 has at least two spaces in front of the first element
    #    will be treated as comments and add to asl code.
    #    Rule: If there are over three spaces, fix the indent in ASL to be 16 columns.
    #          If there are only two spaces, keep the indent in ASL to be 2 columns.
    #          If there is no space in front of the asl member name, ignore the comment.
    # 2. Reserved items
    #    Reserved items will be transfer to "Offset(...)" in ASL to reserve the position.
    #    If it is an array, proper value will also be input to the "Offset(...)".
    # 3. Structure member array
    #    The application needs to know how a structure member is to be placed and named.
    #    There are two posible ways:
    #       (1) UINT8       LtrEnable[20];                               ///< 176:183 [LTR:1-K] Latency Tolerance Reporting Enable
    #         [LTR:1-K] means that this UINT8 x 20 array will be added to ASL code as LTR1 ~ LTRK as below.
    #           Sample:
    #             ...
    #             LTR8, 8,      // (133) Latency Tolerance Reporting Enable
    #             LTR9, 8,      // (134) Latency Tolerance Reporting Enable
    #             LTRA, 8,      // (135) Latency Tolerance Reporting Enable
    #             LTRB, 8,      // (136) Latency Tolerance Reporting Enable
    #             ...
    #             LTRI, 8,      // (143) Latency Tolerance Reporting Enable
    #             LTRJ, 8,      // (144) Latency Tolerance Reporting Enable
    #             LTRK, 8,      // (145) Latency Tolerance Reporting Enable
    #       (2) UINT8       GtfTaskFileBufferPort0[7];                  ///< 080 [GTF0] GTF Task File Buffer for Port 0
    #         [GTF0] means that this UINT8 x 7 array will be added to ASL code as a single GTF0 as below.
    #           Sample:
    #             GTF0, 56,     // (080) GTF Task File Buffer for Port 0
    # 4. Error handling
    #    Proper error handling will be used to make sure the application won't be confused by incorrect inputs.
    #       (1) For the structure member array, if application found ':', it will check for '-' in the ASL variable name.
    #           If '-' is missing in this condition, error occurs.
    #       (2) For the structure member array, values separated by '-' in the ASL variable name should be valid digits.
    #           If those values are not valid digits, error occurs.
    #       (3) If the ASL variable name length is longer than 4, error occurs.
    #
    global mStructItems
    global mAslItems
    global mCStructLineNumStart

    PreviousIdx = 0
    CurrentByteIdx = 0
    LineNum = 0
    ItemLineHandled = 0

    for data in mStructItems:
        if data[0] == 0:
            if ItemLineHandled == 0 or ItemLineHandled == 1:
                if data[1].startswith('   '):
                    mAslItems.append('{0:28s}{1}'.format(' ', data[1].strip(' ')))
                elif  data[1].startswith('  '):
                    mAslItems.append(data[1])
            else:
                ItemLineHandled = ItemLineHandled - 1
            LineNum = LineNum + 1
            continue

        CurrentByteIdx = CurrentByteIdx + PreviousIdx
        if data[1].startswith('Reserved'):
            AslItem = '  Offset(' + str(CurrentByteIdx + data[0]) + '),\n'
            mAslItems.append(AslItem)
        elif data[1].endswith(']'):
            #
            # When the structure item is an array, check if it needs to be separated into different asl items.
            #
            itemdata = data[4].strip(']').strip('[')
            ArrayCount = int(data[0])
            TotalItemCount = 1
            idx = 1
            ItemLineHandled = 0
            while (TotalItemCount < ArrayCount):
                if LineNum + mCStructLineNumStart + 2 + idx >= mCStructLineNumEnd:
                    print('')
                    print('[ERROR] Line: ' + str(LineNum + mCStructLineNumStart + 2) + ', Array sub item number does not match array size.')
                    return False
                if mStructItems[LineNum+idx][0] == 0:
                    if (mStructItems[LineNum+idx][1].startswith('   ') and
                        mStructItems[LineNum+idx][1].strip(' ').startswith('///< Offset ')):
                        TotalItemCount = TotalItemCount + 1
                else:
                    if not (ArrayCount % TotalItemCount == 0):
                        print('')
                        print('[ERROR] Line: ' + str(LineNum + mCStructLineNumStart + 2) + ', Array sub item number does not match array size.')
                        return False
                    break;
                idx = idx + 1
            SizeOfEachElement = ArrayCount / TotalItemCount
            SubItemName = ''
            SubItemNameIdxStart = 0
            SubItemNameIdxEnd = 0
            idx = 0
            ItemCount = 1
            while (ItemCount < TotalItemCount):
                if mStructItems[LineNum+idx][0] == 0:
                    if (mStructItems[LineNum+idx][1].startswith('   ') and
                        mStructItems[LineNum+idx][1].strip(' ').startswith('///< Offset ')):
                        SubItemNameIdxStart = mStructItems[LineNum+idx][1].find('[') + 1
                        SubItemNameIdxEnd = mStructItems[LineNum+idx][1].find(']')
                        SubItemName = mStructItems[LineNum+idx][1][SubItemNameIdxStart:SubItemNameIdxEnd]
                        ItemCount = ItemCount + 1
                    else:
                        if mStructItems[LineNum+idx][1].startswith('   '):
                            mAslItems.append('{0:28s}{1}'.format(' ', mStructItems[LineNum+idx][1].strip(' ')))
                        elif  mStructItems[LineNum+idx][1].startswith('  '):
                            mAslItems.append(mStructItems[LineNum+idx][1])
                        ItemLineHandled = ItemLineHandled + 1
                        idx = idx + 1
                        continue
                else:
                    SubItemName = mStructItems[LineNum+idx][4]
                AslItem = '  {0:<16s}{1:<6s}{2:<4s}{3:<17s}'.format( 
                              'Offset(' + str(CurrentByteIdx + SizeOfEachElement * (ItemCount - 1)) + '),',
                              SubItemName + ', ', 
                              str(SizeOfEachElement * 8) + ',', 
                              '// Offset(' + str(CurrentByteIdx + SizeOfEachElement * (ItemCount - 1)) + '),')
                if mStructItems[LineNum+idx][0] == 0:
                    AslItem = AslItem + mStructItems[LineNum+idx][1][mStructItems[LineNum+idx][1].find(']') + 4:]
                else:
                    if (len(mStructItems[LineNum+idx]) >= 6):
                        AslItem = AslItem + ' ' + mStructItems[LineNum+idx][5]
                    AslItem = AslItem + '\n'
                mAslItems.append(AslItem)
                ItemLineHandled = ItemLineHandled + 1
                idx = idx + 1
            if TotalItemCount == 1:
                if len(data[4]) > 4:
                    print('')
                    print('[ERROR] Line: ' + str(LineNum + mCStructLineNumStart + 2) + ', Asl variable name length should not be longer than 4.')
                    return False
                AslItem = '  {0:<16s}{1:<6s}{2:<4s}{3:<17s}'.format( 
                              'Offset(' + str(CurrentByteIdx) + '),',
                              data[4] + ', ', 
                              str(data[0] * 8) + ',', 
                              '// Offset(' + str(CurrentByteIdx) + '),')
                if (len(data) >= 6):
                    AslItem = AslItem + ' ' + data[5]
                AslItem = AslItem + '\n'
                mAslItems.append(AslItem)
        else:
            if len(data[4]) > 4:
                print('')
                print('[ERROR] Line: ' + str(LineNum + mCStructLineNumStart + 2) + ', Asl variable name length should not be longer than 4.')
                return False
            AslItem = '  {0:<16s}{1:<6s}{2:<4s}{3:<17s}'.format( 
                          'Offset(' + str(CurrentByteIdx) + '),',
                          data[4] + ', ', 
                          str(data[0] * 8) + ',', 
                          '// Offset(' + str(CurrentByteIdx) + '),')
            if (len(data) >= 6):
                AslItem = AslItem + ' ' + data[5]
            AslItem = AslItem + '\n'
            mAslItems.append(AslItem)

        PreviousIdx = data[0]
        LineNum = LineNum + 1

    return True

def GnvsHdrToAsl (InputHeaderFile, InputAslFile, OutputAslFile):
    #
    # The main funtion for this module.
    #   1. Open the input C header file.
    #   2. Get the C NVS structure data from the file.
    #   3. Transfer C structure to ASL format.
    #   4. Patch the asl file and produce a new one.
    #

    #
    # Check if the input file exists or not.
    #
    if not os.path.exists(InputHeaderFile):
        print ('\"' + InputHeaderFile + '\"' + ' does not exist!')
        sys.exit(-1)
    if not os.path.exists(InputAslFile):
        print ('\"' + InputAslFile + '\"' + ' does not exist!')
        sys.exit(-1)

    #
    # Process those two input files.
    #
    try:
        #
        # Process C header structure data and transfer it to ASL format.
        #
        StructFile = open (InputHeaderFile, 'r')
        if GetGnvsStruct(StructFile) == False or TransferCHeaderStructureToAsl() == False:
            if 'StructFile' in locals():
                StructFile.close()
            return

        #
        # Process ASL data.
        #
        AslFile = open (InputAslFile, 'r')
        PatchAsl(AslFile, OutputAslFile)
    except IOError as err:
        print ('Read fild error:' + str(err))
    finally:
        if 'StructFile' in locals():
            StructFile.close()
        if 'AslFile' in locals():
            AslFile.close()

    print('Transformation Accomplished!\nPlease check the ' + OutputAslFile + ' file.')

if len(sys.argv) < 3:
    print 'IMPORTANT NOTE: This tool follows the latest Intel Skylake RC format.'
    print 'Using this tool on previous or later Intel projects may cause failure.'
    print ''
    print 'This tool transfers Global NVS from C header file to ASL.'
    print 'Before using this tool, please make sure you copied the latest'
    print 'global nvs C header/asl file to this folder.'
    print 'The C header file is the source. The asl file just provides a frame.'
    print 'The tool only updates the data inside the Field(GNVS,..{...}'
    print '[Usage]: GnvsHdrToAsl.exe argv1 argv2 argv3'
    print 'argv1: The C header file of global nvs area.'
    print 'argv2: The asl file of global nvs area to patch.'
    print 'argv3: Use argv1 header to patch argv2 asl NVS area. Argv3 asl file is the output.'
    sys.exit(-1)

GnvsHdrToAsl (sys.argv[1], sys.argv[2], sys.argv[3])
