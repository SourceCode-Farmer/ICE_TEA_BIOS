#;******************************************************************************
#;* Copyright (c) 2012 - 2017, Insyde Software Corp. All Rights Reserved.
#;*
#;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#;* transmit, broadcast, present, recite, release, license or otherwise exploit
#;* any part of this publication in any form, by any means, without the prior
#;* written permission of Insyde Software Corporation.
#;*
#;******************************************************************************
#;
#;  Module Name:
#;
#;    FitInfo.py
#;
#;  Abstract:
#;
#;    Utility to patch Firmware Interface Table to FD for IA32/X64 Architecture
#;

import os
import sys
import struct
import logging
from optparse import OptionParser
from KmBpmClass import *  # @UnusedWildImport
Tool_Version                 = "05.04.10.0020.0319"         # Format: ww.xx.yy.zz.dddd  (ww.xx.yy.zz is bios tag version , dddd is modify date)
FIT_TABLE_SIZE               = 0x10
FIT_VERSION                  = 0x0100
FIT_HEADER_TABLE_TYPE        = 0x00
FIT_MICROCODE_TABLE_TYPE     = 0x01
FIT_ACM_TABLE_TYPE           = 0x02
FIT_TABLE_CHECKSUM_VALID     = 0x80
FIT_SIGNATURE = '_FIT_   '
MAX_FIT_TABLE_TYPE           = 0x70
FV_HEADER_SIZE               = 0x48
FFS_HEADER_SIZE              = 0x18
MICROCODE_TOTAL_SIZE_FIELD   = 8
STRATEGY_MODULE_GUID        = 'ModuleGuid'
STRATEGY_MODULE_MEM_ADDRESS = 'ModuleMemoryAddress'
STRATEGY_HEADER_TABLE       = 'HeaderTable'
STRATEGY_MICROCODE          = 'Microcode'  
uCODE_VERSION_FIELD      = 0
uCODE_UPDATE_REV_FIELD   = 1
uCODE_DATE_FIELD         = 2
uCODE_PID_FIELD          = 3
uCODE_LOADER_REV_FIELD   = 5
KEY_MODULE_OFFSET_IN_FD = 'MODULE_OFFSET_IN_FD'
KEY_MODULE_GUID         = 'KEY_MODULE_GUID'
KEY_MODULE_SIZE         = 'MODULE_SIZE'
KEY_VERSION             = 'VERSION'
KEY_ADDRESS             = 'ADDRESS'
KEY_ADDRESS_AND_BIT0    = 'ADDRESS_AND_BIT0'
KEY_STRATEGY            = 'STRATEGY'
KEY_OFFSET_TO_MODULE_BASE = 'OFFSET_TO_MODULE_BASE'
options = {}
class FitInfo():
    def __init__(self):
        self.EnableFunction  = False
        self.TableCount       = 1
        self.Parser = OptionParser(version = "%prog Tool version "+Tool_Version)
        #{ 0xcc617389, 0x939f, 0x4f53, { 0xbc, 0xd6, 0xb5, 0x5f, 0x97, 0xd4, 0x5c, 0xe1 } }
        self.FitGuid = '\x89\x73\x61\xCC\x9F\x93\x53\x4F\xBC\xD6\xB5\x5F\x97\xd4\x5C\xE1'
        self.FitSize = 0
        self.WorkSpaceDir = os.getenv('WORKSPACE')
        self.OutputDir    = '' 
        self.DscPath      = ''
        self.FdfPath      = ''
        self.OutputDir    = ''
        self.PlatformName = ''
        self.PlatformPkgName = ''
        self.ProjectPkgName  = os.getenv('PROJECT_PKG')
        self.FitVersion = FIT_VERSION
        self.BuildOutputDir = ''
        self.OutputBinFile = ''
        self.FdToBePatched = ''
        self.PatchOffset  = 0
        self.FitAddr    = 0
        self.ConfigFile    = ''
        self.PcdReportFile = ''
        self.FvReportFile  = ''
        self.MicrocodeCount      = 0
        self.MicrocodeMemAddr    = 0
        self.MicrocodeBinFileDir = ''
        self.FlashBaseStr  = ''
        self.FlashBaseTokenSpaceGuid = ''
        self.FlashBasePcd  = ''
        self.FlashBaseAddr = 0
        self.KMPatchOffset  = 0
        self.BPMPatchOffset = 0
        self.BuildType2_B_C = False
        self.IBB_Base = 0
        self.Logger = logging.getLogger("tool_logger")
        LogHandler = logging.StreamHandler()
        self.Logger.addHandler(LogHandler)
        self.Logger.setLevel(logging.WARNING)
        #{TableType:{}}
        self.TableDict = {}
        self.TableDict[0] = {KEY_STRATEGY:STRATEGY_HEADER_TABLE}
    def MyOptionParser(self):
        self.Parser.add_option("-i", "--filename", metavar="FILE", dest="ConfigFile", help="input configuration file")
        self.Parser.add_option("-b", "--btype2bc", action="store_true", dest="BuildType2_B_C", default=False, help="flag to build type 2, B, C [default: %default]")
        (Opt, args) = self.Parser.parse_args()  # @UnusedVariable
        self.BuildType2_B_C = Opt.BuildType2_B_C
        if Opt.ConfigFile:
            self.ConfigFile = Opt.ConfigFile
        else:
            self.Parser.error('Please specify Configuration File with -i or --filename argument')
    def checksum8(self, Bytes):
        checksum = 0
        for i in range(0, len(Bytes)):
            checksum += ord(Bytes[i])
        checksum &= 0xFF
        checksum  = 0x100 - checksum
        return checksum
    def UpdateHeaderTable(self, FitBinaryName):
        FitTable = struct.Struct('<QLH2B')
        FitBinaryFile = open(FitBinaryName, "r+b")
        #
        # Read Type 0 Table
        #
        FitBinaryFile.seek(0)
        HeaderTable = FitBinaryFile.read(FitTable.size)
        TableType = FitTable.unpack(HeaderTable)[3]
        if not TableType == FIT_HEADER_TABLE_TYPE and not TableType == FIT_HEADER_TABLE_TYPE | FIT_TABLE_CHECKSUM_VALID:
            print('\nERROR: First Table must be type 0\n')
            sys.exit(-1)
        # Update Table Size
        FitBinaryFile.seek(0x8)
        FitBinaryFile.write(struct.pack('<L', self.TableCount & 0xFFFFFF))
        # Update checksum
        FitBinaryFile.seek(0)
        FitBinary = FitBinaryFile.read()
        checksum = self.checksum8(FitBinary)
        FitBinaryFile.seek(0)
        FitBinaryFile.seek(0xf)
        FitBinaryFile.write(struct.pack('<B', checksum))
        FitBinaryFile.close()
    def ParseDefineSection(self, lines):
        for line in lines:
            line = line.strip()
            # Parse Context Only Beside This Section 
            if line.startswith('['):
                break
            line = line.replace('$(PLATFORM_PKG)', self.PlatformPkgName)
            line = line.replace('$(PROJECT_PKG)', self.ProjectPkgName)
            line = line.replace('$(OUTPUT_DIRECTORY)', self.OutputDir)
            line = line.replace('$(TARGET)', self.Target)
            line = line.replace('$(TOOL_CHAIN_TAG)', self.ToolChainTag)
            if ('=' in line) and line.split('=')[1].strip() != '':
                if line.split('=')[0].strip() == 'PATCH_FIT_ENABLE':
                    self.EnableFunction = True if line.split('=')[1].strip() == 'YES' else False
                elif line.split('=')[0].strip() == 'FIT_VERSION':
                    self.FitVersion  =  int(line.split('=')[1].strip(), 16)
                elif line.split('=')[0].strip() == 'FD_TO_BE_PATCHED':
                    self.FdToBePatched = self.WorkSpaceDir + '/' + line.split('=')[1].strip()
                elif line.split('=')[0].strip() == 'OUTPUT_BIN_FILE':
                    self.OutputBinFile = self.WorkSpaceDir + '/' + line.split('=')[1].strip()
                elif line.split('=')[0].strip() == 'FV_REPORT_FILE':
                    self.FvReportFile = self.WorkSpaceDir + '/' + line.split('=')[1].strip() 
                elif line.split('=')[0].strip() == 'PCD_REPORT_FILE':
                    self.PcdReportFile = self.WorkSpaceDir + '/' + line.split('=')[1].strip()
                elif line.split('=')[0].strip() == 'FLASH_BASEADDRESS':
                    self.FlashBaseStr = line.split('=')[1].strip()
                    if '.' in self.FlashBaseStr and not self.FlashBaseStr.endswith('.'):
                        self.FlashBaseTokenSpaceGuid = self.FlashBaseStr.split('.')[0].strip()
                        self.FlashBasePcd = self.FlashBaseStr.split('.')[1].strip()
                    else:
                        self.FlashBaseAddr = int(self.FlashBaseStr, 16)
                elif line.split('=')[0].strip() == 'DEBUG_MODE':
                    if line.split('=')[1].strip() == 'YES':
                        self.Logger.setLevel(logging.INFO)
                    else:
                        self.Logger.setLevel(logging.WARNING)
    def ParseTableSection(self, lines):
        IsSectionTableTypeSet = False
        IsModuleGuidSet       = False
        IsModuleMemAddrSet = False
        #{TableType:{}}
        d = {}
        DefineDict = {}
        for line in lines:
            line = line.strip()
            # Parse Context Only Beside This Section 
            if line.startswith('['):
                if IsSectionTableTypeSet and (IsModuleGuidSet or IsModuleMemAddrSet):
                    #d[TableType] = ModuleGuid if ModuleGuid != '' else ModuleOffsetInFd
                    d[TableType] = DefineDict  # @UndefinedVariable
                    return d
                else:
                    self.Parser.error('Does not set needed Table Info')
            if ('=' in line) and line.split('=')[1].strip() != '':
                if line.split('=')[0].strip() == 'TABLE_TYPE':
                    TableType = int(line.split('=')[1].strip(), 16)
                    IsSectionTableTypeSet = True
                elif line.split('=')[0].strip() == 'MODULE_GUID':
                    DefineDict[KEY_MODULE_GUID] = line.split('=')[1].strip()
                    DefineDict[KEY_STRATEGY] = STRATEGY_MODULE_GUID
                    IsModuleGuidSet = True
                elif line.split('=')[0].strip() == 'ADDRESS':
                    DefineDict[KEY_ADDRESS] = line.split('=')[1].strip()
                    DefineDict[KEY_STRATEGY] = STRATEGY_MODULE_MEM_ADDRESS
                    IsModuleMemAddrSet = True
                elif line.split('=')[0].strip() == 'VERSION':
                    DefineDict[KEY_VERSION] = line.split('=')[1].strip()
                elif line.split('=')[0].strip() == 'MODULE_SIZE':
                    DefineDict[KEY_MODULE_SIZE] = line.split('=')[1].strip()
                elif line.split('=')[0].strip() == 'ADDRESS_AND_BIT0':
                    DefineDict[KEY_ADDRESS_AND_BIT0] = line.split('=')[1].strip()
                elif line.split('=')[0].strip() == 'OFFSET_TO_MODULE_BASE':
                    DefineDict[KEY_OFFSET_TO_MODULE_BASE] = line.split('=')[1].strip()
        if IsSectionTableTypeSet and (IsModuleGuidSet or IsModuleMemAddrSet):
            d[TableType]= DefineDict
            return d
        else:
            self.Parser.error('Does not set needed Table Info')
    def ParsePcdReportFile(self, PcdReportFile, TokenSpaceGuid, Pcd):
        Lines = enumerate(open(PcdReportFile))
        for Index, Line in Lines:  # @UnusedVariable
            if Line.rstrip() == TokenSpaceGuid.strip():
                break
        for Index, Line in Lines:  # @UnusedVariable
            if Pcd in Line:
                ValueToken = Line.split('=')[1].strip()
                if ValueToken.lower().startswith('0x'):
                    return int(ValueToken, 16)
                elif ValueToken.isdigit():
                    return int(ValueToken)
                elif ValueToken.startswith('{'):
                    while not ValueToken.endswith('}'):
                        ValueToken += Lines.next()[1].strip()
                return ValueToken

        print(''.join(['\n',TokenSpaceGuid, '.', Pcd, ' is not found in report file\n']))
        sys.exit(-1)

    def GetConfiguration(self):
        
        #
        # Get setting from Conf/Target.txt and environment variable 
        #
        Target = ''
        ToolChainTag = ''

        TargetTxtName = self.WorkSpaceDir + '/' + 'Conf/target.txt'
        TargetTxt = open(TargetTxtName)
        for line in TargetTxt.readlines():
            line = line.strip()
            if line.startswith('TARGET '):
                self.Target = line.split('=')[1].strip()
            elif line.startswith('TOOL_CHAIN_TAG'):
                self.ToolChainTag = line.split('=')[1].strip()
            elif line.startswith('ACTIVE_PLATFORM'):
                self.DscPath = line.split('=')[1].strip()
                self.DscPath = self.DscPath.replace('\\', '/')
                self.PlatformPkgName = self.DscPath.split('/')[len(self.DscPath.split('/')) - 2]
                self.PlatformName = self.PlatformPkgName[0:self.PlatformPkgName.rfind('Pkg')]
                self.DscPath = self.WorkSpaceDir + '/' + self.DscPath
            if Target != '' and ToolChainTag != '' and self.DscPath !='':
                break;
        TargetTxt.close()

        PlatformDsc = open(self.DscPath)
        for line in PlatformDsc.readlines():
            line = line.strip()
            if line.startswith('OUTPUT_DIRECTORY'):
                self.OutputDir = line.split('=')[1].strip().replace('$(PLATFORM_PKG)', self.PlatformPkgName)
                self.OutputDir = line.split('=')[1].strip().replace('$(PROJECT_PKG)', self.ProjectPkgName)
            elif line.startswith('FLASH_DEFINITION'):
                self.FdfPath = self.WorkSpaceDir + '/' + line.split('=')[1].strip().replace(\
                     '$(PLATFORM_PKG)', self.PlatformPkgName)
                self.FdfPath = self.WorkSpaceDir + '/' + line.split('=')[1].strip().replace(\
                     '$(PROJECT_PKG)', self.ProjectPkgName)
            if self.FdfPath != '' and self.OutputDir != '':
                break;
        PlatformDsc.close()
        if self.OutputDir.find(':') > -1:
            self.BuildOutputDir = self.OutputDir + '/' + Target + '_' + ToolChainTag
        else:
            self.BuildOutputDir = self.WorkSpaceDir + '/' + self.OutputDir + '/' + Target + '_' + ToolChainTag

        PlatformFdf = open(self.FdfPath)
        for line in PlatformFdf.readlines():
            if line.find('PcdFitTableReservedFile') != -1:
                self.FitAddr = line.split('=')[2].strip()
                self.FitAddr = self.FitAddr.split('{')[0].strip()
                self.FitAddr = int(self.FitAddr, 16)
                break;
        PlatformFdf.close()
        if self.FitAddr == 0:
            self.Parser.error('can not find PcdFitTableReservedFile')
        #
        # Parse Config file
        #
        self.ConfigFile = self.WorkSpaceDir + '/' + self.ConfigFile
        self.ConfigFile = self.ConfigFile.strip().replace('$(PLATFORM_PKG)', self.PlatformPkgName)
        self.ConfigFile = self.ConfigFile.strip().replace('$(PROJECT_PKG)', self.ProjectPkgName)
        ConfigFile = open(self.ConfigFile)

        Index = 0
        configLines = ConfigFile.readlines()
        for line in configLines:
            line = line.strip()

            # [DEFINE]
            if line.upper().startswith('[DEFINE]'):
                defineLines = configLines[Index + 1:]
                self.ParseDefineSection(defineLines)

            # [Table] or [Table.ModuleName]
            if line.startswith('[') and line.endswith(']') and 'Table' in line.split('[')[1].split(']')[0] :
                defineLines = configLines[Index + 1:]
                self.TableDict.update(self.ParseTableSection(defineLines))
            Index += 1

        self.ParseConfigFileHook()
        
        #
        # Check Needed Info is Valid
        #
        if self.EnableFunction and (self.FdToBePatched == ''  or self.OutputBinFile == ''\
                or self.FvReportFile == '' or self.PcdReportFile == '' or self.FlashBaseTokenSpaceGuid ==''\
                or self.FlashBasePcd == ''):
            self.Parser.error('Please assign suitable value in PatchFitConfig.ini')
        
        #
        # Parse Pcd Report File
        #
        self.FlashBaseAddr = self.ParsePcdReportFile(self.PcdReportFile, self.FlashBaseTokenSpaceGuid, self.FlashBasePcd)
        #
        # Check Needed Info is Valid
        #
        if self.EnableFunction and (self.FlashBaseAddr == 0):
            self.Parser.error('Fail to get Pcd on %s' % self.PcdReportFile)

        #
        # Debug Message
        #
        self.Logger.info('\n')
        self.Logger.info('WorkSpaceDir:            %s' % self.WorkSpaceDir)
        self.Logger.info('PcdReportFile:           %s' % self.PcdReportFile)
        self.Logger.info('FlashBaseTokenSpaceGuid: %s' % self.FlashBaseTokenSpaceGuid)
        self.Logger.info('FlashBasePcd:            %s' % self.FlashBasePcd)
        self.Logger.info('FlashBaseAddr:           %x' % self.FlashBaseAddr)
        self.Logger.info('Retrieved Table Count:   %d' % len(self.TableDict))
        self.Logger.info(self.TableDict)
        self.Logger.info('\n')

    def ParseConfigFileHook(self):
        if (self.TableDict.get(FIT_MICROCODE_TABLE_TYPE) != None) and (self.TableDict.get(FIT_MICROCODE_TABLE_TYPE).get(KEY_STRATEGY) != None):
            self.TableDict[FIT_MICROCODE_TABLE_TYPE][KEY_STRATEGY] = STRATEGY_MICROCODE

    def GenTableWorker(self, Buffer, Address, Size, Version, TableType, Checksum):

        if TableType == 2:
            if not self.BuildType2_B_C:                 # Skip to build type 2
                return Buffer
            self.IBB_Base = Address + Size

        Buffer += struct.pack('<Q', Address)         # address
        Buffer += struct.pack('<L', Size & 0xFFFFFF) # component size[3] and reserved[1] 
        Buffer += struct.pack('<H', Version)         # version    
        Buffer += struct.pack('<B', TableType)       # chechsum_valid, type
        Buffer += struct.pack('<B', Checksum)        # checksum

        self.TableCount += 1

        return Buffer


    def GenHeaderTable(self):

        checksum = 0
        TableType = FIT_HEADER_TABLE_TYPE
        TableType |= FIT_TABLE_CHECKSUM_VALID

        HeaderTable = '_FIT_   '.encode('utf8')                      # address/signature
        HeaderTable += struct.pack('<L', self.TableCount & 0xFFFFFF) # component size & reserve field
        HeaderTable += struct.pack('<H', self.FitVersion)            # version
        HeaderTable += struct.pack('<B', TableType)                  # chechsum_valid, type
        HeaderTable += struct.pack('<B', checksum)                   # checksum

        return HeaderTable

    def GenKMTable(self, Bytes):
        KMBinFileNamePath = self.OutputBinFile.replace('FirmwareInterfaceTable.bin', KMBinFileName)
        KMBinFile = open(KMBinFileNamePath, 'w+b')
        for i in range(0, KMTableCount):
            #
            # Build KM Table
            #
            KMS = KEY_MANIFEST_STRAUCTURE()
            KMS.KMS_StructureID         = KEY_MANIFEST_STRUCTURE_ID.encode('utf8')
            KMS.KMS_StructVersion       = KEY_MANIFEST_VERSION
            KMS.KMS_KeyManifestVersion  = 0x10
            KMS.KMS_KMSVN               = 0x00
            KMS.KMS_KeyManifestID       = 0x01
            KMS.KMS_BPKey.HS_HashAlg    = ALG_SHA256
            KMS.KMS_BPKey.HS_Size       = SHA256_DIGEST_SIZE
            KMS.KMS_KeyManifestSignature.KSS_Version               = KEY_SIGNATURE_STRUCT_VERSION
            KMS.KMS_KeyManifestSignature.KSS_KeyAlg                = ALG_RSA
            KMS.KMS_KeyManifestSignature.KSS_key.RPKS_Version      = RSA_PUBLIC_KEY_STRUCT_VERSION
            KMS.KMS_KeyManifestSignature.KSS_key.RPKS_KeySize      = RSA_PUBLIC_KEY_STRUCT_KEY_SIZE_DEFAULT
            KMS.KMS_KeyManifestSignature.KSS_key.RPKS_Exponent     = RSA_PUBLIC_KEY_STRUCT_KEY_EXPONENT_DEFAULT
            KMS.KMS_KeyManifestSignature.KSS_SigScheme             = ALG_RSASSA
            KMS.KMS_KeyManifestSignature.KSS_Signature.RSS_Version = RSASSA_SIGNATURE_STRUCT_VERSION
            KMS.KMS_KeyManifestSignature.KSS_Signature.RSS_KeySize = RSASSA_SIGNATURE_STRUCT_KEY_SIZE_DEFAULT
            KMS.KMS_KeyManifestSignature.KSS_Signature.RSS_HashAlg = ALG_SHA256
            KMBinFile.write(KMS)

            #
            # Update Fit table
            #
            KMTableAddress.append(self.FitAddr + 0x400 * (i+1))
            Bytes = self.GenTableWorker(Bytes, KMTableAddress[i], KMTableSize, self.FitVersion, 0x0B, 0)

        KMBinFile.close()
        return Bytes

    def GenBPMTable(self, Bytes):
        BPMBinFileNamePath = self.OutputBinFile.replace('FirmwareInterfaceTable.bin', BPMBinFileName)
        BPMBinFile = open(BPMBinFileNamePath, 'w+b')
        #
        # BOOT_POLICY_MANIFEST_HEADER
        #
        BPMC = BOOT_POLICY_MANIFEST_COMPLEX()
        BPMC.BPMH.BPMH_StructureID      = BOOT_POLICY_MANIFEST_HEADER_STRUCTURE_ID.encode('utf8')
        BPMC.BPMH.BPMH_StructVersion    = BOOT_POLICY_MANIFEST_STRUCTURE_VERSION
        BPMC.BPMH.BPMH_HdrStructVersion = BOOT_POLICY_MANIFEST_HEADER_STRUCTURE_VERSION
        BPMC.BPMH.BPMH_PMBPMVersion     = 0x10
        BPMC.BPMH.BPMH_BPSVN            = 0x00
        BPMC.BPMH.BPMH_ACMSVN           = 0x00
        BPMC.BPMH.BPMH_NEMDataStack     = 0x40    # value of 1 = 4096, 2 = 8092, synchronize with gChipsetPkgTokenSpaceGuid.PcdNemDataStackSize
        #
        # IBB_ELEMENT
        #
        BPMC.IES.IES_StructureID        = BOOT_POLICY_MANIFEST_IBB_ELEMENT_STRUCTURE_ID.encode('utf8')
        BPMC.IES.IES_StructVersion      = BOOT_POLICY_MANIFEST_IBB_ELEMENT_VERSION
        BPMC.IES.IES_PBETValue          = 0x00
        BPMC.IES.IES_Flags              = 0x00
        BPMC.IES.IES_IBB_MCHBAR         = 0x00
        BPMC.IES.IES_VTD_BAR            = 0x00
        BPMC.IES.IES_PMRL_Base          = 0x00
        BPMC.IES.IES_PMRL_Limit         = 0x00
        BPMC.IES.IES_Reserved1[0]       = 0x00
        BPMC.IES.IES_Reserved1[1]       = 0x00
        BPMC.IES.IES_PostIBBHash.HS_HashAlg = ALG_SHA256
        BPMC.IES.IES_PostIBBHash.HS_Size    = SHA256_DIGEST_SIZE
        BPMC.IES.IES_EntryPoint         = 0xFFFFFFF0
        BPMC.IES.IES_Digest.HS_HashAlg  = ALG_SHA256
        BPMC.IES.IES_Digest.HS_Size     = SHA256_DIGEST_SIZE
        BPMC.IES.IES_SegmentCount       = 0x01
        BPMC.IES.IES_IBBSegment.ISS_Reserved      = 0x0000
        BPMC.IES.IES_IBBSegment.ISS_Flags         = IBB_SEGMENT_FLAG_IBB
        BPMC.IES.IES_IBBSegment.ISS_Base          = self.IBB_Base
        BPMC.IES.IES_IBBSegment.ISS_Size          = 0x100000000 - BPMC.IES.IES_IBBSegment.ISS_Base
        #
        # PLATFORM_MANUFACTURER_ELEMENT PlatformDataCount = 0
        #
        #  BPMC.PME.PME_StructureID = BOOT_POLICY_MANIFEST_PLATFORM_MANUFACTURER_ELEMENT_STRUCTURE_ID.encode('utf8')
        #  BPMC.PME.PME_StructVersion = BOOT_POLICY_MANIFEST_PLATFORM_MANUFACTURER_ELEMENT_VERSION
        #  BPMC.PME.PME_PMDataSize = BOOT_POLICY_MANIFEST_PLATFORM_MANUFACTURER_ELEMENT_PMDatasize

        #
        # BOOT_POLICY_MANIFEST_SIGNATURE_ELEMENT
        #
        BPMC.BPMSE.BPMSE_StructureID                        = BOOT_POLICY_MANIFEST_SIGNATURE_ELEMENT_STRUCTURE_ID.encode('utf8')
        BPMC.BPMSE.BPMSE_StructVersion                      = BOOT_POLICY_MANIFEST_SIGNATURE_ELEMENT_VERSION
        BPMC.BPMSE.BPMSE_KeySignature.KSS_Version           = KEY_SIGNATURE_STRUCT_VERSION
        BPMC.BPMSE.BPMSE_KeySignature.KSS_KeyAlg            = ALG_RSA
        BPMC.BPMSE.BPMSE_KeySignature.KSS_key.RPKS_Version  = RSA_PUBLIC_KEY_STRUCT_VERSION
        BPMC.BPMSE.BPMSE_KeySignature.KSS_key.RPKS_KeySize  = RSA_PUBLIC_KEY_STRUCT_KEY_SIZE_DEFAULT
        BPMC.BPMSE.BPMSE_KeySignature.KSS_key.RPKS_Exponent = RSA_PUBLIC_KEY_STRUCT_KEY_EXPONENT_DEFAULT
        BPMC.BPMSE.BPMSE_KeySignature.KSS_SigScheme         = ALG_RSASSA
        BPMC.BPMSE.BPMSE_KeySignature.KSS_Signature.RSS_Version = RSASSA_SIGNATURE_STRUCT_VERSION
        BPMC.BPMSE.BPMSE_KeySignature.KSS_Signature.RSS_KeySize = RSASSA_SIGNATURE_STRUCT_KEY_SIZE_DEFAULT
        BPMC.BPMSE.BPMSE_KeySignature.KSS_Signature.RSS_HashAlg = ALG_SHA256
        BPMBinFile.write(BPMC)
        #
        # Update Fit table
        #
        BPMTableAddress.append(KMTableAddress[-1] + 0x400)
        Bytes = self.GenTableWorker(Bytes, BPMTableAddress[0], BPMTableSize, self.FitVersion, 0x0C, 0)

        BPMBinFile.close()
        return Bytes

    def GenMicrocodeTable(self, Bytes, TableType, ModuleOffsetInFdStr):
        if '.' in ModuleOffsetInFdStr:
            TokenSpaceGuid = ModuleOffsetInFdStr.split('.')[0].strip()
            Pcd            = ModuleOffsetInFdStr.split('.')[1].strip()
        else:
            print('Please assign TokenSpaceGuid.Pcd of Base Address of Microcode Module to calculate Module offset in fd')
            sys.exit(-1)

        self.MicrocodeMemAddr = self.ParsePcdReportFile(self.PcdReportFile, TokenSpaceGuid, Pcd) + FV_HEADER_SIZE + FFS_HEADER_SIZE
        self.Logger.info('MicrocodeMemAddr:    %x' % self.MicrocodeMemAddr)

        FdFile = open(self.FdToBePatched, 'rb')
        MicrocodeHeader = struct.Struct('<9I12B')
        size = 0
        ModuleSize = 0
        NextOffset = 0
        sizeList = []
        checksum = 0
        version = self.FitVersion

        ModuleOffsetInFd = self.MicrocodeMemAddr - self.FlashBaseAddr
        FdFile.seek(ModuleOffsetInFd)

        try:
            while True:
                Header =FdFile.read(MicrocodeHeader.size)

                if MicrocodeHeader.unpack(Header)[uCODE_VERSION_FIELD]      == 0xFFFFFFFF or \
                    MicrocodeHeader.unpack(Header)[uCODE_UPDATE_REV_FIELD]  == 0xFFFFFFFF or \
                    MicrocodeHeader.unpack(Header)[uCODE_DATE_FIELD]        == 0xFFFFFFFF or \
                    MicrocodeHeader.unpack(Header)[uCODE_PID_FIELD]         == 0xFFFFFFFF or \
                    MicrocodeHeader.unpack(Header)[uCODE_LOADER_REV_FIELD]  == 0xFFFFFFFF :
                    FdFile.close()
                    break
                size =  MicrocodeHeader.unpack(Header)[8]
#                #
#                #  Adjust the size / next microcode offset to be 2k aligned.
#                #
#                if size & 0x7FF:
#                    size = ((size >> 11) + 1) << 11
                sizeList.append(size)
                NextOffset += size
                self.MicrocodeCount += 1
                FdFile.seek(0)
                FdFile.seek(ModuleOffsetInFd + NextOffset)

        except EOFError:
            FdFile.close()
            pass

        for i in range(0, self.MicrocodeCount):
            #
            # Retrieve Customized Table Data
            #
            ModuleSize = sizeList[i]
            if self.TableDict[TableType].get(KEY_MODULE_SIZE) == 'PRESENTED_IN_16_BYTES_MULTIPLE':
                ModuleSize = sizeList[i] /0x10
            elif self.TableDict[TableType].get(KEY_MODULE_SIZE) != None :
                ModuleSize = int(self.TableDict[TableType][KEY_MODULE_SIZE], 16)
                
            if self.TableDict[TableType].get(KEY_VERSION) != None :
                version = int(self.TableDict[TableType][KEY_VERSION], 16)
            Bytes = self.GenTableWorker(Bytes, self.MicrocodeMemAddr,  ModuleSize, version, TableType, checksum)
            self.MicrocodeMemAddr += sizeList[i]
        FdFile.close()
        return Bytes

    #
    #   Gen tables by retrieving module guid
    #
    def GenModuleGuidTable(self, Bytes, TableType, ModuleGuid):

        self.Logger.info('GenModuleGuidTable: %x' % TableType)

        FvReportFile = open(self.FvReportFile, 'r')
        ReportLines = FvReportFile.readlines()
        FvReportFile.close()

        ModuleSize = ''
        version = self.FitVersion
        checksum = 0
        AddressOffset = 0
        FoundModule = False

        #
        # Get Ffs File Size from FvReport File
        # 
        for line in ReportLines:
            if ModuleGuid.upper() in line.upper():
                # Retrieve Module Size from FV report file
                #
                #  OFFSET              FILE GUID                   START   -   END            SIZE       ALIGN    ModuleType          FILE NAME
                #========== ==================================== ========== ========== ================= ===== ================ ========================
                ModuleSize = line.split()[3]

                # remove 'h' in tail of string
                if ModuleSize.strip().endswith('h'):
                    ModuleSize = ModuleSize.strip()[:-1]

                FoundModule = True
                break
        if not FoundModule:
            print("\nERROR:  can't' found Module in FV\n")
            sys.exit(-1)
            return

        ModuleSize = int(ModuleSize, 16)

        #
        # Retrieve Customized Table Data
        #
        if self.TableDict[TableType].get(KEY_MODULE_SIZE) == 'PRESENTED_IN_16_BYTES_MULTIPLE':
            ModuleSize /= 0x10
        elif self.TableDict[TableType].get(KEY_MODULE_SIZE) != None :
            ModuleSize = int(self.TableDict[TableType][KEY_MODULE_SIZE], 16)
        if self.TableDict[TableType].get(KEY_VERSION) != None :
            version = int(self.TableDict[TableType][KEY_VERSION], 16)

        # Get address from report file specific field
        StartEndAddress = line.split()[2]
        Address = int(StartEndAddress.split('-' )[0], 16)

        if self.TableDict[TableType].get(KEY_OFFSET_TO_MODULE_BASE) != None :
            AddressOffset = int(self.TableDict[TableType][KEY_OFFSET_TO_MODULE_BASE], 16)

            if Address > AddressOffset:
                Address += AddressOffset
                self.Logger.info('Address: %x' % Address)
            else:
                Address = 0

            if ModuleSize > AddressOffset:
                self.Logger.info('ModuleSize: %x' % ModuleSize)
            else:
                ModuleSize = 0

        if self.TableDict[TableType].get(KEY_ADDRESS_AND_BIT0) != None :
            AndBit = int(self.TableDict[TableType][KEY_ADDRESS_AND_BIT0], 16)
            AndBit &= 0x1
            Address = Address >> 1
            Address = Address << 1
            if AndBit:
                Address |= 1

        Bytes = self.GenTableWorker(Bytes, Address, ModuleSize, version, TableType, checksum)

        return Bytes

    ###
    #   Gen tables by memory address specified in INI file
    #            
    def GenModuleMemAddressTable(self, Bytes, TableType, ModuleMemAddressStr):

        version = self.FitVersion
        checksum = 0
        ModuleSize = 0

        if '.' in ModuleMemAddressStr:
            TokenSpaceGuid = ModuleMemAddressStr.split('.')[0].strip()
            Pcd            = ModuleMemAddressStr.split('.')[1].strip()
            ModuleMemAddr  = self.ParsePcdReportFile(self.PcdReportFile, TokenSpaceGuid, Pcd)
        else:
            ModuleMemAddr = int(ModuleMemAddressStr, 16)

        self.Logger.info('TableType:     %x' % TableType )
        self.Logger.info('ModuleMemAddr: %x' % ModuleMemAddr)
        #
        # Retrieve Customized Table Data
        #
        if self.TableDict[TableType].get(KEY_MODULE_SIZE) == 'PRESENTED_IN_16_BYTES_MULTIPLE':
            ModuleSize /= 0x10
        elif self.TableDict[TableType].get(KEY_MODULE_SIZE) != None :
            ModuleSize = int(self.TableDict[TableType][KEY_MODULE_SIZE], 16)

        if self.TableDict[TableType].get(KEY_VERSION) != None :
            version = int(self.TableDict[TableType][KEY_VERSION], 16)

        if self.TableDict[TableType].get(KEY_OFFSET_TO_MODULE_BASE) != None :
            AddressOffset = int(self.TableDict[TableType][KEY_OFFSET_TO_MODULE_BASE], 16)

            if ModuleMemAddr > AddressOffset:
                ModuleMemAddr += AddressOffset
                self.Logger.info('ModuleMemAddr: %x' % ModuleMemAddr)
            else:
                ModuleMemAddr = 0

            if ModuleSize > AddressOffset:
                ModuleSize -= AddressOffset
                self.Logger.info('ModuleSize: %x' % ModuleSize)
            else:
                ModuleSize = 0

        if self.TableDict[TableType].get(KEY_ADDRESS_AND_BIT0) != None :
            AndBit = int(self.TableDict[TableType][KEY_ADDRESS_AND_BIT0], 16)
            AndBit &= 0x1
            ModuleMemAddr = ModuleMemAddr >> 1
            ModuleMemAddr = ModuleMemAddr << 1
            if AndBit:
                ModuleMemAddr |= 1

        Bytes = self.GenTableWorker(Bytes, ModuleMemAddr,  ModuleSize, version, TableType, checksum)
        return Bytes


    def GenTables(self, Bytes):
        for i in range(MAX_FIT_TABLE_TYPE + 1):
            if self.TableDict.get(i) != None :
                if i == 2:
                    if not self.BuildType2_B_C:
                        return Bytes
                Bytes = self._GenTables(Bytes, i, self.TableDict[i].get(KEY_STRATEGY))

        return Bytes


    def _GenTables(self, Bytes, TableType, Strategy):
        if Strategy == STRATEGY_MODULE_MEM_ADDRESS:
            Bytes = self.GenModuleMemAddressTable(Bytes, TableType, self.TableDict[TableType][KEY_ADDRESS])
            return Bytes
        elif Strategy == STRATEGY_MODULE_GUID:
            Bytes = self.GenModuleGuidTable(Bytes, TableType, self.TableDict[TableType][KEY_MODULE_GUID])
            return Bytes
        elif Strategy == STRATEGY_MICROCODE:
            Bytes = self.GenMicrocodeTable(Bytes, TableType, self.TableDict[TableType][KEY_ADDRESS])
            return Bytes
        else:
            return Bytes


    def GenTablesWrapper(self):
        Tables = self.GenHeaderTable()
        Tables = self.GenTables(Tables)
        if self.BuildType2_B_C:
            Tables = self.GenKMTable(Tables)
            Tables = self.GenBPMTable(Tables)

        self.Logger.info('Gen TableCount: %d' % self.TableCount)

        return Tables

    def PreGen(self):
        if not self.EnableFunction:
            print('FitInfo: Function not enabled')
            sys.exit(0)


    def Gen(self):
        FitContent = self.GenTablesWrapper()
        Fit = open(self.OutputBinFile, 'wb')
        Fit.write(FitContent)
        Fit.close()

    def PatchKMTable(self):
        KMBinFileNamePath = self.OutputBinFile.replace('FirmwareInterfaceTable.bin', KMBinFileName)
        KMFile = open(KMBinFileNamePath, 'rb') 
        FdFile = open(self.FdToBePatched, 'r+b')
        FdFile.read()
        for i in range(0, KMTableCount):
            KMFile.seek(KMTableSize * i)
            KMBinary = KMFile.read(KMTableSize)
            self.KMPatchOffset = KMTableAddress[i] - self.FlashBaseAddr

            FdFile.seek(self.KMPatchOffset)
            FdFile.write(KMBinary)

        FdFile.close()
        KMFile.close()

        self.Logger.info('KMTableAddr:     %x' % KMTableAddress[i] )
        self.Logger.info('KMTablePatchOffset: %x' % self.KMPatchOffset )

    def PatchBPMTable(self):
        BPMBinFileNamePath = self.OutputBinFile.replace('FirmwareInterfaceTable.bin', BPMBinFileName)
        BPMFile = open(BPMBinFileNamePath, 'rb')
        FdFile = open(self.FdToBePatched, 'r+b')
        FdFile.read()

        BPMFile.seek(0)
        BPMBinary = BPMFile.read()
        self.BPMPatchOffset = BPMTableAddress[0] - self.FlashBaseAddr

        FdFile.seek(self.BPMPatchOffset)
        FdFile.write(BPMBinary)

        FdFile.close()
        BPMFile.close()

        self.Logger.info('BPMTableAddr:     %x' % BPMTableAddress[0] )
        self.Logger.info('BPMTablePatchOffset: %x' % self.BPMPatchOffset )

    def PatchFd(self):
        FitFile = open(self.OutputBinFile, 'rb')

        FitFile.seek(0)
        FitBinary = FitFile.read()

        FdFile = open(self.FdToBePatched, 'r+b')
        self.PatchOffset = self.FitAddr - self.FlashBaseAddr
        FdFile.seek(self.PatchOffset)
        FdFile.write(FitBinary)

        # Patch FIT Pointer (8 bytes, but upper 32 bits must be zero) for Ia32/X64 Architecture
        if self.FitAddr >  (0xFFFFFFFF - 0x40):
            print('Fit must reside with 4G Memory Space')
            sys.exit(-1)
        FdFile.seek(-0x40, os.SEEK_END)
        FdFile.write(struct.pack('<Q', self.FitAddr))

        FdFile.close()
        FitFile.close()

        self.Logger.info('FitAddr:     %x' % self.FitAddr )
        self.Logger.info('PatchOffset: %x' % self.PatchOffset )
        if self.BuildType2_B_C:
            self.PatchKMTable()
            self.PatchBPMTable()

    def main(self):
        self.MyOptionParser()
        self.GetConfiguration()
        self.PreGen()
        self.Gen()
        self.UpdateHeaderTable(self.OutputBinFile)
        self.PatchFd()


if __name__ == '__main__':
    Fit = FitInfo()
    Fit.main()
