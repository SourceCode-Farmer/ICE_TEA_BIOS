#;******************************************************************************
#;* Copyright (c) 1983-2017, Insyde Software Corporation. All Rights Reserved.
#;*
#;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#;* transmit, broadcast, present, recite, release, license or otherwise exploit
#;* any part of this publication in any form, by any means, without the prior
#;* written permission of Insyde Software Corporation.
#;*
#;******************************************************************************
#;
#;  Module Name:
#;
#;    KmBpmClass.py
#;
#;  Abstract:
#;
#;    Utility to patch Firmware Interface Table to FD for IA32/X64 Architecture
#;

from ctypes import *  # @UnusedWildImport

RSA_PUBLIC_KEY_STRUCT_KEY_SIZE_DEFAULT            = 2048
RSA_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT             = RSA_PUBLIC_KEY_STRUCT_KEY_SIZE_DEFAULT/8
RSA_PUBLIC_KEY_STRUCT_VERSION                     = 0x10
RSA_PUBLIC_KEY_STRUCT_KEY_EXPONENT_DEFAULT        = 0x10001
RSASSA_SIGNATURE_STRUCT_KEY_SIZE_DEFAULT          = 2048
RSASSA_SIGNATURE_STRUCT_KEY_LEN_DEFAULT           = RSASSA_SIGNATURE_STRUCT_KEY_SIZE_DEFAULT/8
RSASSA_SIGNATURE_STRUCT_VERSION                   = 0x10
ALG_RSA                                           = 0x01
ALG_RSASSA                                        = 0x14
ALG_SHA256                                        = 0x0B
SHA256_DIGEST_SIZE                                = 0x20

KEY_MANIFEST_STRUCTURE_ID                         = "__KEYM__"
KEY_MANIFEST_VERSION                              = 0x10
KEY_SIGNATURE_STRUCT_VERSION                      = 0x10

KMTableAddress = [ ]
KMTableCount = 1
KMBinFileName = 'KM.bin'

class HASH_STRUCTURE(Structure):
    _pack_ = 1
    _fields_ = [
        ('HS_HashAlg',                  c_ushort),
        ('HS_Size',                     c_ushort),
        ('HS_HashBuffer',               c_ubyte * SHA256_DIGEST_SIZE),
        ]

class RSA_PUBLIC_KEY_STRUC(Structure):
    _pack_ = 1
    _fields_ = [
        ('RPKS_Version',                c_ubyte),
        ('RPKS_KeySize',                c_ushort),
        ('RPKS_Exponent',               c_ulong),
        ('RPKS_Modulus',                c_ubyte * RSA_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT),
        ]

class RSASSA_SIGNATURE_STRUCT(Structure):
    _pack_ = 1
    _fields_ = [
        ('RSS_Version',                 c_ubyte),
        ('RSS_KeySize',                 c_ushort),
        ('RSS_HashAlg',                 c_ushort),
        ('RSS_Signature',               c_ubyte * RSASSA_SIGNATURE_STRUCT_KEY_LEN_DEFAULT),
        ]

class KEY_SIGNATURE_STRUCT(Structure):
    _pack_ = 1
    _fields_ = [
        ('KSS_Version',                 c_ubyte),
        ('KSS_KeyAlg',                  c_ushort),
        ('KSS_key',                     RSA_PUBLIC_KEY_STRUC),
        ('KSS_SigScheme',               c_ushort),
        ('KSS_Signature',               RSASSA_SIGNATURE_STRUCT),
        ]

class KEY_MANIFEST_STRAUCTURE(Structure):
    _pack_ = 1
    _fields_ = [
        ('KMS_StructureID',             c_char*8),
        ('KMS_StructVersion',           c_ubyte),
        ('KMS_KeyManifestVersion',      c_ubyte),
        ('KMS_KMSVN',                   c_ubyte),
        ('KMS_KeyManifestID',           c_ubyte),
        ('KMS_BPKey',                   HASH_STRUCTURE),
        ('KMS_KeyManifestSignature',    KEY_SIGNATURE_STRUCT),
        ]
KMTableSize = (sizeof(KEY_MANIFEST_STRAUCTURE))
####################################################################################################

BPMTableAddress = [ ]
BPMBinFileName = 'BPM.bin'

BOOT_POLICY_MANIFEST_HEADER_STRUCTURE_ID          = "__ACBP__"
BOOT_POLICY_MANIFEST_STRUCTURE_VERSION            = 0x10
BOOT_POLICY_MANIFEST_HEADER_STRUCTURE_VERSION     = 0x01

IBB_SEGMENT_FLAG_IBB                              = 0x0
IBB_SEGMENT_FLAG_NON_IBB                          = 0x1

BOOT_POLICY_MANIFEST_IBB_ELEMENT_STRUCTURE_ID     = "__IBBS__"
BOOT_POLICY_MANIFEST_IBB_ELEMENT_VERSION          = 0x10
IBB_FLAG_ENABLE_VTD                               = 0x1
IBB_ENTRYPOINT_MIN                                = 0xFFFFFFC0
MCH_BASE_ADDRESS                                  = 0xfed10000

BOOT_POLICY_MANIFEST_SIGNATURE_ELEMENT_STRUCTURE_ID = "__PMSG__"
BOOT_POLICY_MANIFEST_SIGNATURE_ELEMENT_VERSION      = 0x10

BOOT_POLICY_MANIFEST_PLATFORM_MANUFACTURER_ELEMENT_STRUCTURE_ID = "__PMDA__"
BOOT_POLICY_MANIFEST_PLATFORM_MANUFACTURER_ELEMENT_VERSION      = 0x10
BOOT_POLICY_MANIFEST_PLATFORM_MANUFACTURER_ELEMENT_PMDatasize   = 0x04  # Must be multiple of 4 bytes and 
                                                                        # < 512bytes
class BOOT_POLICY_MANIFEST_HEADER_Element(Structure):
    _pack_ = 1
    _fields_ = [
        ('BPMH_StructureID',            c_char*8),
        ('BPMH_StructVersion',          c_ubyte),
        ('BPMH_HdrStructVersion',       c_ubyte),
        ('BPMH_PMBPMVersion',           c_ubyte),
        ('BPMH_BPSVN',                  c_ubyte),
        ('BPMH_ACMSVN',                 c_ubyte),
        ('BPMH_Reserved',               c_ubyte),
        ('BPMH_NEMDataStack',           c_ushort),
        ]
class IBB_SEGMENT_STRUCTURE(Structure):
    _pack_ = 1
    _fields_ = [
        ('ISS_Reserved',                c_ushort),
        ('ISS_Flags',                   c_ushort),
        ('ISS_Base',                    c_ulong),
        ('ISS_Size',                    c_ulong),
        ]

class IBB_ELEMENT_STRUCTURE(Structure):
    _pack_ = 1
    _fields_ = [
        ('IES_StructureID',             c_char * 8),
        ('IES_StructVersion',           c_ubyte),
        ('IES_Reserved',                c_ubyte * 2),
        ('IES_PBETValue',               c_ubyte),
        ('IES_Flags',                   c_ulong),
        ('IES_IBB_MCHBAR',              c_ulonglong),
        ('IES_VTD_BAR',                 c_ulonglong),
        ('IES_PMRL_BASE',               c_ulong),
        ('IES_PMRL_Limit',              c_ulong),
        ('IES_Reserved1',               c_ulonglong * 2),
        ('IES_PostIBBHash',             HASH_STRUCTURE),
        ('IES_EntryPoint',              c_ulong),
        ('IES_Digest',                  HASH_STRUCTURE),
        ('IES_SegmentCount',            c_ubyte),
        ('IES_IBBSegment',              IBB_SEGMENT_STRUCTURE),
        ]

class BOOT_POLICY_MANIFEST_SIGNATURE_ELEMENT(Structure):
    _pack_ = 1
    _fields_ = [
        ('BPMSE_StructureID',           c_char * 8),
        ('BPMSE_StructVersion',         c_ubyte),
        ('BPMSE_KeySignature',          KEY_SIGNATURE_STRUCT),
        ]

class PLATFORM_MANUFACTURER_ELEMENT(Structure):
    _pack_ = 1
    _fields_ = [
        ('PME_StructureID',             c_char * 8),
        ('PME_StructVersion',           c_ubyte),
        ('PME_PMDataSize',              c_ushort),
#       ('PME_PMData',                  c_byte * BOOT_POLICY_MANIFEST_PLATFORM_MANUFACTURER_ELEMENT_PMDatasize),
        ]

class BOOT_POLICY_MANIFEST_COMPLEX(Structure):
    _pack_ = 1
    _fields_ = [
        ('BPMH',                        BOOT_POLICY_MANIFEST_HEADER_Element),
        ('IES',                         IBB_ELEMENT_STRUCTURE),
#        ('PME',                        PLATFORM_MANUFACTURER_ELEMENT),
        ('BPMSE',                       BOOT_POLICY_MANIFEST_SIGNATURE_ELEMENT),
        ]
BPMTableSize = (sizeof(BOOT_POLICY_MANIFEST_COMPLEX))
