import os
import sys

#SearchList = ["gSiPkgTokenSpaceGuid.PcdBiosAreaBaseAddress",
#             "gInsydeTokenSpaceGuid.PcdFlashAreaSize",
#             "gInsydeTokenSpaceGuid.PcdFlashPbbBase",
#             "gInsydeTokenSpaceGuid.PcdFlashPbbSize"
#             ]
PcdValue = {'PcdBiosAreaBaseAddress':0, 
            'PcdChasmFallsSupport':0, 
            'PcdFlashPbbBase':0, 
            'PcdFlashPbbSize':0 
            }

NeedData = {'ChasmFallsType':0,'PbbOffset':0,'PbbSize':0}
def ArrangeData():
    NeedData['ChasmFallsType'] = hex(int(PcdValue['PcdChasmFallsSupport']))
    if (int(PcdValue['PcdChasmFallsSupport']) != 0):
        NeedData['PbbOffset'] = hex(int(PcdValue['PcdFlashPbbBase']-PcdValue['PcdBiosAreaBaseAddress']))
        NeedData['PbbSize'] = hex(int(PcdValue['PcdFlashPbbSize']))
    else:
        NeedData['PbbOffset'] = '0x0'
        NeedData['PbbSize'] = '0x0'

    # //Debug
    #for key,value in NeedData.items():
    #    print(key,value)
            
def FindPcdValue(File):
    if not os.path.exists(File):
        print ('BiosGuardSettingUpdate.exe get PcdList error! ' + File +' is inexistence!')
        sys.exit(-1)
    try:
        InputFile = open(File, 'r')
        FileLinesList = InputFile.readlines()
        for key in PcdValue.keys():
            for Line in FileLinesList:
                if Line.find(key) > 0:
                    Value = Line.split(': ')[1][0:-1]
                    if '0x' in Value:
                        Value = str(int(str(Value[2:]),16))
                    PcdValue[key]= int(Value)
                    break
        # //Debug
        #for key,value in PcdValue.items():
        #    print(key, value)
        
        ArrangeData()

    except IOError:
        print ('BiosGuardSettingUpdate.exe file access error! ' + File)
    finally:
        InputFile.close()
    return

def UpdateINIData(File):
    """ Update BGDataUpdate.exe to BiosGuardSetting.ini."""

    if not os.path.exists(File):
        print ('BiosGuardSettingUpdate.exe get INI file error! ' + File +' is inexistence!')
        sys.exit(-1)
    try:
        InputFile = open(File, 'r')
        FileLinesList = InputFile.readlines()
        Index = 0
        for key,value in NeedData.items():
            for Line in FileLinesList:
                if Line.find(key) > 0 and (Line.find(key) < Line.find(';;') or Line.find(';;') < 0):
                    #print FileLinesList[Index]
                    FileLinesList[Index] = '  ' + key + ' = ' + value + '\n'
                    #print FileLinesList[Index]
                    Index = 0;
                    break
                Index += 1
    except IOError:
        print ('BGDataUpdate.exe file access error! ' + File)
    finally:
        InputFile.close()
    
    try:
        InputFile = open(File, 'w')
        InputFile.writelines(FileLinesList)
    except IOError:
        print ('BGDataUpdate.exe update error! ' + File)
    finally:
        InputFile.close()
    return 

def BiosGuardSettingUpdate(InputPcdListFilePath, InputINIFilePath):
    if len(sys.argv) < 3:
        print len(sys.argv)
        print ("BiosGuardSettingUpdate.exe FAIL!!!")
        print ("incorrent number of arguments")
        print ("Usage: BiosGuardSettingUpdate.exe (PcdList_File_Path) (BiosGuardSetting_INI_Path)")
        sys.exit(-1)
    print (InputPcdListFilePath)
    FindPcdValue (InputPcdListFilePath)
    print (InputINIFilePath)
    UpdateINIData(InputINIFilePath)
    sys.exit(-1) 

def Main(InputPcdListFilePath, InputINIFilePath):
    if len(sys.argv) < 3:
        print len(sys.argv)
        print ("BiosGuardSettingUpdate.exe FAIL!!!")
        print ("incorrent number of arguments")
        print ("Usage: BiosGuardSettingUpdate.exe (PcdList_File_Path) (BiosGuardSetting_INI_Path)")
        sys.exit(-1)
    BiosGuardSettingUpdate(InputPcdListFilePath, InputINIFilePath)

Main(sys.argv[1], sys.argv[2])
