/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_GEN_GENFORBOOTGUARD_H

#include <Common/UefiBaseTypes.h>
//#include "TianoBind.h"
//#include "EfiTypes.h"
#define MAX_PATH                                255

//
// FIT Definitions
//
#define FIT_ADDRESS_OFFSET                      0x40
#define FIT_SIZE_OFFSET                         0x08
#define FIT_TYPE_OFFSET                         0x0E

//
// KM Definitions
//
#define KM_TYPE                                 0x0B
#define KM_HASH_PUBLIC_KEY_ADDRESS_OFFSET       0x10
#define KM_PUBLIC_KEY_ADDRESS_OFFSET            0x3A

//
// BPM Definitions
//
#define BPM_TYPE                                0x0C
#define BPM_POSTIBBHASH_ADDRESS_OFFSET          0x4C
#define BPM_IBBDIGEST_ADDRESS_OFFSET            0x74
#define BPM_IBBSIZE_ADDRESS_OFFSET              0x9D
#define BPM_PUBLIC_KEY_ADDRESS_OFFSET           0xB4
#define BPM_SIGN_AREA_OFFSET                    0xA1
#define BPM_SIGN_SIGNATURE_OFFSET               0x01BB

#define SHA256_DIGEST_SIZE                      32
#define FV_IMAGES_TOP_ADDRESS                   0x100000000

#define RSA_PUBLIC_KEY_STRUCT_KEY_SIZE_DEFAULT  2048
#define RSA_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT   (RSA_PUBLIC_KEY_STRUCT_KEY_SIZE_DEFAULT/8)

#endif _EFI_GEN_GENFORBOOTGUARD_H
