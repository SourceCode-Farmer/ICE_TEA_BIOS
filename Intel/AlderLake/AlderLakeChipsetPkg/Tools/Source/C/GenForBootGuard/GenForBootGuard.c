/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <stdio.h>
#include "string.h"
#include "stdlib.h"
#include "GenForBootGuard.h"

#pragma pack (1)
struct {
  INT8    *BinFileName;
  INT8    *OemPrivateKeyName;
  INT8    *OemPublicKeyName;
  INT8    *OdmPrivateKeyName;
  INT8    *OdmPublicKeyName;
} gGlobals;
#pragma pack ()

//static
//void
//Usage (
//  void
//  )
//{
//  int               i;
//  static const INT8 *Help[] = {
//    "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
//
//    NULL
//  };
//  for (i = 0; Help[i] != NULL; i++) {
//    fprintf (stdout, "%s\n", Help[i]);
//  }
//}

#define STATUS_ERROR 1
/*****************************************************************************
  FUNCTION:  ProcessOptions()

  DESCRIPTION: Process the command-line options.
******************************************************************************/
/**
 Process the command line options to this utility.

 @param        Argc             Standard Argc.
 @param        Argv[]           Standard Argv.


**/
static
int
ProcessOptions (
  int   Argc,
  INT8  *Argv[]
  )
{
  UINT8       Index=0;
  //
  // Clear out the options
  //
  memset ((INT8 *) &gGlobals, 0, sizeof (gGlobals));
  if (Argc == 0) {
//    Usage ();
    return STATUS_ERROR;
  }
  //
  // Now process the arguments
  //
  while (Argc > Index) {
    if ((Argv[Index][0] == '-') || (Argv[Index][0] == '/')) {
      switch (Argv[Index][1]) {
      //
      // -? or -h help option
      //
      case '?':
      case 'h':
      case 'H':
//        Usage ();
//        return STATUS_ERROR;

      case 'a':
        gGlobals.OemPrivateKeyName = Argv[Index+1];
        break;      	
      case 'A':
        gGlobals.OemPublicKeyName = Argv[Index+1];
        break;
      case 'b':
        gGlobals.OdmPrivateKeyName = Argv[Index+1];
        break;      	
      case 'B':
        gGlobals.OdmPublicKeyName = Argv[Index+1];
        break;        

      default:
//-        Error (NULL, 0, 0, Argv[0], "unrecognized option");
//        return STATUS_ERROR;
        break;
      }
    }
    Index++;
  }
  //
  // Must be at least one arg left
  //
  if (Argc > 0) {
    gGlobals.BinFileName = Argv[1];
  }     
  if (gGlobals.BinFileName == NULL) {
//-    Error (NULL, 0, 0, NULL, "must specify DSC fileName on command line");
    return STATUS_ERROR;
  }

  return 0;
}


int
main (
  int   Argc,
  char  *Argv[]
  )
{

  UINT32                    fileSize;
  CHAR8                     ReportFileName[_MAX_PATH];
  FILE                      *File = NULL;
  FILE                      *File1 = NULL;
  FILE                      *File2 = NULL;
  FILE                      *FileDigest = NULL;
  UINT8                     *Buffer = NULL;
  UINT8                     *Buffer1 = NULL;
  UINTN                     Index=0;    
  UINT32                    Temp =0;
  UINT8                     Temp8 =0;
  UINT32                    TableCount;
  UINT32                    IBBDigestAddress=0;
  UINT32                    IBBSize=0;  
  FILE                      *mHeadersFPtr = NULL;
  UINT32                    BPMMemAddress =0;
  UINT32                    BPMAddress =0;
  UINT32                    BPMAddressOffset =0;
  UINT32                    PostIbbHashAddress;
  UINT32                    KMMemAddress =0;
  UINT32                    KMAddress =0;
  UINT32                    KMAddressOffset =0;
  UINT32                    PBKeyDigestAddress=0;
  UINT32                    KMPBKeyAddress=0;
  UINT32                    BPMPBKeyAddress=0;
  UINT8                     PBKeyTemp=0;
  char                      *PreProcessCmd;

  //
  // Local Variable Initialize
  //
  PostIbbHashAddress = 0x00000000;
  
  if (ProcessOptions (Argc, Argv)) {
    return STATUS_ERROR;
  }
  if ((mHeadersFPtr = fopen (gGlobals.BinFileName, "rb+")) == NULL) {
    printf ("Invalid Parameter: Cannot open image file.\n");
    return STATUS_ERROR;
  }
  //
  //  change name for BOOT_GUARD_SINGLE_KEY 
  //
  if (gGlobals.OemPrivateKeyName != NULL && gGlobals.OemPublicKeyName != NULL \
    && gGlobals.OdmPrivateKeyName == NULL && gGlobals.OdmPublicKeyName == NULL) {
    gGlobals.OdmPrivateKeyName = gGlobals.OemPrivateKeyName;
    gGlobals.OdmPublicKeyName = gGlobals.OemPublicKeyName;  	
  }
  //
  //  get OEM RSA Public Modulus (N)
  //
  if (gGlobals.OemPublicKeyName != NULL) {	
    PreProcessCmd = malloc (MAX_PATH);
    strcpy (PreProcessCmd, "MODULUS.exe ");
    strcat (PreProcessCmd, gGlobals.OemPublicKeyName);
    strcat (PreProcessCmd, " OemPBkeyTemp.bin");
    system (PreProcessCmd);
//  system ("openssl rsa -pubin -modulus < OemPublicKeyName -outform -binary > OemPBkeyTemp.bin");
    free (PreProcessCmd);
  
    PreProcessCmd = malloc (MAX_PATH);
    strcpy (PreProcessCmd, "Modulus2Bin OemPBkeyTemp.bin OemPBkey.bin");
    system (PreProcessCmd);
    free (PreProcessCmd);  
  }
  //
  //  get ODM RSA Public Modulus (N)
  //
  if (gGlobals.OemPublicKeyName != NULL) {
    PreProcessCmd = malloc (MAX_PATH);
    strcpy (PreProcessCmd, "MODULUS.exe ");
    strcat (PreProcessCmd, gGlobals.OdmPublicKeyName);
    strcat (PreProcessCmd, " OdmPBkeyTemp.bin");
    system (PreProcessCmd);
//  system ("openssl rsa -pubin -modulus < OdmPublicKeyName -outform -binary > OdmPBkeyTemp.bin");
    free (PreProcessCmd);
  
    PreProcessCmd = malloc (MAX_PATH);
    strcpy (PreProcessCmd, "Modulus2Bin OdmPBkeyTemp.bin OdmPBkey.bin");
    system (PreProcessCmd);
    free (PreProcessCmd);  
  }
  //
  // find rom size
  //
  fseek(mHeadersFPtr, 0, SEEK_END);      // seek to end of file
  fileSize = ftell(mHeadersFPtr);        // get current file pointer  
  
  //
  // find FIT table
  //
  fseek(mHeadersFPtr, fileSize - FIT_ADDRESS_OFFSET, SEEK_SET);
  fread (&Temp, 4, 1, mHeadersFPtr);
  Temp = (UINT32)(FV_IMAGES_TOP_ADDRESS - Temp);
  //
  // find FIT Size
  //
  fseek(mHeadersFPtr, fileSize - Temp + FIT_SIZE_OFFSET, SEEK_SET);
  fread (&TableCount, 4, 1, mHeadersFPtr);
  //
  // find KM ,BPM address
  //
  for (Index=1 ;Index < TableCount; Index++) {
    fseek(mHeadersFPtr, fileSize - Temp + Index*16 + FIT_TYPE_OFFSET, SEEK_SET);	
    fread (&Temp8, 1, 1, mHeadersFPtr);
    if (Temp8 == KM_TYPE) {
      KMAddressOffset = fileSize - Temp + Index*16;
      fseek(mHeadersFPtr, fileSize - Temp + Index*16, SEEK_SET); 
      fread (&KMMemAddress, 4, 1, mHeadersFPtr); 
    }
    if (Temp8 == BPM_TYPE) {
      BPMAddressOffset = fileSize - Temp + Index*16;
      fseek(mHeadersFPtr, fileSize - Temp + Index*16, SEEK_SET); 
      fread (&BPMMemAddress, 4, 1, mHeadersFPtr);  	
      break;
    }
  }
  //
  // goto KM address
  //  
  fseek(mHeadersFPtr, KMAddressOffset, SEEK_SET); 
  fread (&Temp, 4, 1, mHeadersFPtr);     
  //
  // find Public Key Hash address
  //
  Temp = (UINT32)(FV_IMAGES_TOP_ADDRESS - Temp);
  KMAddress = fileSize - Temp;
  PBKeyDigestAddress = fileSize - Temp + KM_HASH_PUBLIC_KEY_ADDRESS_OFFSET;
  KMPBKeyAddress = fileSize - Temp + KM_PUBLIC_KEY_ADDRESS_OFFSET;  
  //
  // goto BPM address
  //
  fseek(mHeadersFPtr, BPMAddressOffset, SEEK_SET); 
  fread (&Temp, 4, 1, mHeadersFPtr); 
  Temp = (UINT32)(FV_IMAGES_TOP_ADDRESS - Temp);
  //
  // find Public Key of BPM
  //
  BPMAddress = fileSize - Temp;
  BPMPBKeyAddress = fileSize - Temp + BPM_PUBLIC_KEY_ADDRESS_OFFSET;
  //
  // find IBBSize
  //
  fseek(mHeadersFPtr, fileSize - Temp + BPM_IBBSIZE_ADDRESS_OFFSET, SEEK_SET);
  fread (&IBBSize, 4, 1, mHeadersFPtr);  
  //
  // find IBBDigest address
  //
  fseek(mHeadersFPtr, fileSize - Temp + BPM_IBBDIGEST_ADDRESS_OFFSET, SEEK_SET);
  IBBDigestAddress = fileSize - Temp + BPM_IBBDIGEST_ADDRESS_OFFSET;
  //
  // Find PostIbbHash Address
  //
  fseek(mHeadersFPtr, fileSize - Temp + BPM_POSTIBBHASH_ADDRESS_OFFSET, SEEK_SET);
  PostIbbHashAddress = fileSize - Temp + BPM_POSTIBBHASH_ADDRESS_OFFSET;
  //
  // create IBB.bin
  //
  Buffer = (UINT8 *) malloc (IBBSize); 
  memset (Buffer,  0xFF, IBBSize);
  fseek(mHeadersFPtr, fileSize - IBBSize, SEEK_SET);
  fread (Buffer, IBBSize, 1, mHeadersFPtr);  
  strcpy (ReportFileName, "IBB"); 
  strcat (ReportFileName, ".bin");
  File = fopen (ReportFileName, "wb+"); 
  fwrite (Buffer, IBBSize, 1, File);   
  fclose (File);
  //
  // HASH IBB.bin
  //
  system ("SHA256HASH.exe IBB.bin IBBdigest.bin");
  //system ("openssl.exe dgst -sha256 -binary IBB.bin > IBBdigest.bin");
  //
  // HASH FvMain_ComPact.fv
  //
  system ("SHA256HASH.exe BIOS\\FVMAIN_COMPACT.fv FvMainCompactHash.bin");
  //system ("openssl.exe dgst -sha256 -binary BIOS\\FVMAIN_COMPACT.fv > FvMainCompactHash.bin");
  //
  // copy a new BIOS
  //  
  fseek(mHeadersFPtr, 0, SEEK_SET);
  Buffer = (UINT8 *) malloc (fileSize); 
  memset (Buffer,  0xFF, fileSize);
  fseek(mHeadersFPtr, 0, SEEK_SET);  
  File1 = fopen ("BootGuardTemp.bin", "wb+"); 
  fread (Buffer, fileSize, 1, mHeadersFPtr);       
  fwrite (Buffer, fileSize, 1, File1);
  if (gGlobals.OemPublicKeyName != NULL || gGlobals.OdmPublicKeyName != NULL) {
    if (gGlobals.OemPublicKeyName != NULL) {
      //
      // change OemPBkey to LSB
      //
      FileDigest = fopen ("OemPBkey.bin", "rb");
      Buffer1 = (UINT8 *) malloc (256);
      File2 = fopen ("OemPBkeyL.bin", "wb+"); 
      fseek(File2, 0, SEEK_SET);
      for (Index=0 ;Index < 256 ; Index++) {
        fseek(FileDigest, 255 - Index, SEEK_SET);
        fread (&PBKeyTemp, 1, 1, FileDigest);
        fwrite (&PBKeyTemp, 1, 1, File2);     
      }
      fclose (File2); 
      fclose (FileDigest); 
    }
    if (gGlobals.OdmPublicKeyName != NULL) {
      //
      // change OdmPBkey to LSB
      //
      FileDigest = fopen ("OdmPBkey.bin", "rb");
      Buffer1 = (UINT8 *) malloc (256);
      File2 = fopen ("OdmPBkeyL.bin", "wb+"); 
      fseek(File2, 0, SEEK_SET);
      for (Index=0 ;Index < 256 ; Index++) {
        fseek(FileDigest, 255 - Index, SEEK_SET);
        fread (&PBKeyTemp, 1, 1, FileDigest);
        fwrite (&PBKeyTemp, 1, 1, File2);     
      }
      fclose (File2); 
      fclose (FileDigest); 
    }
    //
    // HASH OEM Public Key OemPBkey.bin
    //
    system ("SHA256HASH.exe OemPBkeyL.bin OemPublicKeyHash.bin");
    //system ("openssl.exe dgst -sha256 -binary OemPBkeyL.bin > OemPublicKeyHash.bin");    
    //
    // HASH ODM Public Key OdmPBkey.bin
    //
    system ("SHA256HASH.exe OdmPBkeyL.bin PBdigest.bin");
    //system ("openssl.exe dgst -sha256 -binary OdmPBkeyL.bin > PBdigest.bin");
    //
    // put PBdigest.bin to KM in New BIOS
    //
    Buffer1 = (UINT8 *) malloc (SHA256_DIGEST_SIZE);
    FileDigest = fopen ("PBdigest.bin", "rb");
    fread (Buffer1, SHA256_DIGEST_SIZE, 1, FileDigest);
    fseek(File1, PBKeyDigestAddress, SEEK_SET);
    fwrite (Buffer1, SHA256_DIGEST_SIZE, 1, File1);  
    fclose (FileDigest);    
    //
    // put OemPBkeyL.bin to KM in New BIOS
    //
    Buffer1 = (UINT8 *) malloc (RSA_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT);
    FileDigest = fopen ("OemPBkeyL.bin", "rb");
    fread (Buffer1, RSA_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT, 1, FileDigest);
    fseek(File1, KMPBKeyAddress, SEEK_SET);
    fwrite (Buffer1, RSA_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT, 1, File1);  
    fclose (FileDigest);  
    //
    // put OdmPBkeyL.bin to BPM in New BIOS
    //
    Buffer1 = (UINT8 *) malloc (RSA_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT);
    FileDigest = fopen ("OdmPBkeyL.bin", "rb");
    fread (Buffer1, RSA_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT, 1, FileDigest);
    fseek(File1, BPMPBKeyAddress, SEEK_SET);
    fwrite (Buffer1, RSA_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT, 1, File1);  
    fclose (FileDigest);
  }
  
  //
  // Put FvMainCompact.bin to PostIbbHash in New BIOS
  //
  Buffer1 = (UINT8 *) malloc (SHA256_DIGEST_SIZE);
  FileDigest = fopen ("FvMainCompactHash.bin", "rb");
  fread (Buffer1, SHA256_DIGEST_SIZE, 1, FileDigest);
  fseek (File1, PostIbbHashAddress, SEEK_SET);
  fwrite (Buffer1, SHA256_DIGEST_SIZE, 1, File1);
  fclose (FileDigest);

  //
  // put IBBdigest.bin to IBBDigestAddress in New BIOS
  //
  Buffer1 = (UINT8 *) malloc (SHA256_DIGEST_SIZE);
  FileDigest = fopen ("IBBdigest.bin", "rb");
  fread (Buffer1, SHA256_DIGEST_SIZE, 1, FileDigest);
  fseek(File1, IBBDigestAddress, SEEK_SET);
  fwrite (Buffer1, SHA256_DIGEST_SIZE, 1, File1);  
  fclose (File1);  
  fclose (FileDigest);   
  fclose (mHeadersFPtr);
  
  //
  // get KM sign area in new bios
  //
  File1 = fopen ("BootGuardTemp.bin", "rb");   
  fseek(File1, KMAddress, SEEK_SET); 
  Buffer1 = (UINT8 *) malloc (48);
  fread (Buffer1, 48, 1, File1);
  FileDigest = fopen ("km.bin", "wb+");
  fwrite (Buffer1, 48, 1, FileDigest);
  fclose (FileDigest);  
  //
  // sign km.bin
  //
  printf ("\n[Boot Guard] Sign KM ...\n");
  if (gGlobals.OemPrivateKeyName != NULL) {
    PreProcessCmd = malloc (MAX_PATH);
    strcpy (PreProcessCmd, "SIGN.exe ");
    strcat (PreProcessCmd, gGlobals.OemPrivateKeyName);
    strcat (PreProcessCmd, " km.bin kms.bin");
    system (PreProcessCmd);
//  system ("openssl dgst -sha256 -sign key.pem -out kms.bin km.bin");
    free (PreProcessCmd);

    PreProcessCmd = malloc (MAX_PATH);
    strcpy (PreProcessCmd, "VERIFY.exe ");
    strcat (PreProcessCmd, gGlobals.OemPublicKeyName);
    strcat (PreProcessCmd, " km.bin kms.bin");
    system (PreProcessCmd);  
//  system ("openssl dgst -sha256 -verify pkey.pem -signature kms.bin km.bin");
    free (PreProcessCmd);
  }
  //
  // get BPM sign area in new bios
  //
  fseek(File1, BPMAddress, SEEK_SET); 
  Buffer1 = (UINT8 *) malloc (BPM_SIGN_AREA_OFFSET);
  fread (Buffer1, BPM_SIGN_AREA_OFFSET, 1, File1);
  FileDigest = fopen ("bpm.bin", "wb+");
  fwrite (Buffer1, BPM_SIGN_AREA_OFFSET, 1, FileDigest);
  fclose (FileDigest);  
  //
  // sign bpm.bin
  //
  printf ("\n[Boot Guard] Sign BPM ...\n");  
  if (gGlobals.OdmPrivateKeyName != NULL) {
    PreProcessCmd = malloc (MAX_PATH);
    strcpy (PreProcessCmd, "SIGN.exe ");
    strcat (PreProcessCmd, gGlobals.OdmPrivateKeyName);
    strcat (PreProcessCmd, " bpm.bin bpms.bin");
    system (PreProcessCmd);
  
    free (PreProcessCmd);
//  system ("openssl dgst -sha256 -sign key.pem -out bpms.bin bpm.bin");

    PreProcessCmd = malloc (MAX_PATH);
    strcpy (PreProcessCmd, "VERIFY.exe ");
    strcat (PreProcessCmd, gGlobals.OdmPublicKeyName);
    strcat (PreProcessCmd, " bpm.bin bpms.bin");
    system (PreProcessCmd);  
  
    free (PreProcessCmd);   
  }
//  system ("openssl dgst -sha256 -verify pkey.pem -signature bpms.bin bpm.bin");  
  fclose (File1);  
  //
  // copy a new bios again
  //
  File2 = fopen ("BootGuardTemp.bin", "rb");
  fseek(File2, 0, SEEK_SET);
  memset (Buffer,  0xFF, fileSize);
  File1 = fopen ("BootGuard.bin", "wb+"); 
  fseek(File1, 0, SEEK_SET);  
  fread (Buffer, fileSize, 1, File2);       
  fwrite (Buffer, fileSize, 1, File1);
  if (gGlobals.OemPrivateKeyName != NULL) {  
    //
    // put KMS.bin to new bios
    //      
    fseek(File1, KMAddress + 321, SEEK_SET);     
    Buffer1 = (UINT8 *) malloc (256);
    FileDigest = fopen ("kms.bin", "rb");
    fread (Buffer1, 256, 1, FileDigest);
    fwrite (Buffer1, 256, 1, File1);     
    fclose (FileDigest);
  }
  if (gGlobals.OdmPrivateKeyName != NULL) {
    //
    // put BPMS.bin to new bios
    //      
    fseek(File1, BPMAddress + BPM_SIGN_SIGNATURE_OFFSET, SEEK_SET);     
    Buffer1 = (UINT8 *) malloc (256);
    FileDigest = fopen ("bpms.bin", "rb");
    fread (Buffer1, 256, 1, FileDigest);
    fwrite (Buffer1, 256, 1, File1);   
  }  

  fclose (File1);
  fclose (File2);
  fclose (FileDigest);   
  system ("del IBBdigest.bin");
  system ("del IBB.bin");
  system ("del FvMainCompactHash.bin");
  system ("del BootGuardTemp.bin");
  system ("del km.bin");  
  system ("del bpm.bin");  
  system ("del Modulus2Bin.exe");
//  system ("del PBdigest.bin");
  system ("del MODULUS.exe");
  system ("del SHA256HASH.exe");
  system ("del SIGN.exe");
  system ("del VERIFY.exe");

  if (gGlobals.OemPublicKeyName != NULL || gGlobals.OdmPublicKeyName != NULL) {
    system ("del OemPBkey.bin");
    system ("del OdmPBkey.bin");
    system ("del OemPBkeyL.bin");
    system ("del OdmPBkeyL.bin");  
    system ("del OemPBkeyTemp.bin");
    system ("del OdmPBkeyTemp.bin");
    system ("del kms.bin");
    system ("del bpms.bin");
    system ("del PBdigest.bin");
  } 
  return 0;
}
