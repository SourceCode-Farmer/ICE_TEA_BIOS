/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _EFI_GEN_BIOS_GUARD_HEADER_H

#include <Common/BaseTypes.h>
#include <Common/UefiBaseTypes.h>
#include "ctype.h"

#define BIOSGUARD_PACKAGE_BIOS      1
#define BIOSGUARD_PACKAGE_EC        2
#define BIOSGUARD_PACKAGE_FIRMWARE  3
#define BIOSGUARD_PACKAGE_ROM_TRANS 4
#define BIOSGUARD_PACKAGE_HD2ROM    5

// For compatibility of flash tool, Block header offset:0x3C
// 0xFFFFFFFF mean based on BIOS region base offset.
// 0x00000000 mean based on ROM base offset
#define BGSL_DST_TYPE_OFFSET       0x03 

#define BGSL_SPI_SRC_ADDR_OFFSET   0x03
#define BGSL_SIZE_OFFSET           0x05
#define BGSL_SPI_DST_ADDR_OFFSET   0x07 //(Block header offset:0x4C)

#define STATUS_SUCCESS 0
#define STATUS_ERROR   1
#define SIZE_4K        0x1000
#define SIZE_64K       0x10000
#define SIZE_128k      0X20000
#define GRANULARITY_SIZE  SIZE_64K
#define MessageRange   0x4   //Only PackageMode = 1 2 3 will show message.

#define CHASME_FALLS_DISABLED 0
#define CHASME_FALLS_GEN1     1
#define CHASME_FALLS_GEN2     2
#define UPDPKG_ATTR_SFAM_CHECK             BIT0
#define UPDPKG_ATTR_PROT_EC_OPR            BIT1
#define UPDPKG_ATTR_GFX_MITIGATION_DISABLE BIT2
#define UPDPKG_ATTR_FAULT_TOLERANT_UPDATE  BIT3

//
// DESCRIPTOR CONTENT
// 
#define R_DESCRIPTOR_FDBAR_FLMAP0                    ( 0x14 )
#define B_DESCRIPTOR_FDBAR_FLMAP0_FRBA               ( 0x00FF0000 )
#define N_DESCRIPTOR_FDBAR_FLMAP0_FRBA               ( 16 )
#define R_DESCRIPTOR_FRBA_FLREG1_BIOS                ( 0x04 )
#define R_DESCRIPTOR_FRBA_FLREG2_CSE                 ( 0x08 )
#define R_DESCRIPTOR_FRBA_FLREG3_GBE                 ( 0x0C )
#define R_DESCRIPTOR_FRBA_FLREG8_EC                  ( 0x20 )

#define B_DESCRIPTOR_FRBA_FLREG1_REGION_BASE         ( 0x00007FFF )
#define N_DESCRIPTOR_FRBA_FLREG1_REGION_BASE         ( 0 )
#define B_DESCRIPTOR_FRBA_FLREG1_REGION_LIMIT        ( 0x7FFF0000 )
#define N_DESCRIPTOR_FRBA_FLREG1_REGION_LIMIT        ( 16 )

#define DESCRIPTOR_REGION  0
#define EC_REGION          1
#define GBE_REGION         2
#define CSE_REGION         3
#define BIOS_REGION        4
#define REGION_MAX         9

#define BIOSGUARD_SIGN "$PFATHDR"

typedef struct {
  UINT8                                 RegionType;
  UINT8                                 FRBA_Offset;
} DESCRIPTOR_REGION_LIST;

//
// Section name relative definitions
//
typedef enum {
  PlatformId,
  FileName,
  BGSLFile,
  PackageMode,
  EcSizeSection,
  EcOffsetSection,
  FwSizeSection,
  FwOffsetSection,
  ChasmFallsType,
  PbbOffsetSection,
  PbbSizeSection,
  SFAM_CHECK,
  PROT_EC_OPR,
  GFX_MITIGATION_DISABLE,
  FTU,
  BiosSvnSection,
  EcSvnSection,
  VendorSpecificSection,
  MaxSection
} BIOSGUARD_SECTION_TYPE;

#pragma pack (1)

//// INSYDE_BIOSGUARD_HEADER Version.2 
//// Support Flexiable Data Size between 4K-64K
//typedef struct
//{
//    BYTE                pbySignature[8];    // "$PFATHDR"
//    DWORD               dwVersion;          // 2
//    DWORD               dwBlockCount;       // Total update block count
//    DWORD               dwBlockSize;        // The PFAT block table size
//    DWORD               dwStructureSize;    // This structure size
//    DWORD               dwDataSize[dwBlockCount];  // Data size of each PUP block (<=64K)
//} SPfatHeaderV2;

//// INSYDE_BIOSGUARD_HEADER Version.3 
//// Support Chasm Falls Gen 1 (IBB & IBBr) same data updata to different address.
//typedef struct
//{
//    DWORD               dwOffset;           // The data offset of this PFAT block
//    DWORD               dwSize;             // The data size of this PFAT block
//} SPfatBlockInfo;
//
//typedef struct
//{
//    BYTE                pbySignature[8];    // "$PFATHDR"
//    DWORD               dwVersion;          // 3
//    DWORD               dwBlockCount;       // Total update block count
//    DWORD               dwBlockSize;        // The PFAT block table size
//    DWORD               dwStructureSize;    // This structure size
//    SPfatBlockInfo      psPfatBlockInfo[dwBlockCount];  // Block info structure array, total dwBlockCount
//} SPfatHeaderV3;

typedef struct _INSYDE_BIOSGUARD_HEADER {
  UINT8   Signature[8];  // $PFATHDR
  UINT32  Version;       // 
  UINT32  BlockCount;    // Block Count (Total count of PUP blocks)
  UINT32  BlockSize;     // Block Size (Size of each PUP block)
  UINT32  StructureSize; // INSYDE_BIOSGUARD_HEADER Structure Size 
} INSYDE_BIOSGUARD_HEADER;

typedef struct _BIOSGUARD_BLOCK_INFO {
  UINT32  Offset; // The data offset of the BiosGuard block.
  UINT32  Size;   // The data size of this BiosGuard block.
} BIOSGUARD_BLOCK_INFO;

typedef struct _BGUP_HEADER {
  UINT16  Version;      // Version of the update package header. 
                        // Must be 0x0002.
                        
  UINT16  Reserved1;    // Must be 0.
  
  UINT8   PLAT_ID[16];  // The BIOS Guard module will compare the 
                        // PLAT_ID found in the BGPDT with the
                        // PLAT_ID found in the BIOS Guard update
                        // package header to ensure a given update
                        // package is appropriate for the platform.
                        // This prevents cross flashing of a platform
                        // image to the wrong platform when a
                        // single platform signing key is used.
                        
  UINT16  UPDPKG_ATTR;  // Any bit set in this field indicates the
                        // update package must be signed and the
                        // update package must not be processed 
                        // unless it is accompanied by a valid certificate.
                        //
                        // Bit 0: SFAM_CHECK
                        // Specific meaning of this bit as follows:
                        // 0b = No update of SFAM flash, no need to 
                        // evaluate BIOS_SVN.
                        // 1b = Host flash update of flash covered by 
                        // the SFAM and the BIOS_SVN policy must be 
                        // evaluated and applied.
                        //
                        // Bit 1: (PROT_EC_OPR)
                        // Specific meaning of this bit is as follows:
                        // 0b = No protected EC operations included in
                        // script
                        // 1b =  Protected EC operations included in 
                        // script. If PROT_EC_OPR is set, the EC SVN 
                        // policy (obtained via 
                        // ECCMD_GetSecurityVersionNumber) must be 
                        // evaluated and applied. Additionally, the BIOS 
                        // Guard module will issue EC_OPEN command 
                        // to EC prior in beginning script execution; 
                        // similarly, it will issue EC_CLOSE command to 
                        // EC after script execution terminates.
                        // 
                        // Bit 2: GFX_MITIGATION_DISABLE
                        // 0b = All IntelR GFX related checks and                         // 
                        // security enhancements will be enforced.
                        // 1b = No IntelR GFX related checks or 
                        // security enhancements will be enforced.
                        // 
                        // Bit 3: FAULT_TOLERANT_UPDATE (FTU)
                        // 0b = indicates the script will not manipulate 
                        // TOP_SWAP or bypass Flash Protected Range 
                        // Registers.
                        // 1b = The script requires ability to manipulate 
                        // TOP_SWAP, or bypass Flash Protected Range 
                        // Registers. If FTU is set, the BIOS_SVN policy 
                        // must be evaluated and applied.
                        // 
                        // Bits 15:4 reserved, must be 0.
                      
  UINT16  Reserved4;    // Must be 0.
  
  UINT16  BGSL_VER_MAJ; // Indicates the BGSL major.minor version of
  UINT16  BGSL_VER_MIN; // the script associated with this update
                        // package. For this version of the specification,
                        // this should indicate version 2.0.
                        
  UINT32  SCRIPT_SIZE;  // Size in bytes of the script. Must be a multiple 
                        // of 8 bytes (script line size). Minimum value is 
                        // 16 bytes (script with BEGIN and END only). 
                        // Maximum valid value for BIOS Guard 1.0 is 
                        // 16384 (16K) bytes (2048 script lines).
                        
  UINT32  DATA_SIZE;    // Size of the data region in bytes. A value of 0 
                        // indicates no data is included in update 
                        // package and therefore B0 does not exist.
                        
  UINT32  BIOS_SVN;     // If UPDPKG_ATTR.SFAM_CHECK == 1, 
                        // indicates the Security Version Number of the 
                        // BIOS update contained within this update 
                        // package. The BIOS Guard module must 
                        // verify BGUP.Header.BIOS_SVN >= 
                        // BGPDT.BIOS_SVN. If 
                        // UPDPKG_ATTR.SFAM_CHECK == 0, 
                        // BIOS_SVN checking is not done and this field 
                        // is ignored by the BIOS Guard module.
                        
  UINT32  EC_SVN;       // If UPDPKG_ATTR.PROT_EC_OPR == 1, 
                        // indicates the Security Version Number of the 
                        // EC FW update, if there is one contained 
                        // within this update package. If the update 
                        // package contains protected EC operations, 
                        // but does not contain an EC firmware update, 
                        // the BGUP.Header.EC_SVN should be set to 
                        // 0xFFFFFFFF. The BIOS Guard module must 
                        // verify BGUP.Header.EC_SVN >= EC_SVN 
                        // retrieved from the EC via 
                        // ECCMD_GetSecurityVersionNumber. If 
                        // UPDPKG_ATTR.PROT_EC_OPR == 0, EC_SVN 
                        // checking is not done and this field is ignored 
                        // by the BIOS Guard module.
                        
  UINT32  VEND_SPECIFIC;// 4 bytes available to script writer. This field is 
                        // not referenced by the BIOS Guard module. It 
                        // is, however included in digital signature of 
                        // signed scripts. It is expected that it will be 
                        // used for vendor specific versioning or script 
                        // identification.
} BGUP_HEADER;

typedef struct _H2O_FLASH_DEVICE_MAP_HEADER {
  UINT32 Signature;
  UINT32 Size;
  UINT32 Offset;
  UINT32 EntrySize;
  UINT8  EntryFormat;
  UINT8  Revision;
  UINT8  Reserved;
  UINT8  Checksum;
  UINT64 FdBaseAddr;
} H2O_FLASH_DEVICE_MAP_HEADER;

typedef struct _H2O_FLASH_DEVICE_MAP_ENTRY {
  EFI_GUID RegionType;
  UINT8    RegionId[16];
  UINT64   RegionOffset;
  UINT64   RegionSize;
  UINT32   Attribs;
} H2O_FLASH_DEVICE_MAP_ENTRY;

#pragma pack ()

UINT8
ProcessIniFile (
  IN  char                       *ConfigFile,
  IN  BGUP_HEADER                *BgupHeader
  );

UINT8
ProcessSection (
  IN  BIOSGUARD_SECTION_TYPE     SectionType,
  IN  char                       *Line,
  IN  BGUP_HEADER                *BgupHeader
  );


UINT8
ReadFileToArray (
  IN      char                   *FileName,
  IN OUT  UINT8                  **Binary,
  IN OUT  UINT32                 *BinarySize
  );

UINT8
GetFdmDataFromFd (
  IN UINT8                       *FdBuffer,
  IN UINT32                       ImageSize,
  OUT UINT32                     *CalculateBiosSize
  );

VOID
HexStrToValue (
  IN  char        *Buffer,
  OUT UINTN       *Output
);

UINTN
CharToDec (
  char Data
  );

VOID
DecStrToValue (
  IN  char        *Buffer,
  OUT UINTN       *Output
);

VOID
GetSPIRegionMap (
  IN UINT8                       *FdBuffer,
  IN UINT32                       ImageSize
  );
#endif _EFI_GEN_BIOS_GUARD_HEADER_H
