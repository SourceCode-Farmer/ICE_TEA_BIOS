/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "GenBiosGuardHdr.h"

struct {
  INT8    *ProjectSettingPath;
  INT8    *FileName;
  INT8    *BgslFileName;
  UINT32  EcSize;
  UINT32  EcOffset;
  UINT32  FwSize;
  UINT32  FwOffset;
  UINT8   PackageMode;
  UINT8   ChasmFallsType;
  UINT32  PbbOffset;
  UINT32  PbbSize;
  UINT32  SourceOffset;
  UINT32  DestOffset;
  UINT32  DataSize;
  INT8    *OutputFileName;
} gGlobals;

typedef struct {
  UINT32  BeginOffset;
  UINT32  EndOffset;
  UINT32  Size;
} RegionMap;


FILE           *mOutputHeaderFilePtr = NULL;
STATIC CHAR8   mSignature[] = {'H', 'F', 'D', 'M'};
UINT32         *mBGSL = NULL;
UINT32         mBiosBase = 0;
UINT32         mBgslBinarySize = 0;
BGUP_HEADER    *mBgupHeader = NULL;
INSYDE_BIOSGUARD_HEADER    *mInsydeBiosGuardHeader;

RegionMap  *mBiosRegionMap;
RegionMap  *mSPIRegionMap;   //0-Descriptor Region; 1-EC Region; 2-GBE Region; 3-CSE Region; 4-BIOS Region
UINT32         mFdmEntryCount;
UINT32         mDataListIndex = 0;
BIOSGUARD_BLOCK_INFO       *mDataInfoList = NULL;

static
void
Usage (
  void
  )
{
  int               i;
  static const INT8 *Help[] = {
    "Usage:  GenBiosGuardHdr {options} [Filename]",
    "    Options:",
    "       -i File Path          , INI file",
    "       -f File Path          , RowData Image, ex.BIOS,EC,FW",
    "       -p BGSL bin File Path , Bin file",
    "       -m PackageMode        , 1=BIOS, 2=EC, 3=FW",
    "      ==For ChasmeFall Gen2==",
    "       -m PackageMode        , 4=RomDataTransfer ex.PBB/PBBR, 5=HdDataToRom, ex.SBB",
    "       -s Source Offset      , ex.Pbb/Pbbr Offset in Bios image",
    "       -d Dest Offset        , ex.Pbbr/Pbb or Sbb Offset in Bios image",
    "       -l DataSize           , ex.Pbb or Sbb Size",
    "       -o Output File Name   ",
    NULL
  };
  for (i = 0; Help[i] != NULL; i++) {
    fprintf (stdout, "%s\n", Help[i]);
  }
}

/*****************************************************************************
  FUNCTION:  ProcessOptions()

  DESCRIPTION: Process the command-line options.
******************************************************************************/
/**
 Process the command line options to this utility.

 @param        Argc             Standard Argc.
 @param        Argv[]           Standard Argv.


**/
static
int
ProcessOptions (
  int   Argc,
  INT8  *Argv[]
  )
{
  INT8 *Temp;
  //
  // Clear out the options
  //
  memset ((INT8 *) &gGlobals, 0, sizeof (gGlobals));

  Argc--;
  Argv++;

  if (Argc == 0) {
    Usage ();
    return STATUS_ERROR;
  }
  //
  // Now process the arguments
  //
  while (Argc > 0) {

    if ((Argv[0][0] == '-') || (Argv[0][0] == '/')) {
      switch (Argv[0][1]) {
      //
      // -? or -h help option
      //
      case '?':
      case 'h':
      case 'H':
        Usage ();
        return STATUS_ERROR;

      //
      // Load Setting File
      //
      case 'i':
      case 'I':
        //
        // Skip to next arg
        //
        Argc--;
        Argv++;
        if (Argc == 0) {
          Argv--;
          return STATUS_ERROR;
        } else {
          gGlobals.ProjectSettingPath = Argv[0];
//          printf("gGlobals.ProjectSettingPath : %s\n", gGlobals.ProjectSettingPath);
        }
        break;

      case 'f':
      case 'F':
        Argc--;
        Argv++;
        if (Argc == 0) {
          Argv--;
          return STATUS_ERROR;
        } else {
          gGlobals.FileName = (INT8 *) malloc (strlen(Argv[0])+1);
          memset (gGlobals.FileName, 0x0, strlen(Argv[0])+ 1);
          memcpy (gGlobals.FileName, Argv[0], strlen(Argv[0]));
          //printf("gGlobals.FileName : %s\n", gGlobals.FileName);
        }
        break;

      case 'p':
      case 'P':
        //
        // Skip to next arg
        //
        Argc--;
        Argv++;
        if (Argc == 0) {
          Argv--;
          return STATUS_ERROR;
        } else {
          gGlobals.BgslFileName = (INT8 *) malloc (strlen(Argv[0])+1);
          memset (gGlobals.BgslFileName, 0x0, strlen(Argv[0])+ 1);
          memcpy (gGlobals.BgslFileName, Argv[0], strlen(Argv[0]));
          //printf("gGlobals.BgslFileName : %s\n", gGlobals.BgslFileName);
        }
        break;      

      case 'm':
      case 'M':
        Argc--;
        Argv++;
        if (Argc == 0) {
          Argv--;
          return STATUS_ERROR;
        } else {
          gGlobals.PackageMode = (UINT8)**Argv - 0x30; //AsciiCode 0x30 = 0
          if (gGlobals.PackageMode > 5){
            printf ("gGlobals.PackageMode: %x (Range 1-5)\n", gGlobals.PackageMode);
            return STATUS_ERROR;
          }
          //printf ("gGlobals.PackageMode: %x\n", gGlobals.PackageMode);
        }
       break;

      case 's':
      case 'S':
        //
        // Skip to next arg
        //
        Argc--;
        Argv++;
        if (Argc == 0) {
          Argv--;
          return STATUS_ERROR;
        } else {
          Temp = (INT8 *) malloc (strlen(Argv[0])+1);
          memset (Temp, 0x0, strlen(Argv[0])+ 1);
          memcpy (Temp, Argv[0], strlen(Argv[0]));         
//          printf("gGlobals.SourceOffset_temp : %s\n", Temp);
          HexStrToValue (Temp, (UINTN*)&gGlobals.SourceOffset);
          if (gGlobals.SourceOffset == 0) {
            DecStrToValue(Temp, (UINTN*)&gGlobals.SourceOffset);
          }
//          printf("gGlobals.SourceOffset : %x\n", gGlobals.SourceOffset);
        }
        break;

      case 'd':
      case 'D':
        //
        // Skip to next arg
        //
        Argc--;
        Argv++;
        if (Argc == 0) {
          Argv--;
          return STATUS_ERROR;
        } else {
          Temp = (INT8 *) malloc (strlen(Argv[0])+1);
          memset (Temp, 0x0, strlen(Argv[0])+ 1);
          memcpy (Temp, Argv[0], strlen(Argv[0])); 
//          printf("gGlobals.DestOffset_temp : %s\n", Temp);
          HexStrToValue (Temp, (UINTN*)&gGlobals.DestOffset);
          if (gGlobals.DestOffset == 0) {
            DecStrToValue(Temp,(UINTN*)&gGlobals.DestOffset);
          }
//          printf("gGlobals.DestOffset : %x\n", gGlobals.DestOffset);
        }
        break;

      case 'l':
      case 'L':
        //
        // Skip to next arg
        //
        Argc--;
        Argv++;
        if (Argc == 0) {
          Argv--;
          return STATUS_ERROR;
        } else {
          Temp = (INT8 *) malloc (strlen(Argv[0])+1);
          memset (Temp, 0x0, strlen(Argv[0])+ 1);
          memcpy (Temp, Argv[0], strlen(Argv[0])); 
//          printf("gGlobals.DataSize _temp : %s\n", Temp);
          HexStrToValue (Temp, (UINTN*)&gGlobals.DataSize);
          if (gGlobals.DataSize == 0) {
            DecStrToValue(Temp,(UINTN*)&gGlobals.DataSize);
          }
//          printf("gGlobals.DataSize : %x\n", gGlobals.DataSize);
        }
        break;

     case 'o':
     case 'O':
        Argc--;
        Argv++;
        if (Argc == 0) {
          Argv--;
          return STATUS_ERROR;
        } else {
          gGlobals.OutputFileName = (INT8 *) malloc (strlen(Argv[0])+1);
          memset (gGlobals.OutputFileName, 0x0, strlen(Argv[0])+ 1);
          memcpy (gGlobals.OutputFileName, Argv[0], strlen(Argv[0]));
        }
        break;

      default:
        printf ("Wrong param\n");
        return STATUS_ERROR;
      }
    } else {
      break;
    }

    Argc--;
    Argv++;
  }

  return 0;
}

UINT8
ReadInputFile (
  IN CHAR8    *FileName,
  IN OUT UINT32  *BinarySize,
  IN OUT UINT8   **FileData
  )
/*++

Routine Description:

  Read input file

Arguments:

  FileName      - The input file name
  FileData      - The input file data, the memory is aligned.

Returns:

  STATUS_SUCCESS - The file found and data read
  STATUS_ERROR   - The file data is not read

-*/
{
  FILE                *InputFile  = NULL;
  UINT32              FileSize;
  UINT32              TempResult;
  TempResult = 0;
  FileSize  = 0;
  //
  // Open the Input file
  //
  if ((InputFile = fopen (FileName, "rb")) == NULL) {
    return STATUS_ERROR;
  }
  //
  // Get the Input file size
  //
  fseek (InputFile, 0, SEEK_END);
  FileSize = ftell (InputFile);
  *BinarySize = FileSize;
  //
  // Read the contents of input file to memory buffer
  //
  *FileData = (UINT8 *) malloc (FileSize);
  if (NULL == *FileData) {
    printf ("No sufficient memory to allocate!\n");
    fclose (InputFile);
    return STATUS_ERROR;
  }

  fseek (InputFile, 0, SEEK_SET);
  TempResult = fread (*FileData, 1, FileSize, InputFile);
  if (TempResult != FileSize) {
    printf ("Read input file error!\n");
    free ((VOID *)*FileData);
    fclose (InputFile);
    return STATUS_ERROR;
  }

  //
  // Close the input file
  //
  fclose (InputFile);

  return STATUS_SUCCESS;
}
UINT32
BlockCalculate (
  UINT32  ImageSize
)
{
  UINT32 BlockCount;
  BlockCount = 0;
  BlockCount = (ImageSize / GRANULARITY_SIZE);
  if ((ImageSize % GRANULARITY_SIZE) != 0) {
    BlockCount++;
  }
  return BlockCount;
}
VOID
SetDataInfoList (
 UINT32                Offset,
 UINT32                Size
){
  UINT32  RemainSize;
  BOOLEAN FirstOffset;
  FirstOffset = TRUE;
  RemainSize = Size;
  
  mDataInfoList[mDataListIndex].Offset = Offset;
  while (RemainSize > 0) {
    if (RemainSize >= GRANULARITY_SIZE){
      mDataInfoList[mDataListIndex].Size = GRANULARITY_SIZE;           
      RemainSize -= GRANULARITY_SIZE;
    } else {
      mDataInfoList[mDataListIndex].Size = RemainSize;
      RemainSize = 0;
    }

    if ((mDataListIndex != 0) && (!FirstOffset)){
        mDataInfoList[mDataListIndex].Offset = mDataInfoList[mDataListIndex-1].Offset + mDataInfoList[mDataListIndex-1].Size;
      }
    mDataListIndex++;
    FirstOffset = FALSE;
  }
  return;
}

UINT8
DefaultBGUPInit ()
{
  mBgupHeader = (BGUP_HEADER *) malloc (sizeof (BGUP_HEADER));
  if (mBgupHeader == NULL) {
    printf ("Invalid Parameter: Allocate memory for Intel BIOS Guard Update Package Header fail.\n");
    return STATUS_ERROR;
  }
  memset (mBgupHeader, 0x00, (sizeof (BGUP_HEADER)));
  mBgupHeader->Version      = 0x0002;
  mBgupHeader->BGSL_VER_MAJ = 0x0002;
  mBgupHeader->BGSL_VER_MIN = 0x0000;
  mBgupHeader->UPDPKG_ATTR  = 0x0001;
  return STATUS_SUCCESS;
}

VOID
PrintInputData ()
{
  if (gGlobals.PackageMode < MessageRange) {
    printf ("----------INI Setting Input Data ----------\n");
    printf (" ProjectSettingPath : %s\n", gGlobals.ProjectSettingPath);
    printf (" FileName           : %s\n", gGlobals.FileName);
    printf (" BgslFileName       : %s\n", gGlobals.BgslFileName);
    printf (" PackageMode        : ");
    switch (gGlobals.PackageMode) {
      case  BIOSGUARD_PACKAGE_BIOS:
        printf ("BIOS\n");
        break;
      case  BIOSGUARD_PACKAGE_EC:
        printf ("EC\n");
        printf ("   EcSize             : 0x%08x\n", gGlobals.EcSize);
        printf ("   EcOffset           : 0x%08x\n", gGlobals.EcOffset);
        break;
      case  BIOSGUARD_PACKAGE_FIRMWARE:
        printf ("Firmware\n");
        printf ("   FwSize             : 0x%08x\n", gGlobals.FwSize);
        printf ("   FwOffset           : 0x%08x\n", gGlobals.FwOffset);
        break;
      case  BIOSGUARD_PACKAGE_ROM_TRANS:
        printf ("ROM DATA TRANSFER\n");
        printf ("   SourceOffset       : 0x%08x\n", gGlobals.SourceOffset);
        printf ("   DestOffset         : 0x%08x\n", gGlobals.DestOffset);      
        printf ("   DataSize           : 0x%08x\n", gGlobals.DataSize);
        break;
      case  BIOSGUARD_PACKAGE_HD2ROM:
        printf ("HD DATA Bake TO ROM\n");
        printf ("   DestOffset         : 0x%08x\n", gGlobals.DestOffset);      
        printf ("   DataSize           : 0x%08x\n", gGlobals.DataSize);
        break;
    }
    printf (" ChasmFallsType     : ");
    switch (gGlobals.ChasmFallsType) {
      case  0:
        printf ("None\n");
        break;
      case  1:
        printf ("Gen1\n");
        break;
      case  2:
        printf ("Gen2\n");
        break;
    }
    if (gGlobals.ChasmFallsType == 1) {
      printf ("   PbbOffset          : 0x%08x\n", gGlobals.PbbOffset);
      printf ("   PbbSize            : 0x%08x\n", gGlobals.PbbSize);
    }

    if (mBgupHeader->UPDPKG_ATTR == 0) {
      mBgupHeader->UPDPKG_ATTR |= UPDPKG_ATTR_SFAM_CHECK;
      printf ("!! UPDPKG_ATTR not allow all of bits did not be set, change to default setting !!\n");
    }
    printf (" UPDPKG_ATTR        : ");
    if (mBgupHeader->UPDPKG_ATTR & UPDPKG_ATTR_SFAM_CHECK)             { printf ("[SFAM_CHECK] "); }
    if (mBgupHeader->UPDPKG_ATTR & UPDPKG_ATTR_PROT_EC_OPR)            { printf ("[PROT_EC_OPR] "); }
    if (mBgupHeader->UPDPKG_ATTR & UPDPKG_ATTR_GFX_MITIGATION_DISABLE) { printf ("[GFX_MITIGATION_DISABLE] "); }
    if (mBgupHeader->UPDPKG_ATTR & UPDPKG_ATTR_FAULT_TOLERANT_UPDATE)  { printf ("[FAULT_TOLERANT_UPDATE] "); }
    printf ("\n");
    
    printf (" BiosSvn            : 0x%08x\n", mBgupHeader->BIOS_SVN);
    printf (" EcSvn              : 0x%08x\n", mBgupHeader->EC_SVN);
    printf (" VendorSpecific     : 0x%08x\n", mBgupHeader->VEND_SPECIFIC);
  }
}

VOID
PrintBgslBinData(
  UINT32* BGSL,
  UINT32 size
)
{
  if (gGlobals.PackageMode < MessageRange) {
    UINT32   Index;
    printf ("---------- BGSL BIN Data ----------\n");
    for (Index = 0; Index < (size / 4); Index++) {
      if ((Index != 0) && (Index % 2 == 0)){
        printf ("\n");
      }
      printf ("0x%08x ", *(BGSL + Index));
    }
    printf ("\n");
  }
}

VOID
PrintInsydeBGHdrData(
 INSYDE_BIOSGUARD_HEADER *InsydeBiosGuardHeader
)
{
  if (gGlobals.PackageMode < MessageRange) {
    printf ("---------- InsydeBiosGuardHeader ----------\n");
    printf (" Signature     : %s\n",     InsydeBiosGuardHeader->Signature);
    printf (" Version       : 0x%08x\n", InsydeBiosGuardHeader->Version);
    printf (" BlockCount    : 0x%08x\n", InsydeBiosGuardHeader->BlockCount);
    printf (" BlockSize     : 0x%08x\n", InsydeBiosGuardHeader->BlockSize);
    printf (" StructureSize : 0x%08x\n", InsydeBiosGuardHeader->StructureSize);
  }
}

VOID
PrintIntelBGHdrData(
)
{
  if (gGlobals.PackageMode < MessageRange) {
    printf ("---------- BGUP_HEADER IntelBiosGuardHeader ----------\n");
    printf (" Version       : 0x%04x\n", mBgupHeader->Version);
    printf (" PLAT_ID       : %s\n",     mBgupHeader->PLAT_ID);
    printf (" UPDPKG_ATTR   : 0x%04x\n", mBgupHeader->UPDPKG_ATTR);
    printf (" BGSL_VER_MAJ  : 0x%04x\n", mBgupHeader->BGSL_VER_MAJ);
    printf (" BGSL_VER_MIN  : 0x%04x\n", mBgupHeader->BGSL_VER_MIN);
    printf (" SCRIPT_SIZE   : 0x%08x\n", mBgupHeader->SCRIPT_SIZE);
    printf (" BIOS_SVN      : 0x%08x\n", mBgupHeader->BIOS_SVN);
    printf (" EC_SVN        : 0x%08x\n", mBgupHeader->EC_SVN);
    printf (" VEND_SPECIFIC : 0x%08x\n", mBgupHeader->VEND_SPECIFIC);
  }
}

UINT8
CreateBiosBGUP(
)
{
  UINT8                      Status;
  FILE                       *InputFile;
  UINT32                     ImageSize;
  BIOSGUARD_BLOCK_INFO       *BlockInfo;
  UINT32                     BiosBase;
  UINT32                     BlockCount;
  UINT32                     NonBiosBlockCount;
  UINT32                     BiosBlockCount;
  UINT32                     PbbrBlockCount;
  UINT32                     Index;
  BOOLEAN                    IsFullImage;
  UINT32                     LastSize;

  UINT8                      *FdFileBuffer;
  UINT32                     CalculateBiosSize;
  Status                     = STATUS_SUCCESS;
  InputFile                  = NULL;
  ImageSize                  = 0;
  BlockInfo                  = NULL;
  BiosBase                   = 0;
  BlockCount                 = 0;
  NonBiosBlockCount          = 0;
  BiosBlockCount             = 0;
  PbbrBlockCount             = 0;
  Index                      = 0;

  IsFullImage                = FALSE;
  LastSize                   = 0;
  CalculateBiosSize          = 0;
  
  printf(" Create BIOS GUARD Update Package Header\n");
  //
  // Get RowData
  //
  ReadInputFile (gGlobals.FileName, &ImageSize, &FdFileBuffer);

  Status = GetFdmDataFromFd (FdFileBuffer, ImageSize, &CalculateBiosSize);
  if (Status != STATUS_SUCCESS) {
    return Status;
  }
  if (ImageSize != CalculateBiosSize) {
    if (gGlobals.ChasmFallsType != CHASME_FALLS_DISABLED) {
      printf ("ERROR\n");
      printf ("ImageSize = 0x%08x, CalculateSize = 0x%08x\n",ImageSize, CalculateBiosSize);
      printf ("Chasme Falls only support BIOS update only. Please check image again.\n");
      printf ("ERROR\n");
      return STATUS_ERROR;
    } else {
      mBGSL[BGSL_DST_TYPE_OFFSET] = 0x00000000;
      IsFullImage = TRUE;
      GetSPIRegionMap (FdFileBuffer, ImageSize);
    }
  }
  //
  // Insyde Bios Guard Header
  //
  if (IsFullImage) {
    for (Index = 0; Index <= REGION_MAX; Index++){
      if (Index == BIOS_REGION){
        continue;
      }
      NonBiosBlockCount += BlockCalculate(mSPIRegionMap[Index].Size);
    }
    for (Index = 0; Index < mFdmEntryCount; Index++){
      BiosBlockCount += BlockCalculate(mBiosRegionMap[Index].Size);
    }
    BlockCount = NonBiosBlockCount + BiosBlockCount;
  } else {
    for (Index = 0; Index < mFdmEntryCount; Index++){
      BiosBlockCount += BlockCalculate(mBiosRegionMap[Index].Size);
    }
    if (gGlobals.ChasmFallsType == CHASME_FALLS_GEN1) {       
      for (Index = 0; Index < mFdmEntryCount; Index++) {
        if (mBiosRegionMap[Index].BeginOffset < gGlobals.PbbOffset) {
          continue;
        }
        PbbrBlockCount += BlockCalculate(mBiosRegionMap[Index].Size);
      }
    }
    BlockCount = BiosBlockCount + PbbrBlockCount;
  }

  mDataInfoList = (BIOSGUARD_BLOCK_INFO *) malloc (BlockCount * sizeof (BIOSGUARD_BLOCK_INFO));
  memset (mDataInfoList, 0x00, BlockCount * sizeof (BIOSGUARD_BLOCK_INFO));

  if (IsFullImage) {

    for (Index = 0; Index <= REGION_MAX; Index++){
      if (Index == BIOS_REGION){
        continue;
      }
      SetDataInfoList (mSPIRegionMap[Index].BeginOffset, mSPIRegionMap[Index].Size);
    }
    printf("\n");
    for (Index = 0; Index < mFdmEntryCount; Index++) {
       mBiosRegionMap[Index].BeginOffset += mSPIRegionMap[BIOS_REGION].BeginOffset;
       SetDataInfoList (mBiosRegionMap[Index].BeginOffset, mBiosRegionMap[Index].Size);
    }
  } else {
    for (Index = 0; Index < mFdmEntryCount; Index++) {
       SetDataInfoList (mBiosRegionMap[Index].BeginOffset, mBiosRegionMap[Index].Size);
    }
    if (gGlobals.ChasmFallsType == CHASME_FALLS_GEN1) {
    //
    // PBBr DataInfo List, as same as Pbb.
    //
      while (mDataInfoList[Index].Offset < gGlobals.PbbOffset) {
        Index++;
      }
      memcpy (&mDataInfoList[mDataListIndex], &mDataInfoList[Index], (mDataListIndex - Index) * sizeof (BIOSGUARD_BLOCK_INFO));
    }
  }

  mInsydeBiosGuardHeader->Version       = 0x00000003;
  mInsydeBiosGuardHeader->BlockCount    = BlockCount;
  mInsydeBiosGuardHeader->BlockSize     = sizeof(BGUP_HEADER) + mBgslBinarySize;
  mInsydeBiosGuardHeader->StructureSize = sizeof (INSYDE_BIOSGUARD_HEADER) + BlockCount * sizeof(BIOSGUARD_BLOCK_INFO);
  fwrite (mInsydeBiosGuardHeader, sizeof (INSYDE_BIOSGUARD_HEADER), 1, mOutputHeaderFilePtr);

  fwrite (mDataInfoList, BlockCount * sizeof(BIOSGUARD_BLOCK_INFO), 1, mOutputHeaderFilePtr);
  PrintInsydeBGHdrData (mInsydeBiosGuardHeader);
  printf("  --Block Count Detail(0x%08x, %d)--\n", BlockCount, BlockCount);
  if (IsFullImage) {
    printf("  [FULL Image]\n");
    printf("    NonBios Block Count        = 0x%08x, %d\n", NonBiosBlockCount, NonBiosBlockCount);
    printf("    Bios Block Count           = 0x%08x, %d\n", BiosBlockCount, BiosBlockCount);
  } else {
    printf("    Bios Block Count           = 0x%08x, %d\n", BiosBlockCount, BiosBlockCount);
    if (gGlobals.ChasmFallsType == CHASME_FALLS_GEN1) {
      printf("    Pbbr Block Count           = 0x%08x, %d\n", PbbrBlockCount, PbbrBlockCount);
    }
  }

  for (Index = 0; Index < BlockCount; Index++) { 
  //
  // BGUP_HEADER, Intel Bios Guard Update Package Header
  //
    mBgupHeader->SCRIPT_SIZE  = mBgslBinarySize;
    mBgupHeader->DATA_SIZE    = mDataInfoList[Index].Size;

    //
    // Adjust Offset and Size in BGSL
    //
    mBGSL[BGSL_SPI_DST_ADDR_OFFSET] = mDataInfoList[Index].Offset;
    mBGSL[BGSL_SIZE_OFFSET]         = mDataInfoList[Index].Size;
    
    //PrintBgslBinData(mBGSL,mBgslBinarySize);
    fwrite (mBgupHeader,sizeof(BGUP_HEADER), 1,mOutputHeaderFilePtr);
    fwrite (mBGSL, mBgslBinarySize, 1, mOutputHeaderFilePtr);
  }
  PrintIntelBGHdrData();
  return STATUS_SUCCESS;  
}

UINT8
CreateEcBGUP(
){
  FILE                       *InputFile;
  UINT32                     ImageSize;
  UINT32                     BlockCount;
  UINT32                     Index;

  InputFile                  = NULL;
  ImageSize                  = 0;
  BlockCount                 = 0;
  printf(" Create EC BiosGuard Update Package Header\n");
  //
  // Get RowData
  //
  if ((InputFile = fopen (gGlobals.FileName, "rb"))== NULL) {
    printf ("Invalid Parameter: Cannot open %s file.\n", gGlobals.FileName);
    return STATUS_ERROR;
  }
  fseek(InputFile, 0, SEEK_END);
  ImageSize = ftell (InputFile);
  if (ImageSize != gGlobals.EcSize) {
    printf ("!!! Warning %s File Size (0x%08x) is NOT MATCH EcSize (0x%08x) descirption in Setting file (INI)\n ",gGlobals.FileName, ImageSize, gGlobals.EcSize);
    return STATUS_ERROR;
  }
  //
  // Insyde Bios Guard Header
  //
  mInsydeBiosGuardHeader->Version = 0x00000003;
  BlockCount = BlockCalculate (ImageSize);
  mInsydeBiosGuardHeader->BlockCount = BlockCount;
  mInsydeBiosGuardHeader->BlockSize = sizeof(BGUP_HEADER) + mBgslBinarySize;

  mDataInfoList = (BIOSGUARD_BLOCK_INFO *) malloc (BlockCount * sizeof (BIOSGUARD_BLOCK_INFO));
  memset (mDataInfoList, 0x00, BlockCount * sizeof (BIOSGUARD_BLOCK_INFO));
  SetDataInfoList (gGlobals.EcOffset, gGlobals.EcSize);
  mInsydeBiosGuardHeader->StructureSize = sizeof (INSYDE_BIOSGUARD_HEADER) + BlockCount * sizeof(BIOSGUARD_BLOCK_INFO);
  fwrite (mInsydeBiosGuardHeader, sizeof (INSYDE_BIOSGUARD_HEADER), 1, mOutputHeaderFilePtr);
  fwrite (mDataInfoList, BlockCount * sizeof(BIOSGUARD_BLOCK_INFO), 1, mOutputHeaderFilePtr);
  PrintInsydeBGHdrData (mInsydeBiosGuardHeader);


  for (Index = 0; Index < BlockCount; Index++) { 
    //
    // BGUP_HEADER, Intel Bios Guard Update Package Header
    //
    mBgupHeader->SCRIPT_SIZE  = mBgslBinarySize;
    mBgupHeader->DATA_SIZE    = mDataInfoList[Index].Size;

    //
    // Adjust Offset and Size in BGSL
    //
    mBGSL[BGSL_SPI_DST_ADDR_OFFSET] = mDataInfoList[Index].Offset;
    mBGSL[BGSL_SIZE_OFFSET]         = mDataInfoList[Index].Size;
    //PrintBgslBinData(mBGSL,mBgslBinarySize);
    fwrite (mBgupHeader,sizeof(BGUP_HEADER), 1,mOutputHeaderFilePtr);
    fwrite (mBGSL, mBgslBinarySize, 1, mOutputHeaderFilePtr);
  }
    PrintIntelBGHdrData();
  return STATUS_SUCCESS;  
}

UINT8
CreateFwSplitBGUP(
  IN UINT32 Offset,
  IN UINT32 Size,
  IN UINT32 InsydeHdrVer
) {
  FILE                       *InputFile;
  UINT32                     ImageSize;
  UINT32                     BlockCount;
  UINT32                     Index;

  InputFile                  = NULL;
  ImageSize                  = 0;
  BlockCount                 = 0;
  printf(" Create FW BiosGuard Update Package Header\n");
  //
  // Get RowData
  //
  if ((InputFile = fopen (gGlobals.FileName, "rb"))== NULL) {
    printf ("Invalid Parameter: Cannot open %s file.\n", gGlobals.FileName);
    return STATUS_ERROR;
  }
  fseek(InputFile, 0, SEEK_END);
  ImageSize = ftell (InputFile);
  if (ImageSize != Size) {
    printf ("!!! Warning %s File Size (0x%08x) is NOT MATCH Size (0x%08x) descirption in Setting file (INI)\n ",gGlobals.FileName, ImageSize, Size);
  }

  //
  // Insyde Bios Guard Header
  //
  BlockCount = BlockCalculate (ImageSize);
  mDataInfoList = (BIOSGUARD_BLOCK_INFO *) malloc (BlockCount * sizeof (BIOSGUARD_BLOCK_INFO));
  memset (mDataInfoList, 0x00, BlockCount * sizeof (BIOSGUARD_BLOCK_INFO));
  SetDataInfoList (Offset, Size);
  mInsydeBiosGuardHeader->Version = InsydeHdrVer;
  mInsydeBiosGuardHeader->BlockCount = BlockCount;
  mInsydeBiosGuardHeader->BlockSize = sizeof(BGUP_HEADER) + mBgslBinarySize;

  if (InsydeHdrVer == 0x00000002) {
    mInsydeBiosGuardHeader->StructureSize = sizeof (INSYDE_BIOSGUARD_HEADER) + (BlockCount * sizeof(UINT32));
    fwrite (mInsydeBiosGuardHeader, sizeof (INSYDE_BIOSGUARD_HEADER), 1, mOutputHeaderFilePtr);
    for (Index = 0; Index < BlockCount; Index++) {
      fwrite (&(mDataInfoList[Index].Size), sizeof (UINT32), 1, mOutputHeaderFilePtr);
    }
  } else if (InsydeHdrVer == 0x00000003) {
    mInsydeBiosGuardHeader->StructureSize = sizeof (INSYDE_BIOSGUARD_HEADER) + BlockCount * sizeof(BIOSGUARD_BLOCK_INFO);
    fwrite (mInsydeBiosGuardHeader, sizeof (INSYDE_BIOSGUARD_HEADER), 1, mOutputHeaderFilePtr);
    fwrite (mDataInfoList, BlockCount * sizeof(BIOSGUARD_BLOCK_INFO), 1, mOutputHeaderFilePtr);
  }
  PrintInsydeBGHdrData (mInsydeBiosGuardHeader);

  for (Index = 0; Index < BlockCount; Index++) { 
    //
    // BGUP_HEADER, Intel Bios Guard Update Package Header
    //
    mBgupHeader->SCRIPT_SIZE  = mBgslBinarySize;
    mBgupHeader->DATA_SIZE    = mDataInfoList[Index].Size;

    //
    // Adjust Offset and Size in BGSL
    //
    mBGSL[BGSL_SPI_DST_ADDR_OFFSET] = mDataInfoList[Index].Offset;
    mBGSL[BGSL_SIZE_OFFSET]         = mDataInfoList[Index].Size;
    //PrintBgslBinData(mBGSL,mBgslBinarySize);
    fwrite (mBgupHeader,sizeof(BGUP_HEADER), 1,mOutputHeaderFilePtr);
    fwrite (mBGSL, mBgslBinarySize, 1, mOutputHeaderFilePtr);
  }
  PrintIntelBGHdrData();

  return STATUS_SUCCESS;  
}

UINT8
CreateFwBGUP(
  IN UINT32 Offset,
  IN UINT32 Size,
  IN UINT32 InsydeHdrVer
) {
  FILE                       *InputFile;
  UINT32                     ImageSize;
  BIOSGUARD_BLOCK_INFO       *BlockInfo;

  InputFile                  = NULL;
  ImageSize                  = 0;
  BlockInfo                  = NULL;
  printf(" Create FW BiosGuard Update Package Header \n");

  //
  // Get RowData
  //
  if ((InputFile = fopen (gGlobals.FileName, "rb"))== NULL) {
    printf ("Invalid Parameter: Cannot open %s file.\n", gGlobals.FileName);
    return STATUS_ERROR;
  }
  fseek(InputFile, 0, SEEK_END);
  ImageSize = ftell (InputFile);

  //
  // Insyde Bios Guard Header
  //
//  BlockInfo = (BIOSGUARD_BLOCK_INFO *) malloc (sizeof (BIOSGUARD_BLOCK_INFO));
//  memset (BlockInfo, 0x00, sizeof (BIOSGUARD_BLOCK_INFO));
//
//  mInsydeBiosGuardHeader->Version = InsydeHdrVer;
//  mInsydeBiosGuardHeader->BlockCount = 1;
//  mInsydeBiosGuardHeader->BlockSize = sizeof(BGUP_HEADER) + mBgslBinarySize;
//
//  BlockInfo->Offset = Offset;  
//  BlockInfo->Size   = Size;
//  if (InsydeHdrVer == 0x00000002) {
//    mInsydeBiosGuardHeader->StructureSize = sizeof (INSYDE_BIOSGUARD_HEADER) + sizeof(UINT32);
//    fwrite (mInsydeBiosGuardHeader, sizeof (INSYDE_BIOSGUARD_HEADER), 1, mOutputHeaderFilePtr);
//    fwrite (&(BlockInfo->Size), sizeof (UINT32), 1, mOutputHeaderFilePtr);
//  } else if (InsydeHdrVer == 0x00000003) {
//    fwrite (mInsydeBiosGuardHeader, sizeof (INSYDE_BIOSGUARD_HEADER), 1, mOutputHeaderFilePtr);
//    fwrite (BlockInfo, sizeof (BIOSGUARD_BLOCK_INFO), 1, mOutputHeaderFilePtr);
//  }
  //  PrintInsydeBGHdrData (InsydeBiosGuardHeader);

  //
  // BGUP_HEADER, Intel Bios Guard Update Package Header
  //
  mBgupHeader->SCRIPT_SIZE  = mBgslBinarySize;
  mBgupHeader->DATA_SIZE    = ImageSize;
  PrintIntelBGHdrData();
  fwrite (mBgupHeader,sizeof(BGUP_HEADER), 1,mOutputHeaderFilePtr);

  //
  // Adjust Offset and Size in BGSL
  //
  mBGSL[BGSL_SPI_DST_ADDR_OFFSET] = Offset;
  mBGSL[BGSL_SIZE_OFFSET]         = Size;
  //PrintBgslBinData(mBGSL,mBgslBinarySize);
  fwrite (mBGSL, mBgslBinarySize, 1, mOutputHeaderFilePtr);
  
  return STATUS_SUCCESS;  
}

UINT8
CreateRomDataTransferBGUP(
  IN UINT32 SrcOffset,
  IN UINT32 DstOffset,
  IN UINT32 Size
) {
  printf(" Create Rom Data Transfer  BiosGuard Update Package Header\n");

  UINT8 Status;
  UINT32 RowDataSize;
  Status = STATUS_SUCCESS;
  RowDataSize = 0; // Row Transfer, no need RowData.
  //
  // Insyde Bios Guard Header
  //
//  mInsydeBiosGuardHeader->Version = 0x00000002;
//  mInsydeBiosGuardHeader->BlockCount = 1;
//  mInsydeBiosGuardHeader->BlockSize = sizeof(BGUP_HEADER) + mBgslBinarySize;
//  mInsydeBiosGuardHeader->StructureSize = sizeof (INSYDE_BIOSGUARD_HEADER) + sizeof(UINT32);
//  fwrite (mInsydeBiosGuardHeader, sizeof (INSYDE_BIOSGUARD_HEADER), 1, mOutputHeaderFilePtr);
//  fwrite (&RowDataSize, sizeof (UINT32), 1, mOutputHeaderFilePtr);
  //  PrintInsydeBGHdrData (InsydeBiosGuardHeader);

  //
  // BGUP_HEADER, Intel Bios Guard Update Package Header
  //
  mBgupHeader->SCRIPT_SIZE = mBgslBinarySize;
  mBgupHeader->DATA_SIZE = 0;
  PrintIntelBGHdrData();
  fwrite (mBgupHeader,sizeof(BGUP_HEADER), 1,mOutputHeaderFilePtr);

  //
  // Adjust Offset and Size in BGSL
  //
  mBGSL[BGSL_SPI_DST_ADDR_OFFSET] = DstOffset;
  mBGSL[BGSL_SPI_SRC_ADDR_OFFSET] = SrcOffset;   
  mBGSL[BGSL_SIZE_OFFSET]         = Size;
  //PrintBgslBinData(mBGSL,mBgslBinarySize);
  fwrite (mBGSL, mBgslBinarySize, 1, mOutputHeaderFilePtr);
  
  return STATUS_SUCCESS;  
}

int
main (
  int   Argc,
  char  *Argv[]
  )
/*++

Routine Description:
  GenBiosGuardHdr Flow
  1. Read/Analyze file
  2. Generate Insyde BIOS Guard Header
  3. Generate Intel BIOS Guard Header 
Arguments:

  FileName      - The input file name
  FileData      - The input file data, the memory is aligned.

Returns:

  STATUS_SUCCESS - The file found and data read
  STATUS_ERROR   - The file data is not read

--*/

{
  UINT8                 Status;
  UINT32                Index;
  UINT8                 BiosGuardSignature[] = "$PFATHDR";

  Status                = STATUS_SUCCESS;
//  mBgupHeader           = NULL;
  Index                 = 0;
  printf("\n ===GenBiosGuardHdr.exe === \n");
  if (ProcessOptions (Argc, Argv)) {
    return STATUS_ERROR;
  }

  //
  // Intel BGUP Init
  //

  mBgupHeader = (BGUP_HEADER *) malloc (sizeof (BGUP_HEADER));
  if (mBgupHeader == NULL) {
    printf ("Invalid Parameter: Allocate memory for Intel BIOS Guard Update Package Header fail.\n");
    return STATUS_ERROR;
  }
  memset (mBgupHeader, 0x00, (sizeof (BGUP_HEADER)));
  Status = DefaultBGUPInit ();
  if (Status == STATUS_ERROR) {
    return Status;
  }
  //
  // Open (BiosGuardSetting.ini) Setting file file and get data
  //
  Status = ProcessIniFile (gGlobals.ProjectSettingPath, mBgupHeader);
  if (Status == STATUS_ERROR) {
    printf ("\nError: Open BIOS Guard setting file %s fail\n", gGlobals.ProjectSettingPath);
    return Status;
  }
  PrintInputData();

  mInsydeBiosGuardHeader = (INSYDE_BIOSGUARD_HEADER *) malloc (sizeof (INSYDE_BIOSGUARD_HEADER));
  memset (mInsydeBiosGuardHeader, 0x00, (sizeof (INSYDE_BIOSGUARD_HEADER)));
  memcpy (mInsydeBiosGuardHeader->Signature, BiosGuardSignature, sizeof (BiosGuardSignature)); //$PFATHDR

  //
  // Read Bios Guard Script language Binary file.
  //
  Status = ReadFileToArray (gGlobals.BgslFileName, (UINT8 **)&mBGSL, &mBgslBinarySize);
  if (Status == STATUS_ERROR) {
    printf ("Invalid Parameter: Cannot open %s file.\n", gGlobals.BgslFileName);          
    return Status;
  }
  //  PrintBgslBinData(mBGSL,mBgslBinarySize);


  //
  // Create InsydeBiosGuardHeader.bin
  //
  if (gGlobals.PackageMode <=3){
    if ((mOutputHeaderFilePtr = fopen ("InsydeBiosGuardHeader.bin", "wb")) == NULL) {
      printf ("Create output file (InsydeBiosGuardHeader.bin) fail.\n");
      return STATUS_ERROR;
    }
  }
  switch (gGlobals.PackageMode) {
    case (BIOSGUARD_PACKAGE_BIOS):
      Status = CreateBiosBGUP();
      break;

    case (BIOSGUARD_PACKAGE_EC):
      Status = CreateEcBGUP();
      break;

    case (BIOSGUARD_PACKAGE_FIRMWARE):
      Status = CreateFwSplitBGUP(gGlobals.FwOffset, gGlobals.FwSize, 2);
      break;

    case (BIOSGUARD_PACKAGE_ROM_TRANS):
      if ((mOutputHeaderFilePtr = fopen (gGlobals.OutputFileName, "wb")) == NULL) {
        printf ("Create output file (%s) fail.\n",gGlobals.OutputFileName);
        return STATUS_ERROR;
      }
      Status = CreateRomDataTransferBGUP(gGlobals.SourceOffset, gGlobals.DestOffset, gGlobals.DataSize);
      break;

    case (BIOSGUARD_PACKAGE_HD2ROM):
      if ((mOutputHeaderFilePtr = fopen (gGlobals.OutputFileName, "wb")) == NULL) {
        printf ("Create output file (%s) fail.\n",gGlobals.OutputFileName);
        return STATUS_ERROR;
      }
      Status = CreateFwBGUP(gGlobals.DestOffset, gGlobals.DataSize, 2);
      break;    

    default:
      printf ("Invalid Parameter: gGlobals.PackageMode = %x \n", gGlobals.PackageMode);
      return STATUS_ERROR;
  }
  fclose (mOutputHeaderFilePtr);
  return Status;
}

/**
 Internal function to replace any white-space character (0x09 !V 0x0D or 0x20) to space (0x20).


 @retval None

**/
STATIC
VOID
ReplaceSpaceToRealSpace (
  IN OUT  char    *Line
  )
{
  char      *Str;

  Str = NULL;
  
  if (Line == NULL) {
    return;
  }

  Str = Line;
  while (*Str != 0x00) {
    if (isspace (*Str)) {
      *Str = 0x20;
    }
    Str++;
  }

  return;
}

/**
 Remove the all of the space characters which are from the start of line to the first
 alphanumeric or punctuation character. This function also changes all of characters
 which aren't  alphanumeric or punctuation characters.

 @param [in, out] Line          On input the char buffer need to remove unnecessary space characters.
                                On output the char buffer has been removed unnecessary space characters.

 @retval None

**/
VOID
RemoveSpace (
  IN OUT char         *Line
)
{
  char     *Start;
  char     *End;
  char     *Ptr;

  Start = NULL;
  End = NULL;
  Ptr = NULL;

  Start = Line;
  if (Line[0] == '\0' || Line[0] == '\n') {
    return;
  }

  Ptr = Line;
  while (*Ptr != '\0' && *Ptr != '\n' && *Ptr != 0x20) {
    if (!isgraph (*Ptr)) {
      *Ptr = 0x20;
    }
    Ptr++;
  }

  //
  // skip the unnecessary characters in the front of buffer
  //
  while (*Start == 0x20) {
    Start++;
  }

  //
  // Find last character in the buffer.
  //
  End = Start;
  while (*(End + 1) != '\0' && *(End + 1) != '\n' && *(End + 1) != ';') {
    End++;
  }
  //
  // Remove unnecessary space in the end of buffer
  //
  while (End != Start && *End == ' ') {
    End --;
  }

  if (End == Start) {
    Line[0] = '\n';
    return;
  }
  *(End + 1) = '\0';

  //
  // Remove unnecessary space in the buffer
  //
  Ptr = Line;
  if (Ptr != Start) {
    do {
      *Ptr =  *Start;
      Start ++;
      Ptr ++;
    } while (Start <= (End + 1));
  }
}

UINTN
CharToHex (
  char       Data
)
{
  if (isdigit (Data)) {
    return Data - '0';
  } else if (islower (Data)) {
    return Data - 'a' + 10;
  } else if (isupper (Data)) {
    return Data - 'A' + 10;
  }
  return 0;
}

VOID
HexStrToValue (
  IN  char        *Buffer,
  OUT UINTN       *Output
)
{
  char      *Ptr;
  UINTN     Number;

  Ptr = NULL;
  Number =0;

  Ptr = Buffer;
  if ((*Ptr != '0') ||
      (*(Ptr + 1) != 'x' && *(Ptr + 1) != 'X')) {
    return;
  }
  Ptr += 2;
  Number = 0;
  while (isalnum (*Ptr)) {
    Number = Number * 0x10 + CharToHex (*Ptr);
    Ptr++;
  }
  *Output = Number;
  return;
}

UINTN
CharToDec (
  char Data
  )
{
  if (isdigit (Data)) {
    return Data - '0';
  }
  return 0;
}

VOID
DecStrToValue (
  IN  char        *Buffer,
  OUT UINTN       *Output
)
{
  char      *Ptr;
  UINTN     Number;
  
  Ptr = NULL;
  Number =0;

  Ptr = Buffer;
  while (isspace (*Ptr)) {
    Ptr++;
  }
  Number = 0;
  while (isalnum (*Ptr)) {
    Number = Number * 10 + CharToDec (*Ptr);
    Ptr++;
  }
  *Output = Number;
  return;
}

/**
 Parsing input config file and save the setting from this file to private
 data.

 @param [in]   ConfigFile       Ascii string saves configuration file name.
 @param [in]   BgupHeader
 @param [in]   InsydeBiosGuardHeader

 @retval EFI_SUCCESS            The function completed successfully.
 @retval EFI_INVALID_PARAMETER  One of the input parameters was invalid or one of the parameters in the text file was invalid.
 @retval EFI_OUT_OF_RESOURCES   Unable to allocate memory.
 @retval EFI_ABORTED            Unable to open/create a file or a misc error.

**/
UINT8
ProcessIniFile (
  IN  char                       *ConfigFile,
  IN  BGUP_HEADER                *BgupHeader
  )
{
  char                      Line[500] = {0};
  FILE                      *SettingFile;
  UINT8                     Status;
  int                       Index;
  static const INT8         *Symbols[] = {
    "PlatId",                    // PlatformIdSection
    "FileName",                  // ImageFileNameSection
    "BiosGuardScriptFile",       // BGSLBinFileNameSection
    "PackageMode",               // PackageMode
    "EcSize",                    // EcSizeSection
    "EcOffset",                  // EcOffsetSection
    "FwSize",                    // FwSizeSection
    "FwOffset",                  // FwOffsetSection
    "ChasmFallsType",            // ChasmFallsType
    "PbbOffset",                 // PbbOffset
    "PbbSize",                   // PbbSize
    "SFAM_CHECK",                // UPDPKG_ATTR[0] SFAM_CHECK
    "PROT_EC_OPR",               // UPDPKG_ATTR[1] PROT_EC_OPR
    "GFX_MITIGATION_DISABLE",    // UPDPKG_ATTR[2] GFX_MITIGATION_DISABLE
    "FAULT_TOLERANT_UPDATE",     // UPDPKG_ATTR[3] FAULT_TOLERANT_UPDATE
    "BiosSvn",                   // BiosSvnSection
    "EcSvn",                     // EcSvnSection
    "VendorSpecific",            // VendorSpecificSection
    NULL
  };

  SettingFile    = NULL;
  Status         = STATUS_SUCCESS;
  Index          = 0;

  if (ConfigFile == NULL) {
    return STATUS_ERROR;
  }

  //
  // Open config file and set file position to beginning of file.
  //
  SettingFile = fopen(ConfigFile, "r");
  if (SettingFile == NULL) {
    printf ("Invalid Parameter: Cannot open project setting file.\n");
    printf ("%s not found.\n", ConfigFile);
    return STATUS_ERROR;
  }

  if (fseek (SettingFile, 0, SEEK_SET) != 0) {
    printf ("Access %s failed\n", ConfigFile);
    fclose (SettingFile);
    return STATUS_ERROR;
  }

  do {
    if (fgets (Line, sizeof (Line), SettingFile) == NULL) {
      break;
    }
    RemoveSpace (Line);
    if (Line [0] == '\n' || Line[0] == ';') {
      continue;
    }
    ReplaceSpaceToRealSpace (Line);
    for (Index = 0; Symbols[Index] != NULL; Index++) {
      if (strstr (Line, Symbols[Index])) {
        break;
      }
    }
    if (Index < MaxSection) {
      Status = ProcessSection (Index, Line, BgupHeader);
      if (Status != STATUS_SUCCESS){
        break;
      }  
    }
  } while (!feof(SettingFile));

  fclose (SettingFile);
  return Status;
}

UINT8
ProcessSection (
  IN  BIOSGUARD_SECTION_TYPE     SectionType,
  IN  char                       *Line,
  IN  BGUP_HEADER                *BgupHeader
  )
{
  char                      *Ptr;
  UINTN                     HexData;
  UINTN                     DecData;

  Ptr = NULL;
  HexData = 0;
  DecData = 0;
  //
  // Get string after "=".
  //
  Ptr = strstr (Line, "=") + sizeof(char);
  RemoveSpace (Ptr);
  switch (SectionType) {
  case FileName:
    if (gGlobals.FileName == 0){
      gGlobals.FileName = (INT8 *) malloc (strlen(Ptr) + 1);
      memset (gGlobals.FileName, 0x0, strlen(Ptr)+ 1);
      memcpy (gGlobals.FileName, Ptr, strlen(Ptr));
    }
    break;
  case BGSLFile:
    if (gGlobals.BgslFileName == 0){
      gGlobals.BgslFileName = (INT8 *) malloc (strlen(Ptr) + 1);
      memset (gGlobals.BgslFileName, 0x0, strlen(Ptr) + 1);
      memcpy (gGlobals.BgslFileName, Ptr, strlen(Ptr));
    }
    break;
  case PlatformId:
    memcpy (BgupHeader->PLAT_ID, Ptr, strlen(Ptr));
    break;
  case EcSizeSection:
    HexStrToValue (Ptr, &gGlobals.EcSize);
    break;
  case EcOffsetSection:
    HexStrToValue (Ptr, &gGlobals.EcOffset);
    break;
  case FwSizeSection:
    HexStrToValue (Ptr, &gGlobals.FwSize);
    break;
  case FwOffsetSection:
    HexStrToValue (Ptr, &gGlobals.FwOffset);
    break;
  case PackageMode:
    if (gGlobals.PackageMode == 0) {
      HexStrToValue (Ptr, &HexData);
      gGlobals.PackageMode = (UINT8)HexData;
    }
    break;
  case ChasmFallsType:
    HexStrToValue (Ptr, &HexData);
    gGlobals.ChasmFallsType = (UINT8)HexData;
    break;
  case PbbOffsetSection:
    HexStrToValue (Ptr, &gGlobals.PbbOffset);
    break;
  case PbbSizeSection:
    HexStrToValue (Ptr, &gGlobals.PbbSize);
    break;
  case SFAM_CHECK:
      DecStrToValue (Ptr, &DecData);
      if (DecData < 2) {
        mBgupHeader->UPDPKG_ATTR &= (UINT16)DecData << 0;
      } else {
         printf ("Invalid Parameter: SFAM_CHECK must be 0 or 1.\n");
         return STATUS_ERROR;
      }
    break;
  case PROT_EC_OPR:
      DecStrToValue (Ptr, &DecData);
      if (DecData < 2) {
        mBgupHeader->UPDPKG_ATTR |= (UINT16)DecData << 1;
      } else {
         printf ("Invalid Parameter: PROT_EC_OPR must be 0 or 1.\n");
         return STATUS_ERROR;
      }
    break;    
  case GFX_MITIGATION_DISABLE:
      DecStrToValue (Ptr, &DecData);
      if (DecData < 2) {
        mBgupHeader->UPDPKG_ATTR |= (UINT16)DecData << 2;
      } else {
         printf ("Invalid Parameter: GFX_MITIGATION_DISABLE must be 0 or 1.\n");
         return STATUS_ERROR;
      }
    break;
  case FTU:
      DecStrToValue (Ptr, &DecData);
      if (DecData < 2) {
        mBgupHeader->UPDPKG_ATTR |= (UINT16)DecData << 3;
      } else {
         printf ("Invalid Parameter: FAULT_TOLERANT_UPDATE must be 0 or 1.\n");
         return STATUS_ERROR;
      }
    break;
  case BiosSvnSection:
    HexStrToValue (Ptr, &HexData);
    mBgupHeader->BIOS_SVN = (UINT32)HexData;
    break;
  case EcSvnSection:
    HexStrToValue (Ptr, &HexData);
    mBgupHeader->EC_SVN = (UINT32)HexData;
    break;
  case VendorSpecificSection:
    HexStrToValue (Ptr, &HexData);
    mBgupHeader->VEND_SPECIFIC = (UINT32)HexData;
    break;
  default:
    printf ("Invalid Parameter: Unsupport Section.\n");
    return STATUS_ERROR;
    break;
  }

  return STATUS_SUCCESS;
}

UINT8
ReadFileToArray (
  IN      char                   *FileName,
  IN OUT  UINT8                  **Binary,
  IN OUT  UINT32                 *BinarySize
  )
{
  FILE                           *FilePtr;
  UINT8                          *Data;
  UINT8                          Status;
  UINT32                         FileSize;
  UINT32                         Index;

  FilePtr  = NULL;
  Data     = NULL;
  Status   = STATUS_SUCCESS;
  FileSize = 0;
  Index    = 0;
  
  if (FileName == NULL) {
    return STATUS_ERROR;
  }
  //
  // Open cofnig file and set file position to beginning of file.
  //
  FilePtr = fopen(FileName, "rb");
  if (FilePtr == NULL) {
    printf ("Invalid Parameter: Cannot open %s file.\n", FileName);
    return STATUS_ERROR;
  }
  
  //
  //  Get file size and 
  //
  fseek(FilePtr, 0, SEEK_END);      // seek to end of file
  FileSize = ftell(FilePtr);        // get current file pointer
  *BinarySize = FileSize;

  Data = (UINT8 *) malloc (FileSize);
  *Binary = Data;

  if (fseek (FilePtr, 0, SEEK_SET) != 0) {
    printf ("Access %s failed\n", FileName);
    fclose (FilePtr);
    return STATUS_ERROR;
  }

  for (Index = 0; Index < FileSize; Index ++,Data++) {
    fread(Data, 1, 1, FilePtr);
  }
  
  fclose (FilePtr); 
  return Status;
}

STATIC
BOOLEAN
EFIAPI
IsFdmHeaderValid (
  IN H2O_FLASH_DEVICE_MAP_HEADER    *FdmHeader
 ){

  UINT32 Index;
  UINT8  Sum;
  UINT8  *Data;

  Sum = 0;
  Data = (UINT8*) FdmHeader;

  //
  // Verify check sum
  //
  for (Index = 0; Index < sizeof (H2O_FLASH_DEVICE_MAP_HEADER); Index++, Data++){
    Sum += (*Data);
  }

  return (Sum == 0);

}

int
CompareFdmEntries (
  IN const VOID  *Arg1,
  IN const VOID  *Arg2
  )
/*++

Routine Description:

    This function is used by qsort to sort the FDM entries based upon Region
    Offset in their decresing order.

Arguments:

    Arg1  -   Pointer to Arg1
    Arg2  -   Pointer to Arg2

Returns:

    None

-*/
{
  if ((((H2O_FLASH_DEVICE_MAP_ENTRY *) Arg1)->RegionOffset) < (((H2O_FLASH_DEVICE_MAP_ENTRY *) Arg2)->RegionOffset)) {
    return -1;
  } else if ((((H2O_FLASH_DEVICE_MAP_ENTRY *) Arg1)->RegionOffset) > (((H2O_FLASH_DEVICE_MAP_ENTRY *) Arg2)->RegionOffset)) {
    return 1;
  } else {
    return 0;
  }
}

UINT8
GetFdmDataFromFd (
  IN UINT8                       *FdBuffer,
  IN UINT32                       ImageSize,
  OUT UINT32                     *CalculateBiosSize
  )
/*++

Routine Description:

  Get FDM data from FD

Arguments:

  FdBuffer     - Fd file buffer.
  FdFileSize   - Fd file size.

Returns:

  STATUS_SUCCESS - 
  STATUS_ERROR   - 

-*/
{
  H2O_FLASH_DEVICE_MAP_HEADER *FdmPtr;
  H2O_FLASH_DEVICE_MAP_ENTRY  *FdmEntryDataPtr;
  BOOLEAN                     FoundMatched;
  UINT32                      Index;
  UINT32                      Offset;
  H2O_FLASH_DEVICE_MAP_ENTRY  *FdmEntries;
  UINT32                      BiosSize;

  FdmPtr          = NULL;
  FdmEntryDataPtr = NULL;
  FoundMatched    = FALSE;
  mFdmEntryCount  = 0;
  FdmEntries      = NULL;
  BiosSize        = 0;

  FdmPtr = (H2O_FLASH_DEVICE_MAP_HEADER *)(UINT8 *)FdBuffer;
  for (; (UINT8 *)FdmPtr < (UINT8 *)FdBuffer + ImageSize; FdmPtr = (H2O_FLASH_DEVICE_MAP_HEADER *)((UINT8 *)FdmPtr + 0x1000)) {
    if (!memcmp (FdmPtr, mSignature, sizeof(mSignature))) {
      if (IsFdmHeaderValid ((H2O_FLASH_DEVICE_MAP_HEADER *) FdmPtr)) {
        FoundMatched = TRUE;
        break;
      }
    }
  }
  
  if (FoundMatched) {
    //
    // Get Number of Entries
    //
    mFdmEntryCount = (FdmPtr->Size - FdmPtr->Offset) / FdmPtr->EntrySize;
    
    //
    // Sort Entries by offset
    //
    FdmEntries = malloc(mFdmEntryCount * FdmPtr->EntrySize);

    memcpy (FdmEntries, ((UINT8 *)FdmPtr + FdmPtr->Offset), (FdmPtr->Size - FdmPtr->Offset));

    qsort(FdmEntries, mFdmEntryCount, FdmPtr->EntrySize, CompareFdmEntries);

    mBiosRegionMap = malloc(sizeof(RegionMap)* mFdmEntryCount);
    for (Index = 0; Index < mFdmEntryCount; Index++) {
      Offset = Index * (FdmPtr->EntrySize);
      FdmEntryDataPtr = (H2O_FLASH_DEVICE_MAP_ENTRY *)((UINT8 *)FdmEntries + Offset);
      mBiosRegionMap[Index].BeginOffset = (UINT32)(FdmEntryDataPtr->RegionOffset);
      mBiosRegionMap[Index].EndOffset   = (UINT32)((FdmEntryDataPtr->RegionOffset)+(FdmEntryDataPtr->RegionSize));
      mBiosRegionMap[Index].Size        = (UINT32)(FdmEntryDataPtr->RegionSize);
//      printf("%08x - %08x (%08x)\n",mRegionMap[Index].BeginOffset, mRegionMap[Index].EndOffset, mRegionMap[Index].Size);
    }
    *CalculateBiosSize = mBiosRegionMap[mFdmEntryCount-1].EndOffset;
    return STATUS_SUCCESS;
  }

  // 
  // Cannot find the FDM, return ERROR
  //
  printf("Cannot find the FDM, return ERROR\n");
  return STATUS_ERROR;
}

VOID
GetSPIRegionMap (
  IN UINT8                       *FdBuffer,
  IN UINT32                       ImageSize
  )
{
  UINT32  *Ptr32;
  UINTN   FDBAR;
  UINTN   FRBA;
  UINT32  Index;
  UINTN   RegionTableSize;
  UINT8   RegionType;

  DESCRIPTOR_REGION_LIST RegionTable[] = {{  EC_REGION, R_DESCRIPTOR_FRBA_FLREG8_EC},
                                          { GBE_REGION, R_DESCRIPTOR_FRBA_FLREG3_GBE},
                                          { CSE_REGION, R_DESCRIPTOR_FRBA_FLREG2_CSE},
                                          {BIOS_REGION, R_DESCRIPTOR_FRBA_FLREG1_BIOS}};
  
  mSPIRegionMap = malloc(sizeof(RegionMap) * (REGION_MAX+1));
  memset (mSPIRegionMap, 0x00, (sizeof(RegionMap) * (REGION_MAX+1)));

  RegionTableSize = sizeof (RegionTable) / sizeof(DESCRIPTOR_REGION_LIST);
  FDBAR       = (UINTN) FdBuffer;
  Ptr32       = (UINT32 *) (UINTN) (FDBAR + R_DESCRIPTOR_FDBAR_FLMAP0);
  FRBA        = FDBAR + (((*Ptr32 & B_DESCRIPTOR_FDBAR_FLMAP0_FRBA) >> N_DESCRIPTOR_FDBAR_FLMAP0_FRBA) << 4);

  // Descriptor Region
  mSPIRegionMap[DESCRIPTOR_REGION].BeginOffset = 0;
  mSPIRegionMap[DESCRIPTOR_REGION].EndOffset = (0xFFF + 1) ;
  mSPIRegionMap[DESCRIPTOR_REGION].Size = mSPIRegionMap[DESCRIPTOR_REGION].EndOffset - mSPIRegionMap[DESCRIPTOR_REGION].BeginOffset;

  for (Index = 0; Index < RegionTableSize; Index++) {
    RegionType = RegionTable[Index].RegionType;
    Ptr32       = (UINT32 *)(UINTN)(FRBA + RegionTable[Index].FRBA_Offset);  
    if (*Ptr32 != B_DESCRIPTOR_FRBA_FLREG1_REGION_BASE) {
      mSPIRegionMap[RegionType].BeginOffset = ( *Ptr32 & B_DESCRIPTOR_FRBA_FLREG1_REGION_BASE ) << 0xC;
      mSPIRegionMap[RegionType].EndOffset = ((( *Ptr32 & B_DESCRIPTOR_FRBA_FLREG1_REGION_LIMIT ) >> 0x10) + 1) << 0xC;
    }
    mSPIRegionMap[RegionType].Size = mSPIRegionMap[RegionType].EndOffset - mSPIRegionMap[RegionType].BeginOffset;
  }
  
  Index = BIOS_REGION + 1;
  RegionType = 1;
  while (RegionType != BIOS_REGION) {
    if (mSPIRegionMap[RegionType].Size != 0) {
      mSPIRegionMap[Index].BeginOffset = mSPIRegionMap[RegionType].EndOffset;
     
    }
    if (mSPIRegionMap[RegionType + 1].Size != 0) {
      mSPIRegionMap[Index].EndOffset = mSPIRegionMap[RegionType + 1].BeginOffset;
    }

    if ((mSPIRegionMap[Index].BeginOffset != 0) && (mSPIRegionMap[Index].EndOffset != 0)) {
      if (mSPIRegionMap[Index].BeginOffset == mSPIRegionMap[Index].EndOffset){
        mSPIRegionMap[Index].BeginOffset = 0;
        mSPIRegionMap[Index].EndOffset   = 0;
      }
      if (mSPIRegionMap[Index].EndOffset > mSPIRegionMap[Index].BeginOffset) {
        mSPIRegionMap[Index].Size = mSPIRegionMap[Index].EndOffset - mSPIRegionMap[Index].BeginOffset;
        if (mSPIRegionMap[Index].Size != 0) { 
          Index++;
        }
      }
    }
    RegionType++;
  }

  // For the data which were located behind BIOS region 
  if (mSPIRegionMap[BIOS_REGION].EndOffset != ImageSize){
    mSPIRegionMap[REGION_MAX].BeginOffset = mSPIRegionMap[BIOS_REGION].EndOffset;
    mSPIRegionMap[REGION_MAX].EndOffset   = ImageSize;
    mSPIRegionMap[REGION_MAX].Size        = mSPIRegionMap[REGION_MAX].EndOffset - mSPIRegionMap[REGION_MAX].BeginOffset;
  }

  for (Index = 0; Index <= REGION_MAX; Index++) {
//    printf("[%d] %08x - %08x (%08x)\n",Index,mSPIRegionMap[Index].BeginOffset, mSPIRegionMap[Index].EndOffset, mSPIRegionMap[Index].Size);
  }
  
}
