/** @file
    Implement the test application for status code to RAM.

***************************************************************************
* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
*
* You may not reproduce, distribute, publish, display, perform, modify, adapt,
* transmit, broadcast, present, recite, release, license or otherwise exploit
* any part of this publication in any form, by any means, without the prior
* written permission of Insyde Software Corporation.
*
******************************************************************************
*/

#include <Uefi.h>
#include <Pi/PiStatusCode.h>
#include <Guid/MemoryStatusCodeRecord.h>
#include <Library/BaseLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/PrintLib.h>


RUNTIME_MEMORY_STATUSCODE_HEADER  *mRtMemoryStatusCodeTable;

/**
  Test application for status code to RAM.

  @param[in]  ImageHandle     The image handle.
  @param[in]  SystemTable     The system table.

  @retval EFI_SUCCESS            A configuration table matching TableGuid was found.
  @retval EFI_NOT_FOUND          A configuration table matching TableGuid could not be found. .
**/
EFI_STATUS
EFIAPI
UefiMain (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS                            Status;
  MEMORY_STATUSCODE_RECORD              *Record;
  UINTN                                 Index;
  UINTN                                 TempRecordIndex;


  Print(L"StatusCodeHandlerTestApp: Start.\n");

  Status = EfiGetSystemConfigurationTable (&gMemoryStatusCodeRecordGuid, (VOID **)&mRtMemoryStatusCodeTable);
  if (EFI_ERROR (Status)) {
    Print(L"EfiGetSystemConfigurationTable for gMemoryStatusCodeRecordGuid! Status = %r\n", Status);
    return Status;
  }
  Print(L"mRtMemoryStatusCodeTable address: 0x%X\n", mRtMemoryStatusCodeTable);
  Print(L"mRtMemoryStatusCodeTable->RecordIndex = %d\n", mRtMemoryStatusCodeTable->RecordIndex);
  Print(L"mRtMemoryStatusCodeTable->NumberOfRecords = %d\n", mRtMemoryStatusCodeTable->NumberOfRecords);
  Print(L"mRtMemoryStatusCodeTable->MaxRecordsNumber = %d\n", mRtMemoryStatusCodeTable->MaxRecordsNumber);
  TempRecordIndex = mRtMemoryStatusCodeTable->RecordIndex;
  Print(L"TempRecordIndex = %d\n", TempRecordIndex);

  if (mRtMemoryStatusCodeTable != NULL) {
    Record = (MEMORY_STATUSCODE_RECORD *) (mRtMemoryStatusCodeTable + 1);
    for (Index = 0; Index < TempRecordIndex; Index++) {
      Print(L"App[%d] CodeType 0x%X V%08X I%X\n", Index + 1, Record->CodeType, Record->Value, Record->Instance);
    }
  }
  Print(L"StatusCodeHandlerTestApp: End. Status = %r\n", Status);

  return Status;
}