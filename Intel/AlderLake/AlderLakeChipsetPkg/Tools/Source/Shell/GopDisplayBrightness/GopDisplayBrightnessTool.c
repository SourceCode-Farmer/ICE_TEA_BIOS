/** @file
  GopDisplayBrightness test tool to try GopDisplayBrightness functions
 
;***************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Protocol/GopDisplayBrightness.h>
#include <Library/UefiLib.h>

GOP_DISPLAY_BRIGHTNESS_PROTOCOL   *mGopDisplayBrightness = NULL;

/**
  The routin gets the max brightness level.

  @param[OUT] Level         The brightness level.  
  
  @retval EFI_SUCCESS       Get max brightness level successfully.
  @retval other             Some error occurs when executing this function.

**/
EFI_STATUS
GetMaxLevel (
  OUT UINT32   *Level
)
{
  return mGopDisplayBrightness->GetMaxBrightnessLevel (mGopDisplayBrightness, Level);
}

/**
  The routin gets the current brightness level.

  @param[OUT] Level         The brightness level.  
  
  @retval EFI_SUCCESS       Get current brightness level successfully.
  @retval other             Some error occurs when executing this function.

**/
EFI_STATUS
GetCurrentLevel (
  OUT UINT32   *Level
)
{
  return mGopDisplayBrightness->GetCurrentBrightnessLevel (mGopDisplayBrightness, Level);
}

/**
  The routin sets the brightness level.

  @param[IN] Level         The brightness level.  
  
  @retval EFI_SUCCESS       Set the brightness level successfully.
  @retval other             Some error occurs when executing this function.

**/
EFI_STATUS
SetLevel (
  IN UINT32  *Level
)
{
  return mGopDisplayBrightness->SetBrightnessLevel (mGopDisplayBrightness, *Level);
}


/**
  The user Entry Point for Application. The user code starts with this function
  as the real entry point for the application.

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.  
  @param[in] SystemTable    A pointer to the EFI System Table.
  
  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurs when executing this entry point.

**/
EFI_STATUS
EFIAPI
GopDisplayBrightnessToolEntry (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS             Status;
  BOOLEAN                ExitThisTool;
  EFI_INPUT_KEY          Key;
  UINT32                 BrightnessLevel;
  UINTN                  KeyEvent;
  UINT32                 MaxLevel;
  UINT32                 MinLevel;

  Status = EFI_SUCCESS;
  ExitThisTool = FALSE;
  BrightnessLevel = 0;
  KeyEvent = 0;
  MaxLevel = 0;
  MinLevel = 0;

  Status = gBS->LocateProtocol (&gGopDisplayBrightnessProtocoGuid, NULL, (VOID **)&mGopDisplayBrightness);
  if (EFI_ERROR (Status)) {
    Print (L"Locate GopDisplayBrightnessProtocol failed.\n");
    return Status;
  }
  GetMaxLevel (&MaxLevel);
  
  gST->ConOut->ClearScreen (gST->ConOut);
  Print (L"Select the test function:\n");
  Print (L"[1] Get the max brightness level.\n");
  Print (L"[2] Get the current brightness level.\n");
  Print (L"[3] Incress 10 brightness level.\n");
  Print (L"[4] Decrease 10 brightness level.\n");
  Print (L"[+] Incress 1 brightness level.\n");
  Print (L"[-] Decrease 1 brightness level.\n");
  Print (L"[Q] Exit this program.\n\n");
  
  do {
    gBS->WaitForEvent (1, &gST->ConIn->WaitForKey, &KeyEvent);
    gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);
    gST->ConOut->SetCursorPosition (gST->ConOut, 0, 9);
    Print (L"                                                                               \n");
    Print (L"                                                                               \n");
    gST->ConOut->SetCursorPosition (gST->ConOut, 0, 9);
   
    switch (Key.UnicodeChar) {

    case '1':
      Status = GetMaxLevel (&BrightnessLevel);
      if (!EFI_ERROR (Status)) {
        Print (L"Max brightness level is %d.\n", BrightnessLevel);
      } else {
        Print (L"Get max brightness level failed!\n");
      }
      break;

    case '2':
      Status = GetCurrentLevel (&BrightnessLevel);
      if (!EFI_ERROR (Status)) {
        Print (L"Current brightness level is %d.\n", BrightnessLevel);
      } else {
        Print (L"Get current brightness level failed!\n");
      }
      break;
      
    case '3':
      Status = GetCurrentLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Get current level failed when trying to add brightness level!\n");
        break;
      }
      if (BrightnessLevel == MaxLevel) {
        Print (L"Current brightness level is max.\n");
        break;
      }
      if ((BrightnessLevel + 10) > MaxLevel) {
        Print (L"The settign is out of range!\n");
        break;
      }
      BrightnessLevel += 10;
      Status = SetLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Set brightness level failed!\n");
        break;
      }
      Print (L"Add brightness level successfully.\n");
      Status = GetCurrentLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Get currnet level failed after add level!\n");
        break;
      }       
      Print (L"Current brightness level is %d.\n", BrightnessLevel);
      break;

    case '4':
      Status = GetCurrentLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Get current level failed when trying to decrease brightness level!\n");
        break;
      }
      if (BrightnessLevel == MinLevel) {
        Print (L"Current brightness level is minimum.\n");
        break;
      }
      if (BrightnessLevel < (MinLevel + 10)) {
        Print (L"The settign is out of range!\n");
        break;
      }
      BrightnessLevel -= 10;
      Status = SetLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Set brightness level failed!\n");
        break;
      }
      Print (L"Decrease brightness level successfully.\n");
      Status = GetCurrentLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Get currnet level failed after add level!\n");
        break;
      }       
      Print (L"Current brightness level is %d.\n", BrightnessLevel);
      break;

    case '+':
      Status = GetCurrentLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Get current level failed when trying to add brightness level!\n");
        break;
      }
      if (BrightnessLevel == MaxLevel) {
        Print (L"Current brightness level is max.\n");
        break;
      }
      BrightnessLevel++;
      Status = SetLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Set brightness level failed!\n");
        break;
      }
      Print (L"Add brightness level successfully.\n");
      Status = GetCurrentLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Get currnet level failed after add level!\n");
        break;
      }
      Print (L"Current brightness level is %d.\n", BrightnessLevel);
      break;

    case '_':
    case '-':
      Status = GetCurrentLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Get current level failed when trying to decrease brightness level!\n");
        break;
      }
      if (BrightnessLevel == MinLevel) {
        Print (L"Current brightness level is minimum level.\n");
        break;
      }
      BrightnessLevel--;
      Status = SetLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Set brightness level failed!\n");
        break;
      }
      Print (L"Decrease brightness level successfully.\n");
      Status = GetCurrentLevel (&BrightnessLevel);
      if (EFI_ERROR (Status)) {
        Print (L"Get currnet level failed after add level!\n");
        break;
      }
      Print (L"Current brightness level is %d.\n", BrightnessLevel);
      break;

    case 'Q':
    case 'q':
      ExitThisTool = TRUE;
      Print (L"Exit the tool.\n");

    default:
      Print (L"Wrong input.\n");
    }
  } while (!ExitThisTool);

  gST->ConOut->ClearScreen (gST->ConOut);

  return Status;
}
