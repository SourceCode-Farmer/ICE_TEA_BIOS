/** @file
  Dump gpio information by matrix and specific type

;***************************************************************************
;* Copyright (c) 2017 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <GpioShellTool.h>

SHELL_PARAM_ITEM gpioshelltoolParamList[] = {
  {L"/b", TypeValue},
  {L"/m", TypeValue},
  {L"/?", TypeFlag},
  {NULL, TypeMax},
};

#define PCH_LP_MAX ICL_PCH_LP_GPIO_GPP_A_PAD_MAX +\
                   ICL_PCH_LP_GPIO_GPP_B_PAD_MAX +\
                   ICL_PCH_LP_GPIO_GPP_C_PAD_MAX +\
                   ICL_PCH_LP_GPIO_GPP_D_PAD_MAX +\
                   ICL_PCH_LP_GPIO_GPP_E_PAD_MAX +\
                   ICL_PCH_LP_GPIO_GPP_F_PAD_MAX +\
                   ICL_PCH_LP_GPIO_GPP_G_PAD_MAX +\
                   ICL_PCH_LP_GPIO_GPP_H_PAD_MAX +\
                   ICL_PCH_LP_GPIO_GPP_R_PAD_MAX +\
                   ICL_PCH_LP_GPIO_GPP_S_PAD_MAX +\
                   ICL_PCH_LP_GPIO_GPD_PAD_MAX 
//                   ICL_PCH_LP_GPIO_VGPIO_PAD_MAX +\
//                   ICL_PCH_LP_GPIO_SPI_PAD_MAX +\
//                   ICL_PCH_LP_GPIO_CPU_PAD_MAX +\
//                   ICL_PCH_LP_GPIO_JTAG_PAD_MAX +\
//                   ICL_PCH_LP_GPIO_HVMOS_PAD_MAX

#define PCH_H_MAX  ICL_PCH_H_GPIO_GPP_A_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_B_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_C_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_D_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_E_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_F_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_G_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_H_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_I_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_J_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_K_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_R_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPP_S_PAD_MAX +\
                   ICL_PCH_H_GPIO_GPD_PAD_MAX
//                   ICL_PCH_H_GPIO_VGPIO_PAD_MAX +\
//                   ICL_PCH_H_GPIO_VGPIO_0_PAD_MAX +\
//                   ICL_PCH_H_GPIO_VGPIO_3_PAD_MAX +\
//                   ICL_PCH_H_GPIO_CPU_PAD_MAX +\
//                   ICL_PCH_H_GPIO_JTAG_PAD_MAX +\
//                   ICL_PCH_H_GPIO_JTAG_PAD_MAX


/**
  The user Entry Point for Application. The user code starts with this function
  as the real entry point for the application.

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.
  @param[in] SystemTable    A pointer to the EFI System Table.

  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurs when executing this entry point.

**/
EFI_STATUS
EFIAPI
GpioShellToolEntry (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS     Status;
  EFI_HII_HANDLE mHiiHandle;
  LIST_ENTRY     *ParamPackage;
  H2O_BOARD_ID   BoardId;
  BOOLEAN        NoCommand;

  NoCommand = TRUE;
  mHiiHandle = HiiAddPackages (&gEfiCallerIdGuid, ImageHandle, GpioShellToolStrings, NULL);
  Status = ShellCommandLineParseEx (gpioshelltoolParamList, &ParamPackage, NULL, TRUE, FALSE);
  if (EFI_ERROR (Status)) {
    ShellPrintHiiEx (-1, -1, NULL, STRING_TOKEN (STR_TOOLS_INVALID_INPUT), mHiiHandle);
    goto ON_EXIT;
  }

  if (ShellCommandLineGetFlag (ParamPackage, L"/?")) {
    ShellPrintHiiEx (-1, -1, NULL, STRING_TOKEN (STR_TOOLS_HELP), mHiiHandle);
    NoCommand = FALSE;
    goto ON_EXIT;
  }
  if (ShellCommandLineGetFlag (ParamPackage ,L"/b")) {
    BoardId = PcdGet64 (PcdH2OBoardId);
    Print (L"Board ID = %x\n", BoardId);
    NoCommand = FALSE;
    goto ON_EXIT;
  }
  if (ShellCommandLineGetFlag (ParamPackage ,L"/m")) {
    Status = ReadGpioInformation (PchSeries ());
    if (EFI_ERROR (Status)) {
      ShellPrintHiiEx (-1, -1, NULL, STRING_TOKEN (STR_TOOLS_UNSUPPORT), mHiiHandle);
    }
    NoCommand = FALSE;
    goto ON_EXIT;
  }
  if (NoCommand) {
    ShellPrintHiiEx (-1, -1, NULL, STRING_TOKEN (STR_TOOLS_INVALID_INPUT), mHiiHandle);
    goto ON_EXIT;
  }

ON_EXIT:
  ShellCommandLineFreeVarList (ParamPackage);
  HiiRemovePackages (mHiiHandle);
  return Status;
}

//GPIO_PAD
//RevertGpioPad (
//  UINT32                    GroupOffset,
//  UINT32                    PadNumber
//)
//{
//  GPIO_GROUP                LowestGroup;
//
//  LowestGroup = GpioGetLowestGroup ();
//  return ((LowestGroup + GroupOffset) << 16 | PadNumber);
//}

/**
  Read gpio information then transparent content into matrix.
  
  Read gpio information then print content by 571034-icl-pch-lp-eds-v2of2-rev1p0.pdf

  @param[in] PchSeries

  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurs when executing this entry point.

**/
EFI_STATUS
ReadGpioInformation (
  IN PCH_SERIES             PchSeries
  )
{
  UINT16                      Index;
  UINT8                       PadIndex;
  GPIO_GROUP_INFO_SHELL_TOOL  *GpioGroupInfo;
  UINT32                      GpioGroupInfoLength;
  GPIO_DATA                   *GpioData;
  UINT8                       GroupIndex;
  UINT16                      PadMaxSize;
  CHAR16                      *PchSeriesChar;
//  UINT8                     GroupIndexOthers;

//  UINT32                    GpioTableIndex;
//  UINT32                    PadNumber;
//  UINT32                    HostSW_OwnIndex;
//  UINT32                    GpiGpeEnIndex;
//  UINT32                    GpioTableIndexCount;
//  BOOLEAN                   Dw0;
//  GPIO_GROUP_INFO           *GpioGroupInfo;
//  UINTN                     GpioGroupInfoLength;
//  UINT32                    NumberOfGroups;
//  UINT32                    NumberOfGroupsIndex;
//  GPIO_PAD                  GpioTablePad;
//  UINT32                    GpioToLockIndex;
//  UINT32                    GpioToLockTxIndex;

  DEBUG ((DEBUG_INFO, "ReadGpioInformation Start\n"));

  switch (PchSeries) {
  case PCH_LP:
    PadMaxSize = PCH_LP_MAX;
    PchSeriesChar = L"PCH-Lp";
//    GroupIndexOthers = 9; // VGPIO
    break;
  case PCH_H:
    PadMaxSize = PCH_H_MAX;
    PchSeriesChar = L"PCH-H";
//    GroupIndexOthers = 12; // VGPIO
    break;
  default:
    DEBUG ((DEBUG_INFO, "PCH_UNKNOWN_SERIES\n"));
    return EFI_UNSUPPORTED;
  }
  GpioData = NULL;
  GpioData = (GPIO_DATA *)AllocateZeroPool (PadMaxSize * sizeof (GPIO_DATA));
  GpioGroupInfo = GpioGetGroupInfoTableShellTool (&GpioGroupInfoLength);
  Index = 0;
  for (GroupIndex = 0; GroupIndex < GpioGroupInfoLength; GroupIndex++) {
    for (PadIndex = 0; PadIndex < GpioGroupInfo[GroupIndex].PadPerGroup; PadIndex++) {
      GpioData[Index].GroupIndex = GroupIndex;
      GpioData[Index].GroupName  = GpioGroupInfo[GroupIndex].GroupName;
      GpioData[Index].PadIndex   = PadIndex;
      GpioData[Index].Address    = PCH_PCR_ADDRESS (GpioGroupInfo[GroupIndex].Community, GpioGroupInfo[GroupIndex].PadCfgOffset + PadIndex*0x10);
      Index++;
    }
  }
  
  for (Index = 0; Index < PadMaxSize; Index++) {
    GpioData[Index].Dw0Value = MmioRead32 (GpioData[Index].Address);
    GpioData[Index].Dw1Value = MmioRead32 (GpioData[Index].Address + 0x4);
  }

  //
  // Print GpioData
  //
  Print (L"ICL GPIO RAW VALUE -- %s\n", PchSeriesChar);
  Print (L"===========================================\n");
  Print (L"PadName  Register    DW0         DW1\n");
  Print (L"-------------------------------------------\n");
  for (Index = 0; Index < PadMaxSize; Index++) {
    Print (L"%5s%02d  0x%08x  0x%08x  0x%08x\n", GpioData[Index].GroupName, GpioData[Index].PadIndex, GpioData[Index].Address, GpioData[Index].Dw0Value, GpioData[Index].Dw1Value);
  }
  
//  GpioTableIndex = 0;
//  GpioTableIndexCount = 0;
//  for (NumberOfGroupsIndex = 0; NumberOfGroupsIndex < NumberOfGroups; NumberOfGroupsIndex ++) {
//    switch (GpioGroupInfo[NumberOfGroupsIndex].Community) {
//    case PID_GPIOCOM0:
//      Community = Community0;
//      break;
//    case PID_GPIOCOM1:
//      Community = Community1;
//      break;
//    case PID_GPIOCOM2:
//      Community = Community2;
//      break;
//    case PID_GPIOCOM3:
//      Community = Community3;
//      break;
//    default:
//      Community = Community0;
//      break;
//    }
//    PadIndex = 0;
//    while (Community[PadIndex].Address != GpioGroupInfo[NumberOfGroupsIndex].PadCfgOffset) {
//      PadIndex++;
//    }
//    HostSW_OwnIndex = 0;
//    while (Community[HostSW_OwnIndex].Address != GpioGroupInfo[NumberOfGroupsIndex].HostOwnOffset) {
//      HostSW_OwnIndex++;
//    }
//    GpiGpeEnIndex = 0;
//    while (Community[GpiGpeEnIndex].Address != GpioGroupInfo[NumberOfGroupsIndex].GpiGpeEnOffset) {
//      GpiGpeEnIndex++;
//    }
//    GpioToLockIndex = 0;
//    while (Community[GpioToLockIndex].Address != GpioGroupInfo[NumberOfGroupsIndex].PadCfgLockOffset) {
//      GpioToLockIndex++;
//    }
//    GpioToLockTxIndex = 0;
//    while (Community[GpioToLockTxIndex].Address != GpioGroupInfo[NumberOfGroupsIndex].PadCfgLockTxOffset) {
//      GpioToLockTxIndex++;
//    }
//    
//    GpioTableIndexCount += GpioGroupInfo[NumberOfGroupsIndex].PadPerGroup;
//    //
//    // Each Pad configuration has two registers, Dw0 and Dw1.
//    // Dw0 == TRUE  : Now the register is Dw0.
//    // Dw0 == FALSE : Now the register is Dw1.
//    //
//    Dw0 = TRUE;
//    for (PadNumber = 0; GpioTableIndex < GpioTableIndexCount; PadIndex++) {
//      GpioTablePad = RevertGpioPad (NumberOfGroupsIndex, PadNumber);
//      BuildCrbGpioDefinitionTableItem (GpioTableIndex, GpioTablePad, PadIndex, HostSW_OwnIndex, GpiGpeEnIndex, GpioToLockIndex, GpioToLockTxIndex, Community, Dw0);
//      if (Dw0) {
//        Dw0 = FALSE;
//      } else {
//        GpioTableIndex++;
//        PadNumber++;
//        Dw0 = TRUE;
//      }
//    }
//  }
//
//  DisplyGpioInformation (GpioTableIndexCount);

  if (GpioData != NULL) {
    FreePool (GpioData);
  }
  DEBUG ((DEBUG_INFO, "ReadGpioInformation end\n"));
  
  return EFI_SUCCESS;
}

#if 0
/**
  Display gpio table content by
  545659_SKL_PCH_U_Y_EDS_R0_9_pub.pdf &
  546717_SKL_PCH_H_EDS_r0_9.pdf

  Matrix is the same KabylakeChipsetPkg\Package.dsc and KabylakeMultiBoardPkg\Project.dsc
  Read gpio information then print content.
  
  @param[in] IndexCount     Total number of the GPIO table pad.    

  @retval EFI_SUCCESS       The entry point is executed successfully.

**/
EFI_STATUS
DisplyGpioInformation (
  IN UINT32                 IndexCount
  )
{
  UINT8                     Index;
  GPIO_NAME_TABLE           *GpioDescription;

  if (IsPchLp()) {
    GpioDescription = mGpioDescriptionLp;
  } else {
    GpioDescription = mGpioDescriptionH;
  }

  Print (L"[CRBGpioDefinitionTable] \n");
  Print (L"#                     |        |HostSoft|         |Output |Interrupt|Power |Electrical|Lock    |Other   |\n");
  Print (L"#  Pad                | PadMode|PadOwn  |Direction| State | Config  |Config|Config    |Config  |Settings|Rsvdbits\n");
  Print (L"#  (1)                |  (2)   |  (3)   |  (4)    |  (5)  | (6)     | (7)  |(8)       |   (9)  |(10)    |(11)\n");

  for (Index = 0; Index < IndexCount ; Index++) {
    switch (mCrbGpioDefinitionTable[Index].GpioPad){
      case GPIO_SKL_LP_GPP_B6:
      case GPIO_SKL_LP_GPP_B7:
      case GPIO_SKL_LP_GPP_B8:
      case GPIO_SKL_LP_GPP_B9:
      case GPIO_SKL_LP_GPP_B10:
      case GPIO_SKL_H_GPP_B6:
      case GPIO_SKL_H_GPP_B7:
      case GPIO_SKL_H_GPP_B8:
      case GPIO_SKL_H_GPP_B9:
      case GPIO_SKL_H_GPP_B10:
        Print (L"#  UINT32(0x%08x),", mCrbGpioDefinitionTable[Index].GpioPad);
        break;
      default:
        Print (L"  UINT32(0x%08x),", mCrbGpioDefinitionTable[Index].GpioPad);
        break;
    }
    Print (L"    0x%02x,", mCrbGpioDefinitionTable[Index].GpioConfig.PadMode);
    Print (L"    0x%02x,", mCrbGpioDefinitionTable[Index].GpioConfig.HostSoftPadOwn);
    Print (L"    0x%02x,", mCrbGpioDefinitionTable[Index].GpioConfig.Direction);
    Print (L"    0x%02x,", mCrbGpioDefinitionTable[Index].GpioConfig.OutputState);
    Print (L"    0x%02x,", mCrbGpioDefinitionTable[Index].GpioConfig.InterruptConfig);
    Print (L"    0x%02x,", mCrbGpioDefinitionTable[Index].GpioConfig.PowerConfig);
    Print (L"    0x%02x,", mCrbGpioDefinitionTable[Index].GpioConfig.ElectricalConfig);
    Print (L"    0x%02x,", mCrbGpioDefinitionTable[Index].GpioConfig.LockConfig);
    Print (L"    0x%02x,", mCrbGpioDefinitionTable[Index].GpioConfig.OtherSettings);
    Print (L"    0x%02x, \\", mCrbGpioDefinitionTable[Index].GpioConfig.RsvdBits);
    Print (L"#%s %s\n", GpioDescription[Index].PinName, GpioDescription[Index].Allocation);
  }

  return EFI_SUCCESS;
}


VOID
BuildCrbGpioDefinitionTableItem (
  UINT32                 Index,
  GPIO_PAD               GpioTablePad,
  UINT32                 PadIndex,
  UINT32                 HostSW_OwnIndex,
  UINT32                 GpiGpeEnIndex,
  UINT32                 GpioToLockIndex,
  UINT32                 GpioToLockTxIndex,
  GPIO_COMMUNITY         *CommunityX,
  BOOLEAN                Dw0
  )
{
  UINT32 Value;
  UINT32 PadNumber;
  
  PadNumber = GpioTablePad & 0xFFFF;

  if (Dw0) {
    //
    // GpioPad
    //
    mCrbGpioDefinitionTable[Index].GpioPad = GpioTablePad;
    //
    // PadMode
    //
    Value = ((CommunityX[PadIndex].Value & B_PCH_GPIO_PAD_MODE) >> N_PCH_GPIO_PAD_MODE) << 1;
    Value |=  0x01;
    mCrbGpioDefinitionTable[Index].GpioConfig.PadMode = Value; // Reference to GPIO_PAD_MODE

    //
    // HostSoftPadOwn
    //
    Value = ((CommunityX[HostSW_OwnIndex].Value & (1 << PadNumber)) >> PadNumber) << 1;
    Value |=  0x01;
    mCrbGpioDefinitionTable[Index].GpioConfig.HostSoftPadOwn = Value;

    //
    // Direction
    //
        // 1. (GPIORxDis and GPIOTxDis)
    Value = ((CommunityX[PadIndex].Value & (B_PCH_GPIO_RXDIS | B_PCH_GPIO_TXDIS)) >> N_PCH_GPIO_TXDIS) << 1;
    Value |=  0x01;
        // 2. (RXINV)
    Value |= ((CommunityX[PadIndex].Value & B_PCH_GPIO_RXINV) >> N_PCH_GPIO_RXINV) << 4;
    if (((Value & (BIT0 | BIT1 | BIT2)) != GpioDirOut) && ((Value & (BIT0 | BIT1 | BIT2)) != GpioDirNone)) {
      Value |= BIT3;
    }
    mCrbGpioDefinitionTable[Index].GpioConfig.Direction = Value;

    //
    // OutputState
    //
    Value = ((CommunityX[PadIndex].Value & B_PCH_GPIO_TX_STATE) >> N_PCH_GPIO_TX_STATE) << 1;
    Value |=  0x01;
    mCrbGpioDefinitionTable[Index].GpioConfig.OutputState = Value; // Reference to GPIO_OUTPUT_STATE

    //
    // InterruptConfig
    //
       // (GPIRoutIOxAPIC/SCI/SMI/NMI)
    Value = ((CommunityX[PadIndex].Value & (B_PCH_GPIO_RX_NMI_ROUTE | B_PCH_GPIO_RX_SCI_ROUTE | B_GPIO_PCR_RX_SMI_ROUTE | B_PCH_GPIO_RX_APIC_ROUTE)) >> N_PCH_GPIO_RX_NMI_ROUTE) << 1;
    Value |=  0x01;
       // (RxEvCfg)
    Value |= ((((CommunityX[PadIndex].Value & B_PCH_GPIO_RX_LVL_EDG) >> N_PCH_GPIO_RX_LVL_EDG) << 1)|0x01) << 5;
    mCrbGpioDefinitionTable[Index].GpioConfig.InterruptConfig = Value;

    //
    // PowerConfig
    //
    Value = ((CommunityX[PadIndex].Value & B_PCH_GPIO_RST_CONF) >> N_PCH_GPIO_RST_CONF) << 1;
    Value |=  0x01;
    mCrbGpioDefinitionTable[Index].GpioConfig.PowerConfig = Value;

    //
    // LockConfig
    //
    Value = ((CommunityX[GpioToLockIndex].Value & (1 << PadNumber)) >> PadNumber) << 1;
    Value |=  0x01;
    Value |= ((CommunityX[GpioToLockTxIndex].Value & (1 << PadNumber)) >> PadNumber) << 2;
    mCrbGpioDefinitionTable[Index].GpioConfig.LockConfig = Value;
    
    //
    // Others
    //
    Value = ((CommunityX[PadIndex].Value & B_PCH_GPIO_RX_RAW1) >> N_PCH_GPIO_RX_RAW1) << 1;
    Value |=  0x01;
    mCrbGpioDefinitionTable[Index].GpioConfig.OtherSettings = Value;
  } else {
    //
    // ElectricalConfig
    //
        // (Term)
    Value = ((CommunityX[PadIndex].Value & B_PCH_GPIO_TERM) >> N_PCH_GPIO_TERM) << 1;
    Value |=  0x01;
       // (padtol)
    Value |= (((CommunityX[PadIndex].Value & B_PCH_GPIO_PADTOL) >> N_PCH_GPIO_PADTOL) != 0) ? ((((CommunityX[PadIndex].Value & B_PCH_GPIO_PADTOL) >> N_PCH_GPIO_PADTOL) << 1) | 1) << 5 : ((CommunityX[PadIndex].Value & B_PCH_GPIO_PADTOL) >> N_PCH_GPIO_PADTOL) << 6;
    mCrbGpioDefinitionTable[Index].GpioConfig.ElectricalConfig = Value;

  }
}
#endif