/** @file
 Dump gpio information

;***************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _GPIO_SHELL_TOOL_REGS_H
#define _GPIO_SHELL_TOOL_REGS_H

//
// Common
//
#define R_GPIO_FAMBAR                0x08    ///> Family Base Address
#define R_GPIO_PADBAR                0x0C    ///> Pad Base Address
#define R_GPIO_MISCCFG               0x10    ///> Miscellaneous Configuration

//
// Community 0
//
#define R_GPIO_H_PADCFGLOCK_GPP_A_0    0x90
#define R_GPIO_H_PADCFGLOCKTX_GPP_A_0  0x94
#define R_GPIO_H_PADCFGLOCK_GPP_B_0    0x98    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCKTX_GPP_B_0  0x9C    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCK_GPP_A_0   0xA0    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCKTX_GPP_A_0 0xA4    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCK_GPP_B_0   0xA8    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCKTX_GPP_B_0 0xAC    ///> Pad Configuration Lock
#define R_GPIO_HOSTSW_OWN_GPP_A_0      0xD0    ///> Host Software Pad Ownership
#define R_GPIO_HOSTSW_OWN_GPP_B_0      0xD4    ///> Host Software Pad Ownership
#define R_GPIO_GPI_IS_GPP_A_0          0x100   ///> GPI Interrupt Status
#define R_GPIO_GPI_IS_GPP_B_0          0x104   ///> GPI Interrupt Status
#define R_GPIO_GPI_IE_GPP_A_0          0x120   ///> GPI Interrupt Status
#define R_GPIO_GPI_IE_GPP_B_0          0x124   ///> GPI Interrupt Status
#define R_GPIO_GPI_GPE_STS_GPP_A_0     0x140   ///> GPI General Purpose Events Status
#define R_GPIO_GPI_GPE_STS_GPP_B_0     0x144   ///> GPI General Purpose Events Status
#define R_GPIO_GPI_GPE_EN_GPP_A_0      0x160   ///> GPI General Purpose Events Enable
#define R_GPIO_GPI_GPE_EN_GPP_B_0      0x164   ///> GPI General Purpose Events Enable
#define R_GPIO_GPI_SMI_STS_GPP_B       0x184   ///> SMI Status
#define R_GPIO_GPI_SMI_EN_GPP_B        0x1A4   ///> SMI Enable
#define R_GPIO_GPI_NMI_STS_B_0         0x1C4   ///> NMI Status
#define R_GPIO_GPI_NMI_EN_B_0          0x1E4   ///> NMI Enable
//
// Community 1
//
#define R_GPIO_H_PADCFGLOCK_GPP_C_0    0x90    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCKTX_GPP_C_0  0x94    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCK_GPP_D_0    0x98    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCKTX_GPP_D_0  0x9C    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCK_GPP_E_0    0xA0    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCKTX_GPP_E_0  0xA4    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCK_GPP_F_0    0xA8    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCKTX_GPP_F_0  0xAC    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCK_GPP_G_0    0xB0    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCKTX_GPP_G_0  0xB4    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCK_GPP_H_0    0xB8    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCKTX_GPP_H_0  0xBC    ///> Pad Configuration Lock
#define R_GPIO_H_HOSTSW_OWN_GPP_F_0    0xDC    ///> Host Software Pad Ownership
#define R_GPIO_H_HOSTSW_OWN_GPP_G_0    0xE0    ///> Host Software Pad Ownership
#define R_GPIO_H_HOSTSW_OWN_GPP_H_0    0xE4    ///> Host Software Pad Ownership
#define R_GPIO_H_GPI_IS_GPP_F_0        0x10C   ///> GPI Interrupt Status
#define R_GPIO_H_GPI_IS_GPP_G_0        0x110   ///> GPI Interrupt Status
#define R_GPIO_H_GPI_IS_GPP_H_0        0x114   ///> GPI Interrupt Status
#define R_GPIO_H_GPI_IE_GPP_F_0        0x12C   ///> GPI Interrupt Status
#define R_GPIO_H_GPI_IE_GPP_G_0        0x130   ///> GPI Interrupt Status
#define R_GPIO_H_GPI_IE_GPP_H_0        0x134   ///> GPI Interrupt Status
#define R_GPIO_H_GPI_GPE_STS_GPP_F_0   0x14C   ///> GPI General Purpose Events Status
#define R_GPIO_H_GPI_GPE_STS_GPP_G_0   0x150   ///> GPI General Purpose Events Status
#define R_GPIO_H_GPI_GPE_STS_GPP_H_0   0x154   ///> GPI General Purpose Events Status
#define R_GPIO_H_GPI_GPE_EN_GPP_F_0    0x16C   ///> GPI General Purpose Events Enable
#define R_GPIO_H_GPI_GPE_EN_GPP_G_0    0x170   ///> GPI General Purpose Events Enable
#define R_GPIO_H_GPI_GPE_EN_GPP_H_0    0x174   ///> GPI General Purpose Events Enable
#define R_GPIO_H_GPI_SMI_STS_GPP_F_0   0x18C   ///> SMI Status
#define R_GPIO_H_GPI_SMI_STS_GPP_G_0   0x190   ///> SMI Status
#define R_GPIO_H_GPI_SMI_STS_GPP_H_0   0x194   ///> SMI Status
#define R_GPIO_H_GPI_SMI_EN_GPP_F_0    0x1AC   ///> SMI Enable
#define R_GPIO_H_GPI_SMI_EN_GPP_G_0    0x1B0   ///> SMI Enable
#define R_GPIO_H_GPI_SMI_EN_GPP_H_0    0x1B4   ///> SMI Enable
#define R_GPIO_H_GPI_NMI_STS_F_0       0x1CC   ///> NMI Status
#define R_GPIO_H_GPI_NMI_STS_G_0       0x1D0   ///> NMI Status
#define R_GPIO_H_GPI_NMI_STS_H_0       0x1D4   ///> NMI Status
#define R_GPIO_H_GPI_NMI_EN_F_0        0x1EC   ///> NMI Enable
#define R_GPIO_H_GPI_NMI_EN_G_0        0x1F0   ///> NMI Enable
#define R_GPIO_H_GPI_NMI_EN_H_0        0x1F4   ///> NMI Enable
#define R_GPIO_LP_PADCFGLOCK_GPP_C_0   0xA0    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCKTX_GPP_C_0 0xA4    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCK_GPP_D_0   0xA8    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCKTX_GPP_D_0 0xAC    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCK_GPP_E_0   0xB0    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCKTX_GPP_E_0 0xB4    ///> Pad Configuration Lock
#define R_GPIO_HOSTSW_OWN_GPP_C_0      0xD0    ///> Host Software Pad Ownership
#define R_GPIO_HOSTSW_OWN_GPP_D_0      0xD4    ///> Host Software Pad Ownership
#define R_GPIO_HOSTSW_OWN_GPP_E_0      0xD8    ///> Host Software Pad Ownership
#define R_GPIO_GPI_IS_GPP_C_0          0x100   ///> GPI Interrupt Status
#define R_GPIO_GPI_IS_GPP_D_0          0x104   ///> GPI Interrupt Status
#define R_GPIO_GPI_IS_GPP_E_0          0x108   ///> GPI Interrupt Status
#define R_GPIO_GPI_IE_GPP_C_0          0x120   ///> GPI Interrupt Status
#define R_GPIO_GPI_IE_GPP_D_0          0x124   ///> GPI Interrupt Status
#define R_GPIO_GPI_IE_GPP_E_0          0x128   ///> GPI Interrupt Status
#define R_GPIO_GPI_GPE_STS_GPP_C_0     0x140   ///> GPI General Purpose Events Status
#define R_GPIO_GPI_GPE_STS_GPP_D_0     0x144   ///> GPI General Purpose Events Status
#define R_GPIO_GPI_GPE_STS_GPP_E_0     0x148   ///> GPI General Purpose Events Status
#define R_GPIO_GPI_GPE_EN_GPP_C_0      0x160   ///> GPI General Purpose Events Enable
#define R_GPIO_GPI_GPE_EN_GPP_D_0      0x164   ///> GPI General Purpose Events Enable
#define R_GPIO_GPI_GPE_EN_GPP_E_0      0x168   ///> GPI General Purpose Events Enable
#define R_GPIO_GPI_SMI_STS_GPP_C_0     0x180   ///> SMI Status
#define R_GPIO_GPI_SMI_STS_GPP_D_0     0x184   ///> SMI Status
#define R_GPIO_GPI_SMI_STS_GPP_E_0     0x188   ///> SMI Status
#define R_GPIO_GPI_SMI_EN_GPP_C_0      0x1A0   ///> SMI Enable
#define R_GPIO_GPI_SMI_EN_GPP_D_0      0x1A4   ///> SMI Enable
#define R_GPIO_GPI_SMI_EN_GPP_E_0      0x1A8   ///> SMI Enable
#define R_GPIO_GPI_NMI_STS_C_0         0x1C0   ///> NMI Status
#define R_GPIO_GPI_NMI_STS_D_0         0x1C4   ///> NMI Status
#define R_GPIO_GPI_NMI_STS_E_0         0x1C8   ///> NMI Status
#define R_GPIO_GPI_NMI_EN_C_0          0x1E0   ///> NMI Enable
#define R_GPIO_GPI_NMI_EN_D_0          0x1E4   ///> NMI Enable
#define R_GPIO_GPI_NMI_EN_E_0          0x1E8   ///> NMI Enable
#define R_GPIO_PWMC                    0x204   ///> PWM Control
#define R_GPIO_GP_SER_BLINK            0x20C   ///> GPIO Serial Blink Enable
#define R_GPIO_GP_SER_CMDSTS           0x210   ///> GPIO Serial Blink Command/Status
#define R_GPIO_GP_SER_DATA             0x214   ///> GPIO Serial Blink Data
#define R_GPIO_H_GSX_CAP               0x21C   ///> GSX Controller Capabilities
#define R_GPIO_H_GSX_C0CAP_DW0         0x220   ///> GSX Channel-0 Capabilities DW0
#define R_GPIO_H_GSX_C0CAP_DW1         0x224   ///> GSX Channel-0 Capabilities DW1
#define R_GPIO_H_GSX_C0GPILVL_DW0      0x228   ///> GSX Channel-0 GP Input Level DW0
#define R_GPIO_H_GSX_C0GPILVL_DW1      0x22C   ///> GSX Channel-0 GP Input Level DW1
#define R_GPIO_H_GSX_C0GPOLVL_DW0      0x230   ///> GSX Channel-0 GP Output Level DW0
#define R_GPIO_H_GSX_C0GPOLVL_DW1      0x234   ///> GSX Channel-0 GP Output Level DW1
#define R_GPIO_H_GSX_C0CMD             0x238   ///> GSX Channel-0 Command
#define R_GPIO_H_GSX_C0TM              0x23C   ///> GSX Channel-0 Test Mode
//
// Community 2
//
#define R_GPIO_H_PADCFGLOCK_GPD_0      0x90    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCKTX_GPD_0    0x94    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCK_GPD_0     0xA0    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCKTX_GPD_0   0xA4    ///> Pad Configuration Lock
#define R_GPIO_HOSTSW_OWN_GPD_0        0xD0    ///> Host Software Pad Ownership
#define R_GPIO_GPI_IS_GPD_0            0x100   ///> GPI Interrupt Status
#define R_GPIO_GPI_IE_GPD_0            0x120   ///> GPI Interrupt Status
#define R_GPIO_GPI_GPE_STS_GPD_0       0x140   ///> GPI General Purpose Events Status
#define R_GPIO_GPI_GPE_EN_GPD_0        0x160   ///> GPI General Purpose Events Enable
//
// Community 3
//
#define R_GPIO_H_CAP_LIST_0            0x04    ///> Capability List Register
#define R_GPIO_H_PADCFGLOCK_GPP_I_0    0x90    ///> Pad Configuration Lock
#define R_GPIO_H_PADCFGLOCKTX_GPP_I_0  0x94    ///> Pad Configuration Lock
#define R_GPIO_H_HOSTSW_OWN_GPP_I_0    0xD0    ///> Host Software Pad Ownership
#define R_GPIO_H_GPI_IS_GPP_I_0        0x100   ///> GPI Interrupt Status
#define R_GPIO_H_GPI_IE_GPP_I_0        0x120   ///> GPI Interrupt Status
#define R_GPIO_H_GPI_GPE_STS_GPP_I_0   0x140   ///> GPI General Purpose Events Status
#define R_GPIO_H_GPI_GPE_EN_GPP_I_0    0x160   ///> GPI General Purpose Events Enable
#define R_GPIO_H_GPI_SMI_STS_GPP_I_0   0x180   ///> SMI Status
#define R_GPIO_H_GPI_SMI_EN_GPP_I_0    0x1A0   ///> SMI Enable
#define R_GPIO_H_GPI_NMI_STS_I_0       0x1C0   ///> NMI Status
#define R_GPIO_H_GPI_NMI_EN_I_0        0x1E0   ///> NMI Enable
#define R_GPIO_LP_PADCFGLOCK_GPP_F_0   0xA0    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCKTX_GPP_F_0 0xA4    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCK_GPP_G_0   0xA8    ///> Pad Configuration Lock
#define R_GPIO_LP_PADCFGLOCKTX_GPP_G_0 0xAC    ///> Pad Configuration Lock
#define R_GPIO_LP_HOSTSW_OWN_GPP_F_0   0xD0    ///> Host Software Pad Ownership
#define R_GPIO_LP_HOSTSW_OWN_GPP_G_0   0xD4    ///> Host Software Pad Ownership
#define R_GPIO_LP_GPI_IS_GPP_F_0       0x100   ///> GPI Interrupt Status
#define R_GPIO_LP_GPI_IS_GPP_G_0       0x104   ///> GPI Interrupt Status
#define R_GPIO_LP_GPI_IE_GPP_F_0       0x120   ///> GPI Interrupt Status
#define R_GPIO_LP_GPI_IE_GPP_G_0       0x124   ///> GPI Interrupt Status
#define R_GPIO_LP_GPI_GPE_STS_GPP_F_0  0x140   ///> GPI General Purpose Events Status
#define R_GPIO_LP_GPI_GPE_STS_GPP_G_0  0x144   ///> GPI General Purpose Events Status
#define R_GPIO_LP_GPI_GPE_EN_GPP_F_0   0x160   ///> GPI General Purpose Events Enable
#define R_GPIO_LP_GPI_GPE_EN_GPP_G_0   0x164   ///> GPI General Purpose Events Enable
//
// Community 0 - Group GPP A
//
#define R_GPIO_PAD_CFG_DW0_GPP_A_0   0x400   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_0   0x404   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_1   0x408   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_1   0x40C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_2   0x410   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_2   0x414   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_3   0x418   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_3   0x41C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_4   0x420   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_4   0x424   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_5   0x428   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_5   0x42C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_6   0x430   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_6   0x434   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_7   0x438   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_7   0x43C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_8   0x440   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_8   0x444   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_9   0x448   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_9   0x44C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_10  0x450   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_10  0x454   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_11  0x458   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_11  0x45C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_12  0x460   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_12  0x464   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_13  0x468   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_13  0x46C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_14  0x470   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_14  0x474   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_15  0x478   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_15  0x47C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_16  0x480   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_16  0x484   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_17  0x488   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_17  0x48C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_18  0x490   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_18  0x494   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_19  0x498   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_19  0x49C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_20  0x4A0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_20  0x4A4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_21  0x4A8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_21  0x4AC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_22  0x4B0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_22  0x4B4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_A_23  0x4B8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_A_23  0x4BC   ///> Pad Configuration DW1
//
// Community 0 - Group GPP B
//
#define R_GPIO_PAD_CFG_DW0_GPP_B_0   0x4C0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_0   0x4C4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_1   0x4C8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_1   0x4CC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_2   0x4D0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_2   0x4D4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_3   0x4D8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_3   0x4DC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_4   0x4E0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_4   0x4E4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_5   0x4E8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_5   0x4EC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_6   0x4F0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_6   0x4F4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_7   0x4F8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_7   0x4FC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_8   0x500   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_8   0x504   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_9   0x508   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_9   0x50C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_10  0x510   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_10  0x514   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_11  0x518   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_11  0x51C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_12  0x520   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_12  0x524   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_13  0x528   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_13  0x52C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_14  0x530   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_14  0x534   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_15  0x538   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_15  0x53C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_16  0x540   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_16  0x544   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_17  0x548   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_17  0x54C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_18  0x550   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_18  0x554   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_19  0x558   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_19  0x55C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_20  0x560   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_20  0x564   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_21  0x568   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_21  0x56C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_22  0x570   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_22  0x574   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_B_23  0x578   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_B_23  0x57C   ///> Pad Configuration DW1
//
// Community 1 - Group GPP C
//
#define R_GPIO_PAD_CFG_DW0_GPP_C_0   0x400   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_0   0x404   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_1   0x408   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_1   0x40C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_2   0x410   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_2   0x414   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_3   0x418   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_3   0x41C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_4   0x420   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_4   0x424   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_5   0x428   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_5   0x42C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_6   0x430   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_6   0x434   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_7   0x438   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_7   0x43C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_8   0x440   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_8   0x444   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_9   0x448   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_9   0x44C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_10  0x450   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_10  0x454   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_11  0x458   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_11  0x45C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_12  0x460   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_12  0x464   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_13  0x468   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_13  0x46C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_14  0x470   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_14  0x474   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_15  0x478   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_15  0x47C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_16  0x480   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_16  0x484   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_17  0x488   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_17  0x48C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_18  0x490   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_18  0x494   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_19  0x498   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_19  0x49C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_20  0x4A0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_20  0x4A4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_21  0x4A8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_21  0x4AC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_22  0x4B0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_22  0x4B4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_C_23  0x4B8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_C_23  0x4BC   ///> Pad Configuration DW1
//
// Community 1 - Group GPP D
//
#define R_GPIO_PAD_CFG_DW0_GPP_D_0   0x4C0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_0   0x4C4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_1   0x4C8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_1   0x4CC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_2   0x4D0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_2   0x4D4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_3   0x4D8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_3   0x4DC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_4   0x4E0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_4   0x4E4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_5   0x4E8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_5   0x4EC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_6   0x4F0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_6   0x4F4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_7   0x4F8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_7   0x4FC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_8   0x500   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_8   0x504   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_9   0x508   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_9   0x50C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_10  0x510   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_10  0x514   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_11  0x518   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_11  0x51C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_12  0x520   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_12  0x524   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_13  0x528   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_13  0x52C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_14  0x530   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_14  0x534   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_15  0x538   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_15  0x53C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_16  0x540   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_16  0x544   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_17  0x548   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_17  0x54C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_18  0x550   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_18  0x554   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_19  0x558   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_19  0x55C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_20  0x560   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_20  0x564   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_21  0x568   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_21  0x56C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_22  0x570   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_22  0x574   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_D_23  0x578   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_D_23  0x57C   ///> Pad Configuration DW1
//
// Community 1 - Group GPP E
// PchH  - 0~12
// PchLp - 0~23
//
#define R_GPIO_PAD_CFG_DW0_GPP_E_0   0x580   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_0   0x584   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_1   0x588   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_1   0x58C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_2   0x590   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_2   0x594   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_3   0x598   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_3   0x59C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_4   0x5A0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_4   0x5A4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_5   0x5A8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_5   0x5AC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_6   0x5B0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_6   0x5B4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_7   0x5B8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_7   0x5BC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_8   0x5C0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_8   0x5C4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_9   0x5C8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_9   0x5CC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_10  0x5D0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_10  0x5D4   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_11  0x5D8   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_11  0x5DC   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPP_E_12  0x5E0   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPP_E_12  0x5E4   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_13  0x5E8   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_13  0x5EC   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_14  0x5F0   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_14  0x5F4   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_15  0x5F8   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_15  0x5FC   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_16  0x600   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_16  0x604   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_17  0x608   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_17  0x60C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_18  0x610   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_18  0x614   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_19  0x618   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_19  0x61C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_20  0x620   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_20  0x624   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_21  0x628   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_21  0x62C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_22  0x630   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_22  0x634   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_E_23  0x638   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_E_23  0x63C   ///> Pad Configuration DW1
//
// PchH Community 1 - Group F
//
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_0   0x5E8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_0   0x5EC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_1   0x5F0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_1   0x5F4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_2   0x5F8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_2   0x5FC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_3   0x600   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_3   0x604   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_4   0x608   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_4   0x60C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_5   0x610   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_5   0x614   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_6   0x618   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_6   0x61C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_7   0x620   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_7   0x624   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_8   0x628   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_8   0x62C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_9   0x630   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_9   0x634   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_10  0x638   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_10  0x63C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_11  0x640   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_11  0x644   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_12  0x648   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_12  0x64C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_13  0x650   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_13  0x654   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_14  0x658   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_14  0x65C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_15  0x660   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_15  0x664   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_16  0x668   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_16  0x66C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_17  0x670   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_17  0x674   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_18  0x678   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_18  0x67C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_19  0x680   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_19  0x684   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_20  0x688   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_20  0x68C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_21  0x690   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_21  0x694   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_22  0x698   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_22  0x69C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_F_23  0x6A0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_F_23  0x6A4   ///> Pad Configuration DW1
//
// PchH Community 1 - Group G
//
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_0   0x6A8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_0   0x6AC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_1   0x6B0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_1   0x6B4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_2   0x6B8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_2   0x6BC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_3   0x6C0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_3   0x6C4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_4   0x6C8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_4   0x6CC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_5   0x6D0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_5   0x6D4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_6   0x6D8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_6   0x6DC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_7   0x6E0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_7   0x6E4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_8   0x6E8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_8   0x6EC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_9   0x6F0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_9   0x6F4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_10  0x6F8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_10  0x6FC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_11  0x700   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_11  0x704   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_12  0x708   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_12  0x70C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_13  0x710   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_13  0x714   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_14  0x718   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_14  0x71C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_15  0x720   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_15  0x724   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_16  0x728   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_16  0x72C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_17  0x730   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_17  0x734   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_18  0x738   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_18  0x73C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_19  0x740   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_19  0x744   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_20  0x748   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_20  0x74C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_21  0x750   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_21  0x754   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_22  0x758   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_22  0x75C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_G_23  0x760   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_G_23  0x764   ///> Pad Configuration DW1
//
// PchH Community 1 - Group H
//
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_0   0x768   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_0   0x76C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_1   0x770   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_1   0x774   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_2   0x778   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_2   0x77C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_3   0x780   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_3   0x784   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_4   0x788   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_4   0x78C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_5   0x790   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_5   0x794   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_6   0x798   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_6   0x79C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_7   0x7A0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_7   0x7A4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_8   0x7A8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_8   0x7AC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_9   0x7B0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_9   0x7B4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_10  0x7B8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_10  0x7BC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_11  0x7C0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_11  0x7C4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_12  0x7C8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_12  0x7CC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_13  0x7D0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_13  0x7D4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_14  0x7D8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_14  0x7DC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_15  0x7E0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_15  0x7E4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_16  0x7E8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_16  0x7EC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_17  0x7F0   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_17  0x7F4   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_18  0x7F8   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_18  0x7FC   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_19  0x800   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_19  0x804   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_20  0x808   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_20  0x80C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_21  0x810   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_21  0x814   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_22  0x818   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_22  0x81C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_H_23  0x820   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_H_23  0x824   ///> Pad Configuration DW1


//
// Community 2 - Group GPD
//
#define R_GPIO_PAD_CFG_DW0_GPD_0     0x400   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_0     0x404   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_1     0x408   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_1     0x40C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_2     0x410   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_2     0x414   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_3     0x418   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_3     0x41C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_4     0x420   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_4     0x424   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_5     0x428   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_5     0x42C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_6     0x430   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_6     0x434   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_7     0x438   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_7     0x43C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_8     0x440   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_8     0x444   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_9     0x448   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_9     0x44C   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_10    0x450   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_10    0x454   ///> Pad Configuration DW1
#define R_GPIO_PAD_CFG_DW0_GPD_11    0x458   ///> Pad Configuration DW0
#define R_GPIO_PAD_CFG_DW1_GPD_11    0x45C   ///> Pad Configuration DW1
//
// PchLp Community 3 - Group GPP F
//
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_0   0x400   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_0   0x404   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_1   0x408   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_1   0x40C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_2   0x410   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_2   0x414   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_3   0x418   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_3   0x41C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_4   0x420   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_4   0x424   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_5   0x428   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_5   0x42C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_6   0x430   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_6   0x434   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_7   0x438   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_7   0x43C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_8   0x440   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_8   0x444   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_9   0x448   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_9   0x44C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_10  0x450   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_10  0x454   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_11  0x458   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_11  0x45C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_12  0x460   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_12  0x464   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_13  0x468   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_13  0x46C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_14  0x470   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_14  0x474   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_15  0x478   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_15  0x47C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_16  0x480   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_16  0x484   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_17  0x488   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_17  0x48C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_18  0x490   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_18  0x494   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_19  0x498   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_19  0x49C   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_20  0x4A0   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_20  0x4A4   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_21  0x4A8   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_21  0x4AC   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_22  0x4B0   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_22  0x4B4   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_F_23  0x4B8   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_F_23  0x4BC   ///> Pad Configuration DW1
//
// Community 3 - Group GPP G
//
#define R_GPIO_LP_PAD_CFG_DW0_GPP_G_0   0x4C0   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_G_0   0x4C4   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_G_1   0x4C8   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_G_1   0x4CC   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_G_2   0x4D0   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_G_2   0x4D4   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_G_3   0x4D8   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_G_3   0x4DC   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_G_4   0x4E0   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_G_4   0x4E4   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_G_5   0x4E8   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_G_5   0x4EC   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_G_6   0x4F0   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_G_6   0x4F4   ///> Pad Configuration DW1
#define R_GPIO_LP_PAD_CFG_DW0_GPP_G_7   0x4F8   ///> Pad Configuration DW0
#define R_GPIO_LP_PAD_CFG_DW1_GPP_G_7   0x4FC   ///> Pad Configuration DW1
//
// PchH Community 3 - Group GPP I
//
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_0   0x400   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_0   0x404   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_1   0x408   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_1   0x40C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_2   0x410   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_2   0x414   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_3   0x418   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_3   0x41C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_4   0x420   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_4   0x424   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_5   0x428   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_5   0x42C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_6   0x430   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_6   0x434   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_7   0x438   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_7   0x43C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_8   0x440   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_8   0x444   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_9   0x448   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_9   0x44C   ///> Pad Configuration DW1
#define R_GPIO_H_PAD_CFG_DW0_GPP_I_10  0x450   ///> Pad Configuration DW0
#define R_GPIO_H_PAD_CFG_DW1_GPP_I_10  0x454   ///> Pad Configuration DW1

#endif // _GPIO_SHELL_TOOL_REGS_H

