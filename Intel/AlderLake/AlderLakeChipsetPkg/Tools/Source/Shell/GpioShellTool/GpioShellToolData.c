/** @file
  Dump gpio information by matrix and specific type

;***************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "GpioShellTool.h"

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_GROUP_INFO_SHELL_TOOL mPchLpGpioGroupInfoShellTool[] = {
  {PID_GPIOCOM0, R_ICL_PCH_LP_GPIO_PCR_GPP_A_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_A_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_A_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_GPP_A_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_GPP_A_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_A_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           R_ICL_PCH_LP_GPIO_PCR_GPP_A_PADCFGLOCK,   R_ICL_PCH_LP_GPIO_PCR_GPP_A_PADCFGLOCKTX,   R_ICL_PCH_LP_GPIO_PCR_GPP_A_PADCFG_OFFSET, ICL_PCH_LP_GPIO_GPP_A_PAD_MAX, L"GPP_A"}, //ICL PCH-LP GPP_A
  {PID_GPIOCOM0, R_ICL_PCH_LP_GPIO_PCR_GPP_B_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_B_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_B_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_GPP_B_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_GPP_B_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_B_GPI_GPE_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_B_SMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_B_SMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_B_NMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_B_NMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_B_PADCFGLOCK,   R_ICL_PCH_LP_GPIO_PCR_GPP_B_PADCFGLOCKTX,   R_ICL_PCH_LP_GPIO_PCR_GPP_B_PADCFG_OFFSET, ICL_PCH_LP_GPIO_GPP_B_PAD_MAX, L"GPP_B"}, //ICL PCH-LP GPP_B
  {PID_GPIOCOM4, R_ICL_PCH_LP_GPIO_PCR_GPP_C_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_C_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_C_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_GPP_C_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_GPP_C_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_C_GPI_GPE_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_C_SMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_C_SMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_C_NMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_C_NMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_C_PADCFGLOCK,   R_ICL_PCH_LP_GPIO_PCR_GPP_C_PADCFGLOCKTX,   R_ICL_PCH_LP_GPIO_PCR_GPP_C_PADCFG_OFFSET, ICL_PCH_LP_GPIO_GPP_C_PAD_MAX, L"GPP_C"}, //ICL PCH-LP GPP_C
  {PID_GPIOCOM1, R_ICL_PCH_LP_GPIO_PCR_GPP_D_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_D_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_D_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_GPP_D_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_GPP_D_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_D_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           R_ICL_PCH_LP_GPIO_PCR_GPP_D_PADCFGLOCK,   R_ICL_PCH_LP_GPIO_PCR_GPP_D_PADCFGLOCKTX,   R_ICL_PCH_LP_GPIO_PCR_GPP_D_PADCFG_OFFSET, ICL_PCH_LP_GPIO_GPP_D_PAD_MAX, L"GPP_D"}, //ICL PCH-LP GPP_D
  {PID_GPIOCOM4, R_ICL_PCH_LP_GPIO_PCR_GPP_E_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_E_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_E_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_GPP_E_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_GPP_E_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_E_GPI_GPE_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_E_SMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_E_SMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_E_NMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_E_NMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_E_PADCFGLOCK,   R_ICL_PCH_LP_GPIO_PCR_GPP_E_PADCFGLOCKTX,   R_ICL_PCH_LP_GPIO_PCR_GPP_E_PADCFG_OFFSET, ICL_PCH_LP_GPIO_GPP_E_PAD_MAX, L"GPP_E"}, //ICL PCH-LP GPP_E
  {PID_GPIOCOM1, R_ICL_PCH_LP_GPIO_PCR_GPP_F_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_F_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_F_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_GPP_F_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_GPP_F_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_F_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           R_ICL_PCH_LP_GPIO_PCR_GPP_F_PADCFGLOCK,   R_ICL_PCH_LP_GPIO_PCR_GPP_F_PADCFGLOCKTX,   R_ICL_PCH_LP_GPIO_PCR_GPP_F_PADCFG_OFFSET, ICL_PCH_LP_GPIO_GPP_F_PAD_MAX, L"GPP_F"}, //ICL PCH-LP GPP_F
  {PID_GPIOCOM0, R_ICL_PCH_LP_GPIO_PCR_GPP_G_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_G_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_G_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_GPP_G_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_GPP_G_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_G_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           R_ICL_PCH_LP_GPIO_PCR_GPP_G_PADCFGLOCK,   R_ICL_PCH_LP_GPIO_PCR_GPP_G_PADCFGLOCKTX,   R_ICL_PCH_LP_GPIO_PCR_GPP_G_PADCFG_OFFSET, ICL_PCH_LP_GPIO_GPP_G_PAD_MAX, L"GPP_G"}, //ICL PCH-LP GPP_G
  {PID_GPIOCOM1, R_ICL_PCH_LP_GPIO_PCR_GPP_H_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_H_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_H_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_GPP_H_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_GPP_H_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_H_GPI_GPE_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_H_SMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_H_SMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_H_NMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_H_NMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_H_PADCFGLOCK,   R_ICL_PCH_LP_GPIO_PCR_GPP_H_PADCFGLOCKTX,   R_ICL_PCH_LP_GPIO_PCR_GPP_H_PADCFG_OFFSET, ICL_PCH_LP_GPIO_GPP_H_PAD_MAX, L"GPP_H"}, //ICL PCH-LP GPP_H
  {PID_GPIOCOM5, R_ICL_PCH_LP_GPIO_PCR_GPP_R_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_R_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_R_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_GPP_R_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_GPP_R_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_R_GPI_GPE_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_R_SMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_R_SMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_R_NMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_R_NMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_R_PADCFGLOCK,   R_ICL_PCH_LP_GPIO_PCR_GPP_R_PADCFGLOCKTX,   R_ICL_PCH_LP_GPIO_PCR_GPP_R_PADCFG_OFFSET, ICL_PCH_LP_GPIO_GPP_R_PAD_MAX, L"GPP_R"}, //ICL PCH-LP GPP_R
  {PID_GPIOCOM5, R_ICL_PCH_LP_GPIO_PCR_GPP_S_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_S_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_GPP_S_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_GPP_S_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_GPP_S_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_S_GPI_GPE_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_S_SMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_S_SMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_S_NMI_STS, R_ICL_PCH_LP_GPIO_PCR_GPP_S_NMI_EN, R_ICL_PCH_LP_GPIO_PCR_GPP_S_PADCFGLOCK,   R_ICL_PCH_LP_GPIO_PCR_GPP_S_PADCFGLOCKTX,   R_ICL_PCH_LP_GPIO_PCR_GPP_S_PADCFG_OFFSET, ICL_PCH_LP_GPIO_GPP_S_PAD_MAX, L"GPP_S"}, //ICL PCH-LP GPP_S
  {PID_GPIOCOM2, R_ICL_PCH_LP_GPIO_PCR_GPD_PAD_OWN,   R_ICL_PCH_LP_GPIO_PCR_GPD_HOSTSW_OWN,   R_ICL_PCH_LP_GPIO_PCR_GPD_GPI_IS,   R_ICL_PCH_LP_GPIO_PCR_GPD_GPI_IE,   R_ICL_PCH_LP_GPIO_PCR_GPD_GPI_GPE_STS,   R_ICL_PCH_LP_GPIO_PCR_GPD_GPI_GPE_EN,   NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           R_ICL_PCH_LP_GPIO_PCR_GPD_PADCFGLOCK,     R_ICL_PCH_LP_GPIO_PCR_GPD_PADCFGLOCKTX,     R_ICL_PCH_LP_GPIO_PCR_GPD_PADCFG_OFFSET,   ICL_PCH_LP_GPIO_GPD_PAD_MAX,   L"GPD"}    //ICL PCH-LP GPD
//  {PID_GPIOCOM1, R_ICL_PCH_LP_GPIO_PCR_VGPIO_PAD_OWN, R_ICL_PCH_LP_GPIO_PCR_VGPIO_HOSTSW_OWN, R_ICL_PCH_LP_GPIO_PCR_VGPIO_GPI_IS, R_ICL_PCH_LP_GPIO_PCR_VGPIO_GPI_IE, R_ICL_PCH_LP_GPIO_PCR_VGPIO_GPI_GPE_STS, R_ICL_PCH_LP_GPIO_PCR_VGPIO_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           R_ICL_PCH_LP_GPIO_PCR_VGPIO_0_PADCFGLOCK, R_ICL_PCH_LP_GPIO_PCR_VGPIO_0_PADCFGLOCKTX, R_ICL_PCH_LP_GPIO_PCR_VGPIO_PADCFG_OFFSET, ICL_PCH_LP_GPIO_VGPIO_PAD_MAX}, //ICL PCH-LP vGPIO
//  {PID_GPIOCOM5, R_ICL_PCH_LP_GPIO_PCR_SPI_PAD_OWN,   R_ICL_PCH_LP_GPIO_PCR_SPI_HOSTSW_OWN,   R_ICL_PCH_LP_GPIO_PCR_SPI_GPI_IS,   R_ICL_PCH_LP_GPIO_PCR_SPI_GPI_IE,   R_ICL_PCH_LP_GPIO_PCR_SPI_GPI_GPE_STS,   R_ICL_PCH_LP_GPIO_PCR_SPI_GPI_GPE_EN,   NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           R_ICL_PCH_LP_GPIO_PCR_SPI_PADCFGLOCK,     R_ICL_PCH_LP_GPIO_PCR_SPI_PADCFGLOCKTX,     R_ICL_PCH_LP_GPIO_PCR_SPI_PADCFG_OFFSET,   ICL_PCH_LP_GPIO_SPI_PAD_MAX},   //ICL PCH-LP SPI
//  {PID_GPIOCOM3, R_ICL_PCH_LP_GPIO_PCR_CPU_PAD_OWN,   R_ICL_PCH_LP_GPIO_PCR_CPU_HOSTSW_OWN,   NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,                NO_REGISTER_FOR_PROPERTY,               NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           R_ICL_PCH_LP_GPIO_PCR_CPU_PADCFGLOCK,     R_ICL_PCH_LP_GPIO_PCR_CPU_PADCFGLOCKTX,     R_ICL_PCH_LP_GPIO_PCR_CPU_PADCFG_OFFSET,   ICL_PCH_LP_GPIO_CPU_PAD_MAX},   //ICL PCH-LP CPU
//  {PID_GPIOCOM4, R_ICL_PCH_LP_GPIO_PCR_JTAG_PAD_OWN,  R_ICL_PCH_LP_GPIO_PCR_JTAG_HOSTSW_OWN,  NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,                NO_REGISTER_FOR_PROPERTY,               NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           R_ICL_PCH_LP_GPIO_PCR_JTAG_PADCFGLOCK,    R_ICL_PCH_LP_GPIO_PCR_JTAG_PADCFGLOCKTX,    R_ICL_PCH_LP_GPIO_PCR_JTAG_PADCFG_OFFSET,  ICL_PCH_LP_GPIO_JTAG_PAD_MAX},  //ICL PCH-LP JTAG
//  {PID_GPIOCOM4, R_ICL_PCH_LP_GPIO_PCR_HVCMOS_PAD_OWN,R_ICL_PCH_LP_GPIO_PCR_HVCMOS_HOSTSW_OWN,R_ICL_PCH_LP_GPIO_PCR_HVCMOS_GPI_IS,R_ICL_PCH_LP_GPIO_PCR_HVCMOS_GPI_IE,R_ICL_PCH_LP_GPIO_PCR_HVCMOS_GPI_GPE_STS,R_ICL_PCH_LP_GPIO_PCR_HVCMOS_GPI_GPE_EN,NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,            NO_REGISTER_FOR_PROPERTY,           R_ICL_PCH_LP_GPIO_PCR_HVCMOS_PADCFGLOCK,  R_ICL_PCH_LP_GPIO_PCR_HVCMOS_PADCFGLOCKTX,  R_ICL_PCH_LP_GPIO_PCR_HVCMOS_PADCFG_OFFSET,ICL_PCH_LP_GPIO_HVCMOS_PAD_MAX} //ICL PCH-LP HVCMOS
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_GROUP_INFO_SHELL_TOOL mPchHGpioGroupInfoShellTool[] = {
  {PID_GPIOCOM0, R_ICL_PCH_H_GPIO_PCR_GPP_A_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_A_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_A_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_A_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_A_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_A_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_GPP_A_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_A_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_A_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_A_PAD_MAX, L"GPP_A"},   //ICL PCH-H GPP_A
  {PID_GPIOCOM0, R_ICL_PCH_H_GPIO_PCR_GPP_B_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_B_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_B_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_B_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_B_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_B_GPI_GPE_EN, R_ICL_PCH_H_GPIO_PCR_GPP_B_SMI_STS, R_ICL_PCH_H_GPIO_PCR_GPP_B_SMI_EN, R_ICL_PCH_H_GPIO_PCR_GPP_B_NMI_STS, R_ICL_PCH_H_GPIO_PCR_GPP_B_NMI_EN, R_ICL_PCH_H_GPIO_PCR_GPP_B_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_B_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_B_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_B_PAD_MAX, L"GPP_B"},   //ICL PCH-H GPP_B
  {PID_GPIOCOM1, R_ICL_PCH_H_GPIO_PCR_GPP_C_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_C_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_C_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_C_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_C_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_C_GPI_GPE_EN, R_ICL_PCH_H_GPIO_PCR_GPP_C_SMI_STS, R_ICL_PCH_H_GPIO_PCR_GPP_C_SMI_EN, R_ICL_PCH_H_GPIO_PCR_GPP_C_NMI_STS, R_ICL_PCH_H_GPIO_PCR_GPP_C_NMI_EN, R_ICL_PCH_H_GPIO_PCR_GPP_C_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_C_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_C_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_C_PAD_MAX, L"GPP_C"},   //ICL PCH-H GPP_C
  {PID_GPIOCOM1, R_ICL_PCH_H_GPIO_PCR_GPP_D_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_D_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_D_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_D_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_D_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_D_GPI_GPE_EN, R_ICL_PCH_H_GPIO_PCR_GPP_D_SMI_STS, R_ICL_PCH_H_GPIO_PCR_GPP_D_SMI_EN, R_ICL_PCH_H_GPIO_PCR_GPP_D_NMI_STS, R_ICL_PCH_H_GPIO_PCR_GPP_D_NMI_EN, R_ICL_PCH_H_GPIO_PCR_GPP_D_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_D_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_D_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_D_PAD_MAX, L"GPP_D"},   //ICL PCH-H GPP_D
  {PID_GPIOCOM3, R_ICL_PCH_H_GPIO_PCR_GPP_E_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_E_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_E_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_E_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_E_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_E_GPI_GPE_EN, R_ICL_PCH_H_GPIO_PCR_GPP_E_SMI_STS, R_ICL_PCH_H_GPIO_PCR_GPP_E_SMI_EN, R_ICL_PCH_H_GPIO_PCR_GPP_E_NMI_STS, R_ICL_PCH_H_GPIO_PCR_GPP_E_NMI_EN, R_ICL_PCH_H_GPIO_PCR_GPP_E_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_E_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_E_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_E_PAD_MAX, L"GPP_E"},   //ICL PCH-H GPP_E
  {PID_GPIOCOM3, R_ICL_PCH_H_GPIO_PCR_GPP_F_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_F_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_F_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_F_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_F_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_F_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_GPP_F_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_F_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_F_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_F_PAD_MAX, L"GPP_F"},   //ICL PCH-H GPP_F
  {PID_GPIOCOM1, R_ICL_PCH_H_GPIO_PCR_GPP_G_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_G_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_G_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_G_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_G_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_G_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_GPP_G_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_G_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_G_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_G_PAD_MAX, L"GPP_G"},   //ICL PCH-H GPP_G
  {PID_GPIOCOM4, R_ICL_PCH_H_GPIO_PCR_GPP_H_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_H_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_H_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_H_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_H_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_H_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_GPP_H_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_H_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_H_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_H_PAD_MAX, L"GPP_H"},   //ICL PCH-H GPP_H
  {PID_GPIOCOM5, R_ICL_PCH_H_GPIO_PCR_GPP_I_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_I_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_I_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_I_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_I_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_I_GPI_GPE_EN, R_ICL_PCH_H_GPIO_PCR_GPP_I_SMI_STS, R_ICL_PCH_H_GPIO_PCR_GPP_I_SMI_EN, R_ICL_PCH_H_GPIO_PCR_GPP_I_NMI_STS, R_ICL_PCH_H_GPIO_PCR_GPP_I_NMI_EN, R_ICL_PCH_H_GPIO_PCR_GPP_I_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_I_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_I_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_I_PAD_MAX, L"GPP_I"},   //ICL PCH-H GPP_I
  {PID_GPIOCOM4, R_ICL_PCH_H_GPIO_PCR_GPP_J_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_J_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_J_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_J_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_J_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_J_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_GPP_J_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_J_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_J_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_J_PAD_MAX, L"GPP_J"},   //ICL PCH-H GPP_J
  {PID_GPIOCOM4, R_ICL_PCH_H_GPIO_PCR_GPP_K_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_K_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_K_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_K_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_K_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_K_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_GPP_K_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_K_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_K_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_K_PAD_MAX, L"GPP_K"},   //ICL PCH-H GPP_K
  {PID_GPIOCOM1, R_ICL_PCH_H_GPIO_PCR_GPP_R_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_R_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_R_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_R_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_R_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_R_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_GPP_R_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_R_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_R_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_R_PAD_MAX, L"GPP_R"},   //ICL PCH-H GPP_R
  {PID_GPIOCOM1, R_ICL_PCH_H_GPIO_PCR_GPP_S_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_S_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_GPP_S_GPI_IS, R_ICL_PCH_H_GPIO_PCR_GPP_S_GPI_IE, R_ICL_PCH_H_GPIO_PCR_GPP_S_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_GPP_S_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_GPP_S_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_GPP_S_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_GPP_S_PADCFG_OFFSET,  ICL_PCH_H_GPIO_GPP_S_PAD_MAX, L"GPP_S"},   //ICL PCH-H GPP_S
  {PID_GPIOCOM2, R_ICL_PCH_H_GPIO_PCR_GPD_PAD_OWN,    R_ICL_PCH_H_GPIO_PCR_GPD_HOSTSW_OWN,    R_ICL_PCH_H_GPIO_PCR_GPD_GPI_IS,   R_ICL_PCH_H_GPIO_PCR_GPD_GPI_IE,   R_ICL_PCH_H_GPIO_PCR_GPD_GPI_GPE_STS,   R_ICL_PCH_H_GPIO_PCR_GPD_GPI_GPE_EN,   NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_GPD_PADCFGLOCK,     R_ICL_PCH_H_GPIO_PCR_GPD_PADCFGLOCKTX,     R_ICL_PCH_H_GPIO_PCR_GPD_PADCFG_OFFSET,    ICL_PCH_H_GPIO_GPD_PAD_MAX,   L"GPD"}      //ICL PCH-H GPD
//  {PID_GPIOCOM1, R_ICL_PCH_H_GPIO_PCR_VGPIO_PAD_OWN,  R_ICL_PCH_H_GPIO_PCR_VGPIO_HOSTSW_OWN,  R_ICL_PCH_H_GPIO_PCR_VGPIO_GPI_IS, R_ICL_PCH_H_GPIO_PCR_VGPIO_GPI_IE, R_ICL_PCH_H_GPIO_PCR_VGPIO_GPI_GPE_STS, R_ICL_PCH_H_GPIO_PCR_VGPIO_GPI_GPE_EN, NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_VGPIO_PADCFGLOCK,   R_ICL_PCH_H_GPIO_PCR_VGPIO_PADCFGLOCKTX,   R_ICL_PCH_H_GPIO_PCR_VGPIO_PADCFG_OFFSET,  ICL_PCH_H_GPIO_VGPIO_PAD_MAX},   //ICL PCH-H vGPIO
//  {PID_GPIOCOM0, R_ICL_PCH_H_GPIO_PCR_VGPIO_0_PAD_OWN,R_ICL_PCH_H_GPIO_PCR_VGPIO_0_HOSTSW_OWN,NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,               NO_REGISTER_FOR_PROPERTY,              NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_VGPIO_0_PADCFGLOCK, R_ICL_PCH_H_GPIO_PCR_VGPIO_0_PADCFGLOCKTX, R_ICL_PCH_H_GPIO_PCR_VGPIO_0_PADCFG_OFFSET,ICL_PCH_H_GPIO_VGPIO_0_PAD_MAX}, //ICL PCH-H VGPIO_0
//  {PID_GPIOCOM3, R_ICL_PCH_H_GPIO_PCR_VGPIO_3_PAD_OWN,R_ICL_PCH_H_GPIO_PCR_VGPIO_3_HOSTSW_OWN,NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,               NO_REGISTER_FOR_PROPERTY,              NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_VGPIO_3_PADCFGLOCK, R_ICL_PCH_H_GPIO_PCR_VGPIO_3_PADCFGLOCKTX, R_ICL_PCH_H_GPIO_PCR_VGPIO_3_PADCFG_OFFSET,ICL_PCH_H_GPIO_VGPIO_3_PAD_MAX}, //ICL PCH-H VGPIO_3
//  {PID_GPIOCOM0, R_ICL_PCH_H_GPIO_PCR_SPI_PAD_OWN,    R_ICL_PCH_H_GPIO_PCR_SPI_HOSTSW_OWN,    NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,               NO_REGISTER_FOR_PROPERTY,              NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_SPI_PADCFGLOCK,     R_ICL_PCH_H_GPIO_PCR_SPI_PADCFGLOCKTX,     R_ICL_PCH_H_GPIO_PCR_SPI_PADCFG_OFFSET,    ICL_PCH_H_GPIO_SPI_PAD_MAX},     //ICL PCH-H SPI
//  {PID_GPIOCOM5, R_ICL_PCH_H_GPIO_PCR_JTAG_PAD_OWN,   R_ICL_PCH_H_GPIO_PCR_JTAG_HOSTSW_OWN,   NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,               NO_REGISTER_FOR_PROPERTY,              NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_JTAG_PADCFGLOCK,    R_ICL_PCH_H_GPIO_PCR_JTAG_PADCFGLOCKTX,    R_ICL_PCH_H_GPIO_PCR_JTAG_PADCFG_OFFSET,   ICL_PCH_H_GPIO_JTAG_PAD_MAX},    //ICL PCH-H JTAG
//  {PID_GPIOCOM5, R_ICL_PCH_H_GPIO_PCR_CPU_PAD_OWN,    R_ICL_PCH_H_GPIO_PCR_CPU_HOSTSW_OWN,    NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,               NO_REGISTER_FOR_PROPERTY,              NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          NO_REGISTER_FOR_PROPERTY,           NO_REGISTER_FOR_PROPERTY,          R_ICL_PCH_H_GPIO_PCR_CPU_PADCFGLOCK,     R_ICL_PCH_H_GPIO_PCR_CPU_PADCFGLOCKTX,     R_ICL_PCH_H_GPIO_PCR_CPU_PADCFG_OFFSET,    ICL_PCH_H_GPIO_CPU_PAD_MAX}      //ICL PCH-H CPU
};

/**
  This procedure will retrieve address and length of GPIO info table

  @param[out]  GpioGroupInfoTableLength   Length of GPIO group table

  @retval Pointer to GPIO group table

**/
GPIO_GROUP_INFO_SHELL_TOOL*
GpioGetGroupInfoTableShellTool (
  OUT UINT32              *GpioGroupInfoTableLength
  )
{
  if (IsPchLp ()) {
    *GpioGroupInfoTableLength = ARRAY_SIZE (mPchLpGpioGroupInfoShellTool);
    return mPchLpGpioGroupInfoShellTool;
  } else {
    *GpioGroupInfoTableLength = ARRAY_SIZE (mPchHGpioGroupInfoShellTool);
    return mPchHGpioGroupInfoShellTool;
  }
}

///**
//  This procedure will get lowest group
//
//  @param[in] none
//
//  @retval Value               Lowest Group
//**/
//GPIO_GROUP
//GpioGetLowestGroup (
//  VOID
//  )
//{
//  if (IsPchLp()) {
//    return (UINT32)GPIO_SKL_LP_GROUP_GPP_A;
//  } else {
//    return (UINT32)GPIO_SKL_H_GROUP_GPP_A;
//  }
//}
//
///**
//  This procedure will get number of groups
//
//  @param[in] none
//
//  @retval Value               Group number
//**/
//UINT8
//GpioGetNumberOfGroups (
//  VOID
//  )
//{
//  if (IsPchLp()) {
//    return V_PCH_LP_GPIO_GROUP_MAX;
//  } else {
//    return V_PCH_H_GPIO_GROUP_MAX;
//  }
//}


