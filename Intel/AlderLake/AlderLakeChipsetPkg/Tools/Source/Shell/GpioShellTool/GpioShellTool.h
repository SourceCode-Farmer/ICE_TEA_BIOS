/** @file
 Dump gpio information

;***************************************************************************
;* Copyright (c) 2017 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _GPIO_SHELL_TOOL_H
#define _GPIO_SHELL_TOOL_H

#include <Library/IoLib.h>
#include <Uefi.h>
#include <PiDxe.h>
#include <Library/PcdLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/HiiDbLib.h>
#include <Library/HiiStringLib.h>
#include <Library/HiiConfigAccessLib.h>
#include <Library/ShellLib.h>
#include <Library/DebugLib.h>
#include <Library/PchInfoLib.h>
#include <Private/Library/GpioPrivateLib.h>
#include <Library/PchPcrLib.h>
#include <Register/PchRegsGpioIcl.h>
#include <Register/PchRegsPcr.h>
#include <H2OBoardId.h>

typedef struct {
  UINT8   GroupIndex;
  CHAR16  *GroupName;
  UINT8   PadIndex;
  UINT32  Address;
  UINT32  Dw0Value;
  UINT32  Dw1Value;
} GPIO_DATA;

//typedef struct {
//  CHAR16* PinName;
//  CHAR16* Allocation;
//} GPIO_NAME_TABLE;

//
// Structure for storing information about registers offset, community,
// maximal pad number for available groups
//
typedef struct {
  PCH_SBI_PID  Community;
  UINT16       PadOwnOffset;
  UINT16       HostOwnOffset;
  UINT16       GpiIsOffset;
  UINT16       GpiIeOffset;
  UINT16       GpiGpeStsOffset;
  UINT16       GpiGpeEnOffset;
  UINT16       SmiStsOffset;
  UINT16       SmiEnOffset;
  UINT16       NmiStsOffset;
  UINT16       NmiEnOffset;
  UINT16       PadCfgLockOffset;
  UINT16       PadCfgLockTxOffset;
  UINT16       PadCfgOffset;
  UINT16       PadPerGroup;
  CHAR16       *GroupName;
} GPIO_GROUP_INFO_SHELL_TOOL;


GPIO_GROUP_INFO_SHELL_TOOL*
GpioGetGroupInfoTableShellTool (
  OUT UINT32              *GpioGroupInfoTableLength
  );

EFI_STATUS
ReadGpioInformation (
  IN PCH_SERIES             PchSeries
  );

#if 0
EFI_STATUS
DisplyGpioInformation (
  IN UINT32                 IndexCount
  );

VOID
BuildCrbGpioDefinitionTableItem (
  UINT32           Index,
  GPIO_PAD         GpioTablePad,
  UINT32           PadIndex,
  UINT32           HostSW_OwnIndex,
  UINT32           GpiGpeEnIndex,
  UINT32           GpioToLockIndex,
  UINT32           GpioToLockTxIndex,
  GPIO_COMMUNITY   *CommunityX,
  BOOLEAN          Dw0
  );
#endif
#endif // _GPIO_SHELL_TOOL_H

