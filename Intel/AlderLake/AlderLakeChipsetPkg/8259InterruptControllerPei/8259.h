/** @file
   Legacy 8259 PEIM definition
;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
**/

#ifndef _LEGACY_8259_PEI_H_
#define _LEGACY_8259_PEI_H_

#include <Library/IoLib.h>
#include <Library/BaseLib.h>
#include <Library/PcdLib.h>
#include <Ppi/Legacy8259.h>

//
// 8259 Hardware definitions
//
#define LEGACY_MODE_BASE_VECTOR_MASTER                    0x08
#define LEGACY_MODE_BASE_VECTOR_SLAVE                     0x70

#define PROTECTED_MODE_BASE_VECTOR_MASTER                 0x68
#define PROTECTED_MODE_BASE_VECTOR_SLAVE                  0x70

#define LEGACY_8259_CONTROL_REGISTER_MASTER               0x20
#define LEGACY_8259_MASK_REGISTER_MASTER                  0x21
#define LEGACY_8259_CONTROL_REGISTER_SLAVE                0xA0
#define LEGACY_8259_MASK_REGISTER_SLAVE                   0xA1
#define LEGACY_8259_EDGE_LEVEL_TRIGGERED_REGISTER_MASTER  0x4D0
#define LEGACY_8259_EDGE_LEVEL_TRIGGERED_REGISTER_SLAVE   0x4D1

#define LEGACY_8259_EOI                                   0x20

//
// Ppi Function Prototypes
//

/**
  Get the 8259 interrupt masks for Irq0 - Irq15. A different mask exists for
  the legacy mode mask and the protected mode mask. The base address for the 8259
  is different for legacy and protected mode, so two masks are required.

  @param[in]  This                  The PPI instance pointer.
  @param[in]  MasterBase            The base vector for the Master PIC in the 8259 controller.
  @param[in]  SlaveBase             The base vector for the Slave PIC in the 8259 controller.

  @retval     EFI_SUCCESS           The new bases were programmed.
  @retval     EFI_DEVICE_ERROR      A device error occured programming the vector bases.

**/
EFI_STATUS
EFIAPI
Interrupt8259SetVectorBase (
  IN  H2O_LEGACY_8259_PPI               *This,
  IN  UINT8                             MasterBase,
  IN  UINT8                             SlaveBase
  );

/**
  Get the 8259 interrupt masks for Irq0 - Irq15. A different mask exists for
  the legacy mode mask and the protected mode mask. The base address for the 8259
  is different for legacy and protected mode, so two masks are required.

  @param[in]   This                  The PPI instance pointer.
  @param[out]  LegacyMask            Bit 0 is Irq0 - Bit 15 is Irq15.
  @param[out]  LegacyEdgeLevel       Bit 0 is Irq0 - Bit 15 is Irq15.
  @param[out]  ProtectedMask         Bit 0 is Irq0 - Bit 15 is Irq15.
  @param[out]  ProtectedEdgeLevel    Bit 0 is Irq0 - Bit 15 is Irq15.

  @retval      EFI_SUCCESS           8259 status returned.
  @retval      EFI_DEVICE_ERROR      Error reading 8259.

**/

EFI_STATUS
EFIAPI
Interrupt8259GetMask (
  IN  H2O_LEGACY_8259_PPI       *This,
  OUT UINT16                    *LegacyMask OPTIONAL, 
  OUT UINT16                    *LegacyEdgeLevel OPTIONAL,
  OUT UINT16                    *ProtectedMask OPTIONAL,
  OUT UINT16                    *ProtectedEdgeLevel OPTIONAL
  );

/**
  Set the 8259 interrupt masks for Irq0 - Irq15. A different mask exists for
  the legacy mode mask and the protected mode mask. The base address for the 8259
  is different for legacy and protected mode, so two masks are required.
  Also set the edge/level masks.

  @param[in]  This                  The PPI instance pointer.
  @param[in]  LegacyMask            Bit 0 is Irq0 - Bit 15 is Irq15.
  @param[in]  LegacyEdgeLevel       Bit 0 is Irq0 - Bit 15 is Irq15.
  @param[in]  ProtectedMask         Bit 0 is Irq0 - Bit 15 is Irq15.
  @param[in]  ProtectedEdgeLevel    Bit 0 is Irq0 - Bit 15 is Irq15.

  @retval EFI_SUCCESS           8259 status returned.
  @retval EFI_DEVICE_ERROR      Error writing 8259.

**/
EFI_STATUS
EFIAPI 
Interrupt8259SetMask (
  IN  H2O_LEGACY_8259_PPI               *This,
  IN  UINT16                            *LegacyMask OPTIONAL,
  IN  UINT16                            *LegacyEdgeLevel OPTIONAL,
  IN  UINT16                            *ProtectedMask OPTIONAL,
  IN  UINT16                            *ProtectedEdgeLevel OPTIONAL
  );

/**
  Set the 8259 mode of operation. The base address for the 8259 is different for
  legacy and protected mode. The legacy mode requires the master 8259 to have a
  master base of 0x08 and the slave base of 0x70. The protected mode base locations
  are not defined. Interrupts must be masked by the caller before this function
  is called. The interrupt mask from the current mode is saved. The interrupt
  mask for the new mode is Mask, or if Mask does not exist the previously saved
  mask is used.

  @param[in]  This                  The PPI instance pointer.
  @param[in]  Mode                  The mode of operation. i.e. the real mode or protected mode.
  @param[in]  Mask                  Optional interupt mask for the new mode.
  @param[in]  EdgeLevel             Optional trigger mask for the new mode.

  @retval     EFI_SUCCESS           8259 programmed.
  @retval     EFI_DEVICE_ERROR      Error writing to 8259.

**/
EFI_STATUS
EFIAPI 
Interrupt8259SetMode (
  IN  H2O_LEGACY_8259_PPI             *This,
  IN  EFI_8259_MODE                   Mode,
  IN  UINT16                          *Mask OPTIONAL,
  IN  UINT16                          *EdgeLevel OPTIONAL
  );

/**
  Convert from IRQ to processor interrupt vector number.

  @param[in]  This                  The PPI instance pointer.
  @param[in]  Irq                   8259 IRQ0 - IRQ15.
  @param[in]  Vector                The processor vector number that matches an Irq.

  @retval     EFI_SUCCESS           The Vector matching Irq is returned.
  @retval     EFI_INVALID_PARAMETER The Irq not valid.

**/
EFI_STATUS
EFIAPI 
Interrupt8259GetVector (
  IN  H2O_LEGACY_8259_PPI               *This,
  IN  EFI_8259_IRQ                      Irq,
  OUT UINT8                             *Vector
  );

/**
  Enables the specified IRQ.

  @param[in]  This            The PPI instance pointer.
  @param[in]  Irq             IRQ0-IRQ15.
  @param[in]  LevelTriggered  0 = Edge triggered; 1 = Level triggered.
 
  @retval     EFI_SUCCESS            The Irq was enabled on the 8259 PIC.
  @retval     EFI_INVALID_PARAMETER  The Irq is not valid.
 
**/
EFI_STATUS
EFIAPI
Interrupt8259EnableIrq (
  IN  H2O_LEGACY_8259_PPI               *This,
  IN  EFI_8259_IRQ                      Irq,
  IN  BOOLEAN                           LevelTriggered
  );

/**
  Disables the specified IRQ.
  
  @param[in]  This  The PPI instance pointer.
  @param[in]  Irq   IRQ0-IRQ15.
 
  @retval     EFI_SUCCESS            The Irq was disabled on the 8259 PIC.
  @retval     EFI_INVALID_PARAMETER  The Irq is not valid.
 
**/
EFI_STATUS
EFIAPI
Interrupt8259DisableIrq (
  IN  H2O_LEGACY_8259_PPI               *This,
  IN  EFI_8259_IRQ                      Irq
  );

/**
  PciHandle represents a PCI config space of a PCI function. Vector
  represents Interrupt Pin (from PCI config space) and it is the data
  that is programmed into the Interrupt Line (from the PCI config space)
  register.

  @param[in]   This                  The PPI instance pointer.
  @param[in]   PciHandle             The PCI function to return the vector for.
  @param[out]  Vector                The vector for the function it matches.

  @retval      EFI_SUCCESS           A valid Vector was returned.
  @retval      EFI_INVALID_PARAMETER PciHandle not valid.

**/
EFI_STATUS
EFIAPI 
Interrupt8259GetInterruptLine (
  IN  H2O_LEGACY_8259_PPI               *This,
  IN  EFI_HANDLE                        PciHandle,
  OUT UINT8                             *Vector
  );

/**
  Issues the End of Interrupt (EOI) commands to PICs.
  
  @param[in]  This  The PPI instance pointer.
  @param[in]  Irq   The interrupt for which to issue the EOI command.

  @retval     EFI_SUCCESS            The EOI command was issued.
  @retval     EFI_INVALID_PARAMETER  The Irq is not valid.

**/
EFI_STATUS
EFIAPI
Interrupt8259EndOfInterrupt (
  IN  H2O_LEGACY_8259_PPI               *This,
  IN  EFI_8259_IRQ                      Irq
  );

#endif
