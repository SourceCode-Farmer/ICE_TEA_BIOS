/** @file
  Memory Thermal Initialization Driver.

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <ChipsetSetupConfig.h>
#include <SaAccess.h>
#include <Source/Include/MrcRegisters/Msa.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/PciLib.h>
#include <Library/DxeInsydeChipsetLib.h>

EFI_STATUS
MemoryThermalEntryPoint (
  IN EFI_HANDLE                      ImageHandle,
  IN EFI_SYSTEM_TABLE                *SystemTable
  )
{
  EFI_STATUS                         Status;
  SA_SETUP                           *SaSetup;

  UINT16                             Data16;
  UINT32                             Data32;

  Status  = EFI_SUCCESS;
  Data16  = 0;
  Data32  = 0;
  SaSetup = NULL;

  SaSetup = AllocateZeroPool (sizeof (SA_SETUP));
  if (SaSetup == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = GetChipsetSaSetupVariableDxe (SaSetup, sizeof (SA_SETUP));
  if (EFI_ERROR (Status)) {
    FreePool (SaSetup);
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Data32 = (BIT0 << PCU_CR_DDR_PTM_CTL_PCU_PDWN_CONFIG_CTL_OFF);

  if (SaSetup->EnableExtts) {
    Data32 |= (BIT0 << PCU_CR_DDR_PTM_CTL_PCU_EXTTS_ENABLE_OFF);
  }
  if (SaSetup->EnableCltm) {
    Data32 |= (BIT0 << PCU_CR_DDR_PTM_CTL_PCU_CLTM_ENABLE_OFF);
  }
  if (SaSetup->EnableOltm) {
    Data32 |= (BIT0 << PCU_CR_DDR_PTM_CTL_PCU_OLTM_ENABLE_OFF);
  }
  McMmio32Or (PCU_CR_DDR_PTM_CTL_PCU_REG, Data32);

  Data16 = (SaSetup->WarmThresholdCh0Dimm0 << PCU_CR_DDR_WARM_THRESHOLD_CH0_PCU_DIMM0_OFF) |
           (SaSetup->WarmThresholdCh0Dimm1 << PCU_CR_DDR_WARM_THRESHOLD_CH0_PCU_DIMM1_OFF);
  McMmio16AndThenOr(PCU_CR_DDR_WARM_THRESHOLD_CH0_PCU_REG, 0,Data16);

  Data16 = (SaSetup->WarmThresholdCh1Dimm0 << PCU_CR_DDR_WARM_THRESHOLD_CH1_PCU_DIMM0_OFF) |
           (SaSetup->WarmThresholdCh1Dimm1 << PCU_CR_DDR_WARM_THRESHOLD_CH1_PCU_DIMM1_OFF);
  McMmio16AndThenOr(PCU_CR_DDR_WARM_THRESHOLD_CH1_PCU_REG, 0,Data16);


  Data16 = (SaSetup->HotThresholdCh0Dimm0 << PCU_CR_DDR_HOT_THRESHOLD_CH0_PCU_DIMM0_OFF) |
           (SaSetup->HotThresholdCh0Dimm1 << PCU_CR_DDR_HOT_THRESHOLD_CH0_PCU_DIMM1_OFF);
  McMmio16AndThenOr(PCU_CR_DDR_HOT_THRESHOLD_CH0_PCU_REG, 0,Data16);

  Data16 = (SaSetup->HotThresholdCh1Dimm0 << PCU_CR_DDR_HOT_THRESHOLD_CH1_PCU_DIMM0_OFF) |
           (SaSetup->HotThresholdCh1Dimm1 << PCU_CR_DDR_HOT_THRESHOLD_CH1_PCU_DIMM1_OFF);
  McMmio16AndThenOr (PCU_CR_DDR_HOT_THRESHOLD_CH1_PCU_REG, 0, Data16);

  Data16 = (SaSetup->WarmBudgetCh0Dimm0 << PCU_CR_DDR_WARM_BUDGET_CH0_PCU_DIMM0_OFF) |
           (SaSetup->WarmBudgetCh0Dimm1 << PCU_CR_DDR_WARM_BUDGET_CH0_PCU_DIMM1_OFF);
  McMmio16AndThenOr (PCU_CR_DDR_WARM_BUDGET_CH0_PCU_REG, 0, Data16);

  Data16 = (SaSetup->WarmBudgetCh1Dimm0 << PCU_CR_DDR_WARM_BUDGET_CH1_PCU_DIMM0_OFF) |
           (SaSetup->WarmBudgetCh1Dimm1 << PCU_CR_DDR_WARM_BUDGET_CH1_PCU_DIMM1_OFF);
  McMmio16AndThenOr (PCU_CR_DDR_WARM_BUDGET_CH1_PCU_REG, 0, Data16);

  Data16 = (SaSetup->HotBudgetCh0Dimm0 << PCU_CR_DDR_HOT_BUDGET_CH0_PCU_DIMM0_OFF) |
           (SaSetup->HotBudgetCh0Dimm1 << PCU_CR_DDR_HOT_BUDGET_CH0_PCU_DIMM1_OFF);
  McMmio16AndThenOr (PCU_CR_DDR_HOT_BUDGET_CH0_PCU_REG, 0, Data16);

  Data16 = (SaSetup->HotBudgetCh1Dimm0 << PCU_CR_DDR_HOT_BUDGET_CH1_PCU_DIMM0_OFF) |
           (SaSetup->HotBudgetCh1Dimm1 << PCU_CR_DDR_HOT_BUDGET_CH1_PCU_DIMM1_OFF);
  McMmio16AndThenOr (PCU_CR_DDR_HOT_BUDGET_CH1_PCU_REG, 0, Data16);

  // gino: new RC already remove
  Data32 = (SaSetup->RaplLim1Pwr & PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT1_POWER_MSK) |
            (SaSetup->RaplLim1Ena << PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT1_ENABLE_OFF) |
            (SaSetup->RaplLim1WindX << PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_X_OFF) |
            (SaSetup->RaplLim1WindY << PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_Y_OFF);
  // Data32 =  (SaSetup->RaplLim1Ena << PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT1_ENABLE_OFF) |
  //           (SaSetup->RaplLim1WindX << PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_X_OFF) |
  //           (SaSetup->RaplLim1WindY << PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_Y_OFF);

  McMmio32Or (PCU_CR_DDR_RAPL_LIMIT_PCU_REG, Data32);

  Data32 = (SaSetup->RaplLim2Pwr & PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT1_POWER_MSK) |
            (SaSetup->RaplLim2Ena << (PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT2_ENABLE_OFF - 32)) |
            (SaSetup->RaplLim2WindX << (PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_X_OFF - 32)) |
            (SaSetup->RaplLim2WindY << (PCU_CR_DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_Y_OFF - 32));

  McMmio32Or (PCU_CR_DDR_RAPL_LIMIT_PCU_REG + 4, Data32);

  Data32 = McMmio32(PCU_CR_DDR_RAPL_LIMIT_PCU_REG + 4);
  Data32 |= (SaSetup->RaplLim2Lock << (PCU_CR_DDR_RAPL_LIMIT_PCU_LOCKED_OFF - 32));
  McMmio32Or (PCU_CR_DDR_RAPL_LIMIT_PCU_REG + 4, Data32);

  gBS->FreePool (SaSetup);
  return EFI_SUCCESS;

}
