## @file
#  Platform Package Description file
#
#******************************************************************************
#* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##

[Defines]
DEFINE    CHIPSET_PKG                     = AlderLakeChipsetPkg
DEFINE    PLATFORMFSP_PACKAGE             = AlderLakeFspPkg
#[-start-200917-IB06462159-remove]#
#
# Remove definition for PLATFORM_FSP_BIN_PACKAGE, because that we need to define our owned path
#
# #[-start-200206-IB16740086-add]#
# DEFINE    PLATFORM_FSP_BIN_PACKAGE        = AlderLakeFspBinPkg
# #[-end-200206-IB16740086-add]#
#[-end-200917-IB06462159-remove]#
DEFINE    CLIENT_COMMON_PACKAGE           = ClientCommonPkg
#[-start-190708-IB11270240-add]#
DEFINE    MIN_PLATFORM_PACKAGE            = MinPlatformPkg
#[-end-190708-IB11270240-add]#
DEFINE    C1S_PRODUCT_PATH                = ClientOneSiliconPkg/Product/AlderLake
DEFINE    PLATFORMSAMPLE_PACKAGE          = AlderLakePlatSamplePkg
DEFINE    PLATFORM_BOARD_PACKAGE          = AlderLakeBoardPkg
!include $(CHIPSET_PKG)/Package.env
!include $(PROJECT_PKG)/Project.env

[Packages]

[PcdsFeatureFlag]
  gInsydeTokenSpaceGuid.PcdH2OResetSupported|TRUE
#[-start-200121-IB10189023-add]#
  gInsydeTokenSpaceGuid.PcdH2OBdsCpBootDeviceEnumAfterSupported|TRUE
  gInsydeTokenSpaceGuid.PcdH2OBdsCpBootDeviceEnumBeforeSupported|TRUE
#[-end-200121-IB10189023-add]#

  gInsydeTokenSpaceGuid.PcdH2ODxeCpPciHpcGetResourcePaddingSupported|TRUE

  gInsydeTokenSpaceGuid.PcdH2OHddPasswordPeiSupported|TRUE

[Libraries]

[LibraryClasses]
  H2OSpiAccessLib|$(CHIPSET_PKG)/Library/FlashDeviceSupport/SpiAccessLib/SpiAccessLib.inf
  FlashWriteEnableLib|InsydeModulePkg/Library/FlashDeviceSupport/FlashWriteEnableLibNull/FlashWriteEnableLibNull.inf
  SpiAccessInitLib|$(CHIPSET_PKG)/Library/FlashDeviceSupport/DxeSmmSpiAccessInitLib/DxeSmmSpiAccessInitLib.inf
  PlatformBdsLib|$(CHIPSET_PKG)/Library/PlatformBdsLib/PlatformBdsLib.inf
  BaseOemSvcChipsetLib|$(CHIPSET_PKG)/Library/BaseOemSvcChipsetLib/BaseOemSvcChipsetLib.inf
  BaseOemSvcChipsetLibDefault|$(CHIPSET_PKG)/Library/BaseOemSvcChipsetLib/BaseOemSvcChipsetLibDefault.inf
  SortLib|MdeModulePkg/Library/UefiSortLib/UefiSortLib.inf
  ShellLib|ShellPkg/Library/UefiShellLib/UefiShellLib.inf
  BootGuardPlatformLib|$(CHIPSET_PKG)/Library/BootGuardPlatformLib/BootGuardPlatformLib.inf
  FitPlatformLib|$(CHIPSET_PKG)/Library/FitPlatformLib/FitPlatformLib.inf
  BaseInsydeChipsetLib|$(CHIPSET_PKG)/Library/BaseInsydeChipsetLib/BaseInsydeChipsetLib.inf
  BasePciLibPciExpress|MdePkg/Library/BasePciLibPciExpress/BasePciLibPciExpress.inf
  PciLib|MdePkg/Library/BasePciLibPciExpress/BasePciLibPciExpress.inf
  TcssRetimerNvmUpdateLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/TbtRetimerNvmUpdateLib/TbtRetimerNvmUpdateLib.inf
  GenericBdsLib|InsydeModulePkg/Library/GenericBdsLib/GenericBdsLib.inf {
    <SOURCE_OVERRIDE_PATH>
      $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Library/GenericBdsLib
  }
  SetupUtilityLib|InsydeModulePkg/Library/SetupUtilityLib/SetupUtilityLib.inf {
    <SOURCE_OVERRIDE_PATH>
      $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Library/SetupUtilityLib
  }

#[-start-180509-IB11270200-modify]#
  BaseTraceHubDebugLib|$(CHIPSET_PKG)/Library/BaseTraceHubDebugLibNull/BaseTraceHubDebugLibNull.inf
  DebugPrintErrorLevelLib|MdePkg/Library/BaseDebugPrintErrorLevelLib/BaseDebugPrintErrorLevelLib.inf
#[-end-180509-IB11270200-modify]#

  BaseSetupDefaultLib|InsydeModulePkg/Library/SetupDefaultLib/SetupDefaultLib.inf {
    <SOURCE_OVERRIDE_PATH>
      $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Library/SetupDefaultLib
  }
  UefiBootManagerLib|$(CHIPSET_PKG)/Library/UefiBootManagerLib/UefiBootManagerLib.inf
  ChipsetCapsuleLib|$(CHIPSET_PKG)/CapsuleIFWU/CapsuleLib/ChipsetCapsuleLib.inf
  ChipsetCapsuleRecoveryLib|$(CHIPSET_PKG)/CapsuleIFWU/CapsuleLib/ChipsetCapsuleRecoveryLib.inf
  ChipsetSignatureLib|$(CHIPSET_PKG)/CapsuleIFWU/CapsuleLib/ChipsetSignatureLib.inf
###
### Override
###
#[-start-201220-IB16740127-remove]# The judgment was removed in RC1513
#!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
#[-end-201220-IB16740127-remove]#
  FspCommonLib|IntelFsp2Pkg/Library/BaseFspCommonLib/BaseFspCommonLib.inf
  FspWrapperApiLib|IntelFsp2WrapperPkg/Library/BaseFspWrapperApiLib/BaseFspWrapperApiLib.inf
  FspWrapperApiTestLib|IntelFsp2WrapperPkg/Library/PeiFspWrapperApiTestLib/PeiFspWrapperApiTestLib.inf
  CacheAsRamLib|IntelFsp2Pkg/Library/BaseCacheAsRamLibNull/BaseCacheAsRamLibNull.inf
#[-start-201220-IB16740127-remove]# The judgment was removed in RC1513
#!endif
#[-end-201220-IB16740127-remove]#
  FspMeasurementLib|IntelFsp2WrapperPkg/Library/BaseFspMeasurementLib/BaseFspMeasurementLib.inf

  IoApicLib|PcAtChipsetPkg/Library/BaseIoApicLib/BaseIoApicLib.inf
  SmiHandlerProfileLib|MdeModulePkg/Library/SmmSmiHandlerProfileLib/SmmSmiHandlerProfileLib.inf
  TraceHubHookLib|$(PLATFORM_SI_PACKAGE)/IpBlock/TraceHub/Library/PeiDxeSmmTraceHubHookLib/PeiDxeSmmTraceHubHookLib.inf
  FlashDevicesLib|InsydeFlashDevicePkg/Library/FlashDevicesLib/FlashDevicesLib.inf {
    <SOURCE_OVERRIDE_PATH>
      $(CHIPSET_PKG)/Override/Insyde/InsydeFlashDevicePkg/Library/FlashDevicesLib
  }
  SiMtrrLib|$(PLATFORM_SI_PACKAGE)/Library/SiMtrrLib/SiMtrrLib.inf
#
# WifiConnection
#
  FileExplorerLib|MdeModulePkg/Library/FileExplorerLib/FileExplorerLib.inf

!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE || (gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2)
  SeamlessRecoveryLib|$(CHIPSET_PKG)/Library/SeamlessRecoveryLibSysFw/SeamlessRecoveryLib.inf
!endif

  OemGraphicsLib|InsydeModulePkg/Library/OemGraphicsLib/OemGraphicsLib.inf

[LibraryClasses.common]
  PciHostBridgeLib|$(MIN_PLATFORM_PACKAGE)/Pci/Library/PciHostBridgeLibSimple/PciHostBridgeLibSimple.inf
  #
  # For RemotePlatformErasef. RC1353 new add.
  #
  PlatformPasswordLib|UserAuthFeaturePkg/Library/PlatformPasswordLibNull/PlatformPasswordLibNull.inf
  UserPasswordLib|UserAuthFeaturePkg/Library/UserPasswordLib/UserPasswordLib.inf
  UserPasswordUiLib|UserAuthFeaturePkg/Library/UserPasswordUiLib/UserPasswordUiLib.inf

[LibraryClasses.common.SEC]
  SecOemSvcChipsetLib|$(CHIPSET_PKG)/Library/SecOemSvcChipsetLib/SecOemSvcChipsetLib.inf
  SecOemSvcChipsetLibDefault|$(CHIPSET_PKG)/Library/SecOemSvcChipsetLib/SecOemSvcChipsetLibDefault.inf

[LibraryClasses.common.PEI_CORE]
!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2
  FlashRegionLib|$(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Library/FlashRegionLib/PeiFlashRegionLib/PeiFlashRegionLib.inf
!endif

[LibraryClasses.common.PEIM]
!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2
  FlashRegionLib|$(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Library/FlashRegionLib/PeiFlashRegionLib/PeiFlashRegionLib.inf
!endif
  TimerLib|$(CHIPSET_PKG)/Override/EDK2/PcAtChipsetPkg/Library/TscAcpiTimerLib/PeiTscTimerLib.inf
  KernelConfigLib|$(CHIPSET_PKG)/Library/PeiKernelConfigLib/PeiKernelConfigLib.inf
  PeiOemSvcChipsetLib|$(CHIPSET_PKG)/Library/PeiOemSvcChipsetLib/PeiOemSvcChipsetLib.inf
  PeiOemSvcChipsetLibDefault|$(CHIPSET_PKG)/Library/PeiOemSvcChipsetLib/PeiOemSvcChipsetLibDefault.inf
  PeiInsydeChipsetLib|$(CHIPSET_PKG)/Library/PeiInsydeChipsetLib/PeiInsydeChipsetLib.inf
  SpiAccessInitLib|$(CHIPSET_PKG)/Library/FlashDeviceSupport/PeiSpiAccessInitLib/PeiSpiAccessInitLib.inf
  TpmMeasurementLib|SecurityPkg/Library/PeiTpmMeasurementLib/PeiTpmMeasurementLib.inf
  TcgEventLogRecordLib|SecurityPkg/Library/TcgEventLogRecordLib/TcgEventLogRecordLib.inf
###
### Override
###
  MultiPlatSupportLib|$(CHIPSET_PKG)/Library/PeiMultiPlatSupportLib/PeiMultiPlatSupportLibOptSize.inf

  FspSwitchStackLib|IntelFsp2Pkg/Library/BaseFspSwitchStackLib/BaseFspSwitchStackLib.inf

!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE || gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport != 0
  SeamlessRecoveryLib|$(CHIPSET_PKG)/Library/PeiSeamlessRecoveryLibSysFw/SeamlessRecoveryLib.inf
!endif

!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE || (gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2 && gChipsetPkgTokenSpaceGuid.PcdUcodeCapsuleUpdateSupported == TRUE) || (gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2 && gChipsetPkgTokenSpaceGuid.PcdBtGAcmCapsuleUpdateSupported == TRUE)
  FirmwareAuthenticationLib|$(CHIPSET_PKG)/Library/FirmwareAuthenticationLibSysFw/FirmwareAuthenticationLib.inf
!endif

[LibraryClasses.common.DXE_CORE]

[LibraryClasses.common.DXE_RUNTIME_DRIVER]
  DxeOemSvcChipsetLib|$(CHIPSET_PKG)/Library/DxeOemSvcChipsetLib/DxeOemSvcChipsetLib.inf
  DxeOemSvcChipsetLibDefault|$(CHIPSET_PKG)/Library/DxeOemSvcChipsetLib/DxeOemSvcChipsetLibDefault.inf

  TimerLib|$(CHIPSET_PKG)/Override/EDK2/PcAtChipsetPkg/Library/TscAcpiTimerLib/DxeTscTimerLib.inf

[LibraryClasses.common.UEFI_DRIVER]
  DxeOemSvcChipsetLib|$(CHIPSET_PKG)/Library/DxeOemSvcChipsetLib/DxeOemSvcChipsetLib.inf
  DxeOemSvcChipsetLibDefault|$(CHIPSET_PKG)/Library/DxeOemSvcChipsetLib/DxeOemSvcChipsetLibDefault.inf
  PcdLib|MdePkg/Library/DxePcdLib/DxePcdLib.inf
  DxeInsydeChipsetLib|$(CHIPSET_PKG)/Library/DxeInsydeChipsetLib/DxeInsydeChipsetLib.inf

[LibraryClasses.common.DXE_DRIVER]
#  Replace TscAcpiTimerLib with AcpiTimerLib.
#  TimerLib|$(CHIPSET_PKG)/Override/EDK2/PcAtChipsetPkg/Library/TscAcpiTimerLib/DxeTscTimerLib.inf
  TimerLib|PcAtChipsetPkg/Library/AcpiTimerLib/DxeAcpiTimerLib.inf
  DxeInsydeChipsetLib|$(CHIPSET_PKG)/Library/DxeInsydeChipsetLib/DxeInsydeChipsetLib.inf
  DxeOemSvcChipsetLib|$(CHIPSET_PKG)/Library/DxeOemSvcChipsetLib/DxeOemSvcChipsetLib.inf
  DxeOemSvcChipsetLibDefault|$(CHIPSET_PKG)/Library/DxeOemSvcChipsetLib/DxeOemSvcChipsetLibDefault.inf

!if !gInsydeCrTokenSpaceGuid.PcdH2OConsoleRedirectionSupported
  CrConfigDefaultLib|$(CHIPSET_PKG)/Library/CrConfigDefaultLibNull/CrConfigDefaultLibNull.inf
  CrVfrConfigLib|$(CHIPSET_PKG)/Library/CrVfrConfigLibNull/CrVfrConfigLibNull.inf
!endif

[LibraryClasses.common.DXE_SMM_DRIVER]
  SmmOemSvcChipsetLib|$(CHIPSET_PKG)/Library/SmmOemSvcChipsetLib/SmmOemSvcChipsetLib.inf
  SmmOemSvcChipsetLibDefault|$(CHIPSET_PKG)/Library/SmmOemSvcChipsetLib/SmmOemSvcChipsetLibDefault.inf
  PcdLib|MdePkg/Library/DxePcdLib/DxePcdLib.inf
  SmmHeciLib|$(CHIPSET_PKG)/Library/SmmHeciLib/SmmHeciLib.inf
###
### Override
###
  SmmMemLib|MdePkg/Library/SmmMemLib/SmmMemLib.inf {
    <SOURCE_OVERRIDE_PATH>
      MdePkg/Override/Library/SmmMemLib
  }

[LibraryClasses.common.COMBINED_SMM_DXE]
  SmmOemSvcChipsetLib|$(CHIPSET_PKG)/Library/SmmOemSvcChipsetLib/SmmOemSvcChipsetLib.inf
  SmmOemSvcChipsetLibDefault|$(CHIPSET_PKG)/Library/SmmOemSvcChipsetLib/SmmOemSvcChipsetLibDefault.inf
#[-start-180420-IB11270199-add]#
  DxeAsfLib|$(CHIPSET_PKG)/Library/DxeAsfLibNull/DxeAsfLibNull.inf
#[-end-180420-IB11270199-add]#

[LibraryClasses.common.SMM_CORE]
  TimerLib|$(CHIPSET_PKG)/Override/EDK2/PcAtChipsetPkg/Library/TscAcpiTimerLib/DxeTscTimerLib.inf

[LibraryClasses.common.UEFI_APPLICATION]

[LibraryClasses.common.SMM_CORE]
  #
  # This library instance halts TCO timer to stall watchdog when booting into OS and entering
  # SMM mode debug in DDT. Enable TCO timer to count when exiting SMM mode
  #
!if $(INSYDE_DEBUGGER) == YES
  SmmCorePlatformHookLib|$(CHIPSET_PKG)/Library/SmmCorePlatformHookLib/SmmCorePlatformHookLib.inf
!endif
[LibraryClasses.X64]
  #
  # For RemotePlatformErasef. RC1353 new add.
  #
  BootLogoLib|MdeModulePkg/Library/BootLogoLib/BootLogoLib.inf
  TcgPhysicalPresenceLib|SecurityPkg/Library/DxeTcgPhysicalPresenceLib/DxeTcgPhysicalPresenceLib.inf

[PcdsFeatureFlag]
  #
  # The PCD for Microsoft HID over I2C devices, the bits definition below
  #
  # Bit  0 ~  6 : Slave address
  # Bit  7      : Interrupt GPIO pin active level, 0 = low active, 1 = high active
  # Bit  8 ~ 15 : Interrupt GPIO pin number
  # Bit 16 ~ 31 : HID descriptor register number
  # Bit 32 ~ 47 : Device type,
  #               0x0000 -> (AutoDetect)
  #               0x0d00 -> Touch panel
  #               0x0102 -> Mouse
  #               0x0106 -> Keyboard
  # Bit 48 ~ 51 : Host controller number
  #               0x00      -> (AutoDetect)
  #               0x01~0x0f -> I2C host controller 0~14 (One based)
  # Bit 52 ~ 55 : Device Speed Override
  #               0x01      -> V_SPEED_STANDARD
  #               0x02      -> V_SPEED_FAST
  #               0x03      -> V_SPEED_HIGH
  #
  #
  # Required checkpoints
  #
  gInsydeTokenSpaceGuid.PcdH2OPeiCpInitChipsetPolicySupported|TRUE
  gInsydeTokenSpaceGuid.PcdH2OPeiCpSetBootModeBeforeSupported|TRUE
  gInsydeTokenSpaceGuid.PcdH2OBdsCpRecoveryCompleteSupported|TRUE
  gInsydeTokenSpaceGuid.PcdH2OBdsCpSendFormAfterSupported|TRUE
  gInsydeTokenSpaceGuid.PcdH2OPeiCpPciEnumUpdateDevResourcesSupported|TRUE
#[-start-190613-IB16990064-add]#
  gInsydeTokenSpaceGuid.PcdH2OPeiCpCrisisRecoveryPublishFvSupported|TRUE
#[-end-190613-IB16990064-add]#

  #
  # Always set TRUE.
  # Set FALSE:Fwblockservice.c will install gEfiFirmwareVolumeBlockProtocolGuid, it will change driver dispath sequence.
  #
  gInsydeTokenSpaceGuid.PcdFvbAccessThroughSmi|TRUE
  gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag|$(USE_INTEL_CRB_H8_EC)

  gChipsetPkgTokenSpaceGuid.PcdEnableEconFlag|$(ENABLE_ASL_ECON)

  gEfiIntelFrameworkModulePkgTokenSpaceGuid.PcdStatusCodeUseDataHub|TRUE
  gInsydeTokenSpaceGuid.PcdEndOfDxeEventSupported|FALSE
  gChipsetPkgTokenSpaceGuid.PcdHstiSupported|TRUE
  gH2OFlashDeviceEnableGuid.PcdCommonvidCommondidSpiEnable|FALSE
  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmEnableBspElection|FALSE
  gChipsetPkgTokenSpaceGuid.PcdMeCapsuleUpdateSupported|TRUE
  gChipsetPkgTokenSpaceGuid.PcdIshCapsuleUpdateSupported|TRUE
  gChipsetPkgTokenSpaceGuid.PcdPdtCapsuleUpdateSupported|TRUE
  gChipsetPkgTokenSpaceGuid.PcdEcCapsuleUpdateSupported|TRUE
  gChipsetPkgTokenSpaceGuid.PcdIomCapsuleUpdateSupported|TRUE
  gChipsetPkgTokenSpaceGuid.PcdMgPhyCapsuleUpdateSupported|TRUE
  gChipsetPkgTokenSpaceGuid.PcdTbtCapsuleUpdateSupported|TRUE

#
# Please notice that pcd PcdTcssPartialUpdateEnable must be same as ME setting "Tcss - Partial Update Enable".
#
  gChipsetPkgTokenSpaceGuid.PcdTcssPartialUpdateEnable|FALSE
  gInsydeTokenSpaceGuid.PcdH2OIhisiCmdBufferSupported|TRUE

#
# Reduce POST Time when issuing device reset, soluiton IB05300634 from KBL.
#
  gEfiMdeModulePkgTokenSpaceGuid.PcdPs2KbdExtendedVerification|FALSE
  gEfiMdeModulePkgTokenSpaceGuid.PcdPs2MouseExtendedVerification|FALSE
  gInsydeTokenSpaceGuid.PcdH2ODxeCpUpdateAcpiDescHdrSupported|TRUE
  gChipsetPkgTokenSpaceGuid.PcdUefiWirelessCnvtEnable|FALSE
  gInsydeTokenSpaceGuid.PcdH2OPeiCpDxeFvCorruptedSupported|TRUE
  gChipsetPkgTokenSpaceGuid.PcdBiosGuardEcSupport|FALSE

[PcdsFixedAtBuild]
!if $(TARGET) == DEBUG
  gSiPkgTokenSpaceGuid.PcdSerialIoUartDebugEnable|1
!endif
  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmStackSize|0x20000
#[-start-200225-IB14630334-add]#
  gPlatformModuleTokenSpaceGuid.PcdRamDebugEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdSerialPortEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable|FALSE  # Serial debug to USB3 port and DbC is mutually exclusive. The major use is DbC thus set FALSE by default.
  gSiPkgTokenSpaceGuid.PcdSerialIoUartEnable|TRUE
#[-end-200225-IB14630334-add]#
#[-start-180830-IB15590135-add]#
!if $(TARGET) == DEBUG
  ## This flag is used to initialize debug output interface.
  #  BIT0 - RAM debug interface.
  #  BIT1 - UART debug interface.
  #  BIT2 - USB debug interface.
  #  BIT3 - USB3 debug interface.
  #  BIT4 - Serial IO debug interface.
  #  BIT5 - TraceHub debug interface.
  #  BIT6 - Reserved.
  #  BIT7 - CMOS control.
!endif
#[-end-180830-IB15590135-add]#

### API mode ##
#            PCD        FSP     Platform
#  PcdFspBinaryEnable   TRUE     FALSE
#  PcdFspModeSelection  TRUE     TRUE
#
#
### Non-API mode ##
#            PCD        FSP     Platform
#  PcdFspBinaryEnable   TRUE     FALSE
#  PcdFspModeSelection  FALSE    FALSE
#
#[-start-190115-IB10182006-modify]#
#[-start-180815-IB11270209-modify]#
#[-start-200420-IB17800056-7-modify]#
  gSiPkgTokenSpaceGuid.PcdFspBinaryEnable|FALSE
  #gSiPkgTokenSpaceGuid.PcdFspBinaryEnable|TRUE
#[-end-200420-IB17800056-7-modify]#
#[-end-180815-IB11270209-modify]#
#[-end-190115-IB10182006-modify]#
  gSiPkgTokenSpaceGuid.PcdTemporaryRamBase|0xFEF80000
  gSiPkgTokenSpaceGuid.PcdTemporaryRamSize|0x00040000

  ## Stack size in the temporary RAM.
  #  0 means half of PcdTemporaryRamSizeWhenRecovery.
  #
  #  WARNING:
  #  If you change the value of PcdPeiTemporaryRamStackSizeWhenRecovery,
  #  you also need to adjust the value of gChipsetPkgTokenSpaceGuid.PcdNemDataStackSize.
  #  These PCDs values must be same.
  gInsydeTokenSpaceGuid.PcdPeiTemporaryRamStackSizeWhenRecovery|0x0

!if gInsydeTokenSpaceGuid.PcdH2ODdtSupported
  #
  # Insyde DDT needs the PcdCpuSmmCodeAccessCheckEnable to be set as FALSE.
  #
  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmCodeAccessCheckEnable|FALSE
!endif

  gEfiMdeModulePkgTokenSpaceGuid.PcdPeiCoreMaxPeiStackSize|0x40000

!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == FALSE
  gUefiCpuPkgTokenSpaceGuid.PcdPeiTemporaryRamStackSize|0x25000
!else

!endif
!if $(FIRMWARE_PERFORMANCE) == YES
  gUefiCpuPkgTokenSpaceGuid.PcdPeiTemporaryRamStackSize|0xCF80  # (PcdTemporaryRamSize - PcdFspTemporaryRamSize)/2 - 0x1000
!endif

  gInsydeTokenSpaceGuid.PcdTemporaryRamSizeWhenRecovery|0x40000
  gInsydeTokenSpaceGuid.PcdUmaDeviceNumber|0x02                 # 0xFF no Uma port.
  gInsydeTokenSpaceGuid.PcdPegDeviceNumber|0x01                 # 0xFF no Peg port.

# !if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
#   gIntelFsp2WrapperTokenSpaceGuid.PcdFsptBaseAddress|0xFFE80000
# !endif

  gEfiMdeModulePkgTokenSpaceGuid.PcdSmiHandlerProfilePropertyMask|1

  ## Indicates if to shadow PEIM on S3 boot path after memory is ready.<BR><BR>
  #   TRUE  - Shadow PEIM on S3 boot path after memory is ready.<BR>
  #   FALSE - Not shadow PEIM on S3 boot path after memory is ready.<BR>
  # @Prompt Shadow Peim On S3 Boot.
  gEfiMdeModulePkgTokenSpaceGuid.PcdShadowPeimOnS3Boot|TRUE

!if $(PCI_EXPRESS_SIZE) == 256
  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress|0xC0000000
  gChipsetPkgTokenSpaceGuid.PcdPciExpressSize|256
!else
  #
  # Size = 128 or 64 , PciExpressBaseAddress both set as 0xF0000000
  #
  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress|0xF0000000
  gChipsetPkgTokenSpaceGuid.PcdPciExpressSize|$(PCI_EXPRESS_SIZE)

!if $(PCI_EXPRESS_SIZE) == 128
  gChipsetPkgTokenSpaceGuid.PcdPciExpressMaxBusNumber|0x7F
!else
  gChipsetPkgTokenSpaceGuid.PcdPciExpressMaxBusNumber|0x3F
!endif
!endif

  gSiPkgTokenSpaceGuid.PcdRegBarBaseAddress|0xFB000000

!if gSiPkgTokenSpaceGuid.PcdPpamEnable == TRUE
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMsegSize|0x80000
!endif

  #
  # Enable PcdShadowPeimOnS3Boot to load PEIM into memory during S3 resume
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdShadowPeimOnS3Boot|TRUE

  #
  # CMOS Debug Code Table
  #
  gInsydeTokenSpaceGuid.PcdPeiChipsetDebugCodeTable|{ \
    GUID("145971E9-AD52-4094-A8C8-BE5B3FECC82D"), 0x40, 0x02, \ #CpuPeim
    GUID("C866BD71-7C79-4BF1-A93B-066B830D8F9A"), 0x40, 0x06, \ #Init MP for S3
    GUID("ABB74F50-FD2D-4072-A321-CAFC72977EFA"), 0x40, 0x07, \ #PeiSmmRelocate
    GUID("A89EC8E0-0BA1-40AA-A03E-ABDDA5295CDE"), 0x43, 0x02, \ #PciExpress
    GUID("D4EE25EA-0B48-43AE-A016-4D6E8B6C43B3"), 0x43, 0x04, \ #CantigaMemoryInit
    GUID("921B35BF-0255-4722-BF5A-5B8B69093593"), 0x46, 0x02, \ #IchInit
    GUID("64f3DF77-F312-42ED-81CC-1B1F57E18AD6"), 0x46, 0x03, \ #IchSmbusArpDisabled
    GUID("C5442B7B-58CF-4375-B152-4383132607FC"), 0x46, 0x05, \ #For recover PEIM
    GUID("C3619722-EF37-4605-8C35-C7A37B51EDD2"), 0x49, 0x02, \ #ClockGen and ck505
    GUID("50EE664D-7703-42C3-9E69-8C89DE70D1D5"), 0x4C, 0x02, \ #SioInit
    GUID("8A78B107-0FDD-4CC8-B7BA-DC3E13CB8524"), 0x4C, 0x03, \ #PeiCpuIoPciCfg
    GUID("A85027FC-0E09-4FA9-A407-CAD206FB4F1D"), 0x4C, 0x04, \ #PlatformStage1
    GUID("1191BBF5-DCB9-44F4-827E-95359744C987"), 0x4C, 0x05, \ #PlatformStage2
    GUID("5479E09C-2E74-481B-89F8-B0172E388D1F"), 0x4F, 0x01, \ #Start Watch Dog PEIM driver
    GUID("4862AFF3-667C-5458-B274-A1C62DF8BA80"), 0x4F, 0x02, \ #Framework PEIM to HECI
    GUID("CA9D8617-D652-403B-B6C5-BA47570116AD"), 0x50, 0x01, \ #Txt PEIM
    GUID("AE265864-CF5D-41A8-913D-71C155E76442"), 0x51, 0x0D, \ #CpuIoPei
    GUID("E2FC5838-16A9-4ED7-96E0-9A75F5AF711D"), 0x51, 0x0F, \ #EmuPeiHelperPei
    GUID("EAA96391-9BE3-4488-8AF3-B3E6EFD157D5"), 0x51, 0x10, \ #EmuSecPei
    GUID("A56FAD72-A264-4370-85C5-00584654DCE2"), 0x51, 0x11, \ #InstallVerbTablePei
    GUID("6D3D1021-ECFC-42C2-B301-ECE9DB198287"), 0x51, 0x12, \ #LegacySpeakerPei
    GUID("1C8B7F78-1699-40E6-AF33-9B995D16B043"), 0x51, 0x15, \ #PiSmmCommunicationPei
    GUID("FDB3B9A7-1E82-4C77-9C6C-4305C851F253"), 0x51, 0x1A, \ #ProgClkGenPeim
    GUID("00000000-0000-0000-0000-000000000000"), 0x00, 0x00} #EndEntry

  gInsydeTokenSpaceGuid.PcdDxeChipsetDebugCodeTable|{ \
    GUID("62D171CB-78CD-4480-8678-C6A2A797A8DE"), 0x41, 0x01, \ #MpCpu
    GUID("BAE7599F-3C6B-43BC-BDF0-9CE07AA91AA6"), 0x41, 0x02, \ #CpuIo
    GUID("5552575A-7E00-4D61-A3A4-F7547351B49E"), 0x42, 0x02, \ #SmmBase
    GUID("9CC55D7D-FBFF-431C-BC14-334EAEA6052B"), 0x42, 0x04, \ #SmmCoreDispatcher
    GUID("7FED72EE-0170-4814-9878-A8FB1864DFAF"), 0x42, 0x06, \ #SmmRelocate
    GUID("8D3BE215-D6F6-4264-BEA6-28073FB13AEA"), 0x42, 0x07, \ #SmmThunk
    GUID("3151F203-546B-4683-AD72-D8B16BC7D75E"), 0x44, 0x02, \ #DxeMchInit
    GUID("8D6756B9-E55E-4D6A-A3A5-5E4D72DDF772"), 0x44, 0x03, \ #PciHostBridge
    GUID("EDA791A4-384B-4906-89D1-EE4DC972FE4A"), 0x44, 0x04, \ #SmbiosMemory
    GUID("CDC204A8-F5DB-44F6-BDC6-446EEE54316F"), 0x44, 0x05, \ #IgdOpRegion
    GUID("EFFC8F05-B526-4EB5-B36B-8CD889923C0C"), 0x44, 0x06, \ #LegacyRegion
    GUID("B09CB87C-67D8-412B-BB9D-9F4B214D720A"), 0x44, 0x07, \ #VTd
    GUID("90F1E37A-A26B-4E50-9A19-F9DD65F9F173"), 0x44, 0x08, \ #HDCP
    GUID("F50251DA-B608-4177-991A-7DF4278C9753"), 0x44, 0x09, \ #SmmAccess
    GUID("3AA01781-5E40-4524-B6CA-1ACB3B45578C"), 0x45, 0x02, \ #HDCPSMI
    GUID("21094ECB-9F20-4781-AE4B-50728B389A6E"), 0x47, 0x02, \ #DxeIchInit
    GUID("BB65942B-521F-4EC3-BAF9-A92540CF60D2"), 0x47, 0x04, \ #SataController
    GUID("E052D8A6-224A-4C32-8D37-2E0AE162364D"), 0x47, 0x05, \ #DxeIchSmbus
    GUID("A0BAD9F7-AB78-491B-B583-C52B7F84B9E0"), 0x47, 0x06, \ #SmmControl
    GUID("6F0198AA-1F1D-426D-AE3E-39AB633FCC28"), 0x47, 0x07, \ #Cf9Reset
    GUID("C1C418F9-591D-461C-82A2-B9CD96DFEA86"), 0x47, 0x08, \ #IntelIchLegacyInterrupt
    GUID("90CB75DB-71FC-489D-AACF-943477EC7212"), 0x47, 0x09, \ #SmartTimer
    GUID("C194C6EA-B68C-4981-B64B-9BD271474B20"), 0x47, 0x0A, \ #RuntimeDxeIchSpi
    GUID("FC1B7640-3466-4C06-B1CC-1C935394B5C2"), 0x47, 0x0B, \ #IchSerialGpio
    GUID("75521DD9-DBF3-4F4D-80B7-F5144FD74BF8"), 0x47, 0x0C, \ #AhciBus
    GUID("B0D6ED53-B844-43F5-BD2F-61095264E77E"), 0x48, 0x01, \ #IchSmmDispatcher
    GUID("2374EDDF-F203-4FC0-A20E-61BAD73089D6"), 0x48, 0x03, \ #IoTrap
    GUID("DE5B1E13-1427-453F-ACC4-CDEB0E15797E"), 0x48, 0x04, \ #IchS3Save
    GUID("07A9330A-F347-11D4-9A49-0090273FC14D"), 0x4A, 0x02, \ #LegacyMetronome
    GUID("378D7B65-8DA9-4773-B6E4-A47826A833E1"), 0x4A, 0x03, \ #PcRtc
    GUID("79CA4208-BBA1-4A9A-8456-E1E66A81484E"), 0x4A, 0x04, \ #Legacy8259
    GUID("27835690-BEBE-4929-BC60-94D77C6862F7"), 0x4A, 0x05, \ #IsaAcpiDriver (Sio1007)
    GUID("A6F691AC-31C8-4444-854C-E2C1A6950F92"), 0x4D, 0x01, \ #Bds
    GUID("1967DD9B-B72C-4328-8C80-D4ACFC83FDF8"), 0x4D, 0x02, \ #PciHotPlug
    GUID("FE3542FE-C1D3-4EF8-657C-8048606FF670"), 0x4D, 0x03, \ #SetupUtility
    GUID("94EDD12A-419B-447F-9434-9B3B70783903"), 0x4D, 0x04, \ #DxePlatform
    GUID("FDA14FA3-AFFC-469A-B7BB-34BCDD4AC096"), 0x4D, 0x05, \ #PlatformIde
    GUID("EF0C99B6-B1D3-4025-9405-BF6A560FE0E0"), 0x4D, 0x07, \ #MiscSubclass
    GUID("AFC04099-0D39-405D-BE46-846F08C51A31"), 0x4D, 0x08, \ #AcpiPlatform
    GUID("E3932A34-5729-4F24-9FB1-D7409B456A15"), 0x4D, 0x09, \ #OemBadgingSupport
    GUID("F84CFFF4-511E-41C8-B829-519F5152F444"), 0x4D, 0x0A, \ #LegacyBiosPlatform
    GUID("E2441B64-7EF4-41FE-B3A3-8CAA7F8D3017"), 0x4D, 0x0B, \ #PciPlatform
    GUID("99C20A37-042A-46E2-80F4-E4027FDBC86F"), 0x4E, 0x01, \ #SmmPlatform
    GUID("77A6009E-116E-464D-8EF8-B35201A022DD"), 0x4E, 0x05, \ #DigitalThermalSensor
    GUID("F7731B4C-58A2-4DF4-8980-5645D39ECE58"), 0x4E, 0x06, \ #ProcessorPowerManagement
    GUID("76AD6E27-6181-4769-9188-233CEA0A81F3"), 0x4F, 0x03, \ #Create a dataHub for AMT
    GUID("7945C163-58D4-4DA8-A57B-081130AF3D21"), 0x4F, 0x04, \ #Heci driver core
    GUID("D739F969-FB2D-4BC2-AFE7-081327D3FEDE"), 0x4F, 0x05, \ #AMT driver
    GUID("55E76644-78A5-4A82-A900-7126A5798892"), 0x4F, 0x06, \ #Heci driver
    GUID("32C1C9F8-D53F-41C8-94D0-F6739F231011"), 0x4F, 0x07, \ #AMT Bios Extensions
    GUID("C4F2D007-37FD-422D-B63D-7ED73886E6CA"), 0x4F, 0x08, \ #IDE_CONTROLLER_INIT protocol
    GUID("33C6406D-2F6B-41B5-8705-52BAFB633C09"), 0x4F, 0x09, \ #ASF messages
    GUID("A7FA30F2-2D70-4EE6-B1F2-44ECD5056011"), 0x4F, 0x0A, \ #AMT module on BDS
    GUID("FB142B99-DF57-46CB-BC69-0BF858A734F9"), 0x4F, 0x0B, \ #AMT PCI serial
    GUID("EB78CE7E-4107-4EF5-86CB-22E8D8AC4950"), 0x4F, 0x0C, \ #SmmAsfInit
    GUID("69EF78BC-3B71-4ECC-834F-3B74F9148430"), 0x4F, 0x0D, \ #SmmFlashWriteProtect
    GUID("FF917E22-A228-448D-BDAA-68EFCCDDA5D3"), 0x50, 0x02, \ #TXT dxe driver
    GUID("F2765DEC-6B41-11D5-8E71-00902707B35E"), 0x51, 0x01, \ #8254Timer
    GUID("4FB2CE1F-1A3A-42E3-BD0C-7B84F954189A"), 0x51, 0x03, \ #AcpiCallBacksSmm
    GUID("CB933912-DF8F-4305-B1F9-7B44FA11395C"), 0x51, 0x04, \ #AcpiPlatformDxe
    GUID("A8913EC1-C00C-4C3A-A245-077C0CA35738"), 0x51, 0x05, \ #AspmOverrideDxe
    GUID("C7D4F4E3-DAEA-40B0-8846-F4CAF3135CE8"), 0x51, 0x06, \ #BiosProtectDxe
    GUID("B7358BEB-6A52-4D50-98F9-7EDD70B4B320"), 0x51, 0x09, \ #CommonPciPlatformDxe
    GUID("A19B1FE7-C1BC-49F8-875F-54A5D542443F"), 0x51, 0x0A, \ #CpuIo2Dxe
    GUID("A47EE2D8-F60E-42FD-8E58-7BD65EE4C29B"), 0x51, 0x0B, \ #CpuIo2Smm
    GUID("26452F27-45DE-4A94-807A-0E6FDC1CB962"), 0x51, 0x0E, \ #EmuPeiGateDxe
    GUID("51739E2A-A022-4D73-ADB9-91F0C9BC7142"), 0x51, 0x13, \ #MpServicesOnFrameworkMpServicesThunk
    GUID("E21F35A8-42FF-4050-82D6-93F7CDFA7073"), 0x51, 0x16, \ #PiSmmCommunicationSmm
    GUID("A3FF0EF5-0C28-42F5-B544-8C7DE1E80014"), 0x51, 0x17, \ #PiSmmCpuDxeSmm
    GUID("96BDEA61-C364-4513-B6B3-037E9AD54CE4"), 0x51, 0x1B, \ #SetSsidSvidDxe
    GUID("EF0C99B6-B1D3-4025-9405-BF6A560FE0E0"), 0x51, 0x1C, \ #SmbiosMiscDxe
    GUID("00000000-0000-0000-0000-000000000000"), 0x00, 0x00} #EndEntry

  #
  # LegacyBios Configuration
  #
  gEfiIntelFrameworkModulePkgTokenSpaceGuid.PcdLowPmmMemorySize|0x20000

  #
  # Intel RAID oprom need 130MB PMM memory for Intel Smart Response Technology(ISRT).
  # Setting the size to 134MB (130MB for ISRT + 4MB for kerenl default).
  #
  gEfiIntelFrameworkModulePkgTokenSpaceGuid.PcdHighPmmMemorySize|0x8600000
  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmApSyncTimeout|10000
#
# When PcdRestoreCmosfromVariableFlag|TRUE. Save cmos in variable will be enabled.
# This function will follow PcdSaveCmosFieldCompareList to compare Cmos value and CmosInVariable.
# If they are different, current Cmos value will be copied to CmosInVariable.
#
  gChipsetPkgTokenSpaceGuid.PcdSaveCmosFieldCompareList|{ \
# Offset  Length                      # Bank0: 0x00~0x7F
                                      # Bank1: 0x80~0xFF
    0x10, 0x2F,                     \ # 0x10 ~ 0x3F
    0x44, 0xBC,                     \ # 0x44 ~ 0xFF
    0x00, 0x0}                        # EndEntry

  gInsydeTokenSpaceGuid.PcdI2cControllerTable|{  \ # The definition of I2C host controller number lookup table
    UINT64(0x0000000000001500),                  \ # Number 0 I2C controller is located on dev:0x15 fun:0x00
    UINT64(0x0000000000001501),                  \ # Number 1 I2C controller is located on dev:0x15 fun:0x01
    UINT64(0x0000000000001502),                  \ # Number 2 I2C controller is located on dev:0x15 fun:0x02
    UINT64(0x0000000000001503),                  \ # Number 3 I2C controller is located on dev:0x15 fun:0x03
    UINT64(0x0000000000001900),                  \ # Number 4 I2C controller is located on dev:0x19 fun:0x00
    UINT64(0x0000000000001901),                  \ # Number 5 I2C controller is located on dev:0x19 fun:0x01
#    UINT64(0x0000000000001103),                  \ # Number 6 I2C controller is located on dev:0x11 fun:0x03
    UINT64(0x0000000000000000)                   \ # End of table
  }

  gInsydeTokenSpaceGuid.PcdI2cBusSpeedTable|{                                 \  # The definition of I2C bus configuration lookup table
    UINT32(100000),                                                           \  # Number 0 stands for 100 Khz
    UINT32(400000),                                                           \  # Number 0 stands for 400 khz
    UINT32(1000000),                                                          \  # Number 0 stands for 1  Mhz
    UINT32(00)                                                                \  # End of table
  }
#
# The PCDs are used to control the Windows SMM Security Mitigations Table - Protection Flags
#
# BIT0: If set, expresses that for all synchronous SMM entries,SMM will validate that input and output buffers lie entirely within the expected fixed memory regions.
# BIT1: If set, expresses that for all synchronous SMM entries, SMM will validate that input and output pointers embedded within the fixed communication buffer only refer to address ranges \
#       that lie entirely within the expected fixed memory regions.
# BIT2: Firmware setting this bit is an indication that it will not allow reconfiguration of system resources via non-architectural mechanisms.
# BIT3-31: Reserved
#
  gInsydeTokenSpaceGuid.PcdH2OPeiMinMemorySize|0x15000000

  ##
  ## ME firmware update reserved memory size
  ##
  gChipsetPkgTokenSpaceGuid.PcdMeFirmwareUpdateReservedMemorySize|0x01000000

!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == FALSE
!endif

#[-start-190606-IB16990032-add]#
  gChipsetPkgTokenSpaceGuid.PcdSaGlobalNvsDeviceId|{ \
  UINT32(0x00000000), \ # Device ID 1
	UINT32(0x00000000), \ # Device ID 2
	UINT32(0x00000000), \ # Device ID 3
	UINT32(0x00000000), \ # Device ID 4
	UINT32(0x00000000), \ # Device ID 5
	UINT32(0x00000000), \ # Device ID 6
	UINT32(0x00000000), \ # Device ID 7
	UINT32(0x00000000), \ # Device ID 8
	UINT32(0x00000000), \ # Device ID 9
	UINT32(0x00000000), \ # Device ID 10
	UINT32(0x00000000), \ # Device ID 11
	UINT32(0x00000000), \ # Device ID 12
	UINT32(0x00000000), \ # Device ID 13
	UINT32(0x00000000), \ # Device ID 14
	UINT32(0x00000000)}	# Device ID 15
#[-end-190606-IB16990032-add]#

  gChipsetPkgTokenSpaceGuid.PcdFlashFirmwareBinariesBtGAcmAlignmentOffset|0x00040000

  gChipsetPkgTokenSpaceGuid.PcdBiosGuardConfigBgpdtBiosSvn|0x71024035

  gChipsetPkgTokenSpaceGuid.PcdTdsEnable|FALSE

  gChipsetPkgTokenSpaceGuid.PcdH2OPcieNonCpuPortNumber|28

[PcdsDynamicExDefault]
  # Crisis recovery supported devices
#  gH2ODeviceInfo2TokenSpaceGuid.PcdH2ODeviceInfoOnboradXhci|{DEVICE_PATH("PciRoot(0x0)/Pci(0x14,0x0)"), "pcie_pei_enable=crisisrecovery", "pcie_pei_bar0=0xd8200000"}
  gH2ODeviceInfo2TokenSpaceGuid.PcdH2ODeviceInfoOnboradAhci|{DEVICE_PATH("PciRoot(0x0)/Pci(0x17,0x0)"), "pcie_pei_enable=crisisrecovery", "pcie_pei_bar5=0xb0430000"}
#  EMMC & SD Was removed
  gH2ODeviceInfo2TokenSpaceGuid.PcdH2ODeviceInfoM2Nvme|{DEVICE_PATH("PciRoot(0x0)/Pci(0x1D,0x0)/Pci(0x0,0x0)"), "pcie_pei_enable=crisisrecovery", "pcie_pei_bar0=0xd9000000"}
  gH2ODeviceInfo2TokenSpaceGuid.PcdH2ODeviceInfoCpuM2Nvme|{DEVICE_PATH("PciRoot(0x0)/Pci(0x06,0x0)/Pci(0x0,0x0)"), "pcie_pei_enable=crisisrecovery", "pcie_pei_bar0=0xda000000"}

## SIO Table
  gChipsetPkgTokenSpaceGuid.PcdPeiSioTable|{ \
    0x02, 0x88, \   #Power On UARTs
    0x28, 0x43, \   #IRQ of UARTs, UART2 IRQ=3,UART1 IRQ=4,
    0x29, 0x80, \   # SIRQ_CLKRUN_EN
    0x2A, 0x00, \
    0x2B, 0xDE, \
    0x2C, 0x02, \
    0x30, 0x60, \
    0x3B, 0x06, \
    0x3C, 0x10, \
    0x3A, 0x0A, \   # LPC Docking Enabling
    0x31, 0x04, \
    0x32, 0x04, \
    0x33, 0x04, \
    0x38, 0xFB, \
    0x35, 0x80, \
    0x36, 0x00, \
    0x37, 0xFE, \
    0x3A, 0x0B, \
    0x3C, 0x90, \
    0x39, 0x00, \
    0x34, 0x01, \
    0x12, 0x2E, \   # Relocate configuration ports base address to 0x2E
    0x13, 0x00}     # to ensure SIO config address can be accessed in OS

## SIO GPIO Table
  gChipsetPkgTokenSpaceGuid.PcdPeiSioGpioTable1|{ \
    0x00,0x00, \
    0x01,0x00, \
    0x02,0x1F, \
    0x03,0x03, \
    0x04,0xF2, \
    0x05,0x00, \
    0x06,0x00, \
    0x07,0x00, \
    0x08,0x1F, \
    0x09,0x04, \
    0x0A,0x00, \
    0x0B,0x00, \
    0x0D,0x00, \
    0x0E,0x32, \
    0x0F,0x00, \
    0x10,0x07, \
    0x20,0x00, \
    0x24,0x00}
#[-start-190613-IB16990060-add]#
  #
  # Preserve Memory Table
  #
!if gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiS3Enable
  gInsydeTokenSpaceGuid.PcdPreserveMemoryTable|{ \
     UINT32(0x09  ), UINT32(0xD0  ),  \ # Preserve 832K(0xD0 pages) for ASL
     UINT32(0x0A  ), UINT32(0x2FB0 ),  \ # Preserve 12.208M(0x2FB0 pages) for S3, SMM, etc
     UINT32(0x00  ), UINT32(0x1000),  \ # Preserve 16M(0x1000 pages) for BIOS reserved memory
     UINT32(0x06  ), UINT32(0xCD0 ),  \ # Preserve 12.8M(0xCD0 pages) for UEFI OS runtime data to make S4 memory consistency
     UINT32(0x05  ), UINT32(0x3D0 ),  \ # Preserve 3.8M(0x3D0 pages) for UEFI OS runtime drivers to make S4 memory consistency
     UINT32(0x03  ), UINT32(0x1E00),  \ # Preserve 30M(0x1E00 pages) for boot service drivers to reduce memory fragmental
     UINT32(0x01  ), UINT32(0x00  ),  \ # Preserve 0M(0x00 pages) for UEFI OS boot loader to keep on same address
     UINT32(0x02  ), UINT32(0x00  ),  \ # Preserve 0M(0x00 pages) for EfiLoaderData
     UINT32(0x0E  ), UINT32(0x00  ),  \ # Preserve 0M(0x00 pages) for EfiPersistentMemory
     UINT32(0x0F  ), UINT32(0x00  )}    # EfiMaxMemoryType
#[-end-190613-IB16990060-add]#
!else
  gInsydeTokenSpaceGuid.PcdPreserveMemoryTable|{ \
     UINT32(0x09  ), UINT32(0x80  ),  \ # Preserve 512K(0x80 pages) for ASL
     UINT32(0x0A  ), UINT32(0x6F0 ),  \ # Preserve 6.93M(0x6F0 pages) for S3, SMM, etc
     UINT32(0x00  ), UINT32(0x6F0 ),  \ # Preserve 6.93M(0x6F0 pages) for BIOS reserved memory
     UINT32(0x06  ), UINT32(0x980 ),  \ # Preserve 9.5M(0x980 pages) for UEFI OS runtime data to make S4 memory consistency
     UINT32(0x05  ), UINT32(0x3D0 ),  \ # Preserve 3.8M(0x3D0 pages) for UEFI OS runtime drivers to make S4 memory consistency
     UINT32(0x03  ), UINT32(0x1E00),  \ # Preserve 30M(0x1E00 pages) for boot service drivers to reduce memory fragmental
     UINT32(0x01  ), UINT32(0x00  ),  \ # Preserve 0M(0x00 pages) for UEFI OS boot loader to keep on same address
     UINT32(0x02  ), UINT32(0x00  ),  \ # Preserve 0M(0x00 pages) for EfiLoaderData
     UINT32(0x0E  ), UINT32(0x00  ),  \ # Preserve 0M(0x00 pages) for EfiPersistentMemory
     UINT32(0x0F  ), UINT32(0x00  )}    # EfiMaxMemoryType
!endif

#
# Define the devices which SSID/SVID must be programmed before init.
#
# If PcdDefaultSsidSvid is 0 and the device is not described in PcdDefaultSsidSvidPeiTable,
# then use DEFAULT_SSVID (0x8086) and DEFAULT_SSDID (0x7270) as default SSID/SVID value.
#
# If PcdDefaultSsidSvid is not 0 and the device is not described in PcdDefaultSsidSvidPeiTable,
# then use PcdDefaultSsidSvid as default SSID/SVID value.
#
# If the device is described in PcdDefaultSsidSvidPeiTable,
# then adopt the value of SsidSvid field regardless the value of PcdDefaultSsidSvid
#
#  gChipsetPkgTokenSpaceGuid.PcdDefaultSsidSvidPeiTable|{ \
##   Bus,  Dev,  Func, SsidSvid,
#    0x00, 0x14, 0x00, UINT32(0x80867270), \ # XHCI Controller
#    0x00, 0x1F, 0x03, UINT32(0x80867270)}   # HDA Controller

  gEfiMdeModulePkgTokenSpaceGuid.PcdResetOnMemoryTypeInformationChange|TRUE
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedMeVersion|0x3E8
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedIshVersion|0x3E8
#[-start-190613-IB16990069-add]#
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedEcVersion|0x100
#[-end-190613-IB16990069-add]#
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedIomVersion|0x000
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedMgPhyVersion|0x000
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedTbtVersion|0x000
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedBtGAcmVersion|0x000
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedRetimerVersion|0x000
  # gino: RC 2201 new add >>
  gEfiMdeModulePkgTokenSpaceGuid.PcdSrIovSupport|FALSE
  # gino: RC 2201 new add <<

[PcdsDynamicExDefault]
  gInsydeTokenSpaceGuid.PcdH2OSdhcEmmcSupported|TRUE
  #
  # Port number mapping table Define
  #
  gInsydeTokenSpaceGuid.PcdPortNumberMapTable|{ \
    0x00, 0x17, 0x00, 0, 0, 0x00, \
    0x00, 0x17, 0x00, 0, 1, 0x02, \
    0x00, 0x17, 0x00, 1, 0, 0x01, \
    0x00, 0x17, 0x00, 1, 1, 0x03, \
    0x00, 0x0E, 0x00, 0, 0, 0x04, \ 
    0x00, 0x16, 0x02, 0, 0, 0x06, \
    0x00, 0x16, 0x02, 0, 1, 0x07, \
    0x00, 0x16, 0x02, 1, 0, 0x08, \
    0x00, 0x16, 0x02, 1, 1, 0x09, \
    0x00, 0x00, 0x00, 0, 0, 0x00} #EndEntry
#
# PcdsDynamicEx that is defined by Chipset team put on here. Start from 0x80000000
#
  # PIRQ link value                               PIRQA, PIRQB, PIRQC, PIRQD, PIRQE, PIRQF, PIRQG, PIRQH
  gChipsetPkgTokenSpaceGuid.PcdPirqLinkValueArray|{0x60,  0x61,  0x62,  0x63,  0x68,  0x69,  0x6A,  0x6B} #EndEntry

# D18 => Dev 0x12 => 0x90
# D19 => Dev 0x13 => 0x98
# D20 => Dev 0x14 => 0xA0
# D21 => Dev 0x15 => 0xA8
# D22 => Dev 0x16 => 0xB0
# D23 => Dev 0x17 => 0xB8
# D24 => Dev 0x18 => 0xC0
# D25 => Dev 0x19 => 0xC8
# D26 => Dev 0x1A => 0xD0
# D27 => Dev 0x1B => 0xD8
# D28 => Dev 0x1C => 0xE0
# D29 => Dev 0x1D => 0xE8
# D31 => Dev 0x1F => 0xF8
#        Dev 0x01 => 0x08 PEG
#        Dev 0x02 => 0x10 IGD
#        Dev 0x03 => 0x18 SA Audio Device
#        Dev 0x04 => 0x20 SA Thermal Device

#
# IRQMask Define
#
# PIRQA  PIRQB  PIRQC  PIRQD  PIRQE  PIRQF  PIRQG  PIRQH  NPIRQ
# 0xDEB8 0xDEB8 0xDEB8 0xDEB8 0xDEB8 0xDEB8 0xDEB8 0xDEB8 0xDEF8
  #
  #Bus,  Dev, INT#A,       IrqMask, INT#B,       IrqMask, INT#C,       IrqMask, INT#D,       IrqMask, Slot, Rsv.,      DevIpRegValue, ProgrammableIrq.
  #
  gChipsetPkgTokenSpaceGuid.PcdControllerDeviceIrqRoutingEntry| { \
  0x00, 0xF8, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D31
  0x00, 0xE8, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D29
  0x00, 0xE0, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D28
  0x00, 0xD8, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D27
  0x00, 0xB8, 0x60, UINT16(0xDEB8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D23
  0x00, 0xB0, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D22
  0x00, 0xA0, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D20
  0x00, 0x90, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D18
  0x00, 0x08, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D01
  0x00, 0x10, 0x60, UINT16(0xDEB8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D02
  0x00, 0x20, 0x60, UINT16(0xDEB8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D04
  0x00, 0x28, 0x60, UINT16(0xDEB8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D05
  0x00, 0x40, 0x60, UINT16(0xDEB8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D08
  0x04, 0x00, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B04
  0x05, 0x00, 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B05
  0x06, 0x00, 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B06
  0x07, 0x00, 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B07
  0x08, 0x00, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B08
  0x09, 0x00, 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B09
  0x0E, 0x00, 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B0E
  0x0F, 0x00, 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B0F
  0x02, 0x00, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B02
  0x0A, 0x00, 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B0A
  0x0B, 0x00, 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00} #EndEntry

  #
  #Bus,  Dev, INT#A,       IrqMask, INT#B,       IrqMask, INT#C,       IrqMask, INT#D,       IrqMask, Slot, Rsv.,      DevIpRegValue, ProgrammableIrq.
  #
  gChipsetPkgTokenSpaceGuid.Pcd2HControllerDeviceIrqRoutingEntry| { \
  0x00, 0xF8, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D31
  0x00, 0xE8, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D29
  0x00, 0xE0, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D28
  0x00, 0xD8, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D27
  0x00, 0xB8, 0x60, UINT16(0xDEB8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D23
  0x00, 0xB0, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D22
  0x00, 0xA0, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D20
  0x00, 0x90, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D18
  0x00, 0x08, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D01
  0x00, 0x10, 0x60, UINT16(0xDEB8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D02
  0x00, 0x20, 0x60, UINT16(0xDEB8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D04
  0x00, 0x28, 0x60, UINT16(0xDEB8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D05
  0x00, 0x40, 0x60, UINT16(0xDEB8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, UINT16(0xDEF8), 0x00, 0x00, UINT32(0x00000000), 0x00,  \  #D08
  0x04, 0x00, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B04
  0x05, 0x00, 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B05
  0x06, 0x00, 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B06
  0x07, 0x00, 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B07
  0x08, 0x00, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B08
  0x09, 0x00, 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B09
  0x0E, 0x00, 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B0E
  0x0F, 0x00, 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B0F
  0x02, 0x00, 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B02
  0x0A, 0x00, 0x61, UINT16(0xDEB8), 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00,  \  #B0A
  0x0B, 0x00, 0x62, UINT16(0xDEB8), 0x63, UINT16(0xDEB8), 0x60, UINT16(0xDEB8), 0x61, UINT16(0xDEB8), 0x00, 0xFF, UINT32(0x00000000), 0x00} #EndEntry

  gChipsetPkgTokenSpaceGuid.PcdVirtualBusTable|{ \
  0x00, 0x1c, 0x00, 0x04, \  # PCIE Root Port 1
  0x00, 0x1c, 0x01, 0x05, \  # PCIE Root Port 2
  0x00, 0x1c, 0x02, 0x06, \  # PCIE Root Port 3
  0x00, 0x1c, 0x03, 0x07, \  # PCIE Root Port 4
  0x00, 0x1c, 0x04, 0x08, \  # PCIE Root Port 5
  0x00, 0x1c, 0x05, 0x09, \  # PCIE Root Port 6
  0x00, 0x1c, 0x06, 0x0E, \  # PCIE Root Port 7
  0x00, 0x1c, 0x07, 0x0F, \  # PCIE Root Port 8
  0x00, 0x01, 0x00, 0x02, \  # P.E.G. Root Port D1F1
  0x00, 0x01, 0x01, 0x0A, \  # P.E.G. Root Port D1F2
  0x00, 0x01, 0x02, 0x0B}    # P.E.G. Root Port D1F0 #EndEntry

  gChipsetPkgTokenSpaceGuid.PcdIrqPoolTable|{11, 0x00, \ #IRQ11
                                             10, 0x00}   #IRQ10 #EndEntry

  gChipsetPkgTokenSpaceGuid.PcdPirqPriorityTable|{0,  \# PIRQ A
                                                  0,  \# PIRQ B
                                                  0,  \# PIRQ C
                                                  0,  \# PIRQ D
                                                  0,  \# PIRQ E
                                                  0,  \# PIRQ F
                                                  0,  \# PIRQ G
                                                  0}   # PIRQ H #EndEntry

  ## Default OEM Table ID for ACPI table creation, it is "EDK2    ".
  #  According to ACPI specification, this field is particularly useful when
  #  defining a definition block to distinguish definition block functions.
  #  The OEM assigns each dissimilar table a new OEM Table ID.
  #  This PCD is ignored for definition block.
  # @Prompt Default OEM Table ID for ACPI table creation.
  # default set to "EDK2INTL", will be patched by AcpiPlatform per CPU family
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemTableId|0x4C544E49324B4445

  gEfiSecurityPkgTokenSpaceGuid.PcdFirmwareDebuggerInitialized|FALSE

#
# Internal PCI device disable bus master list PCDs
# Entry format:
#      Device,     Fuction, (assume the bus number is 0)
#
  gInsydeTokenSpaceGuid.PcdH2OInternalDmaDisableTable|{ \
    UINT8(0x1f), UINT8(0x03),    \  # Multimedia Device - Mixed mode device
    UINT8(0xff), UINT8(0xff)     \  #List end
    }


#
# If PcdOverrideMemorySlotMapEnable set to TRUE, necessary to modify PcdOverrideMemorySlotMap to control memory slot map.
#
  gChipsetPkgTokenSpaceGuid.PcdOverrideMemorySlotMapEnable|FALSE
#  Each DIMM Slot Mechanical present bit map, bit 0 -> DIMM 0, bit 1 -> DIMM1, ...
#  if the bit is 1, the related DIMM slot is present.
#  E.g. if memory controller 0 channel 3 has 2 DIMMs,  SlotMap[0][3] = 0x03;
#  E.g. if memory controller 1 channel 2 has only 1 DIMMs,  SlotMap[1][2] = 0x1;
#
#  !!!Notify!!!
#  If your memory type is DDR5, you must conform to the one DIMM two Channel rules.
#
#  if PcdOverrideMemorySlotMap set to 0xFF or 0x00, indicate this channel heve not any slot.
#  This PCD only influence SMBios type 17
#                                                     Channel A    Channel B    Channel C    Channel D
#                                                    ===========  ===========  ===========  =========== 
  gChipsetPkgTokenSpaceGuid.PcdOverrideMemorySlotMap|{UINT8(0xFF), UINT8(0xFF), UINT8(0xFF), UINT8(0xFF), \ ## Controller-0
                                                      UINT8(0xFF), UINT8(0xFF), UINT8(0xFF), UINT8(0xFF)}   ## Controller-1


[PcdsPatchableInModule]
  #
  #  PcdAcpiDefaultOemId that is updated by Oem and max length is 6 bytes
  #  PcdAcpiDefaultOemTableId that is updated by Oem and max length is 8 bytes
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemId|"INSYDE"
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemTableId|0x00544C552D4C4B53

[PcdsDynamicHii.X64.DEFAULT]

[Components]

[Components.$(PEI_ARCH)]
  !disable MdeModulePkg/Core/Pei/PeiMain.inf

!if (gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == FALSE) || (gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == TRUE)
  MdeModulePkg/Core/Pei/PeiMain.inf {
    <SOURCE_OVERRIDE_PATH>
#[-start-211028-IB05660184-add]#
!if $(FIRMWARE_PERFORMANCE) == YES
      Intel/AlderLake/$(CHIPSET_PKG)/Override/EDK2/MdeModulePkg/Core/Pei
!endif
#[-end-211028-IB05660184-add]#
      MdeModulePkg/Override/Core/Pei
    <LibraryClasses>
!if $(TARGET) == DEBUG
      DebugLib|MdePkg/Library/BaseDebugLibSerialPort/BaseDebugLibSerialPort.inf
!endif
    <PcdsFixedAtBuild>
      gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x27
  }
!endif

#
# Initial Xhci Host Controller before DDT
#
!if gInsydeTokenSpaceGuid.PcdH2ODdtSupported
  $(CHIPSET_PKG)/PchDdtUsbPei/PchDdtUsbPei.inf
!endif

  $(CHIPSET_PKG)/EarlyProgramGpioPei/EarlyProgramGpioPei.inf
  $(CHIPSET_PKG)/InitSerialPortPei/InitSerialPortPei.inf

!if gChipsetPkgTokenSpaceGuid.PcdRestoreCmosfromVariableFlag
  $(CHIPSET_PKG)/SaveRestoreCmos/RestoreCmosPei/RestoreCmosPei.inf
!endif

!if gInsydeTokenSpaceGuid.PcdCrisisRecoverySupported
  !disable InsydeModulePkg/Universal/Recovery/CrisisRecoveryPei/CrisisRecoveryPei.inf
  InsydeModulePkg/Universal/Recovery/CrisisRecoveryPei/CrisisRecoveryPei.inf {
   <SOURCE_OVERRIDE_PATH>
     $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Universal/Recovery/CrisisRecoveryPei
  }
  $(CHIPSET_PKG)/CommonChipset/SpeakerPei/LegacySpeakerPei.inf
  $(CHIPSET_PKG)/PchAhciPei/PchAhciPei.inf
!endif
#[-start-190909-IB11270246-add]#
!if gInsydeTokenSpaceGuid.PcdH2OUsbPeiSupported
  $(CHIPSET_PKG)/PchXhciPei/PchXhciPei.inf
!endif
#[-end-190909-IB11270246-add]#
  MdeModulePkg/Universal/ResetSystemPei/ResetSystemPei.inf {
    <LibraryClasses>
      ResetSystemLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/BaseResetSystemLib/BaseResetSystemLib.inf
  }
  $(CHIPSET_PKG)/ChipsetSvcPei/ChipsetSvcPei.inf

!if gChipsetPkgTokenSpaceGuid.PcdHybridGraphicsSupported
  $(CHIPSET_PKG)/HybridGraphicsPei/HybridGraphicsPei.inf
!endif

!if gInsydeTokenSpaceGuid.PcdH2OPeiTimerSupported
  $(CHIPSET_PKG)/8259InterruptControllerPei/8259.inf
  $(CHIPSET_PKG)/CpuArchPei/CpuArchPei.inf
  $(CHIPSET_PKG)/SmartTimer/Pei/SmartTimer.inf
!endif

!if gEfiTraceHubTokenSpaceGuid.PcdStatusCodeUseTraceHub
  $(CHIPSET_PKG)/TraceHubPostCodeHandler/Pei/TraceHubPostCodeHandlerPei.inf
!endif

################################################################################
#
# Components.IA32 Override
#
################################################################################
#
# The override should be removed after the DDT debug drivers are ready for SKL from kernel code.
#
# Fix Post hang on CP A2 issue
#

#!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == FALSE
    UefiCpuPkg/CpuMpPei/CpuMpPei.inf
#
# CpuFeatures PEIM
#
  UefiCpuPkg/CpuFeatures/CpuFeaturesPei.inf {
    <LibraryClasses>
      RegisterCpuFeaturesLib|UefiCpuPkg/Library/RegisterCpuFeaturesLib/PeiRegisterCpuFeaturesLib.inf
      NULL|UefiCpuPkg/Library/CpuCommonFeaturesLib/CpuCommonFeaturesLib.inf
      NULL|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiCpuSpecificFeaturesLib/PeiCpuSpecificFeaturesLib.inf
  }
#!else
  !if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == TRUE
    UefiCpuPkg/CpuMpPei/CpuMpPei.inf {
      <LibraryClasses>
        NULL|$(PLATFORM_SI_PACKAGE)/Library/PeiReadyToInstallMpLib/PeiReadyToInstallMpLib.inf
    }
  !endif #PcdFspModeSelection
#!endif #PcdFspWrapperEnable



#[-start-201220-IB16740127-remove]# The judgment was removed in RC1513
#!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
#[-end-201220-IB16740127-remove]#
  IntelFsp2WrapperPkg/FspmWrapperPeim/FspmWrapperPeim.inf {
    <LibraryClasses>
      MemoryAllocationLib|MdePkg/Library/PeiMemoryAllocationLib/PeiMemoryAllocationLib.inf
    !if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1  # API mode
      SiliconPolicyInitLib|$(PLATFORM_SI_PACKAGE)/Library/PeiSiliconPolicyInitLibDependency/PeiPreMemSiliconPolicyInitLibDependency.inf
    !endif
#[-start-200206-IB16740086-add]#
    !if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 0
      #
      # In FSP Dispatch mode below dummy library should be linked to bootloader PEIM
      # to build all DynamicEx PCDs that FSP consumes into bootloader PCD database.
      #
      NULL|$(PLATFORM_FSP_BIN_PACKAGE)/Library/FspPcdListLib/FspPcdListLibNull.inf
    !endif
#[-end-200206-IB16740086-add]#
  }

  IntelFsp2WrapperPkg/FspsWrapperPeim/FspsWrapperPeim.inf { # RPPO-ICL-0003
    <LibraryClasses>
      !if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1  # API mode
        SiliconPolicyInitLib|$(PLATFORM_SI_PACKAGE)/Library/PeiSiliconPolicyInitLibDependency/PeiPostMemSiliconPolicyInitLibDependency.inf
      !endif
  }
#[-start-201220-IB16740127-remove]# The judgment was removed in RC1513
#!endif
#[-end-201220-IB16740127-remove]#
  $(CHIPSET_PKG)/DummyPcdPei/DummyPcdPei.inf
  $(CHIPSET_PKG)/InsydeReportFvPei/InsydeReportFvPei.inf

  #
  # Replace PlatformStage1Pei with PlatformInitPreMem
  #
  !disable InsydeModulePkg/Universal/CommonPolicy/PlatformStage1Pei/PlatformStage1Pei.inf
  !disable InsydeModulePkg/Universal/CommonPolicy/PlatformStage2Pei/PlatformStage2Pei.inf

  #
  # Override H2ODebug/DebugConfig.exe
  #
  !disable InsydeModulePkg/H2ODebug/$(H2O_DDT_DEBUG_IO)DebugIoPei/$(H2O_DDT_DEBUG_IO)DebugIoPei.inf
  !disable InsydeModulePkg/H2ODebug/DebugEnginePei/DebugEnginePei.inf
!if gInsydeTokenSpaceGuid.PcdH2ODdtSupported
  $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/H2ODebug/$(H2O_DDT_DEBUG_IO)DebugIoPei/$(H2O_DDT_DEBUG_IO)DebugIoPei.inf
  $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/H2ODebug/DebugEnginePei/DebugEnginePei.inf
!endif
##[-start-180813-IB11270208-add]#
#  !disable InsydeModulePkg/Core/DxeIplPeim/DxeIpl.inf
#  InsydeModulePkg/Core/DxeIplPeim/DxeIpl.inf {
#   <LibraryClasses>
#      NULL|MdeModulePkg/Library/LzmaCustomDecompressLib/LzmaCustomDecompressLib.inf
#   <SOURCE_OVERRIDE_PATH>
#     $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Core/DxeIplPeim
#  }
##[-end-180813-IB11270208-add]#
!if gInsydeTokenSpaceGuid.PcdH2OFdmChainOfTrustSupported
  $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Universal/Security/H2OVerifyRegion/H2OVerifyRegionPei/H2OVerifyRegionPei.inf
!endif

#[-start-190702-16990089-add]#
!if gSiPkgTokenSpaceGuid.PcdBootGuardEnable
  $(CHIPSET_PKG)/BootGuardRecoveryHookPei/BootGuardRecoveryHookPei.inf
!endif
#[-end-190702-16990089-add]#
#
# Unmark to launch PPI to output some status code from reserved memory for verification.
#
#!if gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseMemory == 1
#  $(CHIPSET_PKG)/StatusCodeHandlerTestPei/StatusCodeHandlerTestPei.inf
#!endif
!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport != 0
  $(CHIPSET_PKG)/TopSwapRestorePei/TopSwapRestorePei.inf
!endif

[Components.$(DXE_ARCH)]
  !disable MdeModulePkg/Core/Dxe/DxeMain.inf
  MdeModulePkg/Core/Dxe/DxeMain.inf {
    <LibraryClasses>
      CpuExceptionHandlerLib|InsydeModulePkg/Library/CpuExceptionHandlerLib/ThunkCpuExceptionHandlerLib.inf
    <SOURCE_OVERRIDE_PATH>
      MdeModulePkg/Override/Core/Dxe
  }

  $(CHIPSET_PKG)/Universal/SmbiosProcessorDxe/SmbiosProcessorDxe.inf # RPPO-KBL-0036
  $(CHIPSET_PKG)/RestoreMtrrDxe/RestoreMtrrDxe.inf
  $(CHIPSET_PKG)/UsbLegacyControlSmm/UsbLegacyControlSmm.inf
  $(CHIPSET_PKG)/UefiSetupUtilityDxe/SetupUtilityDxe.inf
  $(CHIPSET_PKG)/FrontPageDxe/FrontPageDxe.inf
  $(CHIPSET_PKG)/SataDevSleepDxe/SataDevSleepDxe.inf
  $(CHIPSET_PKG)/BbstableHookDxe/BbstableHookDxe.inf
#[-start-200420-IB17800056-5-modify]#
  $(CHIPSET_PKG)/Platform/SmmPlatform/Smm/SmmPlatform.inf {
    <LibraryClasses>
      ReportStatusCodeLib|MdeModulePkg/Library/SmmReportStatusCodeLib/SmmReportStatusCodeLib.inf
      ResetSystemLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/BaseResetSystemLib/BaseResetSystemLib.inf
   }
#[-end-200420-IB17800056-5-modify]#

#
# Accelerator for Windows 7 warm booting.
#
!if gInsydeTokenSpaceGuid.PcdH2OCsmSupported == TRUE
  $(CHIPSET_PKG)/CsmInt10BlockDxe/CsmInt10BlockDxe.inf
  $(CHIPSET_PKG)/CsmInt10HookSmm/CsmInt10HookSmm.inf
  $(CHIPSET_PKG)/CsmInt15HookSmm/CsmInt15HookSmm.inf
!endif
  $(CHIPSET_PKG)/BiosRegionLockDxe/BiosRegionLockDxe.inf

#
#  I2C Platform Specific drvier.
#
  $(CHIPSET_PKG)/Features/I2cPlatformSpecificDxe/I2cPlatformSpecificDxe.inf

#
#  I2C Master drvier.
#
  $(CHIPSET_PKG)/I2cMaster/Dxe/I2cMasterDxe.inf
  !if gSiPkgTokenSpaceGuid.PcdBiosGuardEnable
    $(CHIPSET_PKG)/Platform/MsdmUpdateSmm/MsdmUpdateSmm.inf
  !endif


#  Policy
#
  $(CHIPSET_PKG)/PolicyInit/InsydeChipsetPolicy/GopPolicyDxe/GopPolicyDxe.inf
  EDK2/PcAtChipsetPkg/HpetTimerDxe/HpetTimerDxe.inf
  $(CHIPSET_PKG)/OemModifyOpRegionDxe/OemModifyOpRegionDxe.inf
  $(CHIPSET_PKG)/OemAcpiPlatformDxe/OemAcpiPlatformDxe.inf
  $(CHIPSET_PKG)/UpdateDsdtByAcpiSdtDxe/UpdateDsdtByAcpiSdtDxe.inf
  $(CHIPSET_PKG)/MemInfoDxe/MemInfoDxe.inf
  $(CHIPSET_PKG)/VbiosHookSmm/VbiosHookSmm.inf
  $(CHIPSET_PKG)/OemBadgingSupportDxe/OEMBadgingSupportDxe.inf
  $(CHIPSET_PKG)/ChipsetSvcDxe/ChipsetSvcDxe.inf
#[-start-190909-IB11270246-add]#
  $(CHIPSET_PKG)/ChipsetSvcSmm/ChipsetSvcSmm.inf
#[-end-190909-IB11270246-add]#
  $(CHIPSET_PKG)/CommonChipset/SetSsidSvidDxe/SetSsidSvidDxe.inf
  $(CHIPSET_PKG)/CommonChipset/SpeakerDxe/LegacySpeakerDxe.inf
!if gSiPkgTokenSpaceGuid.PcdAmtEnable
  $(CHIPSET_PKG)/Features/AMT/AmtLockPs2ConInDxe/AmtLockPs2ConInDxe.inf
  $(CHIPSET_PKG)/Features/AMT/AmtLockI2cConInDxe/AmtLockI2cConInDxe.inf
!if gInsydeTokenSpaceGuid.PcdH2OUsbSupported
  $(CHIPSET_PKG)/Features/AMT/AmtLockUsbConInDxe/AmtLockUsbConInDxe.inf
!endif
!endif

  $(CHIPSET_PKG)/IhisiSmm/IhisiSmm.inf


!disable InsydeModulePkg/Universal/CapsuleUpdate/CapsuleLoaderTriggerDxe/CapsuleLoaderTriggerDxe.inf
!if gInsydeTokenSpaceGuid.PcdH2OCapsuleUpdateSupported
InsydeModulePkg/Universal/CapsuleUpdate/CapsuleLoaderTriggerDxe/CapsuleLoaderTriggerDxe.inf {
  <LibraryClasses>
!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
    NULL|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/SeamlessRecoverySupportLib/SeamlessRecoveryVarLockLib.inf
!endif
}
!endif

!if gChipsetPkgTokenSpaceGuid.PcdMeCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdIshCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdEcCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdPdtCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdIomCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdMgPhyCapsuleUpdateSupported == TRUE || gChipsetPkgTokenSpaceGuid.PcdTbtCapsuleUpdateSupported == TRUE
!if gInsydeTokenSpaceGuid.PcdH2OIhisiFmtsSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleSmm/MeCapsuleSmm.inf
!endif
!endif

!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport != 0
  $(CHIPSET_PKG)/SwapAddressRangeDxe/SwapAddressRangeDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdHybridGraphicsSupported
  $(CHIPSET_PKG)/HybridGraphicsDxe/HybridGraphicsDxe.inf
  $(CHIPSET_PKG)/HybridGraphicsSmm/HybridGraphicsSmm.inf
#[-start-210722-QINGLIN0002-modify]#
!if $(LCFC_SUPPORT_ENABLE) == NO
  $(CHIPSET_PKG)/HybridGraphicsAcpi/AmdDiscreteSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/AmdPowerXpressSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/AmdUltPowerXpressSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N17/NvidiaDiscreteSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N17/NvidiaOptimusSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N17/NvidiaUltDiscreteSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N17/NvidiaUltOptimusSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaDiscreteSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaOptimusSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaUltDiscreteSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaUltOptimusSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaDiscreteSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaOptimusSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaUltDiscreteSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaUltOptimusSsdt.inf
!endif
#[-start-210918-QINGLIN0068-modify]#
!if $(S570_SUPPORT_ENABLE) == YES
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaOptimusSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaOptimusSsdt.inf
!endif
#[-end-210918-QINGLIN0068-modify]#
#[-end-210722-QINGLIN0002-modify]#
#[-start-210902-GEORGE0003-add]#
#[-start-210915-GEORGE0004-modify]#
!if ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N18/NvidiaOptimusSsdt.inf
  $(CHIPSET_PKG)/HybridGraphicsAcpi/N20/NvidiaOptimusSsdt.inf
!endif
#[-end-210915-GEORGE0004-modify]#
#[-end-210902-GEORGE0003-add]#
!endif
!if gChipsetPkgTokenSpaceGuid.PcdMeCapsuleUpdateSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleDxe/MeCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdIshCapsuleUpdateSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleIshDxe/IshCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdPdtCapsuleUpdateSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsulePdtDxe/PdtCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdEcCapsuleUpdateSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleEcDxe/EcCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport != 0

!if gChipsetPkgTokenSpaceGuid.PcdMonolithicCapsuleUpdateSupported == TRUE
$(CHIPSET_PKG)/CapsuleIFWU/CapsuleMonolithicDxe/MonolithicCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport == 2
!if gChipsetPkgTokenSpaceGuid.PcdUcodeCapsuleUpdateSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleUcodeDxe/UcodeCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdBtGAcmCapsuleUpdateSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleBtGAcmDxe/BtGAcmCapsuleDxe.inf
!endif
!endif

!endif

!if gChipsetPkgTokenSpaceGuid.PcdRetimerCapsuleUpdateSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtRetimer1Dxe/TbtRetimerCapsule1Dxe.inf {
   <LibraryClasses>
    TcssRetimerNvmUpdateLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/TbtRetimerNvmUpdateLib/TbtRetimerNvmUpdateLib.inf
  }
#[-start-211109-BAIN000054-modify]#
!if ($(C970_SUPPORT_ENABLE) == YES) OR ($(C770_SUPPORT_ENABLE) == YES) OR ($(S77013_SUPPORT_ENABLE) == YES) OR ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)

!else
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtRetimer2Dxe/TbtRetimerCapsule2Dxe.inf {
   <LibraryClasses>
    TcssRetimerNvmUpdateLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/TbtRetimerNvmUpdateLib/TbtRetimerNvmUpdateLib.inf
  }
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtRetimer3Dxe/TbtRetimerCapsule3Dxe.inf {
   <LibraryClasses>
    TcssRetimerNvmUpdateLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/TbtRetimerNvmUpdateLib/TbtRetimerNvmUpdateLib.inf
  }
!endif
#[-end-211109-BAIN000054-modify]#
!endif
!if gChipsetPkgTokenSpaceGuid.PcdTcssPartialUpdateEnable == TRUE

!if gChipsetPkgTokenSpaceGuid.PcdIomCapsuleUpdateSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleIomDxe/IomCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdMgPhyCapsuleUpdateSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleMgPhyDxe/MgPhyCapsuleDxe.inf
!endif

!if gChipsetPkgTokenSpaceGuid.PcdTbtCapsuleUpdateSupported == TRUE
  $(CHIPSET_PKG)/CapsuleIFWU/CapsuleTbtDxe/TbtCapsuleDxe.inf
!endif

!endif

!if gPlatformModuleTokenSpaceGuid.PcdMeResiliencyEnable == TRUE
  $(CHIPSET_PKG)/Recovery/MeUpdateFaultToleranceDxe/MeUpdateFaultToleranceDxe.inf
!endif


  $(CHIPSET_PKG)/SmbiosUpdateDxe/SmbiosUpdateDxe.inf

!if gChipsetPkgTokenSpaceGuid.PcdRestoreCmosfromVariableFlag
  $(CHIPSET_PKG)/SaveRestoreCmos/SaveCmosDxe/SaveCmosDxe.inf
!endif

!if gInsydeTokenSpaceGuid.PcdDynamicHotKeySupported
  $(CHIPSET_PKG)/DynamicHotKeyDxe/DynamicHotKeyDxe.inf
!endif

  $(CHIPSET_PKG)/Tools/Source/Shell/KblRecovery/KblRecovery.inf

!if gChipsetPkgTokenSpaceGuid.PcdSupportUnLockedBarHandle
  $(CHIPSET_PKG)/UnLockedBarHandleSmm/UnLockedBarHandleSmm.inf
!endif

  $(CHIPSET_PKG)/AsfSecureBootSmm/AsfSecureBootSmm.inf
  $(CHIPSET_PKG)/AsfSecureBootDxe/AsfSecureBootDxe.inf
!if gEfiTraceHubTokenSpaceGuid.PcdStatusCodeUseTraceHub
  $(CHIPSET_PKG)/TraceHubPostCodeHandler/Dxe/TraceHubPostCodeHandlerDxe.inf
!endif
#
# Unmark to launch DXE/SMM to output some status code from reserved memory for verification.
#
#!if gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseMemory == 1
#  $(CHIPSET_PKG)/StatusCodeHandlerTestDxe/StatusCodeHandlerTestDxe.inf
#  $(CHIPSET_PKG)/StatusCodeHandlerTestSmm/StatusCodeHandlerTestSmm.inf
#  $(CHIPSET_PKG)/Tools/Source/Shell/StatusCodeHandlerTestApp/StatusCodeHandlerTestApp.inf
#!endif

!if gInsydeTokenSpaceGuid.PcdH2OPeiTimerSupported
  $(CHIPSET_PKG)/ExtendPeiTimer/ExtendPeiTimer.inf
!endif

!if gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceEnabled
  !disable InsydeModulePkg/Universal/Recovery/BiosUpdateFaultToleranceDxe/BiosUpdateFaultToleranceDxe.inf
  InsydeModulePkg/Universal/Recovery/BiosUpdateFaultToleranceDxe/BiosUpdateFaultToleranceDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Universal/Recovery/BiosUpdateFaultToleranceDxe
  }
!endif

  !disable InsydeModulePkg/Universal/Security/HstiDxe/HstiDxe.inf
#
# WifiConnectionManagerDxe Related
#
  $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Universal/WifiConnectionManagerDxe/WifiConnectionManagerDxe.inf
#
# SmBios Table Type.222
#
#[-start-200420-IB17800056-modify]#
  #$(CLIENT_COMMON_PACKAGE)/Universal/IsvtCheckpointDxe/IsvtCheckpointDxe.inf
#[-end-200420-IB17800056-modify]#

################################################################################
#
# Components.X64 Override
#
################################################################################
  !disable InsydeModulePkg/Bus/Pci/PciBusDxe/PciBusDxe.inf
  InsydeModulePkg/Bus/Pci/PciBusDxe/PciBusDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Bus/Pci/PciBusDxe
  }
  !disable InsydeModulePkg/Universal/Acpi/BootScriptExecutorDxe/BootScriptExecutorDxe.inf
  InsydeModulePkg/Universal/Acpi/BootScriptExecutorDxe/BootScriptExecutorDxe.inf {
    <LibraryClasses>
      DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
  }
  !disable InsydeModulePkg/Universal/Acpi/AcpiPlatformDxe/AcpiPlatformDxe.inf
  InsydeModulePkg/Universal/Acpi/AcpiPlatformDxe/AcpiPlatformDxe.inf {
    <Depex>
      gEfiAcpiTableProtocolGuid AND
      gH2OChipsetServicesProtocolGuid AND
      gEfiFirmwareVolume2ProtocolGuid AND
      gEfiCpuIo2ProtocolGuid AND
      gEfiMpServiceProtocolGuid AND
      $(CHIPSET_REF_CODE_PKG)/SiPkg.dec|gCpuNvsAreaProtocolGuid AND
      gEfiSetupUtilityProtocolGuid AND
      $(PLATFORM_BOARD_PACKAGE)/BoardPkg.dec|gPlatformNvsAreaProtocolGuid
  }
  $(CHIPSET_PKG)/SmmConfidentialMem/SmmConfidentialMem.inf

#[-start-190611-IB16990044-add]#
#  !disable InsydeModulePkg/Universal/Security/Tcg/Tcg2Dxe/Tcg2Dxe.inf
#  !disable InsydeModulePkg/Universal/Security/Tcg/Tcg2PhysicalPresenceDxe/Tcg2PhysicalPresenceDxe.inf
#!if gInsydeTokenSpaceGuid.PcdH2OTpm2Supported
#  InsydeModulePkg/Universal/Security/Tcg/Tcg2Dxe/Tcg2Dxe.inf {
#    <LibraryClasses>
#      Tpm2DeviceLib|SecurityPkg/Library/Tpm2DeviceLibRouter/Tpm2DeviceLibRouterDxe.inf
#      Tpm2CommandLib|SecurityPkg/Library/Tpm2CommandLib/Tpm2CommandLib.inf
#      NULL|SecurityPkg/Library/Tpm2DeviceLibDTpm/Tpm2InstanceLibDTpm.inf
#      NULL|SecurityPkg/Library/HashInstanceLibSha1/HashInstanceLibSha1.inf
#      NULL|SecurityPkg/Library/HashInstanceLibSha256/HashInstanceLibSha256.inf
#      PcdLib|MdePkg/Library/DxePcdLib/DxePcdLib.inf
#  }
#  InsydeModulePkg/Universal/Security/Tcg/Tcg2PhysicalPresenceDxe/Tcg2PhysicalPresenceDxe.inf {
#    <LibraryClasses>
#      Tpm2DeviceLib|SecurityPkg/Library/Tpm2DeviceLibRouter/Tpm2DeviceLibRouterDxe.inf
#      Tpm2CommandLib|SecurityPkg/Library/Tpm2CommandLib/Tpm2CommandLib.inf
#      NULL|SecurityPkg/Library/Tpm2DeviceLibDTpm/Tpm2InstanceLibDTpm.inf
#  }
#!endif
#[-end-190611-IB16990044-add]#

!if gSiPkgTokenSpaceGuid.PcdAmtEnable
  InsydeModulePkg/Universal/StatusCode/DatahubStatusCodeHandlerDxe/DatahubStatusCodeHandlerDxe.inf
!endif

  !disable InsydeModulePkg/Universal/FirmwareVolume/FwBlockServiceSmm/FwBlockServiceSmm.inf
  InsydeModulePkg/Universal/FirmwareVolume/FwBlockServiceSmm/FwBlockServiceSmm.inf {
    <SOURCE_OVERRIDE_PATH>
      $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Universal/FirmwareVolume/FwBlockServiceSmm
  }

  MdeModulePkg/Bus/Pci/PciHostBridgeDxe/PciHostBridgeDxe.inf

!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == TRUE
  IntelFsp2WrapperPkg/FspWrapperNotifyDxe/FspWrapperNotifyDxe.inf
!endif

#  !disable InsydeModulePkg/Csm/LegacyBiosDxe/LegacyBiosDxe.inf
#    InsydeModulePkg/Csm/LegacyBiosDxe/LegacyBiosDxe.inf {
#      <SOURCE_OVERRIDE_PATH>
#        $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/Csm/LegacyBiosDxe
#  }

!if gChipsetPkgTokenSpaceGuid.PcdTdsEnable
  $(PLATFORMSAMPLE_PACKAGE)/Features/TrustedDeviceSetup/TrustedDeviceSetup.inf
!endif

#
# End of Components.$(DXE_ARCH)
#
#
# Override H2ODebug/DebugConfig.exe
#
  !disable InsydeModulePkg/H2ODebug/$(H2O_DDT_DEBUG_IO)DebugIoDxe/$(H2O_DDT_DEBUG_IO)DebugIoDxe.inf
  !disable InsydeModulePkg/H2ODebug/DebugEngineDxe/DebugEngineDxe.inf
!if gInsydeTokenSpaceGuid.PcdH2ODdtSupported
  $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/H2ODebug/$(H2O_DDT_DEBUG_IO)DebugIoDxe/$(H2O_DDT_DEBUG_IO)DebugIoDxe.inf
  $(CHIPSET_PKG)/Override/Insyde/InsydeModulePkg/H2ODebug/DebugEngineDxe/DebugEngineDxe.inf
!endif

#!if gInsydeCrTokenSpaceGuid.PcdH2OConsoleRedirectionSupported
#  !disable InsydeCrPkg/ConfigUtility/CrConfigUtilDxe/CrConfigUtil.inf
#  InsydeCrPkg/ConfigUtility/CrConfigUtilDxe/CrConfigUtil.inf {
#    <SOURCE_OVERRIDE_PATH>
#      $(CHIPSET_PKG)/Override/Insyde/InsydeCrPkg/ConfigUtility/CrConfigUtilDxe
#  }
#!endif

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  MdeModulePkg/Bus/Pci/UfsPciHcDxe/UfsPciHcDxe.inf
  MdeModulePkg/Bus/Ufs/UfsPassThruDxe/UfsPassThruDxe.inf
  UfsFeaturePkg/UfsPlatform/UfsPlatform.inf
!endif

!if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE
!disable InsydeSetupPkg/Drivers/H2OFormBrowserDxe/H2OFormBrowserDxe.inf
InsydeSetupPkg/Drivers/H2OFormBrowserDxe/H2OFormBrowserDxe.inf {
  <SOURCE_OVERRIDE_PATH>
    $(CHIPSET_PKG)/Override/Insyde/InsydeSetupPkg/Drivers/H2OFormBrowserDxe
}

  !disable InsydeSetupPkg/Drivers/HiiLayoutPkgDxe/HiiLayoutPkgDxe.inf
  InsydeSetupPkg/Drivers/HiiLayoutPkgDxe/HiiLayoutPkgDxe.inf {
    <SOURCE_OVERRIDE_PATH>
      $(CHIPSET_PKG)/Override/Insyde/InsydeSetupPkg/Drivers/HiiLayoutPkgDxe
  }
!endif
