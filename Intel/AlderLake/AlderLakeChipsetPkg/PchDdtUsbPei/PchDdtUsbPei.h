/** @file
  

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

// xHC Latency Tolerance Parameters used during initialization process
#define V_XHCI_LTR_HIT_1    0x033200A3
#define V_XHCI_LTR_HIT_2    0x00050001
#define V_XHCI_LTR_MIT_1    0x00CB0028
#define V_XHCI_LTR_MIT_2    0x00050001
#define V_XHCI_LTR_LIT_1    0x00CB001E
#define V_XHCI_LTR_LIT_2    0x00050001

// xHCI controller DID for USB IP Version detection
// xHCI Controler IDs with USB IP V16_0
#define V_XHCI_DID_CNP_LP         0x9DED
#define V_XHCI_DID_CNP_H          0xA36D
// xHCI Controler IDs with USB IP V17_0
#define V_XHCI_DID_CPU_V17_0_1    0x8A13
// xHCI Controler IDs with USB IP V17_1
#define V_XHCI_DID_PCH_V17_1_1    0x34ED
#define V_XHCI_DID_PCH_V17_1_2    0x38ED
#define V_XHCI_DID_PCH_V17_1_3    0x3DED


// USB IP Version enumeration
typedef enum {
  V16_0 = 0,
  V17_0,
  V17_1
} USB_IP_VERSION;

// xHCI controller information structure
typedef struct {
  BOOLEAN          OnSouth;
  USB_IP_VERSION   IpVersion;       // Version of USB IP (value according to USB_IP_VERSION enumeration
  BOOLEAN          IsCpuStepA0;  // Flag for applying programming in specific steps for CPU
  UINT16           DeviceId;        // Host Controller device ID
  UINT32           UsbLtrHigh;      // High Idle Time Control LTR parameters
  UINT32           UsbLtrMid;       // Medium Idle Time Control LTR parameters
  UINT32           UsbLtrLow;       // Low Idle Time Control LTR parameters
} USB_CONTROLLER_INFO;

//RC2117 remove this define from UsbRegs.h and it will cause ddt build error

#define R_XHCI_CFG_MSI_NEXT                             0x81
#define V_XHCI_MEM_HCIVERSION_0110                      0x0110
#define B_XHCI_MEM_HCSPARAMS3_U1DEL                     0x000000FF
#define R_XHCI_MEM_HCCPARAMS1                           0x10
#define R_XHCI_MEM_XECP_CMDM_CTRL_REG1                  0x818C  ///< Command Manager Control 1
#define B_XHCI_MEM_PMCTRL_SSU3LFPS_DET                  0xFF00  ///< SS U3 LFPS Detection Threshold Mask
#define N_XHCI_MEM_PMCTRL_SSU3LPFS_DET                  8       ///< SS U3 LFPS Detection Threshold position
#define R_XHCI_MEM_AUX_CTRL_REG1                        0x80E0

#define R_XHCI_MEM_USB2_LINK_MGR_CTRL_REG1_DW1          0x80F0  ///< USB2_LINK_MGR_CTRL_REG1 - USB2 Port Link Control 1, 2, 3, 4
#define R_XHCI_MEM_USB2_LINK_MGR_CTRL_REG1_DW4          0x80FC  ///< USB2_LINK_MGR_CTRL_REG1 - USB2 Port Link Control 1, 2, 3, 4
#define R_XHCI_MEM_HOST_CTRL_BW_CTRL_REG                0x8100  ///< HOST_CTRL_BW_CTRL_REG - Host Controller Bandwidth Control Register
#define R_XHCI_MEM_HOST_CTRL_TRM_REG2                   0x8110  ///< HOST_CTRL_TRM_REG2 - Host Controller Transfer Manager Control 2
#define R_XHCI_MEM_HOST_CTRL_BW_MAX_REG                 0x8128  ///< HOST_CTRL_BW_MAX_REG - Max BW Control Reg 4

#define B_XHCI_MEM_HOST_CTRL_BW_MAX_REG_MAX_HS_BW       0xFFF000 ///< HOST_CTRL_BW_MAX_REG - Max. Number of BW units for HS ports
#define N_XHCI_MEM_HOST_CTRL_BW_MAX_REG_MAX_HS_BW       12      ///< HOST_CTRL_BW_MAX_REG - Max. Number of BW units for HS ports position
#define R_XHCI_MEM_AUXCLKCTL                            0x816C  ///< xHCI Aux Clock Control Register
#define B_XHCI_MEM_HOST_BW_OV_HS_REG_OVHD_HSTTBW        0x0FFF  ///< Mask for Overhead per packet for HS-TT BW calculations value
#define B_XHCI_MEM_HOST_BW_OV_HS_REG_OVHD_HSBW          0xFFF000 ///< Mask for Overhead per packet for HS BW calculations value
#define N_XHCI_MEM_HOST_BW_OV_HS_REG_OVHD_HSBW          12
#define R_XHCI_MEM_HOST_BW_OV_HS_REG                    0x80C8  ///< HOST_BW_OV_HS_REG - High Speed TT Bandwidth Overhead
#define R_XHCI_MEM_THROTT                               0x819C  ///< XHCI Throttle Control
#define R_XHCI_MEM_THROTT2                              0x81B4  ///< XHCI Throttle
#define N_XHCI_MEM_D0I2CTRL_MSI_IDLE_THRESHOLD          4       ///< Bitshift for MSI Idle Threshold
#define R_XHCI_MEM_TRBPRFCTRLREG1                       0x81D0  ///< TRB Prefetch Control Register 1
#define R_XHCI_MEM_TRBPRFCACHEINVREG                    0x81D8  ///< TRB Prefetch Cache Invalidation Register 1
#define N_XHCI_MEM_TRBPRFCACHEINVREG_EN_TRB_FLUSH       17      ///< Enable TRB flushing for various command

#define R_XHCI_MEM_ENH_CLK_GATE_CTRL                    0x83D8  ///< Enhanced Clock Gate Control Policy Register
#define R_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG2              0x8E74
#define R_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG3              0x8E78
#define R_XHCI_MEM_HOST_CTRL_SSP_CONFIG_REG1            0x8E80

#define B_XHCI_MEM_CAPABILITY_ID                        0xFF      ///< Capability ID
#define V_XHCI_MEM_DBC_DCID                             0x0A      ///< Debug Capability ID
#define R_XHCI_MEM_DBC_DCCTRL                           0x20      ///< Debug Capability Control Register (DCCTRL)
#define B_XHCI_MEM_DBC_DCCTRL_DCR                       BIT0      ///< Debug Capability - DbC Run (DCR)
#define R_XHCI_MEM_DBC_DCST                             0x24      ///< Debug Capability Status Register (DCST)
#define N_XHCI_MEM_DBC_DCST_DBG_PORT_NUMBER             24        ///< Debug Port Number Offset in DCST register
#define B_XHCI_MEM_CAPABILITY_NEXT_CAP_PTR              0xFF00    ///< Next Capability Pointer
#define N_XHCI_MEM_CAPABILITY_NEXT_CAP_PTR              8         ///< Byte shift for next capability pointer
#define B_XHCI_MEM_PORTSCXUSB3_PED                      BIT1      ///< Port Enable/Disabled
#define B_XHCI_MEM_PORTSCXUSB3_WPR                      BIT31     ///< Warm Port Reset
