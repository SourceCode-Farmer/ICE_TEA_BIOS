/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <GopPolicy.h>
#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/DxeInsydeChipsetLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/EcMiscLib.h>

extern EFI_GUID gGopOverrideProtocolGuid;

//
// Function implementations
//

/**

  @param[out] CurrentLidStatus

  @retval     EFI_SUCCESS
  @retval     EFI_UNSUPPORTED
**/
EFI_STATUS
GetPlatformLidStatus (
  OUT LID_STATUS *CurrentLidStatus
  )
{
  EFI_STATUS    Status;
  EFI_STATUS    EcGetLidState;
  BOOLEAN       LidIsOpen;

  EcGetLidState = EFI_SUCCESS;
  LidIsOpen = TRUE;
  Status = EFI_UNSUPPORTED;

  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcGetLidState \n"));
  Status = OemSvcEcGetLidState (&EcGetLidState, &LidIsOpen);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcGetLidState Status: %r\n", Status));
  ASSERT (!EFI_ERROR (EcGetLidState));
  if (Status == EFI_MEDIA_CHANGED) {
    //
    // return Lid supported to GOP, because Lid state has checked  in OemService.
    //
    Status = EFI_SUCCESS;
  }

  *CurrentLidStatus = LidOpen;
  if (!EFI_ERROR (EcGetLidState)) {
    if (!LidIsOpen) {
      //
      // If get lid state form EC successfully and lid is closed.
      //
      *CurrentLidStatus = LidClosed;
    }
  } else {
    DEBUG ((DEBUG_INFO | DEBUG_ERROR, "EcGetLidState ERROR in GopPolicy! Status is %r.\n", EcGetLidState));
  }

  return Status;
}

/**

  @param[out] CurrentDockStatus

  @retval     EFI_SUCCESS
  @retval     EFI_UNSUPPORTED
**/
EFI_STATUS
GetPlatformDockStatus (
  OUT DOCK_STATUS  *CurrentDockStatus
  )
{
    return EFI_UNSUPPORTED;
}

/**

  @param[out] VbtAddress
  @param[out] VbtSize

  @retval     EFI_SUCCESS
  @retval     EFI_NOT_FOUND
**/
EFI_STATUS
GetVbtData (
  OUT EFI_PHYSICAL_ADDRESS *VbtAddress,
  OUT UINT32               *VbtSize
  )
{
  EFI_STATUS                    Status;
  EFI_HANDLE                    *HandleBuffer;
  UINTN                         NumberOfHandles;
  UINT32                        FvStatus;
  UINTN                         Index;
  EFI_FIRMWARE_VOLUME2_PROTOCOL *FwVol = NULL;
  VOID                          *VbtSectionPtr = NULL;
  UINTN                         VbtSectionSize = 0;
//  UINT8                         FabId;
  UINT8                         BoardId;
  EFI_GUID                      *InsydeVbtFileGuid;
  EFI_BOOT_MODE                 BootMode;

  Status = EFI_SUCCESS;
  InsydeVbtFileGuid = NULL;
  //
  // Locate FV protocol.
  // There is little chance we can't find an FV protocol
  //
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiFirmwareVolume2ProtocolGuid,
                  NULL,
                  &NumberOfHandles,
                  &HandleBuffer
                  );
  ASSERT_EFI_ERROR (Status);
  DEBUG ((DEBUG_ERROR, "NumberOfHandles: 0x%X\n", NumberOfHandles));
  ASSERT((NumberOfHandles != 0));
  BoardId = (UINT8)PcdGet64 (PcdH2OBoardId);
  BoardId &= 0x01F;
  BootMode = GetBootModeHob ();

  // switch (BoardId) {
//  case BoardIdSkylakeA0Rvp3:
//  case BoardIdSkylakeURvp7:
////    if (BootMode != BOOT_IN_RECOVERY_MODE) {
//      InsydeVbtFileGuid = &gVbtRvp3Rvp7FileGuid;
////    } else {
////      InsydeVbtFileGuid = &gGop1021VbtRvp3Rvp7FileGuid;
////    }
//    break;
//  case BoardIdSkylakeAioRvp10Crb:
////    if (BootMode != BOOT_IN_RECOVERY_MODE) {
//      InsydeVbtFileGuid = &gVbtRvp10FileGuid;
////    } else {
////      InsydeVbtFileGuid = &gGop1021VbtRvp10FileGuid;
////    }
//    break;
//  case BoardIdSkylakeHaloDdr4Rvp11:
//      InsydeVbtFileGuid = &gVbtRvp11FileGuid;
//    break;
//  case BoardIdSkylakeDtRvp8Crb:
////    if (BootMode != BOOT_IN_RECOVERY_MODE) {
//      InsydeVbtFileGuid = &gVbtRvp8FileGuid;
////    } else {
////      InsydeVbtFileGuid = &gGop1021VbtRvp8FileGuid;
////    }
//    break;
////
//// if other BoardId is as same as ApacFakeBoardId but Apac is disable, current setting follow default.
////
//  case ApacFakeBoardId:    // Board ID = 0xFE
//    if (FeaturePcdGet(PcdApacSupport)) {
//      InsydeVbtFileGuid = &gVbtRvp10FileGuid;
//    } else {
//      InsydeVbtFileGuid = &gVbtRvp3Rvp7FileGuid;
//    }
//    break;

  // default:
  //     InsydeVbtFileGuid = &gVbtRvpFileGuid;
  //   break;
  // }
  // }
  InsydeVbtFileGuid = PcdGetPtr (PcdIntelGraphicsVbtFileGuid);

  //
  // Looking for FV with VBT storage file
  //
  for (Index = 0; Index < NumberOfHandles; Index++) {
    //
    // Get the protocol on this handle
    // This should not fail because of LocateHandleBuffer
    //
    Status = gBS->HandleProtocol (
                    HandleBuffer[Index],
                    &gEfiFirmwareVolume2ProtocolGuid,
                    (VOID**)&FwVol
                    );
    ASSERT_EFI_ERROR (Status);

    //
    // See if it has the VBT storage file
    //
    VbtSectionSize = 0;
    FvStatus    = 0;
    VbtSectionPtr  = NULL;   //ask ReadFile to allocate

    Status = FwVol->ReadSection (
                       FwVol,
                       InsydeVbtFileGuid,
                       EFI_SECTION_RAW,
                       0,
                       (VOID **) &VbtSectionPtr,
                       &VbtSectionSize,
                       &FvStatus
                       );

    //
    // If we found it, then we are done
    //
    if (!EFI_ERROR (Status)) {
      break;
    }
  }

  //
  // Free any allocated buffers
  //
  (gBS->FreePool) (HandleBuffer);

  *VbtAddress = (EFI_PHYSICAL_ADDRESS) VbtSectionPtr;
  *VbtSize = (UINT32) VbtSectionSize;

  VBTPlatformHook (VbtAddress, VbtSize);

  return Status;
}

/**
  Entry point for the GOP Policy Driver.

  @param[in] ImageHandle           Image handle of this driver.
  @param[in] SystemTable           Global system service table.

  @retval    EFI_SUCCESS           Initialization complete.
  @retval    EFI_OUT_OF_RESOURCES  Do not have enough resources to initialize the driver.
**/
EFI_STATUS
EFIAPI
GopPolicyEntry (
  IN EFI_HANDLE       ImageHandle,
  IN EFI_SYSTEM_TABLE *SystemTable
  )
{
  GOP_POLICY_PROTOCOL       *PlatformGopPolicyPtr = NULL;
  EFI_STATUS                Status;
  EFI_BOOT_MODE             BootMode;
  EFI_HANDLE                Handle;
//  UINT8                     FabId;
  DEBUG ((DEBUG_INFO | DEBUG_ERROR, "\nGop Policy Entry\n"));

  PlatformGopPolicyPtr = AllocateZeroPool (sizeof (GOP_POLICY_PROTOCOL));
  if (PlatformGopPolicyPtr == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  PlatformGopPolicyPtr->Revision              = GOP_POLICY_PROTOCOL_REVISION_03;
  PlatformGopPolicyPtr->GetPlatformLidStatus  = GetPlatformLidStatus;
  PlatformGopPolicyPtr->GetVbtData            = GetVbtData;
  PlatformGopPolicyPtr->GetPlatformDockStatus = GetPlatformDockStatus;

//[-start-201227-IB16740000-remove]// GopOverrideDriver has removed in RC1515.01
//  if (PcdGetBool(PcdGopOverrideDriverEnable)) {
//    CopyGuid(&PlatformGopPolicyPtr->GopOverrideGuid, &gGopOverrideProtocolGuid);
//    DEBUG ((DEBUG_INFO, "DxeGopOverride Enabled\n"));
//  } else {
//    DEBUG ((DEBUG_INFO, "DxeGopOverride Disabled\n"));
//  }
//[-end-201227-IB16740000-remove]//

  //
  // OemServices
  //

  Status = gBS->InstallProtocolInterface (
                &ImageHandle,
                &gGopPolicyProtocolGuid,
                EFI_NATIVE_INTERFACE,
                PlatformGopPolicyPtr
                );
  ASSERT_EFI_ERROR (Status);

  BootMode = GetBootModeHob ();
  Handle = NULL;
//  if (BootMode == BOOT_IN_RECOVERY_MODE) {
//    if (BoardId != BoardIdSkylakeHaloDdr4Rvp11) {
//      Status = gBS->InstallProtocolInterface (
//                      &Handle,
//                      &gCrisisGopGuid,
//                      EFI_NATIVE_INTERFACE,
//                      NULL
//                      );
//    } else {
      Status = gBS->InstallProtocolInterface (
                      &Handle,
                      &gGopGuid,
                      EFI_NATIVE_INTERFACE,
                      NULL
                      );
//    }
//    ASSERT_EFI_ERROR (Status);
//  } else {
//    Status = gBS->InstallProtocolInterface (
//                    &Handle,
//                    &gGopGuid,
//                    EFI_NATIVE_INTERFACE,
//                    NULL
//                    );
//    ASSERT_EFI_ERROR (Status);
//  }

  DEBUG ( ( DEBUG_INFO | DEBUG_ERROR, "Gop Policy exit\n" ) );
  return Status;
}

EFI_STATUS
VBTPlatformHook (
  IN OUT  EFI_PHYSICAL_ADDRESS        *VBTAddress,
  IN OUT  UINT32                      *VBTSize
  )
{
  EFI_STATUS                          Status;
//  UINTN                               VariableSize;
  CHIPSET_CONFIGURATION               SystemConfiguration;
//  EFI_GUID                            SystemConfigurationGuid = SYSTEM_CONFIGURATION_GUID;
  UINTN                               SetupSize;
  SA_SETUP                            *SaSetup;
  UINT8                               IgdLcdBlc;

//  VariableSize = 0;
//  VariableSize = PcdGet32 (PcdSetupConfigSize);
//  Status = gRT->GetVariable (
//                  L"Setup",
//                  &SystemConfigurationGuid,
//                  NULL,
//                  &VariableSize,
//                  &SystemConfiguration
//                  );
//  ASSERT_EFI_ERROR (Status);
  Status = GetChipsetSetupVariableDxe (&SystemConfiguration, sizeof (CHIPSET_CONFIGURATION));
  if (EFI_ERROR (Status)) {
  ASSERT_EFI_ERROR (Status);
    return Status;
  }

  SetupSize = sizeof (SA_SETUP);
  SaSetup = AllocateZeroPool (SetupSize);
  if (SaSetup == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status = gRT->GetVariable (
                SA_SETUP_VARIABLE_NAME,
                &gSaSetupVariableGuid,
                NULL,
                &SetupSize,
                SaSetup
                );
  if (EFI_ERROR (Status)) {
    FreePool (SaSetup);
    return Status;
  } else {
    IgdLcdBlc = SaSetup->IgdLcdBlc;
  }

  if (SaSetup->ActiveLFP == 0x03) {
    //
    // Update Active LFP Setting
    //
    VBT8AndThenOr (*VBTAddress, R_VBT_LFP_CONFIG, (~B_VBT_LFP_CONFIG), (V_VBT_LFP_CONFIG_eDP << N_VBT_LFP_CONFIG));
  }
  FreePool (SaSetup);

  switch (IgdLcdBlc) {
    case 0x00:
      VBT8AndThenOr (*VBTAddress, R_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE, (~B_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE ), ( V_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE_PWM << N_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE));
      VBT8AndThenOr (*VBTAddress, R_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY, (~B_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY ), ( V_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY_INVERTED << N_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY));
      break;

    case 0x01:
      VBT8AndThenOr (*VBTAddress, R_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE, (~B_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE ), ( V_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE_I2C << N_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE));
      VBT8AndThenOr (*VBTAddress, R_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY, (~B_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY ), ( V_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY_INVERTED << N_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY));
      break;

    case 0x02:
      VBT8AndThenOr (*VBTAddress, R_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE, (~B_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE ), ( V_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE_PWM << N_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE));
      VBT8AndThenOr (*VBTAddress, R_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY, (~B_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY ), ( V_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY_NORMAL << N_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY));
      break;

    case 0x03:
      VBT8AndThenOr (*VBTAddress, R_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE, (~B_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE ), ( V_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE_I2C << N_VBT_BACKLIGHT_CONTROL_INVERTER_TYPE));
      VBT8AndThenOr (*VBTAddress, R_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY, (~B_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY ), ( V_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY_NORMAL << N_VBT_BACKLIGHT_CONTROL_INVERTER_POLARITY));
      break;

    default:
      break;
  }


  return Status;
}
