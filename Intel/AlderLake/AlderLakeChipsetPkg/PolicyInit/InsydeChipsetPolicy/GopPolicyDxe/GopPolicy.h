/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _GOP_POLICY_DXE_H_
#define _GOP_POLICY_DXE_H_

#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/DxeOemSvcChipsetLib.h>
#include <Library/PcdLib.h>
#include <Library/HobLib.h>
#include <Protocol/GopPolicy.h>
#include <Protocol/FirmwareVolume2.h>
#include <ChipsetSetupConfig.h>
#include <VBTAccess.h>

//
// VBT GUID
///
extern EFI_GUID gVbtCnlRvpFileGuid;
extern EFI_GUID gVbtRvp3Rvp7FileGuid;
extern EFI_GUID gVbtRvp10FileGuid;
extern EFI_GUID gVbtRvp11FileGuid;
extern EFI_GUID gVbtRvp8FileGuid;
extern EFI_GUID gGop1021VbtRvp3Rvp7FileGuid;
extern EFI_GUID gGop1021VbtRvp10FileGuid;
extern EFI_GUID gGop1021VbtRvp8FileGuid;
extern EFI_GUID gCrisisGopGuid;
extern EFI_GUID gGopGuid;

EFI_STATUS
GetPlatformLidStatus (
  OUT LID_STATUS             *CurrentLidStatus
);

EFI_STATUS
GetVbtData (
  OUT EFI_PHYSICAL_ADDRESS   *VbtAddress,
  OUT UINT32                 *VbtSize
);

EFI_STATUS
VBTPlatformHook (
  IN OUT  EFI_PHYSICAL_ADDRESS        *VBTAddress,
  IN OUT  UINT32                      *VBTSize
  );

#endif

