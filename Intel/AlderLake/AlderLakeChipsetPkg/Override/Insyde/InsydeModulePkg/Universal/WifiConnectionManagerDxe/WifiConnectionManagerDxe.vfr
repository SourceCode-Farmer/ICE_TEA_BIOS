/*++
  This file contains a 'Sample Driver' and is licensed as such
  under the terms of your license agreement with Intel or your
  vendor.  This file may be modified by the user, subject to
  the additional terms of the license agreement
--*/
/** @file
 Vfr files used in wireless connection manager.

Copyright (c) 2013 - 2017, Intel Corporation. All rights reserved.<BR>
This software and associated documentation (if any) is furnished
under a license and may only be used or copied in accordance
with the terms of the license. Except as permitted by such
license, no part of this software or documentation may be
reproduced, stored in a retrieval system, or transmitted in any
form or by any means without the express written consent of
Intel Corporation.

**/

#include "WifiConnectionMgrConfigNVDataStruct.h"

#define EFI_NETWORK_DEVICE_CLASS  0x04

formset
  guid     = WIFI_CONFIG_FORM_SET_GUID,
  title    = STRING_TOKEN(STR_WIFI_MGR_FORM_TITLE),
  help     = STRING_TOKEN(STR_WIFI_MGR_FORM_HELP),
  class    = EFI_NETWORK_DEVICE_CLASS,
  subclass = 0x03,

  varstore WIFI_MANAGER_IFR_NVDATA,
    varid = MANAGER_VARSTORE_ID,
    name  = WIFI_MANAGER_IFR_NVDATA,
    guid  = WIFI_CONFIG_FORM_SET_GUID;

  form formid = FORMID_MAC_SELECTION,
    title = STRING_TOKEN(STR_WIFI_MAC_FORM_TITLE);

    suppressif TRUE;
    text
      help   = STRING_TOKEN(STR_NULL_STRING),
      text   = STRING_TOKEN(STR_WIFI_MAC_LIST),
      flags  = INTERACTIVE,
      key    = KEY_MAC_LIST;
    endif;

    label LABEL_MAC_ENTRY;
    label LABEL_END;
  endform;

  form formid = FORMID_WIFI_MAINPAGE,
    title = STRING_TOKEN(STR_NETWORK_MANAGEMENT_TITLE);

    text
      help   = STRING_TOKEN(STR_MAC_ADDRESS_HELP),      // Help string
      text   = STRING_TOKEN(STR_MAC_ADDRESS_TITLE),     // Prompt string
        text   = STRING_TOKEN(STR_MAC_ADDRESS);         // TextTwo

    text
      help   = STRING_TOKEN(STR_NULL_STRING),           // Help string
      text   = STRING_TOKEN(STR_CONNECTION_INFO),       // Prompt string
        text   = STRING_TOKEN(STR_CONNECTED_SSID);      // TextTwo;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    goto FORMID_NETWORK_LIST,
         prompt = STRING_TOKEN(STR_NETWORK_LIST),
         help   = STRING_TOKEN(STR_NETWORK_LIST_HELP),
         flags  = INTERACTIVE,
         key    = KEY_NETWORK_LIST;
  
    goto FORMID_ADD_NETWORK,
         prompt = STRING_TOKEN(STR_ADD_NETWORK),
         help   = STRING_TOKEN(STR_ADD_NETWORK_HELP),
         flags  = INTERACTIVE,
         key    = KEY_ADD_NETWORK;
  
    goto FORMID_SAVED_LIST,
         prompt = STRING_TOKEN(STR_MANAGE_NETWORK),
         help   = STRING_TOKEN(STR_MANAGE_NETWORK_HELP),
         flags  = INTERACTIVE,
         key    = KEY_SAVED_LIST;

    action
         questionid  = KEY_REFRESH_TITLE_CONNECTION_STATUS,
         prompt      = STRING_TOKEN(STR_NULL_STRING),
         help        = STRING_TOKEN(STR_NULL_STRING),
         flags       = INTERACTIVE,
         config      = STRING_TOKEN(STR_NULL_STRING),
         refreshguid = WIFI_CONFIG_MAIN_FORM_REFRESH_GUID,
    endaction;

  endform;

  form formid = FORMID_NETWORK_LIST,
    title = STRING_TOKEN(STR_NETWORK_LIST);

    numeric varid   = WIFI_MANAGER_IFR_NVDATA.ProfileCount,
            prompt  = STRING_TOKEN(STR_REFRESH_NETWORK_COUNT),
            help    = STRING_TOKEN(STR_REFRESH_NETWORK_COUNT_HELP),
            flags   = INTERACTIVE | READ_ONLY,
            key     = KEY_REFRESH_NETWORK_LIST,
            minimum = 0,
            maximum = 0xffffffff,
            step    = 0,
            default = 0,
            refreshguid = WIFI_CONFIG_NETWORK_LIST_REFRESH_GUID,
    endnumeric;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    label LABEL_NETWORK_LIST_ENTRY;
    label LABEL_END;
  endform;

  form formid = FORMID_CONNECT_NETWORK,
    title = STRING_TOKEN(STR_NETWORK_CONFIGURATION);

    text
      help   = STRING_TOKEN(STR_SSID_HELP),     // Help string
      text   = STRING_TOKEN(STR_SSID_TITLE),    // Prompt string
        text   = STRING_TOKEN(STR_SSID);        // TextTwo

    text
      help   = STRING_TOKEN(STR_SECURITY_TYPE_HELP),       // Help string
      text   = STRING_TOKEN(STR_SECURITY_TYPE_TITLE),      // Prompt string
        text   = STRING_TOKEN(STR_SECURITY_TYPE);          // TextTwo


    suppressif NOT ideqval WIFI_MANAGER_IFR_NVDATA.SecurityType == SECURITY_TYPE_WPA2_PERSONAL;
      password  varid    = WIFI_MANAGER_IFR_NVDATA.Password,
                prompt   = STRING_TOKEN(STR_PASSWORD),
                help     = STRING_TOKEN(STR_PASSWORD_HELP),
                flags    = INTERACTIVE,
                key      = KEY_PASSWORD_CONNECT_NETWORK,
                minsize  = PASSWORD_MIN_LEN,
                maxsize  = PASSWORD_MAX_LEN,
      endpassword;
    endif;

    suppressif NOT ideqval WIFI_MANAGER_IFR_NVDATA.SecurityType == SECURITY_TYPE_WPA2_ENTERPRISE;

      oneof varid       = WIFI_MANAGER_IFR_NVDATA.EapAuthMethod,
            questionid  = KEY_EAP_AUTH_METHOD_CONNECT_NETWORK,
            prompt      = STRING_TOKEN(STR_EAP_AUTH_METHOD),
            help        = STRING_TOKEN(STR_EAP_AUTH_METHOD_HELP),
            flags       = INTERACTIVE,
            option text = STRING_TOKEN(STR_EAP_AUTH_METHOD_TTLS), value = EAP_AUTH_METHOD_TTLS, flags = DEFAULT;
            option text = STRING_TOKEN(STR_EAP_AUTH_METHOD_PEAP), value = EAP_AUTH_METHOD_PEAP, flags = 0;
            option text = STRING_TOKEN(STR_EAP_AUTH_METHOD_TLS),  value = EAP_AUTH_METHOD_TLS,  flags = 0;
      endoneof;

      suppressif NOT ideqvallist WIFI_MANAGER_IFR_NVDATA.EapAuthMethod == EAP_AUTH_METHOD_TLS
                                                                          EAP_AUTH_METHOD_TTLS
                                                                          EAP_AUTH_METHOD_PEAP;

        goto FORMID_ENROLL_CERT,
           prompt = STRING_TOKEN(STR_EAP_ENROLL_CA_CERT),
           help   = STRING_TOKEN(STR_EAP_ENROLL_CA_CERT_HELP),
           flags  = INTERACTIVE,
           key    = KEY_ENROLL_CA_CERT_CONNECT_NETWORK;

        suppressif NOT ideqval WIFI_MANAGER_IFR_NVDATA.EapAuthMethod == EAP_AUTH_METHOD_TLS;

            goto FORMID_ENROLL_CERT,
               prompt = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_CERT),
               help   = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_CERT_HELP),
               flags  = INTERACTIVE,
               key    = KEY_ENROLL_CLIENT_CERT_CONNECT_NETWORK;

            goto FORMID_ENROLL_PRIVATE_KEY,
               prompt = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_KEY),
               help   = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_KEY_HELP),
               flags  = INTERACTIVE,
               key    = KEY_ENROLL_PRIVATE_KEY_CONNECT_NETWORK;

        endif;
        suppressif NOT ideqvallist WIFI_MANAGER_IFR_NVDATA.EapAuthMethod == EAP_AUTH_METHOD_TTLS
                                                                            EAP_AUTH_METHOD_PEAP;

            oneof varid       = WIFI_MANAGER_IFR_NVDATA.EapSecondAuthMethod,
                  questionid  = KEY_EAP_SEAUTH_METHOD_CONNECT_NETWORK,
                  prompt      = STRING_TOKEN(STR_EAP_SEAUTH_METHOD),
                  help        = STRING_TOKEN(STR_EAP_SEAUTH_METHOD_HELP),
                  flags       = INTERACTIVE,
                  option text = STRING_TOKEN(STR_EAP_SEAUTH_METHOD_MSCHAPV2), value = EAP_SEAUTH_METHOD_MSCHAPV2, flags = DEFAULT;
            endoneof;
        endif;

        string  varid  = WIFI_MANAGER_IFR_NVDATA.EapIdentity,
                prompt  = STRING_TOKEN(STR_EAP_IDENTITY),
                help    = STRING_TOKEN(STR_EAP_IDENTITY_HELP),
                flags   = INTERACTIVE,
                key     = KEY_EAP_IDENTITY_CONNECT_NETWORK,
                minsize = 6,
                maxsize = EAP_IDENTITY_LEN,
        endstring;

        password  varid    = WIFI_MANAGER_IFR_NVDATA.EapPassword,
                  prompt   = STRING_TOKEN(STR_EAP_PASSWORD),
                  help     = STRING_TOKEN(STR_EAP_PASSWORD_HELP),
                  flags    = INTERACTIVE,
                  key      = KEY_EAP_PASSWORD_CONNECT_NETWORK,
                  minsize  = 0,
                  maxsize  = PASSWORD_MAX_LEN,
        endpassword;

      endif;
    endif;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    text
      help   = STRING_TOKEN(STR_CONNECT_NOW_HELP),
      text   = STRING_TOKEN(STR_CONNECT_NOW),
      flags  = INTERACTIVE,
      key    = KEY_CONNECT_ACTION;

    action
      questionid  = KEY_REFRESH_CONNECT_CONFIGURATION,
      prompt      = STRING_TOKEN(STR_NULL_STRING),
      help        = STRING_TOKEN(STR_NULL_STRING),
      flags       = INTERACTIVE,
      config      = STRING_TOKEN(STR_NULL_STRING),
      refreshguid = WIFI_CONFIG_CONNECT_FORM_REFRESH_GUID,
    endaction;

  endform;

  form formid = FORMID_MANAGE_NETWORK,
    title = STRING_TOKEN(STR_NETWORK_CONFIGURATION);

    string  varid   = WIFI_MANAGER_IFR_NVDATA.SSId,
            prompt  = STRING_TOKEN(STR_SSID_TITLE),
            help    = STRING_TOKEN(STR_SSID_HELP),
            flags   = INTERACTIVE,
            key     = KEY_SSID_MANAGE_NETWORK,
            minsize = SSID_MIN_LEN,
            maxsize = SSID_MAX_LEN,
    endstring;

    oneof varid  = WIFI_MANAGER_IFR_NVDATA.SecurityType,
          questionid = KEY_SECURITY_TYPE_MANAGE_NETWORK,
          prompt = STRING_TOKEN(STR_SECURITY_TYPE_TITLE),
          help   = STRING_TOKEN(STR_SECURITY_TYPE_HELP),
          flags  = INTERACTIVE,
          option text = STRING_TOKEN(STR_SECURITY_NONE),           value = SECURITY_TYPE_NONE,             flags = DEFAULT;
          option text = STRING_TOKEN(STR_SECURITY_WPA2_PERSONAL),   value = SECURITY_TYPE_WPA2_PERSONAL,   flags = 0;
          option text = STRING_TOKEN(STR_SECURITY_WPA2_ENTERPRISE), value = SECURITY_TYPE_WPA2_ENTERPRISE, flags = 0;
    endoneof;

    suppressif NOT ideqval WIFI_MANAGER_IFR_NVDATA.SecurityType == SECURITY_TYPE_WPA2_PERSONAL;

      password  varid    = WIFI_MANAGER_IFR_NVDATA.Password,
                prompt   = STRING_TOKEN(STR_PASSWORD),
                help     = STRING_TOKEN(STR_PASSWORD_HELP),
                flags    = INTERACTIVE,
                key      = KEY_PASSWORD_MANAGE_NETWORK,
                minsize  = PASSWORD_MIN_LEN,
                maxsize  = PASSWORD_MAX_LEN,
      endpassword;

    endif;

    suppressif NOT ideqval WIFI_MANAGER_IFR_NVDATA.SecurityType == SECURITY_TYPE_WPA2_ENTERPRISE;

      oneof varid       = WIFI_MANAGER_IFR_NVDATA.EapAuthMethod,
            questionid  = KEY_EAP_AUTH_METHOD_MANAGE_NETWORK,
            prompt      = STRING_TOKEN(STR_EAP_AUTH_METHOD),
            help        = STRING_TOKEN(STR_EAP_AUTH_METHOD_HELP),
            flags       = INTERACTIVE,
            option text = STRING_TOKEN(STR_EAP_AUTH_METHOD_TTLS), value = EAP_AUTH_METHOD_TTLS, flags = DEFAULT;
            option text = STRING_TOKEN(STR_EAP_AUTH_METHOD_PEAP), value = EAP_AUTH_METHOD_PEAP, flags = 0;
            option text = STRING_TOKEN(STR_EAP_AUTH_METHOD_TLS),  value = EAP_AUTH_METHOD_TLS,  flags = 0;
      endoneof;

      suppressif NOT ideqvallist WIFI_MANAGER_IFR_NVDATA.EapAuthMethod == EAP_AUTH_METHOD_TLS
                                                                          EAP_AUTH_METHOD_TTLS
                                                                          EAP_AUTH_METHOD_PEAP;

        goto FORMID_ENROLL_CERT,
           prompt = STRING_TOKEN(STR_EAP_ENROLL_CA_CERT),
           help   = STRING_TOKEN(STR_EAP_ENROLL_CA_CERT_HELP),
           flags  = INTERACTIVE,
           key    = KEY_ENROLL_CA_CERT_MANAGE_NETWORK;

        suppressif NOT ideqval WIFI_MANAGER_IFR_NVDATA.EapAuthMethod == EAP_AUTH_METHOD_TLS;

            goto FORMID_ENROLL_CERT,
               prompt = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_CERT),
               help   = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_CERT_HELP),
               flags  = INTERACTIVE,
               key    = KEY_ENROLL_CLIENT_CERT_MANAGE_NETWORK;

            goto FORMID_ENROLL_PRIVATE_KEY,
               prompt = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_KEY),
               help   = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_KEY_HELP),
               flags  = INTERACTIVE,
               key    = KEY_ENROLL_PRIVATE_KEY_MANAGE_NETWORK;

        endif;
        suppressif NOT ideqvallist WIFI_MANAGER_IFR_NVDATA.EapAuthMethod == EAP_AUTH_METHOD_TTLS
                                                                            EAP_AUTH_METHOD_PEAP;

            oneof varid       = WIFI_MANAGER_IFR_NVDATA.EapSecondAuthMethod,
                  questionid  = KEY_EAP_SEAUTH_METHOD_MANAGE_NETWORK,
                  prompt      = STRING_TOKEN(STR_EAP_SEAUTH_METHOD),
                  help        = STRING_TOKEN(STR_EAP_SEAUTH_METHOD_HELP),
                  flags       = INTERACTIVE,
                  option text = STRING_TOKEN(STR_EAP_SEAUTH_METHOD_MSCHAPV2), value = EAP_SEAUTH_METHOD_MSCHAPV2, flags = DEFAULT;
            endoneof;

        endif;

        string  varid  = WIFI_MANAGER_IFR_NVDATA.EapIdentity,
                prompt  = STRING_TOKEN(STR_EAP_IDENTITY),
                help    = STRING_TOKEN(STR_EAP_IDENTITY_HELP),
                flags   = INTERACTIVE,
                key     = KEY_EAP_IDENTITY_MANAGE_NETWORK,
                minsize = 6,
                maxsize = EAP_IDENTITY_LEN,
        endstring;

        password  varid    = WIFI_MANAGER_IFR_NVDATA.EapPassword,
                  prompt   = STRING_TOKEN(STR_EAP_PASSWORD),
                  help     = STRING_TOKEN(STR_EAP_PASSWORD_HELP),
                  flags    = INTERACTIVE,
                  key      = KEY_EAP_PASSWORD_MANAGE_NETWORK,
                  minsize  = 0,
                  maxsize  = PASSWORD_MAX_LEN,
        endpassword;

      endif;
    endif;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    checkbox varid  = WIFI_MANAGER_IFR_NVDATA.AutoConnect,
             prompt = STRING_TOKEN(STR_AUTO_CONNNECT),
             help   = STRING_TOKEN(STR_AUTO_CONNNECT_HELP),
             flags  = INTERACTIVE,
             key    = KEY_AUTO_CONNECT_MANAGE_NETWORK,
    endcheckbox;

    checkbox varid  = WIFI_MANAGER_IFR_NVDATA.ScanAnyway,
             prompt = STRING_TOKEN(STR_SCAN_ANYWAY),
             help   = STRING_TOKEN(STR_SCAN_ANYWAY_HELP),
             flags  = INTERACTIVE,
             key    = KEY_SCAN_ANYWAY_MANAGE_NETWORK,
    endcheckbox;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    text
      help   = STRING_TOKEN(STR_SAVE_EXIT_HELP),
      text   = STRING_TOKEN(STR_SAVE_EXIT),
      flags  = INTERACTIVE,
      key    = KEY_UPDATE_NETWORK_ACTION;

    text
      help   = STRING_TOKEN(STR_FORGET_NETWORK_HELP),
      text   = STRING_TOKEN(STR_FORGET_NETWORK),
      flags  = INTERACTIVE,
      key    = KEY_FORGET_NETWORK_ACTION;

  endform;

  form formid = FORMID_SAVED_LIST,
    title = STRING_TOKEN(STR_MANAGE_NETWORK);

    label LABEL_SAVED_LIST_ENTRY;
    label LABEL_END;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    goto FORMID_CHANGE_PRIORITY,
           prompt = STRING_TOKEN(STR_CHANGE_PRIORITY),
           help   = STRING_TOKEN(STR_CHANGE_PRIORITY_HELP),
           flags  = INTERACTIVE,
           key    = KEY_CHANGE_PRIORITY;
  endform;

  form formid = FORMID_CHANGE_PRIORITY,
    title = STRING_TOKEN(STR_CHANGE_PRIORITY);

    label LABEL_PRIORITY_ENTRY;
    label LABEL_END;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    text
      help   = STRING_TOKEN(STR_SAVE_EXIT_HELP),
      text   = STRING_TOKEN(STR_SAVE_EXIT),
      flags  = INTERACTIVE,
      key    = KEY_SAVE_PRIORITY;

  endform;

  form formid = FORMID_ADD_NETWORK,
    title = STRING_TOKEN(STR_ADD_NETWORK);

    string  varid   = WIFI_MANAGER_IFR_NVDATA.SSId,
            prompt  = STRING_TOKEN(STR_SSID_TITLE),
            help    = STRING_TOKEN(STR_SSID_HELP),
            flags   = INTERACTIVE,
            key     = KEY_SSID_ADD_NETWORK,
            minsize = SSID_MIN_LEN,
            maxsize = SSID_MAX_LEN,
    endstring;

    oneof varid  = WIFI_MANAGER_IFR_NVDATA.SecurityType,
          questionid = KEY_SECURITY_TYPE_ADD_NETWORK,
          prompt = STRING_TOKEN(STR_SECURITY_TYPE_TITLE),
          help   = STRING_TOKEN(STR_SECURITY_TYPE_HELP),
          flags  = INTERACTIVE,
          option text = STRING_TOKEN(STR_SECURITY_NONE),           value = SECURITY_TYPE_NONE,             flags = DEFAULT;
          option text = STRING_TOKEN(STR_SECURITY_WPA2_PERSONAL),   value = SECURITY_TYPE_WPA2_PERSONAL,   flags = 0;
          option text = STRING_TOKEN(STR_SECURITY_WPA2_ENTERPRISE), value = SECURITY_TYPE_WPA2_ENTERPRISE, flags = 0;
    endoneof;

    suppressif NOT ideqval WIFI_MANAGER_IFR_NVDATA.SecurityType == SECURITY_TYPE_WPA2_PERSONAL;

      password  varid    = WIFI_MANAGER_IFR_NVDATA.Password,
                prompt   = STRING_TOKEN(STR_PASSWORD),
                help     = STRING_TOKEN(STR_PASSWORD_HELP),
                flags    = INTERACTIVE,
                key      = KEY_PASSWORD_ADD_NETWROK,
                minsize  = PASSWORD_MIN_LEN,
                maxsize  = PASSWORD_MAX_LEN,
      endpassword;

    endif;

    suppressif NOT ideqval WIFI_MANAGER_IFR_NVDATA.SecurityType == SECURITY_TYPE_WPA2_ENTERPRISE;

      oneof varid       = WIFI_MANAGER_IFR_NVDATA.EapAuthMethod,
            questionid  = KEY_EAP_AUTH_METHOD_ADD_NETWORK,
            prompt      = STRING_TOKEN(STR_EAP_AUTH_METHOD),
            help        = STRING_TOKEN(STR_EAP_AUTH_METHOD_HELP),
            flags       = INTERACTIVE,
            option text = STRING_TOKEN(STR_EAP_AUTH_METHOD_TTLS), value = EAP_AUTH_METHOD_TTLS, flags = DEFAULT;
            option text = STRING_TOKEN(STR_EAP_AUTH_METHOD_PEAP), value = EAP_AUTH_METHOD_PEAP, flags = 0;
            option text = STRING_TOKEN(STR_EAP_AUTH_METHOD_TLS),  value = EAP_AUTH_METHOD_TLS,  flags = 0;
      endoneof;

      suppressif NOT ideqvallist WIFI_MANAGER_IFR_NVDATA.EapAuthMethod == EAP_AUTH_METHOD_TLS
                                                                          EAP_AUTH_METHOD_TTLS
                                                                          EAP_AUTH_METHOD_PEAP;

        goto FORMID_ENROLL_CERT,
           prompt = STRING_TOKEN(STR_EAP_ENROLL_CA_CERT),
           help   = STRING_TOKEN(STR_EAP_ENROLL_CA_CERT_HELP),
           flags  = INTERACTIVE,
           key    = KEY_ENROLL_CA_CERT_ADD_NETWORK;

        suppressif NOT ideqval WIFI_MANAGER_IFR_NVDATA.EapAuthMethod == EAP_AUTH_METHOD_TLS;

            goto FORMID_ENROLL_CERT,
               prompt = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_CERT),
               help   = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_CERT_HELP),
               flags  = INTERACTIVE,
               key    = KEY_ENROLL_CLIENT_CERT_ADD_NETWORK;

            goto FORMID_ENROLL_PRIVATE_KEY,
               prompt = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_KEY),
               help   = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_KEY_HELP),
               flags  = INTERACTIVE,
               key    = KEY_ENROLL_PRIVATE_KEY_ADD_NETWORK;

        endif;

        suppressif NOT ideqvallist WIFI_MANAGER_IFR_NVDATA.EapAuthMethod == EAP_AUTH_METHOD_TTLS
                                                                            EAP_AUTH_METHOD_PEAP;

            oneof varid       = WIFI_MANAGER_IFR_NVDATA.EapSecondAuthMethod,
                  questionid  = KEY_EAP_SEAUTH_METHOD_ADD_NETWORK,
                  prompt      = STRING_TOKEN(STR_EAP_SEAUTH_METHOD),
                  help        = STRING_TOKEN(STR_EAP_SEAUTH_METHOD_HELP),
                  flags       = INTERACTIVE,
                  option text = STRING_TOKEN(STR_EAP_SEAUTH_METHOD_MSCHAPV2), value = EAP_SEAUTH_METHOD_MSCHAPV2, flags = DEFAULT;
            endoneof;
        endif;

        string  varid  = WIFI_MANAGER_IFR_NVDATA.EapIdentity,
                prompt  = STRING_TOKEN(STR_EAP_IDENTITY),
                help    = STRING_TOKEN(STR_EAP_IDENTITY_HELP),
                flags   = INTERACTIVE,
                key     = KEY_EAP_IDENTITY_ADD_NETWORK,
                minsize = 6,
                maxsize = EAP_IDENTITY_LEN,
        endstring;

        password  varid    = WIFI_MANAGER_IFR_NVDATA.EapPassword,
                  prompt   = STRING_TOKEN(STR_EAP_PASSWORD),
                  help     = STRING_TOKEN(STR_EAP_PASSWORD_HELP),
                  flags    = INTERACTIVE,
                  key      = KEY_EAP_PASSWORD_ADD_NETWORK,
                  minsize  = 0,
                  maxsize  = PASSWORD_MAX_LEN,
        endpassword;
      endif;
    endif;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    checkbox varid  = WIFI_MANAGER_IFR_NVDATA.AutoConnect,
             prompt = STRING_TOKEN(STR_AUTO_CONNNECT),
             help   = STRING_TOKEN(STR_AUTO_CONNNECT_HELP),
             flags  = INTERACTIVE,
             key    = KEY_AUTO_CONNECT_ADD_NETWORK,
    endcheckbox;

    checkbox varid  = WIFI_MANAGER_IFR_NVDATA.ScanAnyway,
             prompt = STRING_TOKEN(STR_SCAN_ANYWAY),
             help   = STRING_TOKEN(STR_SCAN_ANYWAY_HELP),
             flags  = INTERACTIVE,
             key    = KEY_SCAN_ANYWAY_ADD_NETWORK,
    endcheckbox;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    text
      help   = STRING_TOKEN(STR_SAVE_EXIT_HELP),
      text   = STRING_TOKEN(STR_SAVE_EXIT),
      flags  = INTERACTIVE,
      key    = KEY_ADD_NETWORK_ACTION;
  endform;

  form formid = FORMID_ENROLL_CERT,
    title = STRING_TOKEN(STR_EAP_ENROLL_CERT);
//  goto FORMID_ENROLL_CERT,
    goto FORMID_FILE_EXPLORER,
         prompt = STRING_TOKEN(STR_EAP_ENROLL_CERT_FROM_FILE),
         help   = STRING_TOKEN(STR_EAP_ENROLL_CERT_FROM_FILE_HELP),
         flags  = INTERACTIVE,
         key    = KEY_EAP_ENROLL_CERT_FROM_FILE;

    text
      help   = STRING_TOKEN(STR_NULL_STRING),
      text   = STRING_TOKEN(STR_EAP_ENROLLED_CERT_NAME),
      flags  = INTERACTIVE,
      key    = KEY_ENROLLED_CERT_NAME;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    string  varid   = WIFI_MANAGER_IFR_NVDATA.CertGuid,
            prompt  = STRING_TOKEN(STR_EAP_CERT_GUID),
            help    = STRING_TOKEN(STR_EAP_CERT_GUID_HELP),
            flags   = INTERACTIVE,
            key     = KEY_CERT_GUID,
            minsize = 0,
            maxsize = CERT_GUID_LEN,
    endstring;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);
    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    text
      help   = STRING_TOKEN(STR_SAVE_EXIT_HELP),
      text   = STRING_TOKEN(STR_SAVE_EXIT),
      flags  = INTERACTIVE,
      key    = KEY_SAVE_CERT;

    text
      help   = STRING_TOKEN(STR_NO_SAVE_EXIT_HELP),
      text   = STRING_TOKEN(STR_NO_SAVE_EXIT),
      flags  = INTERACTIVE,
      key    = KEY_NO_SAVE_CERT;

  endform;

  form formid = FORMID_ENROLL_PRIVATE_KEY,
    title = STRING_TOKEN(STR_EAP_ENROLL_CLIENT_KEY);
//  goto FORMID_ENROLL_PRIVATE_KEY,
    goto FORMID_FILE_EXPLORER,
         prompt = STRING_TOKEN(STR_EAP_ENROLL_KEY_FROM_FILE),
         help   = STRING_TOKEN(STR_EAP_ENROLL_KEY_FROM_FILE_HELP),
         flags  = INTERACTIVE,
         key    = KEY_EAP_ENROLL_PRIVATE_KEY_FROM_FILE;

    text
      help   = STRING_TOKEN(STR_NULL_STRING),
      text   = STRING_TOKEN(STR_EAP_ENROLLED_PRIVATE_KEY_NAME),
      flags  = INTERACTIVE,
      key    = KEY_ENROLLED_PRIVATE_KEY_NAME;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    password  varid    = WIFI_MANAGER_IFR_NVDATA.PrivateKeyPassword,
              prompt   = STRING_TOKEN(STR_EAP_CLIENT_KEY_PASSWORD),
              help     = STRING_TOKEN(STR_NULL_STRING),
              flags    = INTERACTIVE,
              key      = KEY_PRIVATE_KEY_PASSWORD,
              minsize  = 0,
              maxsize  = PASSWORD_MAX_LEN,
    endpassword;

    subtitle text = STRING_TOKEN(STR_NULL_STRING);
    subtitle text = STRING_TOKEN(STR_NULL_STRING);

    text
      help   = STRING_TOKEN(STR_SAVE_EXIT_HELP),
      text   = STRING_TOKEN(STR_SAVE_EXIT),
      flags  = INTERACTIVE,
      key    = KEY_SAVE_PRIVATE_KEY;

    text
      help   = STRING_TOKEN(STR_NO_SAVE_EXIT_HELP),
      text   = STRING_TOKEN(STR_NO_SAVE_EXIT),
      flags  = INTERACTIVE,
      key    = KEY_NO_SAVE_PRIVATE_KEY;

  endform;

  form formid = FORMID_FILE_EXPLORER,
     title  = STRING_TOKEN(STR_WIFI_MGR_FILE_EXPLORER_TITLE);
     label LABEL_FORM_FILE_EXPLORER_START_ID;
     label LABEL_FORM_FILE_EXPLORER_END_ID;
  endform;
endformset;
