/** @file
  This module provides default RC Setup variable data if RC Setupvariable is
  not found and also provides a RC Setup variable cache mechanism in PEI phase

;******************************************************************************
;* Copyright (c) 2015 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <PiPei.h>
#include <Library/BaseMemoryLib.h>
#include <Library/VariableLib.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/BaseRcSetupDefaultLib.h>
#include <SetupVariable.h>
#include <PlatformBoardId.h>
#include <MeSetup.h>
#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>

/**
  Extract default SETUP DATA variable data from VFR forms for DXE recovery

  @param[in,out]  SetupData                   A pointer of a pointer to the Setup_Data variable data buffer
  @param[in]      SetupDataSize               Data size in bytes of the Setup Data variable
  @param[in]      PoolType                    The type of pool to allocate
*/
VOID
ExtractSetupDataDefault (
  IN OUT UINT8                                  **SetupData,
  IN     UINTN                                  SetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType
  )
{
  EFI_STATUS                                    Status;
  H2O_BOARD_ID                                  SkuId;
  UINTN                                         SetupDataVariableSize;
  UINT8                                         *TempSetupData;

  SetupDataVariableSize = sizeof (SETUP_DATA);
  if (SetupDataVariableSize != SetupDataSize) {
    return;
  }
  SkuId = (H2O_BOARD_ID) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             PLATFORM_SETUP_VARIABLE_NAME,
             &gSetupVariableGuid,
             SkuId,
             NULL,
             &SetupDataSize,
             *SetupData
             );
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable first = %r, SetupDataSize = %x\n", Status, SetupDataSize));
  if (Status == EFI_BUFFER_TOO_SMALL) {
    TempSetupData = *SetupData;
    Status = gBS->AllocatePool(
                    PoolType,
                    SetupDataSize,
                    (VOID **) SetupData
                    );
    if (EFI_ERROR (Status)) {
      ASSERT (Status == EFI_SUCCESS);
      return;
    }
    Status = gBS->FreePool((VOID *) TempSetupData);
    Status = CommonGetDefaultVariable (
             PLATFORM_SETUP_VARIABLE_NAME,
             &gSetupVariableGuid,
             SkuId,
             NULL,
             &SetupDataSize,
             *SetupData
             );
    DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable secondary = %r, SetupDataSize = %x\n", Status, SetupDataSize));
  }
  if (EFI_ERROR (Status)) {
    Status = CommonGetDefaultVariable (
               PLATFORM_SETUP_VARIABLE_NAME,
               &gSetupVariableGuid,
               0,
               NULL,
               &SetupDataSize,
               *SetupData
               );
  }
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable %r, SetupDataSize = %x\n", Status, SetupDataSize));
  ASSERT (Status == EFI_SUCCESS);
  if (EFI_ERROR (Status)) {
    return;
  }
}

/**
  Extract default SA Setup variable data from VFR forms for DXE recovery

  @param[in,out]  SaSetupData                   A pointer of a pointer to the SA Setup variable data buffer
  @param[in]      SaSetupDataSize               Data size in bytes of the SA Setup variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractSaSetupDefault (
  IN OUT UINT8                                  **SaSetupData,
  IN     UINTN                                  SaSetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType
  )
{
  EFI_STATUS                                    Status;
  H2O_BOARD_ID                                  SkuId;
  UINTN                                         SaVariableSize;
  UINT8                                         *TempSaSetupData;

  SaVariableSize = sizeof (SA_SETUP);
  if (SaVariableSize != SaSetupDataSize) {
    return;
  }
  SkuId = (H2O_BOARD_ID) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             SA_SETUP_VARIABLE_NAME,
             &gSaSetupVariableGuid,
             SkuId,
             NULL,
             &SaSetupDataSize,
             *SaSetupData
             );
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable first = %r, SaSetupDataSize = %x\n", Status, SaSetupDataSize));
  if (Status == EFI_BUFFER_TOO_SMALL) {
    TempSaSetupData = *SaSetupData;
    Status = gBS->AllocatePool(
                    PoolType,
                    SaSetupDataSize,
                    (VOID **) SaSetupData
                    );
    if (EFI_ERROR (Status)) {
      ASSERT (Status == EFI_SUCCESS);
      return;
    }
    Status = gBS->FreePool((VOID *) TempSaSetupData);
    Status = CommonGetDefaultVariable (
             SA_SETUP_VARIABLE_NAME,
             &gSaSetupVariableGuid,
             SkuId,
             NULL,
             &SaSetupDataSize,
             *SaSetupData
             );
    DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable secondary = %r, SaSetupDataSize = %x\n", Status, SaSetupDataSize));
  }
  if (EFI_ERROR (Status)) {
    Status = CommonGetDefaultVariable (
               SA_SETUP_VARIABLE_NAME,
               &gSaSetupVariableGuid,
               0,
               NULL,
               &SaSetupDataSize,
               *SaSetupData
               );
  }
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable = %r, SaSetupDataSize = %x\n", Status, SaSetupDataSize));
  ASSERT (Status == EFI_SUCCESS);
  if (EFI_ERROR (Status)) {
    return;
  }
}

/**
  Extract default ME Setup variable data from VFR forms for DXE recovery

  @param[in,out]  MeSetupData                   A pointer of a pointer to the ME Setup variable data buffer
  @param[in]      MeSetupDataSize               Data size in bytes of the ME Setup variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractMeSetupDefault (
  IN OUT UINT8                                  **MeSetupData,
  IN     UINTN                                  MeSetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType
  )
{
  EFI_STATUS                                    Status;
  H2O_BOARD_ID                                  SkuId;
  UINTN                                         MeVariableSize;
  UINT8                                         *TempMeSetupData;

  MeVariableSize = sizeof (ME_SETUP);
  if (MeVariableSize != MeSetupDataSize) {
    return;
  }
  SkuId = (H2O_BOARD_ID) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             ME_SETUP_VARIABLE_NAME,
             &gMeSetupVariableGuid,
             SkuId,
             NULL,
             &MeSetupDataSize,
             *MeSetupData
             );
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable first = %r, MeSetupDataSize = %x\n", Status, MeSetupDataSize));
  if (Status == EFI_BUFFER_TOO_SMALL) {
    TempMeSetupData = *MeSetupData;
    Status = gBS->AllocatePool(
                    PoolType,
                    MeSetupDataSize,
                    (VOID **) MeSetupData
                    );
    if (EFI_ERROR (Status)) {
      ASSERT (Status == EFI_SUCCESS);
      return;
    }
    Status = gBS->FreePool((VOID *) TempMeSetupData);
    Status = CommonGetDefaultVariable (
               ME_SETUP_VARIABLE_NAME,
               &gMeSetupVariableGuid,
               SkuId,
               NULL,
               &MeSetupDataSize,
               *MeSetupData
               );
    DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable secondary = %r, MeSetupDataSize = %x\n", Status, MeSetupDataSize));
  }
  if (EFI_ERROR (Status)) {
    Status = CommonGetDefaultVariable (
               ME_SETUP_VARIABLE_NAME,
               &gMeSetupVariableGuid,
               0,
               NULL,
               &MeSetupDataSize,
               *MeSetupData
               );
  }
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable = %r, MeSetupDataSize = %x\n", Status, MeSetupDataSize));
  ASSERT (Status == EFI_SUCCESS);
  if (EFI_ERROR (Status)) {
    return;
  }
}


/**
  Extract default CPU Setup variable data from VFR forms for DXE recovery

  @param[in,out]  CpuSetupData                  A pointer of a pointer to the CPU Setup variable data buffer
  @param[in]      CpuSetupDataSize              Data size in bytes of the CPU Setup variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractCpuSetupDefault (
  IN OUT UINT8                                  **CpuSetupData,
  IN     UINTN                                  CpuSetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType
  )
{
  EFI_STATUS                                    Status;
  H2O_BOARD_ID                                  SkuId;
  UINTN                                         CpuVariableSize;
  UINT8                                         *TempCpuSetupData;

  CpuVariableSize = sizeof (CPU_SETUP);
  if (CpuVariableSize != CpuSetupDataSize) {
    return;
  }
  SkuId = (H2O_BOARD_ID) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             CPU_SETUP_VARIABLE_NAME,
             &gCpuSetupVariableGuid,
             SkuId,
             NULL,
             &CpuSetupDataSize,
             *CpuSetupData
             );
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable first = %r, CpuSetupDataSize = %x\n", Status, CpuSetupDataSize));
  if (Status == EFI_BUFFER_TOO_SMALL) {
    TempCpuSetupData = *CpuSetupData;
    Status = gBS->AllocatePool(
                    PoolType,
                    CpuSetupDataSize,
                    (VOID **) CpuSetupData
                    );
    if (EFI_ERROR (Status)) {
      ASSERT (Status == EFI_SUCCESS);
      return;
    }
    Status = gBS->FreePool((VOID *) TempCpuSetupData);
    Status = CommonGetDefaultVariable (
               CPU_SETUP_VARIABLE_NAME,
               &gCpuSetupVariableGuid,
               SkuId,
               NULL,
               &CpuSetupDataSize,
               *CpuSetupData
               );
    DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable secondary = %r, CpuSetupDataSize = %x\n", Status, CpuSetupDataSize));
  }
  if (EFI_ERROR (Status)) {
    Status = CommonGetDefaultVariable (
               CPU_SETUP_VARIABLE_NAME,
               &gCpuSetupVariableGuid,
               0,
               NULL,
               &CpuSetupDataSize,
               *CpuSetupData
               );
  }
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable = %r, CpuSetupDataSize = %x\n", Status, CpuSetupDataSize));
  ASSERT (Status == EFI_SUCCESS);
  if (EFI_ERROR (Status)) {
    return;
  }
}

/**
  Extract default PCH Setup variable data from VFR forms for DXE recovery

  @param[in,out]  PchSetupData                  A pointer of a pointer to the PCH Setup variable data buffer
  @param[in]      PchSetupDataSize              Data size in bytes of the PCH Setup variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractPchSetupDefault (
  IN OUT UINT8                                  **PchSetupData,
  IN     UINTN                                  PchSetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType
  )
{
  EFI_STATUS                                    Status;
  H2O_BOARD_ID                                  SkuId;
  UINTN                                         PchVariableSize;
  UINT8                                         *TempPchSetupData;

  PchVariableSize = sizeof (PCH_SETUP);
  if (PchVariableSize != PchSetupDataSize) {
    return;
  }
  SkuId = (H2O_BOARD_ID) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             PCH_SETUP_VARIABLE_NAME,
             &gPchSetupVariableGuid,
             SkuId,
             NULL,
             &PchSetupDataSize,
             *PchSetupData
             );
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable first = %r, PchSetupDataSize = %x\n", Status, PchSetupDataSize));
  if (Status == EFI_BUFFER_TOO_SMALL) {
    TempPchSetupData = *PchSetupData;
    Status = gBS->AllocatePool(
                    PoolType,
                    PchSetupDataSize,
                    (VOID **) PchSetupData
                    );
    if (EFI_ERROR (Status)) {
      ASSERT (Status == EFI_SUCCESS);
      return;
    }
    Status = gBS->FreePool((VOID *) TempPchSetupData);
    Status = CommonGetDefaultVariable (
             PCH_SETUP_VARIABLE_NAME,
             &gPchSetupVariableGuid,
             SkuId,
             NULL,
             &PchSetupDataSize,
             *PchSetupData
             );
    DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable secondary = %r, PchSetupDataSize = %x\n", Status, PchSetupDataSize));
  }
  if (EFI_ERROR (Status)) {
    Status = CommonGetDefaultVariable (
               PCH_SETUP_VARIABLE_NAME,
               &gPchSetupVariableGuid,
               0,
               NULL,
               &PchSetupDataSize,
               *PchSetupData
               );
  }
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable = %r, PchSetupDataSize = %x\n", Status, PchSetupDataSize));
  ASSERT (Status == EFI_SUCCESS);
  if (EFI_ERROR (Status)) {
    return;
  }
}


/**
  Extract default SI Setup variable data from VFR forms for DXE recovery

  @param[in,out]  PchSetupData                  A pointer of a pointer to the PCH Setup variable data buffer
  @param[in]      PchSetupDataSize              Data size in bytes of the PCH Setup variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractSiSetupDefault (
  IN OUT UINT8                                  **SiSetupData,
  IN     UINTN                                  SiSetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType
  )
{
  EFI_STATUS                                    Status;
  H2O_BOARD_ID                                  SkuId;
  UINTN                                         SiVariableSize;
  UINT8                                         *TempSiSetupData;

  SiVariableSize = sizeof (SI_SETUP);
  if (SiVariableSize != SiSetupDataSize) {
    return;
  }
  SkuId = (H2O_BOARD_ID) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             SI_SETUP_VARIABLE_NAME,
             &gSiSetupVariableGuid,
             SkuId,
             NULL,
             &SiSetupDataSize,
             *SiSetupData
             );
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable first = %r, SiSetupDataSize = %x\n", Status, SiSetupData));
  if (Status == EFI_BUFFER_TOO_SMALL) {
    TempSiSetupData = *SiSetupData;
    Status = gBS->AllocatePool(
                    PoolType,
                    SiSetupDataSize,
                    (VOID **) SiSetupData
                    );
    if (EFI_ERROR (Status)) {
      ASSERT (Status == EFI_SUCCESS);
      return;
    }
    Status = gBS->FreePool((VOID *) TempSiSetupData);
    Status = CommonGetDefaultVariable (
             SI_SETUP_VARIABLE_NAME,
             &gSiSetupVariableGuid,
             SkuId,
             NULL,
             &SiSetupDataSize,
             *SiSetupData
             );
    DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable secondary = %r, SiSetupDataSize = %x\n", Status, SiSetupData));
  }
  if (EFI_ERROR (Status)) {
    Status = CommonGetDefaultVariable (
               SI_SETUP_VARIABLE_NAME,
               &gSiSetupVariableGuid,
               0,
               NULL,
               &SiSetupDataSize,
               *SiSetupData
               );
  }
  DEBUG ((DEBUG_INFO, "CommonGetDefaultVariable = %r, SiSetupDataSize = %x\n", Status, SiSetupData));
  ASSERT (Status == EFI_SUCCESS);

}


/**
  Extract default ME Setup storage variable data from VFR forms for DXE recovery

  @param[in,out]  MeSetupStorageData            A pointer of a pointer to the ME Setup storage variable data buffer
  @param[in]      MeSetupStorageDataSize        Data size in bytes of the ME Setup storage variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractMeSetupStorageDefault (
  IN OUT UINT8                                  **MeSetupStorageData,
  IN     UINTN                                  MeSetupStorageDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType
  )
{
  EFI_STATUS                                    Status;
  H2O_BOARD_ID                                  SkuId;
  UINTN                                         MeVariableSize;
  UINT8                                         *TempMeSetupStorageData;

  MeVariableSize = sizeof (ME_SETUP_STORAGE);
  if (MeVariableSize != MeSetupStorageDataSize) {
    return;
  }
  SkuId = (H2O_BOARD_ID) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             ME_SETUP_STORAGE_VARIABLE_NAME,
             &gMeSetupVariableGuid,
             SkuId,
             NULL,
             &MeSetupStorageDataSize,
             *MeSetupStorageData
             );
  DEBUG ((DEBUG_INFO,
          "CommonGetDefaultVariable first = %r, MeSetupStorageDataSize = %x\n",
          Status, MeSetupStorageDataSize)
          );
  if (Status == EFI_BUFFER_TOO_SMALL) {
    TempMeSetupStorageData = *MeSetupStorageData;
    Status = gBS->AllocatePool(
                    PoolType,
                    MeSetupStorageDataSize,
                    (VOID **) MeSetupStorageData
                    );
    if (EFI_ERROR (Status)) {
      ASSERT (Status == EFI_SUCCESS);
      return;
    }
    Status = gBS->FreePool((VOID *) TempMeSetupStorageData);
    Status = CommonGetDefaultVariable (
             ME_SETUP_STORAGE_VARIABLE_NAME,
             &gMeSetupVariableGuid,
             SkuId,
             NULL,
             &MeSetupStorageDataSize,
             *MeSetupStorageData
             );
    DEBUG ((DEBUG_INFO,
          "CommonGetDefaultVariable secondary = %r, MeSetupStorageDataSize = %x\n",
          Status, MeSetupStorageDataSize)
          );
  }
  if (EFI_ERROR (Status)) {
    Status = CommonGetDefaultVariable (
               ME_SETUP_STORAGE_VARIABLE_NAME,
               &gMeSetupVariableGuid,
               0,
               NULL,
               &MeSetupStorageDataSize,
               *MeSetupStorageData
               );
  }
  DEBUG ((DEBUG_INFO,
          "CommonGetDefaultVariable = %r, MeSetupStorageDataSize = %x\n",
          Status, MeSetupStorageDataSize)
          );
  ASSERT (Status == EFI_SUCCESS);
  if (EFI_ERROR (Status)) {
    return;
  }
}
