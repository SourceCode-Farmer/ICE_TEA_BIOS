/** @file
  This module provides default Setup variable data if Setupvariable is not found
  and also provides a Setup variable cache mechanism in PEI phase

;******************************************************************************
;* Copyright (c) 2013 - 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <PiPei.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/KernelConfigLib.h>
#include <Library/VariableLib.h>
#include <ChipsetSetupConfig.h>


VOID
UpdateSetupDefaultByBoard (
  IN OUT UINT8                                  *SetupVfrDefaultData
  );

/**
  Extract default Setup variable data from VFR forms

  @param[in,out]  SetupData     A pointer to the Setup variable data buffer
  @param[in]  VfrDefault        The VFR default data of a single VFR form
  @param[in]  SetupDataSize     Data size in bytes of the Setup variable
*/
VOID
ExtractVfrDefault (
    UINT8 *SetupData,
    UINT8 *VfrDefault,
    UINTN SetupDataSize
  )
{
   UINTN   VfrBufSize;
   UINTN   DataSize;
   UINTN   VfrIndex;
   UINTN   NvDataIndex;

   VfrBufSize = (UINTN)( *(UINT32 *)VfrDefault );
   VfrIndex = sizeof (UINT32);
   do {
     NvDataIndex  = *(UINT16 *)(VfrDefault + VfrIndex);
     if (NvDataIndex > SetupDataSize) {
       ASSERT (NvDataIndex < SetupDataSize );
       return;
     }
     VfrIndex    += 2;
     DataSize     = *(UINT16 *)(VfrDefault + VfrIndex);
     VfrIndex    += 2;
     CopyMem(SetupData + NvDataIndex, VfrDefault + VfrIndex, DataSize);
     VfrIndex += DataSize;
   } while (VfrIndex < VfrBufSize);
}

/*
  Update legacy boot type order from PCD setting to kernel configuration

  @param[out]  KernelConfig     A pointer to kernel configuration
*/
VOID
UpdateDefaultLegacyBootTypeOrder (
  OUT KERNEL_CONFIGURATION        *KernelConfig
  )
{
  UINT8                           *DefaultLegacyBootTypeOrder;
  UINTN                           Index;

  if (KernelConfig == NULL) {
    return;
  }

  ZeroMem (KernelConfig->BootTypeOrder, MAX_BOOT_ORDER_NUMBER);

  DefaultLegacyBootTypeOrder = (UINT8 *) PcdGetPtr (PcdLegacyBootTypeOrder);
  if (DefaultLegacyBootTypeOrder == NULL) {
    return;
  }

  Index = 0;
  while (DefaultLegacyBootTypeOrder[Index] != 0 && Index < MAX_BOOT_ORDER_NUMBER) {
    KernelConfig->BootTypeOrder[Index] = DefaultLegacyBootTypeOrder[Index];
    Index++;
  }
}

/**
  Extract default Setup variable data from VFR forms

  @param[in,out]  SetupData     A pointer to the Setup variable data buffer
  @param[in]  SetupDataSize     Data size in bytes of the Setup variable
*/
VOID
ExtractSetupDefault (
  UINT8 *SetupData
  )
{
  UINTN                  SetupDataSize;
  H2O_BOARD_ID           SkuId;
  EFI_STATUS             Status;

  SetupDataSize = GetSetupVariableSize();
  SkuId = (H2O_BOARD_ID) LibPcdGetSku ();
  Status = CommonGetDefaultVariable (
             L"Setup",
             &gSystemConfigurationGuid,
             SkuId,
             NULL,
             &SetupDataSize,
             SetupData
             );
  if (Status != EFI_SUCCESS) {
    Status = CommonGetDefaultVariable (
               L"Setup",
               &gSystemConfigurationGuid,
               0,
               NULL,
               &SetupDataSize,
               SetupData
               );
  }
  ASSERT (Status == EFI_SUCCESS);
  if (EFI_ERROR (Status)) {
    return;
  }
  UpdateDefaultLegacyBootTypeOrder ((KERNEL_CONFIGURATION *) SetupData);
  UpdateSetupDefaultByBoard (SetupData);
}


/**
  Update default Setup variable data depend on Board

  @param[in,out]  SetupVfrDefaultData           A pointer to Setup vfr/hfr default data
*/
VOID
UpdateSetupDefaultByBoard (
  IN OUT UINT8                                  *SetupVfrDefaultData
  )
{
  CHIPSET_CONFIGURATION                         *SetupNvData;
  UINT16                                         BoardId;

  SetupNvData = ((CHIPSET_CONFIGURATION *) SetupVfrDefaultData);

  BoardId = PcdGet16(PcdBoardId);
  switch (BoardId) {

//  case BoardIdSkylakeA0Rvp3:
//    SetupNvData->CameraType = 1;
//    break;
//  
//  case BoardIdSkylakeULpddr3Rvp5:
//    SetupNvData->HidEventFilterDriverEnable = 0;
//    break;
//  
//  case BoardIdSkylakeURvp7:
//    //
//    // do not thing.
//    // current vfr/hfr file is base on Rvp7 to set every option.
//    //
//    break;
//
//  case BoardIdSkylakeDtRvp8Crb:
//    SetupNvData->HidEventFilterDriverEnable = 0;
//    break;
//
//  case BoardIdSkylakeAioRvp10Crb:
//    SetupNvData->HidEventFilterDriverEnable = 0;
//    break;
//
//  case BoardIdSkylakeHaloDdr4Rvp11:
//    SetupNvData->HidEventFilterDriverEnable = 0;
//    break;

  default:
    break;
  }
}
