/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  TraceHubStatusCodeHandlerPei implementation.

@copyright
 Copyright (c) 2013 - 2014 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains 'Framework Code' and is licensed as such
 under the terms of your license agreement with Intel or your
 vendor. This file may not be modified, except as allowed by
 additional terms of your license agreement.

@par Specification Reference:
**/

#include <TraceHubStatusCodeHandlerPei.h>

extern EFI_GUID   gEfiTraceHubStatusCodeHandlePeiGuid;
extern EFI_GUID   gEfiTraceHubStatusCodeHandlePpi;
extern EFI_GUID   gEfiTraceHubStatusCodeHandleHeaderPpi;

/**
  Initialization of TraceHub handle for SventxLib usage.

  @retval EFI_SUCESS  The entry point executes successfully.

**/
RETURN_STATUS
EFIAPI
TraceHubHandleInit (
  VOID
  )
{
  RETURN_STATUS             Status;
  TRACE_HUB_PLATFORM_DATA   mTraceHubPlatformData;
  psven_handle_t            TraceHubHndl;
  psven_header_t            TraceHubHandleHeader;
  EFI_PEI_PPI_DESCRIPTOR    *GuidPpi;

  //
  // Create TraceHubHandle for SVEN_DEBUG usage.
  //
  mTraceHubPlatformData.Master = FixedPcdGet8 (PcdTraceHubStatusCodeMaster);
  mTraceHubPlatformData.Channel = FixedPcdGet8 (PcdTraceHubStatusCodeChannel);
  TraceHubHndl = NULL;
  TraceHubHandleHeader = NULL;

  PeiServicesAllocatePool (sizeof (sven_header_t), (VOID *) &TraceHubHandleHeader);
  if (TraceHubHandleHeader == NULL) {
    return RETURN_OUT_OF_RESOURCES;
  }
  ZeroMem (TraceHubHandleHeader, sizeof (sven_header_t));

  SVEN_INIT_STATE (TraceHubHandleHeader, sventx_PlatformInit, NULL);

  TraceHubHndl = SVEN_ALLOC_HANDLE_STATE (TraceHubHandleHeader, &mTraceHubPlatformData);

  SVEN_SET_HANDLE_GUID_UNIT (
    TraceHubHndl, 
    *((sven_guid_t *)(VOID *) &gEfiTraceHubStatusCodeHandlePeiGuid),
    0
    );

  //
  // Install Ppi to pass TraceHubHandle to NothPeak worker.
  //
  GuidPpi = NULL;
  PeiServicesAllocatePool (sizeof (EFI_PEI_PPI_DESCRIPTOR), (VOID *) &GuidPpi);
  if (GuidPpi == NULL) {
    return RETURN_OUT_OF_RESOURCES;
  }

  GuidPpi->Flags = EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST;
  GuidPpi->Ppi   = (VOID *) TraceHubHndl;
  GuidPpi->Guid  = &gEfiTraceHubStatusCodeHandlePpi;

  Status = PeiServicesInstallPpi (GuidPpi);
  if (RETURN_ERROR (Status)) {
    return Status;
  }

  //
  // Install Ppi to pass TraceHubHandleHeader to NothPeak worker.
  //
  GuidPpi = NULL;
  PeiServicesAllocatePool (sizeof (EFI_PEI_PPI_DESCRIPTOR), (VOID *) &GuidPpi);
  if (GuidPpi == NULL) {
    return RETURN_OUT_OF_RESOURCES;
  }

  GuidPpi->Flags = EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST;
  GuidPpi->Ppi   = (VOID *) TraceHubHandleHeader;
  GuidPpi->Guid  = &gEfiTraceHubStatusCodeHandleHeaderPpi;

  Status = PeiServicesInstallPpi (GuidPpi);
  if (RETURN_ERROR (Status)) {
    return Status;
  }

  return Status;
}

/**
  Entry point of TraceHub Status Code PEIM.
  
  This function is the entry point of this Status Code PEIM.
  It initializes supported status code devices.

  @param  FileHandle  Handle of the file being invoked.
  @param  PeiServices Describes the list of possible PEI Services.

  @retval EFI_SUCESS  The entry point executes successfully.

**/
EFI_STATUS
EFIAPI
TraceHubStatusCodeHandlerPeiEntry (
  IN       EFI_PEI_FILE_HANDLE    FileHandle,
  IN CONST EFI_PEI_SERVICES       **PeiServices
  )
{
  EFI_STATUS                Status;
  EFI_PEI_RSC_HANDLER_PPI   *RscHandlerPpi;

  Status = PeiServicesLocatePpi (
             &gEfiPeiRscHandlerPpiGuid,
             0,
             NULL,
             (VOID **) &RscHandlerPpi
             );
  ASSERT_EFI_ERROR (Status);

  //
  // Register NothPeak worker to ReportStatusCodeRouter.
  //
  if (FeaturePcdGet (PcdStatusCodeUseTraceHub)) {
    //
    // Initialization of TraceHub handle for SventxLib usage.
    //
    Status = TraceHubHandleInit ();
    ASSERT_EFI_ERROR (Status);

    //
    // Register NothPeak worker.
    //
    Status = RscHandlerPpi->Register (TraceHubStatusCodeReportWorkerPei);
    ASSERT_EFI_ERROR (Status);
  }

  return EFI_SUCCESS;
}
