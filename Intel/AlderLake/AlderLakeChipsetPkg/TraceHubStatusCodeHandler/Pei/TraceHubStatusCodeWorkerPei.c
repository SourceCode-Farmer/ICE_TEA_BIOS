/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  TraceHubStatusCodeHandlerPei implementation.

@copyright
 Copyright (c) 2013 - 2014 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains 'Framework Code' and is licensed as such
 under the terms of your license agreement with Intel or your
 vendor. This file may not be modified, except as allowed by
 additional terms of your license agreement.

@par Specification Reference:
**/

#include <TraceHubStatusCodeWorkerPei.h>

extern EFI_GUID   gEfiTraceHubStatusCodeHandlePpi;
extern EFI_GUID   gEfiTraceHubStatusCodeHandleHeaderPpi;

/**
  Convert status code value and extended data to readable ASCII string, send string to TraceHub device.

  @param  PeiServices      An indirect pointer to the EFI_PEI_SERVICES table published by the PEI Foundation.
  @param  CodeType         Indicates the type of status code being reported.
  @param  Value            Describes the current status of a hardware or
                           software entity. This includes information about the class and
                           subclass that is used to classify the entity as well as an operation.
                           For progress codes, the operation is the current activity.
                           For error codes, it is the exception.For debug codes,it is not defined at this time.
  @param  Instance         The enumeration of a hardware or software entity within
                           the system. A system may contain multiple entities that match a class/subclass
                           pairing. The instance differentiates between them. An instance of 0 indicates
                           that instance information is unavailable, not meaningful, or not relevant.
                           Valid instance numbers start with 1.
  @param  CallerId         This optional parameter may be used to identify the caller.
                           This parameter allows the status code driver to apply different rules to
                           different callers.
  @param  Data             This optional parameter may be used to pass additional data.

  @retval EFI_SUCCESS      Status code reported to TraceHub successfully.

**/
EFI_STATUS
EFIAPI
TraceHubStatusCodeReportWorkerPei (
  IN CONST  EFI_PEI_SERVICES      **PeiServices,
  IN EFI_STATUS_CODE_TYPE         CodeType,
  IN EFI_STATUS_CODE_VALUE        Value,
  IN UINT32                       Instance,
  IN CONST EFI_GUID               *CallerId,
  IN CONST EFI_STATUS_CODE_DATA   *Data OPTIONAL
  )
{
  CHAR8                     *Filename;
  CHAR8                     *Description;
  CHAR8                     *Format;
  CHAR8                     Buffer[EFI_STATUS_CODE_DATA_MAX_SIZE];
  UINT32                    ErrorLevel;
  UINT32                    LineNumber;
  UINTN                     CharCount;
  BASE_LIST                 Marker;
  UINT32                    PostCodeValue;
  UINT32                    DebugPrintErrorLevel;
  sven_severity_t           TraceHubSeverityLevel;
  psven_handle_t            TraceHubHndl;
  psven_header_t            TraceHubHandleHeader;
  EFI_PEI_PPI_DESCRIPTOR    *GuidPpi;

  Buffer[0] = '\0';

  //
  // Retrieve TraceHubHndl and Update TraceHubHndl->svh_header.
  //
  PeiServicesLocatePpi (
    &gEfiTraceHubStatusCodeHandlePpi,
    0,
    &GuidPpi,
    (VOID **) &TraceHubHndl
    );

  PeiServicesLocatePpi (
    &gEfiTraceHubStatusCodeHandleHeaderPpi,
    0,
    &GuidPpi,
    (VOID **) &TraceHubHandleHeader
    );
  TraceHubHndl->svh_header = TraceHubHandleHeader;

  //
  // PostCode
  //
  PostCodeValue = GetPostCodeFromStatusCode (CodeType, Value);
  if (PostCodeValue != 0) {
    CharCount = AsciiSPrint (
                  Buffer,
                  sizeof (Buffer),
                  "POSTCODE=<%02x>\n\r",
                  PostCodeValue
                  );
    TraceHubSeverityLevel = SVEN_SEVERITY_NORMAL;
    SVEN_DEBUG (TraceHubHndl, TraceHubSeverityLevel, Buffer, (sven_u16_t) CharCount);
  }

  //
  // DebugPrint
  //
  DebugPrintErrorLevel = GetDebugPrintErrorLevel ();

  if (Data != NULL &&
      ReportStatusCodeExtractAssertInfo (CodeType, Value, Data, &Filename, &Description, &LineNumber)) {
    if ((DEBUG_ERROR& DebugPrintErrorLevel) == 0) {
      return EFI_SUCCESS;
    }
    //
    // Print ASSERT() information into output buffer.
    //
    CharCount = AsciiSPrint (
                  Buffer,
                  sizeof (Buffer),
                  "\n\rPEI_ASSERT!: %a (%d): %a\n\r",
                  Filename,
                  LineNumber,
                  Description
                  );
    TraceHubSeverityLevel = SVEN_SEVERITY_FATAL;
  } else if (Data != NULL &&
             ReportStatusCodeExtractDebugInfo (Data, &ErrorLevel, &Marker, &Format)) {
    if ((ErrorLevel & DebugPrintErrorLevel) == 0) {
      return EFI_SUCCESS;
    }
    //
    // Print DEBUG() information into output buffer.
    //
    CharCount = AsciiBSPrint (
                  Buffer,
                  sizeof (Buffer),
                  Format,
                  Marker
                  );

    if (ErrorLevel == DEBUG_WARN) {
      TraceHubSeverityLevel = SVEN_SEVERITY_NORMAL;
    } else if (ErrorLevel == DEBUG_ERROR) {
      TraceHubSeverityLevel = SVEN_SEVERITY_ERROR;
    } else {
      TraceHubSeverityLevel = SVEN_SEVERITY_NORMAL;
    }
  } else if ((CodeType & EFI_STATUS_CODE_TYPE_MASK) == EFI_ERROR_CODE) {
    if ((DEBUG_ERROR& DebugPrintErrorLevel) == 0) {
      return EFI_SUCCESS;
    }
    //
    // Print ERROR information into output buffer.
    //
    CharCount = AsciiSPrint (
                  Buffer,
                  sizeof (Buffer),
                  "ERROR: C%x:V%x I%x",
                  CodeType,
                  Value,
                  Instance
                  );
    ASSERT (CharCount > 0);
    
    if (CallerId != NULL) {
      CharCount += AsciiSPrint (
                     &Buffer[CharCount],
                     (sizeof (Buffer) - (sizeof (Buffer[0]) * CharCount)),
                     " %g",
                     CallerId
                     );
    }

    if (Data != NULL) {
      CharCount += AsciiSPrint (
                     &Buffer[CharCount],
                     (sizeof (Buffer) - (sizeof (Buffer[0]) * CharCount)),
                     " %x",
                     Data
                     );
    }

    CharCount += AsciiSPrint (
                   &Buffer[CharCount],
                   (sizeof (Buffer) - (sizeof (Buffer[0]) * CharCount)),
                   "\n\r"
                   );

    TraceHubSeverityLevel = SVEN_SEVERITY_ERROR;
  } else if ((CodeType & EFI_STATUS_CODE_TYPE_MASK) == EFI_PROGRESS_CODE) {
    if ((DEBUG_INFO & DebugPrintErrorLevel) == 0) {
      return EFI_SUCCESS;
    }
    //
    // Print PROGRESS information into output buffer.
    //
    CharCount = AsciiSPrint (
                  Buffer,
                  sizeof (Buffer),
                  "PROGRESS CODE: V%x I%x\n\r",
                  Value,
                  Instance
                  );

    TraceHubSeverityLevel = SVEN_SEVERITY_NORMAL;
  } else if (Data != NULL &&
             CompareGuid (&Data->Type, &gEfiStatusCodeDataTypeStringGuid) &&
             ((EFI_STATUS_CODE_STRING_DATA *) Data)->StringType == EfiStringAscii) {
    if ((DEBUG_INFO & DebugPrintErrorLevel) == 0) {
      return EFI_SUCCESS;
    }
    //
    // EFI_STATUS_CODE_STRING_DATA
    //
    CharCount = AsciiSPrint (
                  Buffer,
                  sizeof (Buffer),
                  "%a\n\r",
                  ((EFI_STATUS_CODE_STRING_DATA *) Data)->String.Ascii
                  );

    TraceHubSeverityLevel = SVEN_SEVERITY_NORMAL;
  } else {
    if ((DEBUG_INFO & DebugPrintErrorLevel) == 0) {
      return EFI_SUCCESS;
    }
    //
    // Code type is not defined.
    //
    CharCount = AsciiSPrint (
                  Buffer,
                  sizeof (Buffer),
                  "Undefined: C%x:V%x I%x\n\r",
                  CodeType,
                  Value,
                  Instance
                  );

    TraceHubSeverityLevel = SVEN_SEVERITY_NONE;
  }

  //
  // Call TraceHub Lib function to do print.
  //
  SVEN_DEBUG (TraceHubHndl, TraceHubSeverityLevel, Buffer, (sven_u16_t) CharCount);

  return EFI_SUCCESS;
}
