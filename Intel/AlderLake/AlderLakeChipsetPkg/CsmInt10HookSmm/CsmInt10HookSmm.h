/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CSM_INT10_HOOK_H_
#define _CSM_INT10_HOOK_H_

#include <Library/UefiBootServicesTableLib.h>
#include <ChipsetSetupConfig.h>
#include <Library/DxeServicesLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DxeChipsetSvcLib.h>
#include <Library/UefiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <SmiTable.h>
#include <ChipsetSmiTable.h>
#include <Protocol/SmmBase2.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/SmmCpu.h>
#include <Protocol/EdidDiscovered.h>
#include <Protocol/EdidActive.h>
#include <Protocol/VgaMiniPort.h>
#include <VesaBiosExtensions.h>
#include <Library/VariableLib.h>
//
// BIOS Simple Mode Data
//
typedef struct {
  UINT16                      VbeModeNumber;
  VOID                        *LinearFrameBuffer;
  UINTN                       FrameBufferSize;
} BIOS_VIDEO_SIMPLE_MODE_DATA;

typedef struct {
  UINTN                       MaxMode;
  BIOS_VIDEO_SIMPLE_MODE_DATA *SimpleModeData;
} BIOS_VIDEO_DEV_SIMPLE;

#endif