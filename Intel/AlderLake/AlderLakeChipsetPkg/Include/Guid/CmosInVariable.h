/** @file

;******************************************************************************
;* Copyright (c) 2014 , Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CMOS_IN_VARIABLE_H_
#define _CMOS_IN_VARIABLE_H_

#define CMOS_TOTAL_LENGTH               0x100
#define CMOS_SAVE_START_OFFSET          0x10

extern EFI_GUID gCmosInVariableGuid;

typedef struct {
  UINT8             Start_Offset;
  UINT8             Length;
} CMOS_ELEMENT;

#endif
