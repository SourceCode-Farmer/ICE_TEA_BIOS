/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************

**/

#ifndef _FTPM_INSTANCE_GUID_H_
#define _FTPM_INSTANCE_GUID_H_

#define TPM_DEVICE_INTERFACE_TPM20_FTPM \
  { \
    0x151D3671, 0xBDEE, 0x4821, {0xB0, 0xB0, 0x93, 0xD4, 0x37, 0xC3, 0x7C, 0x89} \
  }

extern EFI_GUID gEfiTpmDeviceInstanceTpm20FtpmGuid;

#endif

