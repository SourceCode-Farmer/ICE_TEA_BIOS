/** @file
  This file contains definitions required for creation of HG Information data HOB.

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _HG_INFORMATION_DATA_HOB_H_
#define _HG_INFORMATION_DATA_HOB_H_

//
// {648CE07B-AE5D-4973-BD3C-8C9153C05DC5}
//
#define HG_INFOFORMATION_DATA_HOB_GUID \
  { \
    0x648ce07b, 0xae5d, 0x4973, 0xbd, 0x3c, 0x8c, 0x91, 0x53, 0xc0, 0x5d, 0xc5 \
  }

extern EFI_GUID                               gH2OHgInformationDataHobGuid;

#pragma pack (push, 1)
typedef struct _HG_INFORMATION_DATA_HOB {
  EFI_HOB_GUID_TYPE                           EfiHobGuidType;
  UINT16                                      DelayAfterPwrEn;
  UINT16                                      DelayAfterHoldReset;

  UINT32                                      DgpuPwrOkGpioNo;
  UINT32                                      DgpuHoldRstGpioNo;
  UINT32                                      DgpuSelGpioNo;
  UINT32                                      DgpuPwrEnableGpioNo;
  UINT32                                      DgpuEdidSelGpioNo;
  UINT32                                      DgpuPwmSelGpioNo;
  UINT32                                      DgpuPrsntGpioNo;

  BOOLEAN                                     DgpuPwrOkActive;
  BOOLEAN                                     DgpuHoldRstActive;
  BOOLEAN                                     DgpuSelActive;
  BOOLEAN                                     DgpuPwrEnableActive;
  BOOLEAN                                     DgpuEdidSelActive;
  BOOLEAN                                     DgpuPwmSelActive;
  BOOLEAN                                     DgpuPrsntActive;

  UINT32                                      Dgpu2PwrOkGpioNo;
  UINT32                                      Dgpu2HoldRstGpioNo;
  UINT32                                      Dgpu2PwrEnableGpioNo;
  UINT32                                      Dgpu2PrsntGpioNo;

  BOOLEAN                                     Dgpu2PwrOkActive;
  BOOLEAN                                     Dgpu2HoldRstActive;
  BOOLEAN                                     Dgpu2PwrEnableActive;
  BOOLEAN                                     Dgpu2PrsntActive;

  UINT8                                       HgMode;
  UINT8                                       BootType;
  UINT8                                       PannelScaling;

  BOOLEAN                                     DgpuGpioSupport;
  BOOLEAN                                     Dgpu2GpioSupport;
  BOOLEAN                                     DgpuPwrOkExist;
  BOOLEAN                                     DgpuSelExist;
  BOOLEAN                                     DgpuEdidSelExist;
  BOOLEAN                                     DgpuPwmSelExist;
  BOOLEAN                                     DgpuPrsntExist;
  BOOLEAN                                     Dgpu2PrsntExist;

  UINT8                                       HgSlot;
} HG_INFORMATION_DATA_HOB;
#pragma pack (pop)

#endif
