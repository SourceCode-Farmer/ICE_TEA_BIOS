/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _PDT_UPD_VARIABLE_H_
#define _PDT_UPD_VARIABLE_H_

#define PDT_UPD_COUNT_GUID \
  { \
  	0x103fcd92, 0xa4e6, 0x4a07, 0xb4, 0x58, 0xf7, 0x8c, 0x9d, 0xb4, 0x52, 0x56 \
  };

extern EFI_GUID gPdtUpdCountGuid;

#endif
