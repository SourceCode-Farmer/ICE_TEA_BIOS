/** @file
  A GUID for SGX setup variable

;******************************************************************************
;* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SGX_SETUP_VARIABLE_H_
#define _SGX_SETUP_VARIABLE_H_

#define SGX_SETUP_VARIABLE_GUID  \
  { 0x45b5acb9, 0x0359, 0x49be, 0xad, 0xb1, 0x49, 0x37, 0x7b, 0xd6, 0x07, 0xf7 }

extern EFI_GUID gSgxSetupVariableGuid;

#endif


