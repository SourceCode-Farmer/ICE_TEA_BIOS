/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _ISH_UPD_VARIABLE_H_
#define _ISH_UPD_VARIABLE_H_

#define ISH_Upd_Ver_GUID \
  { \
  	0x5bdddc78, 0xd405, 0x48e0, 0xb0, 0x66, 0xef, 0x50, 0x02, 0x9d, 0xc4, 0x90 \
  };

extern EFI_GUID gIshUpdVerGuid;

#endif
