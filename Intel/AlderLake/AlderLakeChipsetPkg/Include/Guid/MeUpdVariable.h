/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _ME_UPD_VARIABLE_H_
#define _ME_UPD_VARIABLE_H_

#define ME_UPD_COUNT_GUID \
  { \
  	0x225722d1, 0x7d98, 0x4bca, 0x0b, 0x8e, 0x74, 0xc3, 0x17, 0x55, 0x07, 0x7f \
  };

extern EFI_GUID gMeUpdCountGuid;

#endif
