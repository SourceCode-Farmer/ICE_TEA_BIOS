/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _RC_SETUP_UTILITY_H_
#define _RC_SETUP_UTILITY_H_

#define RC_SETUP_UTILITY_VARIABLE_NAME           L"RcSetupUtility"
#define RC_SETUP_UTILITY_VARIABLE_GUID \
  {0x6659b956, 0xcccb, 0x48aa, 0x92, 0x0b, 0xe6, 0x2f, 0x66, 0x7f, 0xf5, 0x78}

#define RC_SETUP_UTILITY_BROWSER_VARIABLE_NAME   L"RcSetupUtilityBrowser"
#define RC_SETUP_UTILITY_BROWSER_VARIABLE_GUID \
  {0x3877eb12, 0x9286, 0x4e60, 0x82, 0xeb, 0x5a, 0x19, 0x29, 0x50, 0xfc, 0xee}

#pragma pack(1)
typedef struct {
  UINT8                                 *SetupDataNvData;
  UINT8                                 *SaSetupNvData;
  UINT8                                 *MeSetupNvData;
  UINT8                                 *CpuSetupNvData;
  UINT8                                 *PchSetupNvData;
  UINT8                                 *SiSetupNvData;
  UINT8                                 *MeSetupStorageNvData;
} RC_SETUP_UTILITY_DATA;

typedef struct _RC_SETUP_UTILITY_BROWSER_DATA {
  UINTN                                 SetupDataSUBDataSize;
  UINTN                                 SaSUBDataSize;
  UINTN                                 MeSUBDataSize;
  UINTN                                 CpuSUBDataSize;
  UINTN                                 PchSUBDataSize;
  UINTN                                 SiSUBDataSize;
  UINTN                                 MeStorageSUBDataSize;
  UINT8                                 *SetupDataSUBrowserData;
  UINT8                                 *SaSUBrowserData;
  UINT8                                 *MeSUBrowserData;
  UINT8                                 *CpuSUBrowserData;
  UINT8                                 *PchSUBrowserData;
  UINT8                                 *SiSUBrowserData;
  UINT8                                 *MeStorageSUBrowserData;
  } RC_SETUP_UTILITY_BROWSER_DATA;
#pragma pack()

extern EFI_GUID                         gRcSetupUtilityVariableGuid;
extern EFI_GUID                         gRcSetupUtilityBrowserVariableGuid;

#endif
