/** @file
  Definitions for H2O chipset check point

;******************************************************************************
;* Copyright (c) 2018 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef __H2O_CHIPSET_CHECK_POINT_H__
#define __H2O_CHIPSET_CHECK_POINT_H__

#include <Guid/H2OCp.h>
#include <BiosGuard.h> 
//
// Note: Please foloow below policy to add new checkpoint.
// 1. Check points should be grouped 1st by phase (PEI, DXE, SMM or BDS)
// 2. Where possible, check points should be grouped by common topic, such as the driver where they are
//    located or the technology they implement. For example: BdsDxe checkpoints together, or Setup
// 3. Checkpoints together. Where possible, check points should be in execution order. For example: "before"
//    checkpoints should appear before "after", "init" checkpoints before "shutdown" checkpoints.
//

//
// PEI CP GUID definitions
//
#define H2O_PEI_CP_EC_GET_BIOSGUARD_SUPPORTC_GUID \
  { \
    0xDDACAC79, 0x8748, 0x4C9E, 0x9B, 0x86, 0x08, 0x02, 0xB3, 0x86, 0x0E, 0xB3 \
  }

#define H2O_PEI_CP_EC_SEND_SET_BSSB_CONNECTED_GUID \
  { \
    0x00FDC16F, 0x53A3, 0x4CF5, 0xAC, 0x2E, 0x42, 0x1C, 0xD3, 0xD9, 0xEE, 0x49 \
  }

//
// DXE CP GUID definitions
//
#define H2O_DXE_CP_EC_SEND_GET_USCI_VER_GUID \
  { \
    0xFD4D4681, 0xF8C2, 0x4AF2, 0x88, 0xE9, 0x89, 0x52, 0xE6, 0x78, 0xBC, 0x5B \
  }

//
// SMM CP GUID definitions
//
#define H2O_SMM_CP_EC_SEND_ESPI_CLEAR \
  { \
    0x88b6a6d0, 0x073e, 0x428e, 0xad, 0xb5, 0x17, 0x12, 0x99, 0x14, 0x92, 0x24 \
  }
  
//
// SMM CP GUID definitions.
// NOTE: Below GUID defines are deprecated. Suggest using H2O_SMM_CP_XXX_YYY_GUID not Below
// H2O_SMM_CP_XXX_YYY_PROTOCOL_GUID
//
#define H2O_SMM_CP_ESPI_EC_SMI_PROTOCOL_GUID                        H2O_SMM_CP_ESPI_EC_SMI_GUID

//
// BDS CP GUID definitions
//

//
// PEI CP strcuture defintions
//

//
// EC supported BIOS Guard Command use H2O_PEI_CP_BIOSGUARD_EC_SUPPORT otherwise use H2O_PEI_CP_BIOSGUARD_UPDATE_BGPDT.
//
typedef struct _H2O_PEI_CP_BIOSGUARD_EC_SUPPORT {
  UINT32                   Size;         /// Unsigned integer that specifies the size of this structure, in bytes.
  UINT32                   Status;       /// Enumerated value that specifies the Chipset behavior.
  BGPDT                    *Bgpdt;
  UINT8                    EcCmdProvisionEav;
  UINT8                    EcCmdLock;       
} H2O_PEI_CP_BIOSGUARD_EC_SUPPORT;

typedef struct _H2O_PEI_CP_BIOSGUARD_UPDATE_BGPDT {
  UINT32                   Size;         /// Unsigned integer that specifies the size of this structure, in bytes.
  UINT32                   Status;       /// Enumerated value that specifies the Chipset behavior.
  BGPDT                    *Bgpdt;
} H2O_PEI_CP_BIOSGUARD_UPDATE_BGPDT;


typedef struct _H2O_PEI_CP_EC_SEND_SET_BSSB_CONNECTED_DATA {
  UINT32          Size;         /// Unsigned integer that specifies the size of this structure, in bytes.
  UINT32          Status;       /// Enumerated value that specifies the Chipset behavior.
} H2O_PEI_CP_EC_SEND_SET_BSSB_CONNECTED_DATA;

//
// DXE CP strcuture defintions
//
// @Prompt Structure definition for gH2OPeiCpEcGetSendGetUsciVerGuid checkpoint

typedef struct _H2O_DXE_CP_EC_SEND_GET_USCI_VER_DATA {
  UINT32          Size;         /// Unsigned integer that specifies the size of this structure, in bytes.
  UINT32          Status;       /// Enumerated value that specifies the Chipset behavior.
  UINT16          UsciVer;
} H2O_DXE_CP_EC_SEND_GET_USCI_VER_DATA;

//
// SMM CP strcuture defintions
//
// @Prompt Structure definition for gH2OSmmCpEspiEcSmiGuid checkpoint
typedef struct _H2O_SMM_CP_EC_SEND_ESPI_CLEAR_DATA {
  UINT32          Size;         /// Unsigned integer that specifies the size of this structure, in bytes.
  UINT32          Status;       /// Enumerated value that specifies the Chipset behavior.
} H2O_SMM_CP_EC_SEND_ESPI_CLEAR_DATA;

//
// BDS CP strcuture defintions
//


//
// PEI CP GUID definitions
//

extern EFI_GUID gH2OPeiCpBiosGuardEcSupportGuid;
extern EFI_GUID gH2OPeiCpBiosGuardUpdateBgpdtGuid;
extern EFI_GUID gH2OPeiCpEcSendSetBssbConnected;  //v
//extern EFI_GUID gH2OPeiCpEcGetSendGetUsciVerGuid;

//
// DXE CP GUID definitions
//
extern EFI_GUID gH2ODxeCpEcGetSendGetUsciVerGuid;   //v

//
// SMM CP GUID definitions
//
extern EFI_GUID gH2OSmmCpEcSendEspiClearGuid;    //v

//
// BDS CP GUID definitions
//

#endif
