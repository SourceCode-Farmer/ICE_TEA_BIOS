/** @file
 GUID and structure definition of check flash access function.

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************

*/

#ifndef _ASF_SECURE_BOOT_VARIABLE_H_
#define _ASF_SECURE_BOOT_VARIABLE_H_

extern EFI_GUID gAsfSecureBootVariableGuid;

#define ASF_SECURE_BOOT_VARIABLE_NAME   L"AsfSecureBoot"

#endif // _ASF_SECURE_BOOT_VARIABLE_H_

