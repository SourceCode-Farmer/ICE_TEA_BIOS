/** @file

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CHIPSET_CAPSULE_SIGNATURE_H_
#define _CHIPSET_CAPSULE_SIGNATURE_H_

#define CHIPSET_CAPSULE_SIGNATURE_GUID \
  { \
    0x4F2CDD19, 0xBA92, 0xD7E4, {0x14, 0x3F, 0x85, 0xD0, 0x3A, 0x5B, 0x1D, 0x6F} \
  }
extern EFI_GUID gChipsetCapsuleSignatureGuid;

#endif
