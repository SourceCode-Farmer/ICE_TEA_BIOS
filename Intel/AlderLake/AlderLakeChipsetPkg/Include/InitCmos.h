/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*++

@file:

  InitCmos.h

@brief:

  Header file for InitCmos.c.

++*/

#ifndef _INIT_CMOS_H_
#define _INIT_CMOS_H_

#define CMOS_BANK_0_START   0x33
#define CMOS_BANK_0_END     0x7F
#define CMOS_BANK_1_START   0
#define CMOS_BANK_1_END     0x7F

#define CMOS_DEFAULT_VALUE  0

#define LENGTH_BYTE         1
#define LENGTH_WORD         2
#define LENGTH_DWORD        4

#define BANK_NUMBER         BIT0
#define BANK_0              0
#define BANK_1              1

#define INIT_CONTROL        BIT1
#define DO_NOT_INIT         2

#pragma pack (1)
typedef struct {
  UINT8  CmosAddress;
  UINT32 DefaultValue;
  UINT8  LengthInByte;
  UINT8  ControlBitMap;     // bit0 - Bank#; bit1 - If do not init;
} CMOS_DEFAULT_DATA;
#pragma pack ()

#endif
