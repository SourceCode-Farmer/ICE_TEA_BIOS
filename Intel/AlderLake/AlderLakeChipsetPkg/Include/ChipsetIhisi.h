/** @file
  This file provides IHISI Structure and define for Chipset driver

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CHIPSET_IHISI_H_
#define _CHIPSET_IHISI_H_

typedef struct {
  UINT8      IhisiAreaType;
  UINT8      FlashAreaType;
} FLASH_MAP_MAPPING_TABLE;

//[-start-190701-16990084-add]//
#define FLASH_VALID_SIGNATURE           V_FLASH_FDBAR_FLVALSIG    //0x0FF0A55A      //Flash Descriptor is considered valid
#define FLASH_MAP_0_OFFSET              R_FLASH_FDBAR_FLASH_MAP0  //0x04
#define FLASH_MAP_1_OFFSET              R_FLASH_FDBAR_FLASH_MAP1  //0x08
#define PLATFORM_REGION_OFFSET          0x00
#define ROM_FILE_REGION_OFFSET          0x100
#define FLASH_MASTER_1_READ_ACCESS_BIT  8
#define FLASH_MASTER_1_WRITE_ACCESS_BIT 20
//[-end-190701-16990084-add]//
#define TDT_STATE_INACTIVE              0x0
#define TDT_STATE_ACTIVE                0x01
#define TDT_STATE_LOCK_OR_STOLEN        0x02
#define DOS_FLASH_TOOL                  0x01
#define WINDOWS_FLASH_TOOL              0x02
#define LINUX_FLASH_TOOL                0x03
#define SHELL_FLASH_TOOL                0x04

#pragma pack(1)

typedef struct {
  UINT16  Version;
  UINT8   Reserved[2];
  UINT32  Algorithm;
} BGUPC_HEADER;

typedef struct {
  UINT16  Version;
  UINT8   Reserved[2];
  UINT32  Algorithm;
  UINT8   VKeyMod[256];
  UINT8   VKeyExp[4];
  UINT8   Signature[256];
} BGUPC_ALGORITHM_1;

typedef struct _INSYDE_BIOSGUARD_HEADER {
  UINT8   Signature[8];  // $PFATHDR
  UINT32  Version;       // 
  UINT32  BlockCount;    // Block Count (Total count of PUP blocks)
  UINT32  BlockSize;     // Block Size (Size of each PUP block)
  UINT32  StructureSize; // INSYDE_BIOSGUARD_HEADER Structure Size 
} INSYDE_BIOSGUARD_HEADER;

typedef struct _BIOSGUARD_BLOCK_INFO {
  UINT32  Offset; // The data offset of the BiosGuard block.
  UINT32  Size;   // The data size of this BiosGuard block.
} BIOSGUARD_BLOCK_INFO;

typedef struct _INSYDE_CERT_HEADER {
  UINT8   Signature[8]; // $PFATCER
  UINT32  Version;      // 0x00000001
  UINT32  BlockCount;   // Block Count (Total count of PUP blocks)
  UINT32  BlockSize;    // Block Size (Size of each PUP block)
  UINT32  Checksum;
  UINT8   Reserved[8];
} INSYDE_CERT_HEADER;

typedef struct {
  UINT32                  StartAddress;
  UINT32                  Length;
} BIOSGUARD_RECOVERY_PROTECT_REGION;

#pragma pack()

#endif
