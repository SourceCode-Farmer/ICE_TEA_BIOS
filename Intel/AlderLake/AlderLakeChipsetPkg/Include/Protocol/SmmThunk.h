/** @file
  This file defines SMM Thunk abstraction protocol.
;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
@copyright
  Copyright (c) 1999 - 2013 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and uniquely 
  identified as "Intel Reference Module" and is licensed for Intel 
  CPUs and chipsets under the terms of your license agreement with 
  Intel or your vendor.  This file may be modified by the user, subject
  to additional terms of the license agreement.
**/
#ifndef _SMM_THUNK_H_
#define _SMM_THUNK_H_
#include <Protocol/LegacyBios.h>
typedef struct _EFI_SMM_THUNK_PROTOCOL EFI_SMM_THUNK_PROTOCOL;

typedef
BOOLEAN
(EFIAPI *EFI_SMM_FARCALL86)(
  IN EFI_SMM_THUNK_PROTOCOL          *This,
  IN UINT16                          Segment,
  IN UINT16                          Offset,
  IN OUT EFI_IA32_REGISTER_SET *Regs OPTIONAL,
  IN VOID *Stack                     OPTIONAL,
  IN UINTN                           StackSize
  );

typedef
BOOLEAN
(EFIAPI *EFI_SMM_INT10) (
  IN EFI_SMM_THUNK_PROTOCOL           * This,
  IN EFI_IA32_REGISTER_SET            * Regs
  );

typedef
BOOLEAN
(EFIAPI *EFI_SMM_INTCALL86)(
  IN EFI_SMM_THUNK_PROTOCOL          *This,
  IN UINT16                          Segment,
  IN UINT16                          Offset,
  IN OUT EFI_IA32_REGISTER_SET *Regs OPTIONAL,
  IN VOID *Stack                     OPTIONAL,
  IN UINTN                           StackSize
  );

///
/// This protocol abstracts SMM thunk
///
struct _EFI_SMM_THUNK_PROTOCOL {
  EFI_SMM_FARCALL86 FarCall86;
  EFI_SMM_INT10     SmmInt10;
  EFI_SMM_INTCALL86 IntCall86;
};

extern EFI_GUID gSmmThunkProtocolGuid;

#endif
