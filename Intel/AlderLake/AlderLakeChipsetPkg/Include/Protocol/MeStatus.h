/** @file
  Protocol for geting ME firmware status.

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _GET_ME_STATUS_H_
#define _GET_ME_STATUS_H_

#define ME_STATUS_PROTOCOL_GUID  \
  { 0x89e70ece, 0x1d19, 0x455d, 0x97, 0x3f, 0x6d, 0x13, 0xe9, 0x7c, 0xe8, 0x92 }

typedef struct _EFI_ME_STATUS_PROTOCOL EFI_ME_STATUS_PROTOCOL;

struct _EFI_ME_STATUS_PROTOCOL {
  UINT32  Status1;
  UINT32  Status2;
};

extern EFI_GUID gH2OMeStatusProtocolGuid;

#endif
