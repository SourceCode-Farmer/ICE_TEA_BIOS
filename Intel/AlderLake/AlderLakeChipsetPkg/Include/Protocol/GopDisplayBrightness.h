//;******************************************************************************
//;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************
//;
//; Module Name :
//;
//;   GopDisplayBrightness.h
//;

#ifndef _GOP_DISPLAY_BRIGHTNESS_H_
#define _GOP_DISPLAY_BRIGHTNESS_H_

#define GOP_DISPLAY_BRIGHTNESS_PROTOCOL_GUID \
  { \
    0x6FF23F1D, 0x877C, 0x4B1B, 0x93, 0xFC, 0xF1, 0x42, 0xB2, 0xEE, 0xA6, 0xA7 \
  }

//
// Extern the GUID for protocol users.
//
extern EFI_GUID  gGopDisplayBrightnessProtocoGuid;

#define GOP_DISPLAY_BRIGHTNESS_PROTOCOL_REVISION_01        0x01

typedef struct _GOP_DISPLAY_BRIGHTNESS_PROTOCOL GOP_DISPLAY_BRIGHTNESS_PROTOCOL;

/**
 This function will return the maximum brightness level supported by protocol instance.

 @param [in]   This                A pointer to the GOP Display Brightness Protocol.
 @param [out]  MaxBrightnessLevel  An integer value of type UINT32.

 @retval EFI_SUCCESS               Valid MaxBrightnessLevel returned.
 @retval EFI_INVALID_PARAMETER     MaxBrightnessLevel is NULL.

**/
typedef
EFI_STATUS
( EFIAPI *GET_MAXIMUM_BRIGHTNESS_LEVEL ) (
  IN      GOP_DISPLAY_BRIGHTNESS_PROTOCOL        *This,
     OUT  UINT32                                 *MaxBrightnessLevel
  );

/**
 This function will return the current brightness level of the display.

 @param [in]   This                    A pointer to the GOP Display Brightness Protocol.
 @param [out]  CurrentBrightnessLevel  An integer value of type UINT32.

 @retval EFI_SUCCESS                   Valid MaxBrightnessLevel returned.
 @retval EFI_INVALID_PARAMETER         CurrentBrightnessLevel is NULL.

**/
typedef
EFI_STATUS
( EFIAPI *GET_CURRENT_BRIGHTNESS_LEVEL ) (
  IN      GOP_DISPLAY_BRIGHTNESS_PROTOCOL        *This,
     OUT  UINT32                                 *CurrentBrightnessLevel
  );

/**
 This function will set the brightness level of the display.

 @param [in]   This                    A pointer to the GOP Display Brightness Protocol.
 @param [in]   BrightnessLevel         An Integer Value of Type UINT32.

 @retval EFI_SUCCESS                   The brightness level was set.
 @retval EFI_INVALID_PARAMETER         BrightnessLevel is out of range.
 @retval EFI_DEVICE_ERROR              Error in setting the brightness level.

**/
typedef
EFI_STATUS
( EFIAPI *SET_BRIGHTNESS_LEVEL ) (
  IN      GOP_DISPLAY_BRIGHTNESS_PROTOCOL        *This,
  IN      UINT32                                 BrightnessLevel
  );

//
// The GOP_DISPLAY_BRIGHTNESS_PROTOCOL is implemented by the GOP Driver and 
// installed on child handles that are capable of brightness control.
//
typedef struct _GOP_DISPLAY_BRIGHTNESS_PROTOCOL {
  UINT32                              Revision;
  GET_MAXIMUM_BRIGHTNESS_LEVEL        GetMaxBrightnessLevel;
  GET_CURRENT_BRIGHTNESS_LEVEL        GetCurrentBrightnessLevel;
  SET_BRIGHTNESS_LEVEL                SetBrightnessLevel;
} GOP_DISPLAY_BRIGHTNESS_PROTOCOL;

#endif
