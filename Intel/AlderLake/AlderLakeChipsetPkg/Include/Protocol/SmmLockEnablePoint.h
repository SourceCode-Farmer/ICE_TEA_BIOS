/*## @file
#
#******************************************************************************
#* Copyright (c) 2017, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
*/

#ifndef _SMM_LOCK_ENABLE_POINT_H_
#define _SMM_LOCK_ENABLE_POINT_H_

#define EFI_SMM_LOCK_ENABLE_POINT_GUID \
  {  \
   0x402522C1, 0x6264, 0x456F, { 0x8D, 0x79, 0xE7, 0x81, 0x04, 0x62, 0x00, 0x6A  } \
  }

extern EFI_GUID gEfiSmmLockEnablePointProtocolGuid;

#endif
