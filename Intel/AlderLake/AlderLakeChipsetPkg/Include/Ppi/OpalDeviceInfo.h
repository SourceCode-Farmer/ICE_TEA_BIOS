/** @file

  The PEI_OPAL_DEVICE_INFO_PPI provides information of re-mapped TCG supported NVMe.
  This PPI would be installed by pre-OS RST driver when there are TCG supported
  re-mapped NVMes connected on system and it will be consumed by Opal PEI driver
  during S3 resume path.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/


#ifndef __PEI_OPAL_DEVICE_INFO_PPI_H__
#define __PEI_OPAL_DEVICE_INFO_PPI_H__

typedef struct _PEI_OPAL_DEVICE_INFO_PPI PEI_OPAL_DEVICE_INFO_PPI;

typedef struct {
  UINT8             Bus;
  UINT8             Device;
  UINT8             Function;
  UINT16            Port;
  UINT16            PortMultiplierPort;
} PEI_OPAL_DEVICE_INFO;


/**
  This function is used to retrieve MMIO base address of re-mapped TCG supported NVMe .

  @param  This                  The PPI instance pointer.
  @param  DeviceInfo            The device information of re-mapped NVMe which is
                                used to match information in Lockbox.
  @param  MmiosBaseAddress      The base system memory address of the allocated range.
  @param  NvmeNamespaceId       Nvme name space ID for NVME communication.

  @retval EFI_SUCCESS           Device instance with DeviceInfo has been found.
  @retval EFI_INVALID_PARAMETER Parameters are NULL.
  @retval EFI_NOT_FOUND         There is no device instance with matched DeviceInfo.

**/
typedef
EFI_STATUS
(EFIAPI *PEI_OPAL_DEVICE_INFO_GET_DEVICE_INFO)(
  IN  PEI_OPAL_DEVICE_INFO_PPI                 *This,
  IN  PEI_OPAL_DEVICE_INFO                     *DeviceInfo,
  OUT UINTN                                    *MmiosBaseAddress,
  OUT UINT32                                   *NvmeNamespaceId
  );

//
// This PPI contains a set of services to interact with RST driver.
//
struct _PEI_OPAL_DEVICE_INFO_PPI {
  PEI_OPAL_DEVICE_INFO_GET_DEVICE_INFO    GetDeviceInfo;
};

extern EFI_GUID gPeiOpalDeviceInfoPpiGuid;

#endif
