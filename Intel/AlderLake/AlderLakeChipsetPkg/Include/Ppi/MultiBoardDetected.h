/** @file
  MultiBoard Detected PPI

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _PEI_MULTIBOARD_DETECTED_PPI_H
#define _PEI_MULTIBOARD_DETECTED_PPI_H
        
#define PEI_MULTIBOARD_DETECTED_PPI_GUID  \
  { \
    0x48c935ea, 0xc017, 0x440d,   0x96, 0x75, 0xbe, 0x13, 0xaf, 0x82, 0xf1, 0x12 \
  }

extern EFI_GUID gPeiCustomerMultiBoardDetectedPpiGuid;

#endif
