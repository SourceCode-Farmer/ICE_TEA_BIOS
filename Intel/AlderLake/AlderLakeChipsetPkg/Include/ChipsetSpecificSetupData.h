/** @file
  Chipset Setup Configuration Data

;******************************************************************************
;* Copyright (c) 2016 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifdef _IMPORT_CHIPSET_SPECIFIC_SETUP_

//==========================================================================================================
//==========================================================================================================
//----------------------------------------------------------------------------------------------------------
// Start area for Chipset team use. The following area is used by Chipset team to modify.
// The total size of variable in this part are fixed (800 bytes). That means if you need to add or remove
// variables, please modify the ChipsetRSV buffer size as well.
//----------------------------------------------------------------------------------------------------------
  UINT8   IGDBootTypeSecondary;                                        //Offset 400
  UINT8   PannelScaling;                                               //Offset 401
  UINT8   PowerXpress;                                                 //Offset 402
  UINT8   SetupVariableInvalid;                                        //Offset 403
  UINT8   UsbLegacySmiBitClean;                                        //Offset 404
  UINT8   NumberOfCores;                                               //Offset 405
  UINT8   AhciOptionRomSupport;                                        //Offset 406
  UINT8   SataSalpPchH;                                                //Offset 407
  UINT8   S5LongRunTest;                                               //Offset 408
  UINT8   DisplayType;                                                 //Offset 409
  UINT8   PTTEnable;                                                   //Offset 410
  UINT8   PchType;                                                     //Offset 411
  UINT8   MeUnconfigOnRtc;                                             //Offset 412 
  //
  // SEG Feature - Remove H2OUVE relevant source codes
  //
  UINT8   ChipsetRSV28;                                                //Offset 413
  //
  // SA = 0, IA = 1, RING = 2, GTUS = 3, GTS = 4
  //
  UINT8   WheaSupport;                                                 //Offset 414
  UINT8   LogErrors;                                                   //Offset 415
  UINT8   ErrorGeneration;                                             //Offset 416
  UINT8   PchErrors;                                                   //Offset 417
  UINT8   PcieErrors;                                                  //Offset 418
  UINT8   PlatformErrors;                                              //Offset 419
  UINT8   PropagateSerr;                                               //Offset 420
  UINT8   PropagatePerr;                                               //Offset 421
  UINT8   FatErr;                                                      //Offset 422
  UINT8   UnCorRecErr;                                                 //Offset 423
  UINT8   CorErr;                                                      //Offset 424
  UINT8   OverClockingSupport;                                         //Offset 425
  UINT8   DisplayMode;                                                 //Offset 426
  UINT8   FipsAutoSync;                                                //Offset 427
  UINT8   HgSlot;                                                      //Offset 428
  UINT8   ChipsetRSV[771];                                             //Offset 429 - 1199
//----------------------------------------------------------------------------------------------------------
// End of area for Chipset team use.
//----------------------------------------------------------------------------------------------------------
#endif
