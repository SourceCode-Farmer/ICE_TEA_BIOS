/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BASE_INSYDE_CHIPSET_LIBRARY_H_
#define _BASE_INSYDE_CHIPSET_LIBRARY_H_

#include <Uefi.h>

#define EFI_CPUID_VERSION_INFO            0x1

/**
 get cpu's thread ,Core enable  count

 @param [in, out] ThreadCount   thread count number
 @param [in, out] CoreCount     core count number

 @retval EFI_SUCCESS            The function completed successfully.
 @retval EFI_INVALID_PARAMETER  should input one and not null parmeter

**/
EFI_STATUS
 GetTheadCoreCount (
   IN OUT   UINT8   *ThreadCount,
   IN OUT   UINT8   *CoreCount
  );

 UINT64
 GetValidMtrrAddress (
   VOID
 );

 UINT64
 EFIAPI
 GetNextPowerOfTwo64 (
  IN      UINT64                    Operand
 );

 UINT64
 EFIAPI
 GetOnePowerOfTwo64 (
  IN      UINT64                    Operand
 );

/**
 Get Board ID & Fab ID from EC.

 @param[In Out]     *FabId              On entry, points to FabId
                                        On exit , 2-H
                                                  Bit [5:6] Fab ID
                                                  PCH-LP/WPT
                                                  Bit [1:3] Fab ID
 @param[In Out]     *BoardId            On entry, points to BoardId
                                        On exit , 2-H
                                                  Bit [0:4] Board ID
                                                  PCH-LP/WPT
                                                  Bit [0:7] Board ID

 @retval EFI_SUCCESS                    successfully

*/
EFI_STATUS
GetBoardId (
  UINT8        *FabId,
  UINT8        *BoardId
  );

/**
  Get Platform information. Codes refer to PlatformInfoCollect.c's PlatformInfoCollect 

  @param[in out]    *BoardId            Board ID
  @param[in out]    *PlatformType       Platform Type TRAD or ULT
  @param[in out]    *BoardType          Board Type for example with EcolaFalls south board ther are 4 flavors of north board.
  @param[in out]    *PlatformFlavor     Platform Flavor

  @retval           EFI_SUCCESS          Command success
  @retval           EFI_DEVICE_ERROR     Command error
  @retval           EFI_UNSUPPORTED      Command not support
**/
EFI_STATUS
EcGePlatformInformation (
  IN OUT UINT8      *BoardId,
  IN OUT UINT8      *PlatformType,
  IN OUT UINT8      *BoardType,
  IN OUT UINT8      *PlatformFlavor
  );

#endif
