/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BOOT_GUARD_PLATFORM_LIB_H_
#define _BOOT_GUARD_PLATFORM_LIB_H_
#include <Uefi.h>
#include <Library/BootGuardLibVer1.h>
#include <Library/FitPlatformLib.h>
#include <Pi/PiBootMode.h>
#include <Pi/PiHob.h>

//[-start-190701-16990082-add]//
#define SHA1_DIGEST_SIZE                                                ( 20 )
#ifndef SHA256_DIGEST_SIZE
#define SHA256_DIGEST_SIZE                                              ( 32 )
#endif
#define SM3_256_DIGEST_SIZE                                             ( 32 )
#define SHA384_DIGEST_SIZE                                              ( 48 )

#define V_RSA_PUBLIC_KEY_VERSION                                        ( 0x10 )
#define V_RSA_PUBLIC_KEY_KEY_SIZE                                       ( 0x800 )
#define V_RSA_PUBLIC_KEY_EXPONENT                                       ( 0x11 )

#define V_RSASSA_SIGNATURE_VERSION                                      ( 0x10 )
#define RSA_KEY_SIZE_1K                                                 ( 1024 )
#define RSA_KEY_SIZE_2K                                                 ( 2048 )
#define RSA_KEY_SIZE_3K                                                 ( 3072 )
#define PKCS_1_5_RSA_SHA1_SIGNATURE_SIZE                                (RSA_KEY_SIZE_1K / 8)
#define PKCS_1_5_RSA_SHA256_SIGNATURE_SIZE                              (RSA_KEY_SIZE_2K / 8)
#define PKCS_1_5_RSA_SHA384_SIGNATURE_SIZE                              (RSA_KEY_SIZE_3K / 8)

#define V_KEY_ALGORITHM_RSA                                             ( 0x01 )
#define V_KEY_ALGORITHM_RSASSA                                          ( 0x14 )

#define V_KEY_SIGNATURE_VERSION                                         ( 0x10 )

#define V_BOOT_POLICY_MANIFEST_HEADER_STRUCTURE_ID                      ( "__ACBP__" )
#define S_BOOT_POLICY_MANIFEST_HEADER_STRUCTURE_ID                      ( 8 )
#define V_BOOT_POLICY_MANIFEST_HEADER_STRUCT_VERSION                    ( 0x21 )


#define V_IBB_STRUCTURE_ID                                              ( "__IBBS__" )
#define S_IBB_STRUCTURE_ID                                              ( 8 )
#define V_IBB_STRUCT_VERSION                                            ( 0x20 )

#define V_PLATFORM_MANUFACTURER_STRUCTURE_ID                            ( "__PMDA__" )
#define S_PLATFORM_MANUFACTURER_STRUCTURE_ID                            ( 8 )
#define V_PLATFORM_MANUFACTURER_STRUCT_VERSION                          ( 0x20 )

#define V_BOOT_POLICY_MANIFEST_SIGNATURE_STRUCTURE_ID                   ( "__PMSG__" )
#define S_BOOT_POLICY_MANIFEST_SIGNATURE_STRUCTURE_ID                   ( 8 )
#define V_BOOT_POLICY_MANIFEST_SIGNATURE_STRUCT_VERSION                 ( 0x10 )

#define V_KEY_MANIFEST_STRUCTURE_ID                                     ( "__KEYM__" )
#define S_KEY_MANIFEST_STRUCTURE_ID                                     ( 8 )
#define V_KEY_MANIFEST_STRUCTURE_VERSION                                ( 0x21 )

#define ECC_PUBLIC_KEY_STRUCT_KEY_SIZE_DEFAULT                          ( 256 )
#define ECC_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT                           ( ECC_PUBLIC_KEY_STRUCT_KEY_SIZE_DEFAULT/8 )

#define MFT_KEY_USAGE_INDEX_BOOT_POLICY_MANIFEST                        32
#define MFT_KEY_USAGE_INDEX_OEM_BTG_KEY_MANIFEST                        54
#define MFT_KEY_USAGE_INDEX_OEM_ACM_MANIFEST                            56

typedef enum {
  BootGuardBootStateLegacyBoot,
  BootGuardBootStateVerificationPassed,
  BootGuardBootStateVerificationFailed,
  BootGuardBootStateMax
} BOOT_GUARD_BOOT_STATE;

typedef struct _REVOCATION_VALUE {
  UINT8                  ACMSVN;
  UINT8                  BPMSVN;
  UINT8                  KMSVN;
  UINT8                  KMID;
  BOOT_GUARD_BOOT_STATE  BootGuardStatus;
} REVOCATION_VALUE;

typedef struct {
  EFI_HOB_GUID_TYPE      EfiHobGuidType;
  UINT8                  ACMSVN;
  UINT8                  BPMSVN;
  UINT8                  KMSVN;
  UINT8                  KMID;
  BOOT_GUARD_BOOT_STATE  BootGuardStatus;
} REVOCATION_VALUE_HOB;

#pragma pack ( 1 )
typedef union {
    UINT8   digest8[SHA1_DIGEST_SIZE];
    UINT32  digest32[SHA1_DIGEST_SIZE / 4];
} BtgSha1Digest_u;
#pragma pack ()

#pragma pack ( 1 )
typedef union {
    UINT8   digest8[SHA256_DIGEST_SIZE];
    UINT32  digest32[SHA256_DIGEST_SIZE / 4];
    UINT64  digest64[SHA256_DIGEST_SIZE / 8];
} BtgSha256Digest_u;
#pragma pack ()

#pragma pack ( 1 )
typedef union {
    UINT8   digest8[SHA384_DIGEST_SIZE];
    UINT32  digest32[SHA384_DIGEST_SIZE / 4];
    UINT64  digest64[SHA384_DIGEST_SIZE / 8];
} BtgSha384Digest_u;
#pragma pack ()

//
// Hash structure
//
#pragma pack ( 1 )
typedef struct {
  UINT16               HashAlg;
  UINT16               Size;
  BtgSha1Digest_u      HashBuffer;
} SHA1_HASH_STRUCTURE;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT16               HashAlg;
  UINT16               Size;
  BtgSha256Digest_u    HashBuffer;
} SHA256_HASH_STRUCTURE;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT16               HashAlg;
  UINT16               Size;
  BtgSha384Digest_u    HashBuffer;
} SHA384_HASH_STRUCTURE;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT16               HashAlg;
  UINT16               Size;
  UINT8                HashBuffer[];
} SHAX_HASH_STRUCTURE;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT8    Version;                    // 0x10
  UINT16   KeySizeBits;                // 1024 or 2048 or 3072 bits
  UINT32   Exponent;                   // The public exponent
  UINT8    Modulus[1];                 // The modulus in LSB format
} RSA_PUBLIC_KEY_STRUCT;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT8    Version;
  UINT16   KeySizeBits;                // 2048 or 3072 bits
  UINT16   HashAlg;
  UINT8    Signature[];
} RSASSA_SIGNATURE_STRUCT;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT8  Version;                      // 0x10
  UINT16 KeySizeBits;                  // 256 - Number of bits in key. Fixed for SM2
  UINT8  Qx[ECC_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT];  // X component. Fixed size for SM2
  UINT8  Qy[ECC_PUBLIC_KEY_STRUCT_KEY_LEN_DEFAULT];  // Y component. Fixed size for SM2
} ECC_PUBLIC_KEY_STRUCT;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT8    Version;
  UINT16   KeySizeBits;                // 256 or 384 bits
  UINT16   HashAlg;
//  UINT8    R[];                      // R component
//  UINT8    S[];                      // S component
} ECC_SIGNATURE_STRUCT;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT8                      Version;
  UINT16                     KeyAlg;       // TPM_ALG_RSA=0x1 or TPM_ALG_ECC=0x23
  union {                                  // Based on KeyAlg
    RSA_PUBLIC_KEY_STRUCT    RsaKey;
    ECC_PUBLIC_KEY_STRUCT    EccKey;
  } Key;
  UINT16                     SigScheme;    // TPM_ALG_RSASSA=0x14 or TPM_ALG_RSAPSS=0x15 or TPM_ALG_SM2=0x1B
  union {                                  // Based on KeyAlg
    RSASSA_SIGNATURE_STRUCT  SignatureRsa;
    ECC_SIGNATURE_STRUCT     SignatureEcc;
  } Sig;
} KEY_AND_SIGNATURE_STRUCT;
#pragma pack ()

#pragma pack ( 1 )
typedef struct _ACM_HEADER {
	 UINT16 	ModuleType;
	 UINT16 	ModuleSubType;
	 UINT32 	HeaderLen;
	 UINT32 	HeaderVersion;
	 UINT16 	ChipsetId;
	 UINT16 	Flags;
	 UINT32 	ModuleVendor;
	 UINT32 	Date;
	 UINT32 	Size;
	 UINT16 	AcmSvn;
	 UINT16 	SeAcmSvn;
	 UINT32 	CodeControl;
	 UINT32 	ErrorEntryPoint;
	 UINT32 	GdtLimit;
	 UINT32 	GdtBasePtr;
	 UINT32 	SegSel;
	 UINT32 	EntryPoint;
	 UINT8		Rsvd2[64];
	 UINT32 	KeySize;			// 64 DWORDS in the Key
	 UINT32 	ScratchSize;
	// UINT8	  Rsa2048PubKey[256];
	// UINT32	  RsaPubExp;
	// UINT8	  Rsa2048Sig[256];
	// UINT8	  Scratch[572]; 	  // 143 DWORDS = 572 BYTES Scratch Size

} ACM_HEADER;
#pragma pack ()

#pragma pack ( 1 )
typedef struct _BOOT_POLICY_MANIFEST_HEADER {
	UINT8			   StructureId[8];
	UINT8			   StructVersion;		 // 0x21
	UINT8			   HdrStructVersion;	 // 0x20
	UINT16			   HdrSize; 			 // total number of bytes in Header (i.e., offset to first element)
	UINT16			   KeySignatureOffset;	 // Offset from start of Bpm to KeySignature field of Signature Element
	UINT8			   BpmRevision;
	UINT8			   BpmRevocation;
	UINT8			   AcmRevocation;
	UINT8			   Reserved;
	UINT16			   NemPages;
} BOOT_POLICY_MANIFEST_HEADER;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT16        Reserved;
  UINT16        Flags;
  UINT32        Base;
  UINT32        Size;
} IBB_SEGMENT;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
	  UINT8 			  StructureId[8];
	  UINT8 			  StructVersion;   // 0x20
	  UINT8 			  Reserved0;
	  UINT16			  ElementSize;	   // Total number of bytes in the element
	  UINT8 			  Reserved1;
	  UINT8 			  SetType;
	  UINT8 			  Reserved;
	  UINT8 			  PbetValue;
	  UINT32			  Flags;
	  UINT64			  IbbMchBar;
	  UINT64			  VtdBar;
	  UINT32			  DmaProtBase0;
	  UINT32			  DmaProtLimit0;
	  UINT64			  DmaProtBase1;
	  UINT64			  DmaProtLimit1;
	  SHAX_HASH_STRUCTURE PostIbbHash;
	//	UINT32				IbbEntryPoint;
	//	HASH_LIST			DigestList;
	//	HASH_STRUCTURE		ObbHash;
	//	UINT8				Reserved2[3];
	//	UINT8				SegmentCount;
	//	IBB_SEGMENT*		IbbSegment;  //IbbSegment[SegmentCount]
} IBB_ELEMENT;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT8               StructureId[8];
  UINT8               StructVersion;   // 0x20
  UINT8               Reserved0;
  UINT16              ElementSize;     // Total number of bytes in the element
  UINT16              Reserved1;
  UINT16              PmDataSize;      // required to be 4 byte multiple
  UINT8*              PmData; //PmData[PmDataSize]
} PLATFORM_MANUFACTURER_ELEMENT;
#pragma pack ()

#pragma pack ( 1 )
typedef struct {
  UINT8               StructureId[8];
  UINT8               StructVersion;       // 0x20
  UINT8               Reserved[3];         // KeySignature must be DWORD aligned
  KEY_AND_SIGNATURE_STRUCT  KeySignature;  // this is now a variable Size
} BOOT_POLICY_MANIFEST_SIGNATURE_ELEMENT;

#pragma pack ()

#pragma pack ( 1 )
typedef struct _KEY_MANIFEST {
	  UINT8 			  StructureId[8];
	  UINT8 			  StructVersion;	   // 0x21
	  UINT8 			  reserved[3];		   // 3 bytes to make KeySignatureOffset same offset as for BPM
	  UINT16			  KeySignatureOffset;  // Offset from start of KM to KeyManifestSignature
	  UINT8 			  Reserved2[3]; 	   // Alignment
	  UINT8 			  KeyManifestRevision;
	  UINT8 			  KmSvn;
	  UINT8 			  KeyManifestId;
	  UINT16			  KmPubKeyHashAlg;
	  UINT16			  KeyCount;
	//	KMHASH_STRUCT*		 KeyHash;  //KeyHash[KM_MAX_KEYS];
	//	KEY_AND_SIGNATURE_STRUCT  KeyManifestSignature;
} KEY_MANIFEST;
#pragma pack ()
//[-end-190701-16990082-add]//

EFI_STATUS
BootGuardPlatformLibGetMeStatus (
  OUT UINT32        *MeStatus
  );

BOOT_GUARD_BOOT_STATE
BootGuardPlatformLibDetermineBootState (
  VOID
  );

EFI_STATUS
BootGuardPlatformLibValidateBPMStructureID (
  IN      BOOT_POLICY_MANIFEST_HEADER        *BPMH
  );

EFI_STATUS
BootGuardPlatformLibValidateKMStructureID (
  IN      KEY_MANIFEST        *KM
  );

EFI_STATUS
BootGuardPlatformLibGetKMID (
     OUT  UINT8        *KMID
  );

EFI_STATUS
BootGuardPlatformLibGetACMEx (
  IN      VOID              *Image,
  IN      UINTN             ImageLength,
  IN      FIT_ENTRY         *FIT,
     OUT  ACM_HEADER        **ACMH
  );

EFI_STATUS
BootGuardPlatformLibGetACM (
  IN      FIT_ENTRY         *FIT,
     OUT  ACM_HEADER        **ACMH
  );

EFI_STATUS
BootGuardPlatformLibGetBPMEx (
  IN      VOID                               *Image,
  IN      UINTN                              ImageLength,
  IN      FIT_ENTRY                          *FIT,
     OUT  BOOT_POLICY_MANIFEST_HEADER        **BPMH
  );

EFI_STATUS
BootGuardPlatformLibGetBPM (
  IN      FIT_ENTRY                          *FIT,
     OUT  BOOT_POLICY_MANIFEST_HEADER        **BPMH
  );

EFI_STATUS
BootGuardPlatformLibGetKMEx (
  IN      VOID                *Image,
  IN      UINTN               ImageLength,
  IN      FIT_ENTRY           *FIT,
     OUT  KEY_MANIFEST        **KM,
  IN      UINT8               *CurrentKMID
  );

EFI_STATUS
BootGuardPlatformLibGetKM (
  IN      FIT_ENTRY           *FIT,
     OUT  KEY_MANIFEST        **KM
  );

EFI_STATUS
BootGuardPlatformLibGetRevocationValues (
  IN      VOID                    *Image,
  IN      UINTN                   ImageLength,
     OUT  REVOCATION_VALUE        *Revocation,
  IN      UINT8                   *CurrentKMID
  );

#endif
