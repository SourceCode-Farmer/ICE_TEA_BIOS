/** @file
  Definition for MultiBoard Support Lib.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _MULTI_BOARD_SUPPORT_LIB_H_
#define _MULTI_BOARD_SUPPORT_LIB_H_

/**
  This function will provide OEM to customize and set the related PCD values.

  @retval RETURN_SUCCESS       The function was executed successfully.
**/
EFI_STATUS
EFIAPI
CustomerMultiBoardDetect (
  VOID
  );

#endif
