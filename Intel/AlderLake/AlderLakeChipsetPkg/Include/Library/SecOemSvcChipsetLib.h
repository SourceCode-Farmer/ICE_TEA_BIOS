/** @file
  Definition for SEC OEM Services Chipset Lib.

;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _SEC_OEM_SVC_CHIPSET_LIB_H_
#define _SEC_OEM_SVC_CHIPSET_LIB_H_

#include <InitCmos.h>

// /**
//  This function will provide OEM CMOS default table.

//  @param[in, out]    CmosDefaultTable    The CMOS default table from OEM.

//  @retval            EFI_UNSUPPORTED     Returns unsupported by default.
//  @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
//  @retval            EFI_SUCCESS         The function performs the same operation as caller.
//                                         The caller will skip the specified behavior and assuming
//                                         that it has been handled completely by this function.
// */
// EFI_STATUS
// OemSvcGetInitCmosTable (
//   OUT CMOS_DEFAULT_DATA       **CmosDefaultTable
//   );

#endif
