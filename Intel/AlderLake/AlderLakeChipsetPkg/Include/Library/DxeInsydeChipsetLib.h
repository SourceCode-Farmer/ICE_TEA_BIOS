/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _DXE_INSYDE_CHIPSET_LIBRARY_H_
#define _DXE_INSYDE_CHIPSET_LIBRARY_H_

#include <Protocol/CpuIo2.h>
#include <Protocol/PciRootBridgeIo.h>
#include <ChipsetSetupConfig.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PcdLib.h>
#include <MeSetup.h>
//
// RegTable OpCodes are encoded as follows:
//
//  |31----------------------------16|15---------8|7-------0|
//                                 \             \         \
//                                 \             \         \
//  31:16 defined by Base OpCode---+             \         \
//                                Opcode Flags---+         \
//                                           Base OpCode---+
//
#define OPCODE_BASE(OpCode)       ((UINT8)((OpCode) & 0xFF))
#define OPCODE_FLAGS(OpCode)      ((UINT8)(((OpCode) >> 8) & 0xFF)) 
#define OPCODE_EXTRA_DATA(OpCode) ((UINT16)((OpCode) >> 16))

//
// RegTable Base OpCodes
//
#define OP_TERMINATE_TABLE                0
#define OP_MEM_WRITE                      1
#define OP_MEM_READ_MODIFY_WRITE          2
#define OP_IO_WRITE                       3
#define OP_IO_READ_MODIFY_WRITE           4
#define OP_PCI_WRITE                      5
#define OP_PCI_READ_MODIFY_WRITE          6
#define OP_STALL                          7

//
// RegTable OpCode Flags
//
#define OPCODE_FLAG_S3SAVE                1

#define TERMINATE_TABLE { (UINT32) OP_TERMINATE_TABLE, (UINT32) 0, (UINT32) 0 }

//
// REG_TABLE_ENTRY_PCI_WRITE encodes the width in the upper bits of the OpCode
// as one of the EFI_PCI_ROOT_BRIDGE_IO_PROTOCOL_WIDTH values
//
typedef struct {
  UINT32                                OpCode;
  UINT32                                PciAddress;
  UINT32                                Data;
} EFI_REG_TABLE_PCI_WRITE;

#define PCI_WRITE(Bus, Dev, Fnc, Reg, Width, Data, S3Flag)                    \
  {                                                                           \
    (UINT32) (OP_PCI_WRITE | ((S3Flag) << 8) | ((Width) << 16)),              \
    (UINT32) (EFI_PCI_ADDRESS ((Bus), (Dev), (Fnc), (Reg))),                  \
    (UINT32) (Data),                                                          \
    (UINT32) (0)                                                              \
  }

typedef struct {
  UINT32                                OpCode;
  UINT32                                PciAddress;
  UINT32                                OrMask;
  UINT32                                AndMask;
} EFI_REG_TABLE_PCI_READ_MODIFY_WRITE;

typedef struct {
  UINT32                                OpCode;
  UINT32                                MemAddress;
  UINT32                                OrMask;
  UINT32                                AndMask;
} EFI_REG_TABLE_MEM_READ_MODIFY_WRITE;

typedef struct {
  UINT32                                OpCode;
  UINT32                                Field2;
  UINT32                                Field3;
  UINT32                                Field4;
} EFI_REG_TABLE_GENERIC;

typedef union {
  EFI_REG_TABLE_GENERIC                 Generic;
  EFI_REG_TABLE_PCI_WRITE               PciWrite;
  EFI_REG_TABLE_PCI_READ_MODIFY_WRITE   PciReadModifyWrite;
  EFI_REG_TABLE_MEM_READ_MODIFY_WRITE   MemReadModifyWrite;
} EFI_REG_TABLE;

/**
 Processes register table assuming which may contain PCI, IO, MEM, and STALL
 entries.
 No parameter checking is done so the caller must be careful about omitting
 values for PciRootBridgeIo or CpuIo parameters.  If the regtable does
 not contain any PCI accesses, it is safe to omit the PciRootBridgeIo (supply
 NULL).  If the regtable does not contain any IO or Mem entries, it is safe to
 omit the CpuIo (supply NULL).
 The RegTableEntry parameter is not checked, but is required.
 gBS is assumed to have been defined and is used when processing stalls.
 The function processes each entry sequentially until an OP_TERMINATE_TABLE
 entry is encountered.

 @param   RegTableEntry     A pointer to the register table to process
 @param   PciRootBridgeIo   A pointer to the instance of PciRootBridgeIo that is used 
                            when processing PCI table entries
 @param   CpuIo             A pointer to the instance of CpuIo that is used when processing 
                            IO and MEM table entries

 @retval Nothing.

**/
VOID
ProcessRegTablePci (
  EFI_REG_TABLE                   * RegTableEntry,
  EFI_PCI_ROOT_BRIDGE_IO_PROTOCOL * PciRootBridgeIo,
  EFI_CPU_IO2_PROTOCOL            * CpuIo
  );

EFI_STATUS
EFIAPI
GetChipsetSetupVariableDxe (
  IN OUT CHIPSET_CONFIGURATION          *SetupVariable,
  IN     UINTN                          SetupVariableSize
  );

EFI_STATUS
EFIAPI
GetChipsetSetupDataVariableDxe (
  IN OUT SETUP_DATA                     *SetupData,
  IN     UINTN                          SetupDataSize
  );

EFI_STATUS
EFIAPI
GetChipsetSaSetupVariableDxe (
  IN OUT SA_SETUP                       *SaSetup,
  IN     UINTN                          SaSetupSize
  );

EFI_STATUS
EFIAPI
GetChipsetMeSetupVariableDxe (
  IN OUT ME_SETUP                       *MeSetup,
  IN     UINTN                          MeSetupSize
  );

EFI_STATUS
EFIAPI
GetChipsetCpuSetupVariableDxe (
  IN OUT CPU_SETUP                      *CpuSetup,
  IN     UINTN                          CpuSetupSize
  );

EFI_STATUS
EFIAPI
GetChipsetPchSetupVariableDxe (
  IN OUT PCH_SETUP                      *PchSetup,
  IN     UINTN                          PchSetupSize
  );

EFI_STATUS
EFIAPI
GetChipsetMeSetupStorageVariableDxe (
  IN OUT ME_SETUP_STORAGE               *MeSetupStorage,
  IN     UINTN                          MeSetupStorageSize
  );

UINT8
DxeGetBootType (
  VOID
  );

#endif
