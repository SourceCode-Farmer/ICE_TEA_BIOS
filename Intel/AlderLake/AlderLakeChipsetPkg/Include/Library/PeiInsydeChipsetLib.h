/** @file

;******************************************************************************
;* Copyright (c) 2015-2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _PEI_INSYDE_CHIPSET_LIBRARY_H_
#define _PEI_INSYDE_CHIPSET_LIBRARY_H_

#include <Uefi.h>
#include <ChipsetSetupConfig.h>
#include <SetupVariable.h>
#include <MeSetup.h>

EFI_STATUS
EFIAPI
GetChipsetSetupVariablePei (
  OUT CHIPSET_CONFIGURATION             *SetupVariable
  );

EFI_STATUS
EFIAPI
GetChipsetSetupDataVariablePei (
  OUT SETUP_DATA                          *SetupVariable
  );

EFI_STATUS
EFIAPI
GetChipsetSaSetupVariablePei (
  OUT SA_SETUP                          *SaSetupVariable
  );

EFI_STATUS
EFIAPI
GetChipsetMeSetupVariablePei (
  OUT ME_SETUP                          *MeSetupVariable
  );

EFI_STATUS
EFIAPI
GetChipsetCpuSetupVariablePei (
  OUT CPU_SETUP                         *CpuSetupVariable
  );

EFI_STATUS
EFIAPI
GetChipsetPchSetupVariablePei (
  OUT PCH_SETUP                         *PchSetupVariable
  );

EFI_STATUS
EFIAPI
GetChipsetMeSetupStorageVariablePei (
  OUT ME_SETUP_STORAGE                  *MeSetupStorageVariable
  );

VOID
EFIAPI
ResetFull (
  VOID
  );
#endif
