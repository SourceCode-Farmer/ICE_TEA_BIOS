/** @file
  Definition for Capsule Update Recovery Criteria Library Class

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CHIPSET_CAPSULE_RECOVERY_LIB_H_
#define _CHIPSET_CAPSULE_RECOVERY_LIB_H_

#include <Uefi.h>

/**
  Determine whether ME state is in recovery mode.

  @retval TRUE                          Is in Recovery mode.
  @retval FALSE                         Not in Recovery mode.

**/  
BOOLEAN
EFIAPI
IsMeStateRecovery (
  VOID
  );

/**
  Determine whether is up to maximum try of ME recovery.

  @param[in] Recorder                   If TRUE, set the variable to record the times of recovery try.
  
  @retval TRUE                          Is up to maximum try.
  @retval FALSE                         Not up to maximum try.

**/
BOOLEAN
EFIAPI
IsMaxRecoveryTry (
  IN  BOOLEAN                           Recorder
  );

/**
  Clear the variable of maximum try of ME recovery.

  @retval EFI_SUCCESS                Get EFI variable Successful.
  @return Other                      Other errors cause get variable failed.
  
**/
EFI_STATUS
EFIAPI
ClearMaxRecoveryTryVariable (
  VOID
  );

/**
  Search the recovery image file and do the image flash process.

  @retval EFI_SUCCESS                   The device was successfully updated with the new image.
  @retval EFI_NOT_FOUND                 The specified file could not be found on the device.
  
**/
EFI_STATUS
MeRecovery (
  VOID
  );
#endif
