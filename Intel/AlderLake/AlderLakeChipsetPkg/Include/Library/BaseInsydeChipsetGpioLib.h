/** @file

;******************************************************************************
;* Copyright (c) 2015 - 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "GpioLibrary.h"
#include <Library/GpioNativeLib.h>

typedef enum {
  GpioHostOwnershipRegister = 0,
  GpioGpeEnableRegister,
  GpioGpeStatusRegister,
  GpioSmiEnableRegister,
  GpioSmiStatusRegister,
  GpioNmiEnableRegister,
  GpioPadConfigLockRegister,
  GpioPadLockOutputRegister
} GPIO_REG;

EFI_STATUS
GpioGetGpiSmiSts (
  IN GPIO_PAD                  GpioPad,
  OUT UINT32*                  Data
  );

EFI_STATUS
GpioEnableGpiSmi (
  IN GPIO_PAD                  GpioPad,
  IN BOOLEAN                   Enable
  );