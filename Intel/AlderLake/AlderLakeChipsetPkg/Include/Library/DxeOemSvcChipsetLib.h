/** @file
  Definition for Dxe OEM Services Chipset Lib.

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _DXE_OEM_SVC_CHIPSET_LIB_H_
#define _DXE_OEM_SVC_CHIPSET_LIB_H_
#include <Uefi.h>
#include <IndustryStandard/Acpi.h>
#include <Protocol/IgdOpRegion.h>
#include <Protocol/PciRootBridgeIo.h>
#include <Protocol/UsbLegacyPlatform.h>
#include <Protocol/PlatformNvsArea.h>
#include <Protocol/AmtPolicy.h>
#include <Protocol/GopPolicy.h>
#include <Protocol/MePolicy.h>
#include <Protocol/SaPolicy.h>
#include <ChipsetSetupConfig.h>
#include <Library/BadgingSupportLib.h>
#include <Protocol/BiosRegionLock.h>
#include <ChipsetSetupConfig.h>
#include <SetupVariable.h>

#define SCU_ACTION_LOAD_DEFAULT BIT0
/**

 This function provides an interface to update IGD OpRegion content.

 @param [in out]    *IgdOpRegion        On entry, points to memorys buffer for Internal graphics device,
                                        this buffer set aside comminocate between ACPI code and POST.
                                        On exit , points to updated memory buffer for Internal Graphics Device.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
*/
EFI_STATUS
OemSvcSetIgdOpRegion (
  IN OUT  IGD_OPREGION_PROTOCOL     *IgdOpRegion
  );

/**
 This function provides an interface to hook before and after DxePlatformEntryPoint (PlatformDxe.inf).PlatformDxe.inf
 is responsible for setting up any platform specific policy or initialization information.

 @param[in out]     *SetupVariable      On entry, points to CHIPSET_CONFIGURATION instance.
                                        On exit , points to updated CHIPSET_CONFIGURATION instance.
 @param[in out]     *PciRootBridgeIo    On entry, points to EFI_PCI_ROOT_BRIDGE_IO_PROTOCOL instance.
                                        On exit , points to updated EFI_PCI_ROOT_BRIDGE_IO_PROTOCOL instance.
 @param[in out]     Flag                TRUE : Before DxePlatformEntryPoint.
                                        FALSE: After DxePlatformEntryPoint.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcHookPlatformDxe (
  IN  CHIPSET_CONFIGURATION             *SetupVariable,
  IN  EFI_PCI_ROOT_BRIDGE_IO_PROTOCOL  *PciRootBridgeIo,
  IN OUT  BOOLEAN                      Flag
  );

/**
 This function provides an interface to modify USB legacy options.

 @param[in out]     *UsbLegacyModifiers On entry, points to USB_LEGACY_MODIFIERS instance.
                                        On exit , points to updated USB_LEGACY_MODIFIERS instance.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
*/
EFI_STATUS
OemSvcSetUsbLegacyPlatformOptions (
  IN OUT  USB_LEGACY_MODIFIERS                *UsbLegacyModifiers
  );

/**
 This function provides an interface to hook PlatformResetSystem

 @param[in]         ResetType           Warm or cold.
 @param[in]         ResetStatus         Possible cause of reset.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcHookPlatformReset (
  IN EFI_RESET_TYPE           ResetType,
  IN EFI_STATUS               ResetStatus
  );
/**
 This function provides an interface to modify OEM Logo and POST String.

 @param[in out]     *EFI_OEM_BADGING_LOGO_DATA                   On entry, points to a structure that specifies image data.
                                                                 On exit , points to updated structure.
 @param[in out]     *BadgingDataSize    On entry, the size of EFI_OEM_BADGING_LOGO_DATA matrix.
                                        On exit , the size of updated EFI_OEM_BADGING_LOGO_DATA matrix.
 @param[in out]     *OemBadgingString   On entry, points to OEM_BADGING_STRING matrix.
                                        On exit , points to updated OEM_BADGING_STRING matrix.
 @param[in out]     *OemBadgingStringInTextMode                  On entry, points to OEM_BADGING_STRING matrix in text mode.
                                                                 On exit , points to updated OEM_BADGING_STRING matrix in text mode.
 @param[in out]     *StringCount        The number is POST string count.
                                        On entry, base on SetupVariable->QuietBoot
                                                  1 : The number of entries in OemBadgingString,
                                                  0 : The number of entries in OemBadgingStringInTextMode.
                                        On exit , base on SetupVariable->QuietBoot
                                                  1 : The number of entries in updated OemBadgingString,
                                                  0 : The number of entries in updated OemBadgingStringInTextMode.
 @param[in out]     *OemBadgingStringAfterSelectWithMe           On entry, points to OEM_BADGING_STRING matrix after selected.
                                                                 On exit , points to updated OEM_BADGING_STRING matrix after selected.
 @param[in out]     *OemBadgingStringAfterSelectWithMeInTextMode On entry, points to OEM_BADGING_STRINGmatrix after selected  in text mode.
                                                                 On exit , points to updated OEM_BADGING_STRING matrix after selected  in text mode.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdateOemBadgingLogoData (
  IN OUT EFI_OEM_BADGING_LOGO_DATA            **EfiOemBadgingLogoData,
  IN OUT UINTN                                *BadgingDataSize,
  IN OUT OEM_BADGING_STRING                   **OemBadgingString,
  IN OUT OEM_BADGING_STRING                   **OemBadgingStringInTextMode,
  IN OUT UINTN                                *StringCount,
  IN OUT OEM_BADGING_STRING                   **OemBadgingStringAfterSelectWithMe,
  IN OUT OEM_BADGING_STRING                   **OemBadgingStringAfterSelectWithMeInTextMode
  );
/**
 This function provides an interface to update GlobalNvs table content.

 @param[in, out]    mPlatformNvsArea    On entry, points to PLATFORM_NVS_AREA instance.
                                        On exit , points to updated PLATFORM_NVS_AREA instance.
 @param[in, out]    mOemPlatformNvsArea On entry, points to OEM_PLATFORM_NVS_AREA instance.
                                        On exit , points to updated OEM_PLATFORM_NVS_AREA instance.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdatePlatformNvs (
  PLATFORM_NVS_AREA                     *mPlatformNvsArea,
  OEM_PLATFORM_NVS_AREA                 *OemArea
  );
/**
 This function offers an interface to modify DXE_AMT_POLICY_PROTOCOL Data before the system
 installs DXE_AMT_POLICY_PROTOCOL.

 @param[in, out]    *AMTPlatformPolicy  On entry, points to DXE_AMT_POLICY_PROTOCOL structure.
                                        On exit, points to updated DXE_AMT_POLICY_PROTOCOL structure.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdateAmtPlatformPolicy (
  IN OUT AMT_POLICY_PROTOCOL        *AmtPlatformPolicy
  );


/**
 This function offers an interface to modify DXE_ME_POLICY_PROTOCOL Data before the system
 installs DXE_ME_POLICY_PROTOCOL.

 @param[in, out]    *MePlatformPolicy   On entry, points to DXE_ME_POLICY_PROTOCOL structure.
                                        On exit, points to updated DXE_ME_POLICY_PROTOCOL structure.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdateDxeMePolicy (
  IN OUT ME_POLICY_PROTOCOL         *MePlatformPolicy
  );

/**
 This function offers an interface to modify SA_POLICY_PROTOCOL Data before the system
 installs SA_POLICY_PROTOCOL.

 @param[in, out]    *SaPlatformPolicy   On entry, points to SA_POLICY_PROTOCOL structure.
                                        On exit, points to updated SA_POLICY_PROTOCOL structure.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdateDxePlatformSaPolicy (
  IN OUT SA_POLICY_PROTOCOL          *SaPlatformPolicy
  );


/**
  Customize BIOS protect region before boot to OS.

  @param[out]        *BiosRegionTable    Pointer to BiosRegion Table.
  @param[out]        ProtectRegionNum    The number of Bios protect region instances.

  @retval            EFI_UNSUPPORTED     Returns unsupported by default, OEM did not implement this function.
  @retval            EFI_MEDIA_CHANGED   Provide table for kernel to set protect region and lock flash program registers.
**/
EFI_STATUS
OemSvcUpdateBiosProtectTable (
  OUT BIOS_PROTECT_REGION           **BiosRegionTable,
  OUT UINT8                         *ProtectRegionNum
  );
/**

 This function provides an interface to set Boot Display Device

  @param             SetupNVRam         Points to CHIPSET_CONFIGURATION instance
  @param             SaSetupNVRam       Points to SA_SETUP instance

  @retval            EFI_UNSUPPORTED    Returns unsupported by default.
  @retval            EFI_MEDIA_CHANGED  Alter the Configuration Parameter.
  @retval            EFI_SUCCESS        The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcSetBootDisplayDevice (
  IN OUT CHIPSET_CONFIGURATION          *SetupNVRam,
  IN OUT SA_SETUP                       *SaSetupNVRam
  );


/**
  This function offers an interface to update a ACPI DSDT table which is signatured by OEM define.

  @param[in]        *Signature          Pointer to ACPI Common Header Signature.

  @retval           EFI_UNSUPPORTED     Returns unsupported by default.
  @retval           EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
  @retval           EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdateDsdtAcpiTable (
  IN CONST UINT32                    *Signature
);

/**
  This function provides an interface to hook GenericRouteConfig.

  @param[in,out]     ScBuffer            A pointer to CHIPSET_CONFIGURATION struct.
  @param[in]         BufferSize          System configuration size.
  @param[in,out]     RcScBuffer          A pointer to RC relative variables struct.
  @param[in]         ScuRecord           The bit mask of the currently SCU record.
                                           Bit 0 = 1 (SCU_ACTION_LOAD_DEFAULT), It indicates system do load default action.

  @retval            EFI_UNSUPPORTED     This function is a pure hook; Chipset code don't care return status.
  @retval            EFI_SUCCESS         This function is a pure hook; Chipset code don't care return status.
**/
EFI_STATUS
OemSvcHookRouteConfig (
  IN OUT CHIPSET_CONFIGURATION          *ScBuffer,
  IN     UINT32                         BufferSize,
  IN OUT VOID                           *RcScBuffer,
  IN     UINT32                         ScuRecord
  );


/**
 This function offers an interface to initial Thunderbolt Setup data in SetupRuntimeUpdateEveryBoot.

 @param[in]         *SetupData
 @param[in]         *SaSetup
 @param[in]         *PchSetup

 @retval            EFI_UNSUPPORTED      Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED    Alter the Configuration Parameter.
 @retval            EFI_SUCCESS          The function performs the same operation as caller.
                                         The caller will skip the specified behavior and assuming
                                         that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcInitTbtFunc (
  IN CHIPSET_CONFIGURATION            *SetupData,
  IN SETUP_DATA                       *RcSetupData,
  IN SA_SETUP                         *SaSetup,
  IN PCH_SETUP                        *PchSetup
  );

/**

 This function provides an interface to update backlight brightness value.

 @retval            EFI_UNSUPPORTED      Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED    Alter the Configuration Parameter.
 @retval            EFI_SUCCESS          The function performs the same operation as caller.
                                         The caller will skip the specified behavior and assuming
                                         that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcSetBacklightControl(
  VOID
  );

#endif
