/** @file
;******************************************************************************
;* Copyright (c) 2014 - 2016, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/**
  KSC library functions and definitions.

  This library provides basic KSC interface.  It is deemed simple enough and uses in
  so few cases that there is not currently benefit to implementing a protocol.
  If more consumers are added, it may be benefitial to implement as a protocol.
  There may be different libraries for different environments (PEI, BS, RT, SMM).
  Make sure you meet the requirements for the library (protocol dependencies, use
  restrictions, etc).

@copyright
  Copyright (c) 2013 - 2014 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains a 'Sample Driver' and is licensed as such
  under the terms of your license agreement with Intel or your
  vendor.  This file may be modified by the user, subject to
  the additional terms of the license agreement.
**/

#ifndef _RC_KSC_LIB_H_
#define _RC_KSC_LIB_H_

///
/// Include files
///
#include <Uefi/UefiBaseType.h>
#include <PlatformBoardId.h>
/**
  Detect EC

  @param[out] EcMajorRevision
  @param[out] EcMinorRevision

  @retval     TRUE            EC is present on the system.
  @retval     FALSE           EC is NOT present on the system.
**/
BOOLEAN
DetectEc1 (
  OUT UINT8           *EcMajorRevision,
  OUT UINT8           *EcMinorRevision
  );

/**
  Get fab ID from EC

  @param[out] FabId              Output fab ID

  @retval EFI_SUCCESS            Get fab ID successfully
  @retval EFI_INVALID_PARAMETER  FabId is NULL
  @retval Other                  Fail to get fab ID from EC
**/
EFI_STATUS
EcLibGetFabId (
  OUT UINT8        *FabId
  );

/**
  Get board ID from EC

  @param[out] BoardId            Output board ID

  @retval EFI_SUCCESS            Get board ID successfully
  @retval EFI_INVALID_PARAMETER  BoardId is NULL
  @retval Other                  Fail to get board ID from EC
**/
EFI_STATUS
EcLibGetBoardId (
  OUT UINT8        *BoardId
  );

/**
  Get the number of battery

  @param[out] DataBuffer        The number of battery
                                Bit 0 for battery 1 status and bit 1 for battery 2
                                Bit 7 and 6 are Battery present indicator bits from EC
  @retval     EFI_SUCCESS       Command success
  @retval     EFI_DEVICE_ERROR  Command error
**/
EFI_STATUS
GetBatteryNumber1 (
  OUT UINT8    *DataBuffer
  );

/**
  Get PCIe dock status

  @param[out] DataBuffer        PCIe dock status 
                                The bit0 is PCIe Dock Status, 1 = docked

  @retval     EFI_SUCCESS       Command success
  @retval     EFI_DEVICE_ERROR  Command error
**/
EFI_STATUS
GetPcieDockStatus1 (
  OUT UINT8    *DataBuffer
  );

/**
  Check there is SPD or not, and get CRB board BOM ID.

  @param[out]  SpdPresent
  @param[out]  BomId

  @retval     EFI_SUCCESS       Command success
  @retval     EFI_DEVICE_ERROR  Command error
  @retval     EFI_UNSUPPORTED   Command not support
**/
EFI_STATUS
CheckSpdPresentAndBoardBomId (
  OUT BOOLEAN       SpdPresent,
  OUT UINT8         BomId
  );
#endif
