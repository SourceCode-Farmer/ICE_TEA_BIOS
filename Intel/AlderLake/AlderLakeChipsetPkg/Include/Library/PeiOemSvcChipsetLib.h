/** @file
  Definition for PEI OEM Services Chipset Lib.

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _PEI_OEM_SVC_CHIPSET_LIB_H_
#define _PEI_OEM_SVC_CHIPSET_LIB_H_

#include <Ppi/SiPolicy.h>
#include <SioCommon.h>
#include <Library/DebugLib.h>
#include <ChipsetGpio.h>
#include <Library/ConfigBlockLib.h>
#include <InitCmos.h>
#include <Guid/MemoryTypeInformation.h>

typedef enum {
  MEMORY_ABSENT,    ///< No memory down and no physical memory slot.
  MEMORY_SLOT_ONLY, ///< No memory down and a physical memory slot.
  MEMORY_DOWN_ONLY, ///< Memory down and not a physical memory slot.
} MemorySlotStatus;



typedef struct {
  UINT8 Register;    ///< GPIO register
  UINT8 Value;       ///< GPIO value
} EFI_SIO_GPIO_TABLE;

#pragma pack(1)
typedef struct {
  UINT8   Bus;
  UINT8   Device;
  UINT8   Function;
  UINT32  SsidSvid;
} SSID_SVID_PEI_CONFIG;
#pragma pack()

/**
 This function offers an interface to dynamically modify GPIO table.

 @param[in, out]    GpioTable           On entry, points to a structure that specifies the GPIO setting.
                                        On exit, points to the updated structure.
 @param[in, out]    GpioTableCount      On entry, points to a value that the length of GPIO table
                                        On exit, points to the updated value.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcModifyGpioSettingTable (
  IN OUT  GPIO_INIT_CONFIG                 **GpioTable,
  IN OUT  UINT16                           *GpioTableCount
  );

/**
 This function offers an interface to Dynamically modify gChipsetPkgTokenSpaceGuid.PcdPeiSioGpioTable,
 gChipsetPkgTokenSpaceGuid.PcdPeiSioTable or change procedure about SIO initial.

 @param[in, out]    SioTable            On entry, points to a structure that specifies the SIO register and value.
                                        On exit, points to the updated structure.
                                        The default value is decided by gChipsetPkgTokenSpaceGuid.PcdPeiSioTable.
 @param[in, out]    SioGpioTable        On entry, points to a structure that specifies the SIO GPIO register and value.
                                        On exit, points to the updated structure.
                                        The default value is decided by gChipsetPkgTokenSpaceGuid.PcdPeiSioGpioTable1.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcHookInitSio (
  IN OUT EFI_SIO_TABLE       *SioTable,
  IN OUT EFI_SIO_GPIO_TABLE  *SioGpioTable
  );

/**
 This function offers an interface for OEM code to change the MXM GPU power enable sequence
 and modify the Hybrid Graphics Information data HOB.

 @param[in]         None

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcMxmDgpuPowerSequence (
  VOID
  );

/**
 This function offers an interface to update SI_POLICY_PPI data before the system
 installs SI_POLICY_PPI.

 @param[in, out]   *SiPolicyPpi          On entry, points to SI_POLICY_PPI structure.
                                         On exit, points to updated SI_POLICY_PPI structure.

 @retval            EFI_UNSUPPORTED      Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED    Alter the Configuration Parameter.
 @retval            EFI_SUCCESS          The function performs the same operation as caller.
                                         The caller will skip the specified behavior and assuming
                                         that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdatePeiPolicyInitPostMem (
  IN OUT SI_POLICY_PPI                   *SiPolicyPpi
  );

/**
 This function offers an interface to update SI_PREMEM_POLICY_PPI data before the system
 installs SI_PREMEM_POLICY_PPI.

 @param[in, out]   *SiPreMemPolicyPpi    On entry, points to PCH_POLICY_PPI structure.
                                         On exit, points to updated PCH_POLICY_PPI structure.

 @retval            EFI_UNSUPPORTED      Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED    Alter the Configuration Parameter.
 @retval            EFI_SUCCESS          The function performs the same operation as caller.
                                         The caller will skip the specified behavior and assuming
                                         that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdatePeiPolicyInitPreMem (
  IN OUT SI_PREMEM_POLICY_PPI         *SiPreMemPolicyPpi
  );

/**
  This function offers an interface to config GPIO table at early phase.

 @param[out]        EarlyGpioTable      On entry, points is NULL.
                                        On exit, points to the updated structure.
 @param[out]        EarlyGpioTableCount On entry, points is 0.
                                        On exit, points to the updated value.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcEarlyGpioSettingTable (
  OUT  GPIO_INIT_CONFIG                 **EarlyGpioTable,
  OUT  UINT16                           *EarlyGpioTableCount
  );

/**
 This function offers an interface to dynamically modify GPIO table before memory initialization.

 @param[in, out]    GpioTablePreMem      On entry, points to a structure that specifies the GPIO setting.
                                         On exit, points to the updated structure.
 @param[in, out]    GpioTableCountPreMem On entry, points to a value that the length of GPIO table .
                                         On exit, points to the updated value.

 @retval            EFI_UNSUPPORTED      Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED    Alter the Configuration Parameter.
 @retval            EFI_SUCCESS          The function performs the same operation as caller.
                                         The caller will skip the specified behavior and assuming
                                         that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcModifyGpioSettingTablePreMem (
  IN OUT  GPIO_INIT_CONFIG                 **GpioTablePreMem,
  IN OUT  UINT16                           *GpioTableCountPreMem
  );

/**
 This function will provide OEM CMOS default table.

 @param[in, out]    CmosDefaultTable    The CMOS default table from OEM.

 @retval            EFI_UNSUPPORTED     Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED   Alter the Configuration Parameter.
 @retval            EFI_SUCCESS         The function performs the same operation as caller.
                                        The caller will skip the specified behavior and assuming
                                        that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcGetInitCmosTable (
  OUT CMOS_DEFAULT_DATA       **CmosDefaultTable
  );

/**
  This OemService provides OEM to decide the method of recovery request.
  When DXE loader found that the DXE-core of firmware volume is corrupt, it will force system to restart.
  This service will be called to set the recovery requests before system restart.
  To design the recovery requests according to OEM specification.

  @param  Based on OEM design.

  @retval EFI_UNSUPPORTED      Returns unsupported by default.
  @retval EFI_SUCCESS          The service is customized in the project.
  @retval EFI_MEDIA_CHANGED    The value of IN OUT parameter is changed.
  @retval Others               Depends on customization.
**/
EFI_STATUS
OemSvcChipsetSetRecoveryRequest (
  VOID
  );

/**
  Initial Setup Variable setting depends on project characteristic.

  @param[in, out]  *Buffer            A pointer to SYSTEM_CONFIGURATION.
  @param[in]       SetupVariableExist Setup variable be found in variable storage or not.

  @retval          EFI_UNSUPPORTED    Returns unsupported by default.
  @retval          EFI_SUCCESS        The service is customized in the project.
  @retval          EFI_MEDIA_CHANGED  The value of IN OUT parameter is changed.
  @retval          Others             Depends on customization.
**/
EFI_STATUS
OemSvcChipsetModifySetupVarPlatformInit (
  IN OUT  VOID                                 *Buffer,
  IN      BOOLEAN                              SetupVariableExist
  );

/**
  CPU/Chipset/Platform Initialization when PeiMemoryDiscoveredPpi be installed according to project characteristic.

  @param[in, out]  *BootMode          A pointer to EFI_BOOT_MODE.

  @retval          EFI_UNSUPPORTED    Returns unsupported by default.
  @retval          EFI_SUCCESS        The service is customized in the project.
  @retval          EFI_MEDIA_CHANGED  The value of IN OUT parameter is changed.
  @retval          Others             Depends on customization.
**/
EFI_STATUS
OemSvcChipsetInitMemoryDiscoverCallback (
  IN OUT  EFI_BOOT_MODE                         *BootMode
  );

/**
  CPU/Chipset/Platform Initial depends on project characteristic.

  @param[in]  *Buffer               A pointer to CHIPSET_CONFIGURATION.
  @param[in]  SetupVariableExist    Setup variable be found in variable storage or not.

  @retval     EFI_UNSUPPORTED       Returns unsupported by default.
  @retval     EFI_SUCCESS           The service is customized in the project.
  @retval     EFI_MEDIA_CHANGED     The value of IN OUT parameter is changed.
  @retval     Others                Depends on customization.
**/
EFI_STATUS
OemSvcInitPlatformInit (
  IN  VOID                                 *Buffer,
  IN  BOOLEAN                              SetupVariableExist
  );

EFI_STATUS
OemSvcChipsetBootModeCreateFv (
  IN  EFI_BOOT_MODE                     BootMode
  );

EFI_STATUS
OemSvcChipsetChangeBootMode (
  IN OUT EFI_BOOT_MODE                      *BootMode,
  OUT    BOOLEAN                            *SkipPriorityPolicy
  );

EFI_STATUS
OemSvcChipsetIsBootWithNoChange (
  IN OUT BOOLEAN                               *IsNoChange
  );

EFI_STATUS
OemSvcPlatformInitPreMem (
  VOID
  );

EFI_STATUS
OemSvcChipsetDetectRecoveryRequest (
  IN OUT BOOLEAN                        *IsRecovery
  );

EFI_STATUS
OemSvcChipsetUpdateMemoryTypeInformation (
  IN OUT  EFI_MEMORY_TYPE_INFORMATION  **MemoryTypeInformation,
  IN OUT  UINT32                       *MemoryTableSize
  );

/**
 This function offers an interface to modify FSPS_UPD data before the FSP-S API be called.

 @param[in,out]     FspUpdDataPtr                 A pointer to the UPD data region data strcture address.
                                                  It must convert to FSPS_UPD structure for update FSPS UPD.

                                                  If you set this pointer to NULL, FSP API will use the
                                                  default FSP-S UPD settings in the binary.

 @retval            EFI_UNSUPPORTED               Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED             Alter the Configuration Parameter.
 @retval            EFI_SUCCESS                   The function performs the same operation as caller.
                                                  The caller will skip the specified behavior and assuming
                                                  that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdateFspsUpd (
  IN OUT VOID                                     **FspUpdDataPtr
  );

/**
 This function offers an interface to modify FSPM_UPD data before the FSP-S API be called.

 @param[in,out]     FspUpdDataPtr                 A pointer to the UPD data region data strcture address.
                                                  It must convert to FSPS_UPD structure for update FSPS UPD.

                                                  If you set this pointer to NULL, FSP API will use the
                                                  default FSP-S UPD settings in the binary.

 @retval            EFI_UNSUPPORTED               Returns unsupported by default.
 @retval            EFI_MEDIA_CHANGED             Alter the Configuration Parameter.
 @retval            EFI_SUCCESS                   The function performs the same operation as caller.
                                                  The caller will skip the specified behavior and assuming
                                                  that it has been handled completely by this function.
*/
EFI_STATUS
OemSvcUpdateFspmUpd (
  IN OUT VOID                                     **FspUpdDataPtr
  );

#endif
