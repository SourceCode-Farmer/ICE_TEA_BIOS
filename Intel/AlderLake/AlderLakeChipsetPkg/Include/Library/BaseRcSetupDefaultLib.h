/** @file
  Get RC Setup variable default from VFR binaries.

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BASE_RC_SETUP_DEFAULT_LIB_H_
#define _BASE_RC_SETUP_DEFAULT_LIB_H_

/**
  Extract default SETUP DATA variable data from VFR forms for DXE recovery

  @param[in,out]  SetupData                   A pointer of a pointer to the Setup Data variable data buffer
  @param[in]      SetupDataSize               Data size in bytes of the Setup Data variable
  @param[in]      PoolType                    The type of pool to allocate
*/
VOID
ExtractSetupDataDefault (
  IN OUT UINT8                                  **SetupData,
  IN     UINTN                                  SetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType
  );
  
/**
  Extract default SA Setup variable data from VFR forms for DXE recovery

  @param[in,out]  SaSetupData                   A pointer of a pointer to the SA Setup variable data buffer
  @param[in]      SaSetupDataSize               Data size in bytes of the SA Setup variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractSaSetupDefault (
  IN OUT UINT8                                  **SaSetupData,
  IN     UINTN                                  SaSetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType 
  );
  
/**
  Extract default ME Setup variable data from VFR forms for DXE recovery

  @param[in,out]  MeSetupData                   A pointer of a pointer to the ME Setup variable data buffer
  @param[in]      MeSetupDataSize               Data size in bytes of the ME Setup variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractMeSetupDefault (
  IN OUT UINT8                                  **MeSetupData,
  IN     UINTN                                  MeSetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType 
  );
  
/**
  Extract default CPU Setup variable data from VFR forms for DXE recovery

  @param[in,out]  CpuSetupData                  A pointer of a pointer to the CPU Setup variable data buffer
  @param[in]      CpuSetupDataSize              Data size in bytes of the CPU Setup variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractCpuSetupDefault (
  IN OUT UINT8                                  **CpuSetupData,
  IN     UINTN                                  CpuSetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType 
  );
  
/**
  Extract default PCH Setup variable data from VFR forms for DXE recovery

  @param[in,out]  PchSetupData                  A pointer of a pointer to the PCH Setup variable data buffer
  @param[in]      PchSetupDataSize              Data size in bytes of the PCH Setup variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractPchSetupDefault (
  IN OUT UINT8                                  **PchSetupData,
  IN     UINTN                                  PchSetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType 
  );
  
/**
  Extract default SI Setup variable data from VFR forms for DXE recovery

  @param[in,out]  MeSetupStorageData            A pointer of a pointer to the ME Setup storage variable data buffer
  @param[in]      MeSetupStorageDataSize        Data size in bytes of the ME Setup storage variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractSiSetupDefault (
  IN OUT UINT8                                  **SiSetupData,
  IN     UINTN                                  SiSetupDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType 
  );
  
/**
  Extract default ME Setup storage variable data from VFR forms for DXE recovery

  @param[in,out]  MeSetupStorageData            A pointer of a pointer to the ME Setup storage variable data buffer
  @param[in]      MeSetupStorageDataSize        Data size in bytes of the ME Setup storage variable
  @param[in]      PoolType                      The type of pool to allocate
*/
VOID
ExtractMeSetupStorageDefault (
  IN OUT UINT8                                  **MeSetupStorageData,
  IN     UINTN                                  MeSetupStorageDataSize,
  IN     EFI_MEMORY_TYPE                        PoolType 
  );

#endif
