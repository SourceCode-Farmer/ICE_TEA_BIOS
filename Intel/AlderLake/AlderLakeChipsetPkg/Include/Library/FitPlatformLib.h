/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _FIT_PLATFORM_LIB_H_
#define _FIT_PLATFORM_LIB_H_

#define MEM_EQU_4GB                                               ( 0x100000000 )

#define R_FITP                                                    ( MEM_EQU_4GB - 0x40 )
#define S_FITP                                                    ( 4 )

#define V_FIT_ENTRY_TYPE_FIT_HEADER                               ( 0x00 )
#define V_FIT_ENTRY_TYPE_MICROCODE_UPDATE                         ( 0x01 )
#define V_FIT_ENTRY_TYPE_STARTUP_AC_MODULE                        ( 0x02 )
#define V_FIT_ENTRY_TYPE_KEY_MANIFEST                             ( 0x0B )
#define V_FIT_ENTRY_TYPE_BOOT_POLICY_MANIFEST                     ( 0x0C )
#define V_FIT_ENTRY_TYPE_BIOS_DATA_AREA                           ( 0x0D )
#define V_FIT_ENTRY_TYPE_JMP_$_DEBUG_POLICY                       ( 0x2F )
#define V_FIT_ENTRY_TYPE_UNUSED                                   ( 0x7F )

#define V_FIT_ENTRY_FIT_HEADER_ADDRESS                            ( "_FIT_   " )
#define S_FIT_ENTRY_FIT_HEADER_ADDRESS                            ( 8 )
#define V_FIT_ENTRY_FIT_HEADER_SIZE_GRANULARITY                   ( 0x10 )
#define V_FIT_ENTRY_FIT_HEADER_RESERVED                           ( 0 )
#define V_FIT_ENTRY_FIT_HEADER_VERSION                            ( 0x0100 )
#define V_FIT_ENTRY_FIT_HEADER_TYPE                               ( V_FIT_ENTRY_TYPE_FIT_HEADER )

#define V_FIT_ENTRY_MICROCODE_UPDATE_ADDRESS_EMPTY                ( 0xFFFFFFFF )
#define B_FIT_ENTRY_MICROCODE_UPDATE_ADDRESS_EMPTY                ( 0xFFFFFFFF )
#define V_FIT_ENTRY_MICROCODE_UPDATE_SIZE                         ( 0 )
#define V_FIT_ENTRY_MICROCODE_UPDATE_RESERVED                     ( 0 )
#define V_FIT_ENTRY_MICROCODE_UPDATE_VERSION                      ( 0x0100 )
#define V_FIT_ENTRY_MICROCODE_UPDATE_TYPE                         ( V_FIT_ENTRY_TYPE_MICROCODE_UPDATE )
#define V_FIT_ENTRY_MICROCODE_UPDATE_C_V                          ( 0 )

#define V_FIT_ENTRY_STARTUP_AC_MODULE_SIZE                        ( 0 )
#define V_FIT_ENTRY_STARTUP_AC_MODULE_RESERVED                    ( 0 )
#define V_FIT_ENTRY_STARTUP_AC_MODULE_VERSION                     ( 0x0100 )
#define V_FIT_ENTRY_STARTUP_AC_MODULE_TYPE                        ( V_FIT_ENTRY_TYPE_STARTUP_AC_MODULE )
#define V_FIT_ENTRY_STARTUP_AC_MODULE_C_V                         ( 0 )

//#define V_FIT_ENTRY_KEY_MANIFEST_SIZE                             ( sizeof ( KEY_MANIFEST ) )
#define V_FIT_ENTRY_KEY_MANIFEST_RESERVED                         ( 0 )
#define V_FIT_ENTRY_KEY_MANIFEST_VERSION                          ( 0x0100 )
#define V_FIT_ENTRY_KEY_MANIFEST_TYPE                             ( V_FIT_ENTRY_TYPE_KEY_MANIFEST )
#define V_FIT_ENTRY_KEY_MANIFEST_C_V                              ( 0 )
#define V_FIT_ENTRY_KEY_MANIFEST_CHKSUM                           ( 0 )

#define V_FIT_ENTRY_BOOT_POLICY_MANIFEST_RESERVED                 ( 0 )
#define V_FIT_ENTRY_BOOT_POLICY_MANIFEST_VERSION                  ( 0x0100 )
#define V_FIT_ENTRY_BOOT_POLICY_MANIFEST_TYPE                     ( V_FIT_ENTRY_TYPE_BOOT_POLICY_MANIFEST )
#define V_FIT_ENTRY_BOOT_POLICY_MANIFEST_C_V                      ( 0 )
#define V_FIT_ENTRY_BOOT_POLICY_MANIFEST_CHKSUM                   ( 0 )

#define V_FIT_ENTRY_BIOS_DATA_AREA_RESERVED                       ( 0 )
#define V_FIT_ENTRY_BIOS_DATA_AREA_VERSION                        ( 0x0100 )
#define V_FIT_ENTRY_BIOS_DATA_AREA_TYPE                           ( V_FIT_ENTRY_TYPE_BIOS_DATA_AREA )
#define V_FIT_ENTRY_BIOS_DATA_AREA_C_V                            ( 0 )
#define V_FIT_ENTRY_BIOS_DATA_AREA_CHKSUM                         ( 0 )

#define V_FIT_ENTRY_JMP_$_DEBUG_POLICY_SIZE                       ( 0 )
#define V_FIT_ENTRY_JMP_$_DEBUG_POLICY_RESERVED                   ( 0 )
#define V_FIT_ENTRY_JMP_$_DEBUG_POLICY_VERSION_INDEXED_IO         ( 0 )
#define V_FIT_ENTRY_JMP_$_DEBUG_POLICY_VERSION_FLAT_MEMORY        ( 1 )
#define V_FIT_ENTRY_JMP_$_DEBUG_POLICY_TYPE                       ( V_FIT_ENTRY_TYPE_JMP_$_DEBUG_POLICY )
#define V_FIT_ENTRY_JMP_$_DEBUG_POLICY_C_V                        ( 0 )

#define V_FIT_ENTRY_UNUSED_TYPE                                   ( V_FIT_ENTRY_TYPE_UNUSED )

#pragma pack ( 1 )
typedef struct _FIT_ENTRY {
  UINT64        Address;
  UINT8         Size[3];
  UINT8         Reserved;
  UINT16        Version;
  UINT8         Type : 7;
  UINT8         C_V  : 1;
  UINT8         Chksum;
} FIT_ENTRY;
#pragma pack ()

EFI_STATUS
EFIAPI
FITPlatformLibGetFITP (
  IN OUT  UINTN        *Address
  );

EFI_STATUS
EFIAPI
FITPlatformLibValidateFITHeaderAddress (
  IN      FIT_ENTRY        *FIT
  );

EFI_STATUS
EFIAPI
FITPlatformLibValidateFITChecksum (
  IN      FIT_ENTRY        *FIT
  );

EFI_STATUS
EFIAPI
FITPlatformLibValidateFIT (
  IN      FIT_ENTRY        *FIT
  );

EFI_STATUS
EFIAPI
FITPlatformLibBIOSImageAddressTranslation (
  IN      VOID         *Image,
  IN      UINTN        ImageLength,
  IN OUT  UINTN        *Address
  );

EFI_STATUS
EFIAPI
FITPlatformLibGetFITEx (
  IN      VOID             *Image,
  IN      UINTN            ImageLength,
     OUT  FIT_ENTRY        **FIT
  );

EFI_STATUS
EFIAPI
FITPlatformLibGetFIT (
     OUT  FIT_ENTRY        **FIT
  );

EFI_STATUS
EFIAPI
FITPlatformLibGetNextEntry (
  IN      FIT_ENTRY        *FIT,
  IN      UINT8            EntryType,
  IN OUT  FIT_ENTRY        **Entry
  );

EFI_STATUS
EFIAPI
FITPlatformLibGetFirstEntry (
  IN      FIT_ENTRY        *FIT,
  IN      UINT8            EntryType,
     OUT  FIT_ENTRY        **Entry
  );

#endif
