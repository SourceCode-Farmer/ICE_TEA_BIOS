/** @file

;******************************************************************************
;* Copyright (c) 2014, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _OEM_OPTION_ROM_TYPE_H_
#define _OEM_OPTION_ROM_TYPE_H_

typedef enum {
  PCI_OPROM = 0x01,
  SYSTEM_ROM,
  SYSTEM_OEM_INT_ROM,
  SERVICE_ROM,
  BIS_LOADER,
  TPM_ROM,
  AMT_ROM,
  MAX_NUM = 0xFF
} OPROM_TYPE;

#endif
