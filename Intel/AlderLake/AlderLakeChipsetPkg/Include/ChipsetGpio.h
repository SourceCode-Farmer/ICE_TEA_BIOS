/** @file
  This header file is provided to Chipset to define GPIO function.

;******************************************************************************
;* Copyright (c) 2014 - 2015, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CHIPSET_GPIO_DEFINE_H_
#define _CHIPSET_GPIO_DEFINE_H_

#pragma pack(1)
typedef struct {
  UINT32  GpioPad;
  UINT8   PadMode;
  UINT8   HostSoftPadOwn;
  UINT8   Direction;
  UINT8   OutputState;
  UINT8   InterruptConfig;
  UINT8   PowerConfig;
  UINT8   ElectricalConfig;
  UINT8   LockConfig;
  UINT8   OtherSettings;
  UINT8   Rsvdbits;
} PCD_GPIO_CONFIG;
#pragma pack()

#endif
