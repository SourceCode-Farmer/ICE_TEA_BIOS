/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

/*--
Module Name:

  ChipsetCmos.h

Abstract:


--*/

#ifndef _CHIPSET_CMOS_H_
#define _CHIPSET_CMOS_H_

#define R_XCMOS_INDEX                     0x72
#define R_XCMOS_DATA                      0x73

//
// PlatformConfigFlag
//
#define B_CONFIG_IGD_PRESENT_FLAG         BIT0

//
// PlatformSettingFlag
//
#define B_SETTING_MEM_REFRESH_FLAG        BIT0

//
// VmxSmxFlag
//
#define B_SMX_SETUP_FLAG                  BIT0
#define B_SMX_CPU_FLAG                    BIT1
#define B_SMX_CHIPSET_FLAG                BIT2
#define B_SMX_FLAG                        ( B_SMX_SETUP_FLAG | B_SMX_CPU_FLAG | B_SMX_CHIPSET_FLAG )
#define B_VMX_SETUP_FLAG                  BIT4
#define B_VMX_CPU_FLAG                    BIT5
#define B_VMX_FLAG                        ( B_VMX_SETUP_FLAG | B_VMX_CPU_FLAG )

//
// TxtMemoryScrubFlag
//
#define B_MEMORY_SCRUB                    BIT0

#define RC_BANK_OFFSET                    0x80

//
// OverclockingFlag
//
#define B_OC_BCLK_WDT_FLAG                BIT0
#define B_OC_BCLK_S3_FLAG                 BIT7

//
// EcGetLidStateFlag
//
#define PLATFORM_TYPE_TRAD                BIT0
#define PLATFORM_TYPE_NOT_TRAD            BIT1
#define PLATFORM_TYPE_MASK                (BIT1 | BIT0)
#define PLATFORM_FLAVOR_DESKTOP           BIT2
#define PLATFORM_FLAVOR_NOT_DESKTOP       BIT3
#define PLATFORM_FLAVOR_MASK              (BIT3 | BIT2)
#define EC_PRESENT_FLAG                   BIT4
#define EC_NOT_PRESENT_FLAG               BIT5
#define EC_PRESENT_MASK                   (BIT5 | BIT4)

//
//MEFlashReset
//
#define V_ME_GLOBAL_RESET                    0xF1
#define V_ME_NO_RESET                        0x00
#define V_ME_NEED_BIOS_SYNC                  0xE1
#define NV_NOT_FOUND                         0xF0
#define NV_AVAILABLE                         0x00
#define B_XTU_WATCH_DOG_TRIGGER           BIT0    // for tuning AP
#define B_XTU_WATCH_DOG_STATUS            BIT1    // for tuning AP
#define B_XTU_WATCH_DOG                   ( B_XTU_WATCH_DOG_TRIGGER | B_XTU_WATCH_DOG_STATUS )
#define SET_WATCH_DOG_STATUS(cmos)        ( (cmos) | B_XTU_WATCH_DOG_TRIGGER )
#define UPDATE_WATCH_DOG_STATUS(cmos)     ( ((cmos) & ~B_XTU_WATCH_DOG) | (((cmos) & B_XTU_WATCH_DOG_TRIGGER) << 1) )
#define B_XTU_WATCH_DOG_FLAG_PEI_1        BIT2
#define B_XTU_WATCH_DOG_FLAG_PEI_2        BIT3
#define B_XTU_WATCH_DOG_FLAG_XMP          BIT4
#define B_XTU_FLAG_FAIL                   BIT5
#define B_XTU_FLAG_POSTING                BIT6
#define B_XTU_FLAG_SAFE_RECOVERY          BIT7  // if clear (clear cmos), XTU will set disable!!!
#define B_XTU_XMP_CHANGE                  BIT0
#define RECOVERY_VALUE                  0xCC
#define ENABLE_OPTIMUS_HDA_VALUE        0x01

//
// add from RC CmosMap.h
//
#define FIT_REC_TXT_POLICY_TYPE_A 0x2A

//[-start-210527-IB16740143-add]//
//
// Redefine USB4 platform settings
// To avoid CMOS_USB4_CM_MODE_REG of CmosMap.h conflicted with the settings of OEM Reserve in ChipsetCmos.h
//
#define  CMOS_USB4_CM_MODE_REG_CHP    0x37        // 0x37        : USB4 CMOS platform setting for Chipset
//[-end-210527-IB16740143-add]//

typedef enum {
  RTC_ADDRESS_CENTURY               = 0x32
                                                  // 0x45        : Chipset Reserved
                                                  // 0x48 ~ 0x4F : OEM Reserve
                                                  // 0x6C ~ 0x7F : OEM Reserve
} OPTION_CMOS_TABLE1;
//} OPTION_COMS_TABLE1;

typedef enum {
                                                  // 0x00 ~ 0x0F : OEM Reserve
  CMOS_CPU_RATIO_OFFSET             = 0x10,       // 0x10        : Used by platform code CpuSetup.c
  CMOS_TXT_REG                      = 0x11,       // 0x11        : Used by platform code CpuSetup.c
  CMOS_OC_SEND_BCLK_RAMP_MSG        = 0x12,       // 0x12        : Used by IccSetup.c, CpuPolicyPei.c
  CMOS_TBTHR_PRESENT_ON_RESUME_1    = 0x13,
  CMOS_ICH_PORT80_OFFSET            = 0x17,       // 0x17        : Used by PlatformSetup.c
  Skylake_Crisis_Reserve_mem0       = 0x18,       // 0x18 ~ 0x21 : Save normal post menory reserved value for Crisis boot
  Skylake_Crisis_Reserve_mem1       = 0x19,       //
  Skylake_Crisis_Reserve_mem2       = 0x20,       //
  Skylake_Crisis_Reserve_mem3       = 0x21,       //
  CMOS_OPTIMUS_AUDIO_CODEC_CTL      = 0x22,       // 0x22        : 0 -> Platform should disable the GPU audio codec during cold start and resume from S3/S4. (default)
                                                  //               1 -> Platform should enable the GPU audio codec during cold start and resume from S3/S4.
                                                  // 0x23 ~ 0x2C : Chipset Reserve
  MEFlashReset                      = 0x2D,       // 0x2D        : ME Flash Reset
                                                  // 0x2E ~ 0x30 : Chipset Reserve
  PlatformConfigFlag                = 0x32,       // 0x32        : Flag of Platform Configuration
  PlatformSettingFlag               = 0x33,       // 0x33        : Flag of Platform Settings
                                                  // 0x34 ~ 0x35 : Chipset Reserve
                                                  // 0x37        : Chipset Reserve
                                                  // 0x38 ~ 0x3F : Chipset Reserved for RTC Lock Area
                                                  // 0x40 ~ 0x57 : OEM Reserve
  S5LongRunTestFlag                 = 0x34,       // 0x34        : Flag of S5 long run test
  EcGetLidStateFlag                 = 0x35,       // 0x35        : Get PcdPlatformType, PcdPlatformFlavor, PcdEcPresent data and set to EcGetLidStateFlag
  ChipsetRecoveryFlag               = 0x58,       // 0x58        : On Skylake Y/U CRB When this value = 0xCC
                                                  //             : BIOS will do crisis.

  // bit0 : set when watchdog trigger, this bit will "<< 1" in next POST time (AcpiWatchDog.c)
  // bit1 : watchdog status, 1=latest reboot/shoudown by watchdog, 0=normal
  XtuWdtStatus                      = 0x59,       // 0x59        : XTU check watch dog status
  IccWatchdogFlag                   = 0x5A,       // 0x5A        : ICC set profile WDT flag
  OverclockingFlag                  = 0x5B,       // 0x5B        : Overclocking flag
  SetupNVFlag                       = 0x5C,       // 0x5C        : NV varible availible flag
  VmxSmxFlag                        = 0x5D,       // 0x5D        : Flag of VT / TXT
  MrcOemDebug1                      = 0x5E,       // 0x5E        : MrcOemDebug1
  MrcOemDebug2                      = 0x5F        // 0x5F        : MrcOemDebug1
                                                  // 0x60 ~ 0x7F : OEM Reserve

} OPTION_CMOS_TABLE2;
//} OPTION_COMS_TABLE2;

#endif
