/** @file

;******************************************************************************
;* Copyright (c) 2017- 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _BOOT_GUARD_RECOVERY_HOOK_H_
#define _BOOT_GUARD_RECOVERY_HOOK_H_

#include <Uefi.h>
#include <Library/BootGuardPlatformLib.h>
#include <Ppi/DeviceRecoveryModule.h>
#include <Library/PeiServicesLib.h>
#include <Library/BaseMemoryLib.h>
//#ifdef SECURE_FLASH_SUPPORT
//#include <Ppi/EmuPei.h>
#include <Ppi/FirmwareAuthentication.h>
//#endif

//#ifdef SECURE_FLASH_SUPPORT
#define SECURE_FLASH_SIGNATURE_SIZE                  ( SIGNATURE_SIZE )
#define ISFLASH_IMAGE_SIGNATURE                      ( ISFLASH_BIOS_IMAGE_TAG )
#define ISFLASH_IMAGE_SIGNATURE_SIZE                 ( ISFLASH_TAG_SIZE )
//#endif

#define R_DESCRIPTOR_FDBAR_FLVALSIG                  ( 0x10 )
#define V_DESCRIPTOR_FDBAR_FLVALSIG                  ( 0x0FF0A55A )
#define R_DESCRIPTOR_FDBAR_FLMAP0                    ( 0x14 )
#define B_DESCRIPTOR_FDBAR_FLMAP0_FRBA               ( 0x00FF0000 )
#define N_DESCRIPTOR_FDBAR_FLMAP0_FRBA               ( 16 )
#define R_DESCRIPTOR_FRBA_FLREG1                     ( 0x04 )
#define B_DESCRIPTOR_FRBA_FLREG1_REGION_BASE         ( 0x00007FFF )
#define N_DESCRIPTOR_FRBA_FLREG1_REGION_BASE         ( 0 )
#define B_DESCRIPTOR_FRBA_FLREG1_REGION_LIMIT        ( 0x7FFF0000 )
#define N_DESCRIPTOR_FRBA_FLREG1_REGION_LIMIT        ( 16 )
#define R_DESCRIPTOR_FRBA_FLASH_REGION_BIOS          ( R_DESCRIPTOR_FRBA_FLREG1 )

typedef struct _DEVICE_RECOVERY_MODULE_HOOK_LIST DEVICE_RECOVERY_MODULE_HOOK_LIST;

struct _DEVICE_RECOVERY_MODULE_HOOK_LIST {
  EFI_PEI_DEVICE_RECOVERY_MODULE_PPI          *DeviceRecoveryModule;
  EFI_PEI_DEVICE_LOAD_RECOVERY_CAPSULE        LoadRecoveryCapsule;
  DEVICE_RECOVERY_MODULE_HOOK_LIST            *NextPtr;
  };

EFI_STATUS
BootGuardRecoveryHookEntry (
  IN EFI_PEI_FILE_HANDLE                   FileHandle,
  IN CONST EFI_PEI_SERVICES                **PeiServices
  );

EFI_STATUS
BootGuardRecoveryHookCallback (
  IN      EFI_PEI_SERVICES                 **PeiServices,
  IN      EFI_PEI_NOTIFY_DESCRIPTOR        *NotifyDescriptor,
  IN      VOID                             *Ppi
  );

EFI_STATUS
EFIAPI
BootGuardRecoveryHook (
  IN OUT  EFI_PEI_SERVICES                            **PeiServices,
  IN      EFI_PEI_DEVICE_RECOVERY_MODULE_PPI          *This,
  IN      UINTN                                       CapsuleInstance,
     OUT  VOID                                        *Buffer
  );

EFI_STATUS
ExtractBIOSFromCapsule (
  IN OUT  UINT8        **Image,
  IN OUT  UINTN        *ImageLength
  );

#endif
