/** @file
  FSP Wrapper Report FV implementation.

@copyright
  Copyright (c) 2015, Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains 'Sample Driver' and is licensed as such
  under the terms of your license agreement with Intel or your
  vendor. This file may not be modified, except as allowed by
  additional terms of your license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/PcdLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeimEntryPoint.h>

EFI_STATUS
EFIAPI
InsydeReportFvPeiEntry (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  PeiServicesInstallFvInfoPpi (
    NULL,
    (VOID *) PcdGet32 (PcdFlashFvRecoveryBase),
    PcdGet32 (PcdFlashFvRecoverySize),
    NULL,
    NULL
    );

  PeiServicesInstallFvInfoPpi (
    NULL,
    (VOID *) PcdGet32 (PcdFlashFvRecovery2Base),
    PcdGet32 (PcdFlashFvRecovery2Size),
    NULL,
    NULL
    );

  return EFI_SUCCESS;
}
