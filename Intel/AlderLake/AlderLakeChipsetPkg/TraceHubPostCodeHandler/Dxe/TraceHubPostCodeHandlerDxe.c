/** @file
  The instance of trace hub post code handler

;******************************************************************************
;* Copyright (c) 2017 - 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Guid/H2OCp.h>
#include <Library/DebugLib.h>
#include <Library/PrintLib.h>
#include <Library/TraceHubHookLib.h>
#include <Library/TraceHubDebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/H2OCpLib.h>


/**
 Write post code value to trace hub.

 @param[in] Event                      A pointer to the Event that triggered the callback.
 @param[in] Handle                     Checkpoint handle.
**/
VOID
EFIAPI
PostCodeCallback (
  IN EFI_EVENT                         Event,
  IN H2O_CP_HANDLE                     Handle
  )
{
  EFI_STATUS                           Status;
  H2O_BASE_CP_POST_CODE_DATA           *PostCodeData;
  CHAR8                                AsciiStrBuffer[50];
  UINTN                                CharCount;

  Status = H2OCpLookup (Handle, (VOID **) &PostCodeData, NULL);
  if (EFI_ERROR (Status)) {
    DEBUG_CP ((DEBUG_ERROR, "Checkpoint Data Not Found: %x (%r)\n", Handle, Status));
    DEBUG_CP ((DEBUG_ERROR, "   %a\n", __FUNCTION__));
    return;
  }

  CharCount = AsciiSPrint (
                AsciiStrBuffer,
                sizeof (AsciiStrBuffer),
                "TraceHub : POSTCODE=<%02x>\n\r",
                PostCodeData->PostCodeValue
                );
  TraceHubDebugWrite (SeverityError, AsciiStrBuffer, CharCount);
}

/**
 Register post code checkpoint.

 @param[in] FfsHeader                  Header for file of Firmware File System
 @param[in] PeiServices                The PEI core services table.

 @retval EFI_SUCCESS                   Module initialized successfully
 @retval Others                        Fail to register checkpoint.
**/
EFI_STATUS
EFIAPI
TraceHubPostCodeHandlerDxeEntry (
  IN  EFI_HANDLE                       ImageHandle,
  IN  EFI_SYSTEM_TABLE                 *SystemTable
  )
{
  if (FeaturePcdGet (PcdH2OBaseCpPostCodeSupported)) {
    UINTN                              Status;
    H2O_CP_HANDLE                      CpHandle;

    Status = H2OCpRegisterHandler (
               &gH2OBaseCpPostCodeGuid,
               PostCodeCallback,
               H2O_CP_MEDIUM,
               &CpHandle
               );
    if (EFI_ERROR (Status)) {
      DEBUG_CP ((DEBUG_ERROR, "Checkpoint Register Fail: %g (%r)\n", &gH2OBaseCpPostCodeGuid, Status));
      return Status;
    }
    DEBUG_CP ((DEBUG_INFO, "Checkpoint Registered: %g (%r)\n", &gH2OBaseCpPostCodeGuid, Status));
  }

  return EFI_SUCCESS;
}

