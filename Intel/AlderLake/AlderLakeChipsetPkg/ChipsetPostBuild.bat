@REM
@REM Chipset PostBuild batch file
@REM
@REM ******************************************************************************
@REM * Copyright (c) 2014 - 2018, Insyde Software Corp. All Rights Reserved.
@REM *
@REM * You may not reproduce, distribute, publish, display, perform, modify, adapt,
@REM * transmit, broadcast, present, recite, release, license or otherwise exploit
@REM * any part of this publication in any form, by any means, without the prior
@REM * written permission of Insyde Software Corporation.
@REM *
@REM ******************************************************************************

@REM Add Chispet specific post-build process here

set FSP_PKG_NAME=AlderLakeFspPkg
set PLATFORM_NAME=AlderLake

for /f "tokens=3" %%a in ('find "TARGET " %WORKSPACE%\Conf\target.txt') do set TARGET=%%a
set BUILD_BOARD_PATH=%WORKSPACE%\Build\%PROJECT_PKG%\%TARGET%_%TOOL_CHAIN%
set BUILD_FSP_PATH=%WORKSPACE%\Build\%FSP_PKG_NAME%\%TARGET%_%TOOL_CHAIN%
set PYTHON_SCRIPT_DIR=%WORKSPACE%\Intel\%PLATFORM_NAME%\%CHIPSET_PKG%\Tools\Source\Python\PcdCompare

echo ##########################################################
echo ## Launch Pcd value comparison process for Bios and Fsp ##
echo ##########################################################
if exist %BUILD_BOARD_PATH% (
  if exist %BUILD_FSP_PATH% (
    if not exist %PYTHON_SCRIPT_DIR%\BiosPcd\IA32 md %PYTHON_SCRIPT_DIR%\BiosPcd\IA32
    if not exist %PYTHON_SCRIPT_DIR%\BiosPcd\X64 md %PYTHON_SCRIPT_DIR%\BiosPcd\X64
    if not exist %PYTHON_SCRIPT_DIR%\FpsPcd\IA32 md %PYTHON_SCRIPT_DIR%\FpsPcd\IA32
    if not exist %PYTHON_SCRIPT_DIR%\FpsPcd\X64 md %PYTHON_SCRIPT_DIR%\FpsPcd\X64
    copy %BUILD_BOARD_PATH%\IA32\PcdList.txt %PYTHON_SCRIPT_DIR%\BiosPcd\IA32\PcdList.txt 
    copy %BUILD_BOARD_PATH%\X64\PcdList.txt %PYTHON_SCRIPT_DIR%\BiosPcd\X64\PcdList.txt
    copy %BUILD_FSP_PATH%\IA32\PcdList.txt %PYTHON_SCRIPT_DIR%\FpsPcd\IA32\PcdList.txt 
    copy %BUILD_FSP_PATH%\X64\PcdList.txt %PYTHON_SCRIPT_DIR%\FpsPcd\X64\PcdList.txt 
    python %PYTHON_SCRIPT_DIR%\PcdCompareScript.py
    rd /s /q %PYTHON_SCRIPT_DIR%\BiosPcd && rd /s /q %PYTHON_SCRIPT_DIR%\FpsPcd
    if exist %PYTHON_SCRIPT_DIR%\PcdCompareResult_IA32.txt (
      move %PYTHON_SCRIPT_DIR%\PcdCompareResult_IA32.txt %WORKSPACE%\Build\%PROJECT_PKG%\PcdCompareResult_IA32.txt
    ) 
    if exist %PYTHON_SCRIPT_DIR%\PcdCompareResult_X64.txt (
      move %PYTHON_SCRIPT_DIR%\PcdCompareResult_X64.txt %WORKSPACE%\Build\%PROJECT_PKG%\PcdCompareResult_X64.txt
    ) 
  )
  echo #######################################
  echo ## Pcd value comparison process done ##
  echo #######################################
)

