/** @file
  Install Cache PPI

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publicat
;******************************************************************************
*/
#include "CacheInitPei.h"

EFI_STATUS
CacheInitEntryPoint (
  IN EFI_PEI_FILE_HANDLE        FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS                                Status;

  DEBUG((DEBUG_INFO, "CacheInitEntryPoint Start!\n"));

  ///
  /// Install Cache PPI
  ///
  Status = CacheInitPpiInit ();
  ASSERT_EFI_ERROR (Status);  
  DEBUG((DEBUG_INFO, "CacheInitEntryPoint Exit!\n"));
  return EFI_SUCCESS;
}