/** @file
  Install Cache PPI

;******************************************************************************
;* Copyright (c) 2016, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _CPU_INIT_PEI_H_
#define _CPU_INIT_PEI_H_

#include <Ppi/Cache.h>
#include <CpuRegs.h>
#include <Library/DebugLib.h>

typedef struct {
  UINT64  MsrValue;
  BOOLEAN Changed;
} MTRR_VALUE;

typedef struct {
  PEI_CACHE_PPI          Ppi;
  EFI_PEI_PPI_DESCRIPTOR PpiDesc;
  MTRR_VALUE             FixedMtrrValue[V_FIXED_MTRR_NUMBER];
  MTRR_VALUE             VariableMtrrValue[V_MAXIMUM_VARIABLE_MTRR_NUMBER * 2];
  BOOLEAN                FixedMtrrChanged;
  BOOLEAN                VariableMtrrChanged;
  BOOLEAN                NemDisabledDone;
} CACHE_PPI_INSTANCE;

#define PEI_CACHE_PPI_INSTANCE_FROM_THIS(a) ((CACHE_PPI_INSTANCE *) ((CHAR8 *) (a) - (CHAR8 *) &(((CACHE_PPI_INSTANCE *) 0)->Ppi)))

/**
  Install CacheInitPpi

  @retval EFI_OUT_OF_RESOURCES - failed to allocate required pool
**/
EFI_STATUS
CacheInitPpiInit (
  VOID
  );

#endif

