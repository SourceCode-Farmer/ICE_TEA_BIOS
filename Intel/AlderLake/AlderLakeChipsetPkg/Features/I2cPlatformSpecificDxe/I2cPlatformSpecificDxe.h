/** @file
  For I2C I2cPlatform Specific Dxe Driver

;******************************************************************************
;* Copyright (c) 2019 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _I2C_PLATFORM_SPECIFIC_DXE_H_
#define _I2C_PLATFORM_SPECIFIC_DXE_H_

#include <IndustryStandard/Pci.h>
#include <Library/BaseLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DxeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/PciSegmentLib.h>
#include <Protocol/I2cPlatformSpecific.h>
#include "../../I2cMaster/Dxe/I2cMasterDxe.h"
////// The following libs depend on the platform.
#include <Library/PchInfoLib.h>
#include <Register/PchRegs.h>
#include <Register/SerialIoRegs.h>
#include <Register/SerialIoRegsVer2.h>
#include <Register/SerialIoRegsVer4.h>
#include <Register/GpioRegs.h>
#include <Register/PchRegsLpc.h>
#include <Library/GpioPrivateLib.h>
#include <Library/SerialIoPrivateLib.h>
//////
VOID
ChipInit (
  IN  I2C_BUS_INSTANCE   *I2cPort    
  );

UINTN
GetDevMode (
  IN I2C_BUS_INSTANCE      *I2cBusInstance
  );

VOID
SwitchDevMode (
  IN I2C_BUS_INSTANCE      *I2cBusInstance,
  IN UINT32                DeviceMode
  );

BOOLEAN
GpioEventArrived(
  IN  I2C_BUS_INSTANCE   *I2cBusInstance,
  IN  UINT8              IntGpioController,  
  IN  UINT8              PadNumber,
  IN  UINT8              ActiveLevel
  );
//
// How to get value without calculate HCNT / LCNT:
// 1. Boot to OS and install I2C driver if controller avaliable.
// 2. Find I2C MMIO base.
// 3. Each chipset platform may has certain IC_SDA_HOLD Register offset,
//    look it up from spec and set it to SHTL
// 4. Base on I2C device ID to decide which I2C setting of mI2cBusInfo[].
// 5. If you want to modify I2C_SCL and I2C_SDA value, you can set OemHookI2cSclSdaEnable 
//    as TRUE to run OemHookI2cSclSda() hook function or modify mI2cBusInfo[] directly.
//     IC_SS_SCL_HCNT - I2C_MMIO[0x14]
//     IC_SS_SCL_LCNT - I2C_MMIO[0x18]
//     IC_SDA_HOLD - I2C_MMIO[SHTL]
//
//     IC_FS_SCL_HCNT - I2C_MMIO[0x1C]
//     IC_FS_SCL_LCNT - I2C_MMIO[0x20]
//     IC_SDA_HOLD - I2C_MMIO[SHTL]
//
//     IC_HS_SCL_HCNT - I2C_MMIO[0x24]
//     IC_HS_SCL_LCNT - I2C_MMIO[0x28]
//     IC_SDA_HOLD - I2C_MMIO[SHTL]
//
I2C_INFO mI2cBusInfoPchLP[] = {
//  I2cPciDID                                  CRG      SHTL  QkR SSH      SSL     S       FSH     FSL     S       HSH     HSL     S        Init      GetDeviceMode  SwitcDevhMode  GpioArrived
  {  V_VER2_PCH_LP_SERIAL_IO_CFG_I2C0_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_LP_SERIAL_IO_CFG_I2C1_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_LP_SERIAL_IO_CFG_I2C2_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_LP_SERIAL_IO_CFG_I2C3_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_LP_SERIAL_IO_CFG_I2C4_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_LP_SERIAL_IO_CFG_I2C5_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_LP_SERIAL_IO_CFG_I2C6_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_LP_SERIAL_IO_CFG_I2C7_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  { 0xFFFF, 0x0, 0x0, { {0x0, 0x0, 0x0}, {0x0, 0x0, 0x0}, {0x0, 0x0, 0x0}, } , NULL, NULL, NULL, NULL}
};

I2C_INFO mI2cBusInfoPchH[] = {
//  I2cPciDID                                  CRG      SHTL  QkR SSH      SSL     S       FSH     FSL     S       HSH     HSL     S        Init      GetDeviceMode  SwitcDevhMode  GpioArrived
  {  V_VER2_PCH_H_SERIAL_IO_CFG_I2C0_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_H_SERIAL_IO_CFG_I2C0_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_H_SERIAL_IO_CFG_I2C2_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_H_SERIAL_IO_CFG_I2C3_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_H_SERIAL_IO_CFG_I2C4_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_H_SERIAL_IO_CFG_I2C5_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_H_SERIAL_IO_CFG_I2C6_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_H_SERIAL_IO_CFG_I2C7_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  { 0xFFFF, 0x0, 0x0, { {0x0, 0x0, 0x0}, {0x0, 0x0, 0x0}, {0x0, 0x0, 0x0}, } , NULL, NULL, NULL, NULL}
};

I2C_INFO mI2cBusInfoPchP[] = {
//  I2cPciDID                                  CRG      SHTL  QkR SSH      SSL     S       FSH     FSL     S       HSH     HSL     S        Init      GetDeviceMode  SwitcDevhMode  GpioArrived
  {  V_VER2_PCH_P_SERIAL_IO_CFG_I2C0_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
//[-start-20211021-OWENWU0020-modify]//
#ifdef S570_SUPPORT
  {  V_VER2_PCH_P_SERIAL_IO_CFG_I2C1_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0055, 0x0090, 0x50}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},  
#else
  {  V_VER2_PCH_P_SERIAL_IO_CFG_I2C1_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},  
#endif
//[-end-20211021-OWENWU0020-modify]//
  {  V_VER2_PCH_P_SERIAL_IO_CFG_I2C2_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_P_SERIAL_IO_CFG_I2C3_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_P_SERIAL_IO_CFG_I2C4_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_P_SERIAL_IO_CFG_I2C5_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_P_SERIAL_IO_CFG_I2C6_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_P_SERIAL_IO_CFG_I2C7_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  { 0xFFFF, 0x0, 0x0, { {0x0, 0x0, 0x0}, {0x0, 0x0, 0x0}, {0x0, 0x0, 0x0}, } , NULL, NULL, NULL, NULL}
};

I2C_INFO mI2cBusInfoPchN[] = {
//  I2cPciDID                                  CRG      SHTL  QkR SSH      SSL     S       FSH     FSL     S       HSH     HSL     S        Init      GetDeviceMode  SwitcDevhMode  GpioArrived
  {  V_VER2_PCH_N_SERIAL_IO_CFG_I2C0_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_N_SERIAL_IO_CFG_I2C1_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_N_SERIAL_IO_CFG_I2C2_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_N_SERIAL_IO_CFG_I2C3_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_N_SERIAL_IO_CFG_I2C4_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_N_SERIAL_IO_CFG_I2C5_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_N_SERIAL_IO_CFG_I2C6_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER2_PCH_N_SERIAL_IO_CFG_I2C7_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  { 0xFFFF, 0x0, 0x0, { {0x0, 0x0, 0x0}, {0x0, 0x0, 0x0}, {0x0, 0x0, 0x0}, } , NULL, NULL, NULL, NULL}
};

I2C_INFO mI2cBusInfoPchS[] = {
//  I2cPciDID                                  CRG      SHTL  QkR SSH      SSL     S       FSH     FSL     S       HSH     HSL     S        Init      GetDeviceMode  SwitcDevhMode  GpioArrived
  {  V_VER4_PCH_S_SERIAL_IO_CFG_I2C0_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER4_PCH_S_SERIAL_IO_CFG_I2C1_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER4_PCH_S_SERIAL_IO_CFG_I2C2_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER4_PCH_S_SERIAL_IO_CFG_I2C3_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER4_PCH_S_SERIAL_IO_CFG_I2C4_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER4_PCH_S_SERIAL_IO_CFG_I2C5_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER4_PCH_S_SERIAL_IO_CFG_I2C6_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  {  V_VER4_PCH_S_SERIAL_IO_CFG_I2C7_DEVICE_ID, 0x100*8, R_I2C_SDA_HOLD, { {0x01F4, 0x024C, 0x1C}, {0x0080, 0x00AD, 0x1C}, {0x000F, 0x0028, 0x1C}, } , ChipInit, GetDevMode, SwitchDevMode, GpioEventArrived},
  { 0xFFFF, 0x0, 0x0, { {0x0, 0x0, 0x0}, {0x0, 0x0, 0x0}, {0x0, 0x0, 0x0}, } , NULL, NULL, NULL, NULL}
};

#endif
