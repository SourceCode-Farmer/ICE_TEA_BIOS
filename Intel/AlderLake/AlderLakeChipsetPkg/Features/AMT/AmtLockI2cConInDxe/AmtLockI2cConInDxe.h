/** @file
 This file include all platform action which can be customized by IBV/OEM.

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************

*/

#ifndef _AMT_LOCK_I2C_CONIN_DXE_H_
#define _AMT_LOCK_I2C_CONIN_DXE_H_

#include <Protocol/SimpleTextIn.h>
#include <Protocol/SimpleTextInEx.h>
#include <Protocol/IsaIo.h>
#include <Protocol/DevicePath.h>
#include <Protocol/Ps2Policy.h>
#include <Protocol/LegacyBios.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DevicePathLib.h>
#include <Library/DebugLib.h>
#include <Library/ReportStatusCodeLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/HiiLib.h>
#include <Library/ConfigBlockLib.h>
// #include <Library/DxeAmtLib.h>
#include <Library/DxeAsfLib.h>
#include <Protocol/DriverBinding.h>
#include <Protocol/UsbCore.h>
#include <Protocol/HiiDatabase.h>
#include <Protocol/AmtWrapperProtocol.h>
#include <Protocol/AlertStandardFormat.h>
#include <Protocol/AmtPolicy.h>
#include <Protocol/HeciProtocol.h>
#include <Guid/StatusCodeDataTypeId.h>
#include <Guid/EventGroup.h>
#include <Guid/HiiKeyBoardLayout.h>
#include <Guid/H2OUsbKeyBoardLayout.h>
#include <MeBiosPayloadHob.h>
#include <Protocol/I2cIo.h>
#include <I2cHidDeviceInfo.h>
#include <SetupVariable.h>
#include <Library/PcdLib.h>
// #include <MeChipset.h>

#define KEYBOARD_CLASS                  0x0106
#define TDS_ENABLE_BITS                 0xC0


EFI_STATUS
EFIAPI
AmtI2cConInLockrDriverBindingSupported (
  IN EFI_DRIVER_BINDING_PROTOCOL    *This,
  IN EFI_HANDLE                     Controller,
  IN EFI_DEVICE_PATH_PROTOCOL       *RemainingDevicePath
  );


EFI_STATUS
EFIAPI
AmtI2cConInLockrDriverBindingStart (
  IN EFI_DRIVER_BINDING_PROTOCOL    *This,
  IN EFI_HANDLE                     Controller,
  IN EFI_DEVICE_PATH_PROTOCOL       *RemainingDevicePath
  );


EFI_STATUS
EFIAPI
AmtI2cConInLockrDriverBindingStop (
  IN  EFI_DRIVER_BINDING_PROTOCOL    *This,
  IN  EFI_HANDLE                     Controller,
  IN  UINTN                          NumberOfChildren,
  IN  EFI_HANDLE                     *ChildHandleBuffer
  );


#endif
