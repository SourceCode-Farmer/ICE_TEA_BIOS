/** @file
 This file include all platform action which can be customized by IBV/OEM.

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************

*/

#include <UefiBootManagerLib.h>
#include <Guid/AdmiSecureBoot.h>
#include <Uefi.h>
#include "AmtLockPs2ConInDxe.h"

UINT8 gTDSBlockConInProtocol = 0;
BOOLEAN mLockConsole = FALSE;

EFI_DRIVER_BINDING_PROTOCOL gAmtPs2ConInLockrDriver = {
  AmtPs2ConInLockrDriverBindingSupported,
  AmtPs2ConInLockrDriverBindingStart,
  AmtPs2ConInLockrDriverBindingStop,
  0x18,
  NULL,
  NULL
};

EFI_STATUS
EFIAPI
AmtPs2ConInLockrDriverBindingSupported (
  IN EFI_DRIVER_BINDING_PROTOCOL    *This,
  IN EFI_HANDLE                     Controller,
  IN EFI_DEVICE_PATH_PROTOCOL       *RemainingDevicePath
  )
{
  EFI_STATUS                          Status;
  EFI_ISA_IO_PROTOCOL                 *IsaIo;
  UINT8                               Data;

  Data = 0;
  //
  // Open the IO Abstraction(s) needed to perform the supported test
  //
  Status = gBS->OpenProtocol (
                  Controller,
                  &gEfiIsaIoProtocolGuid,
                  (VOID **) &IsaIo,
                  This->DriverBindingHandle,
                  Controller,
                  EFI_OPEN_PROTOCOL_BY_DRIVER
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //
  // Use the ISA I/O Protocol to see if Controller is the Keyboard controller
  //
  if (IsaIo->ResourceList->Device.HID != EISA_PNP_ID (0x303) || IsaIo->ResourceList->Device.UID != 0) {
    Status = EFI_UNSUPPORTED;
  } else {
    Status = IsaIo->Io.Read (
                      IsaIo,
                      EfiIsaIoWidthUint8,
                      KEYBOARD_8042_STATUS_REGISTER,
                      1,
                      &Data
                      );
    if ((EFI_ERROR (Status)) || (Data == 0xFF)) {
      //
      //if status register is FF then KBC is missing
      //
      Status =  EFI_UNSUPPORTED;
    }
  }
  //
  // Close the I/O Abstraction(s) used to perform the supported test
  //
  gBS->CloseProtocol (
         Controller,
         &gEfiIsaIoProtocolGuid,
         This->DriverBindingHandle,
         Controller
         );

  return Status;
}

/**

  Start.

  @param  This                  EFI_DRIVER_BINDING_PROTOCOL
  @param  Controller            Controller handle
  @param  RemainingDevicePath   EFI_DEVICE_PATH_PROTOCOL

  @retval EFI_SUCCESS           Success
  @retval EFI_OUT_OF_RESOURCES  Can't allocate memory
  @retval EFI_UNSUPPORTED       The Start routine fail

**/
EFI_STATUS
EFIAPI
AmtPs2ConInLockrDriverBindingStart (
  IN EFI_DRIVER_BINDING_PROTOCOL    *This,
  IN EFI_HANDLE                     Controller,
  IN EFI_DEVICE_PATH_PROTOCOL       *RemainingDevicePath
  )
{
  EFI_STATUS                                Status;
  EFI_ISA_IO_PROTOCOL                       *IsaIo;
  UINT8                                     TDSBlockConIn;

  gBS->InstallMultipleProtocolInterfaces (&Controller, &gTDSBlockConInProtocolGuid, &gTDSBlockConInProtocol, NULL);

  if (PcdGetBool (PcdTDSBlockConInEnable) || mLockConsole) {
    //
    // It's TDS boot or MPM lock mode.
    // Open IsaIo protocol.
    //
    Status = gBS->OpenProtocol (
                    Controller,
                    &gEfiIsaIoProtocolGuid,
                    (VOID **) &IsaIo,
                    This->DriverBindingHandle,
                    Controller,
                    EFI_OPEN_PROTOCOL_BY_DRIVER
                    );
  }
  //
  // Create relationship for disconnecting.
  //
  Status = gBS->OpenProtocol (
                  Controller,
                  &gTDSBlockConInProtocolGuid,
                  (VOID **) &TDSBlockConIn,
                  This->DriverBindingHandle,
                  Controller,
                  EFI_OPEN_PROTOCOL_BY_DRIVER
                  );
  return Status;
}

/**

  Stop.

  @param  This                  EFI_DRIVER_BINDING_PROTOCOL
  @param  Controller            Controller handle
  @param  NumberOfChildren      Child handle number
  @param  ChildHandleBuffer     Child handle buffer

  @retval EFI_SUCCESS           Success
  @retval EFI_UNSUPPORTED       Can't support

**/
EFI_STATUS
EFIAPI
AmtPs2ConInLockrDriverBindingStop (
  IN  EFI_DRIVER_BINDING_PROTOCOL    *This,
  IN  EFI_HANDLE                     Controller,
  IN  UINTN                          NumberOfChildren,
  IN  EFI_HANDLE                     *ChildHandleBuffer
  )
{
  if (mLockConsole) {
    //
    // MPM lock mode. Don't unlock Input device.
    //
    return EFI_UNSUPPORTED;
  }
  gBS->CloseProtocol (
                Controller,
                &gEfiIsaIoProtocolGuid,
                This->DriverBindingHandle,
                Controller
                );

  gBS->CloseProtocol (
                Controller,
                &gTDSBlockConInProtocolGuid,
                This->DriverBindingHandle,
                Controller
                );
  gBS->UninstallMultipleProtocolInterfaces (Controller, &gTDSBlockConInProtocolGuid, &gTDSBlockConInProtocol, NULL);
  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
AmtLockPs2ConInDxeEntryPoint (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS                    Status;
  UINT8                         SpecialCommandParamHighByte;
  SETUP_DATA                    SystemConfiguration;
  UINTN                         VarSize;
  BOOLEAN                       TrustedDeviceSetupSetupVar;
  AMT_WRAPPER_PROTOCOL          *AmtWrapper;
  EFI_STATUS                    TDSStatus;
  EFI_STATUS                    LockConStatus;

  SpecialCommandParamHighByte = 0;
  TrustedDeviceSetupSetupVar  = FALSE;
  TDSStatus                   = EFI_SUCCESS;
  LockConStatus               = EFI_SUCCESS;

  Status = gBS->LocateProtocol (&gAmtWrapperProtocolGuid, NULL, (VOID **) &AmtWrapper);

  if (!EFI_ERROR (Status)) {
    if (AmtWrapper->IsConsoleLocked()) {
      mLockConsole = TRUE;
    } else {
      LockConStatus = EFI_UNSUPPORTED;
    }
  }

  //
  // Check ME status
  //
  SpecialCommandParamHighByte = AsfGetSpecialCmdParamHighByte ();
  //
  // Check SCU TDS option
  //
  VarSize = sizeof (SETUP_DATA);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  NULL,
                  &VarSize,
                  &SystemConfiguration
                  );

  if (!EFI_ERROR (Status)) {
    TrustedDeviceSetupSetupVar = SystemConfiguration.TrustedDeviceSetupBoot;
  }

  if ((SpecialCommandParamHighByte & TDS_ENABLE_BITS) != TDS_ENABLE_BITS && !TrustedDeviceSetupSetupVar) {
    TDSStatus = EFI_UNSUPPORTED;
  } else {
    Status = PcdSetBoolS (PcdTDSBlockConInEnable, TRUE);
    if (EFI_ERROR (Status)) {
      TDSStatus = EFI_UNSUPPORTED;
    }
  }

  if (EFI_ERROR (LockConStatus) && EFI_ERROR (TDSStatus)) {
    return EFI_UNSUPPORTED;
  }

  Status = EfiLibInstallDriverBindingComponentName2 (
             ImageHandle,
             SystemTable,
             &gAmtPs2ConInLockrDriver,
             ImageHandle,
             NULL,
             NULL
             );

  ASSERT_EFI_ERROR (Status);
  return Status;
}
