/** @file
  This driver is for providing the Int15 callback service routines. It is a
  SMM driver. Operations (HOOKS) are assigned (divided) by different Function
  Numbers. Hook only for KBL-R platform ware reset (CTRL + ALT + DEL) under
  DOS (Boot mode is legacy mode).

;******************************************************************************
;* Copyright (c) 2018 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/ResetSystemLib.h>
#include <Protocol/SmmInt15Service.h>
#include <PlatformBoardId.h>

#define INT15_9999_RESET_HOOK			0x9999
#define INT15_9999_DEAD_SUBFUNCTION		0xDEAD

VOID
ResetHookCallBack (
  IN OUT EFI_IA32_REGISTER_SET          *CpuRegs, 
  IN VOID                               *Context
)
{
  UINT32                                Int15FunNum;

  Int15FunNum = (CpuRegs->X.AX & 0xFFFF);
  if (!Int15FunNum) {
    return;
  }

  if ((Int15FunNum == INT15_9999_RESET_HOOK) && (INT15_9999_DEAD_SUBFUNCTION == CpuRegs->X.BX)) {
    ResetWarm();
  }
}


/**
  Initializes the SMM Dispatcher for CSM INT15 Reset HOOK Services.

  @param  ImageHandle       Pointer to the loaded image protocol for this driver.
  @param  SystemTable       Pointer to the EFI System Table.

  @return Status            EFI_SUCCESS
  
**/
EFI_STATUS
CsmInt15HookSmmEntry (
  IN EFI_HANDLE                         ImageHandle,
  IN EFI_SYSTEM_TABLE                   *SystemTable
  )
{
  EFI_STATUS                            Status;
  EFI_SMM_INT15_SERVICE_PROTOCOL        *SmmInt15Service;

  Status = EFI_SUCCESS;
  SmmInt15Service = NULL;

  DEBUG ((DEBUG_ERROR | DEBUG_INFO, "CsmInt15HookSmmEntry\n"));

  Status = gSmst->SmmLocateProtocol (
                    &gEfiSmmInt15ServiceProtocolGuid,
                    NULL,
                    (VOID **)&SmmInt15Service
                    );

  if (EFI_ERROR(Status)) {
    return Status;
  }

  Status = SmmInt15Service->InstallInt15ProtocolInterface (
                              SmmInt15Service,
                              INT15_9999_RESET_HOOK,
                              ResetHookCallBack,
                              NULL
                              );

  if (Status == EFI_ALREADY_STARTED) {
    //
    // use new callback function to replace original one
    //
    Status = SmmInt15Service->ReinstallInt15ProtocolInterface (
                                SmmInt15Service,
                                INT15_9999_RESET_HOOK,
                                ResetHookCallBack,
                                NULL
                                );

    DEBUG ((DEBUG_ERROR | DEBUG_INFO, "ReinstallInt15ProtocolInterface INT15_9999_RESET_HOOK, Status : %r\n", Status));
    return Status;
  }

  DEBUG ((DEBUG_ERROR | DEBUG_INFO, "InstallInt15ProtocolInterface INT15_9999_RESET_HOOK, Status : %r\n", Status));
  return Status;
}