/** @file
 Implement the Chipset IHISI FMTS subfunction for this driver.

***************************************************************************
* Copyright (c) 2019 - 2021, Insyde Software Corp. All Rights Reserved.
*
* You may not reproduce, distribute, publish, display, perform, modify, adapt,
* transmit, broadcast, present, recite, release, license or otherwise exploit
* any part of this publication in any form, by any means, without the prior
* written permission of Insyde Software Corporation.
*
******************************************************************************
*/
#ifndef  _IHISI_FMTS_H_
#define  _IHISI_FMTS_H_

#include <Uefi.h>
#include <H2OIhisi.h>
#include <Library/DebugLib.h>
#include <Library/SmmOemSvcChipsetLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/SmmChipsetSvcLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Protocol/H2OIhisi.h>

#include <Library/FWUpdateLib.h>

#include <Library/FdSupportLib.h>
#include <Library/BaseMemoryLib.h>

#include <Library/VariableLib.h>
#include <Guid/IshUpdVariable.h>
#include <Guid/PdtUpdVariable.h>
#include <Library/HobLib.h>
#include <Guid/GlobalVariable.h>
#include <Guid/SysFwUpdateProgress.h>
#include <Library/VariableLib.h>

#define FMTS_INPUT_SIGNATURE   SIGNATURE_32 ('$', 'M', 'E', 'I')
#define FMTS_OUTPUT_SIGNATURE   SIGNATURE_32 ('$', 'M', 'E', 'O')

typedef enum {
  TYPE_ME,
  TYPE_PERCENTAGE,
  TYPE_PASSWARD,
  TYPE_OEM_ID,
  TYPE_ISH,
  TYPE_PDT,
  TYPE_IOM,
  TYPE_MGPHY,
  TYPE_TBT
} FUNCTION_TYPE;

//
// AH=32h, Update Intel Native ME
//
#pragma pack (1)
// INPUT:
//  ECX: Pointer to AP communication data buffer.
typedef struct {
  UINT32                        Signature;    // $MEI
  UINT32                        StructureSize;
  UINT8                         FunctionType;
  UINT8                         Reserved[3];
  UINT32                        DataSize;
  _UUID                         OemId;
  CHAR8                         UsePassword[16];
} FMTS_INPUT_DATA_STRUCTURE;

// OUTPUT:
//  ECX: Pointer to BIOS communication data buffer
typedef struct {
  UINT32                        Signature;    // $MEO
  UINT32                        StructureSize;
  UINT32                        ReturnStatusCode;
  UINT8                         Percentage;   // 0~100 %
} FMTS_OUTPUT_DATA_STRUCTURE;

#pragma pack ()


EFI_STATUS
EFIAPI
FmtsUseIntelLib(
  VOID
  );

EFI_STATUS
EFIAPI
FmtsUseIntelLibInit (
  VOID
  );

EFI_STATUS
EFIAPI
FmtsUseIntelLibWrite(
  VOID                          *MeDataBuffer,
  UINT32                         MeDataSize,
  _UUID                          UserOemId
//  CHAR8                          *UserPassword
  );

EFI_STATUS
EFIAPI
FmtsUseIntelLibPartialWrite(
  VOID                          *MeDataBuffer,
  UINT32                         MeDataSize,
  _UUID                          OemId,
  UINT8                          FunctionType
  );

EFI_STATUS
EFIAPI
FmtsUseIntelLibSendPdt(
  VOID                          *MeDataBuffer,
  UINT32                         MeDataSize
  );

EFI_STATUS
EFIAPI
FmtsUseIntelLibWriteUpdateStages(
  UINT32   *PercentWritten
  );
#endif
