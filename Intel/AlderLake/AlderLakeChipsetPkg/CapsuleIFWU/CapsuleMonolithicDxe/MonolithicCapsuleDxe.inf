## @file
#  Component description file for Capsule Processor Dxe module.
#
#******************************************************************************
#* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = MonolithicCapsuleDxe
  FILE_GUID                      = 4fab12fa-21d5-42a2-8260-e0cfa50c899b
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = ChipsetMonolithicFWEntryPoint

[sources.common]
  MonolithicCapsuleDxe.c
  MonolithicCapsuleDxe.h

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  $(CHIPSET_REF_CODE_PKG)/SiPkg.dec
  $(PLATFORM_SAMPLE_CODE_DEC_NAME)/PlatformPkg.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec

[LibraryClasses]
  BaseLib
  UefiLib
  PcdLib
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  UefiDriverEntryPoint
  BaseMemoryLib
  MemoryAllocationLib
  DevicePathLib
  DebugLib
  ChipsetCapsuleLib
  ChipsetSignatureLib
  BvdtLib
  OemGraphicsLib
  VariableLib
  BdsCpLib
  BvdtLib
  SeamlessRecoveryLib

[Protocols]
  gEfiFirmwareManagementProtocolGuid

[Guids]
  gEfiSystemResourceTableGuid
  gEfiFmpCapsuleGuid
  gEfiCertX509Guid
  gSecureFlashInfoGuid
  gWindowsUxCapsuleGuid
  gEfiEndOfDxeEventGroupGuid
  gH2OBdsCpDisplayBeforeProtocolGuid
  gSysFwUpdateProgressGuid
  gFmpCapsuleInfoGuid

[Pcd]
  gInsydeTokenSpaceGuid.PcdCapsuleMaxResult
  gInsydeTokenSpaceGuid.PcdSecureFlashCertificateFile
  gInsydeTokenSpaceGuid.PcdOsIndicationsSupported
  gChipsetPkgTokenSpaceGuid.PcdWindowsMonolithicFirmwareCapsuleGuid
  gInsydeTokenSpaceGuid.PcdH2OEsrtSystemFirmwareLowestSupportedVersion
  gInsydeTokenSpaceGuid.PcdFirmwareResourceMaximum
  gChipsetPkgTokenSpaceGuid.PcdChasmFallsSupport
  gPlatformModuleTokenSpaceGuid.PcdMeResiliencyEnable

[Depex]
  gEfiVariableArchProtocolGuid AND
  gEfiVariableWriteArchProtocolGuid

[BuildOptions]
  MSFT:*_VS2015_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_VS2015x86_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_VS2017_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_VS2019_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_DEVTLSxVC14_*_DLINK_FLAGS = /LTCG:OFF

