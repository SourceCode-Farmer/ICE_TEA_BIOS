## @file
#  Component description file for Capsule Processor Dxe module.
#
#******************************************************************************
#* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = MgPhyCapsuleDxe
  FILE_GUID                      = 9ff89bf4-5bcf-4a64-99e2-fbc22aa02d7d
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = ChipsetMgPhyFWUEntryPoint

[sources.common]
  MgPhyCapsuleDxe.c
  MgPhyCapsuleDxe.h

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec

[LibraryClasses]
  BaseLib
  UefiLib
  PcdLib
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  UefiDriverEntryPoint
  BaseMemoryLib
  MemoryAllocationLib
  DevicePathLib
  DebugLib
  ChipsetCapsuleLib
  ChipsetSignatureLib
  BvdtLib
  OemGraphicsLib
  VariableLib
  BdsCpLib

[Protocols]
  gEfiFirmwareManagementProtocolGuid

[Guids]
  gEfiSystemResourceTableGuid
  gEfiFmpCapsuleGuid
  gEfiCertX509Guid
  gSecureFlashInfoGuid
  gWindowsUxCapsuleGuid

  gEfiEndOfDxeEventGroupGuid
  gH2OBdsCpDisplayBeforeProtocolGuid

[Pcd]
  gInsydeTokenSpaceGuid.PcdCapsuleMaxResult
  gInsydeTokenSpaceGuid.PcdSecureFlashCertificateFile
  gInsydeTokenSpaceGuid.PcdOsIndicationsSupported
  gInsydeTokenSpaceGuid.PcdFirmwareResourceMaximum
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedMgPhyVersion
  gChipsetPkgTokenSpaceGuid.PcdWindowsMgPhyFirmwareCapsuleGuid

[Depex]
  gEfiVariableArchProtocolGuid AND
  gEfiVariableWriteArchProtocolGuid

[BuildOptions]
  MSFT:*_*_*_DLINK_FLAGS = /FORCE:MULTIPLE
  MSFT:*_VS2015_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_VS2015x86_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_VS2017_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_VS2019_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_DEVTLSxVC14_*_DLINK_FLAGS = /LTCG:OFF
  MSFT:*_DEVTLSxVC16_*_DLINK_FLAGS = /LTCG:OFF
  
