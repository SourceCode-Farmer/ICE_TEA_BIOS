## @file
#  Component description file for Capsule Processor Dxe module.
#
#******************************************************************************
#* Copyright (c) 2012 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = TbtRetimerCapsule1Dxe
  FILE_GUID                      = e273212c-11d9-4728-b1ac-b6ee5083eed6
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = ChipsetTbtRetimer1FWUEntryPoint

[sources.common]
  TbtRetimerCapsule1Dxe.c
  TbtRetimerCapsule1Dxe.h

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
#[-start-210408-IB17800122-add]#
  #$(PLATFORM_SAMPLE_CODE_DEC_NAME)/$(PLATFORM_SAMPLE_CODE_DEC_NAME).dec 
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-end-210408-IB17800122-add]#
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  MinPlatformPkg/MinPlatformPkg.dec
#[-start-211109-BAIN000054-add]#
!if ($(C970_SUPPORT_ENABLE) == YES) OR ($(C770_SUPPORT_ENABLE) == YES) OR ($(S77013_SUPPORT_ENABLE) == YES) OR ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  LfcPkg/LfcPkg.dec
!endif
#[-end-211109-BAIN000054-add]#

[LibraryClasses]
  BaseLib
  UefiLib
  PcdLib
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  UefiDriverEntryPoint
  BaseMemoryLib
  MemoryAllocationLib
  DevicePathLib
  DebugLib
  ChipsetCapsuleLib
  ChipsetSignatureLib
  BvdtLib
  OemGraphicsLib
  VariableLib
  TcssRetimerNvmUpdateLib
  H2OCpLib
  TimerLib
#[-start-211109-BAIN000054-add]#
!if ($(C970_SUPPORT_ENABLE) == YES) OR ($(C770_SUPPORT_ENABLE) == YES) OR ($(S77013_SUPPORT_ENABLE) == YES) OR ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  LfcEcLib
  GpioLib
!endif
#[-end-211109-BAIN000054-add]#

[Protocols]
  gEfiFirmwareManagementProtocolGuid
  gTcssRetimerProtocolGuid
  
[Guids]
  gEfiSystemResourceTableGuid
  gEfiFmpCapsuleGuid
  gEfiCertX509Guid
  gSecureFlashInfoGuid
  gWindowsUxCapsuleGuid
  gEfiEndOfDxeEventGroupGuid
  gH2OBdsCpDisplayBeforeProtocolGuid
  gAllTbtRetimerDeviceGuid
  gAllNonTbtI2cRetimerDeviceGuid
  gTbtRetimer1VerGuid
  gBoardNotificationInitGuid
  gH2OBdsCpReadyToBootBeforeGuid
  gH2OBdsCpEndOfDxeBeforeGuid

[Pcd]
  gInsydeTokenSpaceGuid.PcdCapsuleMaxResult
  gInsydeTokenSpaceGuid.PcdSecureFlashCertificateFile
  gInsydeTokenSpaceGuid.PcdOsIndicationsSupported
  gInsydeTokenSpaceGuid.PcdFirmwareResourceMaximum
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedRetimerVersion
  gChipsetPkgTokenSpaceGuid.PcdWindowsTbtRetimer1FirmwareCapsuleGuid
  gChipsetPkgTokenSpaceGuid.PcdTbtRetimerAddress
  gChipsetPkgTokenSpaceGuid.PcdTbtRetimerGetVerDuringBoot
  gChipsetPkgTokenSpaceGuid.PcdTbtRetimerDefaultVersion
  gInsydeTokenSpaceGuid.PcdH2OBdsCpEndOfDxeBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpReadyToBootBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceEnabled

[Depex]
  gEfiVariableArchProtocolGuid AND
  gEfiVariableWriteArchProtocolGuid AND
  gTcssRetimerProtocolGuid

[BuildOptions]
  MSFT: *_*_*_DLINK_FLAGS = /FORCE:MULTIPLE
