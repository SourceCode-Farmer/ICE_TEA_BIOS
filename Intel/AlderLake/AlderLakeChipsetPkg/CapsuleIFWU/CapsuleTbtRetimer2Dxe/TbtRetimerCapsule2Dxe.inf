## @file
#  Component description file for Capsule Processor Dxe module.
#
#******************************************************************************
#* Copyright (c) 2012 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************

[defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = TbtRetimerCapsule2Dxe
  FILE_GUID                      = 331f9815-9d94-4a26-9cd2-de377b5e9a08
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = ChipsetTbtRetimer2FWUEntryPoint

[sources.common]
  TbtRetimerCapsule2Dxe.c
  TbtRetimerCapsule2Dxe.h

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
#[-start-210408-IB17800122-add]#
  #$(PLATFORM_SAMPLE_CODE_DEC_NAME)/$(PLATFORM_SAMPLE_CODE_DEC_NAME).dec 
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-end-210408-IB17800122-add]#
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  MinPlatformPkg/MinPlatformPkg.dec

[LibraryClasses]
  BaseLib
  UefiLib
  PcdLib
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  UefiDriverEntryPoint
  BaseMemoryLib
  MemoryAllocationLib
  DevicePathLib
  DebugLib
  ChipsetCapsuleLib
  ChipsetSignatureLib
  BvdtLib
  OemGraphicsLib
  VariableLib
  BdsCpLib
  TcssRetimerNvmUpdateLib
  H2OCpLib
  TimerLib
 
[Protocols]
  gEfiFirmwareManagementProtocolGuid
  gTcssRetimerProtocolGuid
  
[Guids]
  gEfiSystemResourceTableGuid
  gEfiFmpCapsuleGuid
  gEfiCertX509Guid
  gSecureFlashInfoGuid
  gWindowsUxCapsuleGuid
  gEfiEndOfDxeEventGroupGuid
  gH2OBdsCpDisplayBeforeProtocolGuid
  gAllTbtRetimerDeviceGuid
  gAllNonTbtI2cRetimerDeviceGuid
  gTbtRetimer2VerGuid
  gTbtRetimer1VerGuid
  gBoardNotificationInitGuid
  gH2OBdsCpReadyToBootBeforeGuid
  gH2OBdsCpEndOfDxeBeforeGuid
  
[Pcd]
  gInsydeTokenSpaceGuid.PcdCapsuleMaxResult
  gInsydeTokenSpaceGuid.PcdSecureFlashCertificateFile
  gInsydeTokenSpaceGuid.PcdOsIndicationsSupported
  gInsydeTokenSpaceGuid.PcdFirmwareResourceMaximum
  gChipsetPkgTokenSpaceGuid.PcdLowestSupportedRetimerVersion
  gChipsetPkgTokenSpaceGuid.PcdWindowsTbtRetimer2FirmwareCapsuleGuid
  gChipsetPkgTokenSpaceGuid.PcdTbtRetimerAddress
  gChipsetPkgTokenSpaceGuid.PcdTbtRetimerGetVerDuringBoot
  gChipsetPkgTokenSpaceGuid.PcdTbtRetimerDefaultVersion
  gInsydeTokenSpaceGuid.PcdH2OBdsCpEndOfDxeBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBdsCpReadyToBootBeforeSupported
  gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceEnabled

[Depex]
  gEfiVariableArchProtocolGuid AND
  gEfiVariableWriteArchProtocolGuid AND
  gTcssRetimerProtocolGuid

[BuildOptions]
  MSFT: *_*_*_DLINK_FLAGS = /FORCE:MULTIPLE 