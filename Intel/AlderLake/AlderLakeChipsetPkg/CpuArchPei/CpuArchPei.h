/** @file
   Cpu Architecture PEIM header file
;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
**/


#ifndef _CPU_ARCH_PEI_H_
#define _CPU_ARCH_PEI_H_

#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Ppi/Cpu.h>
#include <Ppi/Legacy8259.h>

#define INTERRUPT_VECTOR_NUMBER               256
#define INTERRUPT_GATE_ATTRIBUTE              0x8e00

//
// Local APIC defines
//
#define APIC_REGISTER_LOCAL_ID_OFFSET         0x00000020
#define APIC_REGISTER_APIC_VERSION_OFFSET     0x00000030
#define APIC_REGISTER_SPURIOUS_VECTOR_OFFSET  0x000000F0
#define APIC_REGISTER_ICR_LOW_OFFSET          0x00000300
#define APIC_REGISTER_ICR_HIGH_OFFSET         0x00000310
#define APIC_REGISTER_LINT0_VECTOR_OFFSET     0x00000350
#define APIC_REGISTER_LINT1_VECTOR_OFFSET     0x00000360

//
// MSR defines
//
#define MSR_IA32_APIC_BASE                    0x0000001B

#pragma pack(1)

typedef struct {
  UINT16  OffsetLow;
  UINT16  SegmentSelector;
  UINT16  Attributes;
  UINT16  OffsetHigh;
} INTERRUPT_GATE_DESCRIPTOR;

#pragma pack()

typedef struct {
  VOID  *Start;
  UINTN Size;
  UINTN FixOffset;
} INTERRUPT_HANDLER_TEMPLATE_MAP;


/**
  Creates a new GDT in RAM and load it.

  This function creates a new GDT in RAM and load it.

**/
VOID
EFIAPI
InitializeGdt (
  VOID
  );

/**
  Return address map of interrupt handler template so that C code can generate
  interrupt handlers, and dynamically do address fix.

  @param[in] AddressMap  Pointer to a buffer where the address map is returned.

**/
VOID
EFIAPI
GetTemplateAddressMap (
  OUT INTERRUPT_HANDLER_TEMPLATE_MAP *AddressMap
  );

/**
  Creates an IDT table starting at IdtTablPtr. It has IdtLimit/8 entries.
  Table is initialized to intxx where xx is from 00 to number of entries or
  100h, whichever is smaller. After table has been initialized the LIDT
  instruction is invoked.

  TableStart is the pointer to the callback table and is not used by
  InitializedIdt but by commonEntry. CommonEntry handles all interrupts,
  does the context save and calls the callback entry, if non-NULL.
  It is the responsibility of the callback routine to do hardware EOIs.

  @param[in] TableStart     Pointer to interrupt callback table.
  @param[in] IdtTablePtr    Pointer to IDT table.
  @param[in] IdtTableLimit  IDT Table limit (number of interrupt entries * 8).

**/

VOID
EFIAPI
InitializeIdt (
  IN EFI_CPU_INTERRUPT_HANDLER      *TableStart,
  IN UINTN                          *IdtTablePtr,
  IN UINT16                         IdtTableLimit
  );

/**
  This function flushes the range of addresses from Start to Start+Length
  from the processor's data cache. If Start is not aligned to a cache line
  boundary, then the bytes before Start to the preceding cache line boundary
  are also flushed. If Start+Length is not aligned to a cache line boundary,
  then the bytes past Start+Length to the end of the next cache line boundary
  are also flushed. The FlushType of EfiCpuFlushTypeWriteBackInvalidate must be
  supported. If the data cache is fully coherent with all DMA operations, then
  this function can just return EFI_SUCCESS. If the processor does not support
  flushing a range of the data cache, then the entire data cache can be flushed.

  @param[in]  This              The H2O_CPU_ARCH_PPI instance.
  @param[in]  Start             The beginning physical address to flush from the processor's data
                                cache.
  @param[in]  Length            The number of bytes to flush from the processor's data cache. This
                                function may flush more bytes than Length specifies depending upon
                                the granularity of the flush operation that the processor supports.
  @param[in]  FlushType         Specifies the type of flush operation to perform.

  @retval EFI_SUCCESS           The address range from Start to Start+Length was flushed from
                                the processor's data cache.
  @retval EFI_UNSUPPORTEDT      The processor does not support the cache flush type specified
                                by FlushType.
  @retval EFI_DEVICE_ERROR      The address range from Start to Start+Length could not be flushed
                                from the processor's data cache.

**/
EFI_STATUS
EFIAPI
FlushCpuDataCache (
  IN H2O_CPU_ARCH_PPI                   *This,
  IN EFI_PHYSICAL_ADDRESS               Start,
  IN UINT64                             Length,
  IN EFI_CPU_FLUSH_TYPE                 FlushType
  );

/**
  This function enables interrupt processing by the processor.

  @param[in]  This                  The H2O_CPU_ARCH_PPI instance.

  @retval     EFI_SUCCESS           Interrupts are enabled on the processor.
  @retval     EFI_DEVICE_ERROR      Interrupts could not be enabled on the processor.

**/
EFI_STATUS
EFIAPI
EnableInterrupt (
  IN H2O_CPU_ARCH_PPI              *This
  );

/**
  This function disables interrupt processing by the processor.

  @param[in]  This                  The H2O_CPU_ARCH_PPI instance.

  @retval     EFI_SUCCESS           Interrupts are disabled on the processor.
  @retval     EFI_DEVICE_ERROR      Interrupts could not be disabled on the processor.

**/
EFI_STATUS
EFIAPI
DisableInterrupt (
  IN H2O_CPU_ARCH_PPI              *This
  );

/**
  This function retrieves the processor's current interrupt state a returns it in
  State. If interrupts are currently enabled, then TRUE is returned. If interrupts
  are currently disabled, then FALSE is returned.

  @param[in]  This              The H2O_CPU_ARCH_PPI instance.
  @param[in]  State             A pointer to the processor's current interrupt state. Set to TRUE if
                                interrupts are enabled and FALSE if interrupts are disabled.

  @retval EFI_SUCCESS           The processor's current interrupt state was returned in State.
  @retval EFI_INVALID_PARAMETER State is NULL.

**/
EFI_STATUS
EFIAPI
GetInterruptStateInstance (
  IN  H2O_CPU_ARCH_PPI             *This,
  OUT BOOLEAN                      *State
  );

/**
  This function generates an INIT on the processor. If this function succeeds, then the
  processor will be reset, and control will not be returned to the caller. If InitType is
  not supported by this processor, or the processor cannot programmatically generate an
  INIT without help from external hardware, then EFI_UNSUPPORTED is returned. If an error
  occurs attempting to generate an INIT, then EFI_DEVICE_ERROR is returned.

  @param[in]  This              The H2O_CPU_ARCH_PPI instance.
  @param[in]  InitType          The type of processor INIT to perform.

  @retval EFI_SUCCESS           The processor INIT was performed. This return code should never be seen.
  @retval EFI_UNSUPPORTED       The processor INIT operation specified by InitType is not supported
                                by this processor.
  @retval EFI_DEVICE_ERROR      The processor INIT failed.

**/
EFI_STATUS
EFIAPI
Init
  (
  IN H2O_CPU_ARCH_PPI              *This,
  IN EFI_CPU_INIT_TYPE             InitType
  );


/**
  This function Registers a function to be called from the processor interrupt handler.

  This function implements RegisterInterruptHandler() service of CPU Architecture Protocol.
  This function Registers a function to be called from the processor interrupt handler.

  @param[in]  This               The EFI_CPU_ARCH_PROTOCOL instance.
  @param[in]  InterruptType      Defines which interrupt or exception to hook.
  @param[in]  InterruptHandler   A pointer to a function of type EFI_CPU_INTERRUPT_HANDLER
                                 that is called when a processor interrupt occurs.
                                 If this parameter is NULL, then the handler will be uninstalled.

  @retval EFI_SUCCESS            The handler for the processor interrupt was successfully installed or uninstalled.
  @retval EFI_ALREADY_STARTED    InterruptHandler is not NULL, and a handler for InterruptType was previously installed.
  @retval EFI_INVALID_PARAMETER  InterruptHandler is NULL, and a handler for InterruptType was not previously installed.
  @retval EFI_UNSUPPORTED        The interrupt specified by InterruptType is not supported.

**/
EFI_STATUS
EFIAPI
RegisterInterruptHandler (
  IN H2O_CPU_ARCH_PPI              *This,
  IN EFI_EXCEPTION_TYPE            InterruptType,
  IN EFI_CPU_INTERRUPT_HANDLER     InterruptHandler
  );

/**
  This function reads the processor timer specified by TimerIndex and returns it in TimerValue.

  @param[in]   This             The H2O_CPU_ARCH_PPI instance.
  @param[in]   TimerIndex       Specifies which processor timer is to be returned in TimerValue. This parameter
                                must be between 0 and NumberOfTimers-1.
  @param[out]  TimerValue       Pointer to the returned timer value.
  @param[out]  TimerPeriod      A pointer to the amount of time that passes in femtoseconds for each increment
                                of TimerValue. If TimerValue does not increment at a predictable rate, then 0 is
                                returned. This parameter is optional and may be NULL.

  @retval EFI_SUCCESS           The processor timer value specified by TimerIndex was returned in TimerValue.
  @retval EFI_DEVICE_ERROR      An error occurred attempting to read one of the processor's timers.
  @retval EFI_INVALID_PARAMETER TimerValue is NULL or TimerIndex is not valid.
  @retval EFI_UNSUPPORTED       The processor does not have any readable timers.

**/
EFI_STATUS
GetTimerValue
(
  IN H2O_CPU_ARCH_PPI                   *This,
  IN UINT32                             TimerIndex,
  OUT UINT64                            *TimerValue,
  OUT UINT64                            *TimerPeriod OPTIONAL
  );

/**
  This function modifies the attributes for the memory region specified by BaseAddress and
  Length from their current attributes to the attributes specified by Attributes.

  @param[in]  This              The H2O_CPU_ARCH_PPI instance.
  @param[in]  BaseAddress       The physical address that is the start address of a memory region.
  @param[in]  Length            The size in bytes of the memory region.
  @param[in]  Attributes        The bit mask of attributes to set for the memory region.

  @retval EFI_SUCCESS           The attributes were set for the memory region.
  @retval EFI_ACCESS_DENIED     The attributes for the memory resource range specified by
                                BaseAddress and Length cannot be modified.
  @retval EFI_INVALID_PARAMETER Length is zero.
                                Attributes specified an illegal combination of attributes that
                                cannot be set together.
  @retval EFI_OUT_OF_RESOURCES  There are not enough system resources to modify the attributes of
                                the memory resource range.
  @retval EFI_UNSUPPORTED       The processor does not support one or more bytes of the memory
                                resource range specified by BaseAddress and Length.
                                The bit mask of attributes is not support for the memory resource
                                range specified by BaseAddress and Length.

**/
EFI_STATUS
EFIAPI
SetMemoryAttributes (
  IN  H2O_CPU_ARCH_PPI                  *This,
  IN  EFI_PHYSICAL_ADDRESS              BaseAddress,
  IN  UINT64                            Length,
  IN  UINT64                            Attributes
  );

/**
  Set Interrupt Descriptor Table Handler Address.

  @param [in] Index        The Index of the interrupt descriptor table handle.

**/
VOID
SetInterruptDescriptorTableHandlerAddress (
  IN UINTN                         Index
  );

#endif

