/** @file
  Initializes CPU IDT table and implements CPU Architecture PPI

;******************************************************************************
;* Copyright (c) 2015, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <CpuArchPei.h>

H2O_CPU_INTERRUPT_HANDLER      mExternalVectorTable[INTERRUPT_VECTOR_NUMBER];
INTERRUPT_HANDLER_TEMPLATE_MAP mTemplateMap;
INTERRUPT_GATE_DESCRIPTOR      *mIdtBase          = NULL;
VOID                           *mTemplateHandler  = NULL;
BOOLEAN                        mTriggle           = FALSE;
UINT32                         mErrorCodeFlag     = 0x00027d00;

H2O_CPU_ARCH_PPI               mCpuArchInterfacePpi = {
  FlushCpuDataCache,
  EnableInterrupt,
  DisableInterrupt,
  GetInterruptStateInstance,
  Init,
  RegisterInterruptHandler,
  GetTimerValue,
  SetMemoryAttributes,
  1,
  4,
};

EFI_PEI_PPI_DESCRIPTOR         mCpuArchPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gH2OCpuArchPpiGuid,
  &mCpuArchInterfacePpi
};

/**
  This function flushes the range of addresses from Start to Start+Length 
  from the processor's data cache. If Start is not aligned to a cache line 
  boundary, then the bytes before Start to the preceding cache line boundary 
  are also flushed. If Start+Length is not aligned to a cache line boundary, 
  then the bytes past Start+Length to the end of the next cache line boundary 
  are also flushed. The FlushType of EfiCpuFlushTypeWriteBackInvalidate must be 
  supported. If the data cache is fully coherent with all DMA operations, then 
  this function can just return EFI_SUCCESS. If the processor does not support 
  flushing a range of the data cache, then the entire data cache can be flushed.

  @param[in]  This             The H2O_CPU_ARCH_PPI instance.
  @param[in]  Start            The beginning physical address to flush from the processor's data
                               cache.
  @param[in]  Length           The number of bytes to flush from the processor's data cache. This
                               function may flush more bytes than Length specifies depending upon
                               the granularity of the flush operation that the processor supports.
  @param[in]  FlushType        Specifies the type of flush operation to perform.

  @retval EFI_SUCCESS          The address range from Start to Start+Length was flushed from
                               the processor's data cache.
  @retval EFI_UNSUPPORTEDT     The processor does not support the cache flush type specified
                               by FlushType.
  @retval EFI_DEVICE_ERROR     The address range from Start to Start+Length could not be flushed
                               from the processor's data cache.

**/
EFI_STATUS
EFIAPI 
FlushCpuDataCache (
  IN H2O_CPU_ARCH_PPI                   *This,
  IN EFI_PHYSICAL_ADDRESS               Start,
  IN UINT64                             Length,
  IN EFI_CPU_FLUSH_TYPE                 FlushType
  )
{
  return EFI_UNSUPPORTED;
}

/**
  This function enables interrupt processing by the processor. 

  @param[in]  This              The H2O_CPU_ARCH_PPI instance.

  @retval EFI_SUCCESS           Interrupts are enabled on the processor.
  @retval EFI_DEVICE_ERROR      Interrupts could not be enabled on the processor.

**/
EFI_STATUS
EFIAPI 
EnableInterrupt (
  IN H2O_CPU_ARCH_PPI              *This
  )
{
  EnableInterrupts ();
  return EFI_SUCCESS;
}

/**
  This function disables interrupt processing by the processor.

  @param[in]  This              The H2O_CPU_ARCH_PPI instance.

  @retval EFI_SUCCESS           Interrupts are disabled on the processor.
  @retval EFI_DEVICE_ERROR      Interrupts could not be disabled on the processor.

**/
EFI_STATUS
EFIAPI 
DisableInterrupt (
  IN H2O_CPU_ARCH_PPI              *This
  )
{
  DisableInterrupts ();
  return EFI_SUCCESS;
}

/**
  This function retrieves the processor's current interrupt state a returns it in 
  State. If interrupts are currently enabled, then TRUE is returned. If interrupts 
  are currently disabled, then FALSE is returned.

  @param[in]   This             The H2O_CPU_ARCH_PPI instance.
  @param[out]  State            A pointer to the processor's current interrupt state. Set to TRUE if
                                interrupts are enabled and FALSE if interrupts are disabled.

  @retval EFI_SUCCESS           The processor's current interrupt state was returned in State.
  @retval EFI_INVALID_PARAMETER State is NULL.

**/
EFI_STATUS
EFIAPI 
GetInterruptStateInstance (
  IN  H2O_CPU_ARCH_PPI             *This,
  OUT BOOLEAN                      *State
  )
{
  return EFI_UNSUPPORTED;
}

/**
  This function generates an INIT on the processor. If this function succeeds, then the
  processor will be reset, and control will not be returned to the caller. If InitType is 
  not supported by this processor, or the processor cannot programmatically generate an 
  INIT without help from external hardware, then EFI_UNSUPPORTED is returned. If an error 
  occurs attempting to generate an INIT, then EFI_DEVICE_ERROR is returned.

  @param[in]  This              The H2O_CPU_ARCH_PPI instance.
  @param[in]  InitType          The type of processor INIT to perform.

  @retval EFI_SUCCESS           The processor INIT was performed. This return code should never be seen.
  @retval EFI_UNSUPPORTED       The processor INIT operation specified by InitType is not supported
                                by this processor.
  @retval EFI_DEVICE_ERROR      The processor INIT failed.

**/
EFI_STATUS
EFIAPI 
Init
  (
  IN H2O_CPU_ARCH_PPI              *This,
  IN EFI_CPU_INIT_TYPE             InitType
  )
{
  return EFI_UNSUPPORTED;
}


/**
  This function Registers a function to be called from the processor interrupt handler.

  This function implements RegisterInterruptHandler() service of CPU Architecture Protocol.
  This function Registers a function to be called from the processor interrupt handler.

  @param[in]  This               The EFI_CPU_ARCH_PROTOCOL instance.
  @param[in]  InterruptType      Defines which interrupt or exception to hook.
  @param[in]  InterruptHandler   A pointer to a function of type EFI_CPU_INTERRUPT_HANDLER
                                 that is called when a processor interrupt occurs.
                                 If this parameter is NULL, then the handler will be uninstalled.

  @retval EFI_SUCCESS            The handler for the processor interrupt was successfully installed or uninstalled.
  @retval EFI_ALREADY_STARTED    InterruptHandler is not NULL, and a handler for InterruptType was previously installed.
  @retval EFI_INVALID_PARAMETER  InterruptHandler is NULL, and a handler for InterruptType was not previously installed.
  @retval EFI_UNSUPPORTED        The interrupt specified by InterruptType is not supported.

**/
EFI_STATUS
EFIAPI
RegisterInterruptHandler (
  IN H2O_CPU_ARCH_PPI              *This,
  IN EFI_EXCEPTION_TYPE            InterruptType,
  IN H2O_CPU_INTERRUPT_HANDLER     InterruptHandler
  )
{
  if (InterruptType < 0 || InterruptType > 0xff) {
    return EFI_UNSUPPORTED;
  }

  if (InterruptHandler == NULL && mExternalVectorTable[InterruptType] == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if (InterruptHandler != NULL && mExternalVectorTable[InterruptType] != NULL) {
    return EFI_ALREADY_STARTED;
  }

  SetInterruptDescriptorTableHandlerAddress ((UINTN)InterruptType);

  mExternalVectorTable[InterruptType] = InterruptHandler;
  return EFI_SUCCESS;
}

/**
  This function reads the processor timer specified by TimerIndex and returns it in TimerValue.

  @param[in]   This             The H2O_CPU_ARCH_PPI instance.
  @param[in]   TimerIndex       Specifies which processor timer is to be returned in TimerValue. This parameter
                                must be between 0 and NumberOfTimers-1.
  @param[out]  TimerValue       Pointer to the returned timer value.
  @param[out]  TimerPeriod      A pointer to the amount of time that passes in femtoseconds for each increment
                                of TimerValue. If TimerValue does not increment at a predictable rate, then 0 is
                                returned. This parameter is optional and may be NULL.

  @retval EFI_SUCCESS           The processor timer value specified by TimerIndex was returned in TimerValue.
  @retval EFI_DEVICE_ERROR      An error occurred attempting to read one of the processor's timers.
  @retval EFI_INVALID_PARAMETER TimerValue is NULL or TimerIndex is not valid.
  @retval EFI_UNSUPPORTED       The processor does not have any readable timers.

**/
EFI_STATUS
GetTimerValue 
(
  IN H2O_CPU_ARCH_PPI                   *This,
  IN UINT32                             TimerIndex,
  OUT UINT64                            *TimerValue,
  OUT UINT64                            *TimerPeriod OPTIONAL
  )
{
  return EFI_UNSUPPORTED;
}

/**
  This function modifies the attributes for the memory region specified by BaseAddress and
  Length from their current attributes to the attributes specified by Attributes.

  @param[in]  This              The H2O_CPU_ARCH_PPI instance.
  @param[in]  BaseAddress       The physical address that is the start address of a memory region.
  @param[in]  Length            The size in bytes of the memory region.
  @param[in]  Attributes        The bit mask of attributes to set for the memory region.

  @retval EFI_SUCCESS           The attributes were set for the memory region.
  @retval EFI_ACCESS_DENIED     The attributes for the memory resource range specified by
                                BaseAddress and Length cannot be modified.
  @retval EFI_INVALID_PARAMETER Length is zero.
                                Attributes specified an illegal combination of attributes that
                                cannot be set together.
  @retval EFI_OUT_OF_RESOURCES  There are not enough system resources to modify the attributes of
                                the memory resource range.
  @retval EFI_UNSUPPORTED       The processor does not support one or more bytes of the memory
                                resource range specified by BaseAddress and Length.
                                The bit mask of attributes is not support for the memory resource
                                range specified by BaseAddress and Length.

**/
EFI_STATUS
EFIAPI 
SetMemoryAttributes (
  IN  H2O_CPU_ARCH_PPI                  *This,
  IN  EFI_PHYSICAL_ADDRESS              BaseAddress,
  IN  UINT64                            Length,
  IN  UINT64                            Attributes
  )
{
  return EFI_UNSUPPORTED;
}


/**
  Set Interrupt Descriptor Table Handler Address.

  @param[in] Index        The Index of the interrupt descriptor table handle.
  
**/
VOID
SetInterruptDescriptorTableHandlerAddress (
  IN UINTN Index
  )
{
  UINTN       ExceptionHandle;

  //
  // Get the address of handler for entry
  //
  ExceptionHandle = (UINTN)mTemplateHandler + Index * mTemplateMap.Size;
  CopyMem ((VOID *)ExceptionHandle, mTemplateMap.Start, mTemplateMap.Size);
  *(UINT32 *) (ExceptionHandle + mTemplateMap.FixOffset) = Index;

  //
  // Setting Interrupt Gate Descriptor.
  //
  mIdtBase[Index].OffsetLow  = (UINT16) ExceptionHandle;
  mIdtBase[Index].Attributes = INTERRUPT_GATE_ATTRIBUTE;
  mIdtBase[Index].OffsetHigh = (UINT16) (ExceptionHandle >> 16);
}


/**
  Programs XAPIC registers.

  @param[in] BSP             - Is this BSP?
  
**/
VOID
ProgramXApic (
  BOOLEAN BSP
  )
{
  UINT64               ApicBaseReg;
  EFI_PHYSICAL_ADDRESS ApicBase;
  volatile UINT32      *EntryAddress;
  UINT32               EntryValue;

  ApicBaseReg = AsmReadMsr64 (MSR_IA32_APIC_BASE);
  ApicBase    = ApicBaseReg & 0xffffff000ULL;

  //
  // Program the Spurious Vectore entry
  //
  EntryAddress  = (UINT32 *) (UINTN) (ApicBase + APIC_REGISTER_SPURIOUS_VECTOR_OFFSET);
  EntryValue    = *EntryAddress;
  EntryValue    &= 0xFFFFFD0F;
  EntryValue    |= 0x10F;
  *EntryAddress = EntryValue;

  //
  // Program the LINT1 vector entry as extINT
  //
  EntryAddress  = (UINT32 *) (UINTN) (ApicBase + APIC_REGISTER_LINT0_VECTOR_OFFSET);
  EntryValue    = *EntryAddress;

  if (BSP) {
    EntryValue    &= 0xFFFE00FF;
    EntryValue    |= 0x700;
  } else {
    EntryValue    |= 0x10000;
  }

  *EntryAddress = EntryValue;

  //
  // Program the LINT1 vector entry as NMI
  //
  EntryAddress  = (UINT32 *) (UINTN) (ApicBase + APIC_REGISTER_LINT1_VECTOR_OFFSET);
  EntryValue    = *EntryAddress;
  EntryValue    &= 0xFFFE00FF;
  if (BSP) {
    EntryValue    |= 0x400;
  } else {
    EntryValue    |= 0x10400;
  }

  *EntryAddress = EntryValue;

}

/**
  The Entry point of the CPU PEIM

  This function is the Entry point of the CPU IDT Initialize PEIM 
 
  @param[in]  FileHandle  Handle of the file being invoked.
  @param[in]  PeiServices Describes the list of possible PEI Services. 
                          
  @retval     EFI_SUCCESS CpuInterruptPpi is installed  successfully.

**/
EFI_STATUS
EFIAPI
CpuArchInit (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS                      Status;
  EFI_PHYSICAL_ADDRESS            NewIdtTable;
  IA32_DESCRIPTOR                 Idtr;
  UINT16                          CodeSegment;
  UINT16                          OrigIdtEntryCount;
  UINTN                           Index;

  //
  // Create Interrupt Descriptor Table + PeiService function pointer (1 UINTN) 
  //
  Status = (*PeiServices)->AllocatePages (
                             PeiServices, 
                             EfiBootServicesCode,
                             EFI_SIZE_TO_PAGES(sizeof (INTERRUPT_GATE_DESCRIPTOR) * INTERRUPT_VECTOR_NUMBER + sizeof (UINTN)),
                             &NewIdtTable
                             );
  if (EFI_ERROR(Status)) {
    return Status;
  }
  (**PeiServices).SetMem (
                    (VOID *) (UINTN) NewIdtTable, 
                    sizeof (INTERRUPT_GATE_DESCRIPTOR) * INTERRUPT_VECTOR_NUMBER + sizeof (UINTN), 
                    0
                    );

  //
  // Create Template Hander array.
  // 
  GetTemplateAddressMap (&mTemplateMap);

  Status = (**PeiServices).AllocatePool (
                             PeiServices,
                             mTemplateMap.Size * INTERRUPT_VECTOR_NUMBER,
                             (VOID**)&mTemplateHandler
                             );
  if (EFI_ERROR(Status)) {
    return Status;
  }      

  //
  // Get original IDT address, count.
  //
  AsmReadIdtr ((IA32_DESCRIPTOR *) &Idtr);
  OrigIdtEntryCount = (UINT16) ((Idtr.Limit + 1) / sizeof (INTERRUPT_GATE_DESCRIPTOR));

  //
  // Get current Code Segment and IDT base address.
  //
  CodeSegment = AsmReadCs ();
  mIdtBase    = (INTERRUPT_GATE_DESCRIPTOR*)((UINTN) NewIdtTable + sizeof (UINTN));

  //
  // Copy original IDT Table & PeiService pointer.
  //
  (**PeiServices).CopyMem ((VOID *) (UINTN) NewIdtTable, (VOID *) (Idtr.Base - sizeof (UINTN)), Idtr.Limit + 1 + sizeof (UINTN));

  //
  // Initialize the NewIdt.
  //
  for (Index = 0; Index < INTERRUPT_VECTOR_NUMBER; Index ++) {
    //
    // Update all IDT entries to use current CS value.
    //
    mIdtBase[Index].SegmentSelector = CodeSegment;
    if (Index < OrigIdtEntryCount) {
      //
      // Skip original IDT entry.
      //
      continue;
    }
    //
    // Set the address of interrupt handler to the rest IDT entry.
    //
    SetInterruptDescriptorTableHandlerAddress (Index);
  }

  //
  // Update IDTR 
  //
  InitializeIdt (
    &(mExternalVectorTable[0]),
    (UINTN *) mIdtBase,
    (UINT16) (sizeof (INTERRUPT_GATE_DESCRIPTOR) * INTERRUPT_VECTOR_NUMBER)
    );

  //
  // Init BSP to receive extINT 
  //
  ProgramXApic(TRUE);
  
  Status = (**PeiServices).InstallPpi (PeiServices, &mCpuArchPpi);

  return Status;
}

