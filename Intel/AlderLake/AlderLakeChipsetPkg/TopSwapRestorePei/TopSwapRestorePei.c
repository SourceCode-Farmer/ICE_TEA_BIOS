/** @file
 PEI Chipset PEIM for top swap bit restore.

;***************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <Library/PeimEntryPoint.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/PchPcrLib.h>
#include <Register/RtcRegs.h>
#include <Register/PchPcrRegs.h>

EFI_STATUS
EFIAPI
PchOnEndOfSiInitRestore (
  IN  EFI_PEI_SERVICES            **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR   *NotifyDescriptor,
  IN  VOID                        *Ppi
  );

STATIC
EFI_PEI_NOTIFY_DESCRIPTOR  mPchOnEndOfSiInitNotifyList[] = {
  {
    (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK  | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
    &gEndOfSiInitPpiGuid,
    PchOnEndOfSiInitRestore
  }
};

EFI_STATUS
EFIAPI
PchOnEndOfSiInitRestore (
  IN  EFI_PEI_SERVICES            **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR   *NotifyDescriptor,
  IN  VOID                        *Ppi
  )
{
  EFI_HOB_GUID_TYPE     *GuidHob;
  BOOLEAN               TopSwapStatus;

  TopSwapStatus = FALSE;

  DEBUG ((DEBUG_INFO, "PchOnEndOfSiInitRestore() Start\n"));

  GuidHob = GetFirstGuidHob (&gChasmfallsTopSwapStatusGuid);
  if (GuidHob != NULL) {
    TopSwapStatus = *((BOOLEAN *) GET_GUID_HOB_DATA (GuidHob));
    DEBUG ((DEBUG_INFO, "Top swap status by HOB: %d\n", TopSwapStatus));
    if (TopSwapStatus) {
      ///
      /// Restore top swap bit in RTC based on data of HOB.
      ///
      PchPcrAndThenOr32 (PID_RTC_HOST, R_RTC_PCR_BUC, (UINT32)~(0), B_RTC_PCR_BUC_TS);
    }
  }

  DEBUG ((DEBUG_INFO, "PchOnEndOfSiInitRestore() End\n"));

  return EFI_SUCCESS;
}

/**
  Module Entry Point to install notify for top swap bit restore.

 @param[in]         FileHandle          Handle of the file being invoked.
 @param[in]         PeiServices         Describes the list of possible PEI Services.

 @retval            EFI_SUCCESS         The operation completed successfully.
 @retval            Others              An unexpected error occurred.
**/
EFI_STATUS
EFIAPI
TopSwapRestoreEntryPoint (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS  Status;

  DEBUG ((DEBUG_INFO, "TopSwapRestoreEntryPoint() Start\n"));

  Status = PeiServicesNotifyPpi (mPchOnEndOfSiInitNotifyList);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "TopSwapRestoreEntryPoint() End\n"));

  return Status;
}
