/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2020 , Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#ifndef _SMBIOS_FVI_GROUP_H_
#define _SMBIOS_FVI_GROUP_H_

#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiLib.h>
#include <Library/HiiLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/CommonSmbiosLib.h>
#include <IndustryStandard/SmBios.h>
#include <Protocol/Smbios.h>
#include <Protocol/ExitPmAuth.h>
#include <Protocol/Runtime.h>
#include <Protocol/Smbios.h>
#include <Protocol/MemInfo.h>
#include <Protocol/SaPolicy.h>
#include <Ddr3SpdRegs.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <CommonSmbiosMemory.h>
#include <Guid/EventGroup.h>

//
// Sync with Broadwell 3.7 code base
//
#define SPD_DATA_GUID                   { 0x70a9c11d, 0xf710, 0x42f8, { 0x89, 0xc1, 0xbd, 0xe8, 0x41, 0xdc, 0x9b, 0x45 } }

#define FIRMWARE_VERSION_INFO  "Firmware Version Info"

#define FIRMWARE_VERSION_INFO_LENGTH 21

typedef struct _SMBIOS_HEADER_LINK {
  EFI_SMBIOS_TABLE_HEADER           RecordHeader;
  LIST_ENTRY                        List;
} SMBIOS_HEADER_LINK;

#define FVI_NODE_FROM_LIST(a) BASE_CR (a, SMBIOS_HEADER_LINK, List)


//
// These definitions sync with Kernel @ CommonSmbiosMemory.h for ECP build
//
#ifndef MAX_SOCKETS
#define MAX_SOCKETS 4
#endif
#define MAX_SPD_DATA_SIZE    0x100

/**
  Create SMBIOS Table type - 14 for Firmware Version Info
  This function is referenced from Intel code - SmbiosMiscEntryPoint.c

  @param[in] Event     - A pointer to the Event that triggered the callback.
  @param[in] Context   - A pointer to private data registered with the callback function.
**/
VOID
EFIAPI
AddSmbiosT14Callback (
  IN EFI_EVENT Event,
  IN VOID      *Context
  );
#endif
