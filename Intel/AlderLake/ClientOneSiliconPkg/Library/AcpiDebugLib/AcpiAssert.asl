/** @file
  Source file for ACPI ADBG enable/disable.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Include/AcpiDebug.h>

#if FixedPcdGetBool(PcdAcpiDebugEnableFlag) == 1
Scope (\_SB)
{
  //
  //  Acpi Assert method
  //  Arg0: Post code to publish
  //  Arg1: ACPi debug message to print
  //  On calling the ACPI method, the method outputs postcode data to Port 80 [2 nibbles].
  //  Method prints the debug serial message to serial interface.
  //  A deadloop is implemented within which is a 10 second timer countdown.
  //  Once the timer reaches zero, then ADB6 is printed for one minute before
  //  repeating the timer countdown once again. The serial debug message is printed again
  //  and the last two nibbles of the post code is printed again.
  //
  Method (ASRT, 2)
  {
    Local1 = Arg0 & 0x00FF    // Grabbing the last two hex digits

    While(1) {                // deadloop
      Local0 = 0x9            // This contains the timer value
      P8XH(0,Local1)          // Output post code (LSB) to Port 80h.

      While(Local0 > 0)
      {
        P8XH(1,Local0)        // Output post code (MSB) to port 81h.
        Local0--              // Subtract one

        for (Local2 = 10000, Local2 > 0, Local2--)  // Stall has requirement that argument cannot be greater than 100
        {
          Stall(100);         // wait one second where stall is in microseconds
        }
      }
      P8XH(0,0xB6)       // Output post code (LSB) to Port 80h.
      P8XH(1,0xAD)       // Output post code (MSB) to port 81h.
      Sleep(60000);      // sleep for 1 minute where sleep is in milliseconds
    } //dead loop
  } //method ASRT
} //scope \_SB
#endif
