/** @file
  ACPI Support for PCIe SSD

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

  // Include PciEpSel.asl for PCIe SSD support
  // Input parameters:
  OperationRegion(PCCX,PCI_Config,0x0,16) // PCI Config Space Class Code
  Field(PCCX, ByteAcc, NoLock, Preserve) {
    DVID, 32, // Vendor&Device ID,
    Offset(9),
    PIXX, 8, // Programming Interface
    SCCX, 8, // Sub Class Code
    BCCX, 8, // Base Class Code
  }

  Method(PAHC, Zero, Serialized) // Check if PCIe AHCI Controller
  {
    If(LEqual(BCCX, 0x01)){ // Check Sub Class Code and Base Class Code
      If(LEqual(SCCX, 0x06)){
        If(LEqual(PIXX, 0x01)){
          Return(0x01)
        }
      }
    }
    Return(0x00)
  }

  Method(PNVM, Zero, Serialized) // Check if PCIe NVMe
  {
    If(LEqual(BCCX, 0x01)){ // Check Sub Class Code and Base Class Code
      If(LEqual(SCCX, 0x08)){
        If(LEqual(PIXX, 0x02)){
          Return(0x01)
        }
      }
    }
    Return(0x00)
  }

  //
  // Check if  EP(End Point) is present.
  // Arguments: (0)
  // Return: EP presence status
  //     0->EP is absent; 1->EP is present
  //
  Method(PRES, Zero, Serialized) {
    If(LEqual (DVID, 0xFFFFFFFF)) {
      Return(0)
    } Else {
      Return(1)
    }
  }

  //
  // Check if EP (End Point) is GFX.
  // Arguments: (0)
  // Return:
  //     0->EP is not Gfx; 1->EP is GFX
  //
  Method (ISGX, Zero, Serialized) // Check if PCIe GFX device
  {
    If (LEqual (BCCX, 0x03)){ // Check Base Class Code
      Return (0x01)
    }
    Return (0x00)
  }

  If (CondRefOf (\STD3)) {
    If (LNotEqual (\STD3, 0)) {
      Method (_DSD, 0)
      {
        If (LOr (PAHC (), PNVM ())) {
          Return (
            Package () {
              ToUUID ("5025030F-842F-4AB4-A561-99A5189762D0"),
              // Enable D3 Support for NVMe Storage
              Package () {
                Package (2) {"StorageD3Enable", 1}  // 1 - Enable; 0 - Disable
              }
            }
          )
        } Else {
          Return (
            Package () {
              ToUUID ("5025030F-842F-4AB4-A561-99A5189762D0"),
              Package () {
                Package (2) {"StorageD3Enable", 0}  // 1 - Enable; 0 - Disable
              }
            }
          )
        }
      }
    }
  }

