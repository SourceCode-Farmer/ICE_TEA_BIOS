/** @file
  This library initialize Silicon Policy for PostMemory.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Ppi/SiPolicy.h>
#include <Ppi/PeiSiDefaultPolicy.h>
#include <Ppi/PeiSiPolicyPrint.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/SiPolicyLib.h>

/**
  Performs silicon post-mem policy initialization.

  The returned data must be used as input data for SiliconPolicyDonePostMem (),
  and SiliconPolicyUpdateLib.SiliconPolicyUpdatePostMem ().

  @param[in, out] Policy       Pointer to policy.
  @return the initialized policy.
**/
VOID *
EFIAPI
SiliconPolicyInitPostMem (
  IN OUT VOID *Policy
  )
{
  EFI_STATUS                        Status;
  SI_POLICY_PPI                     *SiPolicyPpi;
  PEI_SI_DEFAULT_POLICY_INIT_PPI    *PeiSiDefaultPolicyInitPpi;
  PEI_SI_POLICY_PRINT_PPI           *PeiSiPolicyPrintPpi;

  DEBUG ((DEBUG_INFO, "Silicon PEI Policy Initialization Start in Post-Memory...\n"));

  ASSERT (Policy == NULL);
  SiPolicyPpi = NULL;
  PeiSiDefaultPolicyInitPpi = NULL;
  PeiSiPolicyPrintPpi = NULL;

  //
  // Locate Policy init PPI to install default silicon policy
  //
  Status = PeiServicesLocatePpi (
             &gSiDefaultPolicyInitPpiGuid,
             0,
             NULL,
             (VOID **) &PeiSiDefaultPolicyInitPpi
             );
  ASSERT_EFI_ERROR (Status);
  if (PeiSiDefaultPolicyInitPpi != NULL) {
    Status = PeiSiDefaultPolicyInitPpi->PeiPolicyInit ();
    ASSERT_EFI_ERROR (Status);
    if (Status == EFI_SUCCESS) {
      Status = PeiServicesLocatePpi (
                 &gSiPolicyPpiGuid,
                 0,
                 NULL,
                 (VOID **) &SiPolicyPpi
                 );
      ASSERT_EFI_ERROR (Status);
    }
  }

  if (SiPolicyPpi == NULL) {
    DEBUG ((DEBUG_ERROR, "Fail to create default policy!\n"));
    return NULL;
  }

  if (PcdGetBool (PcdDumpDefaultSiliconPolicy)) {
    DEBUG ((DEBUG_INFO, "Print Default Silicon Policy...\n"));
    Status = PeiServicesLocatePpi (
               &gSiPolicyPrintPpiGuid,
               0,
               NULL,
               (VOID **) &PeiSiPolicyPrintPpi
               );
    ASSERT_EFI_ERROR (Status);
    if (PeiSiPolicyPrintPpi != NULL) {
      Status = PeiSiPolicyPrintPpi->PeiPolicyPrint ();
      ASSERT_EFI_ERROR (Status);
    }
  }

  return SiPolicyPpi;
}

/**
  The silicon post-mem policy is finalized.
  Silicon code can do initialization based upon the policy data.

  The input Policy must be returned by SiliconPolicyInitPostMem().

  @param[in] Policy       Pointer to policy.
  @retval RETURN_SUCCESS The policy is handled consumed by silicon code.
**/
RETURN_STATUS
EFIAPI
SiliconPolicyDonePostMem (
  IN VOID *Policy
  )
{
  EFI_STATUS                   Status;

  Status = SiInstallPolicyReadyPpi ();
  ASSERT_EFI_ERROR (Status);
  DEBUG ((DEBUG_INFO, "Silicon PEI Policy Initialization Done in Post-Memory\n"));

  return Status;
}
