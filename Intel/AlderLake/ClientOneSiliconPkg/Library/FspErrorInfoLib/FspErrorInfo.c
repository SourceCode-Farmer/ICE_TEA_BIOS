/** @file
  Library to provide service for sending FSP error information to bootloader.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi.h>
#include <Pi/PiMultiPhase.h>
#include <Guid/FspErrorInfo.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/ReportStatusCodeLib.h>
#include <Library/BaseMemoryLib.h>

/**
  Function attempts to send FSP error information to bootloader
  by both FSP_ERROR_INFO_HOB and ReportStatusCode service.

  @param[in] CallerId           - GUID indicates which component is executing.
  @param[in] ErrorType          - GUID indicates what error was encountered.
  @param[in] Status             - EFI_STATUS code for the error.

  @retval EFI_SUCCESS           - The function always return EFI_SUCCESS.
**/
EFI_STATUS
EFIAPI
SendFspErrorInfo (
  IN EFI_GUID CallerId,
  IN EFI_GUID ErrorType,
  IN UINT32   Status
  )
{
  FSP_ERROR_INFO_HOB    *FspErrorInfoHob;
  EFI_HOB_GUID_TYPE     *HobData;
  FSP_ERROR_INFO        FspErrorInfo;

  //
  // Create Guid type HOB
  //
  HobData = BuildGuidHob (&gFspErrorInfoHobGuid, sizeof (FSP_ERROR_INFO_HOB) - sizeof (EFI_HOB_GUID_TYPE));
  if (HobData != NULL) {
    HobData -= 1;
    FspErrorInfoHob = (FSP_ERROR_INFO_HOB *) HobData;
    FspErrorInfoHob->Type = FSP_ERROR_INFO_STATUS_CODE_TYPE;
    FspErrorInfoHob->Value = FSP_ERROR_INFO_STATUS_CODE_VALUE;
    FspErrorInfoHob->Instance = FSP_ERROR_INFO_STATUS_CODE_INSTANCE;
    FspErrorInfoHob->CallerId = CallerId;
    FspErrorInfoHob->ErrorType = ErrorType;
    FspErrorInfoHob->Status = Status;
  }

  //
  // Send Report Status Code
  //
  FspErrorInfo.ErrorType = ErrorType;
  FspErrorInfo.Status = Status;

  REPORT_STATUS_CODE_EX (
    FSP_ERROR_INFO_STATUS_CODE_TYPE,
    FSP_ERROR_INFO_STATUS_CODE_VALUE,
    FSP_ERROR_INFO_STATUS_CODE_INSTANCE,
    &CallerId,
    &gStatusCodeDataTypeFspErrorGuid,
    &FspErrorInfo,
    sizeof (FSP_ERROR_INFO)
    );
  return EFI_SUCCESS;
}

/**
  Function attempts to send FSP error information to bootloader
  by ReportStatusCode service.
  This typically is used by DXE drivers inside FSP which cannot
  create hob.

  @param[in] CallerId           - GUID indicates which component is executing.
  @param[in] ErrorType          - GUID indicates what error was encountered.
  @param[in] Status             - EFI_STATUS code for the error.

  @retval EFI_SUCCESS           - The function always return EFI_SUCCESS.
**/
EFI_STATUS
EFIAPI
SendFspErrorInfoStatusCode (
  IN EFI_GUID   CallerId,
  IN EFI_GUID   ErrorType,
  IN EFI_STATUS Status
  )
{
  FSP_ERROR_INFO        FspErrorInfo;

  //
  // Send Report Status Code
  //
  FspErrorInfo.ErrorType = ErrorType;
  FspErrorInfo.Status = Status;

  REPORT_STATUS_CODE_EX (
    FSP_ERROR_INFO_STATUS_CODE_TYPE,
    FSP_ERROR_INFO_STATUS_CODE_VALUE,
    FSP_ERROR_INFO_STATUS_CODE_INSTANCE,
    &CallerId,
    &gStatusCodeDataTypeFspErrorGuid,
    &FspErrorInfo,
    sizeof (FSP_ERROR_INFO)
    );
  return EFI_SUCCESS;
}

/**
  Function attempts to dump all FSP error information hobs.

  @param[in] HobList            - Pointer to the HOB data structure.

  @retval EFI_SUCCESS           - No FSP_ERROR_INFO_HOB found.
  @retval EFI_DEVICE_ERROR      - At least one FSP_ERROR_INFO_HOB found.
**/
EFI_STATUS
EFIAPI
DumpFspErrorInfo (
  IN VOID *HobList
  )
{
  EFI_STATUS            Status;
  FSP_ERROR_INFO_HOB    *FspErrorInfoHob;
  EFI_HOB_GUID_TYPE     *HobData;

  DEBUG ((DEBUG_ERROR, "Dump FSP_ERROR_INFO_HOB - start:\n"));
  Status = EFI_SUCCESS;
  HobData = GetNextGuidHob (&gFspErrorInfoHobGuid, HobList);

  while (HobData != NULL) {
    FspErrorInfoHob = (FSP_ERROR_INFO_HOB *) HobData;
    DEBUG ((DEBUG_ERROR, "\n"));
    DEBUG ((DEBUG_ERROR, "FspErrorInfoHob->CallerId  = %g\n", (EFI_GUID *) &FspErrorInfoHob->CallerId));
    DEBUG ((DEBUG_ERROR, "FspErrorInfoHob->ErrorType = %g\n", (EFI_GUID *) &FspErrorInfoHob->ErrorType));
    DEBUG ((DEBUG_ERROR, "FspErrorInfoHob->Status    = %r\n", FspErrorInfoHob->Status));
    Status = EFI_DEVICE_ERROR;
    HobData = GET_NEXT_HOB (HobData);
    HobData = GetNextGuidHob (&gFspErrorInfoHobGuid, HobData);
  }
  DEBUG ((DEBUG_ERROR, "\n"));
  DEBUG ((DEBUG_ERROR, "Dump FSP_ERROR_INFO_HOB - done.\n"));
  return Status;
}

/**
  ReportStatusCode worker for FSP Error Information.

  @param  CodeType         Always (EFI_ERROR_CODE | EFI_ERROR_UNRECOVERED)
  @param  Value            Always 0
  @param  Instance         Always 0
  @param  CallerId         This optional parameter may be used to identify the caller.
                           It may be used to identify which internal component of the FSP
                           was executing at the time of the error.
  @param  Data             This data contains FSP error type and status code.

  @retval EFI_SUCCESS      Show error status sent by FSP successfully.
  @retval RETURN_ABORTED   Function skipped as unrelated.

**/
EFI_STATUS
EFIAPI
FspErrorStatusCodeReportWorker (
  IN EFI_STATUS_CODE_TYPE       CodeType,
  IN EFI_STATUS_CODE_VALUE      Value,
  IN UINT32                     Instance,
  IN CONST EFI_GUID             *CallerId,
  IN CONST EFI_STATUS_CODE_DATA *Data OPTIONAL
  )
{
  FSP_ERROR_INFO  *FspErrorInfo;

  if ((CodeType == FSP_ERROR_INFO_STATUS_CODE_TYPE) &&
      (Value == FSP_ERROR_INFO_STATUS_CODE_VALUE) &&
      (Data != NULL) &&
      (CompareGuid (&Data->Type, &gStatusCodeDataTypeFspErrorGuid))) {
    FspErrorInfo = (FSP_ERROR_INFO *) (Data + 1);
    DEBUG ((DEBUG_ERROR, "\nFSP_ERROR signaled by FSP:\n"));
    DEBUG ((DEBUG_ERROR, "  CallerId  = %g\n", (EFI_GUID *) CallerId));
    DEBUG ((DEBUG_ERROR, "  ErrorType = %g\n", (EFI_GUID *) &FspErrorInfo->ErrorType));
    DEBUG ((DEBUG_ERROR, "  Status    = %r\n", FspErrorInfo->Status));
    DEBUG ((DEBUG_ERROR, "\n"));
    return EFI_SUCCESS;
  } else {
    return RETURN_ABORTED;
  }
}
