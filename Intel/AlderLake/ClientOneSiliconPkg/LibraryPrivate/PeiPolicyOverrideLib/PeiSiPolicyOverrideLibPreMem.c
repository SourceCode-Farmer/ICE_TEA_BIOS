/** @file
  Provide polciy override functions in PreMem to enable specific silicon feature.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PeiSiPolicyOverrideLib.h>
#include <Library/PsmiInitLib.h>
#include <Library/PeiPsmiInitFruLib.h>
#include <Library/PeiMeLib.h>
#include <Library/HobLib.h>
#include <DebugTokenDataHob.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/BaseTraceHubInfoFruLib.h>
#include <Library/PeiTraceHubLib.h>
#include <Library/PeiDciSocLib.h>

/*
  Get Debug Token Data and save it in the HOB

  @param[out]  DebugToken       Pointer to Token data

  @retval      TRUE             Debug Token is existed
  @retval      FALSE            Debug Token is not existed
*/
STATIC
BOOLEAN
GetDebugTokenData (
  OUT DEBUG_TOKEN_KNOB_DATA     *DebugToken
  )
{
  EFI_STATUS                     Status;
  DEBUG_TOKEN_DATA_HOB           *DebugTokenDataHob;

  if (IsSimicsEnvironment ()) {
    DEBUG ((DEBUG_INFO, "Skip get debug token for simics\n"));
    return FALSE;
  }
  Status = PeiHeciGetDebugTokenData (DebugToken);
  if (!EFI_ERROR (Status)) {
    //
    // check if StreamingTraceSink is valid or roll it back to setup menu default
    //
    if ((DebugToken->Fields.StreamingTraceSink != SetupMenuDefault) && (DebugToken->Fields.StreamingTraceSink > ForceDisabled)) {
      DEBUG ((DEBUG_ERROR, "Invalid Debug Token setting : StreamingTraceSink = %x, PlatformDebugConsent will not be overriden but comply to setup menu\n", DebugToken->Fields.StreamingTraceSink));
      DebugToken->Fields.StreamingTraceSink = SetupMenuDefault;
    }
    ///
    /// Create HOB for Debug Token Data
    ///
    DebugTokenDataHob = BuildGuidHob (&gDebugTokenDataHobGuid, sizeof (DEBUG_TOKEN_DATA_HOB));
    ASSERT (DebugTokenDataHob != 0);
    if (DebugTokenDataHob != NULL) {
      ///
      /// Initialize default HOB data
      ///
      DebugTokenDataHob->EfiHobGuidType.Name = gDebugTokenDataHobGuid;
      DebugTokenDataHob->DebugTokenData = DebugToken->Data;
    } else {
      DEBUG ((DEBUG_ERROR, "Failed to create Debug Token Data Hob\n"));
    }
    DEBUG ((DEBUG_INFO, "DebugToken Fields\n"));
    DEBUG ((DEBUG_INFO, "ExposeDebugMenu = %x\n", DebugToken->Fields.ExposeDebugMenu));
    DEBUG ((DEBUG_INFO, "StreamingTraceSink = %x\n", DebugToken->Fields.StreamingTraceSink));
    DEBUG ((DEBUG_INFO, "TraceHubForceOn = %x\n", DebugToken->Fields.TraceHubForceOn));
    DEBUG ((DEBUG_INFO, "TraceEnable = %x\n", DebugToken->Fields.TraceEnable));
    DEBUG ((DEBUG_INFO, "JtagC10PgDis = %x\n", DebugToken->Fields.JtagC10PgDis));
    DEBUG ((DEBUG_INFO, "UsbOverCurrentOvrd = %x\n\n", DebugToken->Fields.UsbOverCurrentOvrd));
    return TRUE;
  } else {
    DEBUG ((DEBUG_INFO, "No BIOS payload present in system\n"));
    return FALSE;
  }
}

/**
  Override policies relevant to PlatformDebugConsent to enable debug in PreMem.

  @param[in] SiPreMemPolicyPpi         The Silicon PreMem Policy PPI instance
**/
STATIC
VOID
PlatformDebugConsentEnablePreMem (
  IN  SI_PREMEM_POLICY_PPI             *SiPreMemPolicyPpi
  )
{
  EFI_STATUS                           Status;
  SI_PREMEM_CONFIG                     *SiPreMemConfig;
  PCH_DCI_PREMEM_CONFIG                *DciPreMemConfig;
  CPU_TRACE_HUB_PREMEM_CONFIG          *CpuTraceHubPreMemConfig;
  PCH_TRACE_HUB_PREMEM_CONFIG          *PchTraceHubPreMemConfig;
  CPU_CONFIG_LIB_PREMEM_CONFIG         *CpuConfigLibPreMemConfig;
  PCH_GENERAL_PREMEM_CONFIG            *PchGeneralPreMemConfig;
  DEBUG_TOKEN_KNOB_DATA                DebugToken;
  BOOLEAN                              DebugTokenExist;

  SiPreMemConfig                     = NULL;
  DciPreMemConfig                    = NULL;
  CpuTraceHubPreMemConfig            = NULL;
  PchTraceHubPreMemConfig            = NULL;
  CpuConfigLibPreMemConfig           = NULL;
  PchGeneralPreMemConfig             = NULL;
  DebugTokenExist                    = FALSE;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gSiPreMemConfigGuid, (VOID *) &SiPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gDciPreMemConfigGuid, (VOID *) &DciPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuTraceHubPreMemConfigGuid, (VOID *) &CpuTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gPchTraceHubPreMemConfigGuid, (VOID *) &PchTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuConfigLibPreMemConfigGuid, (VOID *) &CpuConfigLibPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gPchGeneralPreMemConfigGuid, (VOID *) &PchGeneralPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  //
  // Get Debug Token
  //
  DebugTokenExist = GetDebugTokenData (&DebugToken);
  if (DebugTokenExist) {
    if (DebugToken.Fields.JtagC10PgDis == TRUE) {
      CpuConfigLibPreMemConfig->JtagC10PowerGateDisable = TRUE;
    }
    if (DebugToken.Fields.StreamingTraceSink != SetupMenuDefault) {
      switch (DebugToken.Fields.StreamingTraceSink) {
        case EnabledAll:
          SiPreMemConfig->PlatformDebugConsent = ProbeTypeDciOob;
          break;

        case EnabledLowPower:
          SiPreMemConfig->PlatformDebugConsent = ProbeType2WireDciOob;
          break;

        case ForceDisabled:
          SiPreMemConfig->PlatformDebugConsent = ProbeTypeDisabled;
          break;
      }
      DEBUG ((DEBUG_INFO, "Token overrides PlatformDebugConsent to %x\n", SiPreMemConfig->PlatformDebugConsent));
    }
  }

  switch (SiPreMemConfig->PlatformDebugConsent) {
    case ProbeTypeDisabled:
      DciPreMemConfig->DciEn                         = FALSE;
      DciPreMemConfig->DciClkEnable                  = TRUE;
      DciPreMemConfig->KeepEarlyTrace                = FALSE;
      CpuTraceHubPreMemConfig->TraceHub.EnableMode   = TraceHubModeDisabled;
      CpuTraceHubPreMemConfig->TraceHub.MemReg0Size  = TraceBufferNone;
      CpuTraceHubPreMemConfig->TraceHub.MemReg1Size  = TraceBufferNone;
      PchTraceHubPreMemConfig->TraceHub.EnableMode   = TraceHubModeDisabled;
      PchTraceHubPreMemConfig->TraceHub.MemReg0Size  = TraceBufferNone;
      PchTraceHubPreMemConfig->TraceHub.MemReg1Size  = TraceBufferNone;
      PchGeneralPreMemConfig->IoeDebugEn             = FALSE;
      PchGeneralPreMemConfig->PmodeClkEn             = FALSE;
      CpuConfigLibPreMemConfig->InfDfxPwrEnable      = FALSE;
      break;

    case ProbeType2WireDciOob:
      DciPreMemConfig->DciEn                         = TRUE;
      DciPreMemConfig->DciClkEnable                  = FALSE;
      DciPreMemConfig->KeepEarlyTrace                = FALSE;
      CpuTraceHubPreMemConfig->TraceHub.EnableMode   = TraceHubModeDisabled;
      CpuTraceHubPreMemConfig->TraceHub.MemReg0Size  = TraceBufferNone;
      CpuTraceHubPreMemConfig->TraceHub.MemReg1Size  = TraceBufferNone;
      PchTraceHubPreMemConfig->TraceHub.EnableMode   = TraceHubModeDisabled;
      PchTraceHubPreMemConfig->TraceHub.MemReg0Size  = TraceBufferNone;
      PchTraceHubPreMemConfig->TraceHub.MemReg1Size  = TraceBufferNone;
      PchGeneralPreMemConfig->IoeDebugEn             = FALSE;
      PchGeneralPreMemConfig->PmodeClkEn             = FALSE;
      CpuConfigLibPreMemConfig->InfDfxPwrEnable      = TRUE;
      break;

    case ProbeTypeDciOob:
      DciPreMemConfig->DciEn                         = TRUE;
      DciPreMemConfig->DciClkEnable                  = TRUE;
      DciPreMemConfig->KeepEarlyTrace                = TRUE;
      CpuTraceHubPreMemConfig->TraceHub.EnableMode   = TraceHubModeHostDebugger;
      CpuTraceHubPreMemConfig->TraceHub.MemReg0Size  = TraceBuffer8M;
      CpuTraceHubPreMemConfig->TraceHub.MemReg1Size  = TraceBuffer8M;
      PchTraceHubPreMemConfig->TraceHub.EnableMode   = TraceHubModeHostDebugger;
      PchTraceHubPreMemConfig->TraceHub.MemReg0Size  = TraceBuffer8M;
      PchTraceHubPreMemConfig->TraceHub.MemReg1Size  = TraceBuffer8M;
      PchGeneralPreMemConfig->IoeDebugEn             = TRUE;
      PchGeneralPreMemConfig->PmodeClkEn             = TRUE;
      CpuConfigLibPreMemConfig->InfDfxPwrEnable      = TRUE;
      break;

    default:
      break;
  }

  if ((DebugTokenExist && (DebugToken.Fields.TraceHubForceOn == TRUE)) || IsDebuggerInUse (PchTraceHub) || IsProbeConnectedOverride ()) {
    PchTraceHubPreMemConfig->TraceHub.EnableMode     = TraceHubModeHostDebugger;
    PchTraceHubPreMemConfig->TraceHub.MemReg0Size    = TraceBuffer8M;
    PchTraceHubPreMemConfig->TraceHub.MemReg1Size    = TraceBuffer8M;
  }

  //
  // When probe override is supported and connected, BIOS does not configure KET, which is done by DTS tool.
  //
  if (IsProbeConnectedOverride ()) {
    DciPreMemConfig->KeepEarlyTrace = 2;
  }
  if (!IsCpuTraceHubSupport ()) {
    CpuTraceHubPreMemConfig->TraceHub.EnableMode   = TraceHubModeDisabled;
    CpuTraceHubPreMemConfig->TraceHub.MemReg0Size  = TraceBufferNone;
    CpuTraceHubPreMemConfig->TraceHub.MemReg1Size  = TraceBufferNone;
  } else if (IsDebuggerInUse (CpuTraceHub)) {
    CpuTraceHubPreMemConfig->TraceHub.EnableMode   = TraceHubModeHostDebugger;
    CpuTraceHubPreMemConfig->TraceHub.MemReg0Size  = TraceBuffer8M;
    CpuTraceHubPreMemConfig->TraceHub.MemReg1Size  = TraceBuffer8M;
  }

  DEBUG ((DEBUG_INFO, "SiPreMemConfig->PlatformDebugConsent = %x\n", SiPreMemConfig->PlatformDebugConsent));
  DEBUG ((DEBUG_INFO, "DciPreMemConfig->DciEn = %x\n", DciPreMemConfig->DciEn));
  DEBUG ((DEBUG_INFO, "DciPreMemConfig->DciClkEnable = %x\n", DciPreMemConfig->DciClkEnable));
  DEBUG ((DEBUG_INFO, "DciPreMemConfig->KeepEarlyTrace = %x\n", DciPreMemConfig->KeepEarlyTrace));
  DEBUG ((DEBUG_INFO, "CpuTraceHubPreMemConfig->TraceHub.EnableMode = %x\n",  CpuTraceHubPreMemConfig->TraceHub.EnableMode));
  DEBUG ((DEBUG_INFO, "CpuTraceHubPreMemConfig->TraceHub.MemReg0Size = %x\n", CpuTraceHubPreMemConfig->TraceHub.MemReg0Size));
  DEBUG ((DEBUG_INFO, "CpuTraceHubPreMemConfig->TraceHub.MemReg1Size = %x\n", CpuTraceHubPreMemConfig->TraceHub.MemReg1Size));
  DEBUG ((DEBUG_INFO, "PchTraceHubPreMemConfig->TraceHub.EnableMode = %x\n",  PchTraceHubPreMemConfig->TraceHub.EnableMode));
  DEBUG ((DEBUG_INFO, "PchTraceHubPreMemConfig->TraceHub.MemReg0Size = %x\n", PchTraceHubPreMemConfig->TraceHub.MemReg0Size));
  DEBUG ((DEBUG_INFO, "PchTraceHubPreMemConfig->TraceHub.MemReg1Size = %x\n", PchTraceHubPreMemConfig->TraceHub.MemReg1Size));
  DEBUG ((DEBUG_INFO, "CpuConfigLibPreMemConfig->InfDfxPwrEnable = %x\n", CpuConfigLibPreMemConfig->InfDfxPwrEnable));
  DEBUG ((DEBUG_INFO, "PchGeneralPreMemConfig->IoeDebugEn = %x\n", PchGeneralPreMemConfig->IoeDebugEn));
  DEBUG ((DEBUG_INFO, "PchGeneralPreMemConfig->PmodeClkEn = %x\n", PchGeneralPreMemConfig->PmodeClkEn));
}


/**
  Policy override in PreMem phase

  @param[in] SiPreMemPolicyPpi         The Silicon PreMem Policy PPI instance
**/
VOID
PeiSiPolicyOverridePreMem (
  IN  SI_PREMEM_POLICY_PPI             *SiPreMemPolicyPpi
  )
{
  DEBUG ((DEBUG_INFO, "PeiSiPolicyOverridePreMem ()\n"));
  PlatformDebugConsentEnablePreMem (SiPreMemPolicyPpi);
  PsmiTraceHubPolicyOverride (SiPreMemPolicyPpi);
  TraceHubDataHobInit (SiPreMemPolicyPpi);
}