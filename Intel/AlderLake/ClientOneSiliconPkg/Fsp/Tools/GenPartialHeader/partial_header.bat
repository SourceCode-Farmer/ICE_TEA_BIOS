@REM @file
@REM
@REM @copyright
@REM INTEL CONFIDENTIAL
@REM  Copyright (c) 2019 - 2020 Intel Corporation. All rights reserved
@REM  This software and associated documentation (if any) is furnished
@REM  under a license and may only be used or copied in accordance
@REM  with the terms of the license. Except as permitted by the
@REM  license, no part of this software or documentation may be
@REM  reproduced, stored in a retrieval system, or transmitted in any
@REM  form or by any means without the express written consent of
@REM  Intel Corporation.
@REM  This file contains a 'Sample Driver' and is licensed as such
@REM  under the terms of your license agreement with Intel or your
@REM  vendor. This file may be modified by the user, subject to
@REM  the additional terms of the license agreement.
@REM
@REM @par Specification Reference:
@REM

@echo off
set EDIR=ErrorHeader
set PDIR=PartialHeader
set FSPFILE=fsp_error_log.txt
py -3 %~dp0\partial_header.py %1\FspmUpd.h %2\FSPM.txt Fspm
py -3 %~dp0\partial_header.py %1\FspsUpd.h %2\FSPS.txt Fsps

py -3 %~dp0\name_change.py newFspmUPD.h Fspm
py -3 %~dp0\name_change.py newFspsUPD.h Fsps

py -3 %~dp0\Final.py NCFspmUpd.h %2\FSPM.txt fspm
py -3 %~dp0\Final.py NCFspsUpd.h %2\FSPS.txt fsps

if exist %FSPFILE% (
  DEL /F/Q/S fsp_error_log.txt > NUL

  if not exist %EDIR% mkdir %EDIR%

  copy /Y FinalFspmUpd.h %EDIR%\FspmUpd.h > NUL
  copy /Y FinalFspsUpd.h %EDIR%\FspsUpd.h > NUL

  DEL /F/Q/S newFspmUPD.h newFspsUPD.h NCFspmUPD.h NCFspsUPD.h FinalFspsUpd.h FinalFspmUpd.h Fspm_name_change_list.txt Fsps_name_change_list.txt > NUL
  @echo on
  echo "Error: Please check 'fspm_error.log/fsps_error.log' for more details"
  @echo off
  exit /b 1

) else (
  if not exist %PDIR% mkdir %PDIR%

  copy /Y FinalFspmUpd.h %PDIR%\FspmUpd.h > NUL
  copy /Y FinalFspsUpd.h %PDIR%\FspsUpd.h > NUL

  DEL /F/Q/S newFspmUPD.h newFspsUPD.h NCFspmUPD.h NCFspsUPD.h FinalFspsUpd.h FinalFspmUpd.h Fspm_name_change_list.txt Fsps_name_change_list.txt > NUL
  exit /b 0
)
