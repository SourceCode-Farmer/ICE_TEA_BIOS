## @file
# @copyright
#  Copyright (c) 2008 - 2021 Intel Corporation All rights reserved
#  This software and associated documentation (if any) is furnished
#  under a license and may only be used or copied in accordance
#  with the terms of the license. Except as permitted by such
#  license, no part of this software or documentation may be
#  reproduced, stored in a retrieval system, or transmitted in any
#  form or by any means without the express written consent of
#  Intel Corporation.
#  This file contains an 'Intel Peripheral Driver' and is
#  licensed for Intel CPUs and chipsets under the terms of your
#  license agreement with Intel or your vendor.  This file may
#  be modified by the user, subject to additional terms of the
#  license agreement
##
#!/bin/sh

if [ -z "${WORKSPACE}" ]; then
export WORKSPACE=`pwd`
fi

if [ -z "${WORKSPACE_CORE}" ]; then
export WORKSPACE_CORE=$WORKSPACE/Edk2
fi

if [ -z "${WORKSPACE_COMMON}" ]; then
export WORKSPACE_COMMON=$WORKSPACE/Intel
fi

if [ -z "${WORKSPACE_SILICON}" ]; then
export WORKSPACE_SILICON=$WORKSPACE/Intel
fi


export EdkSetup=$WORKSPACE_CORE/edksetup.sh
export EDK_TOOLS_PATH=$WORKSPACE_CORE/BaseTools
export NOTIMESTAMP=
#
# Print usage and exit
#
function USAGE()
{

  echo  "=========================================================================================================================================="
  echo  "=    Supported platform list:                                                                                                            ="
  echo  "=    All                                                                                                                                 ="
  echo  "=========================================================================================================================================="
  echo  "=========================================================================================================================================="
  echo  "=    Supported Compiler list:                                                                                                            ="
  echo  "=    GCC                                                                                                                                 ="
  echo  "=    CLANG                                                                                                                               ="
  echo  "=========================================================================================================================================="
  echo  "=========================================================================================================================================="
  echo  "= Usage:                                                                                                                                 ="
  echo  "=    BuildFsp.sh [package name] [platform name ^optional] [Compiler name] [-h|-?|-r32|-tr32|-d32|-clean] [-header] [chksize threshold]   ="
  echo  "=========================================================================================================================================="
  echo  "=========================================================================================================================================="
  echo  "= Example build CoffeeLake form CannonLakeFspPkg:                                                                                        ="
  echo  "=    ./Intel/ClientOneSiliconPkg/Fsp/BuildFsp.sh CannonLake CoffeeLake GCC -d32                                                          ="
  echo  "= Example build CannonLake form CannonLakeFspPkg:                                                                                        ="
  echo  "=    ./Intel/ClientOneSiliconPkg/Fsp/BuildFsp.sh CannonLake GCC -d32                                                                     ="
  echo  "=========================================================================================================================================="
  echo  "= By default BuildFsp assumes below paths when executing:                                                                                ="
  echo  "=   /Edk2/                 : FSP required open source packages                                                                           ="
  echo  "=   /Intel/                : Rest of the FSP required packages                                                                           ="
  echo  "=   /Python27              : Should be installed for supporting FSP build script                                                         ="
  echo  "=   /nasm                  : Should be installed for building FSP                                                                        ="
  echo  "=========================================================================================================================================="
  echo
  exit 1
}

CLEAN_BUILD=FALSE
EXT_BUILD_FLAGS_BACKUP=$EXT_BUILD_FLAGS
for ((i=2 ; i <= $# ; i++)); do
  if [ "${!i}" = "notimestamp" ]; then
    export NOTIMESTAMP=1
  elif [ "${!i}" = "chksize" ]; then
    i=`expr $i + 1`
    FV_SPARE_SPACE_THRESHOLD=`expr ${!i} + 0`
    if [ "$FV_SPARE_SPACE_THRESHOLD" != "" ]; then
      if [ $FV_SPARE_SPACE_THRESHOLD -gt 0 ]; then
        export EXT_BUILD_FLAGS="$EXT_BUILD_FLAGS -D FV_SPARE_SPACE_THRESHOLD=$FV_SPARE_SPACE_THRESHOLD"
        continue
      fi
    fi
    echo "Invalid threshold of chksize: \"${!i}\""
    echo
    USAGE
  elif [ "${!i}" = "-clean" ]; then
    CLEAN_BUILD=TRUE
    BUILD_ARGS="$BUILD_ARGS ${!i}"
  else
    BUILD_ARGS="$BUILD_ARGS ${!i}"
  fi
done

if [ $1 = "-?" ]; then
  USAGE
fi
if [ $1 = "-h" ]; then
  USAGE
fi

if [ "$CLEAN_BUILD" = "FALSE" ]; then
  if [ ! -d $WORKSPACE/Conf ]
  then
     mkdir $WORKSPACE/Conf
  fi
  . $EdkSetup BaseTools
fi

FSP_PKG_NAME=$1FspPkg

#
# Is FSP package exist
#
if [ -e $WORKSPACE_SILICON/$FSP_PKG_NAME/BuildFv.sh ]; then
  echo  "The current FSP Package is : $FSP_PKG_NAME"
else
  echo
  echo  "Unsupported FSP Package !!"
  echo
  USAGE
  exit 1
fi

cd $WORKSPACE_SILICON/$FSP_PKG_NAME
echo BUILD_ARGS $BUILD_ARGS
echo EXT_BUILD_FLAGS $EXT_BUILD_FLAGS
bash ./BuildFv.sh $BUILD_ARGS
if [ $? -ne 0 ]
then
 export EXT_BUILD_FLAGS=$EXT_BUILD_FLAGS_BACKUP
 exit 1
fi

export EXT_BUILD_FLAGS=$EXT_BUILD_FLAGS_BACKUP
echo
