;; @file
;  This file implements FspDebugServicePei
;
; @copyright
;  INTEL CONFIDENTIAL
;  Copyright 2020 Intel Corporation.
;
;  The source code contained or described herein and all documents related to the
;  source code ("Material") are owned by Intel Corporation or its suppliers or
;  licensors. Title to the Material remains with Intel Corporation or its suppliers
;  and licensors. The Material may contain trade secrets and proprietary and
;  confidential information of Intel Corporation and its suppliers and licensors,
;  and is protected by worldwide copyright and trade secret laws and treaty
;  provisions. No part of the Material may be used, copied, reproduced, modified,
;  published, uploaded, posted, transmitted, distributed, or disclosed in any way
;  without Intel's prior express written permission.
;
;  No license under any patent, copyright, trade secret or other intellectual
;  property right is granted to or conferred upon you by disclosure or delivery
;  of the Materials, either expressly, by implication, inducement, estoppel or
;  otherwise. Any license under such intellectual property rights must be
;  express and approved by Intel in writing.
;
;  Unless otherwise agreed by Intel in writing, you may not remove or alter
;  this notice or any other notice embedded in Materials by Intel or
;  Intel's suppliers or licensors in any way.
;
;  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
;  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
;  the terms of your license agreement with Intel or your vendor. This file may
;  be modified by the user, subject to additional terms of the license agreement.
;
; @par Specification Reference:
;;

extern   ASM_PFX(SwapStack)
extern   ASM_PFX(GetFspGlobalDataPointer)

FSP_DATA_CORE_STACK_OFFSET        EQU   8     ; This offset need sync with FSP_GLOBAL_DATA
FSP_DATA_FUNC_PARAMETER_OFFSET    EQU   64h   ; This offset need sync with FSP_GLOBAL_DATA
SOFTWARE_PEI_MODULE               EQU   3030000h
DEBUG_CODE                        EQU   3h

struc EventHanlderParameter
  .FspEventHandler:       resd  1
  .StatusCodeType:        resd  1
  .StatusCodeValue:       resd  1
  .Instance:              resd  1
  .CallerId:              resd  1
  .Data:                  resd  1
  .size:
endstruc

struc DebugHanlderParameter
  .FspDebugHandler:       resd  1
  .Buffer:                resd  1
  .BufferSize:            resd  1
  .size:
endstruc

%macro P2L_SWITCHSTACK 0
  pushfd
  cli
  pushad
  sub     esp, 8
  sidt    [esp]
  push    esp
  call    ASM_PFX(SwapStack)
  mov     esp, eax
  lidt    [esp]
%endmacro

%macro L2P_SWITCHSTACK 0
  cli
  push    esp
  call    ASM_PFX(SwapStack)
  mov     esp, eax
  lidt    [esp]
  add     esp, 8
  popad
  popfd
%endmacro

%macro SWITCHIDT 0
  pushfd
  cli
  pushad
  sub     esp, 8
  sidt    [esp]
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_CORE_STACK_OFFSET]
  lidt    [eax]
  mov     ecx, DWORD [esp + 4]
  mov     DWORD [eax + 4], ecx
  mov     ecx, DWORD [esp]
  mov     DWORD [eax], ecx
  add     esp, 8
  popad
  popfd
%endmacro

%macro CALL_DEBUG_EVENT_HANDLER 0
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_FUNC_PARAMETER_OFFSET]
  push    DWORD [eax + EventHanlderParameter.Data]
  push    DWORD [eax + EventHanlderParameter.CallerId]
  push    DWORD [eax + EventHanlderParameter.Instance]
  push    DWORD SOFTWARE_PEI_MODULE
  push    DWORD DEBUG_CODE
  call    DWORD [eax + EventHanlderParameter.FspEventHandler]
  add     esp, 14h
%endmacro

%macro CALL_DEBUG_HANDLER 0
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_FUNC_PARAMETER_OFFSET]
  push    DWORD [eax + DebugHanlderParameter.Buffer]
  push    DWORD [eax + DebugHanlderParameter.BufferSize]
  call    DWORD [eax + DebugHanlderParameter.FspDebugHandler]
  add     esp, 8
%endmacro

;------------------------------------------------------------------------------
; VOID
; EFIAPI
; SwitchStackCallDebugEventHandler (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchStackCallDebugEventHandler)
ASM_PFX(SwitchStackCallDebugEventHandler):

  P2L_SWITCHSTACK
  CALL_DEBUG_EVENT_HANDLER
  L2P_SWITCHSTACK
  ret

;------------------------------------------------------------------------------
; VOID
; EFIAPI
; SwitchIdtCallDebugEventHandler (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchIdtCallDebugEventHandler)
ASM_PFX(SwitchIdtCallDebugEventHandler):

  SWITCHIDT
  CALL_DEBUG_EVENT_HANDLER
  SWITCHIDT
  ret

;------------------------------------------------------------------------------
; VOID
; EFIAPI
; SwitchStackCallDebugHandler (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchStackCallDebugHandler)
ASM_PFX(SwitchStackCallDebugHandler):

  P2L_SWITCHSTACK
  CALL_DEBUG_HANDLER
  L2P_SWITCHSTACK
  ret


;------------------------------------------------------------------------------
; VOID
; EFIAPI
; SwitchIdtCallDebugHandler (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchIdtCallDebugHandler)
ASM_PFX(SwitchIdtCallDebugHandler):

  SWITCHIDT
  CALL_DEBUG_HANDLER
  SWITCHIDT
  ret
