;  This file implements FspMpServiceWrapper
;
; @copyright
;  INTEL CONFIDENTIAL
;  Copyright 2020 Intel Corporation.
;
;  The source code contained or described herein and all documents related to the
;  source code ("Material") are owned by Intel Corporation or its suppliers or
;  licensors. Title to the Material remains with Intel Corporation or its suppliers
;  and licensors. The Material may contain trade secrets and proprietary and
;  confidential information of Intel Corporation and its suppliers and licensors,
;  and is protected by worldwide copyright and trade secret laws and treaty
;  provisions. No part of the Material may be used, copied, reproduced, modified,
;  published, uploaded, posted, transmitted, distributed, or disclosed in any way
;  without Intel's prior express written permission.
;
;  No license under any patent, copyright, trade secret or other intellectual
;  property right is granted to or conferred upon you by disclosure or delivery
;  of the Materials, either expressly, by implication, inducement, estoppel or
;  otherwise. Any license under such intellectual property rights must be
;  express and approved by Intel in writing.
;
;  Unless otherwise agreed by Intel in writing, you may not remove or alter
;  this notice or any other notice embedded in Materials by Intel or
;  Intel's suppliers or licensors in any way.
;
;  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
;  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
;  the terms of your license agreement with Intel or your vendor. This file may
;  be modified by the user, subject to additional terms of the license agreement.
;
; @par Specification Reference:
;;

extern   ASM_PFX(SwapStack)
extern   ASM_PFX(GetFspGlobalDataPointer)

FSP_DATA_CORE_STACK_OFFSET        EQU   8     ; This offset need sync with FSP_GLOBAL_DATA
FSP_DATA_FUNC_PARAMETER_OFFSET    EQU   64h   ; This offset need sync with FSP_GLOBAL_DATA

%macro P2L_SWITCHSTACK 0
  pushfd
  cli
  pushad
  sub     esp, 8
  sidt    [esp]
  push    esp
  call    ASM_PFX(SwapStack)
  mov     esp, eax
  lidt    [esp]
%endmacro

%macro L2P_SWITCHSTACK 0
  cli
  push    esp
  call    ASM_PFX(SwapStack)
  mov     esp, eax
  lidt    [esp]
  add     esp, 8
  popad
  popfd
%endmacro

%macro SWITCHIDT 0
  pushfd
  cli
  pushad
  sub     esp, 8
  sidt    [esp]
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_CORE_STACK_OFFSET]
  lidt    [eax]
  mov     ecx, DWORD [esp + 4]
  mov     DWORD [eax + 4], ecx
  mov     ecx, DWORD [esp]
  mov     DWORD [eax], ecx
  add     esp, 8
  popad
  popfd
%endmacro

struc MpServiceParameter
  .CpuMpPpi:          resd 1
  .CpuMpPpiFunction:  resd 1
  .Uintn1:            resd 1
  .Uintn2:            resd 1
  .ProInfo:           resd 1
  .ApPro:             resd 1
  .Void:              resd 1
  .Uint32:            resd 1
  .MpBoolean:         resd 1
  .size:
endstruc

;------------------------------------------------------------------------------
; EFI_STATUS
; EFIAPI
; SwitchStackGetNumberOfProcessors (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchStackGetNumberOfProcessors)
ASM_PFX(SwitchStackGetNumberOfProcessors):

  P2L_SWITCHSTACK
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_FUNC_PARAMETER_OFFSET]
  push    DWORD [eax + MpServiceParameter.Uintn2]
  push    DWORD [eax + MpServiceParameter.Uintn1]
  push    DWORD [eax + MpServiceParameter.CpuMpPpi]
  call    DWORD [eax + MpServiceParameter.CpuMpPpiFunction]
  add     esp, 0ch
  L2P_SWITCHSTACK
  ret

;------------------------------------------------------------------------------
; EFI_STATUS
; EFIAPI
; SwitchStackGetProcessorInfo (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchStackGetProcessorInfo)
ASM_PFX(SwitchStackGetProcessorInfo):

  P2L_SWITCHSTACK
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_FUNC_PARAMETER_OFFSET]
  push    DWORD [eax + MpServiceParameter.ProInfo]
  push    DWORD [eax + MpServiceParameter.Uintn1]
  push    DWORD [eax + MpServiceParameter.CpuMpPpi]
  call    DWORD [eax + MpServiceParameter.CpuMpPpiFunction]
  add     esp, 0ch
  L2P_SWITCHSTACK
  ret

;------------------------------------------------------------------------------
; EFI_STATUS
; EFIAPI
; SwitchStackStartupAllAPs (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchStackStartupAllAPs)
ASM_PFX(SwitchStackStartupAllAPs):

  P2L_SWITCHSTACK
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_FUNC_PARAMETER_OFFSET]
  push    DWORD [eax + MpServiceParameter.Void]
  push    DWORD [eax + MpServiceParameter.Uintn1]
  push    DWORD [eax + MpServiceParameter.MpBoolean]
  push    DWORD [eax + MpServiceParameter.ApPro]
  push    DWORD [eax + MpServiceParameter.CpuMpPpi]
  call    DWORD [eax + MpServiceParameter.CpuMpPpiFunction]
  add     esp, 14h
  L2P_SWITCHSTACK
  ret

;------------------------------------------------------------------------------
; EFI_STATUS
; EFIAPI
; SwitchStackStartupThisAP (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchStackStartupThisAP)
ASM_PFX(SwitchStackStartupThisAP):

  P2L_SWITCHSTACK
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_FUNC_PARAMETER_OFFSET]
  push    DWORD [eax + MpServiceParameter.Void]
  push    DWORD [eax + MpServiceParameter.Uintn2]
  push    DWORD [eax + MpServiceParameter.Uintn1]
  push    DWORD [eax + MpServiceParameter.ApPro]
  push    DWORD [eax + MpServiceParameter.CpuMpPpi]
  call    DWORD [eax + MpServiceParameter.CpuMpPpiFunction]
  add     esp, 14h
  L2P_SWITCHSTACK
  ret

;------------------------------------------------------------------------------
; EFI_STATUS
; EFIAPI
; SwitchStackSwitchBSP (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchStackSwitchBSP)
ASM_PFX(SwitchStackSwitchBSP):

  P2L_SWITCHSTACK
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_FUNC_PARAMETER_OFFSET]
  push    DWORD [eax + MpServiceParameter.MpBoolean]
  push    DWORD [eax + MpServiceParameter.Uintn1]
  push    DWORD [eax + MpServiceParameter.CpuMpPpi]
  call    DWORD [eax + MpServiceParameter.CpuMpPpiFunction]
  add     esp, 0ch
  L2P_SWITCHSTACK
  ret

;------------------------------------------------------------------------------
; EFI_STATUS
; EFIAPI
; SwitchStackEnableDisableAP (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchStackEnableDisableAP)
ASM_PFX(SwitchStackEnableDisableAP):

  P2L_SWITCHSTACK
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_FUNC_PARAMETER_OFFSET]
  push    DWORD [eax + MpServiceParameter.Uint32]
  push    DWORD [eax + MpServiceParameter.MpBoolean]
  push    DWORD [eax + MpServiceParameter.Uintn1]
  push    DWORD [eax + MpServiceParameter.CpuMpPpi]
  call    DWORD [eax + MpServiceParameter.CpuMpPpiFunction]
  add     esp, 10h
  L2P_SWITCHSTACK
  ret

;------------------------------------------------------------------------------
; EFI_STATUS
; EFIAPI
; SwitchStackWhoAmI (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchStackWhoAmI)
ASM_PFX(SwitchStackWhoAmI):

  P2L_SWITCHSTACK
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_FUNC_PARAMETER_OFFSET]
  push    DWORD [eax + MpServiceParameter.Uintn1]
  push    DWORD [eax + MpServiceParameter.CpuMpPpi]
  call    DWORD [eax + MpServiceParameter.CpuMpPpiFunction]
  add     esp, 08h
  L2P_SWITCHSTACK
  ret

;------------------------------------------------------------------------------
; EFI_STATUS
; EFIAPI
; SwitchStackStartupAllCPUs (
;   VOID
;   )
;------------------------------------------------------------------------------
global ASM_PFX(SwitchStackStartupAllCPUs)
ASM_PFX(SwitchStackStartupAllCPUs):

  P2L_SWITCHSTACK
  call    ASM_PFX(GetFspGlobalDataPointer)
  mov     eax, [eax + FSP_DATA_FUNC_PARAMETER_OFFSET]
  push    DWORD [eax + MpServiceParameter.Void]
  push    DWORD [eax + MpServiceParameter.Uintn1]
  push    DWORD [eax + MpServiceParameter.ApPro]
  push    DWORD [eax + MpServiceParameter.CpuMpPpi]
  call    DWORD [eax + MpServiceParameter.CpuMpPpiFunction]
  add     esp, 10h
  L2P_SWITCHSTACK
  ret
