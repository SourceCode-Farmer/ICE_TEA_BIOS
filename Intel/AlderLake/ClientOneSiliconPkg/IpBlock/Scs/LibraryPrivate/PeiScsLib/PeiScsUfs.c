/** @file
  Initializes UFS host controller located on SCS Iosf2Ocp bridge.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Register/ScsUfsRegs.h>
#include "PeiScsInitInternal.h"

/**
  Configure Ufs controller

  @param[in] PciBaseAddress  PCI config base address of the controller
  @param[in] MmioBase        MMIO base address
**/
STATIC
VOID
ScsUfsInitMmioRegisters (
  IN UINT64  PciBaseAddress,
  IN UINTN   MmioBase
  )
{
  ScsControllerEnableMmio (PciBaseAddress, MmioBase);
  //
  // Configure UFS REF_CLK
  //
  MmioWrite32 (MmioBase + R_SCS_MEM_UFS_REF_CLK, V_SCS_MEM_UFS_REF_CLK);
  ScsControllerDisableMmio (PciBaseAddress);
}

/**
  Enables UFS controller.

  @param[in] ScsUfsHandle  Handle.
**/
STATIC
VOID
ScsUfsEnable (
  IN SCS_UFS_HANDLE  *ScsUfsHandle
  )
{
  DEBUG ((DEBUG_INFO, "Enabling UFS\n"));

  Iosf2OcpDisableBar1 (ScsUfsHandle->Controller.Iosf2OcpPort);
  ScsUfsHandle->Callbacks.Bar1Disable (ScsUfsHandle);
  MmpInit (ScsUfsHandle->Controller.Mmp);
  if (!ScsUfsHandle->SocConfig.IsBootMedium) {
    Iosf2OcpEnableUfs (ScsUfsHandle->Controller.Iosf2OcpPort, ScsUfsHandle->SocConfig.NumOfLanes);
    ScsUfsInitMmioRegisters (ScsUfsHandle->Controller.PciCfgBase, ScsUfsHandle->Controller.MmioBase);
  }
  if (ScsUfsHandle->Config->InlineEncryption) {
    Iosf2OcpEnableUfsInlineEncryption (ScsUfsHandle->Controller.Iosf2OcpPort);
  }
  Iosf2OcpConfigureInterrupts (
    ScsUfsHandle->Controller.Iosf2OcpPort,
    ScsUfsHandle->SocConfig.IntPin,
    ScsUfsHandle->SocConfig.Irq
    );
}

/**
  Disable UFS controller.

  @param[in] ScsUfsHandle  Handle.
**/
STATIC
VOID
ScsUfsDisable (
  IN SCS_UFS_HANDLE  *ScsUfsHandle
  )
{
  DEBUG ((DEBUG_INFO, "Disabling UFS\n"));

  Iosf2OcpDisableBar1 (ScsUfsHandle->Controller.Iosf2OcpPort);
  ScsUfsHandle->Callbacks.Bar1Disable (ScsUfsHandle);
  PciSegmentOr16 (
    ScsUfsHandle->Controller.PciCfgBase + R_SCS_CFG_PG_CONFIG,
    (B_SCS_CFG_PG_CONFIG_SE| B_SCS_CFG_PG_CONFIG_PGE | B_SCS_CFG_PG_CONFIG_I3E)
    );
  ScsControllerPutToD3 (ScsUfsHandle->Controller.PciCfgBase);
  MmpDisable (ScsUfsHandle->Controller.Mmp);
  Iosf2OcpDisableUfs (ScsUfsHandle->Controller.Iosf2OcpPort);
  ScsUfsHandle->Callbacks.Disable (ScsUfsHandle);
}

/**
  Prints UFS SoC config into debug port.

  @param[in] ScsUfsHandle  UFS handle
**/
VOID
ScsUfsPrintSocConfig (
  IN SCS_UFS_HANDLE  *ScsUfsHandle
  )
{
  DEBUG ((DEBUG_INFO, "Printing UFS SoC config\n"));
  DEBUG ((DEBUG_INFO, "IsBootMedium: %d\n", ScsUfsHandle->SocConfig.IsBootMedium));
  DEBUG ((DEBUG_INFO, "NumOfLanes: %d\n", ScsUfsHandle->SocConfig.NumOfLanes));
  DEBUG ((DEBUG_INFO, "IntPin: %d\n", ScsUfsHandle->SocConfig.IntPin));
  DEBUG ((DEBUG_INFO, "Irq: %d\n", ScsUfsHandle->SocConfig.Irq));
}

/**
  Configure UFS controller in SCS.

  @param[in] ScsUfsHandle  Handle.
**/
VOID
ScsUfsInit (
  IN SCS_UFS_HANDLE  *ScsUfsHandle
  )
{
  DEBUG ((DEBUG_INFO, "UFS init start\n"));

  ScsUfsPrintSocConfig (ScsUfsHandle);

  if (ScsUfsHandle->SocConfig.NumOfLanes == 0) {
    ScsUfsHandle->UfsInfo->Supported = FALSE;
  } else {
    ScsUfsHandle->UfsInfo->Supported = TRUE;
  }

  if ((ScsUfsHandle->Config->Enable || ScsUfsHandle->SocConfig.IsBootMedium) &&
      (ScsUfsHandle->SocConfig.NumOfLanes != 0)) {
    ScsUfsEnable (
      ScsUfsHandle
      );
  } else {
    ScsUfsDisable (ScsUfsHandle);
  }
  DEBUG ((DEBUG_INFO, "UFS init finished\n"));
}

