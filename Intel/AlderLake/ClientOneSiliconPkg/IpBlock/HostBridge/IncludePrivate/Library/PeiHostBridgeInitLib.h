/** @file
  PeiHostBridgeInitLib header file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _HOST_BRIDGE_INIT_H_
#define _HOST_BRIDGE_INIT_H_

#include <HostBridgeDataHob.h>
#include <MrcInterface.h>

/**
  Programs HostBridge Bars

**/
VOID
ProgramHostBridgeBars (
  VOID
  );

/**
  Determine MMIO Size for Dynamic Tolud

  @param[in] PchPcieMmioLength - Total PCIe MMIO length on all PCH root ports
  @param[in] PegMmioLength     - Total PEG MMIO length on all PEG ports
  @param[in] IGfxMmioLength    - Total IGFX MMIO length
**/
VOID
MmioSizeCalculation (
  IN    UINT32  PchPcieMmioLength,
  IN    UINT32  PegMmioLength,
  IN    UINT32  IGfxMmioLength
  );

/**
  Program IA/GT exclusion region

  @param[in] IMR Base
  @param[in] IMR Limit
**/
VOID
SetIaGtImrExclusion (
  UINT32  ImrBase,
  UINT32  ImrLimit
  );

/**
  Lock IA/GT exclusion region
**/
VOID
SetIaGtImrExclusionLock (
  VOID
  );

/**
  This function program TOLUD register

  @param[in] TouldBase
**/
VOID
ProgramTolud (
  UINT32  ToludBase
  );

/**
  This function program TOUUD register

  @param[in] TouldBase
**/
VOID
ProgramTouud (
  UINT32  TouudBase
  );

/**
  This function program Tom register

  @param[in] TouudBase
**/
VOID
ProgramTom (
  UINT32  TotalMemorySize
  );

/**
  This function program Tseg register

  @param[in] TsegBase
**/
VOID
ProgramTseg (
  UINT32  TsegBase
  );

/**
  This function program BDSM register

  @param[in] BdsmBase
**/
VOID
ProgramBdsm (
  UINT32  BdsmBase
  );

/**
  This function program BGSM register

  @param[in] BgsmBase
**/
VOID
ProgramBgsm (
  UINT32  BgsmBase
  );

/**
  This function program Graphic Control register

  @param[in] GraphicsControlRegister
**/
VOID
ProgramGfxCr (
  UINT32  GraphicsControlRegister
  );

/**
  This function Lock HostBridge Memory Map registers
**/
VOID
LockHostBridgeMemoryMapRegs (
  VOID
  );

/**
  ProgramEdramMode - Disable EDRAM by default and enable it through HW Test Mode policy if needed

  @param[in] HostBridgePeiConfig - Instance of HOST_BRIDGE_PEI_CONFIG
**/
VOID
ProgramEdramMode (
  IN    HOST_BRIDGE_PEI_CONFIG      *HostBridgePeiConfig
  );

/**
  Init and Install Host Bridge Data Hob

  @param[in] HOST_BRIDGE_PREMEM_CONFIG   - Instance of HOST_BRIDGE_PREMEM_CONFIG
  @param[out] HOST_BRIDGE_DATA_HOB       - HOST_BRIDGE_DATA_HOB instance installed by this function

  @retval EFI_SUCCESS
**/
EFI_STATUS
InstallHostBridgeDataHob (
  IN    HOST_BRIDGE_PREMEM_CONFIG   *HostBridgePreMemConfig,
  OUT   HOST_BRIDGE_DATA_HOB        **HostBridgeDataHobOut
  );

/**
  Update HostBridge Hob in PostMem

  @param[in] HostBridgePeiConfig    Instance of HOST_BRIDGE_PEI_CONFIG

  @retval EFI_SUCCESS
**/
EFI_STATUS
UpdateHostBridgeHobPostMem (
  IN    HOST_BRIDGE_PEI_CONFIG      *HostBridgePeiConfig
  );

/**
  Program IA exclusion region

  @param[in] IMR Base
  @param[in] IMR Limit
**/
VOID
SetIaImrExclusion (
  UINT32  ImrBase,
  UINT32  ImrLimit
  );

/**
  Lock IA exclusion region
**/
VOID
SetIaImrExclusionLock (
  VOID
  );

/**
  This function calls into MemoryMap APIs to program memory Map.

  @param[in] MrcData             - The MRC "global data" area.

**/
VOID
HostBridgeMemoryMapInit (
  IN MrcParameters *CONST  MrcData
  );

/**
  This function calls to print memory Map.

  @param[in] MrcData             - The MRC "global data" area.

**/
VOID
PrintMemoryMap (
  IN MrcParameters *CONST  MrcData
  );

/**
  Program DPR lock and EPM bit
  Note : DPR lock and EPM bit programming should always be done after the External Gfx card has been detected and IGD enabled/disabled based on that.
**/
VOID
UpdateDpr (
  VOID
  );

#endif
