/** @file
  This file provides services for PEI HostBridge policy default initialization

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Library/SiConfigBlockLib.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/CpuPlatformLib.h>
#include <Library/HostBridgeInfoLib.h>

/**
  Load Config block default

  @param[in] ConfigBlockPointer         Pointer to config block
**/
VOID
HostBridgeLoadPeiConfigPreMemDefault (
  IN VOID   *ConfigBlockPointer
  )
{
  HOST_BRIDGE_PREMEM_CONFIG   *HostBridgePreMemConfig;

  HostBridgePreMemConfig = ConfigBlockPointer;
  DEBUG ((DEBUG_INFO, "HostBridgePreMemConfig->Header.GuidHob.Name = %g\n", &HostBridgePreMemConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "HostBridgePreMemConfig->Header.GuidHob.Header.HobLength = 0x%x\n", HostBridgePreMemConfig->Header.GuidHob.Header.HobLength));

  HostBridgePreMemConfig->MchBar              = (UINT32) PcdGet64 (PcdMchBaseAddress);
  HostBridgePreMemConfig->DmiBar              = 0xFEDA0000;
  HostBridgePreMemConfig->EpBar               = 0xFEDA1000;
  HostBridgePreMemConfig->EdramBar            = 0xFED80000;
  HostBridgePreMemConfig->RegBar              = PcdGet32 (PcdRegBarBaseAddress);
  HostBridgePreMemConfig->EnableAbove4GBMmio  = 1;

}

/**
  Load Config block default

  @param[in] ConfigBlockPointer         Pointer to config block
**/
VOID
HostBridgeLoadPeiConfigDefault (
  IN VOID   *ConfigBlockPointer
  )
{
  HOST_BRIDGE_PEI_CONFIG   *HostBridgePeiConfig;

  HostBridgePeiConfig = ConfigBlockPointer;
  DEBUG ((DEBUG_INFO, "HostBridgePeiConfig->Header.GuidHob.Name = %g\n", &HostBridgePeiConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "HostBridgePeiConfig->Header.GuidHob.Header.HobLength = 0x%x\n", HostBridgePeiConfig->Header.GuidHob.Header.HobLength));
  HostBridgePeiConfig->EdramTestMode   = 0x2;
  if (IsSaDevice4Enable() == TRUE) {
    HostBridgePeiConfig->Device4Enable = 0x1;
  } else {
    HostBridgePeiConfig->Device4Enable = 0x0;
  }
}


static COMPONENT_BLOCK_ENTRY  mHostBridgeIpBlockPreMem = {
  &gHostBridgePeiPreMemConfigGuid,  sizeof (HOST_BRIDGE_PREMEM_CONFIG), HOST_BRIDGE_PREMEM_CONFIG_REVISION, HostBridgeLoadPeiConfigPreMemDefault
};

static COMPONENT_BLOCK_ENTRY  mHostBridgeIpBlocks = {
  &gHostBridgePeiConfigGuid, sizeof (HOST_BRIDGE_PEI_CONFIG), HOST_BRIDGE_PEI_CONFIG_REVISION, HostBridgeLoadPeiConfigDefault
};

/**
  Get HostBridge PEI PreMem config block table total size.

  @retval     Size of HostBridge PEI PreMem config block table
**/
UINT16
EFIAPI
HostBridgeGetPeiConfigBlockTotalSizePreMem (
  VOID
  )
{
  return mHostBridgeIpBlockPreMem.Size;
}

/**
  Get HostBridge PEI phase config block table total size.

  @retval     Size of HostBridge PEI config block table
**/
UINT16
EFIAPI
HostBridgeGetPeiConfigBlockTotalSize (
  VOID
  )
{
  return mHostBridgeIpBlocks.Size;
}

/**
  HostBridgeAddPeiConfigBlockPreMem add all HostBridge PreMem PEI config block.

  @param[in] ConfigBlockTableAddress    The pointer to add Ip config block

  @retval EFI_SUCCESS                   The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES          Insufficient resources to create buffer
**/
EFI_STATUS
EFIAPI
HostBridgeAddPeiConfigBlockPreMem (
  IN VOID           *ConfigBlockTableAddress
  )
{
  EFI_STATUS  Status;
  Status = AddComponentConfigBlocks (ConfigBlockTableAddress, &mHostBridgeIpBlockPreMem, 1);
  return Status;
}

/**
  HostBridgeAddPeiConfigBlock add all HostBridge PEI config block.

  @param[in] ConfigBlockTableAddress    The pointer to add Ip config block

  @retval EFI_SUCCESS                   The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES          Insufficient resources to create buffer
**/
EFI_STATUS
EFIAPI
HostBridgeAddPeiConfigBlock (
  IN VOID           *ConfigBlockTableAddress
  )
{
  EFI_STATUS  Status;
  Status = AddComponentConfigBlocks (ConfigBlockTableAddress, &mHostBridgeIpBlocks, 1);
  return Status;
}

/**
  Print HostBridge PEI PreMem Config

  @param[in] SiPreMemPolicyPpi            Pointer to a SI_PREMEM_POLICY_PPI
**/
VOID
HostBridgePreMemPrintConfig (
  IN SI_PREMEM_POLICY_PPI         *SiPreMemPolicyPpi
  )
{
  EFI_STATUS                  Status;
  HOST_BRIDGE_PREMEM_CONFIG   *HostBridgePreMemConfig;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gHostBridgePeiPreMemConfigGuid, (VOID *) &HostBridgePreMemConfig);
  ASSERT_EFI_ERROR (Status);
  DEBUG ((DEBUG_INFO, "------------------ HostBridge PreMem Config ------------------\n"));
  DEBUG ((DEBUG_INFO, " Revision : %d\n", HostBridgePreMemConfig->Header.Revision));
  DEBUG ((DEBUG_INFO, "\n MchBar : 0x%x\n", HostBridgePreMemConfig->MchBar));
  DEBUG ((DEBUG_INFO, " DmiBar : 0x%x\n", HostBridgePreMemConfig->DmiBar));
  DEBUG ((DEBUG_INFO, " EpBar : 0x%x\n", HostBridgePreMemConfig->EpBar));
  DEBUG ((DEBUG_INFO, " EdramBar : 0x%x\n", HostBridgePreMemConfig->EdramBar));
  DEBUG ((DEBUG_INFO, " RegBar : 0x%x\n", HostBridgePreMemConfig->RegBar));
  DEBUG ((DEBUG_INFO, " EnableAbove4GBMmio : 0x%x\n", HostBridgePreMemConfig->EnableAbove4GBMmio));
}

/**
  Print HostBridge PEI Config

  @param[in] SiPolicyPpi - Instance of SI_POLICY_PPI
**/
VOID
HostBridgePeiPrintConfig (
  IN  SI_POLICY_PPI     *SiPolicyPpi
  )
{
  EFI_STATUS                  Status;
  HOST_BRIDGE_PEI_CONFIG      *HostBridgePeiConfig;

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gHostBridgePeiConfigGuid, (VOID *) &HostBridgePeiConfig);
  ASSERT_EFI_ERROR (Status);
  DEBUG ((DEBUG_INFO, "------------------ HostBridge Pei Config ------------------\n"));
  DEBUG ((DEBUG_INFO, " Revision : %d\n", HostBridgePeiConfig->Header.Revision));
  DEBUG ((DEBUG_INFO, " Device4Enable : 0x%x\n", HostBridgePeiConfig->Device4Enable));
  DEBUG ((DEBUG_INFO, " SkipPamLock : 0x%x\n", HostBridgePeiConfig->SkipPamLock));
  DEBUG ((DEBUG_INFO, " EdramTestMode : 0x%x\n", HostBridgePeiConfig->EdramTestMode));

}
