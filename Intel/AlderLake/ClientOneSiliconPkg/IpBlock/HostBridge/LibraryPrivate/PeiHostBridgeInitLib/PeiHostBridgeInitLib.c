/** @file
  PEIM Private Library to initialize PeiHostBridgeInitLib registers

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/SiPolicy.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PciSegmentLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Library/HobLib.h>
#include <HostBridgeDataHob.h>
#include <Library/CpuPlatformLib.h>
#include <Library/MsrFruLib.h>
#include <MrcInterface.h>

#define MAX_ADDRESS_32BIT     0xFFFFFFFF

/**
  Determine MMIO Size for Dynamic Tolud

  @param[in] PchPcieMmioLength - Total PCIe MMIO length on all PCH root ports
  @param[in] PegMmioLength     - Total PEG MMIO length on all PEG ports
  @param[in] IGfxMmioLength    - Total IGFX MMIO length
**/
VOID
MmioSizeCalculation (
  IN    UINT32  PchPcieMmioLength,
  IN    UINT32  PegMmioLength,
  IN    UINT32  IGfxMmioLength
  )
{
  HOST_BRIDGE_PREMEM_CONFIG     *HostBridgePreMemConfig;
  SI_PREMEM_POLICY_PPI          *SiPreMemPolicyPpi;
  UINT32                        TotalMmioLength;
  UINT32                        ResMemLimit1;
  UINT32                        AddMem;
  EFI_STATUS                    Status;

  ///
  /// Get policy settings through the SaInitConfigBlock PPI
  ///
  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);
  if ((Status != EFI_SUCCESS) || (SiPreMemPolicyPpi == NULL)) {
    DEBUG ((DEBUG_INFO, "Fail to locate SiPreMemPolicyPpi\n"));
    return;
  }

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gHostBridgePeiPreMemConfigGuid, (VOID *) &HostBridgePreMemConfig);
  ASSERT_EFI_ERROR (Status);


  ///
  /// Determine MMIO Size for Dynamic Tolud
  ///
  TotalMmioLength = PchPcieMmioLength + PegMmioLength + IGfxMmioLength;
  DEBUG ((DEBUG_INFO, "TotalMmioLength:   0x%08X bytes\n", TotalMmioLength));
  if (HostBridgePreMemConfig->MmioSize == 0x00) {
    /**
      Dynamic MmioSize will calculate the value basing on MmioLength found from main consumers. (Video cards)
      Since PCIe and some other Chipset MMIO BAR occupied Top MMIO region, we need to enlarge MmioSize.
      By default 0xE0000000 ~ 0xFFFFFFFF are not available as MMIO resource so add 512MB in all cases.
      HostBridgePreMemConfig->MmioSizeAdjustment will be used later if needed to increase or decrease the final MmioSize.
    **/

    ///
    /// Get PCI Express Base address and calculate the memory occupied on Top MMIO region by that.
    ///
    ResMemLimit1 = PcdGet32 (PcdPciReservedMemLimit);
    if (ResMemLimit1 == 0) {
      ResMemLimit1 = ((UINTN) PcdGet64 (PcdSiPciExpressBaseAddress));
    }

    AddMem = ((MAX_ADDRESS_32BIT - ResMemLimit1) + 1) >> 20;

    ///
    /// if total MMIO need 1.5GB or over
    ///
    if (TotalMmioLength >= 0x60000000) {
      HostBridgePreMemConfig->MmioSize = 0xA00 + (UINT16)AddMem;
    }
    ///
    /// if total MMIO need 1.25GB or over
    ///
    else if (TotalMmioLength >= 0x50000000) {
      HostBridgePreMemConfig->MmioSize = 0x900 + (UINT16)AddMem;
    }
    ///
    /// if total MMIO need 1GB or over
    ///
    else if (TotalMmioLength >= 0x40000000) {
      HostBridgePreMemConfig->MmioSize = 0x800 + (UINT16) AddMem;
    }
    ///
    /// if total MMIO need 728MB~1GB
    ///
    else if (TotalMmioLength >= 0x30000000) {
      HostBridgePreMemConfig->MmioSize = 0x700 + (UINT16) AddMem;
    }
    ///
    /// if total MMIO need 512MB~728MB
    ///
    else if (TotalMmioLength >= 0x20000000) {
      HostBridgePreMemConfig->MmioSize = 0x600 + (UINT16) AddMem;
    }
    ///
    /// if total MMIO need 256MB~512MB
    ///
    else if (TotalMmioLength >= 0x10000000) {
      HostBridgePreMemConfig->MmioSize = 0x500 + (UINT16) AddMem;
    }
    ///
    /// if total MMIO need less than 256MB
    ///
    else {
      HostBridgePreMemConfig->MmioSize = 0x400 + (UINT16) AddMem;
    }
    //
    // Increase or Decrease MMIO size basing on platform requirement: MmioSizeAdjustment and it can be negative or positive.
    // If MmioSize not in reasonable range (Minimal should be larger than 0MB and Maximal 3840MB.), reset to safe default 1GB.
    //
    HostBridgePreMemConfig->MmioSize = (INT32) (HostBridgePreMemConfig->MmioSize) + HostBridgePreMemConfig->MmioSizeAdjustment;
    if ((HostBridgePreMemConfig->MmioSize == 0) || (HostBridgePreMemConfig->MmioSize > 0xF00)) {
      DEBUG ((DEBUG_WARN, "MmioSize is not reasonable, reset to default 1024MB!\n"));
      HostBridgePreMemConfig->MmioSize = 0x400;
    }
  }
}

/**
  This function program TOLUD register

  @param[in] TouldBase
**/
VOID
ProgramTolud (
  UINT32  ToludBase
  )
{
  UINT64    McBaseAddress;

  McBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);

  DEBUG ((DEBUG_INFO, "ToludBase = %x\n", ToludBase));

  PciSegmentWrite32 (McBaseAddress + R_SA_TOLUD, (ToludBase << N_SA_TOLUD_TOLUD_OFFSET) );
}

/**
  This function program TOUUD register

  @param[in] TouudBase
**/
VOID
ProgramTouud (
  UINT32  TouudBase
  )
{
  UINT64    Touud;
  UINT64    McBaseAddress;

  McBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);

  DEBUG ((DEBUG_INFO, "TouudBase = %x\n", TouudBase));

  Touud = LShiftU64 (TouudBase, N_SA_TOUUD_TOUUD_OFFSET);
  PciSegmentWrite32 (McBaseAddress +  R_SA_TOUUD    , (UINT32)Touud);
  PciSegmentWrite32 (McBaseAddress +  R_SA_TOUUD + 4, (UINT32)RShiftU64 (Touud, 32));
}

/**
  This function program Tom register

  @param[in] TouudBase
**/
VOID
ProgramTom (
  UINT32  TotalMemorySize
  )
{
  UINT64    Tom;
  UINT64    McBaseAddress;

  McBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  DEBUG ((DEBUG_INFO, "TotalMemorySize = %x\n", TotalMemorySize));

  Tom = LShiftU64 (TotalMemorySize, N_SA_TOM_TOM_OFFSET);
  PciSegmentWrite32 (McBaseAddress +  R_SA_TOM    , (UINT32)Tom);
  PciSegmentWrite32 (McBaseAddress +  R_SA_TOM + 4, (UINT32)RShiftU64 (Tom, 32));
}

/**
  This function program Tseg register

  @param[in] TsegBase
**/
VOID
ProgramTseg (
  UINT32  TsegBase
  )
{
  UINT64    McBaseAddress;

  McBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);

  DEBUG ((DEBUG_INFO, "TsegBase = %x\n", TsegBase));

  PciSegmentWrite32 (McBaseAddress + R_SA_TSEGMB, (TsegBase << N_SA_TSEGMB_TSEGMB_OFFSET) );
}

/**
  This function program BDSM register

  @param[in] BdsmBase
**/
VOID
ProgramBdsm (
  UINT32  BdsmBase
  )
{
  UINT64    McBaseAddress;

  McBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);

  DEBUG ((DEBUG_INFO, "BdsmBase = %x\n", BdsmBase));

  PciSegmentWrite32 (McBaseAddress + R_SA_BDSM, (BdsmBase << N_SA_BDSM_BDSM_OFFSET) );
}

/**
  This function program BGSM register

  @param[in] BgsmBase
**/
VOID
ProgramBgsm (
  UINT32  BgsmBase
  )
{
  UINT64    McBaseAddress;

  McBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);

  DEBUG ((DEBUG_INFO, "BgsmBase = %x\n", BgsmBase));

  PciSegmentWrite32 (McBaseAddress + R_SA_BGSM, (BgsmBase << N_SA_BGSM_BGSM_OFFSET));
}

/**
  This function program Graphic Control register

  @param[in] GraphicsControlRegister
**/
VOID
ProgramGfxCr (
  UINT32  GraphicsControlRegister
  )
{
  UINT64    McBaseAddress;

  McBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  DEBUG ((DEBUG_INFO, "GraphicsControlRegister = %x\n", GraphicsControlRegister));

  PciSegmentWrite32 (McBaseAddress + R_SA_GGC, GraphicsControlRegister);
}

/**
  This function Lock Memory Map registers
**/
VOID
LockHostBridgeMemoryMapRegs (
  VOID
  )
{
  UINT64    McBaseAddress;

  McBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);

  //
  // Lock TOLUD
  //
  PciSegmentOr32 (McBaseAddress + R_SA_TOLUD, BIT0);
  //
  // Lock TOUUD
  //
  PciSegmentOr32 (McBaseAddress + R_SA_TOUUD, BIT0);
  //
  // Lock Tom
  //
  PciSegmentOr32 (McBaseAddress + R_SA_TOM, BIT0);
  //
  // Lock Tseg
  //
  PciSegmentOr32 (McBaseAddress + R_SA_TSEGMB, BIT0);
  //
  // Lock Bdsm
  //
  PciSegmentOr32 (McBaseAddress + R_SA_BDSM, BIT0);
  //
  // Lock Bgsm
  //
  PciSegmentOr32 (McBaseAddress + R_SA_BGSM, BIT0);
}


/**
  Init and Install Host Bridge Data Hob

  @param[in] HOST_BRIDGE_PREMEM_CONFIG   - Instance of HOST_BRIDGE_PREMEM_CONFIG
  @param[out] HOST_BRIDGE_DATA_HOB       - HOST_BRIDGE_DATA_HOB instance installed by this function

  @retval EFI_SUCCESS
**/
EFI_STATUS
InstallHostBridgeDataHob (
  IN    HOST_BRIDGE_PREMEM_CONFIG   *HostBridgePreMemConfig,
  OUT   HOST_BRIDGE_DATA_HOB        **HostBridgeDataHobOut
  )
{
  HOST_BRIDGE_DATA_HOB      *HostBridgeDataHob;
  EFI_STATUS                 Status;

  //
  // Create HOB for Host Bridge INFO
  //
  Status = PeiServicesCreateHob (
             EFI_HOB_TYPE_GUID_EXTENSION,
             sizeof (HOST_BRIDGE_DATA_HOB),
             (VOID **) &HostBridgeDataHob
             );
  ASSERT_EFI_ERROR (Status);

  //
  // Initialize default HOB data
  //
  HostBridgeDataHob->EfiHobGuidType.Name = gHostBridgeDataHobGuid;
  DEBUG ((DEBUG_INFO, "HostBridgeDataHob->EfiHobGuidType.Name: %g\n", &HostBridgeDataHob->EfiHobGuidType.Name));
  ZeroMem (&(HostBridgeDataHob->EnableAbove4GBMmio), sizeof (HOST_BRIDGE_DATA_HOB) - sizeof (EFI_HOB_GUID_TYPE));

  DEBUG ((DEBUG_INFO, "HostBridgeDataHob @ %X\n", HostBridgeDataHob));
  DEBUG ((DEBUG_INFO, "HostBridgeDataHob Size - HobHeaderSize: %X\n", sizeof (HOST_BRIDGE_DATA_HOB) - sizeof (EFI_HOB_GUID_TYPE)));
  DEBUG ((DEBUG_INFO, "HostBridgeDataHobSize: %X\n", sizeof (HOST_BRIDGE_DATA_HOB)));

  HostBridgeDataHob->EnableAbove4GBMmio = (UINT8) HostBridgePreMemConfig->EnableAbove4GBMmio;
  DEBUG ((DEBUG_INFO, "HostBridgeDataHob->EnableAbove4GBMmio: %X\n", HostBridgeDataHob->EnableAbove4GBMmio));
  (*HostBridgeDataHobOut) = HostBridgeDataHob;

  DEBUG ((DEBUG_INFO, "Host Bridge Data HOB installed\n"));

  return EFI_SUCCESS;
}

/**
  Update HostBridge Hob in PostMem

  @param[in] HostBridgePeiConfig    Instance of HOST_BRIDGE_PEI_CONFIG

  @retval EFI_SUCCESS
**/
EFI_STATUS
UpdateHostBridgeHobPostMem (
  IN    HOST_BRIDGE_PEI_CONFIG      *HostBridgePeiConfig
)
{
  HOST_BRIDGE_DATA_HOB        *HostBridgeDataHob;

  ///
  /// Locate and update Host Bridge Hob Data
  ///
  HostBridgeDataHob = (HOST_BRIDGE_DATA_HOB *) GetFirstGuidHob (&gHostBridgeDataHobGuid);
  if (HostBridgeDataHob != NULL) {
     HostBridgeDataHob->SkipPamLock = (BOOLEAN) (UINTN) HostBridgePeiConfig->SkipPamLock;
  }
  return EFI_SUCCESS;
}

/**
  This function calls into MemoryMap APIs to program memory Map.

  @param[in] MrcData             - The MRC "global data" area.

**/
VOID
HostBridgeMemoryMapInit (
  IN MrcParameters *CONST  MrcData
  )
{
  DEBUG ((DEBUG_INFO,"HostBridgeMemoryMapInit Start\n"));

  MrcMemoryMap *MemoryMap;

  MemoryMap = &MrcData->Outputs.MemoryMapData;

  ProgramTom (MemoryMap->TotalPhysicalMemorySize);
  //
  // Write TOLUD register
  //
  ProgramTolud (MemoryMap->ToludBase);
  //
  // Write TOUUD register
  //
  ProgramTouud (MemoryMap->TouudBase);
  //
  // Write TSEG register
  //
  ProgramTseg (MemoryMap->TsegBase);
  //
  // Write BDSM register
  //
  ProgramBdsm (MemoryMap->BdsmBase);
  //
  // Write BGSM register
  //
  ProgramBgsm (MemoryMap->GttBase);
  //
  // Write graphics control register
  //
  ProgramGfxCr (MemoryMap->GraphicsControlRegister);

  DEBUG ((DEBUG_INFO,"HostBridgeMemoryMapInit End\n"));
}

/**
  Program DPR lock and EPM bit
  Note : DPR lock and EPM bit programming should always be done after the External Gfx card has been detected and IGD enabled/disabled based on that.
**/
VOID
UpdateDpr (
  VOID
)
{
  UINT32                         DprData;

  DprData = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DPR));
  DEBUG ((DEBUG_INFO, "DPR read = %x \n", DprData));
  DprData |= (UINT32)(B_SA_DPR_LOCK_MASK | B_SA_DPR_EPM_MASK);
  PciSegmentOr32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DPR), DprData);
  DprData = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DPR));
  DEBUG ((DEBUG_INFO, "After setting the DPR LOCK & EPM bit DPR = %x \n", DprData));
}