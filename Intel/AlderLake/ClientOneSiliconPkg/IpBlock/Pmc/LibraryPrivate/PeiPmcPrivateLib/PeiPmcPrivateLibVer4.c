/** @file
  PCH private PEI PMC Library Ver4.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PchPcrLib.h>
#include <Library/DciPrivateLib.h>
#include <Library/PmcPrivateLib.h>
#include <Register/PmcRegs.h>
#include <Register/PmcRegsVer2.h>
#include <Register/PmcRegsFivr.h>
#include <Library/BaseMemoryLib.h>
#include <Register/PchRegs.h>
#include <Library/DciPrivateLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/GpioPrivateLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuInfoFruLib.h>

#include "PmcPrivateLibInternal.h"

/**
  Programs the CPPM clock gating policy.

  @param[in] PwrmBase      Power management base.
  @param[in] PmcSocConfig  Pointer to PMC SOC configuration.
**/
VOID
PmcSetCppmCgPolicy (
  IN UINT32          PwrmBase,
  IN PMC_SOC_CONFIG  *PmcSocConfig
  )
{
  UINT32  RegisterOffset;

  //
  // Program clock gating policy. Note that all registers here are HW defaults 0x0 so we don't have
  // to clear affected register fields with AND.
  //
  MmioOr32 (
    PwrmBase + R_PMC_PWRM_CPPM_CG_POL1A,
    0x4000881F
    );

  MmioOr32 (
    PwrmBase + R_PMC_PWRM_CPPM_CG_POL1B,
    0x00000003
    );

  MmioAnd32 (
    PwrmBase + R_PMC_PWRM_CPPM_CG_POL2B,
    0
    );

  if (PmcSocConfig->CppmCgInterfaceVersion == 2) {
    RegisterOffset = R_PMC_PWRM_CPPM_CG_POL2A_V2;
  } else {
    RegisterOffset = R_PMC_PWRM_CPPM_CG_POL2A;
  }

  MmioOr32 (
    PwrmBase + RegisterOffset,
    0x40008825
    );

  MmioOr32(
    PwrmBase + R_PMC_PWRM_CPPM_CG_POL3B,
    0x00000001
  );

  if (PmcSocConfig->CppmCgInterfaceVersion == 2) {
    RegisterOffset = R_PMC_PWRM_CPPM_CG_POL3A_V2;
  } else {
    RegisterOffset = R_PMC_PWRM_CPPM_CG_POL3A;
  }
  MmioOr32 (
    PwrmBase + RegisterOffset,
    0x40008842
    );

  //
  // Map the dynamic shutdown comparators to clock sources.
  //
  MmioOr32 (
    PwrmBase + R_PMC_PWRM_CS_SD_CTL1,
    0x10001020
  );
  MmioOr32 (
    PwrmBase + R_PMC_PWRM_CS_SD_CTL2,
    0x00211110
  );
  ///
  /// CS_SD_OVR_EN (0x1F54[0])=1 When CS_ACRO_CRO_OVR_EN = 1, CSACRO_CRO_OVR_VAL will be driven on the FORCE_ON signal for clock source ACRO_CRO.
  ///
  MmioOr32 (
    PwrmBase + R_PMC_PWRM_CS_SD_OVR_EN,
    B_PMC_PWRM_CS_ACRO_CRO_OVR_EN
    );
}

/**
  Enable OS IDLE Mode
**/
VOID
PmcEnableOsIdleMode (
  VOID
  )
{
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_WINIDLE_BIOS_ENABLE, B_PMC_PWRM_WINIDLE_BIOS_ENABLE_WI_ENABLE);
}

/**
  Lock OS IDLE Mode register
**/
VOID
PmcLockOsIdleMode (
  VOID
  )
{
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_WINIDLE_BIOS_ENABLE, B_PMC_PWRM_WINIDLE_BIOS_ENABLE_WI_LOCK);
}

/**
  Check if OS IDLE Mode is supported by PCH

  @retval OS IDLE Mode support state
**/
BOOLEAN
PmcIsOsIdleModeSupported (
  VOID
  )
{
  return TRUE;
}

/**
  S0ix settings

  @param[in] PmConfig                 PmConfig
**/
VOID
PmcSlpS0Config (
  IN PCH_PM_CONFIG                    *PmConfig
  )
{
  UINT32                              PchPwrmBase;

  PchPwrmBase = PmcGetPwrmBase ();

  if (IsSimicsEnvironment()) {
      MmioOr32 (PchPwrmBase + R_PMC_PWRM_CPPMVRIC, 0x1E);
  } else {
      MmioOr32 (PchPwrmBase + R_PMC_PWRM_CPPMVRIC, 0x801E);
  }

  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_CPPM_MISC_CFG, 0x138901);
}

/**
  Enable ModPHY SUS Power Gating

  @param[in] PwrmBase  PMC MMIO
  @param[in] PmConfig  PMC config
**/
VOID
EnableModPhySusWellPg (
  IN UINT32         PwrmBase,
  IN PCH_PM_CONFIG  *PmConfig
  )
{
  //
  // Disable per-lane modPHY sus pg on ADP-S as it is not supported.
  //
  MmioWrite32 (PwrmBase + R_PMC_PWRM_MODPHY_PM_CFG2, 0);
}

/**
  Configure CPPM Force Alignment Control
**/
VOID
PmcConfigureCppmForceAlignmentControl (
  VOID
  )
{
  UINT32 PwrmBase;
  UINT32 Data32Or;
  UINT32 Data32And;

  PwrmBase = PmcGetPwrmBase ();

  //
  // Recommendation on programming is delivered by IP
  // team.
  //
  Data32Or = 0xD80A8000;
  Data32And = ~0u;

  MmioAndThenOr32 (
    PwrmBase + R_PMC_PWRM_CPPMFALIGNCTL_1,
    Data32And,
    Data32Or
    );

  //
  // Lock PCH Forced alignment config
  //
  MmioOr32 (PwrmBase + R_PMC_PWRM_CPPMFALIGNCTL_1, BIT30);
}

/**
  This function is part of PMC init and configures which clock wake signals should
  set the SLOW_RING, SA, FAST_RING_CF and SLOW_RING_CF indication sent up to the CPU/PCH
**/
VOID
PmcInitClockWakeEnable (
  VOID
  )
{
  UINT32                    PchPwrmBase;

  PchPwrmBase = PmcGetPwrmBase ();

  //
  // Configure which backbone clock wake signals should set the SLOW_RING indication
  // sent up over the PM_SYNC
  // Enabled for:
  // All PCIe ports
  // All SATA ports
  // GbE
  // CNVi
  // HDA
  // USB OTG
  // VLW
  // Legacy cluster
  // P2SB
  // ISH
  // ME
  // PCIe NAND
  // SMBUS
  // CHAP
  // Thermal sensor
  // SPI
  // PMC
  // Display
  // Southport A, B, C and D
  // Primary channel
  // MEIO
  // GPIO
  // Audio DSP
  // LPSS
  // SCS
  // THC 0 and 1
  //
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_EN_CW_SLOW_RING, 0x2F8FBB01);
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_EN_CW_SLOW_RING2, 0x3A80C7E0);

  //
  // Configure which backbone clock wake signals should set the SA indication
  // sent up over the PM_SYNC. This configuration matches the SLOW_RING indication
  // configuration.
  //
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_EN_CW_SA, 0x2F8FBB01);
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_EN_CW_SA2, 0x3A80C7E0);


  //
  // Configure which clock wake signals should set the SLOW_RING_CF indication
  // sent up over the PM_SYNC
  // Enabled for:
  // Legacy cluster
  // P2SB
  //
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_EN_CW_SLOW_RING_CF, 0x00018000);
}

/**
  Configures PM_SYNC state hysteresis according to IP recommendation
  for SoC.
**/
VOID
PmcConfigurePmSyncHysteresis (
  VOID
  )
{
  MmioWrite32 (PmcGetPwrmBase () + R_PMC_PWRM_PM_SYNC_STATE_HYS, 0x00200840);
}

/**
   Configure which PM_SYNC messages will be sent in C0.

  @param[in]  PmcSocConfig  PMC SoC configuration
**/
VOID
PmcConfigurePmSyncC0 (
  IN PMC_SOC_CONFIG  *PmcSocConfig
  )
{
  UINT32  Data32;

  Data32 = 0x700094C0;
  if (PmcSocConfig->AdrSocConfig.Supported &&
      PmcSocConfig->AdrSocConfig.AdrMsgInterface == AdrOverPmSync) {
    Data32 |= (BIT13 | BIT14);
  }
  MmioWrite32 (PmcGetPwrmBase () + R_PMC_PWRM_PM_SYNC_MODE_C0, 0x700094C0);
}

/**
  This function configures PCH FIVR FET Ramp time config
**/
VOID
PmcFivrFetRampTimeConfig (
  VOID
  )
{
  //
  // Lock V1p05 PHY FET Ramp Time settings
  //
    MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_EXT_FET_RAMP_CONFIG, B_PMC_PWRM_EXT_FET_RAMP_CONFIG_V105_PHY_FRT_LOCK | B_PMC_PWRM_EXT_FET_RAMP_CONFIG_V105_PHY_IS_FRT_LOCK);
}

/**
  Get the CPU IOVR ramp duration POR for the SoC

  @retval Ramp duration in the unit of 10us
**/
UINT32
PmcGetCpuIovrRampDuration (
  VOID
  )
{
  if (!IsCpuVccIoVrSupported ()) {
    return 0x18;
  }

  return 0;
}

/**
  Perform generation specific locking configuration.

  @param[in] SiPolicy  Pointer to SiPolicy
**/
VOID
PmcGenerationSpecificLock (
  IN SI_POLICY_PPI  *SiPolicy
  )
{
  //
  // Lock PMC Static function disable configuration fields. It is assumed
  // that at the end of PEI all static disable configuration is finished.
  //
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_GEN_PMCON_B, BIT21);
}

/**
  Initialize LDO settings.
**/
VOID
PmcInitLdo (
  VOID
  )
{
  UINT32  Data32Or;

  //
  // Enable USB2/TS LDO dynamic shutdown and enable CNVi LDO low power mode for all SKUs
  // for Lp additionally enable USB3/PCIe GEN2 LDO dynamic shutdown.
  //
  Data32Or = B_PMC_PWRM_PMLDOCTRL_CNVIP24LDOLPEN | B_PMC_PWRM_PMLDOCTRL_USB2TS1P3LDODSEN;
  if (IsPchLp ()) {
    Data32Or |= B_PMC_PWRM_PMDLOCTRL_U31P2_LDODSEN;
  }

  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_PMLDOCTRL, Data32Or);
}

/**
  Performs generation specific programing.

  @param[in] SiPolicyPpi  Pointer to SI_POLICY_PPI instance
**/
VOID
PmcGenerationSpecificPrograming (
  IN SI_POLICY_PPI  *SiPolicyPpi
  )
{
  UINT32  PwrmBase;

  PwrmBase = PmcGetPwrmBase ();
  //
  // On ADP-S PMC has a bug that causes it to de-assert TNTE too quickly. BIOS
  // needs to force PMC to keep TNTE asserted at least for 2 RTC clock cycles.
  //
  MmioAndThenOr32 (PwrmBase + R_PMC_PWRM_TNTE_CTL_CFG, ~(UINT32)0xF, 2);

  MmioAnd32 (PwrmBase + R_PMC_PWRM_TNTE_RTC_ERROR_LOAD_OFFSET, ~(UINT32) BIT30);
}

/**
  Checks if current PMC supports timed GPIO functionality

  @retval TRUE   Timed GPIO functionality is supported
  @retval FALSE  Timed GPIO functionality is not supported
**/
BOOLEAN
PmcIsTimedGpioSupported (
  VOID
  )
{
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  return TRUE;
#else
  return FALSE;
#endif
}


/**
  Checks if the CPPM qualifiers are supported on the
  SoC.

  @retval TRUE  CPPM qualification is supported
  @retval FALSE CPPM qualification is not supported
**/
BOOLEAN
PmcIsCppmQualificationSupported (
  VOID
  )
{
  return FALSE;
}
