/** @file
  PCH private PEI PMC Library Ver2.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PchPcrLib.h>
#include <Library/DciPrivateLib.h>
#include <Library/PmcPrivateLib.h>
#include <Register/PmcRegs.h>
#include <Register/PmcRegsVer2.h>
#include <Register/PmcRegsFivr.h>
#include <Library/BaseMemoryLib.h>
#include <Register/PchRegs.h>
#include <Library/DciPrivateLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/GpioPrivateLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuInfoFruLib.h>
#include "PmcPrivateLibInternal.h"
#include <Library/PchFiaLib.h>
#include <Library/FiaSocLib.h>
#include <Library/PreSiliconEnvDetectLib.h>

/**
  Programs the CPPM clock gating policy.

  @param[in] PwrmBase      Power management base.
  @param[in] PmcSocConfig  Pointer to PMC SOC configuration.
**/
VOID
PmcSetCppmCgPolicy (
  IN UINT32          PwrmBase,
  IN PMC_SOC_CONFIG  *PmcSocConfig
  )
{
  UINT32  RegisterOffset;
  UINT32  Data32And;
  UINT32  Data32Or;

  //
  // CPPM_CG_POL1A (0x1B24h[30])  = 1,    CPPM Shutdown Qualifier Enable for Clock Source Group 1 (CPPM_G1_QUAL)
  // CPPM_CG_POL1A (0x1B24h[8:0]) = 008h, LTR Threshold for Clock Source Group 1 (LTR_G1_THRESH)
  // CPPM_CG_POL1A (0x1B24h[15:0]) = 85B6h, LTR Threshold for Clock Source Group 1 (LTR_G1_THRESH) for PCH P
  //
  Data32Or = B_PMC_PWRM_CPPM_MPG_POL1A_CPPM_MPHY_QUAL;
  if (IsPchP () || IsPchN ()) {
    Data32Or |=  0x85B6;
    Data32And = (UINT32)~B_PMC_PWRM_CPPM_CG_POL1A_LTR_G1_THRESH_V2;
  } else {
    Data32Or |= 0x8;
    Data32And = (UINT32)~B_PMC_PWRM_CPPM_CG_POL1A_LTR_G1_THRESH;
  }
  MmioAndThenOr32 (
    PwrmBase + R_PMC_PWRM_CPPM_CG_POL1A,
    Data32And,
    Data32Or
    );

  //
  // CPPM_CG_POL2A (0x1B40h[30])  = 1,    CPPM Shutdown Qualifier Enable for Clock Source Group 2 (CPPM_G2_QUAL)
  // CPPM_CG_POL2A (0x1B40h[29])  = 1,    ASLT/PLT Selection for Clock Source Group 1
  // CPPM_CG_POL2A (0x1B40h[8:0]) = 008h, LTR Threshold for Clock Source Group 2 (LTR_G2_THRESH)
  // CPPM_CG_POL2A (0x1B40h[15:0]) = 085B6h, LTR Threshold for Clock Source Group 2 (LTR_G2_THRESH) for Pch P
  //
  if (PmcSocConfig->CppmCgInterfaceVersion == 2) {
    RegisterOffset = R_PMC_PWRM_CPPM_CG_POL2A_V2;
  } else {
    RegisterOffset = R_PMC_PWRM_CPPM_CG_POL2A;
  }
  Data32Or = B_PMC_PWRM_CPPM_MPG_POL1A_CPPM_MPHY_QUAL | B_PMC_PWRM_CPPM_CG_POL1A_LT_G1_SEL;
  if (IsPchP () || IsPchN ()) {
    Data32Or |= 0x85B6;
    Data32And = (UINT32)~B_PMC_PWRM_CPPM_CG_POL1A_LTR_G1_THRESH_V2;
  } else {
    Data32Or |= 0x8;
    Data32And = (UINT32)~B_PMC_PWRM_CPPM_CG_POL1A_LTR_G1_THRESH;
  }
  MmioAndThenOr32 (
    PwrmBase + RegisterOffset,
    Data32And,
    Data32Or
    );

  //
  // CPPM_CG_POL3A (0x1BA8[30])  = 1,    CPPM Shutdown Qualifier Enable for Clock Source Group 3 (CPPM_G3_QUAL)
  // CPPM_CG_POL3A (0x1BA8[29])  = 1,    ASLT/PLT Selection for Clock Source Group 1
  // CPPM_CG_POL3A (0x1BA8[8:0]) = 029h, LTR Threshold for Clock Source Group 3 (LTR_G3_THRESH)
  // CPPM_CG_POL3A (0x1BA8[15:0]) = 0884Fh, LTR Threshold for Clock Source Group 3 (LTR_G3_THRESH) for P PCH
  //
  Data32Or = B_PMC_PWRM_CPPM_MPG_POL1A_CPPM_MPHY_QUAL | B_PMC_PWRM_CPPM_CG_POL1A_LT_G1_SEL;
  if (IsPchP () || IsPchN ()) {
    Data32Or |= 0x884F;
    Data32And = (UINT32)~B_PMC_PWRM_CPPM_CG_POL1A_LTR_G1_THRESH_V2;
  } else {
    Data32Or |= 0x029;
    Data32And = (UINT32)~B_PMC_PWRM_CPPM_CG_POL1A_LTR_G1_THRESH;
  }

  if (PmcSocConfig->CppmCgInterfaceVersion == 2) {
    RegisterOffset = R_PMC_PWRM_CPPM_CG_POL3A_V2;
  } else {
    RegisterOffset = R_PMC_PWRM_CPPM_CG_POL3A;
  }
  MmioAndThenOr32 (
    PwrmBase + RegisterOffset,
    Data32And,
    Data32Or
    );

  //
  // CS_SD_CTL1 (0x1BE8[22:20]) = 010, Clock Source 5 Control Configuration (CS5_CTL_CFG)
  //
  if (IsPchP () || IsPchN ()) {
    MmioAndThenOr32 (
      PwrmBase + R_PMC_PWRM_CS_SD_CTL1,
      (UINT32)~(B_PMC_PWRM_CS_SD_CTL1_CS5_CTL_CFG | B_PMC_PWRM_CS_SD_CTL1_CS_XTAL_CTL_CFG),
      (2 << N_PMC_PWRM_CS_SD_CTL1_CS_XTAL_CTL_CFG)
    );
  } else {
    MmioAndThenOr32 (
      PwrmBase + R_PMC_PWRM_CS_SD_CTL1,
      (UINT32)~(B_PMC_PWRM_CS_SD_CTL1_CS5_CTL_CFG | B_PMC_PWRM_CS_SD_CTL1_CS1_CTL_CFG),
      (2 << N_PMC_PWRM_CS_SD_CTL1_CS5_CTL_CFG)
    );
  }
}

/**
  Enable OS IDLE Mode
**/
VOID
PmcEnableOsIdleMode (
  VOID
  )
{
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_WINIDLE_BIOS_ENABLE, B_PMC_PWRM_WINIDLE_BIOS_ENABLE_WI_ENABLE);
}

/**
  Lock OS IDLE Mode register
**/
VOID
PmcLockOsIdleMode (
  VOID
  )
{
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_WINIDLE_BIOS_ENABLE, B_PMC_PWRM_WINIDLE_BIOS_ENABLE_WI_LOCK);
}

/**
  Check if OS IDLE Mode is supported by PCH

  @retval OS IDLE Mode support state
**/
BOOLEAN
PmcIsOsIdleModeSupported (
  VOID
  )
{
  return TRUE;
}

/**
  S0ix settings

  @param[in] PmConfig                 PmConfig
**/
VOID
PmcSlpS0Config (
  IN PCH_PM_CONFIG                    *PmConfig
  )
{
  UINT32                              PchPwrmBase;
  UINT32                              Data32;

  Data32 = 0;

  PchPwrmBase = PmcGetPwrmBase ();

  ///
  /// PWRMBASE + 0x1B1C = 0x801E
  ///
  MmioOr32 (PchPwrmBase + R_PMC_PWRM_CPPMVRIC, 0x801E);

  ///
  /// PWRMBASE + 0x1B20 = 0x00196501
  ///
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_CPPM_MISC_CFG, 0x00196501);
  ///
  /// Configure PMC S0ix policy (for debug)
  ///
  if(!(IsPchP () || IsPchN ())) {
    if (IsPchLp ()) {
      Data32 = BIT28;
    }
  }
  PmcConfigS0ixPolicies (Data32);
}

/**
  Enable ModPHY SUS Power Gating

  @param[in] PwrmBase  PMC MMIO
  @param[in] PmConfig  PMC config
**/
VOID
EnableModPhySusWellPg (
  IN UINT32         PwrmBase,
  IN PCH_PM_CONFIG  *PmConfig
  )
{
  UINT8  PortNum;
  UINT8  MaxUsb3PortNum;
  UINT32 Data32Or;
  UINT32 Data32And;

  Data32Or = 0;
  Data32And = 0xFFFFF000;

  /// For LP, set R_PMC_PWRM_MODPHY_PM_CFG1[MLS0SWPGP] Bits 15:0 = 0xFFFF
  ///         set R_PMC_PWRM_MODPHY_PM_CFG5[MSPDRTREQ] Bits [25,24,23,22,21,16] = all 1's
  ///
  if (IsPchLp ()) {
    MmioWrite16 (PwrmBase + R_PMC_PWRM_MODPHY_PM_CFG1, 0xFFFF);
    MmioOr32 (PwrmBase + R_PMC_PWRM_MODPHY_PM_CFG5, 0x3E10000);
  }

  ///
  /// Clear corresponding bit in MLSXSWPGP when ModPHY SUS Well Lane Power Gating is not permitted in Sx
  /// As USB3 lanes do not support SUS Well Power Gating as they support in-band wake, the bits corresponding
  /// to USB3 lanes need to be cleared
  ///
  MaxUsb3PortNum = GetPchXhciMaxUsb3PortNum ();
  Data32And = 0;
  for (PortNum = 0; PortNum < MaxUsb3PortNum; PortNum++) {
    if (PchFiaIsUsb3PortConnected (PortNum)) {
      Data32And |= (1 << PortNum);
    }
  }
  MmioAnd32 (
    PwrmBase + R_PMC_PWRM_MODPHY_PM_CFG2,
    ~Data32And
    );

  if (PmConfig->ModPhySusPgEnable && IsPchLp ()) {
    MaxUsb3PortNum = GetPchXhciMaxUsb3PortNum ();
    ///
    /// MODPHY_PM_CFG3 (0x10C8[30]) = 1, ModPHY Lane SUS Power Domain Dynamic Gating Enable
    /// MODPHY_PM_CFG3 (0x10C8[0]) = 1, C10 qualification for MPHY power gating Enable
    ///
    Data32Or = (B_PMC_PWRM_MODPHY_PM_CFG3_MPLSPDDGE | B_PMC_PWRM_MODPHY_PM_CFG3_MLSPDDGE | B_PMC_PWRM_MODPHY_PM_CFG3_C10_QUAL_MPHYPG);
    for (PortNum = 0; PortNum < MaxUsb3PortNum; PortNum++) {
      if (PchFiaIsUsb3PortConnected (PortNum)) {
        Data32Or = B_PMC_PWRM_MODPHY_PM_CFG3_C10_QUAL_MPHYPG;
        DEBUG ((DEBUG_INFO, "One of the modphy lanes assigned to usb skipping setting MPLSPDDGE and MLSPDDGE\n"));
        break;
      }
    }
    MmioOr32 (PwrmBase + R_PMC_PWRM_MODPHY_PM_CFG3, Data32Or);
  }
}

/**
  Configure CPPM Force Alignment Control
**/
VOID
PmcConfigureCppmForceAlignmentControl (
  VOID
  )
{
  UINT32 PwrmBase;
  UINT32 Data32Or;
  UINT32 Data32And;

  PwrmBase = PmcGetPwrmBase ();

  //
  // Recommendation on programming is delivered by IP
  // team.
  //
  Data32Or = 0xD80A8000;
  Data32And = ~0u;

  if ((PchStepping () == PCH_Z0) || (IsPchH ()))  {
    Data32And &= ~(UINT32)BIT28;
    Data32Or  = 0xC80A8000;
  }

  MmioAndThenOr32 (
    PwrmBase + R_PMC_PWRM_CPPMFALIGNCTL_1,
    Data32And,
    Data32Or
    );

  //
  // Lock PCH Forced alignment config
  //
  MmioOr32 (PwrmBase + R_PMC_PWRM_CPPMFALIGNCTL_1, BIT30);
}

/**
  This function is part of PMC init and configures which clock wake signals should
  set the SLOW_RING, SA, FAST_RING_CF and SLOW_RING_CF indication sent up to the CPU/PCH
**/
VOID
PmcInitClockWakeEnable (
  VOID
  )
{
  UINT32                    PchPwrmBase;

  PchPwrmBase = PmcGetPwrmBase ();

  //
  // Configure which backbone clock wake signals should set the SLOW_RING indication
  // sent up over the PM_SYNC
  // Enabled for:
  // All PCIe ports
  // All SATA ports
  // GbE
  // CNVi
  // HDA
  // USB OTG
  // VLW
  // Legacy cluster
  // P2SB
  // ISH
  // ME
  // PCIe NAND
  // SMBUS
  // CHAP
  // Thermal sensor
  // SPI
  // PMC
  // Display
  // Southport A, B, C and D
  // Primary channel
  // MEIO
  // GPIO
  // Audio DSP
  // LPSS
  // SCS
  // THC 0 and 1
  //
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_EN_CW_SLOW_RING, 0x2F8FBB01);
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_EN_CW_SLOW_RING2, 0x1A80C7E0);

  //
  // Configure which backbone clock wake signals should set the SA indication
  // sent up over the PM_SYNC. This configuration matches the SLOW_RING indication
  // configuration.
  //
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_EN_CW_SA, 0x2F8FBB01);
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_EN_CW_SA2, 0x1A80C7E0);

  //
  // Configure which clock wake signals should set the SLOW_RING_CF indication
  // sent up over the PM_SYNC
  // Enabled for:
  // Legacy cluster
  // P2SB
  //
  MmioWrite32 (PchPwrmBase + R_PMC_PWRM_EN_CW_SLOW_RING_CF, 0x00018000);
}

/**
  Configures PM_SYNC state hysteresis according to IP recommendation
  for SoC.
**/
VOID
PmcConfigurePmSyncHysteresis (
  VOID
  )
{
  MmioWrite32 (PmcGetPwrmBase () + R_PMC_PWRM_PM_SYNC_STATE_HYS, 0x01104620);
}

/**
   Configure which PM_SYNC messages will be sent in C0.

  @param[in]  PmcSocConfig  PMC SoC configuration
**/
VOID
PmcConfigurePmSyncC0 (
  IN PMC_SOC_CONFIG  *PmcSocConfig
  )
{
  UINT32  Data32;

  Data32 = 0x700094C0;
  if (PmcSocConfig->AdrSocConfig.Supported &&
      PmcSocConfig->AdrSocConfig.AdrMsgInterface == AdrOverPmSync) {
    Data32 |= (BIT13 | BIT14);
  }
  MmioWrite32 (PmcGetPwrmBase () + R_PMC_PWRM_PM_SYNC_MODE_C0, 0x700094C0);
}

/**
  This function configures PCH FIVR FET Ramp time config
**/
VOID
PmcFivrFetRampTimeConfig (
  VOID
  )
{
  //
  // S0ix requires otpimzed EXT FET RAMP times:
  // Fet:   7 * 31 us = 217 us
  // IsFet: 9 * 10 us = 90  us
  //
  PmcExtFetRampConfig (7, 9);
}

/**
  Checks if current PMC supports timed GPIO functionality

  @retval TRUE   Timed GPIO functionality is supported
  @retval FALSE  Timed GPIO functionality is not supported
**/
BOOLEAN
PmcIsTimedGpioSupported (
  VOID
  )
{
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  return TRUE;
#else
  return FALSE;
#endif
}

/**
  Get the CPU IOVR ramp duration POR for the SoC

  @retval Ramp duration in the unit of 10us
**/
UINT32
PmcGetCpuIovrRampDuration (
  VOID
  )
{
  UINT32         Data32Or;

  /// PM_CFG4 (0x18E8h[8:0]) = 0 for VCCIO VR on CPU, 0x18 for VCCIO VR on board
  Data32Or = 0;
  if (!IsCpuVccIoVrSupported ()) {
    Data32Or |= 0x18;
  }
  return Data32Or;
}

/**
  Perform generation specific locking configuration.

  @param[in] SiPolicy  Pointer to SiPolicy
**/
VOID
PmcGenerationSpecificLock (
  IN SI_POLICY_PPI  *SiPolicy
  )
{
}

/**
  Initialize LDO settings.
**/
VOID
PmcInitLdo (
  VOID
  )
{
  //
  // Enable USB2/TS LDO dynamic shutdown and enable CNVi LDO low power mode for all SKUs
  //
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_PMLDOCTRL, B_PMC_PWRM_PMLDOCTRL_CNVIP24LDOLPEN | B_PMC_PWRM_PMLDOCTRL_USB2TS1P3LDODSEN);
}

/**
  Performs generation specific programing.

  @param[in] SiPolicyPpi  Pointer to SI_POLICY_PPI instance
**/
VOID
PmcGenerationSpecificPrograming (
  IN SI_POLICY_PPI  *SiPolicyPpi
  )
{
  EFI_STATUS     Status;
  PCH_PM_CONFIG  *PmConfig;
  UINT32         PwrmBase;
  UINT32         Data32Or;

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gPmConfigGuid, (VOID *) &PmConfig);
  ASSERT_EFI_ERROR (Status);

  PwrmBase = PmcGetPwrmBase ();
  if ((PchStepping() >= PCH_Z0)) {
    Data32Or = 0x0;
  } else {
    Data32Or = 0x3;
  }
  MmioAndThenOr32 (
    PwrmBase + R_PMC_PWRM_CPPM_CG_POL1B,
    (UINT32) ~B_PMC_PWRM_CPPM_FIVR_POL1B_TNTE_FIVR_VOLT_PRE_WAKE,
    Data32Or
    );

  if (PmConfig->C10DynamicThresholdAdjustment &&
      (((PchStepping () == PCH_Z0) || PchStepping () == PCH_Z1 || (PchStepping () < PCH_B0)))) {
    //
    // On TGP-Z0 and Ax C10 dynamic threshold adjustment is
    // enabled by settign bit 0 of CORE_SPARE_GCR. As this mode is
    // not POR it is not captured by RDL description.
    //
    MmioOr32 (PwrmBase + R_PMC_PWRM_CORE_SPARE_GCR, BIT0);
  }

  //
  // On ADP-P PMC has a bug that causes it to de-assert TNTE too quickly. BIOS
  // needs to force PMC to keep TNTE asserted at least for 2 RTC clock cycles.
  //
  if (IsPchP () || IsPchN ()) {
    MmioAndThenOr32 (PwrmBase + R_PMC_PWRM_TNTE_CTL_CFG, ~(UINT32)0xF, 2);
  }
}


/**
  Checks if the CPPM qualifiers are supported on the
  SoC.

  @retval TRUE  CPPM qualification is supported
  @retval FALSE CPPM qualification is not supported
**/
BOOLEAN
PmcIsCppmQualificationSupported (
  VOID
  )
{
  return FALSE;
}

