/** @file
  Header file for PeiP2sbPrivateLib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_P2SB_PRIVATE_LIB_H_
#define _PEI_P2SB_PRIVATE_LIB_H_

#include <Ppi/SiPolicy.h>
#include <P2SbHandle.h>

/**
  The function check the P2SB controller exists or not.

  @param[in] P2SbPciBase           P2SB controller PCI base address.

  @return TRUE                     P2SB controller does not exists.
  @return FALSE                    P2SB  controller exists.
**/
BOOLEAN
P2sbControllerNotExist (
  IN UINT64      P2SbPciBase
  );

/**
  Check SBREG readiness.

  @param[in] P2sbBase         P2SB PCI base address

  @retval TRUE                SBREG is ready
  @retval FALSE               SBREG is not ready
**/
BOOLEAN
P2sbIsSbregReady (
  IN UINT64                              P2sbBase
  );

/**
  Internal function performing HPET init in early PEI phase

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2sbHpetInit (
  IN  P2SB_HANDLE                       *P2SbHandle
  );

/**
  Early init P2SB configuration

  @param[in] P2SbController            P2SB controller
  @param[in] IosfSbPostedSupport       IOSF-SB Posted Write Supported
**/
VOID
P2sbEarlyConfig (
  IN P2SB_CONTROLLER      *P2SbController,
  IN BOOLEAN              IosfSbPostedSupport
  );

/**
  The function performs P2SB initialization.

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2sbConfigure (
  IN  P2SB_HANDLE             *P2SbHandle
  );

/**
  Hide P2SB device.

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2sbHideDevice (
  IN  P2SB_HANDLE                       *P2SbHandle
  );

/**
  Bios will remove the host accessing right to PSF register range
  prior to any 3rd party code execution.

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2SbRemoveEndpointAccess (
  IN  P2SB_HANDLE             *P2SbHandle
  );

/**
  The function performs P2SB lock programming.

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2sbLock (
  IN  P2SB_HANDLE             *P2SbHandle
  );

/**
  The function configures P2SB Regbar.

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2sbRegbarConfigure (
  IN  P2SB_HANDLE                       *P2SbHandle
  );

#endif // _PEI_P2SB_PRIVATE_LIB_H_
