/** @file
  Header file for P2SB handle

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020-2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/
#ifndef _P2SB_HANDLE_H_
#define _P2SB_HANDLE_H_

#include <ConfigBlock.h>
#include <P2sbConfig.h>
#include <P2SbController.h>

/**
  Internal P2SB policy options
**/
typedef struct {
  /**
    Enable HSLE Workaround to avoid p2sb clock gating by adding 100ms delay
   **/
  BOOLEAN               HsleWorkaround;
  /**
    Set to enable low latency of legacy IO.
    Some systems require lower IO latency irrespective of power.
    This is a tradeoff between power and IO latency.
    @note: Once this is enabled, DmiAspm, Pcie DmiAspm in SystemAgent
    and ITSS Clock Gating are forced to disabled.
    <b>FALSE: Disable</b>, TRUE: Enable
  **/
  BOOLEAN               LegacyIoLowLatency;
  /**
    On server PCHs "Hardware Autonomous Enable" has to be always disabled.
    This flag allow to skip programming Hardware Autonomous Enable bit
    in Power Control Enable (PCE) at offset E4h P2SB config space register.
    <b>FALSE: Disable</b>, TRUE: Enable
  **/
  BOOLEAN               HaPowerGatingSupported;
  /**
    If enabled, sets DPEE, CPEE and DPPEE bits to enable parity error handling
  **/
  BOOLEAN               EnableParityCheck;
  /**
  This option disables Integrated Embedded Controller (IEC)
  and then LPC Fixed IO routing to IEC
  (write 0 to LFIORIEC register)
  <b>FALSE: IEC enabled</b>, TRUE: IEC disabled
  **/
  BOOLEAN               IecSupportDisable;
  /**
    Allow avoid HPET and APIC BDFs to be programmed.
   <b>FALSE: BDF is programmed</b>, TRUE: BDF is not programmed
  **/
  BOOLEAN               DisableHpetAndApicBdfProgramming;
  /**
    Register Endpoint Mask 7 bitfield
    Allow disable particular IOSF-SB endpoints.
  **/
  UINT32                EndpointMask7;
} P2SB_PRIVATE_CONFIG;

/**
  Get list of P2sb Endpoint Masked PIDs

  @param[out] EndpointMaskTable        Endpoint Masked PIDs
  @param[out] EndpointMaskTableLength  Length of EndpointMaskTable
**/

typedef
VOID
(*P2SB_GET_ENDPOINT_MASKED_PIDS) (
  OUT UINT16   **EndpointMaskTable,
  OUT UINT32   *EndpointMaskTableLength
  );

/**
  P2SB callbacks
  List of function pointers can passed to IB Block driver
**/
typedef struct {
  P2SB_GET_ENDPOINT_MASKED_PIDS           GetEndpointMaskedPids;
} P2SB_CALLBACK;

/**
  GBE device structure
  Stores all data necessary to initialize GBE IP block
**/
typedef struct {
  P2SB_CONTROLLER             *Controller;
  PCH_P2SB_CONFIG             *P2SbConfig;
  P2SB_PRIVATE_CONFIG         *PrivateConfig;
  P2SB_CALLBACK               *Callback;
} P2SB_HANDLE;

#endif // _P2SB_HANDLE_H_
