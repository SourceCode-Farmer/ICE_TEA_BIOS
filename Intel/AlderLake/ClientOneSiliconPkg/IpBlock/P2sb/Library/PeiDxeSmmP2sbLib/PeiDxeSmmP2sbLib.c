/** @file
  This file provides P2SB Public Library implementation

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi/UefiBaseType.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <IndustryStandard/Pci30.h>
#include <Library/PciSegmentLib.h>
#include <Register/PchRegs.h>
#include <Register/P2sbRegs.h>
#include <Library/PchPciBdfLib.h>

/**
  Get P2SB EPMASK setting
  Note: after end of pei this function can be used only in SMM

  @param[in]    EpMaskNum  EPMASK number

  @retval       EPMASKx setting
**/
UINT32
P2sbGetEpmask (
  IN UINT32  EpMaskNum
  )
{
  UINT32 EpMaskValue;
  UINT64 P2sbBase;
  BOOLEAN P2sbHidden;

  P2sbHidden = FALSE;

  P2sbBase = P2sbPciCfgBase ();

  if (PciSegmentRead16 (P2sbBase + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    //
    // Try to unhide it if possible, otherwise return 0
    //
    PciSegmentWrite8 (P2sbBase + R_P2SB_CFG_E0 + 1, 0);
    if (PciSegmentRead16 (P2sbBase + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
      return 0;
    }
    P2sbHidden = TRUE;
  }

  switch (EpMaskNum) {
  case 5:
    EpMaskValue = PciSegmentRead32 (P2sbBase + R_P2SB_CFG_EPMASK5);
    break;
  case 7:
    EpMaskValue = PciSegmentRead32 (P2sbBase + R_P2SB_CFG_EPMASK7);
    break;
  default:
    EpMaskValue = 0;
    break;
  }

  if (P2sbHidden == TRUE) {
    //
    // Hide if it was hidden before caling this function
    //
    PciSegmentWrite8 (P2sbBase + R_P2SB_CFG_E0 + 1, 1);
  }

  return EpMaskValue;
}

/**
  Check P2SB EPMASK is locked
  Note: after end of pei this function can be used only in SMM

  @retval TRUE  P2SB EPMASK is locked
          FALSE P2SB EPMASK is not locked
**/
BOOLEAN
P2sbIsEpmaskLock (
  VOID
  )
{
  UINT64 P2sbBase;
  UINT8  Data8;

  BOOLEAN P2sbHidden;

  P2sbHidden = FALSE;

  P2sbBase = P2sbPciCfgBase ();

  if (PciSegmentRead16 (P2sbBase + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    //
    // Try to unhide it if possible, otherwise return 0
    //
    PciSegmentWrite8 (P2sbBase + R_P2SB_CFG_E0 + 1, 0);
    if (PciSegmentRead16 (P2sbBase + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
      return 0;
    }
    P2sbHidden = TRUE;
  }

  if (PciSegmentRead16 (P2sbBase + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    return FALSE;
  }

  Data8 = PciSegmentRead8 (P2sbBase + R_P2SB_CFG_E0 + 2);

  if (P2sbHidden == TRUE) {
    //
    // Hide if it was hidden before caling this function
    //
    PciSegmentWrite8 (P2sbBase + R_P2SB_CFG_E0 + 1, 1);
  }
  return !!(Data8 & BIT1);
}
