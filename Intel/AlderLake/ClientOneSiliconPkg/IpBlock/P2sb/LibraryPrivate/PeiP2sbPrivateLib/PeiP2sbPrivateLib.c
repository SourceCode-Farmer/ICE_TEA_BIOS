/** @file
  Initializes PCH Primary To Sideband Bridge (P2SB) Device in PEI

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/HobLib.h>
#include <Library/PciSegmentLib.h>
#include <IndustryStandard/Pci30.h>
#include <Register/PchRegs.h>
#include <Register/P2sbRegs.h>
#include <Library/TimerLib.h>
#include <P2SbHandle.h>

#define R_HPET_MEM_GEN_CFG            0x10
#define B_HPET_MEM_GEN_CFG_ENABLE_CNF BIT0

/**
  The function check the P2SB controller exists or not.

  @param[in] P2SbPciBase           P2SB controller PCI base address.

  @return TRUE                     P2SB controller does not exists.
  @return FALSE                    P2SB  controller exists.
**/
BOOLEAN
P2sbControllerNotExist (
  IN UINT64      P2SbPciBase
  )
{
  if (PciSegmentRead16 (P2SbPciBase) == V_P2SB_CFG_VENDOR_ID) {
    return FALSE;
  } else {
    return TRUE;
  }
}

/**
  Hide P2SB device.

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2sbHideDevice (
  IN  P2SB_HANDLE                       *P2SbHandle
  )
{
  UINT64                  P2sbBase;

  P2sbBase = P2SbHandle->Controller->PciCfgBaseAddr;

  PciSegmentWrite8 (P2sbBase + R_P2SB_CFG_E0 + 1, BIT0);

  if (P2SbHandle->PrivateConfig->HsleWorkaround) {
    DEBUG ((DEBUG_INFO, "HSLE Workaround to avoid p2sb clock gating\n"));
    if (PciSegmentRead32 (P2sbBase) != 0xFFFFFFFF) {
      DEBUG ((DEBUG_INFO, "100ms delay before P2SB Base read\n"));
      MicroSecondDelay (100 * 1000); //wait for 100 ms
    }
  }
}

/**
  The function configures P2SB power management

  @param[in] P2SbHandle            P2SB controller handle
**/
STATIC
VOID
P2sbPowerGatingConfigure (
  IN  P2SB_HANDLE                       *P2SbHandle
  )
{
  UINT64                      P2sbBase;
  UINT8                       Data8;

  P2sbBase = P2SbHandle->Controller->PciCfgBaseAddr;
  ///
  /// Move setting PGCB clock gating enable (PGCBCGE) 0xE0[16] to P2sbRemovePsfAccess for MASKLOCK 0xE0[17] is RWO
  ///

  /// Set Hardware Autonomous Enable (HAE) and PMC Power Gating Enable (PMCPG_EN)
  /// P2SB PCI offset 0xE4[2,1,0] = 0's
  /// P2SB PCI offset 0xE4[5] = 1 if Legacy IO Low Latency is disabled
  ///
  if (P2SbHandle->PrivateConfig->LegacyIoLowLatency || !P2SbHandle->PrivateConfig->HaPowerGatingSupported) {
    Data8 = 0;
  } else {
    Data8 = BIT5;
  }
  PciSegmentAndThenOr8 (P2sbBase + R_P2SB_CFG_E4, (UINT8) ~(BIT5 | BIT2 | BIT1 | BIT0), Data8);
}


/**
  Check SBREG readiness.

  @param[in] P2sbBase         P2SB PCI base address

  @retval TRUE                SBREG is ready
  @retval FALSE               SBREG is not ready
**/
BOOLEAN
P2sbIsSbregReady (
  IN UINT64                              P2sbBase
  )
{
  if ((PciSegmentRead32 (P2sbBase + R_P2SB_CFG_SBREG_BAR) & B_P2SB_CFG_SBREG_RBA) == 0) {
    return FALSE;
  }
  return TRUE;
}

/**
  Internal function performing HPET init in early PEI phase

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2sbHpetInit (
  IN  P2SB_HANDLE                       *P2SbHandle
  )
{
  UINT64                                P2sbBase;
  UINT32                                HpetBase;

  P2sbBase = P2SbHandle->Controller->PciCfgBaseAddr;
  if (P2sbControllerNotExist (P2sbBase)) {
    return;
  }

  //
  // Initial and enable HPET High Precision Timer memory address for basic usage
  // If HPET base is not set, the default would be 0xFED00000.
  //
  HpetBase = (UINT32) P2SbHandle->Controller->HpetMmio;
  ASSERT ((HpetBase & 0xFFFFCFFF) == 0xFED00000);
  PciSegmentAndThenOr8 (
    P2sbBase + R_P2SB_CFG_HPTC,
    (UINT8) ~B_P2SB_CFG_HPTC_AS,
    (UINT8) (((HpetBase >> N_HPET_ADDR_ASEL) & B_P2SB_CFG_HPTC_AS) | B_P2SB_CFG_HPTC_AE)
    );
  //
  // Read back for posted write to take effect
  //
  PciSegmentRead8 (P2sbBase + R_P2SB_CFG_HPTC);
  //
  // Set HPET Timer enable to start counter spinning
  //
  MmioOr32 (HpetBase + R_HPET_MEM_GEN_CFG, B_HPET_MEM_GEN_CFG_ENABLE_CNF);
  //
  // Build the resource descriptor hob for HPET address resource.
  // HPET only claims 0x400 in size, but the minimal size to reserve memory
  // is one page 0x1000.
  //
  BuildResourceDescriptorHob (
    EFI_RESOURCE_MEMORY_MAPPED_IO,
    (EFI_RESOURCE_ATTRIBUTE_PRESENT    |
     EFI_RESOURCE_ATTRIBUTE_INITIALIZED |
     EFI_RESOURCE_ATTRIBUTE_UNCACHEABLE),
    HpetBase,
    0x1000
    );
  BuildMemoryAllocationHob (
    HpetBase,
    0x1000,
    EfiMemoryMappedIO
    );
}

/**
  Early init P2SB configuration

  @param[in] P2SbController            P2SB controller
  @param[in] IosfSbPostedSupport       IOSF-SB Posted Write Supported
**/
VOID
P2sbEarlyConfig (
  IN P2SB_CONTROLLER      *P2SbController,
  IN BOOLEAN              IosfSbPostedSupport
  )
{
  UINT64                  P2sbBase;

  P2sbBase  = P2SbController->PciCfgBaseAddr;
  if (P2sbControllerNotExist (P2sbBase)) {
    return;
  }

 if (IosfSbPostedSupport) {
    ///
    /// BIOS shall program the PCI Capability List to 0 for P2SB controller.
    ///
    PciSegmentWrite8 (P2sbBase + PCI_CAPBILITY_POINTER_OFFSET, 0);
    ///
    /// For GPIO and ITSS that support sideband posted write, they can support
    /// back to back write after their corresponding bits under P2SB PCI Config
    /// 200h-21Fh are set.
    /// For CNL PCH-LP and CNL PCH-H, program the following at early PCH BIOS init
    /// 1. Set P2SB PCI offset 200h to 0
    /// 2. Set P2SB PCI offset 204h to 0
    /// 3. Set P2SB PCI offset 208h to 0
    /// 4. Set P2SB PCI offset 20Ch to 00007C00h
    /// 5. Set P2SB PCI offset 210h to 0
    /// 6. Set P2SB PCI offset 214h to 0
    /// 7. Set P2SB PCI offset 218h to 00000010h
    /// 8. Set P2SB PCI offset 21Ch to 0
    ///
    PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_200, 0);
    PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_204, 0);
    PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_208, 0);
    PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_20C, 0x00007C00);
    PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_210, 0);
    PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_214, 0);
    PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_218, 0x00000010);
    PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_21C, 0);
  }
  ///
  /// Set P2SB PCI offset 0xF4[0] = 1
  ///
  PciSegmentOr8 (P2sbBase + R_P2SB_CFG_F4, BIT0);
}

/**
  The function performs P2SB initialization.

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2sbConfigure (
  IN  P2SB_HANDLE             *P2SbHandle
  )
{
  UINT64                      P2sbBase;

  P2sbBase  = P2SbHandle->Controller->PciCfgBaseAddr;
  if (P2sbControllerNotExist (P2sbBase)) {
    return;
  }

  if (!P2SbHandle->PrivateConfig->DisableHpetAndApicBdfProgramming) {
    //
    // HPET and APIC BDF programming
    // Assign 0:30:6 for HPET and 0:30:7 for APIC statically.
    //
    PciSegmentWrite16 (
      P2sbBase + R_P2SB_CFG_HBDF,
      (V_P2SB_CFG_HBDF_BUS << 8) | (V_P2SB_CFG_HBDF_DEV << 3) | V_P2SB_CFG_HBDF_FUNC
      );
    PciSegmentWrite16 (
      P2sbBase + R_P2SB_CFG_IBDF,
      (V_P2SB_CFG_IBDF_BUS << 8) | (V_P2SB_CFG_IBDF_DEV << 3) | V_P2SB_CFG_IBDF_FUNC
      );
  }

  //
  // Set P2SB PCI offset 0xE0[4] = 1.
  //
  PciSegmentOr8 (P2sbBase + R_P2SB_CFG_E0, BIT4);


  if (P2SbHandle->PrivateConfig->EnableParityCheck) {
    PciSegmentOr32 (P2sbBase + R_P2SB_CFG_E0, (B_P2SB_CFG_E0_DPEE | B_P2SB_CFG_E0_CPEE | B_P2SB_CFG_E0_DPPEE));
    PciSegmentOr32 (P2sbBase + R_P2SB_CFG_PCICMD, (B_P2SB_CFG_PCICMD_PEE | B_P2SB_CFG_PCICMD_SEE));
  }

  //
  // P2SB power management settings.
  //
  if (P2SbHandle->PrivateConfig->HsleWorkaround) {
    DEBUG ((DEBUG_ERROR, "\nHSLE Workaround to avoid p2sb clock gating\n"));
  } else {
    P2sbPowerGatingConfigure (P2SbHandle);
  }

  //
  // If Legacy IO Low Latency is enabled
  //  set P2SB PCI offset 0xE8[1, 0] = 1's
  //  set P2SB PCI offset 0xEA[1, 0] = 1's
  //
  if (P2SbHandle->PrivateConfig->LegacyIoLowLatency) {
    PciSegmentOr16 (P2sbBase + R_P2SB_CFG_E8,  BIT0 | BIT1);
    PciSegmentOr16 (P2sbBase + R_P2SB_CFG_EA,  BIT0 | BIT1);
  }

  ///
  /// Set LFIORIEC to 0 for Integrated Eembedded Controller (IEC) not supported.
  /// P2SB PCI offset 0x74 = 0
  ///
  if (P2SbHandle->PrivateConfig->IecSupportDisable) {
    PciSegmentWrite8 (P2sbBase + R_P2SB_CFG_LFIORIEC, 0);
  }
}

/**
  Removes the host accessing right to the endpoints.
  This should be called at EndOfPei prior to any 3rd party code execution.

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2SbRemoveEndpointAccess (
  IN  P2SB_HANDLE             *P2SbHandle
  )
{
  UINT64                      P2sbBase;
  UINT32                      RegisterOff;
  UINT32                      BitNo;
  UINT16                      *EndpointMaskTable;
  UINT32                      EndpointMaskTableLength;
  UINTN                       Index;

  P2sbBase = P2SbHandle->Controller->PciCfgBaseAddr;

  if (P2SbHandle->Callback->GetEndpointMaskedPids) {
    P2SbHandle->Callback->GetEndpointMaskedPids (&EndpointMaskTable, &EndpointMaskTableLength);

    for (Index = 0; Index < EndpointMaskTableLength; ++Index) {
      if (EndpointMaskTable[Index] <= 0xFF) {
        ///
        /// Set the bit corresponding to IOSF-SB endpoint id in the P2SB.EPMASK[0-7] register to remove the access
        ///
        RegisterOff = (EndpointMaskTable[Index] / 32) * sizeof (UINT32); // Calculate which 32 bit mask register
        BitNo = EndpointMaskTable[Index] % 32;                          // Calculate the bit number to be set in the mask register
        PciSegmentOr32 ((UINTN) (P2sbBase + R_P2SB_CFG_EPMASK0 + RegisterOff), (UINT32) 1 << BitNo);
        DEBUG ((DEBUG_INFO, "Removing access: EPMASK offset %x, BIT%d, endpoint: %x\n", RegisterOff, BitNo, EndpointMaskTable[Index]));
      } else {
        DEBUG ((DEBUG_INFO, "Error removing endpoint access: incorrect PID\n"));
        continue;
      }
    }
    ///
    ///  Set P2SB PCI offset 23Ch to disabling particular IOSF-SB endpoints.
    ///
    PciSegmentOr32 (P2sbBase + R_P2SB_CFG_EPMASK7, P2SbHandle->PrivateConfig->EndpointMask7);
  } else {
    DEBUG ((DEBUG_INFO, "P2SbRemoveEndpointAccess: No implementation for P2sbGetEndpointMaskedPids\n"));
  }
}

/**
  The function performs P2SB lock programming.

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2sbLock (
  IN  P2SB_HANDLE             *P2SbHandle
  )
{
  UINT64                      P2sbBase;

  if (P2sbControllerNotExist (P2SbHandle->Controller->PciCfgBaseAddr)) {
    return;
  }

  P2sbBase = P2SbHandle->Controller->PciCfgBaseAddr;

  //
  // Program EPMASK.
  // @note, doing EPMASK in end of pei will block the HECI device disable in EndOfPost.
  //
  if (P2SbHandle->P2SbConfig->SbAccessUnlock == 0) {
    P2SbRemoveEndpointAccess (P2SbHandle);
    ///
    ///    Set Endpoint Mask Lock, P2SB PCI offset E2h bit[1] to 1.
    ///    Set PGCB Clock Gating Enable, P2SB PCI offset E2h bit[0] to 1
    ///
    PciSegmentOr8 (P2sbBase + R_P2SB_CFG_E0 + 2, BIT1 | BIT0);
    DEBUG ((DEBUG_INFO, "P2SB PEI MASKLOCK done\n"));
  }

  //
  // Hide P2SB controller in the end of PEI.
  //
  P2sbHideDevice (P2SbHandle);
}

/**
  The function configures P2SB Regbar.

  @param[in] P2SbHandle            P2SB controller handle
**/
VOID
P2sbRegbarConfigure (
  IN  P2SB_HANDLE                       *P2SbHandle
  )
{
  UINT64                      P2sbBase;
  UINT64                      P2sbMmio;

  P2sbBase = P2SbHandle->Controller->PciCfgBaseAddr;
  P2sbMmio = P2SbHandle->Controller->Mmio;
  if (P2sbControllerNotExist (P2sbBase)) {
    return;
  }

  PciSegmentAnd16 (P2sbBase + R_P2SB_CFG_PCICMD, (UINT16) ~B_P2SB_CFG_PCICMD_MEM_SPACE);

  PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_SBREG_BAR, (UINT32) P2sbMmio);
  PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_SBREG_BAR + 4, (UINT32) (P2sbMmio >> 32));

  PciSegmentOr16 (P2sbBase + R_P2SB_CFG_PCICMD, (UINT16) B_P2SB_CFG_PCICMD_MEM_SPACE);
}
