/** @file
  This file contains functions for hybrid storage devices which are devices with both Optane memory
  and QLC SSD.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/PeiHybridStorageLib.h>
#include <Base.h>
#include <Library/BaseMemoryLib.h>
#include <Register/PchRegs.h>
#include <Library/DebugLib.h>
#include <Library/SiScheduleResetLib.h>
#include <Library/PeiMeLib.h>
#include <Library/MemoryAllocationLib.h>
#include <PchResetPlatformSpecific.h>
#include <Library/PchInfoLib.h>
#include <PchPcieRpInfo.h>
#include <Library/PchPcieRpLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PeiServicesLib.h>
#include <Ppi/SiPolicy.h>
#include <PcieRegs.h>
#include <Library/PeiPcieRpInitLib.h>
#include <Library/PcdLib.h>
#include <Register/PchPcieRpRegs.h>
#include <Library/PciExpressHelpersLib.h>
#include <Library/PchFiaLib.h>
#include <Library/PchPciBdfLib.h>
#include <PchHybridStorageHob.h>
#include <Library/FiaSocLib.h>
#include <Library/CpuPcieInfoFruLib.h>

GLOBAL_REMOVE_IF_UNREFERENCED STATIC CONST UINT16 mHybridStorageDidList[] = {
  0x0975,
  0x09AC,
  0x09AD,
  0x09AE,
  0x09AF,
  0x09B0,
  0x09B1,
  0x09B2,
  0x09B3,
  0x09B4,
  0x09B5,
  0x09B6,
  0x09B7,
  0x09B8,
  0x09B9,
  0x09BA,
  0x09BB
};

GLOBAL_REMOVE_IF_UNREFERENCED UINT32 mHybridStorageLocation = 0;

/**
  Determines the location of HybridStorage device on platform.

  @return Returns a 32 bit value with the bit corresponding to the controller where the HybridStorage device is connected set.
**/
UINT32
GetHybridStorageLocation (
  VOID
  )
{
  return mHybridStorageLocation;
}

/**
  Determines the location of CPU HybridStorage device on platform.

  @return Returns CPU rootport number where hybrid storage may be connected
**/
UINT8
GetHybridStorageCpuRpLocation (
  VOID
  )
{
  HYBRID_STORAGE_CONFIG* HybridStorageConfig;

  HybridStorageConfig = GetHybridStoragePolicy ();
  return HybridStorageConfig->CpuRootportUsedForHybridStorage;
}

/**
  Determines the location of Pch HybridStorage device on platform.

  @return Returns Pch rootport number where hybrid storage may be connected
**/
UINT8
GetHybridStoragePchRpForCpuAttach (
  VOID
  )
{
  HYBRID_STORAGE_CONFIG* HybridStorageConfig;

  HybridStorageConfig = GetHybridStoragePolicy();
  return HybridStorageConfig->PchRootportUsedForCpuAttach;
}

/**
  Updates the variable which stores HybridStorage devices location on the platform with new value.

  @param The updated location of HybridStorage device on the platform
**/
VOID
SetHybridStorageLocation (
  UINT32   Location
  )
{
  mHybridStorageLocation = Location;
  if (Location != 0) {
    if (mHybridStorageLocation == 0) {
      DEBUG ((DEBUG_INFO, "This function cannot be used in premem."));
      ASSERT (FALSE);
    }
  }
}

/**
  Determine if device is a Hybrid Storage Device or not

  @param[in] VendorId               Vendor Id of the device
  @param[in] DeviceId               Device Id of the device

  @retval TRUE                      Device is a Hybrid Storage Device
  @retval FALSE                     Device is not a Hybrid Storage Device
**/
BOOLEAN
IsHybridStorageDevice (
  IN    UINT16      VendorId,
  IN    UINT16      DeviceId
  )
{
  UINT8  Index;

  if (VendorId == V_PCH_INTEL_VENDOR_ID) {
    for (Index = 0; Index < ARRAY_SIZE (mHybridStorageDidList); Index++) {
      if (DeviceId == mHybridStorageDidList[Index]) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
  Retrieves and returns Hybrid Storage Policy.

  @retval  pointer to Hybrid Storage Policy
**/
HYBRID_STORAGE_CONFIG*
GetHybridStoragePolicy (
  VOID
  )
{
  EFI_STATUS               Status;
  SI_POLICY_PPI            *SiPolicyPpi;
  HYBRID_STORAGE_CONFIG    *HybridStorageConfig;

  //
  // Get Policy settings through the SiPolicy PPI
  //
  Status = PeiServicesLocatePpi (
             &gSiPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gHybridStorageConfigGuid, (VOID *) &HybridStorageConfig);
  ASSERT_EFI_ERROR (Status);
  return HybridStorageConfig;
}

/**
  Retrieves and returns Hybrid Storage Policy.

  @retval  TRUE if Hybrid Storage Policy is enabled FALSE otherwise
**/
BOOLEAN
IsHybridStoragePolicyEnabled (
  VOID
  )
{
  HYBRID_STORAGE_CONFIG               *HybridStorageConfig;

  HybridStorageConfig = GetHybridStoragePolicy ();
  return (HybridStorageConfig->HybridStorageMode);
}

/**
  Checks and configures clkreq if Hybrid Storage device is present
**/
VOID
HybridStorageCheckAndConfigureClkreq (
  VOID
  )
{
  UINT8                               ControllerConfig[PCH_MAX_PCIE_CONTROLLERS];
  UINT16                              NumOfControllers;
  EFI_STATUS                          Status;
  UINT8                               Index;
  UINT8                               MaxPciePortNum;
  UINT8                               PortIndex;
  UINT8                               ClkReqMap[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8                               ClkReqNum;
  UINT32                              Data32;
  HYBRID_STORAGE_CONFIG               *HybridStorageConfig;

  DEBUG((DEBUG_INFO, "HybridStorageCheckAndConfigureClkreq Entry\n"));
  HybridStorageConfig = GetHybridStoragePolicy ();
  Data32 = 0;
  MaxPciePortNum = GetPchMaxPciePortNum ();
  ASSERT (MaxPciePortNum <= PCH_MAX_PCIE_ROOT_PORTS);
  NumOfControllers = GetPchMaxPcieControllerNum ();

  for (PortIndex = 0; PortIndex < MaxPciePortNum; PortIndex++) {
    ClkReqNum = FindClkReqForUsage (PchClockUsagePchPcie0 + PortIndex);
    if (ClkReqNum == PCH_PCIE_NO_SUCH_CLOCK) {
      ClkReqMap[PortIndex] = PCH_FIA_NO_CLKREQ;
    } else {
      ClkReqMap[PortIndex] = ClkReqNum;
    }
  }

  if (IsHybridStoragePolicyEnabled ()) {
    Status = PeiHeciGetSoftStrpConfigMsg (NumOfControllers, ControllerConfig);
    if (!EFI_ERROR (Status)) {
      for (Index = 0; Index < NumOfControllers; Index++) {
        if (ControllerConfig[Index] == 0x2) {
           Data32 |= BIT0 << Index;
           break;
        }
      }
      DEBUG((DEBUG_INFO, "Updating HybridStorageLocation with %x\n",Data32));
      SetHybridStorageLocation (Data32);
      if (Data32 != 0 || (HybridStorageConfig->CpuRootportUsedForHybridStorage != 0xFF)) {
        HybridStorageClkReqOverride (ClkReqMap, HybridStorageConfig);
      }
    }

  }
  PchFiaAssignPchPciePortsClkReq (ClkReqMap, MaxPciePortNum);
  DEBUG((DEBUG_INFO, "HybridStorageCheckAndConfigureClkreq Exit\n"));
}

/**
  Overrides Clkreq when Hybrid Storage device is connected to the system

  @param[in,out] *ClkReqMap                     Pointer to Clock request map
  @param[in]     *HybridStorageConfig           Pointer to HybridStorageConfig
**/
VOID
HybridStorageClkReqOverride (
  IN UINT8                 *ClkReqMap,
  HYBRID_STORAGE_CONFIG    *HybridStorageConfig
  )
{
  UINT16                              NumOfControllers;
  UINT16                              Index;
  UINT32                              HybridStorageLocation;
  UINT8                               ClkReqNum;

  DEBUG((DEBUG_INFO, "HybridStorageClkReqOverride Entry\n"));
  NumOfControllers = GetPchMaxPcieControllerNum ();
  HybridStorageLocation = GetHybridStorageLocation ();
  ClkReqNum = 0;
  if ( HybridStorageConfig->CpuRootportUsedForHybridStorage == 0xFF) {
    for (Index = 0; Index < NumOfControllers; Index++) {
      if (HybridStorageLocation & (BIT0 << Index)) {
        ClkReqMap[(Index * 4) + 2] = ClkReqMap[Index * PCH_PCIE_CONTROLLER_PORTS];
        break;
      }
    }
  } else {
    //get clkreq used for cpu port and apply that to pch port
    ClkReqNum = FindClkReqForUsage (PchClockUsageCpuPcie0 + HybridStorageConfig->CpuRootportUsedForHybridStorage);
    ClkReqMap[HybridStorageConfig->PchRootportUsedForCpuAttach] = ClkReqNum;
  }
  DEBUG((DEBUG_INFO, "HybridStorageClkReqOverride Exit\n"));
}

/**
  Search for Hybrid Storage device and override softstraps if necessasary
**/
VOID
HybridStorageDynamicDetectionAndConfig (
  VOID
  )
{
  UINT8                               MaxPciePortNum;
  UINT8                               RpIndex;
  UINT64                              RpBase;
  UINT16                              NumOfControllers;
  UINT32                              HybridStorageLocation;
  EFI_STATUS                          Status;
  UINT8                               Index;
  UINT8                               HybridStorageLocationOnPlatform;
  PCH_RESET_DATA                      ResetData;
  UINT8                               TempPciBus;
  PCIE_DEVICE_INFO                    DevInfo;
  PCH_HYBRIDSTORAGE_HOB               HybridStorageHob;
  HYBRID_STORAGE_CONFIG               *HybridStorageConfig;

  DEBUG ((DEBUG_INFO, "HybridStorageDynamicDetectionAndConfig() Start\n"));
  HybridStorageConfig = GetHybridStoragePolicy ();
  if (!IsHybridStoragePolicyEnabled ()) {
    DEBUG ((DEBUG_INFO, "HybridStorage Policy not enabled\n"));
    return;
  }

  ZeroMem (&HybridStorageHob, sizeof(PCH_HYBRIDSTORAGE_HOB));

  //
  // CPU-PCH Hybrid Storage
  //
  if (HybridStorageConfig->CpuRootportUsedForHybridStorage != 0xFF) {
    DEBUG ((DEBUG_INFO, "HybridStorage attached to CPU and PCH combo\n"));
    HybridStorageHob.HybridStorageCpuRpLocation = HybridStorageConfig->CpuRootportUsedForHybridStorage;
    HybridStorageHob.HybridStorageCpuAttachPchRpLocation = HybridStorageConfig->PchRootportUsedForCpuAttach;

    RpBase = CpuPcieBase (HybridStorageConfig->CpuRootportUsedForHybridStorage);
    TempPciBus = PcdGet8(PcdSiliconInitTempPciBusMin);
    GetDeviceInfo(RpBase, TempPciBus, &DevInfo);

    //
    // Update the HOB.
    //
    HybridStorageHob.CpuHybridStorageConnected = IsHybridStorageDevice(DevInfo.Vid, DevInfo.Did);
    BuildGuidDataHob(&gHybridStorageHobGuid, (VOID*)&HybridStorageHob, sizeof(PCH_HYBRIDSTORAGE_HOB));
    return;
  }

  HybridStorageHob.HybridStorageCpuRpLocation = 0xFF;
  HybridStorageHob.HybridStorageCpuAttachPchRpLocation = 0xFF;

  HybridStorageLocation = 0;
  HybridStorageLocationOnPlatform = 0;
  MaxPciePortNum = GetPchMaxPciePortNum ();
  NumOfControllers = GetPchMaxPcieControllerNum ();
  
  TempPciBus = PcdGet8 (PcdSiliconInitTempPciBusMin);
  HybridStorageLocation = GetHybridStorageLocation ();
  //Checking all Pcie controllers for Hybrid Storage device
  for (RpIndex = 0; RpIndex < MaxPciePortNum; RpIndex = RpIndex + 4) {
    DEBUG ((DEBUG_INFO, "HybridStorageDynamicDetectionAndConfig Checking RpIndex %x\n",(RpIndex)));
    RpBase = PchPcieRpPciCfgBase (RpIndex);
    GetDeviceInfo (RpBase, TempPciBus, &DevInfo);
    if (IsHybridStorageDevice (DevInfo.Vid, DevInfo.Did)) {
      DEBUG ((DEBUG_INFO, "HybridStorageDynamicDetectionAndConfig found Hybrid Storage Device on RpIndex %x\n",(RpIndex)));
      HybridStorageLocationOnPlatform |= (BIT0 << (RpIndex/PCH_PCIE_CONTROLLER_PORTS));
      break;
    }
  }
  DEBUG ((DEBUG_INFO, "HybridStorageLocationOnPlatform 0x%x\n", HybridStorageLocationOnPlatform));
  DEBUG ((DEBUG_INFO, "HybridStorageLocation           0x%x\n", HybridStorageLocation));

  HybridStorageHob.HybridStorageLocation = HybridStorageLocationOnPlatform;
  BuildGuidDataHob (&gHybridStorageHobGuid, (VOID*) &HybridStorageHob, sizeof (PCH_HYBRIDSTORAGE_HOB));

  if (HybridStorageLocationOnPlatform == HybridStorageLocation) {
    for (Index = 0; Index < NumOfControllers; Index++) {
      if ((HybridStorageLocation >> Index) & BIT0) {
        PcieEnableTrunkClockGate (Index);
      }
    }
  }
  if (HybridStorageLocationOnPlatform != HybridStorageLocation) {
    Status = PeiHeciOverrideSoftStrapMsg (NumOfControllers, HybridStorageLocationOnPlatform);
    DEBUG ((DEBUG_INFO, "HybridStorageDynamicDetectionAndConfig Detected change in Hybrid storage device location on platform modifying SoftStrap override and scheduling reset\n"));
    if (Status == EFI_SUCCESS) {
      DEBUG ((DEBUG_INFO, "Scheduling global reset\n"));
      CopyMem (&ResetData.Guid, &gPchGlobalResetGuid, sizeof (EFI_GUID));
      StrCpyS (ResetData.Description, PCH_RESET_DATA_STRING_MAX_LENGTH, PCH_PLATFORM_SPECIFIC_RESET_STRING);
      SiScheduleResetSetType (EfiResetPlatformSpecific, &ResetData);
    }
  }
  DEBUG ((DEBUG_INFO, "HybridStorageDynamicDetectionAndConfig() End\n"));
}
