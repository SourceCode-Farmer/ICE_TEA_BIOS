/** @file
  This file provides services for Hybrid Storage policy default initialization

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Library/PeiHybridStoragePolicyLib.h>
#include <Library/SiConfigBlockLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PeiServicesLib.h>
#include <Ppi/SiPolicy.h>
#include <SiPolicyStruct.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>

static COMPONENT_BLOCK_ENTRY mHybridStorageBlock [] = {
  {&gHybridStorageConfigGuid,          sizeof (HYBRID_STORAGE_CONFIG), HYBRID_STORAGE_CONFIG_REVISION, LoadHybridStorageConfigBlockDefault}
};

/**
  Load Config block default

  @param[in] ConfigBlockPointer         Pointer to config block
**/
VOID
LoadHybridStorageConfigBlockDefault (
  IN VOID              *ConfigBlockPointer
  )
{
  HYBRID_STORAGE_CONFIG     *HybridStorageConfig;

  HybridStorageConfig     = ConfigBlockPointer;

  DEBUG ((DEBUG_INFO, "HybridStorageConfig->Header.GuidHob.Name = %g\n", &HybridStorageConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "HybridStorageConfig->Header.GuidHob.Header.HobLength = 0x%x\n", HybridStorageConfig->Header.GuidHob.Header.HobLength));

  HybridStorageConfig->HybridStorageMode = 0;
  HybridStorageConfig->CpuRootportUsedForHybridStorage = 0xFF;
  HybridStorageConfig->PchRootportUsedForCpuAttach = 0xFF;
}

/**
  Get Hybrid Storage config block table total size.

  @retval  Size of Hybrid Storage config block table
**/
UINT16
HybridStorageGetConfigBlockTotalSize (
  VOID
  )
{
  return (UINT16) sizeof (HYBRID_STORAGE_CONFIG);
}

/**
  Add the Hybrid Storage config block.

  @param[in] ConfigBlockTableAddress    The pointer to add config blocks

  @retval EFI_SUCCESS                   The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES          Insufficient resources to create buffer
**/
EFI_STATUS
HybridStorageAddConfigBlocks (
  IN  VOID          *ConfigBlockTableAddress
  )
{
  DEBUG ((DEBUG_INFO, "HybridStorage AddConfigBlocks\n"));
  return AddComponentConfigBlocks (ConfigBlockTableAddress, &mHybridStorageBlock[0], sizeof (mHybridStorageBlock) / sizeof (COMPONENT_BLOCK_ENTRY));
}

/**
  Print Hybrid Storage Config block

  @param[in] SiPolicyPpi          The RC Policy PPI instance
**/
VOID
HybridStoragePrintConfig (
  IN  SI_POLICY_PPI          *SiPolicyPpi
  )
{
  EFI_STATUS                           Status;
  HYBRID_STORAGE_CONFIG                *HybridStorageConfig;

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gHybridStorageConfigGuid, (VOID *) &HybridStorageConfig);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "------------------ Hybrid Storage Config ------------------\n"));
  DEBUG ((DEBUG_INFO, " Hybrid Storage Mode                        = %x\n", HybridStorageConfig->HybridStorageMode));
  DEBUG ((DEBUG_INFO, " CPU Rootport used for Hybrid Storage       = %x\n", HybridStorageConfig->CpuRootportUsedForHybridStorage));
  DEBUG ((DEBUG_INFO, " PCH Rootport used for Hybrid Storage when two lanes are connected to CPU    = %x\n", HybridStorageConfig->PchRootportUsedForCpuAttach));
}