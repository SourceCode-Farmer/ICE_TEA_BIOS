/** @file
  Serial IO I3c Private Lib Null implementation.
  All function in this library is available for PEI, DXE, and SMM,
  But do not support UEFI RUNTIME environment call.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/BaseLib.h>

/**
  Gets Pci Config control offset

  @retval                            Config control offset
**/
UINT16
GetSerialIoI3cConfigControlOffset (
  VOID
  )
{
  return 0;
}

/**
  Gets I3c Device Id

  @retval                            Device Id
**/
UINT16
GetSerialIoI3cDeviceId (
  VOID
  )
{
  return 0;
}

/**
  Configures Serial IO Controller before control is passd to the OS

  @param[in] I3cNumber         I3c Number
  @param[in] I3cMode           SerialIo I3c Mode

**/
VOID
SerialIoI3cBootHandler (
  IN UINT8                      I3cNumber,
  IN UINT8                      I3cMode
  )
{
}

/**
  Sets Pme Control Status and Command register values required for S3 Boot Script

  @param[in]     I3cNumber         I3c Number
  @param[in]     I3cMode           SerialIo I3c Mode
  @param[in/out] S3PciCfgBase      S3 Boot Script Pci Config Base
  @param[in/out] Command           Pci Command register data to save
  @param[in/out] Pme               Pci Pme Control register data to save

**/
VOID
SerialIoI3cS3Handler (
  IN     UINT8                 I3cNumber,
  IN     UINT8                 I3cMode,
  IN OUT UINT64                *S3PciCfgBase,
  IN OUT UINT32                *Command,
  IN OUT UINT32                *Pme
  )
{
}
