/** @file
  Initializes Serial IO Spi Controllers.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/SiPolicy.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/PeiItssLib.h>
#include <Library/SerialIoAccessLib.h>
#include <Library/SerialIoPrivateLib.h>
#include <Library/GpioLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/GpioPrivateLib.h>
#include <Library/GpioNativePads.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PsfLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/SerialIoSpiLib.h>
#include <IndustryStandard/Pci30.h>
#include <Register/PchRegs.h>
#include <Register/SerialIoRegs.h>
#include <Register/PchPcrRegs.h>
#include "PeiSerialIoInitPrivateLib.h"

/**
  Configures GPIO for each Serial IO SPI Controller

  @param[in] SpiNumber         SPI Number
  @param[in] SpiDeviceConfig   SerialIo SPI Config

**/
VOID
STATIC
SerialIoSpiGpioConfiguration (
  IN UINT8                 SpiNumber,
  IN SERIAL_IO_SPI_CONFIG  *SpiDeviceConfig
  )
{
  UINT8                       Index;

  GpioSetNativePadByFunction (GPIO_FUNCTION_SERIAL_IO_SPI_MOSI (SpiNumber), SpiDeviceConfig->PinMux.Mosi);
  GpioSetInputInversion (GpioGetNativePadByFunctionAndPinMux (GPIO_FUNCTION_SERIAL_IO_SPI_MOSI (SpiNumber), SpiDeviceConfig->PinMux.Mosi), 0);
  GpioSetNativePadByFunction (GPIO_FUNCTION_SERIAL_IO_SPI_MISO (SpiNumber), SpiDeviceConfig->PinMux.Miso);
  GpioSetInputInversion (GpioGetNativePadByFunctionAndPinMux (GPIO_FUNCTION_SERIAL_IO_SPI_MISO (SpiNumber), SpiDeviceConfig->PinMux.Miso), 0);
  GpioSetNativePadByFunction (GPIO_FUNCTION_SERIAL_IO_SPI_CLK (SpiNumber), SpiDeviceConfig->PinMux.Clk);
  GpioSetInputInversion (GpioGetNativePadByFunctionAndPinMux (GPIO_FUNCTION_SERIAL_IO_SPI_CLK (SpiNumber), SpiDeviceConfig->PinMux.Clk), 0);

  for (Index = 0; Index < GetPchMaxSerialIoSpiChipSelectsNum (); Index++) {
    if (SpiDeviceConfig->CsEnable[Index]) {
      GpioSetNativePadByFunction (GPIO_FUNCTION_SERIAL_IO_SPI_CS (SpiNumber, Index), SpiDeviceConfig->PinMux.Cs[Index]);
      GpioSetInputInversion (GpioGetNativePadByFunctionAndPinMux (GPIO_FUNCTION_SERIAL_IO_SPI_CS (SpiNumber, Index), SpiDeviceConfig->PinMux.Cs[Index]), 0);
    }
  }
}

/**
  Configures Serial IO Controller

  @param[in] SpiNumber         SPI Number
  @param[in] SpiDeviceConfig   SerialIo SPI Config
  @param[in] PsfDisable        Applies only for SerialIoSpiDisabled devices.
                               TRUE  - Device will be disabled in PSF, and will no longer enumerate on PCI.
                               FALSE - PSF configuration is left unmodified.

**/
VOID
SerialIoSpiConfiguration (
  IN UINT8                      SpiNumber,
  IN SERIAL_IO_SPI_CONFIG       *SpiDeviceConfig,
  IN BOOLEAN                    PsfDisable
  )
{
  SERIAL_IO_PCI_DEVICE_STATE  PciState;
  UINT64                      PciCfgBase;
  UINT64                      Base;
  BOOLEAN                     TempBar;
  BOOLEAN                     Hidden;

  PciState.PciCfgBar0 = 0x0;
  TempBar             = FALSE;
  Hidden              = FALSE;
  Base                = 0x0;

  //
  // This is to prevent from overflow of array access.
  //
  if (SpiNumber >= PCH_MAX_SERIALIO_SPI_CONTROLLERS) {
    return;
  }

  //
  // Override previous SPI configuration
  //
  SerialIoPciPsfEnable (GetSerialIoSpiConfigControlOffset (SpiNumber), PsfSerialIoSpiPort (SpiNumber));

  PciCfgBase = SerialIoSpiPciCfgBase (SpiNumber);

  SerialIoEnablePowerGating (PciCfgBase);

  switch (SpiDeviceConfig->Mode) {
    case SerialIoSpiDisabled:
      SerialIoSetD3 (PciCfgBase);
      if (PsfDisable) {
        PsfDisableDevice (PsfSerialIoSpiPort (SpiNumber));
      }
      return;
    case SerialIoSpiHidden:
      SerialIoSetFixedMmio (PciCfgBase,
                            GetSerialIoSpiFixedMmioAddress (SpiNumber),
                            GetSerialIoSpiFixedPciCfgAddress (SpiNumber),
                            GetSerialIoSpiConfigControlOffset (SpiNumber),
                            PsfSerialIoSpiPort (SpiNumber)
                            );
      PciCfgBase = GetSerialIoSpiFixedPciCfgAddress (SpiNumber);
      Hidden     = TRUE;
      break;
    case SerialIoSpiPci:
      SerialIoPciSave (PciCfgBase, &PciState);
      SerialIoPciEnable (PciCfgBase, GetSerialIoSpiConfigControlOffset (SpiNumber), PsfSerialIoSpiPort (SpiNumber));
      TempBar = TRUE;
      break;
    default:
      return;
  }

  if (SerialIoMmioEnable (PciCfgBase, TempBar, Hidden, &Base)) {
    SerialIoSpiChipSelectConfig (
      Base,
      SpiDeviceConfig->DefaultCsOutput,
      SpiDeviceConfig->CsMode,
      SpiDeviceConfig->CsState,
      (SpiDeviceConfig->CsEnable[0] | (SpiDeviceConfig->CsEnable[1] << 1)),
      (SpiDeviceConfig->CsPolarity[0] | (SpiDeviceConfig->CsPolarity[1] << 1))
      );
    if (SpiDeviceConfig->Mode == SerialIoSpiPci) {
      if (PciState.PciCfgBar0 == 0x0) {
        SerialIoSetD3 (PciCfgBase);
        SerialIoMmioDisable (PciCfgBase, TempBar);
      } else {
        SerialIoPciRestore (PciCfgBase, &PciState);
      }
    }
  }

  SerialIoSpiGpioConfiguration (SpiNumber, SpiDeviceConfig);
}

/**
  Configures Serial IO Spi Controllers

  @param[in] SiPolicy         Silicon policy
  @param[in] SerialIoConfig   SerialIo Config Block

**/
VOID
SerialIoSpiInit (
  IN SI_POLICY_PPI               *SiPolicy,
  IN SERIAL_IO_CONFIG            *SerialIoConfig
  )
{
  UINT8    SpiNumber;
  UINT8    InterruptPin;
  UINT8    Irq;

  DEBUG ((DEBUG_INFO, "SerialIoSpiInit() Start\n"));

  for (SpiNumber = 0; SpiNumber < GetPchMaxSerialIoSpiControllersNum (); SpiNumber++) {
    if ((SerialIoConfig->SpiDeviceConfig[SpiNumber].Mode == SerialIoSpiDisabled) &&
        (SerialIoSpiFuncNumber (SpiNumber) == 0x0)) {
      continue;
    }
    if (SerialIoConfig->SpiDeviceConfig[SpiNumber].Mode != SerialIoSpiDisabled) {
      ItssGetDevIntConfig (SiPolicy, SerialIoSpiDevNumber (SpiNumber), SerialIoSpiFuncNumber (SpiNumber), &InterruptPin, &Irq);
      SerialIoInterruptSet (GetSerialIoSpiConfigControlOffset (SpiNumber), InterruptPin, Irq);
    }
    SerialIoSpiConfiguration (SpiNumber, &SerialIoConfig->SpiDeviceConfig[SpiNumber], TRUE);
  }

  DEBUG ((DEBUG_INFO, "SerialIoSpiInit() End\n"));
}

/**
  Configures Serial IO Spi Function 0 Disabled Controllers

  @param[in] SiPolicy         Silicon policy
  @param[in] SerialIoConfig   SerialIo Config Block

**/
VOID
SerialIoSpiFunction0Disable (
  IN SI_POLICY_PPI               *SiPolicy,
  IN SERIAL_IO_CONFIG            *SerialIoConfig
  )
{
  UINT8    SpiNumber;
  BOOLEAN  PsfDisable;
  UINT8    InterruptPin;
  UINT8    Irq;

  for (SpiNumber = 0; SpiNumber < GetPchMaxSerialIoSpiControllersNum (); SpiNumber++) {
    PsfDisable = TRUE;
    if ((SerialIoConfig->SpiDeviceConfig[SpiNumber].Mode == SerialIoSpiDisabled) &&
        (SerialIoSpiFuncNumber (SpiNumber) == 0x0)) {
      if (SerialIoHigherFunctionsEnabled (SerialIoSpiDevNumber (SpiNumber))) {
        SerialIoBar1Disable (GetSerialIoSpiConfigControlOffset (SpiNumber), PsfSerialIoSpiPort (SpiNumber));
        ItssGetDevIntConfig (SiPolicy, SerialIoSpiDevNumber (SpiNumber), SerialIoSpiFuncNumber (SpiNumber), &InterruptPin, &Irq);
        SerialIoInterruptSet (GetSerialIoSpiConfigControlOffset (SpiNumber), InterruptPin, Irq);
        PsfDisable = FALSE;
      }
      SerialIoSpiConfiguration (SpiNumber, &SerialIoConfig->SpiDeviceConfig[SpiNumber], PsfDisable);
    }
  }
}

