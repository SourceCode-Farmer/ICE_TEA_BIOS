/** @file
 Serial IO I2C Private Registers

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

//
// SDA Hold Defines
//
#define V_SERIAL_IO_I2C_MEM_SDA_TX_100_1000_KHZ    0x10
#define V_SERIAL_IO_I2C_MEM_SDA_RX_100_1000_KHZ    0x20
#define V_SERIAL_IO_I2C_MEM_SDA_TX_1000_3400_KHZ   0x20
#define V_SERIAL_IO_I2C_MEM_SDA_RX_1000_3400_KHZ   0x0
#define V_SERIAL_IO_I2C_MEM_SDA_TX_3400_INF_KHZ    0x10
#define V_SERIAL_IO_I2C_MEM_SDA_RX_3400_INF_KHZ    0x0

//
// I2C Clock SCL Defines
//
#define V_SERIAL_IO_I2C_MEM_SS_SCL_HCNT_20          2045
#define V_SERIAL_IO_I2C_MEM_SS_SCL_LCNT_20          2880
#define V_SERIAL_IO_I2C_MEM_SS_SCL_HCNT_50          818
#define V_SERIAL_IO_I2C_MEM_SS_SCL_LCNT_50          1152
#define V_SERIAL_IO_I2C_MEM_SS_SCL_HCNT_70          706
#define V_SERIAL_IO_I2C_MEM_SS_SCL_LCNT_70          612
#define V_SERIAL_IO_I2C_MEM_SS_SCL_HCNT             409
#define V_SERIAL_IO_I2C_MEM_SS_SCL_LCNT             576
#define V_SERIAL_IO_I2C_MEM_SS_FCL_HCNT_400_1000    63
#define V_SERIAL_IO_I2C_MEM_SS_FCL_LCNT_400_1000    179
#define V_SERIAL_IO_I2C_MEM_SS_FCL_HCNT_1000_3400   28
#define V_SERIAL_IO_I2C_MEM_SS_FCL_LCNT_1000_3400   65
#define V_SERIAL_IO_I2C_MEM_HS_FCL_HCNT             16
#define V_SERIAL_IO_I2C_MEM_HS_FCL_LCNT             34

