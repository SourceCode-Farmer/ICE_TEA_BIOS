/** @file
  @This file implements the functions to get VMD info.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi.h>
#include <IndustryStandard/Pci22.h>
#include <Register/VmdRegs.h>
#include <Library/VmdInfoLib.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PciSegmentLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Library/CpuPlatformLib.h>

#define BAR_VALUE_MASK   0xFFFFFFF0

/**
  GetVmdBusNumber: Get VMD Bus Number

  @retval PCI bus number for VMD
**/
UINT8
GetVmdBusNumber (
  VOID
  )
{
  return (UINT8)VMD_BUS_NUM;
}

/**
  GetVmdDevNumber: Get VMD Dev Number

  @retval PCI dev number for VMD
**/
UINT8
GetVmdDevNumber (
  VOID
  )
{
  return (UINT8)VMD_DEV_NUM;
}

/**
  GetVmdFunNumber: Get VMD Fun Number

  @retval PCI fun number for VMD
**/
UINT8
GetVmdFuncNumber (
  VOID
  )
{
  return (UINT8)VMD_FUN_NUM;
}

/**
  IsVmdEnabled: Check if VMD is enabled by reading the VMD Device Id

  @retval TRUE  if  VMD is enabled
  @retval False if  VMD is not enabled
**/

BOOLEAN
IsVmdEnabled ()
{
  UINT16 DevId;
  DevId = PciSegmentRead16 (
            PCI_SEGMENT_LIB_ADDRESS (
              SA_SEG_NUM,
              VMD_BUS_NUM,
              VMD_DEV_NUM,
              VMD_FUN_NUM,
              PCI_DEVICE_ID_OFFSET
            )
          );
  if (DevId == 0xFFFF) {
    return FALSE;
  }
  return TRUE;
}



/**
  IsVmdLocked: Get the status of VMD lock for critical config space registers of VMD

  @retval TRUE if  VMD lock bit is set
  @retval False if  VMD lock bit is not set
**/
BOOLEAN
IsVmdLocked (
  VOID
  )
{
  if ((PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, VMD_BUS_NUM, VMD_DEV_NUM, VMD_FUN_NUM, R_VMCONFIG_0_14_0_PCI_VMD_REG_OFFSET)) & BIT0) != 0) {
    return TRUE;
  }
  return FALSE;
}

/**
  IsVmdSupported: Get the status of VMD support bit from capability register.

  @retval TRUE  if  VMD is supported
  @retval False if  VMD is not supported
**/
BOOLEAN
IsVmdSupported (
  VOID
  )
{
#if (FixedPcdGetBool (PcdVmdEnable) == 1)
  UINT32 Data32;
  Data32 = PciSegmentRead32 (
             PCI_SEGMENT_LIB_ADDRESS (
               SA_SEG_NUM,
               SA_MC_BUS,
               SA_MC_DEV,
               SA_MC_FUN,
               R_SA_MC_CAPID0_B
             )
           );
  if (Data32 & V_SA_MC_CAPID0_B_VMD_DIS) {
    DEBUG ((DEBUG_INFO, "------- Vmd is not supported -----\n"));
    return FALSE;
  }
  return TRUE;
#else
  return FALSE;
#endif
}


/**
  ProgramVmdTempBars: Program VMD temporary bars.

  @param[in]     VmdCfgBarBase    temporary address for CfgBar.
  @param[in]     VmdMemBar1Base   temporary address for CfgBar.
  @param[in]     VmdMemBar1Base   temporary address for CfgBar.

  @retval VOID
**/
VOID
ProgramVmdTempBars(
    IN UINT32 VmdCfgBarBase,
    IN UINT32 VmdMemBar1Base,
    IN UINT32 VmdMemBar2Base
  )
{
  UINTN           DeviceBaseAddress;

  ///
  /// Program VMD  temp PEI Bars
  ///
  DEBUG((DEBUG_INFO, "Programming temp VMD Bars \n"));
  DeviceBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, VMD_BUS_NUM, VMD_DEV_NUM, VMD_FUN_NUM,0);
  PciSegmentWrite32 (DeviceBaseAddress + R_CFGBAR_N0_0_14_0_PCI_VMD_REG_OFFSET, VmdCfgBarBase);  //32MB
  PciSegmentWrite32 (DeviceBaseAddress + R_MEMBAR1_N0_0_14_0_PCI_VMD_REG_OFFSET, VmdMemBar1Base);  //32MB
  PciSegmentWrite32 (DeviceBaseAddress + R_MEMBAR2_N0_0_14_0_PCI_VMD_REG_OFFSET, VmdMemBar2Base);  //1MB
  ///
  /// Read back the programmed BARs
  ///
  DEBUG ((DEBUG_INFO, "VMD CFG BAR 0X%x:\n", (PciSegmentRead32 (DeviceBaseAddress + R_CFGBAR_N0_0_14_0_PCI_VMD_REG_OFFSET)) & BAR_VALUE_MASK));
  DEBUG ((DEBUG_INFO, "VMD MemBar1 0X%x:\n", (PciSegmentRead32 (DeviceBaseAddress + R_MEMBAR1_N0_0_14_0_PCI_VMD_REG_OFFSET)) & BAR_VALUE_MASK));
  DEBUG ((DEBUG_INFO, "VMD MemBar2 0X%x:\n", (PciSegmentRead32 (DeviceBaseAddress + R_MEMBAR2_N0_0_14_0_PCI_VMD_REG_OFFSET)) & BAR_VALUE_MASK));
}

/**
  ClearVmdTempBars: Clear VMD temporary bars.

  @retval VOID
**/
VOID
ClearVmdTempBars(
  VOID
  )
{
  UINTN           DeviceBaseAddress;

  ///
  /// Clear VMD  temp PEI Bars
  ///
  if (IsVmdEnabled() == TRUE) {
    DeviceBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, VMD_BUS_NUM, VMD_DEV_NUM, VMD_FUN_NUM,0);
    DEBUG ((DEBUG_INFO, "VMD: Reading VMD temp PEI Bars to make sure its cleared\n"));
    // Read back the programmed BARs
    DEBUG ((DEBUG_INFO, "VMD CFG BAR 0X%x:\n", (PciSegmentRead32 (DeviceBaseAddress + R_CFGBAR_N0_0_14_0_PCI_VMD_REG_OFFSET)) & BAR_VALUE_MASK));
    DEBUG ((DEBUG_INFO, "VMD MemBar1 0X%x:\n", (PciSegmentRead32 (DeviceBaseAddress + R_MEMBAR1_N0_0_14_0_PCI_VMD_REG_OFFSET)) & BAR_VALUE_MASK));
    DEBUG ((DEBUG_INFO, "VMD MemBar2 0X%x:\n", (PciSegmentRead32 (DeviceBaseAddress + R_MEMBAR2_N0_0_14_0_PCI_VMD_REG_OFFSET)) & BAR_VALUE_MASK));
    if ((PciSegmentRead32 (DeviceBaseAddress + R_CFGBAR_N0_0_14_0_PCI_VMD_REG_OFFSET) & BAR_VALUE_MASK) != 0) {
      DEBUG ((DEBUG_INFO, "VMD: Clearing VMD  temp PEI Bars \n"));
      PciSegmentWrite32 (DeviceBaseAddress + PCI_COMMAND_OFFSET, 0x0);
      PciSegmentWrite32 (DeviceBaseAddress + R_CFGBAR_N0_0_14_0_PCI_VMD_REG_OFFSET, 0x0);
      PciSegmentWrite32 (DeviceBaseAddress + R_MEMBAR1_N0_0_14_0_PCI_VMD_REG_OFFSET, 0x0);
      PciSegmentWrite32 (DeviceBaseAddress + R_MEMBAR2_N0_0_14_0_PCI_VMD_REG_OFFSET, 0x0);
    } else {
      DEBUG ((DEBUG_INFO, "VMD: Temp PEI Bars are cleared!!\n"));
    }
    ///
    /// Read back the BARs
    ///
    DEBUG ((DEBUG_INFO, "VMD CFG BAR 0X%x:\n", (PciSegmentRead32 (DeviceBaseAddress + R_CFGBAR_N0_0_14_0_PCI_VMD_REG_OFFSET)) & BAR_VALUE_MASK));
    DEBUG ((DEBUG_INFO, "VMD MemBar1 0X%x:\n", (PciSegmentRead32 (DeviceBaseAddress + R_MEMBAR1_N0_0_14_0_PCI_VMD_REG_OFFSET)) & BAR_VALUE_MASK));
    DEBUG ((DEBUG_INFO, "VMD MemBar2 0X%x:\n", (PciSegmentRead32 (DeviceBaseAddress + R_MEMBAR2_N0_0_14_0_PCI_VMD_REG_OFFSET)) & BAR_VALUE_MASK));
  }
}
