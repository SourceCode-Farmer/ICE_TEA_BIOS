/** @file
  VMD Logical Volume RTD3 support in ACPI

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

//
// Include this file in VMD scope to enable VMD Logical volume support
//

//
// NOTE: For platform implementers.
// VR3D method checks if RTD3 flow is allowed for given device
// RTD3 flow is enabled if it hasn't been globally locked or device isn't part of the RAID volume
// VMD driver will globally lock RTD3 flows in following scenarios:
// 1. Volume creation
// 2. System shutdown
// 3. System transition to S3/S4
//
// To enable Logical volume functionality platform code should check in _ON/_OFF methods of the
// power resource associated with the device that can be a part of the VMD logical volume if RTD3 flow has not been blocked.
// If RTD3 flow has been blocked platform code should not execute D3Cold entry logic.
//
// To allow VMD volume to manage PCIe and SATA port power implement VPON/VPOF/VSON/VSOF methods.
// VPON - Method to turn PCIe port on.
// VPOF - Method to turn PCIe port off.
// VSON - Method to turn SATA port on.
// VSOF - Method to turn SATA port off.
//
External(\RTD3, FieldUnitObj) // Global RTD3 Support in BIOS setup menu
External(\STD3, FieldUnitObj) /// 0: D3 disable. 1: D3Hot support for Storages. 2: D3Cold support for Storages.
External(\HBCL, FieldUnitObj) // Cpu Root port used for hybrid storage (0 to 2)
External(\HBPL, FieldUnitObj) // Pch Root port used for hybrid storage (0 to 16)

Name(VLVN, 0) // VMD Logical Volume Number
Name(VV0D, Package(32){0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0}) // List of ACPI address for PCIe device or ACPI address for SATA device for Volume 0
Name(VV1D, Package(32){0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0}) // List of ACPI address for PCIe device or ACPI address for SATA device for Volume 1
Name(VV2D, Package(32){0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0}) // List of ACPI address for PCIe device or ACPI address for SATA device for Volume 2
Name(VV3D, Package(32){0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0}) // List of ACPI address for PCIe device or ACPI address for SATA device for Volume 3
Name(VD3A, 0) //VMD Global RTD3 Allowed Flag
Name(VV0S, 0) // Volume 0 Size
Name(VV1S, 0) // Volume 1 Size
Name(VV2S, 0) // Volume 2 Size
Name(VV3S, 0) // Volume 3 Size


//
// RTD3 support for Hybrid storage connected to CPU slot
//
Include("VmdHybridStorage.asl")
//
// Store the volume package sent by the RST driver
//
// Arg0: Reference to volume package passed from the RST driver
// Arg1: Reference to volume package to which we should store the data
// Arg2: Reference to volume size object
//
Method(SVRP, 3) {
  Subtract(SizeOf(Arg0), 1, Local0)
  ADBG(Concatenate ("Volume size is: ", Local0))

  If (LGreater(Local0, 32)) {
    ADBG ("Volume size over 32")
    Return ()
  }

  Store (Local0, Arg2)
  Store(0, Local1) // Iterator
  While (LLess (Local1, Local0)) {
    Store (DeRefOf(Index (Arg0, Add (Local1, 1))), Index (Arg1, Local1))
    ADBG(Concatenate("Adding to volume: ", ToHexString(DerefOf(Index(Arg1,Local1)))))
    Increment (Local1)
  }
}

//
// _DSM- _DSM method for VMD Logical Volume
// Arg0 - UUID: 9CD9CDDD-8845-4AFD-8392-31C4EB87CABD
// Arg1 - Revision ID: 0x01 (Integer)
// Arg2 - Function Index: 0x01 (Integer)
// Arg3 - Rtd3AcpiVmdDevList : ACPI address for PCIe device or ACPI address for SATA device is stored
//
Method (_DSM, 4, Serialized,,, {BuffObj, IntObj, IntObj, PkgObj}) {
  If (LEqual(Arg0, ToUUID ("9CD9CDDD-8845-4AFD-8392-31C4EB87CABD"))) {
    Switch(ToInteger(Arg2)) {
      Case (0) {
        // Standard query - A bitmask of functions supported
        // Supports function 0-5
        Name(RBUF, Buffer(1){0x00})
        CreateByteField (RBUF, 0, SFUN)
        Store (0x3F, SFUN)
        Return (RBUF)
      }
      Case (1) {
        // Assign ports to this VMD RAID volume - _SAVE_RTD3_PACKAGE
        // Arg3 - Rtd3AcpiVmdDevList : Volume Number and ACPI address for PCIe device or ACPI address for SATA device is stored
        Store(ToInteger(DerefOf(Index(Arg3,0))), VLVN) // Volume Number (0-3)

        If(LEqual(VLVN,0)) { // if Volume number is  0 store address package to VV0D
          SVRP(Arg3, VV0D, RefOf(VV0S))
          Return(0)
        }
        ElseIf(LEqual(VLVN,1)) {
          SVRP(Arg3, VV1D, RefOf(VV1S))
          Return(0)
        }
        ElseIf(LEqual(VLVN,2)) {
          SVRP(Arg3, VV2D, RefOf(VV2S))
          Return(0)
        }
        ElseIf(LEqual(VLVN,3)) {
          SVRP(Arg3, VV3D, RefOf(VV3S))
          Return(0)
        }
        Else { Return(1) }
      }
      Case (2) {
      // VMD Driver access to turn ON ports.
      // Arg3 - List of disk adress that are part of logical volume
        Name(VVON, Package(32){0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0}) // List of ACPI address for PCIe device or ACPI address for SATA device for Logical Volume
        Store(SizeOf(Arg3), Local0)
        Store(0, Local1)
        If(LLessEqual(Local0, 32)) { // Logical Volume can contain maximum of 32 disks
          While (LLess(Local1, Local0)) {
            Store(ToInteger(DerefOf(Index(Arg3, Local1))), Index(VVON, Local1))
            Increment(Local1)
          }
          ^VLON(Local0, VVON);
        }
        Return(0)
      }
      Case (3) {
      // VMD Driver access to turn OFF a port.
      // Arg3 - List of disk adress that are part of logical volume
        Name(VVOF, Package(32){0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0}) // List of ACPI address for PCIe device or ACPI address for SATA device for Logical Volume
        Store(SizeOf(Arg3), Local0)
        Store(0, Local1)
        If(LLessEqual(Local0, 32)) { // Logical Volume can contain maximum of 32 disks
          While (LLess(Local1, Local0)) {
            Store(ToInteger(DerefOf(Index(Arg3, Local1))), Index(VVOF, Local1))
            Increment(Local1)
          }
          ^VLOF(Local0, VVOF);
        }
        Return(0)
      }
      Case(4) {
      // RST Driver read VMD Volume mask - Unsupported in the driver
        Return(0)
      }
      Case(5) {
      // Global Lock to allow/disallow RTD3 for VMD Raid Volumes
      // Arg3 - Lock bit to enable or disable RTD3 flow for VMD logical volume
        Store(DerefOf(Index(Arg3,0)), VD3A)
        Return(0)
      }
      Default {
        Return(2)
      }
    }
  }
  Return(0)
}

// VLON  : Method to turn ON ports per port addres
// Arg0 : Package size
// Arg1 :  List of Port Address
Method(VLON,2,Serialized,,, {IntObj, PkgObj}) {
  Name(DRVA, 0)  /// Storage for ACPI Address of the port
  Store (Arg0, Local1)
  Store (0, Local0) // use this as the index value
  While(LGreater(Local1, Local0))
  {
    Store(ToInteger(DerefOf(Index(Arg1, Local0))), DRVA)
    Switch(ToInteger(DRVA)) {
      Case (0x80E0FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x0))) {
          If((HBSL & 0x01)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP01.RSPT, ^RP01.RSPI)) {
              \_SB.PSD0(^RP03.RSPI) // Restore power to the modPHY
              HBCM(^RP03.RSPI, 1) // UnMask Hybrid Partner CLKREQ
            }
          }

          ^RP01.VPON()

          If((HBSL & 0x01)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP01.RSPT, ^RP01.RSPI)) {
              ^RP03.RLA() // Hybrid Partner Link Active
              Sleep(100) // Sleep for 100ms after transition to link active
            }
          }
        }
      }
      Case (0x80E1FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x1))) {
          ^RP02.VPON()
        }
      }
      Case (0x80E2FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x2))) {
          If((HBSL & 0x01)) // Do nothing it is a RPn+2 port
          {
          }
          Else {
            ^RP03.VPON()
          }
        }
      }
      Case (0x80E3FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x3))) {
          ^RP04.VPON()
        }
      }
      Case (0x80E4FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x4))) {
          If((HBSL & 0x02)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP05.RSPT, ^RP05.RSPI)) {
              \_SB.PSD0(^RP07.RSPI) // Restore power to the modPHY
              HBCM(^RP07.RSPI, 1) // UnMask Hybrid Partner CLKREQ
            }
          }

          ^RP05.VPON()

          If((HBSL & 0x02)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP05.RSPT, ^RP05.RSPI)) {
              ^RP07.RLA() // Hybrid Partner Link Active
              Sleep(100) // Sleep for 100ms after transition to link active
            }
          }
        }
      }
      Case (0x80E5FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x5))) {
          ^RP06.VPON()
        }
      }
      Case (0x80E6FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x6))) {
          If((HBSL & 0x02)) // Do nothing it is a RPn+2 port
          {
          }
          Else {
            ^RP07.VPON()
          }
        }
      }
      Case (0x80E7FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x7))) {
          ^RP08.VPON()
        }
      }
      Case (0x80E8FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x8))) {
          If((HBSL & 0x04)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP09.RSPT, ^RP09.RSPI)) {
              \_SB.PSD0(^RP11.RSPI) // Restore power to the modPHY
              HBCM(^RP11.RSPI, 1) // UnMask Hybrid Partner CLKREQ
            }
          }

          ^RP09.VPON()

          If((HBSL & 0x04)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP09.RSPT, ^RP09.RSPI)) {
              ^RP11.RLA() // Hybrid Partner Link Active
              Sleep(100) // Sleep for 100ms after transition to link active
            }
          }
        }
      }
      Case (0x80E9FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x9))) {
          ^RP10.VPON()
        }
      }
      Case (0x80EAFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xA))) {
          If((HBSL & 0x04)) // Do nothing it is a RPn+2 port
          {
          }
          Else {
            ^RP11.VPON()
          }
        }
      }
      Case (0x80EBFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xB))) {
          ^RP12.VPON()
        }
      }
      Case (0x80ECFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xC))) {
          If((HBSL & 0x08)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP13.RSPT, ^RP13.RSPI)) {
              \_SB.PSD0(^RP15.RSPI) // Restore power to the modPHY
              HBCM(^RP15.RSPI, 1) // UnMask Hybrid Partner CLKREQ
            }
          }

          ^RP13.VPON()

          If((HBSL & 0x08)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP13.RSPT, ^RP13.RSPI)) {
              ^RP15.RLA() // Hybrid Partner Link Active
              Sleep(100) // Sleep for 100ms after transition to link active
            }
          }
        }
      }
      Case (0x80EDFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xD))) {
          ^RP14.VPON()
        }
      }
      Case (0x80EEFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xE))) {
          If((HBSL & 0x08)) // Do nothing it is a RPn+2 port
          {
          }
          Else {
            ^RP15.VPON()
          }
        }
      }
      Case (0x80EFFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xF))) {
          ^RP16.VPON()
        }
      }
      Case (0x80D8FFFF) {
        If((HBSL & 0x10)) // Check if hybrid device is present on RPn root port
        {
          If (D3CV(^RP17.RSPT, ^RP17.RSPI)) {
            \_SB.PSD0(^RP19.RSPI) // Restore power to the modPHY
            HBCM(^RP19.RSPI, 1) // UnMask Hybrid Partner CLKREQ
          }
        }

        ^RP17.VPON()

        If((HBSL & 0x10)) // Check if hybrid device is present on RPn root port
        {
          If (D3CV(^RP17.RSPT, ^RP17.RSPI)) {
            ^RP19.RLA() // Hybrid Partner Link Active
            Sleep(100) // Sleep for 100ms after transition to link active
          }
        }
      }
      Case (0x80D9FFFF) {
        ^RP18.VPON()
      }
      Case (0x80DAFFFF) {
        If((HBSL & 0x10)) // Do nothing it is a RPn+2 port
        {
        }
        Else {
          ^RP19.VPON()
        }
      }
      Case (0x80DBFFFF) {
        ^RP20.VPON()
      }
      Case (0x80DCFFFF) {
        If((HBSL & 0x20)) // Check if hybrid device is present on RPn root port
        {
          If (D3CV(^RP21.RSPT, ^RP21.RSPI)) {
            \_SB.PSD0(^RP23.RSPI) // Restore power to the modPHY
            HBCM(^RP23.RSPI, 1) // UnMask Hybrid Partner CLKREQ
          }
        }

        ^RP21.VPON()

        If((HBSL & 0x20)) // Check if hybrid device is present on RPn root port
        {
          If (D3CV(^RP21.RSPT, ^RP21.RSPI)) {
            ^RP23.RLA() // Hybrid Partner Link Active
            Sleep(100) // Sleep for 100ms after transition to link active
          }
        }
      }
      Case (0x80DDAFFFF) {
        ^RP22.VPON()
      }
      Case (0x80DEFFFF) {
        If((HBSL & 0x20)) // Do nothing it is a RPn+2 port
        {
        }
        Else {
          ^RP23.VPON()
        }
      }
      Case (0x80DFFFFF) {
        ^RP24.VPON()
      }
      Case (0x80D0FFFF) {
        If((HBSL & 0x40)) // Check if hybrid device is present on RPn root port
        {
          If (D3CV(^RP25.RSPT, ^RP25.RSPI)) {
            \_SB.PSD0(^RP27.RSPI) // Restore power to the modPHY
            HBCM(^RP27.RSPI, 1) // UnMask Hybrid Partner CLKREQ
          }
        }

        ^RP25.VPON()

        If((HBSL & 0x40)) // Check if hybrid device is present on RPn root port
        {
          If (D3CV(^RP25.RSPT, ^RP25.RSPI)) {
            ^RP27.RLA() // Hybrid Partner Link Active
            Sleep(100) // Sleep for 100ms after transition to link active
          }
        }
      }
      Case (0x80D1FFFF) {
        ^RP26.VPON()
      }
      Case (0x80D2FFFF) {
        If((HBSL & 0x40)) // Do nothing it is a RPn+2 port
        {
        }
        Else {
          ^RP27.VPON()
        }
      }
      Case (0x80D3FFFF) {
        ^RP28.VPON()
      }
      // PEG Ports
      Case (0x8030FFFF) {
        If(CondRefOf(\HBCL)) {
          If(LAnd(LNotEqual(\HBCL, 0xFF), LEqual(\HBCL, 0))) {
            If (D3CV(^PEG0.RSPT, ^PEG0.RSPI)) {
              PRUN(\HBPL)
            }
          }
        }

        ^PEG0.VPON()

        If(CondRefOf(\HBCL)) {
          If(LAnd(LNotEqual(\HBCL, 0xFF), LEqual(\HBCL, 0))) {
            If (D3CV(^PEG0.RSPT, ^PEG0.RSPI)) {
              PRLA(\HBPL)
            }
          }
        }
      }
      Case (0x8008FFFF) {
        If(CondRefOf(\HBCL)) {
          If(LAnd(LNotEqual(\HBCL, 0xFF), LEqual(\HBCL, 1))) {
            If (D3CV(^PEG1.RSPT, ^PEG1.RSPI)) {
              PRUN(\HBPL)
            }
          }
        }

        ^PEG1.VPON()

        If(CondRefOf(\HBCL)) {
          If(LAnd(LNotEqual(\HBCL, 0xFF), LEqual(\HBCL, 1))) {
            If (D3CV(^PEG1.RSPT, ^PEG1.RSPI)) {
              PRLA(\HBPL)
            }
          }
        }
      }
      Case (0x8009FFFF) {
        If(CondRefOf(\HBCL)) {
          If(LAnd(LNotEqual(\HBCL, 0xFF), LEqual(\HBCL, 2))) {
            If (D3CV(^PEG2.RSPT, ^PEG2.RSPI)) {
              PRUN(\HBPL)
            }
          }
        }

        ^PEG2.VPON()

        If(CondRefOf(\HBCL)) {
          If(LAnd(LNotEqual(\HBCL, 0xFF), LEqual(\HBCL, 2))) {
            If (D3CV(^PEG2.RSPT, ^PEG2.RSPI)) {
              PRLA(\HBPL)
            }
          }
        }
      }
      Case (0x800AFFFF) {
        ^PEG3.VPON()
      }
      Case (0xB8FFF0) {
        ^PRT0.VSON()
      }
      Case (0xB8FFF1) {
        ^PRT1.VSON()
      }
      Case (0xB8FFF2) {
        ^PRT2.VSON()
      }
      Case (0xB8FFF3) {
        ^PRT3.VSON()
      }
      Case (0xB8FFF4) {
        ^PRT4.VSON()
      }
      Case (0xB8FFF5) {
        ^PRT5.VSON()
      }
      Case (0xB8FFF6) {
        ^PRT6.VSON()
      }
      Case (0xB8FFF7) {
        ^PRT7.VSON()
      }
    } //Switch(DRVA)
    Increment(Local0)
  } // while
}

// VLOF  : Method to turn OFF ports per port addres
// Arg0 : Package size
// Arg1 :  List of Port Address
Method(VLOF,2,Serialized,,,{IntObj, PkgObj}) {
  Name(DRVA, 0)  /// Storage for ACPI Address of the port
  Store (Arg0, Local1)
  Store (0, Local0) // use this as the index value
  While(LGreater (Local1, Local0))
  {
    Store(ToInteger(DerefOf(Index(Arg1, Local0))), DRVA)
    Switch(ToInteger(DRVA)) {
      Case (0x80E0FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x0))) {
          If((HBSL & 0x01)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP01.RSPT, ^RP01.RSPI)) {
              ^RP03.RL23() // Hybrid Partner Link L2 or L3 state
              \_SB.PSD3(^RP03.RSPI) // Enable modPHY power gating
              HBCM(^RP03.RSPI, 0) // Mask Hybrid Partner CLKREQ
            }
          }
          ^RP01.VPOF()
        }
      }
      Case (0x80E1FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x1))) {
          ^RP02.VPOF()
        }
      }
      Case (0x80E2FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x2))) {
          If((HBSL & 0x01)) // Do nothing it is a RPn+2 port
          {
          }
          Else {
            ^RP03.VPOF()
          }
        }
      }
      Case (0x80E3FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x3))) {
          ^RP04.VPOF()
        }
      }
      Case (0x80E4FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x4))) {
          If((HBSL & 0x02)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP05.RSPT, ^RP05.RSPI)) {
              ^RP07.RL23() // Hybrid Partner Link L2 or L3 state
              \_SB.PSD3(^RP07.RSPI) // Enable modPHY power gating
              HBCM(^RP07.RSPI, 0) // Mask Hybrid Partner CLKREQ
            }
          }
          ^RP05.VPOF()
        }
      }
      Case (0x80E5FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x5))) {
          ^RP06.VPOF()
        }
      }
      Case (0x80E6FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x6))) {
          If((HBSL & 0x02)) // Do nothing it is a RPn+2 port
          {
          }
          Else {
            ^RP07.VPOF()
          }
        }
      }
      Case (0x80E7FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x7))) {
          ^RP08.VPOF()
        }
      }
      Case (0x80E8FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x8))) {
          If((HBSL & 0x04)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP09.RSPT, ^RP09.RSPI)) {
              ^RP11.RL23() // Hybrid Partner Link L2 or L3 state
              \_SB.PSD3(^RP11.RSPI) // Enable modPHY power gating
              HBCM(^RP11.RSPI, 0) // Mask Hybrid Partner CLKREQ
            }
          }
          ^RP09.VPOF()
        }
      }
      Case (0x80E9FFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0x9))) {
          ^RP10.VPOF()
        }
      }
      Case (0x80EAFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xA))) {
          If((HBSL & 0x04)) // Do nothing it is a RPn+2 port
          {
          }
          Else {
            ^RP11.VPOF()
          }
        }
      }
      Case (0x80EBFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xB))) {
          ^RP12.VPOF()
        }
      }
      Case (0x80ECFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xC))) {
          If((HBSL & 0x08)) // Check if hybrid device is present on RPn root port
          {
            If (D3CV(^RP13.RSPT, ^RP13.RSPI)) {
              ^RP15.RL23() // Hybrid Partner Link L2 or L3 state
              \_SB.PSD3(^RP15.RSPI) // Enable modPHY power gating
              HBCM(^RP15.RSPI, 0) // Mask Hybrid Partner CLKREQ
            }
          }
          ^RP13.VPOF()
        }
      }
      Case (0x80EDFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xD))) {
          ^RP14.VPOF()
        }
      }
      Case (0x80EEFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xE))) {
          If((HBSL & 0x08)) // Do nothing it is a RPn+2 port
          {
          }
          Else {
            ^RP15.VPOF()
          }
        }
      }
      Case (0x80EFFFFF) {
        If(LOr(LOr(LEqual(\HBCL, 0xFF), LGreater(\HBCL, 0x2)), LNotEqual(\HBPL, 0xF))) {
          ^RP16.VPOF()
        }
      }
      Case (0x80D8FFFF) {
        If((HBSL & 0x10)) // Check if hybrid device is present on RPn root port
        {
          If (D3CV(^RP17.RSPT, ^RP17.RSPI)) {
            ^RP19.RL23() // Hybrid Partner Link L2 or L3 state
            \_SB.PSD3(^RP19.RSPI) // Enable modPHY power gating
            HBCM(^RP19.RSPI, 0) // Mask Hybrid Partner CLKREQ
          }
        }
        ^RP17.VPOF()
      }
      Case (0x80D9FFFF) {
        ^RP18.VPOF()
      }
      Case (0x80DAFFFF) {
        If((HBSL & 0x10)) // Do nothing it is a RPn+2 port
        {
        }
        Else {
          ^RP19.VPOF()
        }
      }
      Case (0x80DBFFFF) {
        ^RP20.VPOF()
      }
      Case (0x80DCFFFF) {
        If((HBSL & 0x20)) // Check if hybrid device is present on RPn root port
        {
          If (D3CV(^RP21.RSPT, ^RP21.RSPI)) {
            ^RP23.RL23() // Hybrid Partner Link L2 or L3 state
            \_SB.PSD3(^RP23.RSPI) // Enable modPHY power gating
            HBCM(^RP23.RSPI, 0) // Mask Hybrid Partner CLKREQ
          }
        }
        ^RP21.VPOF()
      }
      Case (0x80DDAFFFF) {
        ^RP22.VPOF()
      }
      Case (0x80DEFFFF) {
        If((HBSL & 0x20)) // Do nothing it is a RPn+2 port
        {
        }
        Else {
          ^RP23.VPOF()
        }
      }
      Case (0x80DFFFFF) {
        ^RP24.VPOF()
      }
      Case (0x80D0FFFF) {
        If((HBSL & 0x40)) // Check if hybrid device is present on RPn root port
        {
          If (D3CV(^RP25.RSPT, ^RP25.RSPI)) {
            ^RP27.RL23() // Hybrid Partner Link L2 or L3 state
            \_SB.PSD3(^RP27.RSPI) // Enable modPHY power gating
            HBCM(^RP27.RSPI, 0) // Mask Hybrid Partner CLKREQ
          }
        }
        ^RP25.VPOF()
      }
      Case (0x80D1FFFF) {
        ^RP26.VPOF()
      }
      Case (0x80D2FFFF) {
        If((HBSL & 0x40)) // Do nothing it is a RPn+2 port
        {
        }
        Else {
          ^RP27.VPOF()
        }
      }
      Case (0x80D3FFFF) {
        ^RP28.VPOF()
      }
      // PEG Port
      Case (0x8030FFFF) {
        If(CondRefOf(\HBCL)) {
          If(LAnd(LNotEqual(\HBCL, 0xFF), LEqual(\HBCL, 0))) {
            If (D3CV(^PEG0.RSPT, ^PEG0.RSPI)) {
              PRLD(\HBPL)
            }
          }
        }
        ^PEG0.VPOF()
      }
      Case (0x8008FFFF) {
        If(CondRefOf(\HBCL)) {
          If(LAnd(LNotEqual(\HBCL, 0xFF), LEqual(\HBCL, 1))) {
            If (D3CV(^PEG1.RSPT, ^PEG1.RSPI)) {
              PRLD(\HBPL)
            }
          }
        }
        ^PEG1.VPOF()
      }
      Case (0x8009FFFF) {
        If(CondRefOf(\HBCL)) {
          If(LAnd(LNotEqual(\HBCL, 0xFF), LEqual(\HBCL, 2))) {
            If (D3CV(^PEG2.RSPT, ^PEG2.RSPI)) {
              PRLD(\HBPL)
            }
          }
        }
        ^PEG2.VPOF()
      }
      Case (0x800AFFFF) {
        ^PEG3.VPOF()
      }
      Case (0xB8FFF0) {
        ^PRT0.VSOF()
      }
      Case (0xB8FFF1) {
        ^PRT1.VSOF()
      }
      Case (0xB8FFF2) {
        ^PRT2.VSOF()
      }
      Case (0xB8FFF3) {
        ^PRT3.VSOF()
      }
      Case (0xB8FFF4) {
        ^PRT4.VSOF()
      }
      Case (0xB8FFF5) {
        ^PRT5.VSOF()
      }
      Case (0xB8FFF6) {
        ^PRT6.VSOF()
      }
      Case (0xB8FFF7) {
        ^PRT7.VSOF()
      }
    } //Switch(DRVA)
    Increment(Local0)
  } // while
}

//
// Is VMD RAID member method
// Arg0 - ACPI address for PCIe device or ACPI address for SATA device
//
Method(IVRM, 1) {
  // Check in volume 0
  Store (0, Local0) // use this as the index value
  While (LGreater(VV0S, Local0)) {
    If(LEqual(Arg0,ToInteger(DerefOf(Index(VV0D, Local0))))) { Return (1) }
    Increment(Local0)
  }

  // Check in volume 1
  Store (0, Local0) // use this as the index value
  While (LGreater(VV1S, Local0)) {
    If(LEqual(Arg0,ToInteger(DerefOf(Index(VV1D, Local0))))) { Return (1) }
    Increment(Local0)
  }

  // Check in volume 2
  Store (0, Local0) // use this as the index value
  While (LGreater(VV2S, Local0)) {
    If(LEqual(Arg0,ToInteger(DerefOf(Index(VV2D, Local0))))) { Return (1) }
    Increment(Local0)
  }

  // Check in volume 3
  Store (0, Local0) // use this as the index value
  While (LGreater(VV3S, Local0)) {
    If(LEqual(Arg0,ToInteger(DerefOf(Index(VV3D, Local0))))) { Return (1) }
    Increment(Local0)
  }

  Return(0)
}

// Is D3Cold blocked on the port.
// Arg0 - PCIe / SATA port address
Method(VD3C, 1) {
  Return (LAnd(VD3A, IVRM(Arg0)))
}

Device(VOL0) {
  Name(_ADR,0x0080FFFF)  // VMD RAID Volume 0

  If(LAnd(CondRefOf(\RTD3), CondRefOf(\STD3))) {
    If(LAnd(LNotEqual(\RTD3, 0), LEqual(\STD3, 0x02))) { // Is Global RTD3 support and RTD3 support for storage enabled in BIOS Setup?
      Method(_PR0) {
        Return(Package(){V0PR})
      }

      Method(_PR3) {
        Return(Package(){V0PR})
      }
    }
  }

  If(LAnd(CondRefOf(\RTD3), CondRefOf(\STD3))) {
    If(LAnd(LNotEqual(\RTD3, 0), LEqual(\STD3, 0x02))) { // Is Global RTD3 support and RTD3 support for storage enabled in BIOS Setup?
      PowerResource(V0PR, 4, 0) { // Power Resource Aggregate for RAID volume 0
        Name(_STA, 0x01)

        Method(_ON, 0) {
          If(VD3A) {
            ^^^VLON(VV0S,VV0D)
            Store(0x1, _STA)
          } Else {
          }
        }

        Method(_OFF, 0) {
          If (VD3A) {
            ^^^VLOF(VV0S,VV0D)
            Store(0x0, _STA)
          } Else {
          }
        }
      }
    }
  }
}

Device(VOL1) {
  Name(_ADR,0x0081FFFF)  // VMD RAID Volume 1

  If(LAnd(CondRefOf(\RTD3), CondRefOf(\STD3))) {
    If(LAnd(LNotEqual(\RTD3, 0), LEqual(\STD3, 0x02))) { // Is Global RTD3 support and RTD3 support for storage enabled in BIOS Setup?
      Method(_PR0) {
        Return(Package(){V1PR})
      }

      Method(_PR3) {
        Return(Package(){V1PR})
      }
    }
  }

  If(LAnd(CondRefOf(\RTD3), CondRefOf(\STD3))) {
    If(LAnd(LNotEqual(\RTD3, 0), LEqual(\STD3, 0x02))) { // Is Global RTD3 support and RTD3 support for storage enabled in BIOS Setup?
      PowerResource(V1PR, 4, 0) { // Power Resource Aggregate for RAID volume 1
        Name(_STA, 0x01)

        Method(_ON, 0) {
          If(VD3A) {
            ^^^VLON(VV1S,VV1D)
            Store(0x1, _STA)
          } Else {
          }
        }

        Method(_OFF, 0) {
          If (VD3A) {
            ^^^VLOF(VV1S,VV1D)
            Store(0x0, _STA)
          } Else {
          }
        }
      }
    }
  }
}

Device(VOL2) {
  Name(_ADR,0x0082FFFF)  // VMD RAID Volume 2

  If(LAnd(CondRefOf(\RTD3), CondRefOf(\STD3))) {
    If(LAnd(LNotEqual(\RTD3, 0), LEqual(\STD3, 0x02))) { // Is Global RTD3 support and RTD3 support for storage enabled in BIOS Setup?
      Method(_PR0) {
        Return(Package(){V2PR})
      }

      Method(_PR3) {
        Return(Package(){V2PR})
      }
    }
  }

  If(LAnd(CondRefOf(\RTD3), CondRefOf(\STD3))) {
    If(LAnd(LNotEqual(\RTD3, 0), LEqual(\STD3, 0x02))) { // Is Global RTD3 support and RTD3 support for storage enabled in BIOS Setup?
      PowerResource(V2PR, 4, 0) { // Power Resource Aggregate for RAID volume 2
        Name(_STA, 0x01)

        Method(_ON, 0)  {
          If (VD3A) {
            ^^^VLON(VV2S,VV2D)
            Store(0x1, _STA)
          } Else {
          }
        }

        Method(_OFF, 0) {
          If (VD3A) {
            ^^^VLOF(VV2S,VV2D)
            Store(0x0, _STA)
          } Else {
          }
        }
      }
    }
  }
}

Device(VOL3) {
  Name(_ADR,0x0083FFFF)  // VMD RAID Volume 4

  If(LAnd(CondRefOf(\RTD3), CondRefOf(\STD3))) {
    If(LAnd(LNotEqual(\RTD3, 0), LEqual(\STD3, 0x02))) { // Is Global RTD3 support and RTD3 support for storage enabled in BIOS Setup?
      Method(_PR0) {
        Return(Package(){V3PR})
      }

      Method(_PR3) {
        Return(Package(){V3PR})
      }
    }
  }

  If(LAnd(CondRefOf(\RTD3), CondRefOf(\STD3))) {
    If(LAnd(LNotEqual(\RTD3, 0), LEqual(\STD3, 0x02))) { // Is Global RTD3 support and RTD3 support for storage enabled in BIOS Setup?
      PowerResource(V3PR, 4, 0) { // Power Resource Aggregate for RAID volume 0
        Name(_STA, 0x01)

        Method(_ON, 0) {
          If(VD3A) {
            ^^^VLON(VV3S,VV3D)
            Store(0x1, _STA)
          } Else {
          }
        }

        Method(_OFF, 0) {
          If (VD3A) {
            ^^^VLOF(VV3S,VV3D)
            Store(0x0, _STA)
          } Else {
          }
        }
      }
    }
  }
}
