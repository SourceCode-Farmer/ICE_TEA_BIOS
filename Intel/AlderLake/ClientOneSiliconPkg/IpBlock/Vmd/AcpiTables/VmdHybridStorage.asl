/**@file
  VMD remapped hybrid storage description for Hybrid Device remapped under VMD controller

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/


//
// Restore power to the Rpn+2 (hybrid partner) modPHY.
// UnMask Hybrid Partner CLKREQ
// Arg0 : PCH Rp number selected by user in setup menu
//
Method(PRUN, 1) {
  Switch(ToInteger(Arg0)) {
    Case(0) {
      \_SB.PSD0(^RP01.RSPI) // Restore power to the modPHY
      HBCM(^RP01.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(1){
      \_SB.PSD0(^RP02.RSPI) // Restore power to the modPHY
      HBCM(^RP02.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(2){
      \_SB.PSD0(^RP03.RSPI) // Restore power to the modPHY
      HBCM(^RP03.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(3){
      \_SB.PSD0(^RP04.RSPI) // Restore power to the modPHY
      HBCM(^RP04.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(4){
      \_SB.PSD0(^RP05.RSPI) // Restore power to the modPHY
      HBCM(^RP05.RSPI, 1) // UnMask Hybrid Partner CLKREQ}
    }
    Case(5){
      \_SB.PSD0(^RP06.RSPI) // Restore power to the modPHY
      HBCM(^RP06.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(6){
      \_SB.PSD0(^RP07.RSPI) // Restore power to the modPHY
      HBCM(^RP07.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(7){
      \_SB.PSD0(^RP08.RSPI) // Restore power to the modPHY
      HBCM(^RP08.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(8){
      \_SB.PSD0(^RP09.RSPI) // Restore power to the modPHY
      HBCM(^RP09.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(9){
      \_SB.PSD0(^RP10.RSPI) // Restore power to the modPHY
      HBCM(^RP10.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(10){
      \_SB.PSD0(^RP11.RSPI) // Restore power to the modPHY
      HBCM(^RP11.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(11){
      \_SB.PSD0(^RP12.RSPI) // Restore power to the modPHY
      HBCM(^RP12.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(12){
      \_SB.PSD0(^RP13.RSPI) // Restore power to the modPHY
      HBCM(^RP13.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(13){
      \_SB.PSD0(^RP14.RSPI) // Restore power to the modPHY
      HBCM(^RP14.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(14){
      \_SB.PSD0(^RP15.RSPI) // Restore power to the modPHY
      HBCM(^RP15.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(15){
      \_SB.PSD0(^RP16.RSPI) // Restore power to the modPHY
      HBCM(^RP16.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
    Case(16){
      \_SB.PSD0(^RP17.RSPI) // Restore power to the modPHY
      HBCM(^RP17.RSPI, 1) // UnMask Hybrid Partner CLKREQ
    }
  }
}

//
// RLA method recovers link from L2 or L3 state. Used for RTD3 flows, right after endpoint is powered up and exits reset.
// Arg0 : PCH Rp number selected by user in setup menu
//
Method(PRLA, 1) {
  Switch(ToInteger(Arg0)) {
    Case(0) { ^RP01.RLA() }
    Case(1) { ^RP02.RLA() }
    Case(2) { ^RP03.RLA() }
    Case(3) { ^RP04.RLA() }
    Case(4) { ^RP05.RLA() }
    Case(5) { ^RP06.RLA() }
    Case(6) { ^RP07.RLA() }
    Case(7) { ^RP08.RLA() }
    Case(8) { ^RP09.RLA() }
    Case(9) { ^RP10.RLA() }
    Case(10) { ^RP11.RLA() }
    Case(11) { ^RP12.RLA() }
    Case(12) { ^RP13.RLA() }
    Case(13) { ^RP14.RLA() }
    Case(14) { ^RP15.RLA() }
    Case(15) { ^RP16.RLA() }
    Case(16) { ^RP17.RLA() }
  }
  Sleep(100) // Sleep for 100ms after transition to link active
}

//
// Hybrid Partner Link L2 or L3 state
// Enable modPHY power gating
// Mask Hybrid Partner CLKREQ
// Arg0 : PCH Rp number selected by user in setup menu
//
Method(PRLD, 1) {
  Switch(ToInteger(Arg0)) {
    Case(0) {
      ^RP01.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP01.RSPI) // Enable modPHY power gating
      HBCM(^RP01.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(1){
      ^RP02.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP02.RSPI) // Enable modPHY power gating
      HBCM(^RP02.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(2){
      ^RP03.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP03.RSPI) // Enable modPHY power gating
      HBCM(^RP03.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(3){
      ^RP04.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP04.RSPI) // Enable modPHY power gating
      HBCM(^RP04.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(4){
      ^RP05.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP05.RSPI) // Enable modPHY power gating
      HBCM(^RP05.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(5){
      ^RP06.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP06.RSPI) // Enable modPHY power gating
      HBCM(^RP06.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(6){
      ^RP07.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP07.RSPI) // Enable modPHY power gating
      HBCM(^RP07.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(7){
      ^RP08.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP08.RSPI) // Enable modPHY power gating
      HBCM(^RP08.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(8){
      ^RP09.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP09.RSPI) // Enable modPHY power gating
      HBCM(^RP09.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(9){
      ^RP10.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP10.RSPI) // Enable modPHY power gating
      HBCM(^RP10.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(10){
      ^RP11.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP11.RSPI) // Enable modPHY power gating
      HBCM(^RP11.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(11){
      ^RP12.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP12.RSPI) // Enable modPHY power gating
      HBCM(^RP12.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(12){
      ^RP13.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP13.RSPI) // Enable modPHY power gating
      HBCM(^RP13.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(13){
      ^RP14.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP14.RSPI) // Enable modPHY power gating
      HBCM(^RP14.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(14){
      ^RP15.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP15.RSPI) // Enable modPHY power gating
      HBCM(^RP15.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(15){
      ^RP16.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP16.RSPI) // Enable modPHY power gating
      HBCM(^RP16.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
    Case(16){
      ^RP17.RL23() // Hybrid Partner Link L2 or L3 state
      \_SB.PSD3(^RP17.RSPI) // Enable modPHY power gating
      HBCM(^RP17.RSPI, 0) // Mask Hybrid Partner CLKREQ
    }
  }
}
