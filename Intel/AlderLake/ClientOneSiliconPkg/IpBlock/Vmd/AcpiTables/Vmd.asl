/**@file
  Intel ACPI Reference Code for VMD device support

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <PcieRegs.h>
#include <Register/PchPcieRpRegs.h>
#include <CpuPcieInfo.h>
#include <PchBdfAssignment.h>
#include <Register/SaRegsHostBridge.h>

#define VMD_STORAGE_REMAP_PORT_SATA         0
#define VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP  1
#define VMD_STORAGE_REMAP_PORT_CPU_PCIE_RP  2
#define PPAR_STATUS                         DPM0
#define PPAR_EXTENDED_STATUS                DPM1
#define RESERVED1                           DPM2
#define PRIVATE_PMEM_STARTING_ADDRESS       DPM3
#define PRIVATE_PMEM_RANGE_LENGTH           DPM4

#define DSM_STATUS_SUCCESS                                  0x0000
#define DSM_STATUS_FUNCTION_NOT_SUPPORTED                   0x0001
#define DSM_STATUS_NONEXISTING_MEMORY_DEVICE                0x0002
#define DSM_STATUS_INVALID_INPUT_PARAMS                     0x0003
#define DSM_STATUS_HW_ERROR                                 0x0004
#define DSM_STATUS_TIMEOUT_MAILBOX_NOT_READY                0x0005
#define DSM_STATUS_UNKNOWN_REASON                           0x0006
#define DSM_STATUS_SPECIFIC_ERROR                           0x0007
#define DSM_STATUS_OUT_OF_RESOURCES                         0x0008
#define DSM_STATUS_HW_NOT_READY                             0x0009
#define DSM_STATUS_INVALID_SECURITY_STATE                   0x000A
#define DSM_STATUS_INVALIS_CURRENT_PASSPHRASE_SUPLIED       0x000B

//
// SSDT RTD3 imports
//
External(\_SB.PC00.SAT0.PRT0.SPON, MethodObj)
External(\_SB.PC00.SAT0.PRT1.SPON, MethodObj)
External(\_SB.PC00.SAT0.PRT2.SPON, MethodObj)
External(\_SB.PC00.SAT0.PRT3.SPON, MethodObj)
External(\_SB.PC00.SAT0.PRT4.SPON, MethodObj)
External(\_SB.PC00.SAT0.PRT5.SPON, MethodObj)
External(\_SB.PC00.SAT0.PRT6.SPON, MethodObj)
External(\_SB.PC00.SAT0.PRT7.SPON, MethodObj)
External(\_SB.PC00.SAT0.PRT0.SPOF, MethodObj)
External(\_SB.PC00.SAT0.PRT1.SPOF, MethodObj)
External(\_SB.PC00.SAT0.PRT2.SPOF, MethodObj)
External(\_SB.PC00.SAT0.PRT3.SPOF, MethodObj)
External(\_SB.PC00.SAT0.PRT4.SPOF, MethodObj)
External(\_SB.PC00.SAT0.PRT5.SPOF, MethodObj)
External(\_SB.PC00.SAT0.PRT6.SPOF, MethodObj)
External(\_SB.PC00.SAT0.PRT7.SPOF, MethodObj)
External(\_SB.PC00.RP01.PON, MethodObj)
External(\_SB.PC00.RP01.POFF, MethodObj)
External(\_SB.PC00.RP02.PON, MethodObj)
External(\_SB.PC00.RP02.POFF, MethodObj)
External(\_SB.PC00.RP03.PON, MethodObj)
External(\_SB.PC00.RP03.POFF, MethodObj)
External(\_SB.PC00.RP04.PON, MethodObj)
External(\_SB.PC00.RP04.POFF, MethodObj)
External(\_SB.PC00.RP05.PON, MethodObj)
External(\_SB.PC00.RP05.POFF, MethodObj)
External(\_SB.PC00.RP06.PON, MethodObj)
External(\_SB.PC00.RP06.POFF, MethodObj)
External(\_SB.PC00.RP07.PON, MethodObj)
External(\_SB.PC00.RP07.POFF, MethodObj)
External(\_SB.PC00.RP08.PON, MethodObj)
External(\_SB.PC00.RP08.POFF, MethodObj)
External(\_SB.PC00.RP09.PON, MethodObj)
External(\_SB.PC00.RP09.POFF, MethodObj)
External(\_SB.PC00.RP10.PON, MethodObj)
External(\_SB.PC00.RP10.POFF, MethodObj)
External(\_SB.PC00.RP11.PON, MethodObj)
External(\_SB.PC00.RP11.POFF, MethodObj)
External(\_SB.PC00.RP12.PON, MethodObj)
External(\_SB.PC00.RP12.POFF, MethodObj)
External(\_SB.PC00.RP13.PON, MethodObj)
External(\_SB.PC00.RP13.POFF, MethodObj)
External(\_SB.PC00.RP14.PON, MethodObj)
External(\_SB.PC00.RP14.POFF, MethodObj)
External(\_SB.PC00.RP15.PON, MethodObj)
External(\_SB.PC00.RP15.POFF, MethodObj)
External(\_SB.PC00.RP16.PON, MethodObj)
External(\_SB.PC00.RP16.POFF, MethodObj)
External(\_SB.PC00.RP17.PON, MethodObj)
External(\_SB.PC00.RP17.POFF, MethodObj)
External(\_SB.PC00.RP18.PON, MethodObj)
External(\_SB.PC00.RP18.POFF, MethodObj)
External(\_SB.PC00.RP19.PON, MethodObj)
External(\_SB.PC00.RP19.POFF, MethodObj)
External(\_SB.PC00.RP20.PON, MethodObj)
External(\_SB.PC00.RP20.POFF, MethodObj)
External(\_SB.PC00.RP21.PON, MethodObj)
External(\_SB.PC00.RP21.POFF, MethodObj)
External(\_SB.PC00.RP22.PON, MethodObj)
External(\_SB.PC00.RP22.POFF, MethodObj)
External(\_SB.PC00.RP23.PON, MethodObj)
External(\_SB.PC00.RP23.POFF, MethodObj)
External(\_SB.PC00.RP24.PON, MethodObj)
External(\_SB.PC00.RP24.POFF, MethodObj)
External(\_SB.PC00.RP25.PON, MethodObj)
External(\_SB.PC00.RP25.POFF, MethodObj)
External(\_SB.PC00.RP26.PON, MethodObj)
External(\_SB.PC00.RP26.POFF, MethodObj)
External(\_SB.PC00.RP27.PON, MethodObj)
External(\_SB.PC00.RP27.POFF, MethodObj)
External(\_SB.PC00.RP28.PON, MethodObj)
External(\_SB.PC00.RP28.POFF, MethodObj)
External(\_SB.PC00.PEG0.PON, MethodObj)
External(\_SB.PC00.PEG0.POFF, MethodObj)
External(\_SB.PC00.PEG1.PON, MethodObj)
External(\_SB.PC00.PEG1.POFF, MethodObj)
External(\_SB.PC00.PEG2.PON, MethodObj)
External(\_SB.PC00.PEG2.POFF, MethodObj)
External(\_SB.PC00.PEG3.PON, MethodObj)
External(\_SB.PC00.PEG3.POFF, MethodObj)
External(\_SB.NVDR._DSM, MethodObj)
External(\_SB.NVDR.NVD1._LSI, MethodObj)
External(\_SB.NVDR.NVD1._LSR, MethodObj)
External(\_SB.NVDR.NVD1._LSW, MethodObj)
External(\_SB.NVDR.NVD1._DSM, MethodObj)
External(\DPMS, FieldUnitObj) // DpmemEnable
External(\PMSA, FieldUnitObj) // PmemStartingAddress
External(\PMRL, FieldUnitObj) // PmemRangeLength
External(\VDSD, IntObj) // VMD _DSD Method - 1: Expose 0: Hide
External(VMCP)

External(\XBAS)

Scope (\_SB.PC00) {
  Device (VMD0) {
    //
    // _ADR format for devices mapped under VMD is defined by RST OS driver team and provided this format
    //
    Name(_ADR, 0x000E0000)

    If (CondRefOf(\VDSD)) {
      If (LEqual (\VDSD, 1)) {
        //
        // Windows defined _DSD that informs the OS
        // that it should support D3 on this storage device.
        //
        Name (_DSD, Package () {
          ToUUID("5025030F-842F-4AB4-A561-99A5189762D0"),
          Package () {
            Package (2) {"StorageD3Enable", 1}
          }
        })
      }
    }

    Method(_S3D, 0, NotSerialized)
    {
      Return(3)
    }

    Method(_S4D, 0, NotSerialized)
    {
      Return(3)
    }

    Method(_PS0, 0, Serialized) {

      If (CondRefOf(VMS0)) {
        If (LNotEqual (VMS0, 0)) { // If SATA Controller is mapped under VMD
          //
          // Clear LTR ignore bit for Sata on D0
          //
          Store(0, ISAT)
        }
      }
    }

    Method(_PS3, 0, Serialized) {

      If (CondRefOf(VMS0)) {
        If (LNotEqual (VMS0, 0)) { // If SATA Controller is mapped under VMD
          //
          // Set LTR ignore bit for Sata on D3
          //
          Store(1, ISAT)
        }
      }
    }

    //
    // Returns PCI config base address
    // in VMD CFGBAR address space.
    // Arg0 - Remapped device bus number
    // Arg1 - Remapped device device number
    // Arg2 - Remapped device function number
    // Return - address of the remapped device PCI config space address
    //
    Method(CBAR, 3, Serialized) {

      Name(VCFB, 0) // VMD PCI config base address
      Store(\XBAS, VCFB)
      Or(VCFB, ShiftLeft(0, 20), VCFB)
      Or(VCFB, ShiftLeft(0xE, 15), VCFB)
      Or(VCFB, ShiftLeft(0, 12), VCFB)

      //
      // This is just a regular PCI access to VMD controller.
      // We have to use SystemMemory region since PCI region access
      // can be blocked by OSPM if PCI_Config is used
      //
      OperationRegion (VCFG, SystemMemory, VCFB, 0x100)
      Field (VCFG, DWordAcc, NoLock, Preserve) {
        Offset(R_PCI_BAR0_OFFSET),
        BAR0, 64,
      }

      Name(DCFB, 0) // VMD remapped device config base address
      Store(BAR0, DCFB)
      And (DCFB, Not(0xF), DCFB)
      Or(DCFB, ShiftLeft(Arg0, 20), DCFB)
      Or(DCFB, ShiftLeft(Arg1, 15), DCFB)
      Or(DCFB, ShiftLeft(Arg2, 12), DCFB)

      Return (DCFB)
    }

    Device(NVDR){
      Name(_ADR, 0x1)

      Method (_STA){
        If(LEqual(\DPMS, 1))
        {
          Return (0x0F)
        }
        Else
        {
          Return (0x00)
        }
      }

      // Passthrough NVDIMM _DSMs
      Method (_DSM, 4, Serialized, 0, UnknownObj, {BuffObj, IntObj, IntObj, PkgObj})
      {
        If(LEqual(Arg0, ToUUID ("A3316317-E8B6-4adf-96E8-94D94A51A38E")))
        {
          //
          // Dynamic PMEM DSMs
          //
          // Arg0 - UUID - A3316317-E8B6-4adf-96E8-94D94A51A38E
          // Arg1 - Revision Id = 1
          // Arg2: Integer Function Index
          //  0 Query implemented functions per ACPI Specification.
          //  1 Get Private PMem Address Range
          switch(ToInteger(Arg2))
          {
            case(0)
            {
              switch(ToInteger(Arg1))
              {
                case(1) {Return (0x03)}  // 0 to 1 functions are supported
                default {Return (0x00)}
              }
            }
            case(1)
            {
              Name(PPAR, Package() {Buffer (24) {}})
              CreateWordField (DerefOf(Index(PPAR, 0)), 0, PPAR_STATUS)
              CreateWordField (DerefOf(Index(PPAR, 0)), 2, PPAR_EXTENDED_STATUS)
              CreateDWordField (DerefOf(Index(PPAR, 0)), 4, RESERVED1)
              CreateQWordField (DerefOf(Index(PPAR, 0)), 8, PRIVATE_PMEM_STARTING_ADDRESS)
              CreateQWordField (DerefOf(Index(PPAR, 0)), 16, PRIVATE_PMEM_RANGE_LENGTH)
              If (LEqual(\DPMS, 0)) {
                Store (DSM_STATUS_NONEXISTING_MEMORY_DEVICE, PPAR_STATUS)
                Store (0, PPAR_EXTENDED_STATUS)
                Store (0, PRIVATE_PMEM_STARTING_ADDRESS)
                Store (0, PRIVATE_PMEM_RANGE_LENGTH)
                Return (PPAR)
              } Else {
                Store (DSM_STATUS_SUCCESS, PPAR_STATUS)
                Store (0, PPAR_EXTENDED_STATUS)
                Store (\PMSA, PRIVATE_PMEM_STARTING_ADDRESS)
                Store (\PMRL, PRIVATE_PMEM_RANGE_LENGTH)
                Return (PPAR)
              }
            }
          }
          Return(0)
        }
        Else
        {
          // Root device _DSMs
          Return(\_SB.NVDR._DSM(Arg0, Arg1, Arg2, Arg3))
        }
      }

      Device(NVD1) {
        // _ADR
        Name(_ADR, 0x2)

        Method (_STA) {
          If(LEqual(\DPMS, 1))
          {
            Return (0x0F)
          }
          Else
          {
            Return (0x00)
          }
        }

        Method (_LSI, 0)
        {
          Return(\_SB.NVDR.NVD1._LSI())
        }

        Method (_LSR, 2, Serialized)
        {
          Return(\_SB.NVDR.NVD1._LSR(Arg0, Arg1))
        }

        Method (_LSW, 3, Serialized)
        {
          Return(\_SB.NVDR.NVD1._LSW(Arg0, Arg1, Arg2))
        }
        // Device DSMs
        Method (_DSM, 4, Serialized, 0, UnknownObj, {BuffObj, IntObj, IntObj, PkgObj})
        {
          Return(\_SB.NVDR.NVD1._DSM(Arg0, Arg1, Arg2, Arg3))
        } //end of NVD1 DSM
      } // end of NVD1
    } // end of namespace

    //
    // PCH PCIe root ports remapped under VMD controller
    // RPD -  remapped root port device number
    // RPF -  remapped root port function number
    // RSPT - remapped slot port type
    // RSPI - remapped slot port index
    //
    Device(RP01) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_1)
      Name (RPF, 0)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 0)
      Name (_ADR, 0x80E0FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP02) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_2)
      Name (RPF, 1)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 1)
      Name (_ADR, 0x80E1FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP03) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_3)
      Name (RPF, 2)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 2)
      Name (_ADR, 0x80E2FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP04) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_4)
      Name (RPF, 3)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 3)
      Name (_ADR, 0x80E3FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP05) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_5)
      Name (RPF, 4)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 4)
      Name (_ADR, 0x80E4FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP06) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_6)
      Name (RPF, 5)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 5)
      Name (_ADR, 0x80E5FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP07) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_7)
      Name (RPF, 6)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 6)
      Name (_ADR, 0x80E6FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP08) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_8)
      Name (RPF, 7)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 7)
      Name (_ADR, 0x80E7FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP09) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_9)
      Name (RPF, 0)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 8)
      Name (_ADR, 0x80E8FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP10) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_10)
      Name (RPF, 1)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 9)
      Name (_ADR, 0x80E9FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP11) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_11)
      Name (RPF, 2)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 10)
      Name (_ADR, 0x80EAFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP12) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_12)
      Name (RPF, 3)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 11)
      Name (_ADR, 0x80EBFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP13) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_13)
      Name (RPF, 4)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 12)
      Name (_ADR, 0x80ECFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP14) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_14)
      Name (RPF, 5)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 13)
      Name (_ADR, 0x80EDFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP15) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_15)
      Name (RPF, 6)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 14)
      Name (_ADR, 0x80EEFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP16) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_16)
      Name (RPF, 7)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 15)
      Name (_ADR, 0x80EFFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP17) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_17)
      Name (RPF, 0)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 16)
      Name (_ADR, 0x80D8FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP18) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_18)
      Name (RPF, 1)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 17)
      Name (_ADR, 0x80D9FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP19) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_19)
      Name (RPF, 2)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 18)
      Name (_ADR, 0x80DAFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP20) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_20)
      Name (RPF, 3)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 19)
      Name (_ADR, 0x80DBFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP21) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_21)
      Name (RPF, 4)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 20)
      Name (_ADR, 0x80DCFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP22) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_22)
      Name (RPF, 5)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 21)
      Name (_ADR, 0x80DDFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP23) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_23)
      Name (RPF, 6)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 22)
      Name (_ADR, 0x80DEFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP24) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_24)
      Name (RPF, 7)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 23)
      Name (_ADR, 0x80DFFFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP25) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_25)
      Name (RPF, 0)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 24)
      Name (_ADR, 0x80D0FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP26) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_26)
      Name (RPF, 1)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 25)
      Name (_ADR, 0x80D1FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP27) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_27)
      Name (RPF, 2)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 26)
      Name (_ADR, 0x80D2FFFF)
      Include ("VmdPcieRp.asl")
    }

    Device(RP28) {
      Name (RPD, PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_28)
      Name (RPF, 3)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP)
      Name (RSPI, 27)
      Name (_ADR, 0x80D3FFFF)
      Include ("VmdPcieRp.asl")
    }

    //
    // CPU PCIe root ports remapped under VMD controller
    //

    // P.E.G. Root Port D6F0
    Device(PEG0) {
      Name (RPD, SA_PEG3_DEV_NUM)
      Name (RPF, SA_PEG3_FUN_NUM)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_CPU_PCIE_RP)
      Name (RSPI, 0)
      Name (_ADR, 0x8030FFFF)
      Include ("VmdPcieRp.asl")
    }

    // P.E.G. Root Port D1F0
    Device(PEG1) {
      Name (RPD, SA_PEG0_DEV_NUM)
      Name (RPF, SA_PEG0_FUN_NUM)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_CPU_PCIE_RP)
      Name (RSPI, 1)
      Name (_ADR, 0x8008FFFF)
      Include ("VmdPcieRp.asl")
    }

    // P.E.G. Root Port D1F1
    Device(PEG2) {
      If (CondRefOf(VMCP)) {
          Name (RPD, 0)
          Name (RPF, 0)
          Name (RSPT, VMD_STORAGE_REMAP_PORT_CPU_PCIE_RP)
          Name (RSPI, 2)
          Name (_ADR, 0x0)
        If (VMCP & 0x2) {
          Store (SA_PEG1_DEV_NUM, RPD)
          Store (SA_PEG1_FUN_NUM, RPF)
          Store (0x8009FFFF, _ADR)
        } ElseIf (VMCP & 0x10) {
          Store (SA_PEG3_DEV_NUM, RPD)
          Store (SA_PEG2_FUN_NUM, RPF)
          Store (0x8032FFFF, _ADR)
        }
      }
      Include ("VmdPcieRp.asl")
    }

    // P.E.G. Root Port D1F2
    Device(PEG3) {
      Name (RPD, SA_PEG2_DEV_NUM)
      Name (RPF, SA_PEG2_FUN_NUM)
      Name (RSPT, VMD_STORAGE_REMAP_PORT_CPU_PCIE_RP)
      Name (RSPI, 3)
      Name (_ADR, 0x800AFFFF)
      Include ("VmdPcieRp.asl")
    }

    //
    // Integrated SATA ports remapped under VMD controller
    // RSPT - remapped slot port type
    // RSPI - remapped slot port index
    //
    Device(PRT0) {
      Name (RSPT, VMD_STORAGE_REMAP_PORT_SATA)
      Name (RSPI, 0)
      Name(_ADR, 0xB8FFF0)
      Include("VmdSata.asl")
    }

    Device(PRT1) {
      Name (RSPT, VMD_STORAGE_REMAP_PORT_SATA)
      Name (RSPI, 1)
      Name(_ADR, 0xB8FFF1)
      Include("VmdSata.asl")
    }

    Device(PRT2) {
      Name (RSPT, VMD_STORAGE_REMAP_PORT_SATA)
      Name (RSPI, 2)
      Name(_ADR, 0xB8FFF2)
      Include("VmdSata.asl")
    }

    Device(PRT3) {
      Name (RSPT, VMD_STORAGE_REMAP_PORT_SATA)
      Name (RSPI, 3)
      Name(_ADR, 0xB8FFF3)
      Include("VmdSata.asl")
    }

    Device(PRT4) {
      Name (RSPT, VMD_STORAGE_REMAP_PORT_SATA)
      Name (RSPI, 4)
      Name(_ADR, 0xB8FFF4)
      Include("VmdSata.asl")
    }

    Device(PRT5) {
      Name (RSPT, VMD_STORAGE_REMAP_PORT_SATA)
      Name (RSPI, 5)
      Name(_ADR, 0xB8FFF5)
      Include("VmdSata.asl")
    }

    Device(PRT6) {
      Name (RSPT, VMD_STORAGE_REMAP_PORT_SATA)
      Name (RSPI, 6)
      Name(_ADR, 0xB8FFF6)
      Include("VmdSata.asl")
    }

    Device(PRT7) {
      Name (RSPT, VMD_STORAGE_REMAP_PORT_SATA)
      Name (RSPI, 7)
      Name(_ADR, 0xB8FFF7)
      Include("VmdSata.asl")
    }

    //
    // RTD3 support for logical volumes
    //
    Include("VmdRaid.asl")

    //
    // Is D3Cold supported for given remapped storage port
    // Support for D3Cold is indicated by presence of POFF method.
    // Arg0  Storage remap port type
    // Arg1  Index of the storage remap port
    //
    Method(D3CV, 2, Serialized) {
      Switch(ToInteger(Arg0)) {
        //
        // For SATA the support for D3Cold transition
        // is indicated by the presence of the SPOF method.
        // We support up to 8 SATA ports as this is the current
        // limit in the integrated SATA controller.
        //
        Case(VMD_STORAGE_REMAP_PORT_SATA) {
          Switch (ToInteger(Arg1)) {
            Case(0) {
              If (CondRefOf(\_SB.PC00.SAT0.PRT0.SPOF)) {Return(1)}
            }
            Case(1) {
              If (CondRefOf(\_SB.PC00.SAT0.PRT1.SPOF)) {Return(1)}
            }
            Case(2) {
              If (CondRefOf(\_SB.PC00.SAT0.PRT2.SPOF)) {Return(1)}
            }
            Case(3){
              If (CondRefOf(\_SB.PC00.SAT0.PRT3.SPOF)) {Return(1)}
            }
            Case(4){
              If (CondRefOf(\_SB.PC00.SAT0.PRT4.SPOF)) {Return(1)}
            }
            Case(5){
              If (CondRefOf(\_SB.PC00.SAT0.PRT5.SPOF)) {Return(1)}
            }
            Case(6){
              If (CondRefOf(\_SB.PC00.SAT0.PRT6.SPOF)) {Return(1)}
            }
            Case(7){
              If (CondRefOf(\_SB.PC00.SAT0.PRT7.SPOF)) {Return(1)}
            }
          }
        }
        //
        // For PCIe root ports the support for D3Cold transition
        // is indicated by the presence of the POFF method.
        // We support up to 24 root ports which is the current
        // limit on the desktop SKUs of the PCH.
        //
        Case(VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP) {
          Switch(ToInteger(Arg1)) {
            Case(0){
              If (CondRefOf(\_SB.PC00.RP01.POFF)) {Return(1)}
            }
            Case(1){
              If (CondRefOf(\_SB.PC00.RP02.POFF)) {Return(1)}
            }
            Case(2){
              If (CondRefOf(\_SB.PC00.RP03.POFF)) {Return(1)}
            }
            Case(3){
              If (CondRefOf(\_SB.PC00.RP04.POFF)) {Return(1)}
            }
            Case(4){
              If (CondRefOf(\_SB.PC00.RP05.POFF)) {Return(1)}
            }
            Case(5){
              If (CondRefOf(\_SB.PC00.RP06.POFF)) {Return(1)}
            }
            Case(6){
              If (CondRefOf(\_SB.PC00.RP07.POFF)) {Return(1)}
            }
            Case(7){
              If (CondRefOf(\_SB.PC00.RP08.POFF)) {Return(1)}
            }
            Case(8){
              If (CondRefOf(\_SB.PC00.RP09.POFF)) {Return(1)}
            }
            Case(9){
              If (CondRefOf(\_SB.PC00.RP10.POFF)) {Return(1)}
            }
            Case(10){
              If (CondRefOf(\_SB.PC00.RP11.POFF)) {Return(1)}
            }
            Case(11){
              If (CondRefOf(\_SB.PC00.RP12.POFF)) {Return(1)}
            }
            Case(12){
              If (CondRefOf(\_SB.PC00.RP13.POFF)) {Return(1)}
            }
            Case(13){
              If (CondRefOf(\_SB.PC00.RP14.POFF)) {Return(1)}
            }
            Case(14){
              If (CondRefOf(\_SB.PC00.RP15.POFF)) {Return(1)}
            }
            Case(15){
              If (CondRefOf(\_SB.PC00.RP16.POFF)) {Return(1)}
            }
            Case(16){
              If (CondRefOf(\_SB.PC00.RP17.POFF)) {Return(1)}
            }
            Case(17){
              If (CondRefOf(\_SB.PC00.RP18.POFF)) {Return(1)}
            }
            Case(18){
              If (CondRefOf(\_SB.PC00.RP19.POFF)) {Return(1)}
            }
            Case(19){
              If (CondRefOf(\_SB.PC00.RP20.POFF)) {Return(1)}
            }
            Case(20){
              If (CondRefOf(\_SB.PC00.RP21.POFF)) {Return(1)}
            }
            Case(21){
              If (CondRefOf(\_SB.PC00.RP22.POFF)) {Return(1)}
            }
            Case(22){
              If (CondRefOf(\_SB.PC00.RP23.POFF)) {Return(1)}
            }
            Case(23){
              If (CondRefOf(\_SB.PC00.RP24.POFF)) {Return(1)}
            }
            Case(24){
              If (CondRefOf(\_SB.PC00.RP25.POFF)) {Return(1)}
            }
            Case(25){
              If (CondRefOf(\_SB.PC00.RP26.POFF)) {Return(1)}
            }
            Case(26){
              If (CondRefOf(\_SB.PC00.RP27.POFF)) {Return(1)}
            }
            Case(27){
              If (CondRefOf(\_SB.PC00.RP28.POFF)) {Return(1)}
            }
          }
        }
        //
        // For CPU PCIe root ports the support for D3Cold transition
        // is indicated by the presence of the POFF method.
        // We support up to 4 root port which is the current
        // limit defined in CPUs.
        //
        Case(VMD_STORAGE_REMAP_PORT_CPU_PCIE_RP) {
          Switch(ToInteger(Arg1)) {
            Case(0){
              If (CondRefOf(\_SB.PC00.PEG0.POFF)) {Return(1)}
            }
            Case(1){
              If (CondRefOf(\_SB.PC00.PEG1.POFF)) {Return(1)}
            }
            Case(2){
              If (CondRefOf(\_SB.PC00.PEG2.POFF)) {Return(1)}
            }
            Case(3){
              If (CondRefOf(\_SB.PC00.PEG3.POFF)) {Return(1)}
            }
          }
        }
      }
      Return(0)
    }

    //
    // Turn on power to the remapped storage
    // Arg0  Storage remap port type
    // Arg1  Index of the storage remap port
    //
    Method(VDON, 2, Serialized) {
      Switch(ToInteger(Arg0)) {
        Case(VMD_STORAGE_REMAP_PORT_SATA) {
          Switch (ToInteger(Arg1)) {
            Case(0) {
              If (CondRefOf(\_SB.PC00.SAT0.PRT0.SPON)) {\_SB.PC00.SAT0.PRT0.SPON()}
            }
            Case(1) {
              If (CondRefOf(\_SB.PC00.SAT0.PRT1.SPON)) {\_SB.PC00.SAT0.PRT1.SPON()}
            }
            Case(2) {
              If (CondRefOf(\_SB.PC00.SAT0.PRT2.SPON)) {\_SB.PC00.SAT0.PRT2.SPON()}
            }
            Case(3){
              If (CondRefOf(\_SB.PC00.SAT0.PRT3.SPON)) {\_SB.PC00.SAT0.PRT3.SPON()}
            }
            Case(4){
              If (CondRefOf(\_SB.PC00.SAT0.PRT4.SPON)) {\_SB.PC00.SAT0.PRT4.SPON()}
            }
            Case(5){
              If (CondRefOf(\_SB.PC00.SAT0.PRT5.SPON)) {\_SB.PC00.SAT0.PRT5.SPON()}
            }
            Case(6){
              If (CondRefOf(\_SB.PC00.SAT0.PRT6.SPON)) {\_SB.PC00.SAT0.PRT6.SPON()}
            }
            Case(7){
              If (CondRefOf(\_SB.PC00.SAT0.PRT7.SPON)) {\_SB.PC00.SAT0.PRT7.SPON()}
            }
          }
        }
        Case(VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP) {
          Switch(ToInteger(Arg1)) {
            Case(0){
              If (CondRefOf(\_SB.PC00.RP01.PON)) {\_SB.PC00.RP01.PON()}
            }
            Case(1){
              If (CondRefOf(\_SB.PC00.RP02.PON)) {\_SB.PC00.RP02.PON()}
            }
            Case(2){
              If (CondRefOf(\_SB.PC00.RP03.PON)) {\_SB.PC00.RP03.PON()}
            }
            Case(3){
              If (CondRefOf(\_SB.PC00.RP04.PON)) {\_SB.PC00.RP04.PON()}
            }
            Case(4){
              If (CondRefOf(\_SB.PC00.RP05.PON)) {\_SB.PC00.RP05.PON()}
            }
            Case(5){
              If (CondRefOf(\_SB.PC00.RP06.PON)) {\_SB.PC00.RP06.PON()}
            }
            Case(6){
              If (CondRefOf(\_SB.PC00.RP07.PON)) {\_SB.PC00.RP07.PON()}
            }
            Case(7){
              If (CondRefOf(\_SB.PC00.RP08.PON)) {\_SB.PC00.RP08.PON()}
            }
            Case(8){
              If (CondRefOf(\_SB.PC00.RP09.PON)) {\_SB.PC00.RP09.PON()}
            }
            Case(9){
              If (CondRefOf(\_SB.PC00.RP10.PON)) {\_SB.PC00.RP10.PON()}
            }
            Case(10){
              If (CondRefOf(\_SB.PC00.RP11.PON)) {\_SB.PC00.RP11.PON()}
            }
            Case(11){
              If (CondRefOf(\_SB.PC00.RP12.PON)) {\_SB.PC00.RP12.PON()}
            }
            Case(12){
              If (CondRefOf(\_SB.PC00.RP13.PON)) {\_SB.PC00.RP13.PON()}
            }
            Case(13){
              If (CondRefOf(\_SB.PC00.RP14.PON)) {\_SB.PC00.RP14.PON()}
            }
            Case(14){
              If (CondRefOf(\_SB.PC00.RP15.PON)) {\_SB.PC00.RP15.PON()}
            }
            Case(15){
              If (CondRefOf(\_SB.PC00.RP16.PON)) {\_SB.PC00.RP16.PON()}
            }
            Case(16){
              If (CondRefOf(\_SB.PC00.RP17.PON)) {\_SB.PC00.RP17.PON()}
            }
            Case(17){
              If (CondRefOf(\_SB.PC00.RP18.PON)) {\_SB.PC00.RP18.PON()}
            }
            Case(18){
              If (CondRefOf(\_SB.PC00.RP19.PON)) {\_SB.PC00.RP19.PON()}
            }
            Case(19){
              If (CondRefOf(\_SB.PC00.RP20.PON)) {\_SB.PC00.RP20.PON()}
            }
            Case(20){
              If (CondRefOf(\_SB.PC00.RP21.PON)) {\_SB.PC00.RP21.PON()}
            }
            Case(21){
              If (CondRefOf(\_SB.PC00.RP22.PON)) {\_SB.PC00.RP22.PON()}
            }
            Case(22){
              If (CondRefOf(\_SB.PC00.RP23.PON)) {\_SB.PC00.RP23.PON()}
            }
            Case(23){
              If (CondRefOf(\_SB.PC00.RP24.PON)) {\_SB.PC00.RP24.PON()}
            }
            Case(24){
              If (CondRefOf(\_SB.PC00.RP25.PON)) {\_SB.PC00.RP25.PON()}
            }
            Case(25){
              If (CondRefOf(\_SB.PC00.RP26.PON)) {\_SB.PC00.RP26.PON()}
            }
            Case(26){
              If (CondRefOf(\_SB.PC00.RP27.PON)) {\_SB.PC00.RP27.PON()}
            }
            Case(27){
              If (CondRefOf(\_SB.PC00.RP28.PON)) {\_SB.PC00.RP28.PON()}
            }
          }
        }
        Case(VMD_STORAGE_REMAP_PORT_CPU_PCIE_RP) {
          Switch(ToInteger(Arg1)) {
            Case(0){
              If (CondRefOf(\_SB.PC00.PEG0.PON)) {\_SB.PC00.PEG0.PON()}
            }
            Case(1){
              If (CondRefOf(\_SB.PC00.PEG1.PON)) {\_SB.PC00.PEG1.PON()}
            }
            Case(2){
              If (CondRefOf(\_SB.PC00.PEG2.PON)) {\_SB.PC00.PEG2.PON()}
            }
            Case(3){
              If (CondRefOf(\_SB.PC00.PEG3.PON)) {\_SB.PC00.PEG3.PON()}
            }
          }
        }
      }
    }

    //
    // Turn off power to the remapped port PCIe slot
    // Arg0  Storage remap port type
    // Arg1  Index of the storage remap port
    //
    Method(VDOF, 2, Serialized) {
      Switch(ToInteger(Arg0)) {
        Case(VMD_STORAGE_REMAP_PORT_SATA) {
          Switch(ToInteger(Arg1)) {
            Case(0) {
              If (CondRefOf(\_SB.PC00.SAT0.PRT0.SPOF)) {\_SB.PC00.SAT0.PRT0.SPOF()}
            }
            Case(1) {
              If (CondRefOf(\_SB.PC00.SAT0.PRT1.SPOF)) {\_SB.PC00.SAT0.PRT1.SPOF()}
            }
            Case(2) {
              If (CondRefOf(\_SB.PC00.SAT0.PRT2.SPOF)) {\_SB.PC00.SAT0.PRT2.SPOF()}
            }
            Case(3){
              If (CondRefOf(\_SB.PC00.SAT0.PRT3.SPOF)) {\_SB.PC00.SAT0.PRT3.SPOF()}
            }
            Case(4){
              If (CondRefOf(\_SB.PC00.SAT0.PRT4.SPOF)) {\_SB.PC00.SAT0.PRT4.SPOF()}
            }
            Case(5){
              If (CondRefOf(\_SB.PC00.SAT0.PRT5.SPOF)) {\_SB.PC00.SAT0.PRT5.SPOF()}
            }
            Case(6){
              If (CondRefOf(\_SB.PC00.SAT0.PRT6.SPOF)) {\_SB.PC00.SAT0.PRT6.SPOF()}
            }
            Case(7){
              If (CondRefOf(\_SB.PC00.SAT0.PRT7.SPOF)) {\_SB.PC00.SAT0.PRT7.SPOF()}
            }
          }
        }
        Case(VMD_STORAGE_REMAP_PORT_PCH_PCIE_RP) {
          Switch(ToInteger(Arg1)){
            Case(0){
              If (CondRefOf(\_SB.PC00.RP01.POFF)) {\_SB.PC00.RP01.POFF()}
            }
            Case(1){
              If (CondRefOf(\_SB.PC00.RP02.POFF)) {\_SB.PC00.RP02.POFF()}
            }
            Case(2){
              If (CondRefOf(\_SB.PC00.RP03.POFF)) {\_SB.PC00.RP03.POFF()}
            }
            Case(3){
              If (CondRefOf(\_SB.PC00.RP04.POFF)) {\_SB.PC00.RP04.POFF()}
            }
            Case(4){
              If (CondRefOf(\_SB.PC00.RP05.POFF)) {\_SB.PC00.RP05.POFF()}
            }
            Case(5){
              If (CondRefOf(\_SB.PC00.RP06.POFF)) {\_SB.PC00.RP06.POFF()}
            }
            Case(6){
              If (CondRefOf(\_SB.PC00.RP07.POFF)) {\_SB.PC00.RP07.POFF()}
            }
            Case(7){
              If (CondRefOf(\_SB.PC00.RP08.POFF)) {\_SB.PC00.RP08.POFF()}
            }
            Case(8){
              If (CondRefOf(\_SB.PC00.RP09.POFF)) {\_SB.PC00.RP09.POFF()}
            }
            Case(9){
              If (CondRefOf(\_SB.PC00.RP10.POFF)) {\_SB.PC00.RP10.POFF()}
            }
            Case(10){
              If (CondRefOf(\_SB.PC00.RP11.POFF)) {\_SB.PC00.RP11.POFF()}
            }
            Case(11){
              If (CondRefOf(\_SB.PC00.RP12.POFF)) {\_SB.PC00.RP12.POFF()}
            }
            Case(12){
              If (CondRefOf(\_SB.PC00.RP13.POFF)) {\_SB.PC00.RP13.POFF()}
            }
            Case(13){
              If (CondRefOf(\_SB.PC00.RP14.POFF)) {\_SB.PC00.RP14.POFF()}
            }
            Case(14){
              If (CondRefOf(\_SB.PC00.RP15.POFF)) {\_SB.PC00.RP15.POFF()}
            }
            Case(15){
              If (CondRefOf(\_SB.PC00.RP16.POFF)) {\_SB.PC00.RP16.POFF()}
            }
            Case(16){
              If (CondRefOf(\_SB.PC00.RP17.POFF)) {\_SB.PC00.RP17.POFF()}
            }
            Case(17){
              If (CondRefOf(\_SB.PC00.RP18.POFF)) {\_SB.PC00.RP18.POFF()}
            }
            Case(18){
              If (CondRefOf(\_SB.PC00.RP19.POFF)) {\_SB.PC00.RP19.POFF()}
            }
            Case(19){
              If (CondRefOf(\_SB.PC00.RP20.POFF)) {\_SB.PC00.RP20.POFF()}
            }
            Case(20){
              If (CondRefOf(\_SB.PC00.RP21.POFF)) {\_SB.PC00.RP21.POFF()}
            }
            Case(21){
              If (CondRefOf(\_SB.PC00.RP22.POFF)) {\_SB.PC00.RP22.POFF()}
            }
            Case(22){
              If (CondRefOf(\_SB.PC00.RP23.POFF)) {\_SB.PC00.RP23.POFF()}
            }
            Case(23){
              If (CondRefOf(\_SB.PC00.RP24.POFF)) {\_SB.PC00.RP24.POFF()}
            }
            Case(24){
              If (CondRefOf(\_SB.PC00.RP25.POFF)) {\_SB.PC00.RP25.POFF()}
            }
            Case(25){
              If (CondRefOf(\_SB.PC00.RP26.POFF)) {\_SB.PC00.RP26.POFF()}
            }
            Case(26){
              If (CondRefOf(\_SB.PC00.RP27.POFF)) {\_SB.PC00.RP27.POFF()}
            }
            Case(27){
              If (CondRefOf(\_SB.PC00.RP28.POFF)) {\_SB.PC00.RP28.POFF()}
            }
          }
        }
        Case(VMD_STORAGE_REMAP_PORT_CPU_PCIE_RP) {
          Switch(ToInteger(Arg1)) {
            Case(0){
              If (CondRefOf(\_SB.PC00.PEG0.POFF)) {\_SB.PC00.PEG0.POFF()}
            }
            Case(1){
              If (CondRefOf(\_SB.PC00.PEG1.POFF)) {\_SB.PC00.PEG1.POFF()}
            }
            Case(2){
              If (CondRefOf(\_SB.PC00.PEG2.POFF)) {\_SB.PC00.PEG2.POFF()}
            }
            Case(3){
              If (CondRefOf(\_SB.PC00.PEG3.POFF)) {\_SB.PC00.PEG3.POFF()}
            }
          }
        }
      }
    }
  }
}
