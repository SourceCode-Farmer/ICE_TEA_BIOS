/**@file
  VMD remapped storage description for PCIe SSD remapped under VMD controller

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

Name(SD3C, 0) // Is device in D3Cold.

//
// Method to find whether SATA port is part of logical volume
//
Method(VS3D) {
  Return(VD3C(_ADR))
}


//
// Method to turn ON remapped SATA Port.
//
Method(VSON, 0, Serialized) {

  // If device was not set in D3 cold do not re-initiate.
  If(LEqual(SD3C ,0)) {
    Return()
  }

  // Turn on power to the remapped SATA port
  \_SB.PC00.VMD0.VDON(RSPT, RSPI)
  Sleep(16)       // Delay for power ramp.

  Store(0, SD3C)
}

//
// Method to turn OFF remapped SATA Port.
//
Method(VSOF, 0, Serialized) {

  // Check if root port supports D3Cold
  // Return if there is no support
  // to avoid L2/L3 ready transition
  If(LNot(D3CV(RSPT, RSPI))) {
    Return()
  }

  // Turn off power to the remapped SATA port
  \_SB.PC00.VMD0.VDOF(RSPT, RSPI)

  Store(1, SD3C)
}
