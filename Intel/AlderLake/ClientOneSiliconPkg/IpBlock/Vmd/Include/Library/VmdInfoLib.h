/** @file
  @ Header file to get VMD information.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _VMD_INFO_LIB_H_
#define _VMD_INFO_LIB_H_

#include <Uefi.h>

#define VMD_MAX_DEVICES   31
#define MAX_OS_VISIBLE_BUSES_WITH_VMD       224  // Bus range from 225 to 255 is reserved for VMD when it is enabled.
#define MAX_OS_VISIBLE_BUS_LENGTH_WITH_VMD  225  // Buses 0 to 224
#define VMD_DUMMY_DEVICE_ID                 (0x09AB) // Dummy Function DID
//
// VMD Default Setup options
//
#define VMD_CFG_BAR_SIZE_DEFAULT           25   //32MB
#define VMD_MEM_BAR1_SIZE_DEFAULT          25   //32MB
#define VMD_MEM_BAR2_SIZE_DEFAULT          20   //1MB

#define VMD_32BIT_NONPREFETCH              0
#define VMD_64BIT_NONPREFETCH              1
#define VMD_64BIT_PREFETCH                 2


#define EFI_VMD_OS_VARIABLE_REVISION_ID 1
#define EFI_VMD_OS_VARIABLE_NAME        L"IntelVmdOsVariable"

extern EFI_GUID gEfiVmdFeatureVariableGuid;

#pragma pack (push,1)

typedef struct {
    UINT16  RpBusOrIndex  :  8;
    UINT16  RpDevice      :  5;
    UINT16  RpFunction    :  3;
} DEVICE_BDF_DATA;

//
// Structure of VMD EFI variable to be able to r/w by OS
// Byte0: VMD selection and Control
//    Bit[5:0]: VMD Request Entry Count (VREC): The number of valid VMD B:D:F Selection Entries.
//           Any VMD B:D:F Selection Entries beyond this count value are invalid.
//    Bit[15:6]: Reserved

typedef struct {
  struct {
    UINT16  VREC      :  6;  // VMD Request Entry Count (VREC): The number of valid VMD B:D:F Selection Entries. B:D:F Selection Entries beyond this count value are invalid.
    UINT16  Rsvd      :  10;
  } BYTE0_1;
  DEVICE_BDF_DATA DevDetails[VMD_MAX_DEVICES];
} EFI_VMD_OS_DATA;

#pragma pack (pop)


/**
  GetVmdBusNumber: Get VMD Bus Number

  @retval PCI bus number for VMD
**/
UINT8
GetVmdBusNumber (
  VOID
  );

/**
  GetVmdDevNumber: Get VMD Dev Number

  @retval PCI dev number for VMD
**/
UINT8
GetVmdDevNumber (
  VOID
  );

/**
  GetVmdFunNumber: Get VMD Fun Number

  @retval PCI fun number for VMD
**/
UINT8
GetVmdFuncNumber (
  VOID
  );


/**
  IsVmdEnabled: Check if VMD is enabled by reading the VMD Device Id

  @retval TRUE  if  VMD is enabled
  @retval False if  VMD is not enabled
**/
BOOLEAN
IsVmdEnabled (
  );


/**
  IsVmdLocked: Get the status of VMD lock for critical config space registers of VMD
re
  @retval EFI_SUCCESS if  fun number for VMD
**/
BOOLEAN
IsVmdLocked (
  );


/**
  IsVmdSupported: Get the status of VMD support bit from capability register.

  @retval TRUE  if  VMD is supported
  @retval False if  VMD is not supported
**/
BOOLEAN
IsVmdSupported (
  VOID
  );


/**
  ProgramVmdTempBars: Program VMD temporary bars.

  @param[in]     VmdCfgBarBase    temporary address for CfgBar.
  @param[in]     VmdMemBar1Base   temporary address for CfgBar.
  @param[in]     VmdMemBar1Base   temporary address for CfgBar.

  @retval VOID
**/
VOID
ProgramVmdTempBars(
  IN UINT32 VmdCfgBarBase,
  IN UINT32 VmdMemBar1Base,
  IN UINT32 VmdMemBar2Base
  );

/**
  ClearVmdTempBars: Clear VMD temporary bars assigned during PEI phase.

  @retval VOID
**/
VOID
ClearVmdTempBars(
  VOID
  );

#endif /* _VMD_INFO_LIB_H_ */
