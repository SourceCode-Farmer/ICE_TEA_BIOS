/** @file
  The PEI VMD Init Library in Si init

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/PciSegmentLib.h>
#include <Uefi/UefiBaseType.h>
#include <PcieRegs.h>
#include <Library/PchPcieRpLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PostCodeLib.h>
#include <Library/PeiVmdInitLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Register/CpuPcieRegs.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/PchInfoLib.h>
#include <Register/PchRegs.h>
#include <VmdInfoHob.h>
#include <Register/SataRegs.h>
#include <Register/VmdRegs.h>
#include <Library/PchPciBdfLib.h>
#include <Library/SataSocLib.h>
#include <CpuPcieInfo.h>
#include <CpuSbInfo.h>
#include <Library/VmdInfoLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/PeiVmdInitFruLib.h>

EFI_PEI_PPI_DESCRIPTOR  gVmdInitDonePpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gVmdInitDonePpiGuid,
  NULL
};

/**
  This function performs basic initialization for VMD in PEI phase (post-mem)
  @param VMD_PEI_PREMEM_CONFIG - Pointer to config data
  @retval     EFI_SUCCESS     Successfully initialized VMD
  @retval     EFI_UNSUPPORTED         VMD unsupported
  @retval     EFI_INVALID_PARAMETER   If VmdPeiConfig is null, return invalid parameter
**/
EFI_STATUS
VmdInit (
  IN   VMD_PEI_CONFIG  *VmdPeiConfig
  )
{
  CFGBARSIZE_VMD_STRUCT      VmdCfgBarSize;
  MEMBAR1SIZE_VMD_STRUCT     VmdMemBar1Size;
  MEMBAR2SIZE_VMD_STRUCT     VmdMemBar2Size;
  VMASSIGN_VMD_STRUCT        VmAssign;
  CFGBAR_N0_VMD_STRUCT       CfgBar;
  MEMBAR1_N0_VMD_STRUCT      MemBar1;
  MEMBAR2_N0_VMD_STRUCT      MemBar2;
  VMCONFIG_VMD_STRUCT        VmConfig;
  VMCAP_VMD_STRUCT           VmCap;

  FN0_ASSIGN_VMD_STRUCT      Fn0Assign;
  FN1_ASSIGN_VMD_STRUCT      Fn1Assign;
  FN2_ASSIGN_VMD_STRUCT      Fn2Assign;
  FN3_ASSIGN_VMD_STRUCT      Fn3Assign;
  FN4_ASSIGN_VMD_STRUCT      Fn4Assign;
  FN5_ASSIGN_VMD_STRUCT      Fn5Assign;
  FN6_ASSIGN_VMD_STRUCT      Fn6Assign;
  FN7_ASSIGN_VMD_STRUCT      Fn7Assign;

  UINT32                     DeviceBaseAddress;
  UINT8                      Attr;
  VMD_INFO_HOB               *VmdInfoHob;
  UINT8                      Index;
  UINT8                      HobIndex;
  UINT16                     DidAssign;
  EFI_STATUS                 Status;
  EFI_VMD_OS_DATA            *VmdVariablePtr;

  //
  // VMD Initializations if the VMD IP is Supported
  //
  if (IsVmdSupported()!= TRUE) {
    DEBUG ((DEBUG_INFO, "------- Vmd is not supported -----\n"));
    return EFI_UNSUPPORTED;
  }
  DEBUG ((DEBUG_INFO, "-----------------VmdInit Start ----------------\n"));
  if (VmdPeiConfig == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Create HOB for VMD INFO
  //
  Status = PeiServicesCreateHob (
             EFI_HOB_TYPE_GUID_EXTENSION,
             sizeof (VMD_INFO_HOB),
             (VOID **) &VmdInfoHob
             );
  ASSERT_EFI_ERROR (Status);

  //
  // Initialize default HOB data
  //
  VmdInfoHob->EfiHobGuidType.Name = gVmdInfoHobGuid;
  DEBUG ((DEBUG_INFO, "VmdInfoHob->EfiHobGuidType.Name: %g\n", &VmdInfoHob->EfiHobGuidType.Name));
  ZeroMem (&(VmdInfoHob->VmdPortInfo), sizeof (VMD_PORT_INFO));

  DEBUG ((DEBUG_INFO, "VmdInfoHob @ %X\n", VmdInfoHob));
  DEBUG ((DEBUG_INFO, "&(VmdInfoHob->VmdPortInfo) @ %X\n", &(VmdInfoHob->VmdPortInfo)));
  DEBUG ((DEBUG_INFO, "VmdHobSize - HobHeaderSize: %X\n", sizeof (VMD_INFO_HOB) - sizeof (EFI_HOB_GUID_TYPE)));
  DEBUG ((DEBUG_INFO, "VmdInfoSize: %X\n", sizeof (VMD_PORT_INFO)));

  //
  // Get the HOB for VMD INFO
  //
  VmdInfoHob = (VMD_INFO_HOB *) GetFirstGuidHob (&gVmdInfoHobGuid);
  if (VmdInfoHob == NULL) {
    DEBUG ((EFI_D_INFO, "Vmd Info Hob not found\n"));
    return EFI_NOT_FOUND;
  }

  //
  // Detect the mass storage devices/controller connected to CPU and PCH root ports
  //
  VmdDetectPcieStorageDevices (VmdInfoHob);

  DEBUG ((DEBUG_INFO, "Dumping port info\n"));
  for (Index = 0; Index < VMD_MAX_DEVICES; ++Index) {
    if (VmdInfoHob->VmdPortInfo.PortInfo[Index].DeviceDetected) {
      DEBUG ((DEBUG_INFO, "RpDev  %d\t", VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev));
      DEBUG ((DEBUG_INFO, "RpFunc  %d\t", VmdInfoHob->VmdPortInfo.PortInfo[Index].RpFunc));
      DEBUG ((DEBUG_INFO, "DevId  0x%x\t", VmdInfoHob->VmdPortInfo.PortInfo[Index].DevId));
      DEBUG ((DEBUG_INFO, "RpIndex  %d\n", VmdInfoHob->VmdPortInfo.PortInfo[Index].RpIndex));
    }
  }

  //
  // Initializations only if VMD is enabled
  //
  if (!VmdPeiConfig->VmdEnable) { // if VMD is not enabled by user
    //disable the devEnable bit for VMD device (bit 14) if its enabled
    if ((PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM,SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DEVEN))) & ((UINT32) (B_SA_DEVEN_D14F0EN_MASK))){
      PciSegmentAnd32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM,SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DEVEN), (UINT32) ~(B_SA_DEVEN_D14F0EN_MASK));
    }
    DEBUG ((DEBUG_INFO, "\nVMD Device has been disabled\n"));
    return EFI_SUCCESS;
  }

  DEBUG((DEBUG_INFO, "VMD option is enabled in the setup \n"));

  // Check the devEnable bit for VMD device (bit 14)
  if ((PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM,SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DEVEN))) & ((UINT32) (B_SA_DEVEN_D14F0EN_MASK))) {
    DEBUG((DEBUG_INFO, "VMD device is already enabled\n"));
  } else {
    DEBUG((DEBUG_INFO, "Enabling VMD device in DevEn \n"));
    PciSegmentOr32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM,SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DEVEN), (UINT32) (B_SA_DEVEN_D14F0EN_MASK));
  }

  VmdVariablePtr = (EFI_VMD_OS_DATA *)VmdPeiConfig->VmdVariablePtr;
  DeviceBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, VMD_BUS_NUM, VMD_DEV_NUM, VMD_FUN_NUM,0);

  //
  // Reset and then Configure FUN assign registers as per the policy / user selection of Dev and Func
  //
  Fn0Assign.Data = 0;
  PciSegmentWrite32 ( DeviceBaseAddress + R_FN0_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn0Assign.Data);
  Fn1Assign.Data = 0;
  PciSegmentWrite32 ( DeviceBaseAddress + R_FN1_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn1Assign.Data);
  Fn2Assign.Data = 0;
  PciSegmentWrite32 ( DeviceBaseAddress + R_FN2_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn2Assign.Data);
  Fn3Assign.Data = 0;
  PciSegmentWrite32 ( DeviceBaseAddress + R_FN3_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn3Assign.Data);
  Fn4Assign.Data = 0;
  PciSegmentWrite32 ( DeviceBaseAddress + R_FN4_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn4Assign.Data);
  Fn5Assign.Data = 0;
  PciSegmentWrite32 ( DeviceBaseAddress + R_FN5_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn5Assign.Data);
  Fn6Assign.Data = 0;
  PciSegmentWrite32 ( DeviceBaseAddress + R_FN6_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn6Assign.Data);
  Fn7Assign.Data = 0;
  PciSegmentWrite32 ( DeviceBaseAddress + R_FN7_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn7Assign.Data);

  // If the Global Mapping Policy is enabled then map all the detected devices under VMD
  if (VmdPeiConfig->VmdGlobalMapping) {
    DEBUG ((DEBUG_INFO, "Vmd Global Mapping policy is enabled so mapping all detected storage devices under VMD!!\n"));
    for (Index = 0; Index < VMD_MAX_DEVICES; ++Index) {
      // check the list of detected devices and map all under VMD
      if (VmdInfoHob->VmdPortInfo.PortInfo[Index].DeviceDetected) {
        VmdInfoHob->VmdPortInfo.PortInfo[Index].PortEn = 1;
      }
    }
  } else { // map devices based on user selection in BIOS setup and UEFI variable input from OS.
    // Check if platform configuration is same as last boot by comparing config block and hob entries
    // Apply policy from previous boot if matching B/D/F is found in current boot.
    DEBUG ((DEBUG_VERBOSE, "Vmd check if platfrom configuration is same as last boot by comparing config block and hob entries\n"));
    for (Index = 0; Index < VMD_MAX_DEVICES; ++Index) {
      if (VmdPeiConfig->VmdPortEnable[Index].RpEnable) {
        // check if this b/d/f is still valid for the current boot
        for (HobIndex = 0; HobIndex < VMD_MAX_DEVICES; ++HobIndex) {
          if ((VmdPeiConfig->VmdPortEnable[Index].RpDevice == VmdInfoHob->VmdPortInfo.PortInfo[HobIndex].RpDev)
            &&(VmdPeiConfig->VmdPortEnable[Index].RpFunction == VmdInfoHob->VmdPortInfo.PortInfo[HobIndex].RpFunc)) {
            DEBUG ((DEBUG_INFO, "Vmd Platfrom configuration is same, applying earlier user selection!!\n"));
            VmdInfoHob->VmdPortInfo.PortInfo[HobIndex].PortEn = 1;
            break;
          }
        }
      }
    }
    DEBUG ((DEBUG_INFO, "Dumping Vmd OS Variable details\n"));
    DEBUG ((DEBUG_INFO, "VMD Request Entry Count is %d\n", VmdVariablePtr->BYTE0_1.VREC));
    for (Index=0; Index < VmdVariablePtr->BYTE0_1.VREC; ++Index) {
      DEBUG ((DEBUG_INFO, "Storage device B/D/F or RpIndex/D/F is %d/%d/%d\n", VmdVariablePtr->DevDetails[Index].RpBusOrIndex,
          VmdVariablePtr->DevDetails[Index].RpDevice, VmdVariablePtr->DevDetails[Index].RpFunction));
    }

    // Assigning devices as per user selection in OS GUI by comparing either Rp Index or Rp function.
    for (Index=0; Index < VmdVariablePtr->BYTE0_1.VREC; ++Index) {
      for (int j = 0; j < VMD_MAX_DEVICES; ++j) {
        if ((VmdInfoHob->VmdPortInfo.PortInfo[j].RpDev == VmdVariablePtr->DevDetails[Index].RpDevice) &&
            ((VmdInfoHob->VmdPortInfo.PortInfo[j].RpFunc == VmdVariablePtr->DevDetails[Index].RpFunction) ||
            (VmdInfoHob->VmdPortInfo.PortInfo[j].RpIndex == VmdVariablePtr->DevDetails[Index].RpBusOrIndex))) {
              VmdInfoHob->VmdPortInfo.PortInfo[j].PortEn = 1;
              DEBUG ((DEBUG_INFO, "Mapping device under VMD as per user selection in OS GUI for b/d/f 0/%d/%d \n",
                  VmdInfoHob->VmdPortInfo.PortInfo[j].RpDev,VmdInfoHob->VmdPortInfo.PortInfo[j].RpFunc));
        }
      }
    }
  }

  for (Index = 0; Index < VMD_MAX_DEVICES; ++Index) {
    if (VmdInfoHob->VmdPortInfo.PortInfo[Index].DeviceDetected) {
      if (VmdInfoHob->VmdPortInfo.PortInfo[Index].PortEn == 1) {
        DEBUG ((DEBUG_INFO, "VMD: Port %d is mapped under VMD\n", Index));
        switch(VmdInfoHob->VmdPortInfo.PortInfo[Index].RpFunc) {
          case 0:
          Fn0Assign.Data |= 1 << VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev;
          PciSegmentWrite32 ( DeviceBaseAddress + R_FN0_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn0Assign.Data);
          break;
          case 1:
          Fn1Assign.Data |= 1 << VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev;
          PciSegmentWrite32 ( DeviceBaseAddress + R_FN1_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn1Assign.Data);
          break;
          case 2:
          Fn2Assign.Data |= 1 << VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev;
          PciSegmentWrite32 ( DeviceBaseAddress + R_FN2_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn2Assign.Data);
          break;
          case 3:
          Fn3Assign.Data |= 1 << VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev;
          PciSegmentWrite32 ( DeviceBaseAddress + R_FN3_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn3Assign.Data);
          break;
          case 4:
          Fn4Assign.Data |= 1 << VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev;
          PciSegmentWrite32 ( DeviceBaseAddress + R_FN4_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn4Assign.Data);
          break;
          case 5:
          Fn5Assign.Data |= 1 << VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev;
          PciSegmentWrite32 ( DeviceBaseAddress + R_FN5_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn5Assign.Data);
          break;
          case 6:
          Fn6Assign.Data |= 1 << VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev;
          PciSegmentWrite32 ( DeviceBaseAddress + R_FN6_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn6Assign.Data);
          break;
          case 7:
          Fn7Assign.Data |= 1 << VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev;
          PciSegmentWrite32 ( DeviceBaseAddress + R_FN7_ASSIGN_0_14_0_PCI_VMD_REG_OFFSET, Fn7Assign.Data);
          break;
        }
      }
    }// end of device detected
  }
  // program VMAssign only when need to assign device and all its function under VMD
  VmAssign.Data = PciSegmentRead32 (DeviceBaseAddress + R_VMASSIGN_0_14_0_PCI_VMD_REG_OFFSET);
  // Skip VMD configuration if Config space is not valid.
  if (VmAssign.Data == 0xFFFFFFFF) {
    DEBUG((DEBUG_INFO, "VMD %02d not present%d\n",VmAssign.Data));
    return EFI_UNSUPPORTED;
  }

  // program CfgBarSize
  VmdCfgBarSize.Data  = PciSegmentRead8 (DeviceBaseAddress + R_CFGBARSIZE_0_14_0_PCI_VMD_REG_OFFSET);
  VmdCfgBarSize.Bits.Size = VmdPeiConfig->VmdCfgBarSize;
  PciSegmentWrite8 (DeviceBaseAddress + R_CFGBARSIZE_0_14_0_PCI_VMD_REG_OFFSET,VmdCfgBarSize.Data);

  // program MemBarSize 1/2
  VmdMemBar1Size.Data = PciSegmentRead8 (DeviceBaseAddress + R_MEMBAR1SIZE_0_14_0_PCI_VMD_REG_OFFSET);
  VmdMemBar1Size.Bits.Size = VmdPeiConfig->VmdMemBarSize1;
  PciSegmentWrite8 (DeviceBaseAddress + R_MEMBAR1SIZE_0_14_0_PCI_VMD_REG_OFFSET,VmdMemBar1Size.Data);

  VmdMemBar2Size.Data = PciSegmentRead8 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM,
  VMD_BUS_NUM, VMD_DEV_NUM, VMD_FUN_NUM, R_MEMBAR2SIZE_0_14_0_PCI_VMD_REG_OFFSET));
  VmdMemBar2Size.Bits.Size = VmdPeiConfig->VmdMemBarSize2;
  PciSegmentWrite8 (DeviceBaseAddress + R_MEMBAR2SIZE_0_14_0_PCI_VMD_REG_OFFSET,VmdMemBar2Size.Data);

  DEBUG ((DEBUG_INFO, "VMD VMAssign:%02x CfgBarSize:%02d MemBar1Size:%02d MemBar2Size:%02d\n",
  VmAssign.Data, VmdCfgBarSize.Data, VmdMemBar1Size.Data, VmdMemBar2Size.Data));

  DEBUG((DEBUG_INFO, "Fn0Assign.Data = 0x%x \n", Fn0Assign.Data));
  DEBUG((DEBUG_INFO, "Fn1Assign.Data = 0x%x \n", Fn1Assign.Data));
  DEBUG((DEBUG_INFO, "Fn2Assign.Data = 0x%x \n", Fn2Assign.Data));
  DEBUG((DEBUG_INFO, "Fn3Assign.Data = 0x%x \n", Fn3Assign.Data));
  DEBUG((DEBUG_INFO, "Fn4Assign.Data = 0x%x \n", Fn4Assign.Data));
  DEBUG((DEBUG_INFO, "Fn5Assign.Data = 0x%x \n", Fn5Assign.Data));
  DEBUG((DEBUG_INFO, "Fn6Assign.Data = 0x%x \n", Fn6Assign.Data));
  DEBUG((DEBUG_INFO, "Fn7Assign.Data = 0x%x \n", Fn7Assign.Data));

  // program CfgBar attribute
  CfgBar.Data = PciSegmentRead32 (DeviceBaseAddress + R_CFGBAR_N0_0_14_0_PCI_VMD_REG_OFFSET);
  Attr = VmdPeiConfig->VmdCfgBarAttr;
  switch(Attr) {
    case VMD_32BIT_NONPREFETCH:  //32-bit non-prefetchable
    CfgBar.Bits.Prefetchable = 0;
    CfgBar.Bits.Type = 0;
    break;
    case VMD_64BIT_NONPREFETCH: //64-bit non-prefetcheble
    CfgBar.Bits.Prefetchable = 0;
    CfgBar.Bits.Type = 2;
    break;
    case VMD_64BIT_PREFETCH:  //64-bit prefetchable
    CfgBar.Bits.Prefetchable = 1;
    CfgBar.Bits.Type = 2;
    break;
  }
  PciSegmentWrite32 (DeviceBaseAddress + R_CFGBAR_N0_0_14_0_PCI_VMD_REG_OFFSET, CfgBar.Data);

  // program MemBar1 attribute
  MemBar1.Data = PciSegmentRead32 (DeviceBaseAddress + R_MEMBAR1_N0_0_14_0_PCI_VMD_REG_OFFSET);
  Attr = VmdPeiConfig->VmdMemBar1Attr;
  switch(Attr) {
    case VMD_32BIT_NONPREFETCH:  //32-bit non-prefetchable
    MemBar1.Bits.Prefetchable = 0;
    MemBar1.Bits.Type = 0;
    break;
    case VMD_64BIT_NONPREFETCH: //64-bit non-prefetchable
    MemBar1.Bits.Prefetchable = 0;
    MemBar1.Bits.Type = 2;
    break;
    case VMD_64BIT_PREFETCH:  //64-bit prefetchable
    MemBar1.Bits.Prefetchable = 1;
    MemBar1.Bits.Type = 2;
    break;
  }
  PciSegmentWrite32 (DeviceBaseAddress + R_MEMBAR1_N0_0_14_0_PCI_VMD_REG_OFFSET, MemBar1.Data);

  // program MemBar2 attribute
  MemBar2.Data = PciSegmentRead32 (DeviceBaseAddress + R_MEMBAR2_N0_0_14_0_PCI_VMD_REG_OFFSET);
  Attr = VmdPeiConfig->VmdMemBar2Attr;
  switch(Attr) {
    case VMD_32BIT_NONPREFETCH:  //32-bit non-prefetchable
    MemBar2.Bits.Prefetchable = 0;
    MemBar2.Bits.Type = 0;
    break;
    case VMD_64BIT_NONPREFETCH: // 64-bit non-prefetchable
    MemBar2.Bits.Prefetchable = 0;
    MemBar2.Bits.Type = 2;
    break;
    case VMD_64BIT_PREFETCH:  //64-bit prefetchable
    MemBar2.Bits.Prefetchable = 1;
    MemBar2.Bits.Type = 2;
    break;
  }
  PciSegmentWrite32 (DeviceBaseAddress + R_MEMBAR2_N0_0_14_0_PCI_VMD_REG_OFFSET, MemBar2.Data);

  DEBUG((DEBUG_INFO, "VMD VmAssign:0x%02x CfgBar:0x%02x MemBar1:0x%02x MemBar2:0x%02x\n",
  VmAssign.Data, CfgBar.Data, MemBar1.Data, MemBar2.Data));

  // program VMCAP and VMConfig to apply bus restrictions and select bus numbering
  VmCap.Data = PciSegmentRead32 (DeviceBaseAddress + R_VMCAP_0_14_0_PCI_VMD_REG_OFFSET);
  VmCap.Bits.BusRestrictCap = 0x1;
  PciSegmentWrite32 (DeviceBaseAddress + R_VMCAP_0_14_0_PCI_VMD_REG_OFFSET, VmCap.Data);

  VmConfig.Data = PciSegmentRead32 (DeviceBaseAddress + R_VMCONFIG_0_14_0_PCI_VMD_REG_OFFSET);
  DEBUG((DEBUG_INFO, "Read after write VmConfig 0x%x\n", VmConfig.Data));

  // Select bus numbers 225 to 255 for VMD
  VmConfig.Bits.BusRestrictions = 0x2;

  // write DID value to DID_ASSIGN field
  // BIOS can simply copy those 8 MSB from DID of device0.
  // BIOS should write the lower 8 MSB to 0x0B (this value is not expected to change)
  DidAssign = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM,SA_MC_BUS,
        SA_MC_DEV, SA_MC_FUN, PCI_DEVICE_ID_OFFSET));
  DidAssign = (DidAssign & 0xFF00) | VMD_DID_LSB;
  DEBUG((DEBUG_INFO, "DidAssign is 0x%x\n", DidAssign));

  VmConfig.Bits.Did_Assign = DidAssign;

  // program VMConfig to lock VMD registers
  VmConfig.Bits.VmdLock = 1;

  DEBUG((DEBUG_INFO, "value to be written to VmConfig 0x%x\n", VmConfig.Data));
  PciSegmentWrite32 (DeviceBaseAddress + R_VMCONFIG_0_14_0_PCI_VMD_REG_OFFSET, VmConfig.Data);
  VmConfig.Data = PciSegmentRead32 (DeviceBaseAddress + R_VMCONFIG_0_14_0_PCI_VMD_REG_OFFSET);
  DEBUG((DEBUG_INFO, "Read after write VmConfig 0x%x\n", VmConfig.Data));

  DEBUG((DEBUG_INFO, "Bus Restrictions are applied and Lock Bit is set for VMD \n"));
  ///
  /// Program temp VMD BARs
  ///
  ProgramVmdTempBars(VmdPeiConfig->VmdCfgBarBase, VmdPeiConfig->VmdMemBar1Base, VmdPeiConfig->VmdMemBar2Base);
  //
  // Install Vmd Init Done PPI to notify PEIM module.
  //
  Status = PeiServicesInstallPpi (&gVmdInitDonePpi);
  DEBUG ((DEBUG_INFO, "Installed gVmdInitDonePpi\n"));
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "------------------ VmdInit End ------------------\n"));
  return EFI_SUCCESS;
}