/** @file
  Source file for Tcss Info Lib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/TcssInfoLib.h>
#include <Register/HostDmaRegs.h>
#include <Register/CpuUsbRegs.h>
#include <Register/ItbtPcieRegs.h>
/**
  GetTbtDmaBusNumber: Get TbtDma Bus Number

  @retval PCI bus number for TbtDma
**/
UINT64
GetTbtDmaBusNumber (
  VOID
  )
{
  return (UINT64)HOST_DMA_BUS_NUM;
}

/**
  GetTbtDmaDevNumber: Get TbtDma Dev Number

  @retval PCI dev number for TbtDma
**/
UINT64
GetTbtDmaDevNumber (
  VOID
  )
{
  return (UINT64)HOST_DMA_DEV_NUM;
}

/**
  GetTbtDma0FunNumber: Get TbtDma Fun Number

  @retval PCI fun number for TbtDma
**/
UINT64
GetTbtDma0FuncNumber (
  VOID
  )
{
  return (UINT64)HOST_DMA0_FUN_NUM;
}

/**
  GetCpuXhciBusNumber: Get CpuXhci Bus Number

  @retval PCI bus number for CpuXhci
**/
UINT64
GetCpuXhciBusNumber (
  VOID
  )
{
  return (UINT64)XHCI_NORTH_BUS_NUM;
}

/**
  GetCpuXhciDevNumber: Get CpuXhci Dev Number

  @retval PCI dev number for CpuXhci
**/
UINT64
GetCpuXhciDevNumber (
  VOID
  )
{
  return (UINT64)XHCI_NORTH_DEV_NUM;
}

/**
  GetCpuXhciFunNumber: Get CpuXhci Fun Number

  @retval PCI fun number for CpuXhci
**/
UINT64
GetCpuXhciFuncNumber (
  VOID
  )
{
  return (UINT64)XHCI_NORTH_FUNC_NUM;
}

/**
  GetITbtPcieDevNumber: Get ITbt Dev Number

  @retval ITbt dev number for CpuXhci
**/
UINT64
GetITbtPcieDevNumber (
  VOID
  )
{
  return (UINT64)ITBT_PCIE_DEV_NUM;
}
