/** @file
  The PEI TCSS Init Library Implements After Memory PEIM

@copyright
  INTEL CONFIDENTIAL
  Copyright 2016 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/PeiServicesLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/PciSegmentLib.h>
#include <Uefi/UefiBaseType.h>
#include <PcieRegs.h>
#include <Register/HostDmaRegs.h>
#include <Register/IomRegs.h>
#include <Register/CpuUsbRegs.h>
#include <Register/PchPcrRegs.h>
#include <Register/UsbRegs.h>
#include <Library/PchPcieRpLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PostCodeLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/TcssInitLib.h>
#include <SaConfigHob.h>
#include <TcssDataHob.h>
#include <Ppi/SiPolicy.h>
#include <Library/TimerLib.h>
#include <Library/GpioPrivateLib.h>
#include <Library/DciPrivateLib.h>
#include <Library/CpuRegbarAccessLib.h>
#include <Library/ItbtPcieRpInitLib.h>
#include <Library/UsbHostControllerInitLib.h>
#include <Library/UsbDeviceControllerInitLib.h>
#include <Library/PeiTbtTaskDispatchLib.h>
#include <Library/PeiITbtInitLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/TcssPmcLib.h>
#include <Library/ItbtPcieRpLib.h>
#include <PeiITbtConfig.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <CpuSbInfo.h>
#include <Register/ItbtPcieRegs.h>
#include <Library/PeiTcssInitFruLib.h>
#include <Library/PeiSiSsidLib.h>
#include <ConfigBlock.h>
#include <ConfigBlock/SiConfig.h>

STATIC SVID_SID_SKIP_TABLE mTcssSkipSsidTable[] = {{ITBT_PCIE_BUS_NUM,  ITBT_PCIE_DEV_NUM,  ITBT_PCIE_0_DEFAULT_FUN_NUM},
                                                   {ITBT_PCIE_BUS_NUM,  ITBT_PCIE_DEV_NUM,  ITBT_PCIE_1_DEFAULT_FUN_NUM},
                                                   {ITBT_PCIE_BUS_NUM,  ITBT_PCIE_DEV_NUM,  ITBT_PCIE_2_DEFAULT_FUN_NUM},
                                                   {ITBT_PCIE_BUS_NUM,  ITBT_PCIE_DEV_NUM,  ITBT_PCIE_3_DEFAULT_FUN_NUM},
                                                   {XHCI_NORTH_BUS_NUM, XHCI_NORTH_DEV_NUM, XHCI_NORTH_FUNC_NUM},
                                                   {XDCI_NORTH_BUS_NUM, XDCI_NORTH_DEV_NUM, XDCI_NORTH_FUNC_NUM},
                                                   {HOST_DMA_BUS_NUM,   HOST_DMA_DEV_NUM,   HOST_DMA0_FUN_NUM},
                                                   {HOST_DMA_BUS_NUM,   HOST_DMA_DEV_NUM,   HOST_DMA1_FUN_NUM}
                                                  };

STATIC SVID_SID_SKIP_TABLE mTcssSsidTable[] = {{ITBT_PCIE_BUS_NUM,  ITBT_PCIE_DEV_NUM,  ITBT_PCIE_0_DEFAULT_FUN_NUM},
                                               {ITBT_PCIE_BUS_NUM,  ITBT_PCIE_DEV_NUM,  ITBT_PCIE_1_DEFAULT_FUN_NUM},
                                               {ITBT_PCIE_BUS_NUM,  ITBT_PCIE_DEV_NUM,  ITBT_PCIE_2_DEFAULT_FUN_NUM},
                                               {ITBT_PCIE_BUS_NUM,  ITBT_PCIE_DEV_NUM,  ITBT_PCIE_3_DEFAULT_FUN_NUM},
                                               {XHCI_NORTH_BUS_NUM, XHCI_NORTH_DEV_NUM, XHCI_NORTH_FUNC_NUM},
                                               {XDCI_NORTH_BUS_NUM, XDCI_NORTH_DEV_NUM, XDCI_NORTH_FUNC_NUM}
                                              };

EFI_STATUS
EFIAPI
TcssInitEndOfPei (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR  mEndOfPeiNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) TcssInitEndOfPei
};

EFI_PEI_PPI_DESCRIPTOR mTcssPeiInitDonePpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gTcssPeiInitDonePpiGuid,
  NULL
};

/**
  Installs TCSS Data Hob

  @retval EFI_SUCCESS           The HOB was successfully created.
  @retval EFI_OUT_OF_RESOURCES  There is no additional space for HOB creation.
**/
EFI_STATUS
InstallTcssDataHob (
  VOID
  )
{

  EFI_STATUS                  Status;
  TCSS_DATA_HOB               *TcssHob;

  ///
  /// Create HOB for TCSS Data
  ///
  Status = PeiServicesCreateHob (
             EFI_HOB_TYPE_GUID_EXTENSION,
             sizeof (TCSS_DATA_HOB),
             (VOID **) &TcssHob
             );
  ASSERT_EFI_ERROR (Status);
  ///
  /// Initialize default HOB data
  ///
  TcssHob->EfiHobGuidType.Name = gTcssHobGuid;
  DEBUG ((DEBUG_INFO, "TcssHob->EfiHobGuidType.Name: %g\n", &TcssHob->EfiHobGuidType.Name));
  ZeroMem (&(TcssHob->TcssData), sizeof (TCSS_DATA));

  DEBUG ((DEBUG_INFO, "TcssHob @ %X\n", TcssHob));
  DEBUG ((DEBUG_INFO, "&(TcssHob->TcssData) @ %X\n", &(TcssHob->TcssData)));
  DEBUG ((DEBUG_INFO, "TcssHob - HobHeader: %X\n", sizeof (TCSS_DATA_HOB) - sizeof (EFI_HOB_GUID_TYPE)));
  DEBUG ((DEBUG_INFO, "TcssData: %X\n", sizeof (TCSS_DATA)));

  return Status;
}

/**
  Installs TCSS SSID data hob

  @retval EFI_SUCCESS           The HOB was successfully created.
  @retval EFI_OUT_OF_RESOURCES  There is no additional space for HOB creation.
**/
EFI_STATUS
InstallTcssSsidDataHob (
  VOID
  )
{

  EFI_STATUS                  Status;
  TCSS_DATA_SSID_HOB          *TcssSsidHob;

  ///
  /// Create HOB for TCSS SSID Data
  ///
  Status = PeiServicesCreateHob (
             EFI_HOB_TYPE_GUID_EXTENSION,
             sizeof (TCSS_DATA_SSID_HOB),
             (VOID **) &TcssSsidHob
             );
  ASSERT_EFI_ERROR (Status);
  ///
  /// Initialize default HOB data
  ///
  TcssSsidHob->EfiHobGuidType.Name = gTcssSsidHobGuid;
  TcssSsidHob->TcssSsidTable = mTcssSkipSsidTable;
  TcssSsidHob->NumberOfTcssSsidTable = sizeof (mTcssSkipSsidTable) / sizeof (mTcssSkipSsidTable[0]);
  DEBUG ((DEBUG_INFO, "TcssSsidHob->EfiHobGuidType.Name: %g\n", &TcssSsidHob->EfiHobGuidType.Name));
  DEBUG ((DEBUG_INFO, "TcssSsidHob @ %X\n", TcssSsidHob));
  DEBUG ((DEBUG_INFO, "&(TcssHob->TcssSsidTable) @ %X\n", &(TcssSsidHob->TcssSsidTable)));
  DEBUG ((DEBUG_INFO, "TcssSsidHob->NumberOfTcssSsidTable: %X\n", TcssSsidHob->NumberOfTcssSsidTable));

  return Status;
}

/**
  This function performs read MG IMR status to HOB

**/
VOID
UpdateMgImrStatus (
  VOID
  )
{
  MG_IMR_STATUS               MgImr;
  TCSS_DATA_HOB               *TcssHob;

  DEBUG ((DEBUG_INFO, "[TCSS] UpdateMgImrStatus Entry\n"));

  TcssHob = NULL;
  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob == NULL) {
    ASSERT (FALSE);
    return;
  }

  //
  // Read MG IMR status
  //
  MgImr.RegValue = TcssSbIomRead (R_IOM_CSME_IMR_MG_STATUS);
  DEBUG ((DEBUG_INFO, "R_IOM_CSME_IMR_MG_STATUS: 0x%08X\n", MgImr.RegValue));
  DEBUG ((DEBUG_INFO, "MG FW version: 0x%04X\n", (UINT16) MgImr.Bits.MgFwVersion));

  if (MgImr.Bits.Done) {
    if (MgImr.Bits.Valid) {
      TcssHob->TcssData.MgImrStatus.RegValue = MgImr.RegValue;
      DEBUG ((DEBUG_ERROR, "Load MG FW success\n"));
    } else {
      DEBUG ((DEBUG_ERROR, "untrusted MG FW\n"));
      ASSERT (FALSE);
    }
  } else {
    DEBUG ((DEBUG_ERROR, "MG FW has not yet downloaded to IMR\n"));
    if (!IsSimicsEnvironment ()) {
      ASSERT (FALSE);
    }
  }

  DEBUG ((DEBUG_INFO, "[TCSS] UpdateMgImrStatus End\n"));

  return;
}

/**
  This function handles TcssInit task at the end of PEI

  @param[in]  PeiServices  Pointer to PEI Services Table.
  @param[in]  NotifyDesc   Pointer to the descriptor for the Notification event that
                           caused this function to execute.
  @param[in]  Ppi          Pointer to the PPI data associated with this function.

  @retval     EFI_SUCCESS      The function completes successfully
  @retval     EFI_UNSUPPORTED  The TBT FW not ready
  @retval     others
**/
EFI_STATUS
EFIAPI
TcssInitEndOfPei (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                    Status;
  UINT8                         Index;
  BOOLEAN                       ITbtExisted;
  SI_POLICY_PPI                 *SiPolicyPpi;
  PEI_ITBT_CONFIG               *PeiITbtConfig;
  TCSS_DATA_HOB                 *TcssHob;
  TBT_IMR_STATUS                TbtImr;
  UINT64                        McD0BaseAddress;
  UINT32                        MchBar;
  TCSS_DATA_SSID_HOB            *TcssSsidHob;

  DEBUG ((DEBUG_INFO, "[TCSS] TcssInitEndOfPei Entry\n"));

  Status                = EFI_SUCCESS;
  SiPolicyPpi           = NULL;
  PeiITbtConfig         = NULL;
  Index                 = 0;
  ITbtExisted           = FALSE;
  McD0BaseAddress       = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
  MchBar                = PciSegmentRead32 (McD0BaseAddress + R_SA_MCHBAR) & ~B_SA_MCHBAR_MCHBAREN_MASK;

  UpdateMgImrStatus ();

  Status = PeiServicesLocatePpi (
             &gSiPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gPeiITbtConfigGuid, (VOID *) &PeiITbtConfig);
  ASSERT_EFI_ERROR (Status);

  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob == NULL) {
    DEBUG ((DEBUG_ERROR, "TcssHob is NULL\n"));
    if (!IsSimicsEnvironment ()) {
      ASSERT(FALSE);
    }
    return EFI_UNSUPPORTED;
  }

  for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
    if (TcssHob->TcssData.ItbtPcieRpEn[Index] == 1) {
      ITbtExisted = TRUE;
      break;
    }
  }

  for (Index = 0; Index < MAX_HOST_ITBT_DMA_NUMBER; Index++) {
    if (TcssHob->TcssData.ItbtDmaEn[Index] == 1) {
      ITbtExisted = TRUE;
      break;
    }
  }

  ///
  /// Save TBT FW state/version.
  /// Assumption - All TBT DMA controllers have the same FW loading status.
  ///
  TbtImr.RegValue = TcssSbIomRead (R_IOM_CSME_IMR_TBT_STATUS);
  DEBUG ((DEBUG_INFO, "R_IOM_CSME_IMR_TBT_STATUS: 0x%08X\n", TbtImr.RegValue));
  DEBUG ((DEBUG_INFO, "TBT FW version: 0x%04X\n", (UINT16) TbtImr.Bits.TbtFwVersion));

  //
  // Update information to hob
  //
  if (TbtImr.Bits.Done && TbtImr.Bits.Valid) {
    TcssHob->TcssData.TbtImrStatus.RegValue = TbtImr.RegValue;
    DEBUG ((DEBUG_INFO, "TBT FW IMR info is updated to TCSS HOB, value is %x\n", TcssHob->TcssData.TbtImrStatus.RegValue));
  } else {
    DEBUG ((DEBUG_ERROR, "TBT FW has not yet downloaded to IMR\n"));
    if ((!IsSimicsEnvironment ()) && (ITbtExisted == TRUE)) {
      ASSERT(FALSE);
    }
    //
    // Disable TBT IPs of the TCSS DEVEN register when TBT FW is not downloaded to IMR
    //
    MmioAnd32 (MchBar + R_SA_MCHBAR_TCSS_DEVEN_OFFSET,
      (UINT32) ~(B_SA_MCHBAR_TCSS_DEVEN_PCIE0_MASK |
      B_SA_MCHBAR_TCSS_DEVEN_PCIE1_MASK |
      B_SA_MCHBAR_TCSS_DEVEN_PCIE2_MASK |
      B_SA_MCHBAR_TCSS_DEVEN_PCIE3_MASK |
      B_SA_MCHBAR_TCSS_DEVEN_DMA0_MASK |
      B_SA_MCHBAR_TCSS_DEVEN_DMA1_MASK)
    );
    return EFI_UNSUPPORTED;
  }

  if (ITbtExisted == TRUE) {
    //
    // Dispatch ITBT task table
    //
    TbtTaskDistpach (ITbtCallTable, (VOID *) PeiITbtConfig);
  }

  //
  // the TcssSsidHob only use PEI phase, so claer value before entry DXE phase.
  //
  TcssSsidHob = NULL;
  TcssSsidHob = (TCSS_DATA_SSID_HOB *) GetFirstGuidHob (&gTcssSsidHobGuid);
  if (TcssSsidHob != NULL) {
    TcssSsidHob->TcssSsidTable = NULL;
    TcssSsidHob->NumberOfTcssSsidTable = 0;
  }

  return EFI_SUCCESS;
}

/**
  This function performs set ITBT function disable register status to HOB

  @param[in] TcssDeven          ITBT function disable register value
**/
VOID
UpdateTcssDevenToHob (
  IN UINT32                       TcssDeven
  )
{
  TCSS_DATA_HOB                   *TcssHob;
  UINT32                          Data;
  UINT32                          Index;

  ///
  /// Update TCSS Device Enable status to TCSS HOB
  ///
  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob == NULL) {
    ASSERT (FALSE);
    return;
  }

  //
  // iTBT PCIE
  //
  for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
    TcssHob->TcssData.ItbtPcieRpEn[Index] = (UINT8)((TcssDeven >> Index) & BIT0);
  }

  //
  // USB
  //
  TcssHob->TcssData.ItbtXhciEn = (TcssDeven & B_SA_MCHBAR_TCSS_DEVEN_XHCI_MASK) ? 1 : 0;
  TcssHob->TcssData.ItbtXdciEn = (TcssDeven & B_SA_MCHBAR_TCSS_DEVEN_XDCI_MASK) ? 1 : 0;

  //
  // TBT DMA
  //
  Data = (TcssDeven & (B_SA_MCHBAR_TCSS_DEVEN_DMA0_MASK | B_SA_MCHBAR_TCSS_DEVEN_DMA1_MASK |\
            B_SA_MCHBAR_TCSS_DEVEN_DMA2_MASK)) >> N_SA_MCHBAR_TCSS_DEVEN_DMA0_OFFSET;
  for (Index = 0; Index < MAX_HOST_ITBT_DMA_NUMBER; Index++) {
    TcssHob->TcssData.ItbtDmaEn[Index] = (UINT8)((Data >> Index) & BIT0);
  }

  DEBUG ((DEBUG_INFO, "[TCSS] Updated TCSS_DEVEN to TCSS HOB\n"));
  return;
}

/**
  TcssDevEnInitPreMem - Initializes the register TCSS_DEVEN_0_0_0_MCHBAR_IMPH

  Before DID, BIOS performs TCSS DEVEN disabling/enabling -
   (1).BIOS fills TCSS DEVEN value to TCSS_DEVEN_0_0_0_MCHBAR_IMPH based on BIOS policy accordingly,
       BIOS can only disables/enables supported TCSS DEV IP by SKU/Fuse accordingly.
   (2) BIOS reads TCSS DEVEN register back into local stack,
       the value reflects the exact TCSS DEV IP which can be enabled currently based on fuse info and BIOS policy.
       (Failsafe to ensure IOM can get the exactly DEV map used by Punit)
   (3).BIOS writes the TCSS DEVEN to IOM BIOS mailbox
   (4).BIOS guarantees the value write to TCSS DEVEN is as same as the one sent to IOM WB inside SoC BIOS reference code.

  @param[in]                      Pointer to TCSS pre-mem configuration policy
**/
VOID
TcssDevEnInitPreMem (
  IN  TCSS_PEI_PREMEM_CONFIG      *TcssPeiPreMemConfig
  )
{
  UINT32                          Cmd;
  UINT32                          TcssDeven;
  UINT32                          MchBar;

  DEBUG ((DEBUG_INFO, "[TCSS] TcssDevEnInitPreMem - Start\n"));

  ///
  /// 1.BIOS fills TCSS DEVEN value to IOP based on BIOS policy accordingly,
  ///   BIOS can only disables/enables supported TCSS DEV IP by SKU/Fuse accordingly.
  ///
  MchBar = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MCHBAR)) & ~BIT0;
  MmioWrite32(MchBar + R_SA_MCHBAR_TCSS_DEVEN_OFFSET, TcssPeiPreMemConfig->DevEnConfig.TcssDevEn);

  ///
  /// 2.BIOS reads TCSS DEVEN register back into local stack,
  ///   the value reflects the exact TCSS DEV IP which can be enabled currently based on fuse info and BIOS policy.
  ///   (Failsafe to ensure IOM can get the exactly DEV map used by Punit)
  ///
  TcssDeven = MmioRead32 (MchBar + R_SA_MCHBAR_TCSS_DEVEN_OFFSET);

  DEBUG ((DEBUG_INFO, "TCSS DEVEN = 0x%08X\n", TcssDeven));

  ///
  /// 3.BIOS writes the TCSS DEVEN data to IOM-BIOS mailbox
  ///

  //
  // BIOS updates TCSS DEVEN in IOP and read the register back to understand what the end result of its configuration.
  //
  TcssSbIomWrite (R_IOM_BIOS_MAIL_BOX_DATA, TcssDeven | B_IOM_BIOS_MAIL_BOX_IOM_EN);
  DEBUG ((DEBUG_INFO, "IOM mailbox data = 0x%08X\n", TcssDeven));

  //
  // BIOS writes the UPDATE_DEVEN command to IOM-BIOS MAILBOX command register
  //
  Cmd = V_IOM_BIOS_UPDATE_DEVEN_CMD;
  TcssSbIomWrite (R_IOM_BIOS_MAIL_BOX_CMD, Cmd);
  DEBUG ((DEBUG_INFO, "IOM mailbox command = 0x%08X\n", Cmd));

  //
  // Set the Run/Busy bit to signal mailbox data is ready to process. IOM FW is not yet
  // out of reset at this point, so don't wait for response.
  //
  Cmd |= B_IOM_BIOS_SET_TO_RUN;
  TcssSbIomWrite (R_IOM_BIOS_MAIL_BOX_CMD, Cmd);

  //
  // Update TcssDeven status via HOB
  //
  UpdateTcssDevenToHob (TcssDeven);

  DEBUG ((DEBUG_INFO, "[TCSS] TcssDevEnInitPreMem - End\n"));
}

/**
  This function program the PCIEFUNCMAP registers for assigning Function number to TBT PCIe port
**/
VOID
ConfigureTbtPcieSegFuncMap (
  IN  TCSS_PEI_PREMEM_CONFIG      *TcssPeiPreMemConfig
  )
{
  UINT64                          McD0BaseAddress;
  UINT8                           Index;
  INTN                            FirstEnabledItbtPortNumber;
  UINT32                          TcssDevEn;
  UINT32                          MchBar;
  UINT8                           ItbtPcieFunctionNumber[MAX_ITBT_PCIE_PORT];
  UINT32                          PcieFuncMap;
  TCSS_DATA_HOB                   *TcssHob;
  UINT32                          Data32And;

  DEBUG ((DEBUG_INFO, "[TCSS] ConfigureTbtPcieSegFuncMap() - Start\n"));
  PcieFuncMap = 0;
  FirstEnabledItbtPortNumber = 0;
  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
  MchBar = PciSegmentRead32 (McD0BaseAddress + R_SA_MCHBAR) & ~BIT0;
  TcssDevEn = MmioRead32(MchBar + R_SA_MCHBAR_TCSS_DEVEN_OFFSET);

  //
  // Get the first enabled iTBT PCIe port number
  //
  FirstEnabledItbtPortNumber = LowBitSet32 (TcssDevEn & 0xFF);

  ///
  /// According to each iTBT PCIe RP enable status to update Function Map.
  ///
  for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
    ItbtPcieFunctionNumber[Index] = Index;
    //
    // Swap the function number if the first enabled iTBT PCIe RP is not port0.
    //
    if ((Index != 0) && (FirstEnabledItbtPortNumber == Index)) {
      ItbtPcieFunctionNumber[Index] = 0;
      ItbtPcieFunctionNumber[0] = Index;
    }
  }
  for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
    ///
    /// Program RP0FN for each iTBT PCIE Root Port if it is enabled then lock the register.
    /// Since currently we only defined PortId for PCIe0 ~ PCIe3, so we only program it for these 4 ports.
    ///
    TcssSbPcieWrite32 (Index, R_TCSS_PCIEX_PCD, ItbtPcieFunctionNumber[Index]);
  }

  //
  // iTBT PCIE RP Segment Number Assignment. Hardcoding it to Segment 0x0
  //
#ifdef R_SA_PCIESEGMAP
  //
  // Note: Assign Segment number for the Program only if "R_SA_PCIESEGMAP" register
  // MARCO is defined for the Program. Otherwise don't Update it.
  //
  Data32And = (UINT32)~(B_SA_PCIESEGMAP_PCIE0SEG_MASK | B_SA_PCIESEGMAP_PCIE1SEG_MASK |
                        B_SA_PCIESEGMAP_PCIE2SEG_MASK | B_SA_PCIESEGMAP_PCIE3SEG_MASK);

  PciSegmentAndThenOr32 (McD0BaseAddress + R_SA_PCIESEGMAP, Data32And, 0x00);
#endif

  //
  // iTBT PCIE RP Function Number Assignment
  //
  PcieFuncMap = (ItbtPcieFunctionNumber[0] << N_SA_PCIEFUNCMAP_PCIE0FUNC_OFFSET |
                 ItbtPcieFunctionNumber[1] << N_SA_PCIEFUNCMAP_PCIE1FUNC_OFFSET |
                 ItbtPcieFunctionNumber[2] << N_SA_PCIEFUNCMAP_PCIE2FUNC_OFFSET |
                 ItbtPcieFunctionNumber[3] << N_SA_PCIEFUNCMAP_PCIE3FUNC_OFFSET);

  Data32And = (UINT32)~(B_SA_PCIEFUNCMAP_PCIE0FUNC_MASK | B_SA_PCIEFUNCMAP_PCIE1FUNC_MASK |
                        B_SA_PCIEFUNCMAP_PCIE2FUNC_MASK | B_SA_PCIEFUNCMAP_PCIE3FUNC_MASK);

  PciSegmentAndThenOr32 (McD0BaseAddress + R_SA_PCIEFUNCMAP, Data32And, PcieFuncMap);

  ///
  /// Update SA HOB's TCSS data TBT variables for function
  ///
  TcssHob = NULL;
  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);

  if (TcssHob != NULL) {
    TcssHob->TcssData.ItbtPcieFuncMap = PcieFuncMap;
  }

  DEBUG ((DEBUG_INFO, "[TCSS] ConfigureTbtPcieSegFuncMap() - End\n"));
}

/**
  This function initializes Aux and HSL Orientation registers in IOM.

  @param[in] IomConfig            Pointer to TCSS_IOM_PEI_CONFIG struct containing IOM configuration.
**/
VOID
TcssInitAuxHslOri (
  IN TCSS_IOM_PEI_CONFIG          *IomConfig
  )
{
  UINT32                          AuxOriReg;
  UINT32                          HslOriReg;

  ///
  /// Set IOM Aux Orientation per configuration.
  ///
  DEBUG ((DEBUG_INFO, "[TCSS] TcssInitAuxHslOri() - Start\n"));

  AuxOriReg = TcssSbIomRead (R_IOM_AUX_ORI_OFFSET);
  DEBUG ((DEBUG_INFO, "Origional AuxOriReg: 0x%08X\n", AuxOriReg));
  DEBUG ((DEBUG_INFO, "IomConfig->IomOverrides.AuxOri: 0x%08X\n", (UINT32)IomConfig->IomOverrides.AuxOri));
  TcssSbIomWrite (R_IOM_AUX_ORI_OFFSET,  (UINT32)IomConfig->IomOverrides.AuxOri);
  AuxOriReg = TcssSbIomRead (R_IOM_AUX_ORI_OFFSET);
  TcssSbIomWrite (R_IOM_AUX_ORI_OFFSET, (UINT32)AuxOriReg | B_IOM_AUX_ORI_LOCK);

  DEBUG ((DEBUG_INFO, "Update AuxOri: 0x%08X\n", TcssSbIomRead (R_IOM_AUX_ORI_OFFSET)));

  ///
  /// Set IOM HSL Orientation per configuration.
  ///
  HslOriReg = TcssSbIomRead (R_IOM_HSL_ORI_OFFSET);
  DEBUG ((DEBUG_INFO, "Origional HslOriReg: 0x%08X\n", HslOriReg));
  DEBUG ((DEBUG_INFO, "IomConfig->IomOverrides.HslOri: 0x%08X\n", (UINT32)IomConfig->IomOverrides.HslOri));
  TcssSbIomWrite (R_IOM_HSL_ORI_OFFSET, (UINT32) IomConfig->IomOverrides.HslOri);
  HslOriReg = TcssSbIomRead (R_IOM_HSL_ORI_OFFSET);
  TcssSbIomWrite (R_IOM_HSL_ORI_OFFSET, (UINT32) HslOriReg | B_IOM_AUX_ORI_LOCK);
  DEBUG ((DEBUG_INFO, "Update HslOri: 0x%08X\n", TcssSbIomRead (R_IOM_HSL_ORI_OFFSET)));

  DEBUG ((DEBUG_INFO, "[TCSS] TcssInitAuxHslOri() - End\n"));
}

/**
  IomReadyCheck - This function performs IOM ready state

  This function does the following steps:
  (1) Examines IOM_READY (Register IOM_TYPEC_STATUS_1, bit 30)
  (2) If 0, retry in IOM_READY_TIMEOUT ms (1ms timeout)
  (3) Save success/failure in Tcss HOB
  (4) Print success/failure
**/
EFI_STATUS
EFIAPI
IomReadyCheck (
  VOID
  )
{
  BOOLEAN                         IomReady;
  TCSS_DATA_HOB                   *TcssHob;

  TcssHob = NULL;
  DEBUG ((DEBUG_INFO, "[TCSS] IomReadyCheck - Start\n"));

  IomReady = ((TcssSbIomRead (R_IOM_TYPEC_STATUS_1)) & B_IOM_IOM_READY) ? TRUE: FALSE;

  if (!IomReady) {
    //
    // Try again after 1ms if failed
    //
    DEBUG ((DEBUG_INFO, "IOM is not ready. Trying again ..\n"));
    MicroSecondDelay (1000);
    IomReady = ((TcssSbIomRead (R_IOM_TYPEC_STATUS_1)) & B_IOM_IOM_READY) ? TRUE: FALSE;
  }

  DEBUG ((DEBUG_INFO, "IOMReady bit = %d\n", (int)IomReady));

  ///
  /// Locate HOB for TCSS Data and save IOM ready state/version
  ///
  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob != NULL) {
    TcssHob->TcssData.IOMReady = IomReady;
    if (IomReady) {   ///< Pull the version if IOM is detected and save
      TcssHob->TcssData.IomFwVersion = TcssSbIomRead (R_IOM_FW_INFO);
      TcssHob->TcssData.IomFwEngrVersion = TcssSbIomRead (R_IOM_FW_INFO_ENGR);
      DEBUG ((DEBUG_INFO, "IomReady bit updated to TCSS HOB\n"));
      DEBUG ((DEBUG_INFO, "\tIOM version: 0x%08X; Engr version: 0x%08X\n", TcssHob->TcssData.IomFwVersion, TcssHob->TcssData.IomFwEngrVersion));
    }
    DEBUG ((DEBUG_INFO, "\tTCSS HOB updated\n"));
  }

  if (!IomReady){
    DEBUG ((DEBUG_INFO, "\tIomReadyCheck Failed!\n"));
    return EFI_UNSUPPORTED;
  }
  DEBUG ((DEBUG_INFO, "[TCSS] IomReadyCheck - End\n"));
  return EFI_SUCCESS;

}

/**
  This function is IOM send mail box command data.

  @param[in]  UINT32                 Command
  @param[in]  UINT32                 MailBoxTimeOut
  @param[in]  UINT8                  MailBoxWaitStall
  @param[in]  UINT32                 Data

  @retval     EFI_SUCCESS            The function completes successfully.
  @retval     EFI_TIMEOUT            Command timeout.
  @retval     EFI_UNSUPPORTED        Unrecognized Mailbox Type.
**/
EFI_STATUS
EFIAPI
IomSendMailBox (
  IN UINT32 Command,
  IN UINT32 MailBoxTimeOut,
  IN UINT8  MailBoxWaitStall,
  IN UINT32 Data
  )
{
  UINT32   CmdCount;
  //
  // BIOS Updates data via IOM MAILBOX data register.
  //
  TcssSbIomWrite (R_IOM_BIOS_MAIL_BOX_DATA, Data);
  DEBUG ((DEBUG_INFO, "IOM mailbox data = 0x%08X\n", Data));

  //
  // BIOS writes command to IOM MAILBOX command register.
  //
  TcssSbIomWrite (R_IOM_BIOS_MAIL_BOX_CMD, Command);
  DEBUG ((DEBUG_INFO, "IOM mailbox command = 0x%08X\n", Command));

  //
  // Set the Run/Busy bit to signal mailbox data is ready to process. IOM FW is not yet
  // out of reset at this point, so don't wait for response.
  //
  Command |= B_IOM_BIOS_SET_TO_RUN;
  TcssSbIomWrite (R_IOM_BIOS_MAIL_BOX_CMD, Command);

  //
  // Waiting IOM complete V_SA_IOM_BIOS_GEM_SB_TRAN_CMD.
  // Time out is MailBoxTimeOut * MailBoxWaitStall microsecond.
  //
  CmdCount = 0;
  while ((CmdCount < MailBoxTimeOut) && \
     ((TcssSbIomRead (R_IOM_BIOS_MAIL_BOX_CMD) & B_IOM_BIOS_SET_TO_RUN) != 0)) {
    CmdCount++;
    MicroSecondDelay (MailBoxWaitStall);
  }

  if (CmdCount >= MailBoxTimeOut) {
    DEBUG ((DEBUG_ERROR, "[IOM mailbox] Execute command timeout\n"));
    return EFI_TIMEOUT;
  }

  Command = TcssSbIomRead (R_IOM_BIOS_MAIL_BOX_CMD);
  if (Command != 0) {
    DEBUG ((DEBUG_ERROR, "[IOM mailbox] response fail error code 0x%x\n", Command));
    return EFI_UNSUPPORTED;
  }
  return EFI_SUCCESS;
}

/**
  SaXdciIrq - This function program SA Xdci Irq

  @param[in] InterruptPin - SA XDCI Interrup Pin Number
  @param[in] IrqNumber - SA XDCI IRQ number
**/
VOID
SaXdciIrq (
  IN UINT8 InterruptPin,
  IN UINT8 IrqNumber
  )
{
  EFI_STATUS       Status;
  TCSS_DATA_HOB    *TcssHob;
  UINT32           SaIomMailBoxData;
  UINT32           Cmd;

  TcssHob = NULL;
  DEBUG ((DEBUG_INFO, "[TCSS] SaXdciIrq - Start\n"));

  //
  // Set Interrupt Pin and IRQ number
  //
  SaIomMailBoxData = (UINT32) ((InterruptPin << N_OTG_PCR_PCICFGCTRL_INT_PIN) |
                        (IrqNumber << N_OTG_PCR_PCICFGCTRL_PCI_IRQ));

  //
  // BIOS updates SA XDCI IRQ setting via IOM mail box.
  //
  Cmd = V_IOM_BIOS_GEM_SB_TRAN_CMD | (V_IOM_BIOS_CMD_TYPE_XDCI_INT << 8);

  Status = IomSendMailBox (Cmd,
                           IOM_BIOS_MAILBOX_TYPE_XDCI_INT_READ_TIMEOUT,
                           IOM_BIOS_MAILBOX_TYPE_XDCI_INT_WAIT_STALL,
                           SaIomMailBoxData
                           );

  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob != NULL) {
    TcssHob->TcssData.TcssxDCIInt = InterruptPin;
    TcssHob->TcssData.TcssxDCIIrq = IrqNumber;
    DEBUG ((DEBUG_INFO, "SA xDCI Int pin: 0x%02X\n", TcssHob->TcssData.TcssxDCIInt));
    DEBUG ((DEBUG_INFO, "SA xDCI Irq: 0x%02X\n", TcssHob->TcssData.TcssxDCIIrq));
  }
  DEBUG ((DEBUG_INFO, "[TCSS] SaXdciIrq - End - Status \n", Status));
}

/**
  This function is set Tc cold power saving factor.

  @param[in]  VOID                   PeiTbtConfig

  @retval     EFI_SUCCESS            The function completes successfully
  @retval     EFI_UNSUPPORTED        SiPolicyPpi is unsupported.
**/
VOID
IomSetTcColdPowerSavingFactor (
  IN  TCSS_PEI_CONFIG        *TcssPeiConfig
  )
{

  EFI_STATUS             Status;
  UINT32                 IomPowerSavingFactorData;

  DEBUG ((DEBUG_INFO, "[TCSS] IomSetTcColdPowerSavingFactor - Start\n"));

  IomPowerSavingFactorData = 0;
  IomPowerSavingFactorData |= (UINT32) TcssPeiConfig->MiscConfig.IomStayInTCColdSeconds;
  IomPowerSavingFactorData |= (UINT32) (TcssPeiConfig->MiscConfig.IomBeforeEnteringTCColdSeconds << 8);
  ///
  /// BIOS writes the IOM Power saving factor data to IOM-BIOS mailbox
  /// Also writes the IOM Power saving factor command to IOM-BIOS MAILBOX command register
  ///
  Status = IomSendMailBox (V_IOM_BIOS_POWER_SAVING_FACTOR_CMD,
                           IOM_BIOS_MAILBOX_READ_TIMEOUT,
                           IOM_BIOS_MAILBOX_WAIT_STALL,
                           IomPowerSavingFactorData
                           );

  DEBUG ((DEBUG_INFO, "IomSetTcColdPowerSavingFactor - End - Status %r\n", Status));
}

/**
  This function configs IOM_TYPEC_SW_CONFIGURATION_1 register crid bit
**/
VOID
TcssCridEnable (
  VOID
  )
{
  UINT32           IomTypecConfigValue;
  UINT16           IomTypecSwConfig1Reg;
  UINT32           Status;

  DEBUG ((DEBUG_INFO, "[TCSS] TcssCridEnable() - Start\n"));

  IomTypecSwConfig1Reg = (UINT16)R_IOM_TYPEC_SW_CONFIGURATION_1;


  //
  // Read IOM_TYPEC_SW_CONFIGURATION_1
  // Set BIT 18 if Crid enable
  //
  IomTypecConfigValue =  TcssSbIomRead (IomTypecSwConfig1Reg);
  IomTypecConfigValue |= B_IOM_TYPEC_SW_CONFIGURATION_1_CRID_EN;

  //
  // Write to IOM_TYPEC_SW_CONFIGURATION_1
  //
  Status = TcssSbIomWrite (IomTypecSwConfig1Reg, IomTypecConfigValue);
  if (Status == INVALID_DATA_32) {
    DEBUG ((DEBUG_INFO, "TcssSbIomWrite Fail\n"));
  } else {
    DEBUG ((DEBUG_INFO, "TCSS CRID is enable\n"));
  }

  DEBUG ((DEBUG_INFO, "[TCSS] TcssCridEnable() - End\n"));
}

/**
  TcssRtd3InfoUpdate - This function update TCSS RTD3 information into Hob

  @param[in] TcssConfig           Pointer to config block
**/
VOID
TcssRtd3InfoUpdate (
  IN  TCSS_PEI_CONFIG             *TcssPeiConfig
  )
{
  TCSS_DATA_HOB      *TcssHob;

  TcssHob = NULL;
  DEBUG ((DEBUG_INFO, "[TCSS] TcssRtd3Update - Start\n"));

  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob != NULL) {
    if (TcssPeiConfig->IomConfig.IomInterface.D3ColdEnable == 1) {
      TcssHob->TcssData.TcssRtd3 = TRUE;
    }
    DEBUG ((DEBUG_INFO, "TCSS RTD3: 0x%X\n", TcssHob->TcssData.TcssRtd3));
  }
  DEBUG ((DEBUG_INFO, "[TCSS] TcssRtd3Update - End\n"));
}

/**
  TcssIomVccStInfoUpdate - This function update TCSS IOM VccSt information into Hob

  @param[in] TcssConfig           Pointer to config block
**/
VOID
TcssIomVccStInfoUpdate (
  IN  TCSS_PEI_CONFIG             *TcssPeiConfig
  )
{
  TCSS_DATA_HOB      *TcssHob;

  TcssHob = NULL;
  DEBUG ((DEBUG_INFO, "[TCSS] TcssIomVccStInfoUpdate - Start\n"));

  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob != NULL) {
    TcssHob->TcssData.TcssIomVccSt = (UINT8) TcssPeiConfig->IomConfig.IomInterface.VccSt;
    DEBUG ((DEBUG_INFO, "TCSS IOM VccSt: 0x%X\n", TcssHob->TcssData.TcssIomVccSt));
  }
  DEBUG ((DEBUG_INFO, "[TCSS] TcssIomVccStInfoUpdate - End\n"));
}

/**
  This function performs basic initialization for TCSS in PEI phase after Policy produced at Pre-Mem phase.

  @param[in]  TcssConfig              Pointer to TCSS config block
  @param[in]  MiscSaPeiPreMemConfig   Pointer to SA miscellaneous config block
**/
VOID
EFIAPI
TcssPreMemInit (
  IN TCSS_PEI_PREMEM_CONFIG      *TcssPeiPreMemConfig,
  IN SA_MISC_PEI_PREMEM_CONFIG   *MiscSaPeiPreMemConfig
  )
{
  DEBUG ((DEBUG_INFO, "[TCSS] TcssPreMemInit() - Start\n"));

  //
  // Check Integrated silicon is supported.
  //
  if (IsTcssSupported() == FALSE) {
    DEBUG ((DEBUG_INFO, "[TCSS] TcssPreMemInit() - Unsupported - End\n"));
    return;
  }

  //
  // Performing Tcss PreMem initialization from here.
  //

  //
  // Install TCSS Data Hob
  //
  DEBUG ((DEBUG_INFO, "Install TCSS DATA HOB\n"));
  InstallTcssDataHob ();

  //
  // Install TCSS SSID data hob
  //
  DEBUG ((DEBUG_INFO, "Install TCSS SSID DATA HOB\n"));
  InstallTcssSsidDataHob ();

  ///
  /// Configure hardware register TCSS_DEVEN_0_0_0_MCHBAR_IMPH
  ///
  TcssDevEnInitPreMem (TcssPeiPreMemConfig);

  //
  // Enable Crid if it's needed
  //
  if (MiscSaPeiPreMemConfig->CridEnable == TRUE) {
    TcssCridEnable ();
  }

  DEBUG ((DEBUG_INFO, "[TCSS] TcssPreMemInit() - End\n"));
}

/**
  Limit TCSS C-State option.
  BIOS will only update this one-time during boot exit (Cold/warm/Sx).

  This function does the following steps:
  (1) BIOS will send desired C-State value (legal values are 0,1,2,4,5,6,7,10) via
      IOM_BIOS_MAILBOX_DATA register and set the RUN_BUSY bit.
  (2) IOMFW will consume this BIOS mailbox request,
      and update the IOM_BIOS_MAILBOX_DATA register with resolved deepest TCx
      state that can be supported.
  (3) IOMFW will clear the RUN_BUSY bit after step 2.


  @param[in] IomConfig            Pointer to TCSS_IOM_PEI_CONFIG struct containing IOM configuration.
  @retval Status                  Deepst TC C-State limit number
                                  value 0xFF means there's something wrong

**/
UINT8
TcssCstatelimit (
  IN  TCSS_IOM_PEI_CONFIG     *IomConfigPtr
  )
{
  EFI_STATUS        Status;
  UINT32            Cmd;
  UINT32            MailBoxData;
  UINT32            LegalCstateValue[] = {0, 1, 2, 4, 5, 6, 7, 10};
  BOOLEAN           LegalCstateFlag;
  UINT8             i;

  //
  // Initialization
  //
  LegalCstateFlag = FALSE;
  Cmd = 0;
  MailBoxData = 0xFF;

  DEBUG ((DEBUG_INFO, "[TCSS] TcssCstatelimit() - Start\n"));

  //
  // Check if users provide a valid C-State value
  //
  for (i = 0; i < (sizeof(LegalCstateValue)/ sizeof(UINT32)); i++) {
    if (IomConfigPtr->TcStateLimit == LegalCstateValue[i]) {
      LegalCstateFlag = TRUE;
      break;
    }
  }

  if (LegalCstateFlag == FALSE) {
    //Set to TC10 if users provide an invalid C-State value
    IomConfigPtr->TcStateLimit = 10;
    DEBUG ((DEBUG_ERROR, "Invalid C-State value, set TcStateLimit to TC10\n"));
  }

  //
  // BIOS updates C-State limit to IOM-BIOS MAILBOX data register.
  // Also writes the C-State limit command to IOM-BIOS MAILBOX command register.
  //
  Cmd = V_IOM_BIOS_CMD_TCSTATE_LIMIT | B_IOM_BIOS_SET_TO_RUN;

  Status = IomSendMailBox (Cmd,
                           IOM_BIOS_MAILBOX_READ_TIMEOUT,
                           IOM_BIOS_MAILBOX_WAIT_STALL,
                           IomConfigPtr->TcStateLimit
                           );

  MailBoxData = CpuRegbarRead32 (CPU_SB_PID_IOM, R_IOM_BIOS_MAIL_BOX_DATA);
  //
  //If IOM replys any value greater than 8, that means TC10
  //
  if (MailBoxData >= 8) {
    DEBUG ((DEBUG_INFO, "Override MailBoxData from %d to 10\n", MailBoxData));
    MailBoxData = 10;
  }
  DEBUG ((DEBUG_INFO, "[TCSS] System Supported TC State = 0x%08x\n", MailBoxData));
  DEBUG ((DEBUG_INFO, "[TCSS] TcssCstatelimit() - End - Status %r\n",Status));

  return (UINT8) MailBoxData;
}

/**
  Check if this chipset Enable TCSS

  @retval BOOLEAN  TRUE if IP enable, FALSE disable
**/
BOOLEAN
EFIAPI
IsItcssEnabled (
  VOID
  )
{
  TCSS_DATA_HOB                   *TcssHob;

  //
  // Check Integrated silicon is supported.
  //
  if (IsTcssSupported () == FALSE) {
    return FALSE;
  }

  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob == NULL) {
    return FALSE;
  }
  return TRUE;
}

/**
  Entrypoint to perform basic initialization for TCSS in PEI phase after Policy produced at Post-Mem phase.

  This function does the following steps:
  (1) Check IOM Ready bit.
  (2) If IOM is ready, then programs VCCST, REPLAY and CONNECTION_OVERRIDE setting.
  (3) If IOM is ready, then programs AUX/HSL orientation support per platform design
  (4) Do iTBT PCIE RP programming
  (5) Start iTBT specific BIOS support
  (6) Init TCSS USB controllers (xHCI and xDCI)
  (7) Handshake with PMC if PMC handshaking support is enabled

  @param[in] SiPolicy             The SI Policy PPI instance
  @param[in] TcssPeiPreMemConfig  Pointer to config block
  @param[in] TcssConfig           Pointer to config block
**/
VOID
EFIAPI
TcssInit (
  IN  SI_POLICY_PPI               *SiPolicy,
  IN  TCSS_PEI_PREMEM_CONFIG      *TcssPeiPreMemConfig,
  IN  TCSS_PEI_CONFIG             *TcssPeiConfig,
  IN  USB_HANDLE                  *UsbHandle
  )
{
  EFI_STATUS                      Status;
  UINT64                          McD0BaseAddress;
  UINT32                          MchBar;
  UINT32                          IomTypecConfigValue;
  UINT16                          IomTypecSwConfig1Reg;
  EFI_STATUS                      IomReadyStatus;
  EFI_STATUS                      PmcTcssInitStatus;
  UINT8                           PmcReplay;
  EFI_BOOT_MODE                   BootMode;
  TCSS_DATA_HOB                   *TcssHob;
  UINT16                          NumberOfTcssTable;
  TCSS_DATA_SSID_HOB              *TcssSsidHob;
  UINT8                           Index;

  //
  // Performing Tcss initialization from here.
  //
  DEBUG ((DEBUG_INFO, "[TCSS] TcssInit() - Start\n"));

  //
  // Check Integrated silicon is supported.
  //
  if (IsTcssSupported () == FALSE) {
    DEBUG ((DEBUG_INFO, "[TCSS] TcssInit() - UnSupported - return\n"));
    return ;
  }

  TcssHob = NULL;
  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);
  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
  MchBar = PciSegmentRead32 (McD0BaseAddress + R_SA_MCHBAR) & ~B_SA_MCHBAR_MCHBAREN_MASK;

  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob == NULL) {
    ASSERT (FALSE);
    return;
  }
  IomTypecSwConfig1Reg = (UINT16)R_IOM_TYPEC_SW_CONFIGURATION_1;

  ///
  /// BIOS <-> IOM Handshake
  ///

  ///
  /// Step 1 - Perform IOM/USBC ready check
  ///
  IomReadyStatus = IomReadyCheck ();
  if (!IsSimicsEnvironment ()) {
    ASSERT_EFI_ERROR (IomReadyStatus);
    if (EFI_ERROR (IomReadyStatus)) {
      //
      // Clear the TCSS DEVEN register when IOM is corrupted
      //
      DEBUG ((DEBUG_INFO, "[TCSS] Clearing the TCSS DEVEN register as IOM is corrupted\n"));
      MmioWrite32 (MchBar + R_SA_MCHBAR_TCSS_DEVEN_OFFSET, 0);
      return;
    }
  }
  if (TcssHob->TcssData.ItbtXhciEn == 0) {
    DEBUG ((DEBUG_INFO, "[TCSS] CPU XHCI is disabled, skip whole TCSS initialization\n"));
    //
    // Request TC cold for TCSS disabling case
    //
    MmioOr32 (MchBar + R_SA_MCHBAR_PRIMDN_MASK1_IMPH, B_SA_MCHBAR_PRIMDN_MASK1_IMPH_TCSS_IN_D3);
    TcssSbIomOr (IomTypecSwConfig1Reg, B_IOM_TYPEC_SW_CONFIGURATION_1_D3COLD_REQ);
    return;
  }

  ///
  /// Step 2 - If IOM is ready, then programs VCCST, REPLAY and CONNECTION_OVERRIDE setting
  ///

  if (IomReadyStatus == EFI_SUCCESS) {
    IomTypecSwConfig1Reg = (UINT16)R_IOM_TYPEC_SW_CONFIGURATION_1;

    //
    // Read IOM_TYPEC_SW_CONFIGURATION_1
    //
    IomTypecConfigValue = TcssSbIomRead (IomTypecSwConfig1Reg);

    //
    // CLEAR BITS of IOM_TYPEC_SW_CONFIGURATION_1
    //
    DEBUG ((DEBUG_INFO, "[TCSS] Clearing all IomTypecConfigValue as default initialization \n"));
    IomTypecConfigValue &= ~(B_IOM_UNWAKEBLE_SX | BIOS_EC_REPLAY_CONNECTION_S4S5 | BIOS_EC_REPLAY_CONNECTION_S3 | B_IOM_D3_HOT_ENABLE | B_IOM_D3_COLD_ENABLE | B_IOM_USB_CONNECT_OVERRIDE);

    if (TcssPeiConfig->IomConfig.IomInterface.VccSt == 0) {
      //
      // SET BIT 13 of IOM_TYPEC_SW_CONFIGURATION_1
      //
      DEBUG ((DEBUG_INFO, "[TCSS] Iom VccSt == 0, So enabling IOM UnWakebleSx and BIOS EC REPLY Connection S4 for IomTypecConfigValue\n"));
      IomTypecConfigValue |= (B_IOM_UNWAKEBLE_SX | BIOS_EC_REPLAY_CONNECTION_S4S5);
    }

    //
    // Enable D3 Hot based on policy.
    //
    if (TcssPeiConfig->IomConfig.IomInterface.D3HotEnable == 1) {
      //
      // SET BIT 8 of IOM_TYPEC_SW_CONFIGURATION_1
      //
      DEBUG ((DEBUG_INFO, "[TCSS] IOM D3Hot == 1, So enabling IOM D3Hot for IomTypecConfigValue\n"));
      IomTypecConfigValue |= B_IOM_D3_HOT_ENABLE;
    }

    //
    // Enable D3 Cold based on policy.
    //
    if (TcssPeiConfig->IomConfig.IomInterface.D3ColdEnable == 1) {
      //
      // SET BIT 9 of IOM_TYPEC_SW_CONFIGURATION_1
      //
      DEBUG ((DEBUG_INFO, "[TCSS] IOM D3Cold == 1, So enabling IOM D3Cold for IomTypecConfigValue\n"));
      IomTypecConfigValue |= B_IOM_D3_COLD_ENABLE;
    }

    TcssRtd3InfoUpdate (TcssPeiConfig);

    //
    // Set USB Override based on policy.
    //
    if (TcssPeiConfig->IomConfig.IomInterface.UsbOverride == 1) {
      //
      // SET BIT 13 of IOM_TYPEC_SW_CONFIGURATION_1
      //
      DEBUG ((DEBUG_INFO, "[TCSS] IOM UsbOverride == 1, So enabling IOM Usb Connect Override for IomTypecConfigValue\n"));
      IomTypecConfigValue |= B_IOM_USB_CONNECT_OVERRIDE;
    }

    IomTypecConfigValue = TcssIomConfig1RegOverride (IomTypecConfigValue);
    //
    // Write to IOM_TYPEC_SW_CONFIGURATION_1
    //
    TcssSbIomWrite (IomTypecSwConfig1Reg,  IomTypecConfigValue);
    //
    // Set Tcss Cstate limit
    //
    TcssHob->TcssData.DeepstTcState = TcssCstatelimit (&(TcssPeiConfig->IomConfig));

    //
    // Configure SA xDCI Interrupt
    //
    SaXdciIrq (TcssPeiConfig->MiscConfig.SaXdci.IntPing, TcssPeiConfig->MiscConfig.SaXdci.Irq);

    //
    // Configure Iom Set Tc Cold Power Saving Factor
    //
    if ((BootMode != BOOT_ON_S3_RESUME) && (BootMode != BOOT_ON_S4_RESUME)) {
      if ((TcssPeiConfig->MiscConfig.IomStayInTCColdSeconds == 0) && (TcssPeiConfig->MiscConfig.IomBeforeEnteringTCColdSeconds == 0)) {
        DEBUG ((DEBUG_ERROR, "Bypass IomSetTcColdPowerSavingFactor.\n"));
      } else {
        IomSetTcColdPowerSavingFactor (TcssPeiConfig);
      }
    }
  }


  ///
  /// Step3 - Configuring AUX orientation to IOM
  ///
  GpioIomAuxOriSetting (&TcssPeiConfig->IomConfig);
  TcssInitAuxHslOri (&TcssPeiConfig->IomConfig);

  ///
  /// Step 4 & 5- Configuring TBT PCIe Segment and Function number
  ///
  ConfigureTbtPcieSegFuncMap (TcssPeiPreMemConfig);

  ///
  /// Initiazlize TBT PCI Root Ports
  ///
  ItbtInitRootPorts(TcssPeiConfig);

  ///
  /// Step 6 - Init TCSS USB controllers (xHCI and xDCI)
  ///
  //
  // Configure Tcss xHCI
  //
  XhciConfigure (UsbHandle);

  //
  // Configure Tcss xDCI
  //
  XdciConfigure (UsbHandle);

  ///
  /// Step 7 - Handshake with PMC if PMC handshaking support is enabled
  ///          BIOS sends TCSS_BIOS_INIT Done (aka.USBC connection replay)
  ///          message along with USBC connctions replay setting to PMC.
  ///          All TCSS initialization is expected to be done before PMC
  ///          do any port negotiation. The indicator sent from BIOS to
  ///          PMC should be treated as TCSS BIOS initialization done
  ///          semaphore.


  TcssSsidHob = NULL;
  TcssSsidHob = (TCSS_DATA_SSID_HOB *) GetFirstGuidHob (&gTcssSsidHobGuid);
  if (TcssSsidHob != NULL) {
    NumberOfTcssTable = sizeof (mTcssSsidTable) / sizeof (mTcssSsidTable[0]);
    SiTcssProgramSsid (SiPolicy, mTcssSsidTable, NumberOfTcssTable);
  }

  //
  // Set all necessary lock bits in xHCI controller
  //
  XhciLockConfiguration (UsbHandle);

  ///
  /// BIOS <-> PMC Handshake
  ///
  if (TcssPeiConfig->IomConfig.PmcInterface.PmcPdEnable) {
    PmcReplay = 1;
    if (BootMode == BOOT_ON_S3_RESUME) {
      PmcReplay = 0;
    }
    if ((BootMode == BOOT_ON_S4_RESUME) &&
      (TcssPeiConfig->IomConfig.IomInterface.VccSt != 0)) {
        PmcReplay = 0;
    }
    PmcReplay = TcssPmcReplayOverride(PmcReplay);
    PmcTcssInitStatus = PmcTcssBiosInitDone (IomReadyStatus, PmcReplay);
    DEBUG ((DEBUG_INFO, "[TCSS] PmcTcssInitStatus = %r\n", PmcTcssInitStatus));
  }

  for (Index = 0; Index < MAX_TCSS_USB3_PORTS; Index++) {
    if ((TcssPeiConfig->MiscConfig.EnableTcssCovTypeA[Index] == 0) ||
        (TcssPeiConfig->MiscConfig.MappingPchXhciUsbA[Index] == 0)) {
      continue;
    }

    //
    // tcss usb & pch xhci port number base is 1
    //
    PmcTcssCovTypeA ((Index + 1), TcssPeiConfig->MiscConfig.MappingPchXhciUsbA[Index]);
  }

  TcssIomVccStInfoUpdate (TcssPeiConfig);

  //
  // Performing TcssInitEndOfPei after EndOfPei PPI produced
  //
  PeiServicesNotifyPpi (&mEndOfPeiNotifyList);

  PeiServicesInstallPpi (&mTcssPeiInitDonePpi);
  DEBUG ((DEBUG_INFO, "[TCSS] TcssInit() - End\n"));
}