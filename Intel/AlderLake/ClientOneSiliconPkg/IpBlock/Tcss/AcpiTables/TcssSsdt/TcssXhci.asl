/** @file
  This file contains the TCSS XHCI ASL code.
  It defines a Power Management for TCSS XHCI

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
  Method (BASE, 0) {
    Store (And(_ADR, 0x7), Local0) // Function Number
    Store (And(ShiftRight(_ADR, 16), 0x1F), Local1) // Device Number
    Add (ShiftLeft(Local0, 12), ShiftLeft(Local1, 15), Local2)
    Add (\_SB.PC00.GPCB(), Local2, Local3)
    ADBG(Concatenate("CPU XHCI PCIe MMIO Address", ToHexString(Local3)))
    Return (Local3)
  }

  Method(_PS0,0,Serialized)
  {
    ADBG("CPU XHCI _PS0 Start")
    If (LEqual(\_SB.PC00.TXHC.PMEE, 1)) {
      ADBG("Clear PME_EN of CPU XHCI")
      Store(0, \_SB.PC00.TXHC.PMEE)
    }
    ADBG("CPU XHCI _PS0 End")
  }

  Method(_PS3,0,Serialized)
  {
    ADBG("CPU XHCI _PS3 Start")
    If (LEqual(\_SB.PC00.TXHC.PMEE, 0)) {
      ADBG("PME_EN was not set. Set PME_EN of CPU XHCI")
      Store(1, \_SB.PC00.TXHC.PMEE)
    }
    ADBG("CPU XHCI _PS3 End")
  }

  //
  // The deepest D-state supported by this device in the S0 system sleeping state where the device can wake itself,
  // "4" represents D3cold.
  //
  Method(_S0W, 0x0, NotSerialized)
  {
    If (TRTD) {
      Return(0x4)
    } Else {
      Return(0x3)
    }
  }
  //
  // Variable to skip TCSS/TBT D3 cold; 1+: Skip D3CE, 0 - Enable D3CE
  // TCSS D3 Cold and TBT RTD3 is only available when system power state is in S0.
  //
  Name(SD3C, 0)
  //
  // Power Management routine for D3
  //
  If (TRTD) {
    // To evaluates to a list of power resources that are dependent on this device. For OSPM to "put
    // the device in" the D0 device state
    Method(_PR0) {
      Return (Package(){\_SB.PC00.D3C})
    }
    // To evaluates to a list of power resources upon which this device is dependent when it is in the D3hot state.
    // The PMSS is in D3H when this method is called. For devices that support both D3hot and D3 exposed to OSPM via _PR3,
    // device software/drivers must always assume OSPM will target D3 and must assume all device context will be lost
    // and the device will no longer be enumerable.
    Method(_PR3) {
      Return (Package(){\_SB.PC00.D3C})
    }
  }

  OperationRegion(XPRT,SystemMemory,BASE(),0x100)
  //
  // Byte access for PMCS field to avoid race condition on device D-state
  //
  Field(XPRT,ByteAcc,NoLock,Preserve)
  {
    VDID, 32,
    Offset(R_XHCI_CFG_PWR_CNTL_STS), // 0x74
    D0D3,  2,  // 0x74 BIT[1:0]
        ,  6,
    PMEE,  1,  // PME Enable
        ,  6,
    PMES,  1,  // PME Status
  }

  //
  // Variable to store the maximum D state supported in S0.
  //
  Name (XFLT, 0)

  //
  // XHCI controller _DSM method
  //
  Method(_DSM,4,serialized)
  {
    ADBG("XHCI _DSM")

    If(PCIC(Arg0)) { return(PCID(Arg0,Arg1,Arg2,Arg3)) }

    Return(Buffer() {0})
  }


  //
  // _SXD and _SXW methods
  //
  Method(_S3D, 0, NotSerialized)
  {
    Return(3)
  }
  Method(_S4D, 0, NotSerialized)
  {
    Return(3)
  }
  Method(_S3W, 0, NotSerialized)
  {
    Return(3)
  }
  Method(_S4W, 0, NotSerialized)
  {
    Return(3)
  }

  //
  // Additionally if the device is capable of waking up
  // the platform from a low power System state (S3 or S4), there is a _S0W and _PRW
  // associated with the device as well.
  //
  Method(_PRW, 0) {
    //
    // Report the deepest wakeable state is S4 always in order to supporting TCSS RTD3 capability is some OS builds. The actual Sx wakeable state is mapped and managed by
    // VCCST's preference to IOM for remaining or removing VNNAON rail in S4 state. BIOS is no longer required to report _PRW with S3 wake capability to OS even if
    // the platform wants to limit USBC Sx wake capability to S4 only, or when VCCST support is disabled.
    // The actual S4 wake capability will be limited by VCST setting instead of OS managment. Platform shall set VCCST setting to OFF when not supporting S4 wake over USBC.
    //
    Return (GPRW (0x6D, 4)) // can wakeup from S4 state
  }

  Method(_DSW, 3)
  {
    ADBG("TCSS XHCI _DSW")
    ADBG(Concatenate("Arg0 -", ToHexString(Arg0)))
    ADBG(Concatenate("Arg1 -", ToHexString(Arg1)))
    ADBG(Concatenate("Arg2 -", ToHexString(Arg2)))
    C2PM (Arg0, Arg1, Arg2, DCPM)
    Store(Arg1, SD3C)                                 // If entering Sx (Arg1 > 1), need to skip TCSS D3Cold & TBT RTD3/D3Cold.
    ADBG(Concatenate("SD3C -", ToHexString(SD3C)))
  }

  //
  // xHCI Root Hub Device
  //
  Device(RHUB)
  {
    Name(_ADR, Zero)

    //
    // Method for creating Type C _PLD buffers
    // _PLD contains lots of data, but for purpose of internal validation we care only about
    // ports visibility and pairing (this requires group position)
    // so these are the only 2 configurable parameters
    //
    Method(TPLD, 2, Serialized) { // For the port related with USB Type C.
      // Arg0:  Visible
      // Arg1:  Group position
      Name(PCKG, Package() { Buffer(0x10) {} } )
      CreateField(DerefOf(Index(PCKG,0)), 0, 7, REV)
      Store(1,REV)
      CreateField(DerefOf(Index(PCKG,0)), 64, 1, VISI)
      Store(Arg0, VISI)
      CreateField(DerefOf(Index(PCKG,0)), 87, 8, GPOS)
      Store(Arg1, GPOS)
      // For USB type C, Standerd values
      CreateField(DerefOf(Index(PCKG,0)), 74, 4, SHAP)  // Shape set to Oval
      Store(1, SHAP)
      CreateField(DerefOf(Index(PCKG,0)), 32, 16, WID)  // Width of the connector, 8.34mm
      Store(8, WID)
      CreateField(DerefOf(Index(PCKG,0)), 48, 16, HGT)  // Height of the connector, 2.56mm
      Store(3, HGT)
      return (PCKG)
    }

    //
    // Method for creating Type C _UPC buffers
    // Similar to _PLD, for internal testing we only care about 1 parameter
    //
    Method(TUPC, 2, Serialized) { // For the port related with USB Type C.
      // Arg0: Connectable
      // Arg1: Type
      // Type:
      //  0x08:     Type C connector - USB2-only
      //  0x09:     Type C connector - USB2 and SS with Switch
      //  0x0A:     Type C connector - USB2 and SS without Switch
      Name(PCKG, Package(4) { 1, 0x00, 0, 0 } )
      Store(Arg0,Index(PCKG,0))
      Store(Arg1,Index(PCKG,1))
      return (PCKG)
    }
//[-start-210818-TAMT000001-add]//
//[-start-210830-TAMT000004-modify]//
#if defined(C970_SUPPORT)
//[-end-210830-TAMT000004-modify]//
    //
    // High Speed Ports
    //
    Device(HS01)
    {
      Name(_ADR, 0x01)
      // TODO: Check what to do about _UPC & _PLD
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
    }

    //
    // Super Speed Ports
    //
    Device(SS01)
    {
      Name(_ADR, 0x02)
      Name(_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB3.0 type C
                                                     //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
      0x31, 0x1C, 0x80, 0x01, //bit64 Visible, bit67:69 Unknown 
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS01 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 0}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS02)
    {
      Name(_ADR, 0x03)
      Name(_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB3.0 type C
                                                     //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
      0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
//[-start-220816-SUSIE0002-remove]//
/*
      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS02 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 1}
                }
              }
            ) // End of Return ()
          }
        }
      }
*/
//[-end-220816-SUSIE0002-remove]//
    }

    Device(SS03)
    {
      Name(_ADR, 0x04)
      Name(_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB3.0 type C
                                                     //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
      0x31, 0x1C, 0x80, 0x03, //bit64 Visible, bit67:69 Unknown 
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
//[-start-220816-SUSIE0002-remove]//
/*
      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS03 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 2}
                }
              }
            ) // End of Return ()
          }
        }
      }
*/
//[-end-220816-SUSIE0002-remove]//
    }

    Device(SS04)
    {
      Name(_ADR, 0x05)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS04 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 3}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }
//[-start-210830-TAMT000004-add]//
#elif defined(C770_SUPPORT)
    //
    // High Speed Ports
    //
    Device(HS01)
    {
      Name(_ADR, 0x01)
      // TODO: Check what to do about _UPC & _PLD
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
    }

    //
    // Super Speed Ports
    //
    Device(SS01)
    {
      Name(_ADR, 0x02)
      Name(_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB3.0 type C
                                                     //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
      0x31, 0x1C, 0x80, 0x01, //bit64 Visible, bit67:69 Unknown 
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS01 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 0}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS02)
    {
      Name(_ADR, 0x03)
//[-start-211228-BAOBO00021-modify]//
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
                                                     //connector
                                             //connector
//      Name (_PLD, Package() { Buffer(0x10) {
//      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
//      0x00, 0x00, 0x00, 0x00, 
//      0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
//      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
//      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
//[-end-211228-BAOBO00021-modify]//

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS02 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 1}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS03)
    {
      Name(_ADR, 0x04)
      Name(_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB3.0 type C
                                                     //connector
      Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
//[-start-211228-BAOBO00021-modify]//
        0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
//[-end-211228-BAOBO00021-modify]//
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS03 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 2}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS04)
    {
      Name(_ADR, 0x05)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS04 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 3}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }
//[-end-210830-TAMT000004-add]//

//[-start-210906-Xinwei001-add]//
#elif defined(S370_SUPPORT)
    //
    // High Speed Ports
    //
    Device(HS01)
    {
      Name(_ADR, 0x01)
      // TODO: Check what to do about _UPC & _PLD
      //Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
    }

    //
    // Super Speed Ports
    //
    Device(SS01)
    {
      Name(_ADR, 0x02)
      Name(_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB3.0 type C
                                                       //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
//[-start-210911-QINGLIN0064-modify]//
      0x31, 0x1C, 0x00, 0x01, //bit64 Visible, bit67:69 Unknown 
//[-end-210911-QINGLIN0064-modify]//
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS01 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 0}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS02)
    {
      Name(_ADR, 0x03)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS02 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 1}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS03)
    {
      Name(_ADR, 0x04)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS03 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 2}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS04)
    {
      Name(_ADR, 0x05)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS04 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 3}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }
//[-end-210906-Xinwei001-add]//
//[-start-210919-QINGLIN0073-add]//
#elif defined(S570_SUPPORT)
    //
    // High Speed Ports
    //
    Device(HS01)
    {
      Name(_ADR, 0x01)
      // TODO: Check what to do about _UPC & _PLD
      //Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
    }

    //
    // Super Speed Ports
    //
    Device(SS01)
    {
      Name(_ADR, 0x02)
      Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C port1
                                                        //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
      0x31, 0x1C, 0x00, 0x01, //bit64 Visible, bit67:69 Unknown 
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS01 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 0}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS02)
    {
      Name(_ADR, 0x03)
      Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C port2
                                                        //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
      0x31, 0x1C, 0x80, 0x01, //bit64 Visible, bit67:69 Unknown 
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 3, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS02 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 1}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS03)
    {
      Name(_ADR, 0x04)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS03 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 2}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS04)
    {
      Name(_ADR, 0x05)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS04 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 3}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }
//[-end-210919-QINGLIN0073-add]//
//[-start-210919-TAMT000009-add]//
#elif defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    //
    // High Speed Ports
    //
    Device(HS01)
    {
      Name(_ADR, 0x01)
      // TODO: Check what to do about _UPC & _PLD
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
    }

    //
    // Super Speed Ports
    //
    Device(SS01)
    {
      Name(_ADR, 0x02)
      Name(_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB3.0 type C
                                                     //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
      0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS01 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 0}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS02)
    {
      Name(_ADR, 0x03)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS02 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 1}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS03)
    {
      Name(_ADR, 0x04)
      Name(_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB3.0 type C
                                                     //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
      0x31, 0x1C, 0x80, 0x01, //bit64 Visible, bit67:69 Unknown 
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS03 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 2}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS04)
    {
      Name(_ADR, 0x05)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS04 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 3}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }
//[-end-210919-TAMT000009-add]//
//[-start-211013-TAMT000028-add]//
#elif defined(S77013_SUPPORT)
    //
    // High Speed Ports
    //
    Device(HS01)
    {
      Name(_ADR, 0x01)
      // TODO: Check what to do about _UPC & _PLD
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
    }

    //
    // Super Speed Ports
    //
    Device(SS01)
    {
      Name(_ADR, 0x02)
      Name(_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB3.0 type C
                                                     //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
      0x31, 0x1C, 0x80, 0x01, //bit64 Visible, bit67:69 Unknown 
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS01 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 0}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS02)
    {
      Name(_ADR, 0x03)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS02 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 1}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS03)
    {
      Name(_ADR, 0x04)
      Name(_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB3.0 type C
                                                     //connector
      Name(_PLD, Package() { Buffer(0x10) {
      0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
      0x00, 0x00, 0x00, 0x00, 
      0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
      0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
      })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
//[-start-220816-SUSIE0002-remove]//
/*
      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS03 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 2}
                }
              }
            ) // End of Return ()
          }
        }
      }
*/
//[-end-220816-SUSIE0002-remove]//
    }

    Device(SS04)
    {
      Name(_ADR, 0x05)
      Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS04 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 3}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }
//[-end-211013-TAMT000028-add]//
#else
//[-end-210818-TAMT000001-add]//
   //
    // High Speed Ports
    //
    Device(HS01)
    {
      Name(_ADR, 0x01)
      // TODO: Check what to do about _UPC & _PLD
    }

    //
    // Super Speed Ports
    //
    Device(SS01)
    {
      Name(_ADR, 0x02)

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS01 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 0}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS02)
    {
      Name(_ADR, 0x03)

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS02 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM0},
                  Package (2) {"usb4-port-number", 1}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS03)
    {
      Name(_ADR, 0x04)

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS03 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 2}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }

    Device(SS04)
    {
      Name(_ADR, 0x05)

      If(CondRefOf(U4SE))
      {
        If (LEqual (U4SE, 1))
        {
          Method (_DSD, 0) {
            ADBG ("XHC SS04 _DSD")
            Return (
              Package () {
                ToUUID ("DAFFD814-6EBA-4D8C-8A91-BC9BBF4AA301"),
                Package () {
                  Package (2) {"usb4-host-interface", \_SB.PC00.TDM1},
                  Package (2) {"usb4-port-number", 3}
                }
              }
            ) // End of Return ()
          }
        }
      }
    }
//[-start-210818-TAMT000001-add]//
#endif
//[-end-210818-TAMT000001-add]//
    Method(_PS0,0,Serialized)
    {
      ADBG("TCSS RHUB XHCI PS0")
    }

    Method(_PS2,0,Serialized)
    {
      ADBG("TCSS RHUB XHCI PS2")
    }

    Method(_PS3,0,Serialized)
    {
      ADBG("TCSS RHUB XHCI PS3")
    }

  } // End of Device(RHUB)