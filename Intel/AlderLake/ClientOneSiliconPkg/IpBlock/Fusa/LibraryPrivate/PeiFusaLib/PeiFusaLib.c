/** @file
  Initilize FUSA device in PEI

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 -2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Ppi/SiPolicy.h>
#include <FusaConfig.h>
#include <Register/FusaRegs.h>
#include <Register/IgdRegs.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchPcrLib.h>
#include <Library/IoLib.h>
#include <Library/CpuDmiInfoLib.h>
#include <Library/PsfLib.h>
#include <CpuSbInfo.h>
#include <Register/CpuPsfRegs.h>
#include <Library/CpuRegbarAccessLib.h>
#include <Register/ArchMsr.h>
#include <Library/PeiFusaLib.h>

#include <Library/PeiServicesLib.h>
#include <Ppi/MpServices2.h>

extern EFI_GUID                   gEdkiiPeiMpServices2PpiGuid;
STATIC EDKII_PEI_MP_SERVICES2_PPI *gMpServices2Ppi = NULL;
UINTN                             mNumberOfProcessors;
UINTN                             mNumberOfEnabledProcessors;

typedef union {
  struct {
    UINT32  Low;
    UINT32  High;
  } Data32;
  UINT64 Data;
} UINT64_STRUCT;

/**
  Lock FUSA Registers After BIOS_DONE.
**/
VOID
FusaRegisterLock (
  VOID
  )
{
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  UINT32      Data32;
  UINT64      McD2BaseAddress;
  UINTN       GttMmAdr;

  McD2BaseAddress   = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;

  //lock the FUSA lock bit
  MmioOr32 (GttMmAdr + R_SA_GTMMADR_DE_FUSA_PARITY_CTRL_OFFSET, B_SA_GTMMADR_DE_FUSA_IOSF_PARITY_CTL_LOCK_BIT);
  Data32 = MmioRead32(GttMmAdr + R_SA_GTMMADR_DE_FUSA_PARITY_CTRL_OFFSET);
  DEBUG ((DEBUG_INFO, "Lock FUSA Display Lock Bit 0x%x\n", Data32));

  MmioOr32 (GttMmAdr + R_SA_GTMMADR_SF_CTL_0, B_SA_GTMMADR_DE_FUSA_SF_CTL_LOCK_BIT);
  Data32 = MmioRead32(GttMmAdr + R_SA_GTMMADR_SF_CTL_0);
  DEBUG ((DEBUG_INFO, "Lock FUSA Graphics Lock Bit 0x%x\n", Data32));
#endif
}

/**
  FusaOverrideProgramming: Override the Display FUSA register for enabling/disabling FUSA features

  @param[in] FUSA_CONFIG        The FUSA Policy PPI instance
  @retval     EFI_SUCCESS     - Display workarounds done
**/
EFI_STATUS
DisplayFusaOverrideProgramming (
   IN  FUSA_CONFIG         *FusaConfig,
   IN  UINTN               GttMmAdr
  )
{
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  UINT32                  Data32;

  if (FusaConfig->DisplayFusaConfigEnable == 1){
    DEBUG ((DEBUG_INFO, "Enable Fusa on Display\n"));

    Data32 = ( B_SA_GTMMADR_DE_FUSA_IOSF_PARITY_CMD_BIT | B_SA_GTMMADR_DE_FUSA_IOSF_PARITY_DATA_BIT | B_SA_GTMMADR_DE_FUSA_DIP_PARITY_CMD_BIT | B_SA_GTMMADR_DE_FUSA_DIP_PARITY_DATA_BIT);
    MmioOr32 (GttMmAdr + R_SA_GTMMADR_DE_FUSA_PARITY_CTRL_OFFSET, Data32);

    Data32 = B_SA_GTMMADR_DISPLAY_ERROR_MASK;
    MmioOr32 (GttMmAdr + R_SA_GTMMADR_DISPLAY_ERR_COR_OFFSET, Data32);
    MmioOr32 (GttMmAdr + R_SA_GTMMADR_DISPLAY_ERR_NONFATAL_OFFSET, Data32);
    MmioOr32 (GttMmAdr + R_SA_GTMMADR_DISPLAY_ERR_FATAL_OFFSET, Data32);
  } else {
    DEBUG ((DEBUG_INFO, "Disable Fusa on Display\n"));
    Data32 = (UINT32)~( B_SA_GTMMADR_DE_FUSA_IOSF_PARITY_CMD_BIT | B_SA_GTMMADR_DE_FUSA_IOSF_PARITY_DATA_BIT | B_SA_GTMMADR_DE_FUSA_DIP_PARITY_CMD_BIT | B_SA_GTMMADR_DE_FUSA_DIP_PARITY_DATA_BIT);
    MmioAnd32(GttMmAdr + R_SA_GTMMADR_DE_FUSA_PARITY_CTRL_OFFSET, Data32);

    Data32 = ~(B_SA_GTMMADR_DISPLAY_ERROR_MASK);
    MmioAnd32 (GttMmAdr + R_SA_GTMMADR_DISPLAY_ERR_COR_OFFSET, Data32);
    MmioAnd32 (GttMmAdr + R_SA_GTMMADR_DISPLAY_ERR_NONFATAL_OFFSET, Data32);
    MmioAnd32 (GttMmAdr + R_SA_GTMMADR_DISPLAY_ERR_FATAL_OFFSET, Data32);
  }

#endif
  return EFI_SUCCESS;
}

/**
  FusaOverrideProgramming: Override the Graphics FUSA register for enabling/disabling FUSA features

  @param[in] SiPolicyPpi        The SI Policy PPI instance
  @retval     EFI_SUCCESS     - Graphics workarounds done
**/
EFI_STATUS
GraphicsFusaOverrideProgramming (
   IN  FUSA_CONFIG         *FusaConfig,
   IN  UINTN               GttMmAdr
  )
{
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  UINT32                  Data32;

  if (FusaConfig->GraphicFusaConfigEnable == 1){
    DEBUG ((DEBUG_INFO, "Enable Fusa on Graphics\n"));
    Data32 = B_SA_GTMMADR_GT_ERROR_MASK;
    MmioOr32 (GttMmAdr + R_SA_GTMMADR_GT_ERR_COR_OFFSET, Data32);
    MmioOr32 (GttMmAdr + R_SA_GTMMADR_GT_ERR_NONFATAL_OFFSET, Data32);
    MmioOr32 (GttMmAdr + R_SA_GTMMADR_GT_ERR_FATAL_OFFSET, Data32);
  }
  else{
    DEBUG ((DEBUG_INFO, "Disable Fusa on Graphics\n"));
    Data32 = ~(B_SA_GTMMADR_GT_ERROR_MASK);
    MmioAnd32 (GttMmAdr + R_SA_GTMMADR_GT_ERR_COR_OFFSET, Data32);
    MmioAnd32 (GttMmAdr + R_SA_GTMMADR_GT_ERR_NONFATAL_OFFSET, Data32);
    MmioAnd32 (GttMmAdr + R_SA_GTMMADR_GT_ERR_FATAL_OFFSET, Data32);
  }

#endif
  return EFI_SUCCESS;
}

/**
  FusaOverrideProgramming: Override the IGD FUSA registers for enabling/disabling FUSA features

  @param[in] SiPolicyPpi        The SI Policy PPI instance
  @retval     EFI_SUCCESS     - IGD workarounds done
**/
EFI_STATUS
IgdFusaOverrideProgramming (
   IN  FUSA_CONFIG    *FusaConfig
  )
{
  #if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
    UINT64                  McD2BaseAddress;
    UINTN                   GttMmAdr;

    McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
    GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;

    // Enable error reporting for Fusa Graphic or Display
    if (FusaConfig->GraphicFusaConfigEnable == 1 || FusaConfig->DisplayFusaConfigEnable == 1) {
      PciSegmentOr32 (McD2BaseAddress + R_SA_PCI_DEVICECTL_OFFSET, ( BIT2 | BIT1 | BIT0));
    } else {
      PciSegmentAnd32 (McD2BaseAddress + R_SA_PCI_DEVICECTL_OFFSET, (UINT32)~( BIT2 | BIT1 | BIT0));
    }

    GraphicsFusaOverrideProgramming (FusaConfig, GttMmAdr);
    DisplayFusaOverrideProgramming (FusaConfig, GttMmAdr);

  #endif
  return EFI_SUCCESS;
}

/**
  FusaOverrideProgramming: Override the Opio FUSA register for enabling/disabling FUSA features

  @param[in] SiPolicyPpi        The SI Policy PPI instance
  @retval     EFI_SUCCESS     - Display workarounds done
**/
EFI_STATUS
OpioFusaOverrideProgramming (
   IN  FUSA_CONFIG    *FusaConfig
  )
{
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  UINT32                      Data32;
  UINT64_STRUCT               DmiBar;
  UINT64                      McD0BaseAddress;
  UINTN                       DmiBarAddress;


  McD0BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
  DmiBar.Data32.High = PciSegmentRead32 (McD0BaseAddress + R_SA_DMIBAR + 4);
  DmiBar.Data32.Low  =  PciSegmentRead32 (McD0BaseAddress + R_SA_DMIBAR);
  DmiBar.Data       &= (UINT64) ~BIT0;

  DmiBarAddress = (UINTN)DmiBar.Data;

  DEBUG ((DEBUG_INFO, "OPIO FUSA override Fuction \n"));
  if (FusaConfig-> OpioFusaConfigEnable == 1){
    DEBUG ((DEBUG_INFO, "Enable Fusa on Opio\n"));
    Data32 = ( B_SA_DMIBAR_PARITY_GEN_EN | B_SA_DMIBAR_DATA_PARITY_FATAL_CFG | B_SA_DMIBAR_DATA_PARITY_FATAL_OPI_CFG);
    MmioOr32 (DmiBarAddress+ R_SA_DMIBAR_FUSA_E2EPARITY_CTL_OFFSET, Data32);
  }
  else{
    DEBUG ((DEBUG_INFO, "Disable Fusa on Opio\n"));
    Data32 = Data32 & ~( B_SA_DMIBAR_PARITY_GEN_EN | B_SA_DMIBAR_DATA_PARITY_FATAL_CFG | B_SA_DMIBAR_DATA_PARITY_FATAL_OPI_CFG);
    MmioAnd32 ((DmiBarAddress + R_SA_DMIBAR_FUSA_E2EPARITY_CTL_OFFSET), Data32);
  }
#endif
  return EFI_SUCCESS;
}

/**
  FusaOverrideProgramming: Override the Psf FUSA register for enabling/disabling FUSA features

  @param[in] SiPolicyPpi        The SI Policy PPI instance
  @retval     EFI_SUCCESS     - Psf workarounds done
**/
EFI_STATUS
PsfFusaOverrideProgramming (
  IN  SI_POLICY_PPI    *SiPolicy
  )
{
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  FUSA_CONFIG                 *FusaConfig;
  EFI_STATUS                  Status;
  UINT16                      OrData;
  UINT16                      AndData;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gFusaConfigGuid, (VOID *) &FusaConfig);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "PSF CPU FUSA override Function \n"));
  DEBUG ((DEBUG_INFO, "Read Default PSF Config = %04x\n", CpuRegbarRead16 (CPU_SB_PID_PSF0, R_SA_PSF_GLOBAL_CONFIG)));
  DEBUG ((DEBUG_INFO, "Read Default PSF Config = %04x\n", CpuRegbarRead16 (CPU_SB_PID_PSF1, R_SA_PSF_GLOBAL_CONFIG)));

  if (FusaConfig-> PsfFusaConfigEnable == 1){
    DEBUG ((DEBUG_INFO, "Enable Fusa on PSF \n"));
    OrData = (UINT16)(B_SA_PSF_GLOBAL_CONFIG_ENCMDPARDETEGRESS | B_SA_PSF_GLOBAL_CONFIG_ENCMDPARDETINGRESS);
    CpuRegbarOr16 (CPU_SB_PID_PSF0, R_SA_PSF_GLOBAL_CONFIG , OrData);
    CpuRegbarOr16 (CPU_SB_PID_PSF1, R_SA_PSF_GLOBAL_CONFIG , OrData);
    DEBUG ((DEBUG_INFO, "Read After PSF Config = %04x\n", CpuRegbarRead16 (CPU_SB_PID_PSF0, R_SA_PSF_GLOBAL_CONFIG)));
    DEBUG ((DEBUG_INFO, "Read After PSF Config = %04x\n", CpuRegbarRead16 (CPU_SB_PID_PSF1, R_SA_PSF_GLOBAL_CONFIG)));
  } else {
    AndData = (UINT16)~(B_SA_PSF_GLOBAL_CONFIG_ENCMDPARDETEGRESS | B_SA_PSF_GLOBAL_CONFIG_ENCMDPARDETINGRESS);
    DEBUG ((DEBUG_INFO, "Disable Fusa on PSF \n"));
    CpuRegbarAnd16 (CPU_SB_PID_PSF0, R_SA_PSF_GLOBAL_CONFIG  , AndData);
    CpuRegbarAnd16 (CPU_SB_PID_PSF1, R_SA_PSF_GLOBAL_CONFIG  , AndData);
  }
#endif
  return EFI_SUCCESS;
}

/**
  FusaOverrideProgramming: Override the IOP FUSA register for enabling/disabling FUSA features

  @param[in] SiPolicyPpi        The SI Policy PPI instance
  @retval     EFI_SUCCESS     - IOP workarounds done
**/
EFI_STATUS
IopFusaOverrideProgramming (
  IN SI_POLICY_PPI *SiPolicy
  )
{
  FUSA_CONFIG                 *FusaConfig;
  EFI_STATUS                  Status;
  UINT64                      McD0BaseAddress;
  UINT64_STRUCT               MchBar;
  UINT32                      Data32;
  UINTN                       MchBarAddress;

  McD0BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  MchBar.Data32.High = PciSegmentRead32 (McD0BaseAddress + R_SA_MCHBAR + 4);
  MchBar.Data32.Low  = PciSegmentRead32 (McD0BaseAddress + R_SA_MCHBAR);
  MchBar.Data       &= (UINT64) ~BIT0;
  MchBarAddress = (UINTN) MchBar.Data;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gFusaConfigGuid, (VOID *) &FusaConfig);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "IOP FUSA override Function \n"));

  DEBUG ((DEBUG_INFO, "PsfFusaConfigEnable %x DisplayFusaConfigEnable %x GraphicFusaConfigEnable %x OpioFusaConfigEnable %x IopFusaConfigEnable %x IopFusaMcaReporting %x\n", FusaConfig->PsfFusaConfigEnable, FusaConfig->DisplayFusaConfigEnable,
  FusaConfig->GraphicFusaConfigEnable, FusaConfig->OpioFusaConfigEnable, FusaConfig->IopFusaConfigEnable, FusaConfig->IopFusaMcaCheckEnable));

  if ((FusaConfig->PsfFusaConfigEnable == 1) || (FusaConfig->DisplayFusaConfigEnable == 1)
        || (FusaConfig->OpioFusaConfigEnable == 1) || (FusaConfig->OpioFusaConfigEnable == 1) || (FusaConfig->IopFusaConfigEnable == 1) || (FusaConfig->IopFusaMcaCheckEnable == 1)) {
    Data32 = B_SA_MCHBAR_FUSA_MCA_REPORTING_EN_0_0_0_MCHBAR_IMPH_MCA_REPORTING_EN;
    MmioOr32 (MchBarAddress + R_MCH_IMPH_FUSA_MCA_REPORTING, Data32);
  } else {
    Data32 = (UINT16)~(B_MCH_IMPH_FUSA_MCA_REPORTING_EN);
    MmioAnd32 (MchBarAddress + R_MCH_IMPH_FUSA_MCA_REPORTING, Data32);
  }
  return EFI_SUCCESS;
}

/**
  Check is the silicon is supporting fusa.

  @retval BOOLEAN TRUE/FALSE
**/
BOOLEAN
IsFusaSupported (
  VOID
  )
{
  MSR_CORE_CAPABILITIES_REGISTER                Msr;

  Msr.Uint64 = AsmReadMsr64 (MSR_CORE_CAPABILITIES);
  if (Msr.Bits.FusaSupported) {
    DEBUG ((DEBUG_INFO, "FuSA supported sku detected\n"));
    return TRUE;
  } else {
    DEBUG ((DEBUG_INFO, "non-FuSA supported sku detected\n"));
    return FALSE;
  }
}

/**
  Set processor periodic array based on setup menu options
**/
VOID
EFIAPI
PerformPeriodicArrayUpdate (
  VOID
  )
{
  UINTN                           MyCpuNumber;
  EFI_STATUS                      Status = EFI_SUCCESS;
  UINT8                           CurrProcessor;
  EFI_PROCESSOR_INFORMATION       MpContext;
  UINT64                          FusaConfigMsr;


  Status = gMpServices2Ppi->WhoAmI (
                        gMpServices2Ppi,
                        &MyCpuNumber
                        );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "ERROR who am I! Status: %x\n", Status));
  } else {
    DEBUG ((DEBUG_INFO, "Main core's CpuNumber =  %x\n", MyCpuNumber));
  }
  for (CurrProcessor = 0; CurrProcessor < mNumberOfProcessors; CurrProcessor++) {
    Status = gMpServices2Ppi->GetProcessorInfo (
                          gMpServices2Ppi,
                          CurrProcessor,
                          &MpContext
                          );
    DEBUG((DEBUG_INFO, "The CurrProcessor 0x%x ApicId is 0x%x\n", CurrProcessor, MpContext.ProcessorId));
    Status = gMpServices2Ppi->SwitchBSP (
                          gMpServices2Ppi,
                          CurrProcessor,
                          TRUE
                          );
    // Only configures all atom cores
    if ((MpContext.ProcessorId >= GRT_MOD8_LPID0_APIC_ID) && (MpContext.ProcessorId <= GRT_MOD9_LPID3_APIC_ID)) {
      FusaConfigMsr = AsmReadMsr64 (MSR_FUSA_CONFIG);
      DEBUG ((DEBUG_INFO, "FusaConfigMsr initial value =  %x\n", FusaConfigMsr));
      FusaConfigMsr = FusaConfigMsr | BIT0;
      AsmWriteMsr64 (MSR_FUSA_CONFIG, FusaConfigMsr);
      FusaConfigMsr = AsmReadMsr64 (MSR_FUSA_CONFIG);
      DEBUG ((DEBUG_INFO, "FusaConfigMsr readback =  %x\n", FusaConfigMsr));
    }
  }

  DEBUG ((DEBUG_INFO, "Switch back to Main core's CpuNumber =  %x\n", MyCpuNumber));
  Status = gMpServices2Ppi->SwitchBSP (
                        gMpServices2Ppi,
                        MyCpuNumber,
                        TRUE
                        );
  return;
}

/**
  Set processor periodic scan based on setup menu options
**/
VOID
EFIAPI
PerformPeriodicScanUpdate (
  VOID
  )
{
  UINTN                           MyCpuNumber;
  EFI_STATUS                      Status = EFI_SUCCESS;
  UINT8                           CurrProcessor;
  EFI_PROCESSOR_INFORMATION       MpContext;
  UINT64                          FusaConfigMsr;

  Status = gMpServices2Ppi->WhoAmI (
                        gMpServices2Ppi,
                        &MyCpuNumber
                        );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "ERROR who am I! Status: %x\n", Status));
  } else {
    DEBUG ((DEBUG_INFO, "Main core's CpuNumber =  %x\n", MyCpuNumber));
  }
  for (CurrProcessor = 0; CurrProcessor < mNumberOfProcessors; CurrProcessor++) {
    Status = gMpServices2Ppi->GetProcessorInfo (
                          gMpServices2Ppi,
                          CurrProcessor,
                          &MpContext
                          );
    DEBUG((DEBUG_INFO, "The CurrProcessor 0x%x ApicId is 0x%x\n", CurrProcessor, MpContext.ProcessorId));
    Status = gMpServices2Ppi->SwitchBSP (
                          gMpServices2Ppi,
                          CurrProcessor,
                          TRUE
                          );
    // Only configures all atom cores
    if ((MpContext.ProcessorId >= GRT_MOD8_LPID0_APIC_ID) && (MpContext.ProcessorId <= GRT_MOD9_LPID3_APIC_ID)) {
      FusaConfigMsr = AsmReadMsr64 (MSR_FUSA_CONFIG);
      DEBUG ((DEBUG_INFO, "FusaConfigMsr initial value =  %x\n", FusaConfigMsr));
      FusaConfigMsr = FusaConfigMsr | BIT1;
      AsmWriteMsr64 (MSR_FUSA_CONFIG, FusaConfigMsr);
      FusaConfigMsr = AsmReadMsr64 (MSR_FUSA_CONFIG);
      DEBUG ((DEBUG_INFO, "FusaConfigMsr readback =  %x\n", FusaConfigMsr));
    }
  }

  DEBUG ((DEBUG_INFO, "Switch back to Main core's CpuNumber =  %x\n", MyCpuNumber));
  Status = gMpServices2Ppi->SwitchBSP (
                        gMpServices2Ppi,
                        MyCpuNumber,
                        TRUE
                        );
  return;
}

/**
  Execute FUSA Startup Array BIST if enabled in BIOS menu

  @param[in] FusaConfig
**/
VOID
EFIAPI
ExecuteFusaStartUpArrayBIST (
  IN  FUSA_CONFIG    *FusaConfig
  )
{
  UINTN                           MyCpuNumber;
  EFI_STATUS                      Status = EFI_SUCCESS;
  UINT8                           CurrProcessor;
  EFI_PROCESSOR_INFORMATION       MpContext;
  UINT64                          FusaConfigMsr;

  if (FusaConfig->FusaRunStartupArrayBist == 1) {
    DEBUG ((DEBUG_INFO, "Execute Startup Array BIST"));

    //
    // Locate MP protocol, enumerate AP and update MSR accordingly
    //
    Status = gMpServices2Ppi->WhoAmI (
                          gMpServices2Ppi,
                          &MyCpuNumber
                          );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "ERROR who am I! Status: %x\n", Status));
    } else {
      DEBUG ((DEBUG_INFO, "Main core's CpuNumber =  %x\n", MyCpuNumber));
    }
    for (CurrProcessor = 0; CurrProcessor < mNumberOfProcessors; CurrProcessor++) {
      Status = gMpServices2Ppi->GetProcessorInfo (
                            gMpServices2Ppi,
                            CurrProcessor,
                            &MpContext
                            );
      DEBUG((DEBUG_INFO, "The CurrProcessor 0x%x ApicId is 0x%x\n", CurrProcessor, MpContext.ProcessorId));
      Status = gMpServices2Ppi->SwitchBSP (
                            gMpServices2Ppi,
                            CurrProcessor,
                            TRUE
                            );
      // configures all atom cores to trigger startup array BIST
      if ((MpContext.ProcessorId >= GRT_MOD8_LPID0_APIC_ID) && (MpContext.ProcessorId <= GRT_MOD9_LPID3_APIC_ID)) {
        FusaConfigMsr = AsmReadMsr64 (MSR_TRIGGER_STARTUP_MEM_BIST);
        DEBUG ((DEBUG_INFO, "FusaConfigMsr initial value =  %x\n", FusaConfigMsr));
        FusaConfigMsr = FusaConfigMsr | BIT0;
        AsmWriteMsr64 (MSR_TRIGGER_STARTUP_MEM_BIST, FusaConfigMsr);
        FusaConfigMsr = AsmReadMsr64 (MSR_TRIGGER_STARTUP_MEM_BIST);
        DEBUG ((DEBUG_INFO, "FusaConfigMsr readback =  %x\n", FusaConfigMsr));
      }
    }

    DEBUG ((DEBUG_INFO, "After configuring all atom cores to trigger array BIST, read the status now.\n"));

    for (CurrProcessor = 0; CurrProcessor < mNumberOfProcessors; CurrProcessor++) {
      Status = gMpServices2Ppi->GetProcessorInfo (
                            gMpServices2Ppi,
                            CurrProcessor,
                            &MpContext
                            );
      DEBUG((DEBUG_INFO, "The CurrProcessor 0x%x ApicId is 0x%x\n", CurrProcessor, MpContext.ProcessorId));
      Status = gMpServices2Ppi->SwitchBSP (
                            gMpServices2Ppi,
                            CurrProcessor,
                            TRUE
                            );
      // read all atom cores' status on startup array BIST
      if ((MpContext.ProcessorId >= GRT_MOD8_LPID0_APIC_ID) && (MpContext.ProcessorId <= GRT_MOD9_LPID3_APIC_ID)) {
        FusaConfigMsr = AsmReadMsr64 (MSR_STARTUP_MEM_BIST_STATUS);
        DEBUG ((DEBUG_INFO, "Start up BIST read status is %x\n", FusaConfigMsr));
        if ((FusaConfigMsr & BIT0) == BIT0) {
          DEBUG ((DEBUG_INFO, "Startup array BIST passed!\n"));
        } else {
          DEBUG ((DEBUG_INFO, "Startup array BIST failed!\n"));
        }
      }
    }

    DEBUG ((DEBUG_INFO, "Switch back to Main core's CpuNumber =  %x\n", MyCpuNumber));
    Status = gMpServices2Ppi->SwitchBSP (
                          gMpServices2Ppi,
                          MyCpuNumber,
                          TRUE
                          );

  } else {
    DEBUG ((DEBUG_INFO, "Skip executing Startup Array BIST"));
  }
}

/**
  Execute FUSA Startup scan BIST if enabled in BIOS menu

  @param[in]  FusaConfig
**/
VOID
EFIAPI
ExecuteFusaStartupScanBIST (
  IN  FUSA_CONFIG    *FusaConfig
  )
{
  if (FusaConfig->FusaRunStartupScanBist == 1) {
    DEBUG ((DEBUG_INFO, "Execute Startup Scan BIST"));
    // TBD: This can be either done in OS or BIOS level
    // creating this option to implement in BIOS level
    // after receiving scan pattern from validation team.
  } else {
    DEBUG ((DEBUG_INFO, "Skip executing Startup Scan BIST"));
  }
}

/**
  Execute FUSA periodic scan BIST if enabled in BIOS menu

  @param[in] FusaConfig
**/
VOID
EFIAPI
ExecuteFusaPeriodicScanArrayBIST (
  IN  FUSA_CONFIG    *FusaConfig
  )
{
  if (FusaConfig->FusaRunPeriodicScanBist == 1) {
    DEBUG ((DEBUG_INFO, "Execute Periodic Scan BIST"));
    // TBD: This can be either done in OS or BIOS level
    // creating this option to implement in BIOS level
    // after receiving scan pattern from validation team.
  } else {
    DEBUG ((DEBUG_INFO, "Skip executing Periodic Scan BIST"));
  }
}

/**
  Set processor lockstep based on setup menu options

  @param[in] LockStepConfig
**/
VOID
EFIAPI
ApSetLockStep (
  LOCKSTEP_CONFIG  LockStepConfig
  )
{
  while(!LockStepConfig.GlobalApEnable){}
  if (!LockStepConfig.LockStepEn[LockStepConfig.CurrProcessor]) {
#if defined __GNUC__  // GCC compiler
    __asm__ __volatile__ (
      "wrmsr"
      :
      : "c" (MSR_ENABLE_LSM),
        "A" (0)
      );
#else // MSFT compiler
    // lockstep disable
    _asm
    {
      mov     eax, 0      //  lower half of 64 bit data
      mov     ebx, 0
      mov     ecx, MSR_ENABLE_LSM   //  msr address
      mov     edx, 0      //  upper half of 64 bit data
      wrmsr
    }
#endif
  } else {
#if defined __GNUC__  // GCC compiler
    __asm__ __volatile__ (
      "wrmsr"
      :
      : "c" (MSR_ENABLE_LSM),
        "A" (1)
      );
#else // MSFT compiler
    // lockstep enable
    _asm
    {
      mov     eax, 1
      mov     ebx, 0
      mov     ecx, MSR_ENABLE_LSM
      mov     edx, 0
      wrmsr
    }
#endif
  }
  return;
}

/**
  Set processor lockstep based on setup menu options

  @param[in] FusaConfig
**/
VOID
EFIAPI
PerformLockStepUpdate (
  IN  FUSA_CONFIG    *FusaConfig
  )
{
  UINTN                           MyCpuNumber;
  EFI_STATUS                      Status = EFI_SUCCESS;
  UINT8                           CurrProcessor;
  EFI_PROCESSOR_INFORMATION       MpContext;
  LOCKSTEP_CONFIG                 LockStepConfig;

  //
  // Check setup option lockstep
  //
  DEBUG ((DEBUG_INFO, "Module0Lockstep  = %x\n", FusaConfig->Module0Lockstep));
  DEBUG ((DEBUG_INFO, "Module1Lockstep  = %x\n", FusaConfig->Module1Lockstep));

  if ((FusaConfig->Module0Lockstep == LockstepDisabled) && (FusaConfig->Module1Lockstep == LockstepDisabled)) {
    DEBUG ((DEBUG_INFO, "Both locksteps are disabled, returning.\n"));
    return;
  }

  //
  // If any of the lockstep option is set, then proceed with below steps
  // 1. MP Init to get all APs
  // 2. Enumerate the AP to construct lockstep struct according to the option
  // 3. Set each AP to have lockstep procedure, hold them with a while loop waiting for a flag
  // 4. When setting AP 0 (BSP), set the flag to unblock all AP
  //
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "ERROR getting number of processor! Status: %x\n", Status));
  } else {
    DEBUG ((DEBUG_INFO, "NumberOfProcessors =  %x\n", mNumberOfProcessors));
    DEBUG ((DEBUG_INFO, "NumberOfEnabledProcessors =  %x\n", mNumberOfEnabledProcessors));
  }
  Status = gMpServices2Ppi->WhoAmI (
                        gMpServices2Ppi,
                        &MyCpuNumber
                        );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "ERROR who am I! Status: %x\n", Status));
  } else {
    DEBUG ((DEBUG_INFO, "Main core's CpuNumber =  %x\n", MyCpuNumber));
  }

  //
  // This loop iterates through all possible APs and construct the boolean array of LockStepConfig.
  //
  for (CurrProcessor = 0; CurrProcessor < mNumberOfProcessors; CurrProcessor++) {
    Status = gMpServices2Ppi->GetProcessorInfo (
                          gMpServices2Ppi,
                          CurrProcessor,
                          &MpContext
                          );
    DEBUG((DEBUG_INFO, "The CurrProcessor 0x%x ApicId is 0x%x\n", CurrProcessor, MpContext.ProcessorId));
    // Look only at lock step setting for module 0
    if ((MpContext.ProcessorId >= GRT_MOD8_LPID0_APIC_ID) && (MpContext.ProcessorId <= GRT_MOD8_LPID3_APIC_ID)) {
      DEBUG ((DEBUG_INFO, "For module 0, "));
      switch (FusaConfig->Module0Lockstep) {
        // Set all cores LSM to 0.
        case LockstepDisabled:
          DEBUG ((DEBUG_INFO, "LockstepDisabled\n"));
          LockStepConfig.LockStepEn[CurrProcessor] = FALSE;
          break;
        // Set C0C1 and C2C3 LSM to 1, the rest to 0.
        case LockstepC0C1_C2C3:
          DEBUG ((DEBUG_INFO, "LockstepC0C1_C2C3\n"));
          if (MpContext.ProcessorId == GRT_MOD8_LPID0_APIC_ID ||
              MpContext.ProcessorId == GRT_MOD8_LPID1_APIC_ID ||
              MpContext.ProcessorId == GRT_MOD8_LPID2_APIC_ID ||
              MpContext.ProcessorId == GRT_MOD8_LPID3_APIC_ID) {
            LockStepConfig.LockStepEn[CurrProcessor] = TRUE;
          } else {
            LockStepConfig.LockStepEn[CurrProcessor] = FALSE;
          }
          break;
        // Set C0C1 LSM to 1, the rest to 0.
        case LockstepC0C1:
          DEBUG ((DEBUG_INFO, "LockstepC0C1\n"));
          if (MpContext.ProcessorId == GRT_MOD8_LPID0_APIC_ID ||
              MpContext.ProcessorId == GRT_MOD8_LPID1_APIC_ID) {
            LockStepConfig.LockStepEn[CurrProcessor] = TRUE;
          } else {
            LockStepConfig.LockStepEn[CurrProcessor] = FALSE;
          }
          break;
        // Set C2C3 LSM to 1, the rest to 0.
        case LockstepC2C3:
          DEBUG ((DEBUG_INFO, "LockstepC2C3\n"));
          if (MpContext.ProcessorId == GRT_MOD8_LPID2_APIC_ID ||
              MpContext.ProcessorId == GRT_MOD8_LPID3_APIC_ID) {
            LockStepConfig.LockStepEn[CurrProcessor] = TRUE;
          } else {
            LockStepConfig.LockStepEn[CurrProcessor] = FALSE;
          }
          break;
        default:
          ASSERT (0);
      }
    } else if ((MpContext.ProcessorId >= GRT_MOD9_LPID0_APIC_ID) && (MpContext.ProcessorId <= GRT_MOD9_LPID3_APIC_ID)) {
      DEBUG ((DEBUG_INFO, "For module 1, "));
      switch (FusaConfig->Module1Lockstep) {
        // Set all cores LSM to 0.
        case LockstepDisabled:
          DEBUG ((DEBUG_INFO, "LockstepDisabled\n"));
          LockStepConfig.LockStepEn[CurrProcessor] = FALSE;
          break;
        // Set C0C1 and C2C3 LSM to 1, the rest to 0.
        case LockstepC0C1_C2C3:
          DEBUG ((DEBUG_INFO, "LockstepC0C1_C2C3\n"));
          if (MpContext.ProcessorId == GRT_MOD9_LPID0_APIC_ID ||
              MpContext.ProcessorId == GRT_MOD9_LPID1_APIC_ID ||
              MpContext.ProcessorId == GRT_MOD9_LPID2_APIC_ID ||
              MpContext.ProcessorId == GRT_MOD9_LPID3_APIC_ID) {
            LockStepConfig.LockStepEn[CurrProcessor] = TRUE;
          } else {
            LockStepConfig.LockStepEn[CurrProcessor] = FALSE;
          }
          break;
        // Set C0C1 LSM to 1, the rest to 0.
        case LockstepC0C1:
          DEBUG ((DEBUG_INFO, "LockstepC0C1\n"));
          if (MpContext.ProcessorId == GRT_MOD9_LPID0_APIC_ID ||
              MpContext.ProcessorId == GRT_MOD9_LPID1_APIC_ID) {
            LockStepConfig.LockStepEn[CurrProcessor] = TRUE;
          } else {
            LockStepConfig.LockStepEn[CurrProcessor] = FALSE;
          }
          break;
        // Set C2C3 LSM to 1, the rest to 0.
        case LockstepC2C3:
          DEBUG ((DEBUG_INFO, "LockstepC2C3\n"));
          if (MpContext.ProcessorId == GRT_MOD9_LPID2_APIC_ID ||
              MpContext.ProcessorId == GRT_MOD9_LPID3_APIC_ID) {
            LockStepConfig.LockStepEn[CurrProcessor] = TRUE;
          } else {
            LockStepConfig.LockStepEn[CurrProcessor] = FALSE;
          }
          break;
        default:
          ASSERT (0);
      }
    }
  }

  //
  // Lockstep config is done constructed for all.
  // Next is to iterate through the data struct and run StartupAP for non-BSP in descending order.
  // GlobalApEnable is to hold all AP in while loop, until all setup is ready,
  // BSP will set it to True to start all AP execution concurrently.
  //
  LockStepConfig.GlobalApEnable = FALSE;
  for (CurrProcessor = (UINT8) mNumberOfProcessors - 1; CurrProcessor > 0 ; --CurrProcessor) {
    LockStepConfig.CurrProcessor = CurrProcessor;
    DEBUG ((DEBUG_INFO, "LockStepConfig.CurrProcessor =  %x\n", LockStepConfig.CurrProcessor));
    DEBUG ((DEBUG_INFO, "LockStepConfig.LockStepEn[LockStepConfig.CurrProcessor] =  %x\n", LockStepConfig.LockStepEn[LockStepConfig.CurrProcessor]));
    Status = gMpServices2Ppi->StartupThisAP (
                gMpServices2Ppi,
                (EFI_AP_PROCEDURE) ApSetLockStep,
                LockStepConfig.CurrProcessor,
                1, // timeout
                &LockStepConfig);
  }

  //
  // The last trigger at AP 0, which is also the BSP.
  // With this, we also set GlobalApEnable to true to unlock other APs
  //
  LockStepConfig.GlobalApEnable = TRUE;
  LockStepConfig.CurrProcessor  = 0;
  DEBUG ((DEBUG_INFO, "LockStepConfig.CurrProcessor =  %x\n", LockStepConfig.CurrProcessor));
  DEBUG ((DEBUG_INFO, "LockStepConfig.LockStepEn[LockStepConfig.CurrProcessor] =  %x\n", LockStepConfig.LockStepEn[LockStepConfig.CurrProcessor]));
  Status = gMpServices2Ppi->StartupThisAP (
                gMpServices2Ppi,
                (EFI_AP_PROCEDURE) ApSetLockStep,
                LockStepConfig.CurrProcessor,
                1, // timeout
                &LockStepConfig);

  DEBUG ((DEBUG_INFO, "Switch back to Main core's CpuNumber =  %x\n", MyCpuNumber));
  Status = gMpServices2Ppi->SwitchBSP (
                        gMpServices2Ppi,
                        MyCpuNumber,
                        TRUE
                        );
  return;
}

/**
  Initialize FUSA

  @param[in] FUSA_CONFIG      FusaConfig

  @retval EFI_SUCCESS          - FUSA initialization complete
**/
EFI_STATUS
FusaInit (
  IN  SI_POLICY_PPI    *SiPolicy
  )
{
  EFI_STATUS                  Status;
  FUSA_CONFIG                 *FusaConfig;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gFusaConfigGuid, (VOID *) &FusaConfig);
  ASSERT_EFI_ERROR (Status);
  if (FusaConfig->FusaConfigEnable == 1) {
    // Locate CpuMpCpu MpService Ppi
    Status = PeiServicesLocatePpi (
              &gEdkiiPeiMpServices2PpiGuid,
              0,
              NULL,
              (VOID **) &gMpServices2Ppi
              );
    //
    // Locate MP protocol
    //
    Status = gMpServices2Ppi->GetNumberOfProcessors (gMpServices2Ppi, (UINTN *) &mNumberOfProcessors, (UINTN *) &mNumberOfEnabledProcessors);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "ERROR getting number of processor! Status: %x\n", Status));
    } else {
      DEBUG ((DEBUG_INFO, "NumberOfProcessors =  %x\n", mNumberOfProcessors));
      DEBUG ((DEBUG_INFO, "NumberOfEnabledProcessors =  %x\n", mNumberOfEnabledProcessors));
    }
    //
    // Configure Fusa Parity configuration
    //
    if (!IsPchLinkDmi ()) {
      DEBUG ((DEBUG_INFO, "OpioFusaOverrideProgramming\n"));
      OpioFusaOverrideProgramming (FusaConfig);
    }
    DEBUG ((DEBUG_INFO, "IgdFusaOverrideProgramming\n"));
    IgdFusaOverrideProgramming (FusaConfig);

    ///
    /// Check FUSA capabilities for the following and perform BIOS handling w.r.t GRT FUSA EAS
    ///

    ///
    /// Perform start up scan diagnogsis initialization
    ///
    DEBUG ((DEBUG_INFO, "Reading MSR_FUSA_CAPABILITIES_A for B_MSR_FUSA_CAP_STARTUP_SCAN_DIAGNOGSIS_MASK..\n"));
    if ((AsmReadMsr64 (MSR_FUSA_CAPABILITIES_A) & B_MSR_FUSA_CAP_STARTUP_SCAN_DIAGNOGSIS_MASK) ==
      B_MSR_FUSA_CAP_STARTUP_SCAN_DIAGNOGSIS_MASK) {
      DEBUG ((DEBUG_INFO, "B_MSR_FUSA_CAP_STARTUP_SCAN_DIAGNOGSIS_MASK  = 1\n"));
      ExecuteFusaStartupScanBIST(FusaConfig);
    }

    ///
    /// Perform start up array diagnogsis initialization
    ///
    DEBUG ((DEBUG_INFO, "Reading MSR_FUSA_CAPABILITIES_A for B_MSR_FUSA_CAP_STARTUP_ARRAY_DIAGNOGSIS_MASK..\n"));
    if ((AsmReadMsr64 (MSR_FUSA_CAPABILITIES_A) & B_MSR_FUSA_CAP_STARTUP_ARRAY_DIAGNOGSIS_MASK) ==
      B_MSR_FUSA_CAP_STARTUP_ARRAY_DIAGNOGSIS_MASK) {
      DEBUG ((DEBUG_INFO, "B_MSR_FUSA_CAP_STARTUP_ARRAY_DIAGNOGSIS_MASK  = 1\n"));
      ExecuteFusaStartUpArrayBIST(FusaConfig);
    }

    /// Perform periodic array diagnogsis initialization
    ///
    DEBUG ((DEBUG_INFO, "Reading MSR_FUSA_CAPABILITIES_A for B_MSR_FUSA_CAP_PERIODIC_ARRAY_DIAGNOGSIS_MASK..\n"));
    if ((AsmReadMsr64 (MSR_FUSA_CAPABILITIES_A) & B_MSR_FUSA_CAP_PERIODIC_ARRAY_DIAGNOGSIS_MASK) ==
      B_MSR_FUSA_CAP_PERIODIC_ARRAY_DIAGNOGSIS_MASK) {
      DEBUG ((DEBUG_INFO, "B_MSR_FUSA_CAP_PERIODIC_ARRAY_DIAGNOGSIS_MASK  = 1\n"));
      PerformPeriodicArrayUpdate();
    }

    ///
    /// Perform periodic scan initialization
    ///
    DEBUG ((DEBUG_INFO, "Reading MSR_FUSA_CAPABILITIES_A for B_MSR_FUSA_CAP_PERIODIC_SCAN_DIAGNOGSIS_MASK..\n"));
    if ((AsmReadMsr64 (MSR_FUSA_CAPABILITIES_A) & B_MSR_FUSA_CAP_PERIODIC_SCAN_DIAGNOGSIS_MASK) ==
      B_MSR_FUSA_CAP_PERIODIC_SCAN_DIAGNOGSIS_MASK) {
      DEBUG ((DEBUG_INFO, "B_MSR_FUSA_CAP_PERIODIC_SCAN_DIAGNOGSIS_MASK  = 1\n"));
      PerformPeriodicScanUpdate();
      ExecuteFusaPeriodicScanArrayBIST(FusaConfig);
    }

    ///
    /// Perform lock step initialization
    ///
    DEBUG ((DEBUG_INFO, "Reading MSR_FUSA_CAPABILITIES_A for B_MSR_FUSA_CAP_LOCKSTEP_MODE_MASK..\n"));
    if ((AsmReadMsr64 (MSR_FUSA_CAPABILITIES_A) & B_MSR_FUSA_CAP_LOCKSTEP_MODE_MASK) ==
      B_MSR_FUSA_CAP_LOCKSTEP_MODE_MASK) {
      DEBUG ((DEBUG_INFO, "B_MSR_FUSA_CAP_LOCKSTEP_MODE_MASK  = 1\n"));
      PerformLockStepUpdate(FusaConfig);
    }

    /// Lock from OS to modify parity controls and injection egisters
    ///
    DEBUG ((DEBUG_INFO, "FusaRegisterLock\n"));
    FusaRegisterLock ();
  } else {
    DEBUG ((DEBUG_INFO, "Fusa is disabled\n"));
  }
  return EFI_SUCCESS;
}