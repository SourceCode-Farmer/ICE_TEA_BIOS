/** @file
  Internal header for CpuPcieRpInitLib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _CPU_PCIE_RP_INIT_LIB_INTERNAL_H_
#define _CPU_PCIE_RP_INIT_LIB_INTERNAL_H_

#include <Library/PeiPcieSipInitLib.h>
#include <Library/CpuSbiAccessLib.h>
#include <Library/P2SbSidebandAccessLib.h>
#include <Library/PciSegmentRegAccessLib.h>
#include <CpuPcieHob.h>

typedef struct {
  PCIE_ROOT_PORT_DEV             RpDev;
  PCI_SEGMENT_REG_ACCESS         PciSegmentRegAccess;
  P2SB_SIDEBAND_REGISTER_ACCESS  P2SbMsgCfgAccess;
  P2SB_SIDEBAND_REGISTER_ACCESS  P2SbMsgMemAccess;
} PCIE_ROOT_PORT_DEV_PRIVATE;

/**
  Create PCIE_ROOT_PORT_DEV_PRIVATE object

  @param[in]  RpIndex  Index of the root port on the CPU
  @param[out] RpDev    On output pointer to the valid PCIE_ROOT_PORT_DEV_PRIVATE
**/
VOID
CpuPcieGetRpDev (
  IN UINT32                       RpIndex,
  OUT PCIE_ROOT_PORT_DEV_PRIVATE  *RpDevPrivate
  );

/**
  Disable the CLKREQ# Messaging based on the polling of all rootport status bits

  @param[in] RpEnabledMask                 Rootport Enable mask
  @param[in] StatusBit                     CLKREQ status bit
**/
VOID
EFIAPI
CpuPcieDisableClkReqMsg (
  IN  UINT32                        RpEnabledMask,
  IN  UINT32                        StatusBit,
  IN  CPU_PCIE_HOB                  *CpuPcieHob
  );

/**
  Program the CLKREQ Assert pin

  @param[in] RpIndex                 Rootport Index
**/
VOID
EFIAPI
CpuPcieSetAssertClkReq (
  IN  UINT32                RpIndex
  );

/**
  Polling the CLKREQ# Messaging Status bits

  @param[in] RpEnabledMask        Rootport Enable mask

  @retval ClkReqPollSts           Polling status bits
**/
UINT32
CpuPciePollClkReqMsgSts (
  IN  UINT32  RpEnabledMask
  );

/**
  Train the CPU PCIe link to Gen1

  @param[in] RpIndex                 Rootport Index
**/
VOID
EFIAPI
CpuPcieTrainLinkToGen1 (
  IN  UINT32   RpIndex
  );

/**
  Check the Link Active bit for Trained CPU PCIe Ports

  @param[in] MaxCpuPciePortNum       Maximum Rootports
  @param[in] RpEnabledMask           bitmask for all enable rootports
**/
VOID
EFIAPI
CpuPcieCheckforLinkActive (
  IN  UINT8    MaxCpuPciePortNum,
  IN  UINT32   RpEnabledMask
  );
#endif

