
/** @file
  This file contains functions that initializes PCI Express Root Ports for Link training .

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/PeiCpuPciePreMemRpInitLib.h>
#include <CpuPcieHob.h>
#include <Library/CpuPcieRpLib.h>
#include <Library/PeiPchPcieClocksLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/PeiCpuPcieDekelInitLib.h>
#include <Register/CpuPcieRegs.h>
#include <Library/GpioPrivateLib.h>
#include <CpuPcieInfo.h>
#include <Library/TimerLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PeiServicesLib.h>
#include <CpuSbInfo.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <CpuRegs.h>
#include <Library/PchFiaLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuPcieInitCommon.h>
#include <Library/PeiCpuPcieHsPhyInfoLib.h>

#define LA_LOOP_COUNT           1000   // 100ms Time Delay is to set LA bit
#define PDS_LOOP_COUNT          240    // 24ms Time Delay is to set PDS bit

/**
  Limit Link Speed Based on Policy

  @param[in] RpIndex                 Rootport Index to Limit Speed
**/
VOID
EFIAPI
LimitLinkSpeed (
  IN     UINT32     RpIndex
  )
{
  CONST CPU_PCIE_RP_PREMEM_CONFIG    *CpuPcieRpPreMemConfig;
  UINT32                             Data32Or;
  UINT32                             Data32And;
  UINT64                             RpBase;
  EFI_STATUS                         Status;
  SI_PREMEM_POLICY_PPI               *SiPreMemPolicyPpi;
  UINT8                              PcieSdOffset;

  ///
  /// Get policy settings through the SaInitConfigBlock PPI
  ///
  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *)SiPreMemPolicyPpi, &gCpuPcieRpPrememConfigGuid, (VOID *)&CpuPcieRpPreMemConfig);
  ASSERT_EFI_ERROR(Status);

  RpBase = CpuPcieBase (RpIndex);
  //
  // Set speed capability in rootport
  //
  DEBUG ((DEBUG_INFO, "Set speed capability in rootport %d\n", RpIndex));
  if (GetCpuPcieSipInfo(RpIndex) == PCIE_SIP16) {
    Data32And = (~((UINT32)(B_PCIE_MPC_SIP16_PCIESD_MASK)));
    PcieSdOffset = B_PCIE_MPC_SIP16_PCIESD_OFFSET;
  } else {
    Data32And = (~((UINT32)(B_PCIE_MPC_SIP17_PCIESD_MASK)));
    PcieSdOffset = B_PCIE_MPC_SIP17_PCIESD_OFFSET;
  }
  Data32Or = 0;
  switch (CpuPcieRpPreMemConfig->PcieSpeed[RpIndex]) {
    case CpuPcieGen1:
      DEBUG((DEBUG_INFO, "PcieSpeed from policy = Gen1\n"));
      Data32Or |= (V_PCIE_MPC_PCIESD_GEN1 << PcieSdOffset);
      break;
    case CpuPcieGen2:
      DEBUG((DEBUG_INFO, "PcieSpeed from policy = Gen2\n"));
      Data32Or |= (V_PCIE_MPC_PCIESD_GEN2 << PcieSdOffset);
      break;
    case CpuPcieGen3:
      DEBUG((DEBUG_INFO, "PcieSpeed from policy = Gen3\n"));
      Data32Or |= (V_PCIE_MPC_PCIESD_GEN3 << PcieSdOffset);
      break;
    case CpuPcieGen4:
      if (GetCpuPcieSipInfo(RpIndex) == PCIE_SIP17){
        DEBUG((DEBUG_INFO, "PcieSpeed from policy = Gen4\n"));
        Data32Or |= (V_PCIE_MPC_PCIESD_GEN4 << PcieSdOffset);
      }
      break;
    case CpuPcieAuto:
      DEBUG((DEBUG_INFO, "PcieSpeed from policy = Auto\n"));
      break;
  }
  if (Data32Or != 0) {
    DEBUG((DEBUG_INFO, "PCIe Link Speed is Limited to Gen%x for port%x based on policy!!!\n", CpuPcieRpPreMemConfig->PcieSpeed[RpIndex], RpIndex+1));
    PciSegmentAndThenOr32 (RpBase + R_PCIE_MPC, Data32And, Data32Or);
  }
}

/*
  Program registers for Elastic Buffer

  @param[in] RpIndex                 Rootport Index
*/
VOID
EFIAPI
ElasticBufferProgramming (
  IN  UINT32  RpIndex
)
{
  UINT32  Data32Or;
  UINT32  Data32And;
  UINT64  RpBase;
  DEBUG((DEBUG_INFO, "ElasticBufferProgramming Start\n"));
  RpBase = CpuPcieBase(RpIndex);

  Data32And = (UINT32)~(B_PCIE_ACGR3S2_G1EBM | B_PCIE_ACGR3S2_G2EBM | B_PCIE_ACGR3S2_G3EBM | B_PCIE_ACGR3S2_G4EBM | B_PCIE_ACGR3S2_G5EBM);
  Data32Or = (UINT32)(B_PCIE_ACGR3S2_G1EBM | B_PCIE_ACGR3S2_G2EBM);

  PciSegmentAndThenOr32(RpBase + R_PCIE_ACGR3S2, Data32And, Data32Or);
  DEBUG((DEBUG_INFO, "ElasticBufferProgramming End\n"));
}

/**
  Train the CPU PCIe link to Gen1

  @param[in] RpIndex                 Rootport Index
**/
VOID
EFIAPI
CpuPcieTrainLinkToGen1 (
IN  UINT32   RpIndex
  )
{
  UINT64                RpBase;
  UINTN                 RpDevice;
  UINTN                 RpFunction;

  RpBase = CpuPcieBase (RpIndex);

  //
  // Set TLS to gen1
  //
  GetCpuPcieRpDevFun (RpIndex, &RpDevice, &RpFunction);
  DEBUG ((DEBUG_INFO, "**** BDF = 0%x%x ****\n", RpDevice, RpFunction));
  DEBUG ((DEBUG_INFO, "Set Slot Implemented Bit\n"));
  PciSegmentOr16 (RpBase + R_PCIE_XCAP, B_PCIE_XCAP_SI);
  DEBUG ((DEBUG_INFO, "Set TLS to gen1\n"));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_LCTL2, (UINT32)~(B_PCIE_LCTL2_TLS_MASK), (UINT32) V_PCIE_LCTL2_TLS_GEN1);
  //
  // Limit Link Speed Based on Policy - Should be Executed before BLKDQDASD is cleared.
  //
  LimitLinkSpeed (RpIndex);
  //
  // Downstream Config Type 1 Conversion before any downstream aceess 0xEC[15] 1
  //
  RpBase = CpuPcieBase(RpIndex);
  PciSegmentOr32 (RpBase + R_PCIE_DC, B_PCIE_DC_DCT1C);

  //
  // Local Scaled Flow Control Supported Bit should be set in Data Link Feature Capabilities Register for Ports that support Gen4
  //  Program the Scaled flow control 0xA94[0] to 1
  //
  PciSegmentOr32 (RpBase + R_PCIE_DLFCAP, B_PCIE_DLFCAP_LSFCS);
  DEBUG ((DEBUG_VERBOSE, "Local Scaled Flow Control Support Enabled\n"));
  //
  // Programming the FCUCTL Register.
  //
  if ((!IsSimicsEnvironment()) && (GetCpuPcieSipInfo(RpIndex) == PCIE_SIP17)) {
    CpuRegbarAnd32 (GetCpuPcieRpSbiPid(RpIndex), R_PCIE_FCUCTL, (UINT32)~(B_PCIE_FCUCTL_FC_CP_FCM));
  }
  ElasticBufferProgramming(RpIndex);
  if (IsSkipLinkTrainingWaRequired (RpIndex)) {
    PciSegmentOr32 (RpBase + R_PCIE_MPC, B_PCIE_MPC_FORCEDET);
  }
  //
  // Retrain link by clearing BLKDQDASD
  //
  DEBUG ((DEBUG_INFO, "Retrain link by clearing BLKDQDASD\n"));
  PciSegmentAnd32 (RpBase + R_PCIE_PCIEALC, (UINT32)~(B_PCIE_PCIEALC_BLKDQDASD));

  return;
}

/**
  Check the Link Active bit for Trained CPU PCIe Ports

  @param[in] MaxCpuPciePortNum       Maximum Rootports
  @param[in] RpEnabledMask           bitmask for all enable rootports
**/
VOID
EFIAPI
CpuPcieCheckforLinkActive (
  IN  UINT8    MaxCpuPciePortNum,
  IN  UINT32   RpEnabledMask
  )
{
  UINT64                RpBase;
  UINT32                TimeoutUs;
  UINT32                MaxTimeout;
  UINT32                PdsEnabledMask;
  UINT32                LinkActiveEnableMask;
  UINT32                Data32;
  UINT8                 RpIndex;

  PdsEnabledMask       = 0x00;
  LinkActiveEnableMask = 0x00;
  MaxTimeout           = 0;

  //
  // If PDS bit is not set, there is no need to check for LA bit for that particular RP
  // So use PDSSetMask to check for LinkActive bit
  //
  for (TimeoutUs = 0; ((PdsEnabledMask != RpEnabledMask) && (TimeoutUs < PDS_LOOP_COUNT)); TimeoutUs++) {
    for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
      RpBase = CpuPcieBase (RpIndex);
      if (RpEnabledMask & (BIT0 << RpIndex)) {
        if (PciSegmentRead16(RpBase + R_PCIE_SLSTS) & B_PCIE_SLSTS_PDS) {
          PdsEnabledMask |= (BIT0 << RpIndex);
        }
      }
    }
    if (PdsEnabledMask == RpEnabledMask) {
      break;
    }
    MicroSecondDelay (100);
  }

  DEBUG((DEBUG_INFO, "PdsEnabledMask = %x\n", PdsEnabledMask));

  MaxTimeout = LA_LOOP_COUNT - TimeoutUs;

  for (TimeoutUs = 0; ((LinkActiveEnableMask != PdsEnabledMask) && (TimeoutUs < MaxTimeout)); TimeoutUs++) {
    //DEBUG ((DEBUG_INFO, "RpIndex %d, Timeout %dus\n", RpIndex, TimeoutUs*100));
    for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
      RpBase = CpuPcieBase (RpIndex);
      if (RpEnabledMask & (BIT0 << RpIndex)) {
        if (CpuPcieIsLinkActive (RpBase)) {
          LinkActiveEnableMask |= (BIT0 << RpIndex);
        }
      }
    }
    MicroSecondDelay (100);
  }
  ///
  /// This is a spec defined delay - a hard coded 100ms before endpoint access
  ///
  if (LinkActiveEnableMask != 0) {
    MicroSecondDelay (100000);
  }
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
    RpBase = CpuPcieBase (RpIndex);
    if (RpEnabledMask & (BIT0 << RpIndex)) {
      DEBUG((DEBUG_INFO, "Target link speed = %x\n", (PciSegmentRead32(RpBase + R_PCIE_LCTL2) & B_PCIE_LCTL2_TLS_MASK)));
      Data32 = (PciSegmentRead32 (RpBase + R_PCIE_LCTL) >> 16); // Value of LSTS register
      DEBUG ((DEBUG_INFO, "Current link speed = %x\n", Data32 & B_PCIE_LSTS_CLS_MASK));
      DEBUG ((DEBUG_INFO, "Negotiated link Width = %x\n", (Data32 & B_PCIE_LSTS_NLW_MASK) >> 4));
      if (!(Data32 & B_PCIE_LSTS_LA)){
        DEBUG((DEBUG_ERROR, "Link is not active yet!!!\n"));
      }
    }
  }
  return;
}
