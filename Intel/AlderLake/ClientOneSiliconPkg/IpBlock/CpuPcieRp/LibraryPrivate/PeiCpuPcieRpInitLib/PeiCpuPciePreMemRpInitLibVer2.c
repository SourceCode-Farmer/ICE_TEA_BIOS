
/** @file
  This file contains functions that initializes PCI Express Root Ports of CPU.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/PeiCpuPciePreMemRpInitLib.h>
#include <CpuPcieHob.h>
#include <Library/CpuPcieRpLib.h>
#include <Library/PeiPchPcieClocksLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/PeiCpuPcieDekelInitLib.h>
#include <Register/CpuPcieRegs.h>
#include <Library/GpioPrivateLib.h>
#include <CpuPcieInfo.h>
#include <Library/TimerLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PeiServicesLib.h>
#include <CpuSbInfo.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <CpuRegs.h>
#include <Library/FiaSocLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuPcieInitCommon.h>
#include <Library/PeiCpuPcieHsPhyInfoLib.h>
#include "CpuPcieRpInitLibInternal.h"

/**
  Program the registers in CPU Rootport

  @param[in] RpIndex                 Rootport Index
**/
VOID
EFIAPI
CpuPcieDisableClkReqMessaging (
  IN  UINT32                RpIndex
  )
{
  UINT64 RpBase;
  RpBase = CpuPcieBase (RpIndex);
  DEBUG ((DEBUG_INFO, "CpuPcieDisableClkReqMessaging Start\n"));
  PciSegmentAnd32 (RpBase + R_PCIE_CLKREQMP, (UINT32)~B_PCIE_CLKREQMP_CKREQMPEN);
  DEBUG ((DEBUG_INFO, "CpuPcieDisableClkReqMessaging End\n"));
}

/**
  Program the CLKREQ Assert pin

  @param[in] RpIndex                 Rootport Index
**/
VOID
EFIAPI
CpuPcieSetAssertClkReq (
  IN  UINT32                RpIndex
  )
{
  UINT64 RpBase;
  RpBase = CpuPcieBase (RpIndex);
  DEBUG ((DEBUG_INFO, "CpuPcieSetAssertClkReq Start\n"));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_CLKREQMP, (UINT32)~B_PCIE_CLKREQMP_ASRTCLKREQP, B_PCIE_CLKREQMP_ASRTCLKREQP);
  DEBUG ((DEBUG_INFO, "CpuPcieSetAssertClkReq End\n"));
}

/**
  Program the registers in CPU Rootport

  @param[in] RpIndex                 Rootport Index
**/
VOID
EFIAPI
CpuPcieProgramClkReqMsgProtocolReg (
  IN  UINT32                RpIndex
  )
{
  UINT64       RpBase;
  UINT32       Data32Or;
  UINT32       Data32And;
  VWM_MAPPING  *VwmMapping;

  VwmMapping = GetVwmMapping(RpIndex);

  DEBUG ((DEBUG_INFO, "CpuPcieProgramClkReqMsgProtocolReg Start\n"));
  Data32And = (UINT32) ~B_PCIE_ACGR3S2_CLKREQMSGTXVLMDESTID_MASK;
  Data32Or = VwmMapping->TxVlmDestId << B_PCIE_ACGR3S2_CLKREQMSGTXVLMDESTID_OFFSET;
  RpBase = CpuPcieBase (RpIndex);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_ACGR3S2, Data32And, Data32Or);

  PciSegmentOr32 (RpBase + R_PCIE_CLKREQMP, (B_PCIE_CLKREQMP_CLKREQMSGDL2DEBM | B_PCIE_CLKREQMP_CLKREQMSGL1EBM));

  Data32Or = ((VwmMapping->RxVlmI << B_PCIE_CLKREQMP_CLKREQMSGRXVLMI_OFFSET) | (VwmMapping->TxVlmI1 << B_PCIE_CLKREQMP_CLKREQMSGTXVLMI1_OFFSET) | (VwmMapping->TxVlmI0 << B_PCIE_CLKREQMP_CLKREQMSGTXVLMI0_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_CLKREQMP, (UINT32)~(B_PCIE_CLKREQMP_CLKREQMSGRXVLMI_MASK | B_PCIE_CLKREQMP_CLKREQMSGTXVLMI1_MASK | B_PCIE_CLKREQMP_CLKREQMSGTXVLMI0_MASK), Data32Or);
  ///
  ///
  Data32And = ~(UINT32)(B_PCIE_AECR1G3_CRMTDDE | B_PCIE_AECR1G3_L1OFFRDYHEWT_MASK);
  Data32Or = B_PCIE_AECR1G3_L1OFFRDYHEWTEN | (V_PCIE_AECR1G3_L1OFFRDYHEWT << B_PCIE_AECR1G3_L1OFFRDYHEWT_OFFSET) | B_PCIE_AECR1G3_TPSE | B_PCIE_AECR1G3_DTCGCM;
  PciSegmentAndThenOr32 (RpBase + R_PCIE_AECR1G3, Data32And, Data32Or);

  DEBUG ((DEBUG_INFO, "CpuPcieProgramClkReqMsgProtocolReg End\n"));
}

/**
  Assign and Enable the CLKREQ# Messaging

  @param[in] RpEnabledMask                 Rootport Enable mask
**/
VOID
EFIAPI
CpuPcieEnableClkReqMsg (
  IN  UINT32                        RpEnabledMask
  )
{
  UINT32        RpIndex;
  UINT32        MaxCpuPciePortNum;

  DEBUG ((DEBUG_INFO, "CpuPcieEnableClkReqMsg Start\n"));

  MaxCpuPciePortNum = GetMaxCpuPciePortNum();
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
    if (RpEnabledMask & (BIT0 << RpIndex)) {
      DEBUG ((DEBUG_INFO, "Configure CLKREQ# GPIO\n"));
      PchConfigureCpuPciePortClkReqMapping (RpIndex);
      DEBUG ((DEBUG_INFO, "Program the registers in CPU Root port\n"));
      CpuPcieProgramClkReqMsgProtocolReg (RpIndex); // Program the registers in CPU Root port
      PchEnableCpuPciePortClkReqMapping(RpIndex);
    }
  }
  DEBUG ((DEBUG_INFO, "CpuPcieEnableClkReqMsg End\n"));
}

/**
  Polling the CLKREQ# Messaging Status bits

  @param[in] RpEnabledMask        Rootport Enable mask

  @retval ClkReqPollSts           Polling status bits
**/
UINT32
CpuPciePollClkReqMsgSts (
  IN  UINT32  RpEnabledMask
  )
{
  UINT32   Timeout;
  UINT32   RpIndex;
  UINT32   ClkReqAssertedMask;
  UINT32   MaxCpuPciePortNum;

  ClkReqAssertedMask = 0;
  MaxCpuPciePortNum = GetMaxCpuPciePortNum();
  Timeout = 0;

  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
    if (RpEnabledMask & (BIT0 << RpIndex)) {
      for (Timeout = 0; Timeout < 1000; Timeout++) {
        if (PchGetCpuPcieClkReqStatus (RpIndex)) {
          ClkReqAssertedMask |= (BIT0 << RpIndex);
          if (ClkReqAssertedMask == RpEnabledMask) {
            DEBUG ((DEBUG_INFO, "RpEnabledMask is equal to ClkReqAssertedMask = %x \n", ClkReqAssertedMask));
            return ClkReqAssertedMask;
          } else {
            MicroSecondDelay (1);
            Timeout++;
          }
        }
      }
    }
  }

  DEBUG ((DEBUG_INFO, "ClkReqAssertedMask = %x \n", ClkReqAssertedMask));
  return ClkReqAssertedMask;
}

/**
  Disable the CLKREQ# Messaging based on the polling of all rootport status bits

  @param[in] RpEnabledMask                 Rootport Enable mask
  @param[in] StatusBit                     CLKREQ status bit
**/
VOID
EFIAPI
CpuPcieDisableClkReqMsg (
  IN  UINT32                        RpEnabledMask,
  IN  UINT32                        StatusBit,
  IN  CPU_PCIE_HOB                  *CpuPcieHob
  )
{
  UINT32        RpIndex;
  DEBUG ((DEBUG_INFO, "CpuPcieDisableClkReqMsg Start\n"));
  DEBUG ((DEBUG_INFO, "Disable the CLKREQ# Messaging based on the polling\n"));
  for (RpIndex = 0; RpIndex < GetMaxCpuPciePortNum(); RpIndex++) {
    if ((RpEnabledMask - StatusBit) & (BIT0 << RpIndex)) {
      PchDisableCpuPciePortClkReq (RpIndex);
      DEBUG ((DEBUG_INFO, "L1SubStates should not be enabled if CLKREQ# messaging is not enabled\n"));
      CpuPcieHob->L1SubStates[RpIndex] = 0; //L1SubStates should not be enabled if CLKREQ# messaging is not enabled
      CpuPcieHob->DisableClkReqMsg[RpIndex] = 1; //Indicates if CLKREQ# messaging is enabled or not
      DEBUG ((DEBUG_INFO, "Disable CLKREQ# messaging in Cpu Pcie RP %d\n", RpIndex));
      CpuPcieDisableClkReqMessaging (RpIndex); // Disable CLKREQ# messaging in Cpu Pcie RP
    }
  }
  DEBUG ((DEBUG_INFO, "CpuPcieDisableClkReqMsg End\n"));
}
/**
  Assigns CLKREQ# to CPU PCIe ports

  @param[in] RpEnabledMask                 Rootport Enable mask
  @param[in] CPU_PCIE_RP_PREMEM_CONFIG      CpuPcieRpPreMemConfig to access the SA PCIe Pre Mem configuration
**/
VOID
EFIAPI
CpuPcieConfigureClockReqMessaging (
  IN  UINT32                        RpEnabledMask,
  IN  CPU_PCIE_RP_PREMEM_CONFIG     *CpuPcieRpPreMemConfig,
  IN  CPU_PCIE_HOB                  *CpuPcieHob,
  IN  UINT32                        RpIndex
  )
{
  DEBUG ((DEBUG_INFO, "CPU PCIE Clock Req configuration Start\n"));

  DEBUG ((DEBUG_INFO, "Keeping L1SubStates enable by default for RpIndex %x\n", RpIndex));
  CpuPcieHob->L1SubStates[RpIndex] = 1; // Keeping L1SubStates enable by default.
  CpuPcieHob->DisableClkReqMsg[RpIndex] = 0;// Keeping CLKREQ message enable by default
  CpuPcieEnableClkReqMsg (RpEnabledMask);

  DEBUG ((DEBUG_INFO, "CPU PCIE Clock Req configuration End\n"));
}

/**
  Assertion of GPIOs on Link Down. Upon Link Down detection VWM message will be send to assert GPIO.

  @param[in] RpIndex                 Rootport Index
**/
VOID
EFIAPI
CpuPcieRpLinkDownGpios (
  IN  UINT32                RpIndex
  )
{
  UINT64  RpBase;
  UINT32  Data32Or;

  Data32Or = 0;

  DEBUG ((DEBUG_INFO, "Configure GPIOs for Link down Start\n"));

  RpBase = CpuPcieBase (RpIndex);
  Data32Or = (CPU_PCIE_CLKREQMSGTXVLMDESTID << B_PCIE_GDR_LDVWMDESTID_OFFSET) | (CPU_PCIE_GDR_LDVWMIDX << B_PCIE_GDR_LDVWMIDX_OFFSET) | (B_PCIE_GDR_VGPIOALDE) | (B_PCIE_GDR_GPIOALD);

    switch (RpIndex) {
      case 0:
        Data32Or |= 3 << B_PCIE_GDR_LDVWMBITLOC_OFFSET;
        break;
      case 1:
        Data32Or |= 0 << B_PCIE_GDR_LDVWMBITLOC_OFFSET;
        break;
      case 2:
        Data32Or |= 1 << B_PCIE_GDR_LDVWMBITLOC_OFFSET;
        break;
      case 3:
        Data32Or |= 2 << B_PCIE_GDR_LDVWMBITLOC_OFFSET;
        break;
      default:
        ASSERT (FALSE);
        return;
    }
  DEBUG ((DEBUG_INFO, "Value written to GDR register = %x\n", Data32Or));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_GDR, (UINT32)~(B_PCIE_ACGR3S2_CLKREQMSGTXVLMDESTID_MASK | B_PCIE_GDR_LDVWMBITLOC_MASK | B_PCIE_GDR_LDVWMIDX_MASK | B_PCIE_GDR_VGPIOALDE), Data32Or);
  DEBUG ((DEBUG_INFO, "Value read back from GDR register = %x\n", PciSegmentRead32 (RpBase + R_PCIE_GDR)));

  DEBUG ((DEBUG_INFO, "Configure GPIOs for Link down End\n"));
}

/**
  Create Root Port Enabled mask by reading the DEVEN register

  @param[out] RpEnabledMask                 Rootport enabled mask based on DEVEN register
**/

UINT32
EFIAPI
GetRpEnMaskfromDevEn (
  )
{
  UINT32                 RpEnabledMask;
  UINT32                 DevEnable;

  RpEnabledMask = 0;

  //
  // Check DEVEN register
  //
  DEBUG((DEBUG_INFO, "Read DEVEN register\n"));
  DevEnable = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DEVEN));
  DEBUG((DEBUG_INFO, "DevEnable = %x\n",DevEnable));
  if (((DevEnable & B_SA_DEVEN_D6EN_MASK) == B_SA_DEVEN_D6EN_MASK)) {
    RpEnabledMask  |= BIT0;
  }
  if ((DevEnable & B_SA_DEVEN_D1F0EN_MASK) == B_SA_DEVEN_D1F0EN_MASK) {
    RpEnabledMask  |= BIT1;
  }
  if (((DevEnable &  B_SA_DEVEN_D1F1EN_MASK) ==  B_SA_DEVEN_D1F1EN_MASK) ||
    ((DevEnable & B_SA_DEVEN_D6F2EN_MASK) == B_SA_DEVEN_D6F2EN_MASK)) {
    RpEnabledMask  |= BIT2;
  }
  if ((DevEnable & B_SA_DEVEN_D1F2EN_MASK) == B_SA_DEVEN_D1F2EN_MASK) {
    RpEnabledMask  |= BIT3;
  }
  DEBUG((DEBUG_INFO, "RpEnabledMask formed from DEVEN = %x\n", RpEnabledMask));
  return RpEnabledMask;
}

/**
  Get the CPU PCIe enable root ports from straps fuse config

**/
UINT32
EFIAPI
GetRpEnMaskfromRpc (
  VOID
  )
{
  UINT32                 RpEnabledMask;

  RpEnabledMask  = 0x0F; //assuming all Root ports are enabled

  //
  // Read STRPFUSECFG to get the furcation logic
  //
  switch (GetCpuPcieControllerConfig ()) {
    case CPU_PCIE_1x16: //Pcie1x16
      RpEnabledMask &= ~(BIT1 | BIT2 | BIT3);
      RpEnabledMask |= BIT1;
      break;
    case CPU_PCIE_2x8: //Pcie2x8
      RpEnabledMask &= ~(BIT1 | BIT2 | BIT3);
      RpEnabledMask |= (BIT1 | BIT2);
      break;
    case CPU_PCIE_1x8_2x4: //Pcie1x8_2x4
      RpEnabledMask |= (BIT1 | BIT2 | BIT3);
      break;
    case CPU_PCIE_1x4: //Pcie1x4
      RpEnabledMask |= BIT0;
      break;
    default:
      ASSERT(FALSE);
  }
  DEBUG((DEBUG_INFO, "RpEnabledMask formed after controllerconfig = %x\n", RpEnabledMask));
  return RpEnabledMask;
}

/**
  Initialize the CPU PciExpress in PEI

  @param[in] IN PCIE_PEI_PREMEM_CONFIG    PciePeiPreMemConfig to access the PCIe Config related information
**/
VOID
EFIAPI
PciExpressInit (
  IN  CPU_PCIE_RP_PREMEM_CONFIG     *CpuPcieRpPreMemConfig
  )
{
  EFI_STATUS            Status;
  UINT32                RpEnabledMask;
  UINT32                TempRpEnabledMask;
  UINT32                MaxCpuPciePortNum;
  UINT32                RpIndex;
  CPU_PCIE_HOB          *CpuPcieHob;
  UINT32                RpEnMaskFromPolicy;
  UINT32                RpEnMaskFromRpc;
  UINT32                RpEnMaskFromDevEn;
  UINT32                StatusBit;
  UINT32                CpuPcieDekelMap;
  UINT64                RpBase;

  RpEnabledMask         = 0x00; //assuming all Root ports are disabled
  RpEnMaskFromPolicy    = 0x00;
  RpEnMaskFromDevEn     = 0x00;
  RpEnMaskFromRpc       = 0x00;
  CpuPcieDekelMap       = 0;

  MaxCpuPciePortNum = GetMaxCpuPciePortNum ();

  CpuPcieHob = NULL;
  ///
  /// Create HOB for CPU PCIE
  ///
  Status = PeiServicesCreateHob (
             EFI_HOB_TYPE_GUID_EXTENSION,
             sizeof (CPU_PCIE_HOB),
             (VOID **) &CpuPcieHob
             );
  DEBUG ((DEBUG_INFO, "CpuPcieHob Created \n"));
  ASSERT_EFI_ERROR (Status);

  if (CpuPcieHob != NULL) {
    //
    // Initialize the CPU PCIE HOB.
    //
    CpuPcieHob->EfiHobGuidType.Name = gCpuPcieHobGuid;
    DEBUG ((DEBUG_INFO, "CpuPcieHob->EfiHobGuidType.Name: %g\n", &CpuPcieHob->EfiHobGuidType.Name));
    DEBUG((DEBUG_INFO, "CpuPcieHob @ %X\n", CpuPcieHob));
    DEBUG ((DEBUG_INFO, "CpuPcieHob - HobHeader: %X\n", sizeof (CPU_PCIE_HOB) - sizeof (EFI_HOB_GUID_TYPE)));
  }

  DEBUG((DEBUG_INFO, "Initialize CPU PciExpress controllers in Premem : PciExpressInit Start\n"));
  DEBUG((DEBUG_INFO, "Maximum Cpu Pcie Ports = %x\n", MaxCpuPciePortNum));
  if (CpuPcieHob == NULL) {
    DEBUG((DEBUG_ERROR, "CpuPcieHob not found\n"));
    ASSERT(CpuPcieHob == NULL);
    return;
  }

  //
  //  Get RpEnabled mask from Policy
  //
  DEBUG ((DEBUG_INFO, "RpEnabledMask from Policy = %x\n", CpuPcieRpPreMemConfig->RpEnabledMask));
  if (CpuPcieRpPreMemConfig->RpEnabledMask != 0) {
    RpEnMaskFromPolicy = CpuPcieRpPreMemConfig->RpEnabledMask;
  }

  //
  //  Check for enabled Ports using Device ID
  //
  RpEnMaskFromDevEn = GetRpEnMaskfromDevEn();

  RpEnabledMask = (RpEnMaskFromPolicy & RpEnMaskFromDevEn);
  //
  // Read RPC to identify the furcation logic - applicable only for DT_HALO
  //
  if (GetCpuSku() == EnumCpuTrad || GetCpuSku() == EnumCpuHalo) {
    RpEnMaskFromRpc = GetRpEnMaskfromRpc ();
    RpEnabledMask &= RpEnMaskFromRpc;
  }

  if (CpuPcieHob != NULL) {
    CpuPcieHob->RpEnabledMask = RpEnabledMask;
    CpuPcieHob->RpEnMaskFromDevEn = RpEnMaskFromDevEn;
    CpuPcieHob->HsPhyMap = CpuPcieGetHsPhyPortMap() & CpuPcieHob->RpEnabledMask;
  }
  DEBUG ((DEBUG_INFO, "CpuPcieHob->RpEnabledMask formed after reading DID/setup and controllerconfig = %x\n", CpuPcieHob->RpEnabledMask));

  if (RpEnabledMask == 0) {
    DEBUG((DEBUG_ERROR, " All CPU PCIe root ports are disabled!!!\n"));
    return;
  }

  //
  // Prior to initiating link training, BIOS should program PHY configuration registers.
  // Call DEKEL init function to configure the PHY registers
  //
    for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
      if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP16) {
        DEBUG ((DEBUG_INFO, "Call DEKEL init function to configure the PHY registers for Root Port %d\n", RpIndex));
        if (!IsSimicsEnvironment()) {
          DekelInit (RpIndex, CpuPcieRpPreMemConfig);
        }
      }
    }


  //
  // Enable CLKREQ# messaging flow
  //
  TempRpEnabledMask = RpEnabledMask;
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
    CpuPcieHob->L1SubStates[RpIndex] = 0; // Keeping L1SubStates disable by default.
    CpuPcieHob->DisableClkReqMsg[RpIndex] = 1;// Keeping CLKREQ message disable by default
    if (CpuPcieRpPreMemConfig->ClkReqMsgEnableRp[RpIndex] != 0) {
      TempRpEnabledMask = (RpEnabledMask & (BIT0 << RpIndex));
      DEBUG ((DEBUG_INFO, "ClockReq Messaging Enable for RpIndex %d, RpEnabledMask %x\n", RpIndex, TempRpEnabledMask));
      CpuPcieConfigureClockReqMessaging (TempRpEnabledMask, CpuPcieRpPreMemConfig, CpuPcieHob, RpIndex);
      if (RpEnabledMask & (BIT0 << RpIndex)) {
        DEBUG((DEBUG_INFO, "Program the CLKREQ ASSERT pin\n"));
        CpuPcieSetAssertClkReq(RpIndex); // Program the CLKREQ ASSERT pin
      }
      StatusBit = CpuPciePollClkReqMsgSts(TempRpEnabledMask);
      DEBUG((DEBUG_INFO, "CpuPciePollClkReqMsgSts StatusBit = %x\n", StatusBit));
      if ((TempRpEnabledMask & StatusBit) != 0) {
        CpuPcieHob->L1SubStates[RpIndex] = 1; //L1SubStates Enable
        CpuPcieHob->DisableClkReqMsg[RpIndex] = 0;//CLKREQ message Enable
        DEBUG((DEBUG_INFO, "CpuPciePollClkReqMsgSts success, so Enable CLKREQ# MP bit\n"));
        RpBase = CpuPcieBase(RpIndex);
        PciSegmentOr32 (RpBase + R_PCIE_CLKREQMP, B_PCIE_CLKREQMP_CKREQMPEN);
        PegPciSegmentOr32 (RpIndex, R_PCIE_PCIEPMECTL, B_PCIE_PCIEPMECTL_DLSULPPGE);
      }
    }
  }
  //
  // Clock output delay must be configured before enabling CLKREQ messaging flow
  //
  DEBUG((DEBUG_ERROR, " Before PchFiaSetClockOutputDelay!!!\n"));

  PchFiaSetClockOutputDelay();

  //
  // Assertion of GPIOs on Link Down - this is a debug feature, if any of the links go down, a GPIO is asserted to indicate the failure
  //
  if (CpuPcieRpPreMemConfig->LinkDownGpios != 0) {
    DEBUG ((DEBUG_INFO, "Assertion of GPIOs on Link Down\n"));
    for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
      if (RpEnabledMask & (BIT0 << RpIndex)) {
        CpuPcieRpLinkDownGpios (RpIndex);
      }
    }
  }

  //
  // Train the link to Gen1
  //
  CpuPcieDekelMap = CpuPcieGetDekelPortMap () & RpEnabledMask;
  DEBUG((DEBUG_INFO, "CpuPcieDekelMap = %d\n", CpuPcieDekelMap));
  for (RpIndex = 0; RpIndex < GetMaxCpuPciePortNum (); RpIndex++) {
    if (CpuPcieDekelMap & (BIT0 << RpIndex)) {
      CpuPcieTrainLinkToGen1 (RpIndex);
    }
  }
  
  DEBUG((DEBUG_INFO, "**** Premem PciExpressInit End ****\n"));
  return;
}


