/** @file
  This file contains functions needed to create PCIE_ROOT_PORT_DEV structure.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Register/PchRegs.h>
#include <Library/DebugLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/CpuPcieRpLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/CpuPcieInitCommon.h>
#include <Library/PciSegmentRegAccessLib.h>

#include "CpuPcieRpInitLibInternal.h"

/**
  Create PCIE_ROOT_PORT_DEV_PRIVATE object

  @param[in]  RpIndex  Index of the root port on the CPU
  @param[out] RpDev    On output pointer to the valid PCIE_ROOT_PORT_DEV_PRIVATE
**/
VOID
CpuPcieGetRpDev (
  IN UINT32                       RpIndex,
  OUT PCIE_ROOT_PORT_DEV_PRIVATE  *RpDevPrivate
  )
{
  UINTN               RpDevice;
  UINTN               RpFunction;
  PCIE_ROOT_PORT_DEV  *RpDev;
  UINT64              RpSegmentBase;
  UINT16              Fid;

  if (RpDevPrivate == NULL) {
    DEBUG ((DEBUG_ERROR, "RpDevPrivate can't be NULL\n"));
    return;
  }

  RpDev = &RpDevPrivate->RpDev;

  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
    RpDev->SipVersion = PcieSip17;
  } else {
    RpDev->SipVersion = PcieSip16;
  }

  RpDev->Integration = CpuPcie;

  GetCpuPcieRpDevFun (RpIndex, &RpDevice, &RpFunction);
  RpDev->Sbdf.Segment = SA_SEG_NUM;
  RpDev->Sbdf.Bus = SA_MC_BUS;
  RpDev->Sbdf.Device = (UINT16)RpDevice;
  RpDev->Sbdf.Function = (UINT16)RpFunction;
  RpSegmentBase = PCI_SEGMENT_LIB_ADDRESS (
                    SA_SEG_NUM,
                    SA_MC_BUS,
                    RpDevice,
                    RpFunction,
                    0
                    );
  BuildPciSegmentAccess (RpSegmentBase, &RpDevPrivate->PciSegmentRegAccess);
  RpDev->PciCfgAccess = &RpDevPrivate->PciSegmentRegAccess.RegAccess;

  // Fid = 1 only for PEG12 which according to our design will always be RpIndex 3
  if(RpIndex == 3) {
    Fid = 1;
  } else {
    Fid = 0;
  }
  BuildCpuSbiAccess (GetCpuPcieRpSbiPid (RpIndex), Fid, P2SbPciConfig, P2SbMsgAccess, &RpDevPrivate->P2SbMsgMemAccess);
  RpDev->PciSbiMsgCfgAccess = &RpDevPrivate->P2SbMsgCfgAccess.Access;

  BuildCpuSbiAccess (GetCpuPcieRpSbiPid (RpIndex), Fid, P2SbMemory, P2SbMsgAccess, &RpDevPrivate->P2SbMsgMemAccess);
  RpDev->PciSbiMsgMemAccess = &RpDevPrivate->P2SbMsgMemAccess.Access;
}

