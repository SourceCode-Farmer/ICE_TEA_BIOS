/** @file
  This file contains functions that initializes PCI Express Root Ports of CPU PCIe.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HobLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/TimerLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/CpuPcieExpressHelpersLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PsfLib.h>
#include <IndustryStandard/Pci30.h>
#include <Library/PeiPchPcieClocksLib.h>
#include <Library/GpioLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/CpuPcieRpLib.h>
#include <Library/PeiCpuPcieVgaInitLib.h>
#include <Library/PeiPcieRpLib.h>
#include <Library/PcdLib.h>
#include <Library/PchPcrLib.h>
#include <Uefi/UefiBaseType.h>
#include <PcieRegs.h>
#include <Register/CpuPcieRegs.h>
#include <CpuPcieHob.h>
#include <Library/PciLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/CpuPcieInitCommon.h>
#include <Library/PeiCpuPcieSip16InitLib.h>
#include <Library/PeiCpuPcieSip17InitLib.h>
#include <Library/PeiCpuPcieDekelInitLib.h>
#include <Library/PciExpressHelpersLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/PcieHelperLib.h>
#include <Library/CpuRegbarAccessLib.h>
#include <PciePreMemConfig.h>
#include <CpuSbInfo.h>
#include <Register/PchFiaRegs.h>
#include <Library/CpuPcieRpLib.h>
#include <Library/PeiCpuPcieHsPhyInfoLib.h>
#include "CpuPcieRpInitLibInternal.h"
#include <Library/CpuMailboxLib.h>
#include <Library/MeInitLib.h>

GLOBAL_REMOVE_IF_UNREFERENCED PCIE_EQ_PARAM               mCpuPcieEqParamList[PCIE_HWEQ_COEFFS_MAX][2];
GLOBAL_REMOVE_IF_UNREFERENCED UINT32                      List[12];
GLOBAL_REMOVE_IF_UNREFERENCED UINT32                      mDoGen3HwEqRpMask = 0;
GLOBAL_REMOVE_IF_UNREFERENCED UINT32                      mDoGen4HwEqRpMask = 0;
GLOBAL_REMOVE_IF_UNREFERENCED UINT32                      mDoGen5HwEqRpMask = 0;
GLOBAL_REMOVE_IF_UNREFERENCED CPU_PCIE_CONFIG             *mCpuPcieRpConfig  = NULL;

#define LTSSM_POLL_INTERVAL       10u // in microseconds, period for polling port state during SW EQ
#define RECOVERY_TIME_THRESHOLD   40  // in percent, how much time can SW EQ spend in recovery during a single step

#define LINK_ACTIVE_POLL_INTERVAL 100     // in microseconds
#define LINK_ACTIVE_POLL_TIMEOUT  1000000 // in microseconds

#define HWEQ_DONE_POLL_INTERVAL 100     // in microseconds
#define HWEQ_DONE_POLL_TIMEOUT  312000  // in microseconds

#define BUS_NUMBER_FOR_IMR 0x00

/**
  Device information structure
**/
typedef struct {
  UINT16  Vid;
  UINT16  Did;
  UINT8   MaxLinkSpeed;
} CPU_PCIE_DEVICE_INFO;

//
// Initialize PCIE device information structure with default values
//                                    VID    DID   MLS
//
CPU_PCIE_DEVICE_INFO  DevInfo[] = {{0xFFFF, 0xFFFF, 0},
                                   {0xFFFF, 0xFFFF, 0},
                                   {0xFFFF, 0xFFFF, 0}};

/**
  This function checks if end point supports L1 SubStates.

  @param[in] RpIndex  Root Port Number (0-based)
  @retval    TRUE     If end point supports L1SS
             FALSE    If end point does not supports L1SS
**/
BOOLEAN
IsEndPointSupportsL1ss (
  IN  UINT32 RpIndex
  );

/**
  This function checks if Gen3 HW Equalization method supported.

  @param[in] RpConfig  Cpu Pcie Root Port Configuration
  @retval    TRUE      If Gen3 Equlization method supported
             FALSE     If Gen3 Equlization method not supported
**/

BOOLEAN
IsGen3EqMethodSupported (
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG   *RpConfig
)
{
  if ((RpConfig->PcieRpCommonConfig.Gen3EqPh3Method == CpuPcieEqHardware) || (RpConfig->PcieRpCommonConfig.Gen3EqPh3Method == CpuPcieEqDefault)) {
    return TRUE;
  }
  return FALSE;
}

/**
  This function checks if Gen3 Speed supported.

  @param[in] RpIndex                Root Port Index
  @param[in] RpConfig               Cpu Pcie Root Port Configuration
  @param[in] CpuPcieRpPreMemConfig  Cpu Pcie Root Port Pre-Memory Configuration
  @retval    TRUE                   If Gen3 Speed supported
             FALSE                  If Gen3 Speed not supported
**/

BOOLEAN
IsGen3SpeedSupported (
  IN UINT8                             RpIndex,
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG   *RpConfig,
  IN BOOLEAN                           KeepPortVisible,
  IN CPU_PCIE_RP_PREMEM_CONFIG         *CpuPcieRpPreMemConfig
  )
{
  IN UINT64        RpBase;
  RpBase = CpuPcieBase (RpIndex);
  if ((CpuPcieRpPreMemConfig->PcieSpeed[RpIndex] >= 3) || (CpuPcieRpPreMemConfig->PcieSpeed[RpIndex] == 0)) {
    if (RpConfig->PcieRpCommonConfig.HotPlug || KeepPortVisible) {
      if (CpuPcieGetMaxLinkSpeed (RpBase) >= V_PCIE_LCAP_MLS_GEN3) {
        return TRUE;
      }
    } else {
      if ((CpuPcieGetMaxLinkSpeed (RpBase) >= V_PCIE_LCAP_MLS_GEN3) && (DevInfo[RpIndex].MaxLinkSpeed >= V_PCIE_LCAP_MLS_GEN3)) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
  This function checks if Gen4 Speed supported.

  @param[in] RpIndex                Root Port Index
  @param[in] RpConfig               Cpu Pcie Root Port Configuration
  @param[in] CpuPcieRpPreMemConfig  Cpu Pcie Root Port Pre-Memory Configuration
  @retval    TRUE                   If Gen4 Speed supported
             FALSE                  If Gen4 Speed not supported
**/

BOOLEAN
IsGen4SpeedSupported (
  IN UINT8                             RpIndex,
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG   *RpConfig,
  IN BOOLEAN                           KeepPortVisible,
  IN CPU_PCIE_RP_PREMEM_CONFIG         *CpuPcieRpPreMemConfig
  )
{
  IN UINT64        RpBase;
  RpBase = CpuPcieBase (RpIndex);
  if ((CpuPcieRpPreMemConfig->PcieSpeed[RpIndex] >= 4) || (CpuPcieRpPreMemConfig->PcieSpeed[RpIndex] == 0)) {
    if (RpConfig->PcieRpCommonConfig.HotPlug || KeepPortVisible) {
      if (CpuPcieGetMaxLinkSpeed (RpBase) >= V_PCIE_LCAP_MLS_GEN4) {
        return TRUE;
      }
    } else {
      if ((CpuPcieGetMaxLinkSpeed (RpBase) >= V_PCIE_LCAP_MLS_GEN4) && (DevInfo[RpIndex].MaxLinkSpeed >= V_PCIE_LCAP_MLS_GEN4)) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
  This function checks if Gen5 Speed supported.

  @param[in] RpIndex                Root Port Index
  @param[in] CpuPcieRpPreMemConfig  Cpu Pcie Root Port Pre-Memory Configuration
  @retval    TRUE                   If Gen5 Speed supported
             FALSE                  If Gen5 Speed not supported
**/

BOOLEAN
IsGen5SpeedSupported (
  IN UINT8                             RpIndex,
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG   *RpConfig,
  IN BOOLEAN                           KeepPortVisible,
  IN CPU_PCIE_RP_PREMEM_CONFIG         *CpuPcieRpPreMemConfig
  )
{
  IN UINT64        RpBase;
  RpBase = CpuPcieBase (RpIndex);
  if ((CpuPcieRpPreMemConfig->PcieSpeed[RpIndex] >= 5) || (CpuPcieRpPreMemConfig->PcieSpeed[RpIndex] == 0)) {
    if (RpConfig->PcieRpCommonConfig.HotPlug || KeepPortVisible) {
      if (CpuPcieGetMaxLinkSpeed (RpBase) >= V_PCIE_LCAP_MLS_GEN5) {
        return TRUE;
      }
    } else {
      if ((CpuPcieGetMaxLinkSpeed (RpBase) >= V_PCIE_LCAP_MLS_GEN5) && (DevInfo[RpIndex].MaxLinkSpeed >= V_PCIE_LCAP_MLS_GEN5)) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
  This function checks if Gen3 HW Equalization supported.

  @param[in] RpIndex                Root Port Index
  @param[in] RpConfig               Cpu Pcie Root Port Configuration
  @param[in] CpuPcieRpPreMemConfig  Cpu Pcie Root Port Pre-Memory Configuration
  @retval    TRUE                   If Gen3 Speed supported
             FALSE                  If Gen3 Speed not supported
**/

BOOLEAN
IsGen3HwEqAllowed (
  IN UINT8                             RpIndex,
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG   *RpConfig,
  IN BOOLEAN                           KeepPortVisible,
  IN CPU_PCIE_RP_PREMEM_CONFIG         *CpuPcieRpPreMemConfig
  )
{
  if (mCpuPcieRpConfig->PcieCommonConfig.ComplianceTestMode) {
    return TRUE;
  } else if (IsGen3EqMethodSupported (RpConfig) && IsGen3SpeedSupported (RpIndex, RpConfig, KeepPortVisible, CpuPcieRpPreMemConfig)) {
    return TRUE;
  }
  return FALSE;
}

/**
  This function checks if Gen4 HW Equalization supported.

  @param[in] RpIndex                Root Port Index
  @param[in] RpConfig               Cpu Pcie Root Port Configuration
  @param[in] CpuPcieRpPreMemConfig  Cpu Pcie Root Port Pre-Memory Configuration
  @retval    TRUE                   If Gen4 Speed supported
             FALSE                  If Gen4 Speed not supported
**/

BOOLEAN
IsGen4HwEqAllowed (
  IN UINT8                             RpIndex,
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG   *RpConfig,
  IN BOOLEAN                           KeepPortVisible,
  IN CPU_PCIE_RP_PREMEM_CONFIG         *CpuPcieRpPreMemConfig
  )
{
  if (mCpuPcieRpConfig->PcieCommonConfig.ComplianceTestMode) {
    return TRUE;
  } else if (IsGen4SpeedSupported (RpIndex, RpConfig, KeepPortVisible, CpuPcieRpPreMemConfig)) {
    return TRUE;
  }
  return FALSE;
}

/**
  This function checks if Gen5 HW Equalization supported.

  @param[in] RpIndex                Root Port Index
  @param[in] CpuPcieRpPreMemConfig  Cpu Pcie Root Port Pre-Memory Configuration
  @retval    TRUE                   If Gen5 Speed supported
             FALSE                  If Gen5 Speed not supported
**/

BOOLEAN
IsGen5HwEqAllowed (
  IN UINT8                             RpIndex,
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG   *RpConfig,
  IN BOOLEAN                            KeepPortVisible,
  IN CPU_PCIE_RP_PREMEM_CONFIG         *CpuPcieRpPreMemConfig
  )
{
  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
    if (mCpuPcieRpConfig->PcieCommonConfig.ComplianceTestMode) {
      return TRUE;
    }
    else if (IsGen5SpeedSupported(RpIndex, RpConfig, KeepPortVisible, CpuPcieRpPreMemConfig)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
  Programs Isolated Memory Region feature.
  IMR is programmed in a CPU PCIe rootport, based on data retrieved from CPU registers.

  @param[in] RpIndex     Root Port Number (0-based)
**/
VOID
CpuPcieEnablePcieImr (
  UINT8 RpIndex
  )
{
  UINT32                         ImrBaseLow;
  UINT32                         ImrBaseHigh;
  UINT32                         ImrLimitLow;
  UINT32                         ImrLimitHigh;
  UINT64                         ImrLimit;
  PCI_IMR_HOB                    *PciImrHob;
  UINT32                         Data32;

  DEBUG ((DEBUG_INFO, "EnablePcieImr: RP %d\n", RpIndex));

  if (RpIndex > GetMaxCpuPciePortNum ()){
    DEBUG ((DEBUG_INFO, "Invalid Root Port Selection\n"));
    return;
  }

  PciImrHob = NULL;
  PciImrHob = GetFirstGuidHob (&gPciImrHobGuid);
  if (PciImrHob == NULL) {
    DEBUG ((DEBUG_INFO, "EnablePcieImr: no HOB\n"));
    return;
  }
  //
  // Sanity check - don't program PCIe IMR if base address is 0
  //
  if (PciImrHob->PciImrBase == 0) {
    DEBUG ((DEBUG_ERROR, "PcieImr base address is 0, PCIe IMR programming skipped!\n"));
    return;
  }
  DEBUG ((DEBUG_ERROR, "PcieImr base address is %x\n", PciImrHob->PciImrBase));

  ImrLimit = PciImrHob->PciImrBase + LShiftU64 (GetPcieImrSize (), 20);
  ImrBaseLow = (UINT32)RShiftU64 ((PciImrHob->PciImrBase & 0xFFF00000), 20);
  ImrBaseHigh = (UINT32)RShiftU64 (PciImrHob->PciImrBase, 32);
  ImrLimitLow = (UINT32)RShiftU64 ((ImrLimit & 0xFFF00000), 20);
  ImrLimitHigh = (UINT32)RShiftU64 (ImrLimit, 32);

  //
  // PCIe IMR base & limit registers in SA contain bits 63:20 of adress, divided into upper (64:32) and lower (31:20) parts
  // That means bits 19:10 are ignored and addresses are aligned to 1MB.
  //
  Data32 = (UINT32) (LShiftU64 (ImrBaseLow, N_PCIE_IMRAMBL_IAMB) | LShiftU64 (ImrLimitLow, N_PCIE_IMRAMBL_IAML));

  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_IMRAMBL, Data32);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_IMRAMBU32, ImrBaseHigh);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_IMRAMLU32, ImrLimitHigh);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_IMRAMLE, (UINT32) (LShiftU64 (BIT0, (RpIndex % CPU_PCIE_MAX_ROOT_PORTS)) | B_PCIE_IMRAMLE_SRL));

  DEBUG ((DEBUG_INFO, "PCIe IMR registers: RpIndex %x, +10=%08x, +14=%08x, +18=%08x, +1c=%08x\n",
    RpIndex,
    CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_IMRAMBL),
    CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_IMRAMBU32),
    CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_IMRAMLU32),
    CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_IMRAMLE)
    ));
}

/**
  This function assigns bus number to PCIe bus .

  @param[in] RpIndex     Root Port index
**/
VOID
CpuPcieAssignTemporaryBus (
  IN UINT64 RpBase,
  IN UINT8  TempPciBus
  )
{
  UINT64 EpBase;
  DEBUG((DEBUG_VERBOSE, "CpuPcieAssignTemporaryBus Start\n"));
  //
  // Assign bus numbers to the root port
  //
  PciSegmentAndThenOr32 (
    RpBase + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET,
    (UINT32) ~B_PCI_BRIDGE_BNUM_SBBN_SCBN,
    ((UINT32) (TempPciBus << 8)) | ((UINT32) (TempPciBus << 16))
    );
  //
  // A config write is required in order for the device to re-capture the Bus number,
  // according to PCI Express Base Specification, 2.2.6.2
  // Write to a read-only register VendorID to not cause any side effects.
  //

  EpBase  = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, TempPciBus, 0, 0, 0);
  PciSegmentWrite16 (EpBase + PCI_VENDOR_ID_OFFSET, 0);
  DEBUG ((DEBUG_VERBOSE, "CpuPcieAssignTemporaryBus End\n"));
}

/**
  Clear temp bus usage.

  @param[in] RpBase     Root Port PCI base address
**/
VOID
CpuPcieClearBus (
  IN UINT64 RpBase
  )
{
  DEBUG ((DEBUG_VERBOSE, "CpuPcieClearBus Start\n"));
  PciSegmentAnd32 (
    RpBase + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET,
    (UINT32) ~B_PCI_BRIDGE_BNUM_SBBN_SCBN
    );
  DEBUG ((DEBUG_VERBOSE, "CpuPcieClearBus End\n"));
}

/**
  This function sets Common Clock Mode bit in rootport and endpoint connected to it, if both sides support it.
  This bit influences rootport's Gen4 training and should be set before Gen4 software equalization is attempted.
  It does not attempt to set CCC in further links behind rootport

  @param[in] RpIndex     Root Port index
**/
VOID
CpuPcieEnableCommonClock (
  IN UINT32 RpIndex,
  IN UINT8  TempPciBus
  )
{
  UINTN  RpDevice;
  UINTN  RpFunction;
  UINT64 RpBase;
  UINT64 EpBase;
  UINT8  Func;
  UINT8  EpPcieCapOffset;

  DEBUG ((DEBUG_VERBOSE, "CpuPcieEnableCommonClock Start\n"));
  GetCpuPcieRpDevFun (RpIndex, &RpDevice, &RpFunction);
  RpBase = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, RpDevice, RpFunction, 0);
  CpuPcieAssignTemporaryBus (RpBase, TempPciBus);
  EpBase = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, TempPciBus, 0, 0, 0);
  if (IsDevicePresent (EpBase)) {
    EpPcieCapOffset = PcieBaseFindCapId (EpBase, EFI_PCI_CAPABILITY_ID_PCIEXP);
    if (GetScc (RpBase, R_PCIE_CLIST) && (EpPcieCapOffset != 0) && GetScc (EpBase, EpPcieCapOffset)) {
      EnableCcc (RpBase, R_PCIE_CLIST);
      EnableCcc (EpBase, EpPcieCapOffset);
      if (IsMultifunctionDevice (EpBase)) {
        for (Func = 1; Func <= PCI_MAX_FUNC; Func++) {
          EpBase = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, TempPciBus, 0, Func, 0);
          EnableCcc (EpBase, PcieBaseFindCapId (EpBase, EFI_PCI_CAPABILITY_ID_PCIEXP));
        }
      }
      RetrainLink (RpBase, R_PCIE_CLIST, TRUE);
    }
  }
  CpuPcieClearBus (RpBase);
  DEBUG ((DEBUG_VERBOSE, "CpuPcieEnableCommonClock End\n"));
}

/**
  Detect whether CLKREQ# is supported by the platform and device.

  Assumes device presence. Device will pull CLKREQ# low until CPM is enabled.
  Test pad state to verify that the signal is correctly connected.
  This function will change pad mode to GPIO!

  @param[in] RootPortConfig      Root port configuration
  @param[in] DevicePresent       Determines whether there is a device on the port

  @retval TRUE if supported, FALSE otherwise.
**/
BOOLEAN
CpuPcieDetectClkreq (
  IN UINT32                    RpIndex,
  IN CONST CPU_PCIE_CONFIG     *PcieConfig
  )
{
  CPU_PCIE_HOB                        *CpuPcieHob;

  CpuPcieHob = NULL;
  CpuPcieHob = (CPU_PCIE_HOB *)GetFirstGuidHob(&gCpuPcieHobGuid);
  if (!IsClkReqAssigned (PchClockUsageCpuPcie0 + RpIndex)) {
    DEBUG ((DEBUG_ERROR, "CLKREQ is not assigned, disabling power management for RP %d.\n", RpIndex));
    return FALSE;
  }
  if (CpuPcieHob != NULL) {
    if (CpuPcieHob->DisableClkReqMsg[RpIndex] || EFI_ERROR (CheckClkReq (PchClockUsageCpuPcie0 + RpIndex))){
      DEBUG ((DEBUG_ERROR, "CLKREQ is not assigned, or not enabled for RP %d.\n", RpIndex));
      return FALSE;
    }
  } else {
    DEBUG((DEBUG_ERROR, "CpuPcieHob not found\n"));
    ASSERT(CpuPcieHob != NULL);
    return FALSE;
  }
  return TRUE;
}

/**
  Disables the root port using DEV_EN bit.

  @param[in] RpIndex            Root Port Number

**/
VOID
DevEnDisablePcieRootPort (
  IN UINT8   RpIndex
)
{
  UINT64     McD0BaseAddress;
  UINT32     DevEn;
  UINTN      RpDevice;
  UINTN      RpFunction;

  DEBUG ((DEBUG_INFO, "DevEnDisablePcieRootPort Start\n"));

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
  DevEn = PciSegmentRead32 (McD0BaseAddress + R_SA_DEVEN);
  DEBUG ((DEBUG_VERBOSE, "DevEn = %x\n", DevEn));
  GetCpuPcieRpDevFun (RpIndex, &RpDevice, &RpFunction);

  switch (RpDevice)
  {
   case 6:
     if (RpFunction == 2) {
       DevEn &= ~B_SA_DEVEN_D6F2EN_MASK;
     } else {
       DevEn &= ~B_SA_DEVEN_D6EN_MASK;
     }
     break;
   case 1:
     if (RpFunction == 1) {
       DevEn &= ~B_SA_DEVEN_D1F1EN_MASK;
     } else if (RpFunction == 2) {
       DevEn &= ~B_SA_DEVEN_D1F2EN_MASK;
     } else {
       DevEn &= ~B_SA_DEVEN_D1F0EN_MASK;
     }
     break;
  }
  PciSegmentWrite32 (McD0BaseAddress + R_SA_DEVEN, DevEn);
  DEBUG ((DEBUG_VERBOSE, "DevEn after write = %x\n", PciSegmentRead32 (McD0BaseAddress + R_SA_DEVEN)));
  DEBUG ((DEBUG_INFO, "DevEnDisablePcieRootPort End\n"));
  return;
}

/**
  Disables the root port. Depending on 2nd param, port's PCI config space may be left visible
  to prevent function swapping

  @param[in] RpIndex            Root Port Number.
  @param[in] KeepPortVisible    Indicates if the port's interface should be visible on PCI Bus0.
**/
VOID
DisableCpuPcieRootPort (
  IN UINT8   RpIndex,
  IN BOOLEAN KeepPortVisible
  )
{
  UINT32      Data32;
  UINT16      Data16;
  UINT32      LoopTime;
  UINT32      TargetState;
  UINT32      LinkActive;
  UINT64      RpBase;
  UINT32      Data32And;
  UINT32      Data32Or;
  UINT8       PortId;

  DEBUG ((DEBUG_INFO, "CPU: DisablePcieRootPort(%d) Start\n", RpIndex + 1));

  Data32   = 0;
  PortId   = 0;
  RpBase   = CpuPcieBase(RpIndex);
  Data32Or = 0;

  //
  // if (ADP-S D2 PCIe*, ADL-S D2 PCIe* or onwards)
  // set PMCS.PS = 2'b11 (D3HOT) before Function Disable the port.
  //
  PortId = (UINT8) GetCpuPcieRpSbiPid (RpIndex);
  Data16 = PegPciSegmentRead16 (RpIndex, R_PCIE_LSTS); /// Access LSTS using dword-aligned read
  LinkActive = Data16 & (UINT16)B_PCIE_LSTS_LA;

  ///
  /// Set B0:Dxx:Fn:338h[26] = 1b
  ///
  PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_PCIEALC, ~0u, B_PCIE_PCIEALC_BLKDQDA);

  if (LinkActive) {
    ///
    /// If device is present disable the link.
    ///
    DEBUG ((DEBUG_INFO, "Device present. Disabling the link.\n"));
    PegPciSegmentAndThenOr32(RpIndex, R_PCIE_LCTL, ~0u, B_PCIE_LCTL_LD);

    //
    // Set FCTL2.BLKNAT = 1 as per BWG 2.3 recommedation
    //
    if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
      CpuRegbarOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_FCTL2, B_PCIE_RCRB_FCTL2_BLKNAT);
    }
  }

  ///
  /// Poll B0:Dxx:Fn:328h[31:24] until 0x1 or 50ms timeout or Disable state(0x60) if link active.
  ///
  TargetState = V_PCIE_PCIESTS1_LTSMSTATE_DETRDY;
  for (LoopTime = 0; LoopTime < 5000; LoopTime++) {
    Data32 = PciSegmentRead32 (RpBase + R_PCIE_PCIESTS1);
    if (((Data32 & B_PCIE_PCIESTS1_LTSMSTATE) >> B_PCIE_PCIESTS1_LTSMSTATE_OFFSET) == TargetState) {
      break;
    }
    if (LinkActive) {
      if (((Data32 & B_PCIE_PCIESTS1_LTSMSTATE) >> B_PCIE_PCIESTS1_LTSMSTATE_OFFSET) == V_PCIE_PCIESTS1_LTSMSTATE_DISWAITPG) {
        break;
      }
    }
    MicroSecondDelay (10);
  }
  ///
  ///

  if (LinkActive) {
    ///
    /// Set offset 50h[4] to 1b.
    ///
    PegPciSegmentAnd32(RpIndex, R_PCIE_LCTL, ~(UINT32)B_PCIE_LCTL_LD);
  }

  ///
  /// Set offset 408h[27] to 1b to disable squelch.
  ///
  DEBUG ((DEBUG_VERBOSE, "Disable squelch\n"));
  PegPciSegmentAndThenOr32(RpIndex, R_PCIE_PHYCTL4, ~0u, B_PCIE_PHYCTL4_SQDIS);
  ///
  /// Make port disappear from PCI bus
  ///
  if (!KeepPortVisible) {
    DEBUG ((DEBUG_INFO, "CPU: Hiding the port %d\n", RpIndex + 1));
    //
    // Offset 0xA4 R_PCIE_PMCS - Program 11b to Bits 1:0 (B_PCIE_PMCS_PS)
    //
    if (IsSimicsEnvironment()) {
      PciSegmentOr32(RpBase + R_PCIE_PMCS, V_PCIE_PMCS_PS);
    } else {
      if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
        Data32And = (UINT32)~B_PCIE_PMCS_PS_MASK;
        Data32Or = (UINT32)V_PCIE_PMCS_PS;
        PciSegmentAndThenOr32 (RpBase +  R_PCIE_PMCS, Data32And, Data32Or);
      }

      GetCpuPcieRpPcdPortDisable (RpIndex, &Data32Or, &PortId);

      if (PortId != 0) {
        CpuRegbarOr32 (PortId, R_SA_SPX_PCR_PCD, Data32Or);
        DEBUG ((DEBUG_INFO, "PCD register after write = 0x%08x\n",  CpuRegbarRead32 (PortId, R_SA_SPX_PCR_PCD)));
      }
    }

    ///
    /// Function disable port using DEV_EN register.
    ///
    DevEnDisablePcieRootPort (RpIndex);
  }
  DisableClock (PchClockUsageCpuPcie0 + RpIndex);
  DEBUG ((DEBUG_INFO, "CPU: DisablePcieRootPort() End\n"));
}

/**
Checks if given rootport has an endpoint connected

@param[in] DeviceBase       PCI segment base address of root port

@retval                     TRUE if endpoint is connected
@retval                     FALSE if no endpoint was detected
**/
BOOLEAN
CpuPcieIsEndpointConnected(
  UINT64 DeviceBase
)
{
  return !!(PciSegmentRead16 (DeviceBase + R_PCIE_SLSTS) & B_PCIE_SLSTS_PDS);
}

/**
Get information about the endpoint

@param[in]  PortIndex   Root port index. (0-based)
@param[in]  TempPciBus  Temporary bus number
@param[out] DeviceInfo  Device information structure

@return TRUE if device was found, FALSE otherwise
**/
BOOLEAN
CpuPcieGetDeviceInfo (
  IN  UINT8                  PortIndex,
  IN  UINT8                  TempPciBus,
  OUT CPU_PCIE_DEVICE_INFO  *DeviceInfo
  )
{
  UINT64                  RpBase;
  UINT64                  EpBase;
  UINT32                  Data32;
  UINT8                   EpPcieCapPtr;
  UINT8                   EpLinkSpeed;
  UINT8                   Device;
  UINT8                   Function;

  DEBUG ((DEBUG_INFO, "CpuPcieGetDeviceInfo Start\n"));

  RpBase = CpuPcieBase (PortIndex);
  //
  // Check for device presence
  //
  if (!CpuPcieIsEndpointConnected (RpBase)) {
    DEBUG ((DEBUG_INFO, "Endpoint is not connected\n"));
    return FALSE;
  }

  Device          = 0;
  Function        = 0;
  EpPcieCapPtr    = 0;
  EpLinkSpeed     = 0;

  //
  // Assign temporary bus numbers to the root port
  //
  PciSegmentAndThenOr32 (
    RpBase + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET,
    (UINT32)~B_PCI_BRIDGE_BNUM_SBBN_SCBN,
    ((UINT32)(TempPciBus << 8)) | ((UINT32)(TempPciBus << 16))
    );

  //
  // A config write is required in order for the device to re-capture the Bus number,
  // according to PCI Express Base Specification, 2.2.6.2
  // Write to a read-only register VendorID to not cause any side effects.
  //
  EpBase = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, TempPciBus, Device, Function, 0);
  PciSegmentWrite16 (EpBase + PCI_VENDOR_ID_OFFSET, 0x8086);

  Data32          = PciSegmentRead32 (EpBase + PCI_VENDOR_ID_OFFSET);
  DeviceInfo->Vid = (UINT16)(Data32 & 0xFFFF);
  DeviceInfo->Did = (UINT16)(Data32 >> 16);

  if (Data32 != 0xFFFFFFFF) {
    EpPcieCapPtr = PcieFindCapId (SA_SEG_NUM, TempPciBus, Device, Function, EFI_PCI_CAPABILITY_ID_PCIEXP);
    if (EpPcieCapPtr != 0) {
      EpLinkSpeed = PciSegmentRead8 (EpBase + EpPcieCapPtr + R_PCIE_LCAP_OFFSET) & B_PCIE_LCAP_MLS;
    }
    DeviceInfo->MaxLinkSpeed = EpLinkSpeed;
  }

  //
  // Clear bus numbers
  //
  PciSegmentAnd32 (RpBase + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, (UINT32)~B_PCI_BRIDGE_BNUM_SBBN_SCBN);

  DEBUG ((DEBUG_INFO, "VID: %04X DID: %04X  MLS: %d\n",
    DeviceInfo->Vid, DeviceInfo->Did, DeviceInfo->MaxLinkSpeed));

  DEBUG ((DEBUG_INFO, "CpuPcieGetDeviceInfo End\n"));
  return (Data32 != 0xFFFFFFFF);
}

/**
  The function to change the root port speed based on policy

  @param[in] RpIndex      Port index
  @param[in] SiPolicyPpi  The SI Policy PPI instance

  @retval EFI_SUCCESS             Succeeds.
  @retval EFI_UNSUPPORTED         DEKEL Firmware Download fails.
**/
EFI_STATUS
CpuPcieRpSpeedChange (
  VOID
  )
{
  EFI_STATUS                        Status;
  UINT8                             RpIndex;
  UINTN                             MaxCpuPciePortNum;
  UINT64                            PciRootPortRegBase[CPU_PCIE_MAX_ROOT_PORTS];
  UINTN                             RpDev;
  UINTN                             RpFunc;
  UINTN                             LinkRetrainedBitmap;
  UINTN                             TimeoutCount;
  UINT32                            MaxLinkSpeed;
  CPU_PCIE_HOB                      *CpuPcieHob;

  CpuPcieHob = NULL;
  CpuPcieHob = (CPU_PCIE_HOB *)GetFirstGuidHob(&gCpuPcieHobGuid);
  MaxCpuPciePortNum = GetMaxCpuPciePortNum();
  if (CpuPcieHob == NULL) {
    DEBUG((DEBUG_ERROR, "CpuPcieHob not found\n"));
    ASSERT(CpuPcieHob != NULL);
    return EFI_NOT_FOUND;
  }
  //
  // Since we are using the root port base many times, it is best to cache them.
  //
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
    PciRootPortRegBase[RpIndex] = 0;
    Status = GetCpuPcieRpDevFun (RpIndex, &RpDev, &RpFunc);
    if (EFI_ERROR (Status)) {
      ASSERT (FALSE);
      continue;
    }
    PciRootPortRegBase[RpIndex] = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, (UINT32) RpDev, (UINT32) RpFunc, 0);
  }
  ///
  /// NOTE: Detection of Non-Complaint PCI Express Devices
  ///
  LinkRetrainedBitmap = 0;
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
    if (PciRootPortRegBase[RpIndex] == 0) {
      continue;
    }
    if (PciSegmentRead16 (PciRootPortRegBase[RpIndex] + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
      continue;
    }
    if (!(CpuPcieHob->RpEnabledMask & (BIT0 << RpIndex))) {
      continue;
    }
    MaxLinkSpeed = CpuPcieGetMaxLinkSpeed (PciRootPortRegBase[RpIndex]);

    if ((DevInfo[RpIndex].Vid != 0xFFFF) && (DevInfo[RpIndex].MaxLinkSpeed < MaxLinkSpeed)){
      MaxLinkSpeed = DevInfo[RpIndex].MaxLinkSpeed;
    }

    if ((MaxLinkSpeed > 1) && (DevInfo[RpIndex].Vid != 0xFFFF)) {
      if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP16) {
        //
        // DEKEL FW should be downloaded before speed change, so check for download status before initiating a speed change.
        //
        if ((RpIndex == 0) && (DekelFwStatus (RpIndex) == FALSE)) { // RpIndex = 0 always corresponds to PEG 060 in BIOS
          continue;
        }
        if ((RpIndex != 0) && (DekelFwStatus (RpIndex) == FALSE)) { // Dekel FW status needs to be checked only once for x16 PHY
          break; // If DekelIo16FwStatus == FALSE, just break - speed change cannot happen for the rest of the ports in x16 controller
        }
      }
      if (MaxLinkSpeed > 2) {
        PciSegmentOr32 (PciRootPortRegBase[RpIndex] + R_PCIE_LCTL3, B_PCIE_LCTL3_PE);
      }
      PciSegmentAndThenOr16 (
        PciRootPortRegBase[RpIndex] + R_PCIE_LCTL2,
        (UINT16) ~B_PCIE_LCTL2_TLS,
        (UINT16) MaxLinkSpeed
        );
      DEBUG((DEBUG_INFO, "Program TLS to %x \n", MaxLinkSpeed));
      if (CpuPcieIsLinkActive (PciRootPortRegBase[RpIndex])) {
        //
        //At least give 100ns delay before LCTL.RL programming as per BWG 1.9
        //
        NanoSecondDelay (100);
        //
        // Retrain the link if device is present
        //
        PciSegmentOr16 (PciRootPortRegBase[RpIndex] + R_PCIE_LCTL, B_PCIE_LCTL_RL);
        LinkRetrainedBitmap |= (1u << RpIndex);
        DEBUG((DEBUG_INFO, "Retrain the link if device is present! \n"));
      }
    }
  }

  //
  // 100 ms timeout while checking for link active on retrained link
  //
  for (TimeoutCount = 0; ((LinkRetrainedBitmap != 0) && (TimeoutCount < 1000)); TimeoutCount++) {
    //
    // Delay 100 us
    //
    MicroSecondDelay (100);
    //
    // Check for remaining root port which was link retrained
    //
    for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
      if ((LinkRetrainedBitmap & (1u << RpIndex)) != 0) {
        //
        // If the link is active, clear the bitmap
        //
        if (PciSegmentRead16 (PciRootPortRegBase[RpIndex] + R_PCIE_LSTS) & B_PCIE_LSTS_LA) {
          LinkRetrainedBitmap &= ~(1u << RpIndex);
        }
      }
    }
  }
  //
  // If 100 ms has timeout, and some link are not active, print a message and continue execution
  //
  if (LinkRetrainedBitmap != 0) {
    for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
      Status = GetCpuPcieRpDevFun (RpIndex, &RpDev, &RpFunc);
      if ((LinkRetrainedBitmap & (1u << RpIndex)) != 0) {
        DEBUG((DEBUG_ERROR, "Link failed to train to higher speed for port %d, Dev Number %x, Func Number %x\n",RpIndex+1, RpDev, RpFunc));
      }
    }
  }

  return EFI_SUCCESS;
}

/*
  Some PCIe devices may take long time between they become detected and form a link.
  This function waits until all enabled, non-empty rootports form a link or until a timeout

  @param[in] MaxRootPorts      number of rootports
  @param[in] DisabledPortMask  mask of rootprots that don't need to be considered
*/
VOID
CpuPcieWaitForLinkActive (
  UINT32 MaxRootPorts,
  UINT32 DisabledPortMask
  )
{
  UINT32 PortMask;
  UINT32 Index;
  UINT32 Time;
  UINT64 RpBase;

  Time = 0;
  //
  // Set a bit in PortMask for each rootport that exists and isn't going to be disabled
  //
  PortMask = (0x1 << MaxRootPorts) - 1;
  PortMask &= ~DisabledPortMask;

  DEBUG ((DEBUG_INFO, "CpuPcieWaitForLinkActive, RP mask to wait for = 0x%08x\n", PortMask));
  while (Time < LINK_ACTIVE_POLL_TIMEOUT) {
    for (Index = 0; Index < MaxRootPorts; Index ++) {
      if (!(PortMask & (BIT0 << Index))) {
        continue;
      }
      RpBase = CpuPcieBase (Index);
      //
      // if PDS is not set or if LA is set then this rootport is done - clear it from mask
      //
      if (!CpuPcieIsEndpointConnected (RpBase) || CpuPcieIsLinkActive (RpBase)) {
        PortMask &= ~ (BIT0 << Index);
      }
    }
    if (PortMask == 0x0) {
      DEBUG ((DEBUG_INFO, "CpuPcieWaitForLinkActive, all RPs done, lost %dms waiting\n", Time/1000));
      return;
    }
    MicroSecondDelay (LINK_ACTIVE_POLL_INTERVAL);
    Time += LINK_ACTIVE_POLL_INTERVAL;
  }

  DEBUG ((DEBUG_WARN, "CpuPcieWaitForLinkActive, timeout with the following RPs still not done: 0x%08x\n", PortMask));
}

/**
  Check for device presence with timeout.

  @param[in]     RpBase      Root Port base address
  @param[in]     TimeoutUs   Timeout in microseconds
  @param[in,out] Timer       Timer value, must be initialized to zero
                             before the first call of this function.
**/
BOOLEAN
IsCpuPcieDevicePresent (
  IN     UINT64  RpBase,
  IN     UINT32  TimeoutUs,
  IN OUT UINT32  *Timer
  )
{
  while (TRUE) {
    if (CpuPcieIsEndpointConnected (RpBase)) {
      return TRUE;
    }
    if (*Timer < TimeoutUs) {
      MicroSecondDelay (10);
      *Timer += 10;
    } else {
      break;
    }
  }
  return FALSE;
}

/*
  After programming the linear mode HW EQ flow, BIOS performs and link retrain
  and then polls for Hardware Equalization done bit.
  This function waits until HAED bit is set or until a timeout

  @param[in] CpuPcieRpPreMemConfig  Cpu Pcie Root Port Pre-Memory Configuration
  @param[in] RpDisableMask     bitmask of all disabled rootports
*/
VOID
CpuPcieWaitForHwEqDone (
  CPU_PCIE_RP_PREMEM_CONFIG  *CpuPcieRpPreMemConfig,
  UINT32                     RpDisableMask
  )
{
  UINT64 RpBase;
  UINT32 Time;
  UINT32 Haed;
  UINT32 Px16ghaed;
  UINT32 Px32ghaed;
  UINT8  Gen3Haed;
  UINT8  Gen4Haed;
  UINT8  Gen5Haed;
  UINT32 Data32;
  UINT32 TargetState;
  UINT32 TargetStateMask;
  UINT32 Timer;
  UINT32 DetectTimeoutUs;
  UINT8  RpIndex;
  UINT8  MaxCpuPciePortNum;

  Time      = 0;
  Haed      = 0;
  Px16ghaed = 0;
  Px32ghaed = 0;
  Gen3Haed  = 0;
  Gen4Haed  = 0;
  Gen5Haed  = 0;
  DetectTimeoutUs = 0;
  TargetStateMask = 0;
  MaxCpuPciePortNum = GetMaxCpuPciePortNum();

  DEBUG ((DEBUG_INFO, "CpuPcieWaitForHwEqDone Begin:\n"));
  DEBUG((DEBUG_INFO, "mDoGen3HwEqRpMask = %x\n", mDoGen3HwEqRpMask));
  DEBUG((DEBUG_INFO, "mDoGen4HwEqRpMask = %x\n", mDoGen4HwEqRpMask));
  DEBUG((DEBUG_INFO, "mDoGen5HwEqRpMask = %x\n", mDoGen5HwEqRpMask));
  do {
    for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex ++) {
      RpBase = CpuPcieBase (RpIndex);
      DetectTimeoutUs = mCpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.DetectTimeoutMs * 1000;
      if (IsCpuPcieDevicePresent (RpBase, DetectTimeoutUs, &Timer)) {
        if (((CpuPcieRpPreMemConfig->PcieSpeed[RpIndex] >= 3) || (CpuPcieRpPreMemConfig->PcieSpeed[RpIndex] == 0)) && ((RpDisableMask & (BIT0 << RpIndex)) == 0)) {
          if (mDoGen3HwEqRpMask & (BIT0 << RpIndex)) {
            Haed = PciSegmentRead32 (RpBase + R_PCIE_EQCFG1);
            Haed &= (UINT32) B_PCIE_EQCFG1_HAED;
          }
          if (mDoGen4HwEqRpMask & (BIT0 << RpIndex)) {
            Px16ghaed = PciSegmentRead32 (RpBase + R_PCIE_EQCFG4);
            Px16ghaed &= (UINT32) B_PCIE_EQCFG4_PX16GHAED;
          }
          if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
            if (mDoGen5HwEqRpMask & (BIT0 << RpIndex)) {
              Px32ghaed = CpuPcieSip17GetPx32EqCfg1 (RpIndex);
              Px32ghaed &= CpuPcieSip17GetGen5HaedBitMap ();
            }
          }
          if (Haed == (UINT32) B_PCIE_EQCFG1_HAED) {
            Gen3Haed |= (BIT0 << RpIndex);
          }
          if (Px16ghaed == (UINT32) B_PCIE_EQCFG4_PX16GHAED) {
            Gen4Haed |= (BIT0 << RpIndex);
          }
          if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
            if (Px32ghaed == (UINT32) CpuPcieSip17GetGen5HaedBitMap ()) {
              Gen5Haed |= (BIT0 << RpIndex);
            }
          }
          Data32      = PciSegmentRead32 (RpBase + R_PCIE_PCIESTS1);
          TargetState = (Data32 & B_PCIE_PCIESTS1_LTSMSTATE_MASK) >> B_PCIE_PCIESTS1_LTSMSTATE_OFFSET;
          if (TargetState == V_PCIE_PCIESTS1_LTSMSTATE_L0) {
            TargetStateMask |= (BIT0 << RpIndex);
          }
        }
      }
    }
    if ((Gen3Haed == mDoGen3HwEqRpMask) && (Gen4Haed  == mDoGen4HwEqRpMask) && (Gen5Haed  == mDoGen5HwEqRpMask) && (TargetStateMask == (~RpDisableMask))) {
      break; // All the HAED bits are set, so break the do-while.
    }
    MicroSecondDelay (HWEQ_DONE_POLL_INTERVAL);
    Time += HWEQ_DONE_POLL_INTERVAL;
  } while (Time < HWEQ_DONE_POLL_TIMEOUT);

  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
    if (Gen3Haed & (BIT0 << RpIndex)) {
      DEBUG((DEBUG_INFO, "Gen3 Hardware Equalization Done bit is set for Root Port %x\n", RpIndex));
    }
    if (Gen4Haed & (BIT0 << RpIndex)) {
      DEBUG((DEBUG_INFO, "Gen4 Hardware Equalization Done bit is set for Root Port %x\n", RpIndex));
    }
    if (Gen5Haed & (BIT0 << RpIndex)) {
      DEBUG((DEBUG_INFO, "Gen5 Hardware Equalization Done bit is set for Root Port %x\n", RpIndex));
    }

    if (((mDoGen3HwEqRpMask & (BIT0 << RpIndex)) || (mDoGen4HwEqRpMask & (BIT0 << RpIndex)) || (mDoGen5HwEqRpMask & (BIT0 << RpIndex))) \
      && (((Gen3Haed & (BIT0 << RpIndex)) || (Gen4Haed & (BIT0 << RpIndex)) || (Gen5Haed & (BIT0 << RpIndex))) == 0)) {
      DEBUG ((DEBUG_INFO, "Hardware Equalization Done bit is NOT set for Root Port %x even after timeout!!!\n", RpIndex));
      if (!IsSimicsEnvironment()) {
        ASSERT(FALSE);
      }
    }
  }
}

/**
  Maps the index of the first port on the same controller.

  @param[in] RpIndex     Root Port Number (0-based)

  @retval Index of the first port on the first controller.
**/
UINT32
GetCpuPcieFirstPortIndex (
  IN     UINT32  RpIndex
  )
{
  switch (RpIndex)
  {
   case 0:
     RpIndex = 0; //RpIndex = 0 always maps to BDF = 060 irrespective of the SKU and there is only one Root port so return the same number
     break;
   case 1:
     RpIndex = 1; //RpIndex = 1 maps tp BDF = 010
   case 2:
   case 3:
     RpIndex = 2; //RpIndex = 2 or 3 maps to BDF = 01 1/2 for which the first root port is 2
     break;
   default:
     RpIndex = 0; //RpIndex = 0 always maps to BDF = 060 irrespective of the SKU and there is only one Root port so return the same number
     break;
  }
  return RpIndex;
}
/**
  Program controller power management settings.
  This settings are relevant to all ports including disabled ports.
  All registers are located in the first port of the controller.
  Use sideband access since primary may not be available.

  @param[in]  RpIndex                         The root port to be initialized (zero based).
  @param[in]  TrunkClockGateEn                Indicates whether trunk clock gating is to be enabled,
                                              requieres all controller ports to have dedicated CLKREQ#
                                              or to be disabled.
  @param[in]  ClockGating                     Indicates whether the PCI Express Clock Gating for each root port
                                              is enabled by platform modules.
  @param[in]  PowerGating                     Indicates whether the PCI Express Power Gating for each root port
                                              is enabled by platform modules.
**/
VOID
CpuPcieConfigureControllerBasedPowerManagement (
  IN  UINT32   RpIndex,
  IN  BOOLEAN  TrunkClockGateEn,
  IN  BOOLEAN  ClockGating,
  IN  BOOLEAN  PowerGating
  )
{
  UINT32                    Data32And;
  UINT32                    Data32Or;
  UINT64                    RpBase;
  CPU_PCIE_HOB              *CpuPcieHob;

  CpuPcieHob = NULL;
  CpuPcieHob = (CPU_PCIE_HOB *)GetFirstGuidHob(&gCpuPcieHobGuid);
  if (CpuPcieHob == NULL) {
    DEBUG((DEBUG_INFO, "CpuPcieHob not found!\n"));
    ASSERT(CpuPcieHob != NULL);
    return;
  }

  RpBase = CpuPcieBase(RpIndex);


  DEBUG ((DEBUG_INFO, "CpuPcieConfigureControllerBasedPowerManagement(%d) Start\n", RpIndex + 1));

  if (ClockGating) {
    DEBUG ((DEBUG_INFO, "Clock gating is enabled!\n"));
    ///
    /// Program (R_PCIE_CCFG) D0h
    /// Set D0h[15] to 1b for dynamic clock gating enable when ISM is in Idle state
    ///
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_CCFG, ~0u, B_PCIE_CCFG_DCGEISMA);

    ///
    /// Program (R_PCIE_RPDCGEN) E1h
    /// Set E1h[5] to 1b to enable PCIe Link clock trunk gating.
    /// Set E1h[4] to 1b to enable PCIe backbone clock trunk gating.
    /// Set E1h[2] to 1b to enable shared resource dynamic backbone clock gating.
    /// Set E1h[6] to 1b to enable Partition/Trunk Oscillator clock gating; if all ports on the controller support CLKREQ#.
    ///
    DEBUG ((DEBUG_INFO, "Setting PTOCGE \n"));
    Data32Or = (B_PCIE_RPDCGEN_LCLKREQEN | B_PCIE_RPDCGEN_BBCLKREQEN | B_PCIE_RPDCGEN_SRDBCGEN | B_PCIE_RPDCGEN_PTOCGE);
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_SPR, ~0u, (UINT32) (Data32Or << 8)); ///< For 32-bit boundary alignment for 0xE1 (RPDCGEN), shift by 8 bits from 0xE0
    ///
    /// Program (R_PCIE_RPPGEN) E2h
    /// Set E2h[4] to 1b to enable side clock gating.
    ///
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_SPR, ~0u, (UINT32) (B_PCIE_RPPGEN_SEOSCGE << 16)); ///< For 32-bit boundary alignment for 0xE1 (RPDCGEN), shift by 8 bits from 0xE0
    ///
    /// Program (R_PCIE_SPR) E0h
    /// E0h[0] - Sticky Chicken Bit, clear this bit
    ///
    PciSegmentAnd32 (RpBase + R_PCIE_SPR, (UINT32) ~B_PCIE_SPR_SCB);
  }

  if (PowerGating) {
    DEBUG ((DEBUG_INFO, "Power gating is enabled!\n"));
    ///
    /// Set F5h[1:0] to 11b  (R_PCH_PCIE_CFG_PHYCTL2)
    /// Set F7h[3:2] = 00b   (R_PCH_PCIE_CFG_IOSFSBCS)
    ///
    Data32And = (UINT32) ~(B_PCIE_IOSFSBCS_SIID_MASK << 24);  ///< For 32-bit boundary alignment, shift by 24bits from 0xF4 to reach 0xF7
    Data32Or = (B_PCIE_PHYCTL2_PXPG3PLLOFFEN | B_PCIE_PHYCTL2_PXPG2PLLOFFEN) << 8; ///< For 32-bit boundary alignment for 0xF7 ( IOSFSBCS) to offset 0xF4
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_F4, Data32And, Data32Or);

    ///
    /// Program (R_PCIE_PCIEPMECTL2) 424h
    /// Set 424h[11] to 1b to enable mod-PHY common lane power gating.
    /// SIP16: Set 424h[15:14] to 1b to program Controller Power Gating Exit Hysteresis (CPGEXH) to 5us.
    /// SIP17: Set 424h[15:14] to 0b to program Controller Power Gating Exit Hysteresis (CPGEXH) to 0us.
    /// SIP16: Set 424h[13:12] to 1b to program Controller Power Gating Entry Hysteresis (CPGENH) to 5us.
    /// SIP17: Set 424h[13:12] to 0b to program Controller Power Gating Entry Hysteresis (CPGENH) to 0us.
    ///

    Data32And = (UINT32) ~(B_PCIE_PCIEPMECTL2_CPGEXH_MASK | B_PCIE_PCIEPMECTL2_PHYCLPGE | B_PCIE_PCIEPMECTL2_CPGENH_MASK);
    Data32Or = B_PCIE_PCIEPMECTL2_PHYCLPGE;

    if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP16) {
      Data32Or |= (V_PCIE_PCIEPMECTL2_CPGEXH << B_PCIE_PCIEPMECTL2_CPGEXH_OFFSET) | (V_PCIE_PCIEPMECTL2_CPGENH << B_PCIE_PCIEPMECTL2_CPGENH_OFFSET);
    }
    PciSegmentAndThenOr32 (RpBase + R_PCIE_PCIEPMECTL2, Data32And, Data32Or);

    ///
    /// Progarm (R_PCIE_HWSNR) 5F0h
    /// Set 5F0h[3:0] to 0101b for Bank Enable Pulse Width.
    /// Set 5F0h[7:4] to 01b for Restore Enable Pulse Width. 01b = 2 clocks.
    /// Set 5F0h[9:8] to 10b for Entry and Exit hysteresis. 10b = 16clocks.
    ///
    Data32And = (UINT32)~(B_PCIE_HWSNR_BEPW_MASK | B_PCIE_HWSNR_REPW_MASK | B_PCIE_HWSNR_EEH_MASK);
    Data32Or = B_PCIE_HWSNR_BEPW_8_CLKS | (0x1 << B_PCIE_HWSNR_REPW_OFFSET) | (0x2 << B_PCIE_HWSNR_EEH_OFFSET);
    PciSegmentAndThenOr32 (RpBase + R_PCIE_HWSNR, Data32And, Data32Or);

    ///
    /// Program (R_PCIE_PGCTRL) 5F4h
    /// Set 5F4h[2:0] to 010b for PM_REQ Block Response Time.
    /// 010b = 10 micro seconds for 1LM.
    ///
    Data32Or = V_PCIE_PGCTRL_PMREQBLKRSPT_10MICRO_SEC;
    PciSegmentAndThenOr32 (RpBase + R_PCIE_PGCTRL, ~0u, Data32Or);

    ///
    /// Program (R_PCIE_ACRG3) 6CCh
    /// Set 6CCh[23:22] to 10b for CPG Wake Control for 8 micro sec.
    ///
    Data32And = (UINT32) ~(B_PCIE_ACRG3_CPGWAKECTRL_MASK);
    Data32Or = V_PCIE_ACRG3_CPGWAKECTRL << B_PCIE_ACRG3_CPGWAKECTRL_OFFSET;
    PciSegmentAndThenOr32 (RpBase + R_PCIE_ACRG3, Data32And, Data32Or);

  }

  ///
  /// @todo: Squelch and CLKREQ# programming pending. To be reviewed after MVE validation.
  ///
}

/**
  Configure ASPM settings which are applicable to both enabled and disabled ports.
  These settings are relevant to all ports including disabled ports.
  Use sideband access since primary may not be available.

  @param[in]  RpIndex                         The root port to be initialized (zero based).
**/
VOID
CpuPcieConfigureASPM (
  IN  UINT32   RpIndex
  )
{
  UINT32         Data32And;
  UINT32         Data32Or;
  UINT64         RpBase;

  RpBase = CpuPcieBase(RpIndex);
  ///
  /// Program (R_PCIE_G4L0SCTL) 0x310
  /// Gen4 Active State L0s Preparation Latency (G4ASL0SPL) 0x310  31:24 0x28
  /// Gen4 L0s Entry Idle Control(G4L0SIC) 0x310  23:22 0x3
  ///
  Data32And = (UINT32) ~(B_PCIE_G4L0SCTL_G4ASL0SPL_MASK | B_PCIE_G4L0SCTL_G4L0SIC_MASK);
  Data32Or = (UINT32) ((V_PCIE_G4L0SCTL_G4ASL0SPL << B_PCIE_G4L0SCTL_G4ASL0SPL_OFFSET) | (V_PCIE_G4L0SCTL_G4L0SIC << B_PCIE_G4L0SCTL_G4L0SIC_OFFSET));

  PciSegmentAndThenOr32 (RpBase + R_PCIE_G4L0SCTL, Data32And, Data32Or);

  ///
  /// Program (R_PCIE_G3L0SCTL) 0x478
  /// Gen3 Active State L0s Preparation Latency (G3ASL0SPL) 0x478  31:24 0x28
  ///
  Data32And = (UINT32) ~(B_PCIE_G3L0SCTL_G3ASL0SPL_MASK);
  Data32Or = (UINT32) (V_PCIE_G3L0SCTL_G3ASL0SPL << B_PCIE_G3L0SCTL_G3ASL0SPL_OFFSET);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_G3L0SCTL, Data32And, Data32Or);

  ///
  /// Program (R_PCIE_PCIEL0SC) 0x318
  /// Gen2 Active State L0s Preparation Latency (G2ASL0SPL) 0x318  31:24 0x14
  /// Gen1 Active State L0s Preparation Latency (G1ASL0SPL) 0x318  23:16 0x14
  ///
  Data32And = (UINT32) ~(UINT32)(B_PCIE_PCIEL0SC_G2ASL0SPL_MASK | B_PCIE_PCIEL0SC_G1ASL0SPL_MASK);
  Data32Or = (UINT32) (V_PCIE_PCIEL0SC_G2ASL0SPL << B_PCIE_PCIEL0SC_G2ASL0SPL_OFFSET) | (V_PCIE_PCIEL0SC_G1ASL0SPL << B_PCIE_PCIEL0SC_G1ASL0SPL_OFFSET);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_PCIEL0SC, Data32And, Data32Or);

  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
    CpuPcieSip17ConfigureNfts (RpIndex);
  } else {
    CpuPcieSip16ConfigureNfts (RpBase);
  }
}

/**
  Configure LTR Subtraction settings.

  @param[in]  RpIndex  The root port to be initialized (zero based).
**/
VOID
CpuPcieConfigureLtrSubtraction (
  IN  UINT32   RpIndex
  )
{
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;
  LTR_SUBTRACTION_CONFIG      L1StandardConfig = {0x1, 0x2, 0x23, 0x1, 0x2, 0x23};
  LTR_SUBTRACTION_CONFIG      L1p1Config = {0x1, 0x2, 0x32, 0x1, 0x2, 0x32};
  LTR_SUBTRACTION_CONFIG      L1p2Config = {0x1, 0x2, 0xC8, 0x1, 0x2, 0xC8};
  LTR_SUBTRACTION_CONFIG      LtrSubL11Npg = {0x1, 0x2, 0x1E, 0x1, 0x2, 0x1E};

  CpuPcieGetRpDev (RpIndex, &RpDevPrivate);
  PcieSipConfigureLtrSubstraction (&RpDevPrivate.RpDev, &L1StandardConfig, &L1p1Config, &L1p2Config, &LtrSubL11Npg);
}

/**
  Configure power management settings which are applicable to both enabled and disabled ports.
  These settings are relevant to all ports including disabled ports.
  Use sideband access since primary may not be available.

  @param[in]  RpIndex                         The root port to be initialized (zero based).
  @param[in]  PhyLanePgEnable                 Indicates whether PHY lane power gating is to be enabled,
                                              requires CLKREQ# to supported by the port or the port to be disabled.
  @param[in]  ClockGating                     Indicates whether the PCI Express Clock Gating for each root port
                                              is enabled by platform modules.
  @param[in]  PowerGating                     Indicates whether the PCI Express Power Gating for each root port
                                              is enabled by platform modules.
**/
VOID
CpuPcieConfigurePortBasedPowerManagement (
  IN  UINT32   RpIndex,
  IN  BOOLEAN  PhyLanePgEnable,
  IN  BOOLEAN  ClockGating,
  IN  BOOLEAN  PowerGating
  )
{
  UINT32                            Data32;
  UINT32                            Data32Or;
  UINT32                            Data32And;
  CPU_PCIE_HOB                      *CpuPcieHob;
  CONST CPU_PCIE_ROOT_PORT_CONFIG   *RpConfig;
  UINT64                            RpBase;

  CpuPcieHob = NULL;
  RpBase   = CpuPcieBase(RpIndex);
  CpuPcieHob = (CPU_PCIE_HOB *)GetFirstGuidHob(&gCpuPcieHobGuid);
  if (CpuPcieHob == NULL) {
    DEBUG ((DEBUG_INFO, "CpuPcieHob not found!\n"));
    ASSERT (CpuPcieHob != NULL);
    return;
  }

  RpConfig = &mCpuPcieRpConfig->RootPort[RpIndex];

  DEBUG ((DEBUG_INFO, "CpuPcieConfigurePortBasedPowerManagement(%d) Start\n", RpIndex + 1));

  ///
  /// Section 4.30 LTR
  ///
  CpuPcieConfigureLtrSubtraction (RpIndex);

  ///
  /// Squelch setting for ASPM programming needs to be done before enabling of ASPM for Gen5 controllers
  ///
  if (GetCpuPcieSipInfo(RpIndex) == PCIE_SIP17) {
    CpuPcieSip17SquelchSettingAspm (RpIndex);
  }

  if (RpConfig->PcieRpCommonConfig.Aspm != FALSE) {
    ///
    /// Section 4.4.2 ASPM
    ///
    CpuPcieConfigureASPM (RpIndex);
  }

  if (ClockGating) {
    DEBUG ((DEBUG_INFO, "Root port Clock gating is enabled!\n"));

    ///
    /// Program (R_PCH_PCIE_CFG_PCIEPMECTL3) 434h
    /// Set 434h[4] to 1b to enable L1 Auto power gating only if L1SS is disabled
    /// Set 434h[3:2] to 01b to program OSC Clock gate hysteresis to 1us.
    /// Set 434h[1:0] t0 00b to Program Power Gating Exit Hysteresis to 0us.
    ///
    Data32And = (UINT32) ~(B_PCIE_PCIEPMECTL3_PMREQCPGEXH_MASK | B_PCIE_PCIEPMECTL3_OSCCGH_MASK | B_PCIE_PCIEPMECTL3_L1PGAUTOPGEN);
    Data32Or = (UINT32) ((V_PCIE_PCIEPMECTL3_OSCCGH_1US << B_PCIE_PCIEPMECTL3_OSCCGH_OFFSET));
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_PCIEPMECTL3, Data32And, Data32Or);
    ///
    /// Program (R_PCIE_RPDCGEN) E1h
    /// Set E1h[1] to 1b to enable dynamic link clock gating.
    /// Set E1h[0] to 1b to enable dynamic bbackbone clock gating.
    ///
    Data32Or = (UINT32) ((B_PCIE_RPDCGEN_RPDLCGEN) << 8); ///< For 32-bit boundary alignment for 0xE1 (RPDCGEN), shift by 8bits from 0xE0 to reach 0xE1
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_SPR, ~0u, Data32Or);
    ///
    /// Program (R_PCIE_IOSFSBCS) F7h
    /// Set F7h[6] to 1b to enable side clock partition/ trunk clock gating.
    ///
    Data32Or = (UINT32) (B_PCIE_IOSFSBCS_SCPTCGE << 24); ///< For 32-bit boundary alignment for 0xF7 ( IOSFSBCS), shift by 24bits from 0xF4 to reach 0xF7
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_F4, ~0u, Data32Or);
    ///
    /// Program Dynamic Clock Gating registers for SIP17 controller
    ///
    if (GetCpuPcieSipInfo(RpIndex) == PCIE_SIP17) {
        CpuPcieSip17DynamicClockGating (RpIndex);
    }
  }

  ///
  ///SIP16: Set 4A4h[6] t0 0b for PMETO Timeout Fix Disable
  ///SIP17: Set 4A4h[6] t0 1b for PMETO Timeout Fix Disable
  ///
  Data32And = (UINT32) ~(B_PCIE_CFG_CONTROL2_PMETOFD);
  Data32Or  = 0;
  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
    Data32Or = (B_PCIE_CFG_CONTROL2_PMETOFD);
  }
  PciSegmentAndThenOr32 (RpBase + R_PCIE_CFG_CONTROL2, Data32And, Data32Or);

  ///
  /// Set 420h[19:18] to 00b for Power Off Wait Time
  ///
  Data32And = (UINT32) ~(B_PCIE_PCIEPMECTL_POFFWT_MASK);
  PciSegmentAnd32 (RpBase + R_PCIE_PCIEPMECTL, Data32And);

  if (PowerGating) {
    DEBUG ((DEBUG_INFO, "Root port Power gating is enabled!\n"));

    ///
    /// SIP17: Set E8h[23] to 1b for Logical Idle Framing Error Check Filter
    /// SIP17: Set E8h[28] to 0b for Delayed Ack for Recovery Entry
    ///
    Data32And = (UINT32) ~(B_PCIE_PWRCTL_DARECE);
    Data32Or  = (UINT32) B_PCIE_PWRCTL_LIFECF;
    PciSegmentAndThenOr32 (RpBase + R_PCIE_PWRCTL, Data32And, Data32Or);

    ///
    /// Program (WPDMPGEP) E8h
    /// Set E8h[17] to 1b to wake PLL on exit from mod-PHY power gating in detect state.
    /// Set E8h[16] to 1b  for Down Configured Lanes policy (DLP)
    ///
    Data32Or = B_PCIE_PWRCTL_WPDMPGEP | B_PCIE_PWRCTL_DBUPI | B_PCIE_PWRCTL_DLP;
    PciSegmentAndThenOr32 (RpBase + R_PCIE_PWRCTL, ~0u, Data32Or);

    ///
    /// Program (R_PCIE_TNPT) 418h
    /// Set 418h[31:24] Throttle Period (TP) to 0x2
    /// Set 418h[23:16] Throttle Time (TT) to 0x0
    /// Set 418h[1] Dynamic RX Link Throttling Enable (DRXLTE) to 0b
    /// Set 418h[0] Dynamic TX Link Throttling Enable (DTXLTE) to 0b
    ///
    Data32And = (UINT32) ~(B_PCIE_TNPT_TP_MASK | B_PCIE_TNPT_TT_MASK | B_PCIE_TNPT_DRXLTE | B_PCIE_TNPT_DTXLTE);
    Data32Or = (UINT32)(V_PCIE_TNPT_TP << B_PCIE_TNPT_TP_OFFSET);
    PciSegmentAndThenOr32 (RpBase + R_PCIE_TNPT, Data32And, Data32Or);

    DEBUG_CODE_BEGIN ();
    ///
    /// Ensure PHYCLPGE is set before DLSULPPGE and FDPPGE.
    /// PHYCLPGE is programmed in CpuPcieConfigureControllerBasedPowerManagement()
    ///
    if (RpIndex != (UINT32) (GetMaxCpuPcieControllerNum ())) {
      Data32 = PciSegmentRead32 (RpBase + R_PCIE_PCIEPMECTL2);
      ASSERT ((Data32 & B_PCIE_PCIEPMECTL2_PHYCLPGE) != 0);
    }
    DEBUG_CODE_END ();

    ///
    /// Set 420h[31] = 1b to enable function disable PHY power gating.
    /// If CLKREQ# is supported or port is disabled set 420h[30,29] to 11b.
    /// 420h[29] (DLSULDLSD) and 420h[0] must be set if DLSULPPGE is set or PTOCGE is set.
    /// Assume that if PTOCGE is set CLKREQ is supported on this port.
    /// L1.LOW is disabled; if all conditions are met, it will be enabled later.
    ///
    Data32Or  = B_PCIE_PCIEPMECTL_FDPPGE | B_PCIE_PCIEPMECTL_DLSULPPGE | B_PCIE_PCIEPMECTL_DLSULDLSD | B_PCIE_PCIEPMECTL_L1FSOE;
    Data32And = (UINT32) ~(B_PCIE_PCIEPMECTL_L1OCREWD | B_PCIE_PCIEPMECTL_POFFWT_MASK | B_PCIE_PCIEPMECTL_L1SNZCREWD);

    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_PCIEPMECTL, Data32And, Data32Or);

    ///
    /// Program (R_PCIE_PCIEPMECTL2) 424h
    /// Set 424h[8] to 1b to enable dynamic controller power gating in detect state when port is function disabled.
    /// Set 424h[7] to 1b to enable dynamic controller power gating in detect state when CLKREQ# is de-asserted.
    /// Set 424h[6] to 1b to enable dynamic controller power gating in L23_Rdy state.
    /// Set 424h[5] to 1b to enable dynamic controller power gating in disabled state.
    /// Set 424h[4] to 1b to enable dynamic controller power gating in L1 sub states.
    /// SIP17: Set 424h[27] to 0b to enable Chassis PMC save and restore enable.
    ///
    Data32Or = B_PCIE_PCIEPMECTL2_FDCPGE | B_PCIE_PCIEPMECTL2_DETSCPGE | B_PCIE_PCIEPMECTL2_L23RDYSCPGE
               | B_PCIE_PCIEPMECTL2_DISSCPGE | B_PCIE_PCIEPMECTL2_L1SCPGE;
    if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
      Data32Or |= B_PCIE_PCIEPMECTL2_CPMCSRE;
    }
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_PCIEPMECTL2, ~0u, Data32Or);

    ///
    /// Program (R_PCIE_PCE) 428h
    /// Set 428h[0] to for PMC Request Enable.
    /// Set 428h[3] to 0b for Sleep Enable.
    /// Set 428h[5] to 0b. Hardware Autonomous Enable (HAE).
    ///
    Data32And = (UINT32) ~(B_PCIE_PCE_HAE | B_PCIE_PCE_SE);
    Data32Or  = B_PCIE_PCE_PMCRE;

    ///
    /// Set 428h[3] to 1b for Sleep Enable if for mobile segment.
    ///
    if (GetCpuSku () != EnumCpuTrad) {
      Data32Or |= B_PCIE_PCE_SE;
    }
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_PCE, Data32And, Data32Or);

    ///
    /// Program (R_PCIE_ADVMCTRL) 5BCh
    /// Set 5BCh[14] to 0b for ClkReq Serialization Mode.
    /// Set 5BCh[11] to 0b for Reset Receiver Link Layer Common Logic.
    /// Set 5BCh[10] to 0b for Reset Link Layer in Gen1, 2 Recovery.
    /// SIP16: Set 5BCh[15] to 0b for RxL0 DeAssertion Control
    /// SIP17: Set 5BCh[15] to 1b for RxL0 DeAssertion Control
    /// Set 5BCh[22] to 1b for InRxL0 Control.
    /// Set 5BCh[20] to 1b for EIOS Disable DeSkew.
    /// Set 5BCh[19] to 1b for EIOS Mask Receiver Datapath.
    /// Set 5BCh[13] to 1b for Gen3 Short TLP Framing Error Reporting.
    /// Set 5BCh[7:5] to 010b for PM_REQ Block and Power Gate Response time. 010b = 10 micro sec.
    ///
    Data32And = (UINT32)~(B_PCIE_ADVMCTRL_PMREQCWC_MASK | B_PCIE_ADVMCTRL_CLKREQSM | B_PCIE_ADVMCTRL_RRLLCL | B_PCIE_ADVMCTRL_RLLG12R) | (B_PCIE_ADVMCTRL_RXL0DC);
    Data32Or  = (UINT32)(B_PCIE_ADVMCTRL_INRXL0CTRL | B_PCIE_ADVMCTRL_EIOSDISDS | B_PCIE_ADVMCTRL_EIOSMASKRX | B_PCIE_ADVMCTRL_G3STFER | (0x2 << B_PCIE_ADVMCTRL_PMREQBLKPGRSPT_OFFSET) | (0x6 << B_PCIE_ADVMCTRL_PMREQCWC_OFFSET));

    if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
      Data32Or |= (B_PCIE_ADVMCTRL_RXL0DC);
      if (!IsSimicsEnvironment ()) {
        CpuPcieSip17PowerManagement (RpIndex);
      }
    }
    PciSegmentAndThenOr32 (RpBase + R_PCIE_ADVMCTRL, Data32And, Data32Or);
  }
  ///
  /// Section 3.1.8  L1 Substate
  ///

  ///
  ///L1PM Substates Supported (L1SSES) 0x204  5 1
  /// L1PM Substates Supported (L1PSS) 0x204  4 0
  /// ASPM L1.1 Supported (AL11S) 0x204  3 0
  /// ASPM L1.2 Supported (AL12S) 0x204  2 0
  /// PCI-PM L1.1 Supported (PPL11S) 0x204  1 0
  /// PCI-PM L1.2 Supported (PPL12S) 0x204  0 0
  ///
  /// NOTE!!!!!!!!!!!!
  /// Port Tpower_on Value (PTV) using from PCH
  /// Need to be characterized during post silicon
  /// Port Tpower_on Scale (PTPOS) 0x204  17:16 0x1
  /// Port Tpower_on Value (PTV) 0x204  23:19 0x5
  /// Port Common Mode Restore Time (PCMRT) 0x204  15:8 0x6E
  ///

  Data32And = (UINT32)~(B_PCIE_L1SCAP_PTV_MASK | B_PCIE_L1SCAP_PTPOS_MASK | B_PCIE_L1SCAP_PCMRT_MASK | B_PCIE_L1SCAP_PPL12S | B_PCIE_L1SCAP_PPL11S | B_PCIE_L1SCAP_AL12S | B_PCIE_L1SCAP_AL11S | B_PCIE_L1SCAP_L1PSS);
  Data32Or = (UINT32)(B_PCIE_L1SCAP_L1SSES | (V_PCIE_L1SCAP_PTV << B_PCIE_L1SCAP_PTV_OFFSET) | (V_PCIE_L1SCAP_PTPOS << B_PCIE_L1SCAP_PTPOS_OFFSET) | (V_PCIE_L1SCAP_PCMRT << B_PCIE_L1SCAP_PCMRT_OFFSET));


  if (CpuPcieHob->L1SubStates[RpIndex] != 0){
    if (RpConfig->PcieRpCommonConfig.L1Substates == CpuPcieL1SubstatesL1_1) {
      Data32And &= (UINT32)~(B_PCIE_L1SCAP_PPL12S | B_PCIE_L1SCAP_AL12S);
      Data32Or |= (UINT32)(B_PCIE_L1SCAP_PPL11S | B_PCIE_L1SCAP_AL11S | B_PCIE_L1SCAP_L1PSS);
    } else if (RpConfig->PcieRpCommonConfig.L1Substates == CpuPcieL1SubstatesL1_1_2) {
      Data32Or |= (UINT32)(B_PCIE_L1SCAP_PPL12S | B_PCIE_L1SCAP_PPL11S | B_PCIE_L1SCAP_AL12S | B_PCIE_L1SCAP_AL11S | B_PCIE_L1SCAP_L1PSS);
    }
  }

  switch (RpConfig->PcieRpCommonConfig.L1Substates) {
    case PcieDisabled:
      DEBUG((DEBUG_INFO, "RpConfig->PcieRpCommonConfig.L1Substates = Disabled\n"));
      break;
    case PcieL1SUB_1:
      DEBUG((DEBUG_INFO, "RpConfig->PcieRpCommonConfig.L1Substates = L1SUB_1\n"));
      break;
    case PcieL1SUB_1_2:
      DEBUG((DEBUG_INFO, "RpConfig->PcieRpCommonConfig.L1Substates = L1SUB_1_2\n"));
      break;
  }

  PciSegmentAndThenOr32 (RpBase + R_PCIE_L1SCAP, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_L1SCAP Value after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_L1SCAP)));

  ///
  /// CLKREQ# Acceleration Interrupt Enable (L1SSEIE) 0x208  4 1
  /// Common Mode Restore Time (CMRT) 0x208[15:8] 0x2D
  ///
  Data32And = (UINT32)~(B_PCIE_L1SCTL1_L1SSEIE | B_PCIE_L1SCAP_CMRT_MASK );
  Data32Or  = (UINT32)(B_PCIE_L1SCTL1_L1SSEIE | (V_PCIE_L1SCAP_CMRT << B_PCIE_L1SCAP_CMRT_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_L1SCTL1, Data32And, Data32Or);

  Data32And = (UINT32)~(B_PCIE_L1SCTL2_POWT_MASK | B_PCIE_L1SCTL2_TPOS_MASK);
  Data32Or = (UINT32)((V_PCIE_L1SCTL2_TPOS << B_PCIE_L1SCTL2_TPOS_OFFSET) | (V_PCIE_L1SCTL2_POWT << B_PCIE_L1SCTL2_POWT_OFFSET));

  PciSegmentAndThenOr32 (RpBase + R_PCIE_L1SCTL2, Data32And, Data32Or);

  /// L1 Substate Exit SCI Enable (L1SSESE) 0xD4  30
  PciSegmentAndThenOr32 (RpBase + R_PCIE_MPC2, ~0u, B_PCIE_MPC2_L1SSESE);
  if ((RpConfig->PcieRpCommonConfig.L1Substates == CpuPcieL1SubstatesDisabled) || (CpuPcieHob->L1SubStates[RpIndex] == 0)) {
    //
    // Set L1PGAUTOPGEN, L1PGLTREN bits if end point supports only L1, or RP doesn't support L1SS
    //
    PegPciSegmentOr16 (RpIndex, R_PCIE_PCIEPMECTL3, B_PCIE_PCIEPMECTL3_L1PGAUTOPGEN);
    Data32Or = B_PCIE_PGTHRES_L1PGLTREN | (0x2 << B_PCIE_PGTHRES_L1PGLTRTLSV_OFFSET) | (0x32 << B_PCIE_PGTHRES_L1PGLTRTLV_OFFSET);
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_PGTHRES, ~0u, Data32Or);
  }

  ///
  /// Program (R_PCIE_RPDCGEN) E1h
  /// Set E1h[0] to 1b to enable dynamic backbone clock gating.
  ///
  Data32Or = (UINT32) ((B_PCIE_RPDCGEN_RPDBCGEN) << 8); ///< For 32-bit boundary alignment for 0xE1 (RPDCGEN), shift by 8bits from 0xE0 to reach 0xE1
  PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_SPR, ~0u, Data32Or);
}

/**
  Set Gen3 coefficient list entry.

  @param[in] RpBase      Root Port pci segment base address
  @param[in] ListEntry   ListEntry (0-9)
  @param[in] Cm          C-1
  @param[in] Cp          C+1
**/
VOID
CpuPcieSetGen3Presets (
  UINT8  RpIndex,
  UINT32 ListEntry,
  UINT32 Cm,
  UINT32 Cp
  )
{
  UINT32  PreReg;
  UINT32  PostReg;
  UINT32  PreField;
  UINT32  PostField;
  UINT32  Data32And;
  UINT32  Data32Or;
  UINT64  RpBase;

  DEBUG ((DEBUG_VERBOSE, "CpuPcieSetGen3Presets Start!\n"));

  RpBase = CpuPcieBase (RpIndex);

  ASSERT (ListEntry < 10);
  ASSERT ((Cm & ~0x3F) == 0);
  ASSERT ((Cp & ~0x3F) == 0);
  ///
  /// CPU PCIe has 5 pairs of coefficients Cm (Co-efficient minus) and Cp (Co-efficient Plus) which is stored in RTPCL registers.
  /// Each of this is stored in 5 bitfields in RTPCL and is covered in 2 registers.
  ///
  PreReg    = ((ListEntry * 2)) / 5;
  PreField  = ((ListEntry * 2)) % 5;
  PostReg   = ((ListEntry * 2) + 1) / 5;
  PostField = ((ListEntry * 2) + 1) % 5;

  ASSERT (PreReg  < 2);
  ASSERT (PostReg < 2);

  Data32And = (UINT32) ~(0x3F << (6 * PreField));
  Data32Or  = (Cm << (6 * PreField));
  ASSERT ((Data32And & Data32Or) == 0);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_RTPCL1 + (PreReg  * 4), Data32And, Data32Or);

  Data32And = (UINT32) ~(0x3F << (6 * PostField));
  Data32Or  = (Cp << (6 * PostField));
  ASSERT ((Data32And & Data32Or) == 0);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_RTPCL1 + (PostReg * 4), Data32And, Data32Or);

  DEBUG ((DEBUG_INFO, "Port %d list %d: (%d,%d)\n",
          RpIndex, ListEntry, Cm, Cp));
  DEBUG ((DEBUG_VERBOSE, "CpuPcieSetGen3Presets End!\n"));
}

/**
  Populate CPU PCIe Gen3 HW EQ coefficient search list.
  @param[in] RpBase    Root Port base address
  @param[in] Params    Equalization parameters
**/
VOID
CpuPcieGen3InitializePresets (
  UINT8                  RpIndex,
  UINT64                 RpBase,
  CONST CPU_PCIE_CONFIG  *CpuPcieRpConfig
  )
{
  UINT32     Index;
  UINT32     Register;
  UINT8      Gen3Cm[PCIE_HWEQ_COEFFS_MAX] = { 0, 2, 4, 0, 0 };
  UINT8      Gen3Cp[PCIE_HWEQ_COEFFS_MAX] = { 1, 3, 0, 0, 0 };

  DEBUG ((DEBUG_INFO, "CpuPcieGen3InitializePresets Start!\n"));
  Register = R_PCIE_RTPCL1;
  for (Index = 0; Index < PCIE_HWEQ_COEFFS_MAX; ++Index) {
    Gen3Cm[Index] = CpuPcieRpConfig->PcieCommonConfig2.HwEqGen3CoeffList[RpIndex][Index].Cm;
    Gen3Cp[Index] = CpuPcieRpConfig->PcieCommonConfig2.HwEqGen3CoeffList[RpIndex][Index].Cp;
    CpuPcieSetGen3Presets (
      RpIndex,
      Index,
      Gen3Cm[Index],
      Gen3Cp[Index]
      );
  }
  for (Index = 0; Index < 4; ++Index) {
    DEBUG ((DEBUG_INFO, "RTPCL%d = 0x%08x\n", Index, PciSegmentRead32 (RpBase + Register + (Index * 4))));
  }
  DEBUG ((DEBUG_INFO, "CpuPcieGen3InitializePresets End!\n"));
}

/**
  Set Gen4 Preset entry.

  @param[in] RpBase      Root Port pci segment base address
  @param[in] ListEntry   ListEntry (0-9)
  @param[in] Cm          C-1
  @param[in] Cp          C+1
**/
VOID
CpuPcieSetGen4Presets (
  UINT8  RpIndex,
  UINT32 ListEntry,
  UINT32 Cm,
  UINT32 Cp
  )
{
  UINT32  PreReg;
  UINT32  PostReg;
  UINT32  PreField;
  UINT32  PostField;
  UINT32  Data32And;
  UINT32  Data32Or;
  UINT64  RpBase;

  DEBUG ((DEBUG_VERBOSE, "CpuPcieSetGen4Presets Start!\n"));

  RpBase = CpuPcieBase (RpIndex);

  ASSERT (ListEntry < 10);
  ASSERT ((Cm & ~0x3F) == 0);
  ASSERT ((Cp & ~0x3F) == 0);
  ///
  /// CPU PCIe has 5 pairs of coefficients Cm (Co-efficient minus) and Cp (Co-efficient Plus) which is stored in RTPCL registers.
  /// Each of this is stored in 5 bitfields in RTPCL and is covered in 2 registers.
  ///
  PreReg    = ((ListEntry * 2)) / 5;
  PreField  = ((ListEntry * 2)) % 5;
  PostReg   = ((ListEntry * 2) + 1) / 5;
  PostField = ((ListEntry * 2) + 1) % 5;

  ASSERT (PreReg  < 2);
  ASSERT (PostReg < 2);

  Data32And = (UINT32) ~(0x3F << (6 * PreField));
  Data32Or  = (Cm << (6 * PreField));
  ASSERT ((Data32And & Data32Or) == 0);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_PX16GRTPCL1 + (PreReg  * 4), Data32And, Data32Or);

  Data32And = (UINT32) ~(0x3F << (6 * PostField));
  Data32Or  = (Cp << (6 * PostField));
  ASSERT ((Data32And & Data32Or) == 0);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_PX16GRTPCL1 + (PostReg * 4), Data32And, Data32Or);

  DEBUG ((DEBUG_INFO, "Port %d list %d: (%d,%d)\n",
          RpIndex, ListEntry, Cm, Cp));

  DEBUG ((DEBUG_VERBOSE, "CpuPcieSetGen4Presets End!\n"));
}

/**
  Populate CPU PCIe Gen4 HW EQ coefficient search list.
  @param[in] RpBase    Root Port base address
  @param[in] Params    Equalization parameters
**/
VOID
CpuPcieGen4InitializePresets (
  UINT8                  RpIndex,
  UINT64                 RpBase,
  CONST CPU_PCIE_CONFIG  *CpuPcieRpConfig
  )
{
  UINT32     Index;
  UINT32     Register;
  UINT8      Gen4Cm[PCIE_HWEQ_COEFFS_MAX] = { 0, 8, 0, 0, 0 };
  UINT8      Gen4Cp[PCIE_HWEQ_COEFFS_MAX] = { 7, 9, 0, 0, 0 };

  DEBUG ((DEBUG_INFO, "CpuPcieGen4InitializePresets Start!\n"));
  Register = R_PCIE_PX16GRTPCL1;
  for (Index = 0; Index < PCIE_HWEQ_COEFFS_MAX; ++Index) {
    Gen4Cm[Index] = CpuPcieRpConfig->PcieCommonConfig2.HwEqGen4CoeffList[RpIndex][Index].Cm;
    Gen4Cp[Index] = CpuPcieRpConfig->PcieCommonConfig2.HwEqGen4CoeffList[RpIndex][Index].Cp;
    CpuPcieSetGen4Presets (
      RpIndex,
      Index,
      Gen4Cm[Index],
      Gen4Cp[Index]
      );
  }
  for (Index = 0; Index < 4; ++Index) {
    DEBUG ((DEBUG_INFO, "PX16GRTPCL%d = 0x%08x\n", Index, PciSegmentRead32 (RpBase + Register + (Index * 4))));
  }
  DEBUG ((DEBUG_INFO, "CpuPcieGen4InitializePresets End!\n"));
}

/**
  Reset and enable Recovery Entry and Idle Framing Error Count

  @param[in] RpBase    Root Port base address
**/
STATIC
VOID
CpuPcieResetErrorCounts (
  UINT64 RpBase
  )
{
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32) ~B_PCIE_EQCFG1_REIFECE);
  PciSegmentOr32 (RpBase + R_PCIE_EQCFG1, B_PCIE_EQCFG1_REIFECE);
}

UINT32
CpuPcieGetMonitor (
  UINT64 RpBase,
  UINT32 RpLaneIndex,
  UINT8  CpuPcieSpeed
  )
{
  UINT32  ReturnValue;
  DEBUG ((DEBUG_VERBOSE, "CpuPcieGetMonitor\n"));
  ReturnValue = 0;
DEBUG_CODE_BEGIN ();
  if (CpuPcieSpeed == CpuPcieGen3) {
    PciSegmentAndThenOr32 (RpBase + R_PCIE_MM, (UINT32)~B_PCIE_MM_MSS_MASK, (UINT32)0x0F);
  } else if (CpuPcieSpeed == CpuPcieGen4) {
    PciSegmentAndThenOr32 (RpBase + R_PCIE_MM, (UINT32)~B_PCIE_MM_MSS_MASK, (UINT32)0x10);
  }
  PciSegmentAndThenOr32 (RpBase + R_PCIE_CDM, (UINT32)~B_PCIE_CDM_MSS_MASK, (UINT32)RpLaneIndex);
  ReturnValue = PciSegmentRead32 (RpBase + R_PCIE_MM) >> B_PCIE_MM_MSST_OFFSET;
DEBUG_CODE_END ();
  return ReturnValue;
}

VOID
CpuPcieHwEqDumpBestCoeffs (
  UINT64 RpBase,
  UINT32 RpLaneIndex,
  UINT8  CpuPcieSpeed
  )
{
DEBUG_CODE_BEGIN ();
  UINT32 Value;

  Value = CpuPcieGetMonitor (RpBase, RpLaneIndex, CpuPcieSpeed);
  DEBUG ((DEBUG_INFO, "MM.MSS value: 0x%08x\n", Value));
  DEBUG ((DEBUG_INFO, "Lane %x Best Coeffiecient: (0x%x, 0x%x)\n", RpLaneIndex, mCpuPcieEqParamList[Value][0].Cm, mCpuPcieEqParamList[Value][1].Cp));

DEBUG_CODE_END ();
}

/**
  Dump HW EQ scoreboard.

  @param[in] RpIndex            Root port index to Dump HW EQ scoreboard
  @param[in] CpuPcieSpeed       Speed of the CPU Pcie
**/
VOID
CpuPcieHwEqDumpFoms (
  UINT32 RpIndex,
  UINT8  CpuPcieSpeed
  )
{
DEBUG_CODE_BEGIN ();
  UINT32 RpLaneIndex;
  UINT32 Index;
  UINT32 Foms;
  UINT32 TimeoutUs;
  UINT32 MaxLinkWidth;
  UINT64 RpBase;

  RpBase = CpuPcieBase (RpIndex);
  if (CpuPcieIsLinkActive (RpBase) == 0) {
    DEBUG ((DEBUG_ERROR, "No link!\n"));
    return;
  }

  //
  // Wait for link training to complete
  //
  for (TimeoutUs = 0; TimeoutUs < 50000; TimeoutUs += 10) {
    if ((PciSegmentRead16 (RpBase + R_PCIE_LSTS) & B_PCIE_LSTS_LT) == 0) {
      break;
    }
    MicroSecondDelay (10);
  }
  DEBUG ((DEBUG_INFO, "EQCFG1: 0x%08x\n", PciSegmentRead32 (RpBase + R_PCIE_EQCFG1)));
  DEBUG ((DEBUG_INFO, "LSTS2:  0x%04x\n", PciSegmentRead16 (RpBase + R_PCIE_LSTS2)));
  DEBUG ((DEBUG_INFO, "Training time = %dus\n", TimeoutUs));
  DEBUG ((DEBUG_INFO, "Controller : %d\n", GetCpuPcieControllerNum(RpIndex)));
  MaxLinkWidth = CpuPcieGetMaxLinkWidth (RpBase);
  for (RpLaneIndex = 0; RpLaneIndex < MaxLinkWidth; ++RpLaneIndex) {
    DEBUG ((DEBUG_INFO, "%d.%d FOM Scoreboard", RpIndex, RpLaneIndex));
    for (Index = 0; Index < 11; Index += 3) {
      PciSegmentAndThenOr32 (
        RpBase + R_PCIE_FOMS,
        (UINT32)     ~(B_PCIE_FOMS_I  |            B_PCIE_FOMS_LN),
        ((Index/3)  << B_PCIE_FOMS_I_OFFSET) | (RpLaneIndex << B_PCIE_FOMS_LN_OFFSET)
        );
      Foms = PciSegmentRead32 (RpBase + R_PCIE_FOMS);
      List[Index] = (Foms & B_PCIE_FOMS_FOMSV0) >> B_PCIE_FOMS_FOMSV0_OFFSET;
      List[Index + 1] = (Foms & B_PCIE_FOMS_FOMSV1) >> B_PCIE_FOMS_FOMSV1_OFFSET;
      List[Index + 2] = (Foms & B_PCIE_FOMS_FOMSV2) >> B_PCIE_FOMS_FOMSV2_OFFSET;
      DEBUG ((DEBUG_INFO, "%d:%03d  ", Index,     (Foms & B_PCIE_FOMS_FOMSV0) >> B_PCIE_FOMS_FOMSV0_OFFSET));
      DEBUG ((DEBUG_INFO, "%d:%03d  ", Index + 1, (Foms & B_PCIE_FOMS_FOMSV1) >> B_PCIE_FOMS_FOMSV1_OFFSET));
      DEBUG ((DEBUG_INFO, "%d:%03d  ", Index + 2, (Foms & B_PCIE_FOMS_FOMSV2) >> B_PCIE_FOMS_FOMSV2_OFFSET));
    }
    DEBUG ((DEBUG_INFO, "\n"));
    PciSegmentAndThenOr32 (
      RpBase + R_PCIE_FOMS,
      (UINT32)     ~(B_PCIE_FOMS_I  |            B_PCIE_FOMS_LN),
      ((7)  << B_PCIE_FOMS_I_OFFSET) | (RpLaneIndex << B_PCIE_FOMS_LN_OFFSET)
      );
    Foms = PciSegmentRead32 (RpBase + R_PCIE_FOMS);
    DEBUG ((DEBUG_INFO, "FOMS : %x\n", Foms));
    if (Foms & BIT0) {
      List[0] = 0;
      DEBUG ((DEBUG_INFO, "List 0 is invalid\n"));
    } if (Foms & BIT1) {
      List[1] = 0;
      DEBUG ((DEBUG_INFO, "List 1 is invalid\n"));
    } if (Foms & BIT2) {
      List[2] = 0;
      DEBUG ((DEBUG_INFO, "List 2 is invalid\n"));
    } if (Foms & BIT3) {
      List[3] = 0;
      DEBUG ((DEBUG_INFO, "List 3 is invalid\n"));
    } if (Foms & BIT4) {
      List[4] = 0;
      DEBUG ((DEBUG_INFO, "List 4 is invalid\n"));
    } if (Foms & BIT5) {
      List[5] = 0;
      DEBUG ((DEBUG_INFO, "List 5 is invalid\n"));
    } if (Foms & BIT6) {
      List[6] = 0;
      DEBUG ((DEBUG_INFO, "List 6 is invalid\n"));
    } if (Foms & BIT7) {
      List[7] = 0;
      DEBUG ((DEBUG_INFO, "List 7 is invalid\n"));
    } if (Foms & BIT8) {
      List[8] = 0;
      DEBUG ((DEBUG_INFO, "List 8 is invalid\n"));
    } if (Foms & BIT9) {
      List[9] = 0;
      DEBUG ((DEBUG_INFO, "List 9 is invalid\n"));
    }
    CpuPcieHwEqDumpBestCoeffs (RpBase, RpLaneIndex, CpuPcieSpeed);
  }
DEBUG_CODE_END ();
}


/**
  Dump CPU PCIe Gen3 HW EQ Registers.
  @param[in] RpBase       Equaliztion context structure
**/
VOID
CpuPcieGen3DumpHwEqRegs (
  UINT64 RpBase
  )
{
  DEBUG ((DEBUG_VERBOSE, "EQCFG1: 0x%08x\n", PciSegmentRead32 (RpBase + R_PCIE_EQCFG1)));
  DEBUG ((DEBUG_VERBOSE, "EQCFG2: 0x%08x\n", PciSegmentRead32 (RpBase + R_PCIE_EQCFG2)));
  DEBUG ((DEBUG_VERBOSE, "HAEQ:   0x%08x\n", PciSegmentRead32 (RpBase + R_PCIE_HAEQ)));
  DEBUG ((DEBUG_VERBOSE, "L01EC:  0x%08x\n", PciSegmentRead32 (RpBase + R_PCIE_L01EC)));
  DEBUG ((DEBUG_VERBOSE, "L23EC:  0x%08x\n", PciSegmentRead32 (RpBase + R_PCIE_L23EC)));
}

/**
  Dump CPU PCIe Gen4 HW EQ Registers.
  @param[in] RpBase       Equaliztion context structure
**/
VOID
CpuPcieGen4DumpHwEqRegs (
  UINT64 RpBase
  )
{
  DEBUG ((DEBUG_VERBOSE, "EQCFG4: 0x%08x\n", PciSegmentRead32 (RpBase + R_PCIE_EQCFG4)));
  DEBUG ((DEBUG_VERBOSE, "EQCFG5: 0x%08x\n", PciSegmentRead32 (RpBase + R_PCIE_EQCFG5)));
  DEBUG ((DEBUG_VERBOSE, "PL16L01EC:  0x%08x\n", PciSegmentRead16 (RpBase + R_PCIE_PL16L01EC)));
  DEBUG ((DEBUG_VERBOSE, "PL16L23EC:  0x%08x\n", PciSegmentRead16 (RpBase + R_PCIE_PL16L23EC)));
}

/**
  Configures rootport for hardware Gen3 link equalization.
  @param[in] RpIndex              Root Port Index for which HW EQ is to be performed
  @param[in] RpBase               Root Port Base
  @param[in] TempPciBus           The temporary Bus number for root port initialization
  @param[in] Params               Equalization parameters
**/
VOID
CpuPcieDoGen3HardwareEq (
  UINT8                       RpIndex,
  UINT64                      RpBase,
  UINT8                       TempPciBus,
  CONST CPU_PCIE_CONFIG       *Params
  )
{
  UINT32                            Data32Or;
  UINT32                            Data32And;
  UINT32                            Gen3Uptp;
  UINT32                            Gen3Dptp;
  UINT8                             Gen3PcetTimer;
  UINT8                             Gen3TsLockTimer;
  UINT8                             Gen3NumberOfPresetCoeffList;
  UINT8                             Gen3PresetCoeffSelection;
  UINT32                            LaneIndex;
  UINT32                            LaneRegisterIndex;
  UINT32                            LaneIndexInsideRegister;
  UINT32                            MaxLinkWidth;

  DEBUG ((DEBUG_INFO, "CpuPcieDoGen3HardwareEq Start!\n"));
  mDoGen3HwEqRpMask |= BIT0 << RpIndex;
  DEBUG ((DEBUG_INFO, "LSTS2: 0x%04x\n", PciSegmentRead16 (RpBase + R_PCIE_LSTS2)));

  MaxLinkWidth = CpuPcieGetMaxLinkWidth (RpBase);

  //
  // Programming done as per section 5.3.1.2 Linear Mode Hardware Flow ( Gen3 )
  // SIP17 Converge PCIe BWG r0.9
  //

  if (IsIrrcProgrammingRequired (RpIndex)) {
    PciSegmentOr8 (RpBase + R_PCIE_EQCFG1, B_PCIE_EQCFG1_EQTS2IRRC);
  }

  //
  // Step1: Program all the preset-coeff mapping registers - done in CpuPcieTxPresetOverride() fn
  //
  // Preset to Coefficient programming are per controller registers
  switch (GetCpuPcieSipInfo (RpIndex)) {
    case PCIE_SIP17:
      CpuPcieSip17Gen3PresetToCoeff (RpIndex);
      // L0LFFS is per port register
      PciSegmentWrite32 (RpBase + R_PCIE_L0LFFS, 0x00003010);
      break;
    case PCIE_SIP16:
      CpuPcieSip16Gen3PresetToCoeff (RpIndex);
      // L0LFFS is per port register
      PciSegmentWrite32 (RpBase + R_PCIE_L0LFFS, 0x00003F15);
      break;
    default:
      ASSERT (FALSE);
      return;
  }

  //
  // Step2:
  // Program LCTL2.TLS to Gen3 for Compliacne Mode
  //
  if(mCpuPcieRpConfig->PcieCommonConfig.ComplianceTestMode) {
    PciSegmentAndThenOr32 (RpBase + R_PCIE_LCTL2, (UINT16)~B_PCIE_LCTL2_TLS, V_PCIE_LCAP_MLS_GEN3);
  }

  //
  // Step3:Program RTPCL1.PCM = 0 to enable Preset mode - Gen3 Preset/Coefficient Mode Selection
  //

  Gen3PresetCoeffSelection = Params->RootPort2[RpIndex].PcieGen3PresetCoeffSelection;
  DEBUG ((DEBUG_INFO, "Gen3 Preset/Coefficient Mode Selection %x\n", Gen3PresetCoeffSelection));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_RTPCL1, (UINT32)~(B_PCIE_RTPCL1_PCM), (Gen3PresetCoeffSelection << B_PCIE_RTPCL1_PCM_OFFSET));

    DEBUG((DEBUG_INFO, "TxOverride Disabled\n"));
    for (LaneIndex = 0; LaneIndex < 4; LaneIndex++) {
      LaneRegisterIndex = LaneIndex / 2;
      LaneIndexInsideRegister = LaneIndex % 2;
      PciSegmentAnd32 (RpBase + R_PCIE_LTCO1 + (4 * LaneRegisterIndex),
        ~(B_PCIE_LTCO1_L0TCOE << LaneIndexInsideRegister)
      );
    }

    for (LaneIndex = 4; LaneIndex < MaxLinkWidth; LaneIndex++) {
      LaneRegisterIndex = LaneIndex / 2;
      LaneIndexInsideRegister = LaneIndex % 2;
      PciSegmentAnd32 (RpBase + R_PCIE_LTCO3 + (4 * LaneRegisterIndex),
        ~(B_PCIE_LTCO1_L0TCOE << LaneIndexInsideRegister)
      );
    }


  //
  // Step4:Program the Presets
  // Note : Requires silicon characterization
  //
  CpuPcieGen3InitializePresets (RpIndex, RpBase, Params);
  //
  // Step5:Program EQCFG1.LEB to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32)~(B_PCIE_EQCFG1_LEB));
  //
  // Step6:Program EQCFG1.LEP23B to 0 so that Phase 2 and Phase 3 EQ is not bypass
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1,  (UINT32)~(B_PCIE_EQCFG1_LEP23B));
  //
  // Step7:Program EQCFG1.LEP3B to 0 so that Phase 3 EQ is not bypass
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1,  (UINT32)~(B_PCIE_EQCFG1_LEP3B));
  //
  // Step8:Program EQCFG1 RTLEPCEB to 0 to enable HWEQ
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32)~B_PCIE_EQCFG1_RTLEPCEB);
  //
  // Step9:Program EQCFG1.RTPCOE to 0 enable Hardware Search Algorithm
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32)~B_PCIE_EQCFG1_RTPCOE);
  //
  // Step10:Clear EQCFG1.HPCMQE to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32)~B_PCIE_EQCFG1_HPCMQE);
  //
  // Step11:Rx wait time for each new eq should be configured to 1us
  //
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG1, (UINT32)~B_PCIE_EQCFG1_RWTNEVE_MASK, (UINT32)V_PCIE_EQCFG1_RWTNEVE_1US << B_PCIE_EQCFG1_RWTNEVE_OFFSET);
  //
  // Step12: For Linear Mode, BIOS needs to program the EQCFG1.MFLNTL to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32)~B_PCIE_EQCFG1_MFLNTL);
  //
  // Step13: Program EQCFG4.PX8GTSWLPCE as per post silicon charecterization - Gen3 TS Lock Timer
  //
  Gen3TsLockTimer = 2;
  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP16) {
    Gen3TsLockTimer = 3;
  }
  DEBUG ((DEBUG_INFO, "Gen3 TS Lock Timer = %d\n", Gen3TsLockTimer));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG4, (UINT32)~B_PCIE_EQCFG4_PX8GTSWLPCE_MASK , (Gen3TsLockTimer << B_PCIE_EQCFG4_PX8GTSWLPCE_OFFSET));
  //
  // Step14: Program EQCFG1.TUPP to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32)~B_PCIE_EQCFG1_TUPP);
  //
  // Step15: Program EQCFG1.RUPP to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32)~B_PCIE_EQCFG1_RUPP);
  //
  // Step16: BIOS would program the Receiver Eye Margin Error Threshold and Multiplier to 4 errors; in
  // which the EQCFG2.REWMETM would be set to 00b and EQCFG2.REWMET would be set to 10h
  //
  Data32And = (UINT32)~(B_PCIE_EQCFG2_REWMETM |
                         B_PCIE_EQCFG2_REWMET);
  Data32Or = (0 << B_PCIE_EQCFG2_REWMETM_OFFSET) | 0x02; // REWMET = 2 (4 errors)
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG2, Data32And, Data32Or);
  //
  // Step17,18: Program EQCFG2.NTIC,EQCFG2.EMD  to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG2, (UINT32)~(B_PCIE_EQCFG2_NTIC | B_PCIE_EQCFG2_EMD));
  //
  // Step19: Program EQCFG2.PECT as below - Gen3 PCET Timer
  //
  Gen3PcetTimer = 11;
  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP16) { 
    Gen3PcetTimer = 4;
  }
  DEBUG ((DEBUG_INFO, "Gen3 PCET timeout = %d ms\n",Gen3PcetTimer));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG2, (UINT32)~(B_PCIE_EQCFG2_PCET), (Gen3PcetTimer << B_PCIE_EQCFG2_PCET_OFFSET));
  //
  // Step20: Program EQCFG2.HAPCSB as below and clear HAPCCPIE - Gen3 Number of Preset/Coefficient List
  //
  Gen3NumberOfPresetCoeffList = 0;
  DEBUG ((DEBUG_INFO, "Number Of Preset/Coefficient List = %d\n", Gen3NumberOfPresetCoeffList));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG2, (UINT32)~B_PCIE_EQCFG2_HAPCSB, Gen3NumberOfPresetCoeffList << B_PCIE_EQCFG2_HAPCSB_OFFSET);
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32)~(B_PCIE_EQCFG1_HAPCCPIE));
  //
  // Step21,22: Program EQCFG2.NTEME and EQCFG2.MPEME to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG2, (UINT32)~(B_PCIE_EQCFG2_NTEME | B_PCIE_EQCFG2_MPEME));
  //
  // Step23,24: Program EQCFG1.MEQSMMFLNTL and EQCFG1.MFLNTL to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32)~(B_PCIE_EQCFG1_MEQSMMFLNTL | B_PCIE_EQCFG1_MFLNTL));
  //
  // Step25: Program EQCFG1.REIC to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG1, (UINT32)~(B_PCIE_EQCFG1_REIC));

  //
  // Step26: Program the HAEQ.HAPCCPI 0 to allow 1 iteration of Recovery.Equalization entry
  //
  PciSegmentAndThenOr32 (RpBase + R_PCIE_HAEQ, (UINT32)~B_PCIE_HAEQ_HAPCCPI, 0 << B_PCIE_HAEQ_HAPCCPI_OFFSET);
  //
  // Step27-31: Program HAEQ.FOMEM to 0 | HAEQ.MACFOMC to 0 | HAEQ.SL to 0 | HAEQ.DL to 0xE | HAEQ.SFOMFM to 0
  //
  Data32And = (UINT32)~(B_PCIE_HAEQ_FOMEM |
                        B_PCIE_HAEQ_MACFOMC |
                        B_PCIE_HAEQ_SL |
                        B_PCIE_HAEQ_DL |
                        B_PCIE_HAEQ_SFOMFM);

  Data32Or = (0xE << B_PCIE_HAEQ_DL_OFFSET);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_HAEQ, Data32And, Data32Or);

  //
  // Step32: Program the following register for the Downstream Port Transmitter Preset value to P7
  //
  ///
  /// Configure Transmitter Preset for each Upstream and Downstream Port Lane:
  /// 1.  Set L01EC.DPL0TP, Dxx:Fn + 22Ch[3:0]    = 7
  /// 2.  Set L01EC.UPL0TP, Dxx:Fn + 22Ch[11:8]   = 7
  /// 3.  Set L01EC.DPL1TP, Dxx:Fn + 22Ch[19:16]  = 7
  /// 4.  Set L01EC.UPL1TP, Dxx:Fn + 22Ch[27:24]  = 7
  /// 5.  Set L23EC.DPL2TP, Dxx:Fn + 230h[3:0]    = 7
  /// 6.  Set L23EC.UPL2TP, Dxx:Fn + 230h[11:8]   = 7
  /// 7.  Set L23EC.DPL3TP, Dxx:Fn + 230h[19:16]  = 7
  /// 8.  Set L23EC.UPL3TP, Dxx:Fn + 230h[27:24]  = 7
  ///    Upto L23EC.UPL3TP, Dxx:Fn + 258h[27:24]  = 7
  ///
  Gen3Uptp = Params->RootPort[RpIndex].Gen3Uptp;
  Gen3Dptp = Params->RootPort[RpIndex].Gen3Dptp;
  Data32And = (UINT32) ~(B_PCIE_L01EC_UPL1TP_MASK | B_PCIE_L01EC_DPL1TP_MASK | B_PCIE_L01EC_UPL0TP_MASK | B_PCIE_L01EC_DPL0TP_MASK);
  Data32Or = ((Gen3Uptp << B_PCIE_L01EC_UPL1TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L01EC_DPL1TP_OFFSET) |
              (Gen3Uptp << B_PCIE_L01EC_UPL0TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L01EC_DPL0TP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_L01EC, Data32And, Data32Or);

  Data32And = (UINT32) ~(B_PCIE_L23EC_UPL3TP_MASK | B_PCIE_L23EC_DPL3TP_MASK | B_PCIE_L23EC_UPL2TP_MASK | B_PCIE_L23EC_DPL2TP_MASK);
  Data32Or = ((Gen3Uptp << B_PCIE_L23EC_UPL3TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L23EC_DPL3TP_OFFSET) |
              (Gen3Uptp << B_PCIE_L23EC_UPL2TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L23EC_DPL2TP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_L23EC, Data32And, Data32Or);

  Data32And = (UINT32)~(B_PCIE_L45EC_UPL5TP_MASK | B_PCIE_L45EC_DPL5TP_MASK | B_PCIE_L45EC_UPL4TP_MASK | B_PCIE_L45EC_DPL4TP_MASK);
  Data32Or = ((Gen3Uptp << B_PCIE_L45EC_UPL5TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L45EC_DPL5TP_OFFSET) |
              (Gen3Uptp << B_PCIE_L45EC_UPL4TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L45EC_DPL4TP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_L45EC, Data32And, Data32Or);

  Data32And = (UINT32)~(B_PCIE_L67EC_UPL7TP_MASK | B_PCIE_L67EC_DPL7TP_MASK | B_PCIE_L67EC_UPL6TP_MASK | B_PCIE_L67EC_DPL6TP_MASK);
  Data32Or = ((Gen3Uptp << B_PCIE_L67EC_UPL7TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L67EC_DPL7TP_OFFSET) |
              (Gen3Uptp << B_PCIE_L67EC_UPL6TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L67EC_DPL6TP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_L67EC, Data32And, Data32Or);

  Data32And = (UINT32)~(B_PCIE_L89EC_UPL9TP_MASK | B_PCIE_L89EC_DPL9TP_MASK | B_PCIE_L89EC_UPL8TP_MASK | B_PCIE_L89EC_DPL8TP_MASK);
  Data32Or = ((Gen3Uptp << B_PCIE_L89EC_UPL9TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L89EC_DPL9TP_OFFSET) |
              (Gen3Uptp << B_PCIE_L89EC_UPL8TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L89EC_DPL8TP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_L89EC, Data32And, Data32Or);

  Data32And = (UINT32)~(B_PCIE_L1011EC_UPL11TP_MASK | B_PCIE_L1011EC_DPL11TP_MASK | B_PCIE_L1011EC_UPL10TP_MASK | B_PCIE_L1011EC_DPL10TP_MASK);
  Data32Or = ((Gen3Uptp << B_PCIE_L1011EC_UPL11TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L1011EC_DPL11TP_OFFSET) |
              (Gen3Uptp << B_PCIE_L1011EC_UPL10TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L1011EC_DPL10TP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_L1011EC, Data32And, Data32Or);

  Data32And = (UINT32)~(B_PCIE_L1213EC_UPL13TP_MASK | B_PCIE_L1213EC_DPL13TP_MASK | B_PCIE_L1213EC_UPL12TP_MASK | B_PCIE_L1213EC_DPL12TP_MASK);
  Data32Or = ((Gen3Uptp << B_PCIE_L1213EC_UPL13TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L1213EC_DPL13TP_OFFSET) |
              (Gen3Uptp << B_PCIE_L1213EC_UPL12TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L1213EC_DPL12TP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_L1213EC, Data32And, Data32Or);

  Data32And = (UINT32)~(B_PCIE_L1415EC_UPL15TP_MASK | B_PCIE_L1415EC_DPL15TP_MASK | B_PCIE_L1415EC_UPL14TP_MASK | B_PCIE_L1415EC_DPL14TP_MASK);
  Data32Or = ((Gen3Uptp << B_PCIE_L1415EC_UPL15TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L1415EC_DPL15TP_OFFSET) |
              (Gen3Uptp << B_PCIE_L1415EC_UPL14TP_OFFSET) |
              (Gen3Dptp << B_PCIE_L1415EC_DPL14TP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_L1415EC, Data32And, Data32Or);
  //
  // Step33-34: Program Perform EQ LCTL3.PE to 1 and LCTL.RL = 1 = these steps will be done by the caller if endpoint supports only upto Gen3, otherwise SaDoGen4HardwareEq will be called
  //

  DEBUG ((DEBUG_INFO, "CpuPcieDoGen3HardwareEq End!\n"));
}

/**
  Configures rootport for hardware Gen4 link equalization.
  @param[in] RpIndex              Root Port Index for which HW EQ is to be performed
  @param[in] RpBase               Root Port Base
  @param[in] Params               Equalization parameters
**/
VOID
CpuPcieDoGen4HardwareEq (
  UINT8                      RpIndex,
  UINT64                      RpBase,
  CONST CPU_PCIE_CONFIG       *Params
  )
{
  UINT16                            Data16Or;
  UINT16                            Data16And;
  UINT16                            Gen4Uptp;
  UINT16                            Gen4Dptp;
  UINT8                             Gen4PcetTimer;
  UINT8                             Gen4TsLockTimer;
  UINT8                             Gen4NumberOfPresetCoeffList;
  UINT8                             Gen4PresetCoeffSelection;
  UINT8                             Gen4Px16Ghapccpi;
  UINT32                            LaneIndex;
  UINT32                            LaneRegisterIndex;
  UINT32                            LaneIndexInsideRegister;
  UINT32                            MaxLinkWidth;

  DEBUG ((DEBUG_INFO, "CpuPcieDoGen4HardwareEq Start!\n"));

  mDoGen4HwEqRpMask |= BIT0 << RpIndex;
  DEBUG ((DEBUG_INFO, "LSTS2: 0x%04x\n", PciSegmentRead16 (RpBase + R_PCIE_LSTS2)));

  MaxLinkWidth = CpuPcieGetMaxLinkWidth (RpBase);

  //
  // Programming done as per section 5.3.1.3 Linear Mode Hardware Flow ( Gen4 )
  // SIP17 Converge PCIe BWG r0.90
  //

  if (IsIrrcProgrammingRequired (RpIndex)) {
    PciSegmentOr8 (RpBase + R_PCIE_EQCFG4, B_PCIE_EQCFG4_PX16GEQTS2IRRC);
  }

  //
  // Step1: Program all the preset-coeff mapping registers - done in CpuPcieTxPresetOverride() fn
  //
  // Preset to Coefficient programming are per controller registers
  switch (GetCpuPcieSipInfo (RpIndex)) {
    case PCIE_SIP17:
      CpuPcieSip17Gen4PresetToCoeff (RpIndex);
      // R_PCIE_PX16GLFFS is per port register
      PciSegmentWrite32 (RpBase + R_PCIE_PX16GLFFS, 0x00003010);
      break;
    case PCIE_SIP16:
      CpuPcieSip16Gen4PresetToCoeff (RpIndex);
      // R_PCIE_PX16GLFFS is per port register
      PciSegmentWrite32 (RpBase + R_PCIE_PX16GLFFS, 0x00003F15);
      break;
    default:
      ASSERT (FALSE);
      return;
  }

  //
  // Step2:
  // Program LCTL2.TLS to Gen4 for Compliace Mode
  //
  if(mCpuPcieRpConfig->PcieCommonConfig.ComplianceTestMode) {
    PciSegmentAndThenOr32 (RpBase + R_PCIE_LCTL2, (UINT16)~B_PCIE_LCTL2_TLS, V_PCIE_LCAP_MLS_GEN4);
  }

  //
  // Step3:Program PX16GRTPCL1.PCM = 0 to enable Preset mode - Gen4 Preset/Coefficient Mode Selection
  //

  Gen4PresetCoeffSelection = Params->RootPort2[RpIndex].PcieGen4PresetCoeffSelection;
  DEBUG((DEBUG_INFO, "Gen4 Preset/Coefficient Mode Selection %x\n", Gen4PresetCoeffSelection));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_PX16GRTPCL1, (UINT32)~B_PCIE_PX16GRTPCL1_PCM, (Gen4PresetCoeffSelection << B_PCIE_PX16GRTPCL1_PCM_OFFSET));

    for (LaneIndex = 0; LaneIndex < MaxLinkWidth; LaneIndex++) {
      LaneRegisterIndex = LaneIndex / 2;
      LaneIndexInsideRegister = LaneIndex % 2;

      PciSegmentAnd32 (
        RpBase + R_PCIE_PX16GLTCO1 + (4 * LaneRegisterIndex),
        ~(B_PCIE_PX16GLTCO1_L0TCOE << LaneIndexInsideRegister)
        );
    }

  //
  // Step4:Program the Presets
  // Note : Requires silicon characterization
  //
  CpuPcieGen4InitializePresets (RpIndex, RpBase, Params);
  //
  // Step5:Program EQCFG4.LEP23B to 0 so that Phase 2 and Phase 3 EQ is not bypass
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG4, (UINT32)~(B_PCIE_EQCFG4_PX16GLEP23B));
  //
  // Step6:Program EQCFG4.LEP3B to 0 so that Phase 3 EQ is not bypass
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG4, (UINT32)~(B_PCIE_EQCFG4_PX16GLEP3B));
  //
  // Step7:Program EQCFG4 RTLEPCEB to 0 to enable HWEQ
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG4, (UINT32)~B_PCIE_EQCFG4_PX16GLEPCEB);
  //
  // Step8:Program EQCFG4.RTPCOE to 0 enable Hardware Search Algorithm
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG4, (UINT32)~B_PCIE_EQCFG4_PX16GRTPCOE);
  //
  // Step9,10:Rx wait time for each new eq should be configured to 1us
  //
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG4, (UINT32)~B_PCIE_EQCFG4_PX16GRWTNEVE_MASK, ((UINT32)V_PCIE_EQCFG4_PX16GRWTNEVE_1US << B_PCIE_EQCFG4_PX16GRWTNEVE_OFFSET));  //
  // Step11: For Linear Mode, BIOS needs to program the EQCFG4.MFLNTL to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG4, (UINT32)~B_PCIE_EQCFG4_PX16GMFLNTL);
  //
  // Step12: Program EQCFG4.PX16GTSWLPCE to 0 // Program it to 4 (5us) as per post silicon recommendation - Gen4 TS Lock Timer
  //
  Gen4TsLockTimer = 2;
  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP16) {
    Gen4TsLockTimer = 4;
  }
  DEBUG((DEBUG_INFO, "Gen4 TS Lock Timer = %d \n", Gen4TsLockTimer));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG4, (UINT32)~B_PCIE_EQCFG4_PX16GTSWLPCE_MASK, (Gen4TsLockTimer << B_PCIE_EQCFG4_PX16GTSWLPCE_OFFSET));
  //
  // Step13:Rx wait time for each new eq should be configured to 3us
  //
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG4, (UINT32)~B_PCIE_EQCFG4_PX16GRWTNEVE_MASK, ((UINT32)V_PCIE_EQCFG4_PX16GRWTNEVE_3US << B_PCIE_EQCFG4_PX16GRWTNEVE_OFFSET));
  //
  // Step14: Program the HAEQ.HAPCCPI 0 to allow 1 iteration of Recovery.Equalization entry
  // @ TODO : Follow up with IP team on the value
  //
  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP16) {
    Gen4Px16Ghapccpi = 2;
  } else {
    Gen4Px16Ghapccpi = 0;
  }
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG4, (UINT32)~B_PCIE_EQCFG4_PX16GHAPCCPI_MASK, Gen4Px16Ghapccpi << B_PCIE_EQCFG4_PX16GHAPCCPI_OFFSET);
  //
  // Step15: Program EQCFG5.HAPCSB as below and clear HAPCCPIE
  //
  Gen4NumberOfPresetCoeffList = 0;
  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP16) {
    Gen4NumberOfPresetCoeffList = 2;
  }
  DEBUG((DEBUG_INFO, "Gen4 Number of Preset/Coefficient = %d\n", Gen4NumberOfPresetCoeffList));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG5, (UINT32)~B_PCIE_EQCFG5_HAPCSB_MASK, Gen4NumberOfPresetCoeffList << B_PCIE_EQCFG5_HAPCSB_OFFSET);
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG4, (UINT32)~(B_PCIE_EQCFG4_PX16GHAPCCPIE));
  //
  // Step16,17: Program EQCFG4.MEQSMMFLNTL and EQCFG4.MFLNTL to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG4, (UINT32)~(B_PCIE_EQCFG4_PX16GMEQSMMFLNTL | B_PCIE_EQCFG4_PX16GMFLNTL));
  //
  // Step18: Program EQCFG4.REIC to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG4, (UINT32)~(B_PCIE_EQCFG4_PX16GREIC));
  //
  // Step19,20: Program EQCFG5.NTIC,EQCFG5.EMD  to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG5, (UINT32)~(B_PCIE_EQCFG5_NTIC_MASK | B_PCIE_EQCFG5_EMD));

  //
  // Step21: To support 4 coefficients PECT needs to program to 4h to allow 4ms in SIP16 controller
  // To support 1 coefficient PECT needs to program to 11h to allow 10ms in SIP17 controller
  //
  Gen4PcetTimer = 11;
  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP16) {
    Gen4PcetTimer = 4;
  }
  DEBUG((DEBUG_INFO, "Gen4 PCET timeout for = %d\n", Gen4PcetTimer));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_EQCFG5, (UINT32)~(B_PCIE_EQCFG5_PCET_MASK), (Gen4PcetTimer << B_PCIE_EQCFG5_PCET_OFFSET));
  //
  // Step22,23: Program EQCFG5.NTEME and EQCFG5.MPEME to 0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_EQCFG5, (UINT32)~(B_PCIE_EQCFG5_NTEME | B_PCIE_EQCFG5_MPEME));
  //
  // Step24: Program the following register for the Downstream Port Transmitter Preset value to P7
  //
  ///
  /// Configure Transmitter Preset for each Upstream and Downstream Port Lane:
  /// 1.  Set PL16L01EC.DPL0TP, Dxx:Fn + ABCh[3:0]    = 7
  /// 2.  Set PL16L01EC.UPL0TP, Dxx:Fn + ABCh[11:8]   = 7
  /// 3.  Set PL16L01EC.DPL1TP, Dxx:Fn + ABCh[19:16]  = 7
  /// 4.  Set PL16L01EC.UPL1TP, Dxx:Fn + ABCh[27:24]  = 7
  /// 5.  Set PL16L23EC.DPL2TP, Dxx:Fn + AC0h[3:0]    = 7
  /// 6.  Set PL16L23EC.UPL2TP, Dxx:Fn + AC0h[11:8]   = 7
  /// 7.  Set PL16L23EC.DPL3TP, Dxx:Fn + AC0h[19:16]  = 7
  /// 8.  Set PL16L23EC.UPL3TP, Dxx:Fn + AC0h[27:24]  = 7
  ///    Upto PL16L23EC.UPL3TP, Dxx:Fn + AC8h[27:24]  = 7
  ///
  Gen4Uptp = (UINT16) Params->RootPort[RpIndex].Gen4Uptp;
  Gen4Dptp = (UINT16) Params->RootPort[RpIndex].Gen4Dptp;
  DEBUG ((DEBUG_INFO, "Gen4Uptp = %x, Gen4Dptp = %x!\n", Gen4Uptp, Gen4Dptp));
  Data16And = 0;
  Data16Or = ((Gen4Uptp << B_PCIE_PL16L01EC_UP16L1TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L01EC_DP16L1TP_OFFSET) |
    (Gen4Uptp << B_PCIE_PL16L01EC_UP16L0TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L01EC_DP16L0TP_OFFSET));
  PciSegmentAndThenOr16 (RpBase + R_PCIE_PL16L01EC, Data16And, Data16Or);

  Data16And = 0;
  Data16Or = ((Gen4Uptp << B_PCIE_PL16L23EC_UP16L3TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L23EC_DP16L3TP_OFFSET) |
    (Gen4Uptp << B_PCIE_PL16L23EC_UP16L2TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L23EC_DP16L2TP_OFFSET));
  PciSegmentAndThenOr16 (RpBase + R_PCIE_PL16L23EC, Data16And, Data16Or);

  Data16And = 0;
  Data16Or = ((Gen4Uptp << B_PCIE_PL16L45EC_UP16L5TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L45EC_DP16L5TP_OFFSET) |
    (Gen4Uptp << B_PCIE_PL16L45EC_UP16L4TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L45EC_DP16L4TP_OFFSET));
  PciSegmentAndThenOr16 (RpBase + R_PCIE_PL16L45EC, Data16And, Data16Or);

  Data16And = 0;
  Data16Or = ((Gen4Uptp << B_PCIE_PL16L67EC_UP16L7TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L67EC_DP16L7TP_OFFSET) |
    (Gen4Uptp << B_PCIE_PL16L67EC_UP16L6TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L67EC_DP16L6TP_OFFSET));
  PciSegmentAndThenOr16 (RpBase + R_PCIE_PL16L67EC, Data16And, Data16Or);

  Data16And = 0;
  Data16Or = ((Gen4Uptp << B_PCIE_PL16L89EC_UP16L9TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L89EC_DP16L9TP_OFFSET) |
    (Gen4Uptp << B_PCIE_PL16L89EC_UP16L8TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L89EC_DP16L8TP_OFFSET));
  PciSegmentAndThenOr16 (RpBase + R_PCIE_PL16L89EC, Data16And, Data16Or);

  Data16And = 0;
  Data16Or = ((Gen4Uptp << B_PCIE_PL16L1011EC_UP16L11TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L1011EC_DP16L11TP_OFFSET) |
    (Gen4Uptp << B_PCIE_PL16L1011EC_UP16L10TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L1011EC_DP16L10TP_OFFSET));
  PciSegmentAndThenOr16 (RpBase + R_PCIE_PL16L1011EC, Data16And, Data16Or);

  Data16And = 0;
  Data16Or = ((Gen4Uptp << B_PCIE_PL16L1213EC_UP16L13TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L1213EC_DP16L13TP_OFFSET) |
    (Gen4Uptp << B_PCIE_PL16L1213EC_UP16L12TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L1213EC_DP16L12TP_OFFSET));
  PciSegmentAndThenOr16 (RpBase + R_PCIE_PL16L1213EC, Data16And, Data16Or);

  Data16And = 0;
  Data16Or = ((Gen4Uptp << B_PCIE_PL16L1415EC_UP16L15TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L1415EC_DP16L15TP_OFFSET) |
    (Gen4Uptp << B_PCIE_PL16L1415EC_UP16L14TP_OFFSET) |
    (Gen4Dptp << B_PCIE_PL16L1415EC_DP16L14TP_OFFSET));
  PciSegmentAndThenOr16 (RpBase + R_PCIE_PL16L1415EC, Data16And, Data16Or);

  DEBUG ((DEBUG_INFO, "CpuPcieDoGen4HardwareEq End!\n"));
}

/**
  Perform link equaliztion (coefficient search).
  @param[in] RpIndex      Port index
  @param[in] SiPolicy     The SI Policy
  @param[in] TempPciBus   Temp bus number
**/
VOID
CpuPcieLinkEqualization (
  UINT8                      RpIndex,
  CONST SI_POLICY_PPI        *SiPolicy,
  UINT8                      TempPciBus,
  BOOLEAN                    KeepPortVisible,
  CPU_PCIE_RP_PREMEM_CONFIG  *CpuPcieRpPreMemConfig
  )
{
  EFI_STATUS                        Status;
  UINT64                            RpBase;
  CONST CPU_PCIE_ROOT_PORT_CONFIG   *RpConfig;
  CPU_PCIE_CONFIG                   *CpuPcieRpConfig;

  DEBUG ((DEBUG_INFO, "CpuPcieLinkEqualization Start!\n"));

  RpBase = CpuPcieBase (RpIndex);

  DEBUG ((DEBUG_INFO, "Link Speed = %x\n", CpuPcieGetLinkSpeed (RpBase)));

  Status = GetConfigBlock ((VOID *) SiPolicy, &gCpuPcieRpConfigGuid, (VOID *) &CpuPcieRpConfig);
  ASSERT_EFI_ERROR (Status);

  RpConfig = &CpuPcieRpConfig->RootPort[RpIndex];

  //
  // If both rootport and endpoint support Common Clock config, set it before equalization
  //
  CpuPcieEnableCommonClock (RpIndex, TempPciBus);

  //
  // Gen3 HW Equalization
  //
  if (IsGen3HwEqAllowed (RpIndex, RpConfig, KeepPortVisible, CpuPcieRpPreMemConfig)) {
    DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
    DEBUG ((DEBUG_VERBOSE, "Dumping Registers before Gen3 HW Equalization\n"));
    CpuPcieGen3DumpHwEqRegs (RpBase);
    DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
    DEBUG ((DEBUG_INFO, "CpuPcieDoGen3HardwareEq\n"));
    CpuPcieDoGen3HardwareEq (RpIndex, RpBase, TempPciBus, CpuPcieRpConfig);
    DEBUG ((DEBUG_VERBOSE, "Root port is capable of Gen3, so retrain the link to Gen3"));
    DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
    DEBUG ((DEBUG_VERBOSE, "Dumping Registers after Gen3 HW Equalization\n"));
    CpuPcieGen3DumpHwEqRegs (RpBase);
    DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
    // Gen4 HW Equalization
    //
   if (IsGen4HwEqAllowed (RpIndex, RpConfig, KeepPortVisible, CpuPcieRpPreMemConfig)) {
      DEBUG ((DEBUG_INFO, "CpuPcieDoGen4HardwareEq\n"));
      DEBUG((DEBUG_VERBOSE, "Root port is capable of Gen4, so retrain the link to Gen4"));
      DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
      DEBUG ((DEBUG_VERBOSE, "Dumping Registers before Gen4 HW Equalization\n"));
      CpuPcieGen4DumpHwEqRegs (RpBase);
      DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
      CpuPcieDoGen4HardwareEq (RpIndex, RpBase, CpuPcieRpConfig);
      DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
      DEBUG ((DEBUG_VERBOSE, "Dumping Registers after Gen4 HW Equalization\n"));
      CpuPcieGen4DumpHwEqRegs (RpBase);
      DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
    }
    //
    // Gen5 HW Equalization
    //
    if (IsGen5HwEqAllowed (RpIndex, RpConfig, KeepPortVisible, CpuPcieRpPreMemConfig)) {
      DEBUG ((DEBUG_INFO, "CpuPcieDoGen5HardwareEq\n"));
      DEBUG ((DEBUG_VERBOSE, "Root port is capable of Gen5, so retrain the link to Gen5"));
      DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
      DEBUG ((DEBUG_VERBOSE, "Dumping Registers before Gen5 HW Equalization\n"));
      CpuPcieSip17Gen5DumpHwEqRegs (RpIndex);
      DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
      CpuPcieSip17DoGen5HardwareEq (RpIndex, RpBase, &mDoGen5HwEqRpMask, CpuPcieRpConfig);
      DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
      DEBUG ((DEBUG_VERBOSE, "Dumping Registers after Gen5 HW Equalization\n"));
      CpuPcieSip17Gen5DumpHwEqRegs (RpIndex);
      DEBUG ((DEBUG_VERBOSE, "*****************************************\n"));
    }
    if (RpConfig->PcieRpCommonConfig.Gen3EqPh3Method != CpuPcieEqHardware) {
      DEBUG((DEBUG_INFO, "Invalid EqMethod %d\n", RpConfig->PcieRpCommonConfig.Gen3EqPh3Method));
      ASSERT(FALSE);
    }
  }
  CpuPcieResetErrorCounts (RpBase);
  DEBUG ((DEBUG_INFO, "CpuPcieLinkEqualization End!\n"));
}

/**
  Checks if given rootport should be left visible even though disabled, in order to avoid PCIE rootport swapping

  @param[in] RpIndex           rootport number
  @param[in] RpDisableMask     bitmask of all disabled rootports
  @param[in] PciExpressConfig  PCIe policy configuration

  @retval TRUE  port should be kept visible despite being disabled
  @retval FALSE port should be disabled and hidden

**/
BOOLEAN
CpuPcieIsPortForceVisible (
  IN UINT8                 RpIndex,
  IN UINT32                RpDisableMask,
  IN CONST CPU_PCIE_CONFIG *PciExpressConfig
  )
{
  UINT32                            RpEnabledMask;
  UINTN                             CurrentRpDevNum;
  UINTN                             CurrentRpFnNum;
  UINTN                             NextRpDevNum;
  UINTN                             NextRpFnNum;

  RpEnabledMask = (1u << GetMaxCpuPciePortNum ()) - 1;
  RpEnabledMask &= (~RpDisableMask);

  //
  // Only Function 0 needs to be kept force visible if Function swap is disabled, Function 0 is disabled and Function 1 and Function 2 are enabled
  //
  GetCpuPcieRpDevFun (RpIndex, &CurrentRpDevNum, &CurrentRpFnNum);
  GetCpuPcieRpDevFun (RpIndex + 1, &NextRpDevNum, &NextRpFnNum);
  if (RpDisableMask & (BIT0 << (RpIndex))){
    if ((RpEnabledMask & (BIT0 << (RpIndex+1))) && (CurrentRpDevNum == NextRpDevNum) && (CurrentRpFnNum == 0)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
  Lock SRL bits - lock it for all root ports

**/
VOID
CpuPcieRpSetSecuredRegisterLock (
  UINT32        RpDisableMask
  )
{
  UINT32                                Data32Or;
  UINT32                                Data32And;
  UINT64                                RpBase;
  UINT8                                 PortIndex;
  UINT8                                 MaxCpuPciePortNum;

  DEBUG ((DEBUG_INFO, "CpuPcieRpSetSecuredRegisterLock () Start!\n"));
  MaxCpuPciePortNum = GetMaxCpuPciePortNum();
  for (PortIndex = 0; PortIndex < MaxCpuPciePortNum; PortIndex++) {
    if ((RpDisableMask & (BIT0 << PortIndex)) == 0) {
      RpBase = CpuPcieBase(PortIndex);
      Data32And = (UINT32)~B_SA_SPX_PCR_PCD_SRL;
      Data32Or = (UINT32) B_SA_SPX_PCR_PCD_SRL;
      CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (PortIndex), R_SA_SPX_PCR_SRL, Data32And, Data32Or);
      ///
      ///Secure Register Lock (SRL) 0xC8C[0]: 0 as per BWG 1.3
      ///Secure Equalization Register Lock (SERL) 0XC8C[8]: 1
      ///LTR Configuration Lock (LTRCFGLOCK) 0xC8C[16]: 0
      ///Device ID Override Lock (DIDOVR_LOCK) 0xC8C[24]: 1
      ///
      Data32And = (UINT32) ~(B_PCIE_LPCR_SERL | B_PCIE_LPCR_SRL | B_PCIE_LPCR_LTRCFGLOCK);
      Data32Or = B_PCIE_LPCR_DIDOVR_LOCK;
      if (IsEom()) {
        DEBUG ((DEBUG_INFO, "IsEndOfManufactoring: %d\n", IsEom()));
        Data32Or |= B_PCIE_LPCR_SERL;
      }else {
        DEBUG ((DEBUG_INFO, "Serl from policy: %d\n", mCpuPcieRpConfig->Serl));
        Data32Or |= mCpuPcieRpConfig->Serl << B_PCIE_LPCR_SERL_OFFSET;
      }
      PciSegmentAndThenOr32 (RpBase + R_PCIE_LPCR, Data32And, Data32Or);
      DEBUG ((DEBUG_INFO, "R_PCIE_LPCR after setting SRL = %x\n", PciSegmentRead32 (RpBase + R_PCIE_LPCR)));
    }
  }
  DEBUG ((DEBUG_INFO, "CpuPcieRpSetSecuredRegisterLock () End!\n"));
}

/**
  Checks integrity of Policy settings for all rootports.
  Triggers assert if anything is wrong. For debug builds only

  @param[in] PciExpressConfig     Pointer to CPU_PCIE_CONFIG instance
**/
VOID
CpuPciePolicySanityCheck (
  IN OUT CPU_PCIE_CONFIG *PciExpressConfig
  )
{
  UINT8                       RpIndex;
  CPU_PCIE_ROOT_PORT_CONFIG   *RpConfig;

  for (RpIndex = 0; RpIndex < GetMaxCpuPciePortNum (); RpIndex++) {
    RpConfig  = &PciExpressConfig->RootPort[RpIndex];
    //
    // Ports with hotplug support must have SlotImplemented bit set
    //
    ASSERT (!RpConfig->PcieRpCommonConfig.HotPlug || RpConfig->PcieRpCommonConfig.SlotImplemented);
  }
}

/**
  Performs mandatory Root Port Initialization.
  This function is silicon-specific and configures proprietary registers.

  @param[in]  PortIndex               The root port to be initialized (zero based)
  @param[in]  SiPolicy                The SI Policy PPI
  @param[in]  SiPreMemPolicyPpi       The SI PreMem Policy PPI
  @param[in]  TempPciBus              The temporary Bus number for root port initialization
  @param[out] Gen4DeviceFound         Reports if there's Gen3 capable endpoint connected to this rootport
**/
STATIC
VOID
InitCpuPcieSingleRootPort(
  IN  UINT8                                     PortIndex,
  IN  CONST SI_POLICY_PPI                       *SiPolicy,
  IN  SI_PREMEM_POLICY_PPI                      *SiPreMemPolicyPpi,
  IN  UINT8                                     TempPciBus,
  OUT BOOLEAN                                   *Gen4DeviceFound,
  IN  BOOLEAN                                   KeepPortVisible,
  CPU_PCIE_RP_PREMEM_CONFIG                     *CpuPcieRpPreMemConfig
  )
{
  EFI_STATUS                        Status;
  UINT64                            RpBase;
  UINT32                            Data32Or;
  UINT32                            Data32And;
  UINT16                            Data16;
  UINT16                            Data16Or;
  UINT16                            Data16And;
  UINT8                             Data8Or;
  UINT8                             Data8And;
  CONST CPU_PCIE_ROOT_PORT_CONFIG   *RootPortConfig;
  VTD_CONFIG                        *VtdConfig;
  BOOLEAN                           DeviceFound;
  UINT32                            Tls;
  UINT8                             RpLinkSpeed;
  UINT32                            RpMaxPayloadCapability;
  BOOLEAN                           RpGen34Capable;
  UINT8                             RpIndex;
  PCIE_ROOT_PORT_DEV_PRIVATE        RpDevPrivate;
  UINT32                            DCap2Data32Or;
  UINT32                            DCap2Data32And;
  UINTN                             RpDev;
  UINTN                             RpFunc;

  DCap2Data32Or = 0;
  DCap2Data32And = ~0u;
  RpIndex = PortIndex;
  DEBUG ((DEBUG_INFO, "InitCpuPcieSingleRootPort (%d) Start \n", PortIndex + 1));

  CpuPcieGetRpDev (PortIndex, &RpDevPrivate);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gVtdConfigGuid, (VOID *) &VtdConfig);
  ASSERT_EFI_ERROR (Status);

  RootPortConfig = &mCpuPcieRpConfig->RootPort[PortIndex];
  RpBase = CpuPcieBase (PortIndex);
  RpGen34Capable = FALSE;

  GetCpuPcieRpDevFun (PortIndex, &RpDev, &RpFunc);

  Tls = PciSegmentRead16 (RpBase + R_PCIE_LCTL2) & B_PCIE_LCTL2_TLS;
  DEBUG ((DEBUG_INFO, "Target Link Speed = %x \n", Tls));
  ASSERT (Tls < V_PCIE_LCTL2_TLS_GEN4);

  /// Following reset it is possible for a device to terminate the
  /// configuration request but indicate that it is temporarily unable to process it,
  /// but in the future. The device will return the Configuration Request Retry Status.
  /// By setting the Completion Retry Status Replay Enable, Dxx:Fn + 320h[22],
  /// the RP will re-issue the request on receiving such status.
  /// The BIOS shall set this bit before first configuration access to the endpoint.
  DEBUG ((DEBUG_INFO, "Set the Completion Retry Status Replay Enable, Dxx:Fn + 320h[22]\n"));
  PciSegmentOr32 (RpBase + R_PCIE_PCIECFG2, B_PCIE_PCIECFG2_CRSREN);

  DeviceFound = CpuPcieGetDeviceInfo (PortIndex, TempPciBus, &DevInfo[PortIndex]);
  if (DeviceFound) {
    *Gen4DeviceFound = ((CpuPcieGetMaxLinkSpeed (RpBase) >= 4) && DeviceFound && (DevInfo[PortIndex].MaxLinkSpeed >= 4));
    DEBUG ((DEBUG_INFO, "End point detected with Gen%x capability!!!\n", DevInfo[PortIndex].MaxLinkSpeed));
  } else {
    DEBUG ((DEBUG_INFO, "No endpoint connected to root port\n", PortIndex + 1));
    DevInfo[PortIndex].MaxLinkSpeed = 0;
  }
  //
  // Some features are incompatible with Gen4 and should not be enabled if rootport is capable of Gen4.
  // As an optimization we consider RP as Gen4 capable only if it supports hotplug or has Gen4 device connected.
  // It makes those features available when RP is Gen4 capable but a slower device is connected.
  // This optimization can be deleted if accessing endpoint in early boot is undesired.
  //
  if ((CpuPcieGetMaxLinkSpeed (RpBase) >= 3) && (RootPortConfig->PcieRpCommonConfig.HotPlug || DeviceFound)) {
    RpGen34Capable = TRUE;
    DEBUG ((DEBUG_INFO, "Root port is Gen3/4 capable!!!\n"));
  }

  ///
  /// IOSF_Max_Payload_Size (IMPS) 0xF0[10:8]: '111'b
  /// IOSF Max Read Request Size (IMRS) 0xF0[6:4] '111'b(SIP17)
  ///
  Data32And = (UINT32)~(B_PCIE_IPCS_IMPS_MASK);
  Data32Or = (V_PCIE_IPCS_IMPS << B_PCIE_IPCS_IMPS_OFFSET);
  if (GetCpuPcieSipInfo(PortIndex) == PCIE_SIP17) {
    Data32And &= (UINT32)~(B_PCIE_IPCS_IMRS_MASK);
    Data32Or |= (V_PCIE_IPCS_IMRS << B_PCIE_IPCS_IMRS_OFFSET);
  }
  PciSegmentAndThenOr32 (RpBase + R_PCIE_IPCS, Data32And, Data32Or);

  ///
  /// If only 128B max payload is supported set CCFG.UNRS to 0.
  ///
  /// If peer writes are supported set max payload size supported to 64B due to CPU PSF limitation, clear CCFG.UPMWPD
  /// and program all the CPU PCIe Root Ports such that upstream posted writes and upstream non-posted requests
  /// are split at 128B boundary by setting CCFG fields: UPSD to 0, CCFG.UPRS to 000b and UNSD to 0, UNRS to 000b
  ///
  Data32And = ~0u;
  Data32Or  = 0;
  ///
  /// UPRS and UNRS needs to be programmed to 64B irrespective of any condition for CPU PCIe
  ///
  RpMaxPayloadCapability = CpuPcieMaxPayload256;
  Data32And &= (UINT32) ~(B_PCIE_CCFG_UNSD | B_PCIE_CCFG_UNRS_MASK);
  Data32Or  |= (UINT32)  (V_PCIE_CCFG_UNRS_64B << B_PCIE_CCFG_UNRS_OFFSET);
  Data32And &= (UINT32) ~( B_PCIE_CCFG_UPSD | B_PCIE_CCFG_UPRS_MASK);
  Data32Or  |= (UINT32)  (V_PCIE_CCFG_UPRS_64B << B_PCIE_CCFG_UPRS_OFFSET);

  DEBUG ((DEBUG_INFO, "CpuPcieMaxPayloadMax = %x\n", CpuPcieMaxPayloadMax));
  ASSERT (RootPortConfig->PcieRpCommonConfig.MaxPayload < CpuPcieMaxPayloadMax);

  ///
  /// Set B0:Dxx:Fn + D0h [13:12] to 00b
  ///
  DEBUG ((DEBUG_INFO, "Upstream Non-Posted Request Delay set to 00b which puts delay of 0 clocks\n"));
  Data32And &= (UINT32)~(B_PCIE_CCFG_UNRD);

  ///
  /// Clear B0:Dxx:Fn + D0h [25:27] bits if P2P is enabled
  /// B_PCIE_CCFG_UPMWPD
  /// B_PCIE_CCFG_UMRPD
  /// B_PCIE_CCFG_UARPD
  ///
  if (RootPortConfig->PcieRpCommonConfig.EnablePeerMemoryWrite == TRUE) {
    if (GetCpuPcieSipInfo (PortIndex) == PCIE_SIP16) {
      Data32And &= (UINT32) ~(B_PCIE_CCFG_UPMWPD | B_PCIE_CCFG_UMRPD | B_PCIE_CCFG_UARPD);
    } else {
      Data32And &= (UINT32) ~(B_PCIE_CCFG_UPMWPD | B_PCIE_CCFG_UARPD);
    }
  } else {
    Data32Or |= (UINT32) (B_PCIE_CCFG_UPMWPD | B_PCIE_CCFG_UMRPD | B_PCIE_CCFG_UARPD);
  }

  PciSegmentAndThenOr32 (RpBase + R_PCIE_CCFG, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_CCFG after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_CCFG)));
  DEBUG ((DEBUG_INFO, "Set R_PCIE_DCAP[B_PCIE_DCAP_MPS] = %x\n", RpMaxPayloadCapability));
  PciSegmentAndThenOr16 (RpBase + R_PCIE_DCAP, (UINT16) ~B_PCIE_DCAP_MPS, (UINT16)RpMaxPayloadCapability);

  ///
  /// Program VC0CTL [7:1] to 1111111b
  ///

  Data32And = (UINT32)~B_PCIE_VC0CTL_TVM_MASK;
  Data32Or = (UINT32)(V_PCIE_VC0CTL_TVM_NO_VC << B_PCIE_VC0CTL_TVM_OFFSET);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_VC0CTL, Data32And, Data32Or);

  // Program VC1CTL.ID [27:24] to 0001b

  Data32And = (UINT32)~(B_PCIE_VC1CTL_ID_MASK | B_PCIE_VC1CTL_TVM_MASK);
  Data32Or  = (UINT32)((V_PCIE_VC1CTL_ID_ONE << B_PCIE_VC1CTL_ID_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_VC1CTL, Data32And, Data32Or);

  //
  // Configuration for Multi VC PCIe Controller
  //
  if (IsRpMultiVc(PortIndex) &&
      (RootPortConfig->MultiVcEnabled == TRUE)) {
      //
      // Program PVCCR1.EVCC [2:0] to 01b
      //
      Data32And = (UINT32)~B_PCIE_PVCCR1_EVCC_MASK;
      if (GetCpuPcieSipInfo (PortIndex) == PCIE_SIP16) {
        Data32Or = (UINT32)(V_PCIE_PVCCR1_EVCC_2_VC << B_PCIE_PVCCR1_EVCC_OFFSET);
      }
      PciSegmentAndThenOr32 (RpBase + R_PCIE_PVCCR1, Data32And, Data32Or);
  }

  ///
  /// Coalescing Start
  ///
  DEBUG ((DEBUG_INFO, "Coalescing Start\n"));
  ///
  /// Program COCTL.PWCE 594h [0] = 1b
  /// Program COCTL.DDCE 594h [1] = 1b
  /// Program COCTL.CT 594h [9:2] = 3b
  /// Program COCTL.CTE 594h [10] = 1b
  /// Program COCTL.ROAOP 594h [11] = 0b(SIP16)
  /// Program COCTL.ROAOP 594h [11] = 1b(SIP17)
  /// Program COCTL.PCLM 594h [14:13] = 2b
  /// Program COCTL.NPCLM 594h [16:15] = 2b

  Data32And = ~(UINT32)(B_PCIE_COCTL_CT_MASK | B_PCIE_COCTL_ROAOP | B_PCIE_COCTL_PCLM_MASK | B_PCIE_COCTL_NPCLM_MASK);
  Data32Or = (B_PCIE_COCTL_PWCE | B_PCIE_COCTL_DDCE | (3 << B_PCIE_COCTL_CT_OFFSET) | B_PCIE_COCTL_CTE | (0x2 << B_PCIE_COCTL_PCLM_OFFSET) | (0x2 << B_PCIE_COCTL_NPCLM_OFFSET));
  if (GetCpuPcieSipInfo (PortIndex) == PCIE_SIP17) {
    Data32Or |= B_PCIE_COCTL_ROAOP;
  }
  PciSegmentAndThenOr32 (RpBase + R_PCIE_COCTL, Data32And, Data32Or);
  DEBUG((DEBUG_INFO, "COCTL = %x\n", PciSegmentRead32 (RpBase + R_PCIE_COCTL)));
  ///
  /// Chain Bit Generation Mode(CBGM) 0x6CC 21 1
  ///
  Data32And = ~(UINT32)B_PCIE_ACRG3_CBGM;
  Data32Or  = B_PCIE_ACRG3_CBGM;
  PciSegmentAndThenOr32 (RpBase + R_PCIE_ACRG3, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_ACRG3 = %x\n", PciSegmentRead32 (RpBase + R_PCIE_ACRG3)));
  DEBUG ((DEBUG_INFO, "Coalescing End\n"));
  ///
  /// Coalescing End
  ///

  ///
  /// Power Optimizer Configuration
  /// If B0:Dxx:Fn + 400h is programmed, BIOS will also program B0:Dxx:Fn + 404h [1:0] = 11b,
  /// to enable these override values.
  /// - Fn refers to the function number of the root port that has a device attached to it.
  /// - Default override value for B0:Dxx:Fn + 400h should be 880F880Fh
  /// - Also set 404h[2] to lock down the configuration
  /// - Refer to table below for the 404h[3] policy bit behavior.
  /// Done in PcieSetPm()
  ///
  /// Program B0:Dxx:Fn + 64h [11] = 1b
  ///
  if (RootPortConfig->PcieRpCommonConfig.Aspm != FALSE) {
    Data32Or = 0;
    Data32And = ~0u;
    if (RootPortConfig->PcieRpCommonConfig.LtrEnable == TRUE) {
      DCap2Data32Or |= B_PCIE_DCAP2_LTRMS;
    } else {
      DCap2Data32And &= (UINT32) ~(B_PCIE_DCAP2_LTRMS);
    }
    ///
    /// Optimized Buffer Flush/Fill (OBFF) is not supported.
    /// Program B0:Dxx:Fn + 64h [19:18] = 0h
    /// BIOS: Program this field to 10. Note that this register is not required to be program if there is no PCIe Device attached for non-Hot Pluggable Port.
    /// @ TODO : Add condition to check if end point is connected for non-Hot Pluggable Port, if not then this register need not be programmed
    ///
    DCap2Data32And &= (UINT32) ~B_PCIE_DCAP2_OBFFS_MASK;

    ///
    /// Latency Tolerance Reporting Override (LTROVR) 0x400  31:0 0x88248824
    ///
    Data32And = 0xFFFFFFFF;
    Data32Or = 0x88248824;
    PciSegmentAndThenOr32 (RpBase + R_PCIE_LTROVR, Data32And, Data32Or);

    ///
    /// LTR LTR Override Policy (LTROVRPLCY) 0x404 3 0
    /// LTR Non-Snoop Override Enable (LTRNSOVREN) 0x404  1 1
    /// LTR Snoop Override Enable (LTRSOVREN) 0x404  0 1
    ///
    Data32And = (UINT32) ~(B_PCIE_LTROVR2_LTROVRPLCY | B_PCIE_LTROVR2_LTRNSOVREN | B_PCIE_LTROVR2_LTRSOVREN);
    Data32Or = (UINT32) (B_PCIE_LTROVR2_LTRNSOVREN | B_PCIE_LTROVR2_LTRSOVREN);
    PciSegmentAndThenOr32 (RpBase + R_PCIE_LTROVR2, Data32And, Data32Or);

    ///
    /// Program B0:Dxx:Fn + 68h [10] = 1b
    ///
    Data16 = PciSegmentRead16 (RpBase + R_PCIE_DCTL2);
    if (RootPortConfig->PcieRpCommonConfig.LtrEnable == TRUE) {
      Data16 |= B_PCIE_DCTL2_LTREN;
    } else {
      Data16 &= (UINT16) ~(B_PCIE_DCTL2_LTREN);
    }
    PciSegmentWrite16 (RpBase + R_PCIE_DCTL2, Data16);
    DEBUG ((DEBUG_INFO, "R_PCIE_DCTL2 after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_DCTL2)));
  } /// End of ASPM enable check

    ///
    /// Program registers to enable Atomics and 10-Bit Requester Supported
    /// Program B0:Dxx:Fn + 64h [17] = 0x1
    /// Program B0:Dxx:Fn + 64h [16] = 0x1
    /// Program B0:Dxx:Fn + 64h [9] = 1h
    /// Program B0:Dxx:Fn + 64h [8] = 1h
    /// Program B0:Dxx:Fn + 64h [7] = 1h
    /// Program B0:Dxx:Fn + 64h [6] = 1h
    ///
    DCap2Data32And &= (UINT32)~(B_PCIE_DCAP2_AC64BS | B_PCIE_DCAP2_AC32BS | B_PCIE_DCAP2_ARS | B_PCIE_DCAP2_PX10BTRS | B_PCIE_DCAP2_PX10BTCS);
    DCap2Data32Or |= (B_PCIE_DCAP2_AC128BS | B_PCIE_DCAP2_AC64BS | B_PCIE_DCAP2_AC32BS | B_PCIE_DCAP2_ARS  | B_PCIE_DCAP2_PX10BTCS | B_PCIE_DCAP2_PX10BTRS);

    PciSegmentAndThenOr32 (RpBase + R_PCIE_DCAP2, DCap2Data32And, DCap2Data32Or);
    DEBUG((DEBUG_INFO, "R_PCIE_DCAP2 after programming Atomics and 10-Bit Requester Supported = %x\n", PciSegmentRead32 (RpBase + R_PCIE_DCAP2)));



  ///
  /// PCH BIOS Spec Section 8.15.1 Power Optimizer Configuration
  /// Step 3 done in SaPciexpressHelpersLibrary.c ConfigureLtr
  ///

  if (GetCpuPcieSipInfo (PortIndex) == PCIE_SIP16) {
    ///
    /// Set Dxx:Fn + 300h[23:00] = 00B799AAh if LCTL.ES = 1 else program it to 0x00B7E5BB
    /// Set Dxx:Fn + 304h[11:00] = 0C97h
    ///
    if (PciSegmentRead16 (RpBase + R_PCIE_LCTL) & B_PCIE_LCTL_ES) {
      Data32And = (UINT32) ~(B_PCIE_PCIERTP1_G1X4_MASK | B_PCIE_PCIERTP1_G1X2_MASK | B_PCIE_PCIERTP1_G1X1_MASK | B_PCIE_PCIERTP1_G2X4_MASK);
      Data32Or  = (UINT32) ((V_PCIE_PCIERTP1_G1X4 << B_PCIE_PCIERTP1_G1X4_OFFSET) | (V_PCIE_PCIERTP1_G1X2 << B_PCIE_PCIERTP1_G1X2_OFFSET) | (V_PCIE_PCIERTP1_G1X1 << B_PCIE_PCIERTP1_G1X1_OFFSET) | (V_PCIE_PCIERTP1_G2X4 << B_PCIE_PCIERTP1_G2X4_OFFSET));
      PciSegmentAndThenOr32 (RpBase + R_PCIE_PCIERTP1, Data32And, Data32Or);
    } else {
      PciSegmentAndThenOr32 (RpBase + R_PCIE_PCIERTP1, ~0x00FFFFFFu, 0x00B7E5B0);
    }
    PciSegmentAndThenOr32 (RpBase + R_PCIE_PCIERTP2, ~0x00000FFFu, 0x00000C97);
    DEBUG ((DEBUG_INFO, "R_PCIE_PCIERTP1 after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_PCIERTP1)));
    DEBUG ((DEBUG_INFO, "R_PCIE_PCIERTP2 after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_PCIERTP2)));
  } else {
      PciSegmentAnd32 (RpBase + R_PCIE_PCIERTP1, ~0x0000000Fu);
      DEBUG ((DEBUG_INFO, "R_PCIE_PCIERTP1 after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_PCIERTP1)));
  }

  ///
  /// Set Dxx:Fn + 0xC50[5] = 1 (ACGR3S2.SRT = 1)
  ///
  PciSegmentOr32 (RpBase + R_PCIE_ACGR3S2, B_PCIE_ACGR3S2_SRT);

  DEBUG ((DEBUG_INFO, "R_PCIE_ACGR3S2 after setting SRT = %x\n", PciSegmentRead32 (RpBase + R_PCIE_ACGR3S2)));

  ///
  /// Set Dxx:Fn + 0xC70[1:0] = 0 (VNNREMCTL.LRSLFVNNRE = 1)
  ///
  PciSegmentAnd32 (RpBase + R_PCIE_VNNREMCTL, (UINT32)~(B_PCIE_VNNREMCTL_LRSLFVNNRE));
  DEBUG ((DEBUG_INFO, "R_PCIE_VNNREMCTL = %x\n", PciSegmentRead32 (RpBase + R_PCIE_VNNREMCTL)));

  ///
  /// Set Dxx:Fn + 0xC80[2] = 0x1 (AECR1G3.CRMTDDE)
  /// Set Dxx:Fn + 0xC80[6] = 0x1 (AECR1G3.L1OFFRDYHEWTEN)
  /// Set Dxx:Fn + 0xC80[9:7] = 0x6 (AECR1G3.L1OFFRDYHEWT)
  ///
  ///
  ///
  Data32And = ~(UINT32)(B_PCIE_AECR1G3_L1OFFRDYHEWT_MASK);
  Data32Or = B_PCIE_AECR1G3_L1OFFRDYHEWTEN | (V_PCIE_AECR1G3_L1OFFRDYHEWT << B_PCIE_AECR1G3_L1OFFRDYHEWT_OFFSET);
  if (GetCpuPcieSipInfo(PortIndex) == PCIE_SIP17) {
    Data32Or |= (UINT32)B_PCIE_AECR1G3_CRMTDDE;
  }
  PciSegmentAndThenOr32 (RpBase + R_PCIE_AECR1G3, Data32And , Data32Or);
  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// 1.
  /// Root Port L1 Squelch Polling (RPL1SQPOL) 0xE8  1 0
  /// Root Port Detect Squelch Polling (RPDTSQPOL) 0xE8  0 0
  ///
  Data32And = ~ (UINT32) (B_PCIE_PWRCTL_RPDTSQPOL | B_PCIE_PWRCTL_RPL1SQPOL);
  PciSegmentAnd32 (RpBase + R_PCIE_PWRCTL, Data32And);
  DEBUG ((DEBUG_INFO, "R_PCIE_PWRCTL after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_PWRCTL)));
  ///
  /// 2.  Program Dxx:Fn + 320h[27, 30] to [0,1]
  /// Enable PCIe Relaxed Order to always allow downstream completions to pass posted writes,
  /// 3.  Set B0:Dxx:Fn:320h[24:23] = 11b
  /// Set PME timeout to 10ms, by
  /// 4.  Set B0:Dxx:Fn:320h[21:20] = 01b
  ///

  Data32And = (UINT32) ~(B_PCIE_PCIECFG2_PMET | B_PCIE_PCIECFG2_RLLG3R);
  Data32Or  = B_PCIE_PCIECFG2_LBWSSTE |
    B_PCIE_PCIECFG2_CROAOV |
    B_PCIE_PCIECFG2_CROAOE;

  if (GetCpuPcieSipInfo(PortIndex) < PCIE_SIP17) {
    Data32Or |= (V_PCIE_PCIECFG2_PMET << B_PCIE_PCIECFG2_PMET_OFFSET);
  }

  PciSegmentAndThenOr32 (RpBase + R_PCIE_PCIECFG2, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_PCIECFG2 after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_PCIECFG2)));
  ///
  /// SIP16: Squelch Propagation Control Enable (SPCE) 0x324  5 1
  /// SIP17: Squelch Propagation Control Enable (SPCE) 0x324  5 0
  /// Squelch Off in L0 (SQOL0) 0x324  7 0
  /// Link Down SWQ Reset Policy 0x324  13 0
  /// Un-Squelch Sampling Period(USSP):0x324  27:26 10b
  /// B_PCIE_PCIEDBG_CTONFAE 1
  /// B_PCIE_PCIEDBG_LGCLKSQEXITDBTIMERS_MASK 00
  ///
  Data32And = (UINT32)~(B_PCIE_PCIEDBG_SPCE | B_PCIE_PCIEDBG_SQOL0 | B_PCIE_PCIEDBG_LDSWQRP | B_PCIE_PCIEDBG_USSP_MASK | B_PCIE_PCIEDBG_LGCLKSQEXITDBTIMERS_MASK);
  Data32Or = (V_PCIE_PCIEDBG_USSP_32NS << B_PCIE_PCIEDBG_USSP_OFFSET) | B_PCIE_PCIEDBG_CTONFAE;
  if (GetCpuPcieSipInfo(PortIndex) == PCIE_SIP16) {
    Data32Or |= B_PCIE_PCIEDBG_SPCE;
  }
  PciSegmentAndThenOr32 (RpBase + R_PCIE_PCIEDBG, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_PCIEDBG after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_PCIEDBG)));
  if (GetCpuPcieSipInfo(PortIndex) == PCIE_SIP17) {
    CpuPcieSip17SetErrorInjectionDisable (RpBase);
    CpuPcieSip17CompletionTimerTimeout (PortIndex);
  }

  ///
  /// Program Dxx:Fn + 424h [6, 5, 4] = [1, 1, 1]
  ///
  PciSegmentOr32 (
    RpBase + R_PCIE_PCIEPMECTL2,
    (B_PCIE_PCIEPMECTL2_L23RDYSCPGE |
     B_PCIE_PCIEPMECTL2_L1SCPGE)
    );
  DEBUG ((DEBUG_INFO, "R_PCIE_PCIEPMECTL2 after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_PCIEPMECTL2)));
  ///
  /// Pogram L0s/L1 Exit Latency depend on SIP version
  /// SIP16: Dxx:Fn + 4Ch[14:12] = 100b
  /// SIP17: Dxx:Fn + 4Ch[14:12] = 110b
  /// Program Dxx:Fn + 4Ch[17:15] = 100b
  ///
  Data32And = (UINT32) (~(B_PCIE_LCAP_EL1_MASK | B_PCIE_LCAP_EL0_MASK));
  Data32Or  = (4 << B_PCIE_LCAP_EL1_OFFSET);
  Data32Or |= (6 << B_PCIE_LCAP_EL0_OFFSET);

  ///
  /// Set LCAP APMS according to platform policy.
  ///
  DEBUG ((DEBUG_INFO, "Set LCAP APMS according to platform policy %x\n", RootPortConfig->PcieRpCommonConfig.Aspm));
  Data32And &= (UINT32)~B_PCIE_LCAP_APMS_MASK;
  Data32Or |= RootPortConfig->PcieRpCommonConfig.Aspm << B_PCIE_LCAP_APMS_OFFSET;
  PciSegmentAndThenOr32 (RpBase + R_PCIE_LCAP, Data32And, Data32Or);
  if (GetCpuPcieSipInfo(PortIndex) == PCIE_SIP17) {
    CpuPcieSip17RxMasterCycleDecode (PortIndex);
  }

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set MPC.BMERCE, Dxx:Fn + D8h[24] = 1b using byte access (only for SIP17)
  /// Set MPC.IRRCE, Dxx:Fn + D8h[25] = 1b using byte access
  /// For system that support MCTP over PCIE set
  /// Set PCIE RP PCI offset D8h[27] = 1b
  /// Set PCIE RP PCI offset D8h[3] = 1b
  ///
  Data8And = (UINT8) (~(B_PCIE_MPC_IRRCE | B_PCIE_MPC_MMBNCE) >> 24);
  Data8Or = B_PCIE_MPC_MMBNCE >> 24;
  if (VtdConfig->VtdDisable) {
    DEBUG ((DEBUG_INFO, "Enable IRRCE\n"));
    Data8Or |= B_PCIE_MPC_IRRCE >> 24;
  }
  if (GetCpuPcieSipInfo(PortIndex) == PCIE_SIP17) {
    Data8Or |= B_PCIE_MPC_BMERCE >> 24;
  }
  PciSegmentAndThenOr8 (RpBase + R_PCIE_MPC + 3, Data8And, Data8Or);

  //
  // Offset 0x590 R_PCIE_VTDBADDRL - Set Bit 0 (PCIEVTDBAREN) based on VtdIopEnable
  //
  if (VtdConfig->VtdIopEnable) {
    PciSegmentOr8 (RpBase + R_PCIE_VTDBADDRL , B_PCIE_VTDBADDRL_PCIEVTDBAREN);
  }

  Data8And = (UINT8) ~(B_PCIE_MPC_MCTPSE);
  Data8Or  = B_PCIE_MPC_MCTPSE;
  PciSegmentAndThenOr8 (RpBase + R_PCIE_MPC, Data8And, Data8Or);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// SIP16: Set PCIE RP PCI offset F5h[7:4] = 0000b
  /// SIP17: Set PCIE RP PCI offset F5h[7:4] = 1b
  ///
  Data8And = (UINT8) ~(B_PCIE_PHYCTL2_TDFT_MASK | B_PCIE_PHYCTL2_TXCFGCHGWAIT_MASK);
  Data8Or  = 0;
  if (GetCpuPcieSipInfo (PortIndex) == PCIE_SIP17) {
    Data8Or |= (0x1 << B_PCIE_PHYCTL2_TDFT_OFFSET);
  }
  PciSegmentAndThenOr8 (RpBase + R_PCIE_PHYCTL2, Data8And, Data8Or);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Enable PME_TO Time-Out Policy, Dxx:Fn + E2h[6] =1b
  ///
  PciSegmentOr8 (RpBase + R_PCIE_RPPGEN, B_PCIE_RPPGEN_PTOTOP);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Enable EQ TS2 in Recovery Receiver Config, Dxx:Fn + 450h[7]= 1b
  ///

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// If there is no IOAPIC behind the root port, set EOI Forwarding Disable bit (PCIE RP PCI offset D4h[1]) to 1b.
  /// For Empty Hot Plug Slot, set is done in InitCpuPcieSingleRootPort ()
  ///

  ///
  /// System bios should initiate link retrain for all slots that has card populated after register restoration.
  /// Done in SaPciexpressHelpersLibrary.c CpuPcieInitRootPortDownstreamDevices ()
  ///

  ///
  /// Configure Completion Timeout
  ///
  Data16And = (UINT16) ~(B_PCIE_DCTL2_CTD | B_PCIE_DCTL2_CTV_MASK);
  Data16Or  = 0;
  if (RootPortConfig->PcieRpCommonConfig.CompletionTimeout == CpuPcieCompletionTO_Disabled) {
    DEBUG((DEBUG_INFO, "Completion timeout disabled\n"));
    Data16Or = B_PCIE_DCTL2_CTD;
  } else {
    switch (RootPortConfig->PcieRpCommonConfig.CompletionTimeout) {
      case CpuPcieCompletionTO_Default:
        Data16Or = V_PCIE_DCTL2_CTV_DEFAULT;
        break;

      case CpuPcieCompletionTO_16_55ms:
        Data16Or = V_PCIE_DCTL2_CTV_40MS_50MS;
        break;

      case CpuPcieCompletionTO_65_210ms:
        Data16Or = V_PCIE_DCTL2_CTV_160MS_170MS;
        break;

      case CpuPcieCompletionTO_260_900ms:
        Data16Or = V_PCIE_DCTL2_CTV_400MS_500MS;
        break;

      case CpuPcieCompletionTO_1_3P5s:
        Data16Or = V_PCIE_DCTL2_CTV_1P6S_1P7S;
        break;

      default:
        Data16Or = 0;
        break;
    }
  }
  DEBUG ((DEBUG_INFO, "Completion timeout = %x\n", Data16Or));
  PciSegmentAndThenOr16 (RpBase + R_PCIE_DCTL2, Data16And, Data16Or);

  ///
  /// For Root Port Slots Numbering on the CRBs.
  ///
  Data32Or  = 0;
  Data32And = (UINT32) (~(B_PCIE_SLCAP_SLV_MASK | B_PCIE_SLCAP_SLS_MASK | B_PCIE_SLCAP_PSN_MASK));
  ///
  /// PCH BIOS Spec section 8.8.2.1
  /// Note: If Hot Plug is supported, then write a 1 to the Hot Plug Capable (bit6) and Hot Plug
  /// Surprise (bit5) in the Slot Capabilities register, PCIE RP PCI offset 54h. Otherwise,
  /// write 0 to the bits PCIe Hot Plug SCI Enable
  ///
  Data32And &= (UINT32) (~(B_PCIE_SLCAP_HPC | B_PCIE_SLCAP_HPS));
  if (RootPortConfig->PcieRpCommonConfig.HotPlug) {
    DEBUG ((DEBUG_INFO, "Write a 1 to the Hot Plug Capable (bit6) and Hot PlugSurprise (bit5)\n"));
    Data32Or |= (B_PCIE_SLCAP_HPC | B_PCIE_SLCAP_HPS);
  }
  //
  // Recommended values are SLV = 75 and SLS = 0 for max 75 Watt Slot power
  //
  Data32Or |= (UINT32) (75 << B_PCIE_SLCAP_SLV_OFFSET);
  Data32Or |= (UINT32) (0 << B_PCIE_SLCAP_SLS_OFFSET);

  ///
  /// PCH BIOS Spec section 8.2.4
  /// Initialize Physical Slot Number for Root Ports
  ///
  Data32Or |= (UINT32)(RootPortConfig->PcieRpCommonConfig.PhysicalSlotNumber << B_PCIE_SLCAP_PSN_OFFSET);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_SLCAP, Data32And, Data32Or);

  switch (GetCpuPcieSipInfo (PortIndex)) {
    case PCIE_SIP17:
      CpuPcieSip17InitCapabilityList (PortIndex, RpBase, RootPortConfig);
      break;
    case PCIE_SIP16:
      CpuPcieSip16InitCapabilityList (PortIndex, RpBase, RootPortConfig);
      break;
    default:
      ASSERT (FALSE);
      return;
  }

  //
  // All actions involving LinkDisable must finish before anything is programmed on endpoint side
  // because LinkDisable resets endpoint
  //

  ///
  /// Perform equalization for Gen3/Gen4 capable ports
  ///
  if (RpGen34Capable || mCpuPcieRpConfig->PcieCommonConfig.ComplianceTestMode) {
    DEBUG ((DEBUG_INFO, "Perform equalization for Gen3/Gen4 capable ports\n"));
    CpuPcieLinkEqualization (PortIndex, SiPolicy, TempPciBus, KeepPortVisible, CpuPcieRpPreMemConfig);
  }
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set "Link Speed Training Policy", Dxx:Fn + D4h[6] to 1.
  /// Make sure this is after mod-PHY related programming is completed.
  /// C50h[3:0] "LSTP Target Link Speed" doesn't need to program because LSTP is set to 1
  PciSegmentOr32 (RpBase + R_PCIE_MPC2, B_PCIE_MPC2_LSTP);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Step 29 If Transmitter Half Swing is enabled, program the following sequence
  /// a. Ensure that the link is in L0.
  /// b. Program the Link Disable bit (0x50[4]) to 1b.
  /// c. Program the Analog PHY Transmitter Voltage Swing bit (0xE8[13]) to set the transmitter swing to half/full swing
  /// d. Program the Link Disable bit (0x50[4]) to 0b.
  /// BIOS can only enable this on SKU where GEN3 capability is disabled on that port
  RpLinkSpeed   = PciSegmentRead8 (RpBase + R_PCIE_LCAP) & B_PCIE_LCAP_MLS;
  if (RpLinkSpeed < V_PCIE_LCAP_MLS_GEN3 && RootPortConfig->PcieRpCommonConfig.TransmitterHalfSwing) {
    PciSegmentOr8 (RpBase + R_PCIE_LCTL, B_PCIE_LCTL_LD);
    while (CpuPcieIsLinkActive (RpBase)) {
      // wait until link becomes inactive before changing swing
    }
    DEBUG ((DEBUG_INFO, "Program Tx Swing\n"));
    PciSegmentOr16 (RpBase + R_PCIE_PWRCTL, B_PCIE_PWRCTL_TXSWING);
    PciSegmentAnd8 (RpBase + R_PCIE_LCTL, (UINT8) ~(B_PCIE_LCTL_LD));
  }
  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set "Poisoned TLP Non-Fatal Advisory Error Enable", Dxx:Fn + D4h[12] to 1
  /// Set "IOSF Packet Fast Transmit Mode(IPF)", Dxx:Fn + D4h[11] to 1
  ///
  Data32And = (UINT32)~(B_PCIE_MPC2_DISPLLEWL1SE);
  Data32Or = B_PCIE_MPC2_PTNFAE | B_PCIE_MPC2_IPF;
  PciSegmentAndThenOr32 (RpBase + R_PCIE_MPC2, Data32And, Data32Or);

  //
  // L1LOW LTR threshold latency value
  //
  PciSegmentAndThenOr32 (
    RpBase + R_PCIE_PCIEPMECTL,
    (UINT32) ~B_PCIE_PCIEPMECTL_L1LTRTLV_MASK,
    (V_PCIE_PCIEPMECTL_L1LTRTLV << B_PCIE_PCIEPMECTL_L1LTRTLV_OFFSET)
    );
  DEBUG ((DEBUG_VERBOSE, "R_PCIE_PCIEPMECTL after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_PCIEPMECTL)));
  ///
  /// Additional configurations
  ///
  ///
  /// Configure Error Reporting policy in the Device Control Register
  ///
  Data16And = (UINT16) (~(B_PCIE_DCTL_URE | B_PCIE_DCTL_FEE | B_PCIE_DCTL_NFE | B_PCIE_DCTL_CEE));
  Data16Or  = 0;

  if (RootPortConfig->PcieRpCommonConfig.UnsupportedRequestReport) {
    Data16Or |= B_PCIE_DCTL_URE;
  }

  if (RootPortConfig->PcieRpCommonConfig.FatalErrorReport) {
    Data16Or |= B_PCIE_DCTL_FEE;
  }

  if (RootPortConfig->PcieRpCommonConfig.NoFatalErrorReport) {
    Data16Or |= B_PCIE_DCTL_NFE;
  }

  if (RootPortConfig->PcieRpCommonConfig.CorrectableErrorReport) {
    Data16Or |= B_PCIE_DCTL_CEE;
  }

  PciSegmentAndThenOr16 (RpBase + R_PCIE_DCTL, Data16And, Data16Or);
  DEBUG ((DEBUG_VERBOSE, "R_PCIE_DCTL after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_DCTL)));
  ///
  /// Configure Interrupt / Error reporting in R_PCIE_RCTL
  ///
  Data16And = (UINT16) (~(B_PCIE_RCTL_SFE | B_PCIE_RCTL_SNE | B_PCIE_RCTL_SCE));
  Data16Or  = 0;

  if (RootPortConfig->PcieRpCommonConfig.SystemErrorOnFatalError) {
    Data16Or |= B_PCIE_RCTL_SFE;
  }

  if (RootPortConfig->PcieRpCommonConfig.SystemErrorOnNonFatalError) {
    Data16Or |= B_PCIE_RCTL_SNE;
  }

  if (RootPortConfig->PcieRpCommonConfig.SystemErrorOnCorrectableError) {
    Data16Or |= B_PCIE_RCTL_SCE;
  }

  PciSegmentAndThenOr16 (RpBase + R_PCIE_RCTL, Data16And, Data16Or);
  DEBUG ((DEBUG_VERBOSE, "R_PCIE_RCTL after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_RCTL)));
  ///
  /// Root PCI-E Powermanagement SCI Enable
  ///
  DEBUG ((DEBUG_INFO, "Root PCI-E Powermanagement SCI Enable\n"));
  if (RootPortConfig->PcieRpCommonConfig.PmSci) {
    ///
    /// PCH BIOS Spec section 8.7.3 BIOS Enabling of Intel PCH PCI Express* PME SCI Generation
    /// Step 1
    /// Make sure that PME Interrupt Enable bit, Dxx:Fn:Reg 5Ch[3] is cleared
    ///
    PciSegmentAnd16 (RpBase + R_PCIE_RCTL, (UINT16) (~B_PCIE_RCTL_PIE));
    DEBUG ((DEBUG_VERBOSE, "R_PCIE_RCTL after clearing PIE = %x\n", PciSegmentRead32 (RpBase + R_PCIE_RCTL)));
    ///
    /// Step 2
    /// Program Misc Port Config (MPC) register at PCI config space offset
    /// D8h as follows:
    /// Power Management SCI Enable bit, Dxx:Fn:Reg D8h[31]
    /// Power Management SMI Enable bit, Dxx:Fn:Reg D8h[0]
    /// Use Byte Access to avoid RWO bit
    ///
    PciSegmentAnd8 (RpBase + R_PCIE_MPC, (UINT8) ~(B_PCIE_MPC_PMME));
    PciSegmentOr8 ((RpBase + R_PCIE_MPC + 3), (UINT8) (B_PCIE_MPC_PMCE >> 24));
    DEBUG ((DEBUG_VERBOSE, "R_PCIE_MPC after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_MPC)));
  }

  ///
  ///HotPlug is supported if its Enabled in Setup and feature.
  ///
  if ((RootPortConfig->PcieRpCommonConfig.HotPlug) && (IsHotPlugSupported(RpIndex))) {
    DEBUG ((DEBUG_INFO, "HotPlug is enabled in setup!\n"));
    ///
    /// PCH BIOS Spec section 8.8.2.1
    /// Step 1
    /// Clear following status bits, by writing 1b to them, in the Slot
    /// Status register at offset 1Ah of PCI Express Capability structure:
    /// Presence Detect Changed (bit3)
    /// Clear DLLSC (bit8)
    ///
    Data16Or = (UINT16)(B_PCIE_SLSTS_PDC | B_PCIE_SLSTS_DLLSC);
    PciSegmentOr16 (RpBase + R_PCIE_SLSTS, Data16Or);
    DEBUG ((DEBUG_VERBOSE, "Slot Status = %x\n", PciSegmentRead16 (RpBase + R_PCIE_SLSTS)));
    ///
    /// Step 2
    /// Program the following bits in Slot Control register at offset 18h
    /// of PCI Express* Capability structure:
    /// Presence Detect Changed Enable (bit3) = 1b
    /// Hot Plug Interrupt Enable (bit5) = 0b
    ///
    PciSegmentAndThenOr16 (RpBase + R_PCIE_SLCTL, (UINT16) (~B_PCIE_SLCTL_HPE), B_PCIE_SLCTL_PDE);
    DEBUG ((DEBUG_VERBOSE, "Slot Control = %x\n", PciSegmentRead16 (RpBase + R_PCIE_SLCTL)));
    ///
    /// Step 3
    /// Program Misc Port Config (MPC) register at PCI config space offset
    /// D8h as follows:
    /// Hot Plug SCI Enable (HPCE, bit30)
    /// Hot Plug SMI Enable (HPME, bit1)
    /// Use byte access to avoid premature locking BIT23, SRL
    ///
    PciSegmentAnd8 (RpBase + R_PCIE_MPC, (UINT8)(~B_PCIE_MPC_HPME));
    PciSegmentOr8 (RpBase + R_PCIE_MPC + 3, B_PCIE_MPC_HPCE >> 24);
    DEBUG ((DEBUG_VERBOSE, "R_PCIE_MPC after write = %x\n", PciSegmentRead8 (RpBase + R_PCIE_MPC)));

    /// Program EQCFG1 register at PCI config space offset
    /// 450h as follows:
    /// Link Equalization Request SMI Enable (LERSMIE, bit21)
    /// Link Equalization Request SCI Enable (LERSCIE, bit14)
    ///

    /// Program SMSCS register at PCI config space offset
    /// DCh as follows:
    /// Power Management SCI Status (PMCS, bit31)
    /// Hot Plug SCI Status (HPCS, bit30)
    /// L1 Substate Exit SCI Status (L1SSECS, bit29)
    /// Link Equalization Request SCI Status (LERSCIS, bit6)
    /// Link Equalization Request SMI Status (LERSMIS, bit5)
    /// Hot Plug Link Active State Changed SMI Status (HPLAS, bit4)
    /// Hot Plug Presence Detect SMI Status (HPPDM, bit1)
    /// Power Management SMI Status (PMMS, bit0)

    ///
    /// PCH BIOS Spec section 8.9
    /// BIOS should mask the reporting of Completion timeout (CT) errors by setting
    /// the uncorrectable Error Mask register PCIE RP PCI offset 108h[14].
    ///
    PciSegmentOr32 (RpBase + R_PCIE_UEM, B_PCIE_UEM_CT);
    DEBUG ((DEBUG_VERBOSE, "R_PCIE_UEM after write = %x\n", PciSegmentRead32 (RpBase + R_PCIE_UEM)));
  }

  ///
  /// PCH BIOS Spec Section 8.10 PCI Bus Emulation & Port80 Decode Support
  /// The I/O cycles within the 80h-8Fh range can be explicitly claimed
  /// by the PCIe RP by setting MPC.P8XDE, PCI offset D8h[26] = 1 (using byte access)
  /// BIOS must also configure the corresponding DMI registers GCS.RPR and GCS.RPRDID
  /// to enable DMI to forward the Port8x cycles to the corresponding PCIe RP
  ///

  /// @ TODO : EnablePort8xhDecode can be enabled only in one port(CPU + PCH) for a given platform so need to have a common setup option for this as well

  //if (mCpuPcieRpConfig->EnablePort8xhDecode && (PortIndex == (UINT8) mCpuPcieRpConfig->CpuPciePort8xhDecodePortIndex)) {
  //  PciSegmentOr8 (RpBase + R_PCIE_MPC + 3, (UINT8) (B_PCIE_MPC_P8XDE >> 24));
  //  PchIoPort80DecodeSet (PortIndex);
  //}

  ///
  /// Program B0:Dxx:Fn + 68h [7] = 1h (SIP16)
  /// Program B0:Dxx:Fn + 68h [6] = 1h
  ///
  Data32And = (UINT32)~(B_PCIE_DCTL2_AEB | B_PCIE_DCTL2_ARE);
  Data32Or  = B_PCIE_DCTL2_ARE;
  if (GetCpuPcieSipInfo(RpIndex) == PCIE_SIP16) {
    Data32Or |= B_PCIE_DCTL2_AEB;
  }
  PciSegmentAndThenOr32 (RpBase + R_PCIE_DCTL2, Data32And, Data32Or);
  DEBUG ((DEBUG_VERBOSE, "R_PCIE_DCTL2 after programming Atomics = %x\n", PciSegmentRead32 (RpBase + R_PCIE_DCTL2)));

  //
  // Program 10-Bit Tag Requester Enable (PX10BTRE) 0x68[12] = 0x0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_DCTL2_OFFSET,(UINT32) ~(B_PCIE_DCTL2_PX10BTRE));
  DEBUG ((DEBUG_INFO, "R_PCIE_DCTL2_OFFSET = %x\n", PciSegmentRead32 (RpBase + R_PCIE_DCTL2_OFFSET)));
  //
  // Program Fabric 10-bit Tag Support Enable (F10BTSE) 0x5BC[24] = 0x0
  //
  PciSegmentAnd32 (RpBase + R_PCIE_ADVMCTRL, (UINT32)~B_PCIE_ADVMCTRL_F10BTSE);
  DEBUG ((DEBUG_INFO, "R_PCIE_ADVMCTRL = %x\n", PciSegmentRead32 (RpBase + R_PCIE_ADVMCTRL)));

  ///
  /// Program registers for Retimer Support.
  ///
  PcieSipConfigureRetimerSupport (&RpDevPrivate.RpDev);

  //
  // Initialize R/WO Registers that described in PCH BIOS Spec
  //
  ///
  /// SRL bit is write-once and lock, so SRL, UCEL and CCEL must be programmed together
  /// otherwise UCEL/CCEL programming would lock SRL prematurely in wrong state
  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set Common Clock Exit Latency,      Dxx:Fn + D8h[17:15] = 6h
  /// Set Non-common Clock Exit Latency,  Dxx:Fn + D8h[20:18] = 7h
  /// Set Flow Control During L1 Entry,   Dxx:Fn + D8h[21]= 1h (BIOS to set it to 1b from ADP*/ADL* onwards)
  /// Set Flow Control Update Policy,     Dxx:Fn + D8h[6:4]= 4h (BIOS to set it to 100b from ADP*/ADL* D* onwards)
  ///
  Data32And = ~(UINT32) (B_PCIE_MPC_UCEL_MASK | B_PCIE_MPC_CCEL_MASK);
  Data32Or = (V_PCIE_MPC_UCEL << B_PCIE_MPC_UCEL_OFFSET) | B_PCIE_MPC_FCDL1E;
  if (GetCpuPcieSipInfo (PortIndex) == PCIE_SIP17) {
    Data32And &= ~(UINT32) (B_PCIE_MPC_FCP_MASK);
    Data32Or |= (V_PCIE_MPC_SIP17_CCEL << B_PCIE_MPC_CCEL_OFFSET) | (V_PCIE_MPC_SIP17_FCP << B_PCIE_MPC_FCP_OFFSET);
  } else {
    Data32Or |= (V_PCIE_MPC_SIP16_CCEL << B_PCIE_MPC_CCEL_OFFSET);
  }

  PciSegmentAndThenOr32 (RpBase + R_PCIE_MPC, Data32And, Data32Or);
  DEBUG ((DEBUG_VERBOSE, "R_PCIE_MPC = %x\n", PciSegmentRead32 (RpBase + R_PCIE_MPC)));

  //
  // Set Dxx:Fn +0x04[8]=1
  // Set Dxx:Fn +0x3E[1]=1
  //
  Data16Or = (UINT16) (B_PCIE_CMD_SEE);
  PciSegmentOr16 (RpBase + PCI_COMMAND_OFFSET, Data16Or);
  DEBUG ((DEBUG_INFO, "PCI_COMMAND_OFFSET after write SEE = %x\n", PciSegmentRead16 (RpBase + PCI_COMMAND_OFFSET)));

  Data16Or = (UINT16) (B_PCIE_BCTRL_SE);
  PciSegmentOr16 (RpBase + R_PCIE_BCTRL, Data16Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_BCTRL after write SE= %x\n", PciSegmentRead16 (RpBase + R_PCIE_BCTRL)));

  //
  // Note : BIOS maps RpIndex = 0 to PEG060 to be consistent across all SKUs
  // Set Dxx:Fn +0xFC[26]=1 for RootPort  0
  // Set Dxx:Fn +0xFC[24]=1 for RootPort  1
  // Set Dxx:Fn +0xFC[25]=1 for RootPort  2
  // Set Dxx:Fn +0xFC[25:24]=11b for RootPort  3
  //

  Data32Or = 0;
    switch (RpIndex) {
      case 0:
        Data32Or |= V_PCIE_STRPFUSECFG_PXIP_INTD;
        break;
      case 1:
        Data32Or |= V_PCIE_STRPFUSECFG_PXIP_INTA;
        break;
      case 2:
        Data32Or |= V_PCIE_STRPFUSECFG_PXIP_INTB;
        break;
      case 3:
        Data32Or |= V_PCIE_STRPFUSECFG_PXIP_INTC;
        break;
      default:
        ASSERT (FALSE);
        return;
    }
  PciSegmentAndThenOr32 (RpBase + R_PCIE_STRPFUSECFG, (UINT32) ~B_PCIE_STRPFUSECFG_PXIP_MASK, Data32Or);

  if (!IsSimicsEnvironment()) {
    if (GetCpuPcieSipInfo(PortIndex) == PCIE_SIP17) {
      CpuPcieSip17ResetPolicyInit(PortIndex);
      CpuPcieSip17PhyInit(PortIndex);
      ///
      /// PHY RECAL programming needs to be done before training the link to higher speeds for Gen5 controllers
      ///
      CpuPcieSip17PhyRecalRequest(PortIndex);
      CpuPcieSip17EbThreshold (PortIndex);
      ///
      /// Program (R_PCIE_RCRB_SDPILPCFG1) 1484h
      /// Set 1484h[22] to 0x1 G12kAlignMode
      /// Set 1484h[21:19] to 0x1 G12kAlignUnlockCnt
      /// Set 1484h[18:15] to 0x3 G12kAlignlockCnt
      ///
      Data32And = (UINT32) ~(B_PCIE_RCRB_SDPILPCFG1_G12KALIGNMODE | B_PCIE_RCRB_SDPILPCFG1_G12KALIGNUNLOCKCNT_MASK | B_PCIE_RCRB_SDPILPCFG1_G12KALIGNLCKCNT_MASK);
      Data32Or  = (UINT32) (B_PCIE_RCRB_SDPILPCFG1_G12KALIGNMODE | (V_PCIE_RCRB_SDPILPCFG1_G12KALIGNUNLOCKCNT << B_PCIE_RCRB_SDPILPCFG1_G12KALIGNUNLOCKCNT_OFFSET)
                            | (V_PCIE_RCRB_SDPILPCFG1_G12KALIGNLCKCNT << B_PCIE_RCRB_SDPILPCFG1_G12KALIGNLCKCNT_OFFSET));
      CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_SDPILPCFG1, Data32And, Data32Or);
    }
  }
}

/**
  Perform ACK Latency Register Programming.

  @param[in] RpBase             Root Port PCI base address
**/
VOID
AckLatency (
  IN UINT64               RpBase
  )
{
  UINT32                            Data32Or;
  UINT32                            Data32And;

  ///
  /// Program R_PCIE_PCIEATL[31C]
  /// SIP16:
  /// Set 31C[8:6] G1x1 to 0x7
  ///
  Data32And = (UINT32)~ (B_PCIE_PCIEATL_G1X1_MASK);
  Data32Or = (V_PCIE_PCIEATL_G1X1 << B_PCIE_PCIEATL_G1X1_OFFSET);
  PciSegmentAndThenOr32(RpBase + R_PCIE_PCIEATL, Data32And, Data32Or);
  DEBUG((DEBUG_INFO, "R_PCIE_PCIEATL after read : %x\n", PciSegmentRead32(RpBase + R_PCIE_PCIEATL)));

  ///
  /// Program R_PCIE_PCIEATL1[6A8]
  /// SIP16:
  /// Set 6A8[27:29] G4x8 to 0x2
  /// Set 6A8[26:24] G4x4 to 0x3
  /// Set 6A8[23:21] G4x2 to 0x4
  /// Set 6A8[20:18] G4x1 to 0x4
  /// Set 6A8[17:15] G3x8 to 0x2
  /// Set 6A8[14:12] G3x16 to 0x1
  /// Set 6A8[8:6] G2x16 to 0x1
  /// Set 6A8[11:9] G2x8 to 0x1
  /// Set 6A8[5:3] G1x8 to 0x1
  ///
  Data32And = (UINT32)~(B_PCIE_PCIEATL1_G4X8_MASK | B_PCIE_PCIEATL1_G4X4_MASK | B_PCIE_PCIEATL1_G4X2_MASK | B_PCIE_PCIEATL1_G4X1_MASK
              | B_PCIE_PCIEATL1_G3X16_MASK | B_PCIE_PCIEATL1_G3X8_MASK | B_PCIE_PCIEATL1_G2X16_MASK | B_PCIE_PCIEATL1_G2X8_MASK | B_PCIE_PCIEATL1_G1X8_MASK);
  Data32Or = (V_PCIE_PCIEATL1_G4X8 << B_PCIE_PCIEATL1_G4X8_OFFSET) | (V_PCIE_PCIEATL1_G4X4 << B_PCIE_PCIEATL1_G4X4_OFFSET) | (V_PCIE_PCIEATL1_G4X2 << B_PCIE_PCIEATL1_G4X2_OFFSET) | (V_PCIE_PCIEATL1_G4X1 << B_PCIE_PCIEATL1_G4X1_OFFSET)
              | (V_PCIE_PCIEATL1_G3X16 << B_PCIE_PCIEATL1_G3X16_OFFSET) | (V_PCIE_PCIEATL1_G3X8 << B_PCIE_PCIEATL1_G3X8_OFFSET) | (V_PCIE_PCIEATL1_G2X16 << B_PCIE_PCIEATL1_G2X16_OFFSET) | (V_PCIE_PCIEATL1_G2X8 << B_PCIE_PCIEATL1_G2X8_OFFSET) | (V_PCIE_PCIEATL1_G1X8 << B_PCIE_PCIEATL1_G1X8_OFFSET);

  PciSegmentAndThenOr32 (RpBase + R_PCIE_PCIEATL1, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_PCIEATL1 after read : %x\n", PciSegmentRead32(RpBase + R_PCIE_PCIEATL1)));
}

/**
  Disables the root port at Function 0.

  @param[in] RpIndex            Root Port Number.
**/
VOID
DisableCpuPcieFunc0 (
  IN UINT8   RpIndex
  )
{
  UINT32      Data32;
  UINT32      LoopTime;
  UINT32      TargetState;
  UINT64      RpBase;
  UINT32      Data32And;
  UINT32      Data32Or;

  DEBUG ((DEBUG_INFO, "CPU: LinkDisablePcieRootPort(%d) Start\n", RpIndex + 1));

  Data32   = 0;
  RpBase   = CpuPcieBase(RpIndex);
  Data32Or = 0;

  //
  // if (ADP-S D2 PCIe*, ADL-S D2 PCIe* or onwards)
  // set PMCS.PS = 2'b11 (D3HOT) before Function Disable the port.
  //
  if (CpuPcieIsEndpointConnected(RpBase)) {
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_LCTL, ~0u, B_PCIE_LCTL_LD);
  } else {
    DEBUG ((DEBUG_INFO, "Device is not present.Stopping the port.\n"));
    PegPciSegmentAndThenOr32 (RpIndex, R_PCIE_PCIEALC, ~0u, B_PCIE_PCIEALC_BLKDQDA);

    TargetState = V_PCIE_PCIESTS1_LTSMSTATE_DETRDY;
    for (LoopTime = 0; LoopTime < 5000; LoopTime++) {
      Data32 = PciSegmentRead32 (RpBase + R_PCIE_PCIESTS1);
      if (((Data32 & B_PCIE_PCIESTS1_LTSMSTATE) >> B_PCIE_PCIESTS1_LTSMSTATE_OFFSET) == TargetState) {
        break;
      }
      MicroSecondDelay (10);
    }
    ///
    ///
  }

  if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
    Data32And = (UINT32)~B_PCIE_PMCS_PS_MASK;
    Data32Or = (UINT32)V_PCIE_PMCS_PS;
    PciSegmentAndThenOr32 (RpBase +  R_PCIE_PMCS, Data32And, Data32Or);
  }
  ///
  /// Set offset 408h[27] to 1b to disable squelch.
  ///
  DEBUG ((DEBUG_VERBOSE, "Disable squelch\n"));
  PegPciSegmentAndThenOr32(RpIndex, R_PCIE_PHYCTL4, ~0u, B_PCIE_PHYCTL4_SQDIS);
  DisableClock (PchClockUsageCpuPcie0 + RpIndex);
  DEBUG ((DEBUG_INFO, "CPU: LinkDisablePcieRootPort() End\n"));
}

/**
  Perform Initialization of the Downstream Root Ports.

  @param[in] SiPolicy             The SI Policy PPI
**/
VOID
CpuPcieRpInit (
  IN CONST SI_POLICY_PPI     *SiPolicy
  )
{
  CPU_PCIE_RP_PREMEM_CONFIG  *CpuPcieRpPreMemConfig;
  SI_PREMEM_POLICY_PPI       *SiPreMemPolicyPpi;
  EFI_STATUS                 Status;
  UINT8                      RpIndex;
  UINT64                     RpBase;
  UINTN                      RpDevice;
  UINTN                      RpFunction;
  UINT8                      MaxCpuPciePortNum;
  UINT32                     RpDisableMask;
  UINT32                     RpClkreqMask;
  UINT32                     Timer;
  UINT32                     DetectTimeoutUs;
  BOOLEAN                    Gen4DeviceFound[CPU_PCIE_MAX_ROOT_PORTS];
  BOOLEAN                    KeepPortVisible;
  UINT8                      TempPciBusMin;
  UINT8                      TempPciBusMax;
  UINT32                     Data32Or;
  UINT32                     Data32And;
  CPU_PCIE_HOB               *CpuPcieHob;
  UINT8                      FomsCpValue;
  UINT8                      CpuPcieSpeed;
  UINT8                      Tls;
  SBDF                       RpSbdf;
  UINTN                      RpDev;
  UINTN                      RpFunc;
  UINT64                     PcieBase;
  CPU_PCIE_ROOT_PORT_CONFIG  *PcieRpConfig;
  UINT32                     DekelFwVersion;
  UINT32                     Haed;
  UINT32                     Px16ghaed;
  UINT32                     HsPhyRpEnabledMask;
  UINT32                     HsPhyRpMap;
  UINT32                     HsPhyRecipeVersion;
  UINT32                     HsPhyFwVersion;
  UINT32                     CpuPcieDekelMap;
  UINT32                     Function0RemappingEnableMask;
  UINT32                     MailboxStatus;
  UINT32                     MailboxData;

  Haed            = 0;
  RpBase          = 0;
  Px16ghaed       = 0;
  CpuPcieSpeed    = 0;
  DetectTimeoutUs = 0;

  HsPhyRpEnabledMask = 0;
  HsPhyRpMap         = 0;
  KeepPortVisible    = FALSE;

  DEBUG ((DEBUG_INFO, "CPU PcieRpInit() Start\n"));
  TempPciBusMin = PcdGet8 (PcdSiliconInitTempPciBusMin);
  TempPciBusMax = PcdGet8 (PcdSiliconInitTempPciBusMax);

  Status = GetConfigBlock ((VOID *) SiPolicy, &gCpuPcieRpConfigGuid, (VOID *) &mCpuPcieRpConfig);
  ASSERT_EFI_ERROR (Status);

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuPcieRpPrememConfigGuid, (VOID *) &CpuPcieRpPreMemConfig);
  ASSERT_EFI_ERROR(Status);
  DEBUG_CODE_BEGIN ();
  CpuPciePolicySanityCheck (mCpuPcieRpConfig);
  DEBUG_CODE_END ();

  CpuPcieHob = NULL;
  CpuPcieHob = (CPU_PCIE_HOB *)GetFirstGuidHob(&gCpuPcieHobGuid);
  if (CpuPcieHob == NULL) {
    DEBUG ((DEBUG_ERROR, "ERROR: CpuPcieHob not found so return from post mem RpInit\n"));
    ASSERT (CpuPcieHob != NULL);
    return;
  }
  if (CpuPcieHob->RpEnMaskFromDevEn == 0) {
    DEBUG ((DEBUG_ERROR, "All CPU PCIe root ports are fused off!!\n"));
    return;
  } else if (CpuPcieHob->RpEnabledMask == 0) {
    DEBUG ((DEBUG_ERROR, "All CPU PCIe root ports are disabled!!\n"));
  }


  Timer            = 0;
  MaxCpuPciePortNum = GetMaxCpuPciePortNum ();
  RpDisableMask    = ~(CpuPcieHob->RpEnabledMask);
  RpClkreqMask     = 0;
  CpuPcieHob->SlotSelection = (UINT8) mCpuPcieRpConfig->SlotSelection;

  DEBUG ((DEBUG_INFO, "----    RpIndex mapping    ----\n"));
  DEBUG ((DEBUG_INFO, "---- RpIndex = 0 ; PortId = 1 => PEG060 ----\n"));

  DEBUG ((DEBUG_INFO, "Maximum Cpu Pcie Ports = %x\n", MaxCpuPciePortNum));

  if (GetPcieImrPortLocation () == ImrRpLocationSa) {
    CpuPcieEnablePcieImr (GetPcieImrPortNumber ());
  }

  CpuPcieDekelMap = CpuPcieGetDekelPortMap() & CpuPcieHob->RpEnabledMask;
  HsPhyRpMap = CpuPcieHob->HsPhyMap;
  for (RpIndex = 0; RpIndex < GetMaxCpuPciePortNum (); RpIndex++) {
    HsPhyRpEnabledMask |= (BIT0 << RpIndex);
    if (HsPhyRpMap & (BIT0 << RpIndex)) {
      DEBUG ((DEBUG_INFO, "Initialize  Link training for Hsphy supported port%x!\n", RpIndex+1));
      if (HsPhyFwStatus (RpIndex) == TRUE) {
        DEBUG((DEBUG_INFO, "HsPhy FW download Successfull!!!\n"));
        CpuPcieTrainLinkToGen1 (RpIndex);
        //
        // Read the HS-Phy Recipe version and populate in HOB to be printed in BIOS setup
        //
        HsPhyRecipeVersion = GetHsPhyRecipeVersion(RpIndex);
        CpuPcieHob->HsPhyRecipeVersionMajor = (UINT32)((HsPhyRecipeVersion & B_HSPHY_RECIPE_VERSION_MAJOR_MASK) >> B_HSPHY_RECIPE_VERSION_MAJOR_OFFSET);
        CpuPcieHob->HsPhyRecipeVersionMinor = (UINT32)((HsPhyRecipeVersion & B_HSPHY_RECIPE_VERSION_MINOR_MASK) >> B_HSPHY_RECIPE_VERSION_MINOR_OFFSET);
        DEBUG((DEBUG_INFO, "HS-Phy Recipe version : %d.%d\n", CpuPcieHob->HsPhyRecipeVersionMajor, CpuPcieHob->HsPhyRecipeVersionMinor));
        //
        // Read the HS-Phy FW version and populate in HOB to be printed in BIOS setup
        //
        HsPhyFwVersion = GetHsPhyFwVersion(RpIndex);
        CpuPcieHob->HsPhyFwProdMajor      = (UINT32)((HsPhyFwVersion & B_HSPHY_FW_VERSION_PROD_MAJOR_MASK) >> B_HSPHY_FW_VERSION_PROD_MAJOR_OFFSET);
        CpuPcieHob->HsPhyFwProdMinor      = (UINT32)((HsPhyFwVersion & B_HSPHY_FW_VERSION_PROD_MINOR_MASK) >> B_HSPHY_FW_VERSION_PROD_MAJOR_OFFSET);
        CpuPcieHob->HsPhyFwHotFix         = (UINT32)((HsPhyFwVersion & B_HSPHY_FW_VERSION_HOTFIX_MASK) >> B_HSPHY_FW_VERSION_HOTFIX_OFFSET);
        CpuPcieHob->HsPhyFwBuild          = (UINT32)((HsPhyFwVersion & B_HSPHY_FW_VERSION_BUILD_MASK) >> B_HSPHY_FW_VERSION_BUILD_OFFSET);
        CpuPcieHob->HsPhyFwEvBitProgMajor = (UINT32)((HsPhyFwVersion & B_HSPHY_FW_VERSION_EV_BITPROG_MAJOR_MASK) >> B_HSPHY_FW_VERSION_EV_BITPROG_MAJOR_OFFSET);
        CpuPcieHob->HsPhyFwEvBitProgMinor = (UINT32)((HsPhyFwVersion & B_HSPHY_FW_VERSION_EV_BITPROG_MINOR_MASK) >> B_HSPHY_FW_VERSION_EV_BITPROG_MINOR_OFFSET);
        DEBUG((DEBUG_INFO, "HS-Phy Firmware version : %d.%d.%d.%d.%d.%d\n", CpuPcieHob->HsPhyFwProdMajor, CpuPcieHob->HsPhyFwProdMinor, CpuPcieHob->HsPhyFwHotFix, CpuPcieHob->HsPhyFwBuild, CpuPcieHob->HsPhyFwEvBitProgMajor, CpuPcieHob->HsPhyFwEvBitProgMinor));
      } else {
        DEBUG ((DEBUG_ERROR, "ERROR: HsPhy FW download failed!!!\n"));
        HsPhyRpEnabledMask &= ~ (BIT0 << RpIndex);
      }
    } else {
      DEBUG ((DEBUG_INFO, "Port%x is not connected to HSPHY\n", RpIndex+1));
    }
  }

  CpuPcieCheckforLinkActive (MaxCpuPciePortNum, CpuPcieHob->RpEnabledMask);

  RpDisableMask |= ~(HsPhyRpEnabledMask);

  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
    RpBase = CpuPcieBase (RpIndex);
    ///
    /// Set C80[0] DCDCCTDT to 0x0.
    ///
    PciSegmentAnd32 (RpBase + R_PCIE_AECR1G3, (UINT32)~(B_PCIE_AECR1G3_DCDCCTDT));

    if ((CpuPcieHob->RpEnabledMask) & (BIT0 << RpIndex)) {
      DEBUG ((DEBUG_INFO, "**** RpIndex = %x ****\n", RpIndex));
      //
      // Enable CLKREQ# regardless of port being available/enabled to allow clock gating.
      //
      if (IsClkReqAssigned (PchClockUsageCpuPcie0 + RpIndex)) {
        DEBUG ((DEBUG_INFO, "ClkReqAssigned, set RpClkreqMask\n"));
        RpClkreqMask |= (BIT0 << RpIndex);
      }

      ///
      /// Set the Slot Implemented Bit.
      /// The System BIOS must initialize the "Slot Implemented" bit of the PCI Express* Capabilities Register,
      /// XCAP Dxx:Fn:42h[8] of each available and enabled downstream root port.
      /// Ports with hotplug capability must have SI bit set
      /// The register is write-once so must be written even if we're not going to set SI, in order to lock it.
      ///
      /// This must happen before code reads PresenceDetectState, because PDS is invalid unless SI is set
      ///
      if (mCpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.SlotImplemented || mCpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.HotPlug) {
        PciSegmentOr16 (RpBase + R_PCIE_XCAP, B_PCIE_XCAP_SI);
      } else {
        PciSegmentAnd16 (RpBase + R_PCIE_XCAP, (UINT16)(~B_PCIE_XCAP_SI));
      }
      if ((PciSegmentRead16 (RpBase + R_PCIE_XCAP)) & B_PCIE_XCAP_SI){
        DEBUG ((DEBUG_INFO, "Slot implemented bit is set!\n"));
      }
      ///
      /// For non-hotplug ports disable the port if there is no device present.
      ///
      DetectTimeoutUs = mCpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.DetectTimeoutMs * 1000;
      if (IsCpuPcieDevicePresent (RpBase, DetectTimeoutUs, &Timer)) {
        DEBUG ((DEBUG_INFO, "Port %d has a device attached.\n", RpIndex + 1));
        //
        // At this point in boot, CLKREQ pad is still configured as GP input and doesnt' block clock generation
        // regardless of input state. Before switching it to native mode when it will start gating clock, we
        // verify if CLKREQ is really connected. If not, pad will not switch and power management
        // will be disabled in rootport.
        // By the time this code runs device can't have CPM or L1 substates enabled, so it is guaranteed to pull ClkReq down.
        // If ClkReq is detected to be high anyway, it means ClkReq is not connected correctly.
        // Checking pad's input value is primarily a measure to prevent problems with long cards inserted into short
        // open-ended PCIe slots on motherboards which route PRSNT signal to CLKREQ. Such config causes CLKREQ signal to float.
        //
        if (!CpuPcieDetectClkreq (RpIndex, mCpuPcieRpConfig)) {
          DEBUG ((DEBUG_INFO, "CLKREQ is not connected, clear RpClkreqMask!\n"));
          RpClkreqMask &= ~(BIT0 << RpIndex);
        }
      } else {
        if (mCpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.HotPlug == FALSE) {
          DEBUG ((DEBUG_INFO, "Port %d neither has Endpoint connected not HotPlug enabled!\n", RpIndex + 1));
          DEBUG ((DEBUG_INFO, "Set RpDisableMask for Port %d\n", RpIndex + 1));
          RpDisableMask |= (BIT0 << RpIndex);
        }
      }
    }
  }

  if (CheckHsphyWaRequired ()) {
    MailboxData = B_MAILBOX_BIOS_ALLOW_HSPHY_1;
    Status = MailboxWrite (MAILBOX_TYPE_PCODE, MAILBOX_BIOS_CMD_WRITE_HSPHY_CONFIG, MailboxData , &MailboxStatus);
    DEBUG ((DEBUG_INFO, "HSPHY 1 Configuration, Mailbox write Status = %x\n", MailboxStatus));
  }

  Function0RemappingEnableMask = CpuPcieFunction0RemapEnable (RpDisableMask);
  if (RpDisableMask != Function0RemappingEnableMask) {
    KeepPortVisible = TRUE;
    RpDisableMask &= Function0RemappingEnableMask;
    DEBUG ((DEBUG_INFO, "KeepPortVisible for Function0Remap: %x!\n", KeepPortVisible));
  }
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
    if (mCpuPcieRpConfig->RootPort2[RpIndex].Func0LinkDisable == 1) {
      RpDisableMask &= CpuPcieFunc0LinkDisable (RpDisableMask);
    }
  }

  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
    if ((~RpDisableMask & (BIT0 << RpIndex)) &&
        (CpuPcieDetectClkreq (RpIndex, mCpuPcieRpConfig))) {
      DEBUG ((DEBUG_INFO,"EnableClkReq for RPIndex = %x \n",RpIndex));
      EnableClkReq (PchClockUsageCpuPcie0 + RpIndex); //Configure CLKREQ# GPIO
    }
    //
    // Need to enable CLKREQ for function0 RootPort if next Function RootPort is enabled to allow RootPort to be PGed
    //
    if ((~Function0RemappingEnableMask & (BIT0 << RpIndex)) &&
       (CpuPcieRpPreMemConfig->ClkReqMsgEnableRp[RpIndex] != 0)) {
      if (KeepPortVisible) {
        DEBUG ((DEBUG_INFO,"KeepPortVisible EnableClkReq for RPIndex = %x \n",RpIndex));
        EnableClkReq (PchClockUsageCpuPcie0 + RpIndex); //Configure CLKREQ# GPIO
      }
    }
  }

  if (CpuPcieHob->RpEnMaskFromDevEn != 0) {
    DEBUG ((DEBUG_INFO, "Configure power management applicable to all port including disabled ports!\n"));
    for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
      RpBase = CpuPcieBase(RpIndex);
      if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
        DEBUG ((DEBUG_INFO, "Configure Power Down Mapping!\n"));
        CpuPcieSip17PowerDownMapping (RpIndex);
      }
      ///
      /// Configure power management applicable to all port including disabled ports.
      ///
      if (RpIndex != (UINT32) (GetMaxCpuPcieControllerNum ())) {
        ///
        /// TrunkClockGateEn depends on each of the controller ports supporting CLKREQ# or being disabled.
        /// power management done only on RP controllers.
        ///
        CpuPcieConfigureControllerBasedPowerManagement (
          RpIndex,
          (((RpClkreqMask | RpDisableMask) & (0xFu << RpIndex)) == (0xFu << RpIndex)),
          ((mCpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.ClockGating) ? TRUE:FALSE),
          ((mCpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PowerGating) ? TRUE:FALSE)
        );
      }
      ///
      /// PhyLanePgEnable depends on the port supporting CLKREQ# or being disabled.
      ///
      CpuPcieConfigurePortBasedPowerManagement (
        RpIndex,
        (((RpClkreqMask | RpDisableMask) & (BIT0 << RpIndex)) != 0),
        ((mCpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.ClockGating) ? TRUE:FALSE),
        ((mCpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PowerGating) ? TRUE:FALSE)
      );
    }
  }
  //
  // Lock Fia Configuration
  //
  if (mCpuPcieRpConfig->FiaProgramming) {
    for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
      DEBUG ((DEBUG_INFO, "Load Fia Programming - FIA programming Enabled!\n"));
      if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
        CpuPcieSip17FiaFinalizeConfigurationAndLock (RpIndex);
      } else {
        CpuPcieSip16FiaFinalizeConfigurationAndLock (RpIndex);
      }
    }
  }
  //
  // Wait for all ports with PresenceDetect=1 to form a link
  // Having an active link is necessary to access and configure the endpoint
  // We cannot use results of IsCpuPcieDevicePresent() because it checks PDS only and may include
  // PCIe cards that never form a link, such as compliance load boards.
  //
  CpuPcieWaitForLinkActive (MaxCpuPciePortNum, RpDisableMask);
  // For each controller set Initialize Transaction Layer Receiver Control on Link Down
  // and Initialize Link Layer Receiver Control on Link Down.
  // Use sideband access in case 1st port of a controller is disabled
  ///
  /// Set 338h[29] to 0b for Initialize Transaction Layer Receiver Control on Link Down.
  /// Set 338h[28] to 0b for Initialize Link Layer Receiver Control on Link Down.
  /// Set 338h[21] to 1b for RTD3 Present Detect State Policy.
  ///
  Data32And = (UINT32)~(B_PCIE_PCIEALC_ITLRCLD | B_PCIE_PCIEALC_ILLRCLD);
  Data32Or = (UINT32) (B_PCIE_PCIEALC_RTD3PDSP);

  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum && RpIndex < 3; ++RpIndex) {
    RpBase = CpuPcieBase (RpIndex);
    PciSegmentAndThenOr32 (RpBase +  R_PCIE_PCIEALC, Data32And, Data32Or);
  }

  ///
  /// CPU PCIe PTM initialization
  ///
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; ++RpIndex) {
    RpBase = CpuPcieBase (RpIndex);
    switch (GetCpuPcieSipInfo (RpIndex)) {
      case PCIE_SIP17:
        if (CpuPciePtmConfiguration ())
        {
          CpuPcieSip17Ptm1px8Init (RpIndex);
        }
        else
        {
          CpuPcieSip17Ptm2px16Init (RpIndex);
        }
        break;
      case PCIE_SIP16:
        CpuPcieSip16PtmInit (RpBase);
        break;
      default:
        ASSERT (FALSE);
        return;
    }
  }

  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; ++RpIndex) {
    RpBase = CpuPcieBase(RpIndex);
    ///
    /// Set ECh[13] to 1b for Initialize Link Layer Receiver Control on Link Down.
    ///
    PciSegmentAndThenOr32 (RpBase + R_PCIE_DC, (UINT32)~B_PCIE_DC_COM, (UINT32)B_PCIE_DC_COM);
  }

  //
  // Read the FOMS Control Policy and program the register accordingly
  //
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; ++RpIndex) {
    DEBUG ((DEBUG_INFO, "FomsCp from policy : %x, RpIndex : %x\n", mCpuPcieRpConfig->RootPort[RpIndex].FomsCp, RpIndex));
    FomsCpValue = 0;
    switch (mCpuPcieRpConfig->RootPort[RpIndex].FomsCp)
    {
      case 0: // Auto
        Tls = PciSegmentRead16 (RpBase + R_PCIE_LCTL2) & B_PCIE_LCTL2_TLS;
        if (Tls == V_PCIE_LCTL2_TLS_GEN3) {
          FomsCpValue = 0;
        } else if (Tls == V_PCIE_LCTL2_TLS_GEN4) {
          FomsCpValue = 1;
        } else if (Tls == V_PCIE_LCTL2_TLS_GEN5) {
          FomsCpValue = 3;
        }
        break;
      case 1: // Gen3 FOMS
        FomsCpValue = 0;
        CpuPcieSpeed = CpuPcieGen3;
        break;
      case 2: // Gen4 FOMS
        FomsCpValue = 1;
        CpuPcieSpeed = CpuPcieGen4;
        break;
      case 3: // Gen3 and Gen4 FOMS
        FomsCpValue = 2;
        CpuPcieSpeed = CpuPcieGen4;
        break;
      case 4: // Gen5 FOMS
        FomsCpValue = 3;
        CpuPcieSpeed = CpuPcieGen5;
        break;
      default:
        FomsCpValue = 0;
        break;
    }
    DEBUG ((DEBUG_INFO, "FomsCpValue : %x\n", FomsCpValue));
    RpBase = CpuPcieBase (RpIndex);
    PciSegmentAndThenOr32  (RpBase + R_PCIE_EQCFG4, (UINT32)~B_PCIE_EQCFG4_FOMSCP_MASK , (FomsCpValue << B_PCIE_EQCFG4_FOMSCP_OFFSET));
    DEBUG ((DEBUG_INFO, "FomsCp after read : %x\n", PciSegmentRead32 (RpBase + R_PCIE_EQCFG4) & B_PCIE_EQCFG4_FOMSCP_MASK));
  }

  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; ++RpIndex) {
    GetCpuPcieRpDevFun (RpIndex, &RpDev, &RpFunc);
    RpBase = CpuPcieBase (RpIndex);
    RpSbdf.Seg = 0;
    RpSbdf.Bus = 0;
    RpSbdf.Dev = RpDev;
    RpSbdf.Func = RpFunc;
    PcieBase = PCI_SEGMENT_LIB_ADDRESS (0, 0, RpSbdf.Dev, RpSbdf.Func, 0);
    RpSbdf.PcieCap = PcieBaseFindCapId (PcieBase, EFI_PCI_CAPABILITY_ID_PCIEXP);
    PcieRpConfig = &mCpuPcieRpConfig->RootPort[RpIndex];

    if (RpDisableMask & (BIT0 << RpIndex)) {
      KeepPortVisible = CpuPcieIsPortForceVisible (RpIndex, RpDisableMask, mCpuPcieRpConfig);
      DEBUG ((DEBUG_INFO, "Disable mask is set for Portid %d\n", RpIndex + 1));
      DEBUG ((DEBUG_INFO, "KeepPortVisible = %x\n", KeepPortVisible));
      DisableCpuPcieRootPort (RpIndex, KeepPortVisible);
    } else {
      DEBUG ((DEBUG_INFO, "Call InitCpuPcieSingleRootPort for Portid %d\n", RpIndex + 1));
      InitCpuPcieSingleRootPort (
        RpIndex,
        SiPolicy,
        SiPreMemPolicyPpi,
        TempPciBusMin,
        &Gen4DeviceFound[RpIndex],
        KeepPortVisible,
        CpuPcieRpPreMemConfig
        );
      if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP16) {
        AckLatency (RpBase);
      }
      if (IsCpuPcieDevicePresent(RpBase, DetectTimeoutUs, &Timer)) {
        ///
        /// Initialize downstream devices
        ///
        DEBUG ((DEBUG_INFO, "Initialize downstream devices\n"));
        GetCpuPcieRpDevFun (RpIndex, &RpDevice, &RpFunction);
        RootportDownstreamConfiguration (
          SA_SEG_NUM,
          SA_MC_BUS,
          (UINT8) RpDevice,
          (UINT8) RpFunction,
          TempPciBusMin,
          TempPciBusMax,
          EnumCpuPcie
          );
      }
      if (IsRpMultiVc (RpIndex) && (PcieRpConfig->MultiVcEnabled == TRUE)) {
        RootportMultiVcConfiguration (RpSbdf);
      }
    }
  }

  //
  // Rx margin programming as per BWG
  //
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; ++RpIndex) {
    if ((RpDisableMask & (BIT0 << RpIndex)) == 0) {
    /// Program R_PCIE_RXMC1[C90]
    /// Set C90[20:25] MSRTS to 0x3F
    /// Set C90[13:14] TMSLOP to 0x1
    /// Set C90[11:12] VMSLOP to 0x1
    /// Set C90[9] MSRM to 0x0
    /// Set C90[2] MILRTS to 0x0
    /// Set C90[0] MIESS to 0x0
    /// SIP16:
    /// Set C90[4:8] MMNOLS to 0x1
    /// Set C90[1] MIUDVMS to 0x0
    /// Set C90[3] MVS to 0x0
    /// Set C90[26:31] MSRVS to 0x1F
    /// SIP17:
    /// Set C90[4:8] MMNOLS to 0x0
    /// Set C90[1] MIUDVMS to 0x1
    /// Set C90[3] MVS to 0x1
    /// Set C90[26:31] MSRVS to 0x3F
    ///

    Data32And = (UINT32)~(B_PCIE_RXMC1_MMNOLS_MASK | B_PCIE_RXMC1_VMSLOP_MASK | B_PCIE_RXMC1_TMSLOP_MASK
                          | B_PCIE_RXMC1_MSRTS_MASK | B_PCIE_RXMC1_MSRVS_MASK | B_PCIE_RXMC1_MSRM | B_PCIE_RXMC1_MVS
                          | B_PCIE_RXMC1_MILRTS | B_PCIE_RXMC1_MIUDVMS | B_PCIE_RXMC1_MIESS);
    Data32Or  = (UINT32) ((V_PCIE_RXMC1_VMSLOP << B_PCIE_RXMC1_VMSLOP_OFFSET) | (V_PCIE_RXMC1_TMSLOP << B_PCIE_RXMC1_TMSLOP_OFFSET) | (V_PCIE_RXMC1_MSRTS << B_PCIE_RXMC1_MSRTS_OFFSET));

    if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
      Data32Or |= B_PCIE_RXMC1_MVS | (0x3F << B_PCIE_RXMC1_MSRVS_OFFSET) | B_PCIE_RXMC1_MIUDVMS;
    } else {
      Data32Or |= (0x1F << B_PCIE_RXMC1_MSRVS_OFFSET) | (0x1 << B_PCIE_RXMC1_MMNOLS_OFFSET);
    }
    if (IsSimicsEnvironment()) {
      PciSegmentAndThenOr32(RpBase + R_PCIE_RXMC1, Data32And, Data32Or);
      DEBUG((DEBUG_INFO, "RXMC1 : %x\n", PciSegmentRead32(RpBase + R_PCIE_RXMC1)));
    } else {
      CpuRegbarAndThenOr32(GetCpuPcieRpSbiPid(RpIndex), R_PCIE_RXMC1, Data32And, Data32Or);
      DEBUG((DEBUG_INFO, "RXMC1 SB : %x\n", CpuRegbarRead32(GetCpuPcieRpSbiPid(RpIndex), R_PCIE_RXMC1)));
    }
    ///
    /// Program R_PCIE_RXMC2[C94]
    /// SIP16:
    /// Set C94[19:24] MNOTSS to 0x20
    /// Set C94[18:13] MMTOS to 0x28
    /// Set C94[12:6] MNOVSS to 0x28
    /// Set C94[5:0] MMVOS to 0x0
    ///
    /// SIP17:
    /// Set C94[19:24] MNOTSS to 0x3F
    /// Set C94[18:13] MMTOS to 0x32
    /// Set C94[12:6] MNOVSS to 0x7F
    /// Set C94[5:0] MMVOS to 0x0C
    ///
    Data32And = (UINT32) ~(B_PCIE_RXMC2_MNOTSS_MASK | B_PCIE_RXMC2_MMTOS_MASK | B_PCIE_RXMC2_MNOVSS_MASK | B_PCIE_RXMC2_MMVOS_MASK);
    if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
     Data32Or = ((0x3F << B_PCIE_RXMC2_MNOTSS_OFFSET) | (0x32 << B_PCIE_RXMC2_MMTOS_OFFSET)
                  | (0x7F << B_PCIE_RXMC2_MNOVSS_OFFSET) | (0x0C << B_PCIE_RXMC2_MMVOS_OFFSET));
    } else {
      Data32Or = ((0x20 << B_PCIE_RXMC2_MNOTSS_OFFSET) | (0x28 << B_PCIE_RXMC2_MMTOS_OFFSET) | (0x28 << B_PCIE_RXMC2_MNOVSS_OFFSET));
    }
    if (IsSimicsEnvironment()) {
      PciSegmentAndThenOr32(RpBase + R_PCIE_RXMC2, Data32And, Data32Or);
      DEBUG((DEBUG_INFO, "RXMC2 : %x\n", PciSegmentRead32(RpBase + R_PCIE_RXMC2)));
    } else {
    CpuRegbarAndThenOr32(GetCpuPcieRpSbiPid(RpIndex), R_PCIE_RXMC2, Data32And, Data32Or);
    DEBUG((DEBUG_INFO, "RXMC2 SB : %x\n", CpuRegbarRead32(GetCpuPcieRpSbiPid(RpIndex), R_PCIE_RXMC2)));
    }
    ///
    /// Set CE4[0] MARGINDRISW to 0x0
    ///
    PciSegmentAnd32(RpBase + R_PCIE_PL16MPCPS, (UINT32) ~(B_PCIE_PL16MPCPS_MARGINDRISW));

    }
  }

  //
  // Program the root port target link speed.
  //
  Status = CpuPcieRpSpeedChange ();
  ASSERT_EFI_ERROR(Status);

  //
  // We should not wait for HwEq done bit to get set because compliance load boards never form a link and does not complete HwEq flow
  //
  if (!mCpuPcieRpConfig->PcieCommonConfig.ComplianceTestMode) {
    ///
    /// check for hardware equalization done bit set in parallel for all rootports to optimize the wait time
    ///
    CpuPcieWaitForHwEqDone (CpuPcieRpPreMemConfig, RpDisableMask);
  }
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; ++RpIndex) {
    RpBase = CpuPcieBase (RpIndex);
    Haed = PciSegmentRead32 (RpBase + R_PCIE_EQCFG1);
    Haed &= (UINT32) B_PCIE_EQCFG1_HAED;
    Px16ghaed = PciSegmentRead32 (RpBase + R_PCIE_EQCFG4);
    Px16ghaed &= (UINT32) B_PCIE_EQCFG4_PX16GHAED;
    DetectTimeoutUs = mCpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.DetectTimeoutMs * 1000;
    if (IsCpuPcieDevicePresent (RpBase, DetectTimeoutUs, &Timer) && ((Haed == (UINT32) B_PCIE_EQCFG1_HAED) || (Px16ghaed == (UINT32) B_PCIE_EQCFG4_PX16GHAED))) {
      if (!mCpuPcieRpConfig->PcieCommonConfig.ComplianceTestMode) {
        if (((CpuPcieRpPreMemConfig->PcieSpeed[RpIndex] >= 3) || (CpuPcieRpPreMemConfig->PcieSpeed[RpIndex] == 0)) && ((RpDisableMask & (BIT0 << RpIndex)) == 0)) {
          //
          // Dump FOMS value - FOMS score gets populated only after HW EQ is done.
          //
          CpuPcieHwEqDumpFoms (RpIndex, CpuPcieSpeed);
        }
      }
      //
      // Read the DEKEL FW version and populate in HOB to be printed in BIOS setup
      //
      if ((CpuPcieHob != NULL) && (!(RpDisableMask & (BIT0 << RpIndex))) && !(HsPhyRpMap & (BIT0 << RpIndex)) && (CpuPcieDekelMap & (BIT0 << RpIndex))) {
        DekelFwVersion = GetDekelFwVersion (RpIndex);
        CpuPcieHob->DekelFwVersionMajor = (UINT32)((DekelFwVersion & B_DEKEL_FW_VERSION_MAJOR_MASK) >> B_DEKEL_FW_VERSION_MAJOR_OFFSET);
        CpuPcieHob->DekelFwVersionMinor = (UINT32)((DekelFwVersion & B_DEKEL_FW_VERSION_MINOR_MASK) >> B_DEKEL_FW_VERSION_MINOR_OFFSET);
        DEBUG ((DEBUG_INFO, "Dekel FW version : %d.%d\n", CpuPcieHob->DekelFwVersionMajor, CpuPcieHob->DekelFwVersionMinor));
      }
    }

    if (mCpuPcieRpConfig->RootPort2[RpIndex].Func0LinkDisable == 1) {
      DisableCpuPcieFunc0 (RpIndex);
    }
  }

  SocSpecificPowerGating ();

  //
  // Lock SRL bits after function remapping - lock it for all root ports
  //
    DEBUG((DEBUG_INFO, "Lock the registers\n"));
    CpuPcieRpSetSecuredRegisterLock (RpDisableMask);
  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; ++RpIndex) {
    RpBase = CpuPcieBase (RpIndex);
    PciSegmentRead32 (RpBase + PCI_VENDOR_ID_OFFSET);
  }

  DEBUG ((DEBUG_INFO, "CPU PcieRpInit() End\n"));
}

/**
  Update CpuPcie Hob in PostMem

  @param[in]  PciePeiPreMemConfig       Instance of PCIE_CONFIG
  @param[in]  CpuPcieRpConfig           Instance of CPU_PCIE_CONFIG

  @retval EFI_SUCCESS
**/
EFI_STATUS
UpdateCpuPcieHobPostMem (

  IN       PCIE_PEI_PREMEM_CONFIG      *PciePeiPreMemConfig,
  IN       CPU_PCIE_CONFIG             *CpuPcieRpConfig
)
{
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  CPU_PCIE_HOB                *CpuPcieHob;

  ///
  /// Locate HOB for SA PEG
  ///
  CpuPcieHob = GetFirstGuidHob(&gCpuPcieHobGuid);
  if (CpuPcieHob != NULL) {
    CpuPcieHob->InitPcieAspmAfterOprom = (BOOLEAN) (PciePeiPreMemConfig->InitPcieAspmAfterOprom);
    ///
    /// Init CpuPcie field in CpuPcieHob
    ///
    CopyMem (
      CpuPcieHob->RootPort,
      CpuPcieRpConfig->RootPort,
      sizeof (CpuPcieHob->RootPort)
      );
    DEBUG ((DEBUG_INFO, "PostMem CPU PCIE HOB updated\n"));
  }
#endif

  return EFI_SUCCESS;
}

/**
  This function checks if end point supports L1 SubStates.

  @param[in] RpIndex  Root Port Number (0-based)
  @retval    TRUE     If end point supports L1SS
             FALSE    If end point does not supports L1SS
**/
BOOLEAN
IsEndPointSupportsL1ss (
  IN  UINT32 RpIndex
  )
{
  UINTN    RpDevice;
  UINTN    RpFunction;
  UINT64   RpBase;
  UINT64   EpBase;
  UINT8    TempPciBusMin;
  UINT16   PcieCapOffset;
  UINT32   CapsRegister;
  BOOLEAN  IsL1ss;

  IsL1ss         = FALSE;
  GetCpuPcieRpDevFun (RpIndex, &RpDevice, &RpFunction);
  RpBase         = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, RpDevice, RpFunction, 0);
  TempPciBusMin  = PcdGet8 (PcdSiliconInitTempPciBusMin);
  CpuPcieAssignTemporaryBus (RpBase, TempPciBusMin);
  EpBase         = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, TempPciBusMin, 0, 0, 0);

  if (IsDevicePresent (EpBase)) {
    PcieCapOffset = PcieBaseFindExtendedCapId (EpBase, V_PCIE_EX_L1S_CID);
    if (PcieCapOffset) {
      CapsRegister  = PciSegmentRead32 (EpBase + PcieCapOffset + R_PCIE_EX_L1SCAP_OFFSET);
      IsL1ss        = !!(CapsRegister & B_PCIE_EX_L1SCAP_L1PSS);
    }
  }
  CpuPcieClearBus (RpBase);
  return IsL1ss;
}
