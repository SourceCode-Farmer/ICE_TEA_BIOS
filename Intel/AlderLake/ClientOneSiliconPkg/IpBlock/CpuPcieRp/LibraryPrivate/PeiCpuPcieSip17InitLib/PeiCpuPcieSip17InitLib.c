
/** @file
  This file contains functions that for PCIe SIP17.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <IndustryStandard/Pci30.h>
#include <Library/PciSegmentLib.h>
#include <Library/PeiPchPcieClocksLib.h>
#include <PcieRegs.h>
#include <Register/CpuPcieRegs.h>
#include <Library/PciLib.h>
#include <Library/CpuPcieRpLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/PeiCpuPcieSip17InitLib.h>
#include <CpuSbInfo.h>
#include <Register/PchFiaRegs.h>
#include <Library/PreSiliconEnvDetectLib.h>

GLOBAL_REMOVE_IF_UNREFERENCED PCIE_EQ_PARAM               mCpuPcieSip17Gen5EqParamList[PCIE_HWEQ_COEFFS_MAX][2];

/**
  The constructor function

  @param  FileHandle   The handle of FFS header the loaded driver.
  @param  PeiServices  The pointer to the PEI services.

  @retval EFI_SUCCESS           The constructor always returns EFI_SUCCESS.
**/
EFI_STATUS
EFIAPI
CpuPcieSip17InitLibConstructor (
  IN EFI_PEI_FILE_HANDLE        FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  UINT8                      RpIndex;
  UINT8                      MaxCpuPciePortNum;
  UINTN                      RpDev;
  UINTN                      RpFun;
  UINT8                      RpCount;
  UINT64                     RpBase;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17InitLibConstructor\n"));

  MaxCpuPciePortNum = GetMaxCpuPciePortNum ();
  RpCount = 0;

  for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; ++RpIndex) {
    if (GetCpuPcieSipInfo(RpIndex) == PCIE_SIP17) {
      if(IsSimicsEnvironment()) {
        //
        // Configure RCRB MMIO space for each CPU PCIe SIP17 RP.
        //
        RpBase = CpuPcieBase(RpIndex);
        PciSegmentAndThenOr32 (RpBase + PCI_BASE_ADDRESSREG_OFFSET, 0, ((PcdGet32 (PcdCpuPcieRcrbTempBase) + (RpCount * SIZE_8KB)) | BIT2));
        PciSegmentAndThenOr32 (RpBase + PCI_BASE_ADDRESSREG_OFFSET + 4, 0, 0);
        //
        // Program Offset 0x10 R_PCIE_MMT - Set Bits 02:01 to Zero (If BAR is enabled this filed should be 10b)
        //
        PciSegmentAnd32 (RpBase + R_PCIE_BAR0, (UINT32)~(B_PCIE_BAR0_MMT));
        GetCpuPcieRpDevFun (RpIndex, &RpDev, &RpFun);
        DEBUG ((DEBUG_INFO, "Program RCRB BAR to 0:%x:%x with BAR address 0x%x\n", RpDev, RpFun, PcdGet32 (PcdCpuPcieRcrbTempBase) + (RpCount * SIZE_8KB)));
        DEBUG ((DEBUG_INFO, "Read RCRB BAR of 0:%x:%x, the BAR address is 0x%x\n", RpDev, RpFun, PciSegmentRead32 (RpBase + PCI_BASE_ADDRESSREG_OFFSET)));
        RpCount ++;
      } else {
        //
        // Configure RCRB MMIO space for each CPU PCIe SIP17 RP.
        //
        CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_BAR0, (UINT32)~(B_PCIE_BAR0_MMT));
      }
    }
  }

  return EFI_SUCCESS;
}

/**
  Get RCRB BAR address

  @param[in] RpBase      Root Port pci segment base address

  @retval                     RCRB BAR address of this PCIe RP
**/
UINTN
CpuPcieGetRcrbBar (
  UINT64 RpBase
  )
{
  return (PciSegmentRead32 (RpBase + PCI_BASE_ADDRESSREG_OFFSET) & ~(SIZE_8KB - 1));
}

/**
  Check RCRB BAR is avaliable or not.

  @param[in] RpBase      Root Port pci segment base address

  @retval                TRUE - RCRB BAR is available, FALSE - RCRB BAR is not available.
**/
BOOLEAN
IsRcrbBarAvailable (
  UINT64 RpBase
  )
{
  if (CpuPcieGetRcrbBar (RpBase) == 0xFFFFFFFF) {
    DEBUG ((DEBUG_INFO, "RCRB BAR is not available.\n"));
    return FALSE;
  }
  return TRUE;
}

/**
  This function creates SIP17 Capability and Extended Capability List

  @param[in] RpIndex         Root Port index
  @param[in] RpBase          Root Port pci segment base address
  @param[in] CpuPcieRpConfig Pointer to a CPU_PCIE_CONFIG that provides the platform setting

**/
VOID
CpuPcieSip17InitCapabilityList (
  IN UINT32                           RpIndex,
  IN UINT64                           RpBase,
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG  *CpuPcieRpConfig
  )
{
  UINT32      Data32;
  UINT16      Data16;
  UINT8       Data8;
  UINT16      NextCap;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17InitCapabilityList start\n"));
  ///
  /// Build Capability linked list
  /// 1.  Read and write back to capability registers 34h, 41h, 81h and 91h using byte access.
  /// 2.  Program NSR, A4h[3] = 0b
  /// 3.  Program next holding offset of MID to 0x98
  ///
  Data8 = PciSegmentRead8 (RpBase + PCI_CAPBILITY_POINTER_OFFSET);
  PciSegmentWrite8 (RpBase + PCI_CAPBILITY_POINTER_OFFSET, Data8);

  Data16 = PciSegmentRead16 (RpBase + R_PCIE_CLIST);
  PciSegmentWrite16 (RpBase + R_PCIE_CLIST, Data16);

  NextCap = V_PCIE_MID_NEXT_CAP;
  Data32  = PciSegmentRead32 (RpBase + R_PCIE_MID);
  Data32 |= (NextCap << B_PCIE_MID_NEXT_OFFSET);
  PciSegmentWrite32 (RpBase + R_PCIE_MID, Data32);

  Data16 = PciSegmentRead16 (RpBase + R_PCIE_SIP17_SVCAP);
  PciSegmentWrite16 (RpBase + R_PCIE_SIP17_SVCAP, Data16);

  Data32 = PciSegmentRead32 (RpBase + R_PCIE_PMCS);
  Data32 &= (UINT32) ~(B_PCIE_PMCS_NSR);
  PciSegmentWrite32 (RpBase + R_PCIE_PMCS, Data32);

  ///
  /// II.Build PCI Extended Capability linked list
  /// 0xB0C - APEC    (CAPID:002Bh) Alternate Protocol Extended Capability Header
  /// 0xADC - G5ECH   (CAPID:002Ah) Physical Layer 32.0 GT/s Extended Capability Header
  /// 0xEDC - PL16MECH(CAPID:0027h) Physical Layer 16.0 GT/s Margining Extended Capability Header
  /// 0xA9C - PL16GECH(CAPID:0026h) Physical Layer 16.0 GT/s Extended Capability Header
  /// 0xA90 - DLFECH  (CAPID:0025h) Data Link Feature Extended Capability Header
  /// 0xA30 - SPEECH  (CAPID:0019h) Secondary PCI Express Extended Capability Header
  /// 0xA00 - DPCECH  (CAPID:001Dh)
  /// 0x280 - VCECH   (CAPID:0002h) VC capability
  /// 0x150 - PTMECH  (CAPID:001Fh) PTM Extended Capability Register
  /// 0x200 - L1SSECH (CAPID:001Eh) L1SS Capability register
  /// 0x220 - ACSECH  (CAPID:000Dh) ACS Capability register
  /// 0x100 - AECH    (CAPID:0001h) Advanced Error Reporting Capability
  ///
  /*
  a. NEXT_CAP = 0h
  */
  NextCap     = V_PCIE_EXCAP_NCO_LISTEND;

  /*
  b. Program [0xB0C] Alternate Protocol Extended Capability Header(APEC)
    1. Set Next Capability Offset, 0xB0C[31:20] = NEXT_CAP
    2. Set Capability Version, 0xB0C[19:16] = 1h
    3. Set Capability ID, 0xB0C[15:0] = 002Bh
    4. NEXT_CAP = 0xB0C
  */
  Data32 = (V_PCIE_APEC_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_APEC_CID;
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  NextCap = R_PCIE_APEC;
  PciSegmentWrite32 (RpBase + R_PCIE_APEC, Data32);
  DEBUG ((DEBUG_VERBOSE, "Alternate Protocol Extended Capability Header\n"));
  DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_APEC_CID));

  /*
  c. Program [0xADC] Physical Layer 32.0 GT/s Extended Capability Header(G5ECH)
    1. Set Next Capability Offset, 0xADC[31:20] = NEXT_CAP
    2. Set Capability Version, 0xADC[19:16] = 1h
    3. Set Capability ID, 0xADC[15:0] = 002Ah
    4. NEXT_CAP = 0xADC
  */
  Data32 = (V_PCIE_G5ECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_G5ECH_CID;
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  NextCap = R_PCIE_G5ECH;
  PciSegmentWrite32 (RpBase + R_PCIE_G5ECH, Data32);
  DEBUG ((DEBUG_VERBOSE, "Physical Layer 32.0 GT/s Extended Capability Header\n"));
  DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_G5ECH_CID));

  /*
  d. Program [0xEDC] Physical Layer 16.0 GT/s Margining Extended Capability Header(PL16MECH)
    1. Set Next Capability Offset, 0xEDC[31:20] = NEXT_CAP
    2. Set Capability Version, 0xEDC[19:16] = 1h
    3. Set Capability ID, 0xEDC[15:0] = 0027h
    4. NEXT_CAP = 0xEDC
  */
  Data32 = (V_PCIE_PL16MECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PL16MECH_CID;
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  NextCap = R_PCIE_PL16MECH;
  PciSegmentWrite32 (RpBase + R_PCIE_PL16MECH, Data32);
  DEBUG ((DEBUG_VERBOSE, "Physical Layer 16.0 GT/s Margining Extended Capability Header\n"));
  DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PL16MECH_CID));

  if (GetCapabilityVersion () == 2) {
  /*
  Program [0xBE0] Physical Layer 16.0 GT/s IDE Extended Capability Header (IDEEXTCAPHDR)
    1. Set Next Capability Offset, 0xBE0[31:20] = NEXT_CAP
    2. Set Capability Version, 0xBE0[19:16] = 1h
    3. Set Capability ID, 0xBE0[15:0] = 0027h
    4. NEXT_CAP = 0xEDC
  */
  Data32 = (V_PCIE_IDEEXTCAPHDR_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_IDEEXTCAPHDR_CID;
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  NextCap = R_PCIE_IDEEXTCAPHDR;
  PciSegmentWrite32 (RpBase + R_PCIE_IDEEXTCAPHDR, Data32);
  DEBUG ((DEBUG_VERBOSE, "IDE Extended Capability Header\n"));
  DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_IDEEXTCAPHDR_CID));
  }

  /*
  e. Program [0xA9C] Physical Layer 16.0 GT/s Extended Capability Header(PL16GECH)
    1. Set Next Capability Offset, 0xA9C[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA9C[19:16] = 1h
    3. Set Capability ID, 0xA9C[15:0] = 0026h
    4. NEXT_CAP = 0xA9C
  */
  Data32 = (V_PCIE_PL16GECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PL16GECH_CID;
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  NextCap = R_PCIE_PL16GECH;
  PciSegmentWrite32 (RpBase + R_PCIE_PL16GECH, Data32);
  DEBUG ((DEBUG_VERBOSE, "Physical Layer 16.0 GT/s Extended Capability Header\n"));
  DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PL16GECH_CID));

  /*
  f. Program [0xA90] Data Link Feature Extended Capability Header(DLFECH)
    1. Set Next Capability Offset, 0xA90[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA90[19:16] = 1h
    3. Set Capability ID, 0xA90[15:0] = 0025h
    4.  NEXT_CAP = 0xA90
  */
  Data32 = (V_PCIE_DLFECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_DLFECH_CID;
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  NextCap = R_PCIE_DLFECH;
  PciSegmentWrite32 (RpBase + R_PCIE_DLFECH, Data32);
  DEBUG ((DEBUG_VERBOSE, "Data Link Feature Extended Capability Header\n"));
  DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_DLFECH_CID));

  /*
  g. If the RP is GEN3 capable (by fuse and BIOS policy), enable Secondary PCI Express Extended Capability
    1. Set Next Capability Offset,0xA30[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA30[19:16] = 1h
    3. Set Capability ID,  0xA30[15:0] = 0019h
    4. NEXT_CAP = 0xA30
    ELSE, set 0xA30[31:0] = 0
  */
  Data32 = 0;
  if (CpuPcieGetMaxLinkSpeed (RpBase) >= V_PCIE_LCAP_MLS_GEN3) {
    Data32 = (V_PCIE_SPEECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_SPE_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "Secondary PCI Express Extended Capability\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_SPE_CID));
    NextCap = R_PCIE_SPEECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_SPEECH, Data32);

  /*
  h. If Downstream Port Containment is enabled, then
    1. Set Next Capability Offset, 0xA00[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA00[19:16] = 1h
    3. Set Capability ID, 0xA00[15:0] = 001h
    4. NEXT_CAP = 0xA00
    ELSE, set 0xA00 [31:0] = 0
  */
  Data32 = 0;
  if (CpuPcieRpConfig->PcieRpCommonConfig.DpcEnabled == TRUE) {
    Data32 = (V_PCIE_DPCECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_DPC_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "Downstream port containment\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_DPC_CID));
    NextCap = R_PCIE_DPCECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_DPCECH, Data32);

  /*
  i. If VC is enabled, then
    1. Set Next Capability Offset, 0x280[31:20] = NEXT_CAP
    2. Set Capability Version, 0x280[19:16] = 1h
    3. Set Capability ID, 0x280[15:0] = 002h
    4. NEXT_CAP = 0x280
    ELSE, set 0x280 [31:0] = 0
  */
  Data32 = 0;
  if (CpuPcieRpConfig->MultiVcEnabled == TRUE) {
    Data32 = (V_PCIE_VCECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_VC_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "Virtual Channel\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_VC_CID));
    NextCap = R_PCIE_VCECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_VCECH, Data32);

   /*
  j. If support PTM
    1. Set Next Capability Offset, Dxx:Fn +150h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +150h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +150h[15:0] = 001Fh
    4. Read and write back to Dxx:Fn +144h
    5. NEXT_CAP = 150h
    ELSE, set Dxx:Fn +150h [31:0] = 0
    In both cases: read Dxx:Fn + 154h, set BIT1 and BIT2 then write it back
  */
  Data32 = 0;
  if (CpuPcieRpConfig->PcieRpCommonConfig.PtmEnabled == TRUE) {
    Data32 = (V_PCIE_PTMECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PTM_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "PTM\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PTM_CID));
    NextCap = R_PCIE_PTMECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_PTMECH, Data32);
  Data32 = PciSegmentRead32 (RpBase + R_PCIE_PTMCAPR);
  PciSegmentWrite32 (RpBase + R_PCIE_PTMCAPR, (Data32 | B_PCIE_PTMCAPR_PTMRC | B_PCIE_PTMCAPR_PTMRSPC));

  /*
  k. If support L1 Sub-State
    1. Set Next Capability Offset, Dxx:Fn +200h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +200h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +200h[15:0] = 001Eh
    4. Read and write back to Dxx:Fn +204h
    5. Refer to section 8.3 for other requirements (Not implemented here)
    6. NEXT_CAP = 200h
    ELSE, set Dxx:Fn +200h [31:0] = 0, and read and write back to Dxx:Fn +204h
  */
  Data32 = 0;
  if (IsClkReqAssigned (PchClockUsageCpuPcie0 + RpIndex) &&
    (CpuPcieRpConfig->PcieRpCommonConfig.L1Substates != CpuPcieL1SubstatesDisabled)) {
    Data32  = (V_PCIE_L1S_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_L1S_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "L1SS\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_L1S_CID));
    NextCap = R_PCIE_L1SECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_L1SECH, Data32);

  /*
  l. If support ACS
    1. Set Next Capability Offset, Dxx:Fn +220h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +220h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +220h[15:0] = 000Dh
    4. Read and write back to Dxx:Fn +224h
    5. NEXT_CAP = 220h
    ELSE, set Dxx:Fn +220h [31:0] = 0, and read and write back to Dxx:Fn +224h
  */
  Data32 = 0;
  if (CpuPcieRpConfig->PcieRpCommonConfig.AcsEnabled == TRUE) {
    Data32 = (V_PCIE_ACSECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_ACS_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "ACS\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_ACS_CID));
    NextCap = R_PCIE_ACSECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_ACSECH, Data32);

  Data32 = PciSegmentRead32 (RpBase + R_PCIE_ACSCAPR);
  PciSegmentWrite32 (RpBase + R_PCIE_ACSCAPR, Data32);

  /*
  m. If support Advanced Error Reporting
    1. Set Next Capability Offset, Dxx:Fn +100h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +100h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +100h[15:0] = 0001h
    ELSE
    1. Set Next Capability Offset, Dxx:Fn +100h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +100h[19:16]  = 0h
    3. Set Capability ID, Dxx:Fn +100h[15:10]  = 0000h
  */
  Data32 = 0;
  if (CpuPcieRpConfig->PcieRpCommonConfig.AdvancedErrorReporting) {
    Data32 = (V_PCIE_AECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_AEC_CID;
  }
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  PciSegmentWrite32 (RpBase + R_PCIE_AECH, Data32);

  DEBUG ((DEBUG_INFO, "CpuPcieSip17InitCapabilityList end\n"));

  //
  // Mask Unexpected Completion uncorrectable error
  //
  PciSegmentOr32 (RpBase + R_PCIE_UEM, B_PCIE_UEM_UC);
}

/**
  CPU PCIe SIP17 PTM initialization for 2px16

  @param[in]  RpIndex         Root Port Index
**/
VOID
EFIAPI
CpuPcieSip17Ptm2px16Init (
  IN UINT32      RpIndex
  )
{
  UINT64                         RpBase;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17Ptm2px16Init for RpIndex %x\n", RpIndex));
  //
  // PTM programming happens per controller
  // BWG recommended values for SIP17 ADL-S controller are programmed here
  //
  RpBase = CpuPcieBase (RpIndex);

  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC1, 0x0, 0x691A891C);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC2, 0x0, 0x5E0A5C0E);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC3, 0x0, 0x360C410C);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC4, 0x0, 0x30173C18);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC5, (UINT32)0xFFFF0000, 0x00002E18);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC6, 0x0, 0x4E0D470F);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC7, 0x0, 0x340E300D);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC8, 0x0, 0x26182319);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC9, 0x0, 0x0A090A09);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC10, 0x0, 0x0A090A09);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC11, (UINT32)0xFFFF0000, 0x00000A09);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC12, 0x0, 0x07070707);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC13, 0x0, 0x07070707);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC14, (UINT32)0xFFFF0000, 0x00000707);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_PTMECFG, (UINT32)0xFFE00000, 0x00040452);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_PTMCAPR, (UINT32)~(B_PCIE_PTMCAPR_PTMRC | B_PCIE_PTMCAPR_PTMRSPC | B_PCIE_PTMCAPR_PTMREQC), 0x00000007);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_PTMCTLR, (UINT32)~(B_PCIE_PTMCTLR_RS | B_PCIE_PTMCTLR_PTME), 0x00000003);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_ACRG3, (UINT32)~(B_PCIE_ACRG3_PPTMTMUCF_MASK | B_PCIE_ACRG3_PPTMTMUCFE), 0x00000000);
}

/**
  CPU PCIe SIP17 PTM initialization for 1pX8

  @param[in]  RpIndex         Root Port Index
**/
VOID
EFIAPI
CpuPcieSip17Ptm1px8Init (
  IN UINT32      RpIndex
  )
{
  UINT64                         RpBase;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17Ptm1px8Init for RpIndex %x\n", RpIndex));
  //
  // PTM programming happens per controller
  // BWG recommended values for SIP17 ADL-S controller are programmed here
  //
  RpBase = CpuPcieBase (RpIndex);

  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC1, 0x0, 0x691A891C);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC2, 0x0, 0x5E0A5C0E);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC3, 0x0, 0x360C410C);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC4, 0x0, 0x30173C18);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC5, (UINT32)0xFFFF0000, 0x00002E18);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC6, (UINT32)0xFFFF0000, 0x0000470F);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC7, (UINT32)0xFFFF0000, 0X0000300D);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC8, (UINT32)0xFFFF0000, 0x00002319);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC9, 0x0, 0x0A090A09);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC10, 0x0, 0x0A090A09);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC12, 0x0, 0x07070707);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PTMPSDC13, 0x0, 0x07070707);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_PTMECFG, (UINT32)0xFFE00000, 0x00040452);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_PTMCAPR, (UINT32)~(B_PCIE_PTMCAPR_PTMRC | B_PCIE_PTMCAPR_PTMRSPC | B_PCIE_PTMCAPR_PTMREQC), 0x00000007);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_PTMCTLR, (UINT32)~(B_PCIE_PTMCTLR_RS | B_PCIE_PTMCTLR_PTME), 0x00000003);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_ACRG3, (UINT32)~(B_PCIE_ACRG3_PPTMTMUCF_MASK | B_PCIE_ACRG3_PPTMTMUCFE), 0x00000000);
}

/**
  CPU PCIe SIP17 Gen3 PresetToCoeff mapping

  @param[in] RpIndex              Root Port Index for which HW EQ is to be performed
**/
VOID
CpuPcieSip17Gen3PresetToCoeff (
  UINT8      RpIndex
  )
{
  UINT64 RpBase;
  UINT8  ControllerIndex;
  //
  // Before training to GEN3 Configure Lane0 P0-P10 Preset Coefficient mapping based on recommendation for CPU PCIE
  //
  DEBUG ((DEBUG_VERBOSE, "CpuPcieSip17Gen3PresetToCoeff!\n"));
  ControllerIndex = GetCpuPcieControllerNum (RpIndex);
  RpBase = CpuPcieBase (ControllerIndex);
  PciSegmentWrite32 (RpBase + R_PCIE_L0P0P1PCM, 0x00A0C024);
  PciSegmentWrite32 (RpBase + R_PCIE_L0P1P2P3PCM, 0x2A280988);
  PciSegmentWrite32 (RpBase + R_PCIE_L0P3P4PCM, 0x00030180);
  PciSegmentWrite32 (RpBase + R_PCIE_L0P5P6PCM, 0x06A8012C);
  PciSegmentWrite32 (RpBase + R_PCIE_L0P6P7P8PCM, 0x24284880);
  PciSegmentWrite32 (RpBase + R_PCIE_L0P8P9PCM, 0x00228186);
  PciSegmentWrite32 (RpBase + R_PCIE_L0P10PCM, 0x00010020);
}

/**
  CPU PCIe SIP17 Gen4 PresetToCoeff mapping

  @param[in] RpBase      Root Port pci segment base address
**/
VOID
CpuPcieSip17Gen4PresetToCoeff (
  UINT8      RpIndex
  )
{
  UINT64 RpBase;
  UINT8  ControllerIndex;
  //
  // Before training to GEN4 Configure Lane0 P0-P10 Preset Coefficient mapping based on recommendation for CPU PCIE
  //
  DEBUG ((DEBUG_VERBOSE, "CpuPcieSip17Gen4PresetToCoeff!\n"));
  ControllerIndex = GetCpuPcieControllerNum (RpIndex);
  RpBase = CpuPcieBase (ControllerIndex);
  PciSegmentWrite32 (RpBase + R_PCIE_PX16GP0P1PCM, 0x00A0C024);
  PciSegmentWrite32 (RpBase + R_PCIE_PX16GP1P2P3PCM, 0x2A280988);
  PciSegmentWrite32 (RpBase + R_PCIE_PX16GP3P4PCM, 0x00030180);
  PciSegmentWrite32 (RpBase + R_PCIE_PX16GP5P6PCM, 0x06A8012C);
  PciSegmentWrite32 (RpBase + R_PCIE_PX16GP6P7P8PCM, 0x24284880);
  PciSegmentWrite32 (RpBase + R_PCIE_PX16GP8P9PCM, 0x00228186);
  PciSegmentWrite32 (RpBase + R_PCIE_PX16GP10PCM, 0x00010020);
}

/**
  CPU PCIe SIP17 Gen5 PresetToCoeff mapping

  @param[in] RpIndex     Root Port Index
**/
VOID
CpuPcieSip17Gen5PresetToCoeff (
  IN UINT32      RpIndex
  )
{
  UINT8  ControllerIndex;
  DEBUG ((DEBUG_VERBOSE, "CpuPcieSip17Gen5PresetToCoeff!\n"));
  ControllerIndex = GetCpuPcieControllerNum (RpIndex);
  //
  // Before training to GEN5 Configure Lane0 P0-P10 Preset Coefficient mapping based on recommendation for CPU PCIE
  //
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (ControllerIndex), R_PCIE_RCRB_CFG_PX32GP0P1PCM, 0x00A0C024);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (ControllerIndex), R_PCIE_RCRB_CFG_PX32GP1P2P3PCM, 0x2A280988);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (ControllerIndex), R_PCIE_RCRB_CFG_PX32GP3P4PCM, 0x00030180);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (ControllerIndex), R_PCIE_RCRB_CFG_PX32GP5P6PCM, 0x06A8012C);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (ControllerIndex), R_PCIE_RCRB_CFG_PX32GP6P7P8PCM, 0x24284880);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (ControllerIndex), R_PCIE_RCRB_CFG_PX32GP8P9PCM, 0x00228186);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (ControllerIndex), R_PCIE_RCRB_CFG_PX32GP10PCM, 0x00010020);
}

/**
  Set CPU PCIe SIP17 Gen5 Preset entry.

  @param[in] RpIndex     Root Port Index
  @param[in] ListEntry   ListEntry (0-9)
  @param[in] Cm          C-1
  @param[in] Cp          C+1
**/
VOID
CpuPcieSip17SetGen5Presets (
  UINT32 RpIndex,
  UINT32 ListEntry,
  UINT32 Cm,
  UINT32 Cp
  )
{
  UINT32  PreReg;
  UINT32  PostReg;
  UINT32  PreField;
  UINT32  PostField;
  UINT32  Data32And;
  UINT32  Data32Or;

  DEBUG ((DEBUG_VERBOSE, "CpuPcieSip17SetGen5Presets Start!\n"));

  ASSERT (ListEntry < 10);
  ASSERT ((Cm & ~0x3F) == 0);
  ASSERT ((Cp & ~0x3F) == 0);
  ///
  /// CPU PCIe has 5 pairs of coefficients Cm (Co-efficient minus) and Cp (Co-efficient Plus) which is stored in RTPCL registers.
  /// Each of this is stored in 5 bitfields in RTPCL and is covered in 2 registers.
  ///
  PreReg    = ((ListEntry * 2)) / 5;
  PreField  = ((ListEntry * 2)) % 5;
  PostReg   = ((ListEntry * 2) + 1) / 5;
  PostField = ((ListEntry * 2) + 1) % 5;

  ASSERT (PreReg  < 2);
  ASSERT (PostReg < 2);

  Data32And = (UINT32) ~(0x3F << (6 * PreField));
  Data32Or  = (Cm << (6 * PreField));
  ASSERT ((Data32And & Data32Or) == 0);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GRTPCL1 + (PreReg  * 4), Data32And, Data32Or);

  Data32And = (UINT32) ~(0x3F << (6 * PostField));
  Data32Or  = (Cp << (6 * PostField));
  ASSERT ((Data32And & Data32Or) == 0);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GRTPCL1 + (PostReg * 4), Data32And, Data32Or);

  DEBUG ((DEBUG_INFO, "Port %d list %d: (%d,%d)\n",
          (RpIndex), ListEntry, Cm, Cp));

  DEBUG ((DEBUG_VERBOSE, "CpuPcieSip17SetGen5Presets End!\n"));
}

/**
  Populate CPU PCIe Gen5 HW EQ coefficient search list.
  @param[in] RpIndex   Root Port Index
  @param[in] Params    Equalization parameters
**/
VOID
CpuPcieGen5InitializePresets (
  UINT32                 RpIndex,
  CONST CPU_PCIE_CONFIG  *CpuPcieRpConfig
  )
{
  UINT32     Index;
  UINT32     Register;
  UINT8      MaxCpuPciePortNum;
  UINT8      Gen5Cm[PCIE_HWEQ_COEFFS_MAX] = { 0, 8, 0, 0, 0 };
  UINT8      Gen5Cp[PCIE_HWEQ_COEFFS_MAX] = { 7, 9, 0, 0, 0 };

  DEBUG ((DEBUG_INFO, "CpuPcieGen5InitializePresets Start!\n"));

  MaxCpuPciePortNum = GetMaxCpuPciePortNum ();
  Register = R_PCIE_RCRB_CFG_PX32GRTPCL1;
  for (Index = 0; Index < PCIE_HWEQ_COEFFS_MAX; ++Index) {
    Gen5Cm[Index] = CpuPcieRpConfig->PcieCommonConfig2.HwEqGen5CoeffList[RpIndex][Index].Cm;
    Gen5Cp[Index] = CpuPcieRpConfig->PcieCommonConfig2.HwEqGen5CoeffList[RpIndex][Index].Cp;

    CpuPcieSip17SetGen5Presets (
      RpIndex,
      Index,
      Gen5Cm[Index],
      Gen5Cp[Index]
      );
  }

  for (Index = 0; Index < MaxCpuPciePortNum; ++Index) {
    DEBUG ((DEBUG_INFO, "PX32GRTPCL%d = 0x%08x\n", Index, CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), Register + (Index * 4))));
  }
  DEBUG ((DEBUG_INFO, "CpuPcieGen5InitializePresets End!\n"));
}

/**
  Program CPU PCIe SIP17 HW EQ Local Tx Override for Gen5

  @param[in] RpIndex         Root Port Index
  @param[in] LaneIndex       Max Lane for each RootPort
  @param[in] PreCursor       Pre-Cursor Coefficient Override
  @param[in] PostCursor      Post-Cursor Coefficient Override
**/
VOID
CpuPcieSip17SetGen5TxOverride(
  UINT32                  RpIndex,
  UINT32                  LaneIndex,
  UINT32                  PreCursor,
  UINT32                  PostCursor
  )
{
  UINT32  PreCursorReg;
  UINT32  PreCursorField;
  UINT32  PostCursorReg;
  UINT32  PostCursorField;
  UINT32  Data32Or;
  UINT32  Data32And;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17SetGen5TxOverride Start\n"));


  //
  // Each of the local transmitter override registers
  // holds configuration fields for 2 PCIe lanes.
  // The layout of the register is as follows
  // lower lane pre-cursor value
  // lower lane post-cursor value
  // upper lane pre-cursor value
  // upper lane post-cursor value
  // lower lane local transmitter override enable
  // upper lane local transmitter override enable
  // Cursor values fields are 6 bits wide.
  //
  PreCursorReg = (LaneIndex * 2) / 4;
  PreCursorField = (LaneIndex * 2) % 4;
  PostCursorReg = ((LaneIndex * 2) + 1) / 4;
  PostCursorField = ((LaneIndex * 2) + 1) % 4;

  //
  // Program the PX32GLTCOXX register in 2 writes since according to PCIe BWG
  // the pre and post cursor values must be valid at the time of setting the
  // override enable bits.
  //
  DEBUG ((DEBUG_INFO, "Gen5 LaneIndex = %d PreCursor = %d\n", LaneIndex, PreCursor));
  Data32And = (UINT32) ~(0x3F << (6 * PreCursorField));
  Data32Or  = (PreCursor << (6 * PreCursorField));
  ASSERT ((Data32And & Data32Or) == 0);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GLTCO1 + (PreCursorReg * 4), Data32And, Data32Or);

  DEBUG ((DEBUG_INFO, "Gen5 LaneIndex = %d PostCursor = %d\n",LaneIndex, PostCursor));
  Data32And = (UINT32) ~(0x3F << (6 * PostCursorField));
  Data32Or  = (PostCursor << (6 * PostCursorField));
  ASSERT ((Data32And & Data32Or) == 0);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GLTCO1 + (PostCursorReg * 4), Data32And, Data32Or);

  DEBUG ((DEBUG_INFO, "PX32GLTCO1: %x\n", CpuRegbarRead32(GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GLTCO1)));
  DEBUG ((DEBUG_INFO, "CpuPcieSip17SetGen5TxOverride End\n"));
}

/**
  Dump CPU PCIe SIP17 Gen5 HW EQ Registers.
  @param[in] RpIndex         Root Port Index
**/
VOID
CpuPcieSip17Gen5DumpHwEqRegs (
  UINT32 RpIndex
  )
{
  DEBUG ((DEBUG_VERBOSE, "PX32EQCFG1: 0x%08x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1)));
  DEBUG ((DEBUG_VERBOSE, "PX32EQCFG2: 0x%08x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG2)));
  DEBUG ((DEBUG_VERBOSE, "L01EC:  0x%08x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GP0P1PCM)));
  DEBUG ((DEBUG_VERBOSE, "L23EC:  0x%08x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GP1P2P3PCM)));
}

/**
  Configures CPU PCIe SIP17 rootport for hardware Gen5 link equalization.
  @param[in] RpIndex              Root Port Index for which HW EQ is to be performed
  @param[in] RpBase               Root Port Base
  @param[in] Params               Equalization parameters
**/
VOID
CpuPcieSip17DoGen5HardwareEq (
  UINT32                      RpIndex,
  UINT64                      RpBase,
  UINT32                      *DoGen5HwEqRpMask,
  CONST CPU_PCIE_CONFIG       *Params
  )
{
  UINT32                            Data32Or;
  UINT32                            Data32And;
  UINT32                            Gen5Uptp;
  UINT32                            Gen5Dptp;
  UINT8                             Gen5PcetTimer;
  UINT8                             Gen5TsLockTimer;
  UINT8                             Gen5NumberOfPresetCoeffList;
  UINT8                             Gen5PresetCoeffSelection;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17DoGen5HardwareEq Start!\n"));

  *DoGen5HwEqRpMask |= BIT0 << RpIndex;
  DEBUG ((DEBUG_INFO, "LSTS2: 0x%04x\n", PciSegmentRead16 (RpBase + R_PCIE_LSTS2)));

  //
  // Programming done as per section 5.3.1.4 Linear Mode Hardware Flow ( Gen5 )
  // SIP17 Converge PCIe BWG r0.9
  //

  if (IsIrrcProgrammingRequired (RpIndex)) {
    CpuRegbarOr8 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GEQTS2IRRC);
  }

  //
  // Step1: Program all the preset-coeff mapping registers - done in CpuPcieTxPresetOverride() fn
  //
  if (RpIndex < 3){ // Preset Overrides are per controller registers, so skip it for the last port
    CpuPcieSip17Gen5PresetToCoeff (RpIndex);
  }
  CpuRegbarWrite32(GetCpuPcieRpSbiPid(RpIndex), R_PCIE_RCRB_CFG_PX32GLFFS, 0x00003010);

  //
  // Step2:
  // Program LCTL2.TLS to Gen5 for Compliace Mode
  //
  if(Params->PcieCommonConfig.ComplianceTestMode) {
    PciSegmentAndThenOr32 (RpBase + R_PCIE_LCTL2, (UINT16)~B_PCIE_LCTL2_TLS, V_PCIE_LCAP_MLS_GEN5);
  }

  //
  // Step3:Program PX32GRTPCL1.PCM = 0 to enable Preset mode - Gen5 Preset/Coefficient Mode Selection
  //

  Gen5PresetCoeffSelection = Params->RootPort2[RpIndex].PcieGen5PresetCoeffSelection;
  DEBUG((DEBUG_INFO, "Gen5 Preset/Coefficient Mode Selection %x\n", Gen5PresetCoeffSelection));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GRTPCL1, (UINT32)~B_PCIE_RCRB_CFG_PX32GRTPCL1_PCM, (Gen5PresetCoeffSelection << B_PCIE_RCRB_CFG_PX32GRTPCL1_PCM_OFFSET));


  //
  // Program the Presets
  // Note : Requires silicon characterization
  //
  CpuPcieGen5InitializePresets (RpIndex, Params);
  //
  // Step4:Program PX32EQCFG1.PX32GLEP23B to 0 so that Phase 2 and Phase 3 EQ is not bypass
  //
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~(B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GLEP23B));
  //
  // Step5:Program PX32EQCFG1.PX32GLEP3B to 0 so that Phase 3 EQ is not bypass
  //
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~(B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GLEP3B));
  //
  // Step6:Program PX32EQCFG1.PX32GRTLEPCEB to 0 to enable HWEQ
  //
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GLEPCEB);
  //
  // Step7:Program PX32EQCFG1.PX32GRTPCOE to 0 enable Hardware Search Algorithm
  //
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRTPCOE);
  //
  // Step8,9:Rx wait time for each new eq should be configured to 1us
  //
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE_MASK, ((UINT32)V_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE_1US << B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE_OFFSET));  //
  // Step10: For Linear Mode, BIOS needs to program the PX32EQCFG1.PX32GMFLNTL to ?
  // @ TODO : Check what value should be programmed
  //
  //CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GMFLNTL);
  //
  // Step11: Program PX32EQCFG1.PX32GTSWLPCE to 2 // Program it to 2 (1.5us) as per post silicon recommendation - Gen5 TS Lock Timer
  //
  Gen5TsLockTimer = 2;
  DEBUG((DEBUG_INFO, "Gen5 TS Lock Timer = %d \n", Gen5TsLockTimer));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GTSWLPCE_MASK, (Gen5TsLockTimer << B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GTSWLPCE_OFFSET));
  //
  // Step12:Rx wait time for each new eq should be configured to 1us
  //
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE_MASK, ((UINT32)V_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE_1US << B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE_OFFSET));
  //
  // Step13: Program the PX32EQCFG1.PX32GHAPCCPI 2 to allow 3 iteration of Recovery.Equalization entry
  //
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAPCCPI_MASK, 0 << B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAPCCPI_OFFSET);
  //
  // Step14: Program PX32EQCFG2.HAPCSB as below and clear PX32EQCFG1.PX32GHAPCCPIE
  //
  Gen5NumberOfPresetCoeffList = 0;
  DEBUG((DEBUG_INFO, "Gen5 Number of Preset/Coefficient = %d\n", Gen5NumberOfPresetCoeffList));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG2, (UINT32)~B_PCIE_RCRB_CFG_PX32EQCFG2_HAPCSB_MASK, Gen5NumberOfPresetCoeffList << B_PCIE_RCRB_CFG_PX32EQCFG2_HAPCSB_OFFSET);
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~(B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAPCCPIE));
  //
  // Step15,16: Program PX32EQCFG1.PX32GMEQSMMFLNTL and PX32EQCFG1.PX32GMFLNTL to 0
  //
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~(B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GMEQSMMFLNTL | B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GMFLNTL));
  //
  // Step17: Program PX32EQCFG1.REIC - 0
  //
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1, (UINT32)~(B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GREIC));
  //
  // Step18,19: Program PX32EQCFG2.NTIC,PX32EQCFG2.EMD  to 0
  //
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG2, (UINT32)~(B_PCIE_RCRB_CFG_PX32EQCFG2_NTIC | B_PCIE_RCRB_CFG_PX32EQCFG2_EMD));

  //
  // Step20: To support 1 coefficient PECT needs to program to 11h to allow 10ms - PCET Timer for Gen5
  //
  Gen5PcetTimer = 11;
  DEBUG((DEBUG_INFO, "Gen5 PCET timeout for = %d\n", Gen5PcetTimer));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG2, (UINT32)~(B_PCIE_RCRB_CFG_PX32EQCFG2_PCET_MASK), (Gen5PcetTimer << B_PCIE_RCRB_CFG_PX32EQCFG2_PCET_OFFSET));
  //
  // Step21,22: Program PX32EQCFG2.NTEME and PX32EQCFG2.MPEME to 0
  //
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG2, (UINT32)~(B_PCIE_RCRB_CFG_PX32EQCFG2_NTEME | B_PCIE_RCRB_CFG_PX32EQCFG2_MPEME));
  //
  // Step23: Program the following register for the Downstream Port Transmitter Preset value to P7
  //
  ///
  /// Configure Transmitter Preset for each Upstream and Downstream Port Lane:
  /// 1.  Set G5LANEEQCTL_0.L0DPTP, Dxx:Fn + AFCh[3:0]     = 7
  /// 2.  Set G5LANEEQCTL_0.L0UPTP, Dxx:Fn + AFCh[7:4]     = 7
  /// 3.  Set G5LANEEQCTL_0.L1DPTP, Dxx:Fn + AFCh[11:8]    = 7
  /// 4.  Set G5LANEEQCTL_0.L1UPTP, Dxx:Fn + AFCh[15:12]   = 7
  /// 5.  Set G5LANEEQCTL_0.L2DPTP, Dxx:Fn + AFCh[19:16]   = 7
  /// 6.  Set G5LANEEQCTL_0.L2UPTP, Dxx:Fn + AFCh[23:20]   = 7
  /// 7.  Set G5LANEEQCTL_0.L3DPTP, Dxx:Fn + AFCh[27:24]   = 7
  /// 8.  Set G5LANEEQCTL_0.L3UPTP, Dxx:Fn + AFCh[31:28]   = 7
  /// 9.  Set G5LANEEQCTL_4.L4DPTP, Dxx:Fn + B00h[3:0]     = 7
  /// 10. Set G5LANEEQCTL_4.L4UPTP, Dxx:Fn + B00h[7:4]     = 7
  /// 11. Set G5LANEEQCTL_4.L5DPTP, Dxx:Fn + B00h[11:8]    = 7
  /// 12. Set G5LANEEQCTL_4.L5UPTP, Dxx:Fn + B00h[15:12]   = 7
  /// 13. Set G5LANEEQCTL_4.L6DPTP, Dxx:Fn + B00h[19:16]   = 7
  /// 14. Set G5LANEEQCTL_4.L6UPTP, Dxx:Fn + B00h[23:20]   = 7
  /// 15. Set G5LANEEQCTL_4.L7DPTP, Dxx:Fn + B00h[27:24]   = 7
  /// 16. Set G5LANEEQCTL_4.L7UPTP, Dxx:Fn + B00h[31:28]   = 7
  /// 17. Set G5LANEEQCTL_8.L8DPTP, Dxx:Fn + B04h[3:0]     = 7
  /// 18. Set G5LANEEQCTL_8.L8UPTP, Dxx:Fn + B04h[7:4]     = 7
  /// 19. Set G5LANEEQCTL_8.L9DPTP, Dxx:Fn + B04h[11:8]    = 7
  /// 20. Set G5LANEEQCTL_8.L9UPTP, Dxx:Fn + B04h[15:12]   = 7
  /// 21. Set G5LANEEQCTL_8.L10DPTP, Dxx:Fn + B04h[19:16]  = 7
  /// 22. Set G5LANEEQCTL_8.L10UPTP, Dxx:Fn + B04h[23:20]  = 7
  /// 23. Set G5LANEEQCTL_8.L11DPTP, Dxx:Fn + B04h[27:24]  = 7
  /// 24. Set G5LANEEQCTL_8.L11UPTP, Dxx:Fn + B04h[31:28]  = 7
  /// 25. Set G5LANEEQCTL_12.L12DPTP, Dxx:Fn + B08h[3:0]   = 7
  /// 26. Set G5LANEEQCTL_12.L12UPTP, Dxx:Fn + B08h[7:4]   = 7
  /// 27. Set G5LANEEQCTL_12.L13DPTP, Dxx:Fn + B08h[11:8]  = 7
  /// 28. Set G5LANEEQCTL_12.L13UPTP, Dxx:Fn + B08h[15:12] = 7
  /// 29. Set G5LANEEQCTL_12.L14DPTP, Dxx:Fn + B08h[19:16] = 7
  /// 30. Set G5LANEEQCTL_12.L14UPTP, Dxx:Fn + B08h[23:20] = 7
  /// 31. Set G5LANEEQCTL_12.L15DPTP, Dxx:Fn + B08h[27:24] = 7
  /// 32. Set G5LANEEQCTL_12.L15UPTP, Dxx:Fn + B08h[31:28] = 7
  ///
  Gen5Uptp = Params->RootPort[RpIndex].Gen5Uptp;
  Gen5Dptp = Params->RootPort[RpIndex].Gen5Dptp;
  DEBUG ((DEBUG_INFO, "Gen5Uptp = %x, Gen5Dptp = %x!\n", Gen5Uptp, Gen5Dptp));
  Data32And = (UINT32) ~(B_PCIE_G5LANEEQCTL_0_L3UPTP_MASK | B_PCIE_G5LANEEQCTL_0_L3DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_0_L2UPTP_MASK | B_PCIE_G5LANEEQCTL_0_L2DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_0_L1UPTP_MASK | B_PCIE_G5LANEEQCTL_0_L1DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_0_L0UPTP_MASK | B_PCIE_G5LANEEQCTL_0_L0DPTP_MASK);
  Data32Or = ((Gen5Uptp << B_PCIE_G5LANEEQCTL_0_L3UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_0_L3DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_0_L2UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_0_L2DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_0_L1UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_0_L1DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_0_L0UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_0_L0DPTP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_G5LANEEQCTL_0, Data32And, Data32Or);

  Data32And = (UINT32) ~(B_PCIE_G5LANEEQCTL_4_L7UPTP_MASK | B_PCIE_G5LANEEQCTL_4_L7DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_4_L6UPTP_MASK | B_PCIE_G5LANEEQCTL_4_L6DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_4_L5UPTP_MASK | B_PCIE_G5LANEEQCTL_4_L5DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_4_L4UPTP_MASK | B_PCIE_G5LANEEQCTL_4_L4DPTP_MASK);
  Data32Or = ((Gen5Uptp << B_PCIE_G5LANEEQCTL_4_L7UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_4_L7DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_4_L6UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_4_L6DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_4_L5UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_4_L5DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_4_L4UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_4_L4DPTP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_G5LANEEQCTL_4, Data32And, Data32Or);

  Data32And = (UINT32) ~(B_PCIE_G5LANEEQCTL_8_L11UPTP_MASK | B_PCIE_G5LANEEQCTL_8_L11DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_8_L10UPTP_MASK | B_PCIE_G5LANEEQCTL_8_L10DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_8_L9UPTP_MASK  | B_PCIE_G5LANEEQCTL_8_L9DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_8_L8UPTP_MASK  | B_PCIE_G5LANEEQCTL_8_L8DPTP_MASK);
  Data32Or = ((Gen5Uptp << B_PCIE_G5LANEEQCTL_8_L11UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_8_L11DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_8_L10UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_8_L10DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_8_L9UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_8_L9DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_8_L8UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_8_L8DPTP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_G5LANEEQCTL_8, Data32And, Data32Or);

  Data32And = (UINT32) ~(B_PCIE_G5LANEEQCTL_12_L15UPTP_MASK | B_PCIE_G5LANEEQCTL_12_L15DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_12_L14UPTP_MASK | B_PCIE_G5LANEEQCTL_12_L14DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_12_L13UPTP_MASK | B_PCIE_G5LANEEQCTL_12_L13DPTP_MASK |
                         B_PCIE_G5LANEEQCTL_12_L12UPTP_MASK | B_PCIE_G5LANEEQCTL_12_L12DPTP_MASK);
  Data32Or = ((Gen5Uptp << B_PCIE_G5LANEEQCTL_12_L15UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_12_L15DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_12_L14UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_12_L14DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_12_L13UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_12_L13DPTP_OFFSET) |
    (Gen5Uptp << B_PCIE_G5LANEEQCTL_12_L12UPTP_OFFSET) |
    (Gen5Dptp << B_PCIE_G5LANEEQCTL_12_L12DPTP_OFFSET));
  PciSegmentAndThenOr32 (RpBase + R_PCIE_G5LANEEQCTL_12, Data32And, Data32Or);

  DEBUG ((DEBUG_INFO, "CpuPcieSip17DoGen5HardwareEq End!\n"));
}

/**
  Dump CPU PCIe SIP17 HW EQ Coefficients for Gen5.

  @param[in] RpIndex       Root Port Index
  @param[in] ListEntry   ListEntry (0-5)
**/
VOID
CpuPcieSip17DumpHwEqGen5 (
  UINT32 RpIndex,
  UINT32 ListEntry
  )
{
DEBUG_CODE_BEGIN ();
  UINT32  PreReg;
  UINT32  PostReg;
  UINT32  PreField;
  UINT32  PostField;
  UINT32  Data32And;
  UINT32 PresetListEntry;
  UINT32 Cm;
  UINT32 Cp;

  DEBUG ((DEBUG_VERBOSE, "CpuPcieSip17DumpHwEqGen5 Start!\n"));
  ASSERT (ListEntry < 6);

  ///
  /// CPU PCIe has 5 pairs of coefficients Cm (Co-efficient minus) and Cp (Co-efficient Plus) which is stored in RTPCL registers.
  /// Each of this is stored in 5 bitfields in RTPCL and is covered in 2 registers.
  ///
  PreReg    = ((ListEntry * 2)) / 5;
  PreField  = ((ListEntry * 2)) % 5;
  PostReg   = ((ListEntry * 2) + 1) / 5;
  PostField = ((ListEntry * 2) + 1) % 5;

  ASSERT (PreReg  < 2);
  ASSERT (PostReg < 2);

  Data32And = (UINT32) (0x3F << (6 * PreField));
  Cm = CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GRTPCL1 + (PreReg * 4));
  Cm &= Data32And;
  Cm = (UINT8) (Cm >> (6 * PreField));

  Data32And = (UINT32) (0x3F << (6 * PostField));
  Cp = CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GRTPCL1 + (PostReg * 4));
  Cp &= Data32And;
  Cp = (UINT8) (Cp >> (6 * PostField));

  mCpuPcieSip17Gen5EqParamList[ListEntry][0].Cm = (UINT8)Cm;
  mCpuPcieSip17Gen5EqParamList[ListEntry][1].Cp = (UINT8)Cp;

  if (CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32GRTPCL1) & (UINT32)(B_PCIE_RCRB_CFG_PX32GRTPCL1_PCM)){
    DEBUG ((DEBUG_INFO, "Gen5 Coeffiecient List : \n"));
    DEBUG ((DEBUG_INFO, "Port %d list %d: (%d,%d)\n",
            (RpIndex), ListEntry, mCpuPcieSip17Gen5EqParamList[ListEntry][0].Cm, mCpuPcieSip17Gen5EqParamList[ListEntry][1].Cp));
  }
  else {
    PresetListEntry = ListEntry*2;
    DEBUG ((DEBUG_INFO, "Gen5 Preset List : \n"));
    DEBUG ((DEBUG_INFO, "Port %d list %d: (%d)\n",
            (RpIndex), PresetListEntry, mCpuPcieSip17Gen5EqParamList[ListEntry][0].Cm));
    DEBUG ((DEBUG_INFO, "Port %d list %d: (%d)\n",
            (RpIndex), PresetListEntry+1, mCpuPcieSip17Gen5EqParamList[ListEntry][1].Cp));
  }
  DEBUG ((DEBUG_VERBOSE, "CpuPcieSip17DumpHwEqGen5 End!\n"));
DEBUG_CODE_END ();
}

/**
  Configure Number of Fast Training Sequence ordered sets for common and Unique Clock

  @param[in] RpIndex       Root Port Index
**/
VOID
CpuPcieSip17ConfigureNfts (
  UINT32 RpIndex
  )
{
  UINT64    RpBase;
  UINT32    Data32Or;
  UINT32    Data32And;
  UINT32    Data32;
  RpBase = CpuPcieBase (RpIndex);

  ///
  /// Program (R_PCIE_PCIENFTS) 0x314
  /// Gen1 Unique Clock N_FTS (G1UCNFTS) 0x314[15:8] 0x98
  /// Gen1 Common Clock N_FTS (G1CCNFTS) 0x314[7:0] 0xA0
  /// Gen2 Unique Clock N_FTS (G2UCNFTS) 0x314[31:24] 0xCC
  /// Gen2 Common Clock N_FTS (G2CCNFTS) 0x314[23:16] 0xDC
  ///
  Data32 = (UINT32) ((0xCC << B_PCIE_PCIENFTS_G2UCNFTS_OFFSET) | (0xDC << B_PCIE_PCIENFTS_G2CCNFTS_OFFSET)\
            | (0x98 << B_PCIE_PCIENFTS_G1UCNFTS_OFFSET) | (0xA0 << B_PCIE_PCIENFTS_G1CCNFTS_OFFSET));
  PciSegmentWrite32 (RpBase + R_PCIE_PCIENFTS, Data32);

  ///
  /// Program (R_PCIE_G3L0SCTL) 0x478
  /// Gen3 Unique Clock N_FTS (G3UCNFTS) 0x478[15:8] 0x4E
  /// Gen3 Common Clock N_FTS (G3CCNFTS) 0x478[7:0] 0x90
  ///
  Data32And = (UINT32) ~(B_PCIE_G3L0SCTL_G3UCNFTS_MASK | B_PCIE_G3L0SCTL_G3CCNFTS_MASK);
  Data32Or = (UINT32) (0x4E << B_PCIE_G3L0SCTL_G3UCNFTS_OFFSET) | (0x90 << B_PCIE_G3L0SCTL_G3CCNFTS_OFFSET);

  PciSegmentAndThenOr32 (RpBase + R_PCIE_G3L0SCTL, Data32And, Data32Or);
  ///
  /// Program (R_PCIE_G4L0SCTL) 0x310
  /// Gen4 Unique Clock N_FTS (G4UCNFTS) 0x310[15:8] 0xE0
  /// Gen4 Common Clock N_FTS (G4CCNFTS) 0x310[7:0] 0xE0
  ///
  Data32And = (UINT32) ~(B_PCIE_G4L0SCTL_G4UCNFTS_MASK | B_PCIE_G4L0SCTL_G4CCNFTS_MASK);
  Data32Or = (UINT32) (0xE0 << B_PCIE_G4L0SCTL_G4UCNFTS_OFFSET) | (0xE0 << B_PCIE_G4L0SCTL_G4CCNFTS_OFFSET);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_G4L0SCTL, Data32And, Data32Or);

  /// Program (R_PCIE_PCIEL0SC) 0x1E00
  /// Gen5 Active State L0s Preparation Latency (G5ASL0SPL) 0x1E00[31:24] 0x50
  /// Gen5 L0s Entry Idle Control (G5L0SIC) 0x1E00[23:22] 0x3
  /// Gen5 Unique Clock N_FTS (G5UCNFTS) 0x1E00[15:8] 0xE0
  /// Gen5 Common Clock N_FTS (G5CCNFTS) 0x1E00[7:0] 0xE0

  Data32And = (UINT32) ~(B_PCIE_RCRB_G5L0SCTL_G5ASL0SPL_MASK | B_PCIE_RCRB_G5L0SCTL_G5UCNFTS_MASK | B_PCIE_RCRB_G5L0SCTL_G5CCNFTS_MASK);
  Data32Or  = (UINT32) ((V_PCIE_RCRB_G5L0SCTL_G5ASL0SPL << B_PCIE_RCRB_G5L0SCTL_G5ASL0SPL_OFFSET) | (V_PCIE_RCRB_G5L0SCTL_G5L0SIC << B_PCIE_RCRB_G5L0SCTL_G5L0SIC_OFFSET) |
                        (0xE0 << B_PCIE_RCRB_G5L0SCTL_G5UCNFTS_OFFSET) | (0xE0 << B_PCIE_RCRB_G5L0SCTL_G5CCNFTS_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_G5L0SCTL, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_RCRB_G5L0SCTL : %x\n",CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_G5L0SCTL)));

  ///
  /// Program R_PCIE_LPHYCP4.DPMDNDBFE [0x1434][12] - 0x1
  ///
  CpuRegbarOr32 (GetCpuPcieRpSbiPid (RpIndex),R_PCIE_LPHYCP4, B_PCIE_LPHYCP4_DPMDNDBFE);
}

/**
  Get CPU PCIe SIP17 PX32EQCFG1 value

  @param[in] RpIndex            Root Port Index

  @retval   PX32EQCFG1 value
**/
UINT32
CpuPcieSip17GetPx32EqCfg1 (
  UINT32                 RpIndex
  )
{
  return (CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PX32EQCFG1));
}

/**
  Get CPU PCIe SIP17 B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAED bit map

  @retval   B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAED bit map
**/
UINT32
CpuPcieSip17GetGen5HaedBitMap (
 VOID
  )
{
  return B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAED;
}

/**
Program Clock gating registers for SIP17

@param[in] RpIndex      Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17DynamicClockGating (
  UINT32 RpIndex
  )
{
  UINT32                    Data32Or;
  UINT32                    Data32And;

  DEBUG((DEBUG_INFO, "CpuPcieSip17DynamicClockGating for RpIndex %x\n", RpIndex));
  //
  // DCG programming happens per controller
  // BWG recommended values for SIP17 ADL-S controller are programmed here
  // Section 4.4.3 Clock Gating
  // To enable the Unit Level Dynamic Clock Gating, BIOS is required to set the enable bits in DCGM1, 2, 3, 4 before setting any bits in DCGEN1, 2, 3, 4
  //

  Data32Or = (B_PCIE_RCRB_DCGM1_PXTTSULDCGM | B_PCIE_RCRB_DCGM1_PXTRSULDCGM | B_PCIE_RCRB_DCGM1_PXTRULDCGM | \
              B_PCIE_RCRB_DCGM1_PXLSULDCGM | B_PCIE_RCRB_DCGM1_PXLIULDCGM | B_PCIE_RCRB_DCGM1_PXLTULDCGM | \
              B_PCIE_RCRB_DCGM1_PXLRULDCGM | B_PCIE_RCRB_DCGM1_PXCULDCGM | B_PCIE_RCRB_DCGM1_PXKGULDCGM);
  Data32And = (UINT32)~(B_PCIE_RCRB_DCGM1_PXTTULDCGM);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGM1, Data32And, Data32Or);
  DEBUG((DEBUG_INFO, "R_PCIE_RCRB_DCGM1 after write = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGM1)));

  Data32Or = (B_PCIE_RCRB_DCGM2_PXSDRULDCGM | B_PCIE_RCRB_DCGM2_PXSDTULDCGM | B_PCIE_RCRB_DCGM2_PXSDIULDCGM | B_PCIE_RCRB_DCGM2_PXFRULDCGM | \
              B_PCIE_RCRB_DCGM2_PXFTULDCGM | B_PCIE_RCRB_DCGM2_PXPBULDCGM | B_PCIE_RCRB_DCGM2_PXPSULDCGM | B_PCIE_RCRB_DCGM2_PXPIULDCGM );
  CpuRegbarOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGM2, Data32Or);
  DEBUG((DEBUG_INFO, "R_PCIE_RCRB_DCGM2 after write = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGM2)));

  Data32And = (UINT32)~(B_PCIE_RCRB_DCGM3_PXTOUPDCGM);
  Data32Or = (B_PCIE_RCRB_DCGM3_PXTTSUPDCGM | B_PCIE_RCRB_DCGM3_PXTTUPDCGM | B_PCIE_RCRB_DCGM3_PXTRSUPDCGM | \
              B_PCIE_RCRB_DCGM3_PXTRUPDCGM | B_PCIE_RCRB_DCGM3_PXLIUPDCGM | B_PCIE_RCRB_DCGM3_PXLTUPDCGM | \
              B_PCIE_RCRB_DCGM3_PXLRUPDCGM | B_PCIE_RCRB_DCGM3_PXBUPDCGM | B_PCIE_RCRB_DCGM3_PXEUPDCGM | \
              B_PCIE_RCRB_DCGM3_PXCUPSRCDCGM | B_PCIE_RCRB_DCGM3_PXCUPSNRDCGM | B_PCIE_RCRB_DCGM3_PXSRUSSNRDCGM);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGM3, Data32And, Data32Or);
  DEBUG((DEBUG_INFO, "R_PCIE_RCRB_DCGM3 after write = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGM3)));

  Data32Or = (B_PCIE_RCRB_DCGEN1_PXTTSULDCGEN | B_PCIE_RCRB_DCGEN1_PXTRSULDCGEN | B_PCIE_RCRB_DCGEN1_PXTRULDCGEN | \
              B_PCIE_RCRB_DCGEN1_PXLSULDCGEN | B_PCIE_RCRB_DCGEN1_PXLIULDCGEN | \
              B_PCIE_RCRB_DCGEN1_PXLRULDCGEN | B_PCIE_RCRB_DCGEN1_PXCULDCGEN | B_PCIE_RCRB_DCGEN1_PXKGULDCGEN);
  Data32And = (UINT32)~(B_PCIE_RCRB_DCGEN1_PXTTULDCGEN);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGEN1, Data32And, Data32Or);
  DEBUG((DEBUG_INFO, "R_PCIE_RCRB_DCGEN1 after write = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGEN1)));

  Data32Or = (B_PCIE_RCRB_DCGEN2_PXSDRULDCGEN | B_PCIE_RCRB_DCGEN2_PXSDTULDCGEN | B_PCIE_RCRB_DCGEN2_PXSDIULDCGEN | \
              B_PCIE_RCRB_DCGEN2_PXFRULDCGEN | B_PCIE_RCRB_DCGEN2_PXFTULDCGEN | \
              B_PCIE_RCRB_DCGEN2_PXPBULDCGEN | B_PCIE_RCRB_DCGEN2_PXPSULDCGEN | B_PCIE_RCRB_DCGEN2_PXPIULDCGEN );
  CpuRegbarOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGEN2, Data32Or);
  DEBUG((DEBUG_INFO, "R_PCIE_RCRB_DCGEN2 after write = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGEN2)));

  Data32And = (UINT32)~(B_PCIE_RCRB_DCGEN3_PXCUPSRCDCGEN | B_PCIE_RCRB_DCGEN3_PXCUPSNRDCGEN | B_PCIE_RCRB_DCGEN3_PXTOUPDCGEN);
  Data32Or = (B_PCIE_RCRB_DCGEN3_PXTTSUPDCGEN | B_PCIE_RCRB_DCGEN3_PXTTUPDCGEN | B_PCIE_RCRB_DCGEN3_PXTRSUPDCGEN |
              B_PCIE_RCRB_DCGEN3_PXTRUPDCGEN | B_PCIE_RCRB_DCGEN3_PXLIUPDCGEN | B_PCIE_RCRB_DCGEN3_PXSRUSSNRDCGEN | \
              B_PCIE_RCRB_DCGEN3_PXLRUPDCGEN | B_PCIE_RCRB_DCGEN3_PXBUPDCGEN | B_PCIE_RCRB_DCGEN3_PXEUPDCGEN);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGEN3, Data32And, Data32Or);
  DEBUG((DEBUG_INFO, "R_PCIE_RCRB_DCGEN3 after write = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_DCGEN3)));

  Data32Or = B_PCIE_RCRB_IPCLKCTR_PXDCGE;
  if (IsMildDcgSupported() == TRUE) {
    Data32Or |= B_PCIE_RCRB_IPCLKCTR_MDPCCEN;
  }
  CpuRegbarOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_IPCLKCTR, Data32Or);
  DEBUG((DEBUG_INFO, "R_PCIE_RCRB_IPCLKCTR after write = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_IPCLKCTR)));
}

/**
  CPU PCIe SIP17 Reset Policy initialization

  @param[in] RpIndex     Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17ResetPolicyInit (
  UINT32 RpIndex
  )
{
  UINT32   Data32Or;
  UINT32   Data32And;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17ResetPolicyInit for RpIndex %x\n", RpIndex));

  //
  // BWG Section 4.36 Reset Prep Setting
  // To remap various Reset Prep Message to different link handling event. BIOS required to configure Reset Type and Prep Type
  // Program RESET POLICY (RPR)
  //
  Data32And = (UINT32) ~(B_PCIE_RCRB_RPR_WRM_MASK | B_PCIE_RCRB_RPR_CRM_MASK | B_PCIE_RCRB_RPR_S3SM_MASK | B_PCIE_RCRB_RPR_S4SM_MASK | \
                         B_PCIE_RCRB_RPR_S5SM_MASK | B_PCIE_RCRB_RPR_DMTO_MASK | B_PCIE_RCRB_RPR_WRMTO_MASK | B_PCIE_RCRB_RPR_CRMTO_MASK | \
                         B_PCIE_RCRB_RPR_S3SMTO_MASK | B_PCIE_RCRB_RPR_S4SMTO_MASK | B_PCIE_RCRB_RPR_S5SMTO_MASK);
  Data32Or  = (UINT32) ((0x01 << B_PCIE_RCRB_RPR_WRM_OFFSET) | (0x01 << B_PCIE_RCRB_RPR_CRM_OFFSET) | (0x01 << B_PCIE_RCRB_RPR_S3SM_OFFSET) | \
                        (0x01 << B_PCIE_RCRB_RPR_S4SM_OFFSET) | (0x01 << B_PCIE_RCRB_RPR_S5SM_OFFSET) | (0x01 << B_PCIE_RCRB_RPR_DMTO_OFFSET) | \
                        (0x01 << B_PCIE_RCRB_RPR_WRMTO_OFFSET) | (0x01 << B_PCIE_RCRB_RPR_CRMTO_OFFSET) | (0x01 << B_PCIE_RCRB_RPR_S3SMTO_OFFSET) | \
                        (0x01 << B_PCIE_RCRB_RPR_S4SMTO_OFFSET) | (0x01 << B_PCIE_RCRB_RPR_S5SMTO_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_RPR, Data32And, Data32Or);

  //
  // Program Reset prep Decode 1 (RPDEC1)
  //
  Data32And = (UINT32) ~(B_PCIE_RCRB_RPDEC1_RPWRERT_MASK | B_PCIE_RCRB_RPDEC1_RPWREPT_MASK | \
                         B_PCIE_RCRB_RPDEC1_RPCRERT_MASK | B_PCIE_RCRB_RPDEC1_RPCREPT_MASK);
  Data32Or  = (UINT32) ((0x10 << B_PCIE_RCRB_RPDEC1_RPWRERT_OFFSET) | (0x11 << B_PCIE_RCRB_RPDEC1_RPCRERT_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_RPDEC1, Data32And, Data32Or);

  //
  // Program Reset Prep Decode 2 (RPDEC2)
  //
  Data32And = (UINT32) ~(B_PCIE_RCRB_RPDEC2_RPS3ERT_MASK | B_PCIE_RCRB_RPDEC2_RPS3EPT_MASK | \
                         B_PCIE_RCRB_RPDEC2_RPS4ERT_MASK | B_PCIE_RCRB_RPDEC2_RPS4EPT_MASK);
  Data32Or  = (UINT32) ((0x03 << B_PCIE_RCRB_RPDEC2_RPS3ERT_OFFSET) | (0x04 << B_PCIE_RCRB_RPDEC2_RPS4ERT_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_RPDEC2, Data32And, Data32Or);

  //
  // Program Reset Prep Decode 3 (RPDEC3)
  //
  Data32And = (UINT32) ~(B_PCIE_RCRB_RPDEC3_RPS5ERT_MASK | B_PCIE_RCRB_RPDEC3_RPS5EPT_MASK | \
                         B_PCIE_RCRB_RPDEC3_RPDH_MASK );
  Data32Or  = (UINT32) ((0x05 << B_PCIE_RCRB_RPDEC3_RPS5ERT_OFFSET) | (0x05 << B_PCIE_RCRB_RPDEC3_RPDH_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_RPDEC3, Data32And, Data32Or);

  //
  // Program Feature Control 2 (FCTL2)
  // Enabled Hot Plug Interrupt Control (HPICTL) as per BWG 1.4 recommedation
  //
  Data32And = (UINT32) ~(B_PCIE_RCRB_FCTL2_RPTOT_MASK | B_PCIE_RCRB_FCTL2_HRTCTL_MASK);
  Data32Or  = (UINT32) (B_PCIE_RCRB_FCTL2_RXCPPREALLOCEN | (0x05 << B_PCIE_RCRB_FCTL2_RPTOT_OFFSET) | (0x01 << B_PCIE_RCRB_FCTL2_HRTCTL_OFFSET) | B_PCIE_RCRB_FCTL_HPICTL);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_FCTL2, Data32And, Data32Or);

  //
  // Program TXCRSTOCTL
  //
  Data32Or  = (UINT32) (B_PCIE_RCRB_TXCRSTOCTL_TXNPCTODIS);
  CpuRegbarOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_TXCRSTOCTL, Data32Or);

}

/**
  CPU PCIe Completion Timer Timeout Policy

  @param[in]  RpIndex         Root Port Index
**/
VOID
EFIAPI
CpuPcieSip17CompletionTimerTimeout (
  IN  UINT32  RpIndex
)
{
  DEBUG ((DEBUG_INFO, "CpuPcieSip17CompletionTimerTimeout for RpIndex %x\n", RpIndex));

  //
  // BWG recommended values for SIP17 ADL-S controller are programmed here
  //
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_FCTL, (UINT32)~B_PCIE_RCRB_FCTL_CRSPSEL);
}

/**
  CPU PCIe SIP17 Error Injection Disable Bit

  @param[in]  RpBase                Root Port pci segment base address.
**/
VOID
CpuPcieSip17SetErrorInjectionDisable (
  IN UINT64 RpBase
)
{
  DEBUG ((DEBUG_VERBOSE, "CpuPcieSip17SetErrorInjectionDisable!\n"));
  ///
  /// EINJ Disable (EINJDIS) 0xCA8 0 1
  ///
  PciSegmentOr32 (RpBase + R_PCIE_EINJCTL, B_PCIE_EINJCTL_EINJDIS);
}

/**
  Program Rx Master Cycle Decode Registers for SIP17

  @param[in] RpIndex     Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17RxMasterCycleDecode (
  UINT32 RpIndex
)
{
  UINT32                    Data32Or;
  UINT32                    Data32And;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17RxMasterCycleDecode for RpIndex %x\n", RpIndex));

  ///
  /// Program (R_PCIE_RCRB_CFG_DECCTL) 0X1904
  /// Program (ATSCE) 0X1904[1]                0x1
  /// Program (BUSNUMZCHK) 0X1904[2]           0x1
  /// Program (MTRXLTC) 0X1904[3]              0x0
  /// Program (OUTBEXPCPLCHKEN) 0X1904[4]      0x1
  /// Program (DROPCPLATNZCE) 0X1904[5]        0x1
  /// Program (LCRXINT) 0X1904[6]              0x1
  /// Program (VDMATAC) 0X1904[7]              0x1
  /// Program (URVDME16DW) 0X1904[8]           0x1
  /// Program (URRXUVDMINTELVID) 0X1904[9]     0x0
  /// Program (URRXUVDMUVID) 0X1904[10]        0x0
  /// Program (URRXURTRCVDM) 0X1904[11]        0x1
  /// Program (URRXULTVDM) 0X1904[12]          0x1
  /// Program (URRXURAVDM) 0X1904[13]          0x1
  /// Program (URRXURIDVDM) 0X1904[14]         0x1
  /// Program (URRXUORVDM) 0X1904[15]          0x1
  /// Program (URRXVMCTPBFRC) 0X1904[16]       0x1
  /// Program (ICHKINVREQRMSGRBID) 0X1904[17]  0x1
  /// Program (RXMCTPDECEN) 0X1904[18]         0x1
  /// Program (RXVDMDECE) 0X1904[19]           0x0
  /// Program (RXGPEDECEN) 0X1904[20]          0x1
  /// Program (RXSBEMCAPDECEN) 0X1904[21]      0x1
  /// Program (RXADLEDDECEN) 0X1904[22]        0x1
  /// Program (RXLTRMDECH) 0X1904[23]          0x0
  /// Program (LCRXERRMSG) 0X1904[24]          0x1
  /// Program (RSVD_RW) 0X1904[25]             0x1
  /// Program (LCRXPTMREQ) 0X1904[26]          0x1
  /// Program (URRXUVDMRBFRC) 0X1904[27]       0x1
  /// Program (URRXUVDMRGRTRC) 0X1904[28]      0x1
  /// Program (RXMCTPBRCDECEN) 0X1904[29]      0x1
  /// Program (URRXMCTPNTCO) 0X1904[30]        0x1
  /// Program (RXIMDECEN) 0X1904[31]           0x0
  ///
  Data32And = (UINT32) ~(B_PCIE_RCRB_CFG_DECCTL_MTRXLTC | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMINTELVID | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMUVID | B_PCIE_RCRB_CFG_DECCTL_RXVDMDECE | B_PCIE_RCRB_CFG_DECCTL_RXIMDECEN | B_PCIE_RCRB_CFG_DECCTL_RXLTRMDECH);
  Data32Or = (UINT32) (B_PCIE_RCRB_CFG_DECCTL_ATSCE | B_PCIE_RCRB_CFG_DECCTL_BUSNUMZCHK | B_PCIE_RCRB_CFG_DECCTL_OUTBEXPCPLCHKEN | B_PCIE_RCRB_CFG_DECCTL_DROPCPLATNZCE | B_PCIE_RCRB_CFG_DECCTL_LCRXINT | B_PCIE_RCRB_CFG_DECCTL_VDMATAC | B_PCIE_RCRB_CFG_DECCTL_URVDME16DW | B_PCIE_RCRB_CFG_DECCTL_URRXURTRCVDM | B_PCIE_RCRB_CFG_DECCTL_URRXULTVDM | B_PCIE_RCRB_CFG_DECCTL_URRXURAVDM | B_PCIE_RCRB_CFG_DECCTL_URRXURIDVDM | B_PCIE_RCRB_CFG_DECCTL_URRXUORVDM | B_PCIE_RCRB_CFG_DECCTL_URRXVMCTPBFRC | B_PCIE_RCRB_CFG_DECCTL_ICHKINVREQRMSGRBID | B_PCIE_RCRB_CFG_DECCTL_RXMCTPDECEN | B_PCIE_RCRB_CFG_DECCTL_RXGPEDECEN | B_PCIE_RCRB_CFG_DECCTL_RXSBEMCAPDECEN | B_PCIE_RCRB_CFG_DECCTL_RXADLEDDECEN | B_PCIE_RCRB_CFG_DECCTL_URRXMCTPNTCO | B_PCIE_RCRB_CFG_DECCTL_LCRXERRMSG | B_PCIE_RCRB_CFG_DECCTL_RSVD_RW | B_PCIE_RCRB_CFG_DECCTL_LCRXPTMREQ | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMRBFRC | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMRGRTRC | B_PCIE_RCRB_CFG_DECCTL_RXMCTPBRCDECEN);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_DECCTL, Data32And, Data32Or);

  DEBUG((DEBUG_INFO, "R_PCIE_RCRB_CFG_DECCTL after write = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_DECCTL)));

  ///
  /// Program (R_PCIE_RCRB_CFG_DECCTL) 0X190C
  /// Program (CPLBNCHK) 0X190C[0]  0x1
  ///
  CpuRegbarOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PIDECCTL, (UINT32)(B_PCIE_RCRB_CFG_PIDECCTL_CPLBNCHK));
  DEBUG ((DEBUG_INFO, "R_PCIE_RCRB_CFG_PIDECCTL after write = %x\n", CpuRegbarRead32(GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PIDECCTL)));
}

/**
  CPU PCIe SIP17 PHY initialization

  @param[in] RpIndex         Root Port Index
**/
VOID
CpuPcieSip17PhyInit (
 UINT32 RpIndex
 )
{
  UINT32   Data32Or;
  UINT32   Data32And;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17ResetPhyInit for RpIndex %x\n", RpIndex));

  //
  // BWG Section 5.1 Phy Configuration Setting
  //
  // Program Log Phy Control Policy 1 (LPHYCP1)
  //
  Data32And = (UINT32) ~(B_PCIE_RCRB_LPHYCP1_RXADPSVH | B_PCIE_RCRB_LPHYCP1_RXADPHM | B_PCIE_RCRB_LPHYCP1_RXEQFNEVC | \
                         B_PCIE_RCRB_LPHYCP1_PIPEMBIP);
  Data32Or  = (UINT32) (B_PCIE_RCRB_LPHYCP1_PIPEMBIP);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_LPHYCP1, Data32And, Data32Or);

  //
  // Program Log Phy Control Policy 4 (LPHYCP4)
  //
  Data32And = (UINT32) ~B_PCIE_LPHYCP4_PLLBUSDRC;
  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_LPHYCP4, Data32And);
}

/**
  Program Squelch Settings for ASPM L0s Support for SIP17

  @param[in] RpIndex     Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17SquelchSettingAspm (
  UINT32 RpIndex
  )
{
  UINT32           Data32And;
  UINT32           Data32Or;

  Data32And = (UINT32) ~(B_PCIE_RXL0SESQCP2_G1SSDFRXL0S_MASK | B_PCIE_RXL0SESQCP2_G1SQPDC_MASK | B_PCIE_RXL0SESQCP2_G2SSDFRXL0S_MASK | B_PCIE_RXL0SESQCP2_G2SQPDC_MASK | B_PCIE_RXL0SESQCP2_G3SSDFRXL0S_MASK | B_PCIE_RXL0SESQCP2_G3SQPDC_MASK | B_PCIE_RXL0SESQCP2_G4SSDFRXL0S_MASK | B_PCIE_RXL0SESQCP2_G4SQPDC_MASK);
  Data32Or = (UINT32)((V_PCIE_RXL0SESQCP2_G1SSDFRXL0S << B_PCIE_RXL0SESQCP2_G1SSDFRXL0S_OFFSET) | (V_PCIE_RXL0SESQCP2_G2SSDFRXL0S << B_PCIE_RXL0SESQCP2_G2SSDFRXL0S_OFFSET) | (V_PCIE_RXL0SESQCP2_G3SSDFRXL0S << B_PCIE_RXL0SESQCP2_G3SSDFRXL0S_OFFSET) | (V_PCIE_RXL0SESQCP2_G4SSDFRXL0S << B_PCIE_RXL0SESQCP2_G4SSDFRXL0S_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP2, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_RXL0SESQCP2 after setting = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP2)));

  Data32And = (UINT32) ~(B_PCIE_RXL0SESQCP3_G5SSDFRXL0S_MASK | B_PCIE_RXL0SESQCP3_G5SQPDC_MASK);
  Data32Or = (UINT32)(V_PCIE_RXL0SESQCP3_G5SSDFRXL0S << B_PCIE_RXL0SESQCP3_G5SSDFRXL0S_OFFSET);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP3, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_RXL0SESQCP3 after setting = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP3)));

  Data32And = (UINT32) ~(B_PCIE_RXL0SESQCP4_G1LBWSST_MASK | B_PCIE_RXL0SESQCP4_G2LBWSST_MASK | B_PCIE_RXL0SESQCP4_G3LBWSST_MASK | B_PCIE_RXL0SESQCP4_G4LBWSST_MASK);
  Data32Or = (UINT32)((V_PCIE_RXL0SESQCP4_G1LBWSST << B_PCIE_RXL0SESQCP4_G1LBWSST_OFFSET) | (V_PCIE_RXL0SESQCP4_G2LBWSST << B_PCIE_RXL0SESQCP4_G2LBWSST_OFFSET) | (V_PCIE_RXL0SESQCP4_G3LBWSST << B_PCIE_RXL0SESQCP4_G3LBWSST_OFFSET) | (V_PCIE_RXL0SESQCP4_G4LBWSST << B_PCIE_RXL0SESQCP4_G4LBWSST_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP4, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_RXL0SESQCP4 after setting = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP4)));

  Data32And = (UINT32) ~(B_PCIE_RXL0SESQCP5_G5LBWSST_MASK);
  Data32Or = (UINT32)(V_PCIE_RXL0SESQCP5_G5LBWSST << B_PCIE_RXL0SESQCP5_G5LBWSST_OFFSET);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP5, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_RXL0SESQCP5 after setting = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP5)));

  Data32And = (UINT32) ~(B_PCIE_LPHYCP4_LGCLKSQEXITDBTIMERS_MASK | B_PCIE_LPHYCP4_OSCCLKSQEXITDBTIMERS_MASK | B_PCIE_LPHYCP4_LGUSSP_MASK | B_PCIE_LPHYCP4_OSUSSP_MASK | B_PCIE_LPHYCP4_REEFRXL0SED);
  Data32Or = (UINT32)((V_PCIE_LPHYCP4_LGCLKSQEXITDBTIMERS << B_PCIE_LPHYCP4_LGCLKSQEXITDBTIMERS_OFFSET) | (V_PCIE_LPHYCP4_OSCCLKSQEXITDBTIMERS << B_PCIE_LPHYCP4_OSCCLKSQEXITDBTIMERS_OFFSET) | (V_PCIE_LPHYCP4_LGUSSP << B_PCIE_LPHYCP4_LGUSSP_OFFSET) | (V_PCIE_LPHYCP4_OSUSSP << B_PCIE_LPHYCP4_OSUSSP_OFFSET) | B_PCIE_LPHYCP4_DPMDNDBFE);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_LPHYCP4, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_LPHYCP4 after setting = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_LPHYCP4)));

  Data32And = (UINT32) ~(B_PCIE_RXL0SESQCP6_G1RXESPEMPEC_MASK | B_PCIE_RXL0SESQCP6_G1RXESWLDEMPEC_MASK | B_PCIE_RXL0SESQCP6_G2RXESPEMPEC_MASK | B_PCIE_RXL0SESQCP6_G2RXESWLDEMPEC_MASK);
  Data32Or = (UINT32)((V_PCIE_RXL0SESQCP6_G1RXESPEMPEC << B_PCIE_RXL0SESQCP6_G1RXESPEMPEC_OFFSET) | (V_PCIE_RXL0SESQCP6_G1RXESWLDEMPEC << B_PCIE_RXL0SESQCP6_G1RXESWLDEMPEC_OFFSET) | (V_PCIE_RXL0SESQCP6_G2RXESPEMPEC << B_PCIE_RXL0SESQCP6_G2RXESPEMPEC_OFFSET) | (V_PCIE_RXL0SESQCP6_G2RXESWLDEMPEC << B_PCIE_RXL0SESQCP6_G2RXESWLDEMPEC_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP6, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_RXL0SESQCP6 after setting = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP6)));

  Data32And = (UINT32) ~(B_PCIE_RXL0SESQCP7_G3RXESPEMPEC_MASK | B_PCIE_RXL0SESQCP7_G3RXESWLDEMPEC_MASK | B_PCIE_RXL0SESQCP7_G4RXESPEMPEC_MASK | B_PCIE_RXL0SESQCP7_G4RXESWLDEMPEC_MASK);
  Data32Or = (UINT32)((V_PCIE_RXL0SESQCP7_G3RXESPEMPEC << B_PCIE_RXL0SESQCP7_G3RXESPEMPEC_OFFSET) | (V_PCIE_RXL0SESQCP7_G3RXESWLDEMPEC << B_PCIE_RXL0SESQCP7_G3RXESWLDEMPEC_OFFSET) | (V_PCIE_RXL0SESQCP7_G4RXESPEMPEC << B_PCIE_RXL0SESQCP7_G4RXESPEMPEC_OFFSET) | (V_PCIE_RXL0SESQCP7_G4RXESWLDEMPEC << B_PCIE_RXL0SESQCP7_G4RXESWLDEMPEC_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP7, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_RXL0SESQCP7 after setting = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP7)));

  Data32And = (UINT32) ~(B_PCIE_RXL0SESQCP8_G5RXESPEMPEC_MASK | B_PCIE_RXL0SESQCP8_G5RXESWLDEMPEC_MASK);
  Data32Or = (UINT32)((V_PCIE_RXL0SESQCP8_G5RXESPEMPEC << B_PCIE_RXL0SESQCP8_G5RXESPEMPEC_OFFSET) | (V_PCIE_RXL0SESQCP8_G5RXESWLDEMPEC << B_PCIE_RXL0SESQCP8_G5RXESWLDEMPEC_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP8, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_RXL0SESQCP8 after setting = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP8)));

  Data32And = (UINT32) ~(B_PCIE_RXL0SESQCP1_SSMFRXL0S_MASK);
  Data32Or = (UINT32)(V_PCIE_RXL0SESQCP1_SSMFRXL0S << B_PCIE_RXL0SESQCP1_SSMFRXL0S_OFFSET);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP1, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_RXL0SESQCP1 after setting = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RXL0SESQCP1)));
}

/**
  Program Phy Recal Request for SIP17

  @param[in] RpIndex     Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17PhyRecalRequest (
  UINT32 RpIndex
  )
{
  UINT32           Data32And;
  UINT32           Data32Or;
  UINT64           RpBase;

  RpBase = CpuPcieBase (RpIndex);
  Data32And = (UINT32) ~(B_PCIE_IORCCP1_DISORCL12REC | B_PCIE_IORCCP1_DRCORRP | B_PCIE_IORCCP1_DISORCRODI | B_PCIE_IORCCP1_ORRPGM_MASK | B_PCIE_IORCCP1_G1ORRRXECC_MASK | B_PCIE_IORCCP1_G2ORRRXECC_MASK | B_PCIE_IORCCP1_G3ORRRXECC_MASK | B_PCIE_IORCCP1_G4ORRRXECC_MASK | B_PCIE_IORCCP1_G5ORRRXECC_MASK);
  Data32Or = (UINT32)((V_PCIE_IORCCP1_DISORCL12REC << B_PCIE_IORCCP1_DISORCL12REC_OFFSET) | (V_PCIE_IORCCP1_DRCORRP << B_PCIE_IORCCP1_DRCORRP_OFFSET) | (V_PCIE_IORCCP1_DISORCRODI << B_PCIE_IORCCP1_DISORCRODI_OFFSET) | (V_PCIE_IORCCP1_G1ORRRXECC << B_PCIE_IORCCP1_G1ORRRXECC_OFFSET) | (V_PCIE_IORCCP1_G2ORRRXECC << B_PCIE_IORCCP1_G2ORRRXECC_OFFSET)|(V_PCIE_IORCCP1_G3ORRRXECC << B_PCIE_IORCCP1_G3ORRRXECC_OFFSET) | (V_PCIE_IORCCP1_G4ORRRXECC << B_PCIE_IORCCP1_G4ORRRXECC_OFFSET) | (V_PCIE_IORCCP1_G5ORRRXECC << B_PCIE_IORCCP1_G5ORRRXECC_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_IORCCP1, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_IORCCP1 after setting = %x\n", CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_IORCCP1)));

  Data32And = (UINT32) ~(B_PCIE_MPC2_ORCE_MASK);
  Data32Or = (UINT32)((V_PCIE_MPC2_ORCE << B_PCIE_MPC2_ORCE_OFFSET));
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_MPC2, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_MPC2 after setting = %x\n", PciSegmentRead32 (RpBase +  R_PCIE_MPC2)));
}

/**
  Program Power Down Mapping Registers for SIP17

  @param[in] RpIndex     Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17PowerDownMapping (
  IN UINT32 RpIndex
)
{
  UINT32  Data32Or;
  UINT32  Data32And;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17PowerDownMapping for RpIndex %x\n", RpIndex));
  //
  // BWG recommended values for SIP17 controller are programmed here
  // Section 4.37 Power Down Mapping
  //
  Data32And = (UINT32) ~(B_PCIE_RCRB_CFG_PIPEPDCTL_DETNOPGPDCTL_MASK  | B_PCIE_RCRB_CFG_PIPEPDCTL_DETPGPDCTL_MASK   | B_PCIE_RCRB_CFG_PIPEPDCTL_L23NOPGPDCTL_MASK |
                         B_PCIE_RCRB_CFG_PIPEPDCTL_L23PGPDCTL_MASK    | B_PCIE_RCRB_CFG_PIPEPDCTL_DISNOPGPDCTL_MASK | B_PCIE_RCRB_CFG_PIPEPDCTL_DISPGPDCTL_MASK   |
                         B_PCIE_RCRB_CFG_PIPEPDCTL_L1PGNOPGPDCTL_MASK | B_PCIE_RCRB_CFG_PIPEPDCTL_L1PGUPGPDCTL_MASK);
  Data32Or  = (UINT32) ((V_PCIE_RCRB_CFG_PIPEPDCTL_DETNOPGPDCTL  << B_PCIE_RCRB_CFG_PIPEPDCTL_DETNOPGPDCTL_OFFSET) |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL_DETPGPDCTL    << B_PCIE_RCRB_CFG_PIPEPDCTL_DETPGPDCTL_OFFSET)   |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL_L23NOPGPDCTL  << B_PCIE_RCRB_CFG_PIPEPDCTL_L23NOPGPDCTL_OFFSET) |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL_L23PGPDCTL    << B_PCIE_RCRB_CFG_PIPEPDCTL_L23PGPDCTL_OFFSET)   |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL_DISNOPGPDCTL  << B_PCIE_RCRB_CFG_PIPEPDCTL_DISNOPGPDCTL_OFFSET) |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL_DISPGPDCTL    << B_PCIE_RCRB_CFG_PIPEPDCTL_DISPGPDCTL_OFFSET)   |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL_L1PGNOPGPDCTL << B_PCIE_RCRB_CFG_PIPEPDCTL_L1PGNOPGPDCTL_OFFSET)|
                        (V_PCIE_RCRB_CFG_PIPEPDCTL_L1PGUPGPDCTL  << B_PCIE_RCRB_CFG_PIPEPDCTL_L1PGUPGPDCTL_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PIPEPDCTL, Data32And, Data32Or);

  Data32And = (UINT32) ~(B_PCIE_RCRB_CFG_PIPEPDCTL2_L1UPNOPGPDCTL_MASK  | B_PCIE_RCRB_CFG_PIPEPDCTL2_L1D1UPUPGPDCTL_MASK | B_PCIE_RCRB_CFG_PIPEPDCTL2_L1D1PGNOPGPDCTL_MASK |
                         B_PCIE_RCRB_CFG_PIPEPDCTL2_L1D1PGPGPDCTL_MASK  | B_PCIE_RCRB_CFG_PIPEPDCTL2_L1D2NOPGPDCTL_MASK  | B_PCIE_RCRB_CFG_PIPEPDCTL2_L1D2PGPDCTL_MASK     |
                         B_PCIE_RCRB_CFG_PIPEPDCTL2_DUCFGNOPGPDCTL_MASK | B_PCIE_RCRB_CFG_PIPEPDCTL2_DUCFGPGPDCTL_MASK);
  Data32Or  = (UINT32) ((V_PCIE_RCRB_CFG_PIPEPDCTL2_L1UPNOPGPDCTL   << B_PCIE_RCRB_CFG_PIPEPDCTL2_L1UPNOPGPDCTL_OFFSET)   |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL2_L1D1UPUPGPDCTL  << B_PCIE_RCRB_CFG_PIPEPDCTL2_L1D1UPUPGPDCTL_OFFSET)  |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL2_L1D1PGNOPGPDCTL << B_PCIE_RCRB_CFG_PIPEPDCTL2_L1D1PGNOPGPDCTL_OFFSET) |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL2_L1D1PGPGPDCTL   << B_PCIE_RCRB_CFG_PIPEPDCTL2_L1D1PGPGPDCTL_OFFSET)   |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL2_L1D2NOPGPDCTL   << B_PCIE_RCRB_CFG_PIPEPDCTL2_L1D2NOPGPDCTL_OFFSET)   |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL2_L1D2PGPDCTL     << B_PCIE_RCRB_CFG_PIPEPDCTL2_L1D2PGPDCTL_OFFSET)     |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL2_DUCFGNOPGPDCTL  << B_PCIE_RCRB_CFG_PIPEPDCTL2_DUCFGNOPGPDCTL_OFFSET)  |
                        (V_PCIE_RCRB_CFG_PIPEPDCTL2_DUCFGPGPDCTL    << B_PCIE_RCRB_CFG_PIPEPDCTL2_DUCFGPGPDCTL_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PIPEPDCTL2, Data32And, Data32Or);

  Data32And = (UINT32) ~(B_PCIE_RCRB_CFG_PIPEPDCTL3_L1DLOWNOPGPDCTL_MASK | B_PCIE_RCRB_CFG_PIPEPDCTL3_L1DLOWPGPDCTL_MASK);
  Data32Or  = (UINT32) ((V_PCIE_RCRB_CFG_PIPEPDCTL3_L1DLOWNOPGPDCTL << B_PCIE_RCRB_CFG_PIPEPDCTL3_L1DLOWNOPGPDCTL_OFFSET) | (V_PCIE_RCRB_CFG_PIPEPDCTL3_L1DLOWPGPDCTL << B_PCIE_RCRB_CFG_PIPEPDCTL3_L1DLOWPGPDCTL_OFFSET));
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PIPEPDCTL3, Data32And, Data32Or);

  CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_CFG_PIPEPDCTLEXT, (UINT32) ~(B_PCIE_RCRB_CFG_PIPEPDCTLEXT_P2TP2TCD | B_PCIE_RCRB_CFG_PIPEPDCTLEXT_P2TP2TP));
}

/**
  Program FIA Registers for SIP17

  @param[in] RpIndex     Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17FiaFinalizeConfigurationAndLock (
  IN UINT8                      RpIndex
)
{
  CPU_SB_DEVICE_PID  CpuRpFiaPortId;

  CpuRpFiaPortId = GetCpuPcieFiaSbiPid (RpIndex);
  if (CpuRpFiaPortId != 0xFF) {
    //
    // Set PCR[FIA] + 0h bit [31, 16, 15] to [1, 1, 1]
    //
    CpuRegbarAndThenOr32 (
        CpuRpFiaPortId,
        R_PCH_FIA_PCR_CC,
        ~0u,
        B_PCH_FIA_PCR_CC_SRL | B_PCH_FIA_PCR_CC_OSCDCGE | B_PCH_FIA_PCR_CC_SCPTCGE
        );
    DEBUG ((DEBUG_INFO, "Offset 0x00[CC] = 0x%x\n", CpuRegbarRead32 (CpuRpFiaPortId, R_PCH_FIA_PCR_CC)));

    //
    // Program PCR[FIA] + 18h bit [17:16, 1:0] to [00, 00]
    //
    CpuRegbarAndThenOr32 (
      CpuRpFiaPortId,
      R_PCH_FIA_PCR_CLSDM,
      (UINT32) ~(B_PCH_FIA_PCR_CLSDM_DMIIPSLSD | B_PCH_FIA_PCR_CLSDM_PCIEGBEIPSLSD),
      0
      );
    DEBUG ((DEBUG_INFO, "Offset 0x18[CLSDM] = 0x%x\n", CpuRegbarRead32 (CpuRpFiaPortId, R_PCH_FIA_PCR_CLSDM)));

    //
    // Set PCR[FIA] + 40h bit [3] to [1]
    //
    CpuRegbarAndThenOr32 (
        CpuRpFiaPortId,
        R_PCH_FIA_PCR_PMC,
        ~0u,
        B_PCH_FIA_PCR_PMC_PRDPGE
        );
    DEBUG ((DEBUG_INFO, "Offset 0x40[PMC] = 0x%x\n", CpuRegbarRead32 (CpuRpFiaPortId, R_PCH_FIA_PCR_PMC)));

    //
    // Set PCR[FIA] + 48h bit [0] to [0]
    //
    CpuRegbarAndThenOr32 (
        CpuRpFiaPortId,
        R_PCH_FIA_PCR_PGCUC,
        (UINT32)~(B_PCH_FIA_PCR_PGCUC_ACC_CLKGATE_DISABLED),
        0
        );
    DEBUG ((DEBUG_INFO, "Offset 0x48[PGCUC] = 0x%x\n", CpuRegbarRead32 (CpuRpFiaPortId, R_PCH_FIA_PCR_PGCUC)));

    //
    // Set PCR[FIA] + 50h bit [0] to [0]
    //
    CpuRegbarAndThenOr32 (
        CpuRpFiaPortId,
        R_PCH_FIA_PCR_PGCUCSOC,
        (UINT32)~(B_PCH_FIA_PCR_PGCUCSOC_ACC_CLKGATE_DISABLED),
        0
        );
    DEBUG ((DEBUG_INFO, "Offset 0x50[PGCUCSOC] = 0x%x\n", CpuRegbarRead32 (CpuRpFiaPortId, R_PCH_FIA_PCR_PGCUCSOC)));
  }
}

/**
  Program Power Management Registers for SIP17

  @param[in] RpIndex      Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17PowerManagement (
  IN UINT32 RpIndex
)
{
  UINT32  Data32Or;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17PowerManagement for RpIndex %x\n", RpIndex));
  //
  // As per BWG, Power Management Registers for SIP17 controller are programmed here
  //
  Data32Or = (UINT32) (B_PCIE_RCRB_PHYPG_DLPPGP | B_PCIE_RCRB_PHYPG_DETPHYPGE | B_PCIE_RCRB_PHYPG_DISPHYPGE | B_PCIE_RCRB_PHYPG_L23PHYPGE | B_PCIE_RCRB_PHYPG_DUCFGPHYPGE);
  CpuRegbarOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_PHYPG, Data32Or);
}

/**
  Program Power Management Registers for SIP17

  @param[in] RpIndex      Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17EbThreshold (
  IN UINT32 RpIndex
 )
{
  UINT32  Data32And;
  UINT32  Data32Or;

  DEBUG ((DEBUG_INFO, "CpuPcieSip17EbThreshold for RpIndex %x\n", RpIndex));

  ///
  /// Program (R_PCIE_RCRB_SDPILPCFG2) 1488h
  /// Set 1488h[3:0] to 0x3 for LB EB Int Seperation
  ///
  Data32And = (UINT32) ~(B_PCIE_RCRB_SDPILPCFG2_LBEBINTSEPERATION_MASK);
  Data32Or  = (UINT32) (V_PCIE_RCRB_SDPILPCFG2_LBEBINTSEPERATION);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_SDPILPCFG2, Data32And, Data32Or);

  ///
  /// Program (R_PCIE_RCRB_SDPILPCFG1) 1484h
  /// Set 1484h[29:26] to 0x6 for LB Eb Discard SKP Threshold
  ///
  Data32And = (UINT32) ~(B_PCIE_RCRB_SDPILPCFG1_EBDISSKPTHSLD_MASK);
  Data32Or = (UINT32) (V_PCIE_RCRB_SDPILPCFG1_EBDISSKPTHSLD << B_PCIE_RCRB_SDPILPCFG1_EBDISSKPTHSLD_OFFSET);
  CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_RCRB_SDPILPCFG1, Data32And, Data32Or);
}