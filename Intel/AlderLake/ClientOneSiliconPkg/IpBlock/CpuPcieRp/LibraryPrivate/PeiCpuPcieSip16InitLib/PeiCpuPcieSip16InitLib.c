
/** @file
  This file contains functions that for PCIe SIP16.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PeiPchPcieClocksLib.h>
#include <PcieRegs.h>
#include <Register/CpuPcieRegs.h>
#include <Library/PciLib.h>
#include <Library/CpuPcieInitCommon.h>
#include <Library/CpuPcieRpLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/PeiCpuPcieSip16InitLib.h>
#include <CpuSbInfo.h>
#include <Register/PchFiaRegs.h>
#include <Library/PreSiliconEnvDetectLib.h>

/**
  This function creates SIP16 Capability and Extended Capability List

  @param[in] RpIndex         Root Port index
  @param[in] RpBase          Root Port pci segment base address
  @param[in] CpuPcieRpConfig Pointer to a CPU_PCIE_CONFIG that provides the platform setting

**/
VOID
CpuPcieSip16InitCapabilityList (
  IN UINT32                           RpIndex,
  IN UINT64                           RpBase,
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG  *CpuPcieRpConfig
  )
{
  UINT32      Data32;
  UINT16      Data16;
  UINT8       Data8;
  UINT16      NextCap;

  DEBUG ((DEBUG_INFO, "CpuPcieSip16InitCapabilityList start\n"));
  ///
  /// Build Capability linked list
  /// 1.  Read and write back to capability registers 34h, 41h, 81h and 91h using byte access.
  /// 2.  Program NSR, A4h[3] = 0b
  ///
  Data8 = PciSegmentRead8 (RpBase + PCI_CAPBILITY_POINTER_OFFSET);
  PciSegmentWrite8 (RpBase + PCI_CAPBILITY_POINTER_OFFSET, Data8);

  Data16 = PciSegmentRead16 (RpBase + R_PCIE_CLIST);
  PciSegmentWrite16 (RpBase + R_PCIE_CLIST, Data16);

  Data16 = PciSegmentRead16 (RpBase + R_PCIE_MID);
  PciSegmentWrite16 (RpBase + R_PCIE_MID, Data16);

  Data16 = PciSegmentRead16 (RpBase + R_PCIE_SIP16_SVCAP);
  PciSegmentWrite16 (RpBase + R_PCIE_SIP16_SVCAP, Data16);

  Data32 = PciSegmentRead32 (RpBase + R_PCIE_PMCS);
  Data32 &= (UINT32) ~(B_PCIE_PMCS_NSR);
  PciSegmentWrite32 (RpBase + R_PCIE_PMCS, Data32);

  ///
  /// II.Build PCI Extended Capability linked list
  /// 0xEDC - PL16MECH(CAPID:0027h) Physical Layer 16.0 GT/s Margining Extended Capability Header
  /// 0xA9C - PL16GECH(CAPID:0026h) Physical Layer 16.0 GT/s Extended Capability Header
  /// 0xA90 - DLFECH  (CAPID:0025h) Data Link Feature Extended Capability Header
  /// 0xA30 - SPEECH  (CAPID:0019h) Secondary PCI Express Extended Capability Header
  /// 0xA00 - DPCECH  (CAPID:001Dh)
  /// 0x280 - VCECH   (CAPID:0002h) VC capability
  /// 0x150 - PTMECH  (CAPID:001Fh) PTM Extended Capability Register
  /// 0x200 - L1SSECH (CAPID:001Eh) L1SS Capability register
  /// 0x220 - ACSECH  (CAPID:000Dh) ACS Capability register
  /// 0x100 - AECH    (CAPID:0001h) Advanced Error Reporting Capability
  ///
  /*
  a. NEXT_CAP = 0h
  */
  NextCap     = V_PCIE_EXCAP_NCO_LISTEND;

  /*
  b. Program [0xEDC] Physical Layer 16.0 GT/s Margining Extended Capability Header(PL16MECH)
    1. Set Next Capability Offset, 0xEDC[31:20] = NEXT_CAP
    2. Set Capability Version, 0xEDC[19:16] = 1h
    3. Set Capability ID, 0xEDC[15:0] = 0027h
    4. NEXT_CAP = 0xEDC
  */
  Data32 = (V_PCIE_PL16MECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PL16MECH_CID;
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  NextCap = R_PCIE_PL16MECH;
  PciSegmentWrite32 (RpBase + R_PCIE_PL16MECH, Data32);
  DEBUG ((DEBUG_VERBOSE, "Physical Layer 16.0 GT/s Margining Extended Capability Header\n"));
  DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PL16MECH_CID));

  /*
  c. Program [0xA9C] Physical Layer 16.0 GT/s Extended Capability Header(PL16GECH)
    1. Set Next Capability Offset, 0xA9C[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA9C[19:16] = 1h
    3. Set Capability ID, 0xA9C[15:0] = 0026h
    4. NEXT_CAP = 0xA9C
  */
  Data32 = (V_PCIE_PL16GECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PL16GECH_CID;
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  NextCap = R_PCIE_PL16GECH;
  PciSegmentWrite32 (RpBase + R_PCIE_PL16GECH, Data32);
  DEBUG ((DEBUG_VERBOSE, "Physical Layer 16.0 GT/s Extended Capability Header\n"));
  DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PL16GECH_CID));

  /*
  d. Program [0xA90] Data Link Feature Extended Capability Header(DLFECH)
    1. Set Next Capability Offset, 0xA90[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA90[19:16] = 1h
    3. Set Capability ID, 0xA90[15:0] = 0025h
    4.  NEXT_CAP = 0xA90
  */
  Data32 = (V_PCIE_DLFECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_DLFECH_CID;
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  NextCap = R_PCIE_DLFECH;
  PciSegmentWrite32 (RpBase + R_PCIE_DLFECH, Data32);
  DEBUG ((DEBUG_VERBOSE, "Data Link Feature Extended Capability Header\n"));
  DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_DLFECH_CID));

  /*
  e. If the RP is GEN3 capable (by fuse and BIOS policy), enable Secondary PCI Express Extended Capability
    1. Set Next Capability Offset,0xA30[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA30[19:16] = 1h
    3. Set Capability ID,  0xA30[15:0] = 0019h
    4. NEXT_CAP = 0xA30
    ELSE, set 0xA30[31:0] = 0
  */
  Data32 = 0;
  if (CpuPcieGetMaxLinkSpeed (RpBase) >= V_PCIE_LCAP_MLS_GEN3) {
    Data32 = (V_PCIE_SPEECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_SPE_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "Secondary PCI Express Extended Capability\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_SPE_CID));
    NextCap = R_PCIE_SPEECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_SPEECH, Data32);

  /*
  f. If Downstream Port Containment is enabled, then
    1. Set Next Capability Offset, 0xA00[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA00[19:16] = 1h
    3. Set Capability ID, 0xA00[15:0] = 001h
    4. NEXT_CAP = 0xA00
    ELSE, set 0xA00 [31:0] = 0
  */
  Data32 = 0;
  if (CpuPcieRpConfig->PcieRpCommonConfig.DpcEnabled == TRUE) {
    Data32 = (V_PCIE_DPCECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_DPC_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "Downstream port containment\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_DPC_CID));
    NextCap = R_PCIE_DPCECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_DPCECH, Data32);

  /*
  g. If VC is enabled, then
    1. Set Next Capability Offset, 0x280[31:20] = NEXT_CAP
    2. Set Capability Version, 0x280[19:16] = 1h
    3. Set Capability ID, 0x280[15:0] = 002h
    4. NEXT_CAP = 0x280
    ELSE, set 0x280 [31:0] = 0
  */
  Data32 = 0;
  if (CpuPcieRpConfig->MultiVcEnabled == TRUE) {
    Data32 = (V_PCIE_VCECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_VC_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "Virtual Channel\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_VC_CID));
    NextCap = R_PCIE_VCECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_VCECH, Data32);

   /*
  h. If support PTM
    1. Set Next Capability Offset, Dxx:Fn +150h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +150h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +150h[15:0] = 001Fh
    4. Read and write back to Dxx:Fn +144h
    5. NEXT_CAP = 150h
    ELSE, set Dxx:Fn +150h [31:0] = 0
    In both cases: read Dxx:Fn + 154h, set BIT1 and BIT2 then write it back
  */
  Data32 = 0;
  if (CpuPcieRpConfig->PcieRpCommonConfig.PtmEnabled == TRUE) {
    Data32 = (V_PCIE_PTMECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PTM_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "PTM\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PTM_CID));
    NextCap = R_PCIE_PTMECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_PTMECH, Data32);
  Data32 = PciSegmentRead32 (RpBase + R_PCIE_PTMCAPR);
  PciSegmentWrite32 (RpBase + R_PCIE_PTMCAPR, (Data32 | B_PCIE_PTMCAPR_PTMRC | B_PCIE_PTMCAPR_PTMRSPC));

  /*
  i. If support L1 Sub-State
    1. Set Next Capability Offset, Dxx:Fn +200h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +200h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +200h[15:0] = 001Eh
    4. Read and write back to Dxx:Fn +204h
    5. Refer to section 8.3 for other requirements (Not implemented here)
    6. NEXT_CAP = 200h
    ELSE, set Dxx:Fn +200h [31:0] = 0, and read and write back to Dxx:Fn +204h
  */
  Data32 = 0;
  if (IsClkReqAssigned (PchClockUsageCpuPcie0 + RpIndex) &&
    (CpuPcieRpConfig->PcieRpCommonConfig.L1Substates != CpuPcieL1SubstatesDisabled)) {
    Data32  = (V_PCIE_L1S_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_L1S_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "L1SS\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_L1S_CID));
    NextCap = R_PCIE_L1SECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_L1SECH, Data32);

  /*
  j. If support ACS
    1. Set Next Capability Offset, Dxx:Fn +220h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +220h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +220h[15:0] = 000Dh
    4. Read and write back to Dxx:Fn +224h
    5. NEXT_CAP = 220h
    ELSE, set Dxx:Fn +220h [31:0] = 0, and read and write back to Dxx:Fn +224h
  */
  Data32 = 0;
  if (CpuPcieRpConfig->PcieRpCommonConfig.AcsEnabled == TRUE) {
    Data32 = (V_PCIE_ACSECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_ACS_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "ACS\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_ACS_CID));
    NextCap = R_PCIE_ACSECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_ACSECH, Data32);

  Data32 = PciSegmentRead32 (RpBase + R_PCIE_ACSCAPR);
  PciSegmentWrite32 (RpBase + R_PCIE_ACSCAPR, Data32);

  /*
  k. If support Advanced Error Reporting
    1. Set Next Capability Offset, Dxx:Fn +100h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +100h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +100h[15:0] = 0001h
    ELSE
    1. Set Next Capability Offset, Dxx:Fn +100h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +100h[19:16]  = 0h
    3. Set Capability ID, Dxx:Fn +100h[15:10]  = 0000h
  */
  Data32 = 0;
  if (CpuPcieRpConfig->PcieRpCommonConfig.AdvancedErrorReporting) {
    Data32 = (V_PCIE_AECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_AEC_CID;
  }
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  PciSegmentWrite32 (RpBase + R_PCIE_AECH, Data32);

  DEBUG ((DEBUG_INFO, "CpuPcieSip16InitCapabilityList end\n"));

  //
  // Mask Unexpected Completion uncorrectable error
  //
  PciSegmentOr32 (RpBase + R_PCIE_UEM, B_PCIE_UEM_UC);
}

/**
  CPU PCIe SIP16 PTM initialization

  @param[in]  RpBase                Root Port pci segment base address.
**/
VOID
EFIAPI
CpuPcieSip16PtmInit (
  IN  UINT64  RpBase
)
{

  DEBUG ((DEBUG_INFO, "CpuPcieSip16PtmInit for RpBase %x\n", RpBase));

  //
  // PTM programming happens per controller
  // BWG recommended values for SIP16 controller are programmed here
  //
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_CFG_PTMPSDC1, 0x0, 0x321A3D26);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_CFG_PTMPSDC2, 0x0, 0x21172D13);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_CFG_PTMPSDC3, 0x0, 0x190C1B10);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_CFG_PTMPSDC4, 0x0, 0x26162D1D);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_CFG_PTMPSDC5, (UINT32)0xFFFF0000, 0x00002413);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_CFG_PTMPSDC9, 0x0, 0x301C372F);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_CFG_PTMPSDC10, (UINT32)0xFFFF0000, 0x00002E18);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_PTMECFG, (UINT32)0xFFE00000, 0x00040250);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_PTMCAPR, (UINT32)~(B_PCIE_PTMCAPR_PTMRC | B_PCIE_PTMCAPR_PTMRSPC | B_PCIE_PTMCAPR_PTMREQC), 0x00000007);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_PTMCTLR, (UINT32)~(B_PCIE_PTMCTLR_RS), 0x00000003);
  PciSegmentAndThenOr32 (RpBase +  R_PCIE_ACRG3, (UINT32)~(B_PCIE_ACRG3_PPTMTMUCF_MASK | B_PCIE_ACRG3_PPTMTMUCFE), 0x00000000);
}

/**
  CPU PCIe SIP16 Gen3 PresetToCoeff mapping

  @param[in] RpIndex              Root Port Index for which HW EQ is to be performed
**/
VOID
CpuPcieSip16Gen3PresetToCoeff (
  UINT8      RpIndex
  )
{
  //
  // Before training to GEN3 Configure Lane0 P0-P10 Preset Coefficient mapping based on recommendation for CPU PCIE
  //
  DEBUG ((DEBUG_VERBOSE, "CpuPcieSip16Gen3PresetToCoeff!\n"));
  //
  // Preset to Coefficient programming are per controller and hence for PEG12, controller related programming will be on PEG11 because PEG11/PEG12 share common controller
  //
  if (RpIndex == 0x3) {
  RpIndex = RpIndex - 1;
  }
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_L0P0P1PCM, 0x00D1002F);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_L0P1P2P3PCM, 0x37340C8B);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_L0P3P4PCM, 0x0003F200);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_L0P5P6PCM, 0x08DC01B9);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_L0P6P7P8PCM, 0x2F346B00);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_L0P8P9PCM, 0x002F4208);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_L0P10PCM, 0x0001502A);
}

/**
  CPU PCIe SIP16 Gen4 PresetToCoeff mapping

  @param[in] RpIndex              Root Port Index for which HW EQ is to be performed
**/
VOID
CpuPcieSip16Gen4PresetToCoeff (
  UINT8      RpIndex
  )
{
  //
  // Before training to GEN4 Configure Lane0 P0-P10 Preset Coefficient mapping based on recommendation for CPU PCIE
  //
  DEBUG ((DEBUG_VERBOSE, "CpuPcieSip16Gen4PresetToCoeff!\n"));
  //
  // Preset to Coefficient programming are per controller and hence for PEG12, controller related programming will be on PEG11 because PEG11/PEG12 share common controller
  //
  if (RpIndex == 0x3) {
  RpIndex = RpIndex - 1;
  }
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_PX16GP0P1PCM, 0x00D1002F);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_PX16GP1P2P3PCM, 0x37340C8B);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_PX16GP3P4PCM, 0x0003F200);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_PX16GP5P6PCM, 0x08DC01B9);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_PX16GP6P7P8PCM, 0x2F346B00);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_PX16GP8P9PCM, 0x002F4208);
  CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), R_PCIE_PX16GP10PCM, 0x0001502A);
}

/**
  Configure Number of Fast Training Sequence ordered sets for common and Unique Clock

  @param[in] RpBase      Root Port pci segment base address
**/
VOID
CpuPcieSip16ConfigureNfts (
  UINT64 RpBase
  )
{
  UINT32    Data32Or;
  UINT32    Data32And;
  UINT32    Data32;

  ///
  /// Program (R_PCIE_PCIENFTS) 0x314
  /// Gen1 Unique Clock N_FTS (G1UCNFTS) 0x314[15:8] 0x3F
  /// Gen1 Common Clock N_FTS (G1CCNFTS) 0x314[7:0] 0xA0
  /// Gen2 Unique Clock N_FTS (G2UCNFTS) 0x314[31:24] 0x7E
  /// Gen2 Common Clock N_FTS (G2CCNFTS) 0x314[23:16] 0xD5
  ///
  Data32 = (0x7E << B_PCIE_PCIENFTS_G2UCNFTS_OFFSET) | (0xD5 << B_PCIE_PCIENFTS_G2CCNFTS_OFFSET) \
            | (0x3F << B_PCIE_PCIENFTS_G1UCNFTS_OFFSET) | (0xBB << B_PCIE_PCIENFTS_G1CCNFTS_OFFSET);
  PciSegmentWrite32 (RpBase + R_PCIE_PCIENFTS, Data32);

  ///
  /// Program (R_PCIE_G3L0SCTL) 0x478
  /// Gen3 Unique Clock N_FTS (G3UCNFTS) 0x478[15:8] 0x40
  /// Gen3 Common Clock N_FTS (G3CCNFTS) 0x478[7:0] 0x2C
  ///
  Data32And = (UINT32) ~(B_PCIE_G3L0SCTL_G3UCNFTS_MASK | B_PCIE_G3L0SCTL_G3CCNFTS_MASK);
  Data32Or = (UINT32) (0x40 << B_PCIE_G3L0SCTL_G3ASL0SPL_OFFSET) | (V_PCIE_G3L0SCTL_G3UCNFTS << B_PCIE_G3L0SCTL_G3UCNFTS_OFFSET) | (V_PCIE_G3L0SCTL_G3CCNFTS << B_PCIE_G3L0SCTL_G3CCNFTS_OFFSET);

  PciSegmentAndThenOr32 (RpBase + R_PCIE_G3L0SCTL, Data32And, Data32Or);

  ///
  /// Program (R_PCIE_G4L0SCTL) 0x310
  /// Gen4 Unique Clock N_FTS (G4UCNFTS) 0x310[15:8] 0x80
  /// Gen4 Common Clock N_FTS (G4CCNFTS) 0x310[7:0] 0x5B
  ///
  Data32And = (UINT32) ~(B_PCIE_G4L0SCTL_G4UCNFTS_MASK | B_PCIE_G4L0SCTL_G4CCNFTS_MASK);
  Data32Or = (UINT32) (0x80 << B_PCIE_G4L0SCTL_G4UCNFTS_OFFSET) | (0x5B << B_PCIE_G4L0SCTL_G4CCNFTS_OFFSET);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_G4L0SCTL, Data32And, Data32Or);
}

/**
  Program FIA Registers for SIP16

  @param[in] RpIndex     Root Port Index

  @retval                None
**/
VOID
CpuPcieSip16FiaFinalizeConfigurationAndLock (
  IN UINT8                      RpIndex
)
{
  CPU_SB_DEVICE_PID   CpuRpFiaPortId;

  CpuRpFiaPortId = GetCpuPcieFiaSbiPid (RpIndex);
  if (CpuRpFiaPortId != 0xFF) {
    //
    // Set PCR[FIA] + 0h bit [17, 16, 15] to [1, 1, 1]
    //
    CpuRegbarAndThenOr32 (
      CpuRpFiaPortId,
      R_PCH_FIA_PCR_CC,
      ~0u,
      B_PCH_FIA_PCR_CC_PTOCGE | B_PCH_FIA_PCR_CC_OSCDCGE | B_PCH_FIA_PCR_CC_SCPTCGE
    );
    DEBUG ((DEBUG_INFO, "Offset 0x00[CC] = 0x%x\n", CpuRegbarRead32 (CpuRpFiaPortId, R_PCH_FIA_PCR_CC)));

    //
    // Set PCR[FIA] + 20h bit [0, 1, 31] to [0, 0, 0]
    //
    CpuRegbarAndThenOr32 (
      CpuRpFiaPortId,
      R_PCH_FIA_PCR_PLLCTL,
      (UINT32)~(B_PCH_FIA_PCR_PLLCTL_CL0PLLFO | B_PCH_FIA_PCR_PLLCTL_CL1PLLFO | B_PCH_FIA_PCR_PLLCTL_PLLACBGD),
      0
    );
    DEBUG ((DEBUG_INFO, "Offset 0x20[PLLCTL] = 0x%x\n", CpuRegbarRead32 (CpuRpFiaPortId, R_PCH_FIA_PCR_PLLCTL)));

    //
    // Set PCR[FIA] + 40h bit [3] to [1]
    //
    CpuRegbarAndThenOr32 (
      CpuRpFiaPortId,
      R_PCH_FIA_PCR_PMC,
      ~0u,
      B_PCH_FIA_PCR_PMC_PRDPGE
    );
    DEBUG ((DEBUG_INFO, "Offset 0x40[PMC] = 0x%x\n", CpuRegbarRead32 (CpuRpFiaPortId, R_PCH_FIA_PCR_PMC)));

    //
    // Set PCR[FIA] + 48h bit [0] to [0]
    //
    CpuRegbarAndThenOr32 (
      CpuRpFiaPortId,
      R_PCH_FIA_PCR_PGCUC,
      (UINT32)~(B_PCH_FIA_PCR_PGCUC_ACC_CLKGATE_DISABLED),
      0
    );
    DEBUG ((DEBUG_INFO, "Offset 0x48[PGCUC] = 0x%x\n", CpuRegbarRead32 (CpuRpFiaPortId, R_PCH_FIA_PCR_PGCUC)));
  }
}
