/** @file
  The DXE CPU PCIE RP Library Implements After Memory PEIM

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/HobLib.h>
#include <Library/PciSegmentLib.h>
#include <IndustryStandard/Pci22.h>
#include <Protocol/SaPolicy.h>
#include <Protocol/SaNvsArea.h>
#include <Library/PcdLib.h>
#include <Library/UsbLib.h>
#include <Library/PciExpressHelpersLib.h>
#include <Library/BaseMemoryLib.h>
#include <ConfigBlock/PcieDxeConfig.h>
#include <Register/SaRegsHostBridge.h>

/**
Update CPU PCIE RP NVS AREA tables

**/
VOID
UpdateCpuPcieNVS (
  VOID
  )
{
  EFI_STATUS                       Status;
  PCIE_DXE_CONFIG                  *PcieDxeConfig;
  SA_POLICY_PROTOCOL               *SaPolicy;
  SYSTEM_AGENT_NVS_AREA_PROTOCOL   *SaNvsAreaProtocol;
  UINTN                            CpuPcieRpDev;
  UINTN                            CpuPcieRpFunc;
  UINTN                            RpIndex;
  UINT32                           Data32;

  DEBUG ((DEBUG_INFO, "Update Cpu Pcie NVS Area.\n"));

  Status = gBS->LocateProtocol (&gSaPolicyProtocolGuid, NULL, (VOID **) &SaPolicy);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *)SaPolicy, &gPcieDxeConfigGuid, (VOID *)&PcieDxeConfig);
  ASSERT_EFI_ERROR (Status);

  Status = gBS->LocateProtocol (&gSaNvsAreaProtocolGuid, NULL, (VOID **) &SaNvsAreaProtocol);
  if (Status != EFI_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "Locate SA NVS Area failed.\n"));
    return;
  }

  //
  // Update ACPI NVS variable with corresponding Root port's BDF
  //
  for (RpIndex = 0; RpIndex < GetMaxCpuPciePortNum (); RpIndex++) {
    GetCpuPcieRpDevFun (RpIndex, &CpuPcieRpDev, &CpuPcieRpFunc);
    DEBUG ((DEBUG_INFO, "CpuPcieRp[%d] Device No= %d, Function No =%d.\n", RpIndex, CpuPcieRpDev, CpuPcieRpFunc));
    Data32 = ((UINT8) CpuPcieRpDev << 16) | (UINT8) CpuPcieRpFunc;
    DEBUG ((DEBUG_INFO, "ACPI NVS variable with corresponding Root port's BDF=%x.\n", Data32));
    SaNvsAreaProtocol->Area->CpuPcieRpAddress[RpIndex] = Data32;
  }

  SaNvsAreaProtocol->Area->Peg0LtrEnable  = PcieDxeConfig->PegPwrOpt[0].LtrEnable;
  SaNvsAreaProtocol->Area->Peg0ObffEnable = PcieDxeConfig->PegPwrOpt[0].ObffEnable;
  SaNvsAreaProtocol->Area->Peg1LtrEnable  = PcieDxeConfig->PegPwrOpt[1].LtrEnable;
  SaNvsAreaProtocol->Area->Peg1ObffEnable = PcieDxeConfig->PegPwrOpt[1].ObffEnable;
  SaNvsAreaProtocol->Area->Peg2LtrEnable  = PcieDxeConfig->PegPwrOpt[2].LtrEnable;
  SaNvsAreaProtocol->Area->Peg2ObffEnable = PcieDxeConfig->PegPwrOpt[2].ObffEnable;
  SaNvsAreaProtocol->Area->Peg3LtrEnable  = PcieDxeConfig->PegPwrOpt[3].LtrEnable;
  SaNvsAreaProtocol->Area->Peg3ObffEnable = PcieDxeConfig->PegPwrOpt[3].ObffEnable;
  SaNvsAreaProtocol->Area->PegLtrMaxSnoopLatency = V_SA_LTR_MAX_SNOOP_LATENCY_VALUE;
  SaNvsAreaProtocol->Area->PegLtrMaxNoSnoopLatency = V_SA_LTR_MAX_NON_SNOOP_LATENCY_VALUE;
}
