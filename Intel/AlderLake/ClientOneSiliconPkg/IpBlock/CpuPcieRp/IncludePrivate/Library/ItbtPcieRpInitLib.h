/** @file
  Header file for TCSS North Lib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _SA_PEI_PCIE_COMMON_LIB_LIB_H_
#define _SA_PEI_PCIE_COMMON_LIB_LIB_H_

#include <Ppi/SiPolicy.h>
#include <TcssPeiConfig.h>

#define V_PCIE_EX_CV                   0x1     /// PCI Extended capability Version

/**
ASPM Settings
**/
typedef enum {
  PcieL1SubstatesDisabled,
  PcieL1SubstatesL1_1,
  PcieL1SubstatesL1_2,
  PcieL1SubstatesL1_1_2,
  PcieL1SubstatesMax
} PCIE_L1SUBSTATES_CONTROL;

/**
PCIe Max Payload
**/
typedef enum {
  PcieMaxPayload_128B,
  PcieMaxPayload_256B,
} PCIE_MAX_PAYLOAD;

/**
This function Implements iTbt PCI Port initialization

SIP15 PCI Express Root Port BIOS Requirement Rev 0.9
@param[in] TcssConfig           Pointer to config block

**/
VOID
ItbtInitRootPorts(
  IN  TCSS_PEI_CONFIG             *TcssPeiConfig
);

#endif
