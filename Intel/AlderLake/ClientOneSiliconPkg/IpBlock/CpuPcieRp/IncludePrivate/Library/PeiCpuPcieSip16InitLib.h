/** @file
  Header file for private PeiDxeSmmCpuPcieSip16InitLib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_CPU_PCIE_SIP16_INIT_LIB_H_
#define _PEI_CPU_PCIE_SIP16_INIT_LIB_H_

#include <CpuPcieConfig.h>

/**
  This function creates SIP16 Capability and Extended Capability List

  @param[in] RpIndex         Root Port index
  @param[in] RpBase          Root Port pci segment base address
  @param[in] CpuPcieRpConfig Pointer to a CPU_PCIE_CONFIG that provides the platform setting

**/
VOID
CpuPcieSip16InitCapabilityList (
  IN UINT32                           RpIndex,
  IN UINT64                           RpBase,
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG  *CpuPcieRpConfig
  );

/**
  CPU PCIe SIP16 PTM initialization

  @param[in]  RpBase                Root Port pci segment base address.
**/
VOID
EFIAPI
CpuPcieSip16PtmInit (
  IN  UINT64  RpBase
  );

/**
  CPU PCIe SIP16 Gen3 PresetToCoeff mapping

  @param[in] RpIndex              Root Port Index for which HW EQ is to be performed
**/
VOID
CpuPcieSip16Gen3PresetToCoeff (
  IN UINT8       RpIndex
  );

/**
  CPU PCIe SIP16 Gen4 PresetToCoeff mapping
**/
VOID
CpuPcieSip16Gen4PresetToCoeff (
  IN UINT8       RpIndex
  );

/**
  Configure Number of Fast Training Sequence ordered sets for common and Unique Clock

  @param[in] RpBase      Root Port pci segment base address
**/
VOID
CpuPcieSip16ConfigureNfts (
  UINT64 RpBase
  );

/**
  Program FIA Registers for SIP16

  @param[in] RpIndex     Root Port Index

  @retval                None
**/
VOID
CpuPcieSip16FiaFinalizeConfigurationAndLock (
  IN UINT8                      RpIndex
  );
#endif
