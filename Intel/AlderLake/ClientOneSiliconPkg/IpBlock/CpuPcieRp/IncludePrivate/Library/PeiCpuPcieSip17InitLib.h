/** @file
  Header file for private PeiDxeSmmCpuPcieSip17InitLib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_CPU_PCIE_SIP17_INIT_LIB_H_
#define _PEI_CPU_PCIE_SIP17_INIT_LIB_H_

#include <CpuPcieConfig.h>

/**
  Get RCRB BAR address

  @param[in] RpBase      Root Port pci segment base address

  @retval                     RCRB BAR address of this PCIe RP
**/
UINTN
CpuPcieGetRcrbBar (
  UINT64 RpBase
  );

/**
  This function creates SIP17 Capability and Extended Capability List

  @param[in] RpIndex         Root Port index
  @param[in] RpBase          Root Port pci segment base address
  @param[in] CpuPcieRpConfig Pointer to a CPU_PCIE_CONFIG that provides the platform setting

**/
VOID
CpuPcieSip17InitCapabilityList (
  IN UINT32                           RpIndex,
  IN UINT64                           RpBase,
  IN CONST CPU_PCIE_ROOT_PORT_CONFIG  *CpuPcieRpConfig
  );

/**
  CPU PCIe SIP17 PTM initialization for 2px16

  @param[in] RpIndex         Root Port Index
**/
VOID
EFIAPI
CpuPcieSip17Ptm2px16Init (
  IN UINT32      RpIndex
  );

/**
  CPU PCIe SIP17 PTM initialization for 1px8

  @param[in] RpIndex         Root Port Index
**/
VOID
EFIAPI
CpuPcieSip17Ptm1px8Init (
  IN UINT32      RpIndex
  );

/**
  CPU PCIe SIP17 Gen3 PresetToCoeff mapping

  @param[in] RpIndex              Root Port Index for which HW EQ is to be performed
**/
VOID
CpuPcieSip17Gen3PresetToCoeff (
  IN UINT8      RpIndex
  );

/**
  CPU PCIe SIP17 Gen4 PresetToCoeff mapping

  @param[in] RpIndex              Root Port Index for which HW EQ is to be performed
**/
VOID
CpuPcieSip17Gen4PresetToCoeff (
  IN UINT8      RpIndex
  );

/**
  CPU PCIe SIP17 Gen5 PresetToCoeff mapping

  @param[in] RpIndex     Root Port Index
**/
VOID
CpuPcieSip17Gen5PresetToCoeff (
  IN UINT32      RpIndex
  );

/**
  Set CPU PCIe SIP17 Gen5 Preset entry.

  @param[in] RpIndex     Root Port Index
  @param[in] ListEntry   ListEntry (0-9)
  @param[in] Cm          C-1
  @param[in] Cp          C+1
**/
VOID
CpuPcieSip17SetGen5Presets (
  UINT32 RpIndex,
  UINT32 ListEntry,
  UINT32 Cm,
  UINT32 Cp
  );

/**
  Program CPU PCIe SIP17 HW EQ Local Tx Override for Gen5

  @param[in] RpIndex         Root Port Index
  @param[in] LaneIndex       Max Lane for each RootPort
  @param[in] PreCursor       Pre-Cursor Coefficient Override
  @param[in] PostCursor      Post-Cursor Coefficient Override
**/
VOID
CpuPcieSip17SetGen5TxOverride(
  UINT32                  RpIndex,
  UINT32                  LaneIndex,
  UINT32                  PreCursor,
  UINT32                  PostCursor
  );

/**
  Dump CPU PCIe SIP17 Gen5 HW EQ Registers.
  @param[in] RpIndex       Root Port Index
**/
VOID
CpuPcieSip17Gen5DumpHwEqRegs (
  UINT32 RpIndex
  );

/**
  Configures CPU PCIe SIP17 rootport for hardware Gen5 link equalization.
  @param[in] RpIndex              Root Port Index for which HW EQ is to be performed
  @param[in] RpBase               Root Port Base
  @param[in] Params               Equalization parameters
**/
VOID
CpuPcieSip17DoGen5HardwareEq (
  UINT32                      RpIndex,
  UINT64                      RpBase,
  UINT32                      *DoGen5HwEqRpMask,
  CONST CPU_PCIE_CONFIG       *Params
  );

/**
  Dump CPU PCIe SIP17 HW EQ Coefficients for Gen5.

  @param[in] RpIndex       Root Port Index
  @param[in] ListEntry   ListEntry (0-5)
**/
VOID
CpuPcieSip17DumpHwEqGen5 (
  UINT32 RpIndex,
  UINT32 ListEntry
  );

/**
  Configure Number of Fast Training Sequence ordered sets for common and Unique Clock

  @param[in] RpIndex      Root Port Index
**/
VOID
CpuPcieSip17ConfigureNfts (
  UINT32 RpIndex
  );


/**
  Get CPU PCIe SIP17 PX32EQCFG1 value

  @param[in] RpIndex            Root Port Index

  @retval   PX32EQCFG1 value
**/
UINT32
CpuPcieSip17GetPx32EqCfg1 (
  UINT32                 RpIndex
  );

/**
  Get CPU PCIe SIP17 B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAED bit map

  @retval   B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAED bit map
**/
UINT32
CpuPcieSip17GetGen5HaedBitMap (
 VOID
  );

/**
  Program Clock gating registers for SIP17

  @param[in] RpIndex      Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17DynamicClockGating (
  UINT32 RpIndex
  );

/**
  CPU PCIe SIP17 Reset Policy initialization

  @param[in] RpIndex      Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17ResetPolicyInit (
 UINT32 RpIndex
  );

/**
  CPU PCIe Completion Timer Timeout Policy

  @param[in]  RpIndex         Root Port Index
**/
VOID
EFIAPI
CpuPcieSip17CompletionTimerTimeout (
  IN  UINT32  RpIndex
  );

/**
  CPU PCIe SIP17 Error Injection Disable Bit

  @param[in]  RpBase                Root Port pci segment base address.
**/
VOID
CpuPcieSip17SetErrorInjectionDisable (
  IN UINT64 RpBase
  );

/**
  Program Rx Master Cycle Decode Registers for SIP17

  @param[in] RpIndex         Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17RxMasterCycleDecode (
  UINT32 RpIndex
  );

/**
  CPU PCIe SIP17 PHY initialization

  @param[in] RpIndex         Root Port Index
**/
VOID
CpuPcieSip17PhyInit (
 UINT32 RpIndex
  );

/**
  Program Power Down Mapping Registers for SIP17

  @param[in] RpIndex         Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17PowerDownMapping (
  IN UINT32 RpIndex
  );

  /**
  Program Squelch Settings for ASPM L0s Support for SIP17

  @param[in] RpIndex         Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17SquelchSettingAspm (
  UINT32 RpIndex
  );

/**
  Program Phy Recal Request for SIP17

  @param[in] RpIndex         Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17PhyRecalRequest (
  UINT32 RpIndex
  );

/**
  Program FIA Registers for SIP17

  @param[in] RpIndex     Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17FiaFinalizeConfigurationAndLock (
  IN UINT8                      RpIndex
  );

/**
  Program Power Management Registers for SIP17

  @param[in]  RpIndex         Root Port Index

  @retval                None

**/
VOID
CpuPcieSip17PowerManagement(
  IN UINT32                      RpIndex
);

/**
  Program Power Management Registers for SIP17

  @param[in] RpIndex      Root Port Index

  @retval                None
**/
VOID
CpuPcieSip17EbThreshold (
  IN UINT32 RpIndex
  );
#endif
