/** @file
  ME HECI reset PEI lib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi.h>
#include <PiPei.h>
#include <Library/DebugLib.h>
#include <Library/TimerLib.h>
#include <Library/IoLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/ResetSystemLib.h>
#include <Ppi/PlatformSpecificResetHandler.h>
#include <Library/BaseMemoryLib.h>
#include <CoreBiosMsg.h>
#include <Library/PmcLib.h>
#include <Library/PeiMeLib.h>
#include <Library/HobLib.h>
#include <Library/PmcPrivateLib.h>
#include <Ppi/HeciPpi.h>
#include <Register/PchRegsLpc.h>

/**
  Print reset message for debug build readability
**/
VOID
EFIAPI
PrintResetMessage (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "******************************\n"));
  DEBUG ((DEBUG_INFO, "**    SYSTEM REBOOT !!!     **\n"));
  DEBUG ((DEBUG_INFO, "******************************\n"));
}

/**
  Wait until CSME completes unconfiguration process.

  @retval EFI_SUCCESS             Unconfiguration not in progress
  @retval EFI_UNSUPPORTED         Unconfiguration error
  @retval EFI_TIMEOUT             Timeout exceeded
**/
EFI_STATUS
WaitForUnconfigFinish (
  VOID
  )
{
  EFI_STATUS Status;
  UINT8      UnconfigStatus;
  UINT8      Timeout;

  Timeout = ME_UNCONFIG_TIMEOUT;

  do {
    Status = PeiHeciGetUnconfigureStatus (&UnconfigStatus);
    if (EFI_ERROR (Status)) {
      return Status;
    }

    switch (UnconfigStatus) {
      case ME_UNCONFIG_IN_PROGRESS:
        MicroSecondDelay (1000000);
        break;

      case ME_UNCONFIG_NOT_IN_PROGRESS:
      case ME_UNCONFIG_FINISHED:
        return EFI_SUCCESS;

      default:
        return EFI_UNSUPPORTED;
    }
  } while (Timeout--);

  return EFI_TIMEOUT;
}

/**
  Sent HECI command to ME for GlobalReset

  @param[in]  ResetType         The type of reset to perform.
  @param[in]  ResetStatus       The status code for the reset.
  @param[in]  DataSize          The size, in bytes, of ResetData.
  @param[in]  ResetData         For a ResetType of EfiResetCold, EfiResetWarm, or
                                EfiResetShutdown the data buffer starts with a Null-terminated
                                string, optionally followed by additional binary data.
                                The string is a description that the caller may use to further
                                indicate the reason for the system reset.
                                For a ResetType of EfiResetPlatformSpecific the data buffer
                                also starts with a Null-terminated string that is followed
                                by an EFI_GUID that describes the specific type of reset to perform.
**/
VOID
EFIAPI
PeiMeResetHandlerCallback (
  IN EFI_RESET_TYPE           ResetType,
  IN EFI_STATUS               ResetStatus,
  IN UINTN                    DataSize,
  IN VOID                     *ResetData OPTIONAL
  )
{
  EFI_GUID                *GuidPtr;
  EFI_PEI_HOB_POINTERS    HobPtr;
  EFI_STATUS              Status;

  PrintResetMessage ();
  if (ResetType != EfiResetPlatformSpecific || (ResetData == NULL)) {
    return;
  }

  GuidPtr = (EFI_GUID *) ((UINT8 *) ResetData + DataSize - sizeof (EFI_GUID));
  if (!CompareGuid (GuidPtr, &gPchGlobalResetGuid)) {
    return;
  }

  //
  // PCH BIOS Spec Section 4.6 GPIO Reset Requirement
  //
  DEBUG ((DEBUG_INFO, "Enable CF9 Global Reset - PEI\n"));
  PmcEnableCf9GlobalReset ();

  HobPtr.Guid  = GetFirstGuidHob (&gMeDidSentHobGuid);
  if (HobPtr.Guid != NULL) {
    //
    // After sending DRAM Init Done to ME FW, please do the global reset through HECI.
    //
    Status = PeiHeciSendCbmResetRequest (CBM_RR_REQ_ORIGIN_BIOS_POST, CBM_HRR_GLOBAL_RESET);
    if (EFI_ERROR (Status)) {
      //
      // Check if ME is undergoing unconfiguration. If yes, then wait for the queued message to be executed.
      // Platform should reset during function execution. If not, attempt to do CF9 reset
      //
      WaitForUnconfigFinish ();
    }
    //
    // Allow 1 second delay so CSME can perform the global reset within this period.
    //
    MicroSecondDelay (1000000);
  }
  //
  // In case reset through HECI fails, continue with CF9 reset
  //
  IoWrite8 (R_PCH_IO_RST_CNT, V_PCH_IO_RST_CNT_FULLRESET);
}


/**
  This function register reset handler ppi for ME in PEI

  @param[in]  PeiServices      Pointer to PEI Services Table.
  @param[in]  NotifyDescriptor Pointer to the descriptor for the Notification event that
                               caused this function to execute.
  @param[in]  Interface        Pointer to the PPI data associated with this function.

  @retval     EFI_SUCCESS  The function completes successfully
  @retval     others

**/
EFI_STATUS
EFIAPI
MeResetHandlerCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Interface
  )
{
  EFI_STATUS                                Status;
  EDKII_PLATFORM_SPECIFIC_RESET_HANDLER_PPI *ResetHandlerPpi;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));

  Status = PeiServicesLocatePpi (
             &gEdkiiPlatformSpecificResetHandlerPpiGuid,
             0,
             NULL,
             (VOID **) &ResetHandlerPpi
             );
  if (!EFI_ERROR (Status)) {
    ResetHandlerPpi->RegisterResetNotify (ResetHandlerPpi, PeiMeResetHandlerCallback);
  }

  return EFI_SUCCESS;
}


GLOBAL_REMOVE_IF_UNREFERENCED EFI_PEI_NOTIFY_DESCRIPTOR  mResetHandlerReady = {
  EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
  &gEdkiiPlatformSpecificResetHandlerPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) MeResetHandlerCallback
};

/**
  Install ME call back function for rest2 ppi.

**/
VOID
RegisterMeReset (
  VOID
  )
{
  EFI_STATUS    Status;
  HECI_PPI      *HeciPpi;

  DEBUG ((DEBUG_INFO, "%a () Start\n", __FUNCTION__));
  Status = PeiServicesLocatePpi (
             &gHeciPpiGuid,
             0,
             NULL,
             (VOID **) &HeciPpi
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "HECI PPI is prerequisite for ME reset, install it\n"));
    Status = InstallHeciPpi ();
  }

  if (!EFI_ERROR (Status)) {
    PeiServicesNotifyPpi (&mResetHandlerReady);
  }
}

