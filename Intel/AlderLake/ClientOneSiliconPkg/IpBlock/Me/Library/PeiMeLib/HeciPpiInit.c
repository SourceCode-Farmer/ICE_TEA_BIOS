/** @file
  This is a library to provide Heci PPI Service.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Ppi/HeciPpi.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/PostCodeLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/HeciInitLib.h>
#include <Library/MeTypeLib.h>
#include <Register/HeciRegs.h>

//
// Function Declarations
//
static HECI_PPI               mHeciPpi = {
  HeciSendwAck,
  HeciReceive,
  HeciSend,
  HeciInitialize,
  HeciGetMeStatus,
  HeciGetMeMode
};

static EFI_PEI_PPI_DESCRIPTOR mInstallHeciPpi = {
  EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
  &gHeciPpiGuid,
  &mHeciPpi
};

/**
  Install Heci PPIs needed in PEI phase

  @retval EFI_SUCCESS             The function completed successfully.
  @retval EFI_DEVICE_ERROR        ME FPT is bad
**/
EFI_STATUS
InstallHeciPpi (
  VOID
  )
{
  EFI_STATUS         Status;
  UINT64             HeciBaseAddress;
  HECI_FWS_REGISTER  MeFirmwareStatus;

  DEBUG ((DEBUG_INFO, "ME-BIOS: HECI PPI Entry.\n"));
  PostCode (0xE03);

  ///
  /// Check for HECI device present and ME FPT Bad
  ///
  HeciBaseAddress = PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, 0);
  if (PciSegmentRead16 (HeciBaseAddress + PCI_DEVICE_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_ERROR, "ME-BIOS: HECI PPI Exit - Error by HECI device is disabled.\n"));
    PostCode (0xE83);
    Status = EFI_DEVICE_ERROR;
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  MeFirmwareStatus.ul = PciSegmentRead32 (HeciBaseAddress + R_ME_HFS);
  if (MeFirmwareStatus.r.FptBad && !MeTypeIsSps ()) {
    DEBUG ((DEBUG_ERROR, "ME-BIOS: HECI PPI Exit - Error by HECI device error.\n"));
    PostCode (0xE83);
    Status = EFI_DEVICE_ERROR;
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  ///
  /// Heci Ppi should be installed always due to DID message might be required still.
  /// Unsupported messages requested will be blocked when utilize Heci Ppi
  ///
  Status = PeiServicesInstallPpi (&mInstallHeciPpi);
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "ME-BIOS: HECI PPI Exit - Success.\n"));
    PostCode (0xE23);
  } else {
    DEBUG ((DEBUG_ERROR, "ME-BIOS: HECI PPI Exit - Error by install HeciPpi fail, Status: %r\n", Status));
    ASSERT_EFI_ERROR (Status);
    PostCode (0xEA3);
  }

  return Status;
}
