/** @file
  Framework PEIM to HECI.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2008 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/HeciPpi.h>
#include <Ppi/HeciControlPpi.h>
#include <Ppi/SiPolicy.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/PeiMeLib.h>
#include <Library/PostCodeLib.h>
#include <Library/PmcLib.h>
#include <Library/HeciInitLib.h>
#include <Register/PmcRegs.h>
#include <Library/IoLib.h>

//
// Function Implementations
//

/**
  Initialize MMIO BAR address for HECI devices
  For SPS HeciControl is used (it has self initialization)

  @retval EFI_SUCCESS             HECI devices initialize successfully
  @retval others                  Error occur
**/
EFI_STATUS
PeiHeciDevicesInit (
  VOID
  )
{
  SI_PREMEM_POLICY_PPI *SiPreMemPolicyPpi;
  EFI_STATUS           Status;
  ME_PEI_PREMEM_CONFIG *MePeiPreMemConfig;
  BOOLEAN              HeciCommunication2;
  HECI_CONTROL         *HeciControl;
  VOID                 *HobPtr;
  UINT32               RegisterVal32;

  Status = PeiServicesLocatePpi (&gHeciControlPpiGuid, 0, NULL, (VOID**) &HeciControl);
  if (!EFI_ERROR (Status)) {
    // HeciControl will initialize HECI
    return EFI_SUCCESS;
  }

  ///
  /// Get Policy settings through the SiPreMemPolicy PPI
  ///
  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicyPpi
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot locate gSiPreMemPolicyPpiGuid, Status = %r\n", Status));
    return Status;
  }

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gMePeiPreMemConfigGuid, (VOID *) &MePeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  MeDeviceInit (HECI1, MePeiPreMemConfig->Heci1BarAddress, 0);
  MeDeviceInit (HECI2, MePeiPreMemConfig->Heci2BarAddress, 0);
  MeDeviceInit (HECI3, MePeiPreMemConfig->Heci3BarAddress, 0);

  ///
  /// Determine if ME devices should be Enabled/Disable and Program Subsystem IDs if Enabled
  /// Zero in Bit x enables the device
  ///
  HeciCommunication2 = MeHeci2Enabled ();

    DEBUG ((DEBUG_INFO, "HeciEnable\n"));
    HeciEnable ();
    HeciInitialize (HECI1_DEVICE);

  if (HeciCommunication2) {
    DEBUG ((DEBUG_INFO, "Heci2Enable\n"));
    Heci2Enable ();
  } else {
    DEBUG ((DEBUG_INFO, "Heci2Disable\n"));
    Heci2Disable ();
  }

    DEBUG ((DEBUG_INFO, "Heci3Enable\n"));
    Heci3Enable ();
  if (MePeiPreMemConfig->KtDeviceEnable) {
    DEBUG ((DEBUG_INFO, "SolEnable\n"));
    SolEnable ();
  } else {
    DEBUG ((DEBUG_INFO, "SolDisable\n"));
    SolDisable ();
  }

  RegisterVal32 = MmioRead32 ((UINTN) (PmcGetPwrmBase () + R_PMC_PWRM_GEN_PMCON_A));
  HobPtr = BuildGuidDataHob (&gMeSavedPmconHobGuid, &RegisterVal32, sizeof (RegisterVal32));
  ASSERT (HobPtr != NULL);

  return Status;
}

/**
  Internal function performing PM register initialization for Me
**/
VOID
MePmInit (
  VOID
  )
{
  ///
  /// Before system memory initialization, BIOS should check the wake status to determine if Intel Management Engine
  /// has reset the system while the host was in a sleep state. If platform was not in a sleep state
  /// BIOS should ensure a non-sleep exit path is taken. One way to accomplish this is by forcing
  /// an S5 exit path by the BIOS.
  ///
  if (PmcGetSleepTypeAfterWake () == PmcNotASleepState) {
    PmcSetSleepState (PmcS5SleepState);
    DEBUG ((DEBUG_INFO, "MePmInit () - Force an S5 exit path.\n"));
  }
}

