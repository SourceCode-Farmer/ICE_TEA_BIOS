/** @file
  Register names for PCIe SIP specific registers

  Conventions:

  - Register definition format:
  Prefix_[GenerationName]_[ComponentName]_SubsystemName_RegisterSpace_RegisterName
  - Prefix:
  Definitions beginning with "R_" are registers
  Definitions beginning with "B_" are bits within registers
  Definitions beginning with "V_" are meaningful values within the bits
  Definitions beginning with "S_" are register size
  Definitions beginning with "N_" are the bit position
  - [GenerationName]:
  Three letter acronym of the generation is used (e.g. SKL,KBL,CNL etc.).
  Register name without GenerationName applies to all generations.
  - [ComponentName]:
  This field indicates the component name that the register belongs to (e.g. PCH, SA etc.)
  Register name without ComponentName applies to all components.
  Register that is specific to -H denoted by "_PCH_H_" in component name.
  Register that is specific to -LP denoted by "_PCH_LP_" in component name.
  - SubsystemName:
  This field indicates the subsystem name of the component that the register belongs to
    (e.g. PCIE, USB, SATA, GPIO, PMC etc.).
  - RegisterSpace:
  MEM - MMIO space register of subsystem.
  IO  - IO space register of subsystem.
  PCR - Private configuration register of subsystem.
  CFG - PCI configuration space register of subsystem.
  - RegisterName:
  Full register name.

  @copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary    and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PCIE_SIP_RP_REGS_H_
#define _PCIE_SIP_RP_REGS_H_

#include <PcieRegs.h>

#define R_PCIE_CFG_CLIST                              0x40
#define R_PCIE_CFG_XCAP                               (R_PCIE_CFG_CLIST + R_PCIE_XCAP_OFFSET)
#define R_PCIE_CFG_DCAP                               (R_PCIE_CFG_CLIST + R_PCIE_DCAP_OFFSET)
#define R_PCIE_CFG_DCTL                               (R_PCIE_CFG_CLIST + R_PCIE_DCTL_OFFSET)
#define R_PCIE_CFG_DSTS                               (R_PCIE_CFG_CLIST + R_PCIE_DSTS_OFFSET)
#define R_PCIE_CFG_LCAP                               (R_PCIE_CFG_CLIST + R_PCIE_LCAP_OFFSET)
#define B_PCIE_CFG_LCAP_PN                            0xFF000000
#define N_PCIE_CFG_LCAP_PN                            24
#define R_PCIE_CFG_LCTL                               (R_PCIE_CFG_CLIST + R_PCIE_LCTL_OFFSET)
#define R_PCIE_CFG_LSTS                               (R_PCIE_CFG_CLIST + R_PCIE_LSTS_OFFSET)
#define R_PCIE_CFG_SLCAP                              (R_PCIE_CFG_CLIST + R_PCIE_SLCAP_OFFSET)
#define R_PCIE_CFG_SLCTL                              (R_PCIE_CFG_CLIST + R_PCIE_SLCTL_OFFSET)
#define R_PCIE_CFG_SLSTS                              (R_PCIE_CFG_CLIST + R_PCIE_SLSTS_OFFSET)
#define R_PCIE_CFG_RCTL                               (R_PCIE_CFG_CLIST + R_PCIE_RCTL_OFFSET)
#define R_PCIE_CFG_RSTS                               (R_PCIE_CFG_CLIST + R_PCIE_RSTS_OFFSET)
#define R_PCIE_CFG_DCAP2                              (R_PCIE_CFG_CLIST + R_PCIE_DCAP2_OFFSET)

#define R_PCIE_CFG_DCTL2                              (R_PCIE_CFG_CLIST + R_PCIE_DCTL2_OFFSET)
#define B_PCIE_CFG_DCTL2_AEB                          BIT7
#define B_PCIE_CFG_DCTL2_ARE                          BIT6

#define R_PCIE_CFG_LCTL2                              (R_PCIE_CFG_CLIST + R_PCIE_LCTL2_OFFSET)
#define R_PCIE_CFG_LSTS2                              (R_PCIE_CFG_CLIST + R_PCIE_LSTS2_OFFSET)
#define R_PCIE_CFG_LCAP2                              (R_PCIE_CFG_CLIST + R_PCIE_LCAP2_OFFSET)

#define R_PCIE_CFG_MID                                0x80
#define S_PCIE_CFG_MID                                2
#define R_PCIE_CFG_MC                                 0x82
#define S_PCIE_CFG_MC                                 2
#define R_PCIE_CFG_MA                                 0x84
#define S_PCIE_CFG_MA                                 4
#define R_PCIE_CFG_MD                                 0x88
#define S_PCIE_CFG_MD                                 2

#define R_PCIE_CFG_SVCAP                              0x90
#define S_PCIE_CFG_SVCAP                              2
#define R_PCIE_CFG_SVID                               0x94
#define S_PCIE_CFG_SVID                               4
#define R_PCIE_CFG_SIP17_SVCAP                        0x98

#define R_PCIE_CFG_PMCAP                              0xA0
#define R_PCIE_CFG_PMCS                               (R_PCIE_CFG_PMCAP + R_PCIE_PMCS_OFFST)
#define B_PCIE_CFG_DCAP_MPS_MASK                      (BIT2 | BIT1 | BIT0)
#define B_PCIE_CFG_DCAP_MPS_OFFSET                    0
#define V_PCIE_CFG_DCAP_MPS_256                       0x1

#define B_PCIE_CFG_DCTL_MPS_MASK                      (BIT7 | BIT6 | BIT5)
#define B_PCIE_CFG_DCTL_MPS_OFFSET                    5
#define V_PCIE_CFG_DCTL_MPS_256                       0x1
#define B_PCIE_CFG_DCTL_URE                           BIT3
#define B_PCIE_CFG_DCTL_FEE                           BIT2
#define B_PCIE_CFG_DCTL_NFE                           BIT1
#define B_PCIE_CFG_DCTL_CEE                           BIT0

#define B_PCIE_CFG_LCAP_ASPMOC                        BIT22
#define B_PCIE_CFG_LCAP_EL1_MASK                      (BIT17 | BIT16 | BIT15)
#define B_PCIE_CFG_LCAP_EL1_OFFSET                    15
#define V_PCIE_CFG_LCAP_EL1                           0x6
#define V_PCIE_CFG_LCAP_EL1_8US_16US                  0x4                         ///< L1 Exit Latency = 8us to less than 16us
#define B_PCIE_CFG_LCAP_EL0_MASK                      (BIT14 | BIT13 | BIT12)
#define B_PCIE_CFG_LCAP_EL0_OFFSET                    12
#define V_PCIE_CFG_LCAP_EL0                           0x4
#define V_PCIE_CFG_LCAP_EL0_256NS_512NS               0x3                         ///< L0 Exit Latency = 256ns to less than 512ns
#define B_PCIE_CFG_LCAP_APMS_MASK                     (BIT11 | BIT10)
#define B_PCIE_CFG_LCAP_APMS_OFFSET                   10
#define V_PCIE_CFG_LCAP_APMS_DIS                      0x0
#define V_PCIE_CFG_LCAP_APMS_L0S                      0x1
#define V_PCIE_CFG_LCAP_APMS_L1                       0x2
#define V_PCIE_CFG_LCAP_APMS_L0SL1                    0x3
#define B_PCIE_CFG_LCAP_MLW                           0x3F0
#define B_PCIE_CFG_LCAP_MLS                           0xF

#define B_PCIE_CFG_LCTL_ES                            BIT7                        ///< Extended Synch
#define B_PCIE_CFG_LCTL_ASPM                          (BIT1 | BIT0)               ///< Active State Link PM Control
#define V_PCIE_CFG_LCTL_ASPM_DIS                      0x0                         ///< ASPM Disabled
#define V_PCIE_CFG_LCTL_ASPM_L0S                      0x1                         ///< L0s Entry Enabled
#define V_PCIE_CFG_LCTL_ASPM_L1                       0x2                         ///< L1 Entry Enabled
#define V_PCIE_CFG_LCTL_ASPM_L0SL1                    0x3                         ///< L0s and L1 Entry Enabled
#define B_PCIE_CFG_LCTL_RL                            BIT5                        ///< Link Retrain

#define B_PCIE_CFG_DCAP2_PX10BTRS                     BIT17
#define B_PCIE_CFG_DCAP2_PX10BTCS                     BIT16
#define B_PCIE_CFG_DCAP2_AC64BS                       BIT8
#define B_PCIE_CFG_DCAP2_AC32BS                       BIT7
#define B_PCIE_CFG_DCAP2_ARS                          BIT6

#define B_PCIE_CFG_DCTL2_PX10BTRE                     BIT11
#define B_PCIE_CFG_DCTL2_AEB                          BIT7
#define B_PCIE_CFG_DCTL2_ARE                          BIT6

#define B_PCIE_CFG_LCTL2_TLS                          0xF                         ///< Target Link Speed

#define R_PCIE_CFG_CMD                                0x4
#define B_PCIE_CFG_CMD_SEE                            BIT8

#define R_PCIE_CFG_BCTRL                              0x3E
#define B_PCIE_CFG_BCTRL_SE                           BIT1

#define R_PCIE_CFG_CCFG                               0xD0
#define B_PCIE_CFG_CCFG_UMRPD                         BIT26
#define B_PCIE_CFG_CCFG_UPMWPD                        BIT25
#define B_PCIE_CFG_CCFG_UPSD                          BIT24
#define B_PCIE_CFG_CCFG_UNSD                          BIT23
#define B_PCIE_CFG_CCFG_DCGEISMA                      BIT15
#define B_PCIE_CFG_CCFG_UNRD                          (BIT13 | BIT12)
#define N_PCIE_CFG_CCFG_UNRD                          12
#define B_PCIE_CFG_CCFG_UNRS                          (BIT6 | BIT5 | BIT4)
#define N_PCIE_CFG_CCFG_UNRS                          4
#define V_PCIE_CFG_CCFG_UNRS_128B                     0
#define V_PCIE_CFG_CCFG_UNRS_256B                     1
#define V_PCIE_CFG_CCFG_UNRS_64B                      7
#define B_PCIE_CFG_CCFG_UPRS                          (BIT2 | BIT1 | BIT0)
#define N_PCIE_CFG_CCFG_UPRS                          0
#define V_PCIE_CFG_CCFG_UPRS_128B                     0
#define V_PCIE_CFG_CCFG_UPRS_256B                     1
#define V_PCIE_CFG_CCFG_UPRS_64B                      7

#define R_PCIE_CFG_MPC2                               0xD4                        ///< Miscellaneous Port Configuration 2
#define B_PCIE_CFG_MPC2_L1SSESE                       BIT30
#define B_PCIE_CFG_MPC2_PLLWAIT                       (BIT26 | BIT25 | BIT24)     ///< PLL Wait
#define N_PCIE_CFG_MPC2_PLLWAIT                       24                          ///< PLL Wait
#define V_PCIE_CFG_MPC2_PLLWAIT_0US                   0x0                         ///< PLL Wait = 0us
#define V_PCIE_CFG_MPC2_PLLWAIT_5US                   0x1                         ///< PLL Wait = 5us
#define B_PCIE_CFG_MPC2_RUD                           BIT23                       ///< Recovery Upconfiguration Disable
#define B_PCIE_CFG_MPC2_GEN2PLLC                      BIT21                       ///< GEN2 PLL Coupling
#define B_PCIE_CFG_MPC2_GEN3PLLC                      BIT20                       ///< GEN3 PLL Coupling
#define B_PCIE_CFG_MPC2_DISPLLEWL1SE                  BIT16
#define B_PCIE_CFG_MPC2_ORCE                          (BIT15 | BIT14)             ///< Offset Re-Calibration Enable
#define N_PCIE_CFG_MPC2_ORCE                          14                          ///< Offset Re-Calibration Enable
#define V_PCIE_CFG_MPC2_ORCE_EN_GEN2_GEN3             1                           ///< Enable offset re-calibration for Gen 2 and Gen 3 data rate only.
#define V_PCIE_CFG_MPC2_ORCE_DIS                      0x0
#define B_PCIE_CFG_MPC2_PTNFAE                        BIT12
#define B_PCIE_CFG_MPC2_TLPF                          BIT9                        ///< Transaction Layer Packet Fast Transmit Mode
#define B_PCIE_CFG_MPC2_CAM                           BIT8                        ///< DMI Credit Allocated Update Mode
#define B_PCIE_CFG_MPC2_LSTP                          BIT6
#define B_PCIE_CFG_MPC2_IEIME                         BIT5
#define B_PCIE_CFG_MPC2_ASPMCOEN                      BIT4
#define B_PCIE_CFG_MPC2_ASPMCO                        (BIT3 | BIT2)
#define V_PCIE_CFG_MPC2_ASPMCO_DISABLED               0
#define V_PCIE_CFG_MPC2_ASPMCO_L0S                    (1 << 2)
#define V_PCIE_CFG_MPC2_ASPMCO_L1                     (2 << 2)
#define V_PCIE_CFG_MPC2_ASPMCO_L0S_L1                 (3 << 2)
#define B_PCIE_CFG_MPC2_EOIFD                         BIT1

#define R_PCIE_CFG_MPC                                0xD8
#define B_PCIE_CFG_MPC_PMCE                           BIT31

#define B_PCIE_CFG_MPC_HPCE                           BIT30
#define B_PCIE_CFG_MPC_MMBNCE                         BIT27
#define B_PCIE_CFG_MPC_P8XDE                          BIT26
#define B_PCIE_CFG_MPC_IRRCE                          BIT25
#define B_PCIE_CFG_MPC_SRL                            BIT23
#define B_PCIE_CFG_MPC_FCDL1E                         BIT21
#define B_PCIE_CFG_MPC_UCEL                           (BIT20 | BIT19 | BIT18)
#define N_PCIE_CFG_MPC_UCEL                           18
#define B_PCIE_CFG_MPC_CCEL                           (BIT17 | BIT16 | BIT15)
#define N_PCIE_CFG_MPC_CCEL                           15
#define V_PCIE_CFG_MPC_CCEL                           0x4
#define B_PCIE_CFG_MPC_PCIESD                         (BIT14 | BIT13)
#define N_PCIE_CFG_MPC_PCIESD                         13
#define V_PCIE_CFG_MPC_PCIESD_GEN1                    1
#define V_PCIE_CFG_MPC_PCIESD_GEN2                    2
#define V_PCIE_CFG_MPC_PCIESD_GEN3                    3
#define V_PCIE_CFG_MPC_PCIESD_GEN4                    4
#define B_PCIE_CFG_MPC_FCP                            (BIT6 | BIT5 | BIT4)
#define B_PCIE_CFG_MPC_FCP_OFFSET                     4
#define V_PCIE_CFG_MPC_FCP                            4
#define B_PCIE_CFG_MPC_MCTPSE                         BIT3
#define B_PCIE_CFG_MPC_BT                             BIT2
#define B_PCIE_CFG_MPC_HPME                           BIT1
#define N_PCIE_CFG_MPC_HPME                           1
#define B_PCIE_CFG_MPC_PMME                           BIT0

#define R_PCIE_CFG_SMSCS                              0xDC
#define B_PCIE_CFG_SMSCS_PMCS                         BIT31
#define S_PCIE_CFG_SMSCS                              4
#define N_PCIE_CFG_SMSCS_LERSMIS                      5
#define N_PCIE_CFG_SMSCS_HPLAS                        4
#define N_PCIE_CFG_SMSCS_HPPDM                        1

#define R_PCIE_CFG_SPR                                0xE0

#define R_PCIE_CFG_RPDCGEN                            0xE1           ///< Root Port Dynamic Clock Gate Enable
#define S_PCIE_CFG_RPDCGEN                            1
#define B_PCIE_CFG_RPDCGEN_RPSCGEN                    BIT7
#define B_PCIE_CFG_RPDCGEN_PTOCGE                     BIT6           ///< Partition/Trunk Oscillator Clock Gate Enable
#define B_PCIE_CFG_RPDCGEN_LCLKREQEN                  BIT5           ///< Link CLKREQ Enable
#define B_PCIE_CFG_RPDCGEN_BBCLKREQEN                 BIT4           ///< Backbone CLKREQ Enable
#define B_PCIE_CFG_RPDCGEN_SRDBCGEN                   BIT2           ///< Shared Resource Dynamic Backbone Clock Gate Enable
#define B_PCIE_CFG_RPDCGEN_RPDLCGEN                   BIT1           ///< Root Port Dynamic Link Clock Gate Enable
#define B_PCIE_CFG_RPDCGEN_RPDBCGEN                   BIT0           ///< Root Port Dynamic Backbone Clock Gate Enable

#define R_PCIE_CFG_RPPGEN                             0xE2           ///< Root Port Power Gating Enable
#define B_PCIE_CFG_RPPGEN_PTOTOP                      BIT6
#define B_PCIE_CFG_RPPGEN_SEOSCGE                     BIT4           ///< Sideband Endpoint Oscillator/Side Clock Gating Enable

#define R_PCIE_CFG_PWRCTL                             0xE8           ///< Power Control Register
#define B_PCIE_CFG_PWRCTL_DARECE                      BIT28
#define B_PCIE_CFG_PWRCTL_RSP                         BIT25          ///< RxElecIdle Sampling Policy
#define B_PCIE_CFG_PWRCTL_LIFECF                      BIT23
#define B_PCIE_CFG_PWRCTL_LTSSMRTC                    BIT20
#define B_PCIE_CFG_PWRCTL_WPDMPGEP                    BIT17
#define B_PCIE_CFG_PWRCTL_DLP                         BIT16
#define B_PCIE_CFG_PWRCTL_DBUPI                       BIT15          ///< De-skew Buffer Unload Pointer Increment
#define B_PCIE_CFG_PWRCTL_TXSWING                     BIT13
#define B_PCIE_CFG_PWRCTL_RPSEWL                      (BIT3 | BIT2)  ///< Root Port Squelch Exit Wait Latency
#define N_PCIE_CFG_PWRCTL_RPSEWL                      2              ///< Root Port Squelch Exit Wait Latency
#define V_PCIE_CFG_PWRCTL_RPSEWL_120NS                0x1            ///< Root Port Squelch Exit Wait Latency = 120 ns
#define B_PCIE_CFG_PWRCTL_RPL1SQPOL                   BIT1           ///< Root Port L1 Squelch Polling
#define B_PCIE_CFG_PWRCTL_RPDTSQPOL                   BIT0           ///< Root Port Detect Squelch Polling

#define R_PCIE_CFG_DC                                 0xEC
#define B_PCIE_CFG_DC_DCT1C                           BIT15
#define B_PCIE_CFG_DC_DCT0C                           BIT14
#define B_PCIE_CFG_DC_COM                             BIT13
#define B_PCIE_CFG_DC_PCIBEM                          BIT2

#define R_PCIE_CFG_IPCS                               0xF0
#define B_PCIE_CFG_IPCS_IMPS_OFFSET                   8
#define B_PCIE_CFG_IPCS_IMPS_MASK                     0x700
#define V_PCIE_CFG_IPCS_IMPS                          0x1
#define V_PCIE_CFG_IPCS_IMPS_64B                      7
#define V_PCIE_CFG_IPCS_IMPS_256B                     1

#define R_PCIE_CFG_PHYCTL2                            0xF5           ///< Physical Layer And AFE Control 2
#define B_PCIE_CFG_PHYCTL2_TDFT                       (BIT7 | BIT6)  ///< Transmit Datapath Flush Timer
#define B_PCIE_CFG_PHYCTL2_TXCFGCHGWAIT               (BIT5 | BIT4)  ///< Transmit Configuration Change Wait Time
#define N_PCIE_CFG_PHYCTL2_TXCFGCHGWAIT               4
#define B_PCIE_CFG_PHYCTL2_PXPG3PLLOFFEN              BIT1           ///< PCI Express GEN3 PLL Off Enable
#define B_PCIE_CFG_PHYCTL2_PXPG2PLLOFFEN              BIT0           ///< PCI Express GEN2 PLL Off Enable

#define R_PCIE_CFG_PHYCTL3                            0xF6           ///< Physical Layer And AFE Control 3 Register
#define B_PCIE_CFG_PHYCTL3_SQDIROVREN                 BIT2           ///< Squelch Direction Override Enable
#define B_PCIE_CFG_PHYCTL3_SQDIRCTRL                  BIT1           ///< Squelch Direction

#define R_PCIE_CFG_IOSFSBCS                           0xF7           ///< IOSF Sideband Control And Status
#define B_PCIE_CFG_IOSFSBCS_SCPTCGE                   BIT6           ///< Side Clock Partition/Trunk Clock Gating Enable
#define B_PCIE_CFG_IOSFSBCS_SIID                      (BIT3 | BIT2)  ///< IOSF Sideband Interface Idle Counter

#define R_PCIE_CFG_STRPFUSECFG                        0xFC
#define B_PCIE_CFG_STRPFUSECFG_SERM                   BIT29
#define B_PCIE_CFG_STRPFUSECFG_PXIP                   (BIT27 | BIT26 | BIT25 | BIT24)
#define N_PCIE_CFG_STRPFUSECFG_PXIP                   24
#define V_PCIE_CFG_STRPFUSECFG_PXIP                   0x0
#define B_PCIE_CFG_STRPFUSECFG_RPC_SIP17              (BIT16 | BIT15 | BIT14)
#define B_PCIE_CFG_STRPFUSECFG_RPC                    (BIT15 | BIT14)
#define V_PCIE_CFG_STRPFUSECFG_RPC_1_1_1_1            0
#define V_PCIE_CFG_STRPFUSECFG_RPC_2_1_1              1
#define V_PCIE_CFG_STRPFUSECFG_RPC_2_2                2
#define V_PCIE_CFG_STRPFUSECFG_RPC_4                  3
#define V_PCIE_CFG_STRPFUSECFG_RPC_4_SIP17            4
#define N_PCIE_CFG_STRPFUSECFG_RPC                    14
#define B_PCIE_CFG_STRPFUSECFG_LR                     BIT13
#define B_PCIE_CFG_STRPFUSECFG_MODPHYIOPMDIS          BIT9
#define B_PCIE_CFG_STRPFUSECFG_PLLSHTDWNDIS           BIT8
#define B_PCIE_CFG_STRPFUSECFG_STPGATEDIS             BIT7
#define B_PCIE_CFG_STRPFUSECFG_ASPMDIS                BIT6
#define B_PCIE_CFG_STRPFUSECFG_LDCGDIS                BIT5
#define B_PCIE_CFG_STRPFUSECFG_LTCGDIS                BIT4
#define B_PCIE_CFG_STRPFUSECFG_CDCGDIS                BIT3
#define B_PCIE_CFG_STRPFUSECFG_DESKTOPMOB             BIT2

//
// PCI Express Extended Capability Registers
//
#define R_PCIE_CFG_EXCAP_OFFSET                       0x100

#define R_PCIE_CFG_AECH                               0x100
#define B_PCIE_CFG_AECH_CID_MASK                      0xFFFF
#define B_PCIE_CFG_AECH_CID_OFFSET                    0
#define V_PCIE_CFG_AECH_CID                           0x1
#define B_PCIE_CFG_AECH_CV_MASK                       (BIT19 | BIT18 | BIT17 | BIT16)
#define B_PCIE_CFG_AECH_CV_OFFSET                     16
#define V_PCIE_CFG_AECH_CV                            0x1
#define B_PCIE_CFG_AECH_NCO_MASK                      0xFFF00000
#define B_PCIE_CFG_AECH_NCO_OFFSET                    20
#define V_PCIE_CFG_AECH_NCO                           0x280
#define V_PCIE_CFG_AEC_CV                             0x1
#define R_PCIE_CFG_UEM                                (R_PCIE_CFG_AECH + R_PCIE_EX_UEM_OFFSET) // Uncorrectable Error Mask
#define B_PCIE_CFG_UEM_URE                            BIT20          ///< Unsupported Request Error Mask
#define B_PCIE_CFG_UEM_CM                             BIT15          ///< Completer Abort Mask
#define B_PCIE_CFG_UEM_PT                             BIT12          ///< Poisoned TLP Mask

#define R_PCIE_CFG_EX_CES                             0x110 ///< Correctable Error Status
#define B_PCIE_CFG_EX_CES_BD                          BIT7  ///< Bad DLLP Status
#define B_PCIE_CFG_EX_CES_BT                          BIT6  ///< Bad TLP Status
#define B_PCIE_CFG_EX_CES_RE                          BIT0  ///< Receiver Error Status

#define R_PCIE_CFG_EX_PTMECH                          0x150 ///< PTM Extended Capability Header
#define V_PCIE_CFG_EX_PTM_CV                          0x1
#define R_PCIE_CFG_EX_PTMCAPR                         (R_PCIE_CFG_EX_PTMECH + R_PCIE_EX_PTMCAP_OFFSET)

#define R_PCIE_CFG_EX_L1SECH                          0x200 ///< L1 Sub-States Extended Capability Header
#define V_PCIE_CFG_EX_L1S_CV                          0x1
#define R_PCIE_CFG_EX_L1SCAP                          (R_PCIE_CFG_EX_L1SECH + R_PCIE_EX_L1SCAP_OFFSET)
#define R_PCIE_CFG_EX_L1SCTL1                         (R_PCIE_CFG_EX_L1SECH + R_PCIE_EX_L1SCTL1_OFFSET)
#define R_PCIE_CFG_EX_L1SCTL2                         (R_PCIE_CFG_EX_L1SECH + R_PCIE_EX_L1SCTL2_OFFSET)

#define R_PCIE_CFG_L1SCAP                             0x204
#define B_PCIE_CFG_L1SCAP_L1PSS                       BIT4
#define B_PCIE_CFG_L1SCAP_AL11S                       BIT3
#define B_PCIE_CFG_L1SCAP_AL12S                       BIT2
#define B_PCIE_CFG_L1SCAP_PPL11S                      BIT1
#define B_PCIE_CFG_L1SCAP_PPL12S                      BIT0

#define R_PCIE_CFG_L1SCTL2                            0x20C ///< L1 Sub-States Control 2
#define B_PCIE_CFG_L1SCTL2_POWT                       BIT3
#define B_PCIE_CFG_L1SCTL2_POWT_OFFSET                3
#define B_PCIE_CFG_L1SCTL2_POWT_MASK                  0x000000F8
#define V_PCIE_CFG_L1SCTL2_POWT                       0x5
#define B_PCIE_CFG_L1SCTL2_TPOS                       BIT0
#define B_PCIE_CFG_L1SCTL2_TPOS_OFFSET                0
#define B_PCIE_CFG_L1SCTL2_TPOS_MASK                  0x00000003
#define V_PCIE_CFG_L1SCTL2_TPOS                       0x2

#define R_PCIE_CFG_EX_ACSECH                          0x220 ///< ACS Extended Capability Header
#define V_PCIE_CFG_EX_ACS_CV                          0x1
#define R_PCIE_CFG_EX_ACSCAPR                         (R_PCIE_CFG_EX_ACSECH + R_PCIE_EX_ACSCAPR_OFFSET)

#define R_PCIE_CFG_VCCH                               0x280
#define B_PCIE_CFG_VCCH_CID_MASK                      0xFFFF
#define B_PCIE_CFG_VCCH_CID_OFFSET                    0
#define V_PCIE_CFG_VCCH_CID_3_VC                      0x2
#define B_PCIE_CFG_VCCH_CV_MASK                       (BIT19 | BIT18 | BIT17 | BIT16)
#define B_PCIE_CFG_VCCH_CV_OFFSET                     16
#define V_PCIE_CFG_VCCH_CV_3_VC                       0x1
#define B_PCIE_CFG_VCCH_NCO_MASK                      0xFFF00000
#define B_PCIE_CFG_VCCH_NCO_OFFSET                    20
#define V_PCIE_CFG_VCCH_NCO                           0xA30

#define R_PCIE_CFG_PVCCR1                             0x284
#define B_PCIE_CFG_PVCCR1_EVCC_MASK                   0x7
#define B_PCIE_CFG_PVCCR1_EVCC_OFFSET                 0
#define V_PCIE_CFG_PVCCR1_EVCC_2_VC                   0x1
#define V_PCIE_CFG_PVCCR1_EVCC_3_VC                   0x2

#define R_PCIE_CFG_V0VCRC                             0x290
#define B_PCIE_CFG_V0VCRC_MTS_MASK                    0x7F0000

#define R_PCIE_CFG_VC0CTL                             0x294
#define B_PCIE_CFG_VC0CTL_TVM_MASK                    0xFE
#define B_PCIE_CFG_VC0CTL_TVM_OFFSET                  1
#define V_PCIE_CFG_VC0CTL_TVM_NO_VC                   0x7F
#define B_PCIE_CFG_VC0CTL_ETVM_MASK                   0x3F
#define B_PCIE_CFG_VC0CTL_ETVM_OFFSET                 10
#define V_PCIE_CFG_VC0CTL_TVM_EN_VC                   0x1F
#define V_PCIE_CFG_VC0CTL_ETVM_MASK                   0xFC00

#define R_PCIE_CFG_V0STS                              0x29A

#define R_PCIE_CFG_V1VCRC                             0x29C
#define B_PCIE_CFG_V1VCRC_MTS_MASK                    0x7F0000

#define R_PCIE_CFG_VC1CTL                             0x2A0
#define B_PCIE_CFG_VC1CTL_TVM_MASK                    0xFE
#define B_PCIE_CFG_VC1CTL_TVM                         BIT1
#define V_PCIE_CFG_VC1CTL_TVM                         0x7F
#define B_PCIE_CFG_VC1CTL_TVM_OFFSET                  1
#define V_PCIE_CFG_VC1CTL_TVM_EN_VC                   0x60
#define B_PCIE_CFG_VC1CTL_ID_MASK                     0xF000000
#define B_PCIE_CFG_VC1CTL_ID_OFFSET                   24
#define V_PCIE_CFG_VC1CTL_ID_ONE                      1
#define B_PCIE_CFG_VC1CTL_ETVM_MASK                   0xFC00
#define B_PCIE_CFG_VC1CTL_EN_OFFSET                   0x80000000
#define B_PCIE_CFG_VC1CTL_EN                          BIT31

#define R_PCIE_CFG_VC1STS                             0x2A6
#define B_PCIE_CFG_VC1STS_NP                          BIT1

#define R_PCIE_CFG_VC0STS                             0x2A9
#define B_PCIE_CFG_VC0STS_NP                          BIT1

#define R_PCIE_CFG_VMCTL                              0x2B0
#define B_PCIE_CFG_VMCTL_TVM_MASK                     0xFE
#define B_PCIE_CFG_VMCTL_TVM                          BIT7
#define B_PCIE_CFG_VMCTL_ID_MASK                      0xF000000
#define B_PCIE_CFG_VMCTL_ID_OFFSET                    24
#define V_PCIE_CFG_VMCTL_ID_TWO                       7
#define B_PCIE_CFG_VMCTL_EN_MASK                      0x80000000
#define B_PCIE_CFG_VMCTL_EN                           BIT31

#define R_PCIE_CFG_VMSTS                              0x2B6

#define R_PCIE_CFG_PCIERTP1                           0x300
#define B_PCIE_CFG_PCIERTP1_G2X1_MASK                 (BIT23 | BIT22 | BIT21 | BIT20)
#define B_PCIE_CFG_PCIERTP1_G2X1_OFFSET               20
#define V_PCIE_CFG_PCIERTP1_G2X1                      0xB
#define B_PCIE_CFG_PCIERTP1_G2X2_MASK                 (BIT19 | BIT18 | BIT17 | BIT16)
#define B_PCIE_CFG_PCIERTP1_G2X2_OFFSET               16
#define V_PCIE_CFG_PCIERTP1_G2X2                      0x7
#define B_PCIE_CFG_PCIERTP1_G2X4_MASK                 (BIT15 | BIT14 | BIT13 | BIT12)
#define B_PCIE_CFG_PCIERTP1_G2X4_OFFSET               12
#define V_PCIE_CFG_PCIERTP1_G2X4                      0xE
#define B_PCIE_CFG_PCIERTP1_G1X1_MASK                 (BIT11 | BIT10 | BIT9 | BIT8)
#define B_PCIE_CFG_PCIERTP1_G1X1_OFFSET               8
#define V_PCIE_CFG_PCIERTP1_G1X1                      0x5
#define B_PCIE_CFG_PCIERTP1_G1X2_MASK                 (BIT7 | BIT6 | BIT5 | BIT4)
#define B_PCIE_CFG_PCIERTP1_G1X2_OFFSET               4
#define V_PCIE_CFG_PCIERTP1_G1X2                      0xB
#define B_PCIE_CFG_PCIERTP1_G1X4_MASK                 (BIT3 | BIT2 | BIT1 | BIT0)
#define B_PCIE_CFG_PCIERTP1_G1X4_OFFSET               0
#define V_PCIE_CFG_PCIERTP1_G1X4                      0xB

#define R_PCIE_CFG_PCIERTP2                           0x304

#define R_PCIE_CFG_G4L0SCTL                           0x310
#define B_PCIE_CFG_G4L0SCTL_G4ASL0SPL_MASK            0xFF000000
#define B_PCIE_CFG_G4L0SCTL_G4ASL0SPL_OFFSET          24
#define V_PCIE_CFG_G4L0SCTL_G4ASL0SPL                 0x28
#define B_PCIE_CFG_G4L0SCTL_G4L0SIC_MASK              (BIT23 | BIT22)
#define B_PCIE_CFG_G4L0SCTL_G4L0SIC_OFFSET            22
#define V_PCIE_CFG_G4L0SCTL_G4L0SIC                   0x3
#define B_PCIE_CFG_G4L0SCTL_G4UCNFTS_MASK             0xFF00
#define B_PCIE_CFG_G4L0SCTL_G4UCNFTS_OFFSET           8
#define V_PCIE_CFG_G4L0SCTL_G4UCNFTS                  0x80
#define B_PCIE_CFG_G4L0SCTL_G4CCNFTS_MASK             0xFF
#define B_PCIE_CFG_G4L0SCTL_G4CCNFTS_OFFSET           0
#define V_PCIE_CFG_G4L0SCTL_G4CCNFTS                  0x5B

#define R_PCIE_CFG_PCIENFTS                           0x314
#define B_PCIE_CFG_PCIENFTS_G2UCNFTS_MASK             0xFF000000
#define B_PCIE_CFG_PCIENFTS_G2UCNFTS_OFFSET           24
#define V_PCIE_CFG_PCIENFTS_G2UCNFTS                  0x7E
#define B_PCIE_CFG_PCIENFTS_G2CCNFTS_MASK             0xFF0000
#define B_PCIE_CFG_PCIENFTS_G2CCNFTS_OFFSET           16
#define V_PCIE_CFG_PCIENFTS_G2CCNFTS                  0x5B
#define B_PCIE_CFG_PCIENFTS_G1UCNFTS_MASK             0xFF00
#define B_PCIE_CFG_PCIENFTS_G1UCNFTS_OFFSET           8
#define V_PCIE_CFG_PCIENFTS_G1UCNFTS                  0x3F
#define B_PCIE_CFG_PCIENFTS_G1CCNFTS_MASK             0xFF
#define B_PCIE_CFG_PCIENFTS_G1CCNFTS_OFFSET           0
#define V_PCIE_CFG_PCIENFTS_G1CCNFTS                  0x2C

#define R_PCIE_CFG_PCIEL0SC                           0x318          ///< PCI Express L0s Control
#define B_PCIE_CFG_PCIEL0SC_G2ASL0SPL                 0xFF000000     ///< Gen2 Active State L0s Preparation Latency
#define N_PCIE_CFG_PCIEL0SC_G2ASL0SPL                 24             ///< Gen2 Active State L0s Preparation Latency
#define B_PCIE_CFG_PCIEL0SC_G1ASL0SPL                 0x00FF0000     ///< Gen1 Active State L0s Preparation Latency
#define N_PCIE_CFG_PCIEL0SC_G1ASL0SPL                 16             ///< Gen1 Active State L0s Preparation Latency

#define R_PCIE_CFG_PCIECFG2                           0x320          ///< PCI Express Configuration 2 Register
#define B_PCIE_CFG_PCIECFG2_LBWSSTE                   BIT30          ///< Low Bandwidth Squelch Settling Timer Enable
#define B_PCIE_CFG_PCIECFG2_RLLG3R                    BIT27          ///< Reset Link Layer In GEN3 Recovery
#define B_PCIE_CFG_PCIECFG2_CROAOV                    BIT24          ///< Completion Relaxed Ordering Attribute Override Value
#define B_PCIE_CFG_PCIECFG2_CROAOE                    BIT23          ///< Completion Relaxed Ordering Attribute Override Enable
#define B_PCIE_CFG_PCIECFG2_CRSREN                    BIT22          ///< Completion Retry Status Replay Enable
#define B_PCIE_CFG_PCIECFG2_PMET_MASK                 (BIT21 | BIT20)
#define B_PCIE_CFG_PCIECFG2_PMET_OFFSET               20
#define V_PCIE_CFG_PCIECFG2_PMET                      0x1
#define B_PCIE_CFG_PCIECFG2_DTCA                      BIT4

#define R_PCIE_CFG_PCIEDBG                            0x324          ///< PCI Express Debug And Configuration Resgister
#define B_PCIE_CFG_PCIEDBG_USSP                       BIT26 | BIT27  ///< Un-Squelch Sampling Period
#define N_PCIE_CFG_PCIEDBG_USSP_OFFSET                26             ///< Un-Squelch Sampling Period offset
#define V_PCIE_CFG_PCIEDBG_USSP_32NS                  2              ///< 32ns
#define V_PCIE_CFG_PCIEDBG_USSP_16NS                  1              ///< 16ns
#define B_PCIE_CFG_PCIEDBG_LGCLKSQEXITDBTIMERS        BIT24          ///< Link Clock Domain Squelch Exit Debounce Timers
#define V_PCIE_CFG_PCIEDBG_LGCLKSQEXITDBTIMERS_0NS    0
#define N_PCIE_CFG_PCIEDBG_LGCLKSQEXITDBTIMERS_OFFSET 24
#define B_PCIE_CFG_PCIEDBG_CTONFAE                    BIT14
#define B_PCIE_CFG_PCIEDBG_LDSWQRP                    BIT13
#define B_PCIE_CFG_PCIEDBG_SQOL0                      BIT7           ///< Squelch OFF in L0
#define B_PCIE_CFG_PCIEDBG_SPCE                       BIT5           ///< Squelch Propagation Control Enable
#define B_PCIE_CFG_PCIEDBG_DTCA                       BIT4           ///< Disable TC Aliasing

#define R_PCIE_CFG_PCIESTS1                              0x328
#define B_PCIE_CFG_PCIESTS1_LTSMSTATE                    0xFF000000
#define N_PCIE_CFG_PCIESTS1_LTSMSTATE                    24
#define V_PCIE_CFG_PCIESTS1_LTSMSTATE_DETRDY             0x01
#define V_PCIE_CFG_PCIESTS1_LTSMSTATE_DETRDYECINP1CG     0x0E
#define V_PCIE_CFG_PCIESTS1_LTSMSTATE_L0                 0x33
#define V_PCIE_CFG_PCIESTS1_LTSMSTATE_DISWAIT            0x5E
#define V_PCIE_CFG_PCIESTS1_LTSMSTATE_DISWAITPG          0x60
#define V_PCIE_CFG_PCIESTS1_LTSMSTATE_RECOVERYSPEEDREADY 0x6C
#define V_PCIE_CFG_PCIESTS1_LTSMSTATE_RECOVERYLNK2DETECT 0x6F
#define B_PCIE_CFG_PCIESTS1_LNKSTAT                      (BIT22 | BIT21 | BIT20 | BIT19)
#define N_PCIE_CFG_PCIESTS1_LNKSTAT                      19
#define V_PCIE_CFG_PCIESTS1_LNKSTAT_L0                   0x7

#define R_PCIE_CFG_PCIEALC                            0x338
#define B_PCIE_CFG_PCIEALC_ITLRCLD                    BIT29
#define B_PCIE_CFG_PCIEALC_ILLRCLD                    BIT28
#define B_PCIE_CFG_PCIEALC_BLKDQDA                    BIT26
#define B_PCIE_CFG_PCIEALC_BLKDQDASD                  BIT25
#define B_PCIE_CFG_PCIEALC_SSRLD                      BIT24
#define B_PCIE_CFG_PCIEALC_SSRRS                      BIT23
#define B_PCIE_CFG_PCIEALC_RTD3PDSP_SIP17             BIT21
#define B_PCIE_CFG_PCIEALC_RTD3PDSP                   BIT20

#define R_PCIE_CFG_PCIEALC                            0x338
#define B_PCIE_CFG_PCIEALC_ITLRCLD                    BIT29
#define B_PCIE_CFG_PCIEALC_ILLRCLD                    BIT28
#define B_PCIE_CFG_PCIEALC_BLKDQDA                    BIT26
#define B_PCIE_CFG_PCIEALC_BLKDQDASD                  BIT25
#define B_PCIE_CFG_PCIEALC_RTD3PDSP_SIP17             BIT21
#define B_PCIE_CFG_PCIEALC_RTD3PDSP                   BIT20
#define B_PCIE_CFG_SIP17_PCIEALC_RTD3PDSP             BIT21 /// From SIP17 register layout is changed
#define B_PCIE_CFG_SIP17_PCIEALC_PDSP                 BIT20 /// From SIP17 register layout is changed

#define R_PCIE_CFG_PCIESTS2                           0x32C
#define B_PCIE_CFG_PCIESTS2_P4PNCCWSSCMES             BIT31
#define B_PCIE_CFG_PCIESTS2_P3PNCCWSSCMES             BIT30
#define B_PCIE_CFG_PCIESTS2_P2PNCCWSSCMES             BIT29
#define B_PCIE_CFG_PCIESTS2_P1PNCCWSSCMES             BIT28
#define B_PCIE_CFG_PCIESTS2_CLRE                      0x0000F000
#define N_PCIE_CFG_PCIESTS2_CLRE                      12

#define R_PCIE_CFG_PTMPD                              0x390 ///< PTM Propagation Delay
#define R_PCIE_CFG_PTMLLMT                            0x394 ///< PTM Lower Local Master Time
#define R_PCIE_CFG_PTMULMT                            0x398 ///< PTM Upper Local Master Time
#define R_PCIE_CFG_PTMPSDC1                           0x39C ///< PTM Pipe Stage Delay Configuration 1
#define R_PCIE_CFG_PTMPSDC2                           0x3A0 ///< PTM Pipe Stage Delay Configuration 2
#define R_PCIE_CFG_PTMPSDC3                           0x3A4 ///< PTM Pipe Stage Delay Configuration 3
#define R_PCIE_CFG_PTMPSDC4                           0x3A8 ///< PTM Pipe Stage Delay Configuration 4
#define R_PCIE_CFG_PTMPSDC5                           0x3AC ///< PTM Pipe Stage Delay Configuration 5
#define R_PCIE_CFG_PTMECFG                            0x3B0 ///< PTM Extended Configuration
#define B_PCIE_CFG_PTMECFG_IOSFMADP                   0xF   ///< IOSF Max Allowed Delay programming. bit0~bit3

#define R_PCIE_CFG_LTROVR                             0x400
#define B_PCIE_CFG_LTROVR_LTRNSROVR                   BIT31 ///< LTR Non-Snoop Requirement Bit Override
#define B_PCIE_CFG_LTROVR_LTRSROVR                    BIT15 ///< LTR Snoop Requirement Bit Override

#define R_PCIE_CFG_LTROVR2                            0x404
#define B_PCIE_CFG_LTROVR2_FORCE_OVERRIDE             BIT3 ///< LTR Force Override Enable
#define B_PCIE_CFG_LTROVR2_LOCK                       BIT2 ///< LTR Override Lock
#define B_PCIE_CFG_LTROVR2_LTRNSOVREN                 BIT1 ///< LTR Non-Snoop Override Enable
#define B_PCIE_CFG_LTROVR2_LTRSOVREN                  BIT0 ///< LTR Snoop Override Enable

#define R_PCIE_CFG_PHYCTL4                            0x408          ///< Physical Layer And AFE Control 4
#define B_PCIE_CFG_PHYCTL4_SQDIS                      BIT27          ///< Squelch Disable

#define R_PCIE_CFG_TNPT                               0x418 ///< Thermal and Power Throttling
#define B_PCIE_CFG_TNPT_DRXLTE                        BIT1
#define B_PCIE_CFG_TNPT_DTXLTE                        BIT0
#define N_PCIE_CFG_TNPT_TP                            24
#define V_PCIE_CFG_TNPT_TP_3_MS                       0x2

#define R_PCIE_CFG_PCIEPMECTL                         0x420          ///< PCIe PM Extension Control
#define B_PCIE_CFG_PCIEPMECTL_FDPPGE                  BIT31
#define B_PCIE_CFG_PCIEPMECTL_DLSULPPGE               BIT30          ///< Disabled, Detect and L23_Rdy State PHY Lane Power Gating Enable
#define B_PCIE_CFG_PCIEPMECTL_FDPPGE_OFFSET           31
#define B_PCIE_CFG_PCIEPMECTL_DLSULDLSD               BIT29          ///< Disabled, Detect, L23_Rdy State,Un-Configured Lane and
                                                                     ///< Down-Configured Lane Squelch Disable
#define B_PCIE_CFG_PCIEPMECTL_L1OCREWD                BIT28          ///< L1.OFF Clock Request Early Wake Disable
#define B_PCIE_CFG_PCIEPMECTL_IPIEP                   BIT21          ///< IP-Inaccessible Entry Policy
#define B_PCIE_CFG_PCIEPMECTL_IPACPE                  BIT20          ///< IP-Accessible Context Propagation Enable
#define B_PCIE_CFG_PCIEPMECTL_POFFWT                  (BIT19 | BIT18)
#define B_PCIE_CFG_PCIEPMECTL_L1LE                    BIT17
#define B_PCIE_CFG_PCIEPMECTL_L1LTRTLSV               (BIT16 | BIT15 | BIT14)
#define B_PCIE_CFG_PCIEPMECTL_L1LTRTLSV_OFFSET        14
#define V_PCIE_CFG_PCIEPMECTL_L1LTRTLSV               0x2
#define B_PCIE_CFG_PCIEPMECTL_L1LTRTLV                0x3FF0
#define B_PCIE_CFG_PCIEPMECTL_L1LTRTLV_OFFSET         4
#define V_PCIE_CFG_PCIEPMECTL_L1LTRTLV                0x32
#define B_PCIE_CFG_PCIEPMECTL_L1SNZCREWD              BIT3           ///< L1.SNOOZ/L1.LOW Clock Request Early Wake Disable
#define B_PCIE_CFG_PCIEPMECTL_L1FSOE                  BIT0           ///< L1 Full Squelch OFF Enable

#define R_PCIE_CFG_PCIEPMECTL2                        0x424
#define B_PCIE_CFG_PCIEPMECTL2_CPMCSRE                BIT27
#define B_PCIE_CFG_PCIEPMECTL2_CPMCSRE_OFFSET         27
#define B_PCIE_CFG_PCIEPMECTL2_CPGEXH                 BIT14
#define B_PCIE_CFG_PCIEPMECTL2_CPGEXH_OFFSET          14
#define B_PCIE_CFG_PCIEPMECTL2_CPGEXH_MASK            0xC000
#define V_PCIE_CFG_PCIEPMECTL2_CPGEXH                 0x1
#define B_PCIE_CFG_PCIEPMECTL2_CPGENH                 BIT12
#define B_PCIE_CFG_PCIEPMECTL2_CPGENH_OFFSET          12
#define B_PCIE_CFG_PCIEPMECTL2_CPGENH_MASK            0x3000
#define B_PCIE_CFG_PCIEPMECTL2_PHYCLPGE               BIT11
#define B_PCIE_CFG_PCIEPMECTL2_PHYCLPGE_OFFSET        11
#define B_PCIE_CFG_PCIEPMECTL2_L1SPHYDLPGE            BIT9
#define B_PCIE_CFG_PCIEPMECTL2_L1SPHYDLPGE_OFFSET     9
#define B_PCIE_CFG_PCIEPMECTL2_FDCPGE                 BIT8
#define B_PCIE_CFG_PCIEPMECTL2_FDCPGE_OFFSET          8
#define B_PCIE_CFG_PCIEPMECTL2_DETSCPGE               BIT7
#define B_PCIE_CFG_PCIEPMECTL2_DETSCPGE_OFFSET        7
#define B_PCIE_CFG_PCIEPMECTL2_L23RDYSCPGE            BIT6
#define B_PCIE_CFG_PCIEPMECTL2_L23RDYSCPGE_OFFSET     6
#define B_PCIE_CFG_PCIEPMECTL2_DISSCPGE               BIT5
#define B_PCIE_CFG_PCIEPMECTL2_DISSCPGE_OFFSET        5
#define B_PCIE_CFG_PCIEPMECTL2_L1SCPGE                BIT4
#define B_PCIE_CFG_PCIEPMECTL2_L1SCPGE_OFFSET         4

#define R_PCIE_CFG_PCE                                0x428
#define B_PCIE_CFG_PCE_HAE                            BIT5
#define B_PCIE_CFG_PCE_HAE_OFFSET                     5
#define B_PCIE_CFG_PCE_SE                             BIT3
#define B_PCIE_CFG_PCE_SE_OFFSET                      3
#define B_PCIE_CFG_PCE_PMCRE                          BIT0
#define B_PCIE_CFG_PCE_PMCRE_OFFSET                   0

#define R_PCIE_CFG_PGCBCTL1                           0x42C
#define B_PCIE_CFG_PGCBCTL1_TINACCRSTUP_MASK          0x3000
#define B_PCIE_CFG_PGCBCTL1_TACCRSTUP_MASK            (BIT11 | BIT10)
#define B_PCIE_CFG_PGCBCTL1_TACCRSTUP                 BIT10
#define B_PCIE_CFG_PGCBCTL1_TACCRSTUP_OFFSET          10
#define V_PCIE_CFG_PGCBCTL1_TACCRSTUP                 0x3

#define R_PCIE_CFG_PCIEPMECTL3                        0x434
#define B_PCIE_CFG_PCIEPMECTL3_L1PGAUTOPGEN           BIT4
#define B_PCIE_CFG_PCIEPMECTL3_OSCCGH_MASK            (BIT3 | BIT2)
#define N_PCIE_CFG_PCIEPMECTL3_OSCCGH                 2
#define B_PCIE_CFG_PCIEPMECTL3_OSCCGH_OFFSET          2
#define V_PCIE_CFG_PCIEPMECTL3_OSCCGH_0US             0x0
#define V_PCIE_CFG_PCIEPMECTL3_OSCCGH_1US             0x1
#define B_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH            BIT0
#define B_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH_MASK       0x3
#define B_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH_OFFSET     0
#define V_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH            0x0

#define R_PCIE_CFG_EQCFG1                             0x450
#define S_PCIE_CFG_EQCFG1                             4
#define B_PCIE_CFG_EQCFG1_REC                         0xFF000000
#define N_PCIE_CFG_EQCFG1_REC                         24
#define B_PCIE_CFG_EQCFG1_REIFECE                     BIT23
#define B_PCIE_CFG_EQCFG1_LERSMIE                     BIT21
#define N_PCIE_CFG_EQCFG1_LERSMIE                     21
#define B_PCIE_CFG_EQCFG1_LEP23B                      BIT18
#define B_PCIE_CFG_EQCFG1_LEP3B                       BIT17
#define B_PCIE_CFG_EQCFG1_RTLEPCEB                    BIT16
#define B_PCIE_CFG_EQCFG1_RTPCOE                      BIT15
#define B_PCIE_CFG_EQCFG1_HPCMQE                      BIT13
#define B_PCIE_CFG_EQCFG1_HAED                        BIT12
#define B_PCIE_CFG_EQCFG1_RWTNEVE                     (BIT11 | BIT10 | BIT9 | BIT8)
#define V_PCIE_CFG_EQCFG1_RWTNEVE                     0xF
#define V_PCIE_CFG_EQCFG1_RWTNEVE_1US                 0x1
#define N_PCIE_CFG_EQCFG1_RWTNEVE                     8
#define B_PCIE_CFG_EQCFG1_EQTS2IRRC                   BIT7
#define B_PCIE_CFG_EQCFG1_HAPCCPIE                    BIT5
#define B_PCIE_CFG_EQCFG1_TUPP                        BIT1

#define R_PCIE_CFG_RTPCL1                             0x454
#define N_PCIE_CFG_RTPCL1_RTPRECL2PL4                 24
#define N_PCIE_CFG_RTPCL1_RTPOSTCL1PL3                18
#define N_PCIE_CFG_RTPCL1_RTPRECL1PL2                 12
#define N_PCIE_CFG_RTPCL1_RTPOSTCL0PL1                6
#define N_PCIE_CFG_RTPCL1_RTPRECL0PL0                 0
#define B_PCIE_CFG_RTPCL1_PCM                         BIT31
#define B_PCIE_CFG_RTPCL1_RTPRECL2PL4                 0x3F000000
#define B_PCIE_CFG_RTPCL1_RTPOSTCL1PL3                0xFC0000
#define B_PCIE_CFG_RTPCL1_RTPRECL1PL2                 0x3F000
#define B_PCIE_CFG_RTPCL1_RTPOSTCL0PL1                0xFC0
#define B_PCIE_CFG_RTPCL1_RTPRECL0PL0                 0x3F

#define R_PCIE_CFG_RTPCL2                             0x458
#define N_PCIE_CFG_RTPCL2_RTPOSTCL4PL9                24
#define N_PCIE_CFG_RTPCL2_RTPRECL4PL8                 18
#define N_PCIE_CFG_RTPCL2_RTPOSTCL3PL7                12
#define N_PCIE_CFG_RTPCL2_RTPRECL3PL6                 6
#define N_PCIE_CFG_RTPCL2_RTPOSTCL2PL5                0
#define B_PCIE_CFG_RTPCL2_RTPOSTCL4PL9                0x3F000000
#define B_PCIE_CFG_RTPCL2_RTPRECL4PL8                 0xFC0000
#define B_PCIE_CFG_RTPCL2_RTPOSTCL3PL7                0x3F000
#define B_PCIE_CFG_RTPCL2_RTPRECL3PL6                 0xFC0
#define B_PCIE_CFG_RTPCL2_RTPOSTCL2PL5                0x3F

#define R_PCIE_CFG_EX_L01EC                           (R_PCIE_CFG_EX_SPEECH + R_PCIE_EX_L01EC_OFFSET)
#define B_PCIE_CFG_EX_L01EC_TP                        0xF ///< Bits used for each transmitter preset value
#define S_PCIE_CFG_EX_L01EC_TP                        8 ///< Each of the transmitter presets configuration fields
#define S_PCIE_CFG_EX_PL16L01EC_TP                    4 ///< Each of the transmitter presets configuration fields
#define B_PCIE_CFG_EX_UPL1357TP                       0x0F000000
#define N_PCIE_CFG_EX_UPL1357TP                       24
#define B_PCIE_CFG_EX_UPL0246TP                       0x00000F00
#define N_PCIE_CFG_EX_UPL0246TP                       8
#define R_PCIE_CFG_EX_L23EC                           (R_PCIE_CFG_EX_SPEECH + R_PCIE_EX_L23EC_OFFSET)
#define R_PCIE_CFG_EX_L45EC                           0xA44
#define R_PCIE_CFG_EX_L67EC                           0xA48

#define R_PCIE_CFG_RTPCL3                             0x45C
#define B_PCIE_CFG_RTPCL3_RTPRECL7                    0x3F000000
#define B_PCIE_CFG_RTPCL3_RTPOSTCL6                   0xFC0000
#define B_PCIE_CFG_RTPCL3_RTPRECL6                    0x3F000
#define B_PCIE_CFG_RTPCL3_RTPOSTCL5                   0xFC0
#define B_PCIE_CFG_RTPCL3_RTPRECL5PL10                0x3F

#define R_PCIE_CFG_RTPCL4                             0x460
#define B_PCIE_CFG_RTPCL4_RTPOSTCL9                   0x3F000000
#define B_PCIE_CFG_RTPCL4_RTPRECL9                    0xFC0000
#define B_PCIE_CFG_RTPCL4_RTPOSTCL8                   0x3F000
#define B_PCIE_CFG_RTPCL4_RTPRECL8                    0xFC0
#define B_PCIE_CFG_RTPCL4_RTPOSTCL7                   0x3F

#define R_PCIE_CFG_FOMS                               0x464
#define B_PCIE_CFG_FOMS_I                             (BIT30 | BIT29)
#define N_PCIE_CFG_FOMS_I                             29
#define B_PCIE_CFG_FOMS_LN                            0x1F000000
#define N_PCIE_CFG_FOMS_LN                            24
#define B_PCIE_CFG_FOMS_FOMSV                         0x00FFFFFF
#define B_PCIE_CFG_FOMS_FOMSV0                        0x000000FF
#define N_PCIE_CFG_FOMS_FOMSV0                        0
#define B_PCIE_CFG_FOMS_FOMSV1                        0x0000FF00
#define N_PCIE_CFG_FOMS_FOMSV1                        8
#define B_PCIE_CFG_FOMS_FOMSV2                        0x00FF0000
#define N_PCIE_CFG_FOMS_FOMSV2                        16

#define R_PCIE_CFG_HAEQ                               0x468
#define B_PCIE_CFG_HAEQ_DL                            0xFF00
#define N_PCIE_CFG_HAEQ_DL                            8
#define V_PCIE_CFG_HAEQ_DL                            0xE
#define B_PCIE_CFG_HAEQ_HAPCCPI                       0xF0000000
#define N_PCIE_CFG_HAEQ_HAPCCPI                       28
#define B_PCIE_CFG_HAEQ_MACFOMC                       BIT19

#define R_PCIE_CFG_LTCO1                              0x470
#define B_PCIE_CFG_LTCO1_L1TCOE                       BIT25
#define B_PCIE_CFG_LTCO1_L0TCOE                       BIT24
#define B_PCIE_CFG_LTCO1_L1TPOSTCO                    0xFC0000
#define N_PCIE_CFG_LTCO1_L1TPOSTCO                    18
#define B_PCIE_CFG_LTCO1_L1TPRECO                     0x3F000
#define N_PCIE_CFG_LTCO1_L1TPRECO                     12
#define B_PCIE_CFG_LTCO1_L0TPOSTCO                    0xFC0
#define N_PCIE_CFG_LTCO1_L0TPOSTCO                    6
#define B_PCIE_CFG_LTCO1_L0TPRECO                     0x3F
#define N_PCIE_CFG_LTCO1_L0TPRECO                     0

#define R_PCIE_CFG_LTCO2                              0x474
#define B_PCIE_CFG_LTCO2_L3TCOE                       BIT25
#define B_PCIE_CFG_LTCO2_L2TCOE                       BIT24
#define B_PCIE_CFG_LTCO2_L3TPOSTCO                    0xFC0000
#define B_PCIE_CFG_LTCO2_L3TPRECO                     0x3F000
#define B_PCIE_CFG_LTCO2_L2TPOSTCO                    0xFC0
#define B_PCIE_CFG_LTCO2_L2TPRECO                     0x3F

#define R_PCIE_CFG_G3L0SCTL                           0x478
#define B_PCIE_CFG_G3L0SCTL_G3ASL0SPL_MASK            0xFF000000 
#define B_PCIE_CFG_G3L0SCTL_G3ASL0SPL_OFFSET          24
#define V_PCIE_CFG_G3L0SCTL_G3ASL0SPL                 0x28
#define B_PCIE_CFG_G3L0SCTL_G3UCNFTS_MASK             0xFF00
#define B_PCIE_CFG_G3L0SCTL_G3UCNFTS_OFFSET           8
#define V_PCIE_CFG_G3L0SCTL_G3UCNFTS                  0x40
#define B_PCIE_CFG_G3L0SCTL_G3CCNFTS_MASK             0xFF
#define B_PCIE_CFG_G3L0SCTL_G3CCNFTS_OFFSET           0
#define V_PCIE_CFG_G3L0SCTL_G3CCNFTS                  0x2C

#define R_PCIE_CFG_EQCFG2                             0x47C
#define B_PCIE_CFG_EQCFG2_NTSS                        (BIT22 | BIT21 | BIT20)
#define B_PCIE_CFG_EQCFG2_PCET                        (BIT19 | BIT18 | BIT17 | BIT16)
#define N_PCIE_CFG_EQCFG2_PCET                        16
#define V_PCIE_CFG_EQCFG2_PCET                        0x2
#define B_PCIE_CFG_EQCFG2_HAPCSB                      (BIT15 | BIT14 | BIT13 | BIT12)
#define N_PCIE_CFG_EQCFG2_HAPCSB                      12
#define V_PCIE_CFG_EQCFG2_HAPCSB                      0x2
#define B_PCIE_CFG_EQCFG2_NTEME                       BIT11
#define B_PCIE_CFG_EQCFG2_MPEME                       BIT10
#define B_PCIE_CFG_EQCFG2_REWMETM                     (BIT9 | BIT8)
#define B_PCIE_CFG_EQCFG2_REWMET                      0xFF
#define N_PCIE_CFG_EQCFG2_REWMET                      0
#define V_PCIE_CFG_EQCFG2_REWMET                      0x10

#define R_PCIE_CFG_MM                                 0x480
#define B_PCIE_CFG_MM_MSST                            0xFFFFFF00
#define N_PCIE_CFG_MM_MSST                            8
#define B_PCIE_CFG_MM_MSS                             0xFF

#define R_PCIE_CFG_EQCFG4                             0x48C
#define S_PCIE_CFG_EQCFG4                             4
#define B_PCIE_CFG_EQCFG4_PX16GTSWLPCE                (BIT29 | BIT28 | BIT27)
#define B_PCIE_CFG_EQCFG4_PX16GTSWLPCE_OFFSET         27
#define B_PCIE_CFG_EQCFG4_PX8GTSWLPCE                 (BIT26 | BIT25 | BIT24)
#define B_PCIE_CFG_EQCFG4_PX8GTSWLPCE_OFFSET          24
#define V_PCIE_CFG_EQCFG4_PX8GTSWLPCE                 0x0
#define B_PCIE_CFG_EQCFG4_PX16GLEP23B                 BIT18
#define B_PCIE_CFG_EQCFG4_PX16GLEP3B                  BIT17
#define B_PCIE_CFG_EQCFG4_PX16GRTLEPCEB               BIT16
#define B_PCIE_CFG_EQCFG4_PX16GRTPCOE                 BIT15
#define B_PCIE_CFG_EQCFG4_HPCMQE                      BIT13
#define B_PCIE_CFG_EQCFG4_PX16GHAED                   BIT12
#define B_PCIE_CFG_EQCFG4_PX16GRWTNEVE                0xF00
#define V_PCIE_CFG_EQCFG4_PX16GRWTNEVE                0xF
#define N_PCIE_CFG_EQCFG4_PX16GRWTNEVE                8
#define V_PCIE_CFG_EQCFG4_PX16GRWTNEVE_3US            0x3
#define B_PCIE_CFG_EQCFG4_PX16GEQTS2IRRC              BIT7
#define B_PCIE_CFG_EQCFG4_PX16GHAPCCPI                (BIT6 | BIT5 | BIT4 | BIT3)
#define B_PCIE_CFG_EQCFG4_PX16GHAPCCPI_OFFSET         3
#define V_PCIE_CFG_EQCFG4_PX16GHAPCCPI                0x2
#define B_PCIE_CFG_EQCFG4_PX16GHAPCCPIE               BIT2
#define B_PCIE_CFG_EQCFG4_PX16GMFLNTL                 BIT0

#define R_PCIE_CFG_CTRL1                              0x4A0                       ///< Control 1
#define B_PCIE_CFG_CTRL1_MRSCDIS                      BIT21                       ///< MEUMA Root Space Check Disable
#define B_PCIE_CFG_CTRL1_L0SPFCUF                     (BIT16 | BIT15)             ///< L0s Periodic Flow Control Update Frequency
#define B_PCIE_CFG_CTRL1_L1RC                         BIT12                       ///< DMI L1 Root Port Control
#define B_PCIE_CFG_CTRL1_L1PL                         (BIT11 | BIT10 | BIT9)      ///< DMI L1 Preparation Latency
#define N_PCIE_CFG_CTRL1_L1PL                         9                           ///< DMI L1 Preparation Latency
#define V_PCIE_CFG_CTRL1_L1PL_4US                     0x3                         ///< DMI L1 Preparation Latency = 1us
#define V_PCIE_CFG_CTRL1_L1PL_2US                     0x2
#define B_PCIE_CFG_CTRL1_L1RCEP                       BIT8
#define B_PCIE_CFG_CTRL1_TCUAPF                       BIT6                        ///< Transaction Credit Update Arbitration Performance Fix
#define B_PCIE_CFG_CTRL1_DLDHB                        BIT4                        ///< DMI Link Down Hang Bypass
#define B_PCIE_CFG_CTRL1_PSS                          BIT3                        ///< IOSF Primary SAI Select
#define B_PCIE_CFG_CTRL1_UTPB                         BIT0                        ///< Unsupported Transaction Policy Bit

#define R_PCIE_CFG_CONTROL2                           0x4A4
#define B_PCIE_CFG_CONTROL2_RXHALEP                   BIT19
#define B_PCIE_CFG_CONTROL2_HALEP                     BIT18
#define B_PCIE_CFG_CONTROL2_DLDRSP                    BIT13                       ///< Link Data Rate Sustain Policy
#define B_PCIE_CFG_CONTROL2_PMETOFD                   BIT6
#define B_PCIE_CFG_CONTROL2_CPGEXLCWDIS               BIT5                        ///< CPG Exit Link Clock Wake Disable

#define R_PCIE_CFG_PX16GRTPCL1                        0x4DC
#define N_PCIE_CFG_PX16GRTPCL1_RTPRECL2PL4            24
#define N_PCIE_CFG_PX16GRTPCL1_RTPOSTCL1PL3           18
#define N_PCIE_CFG_PX16GRTPCL1_RTPRECL1PL2            12
#define N_PCIE_CFG_PX16GRTPCL1_RTPOSTCL0PL1           6
#define N_PCIE_CFG_PX16GRTPCL1_RTPRECL0PL0            0
#define B_PCIE_CFG_PX16GRTPCL1_PCM                    BIT31
#define B_PCIE_CFG_PX16GRTPCL1_RTPRECL2PL4            0x3F000000
#define B_PCIE_CFG_PX16GRTPCL1_RTPOSTCL1PL3           0xFC0000
#define B_PCIE_CFG_PX16GRTPCL1_RTPRECL1PL2            0x3F000
#define B_PCIE_CFG_PX16GRTPCL1_RTPOSTCL0PL1           0xFC0
#define B_PCIE_CFG_PX16GRTPCL1_RTPRECL0PL0            0x3F

#define R_PCIE_CFG_PX16GRTPCL2                        0x4E0
#define N_PCIE_CFG_PX16GRTPCL2_RTPOSTCL4PL9           24
#define N_PCIE_CFG_PX16GRTPCL2_RTPRECL4PL8            18
#define N_PCIE_CFG_PX16GRTPCL2_RTPOSTCL3PL7           12
#define N_PCIE_CFG_PX16GRTPCL2_RTPRECL3PL6            6
#define N_PCIE_CFG_PX16GRTPCL2_RTPOSTCL2PL5           0
#define B_PCIE_CFG_PX16GRTPCL2_RTPOSTCL4PL9           0x3F000000
#define B_PCIE_CFG_PX16GRTPCL2_RTPRECL4PL8            0xFC0000
#define B_PCIE_CFG_PX16GRTPCL2_RTPOSTCL3PL7           0x3F000
#define B_PCIE_CFG_PX16GRTPCL2_RTPRECL3PL6            0xFC0
#define B_PCIE_CFG_PX16GRTPCL2_RTPOSTCL2PL5           0x3F

#define R_PCIE_CFG_PX16GRTPCL3                        0x4E4
#define N_PCIE_CFG_PX16GRTPCL3_RTPRECL7               24
#define N_PCIE_CFG_PX16GRTPCL3_RTPOSTCL6              18
#define N_PCIE_CFG_PX16GRTPCL3_RTPRECL6               12
#define N_PCIE_CFG_PX16GRTPCL3_RTPOSTCL5              6
#define N_PCIE_CFG_PX16GRTPCL3_RTPRECL5PL10           0
#define B_PCIE_CFG_PX16GRTPCL3_RTPRECL7               0x3F000000
#define B_PCIE_CFG_PX16GRTPCL3_RTPOSTCL6              0xFC0000
#define B_PCIE_CFG_PX16GRTPCL3_RTPRECL6               0x3F000
#define B_PCIE_CFG_PX16GRTPCL3_RTPOSTCL5              0xFC0
#define B_PCIE_CFG_PX16GRTPCL3_RTPRECL5PL10           0x3F

#define R_PCIE_CFG_PX16GRTPCL4                        0x4E8
#define N_PCIE_CFG_PX16GRTPCL4_RTPOSTCL9              24
#define N_PCIE_CFG_PX16GRTPCL4_RTPRECL9               18
#define N_PCIE_CFG_PX16GRTPCL4_RTPOSTCL8              12
#define N_PCIE_CFG_PX16GRTPCL4_RTPRECL8               6
#define N_PCIE_CFG_PX16GRTPCL4_RTPOSTCL7              0
#define B_PCIE_CFG_PX16GRTPCL4_RTPOSTCL9              0x3F000000
#define B_PCIE_CFG_PX16GRTPCL4_RTPRECL9               0xFC0000
#define B_PCIE_CFG_PX16GRTPCL4_RTPOSTCL8              0x3F000
#define B_PCIE_CFG_PX16GRTPCL4_RTPRECL8               0xFC0
#define B_PCIE_CFG_PX16GRTPCL4_RTPOSTCL7              0x3F

#define R_PCIE_CFG_EQCFG5                             0x4F8
#define B_PCIE_CFG_EQCFG5_PCET                        (BIT19 | BIT18 | BIT17 | BIT16)
#define N_PCIE_CFG_EQCFG5_PCET                        16
#define V_PCIE_CFG_EQCFG5_PCET                        0x2
#define B_PCIE_CFG_EQCFG5_HAPCSB                      (BIT15 | BIT14 | BIT13 | BIT12)
#define N_PCIE_CFG_EQCFG5_HAPCSB                      12
#define V_PCIE_CFG_EQCFG5_HAPCSB                      0x0
#define B_PCIE_CFG_EQCFG5_NTEME                       BIT11
#define B_PCIE_CFG_EQCFG5_MPEME                       BIT10
#define B_PCIE_CFG_EQCFG5_REWMETM                     (BIT9 | BIT8)
#define B_PCIE_CFG_EQCFG5_REWMET                      0xFF

#define R_PCIE_CFG_LANE0_PRESETS                      0x500
#define S_PCIE_CFG_SINGLE_LANE_PRESETS                0x20
#define R_PCIE_CFG_LFFS_OFFSET_FROM_LANE_BASE         0x1C   ///< This register offset is taken from base of the lane presets configuration
                                                             ///< starting with R_PCIE_CFG_LANE0_PRESETS register.
#define N_PCIE_CFG_LFFS_FS                            8      ///< Offset of LFFS being 0x51C

#define R_PCIE_CFG_L0P1P2P3PCM                        0x504
#define R_PCIE_CFG_L0P3P4PCM                          0x508
#define R_PCIE_CFG_L0P5P6PCM                          0x50C
#define R_PCIE_CFG_L0P6P7P8PCM                        0x510
#define R_PCIE_CFG_L0P8P9PCM                          0x514
#define R_PCIE_CFG_L0P10PCM                           0x518
#define R_PCIE_CFG_L0LFFS                             0x51C

#define R_PCIE_CFG_PX16GP0P1PCM                       0x520
#define R_PCIE_CFG_PX16GP1P2P3PCM                     0x524
#define R_PCIE_CFG_PX16GP3P4PCM                       0x528
#define R_PCIE_CFG_PX16GP5P6PCM                       0x52C
#define R_PCIE_CFG_PX16GP6P7P8PCM                     0x530
#define R_PCIE_CFG_PX16GP8P9PCM                       0x534
#define R_PCIE_CFG_PX16GP10PCM                        0x538
#define R_PCIE_CFG_PX16GLFFS                          0x53C

//
// PTM pipe stage delay registers. Valid from PCIe SIP14
//
#define R_PCIE_CFG_PTMPSDC1                           0x39C ///< PTM Pipe Stage Delay Configuration 1
#define R_PCIE_CFG_PTMPSDC2                           0x3A0 ///< PTM Pipe Stage Delay Configuration 2
#define R_PCIE_CFG_PTMPSDC3                           0x3A4 ///< PTM Pipe Stage Delay Configuration 3
#define R_PCIE_CFG_PTMPSDC4                           0x3A8 ///< PTM Pipe Stage Delay Configuration 4
#define R_PCIE_CFG_PTMPSDC5                           0x3AC ///< PTM Pipe Stage Delay Configuration 5
#define R_PCIE_CFG_PTMECFG                            0x3B0 ///< PTM Extended Configuration

//
// Coalescing registers. From PCIe SIP 16
//
#define R_PCIE_CFG_COCTL                              0x594
#define B_PCIE_CFG_COCTL_NPCLM                        BIT15
#define N_PCIE_CFG_COCTL_NPCLM_OFFSET                 15
#define B_PCIE_CFG_COCTL_NPCLM_MASK                   (BIT15 | BIT16)
#define B_PCIE_CFG_COCTL_PCLM                         BIT13
#define N_PCIE_CFG_COCTL_PCLM_OFFSET                  13
#define B_PCIE_CFG_COCTL_PCLM_MASK                    (BIT13 | BIT14)
#define B_PCIE_CFG_COCTL_MAGPARCD                     BIT12
#define N_PCIE_CFG_COCTL_MAGPARCD_OFFSET              12
#define B_PCIE_CFG_COCTL_ROAOP                        BIT11
#define N_PCIE_CFG_COCTL_ROAOP_OFFSET                 11
#define B_PCIE_CFG_COCTL_CTE                          BIT10
#define N_PCIE_CFG_COCTL_CTE_OFFSET                   10
#define B_PCIE_CFG_COCTL_CT                           BIT2
#define N_PCIE_CFG_COCTL_CT_OFFSET                    2
#define B_PCIE_CFG_COCTL_CT_MASK                      0x3FC
#define B_PCIE_CFG_COCTL_DDCE                         BIT1
#define N_PCIE_CFG_COCTL_DDCE_OFFSET                  1
#define B_PCIE_CFG_COCTL_PWCE                         BIT0
#define N_PCIE_CFG_COCTL_PWCE_OFFSET                  0

#define R_PCIE_CFG_LTCO3                              0x2598 ///< Local Transmitter Coefficient Override 3
#define R_PCIE_CFG_LTCO4                              0x259C ///< Local Transmitter Coefficient Override 4

//
// PTM pipe stage delay register to cover x16 and x8 links. Added in PCIe SIP16
//
#define R_PCIE_CFG_PTMPSDC6                           0x5B0 ///< SIP16 PTM Pipe Stage Delay Configuration 6
#define R_PCIE_CFG_PTMPSDC7                           0x5B4 ///< SIP16 PTM Pipe Stage Delay Configuration 7
#define R_PCIE_CFG_PTMPSDC8                           0x5B8 ///< SIP16 PTM Pipe Stage Delay Configuration 8


//
// Advance Mode Control Register
//
#define R_PCIE_ADVMCTRL                               0x5BC
#define B_PCIE_ADVMCTRL_F10BTSE                       BIT24
#define B_PCIE_ADVMCTRL_CCBE                          BIT23
#define B_PCIE_ADVMCTRL_INRXL0CTRL                    BIT22
#define B_PCIE_ADVMCTRL_ACCRM                         BIT21
#define B_PCIE_ADVMCTRL_EIOSDISDS                     BIT20
#define B_PCIE_ADVMCTRL_EIOSMASKRX                    BIT19
#define B_PCIE_ADVMCTRL_PMREQCWC                      BIT16
#define B_PCIE_ADVMCTRL_PMREQCWC_MASK                 (BIT18 | BIT17 | BIT16)
#define N_PCIE_ADVMCTRL_PMREQCWC_OFFSET               16
#define V_PCIE_ADVMCTRL_PMREQCWC_LNK_PRIM             6
#define B_PCIE_ADVMCTRL_RXL0DC                        BIT15
#define B_PCIE_ADVMCTRL_G3STFER                       BIT13
#define B_PCIE_ADVMCTRL_RRLLCL                        BIT11
#define B_PCIE_ADVMCTRL_RLLG12R                       BIT10
#define B_PCIE_ADVMCTRL_PMREQBLKPGRSPT                BIT5
#define B_PCIE_ADVMCTRL_PMREQBLKPGRSPT_MASK           (BIT7 | BIT6 | BIT5)
#define N_PCIE_ADVMCTRL_PMREQBLKPGRSPT_OFFSET         5
#define V_PCIE_ADVMCTRL_PMREQBLKPGRSPT_10US           2

#define R_PCIE_CFG_PGTHRES                            0x5C0
#define B_PCIE_CFG_PGTHRES_L1PGLTRTLSV                BIT29
#define B_PCIE_CFG_PGTHRES_L1PGLTRTLSV_OFFSET         29
#define N_PCIE_CFG_PGTHRES_L1PGLTRTLSV_OFFSET         29
#define B_PCIE_CFG_PGTHRES_L1PGLTRTLSV_MASK           0xE0000000
#define B_PCIE_CFG_PGTHRES_L1PGLTRTLV                 BIT16
#define B_PCIE_CFG_PGTHRES_L1PGLTRTLV_OFFSET          16
#define N_PCIE_CFG_PGTHRES_L1PGLTRTLV_OFFSET          16
#define B_PCIE_CFG_PGTHRES_L1PGLTRTLV_MASK            0x3FF0000
#define B_PCIE_CFG_PGTHRES_L1PGLTREN                  BIT0
#define B_PCIE_CFG_PGTHRES_L1PGLTREN_OFFSET           0
#define N_PCIE_CFG_PGTHRES_L1PGLTREN_OFFSET           0

#define R_PCIE_CFG_HWSNR                              0x5F0
#define B_PCIE_CFG_HWSNR_READY4PG                     BIT10
#define B_PCIE_CFG_HWSNR_READY4PG_OFFSET              10
#define B_PCIE_CFG_HWSNR_EEH_MASK                     (BIT9 | BIT8)
#define B_PCIE_CFG_HWSNR_EEH                          BIT8
#define B_PCIE_CFG_HWSNR_EEH_OFFSET                   8
#define V_PCIE_CFG_HWSNR_EEH_16CLK                    2
#define N_PCIE_CFG_HWSNR_EEH_OFFSET                   8
#define V_PCIE_CFG_HWSNR_EEH                          0x2
#define B_PCIE_CFG_HWSNR_REPW_MASK                    (BIT7 | BIT6 | BIT5 | BIT4)
#define B_PCIE_CFG_HWSNR_REPW                         BIT4
#define B_PCIE_CFG_HWSNR_REPW_OFFSET                  4
#define V_PCIE_CFG_HWSNR_REPW                         0x1
#define V_PCIE_CFG_HWSNR_REPW_2CLK                    1
#define N_PCIE_CFG_HWSNR_REPW_OFFSET                  4
#define B_PCIE_CFG_HWSNR_BEPW_MASK                    (BIT0 | BIT1 | BIT2 | BIT3)
#define B_PCIE_CFG_HWSNR_BEPW                         BIT0
#define B_PCIE_CFG_HWSNR_BEPW_OFFSET                  0
#define B_PCIE_CFG_HWSNR_BEPW_8_CLKS                  0x5
#define V_PCIE_CFG_HWSNR_BEPW                         0x5
#define V_PCIE_CFG_HWSNR_BEPW_8CLK                    5

#define R_PCIE_CFG_PGCTRL                             0x5F4
#define B_PCIE_CFG_PGCTRL_PMREQBLKRSPT                BIT0
#define B_PCIE_CFG_PGCTRL_PMREQBLKRSPT_OFFSET         0
#define V_PCIE_CFG_PGCTRL_PMREQBLKRSPT_10MICRO_SEC    0x2
#define V_PCIE_CFG_PGCTRL_PMREQBLKRSPT_25MICRO_SEC    0x5

#define R_PCIE_CFG_PX16GLTCO1                         0x600
#define B_PCIE_CFG_PX16GLTCO1_L1TCOE                  BIT25
#define B_PCIE_CFG_PX16GLTCO1_L0TCOE                  BIT24
#define B_PCIE_CFG_PX16GLTCO1_L1TPOSTCO               0xFC0000
#define N_PCIE_CFG_PX16GLTCO1_L1TPOSTCO               18
#define B_PCIE_CFG_PX16GLTCO1_L1TPRECO                0x3F000
#define N_PCIE_CFG_PX16GLTCO1_L1TPRECO                12
#define B_PCIE_CFG_PX16GLTCO1_L0TPOSTCO               0xFC0
#define N_PCIE_CFG_PX16GLTCO1_L0TPOSTCO               6
#define B_PCIE_CFG_PX16GLTCO1_L0TPRECO                0x3F
#define N_PCIE_CFG_PX16GLTCO1_L0TPRECO                0

#define R_PCIE_CFG_PCIERTP3                           0x6A0
#define B_PCIE_CFG_PCIERTP3_G1X8_MASK                 (BIT7 | BIT6 | BIT5 | BIT4)
#define B_PCIE_CFG_PCIERTP3_G1X8_OFFSET               4
#define V_PCIE_CFG_PCIERTP3_G1X8                      0x5
#define B_PCIE_CFG_PCIERTP3_G1X16_MASK                (BIT3 | BIT2 | BIT1 | BIT0)
#define B_PCIE_CFG_PCIERTP3_G1X16_OFFSET              0
#define V_PCIE_CFG_PCIERTP3_G1X16                     0x0

#define R_PCIE_CFG_PGCTRL                             0x5F4
#define B_PCIE_CFG_PGCTRL_PMREQBLKRSPT                BIT0
#define B_PCIE_CFG_PGCTRL_PMREQBLKRSPT_MASK           (BIT0 | BIT1 | BIT2)
#define V_PCIE_CFG_PGCTRL_PMREQBLKRSPT_10US           2

#define R_PCIE_CFG_GDR                                0x690
#define B_PCIE_CFG_GDR_GPIOALD                        BIT10

#define R_PCIE_CFG_ACRG3                              0x6CC
#define B_PCIE_CFG_ACRG3_RRXDME                       BIT27
#define B_PCIE_CFG_ACRG3_CPGWAKECTRL                  BIT22
#define B_PCIE_CFG_ACRG3_CPGWAKECTRL_OFFSET           22
#define N_PCIE_CFG_ACRG3_CPGWAKECTRL_OFFSET           22
#define B_PCIE_CFG_ACRG3_CPGWAKECTRL_MASK             0xC00000
#define V_PCIE_CFG_ACRG3_CPGWAKECTRL                  0x2
#define B_PCIE_CFG_ACRG3_CBGM                         BIT21
#define B_PCIE_CFG_ACRG3_ADESKEW_DIS                  BIT10

#define R_PCIE_CFG_EX_SPEECH                          0xA30 ///< Secondary PCI Express Extended Capability Header
#define B_PCIE_CFG_EX_SPEECH_NCO_MASK                 0xFFF00000
#define B_PCIE_CFG_EX_SPEECH_NCO_OFFSET               20
#define V_PCIE_CFG_EX_SPEECH_NCO                      0xA90
#define B_PCIE_CFG_EX_SPEECH_CV_MASK                  0xF0000
#define B_PCIE_CFG_EX_SPEECH_CV_OFFSET                16
#define V_PCIE_CFG_EX_SPEECH_CV                       0x1
#define B_PCIE_CFG_EX_SPEECH_PCIEECID_MASK            0xFFFF
#define B_PCIE_CFG_EX_SPEECH_PCIEECID_OFFSET          0
#define V_PCIE_CFG_EX_SPEECH_PCIEECID                 0x19

#define R_PCIE_CFG_EX_LCTL3                           (R_PCIE_CFG_EX_SPEECH + R_PCIE_EX_LCTL3_OFFSET)
#define R_PCIE_CFG_EX_LES                             (R_PCIE_CFG_EX_SPEECH + R_PCIE_EX_LES_OFFSET)
#define R_PCIE_CFG_EX_LECTL                           (R_PCIE_CFG_EX_SPEECH + R_PCIE_EX_L01EC_OFFSET)
#define B_PCIE_CFG_EX_LECTL_UPTPH                     (BIT14 | BIT13 | BIT12)
#define N_PCIE_CFG_EX_LECTL_UPTPH                     12
#define B_PCIE_CFG_EX_LECTL_UPTP                      0x0F00
#define N_PCIE_CFG_EX_LECTL_UPTP                      8
#define B_PCIE_CFG_EX_LECTL_DPTPH                     (BIT6 | BIT5 | BIT4)
#define N_PCIE_CFG_EX_LECTL_DPTPH                     4
#define B_PCIE_CFG_EX_LECTL_DPTP                      0x000F
#define N_PCIE_CFG_EX_LECTL_DPTP                      0

#define R_PCIE_CFG_EX_DPCECH                          0xA00 ///< Downstream Port Containment
#define V_PCIE_CFG_EX_DPCECH_CV                       0x1

#define R_PCIE_CFG_DLFECH                             0xA90
#define V_PCIE_CFG_DLFECH_NCO                         0xA9C
#define B_PCIE_CFG_DLFECH_NCO                         BIT20
#define B_PCIE_CFG_DLFECH_NCO_OFFSET                  20
#define B_PCIE_CFG_DLFECH_NCO_MASK                    0xFFF00000
#define B_PCIE_CFG_DLFECH_CV                          BIT16
#define B_PCIE_CFG_DLFECH_CV_OFFSET                   16
#define B_PCIE_CFG_DLFECH_CV_MASK                     0xF0000
#define V_PCIE_CFG_DLFECH_CV                          0x1
#define V_PCIE_CFG_DLFECH_PCIEECID                    0x25
#define B_PCIE_CFG_DLFECH_PCIEECID                    BIT0
#define B_PCIE_CFG_DLFECH_PCIEECID_OFFSET             0
#define B_PCIE_CFG_DLFECH_PCIEECID_MASK               0xFFFF

#define R_PCH_PCIE_DLFECH                             0xA90
#define B_PCH_PCIE_DLFECH_NCO                         BIT20
#define B_PCH_PCIE_DLFECH_NCO_OFFSET                  20
#define B_PCH_PCIE_DLFECH_NCO_MASK                    0xFFF00000
#define B_PCH_PCIE_DLFECH_CV                          BIT16
#define B_PCH_PCIE_DLFECH_CV_OFFSET                   16
#define B_PCH_PCIE_DLFECH_CV_MASK                     0xF0000
#define V_PCH_PCIE_DLFECH_CV                          0x1
#define B_PCH_PCIE_DLFECH_PCIEECID                    BIT0
#define B_PCH_PCIE_DLFECH_PCIEECID_OFFSET             0
#define B_PCH_PCIE_DLFECH_PCIEECID_MASK               0xFFFF

#define R_PCH_PCIE_DLFCAP                             0xA94
#define B_PCH_PCIE_DLFCAP_LSFCS                       BIT0

#define R_PCIE_CFG_PL16GECH                           0xA9C
#define V_PCIE_CFG_PL16GECH_NCO                       0xEDC
#define B_PCIE_CFG_PL16GECH_NCO                       BIT20
#define B_PCIE_CFG_PL16GECH_NCO_OFFSET                20
#define B_PCIE_CFG_PL16GECH_NCO_MASK                  0xFFF00000
#define B_PCIE_CFG_PL16GECH_CV                        BIT16
#define B_PCIE_CFG_PL16GECH_CV_OFFSET                 16
#define B_PCIE_CFG_PL16GECH_CV_MASK                   0xF0000
#define V_PCIE_CFG_PL16GECH_CV                        1
#define V_PCIE_CFG_PL16GECH_CID                       0x26
#define B_PCIE_CFG_PL16GECH_CID                       BIT0
#define B_PCIE_CFG_PL16GECH_CID_OFFSET                0
#define B_PCIE_CFG_PL16GECH_CID_MASK                  0xFFFF

#define R_PCIE_CFG_EX_PL16L01EC                       0xABC
#define R_PCIE_CFG_EX_PL16L23EC                       0xABE
#define R_PCIE_CFG_EX_PL16L45EC                       0xAC0
#define R_PCIE_CFG_EX_PL16L67EC                       0xAC2
#define B_PCIE_CFG_EX_PL16UPL1357TP                   0xF000
#define N_PCIE_CFG_EX_PL16UPL1357TP                   12
#define B_PCIE_CFG_EX_PL16UPL0246TP                   0x00F0
#define N_PCIE_CFG_EX_PL16UPL0246TP                   4

#define R_PCIE_CFG_EX_G5LANEEQCTL                     0xAFC

#define R_PCIE_CFG_APEC                               0xB0C
#define B_PCIE_CFG_APEC_NCO                           BIT20
#define B_PCIE_CFG_APEC_NCO_OFFSET                    20
#define B_PCIE_CFG_APEC_NCO_MASK                      0xFFF00000
#define B_PCIE_CFG_APEC_CV                            BIT16
#define B_PCIE_CFG_APEC_CV_OFFSET                     16
#define B_PCIE_CFG_APEC_CV_MASK                       0xF0000
#define V_PCIE_CFG_APEC_CV                            1
#define B_PCIE_CFG_APEC_CID                           BIT0
#define B_PCIE_CFG_APEC_CID_OFFSET                    0
#define B_PCIE_CFG_APEC_CID_MASK                      0xFFFF

#define R_PCIE_CFG_ACGR3S2                            0xC50
#define B_PCIE_CFG_ACGR3S2_UPL1EPC                    BIT19
#define B_PCIE_CFG_ACGR3S2_G4EBM                      BIT9
#define B_PCIE_CFG_ACGR3S2_G3EBM                      BIT8
#define B_PCIE_CFG_ACGR3S2_G2EBM                      BIT7
#define B_PCIE_CFG_ACGR3S2_G1EBM                      BIT6
#define B_PCIE_CFG_ACGR3S2_SRT                        BIT5
#define B_PCIE_CFG_ACGR3S2_LSTPTLS_MASK               0xF
#define B_PCIE_CFG_ACGR3S2_LSTPTLS_OFFSET             0
#define V_PCIE_CFG_ACGR3S2_LSTPTLS                    0x1

#define R_PCIE_CFG_VNNREMCTL                          0xC70
#define B_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_MASK          (BIT3 | BIT2)
#define B_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_OFFSET        2
#define V_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_16CLKS        0x1
#define V_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_8CLKS         0x0
#define B_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_MASK          (BIT1 | BIT0)
#define B_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_OFFSET        0
#define V_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_4CLKS         0x0

#define R_PCIE_CFG_RXQC                               0xC7C
#define B_PCIE_CFG_RXQC_VMRQNPCOC_MASK                (BIT15 | BIT14)
#define B_PCIE_CFG_RXQC_VMRQNPCOC_OFFSET              14
#define V_PCIE_CFG_RXQC_VMRQNPCOC                     0x1
#define B_PCIE_CFG_RXQC_VMRQPCOC_MASK                 (BIT13 | BIT12)
#define B_PCIE_CFG_RXQC_VMRQPCOC_OFFSET               12
#define V_PCIE_CFG_RXQC_VMRQPCOC                      0x1
#define B_PCIE_CFG_RXQC_V1RQNPCOC_MASK                (BIT9 | BIT8)
#define B_PCIE_CFG_RXQC_V1RQNPCOC_OFFSET              8
#define V_PCIE_CFG_RXQC_V1RQNPCOC                     0x1
#define B_PCIE_CFG_RXQC_V1RQPCOC_MASK                 (BIT7 | BIT6)
#define B_PCIE_CFG_RXQC_V1RQPCOC_OFFSET               6
#define V_PCIE_CFG_RXQC_V1RQPCOC                      0x1
#define B_PCIE_CFG_RXQC_V0RQNPCOC_MASK                (BIT3 | BIT2)
#define B_PCIE_CFG_RXQC_V0RQNPCOC_OFFSET              2
#define V_PCIE_CFG_RXQC_V0RQNPCOC                     0x1
#define B_PCIE_CFG_RXQC_V0RQPCOC_MASK                 (BIT1 | BIT0)
#define B_PCIE_CFG_RXQC_V0RQPCOC_OFFSET               0
#define V_PCIE_CFG_RXQC_V0RQPCOC                      0x1

//
// PTM pipe stage delay register to cover GEN4 stage delays. Added in PCIe SIP16
//
#define R_PCIE_CFG_PTMPSDC9                           0xCB0 ///< SIP16 PTM Pipe Stage Delay Configuration 9
#define R_PCIE_CFG_PTMPSDC10                          0xCB4 ///< SIP16 PTM Pipe Stage Delay Configuration 10
#define R_PCIE_CFG_PTMPSDC11                          0xCB8 ///< SIP16 PTM Pipe Stage Delay Configuration 11

#define R_PCIE_CFG_PL16MECH                           0xEDC
#define V_PCIE_CFG_PL16MECH_NCO                       0xEDC
#define B_PCIE_CFG_PL16MECH_NCO                       BIT20
#define B_PCIE_CFG_PL16MECH_NCO_OFFSET                20
#define B_PCIE_CFG_PL16MECH_NCO_MASK                  0xFFF00000
#define B_PCIE_CFG_PL16MECH_CV                        BIT16
#define B_PCIE_CFG_PL16MECH_CV_OFFSET                 16
#define B_PCIE_CFG_PL16MECH_CV_MASK                   0xF0000
#define V_PCIE_CFG_PL16MECH_CV                        1
#define V_PCIE_CFG_PL16MECH_CID                       0x26
#define B_PCIE_CFG_PL16MECH_CID                       BIT0
#define B_PCIE_CFG_PL16MECH_CID_OFFSET                0
#define B_PCIE_CFG_PL16MECH_CID_MASK                  0xFFFF

#define R_PCIE_CFG_ACGR3S2                            0xC50
#define B_PCIE_CFG_ACGR3S2_G5EBM                      BIT20
#define B_PCIE_CFG_ACGR3S2_G4EBM                      BIT9
#define B_PCIE_CFG_ACGR3S2_G3EBM                      BIT8
#define B_PCIE_CFG_ACGR3S2_G2EBM                      BIT7
#define B_PCIE_CFG_ACGR3S2_G1EBM                      BIT6
#define B_PCIE_CFG_ACGR3S2_SRT                        BIT5

//
// LTR subtraction registers. Added in PCIe SIP 16
//
#define R_PCIE_CFG_LTRSUBL1STD                        0xC5C
#define R_PCIE_CFG_LTRSUBL11                          0xC60
#define R_PCIE_CFG_LTRSUBL12                          0xC64
#define B_PCIE_CFG_LTRSUB_LTRNSLSUBEN                 BIT31
#define B_PCIE_CFG_LTRSUB_LTRNSLSSUBV                 BIT26
#define N_PCIE_CFG_LTRSUB_LTRNSLSSUBV                 26
#define B_PCIE_CFG_LTRSUB_LTRNSLSSUBV_MASK            (BIT28 | BIT27 | BIT26)
#define B_PCIE_CFG_LTRSUB_LTRNSLSUBV                  BIT16
#define N_PCIE_CFG_LTRSUB_LTRNSLSUBV                  16
#define B_PCIE_CFG_LTRSUB_LTRNSLSUBV_MASK             0x03FF0000
#define B_PCIE_CFG_LTRSUB_LTRSLSUBEN                  BIT15
#define B_PCIE_CFG_LTRSUB_LTRSLSSUBV                  BIT10
#define N_PCIE_CFG_LTRSUB_LTRSLSSUBV                  10
#define B_PCIE_CFG_LTRSUB_LTRSLSSUBV_MASK             (BIT12 | BIT11 | BIT10)
#define B_PCIE_CFG_LTRSUB_LTRSLSUBV                   BIT0
#define N_PCIE_CFG_LTRSUB_LTRSLSUBV                   0
#define B_PCIE_CFG_LTRSUB_LTRSLSUBV_MASK              0x000003FF

#define R_PCIE_CFG_LTRSUBL11NPG                       0xC68

#define R_PCIE_CFG_VNNREMCTL                          0xC70
#define B_PCIE_CFG_VNNREMCTL_LRSLFVNNRE               (BIT1 | BIT0)
#define V_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_4_OSC_CLK     0x0
#define V_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_8_OSC_CLK     0x1
#define V_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_16_OSC_CLK    0x2
#define V_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_32_OSC_CLK    0x3
#define B_PCIE_CFG_VNNREMCTL_ISPLFVNNRE               (BIT3 | BIT2)
#define N_PCIE_CFG_VNNREMCTL_ISPLFVNNRE               2
#define V_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_8_OSC_CLK     0x0
#define V_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_16_OSC_CLK    0x1
#define V_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_32_OSC_CLK    0x2
#define V_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_64_OSC_CLK    0x3
#define B_PCIE_CFG_VNNREMCTL_LDVNNRE                  BIT4
#define B_PCIE_CFG_VNNREMCTL_RTD3VNNRE                BIT5
#define B_PCIE_CFG_VNNREMCTL_DNPVNNRE                 BIT6
#define B_PCIE_CFG_VNNREMCTL_HPVNNRE                  BIT7
#define B_PCIE_CFG_VNNREMCTL_FDVNNRE                  BIT8

#define R_PCIE_CFG_VNNRSNRC1                          0xC74

#define R_PCIE_CFG_AECR1G3                            0xC80
#define B_PCIE_CFG_AECR1G3_DTCGCM                     BIT14
#define B_PCIE_CFG_AECR1G3_TPSE                       BIT10
#define B_PCIE_CFG_AECR1G3_L1OFFRDYHEWT               BIT7
#define B_PCIE_CFG_AECR1G3_L1OFFRDYHEWTEN             BIT6
#define B_PCIE_CFG_AECR1G3_DCDCCTDT                   BIT0

#define R_PCIE_CFG_LPCR                               0xC8C
#define B_PCIE_CFG_LPCR_DIDOVR_LOCK                   BIT24
#define B_PCIE_CFG_LPCR_LTRCFGLOCK                    BIT16
#define B_PCIE_CFG_LPCR_SERL                          BIT8
#define B_PCIE_CFG_LPCR_SRL                           BIT0

#define R_PCIE_CFG_EINJCTL                            0xCA8
#define B_PCIE_CFG_EINJCTL_EINJDIS                    BIT0

#define R_PCIE_CFG_RXMC1                              0xC90
#define B_PCIE_CFG_RXMC1_MSRVS                        BIT26
#define B_PCIE_CFG_RXMC1_MSRVS_OFFSET                 26
#define B_PCIE_CFG_RXMC1_MSRVS_MASK                   0xFC000000
#define V_PCIE_CFG_RXMC1_MSRVS                        0x13
#define B_PCIE_CFG_RXMC1_MSRTS                        BIT20
#define B_PCIE_CFG_RXMC1_MSRTS_OFFSET                 20
#define B_PCIE_CFG_RXMC1_MSRTS_MASK                   0x3F00000
#define V_PCIE_CFG_RXMC1_MSRTS                        0x3F
#define B_PCIE_CFG_RXMC1_TMSLOP                       BIT13
#define B_PCIE_CFG_RXMC1_TMSLOP_OFFSET                13
#define B_PCIE_CFG_RXMC1_TMSLOP_MASK                  0x6000
#define V_PCIE_CFG_RXMC1_TMSLOP                       0x1
#define B_PCIE_CFG_RXMC1_VMSLOP                       BIT11
#define B_PCIE_CFG_RXMC1_VMSLOP_OFFSET                11
#define B_PCIE_CFG_RXMC1_VMSLOP_MASK                  0x1800
#define V_PCIE_CFG_RXMC1_VMSLOP                       0x1
#define B_PCIE_CFG_RXMC1_MSRM                         BIT9
#define B_PCIE_CFG_RXMC1_MMNOLS                       BIT4
#define B_PCIE_CFG_RXMC1_MMNOLS_OFFSET                4
#define B_PCIE_CFG_RXMC1_MMNOLS_MASK                  0x1F0
#define V_PCIE_CFG_RXMC1_MMNOLS                       0x1
#define B_PCIE_CFG_RXMC1_MVS                          BIT3
#define B_PCIE_CFG_RXMC1_MILRTS                       BIT2
#define B_PCIE_CFG_RXMC1_MIUDVMS                      BIT1
#define B_PCIE_CFG_RXMC1_MIESS                        BIT0

#define R_PCIE_CFG_RXMC2                              0xC94
#define B_PCIE_CFG_RXMC2_MNOTSS                       BIT19
#define B_PCIE_CFG_RXMC2_MNOTSS_OFFSET                19
#define B_PCIE_CFG_RXMC2_MNOTSS_MASK                  0x1F80000
#define V_PCIE_CFG_RXMC2_MNOTSS                       0x20
#define B_PCIE_CFG_RXMC2_MMTOS                        BIT13
#define B_PCIE_CFG_RXMC2_MMTOS_OFFSET                 13
#define B_PCIE_CFG_RXMC2_MMTOS_MASK                   0x7E000
#define V_PCIE_CFG_RXMC2_MMTOS                        0x1C
#define B_PCIE_CFG_RXMC2_MNOVSS                       BIT6
#define B_PCIE_CFG_RXMC2_MNOVSS_OFFSET                6
#define B_PCIE_CFG_RXMC2_MNOVSS_MASK                  0x1FC0
#define V_PCIE_CFG_RXMC2_MNOVSS                       0x1C
#define B_PCIE_CFG_RXMC2_MMVOS                        BIT0
#define B_PCIE_CFG_RXMC2_MMVOS_OFFSET                 0
#define B_PCIE_CFG_RXMC2_MMVOS_MASK                   0x3F
#define V_PCIE_CFG_RXMC2_MMVOS                        0x0

#define R_PCIE_CFG_PL16MPCPS                          0xEE0
#define B_PCIE_CFG_PL16MPCPS_MARGINDRISW              BIT0

//
// PCIe RCRB space available from SIP17
//
#define R_PCIE_RCRB_FCTL                                  0x1300               ///< SIP17 Feature Control
#define B_PCIE_RCRB_FCTL_CRSPSEL                          BIT12
#define B_PCIE_RCRB_FCTL_CRSPSEL_OFFSET                   12

#define R_PCIE_RCRB_TXCRSTOCTL                            0x1320
#define B_PCIE_RCRB_TXCRSTOCTL_TXNPCTODIS                 BIT3

#define R_PCIE_RCRB_FCTL2                                 0x1330               ///< SIP17 Feature Control 2
#define B_PCIE_RCRB_FCTL2_RXCPPREALLOCEN                  BIT27
#define B_PCIE_RCRB_FCTL2_BLKNAT                          BIT11
#define B_PCIE_RCRB_FCTL2_HPICTL                          BIT10
#define B_PCIE_RCRB_FCTL2_HRTCTL                          (BIT9 | BIT8 | BIT7)
#define B_PCIE_RCRB_FCTL2_HRTCTL_OFFSET                   7
#define V_PCIE_RCRB_FCTL2_HRTCTL                          0x1
#define B_PCIE_RCRB_FCTL2_RPTOT                           (BIT6 | BIT5 | BIT4)
#define B_PCIE_RCRB_FCTL2_RPTOT_OFFSET                    4
#define V_PCIE_RCRB_FCTL2_RPTOT                           0x5

#define R_PCIE_RCRB_FCUCTL                                0x1304               ///< FC Update Control
#define B_PCIE_RCRB_FCUCTL_FC_CP_FCM_VCM                  BIT28
#define B_PCIE_RCRB_FCUCTL_FC_CP_FCM_VC1                  BIT27
#define B_PCIE_RCRB_FCUCTL_FC_CP_FCM                      BIT20

#define R_PCIE_RCRB_RPR                                   0x1334               ///< SIP17 Reset Policy
#define B_PCIE_RCRB_RPR_S5SMTO                            (BIT23 | BIT22)
#define B_PCIE_RCRB_RPR_S5SMTO_OFFSET                     22
#define V_PCIE_RCRB_RPR_S5SMTO                            0x1
#define B_PCIE_RCRB_RPR_S4SMTO                            (BIT21 | BIT20)
#define B_PCIE_RCRB_RPR_S4SMTO_OFFSET                     20
#define V_PCIE_RCRB_RPR_S4SMTO                            0x1
#define B_PCIE_RCRB_RPR_S3SMTO                            (BIT19 | BIT18)
#define B_PCIE_RCRB_RPR_S3SMTO_OFFSET                     18
#define V_PCIE_RCRB_RPR_S3SMTO                            0x1
#define B_PCIE_RCRB_RPR_CRMTO                             (BIT17 | BIT16)
#define B_PCIE_RCRB_RPR_CRMTO_OFFSET                      16
#define V_PCIE_RCRB_RPR_CRMTO                             0x1
#define B_PCIE_RCRB_RPR_WRMTO                             (BIT15 | BIT14)
#define B_PCIE_RCRB_RPR_WRMTO_OFFSET                      14
#define V_PCIE_RCRB_RPR_WRMTO                             0x1
#define B_PCIE_RCRB_RPR_DMTO                              (BIT13 | BIT12)
#define B_PCIE_RCRB_RPR_DMTO_OFFSET                       12
#define V_PCIE_RCRB_RPR_DMTO                              0x1
#define B_PCIE_RCRB_RPR_S5SM                              (BIT11 | BIT10)
#define B_PCIE_RCRB_RPR_S5SM_OFFSET                       10
#define V_PCIE_RCRB_RPR_S5SM                              0x1
#define B_PCIE_RCRB_RPR_S4SM                              (BIT9 | BIT8)
#define B_PCIE_RCRB_RPR_S4SM_OFFSET                       8
#define V_PCIE_RCRB_RPR_S4SM                              0x1
#define B_PCIE_RCRB_RPR_S3SM                              (BIT7 | BIT6)
#define B_PCIE_RCRB_RPR_S3SM_OFFSET                       6
#define V_PCIE_RCRB_RPR_S3SM                              0x1
#define B_PCIE_RCRB_RPR_CRM                               (BIT5 | BIT4)
#define B_PCIE_RCRB_RPR_CRM_OFFSET                        4
#define V_PCIE_RCRB_RPR_CRM                               0x1
#define B_PCIE_RCRB_RPR_WRM                               (BIT3 | BIT2)
#define B_PCIE_RCRB_RPR_WRM_OFFSET                        2
#define V_PCIE_RCRB_RPR_WRM                               0x1

#define R_PCIE_RCRB_DCGEN1                                0x1350
#define B_PCIE_RCRB_DCGEN1_PXCP4ULDCGEN                   BIT27
#define B_PCIE_RCRB_DCGEN1_PXTTSSULDCGEN                  BIT26
#define B_PCIE_RCRB_DCGEN1_PXTTSULDCGEN                   BIT25
#define B_PCIE_RCRB_DCGEN1_PXTTULDCGEN                    BIT24
#define B_PCIE_RCRB_DCGEN1_PXCP3ULDCGEN                   BIT19
#define B_PCIE_RCRB_DCGEN1_PXTRSSULDCGEN                  BIT18
#define B_PCIE_RCRB_DCGEN1_PXTRSULDCGEN                   BIT17
#define B_PCIE_RCRB_DCGEN1_PXTRULDCGEN                    BIT16
#define B_PCIE_RCRB_DCGEN1_PXCP2ULDCGEN                   BIT12
#define B_PCIE_RCRB_DCGEN1_PXLSULDCGEN                    BIT11
#define B_PCIE_RCRB_DCGEN1_PXLIULDCGEN                    BIT10
#define B_PCIE_RCRB_DCGEN1_PXLTULDCGEN                    BIT9
#define B_PCIE_RCRB_DCGEN1_PXLRULDCGEN                    BIT8
#define B_PCIE_RCRB_DCGEN1_PXCULDCGEN                     BIT1
#define B_PCIE_RCRB_DCGEN1_PXKGULDCGEN                    BIT0

#define R_PCIE_RCRB_DCGEN2                                0x1354
#define B_PCIE_RCRB_DCGEN2_PXSDRULDCGEN                   BIT17
#define B_PCIE_RCRB_DCGEN2_PXSDTULDCGEN                   BIT16
#define B_PCIE_RCRB_DCGEN2_PXSDIULDCGEN                   BIT15
#define B_PCIE_RCRB_DCGEN2_PXCP5ULDCGEN                   BIT6
#define B_PCIE_RCRB_DCGEN2_PXFRULDCGEN                    BIT5
#define B_PCIE_RCRB_DCGEN2_PXFTULDCGEN                    BIT4
#define B_PCIE_RCRB_DCGEN2_PXFIULDCGEN                    BIT3
#define B_PCIE_RCRB_DCGEN2_PXPBULDCGEN                    BIT2
#define B_PCIE_RCRB_DCGEN2_PXPSULDCGEN                    BIT1
#define B_PCIE_RCRB_DCGEN2_PXPIULDCGEN                    BIT0

#define R_PCIE_RCRB_DCGM1                                 0x1358
#define B_PCIE_RCRB_DCGM1_PXCP4ULDCGM                     BIT27
#define B_PCIE_RCRB_DCGM1_PXTTSSULDCGM                    BIT26
#define B_PCIE_RCRB_DCGM1_PXTTSULDCGM                     BIT25
#define B_PCIE_RCRB_DCGM1_PXTTULDCGM                      BIT24
#define B_PCIE_RCRB_DCGM1_PXCP3ULDCGM                     BIT19
#define B_PCIE_RCRB_DCGM1_PXTRSSULDCGM                    BIT18
#define B_PCIE_RCRB_DCGM1_PXTRSULDCGM                     BIT17
#define B_PCIE_RCRB_DCGM1_PXTRULDCGM                      BIT16
#define B_PCIE_RCRB_DCGM1_PXCP2ULDCGM                     BIT12
#define B_PCIE_RCRB_DCGM1_PXLSULDCGM                      BIT11
#define B_PCIE_RCRB_DCGM1_PXLIULDCGM                      BIT10
#define B_PCIE_RCRB_DCGM1_PXLTULDCGM                      BIT9
#define B_PCIE_RCRB_DCGM1_PXLRULDCGM                      BIT8
#define B_PCIE_RCRB_DCGM1_PXCULDCGM                       BIT1
#define B_PCIE_RCRB_DCGM1_PXKGULDCGM                      BIT0

#define R_PCIE_RCRB_DCGM2                                 0x135C
#define B_PCIE_RCRB_DCGM2_PXSDRULDCGM                     BIT17
#define B_PCIE_RCRB_DCGM2_PXSDTULDCGM                     BIT16
#define B_PCIE_RCRB_DCGM2_PXSDIULDCGM                     BIT15
#define B_PCIE_RCRB_DCGM2_PXCP5ULDCGM                     BIT6
#define B_PCIE_RCRB_DCGM2_PXFRULDCGM                      BIT5
#define B_PCIE_RCRB_DCGM2_PXFTULDCGM                      BIT4
#define B_PCIE_RCRB_DCGM2_PXFIULDCGM                      BIT3
#define B_PCIE_RCRB_DCGM2_PXPBULDCGM                      BIT2
#define B_PCIE_RCRB_DCGM2_PXPSULDCGM                      BIT1
#define B_PCIE_RCRB_DCGM2_PXPIULDCGM                      BIT0

#define R_PCIE_RCRB_DCGEN3                                0x1360
#define B_PCIE_RCRB_DCGEN3_PXCP4UPDCGEN                   BIT27
#define B_PCIE_RCRB_DCGEN3_PXTTSSUPDCGEN                  BIT26
#define B_PCIE_RCRB_DCGEN3_PXTTSUPDCGEN                   BIT25
#define B_PCIE_RCRB_DCGEN3_PXTTUPDCGEN                    BIT24
#define B_PCIE_RCRB_DCGEN3_PXCP3UPDCGEN                   BIT20
#define B_PCIE_RCRB_DCGEN3_PXTOUPDCGEN                    BIT19
#define B_PCIE_RCRB_DCGEN3_PXTRSSUPDCGEN                  BIT18
#define B_PCIE_RCRB_DCGEN3_PXTRSUPDCGEN                   BIT17
#define B_PCIE_RCRB_DCGEN3_PXTRUPDCGEN                    BIT16
#define B_PCIE_RCRB_DCGEN3_PXCP2UPDCGEN                   BIT12
#define B_PCIE_RCRB_DCGEN3_PXLIUPDCGEN                    BIT10
#define B_PCIE_RCRB_DCGEN3_PXLTUPDCGEN                    BIT9
#define B_PCIE_RCRB_DCGEN3_PXLRUPDCGEN                    BIT8
#define B_PCIE_RCRB_DCGEN3_PXSRUSSNRDCGEN                 BIT6
#define B_PCIE_RCRB_DCGEN3_PXCUPSNRDCGEN                  BIT5
#define B_PCIE_RCRB_DCGEN3_PXCUPSRCDCGEN                  BIT4
#define B_PCIE_RCRB_DCGEN3_PXCUPDCGEN                     BIT2
#define B_PCIE_RCRB_DCGEN3_PXBUPDCGEN                     BIT1
#define B_PCIE_RCRB_DCGEN3_PXEUPDCGEN                     BIT0

#define R_PCIE_RCRB_DCGM3                                 0x1368
#define B_PCIE_RCRB_DCGM3_PXCP4UPDCGM                     BIT27
#define B_PCIE_RCRB_DCGM3_PXTTSSUPDCGM                    BIT26
#define B_PCIE_RCRB_DCGM3_PXTTSUPDCGM                     BIT25
#define B_PCIE_RCRB_DCGM3_PXTTUPDCGM                      BIT24
#define B_PCIE_RCRB_DCGM3_PXCP3UPDCGM                     BIT20
#define B_PCIE_RCRB_DCGM3_PXTOUPDCGM                      BIT19
#define B_PCIE_RCRB_DCGM3_PXTRSSUPDCGM                    BIT18
#define B_PCIE_RCRB_DCGM3_PXTRSUPDCGM                     BIT17
#define B_PCIE_RCRB_DCGM3_PXTRUPDCGM                      BIT16
#define B_PCIE_RCRB_DCGM3_PXCP2UPDCGM                     BIT12
#define B_PCIE_RCRB_DCGM3_PXLIUPDCGM                      BIT10
#define B_PCIE_RCRB_DCGM3_PXLTUPDCGM                      BIT9
#define B_PCIE_RCRB_DCGM3_PXLRUPDCGM                      BIT8
#define B_PCIE_RCRB_DCGM3_PXSRUSSNRDCGM                   BIT6
#define B_PCIE_RCRB_DCGM3_PXCUPSNRDCGM                    BIT5
#define B_PCIE_RCRB_DCGM3_PXCUPSRCDCGM                    BIT4
#define B_PCIE_RCRB_DCGM3_PXCUPDCGM                       BIT2
#define B_PCIE_RCRB_DCGM3_PXBUPDCGM                       BIT1
#define B_PCIE_RCRB_DCGM3_PXEUPDCGM                       BIT0

#define R_PCIE_RCRB_DCGM4                                 0x136C
#define B_PCIE_RCRB_DCGM4_PXCP6UPDCGM                     BIT8
#define B_PCIE_RCRB_DCGM4_PXCP5UPDCGM                     BIT0

#define R_PCIE_RCRB_IPCLKCTR                              0x1370
#define B_PCIE_RCRB_IPCLKCTR_MDPCCEN                      BIT3
#define B_PCIE_RCRB_IPCLKCTR_PXDCGE                       BIT2

#define R_PCIE_RCRB_LPHYCP1                               0x1410
#define B_PCIE_RCRB_LPHYCP1_BPRXSTNDBYHSRECAL             BIT12
#define B_PCIE_RCRB_LPHYCP1_RXADPRECE                     BIT9
#define B_PCIE_RCRB_LPHYCP1_PIPEMBIP                      BIT6
#define B_PCIE_RCRB_LPHYCP1_RXEQFNEVC                     BIT4
#define B_PCIE_RCRB_LPHYCP1_RXADPHM                       BIT3
#define B_PCIE_RCRB_LPHYCP1_RXADPSVH                      BIT2

#define R_PCIE_RCRB_UPDLPHYCP                             0x1414
#define B_PCIE_RCRB_UPDLPHYCP_UPDLTCDC                    BIT1
#define B_PCIE_RCRB_UPDLPHYCP_UPDLLSMFLD                  BIT0

#define R_PCIE_RCRB_LPHYCP4                               0x1434
#define B_PCIE_RCRB_LPHYCP4_RXL0SEG12FTSE                 BIT23
#define B_PCIE_RCRB_LPHYCP4_RXL0SEG12FTSE_OFFSET          23
#define V_PCIE_RCRB_LPHYCP4_RXL0SEG12FTSE                 0x1
#define B_PCIE_RCRB_LPHYCP4_G5AREDRL0S                    BIT20
#define B_PCIE_RCRB_LPHYCP4_G4AREDRL0S                    BIT19
#define B_PCIE_RCRB_LPHYCP4_G2AREDRL0S                    BIT18
#define B_PCIE_RCRB_LPHYCP4_G1AREDRL0S                    BIT16
#define B_PCIE_RCRB_LPHYCP4_DPMDNDBFE                     BIT12
#define B_PCIE_RCRB_LPHYCP4_DPMDNDBFE_OFFSET              12
#define B_PCIE_RCRB_LPHYCP4_REEFRXL0SED                   BIT11
#define B_PCIE_RCRB_LPHYCP4_REEFRXL0SED_OFFSET            11
#define B_PCIE_RCRB_LPHYCP4_PLLBUSDRC                     BIT10
#define B_PCIE_RCRB_LPHYCP4_OSUSSP                        BIT8
#define B_PCIE_RCRB_LPHYCP4_OSUSSP_OFFSET                 8
#define B_PCIE_RCRB_LPHYCP4_OSUSSP_MASK                   0x300
#define V_PCIE_RCRB_LPHYCP4_OSUSSP                        0x1
#define B_PCIE_RCRB_LPHYCP4_LGUSSP                        BIT6
#define B_PCIE_RCRB_LPHYCP4_LGUSSP_OFFSET                 6
#define B_PCIE_RCRB_LPHYCP4_LGUSSP_MASK                   0xC0
#define V_PCIE_RCRB_LPHYCP4_LGUSSP                        0x1
#define B_PCIE_RCRB_LPHYCP4_OSCCLKSQEXITDBTIMERS          BIT3
#define B_PCIE_RCRB_LPHYCP4_OSCCLKSQEXITDBTIMERS_OFFSET   3
#define B_PCIE_RCRB_LPHYCP4_OSCCLKSQEXITDBTIMERS_MASK     0x38
#define V_PCIE_RCRB_LPHYCP4_OSCCLKSQEXITDBTIMERS          0x2
#define B_PCIE_RCRB_LPHYCP4_LGCLKSQEXITDBTIMERS           BIT0
#define B_PCIE_RCRB_LPHYCP4_LGCLKSQEXITDBTIMERS_OFFSET    0
#define B_PCIE_RCRB_LPHYCP4_LGCLKSQEXITDBTIMERS_MASK      0x7
#define V_PCIE_RCRB_LPHYCP4_LGCLKSQEXITDBTIMERS           0x2

#define R_PCIE_RCRB_IORCCP1                               0x144C
#define B_PCIE_RCRB_IORCCP1_G5ORRRXECC_MASK               (BIT30 | BIT29 | BIT28)
#define B_PCIE_RCRB_IORCCP1_G5ORRRXECC_OFFSET             28
#define V_PCIE_RCRB_IORCCP1_G5ORRRXECC                    0x1
#define B_PCIE_RCRB_IORCCP1_G4ORRRXECC_MASK               (BIT27 | BIT26 | BIT25)
#define B_PCIE_RCRB_IORCCP1_G4ORRRXECC_OFFSET             25
#define V_PCIE_RCRB_IORCCP1_G4ORRRXECC                    0x1
#define B_PCIE_RCRB_IORCCP1_G3ORRRXECC_MASK               (BIT24 | BIT23 | BIT22)
#define B_PCIE_RCRB_IORCCP1_G3ORRRXECC_OFFSET             22
#define V_PCIE_RCRB_IORCCP1_G3ORRRXECC                    0x1
#define B_PCIE_RCRB_IORCCP1_G2ORRRXECC_MASK               (BIT21 | BIT20 | BIT19)
#define B_PCIE_RCRB_IORCCP1_G2ORRRXECC_OFFSET             19
#define V_PCIE_RCRB_IORCCP1_G2ORRRXECC                    0x1
#define B_PCIE_RCRB_IORCCP1_G1ORRRXECC_MASK               (BIT18 | BIT17 | BIT16)
#define B_PCIE_RCRB_IORCCP1_G1ORRRXECC_OFFSET             16
#define V_PCIE_RCRB_IORCCP1_G1ORRRXECC                    0x1
#define B_PCIE_RCRB_IORCCP1_DISORCRODI                    BIT2
#define B_PCIE_RCRB_IORCCP1_DRCORRP                       BIT1
#define B_PCIE_RCRB_IORCCP1_DISORCL12REC                  BIT0

#define R_PCIE_RCRB_PIPEPDCTL                             0x1594               ///< SIP17 PIPE Power Down Control
#define B_PCIE_RCRB_PIPEPDCTL_L1PGUPGPDCTL_MASK           (BIT31 | BIT30 | BIT29 | BIT28)
#define B_PCIE_RCRB_PIPEPDCTL_L1PGUPGPDCTL_OFFSET         28
#define V_PCIE_RCRB_PIPEPDCTL_L1PGUPGPDCTL                0x4
#define B_PCIE_RCRB_PIPEPDCTL_L1PGNOPGPDCTL_MASK          (BIT27 | BIT26 | BIT25 | BIT24)
#define B_PCIE_RCRB_PIPEPDCTL_L1PGNOPGPDCTL_OFFSET        24
#define V_PCIE_RCRB_PIPEPDCTL_L1PGNOPGPDCTL               0x4
#define B_PCIE_RCRB_PIPEPDCTL_DISPGPDCTL_MASK             (BIT23 | BIT22 | BIT21 | BIT20)
#define B_PCIE_RCRB_PIPEPDCTL_DISPGPDCTL_OFFSET           20
#define V_PCIE_RCRB_PIPEPDCTL_DISPGPDCTL                  0x3
#define B_PCIE_RCRB_PIPEPDCTL_DISNOPGPDCTL_MASK           (BIT19 | BIT18 | BIT17 | BIT16)
#define B_PCIE_RCRB_PIPEPDCTL_DISNOPGPDCTL_OFFSET         16
#define V_PCIE_RCRB_PIPEPDCTL_DISNOPGPDCTL                0x3
#define B_PCIE_RCRB_PIPEPDCTL_L23PGPDCTL_MASK             (BIT15 | BIT14 | BIT13 | BIT12)
#define B_PCIE_RCRB_PIPEPDCTL_L23PGPDCTL_OFFSET           12
#define V_PCIE_RCRB_PIPEPDCTL_L23PGPDCTL                  0x3
#define B_PCIE_RCRB_PIPEPDCTL_L23NOPGPDCTL_MASK           (BIT11 | BIT10 | BIT9 | BIT8)
#define B_PCIE_RCRB_PIPEPDCTL_L23NOPGPDCTL_OFFSET         8
#define V_PCIE_RCRB_PIPEPDCTL_L23NOPGPDCTL                0x3
#define B_PCIE_RCRB_PIPEPDCTL_DETPGPDCTL_MASK             (BIT7 | BIT6 | BIT5 | BIT4)
#define B_PCIE_RCRB_PIPEPDCTL_DETPGPDCTL_OFFSET           4
#define V_PCIE_RCRB_PIPEPDCTL_DETPGPDCTL                  0x3
#define B_PCIE_RCRB_PIPEPDCTL_DETNOPGPDCTL_MASK           (BIT3 | BIT2 | BIT1 | BIT0)
#define B_PCIE_RCRB_PIPEPDCTL_DETNOPGPDCTL_OFFSET         0
#define V_PCIE_RCRB_PIPEPDCTL_DETNOPGPDCTL                0x3

#define R_PCIE_RCRB_PIPEPDCTL2                            0x1598               ///< SIP17 PIPE Power Down Control 2
#define B_PCIE_RCRB_PIPEPDCTL2_DUCFGPGPDCTL_MASK          (BIT31 | BIT30 | BIT29 | BIT28)
#define B_PCIE_RCRB_PIPEPDCTL2_DUCFGPGPDCTL_OFFSET        28
#define V_PCIE_RCRB_PIPEPDCTL2_DUCFGPGPDCTL               0x3
#define B_PCIE_RCRB_PIPEPDCTL2_DUCFGNOPGPDCTL_MASK        (BIT27 | BIT26 | BIT25 | BIT24)
#define B_PCIE_RCRB_PIPEPDCTL2_DUCFGNOPGPDCTL_OFFSET      24
#define V_PCIE_RCRB_PIPEPDCTL2_DUCFGNOPGPDCTL             0x3
#define B_PCIE_RCRB_PIPEPDCTL2_L1D2PGPDCTL_MASK           (BIT23 | BIT22 | BIT21 | BIT20)
#define B_PCIE_RCRB_PIPEPDCTL2_L1D2PGPDCTL_OFFSET         20
#define V_PCIE_RCRB_PIPEPDCTL2_L1D2PGPDCTL                0x4
#define B_PCIE_RCRB_PIPEPDCTL2_L1D2NOPGPDCTL_MASK         (BIT19 | BIT18 | BIT17 | BIT16)
#define B_PCIE_RCRB_PIPEPDCTL2_L1D2NOPGPDCTL_OFFSET       16
#define V_PCIE_RCRB_PIPEPDCTL2_L1D2NOPGPDCTL              0x4
#define B_PCIE_RCRB_PIPEPDCTL2_L1D1PGPGPDCTL_MASK         (BIT15 | BIT14 | BIT13 | BIT12)
#define B_PCIE_RCRB_PIPEPDCTL2_L1D1PGPGPDCTL_OFFSET       12
#define V_PCIE_RCRB_PIPEPDCTL2_L1D1PGPGPDCTL              0x4
#define B_PCIE_RCRB_PIPEPDCTL2_L1D1PGNOPGPDCTL_MASK       (BIT11 | BIT10 | BIT9 | BIT8)
#define B_PCIE_RCRB_PIPEPDCTL2_L1D1PGNOPGPDCTL_OFFSET     8
#define V_PCIE_RCRB_PIPEPDCTL2_L1D1PGNOPGPDCTL            0x4
#define B_PCIE_RCRB_PIPEPDCTL2_L1D1UPUPGPDCTL_MASK        (BIT7 | BIT6 | BIT5 | BIT4)
#define B_PCIE_RCRB_PIPEPDCTL2_L1D1UPUPGPDCTL_OFFSET      4
#define V_PCIE_RCRB_PIPEPDCTL2_L1D1UPUPGPDCTL_DMI         0x6
#define V_PCIE_RCRB_PIPEPDCTL2_L1D1UPUPGPDCTL_PCIE        0x0
#define B_PCIE_RCRB_PIPEPDCTL2_L1UPNOPGPDCTL_MASK         (BIT3 | BIT2 | BIT1 | BIT0)
#define B_PCIE_RCRB_PIPEPDCTL2_L1UPNOPGPDCTL_OFFSET       0
#define V_PCIE_RCRB_PIPEPDCTL2_L1UPNOPGPDCTL              0x6

#define R_PCIE_RCRB_PIPEPDCTL3                            0x159C               ///< SIP17 PIPE Power Down Control 3
#define B_PCIE_RCRB_PIPEPDCTL3_L1DLOWPGPDCTL_MASK         (BIT7 | BIT6 | BIT5 | BIT4)
#define B_PCIE_RCRB_PIPEPDCTL3_L1DLOWPGPDCTL_OFFSET       4
#define V_PCIE_RCRB_PIPEPDCTL3_L1DLOWPGPDCTL              0x4
#define B_PCIE_RCRB_PIPEPDCTL3_L1DLOWNOPGPDCTL_MASK       (BIT3 | BIT2 | BIT1 | BIT0)
#define B_PCIE_RCRB_PIPEPDCTL3_L1DLOWNOPGPDCTL_OFFSET     0
#define V_PCIE_RCRB_PIPEPDCTL3_L1DLOWNOPGPDCTL            0x4

#define R_PCIE_RCRB_PIPEPDCTLEXT                          0x15A0               ///< SIP17 PIPE Power Down Control Extension
#define V_PCIE_RCRB_PIPEPDCTLEXT_LSDPMRFM                 0xE
#define B_PCIE_RCRB_PIPEPDCTLEXT_LSDPMRFM                 BIT4
#define B_PCIE_RCRB_PIPEPDCTLEXT_LSDPMRFM_OFFSET          0x4
#define B_PCIE_RCRB_PIPEPDCTLEXT_P2UGTPGSM                BIT3
#define B_PCIE_RCRB_PIPEPDCTLEXT_P2TP2TP                  BIT2
#define B_PCIE_RCRB_PIPEPDCTLEXT_P2TP2TCD                 BIT1

#define R_PCIE_RCRB_MPHYCAPCFG                            0x15A8
#define B_PCIE_RCRB_MPHYCAPCFG_MLSOSRSSVCC_MASK           0x3F80
#define N_PCIE_RCRB_MPHYCAPCFG_MLSOSRSSVCC                7
#define B_PCIE_RCRB_MPHYCAPCFG_MLSOSGSSVCC_MASK           0x7F
#define N_PCIE_RCRB_MPHYCAPCFG_MLSOSGSSVCC                0

#define R_PCIE_RCRB_RPDEC1                                0x1780               ///< SIP17 Reset Prep Decode
#define B_PCIE_RCRB_RPDEC1_RPCREPT                        0xFF000000
#define B_PCIE_RCRB_RPDEC1_RPCREPT_OFFSET                 24
#define V_PCIE_RCRB_RPDEC1_RPCREPT                        0x3
#define B_PCIE_RCRB_RPDEC1_RPCRERT                        0xFF0000
#define B_PCIE_RCRB_RPDEC1_RPCRERT_OFFSET                 16
#define V_PCIE_RCRB_RPDEC1_RPCRERT                        0x11
#define B_PCIE_RCRB_RPDEC1_RPWREPT                        0xFF00
#define B_PCIE_RCRB_RPDEC1_RPWREPT_OFFSET                 8
#define V_PCIE_RCRB_RPDEC1_RPWREPT                        0x3
#define B_PCIE_RCRB_RPDEC1_RPWRERT                        0xFF
#define B_PCIE_RCRB_RPDEC1_RPWRERT_OFFSET                 0
#define V_PCIE_RCRB_RPDEC1_RPWRERT                        0x10

#define R_PCIE_RCRB_RPDEC2                                0x1784               ///< SIP17 Reset Prep Decode 2
#define B_PCIE_RCRB_RPDEC2_RPS4EPT                        0xFF000000
#define B_PCIE_RCRB_RPDEC2_RPS4EPT_OFFSET                 24
#define V_PCIE_RCRB_RPDEC2_RPS4EPT                        0x3
#define B_PCIE_RCRB_RPDEC2_RPS4ERT                        0xFF0000
#define B_PCIE_RCRB_RPDEC2_RPS4ERT_OFFSET                 16
#define V_PCIE_RCRB_RPDEC2_RPS4ERT                        0x4
#define B_PCIE_RCRB_RPDEC2_RPS3EPT                        0xFF00
#define B_PCIE_RCRB_RPDEC2_RPS3EPT_OFFSET                 8
#define V_PCIE_RCRB_RPDEC2_RPS3EPT                        0x3
#define B_PCIE_RCRB_RPDEC2_RPS3ERT                        0xFF
#define B_PCIE_RCRB_RPDEC2_RPS3ERT_OFFSET                 0
#define V_PCIE_RCRB_RPDEC2_RPS3ERT                        0x3

#define R_PCIE_RCRB_RPDEC3                                0x1788               ///< SIP17 Reset Prep Decode 3
#define B_PCIE_RCRB_RPDEC3_RPDH                           (BIT18 | BIT17 | BIT16)
#define B_PCIE_RCRB_RPDEC3_RPDH_OFFSET                    16
#define V_PCIE_RCRB_RPDEC3_RPDH                           0x5
#define B_PCIE_RCRB_RPDEC3_RPS5EPT                        0xFF00
#define B_PCIE_RCRB_RPDEC3_RPS5EPT_OFFSET                 8
#define V_PCIE_RCRB_RPDEC3_RPS5EPT                        0x3
#define B_PCIE_RCRB_RPDEC3_RPS5ERT                        0xFF
#define B_PCIE_RCRB_RPDEC3_RPS5ERT_OFFSET                 0
#define V_PCIE_RCRB_RPDEC3_RPS5ERT                        0x5

#define R_PCIE_RCRB_TXMDEC1                               0x17A0
#define B_PCIE_RCRB_CRIDBNCD                              BIT0
#define B_PCIE_RCRB_MRBIDBNCD                             BIT1

#define R_PCIE_RCRB_CFG_DECCTL                            0x1904
#define B_PCIE_RCRB_CFG_DECCTL_RXIMDECEN                  BIT31
#define B_PCIE_RCRB_CFG_DECCTL_URRXMCTPNTCO               BIT30
#define B_PCIE_RCRB_CFG_DECCTL_RXMCTPBRCDECEN             BIT29
#define B_PCIE_RCRB_CFG_DECCTL_URRXUVDMRGRTRC             BIT28
#define B_PCIE_RCRB_CFG_DECCTL_URRXUVDMRBFRC              BIT27
#define B_PCIE_RCRB_CFG_DECCTL_LCRXPTMREQ                 BIT26
#define B_PCIE_RCRB_CFG_DECCTL_RSVD_RW                    BIT25
#define B_PCIE_RCRB_CFG_DECCTL_LCRXERRMSG                 BIT24
#define B_PCIE_RCRB_CFG_DECCTL_RXLTRMDECH                 BIT23
#define B_PCIE_RCRB_CFG_DECCTL_RXADLEDDECEN               BIT22
#define B_PCIE_RCRB_CFG_DECCTL_RXSBEMCAPDECEN             BIT21
#define B_PCIE_RCRB_CFG_DECCTL_RXGPEDECEN                 BIT20
#define B_PCIE_RCRB_CFG_DECCTL_RXVDMDECE                  BIT19
#define B_PCIE_RCRB_CFG_DECCTL_RXMCTPDECEN                BIT18
#define B_PCIE_RCRB_CFG_DECCTL_ICHKINVREQRMSGRBID         BIT17
#define B_PCIE_RCRB_CFG_DECCTL_URRXVMCTPBFRC              BIT16
#define B_PCIE_RCRB_CFG_DECCTL_URRXUORVDM                 BIT15
#define B_PCIE_RCRB_CFG_DECCTL_URRXURIDVDM                BIT14
#define B_PCIE_RCRB_CFG_DECCTL_URRXURAVDM                 BIT13
#define B_PCIE_RCRB_CFG_DECCTL_URRXULTVDM                 BIT12
#define B_PCIE_RCRB_CFG_DECCTL_URRXURTRCVDM               BIT11
#define B_PCIE_RCRB_CFG_DECCTL_URRXUVDMUVID               BIT10
#define B_PCIE_RCRB_CFG_DECCTL_URRXUVDMINTELVID           BIT9
#define B_PCIE_RCRB_CFG_DECCTL_URVDME16DW                 BIT8
#define B_PCIE_RCRB_CFG_DECCTL_VDMATAC                    BIT7
#define B_PCIE_RCRB_CFG_DECCTL_LCRXINT                    BIT6
#define B_PCIE_RCRB_CFG_DECCTL_DROPCPLATNZCE              BIT4
#define B_PCIE_RCRB_CFG_DECCTL_OUTBEXPCPLCHKEN            BIT3
#define B_PCIE_RCRB_CFG_DECCTL_MTRXLTC                    BIT2
#define B_PCIE_RCRB_CFG_DECCTL_BUSNUMZCHK                 BIT1
#define B_PCIE_RCRB_CFG_DECCTL_ATSCE                      BIT0

#define R_PCIE_RCRB_CFG_PIDECCTL                          0x190C
#define B_PCIE_RCRB_CFG_PIDECCTL_CPLBNCHK                 BIT0

#define R_PCIE_RCRB_CFG_PTMPSDC1                          0x1920 ///< SIP17 PTM Pipe Stage Delay Configuration 1
#define R_PCIE_RCRB_CFG_PTMPSDC2                          0x1924 ///< SIP17 PTM Pipe Stage Delay Configuration 2
#define R_PCIE_RCRB_CFG_PTMPSDC3                          0x1928 ///< SIP17 PTM Pipe Stage Delay Configuration 3
#define R_PCIE_RCRB_CFG_PTMPSDC4                          0x192C ///< SIP17 PTM Pipe Stage Delay Configuration 4
#define R_PCIE_RCRB_CFG_PTMPSDC5                          0x1930 ///< SIP17 PTM Pipe Stage Delay Configuration 5
#define R_PCIE_RCRB_CFG_PTMPSDC6                          0x1934 ///< SIP17 PTM Pipe Stage Delay Configuration 6
#define R_PCIE_RCRB_CFG_PTMPSDC7                          0x1938 ///< SIP17 PTM Pipe Stage Delay Configuration 7
#define R_PCIE_RCRB_CFG_PTMPSDC8                          0x193C ///< SIP17 PTM Pipe Stage Delay Configuration 8
#define R_PCIE_RCRB_CFG_PTMPSDC9                          0x1940 ///< SIP17 PTM Pipe Stage Delay Configuration 9
#define R_PCIE_RCRB_CFG_PTMPSDC10                         0x1944 ///< SIP17 PTM Pipe Stage Delay Configuration 10
#define R_PCIE_RCRB_CFG_PTMPSDC11                         0x1948 ///< SIP17 PTM Pipe Stage Delay Configuration 11
#define R_PCIE_RCRB_CFG_PTMPSDC12                         0x194C ///< SIP17 PTM Pipe Stage Delay Configuration 12
#define R_PCIE_RCRB_CFG_PTMPSDC13                         0x1950 ///< SIP17 PTM Pipe Stage Delay Configuration 13
#define R_PCIE_RCRB_CFG_PTMPSDC14                         0x1954 ///< SIP17 PTM Pipe Stage Delay Configuration 14

#define R_PCIE_RCRB_CFG_G5L0SCTL                          0x1E00
#define B_PCIE_RCRB_CFG_G5L0SCTL_G5UCNFTS_MASK            0xFF00
#define B_PCIE_RCRB_CFG_G5L0SCTL_G5UCNFTS_OFFSET          8
#define V_PCIE_RCRB_CFG_G5L0SCTL_G5UCNFTS                 0xFF
#define B_PCIE_RCRB_CFG_G5L0SCTL_G5CCNFTS_MASK            0xFF
#define B_PCIE_RCRB_CFG_G5L0SCTL_G5CCNFTS_OFFSET          0
#define V_PCIE_RCRB_CFG_G5L0SCTL_G5CCNFTS                 0xE6

//
// PCIe Gen5 Equalization, accessible via RCRB
//
#define R_PCIE_RCRB_CFG_PX32EQCFG1                        0x1E04
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GTSWLPCE           BIT27
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GTSWLPCE_OFFSET    27
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GTSWLPCE_MASK      (BIT29 | BIT28 | BIT27)
#define V_PCIE_RCRB_CFG_PX32EQCFG1_PX32GTSWLPCE_5US       4
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GREIC              BIT20
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GLEP23B            BIT18
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GLEP3B             BIT17
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GLEPCEB            BIT16
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRTPCOE            BIT15
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE           BIT8
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE_OFFSET    8
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE_MASK      0x00000F00
#define V_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE_1US       1
#define V_PCIE_RCRB_CFG_PX32EQCFG1_PX32GRWTNEVE_3US       3
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAED              BIT12
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GEQTS2IRRC         BIT7
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAPCCPI           BIT3
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAPCCPI_OFFSET    3
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAPCCPI_MASK      (BIT6 | BIT5 | BIT4 | BIT3)
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GHAPCCPIE          BIT2
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GMEQSMMFLNTL       BIT1
#define B_PCIE_RCRB_CFG_PX32EQCFG1_PX32GMFLNTL            BIT0

#define R_PCIE_RCRB_CFG_PX32GRTPCL1                       0x1E08
#define B_PCIE_RCRB_CFG_PX32GRTPCL1_PCM                   BIT31
#define B_PCIE_RCRB_CFG_PX32GRTPCL1_PCM_OFFSET            31
#define B_PCIE_RCRB_CFG_PX32GRTPCL1_RTPRECL2PL4           0x3F000000
#define B_PCIE_RCRB_CFG_PX32GRTPCL1_RTPOSTCL1PL3          0xFC0000
#define B_PCIE_RCRB_CFG_PX32GRTPCL1_RTPRECL1PL2           0x3F000
#define B_PCIE_RCRB_CFG_PX32GRTPCL1_RTPOSTCL0PL1          0xFC0
#define B_PCIE_RCRB_CFG_PX32GRTPCL1_RTPRECL0PL0           0x3F

#define R_PCIE_RCRB_CFG_PX32EQCFG2                        0x1E24
#define B_PCIE_RCRB_CFG_PX32EQCFG2_NTIC                   0xFF000000
#define B_PCIE_RCRB_CFG_PX32EQCFG2_EMD                    BIT23
#define B_PCIE_RCRB_CFG_PX32EQCFG2_NTSS_MASK              (BIT22 | BIT21 | BIT20)
#define B_PCIE_RCRB_CFG_PX32EQCFG2_NTSS_OFFSET            20
#define B_PCIE_RCRB_CFG_PX32EQCFG2_PCET_MASK              (BIT19 | BIT18 | BIT17 | BIT16)
#define B_PCIE_RCRB_CFG_PX32EQCFG2_PCET_OFFSET            16
#define B_PCIE_RCRB_CFG_PX32EQCFG2_HAPCSB_MASK            (BIT15 | BIT14 | BIT13 | BIT12)
#define B_PCIE_RCRB_CFG_PX32EQCFG2_HAPCSB_OFFSET          12
#define B_PCIE_RCRB_CFG_PX32EQCFG2_NTEME                  BIT11
#define B_PCIE_RCRB_CFG_PX32EQCFG2_MPEME                  BIT10
#define B_PCIE_RCRB_CFG_PX32EQCFG2_REWMETM                (BIT9 | BIT8)
#define B_PCIE_RCRB_CFG_PX32EQCFG2_REWMETM_OFFSET         8
#define B_PCIE_RCRB_CFG_PX32EQCFG2_REWMET                 0xFF

#define R_PCIE_RCRB_CFG_PX32GP0P1PCM                      0x1E28
#define R_PCIE_RCRB_CFG_PX32GP1P2P3PCM                    0x1E2C
#define R_PCIE_RCRB_CFG_PX32GP3P4PCM                      0x1E30
#define R_PCIE_RCRB_CFG_PX32GP5P6PCM                      0x1E34
#define R_PCIE_RCRB_CFG_PX32GP6P7P8PCM                    0x1E38
#define R_PCIE_RCRB_CFG_PX32GP8P9PCM                      0x1E3C
#define R_PCIE_RCRB_CFG_PX32GP10PCM                       0x1E40
#define R_PCIE_RCRB_CFG_PX32GLFFS                         0x1E44

#define R_PCIE_RCRB_CFG_PX32GLTCO1                        0x1E48
#define B_PCIE_RCRB_CFG_PX32GLTCO1_L1TCOE                 BIT25
#define B_PCIE_RCRB_CFG_PX32GLTCO1_L0TCOE                 BIT24
#define B_PCIE_RCRB_CFG_PX32GLTCO1_L1TPOSTCO              0xFC0000
#define N_PCIE_RCRB_CFG_PX32GLTCO1_L1TPOSTCO              18
#define B_PCIE_RCRB_CFG_PX32GLTCO1_L1TPRECO               0x3F000
#define N_PCIE_RCRB_CFG_PX32GLTCO1_L1TPRECO               12
#define B_PCIE_RCRB_CFG_PX32GLTCO1_L0TPOSTCO              0xFC0
#define N_PCIE_RCRB_CFG_PX32GLTCO1_L0TPOSTCO              6
#define B_PCIE_RCRB_CFG_PX32GLTCO1_L0TPRECO               0x3F
#define N_PCIE_RCRB_CFG_PX32GLTCO1_L0TPRECO               0

#define R_PCIE_CFG_RCRB_CFG_PHYPG                         0x1590
#define B_PCIE_CFG_RCRB_CFG_PHYPG_RSVD_MASK               0xFFFFFFC0
#define B_PCIE_CFG_RCRB_CFG_PHYPG_RSVD_OFFSET             6
#define B_PCIE_CFG_RCRB_CFG_PHYPG_ULPPGP                  BIT5
#define B_PCIE_CFG_RCRB_CFG_PHYPG_DLPPGP                  BIT4
#define V_PCIE_CFG_RCRB_CFG_PHYPG_DUCFGPHYPGE             0x1
#define B_PCIE_CFG_RCRB_CFG_PHYPG_DUCFGPHYPGE             BIT3
#define V_PCIE_CFG_RCRB_CFG_PHYPG_L23PHYPGE               0x1
#define B_PCIE_CFG_RCRB_CFG_PHYPG_L23PHYPGE               BIT2
#define V_PCIE_CFG_RCRB_CFG_PHYPG_DISPHYPGE               0x1
#define B_PCIE_CFG_RCRB_CFG_PHYPG_DISPHYPGE               BIT1
#define V_PCIE_CFG_RCRB_CFG_PHYPG_DETPHYPGE               0x1
#define B_PCIE_CFG_RCRB_CFG_PHYPG_DETPHYPGE               BIT0

#define R_PCIE_CFG_SRL                                    0x3E24
#define B_PCIE_CFG_SRL_SRL                                BIT0

//
// PCIE PCRs (PID:SPA SPB SPC SPD SPE SPF)
//
#define R_PCIE_PCR_PCD                                0                       ///< Port configuration and disable
#define B_PCIE_PCR_PCD_RP1FN                          (BIT2 | BIT1 | BIT0)    ///< Port 1 Function Number
#define B_PCIE_PCR_PCD_RP1CH                          BIT3                    ///< Port 1 config hide
#define B_PCIE_PCR_PCD_RP2FN                          (BIT6 | BIT5 | BIT4)    ///< Port 2 Function Number
#define B_PCIE_PCR_PCD_RP2CH                          BIT7                    ///< Port 2 config hide
#define B_PCIE_PCR_PCD_RP3FN                          (BIT10 | BIT9 | BIT8)   ///< Port 3 Function Number
#define B_PCIE_PCR_PCD_RP3CH                          BIT11                   ///< Port 3 config hide
#define B_PCIE_PCR_PCD_RP4FN                          (BIT14 | BIT13 | BIT12) ///< Port 4 Function Number
#define B_PCIE_PCR_PCD_RP4CH                          BIT15                   ///< Port 4 config hide
#define S_PCIE_PCR_PCD_RP_FIELD                       4                       ///< 4 bits for each RP FN
#define B_PCIE_PCR_PCD_P1D                            BIT16                   ///< Port 1 disable
#define B_PCIE_PCR_PCD_P2D                            BIT17                   ///< Port 2 disable
#define B_PCIE_PCR_PCD_P3D                            BIT18                   ///< Port 3 disable
#define B_PCIE_PCR_PCD_P4D                            BIT19                   ///< Port 4 disable
#define B_PCIE_PCR_PCD_SRL                            BIT31                   ///< Secured Register Lock

#define R_PCIE_PCR_PCIEHBP                            0x0004                  ///< PCI Express high-speed bypass
#define B_PCIE_PCR_PCIEHBP_PCIEHBPME                  BIT0                    ///< PCIe HBP mode enable
#define B_PCIE_PCR_PCIEHBP_PCIEGMO                    (BIT2 | BIT1)           ///< PCIe gen mode override
#define B_PCIE_PCR_PCIEHBP_PCIETIL0O                  BIT3                    ///< PCIe transmitter-in-L0 override
#define B_PCIE_PCR_PCIEHBP_PCIERIL0O                  BIT4                    ///< PCIe receiver-in-L0 override
#define B_PCIE_PCR_PCIEHBP_PCIELRO                    BIT5                    ///< PCIe link recovery override
#define B_PCIE_PCR_PCIEHBP_PCIELDO                    BIT6                    ///< PCIe link down override
#define B_PCIE_PCR_PCIEHBP_PCIESSM                    BIT7                    ///< PCIe SKP suppression mode
#define B_PCIE_PCR_PCIEHBP_PCIESST                    BIT8                    ///< PCIe suppress SKP transmission
#define B_PCIE_PCR_PCIEHBP_PCIEHBPPS                  (BIT13 | BIT12)         ///< PCIe HBP port select
#define B_PCIE_PCR_PCIEHBP_CRCSEL                     (BIT15 | BIT14)         ///< CRC select
#define B_PCIE_PCR_PCIEHBP_PCIEHBPCRC                 0xFFFF0000              ///< PCIe HBP CRC

#define R_PCIE_PCR_IMRAMBL                            0x10                    ///< IMR access memory base and limit
#define B_PCIE_PCR_IMRAMBL_RS3BN                      0x000000FF              ///< Bus Number for RS3
#define N_PCIE_PCR_IMRAMBL_RS3BN                      0
#define B_PCIE_PCR_IMRAMBL_IAMB                       0x000FFF00              ///< IMR access memory base, lower bits
#define N_PCIE_PCR_IMRAMBL_IAMB                       8
#define B_PCIE_PCR_IMRAMBL_IAML                       0xFFF00000              ///< IMR access memory limit, lower bits
#define N_PCIE_PCR_IMRAMBL_IAML                       20

#define R_PCIE_PCR_IMRAMBU32                          0x14                    ///< IMR access memory base, upper bits

#define R_PCIE_PCR_IMRAMLU32                          0x18                    ///< IMR access memory limit, upper bits

#define R_PCIE_PCR_IMRAMLE                            0x1C                    ///< IMR access memory lock & enable
#define B_PCIE_PCR_IMRAMLE_IAE1                       BIT0                    ///< IMR access enable for port 1 of given controller
#define B_PCIE_PCR_IMRAMLE_IAE2                       BIT1                    ///< IMR access enable for port 1 of given controller
#define B_PCIE_PCR_IMRAMLE_IAE3                       BIT2                    ///< IMR access enable for port 1 of given controller
#define B_PCIE_PCR_IMRAMLE_IAE4                       BIT3                    ///< IMR access enable for port 1 of given controller
#define B_PCIE_PCR_IMRAMLE_SRL                        BIT31                   ///< IMR register lock

#define R_PCIE_SIP16_PCR_PCD                          0x3E00                  ///< Port configuration and disable

#endif
