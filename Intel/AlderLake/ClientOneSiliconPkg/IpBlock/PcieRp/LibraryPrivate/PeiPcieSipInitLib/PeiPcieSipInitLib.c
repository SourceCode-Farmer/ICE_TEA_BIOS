/** @file
  This file contains common initialization code for PCIe SIP controller

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/PeiPcieSipInitLib.h>
#include <Library/DebugLib.h>
#include <Library/TimerLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PciExpressHelpersLib.h>
#include <Library/PchPolicyLib.h> // To import FORCE_ENABLE/DISABLE

#include <Register/PcieSipRegs.h>
#include <Register/PchRegs.h>
#include <PcieRegs.h>

#define LINK_RETRAIN_WAIT_TIME 1000 // microseconds
#define LINK_ACTIVE_POLL_INTERVAL 100     // in microseconds
#define LINK_ACTIVE_POLL_TIMEOUT  1000000 // in microseconds

#define DISABLE_TIMEOUT      5000

/**
  Configure single LTR subtraction register based on the
  configuration.

  @param[in] RpDev                 Pointer to the root port device
  @param[in] LtrSubRegisterOffset  Offset of the particular LTR subtraction register
  @param[in] LtrSubConfig          Pointer to LTR subtraction configuration
**/
VOID
PcieSipConfigureLtrSubtractionSingle (
  IN PCIE_ROOT_PORT_DEV      *RpDev,
  IN UINT32                  LtrSubRegisterOffset,
  IN LTR_SUBTRACTION_CONFIG  *LtrSubConfig
  )
{
  UINT32  Data32And;
  UINT32  Data32Or;

  if (LtrSubConfig->NoSnoopEnable) {
    Data32And = ~(B_PCIE_CFG_LTRSUB_LTRNSLSUBEN |
                  B_PCIE_CFG_LTRSUB_LTRNSLSSUBV_MASK |
                  B_PCIE_CFG_LTRSUB_LTRNSLSUBV_MASK);

    Data32Or =  (B_PCIE_CFG_LTRSUB_LTRNSLSUBEN |
                 (LtrSubConfig->NoSnoopScale << N_PCIE_CFG_LTRSUB_LTRNSLSSUBV) |
                 (LtrSubConfig->NoSnoopValue << N_PCIE_CFG_LTRSUB_LTRNSLSUBV));

    RpDev->PciCfgAccess->AndThenOr32 (
      RpDev->PciCfgAccess,
      LtrSubRegisterOffset,
      Data32And,
      Data32Or
      );
  }

  if (LtrSubConfig->SnoopEnable) {
    Data32And = (UINT32) ~(B_PCIE_CFG_LTRSUB_LTRSLSUBEN |
                           B_PCIE_CFG_LTRSUB_LTRSLSSUBV_MASK |
                           B_PCIE_CFG_LTRSUB_LTRSLSUBV_MASK);

    Data32Or = (B_PCIE_CFG_LTRSUB_LTRSLSUBEN |
                (LtrSubConfig->SnoopScale << N_PCIE_CFG_LTRSUB_LTRSLSSUBV) |
                (LtrSubConfig->SnoopValue << N_PCIE_CFG_LTRSUB_LTRSLSUBV));

    RpDev->PciCfgAccess->AndThenOr32 (
      RpDev->PciCfgAccess,
      LtrSubRegisterOffset,
      Data32And,
      Data32Or
      );
  }
}

/**
  Configure all LTR subtraction registers according to configuration

  @param[in] RpDev  Pointer to the root port device
  @param[in] L1StandardConfig  Pointer to L1Standard LTR subtraction config
  @param[in] L1p1Config        Pointer to L1.1 LTR subtraction config
  @param[in] L1p2Config        Pointer to L1.2 LTR subtraction config
  @param[in] LtrSubL11Npg      Pointer to L1.1 No Power Gate LTR subtraction config
**/
VOID
PcieSipConfigureLtrSubstraction (
  IN PCIE_ROOT_PORT_DEV      *RpDev,
  IN LTR_SUBTRACTION_CONFIG  *L1StandardConfig,
  IN LTR_SUBTRACTION_CONFIG  *L1p1Config,
  IN LTR_SUBTRACTION_CONFIG  *L1p2Config,
  IN LTR_SUBTRACTION_CONFIG  *LtrSubL11Npg
  )
{
  if (RpDev->SipVersion < PcieSip16) {
    DEBUG ((DEBUG_WARN, "PCIe SIP < %d does not support LTR subtraction\n", PcieSip16));
    return;
  }

  PcieSipConfigureLtrSubtractionSingle (RpDev, R_PCIE_CFG_LTRSUBL1STD, L1StandardConfig);
  PcieSipConfigureLtrSubtractionSingle (RpDev, R_PCIE_CFG_LTRSUBL11, L1p1Config);
  PcieSipConfigureLtrSubtractionSingle (RpDev, R_PCIE_CFG_LTRSUBL12, L1p2Config);
  PcieSipConfigureLtrSubtractionSingle (RpDev, R_PCIE_CFG_LTRSUBL11NPG, LtrSubL11Npg);
}

/**
  Configures PTM settings on the PCIe controller.

  @param[in] RpDev      Pointer to the root port device
  @param[in] PtmConfig  Pointer to the PTM configuration structure
**/
VOID
PcieSipConfigurePtm (
  IN PCIE_ROOT_PORT_DEV  *RpDev,
  IN PTM_CONFIGURATION   *PtmConfig
  )
{
  UINT32  PtmRegisterOffset;
  UINT32  PsdRegIndex;
  UINT32  MaxPsdReg;

  if (RpDev == NULL || PtmConfig == NULL) {
    DEBUG ((DEBUG_ERROR, "RpDev or PtmConfig can't be NULL\n"));
    return;
  }

  if (RpDev->SipVersion < PcieSip14) {
    DEBUG ((DEBUG_WARN, "PCIe SIP < %d does not support PTM configuration\n", PcieSip14));
    return;
  }

  //
  // This function can be called when root port 0 has already been hidden. Because of that all accesses done by this function
  // have to be done through sideband access.
  //
  if (RpDev->SipVersion >= PcieSip17) {
    //
    // SIP 17 has moved all PTM registers into RCRB.
    //
    for (PsdRegIndex = 0; PsdRegIndex < MAX_PTM_STAGE_DELAY_CONFIG_REGS_SIP17; PsdRegIndex++) {
      RpDev->PciSbiMsgMemAccess->Write32 (
        RpDev->PciSbiMsgMemAccess,
        R_PCIE_RCRB_CFG_PTMPSDC1 + (4 * PsdRegIndex),
        PtmConfig->PtmPipeStageDelay[PsdRegIndex]
        );
    }
  } else {
    if (RpDev->SipVersion == PcieSip16) {
      MaxPsdReg = MAX_PTM_STAGE_DELAY_CONFIG_REGS_SIP16;
    } else {
      MaxPsdReg = MAX_PTM_STAGE_DELAY_CONFIG_REGS_SIP14;
    }

    for (PsdRegIndex = 0; PsdRegIndex < MaxPsdReg; PsdRegIndex++) {
      if (PsdRegIndex < 5) {
        PtmRegisterOffset = R_PCIE_CFG_PTMPSDC1 + (4 * PsdRegIndex);
      } else if (PsdRegIndex < 8) {
        PtmRegisterOffset = R_PCIE_CFG_PTMPSDC6 + ((PsdRegIndex - 6) * PsdRegIndex);
      } else {
        PtmRegisterOffset = R_PCIE_CFG_PTMPSDC9 + ((PsdRegIndex - 9) * PsdRegIndex);
      }

      RpDev->PciSbiMsgCfgAccess->Write32 (
        RpDev->PciSbiMsgCfgAccess,
        PtmRegisterOffset,
        PtmConfig->PtmPipeStageDelay[PsdRegIndex]
        );
    }
  }

  RpDev->PciSbiMsgCfgAccess->Write32 (
    RpDev->PciSbiMsgCfgAccess,
    R_PCIE_CFG_PTMECFG,
    PtmConfig->PtmConfig
    );
}

/**
  Enables retimer presence and two retimers presence detect on supported
  SIP versions.

  @param[in] RpDev  Pointer to the root port device
**/
VOID
PcieSipConfigureRetimerSupport (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  if (RpDev->SipVersion < PcieSip16) {
    DEBUG ((DEBUG_WARN, "PCIe SIP < %d does not support retimer presence detect\n", PcieSip16));
    return;
  }

  if ((RpDev->Sbdf.Device == 1)  && (RpDev->SipVersion == PcieSip17)) {
    RpDev->PciCfgAccess->And32 (
      RpDev->PciCfgAccess,
      R_PCIE_CFG_LCAP2,
      (UINT32) ~((B_PCIE_LCAP2_TRPDS | B_PCIE_LCAP2_RPDS))
    );
  } else {
    RpDev->PciCfgAccess->Or32 (
      RpDev->PciCfgAccess,
      R_PCIE_CFG_LCAP2,
      (UINT32) (B_PCIE_LCAP2_TRPDS | B_PCIE_LCAP2_RPDS)
    );
  }
}

/**
  Configure 10-Bit Tag

  @param[in] RpDev               Pointer to the root port device
  @param[in] CompleterSupported  If TRUE it indicates 10-Bit Tag Completer is supported.
  @param[in] RequesterSupported  If TRUE it indicates 10-Bit Tag Requester is supported.

**/
VOID
PcieSipConfigure10BitTag (
  IN PCIE_ROOT_PORT_DEV      *RpDev,
  IN BOOLEAN                 CompleterSupported,
  IN BOOLEAN                 RequesterSupported
  )
{
  if (RpDev->SipVersion < PcieSip16) {
    DEBUG ((DEBUG_WARN, "PCIe SIP < %d does not support 10-bit tag\n", PcieSip16));
    return;
  }

  if (CompleterSupported == FALSE) {
    //
    // Program 10-Bit Tag Completer Supported PX10BTCS = 0x0
    //
    RpDev->PciCfgAccess->And32 (RpDev->PciCfgAccess, R_PCIE_CFG_DCAP2, (UINT32)~B_PCIE_DCAP2_PX10BTCS);
  } else {
    //
    // Program 10-Bit Tag Completer Supported PX10BTCS = 0x1
    //
    RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_DCAP2, B_PCIE_DCAP2_PX10BTCS);
  }

  if (RequesterSupported == FALSE) {
    //
    // Program 10-Bit Tag Requester Enable PX10BTRE = 0x0
    // Program 10-Bit Tag Requester Supported PX10BTRS = 0x0
    //
    RpDev->PciCfgAccess->And32 (RpDev->PciCfgAccess, R_PCIE_CFG_DCTL2, (UINT32)~B_PCIE_DCTL2_PX10BTRE);
    RpDev->PciCfgAccess->And32 (RpDev->PciCfgAccess, R_PCIE_CFG_DCAP2, (UINT32)~B_PCIE_DCAP2_PX10BTRS);
  } else {
    //
    // Program 10-Bit Tag Requester Enable PX10BTRE = 0x1
    // Program 10-Bit Tag Requester Supported PX10BTRS = 0x1
    //
    RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_DCTL2, B_PCIE_DCTL2_PX10BTRE);
    RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_DCAP2, B_PCIE_DCAP2_PX10BTRS);
  }

  //
  // Program Fabric 10-bit Tag Support Enable F10BTSE = 0x0
  //
  RpDev->PciCfgAccess->And32 (RpDev->PciCfgAccess, R_PCIE_ADVMCTRL, (UINT32)~B_PCIE_ADVMCTRL_F10BTSE);
}

/**
  Configure Peer Disable

  @param[in] RpDev               Pointer to the root port device
  @param[in] CfgReads            TRUE/FALSE, enable/disable forwarding of Upstream Posted Memory Reads
  @param[in] CfgWrites           TRUE/FALSE, enable/disable forwarding of Upstream Posted Memory Writes
**/
VOID
PcieSipConfigurePeerDisable (
  IN PCIE_ROOT_PORT_DEV      *RpDev,
  IN BOOLEAN                 CfgReads,
  IN BOOLEAN                 CfgWrites
  )
{
  UINT32  Data32And;
  UINT32  Data32Or;

  Data32Or = 0;

  if (RpDev->SipVersion < PcieSip16) {
    DEBUG ((DEBUG_WARN, "PCIe SIP < %d does not support Peer Disable\n", PcieSip16));
    return;
  }

  Data32And = (UINT32) ~((B_PCIE_CFG_CCFG_UPMWPD) | (B_PCIE_CFG_CCFG_UMRPD));
  if (CfgReads) {
    Data32Or |=(UINT32) (B_PCIE_CFG_CCFG_UMRPD);
  }
  if (CfgWrites) {
    Data32Or |= (UINT32) (B_PCIE_CFG_CCFG_UPMWPD);
  }

  RpDev->PciCfgAccess->AndThenOr32 (
    RpDev->PciCfgAccess,
    R_PCIE_CFG_CCFG,
    Data32And,
    Data32Or
    );
}

/**
  Configure PCIe Completion Coalescing

  @param[in] RpDev               Pointer to the root port device
**/
VOID
PcieConfigureCoalescing (
  IN  PCIE_ROOT_PORT_DEV    *RpDev
  )
{
  UINT32    Data32And;
  UINT32    Data32Or;

  Data32Or = 0x0;

  if (RpDev->SipVersion < PcieSip16) {
    DEBUG ((DEBUG_WARN, "PCIe SIP < %d does not support Completion Coalescing\n", PcieSip16));
    return;
  }

  Data32And = (RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_COCTL) &
              ~(UINT32)(B_PCIE_CFG_COCTL_PWCE | B_PCIE_CFG_COCTL_DDCE |
                        B_PCIE_CFG_COCTL_CT_MASK | B_PCIE_CFG_COCTL_CTE |
                        B_PCIE_CFG_COCTL_ROAOP | B_PCIE_CFG_COCTL_PCLM_MASK |
                        B_PCIE_CFG_COCTL_NPCLM_MASK));
  ///
  /// Program COCTL.PCLM 594h [14:13] = 2b
  /// Program COCTL.NPCLM 594h [16:15] = 2b
  ///
  if (RpDev->SipVersion > PcieSip17) {
    Data32Or  = ((0x2 << N_PCIE_CFG_COCTL_PCLM_OFFSET) | (0x2 << N_PCIE_CFG_COCTL_NPCLM_OFFSET));
  }

  RpDev->PciCfgAccess->AndThenOr32(RpDev->PciCfgAccess, R_PCIE_CFG_COCTL, Data32And, Data32Or);
  DEBUG((DEBUG_INFO, "COCTL = %x\n", RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_COCTL)));
  return;
}

/**
  Configure Pcie Squelch Power Management.

  @param[in] RpDev  Pointer to the root port device.

**/
VOID
ConfigurePcieSquelchPowerManagement (
  IN PCIE_ROOT_PORT_DEV      *RpDev
  )
{
  UINT32  Data32And;
  UINT32  Data32Or;

  //
  // Enable Squelch propagation control
  // Program Link Clock Domain Squelch Exit Debounce Timers to 0ns
  //
  Data32And = (UINT32)~(B_PCIE_CFG_PCIEDBG_SPCE | B_PCIE_CFG_PCIEDBG_LGCLKSQEXITDBTIMERS);
  Data32Or = (V_PCIE_CFG_PCIEDBG_LGCLKSQEXITDBTIMERS_0NS << N_PCIE_CFG_PCIEDBG_LGCLKSQEXITDBTIMERS_OFFSET);
  if (RpDev->SipVersion < PcieSip17) {
    Data32Or |= B_PCIE_CFG_PCIEDBG_SPCE;
  }
  RpDev->PciCfgAccess->AndThenOr32 (
                          RpDev->PciCfgAccess,
                          R_PCIE_CFG_PCIEDBG,
                          Data32And,
                          Data32Or
                          );

  //
  // Disable RxElecIdle Sampling Policy (RSP) and Root Port Detect Squelch Polling
  // and program the Root Port L1 Squelch Polling.
  //
  Data32And = (UINT32)~(B_PCIE_CFG_PWRCTL_RSP | B_PCIE_CFG_PWRCTL_RPL1SQPOL | B_PCIE_CFG_PWRCTL_RPDTSQPOL);
  Data32Or  = (UINT32)B_PCIE_CFG_PWRCTL_DLP;
  if (RpDev->SipVersion < PcieSip16) {
    Data32Or |= B_PCIE_CFG_PWRCTL_RPL1SQPOL;
  }
  RpDev->PciCfgAccess->AndThenOr32 (
                          RpDev->PciCfgAccess,
                          R_PCIE_CFG_PWRCTL,
                          Data32And,
                          Data32Or
                          );

  //
  // Configure Squelch Direction settings
  //
  RpDev->PciCfgAccess->And8 (
                          RpDev->PciCfgAccess,
                          R_PCIE_CFG_PHYCTL3,
                          (UINT8)~(B_PCIE_CFG_PHYCTL3_SQDIROVREN | B_PCIE_CFG_PHYCTL3_SQDIRCTRL)
                          );

  //
  // Disable Squelch Off in L0
  //
  RpDev->PciCfgAccess->And32 (
                          RpDev->PciCfgAccess,
                          R_PCIE_CFG_PCIEDBG,
                          (UINT32)~B_PCIE_CFG_PCIEDBG_SQOL0
                          );

  //
  // Enable Low Bandwidth Squelch Settling Timer
  // Set this before enabling any of the squelch power management
  //
  if (RpDev->SipVersion < PcieSip17) {
    Data32Or = B_PCIE_CFG_PCIECFG2_LBWSSTE;
  } else {
    Data32Or = B_PCIE_CFG_PCIECFG2_CRSREN | B_PCIE_CFG_PCIECFG2_CROAOV | B_PCIE_CFG_PCIECFG2_CROAOE;
  }
  RpDev->PciCfgAccess->Or32 (
                          RpDev->PciCfgAccess,
                          R_PCIE_CFG_PCIECFG2,
                          Data32Or
                          );

  //
  // Program Un-Squelch Sampling Period
  //
  Data32And = (UINT32)~B_PCIE_CFG_PCIEDBG_USSP;
  if (RpDev->SipVersion < PcieSip17) {
    Data32Or = (V_PCIE_CFG_PCIEDBG_USSP_32NS << N_PCIE_CFG_PCIEDBG_USSP_OFFSET);
  } else {
    Data32Or = (V_PCIE_CFG_PCIEDBG_USSP_16NS << N_PCIE_CFG_PCIEDBG_USSP_OFFSET);
  }
  RpDev->PciCfgAccess->AndThenOr32 (
                          RpDev->PciCfgAccess,
                          R_PCIE_CFG_PCIEDBG,
                          Data32And,
                          Data32Or
                          );
}

/**
  Configures the dynamic clock gating

  @param[in] RpDev               Pointer to the root port device
**/
VOID
ConfigureDynamicClockGating (
  IN  PCIE_ROOT_PORT_DEV    *RpDev
)
{
  UINT32  DataOr32;

  if (RpDev->SipVersion < PcieSip17) {
    DEBUG ((DEBUG_WARN, "PCIe SIP < %d does not support Dynamic clock gating\n", PcieSip16));
    return;
  }

  //
  // BWG recommended values for  SIP17 are programmed here
  // Section 4.4.3 Clock Gating
  // To enable the Unit Level Dynamic Clock Gating, BIOS is required to set the enable bits in DCGM1, 2, 3, 4 before setting any bits in DCGEN1, 2, 3, 4
  //
  DataOr32 = (B_PCIE_RCRB_DCGM1_PXTTSULDCGM | B_PCIE_RCRB_DCGM1_PXKGULDCGM | B_PCIE_RCRB_DCGM1_PXTRSULDCGM | \
              B_PCIE_RCRB_DCGM1_PXTRULDCGM | B_PCIE_RCRB_DCGM1_PXLSULDCGM | B_PCIE_RCRB_DCGM1_PXLIULDCGM | \
              B_PCIE_RCRB_DCGM1_PXLTULDCGM | B_PCIE_RCRB_DCGM1_PXLRULDCGM | B_PCIE_RCRB_DCGM1_PXCULDCGM);
  if (RpDev->SipVersion > PcieSip17) {
    DataOr32 |= B_PCIE_RCRB_DCGM1_PXTTULDCGM;
  }
  RpDev->PciSbiMsgMemAccess->Or32 (
          RpDev->PciSbiMsgMemAccess,
          R_PCIE_RCRB_DCGM1,
          DataOr32
          );

  DataOr32 = (B_PCIE_RCRB_DCGM2_PXFRULDCGM | B_PCIE_RCRB_DCGM2_PXFTULDCGM | B_PCIE_RCRB_DCGM2_PXPBULDCGM | \
              B_PCIE_RCRB_DCGM2_PXPSULDCGM | B_PCIE_RCRB_DCGM2_PXPIULDCGM);
  RpDev->PciSbiMsgMemAccess->Or32 (
          RpDev->PciSbiMsgMemAccess,
          R_PCIE_RCRB_DCGM2,
          DataOr32
          );

  DataOr32 = (B_PCIE_RCRB_DCGM3_PXTTSUPDCGM | B_PCIE_RCRB_DCGM3_PXTTUPDCGM | B_PCIE_RCRB_DCGM3_PXTOUPDCGM | \
              B_PCIE_RCRB_DCGM3_PXTRSUPDCGM | B_PCIE_RCRB_DCGM3_PXTRUPDCGM | B_PCIE_RCRB_DCGM3_PXLIUPDCGM | \
              B_PCIE_RCRB_DCGM3_PXLTUPDCGM | B_PCIE_RCRB_DCGM3_PXLRUPDCGM | B_PCIE_RCRB_DCGM3_PXSRUSSNRDCGM | \
              B_PCIE_RCRB_DCGM3_PXCUPSNRDCGM | B_PCIE_RCRB_DCGM3_PXCUPSRCDCGM | B_PCIE_RCRB_DCGM3_PXBUPDCGM | \
              B_PCIE_RCRB_DCGM3_PXEUPDCGM);
  RpDev->PciSbiMsgMemAccess->Or32 (
          RpDev->PciSbiMsgMemAccess,
          R_PCIE_RCRB_DCGM3,
          DataOr32
          );

  DataOr32 = (B_PCIE_RCRB_DCGEN1_PXTTSULDCGEN | B_PCIE_RCRB_DCGEN1_PXKGULDCGEN |  B_PCIE_RCRB_DCGEN1_PXTRSULDCGEN | \
              B_PCIE_RCRB_DCGEN1_PXTRULDCGEN | B_PCIE_RCRB_DCGEN1_PXLSULDCGEN | B_PCIE_RCRB_DCGEN1_PXLIULDCGEN | \
              B_PCIE_RCRB_DCGEN1_PXLRULDCGEN | B_PCIE_RCRB_DCGEN1_PXCULDCGEN);
  if (RpDev->SipVersion > PcieSip17) {
    DataOr32 |= B_PCIE_RCRB_DCGEN1_PXTTULDCGEN;
  }
  RpDev->PciSbiMsgMemAccess->Or32 (
          RpDev->PciSbiMsgMemAccess,
          R_PCIE_RCRB_DCGEN1,
          DataOr32
          );

  DataOr32 = (B_PCIE_RCRB_DCGEN2_PXFRULDCGEN | B_PCIE_RCRB_DCGEN2_PXFTULDCGEN | B_PCIE_RCRB_DCGEN2_PXPBULDCGEN | \
              B_PCIE_RCRB_DCGEN2_PXPSULDCGEN | B_PCIE_RCRB_DCGEN2_PXPIULDCGEN );
  RpDev->PciSbiMsgMemAccess->Or32 (
          RpDev->PciSbiMsgMemAccess,
          R_PCIE_RCRB_DCGEN2,
          DataOr32
          );

  DataOr32 = (B_PCIE_RCRB_DCGEN3_PXTTSUPDCGEN | B_PCIE_RCRB_DCGEN3_PXTTUPDCGEN | B_PCIE_RCRB_DCGEN3_PXTOUPDCGEN | \
              B_PCIE_RCRB_DCGEN3_PXTRSUPDCGEN | B_PCIE_RCRB_DCGEN3_PXTRUPDCGEN | B_PCIE_RCRB_DCGEN3_PXLIUPDCGEN | \
              B_PCIE_RCRB_DCGEN3_PXLRUPDCGEN | B_PCIE_RCRB_DCGEN3_PXCUPSNRDCGEN | B_PCIE_RCRB_DCGEN3_PXSRUSSNRDCGEN | \
              B_PCIE_RCRB_DCGEN3_PXBUPDCGEN | B_PCIE_RCRB_DCGEN3_PXEUPDCGEN);
  RpDev->PciSbiMsgMemAccess->Or32 (
          RpDev->PciSbiMsgMemAccess,
          R_PCIE_RCRB_DCGEN3,
          DataOr32
          );

  DataOr32 = B_PCIE_RCRB_IPCLKCTR_PXDCGE;
  RpDev->PciSbiMsgMemAccess->Or32 (
          RpDev->PciSbiMsgMemAccess,
          R_PCIE_RCRB_IPCLKCTR,
          DataOr32
          );
}

/**
  Get max link width.

  @param[in] RpDev  Pointer to the root port device
  @retval           Max link width, 0 when failed
**/
UINT8
PcieSipGetMaxLinkWidth (
  IN PCIE_ROOT_PORT_DEV    *RpDev
  )
{
  UINT8  LinkWidth;

  LinkWidth = (UINT8) ((RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_LCAP) & B_PCIE_LCAP_MLW) >> N_PCIE_LCAP_MLW);
  if (LinkWidth != 0xFF) {
    return LinkWidth;
  } else {
    DEBUG ((DEBUG_ERROR, "Incorrect Link Width = %d\n", LinkWidth));
    return 0;
  }
}

/**
  Get PCIe port number for enabled port.
  @param[in] RpDev  Pointer to the root port device

  @retval Root Port number (1 based)
**/
UINT32
PcieSipGetPortNum (
  IN     PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  return RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_LCAP) >> N_PCIE_CFG_LCAP_PN;
}

/**
  Get the negotiated link width

  @param[in] RpDev  Pointer to the root port device

  @return negotiated link width
**/
UINT8
PcieSipGetNegotiatedLinkWidth (
  PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  return (UINT8) ((RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_LSTS) & B_PCIE_LSTS_NLW) >> N_PCIE_LSTS_NLW);
}

/**
  Configure Pcie Phy Configuration.
  This function has to be executed before
  start of equalization.

  @param[in] RpDev  Pointer to the root port device.
**/
VOID
PcieSipConfigurePhyInit (
  IN PCIE_ROOT_PORT_DEV      *RpDev
  )
{
  UINT32  Data32And;
  UINT32  Data32Or;

  if (RpDev->SipVersion < PcieSip17) {
    DEBUG ((DEBUG_WARN, "For PCIe SIP < %d Phy programming not applicable\n", PcieSip16));
    return;
  }

  Data32Or  = (UINT32) B_PCIE_CFG_CONTROL2_PMETOFD;
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_CONTROL2, Data32Or);

  Data32And = (UINT32) ~(B_PCIE_RCRB_LPHYCP1_RXADPSVH | B_PCIE_RCRB_LPHYCP1_RXADPRECE);
  Data32Or  = (UINT32) (B_PCIE_RCRB_LPHYCP1_PIPEMBIP | B_PCIE_RCRB_LPHYCP1_BPRXSTNDBYHSRECAL |
                        B_PCIE_RCRB_LPHYCP1_RXEQFNEVC | B_PCIE_RCRB_LPHYCP1_RXADPHM);
  RpDev->PciSbiMsgMemAccess->AndThenOr32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_LPHYCP1, Data32And, Data32Or);

  Data32Or  = (UINT32) (B_PCIE_RCRB_LPHYCP4_DPMDNDBFE | B_PCIE_RCRB_LPHYCP4_PLLBUSDRC);

  ///
  /// Program (R_PCIE_RCRB_LPHYCP4) 0x1434
  /// RxL0s Exit Gen 1/2 FTS Timeout Extension (RXL0SEG12FTSE) 0x1434 [25:24] 0x2
  /// Gen 1 Async RxElecidle Detector for RXL0s (G1AREDRL0S) 0x1434 [16] 0x1
  /// Gen 2 Async RxElecidle Detector for RXL0s (G2AREDRL0S) 0x1434 [17] 0x1
  /// Gen 4 Async RxElecidle Detector for RXL0s (G4AREDRL0S) 0x1434 [19] 0x1
  /// Gen 5 Async RxElecidle Detector for RXL0s (G5AREDRL0S) 0x1434 [20] 0x1
  ///
  if (RpDev->SipVersion > PcieSip17) {
    Data32Or  |= (UINT32) (((V_PCIE_RCRB_LPHYCP4_RXL0SEG12FTSE << B_PCIE_RCRB_LPHYCP4_RXL0SEG12FTSE_OFFSET) | B_PCIE_RCRB_LPHYCP4_G5AREDRL0S |
                            B_PCIE_RCRB_LPHYCP4_G4AREDRL0S | B_PCIE_RCRB_LPHYCP4_G2AREDRL0S | B_PCIE_RCRB_LPHYCP4_G1AREDRL0S));
  }
  RpDev->PciSbiMsgMemAccess->Or32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_LPHYCP4, Data32Or);
}

/**
  Finds the Offset to a given Capabilities ID
  Each capability has an ID and a pointer to next Capability, so they form a linked list.
  This function walks the list of Capabilities present in device's pci cfg. If requested capability
  can be found, its offset is returned.
  If the capability can't be found or if device doesn't exist, function returns 0
  CAPID list:
    0x01 = PCI Power Management Interface
    0x04 = Slot Identification
    0x05 = MSI Capability
    0x10 = PCI Express Capability

  @param[in] PciAccess  Pointer to the device PCI config access.
  @param[in] CapId      CAPID to search for

  @retval 0      CAPID not found (this includes situation where device doesn't exit)
  @retval Other  CAPID found, Offset of desired CAPID
**/
UINT8
PcieSipFindCapId (
  IN REGISTER_ACCESS  *PciAccess,
  IN UINT8            CapId
  )
{
  UINT8  CapHeaderOffset;
  UINT8  CapHeaderId;
  UINT16 Data16;
  //
  // We do not explicitly check if device exists to save time and avoid unnecessary PCI access
  // If the device doesn't exist, check for CapHeaderId != 0xFF will fail and function will return offset 0
  //
  if ((PciAccess->Read8 (PciAccess, PCI_PRIMARY_STATUS_OFFSET) & EFI_PCI_STATUS_CAPABILITY) == 0x00) {
    ///
    /// Function has no capability pointer
    ///
    return 0;
  } else {
    ///
    /// Check the header layout to determine the Offset of Capabilities Pointer Register
    ///
    if ((PciAccess->Read8 (PciAccess, PCI_HEADER_TYPE_OFFSET) & HEADER_LAYOUT_CODE) == (HEADER_TYPE_CARDBUS_BRIDGE)) {
      ///
      /// If CardBus bridge, start at Offset 0x14
      ///
      CapHeaderOffset = EFI_PCI_CARDBUS_BRIDGE_CAPABILITY_PTR;
    } else {
      ///
      /// Otherwise, start at Offset 0x34
      ///
      CapHeaderOffset = PCI_CAPBILITY_POINTER_OFFSET;
    }
    ///
    /// Get Capability Header, A pointer value of 00h is used to indicate the last capability in the list.
    ///
    CapHeaderId     = 0;
    CapHeaderOffset = PciAccess->Read8 (PciAccess, CapHeaderOffset) & ((UINT8) ~(BIT0 | BIT1));
    while (CapHeaderOffset != 0 && CapHeaderId != 0xFF) {
      Data16 = PciAccess->Read16 (PciAccess, CapHeaderOffset);
      CapHeaderId = (UINT8)(Data16 & 0xFF);
      if (CapHeaderId == CapId) {
        if (CapHeaderOffset > PCI_MAXLAT_OFFSET) {
          ///
          /// Return valid capability offset
          ///
          return CapHeaderOffset;
        } else {
          ASSERT ((FALSE));
          return 0;
        }
      }
      ///
      /// Each capability must be DWORD aligned.
      /// The bottom two bits of all pointers (including the initial pointer at 34h) are reserved
      /// and must be implemented as 00b although software must mask them to allow for future uses of these bits.
      ///
      CapHeaderOffset = (UINT8)(Data16 >> 8);
    }
    return 0;
  }
}

/**
  Checks if device is a multifunction device
  Besides comparing Multifunction bit (BIT7) it checks if contents of HEADER_TYPE register
  make sense (header != 0xFF) to prevent false positives when called on devices which do not exist

  @param[in] PciAccess  Pointer to the device PCI config access.

  @retval TRUE if multifunction; FALSE otherwise
**/
BOOLEAN
PcieSipIsMultifunctionDevice (
  IN REGISTER_ACCESS  *PciAccess
  )
{
  UINT8 HeaderType;

  HeaderType = PciAccess->Read8 (PciAccess, PCI_HEADER_TYPE_OFFSET);
  if ((HeaderType == 0xFF) || ((HeaderType & HEADER_TYPE_MULTI_FUNCTION) == 0)) {
    return FALSE;
  }
  return TRUE;
}

/**
  Checks device's Slot Clock Configuration

  @param[in] PciAccess       Pointer to the device PCI config access.
  @param[in] PcieCapOffset   devices Pci express capability list register offset

  @retval TRUE when device uses slot clock, FALSE otherwise
**/
BOOLEAN
PcieSipGetScc (
  IN REGISTER_ACCESS  *PciAccess,
  IN UINT8            PcieCapOffset
  )
{
  return !!(PciAccess->Read16 (PciAccess, PcieCapOffset + R_PCIE_LSTS_OFFSET) & B_PCIE_LSTS_SCC);
}

/**
  Sets Common Clock Configuration bit for given device.

  @param[in] PciAccess       Pointer to the device PCI config access.
  @param[in] PcieCapOffset   devices Pci express capability list register offset
**/
VOID
PcieSipEnableCcc (
  IN REGISTER_ACCESS  *PciAccess,
  UINT8               PcieCapOffset
  )
{
  PciAccess->Or8 (PciAccess, PcieCapOffset + R_PCIE_LCTL_OFFSET, B_PCIE_LCTL_CCC);
}

/**
  Retrains link behind given device.
  It only makes sense to call it for downstream ports. If called for upstream port nothing will happen.
  If WaitUntilDone is TRUE function will wait until link retrain had finished, otherwise it will return immediately.
  Link must finish retrain before software can access the device on the other side. If it's not going to access it
  then considerable time can be saved by not waiting here.

  @param[in] PciAccess        Pointer to the device PCI config access.
  @param[in] PcieCapOffset    devices Pci express capability list register offset
  @param[in] WaitUntilDone    when TRUE, function waits until link has retrained
**/
VOID
PcieSipRetrainLink (
  IN REGISTER_ACCESS  *PciAccess,
  IN UINT8            PcieCapOffset,
  IN BOOLEAN          WaitUntilDone
  )
{
  UINT16 LinkTraining;
  UINT32 TimeoutUs;

  TimeoutUs = LINK_RETRAIN_WAIT_TIME;
  //
  // Before triggering link retrain make sure it's not already retraining. Otherwise
  // settings recently entered in LCTL register might go unnoticed
  //
  do {
    LinkTraining = (PciAccess->Read16 (PciAccess, PcieCapOffset + R_PCIE_LSTS_OFFSET) & B_PCIE_LSTS_LT);
    TimeoutUs--;
  } while (LinkTraining && (TimeoutUs != 0));

  PciAccess->Or8 (PciAccess, PcieCapOffset + R_PCIE_LCTL_OFFSET, B_PCIE_LCTL_RL);

  TimeoutUs = LINK_RETRAIN_WAIT_TIME;
  if (WaitUntilDone) {
    do {
      LinkTraining = (PciAccess->Read16 (PciAccess, PcieCapOffset + R_PCIE_LSTS_OFFSET) & B_PCIE_LSTS_LT);
      TimeoutUs--;
    } while (LinkTraining && (TimeoutUs != 0));
  }
}


/**
  Checks if lane reversal is enabled on a given Root Port

  @param[in] RpDev  Pointer to root port device.

  @retval TRUE if lane reversal is enbabled, FALSE otherwise
**/
BOOLEAN
PcieSipIsLaneReversalEnabled (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32  Data32;

  if (RpDev->SipVersion >= PcieSip17) {
    //
    // From SIP17, LR bit is moved from PCIEDBG to STRPFUSECFG
    //
    Data32 = RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_STRPFUSECFG);
    return !! (Data32 & B_PCIE_CFG_STRPFUSECFG_LR);
  } else {
    Data32 = RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_PCIEDBG);
    return !! (Data32 & B_PCIE_CFG_PCIEDBG_DTCA);
  }
}

/**
  Returns the PCIe controller configuration (4x1, 1x2-2x1, 2x2, 1x4)

  @param[in] RpDev  Pointer to root port device.

  @retval PCIe controller configuration
**/
PCIE_CONTROLLER_CONFIG
PcieSipGetControllerConfig (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32                      Data32;
  PCIE_CONTROLLER_CONFIG      Config;

  Data32 = RpDev->PciSbiMsgCfgAccess->Read32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_STRPFUSECFG);
  if (Data32 == MAX_UINT32) {
    DEBUG ((DEBUG_ERROR, "Failed to get controller config, defaulting to Pcie4x1\n"));
    return Pcie4x1;
  }

  if (RpDev->SipVersion >= PcieSip17) {
    //
    // For SIP17, STRPFUSECFG register layout is different than previously
    //
    Config = ((Data32 & B_PCIE_CFG_STRPFUSECFG_RPC_SIP17) >> N_PCIE_CFG_STRPFUSECFG_RPC);
    if (Config == 0x4) {
      return Pcie1x4;
    }
  } else {
    Config = ((Data32 & B_PCIE_CFG_STRPFUSECFG_RPC) >> N_PCIE_CFG_STRPFUSECFG_RPC);
  }
  return Config;
}

/**
  Programs Isolated Memory Region feature.
  PCIe IMR is programmed in a PCH rootport, based on data retrieved from CPU registers.

  @param[in] ControllerDev  Pointer to the controller to which root port belongs.
  @param[in] RpDev          Pointer to root port device.
**/
VOID
PcieSipEnablePcieImr (
  IN PCIE_ROOT_PORT_DEV   *ControllerDev,
  IN PCIE_ROOT_PORT_DEV   *RpDev
  )
{
  UINT32                      ImrBaseLow;
  UINT32                      ImrBaseHigh;
  UINT32                      ImrLimitLow;
  UINT32                      ImrLimitHigh;
  UINT32                      Data32;
  UINT64                      ImrLimit;

  DEBUG ((DEBUG_INFO, "%a: RP %d, bus %d\n", __FUNCTION__, RpDev->Id, RpDev->PrivateConfig.Rs3Bus));

  //
  // Sanity check - don't program PCIe IMR if base address is 0
  //
  if (RpDev->PrivateConfig.PciImrBase == 0) {
    DEBUG ((DEBUG_ERROR, "PcieImr base address is 0, IMR programming skipped!\n"));
    return;
  }
  ImrLimit = RpDev->PrivateConfig.PciImrBase + (RpDev->PrivateConfig.PciImrSize << 20);
  ImrBaseLow   = (UINT32) RShiftU64 ((RpDev->PrivateConfig.PciImrBase & 0xFFF00000), 20);
  ImrBaseHigh  = (UINT32) RShiftU64 (RpDev->PrivateConfig.PciImrBase, 32);
  ImrLimitLow  = (UINT32) RShiftU64 ((ImrLimit & 0xFFF00000), 20);
  ImrLimitHigh = (UINT32) RShiftU64 (ImrLimit, 32);

  Data32 = RpDev->PrivateConfig.Rs3Bus | (ImrBaseLow << N_PCIE_PCR_IMRAMBL_IAMB) | (ImrLimitLow << N_PCIE_PCR_IMRAMBL_IAML);
  ControllerDev->PciPcrAccess->Write32 (ControllerDev->PciPcrAccess, R_PCIE_PCR_IMRAMBL, Data32);
  ControllerDev->PciPcrAccess->Write32 (ControllerDev->PciPcrAccess, R_PCIE_PCR_IMRAMBU32, ImrBaseHigh);
  ControllerDev->PciPcrAccess->Write32 (ControllerDev->PciPcrAccess, R_PCIE_PCR_IMRAMLU32, ImrLimitHigh);
  ControllerDev->PciPcrAccess->Write32 (ControllerDev->PciPcrAccess, R_PCIE_PCR_IMRAMLE, (BIT0 << RpDev->PrivateConfig.RootPortIndexInController) | B_PCIE_PCR_IMRAMLE_SRL);
  ControllerDev->Callbacks.EnablePciImr (RpDev);

  DEBUG ((DEBUG_INFO, "IMR registers: Index %x, +10=%08x, +14=%08x, +18=%08x, +1c=%08x %d\n",
    ControllerDev->Id,
    ControllerDev->PciPcrAccess->Read32 (ControllerDev->PciPcrAccess, R_PCIE_PCR_IMRAMBL),
    ControllerDev->PciPcrAccess->Read32 (ControllerDev->PciPcrAccess, R_PCIE_PCR_IMRAMBU32),
    ControllerDev->PciPcrAccess->Read32 (ControllerDev->PciPcrAccess, R_PCIE_PCR_IMRAMLU32),
    ControllerDev->PciPcrAccess->Read32 (ControllerDev->PciPcrAccess, R_PCIE_PCR_IMRAMLE)
    ));
}

/**
  This function assigns bus number to PCIe bus .

  @param[in]  RpDev       Pointer to root port device.
**/
VOID
PcieSipAssignTemporaryBus (
  IN PCIE_ROOT_PORT_DEV  *RpDev,
  IN UINT8               TempPciBus
  )
{
  REGISTER_ACCESS  *EpPciAccess;
  PCIE_SBDF        EpSbdf;

  //
  // Assign bus numbers to the root port
  //
  RpDev->PciCfgAccess->AndThenOr32 (
    RpDev->PciCfgAccess,
    PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET,
    (UINT32) ~B_PCI_BRIDGE_BNUM_SBBN_SCBN,
    ((UINT32) (TempPciBus << 8)) | ((UINT32) (TempPciBus << 16))
    );
  //
  // A config write is required in order for the device to re-capture the Bus number,
  // according to PCI Express Base Specification, 2.2.6.2
  // Write to a read-only register VendorID to not cause any side effects.
  //
  EpSbdf.Segment = RpDev->Sbdf.Segment;
  EpSbdf.Bus = TempPciBus;
  EpSbdf.Device = 0;
  EpSbdf.Function = 0;
  RpDev->Callbacks.GetPciAccess (RpDev, &EpSbdf, &EpPciAccess);
  EpPciAccess->Write16 (EpPciAccess, PCI_VENDOR_ID_OFFSET, 0);
}

/**
  Clear temp bus usage.

  @param[in]  RpDev  Pointer to root port device.
**/
VOID
PcieSipClearBus (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  RpDev->PciCfgAccess->And32 (
    RpDev->PciCfgAccess,
    PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET,
    (UINT32) ~B_PCI_BRIDGE_BNUM_SBBN_SCBN
    );
}

/**
  This function sets Common Clock Mode bit in rootport and endpoint connected to it, if both sides support it.
  This bit influences rootport's Gen3 training and should be set before Gen3 software equalization is attempted.
  It does not attempt to set CCC in further links behind rootport

  @param[in] RpDev  Pointer to root port device.
**/
VOID
PcieSipEnableCommonClock (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  REGISTER_ACCESS  *EpPciAccess;
  UINT8            Func;
  UINT8            EpPcieCapOffset;
  PCIE_SBDF        EpSbdf;

  //
  // If endpoint gets reset as result of Gen3 training, it will forget all its config including CCC
  // while rootport will keep CCC set. The inconsistency will fix itself slightly later in boot flow when
  // PchPcieInitRootPortDownstreamDevices() is called
  //
  PcieSipAssignTemporaryBus (RpDev, RpDev->PrivateConfig.BusMin);
  //
  // The piece of code below used to be under "if (IsDevicePresent (EpBase))" check
  // which failed to detect Thunderbolt devices (they report DeviceID=0xFFFF)
  // That check helped readability but wasn't necessary - if endpoint is really missing,
  // PcieBaseFindCapId will return 0 and CCC programming will not execute anyway
  //
  EpSbdf.Segment = RpDev->Sbdf.Segment;
  EpSbdf.Bus = RpDev->PrivateConfig.BusMin;
  EpSbdf.Device = 0;
  EpSbdf.Function = 0;
  RpDev->Callbacks.GetPciAccess (RpDev, &EpSbdf, &EpPciAccess);
  EpPcieCapOffset = PcieSipFindCapId (EpPciAccess, EFI_PCI_CAPABILITY_ID_PCIEXP);
  if (PcieSipGetScc (RpDev->PciCfgAccess, R_PCIE_CFG_CLIST) && (EpPcieCapOffset != 0) && PcieSipGetScc (EpPciAccess, EpPcieCapOffset)) {
    PcieSipEnableCcc (RpDev->PciCfgAccess, R_PCIE_CFG_CLIST);
    PcieSipEnableCcc (EpPciAccess, EpPcieCapOffset);
    if (PcieSipIsMultifunctionDevice (EpPciAccess)) {
      for (Func = 1; Func <= PCI_MAX_FUNC; Func++) {
        EpSbdf.Function = Func;
        RpDev->Callbacks.GetPciAccess (RpDev, &EpSbdf, &EpPciAccess);
        PcieSipEnableCcc (EpPciAccess, PcieSipFindCapId (EpPciAccess, EFI_PCI_CAPABILITY_ID_PCIEXP));
      }
    }
    PcieSipRetrainLink (RpDev->PciCfgAccess, R_PCIE_CFG_CLIST, TRUE);
  }
  PcieSipClearBus (RpDev);
}

/**
  Get max PCIe link speed supported by the root port.

  @param[in]  RpDev  Pointer to root port device.

  @retval Max link speed
**/
UINT32
PcieSipGetMaxLinkSpeed (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  return RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_LCAP) & B_PCIE_LCAP_MLS;
}

/**
  Determines whether PCIe link is active

  @param[in] RpDev  Pointer to root port device.

  @retval Link Active state
**/
BOOLEAN
PcieSipIsLinkActive (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  return !! (RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_LSTS) & B_PCIE_LSTS_LA);
}

/**
  This function checks if de-emphasis needs to be changed from default for a given rootport

  @param[in] DevInfo  Information on device that is connected to rootport

  @retval TRUE   De-emphasis needs to be changed
  @retval FALSE  No need to change de-emphasis
**/
BOOLEAN
PcieSipNeedDecreasedDeEmphasis (
  IN PCIE_DEVICE_INFO  DevInfo
  )
{
  //
  // Intel WiGig devices
  //
  if (DevInfo.Vid == V_PCH_INTEL_VENDOR_ID && DevInfo.Did == 0x093C) {
    return TRUE;
  }
  return FALSE;
}

/**
  Disable PCIe root port in controller.

  @param[in] ControllerDev        Pointer to controller device.
  @param[in] RpIndexInController  Which root port in controller to disable.
**/
VOID
PcieSipDisableRootPortInController (
  IN PCIE_ROOT_PORT_DEV  *ControllerDev,
  IN UINT32              RpIndexInController
  )
{
  if (RpIndexInController >= 4) {
    return;
  }

  //
  // PCIe RP IOSF Sideband register offset 0x00[19:16], depending on the port that is Function Disabled
  // Access it by offset 0x02[4:0] to avoid RWO bit
  //
  if (ControllerDev->SipVersion >= PcieSip17) {
    ControllerDev->PciPcrAccess->AndThenOr8 (
      ControllerDev->PciPcrAccess,
      R_PCIE_SIP16_PCR_PCD + 0x02,
      0x0F,
      (UINT8) (1 << RpIndexInController)
      );
  } else {
    ControllerDev->PciPcrAccess->AndThenOr8 (
      ControllerDev->PciPcrAccess,
      R_PCIE_PCR_PCD + 0x02,
      0x0F,
      (UINT8) (1 << RpIndexInController)
      );
  }
}

/**
  Enable PCIe root port in controller.

  @param[in] ControllerDev        Pointer to controller device.
  @param[in] RpIndexInController  Which root port in controller to disable.
**/
VOID
PcieSipEnableRootPortInController (
  IN PCIE_ROOT_PORT_DEV  *ControllerDev,
  IN UINT32              RpIndexInController
  )
{
  if (RpIndexInController >= 4) {
    return;
  }

  //
  // PCIe RP IOSF Sideband register offset 0x00[19:16], depending on the port that is Function Disabled
  // Access it by offset 0x02[4:0] to avoid RWO bit
  //
  if (ControllerDev->SipVersion >= PcieSip17) {
    ControllerDev->PciPcrAccess->AndThenOr8 (
      ControllerDev->PciPcrAccess,
      R_PCIE_SIP16_PCR_PCD + 0x02,
      (UINT8) ~(1 << RpIndexInController),
      0x0
      );
  } else {
    ControllerDev->PciPcrAccess->AndThenOr8 (
      ControllerDev->PciPcrAccess,
      R_PCIE_PCR_PCD + 0x02,
      (UINT8) ~(1 << RpIndexInController),
      0x0
      );
  }
}

/**
  Checks if root port is disabled in controller.

  @param[in] ControllerDev        Pointer to controller device.
  @param[in] RpIndexInController  Which root port in controller to disable.

  @retval TRUE   Root port is enabled.
  @retval FALSE  Root port is disabled.
**/
BOOLEAN
PcieSipIsRootPortEnabledInController (
  IN PCIE_ROOT_PORT_DEV  *ControllerDev,
  IN UINT32              RpIndexInController
  )
{
  UINT8  RpDisabled;

  if (RpIndexInController >= 4) {
    return FALSE;
  }

  if (ControllerDev->SipVersion >= PcieSip17) {
    RpDisabled = ControllerDev->PciPcrAccess->Read8 (ControllerDev->PciPcrAccess, R_PCIE_SIP16_PCR_PCD + 0x02);
  } else {
    RpDisabled = ControllerDev->PciPcrAccess->Read8 (ControllerDev->PciPcrAccess, R_PCIE_PCR_PCD + 0x02);
  }
  return !((RpDisabled >> RpIndexInController) & 0x1);
}

/**
  Assign root port function number in PCIe controller.

  @param[in] ControllerDev        Pointer to controller device.
  @param[in] RpIndexInController  Which root port in controller to disable.
  @param[in] NewFunctionNumber    New function number.
**/
VOID
PcieSipAssignRootPortFunctionNumber (
  IN PCIE_ROOT_PORT_DEV  *ControllerDev,
  IN UINT32              RpIndexInController,
  IN UINT16              NewFunctionNumber
  )
{
  if (RpIndexInController >= 4 || NewFunctionNumber >= 8) {
    DEBUG ((DEBUG_ERROR, "%a incorrect rp index(%d) or function number(%d)\n", RpIndexInController, NewFunctionNumber));
    return;
  }

  if (ControllerDev->SipVersion >= PcieSip17) {
    ControllerDev->PciPcrAccess->AndThenOr32 (
      ControllerDev->PciPcrAccess,
      R_PCIE_SIP16_PCR_PCD,
      (UINT32) ~(B_PCIE_PCR_PCD_RP1FN << (RpIndexInController * S_PCIE_PCR_PCD_RP_FIELD)),
      NewFunctionNumber << (RpIndexInController * S_PCIE_PCR_PCD_RP_FIELD)
      );
  } else {
    ControllerDev->PciPcrAccess->AndThenOr32 (
      ControllerDev->PciPcrAccess,
      R_PCIE_PCR_PCD,
      (UINT32) ~(B_PCIE_PCR_PCD_RP1FN << (RpIndexInController * S_PCIE_PCR_PCD_RP_FIELD)),
      NewFunctionNumber << (RpIndexInController * S_PCIE_PCR_PCD_RP_FIELD)
      );
  }
}

/**
  Disables the root port. Depending on 2nd param, port's PCI config space may be left visible
  to prevent function swapping

  Use sideband access unless the port is still available.

  @param[in] RpList  Pointer to the root port list.
  @param[in] RpDev  Pointer to root port device.
**/
VOID
PcieSipDisableRootPort (
  IN PCIE_ROOT_PORT_LIST  *RpList,
  IN PCIE_ROOT_PORT_DEV   *RpDev
  )
{
  UINT32                      Data32;
  UINT32                      LoopTime;
  UINT32                      TargetState;
  UINT32                      LinkActive;
  PCIE_ROOT_PORT_DEV          *ControllerDev;
  EFI_STATUS                  Status;

  DEBUG ((DEBUG_INFO, "%a(%d) Start\n", __FUNCTION__, RpDev->Id));

  Data32 = RpDev->PciSbiMsgCfgAccess->Read32 (RpDev->PciSbiMsgCfgAccess, (R_PCIE_CFG_LSTS - 2));//access LSTS using dword-aligned read
  LinkActive = (Data32 >> 16) & B_PCIE_LSTS_LA;

  if (LinkActive) {
    ///
    /// If device is present disable the link.
    ///
    DEBUG ((DEBUG_INFO, "Disabling the link.\n"));
    RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCIEALC, B_PCIE_CFG_PCIEALC_BLKDQDA);
    RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_LCTL, B_PCIE_LCTL_LD);
    for (LoopTime = 0; LoopTime < 5000; LoopTime++) {
      Data32 = RpDev->PciSbiMsgCfgAccess->Read32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCIESTS1);
      if ((((Data32 & B_PCIE_CFG_PCIESTS1_LTSMSTATE) >> N_PCIE_CFG_PCIESTS1_LTSMSTATE) == V_PCIE_CFG_PCIESTS1_LTSMSTATE_DETRDY) ||
         (((Data32 & B_PCIE_CFG_PCIESTS1_LTSMSTATE) >> N_PCIE_CFG_PCIESTS1_LTSMSTATE) == V_PCIE_CFG_PCIESTS1_LTSMSTATE_DISWAITPG))  {
        break;
      }
      MicroSecondDelay (10);
    }
    RpDev->PciSbiMsgCfgAccess->And32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_LCTL, ~ (UINT32) B_PCIE_LCTL_LD);
  } else {
    ///
    /// Otherwise if device is not present perform the following steps using sideband access:
    /// 1.  Set B0:Dxx:Fn:338h[26] = 1b
    /// 2.  Poll B0:Dxx:Fn:328h[31:24] until 0x1 with 50ms timeout
    /// 3.  Set B0:Dxx:Fn +408h[27] =1b
    ///

    DEBUG ((DEBUG_INFO, "Stopping the port.\n"));
    RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCIEALC, B_PCIE_CFG_PCIEALC_BLKDQDA);

    TargetState = V_PCIE_CFG_PCIESTS1_LTSMSTATE_DETRDY;
    for (LoopTime = 0; LoopTime < DISABLE_TIMEOUT; LoopTime++) {
      Data32 = RpDev->PciSbiMsgCfgAccess->Read32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCIESTS1);
      if (((Data32 & B_PCIE_CFG_PCIESTS1_LTSMSTATE) >> N_PCIE_CFG_PCIESTS1_LTSMSTATE) == TargetState) {
        break;
      }
      MicroSecondDelay (10);
    }

    //
    // Show polling status
    //
    if (LoopTime > 0) {
      DEBUG ((DEBUG_INFO, "Polling for DETRDY for %dus. PCIESTS1 = 0x%08x\n", LoopTime*10, Data32));
    }
  }
  ///
  /// Set offset 408h[27] to 1b to disable squelch.
  ///
  RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PHYCTL4, B_PCIE_CFG_PHYCTL4_SQDIS);
  RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PMCS, V_PCIE_PMCS_PS_D3H);
  //
  // Make port disappear from PCI bus
  //
  Status = RpList->GetThisController (RpList, &ControllerDev);
  if (!EFI_ERROR (Status)) {
    PcieSipDisableRootPortInController (ControllerDev, RpDev->PrivateConfig.RootPortIndexInController);
  }
  RpDev->Callbacks.DisableRootPort (RpDev);
  DEBUG ((DEBUG_INFO, "%a End\n", __FUNCTION__));
}

/**
  This function creates SIP17 Capability and Extended Capability List

  @param[in] RpDev  Pointer to root port device.
**/
VOID
PcieSipInitCapabilityList (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32                        Data32;
  UINT16                        Data16;
  UINT8                         Data8;
  UINT16                        NextCap;
  UINT32                        Data32And;
  UINT32                        Data32Or;
  PCIE_ROOT_PORT_COMMON_CONFIG  *PcieRpCommonConfig;

  DEBUG ((DEBUG_INFO, "%a start\n", __FUNCTION__));

  PcieRpCommonConfig = &RpDev->PcieRootPortConfig->PcieRpCommonConfig;

  ///
  /// Build Capability linked list
  /// 1.  Read and write back to capability registers 34h, 41h, 81h and 91h using byte access.
  /// 2.  Program NSR, A4h[3] = 0b
  ///
  Data8 = RpDev->PciCfgAccess->Read8 (RpDev->PciCfgAccess, PCI_CAPBILITY_POINTER_OFFSET);
  RpDev->PciCfgAccess->Write8 (RpDev->PciCfgAccess, PCI_CAPBILITY_POINTER_OFFSET, Data8);

  Data16 = RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_CLIST);
  RpDev->PciCfgAccess->Write16 (RpDev->PciCfgAccess, R_PCIE_CFG_CLIST, Data16);

  if (RpDev->SipVersion >= PcieSip17) {
    RpDev->PciCfgAccess->Write16 (RpDev->PciCfgAccess, R_PCIE_CFG_MID, 0x9805);
  } else {
    Data16 = RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_MID);
    RpDev->PciCfgAccess->Write16 (RpDev->PciCfgAccess, R_PCIE_CFG_MID, Data16);
  }

  if  (RpDev->SipVersion >= PcieSip17) {
    Data16 = RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, 0x98);
    RpDev->PciCfgAccess->Write16 (RpDev->PciCfgAccess, 0x98, Data16);
  } else {
    Data16 = RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_SVCAP);
    RpDev->PciCfgAccess->Write16 (RpDev->PciCfgAccess, R_PCIE_CFG_SVCAP, Data16);
  }

  Data32 = RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_PMCS);
  Data32 &= (UINT32) ~(B_PCIE_PMCS_NSR);
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_PMCS, Data32);

  ///
  /// II.Build PCI Extended Capability linked list
  /// 0xEDC - PL16MECH(CAPID:0027h) Physical Layer 16.0 GT/s Margining Extended Capability Header
  /// 0xA9C - PL16GECH(CAPID:0026h) Physical Layer 16.0 GT/s Extended Capability Header
  /// 0xA90 - DLFECH  (CAPID:0025h) Data Link Feature Extended Capability Header
  /// 0xA30 - SPEECH  (CAPID:0019h) Secondary PCI Express Extended Capability Header
  /// 0xA00 - DPCECH  (CAPID:001Dh) DPC Extended Capability Header
  /// 0x280 - VCECH   (CAPID:0002h) VC capability
  /// 0x150 - PTMECH  (CAPID:001Fh) PTM Extended Capability Register
  /// 0x200 - L1SSECH (CAPID:001Eh) L1SS Capability register
  /// 0x220 - ACSECH  (CAPID:000Dh) ACS Capability register
  /// 0x100 - AECH    (CAPID:0001h) Advanced Error Reporting Capability
  ///
  /*
  a. NEXT_CAP = 0h
  */
  NextCap     = V_PCIE_EXCAP_NCO_LISTEND;

  if (PcieSipGetMaxLinkSpeed (RpDev) >= PcieGen4) {
    /*
    b. Program [0xEDC] Physical Layer 16.0 GT/s Margining Extended Capability Header(PL16MECH)
      1. Set Next Capability Offset, 0xEDC[31:20] = NEXT_CAP
      2. Set Capability Version, 0xEDC[19:16] = 1h
      3. Set Capability ID, 0xEDC[15:0] = 0027h
      4. NEXT_CAP = 0xEDC
    */
    Data32 = (V_PCIE_CFG_PL16MECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PL16MECH_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    NextCap = R_PCIE_CFG_PL16MECH;
    RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_PL16MECH, Data32);
    DEBUG ((DEBUG_VERBOSE, "Physical Layer 16.0 GT/s Margining Extended Capability Header\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PL16MECH_CID));

    /*
    c. Program [0xA9C] Physical Layer 16.0 GT/s Extended Capability Header(PL16GECH)
      1. Set Next Capability Offset, 0xA9C[31:20] = NEXT_CAP
      2. Set Capability Version, 0xA9C[19:16] = 1h
      3. Set Capability ID, 0xA9C[15:0] = 0026h
      4. NEXT_CAP = 0xA9C
    */
    Data32 = (V_PCIE_CFG_PL16GECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PL16GECH_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    NextCap = R_PCIE_CFG_PL16GECH;
    RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_PL16GECH, Data32);
    DEBUG ((DEBUG_VERBOSE, "Physical Layer 16.0 GT/s Extended Capability Header\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PL16GECH_CID));
  }

  /*
  d. Program [0xA90] Data Link Feature Extended Capability Header(DLFECH)
    1. Set Next Capability Offset, 0xA90[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA90[19:16] = 1h
    3. Set Capability ID, 0xA90[15:0] = 0025h
    4.  NEXT_CAP = 0xA90
  */
  if (RpDev->SipVersion >= PcieSip17) {
    Data32 = (V_PCIE_CFG_DLFECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_DLFECH_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    NextCap = R_PCIE_CFG_DLFECH;
    RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_DLFECH, Data32);
    DEBUG ((DEBUG_VERBOSE, "Data Link Feature Extended Capability Header\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_DLFECH_CID));
  }

  /*
  e. If the RP is GEN3 capable (by fuse and BIOS policy), enable Secondary PCI Express Extended Capability
    1. Set Next Capability Offset,0xA30[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA30[19:16] = 1h
    3. Set Capability ID,  0xA30[15:0] = 0019h
    4. NEXT_CAP = 0xA30
    ELSE, set 0xA30[31:0] = 0
  */
  Data32 = 0;
  if (PcieSipGetMaxLinkSpeed (RpDev) >= V_PCIE_LCAP_MLS_GEN3) {
    Data32 = (V_PCIE_CFG_EX_SPEECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_SPE_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "Secondary PCI Express Extended Capability\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_SPE_CID));
    NextCap = R_PCIE_CFG_EX_SPEECH;
  }
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_SPEECH, Data32);

  /*
  f. If Downstream Port Containment is enabled, then
    1. Set Next Capability Offset, 0xA00[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA00[19:16] = 1h
    3. Set Capability ID, 0xA00[15:0] = 001h
    4. NEXT_CAP = 0xA00
    ELSE, set 0xA00 [31:0] = 0
  */
  Data32 = 0;
  if (PcieRpCommonConfig->DpcEnabled == TRUE) {
    Data32 = (V_PCIE_CFG_EX_DPCECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_DPC_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "Downstream port containment\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_DPC_CID));
    NextCap = R_PCIE_CFG_EX_DPCECH;
  }
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_DPCECH, Data32);

  /*
    Set DPC capabilities
  */
  if (PcieRpCommonConfig->DpcEnabled == TRUE) {
    Data32 = RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_DPCECH + R_PCIE_EX_DPC_CAP_OFFSET);
    Data32 &= ~BIT5;
    if (PcieRpCommonConfig->RpDpcExtensionsEnabled == TRUE) {
      Data32 |= BIT5;
    }
  } else {
    Data32 = 0;
  }
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_DPCECH + R_PCIE_EX_DPC_CAP_OFFSET, Data32);

  /*
  g. If support PTM
    1. Set Next Capability Offset, Dxx:Fn +150h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +150h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +150h[15:0] = 001Fh
    4. Read and write back to Dxx:Fn +144h
    5. NEXT_CAP = 150h
    ELSE, set Dxx:Fn +150h [31:0] = 0
    In both cases: read Dxx:Fn + 154h, set BIT1 and BIT2 then write it back
  */
  Data32 = 0;
  if (PcieRpCommonConfig->PtmEnabled == TRUE) {
    Data32 = (V_PCIE_CFG_EX_PTM_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PTM_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "PTM\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PTM_CID));
    NextCap = R_PCIE_CFG_EX_PTMECH;
  }
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_PTMECH, Data32);
  Data32 = RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_PTMCAPR);
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_PTMCAPR, (Data32 | B_PCIE_EX_PTMCAP_PTMRC | B_PCIE_EX_PTMCAP_PTMRSPC));

  /*
  h. If support L1 Sub-State
    1. Set Next Capability Offset, Dxx:Fn +200h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +200h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +200h[15:0] = 001Eh
    4. Read and write back to Dxx:Fn +204h
    5. Refer to section 8.3 for other requirements (Not implemented here)
    6. NEXT_CAP = 200h
    ELSE, set Dxx:Fn +200h [31:0] = 0, and read and write back to Dxx:Fn +204h
  */
  Data32 = 0;
  if (RpDev->Status.ClkReqEnable &&
    (PcieRpCommonConfig->L1Substates != PchPcieL1SubstatesDisabled)) {
    Data32  = (V_PCIE_CFG_EX_L1S_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_L1S_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "L1SS\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_L1S_CID));
    NextCap = R_PCIE_CFG_EX_L1SECH;
  }
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_L1SECH, Data32);

  Data32And = RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_L1SCAP);
  if (PcieRpCommonConfig->L1Substates==PchPcieL1SubstatesDisabled) {
    Data32And &= (UINT32)~(0x1F);
  } else if (PcieRpCommonConfig->L1Substates==PchPcieL1SubstatesL1_1) {
    Data32And &= (UINT32)~(BIT0|BIT2|BIT4);
  }
  Data32And &= (UINT32)~(B_PCIE_EX_L1SCAP_PTV | B_PCIE_EX_L1SCAP_PTPOS | B_PCIE_EX_L1SCAP_CMRT);
  Data32Or = (RpDev->PrivateConfig.TPowerOn << N_PCIE_EX_L1SCAP_PTV) | (V_PCIE_EX_L1SCAP_PTPOS_2us << N_PCIE_EX_L1SCAP_PTPOS) | B_PCIE_EX_L1SCAP_L1SSES | (0x6E << N_PCIE_EX_L1SCAP_CMRT);
  if (RpDev->SipVersion > PcieSip17) {
    Data32Or |= (V_PCIE_EX_L1SCAP_PTPOS_10us << N_PCIE_EX_L1SCAP_PTPOS);
  }
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_L1SCAP, Data32And, Data32Or);

  /*
  i. If support ACS
    1. Set Next Capability Offset, Dxx:Fn +220h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +220h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +220h[15:0] = 000Dh
    4. Read and write back to Dxx:Fn +224h
    5. NEXT_CAP = 220h
    ELSE, set Dxx:Fn +220h [31:0] = 0, and read and write back to Dxx:Fn +224h
  */
  Data32 = 0;
  if (PcieRpCommonConfig->AcsEnabled == TRUE) {
    Data32 = (V_PCIE_CFG_EX_ACS_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_ACS_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "ACS\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_ACS_CID));
    NextCap = R_PCIE_CFG_EX_ACSECH;
  }
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_ACSECH, Data32);

  Data32 = RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_ACSCAPR);
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_EX_ACSCAPR, Data32);

  /*
  j. If support Advanced Error Reporting
    1. Set Next Capability Offset, Dxx:Fn +100h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +100h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +100h[15:0] = 0001h
    ELSE
    1. Set Next Capability Offset, Dxx:Fn +100h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +100h[19:16]  = 0h
    3. Set Capability ID, Dxx:Fn +100h[15:10]  = 0000h
  */
  Data32 = 0;
  if (PcieRpCommonConfig->AdvancedErrorReporting) {
    Data32 = (V_PCIE_CFG_AEC_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_AEC_CID;
  }
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_AECH, Data32);

  DEBUG ((DEBUG_INFO, "%a end\n", __FUNCTION__));
}

/**
  The function to change the root port speed

  @param[in] RpList  Pointer to the root port list.
**/
VOID
PcieSipRpSpeedChange (
  IN PCIE_ROOT_PORT_LIST  *RpList
  )
{
  UINTN                       TimeoutCount;
  UINT32                      MaxLinkSpeed;
  PCIE_ROOT_PORT_DEV          *RpDev;
  EFI_STATUS                  Status;
  UINT32                      NumOfRootPortsToTrain;

  ///
  /// PCH BIOS Spec Section 8.14 Additional PCI Express* Programming Steps
  /// NOTE: Detection of Non-Complaint PCI Express Devices
  ///
  NumOfRootPortsToTrain = 0;
  for (Status = RpList->ResetToFirst (RpList, &RpDev); Status == EFI_SUCCESS; Status = RpList->GetNextRootPort (RpList, &RpDev)) {
    if (RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
      continue;
    }

    MaxLinkSpeed = PcieSipGetMaxLinkSpeed (RpDev);
    if (MaxLinkSpeed > 1) {
      RpDev->PciCfgAccess->AndThenOr16 (
        RpDev->PciCfgAccess,
        R_PCIE_CFG_LCTL2,
        (UINT16) ~B_PCIE_LCTL2_TLS,
        (UINT16) MaxLinkSpeed
        );
      if (PcieSipIsLinkActive (RpDev)) {
        //
        // Retrain the link if device is present
        //
        RpDev->PciCfgAccess->Or16 (RpDev->PciCfgAccess, R_PCIE_CFG_LCTL, B_PCIE_LCTL_RL);
        RpDev->Status.LinkRetrainInProgress = TRUE;
        NumOfRootPortsToTrain++;
      }
    }
  }

  //
  // 15 ms timeout while checking for link active on retrained link
  //
  for (TimeoutCount = 0; ((NumOfRootPortsToTrain != 0) && (TimeoutCount < 150)); TimeoutCount++) {
    //
    // Delay 100 us
    //
    MicroSecondDelay (100);
    //
    // Check for remaining root port which was link retrained
    //
    for (Status = RpList->ResetToFirst (RpList, &RpDev); Status == EFI_SUCCESS; Status = RpList->GetNextRootPort (RpList, &RpDev)) {
      if (RpDev->Status.LinkRetrainInProgress) {
        //
        // If the link is active, clear the bitmap
        //
        if (RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_LSTS) & B_PCIE_LSTS_LA) {
          RpDev->Status.LinkRetrainInProgress = FALSE;
          NumOfRootPortsToTrain--;
        }
      }
    }
  }

  //
  // If 15 ms has timeout, and some link are not active, train to gen1
  //
  if (NumOfRootPortsToTrain != 0) {
    for (Status = RpList->ResetToFirst (RpList, &RpDev); Status != EFI_SUCCESS; Status = RpList->GetNextRootPort (RpList, &RpDev)) {
      if (RpDev->Status.LinkRetrainInProgress) {
        //
        // Set TLS to gen1
        //
        RpDev->PciCfgAccess->AndThenOr16 (RpDev->PciCfgAccess, R_PCIE_CFG_LCTL2,
          (UINT16) ~(B_PCIE_LCTL2_TLS),
          V_PCIE_LCTL2_TLS_GEN1);
        //
        // Retrain link
        //
        RpDev->PciCfgAccess->Or16 (RpDev->PciCfgAccess, R_PCIE_CFG_LCTL, B_PCIE_LCTL_RL);
      }
    }

    //
    // Wait for retrain completion or timeout in 15ms. Do not expect failure as
    // port was detected and trained as Gen1 earlier
    //
    for (TimeoutCount = 0; ((NumOfRootPortsToTrain != 0) && (TimeoutCount < 150)); TimeoutCount++) {
      //
      // Delay 100 us
      //
      MicroSecondDelay (100);
      //
      // Check for remaining root port which was link retrained
      //
      for (Status = RpList->ResetToFirst (RpList, &RpDev); Status == EFI_SUCCESS; Status = RpList->GetNextRootPort (RpList, &RpDev)) {
        if (RpDev->Status.LinkRetrainInProgress) {
          //
          // If the link is active, clear the bitmap
          //
          if (RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_LSTS) & B_PCIE_LSTS_LA) {
            RpDev->Status.LinkRetrainInProgress = FALSE;
          }
        }
      }
    }
  }
}

/**
  Checks if given rootport has an endpoint connected

  @param[in]  RpDev  Pointer to root port device.

  @retval TRUE   if endpoint is connected
  @retval FALSE  if no endpoint was detected
**/
BOOLEAN
PcieSipIsEndpointConnected (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  return !!(RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_SLSTS) & B_PCIE_SLSTS_PDS);
}

/**
  Get information about the endpoint

  @param[in]  RpDev       Pointer to root port device.
  @param[in]  TempPciBus  Temporary bus number
  @param[out] DeviceInfo  Device information structure

  @raturn TRUE if device was found, FALSE otherwise
**/
BOOLEAN
PcieSipGetDeviceInfo (
  IN PCIE_ROOT_PORT_DEV  *RpDev,
  IN  UINT8              TempPciBus,
  OUT PCIE_DEVICE_INFO   *DeviceInfo
  )
{
  UINT32                  Data32;
  UINT8                   EpPcieCapPtr;
  UINT8                   EpLinkSpeed;
  REGISTER_ACCESS         *EpPciAccess;
  PCIE_SBDF               EpSbdf;

  DeviceInfo->Vid = 0xFFFF;
  DeviceInfo->Did = 0xFFFF;
  DeviceInfo->MaxLinkSpeed = 0;

  //
  // Check for device presence
  //
  if (!PcieSipIsEndpointConnected (RpDev)) {
    return FALSE;
  }

  PcieSipAssignTemporaryBus (RpDev, TempPciBus);

  //
  // A config write is required in order for the device to re-capture the Bus number,
  // according to PCI Express Base Specification, 2.2.6.2
  // Write to a read-only register VendorID to not cause any side effects.
  //
  EpSbdf.Segment = RpDev->Sbdf.Segment;
  EpSbdf.Bus = TempPciBus;
  EpSbdf.Device = 0;
  EpSbdf.Function = 0;
  RpDev->Callbacks.GetPciAccess (RpDev, &EpSbdf, &EpPciAccess);
  EpPciAccess->Write16 (EpPciAccess,  PCI_VENDOR_ID_OFFSET, 0);

  Data32 = EpPciAccess->Read32 (EpPciAccess, PCI_VENDOR_ID_OFFSET);
  DeviceInfo->Vid = (UINT16) (Data32 & 0xFFFF);
  DeviceInfo->Did = (UINT16) (Data32 >> 16);

  EpLinkSpeed = 0;
  EpPcieCapPtr = PcieSipFindCapId (EpPciAccess, EFI_PCI_CAPABILITY_ID_PCIEXP);
  if (EpPcieCapPtr != 0) {
    EpLinkSpeed = EpPciAccess->Read8 (EpPciAccess, EpPcieCapPtr + R_PCIE_LCAP_OFFSET) & B_PCIE_LCAP_MLS;
  }
  DeviceInfo->MaxLinkSpeed = EpLinkSpeed;

  PcieSipClearBus (RpDev);

  DEBUG ((DEBUG_INFO, "VID: %04X DID: %04X  MLS: %d\n",
          DeviceInfo->Vid, DeviceInfo->Did, DeviceInfo->MaxLinkSpeed));

  return (Data32 != 0xFFFFFFFF);
}

/**
  Program controller power management settings.
  This settings are relevant to all ports including disabled ports.
  All registers are located in the first port of the controller.
  Use sideband access since primary may not be available.

  @param[in]  RpDev             Pointer to root port device.
  @param[in]  TrunkClockGateEn  Indicates whether trunk clock gating is to be enabled,
                                requieres all controller ports to have dedicated CLKREQ#
                                or to be disabled.
**/
VOID
PcieSipConfigureControllerBasePowerManagement (
  IN  PCIE_ROOT_PORT_DEV  *RpDev,
  IN  BOOLEAN              TrunkClockGateEn
  )
{
  UINT32                      Data32And;
  UINT32                      Data32Or;

  DEBUG ((DEBUG_INFO, "%a(%d)\n", __FUNCTION__, RpDev->Id));

  ASSERT (RpDev->IsController);

  Data32Or  =  B_PCIE_CFG_CCFG_DCGEISMA;
  RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_CCFG, Data32Or);
  //
  // Clock gating programming for the PCIE controller
  // Set E1h[5] to 1b to enable PCIe Link clock trunk gating.
  // Set E1h[4] to 1b to enable PCIe backbone clock trunk gating.
  // Set E1h[2] to 1b to enable shared resource dynamic backbone clock gating.
  // Set E1h[6] to 1b to enable Partition/Trunk Oscillator clock gating; if all ports on the controller support CLKREQ#.
  // Set E2h[4] to 1b to enable side clock gating.
  //
  Data32Or  = (B_PCIE_CFG_RPDCGEN_LCLKREQEN |
               B_PCIE_CFG_RPDCGEN_BBCLKREQEN | B_PCIE_CFG_RPDCGEN_SRDBCGEN) << 8;
  Data32Or |= B_PCIE_CFG_RPPGEN_SEOSCGE << 16;
  if (RpDev->SipVersion < PcieSip17) {
    Data32Or |= (B_PCIE_CFG_RPDCGEN_RPSCGEN << 8);
  }
  if (TrunkClockGateEn) {
    DEBUG ((DEBUG_INFO, "Setting PTOCGE\n"));
    Data32Or |= (B_PCIE_CFG_RPDCGEN_PTOCGE << 8);
  }
  RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_SPR, Data32Or);

  //
  // Enable Dynamic and Trunk Clock Gating Coupling Mode
  //
  if (RpDev->SipVersion >= PcieSip17) {
    Data32Or = B_PCIE_CFG_AECR1G3_DTCGCM;
    RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_AECR1G3, Data32Or);
    ///
    /// Progarm (R_PCIE_HWSNR) 5F0h
    /// Set 5F0h[3:0] to 0101b for Bank Enable Pulse Width.
    /// Set 5F0h[7:4] to 01b for Restore Enable Pulse Width. 01b = 2 clocks.
    /// Set 5F0h[9:8] to 10b for Entry and Exit hysteresis. 10b = 16clocks.
    ///
    Data32And = (UINT32)~(B_PCIE_CFG_HWSNR_BEPW_MASK | B_PCIE_CFG_HWSNR_REPW_MASK | B_PCIE_CFG_HWSNR_EEH_MASK);
    Data32Or = V_PCIE_CFG_HWSNR_BEPW_8CLK | (0x1 << N_PCIE_CFG_HWSNR_REPW_OFFSET) | (0x2 << N_PCIE_CFG_HWSNR_EEH_OFFSET);
    RpDev->PciSbiMsgCfgAccess->AndThenOr32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_HWSNR, Data32And, Data32Or);
    ///
    /// Program (R_PCIE_PGCTRL) 5F4h
    /// Set 5F4h[2:0] to 010b for PM_REQ Block Response Time.
    /// 010b = 10 micro seconds for 1LM & 101b = 25 micro seconds for 2LM(PEG60).
    ///
    RpDev->PciSbiMsgCfgAccess->AndThenOr32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PGCTRL, ~0u, V_PCIE_CFG_PGCTRL_PMREQBLKRSPT_10US);
  }

  //
  // Power gating programming for the PCIE controller
  //
  if (RpDev->SipVersion >= PcieSip17) {
    //
    // Set PM Request Controller Power Gating Exit Hysteresis to 0us
    //
    Data32And = (UINT32)~(B_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH_MASK);
    RpDev->PciSbiMsgCfgAccess->And32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCIEPMECTL3, Data32And);

    ///
    /// Set Controller Power Gating Exit Hysteresis to 0us.
    /// Set Controller Power Gating Entry Hysteresis to 0us.
    ///
    Data32And = (UINT32) ~(B_PCIE_CFG_PCIEPMECTL2_CPGEXH_MASK | B_PCIE_CFG_PCIEPMECTL2_CPGENH_MASK);
    RpDev->PciSbiMsgCfgAccess->And32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCIEPMECTL2, Data32And);
  }

  ///
  /// Program (R_PCIE_CFG_ACRG3) 6CCh
  /// Set 6CCh[23:22] to 00b to disable the CPG Wake Control.
  ///
  Data32And = (UINT32) ~(B_PCIE_CFG_ACRG3_CPGWAKECTRL_MASK);
  RpDev->PciSbiMsgCfgAccess->And32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_ACRG3, Data32And);

  ///
  /// Set E8h[17,15] to [1,1]
  ///
  Data32Or = B_PCIE_CFG_PWRCTL_WPDMPGEP | B_PCIE_CFG_PWRCTL_DBUPI;
  RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PWRCTL, Data32Or);

  ///
  /// Set F5h[1:0] to 11b  (R_PCIE_CFG_PHYCTL2)
  /// Set F7h[3:2] = 00b   (R_PCIE_CFG_IOSFSBCS)
  ///
  Data32And = (UINT32) ~(B_PCIE_CFG_IOSFSBCS_SIID << 24);
  Data32Or = (B_PCIE_CFG_PHYCTL2_PXPG3PLLOFFEN | B_PCIE_CFG_PHYCTL2_PXPG2PLLOFFEN) << 8;
  RpDev->PciSbiMsgCfgAccess->AndThenOr32 (RpDev->PciSbiMsgCfgAccess, 0xF4, Data32And, Data32Or);

  ///
  /// Set 424h[11] to 1b
  ///
  Data32Or = B_PCIE_CFG_PCIEPMECTL2_PHYCLPGE;
  RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCIEPMECTL2, Data32Or);

  ///
  ///
  /// Set Hardware Autonomous Enable 428h[5] to 1b
  /// Set PMC Request Enable 428h[0] to 0b
  /// Set Sleep Enable to 1b for SIP version 17
  ///
  Data32And = (UINT32) ~B_PCIE_CFG_PCE_PMCRE;
  Data32Or = B_PCIE_CFG_PCE_HAE;
  if (RpDev->SipVersion >= PcieSip17) {
    Data32Or |= B_PCIE_CFG_PCE_SE;
  }
  ///
  /// Set PMC Request Enable 428h[0] to 1b
  ///
  if (RpDev->SipVersion > PcieSip17) {
   Data32Or |= B_PCIE_CFG_PCE_PMCRE;
  }
  RpDev->PciSbiMsgCfgAccess->AndThenOr32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCE, Data32And, Data32Or);

  if (RpDev->SipVersion >= PcieSip17) {
    //
    // Set L1.PG Auto Power Gate Enable 434h[4] to 0.
    //
    Data32And = (UINT32)~(B_PCIE_CFG_PCIEPMECTL3_L1PGAUTOPGEN);
    RpDev->PciSbiMsgCfgAccess->And32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCIEPMECTL3, Data32And);

    //
    // Set PM_REQ Block Response Time to 10us
    //
    Data32And = (UINT32)~(B_PCIE_CFG_PGCTRL_PMREQBLKRSPT_MASK);
    Data32Or = V_PCIE_CFG_PGCTRL_PMREQBLKRSPT_10US;
    RpDev->PciSbiMsgCfgAccess->AndThenOr32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PGCTRL, Data32And, Data32Or);

    //
    // Set PM_REQ Block Response Time to 10us
    //
    Data32And = (UINT32)~(B_PCIE_ADVMCTRL_PMREQBLKPGRSPT_MASK | B_PCIE_ADVMCTRL_PMREQCWC_MASK);
    Data32Or = ((V_PCIE_ADVMCTRL_PMREQCWC_LNK_PRIM << N_PCIE_ADVMCTRL_PMREQCWC_OFFSET) | (V_PCIE_ADVMCTRL_PMREQBLKPGRSPT_10US << N_PCIE_ADVMCTRL_PMREQBLKPGRSPT_OFFSET));
    RpDev->PciSbiMsgCfgAccess->AndThenOr32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_ADVMCTRL, Data32And, Data32Or);


    Data32Or = ((V_PCIE_CFG_HWSNR_EEH_16CLK << N_PCIE_CFG_HWSNR_EEH_OFFSET) | \
               (V_PCIE_CFG_HWSNR_REPW_2CLK << N_PCIE_CFG_HWSNR_REPW_OFFSET) | \
               V_PCIE_CFG_HWSNR_BEPW_8CLK);
    Data32And = (UINT32)~(B_PCIE_CFG_HWSNR_EEH_MASK | B_PCIE_CFG_HWSNR_BEPW_MASK | B_PCIE_CFG_HWSNR_REPW_MASK);
    RpDev->PciSbiMsgCfgAccess->AndThenOr32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_HWSNR, Data32And, Data32Or);
  }

  if (RpDev->SipVersion < PcieSip17) {
    ///
    /// Set 42Ch[11:10] to 0x3
    ///
    Data32Or = BIT11 | BIT10;
    RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PGCBCTL1, Data32Or);
  }
}

/**
  Configure power management settings whcih are applicable to both enabled and disabled ports.
  This settings are relevant to all ports including disabled ports.
  Use sideband access since primary may not be available.

  @param[in]  RpDev            Pointer to root port device.
  @param[in]  PhyLanePgEnable  Indicates whether PHY lane power gating is to be enabled,
                               requires CLKREQ# to supported by the port or the port to be disabled.
**/
VOID
PcieSipConfigurePortBasePowerManagement (
  IN  PCIE_ROOT_PORT_DEV  *RpDev,
  IN  BOOLEAN             PhyLanePgEnable
  )
{
  UINT32                      Data32And;
  UINT32                      Data32Or;

  DEBUG ((DEBUG_INFO, "%a(%d) Start\n", __FUNCTION__, RpDev->Id));

  ///
  /// Set E1h[1:0] = 11b    (R_PCIE_CFG_RPDCGEN)
  ///

  if (RpDev->SipVersion == PcieSip17) {
    Data32Or = (B_PCIE_CFG_RPDCGEN_RPDLCGEN) << 8;
    RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, 0xE0, Data32Or);

    Data32Or = (B_PCIE_CFG_RPDCGEN_RPDBCGEN) << 8;
    RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, 0xE0, Data32Or);
  } else {
    Data32Or = (B_PCIE_CFG_RPDCGEN_RPDLCGEN | B_PCIE_CFG_RPDCGEN_RPDBCGEN) << 8;
    RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, 0xE0, Data32Or);
  }

  ///
  /// Set F7h[6] to 1b     (R_PCIE_CFG_IOSFSBCS)
  ///
  Data32Or = B_PCIE_CFG_IOSFSBCS_SCPTCGE << 24;
  RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, 0xF4, Data32Or);

  if (RpDev->SipVersion >= PcieSip17) {
    //
    // Set 0x434[3:2] to 1 to set Osc Clock Gate Hysterisis to 1us
    //
    Data32And = (UINT32)~(B_PCIE_CFG_PCIEPMECTL3_OSCCGH_MASK | B_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH_MASK);
    Data32Or = V_PCIE_CFG_PCIEPMECTL3_OSCCGH_1US << B_PCIE_CFG_PCIEPMECTL3_OSCCGH_OFFSET;
    RpDev->PciSbiMsgCfgAccess->AndThenOr32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCIEPMECTL3, Data32And, Data32Or);
    ConfigureDynamicClockGating (RpDev);
    //
    // Set 0x1330[10] FCTL2.HPICTL         - 1b
    // Set 0x1330[27] FCTL2.RXCPPREALLOCEN - 1b
    //
    RpDev->PciSbiMsgMemAccess->Write32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_FCTL2, BIT27 | BIT10);
  }

  ///
  /// Set 420h[31] = 1b
  /// If CLKREQ# is supported or port is disabled set 420h[30,29] to 11b.
  /// 420h[29] (DLSULDLSD) and 420h[0] must be programmed if DLSULPPGE is set or PTOCGE is set.
  /// Assume that if PTOCGE is set CLKREQ is supported on this port.
  /// L1.LOW is disabled; if all conditions are met, it will be enabled later.
  /// Set 420h[0] = 0
  ///
  Data32And = (UINT32) ~(B_PCIE_CFG_PCIEPMECTL_L1LE | B_PCIE_CFG_PCIEPMECTL_DLSULDLSD | B_PCIE_CFG_PCIEPMECTL_L1SNZCREWD | B_PCIE_CFG_PCIEPMECTL_L1FSOE);
  Data32Or  = B_PCIE_CFG_PCIEPMECTL_FDPPGE;
  if (RpDev->SipVersion < PcieSip17) {
    Data32Or  |= B_PCIE_CFG_PCIEPMECTL_L1SNZCREWD;
  }
  if (PhyLanePgEnable) {
    DEBUG ((DEBUG_INFO, "Setting DLSULPPGE+DLSULDLSD.\n"));
    Data32Or |= B_PCIE_CFG_PCIEPMECTL_DLSULPPGE;
    //
    // As per 4.4.4 Squelch Power Management section
    // DLSULDLSD 420h[29] = 0 SIP17, DLSULDLSD 420h[29] = 1 < SIP17
    // POFFWT 420h[19:18] = 11b
    //
    if (RpDev->SipVersion < PcieSip17) {
      Data32Or |= B_PCIE_CFG_PCIEPMECTL_DLSULDLSD;
    } else {
      Data32Or |= B_PCIE_CFG_PCIEPMECTL_POFFWT;
    }
  }
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_PCIEPMECTL, Data32And, Data32Or);

  ///
  /// Set 424h[8,7] to 11b
  /// Disable Chassis PMC Save and Restore
  ///
  Data32And = (UINT32) ~0;
  Data32Or = B_PCIE_CFG_PCIEPMECTL2_FDCPGE | B_PCIE_CFG_PCIEPMECTL2_DETSCPGE | B_PCIE_CFG_PCIEPMECTL2_DISSCPGE;
  if (RpDev->SipVersion >= PcieSip17) {
    Data32And &= (UINT32) ~(B_PCIE_CFG_PCIEPMECTL2_CPGEXH_MASK | B_PCIE_CFG_PCIEPMECTL2_CPGENH_MASK);
    Data32Or |= B_PCIE_CFG_PCIEPMECTL2_CPMCSRE;
  } else {
    Data32And &= (UINT32)~(B_PCIE_CFG_PCIEPMECTL2_CPMCSRE);
  }
  RpDev->PciSbiMsgCfgAccess->AndThenOr32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PCIEPMECTL2, Data32And, Data32Or);


  if (RpDev->SipVersion >= PcieSip17) {

    ///
    /// L1 Power Gating LTR Enable (L1PGLTREN) 0x5C0[0] 1
    /// L1 Power Gating LTR Threshold Latency Scale Value(L1PGLTRTLSV) 0x5C0[31:29] 0x2
    /// L1 Power Gating LTR Threshold Latency Value  (L1PGLTRTLV) 0x5C0[25:16] 0x32
    ///
    Data32And = (UINT32)(~(B_PCIE_CFG_PGTHRES_L1PGLTRTLSV_MASK | B_PCIE_CFG_PGTHRES_L1PGLTRTLV_MASK));
    Data32Or = (B_PCIE_CFG_PGTHRES_L1PGLTREN | 0x2 << N_PCIE_CFG_PGTHRES_L1PGLTRTLSV_OFFSET | 0x32 << N_PCIE_CFG_PGTHRES_L1PGLTRTLV_OFFSET);
    RpDev->PciSbiMsgCfgAccess->AndThenOr32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_PGTHRES, Data32And, Data32Or);
  }

  ///
  /// Vnn Removal register programming
  ///
  if (RpDev->PrivateConfig.VnnRemovalSupported) {
    Data32Or = (B_PCIE_CFG_VNNREMCTL_FDVNNRE | B_PCIE_CFG_VNNREMCTL_HPVNNRE | B_PCIE_CFG_VNNREMCTL_DNPVNNRE
      | B_PCIE_CFG_VNNREMCTL_RTD3VNNRE | B_PCIE_CFG_VNNREMCTL_LDVNNRE
      | (V_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_16_OSC_CLK << N_PCIE_CFG_VNNREMCTL_ISPLFVNNRE) | (V_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_8_OSC_CLK));
    RpDev->PciSbiMsgCfgAccess->Or32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_CFG_VNNREMCTL, Data32Or);
  }
  if (RpDev->SipVersion >= PCIE_SIP17) {
    ///
    /// Program (R_PCIE_ADVMCTRL) 5BCh
    /// Set 5BCh[22] to 1b for InRxL0 Control.
    /// Set 5BCh[21] to 0b for Accumulator Reset Mode
    /// Set 5BCh[20] to 1b for EIOS Disable DeSkew.
    /// Set 5BCh[19] to 1b for EIOS Mask Receiver Datapath.
    /// Set 5BCh[15] to 0b for RxL0 DeAssertion Control
    /// Set 5BCh[13] to 1b for Gen3 Short TLP Framing Error Reporting.
    /// Set 5BCh[11] to 0b for Reset Receiver Link Layer Common Logic.
    /// Set 5BCh[10] to 0b for Reset Link Layer in Gen1, 2 Recovery.
    ///
    Data32And = (UINT32)~(B_PCIE_ADVMCTRL_ACCRM | B_PCIE_ADVMCTRL_RXL0DC | B_PCIE_ADVMCTRL_RRLLCL | B_PCIE_ADVMCTRL_RLLG12R);
    Data32Or = (UINT32)(B_PCIE_ADVMCTRL_INRXL0CTRL | B_PCIE_ADVMCTRL_EIOSDISDS | B_PCIE_ADVMCTRL_EIOSMASKRX | B_PCIE_ADVMCTRL_G3STFER| (6 << N_PCIE_ADVMCTRL_PMREQCWC_OFFSET) |
                       (V_PCIE_ADVMCTRL_PMREQBLKPGRSPT_10US << N_PCIE_ADVMCTRL_PMREQBLKPGRSPT_OFFSET));
    RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_ADVMCTRL, Data32And, Data32Or);
    ///
    /// Program (R_PCIE_PGTHRES) 5C0h.
    /// Set 5C0h[0] to 1b for L1 power gating LTR Enable.
    /// Set 5C0h[31:29] to 10b for L1 power gating LTR Threshold latency Scale value. 010b = 1024nS
    /// Set 5C0h[25:16] to 110010b for L1 power gating LTR Threshold latency value. 110010b (0x32).
    ///
    Data32Or = (0x2 << N_PCIE_CFG_PGTHRES_L1PGLTRTLSV_OFFSET) | (0x32 << N_PCIE_CFG_PGTHRES_L1PGLTRTLV_OFFSET);
    RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_PGTHRES, ~0u, Data32Or);
  }
}

/**
  Verify whether the PCIe port does own all lanes according to the port width.

  @param[in] ControllerDev  Pointer to the controller dev.
  @param[in] RpDev          Pointer to root port device.

  @retval TRUE   All lanes are owned by PCIe controller.
  @retval FALSE  Not all lanes are owned by PCIe root port.
**/
BOOLEAN
PcieSipIsRpOwningLanes (
  IN PCIE_ROOT_PORT_DEV  *ControllerDev,
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32     MaxLinkWidth;
  UINT32     RpLaneIndex;
  UINT32     PhysicalLaneIndex;

  MaxLinkWidth = PcieSipGetMaxLinkWidth (RpDev);
  for (RpLaneIndex = 0; RpLaneIndex < MaxLinkWidth; ++RpLaneIndex) {
    if (PcieSipIsLaneReversalEnabled (ControllerDev)) {
      // 1 is subtracted to make the PhysicalLaneIndex 0-based.
      PhysicalLaneIndex = (RpDev->PrivateConfig.RootPortsInController * RpDev->PrivateConfig.LanesPerRootPort) - RpLaneIndex - 1;
    } else {
      PhysicalLaneIndex = RpLaneIndex;
    }
    if (!RpDev->Callbacks.IsPcieLaneConnected (RpDev, PhysicalLaneIndex)) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
  Check for device presence with timeout.

  @param[in]     RpDev      Pointer to root port device.
  @param[in]     TimeoutUs  Timeout in microseconds
  @param[in,out] Timer      Timer value, must be initialized to zero
                            before the first call of this function.
**/
BOOLEAN
PcieSipIsDevicePresent (
  IN PCIE_ROOT_PORT_DEV  *RpDev,
  IN     UINT32          TimeoutUs,
  IN OUT UINT32          *Timer
  )
{
  while (TRUE) {
    if (PcieSipIsEndpointConnected (RpDev)) {
      return TRUE;
    }
    if (*Timer < TimeoutUs) {
      MicroSecondDelay (10);
      *Timer += 10;
    } else {
      break;
    }
  }
  return FALSE;
}

/**
  Checks integrity of Policy settings for all rootports.
  Triggers assert if anything is wrong. For debug builds only

  @param[in] RpDev  Pointer to root port device.
**/
VOID
PcieSipPolicySanityCheck (
  IN  PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  ASSERT (!RpDev->PcieRootPortConfig->PcieRpCommonConfig.HotPlug || RpDev->PcieRootPortConfig->PcieRpCommonConfig.SlotImplemented);
}

/**
  This function provides BIOS workaround for WiFi device cannot enter S0i3 state due to LCTL.ECPM bit is cleared.
  This workaound is applied for Intel Wireless-AC 9260(Thunder Peak).
  This function does speed change earlier, so that Endpoint will be in correct state by the time
  root port and its downstream devices are initialized.

  @param[in] RpDev   Pointer to root port device.
  @param[in] DevInfo Information on device that is connected to rootport
  @param[in] Speed   PCIe root port policy speed setting
**/
VOID
PcieSipWifiLinkSpeedSyncWorkaround (
  IN  PCIE_ROOT_PORT_DEV  *RpDev,
  IN PCIE_DEVICE_INFO     DevInfo,
  IN UINT8                Speed
  )
{
  UINTN   TimeoutCount;

  if ((DevInfo.Vid == V_PCH_INTEL_VENDOR_ID) &&
      (DevInfo.Did == 0x2526) &&
      (Speed != PcieGen1)) {
    RpDev->PciCfgAccess->AndThenOr16 (
      RpDev->PciCfgAccess,
      R_PCIE_CFG_LCTL2,
      (UINT16) ~B_PCIE_LCTL2_TLS,
      (UINT16) DevInfo.MaxLinkSpeed
      );

    // Retrain the Link
    RpDev->PciCfgAccess->Or16 (RpDev->PciCfgAccess, R_PCIE_CFG_LCTL, B_PCIE_LCTL_RL);
    // 100 ms timeout while checking for link training is completed.
    for (TimeoutCount = 0; TimeoutCount < 1000; TimeoutCount++) {
      // Delay 100 us
      MicroSecondDelay (100);
      if ((RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_LSTS) & B_PCIE_LSTS_LT) == 0) {
        break;
      }
    }

    // 100 ms timeout while checking for link active on retrained link
    for (TimeoutCount = 0; TimeoutCount < 1000; TimeoutCount++) {
      // Delay 100 us
      MicroSecondDelay (100);
      if (PcieSipIsLinkActive (RpDev)) {
        break;
      }
    }
  }
}

/**
  Set SRL.

  @param[in] RpDev  Pointer to root port device.
**/
STATIC
VOID
PcieSipSetSecureRegisterLock (
  IN  PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32                                Data32Or;

  ///Secure Register Lock (SRL) 0xC8C[0]: 1
  ///Secure Equalization Register Lock (SERL) 0XC8C[8]: 1
  ///LTR Configuration Lock (LTRCFGLOCK) 0xC8C[16]: 1
  ///Device ID Override Lock (DIDOVR_LOCK) 0xC8C[24]: 1
  Data32Or = (B_PCIE_CFG_LPCR_SRL | B_PCIE_CFG_LPCR_SERL | B_PCIE_CFG_LPCR_DIDOVR_LOCK);
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_LPCR, Data32Or);
  DEBUG ((DEBUG_INFO, "R_PCIE_LPCR after setting SRL = %x\n", RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_LPCR)));
  ASSERT((RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_LPCR) & B_PCIE_CFG_LPCR_SRL) == B_PCIE_CFG_LPCR_SRL);
}

/**
  Performs mandatory Root Port Initialization.
  This function is silicon-specific and configures proprietary registers.

  @param[in] RpDev  Pointer to root port device.
**/
STATIC
VOID
PcieSipInitRootPort (
  IN  PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32                             Data32Or;
  UINT32                             Data32And;
  UINT16                             Data16;
  UINT16                             Data16Or;
  UINT16                             Data16And;
  UINT8                              Data8Or;
  UINT8                              Data8And;
  PCIE_ROOT_PORT_COMMON_CONFIG       *RootPortCommonConfig;
  UINT32                             Tls;
  PCIE_DEVICE_INFO                   DevInfo;
  UINT32                             RpMaxPayloadCapability;
  PCIE_SPEED                         RootPortSpeed;
  BOOLEAN                            DevicePresent;
  UINT8                              Offset;
  UINT32                             Data32;

  DEBUG ((DEBUG_INFO, "%a(%d) Start \n", __FUNCTION__, RpDev->Id));

  RootPortSpeed = PcieGen1;
  RootPortCommonConfig = &RpDev->PcieRootPortConfig->PcieRpCommonConfig;

  Tls = RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_LCTL2) & B_PCIE_LCTL2_TLS;
  ASSERT (Tls < V_PCIE_LCTL2_TLS_GEN3);

  /// PCH BIOS Spec Section 8.2.10 Completion Retry Status Replay Enable
  /// Following reset it is possible for a device to terminate the
  /// configuration request but indicate that it is temporarily unable to process it,
  /// but in the future. The device will return the Configuration Request Retry Status.
  /// By setting the Completion Retry Status Replay Enable, Dxx:Fn + 320h[22],
  /// the RP will re-issue the request on receiving such status.
  /// The BIOS shall set this bit before first configuration access to the endpoint.
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_PCIECFG2, B_PCIE_CFG_PCIECFG2_CRSREN);
  //
  // Set speed capability in rootport
  //
  Data8And = (UINT8)(~((UINT8)(B_PCIE_CFG_MPC_PCIESD >> 8)));
  Data8Or = 0;
  Offset = 8;
  if (RpDev->SipVersion >= PcieSip17) {
    Offset = 9;
  }
  switch (RootPortCommonConfig->PcieSpeed) {
    case PcieGen1:
      Data8Or |= (V_PCIE_CFG_MPC_PCIESD_GEN1 << (N_PCIE_CFG_MPC_PCIESD - Offset));
      break;
    case PcieGen2:
      Data8Or |= (V_PCIE_CFG_MPC_PCIESD_GEN2 << (N_PCIE_CFG_MPC_PCIESD - Offset));
      break;
    case PcieGen3:
      Data8Or |= (V_PCIE_CFG_MPC_PCIESD_GEN3 << (N_PCIE_CFG_MPC_PCIESD - Offset));
      break;
    case PcieGen4:
      Data8Or |= (V_PCIE_CFG_MPC_PCIESD_GEN4 << (N_PCIE_CFG_MPC_PCIESD - Offset));
      break;
    case PcieAuto:
      break;
  }
  RpDev->PciCfgAccess->AndThenOr8 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC + 1, Data8And, Data8Or);

  DevicePresent = PcieSipGetDeviceInfo (RpDev, RpDev->PrivateConfig.BusMin, &DevInfo);
  RootPortSpeed = (PCIE_SPEED) PcieSipGetMaxLinkSpeed (RpDev);

  if (PcieSipNeedDecreasedDeEmphasis (DevInfo)) {
    RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_LCTL2, B_PCIE_LCTL2_SD);
  }

  PcieSipWifiLinkSpeedSyncWorkaround (RpDev, DevInfo, RootPortCommonConfig->PcieSpeed);

  ///
  /// If only 128B max payload is supported set CCFG.UNRS to 0.
  ///
  /// and program all the PCH Root Ports such that upstream posted writes and upstream non-posted requests
  /// are split at 128B boundary by setting CCFG fields: UPSD to 0, CCFG.UPRS to 000b and UNSD to 0, UNRS to 000b
  ///
  Data32And = ~0u;
  Data32Or  = 0;
  RpMaxPayloadCapability = PchPcieMaxPayload256;
  if (RootPortCommonConfig->MaxPayload == PchPcieMaxPayload128 ||
      RootPortCommonConfig->EnablePeerMemoryWrite) {
    RpMaxPayloadCapability = PchPcieMaxPayload128;

    Data32And &= (UINT32) ~(B_PCIE_CFG_CCFG_UNSD | B_PCIE_CFG_CCFG_UNRS);
    Data32Or  |= (UINT32)  (V_PCIE_CFG_CCFG_UNRS_128B << N_PCIE_CFG_CCFG_UNRS);

    if (RootPortCommonConfig->EnablePeerMemoryWrite) {
      Data32And &= (UINT32) ~(B_PCIE_CFG_CCFG_UPMWPD |
                              B_PCIE_CFG_CCFG_UPSD | B_PCIE_CFG_CCFG_UPRS);
      Data32Or  |= (UINT32)  (V_PCIE_CFG_CCFG_UPRS_128B << N_PCIE_CFG_CCFG_UPRS);
    }
  }
  if ((RootPortCommonConfig->MaxPayload == PchPcieMaxPayload256) && (RpDev->SipVersion >= PcieSip17)) {
    Data32Or |= (V_PCIE_CFG_CCFG_UPRS_256B << N_PCIE_CFG_CCFG_UPRS);
  }
  ASSERT (RootPortCommonConfig->MaxPayload < PchPcieMaxPayloadMax);
  ///
  /// Set B0:Dxx:Fn + D0h [13:12] to 01b
  ///
  Data32And &= (UINT32) ~B_PCIE_CFG_CCFG_UNRD;
  Data32Or  |=  (1u << N_PCIE_CFG_CCFG_UNRD);

  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_CCFG, Data32And, Data32Or);

  RpDev->PciCfgAccess->AndThenOr16 (RpDev->PciCfgAccess, R_PCIE_CFG_DCAP, (UINT16) ~B_PCIE_DCAP_MPS, (UINT16)RpMaxPayloadCapability);

  //
  // Set IOSF max payload size. For root ports with SIP version > 17 set 64 byte aligned
  // offset.
  //
  Data32And = (UINT32)~(B_PCIE_CFG_IPCS_IMPS_MASK);
  if (RpDev->SipVersion > PcieSip17) {
    Data32Or = (V_PCIE_CFG_IPCS_IMPS_64B << B_PCIE_CFG_IPCS_IMPS_OFFSET);
  } else {
    Data32Or = (V_PCIE_CFG_IPCS_IMPS_256B << B_PCIE_CFG_IPCS_IMPS_OFFSET);
  }
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_IPCS, Data32And, Data32Or);

  Data32Or = 0x7F;
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_VC0CTL, Data32Or);
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_VC1CTL, V_PCIE_CFG_VC1CTL_ID_ONE << B_PCIE_CFG_VC1CTL_ID_OFFSET);

  ///
  /// PCH BIOS Spec Section 8.15.1 Power Optimizer Configuration
  /// If B0:Dxx:Fn + 400h is programmed, BIOS will also program B0:Dxx:Fn + 404h [1:0] = 11b,
  /// to enable these override values.
  /// - Fn refers to the function number of the root port that has a device attached to it.
  /// - Default override value for B0:Dxx:Fn + 400h should be 880F880Fh
  /// - Also set 404h[2] to lock down the configuration
  /// - Refer to table below for the 404h[3] policy bit behavior.
  /// Done in PcieSetPm()
  ///
  /// PCH BIOS Spec Section 8.15.1 Power Optimizer Configuration
  /// Program B0:Dxx:Fn + 64h [11] = 1b
  ///
  Data32Or = 0;
  Data32And = ~0u;
  if (RootPortCommonConfig->LtrEnable == TRUE) {
    Data32Or |= B_PCIE_DCAP2_LTRMS;
  } else {
    Data32And &= (UINT32) ~(B_PCIE_DCAP2_LTRMS);
  }

  ///
  /// Latency Tolerance Reporting Override (LTROVR) 0x400  31:0 0x88428842
  ///
  Data32And = 0xFFFFFFFF;
  Data32Or  = 0x88428842;
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_LTROVR, Data32And, Data32Or);

  ///
  /// LTR LTR Override Policy (LTROVRPLCY) 0x404 3 0
  /// LTR Non-Snoop Override Enable (LTRNSOVREN) 0x404  1 1
  /// LTR Snoop Override Enable (LTRSOVREN) 0x404  0 1
  ///
  Data32And = (UINT32) ~(B_PCIE_CFG_LTROVR2_FORCE_OVERRIDE | B_PCIE_CFG_LTROVR2_LTRNSOVREN | B_PCIE_CFG_LTROVR2_LTRSOVREN);
  Data32Or  = (UINT32) (B_PCIE_CFG_LTROVR2_LTRNSOVREN | B_PCIE_CFG_LTROVR2_LTRSOVREN);
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_LTROVR2, Data32And, Data32Or);

  //
  // Squelch Power Management
  //
  ConfigurePcieSquelchPowerManagement (RpDev);

  //
  // Configure Phy Settings
  //
  PcieSipConfigurePhyInit (RpDev);

  ///
  /// PCH BIOS Spec Section 8.15.1 Power Optimizer Configuration
  /// Program B0:Dxx:Fn + 68h [10] = 1b
  ///
  Data16 = RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_DCTL2);
  if (RootPortCommonConfig->LtrEnable == TRUE) {
    Data16 |= B_PCIE_DCTL2_LTREN;
  } else {
    Data16 &= (UINT16) ~(B_PCIE_DCTL2_LTREN);
  }
  //
  // Program ARE and AEB
  //
  RpDev->PciCfgAccess->AndThenOr16 (RpDev->PciCfgAccess, R_PCIE_CFG_DCTL2, (UINT16) ~(B_PCIE_CFG_DCTL2_AEB | B_PCIE_CFG_DCTL2_ARE), Data16);
  ///
  /// PCH BIOS Spec Section 8.15.1 Power Optimizer Configuration
  /// Step 3 done in PciExpressHelpersLibrary.c ConfigureLtr
  ///

  ///
  /// Set Dxx:Fn + 300h[23:00] = 0B75FA7h
  /// Set Dxx:Fn + 304h[11:00] = 0C97h
  ///
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_PCIERTP1, ~0x00FFFFFFu, 0x00B75FA7);
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_PCIERTP2, ~0x00000FFFu, 0x00000C97);

  //
  // Elastic buffer and simplified retimer mode programming. Needs to be done before equalization.
  //
  if (RpDev->SipVersion >= PcieSip17) {
    //
    // Set Dxx:Fn + 0xC50 [5, 6, 7, 8, 9]     to [1, 1, 1, 1, 1] for SIP17
    // Set Dxx:Fn + 0xC50 [5, 6, 7, 8, 9, 20] to [1, 1, 1, 0, 0, 0] for SipVersion > SIP17
    //
    Data32Or  = (B_PCIE_CFG_ACGR3S2_G2EBM | B_PCIE_CFG_ACGR3S2_G1EBM | B_PCIE_CFG_ACGR3S2_SRT);
    Data32And = 0xFFFFFFFF;
    if (RpDev->SipVersion > PcieSip17) {
      Data32And = (UINT32) ~(B_PCIE_CFG_ACGR3S2_G5EBM | B_PCIE_CFG_ACGR3S2_G4EBM | B_PCIE_CFG_ACGR3S2_G3EBM);
    } else {
      Data32Or |= B_PCIE_CFG_ACGR3S2_G4EBM | B_PCIE_CFG_ACGR3S2_G3EBM;
    }
    RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_ACGR3S2, Data32And, Data32Or);

    Data32And = ~(UINT32)(B_PCIE_CFG_AECR1G3_L1OFFRDYHEWT | B_PCIE_CFG_AECR1G3_L1OFFRDYHEWTEN);
    RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_AECR1G3, Data32And, B_PCIE_CFG_AECR1G3_TPSE);
  }

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set Dxx:Fn + 318h [31:16] = 1414h (Gen2 and Gen1 Active State L0s Preparation Latency)
  ///
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_PCIEL0SC, ~0xFFFF0000u, 0x14140000);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// 1.  Program Dxx:Fn + E8h[20] to [1]
  ///
  Data32Or = 0;
  if (RpDev->SipVersion < PcieSip17) {
    Data32Or = B_PCIE_CFG_PWRCTL_LTSSMRTC;
  }

  if (RpDev->SipVersion >= PcieSip17) {
    Data32Or |= B_PCIE_CFG_PWRCTL_LIFECF;
  }
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_PWRCTL, Data32Or);
  ///
  /// 2.  Program Dxx:Fn + 320h[27] to [1]
  /// Enable PCIe Relaxed Order to always allow downstream completions to pass posted writes,
  /// 3.  Set B0:Dxx:Fn:320h[24:23] = 11b
  /// Set PME timeout to 10ms, by
  /// 4.  Set B0:Dxx:Fn:320h[21:20] = 01b
  ///

  Data32And = (UINT32) ~B_PCIE_CFG_PCIECFG2_PMET_MASK;
  Data32Or  = B_PCIE_CFG_PCIECFG2_RLLG3R |
    B_PCIE_CFG_PCIECFG2_CROAOV |
    B_PCIE_CFG_PCIECFG2_CROAOE;
  if (RpDev->SipVersion < PcieSip17) {
    Data32Or |= (V_PCIE_CFG_PCIECFG2_PMET << B_PCIE_CFG_PCIECFG2_PMET_OFFSET);
  }

  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_PCIECFG2, Data32And, Data32Or);

  ///
  /// Enable Completion Time-Out Non-Fatal Advisory Error, Dxx:Fn + 324h[14] = 1b
  /// Set LDSWQRP Dxx:Fn + 324h[13] = 0
  ///
  Data32And = (UINT32) ~(B_PCIE_CFG_PCIEDBG_LDSWQRP);
  Data32Or  = B_PCIE_CFG_PCIEDBG_CTONFAE;
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_PCIEDBG, Data32And, Data32Or);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Program Dxx:Fn + 424h [6, 5, 4] = [1, 1, 1]
  ///
  RpDev->PciCfgAccess->Or32 (
    RpDev->PciCfgAccess,
    R_PCIE_CFG_PCIEPMECTL2,
    (B_PCIE_CFG_PCIEPMECTL2_L23RDYSCPGE |
     B_PCIE_CFG_PCIEPMECTL2_L1SCPGE)
    );
  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// If Dxx:Fn + F5h[0] = 1b or step 3 is TRUE, set Dxx:Fn + 4Ch[17:15] = 4h
  /// Else set Dxx:Fn + 4Ch[17:15] = 010b
  ///
  Data32And = (UINT32) (~B_PCIE_LCAP_EL1);
  if (RpDev->SipVersion != PcieSip17) {
    Data32Or = 4 << N_PCIE_LCAP_EL1;
  } else {
    Data32Or = (6 << N_PCIE_LCAP_EL1);
}

  ///
  /// Set LCAP APMS according to platform policy.
  ///
  if (RootPortCommonConfig->Aspm < PchPcieAspmAutoConfig) {
    Data32And &= (UINT32) ~B_PCIE_LCAP_APMS;
    Data32Or  |= RootPortCommonConfig->Aspm << N_PCIE_LCAP_APMS;
  } else {
    if (RpDev->SipVersion != PcieSip17) {
      Data32Or |= B_PCIE_LCAP_APMS_L0S | B_PCIE_LCAP_APMS_L1;
    } else {
      DEBUG ((DEBUG_INFO, "Programming LCAP to L1 only \n"));
      Data32Or |= B_PCIE_LCAP_APMS_L1;
    }
  }


  //
  // The EL1, ASPMOC and APMS of LCAP are RWO, must program all together.
  //
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_LCAP, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "LCAP = 0x%x \n", RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_LCAP)));

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Configure PCI Express Number of Fast Training Sequence for each port
  /// 1.  Set Dxx:Fn + 314h [31:24, 23:16, 15:8, 7:0] to [7Eh, 70h, 3Fh, 38h]
  /// 2.  Set Dxx:Fn + 478h [31:24, 15:8, 7:0] to [28h, 3Dh, 2Ch]
  ///
  if (RpDev->SipVersion != PcieSip17) {
    Data32 = 0x7E703F38;
  } else {
    Data32 = 0x7E5B3F2C;
  }
  RpDev->PciCfgAccess->Write32 (RpDev->PciCfgAccess, R_PCIE_CFG_PCIENFTS, Data32);
  if (RpDev->SipVersion != PcieSip17) {
    Data32 =  0x28003D2C;
  } else {
    Data32 = 0x2800402C;
  }
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_G3L0SCTL, 0x00FF0000u, Data32);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set MPC.IRRCE, Dxx:Fn + D8h[25] = 1b using byte access
  /// For system that support MCTP over PCIE set
  /// Set PCIE RP PCI offset D8h[27] = 1b
  /// Set PCIE RP PCI offset D8h[3] = 1b
  /// Set Flow Control Update Policy D8[6:4] = 0x4 for SIP Version > 17
  ///
  Data8And = (UINT8) (~(B_PCIE_CFG_MPC_IRRCE | B_PCIE_CFG_MPC_MMBNCE) >> 24);
  Data8Or = B_PCIE_CFG_MPC_MMBNCE >> 24;
  if (RpDev->SipVersion >= PcieSip17) {
    Data8Or |= (UINT8) (B_PCIE_CFG_MPC_FCDL1E >> 24);
  }
  if (!RpDev->PrivateConfig.VtdEnabled) {
    Data8Or |= B_PCIE_CFG_MPC_IRRCE >> 24;
  }
  RpDev->PciCfgAccess->AndThenOr8 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC + 3, Data8And, Data8Or);

  Data8And = (UINT8) ~(B_PCIE_CFG_MPC_MCTPSE | B_PCIE_CFG_MPC_FCP);
  Data8Or  = (UINT8) (B_PCIE_CFG_MPC_MCTPSE | (V_PCIE_CFG_MPC_FCP << B_PCIE_CFG_MPC_FCP_OFFSET));
  RpDev->PciCfgAccess->AndThenOr8 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC, Data8And, Data8Or);

  ///
  /// Peer Disable. Starting from SIP16
  ///
  PcieSipConfigurePeerDisable (RpDev, FALSE, TRUE);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set PCIE RP PCI offset F5h[7:4] = 0000b
  ///
  RpDev->PciCfgAccess->And8 (RpDev->PciCfgAccess, R_PCIE_CFG_PHYCTL2, (UINT8) ~(B_PCIE_CFG_PHYCTL2_TDFT | B_PCIE_CFG_PHYCTL2_TXCFGCHGWAIT));

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Enable PME_TO Time-Out Policy, Dxx:Fn + E2h[6] =1b
  ///
  RpDev->PciCfgAccess->Or8 (RpDev->PciCfgAccess, R_PCIE_CFG_RPPGEN, B_PCIE_CFG_RPPGEN_PTOTOP);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// If there is no IOAPIC behind the root port, set EOI Forwarding Disable bit (PCIE RP PCI offset D4h[1]) to 1b.
  /// For Empty Hot Plug Slot, set is done in InitPcieSingleRootPort ()
  ///

  ///
  /// System bios should initiate link retrain for all slots that has card populated after register restoration.
  /// Done in PchPciExpressHelpersLibrary.c PchPcieInitRootPortDownstreamDevices ()
  ///

  ///
  /// Configure Completion Timeout
  ///
  Data16And = (UINT16) ~(B_PCIE_DCTL2_CTD | B_PCIE_DCTL2_CTV);
  Data16Or  = 0;
  if (RootPortCommonConfig->CompletionTimeout == PcieCompletionTO_Disabled) {
    Data16Or = B_PCIE_DCTL2_CTD;
  } else {
    switch (RootPortCommonConfig->CompletionTimeout) {
      case PcieCompletionTO_Default:
        Data16Or = V_PCIE_DCTL2_CTV_DEFAULT;
        break;

      case PcieCompletionTO_16_55ms:
        Data16Or = V_PCIE_DCTL2_CTV_40MS_50MS;
        break;

      case PcieCompletionTO_65_210ms:
        Data16Or = V_PCIE_DCTL2_CTV_160MS_170MS;
        break;

      case PcieCompletionTO_260_900ms:
        Data16Or = V_PCIE_DCTL2_CTV_400MS_500MS;
        break;

      case PcieCompletionTO_1_3P5s:
        Data16Or = V_PCIE_DCTL2_CTV_1P6S_1P7S;
        break;

      default:
        Data16Or = 0;
        break;
    }
  }

  RpDev->PciCfgAccess->AndThenOr16 (RpDev->PciCfgAccess, R_PCIE_CFG_DCTL2, Data16And, Data16Or);

  PcieSipConfigure10BitTag (RpDev, FALSE, FALSE);
  PcieConfigureCoalescing (RpDev);
  ///
  /// For Root Port Slots Numbering on the CRBs.
  ///
  Data32Or  = 0;
  Data32And = (UINT32) (~(B_PCIE_SLCAP_SLV | B_PCIE_SLCAP_SLS | B_PCIE_SLCAP_PSN));
  ///
  /// PCH BIOS Spec section 8.8.2.1
  /// Note: If Hot Plug is supported, then write a 1 to the Hot Plug Capable (bit6) and Hot Plug
  /// Surprise (bit5) in the Slot Capabilities register, PCIE RP PCI offset 54h. Otherwise,
  /// write 0 to the bits PCIe Hot Plug SCI Enable
  ///
  Data32And &= (UINT32) (~(B_PCIE_SLCAP_HPC | B_PCIE_SLCAP_HPS));
  if (RootPortCommonConfig->HotPlug) {
    Data32Or |= B_PCIE_SLCAP_HPC | B_PCIE_SLCAP_HPS;
  }
  ///
  /// Get the width from LCAP
  /// Slot Type  X1  X2/X4/X8/X16
  /// Default     10W   25W
  /// The slot power consumption and allocation is platform specific. Please refer to the
  /// "PCI Express* Card Electromechanical (CEM) Spec" for details.
  ///
  if (RootPortCommonConfig->SlotPowerLimitValue != 0) {
    Data32Or |= (UINT32) (RootPortCommonConfig->SlotPowerLimitValue << N_PCIE_SLCAP_SLV);
    Data32Or |= (UINT32) (RootPortCommonConfig->SlotPowerLimitScale << N_PCIE_SLCAP_SLS);
  } else {
    if (PcieSipGetMaxLinkWidth (RpDev) == 1) {
      Data32Or |= (UINT32) (100 << N_PCIE_SLCAP_SLV);
      Data32Or |= (UINT32) (1 << N_PCIE_SLCAP_SLS);
    } else if (PcieSipGetMaxLinkWidth (RpDev) >= 2) {
      Data32Or |= (UINT32) (250 << N_PCIE_SLCAP_SLV);
      Data32Or |= (UINT32) (1 << N_PCIE_SLCAP_SLS);
    }
  }

  ///
  /// PCH BIOS Spec section 8.2.4
  /// Initialize Physical Slot Number for Root Ports
  ///
  Data32Or |= (UINT32) (RootPortCommonConfig->PhysicalSlotNumber << N_PCIE_SLCAP_PSN);
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_SLCAP, Data32And, Data32Or);
  PcieSipInitCapabilityList (RpDev);

  //
  // All actions involving LinkDisable must finish before anything is programmed on endpoint side
  // because LinkDisable resets endpoint
  //

  ///
  /// Perform equalization for Gen3 capable ports
  ///
  if (RootPortSpeed >= 3) {
    //
    // Link EQ has to be performed with CCC set if supported.
    //
    PcieSipEnableCommonClock (RpDev);
    PcieSipLinkEqualize (RpDev,
                         DevicePresent,
                         &RpDev->PrivateConfig.Gen3EqSettings,
                         &RpDev->PrivateConfig.Gen4EqSettings,
                         &RpDev->PrivateConfig.Gen5EqSettings,
                         RootPortSpeed,
                         DevInfo.MaxLinkSpeed);
  }
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set "Link Speed Training Policy", Dxx:Fn + D4h[6] to 1.
  /// Make sure this is after mod-PHY related programming is completed.
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC2, B_PCIE_CFG_MPC2_LSTP);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Step 29 If Transmitter Half Swing is enabled, program the following sequence
  /// a. Ensure that the link is in L0.
  /// b. Program the Link Disable bit (0x50[4]) to 1b.
  /// c. Program the Analog PHY Transmitter Voltage Swing bit (0xE8[13]) to set the transmitter swing to half/full swing
  /// d. Program the Link Disable bit (0x50[4]) to 0b.
  /// BIOS can only enable this on SKU where GEN3 capability is disabled on that port
  if (RootPortSpeed < V_PCIE_LCAP_MLS_GEN3 && RootPortCommonConfig->TransmitterHalfSwing) {
    RpDev->PciCfgAccess->Or8 (RpDev->PciCfgAccess, R_PCIE_CFG_LCTL, B_PCIE_LCTL_LD);
    while (PcieSipIsLinkActive (RpDev)) {
      // wait until link becomes inactive before changing swing
    }
    RpDev->PciCfgAccess->Or16 (RpDev->PciCfgAccess, R_PCIE_CFG_PWRCTL, B_PCIE_CFG_PWRCTL_TXSWING);
    RpDev->PciCfgAccess->And8 (RpDev->PciCfgAccess, R_PCIE_CFG_LCTL, (UINT8) ~(B_PCIE_LCTL_LD));
  }
  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set "Poisoned TLP Non-Fatal Advisory Error Enable", Dxx:Fn + D4h[12] to 1
  /// Set "L1 Substate Exit SCI Enable (L1SSESE)", Dxx:Fn + D4[30] to 1
  /// Set "Disable PLL Early Wake on L1 Substate Exit (DISPLLEWL1SE)", Dxx:Fn + D4[16] to 1
  ///
  Data32Or = B_PCIE_CFG_MPC2_PTNFAE | B_PCIE_CFG_MPC2_L1SSESE | B_PCIE_CFG_MPC2_DISPLLEWL1SE;
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC2, Data32Or);

  //
  // L1LOW LTR threshold latency value
  //
  RpDev->PciCfgAccess->AndThenOr32 (
    RpDev->PciCfgAccess,
    R_PCIE_CFG_PCIEPMECTL,
    (UINT32) ~B_PCIE_CFG_PCIEPMECTL_L1LTRTLV,
    (V_PCIE_CFG_PCIEPMECTL_L1LTRTLV << B_PCIE_CFG_PCIEPMECTL_L1LTRTLV_OFFSET)
    );

  ///
  /// Additional configurations
  ///
  ///
  /// Configure Error Reporting policy in the Device Control Register
  ///
  Data16And = (UINT16) (~(B_PCIE_DCTL_URE | B_PCIE_DCTL_FEE | B_PCIE_DCTL_NFE | B_PCIE_DCTL_CEE));
  Data16Or  = 0;

  if (RootPortCommonConfig->UnsupportedRequestReport) {
    Data16Or |= B_PCIE_DCTL_URE;
  }

  if (RootPortCommonConfig->FatalErrorReport) {
    Data16Or |= B_PCIE_DCTL_FEE;
  }

  if (RootPortCommonConfig->NoFatalErrorReport) {
    Data16Or |= B_PCIE_DCTL_NFE;
  }

  if (RootPortCommonConfig->CorrectableErrorReport) {
    Data16Or |= B_PCIE_DCTL_CEE;
  }

  RpDev->PciCfgAccess->AndThenOr16 (RpDev->PciCfgAccess, R_PCIE_CFG_DCTL, Data16And, Data16Or);

  ///
  /// Configure Interrupt / Error reporting in R_PCIE_CFG_RCTL
  ///
  Data16And = (UINT16) (~(B_PCIE_RCTL_SFE | B_PCIE_RCTL_SNE | B_PCIE_RCTL_SCE));
  Data16Or  = 0;

  if (RootPortCommonConfig->SystemErrorOnFatalError) {
    Data16Or |= B_PCIE_RCTL_SFE;
  }

  if (RootPortCommonConfig->SystemErrorOnNonFatalError) {
    Data16Or |= B_PCIE_RCTL_SNE;
  }

  if (RootPortCommonConfig->SystemErrorOnCorrectableError) {
    Data16Or |= B_PCIE_RCTL_SCE;
  }

  RpDev->PciCfgAccess->AndThenOr16 (RpDev->PciCfgAccess, R_PCIE_CFG_RCTL, Data16And, Data16Or);

  ///
  /// Root PCI-E Powermanagement SCI Enable
  ///
  if (RootPortCommonConfig->PmSci) {
    ///
    /// PCH BIOS Spec section 8.7.3 BIOS Enabling of Intel PCH PCI Express* PME SCI Generation
    /// Step 1
    /// Make sure that PME Interrupt Enable bit, Dxx:Fn:Reg 5Ch[3] is cleared
    ///
    RpDev->PciCfgAccess->And16 (RpDev->PciCfgAccess, R_PCIE_CFG_RCTL, (UINT16) (~B_PCIE_RCTL_PIE));

    ///
    /// Step 2
    /// Program Misc Port Config (MPC) register at PCI config space offset
    /// D8h as follows:
    /// Set Power Management SCI Enable bit, Dxx:Fn:Reg D8h[31]
    /// Clear Power Management SMI Enable bit, Dxx:Fn:Reg D8h[0]
    /// Use Byte Access to avoid RWO bit
    ///
    RpDev->PciCfgAccess->And8 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC, (UINT8) (~B_PCIE_CFG_MPC_PMME));
    RpDev->PciCfgAccess->Or8 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC + 3, (UINT8) (B_PCIE_CFG_MPC_PMCE >> 24));
  }
  if (RootPortCommonConfig->HotPlug) {
    ///
    /// PCH BIOS Spec section 8.8.2.1
    /// Step 1
    /// Clear following status bits, by writing 1b to them, in the Slot
    /// Status register at offset 1Ah of PCI Express Capability structure:
    /// Presence Detect Changed (bit3)
    ///
    RpDev->PciCfgAccess->And16 (RpDev->PciCfgAccess, R_PCIE_CFG_SLSTS, B_PCIE_SLSTS_PDC);

    ///
    /// Step 2
    /// Program the following bits in Slot Control register at offset 18h
    /// of PCI Express* Capability structure:
    /// Presence Detect Changed Enable (bit3) = 1b
    /// Hot Plug Interrupt Enable (bit5) = 0b
    ///
    RpDev->PciCfgAccess->AndThenOr16 (RpDev->PciCfgAccess, R_PCIE_CFG_SLCTL, (UINT16) (~B_PCIE_SLCTL_HPE), B_PCIE_SLCTL_PDE);
    ///
    /// Step 3
    /// Program Misc Port Config (MPC) register at PCI config space offset
    /// D8h as follows:
    /// Hot Plug SCI Enable (HPCE, bit30) = 0b
    /// Use byte access to avoid premature locking BIT23, SRL
    ///
    RpDev->PciCfgAccess->And8 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC + 3, (UINT8) ~(B_PCIE_CFG_MPC_HPCE >> 24));
    ///
    /// PCH BIOS Spec section 8.9
    /// BIOS should mask the reporting of Completion timeout (CT) errors by setting
    /// the uncorrectable Error Mask register PCIE RP PCI offset 108h[14].
    ///
    RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_UEM, B_PCIE_EX_UEM_CT);
  }

  ///
  /// PCH BIOS Spec Section 8.10 PCI Bus Emulation & Port80 Decode Support
  /// The I/O cycles within the 80h-8Fh range can be explicitly claimed
  /// by the PCIe RP by setting MPC.P8XDE, PCI offset D8h[26] = 1 (using byte access)
  /// BIOS must also configure the corresponding DMI registers GCS.RPR and GCS.RPRDID
  /// to enable DMI to forward the Port8x cycles to the corresponding PCIe RP
  ///
  if (RpDev->PrivateConfig.EnablePort80Decode) {
    RpDev->PciCfgAccess->Or8 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC + 3, (UINT8) (B_PCIE_CFG_MPC_P8XDE >> 24));
    RpDev->Callbacks.EnablePort80Decode (RpDev);
  }
  //
  // Initialize R/WO Registers that described in PCH BIOS Spec
  //
  ///
  /// SRL bit is write-once and lock, so SRL, UCEL and CCEL must be programmed together
  /// otherwise UCEL/CCEL programming would lock SRL prematurely in wrong state
  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set Common Clock Exit Latency,      Dxx:Fn + D8h[17:15] = 4h
  /// Set Non-common Clock Exit Latency,  Dxx:Fn + D8h[20:18] = 7h
  ///
  Data32And = ~(UINT32) (B_PCIE_CFG_MPC_UCEL | B_PCIE_CFG_MPC_CCEL);
  Data32Or  = (7 << N_PCIE_CFG_MPC_UCEL) | (4 << N_PCIE_CFG_MPC_CCEL);
  if (RpDev->SipVersion != PcieSip17) {
      Data32Or |= B_PCIE_CFG_MPC_SRL;
    RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC, Data32And, Data32Or);
    //
    // Check if SRL bit actually got programmed. If not, then it means some code accessed MPC register earlier and locked it
    //
    ASSERT ( (RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC) & B_PCIE_CFG_MPC_SRL) == B_PCIE_CFG_MPC_SRL);
  } else {
    RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC, Data32And, Data32Or);
  }

  Data32And = (UINT32) ~B_PCIE_CFG_STRPFUSECFG_PXIP;
  Data32Or = (UINT32) (RpDev->PrivateConfig.InterruptPin << N_PCIE_CFG_STRPFUSECFG_PXIP);
  RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_STRPFUSECFG, Data32And, Data32Or);

  //
  // Set Gpio Assertion on Link Down      Dxx:Fn + 0x690h[10] = 1
  //
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_GDR, B_PCIE_CFG_GDR_GPIOALD);

  ///
  /// Dynamic Link Throttling
  ///
  Data32Or = (V_PCIE_CFG_TNPT_TP_3_MS << N_PCIE_CFG_TNPT_TP);
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_TNPT, Data32Or);

  PcieSipConfigureRetimerSupport (RpDev);

  ///
  /// Set Completion Timer Timeout Policy (CRSPSEL) to 0
  ///
  if (RpDev->SipVersion != PcieSip17) {
    RpDev->PciSbiMsgMemAccess->And32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_FCTL, (UINT32) ~BIT12);
  } else {
    RpDev->PciSbiMsgMemAccess->Or32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_FCTL, (UINT32) BIT12);
  }

  ///
  /// Set Error Injection Disable (EINJDIS) to 1
  ///
  RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_EINJCTL, B_PCIE_CFG_EINJCTL_EINJDIS);

  if (RpDev->SipVersion >= PcieSip17) {
    /// Program R_PCIE_RXMC1[C90]
    /// Set C90[26:31] MSRVS to 0x1F
    /// Set C90[20:25] MSRTS to 0x3F
    /// Set C90[13:14] TMSLOP to 0x1
    /// Set C90[11:12] VMSLOP to 0x1
    /// Set C90[9] MSRM to 0x0
    /// Set C90[4:8] MMNOLS to 0x1
    /// Set C90[3] MVS to 0x0
    /// Set C90[2] MILRTS to 0x0
    /// Set C90[2] MIUDVMS to 0x0
    /// Set C90[2] MIESS to 0x0
    ///
    Data32And = (UINT32) ~(B_PCIE_CFG_RXMC1_MMNOLS_MASK | B_PCIE_CFG_RXMC1_VMSLOP_MASK | B_PCIE_CFG_RXMC1_TMSLOP_MASK | B_PCIE_CFG_RXMC1_MSRTS_MASK | B_PCIE_CFG_RXMC1_MSRVS_MASK | B_PCIE_CFG_RXMC1_MSRM | \
                            B_PCIE_CFG_RXMC1_MVS | B_PCIE_CFG_RXMC1_MILRTS | B_PCIE_CFG_RXMC1_MIUDVMS | B_PCIE_CFG_RXMC1_MIESS | B_PCIE_CFG_RXMC1_MILRTS);
    Data32Or = ((V_PCIE_CFG_RXMC1_MMNOLS << B_PCIE_CFG_RXMC1_MMNOLS_OFFSET) | (V_PCIE_CFG_RXMC1_VMSLOP << B_PCIE_CFG_RXMC1_VMSLOP_OFFSET) | (V_PCIE_CFG_RXMC1_TMSLOP << B_PCIE_CFG_RXMC1_TMSLOP_OFFSET) | \
                (V_PCIE_CFG_RXMC1_MSRTS << B_PCIE_CFG_RXMC1_MSRTS_OFFSET) | (V_PCIE_CFG_RXMC1_MSRVS << B_PCIE_CFG_RXMC1_MSRVS_OFFSET));
    RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_RXMC1, Data32And, Data32Or);
    DEBUG((DEBUG_INFO, "RXMC1 : %x\n", RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_RXMC1)));
    ///
    /// Program R_PCIE_RXMC2[C94]
    /// Set C94[19:24] MNOTSS to 0x20
    /// Set C94[18:13] MMTOS to 0x28
    /// Set C94[12:6] MNOVSS to 0x28
    /// Set C94[5:0] MMVOS to 0x0
    ///
    Data32And = (UINT32) ~(B_PCIE_CFG_RXMC2_MNOTSS_MASK | B_PCIE_CFG_RXMC2_MMTOS_MASK | B_PCIE_CFG_RXMC2_MNOVSS_MASK | B_PCIE_CFG_RXMC2_MMVOS_MASK);
    Data32Or = ((V_PCIE_CFG_RXMC2_MNOTSS << B_PCIE_CFG_RXMC2_MNOTSS_OFFSET) | (V_PCIE_CFG_RXMC2_MMTOS << B_PCIE_CFG_RXMC2_MMTOS_OFFSET) | (V_PCIE_CFG_RXMC2_MNOVSS << B_PCIE_CFG_RXMC2_MNOVSS_OFFSET) | \
                (V_PCIE_CFG_RXMC2_MMVOS << B_PCIE_CFG_RXMC2_MMVOS_OFFSET));
    RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_RXMC2, Data32And, Data32Or);
    DEBUG((DEBUG_INFO, "RXMC2 : %x\n", RpDev->PciCfgAccess->Read32 (RpDev->PciCfgAccess, R_PCIE_CFG_RXMC2)));
    ///
    /// Set CE4[0] MARGINDRISW to 0x0
    ///
    RpDev->PciCfgAccess->And32 (RpDev->PciCfgAccess, R_PCIE_CFG_PL16MPCPS, (UINT32) ~(B_PCIE_CFG_PL16MPCPS_MARGINDRISW));

    //
    // Set 0x15A8[13:7] MLSOSRSSVCC to 0x0
    // Set 0x15A8[6:0] MLSOSGSSVCC to 0x0
    //
    Data32And = (UINT32)~(B_PCIE_RCRB_MPHYCAPCFG_MLSOSRSSVCC_MASK | B_PCIE_RCRB_MPHYCAPCFG_MLSOSGSSVCC_MASK);
    RpDev->PciSbiMsgCfgAccess->And32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_RCRB_MPHYCAPCFG, Data32And);

    //
    // Set 0x15A8[13:7] MLSOSRSSVCC to 0x0
    // Set 0x15A8[6:0] MLSOSGSSVCC to 0x0
    //
    Data32And = (UINT32)~(B_PCIE_RCRB_MRBIDBNCD | B_PCIE_RCRB_CRIDBNCD);
    RpDev->PciSbiMsgCfgAccess->And32 (RpDev->PciSbiMsgCfgAccess, R_PCIE_RCRB_TXMDEC1, Data32And);
       PcieSipSetSecureRegisterLock (RpDev);
  }
}

/**
  Configure PCIe power down mapping

  @param[in] RpDev  Pointer to root port device.
**/
VOID
PcieSipConfigurePowerDownMapping (
  IN  PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32                      Data32Or;
  UINT32                      Data32And;

  DEBUG ((DEBUG_INFO, "%a Start\n", __FUNCTION__));
  Data32Or = 0;
  Data32And = 0;
  if (RpDev->Integration != UpDmi) {
    Data32Or = (UINT32)((V_PCIE_RCRB_PIPEPDCTL2_DUCFGPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_DUCFGPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_DUCFGNOPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_DUCFGNOPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1D2PGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_L1D2PGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1D2NOPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_L1D2NOPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1D1PGPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_L1D1PGPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1D1PGNOPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_L1D1PGNOPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1D1UPUPGPDCTL_PCIE << B_PCIE_RCRB_PIPEPDCTL2_L1D1UPUPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1UPNOPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_L1UPNOPGPDCTL_OFFSET));
    Data32And = (UINT32) ~ (B_PCIE_RCRB_PIPEPDCTLEXT_P2TP2TCD | B_PCIE_RCRB_PIPEPDCTLEXT_P2TP2TP);
  } else {
    Data32Or = (UINT32)((V_PCIE_RCRB_PIPEPDCTL2_DUCFGPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_DUCFGPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_DUCFGNOPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_DUCFGNOPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1D2PGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_L1D2PGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1D2NOPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_L1D2NOPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1D1PGPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_L1D1PGPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1D1PGNOPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_L1D1PGNOPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1D1UPUPGPDCTL_DMI << B_PCIE_RCRB_PIPEPDCTL2_L1D1UPUPGPDCTL_OFFSET) |
             (V_PCIE_RCRB_PIPEPDCTL2_L1UPNOPGPDCTL << B_PCIE_RCRB_PIPEPDCTL2_L1UPNOPGPDCTL_OFFSET));
    Data32And = (UINT32) ~ (B_PCIE_RCRB_PIPEPDCTLEXT_P2TP2TCD | B_PCIE_RCRB_PIPEPDCTLEXT_P2TP2TP);
  }
  RpDev->PciSbiMsgMemAccess->Write32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_PIPEPDCTL, 0x44333333);
  RpDev->PciSbiMsgMemAccess->Or32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_PIPEPDCTL2, Data32Or);
  RpDev->PciSbiMsgMemAccess->Or32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_PIPEPDCTL3, 0x44);
  RpDev->PciSbiMsgMemAccess->And32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_PIPEPDCTLEXT, Data32And);
  DEBUG ((DEBUG_INFO, "%a End\n", __FUNCTION__));
}

/**
  Perform reset prep programming

  @param[in] RpDev  Pointer to root port device.
**/
VOID
PcieSipConfigureResetPrep (
  IN  PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a Start\n", __FUNCTION__));
  if (RpDev->Integration != UpDmi) {
    RpDev->PciSbiMsgMemAccess->Write32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_FCTL2, 0xD0);
    RpDev->PciSbiMsgMemAccess->Write32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_RPR, 0x555554);
    RpDev->PciSbiMsgMemAccess->Write32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_RPDEC1, 0x30002);
    RpDev->PciSbiMsgMemAccess->Write32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_RPDEC2, 0x40003);
    RpDev->PciSbiMsgMemAccess->Write32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_RPDEC3, 0x50005);
    RpDev->PciSbiMsgMemAccess->Or32    (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_TXCRSTOCTL, B_PCIE_RCRB_TXCRSTOCTL_TXNPCTODIS);
  } else {
    RpDev->PciSbiMsgMemAccess->Write32 (
      RpDev->PciSbiMsgMemAccess,
      R_PCIE_RCRB_RPR,
      (UINT32) ((V_PCIE_RCRB_RPR_S5SMTO << B_PCIE_RCRB_RPR_S5SMTO_OFFSET) |
               (V_PCIE_RCRB_RPR_S4SMTO << B_PCIE_RCRB_RPR_S4SMTO_OFFSET) |
               (V_PCIE_RCRB_RPR_S3SMTO << B_PCIE_RCRB_RPR_S3SMTO_OFFSET) |
               (V_PCIE_RCRB_RPR_CRMTO << B_PCIE_RCRB_RPR_CRMTO_OFFSET) |
               (V_PCIE_RCRB_RPR_WRMTO << B_PCIE_RCRB_RPR_WRMTO_OFFSET) |
               (V_PCIE_RCRB_RPR_DMTO << B_PCIE_RCRB_RPR_DMTO_OFFSET) |
               (V_PCIE_RCRB_RPR_S5SM << B_PCIE_RCRB_RPR_S5SM_OFFSET) |
               (V_PCIE_RCRB_RPR_S4SM << B_PCIE_RCRB_RPR_S4SM_OFFSET) |
               (V_PCIE_RCRB_RPR_S3SM << B_PCIE_RCRB_RPR_S3SM_OFFSET) |
               (V_PCIE_RCRB_RPR_CRM << B_PCIE_RCRB_RPR_CRM_OFFSET) |
               (V_PCIE_RCRB_RPR_WRM << B_PCIE_RCRB_RPR_WRM_OFFSET))
    );
    RpDev->PciSbiMsgMemAccess->Write32 (
      RpDev->PciSbiMsgMemAccess,
      R_PCIE_RCRB_RPDEC1,
      (UINT32) ((V_PCIE_RCRB_RPDEC1_RPCREPT << B_PCIE_RCRB_RPDEC1_RPCREPT_OFFSET) |
                (V_PCIE_RCRB_RPDEC1_RPCRERT << B_PCIE_RCRB_RPDEC1_RPCRERT_OFFSET) |
                (V_PCIE_RCRB_RPDEC1_RPWREPT << B_PCIE_RCRB_RPDEC1_RPWREPT_OFFSET) |
                (V_PCIE_RCRB_RPDEC1_RPWRERT << B_PCIE_RCRB_RPDEC1_RPWRERT_OFFSET))
      );
    RpDev->PciSbiMsgMemAccess->Write32 (
      RpDev->PciSbiMsgMemAccess,
      R_PCIE_RCRB_RPDEC2,
      (UINT32) ((V_PCIE_RCRB_RPDEC2_RPS4EPT << B_PCIE_RCRB_RPDEC2_RPS4EPT_OFFSET) |
                (V_PCIE_RCRB_RPDEC2_RPS4ERT << B_PCIE_RCRB_RPDEC2_RPS4ERT_OFFSET) |
                (V_PCIE_RCRB_RPDEC2_RPS3EPT << B_PCIE_RCRB_RPDEC2_RPS3EPT_OFFSET) |
                (V_PCIE_RCRB_RPDEC2_RPS3ERT << B_PCIE_RCRB_RPDEC2_RPS3ERT_OFFSET))
      );
    RpDev->PciSbiMsgMemAccess->Write32 (
      RpDev->PciSbiMsgMemAccess,
      R_PCIE_RCRB_RPDEC3,
      (UINT32) ((V_PCIE_RCRB_RPDEC3_RPDH << B_PCIE_RCRB_RPDEC3_RPDH_OFFSET) |
                (V_PCIE_RCRB_RPDEC3_RPS5EPT << B_PCIE_RCRB_RPDEC3_RPS5EPT_OFFSET) |
                (V_PCIE_RCRB_RPDEC3_RPS5ERT << B_PCIE_RCRB_RPDEC3_RPS5ERT_OFFSET))
      );
    RpDev->PciSbiMsgMemAccess->Write32 (
      RpDev->PciSbiMsgMemAccess,
      R_PCIE_RCRB_FCTL2,
      (UINT32) ((V_PCIE_RCRB_FCTL2_HRTCTL << B_PCIE_RCRB_FCTL2_HRTCTL_OFFSET) |
                (V_PCIE_RCRB_FCTL2_RPTOT << B_PCIE_RCRB_FCTL2_RPTOT_OFFSET))
      );
  }
  DEBUG ((DEBUG_INFO, "%a End\n", __FUNCTION__));
}

/**
  Program Rx primary Cycle Decode Registers for SIP17

  @param[in] RpDev  Pointer to root port device.
**/
VOID
PcieSip17RxMasterCycleDecode (
  IN  PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32                      Data32Or;
  UINT32                      Data32And;

  DEBUG ((DEBUG_INFO, "%a for Rp %x\n", __FUNCTION__, RpDev->Id));

  ///
  /// Program (R_PCIE_RCRB_CFG_DECCTL) 0X1904
  /// Program (ATSCE) 0X1904[1]                0x1
  /// Program (BUSNUMZCHK) 0X1904[2]           0x1
  /// Program (MTRXLTC) 0X1904[3]              0x0
  /// Program (OUTBEXPCPLCHKEN) 0X1904[4]      0x1
  /// Program (DROPCPLATNZCE) 0X1904[5]        0x1
  /// Program (LCRXINT) 0X1904[6]              0x1
  /// Program (VDMATAC) 0X1904[7]              0x1
  /// Program (URVDME16DW) 0X1904[8]           0x1
  /// Program (URRXUVDMINTELVID) 0X1904[9]     0x0
  /// Program (URRXUVDMUVID) 0X1904[10]        0x0
  /// Program (URRXURTRCVDM) 0X1904[11]        0x1
  /// Program (URRXULTVDM) 0X1904[12]          0x1
  /// Program (URRXURAVDM) 0X1904[13]          0x1
  /// Program (URRXURIDVDM) 0X1904[14]         0x1
  /// Program (URRXUORVDM) 0X1904[15]          0x1
  /// Program (URRXVMCTPBFRC) 0X1904[16]       0x1
  /// Program (ICHKINVREQRMSGRBID) 0X1904[17]  0x1
  /// Program (RXMCTPDECEN) 0X1904[18]         0x1
  /// Program (RXVDMDECE) 0X1904[19]           0x0
  /// Program (RXGPEDECEN) 0X1904[20]          0x1
  /// Program (RXSBEMCAPDECEN) 0X1904[21]      0x1 for SIP17
  /// Program (RXSBEMCAPDECEN) 0X1904[21]      0x0 for SipVersion > SIP17
  /// Program (RXADLEDDECEN) 0X1904[22]        0x1
  /// Program (RXLTRMDECH) 0X1904[23]          0x0
  /// Program (LCRXERRMSG) 0X1904[24]          0x1
  /// Program (RSVD_RW) 0X1904[25]             0x1
  /// Program (LCRXPTMREQ) 0X1904[26]          0x1
  /// Program (URRXUVDMRBFRC) 0X1904[27]       0x1
  /// Program (URRXUVDMRGRTRC) 0X1904[28]      0x1
  /// Program (RXMCTPBRCDECEN) 0X1904[29]      0x1
  /// Program (URRXMCTPNTCO) 0x1904[30]        0x1
  /// Program (RXIMDECEN) 0X1904[31]           0x0
  ///
  Data32And = (UINT32) ~(B_PCIE_RCRB_CFG_DECCTL_MTRXLTC | B_PCIE_RCRB_CFG_DECCTL_RXSBEMCAPDECEN | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMINTELVID | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMUVID | B_PCIE_RCRB_CFG_DECCTL_RXVDMDECE |
                         B_PCIE_RCRB_CFG_DECCTL_RXIMDECEN | B_PCIE_RCRB_CFG_DECCTL_RXLTRMDECH);
  Data32Or = (UINT32) (B_PCIE_RCRB_CFG_DECCTL_ATSCE | B_PCIE_RCRB_CFG_DECCTL_BUSNUMZCHK | B_PCIE_RCRB_CFG_DECCTL_OUTBEXPCPLCHKEN | B_PCIE_RCRB_CFG_DECCTL_DROPCPLATNZCE |
                       B_PCIE_RCRB_CFG_DECCTL_LCRXINT | B_PCIE_RCRB_CFG_DECCTL_VDMATAC | B_PCIE_RCRB_CFG_DECCTL_URVDME16DW | B_PCIE_RCRB_CFG_DECCTL_URRXURTRCVDM |
                       B_PCIE_RCRB_CFG_DECCTL_URRXULTVDM | B_PCIE_RCRB_CFG_DECCTL_URRXURAVDM | B_PCIE_RCRB_CFG_DECCTL_URRXURIDVDM | B_PCIE_RCRB_CFG_DECCTL_URRXUORVDM |
                       B_PCIE_RCRB_CFG_DECCTL_URRXVMCTPBFRC | B_PCIE_RCRB_CFG_DECCTL_ICHKINVREQRMSGRBID | B_PCIE_RCRB_CFG_DECCTL_RXMCTPDECEN | B_PCIE_RCRB_CFG_DECCTL_RXGPEDECEN |
                       B_PCIE_RCRB_CFG_DECCTL_RXADLEDDECEN | B_PCIE_RCRB_CFG_DECCTL_LCRXERRMSG | B_PCIE_RCRB_CFG_DECCTL_RSVD_RW |
                       B_PCIE_RCRB_CFG_DECCTL_LCRXPTMREQ | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMRBFRC | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMRGRTRC | B_PCIE_RCRB_CFG_DECCTL_RXMCTPBRCDECEN |
                       B_PCIE_RCRB_CFG_DECCTL_URRXMCTPNTCO);
  if (RpDev->SipVersion == PcieSip17) {
    Data32Or |= B_PCIE_RCRB_CFG_DECCTL_RXSBEMCAPDECEN;
  }
  RpDev->PciSbiMsgMemAccess->AndThenOr32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_CFG_DECCTL, Data32And, Data32Or);

  ///
  /// Program (R_PCIE_RCRB_CFG_PIDECCTL) 0X190C
  /// Program (CPLBNCHK) 0X190C[0]  0x1
  ///
  RpDev->PciSbiMsgMemAccess->Or32 (RpDev->PciSbiMsgMemAccess, R_PCIE_RCRB_CFG_PIDECCTL, (UINT32)(B_PCIE_RCRB_CFG_PIDECCTL_CPLBNCHK));
}

/**
  Perform some early Pcie Root port configuration to enable RCRB and program decoder control registers

  @param[in] RpList  Pointer to the root port list.
**/
VOID
PcieSipEarlyDecodeEnable (
  IN PCIE_ROOT_PORT_LIST  *RpList
 )
{
  PCIE_ROOT_PORT_DEV  *RpDev;
  EFI_STATUS          Status;

  DEBUG ((DEBUG_INFO, "%a Start.\n", __FUNCTION__));
  for (Status = RpList->ResetToFirst (RpList, &RpDev); Status == EFI_SUCCESS; Status = RpList->GetNextRootPort (RpList, &RpDev)) {
    if (RpDev->SipVersion >= PcieSip17) {
      //
      // Set Completion Ordering Mode
      //
      RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_DC, B_PCIE_CFG_DC_COM);
      //
      // RX primary Cycle Decoding Programming
      //
      PcieSip17RxMasterCycleDecode (RpDev);
    }
  }
  DEBUG ((DEBUG_INFO, "%a End.\n", __FUNCTION__));
}

/**
  PCIe root port programming that can be executed prior to link active.

  @param[in] ControllerDev  Pointer to the controller dev.
  @param[in] RpDev          Pointer to root port device.
**/
VOID
PcieSipPreLinkActiveProgramming (
  IN PCIE_ROOT_PORT_DEV  *ControllerDev,
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32  DetectTimeoutUs;
  UINT32  DetectTimer;
  UINT32   RpIndex;

  RpIndex = PcieSipGetPortNum (RpDev) - 1;
  RpDev->Status.RootPortDisable = FALSE;
  RpDev->Status.ClkReqEnable = RpDev->PrivateConfig.ClkReqAssigned;

  if (RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, 0) == 0xFFFF ||
      PcieSipIsRpOwningLanes (ControllerDev, RpDev) == FALSE) {
    RpDev->Status.RootPortDisable = TRUE;
  }

  if (RpDev->SipVersion >= PcieSip17) {
    RpDev->PciCfgAccess->And32 (RpDev->PciCfgAccess, PCI_BASE_ADDRESSREG_OFFSET, (UINT32)~(BIT1 | BIT2));
  }

  if (!RpDev->Status.RootPortDisable) {
    ///
    /// Set the Slot Implemented Bit.
    /// PCH BIOS Spec section 8.2.3, The System BIOS must
    /// initialize the "Slot Implemented" bit of the PCI Express* Capabilities Register,
    /// XCAP Dxx:Fn:42h[8] of each available and enabled downstream root port.
    /// Ports with hotplug capability must have SI bit set
    /// The register is write-once so must be written even if we're not going to set SI, in order to lock it.
    ///
    /// This must happen before code reads PresenceDetectState, because PDS is invalid unless SI is set
    ///
    if (RpDev->PcieRootPortConfig->PcieRpCommonConfig.SlotImplemented || RpDev->PcieRootPortConfig->PcieRpCommonConfig.HotPlug) {
      RpDev->PciCfgAccess->Or16 (RpDev->PciCfgAccess, R_PCIE_CFG_XCAP, B_PCIE_XCAP_SI);
    } else {
      RpDev->PciCfgAccess->And16 (RpDev->PciCfgAccess, R_PCIE_CFG_XCAP, (UINT16)(~B_PCIE_XCAP_SI));
    }

    DetectTimeoutUs = RpDev->PcieRootPortConfig->PcieRpCommonConfig.DetectTimeoutMs * 1000;
    if (PcieSipIsDevicePresent (RpDev, DetectTimeoutUs, &DetectTimer)) {
      DEBUG ((DEBUG_INFO, "Port %d has a device attached.\n", RpDev->Id));
      //
      // At this point in boot, CLKREQ pad is still configured as GP input and doesnt' block clock generation
      // regardless of input state. Before switching it to native mode when it will start gating clock, we
      // verify if CLKREQ is really connected. If not, pad will not switch and power management
      // will be disabled in rootport.
      // By the time this code runs device can't have CPM or L1 substates enabled, so it is guaranteed to pull ClkReq down.
      // If ClkReq is detected to be high anyway, it means ClkReq is not connected correctly.
      // Checking pad's input value is primarily a measure to prevent problems with long cards inserted into short
      // open-ended PCIe slots on motherboards which route PRSNT signal to CLKREQ. Such config causes CLKREQ signal to float.
      //
      if (RpDev->PcieRootPortConfig->PcieRpCommonConfig.ClkReqDetect &&
          RpDev->PrivateConfig.ClkReqAssigned &&
          !RpDev->Callbacks.IsClkReqPulledDown (RpDev)) {
        RpDev->Status.ClkReqEnable = FALSE;
      }
    } else {
      if (RpDev->PcieRootPortConfig->PcieRpCommonConfig.HotPlug == FALSE) {
        RpDev->Status.RootPortDisable = TRUE;
      }
    }

    if (PcieSipGetMaxLinkSpeed (RpDev) >= 3) {
      if (RpDev->PcieConfig->RootPort[RpIndex].PcieRpCommonConfig.OverrideEqualizationDefaults) {
       CopyMem (&RpDev->PrivateConfig.Gen3EqSettings.PlatformSettings, &RpDev->PcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings, sizeof (PCIE_LINK_EQ_PLATFORM_SETTINGS));
        CopyMem (&RpDev->PrivateConfig.Gen4EqSettings.PlatformSettings, &RpDev->PcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen4LinkEqPlatformSettings, sizeof (PCIE_LINK_EQ_PLATFORM_SETTINGS));
      }
      PcieSipLinkEqualizeInit (RpDev, &RpDev->PrivateConfig.Gen3EqSettings, &RpDev->PrivateConfig.Gen4EqSettings, &RpDev->PrivateConfig.Gen5EqSettings);
    }
    if (RpDev->IsController && RpDev->SipVersion >= PcieSip17) {
      //
      // Perform Power down mapping before enabling powermanagement
      //
      PcieSipConfigurePowerDownMapping (RpDev);
      //
      // ResetPrep Programming
      //
      PcieSipConfigureResetPrep (RpDev);
    }
  }

  ///
  /// Configure power management applicable to all port including disabled ports.
  ///
  if (RpDev->Status.ClkReqEnable) {
    RpDev->Callbacks.EnableClkReq (RpDev);
  }

  //
  // PhyLanePgEnable depends on the port supporting CLKREQ# or being disabled.
  //
  PcieSipConfigurePortBasePowerManagement (
    RpDev,
    (RpDev->Status.ClkReqEnable || RpDev->Status.RootPortDisable)
    );
}

/**
  Validate root port list.

  @param[in] RpList  Pointer to the root port list.

  @retval TRUE   Root port list valid.
  @retval FALSE  Root port list is invalid.
**/
BOOLEAN
PcieSipRpListValidate (
  IN PCIE_ROOT_PORT_LIST  *RpList
  )
{
  if (RpList == NULL ||
      RpList->GetNextRootPort == NULL ||
      RpList->GetNextController == NULL ||
      RpList->GetNextRootPortInThisController == NULL ||
      RpList->GetThisController == NULL ||
      RpList->GetRootPortOnNextDevice == NULL ||
      RpList->GetRootPortOnNextFunction == NULL ||
      RpList->ResetToFirst == NULL) {
    return FALSE;
  }

  return TRUE;
}

/**
  Prints PCIe root port configuration.

  @param[in] RpDev  Pointer to root port device.
**/
VOID
PcieSipRpPrintPrivateConfig (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32  Index;

  DEBUG ((DEBUG_INFO, "Private configuration of root port %d\n", RpDev->Id));
  DEBUG ((DEBUG_INFO, "IsController: %d\n", RpDev->IsController));
  DEBUG ((DEBUG_INFO, "RootPortIndexInController: %d\n", RpDev->PrivateConfig.RootPortIndexInController));
  DEBUG ((DEBUG_INFO, "ClkReqAssigned: %d\n", RpDev->PrivateConfig.ClkReqAssigned));
  DEBUG ((DEBUG_INFO, "VnnRemovalSupported: %d\n", RpDev->PrivateConfig.VnnRemovalSupported));
  DEBUG ((DEBUG_INFO, "VtdEnabled: %d\n", RpDev->PrivateConfig.VtdEnabled));
  DEBUG ((DEBUG_INFO, "BusMin: %d\n", RpDev->PrivateConfig.BusMin));
  DEBUG ((DEBUG_INFO, "BusMax: %d\n", RpDev->PrivateConfig.BusMax));
  DEBUG ((DEBUG_INFO, "ImrEnabled: %d\n", RpDev->PrivateConfig.ImrEnabled));
  DEBUG ((DEBUG_INFO, "PciImrBase: %X\n", RpDev->PrivateConfig.PciImrBase));
  DEBUG ((DEBUG_INFO, "PciImrSize: %X\n", RpDev->PrivateConfig.PciImrSize));
  DEBUG ((DEBUG_INFO, "Rs3Bus: %d\n", RpDev->PrivateConfig.Rs3Bus));
  DEBUG ((DEBUG_INFO, "InterruptPin: %d\n", RpDev->PrivateConfig.InterruptPin));
  DEBUG ((DEBUG_INFO, "EnablePort80Decode: %d\n", RpDev->PrivateConfig.EnablePort80Decode));
  DEBUG ((DEBUG_INFO, "PTM config:\n"));
  for (Index = 0; Index < MAX_PTM_STAGE_DELAY_CONFIG_REGS; Index++) {
    DEBUG ((DEBUG_INFO, "PTM pipe stage delay reg %d: %X\n", Index, RpDev->PrivateConfig.PtmConfig.PtmPipeStageDelay[Index]));
  }
  DEBUG ((DEBUG_INFO, "PTM config: %X\n", RpDev->PrivateConfig.PtmConfig.PtmConfig));
  DEBUG ((DEBUG_INFO, "LTR subtraction config\n"));
  DEBUG ((DEBUG_INFO, "L1 standard\n"));
  DEBUG ((DEBUG_INFO, "NoSnoopEnable: %d\n", RpDev->PrivateConfig.L1StandardConfig.NoSnoopEnable));
  DEBUG ((DEBUG_INFO, "NoSnoopScale: %X\n", RpDev->PrivateConfig.L1StandardConfig.NoSnoopScale));
  DEBUG ((DEBUG_INFO, "NoSnoopValue: %X\n", RpDev->PrivateConfig.L1StandardConfig.NoSnoopValue));
  DEBUG ((DEBUG_INFO, "SnoopEnable: %d\n", RpDev->PrivateConfig.L1StandardConfig.SnoopEnable));
  DEBUG ((DEBUG_INFO, "SnoopScale: %X\n", RpDev->PrivateConfig.L1StandardConfig.SnoopScale));
  DEBUG ((DEBUG_INFO, "SnoopValue: %X\n", RpDev->PrivateConfig.L1StandardConfig.SnoopValue));
  DEBUG ((DEBUG_INFO, "L1.1 config\n"));
  DEBUG ((DEBUG_INFO, "NoSnoopEnable: %d\n", RpDev->PrivateConfig.L1p1Config.NoSnoopEnable));
  DEBUG ((DEBUG_INFO, "NoSnoopScale: %X\n", RpDev->PrivateConfig.L1p1Config.NoSnoopScale));
  DEBUG ((DEBUG_INFO, "NoSnoopValue: %X\n", RpDev->PrivateConfig.L1p1Config.NoSnoopValue));
  DEBUG ((DEBUG_INFO, "SnoopEnable: %d\n", RpDev->PrivateConfig.L1p1Config.SnoopEnable));
  DEBUG ((DEBUG_INFO, "SnoopScale: %X\n", RpDev->PrivateConfig.L1p1Config.SnoopScale));
  DEBUG ((DEBUG_INFO, "SnoopValue: %X\n", RpDev->PrivateConfig.L1p1Config.SnoopValue));
  DEBUG ((DEBUG_INFO, "L1.2 config\n"));
  DEBUG ((DEBUG_INFO, "NoSnoopEnable: %d\n", RpDev->PrivateConfig.L1p2Config.NoSnoopEnable));
  DEBUG ((DEBUG_INFO, "NoSnoopScale: %X\n", RpDev->PrivateConfig.L1p2Config.NoSnoopScale));
  DEBUG ((DEBUG_INFO, "NoSnoopValue: %X\n", RpDev->PrivateConfig.L1p2Config.NoSnoopValue));
  DEBUG ((DEBUG_INFO, "SnoopEnable: %d\n", RpDev->PrivateConfig.L1p2Config.SnoopEnable));
  DEBUG ((DEBUG_INFO, "SnoopScale: %X\n", RpDev->PrivateConfig.L1p2Config.SnoopScale));
  DEBUG ((DEBUG_INFO, "SnoopValue: %X\n", RpDev->PrivateConfig.L1p2Config.SnoopValue));
  DEBUG ((DEBUG_INFO, "L1.1 NPG config\n"));
  DEBUG ((DEBUG_INFO, "NoSnoopEnable: %d\n", RpDev->PrivateConfig.LtrSubL11Npg.NoSnoopEnable));
  DEBUG ((DEBUG_INFO, "NoSnoopScale: %X\n", RpDev->PrivateConfig.LtrSubL11Npg.NoSnoopScale));
  DEBUG ((DEBUG_INFO, "NoSnoopValue: %X\n", RpDev->PrivateConfig.LtrSubL11Npg.NoSnoopValue));
  DEBUG ((DEBUG_INFO, "SnoopEnable: %d\n", RpDev->PrivateConfig.LtrSubL11Npg.SnoopEnable));
  DEBUG ((DEBUG_INFO, "SnoopScale: %X\n", RpDev->PrivateConfig.LtrSubL11Npg.SnoopScale));
  DEBUG ((DEBUG_INFO, "SnoopValue: %X\n", RpDev->PrivateConfig.LtrSubL11Npg.SnoopValue));
  //
  // EQ configuration is printed when EQ is being initialized.
  //
}

/**
  Validate root port device.

  @param[in] RpDev  Pointer to root port device.

  @retval TRUE   Root port is valid.
  @retval FALSE  Root port is not valid.
**/
BOOLEAN
PcieSipRpValidate (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  if (RpDev == NULL ||
      RpDev->PcieConfig == NULL ||
      RpDev->PcieRootPortConfig == NULL ||
      RpDev->PciCfgAccess == NULL ||
      RpDev->PciSbiMsgCfgAccess == NULL ||
      RpDev->PciSbiMsgMemAccess == NULL ||
      RpDev->PciPcrAccess == NULL ||
      RpDev->Callbacks.GetPciAccess == NULL ||
      RpDev->Callbacks.EnablePciImr == NULL ||
      RpDev->Callbacks.DisableRootPort == NULL ||
      RpDev->Callbacks.IsPcieLaneConnected == NULL ||
      RpDev->Callbacks.EnablePort80Decode == NULL ||
      RpDev->Callbacks.IsClkReqPulledDown == NULL ||
      RpDev->Callbacks.EnableClkReq == NULL ||
      RpDev->Callbacks.InitNccPort == NULL ||
      RpDev->Callbacks.SwapFunctionNumber == NULL ||
      RpDev->Callbacks.ForceEnablePciConfig == NULL ||
      RpDev->Callbacks.DisablePciConfig == NULL) {
    return FALSE;
  }

  return TRUE;
}

/**
  Initialize PCIe root ports.

  @param[in] RpList  Pointer to the root port list.
**/
VOID
PcieSipRpInit (
  IN PCIE_ROOT_PORT_LIST  *RpList
  )
{
  UINT32                      LaTimer;
  PCIE_ROOT_PORT_DEV          *RpDev;
  PCIE_ROOT_PORT_DEV          *ControllerDev;
  UINT32                      Data32And;
  UINT32                      Data32Or;
  BOOLEAN                     AllRpInControllerDisabled;
  BOOLEAN                     AllRpInControllerSupportClkReq;
  EFI_STATUS                  Status;

  DEBUG ((DEBUG_INFO, "%a Start\n", __FUNCTION__));

  if (!PcieSipRpListValidate (RpList)) {
    DEBUG ((DEBUG_ERROR, "Root port list is invalid\n"));
    return;
  }

  for (Status = RpList->ResetToFirst (RpList, &ControllerDev); Status == EFI_SUCCESS; Status = RpList->GetNextController (RpList, &ControllerDev)) {
    AllRpInControllerDisabled = TRUE;
    AllRpInControllerSupportClkReq = TRUE;
    for (RpDev = ControllerDev; Status == EFI_SUCCESS; Status = RpList->GetNextRootPortInThisController (RpList, &RpDev)) {
      if (!PcieSipRpValidate (RpDev)) {
        DEBUG ((DEBUG_ERROR, "Root port %d invalid\n", RpDev->Id));
        continue;
      }
      DEBUG_CODE_BEGIN ();
      PcieSipRpPrintPrivateConfig (RpDev);
      PcieSipPolicySanityCheck (RpDev);
      DEBUG_CODE_END ();

      RpList->GetThisController (RpList, &ControllerDev);
      PcieSipPreLinkActiveProgramming (ControllerDev, RpDev);
      if (!RpDev->Status.RootPortDisable && RpDev->PrivateConfig.ImrEnabled) {
        PcieSipEnablePcieImr (ControllerDev, RpDev);
      }
      AllRpInControllerDisabled &= RpDev->Status.RootPortDisable;
      AllRpInControllerSupportClkReq &= RpDev->Status.ClkReqEnable;
    }

    //
    // TrunkClockGateEn depends on each of the controller ports supporting CLKREQ# or being disabled.
    //
    PcieSipConfigureControllerBasePowerManagement (
      ControllerDev,
      AllRpInControllerDisabled || AllRpInControllerSupportClkReq
      );
    //
    // For each controller set Initialize Transaction Layer Receiver Control on Link Down
    // and Initialize Link Layer Receiver Control on Link Down.
    // Use sideband access in case 1st port of a controller is disabled
    //
    // This is a survivability setting to recover system from crash on surprise removal
    //
    if (ControllerDev->SipVersion >= PcieSip17) {
      Data32And = (UINT32)~(B_PCIE_CFG_PCIEALC_RTD3PDSP_SIP17);
      Data32Or  = (UINT32)(B_PCIE_CFG_PCIEALC_RTD3PDSP);
    } else {
      Data32And = (UINT32)~0u;
      Data32Or  = (UINT32) (B_PCIE_CFG_PCIEALC_ITLRCLD | B_PCIE_CFG_PCIEALC_ILLRCLD | B_PCIE_CFG_PCIEALC_RTD3PDSP);
    }
    ControllerDev->PciSbiMsgCfgAccess->AndThenOr32 (
      ControllerDev->PciSbiMsgCfgAccess,
      R_PCIE_CFG_PCIEALC,
      Data32And,
      Data32Or
      );
    ControllerDev->PciSbiMsgCfgAccess->Or32 (
      ControllerDev->PciSbiMsgCfgAccess,
      R_PCIE_CFG_DC,
      B_PCIE_CFG_DC_COM
      );

    ControllerDev->PciSbiMsgMemAccess->And32 (
      ControllerDev->PciSbiMsgMemAccess,
      R_PCIE_RCRB_FCUCTL,
      (UINT32) ~B_PCIE_RCRB_FCUCTL_FC_CP_FCM);
    PcieSipConfigurePtm (ControllerDev, &ControllerDev->PrivateConfig.PtmConfig);
    if (ControllerDev->SipVersion >= PcieSip17) {
      PcieSipConfigureLtrSubstraction (
        ControllerDev,
        &ControllerDev->PrivateConfig.L1StandardConfig,
        &ControllerDev->PrivateConfig.L1p1Config,
        &ControllerDev->PrivateConfig.L1p2Config,
        &ControllerDev->PrivateConfig.LtrSubL11Npg
        );
    }
  }

  for (Status = RpList->ResetToFirst (RpList, &RpDev); Status == EFI_SUCCESS; Status = RpList->GetNextRootPort (RpList, &RpDev)) {
    //
    // Wait for root port to form a link.
    // Having an active link is necessary to access and configure the endpoint
    // We cannot use results of IsPcieDevicePresent() because it checks PDS only and may include
    // PCIe cards that never form a link, such as compliance load boards.
    //
    LaTimer = 0;
    while (LaTimer < LINK_ACTIVE_POLL_TIMEOUT) {
      //
      // if PDS is not set or if LA is set then this rootport is done - clear it from mask
      //
      if (RpDev->Status.RootPortDisable || !PcieSipIsEndpointConnected (RpDev) || PcieSipIsLinkActive (RpDev)) {
        break;
      }
      MicroSecondDelay (LINK_ACTIVE_POLL_INTERVAL);
      LaTimer += LINK_ACTIVE_POLL_INTERVAL;
    }

    if (RpDev->Status.RootPortDisable) {
      PcieSipDisableRootPort (RpList, RpDev);
    } else {
      PcieSipInitRootPort (RpDev);
      //
      // Initialize standard PCIe features such as MPS, ASPM, etc.
      //
      RootportDownstreamConfiguration (
        (UINT8)RpDev->Sbdf.Segment,
        (UINT8)RpDev->Sbdf.Bus,
        (UINT8)RpDev->Sbdf.Device,
        (UINT8)RpDev->Sbdf.Function,
        RpDev->PrivateConfig.BusMin,
        RpDev->PrivateConfig.BusMax,
        EnumPchPcie
        );
    }
  }

  PcieSipRpSpeedChange (RpList);
  DEBUG ((DEBUG_INFO, "%a End\n", __FUNCTION__));
}

/**
  This function determines whether root port is configured in non-common clock mode.
  Result is based on the NCC soft-strap setting.

  @param[in] RpBase      Root Port pci segment base address

  @retval TRUE           Port in NCC SSC mode.
  @retval FALSE          Port not in NCC SSC mode.
**/
BOOLEAN
PcieSipIsNcc (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  if (RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, R_PCIE_CFG_LSTS) & B_PCIE_LSTS_SCC) {
    return FALSE;
  } else {
    return TRUE;
  }
}

/**
  Initialize non-common clock port for SIP 16 and SIP 17 controllers

  @param[in]  RpDev       Pointer to root port device.
**/
VOID
PcieSipInitNccPort (
  IN PCIE_ROOT_PORT_DEV  *RpDev
  )
{
  UINT32     Data32Or;
  UINT32     Data32And;

  DEBUG ((DEBUG_INFO, "%a(%d)\n", __FUNCTION__, RpDev->Id));

  if (RpDev->SipVersion >= PcieSip17) {
    RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_LCAP, (UINT32) ~B_PCIE_LCAP_APMS, B_PCIE_LCAP_APMS_L1);
    Data32Or = B_PCIE_CFG_MPC2_ASPMCOEN | V_PCIE_CFG_MPC2_ASPMCO_L1;
    RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC2, Data32Or);
    RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_MPC, B_PCIE_CFG_MPC_UCEL);
    RpDev->PciCfgAccess->Or32 (RpDev->PciCfgAccess, R_PCIE_CFG_LCAP, B_PCIE_LCAP_EL1);
    Data32And = (UINT32) ~(B_PCIE_LCAP2_LSOSRSS | B_PCIE_LCAP2_LSOSGSSV);
    RpDev->PciCfgAccess->AndThenOr32 (RpDev->PciCfgAccess, R_PCIE_CFG_LCAP2, Data32And, 0xF << N_PCIE_LCAP2_LSOSGSSV);
  }
  RpDev->Callbacks.InitNccPort (RpDev);
  RpDev->PciCfgAccess->And32 (RpDev->PciCfgAccess, R_PCIE_CFG_PCIEALC, (UINT32) ~B_PCIE_CFG_PCIEALC_BLKDQDA);
}

/**
  Initializes ports with NonCommonClock and SSC configuration.
**/
VOID
PcieSipInitNccRootPorts (
  IN PCIE_ROOT_PORT_LIST  *RpList
  )
{
  PCIE_ROOT_PORT_DEV          *RpDev;
  EFI_STATUS                  Status;

  for (Status = RpList->ResetToFirst (RpList, &RpDev); Status == EFI_SUCCESS; Status = RpList->GetNextRootPort (RpList, &RpDev)) {
    if (PcieSipIsNcc (RpDev)) {
      PcieSipInitNccPort (RpDev);
    }
  }
}

/**
  Configure root port function number mapping.

  @param[in] RpList  Pointer to the root port list.
**/
VOID
PcieSipConfigureRpfnMapping (
  IN PCIE_ROOT_PORT_LIST  *RpList
  )
{
  PCIE_ROOT_PORT_DEV  *Func0RpDev;
  PCIE_ROOT_PORT_DEV  *Func0ControllerDev;
  PCIE_ROOT_PORT_DEV  *FirstEnabledFunctionRpDev;
  PCIE_ROOT_PORT_DEV  *FirstEnabledFunctionControllerDev;
  PCIE_ROOT_PORT_DEV  *RpDev;
  BOOLEAN             FunctionEnabled;
  UINT16              OldFunction;
  EFI_STATUS          Status;

  DEBUG ((DEBUG_INFO,"%a () Start\n", __FUNCTION__));

  if (!PcieSipRpListValidate (RpList)) {
    DEBUG ((DEBUG_ERROR, "Invalid root port list\n"));
    return;
  }

  for (Status = RpList->ResetToFirst (RpList, &RpDev); Status == EFI_SUCCESS; Status = RpList->GetRootPortOnNextDevice (RpList, &RpDev)) {
    Func0RpDev = RpDev;
    Status = RpList->GetThisController (RpList, &Func0ControllerDev);
    if (EFI_ERROR (Status)) {
      continue;
    }
    FunctionEnabled = FALSE;

    for (; Status == EFI_SUCCESS; Status = RpList->GetRootPortOnNextFunction (RpList, &RpDev)) {
      if (RpDev->PciCfgAccess->Read16 (RpDev->PciCfgAccess, 0) != 0xFFFF) {
        FunctionEnabled = TRUE;
        break;
      }
    }

    if (!FunctionEnabled) {
      continue;
    }

    FirstEnabledFunctionRpDev = RpDev;
    if (FirstEnabledFunctionRpDev->Sbdf.Function != 0) {
      if (FirstEnabledFunctionRpDev->PcieConfig->PcieCommonConfig.RpFunctionSwap) {
        Status = RpList->GetThisController (RpList, &FirstEnabledFunctionControllerDev);
        if (EFI_ERROR (Status)) {
          continue;
        }
        OldFunction = FirstEnabledFunctionRpDev->Sbdf.Function;
        DEBUG ((DEBUG_INFO, "Swap Rp %d function 0 with Rp %d function %d\n", Func0RpDev->Id, FirstEnabledFunctionRpDev->Id, FirstEnabledFunctionRpDev->Sbdf.Function));
        PcieSipAssignRootPortFunctionNumber (FirstEnabledFunctionControllerDev, FirstEnabledFunctionRpDev->PrivateConfig.RootPortIndexInController, 0);
        PcieSipAssignRootPortFunctionNumber (Func0ControllerDev, Func0RpDev->PrivateConfig.RootPortIndexInController, OldFunction);
        FirstEnabledFunctionControllerDev->Callbacks.SwapFunctionNumber (FirstEnabledFunctionControllerDev, 0);
        Func0RpDev->Callbacks.SwapFunctionNumber (Func0RpDev, OldFunction);
      } else {
        DEBUG ((DEBUG_INFO, "Force enable PCI config for Rp %d\n", Func0ControllerDev->Id));
        PcieSipEnableRootPortInController (Func0ControllerDev, Func0RpDev->PrivateConfig.RootPortIndexInController);
        Func0RpDev->Callbacks.ForceEnablePciConfig (Func0RpDev);
      }
    }
  }
}

/**
  Hides root ports that have been disabled by straps
  or by the controller config.

  @param[in] RpList  Pointer to the root port list.
**/
VOID
PcieSipHideDisableRootPorts (
  IN PCIE_ROOT_PORT_LIST  *RpList
  )
{
  PCIE_ROOT_PORT_DEV  *RpDev;
  PCIE_ROOT_PORT_DEV  *ControllerDev;
  EFI_STATUS          Status;
  UINT8               RpDisableMask;

  DEBUG ((DEBUG_INFO, "%a start\n", __FUNCTION__));

  for (Status = RpList->ResetToFirst (RpList, &ControllerDev); Status == EFI_SUCCESS; Status = RpList->GetNextController (RpList, &ControllerDev)) {
    switch (PcieSipGetControllerConfig (ControllerDev)) {
      case Pcie1x4:
        RpDisableMask = 0xE;
        break;
      case Pcie2x2:
        RpDisableMask = 0xA;
        break;
      case Pcie1x2_2x1:
        RpDisableMask = 0x2;
        break;
      case Pcie4x1:
      default:
        RpDisableMask = 0x0;
        break;
    }
    for (RpDev = ControllerDev; Status == EFI_SUCCESS; Status = RpList->GetNextRootPortInThisController (RpList, &RpDev)) {
      if (!PcieSipIsRootPortEnabledInController (ControllerDev, RpDev->PrivateConfig.RootPortIndexInController) ||
          (RpDisableMask & (1 << RpDev->PrivateConfig.RootPortIndexInController)) != 0) {
        DEBUG ((DEBUG_INFO, "Disabling Rp %d\n", RpDev->Id));
        RpDev->Callbacks.DisablePciConfig (RpDev);
      }
    }
  }
}