/** @file
  This file contains functions for PCH DMI configuration for SIP15

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PchInfoLib.h>
#include <Library/PeiPchDmiLib.h>
#include <Library/PchDmiLib.h>
#include <Library/P2SbSidebandAccessLib.h>
#include <Library/PeiPcieSipInitLib.h>
#include <Register/PchPcrRegs.h>
#include <Register/PchDmiRegs.h>
#include <Register/PchDmi15Regs.h>
#include <Register/PcieSipRegs.h>


/**
  This function builds DMI specific address offset from PCIe config space.
  For DMI config space under 0xFFF, addresses are created by adding offset,
  specific for this IP.

  @param[in]  PcieOffset             PCIe register space address
  @param[in]  DmiOffset              DMI register space offset
  @retval                            DMI register space address
**/
UINT32
BuildDmiOffset (
  IN  UINT32                PcieOffset,
  IN  UINT32                DmiOffset
  )
{
  if (!(PcieOffset & DmiOffset)) {
    return PcieOffset + DmiOffset;
  }
  return PcieOffset;
}

/**
  This function configures PCH DMI MultiVC support.
  Applicable from SIP17.

  @param[in]  RpDev                Pointer to the root port device
  @param[in]  Vc                   The virtual channel number for programing
**/
VOID
DmiSipConfigurePchMultiVC (
  IN  PCIE_ROOT_PORT_DEV                *RpDev,
  IN  PCH_DMI_VC_TYPE                   Vc
  )
{
  UINT32                        Data32Or;
  UINT32                        Data32And;

  Data32And = ~0u;
  Data32Or = 0u;

  DEBUG ((DEBUG_INFO, "%a() Start on VC %d\n", __FUNCTION__, Vc));
  switch (Vc) {
    case PchDmiVcTypeVc0:
      Data32And = (UINT32)~B_PCIE_CFG_PVCCR1_EVCC_MASK;
      Data32Or = (UINT32)(V_PCIE_CFG_PVCCR1_EVCC_3_VC << B_PCIE_CFG_PVCCR1_EVCC_OFFSET);
      RpDev->PciPcrAccess->AndThenOr32 (
        RpDev->PciPcrAccess,
        BuildDmiOffset (R_PCIE_CFG_PVCCR1, RpDev->PrivateConfig.AddressOffset),
        Data32And,
        Data32Or
        );
      Data32And = (UINT32)~(B_PCIE_CFG_VCCH_NCO_MASK | B_PCIE_CFG_VCCH_CV_MASK | B_PCIE_CFG_VCCH_CID_MASK);
      Data32Or = (UINT32)((V_PCIE_CFG_VCCH_NCO << B_PCIE_CFG_VCCH_NCO_OFFSET) |
                          (V_PCIE_CFG_VCCH_CV_3_VC << B_PCIE_CFG_VCCH_CV_OFFSET) |
                          (V_PCIE_CFG_VCCH_CID_3_VC << B_PCIE_CFG_VCCH_CID_OFFSET));
      RpDev->PciPcrAccess->AndThenOr32 (
        RpDev->PciPcrAccess,
        BuildDmiOffset (R_PCIE_CFG_VCCH, RpDev->PrivateConfig.AddressOffset),
        Data32And,
        Data32Or
        );
      Data32And = (RpDev->PciPcrAccess->Read32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_V0VCRC, RpDev->PrivateConfig.AddressOffset)) &
               (UINT32)~B_PCIE_CFG_V0VCRC_MTS_MASK);
      RpDev->PciPcrAccess->Write32 (
        RpDev->PciPcrAccess,
        BuildDmiOffset (R_PCIE_CFG_V0VCRC, RpDev->PrivateConfig.AddressOffset),
        Data32And
        );
      break;

    case PchDmiVcTypeVc1:
      Data32And = (RpDev->PciPcrAccess->Read32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_V1VCRC, RpDev->PrivateConfig.AddressOffset)) &
               (UINT32)~B_PCIE_CFG_V1VCRC_MTS_MASK);
      RpDev->PciPcrAccess->Write32 (
        RpDev->PciPcrAccess,
        BuildDmiOffset (R_PCIE_CFG_V1VCRC, RpDev->PrivateConfig.AddressOffset),
        Data32And
        );
      break;

    case PchDmiVcTypeVcm:
      break;

    case PchDmiVcTypeMax:
      ASSERT (FALSE);
  }
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_RCRB_FCUCTL, RpDev->PrivateConfig.AddressOffset),
    ~0u,
    (B_PCIE_RCRB_FCUCTL_FC_CP_FCM_VCM | B_PCIE_RCRB_FCUCTL_FC_CP_FCM_VC1 | B_PCIE_RCRB_FCUCTL_FC_CP_FCM)
    );
  DEBUG ((DEBUG_INFO, "%a() End on VC %d\n", __FUNCTION__, Vc));
}

/**
  Get PCH DMI SIP17 Virtual Channel Control and Status registers

  @param[in]  Vc                      The virtual channel number for programing
  @param[out] PchDmiVcCtlAddress      DMI Virtual Channel Control register address
  @param[out] PchDmiVcStsAddress      DMI Virtual Channel Status register address
**/
VOID
SipDmi17VcRegs (
  IN   PCH_DMI_VC_TYPE  Vc,
  OUT  UINT16           *PchDmiVcCtlAddress,
  OUT  UINT16           *PchDmiVcStsAddress
  )
{
  switch (Vc) {
    case PchDmiVcTypeVc0:
      *PchDmiVcCtlAddress = R_PCIE_CFG_VC0CTL;
      *PchDmiVcStsAddress = R_PCIE_CFG_V0STS;
      break;

    case PchDmiVcTypeVc1:
      *PchDmiVcCtlAddress = R_PCIE_CFG_VC1CTL;
      *PchDmiVcStsAddress = R_PCIE_CFG_VC1STS;
      break;

    case PchDmiVcTypeVcm:
      *PchDmiVcCtlAddress = R_PCIE_CFG_VMCTL;
      *PchDmiVcStsAddress = R_PCIE_CFG_VMSTS;
      break;

    case PchDmiVcTypeMax:
      *PchDmiVcCtlAddress = 0;
      *PchDmiVcStsAddress = 0;
      ASSERT (FALSE);
  }
}

/**
  Get PCH default TC VC Mapping settings. This funciton returns the default PCH setting
  System Agent can update the settings according to polices.

  @param[in, out] PchDmiTcVcMap         Buffer for PCH_DMI_TC_VC_MAP instance.

**/
VOID
SipDmiTcVcMapInit (
  IN OUT  PCH_DMI_TC_VC_MAP             *PchDmiTcVcMap
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  if (PchDmiTcVcMap == NULL) {
    return;
  }

  PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc0].Vc   = PchDmiVcTypeVc0;
  PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc0].TcId = PchDmiTcTypeTc0;
  PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc1].Vc   = PchDmiVcTypeVc1;
  PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc1].TcId = PchDmiTcTypeTc1;
  if ((IsAdlPch () && IsPchS ())
     ) {
    PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc2].Vc   = PchDmiVcTypeVc0;
    PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc2].TcId = PchDmiTcTypeTc2;
    PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc3].Vc   = PchDmiVcTypeVc0;
    PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc3].TcId = PchDmiTcTypeTc3;
    PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc4].Vc   = PchDmiVcTypeVc0;
    PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc4].TcId = PchDmiTcTypeTc4;
    PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc5].Vc   = PchDmiVcTypeVc0;
    PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc5].TcId = PchDmiTcTypeTc5;
    PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc6].Vc   = PchDmiVcTypeVc0;
    PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc6].TcId = PchDmiTcTypeTc6;
  }
  PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc7].Vc   = PchDmiVcTypeVcm;
  PchDmiTcVcMap->DmiTc[PchDmiTcTypeTc7].TcId = PchDmiTcTypeTc7;

  PchDmiTcVcMap->DmiVc[PchDmiVcTypeVc0].Enable          = TRUE;
  PchDmiTcVcMap->DmiVc[PchDmiVcTypeVc0].VcId            = 0;
  PchDmiTcVcMap->DmiVc[PchDmiVcTypeVc1].Enable          = TRUE;
  PchDmiTcVcMap->DmiVc[PchDmiVcTypeVc1].VcId            = 1;
  PchDmiTcVcMap->DmiVc[PchDmiVcTypeVcm].Enable          = TRUE;
  PchDmiTcVcMap->DmiVc[PchDmiVcTypeVcm].VcId            = 7;
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  Programing transaction classes of the corresponding virtual channel and Enable it

  @param[in]  Vc                   The virtual channel number for programming
  @param[in]  VcId                 The Identifier to be used for this virtual channel
  @param[in]  VcMap                The transaction classes are mapped to this virtual channel.
                                   When a bit is set, this transaction class is mapped to the virtual channel
  @param[in]  RpDev                Pointer to the root port device

  @retval EFI_INVALID_PARAMETER    Invalid parameter.
  @retval EFI_SUCCESS              The function completed successfully
**/
STATIC
VOID
SipDmiSetTcVcMapping (
  IN   PCH_DMI_VC_TYPE            Vc,
  IN   UINT8                      VcId,
  IN   UINT8                      VcMap,
  IN   IN  PCIE_ROOT_PORT_DEV     *RpDev
  )
{
  UINT32                          VxCtlAnd = 0;
  UINT32                          VxCtlOr = 0;
  UINT16                          VcCtlAddress = 0;
  UINT16                          VcStsAddress = 0;

  if ((IsAdlPch () && IsPchS ())
     ) {
    SipDmi17VcRegs (Vc, &VcCtlAddress, &VcStsAddress);
    switch (Vc) {
      case PchDmiVcTypeVc0:
      VxCtlAnd  = (UINT32) (~(B_PCH_DMI_PCR_V0CTL_ID | V_PCH_DMI_PCR_V0CTL_TVM_MASK));
      VxCtlOr   = ((UINT32) VcId << N_PCH_DMI_PCR_V0CTL_ID) & B_PCH_DMI_PCR_V0CTL_ID;
      VxCtlOr |= (UINT32) VcMap;
      VxCtlOr |= B_PCH_DMI_PCR_V0CTL_EN;
      break;

    case PchDmiVcTypeVc1:
      VxCtlAnd  = (UINT32) (~(B_PCH_DMI_PCR_V1CTL_ID | V_PCH_DMI_PCR_V1CTL_TVM_MASK));
      VxCtlOr   = ((UINT32) VcId << N_PCH_DMI_PCR_V1CTL_ID) & B_PCH_DMI_PCR_V1CTL_ID;
      VxCtlOr |= (UINT32) VcMap;
      VxCtlOr |= B_PCH_DMI_PCR_V1CTL_EN;
      break;

    case PchDmiVcTypeVcm:
      VxCtlAnd  = (UINT32) (~(B_PCH_DMI_PCR_V1CTL_ID | V_PCH_DMI_PCR_V1CTL_TVM_MASK));
      VxCtlOr   = ((UINT32) VcId << N_PCH_DMI_PCR_V1CTL_ID) & B_PCH_DMI_PCR_V1CTL_ID;
      VxCtlOr |= (UINT32) VcMap;
      VxCtlOr |= B_PCH_DMI_PCR_V1CTL_EN;
      break;

    case PchDmiVcTypeMax:
      ASSERT (FALSE);
    }
    DmiSipConfigurePchMultiVC (RpDev, Vc);
    SipDmi17VcRegs (Vc, &VcCtlAddress, &VcStsAddress);
    VxCtlAnd  = (UINT32) (~(B_PCH_DMI_PCR_V1CTL_ID | V_PCH_DMI_PCR_V1CTL_TVM_MASK));
    VxCtlOr   = ((UINT32) VcId << N_PCH_DMI_PCR_V1CTL_ID) & B_PCH_DMI_PCR_V1CTL_ID;
    VxCtlOr |= (UINT32) VcMap;
    VxCtlOr |= B_PCH_DMI_PCR_V1CTL_EN;
  }

  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (VcCtlAddress, RpDev->PrivateConfig.AddressOffset),
                         VxCtlAnd,
                         VxCtlOr
                         );
  //
  // Reads back for posted write to take effect
  // Read back is done in PchPcr lib
  //
}

/**
  Polling negotiation status of the corresponding virtual channel

  @param[in]  Vc                   The virtual channel number for programming
  @param[in]  RpDev                Pointer to the root port device

  @retval EFI_INVALID_PARAMETER    Invalid parameter.
  @retval EFI_SUCCESS              The function completed successfully
**/
STATIC
VOID
SipDmiPollVcStatus (
  IN   PCH_DMI_VC_TYPE            Vc,
  IN   IN  PCIE_ROOT_PORT_DEV     *RpDev
  )
{
  UINT16         VcCtlAddress;
  UINT16         VcStsAddress;

  SipDmi17VcRegs (Vc, &VcCtlAddress, &VcStsAddress);

  //
  // Wait for negotiation to complete
  //
  while ((RpDev->PciPcrAccess->Read16 (RpDev->PciPcrAccess, BuildDmiOffset (VcStsAddress, RpDev->PrivateConfig.AddressOffset)) & B_PCH_DMI_PCR_V1STS_NP) != 0) {
  }
}

/**
  The function performing TC/VC mapping program, and poll all PCH Virtual Channel
  until negotiation completion.

  @param[in]  PchDmiTcVcMap              Buffer for PCH_DMI_TC_VC_MAP instance.
  @param[in]  RpDev                      Pointer to the root port device

  @retval EFI_SUCCESS                   The function completed successfully
  @retval Others                        All other error conditions encountered result in an ASSERT.
**/
EFI_STATUS
SipDmiTcVcProgPoll (
  IN   PCH_DMI_TC_VC_MAP          *PchDmiTcVcMap,
  IN   IN  PCIE_ROOT_PORT_DEV     *RpDev
  )
{
  UINT8                   Index;
  UINT8                   VcMap[PchDmiVcTypeMax];

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  ZeroMem (VcMap, PchDmiVcTypeMax);

  ///
  /// Set the TC/VC mappings
  ///
  for (Index = 0; Index < PchDmiTcTypeMax; Index++) {
    DEBUG ((DEBUG_INFO, "TC:%0x VC:%0x!\n", PchDmiTcVcMap->DmiTc[Index].TcId, PchDmiTcVcMap->DmiTc[Index].Vc));
    VcMap[PchDmiTcVcMap->DmiTc[Index].Vc] |= (BIT0 << PchDmiTcVcMap->DmiTc[Index].TcId);
  }

  for (Index = 0; Index < PchDmiVcTypeMax; Index++) {
    DEBUG ((DEBUG_INFO, "VC:%0x VCID:%0x Enable:%0x!\n",Index, PchDmiTcVcMap->DmiVc[Index].VcId, PchDmiTcVcMap->DmiVc[Index].Enable));
    if (PchDmiTcVcMap->DmiVc[Index].Enable == TRUE) {
      SipDmiSetTcVcMapping (
        Index,
        PchDmiTcVcMap->DmiVc[Index].VcId,
        VcMap[Index],
        RpDev
        );
    }
  }

  ///
  /// After both above and System Agent DMI TC/VC mapping are programmed,
  /// poll VC negotiation pending status until is zero:
  ///
  for (Index = 0; Index < PchDmiVcTypeMax; Index++) {
    if (PchDmiTcVcMap->DmiVc[Index].Enable == TRUE) {
      SipDmiPollVcStatus (Index, RpDev);
    }
  }
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));

  return EFI_SUCCESS;
}

/**
  This function checks if DMI SIP15 Secured Register Lock (SRL) is set

  @param[in]  RpDev                Pointer to the root port device
  @retval SRL state
**/
BOOLEAN
DmiSipIsPchDmiLocked (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  return ((RpDev->PciPcrAccess->Read32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_MPC, RpDev->PrivateConfig.AddressOffset)) & B_PCIE_CFG_MPC_SRL) != 0);
}

/**
  Enable PCIe Relaxed Order for DMI SIP15

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipEnablePcieRelaxedOrder (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  //
  // Enable Forced Relaxed Ordering to always allow downstream completions to pass posted writes.
  // Set Completion Relaxed Ordering Attribute Override Value
  // and Completion Relaxed Ordering Attribute Override Enable
  //
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PCIECFG2, RpDev->PrivateConfig.AddressOffset), ~0u, (B_PCIE_CFG_PCIECFG2_CROAOV | B_PCIE_CFG_PCIECFG2_CROAOE));
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function will switch SAI value to be driven to IOSF Primary Fabric
  for cycles with Core BDF from HOSTIA_BOOT_SAI to HOSTIA_POSTBOOT_SAI.

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipEnablePostBootSai (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  //
  // Lock SIP17 Registers
  //
  if (RpDev->SipVersion >= PcieSip17) {
    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_LPCR, RpDev->PrivateConfig.AddressOffset),
      (UINT32)~0u,
      (UINT32)(B_PCIE_CFG_LPCR_DIDOVR_LOCK | B_PCIE_CFG_LPCR_SERL)
      );
    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_SRL, RpDev->PrivateConfig.AddressOffset),
      (UINT32)~0u,
      (UINT32)(B_PCIE_CFG_SRL_SRL)
      );
    DEBUG ((DEBUG_INFO, "PchDmi15EnablePostBootSai: Sip17lock active\n"));
  }
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_CTRL1, RpDev->PrivateConfig.AddressOffset),
    ~0u,
    B_PCIE_CFG_CTRL1_PSS
    );
  //
  // Read back to ensure that the programming through sideband completes
  // before any new transaction is generated by subsequent code.
  // Ordering between primary and sideband is not guaranteed and
  // primary transaction triggered afterwards may be completed
  // before IPCS_PSS programming is finished.
  //
  RpDev->PciPcrAccess->Read32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_CTRL1, RpDev->PrivateConfig.AddressOffset));
}

/**
  This function performs basic DMI initialization.

  @param[in]  RpDev                Pointer to the root port device
**/
STATIC
VOID
DmiSipBasicInit (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  //
  // Configure Uncorrectable Error Mask
  // Set the Completer Abort Mask, Poisoned TLP and Unsupported Request Error Mask
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_UEM, RpDev->PrivateConfig.AddressOffset),
    ~0u,
    (B_PCIE_CFG_UEM_URE | B_PCIE_CFG_UEM_CM | B_PCIE_CFG_UEM_PT)
    );

  //
  // Configure Transmit Datapath Flush Timer
  //  00b - Wait for 4 clocks prior to initiating rate or powerstate change.
  // Configure Transmit Configuration Change Wait Time
  //  00b - 128ns for GEN1 and GEN2 mode. 256ns for GEN3 mode.
  //
  RpDev->PciPcrAccess->AndThenOr8 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PHYCTL2, RpDev->PrivateConfig.AddressOffset),
    (UINT8) ~(B_PCIE_CFG_PHYCTL2_TDFT | B_PCIE_CFG_PHYCTL2_TXCFGCHGWAIT),
    0
    );

  //
  // Configure Squelch Direction settings
  //
  if (RpDev->PrivateConfig.SquelchDirectionOverrideDisable) {
    RpDev->PciPcrAccess->AndThenOr8 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_PHYCTL3, RpDev->PrivateConfig.AddressOffset),
      (UINT8) ~(B_PCIE_CFG_PHYCTL3_SQDIROVREN | B_PCIE_CFG_PHYCTL3_SQDIRCTRL),
      (UINT8) 0
      );
  } else {
    RpDev->PciPcrAccess->AndThenOr8 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_PHYCTL3, RpDev->PrivateConfig.AddressOffset),
      (UINT8) ~0,
      (B_PCIE_CFG_PHYCTL3_SQDIROVREN | B_PCIE_CFG_PHYCTL3_SQDIRCTRL)
      );
  }

  //
  // Set Recovery Upconfiguration Disable
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_MPC2, RpDev->PrivateConfig.AddressOffset),
    ~0u,
    B_PCIE_CFG_MPC2_RUD
    );

  //
  // Set De-skew Buffer Unload Pointer Increment
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PWRCTL, RpDev->PrivateConfig.AddressOffset),
    ~0u,
    B_PCIE_CFG_PWRCTL_DBUPI
    );

  //
  // Set FC to send update immediately after the CREDITS ALLOCATED register has been updated
  //
  RpDev->PciPcrAccess->AndThenOr8 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCH_DMI15_PCR_MPC, RpDev->PrivateConfig.AddressOffset),
    (UINT8) ~B_PCIE_CFG_MPC_FCP,
    0
    );

  if (RpDev->PrivateConfig.DoNotHangOnLinkDown) {
    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_CTRL1, RpDev->PrivateConfig.AddressOffset),
      ~0u,
      B_PCIE_CFG_CTRL1_DLDHB
      );
  } else {
    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_CTRL1, RpDev->PrivateConfig.AddressOffset),
      (UINT32)~B_PCIE_CFG_CTRL1_L1RCEP,
      (UINT32) 0u
      );
  }
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  Configure DMI SIP17 aggresive clock gating.
  This function must be called before ASPM is enabled

  @param[in]  RpDev                Pointer to the root port device
**/
STATIC
VOID
DmiSipConfigureAggresiveClockGating (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32   Data32Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    R_PCIE_RCRB_IPCLKCTR,
    ~0u,
    B_PCIE_RCRB_IPCLKCTR_PXDCGE
    );

  //
  // Configure Hardware Save and Restore
  //
  Data32Or = (UINT32)((V_PCIE_CFG_HWSNR_EEH << B_PCIE_CFG_HWSNR_EEH_OFFSET) |
                      (V_PCIE_CFG_HWSNR_REPW << B_PCIE_CFG_HWSNR_REPW_OFFSET) |
                      (V_PCIE_CFG_HWSNR_BEPW << B_PCIE_CFG_HWSNR_BEPW_OFFSET));
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_HWSNR, RpDev->PrivateConfig.AddressOffset),
    (UINT32)~(B_PCIE_CFG_HWSNR_EEH | B_PCIE_CFG_HWSNR_REPW | B_PCIE_CFG_HWSNR_BEPW),
    Data32Or
    );

  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PGCTRL, RpDev->PrivateConfig.AddressOffset),
    (UINT32)~B_PCIE_CFG_PGCTRL_PMREQBLKRSPT,
    (UINT32)(V_PCIE_CFG_PGCTRL_PMREQBLKRSPT_10MICRO_SEC << B_PCIE_CFG_PGCTRL_PMREQBLKRSPT_OFFSET)
    );

  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PGCBCTL1, RpDev->PrivateConfig.AddressOffset),
    (UINT32)~B_PCIE_CFG_PGCBCTL1_TACCRSTUP_MASK,
    (UINT32)(V_PCIE_CFG_PGCBCTL1_TACCRSTUP << B_PCIE_CFG_PGCBCTL1_TACCRSTUP_OFFSET)
    );

  //
  // Configure Replay Timer Policy
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PCIERTP1, RpDev->PrivateConfig.AddressOffset),
    (UINT32)~(B_PCIE_CFG_PCIERTP1_G2X1_MASK | B_PCIE_CFG_PCIERTP1_G2X2_MASK |
              B_PCIE_CFG_PCIERTP1_G2X4_MASK | B_PCIE_CFG_PCIERTP1_G1X1_MASK |
              B_PCIE_CFG_PCIERTP1_G1X2_MASK | B_PCIE_CFG_PCIERTP1_G1X4_MASK),
    (UINT32)((V_PCIE_CFG_PCIERTP1_G2X1 << B_PCIE_CFG_PCIERTP1_G2X1_OFFSET) |
             (V_PCIE_CFG_PCIERTP1_G2X2 << B_PCIE_CFG_PCIERTP1_G2X2_OFFSET) |
             (V_PCIE_CFG_PCIERTP1_G2X4 << B_PCIE_CFG_PCIERTP1_G2X4_OFFSET) |
             (V_PCIE_CFG_PCIERTP1_G1X1 << B_PCIE_CFG_PCIERTP1_G1X1_OFFSET) |
             (V_PCIE_CFG_PCIERTP1_G1X2 << B_PCIE_CFG_PCIERTP1_G1X2_OFFSET) |
             (V_PCIE_CFG_PCIERTP1_G1X4 << B_PCIE_CFG_PCIERTP1_G1X4_OFFSET))
    );

  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PCIERTP3, RpDev->PrivateConfig.AddressOffset),
    (UINT32)~(B_PCIE_CFG_PCIERTP3_G1X8_MASK | B_PCIE_CFG_PCIERTP3_G1X16_MASK),
    (UINT32)((V_PCIE_CFG_PCIERTP3_G1X8 << B_PCIE_CFG_PCIERTP3_G1X8_OFFSET) |
             (V_PCIE_CFG_PCIERTP3_G1X16 << B_PCIE_CFG_PCIERTP3_G1X16_OFFSET))
    );
  //
  // Configure Aggresive Clock Gating
  //
  Data32Or = (UINT32)(B_PCIE_RCRB_DCGM1_PXTTSULDCGM | B_PCIE_RCRB_DCGM1_PXTRSULDCGM |
                      B_PCIE_RCRB_DCGM1_PXTRULDCGM | B_PCIE_RCRB_DCGM1_PXLSULDCGM |
                      B_PCIE_RCRB_DCGM1_PXLIULDCGM | B_PCIE_RCRB_DCGM1_PXLTULDCGM |
                      B_PCIE_RCRB_DCGM1_PXLRULDCGM | B_PCIE_RCRB_DCGM1_PXCULDCGM |
                      B_PCIE_RCRB_DCGM1_PXKGULDCGM);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_DCGM1, ~0u, Data32Or);

  Data32Or = (UINT32)(B_PCIE_RCRB_DCGM2_PXSDRULDCGM | B_PCIE_RCRB_DCGM2_PXSDTULDCGM |
                      B_PCIE_RCRB_DCGM2_PXSDIULDCGM | B_PCIE_RCRB_DCGM2_PXFRULDCGM |
                      B_PCIE_RCRB_DCGM2_PXFTULDCGM | B_PCIE_RCRB_DCGM2_PXPBULDCGM |
                      B_PCIE_RCRB_DCGM2_PXPSULDCGM | B_PCIE_RCRB_DCGM2_PXPIULDCGM);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_DCGM2, ~0u, Data32Or);

  Data32Or = (UINT32)(B_PCIE_RCRB_DCGM3_PXTTSUPDCGM | B_PCIE_RCRB_DCGM3_PXTTUPDCGM |
                      B_PCIE_RCRB_DCGM3_PXTOUPDCGM | B_PCIE_RCRB_DCGM3_PXTRSUPDCGM |
                      B_PCIE_RCRB_DCGM3_PXTRUPDCGM | B_PCIE_RCRB_DCGM3_PXLIUPDCGM |
                      B_PCIE_RCRB_DCGM3_PXLTUPDCGM | B_PCIE_RCRB_DCGM3_PXLRUPDCGM |
                      B_PCIE_RCRB_DCGM3_PXSRUSSNRDCGM | B_PCIE_RCRB_DCGM3_PXCUPSNRDCGM |
                      B_PCIE_RCRB_DCGM3_PXCUPSRCDCGM | B_PCIE_RCRB_DCGM3_PXBUPDCGM |
                      B_PCIE_RCRB_DCGM3_PXEUPDCGM);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_DCGM3, ~0u, Data32Or);

  //
  // Enable Aggresive Clock Gating
  //
  Data32Or = (UINT32)(B_PCIE_RCRB_DCGEN1_PXTTSULDCGEN | B_PCIE_RCRB_DCGEN1_PXTRSULDCGEN |
                      B_PCIE_RCRB_DCGEN1_PXTRULDCGEN | B_PCIE_RCRB_DCGEN1_PXLSULDCGEN |
                      B_PCIE_RCRB_DCGEN1_PXLIULDCGEN | B_PCIE_RCRB_DCGEN1_PXLRULDCGEN |
                      B_PCIE_RCRB_DCGEN1_PXCULDCGEN | B_PCIE_RCRB_DCGEN1_PXKGULDCGEN);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_DCGEN1, ~0u, Data32Or);

  Data32Or = (UINT32)(B_PCIE_RCRB_DCGEN2_PXSDRULDCGEN | B_PCIE_RCRB_DCGEN2_PXSDTULDCGEN |
                      B_PCIE_RCRB_DCGEN2_PXSDIULDCGEN | B_PCIE_RCRB_DCGEN2_PXFRULDCGEN |
                      B_PCIE_RCRB_DCGEN2_PXFTULDCGEN | B_PCIE_RCRB_DCGEN2_PXPBULDCGEN |
                      B_PCIE_RCRB_DCGEN2_PXPSULDCGEN |B_PCIE_RCRB_DCGEN2_PXPIULDCGEN);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_DCGEN2, ~0u, Data32Or);

  Data32Or = (UINT32)(B_PCIE_RCRB_DCGEN3_PXTTSUPDCGEN | B_PCIE_RCRB_DCGEN3_PXTTUPDCGEN |
                      B_PCIE_RCRB_DCGEN3_PXTOUPDCGEN | B_PCIE_RCRB_DCGEN3_PXTRSUPDCGEN |
                      B_PCIE_RCRB_DCGEN3_PXTRUPDCGEN | B_PCIE_RCRB_DCGEN3_PXLIUPDCGEN |
                      B_PCIE_RCRB_DCGEN3_PXLRUPDCGEN | B_PCIE_RCRB_DCGEN3_PXSRUSSNRDCGEN |
                      B_PCIE_RCRB_DCGEN3_PXBUPDCGEN | B_PCIE_RCRB_DCGEN3_PXEUPDCGEN);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_DCGEN3, ~0u, Data32Or);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  Configure DMI clock gating.
  This function must be called before ASPM is enabled

  @param[in]  RpDev                Pointer to the root port device
**/
STATIC
VOID
DmiSipConfigureClockGating (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  //
  // Enable Dynamic Clock Gating on ISM Active
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_CCFG, RpDev->PrivateConfig.AddressOffset),
    ~0u,
    B_PCIE_CFG_CCFG_DCGEISMA
    );

  //
  // Enable below clock gating settings:
  // - Partition/Trunk Oscillator Clock Gate Enable
  // - Link CLKREQ Enable
  // - Backbone CLKREQ Enable
  // - Shared Resource Dynamic Backbone Clock Gate Enable
  // - Root Port Dynamic Link Clock Gate Enable
  // - Root Port Dynamic Backbone Clock Gate Enable
  //
  RpDev->PciPcrAccess->AndThenOr8 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_RPDCGEN, RpDev->PrivateConfig.AddressOffset),
    (UINT8)~0,
    (B_PCIE_CFG_RPDCGEN_PTOCGE | B_PCIE_CFG_RPDCGEN_LCLKREQEN | B_PCIE_CFG_RPDCGEN_BBCLKREQEN |
    B_PCIE_CFG_RPDCGEN_SRDBCGEN | B_PCIE_CFG_RPDCGEN_RPDLCGEN | B_PCIE_CFG_RPDCGEN_RPDBCGEN)
    );

  //
  // Enable Side Clock Partition/Trunk Clock Gating
  // Set IOSF Sideband Interface Idle Counter to 00b - Wait for
  // 32 idle clocks before allowing trunk clock gating.
  //
  RpDev->PciPcrAccess->AndThenOr8 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_IOSFSBCS, RpDev->PrivateConfig.AddressOffset),
    (UINT8)~B_PCIE_CFG_IOSFSBCS_SIID,
    B_PCIE_CFG_IOSFSBCS_SCPTCGE
    );

  //
  // Enable Sideband Endpoint Oscillator/Side Clock Gating
  //
  if (RpDev->SipVersion >= PcieSip17) {
    RpDev->PciPcrAccess->AndThenOr8 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_RPPGEN, RpDev->PrivateConfig.AddressOffset),
      (UINT8)~0,
      (B_PCIE_CFG_RPPGEN_SEOSCGE | B_PCIE_CFG_RPPGEN_PTOTOP)
      );
    //
    // Program OSC Clock Gate Hysteresis
    //
    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_PCIEPMECTL3, RpDev->PrivateConfig.AddressOffset),
      (UINT32)~(B_PCIE_CFG_PCIEPMECTL3_L1PGAUTOPGEN | B_PCIE_CFG_PCIEPMECTL3_OSCCGH_MASK | B_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH_MASK),
      (UINT32)((V_PCIE_CFG_PCIEPMECTL3_OSCCGH_1US << B_PCIE_CFG_PCIEPMECTL3_OSCCGH_OFFSET) |
               (V_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH << B_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH_OFFSET))
      );
    DmiSipConfigureAggresiveClockGating (RpDev);
  } else {
    RpDev->PciPcrAccess->AndThenOr8 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_RPPGEN, RpDev->PrivateConfig.AddressOffset),
      (UINT8)~0,
      B_PCIE_CFG_RPPGEN_SEOSCGE
      );
  }
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  Configure DMI link power management.
  This function must be called before ASPM is enabled

  @param[in]  PchPcieConfig        Pointer to the PcieRpConfig
  @param[in]  RpDev                Pointer to the root port device
**/
STATIC
VOID
DmiSipConfigureLinkPowerManagement (
  IN  PCH_PCIE_CONFIG              *PchPcieConfig,
  IN  PCIE_ROOT_PORT_DEV           *RpDev
  )
{
  UINT8                 Data8And;
  UINT8                 Data8Or;
  UINT32                Data32And;
  UINT32                Data32Or;

  Data32And = ~0u;
  Data32Or = 0u;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  if (RpDev->SipVersion >= PcieSip17) {
    //
    // Programming DMI L1 preparation latency
    // to 4us for PCH A0 stepping,, and to 2us for other
    //
    if (PchStepping () > PCH_A0) {
      Data8Or  = (V_PCIE_CFG_CTRL1_L1PL_2US << N_PCIE_CFG_CTRL1_L1PL) >> 8;
    } else {
      Data8Or  = (V_PCIE_CFG_CTRL1_L1PL_4US << N_PCIE_CFG_CTRL1_L1PL) >> 8;
    }
  } else {
    Data8Or  = (V_PCIE_CFG_CTRL1_L1PL_4US << N_PCIE_CFG_CTRL1_L1PL) >> 8;
  }
  Data8And = (UINT8)~(B_PCIE_CFG_CTRL1_L1PL >> 8);
  /**
   Allow DMI enter L1 when all root ports are in L1, L0s or link down. Disabled by default.
   When set to TRUE turns on:
     - L1 State Controller Power Gating
     - L1 State PHY Data Lane Power Gating
     - PHY Common Lane Power Gating
     - Hardware Autonomous Enable
     - PMC Request Enable and Sleep Enable
  **/
  if (PchPcieConfig->DmiPort.PcieRpCommonConfig.ClockGating == FALSE) {
    Data8And &= (UINT8)~(B_PCIE_CFG_CTRL1_L1RC >> 8);
  }
  //
  // Configure DMI L1 Preparation Latency to 4us - this is the time link layer has to
  // indicate IDLE before the link initialization and control logic enters Active State L1.
  // Be careful not to touch first byte of this register as CTRL1.PSS is RWO.
  //

  RpDev->PciPcrAccess->AndThenOr8 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_CTRL1 + 1, RpDev->PrivateConfig.AddressOffset),
    Data8And,
    Data8Or
    );

  //
  // Configure Gen1 and Gen2 Active State L0s Preparation Latency - time that link layer has to
  // indicate IDLE before the link initialization and control logic enters L0s
  // Set it to 0x14 clocks
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PCIEL0SC, RpDev->PrivateConfig.AddressOffset),
    (UINT32)~(B_PCIE_CFG_PCIEL0SC_G2ASL0SPL | B_PCIE_CFG_PCIEL0SC_G1ASL0SPL),
    ((0x14 << N_PCIE_CFG_PCIEL0SC_G2ASL0SPL) |
    (0x14 << N_PCIE_CFG_PCIEL0SC_G1ASL0SPL))
    );

  //
  // Configure Gen1 and Gen2 Common Clock N_FTS
  //
  if (RpDev->SipVersion >= PcieSip17) {
    Data32And = (UINT32)~(B_PCIE_CFG_PCIENFTS_G2UCNFTS_MASK | B_PCIE_CFG_PCIENFTS_G2CCNFTS_MASK |
                          B_PCIE_CFG_PCIENFTS_G1UCNFTS_MASK | B_PCIE_CFG_PCIENFTS_G1CCNFTS_MASK);
    Data32Or = (UINT32)((V_PCIE_CFG_PCIENFTS_G2UCNFTS << B_PCIE_CFG_PCIENFTS_G2UCNFTS_OFFSET) |
                        (V_PCIE_CFG_PCIENFTS_G2CCNFTS << B_PCIE_CFG_PCIENFTS_G2CCNFTS_OFFSET) |
                        (V_PCIE_CFG_PCIENFTS_G1UCNFTS << B_PCIE_CFG_PCIENFTS_G1UCNFTS_OFFSET) |
                        (V_PCIE_CFG_PCIENFTS_G1CCNFTS << B_PCIE_CFG_PCIENFTS_G1CCNFTS_OFFSET));
    RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PCIENFTS, RpDev->PrivateConfig.AddressOffset), Data32And, Data32Or);

    Data32And = (UINT32)~(B_PCIE_CFG_G3L0SCTL_G3ASL0SPL_MASK | B_PCIE_CFG_G3L0SCTL_G3UCNFTS_MASK | B_PCIE_CFG_G3L0SCTL_G3CCNFTS_MASK);
    Data32Or = (UINT32)((V_PCIE_CFG_G3L0SCTL_G3ASL0SPL << B_PCIE_CFG_G3L0SCTL_G3ASL0SPL_OFFSET) |
                        (V_PCIE_CFG_G3L0SCTL_G3UCNFTS << B_PCIE_CFG_G3L0SCTL_G3UCNFTS_OFFSET) |
                        (V_PCIE_CFG_G3L0SCTL_G3CCNFTS << B_PCIE_CFG_G3L0SCTL_G3CCNFTS_OFFSET));
    RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_G3L0SCTL, RpDev->PrivateConfig.AddressOffset), Data32And, Data32Or);

    //
    // Configure Gen3 Common Clock N_FTS to 0x70
    //
    Data32And = (UINT32)~(B_PCIE_CFG_G4L0SCTL_G4ASL0SPL_MASK | B_PCIE_CFG_G4L0SCTL_G4L0SIC_MASK |
                          B_PCIE_CFG_G4L0SCTL_G4UCNFTS_MASK | B_PCIE_CFG_G4L0SCTL_G4CCNFTS_MASK);
    Data32Or = (UINT32) ((V_PCIE_CFG_G4L0SCTL_G4ASL0SPL << B_PCIE_CFG_G4L0SCTL_G4ASL0SPL_OFFSET) |
                         (V_PCIE_CFG_G4L0SCTL_G4L0SIC << B_PCIE_CFG_G4L0SCTL_G4L0SIC_OFFSET) |
                         (V_PCIE_CFG_G4L0SCTL_G4UCNFTS << B_PCIE_CFG_G4L0SCTL_G4UCNFTS_OFFSET) |
                         (V_PCIE_CFG_G4L0SCTL_G4CCNFTS << B_PCIE_CFG_G4L0SCTL_G4CCNFTS_OFFSET));
    RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_G4L0SCTL, RpDev->PrivateConfig.AddressOffset), Data32And, Data32Or);

    Data32And = (UINT32)~(B_PCIE_RCRB_CFG_G5L0SCTL_G5UCNFTS_MASK | B_PCIE_RCRB_CFG_G5L0SCTL_G5CCNFTS_MASK);
    Data32Or = (UINT32)0u;
    RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_CFG_G5L0SCTL, Data32And, Data32Or);
  } else {
    //
    // Configure Gen3 Active State L0s Preparation Latency - time that link layer has to
    // indicate IDLE before the link initialization and control logic enters L0s
    // Set it to 0x28 clocks
    // Set Gen3 Common Clock N_FTS to 0x38
    //
    Data32And = (UINT32)~(B_PCIE_CFG_PCIENFTS_G2CCNFTS_MASK | B_PCIE_CFG_PCIENFTS_G1CCNFTS_MASK);
    Data32Or = (UINT32)((0x70 << B_PCIE_CFG_PCIENFTS_G2CCNFTS_OFFSET) | (0x38 << B_PCIE_CFG_PCIENFTS_G1CCNFTS_OFFSET));
    RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PCIENFTS, RpDev->PrivateConfig.AddressOffset), Data32And, Data32Or);

    Data32And = (UINT32)~(B_PCIE_CFG_G3L0SCTL_G3ASL0SPL_MASK | B_PCIE_CFG_G3L0SCTL_G3CCNFTS_MASK);
    Data32Or = (UINT32)(((0x28 << B_PCIE_CFG_G3L0SCTL_G3ASL0SPL_OFFSET) | (0x2c << B_PCIE_CFG_G3L0SCTL_G3CCNFTS_OFFSET)));
    RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_G3L0SCTL, RpDev->PrivateConfig.AddressOffset), Data32And, Data32Or);
  }

  //
  // Configure L1.2, L1.1 and L1 Standard
  //
  Data32And = (UINT32)~0u;
  Data32Or = (UINT32)(B_PCIE_CFG_L1SCAP_L1PSS | B_PCIE_CFG_L1SCAP_AL11S |
              B_PCIE_CFG_L1SCAP_AL12S | B_PCIE_CFG_L1SCAP_PPL11S |
              B_PCIE_CFG_L1SCAP_PPL12S);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L1SCAP, RpDev->PrivateConfig.AddressOffset), Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  Configure DMI Squelch Power Management.

  @param[in]  RpDev                Pointer to the root port device
**/
STATIC
VOID
DmiSipConfigureSquelchPowerManagement (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  if (!(RpDev->SipVersion >= PcieSip17)) {
    //
    // Enable Low Bandwidth Squelch Settling Timer
    // Set this before enabling any of the squelch power management
    //
    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_PCIECFG2, RpDev->PrivateConfig.AddressOffset),
      ~0u,
      B_PCIE_CFG_PCIECFG2_LBWSSTE
      );
    //
    // Enable Squelch propagation control
    //
    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_PCIEDBG, RpDev->PrivateConfig.AddressOffset),
      ~0u,
      B_PCIE_CFG_PCIEDBG_SPCE
    );
  }

  //
  // Configure Root Port Squelch Exit Wait Latency to 120ns
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PWRCTL, RpDev->PrivateConfig.AddressOffset),
    (UINT32) ~B_PCIE_CFG_PWRCTL_RPSEWL,
    V_PCIE_CFG_PWRCTL_RPSEWL_120NS << N_PCIE_CFG_PWRCTL_RPSEWL
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  Configure DMI PLL Shutdown.

  @param[in]  RpDev                Pointer to the root port device
**/
STATIC
VOID
DmiSipConfigurePllShutdown (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32   Data32And;
  UINT32   Data32Or;
  UINT8    Data8And;
  UINT8    Data8Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  //
  // Configure PLL Wait to 0us.This is amount of time to wait from gating the link clock
  // before turning off the PLL.
  // Enable Gen3 PLL coupling
  // This needs to be done before enabling PLL shutdown
  //
  Data32And = (UINT32) ~B_PCIE_CFG_MPC2_PLLWAIT;
  Data32Or = (UINT32) ((V_PCIE_CFG_MPC2_PLLWAIT_0US << N_PCIE_CFG_MPC2_PLLWAIT) | B_PCIE_CFG_MPC2_GEN2PLLC | B_PCIE_CFG_MPC2_GEN3PLLC);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_MPC2, RpDev->PrivateConfig.AddressOffset), Data32And, Data32Or);

  //
  // Enable PCI Express GEN2 and GEN3 PLL Off
  // If enabled and no devices require the use of the PXP PLL clock outputs,
  // the PXP PLL can be shutdown.
  // This programming should be done before enabling ASPM
  //
  Data8And = (UINT8) ~0;
  Data8Or = (UINT8) (B_PCIE_CFG_PHYCTL2_PXPG3PLLOFFEN | B_PCIE_CFG_PHYCTL2_PXPG2PLLOFFEN);
  RpDev->PciPcrAccess->AndThenOr8 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PHYCTL2, RpDev->PrivateConfig.AddressOffset), Data8And, Data8Or);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  Configure DMI Power Gating.
  This needs to done before enabling ASPM L1

  @param[in]  PchPcieConfig        The PCH Policy PPI instance
  @param[in]  RpDev                Pointer to the root port device
**/
STATIC
VOID
DmiSipConfigurePowerGating (
  IN  PCH_PCIE_CONFIG              *PchPcieConfig,
  IN  PCIE_ROOT_PORT_DEV           *RpDev
  )
{
  UINT32            AndData;
  UINT32            OrData;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  //
  // Disable the squelch circuitry for all lanes
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PHYCTL4, RpDev->PrivateConfig.AddressOffset),
    ~0u,
    B_PCIE_CFG_PHYCTL4_SQDIS
    );

  //
  // Disable IP-Accessible Context Propagation
  // Enable PHY Lane Power Gating when link resides in LTSSM Disabled, Detect or L23_Rdy state
  // Enable disabling squelch when the link resides in LTSSM Disabled, L23_Rdy state or if the lane is unconfigured
  // Enable IP-Inaccessible Entry Policy
  //
  if (RpDev->SipVersion >= PcieSip17) {
    AndData = (UINT32) ~B_PCIE_CFG_PWRCTL_DARECE;
    OrData = (UINT32) (B_PCIE_CFG_PWRCTL_LIFECF | B_PCIE_CFG_PWRCTL_WPDMPGEP | B_PCIE_CFG_PWRCTL_TXSWING);
    RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PWRCTL, RpDev->PrivateConfig.AddressOffset), AndData, OrData);
    AndData = (UINT32)~(B_PCIE_CFG_PCIEPMECTL_IPACPE | B_PCIE_CFG_PCIEPMECTL_DLSULDLSD);
    OrData = (UINT32) (B_PCIE_CFG_PCIEPMECTL_FDPPGE | B_PCIE_CFG_PCIEPMECTL_DLSULPPGE | B_PCIE_CFG_PCIEPMECTL_IPIEP);
  } else {
    AndData = (UINT32)~B_PCIE_CFG_PCIEPMECTL_IPACPE;
    OrData = (UINT32) (B_PCIE_CFG_PCIEPMECTL_DLSULPPGE | B_PCIE_CFG_PCIEPMECTL_DLSULDLSD | B_PCIE_CFG_PCIEPMECTL_IPIEP);
  }
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PCIEPMECTL, RpDev->PrivateConfig.AddressOffset), AndData, OrData);

  if (PchPcieConfig->DmiPowerReduction) {
    //
    // Enable L1 State Controller Power Gating
    // Enable L1 State PHY Data Lane Power Gating
    // Enable PHY Common Lane Power Gating
    //
    if (RpDev->SipVersion >= PcieSip17) {
      //
      // Configure PHY Power Gating
      //
      RpDev->PciPcrAccess->AndThenOr32 (
        RpDev->PciPcrAccess,
        R_PCIE_CFG_RCRB_CFG_PHYPG,
        ~0u,
        (B_PCIE_CFG_RCRB_CFG_PHYPG_DLPPGP | B_PCIE_CFG_RCRB_CFG_PHYPG_DUCFGPHYPGE |
         B_PCIE_CFG_RCRB_CFG_PHYPG_L23PHYPGE | B_PCIE_CFG_RCRB_CFG_PHYPG_DISPHYPGE |
         B_PCIE_CFG_RCRB_CFG_PHYPG_DETPHYPGE)
        );
      AndData = (UINT32)~(B_PCIE_CFG_PCIEPMECTL2_CPMCSRE | B_PCIE_CFG_PCIEPMECTL2_CPGEXH_MASK | B_PCIE_CFG_PCIEPMECTL2_CPGENH_MASK);
      OrData = (UINT32)(B_PCIE_CFG_PCIEPMECTL2_PHYCLPGE | B_PCIE_CFG_PCIEPMECTL2_L1SPHYDLPGE | B_PCIE_CFG_PCIEPMECTL2_FDCPGE |
                        B_PCIE_CFG_PCIEPMECTL2_DETSCPGE | B_PCIE_CFG_PCIEPMECTL2_L23RDYSCPGE | B_PCIE_CFG_PCIEPMECTL2_DISSCPGE |
                        B_PCIE_CFG_PCIEPMECTL2_L1SCPGE);
      RpDev->PciPcrAccess->AndThenOr32 (
        RpDev->PciPcrAccess,
        BuildDmiOffset (R_PCIE_CFG_PCIEPMECTL, RpDev->PrivateConfig.AddressOffset),
        (UINT32)~(B_PCIE_CFG_PCIEPMECTL_DLSULDLSD | B_PCIE_CFG_PCIEPMECTL_L1FSOE |
                  B_PCIE_CFG_PCIEPMECTL_L1LTRTLSV | B_PCIE_CFG_PCIEPMECTL_L1LTRTLV),
        (UINT32)(B_PCIE_CFG_PCIEPMECTL_DLSULPPGE | (V_PCIE_CFG_PCIEPMECTL_L1LTRTLSV << B_PCIE_CFG_PCIEPMECTL_L1LTRTLSV_OFFSET) |
                 (V_PCIE_CFG_PCIEPMECTL_L1LTRTLV << B_PCIE_CFG_PCIEPMECTL_L1LTRTLV_OFFSET))
        );
    } else {
      AndData = (UINT32)~0u;
      OrData = (UINT32)(B_PCIE_CFG_PCIEPMECTL2_PHYCLPGE | B_PCIE_CFG_PCIEPMECTL2_L1SPHYDLPGE | B_PCIE_CFG_PCIEPMECTL2_L1SCPGE);
    }
    RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PCIEPMECTL2, RpDev->PrivateConfig.AddressOffset), AndData, OrData);
  } else if (RpDev->SipVersion >= PcieSip17) {
    AndData = (UINT32)~0u;
    OrData = (UINT32)(B_PCIE_CFG_PCIEPMECTL2_L1SPHYDLPGE);
    RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PCIEPMECTL2, RpDev->PrivateConfig.AddressOffset), AndData, OrData);
  }

  if (PchPcieConfig->DmiPowerReduction) {
    //
    // Set Hardware Autonomous Enable,
    // PMC Request Enable and Sleep Enable
    //
    AndData = (UINT32)~B_PCIE_CFG_PCE_PMCRE;
    OrData = B_PCIE_CFG_PCE_HAE | B_PCIE_CFG_PCE_SE;
  } else {
    //
    // Clear Hardware Autonomous Enable and Sleep Enable
    //
    AndData = (UINT32)~(B_PCIE_CFG_PCE_PMCRE | B_PCIE_CFG_PCE_SE);
    if (RpDev->SipVersion >= PcieSip17) {
      OrData = B_PCIE_CFG_PCE_SE;
    } else {
      OrData = 0;
    }
  }
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PCE, RpDev->PrivateConfig.AddressOffset),
    AndData,
    OrData
    );

  if (!(RpDev->SipVersion >= PcieSip17)) {
    //
    // CPG Exit Link Clock Wake Disable
    //
    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_CONTROL2, RpDev->PrivateConfig.AddressOffset),
      ~0u,
      B_PCIE_CFG_CONTROL2_CPGEXLCWDIS
      );
  }
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function configures ASPM on DMI

  @param[in]  PchPcieConfig        Pointer to PCH PCIe Config block
  @param[in]  PchGeneralConfig     Pointer to PCH General Config block
  @param[in]  RpDev                Pointer to the root port device
**/
STATIC
VOID
DmiSipConfigureAspm (
  IN  PCH_PCIE_CONFIG              *PchPcieConfig,
  IN  PCH_GENERAL_CONFIG           *PchGeneralConfig,
  IN  PCIE_ROOT_PORT_DEV           *RpDev
  )
{
  UINT16                           Data16And;
  UINT16                           Data16Or;
  UINT32                           Data32And;
  UINT32                           Data32Or;
  PCH_PCIE_ASPM_CONTROL            DmiAspmCtrl;

  Data32And = ~0u;
  Data32Or = 0u;
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  //
  // Enable DMI ASPM
  //
  if (PchGeneralConfig->LegacyIoLowLatency) {
    DmiAspmCtrl = PchPcieAspmDisabled;
  } else {
    DmiAspmCtrl = PchPcieConfig->DmiPort.PcieRpCommonConfig.Aspm;
  }

  Data16And = (UINT16)~B_PCIE_CFG_LCTL_ASPM;

  if ((RpDev->SipVersion >= PcieSip17) || IsEbgPch ()) {
    if (DmiAspmCtrl == PchPcieAspmL0sL1) {
      //
      // Enable L0s/L1 on DMI
      //
      Data16Or = V_PCIE_CFG_LCTL_ASPM_L0SL1;
      Data32Or = (UINT32) (V_PCIE_CFG_LCAP_APMS_L0SL1 << B_PCIE_CFG_LCAP_APMS_OFFSET);
    } else if (DmiAspmCtrl == PchPcieAspmL0s) {
      //
      // Enable L0s Entry only
      //
      Data16Or = V_PCIE_CFG_LCTL_ASPM_L0S;
      Data32Or = (UINT32) (V_PCIE_CFG_LCAP_APMS_L0S << B_PCIE_CFG_LCAP_APMS_OFFSET);
    } else if ((DmiAspmCtrl == PchPcieAspmAutoConfig) || (DmiAspmCtrl == PchPcieAspmL1)) {
      //
      // Enable L1 Entry only
      //
      Data16Or = V_PCIE_CFG_LCTL_ASPM_L1;
      Data32Or = (UINT32) (V_PCIE_CFG_LCAP_APMS_L1 << B_PCIE_CFG_LCAP_APMS_OFFSET);
    } else {
      //
      // ASPM Disabled
      //
      Data16Or = V_PCIE_CFG_LCTL_ASPM_DIS;
      Data32Or = (UINT32) (V_PCIE_CFG_LCAP_APMS_DIS << B_PCIE_CFG_LCAP_APMS_OFFSET);
    }
    Data32And = (UINT32)~(B_PCIE_CFG_LCAP_EL1_MASK | B_PCIE_CFG_LCAP_EL0_MASK | B_PCIE_CFG_LCAP_APMS_MASK);
    Data32Or |= (UINT32) ((V_PCIE_CFG_LCAP_EL1 << B_PCIE_CFG_LCAP_EL1_OFFSET) |
                          (V_PCIE_CFG_LCAP_EL0 << B_PCIE_CFG_LCAP_EL0_OFFSET) | B_PCIE_CFG_LCAP_ASPMOC);
  } else {
    if ((DmiAspmCtrl == PchPcieAspmAutoConfig) || (DmiAspmCtrl == PchPcieAspmL0sL1)) {
      //
      // Enable L0s/L1 on DMI
      //
      Data16Or = V_PCIE_CFG_LCTL_ASPM_L0SL1;
    } else if (DmiAspmCtrl == PchPcieAspmL0s) {
      //
      // Enable L0s Entry only
      //
      Data16Or = V_PCIE_CFG_LCTL_ASPM_L0S;
    } else if (DmiAspmCtrl == PchPcieAspmL1) {
      //
      // Enable L1 Entry only
      //
      Data16Or = V_PCIE_CFG_LCTL_ASPM_L1;
    } else {
      //
      // ASPM Disabled
      //
      Data16Or  = V_PCIE_CFG_LCTL_ASPM_DIS;
    }
    Data32And = (UINT32) ~(B_PCIE_CFG_LCAP_EL1_MASK | B_PCIE_CFG_LCAP_EL0_MASK);
    Data32Or |= (UINT32)((V_PCIE_CFG_LCAP_EL1_8US_16US << B_PCIE_CFG_LCAP_EL1_OFFSET) |
                         (V_PCIE_CFG_LCAP_EL0_256NS_512NS << B_PCIE_CFG_LCAP_EL0_OFFSET));
  }

  //
  // Configue DMI ASPM
  //
  RpDev->PciPcrAccess->AndThenOr16 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_LCTL, RpDev->PrivateConfig.AddressOffset),
    Data16And,
    Data16Or
    );

  //
  // Configure L0s Exit Latency to 0x3 (128ns - 256ns)
  // Configure L1s Exit Latency to 0x4 (8us - 16us)
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_LCAP, RpDev->PrivateConfig.AddressOffset),
    Data32And,
    Data32Or
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  Set DMI thermal throttling to recommended configuration.
  It's intended for PCH-H DMI SIP15 and above.

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipSetRecommendedThermalThrottling (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32  Data32And;
  UINT32  Data32Or;
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  ///
  /// DMI recommended Thermal Sensor Target Width
  /// is the HW default configuration:
  ///  - Thermal Sensor 3 Target Width: 0 (x1)
  ///  - Thermal Sensor 2 Target Width: 1 (x2)
  ///  - Thermal Sensor 1 Target Width: 2 (x4)
  ///  - Thermal Sensor 0 Target Width: 3 (x8)
  /// Enable Thermal Sensor Autonomous Width
  ///
  Data32And = (UINT32)~(B_PCH_DMI15_PCR_UPHWAWC_TS3TW | B_PCH_DMI15_PCR_UPHWAWC_TS2TW |
                        B_PCH_DMI15_PCR_UPHWAWC_TS1TW | B_PCH_DMI15_PCR_UPHWAWC_TS0TW);
  if (RpDev->SipVersion >= PcieSip17) {
    Data32Or  = (0 << N_PCH_DMI15_PCR_UPHWAWC_TS3TW) |
              (1 << N_PCH_DMI15_PCR_UPHWAWC_TS2TW) |
              (2 << N_PCH_DMI15_PCR_UPHWAWC_TS1TW) |
              (3 << N_PCH_DMI15_PCR_UPHWAWC_TS0TW);
  } else {
    Data32Or  = (0 << N_PCH_DMI15_PCR_UPHWAWC_TS3TW) |
              (1 << N_PCH_DMI15_PCR_UPHWAWC_TS2TW) |
              (2 << N_PCH_DMI15_PCR_UPHWAWC_TS1TW) |
              (3 << N_PCH_DMI15_PCR_UPHWAWC_TS0TW) |
              B_PCH_DMI15_PCR_UPHWAWC_TSAWEN;
  }

  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCH_DMI15_PCR_UPHWAWC, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  Set DMI thermal throttling to custom configuration.
  This function will configure Thermal Sensor 0/1/2/3 TargetWidth and set
  DMI Thermal Sensor Autonomous Width Enable.
  It's intended only for PCH-H DMI SIP15.

  @param[in]  PchDmiThermalThrottling       DMI Thermal Throttling structure
  @param[in]  RpDev                         Pointer to the root port device
**/
VOID
DmiSipSetCustomThermalThrottling (
  IN PCH_DMI_THERMAL_THROTTLING         PchDmiThermalThrottling,
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32  Data32And;
  UINT32  Data32Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  ///
  /// DMI Throttling action
  ///
  Data32And = (UINT32)~(B_PCH_DMI15_PCR_UPHWAWC_TS3TW | B_PCH_DMI15_PCR_UPHWAWC_TS2TW |
                        B_PCH_DMI15_PCR_UPHWAWC_TS1TW | B_PCH_DMI15_PCR_UPHWAWC_TS0TW);
  Data32Or  = (PchDmiThermalThrottling.ThermalSensor3TargetWidth << N_PCH_DMI15_PCR_UPHWAWC_TS3TW) |
              (PchDmiThermalThrottling.ThermalSensor2TargetWidth << N_PCH_DMI15_PCR_UPHWAWC_TS2TW) |
              (PchDmiThermalThrottling.ThermalSensor1TargetWidth << N_PCH_DMI15_PCR_UPHWAWC_TS1TW) |
              (PchDmiThermalThrottling.ThermalSensor0TargetWidth << N_PCH_DMI15_PCR_UPHWAWC_TS0TW) |
              B_PCH_DMI15_PCR_UPHWAWC_TSAWEN;

  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCH_DMI15_PCR_UPHWAWC, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}


/**
  This function performs PCH-H DMI Thermal Throttling init.

  @param[in]  ThermalConfig        Pointer to the PCH Thermal Config block
  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureThermalThrottling (
  IN  THERMAL_CONFIG              *ThermalConfig,
  IN  PCIE_ROOT_PORT_DEV          *RpDev
  )
{
  PCH_DMI_THERMAL_THROTTLING    DmiThermalThrottling;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  if (IsPchH () || (RpDev->SipVersion >= PcieSip17)) {
    if (ThermalConfig->DmiHaAWC.SuggestedSetting) {
      DmiSipSetRecommendedThermalThrottling (RpDev);
    } else if (ThermalConfig->DmiHaAWC.DmiTsawEn) {
      DmiThermalThrottling.ThermalSensor0TargetWidth = ThermalConfig->DmiHaAWC.TS0TW;
      DmiThermalThrottling.ThermalSensor1TargetWidth = ThermalConfig->DmiHaAWC.TS1TW;
      DmiThermalThrottling.ThermalSensor2TargetWidth = ThermalConfig->DmiHaAWC.TS2TW;
      DmiThermalThrottling.ThermalSensor3TargetWidth = ThermalConfig->DmiHaAWC.TS3TW;
      DmiSipSetCustomThermalThrottling (DmiThermalThrottling, RpDev);
    }
  }
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  Configure PCH DMI 10 Bit Tag and Scaled Flow Control.
  Applicable from SIP 17.

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigure10BitTagAndScaledFlowControl (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32         Data32And;
  UINT32         Data32Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  Data32And = (UINT32) ~(B_PCIE_CFG_DCAP2_PX10BTRS | B_PCIE_CFG_DCAP2_PX10BTCS);
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_DCAP2, RpDev->PrivateConfig.AddressOffset),
    Data32And,
    0
    );

  Data32And = (UINT32) ~B_PCIE_CFG_DCTL2_PX10BTRE;
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_DCTL2, RpDev->PrivateConfig.AddressOffset), Data32And, 0);
  //
  // Configure Power Gating Clock Wake Control
  //
  Data32And = (UINT32) ~(B_PCIE_ADVMCTRL_F10BTSE | B_PCIE_ADVMCTRL_CCBE | B_PCIE_ADVMCTRL_PMREQCWC_MASK | B_PCIE_ADVMCTRL_PMREQBLKPGRSPT_MASK);
  Data32Or = (UINT32) ((V_PCIE_ADVMCTRL_PMREQCWC_LNK_PRIM << N_PCIE_ADVMCTRL_PMREQCWC_OFFSET) |
                       (V_PCIE_ADVMCTRL_PMREQBLKPGRSPT_10US << N_PCIE_ADVMCTRL_PMREQBLKPGRSPT_OFFSET));
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_ADVMCTRL, RpDev->PrivateConfig.AddressOffset), Data32And, Data32Or);

  Data32Or = (UINT32) B_PCH_PCIE_DLFCAP_LSFCS;
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCH_PCIE_DLFCAP, RpDev->PrivateConfig.AddressOffset), ~0u, Data32Or);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs disabling of PCH DMI TC aliasing.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipDisableTCAliasing (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  //
  // Affects only VC1
  // Cycles to IOSF remap TC to '0000'
  // Cycles from IOSF remap TC to '0001'
  // Configure Un-Squelch Sampling period
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_PCIEDBG, RpDev->PrivateConfig.AddressOffset),
    (UINT32)~(B_PCIE_CFG_PCIEDBG_SPCE),
    (UINT32)(B_PCIE_CFG_PCIEDBG_DTCA)
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs programing of registers for Elastic Buffer.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
EFIAPI
DmiSipConfigureElasticBuffer (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32  Data32Or;
  UINT32  Data32And;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  Data32And = (UINT32)~B_PCIE_CFG_ACGR3S2_LSTPTLS_MASK;
  Data32Or = (UINT32)(B_PCIE_CFG_ACGR3S2_UPL1EPC | B_PCIE_CFG_ACGR3S2_G4EBM | B_PCIE_CFG_ACGR3S2_G3EBM |
                      B_PCIE_CFG_ACGR3S2_G2EBM | B_PCIE_CFG_ACGR3S2_G1EBM | B_PCIE_CFG_ACGR3S2_SRT |
                      (V_PCIE_CFG_ACGR3S2_LSTPTLS << B_PCIE_CFG_ACGR3S2_LSTPTLS_OFFSET));

  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_ACGR3S2, RpDev->PrivateConfig.AddressOffset),
    Data32And,
    Data32Or
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function configures DMI Server Error Reporting Mode
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureSerm (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32     Data32And;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  //
  // Configure SERM and PCIe Interrupt Pin
  //
  Data32And = (RpDev->PciPcrAccess->Read32 (
                 RpDev->PciPcrAccess,
                 BuildDmiOffset (R_PCIE_CFG_STRPFUSECFG, RpDev->PrivateConfig.AddressOffset)
                 ) & (UINT32)~B_PCIE_CFG_STRPFUSECFG_SERM) | B_PCIE_CFG_STRPFUSECFG_PXIP;
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_STRPFUSECFG, RpDev->PrivateConfig.AddressOffset),
    Data32And,
    V_PCIE_CFG_STRPFUSECFG_PXIP << N_PCIE_CFG_STRPFUSECFG_PXIP
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function sets PCH DMI Local and Remote Transmitter
  Link Equalization Preset/Coefficient Evaluation Bypass,
  and performs Precursor and Postcursor Preset/Coefficient
  mapping initialization.

  @param[in]  TestEnable           Is Test Environment in use
  @param[in]  RpDev                Pointer to the root port device
**/
VOID
SipDmiConfigurePresetCoefficientInitialization (
  IN  BOOLEAN                           TestEnable,
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32     Data32And;
  UINT32     Data32Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  Data32And = (UINT32)~(0);
  Data32Or = (UINT32)(0);
  if (RpDev->SipVersion >= PcieSip17) {
    Data32And = (UINT32)~(B_PCIE_CFG_RTPCL1_PCM |
                B_PCIE_CFG_RTPCL1_RTPRECL2PL4 |
                B_PCIE_CFG_RTPCL1_RTPOSTCL1PL3 |
                B_PCIE_CFG_RTPCL1_RTPRECL1PL2 |
                B_PCIE_CFG_RTPCL1_RTPOSTCL0PL1 |
                B_PCIE_CFG_RTPCL1_RTPRECL0PL0
                );

    Data32Or = (UINT32) (( 6 << N_PCIE_CFG_RTPCL1_RTPRECL2PL4) |
                (5 << N_PCIE_CFG_RTPCL1_RTPOSTCL1PL3) |
                (5 << N_PCIE_CFG_RTPCL1_RTPRECL1PL2) |
                (3 << N_PCIE_CFG_RTPCL1_RTPOSTCL0PL1) |
                (2 << N_PCIE_CFG_RTPCL1_RTPRECL0PL0)
                );

    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_RTPCL1, RpDev->PrivateConfig.AddressOffset),
      Data32And,
      Data32Or
      );

    Data32And = (UINT32)~(B_PCIE_CFG_RTPCL2_RTPOSTCL3PL7 |
                          B_PCIE_CFG_RTPCL2_RTPRECL3PL6 |
                          B_PCIE_CFG_RTPCL2_RTPOSTCL2PL5
                          );

    Data32Or = (UINT32) (( 8 << N_PCIE_CFG_RTPCL2_RTPOSTCL3PL7) |
                         ( 9 << N_PCIE_CFG_RTPCL2_RTPRECL3PL6 ) |
                         ( 7 << N_PCIE_CFG_RTPCL2_RTPOSTCL2PL5)
                         );

    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_RTPCL2, RpDev->PrivateConfig.AddressOffset),
      Data32And,
      Data32Or
      );

    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_HAEQ, RpDev->PrivateConfig.AddressOffset),
      (UINT32)~(B_PCIE_CFG_HAEQ_HAPCCPI | B_PCIE_CFG_HAEQ_MACFOMC | B_PCIE_CFG_HAEQ_DL),
      (UINT32)(V_PCIE_CFG_HAEQ_DL << N_PCIE_CFG_HAEQ_DL)
      );

    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_EQCFG2, RpDev->PrivateConfig.AddressOffset),
      (UINT32)~(B_PCIE_CFG_EQCFG2_PCET | B_PCIE_CFG_EQCFG2_HAPCSB | B_PCIE_CFG_EQCFG2_REWMET),
      (UINT32)((V_PCIE_CFG_EQCFG2_PCET << N_PCIE_CFG_EQCFG2_PCET) |
       (V_PCIE_CFG_EQCFG2_HAPCSB << N_PCIE_CFG_EQCFG2_HAPCSB) |
       (V_PCIE_CFG_EQCFG2_REWMET << N_PCIE_CFG_EQCFG2_REWMET))
      );

    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_EQCFG4, RpDev->PrivateConfig.AddressOffset),
      (UINT32)~(B_PCIE_CFG_EQCFG4_PX16GRTLEPCEB | B_PCIE_CFG_EQCFG4_PX16GRTPCOE |
                B_PCIE_CFG_EQCFG4_PX16GRWTNEVE | B_PCIE_CFG_EQCFG4_PX16GHAPCCPI),
      (UINT32)((V_PCIE_CFG_EQCFG4_PX16GRWTNEVE_3US << N_PCIE_CFG_EQCFG4_PX16GRWTNEVE) |
               (V_PCIE_CFG_EQCFG4_PX16GHAPCCPI << B_PCIE_CFG_EQCFG4_PX16GHAPCCPI_OFFSET))
      );

    Data32And = (UINT32)~(B_PCIE_CFG_PX16GRTPCL1_PCM |
                B_PCIE_CFG_PX16GRTPCL1_RTPRECL2PL4 |
                B_PCIE_CFG_PX16GRTPCL1_RTPOSTCL1PL3 |
                B_PCIE_CFG_PX16GRTPCL1_RTPRECL1PL2 |
                B_PCIE_CFG_PX16GRTPCL1_RTPOSTCL0PL1 |
                B_PCIE_CFG_PX16GRTPCL1_RTPRECL0PL0
                );

    Data32Or = (UINT32) ((3 << N_PCIE_CFG_PX16GRTPCL1_RTPRECL2PL4) |
                         (3 << N_PCIE_CFG_PX16GRTPCL1_RTPOSTCL1PL3) |
                         (3 << N_PCIE_CFG_PX16GRTPCL1_RTPRECL1PL2) |
                         (3 << N_PCIE_CFG_PX16GRTPCL1_RTPOSTCL0PL1) |
                         (3 << N_PCIE_CFG_PX16GRTPCL1_RTPRECL0PL0)
                         );

    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_PX16GRTPCL1, RpDev->PrivateConfig.AddressOffset),
      Data32And,
      Data32Or
      );

    Data32And = (UINT32)~(B_PCIE_CFG_PX16GRTPCL2_RTPOSTCL4PL9 |
                          B_PCIE_CFG_PX16GRTPCL2_RTPRECL4PL8 |
                          B_PCIE_CFG_PX16GRTPCL2_RTPOSTCL3PL7 |
                          B_PCIE_CFG_PX16GRTPCL2_RTPRECL3PL6 |
                          B_PCIE_CFG_PX16GRTPCL2_RTPOSTCL2PL5
                          );

    Data32Or = (UINT32) (( 3 << N_PCIE_CFG_PX16GRTPCL2_RTPOSTCL4PL9) |
                         ( 3 << N_PCIE_CFG_PX16GRTPCL2_RTPRECL4PL8 ) |
                         ( 3 << N_PCIE_CFG_PX16GRTPCL2_RTPOSTCL3PL7) |
                         ( 3 << N_PCIE_CFG_PX16GRTPCL2_RTPRECL3PL6) |
                         ( 3 << N_PCIE_CFG_PX16GRTPCL2_RTPOSTCL2PL5)
                         );

    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_PX16GRTPCL2, RpDev->PrivateConfig.AddressOffset),
      Data32And,
      Data32Or
      );

    Data32And = (UINT32)~(B_PCIE_CFG_PX16GRTPCL3_RTPRECL7 |
                          B_PCIE_CFG_PX16GRTPCL3_RTPOSTCL6 |
                          B_PCIE_CFG_PX16GRTPCL3_RTPRECL6 |
                          B_PCIE_CFG_PX16GRTPCL3_RTPOSTCL5 |
                          B_PCIE_CFG_PX16GRTPCL3_RTPRECL5PL10
                          );

    Data32Or = (UINT32) (( 3 << N_PCIE_CFG_PX16GRTPCL3_RTPRECL7) |
                         ( 3 << N_PCIE_CFG_PX16GRTPCL3_RTPOSTCL6 ) |
                         ( 3 << N_PCIE_CFG_PX16GRTPCL3_RTPRECL6) |
                         ( 3 << N_PCIE_CFG_PX16GRTPCL3_RTPOSTCL5) |
                         ( 3 << N_PCIE_CFG_PX16GRTPCL3_RTPRECL5PL10)
                         );

    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_PX16GRTPCL3, RpDev->PrivateConfig.AddressOffset),
      Data32And,
      Data32Or
      );

    Data32And = (UINT32)~(B_PCIE_CFG_PX16GRTPCL4_RTPOSTCL7);

    Data32Or = (UINT32) (3 << N_PCIE_CFG_PX16GRTPCL4_RTPOSTCL7);

    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_PX16GRTPCL4, RpDev->PrivateConfig.AddressOffset),
      Data32And,
      Data32Or
      );

    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_EQCFG5, RpDev->PrivateConfig.AddressOffset),
      (UINT32)~(B_PCIE_CFG_EQCFG5_PCET | B_PCIE_CFG_EQCFG5_HAPCSB),
      (UINT32)((V_PCIE_CFG_EQCFG5_PCET << N_PCIE_CFG_EQCFG5_PCET) |
       (V_PCIE_CFG_EQCFG5_HAPCSB << N_PCIE_CFG_EQCFG5_HAPCSB))
      );
    //
    // Set PCR[DMI] + Offset 2454h
    //
    Data32And = (UINT32) ~(B_PCIE_CFG_EQCFG1_TUPP | B_PCIE_CFG_EQCFG1_LERSMIE |
                           B_PCIE_CFG_EQCFG1_RWTNEVE | B_PCIE_CFG_EQCFG1_HAPCCPIE |
                           B_PCIE_CFG_EQCFG1_RTLEPCEB | B_PCIE_CFG_EQCFG1_RTPCOE);
    Data32Or = (UINT32) ((V_PCIE_CFG_EQCFG1_RWTNEVE_1US << N_PCIE_CFG_EQCFG1_RWTNEVE));
  }

  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_EQCFG1, RpDev->PrivateConfig.AddressOffset),
    Data32And,
    Data32Or
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of miscellaneous SIP 17 DMI registers

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureMiscConfiguration (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32    Data32And;
  UINT32    Data32Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  Data32And = (UINT32) ~(B_PCIE_CFG_PCIECFG2_RLLG3R | B_PCIE_CFG_PCIECFG2_PMET_MASK);
  Data32Or = (UINT32) (B_PCIE_CFG_PCIECFG2_CROAOV | B_PCIE_CFG_PCIECFG2_CROAOE | B_PCIE_CFG_PCIECFG2_CRSREN);
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_PCIECFG2, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  //
  // Configure Rx Delay
  //
  Data32And = (UINT32) ~(B_PCIE_CFG_CCFG_UPMWPD | B_PCIE_CFG_CCFG_UNRD);
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_CCFG, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         0u
                         );

  //
  // Configure Completion Ordering Mode
  //
  Data32And = (UINT32) ~(B_PCIE_CFG_DC_DCT1C |B_PCIE_CFG_DC_DCT0C | B_PCIE_CFG_DC_COM);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_DC, RpDev->PrivateConfig.AddressOffset), Data32And, 0u);

  Data32And = (UINT32) ~(B_PCIE_CFG_PCIEALC_ITLRCLD | B_PCIE_CFG_PCIEALC_ILLRCLD |
                         B_PCIE_CFG_PCIEALC_SSRLD | B_PCIE_CFG_PCIEALC_SSRRS |
                         B_PCIE_CFG_PCIEALC_RTD3PDSP);
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_PCIEALC, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         0u
                         );

  //
  // Configure Peer Disable
  //
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_PCIEDBG, RpDev->PrivateConfig.AddressOffset),
                         (UINT32)~B_PCIE_CFG_PCIEDBG_LDSWQRP,
                         (UINT32)B_PCIE_CFG_PCIEDBG_CTONFAE
                         );

  Data32And = (UINT32) ~(B_PCIE_ADVMCTRL_ACCRM | B_PCIE_ADVMCTRL_EIOSMASKRX |
                         B_PCIE_ADVMCTRL_RRLLCL | B_PCIE_ADVMCTRL_RLLG12R);
  Data32Or = (UINT32)(B_PCIE_ADVMCTRL_INRXL0CTRL | B_PCIE_ADVMCTRL_EIOSDISDS |
                      B_PCIE_ADVMCTRL_RXL0DC | B_PCIE_ADVMCTRL_G3STFER);
  RpDev->PciSbiMsgCfgAccess->AndThenOr32 (
                         RpDev->PciSbiMsgCfgAccess,
                         BuildDmiOffset (R_PCIE_ADVMCTRL, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  //
  // Configure Error Injection
  //
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_EINJCTL, RpDev->PrivateConfig.AddressOffset),
                         ~0u,
                         B_PCIE_CFG_EINJCTL_EINJDIS
                         );

  Data32And = (UINT32)~(B_PCIE_CFG_MPC_CCEL | B_PCIE_CFG_MPC_IRRCE);
  Data32Or = (UINT32) ((V_PCIE_CFG_MPC_CCEL << N_PCIE_CFG_MPC_CCEL) | B_PCIE_CFG_MPC_FCDL1E | B_PCIE_CFG_MPC_BT);
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_MPC, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  Data32And = (UINT32)~(B_PCIE_CFG_AECH_NCO_MASK | B_PCIE_CFG_AECH_CV_MASK | B_PCIE_CFG_AECH_CID_MASK);
  Data32Or = (UINT32) ((V_PCIE_CFG_AECH_NCO << B_PCIE_CFG_AECH_NCO_OFFSET) |
                       (V_PCIE_CFG_AECH_CV << B_PCIE_CFG_AECH_CV_OFFSET) |
                       (V_PCIE_CFG_AECH_CID << B_PCIE_CFG_AECH_CID_OFFSET));
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_AECH, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  Data32And = (UINT32)~(B_PCIE_CFG_EX_SPEECH_NCO_MASK | B_PCIE_CFG_EX_SPEECH_CV_MASK | B_PCIE_CFG_EX_SPEECH_PCIEECID_MASK);
  Data32Or = (UINT32) ((V_PCIE_CFG_EX_SPEECH_NCO << B_PCIE_CFG_EX_SPEECH_NCO_OFFSET) |
                       (V_PCIE_CFG_EX_SPEECH_CV << B_PCIE_CFG_EX_SPEECH_CV_OFFSET) |
                       (V_PCIE_CFG_EX_SPEECH_PCIEECID << B_PCIE_CFG_EX_SPEECH_PCIEECID_OFFSET));
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_EX_SPEECH, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  Data32And = (UINT32)~(B_PCIE_CFG_DLFECH_NCO_MASK | B_PCIE_CFG_DLFECH_CV_MASK | B_PCIE_CFG_DLFECH_PCIEECID_MASK);
  Data32Or = (UINT32) ((V_PCIE_CFG_DLFECH_NCO << B_PCIE_CFG_DLFECH_NCO_OFFSET) |
                       (V_PCIE_CFG_DLFECH_CV << B_PCIE_CFG_DLFECH_CV_OFFSET) |
                       (V_PCIE_CFG_DLFECH_PCIEECID << B_PCIE_CFG_DLFECH_PCIEECID_OFFSET));
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_DLFECH, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  Data32And = (UINT32)~(B_PCIE_CFG_PL16GECH_NCO_MASK | B_PCIE_CFG_PL16GECH_CV_MASK | B_PCIE_CFG_PL16GECH_CID_MASK);
  Data32Or = (UINT32) ((V_PCIE_CFG_PL16GECH_NCO << B_PCIE_CFG_PL16GECH_NCO_OFFSET) |
                       (V_PCIE_CFG_PL16GECH_CV << B_PCIE_CFG_PL16GECH_CV_OFFSET) |
                       (V_PCIE_CFG_PL16GECH_CID << B_PCIE_CFG_PL16GECH_CID_OFFSET));
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_PL16GECH, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  Data32And = (UINT32)~(B_PCIE_CFG_PL16MECH_NCO_MASK | B_PCIE_CFG_PL16MECH_CV_MASK | B_PCIE_CFG_PL16MECH_CID_MASK);
  Data32Or = (UINT32) ((V_PCIE_CFG_PL16MECH_NCO << B_PCIE_CFG_PL16MECH_NCO_OFFSET) |
                       (V_PCIE_CFG_PL16MECH_CV << B_PCIE_CFG_PL16MECH_CV_OFFSET) |
                       (V_PCIE_CFG_PL16MECH_CID << B_PCIE_CFG_PL16MECH_CID_OFFSET));
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_PL16MECH, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         R_PCIE_RCRB_CFG_PIDECCTL,
                         ~0u,
                         B_PCIE_RCRB_CFG_PIDECCTL_CPLBNCHK
                         );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of DMI Rx Margining.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureRxMargining (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32   Data32And;
  UINT32   Data32Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  Data32And = (UINT32)~(B_PCIE_CFG_RXMC1_MSRVS_MASK | B_PCIE_CFG_RXMC1_MSRTS_MASK |
                        B_PCIE_CFG_RXMC1_TMSLOP_MASK | B_PCIE_CFG_RXMC1_VMSLOP_MASK |
                        B_PCIE_CFG_RXMC1_MMNOLS_MASK);
  Data32Or = (UINT32) ((V_PCIE_CFG_RXMC1_MSRVS << B_PCIE_CFG_RXMC1_MSRVS_OFFSET) |
                       (V_PCIE_CFG_RXMC1_MSRTS << B_PCIE_CFG_RXMC1_MSRTS_OFFSET) |
                       (V_PCIE_CFG_RXMC1_TMSLOP << B_PCIE_CFG_RXMC1_TMSLOP_OFFSET) |
                       (V_PCIE_CFG_RXMC1_VMSLOP << B_PCIE_CFG_RXMC1_VMSLOP_OFFSET) |
                       (V_PCIE_CFG_RXMC1_MMNOLS << B_PCIE_CFG_RXMC1_MMNOLS_OFFSET));
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_RXMC1, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  Data32And = (UINT32)~(B_PCIE_CFG_RXMC2_MNOTSS_MASK | B_PCIE_CFG_RXMC2_MMTOS_MASK |
                        B_PCIE_CFG_RXMC2_MNOVSS_MASK);
  Data32Or = (UINT32) ((V_PCIE_CFG_RXMC2_MNOTSS << B_PCIE_CFG_RXMC2_MNOTSS_OFFSET) |
                       (V_PCIE_CFG_RXMC2_MMTOS << B_PCIE_CFG_RXMC2_MMTOS_OFFSET) |
                       (V_PCIE_CFG_RXMC2_MNOVSS << B_PCIE_CFG_RXMC2_MNOVSS_OFFSET));
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_RXMC2, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of DMI Phy.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigurePhy (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32   Data32And;
  UINT32   Data32Or;
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_CONTROL2, RpDev->PrivateConfig.AddressOffset),
                         (UINT32) ~B_PCIE_CFG_CONTROL2_RXHALEP,
                         B_PCIE_CFG_CONTROL2_HALEP
                         );

  Data32And = (UINT32)~(0u);
  Data32Or = (UINT32) (B_PCIE_RCRB_LPHYCP4_DPMDNDBFE | B_PCIE_RCRB_LPHYCP4_PLLBUSDRC);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_LPHYCP4, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of DMI Phy Recalibration.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigurePhyRecal (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32   Data32And;
  UINT32   Data32Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  Data32And = (UINT32)~(B_PCIE_RCRB_IORCCP1_G5ORRRXECC_MASK | B_PCIE_RCRB_IORCCP1_G4ORRRXECC_MASK |
                        B_PCIE_RCRB_IORCCP1_G3ORRRXECC_MASK |B_PCIE_RCRB_IORCCP1_G2ORRRXECC_MASK |
                        B_PCIE_RCRB_IORCCP1_G1ORRRXECC_MASK);
  Data32Or = (UINT32) ((V_PCIE_RCRB_IORCCP1_G5ORRRXECC << B_PCIE_RCRB_IORCCP1_G5ORRRXECC_OFFSET) |
                       (V_PCIE_RCRB_IORCCP1_G4ORRRXECC << B_PCIE_RCRB_IORCCP1_G4ORRRXECC_OFFSET) |
                       (V_PCIE_RCRB_IORCCP1_G3ORRRXECC << B_PCIE_RCRB_IORCCP1_G3ORRRXECC_OFFSET) |
                       (V_PCIE_RCRB_IORCCP1_G2ORRRXECC << B_PCIE_RCRB_IORCCP1_G2ORRRXECC_OFFSET) |
                       (V_PCIE_RCRB_IORCCP1_G1ORRRXECC << B_PCIE_RCRB_IORCCP1_G1ORRRXECC_OFFSET) |
                        B_PCIE_RCRB_IORCCP1_DISORCRODI | B_PCIE_RCRB_IORCCP1_DRCORRP | B_PCIE_RCRB_IORCCP1_DISORCL12REC);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_IORCCP1, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of DMI L1 Support.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureL1Support (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32   Data32Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  Data32Or = (UINT32) (B_PCIE_RCRB_UPDLPHYCP_UPDLTCDC | B_PCIE_RCRB_UPDLPHYCP_UPDLLSMFLD);
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_UPDLPHYCP, ~0u, Data32Or);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of DMI D3 Vnn removal.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureD3VnnRemCtl (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32   Data32And;
  UINT32   Data32Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  Data32And = (UINT32)~(B_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_MASK | B_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_MASK);
  Data32Or = (UINT32) ((V_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_8CLKS << B_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_OFFSET) |
                       (V_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_4CLKS << B_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_OFFSET));
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_VNNREMCTL, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of Master Cycle Decoding.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureRxMasterCycleDecoding (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_CFG_DECCTL, (UINT32)~(B_PCIE_RCRB_CFG_DECCTL_URRXMCTPNTCO), 0u);

  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_CFG_PIDECCTL, ~0u, (UINT32)(B_PCIE_RCRB_CFG_PIDECCTL_CPLBNCHK));
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function  Sets BIOS Lock-Down (BILD)

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipSetBiosLockDown (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32 Data32And;
  UINT32 Data32Or;
  Data32And = 0xFFFFFFFF;
  Data32Or = B_PCH_DMI_PCR_BILD;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCH_DMI_PCR_GCS, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of DMI Max Payload Size.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureMPS (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32   Data32And;
  UINT32   Data32Or;

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  Data32And = B_PCIE_CFG_DCAP_MPS_MASK;
  Data32Or = V_PCIE_CFG_DCAP_MPS_256 << B_PCIE_CFG_DCAP_MPS_OFFSET;
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_DCAP, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  Data32And = B_PCIE_CFG_DCTL_MPS_MASK;
  Data32Or = (UINT32)((V_PCIE_CFG_DCTL_MPS_256 << B_PCIE_CFG_DCTL_MPS_OFFSET) |
                      (B_PCIE_CFG_DCTL_URE | B_PCIE_CFG_DCTL_FEE | B_PCIE_CFG_DCTL_NFE | B_PCIE_CFG_DCTL_CEE));
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_DCTL, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );

  Data32And = (UINT32) ~B_PCIE_CFG_IPCS_IMPS_MASK;
  Data32Or = (V_PCIE_CFG_IPCS_IMPS << B_PCIE_CFG_IPCS_IMPS_OFFSET);
  RpDev->PciPcrAccess->AndThenOr32 (
                         RpDev->PciPcrAccess,
                         BuildDmiOffset (R_PCIE_CFG_IPCS, RpDev->PrivateConfig.AddressOffset),
                         Data32And,
                         Data32Or
                         );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of DMI Coalescing.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureCoalescing (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_ACRG3, RpDev->PrivateConfig.AddressOffset),
    (UINT32) ~(B_PCIE_CFG_ACRG3_RRXDME | B_PCIE_CFG_ACRG3_CBGM | B_PCIE_CFG_ACRG3_ADESKEW_DIS),
    0u
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of DMI SERR# Enable.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureSERREnable (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  RpDev->PciPcrAccess->AndThenOr16 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_CMD, RpDev->PrivateConfig.AddressOffset),
    (UINT16)~0,
    (UINT16)(R_PCIE_CFG_CMD)
    );

  RpDev->PciPcrAccess->AndThenOr8 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_BCTRL, RpDev->PrivateConfig.AddressOffset),
    (UINT8)~0,
    (UINT8)(B_PCIE_CFG_BCTRL_SE)
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs configuration of DMI VC Rx flow control.
  Applicable from SIP 17

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureVcRxFlowControl (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_RXQC, RpDev->PrivateConfig.AddressOffset),
    (UINT32)~(B_PCIE_CFG_RXQC_VMRQNPCOC_MASK | B_PCIE_CFG_RXQC_VMRQPCOC_MASK | B_PCIE_CFG_RXQC_V1RQNPCOC_MASK |
              B_PCIE_CFG_RXQC_V1RQPCOC_MASK | B_PCIE_CFG_RXQC_V0RQNPCOC_MASK | B_PCIE_CFG_RXQC_V0RQPCOC_MASK),
    (UINT32)((V_PCIE_CFG_RXQC_VMRQNPCOC << B_PCIE_CFG_RXQC_VMRQNPCOC_OFFSET) |
             (V_PCIE_CFG_RXQC_VMRQPCOC << B_PCIE_CFG_RXQC_VMRQPCOC_OFFSET) |
             (V_PCIE_CFG_RXQC_V1RQNPCOC << B_PCIE_CFG_RXQC_V1RQNPCOC_OFFSET) |
             (V_PCIE_CFG_RXQC_V1RQPCOC << B_PCIE_CFG_RXQC_V1RQPCOC_OFFSET) |
             (V_PCIE_CFG_RXQC_V0RQNPCOC << B_PCIE_CFG_RXQC_V0RQNPCOC_OFFSET) |
             (V_PCIE_CFG_RXQC_V0RQPCOC << B_PCIE_CFG_RXQC_V0RQPCOC_OFFSET))
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs enabling of Thermal Throttling.
  From SIP 17 it should be performed afrer link training to max speed.

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipEnableThermalThrottling (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCH_DMI15_PCR_UPHWAWC, ~0u, B_PCH_DMI15_PCR_UPHWAWC_TSAWEN);
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function performs early PCH-H DMI SIP15 init.
  It must be called before any upstream bus master transactions are enabled

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
PchDmiEarlyInit (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  //
  // Enable Transaction Layer Packet Fast Transmit Mode and
  // DMI Credit Allocated Update Mode
  //
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_MPC2, RpDev->PrivateConfig.AddressOffset),
    ~0u,
    B_PCIE_CFG_MPC2_TLPF | B_PCIE_CFG_MPC2_CAM
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function enables generating SERR interrupt by DMI whenever Stop and Scream is
  triggered due to Parity Error detected on the TLP Header.

  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipConfigureParityCheck (
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    R_PCH_DMI15_PCR_EEPPES,
    ~0u,
    B_PCH_DMI15_PCR_PSSSGE
  );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function configures DMI SIP15.

  @param[in]  PchPcieConfig        Pointer to PCH PCIe config block
  @param[in]  ThermalConfig        Pointer to Thermal Throttling config block
  @param[in]  PchGeneralConfig     Pointer to PCH General config block
  @param[in]  RpDev                Pointer to the root port device
**/
VOID
DmiSipInit (
  IN  PCH_PCIE_CONFIG             *PchPcieConfig,
  IN  THERMAL_CONFIG              *ThermalConfig,
  IN  PCH_GENERAL_CONFIG          *PchGeneralConfig,
  IN  PCIE_ROOT_PORT_DEV          *RpDev
  )
{
  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  DmiSipBasicInit (RpDev);
  DmiSipConfigureClockGating (RpDev);
  DmiSipConfigureLinkPowerManagement (PchPcieConfig, RpDev);
  DmiSipConfigureThermalThrottling (ThermalConfig, RpDev);
  DmiSipConfigureSquelchPowerManagement (RpDev);
  DmiSipConfigurePllShutdown (RpDev);
  DmiSipConfigurePowerGating (PchPcieConfig, RpDev);
  DmiSipConfigureAspm (PchPcieConfig, PchGeneralConfig, RpDev);
  if (RpDev->SipVersion >= PcieSip17) {
    DmiSipConfigureVcRxFlowControl (RpDev);
    DmiSipConfigureSerm (RpDev);
    DmiSipConfigureSERREnable (RpDev);
    DmiSipConfigureElasticBuffer (RpDev);
    DmiSipConfigureL1Support (RpDev);
    DmiSipConfigureD3VnnRemCtl (RpDev);
    DmiSipConfigureMPS (RpDev);
    DmiSipConfigureCoalescing (RpDev);
    DmiSipConfigureRxMargining (RpDev);
    PcieSipConfigureResetPrep (RpDev);
    PcieSipConfigurePowerDownMapping (RpDev);
    DmiSipConfigurePhy (RpDev);
    DmiSipConfigurePhyRecal (RpDev);
    DmiSipConfigureMiscConfiguration (RpDev);
    DmiSipConfigure10BitTagAndScaledFlowControl (RpDev);
    DmiSipDisableTCAliasing (RpDev);
    DmiSipConfigureRxMasterCycleDecoding (RpDev);
    DmiSipSetBiosLockDown (RpDev);
  }
  if (RpDev->PrivateConfig.ParityCheckEnable) {
    DmiSipConfigureParityCheck (RpDev);
  }
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  This function will perform necessary programming before changing PCH DMI link speed to Gen2 or Gen3.

  @param[in]  DmiSpeedGen          PCH DMI speed Gen (1-Gen1, 2-Gen2, 3-Gen3) to which link is going to be trained
  @param[in]  RpDev                Pointer to the root port device
**/
STATIC
VOID
DmiSipBeforeLinkSpeedChange (
  IN  UINT8                             DmiSpeedGen,
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32            Data32Or;
  UINT32            Data32And;
  UINT16            Data16Or;
  UINT16            Data16And;
  UINTN             LaneIndex;
  BOOLEAN           TestEnable;

  static CONST UINT8  DmiUpPortTranPreset[] = {
    7,
    7,
    7,
    7,
    7,
    7,
    7,
    7
  };
  static CONST UINT16 DmiLaneEqualCntrReg[] = {
    R_PCIE_CFG_EX_L01EC,
    R_PCIE_CFG_EX_L23EC,
    R_PCIE_CFG_EX_L45EC,
    R_PCIE_CFG_EX_L67EC
  };
  static CONST UINT8  DmiGen4UpPortTranPreset[] = {
    7,
    7,
    7,
    7,
    7,
    7,
    7,
    7
  };
  static CONST UINT16 DmiGen4LaneEqualCntrReg[] = {
    R_PCIE_CFG_EX_PL16L01EC,
    R_PCIE_CFG_EX_PL16L23EC,
    R_PCIE_CFG_EX_PL16L45EC,
    R_PCIE_CFG_EX_PL16L67EC
  };

  DEBUG ((DEBUG_INFO, "%a() Start\n", __FUNCTION__));
  if (RpDev->SipVersion >= PcieSip17) {
    //
    // Configure Phy.
    // This programming has to be executed before equalization.
    //
    Data32Or = (UINT32) (B_PCIE_RCRB_LPHYCP1_RXEQFNEVC | B_PCIE_RCRB_LPHYCP1_RXADPHM);
    RpDev->PciPcrAccess->AndThenOr32 (RpDev->PciPcrAccess, R_PCIE_RCRB_LPHYCP1, ~0u, Data32Or);
  }

  TestEnable = FALSE;
  if (DmiSpeedGen > 2) {
    for (LaneIndex = 0; LaneIndex < PCH_MAX_DMI_LANES; LaneIndex += 2) {
      Data32And = ~(UINT32) (B_PCIE_CFG_EX_UPL0246TP | B_PCIE_CFG_EX_UPL1357TP);
      Data32Or = (((UINT32)DmiUpPortTranPreset[LaneIndex] << N_PCIE_CFG_EX_UPL0246TP) |
                  ((UINT32)DmiUpPortTranPreset[LaneIndex + 1] << N_PCIE_CFG_EX_UPL1357TP));

      RpDev->PciPcrAccess->AndThenOr32 (
                             RpDev->PciPcrAccess,
                             BuildDmiOffset (DmiLaneEqualCntrReg[LaneIndex/2], RpDev->PrivateConfig.AddressOffset),
                             Data32And,
                             Data32Or
                             );

      if (RpDev->SipVersion >= PcieSip17) {
        Data16And = (UINT16) ~(B_PCIE_CFG_EX_PL16UPL0246TP | B_PCIE_CFG_EX_PL16UPL1357TP);
        Data16Or = (((UINT16)DmiGen4UpPortTranPreset[LaneIndex] << N_PCIE_CFG_EX_PL16UPL0246TP) |
                    ((UINT16)DmiGen4UpPortTranPreset[LaneIndex + 1] << N_PCIE_CFG_EX_PL16UPL1357TP));

        RpDev->PciPcrAccess->AndThenOr16 (
                               RpDev->PciPcrAccess,
                               BuildDmiOffset (DmiGen4LaneEqualCntrReg[LaneIndex/2],
                               RpDev->PrivateConfig.AddressOffset),
                               Data16And,
                               Data16Or
                               );
      }
    }

    SipDmiConfigurePresetCoefficientInitialization (TestEnable, RpDev);
    //
    // Before training to GEN3 Configure Lane0 P0-P10 Preset Coefficient mapping
    // based on recommendation for PCH DMI
    //
    if (IsEbgPch()) {
      //
      // EBG PCH
      //
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_LANE0_PRESETS, RpDev->PrivateConfig.AddressOffset), 0x00D1002F);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P1P2P3PCM, RpDev->PrivateConfig.AddressOffset),   0x37340C8B);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P3P4PCM, RpDev->PrivateConfig.AddressOffset),     0x0003F200);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P5P6PCM, RpDev->PrivateConfig.AddressOffset),     0x08DC01B9);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P6P7P8PCM, RpDev->PrivateConfig.AddressOffset),   0x2F346B00);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P8P9PCM, RpDev->PrivateConfig.AddressOffset),     0x002F4208);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P10PCM, RpDev->PrivateConfig.AddressOffset),      0x0001502A);
      //
      // Configure Lane0 Local PHY Full Swing and Low Frequency Value
      //
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0LFFS, RpDev->PrivateConfig.AddressOffset), 0x00003F15);
    } else if (RpDev->SipVersion >= PcieSip17) {
      //
      // ADP-S PCH
      //
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_LANE0_PRESETS, RpDev->PrivateConfig.AddressOffset), 0x0084A01E);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P1P2P3PCM, RpDev->PrivateConfig.AddressOffset),   0x23200807);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P3P4PCM, RpDev->PrivateConfig.AddressOffset),     0x00028140);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P5P6PCM, RpDev->PrivateConfig.AddressOffset),     0x058C0124);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P6P7P8PCM, RpDev->PrivateConfig.AddressOffset),   0x1E204700);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P8P9PCM, RpDev->PrivateConfig.AddressOffset),     0x001E1145);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P10PCM, RpDev->PrivateConfig.AddressOffset),      0x0000D01B);
      //
      // Configure Lane0 Local PHY Full Swing and Low Frequency Value
      //
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0LFFS, RpDev->PrivateConfig.AddressOffset), 0x0000280E);

      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PX16GP0P1PCM, RpDev->PrivateConfig.AddressOffset),   0x0084A01E);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PX16GP1P2P3PCM, RpDev->PrivateConfig.AddressOffset), 0x23200807);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PX16GP3P4PCM, RpDev->PrivateConfig.AddressOffset),   0x00028140);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PX16GP5P6PCM, RpDev->PrivateConfig.AddressOffset),   0x058C0124);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PX16GP6P7P8PCM, RpDev->PrivateConfig.AddressOffset), 0x1E204700);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PX16GP8P9PCM, RpDev->PrivateConfig.AddressOffset),   0x001E1145);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PX16GP10PCM, RpDev->PrivateConfig.AddressOffset),    0x0000D01B);
      //
      // Configure Lane0 Local PHY Full Swing and Low Frequency Value
      //
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_PX16GLFFS, RpDev->PrivateConfig.AddressOffset), 0x0000280E);
    } else {
      //
      // other PCH
      //
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_LANE0_PRESETS, RpDev->PrivateConfig.AddressOffset), 0x006C8018);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P1P2P3PCM, RpDev->PrivateConfig.AddressOffset),   0x1C180685);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P3P4PCM, RpDev->PrivateConfig.AddressOffset),     0x00020100);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P5P6PCM, RpDev->PrivateConfig.AddressOffset),     0x047000DD);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P6P7P8PCM, RpDev->PrivateConfig.AddressOffset),   0x181835C0);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P8P9PCM, RpDev->PrivateConfig.AddressOffset),     0x0015B104);
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0P10PCM, RpDev->PrivateConfig.AddressOffset),      0x0000A016);
      //
      // Configure Lane0 Local PHY Full Swing and Low Frequency Value
      //
      RpDev->PciPcrAccess->Write32 (RpDev->PciPcrAccess, BuildDmiOffset (R_PCIE_CFG_L0LFFS, RpDev->PrivateConfig.AddressOffset), 0x0000200A);
    }
  }

  ///
  /// Set  DMI Link Data Rate Sustain Policy (DLDRSP)
  /// When set, DMI will always train to the highest supported link speed common to both sides of the link.
  /// DLDRSP should be set to 1 when the Target Link Speed is still indicating GEN1
  ///
  if (RpDev->SipVersion >= PcieSip17) {
    Data32Or = B_PCIE_CFG_CONTROL2_DLDRSP | B_PCIE_CFG_CONTROL2_PMETOFD;
    //
    // Program Link Speed Training Policy
    // and initial Link Speed requirement
    // it has to be done before programming of TLS
    //
    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_AECR1G3, RpDev->PrivateConfig.AddressOffset),
      ~0u,
      (UINT32)(B_PCIE_CFG_AECR1G3_DTCGCM | B_PCIE_CFG_AECR1G3_TPSE)
      );
    RpDev->PciPcrAccess->AndThenOr32 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_MPC2, RpDev->PrivateConfig.AddressOffset),
      (UINT32)~B_PCIE_CFG_MPC2_ORCE,
      (UINT32)((V_PCIE_CFG_MPC2_ORCE_DIS << N_PCIE_CFG_MPC2_ORCE) |
               B_PCIE_CFG_MPC2_PTNFAE | B_PCIE_CFG_MPC2_LSTP | B_PCIE_CFG_MPC2_EOIFD)
      );
  } else {
    Data32Or = B_PCIE_CFG_CONTROL2_DLDRSP;
  }
  RpDev->PciPcrAccess->AndThenOr32 (
    RpDev->PciPcrAccess,
    BuildDmiOffset (R_PCIE_CFG_CONTROL2, RpDev->PrivateConfig.AddressOffset),
    ~0u,
    Data32Or
    );
  DEBUG ((DEBUG_INFO, "%a() End\n", __FUNCTION__));
}

/**
  The function set the Target Link Speed in PCH-H to DMI GEN 3.

  @param[in]  TargetLinkSpeed       Target Link Speed
                                    2: GEN2
                                    3: GEN3
  @param[in]  RpDev                 Pointer to the root port device
**/
VOID
DmiSipSetTargetLinkSpeed (
  IN  UINT8                             TargetLinkSpeed,
  IN  PCIE_ROOT_PORT_DEV                *RpDev
  )
{
  UINT32   MaxLinkSpeed;

  MaxLinkSpeed = RpDev->PciPcrAccess->Read32 (
                                        RpDev->PciPcrAccess,
                                        BuildDmiOffset (R_PCIE_CFG_LCAP, RpDev->PrivateConfig.AddressOffset)
                                        ) & B_PCIE_LCAP_MLS;

  if (RpDev->SipVersion >= PcieSip17) {
    if ((TargetLinkSpeed < 2) || (TargetLinkSpeed > MaxLinkSpeed)) {
      ASSERT (FALSE);
      return;
    }
  } else {
    if ((TargetLinkSpeed < 2) || (TargetLinkSpeed > MaxLinkSpeed)) {
      ASSERT (FALSE);
      return;
    }
    ASSERT (IsPchWithPdmi ());
  }

  DEBUG ((DEBUG_INFO, "PchDmi15SetTargetLinkSpeed(%d) Start\n", TargetLinkSpeed));

  DmiSipBeforeLinkSpeedChange (TargetLinkSpeed, RpDev);

  ///
  /// PCH BIOS Spec, Section 7
  /// Configure DMI Link Speed (for PCH With DMI ONLY)
  /// Please refer to the System Agent BIOS Writer's Guide on Supported Link Speed
  /// field in Link Capabilities register in CPU complex. (Done in SA code)
  /// If target link speed is GEN2, it can be configured as early as possible.
  /// Else if the target link speed is GEN3, refer to PCH BIOS spec for further programming requirement
  ///
  /// Link speed can be set to GEN2/3 if both LCAP in CPU complex (Done in SA code)
  /// and on PCH side indicates such capability.
  /// DMI Target Link Speed (TLS) is configurable on PCH side in Link Control 2 register (LCTL2)
  ///
  if (MaxLinkSpeed >= TargetLinkSpeed) {
    ///
    /// Set Target Link Speed (TLS)
    ///
    RpDev->PciPcrAccess->AndThenOr8 (
                           RpDev->PciPcrAccess,
                           BuildDmiOffset (R_PCIE_CFG_LCTL2, RpDev->PrivateConfig.AddressOffset),
                           (UINT8)~B_PCIE_CFG_LCTL2_TLS,
                           TargetLinkSpeed
                           );
    ///
    /// Please refer to the System Agent BIOS Writer's Guide to perform DMI Link Retrain after
    /// configures new DMI Link Speed. (Done in SA code)
    ///
  }
  if (RpDev->PrivateConfig.TriggerDmiTraining) {
    DEBUG ((DEBUG_INFO, "PchDmi15SetTargetLinkSpeed programming link retrain"));
    RpDev->PciPcrAccess->AndThenOr16 (
      RpDev->PciPcrAccess,
      BuildDmiOffset (R_PCIE_CFG_LCTL, RpDev->PrivateConfig.AddressOffset),
      (UINT16)~(B_PCIE_CFG_LCTL_RL),
      B_PCIE_CFG_LCTL_RL
    );
  }

  if (RpDev->SipVersion >= PcieSip17) {
    DmiSipEnableThermalThrottling (RpDev);
  }
  DEBUG ((DEBUG_INFO, "PchDmi15SetTargetLinkSpeed(%d) End\n", TargetLinkSpeed));
}
