/** @file
  This file contains functions needed for PCIe root port initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Uefi/UefiBaseType.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchInfoLib.h>
#include <PcieRegs.h>
#include <Register/PchPcieRpRegs.h>

#include "PcieRpInitInternal.h"

CONST PCIE_LINK_EQ_COEFFICIENTS  PcieCoeffList[] = {
{6, 8},
{6, 12},
{8, 8}
};

/**
  Get project specific PTM configuration.

  @param[in]  ControllerIndex  Index of the PCIe controller
  @param[out] PciePtmConfig    Pointer to caller allocated PTM configuration
**/
VOID
PcieGetProjectPtmConfiguration (
  IN  UINT32             ControllerIndex,
  OUT PTM_CONFIGURATION  *PciePtmConfig
  )
{
  PciePtmConfig->PtmPipeStageDelay[0] = 0x661A6A1A;
  PciePtmConfig->PtmPipeStageDelay[1] = 0x3E0C611A;
  PciePtmConfig->PtmPipeStageDelay[2] = 0x390C3C0C;
  PciePtmConfig->PtmPipeStageDelay[3] = 0x43164417;
  PciePtmConfig->PtmPipeStageDelay[4] = 0x4016;
  PciePtmConfig->PtmPipeStageDelay[5] = 0x0;
  PciePtmConfig->PtmPipeStageDelay[6] = 0x0;
  PciePtmConfig->PtmPipeStageDelay[7] = 0x0;
  PciePtmConfig->PtmPipeStageDelay[8] = 0x611C611C;
  PciePtmConfig->PtmPipeStageDelay[9] = 0x00005D1C;
  PciePtmConfig->PtmPipeStageDelay[10] = 0x0;
  PciePtmConfig->PtmPipeStageDelay[11] = 0x0;
  PciePtmConfig->PtmPipeStageDelay[12] = 0x0;
  PciePtmConfig->PtmPipeStageDelay[13] = 0x0;

  PciePtmConfig->PtmConfig = 0x40452;
}

/**
  Get project specific Gen4 preset configuration.

  @param[in/out] PlatformSettings    Pointer to caller allocated project equalization configuration
**/
VOID
PcieGetProjectGen4PresetConfiguration (
  OUT PCIE_LINK_EQ_PLATFORM_SETTINGS  *PlatformSettings
  )
{
  PlatformSettings->Ph3PresetList[0] = 4;
  PlatformSettings->Ph3PresetList[1] = 5;
  PlatformSettings->Ph3PresetList[2] = 6;
  PlatformSettings->Ph3PresetList[3] = 0;
  PlatformSettings->Ph3PresetList[4] = 0;
  PlatformSettings->Ph3PresetList[5] = 0;
  PlatformSettings->Ph3PresetList[6] = 0;
  PlatformSettings->Ph3PresetList[7] = 0;
  PlatformSettings->Ph3PresetList[8] = 0;
  PlatformSettings->Ph3PresetList[9] = 0;
  PlatformSettings->Ph3PresetList[10] = 0;
  PlatformSettings->Ph3NumberOfPresetsOrCoefficients = 3;
}

/**
  Get project specific Gen3 preset configuration.

  @param[in/out] PlatformSettings    Pointer to caller allocated project equalization configuration
**/
VOID
PcieGetProjectGen3PresetConfiguration (
  OUT PCIE_LINK_EQ_PLATFORM_SETTINGS  *PlatformSettings
  )
{
  PlatformSettings->Ph3PresetList[0] = 1;
  PlatformSettings->Ph3PresetList[1] = 4;
  PlatformSettings->Ph3PresetList[2] = 5;
  PlatformSettings->Ph3PresetList[3] = 8;
  PlatformSettings->Ph3PresetList[4] = 9;
  PlatformSettings->Ph3PresetList[5] = 0;
  PlatformSettings->Ph3PresetList[6] = 0;
  PlatformSettings->Ph3PresetList[7] = 0;
  PlatformSettings->Ph3PresetList[8] = 0;
  PlatformSettings->Ph3PresetList[9] = 0;
  PlatformSettings->Ph3PresetList[10] = 0;
  PlatformSettings->Ph3NumberOfPresetsOrCoefficients = 5;
}

/**
  Get project specific Gen4 preset to coefficient mapping configuration.

  @param[in/out] ProjectSettings    Pointer to caller allocated project equalization configuration
**/
VOID
PcieGetProjectGen4PresetToCoefficientConfiguration (
  OUT PCIE_LINK_EQ_PROJECT_SETTINGS  *ProjectSettings
  )
{
  ProjectSettings->PresetToCoefficient[0].CursorCoefficient = 30;
  ProjectSettings->PresetToCoefficient[0].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[0].PostCursorCoefficient = 10;
  ProjectSettings->PresetToCoefficient[1].CursorCoefficient = 33;
  ProjectSettings->PresetToCoefficient[1].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[1].PostCursorCoefficient = 7;
  ProjectSettings->PresetToCoefficient[2].CursorCoefficient = 32;
  ProjectSettings->PresetToCoefficient[2].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[2].PostCursorCoefficient = 8;
  ProjectSettings->PresetToCoefficient[3].CursorCoefficient = 35;
  ProjectSettings->PresetToCoefficient[3].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[3].PostCursorCoefficient = 5;
  ProjectSettings->PresetToCoefficient[4].CursorCoefficient = 40;
  ProjectSettings->PresetToCoefficient[4].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[4].PostCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[5].CursorCoefficient = 36;
  ProjectSettings->PresetToCoefficient[5].PreCursorCoefficient = 4;
  ProjectSettings->PresetToCoefficient[5].PostCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[6].CursorCoefficient = 35;
  ProjectSettings->PresetToCoefficient[6].PreCursorCoefficient = 5;
  ProjectSettings->PresetToCoefficient[6].PostCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[7].CursorCoefficient = 28;
  ProjectSettings->PresetToCoefficient[7].PreCursorCoefficient = 4;
  ProjectSettings->PresetToCoefficient[7].PostCursorCoefficient = 8;
  ProjectSettings->PresetToCoefficient[8].CursorCoefficient = 30;
  ProjectSettings->PresetToCoefficient[8].PreCursorCoefficient = 5;
  ProjectSettings->PresetToCoefficient[8].PostCursorCoefficient = 5;
  ProjectSettings->PresetToCoefficient[9].CursorCoefficient = 33;
  ProjectSettings->PresetToCoefficient[9].PreCursorCoefficient = 7;
  ProjectSettings->PresetToCoefficient[9].PostCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[10].CursorCoefficient = 27;
  ProjectSettings->PresetToCoefficient[10].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[10].PostCursorCoefficient = 13;
}

/**
  Get project specific Gen3 preset to coefficient mapping configuration.

  @param[in/out] ProjectSettings    Pointer to caller allocated project equalization configuration
**/
VOID
PcieGetProjectGen3PresetToCoefficientConfiguration (
  OUT PCIE_LINK_EQ_PROJECT_SETTINGS  *ProjectSettings
  )
{
  ProjectSettings->PresetToCoefficient[0].CursorCoefficient = 30;
  ProjectSettings->PresetToCoefficient[0].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[0].PostCursorCoefficient = 10;
  ProjectSettings->PresetToCoefficient[1].CursorCoefficient = 33;
  ProjectSettings->PresetToCoefficient[1].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[1].PostCursorCoefficient = 7;
  ProjectSettings->PresetToCoefficient[2].CursorCoefficient = 32;
  ProjectSettings->PresetToCoefficient[2].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[2].PostCursorCoefficient = 8;
  ProjectSettings->PresetToCoefficient[3].CursorCoefficient = 35;
  ProjectSettings->PresetToCoefficient[3].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[3].PostCursorCoefficient = 5;
  ProjectSettings->PresetToCoefficient[4].CursorCoefficient = 40;
  ProjectSettings->PresetToCoefficient[4].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[4].PostCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[5].CursorCoefficient = 36;
  ProjectSettings->PresetToCoefficient[5].PreCursorCoefficient = 4;
  ProjectSettings->PresetToCoefficient[5].PostCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[6].CursorCoefficient = 35;
  ProjectSettings->PresetToCoefficient[6].PreCursorCoefficient = 5;
  ProjectSettings->PresetToCoefficient[6].PostCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[7].CursorCoefficient = 28;
  ProjectSettings->PresetToCoefficient[7].PreCursorCoefficient = 4;
  ProjectSettings->PresetToCoefficient[7].PostCursorCoefficient = 8;
  ProjectSettings->PresetToCoefficient[8].CursorCoefficient = 30;
  ProjectSettings->PresetToCoefficient[8].PreCursorCoefficient = 5;
  ProjectSettings->PresetToCoefficient[8].PostCursorCoefficient = 5;
  ProjectSettings->PresetToCoefficient[9].CursorCoefficient = 33;
  ProjectSettings->PresetToCoefficient[9].PreCursorCoefficient = 7;
  ProjectSettings->PresetToCoefficient[9].PostCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[10].CursorCoefficient = 27;
  ProjectSettings->PresetToCoefficient[10].PreCursorCoefficient = 0;
  ProjectSettings->PresetToCoefficient[10].PostCursorCoefficient = 13;
}

/**
  Get the project recommended equalization settings

  @param[in] RpIndex                        Index of the PCI root port
  @param[in] PcieGen3LinkEqSettings         Pointer to the Gen3 equalization config
  @param[in] PcieGen4LinkEqSettings         Pointer to the Gen4 equalization config
  @param[in] PcieGen5LinkEqSettings         Pointer to the Gen5 equalization config
**/
VOID
PcieGetProjectDefaultEqConfiguration (
  IN  UINT32                 RpIndex,
  OUT PCIE_LINK_EQ_SETTINGS  *PcieGen3LinkEqSettings,
  OUT PCIE_LINK_EQ_SETTINGS  *PcieGen4LinkEqSettings,
  OUT PCIE_LINK_EQ_SETTINGS  *PcieGen5LinkEqSettings
  )
{
  PcieGen3LinkEqSettings->PlatformSettings.PcieLinkEqMethod = PcieLinkHardwareEq;
  PcieGen3LinkEqSettings->PlatformSettings.PcieLinkEqMode = PcieLinkEqPresetMode;
  PcieGen3LinkEqSettings->PlatformSettings.Ph1DownstreamPortTransmitterPreset = 7;
  PcieGen3LinkEqSettings->PlatformSettings.Ph1UpstreamPortTransmitterPreset = 7;
  PcieGen3LinkEqSettings->ProjectSettings.PipePresetToCoefficientQuerySupported = FALSE;
  if (PcieGen3LinkEqSettings->PlatformSettings.Ph3NumberOfPresetsOrCoefficients <= 5) {
    PcieGen3LinkEqSettings->PlatformSettings.PCETTimer = 4;
  } else if (PcieGen3LinkEqSettings->PlatformSettings.Ph3NumberOfPresetsOrCoefficients <= 7) {
    PcieGen3LinkEqSettings->PlatformSettings.PCETTimer = 3;
  } else if (PcieGen3LinkEqSettings->PlatformSettings.Ph3NumberOfPresetsOrCoefficients <= 8) {
    PcieGen3LinkEqSettings->PlatformSettings.PCETTimer = 2;
  }
  PcieGen3LinkEqSettings->ProjectSettings.FullSwingValue = 0x28;
  PcieGen3LinkEqSettings->ProjectSettings.LowFrequencyValue = 0xE;
  PcieGen3LinkEqSettings->PlatformSettings.EqPh3Bypass = 0;
  PcieGen3LinkEqSettings->PlatformSettings.EqPh23Bypass = 0;
  PcieGen3LinkEqSettings->PlatformSettings.TsLockTimer = 0;
  PcieGetProjectGen3PresetConfiguration (&PcieGen3LinkEqSettings->PlatformSettings);
  PcieGetProjectGen3PresetToCoefficientConfiguration (&PcieGen3LinkEqSettings->ProjectSettings);

  PcieGen4LinkEqSettings->PlatformSettings.PcieLinkEqMethod = PcieLinkHardwareEq;
  PcieGen4LinkEqSettings->PlatformSettings.PcieLinkEqMode = PcieLinkEqPresetMode;
  PcieGen4LinkEqSettings->PlatformSettings.Ph1DownstreamPortTransmitterPreset = 8;
  PcieGen4LinkEqSettings->PlatformSettings.Ph1UpstreamPortTransmitterPreset = 8;
  PcieGen4LinkEqSettings->ProjectSettings.PipePresetToCoefficientQuerySupported = FALSE;
  if (PcieGen4LinkEqSettings->PlatformSettings.Ph3NumberOfPresetsOrCoefficients <= 5) {
    PcieGen4LinkEqSettings->PlatformSettings.PCETTimer = 4;
  } else if (PcieGen4LinkEqSettings->PlatformSettings.Ph3NumberOfPresetsOrCoefficients <= 7) {
    PcieGen4LinkEqSettings->PlatformSettings.PCETTimer = 3;
  } else if (PcieGen4LinkEqSettings->PlatformSettings.Ph3NumberOfPresetsOrCoefficients <= 8) {
    PcieGen4LinkEqSettings->PlatformSettings.PCETTimer = 2;
  }
  PcieGen4LinkEqSettings->ProjectSettings.FullSwingValue = 0x28;
  PcieGen4LinkEqSettings->ProjectSettings.LowFrequencyValue = 0xE;
  PcieGen4LinkEqSettings->PlatformSettings.EqPh3Bypass = 0;
  PcieGen4LinkEqSettings->PlatformSettings.EqPh23Bypass = 0;
  PcieGen4LinkEqSettings->PlatformSettings.TsLockTimer = 0;
  PcieGetProjectGen4PresetConfiguration (&PcieGen4LinkEqSettings->PlatformSettings);
  PcieGetProjectGen4PresetToCoefficientConfiguration (&PcieGen4LinkEqSettings->ProjectSettings);
}

/**
  Loads the default register value.

  @param[in]  RpBase              Root Port pci segment base address
  @param[in]  RegOffset           Offset of the register
  @param[out] DataAndMask         Mask value for the register
  @param[out] DataOr              Values which need to be set
  @param[in]  PcieRpCommonConfig  Pointer to a PCIE_ROOT_PORT_COMMON_CONFIG
                                  that provides the platform setting.
**/
EFI_STATUS
LoadDefaultRegValues (
  IN  UINT64                              RpBase,
  IN  UINT16                              RegOffset,
  OUT UINT32                              *DataAndMask,
  OUT UINT32                              *DataOr,
  IN  CONST PCIE_ROOT_PORT_COMMON_CONFIG  *PcieRpCommonConfig  OPTIONAL
  )
{
  if ((DataAndMask == NULL) || (DataOr == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  switch (RegOffset) {
    case R_PCH_PCIE_CFG_EX_L1SCAP:
      *DataAndMask = PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_EX_L1SCAP);
      if (PcieRpCommonConfig != NULL) {
        if (PcieRpCommonConfig->L1Substates==PchPcieL1SubstatesDisabled) {
          *DataAndMask &= (UINT32)~(0x1F);
        } else if (PcieRpCommonConfig->L1Substates==PchPcieL1SubstatesL1_1) {
          *DataAndMask &= (UINT32)~(BIT0|BIT2);
        }
      } else {
        *DataAndMask &= (UINT32)~(0x1F);
      }
      //
      // Set TpowerOn capability to 44us
      // [1208708498]
      //
      *DataAndMask &= ~(B_PCIE_EX_L1SCAP_PTV | B_PCIE_EX_L1SCAP_PTPOS | B_PCIE_EX_L1SCAP_CMRT);
      *DataOr = (0x15 << N_PCIE_EX_L1SCAP_PTV) | (V_PCIE_EX_L1SCAP_PTPOS_2us << N_PCIE_EX_L1SCAP_PTPOS) | B_PCIE_EX_L1SCAP_L1SSES | (0x3C << N_PCIE_EX_L1SCAP_CMRT);
      break;
    default:
      DEBUG ((DEBUG_ERROR, "Invalid register offset\n"));
      ASSERT (FALSE);
      return EFI_INVALID_PARAMETER;
  }
  return EFI_SUCCESS;
}

/**
  Determine if Vnn Removal register programming is supported

  @retval TRUE                    Supported
  @retval FALSE                   Not supported
**/
BOOLEAN
IsVnnRemovalSupported (
  VOID
  )
{
  return FALSE;
}

/**
  Returns the SIP version of the PCIe root port.

  @param[in] RpIndex  Index of the root port in PCH

  @return Version of the SIP
**/
PCIE_SIP_VERSION
PcieRpGetSipVer (
  IN UINT32  RpIndex
  )
{
  if (IsPchS ()) {
    return PcieSip17;
  } else {
    return PcieSip14;
  }
}

/**
  Get the FID number of the PCIe root port.

  @param[in] RpIndex  Index of the root port

  @return FID number to use when sending SBI msg
**/
UINT16
PcieRpGetSbiFid (
  IN UINT32  RpIndex
  )
{
  return (UINT16)(RpIndex % 4);
}

