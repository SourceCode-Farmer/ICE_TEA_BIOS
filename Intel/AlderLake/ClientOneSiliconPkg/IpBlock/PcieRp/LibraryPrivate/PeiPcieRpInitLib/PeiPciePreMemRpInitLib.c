/** @file
  This file contains functions that initializes PCI Express Root Ports of PCH in PreMem phase.

@copyright
  INTEL CONFIDENTIAL

  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPcieRpLib.h>
#include <Library/PeiHsioLib.h>
#include <Library/HsioSocLib.h>
#include <Library/PeiPcieRpLib.h>
#include <PcieRegs.h>
#include <Register/PchPcieRpRegs.h>
#include <Register/PcieSipRegs.h>
#include <Library/PchFiaLib.h>
#include <Library/PsfSocLib.h>
#include <Library/PchPciBdfLib.h>

#include "PcieRpInitInternal.h"

/**
  This function set Block Transition From Detect State

  @param[in] RpBase      Root Port pci segment base address
**/
VOID
PcieRpEnableBlockTransitionState (
  IN     UINT64  RpBase
  )
{
  PciSegmentAnd32 (RpBase + R_PCH_PCIE_CFG_PCIEALC, (UINT32) ~B_PCH_PCIE_CFG_PCIEALC_BLKDQDA);
}

/**
  This function determines whether root port is configured in non-common clock mode.
  Result is based on the NCC soft-strap setting.

  @param[in] RpBase      Root Port pci segment base address

  @retval TRUE           Port in NCC SSC mode.
  @retval FALSE          Port not in NCC SSC mode.
**/
BOOLEAN
IsPcieNcc (
  IN     UINT64  RpBase
  )
{
  if (PciSegmentRead16 (RpBase + R_PCIE_CFG_LSTS) & B_PCIE_LSTS_SCC) {
    return FALSE;
  } else {
    return TRUE;
  }
}

/**
  Initialize non-common clock port for SIP 16 and SIP 17 controllers

  @param[in] RpIndex    Root Port index
**/
VOID
PcieSip17InitNccPort (
  IN UINT32   RpIndex
  )
{
  UINT64     RpBase;
  UINT32     Data32Or;
  UINT32     Data32And;

  DEBUG ((DEBUG_INFO, "PcieSip17InitNccPort(%d)\n", RpIndex+1));

  if (PcieRpGetSipVer (RpIndex) < PcieSip17) {
    return;
  }
  RpBase = PchPcieRpPciCfgBase (RpIndex);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_CFG_LCAP, (UINT32) ~B_PCIE_LCAP_APMS, B_PCIE_LCAP_APMS_L1);
  Data32Or = B_PCH_PCIE_CFG_MPC2_ASPMCOEN | V_PCH_PCIE_CFG_MPC2_ASPMCO_L1;
  PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_MPC2, Data32Or);
  PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_MPC, B_PCH_PCIE_CFG_MPC_UCEL);
  PciSegmentOr32 (RpBase + R_PCIE_CFG_LCAP, B_PCIE_LCAP_EL1);
  Data32And = (UINT32) ~(B_PCIE_LCAP2_LSOSRSS | B_PCIE_LCAP2_LSOSGSSV);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_CFG_LCAP2, Data32And, 0xF << N_PCIE_LCAP2_LSOSGSSV);
  PciSegmentAnd32 (RpBase + R_PCH_PCIE_CFG_PCIEALC, (UINT32) ~B_PCH_PCIE_CFG_PCIEALC_BLKDQDA);
}
/**
  Hide rootports disabled by policy. This needs to be done in premem,
  because graphics init from SystemAgent code depends on those ports
  being already hidden

  @param[in] PcieRpPreMemConfig   Platform policy
**/
VOID
PcieRpEarlyDisabling (
  IN PCH_PCIE_RP_PREMEM_CONFIG *PcieRpPreMemConfig
  )
{
  UINT32 RpIndex;
  for (RpIndex = 0; RpIndex < GetPchMaxPciePortNum (); RpIndex++) {
    if ((PcieRpPreMemConfig->RpEnabledMask & (UINT32) (1 << RpIndex)) == 0) {
      PsfDisablePcieRootPort (PsfGetPciePortData (RpIndex), PsfGetSegmentTable ());
    }
  }
}

/**
  Initializes ports with NonCommonClock and SSC configuration.
**/
VOID
PcieRpNccWithSscHandling (
  VOID
  )
{
  UINT32 RpIndex;

  for (RpIndex = 0; RpIndex < GetPchMaxPciePortNum (); RpIndex++) {
    if (IsPcieNcc (PchPcieRpPciCfgBase (RpIndex))) {
      PcieSip17InitNccPort (RpIndex);
    }
  }
}
