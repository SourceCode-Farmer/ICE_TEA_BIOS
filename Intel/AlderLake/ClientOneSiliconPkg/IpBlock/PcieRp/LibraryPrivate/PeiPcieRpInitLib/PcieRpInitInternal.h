/** @file
  Common header for PcieRpInitLib

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PEI_PCIE_RP_INIT_INTERNAL_H_
#define _PEI_PCIE_RP_INIT_INTERNAL_H_

#include <PchPcieRpConfig.h>
#include <Library/PeiPcieSipInitLib.h>
#include <Library/PchSbiAccessLib.h>
#include <Library/PciSegmentRegAccessLib.h>

typedef struct {
  PCIE_ROOT_PORT_DEV  RpDev;
  PCI_SEGMENT_REG_ACCESS         PciSegmentRegAccess;
  P2SB_SIDEBAND_REGISTER_ACCESS  P2SbMsgCfgAccess;
  P2SB_SIDEBAND_REGISTER_ACCESS  P2SbMsgMemAccess;
  P2SB_CONTROLLER                P2SbController;
} PCIE_ROOT_PORT_DEV_PRIVATE;

/**
  Create PCIE_ROOT_PORT_DEV_PRIVATE object for PCIe root port

  @param[in]  RpIndex       Index of the root port on the PCH
  @param[out] RpDevPrivate  On output pointer to the valid PCIE_ROOT_PORT_DEV_PRIVATE
**/
VOID
PcieGetRpDev (
  IN UINT32                       RpIndex,
  OUT PCIE_ROOT_PORT_DEV_PRIVATE  *RpDevPrivate
  );

/**
  Create PCIE_ROOT_PORT_DEV_PRIVATE object for PCIe controller

  @param[in]  ControllerIndex  Index of the PCIe controller on the PCH
  @param[out] RpDevPrivate     On output pointer to the valid PCIE_ROOT_PORT_DEV_PRIVATE
**/
VOID
PcieGetControllerDev (
  IN  UINT32                      ControllerIndex,
  OUT PCIE_ROOT_PORT_DEV_PRIVATE  *RpDevPrivate
  );

/**
  Get project specific PTM configuration.

  @param[in]  ControllerIndex  Index of the PCIe controller
  @param[out] PciePtmConfig    Pointer to caller allocated PTM configuration
**/
VOID
PcieGetProjectPtmConfiguration (
  IN  UINT32             ControllerIndex,
  OUT PTM_CONFIGURATION  *PciePtmConfig
  );

/**
  Get the project recommended equalization settings

  @param[in] RpIndex                        Index of the PCI root port
  @param[in] PcieGen3LinkEqSettings         Pointer to the equalization config
  @param[in] PcieGen4LinkEqSettings         Pointer to the equalization config
  @param[in] PcieGen5LinkEqSettings         Pointer to the equalization config
**/
VOID
PcieGetProjectDefaultEqConfiguration (
  IN  UINT32                         RpIndex,
  OUT PCIE_LINK_EQ_SETTINGS          *PcieGen3LinkEqSettings,
  OUT PCIE_LINK_EQ_SETTINGS          *PcieGen4LinkEqSettings,
  OUT PCIE_LINK_EQ_SETTINGS          *PcieGen5LinkEqSettings
  );

/**
  Performs PCIe link equalization according to platform settings.
  Please make sure that link is in gen1 or gen2 before calling this
  procedure.

  @param[in] RpBase              PCI base of the RP
  @param[in] Gen3DeviceAttached  Gen3 capable device is attached to the root port
  @param[in] PcieLinkEqSettings  PCIe link equalization settings to be used during EQ
**/
VOID
PcieLinkEqualize (
  IN UINT64                 RpBase,
  IN BOOLEAN                Gen3DeviceAttached,
  IN PCIE_LINK_EQ_SETTINGS  *PcieLinkEqSettings
  );

/**
  Loads the default register value.

  @param[in]  RpBase              Root Port pci segment base address
  @param[in]  RegOffset           Offset of the register
  @param[out] DataAndMask         Mask value for the register
  @param[out] DataOr              Values which need to be set
  @param[in]  PcieRpCommonConfig  Pointer to a PCIE_ROOT_PORT_COMMON_CONFIG
                                  that provides the platform setting.
**/
EFI_STATUS
LoadDefaultRegValues (
  IN  UINT64                              RpBase,
  IN  UINT16                              RegOffset,
  OUT UINT32                              *DataAndMask,
  OUT UINT32                              *DataOr,
  IN  CONST PCIE_ROOT_PORT_COMMON_CONFIG  *PcieRpCommonConfig  OPTIONAL
);

/**
  Determine if Vnn Removal register programming is supported

  @retval TRUE                    Supported
  @retval FALSE                   Not supported
**/
BOOLEAN
IsVnnRemovalSupported (
  VOID
  );

/**
  Performs the initialization for hardware equalization.
  Please make sure this function is called before programming any
  power management register in PCIe Root Port

  @param[in] RpBase              PCI base of the RP
  @param[in] PcieLinkEqSettings  PCIe link equalization settings to be used during EQ
**/
VOID
PcieLinkEqualizeInit (
  IN UINT64                         RpBase,
  IN PCIE_LINK_EQ_SETTINGS          *PcieLinkEqSettings
);

/**
  Returns the SIP version of the PCIe root port.

  @param[in] RpIndex  Index of the root port in PCH

  @return Version of the SIP
**/
PCIE_SIP_VERSION
PcieRpGetSipVer (
  IN UINT32  RpIndex
  );

/**
  Get the FID number of the PCIe root port.

  @param[in] RpIndex  Index of the root port

  @return FID number to use when sending SBI msg
**/
UINT16
PcieRpGetSbiFid (
  IN UINT32  RpIndex
  );

#endif

