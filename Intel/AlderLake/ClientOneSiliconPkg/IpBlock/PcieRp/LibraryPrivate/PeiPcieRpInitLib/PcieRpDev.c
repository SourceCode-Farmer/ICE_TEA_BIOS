/** @file
  This file contains functions needed to create PCIE_ROOT_PORT_DEV structure.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Register/PchRegs.h>
#include <Library/DebugLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchPcieRpLib.h>
#include <PchPcieRpInfo.h>
#include <PchBdfAssignment.h>
#include <Library/P2SbSidebandAccessLib.h>
#include <Library/PciSegmentRegAccessLib.h>

#include "PcieRpInitInternal.h"

/**
  Create PCIE_ROOT_PORT_DEV_PRIVATE object for PCIe root port

  @param[in]  RpIndex         Index of the root port on the PCH
  @param[out] RpDevPrivate    On output pointer to the valid PCIE_ROOT_PORT_DEV_PRIVATE
**/
VOID
PcieGetRpDev (
  IN UINT32                       RpIndex,
  OUT PCIE_ROOT_PORT_DEV_PRIVATE  *RpDevPrivate
  )
{
  PCIE_ROOT_PORT_DEV  *RpDev;

  RpDev = &RpDevPrivate->RpDev;

  RpDev->SipVersion = PcieRpGetSipVer (RpIndex);

  RpDev->Integration = PchPcie;

  RpDev->Sbdf.Segment = DEFAULT_PCI_SEGMENT_NUMBER_PCH;
  RpDev->Sbdf.Bus = DEFAULT_PCI_BUS_NUMBER_PCH;
  RpDev->Sbdf.Device = PchPcieRpDevNumber (RpIndex);
  RpDev->Sbdf.Function = PchPcieRpFuncNumber (RpIndex);
  BuildPciSegmentAccess (PchPcieRpPciCfgBase (RpIndex), &RpDevPrivate->PciSegmentRegAccess);
  RpDev->PciCfgAccess = &RpDevPrivate->PciSegmentRegAccess.RegAccess;

  RpDevPrivate->P2SbController.PciCfgBaseAddr = PCI_SEGMENT_LIB_ADDRESS (
                                                  DEFAULT_PCI_SEGMENT_NUMBER_PCH,
                                                  DEFAULT_PCI_BUS_NUMBER_PCH,
                                                  PCI_DEVICE_NUMBER_PCH_P2SB,
                                                  PCI_FUNCTION_NUMBER_PCH_P2SB,
                                                  0
                                                  );
  RpDevPrivate->P2SbController.Mmio = PCH_PCR_BASE_ADDRESS;
  RpDevPrivate->P2SbController.HpetMmio = 0;

  BuildP2SbSidebandAccess (
    &RpDevPrivate->P2SbController,
    GetRpSbiPid (RpIndex),
    PcieRpGetSbiFid (RpIndex),
    P2SbPciConfig,
    P2SbMsgAccess,
    FALSE,
    &RpDevPrivate->P2SbMsgCfgAccess
    );
  RpDev->PciSbiMsgCfgAccess = &RpDevPrivate->P2SbMsgCfgAccess.Access;

  BuildP2SbSidebandAccess (
    &RpDevPrivate->P2SbController,
    GetRpSbiPid (RpIndex),
    PcieRpGetSbiFid (RpIndex),
    P2SbMemory,
    P2SbMsgAccess,
    TRUE,
    &RpDevPrivate->P2SbMsgMemAccess
    );
  RpDev->PciSbiMsgMemAccess = &RpDevPrivate->P2SbMsgMemAccess.Access;
}

/**
  Create PCIE_ROOT_PORT_DEV_PRIVATE object for PCIe controller

  @param[in]  ControllerIndex  Index of the PCIe controller on the PCH
  @param[out] RpDevPrivate     On output pointer to the valid PCIE_ROOT_PORT_DEV_PRIVATE
**/
VOID
PcieGetControllerDev (
  IN  UINT32                      ControllerIndex,
  OUT PCIE_ROOT_PORT_DEV_PRIVATE  *RpDevPrivate
  )
{
  UINT32  RpIndex;

  RpIndex = ControllerIndex * PCH_PCIE_CONTROLLER_PORTS;

  PcieGetRpDev (RpIndex, RpDevPrivate);
}

