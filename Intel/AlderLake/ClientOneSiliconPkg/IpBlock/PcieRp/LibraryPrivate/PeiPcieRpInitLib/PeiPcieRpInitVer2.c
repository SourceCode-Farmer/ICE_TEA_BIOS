/** @file
  This file contains functions needed for PCIe root port initialization
  specific to Ver2 project.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Uefi/UefiBaseType.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPciBdfLib.h>
#include <PcieRegs.h>
#include <Register/PchPcieRpRegs.h>

#include "PcieRpInitInternal.h"

/**
  Get project specific PTM configuration.

  @param[in]  ControllerIndex  Index of the PCIe controller
  @param[out] PciePtmConfig    Pointer to caller allocated PTM configuration
**/
VOID
PcieGetProjectPtmConfiguration (
  IN  UINT32             ControllerIndex,
  OUT PTM_CONFIGURATION  *PciePtmConfig
  )
{
  //
  // First controller's lanes are ML phy
  // so recommendation for ML phy is used.
  //
  if (ControllerIndex == 0) {
    PciePtmConfig->PtmPipeStageDelay[0] = 0x240B2B07;
    PciePtmConfig->PtmPipeStageDelay[1] = 0x1B09200C;
    PciePtmConfig->PtmPipeStageDelay[2] = 0x170B180B;
    PciePtmConfig->PtmPipeStageDelay[3] = 0x190B1C09;
    PciePtmConfig->PtmPipeStageDelay[4] = 0x190B;
  } else {
  //
  // Other PCIe controllers use non ML phy. Non ML recommendation
  // is used.
  //
    PciePtmConfig->PtmPipeStageDelay[0] = 0x250C2C08;
    PciePtmConfig->PtmPipeStageDelay[1] = 0x1C08210C;
    PciePtmConfig->PtmPipeStageDelay[2] = 0x160C180B;
    PciePtmConfig->PtmPipeStageDelay[3] = 0x180A1B08;
    PciePtmConfig->PtmPipeStageDelay[4] = 0x180C;
  }

  PciePtmConfig->PtmConfig = 0x40252;
}

/**
  Get the project recommended equalization settings

  @param[in] RpIndex                        Index of the PCI root port
  @param[in] PcieGen3LinkEqSettings         Pointer to the Gen3 equalization config
  @param[in] PcieGen4LinkEqSettings         Pointer to the Gen4 equalization config
  @param[in] PcieGen5LinkEqSettings         Pointer to the Gen5 equalization config
**/
VOID
PcieGetProjectDefaultEqConfiguration (
  IN  UINT32                 RpIndex,
  OUT PCIE_LINK_EQ_SETTINGS  *PcieLinkEqSettings,
  OUT PCIE_LINK_EQ_SETTINGS  *PcieGen4LinkEqSettings,
  OUT PCIE_LINK_EQ_SETTINGS  *PcieGen5LinkEqSettings
  )
{
  STATIC CONST PCIE_LINK_EQ_COEFFICIENTS  PcieCoeffList[] = {
    {6, 8},
    {6, 12},
    {8, 8}
  };

  PcieLinkEqSettings->PlatformSettings.PcieLinkEqMethod = PcieLinkHardwareEq;
  PcieLinkEqSettings->PlatformSettings.PcieLinkEqMode = PcieLinkEqCoefficientMode;
  if (ARRAY_SIZE (PcieCoeffList) <= ARRAY_SIZE (PcieLinkEqSettings->PlatformSettings.Ph3CoefficientsList)) {
    PcieLinkEqSettings->PlatformSettings.Ph3NumberOfPresetsOrCoefficients = ARRAY_SIZE (PcieCoeffList);
    CopyMem (PcieLinkEqSettings->PlatformSettings.Ph3CoefficientsList, PcieCoeffList, sizeof (PcieCoeffList));
  }
  PcieLinkEqSettings->PlatformSettings.Ph1DownstreamPortTransmitterPreset = 7;
  PcieLinkEqSettings->PlatformSettings.Ph1UpstreamPortTransmitterPreset = 5;
  if (PcieLinkEqSettings->PlatformSettings.Ph3NumberOfPresetsOrCoefficients <= 5) {
    PcieLinkEqSettings->ProjectSettings.PresetCoefficientEvaluationTimeMs = 4;
  } else if (PcieLinkEqSettings->PlatformSettings.Ph3NumberOfPresetsOrCoefficients <= 7) {
    PcieLinkEqSettings->ProjectSettings.PresetCoefficientEvaluationTimeMs = 0;
  } else {
    PcieLinkEqSettings->ProjectSettings.PresetCoefficientEvaluationTimeMs = 2;
  }
  PcieLinkEqSettings->ProjectSettings.PipePresetToCoefficientQuerySupported = TRUE;
}

/**
  Loads the default register value.

  @param[in]  RpBase              Root Port pci segment base address
  @param[in]  RegOffset           Offset of the register
  @param[out] DataAndMask         Mask value for the register
  @param[out] DataOr              Values which need to be set
  @param[in]  PcieRpCommonConfig  Pointer to a PCIE_ROOT_PORT_COMMON_CONFIG
                                  that provides the platform setting.
**/
EFI_STATUS
LoadDefaultRegValues (
  IN  UINT64                              RpBase,
  IN  UINT16                              RegOffset,
  OUT UINT32                              *DataAndMask,
  OUT UINT32                              *DataOr,
  IN  CONST PCIE_ROOT_PORT_COMMON_CONFIG  *PcieRpCommonConfig  OPTIONAL
)
{
  if ((DataAndMask == NULL) || (DataOr == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  switch (RegOffset) {
    case R_PCH_PCIE_CFG_EX_L1SCAP:
      *DataAndMask = PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_EX_L1SCAP);
      if (PcieRpCommonConfig != NULL) {
        if (PcieRpCommonConfig->L1Substates==PchPcieL1SubstatesDisabled) {
          *DataAndMask &= (UINT32)~(0x1F);
        } else if (PcieRpCommonConfig->L1Substates==PchPcieL1SubstatesL1_1) {
          *DataAndMask &= (UINT32)~(BIT0|BIT2);
        }
      } else {
        *DataAndMask &= (UINT32)~(0x1F);
      }
      //
      // Set TpowerOn capability to 44us
      // [1208708498]
      //
      *DataAndMask &= ~(B_PCIE_EX_L1SCAP_PTV | B_PCIE_EX_L1SCAP_PTPOS);
      *DataOr = (22 << N_PCIE_EX_L1SCAP_PTV) | (V_PCIE_EX_L1SCAP_PTPOS_2us << N_PCIE_EX_L1SCAP_PTPOS) | B_PCIE_EX_L1SCAP_L1SSES;
      break;
    default:
      DEBUG ((DEBUG_ERROR, "Invalid register offset\n"));
      ASSERT (FALSE);
      return EFI_INVALID_PARAMETER;

  }
  return EFI_SUCCESS;
}

/**
  Determine if Vnn Removal register programming is supported

  @retval TRUE                    Supported
  @retval FALSE                   Not supported
**/
BOOLEAN
IsVnnRemovalSupported (
  VOID
)
{
  return TRUE;
}

/**
  Returns the SIP version of the PCIe root port.

  @param[in] RpIndex  Index of the root port in PCH

  @return Version of the SIP
**/
PCIE_SIP_VERSION
PcieRpGetSipVer (
  IN UINT32  RpIndex
  )
{
  return PcieSip14;
}

/**
  Get the FID number of the PCIe root port.

  @param[in] RpIndex  Index of the root port

  @return FID number to use when sending SBI msg
**/
UINT16
PcieRpGetSbiFid (
  IN UINT32  RpIndex
  )
{
  return (UINT16)(PchPcieRpDevNumber (RpIndex) << 3) | (RpIndex % 4);
}

