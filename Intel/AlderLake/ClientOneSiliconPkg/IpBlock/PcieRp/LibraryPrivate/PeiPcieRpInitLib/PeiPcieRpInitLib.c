/** @file
  This file contains functions that initializes PCI Express Root Ports of PCH.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HobLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/TimerLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/PcieRpLib.h>
#include <Library/PciExpressHelpersLib.h>
#include <Ppi/SiPolicy.h>
#include <IndustryStandard/Pci30.h>
#include <PchLimits.h>
#include <Library/PeiPchPcieClocksLib.h>
#include <Library/PchFiaLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchPolicyLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PchSbiAccessLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PsfSocLib.h>
#include <Library/PchPcieRpLib.h>
#include <Library/PmcPrivateLib.h>
#include <Library/PeiItssLib.h>
#include <Library/PcdLib.h>
#include <Uefi/UefiBaseType.h>
#include <Library/PchDmiLib.h>
#include <Library/PeiHsioLib.h>
#include <Library/PeiPcieRpLib.h>
#include <Library/PmcLib.h>
#include <PcieRegs.h>
#include <Register/PchRegs.h>
#include <Register/PchPcieRpRegs.h>
#include <Register/PcieSipRegs.h>
#include <PchPcieRpInfo.h>
#include <Library/PcieHelperLib.h>
#include <PciePreMemConfig.h>
#include <Library/PeiPcieRpInitLib.h>
#include <Library/PeiPcieSipInitLib.h>
#include <Library/PeiHybridStorageLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PmcSocLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/FiaSocLib.h>

#include "PcieRpInitInternal.h"

#define LINK_ACTIVE_POLL_INTERVAL 100     // in microseconds
#define LINK_ACTIVE_POLL_TIMEOUT  1000000 // in microseconds

#define TIMEOUT      5000

#define BUS_NUMBER_FOR_IMR 0x00

/**
  PCIe controller configuration strings.
**/
GLOBAL_REMOVE_IF_UNREFERENCED CONST CHAR8* mPcieControllerConfigName[] = {
  "4x1",
  "1x2-2x1",
  "2x2",
  "1x4"
};

/**
  Calculates the index of the first port on the same controller.

  @param[in] RpIndex     Root Port Number (0-based)

  @retval Index of the first port on the first controller.
**/
UINT32
PchGetPcieFirstPortIndex (
  IN     UINT32  RpIndex
  )
{
  UINT32  ControllerIndex;

  ControllerIndex = RpIndex / PCH_PCIE_CONTROLLER_PORTS;
  return ControllerIndex * PCH_PCIE_CONTROLLER_PORTS;
}

/**
  Checks if lane reversal is enabled on a given Root Port

  @param[in] RpIndex  Root Port index (0-based)

  @retval TRUE if lane reversal is enbabled, FALSE otherwise
**/
BOOLEAN
IsPcieLaneReversalEnabled (
  IN UINT32  RpIndex
  )
{
  UINT32  Data32;
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;

  PcieGetRpDev (PchGetPcieFirstPortIndex (RpIndex), &RpDevPrivate);

  Data32 = RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Read32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCIEDBG);
  return !! (Data32 & B_PCH_PCIE_CFG_PCIEDBG_LR);
}

/**
  Translate PCIe Root Port / Lane pair to 0-based PCIe lane number.
  Note: Returns first port lane if the port is multilane port

  @param[in] RpIndex    Root Port index
  @param[in] RpLane     Root Port Lane (0-3)

  @retval PCIe lane number (0-based)
**/
UINT32
PchPciePhysicalLane (
  UINT32 RpIndex,
  UINT32 RpLane
  )
{
  UINT32  ControllerIndex;
  UINT32  ControllerLane;

  ASSERT (RpIndex < GetPchMaxPciePortNum ());
  ASSERT (((RpIndex % 4) + RpLane) < 4);

  ControllerIndex = (RpIndex / 4);
  ControllerLane  = (RpIndex % 4) + RpLane;
  if (IsPcieLaneReversalEnabled (RpIndex)) {
    ControllerLane = 3 - ControllerLane;
  }
  return (ControllerIndex * 4) + ControllerLane;
}

/**
  Returns the PCIe controller configuration (4x1, 1x2-2x1, 2x2, 1x4)

  @param[in] ControllerIndex        Number of PCIe controller (0 based)

  @retval PCIe controller configuration
**/
PCIE_CONTROLLER_CONFIG
GetPcieControllerConfig (
  IN UINT32  ControllerIndex
  )
{
  UINT32                      Data32;
  PCIE_CONTROLLER_CONFIG      Config;
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;

  PcieGetControllerDev (ControllerIndex, &RpDevPrivate);

  Data32 = RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Read32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_STRPFUSECFG);
  if (Data32 == MAX_UINT32) {
    DEBUG ((DEBUG_ERROR, "Failed to get controller config, defaulting to Pcie4x1\n"));
    return Pcie4x1;
  }

  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    //
    // For SIP17, STRPFUSECFG register layout is different than previously
    //
    Config = ((Data32 & B_PCH_PCIE_CFG_STRPFUSECFG_RPC_SIP17) >> N_PCH_PCIE_CFG_STRPFUSECFG_RPC);
    if (Config == 0x4) {
      return Pcie1x4;
    }
  } else {
    Config = ((Data32 & B_PCH_PCIE_CFG_STRPFUSECFG_RPC) >> N_PCH_PCIE_CFG_STRPFUSECFG_RPC);
  }
  DEBUG ((DEBUG_INFO, "PCIe SP%c is %a\n", (UINTN) ('A' + ControllerIndex), mPcieControllerConfigName[Config]));
  return Config;
}

/**
  Programs Isolated Memory Region feature.
  PCIe IMR is programmed in a PCH rootport, based on data retrieved from CPU registers.

  @param[in] RpIndex     Root Port Number (0-based)
  @param[in] Rs3Bus      Bus number for IMR. All PCIE data on RS3 will be identified by this bus number
**/
VOID
EnablePcieImr (
  UINT8  RpIndex,
  UINT8  Rs3Bus
  )
{
  UINT32       ImrBaseLow;
  UINT32       ImrBaseHigh;
  UINT32       ImrLimitLow;
  UINT32       ImrLimitHigh;
  PCI_IMR_HOB  *PciImrHob;
  PCH_SBI_PID  ControllerPid;
  UINT32       Data32;
  UINT64       ImrLimit;

  DEBUG ((DEBUG_INFO, "EnablePcieImr: RP %d, bus %d\n", RpIndex, Rs3Bus));

  PciImrHob = NULL;
  PciImrHob = GetFirstGuidHob (&gPciImrHobGuid);
  if (PciImrHob == NULL) {
    DEBUG ((DEBUG_INFO, "EnablePcieImr: no HOB\n"));
    return;
  }
  //
  // Sanity check - don't program PCIe IMR if base address is 0
  //
  if (PciImrHob->PciImrBase == 0) {
    DEBUG ((DEBUG_ERROR, "PcieImr base address is 0, IMR programming skipped!\n"));
    return;
  }
  ImrLimit = PciImrHob->PciImrBase + (GetPcieImrSize () << 20);
  ImrBaseLow   = (UINT32) RShiftU64 ((PciImrHob->PciImrBase & 0xFFF00000), 20);
  ImrBaseHigh  = (UINT32) RShiftU64 (PciImrHob->PciImrBase, 32);
  ImrLimitLow  = (UINT32) RShiftU64 ((ImrLimit & 0xFFF00000), 20);
  ImrLimitHigh = (UINT32) RShiftU64 (ImrLimit, 32);
  //
  // PCIe IMR base & limit registers in PCH contain bits 63:20 of adress, divided into upper (64:32) and lower (31:20) parts
  // That means bits 19:10 are ignored and addresses are aligned to 1MB.
  //
  ControllerPid = PchGetPcieControllerSbiPid (RpIndex / PCH_PCIE_CONTROLLER_PORTS);


  Data32 = Rs3Bus | (ImrBaseLow << N_SPX_PCR_IMRAMBL_IAMB) | (ImrLimitLow << N_SPX_PCR_IMRAMBL_IAML);
  PchPcrWrite32 (ControllerPid, R_SPX_PCR_IMRAMBL, Data32);
  PchPcrWrite32 (ControllerPid, R_SPX_PCR_IMRAMBU32, ImrBaseHigh);
  PchPcrWrite32 (ControllerPid, R_SPX_PCR_IMRAMLU32, ImrLimitHigh);
  PchPcrWrite32 (ControllerPid, R_SPX_PCR_IMRAMLE, (BIT0 << (RpIndex % PCH_PCIE_CONTROLLER_PORTS)) | B_SPX_PCR_IMRAMLE_SRL);
  PsfSetRs3Bus (Rs3Bus);

  DEBUG ((DEBUG_INFO, "IMR registers: PID %x, +10=%08x, +14=%08x, +18=%08x, +1c=%08x\n",
    ControllerPid,
    PchPcrRead32(ControllerPid, R_SPX_PCR_IMRAMBL),
    PchPcrRead32(ControllerPid, R_SPX_PCR_IMRAMBU32),
    PchPcrRead32(ControllerPid, R_SPX_PCR_IMRAMLU32),
    PchPcrRead32(ControllerPid, R_SPX_PCR_IMRAMLE)
    ));

}


/**
  This function sets Common Clock Mode bit in rootport and endpoint connected to it, if both sides support it.
  This bit influences rootport's Gen3 training and should be set before Gen3 software equalization is attempted.
  It does not attempt to set CCC in further links behind rootport

  @param[in] RpIndex     Root Port index
**/
VOID
EnableCommonClock (
  IN UINT32 RpIndex,
  IN UINT8  TempPciBus
  )
{
  UINT64 RpBase;
  UINT64 EpBase;
  UINT8  Func;
  UINT8  EpPcieCapOffset;

  //
  // If endpoint gets reset as result of Gen3 training, it will forget all its config including CCC
  // while rootport will keep CCC set. The inconsistency will fix itself slightly later in boot flow when
  // PchPcieInitRootPortDownstreamDevices() is called
  //
  RpBase = PchPcieRpPciCfgBase (RpIndex);
  PcieRpSetSubordSecondBus (RpBase, DEFAULT_PCI_SEGMENT_NUMBER_PCH, TempPciBus);
  EpBase = PCI_SEGMENT_LIB_ADDRESS (DEFAULT_PCI_SEGMENT_NUMBER_PCH, TempPciBus, 0, 0, 0);
  //
  // The piece of code below used to be under "if (IsDevicePresent (EpBase))" check
  // which failed to detect Thunderbolt devices (they report DeviceID=0xFFFF)
  // That check helped readability but wasn't necessary - if endpoint is really missing,
  // PcieBaseFindCapId will return 0 and CCC programming will not execute anyway
  //
  EpPcieCapOffset = PcieBaseFindCapId (EpBase, EFI_PCI_CAPABILITY_ID_PCIEXP);
  if (GetScc (RpBase, R_PCIE_CFG_CLIST) && (EpPcieCapOffset != 0) && GetScc (EpBase, EpPcieCapOffset)) {
    EnableCcc (RpBase, R_PCIE_CFG_CLIST);
    EnableCcc (EpBase, EpPcieCapOffset);
    if (IsMultifunctionDevice (EpBase)) {
      for (Func = 1; Func <= PCI_MAX_FUNC; Func++) {
        EpBase = PCI_SEGMENT_LIB_ADDRESS (DEFAULT_PCI_SEGMENT_NUMBER_PCH, TempPciBus, 0, Func, 0);
        EnableCcc (EpBase, PcieBaseFindCapId (EpBase, EFI_PCI_CAPABILITY_ID_PCIEXP));
      }
    }
    RetrainLink (RpBase, R_PCIE_CFG_CLIST, TRUE);
  }
  PcieRpClearSubordSecondBus (RpBase);
}

/**
  Determines whether PCIe link is active

  @param[in] RpBase    Root Port base address
  @retval Link Active state
**/
STATIC
BOOLEAN
IsLinkActive (
  UINT64  RpBase
  )
{
  return !! (PciSegmentRead16 (RpBase + R_PCIE_CFG_LSTS) & B_PCIE_LSTS_LA);
}

/**
  This function checks if de-emphasis needs to be changed from default for a given rootport

  @param[in] DevInfo      Information on device that is connected to rootport

  @retval TRUE            De-emphasis needs to be changed
  @retval FALSE           No need to change de-emphasis
**/
BOOLEAN
NeedDecreasedDeEmphasis (
  IN PCIE_DEVICE_INFO      DevInfo
  )
{
  //
  // Intel WiGig devices
  //
  if (DevInfo.Vid == V_PCH_INTEL_VENDOR_ID && DevInfo.Did == 0x093C) {
    return TRUE;
  }
  return FALSE;
}

/**
  Detect whether CLKREQ# is supported by the platform and device.

  Assumes device presence. Device will pull CLKREQ# low until CPM is enabled.
  Test pad state to verify that the signal is correctly connected.
  This function will change pad mode to GPIO!

  @param[in] RpIndex             Root port number
  @param[in] PcieRpCommonConfig  Address of a Root Port Common Config

  @retval TRUE if supported, FALSE otherwise.
**/
BOOLEAN
PcieDetectClkreq (
  IN       UINT32           RpIndex,
  IN CONST PCH_PCIE_CONFIG  *PchPcieConfig
  )
{
  PCH_PCIE_CLOCK_USAGE ClockUsage;

  if (RpIndex >= PCH_MAX_PCIE_ROOT_PORTS) {
    return FALSE;
  }
  ClockUsage = PchClockUsagePchPcie0 + RpIndex;

#if FixedPcdGetBool (PcdHybridStorageSupport) == 1
  UINT32                     MaxPcieControllerNum;
  UINT32                     Index;
  UINT32                     HybridStorageLocation;

  HybridStorageLocation = GetHybridStorageLocation ();
  if ((HybridStorageLocation != 0)) {
    MaxPcieControllerNum = GetPchMaxPcieControllerNum ();
    for (Index = 0; Index < MaxPcieControllerNum; Index++) {
      if ((HybridStorageLocation & (BIT0 << Index)) && (RpIndex == (Index * PCH_PCIE_CONTROLLER_PORTS + 2))) {
        RpIndex = RpIndex - 2;
        ClockUsage = PchClockUsagePchPcie0 + RpIndex;
      }
    }
  }

  if (GetHybridStoragePchRpForCpuAttach () == RpIndex) {
    ClockUsage = PchClockUsageCpuPcie0 + GetHybridStorageCpuRpLocation ();
  }
#endif
  if (!IsClkReqAssigned (ClockUsage)) {
    return FALSE;
  }
  if (PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.ClkReqDetect &&
      EFI_ERROR (CheckClkReq (ClockUsage))) {
    DEBUG ((DEBUG_INFO, "CLKREQ is not Low, disabling power management for RP %d.\n", RpIndex));
    return FALSE;
  }
  return TRUE;
}

/*
  Get PCIE Port configuration and disable value for each RP
  and perform function disable.
*/
VOID
GetConfigAndDisablePcieRootPort (
  VOID
  )
{
  UINT8                       CtrlIndex;
  UINT8                       MaxPciePortNum;
  UINT8                       RpIndex;
  UINT32                      SpxPcd[PCH_MAX_PCIE_CONTROLLERS];
  UINT32                      SpxMask;
  PCIE_CONTROLLER_CONFIG      ControllerConfig;
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;

  ZeroMem (SpxPcd, sizeof (SpxPcd));

  MaxPciePortNum = GetPchMaxPciePortNum ();

  for (CtrlIndex = 0; CtrlIndex < MaxPciePortNum / PCH_PCIE_CONTROLLER_PORTS; CtrlIndex++) {
    for (RpIndex = 0; RpIndex < MaxPciePortNum; ++RpIndex) {
      //
      // Initialize the PCI root port device structure.
      //
      PcieGetRpDev (RpIndex, &RpDevPrivate);
      if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
        SpxPcd[CtrlIndex] = PchPcrRead32 (PchGetPcieControllerSbiPid (CtrlIndex), R_SPX_SIP16_PCR_PCD);
      } else {
        SpxPcd[CtrlIndex] = PchPcrRead32 (PchGetPcieControllerSbiPid (CtrlIndex), R_SPX_PCR_PCD);
      }
    }
    DEBUG ((DEBUG_INFO, "SP%c Controller PCD Value = %x\n", 'A' + CtrlIndex, SpxPcd[CtrlIndex]));
  }

  for (RpIndex = 0; RpIndex < MaxPciePortNum; RpIndex++) {
    CtrlIndex  = RpIndex / PCH_PCIE_CONTROLLER_PORTS;
    SpxMask = B_SPX_PCR_PCD_P1D << (RpIndex % PCH_PCIE_CONTROLLER_PORTS);
    if ((SpxPcd[CtrlIndex] & SpxMask) == SpxMask) {
      PsfDisablePcieRootPort (PsfGetPciePortData (RpIndex), PsfGetSegmentTable ());
    }
  }

  for (CtrlIndex = 0; CtrlIndex < MaxPciePortNum / PCH_PCIE_CONTROLLER_PORTS; CtrlIndex++) {
    DEBUG ((DEBUG_INFO, "Multilane check for PCIE Controller %d: ", CtrlIndex));
    ControllerConfig = GetPcieControllerConfig (CtrlIndex);
    RpIndex = CtrlIndex * PCH_PCIE_CONTROLLER_PORTS;
    switch (ControllerConfig) {
      case Pcie1x4:
        //
        // Disable ports 2, 3, 4 of a controller when it's set to 1 x4
        //
        DEBUG ((DEBUG_INFO, "Disabling RP %d/%d/%d\n", RpIndex+1, RpIndex+2, RpIndex+3));
        PsfDisablePcieRootPort (PsfGetPciePortData (RpIndex + 1), PsfGetSegmentTable ());
        PsfDisablePcieRootPort (PsfGetPciePortData (RpIndex + 2), PsfGetSegmentTable ());
        PsfDisablePcieRootPort (PsfGetPciePortData (RpIndex + 3), PsfGetSegmentTable ());
        break;
      case Pcie2x2:
        //
        // Disable ports 2, 4 of a controller when it's set to 2 x2
        //
        DEBUG ((DEBUG_INFO, "Disabling RP %d/%d\n", RpIndex+1, RpIndex+3));
        PsfDisablePcieRootPort (PsfGetPciePortData (RpIndex + 1), PsfGetSegmentTable ());
        PsfDisablePcieRootPort (PsfGetPciePortData (RpIndex + 3), PsfGetSegmentTable ());
        break;
      case Pcie1x2_2x1:
        //
        // Disable port 2 of a controller when it's set to 1 x2
        //
        DEBUG ((DEBUG_INFO, "Disabling RP %d\n", RpIndex+1));
        PsfDisablePcieRootPort (PsfGetPciePortData (RpIndex + 1), PsfGetSegmentTable ());
        break;
      case Pcie4x1:
      default:
        DEBUG ((DEBUG_INFO, "No ports disabled\n"));
        break;
    }
  }
}

/**
  Disables the root port. Depending on 2nd param, port's PCI config space may be left visible
  to prevent function swapping

  Use sideband access unless the port is still available.

  @param[in] PortIndex          Root Port Number
  @param[in] KeepPortVisible    Should the port' interface be left visible on PCI Bus0
**/
VOID
DisablePcieRootPort (
  IN UINT8   RpIndex,
  IN BOOLEAN KeepPortVisible
  )
{
  UINT32                      Data32;
  UINT32                      LoopTime;
  UINT32                      TargetState;
  UINT32                      LinkActive;
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;

  DEBUG ((DEBUG_INFO, "DisablePcieRootPort(%d) Start\n", RpIndex));

  Data32 = 0;
  PcieGetRpDev (RpIndex, &RpDevPrivate);
  Data32 = RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Read32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, (R_PCIE_CFG_LSTS - 2));//access LSTS using dword-aligned read
  LinkActive = (Data32 >> 16) & B_PCIE_LSTS_LA;

  if (LinkActive) {
    ///
    /// If device is present disable the link.
    ///
    DEBUG ((DEBUG_INFO, "Disabling the link.\n"));
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCIEALC, B_PCH_PCIE_CFG_PCIEALC_BLKDQDA);
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_LCTL, B_PCIE_LCTL_LD);
    if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
      RpDevPrivate.RpDev.PciSbiMsgMemAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgMemAccess, R_PCIE_RCRB_FCTL2, B_PCIE_RCRB_FCTL2_BLKNAT);
    }
    for (LoopTime = 0; LoopTime < 5000; LoopTime++) {
      Data32 = RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Read32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCIESTS1);
      if ((((Data32 & B_PCH_PCIE_CFG_PCIESTS1_LTSMSTATE) >> N_PCH_PCIE_CFG_PCIESTS1_LTSMSTATE) == V_PCH_PCIE_CFG_PCIESTS1_LTSMSTATE_DETRDY) ||
         (((Data32 & B_PCH_PCIE_CFG_PCIESTS1_LTSMSTATE) >> N_PCH_PCIE_CFG_PCIESTS1_LTSMSTATE) == V_PCH_PCIE_CFG_PCIESTS1_LTSMSTATE_DISWAITPG))  {
        break;
      }
      MicroSecondDelay (10);
    }
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->And32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_LCTL, ~ (UINT32) B_PCIE_LCTL_LD);
  } else {
    ///
    /// Otherwise if device is not present perform the following steps using sideband access:
    /// 1.  Set B0:Dxx:Fn:338h[26] = 1b
    /// 2.  Poll B0:Dxx:Fn:328h[31:24] until 0x1 with 50ms timeout
    /// 3.  Set B0:Dxx:Fn +408h[27] =1b
    ///

    DEBUG ((DEBUG_INFO, "Stopping the port.\n"));
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCIEALC, B_PCH_PCIE_CFG_PCIEALC_BLKDQDA);

    TargetState = V_PCH_PCIE_CFG_PCIESTS1_LTSMSTATE_DETRDY;
    for (LoopTime = 0; LoopTime < TIMEOUT; LoopTime++) {
      Data32 = RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Read32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCIESTS1);
      if (((Data32 & B_PCH_PCIE_CFG_PCIESTS1_LTSMSTATE) >> N_PCH_PCIE_CFG_PCIESTS1_LTSMSTATE) == TargetState) {
        break;
      }
      MicroSecondDelay (10);
    }

    //
    // Show polling status
    //
    if (LoopTime > 0) {
      DEBUG ((DEBUG_INFO, "Polling for DETRDY for %dus. PCIESTS1 = 0x%08x\n", LoopTime*10, Data32));
    }
  }
  ///
  /// Set offset 408h[27] to 1b to disable squelch.
  ///
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PHYCTL4, B_PCH_PCIE_CFG_PHYCTL4_SQDIS);

  ///
  /// Make port disappear from PCI bus
  ///
  if (!KeepPortVisible) {
    DEBUG ((DEBUG_INFO, "Hiding the port\n"));
    ///
    /// PCIe RP IOSF Sideband register offset 0x00[19:16], depending on the port that is Function Disabled
    /// Access it by offset 0x02[4:0] to avoid RWO bit
    ///
    if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
      PchPcrAndThenOr8 (
        GetRpSbiPid (RpIndex),
        R_SPX_SIP16_PCR_PCD + 0x02,
        0x0F,
        (UINT8) (1 << (RpIndex % 4))
        );
    } else {
      PchPcrAndThenOr8 (
        GetRpSbiPid (RpIndex),
        R_SPX_PCR_PCD + 0x02,
        0x0F,
        (UINT8) (1 << (RpIndex % 4))
        );
    }
    ///
    /// Then put root port to D3 and disable the port in PSF
    ///
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PMCS, V_PCIE_PMCS_PS_D3H);
    PsfDisablePcieRootPort (PsfGetPciePortData (RpIndex), PsfGetSegmentTable ());
  }
  ///
  /// Function disable PCIE port in PMC
  ///
  PmcDisablePcieRootPort (RpIndex);
  DisableClock (PchClockUsagePchPcie0 + RpIndex);
  PchFiaDisablePchPciePortClkReq (RpIndex);
  DEBUG ((DEBUG_INFO, "DisablePcieRootPort() End\n"));
}

/**
  This function creates Capability and Extended Capability List

  @param[in] RpIndex               Root Port index
  @param[in] RpBase                Root Port pci segment base address
  @param[in] PcieRpCommonConfig    Pointer to a PCIE_ROOT_PORT_COMMON_CONFIG that provides the platform setting

**/
VOID
InitCapabilityList (
  IN UINT32                              RpIndex,
  IN UINT64                              RpBase,
  IN CONST PCIE_ROOT_PORT_COMMON_CONFIG  *PcieRpCommonConfig
  )
{
  UINT32               Data32;
  UINT16               Data16;
  UINT8                Data8;
  UINT16               NextCap;
  UINT32               Data32And;
  UINT32               Data32Or;
  EFI_STATUS           Status;
  PCH_PCIE_CLOCK_USAGE ClockUsage;

  ClockUsage = PchClockUsagePchPcie0 + RpIndex;
#if FixedPcdGetBool (PcdHybridStorageSupport) == 1
  UINT32      MaxPcieControllerNum;
  UINT32      Index;
  UINT32      HybridStorageLocation;
  HybridStorageLocation = GetHybridStorageLocation ();
  if ((HybridStorageLocation != 0)) {
    MaxPcieControllerNum = GetPchMaxPcieControllerNum ();
    for (Index = 0; Index < MaxPcieControllerNum; Index++) {
      if ((HybridStorageLocation & (BIT0 << Index)) && (RpIndex == (Index * PCH_PCIE_CONTROLLER_PORTS + 2))) {
        RpIndex = RpIndex - 2;
        ClockUsage = PchClockUsagePchPcie0 + RpIndex;
      }
    }
  }
  if (GetHybridStoragePchRpForCpuAttach () == RpIndex) {
    ClockUsage = PchClockUsageCpuPcie0 + GetHybridStorageCpuRpLocation ();
  }
#endif
  ///
  /// Build Capability linked list
  /// 1.  Read and write back to capability registers 34h, 41h, 81h and 91h using byte access.
  /// 2.  Program NSR, A4h[3] = 0b
  ///
  Data8 = PciSegmentRead8 (RpBase + PCI_CAPBILITY_POINTER_OFFSET);
  PciSegmentWrite8 (RpBase + PCI_CAPBILITY_POINTER_OFFSET, Data8);

  Data16 = PciSegmentRead16 (RpBase + R_PCIE_CFG_CLIST);
  PciSegmentWrite16 (RpBase + R_PCIE_CFG_CLIST, Data16);

  Data16 = PciSegmentRead16 (RpBase + R_PCH_PCIE_CFG_MID);
  PciSegmentWrite16 (RpBase + R_PCH_PCIE_CFG_MID, Data16);

  Data16 = PciSegmentRead16 (RpBase + R_PCH_PCIE_CFG_SVCAP);
  PciSegmentWrite16 (RpBase + R_PCH_PCIE_CFG_SVCAP, Data16);

  Data32 = PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_PMCS);
  Data32 &= (UINT32) ~(B_PCIE_PMCS_NSR);
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_PMCS, Data32);

  /*
  a. NEXT_CAP = 0h
  */
  NextCap     = V_PCIE_EXCAP_NCO_LISTEND;


  /*
  b. If Downstream Port Containment is enabled, then
    1. Set Next Capability Offset, Dxx:Fn +A00h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +A00h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +A00h[15:0] = 001h
    4. NEXT_CAP = A00h
    ELSE, set Dxx:Fn +A00h [31:0] = 0

  */
  Data32 = 0;
  if (PcieRpCommonConfig->DpcEnabled == TRUE) {
    Data32  = (V_PCH_PCIE_CFG_EX_DPCECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_DPC_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    NextCap = R_PCH_PCIE_CFG_EX_DPCECH;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_DPCECH, Data32);
  /*
    Set DPC capabilities
  */
  if (PcieRpCommonConfig->DpcEnabled == TRUE) {
    Data32 = PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_EX_DPCECH + R_PCIE_EX_DPC_CAP_OFFSET);
    Data32 &= ~BIT5;
    if (PcieRpCommonConfig->RpDpcExtensionsEnabled == TRUE) {
      Data32 |= BIT5;
    }
  } else {
    Data32 = 0;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_DPCECH + R_PCIE_EX_DPC_CAP_OFFSET, Data32);

  /*
  c. If the RP is GEN3 capable (by fuse and BIOS policy), enable Secondary PCI Express Extended Capability
    1. Set Next Capability Offset, Dxx:Fn +220h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +220h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +220h[15:0] = 0019h
    4. NEXT_CAP = 220h
    ELSE, set Dxx:Fn +220h [31:0] = 0
  */

  Data32 = 0;
  if (GetMaxLinkSpeed (RpBase) >= V_PCIE_LCAP_MLS_GEN3) {
    Data32  = (V_PCIE_CFG_EX_SPEECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_SPE_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    NextCap = R_PCIE_CFG_EX_SPEECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_CFG_EX_SPEECH, Data32);

  /*
  d. If support L1 Sub-State
    1. Set Next Capability Offset, Dxx:Fn +200h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +200h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +200h[15:0] = 001Eh
    4. Read and write back to Dxx:Fn +204h
    5. Refer to section 8.3 for other requirements (Not implemented here)
    6. NEXT_CAP = 200h
    ELSE, set Dxx:Fn +200h [31:0] = 0, and read and write back to Dxx:Fn +204h
  */

  Data32 = 0;
  if (IsClkReqAssigned (ClockUsage) &&
      (PcieRpCommonConfig->L1Substates != PchPcieL1SubstatesDisabled)) {
    Data32  = (V_PCH_PCIE_CFG_EX_L1S_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_L1S_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    NextCap = R_PCH_PCIE_CFG_EX_L1SECH;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_L1SECH, Data32);

  Status = LoadDefaultRegValues (RpBase, R_PCH_PCIE_CFG_EX_L1SCAP, &Data32And, &Data32Or, PcieRpCommonConfig);
  if (!EFI_ERROR (Status)) {
    PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_EX_L1SCAP, Data32And, Data32Or);
  }
  /*
  e. If support PTM
    1. Set Next Capability Offset, Dxx:Fn +150h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +140h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +140h[15:0] = 001Fh
    4. Read and write back to Dxx:Fn +144h
    5. NEXT_CAP = 140h
    ELSE, set Dxx:Fn +150h [31:0] = 0
    In both cases: read Dxx:Fn + 154h, set BIT1 and BIT2 then write it back
  */
  Data32 = 0;
  if (PcieRpCommonConfig->PtmEnabled == TRUE) {
    Data32 = (V_PCH_PCIE_CFG_EX_PTM_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PTM_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    NextCap = R_PCH_PCIE_CFG_EX_PTMECH;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_PTMECH, Data32);
  Data32 = PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_EX_PTMCAPR);
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_PTMCAPR, (Data32 | B_PCIE_EX_PTMCAP_PTMRC | B_PCIE_EX_PTMCAP_PTMRSPC));

  /*
  f. If support ACS
    1. Set Next Capability Offset, Dxx:Fn +140h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +140h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +140h[15:0] = 000Dh
    4. Read and write back to Dxx:Fn +144h
    5. NEXT_CAP = 140h
    ELSE, set Dxx:Fn +140h [31:0] = 0, and read and write back to Dxx:Fn +144h
  */
  Data32 = 0;
  if (PcieRpCommonConfig->AcsEnabled == TRUE) {
    Data32 = (V_PCH_PCIE_CFG_EX_ACS_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_ACS_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    NextCap = R_PCH_PCIE_CFG_EX_ACSECH;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_ACSECH, Data32);

  Data32 = PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_EX_ACSCAPR);
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_ACSCAPR, Data32);

  /*
  g. If support Advanced Error Reporting
    1. Set Next Capability Offset, Dxx:Fn +100h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +100h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +100h[15:0] = 0001h
    ELSE
    1. Set Next Capability Offset, Dxx:Fn +100h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +100h[19:16]  = 0h
    3. Set Capability ID, Dxx:Fn +100h[15:10]  = 0000h
  */

  Data32 = 0;
  if (PcieRpCommonConfig->AdvancedErrorReporting) {
    Data32 = (V_PCH_PCIE_CFG_EX_AEC_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_AEC_CID;
  }
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_AECH, Data32);

}

/**
  This function creates SIP17 Capability and Extended Capability List

  @param[in] RpIndex            Root Port index
  @param[in] RpBase             Root Port pci segment base address
  @param[in] PcieRpCommonConfig Pointer to a PCIE_ROOT_PORT_COMMON_CONFIG that provides the platform setting

**/
VOID
PchPcieSip17InitCapabilityList (
  IN UINT32                              RpIndex,
  IN UINT64                              RpBase,
  IN CONST PCIE_ROOT_PORT_COMMON_CONFIG  *PcieRpCommonConfig
  )
{
  UINT32               Data32;
  UINT16               Data16;
  UINT8                Data8;
  UINT16               NextCap;
  UINT32               Data32And;
  UINT32               Data32Or;
  EFI_STATUS           Status;
  PCH_PCIE_CLOCK_USAGE ClockUsage;

  ClockUsage = PchClockUsagePchPcie0 + RpIndex;

  DEBUG ((DEBUG_INFO, "PchPcieSip17InitCapabilityList start\n"));
#if FixedPcdGetBool (PcdHybridStorageSupport) == 1
  UINT32      MaxPcieControllerNum;
  UINT32      Index;
  UINT32      HybridStorageLocation;
  HybridStorageLocation = GetHybridStorageLocation ();
  if ((HybridStorageLocation != 0)) {
    MaxPcieControllerNum = GetPchMaxPcieControllerNum ();
    for (Index = 0; Index < MaxPcieControllerNum; Index++) {
      if ((HybridStorageLocation & (BIT0 << Index)) && (RpIndex == (Index * PCH_PCIE_CONTROLLER_PORTS + 2))) {
        RpIndex = RpIndex - 2;
        ClockUsage = PchClockUsagePchPcie0 + RpIndex;
      }
    }
  }
  if (GetHybridStoragePchRpForCpuAttach () == RpIndex) {
    ClockUsage = PchClockUsageCpuPcie0 + GetHybridStorageCpuRpLocation ();
  }
#endif
  ///
  /// Build Capability linked list
  /// 1.  Read and write back to capability registers 34h, 41h, 81h and 91h using byte access.
  /// 2.  Program NSR, A4h[3] = 0b
  ///
  Data8 = PciSegmentRead8 (RpBase + PCI_CAPBILITY_POINTER_OFFSET);
  PciSegmentWrite8 (RpBase + PCI_CAPBILITY_POINTER_OFFSET, Data8);

  Data16 = PciSegmentRead16 (RpBase + R_PCIE_CFG_CLIST);
  PciSegmentWrite16 (RpBase + R_PCIE_CFG_CLIST, Data16);

  Data16 = PciSegmentRead16 (RpBase + R_PCH_PCIE_CFG_MID);
  PciSegmentWrite16 (RpBase + R_PCH_PCIE_CFG_MID, 0x9805);

  Data16 = PciSegmentRead16 (RpBase + 0x98);
  PciSegmentWrite16 (RpBase + 0x98, Data16);

  Data32 = PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_PMCS);
  Data32 &= (UINT32) ~(B_PCIE_PMCS_NSR);
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_PMCS, Data32);

  ///
  /// II.Build PCI Extended Capability linked list
  /// 0xEDC - PL16MECH(CAPID:0027h) Physical Layer 16.0 GT/s Margining Extended Capability Header
  /// 0xA9C - PL16GECH(CAPID:0026h) Physical Layer 16.0 GT/s Extended Capability Header
  /// 0xA90 - DLFECH  (CAPID:0025h) Data Link Feature Extended Capability Header
  /// 0xA30 - SPEECH  (CAPID:0019h) Secondary PCI Express Extended Capability Header
  /// 0xA00 - DPCECH  (CAPID:001Dh) DPC Extended Capability Header
  /// 0x280 - VCECH   (CAPID:0002h) VC capability
  /// 0x150 - PTMECH  (CAPID:001Fh) PTM Extended Capability Register
  /// 0x200 - L1SSECH (CAPID:001Eh) L1SS Capability register
  /// 0x220 - ACSECH  (CAPID:000Dh) ACS Capability register
  /// 0x100 - AECH    (CAPID:0001h) Advanced Error Reporting Capability
  ///
  /*
  a. NEXT_CAP = 0h
  */
  NextCap     = V_PCIE_EXCAP_NCO_LISTEND;

  if (GetMaxLinkSpeed (RpBase) >= PcieGen4) {
    /*
    b. Program [0xEDC] Physical Layer 16.0 GT/s Margining Extended Capability Header(PL16MECH)
      1. Set Next Capability Offset, 0xEDC[31:20] = NEXT_CAP
      2. Set Capability Version, 0xEDC[19:16] = 1h
      3. Set Capability ID, 0xEDC[15:0] = 0027h
      4. NEXT_CAP = 0xEDC
    */
    Data32 = (V_PCIE_CFG_PL16MECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PL16MECH_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    NextCap = R_PCIE_CFG_PL16MECH;
    PciSegmentWrite32 (RpBase + R_PCIE_CFG_PL16MECH, Data32);
    DEBUG ((DEBUG_VERBOSE, "Physical Layer 16.0 GT/s Margining Extended Capability Header\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PL16MECH_CID));

    /*
    c. Program [0xA9C] Physical Layer 16.0 GT/s Extended Capability Header(PL16GECH)
      1. Set Next Capability Offset, 0xA9C[31:20] = NEXT_CAP
      2. Set Capability Version, 0xA9C[19:16] = 1h
      3. Set Capability ID, 0xA9C[15:0] = 0026h
      4. NEXT_CAP = 0xA9C
    */
    Data32 = (V_PCIE_CFG_PL16GECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PL16GECH_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    NextCap = R_PCIE_CFG_PL16GECH;
    PciSegmentWrite32 (RpBase + R_PCIE_CFG_PL16GECH, Data32);
    DEBUG ((DEBUG_VERBOSE, "Physical Layer 16.0 GT/s Extended Capability Header\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PL16GECH_CID));
  }

  /*
  d. Program [0xA90] Data Link Feature Extended Capability Header(DLFECH)
    1. Set Next Capability Offset, 0xA90[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA90[19:16] = 1h
    3. Set Capability ID, 0xA90[15:0] = 0025h
    4.  NEXT_CAP = 0xA90
  */
  Data32 = (V_PCIE_CFG_DLFECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_DLFECH_CID;
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  NextCap = R_PCIE_CFG_DLFECH;
  PciSegmentWrite32 (RpBase + R_PCIE_CFG_DLFECH, Data32);
  DEBUG ((DEBUG_VERBOSE, "Data Link Feature Extended Capability Header\n"));
  DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_DLFECH_CID));

  /*
  e. If the RP is GEN3 capable (by fuse and BIOS policy), enable Secondary PCI Express Extended Capability
    1. Set Next Capability Offset,0xA30[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA30[19:16] = 1h
    3. Set Capability ID,  0xA30[15:0] = 0019h
    4. NEXT_CAP = 0xA30
    ELSE, set 0xA30[31:0] = 0
  */
  Data32 = 0;
  if (GetMaxLinkSpeed (RpBase) >= V_PCIE_LCAP_MLS_GEN3) {
    Data32 = (V_PCIE_CFG_EX_SPEECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_SPE_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "Secondary PCI Express Extended Capability\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_SPE_CID));
    NextCap = R_PCIE_CFG_EX_SPEECH;
  }
  PciSegmentWrite32 (RpBase + R_PCIE_CFG_EX_SPEECH, Data32);

  /*
  f. If Downstream Port Containment is enabled, then
    1. Set Next Capability Offset, 0xA00[31:20] = NEXT_CAP
    2. Set Capability Version, 0xA00[19:16] = 1h
    3. Set Capability ID, 0xA00[15:0] = 001h
    4. NEXT_CAP = 0xA00
    ELSE, set 0xA00 [31:0] = 0
  */
  Data32 = 0;
  if (PcieRpCommonConfig->DpcEnabled == TRUE) {
    Data32 = (V_PCH_PCIE_CFG_EX_DPCECH_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_DPC_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "Downstream port containment\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_DPC_CID));
    NextCap = R_PCH_PCIE_CFG_EX_DPCECH;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_DPCECH, Data32);

  /*
    Set DPC capabilities
  */
  if (PcieRpCommonConfig->DpcEnabled == TRUE) {
    Data32 = PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_EX_DPCECH + R_PCIE_EX_DPC_CAP_OFFSET);
    Data32 &= ~BIT5;
    if (PcieRpCommonConfig->RpDpcExtensionsEnabled == TRUE) {
      Data32 |= BIT5;
    }
  } else {
    Data32 = 0;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_DPCECH + R_PCIE_EX_DPC_CAP_OFFSET, Data32);

  /*
  g. If support PTM
    1. Set Next Capability Offset, Dxx:Fn +150h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +150h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +150h[15:0] = 001Fh
    4. Read and write back to Dxx:Fn +144h
    5. NEXT_CAP = 150h
    ELSE, set Dxx:Fn +150h [31:0] = 0
    In both cases: read Dxx:Fn + 154h, set BIT1 and BIT2 then write it back
  */
  Data32 = 0;
  if (PcieRpCommonConfig->PtmEnabled == TRUE) {
    Data32 = (V_PCH_PCIE_CFG_EX_PTM_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_PTM_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "PTM\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_PTM_CID));
    NextCap = R_PCH_PCIE_CFG_EX_PTMECH;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_PTMECH, Data32);
  Data32 = PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_EX_PTMCAPR);
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_PTMCAPR, (Data32 | B_PCIE_EX_PTMCAP_PTMRC | B_PCIE_EX_PTMCAP_PTMRSPC));

  /*
  h. If support L1 Sub-State
    1. Set Next Capability Offset, Dxx:Fn +200h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +200h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +200h[15:0] = 001Eh
    4. Read and write back to Dxx:Fn +204h
    5. Refer to section 8.3 for other requirements (Not implemented here)
    6. NEXT_CAP = 200h
    ELSE, set Dxx:Fn +200h [31:0] = 0, and read and write back to Dxx:Fn +204h
  */
  Data32 = 0;
  if (IsClkReqAssigned (ClockUsage) &&
    (PcieRpCommonConfig->L1Substates != PchPcieL1SubstatesDisabled)) {
    Data32  = (V_PCH_PCIE_CFG_EX_L1S_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_L1S_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "L1SS\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_L1S_CID));
    NextCap = R_PCH_PCIE_CFG_EX_L1SECH;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_L1SECH, Data32);

  Status = LoadDefaultRegValues (RpBase, R_PCH_PCIE_CFG_EX_L1SCAP, &Data32And, &Data32Or, PcieRpCommonConfig);
  if (!EFI_ERROR (Status)) {
    PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_EX_L1SCAP, Data32And, Data32Or);
  }

  /*
  i. If support ACS
    1. Set Next Capability Offset, Dxx:Fn +220h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +220h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +220h[15:0] = 000Dh
    4. Read and write back to Dxx:Fn +224h
    5. NEXT_CAP = 220h
    ELSE, set Dxx:Fn +220h [31:0] = 0, and read and write back to Dxx:Fn +224h
  */
  Data32 = 0;
  if (PcieRpCommonConfig->AcsEnabled == TRUE) {
    Data32 = (V_PCH_PCIE_CFG_EX_ACS_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_ACS_CID;
    Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
    DEBUG ((DEBUG_VERBOSE, "ACS\n"));
    DEBUG ((DEBUG_VERBOSE, "NextCap = %x \t Capability ID = %x\n", NextCap, V_PCIE_EX_ACS_CID));
    NextCap = R_PCH_PCIE_CFG_EX_ACSECH;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_ACSECH, Data32);

  Data32 = PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_EX_ACSCAPR);
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_ACSCAPR, Data32);

  /*
  j. If support Advanced Error Reporting
    1. Set Next Capability Offset, Dxx:Fn +100h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +100h[19:16] = 1h
    3. Set Capability ID, Dxx:Fn +100h[15:0] = 0001h
    ELSE
    1. Set Next Capability Offset, Dxx:Fn +100h[31:20] = NEXT_CAP
    2. Set Capability Version, Dxx:Fn +100h[19:16]  = 0h
    3. Set Capability ID, Dxx:Fn +100h[15:10]  = 0000h
  */
  Data32 = 0;
  if (PcieRpCommonConfig->AdvancedErrorReporting) {
    Data32 = (V_PCH_PCIE_CFG_EX_AEC_CV << N_PCIE_EXCAP_CV) | V_PCIE_EX_AEC_CID;
  }
  Data32 |= (NextCap << N_PCIE_EXCAP_NCO);
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_EX_AECH, Data32);

  DEBUG ((DEBUG_INFO, "PchPcieSip17InitCapabilityList end\n"));
}

/**
  The function to change the root port speed

  @retval EFI_SUCCESS             Succeeds.
**/
EFI_STATUS
PcieRpSpeedChange (
  VOID
  )
{
  UINTN                 PortIndex;
  UINTN                 PchMaxPciePortNum;
  UINT64                PciRootPortRegBase[PCH_MAX_PCIE_ROOT_PORTS];
  UINTN                 LinkRetrainedBitmap;
  UINTN                 TimeoutCount;
  UINT32                MaxLinkSpeed;
  UINT8                 TempPciBusMin;
  PCIE_DEVICE_INFO      DevInfo;

  PchMaxPciePortNum = GetPchMaxPciePortNum ();
  TempPciBusMin = PcdGet8 (PcdSiliconInitTempPciBusMin);
  //
  // Since we are using the root port base many times, it is best to cache them.
  //
  for (PortIndex = 0; PortIndex < PchMaxPciePortNum; PortIndex++) {
    PciRootPortRegBase[PortIndex] = PchPcieRpPciCfgBase (PortIndex);
  }

  ///
  /// PCH BIOS Spec Section 8.14 Additional PCI Express* Programming Steps
  /// NOTE: Detection of Non-Complaint PCI Express Devices
  ///
  LinkRetrainedBitmap = 0;
  for (PortIndex = 0; PortIndex < PchMaxPciePortNum; PortIndex++) {
    if (PciRootPortRegBase[PortIndex] == 0) {
      continue;
    }
    if (PciSegmentRead16 (PciRootPortRegBase[PortIndex] + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
      continue;
    }
  GetDeviceInfo (PciRootPortRegBase[PortIndex], TempPciBusMin, &DevInfo);
  MaxLinkSpeed = MIN (GetMaxLinkSpeed (PciRootPortRegBase[PortIndex]), DevInfo.MaxLinkSpeed);

    if (MaxLinkSpeed > 1) {
      PciSegmentAndThenOr16 (
        PciRootPortRegBase[PortIndex] + R_PCIE_CFG_LCTL2,
        (UINT16) ~B_PCIE_LCTL2_TLS,
        (UINT16) MaxLinkSpeed
        );
      if (IsLinkActive (PciRootPortRegBase[PortIndex])) {
        //
        // Retrain the link if device is present
        //
        PciSegmentOr16 (PciRootPortRegBase[PortIndex] + R_PCIE_CFG_LCTL, B_PCIE_LCTL_RL);
        LinkRetrainedBitmap |= (1u << PortIndex);
      }
    }
  }

  //
  // 15 ms timeout while checking for link active on retrained link
  //
  for (TimeoutCount = 0; ((LinkRetrainedBitmap != 0) && (TimeoutCount < 150)); TimeoutCount++) {
    //
    // Delay 100 us
    //
    MicroSecondDelay (100);
    //
    // Check for remaining root port which was link retrained
    //
    for (PortIndex = 0; PortIndex < PchMaxPciePortNum; PortIndex++) {
      if ((LinkRetrainedBitmap & (1u << PortIndex)) != 0) {
        //
        // If the link is active, clear the bitmap
        //
        if (PciSegmentRead16 (PciRootPortRegBase[PortIndex] + R_PCIE_CFG_LSTS) & B_PCIE_LSTS_LA) {
          LinkRetrainedBitmap &= ~(1u << PortIndex);
        }
      }
    }
  }

  //
  // If 15 ms has timeout, and some link are not active, train to gen1
  //
  if (LinkRetrainedBitmap != 0) {
    for (PortIndex = 0; PortIndex < PchMaxPciePortNum; PortIndex++) {
      if ((LinkRetrainedBitmap & (1u << PortIndex)) != 0) {
        //
        // Set TLS to gen1
        //
        PciSegmentAndThenOr16 (PciRootPortRegBase[PortIndex] + R_PCIE_CFG_LCTL2,
          (UINT16) ~(B_PCIE_LCTL2_TLS),
          V_PCIE_LCTL2_TLS_GEN1);
        //
        // Retrain link
        //
        PciSegmentOr16 (PciRootPortRegBase[PortIndex] + R_PCIE_CFG_LCTL, B_PCIE_LCTL_RL);
      }
    }

    //
    // Wait for retrain completion or timeout in 15ms. Do not expect failure as
    // port was detected and trained as Gen1 earlier
    //
    for (TimeoutCount = 0; ((LinkRetrainedBitmap != 0) && (TimeoutCount < 150)); TimeoutCount++) {
      //
      // Delay 100 us
      //
      MicroSecondDelay (100);
      //
      // Check for remaining root port which was link retrained
      //
      for (PortIndex = 0; PortIndex < PchMaxPciePortNum; PortIndex++) {
        if ((LinkRetrainedBitmap & (1u << PortIndex)) != 0) {
          //
          // If the link is active, clear the bitmap
          //
          if (PciSegmentRead16 (PciRootPortRegBase[PortIndex] + R_PCIE_CFG_LSTS) & B_PCIE_LSTS_LA) {
            LinkRetrainedBitmap &= ~(1u << PortIndex);
          }
        }
      }
    }
  }

  return EFI_SUCCESS;
}

/*
  Checks if given rootport has an endpoint connected

  @param[in] DeviceBase       PCI segment base address of root port

  @retval                     TRUE if endpoint is connected
  @retval                     FALSE if no endpoint was detected
*/
BOOLEAN
IsEndpointConnected (
  UINT64 DeviceBase
  )
{
  return !!(PciSegmentRead16 (DeviceBase + R_PCIE_CFG_SLSTS) & B_PCIE_SLSTS_PDS);
}

/*
  Some PCIe devices may take long time between they become detected and form a link.
  This function waits until all enabled, non-empty rootports form a link or until a timeout

  @param[in] MaxRootPorts      number of rootports
  @param[in] DisabledPortMask  mask of rootprots that don't need to be considered
*/
VOID
WaitForLinkActive (
  UINT32 MaxRootPorts,
  UINT32 DisabledPortMask
  )
{
  UINT32 PortMask;
  UINT32 Index;
  UINT32 Time;
  UINT64 RpBase;

  Time = 0;
  //
  // Set a bit in PortMask for each rootport that exists and isn't going to be disabled
  //
  PortMask = (0x1 << MaxRootPorts) - 1;
  PortMask &= ~DisabledPortMask;

  DEBUG ((DEBUG_INFO, "WaitForLinkActive, RP mask to wait for = 0x%08x\n", PortMask));
  while (Time < LINK_ACTIVE_POLL_TIMEOUT) {
    for (Index = 0; Index < MaxRootPorts; Index ++) {
      if (!(PortMask & (BIT0 << Index))) {
        continue;
      }
      RpBase = PchPcieRpPciCfgBase (Index);
      //
      // if PDS is not set or if LA is set then this rootport is done - clear it from mask
      //
      if (!IsEndpointConnected (RpBase) || IsLinkActive (RpBase)) {
        PortMask &= ~ (BIT0 << Index);
      }
    }
    if (PortMask == 0x0) {
      DEBUG ((DEBUG_INFO, "WaitForLinkActive, all RPs done, lost %dms waiting\n", Time/1000));
      return;
    }
    MicroSecondDelay (LINK_ACTIVE_POLL_INTERVAL);
    Time += LINK_ACTIVE_POLL_INTERVAL;
  }

  DEBUG ((DEBUG_WARN, "WaitForLinkActive, timeout with the following RPs still not done: 0x%08x\n", PortMask));
}

/**
  Get information about the endpoint

  @param[in]  RpBase      Root port pci segment base address
  @param[in]  TempPciBus  Temporary bus number
  @param[out] DeviceInfo  Device information structure

  @raturn TRUE if device was found, FALSE otherwise
**/
BOOLEAN
GetDeviceInfo (
  IN  UINT64            RpBase,
  IN  UINT8             TempPciBus,
  OUT PCIE_DEVICE_INFO  *DeviceInfo
  )
{
  UINT64                  EpBase;
  UINT32                  Data32;
  UINT8                   EpPcieCapPtr;
  UINT8                   EpLinkSpeed;

  DeviceInfo->Vid = 0xFFFF;
  DeviceInfo->Did = 0xFFFF;
  DeviceInfo->MaxLinkSpeed = 0;

  //
  // Check for device presence
  //
  if (!IsEndpointConnected (RpBase)) {
    return FALSE;
  }

  //
  // Assign temporary bus numbers to the root port
  //
  PciSegmentAndThenOr32 (
    RpBase + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET,
    (UINT32) ~B_PCI_BRIDGE_BNUM_SBBN_SCBN,
    ((UINT32) (TempPciBus << 8)) | ((UINT32) (TempPciBus << 16))
    );

  //
  // A config write is required in order for the device to re-capture the Bus number,
  // according to PCI Express Base Specification, 2.2.6.2
  // Write to a read-only register VendorID to not cause any side effects.
  //
  EpBase  = PCI_SEGMENT_LIB_ADDRESS (DEFAULT_PCI_SEGMENT_NUMBER_PCH, TempPciBus, 0, 0, 0);
  PciSegmentWrite16 (EpBase + PCI_VENDOR_ID_OFFSET, 0);

  Data32 = PciSegmentRead32 (EpBase + PCI_VENDOR_ID_OFFSET);
  DeviceInfo->Vid = (UINT16) (Data32 & 0xFFFF);
  DeviceInfo->Did = (UINT16) (Data32 >> 16);

  EpLinkSpeed = 0;
  EpPcieCapPtr = PcieFindCapId (DEFAULT_PCI_SEGMENT_NUMBER_PCH, TempPciBus, 0, 0, EFI_PCI_CAPABILITY_ID_PCIEXP);
  if (EpPcieCapPtr != 0) {
    EpLinkSpeed = PciSegmentRead8 (EpBase + EpPcieCapPtr + R_PCIE_LCAP_OFFSET) & B_PCIE_LCAP_MLS;
  }
  DeviceInfo->MaxLinkSpeed = EpLinkSpeed;

  //
  // Clear bus numbers
  //
  PciSegmentAnd32 (RpBase + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, (UINT32) ~B_PCI_BRIDGE_BNUM_SBBN_SCBN);

  DEBUG ((DEBUG_INFO, "VID: %04X DID: %04X  MLS: %d\n",
          DeviceInfo->Vid, DeviceInfo->Did, DeviceInfo->MaxLinkSpeed));

  return (Data32 != 0xFFFFFFFF);
}

/**
  Program controller power management settings.
  This settings are relevant to all ports including disabled ports.
  All registers are located in the first port of the controller.
  Use sideband access since primary may not be available.

  @param[in]  RpIndex               The root port to be initialized (zero based).
  @param[in]  TrunkClockGateEn      Indicates whether trunk clock gating is to be enabled,
                                    requieres all controller ports to have dedicated CLKREQ#
                                    or to be disabled.
**/
VOID
PcieConfigureControllerBasePowerManagement (
  IN  UINT32   RpIndex,
  IN  BOOLEAN  TrunkClockGateEn
  )
{
  UINT32                      Data32And;
  UINT32                      Data32Or;
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;

  DEBUG ((DEBUG_INFO, "PcieConfigureControllerBasePowerManagement(%d)\n", RpIndex));

  ASSERT ((RpIndex % PCH_PCIE_CONTROLLER_PORTS) == 0);

  PcieGetRpDev (RpIndex, &RpDevPrivate);

  Data32Or  =  B_PCIE_CFG_CCFG_DCGEISMA;
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_CCFG, Data32Or);
  //
  // Clock gating programming for the PCIE controller
  // Set E1h[5] to 1b to enable PCIe Link clock trunk gating.
  // Set E1h[4] to 1b to enable PCIe backbone clock trunk gating.
  // Set E1h[2] to 1b to enable shared resource dynamic backbone clock gating.
  // Set E1h[6] to 1b to enable Partition/Trunk Oscillator clock gating; if all ports on the controller support CLKREQ#.
  // Set E2h[4] to 1b to enable side clock gating.
  //
  Data32Or  = (B_PCH_PCIE_CFG_RPDCGEN_LCLKREQEN |
               B_PCH_PCIE_CFG_RPDCGEN_BBCLKREQEN | B_PCH_PCIE_CFG_RPDCGEN_SRDBCGEN) << 8;
  Data32Or |= B_PCH_PCIE_CFG_RPPGEN_SEOSCGE << 16;
  if (RpDevPrivate.RpDev.SipVersion < PcieSip17) {
    Data32Or |= (B_PCH_PCIE_CFG_RPDCGEN_RPSCGEN << 8);
  }
  if (TrunkClockGateEn) {
    DEBUG ((DEBUG_INFO, "Setting PTOCGE\n"));
    Data32Or |= (B_PCH_PCIE_CFG_RPDCGEN_PTOCGE << 8);
  }
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_SPR, Data32Or);

  //
  // Enable Dynamic and Trunk Clock Gating Coupling Mode
  //
  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    Data32Or = B_PCH_PCIE_AECR1G3_DTCGCM;
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_AECR1G3, Data32Or);
    ///
    /// Progarm (R_PCIE_HWSNR) 5F0h
    /// Set 5F0h[3:0] to 0101b for Bank Enable Pulse Width.
    /// Set 5F0h[7:4] to 01b for Restore Enable Pulse Width. 01b = 2 clocks.
    /// Set 5F0h[9:8] to 10b for Entry and Exit hysteresis. 10b = 16clocks.
    ///
    Data32And = (UINT32)~(B_PCIE_CFG_HWSNR_BEPW_MASK | B_PCIE_CFG_HWSNR_REPW_MASK | B_PCIE_CFG_HWSNR_EEH_MASK);
    Data32Or = V_PCIE_CFG_HWSNR_BEPW_8CLK | (0x1 << N_PCIE_CFG_HWSNR_REPW_OFFSET) | (0x2 << N_PCIE_CFG_HWSNR_EEH_OFFSET);
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_HWSNR, Data32And, Data32Or);
    ///
    /// Program (R_PCIE_PGCTRL) 5F4h
    /// Set 5F4h[2:0] to 010b for PM_REQ Block Response Time.
    /// 010b = 10 micro seconds for 1LM & 101b = 25 micro seconds for 2LM(PEG60).
    ///
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_PGCTRL, ~0u, V_PCIE_CFG_PGCTRL_PMREQBLKRSPT_10US);
  }

  //
  // Power gating programming for the PCIE controller
  //
  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    //
    // Set PM Request Controller Power Gating Exit Hysteresis to 0us
    //
    Data32And = (UINT32)~(B_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH_MASK);
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->And32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_PCIEPMECTL3, Data32And);

    ///
    /// Set Controller Power Gating Exit Hysteresis to 0us.
    /// Set Controller Power Gating Entry Hysteresis to 0us.
    ///
    Data32And = (UINT32) ~(B_PCH_PCIE_CFG_PCIEPMECTL2_CPGEXH_MASK | B_PCH_PCIE_CFG_PCIEPMECTL2_CPGENH_MASK);
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->And32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCIEPMECTL2, Data32And);
  }

  ///
  /// Program (R_PCIE_CFG_ACRG3) 6CCh
  /// Set 6CCh[23:22] to 00b to disable the CPG Wake Control.
  ///
  Data32And = (UINT32) ~(B_PCIE_CFG_ACRG3_CPGWAKECTRL_MASK);
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->And32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_ACRG3, Data32And);

  ///
  /// Set E8h[17,15] to [1,1]
  ///
  Data32Or = B_PCH_PCIE_CFG_PWRCTL_WPDMPGEP | B_PCH_PCIE_CFG_PWRCTL_DBUPI;
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PWRCTL, Data32Or);

  ///
  /// Set F5h[1:0] to 11b  (R_PCH_PCIE_CFG_PHYCTL2)
  /// Set F7h[3:2] = 00b   (R_PCH_PCIE_CFG_IOSFSBCS)
  ///
  Data32And = (UINT32) ~(B_PCH_PCIE_CFG_IOSFSBCS_SIID << 24);
  Data32Or = (B_PCH_PCIE_CFG_PHYCTL2_PXPG3PLLOFFEN | B_PCH_PCIE_CFG_PHYCTL2_PXPG2PLLOFFEN) << 8;
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, 0xF4, Data32And, Data32Or);

  ///
  /// Set 424h[11] to 1b
  ///
  Data32Or = B_PCH_PCIE_CFG_PCIEPMECTL2_PHYCLPGE;
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCIEPMECTL2, Data32Or);

  ///
  ///
  /// Set Hardware Autonomous Enable 428h[5] to 1b
  /// Set PMC Request Enable 428h[0] to 0b
  /// Set Sleep Enable to 1b for SIP version 17
  ///
  Data32And = (UINT32) ~B_PCH_PCIE_CFG_PCE_PMCRE;
  Data32Or = B_PCH_PCIE_CFG_PCE_HAE;
  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    Data32Or |= B_PCH_PCIE_CFG_PCE_SE;
  }
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCE, Data32And, Data32Or);

  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    //
    // Set L1.PG Auto Power Gate Enable 434h[4] to 0.
    //
    Data32And = (UINT32)~(B_PCIE_CFG_PCIEPMECTL3_L1PGAUTOPGEN);
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->And32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_PCIEPMECTL3, Data32And);

    //
    // Set PM_REQ Block Response Time to 10us
    //
    Data32And = (UINT32)~(B_PCIE_CFG_PGCTRL_PMREQBLKRSPT_MASK);
    Data32Or = V_PCIE_CFG_PGCTRL_PMREQBLKRSPT_10US;
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_PGCTRL, Data32And, Data32Or);

    //
    // Set PM_REQ Block Response Time to 10us
    //
    Data32And = (UINT32)~(B_PCIE_ADVMCTRL_PMREQBLKPGRSPT_MASK | B_PCIE_ADVMCTRL_PMREQCWC_MASK);
    Data32Or = ((V_PCIE_ADVMCTRL_PMREQCWC_LNK_PRIM << N_PCIE_ADVMCTRL_PMREQCWC_OFFSET) | (V_PCIE_ADVMCTRL_PMREQBLKPGRSPT_10US << N_PCIE_ADVMCTRL_PMREQBLKPGRSPT_OFFSET));
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_ADVMCTRL, Data32And, Data32Or);


    Data32Or = ((V_PCIE_CFG_HWSNR_EEH_16CLK << N_PCIE_CFG_HWSNR_EEH_OFFSET) | \
               (V_PCIE_CFG_HWSNR_REPW_2CLK << N_PCIE_CFG_HWSNR_REPW_OFFSET) | \
               V_PCIE_CFG_HWSNR_BEPW_8CLK);
    Data32And = (UINT32)~(B_PCIE_CFG_HWSNR_EEH_MASK | B_PCIE_CFG_HWSNR_BEPW_MASK | B_PCIE_CFG_HWSNR_REPW_MASK);
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_HWSNR, Data32And, Data32Or);
  }

  if (RpDevPrivate.RpDev.SipVersion < PcieSip17) {
    ///
    /// Set 42Ch[11:10] to 0x3
    ///
    Data32Or = BIT11 | BIT10;
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PGCBCTL1, Data32Or);
  }
}

/**
  Enables PCIe controller trunk clock gating.

  @param[in] RpIndex  Index of the controller on which to enable trunk clock gating.
**/
VOID
PcieEnableTrunkClockGate (
  IN UINT32 ControllerIndex
  )
{
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;

  PcieGetControllerDev (ControllerIndex, &RpDevPrivate);
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_SPR, (B_PCH_PCIE_CFG_RPDCGEN_PTOCGE << 8));
}

/**
  Configure power management settings whcih are applicable to both enabled and disabled ports.
  This settings are relevant to all ports including disabled ports.
  Use sideband access since primary may not be available.

  @param[in]  RpIndex               The root port to be initialized (zero based).
  @param[in]  PhyLanePgEnable       Indicates whether PHY lane power gating is to be enabled,
                                    requires CLKREQ# to supported by the port or the port to be disabled.
**/
VOID
PcieConfigurePortBasePowerManagement (
  IN  UINT32   RpIndex,
  IN  BOOLEAN  PhyLanePgEnable
  )
{
  UINT32                      Data32And;
  UINT32                      Data32Or;
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;
  UINT64                      RpBase;

  DEBUG ((DEBUG_INFO, "PchPcieConfigureBasePowerManagement(%d) Start\n", RpIndex));

  PcieGetRpDev (RpIndex, &RpDevPrivate);
  RpBase = PchPcieRpPciCfgBase (RpIndex);

  ///
  /// Program (R_PCIE_SPR) E0hc
  /// E0h[0] - Sticky Chicken Bit, clear this bit
  ///
  Data32And = (UINT32) ~BIT0;
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->And32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, 0xE0, Data32And);

  ///
  /// Set E1h[1:0] = 11b    (R_PCH_PCIE_CFG_RPDCGEN)
  ///

  if ((RpDevPrivate.RpDev.SipVersion == PcieSip17)) {
    Data32Or = (B_PCH_PCIE_CFG_RPDCGEN_RPDLCGEN) << 8;
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, 0xE0, Data32Or);

    Data32Or = (B_PCH_PCIE_CFG_RPDCGEN_RPDBCGEN) << 8;
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, 0xE0, Data32Or);
  } else {
    Data32Or = (B_PCH_PCIE_CFG_RPDCGEN_RPDLCGEN | B_PCH_PCIE_CFG_RPDCGEN_RPDBCGEN) << 8;
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, 0xE0, Data32Or);
  }

  ///
  /// Set F7h[6] to 1b     (R_PCH_PCIE_CFG_IOSFSBCS)
  ///
  Data32Or = B_PCH_PCIE_CFG_IOSFSBCS_SCPTCGE << 24;
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, 0xF4, Data32Or);

  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    //
    // Set 0x434[3:2] to 1 to set Osc Clock Gate Hysterisis to 1us
    //
    Data32And = (UINT32)~(B_PCH_PCIE_CFG_PCIEPMECTL3_OSCCGH_MASK | B_PCH_PCIE_CFG_PCIEPMECTL3_PMREQCPGEXH_MASK);
    Data32Or = V_PCH_PCIE_CFG_PCIEPMECTL3_OSCCGH_1US<< B_PCH_PCIE_CFG_PCIEPMECTL3_OSCCGH_OFFSET;
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCIEPMECTL3, Data32And, Data32Or);
    ConfigureDynamicClockGating (&RpDevPrivate.RpDev);
    //
    // Set RXCPPREALLOCEN, HPICTL FCTL2 [27,10]
    //
    RpDevPrivate.RpDev.PciSbiMsgMemAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgMemAccess, R_PCIE_RCRB_FCTL2, BIT27 | BIT10);  }

  ///
  /// Set 420h[31] = 1b
  /// If CLKREQ# is supported or port is disabled set 420h[30,29] to 11b.
  /// 420h[29] (DLSULDLSD) and 420h[0] must be programmed if DLSULPPGE is set or PTOCGE is set.
  /// Assume that if PTOCGE is set CLKREQ is supported on this port.
  /// L1.LOW is disabled; if all conditions are met, it will be enabled later.
  ///
  Data32Or = B_PCH_PCIE_CFG_PCIEPMECTL_FDPPGE;
  Data32And = (UINT32) ~(B_PCH_PCIE_CFG_PCIEPMECTL_L1LE | B_PCH_PCIE_CFG_PCIEPMECTL_DLSULDLSD | B_PCH_PCIE_CFG_PCIEPMECTL_L1FSOE);
  if (PhyLanePgEnable) {
    DEBUG ((DEBUG_INFO, "Setting DLSULPPGE+DLSULDLSD.\n"));
    Data32Or |= B_PCH_PCIE_CFG_PCIEPMECTL_DLSULPPGE;
    //
    // As per 4.4.4 Squelch Power Management section
    // DLSULDLSD 420h[29] = 0 SIP17, DLSULDLSD 420h[29] = 1 < SIP17
    // L1FSOE 420h[0] = 0 SIP17, 420h[0]=1 < SIP17
    //
    if ((RpDevPrivate.RpDev.SipVersion < PcieSip17)) {
      Data32Or |= B_PCH_PCIE_CFG_PCIEPMECTL_DLSULDLSD | B_PCH_PCIE_CFG_PCIEPMECTL_L1FSOE;
    } else {
      Data32Or |= B_PCIE_CFG_PCIEPMECTL_L1OCREWD | B_PCIE_CFG_PCIEPMECTL_POFFWT | B_PCIE_CFG_PCIEPMECTL_L1SNZCREWD;
    }
  }
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCIEPMECTL, Data32And, Data32Or);

  ///
  /// Set 424h[8,7] to 11b
  /// Disable Chassis PMC Save and Restore
  ///
  Data32And = (UINT32) ~0;
  Data32Or = B_PCH_PCIE_CFG_PCIEPMECTL2_FDCPGE | B_PCH_PCIE_CFG_PCIEPMECTL2_DETSCPGE | B_PCH_PCIE_CFG_PCIEPMECTL2_DISSCPGE;
  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    Data32And &= (UINT32) ~(B_PCH_PCIE_CFG_PCIEPMECTL2_CPGEXH_MASK | B_PCH_PCIE_CFG_PCIEPMECTL2_CPGENH_MASK);
    Data32Or |= B_PCH_PCIE_CFG_PCIEPMECTL2_CPMCSRE;
  } else {
    Data32And &= (UINT32)~(B_PCH_PCIE_CFG_PCIEPMECTL2_CPMCSRE);
  }
  RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_PCIEPMECTL2, Data32And, Data32Or);


  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {

    ///
    /// L1 Power Gating LTR Enable (L1PGLTREN) 0x5C0[0] 0
    /// L1 Power Gating LTR Threshold Latency Scale Value(L1PGLTRTLSV) 0x5C0[31:29] 0x2
    /// L1 Power Gating LTR Threshold Latency Value  (L1PGLTRTLV) 0x5C0[25:16] 0x32
    ///
    Data32And = (UINT32)(~(B_PCIE_CFG_PGTHRES_L1PGLTREN | B_PCIE_CFG_PGTHRES_L1PGLTRTLSV_MASK | B_PCIE_CFG_PGTHRES_L1PGLTRTLV_MASK));
    Data32Or = (0x2 << N_PCIE_CFG_PGTHRES_L1PGLTRTLSV_OFFSET | 0x32 << N_PCIE_CFG_PGTHRES_L1PGLTRTLV_OFFSET);
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCIE_CFG_PGTHRES, Data32And, Data32Or);
    //
    // Set PHYPG {4:0} to 0x1F
    //
    Data32Or = B_PCIE_CFG_RCRB_CFG_PHYPG_DLPPGP | B_PCIE_CFG_RCRB_CFG_PHYPG_DUCFGPHYPGE | B_PCIE_CFG_RCRB_CFG_PHYPG_L23PHYPGE | B_PCIE_CFG_RCRB_CFG_PHYPG_DISPHYPGE | B_PCIE_CFG_RCRB_CFG_PHYPG_DETPHYPGE;
    RpDevPrivate.RpDev.PciSbiMsgMemAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgMemAccess, R_PCIE_CFG_RCRB_CFG_PHYPG, BIT10);
  }

  ///
  /// Vnn Removal register programming
  ///
  if (IsVnnRemovalSupported ()) {
    Data32Or = (B_PCH_PCIE_CFG_VNNREMCTL_FDVNNRE | B_PCH_PCIE_CFG_VNNREMCTL_HPVNNRE | B_PCH_PCIE_CFG_VNNREMCTL_DNPVNNRE
      | B_PCH_PCIE_CFG_VNNREMCTL_RTD3VNNRE | B_PCH_PCIE_CFG_VNNREMCTL_LDVNNRE
      | (V_PCH_PCIE_CFG_VNNREMCTL_ISPLFVNNRE_16_OSC_CLK << N_PCH_PCIE_CFG_VNNREMCTL_ISPLFVNNRE) | (V_PCH_PCIE_CFG_VNNREMCTL_LRSLFVNNRE_8_OSC_CLK));
    RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgCfgAccess, R_PCH_PCIE_CFG_VNNREMCTL, Data32Or);
  }
  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    ///
    /// Program (R_PCIE_ADVMCTRL) 5BCh
    /// Set 5BCh[22] to 1b for InRxL0 Control.
    /// Set 5BCh[21] to 0b for Accumulator Reset Mode
    /// Set 5BCh[20] to 1b for EIOS Disable DeSkew.
    /// Set 5BCh[19] to 1b for EIOS Mask Receiver Datapath.
    /// Set 5BCh[15] to 0b for RxL0 DeAssertion Control
    /// Set 5BCh[13] to 1b for Gen3 Short TLP Framing Error Reporting.
    /// Set 5BCh[11] to 0b for Reset Receiver Link Layer Common Logic.
    /// Set 5BCh[10] to 0b for Reset Link Layer in Gen1, 2 Recovery.
    ///
    Data32And = (UINT32)~(B_PCIE_ADVMCTRL_ACCRM | B_PCIE_ADVMCTRL_RXL0DC | B_PCIE_ADVMCTRL_RRLLCL | B_PCIE_ADVMCTRL_RLLG12R);
    Data32Or = (UINT32)(B_PCIE_ADVMCTRL_INRXL0CTRL | B_PCIE_ADVMCTRL_EIOSDISDS | B_PCIE_ADVMCTRL_EIOSMASKRX | B_PCIE_ADVMCTRL_G3STFER| (6 << N_PCIE_ADVMCTRL_PMREQCWC_OFFSET) |
                       (V_PCIE_ADVMCTRL_PMREQBLKPGRSPT_10US << N_PCIE_ADVMCTRL_PMREQBLKPGRSPT_OFFSET));
    PciSegmentAndThenOr32 (RpBase + R_PCIE_ADVMCTRL, Data32And, Data32Or);
    ///
    /// Program (R_PCIE_PGTHRES) 5C0h.
    /// Set 5C0h[0] to 1b for L1 power gating LTR Enable.
    /// Set 5C0h[31:29] to 10b for L1 power gating LTR Threshold latency Scale value. 010b = 1024nS
    /// Set 5C0h[25:16] to 110010b for L1 power gating LTR Threshold latency value. 110010b (0x32).
    ///
    Data32Or = (0x2 << N_PCIE_CFG_PGTHRES_L1PGLTRTLSV_OFFSET) | (0x32 << N_PCIE_CFG_PGTHRES_L1PGLTRTLV_OFFSET);
    PciSegmentAndThenOr32 (RpBase + R_PCIE_CFG_PGTHRES, ~0u, Data32Or);
  }
}

/**
  Get max link width.

  @param[in] RpBase    Root Port base address
  @retval Max link width
**/
UINT8
GetMaxLinkWidth (
  UINT64  RpBase
  )
{
  UINT8  LinkWidth;
  LinkWidth = (UINT8) ((PciSegmentRead32 (RpBase + R_PCIE_CFG_LCAP) & B_PCIE_LCAP_MLW) >> N_PCIE_LCAP_MLW);
  ASSERT (LinkWidth <= 4);
  if (LinkWidth > 4) {
    LinkWidth = 4;
  }
  return LinkWidth;
}

/**
  Verify whether the PCIe port does own all lanes according to the port width.
  @param[in] RpBase    Root Port base address
**/
BOOLEAN
IsPciePortOwningLanes (
  IN     UINT64   RpBase
  )
{
  UINT32     MaxLinkWidth;
  UINT32     RpLaneIndex;
  UINT32     RpIndex;

  RpIndex      = PciePortIndex (RpBase);
  MaxLinkWidth = GetMaxLinkWidth (RpBase);
  for (RpLaneIndex = 0; RpLaneIndex < MaxLinkWidth; ++RpLaneIndex) {
    if (!PchFiaIsPcieRootPortLaneConnected (RpIndex, RpLaneIndex)) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
  Check for device presence with timeout.

  @param[in]     RpBase      Root Port base address
  @param[in]     TimeoutUs   Timeout in microseconds
  @param[in,out] Timer       Timer value, must be initialized to zero
                             before the first call of this function.
**/
BOOLEAN
IsPcieDevicePresent (
  IN     UINT64  RpBase,
  IN     UINT32  TimeoutUs,
  IN OUT UINT32  *Timer
  )
{
  while (TRUE) {
    if (IsEndpointConnected (RpBase)) {
      return TRUE;
    }
    if (*Timer < TimeoutUs) {
      MicroSecondDelay (10);
      *Timer += 10;
    } else {
      break;
    }
  }
  return FALSE;
}

/**
  Checks if given rootport should be left visible even though disabled, in order to avoid PCIE rootport swapping

  @param[in] RpIndex        rootport number
  @param[in] RpDisableMask  bitmask of all disabled rootports
  @param[in] PchPcieConfig  PCIe policy configuration

  @retval TRUE  port should be kept visible despite being disabled
  @retval FALSE port should be disabled and hidden

**/
BOOLEAN
IsPortForceVisible (
  IN UINT8                  RpIndex,
  IN UINT32                 RpDisableMask,
  IN CONST PCH_PCIE_CONFIG  *PchPcieConfig
  )
{
  UINT32 FunctionsEnabledPerDevice;
  UINT32 RpEnabledMask;

  //
  // only rootports mapped to Function0 are relevant for preventing rootport swap
  //
  if ((PchPcieConfig->PcieCommonConfig.RpFunctionSwap == 1) || (RpIndex % 8 != 0)) {
    return FALSE;
  }
  //
  // set a bit for each port that exists and isn't disabled
  //
  RpEnabledMask = (1u << GetPchMaxPciePortNum ()) - 1;
  RpEnabledMask &= (~RpDisableMask);

  FunctionsEnabledPerDevice = (RpEnabledMask >> ((RpIndex/8)*8)) & 0xFF;
  if (FunctionsEnabledPerDevice != 0) {
    return TRUE;
  }
  return FALSE;
}

/**
  Configure root port function number mapping

**/
VOID
PcieRpConfigureRpfnMapping (
  VOID
  )
{
  UINT8                                 PortIndex;
  UINT8                                 OriginalFuncZeroRp;
  UINT8                                 MaxPciePortNum;
  UINT8                                 FuncNum;
  UINT64                                RpBase;
  UINT32                                ControllerPcd[PCH_MAX_PCIE_CONTROLLERS];
  UINT32                                PcieControllers;
  UINT32                                ControllerIndex;
  UINT32                                FirstController;
  PCH_SBI_PID                           ControllerPid;
  PCIE_ROOT_PORT_DEV_PRIVATE            RpDevPrivate;

  DEBUG ((DEBUG_INFO,"PcieRpConfigureRpfnMapping () Start\n"));
  ZeroMem (ControllerPcd, sizeof (ControllerPcd));
  MaxPciePortNum = GetPchMaxPciePortNum ();

  PcieControllers = GetPchMaxPcieControllerNum ();

  for (ControllerIndex = 0; ControllerIndex < PcieControllers; ++ControllerIndex) {
    for (PortIndex = 0; PortIndex < MaxPciePortNum; ++PortIndex) {
      //
      // Initialize the PCI root port device structure.
      //
      PcieGetRpDev (PortIndex, &RpDevPrivate);
      if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
        ControllerPcd[ControllerIndex] = PchPcrRead32 (PchGetPcieControllerSbiPid (ControllerIndex), R_SPX_SIP16_PCR_PCD);
      } else {
        ControllerPcd[ControllerIndex] = PchPcrRead32 (PchGetPcieControllerSbiPid (ControllerIndex), R_SPX_PCR_PCD);
      }
    }
    DEBUG ((DEBUG_INFO, "SP%c = 0x%08x\n", 'A' + ControllerIndex, ControllerPcd[ControllerIndex]));
  }

  ///
  /// Configure root port function number mapping
  ///
  for (PortIndex = 0; PortIndex < MaxPciePortNum; ) {
    RpBase = PchPcieRpPciCfgBase (PortIndex);
    FuncNum = PchPcieRpFuncNumber (PortIndex);
    //
    // Search for first enabled function
    //
    if (PciSegmentRead16 (RpBase) != 0xFFFF) {
      if (FuncNum != 0) {
        //
        // First enabled root port that is not function zero will be swapped with function zero on the same device
        // RP PCD register must sync with PSF RP function config register
        //
        ControllerIndex    = PortIndex / 4;
        OriginalFuncZeroRp = (PortIndex / 8) * 8;
        FirstController    = OriginalFuncZeroRp / 4;

        //
        // The enabled root port becomes function zero
        //
        ControllerPcd[ControllerIndex] &= (UINT32) ~(B_SPX_PCR_PCD_RP1FN << ((PortIndex % 4) * S_SPX_PCR_PCD_RP_FIELD));
        ControllerPcd[ControllerIndex] |= 0u;
        //
        // Origianl function zero on the same device takes the numer of the current port
        //
        ControllerPcd[FirstController] &= (UINT32) ~B_SPX_PCR_PCD_RP1FN;
        ControllerPcd[FirstController] |= (UINT32) FuncNum;

        //
        // Program PSF1 RP function config register.
        //
        PsfSetPcieFunction (PsfGetPciePortData (OriginalFuncZeroRp), (UINT32) FuncNum, PsfGetSegmentTable ());
        PsfSetPcieFunction (PsfGetPciePortData (PortIndex), 0, PsfGetSegmentTable ());
      }
      //
      // Once enabled root port was found move to next PCI device
      //
      PortIndex = ((PortIndex / 8) + 1) * 8;
      continue;
    }
    //
    // Continue search for first enabled root port
    //
    PortIndex++;
  }

  //
  // Write to PCD and lock the register
  //
  for (ControllerIndex = 0; ControllerIndex < PcieControllers; ++ControllerIndex) {
    ControllerPid = PchGetPcieControllerSbiPid (ControllerIndex);
    for (PortIndex = 0; PortIndex < MaxPciePortNum; ++PortIndex) {
      PcieGetRpDev (PortIndex, &RpDevPrivate);
      if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
        PchPcrWrite32 (ControllerPid, R_SPX_SIP16_PCR_PCD, ControllerPcd[ControllerIndex] | B_SPX_PCR_PCD_SRL);
        DEBUG ((DEBUG_INFO, "SP%c = 0x%08x\n", 'A' + ControllerIndex, PchPcrRead32 (ControllerPid, R_SPX_SIP16_PCR_PCD)));
      } else {
        PchPcrWrite32 (ControllerPid, R_SPX_PCR_PCD, ControllerPcd[ControllerIndex] | B_SPX_PCR_PCD_SRL);
        DEBUG ((DEBUG_INFO, "SP%c = 0x%08x\n", 'A' + ControllerIndex, PchPcrRead32 (ControllerPid, R_SPX_PCR_PCD)));
      }
    }
  }
}

/**
  Checks integrity of Policy settings for all rootports.
  Triggers assert if anything is wrong. For debug builds only

  @param[in,out] PchPcieConfig     Pointer to PCH_PCIE_CONFIG instance
**/
VOID
PciePolicySanityCheck (
  IN OUT PCH_PCIE_CONFIG *PchPcieConfig
  )
{
  UINT8                  RpIndex;

  for (RpIndex = 0; RpIndex < GetPchMaxPciePortNum (); RpIndex++) {
    //
    // Ports with hotplug support must have SlotImplemented bit set
    //
    ASSERT (!PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.HotPlug || PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.SlotImplemented);
  }
}

/**
  This function provides BIOS workaround for WiFi device cannot enter S0i3 state due to LCTL.ECPM bit is cleared.
  This workaound is applied for Intel Wireless-AC 9260(Thunder Peak).
  This function does speed change earlier, so that Endpoint will be in correct state by the time
  root port and its downstream devices are initialized.

  @param[in] DevInfo Information on device that is connected to rootport
  @param[in] Speed   PCIe root port policy speed setting
  @param[in] RpBase  Root Port base address
**/
VOID
WifiLinkSpeedSyncWorkaround (
  IN PCIE_DEVICE_INFO DevInfo,
  IN UINT8            Speed,
  IN UINT64           RpBase
  )
{
  UINTN   TimeoutCount;

  if ((DevInfo.Vid == V_PCH_INTEL_VENDOR_ID) &&
      (DevInfo.Did == 0x2526) &&
      (Speed != PcieGen1)) {
    PciSegmentAndThenOr16 (
      RpBase + R_PCIE_CFG_LCTL2,
      (UINT16) ~B_PCIE_LCTL2_TLS,
      (UINT16) DevInfo.MaxLinkSpeed
      );

    // Retrain the Link
    PciSegmentOr16 (RpBase + R_PCIE_CFG_LCTL, B_PCIE_LCTL_RL);
    // 100 ms timeout while checking for link training is completed.
    for (TimeoutCount = 0; TimeoutCount < 1000; TimeoutCount++) {
      // Delay 100 us
      MicroSecondDelay (100);
      if ((PciSegmentRead16 (RpBase + R_PCIE_CFG_LSTS) & B_PCIE_LSTS_LT) == 0) {
        break;
      }
    }

    // 100 ms timeout while checking for link active on retrained link
    for (TimeoutCount = 0; TimeoutCount < 1000; TimeoutCount++) {
      // Delay 100 us
      MicroSecondDelay (100);
      if (IsLinkActive (RpBase)) {
        break;
      }
    }
  }
}

/**
  Lock SRL bits - lock it for all root ports

**/
STATIC
VOID
PchPcieRpSetSecuredRegisterLock (
  VOID
  )
{
  UINT32                                Data32Or;
  UINT64                                RpBase;
  UINT8                                 PortIndex;
  UINT8                                 MaxPciePortNum;
  PCIE_ROOT_PORT_DEV_PRIVATE            RpDevPrivate;

  DEBUG ((DEBUG_INFO, "PchPcieRpSetSecuredRegisterLock () Start!\n"));
  RpBase = 0;
  MaxPciePortNum = GetPchMaxPciePortNum ();

  for (PortIndex = 0; PortIndex < MaxPciePortNum; PortIndex++) {
    PcieGetRpDev (PortIndex, &RpDevPrivate);
    if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
      RpBase = PchPcieRpPciCfgBase (PortIndex);
      ///Secure Register Lock (SRL) 0xC8C[0]: 1
      ///Secure Equalization Register Lock (SERL) 0XC8C[8]: 1
      ///LTR Configuration Lock (LTRCFGLOCK) 0xC8C[16]: 1
      ///Device ID Override Lock (DIDOVR_LOCK) 0xC8C[24]: 1
      Data32Or = (B_PCIE_CFG_LPCR_SRL | B_PCIE_CFG_LPCR_SERL | B_PCIE_CFG_LPCR_DIDOVR_LOCK);
      PciSegmentOr32 (RpBase + R_PCIE_CFG_LPCR, Data32Or);
      DEBUG ((DEBUG_INFO, "R_PCIE_LPCR after setting SRL = %x\n", PciSegmentRead32 (RpBase + R_PCIE_CFG_LPCR)));

      ASSERT ((PciSegmentRead32 (RpBase + R_PCIE_CFG_LPCR) & B_PCIE_CFG_LPCR_SRL) == B_PCIE_CFG_LPCR_SRL);
    }
  }
  DEBUG ((DEBUG_INFO, "PchPcieRpSetSecuredRegisterLock () End!\n"));
}

/**
  Performs mandatory Root Port Initialization.
  This function is silicon-specific and configures proprietary registers.

  @param[in]  PortIndex                   The root port to be initialized (zero based)
  @param[in]  SiPolicy                    The SI Policy PPI
  @param[in]  SiPreMemPolicyPpi           The SI PreMem Policy PPI
  @param[in]  TempPciBus                  The temporary Bus number for root port initialization
  @param[in]  PcieGen3LinkEqSettings      PCIe link equalization settings to be used during EQ
  @param[in]  PcieGen4LinkEqSettings      PCIe link equalization settings to be used during EQ
  @param[out] MaxLinkSpeed                Reports if there's >= Gen3 capable endpoint connected to this rootport
**/
STATIC
VOID
InitPcieSingleRootPort (
  IN  UINT8                                     PortIndex,
  IN  CONST SI_POLICY_PPI                       *SiPolicy,
  IN  SI_PREMEM_POLICY_PPI                      *SiPreMemPolicyPpi,
  IN  UINT8                                     TempPciBus,
  IN  PCIE_LINK_EQ_SETTINGS                     *PcieGen3LinkEqSettings,
  IN  PCIE_LINK_EQ_SETTINGS                     *PcieGen4LinkEqSettings,
  IN  PCIE_LINK_EQ_SETTINGS                     *PcieGen5LinkEqSettings,
  OUT PCIE_SPEED                                *MaxLinkSpeed
  )
{
  EFI_STATUS                         Status;
  UINT64                             RpBase;
  UINT32                             Data32Or;
  UINT32                             Data32And;
  UINT16                             Data16;
  UINT16                             Data16Or;
  UINT16                             Data16And;
  UINT8                              Data8Or;
  UINT8                              Data8And;
  PCH_PCIE_CONFIG                    *PchPcieConfig;
  CONST PCIE_ROOT_PORT_COMMON_CONFIG *RootPortCommonConfig;
  VTD_CONFIG                         *VtdConfig;
  UINT32                             Tls;
  PCIE_DEVICE_INFO                   DevInfo;
  UINT32                             RpMaxPayloadCapability;
  PCIE_SPEED                         RootPortSpeed;
  UINT8                              InterruptPin;
  PCIE_ROOT_PORT_DEV_PRIVATE         RpDevPrivate;
  BOOLEAN                            DevicePresent;
  UINT8                              Offset;
  UINT32                             Data32;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gPchPcieConfigGuid, (VOID *) &PchPcieConfig);
  ASSERT_EFI_ERROR (Status);

  VtdConfig = NULL;
#if FixedPcdGetBool (PcdVtdEnable) == 1
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gVtdConfigGuid, (VOID *) &VtdConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  //
  // Initialize the PCI root port device structure.
  //
  PcieGetRpDev (PortIndex, &RpDevPrivate);

  RootPortCommonConfig = &PchPcieConfig->RootPort[PortIndex].PcieRpCommonConfig;

  DEBUG ((DEBUG_INFO, "InitPcieSingleRootPort (%d) Start \n", PortIndex));
  RpBase = PchPcieRpPciCfgBase (PortIndex);
  PcieGetRpDev (PortIndex, &RpDevPrivate);
  RootPortSpeed = PcieGen1;

  Tls = PciSegmentRead16 (RpBase + R_PCIE_CFG_LCTL2) & B_PCIE_LCTL2_TLS;
  ASSERT (Tls < V_PCIE_LCTL2_TLS_GEN3);

  /// PCH BIOS Spec Section 8.2.10 Completion Retry Status Replay Enable
  /// Following reset it is possible for a device to terminate the
  /// configuration request but indicate that it is temporarily unable to process it,
  /// but in the future. The device will return the Configuration Request Retry Status.
  /// By setting the Completion Retry Status Replay Enable, Dxx:Fn + 320h[22],
  /// the RP will re-issue the request on receiving such status.
  /// The BIOS shall set this bit before first configuration access to the endpoint.
  PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_PCIECFG2, B_PCH_PCIE_CFG_PCIECFG2_CRSREN);
  //
  // Set speed capability in rootport
  //
  Data8And = (UINT8)(~((UINT8)(B_PCH_PCIE_CFG_MPC_PCIESD >> 8)));
  Data8Or = 0;
  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    Offset = 9;
    switch (RootPortCommonConfig->PcieSpeed) {
      case PcieGen1:
        Data8Or |= (V_PCH_PCIE_CFG_MPC_PCIESD_GEN1 << (N_PCH_PCIE_CFG_MPC_PCIESD - Offset));
        break;
      case PcieGen2:
        Data8Or |= (V_PCH_PCIE_CFG_MPC_PCIESD_GEN2 << (N_PCH_PCIE_CFG_MPC_PCIESD - Offset));
        break;
      case PcieGen3:
        Data8Or |= (V_PCH_PCIE_CFG_MPC_PCIESD_GEN3 << (N_PCH_PCIE_CFG_MPC_PCIESD - Offset));
        break;
      case PcieGen4:
        Data8Or |= (V_PCH_PCIE_CFG_MPC_PCIESD_GEN4 << (N_PCH_PCIE_CFG_MPC_PCIESD - Offset));
        break;
      case PcieAuto:
        break;
    }
  } else {
    Offset = 8;
    switch (RootPortCommonConfig->PcieSpeed) {
      case PcieGen1:
        Data8Or |= (V_PCH_PCIE_CFG_MPC_PCIESD_GEN1 << (N_PCH_PCIE_CFG_MPC_PCIESD - Offset));
        break;
      case PcieGen2:
        Data8Or |= (V_PCH_PCIE_CFG_MPC_PCIESD_GEN2 << (N_PCH_PCIE_CFG_MPC_PCIESD - Offset));
        break;
      case PcieGen3:
      case PcieAuto:
        break;
    }
  }
  PciSegmentAndThenOr8 (RpBase + R_PCH_PCIE_CFG_MPC + 1, Data8And, Data8Or);

  DevicePresent = GetDeviceInfo (RpBase, TempPciBus, &DevInfo);
  RootPortSpeed = (PCIE_SPEED) GetMaxLinkSpeed (RpBase);
  if (RootPortCommonConfig->HotPlug && !DevicePresent) {
    *MaxLinkSpeed = RootPortSpeed;
  } else {
    *MaxLinkSpeed = MIN (RootPortSpeed, DevInfo.MaxLinkSpeed);
  }

  if ( NeedDecreasedDeEmphasis (DevInfo)) {
    PciSegmentOr32 (RpBase + R_PCIE_CFG_LCTL2, B_PCIE_LCTL2_SD);
  }

  WifiLinkSpeedSyncWorkaround (DevInfo, RootPortCommonConfig->PcieSpeed, RpBase);

  ///
  /// If only 128B max payload is supported set CCFG.UNRS to 0.
  ///
  /// If peer writes are supported set max payload size supported to 128B, clear CCFG.UPMWPD
  /// and program all the PCH Root Ports such that upstream posted writes and upstream non-posted requests
  /// are split at 128B boundary by setting CCFG fields: UPSD to 0, CCFG.UPRS to 000b and UNSD to 0, UNRS to 000b
  ///
  Data32And = ~0u;
  Data32Or  = 0;
  RpMaxPayloadCapability = PchPcieMaxPayload256;
  if (RootPortCommonConfig->MaxPayload == PchPcieMaxPayload128 ||
      RootPortCommonConfig->EnablePeerMemoryWrite) {
    RpMaxPayloadCapability = PchPcieMaxPayload128;

    Data32And &= (UINT32) ~(B_PCIE_CFG_CCFG_UNSD | B_PCIE_CFG_CCFG_UNRS);
    Data32Or  |= (UINT32)  (V_PCIE_CFG_CCFG_UNRS_128B << N_PCIE_CFG_CCFG_UNRS);

    if (RootPortCommonConfig->EnablePeerMemoryWrite) {
      Data32And &= (UINT32) ~(B_PCIE_CFG_CCFG_UPMWPD |
                              B_PCIE_CFG_CCFG_UPSD | B_PCIE_CFG_CCFG_UPRS);
      Data32Or  |= (UINT32)  (V_PCIE_CFG_CCFG_UPRS_128B << N_PCIE_CFG_CCFG_UPRS);
    }
  }
  if ((RootPortCommonConfig->MaxPayload == PchPcieMaxPayload256)) {
    Data32Or |= (V_PCIE_CFG_CCFG_UPRS_256B << N_PCIE_CFG_CCFG_UPRS);
  }
  ASSERT (RootPortCommonConfig->MaxPayload < PchPcieMaxPayloadMax);
  ///
  /// Set B0:Dxx:Fn + D0h [13:12] to 01b
  ///
  Data32And &= (UINT32) ~(B_PCIE_CFG_CCFG_UNRD | B_PCIE_CFG_CCFG_UPSD);

  PciSegmentAnd32 (RpBase + R_PCIE_CFG_CCFG, Data32And);

  PciSegmentAndThenOr16 (RpBase + R_PCIE_CFG_DCAP, (UINT16) ~B_PCIE_DCAP_MPS, (UINT16)RpMaxPayloadCapability);

  ///
  /// Program IPCS.IPCS F0h [10:8]
  /// ADP-S D2 PCIe  001b 256 bytes max payload size
  /// MTL-M D3 PCIe  111b 64 address aligned bytes max payload size
  ///
  Data32And = (UINT32)~(B_PCH_PCIE_CFG_IPCS_IMPS);
  if (RpDevPrivate.RpDev.SipVersion > PcieSip17) {
    Data32Or = (V_PCH_PCIE_CFG_IPCS_IMPS_64B << N_PCH_PCIE_CFG_IPCS_IMPS);
  } else {
    Data32Or = (V_PCH_PCIE_CFG_IPCS_IMPS_256B << N_PCH_PCIE_CFG_IPCS_IMPS);
  }
  PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_IPCS, Data32And, Data32Or);

  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    ///
    /// Program V0Ctl register for Sip 17 controller
    ///
    Data32Or = 0xFE;
    PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_VC0CTL, Data32Or);
    PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_VC1CTL, V_PCIE_CFG_VC1CTL_ID_ONE << B_PCIE_CFG_VC1CTL_ID_OFFSET);
  }

  ///
  /// PCH BIOS Spec Section 8.15.1 Power Optimizer Configuration
  /// If B0:Dxx:Fn + 400h is programmed, BIOS will also program B0:Dxx:Fn + 404h [1:0] = 11b,
  /// to enable these override values.
  /// - Fn refers to the function number of the root port that has a device attached to it.
  /// - Default override value for B0:Dxx:Fn + 400h should be 880F880Fh
  /// - Also set 404h[2] to lock down the configuration
  /// - Refer to table below for the 404h[3] policy bit behavior.
  /// Done in PcieSetPm()
  ///
  /// PCH BIOS Spec Section 8.15.1 Power Optimizer Configuration
  /// Program B0:Dxx:Fn + 64h [11] = 1b
  ///
  Data32Or = 0;
  Data32And = ~0u;
  if (RootPortCommonConfig->LtrEnable == TRUE) {
    Data32Or |= B_PCIE_DCAP2_LTRMS;
  } else {
    Data32And &= (UINT32) ~(B_PCIE_DCAP2_LTRMS);
  }
  ///
  /// PCH BIOS Spec Section 8.15.1 Power Optimizer Configuration
  /// Optimized Buffer Flush/Fill (OBFF) is not supported.
  /// Program B0:Dxx:Fn + 64h [19:18] = 0h
  ///
  Data32And &= (UINT32) ~B_PCIE_DCAP2_OBFFS;
  PciSegmentAndThenOr32 (RpBase + R_PCIE_DCAP2_OFFSET, Data32And, Data32Or);

  //
  // Squelch Power Management
  //
  ConfigurePcieSquelchPowerManagement (&RpDevPrivate.RpDev);

  //
  // Configure Phy Settings
  //
  PcieSipConfigurePhyInit (&RpDevPrivate.RpDev);

  ///
  /// PCH BIOS Spec Section 8.15.1 Power Optimizer Configuration
  /// Program B0:Dxx:Fn + 68h [10] = 1b
  ///
  Data16 = PciSegmentRead16 (RpBase + R_PCIE_CFG_DCTL2);
  if (RootPortCommonConfig->LtrEnable == TRUE) {
    Data16 |= B_PCIE_DCTL2_LTREN;
  } else {
    Data16 &= (UINT16) ~(B_PCIE_DCTL2_LTREN);
  }

  //
  // Program AEB and ARE
  //
  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    Data16 &= (UINT16) ~(B_PCIE_CFG_DCTL2_ARE | B_PCIE_CFG_DCTL2_AEB);
  } else {
    Data16 |= (UINT16) B_PCIE_CFG_DCTL2_AEB;
  }
  PciSegmentWrite16 (RpBase + R_PCIE_CFG_DCTL2, Data16);

  ///
  /// PCH BIOS Spec Section 8.15.1 Power Optimizer Configuration
  /// Step 3 done in PciExpressHelpersLibrary.c ConfigureLtr
  ///

  ///
  /// Set Dxx:Fn + 300h[23:00] = 0B75FA7h
  /// Set Dxx:Fn + 304h[11:00] = 0C97h
  ///
  PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_PCIERTP1, ~0x00FFFFFFu, 0x00B75FA7);
  PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_PCIERTP2, ~0x00000FFFu, 0x00000C97);

  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    ///
    /// Set Dxx:Fn + 0xC50[5] = 1 (ACGR3S2.SRT = 1)
    /// Set Dxx:Fn + 0xC50[6] = 1 (ACGR3S2.G1EBM = 1)
    /// Set Dxx:Fn + 0xC50[7] = 1 (ACGR3S2.G2EBM = 1)
    /// Set Dxx:Fn + 0xC50[8] = 1 (ACGR3S2.G3EBM = 1)
    /// Set Dxx:Fn + 0xC50[9] = 1 (ACGR3S2.G4EBM = 1)
    /// Set Dxx:Fn +0xC50[20] = 1 (ACGR3S2.G5EBM = 1)
    ///
    Data32Or = (B_PCH_PCIE_ACGR3S2_G4EBM | B_PCH_PCIE_ACGR3S2_G3EBM | B_PCH_PCIE_ACGR3S2_G2EBM | B_PCH_PCIE_ACGR3S2_G1EBM |
                B_PCH_PCIE_ACGR3S2_SRT);
    if (RpDevPrivate.RpDev.SipVersion > PcieSip17) {
      Data32Or |= B_PCH_PCIE_ACGR3S2_G5EBM;
    }
    PciSegmentOr32 (RpBase + R_PCH_PCIE_ACGR3S2, Data32Or);

    Data32And = ~(UINT32)(B_PCH_PCIE_AECR1G3_L1OFFRDYHEWT | B_PCH_PCIE_AECR1G3_L1OFFRDYHEWTEN);
    PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_AECR1G3, Data32And, B_PCH_PCIE_AECR1G3_TPSE);
  }

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set Dxx:Fn + 318h [31:16] = 1414h (Gen2 and Gen1 Active State L0s Preparation Latency)
  ///
  PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_PCIEL0SC, ~0xFFFF0000u, 0x14140000);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// 1.  Program Dxx:Fn + E8h[20] to [1]
  ///
  Data32Or = 0;
  if (RpDevPrivate.RpDev.SipVersion < PcieSip17) {
    Data32Or = B_PCH_PCIE_CFG_PWRCTL_LTSSMRTC;
  }

  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    Data32Or |= B_PCIE_CFG_PWRCTL_LIFECF;
  }
  PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_PWRCTL, Data32Or);
  ///
  /// 2.  Program Dxx:Fn + 320h[27] to [1]
  /// Enable PCIe Relaxed Order to always allow downstream completions to pass posted writes,
  /// 3.  Set B0:Dxx:Fn:320h[24:23] = 11b
  /// Set PME timeout to 10ms, by
  /// 4.  Set B0:Dxx:Fn:320h[21:20] = 01b
  ///

  Data32And = (UINT32) ~B_PCH_PCIE_CFG_PCIECFG2_PMET;
  Data32Or  = B_PCH_PCIE_CFG_PCIECFG2_CROAOV | B_PCH_PCIE_CFG_PCIECFG2_CROAOE;
  if (RpDevPrivate.RpDev.SipVersion < PcieSip17) {
    Data32Or |= (V_PCH_PCIE_CFG_PCIECFG2_PMET << N_PCH_PCIE_CFG_PCIECFG2_PMET);
  }

  PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_PCIECFG2, Data32And, Data32Or);

  ///
  /// Enable Completion Time-Out Non-Fatal Advisory Error, Dxx:Fn + 324h[14] = 1b
  /// Set LDSWQRP Dxx:Fn + 324h[13] = 0
  ///
  Data32And = (UINT32) ~(B_PCH_PCIE_CFG_PCIEDBG_LDSWQRP);
  Data32Or  = B_PCH_PCIE_CFG_PCIEDBG_CTONFAE;
  PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_PCIEDBG, Data32And, Data32Or);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Program Dxx:Fn + 424h [6, 5, 4] = [1, 1, 1]
  ///
  PciSegmentOr32 (
    RpBase + R_PCH_PCIE_CFG_PCIEPMECTL2,
    (B_PCH_PCIE_CFG_PCIEPMECTL2_L23RDYSCPGE |
     B_PCH_PCIE_CFG_PCIEPMECTL2_L1SCPGE)
    );
  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// If Dxx:Fn + F5h[0] = 1b or step 3 is TRUE, set Dxx:Fn + 4Ch[17:15] = 4h
  /// Else set Dxx:Fn + 4Ch[17:15] = 010b
  ///
  Data32And = (UINT32) (~B_PCIE_LCAP_EL1);
  if (RpDevPrivate.RpDev.SipVersion != PcieSip17) {
    Data32Or = 4 << N_PCIE_LCAP_EL1;
  } else {
    Data32Or = (6 << N_PCIE_LCAP_EL1);
  }

  ///
  /// Set LCAP APMS according to platform policy.
  ///
  if (RootPortCommonConfig->Aspm < PchPcieAspmAutoConfig) {
    Data32And &= (UINT32) ~B_PCIE_LCAP_APMS;
    Data32Or  |= RootPortCommonConfig->Aspm << N_PCIE_LCAP_APMS;
  } else {
    if (RpDevPrivate.RpDev.SipVersion != PcieSip17) {
      Data32Or |= B_PCIE_LCAP_APMS_L0S | B_PCIE_LCAP_APMS_L1;
    } else {
      DEBUG ((DEBUG_INFO, "Programming LCAP to L1 only \n"));
      Data32Or |= B_PCIE_LCAP_APMS_L1;
    }
  }

  //
  // The EL1, ASPMOC and APMS of LCAP are RWO, must program all together.
  //
  PciSegmentAndThenOr32 (RpBase + R_PCIE_CFG_LCAP, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "LCAP = 0x%x \n", PciSegmentRead32 ((RpBase + R_PCIE_CFG_LCAP))));

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Configure PCI Express Number of Fast Training Sequence for each port
  /// 1.  Set Dxx:Fn + 314h [31:24, 23:16, 15:8, 7:0] to [7Eh, 70h, 3Fh, 38h]
  /// 2.  Set Dxx:Fn + 478h [31:24, 15:8, 7:0] to [28h, 3Dh, 2Ch]
  ///
  if (RpDevPrivate.RpDev.SipVersion != PcieSip17) {
    Data32 = 0x7E703F38;
  } else {
    Data32 = 0x7E5B3F2C;
  }
  PciSegmentWrite32 (RpBase + R_PCH_PCIE_CFG_PCIENFTS, Data32);
  if (RpDevPrivate.RpDev.SipVersion != PcieSip17) {
    Data32 =  0x28003D2C;
  } else {
    Data32 = 0x2800402C;
  }
  PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_G3L0SCTL, 0x00FF0000u, Data32);

  //
  // Set Dxx.Fn + 310h [31:24, 23:22, 15:8, 7:0] to [0x28, 0x3, 0x80, 0x5B]
  //
  if (RpDevPrivate.RpDev.SipVersion == PcieSip17) {
    PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_G4L0SCTL, 0x28C0805B);
  }
  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set MPC.IRRCE, Dxx:Fn + D8h[25] = 1b using byte access
  /// For system that support MCTP over PCIE set
  /// Set PCIE RP PCI offset D8h[27] = 1b
  /// Set PCIE RP PCI offset D8h[3] = 1b
  ///
  Data8And = (UINT8) (~(B_PCH_PCIE_CFG_MPC_IRRCE | B_PCH_PCIE_CFG_MPC_MMBNCE) >> 24);
  Data8Or = B_PCH_PCIE_CFG_MPC_MMBNCE >> 24;
  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    Data8Or |= (UINT8) (B_PCIE_CFG_MPC_FCDL1E >> 24);
  }
  if (VtdConfig == NULL || VtdConfig->VtdDisable) {
    Data8Or |= B_PCH_PCIE_CFG_MPC_IRRCE >> 24;
  }
  PciSegmentAndThenOr8 (RpBase + R_PCH_PCIE_CFG_MPC + 3, Data8And, Data8Or);

  Data8And = (UINT8) ~(B_PCH_PCIE_CFG_MPC_MCTPSE);
  Data8Or  = B_PCH_PCIE_CFG_MPC_MCTPSE;
  PciSegmentAndThenOr8 (RpBase + R_PCH_PCIE_CFG_MPC, Data8And, Data8Or);

  ///
  /// Peer Disable. Starting from SIP16
  ///
  PcieSipConfigurePeerDisable (&RpDevPrivate.RpDev, TRUE, TRUE);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set PCIE RP PCI offset F5h[7:4] = 0000b
  ///
  PciSegmentAnd8 (RpBase + R_PCH_PCIE_CFG_PHYCTL2, (UINT8) ~(B_PCH_PCIE_CFG_PHYCTL2_TDFT | B_PCH_PCIE_CFG_PHYCTL2_TXCFGCHGWAIT));

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Enable PME_TO Time-Out Policy, Dxx:Fn + E2h[6] =1b
  ///
  PciSegmentOr8 (RpBase + R_PCH_PCIE_CFG_RPPGEN, B_PCH_PCIE_CFG_RPPGEN_PTOTOP);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// If there is no IOAPIC behind the root port, set EOI Forwarding Disable bit (PCIE RP PCI offset D4h[1]) to 1b.
  /// For Empty Hot Plug Slot, set is done in InitPcieSingleRootPort ()
  ///

  ///
  /// System bios should initiate link retrain for all slots that has card populated after register restoration.
  /// Done in PchPciExpressHelpersLibrary.c PchPcieInitRootPortDownstreamDevices ()
  ///

  ///
  /// Configure Completion Timeout
  ///
  Data16And = (UINT16) ~(B_PCIE_DCTL2_CTD | B_PCIE_DCTL2_CTV);
  Data16Or  = 0;
  if (RootPortCommonConfig->CompletionTimeout == PcieCompletionTO_Disabled) {
    Data16Or = B_PCIE_DCTL2_CTD;
  } else {
    switch (RootPortCommonConfig->CompletionTimeout) {
      case PcieCompletionTO_Default:
        Data16Or = V_PCIE_DCTL2_CTV_DEFAULT;
        break;

      case PcieCompletionTO_16_55ms:
        Data16Or = V_PCIE_DCTL2_CTV_40MS_50MS;
        break;

      case PcieCompletionTO_65_210ms:
        Data16Or = V_PCIE_DCTL2_CTV_160MS_170MS;
        break;

      case PcieCompletionTO_260_900ms:
        Data16Or = V_PCIE_DCTL2_CTV_400MS_500MS;
        break;

      case PcieCompletionTO_1_3P5s:
        Data16Or = V_PCIE_DCTL2_CTV_1P6S_1P7S;
        break;

      default:
        Data16Or = 0;
        break;
    }
  }

  PciSegmentAndThenOr16 (RpBase + R_PCIE_CFG_DCTL2, Data16And, Data16Or);

  PcieSipConfigure10BitTag (&RpDevPrivate.RpDev, FALSE, FALSE);
  PcieConfigureCoalescing (&RpDevPrivate.RpDev);
  ///
  /// For Root Port Slots Numbering on the CRBs.
  ///
  Data32Or  = 0;
  Data32And = (UINT32) (~(B_PCIE_SLCAP_SLV | B_PCIE_SLCAP_SLS | B_PCIE_SLCAP_PSN));
  ///
  /// PCH BIOS Spec section 8.8.2.1
  /// Note: If Hot Plug is supported, then write a 1 to the Hot Plug Capable (bit6) and Hot Plug
  /// Surprise (bit5) in the Slot Capabilities register, PCIE RP PCI offset 54h. Otherwise,
  /// write 0 to the bits PCIe Hot Plug SCI Enable
  ///
  Data32And &= (UINT32) (~(B_PCIE_SLCAP_HPC | B_PCIE_SLCAP_HPS));
  if (RootPortCommonConfig->HotPlug) {
    Data32Or |= B_PCIE_SLCAP_HPC | B_PCIE_SLCAP_HPS;
  }
  ///
  /// Get the width from LCAP
  /// Slot Type  X1  X2/X4/X8/X16
  /// Default     10W   25W
  /// The slot power consumption and allocation is platform specific. Please refer to the
  /// "PCI Express* Card Electromechanical (CEM) Spec" for details.
  ///
  if (RootPortCommonConfig->SlotPowerLimitValue != 0) {
    Data32Or |= (UINT32) (RootPortCommonConfig->SlotPowerLimitValue << N_PCIE_SLCAP_SLV);
    Data32Or |= (UINT32) (RootPortCommonConfig->SlotPowerLimitScale << N_PCIE_SLCAP_SLS);
  } else {
    if (GetMaxLinkWidth (RpBase) == 1) {
      Data32Or |= (UINT32) (100 << N_PCIE_SLCAP_SLV);
      Data32Or |= (UINT32) (1 << N_PCIE_SLCAP_SLS);
    } else if (GetMaxLinkWidth (RpBase) >= 2) {
      Data32Or |= (UINT32) (250 << N_PCIE_SLCAP_SLV);
      Data32Or |= (UINT32) (1 << N_PCIE_SLCAP_SLS);
    }
  }

  ///
  /// PCH BIOS Spec section 8.2.4
  /// Initialize Physical Slot Number for Root Ports
  ///
  Data32Or |= (UINT32) (RootPortCommonConfig->PhysicalSlotNumber << N_PCIE_SLCAP_PSN);
  PciSegmentAndThenOr32 (RpBase + R_PCIE_CFG_SLCAP, Data32And, Data32Or);
  if (RpDevPrivate.RpDev.SipVersion != PcieSip17) {
    InitCapabilityList (PortIndex, RpBase, RootPortCommonConfig);
  } else {
    PchPcieSip17InitCapabilityList (PortIndex, RpBase, RootPortCommonConfig);
  }

  //
  // All actions involving LinkDisable must finish before anything is programmed on endpoint side
  // because LinkDisable resets endpoint
  //

  ///
  /// Perform equalization for Gen3 capable ports
  ///
  if (RootPortSpeed >= 3) {
    //
    // Link EQ has to be performed with CCC set if supported.
    //
    EnableCommonClock (PortIndex, TempPciBus);
    PcieSipLinkEqualize (&RpDevPrivate.RpDev,
                         DevicePresent,
                         PcieGen3LinkEqSettings,
                         PcieGen4LinkEqSettings,
                         PcieGen5LinkEqSettings,
                         RootPortSpeed,
                         DevInfo.MaxLinkSpeed);
  }
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set "Link Speed Training Policy", Dxx:Fn + D4h[6] to 1.
  /// Make sure this is after mod-PHY related programming is completed.
  PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_MPC2, B_PCH_PCIE_CFG_MPC2_LSTP);

  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Step 29 If Transmitter Half Swing is enabled, program the following sequence
  /// a. Ensure that the link is in L0.
  /// b. Program the Link Disable bit (0x50[4]) to 1b.
  /// c. Program the Analog PHY Transmitter Voltage Swing bit (0xE8[13]) to set the transmitter swing to half/full swing
  /// d. Program the Link Disable bit (0x50[4]) to 0b.
  /// BIOS can only enable this on SKU where GEN3 capability is disabled on that port
  if (RootPortSpeed < V_PCIE_LCAP_MLS_GEN3 && RootPortCommonConfig->TransmitterHalfSwing) {
    PciSegmentOr8 (RpBase + R_PCIE_CFG_LCTL, B_PCIE_LCTL_LD);
    while (IsLinkActive (RpBase)) {
      // wait until link becomes inactive before changing swing
    }
    PciSegmentOr16 (RpBase + R_PCH_PCIE_CFG_PWRCTL, B_PCH_PCIE_CFG_PWRCTL_TXSWING);
    PciSegmentAnd8 (RpBase + R_PCIE_CFG_LCTL, (UINT8) ~(B_PCIE_LCTL_LD));
  }
  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set "Poisoned TLP Non-Fatal Advisory Error Enable", Dxx:Fn + D4h[12] to 1
  /// Set "L1 Substate Exit SCI Enable (L1SSESE)", Dxx:Fn + D4[30] to 1
  /// Set "Disable PLL Early Wake on L1 Substate Exit (DISPLLEWL1SE)", Dxx:Fn + D4[16] to 1
  ///
  Data32Or = B_PCH_PCIE_CFG_MPC2_PTNFAE | B_PCH_PCIE_CFG_MPC2_L1SSESE | B_PCH_PCIE_CFG_MPC2_DISPLLEWL1SE;
  PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_MPC2, Data32Or);

  //
  // L1LOW LTR threshold latency value
  //
  PciSegmentAndThenOr32 (
    RpBase + R_PCH_PCIE_CFG_PCIEPMECTL,
    (UINT32) ~B_PCH_PCIE_CFG_PCIEPMECTL_L1LTRTLV,
    (V_PCH_PCIE_CFG_PCIEPMECTL_L1LTRTLV << N_PCH_PCIE_CFG_PCIEPMECTL_L1LTRTLV)
    );

  ///
  /// Additional configurations
  ///
  ///
  /// Configure Error Reporting policy in the Device Control Register
  ///
  Data16And = (UINT16) (~(B_PCIE_DCTL_URE | B_PCIE_DCTL_FEE | B_PCIE_DCTL_NFE | B_PCIE_DCTL_CEE));
  Data16Or  = 0;

  if (RootPortCommonConfig->UnsupportedRequestReport) {
    Data16Or |= B_PCIE_DCTL_URE;
  }

  if (RootPortCommonConfig->FatalErrorReport) {
    Data16Or |= B_PCIE_DCTL_FEE;
  }

  if (RootPortCommonConfig->NoFatalErrorReport) {
    Data16Or |= B_PCIE_DCTL_NFE;
  }

  if (RootPortCommonConfig->CorrectableErrorReport) {
    Data16Or |= B_PCIE_DCTL_CEE;
  }

  PciSegmentAndThenOr16 (RpBase + R_PCIE_CFG_DCTL, Data16And, Data16Or);

  ///
  /// Configure Interrupt / Error reporting in R_PCIE_CFG_RCTL
  ///
  Data16And = (UINT16) (~(B_PCIE_RCTL_SFE | B_PCIE_RCTL_SNE | B_PCIE_RCTL_SCE));
  Data16Or  = 0;

  if (RootPortCommonConfig->SystemErrorOnFatalError) {
    Data16Or |= B_PCIE_RCTL_SFE;
  }

  if (RootPortCommonConfig->SystemErrorOnNonFatalError) {
    Data16Or |= B_PCIE_RCTL_SNE;
  }

  if (RootPortCommonConfig->SystemErrorOnCorrectableError) {
    Data16Or |= B_PCIE_RCTL_SCE;
  }

  PciSegmentAndThenOr16 (RpBase + R_PCIE_CFG_RCTL, Data16And, Data16Or);

  ///
  /// Root PCI-E Powermanagement SCI Enable
  ///
  if (RootPortCommonConfig->PmSci) {
    ///
    /// PCH BIOS Spec section 8.7.3 BIOS Enabling of Intel PCH PCI Express* PME SCI Generation
    /// Step 1
    /// Make sure that PME Interrupt Enable bit, Dxx:Fn:Reg 5Ch[3] is cleared
    ///
    PciSegmentAnd16 (RpBase + R_PCIE_CFG_RCTL, (UINT16) (~B_PCIE_RCTL_PIE));

    ///
    /// Step 2
    /// Program Misc Port Config (MPC) register at PCI config space offset
    /// D8h as follows:
    /// Set Power Management SCI Enable bit, Dxx:Fn:Reg D8h[31]
    /// Clear Power Management SMI Enable bit, Dxx:Fn:Reg D8h[0]
    /// Use Byte Access to avoid RWO bit
    ///
    PciSegmentAnd8 (RpBase + R_PCH_PCIE_CFG_MPC, (UINT8) (~B_PCH_PCIE_CFG_MPC_PMME));
  }
  if (RootPortCommonConfig->HotPlug) {
    ///
    /// PCH BIOS Spec section 8.8.2.1
    /// Step 1
    /// Clear following status bits, by writing 1b to them, in the Slot
    /// Status register at offset 1Ah of PCI Express Capability structure:
    /// Presence Detect Changed (bit3)
    ///
    PciSegmentAnd16 (RpBase + R_PCIE_CFG_SLSTS, B_PCIE_SLSTS_PDC);
    ///
    /// Step 2
    /// Program the following bits in Slot Control register at offset 18h
    /// of PCI Express* Capability structure:
    /// Presence Detect Changed Enable (bit3) = 1b
    /// Hot Plug Interrupt Enable (bit5) = 0b
    ///
    PciSegmentAndThenOr16 (RpBase + R_PCIE_CFG_SLCTL, (UINT16) (~B_PCIE_SLCTL_HPE), B_PCIE_SLCTL_PDE);
    ///
    /// Step 3
    /// Program Misc Port Config (MPC) register at PCI config space offset
    /// D8h as follows:
    /// Hot Plug SCI Enable (HPCE, bit30) = 0b
    /// Use byte access to avoid premature locking BIT23, SRL
    ///
    PciSegmentAnd8 (RpBase + R_PCH_PCIE_CFG_MPC + 3, (UINT8) ~(B_PCH_PCIE_CFG_MPC_HPCE >> 24));
    ///
    /// PCH BIOS Spec section 8.9
    /// BIOS should mask the reporting of Completion timeout (CT) errors by setting
    /// the uncorrectable Error Mask register PCIE RP PCI offset 108h[14].
    ///
    PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_EX_UEM, B_PCIE_EX_UEM_CT);
  }

  ///
  /// PCH BIOS Spec Section 8.10 PCI Bus Emulation & Port80 Decode Support
  /// The I/O cycles within the 80h-8Fh range can be explicitly claimed
  /// by the PCIe RP by setting MPC.P8XDE, PCI offset D8h[26] = 1 (using byte access)
  /// BIOS must also configure the corresponding DMI registers GCS.RPR and GCS.RPRDID
  /// to enable DMI to forward the Port8x cycles to the corresponding PCIe RP
  ///
  if (PchPcieConfig->PcieCommonConfig.EnablePort8xhDecode && ((PortIndex + 1) == (UINT8)PchPcieConfig->PchPciePort8xhDecodePortIndex)) {
    PciSegmentOr8 (RpBase + R_PCH_PCIE_CFG_MPC + 3, (UINT8) (B_PCH_PCIE_CFG_MPC_P8XDE >> 24));
    PchIoPort80DecodeSet (PortIndex);
  }
  //
  // Initialize R/WO Registers that described in PCH BIOS Spec
  //
  ///
  /// SRL bit is write-once and lock, so SRL, UCEL and CCEL must be programmed together
  /// otherwise UCEL/CCEL programming would lock SRL prematurely in wrong state
  ///
  /// PCH BIOS Spec Section 8.15 Additional PCI Express* Programming Steps
  /// Set Common Clock Exit Latency,      Dxx:Fn + D8h[17:15] = 4h
  /// Set Non-common Clock Exit Latency,  Dxx:Fn + D8h[20:18] = 7h
  ///
  Data32And = ~(UINT32) (B_PCH_PCIE_CFG_MPC_UCEL | B_PCH_PCIE_CFG_MPC_CCEL);
  Data32Or  = (7 << N_PCH_PCIE_CFG_MPC_UCEL) | (4 << N_PCH_PCIE_CFG_MPC_CCEL);
  if (RpDevPrivate.RpDev.SipVersion != PcieSip17) {
      Data32Or |= B_PCH_PCIE_CFG_MPC_SRL;
    PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_MPC, Data32And, Data32Or);
    //
    // Check if SRL bit actually got programmed. If not, then it means some code accessed MPC register earlier and locked it
    //
    ASSERT ( (PciSegmentRead32 (RpBase + R_PCH_PCIE_CFG_MPC) & B_PCH_PCIE_CFG_MPC_SRL) == B_PCH_PCIE_CFG_MPC_SRL);
  } else {
    PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_MPC, Data32And, Data32Or);
  }

  ///
  /// Configure Root Port interrupt
  ///
  InterruptPin = ItssGetDevIntPin (SiPolicy, PchPcieRpDevNumber (PortIndex), PchPcieRpFuncNumber (PortIndex));

  Data32And = (UINT32) ~B_PCH_PCIE_CFG_STRPFUSECFG_PXIP;
  Data32Or = (UINT32) (InterruptPin << N_PCH_PCIE_CFG_STRPFUSECFG_PXIP);
  PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_CFG_STRPFUSECFG, Data32And, Data32Or);

  //
  // Set Gpio Assertion on Link Down      Dxx:Fn + 0x690h[10] = 1
  //
  PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_GDR, B_PCH_PCIE_CFG_GDR_GPIOALD);

  ///
  /// Dynamic Link Throttling
  ///
  Data32Or = (V_PCH_PCIE_CFG_TNPT_TP_3_MS << N_PCH_PCIE_CFG_TNPT_TP);
  PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_TNPT, Data32Or);

  PcieSipConfigureRetimerSupport (&RpDevPrivate.RpDev);

  ///
  /// Set Completion Timer Timeout Policy (CRSPSEL) to 0
  ///
  if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
    RpDevPrivate.RpDev.PciSbiMsgMemAccess->And32 (RpDevPrivate.RpDev.PciSbiMsgMemAccess, R_PCIE_RCRB_FCTL, (UINT32) ~B_PCIE_RCRB_FCTL_CRSPSEL);
  }

  ///
  /// Set Error Injection Disable (EINJDIS) to 1
  ///
  PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_EINJCTL, B_PCH_PCIE_CFG_EINJCTL_EINJDIS);
}

/**
  Configures PTM in PCIe controller

  @param[in] ControllerIndex  Index of the PCIe controller
**/
STATIC
VOID
PcieConfigurePtm (
  IN UINT32  ControllerIndex
  )
{
  PTM_CONFIGURATION           PtmConfig;
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;

  PcieGetControllerDev (ControllerIndex, &RpDevPrivate);
  PcieGetProjectPtmConfiguration (ControllerIndex, &PtmConfig);
  PcieSipConfigurePtm (&RpDevPrivate.RpDev, &PtmConfig);
}

/**
  Configure LTR subtraction for PCH PCIe controller.

  @param[in] ControllerIndex  Index of the controller on the PCH
**/
STATIC
VOID
PcieConfigureLtrSubtraction (
  IN UINT32  ControllerIndex
  )
{
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;
  LTR_SUBTRACTION_CONFIG      L1StandardConfig = {1, 0x2, 0x41, 1, 0x2, 0x41};
  LTR_SUBTRACTION_CONFIG      L1p1Config = {1, 0x2, 0x41, 1, 0x2, 0x41};
  LTR_SUBTRACTION_CONFIG      L1p2Config = {1, 0x2, 0xD7, 1, 0x2, 0xD7};
  LTR_SUBTRACTION_CONFIG      LtrSubL11Npg = {0x1, 0x2, 0x2D, 0x1, 0x2, 0x2D};

  PcieGetControllerDev (ControllerIndex, &RpDevPrivate);
  if (RpDevPrivate.RpDev.SipVersion < PcieSip16) {
    return;
  }

  PcieSipConfigureLtrSubstraction (&RpDevPrivate.RpDev, &L1StandardConfig, &L1p1Config, &L1p2Config, &LtrSubL11Npg);
}

/**
  Configure PCIe power down mapping

  @param[in] RpIndex               RootPort Index
**/
VOID
PcieConfigurePowerDownMapping (
  IN  UINT8                         RpIndex
  )
{
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;
  UINT32                      Data32And;

  DEBUG ((DEBUG_INFO, "PcieConfigurePowerDownMapping() Start\n"));
  Data32And = (UINT32) ~ (BIT2 | BIT1);
  PcieGetControllerDev ((RpIndex / PCH_PCIE_CONTROLLER_PORTS), &RpDevPrivate);

  RpDevPrivate.RpDev.PciSbiMsgMemAccess->Write32 (RpDevPrivate.RpDev.PciSbiMsgMemAccess, R_PCIE_RCRB_PIPEPDCTL, 0x44333333);
  RpDevPrivate.RpDev.PciSbiMsgMemAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgMemAccess, R_PCIE_RCRB_PIPEPDCTL2, 0x33444406);
  RpDevPrivate.RpDev.PciSbiMsgMemAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgMemAccess, R_PCIE_RCRB_PIPEPDCTL3, 0x44);
  RpDevPrivate.RpDev.PciSbiMsgMemAccess->And32 (RpDevPrivate.RpDev.PciSbiMsgMemAccess, R_PCIE_RCRB_PIPEPDCTLEXT, Data32And);

  DEBUG ((DEBUG_INFO, "PcieConfigurePowerDownMapping() End\n"));
}

/**
  Program Rx primary Cycle Decode Registers for SIP17

  @param[in] RpIndex     Root Port Index
**/
VOID
Pcie17RxMasterCycleDecode (
  UINT32 RpIndex
)
{
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;
  UINT32                      Data32Or;
  UINT32                      Data32And;

  DEBUG ((DEBUG_INFO, "PcieSip17RxMasterCycleDecode for RpIndex %x\n", RpIndex));
  PcieGetRpDev (RpIndex, &RpDevPrivate);

  ///
  /// Program (R_PCIE_RCRB_CFG_DECCTL) 0X1904
  /// Program (ATSCE) 0X1904[1]                0x1
  /// Program (BUSNUMZCHK) 0X1904[2]           0x1
  /// Program (MTRXLTC) 0X1904[3]              0x0
  /// Program (OUTBEXPCPLCHKEN) 0X1904[4]      0x1
  /// Program (DROPCPLATNZCE) 0X1904[5]        0x1
  /// Program (LCRXINT) 0X1904[6]              0x1
  /// Program (VDMATAC) 0X1904[7]              0x1
  /// Program (URVDME16DW) 0X1904[8]           0x1
  /// Program (URRXUVDMINTELVID) 0X1904[9]     0x0
  /// Program (URRXUVDMUVID) 0X1904[10]        0x0
  /// Program (URRXURTRCVDM) 0X1904[11]        0x1
  /// Program (URRXULTVDM) 0X1904[12]          0x1
  /// Program (URRXURAVDM) 0X1904[13]          0x1
  /// Program (URRXURIDVDM) 0X1904[14]         0x1
  /// Program (URRXUORVDM) 0X1904[15]          0x1
  /// Program (URRXVMCTPBFRC) 0X1904[16]       0x1
  /// Program (ICHKINVREQRMSGRBID) 0X1904[17]  0x1
  /// Program (RXMCTPDECEN) 0X1904[18]         0x1
  /// Program (RXVDMDECE) 0X1904[19]           0x0
  /// Program (RXGPEDECEN) 0X1904[20]          0x1
  /// Program (RXSBEMCAPDECEN) 0X1904[21]      0x1
  /// Program (RXADLEDDECEN) 0X1904[22]        0x1
  /// Program (RXLTRMDECH) 0X1904[23]          0x0
  /// Program (LCRXERRMSG) 0X1904[24]          0x1
  /// Program (RSVD_RW) 0X1904[25]             0x1
  /// Program (LCRXPTMREQ) 0X1904[26]          0x1
  /// Program (URRXUVDMRBFRC) 0X1904[27]       0x1
  /// Program (URRXUVDMRGRTRC) 0X1904[28]      0x1
  /// Program (RXMCTPBRCDECEN) 0X1904[29]      0x1
  /// Program (URRXMCTPNTCO) 0x1904[30]        0x1
  /// Program (RXIMDECEN) 0X1904[31]           0x0
  ///
  Data32And = (UINT32) ~(B_PCIE_RCRB_CFG_DECCTL_MTRXLTC | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMINTELVID | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMUVID | B_PCIE_RCRB_CFG_DECCTL_RXVDMDECE |
                         B_PCIE_RCRB_CFG_DECCTL_RXIMDECEN | B_PCIE_RCRB_CFG_DECCTL_RXLTRMDECH);
  Data32Or = (UINT32) (B_PCIE_RCRB_CFG_DECCTL_ATSCE | B_PCIE_RCRB_CFG_DECCTL_BUSNUMZCHK | B_PCIE_RCRB_CFG_DECCTL_OUTBEXPCPLCHKEN | B_PCIE_RCRB_CFG_DECCTL_DROPCPLATNZCE |
                       B_PCIE_RCRB_CFG_DECCTL_LCRXINT | B_PCIE_RCRB_CFG_DECCTL_VDMATAC | B_PCIE_RCRB_CFG_DECCTL_URVDME16DW | B_PCIE_RCRB_CFG_DECCTL_URRXURTRCVDM |
                       B_PCIE_RCRB_CFG_DECCTL_URRXULTVDM | B_PCIE_RCRB_CFG_DECCTL_URRXURAVDM | B_PCIE_RCRB_CFG_DECCTL_URRXURIDVDM | B_PCIE_RCRB_CFG_DECCTL_URRXUORVDM |
                       B_PCIE_RCRB_CFG_DECCTL_URRXVMCTPBFRC | B_PCIE_RCRB_CFG_DECCTL_ICHKINVREQRMSGRBID | B_PCIE_RCRB_CFG_DECCTL_RXMCTPDECEN | B_PCIE_RCRB_CFG_DECCTL_RXGPEDECEN |
                       B_PCIE_RCRB_CFG_DECCTL_RXSBEMCAPDECEN | B_PCIE_RCRB_CFG_DECCTL_RXADLEDDECEN | B_PCIE_RCRB_CFG_DECCTL_LCRXERRMSG | B_PCIE_RCRB_CFG_DECCTL_RSVD_RW |
                       B_PCIE_RCRB_CFG_DECCTL_LCRXPTMREQ | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMRBFRC | B_PCIE_RCRB_CFG_DECCTL_URRXUVDMRGRTRC | B_PCIE_RCRB_CFG_DECCTL_RXMCTPBRCDECEN |
                       B_PCIE_RCRB_CFG_DECCTL_URRXMCTPNTCO);
  RpDevPrivate.RpDev.PciSbiMsgMemAccess->AndThenOr32 (RpDevPrivate.RpDev.PciSbiMsgMemAccess, R_PCIE_RCRB_CFG_DECCTL, Data32And, Data32Or);

  ///
  /// Program (R_PCIE_RCRB_CFG_PIDECCTL) 0X190C
  /// Program (CPLBNCHK) 0X190C[0]  0x1
  ///
  RpDevPrivate.RpDev.PciSbiMsgMemAccess->Or32 (RpDevPrivate.RpDev.PciSbiMsgMemAccess, R_PCIE_RCRB_CFG_PIDECCTL, (UINT32)(B_PCIE_RCRB_CFG_PIDECCTL_CPLBNCHK));
}

/*
  Perform some early Pcie Root port configuration to enable RCRB and program decoder control registers
*/
VOID
EarlyPcieRootPortConfig (
  VOID
 )
{
  UINT8                              RpIndex;
  UINT8                              MaxPciePortNum;
  UINT64                             RpBase;
  PCIE_ROOT_PORT_DEV_PRIVATE         RpDevPrivate;

  MaxPciePortNum = GetPchMaxPciePortNum ();
  DEBUG ((DEBUG_INFO, "EarlyPcieRootPortConfig Start.\n"));
  for (RpIndex = 0; RpIndex < MaxPciePortNum; RpIndex++) {
    PcieGetRpDev (RpIndex, &RpDevPrivate);
    if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
      RpBase = PchPcieRpPciCfgBase (RpIndex);
      PciSegmentOr32 (RpBase + R_PCH_PCIE_CFG_DC, B_PCH_PCIE_CFG_DC_COM);
      Pcie17RxMasterCycleDecode (RpIndex);
    }
  }
  DEBUG ((DEBUG_INFO, "EarlyPcieRootPortConfig End.\n"));
}

/**
  Perform Initialization of the Downstream Root Ports.

  @param[in] SiPolicy             The SI Policy PPI
**/
VOID
PcieRpInit (
  IN CONST SI_POLICY_PPI     *SiPolicy
  )
{
  PCH_PCIE_CONFIG             *PchPcieConfig;
  SI_PREMEM_POLICY_PPI        *SiPreMemPolicyPpi;
  EFI_STATUS                  Status;
  UINT8                       RpIndex;
  UINT64                      RpBase;
  UINT8                       MaxPciePortNum;
  UINT32                      RpDisableMask;
  UINT32                      RpClkreqMask;
  UINT32                      Timer;
  UINT32                      DetectTimeoutUs;
  PCIE_SPEED                  MaxDeviceSpeed[PCH_MAX_PCIE_ROOT_PORTS];
  BOOLEAN                     KeepPortVisible;
  UINT8                       TempPciBusMin;
  UINT8                       TempPciBusMax;
  UINT32                      ControllerIndex;
  UINT32                      MaxPcieControllerNum;
#if FixedPcdGetBool (PcdHybridStorageSupport) == 1
  UINT32                      HybridStorageLocation;
#endif
  PCIE_LINK_EQ_SETTINGS       PcieGen3LinkEqSettings[PCH_MAX_PCIE_ROOT_PORTS];
  PCIE_LINK_EQ_SETTINGS       PcieGen4LinkEqSettings[PCH_MAX_PCIE_ROOT_PORTS];
  PCIE_LINK_EQ_SETTINGS       PcieGen5LinkEqSettings[PCH_MAX_PCIE_ROOT_PORTS];
  PCIE_ROOT_PORT_DEV_PRIVATE  RpDevPrivate;
  UINT32                      Data32And;
  UINT32                      Data32Or;
  PSF_RELAXED_ORDER_REGS*     PsfRelaxedOrderRegs;
  PCH_PCIE_CLOCK_USAGE        ClockUsage;
  UINT8                       ControllerRp0;

  DEBUG ((DEBUG_INFO, "PcieRpInit() Start\n"));

#if FixedPcdGetBool (PcdHybridStorageSupport) == 1
  HybridStorageLocation = GetHybridStorageLocation ();
#endif
  TempPciBusMin = PcdGet8 (PcdSiliconInitTempPciBusMin);
  TempPciBusMax = PcdGet8 (PcdSiliconInitTempPciBusMax);
  Status = GetConfigBlock ((VOID *) SiPolicy, &gPchPcieConfigGuid, (VOID *) &PchPcieConfig);
  ASSERT_EFI_ERROR (Status);

  DEBUG_CODE_BEGIN ();
  PciePolicySanityCheck (PchPcieConfig);
  DEBUG_CODE_END ();

  Timer            = 0;
  MaxPciePortNum   = GetPchMaxPciePortNum ();
  MaxPcieControllerNum = GetPchMaxPcieControllerNum ();
  RpDisableMask    = 0;
  RpClkreqMask     = 0;

  ZeroMem (PcieGen3LinkEqSettings, PCH_MAX_PCIE_ROOT_PORTS * sizeof (PCIE_LINK_EQ_SETTINGS));
  ZeroMem (PcieGen4LinkEqSettings, PCH_MAX_PCIE_ROOT_PORTS * sizeof (PCIE_LINK_EQ_SETTINGS));
  ZeroMem (PcieGen5LinkEqSettings, PCH_MAX_PCIE_ROOT_PORTS * sizeof (PCIE_LINK_EQ_SETTINGS));

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);

  PchFiaSetClockOutputDelay ();

  if (GetPcieImrPortLocation () == ImrRpLocationPch) {
    EnablePcieImr (GetPcieImrPortNumber (), BUS_NUMBER_FOR_IMR);
  }

  for (RpIndex = 0; RpIndex < MaxPciePortNum; RpIndex++) {
    RpBase = PchPcieRpPciCfgBase (RpIndex);
    PcieGetRpDev (RpIndex, &RpDevPrivate);
    ClockUsage = PchClockUsagePchPcie0 + RpIndex;
    //
    // Setting MMT in BAR0 for SIP 17 RP controller to 00b.
    //
    if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
      PciSegmentAnd32 (RpBase + PCI_BASE_ADDRESSREG_OFFSET, (UINT32)~(BIT1 | BIT2));
    }

    PciSegmentAnd32 (RpBase + R_PCH_PCIE_AECR1G3, (UINT32) ~B_PCIE_CFG_AECR1G3_DCDCCTDT);
    //
    // Update RpClkreqMask value for the root port where hybrid storage device is connected
    // Since hybrid storage device has two devices which share a common clock and clockreq,
    // we check if RpIndex is corresponding to the third root port and override clkreq mask to be same as for the first root port of the controller.
    //
#if FixedPcdGetBool (PcdHybridStorageSupport) == 1
    if ((HybridStorageLocation & (BIT0 << (RpIndex / 4))) && (RpIndex % 4 == 2)) {
      if (IsClkReqAssigned (ClockUsage - 2)) {
        RpClkreqMask |= (BIT0 << RpIndex);
      }
    } else if (GetHybridStoragePchRpForCpuAttach () == RpIndex) {
      //
      // Enable CLKREQ# regardless of port being available/enabled to allow clock gating.
      //
      ClockUsage = PchClockUsageCpuPcie0 + GetHybridStorageCpuRpLocation();
      if (IsClkReqAssigned (ClockUsage)) {
        RpClkreqMask |= (BIT0 << RpIndex);
      }
    } else {
      if (IsClkReqAssigned(ClockUsage)) {
        RpClkreqMask |= (BIT0 << RpIndex);
      }
    }
#else
    //
    // Enable CLKREQ# regardless of port being available/enabled to allow clock gating.
    //
    if (IsClkReqAssigned (ClockUsage)) {
      RpClkreqMask |= (BIT0 << RpIndex);
    }
#endif
    //
    // Determine available ports based on lane ownership and port configuration (x1/x2/x4)
    // Root ports can be already disabled by PchEarlyDisabledDeviceHandling
    //
    if (PciSegmentRead16 (RpBase) == 0xFFFF) {
      RpDisableMask |= (BIT0 << RpIndex);
      continue;
    }
    if (IsPciePortOwningLanes (RpBase) == FALSE) {
      RpDisableMask |= (BIT0 << RpIndex);
      continue;
    }

    if (GetMaxLinkSpeed (RpBase) >= 3) {
      PcieGetProjectDefaultEqConfiguration (RpIndex, &PcieGen3LinkEqSettings[RpIndex], &PcieGen4LinkEqSettings[RpIndex], &PcieGen5LinkEqSettings[RpIndex]);
      if (PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.OverrideEqualizationDefaults) {
        CopyMem (&PcieGen3LinkEqSettings[RpIndex].PlatformSettings, &PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings, sizeof (PCIE_LINK_EQ_PLATFORM_SETTINGS));
        CopyMem (&PcieGen4LinkEqSettings[RpIndex].PlatformSettings, &PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen4LinkEqPlatformSettings, sizeof (PCIE_LINK_EQ_PLATFORM_SETTINGS));
        CopyMem (&PcieGen5LinkEqSettings[RpIndex].PlatformSettings, &PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen5LinkEqPlatformSettings, sizeof (PCIE_LINK_EQ_PLATFORM_SETTINGS));
      }
      PcieSipLinkEqualizeInit (&RpDevPrivate.RpDev, &PcieGen3LinkEqSettings[RpIndex], &PcieGen4LinkEqSettings[RpIndex], &PcieGen5LinkEqSettings[RpIndex]);
      if ((RpIndex % 4 != 0) && (RpDevPrivate.RpDev.SipVersion == PcieSip17)) {
        ControllerRp0 = ((RpIndex / 4) * 4);
        PcieGetRpDev (ControllerRp0, &RpDevPrivate);
        PcieGetProjectDefaultEqConfiguration (ControllerRp0, &PcieGen3LinkEqSettings[ControllerRp0], &PcieGen4LinkEqSettings[ControllerRp0], &PcieGen5LinkEqSettings[ControllerRp0]);
        if (PchPcieConfig->RootPort[ControllerRp0].PcieRpCommonConfig.OverrideEqualizationDefaults) {
          CopyMem (&PcieGen3LinkEqSettings[ControllerRp0].PlatformSettings, &PchPcieConfig->RootPort[ControllerRp0].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings, sizeof (PCIE_LINK_EQ_PLATFORM_SETTINGS));
          CopyMem (&PcieGen4LinkEqSettings[ControllerRp0].PlatformSettings, &PchPcieConfig->RootPort[ControllerRp0].PcieRpCommonConfig.PcieGen4LinkEqPlatformSettings, sizeof (PCIE_LINK_EQ_PLATFORM_SETTINGS));
          CopyMem (&PcieGen5LinkEqSettings[ControllerRp0].PlatformSettings, &PchPcieConfig->RootPort[ControllerRp0].PcieRpCommonConfig.PcieGen5LinkEqPlatformSettings, sizeof (PCIE_LINK_EQ_PLATFORM_SETTINGS));
        }
        PcieSipLinkEqualizeInit (&RpDevPrivate.RpDev, &PcieGen3LinkEqSettings[ControllerRp0], &PcieGen4LinkEqSettings[ControllerRp0], &PcieGen5LinkEqSettings[ControllerRp0]);
      }
    }

    ///
    /// Set the Slot Implemented Bit.
    /// PCH BIOS Spec section 8.2.3, The System BIOS must
    /// initialize the "Slot Implemented" bit of the PCI Express* Capabilities Register,
    /// XCAP Dxx:Fn:42h[8] of each available and enabled downstream root port.
    /// Ports with hotplug capability must have SI bit set
    /// The register is write-once so must be written even if we're not going to set SI, in order to lock it.
    ///
    /// This must happen before code reads PresenceDetectState, because PDS is invalid unless SI is set
    ///
    if (PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.SlotImplemented || PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.HotPlug) {
      PciSegmentOr16 (RpBase + R_PCIE_CFG_XCAP, B_PCIE_XCAP_SI);
    } else {
      PciSegmentAnd16 (RpBase + R_PCIE_CFG_XCAP, (UINT16)(~B_PCIE_XCAP_SI));
    }

    ///
    /// For non-hotplug ports disable the port if there is no device present.
    ///
    DetectTimeoutUs = PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.DetectTimeoutMs * 1000;
    if (IsPcieDevicePresent (RpBase, DetectTimeoutUs, &Timer)) {
      DEBUG ((DEBUG_INFO, "Port %d has a device attached.\n", RpIndex));
      //
      // At this point in boot, CLKREQ pad is still configured as GP input and doesnt' block clock generation
      // regardless of input state. Before switching it to native mode when it will start gating clock, we
      // verify if CLKREQ is really connected. If not, pad will not switch and power management
      // will be disabled in rootport.
      // By the time this code runs device can't have CPM or L1 substates enabled, so it is guaranteed to pull ClkReq down.
      // If ClkReq is detected to be high anyway, it means ClkReq is not connected correctly.
      // Checking pad's input value is primarily a measure to prevent problems with long cards inserted into short
      // open-ended PCIe slots on motherboards which route PRSNT signal to CLKREQ. Such config causes CLKREQ signal to float.
      //
      if (!PcieDetectClkreq (RpIndex, PchPcieConfig)) {
        RpClkreqMask &= ~(BIT0 << RpIndex);
      }
    } else {
      if (PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.HotPlug == FALSE) {
        RpDisableMask |= (BIT0 << RpIndex);
      }
    }
    if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
      //
      // Perform Power down mapping before enabling powermanagement
      //
      PcieConfigurePowerDownMapping (RpIndex);
    }
  }

  for (RpIndex = 0; RpIndex < MaxPciePortNum; RpIndex++) {
    if (RpClkreqMask & (BIT0 << RpIndex)) {
      //
      // Enabled CLKREQ# pad if supported to allow clock gating regardless of port being enabled.
      //
      ClockUsage = PchClockUsagePchPcie0 + RpIndex;
#if FixedPcdGetBool (PcdHybridStorageSupport) == 1
        if (GetHybridStoragePchRpForCpuAttach() == RpIndex) {
          //
          // Enable CLKREQ# regardless of port being available/enabled to allow clock gating.
          //
          ClockUsage = PchClockUsageCpuPcie0 + GetHybridStorageCpuRpLocation();
        }
#endif
      EnableClkReq (ClockUsage);

    }

    ///
    /// Configure power management applicable to all port including disabled ports.
    ///
      if ((RpIndex % PCH_PCIE_CONTROLLER_PORTS) == 0) {
        //
        // TrunkClockGateEn depends on each of the controller ports supporting CLKREQ# or being disabled.
        //
        PcieConfigureControllerBasePowerManagement (
          RpIndex,
          1
          );
      }
      //
      // PhyLanePgEnable depends on the port supporting CLKREQ# or being disabled.
      //
      PcieConfigurePortBasePowerManagement (
        RpIndex,
        1
       );
  }

  //
  // Wait for all ports with PresenceDetect=1 to form a link
  // Having an active link is necessary to access and configure the endpoint
  // We cannot use results of IsPcieDevicePresent() because it checks PDS only and may include
  // PCIe cards that never form a link, such as compliance load boards.
  //
  WaitForLinkActive (MaxPciePortNum, RpDisableMask);
  ///
  /// For each controller set Initialize Transaction Layer Receiver Control on Link Down
  /// and Initialize Link Layer Receiver Control on Link Down.
  /// Use sideband access in case 1st port of a controller is disabled
  ///
  /// This is a survivability setting to recover system from crash on surprise removal
  ///
  for (RpIndex = 0; RpIndex < MaxPciePortNum; ++RpIndex) {
    if ((RpIndex % PCH_PCIE_CONTROLLER_PORTS) == 0) {
      PcieGetRpDev (RpIndex, &RpDevPrivate);
      if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
        Data32And = (UINT32)~(B_PCH_PCIE_CFG_SIP17_PCIEALC_PDSP);
        Data32Or  = (UINT32)(B_PCH_PCIE_CFG_SIP17_PCIEALC_RTD3PDSP);
      } else {
        Data32And = (UINT32)~0u;
        Data32Or  = (UINT32)(B_PCH_PCIE_CFG_PCIEALC_ITLRCLD | B_PCH_PCIE_CFG_PCIEALC_ILLRCLD | B_PCH_PCIE_CFG_PCIEALC_RTD3PDSP);
      }

      RpDevPrivate.RpDev.PciSbiMsgCfgAccess->AndThenOr32 (
        RpDevPrivate.RpDev.PciSbiMsgCfgAccess,
        R_PCH_PCIE_CFG_PCIEALC,
        Data32And,
        Data32Or
        );
      RpDevPrivate.RpDev.PciSbiMsgCfgAccess->Or32 (
        RpDevPrivate.RpDev.PciSbiMsgCfgAccess,
        R_PCH_PCIE_CFG_DC,
        B_PCH_PCIE_CFG_DC_COM
        );
    }
  }

  //
  // PTM settings are global per PCIe controller.
  //
  for (ControllerIndex = 0; ControllerIndex < MaxPcieControllerNum; ControllerIndex++) {
    PcieConfigurePtm (ControllerIndex);
    PcieConfigureLtrSubtraction (ControllerIndex);
  }

  for (RpIndex = 0; RpIndex < MaxPciePortNum; ++RpIndex) {
    if (RpDisableMask & (BIT0 << RpIndex)) {
      KeepPortVisible = IsPortForceVisible (RpIndex, RpDisableMask, PchPcieConfig);
      DisablePcieRootPort (RpIndex, KeepPortVisible);
    } else {
      InitPcieSingleRootPort (
        RpIndex,
        SiPolicy,
        SiPreMemPolicyPpi,
        TempPciBusMin,
        &PcieGen3LinkEqSettings[RpIndex],
        &PcieGen4LinkEqSettings[RpIndex],
        &PcieGen5LinkEqSettings[RpIndex],
        &MaxDeviceSpeed[RpIndex]
        );
      ///
      /// Initialize downstream devices
      ///
      RootportDownstreamConfiguration (
        DEFAULT_PCI_SEGMENT_NUMBER_PCH,
        DEFAULT_PCI_BUS_NUMBER_PCH,
        PchPcieRpDevNumber (RpIndex),
        PchPcieRpFuncNumber (RpIndex),
        TempPciBusMin,
        TempPciBusMax,
        EnumPchPcie
        );
    }
  }
  ///
  /// Clear GPE0 Register PCI_EXP_STS and HOT_PLUG_STS by writing 1
  ///
  IoWrite32 (
    PmcGetAcpiBase () + R_ACPI_IO_GPE0_STS_127_96,
    B_ACPI_IO_GPE0_STS_127_96_PCI_EXP | B_ACPI_IO_GPE0_STS_127_96_HOT_PLUG
    );

  ///
  /// If SCI is enabled in any port, Set BIOS_PCI_EXP_EN bit, PMC PCI offset A0h[10],
  /// to globally enable the setting of the PCI_EXP_STS bit by a PCI Express* PME event.
  ///
  for (RpIndex = 0; RpIndex < MaxPciePortNum; RpIndex++) {
    if (PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PmSci) {
      PmcEnablePciExpressPmeEvents (PmcGetPwrmBase ());
      break;
    }
  }

  ///
  /// PCH BIOS Spec Section 8.2.9
  /// Enable PCIe Relaxed Order to always allow downstream completions to pass posted writes.
  /// To enable this feature configure DMI and PSF:
  ///
    PchDmiEnablePcieRelaxedOrder ();

  PsfRelaxedOrderRegs = GetPsfPortRelaxedOrderingTables ();
  PsfEnablePcieRelaxedOrder (PsfRelaxedOrderRegs);


  //
  // Rx margin programming as per BWG
  //
  for (RpIndex = 0; RpIndex < MaxPciePortNum; ++RpIndex) {
    PcieGetRpDev (RpIndex, &RpDevPrivate);
    if (RpDevPrivate.RpDev.SipVersion >= PcieSip17) {
      RpBase = PchPcieRpPciCfgBase (RpIndex);
      /// Program R_PCIE_RXMC1[C90]
      /// Set C90[26:31] MSRVS to 0x1F
      /// Set C90[20:25] MSRTS to 0x3F
      /// Set C90[13:14] TMSLOP to 0x1
      /// Set C90[11:12] VMSLOP to 0x1
      /// Set C90[9] MSRM to 0x0
      /// Set C90[4:8] MMNOLS to 0x1
      /// Set C90[3] MVS to 0x0
      /// Set C90[2] MILRTS to 0x0
      /// Set C90[2] MIUDVMS to 0x0
      /// Set C90[2] MIESS to 0x0
      ///
      Data32And = (UINT32) ~(B_PCH_PCIE_RXMC1_MMNOLS_MASK | B_PCH_PCIE_RXMC1_VMSLOP_MASK | B_PCH_PCIE_RXMC1_TMSLOP_MASK | B_PCH_PCIE_RXMC1_MSRTS_MASK | B_PCH_PCIE_RXMC1_MSRVS_MASK | B_PCH_PCIE_RXMC1_MSRM | \
                             B_PCH_PCIE_RXMC1_MVS | B_PCH_PCIE_RXMC1_MILRTS | B_PCH_PCIE_RXMC1_MIUDVMS | B_PCH_PCIE_RXMC1_MIESS | B_PCH_PCIE_RXMC1_MILRTS);
      Data32Or = ((V_PCH_PCIE_RXMC1_MMNOLS << B_PCH_PCIE_RXMC1_MMNOLS_OFFSET) | (V_PCH_PCIE_RXMC1_VMSLOP << B_PCH_PCIE_RXMC1_VMSLOP_OFFSET) | (V_PCH_PCIE_RXMC1_TMSLOP << B_PCH_PCIE_RXMC1_TMSLOP_OFFSET) | \
                  (V_PCH_PCIE_RXMC1_MSRTS << B_PCH_PCIE_RXMC1_MSRTS_OFFSET) | (V_PCH_PCIE_RXMC1_MSRVS << B_PCH_PCIE_RXMC1_MSRVS_OFFSET));
      PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_RXMC1, Data32And, Data32Or);
      DEBUG((DEBUG_INFO, "RXMC1 : %x\n", PciSegmentRead32 (RpBase + R_PCH_PCIE_RXMC1)));
      ///
      /// Program R_PCIE_RXMC2[C94]
      /// Set C94[19:24] MNOTSS to 0x20
      /// Set C94[18:13] MMTOS to 0x28
      /// Set C94[12:6] MNOVSS to 0x28
      /// Set C94[5:0] MMVOS to 0x0
      ///
      Data32And = (UINT32) ~(B_PCH_PCIE_RXMC2_MNOTSS_MASK | B_PCH_PCIE_RXMC2_MMTOS_MASK | B_PCH_PCIE_RXMC2_MNOVSS_MASK | B_PCH_PCIE_RXMC2_MMVOS_MASK);
      Data32Or = ((V_PCH_PCIE_RXMC2_MNOTSS << B_PCH_PCIE_RXMC2_MNOTSS_OFFSET) | (V_PCH_PCIE_RXMC2_MMTOS << B_PCH_PCIE_RXMC2_MMTOS_OFFSET) | (V_PCH_PCIE_RXMC2_MNOVSS << B_PCH_PCIE_RXMC2_MNOVSS_OFFSET) | \
                  (V_PCH_PCIE_RXMC2_MMVOS << B_PCH_PCIE_RXMC2_MMVOS_OFFSET));
      PciSegmentAndThenOr32 (RpBase + R_PCH_PCIE_RXMC2, Data32And, Data32Or);
      DEBUG((DEBUG_INFO, "RXMC2 : %x\n", PciSegmentRead32 (RpBase + R_PCH_PCIE_RXMC2)));
      ///
      /// Set CE4[0] MARGINDRISW to 0x0
      ///
      PciSegmentAnd32(RpBase + R_PCH_PCIE_PL16MPCPS, (UINT32) ~(B_PCH_PCIE_PL16MPCPS_MARGINDRISW));
    }
  }
  //
  // Program the root port target link speed.
  //
  Status = PcieRpSpeedChange ();
  ASSERT_EFI_ERROR (Status);

  //
  // Set the lock bits
  //
  PchPcieRpSetSecuredRegisterLock ();

  DEBUG ((DEBUG_INFO, "PcieRpInit() End\n"));
}

/**
  Configure PCIe Grant Counts
**/
VOID
PcieRpConfigureGrantCounts (
  VOID
  )
{
   UINT32                ControllerMax;
   UINT32                Controller;
   PSF_PCIE_CTRL_CONFIG  PsfPcieCtrlConfigTable[PCH_MAX_PCIE_CONTROLLERS];

   ControllerMax = GetPchMaxPcieControllerNum ();

  for (Controller = 0; Controller < ControllerMax; Controller++) {
    switch (GetPcieControllerConfig (Controller)) {
      case Pcie4x1:
        PsfPcieCtrlConfigTable[Controller] = PsfPcieCtrl4xn;
        break;
      case Pcie1x2_2x1:
        PsfPcieCtrlConfigTable[Controller] = PsfPcieCtrl1x2n_2xn;
        break;
      case Pcie2x2:
        PsfPcieCtrlConfigTable[Controller] = PsfPcieCtrl2x2n;
        break;
      case Pcie1x4:
        PsfPcieCtrlConfigTable[Controller] = PsfPcieCtrl1x4n;
        break;
      default:
        ASSERT (FALSE);
    }
  }

  PsfConfigurePcieGrantCounts (PsfPcieCtrlConfigTable, ControllerMax, PsfGetRootPciePsfTopology (), PsfGetSegmentTable (), PsfGetRootPciePortTable ());
}
