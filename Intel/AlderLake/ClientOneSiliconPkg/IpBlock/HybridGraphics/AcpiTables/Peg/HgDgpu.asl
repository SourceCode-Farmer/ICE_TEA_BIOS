/** @file

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  This file contains the system BIOS Hybrid graphics code for
  PEG DGPU.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2010 - 2018 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

//[-start-210127-IB05660155-modify]//
//[start-210907-STORM1112-modify]
//[-start-210908-QINGLIN0052-modify]//
//[-start-210917-GEORGE0004-modify]//
//[-start-210917-QINGLIN0068-modify]//
//#ifdef C770_SUPPORT 
#if defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210917-QINGLIN0068-modify]//
//[-end-210917-GEORGE0004-modify]//
//[-end-210908-QINGLIN0052-modify]//
//External(\_SB.PC00.PEG1.PEGP, DeviceObj)
External(\_SB.PC00.PEG2.PEGP, DeviceObj)
#else
External(\_SB.PC00.PEG1.PEGP, DeviceObj)
#endif
//[end-210907-STORM1112-modify]
//[-start-201113-IB09480112-modify]//
//External(\_SB.PC00.PGON, MethodObj)
//External(\_SB.PC00.PGOF, MethodObj)
//[start-210907-STORM1112-modify]
//[-start-210908-QINGLIN0052-modify]//
//[-start-210917-GEORGE0004-modify]//
//[-start-210917-QINGLIN0068-modify]//
//#ifdef C770_SUPPORT 
#if defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210917-QINGLIN0068-modify]//
//[-end-210917-GEORGE0004-modify]//
//[-end-210908-QINGLIN0052-modify]//
//External(\_SB.PC00.PEG1.PON, MethodObj)
//External(\_SB.PC00.PEG1.POFF, MethodObj)
External(\_SB.PC00.PEG2.PON, MethodObj)
External(\_SB.PC00.PEG2.POFF, MethodObj)
#else
External(\_SB.PC00.PEG1.PON, MethodObj)
External(\_SB.PC00.PEG1.POFF, MethodObj)
#endif
//[end-210907-STORM1112-modify]
//[-end-210127-IB05660155-modify]//
//[-end-201113-IB09480112-modify]//
External(\_SB.SGOV, MethodObj)
External(\GBAS)
External(\HGMD)
External(\SGGP)

//[-start-210127-IB05660155-modify]//
//[start-210907-STORM1112-modify]
//[-start-210908-QINGLIN0052-modify]//
//[-start-210917-GEORGE0004-modify]//
//[-start-210917-QINGLIN0068-modify]//
//#ifdef C770_SUPPORT 
#if defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210917-QINGLIN0068-modify]//
//[-end-210917-GEORGE0004-modify]//
//[-end-210908-QINGLIN0052-modify]//
//Scope(\_SB.PC00.PEG1.PEGP)
Scope(\_SB.PC00.PEG2.PEGP)
#else
Scope(\_SB.PC00.PEG1.PEGP)
#endif
//[end-210907-STORM1112-modify]
//[-end-210127-IB05660155-modify]//
{
  Method(_ON,0,Serialized)
  {
//[-start-201113-IB09480112-modify]//
//    \_SB.PC00.PGON(0)
//[-start-210127-IB05660155-modify]//
//[start-210907-STORM1112-modify]
//[-start-210908-QINGLIN0052-modify]//
//[-start-210917-GEORGE0004-modify]//
//[-start-210917-QINGLIN0068-modify]//
//#ifdef C770_SUPPORT 
#if defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210917-QINGLIN0068-modify]//
//[-end-210917-GEORGE0004-modify]//
//[-end-210908-QINGLIN0052-modify]//
//    \_SB.PC00.PEG1.PON ()
    \_SB.PC00.PEG2.PON ()
#else
    \_SB.PC00.PEG1.PON ()
#endif
//[end-210907-STORM1112-modify]
//[-end-210127-IB05660155-modify]//
//[-end-201113-IB09480112-modify]//
    //
    //Ask OS to do a PnP rescan
    //
//[-start-210127-IB05660155-modify]//
//[start-210907-STORM1112-modify]
//[-start-210908-QINGLIN0052-modify]//
//[-start-210917-GEORGE0004-modify]//
//[-start-210917-QINGLIN0068-modify]//
//#ifdef C770_SUPPORT 
#if defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210917-GEORGE0004-modify]//
//[-end-210917-QINGLIN0068-modify]//
//[-end-210908-QINGLIN0052-modify]//
//    Notify(\_SB.PC00.PEG1,0)
    Notify(\_SB.PC00.PEG2,0)
#else
    Notify(\_SB.PC00.PEG1,0)
#endif
//[end-210907-STORM1112-modify]
//[-end-210127-IB05660155-modify]//
  }

  Method(_OFF,0,Serialized)
  {
//[-start-201113-IB09480112-modify]//
//    \_SB.PC00.PGOF(0)
//[-start-210127-IB05660155-modify]//
//[start-210907-STORM1112-modify]
//[-start-210908-QINGLIN0052-modify]//
//[-start-210917-GEORGE0004-modify]//
//[-start-210917-QINGLIN0068-modify]//
//#ifdef C770_SUPPORT 
#if defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210917-QINGLIN0068-modify]//
//[-end-210917-GEORGE0004-modify]//
//[-end-210908-QINGLIN0052-modify]//
//    \_SB.PC00.PEG1.POFF ()
    \_SB.PC00.PEG2.POFF ()
#else
    \_SB.PC00.PEG1.POFF ()
#endif
//[end-210907-STORM1112-modify]
//[-end-210127-IB05660155-modify]//
//[-end-201113-IB09480112-modify]//
    //
    //Ask OS to do a PnP rescan
    //
//[-start-210127-IB05660155-modify]//
//[start-210907-STORM1112-modify]
//[-start-210908-QINGLIN0052-modify]//
//[-start-210917-GEORGE0004-modify]//
//[-start-210917-QINGLIN0068-modify]//
//#ifdef C770_SUPPORT 
#if defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210917-QINGLIN0068-modify]//
//[-end-210917-GEORGE0004-modify]//
//[-end-210908-QINGLIN0052-modify]//
//    Notify(\_SB.PC00.PEG1,0)
    Notify(\_SB.PC00.PEG2,0)
#else
    Notify(\_SB.PC00.PEG1,0)
#endif
//[end-210907-STORM1112-modify]
//[-end-210127-IB05660155-modify]//
  }

  //
  // Name: SGPO [SG GPIO Write]
  // Description: Function to write into GPIO
  // Input: Arg0 -> Expander Number
  //        Arg1 -> Gpio Number
  //        Arg2 -> Active Information
  //        Arg3 -> Value to write
  // Return: Nothing
  //
  Method(SGPO, 4, Serialized)
  {
    //
    // Invert if Active Low
    //
    If (LEqual(Arg2,0))
    {
      Not(Arg3, Arg3)
      And(Arg3, 0x01, Arg3)
    }
    If (LEqual(SGGP, 0x01))
    {
      //
      // PCH based GPIO
      //
      If (CondRefOf(\_SB.SGOV))
      {
        \_SB.SGOV(Arg1, Arg3)
      }
    }
  } // End of Method(SGPO)
//[-start-210127-IB05660155-modify]//
//[start-210907-STORM1112-modify]
//[-start-210908-QINGLIN0052-modify]//
//[-start-210917-GEORGE0004-modify]//
//[-start-210917-QINGLIN0068-modify]//
//#ifdef C770_SUPPORT 
#if defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210917-QINGLIN0068-modify]//
//[-end-210917-GEORGE0004-modify]//
//[-end-210908-QINGLIN0052-modify]//
//} // End of Scope(\_SB.PC00.PEG1.PEGP)
} // End of Scope(\_SB.PC00.PEG2.PEGP)
#else
} // End of Scope(\_SB.PC00.PEG1.PEGP)
#endif
//[end-210907-STORM1112-modify]
//[-end-210127-IB05660155-modify]//
