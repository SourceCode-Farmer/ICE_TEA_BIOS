/** @file
;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  This file contains the system BIOS Hybrid graphics code for
  ULT.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2010 - 2018 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/


//-----------------------------------------
// Runtime Device Power Management - Begin
//-----------------------------------------
//
// Name: PCRP
// Description: Declare a PowerResource object.
//
PowerResource(PCRP, 0, 0) {
  Name(_STA, One)
  Method(_ON, 0, Serialized) {
    If(LNotEqual(OSYS,2009)) {
//[-start-180620-IB15410165-modify]//
//[-start-210309-IB05660156-modify]//
#if FeaturePcdGet (PcdHybridGraphicsSupported) && FeaturePcdGet (PcdNvidiaOptimusSupported)
//[-end-210309-IB05660156-modify]//
//[-start-210507-IB05660160-modify]//
        If (LEqual (DGPV, NVIDIA_VID))
//[-end-210507-IB05660160-modify]//
        {
            If (LEqual (PCI_SCOPE.TDGC, One))
            {
                If (LEqual (PCI_SCOPE.DGCX, 0x03))
                {
                    PCI_SCOPE.GC6O()
                }
                ElseIf (LEqual (PCI_SCOPE.DGCX, 0x04))
                {
                    PCI_SCOPE.GC6O()
                }
                // Clear Defer flag
                Store (Zero, PCI_SCOPE.TDGC)
                Store (Zero, PCI_SCOPE.DGCX)
                Store (One , _STA)
            }
            Else
            {
//[-start-180605-IB15410157-modify]//
//[-start-200227-IB05660117-modify]//
                PCI_SCOPE.HGON ()
//[-end-200227-IB05660117-modify]//
//[-end-180605-IB15410157-modify]//
                Store (0x07, DGPU_BRIDGE_SCOPE.CMDR)
                Store (Zero, DGPU_BRIDGE_SCOPE.D0ST)
//[-start-181008-IB15410196-modify]//
                Store (PCI_SCOPE.DSSV, DGPU_SCOPE.SSSV) // OEM_customize: fill SVID/SSID in DWORD
                If (LEqual (PCI_SCOPE.NVGE, NV_GEN_17))
                {
                    If (PCI_SCOPE.OPTF) // IBV_customize: for hybrid with display platform, read the CMOS flag to determine if dGPU HDA device should be en/dis, default is to disable HDA
                    {
                        Store (One, DGPU_SCOPE.HDAE)
                    }
                    Else
                    {
                        Store (Zero, DGPU_SCOPE.HDAE)
                    }
                }
//[-end-181008-IB15410196-modify]//
                Store (One, _STA)
            }
        }
        Else
        {
#endif
      \_SB.PC00.HGON()
      Store(One, _STA)
//[-start-210309-IB05660156-modify]//
#if FeaturePcdGet (PcdHybridGraphicsSupported) && FeaturePcdGet (PcdNvidiaOptimusSupported)
//[-end-210309-IB05660156-modify]//
        }
#endif
//[-end-180620-IB15410165-modify]//
    }
  }
  Method(_OFF, 0, Serialized) {
    If(LNotEqual(OSYS,2009)) {
//[-start-180620-IB15410165-modify]//
//[-start-201117-IB09480115-modify]//
//[-start-210309-IB05660156-modify]//
#if FeaturePcdGet (PcdHybridGraphicsSupported) && FeaturePcdGet (PcdNvidiaOptimusSupported)
//[-end-210309-IB05660156-modify]//
//[-start-210507-IB05660160-modify]//
        If (LEqual (DGPV, NVIDIA_VID))
//[-end-210507-IB05660160-modify]//
        {
            If (LEqual (PCI_SCOPE.TDGC, One))
            {
                CreateField (PCI_SCOPE.TGPC, 0, 3, GPPC) // GPU Power Control
                If (LEqual (ToInteger (GPPC), 0x01))
                {
                    PCI_SCOPE.GC6I()
                }
                ElseIf (LEqual (ToInteger (GPPC), 0x02))
                {
                    PCI_SCOPE.GC6I()
                }
                Store (Zero, _STA)
            }
            Else
            {
//[-end-201117-IB09480115-modify]//
//[-start-180605-IB15410157-modify]//
//[-start-200227-IB05660117-modify]//
                PCI_SCOPE.HGOF ()
//[-end-200227-IB05660117-modify]//
//[-end-180605-IB15410157-modify]//
                Store (Zero, _STA)
            }
        }
        Else
        {
#endif
      \_SB.PC00.HGOF()
      Store(Zero, _STA)
//[-start-210309-IB05660156-modify]//
#if FeaturePcdGet (PcdHybridGraphicsSupported) && FeaturePcdGet (PcdNvidiaOptimusSupported)
//[-end-210309-IB05660156-modify]//
        }
#endif
//[-end-180620-IB15410165-modify]//
    }
  }
} //End of PowerResource(PCRP, 0, 0)

Name(_PR0,Package(){PCRP})
Name(_PR2,Package(){PCRP})
Name(_PR3,Package(){PCRP})

Method(_S0W, 0) { Return(4) } //D3cold is supported

//-----------------------------------------
// Runtime Device Power Management - End
//-----------------------------------------

//[-start-181127-IB15410201-remove]//
//Device(PEGP) {
//  Name(_ADR, 0x00000000)
//  Method(_PRW, 0) { Return(GPRW(0x69, 4)) } // can wakeup from S4 state
//}
//[-end-181127-IB15410201-remove]//

Device(PEGA) {
  Name(_ADR, 0x00000001)
  Method(_PRW, 0) { Return(GPRW(0x69, 4)) } // can wakeup from S4 state
}





