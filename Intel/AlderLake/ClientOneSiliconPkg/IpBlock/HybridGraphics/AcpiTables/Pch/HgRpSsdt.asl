/** @file
;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  This file contains the system BIOS Hybrid graphics code for
  ULT.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2010 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

DefinitionBlock (
    "HgRpSsdt.aml",
    "SSDT",
    2,
    "HgRef",
    "HgRpSsdt",
    0x1000
    )
{
//[-start-181127-IB15410201-modify]//
External(OSYS)
External(RPIN)
External(GPRW, MethodObj)
External(\_SB.PC00.HGON, MethodObj)
External(\_SB.PC00.HGOF, MethodObj)
//[-start-210423-IB05660158-add]//
External(\_SB.PC00.RP08, DeviceObj)
External(\_SB.PC00.RP08.PXSX, DeviceObj)
External(\_SB.PC00.RP08.PXSX._ADR)
//[-end-210423-IB05660158-add]//
External(\_SB.PC00.RP05, DeviceObj)
External(\_SB.PC00.RP05.PXSX, DeviceObj)
External(\_SB.PC00.RP05.PXSX._ADR)
External(\_SB.PC00.RP01, DeviceObj)
External(\_SB.PC00.RP01.PXSX, DeviceObj)
External(\_SB.PC00.RP01.PXSX._ADR)
External(P8XH, MethodObj)
External(\_SB.PC00, DeviceObj)
External(\_SB.SGOV, MethodObj)
External(\_SB.GGOV, MethodObj)
External(\RPA5)
External(\RPBA)
External(\EECP)
External(\XBAS)
External(\GBAS)
External(\HGMD)
External(\SGGP)
External(\DLPW)
External(\DLHR)
External(\HRE0)
External(\HRG0)
External(\HRA0)
External(\PWE0)
External(\PWG0)
External(\PWA0)
//[-end-181127-IB15410201-modify]//
//[-start-210507-IB05660160-add]//
External(\DGPV)
//[-end-210507-IB05660160-add]//

//[-start-180620-IB15410165-add]//
#if FeaturePcdGet (PcdHybridGraphicsSupported)
#define PCI_SCOPE             \_SB.PC00
//[-start-210428-IB05660158-modify]//
#define DGPU_BRIDGE_SCOPE     PCI_SCOPE.RP08
//[-end-210428-IB05660158-modify]//
#define DGPU_DEVICE           PXSX
#define DGPU_SCOPE            DGPU_BRIDGE_SCOPE.DGPU_DEVICE
#define NVIDIA_VID            0x10DE
#define NV_GEN_17             17

External (DGPU_BRIDGE_SCOPE.CEDR, FieldUnitObj)
External (DGPU_BRIDGE_SCOPE.CMDR, FieldUnitObj)
External (DGPU_BRIDGE_SCOPE.D0ST, FieldUnitObj)
External (DGPU_BRIDGE_SCOPE.LREN, FieldUnitObj)
External (DGPU_SCOPE.HDAE, FieldUnitObj)
External (DGPU_SCOPE.LTRE, IntObj)
External (DGPU_SCOPE.SSSV, FieldUnitObj)
External (PCI_SCOPE.DGCX, IntObj)
External (PCI_SCOPE.DSSV, FieldUnitObj)
External (PCI_SCOPE.NVGE, FieldUnitObj)
External (PCI_SCOPE.GC6I, MethodObj)
External (PCI_SCOPE.GC6O, MethodObj)
External (PCI_SCOPE.NINI, MethodObj)
External (PCI_SCOPE.OPTF, FieldUnitObj)
External (PCI_SCOPE.TDGC, IntObj)
External (PCI_SCOPE.TGPC, BuffObj)
#endif
//[-end-180620-IB15410165-add]//
Scope(\_SB.PC00)
{
  If(LEqual(RPIN,0)) {
    Scope(\_SB.PC00.RP01)
    {
      Include("HgRpCommon.asl")
    }

//[-start-181127-IB15410201-modify]//
      Scope(\_SB.PC00.RP01.PXSX)
    {
//[-start-190111-IB15410218-modify]//
      Method (_INI)
      {
            Store (0x0, \_SB.PC00.RP01.PXSX._ADR)
//[-start-210309-IB05660156-modify]//
#if FeaturePcdGet (PcdHybridGraphicsSupported) && FeaturePcdGet (PcdNvidiaOptimusSupported)
//[-end-210309-IB05660156-modify]//
//[-start-210113-IB05660151-modify]//
//[-start-210428-IB05660158-modify]//
//            If (LEqual (PCI_SCOPE.NVGE, NV_GEN_17))
//            {
              If (CondRefOf (PCI_SCOPE.NINI))
              {
                PCI_SCOPE.NINI ()
              }
//            }
//[-end-210428-IB05660158-modify]//
//[-end-210113-IB05660151-modify]//
#endif
      }
//[-end-190111-IB15410218-modify]//
      Method(_ON,0,Serialized)
      {
        \_SB.PC00.HGON()

        //Ask OS to do a PnP rescan
        Notify(\_SB.PC00.RP01,0)

        Return ()
      }

      Method(_OFF,0,Serialized)
      {
        \_SB.PC00.HGOF()

        //Ask OS to do a PnP rescan
        Notify(\_SB.PC00.RP01,0)

        Return ()
      }
    }
//[-end-181127-IB15410201-modify]//
  }

  ElseIf(LEqual(RPIN,4)) {

    Scope(\_SB.PC00.RP05)
    {
      Include("HgRpCommon.asl")
    }

//[-start-181127-IB15410201-modify]//
      Scope(\_SB.PC00.RP05.PXSX)
    {

      Method (_INI)
      {
            Store (0x0, \_SB.PC00.RP05.PXSX._ADR)
//[-start-210309-IB05660156-modify]//
 #if FeaturePcdGet (PcdHybridGraphicsSupported) && FeaturePcdGet (PcdNvidiaOptimusSupported)
//[-end-210309-IB05660156-modify]//
//[-start-210113-IB05660151-modify]//
//[-start-210428-IB05660158-modify]//
//            If (LEqual (PCI_SCOPE.NVGE, NV_GEN_17))
//            {
              If (CondRefOf (PCI_SCOPE.NINI))
              {
                PCI_SCOPE.NINI ()
              }
//            }
//[-end-210428-IB05660158-modify]//
//[-end-210113-IB05660151-modify]//
 #endif
      }

      Method(_ON,0,Serialized)
      {
        \_SB.PC00.HGON()

        //Ask OS to do a PnP rescan
        Notify(\_SB.PC00.RP05,0)

        Return ()
      }

      Method(_OFF,0,Serialized)
      {
        \_SB.PC00.HGOF()

        //Ask OS to do a PnP rescan
        Notify(\_SB.PC00.RP05,0)

        Return ()
      }
    }
//[-end-181127-IB15410201-modify]//
  }
//[-start-210428-IB05660158-add]//
  ElseIf(LEqual(RPIN,7)) {

    Scope(\_SB.PC00.RP08)
    {
      Include("HgRpCommon.asl")
    }

      Scope(\_SB.PC00.RP08.PXSX)
    {

      Method (_INI)
      {
            Store (0x0, \_SB.PC00.RP08.PXSX._ADR)
#if FeaturePcdGet (PcdHybridGraphicsSupported) && FeaturePcdGet (PcdNvidiaOptimusSupported)
              If (CondRefOf (PCI_SCOPE.NINI))
              {
                PCI_SCOPE.NINI ()
              }
#endif
      }

      Method(_ON,0,Serialized)
      {
        \_SB.PC00.HGON()

        //Ask OS to do a PnP rescan
        Notify(\_SB.PC00.RP08,0)

        Return ()
      }

      Method(_OFF,0,Serialized)
      {
        \_SB.PC00.HGOF()

        //Ask OS to do a PnP rescan
        Notify(\_SB.PC00.RP08,0)

        Return ()
      }
    }

  }
//[-end-210428-IB05660158-add]//
  Include("HgDgpuPch.asl")

}// End of Scope(\_SB.PC00)
}// End of Definition Block
