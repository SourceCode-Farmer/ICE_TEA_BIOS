/** @file
  Library for USB controller initialization for both CPU and PCH.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/UsbHostControllerInitLib.h>

// General includes
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/TimerLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/SiScheduleResetLib.h>
#include <IndustryStandard/Pci30.h>
#include <Library/UsbLib.h>
#include <Register/UsbRegs.h>
#include <Library/DciPrivateLib.h>

// PCH specific includes
#include <Register/PchPcrRegs.h>
#include <Library/PchPcrLib.h>


// xHCI controller DID for USB IP Version detection
#define V_XHCI_DID_PCH_B_V18_0      0x98ED
#define V_XHCI_DID_PCH_LP_V18_0     0xA0ED
#define V_XHCI_DID_PCH_P_V18_0      0x51ED
#define V_XHCI_DID_PCH_N_V18_0      0x54ED
#define V_XHCI_DID_PCH_H_V18_1      0x43ED
#define V_XHCI_DID_CPU_LP_V19_0     0x461E
#define V_XHCI_DID_PCH_S_V19_0      0x7AE0
#define V_XHCI_DID_PCH_S_1_V19_0    0x7A60

#define V_XHCI_DID_PCH_SERVER_V18_0 0x1BCD

// xHCI IP version string structure
typedef struct {
  USB_IP_VERSION    IpVersion;
  CHAR8             *VersionString;
} XHCI_IP_VERSION_STRING;

// xHCI IP version strings array
GLOBAL_REMOVE_IF_UNREFERENCED
XHCI_IP_VERSION_STRING mXhciIpStrings[] = {
  {V18_0, "xHCI IP v18.0"},
  {V18_1, "xHCI IP v18.1"},
  {V19_0, "xHCI IP v19.0"},
  {V19_1, "xHCI IP v19.1"}
};

#ifndef MDEPKG_NDEBUG
/*
  Helper function to retrieve readable xHCI IP Version string instead of enum value

  @retval     xHCI IP version string
*/
STATIC
CHAR8*
GetXhciIpVersionString (
  USB_IP_VERSION    IpVersion
  )
{
  UINT8 Index;

  for (Index = 0; Index < ARRAY_SIZE(mXhciIpStrings); Index++) {
    if (mXhciIpStrings[Index].IpVersion == IpVersion) {
      return mXhciIpStrings[Index].VersionString;
    }
  }

  return "Unknown xHCI IP Version";
}
#endif

/**
  Program and enable XHCI Memory Space

  @param[in] XhciPciBase          XHCI PCI Base Address
  @param[in] XhciMmioBase         Memory base address of XHCI Controller
**/
STATIC
VOID
XhciMemorySpaceOpen (
  IN  UINT64            XhciPciBase,
  IN  UINT32            XhciMmioBase
  )
{
  //
  // Assign memory resources
  //
  PciSegmentWrite32 (XhciPciBase + R_XHCI_CFG_BAR0, XhciMmioBase);
  PciSegmentWrite32 (XhciPciBase + R_XHCI_CFG_BAR0 + 4, 0);

  PciSegmentOr16 (
    XhciPciBase + PCI_COMMAND_OFFSET,
    (UINT16) (EFI_PCI_COMMAND_MEMORY_SPACE)
    );
}

/**
  Clear and disable XHCI Memory Space

  @param[in] XhciPciBase          XHCI PCI Base Address
**/
STATIC
VOID
XhciMemorySpaceClose (
  IN  UINT64                      XhciPciBase
  )
{
  //
  // Clear memory resources
  //
  PciSegmentAnd16 (
    XhciPciBase + PCI_COMMAND_OFFSET,
    (UINT16) ~(EFI_PCI_COMMAND_MEMORY_SPACE)
    );

  PciSegmentWrite32 ((XhciPciBase + R_XHCI_CFG_BAR0), 0);
  PciSegmentWrite32 ((XhciPciBase + R_XHCI_CFG_BAR0 + 4), 0);
}

/**
  Verifies if OverCurrent pins in USB config Block are from valid range

  @param[in]  UsbHandle          Pointer to USB_HANDLE instance

  @retval     BOOLEAN            Over Current pin assignment validity
**/
STATIC
BOOLEAN
XhciIsOverCurrentMappingValid (
  IN  USB_HANDLE        *UsbHandle
  )
{
  UINT32                PortIndex;
  UINT32                OcPin;
  USB_CONFIG            *UsbConfig;
  USB_CONTROLLER        *UsbController;

  UsbConfig     = UsbHandle->UsbConfig;
  UsbController = UsbHandle->HostController;

  for (PortIndex = 0; PortIndex < UsbController->Usb2PortCount; PortIndex++) {
    OcPin = UsbConfig->PortUsb20[PortIndex].OverCurrentPin;
    if (OcPin >= USB_OC_MAX_PINS && OcPin != USB_OC_SKIP) {
      DEBUG ((DEBUG_ERROR, "[%a] Invalid OC pin for USB2 port %d\n", __FUNCTION__, PortIndex));
      return FALSE;
    }
  }

  for (PortIndex = 0; PortIndex < UsbController->Usb3LanesCount; PortIndex++) {
    OcPin = UsbConfig->PortUsb30[PortIndex].OverCurrentPin;
    if (OcPin >= USB_OC_MAX_PINS && OcPin != USB_OC_SKIP) {
      DEBUG ((DEBUG_ERROR, "[%a] Invalid OC pin for USB3 port %d\n", __FUNCTION__, PortIndex));
      return FALSE;
    }
  }

  return TRUE;
}

/**
  Setup XHCI Over-Current Mapping

  @param[in]  UsbHandle         Pointer to USB_HANDLE instance
  @param[in]  XhciMmioBase      XHCI Memory Bar base address
**/
STATIC
VOID
XhciOverCurrentMapping (
  IN  USB_HANDLE        *UsbHandle,
  IN  UINT32            XhciMmioBase
  )
{
  UINT8                 Index;
  UINT32                U2OcMBuf[USB_OC_MAX_PINS];
  UINT32                U3OcMBuf[USB_OC_MAX_PINS];
  UINT32                OcPin;
  UINT32                OcPinsUsedMask;
  USB_CONFIG            *UsbConfig;
  USB_CONTROLLER        *UsbController;
  USB_CALLBACK          *Callback;
  USB_PRIVATE_CONFIG    *PrivateConfig;

  UsbConfig     = UsbHandle->UsbConfig;
  UsbController = UsbHandle->HostController;
  Callback      = UsbHandle->Callback;
  PrivateConfig = UsbHandle->PrivateConfig;

  if (!UsbConfig->OverCurrentEnable) {
    //
    // Clear Over-Current registers to switch off Over-Current detection.
    //
    DEBUG ((DEBUG_WARN, "Clear Over-Current registers to enable OBS pins usage\n"));

    for (Index = 0; Index < USB_OC_MAX_PINS; Index++) {
      MmioWrite32 (XhciMmioBase + R_XHCI_MEM_U2OCM + (Index * 4), 0);
      MmioWrite32 (XhciMmioBase + R_XHCI_MEM_U3OCM + (Index * 4), 0);
    }

    //
    // Exit function after clearing Overcurrent mapping
    //
    return;
  }

  //
  // Verify proper Over Current pins are used in policy
  //
  if (!XhciIsOverCurrentMappingValid (UsbHandle)) {
    DEBUG ((DEBUG_ERROR, "Over Current pin mapping uses invalid values. Skipping USB Over Current programming.\n"));
    return;
  }

  ZeroMem (U2OcMBuf, sizeof (U2OcMBuf));
  ZeroMem (U3OcMBuf, sizeof (U3OcMBuf));
  OcPinsUsedMask   = 0;

  for (Index = 0; Index < UsbController->Usb2PortCount; Index++) {
    if (UsbConfig->PortUsb20[Index].OverCurrentPin != USB_OC_SKIP) {
      OcPin = UsbConfig->PortUsb20[Index].OverCurrentPin;
      U2OcMBuf[OcPin] |= (UINT32) (BIT0 << Index);
      OcPinsUsedMask  |= (UINT32) (BIT0 << OcPin);
    }
  }

  for (Index = 0; Index < UsbController->Usb3LanesCount; Index++) {
    if (UsbConfig->PortUsb30[Index].OverCurrentPin != USB_OC_SKIP) {
      OcPin = UsbConfig->PortUsb30[Index].OverCurrentPin;
      U3OcMBuf[OcPin] |= (UINT32) (BIT0 << Index);
      OcPinsUsedMask  |= (UINT32) (BIT0 << OcPin);
    }
  }

  for (Index = 0; Index < USB_OC_MAX_PINS; Index++) {
    MmioWrite32 (XhciMmioBase + R_XHCI_MEM_U2OCM + (Index * 4), U2OcMBuf[Index]);
    MmioWrite32 (XhciMmioBase + R_XHCI_MEM_U3OCM + (Index * 4), U3OcMBuf[Index]);
  }

  //
  // Only enabling OC GPIOs for xHCI in South Complex (Virtual ones if used will be enabled as well)
  //
  if (PrivateConfig->Location == Standalone) {
    for (Index = 0; Index < USB_OC_MAX_PINS; Index++) {
      if ((OcPinsUsedMask >> Index) & BIT0) {
        if (Callback->UsbEnableOvercurrentPin != NULL) {
          Callback->UsbEnableOvercurrentPin (UsbHandle, Index);
        }
      }
    }
  }
}

/**
  Performs basic configuration of USB3 (xHCI) controller.

  @param[in]  UsbHandle         Pointer to USB_HANDLE instance
  @param[in]  XhciMmioBase      Memory base address of xHCI Controller
**/
STATIC
VOID
XhciHcInit (
  IN  USB_HANDLE            *UsbHandle,
  IN  UINT32                XhciMmioBase
  )
{
  UINT32                Data32;
  UINT32                Data32Or;
  UINT32                Data32And;
  UINT16                Data16Or;
  UINT16                Data16And;
  UINT8                 ReportedHsPortCount;
  UINT8                 ReportedSsPortCount;
  UINT8                 LaneNum;
  UINT8                 MaxUsb3Lanes;
  UINT32                Index;
  UINT64                XhciPciBase;
  USB_CONFIG            *UsbConfig;
  USB_PRIVATE_CONFIG    *PrivateConfig;
  USB_CONTROLLER        *UsbController;
  USB_CALLBACK          *Callback;
  UINT16                DeviceId;

  UsbConfig     = UsbHandle->UsbConfig;
  PrivateConfig = UsbHandle->PrivateConfig;
  UsbController = UsbHandle->HostController;
  Callback      = UsbHandle->Callback;

  XhciPciBase = UsbController->PciCfgBaseAddr;
  DeviceId    = PciSegmentRead16 (XhciPciBase + PCI_DEVICE_ID_OFFSET);


  DEBUG ((DEBUG_INFO, "XhciMmio Base = 0x%x\n", XhciMmioBase));

  ReportedHsPortCount = (MmioRead32 (XhciMmioBase + R_XHCI_MEM_XECP_SUPP_USB2_2) &
                           B_XHCI_MEM_XECP_SUPP_USBX_2_CPC) >>
                           N_XHCI_MEM_XECP_SUPP_USBX_2_CPC;
  ReportedSsPortCount = (MmioRead32 (XhciMmioBase + R_XHCI_MEM_XECP_SUPP_USB3_2) &
                           B_XHCI_MEM_XECP_SUPP_USBX_2_CPC) >>
                           N_XHCI_MEM_XECP_SUPP_USBX_2_CPC;

  DEBUG ((DEBUG_INFO, "Number of reported High Speed Ports  = %d\n", ReportedHsPortCount));
  DEBUG ((DEBUG_INFO, "Number of reported Super Speed Ports = %d\n", ReportedSsPortCount));

  //
  //  XHCC1 - XHCI System Bus Configuration 1
  //  Address Offset: 0x40
  //  Writes to this registers needs to be performed per bytes to avoid touching bit 31
  //  Bit 31 is used to lock RW/L bits and can be writen once.
  //
  if (PrivateConfig->IpVersion < V19_0) {
    PciSegmentOr8 (
      XhciPciBase + R_XHCI_CFG_XHCC1 + 3,
      (UINT8) (BIT5)
      );
  } else if (PrivateConfig->Location == Tcss) {
    PciSegmentAnd16 (
      XhciPciBase + R_XHCI_CFG_XHCC1 + 2,
      (UINT16)~(BIT13)
      );
  }

  //
  //  DBGDEV_CTRL_REG1 - Debug Device Control Register 1
  //  Address Offset: 0x8754
  //
  MmioOr32 (
    (XhciMmioBase + R_XHCI_MEM_DBGDEV_CTRL_REG1),
    (UINT32) (BIT9)
    );

  //
  //  XHCC2 - XHCI System Bus Configuration 2
  //  Address Offset: 0x44
  //
  Data32Or     = (BIT22 | BIT0);
  Data32And    = (UINT32)~(BIT11);
  if ((PrivateConfig->Location == Tcss) || (PrivateConfig->IpVersion >= V19_1)) {
    Data32Or  |= (BIT25 | BIT24 | BIT23 | BIT2 | BIT1);
  }
  if (PrivateConfig->IpVersion < V19_0) {
    Data32And &= (UINT32)~(BIT10);
  }
  PciSegmentAndThenOr32 (
    XhciPciBase + R_XHCI_CFG_XHCC2,
    Data32And,
    Data32Or
    );

  //
  //  PMCTRL2 - Power Management Control 2
  //  Address Offset: 0x8468
  //  This bits must be set before setting USB_SRAM_PG_EN (bit27) in XHCLKGTEN
  //
  Data32Or = 0u;
  Data32And = ~0u;
  if (((PrivateConfig->IpVersion == V19_0) && (PrivateConfig->Location == Standalone)) ||
    (PrivateConfig->IpVersion == V19_1)) {
    Data32And &= (UINT32)~(BIT4);
  }
  if (PrivateConfig->IpVersion <= V19_0) {
    Data32Or = (UINT32) (BIT5 | BIT0);
  }
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_PMCTRL2),
    Data32And,
    Data32Or
    );

  if ((DeviceId == V_XHCI_DID_PCH_LP_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_H_V18_1) ||
      (DeviceId == V_XHCI_DID_PCH_SERVER_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_P_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_N_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_V19_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_1_V19_0)) {
    //
    //  XHCLKGTEN - Clock Gating
    //  Address Offset: 0x50
    //
    PciSegmentAndThenOr32 (
      (XhciPciBase + R_XHCI_CFG_XHCLKGTEN),
      (UINT32)~(B_XHCI_CFG_XHCLKGTEN_SSCTGE),
      (UINT32) (0x8 << N_XHCI_CFG_XHCLKGTEN_SSCTGE)
      );
  }

  //
  //  PCE - Power Control Enables
  //  Address Offset: 0xA2
  //
  if ((PrivateConfig->Location == Standalone) || !PrivateConfig->ExternalPowerControl) {
    Data16And = (UINT16)~0;
    Data16Or  = (UINT16) BIT3;
    if (PrivateConfig->Location == Standalone) {
      if (DeviceId == V_XHCI_DID_PCH_SERVER_V18_0) {
        Data16And = (UINT16)~(BIT5 | BIT1);
        Data16Or |= BIT2;
      } else {
        Data16Or |= (BIT2 | BIT1);
      }
    }
    PciSegmentAndThenOr16 (
      (XhciPciBase + R_XHCI_CFG_PCE),
      Data16And,
      Data16Or
      );
  }

  //
  //  HSCFG2 - High Speed Configuration 2
  //  Address Offset: 0xA4
  //
  PciSegmentAnd32 (
    (XhciPciBase + R_XHCI_CFG_HSCFG2),
    (UINT32)~(BIT15 | BIT14 | BIT13)
    );

  //
  //  SSCFG1 - SuperSpeed Configuration 1
  //  Address Offset: 0xA8
  //
  Data32Or = 0;
  Data32And = ~0u;
  if (PrivateConfig->IpVersion < V19_1) {
    Data32Or |= BIT14;

    if (PrivateConfig->Location == Standalone) {
      Data32Or |= BIT12;
    }
  }
  if (DeviceId == V_XHCI_DID_PCH_SERVER_V18_0 && PrivateConfig->IsDbcDebugEnabled) {
    Data32And &= (UINT32)~(BIT17);
  }
  PciSegmentAndThenOr32 (
    (XhciPciBase + R_XHCI_CFG_SSCFG1),
    Data32And,
    Data32Or
    );

  //
  //  HSCFG1 - High Speed Configuration 1
  //  Address offset: 0xAC
  //
  Data32And = ~0u;
  if (PrivateConfig->IpVersion <= V19_0) {
    Data32And &= (UINT32)~(BIT19 | BIT18);
    if (PrivateConfig->Location == Standalone) {
      Data32Or &= (UINT32)~(BIT9);
    }
  }
  PciSegmentAnd32 (
    (XhciPciBase + R_XHCI_CFG_HSCFG1),
    Data32And
    );

  //
  //  XHCC3 - XHCI System Bus Configuration 3
  //  Address Offset: 0xFC
  //
  if (DeviceId == V_XHCI_DID_PCH_SERVER_V18_0) {
    PciSegmentAnd32 (
      (XhciPciBase + R_XHCI_CFG_XHCC3),
      (UINT32)~(BIT4)
      );
  } else {
    Data32And = ~0u;
    if (((DeviceId == V_XHCI_DID_PCH_S_V19_0) && (PrivateConfig->Stepping == A0)) ||
        ((DeviceId == V_XHCI_DID_PCH_S_1_V19_0) && (PrivateConfig->Stepping == A0)) ||
        (DeviceId == V_XHCI_DID_PCH_H_V18_1)) {
      Data32And &= (UINT32)~(BIT1);
    }
    PciSegmentAndThenOr32 (
      (XhciPciBase + R_XHCI_CFG_XHCC3),
      Data32And,
      (UINT32) (BIT4)
      );
  }

  if (PrivateConfig->IpVersion <= V19_1) {
    //
    //  DAP eSS Port <N> Control 1 Register
    //  Address Offset: 0x604 + N * 0x10
    //
    for (Index = 0; Index < ReportedSsPortCount; Index++) {
      PchPcrAndThenOr32 (
        PID_XHCI,
        R_XHCI_PCR_DAP_ESS_PORT_CONTROL1 + (Index * S_XHCI_PCR_DAP_ESS_PORT_CONTROL1_SPACING),
        (UINT32) ~0u,
        (UINT32) BIT3
        );
    }
  }

  //
  //  DAP USB2 Port <N> Control 1 Register
  //  Address Offset: 0x500 + N * 0x10
  //
  if ((PrivateConfig->Location != Tcss) && (!IsDciDebugEnabled())) {
    for (Index = 0; Index < ReportedHsPortCount; Index++) {
      Data32 = PchPcrRead32 (
        PID_XHCI,
        R_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0 + (Index * S_XHCI_PCR_DAP_ESS_PORT_CONTROL1_SPACING)
        );

      // if SPR value = 0x180, then do 0x180(dbc) -> 0x20(disconnect) -> 0x00(host)
      if ((Data32 & B_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0_CONNECTOR_EVENT) ==
          V_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0_CONNECTOR_EVENT_DBC) {
        DEBUG ((DEBUG_INFO, "Change DbC port from debug mode to functional USB mode when debug is disabled\n"));

        Data32 = PchPcrAndThenOr32 (
          PID_XHCI,
          R_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0 + (Index * S_XHCI_PCR_DAP_ESS_PORT_CONTROL1_SPACING),
          (UINT32) ~B_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0_CONNECTOR_EVENT,
          (UINT32) V_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0_CONNECTOR_EVENT_DISCONNECT
        );
        DEBUG ((DEBUG_INFO, "SPR value = 0x%x\n", Data32));

        Data32 = PchPcrAndThenOr32 (
          PID_XHCI,
          R_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0 + (Index * S_XHCI_PCR_DAP_ESS_PORT_CONTROL1_SPACING),
          (UINT32) ~B_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0_CONNECTOR_EVENT,
          (UINT32) V_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0_CONNECTOR_EVENT_HOST
        );
        DEBUG ((DEBUG_INFO, "SPR value = 0x%x\n", Data32));

        Data32 = PchPcrRead32 (
          PID_XHCI,
          R_XHCI_PCR_DAP_USB2PORT_STATUS_0 + (Index * S_XHCI_PCR_DAP_ESS_PORT_CONTROL1_SPACING)
        );
        DEBUG ((DEBUG_INFO, "SPR USB2 Port %d Status0 value = 0x%x\n", Index, Data32));
      }
    }
  }

  //
  //  HCIVERSION - Host Controller Interface Version Number
  //  Address Offset: 0x02
  //
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_CAPLENGTH),
    (UINT32) B_XHCI_MEM_HCIVERSION,
    (UINT32) (V_XHCI_MEM_HCIVERSION_0120 << N_XHCI_MEM_HCIVERSION)
    );

  //
  //  HOST_CTRL_ODMA_REG - Host Control ODMA Register
  //  Address Offset: 0x8098
  //
  if ((PrivateConfig->Location == Tcss) || (PrivateConfig->IpVersion >= V19_0)) {
    MmioAnd32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_ODMA_REG),
      (UINT32)~(BIT13)
      );
  }

  //
  //  HCSPARAMS3 - Structural Parameters 3
  //  Address Offset: 0x0C
  //
  Data32Or = (UINT32) (0xA0 << N_XHCI_MEM_HCSPARAMS3_U2DEL | 0x0A);
  if (PrivateConfig->Location == Tcss) {
    Data32Or = (UINT32) (0x190 << N_XHCI_MEM_HCSPARAMS3_U2DEL);
  }
  if ((DeviceId == V_XHCI_DID_PCH_S_V19_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_1_V19_0) ||
      ((PrivateConfig->IpVersion >= V19_1) && (PrivateConfig->Location == Standalone))) {
    Data32Or = (UINT32) (0x3E8 << N_XHCI_MEM_HCSPARAMS3_U2DEL);
  }
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_HCSPARAMS3),
    B_XHCI_MEM_HCSPARAMS3,
    Data32Or
    );

  //
  //  HCCPARAMS2 - Capability Parameters 2
  //  Address Offset: 0x1C
  //
  if (UsbConfig->UaolEnable) {
    MmioOr32 (
      (XhciMmioBase + R_XHCI_MEM_HCCPARAMS2),
      (UINT32) BIT8
      );
  }

  //
  //  PMCTRL - Power Management Control
  //  Address Offset: 0x80A4
  //
  Data32Or  = (UINT32) (BIT3);
  Data32And = (UINT32)~(BIT25 | BIT23 | BIT22);
  if (PrivateConfig->IpVersion <= V18_1) {
    Data32Or |= BIT7 | BIT4;
  }
  if ((DeviceId == V_XHCI_DID_PCH_B_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_V19_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_1_V19_0) ||
      ((PrivateConfig->IpVersion == V19_0) && (PrivateConfig->Location == Tcss))) {
    Data32Or |= (UINT32) (BIT23 | BIT22);
  }
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_PMCTRL),
    Data32And,
    Data32Or
    );

  //
  //  Host Controller Misc Reg 2
  //  Address Offset: 0x80B4
  //
  Data32Or = (UINT32) (BIT23);
  if (!PrivateConfig->EnableHcResetIsolationFlow) {
    Data32Or |= BIT15;
  }
  MmioOr32 (
    (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_MISC_REG_2),
    Data32Or
    );

  //
  //  HOST_CTRL_PORT_LINK_REG - SuperSpeed Port Link Control
  //  Address Offset: 0x80EC
  //
  if (PrivateConfig->IpVersion < V19_1) {
    Data32Or    = 0u;
    Data32And   = (UINT32)~(BIT17 | BIT14 | BIT13 | BIT12);
    if (PrivateConfig->IpVersion == V19_0) {
      Data32And &= (UINT32)~(BIT11 | BIT10);
      Data32Or  |= BIT9;
    } else {
      Data32And &= (UINT32)~(BIT11 | BIT10 | BIT9);
    }
  } else {
    Data32Or  = BIT10;
    Data32And = (UINT32)~(BIT11 | BIT9);
  }
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_PORT_LINK_REG),
    Data32And,
    Data32Or
    );

  if (((PrivateConfig->IpVersion == V19_0) && (PrivateConfig->Location == Standalone)) ||
    (PrivateConfig->IpVersion == V19_1)) {
    //
    // USB2_PROTOCOL_GAP_TIMER_HIGH_REG - USB2 Protocol Gap Timer HIGH
    // Offset: 0x8138
    //
    MmioAndThenOr32 (
      (XhciMmioBase + R_XHCI_MEM_USB2_PROTOCOL_GAP_TIMER_HIGH_REG),
      (UINT32)~(B_XHCI_MEM_USB2_PROTOCOL_GAP_TIMER_HIGH_REG_GAP_TIME_AFTER_LS_TX),
      (UINT32) (0x16 << N_XHCI_MEM_USB2_PROTOCOL_GAP_TIMER_HIGH_REG_GAP_TIME_AFTER_LS_TX)
      );
  }

  //
  //  AUX_CTRL_REG2 - Aux PM Control Register 2
  //  Address Offset: 0x8154
  //
  Data32Or  = (UINT32) (BIT31);
  Data32And = (UINT32)~(BIT21);
  if (PrivateConfig->IpVersion == V19_0) {
    Data32And &= (UINT32)~(BIT13);
  }
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_AUX_CTRL_REG2),
    Data32And,
    Data32Or
    );

  //
  //  HOST_CTRL_SCH_REG - Host Control Scheduler
  //  Address Offset: 0x8094
  //
  if (PrivateConfig->Location == Standalone) {
    MmioOr32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_SCH_REG),
      (UINT32) (BIT14 | BIT13)
      );
  }

  //
  //  HOST_IF_CTRL_REG - Host Controller Interface Control Register
  //  Address Offset: 0x8108
  //
  if (DeviceId == V_XHCI_DID_PCH_SERVER_V18_0) {
    MmioAnd32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_IF_CTRL_REG),
      (UINT32)~(BIT30)
      );
  } else {
    MmioOr32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_IF_CTRL_REG),
      (UINT32) (BIT30)
      );
  }

  //
  //  HOST_IF_PWR_CTRL_REG0 - Power Scheduler Control 0
  //  Address Offset: 0x8140
  //
  if (PrivateConfig->IpVersion < V19_1) {
    Data32 = 0xFF00F03C;
    if ((DeviceId == V_XHCI_DID_PCH_LP_V18_0) ||
        (DeviceId == V_XHCI_DID_PCH_P_V18_0) ||
        (DeviceId == V_XHCI_DID_PCH_N_V18_0) ||
        (DeviceId == V_XHCI_DID_PCH_H_V18_1) ||
        (DeviceId == V_XHCI_DID_PCH_SERVER_V18_0)) {
      Data32 = 0xFF02003C;
    }
    MmioWrite32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_IF_PWR_CTRL_REG0),
      Data32
      );
  }

  if (DeviceId != V_XHCI_DID_PCH_SERVER_V18_0) {
    //
    //  HOST_IF_PWR_CTRL_REG1 - Power Scheduler Control 1
    //  Address Offset: 0x8144
    //
    Data32Or = 0u;
    if ((PrivateConfig->Location == Tcss) || UsbConfig->HsiiEnable) {
      Data32Or |= BIT8;
    }
    if ((PrivateConfig->Location == Standalone) &&
      ((DeviceId != V_XHCI_DID_PCH_B_V18_0) &&
      (DeviceId != V_XHCI_DID_PCH_S_V19_0) &&
      (DeviceId != V_XHCI_DID_PCH_S_1_V19_0))) {
      Data32Or |= BIT24;
    }
    MmioOr32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_IF_PWR_CTRL_REG1),
      Data32Or
      );

    //
    //  USBLPM - USB LPM Parameters
    //  Address Offset: 0x8170
    //
    MmioAnd32 (
      (XhciMmioBase + R_XHCI_MEM_USBLPM),
      (UINT32)~(0x3FF)
      );

    //
    //  xHC Latency Tolerance Parameters - LTV Control
    //  Address Offset: 0x8174
    //
    if ((PrivateConfig->Location == Standalone) &&
        ((DeviceId == V_XHCI_DID_PCH_S_V19_0) ||
        (DeviceId == V_XHCI_DID_PCH_S_1_V19_0))) {
      MmioAndThenOr32 (
        (XhciMmioBase + R_XHCI_MEM_XLTP_LTV1),
        (UINT32)~(B_XHCI_MEM_XLTP_LTV1_USB2_PL0_LTV),
        (UINT32) (BIT25 | BIT24 | 0x42C)
        );
    }

    //
    //  xHC Latency Tolerance Control 2 - LTV Control 2
    //  Address Offset: 0x8178
    //
    if (PrivateConfig->Location == Standalone) {
      Data32Or = 0u;

      if (PrivateConfig->IpVersion < V19_1) {
        Data32Or |= BIT15;
      }

      if (DeviceId == V_XHCI_DID_PCH_B_V18_0) {
        Data32Or |= 0xA67;
      } else if ((DeviceId == V_XHCI_DID_PCH_H_V18_1) && (PrivateConfig->LtvLimit != 0)) {
        Data32Or |= (UINT32) (PrivateConfig->LtvLimit & B_XHCI_MEM_XHCLTVCTL2_LTV_LIMIT);
      } else if ((DeviceId == V_XHCI_DID_PCH_S_V19_0) || (DeviceId == V_XHCI_DID_PCH_S_1_V19_0)) {
        Data32Or |= 0x91A;
      } else {
        Data32Or |= 0x990;
      }
      Data32And = (UINT32)~(0x1FFF);
      MmioAndThenOr32 (
        (XhciMmioBase + R_XHCI_MEM_XHCLTVCTL2),
        Data32And,
        Data32Or
        );
    }

    //
    //  xHC Latency Tolerance Parameters - High Idle Time Control
    //  Address Offset: 0x817C
    //
    MmioWrite32 (
      (XhciMmioBase + R_XHCI_MEM_LTVHIT),
      PrivateConfig->LtrHigh
      );

    //
    //  xHC Latency Tolerance Parameters - Medium Idle Time Control
    //  Address Offset: 0x8180
    //
    MmioWrite32 (
      (XhciMmioBase + R_XHCI_MEM_LTVMIT),
      PrivateConfig->LtrMid
      );

    //
    //  xHC Latency Tolerance Parameters - Low Idle Time Control
    //  Address Offset: 0x8184
    //
    MmioWrite32 (
      (XhciMmioBase + R_XHCI_MEM_LTVLIT),
      PrivateConfig->LtrLow
      );

    //
    //  USB2 U2 LTR Policy
    //  Address Offset: 0x8188
    //
    if (PrivateConfig->Location == Standalone) {
      Data32 = (1 << (UsbController->Usb2PortCount - UsbController->UsbrPortCount)) - 1;
      MmioWrite32 (
        (XhciMmioBase + R_XHCI_MEM_CFG_USB2_LTV_U2_NOREQ_REG),
        Data32
        );
    }
  }

  //
  //  D0I2CTRL - D0I2 Control Register
  //  Address Offset: 0x81BC
  //
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_D0I2CTRL),
    (UINT32)~(BIT31),
    (0x1F << N_XHCI_MEM_D0I2CTRL_MSID0I2PWT)
    );

  //
  //  D0i2SchAlarmCtrl - D0i2 Scheduler Alarm Control Register
  //  Address Offset: 0x81C0
  //
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_D0I2SCH_ALARM_CTRL),
    (UINT32)~(B_XHCI_MEM_D0I2SCH_ALARM_CTRL),
    (UINT32) ((0x2D << N_XHCI_MEM_D0I2SCH_ALARM_CTRL_D0I2IT) | 0xF)
    );

  //
  //  USB2PMCTRL - USB2 Power Management Control
  //  Address Offset: 0x81C4
  //
  Data32Or  = (UINT32) (BIT11 | BIT8);
  Data32And = (UINT32)~(BIT10 | BIT9);
  if (DeviceId != V_XHCI_DID_PCH_B_V18_0) {
    Data32Or  |= (BIT3 | BIT1);
    Data32And &= (UINT32)~(BIT2 | BIT0);
  } else {
    Data32And &= (UINT32)~(0xF);
  }
  if ((DeviceId == V_XHCI_DID_PCH_LP_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_H_V18_1) ||
      (DeviceId == V_XHCI_DID_PCH_SERVER_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_P_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_N_V18_0)) {
    Data32Or |= BIT13;
  }
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_USB2PMCTRL),
    Data32And,
    Data32Or
    );

  //
  //  AUX_CTRL_REG3 - Aux PM Control 3 Register
  //  Address Offset: 0x81C8
  //
  Data32Or = BIT6;
  Data32And = ~0u;
  if ((PrivateConfig->IpVersion >= V19_0) && (PrivateConfig->Location == Standalone)) {
    Data32Or |= BIT8;
  }
  if ((DeviceId == V_XHCI_DID_PCH_LP_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_H_V18_1) ||
      (DeviceId == V_XHCI_DID_PCH_SERVER_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_V19_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_1_V19_0) ||
      (DeviceId == V_XHCI_DID_PCH_P_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_N_V18_0)) {
    Data32And &= (UINT32)~(BIT14);
  }
  if ((DeviceId == V_XHCI_DID_PCH_LP_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_P_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_N_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_H_V18_1) ||
      (DeviceId == V_XHCI_DID_PCH_S_V19_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_1_V19_0)) {
    Data32And &= (UINT32)~(B_XHCI_MEM_AUX_CTRL_REG3_HYSTERESIS_TIMEOUT);
    Data32Or |= (UINT32)(B_XHCI_MEM_AUX_CTRL_REG3_HYST_TIMEOUT_150US);
  }
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_AUX_CTRL_REG3),
    Data32And,
    Data32Or
    );

  //
  //  HOST_CTRL_SUS_LINK_PORT_REG
  //  Address Offset: 0x81F8
  //
  if ((DeviceId == V_XHCI_DID_PCH_LP_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_H_V18_1) ||
      (DeviceId == V_XHCI_DID_PCH_SERVER_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_P_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_N_V18_0)) {
    MmioOr32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_SUS_LINK_PORT_REG),
      (UINT32) (BIT17)
      );
  }

  if ((PrivateConfig->DisableBackToBackWRSupport == FALSE) && (PrivateConfig->Location == Standalone)) {
    //
    // HOST CTRL EARLY DBG REG
    // Address Offset: 0x81FC
    //
    MmioAndThenOr32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_EARLY_DBG_REG),
      (UINT32)~(BIT0),
      (UINT32) (BIT1)
      );
  }

  //
  //  MULT_IN_SCH_POLICY - Multiple IN Scheduler Policy Register
  //  Address Offset: 0x82A0
  //
  if (DeviceId == V_XHCI_DID_PCH_H_V18_1) {
    MmioAndThenOr32 (
      (XhciMmioBase + R_XHCI_MEM_MULT_IN_SCH_POLICY),
      (UINT32)~(BIT6 | BIT5),
      (UINT32) (BIT4)
      );
  }

  if (DeviceId != V_XHCI_DID_PCH_SERVER_V18_0) {
    //
    // XHCI_ECN_REG - Misc Reg 3
    // Address Offset: 0x82FC
    //
    Data32Or = (UINT32) (BIT1 | BIT0);
    if (((PrivateConfig->Location == Standalone) && (PrivateConfig->IpVersion <= V18_1)) ||
        (DeviceId == V_XHCI_DID_PCH_S_1_V19_0) ||
        (DeviceId == V_XHCI_DID_PCH_S_V19_0)) {
      Data32Or |= BIT6;
    }
    MmioOr32 (
      (XhciMmioBase + R_XHCI_MEM_XHCI_ECN_REG),
      Data32Or
      );

    //
    //  PMREQ Control Register
    //  Address Offset: 0x83D0
    //
    Data32And = ~0u;
    Data32Or = (UINT32) (BIT24 | BIT15);
    if ((DeviceId == V_XHCI_DID_PCH_S_V19_0) || (DeviceId == V_XHCI_DID_PCH_S_1_V19_0)) {
      Data32Or |= BIT12;
    } else {
      Data32And &= (UINT32)~(BIT12);
    }
    MmioAndThenOr32 (
      (XhciMmioBase + R_XHCI_MEM_PMREQ_CTRL_REG),
      Data32And,
      Data32Or
      );
  }

  //
  //  DBCCTL - DBC Control
  //  Address Offset: 0x8760
  //
  MmioOr32 (
    (XhciMmioBase + R_XHCI_MEM_DBC_DBCCTL),
    (UINT32) ((0x1F << N_XHCI_MEM_DBC_DBCCTL_DISC_RXD_CNT) | BIT0)
    );

  if (PrivateConfig->IpVersion < V19_1) {
    //
    //  HOST_CTRL_SSP_LINK_REG2
    //  Address Offset: 0x8E68
    //
    MmioOr32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_SSP_LINK_REG2),
      (UINT32) (BIT2)
      );
  }

  if ((PrivateConfig->Location == Tcss) && (PrivateConfig->IpVersion >= V18_0)) {
    DEBUG ((DEBUG_INFO, "TCSS CPU XHCI_MEM_DAP_ESS_PORT RTCO Enable\n"));
    //
    //  DAP eSS Port <N> Control 1 Register
    //  Address Offset: 0x8AC8 + N * 0x10
    //
    for (Index = 0; Index < ReportedSsPortCount; Index++) {
      MmioAndThenOr32 (
        (XhciMmioBase + R_CPU_XHCI_MEM_DAP_ESS_PORT_CONTROL1 + (Index * S_CPU_XHCI_MEM_ESS_PORT_CONTROL1_SPACING)),
        (UINT32) ~0u,
        (UINT32) B_CPU_XHCI_MEM_DAP_ESS_PORT_CONTROL1_RTCO
        );
    }
  }

  //
  //  HOST_CTRL_SSP_LFPS_REG4
  //  Address Offset: 0x8E7C
  //
  Data32And = (UINT32)~(B_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4_SCD_LFPS_TIMEOUT |
                        B_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4_SCD_TX_COUNT);
  if ((DeviceId == V_XHCI_DID_PCH_B_V18_0) && (PrivateConfig->Stepping < B0)) {
    Data32Or = ((0x4 << N_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4_SCD_LFPS_TIMEOUT) |
                (0x6 << N_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4_SCD_TX_COUNT));
  } else {
    Data32Or = ((0x3C << N_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4_SCD_LFPS_TIMEOUT) |
                (0x2  << N_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4_SCD_TX_COUNT));
  }
  MmioAndThenOr32 (
    XhciMmioBase + R_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4,
    Data32And,
    Data32Or
    );

  if (PrivateConfig->Location == Tcss) {
    //
    //  HOST_CTRL_USB3_RECAL
    //  Address Offset: 0x8E84
    //
    MmioAnd32 (
      XhciMmioBase + R_XHCI_MEM_HOST_CTRL_USB3_RECAL,
      (UINT32)~(BIT31)
      );
  }

  if (PrivateConfig->Location == Tcss) {
    //
    //  HOST_CTRL_SSP_TUNNELING_REG
    //  Address Offset: 0x8EA0
    //
    MmioAndThenOr32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_SSP_TUNNELING_REG),
      (UINT32)~(BIT4 | BIT2 | BIT1),
      (UINT32) (BIT5 | BIT3 | BIT0)
      );
  }

  if (PrivateConfig->CpxProgramming) {
    //
    //  HOST_CTRL_USB3_CP13_DEEMPH
    //  Address Offset: 0x8E8C
    //
    MmioWrite32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_USB3_CP13_DEEMPH),
      PrivateConfig->Cp13Deemph.Value
      );

    //
    //  HOST_CTRL_USB3_CP14_DEEMPH
    //  Address Offset: 0x8E90
    //
    MmioWrite32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_USB3_CP14_DEEMPH),
      PrivateConfig->Cp14Deemph.Value
      );

    //
    //  HOST_CTRL_USB3_CP15_DEEMPH
    //  Address Offset: 0x8E94
    //
    MmioWrite32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_USB3_CP15_DEEMPH),
      PrivateConfig->Cp15Deemph.Value
      );

    //
    //  HOST_CTRL_USB3_CP16_DEEMPH
    //  Address Offset: 0x8E98
    //
    MmioWrite32 (
      (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_USB3_CP16_DEEMPH),
      PrivateConfig->Cp16Deemph.Value
      );
  }

  //
  //  HOST_CTRL_SSP_CONFIG_REG2
  //  Address Offset: 0x8E9C
  //
  Data32Or = (UINT32) (BIT27);
  Data32And = ~0u;
  if ((DeviceId == V_XHCI_DID_PCH_S_V19_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_1_V19_0) ||
      (DeviceId == V_XHCI_DID_CPU_LP_V19_0)) {
    Data32And &= (UINT32)~(BIT25 | BIT24 | BIT23 | BIT22 | BIT21);
    Data32Or |= BIT22;
  }
  if (PrivateConfig->IpVersion >= V19_0) {
    Data32Or |= BIT2;
  }
  MmioAndThenOr32 (
    (XhciMmioBase + R_XHCI_MEM_HOST_CTRL_SSP_CONFIG_REG2),
    Data32And,
    Data32Or
    );

  if (PrivateConfig->Location == Standalone) {
    //
    //  AUDIO_OFFLOAD_CTR
    //  Address Offset: 0x91F4
    //
    Data32Or = BIT3;
    if (PrivateConfig->IpVersion >= V19_1) {
      Data32Or |= (BIT5 | BIT4);
    } else {
      Data32Or |= BIT31;
    }
    MmioOr32 (
      (XhciMmioBase + R_XHCI_MEM_AUDIO_OFFLOAD_CTR),
      (UINT32) (BIT31 | BIT3)
      );

    //
    //  U2PRM_U2PRDE
    //  Address Offset: 0x92F4
    //
    Data32 = 0;
    if (PrivateConfig->IsPortResetMessagingSupported) {
      for (Index = 0; Index < MAX_USB2_PORTS; Index++) {
        if (UsbConfig->PortUsb20[Index].PortResetMessageEnable == TRUE) {
          Data32 |= (BIT0 << Index);
        }
      }
    }
    MmioWrite32 (
      (XhciMmioBase + R_XHCI_MEM_U2PRM_U2PRDE),
      Data32
      );
  }

  if ((DeviceId == V_XHCI_DID_PCH_S_V19_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_1_V19_0)) {
    //
    //  DAP Common Control Register
    //  Address Offset: 0x440
    //
    PchPcrAndThenOr32 (
      PID_XHCI,
      R_XHCI_PCR_DAP_COMMON_CONTROL_REG,
      ~0u,
      BIT13
      );
  }

  //
  //  Set 1 to enable Super Speed Ports terminations
  //  Required for Deep S3
  //
  if ((PrivateConfig->Location == Standalone) && (Callback->UsbIsLaneOwned != NULL)) {
    MaxUsb3Lanes = UsbController->Usb3LanesCount;
    Data32Or = 0;
    Data32And = (UINT32)((1 << MaxUsb3Lanes) - 1);
    for (LaneNum = 0; LaneNum < MaxUsb3Lanes; LaneNum++) {
      if (Callback->UsbIsLaneOwned (UsbHandle, LaneNum)) {
        Data32Or |= (1 << LaneNum);
      }
    }
  } else {
    Data32Or = (UINT32)((1 << UsbController->Usb3LanesCount) - 1);
    Data32And = ~0u;
  }
  MmioAndThenOr32 (
    XhciMmioBase + R_XHCI_MEM_SSPE,
    Data32And,
    Data32Or
    );

  //
  //  SSIC related programming
  //
  MmioOr32 (
    XhciMmioBase + R_XHCI_MEM_SSIC_CONF_REG2_PORT_1,
    (UINT32) (B_XHCI_MEM_SSIC_CONF_REG2_PORT_UNUSED | B_XHCI_MEM_SSIC_CONF_REG2_PROG_DONE)
    );

  MmioOr32 (
    XhciMmioBase + R_XHCI_MEM_SSIC_CONF_REG2_PORT_2,
    (UINT32) (B_XHCI_MEM_SSIC_CONF_REG2_PORT_UNUSED | B_XHCI_MEM_SSIC_CONF_REG2_PROG_DONE)
    );

  if (PrivateConfig->Location == Tcss) {
    //
    // Support use cases where customers will be able to use DBC in Green(Without unlock) state.
    //
    if (PrivateConfig->IsDebugEnabled) {
      MmioOr32 (XhciMmioBase + R_XHCI_MEM_GRP0_SAI_WAC_POLICY_LO, BIT25);
      MmioOr32 (XhciMmioBase + R_XHCI_MEM_GRP0_SAI_RAC_POLICY_LO, BIT25);
    }
  }

  //
  //  Survivability Register 1
  //  Address Offset: 0x83E8
  //
  if ((DeviceId == V_XHCI_DID_PCH_H_V18_1) ||
      (DeviceId == V_XHCI_DID_PCH_S_V19_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_1_V19_0)) {
    MmioOr32 (
      (XhciMmioBase + R_XHCI_MEM_SURVIVABILITY_REG1),
      (UINT32) BIT0
      );
  }

  //
  //  Survivability Register 2
  //  Address Offset: 0x83EC
  //
  if (((DeviceId == V_XHCI_DID_PCH_LP_V18_0) && (PrivateConfig->Stepping >= B0)) ||
      (DeviceId == V_XHCI_DID_PCH_H_V18_1) ||
      (DeviceId == V_XHCI_DID_PCH_P_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_N_V18_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_V19_0) ||
      (DeviceId == V_XHCI_DID_PCH_S_1_V19_0)) {
    MmioOr32 (
      (XhciMmioBase + R_XHCI_MEM_SURVIVABILITY_REG2),
      B_XHCI_MEM_SURVIVABILITY_REG2_DRIVE_DEV_BYP_CLK_READY
      );
  }
}

/**
  Locks xHCI configuration by setting the proper lock bits in controller

  @param[in]  UsbHandle               USB Handle

  @retval     EFI_SUCCESS             Configuration ended successfully
              EFI_INVALID_PARAMETER   UsbHandle is NULL
              EFI_UNSUPPORTED         USB controller was not found
**/
EFI_STATUS
XhciLockConfiguration (
  IN  USB_HANDLE      *UsbHandle
  )
{
  UINT64        XhciPciBase;
  USB_CONFIG    *UsbConfig;

  if (UsbHandle == NULL) {
    DEBUG ((DEBUG_ERROR, "Invalid parameters provided to %a.\n", __FUNCTION__));
    return EFI_INVALID_PARAMETER;
  }

  UsbConfig = UsbHandle->UsbConfig;
  XhciPciBase = UsbHandle->HostController->PciCfgBaseAddr;

  if (PciSegmentRead16 (XhciPciBase + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_INFO, "xHCI: PCI device NOT found. XhciPciBase 0x%8X\n", XhciPciBase));
    return EFI_UNSUPPORTED;
  }

  //
  // After xHCI is initialized, BIOS should lock the xHCI configuration registers to RO.
  // This prevent any unintended changes. There is also a lockdown feature for OverCurrent
  // registers. BIOS should set these bits to lock down the settings prior to end of POST.
  // 1. Set Access Control bit at XHCI PCI offset 40h[31] to 1b to lock xHCI register settings.
  // 2. Set OC Configuration Done bit at XHCI PCI offset 44h[31] to lock overcurrent mappings from
  //    further changes.
  //
  if (UsbConfig->XhciOcLock) {
    PciSegmentOr32 (XhciPciBase + R_XHCI_CFG_XHCC2, (UINT32) (B_XHCI_CFG_XHCC2_OCCFDONE));
  }

  //
  // Before setting ACCTRL, BIOS needs to clear USB3_PROG_DONE to indicate USB3 Link related BIOS
  // programming is complete
  //
  XhciMemorySpaceOpen (XhciPciBase, (UINT32) UsbHandle->Mmio);

  //
  // HOST_CTRL_USB3_PORT_CONFIG
  // Offset: 0x9470
  //
  MmioAnd32 (
    (UINT32) UsbHandle->Mmio + R_XHCI_MEM_HOST_CTRL_USB3_PORT_CONFIG2,
    (UINT32)~(B_XHCI_MEM_HOST_CTRL_USB3_PORT_CONFIG2_USB3_PROG_DONE)
    );

  XhciMemorySpaceClose (XhciPciBase);

  //
  // XHCI PCI offset 40h is write once register.
  // Unsupported Request Detected bit is write clear
  //
  PciSegmentAndThenOr32 (
    XhciPciBase + R_XHCI_CFG_XHCC1,
    (UINT32)~(B_XHCI_CFG_XHCC1_URD),
    (UINT32) (B_XHCI_CFG_XHCC1_ACCTRL)
    );

  return EFI_SUCCESS;
}

/**
  Prints USB_CONTROLLER configuration information
**/
STATIC
VOID
PrintUsbControllerInfo (
  IN  USB_CONTROLLER    *UsbController
  )
{
  DEBUG ((DEBUG_INFO, "  PciCfgBaseAddr %016llX\n", UsbController->PciCfgBaseAddr));
  DEBUG ((DEBUG_INFO, "  S/B/D/F %02X / %02X / %02X / %02X\n",
                         UsbController->Segment,
                         UsbController->Bus,
                         UsbController->Device,
                         UsbController->Function));
  DEBUG ((DEBUG_INFO, "  Usb2PortCount %08X\n", UsbController->Usb2PortCount));
  DEBUG ((DEBUG_INFO, "  UsbrPortCount %08X\n", UsbController->UsbrPortCount));
  DEBUG ((DEBUG_INFO, "  Usb3LanesCount %08X\n", UsbController->Usb3LanesCount));
}

/**
  Disable USB ports based on UsbConfiguration

  @param[in]  UsbController   Pointer to USB Controller structure
  @param[in]  UsbConfig       USB Config Block instance pointer
  @param[in]  XhciMmioBase    xHCI controller enabled MMIO base address
**/
STATIC
VOID
UsbPortsDisable (
  IN    USB_CONTROLLER    *UsbController,
  IN    USB_CONFIG        *UsbConfig,
  IN    UINT32            XhciMmioBase
  )
{
  UINT32        Usb2DisabledPorts;
  UINT32        Usb3DisabledPorts;
  UINT8         Port;
  EFI_STATUS    Status;

  Usb2DisabledPorts = 0;
  for (Port = 0; Port < UsbController->Usb2PortCount; Port++) {
    if (UsbConfig->PortUsb20[Port].Enable == FALSE) {
      Usb2DisabledPorts |= (BIT0 << Port);
    }
  }

  Usb3DisabledPorts = 0;
  for (Port = 0; Port < UsbController->Usb3LanesCount; Port++) {
    if (UsbConfig->PortUsb30[Port].Enable == FALSE) {
      Usb3DisabledPorts |= (BIT0 << Port);
    }
  }

  Status = UsbDisablePortsEx (
             XhciMmioBase,
             Usb2DisabledPorts,
             Usb3DisabledPorts,
             UsbController->Usb2PortCount,
             UsbController->Usb3LanesCount
             );

  //
  // If PDO register is locked, reset platform to unlock it
  //
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "%a - scheduling WarmReset to unlock PDO registers\n", __FUNCTION__));
    SiScheduleResetSetType (EfiResetWarm, NULL);
  }
}

/**
  Common entry point for PCH and CPU xHCI controller

  @param[in]  UsbHandle               USB Handle

  @retval     EFI_SUCCESS             Configuration ended successfully
              EFI_INVALID_PARAMETER   UsbConfig or UsbController were NULL
              EFI_UNSUPPORTED         USB controller was not found
**/
EFI_STATUS
XhciConfigure (
  IN  USB_HANDLE      *UsbHandle
  )
{
  UINT64                XhciPciBase;
  UINT32                XhciMmioBase;
  USB_CONTROLLER        *UsbController;
  USB_CONFIG            *UsbConfig;
  UINT16                DeviceId;

  DEBUG ((DEBUG_INFO, "XhciConfigure() - Start\n"));

  if (UsbHandle == NULL) {
    DEBUG ((DEBUG_ERROR, "Invalid parameters provided to %a.\n", __FUNCTION__));
    return EFI_INVALID_PARAMETER;
  }

  UsbController = UsbHandle->HostController;
  UsbConfig = UsbHandle->UsbConfig;

  DEBUG ((DEBUG_INFO, "UsbController structure info:\n"));
  PrintUsbControllerInfo (UsbController);

  XhciPciBase = UsbController->PciCfgBaseAddr;
  DeviceId = PciSegmentRead16 (XhciPciBase + PCI_DEVICE_ID_OFFSET);

  if (DeviceId == 0xFFFF) {
    DEBUG ((DEBUG_INFO, "xHCI: PCI device NOT found. XhciPciBase 0x%8X\n", XhciPciBase));
    return EFI_UNSUPPORTED;
  }

  DEBUG ((DEBUG_INFO, "[%a] xHCI Controller DID: 0x%4X\n", __FUNCTION__, DeviceId));
  DEBUG ((
    DEBUG_INFO,"[%a] IP Version: %a\n",
    __FUNCTION__, GetXhciIpVersionString (UsbHandle->PrivateConfig->IpVersion))
    );

  if (UsbHandle->PrivateConfig->IpVersion == UNKNOWN) {
    DEBUG ((DEBUG_ERROR, "%a - unknown xHCI IP version. Skipping entire xHCI initialization.\n", __FUNCTION__));
    return EFI_UNSUPPORTED;
  }

  //
  // Either use propsed LTR values or use ones provided through policy
  //
  if (UsbConfig->LtrOverrideEnable) {
    UsbHandle->PrivateConfig->LtrHigh = UsbConfig->LtrHighIdleTimeOverride;
    UsbHandle->PrivateConfig->LtrMid  = UsbConfig->LtrMediumIdleTimeOverride;
    UsbHandle->PrivateConfig->LtrLow  = UsbConfig->LtrLowIdleTimeOverride;
  }

  XhciMmioBase = (UINT32) UsbHandle->Mmio;

  //
  // Assign memory resources
  //
  XhciMemorySpaceOpen (
    XhciPciBase,
    XhciMmioBase
    );

  //
  // Program Xhci Port Disable Override.
  //
  if (((UsbHandle->PrivateConfig->Location == Standalone) ||
       (UsbHandle->PrivateConfig->Location == Tcss)) &&
       UsbConfig->PdoProgramming) {
    DEBUG ((DEBUG_INFO, "xHCI: PEI phase PDO programming\n"));
    UsbPortsDisable (UsbController, UsbConfig, XhciMmioBase);
  }

  XhciHcInit (
    UsbHandle,
    XhciMmioBase
    );

  //
  // Setup USB Over-Current Mapping.
  //
  XhciOverCurrentMapping (UsbHandle, XhciMmioBase);

  //
  // Clear memory resources
  //
  XhciMemorySpaceClose (XhciPciBase);


  DEBUG ((DEBUG_INFO, "XhciConfigure() - End\n"));
  return EFI_SUCCESS;
}
