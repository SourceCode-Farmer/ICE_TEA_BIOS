/** @file
  Implementation of functions available in general USB Library

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/UsbLib.h>

#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/IoLib.h>
#include <Register/UsbRegs.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PciSegmentLib.h>
#include <IndustryStandard/Pci22.h>

/**
  Function for direct programming in xHCI's USB2 and USB3 PDO registers

  @param[in]  XhciMmioBase        xHCI Memory BAR0 address
  @param[in]  XhciUsb2Pdo         USB2 lane bitmap that will be written to USB2PDO register
  @param[in]  XhciUsb2PdoMask     USB2 PDO port count bitmask
  @param[in]  XhciUsb3Pdo         USB3 lane bitmap that will be written to USB3PDO register
  @param[in]  XhciUsb3PdoMask     USB3 PDO port count bitmask

  @retval     EFI_SUCCESS         Programming ended successfully and no errors occured
              EFI_ACCESS_DENIED   Port Disable Override register was locked and write
                                  didn't go through. Platform may require restart to unlock.
**/
STATIC
EFI_STATUS
ProgramUsbPdo (
  IN  UINTN   XhciMmioBase,
  IN  UINT32  XhciUsb2Pdo,
  IN  UINT32  XhciUsb2PdoMask,
  IN  UINT32  XhciUsb3Pdo,
  IN  UINT32  XhciUsb3PdoMask
  )
{
  UINT32      XhciUsb2PdoRd;
  UINT32      XhciUsb3PdoRd;

  DEBUG ((DEBUG_INFO, "XhciUsb2Pdo 0x%8X, XhciUsb3Pdo 0x%8X\n", XhciUsb2Pdo, XhciUsb3Pdo));

  //
  // USB2PDO and USB3PDO are Write-Once registers and bits in them are in the SUS Well.
  //
  MmioWrite32 (XhciMmioBase + R_PCH_XHCI_MEM_USB2PDO, XhciUsb2Pdo);
  MmioWrite32 (XhciMmioBase + R_PCH_XHCI_MEM_USB3PDO, XhciUsb3Pdo);

  XhciUsb2PdoRd = MmioRead32 (XhciMmioBase + R_PCH_XHCI_MEM_USB2PDO);
  XhciUsb3PdoRd = MmioRead32 (XhciMmioBase + R_PCH_XHCI_MEM_USB3PDO);

  //
  // If USB2PDO and USB3PDO are not updated successfully perform Warm Reset to unlock RWO bits.
  //
  if (((XhciUsb2Pdo & XhciUsb2PdoMask) != (XhciUsb2PdoRd & XhciUsb2PdoMask)) ||
      ((XhciUsb3Pdo & XhciUsb3PdoMask) != (XhciUsb3PdoRd & XhciUsb3PdoMask))) {
    DEBUG ((DEBUG_INFO, "USB2PDO - Read: 0x%8X, Expected: 0x%8X\n", XhciUsb2PdoRd, XhciUsb2Pdo));
    DEBUG ((DEBUG_INFO, "USB3PDO - Read: 0x%8X, Expected: 0x%8X\n", XhciUsb3PdoRd, XhciUsb3Pdo));
    DEBUG ((DEBUG_ERROR, "UsbDisablePorts: PDO register value doesn't match expected value. PDO register might be locked.\n"));
    return EFI_ACCESS_DENIED;
  }

  return EFI_SUCCESS;
}

/*
  Disables requested ports through Port Disable Override register programming

  @param[in]  XhciMmioBase            xHCI Memory BAR0 address
  @param[in]  Usb2DisabledPorts       Disabled ports bitmap with a bit for each USB2 port
                                      i.e. BIT0 is Port 0, BIT1 is Port 1 etc
  @param[in]  Usb3DisabledPorts       Disabled ports bitmap with a bit for each USB3 port
                                      i.e. BIT0 is Port 0, BIT1 is Port 1 etc

  @retval     EFI_SUCCESS             Programming ended successfully and no errors occured
              EFI_ACCESS_DENIED       Port Disable Override register was locked and write
                                      didn't go through. Platform may require restart to unlock.
*/
EFI_STATUS
UsbDisablePorts (
  IN  UINTN   XhciMmioBase,
  IN  UINT32  Usb2DisabledPorts,
  IN  UINT32  Usb3DisabledPorts
  )
{
  UINT32      HsPortCount;
  UINT32      SsPortCount;

  //
  // Read supported USB ports count from Host Controller
  //
  HsPortCount = (MmioRead32 (XhciMmioBase + R_XHCI_MEM_XECP_SUPP_USB2_2) & 0x0000FF00) >> 8;
  SsPortCount = (MmioRead32 (XhciMmioBase + R_XHCI_MEM_XECP_SUPP_USB3_2) & 0x0000FF00) >> 8;

  return UsbDisablePortsEx (
           XhciMmioBase,
           Usb2DisabledPorts,
           Usb3DisabledPorts,
           HsPortCount,
           SsPortCount
           );
}

/*
  Disables requested ports through Port Disable Override register programming.
  This is extended function that allows for defining how many lanes are available
  since PDO constructed on per lane basis.

  @param[in]  XhciMmioBase        xHCI Memory BAR0 address
  @param[in]  Usb2DisabledPorts   Disabled ports bitmap with a bit for each USB2 port
                                  i.e. BIT0 is Port 0, BIT1 is Port 1 etc
  @param[in]  Usb3DisabledPorts   Disabled ports bitmap with a bit for each USB3 port
                                  i.e. BIT0 is Port 0, BIT1 is Port 1 etc
  @param[in]  Usb2LanesCount      Number of USB2 lanes
  @param[in]  Usb3LanesCount      Number of USB3 lanes

  @retval EFI_SUCCESS             Programming ended successfully and no errors occured
          EFI_ACCESS_DENIED       Port Disable Override register was locked and write
                                  didn't go through. Platform may require restart to unlock.
*/
EFI_STATUS
UsbDisablePortsEx (
  IN  UINTN   XhciMmioBase,
  IN  UINT32  Usb2DisabledPorts,
  IN  UINT32  Usb3DisabledPorts,
  IN  UINT32  Usb2LanesCount,
  IN  UINT32  Usb3LanesCount
  )
{
  UINT32      XhciUsb2Pdo;
  UINT32      XhciUsb2PdoMask;
  UINT32      XhciUsb3Pdo;
  UINT32      XhciUsb3PdoMask;

  DEBUG ((DEBUG_INFO, "[%a]: Usb2DisabledPorts = 0x%x\n", __FUNCTION__, Usb2DisabledPorts));
  DEBUG ((DEBUG_INFO, "[%a]: Usb3DisabledPorts = 0x%x\n", __FUNCTION__, Usb3DisabledPorts));
  DEBUG ((DEBUG_INFO, "[%a]: Usb2LanesCount    = 0x%x\n", __FUNCTION__, Usb2LanesCount));
  DEBUG ((DEBUG_INFO, "[%a]: Usb3LanesCount    = 0x%x\n", __FUNCTION__, Usb3LanesCount));

  //
  // Masks for PDO registers based on supported number of ports, with a bit set for each supported port
  //
  XhciUsb2PdoMask = (UINT32)((1 << Usb2LanesCount) - 1);
  XhciUsb3PdoMask = (UINT32)((1 << Usb3LanesCount) - 1);

  DEBUG ((DEBUG_INFO, "XhciUsb2PdoMask 0x%8X, XhciUsb3PdoMask 0x%8X\n", XhciUsb2PdoMask, XhciUsb3PdoMask));

  XhciUsb2Pdo = Usb2DisabledPorts & XhciUsb2PdoMask;  // XHCI PDO for HS
  XhciUsb3Pdo = Usb3DisabledPorts & XhciUsb3PdoMask;  // XHCI PDO for SS

  return ProgramUsbPdo (
           XhciMmioBase,
           XhciUsb2Pdo,
           XhciUsb2PdoMask,
           XhciUsb3Pdo,
           XhciUsb3PdoMask
           );
}

/**
  Set the XHCI controller's PCI CMD.MSE and CMD.BME bits.

**/
VOID
XhciMmioEnable (
  VOID
  )
{
  UINT32  XhciBar;
  UINT32  Command;
  UINT64  XhciBaseAddress;

  //
  // Step 1. Make sure the XHCI BAR is initialized.
  //         Check if lower 32 bits of 64-bit BAR are configured.
  //
  XhciBaseAddress = PchXhciPciCfgBase ();
  XhciBar = PciSegmentRead32 (XhciBaseAddress + R_XHCI_CFG_BAR0) & ~(0xF);

  if (XhciBar == 0xFFFFFFF0) {
    return;
  }
  if ((XhciBar & 0xFFFF0000) == 0) {
    //
    // If lower 32 bits are not configured, check upper 32 bits.
    //
    XhciBar = PciSegmentRead32 (XhciBaseAddress + R_XHCI_CFG_BAR0 + 4);
    if (XhciBar == 0) {
      return;
    }
  }

  //
  // Step 2. If XHCI's MSE (Memory Space Enable) or BME (Bus Master Enable) bits are cleared, set them.
  //
  Command = PciSegmentRead32 (XhciBaseAddress + PCI_COMMAND_OFFSET);
  if ((Command & (EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_BUS_MASTER)) != (EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_BUS_MASTER)) {
    PciSegmentOr32 (XhciBaseAddress + PCI_COMMAND_OFFSET, (EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_BUS_MASTER));
  }
}