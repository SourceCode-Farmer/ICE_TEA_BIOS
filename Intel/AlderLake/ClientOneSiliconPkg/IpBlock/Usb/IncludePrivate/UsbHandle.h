/** @file
  Header file for USB handle structure definition

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _USB_HANDLE_H_
#define _USB_HANDLE_H_

#include <ConfigBlock.h>
#include <Usb2PhyConfig.h>
#include <UsbConfig.h>
#include <UsbController.h>
#include <RegisterAccess.h>

typedef struct _USB_HANDLE USB_HANDLE;

// USB IP Version enumeration
typedef enum {
  UNKNOWN,
  V18_0,
  V18_1,
  V19_0,
  V19_1
} USB_IP_VERSION;

// USB controller location
typedef enum {
  Tcss,       // Type-C SubSystem
  Standalone  // Standalone
} USB_INTEGRATION;

// Location stepping
typedef enum {
  A0,
  A1,
  B0,
  B1,
  C0,
  C1,
  D0,
  D1
} INTEGRATION_STEPPING;

// CPX_DEEMPH register structure definition
typedef union {
  struct {
    UINT32  CminusOnePrecursor    :  6;  // Bits 0-5
    UINT32  Czero                 :  6;  // Bits 6-11
    UINT32  CplusOnePrecursor     :  6;  // Bits 12-17
    UINT32  ReservedBits          : 14;  // Reserved bits
  } Field;
  UINT32  Value;
} XHCI_CPX_DEEMPH;

//
// USB2 PHY reference frequencies values (MHz)
//
typedef enum {
  USB_FREQ_19_2 = 0u,
  USB_FREQ_24_0,
  USB_FREQ_96_0,
  USB_FREQ_MAX
} USB2_PHY_REF_FREQ;

/**
  Internal USB controller description
**/
typedef struct {
  BOOLEAN IsPortResetMessagingSupported;  // Holds true if controller supports port reset messaging
  BOOLEAN IsDebugEnabled;                 // CPU Debug information
  BOOLEAN IsSimulationEnvironment;        // Holds information whether code is executed in Simics simulation
  BOOLEAN ExternalPowerControl;           // Allow outside agent to configure power and clock gating
  BOOLEAN IsDbcDebugEnabled;              // Holds information whether DbC debug is enabled
  /**
    LTV Limit value that will be written to xHCI MMIO 0x8178[12:0] unless it's 0 then it'll be ignored.
    This setting for some PCH's will depend on CPU pairing.
  **/
  UINT16 LtvLimit;
  /**
    xHC Latency Tolerance Parameters used during initialization process
  **/
  UINT32 LtrHigh;
  UINT32 LtrMid;
  UINT32 LtrLow;
  USB_IP_VERSION       IpVersion;      // USB IP Version
  USB_INTEGRATION      Location;       // Location of USB controller
  INTEGRATION_STEPPING Stepping;       // Stepping of the owner
  BOOLEAN              CpxProgramming; // Flag whether to programm CPX_DEEMPH registers
  //
  //  Fields with values that will be programmed to CPX registers if CpxProgramming is enabled
  //
  XHCI_CPX_DEEMPH Cp13Deemph;      // Value to be set in CP13_DEEMPH register
  XHCI_CPX_DEEMPH Cp14Deemph;      // Value to be set in CP14_DEEMPH register
  XHCI_CPX_DEEMPH Cp15Deemph;      // Value to be set in CP15_DEEMPH register
  XHCI_CPX_DEEMPH Cp16Deemph;      // Value to be set in CP16_DEEMPH register

  BOOLEAN RxStandbySupport; // RxStandby capability flag
  BOOLEAN LaneDeassertInPs3Support; // Lane Deassert in PS3 capability flag

  /**
    This is internal switch which allow skip updating Chipset Init table from IP block flow
    EBG is not supporting updating SUS tables.
  **/
  BOOLEAN                    SkipWriteToChipsetInitTable;
  /**
    This is an internal switch to allow for enabling/disabling programming around
    xHCI USB2 Debug Mode Back to Back WR support
  **/
  BOOLEAN                    DisableBackToBackWRSupport;
  USB2_PHY_REF_FREQ          Usb2PhyRefFreq;  // USB2 PHY Reference Clock frequency
  BOOLEAN                    EnableHcResetIsolationFlow; // Enables Reset isolation flow triggered for HCRST
} USB_PRIVATE_CONFIG;

/**
  Configures HHPIO pins for USB OverCurrent detection

  @param[in]  UsbHandle         Pointer to USB_HANDLE instance
  @param[in]  OvercurrentPin    Index of OverCurrent Pin to be enabled
**/
typedef
VOID
(*USB_ENABLE_OVERCURRENT_PIN) (
  IN    USB_HANDLE        *UsbHandle,
  IN    UINT8             OverCurrentPin
  );

/**
  Disables USB Controller

  @param[in]  UsbHandle         Pointer to USB_HANDLE instance
**/
typedef
VOID
(*USB_CONTROLLER_DISABLE) (
  IN    USB_HANDLE        *UsbHandle
  );

/**
  Checks in FIA is given lane is USB owned

  @param[in]  UsbHandle         Pointer to USB_HANDLE instance
  @param[in]  LaneIndex         Index of lane to check
**/
typedef
BOOLEAN
(*USB_LANE_OWNED) (
  IN    USB_HANDLE        *UsbHandle,
  IN    UINT8             LaneNumber
  );

/**
  USB callbacks
  List of function pointer that can be passed to init library dcfor cross IP calls
**/
typedef struct {
  USB_ENABLE_OVERCURRENT_PIN  UsbEnableOvercurrentPin;
  USB_CONTROLLER_DISABLE      UsbHostControllerDisable;
  USB_CONTROLLER_DISABLE      UsbDeviceControllerDisable;
  USB_LANE_OWNED              UsbIsLaneOwned;
} USB_CALLBACK;

/**
  USB controller handle. Stores all data needed for USB IP initialization
**/
struct _USB_HANDLE {
  USB_CONTROLLER        *HostController;
  USB_CONTROLLER        *DeviceController;
  USB_CONFIG            *UsbConfig;
  USB2_PHY_CONFIG       *Usb2PhyConfig;
  USB_PRIVATE_CONFIG    *PrivateConfig;
  USB_CALLBACK          *Callback;
  REGISTER_ACCESS       *Usb2SbAccessPrivateControl;      /* P2SB Message type, USB2 controller Private Config Register (PCR) space access */
  REGISTER_ACCESS       *DciSbAccessMmio;                 /* P2SB MMIO type, DCI controller Private Config Register (PCR) space access */
  REGISTER_ACCESS       *XhciSbAccessMmio;                /* P2SB MMIO type, XHCI controller Private Config Register (PCR) space access */
  UINT64                Mmio;                             /* USB Temporary Bar */
};

#endif// _USB_HANDLE_H_
