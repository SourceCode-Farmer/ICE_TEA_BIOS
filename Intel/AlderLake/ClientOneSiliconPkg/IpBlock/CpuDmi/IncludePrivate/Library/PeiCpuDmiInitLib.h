/** @file
  Header file for the CPU Dmi Init library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_CPU_DMI_INIT_LIB_H_
#define _PEI_CPU_DMI_INIT_LIB_H_

#include <Library/PeiPchDmiLib.h>
#include <Library/PcdLib.h>
#include <HostBridgeConfig.h>

///
/// Driver Consumed PPI Prototypes
///
#include <Ppi/SiPolicy.h>

#define BUNDLE_STEP   0x20

/**
  initialize CPU DMI
**/
VOID
CpuDmiInit (
  VOID
  );
/**
  Initiate Speed Change for DMI SIP16

  @retval EFI_SUCCESS

**/
EFI_STATUS
CpuDmi16SpeedChange (
VOID
  );
/**
  Enable PCIe Legacy Interrupt for DMI SIP16
**/
VOID
CpuDmi16EnablePcieLegacyInterrupt (
  IN  UINT32                       DmiBar
  );

/**
  Enable PCIe Relaxed Order for DMI SIP16
**/
VOID
CpuDmi16EnablePcieRelaxedOrder (
  IN  UINT32                       DmiBar
  );

/**
  Configure DMI Coalescing
  @param[in] DmiBar               - DMIBAR address
**/
VOID
ConfigureCpuDmiCoalescing (
  IN  UINT32                       DmiBar
  );

/**
  Configure DMI Atomics
  @param[in] DmiBar               - DMIBAR address
**/
VOID
ConfigureCpuDmiAtomics (
  IN  UINT32                       DmiBar
  );

/**
  Configure DMI Server Error Reporting Mode
  @param[in] DmiBar               - DMIBAR address
**/
VOID
ConfigureCpuDmiSerm (
  IN  UINT32                       DmiBar
  );

/**
  Configure DMI SMI/SCI
  @param[in] DmiBar               - DMIBAR address
**/
VOID
ConfigureCpuDmiSmiSci (
  IN  UINT32                       DmiBar
  );

/**
  Configure DMI Replay Timer
  @param[in] DmiBar               - DMIBAR address
**/
VOID
ConfigureCpuDmiReplayTimer (
  IN  UINT32                       DmiBar
  );

/**
  Configure DMI Ack Latency

 @param[in] DmiBar         Dmi Base Address
**/
VOID
ConfigureCpuDmiAckLatency (
  IN  UINT32       DmiBar
  );

/**
  Configure DMI Design Specific Configuration
  @param[in] DmiBar               - DMIBAR address
**/
VOID
ConfigureCpuDmiDesignSpecificConfiguration (
  IN  UINT32                       DmiBar
  );

/**
  Configure DMI Enabling
  @param[in] DmiBar               - DMIBAR address
**/
VOID
ConfigureCpuDmiEnabling (
  IN  UINT32       DmiBar
  );

/**
  Configure DMI MultiVC support
  @param[in] DmiBar               - DMIBAR address
  @param[in] CpuDmiPreMemConfig   - Instance of CPU_DMI_PREMEM_CONFIG
**/
VOID
ConfigureCpuDmiMultiVC (
  IN  UINT32                       DmiBar,
  IN  CPU_DMI_PREMEM_CONFIG        *CpuDmiPreMemConfig
  );

/**
  Initialize DMI Tc/Vc mapping through SA-PCH.

  @retval EFI_SUCCESS
**/
EFI_STATUS
CpuDmiTcVcInit (
  VOID
  );

/**
  Map SA DMI TCs to VC

  @param[in] PchDmiTcVcMap        - Instance of PCH_DMI_TC_VC_MAP
  @param[in] DmiBar               - DMIBAR address

  @retval EFI_SUCCESS
**/
EFI_STATUS
SaSetDmiTcVcMapping (
  IN    PCH_DMI_TC_VC_MAP  *PchDmiTcVcMap,
  IN    UINT64             DmiBar
  );

/**
  Poll SA DMI negotiation completion

  @param[in] PchDmiTcVcMap        - Instance of PCH_DMI_TC_VC_MAP
  @param[in] DmiBar               - DMIBAR address

  @retval EFI_SUCCESS
**/
EFI_STATUS
SaPollDmiVcStatus (
  IN    PCH_DMI_TC_VC_MAP  *PchDmiTcVcMap,
  IN    UINT64             DmiBar
  );

/**
  DMI link training

@param[in] DmiBar               - DMIBAR address**/
VOID
DmiLinkTrain (
  IN  UINT64 DmiBar
  );


/**
  DMI Max Payload Size Init
**/
VOID
MaxPayloadSizeInit (
  VOID
  );


 /**
  Configure DMI nFTS

  @param[in] DmiBar                 - DMIBAR address
**/
VOID DmiConfigureNFts (
  IN UINT64                   Dmibar
);



/**
  Set SB Min Duration
**/
VOID
SetSbMin (
  VOID
  );
#endif
