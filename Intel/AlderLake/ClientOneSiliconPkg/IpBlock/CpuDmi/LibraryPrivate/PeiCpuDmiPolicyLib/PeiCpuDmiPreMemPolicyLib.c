/** @file
  This file provides services for CpuDmi PreMem Policy function

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Library/SiConfigBlockLib.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <CpuDmiPreMemConfig.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuDmiInfoLib.h>

/**
  Print CPU_DMI_PREMEM_CONFIG and serial out.

  @param[in] SiPreMemPolicyPpi            Pointer to a SI_PREMEM_POLICY_PPI
**/
VOID
CpuDmiPreMemPrintConfig (
  IN SI_PREMEM_POLICY_PPI     *SiPreMemPolicyPpi
  )
{
  EFI_STATUS            Status;
  CPU_DMI_PREMEM_CONFIG *CpuDmiPreMemConfig;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuDmiPreMemConfigGuid, (VOID *) &CpuDmiPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "------------------ DMI PreMem Config ------------------\n"));
  DEBUG ((DEBUG_INFO, " DmiMaxLinkSpeed : 0x%x\n", CpuDmiPreMemConfig->DmiMaxLinkSpeed));
  DEBUG ((DEBUG_INFO, " DmiAspm : 0x%x\n", CpuDmiPreMemConfig->DmiAspm));
  DEBUG ((DEBUG_INFO, " DmiNewFom : 0x%x\n", CpuDmiPreMemConfig->DmiNewFom));
  DEBUG ((DEBUG_INFO, " DmiCdrRelock : 0x%x\n", CpuDmiPreMemConfig->DmiCdrRelock));
  DEBUG ((DEBUG_INFO, " DmiHweq : 0x%x\n", CpuDmiPreMemConfig->DmiHweq));

}

/**
  Load Config block default

  @param[in] ConfigBlockPointer         Pointer to config block
**/
VOID
CpuDmiPreMemLoadConfigDefault (
  IN VOID          *ConfigBlockPointer
  )
{
  UINTN                  Index;
  CPU_DMI_PREMEM_CONFIG  *CpuDmiPreMemConfig;

  CpuDmiPreMemConfig = ConfigBlockPointer;

  DEBUG ((DEBUG_INFO, "CpuDmiPreMemConfig->Header.GuidHob.Name = %g\n", &CpuDmiPreMemConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "CpuDmiPreMemConfig->Header.GuidHob.Header.HobLength = 0x%x\n", CpuDmiPreMemConfig->Header.GuidHob.Header.HobLength));

  //
  // Initialize the DMI Configuration
  //
  CpuDmiPreMemConfig->DmiAspmCtrl            = 2;
  CpuDmiPreMemConfig->DmiAspm                = 2;
  CpuDmiPreMemConfig->DmiGen3EqPh2Enable     = 2;
  CpuDmiPreMemConfig->DmiGen3ProgramStaticEq = 1;
  CpuDmiPreMemConfig->Gen3LtcoEnable         = 0;
  CpuDmiPreMemConfig->DmiMaxLinkSpeed        = 1;
  CpuDmiPreMemConfig->DmiHweq                = 1;
  CpuDmiPreMemConfig->DmiAspmL1ExitLatency   = 4;
  CpuDmiPreMemConfig->DmiDeEmphasis          = 1;
  CpuDmiPreMemConfig->Gen3EqPhase23Bypass    = 0;
  CpuDmiPreMemConfig->Gen3EqPhase3Bypass     = 0;
  CpuDmiPreMemConfig->Gen3RtcoRtpoEnable     = 1;
  CpuDmiPreMemConfig->DmiGen3DsPresetEnable  = 0;
  CpuDmiPreMemConfig->DmiGen3UsPresetEnable  = 0;
  CpuDmiPreMemConfig->Gen4EqPhase23Bypass    = 0;
  CpuDmiPreMemConfig->Gen4EqPhase3Bypass     = 0;
  CpuDmiPreMemConfig->DmiGen4DsPresetEnable  = 0;
  CpuDmiPreMemConfig->DmiGen4UsPresetEnable  = 0;
  CpuDmiPreMemConfig->Gen4RtcoRtpoEnable     = 1;
  CpuDmiPreMemConfig->Gen4LtcoEnable         = 0;
  CpuDmiPreMemConfig->DmiNewFom              = 0;
  CpuDmiPreMemConfig->DmiCdrRelock           = 0;



  for (Index = 0; Index < GetMaxDmiLanes(); Index++) {
    CpuDmiPreMemConfig->DmiGen3RootPortPreset[Index]   = 4;
    CpuDmiPreMemConfig->DmiGen3EndPointPreset[Index]   = 7;
    CpuDmiPreMemConfig->DmiGen3EndPointHint[Index]     = 2;
    CpuDmiPreMemConfig->DmiGen3DsPortRxPreset[Index]   = 1;
    CpuDmiPreMemConfig->DmiGen3DsPortTxPreset[Index]   = 7;
    CpuDmiPreMemConfig->DmiGen3UsPortRxPreset[Index]   = 7;
    CpuDmiPreMemConfig->DmiGen3UsPortTxPreset[Index]   = 7;
    CpuDmiPreMemConfig->DmiGen3Ltcpre[Index]           = 2;
    CpuDmiPreMemConfig->DmiGen3Ltcpo[Index]            = 2;
    CpuDmiPreMemConfig->DmiGen4DsPortTxPreset[Index]   = 7;
    CpuDmiPreMemConfig->DmiGen4UsPortTxPreset[Index]   = 7;
    CpuDmiPreMemConfig->DmiGen4Ltcpre[Index]           = 7;
    CpuDmiPreMemConfig->DmiGen4Ltcpo[Index]            = 7;
  }

  for (Index = 0; Index < GetMaxDmiLanes() ; Index++){
   if (Index == 0) {
      CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Index].Cp = 0;
      CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Index].Cm = 0;
   } else if (Index == 1) {
      CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Index].Cp = 0xE;
      CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Index].Cm = 7;
   } else if (Index == 2) {
      CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Index].Cp = 0xA;
      CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Index].Cm = 6;
   } else {
      CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Index].Cp = 7;
      CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Index].Cm = 7;
   }
  }

  for (Index = 2; Index < 5; Index++) {
    CpuDmiPreMemConfig->DmiHwEqGen3CoeffList[Index].Cp  = 1;
    CpuDmiPreMemConfig->DmiHwEqGen3CoeffList[Index].Cm  = 1;
  }
  for (Index = 0; Index < GetMaxDmiBundles(); Index++) {
    ///
    /// Gen3 RxCTLE peaking default is 0 for DMI
    ///
    CpuDmiPreMemConfig->DmiGen3RxCtlePeaking[Index]  = 0;
  }
}

STATIC COMPONENT_BLOCK_ENTRY  mCpuDmiPreMemBlocks = {
  &gCpuDmiPreMemConfigGuid,
  sizeof (CPU_DMI_PREMEM_CONFIG),
  CPU_DMI_PREMEM_CONFIG_REVISION,
  CpuDmiPreMemLoadConfigDefault
};

/**
  Get CpuDmi config block table size.

  @retval      Size of config block
**/
UINT16
CpuDmiPreMemGetConfigBlockTotalSize (
  VOID
  )
{
  return mCpuDmiPreMemBlocks.Size;
}

/**
  Add CpuDmi ConfigBlock.

  @param[in] ConfigBlockTableAddress    The pointer to config block table

  @retval EFI_SUCCESS                   The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES          Insufficient resources to create buffer
**/
EFI_STATUS
CpuDmiPreMemAddConfigBlock (
  IN VOID           *ConfigBlockTableAddress
  )
{
  return AddComponentConfigBlocks (ConfigBlockTableAddress, &mCpuDmiPreMemBlocks, 1);
}
