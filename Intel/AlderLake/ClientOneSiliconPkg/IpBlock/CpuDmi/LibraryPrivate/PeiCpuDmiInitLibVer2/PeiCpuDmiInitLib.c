/** @file
  This file contains functions for CPU DMI configuration for SIP16

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/CpuPlatformLib.h>
#include <Library/PeiCpuDmiInitLib.h>
#include <Register/CpuDmiRegs.h>
#include <Register/CpuDmi16Regs.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/PeiServicesLib.h>
#include <CpuRegs.h>
#include <Library/CpuDmiInfoLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PeiCpuDmiInitLib.h>
#include <Library/PeiCpuPcieVgaInitLib.h>
#include <Library/PeiPchDmiLib.h>
#include <ConfigBlock/SiConfig.h>
#include <Register/PchDmiRegs.h>
#include <Register/PchDmi15Regs.h>
#include <Register/SaPcieDmiRegs.h>
#include <Register/PchPcrRegs.h>
#include <CpuPcieInfo.h>
#include <HostBridgeConfig.h>
#include <Library/SaPlatformLib.h>
#include <Library/CpuMailboxLib.h>
#include <Register/CpuPcieRegs.h>
typedef union {
  struct {
    UINT32  Low;
    UINT32  High;
  } Data32;
  UINT64 Data;
} UINT64_STRUCT;


/**
  Map SA DMI TCs to VC

  @param[in] PchDmiTcVcMap        - Instance of PCH_DMI_TC_VC_MAP
  @param[in] DmiBar               - DMIBAR address

  @retval EFI_SUCCESS             -  Succeed.
  @retval EFI_INVALID_PARAMETER   -  Wrong phase parameter passed in.
**/
EFI_STATUS
SaSetDmiTcVcMapping (
  IN    PCH_DMI_TC_VC_MAP   *PchDmiTcVcMap,
  IN    UINT64              DmiBar
  )
{
  UINT32  Data32And;
  UINT32  Data32Or;
  UINT8   Index;
  UINT16  Register;
  UINT8   VcId;
  UINT8   VcMap[PchDmiVcTypeMax];

  ZeroMem (VcMap, PchDmiVcTypeMax);

//pch.pdmi.core.v1ctl.en=0 # make sure VC1 is not enabled before starting if you haven't set this before, then you can skip it.
//cpu.dmi.cfg.vcch=0x10002
  MmioWrite32 ((UINTN)(DmiBar + R_PCIE_VCECH), 0x10002);
//cpu.dmi.cfg.pvccr1.evcc=1
  Data32And = (UINT32)~B_PCIE_PVCCR1_EVCC_MASK;
  Data32Or = (UINT32)(V_PCIE_PVCCR1_EVCC_2_VC << B_PCIE_PVCCR1_EVCC_OFFSET);
  MmioAndThenOr32 ((UINTN)(DmiBar + R_PCIE_PVCCR1), Data32And, Data32Or);

   DEBUG ((DEBUG_INFO, "DmiBar + R_PCIE_PVCCR1 = %x \n", MmioRead32 ((UINTN)(DmiBar + R_PCIE_PVCCR1))));

  ///
  /// Set the TC/VC mappings
  ///
  for (Index = 0; Index < PchDmiTcTypeMax; Index++) {
    VcMap[PchDmiTcVcMap->DmiTc[Index].Vc] |= (BIT0 << PchDmiTcVcMap->DmiTc[Index].TcId);
  }
  ///
  /// System BIOS must perform the following steps for VC0 configuration.
  ///   Program the TCs/VC0 map by setting DMIBAR offset 014h [7:1] = '0111 101b'.
  ///
  /// Virtual Channel for ME (VCm) Configuration
  /// This is configured by ConfigMemMe
  ///
  /// Step1. Assign Virtual Channel ID 7 to VCm:
  ///    Programming the DMIVCMRCTL DMI Port Register DMIBAR Offset 038h[26:24] = '111b'.
  ///
  /// Step2. Enable VCm:
  ///    Programming the DMIVMPRCTL DMI Port Register DMIBAR Offset 038h[31] = '1b'.
  ///
  /// Step3. Enable VCm by programming the DMIVCMRCTL DMI Port Register DMIBAR Offset 038h[31] = '1b'.
  ///
  for (Index = 0; Index < PchDmiVcTypeMax; Index++) {
    if (PchDmiTcVcMap->DmiVc[Index].Enable == TRUE) {
      ///
      /// Map TCs to VC, Set the VC ID, Enable VC
      ///
      VcId = PchDmiTcVcMap->DmiVc[Index].VcId;

      Data32And = (UINT32) (~(V_SA_DMIBAR_DMIVCCTL_ID | B_SA_DMIBAR_DMIVCCTL_TVM_MASK));
      Data32Or = VcId << N_SA_DMIBAR_DMIVCCTL_ID;
      Data32Or |= (UINT32) VcMap[Index];
      Data32Or |= N_SA_DMIBAR_DMIVCCTL_EN;

      switch (Index) {
        case PchDmiVcTypeVc0:
          Register = R_SA_DMIBAR_DMIVC0RCTL_OFFSET;
          break;

        case PchDmiVcTypeVc1:
          Register = R_SA_DMIBAR_DMIVC1RCTL_OFFSET;
          break;

        case PchDmiVcTypeVcm:
          Register = R_SA_DMIBAR_DMIVCMRCTL_OFFSET;
          break;

        default:
          return EFI_INVALID_PARAMETER;
      }

      MmioAndThenOr32 ((UINTN) (DmiBar + Register), Data32And, Data32Or);
    }
  }
  return EFI_SUCCESS;
}
/**
  Poll SA DMI negotiation completion

  @param[in] PchDmiTcVcMap        - Instance of PCH_DMI_TC_VC_MAP
  @param[in] DmiBar               - DMIBAR address

  @retval EFI_SUCCESS             -  Succeed.
  @retval EFI_INVALID_PARAMETER   -  Wrong phase parameter passed in.
**/
EFI_STATUS
SaPollDmiVcStatus (
  IN    PCH_DMI_TC_VC_MAP   *PchDmiTcVcMap,
  IN    UINT64              DmiBar
  )
{
  UINT8   Index;
  UINT16  Register;

  ///
  /// 6.2.3.2 - Step 4, Poll until VC1 has been negotiated
  ///    Read the DMIVC1RSTS DMI Port Register Offset 026h until [1]==0
  ///
  /// 6.2.3.4 - Step4. Poll the VCm Negotiation Pending bit until it reads 0:
  ///    Read the DMIVCMRSTS DMI Port Register Offset 03Eh until [1]==0
  ///
  for (Index = 0; Index < PchDmiVcTypeMax; Index++) {
    if (PchDmiTcVcMap->DmiVc[Index].Enable == TRUE) {
      switch (Index) {
        case PchDmiVcTypeVc0:
          Register = R_SA_DMIBAR_DMIVC0RSTS_OFFSET;
          break;

        case PchDmiVcTypeVc1:
          Register = R_SA_DMIBAR_DMIVC1RSTS_OFFSET;
          break;

        case PchDmiVcTypeVcm:
          Register = R_SA_DMIBAR_DMIVCMRSTS_OFFSET;
          break;

        default:
          return EFI_INVALID_PARAMETER;
      }

      ///
      /// Wait for negotiation to complete
      ///
      while ((MmioRead16 ((UINTN) (DmiBar + Register)) & B_SA_DMIBAR_DMISTS_NP) != 0);
    }
  }

  return EFI_SUCCESS;
}
/**
  DMI Max Payload Size Init
**/
VOID
MaxPayloadSizeInit (
  VOID
  )
{
  UINT64_STRUCT DmiBar;
  UINT32        Data32;
  UINT32        Data32And;
  UINT32        Data32Or;
  UINT32        DmiMaxPayloadSizeCap;
  UINT64        McBaseAddress;


  //
  // Read R_CPU_DMI16_DCAP register
  //
  McBaseAddress      = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  DmiBar.Data32.High = PciSegmentRead32 (McBaseAddress + R_SA_DMIBAR + 4);
  DmiBar.Data32.Low  = PciSegmentRead32 (McBaseAddress + R_SA_DMIBAR);
  DmiBar.Data       &= (UINT64) ~BIT0;
  ASSERT (DmiBar.Data != 0);

  Data32 = MmioRead32 ((UINTN)(DmiBar.Data + R_CPU_DMI16_DCAP));
  DmiMaxPayloadSizeCap = (UINT32) (Data32 & B_CPU_DMI16_DCAP_MPS);  // Apply this to extract Bit [2:0] only

  //
  //
  //
  // Write back after reading contents to ensure bit 0 of DMI DCAP register (RW_O) does not get over ridden
  //
  MmioWrite32 ((UINTN)(DmiBar.Data + R_CPU_DMI16_DCAP), Data32);

  //
  // Checking if PCIE_TL_CR_DCAP_0_0_0_DMIBAR[2:0] is set to 1.
  // If yes, then DMI can be configured for MPS of 256B or 128B
  //
  if (DmiMaxPayloadSizeCap == 0x1) {
    ///
    /// Verified that DMI MPS is 256/128B configurable.
    /// For SKL setting,
    /// PCIE_TL_CR_DCTL_0_0_0_DMIBAR[7:5] = 1 ; 256B of MPS
    /// PCIE_TL_CR_DCTL_0_0_0_DMIBAR[7:5] = 0 ; 128B of MPS
    ///
    Data32And = (UINT32) B_CPU_DMI16_DCTL_MPS_MASK;
    Data32Or  = (UINT32) (DmiMaxPayloadSizeCap  << 5);
    MmioAndThenOr32 ((UINTN)(DmiBar.Data + R_CPU_DMI16_DCTL), Data32And, Data32Or);
  }
  return;
}

/**
  Initialize DMI Tc/Vc mapping through SA-PCH.

  @retval EFI_SUCCESS
**/
EFI_STATUS
CpuDmiTcVcInit (
  VOID
  )
{
    EFI_STATUS                  Status;
  UINT64                      McD0BaseAddress;
  UINT64_STRUCT               DmiBar;
  PCH_DMI_TC_VC_MAP           PchDmiTcVcMap;

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
#endif

  McD0BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
  DmiBar.Data32.High = PciSegmentRead32 (McD0BaseAddress + R_SA_DMIBAR + 4);
  DmiBar.Data32.Low  =  PciSegmentRead32 (McD0BaseAddress + R_SA_DMIBAR);
  DmiBar.Data       &= (UINT64) ~BIT0;

  ///
  /// SA OPI Initialization
  ///
  if (!IsPchLinkDmi ()) {
    MmioOr8 ((UINTN) (DmiBar.Data + 0xA78), BIT1);
  }

  ///
  /// Get default PchDmiTcVcMap
  ///
  ZeroMem (&PchDmiTcVcMap, sizeof (PCH_DMI_TC_VC_MAP));
  PchDmiTcVcMapInit (&PchDmiTcVcMap);

  ///
  /// Program NB TC/VC mapping
  ///
  SaSetDmiTcVcMapping (&PchDmiTcVcMap, DmiBar.Data);

  ///
  /// Call PchDmiTcVcProgPoll
  ///
  Status = PchDmiTcVcProgPoll (&PchDmiTcVcMap);
  ASSERT_EFI_ERROR (Status);

  ///
  /// Poll NB negotiation completion
  ///
  SaPollDmiVcStatus (&PchDmiTcVcMap, DmiBar.Data);
  ASSERT_EFI_ERROR (Status);

  return EFI_SUCCESS;

}

#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
/**
  Set SB Min Duration
**/
VOID
SetSbMin (
  VOID
  )
{
  UINT64_STRUCT                DmiBar;
  UINT64                       McBaseAddress;

  McBaseAddress      = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  DmiBar.Data32.High = PciSegmentRead32 (McBaseAddress + R_SA_DMIBAR + 4);
  DmiBar.Data32.Low  = PciSegmentRead32 (McBaseAddress + R_SA_DMIBAR);
  DmiBar.Data       &= (UINT64) ~BIT0;
  // Set SB Min
  MmioWrite32 ((UINTN)(DmiBar.Data + R_SA_DMIBAR_OPIO_T_SB_MIN), 0x14);
  DEBUG ((DEBUG_INFO, "Set OPIO SB Min 0x14\n"));
}
#endif
