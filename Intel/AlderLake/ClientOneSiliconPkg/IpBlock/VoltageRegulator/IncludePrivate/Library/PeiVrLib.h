/** @file
  Header file for the Voltage Regulator (VR) initializations.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PEI_VR_LIB_H
#define _PEI_VR_LIB_H

#include <Ppi/SiPolicy.h>
#include <Library/BaseLib.h>
#include <VoltageRegulatorCommands.h>
#include <Library/ConfigBlockLib.h>
#include <SiPolicyStruct.h>

///
/// Voltage Regulator domains temporary Vr address and Svid Enbled.
///
typedef struct {
  UINT8                       VrAddress[MAX_VR_INDEX]; ///< Temporary VR address according to various VR domains.
  UINT8                       SvidEnabled[MAX_VR_INDEX];   ///< Svid Enabled\Disabled according to various VR domains. Bit 0 - SA, Bit1 - IA, Bit 2 - RING, Bit - 3- GT, Bit 4 - Fivr
  UINT8                       MaxVrIndex;
  UINT8                       MinVrIndex;
} VR_DOMAIN_TOPOLOGY;

///
///

/**
  Programs the VR parameters for the external VR's which support SVID communication.

  @param[in]  SiPolicyPpi         The SI Policy PPI instance
  @param[in]  SelectedCtdpLevel   Ctdp Level Selected in BIOS
**/
VOID
ConfigureSvidVrs (
  IN OUT SI_POLICY_PPI *SiPolicyPpi,
  IN UINT16            SelectedCtdpLevel
  );

/**
  Updates the Vr Config block with Intel default override values if needed

  @param[in]  CpuPowerMgmtVrConfig       CPU Power Management VR Config Block
  @param[in]  SelectedCtdpLevel          Ctdp Level Selected in BIOS
  @param[out] OverrideIccMax             Override IccMax setting
  @param[out] OverrideTdcCurrentLimit    Override TdcCurrentLimit setting
  @param[out] OverrideTdcTimeWindow      Override TdcTimeWindow setting
  @param[out] OverrideAcLoadline         Override AcLoadline setting
  @param[out] OverrideDcLoadline         Override DcLoadline setting
  @param[out] VccInAuxImonIccImax        Override VccInAuxImonIccImax setting
  @param[out] VccInAuxImonSlope          Override VccInAuxImonSlope setting
  @param[out] OverrideImonSlope          Override ImonSlope setting
**/
VOID
UpdateVrOverrides (
  IN  CPU_POWER_MGMT_VR_CONFIG *CpuPowerMgmtVrConfig,
  IN  UINT16                   SelectedCtdpLevel,
  OUT UINT16                   *OverrideIccMax,
  OUT UINT16                   *OverrideTdcCurrentLimit,
  OUT UINT32                   *OverrideTdcTimeWindow,
  OUT UINT16                   *OverrideAcLoadline,
  OUT UINT16                   *OverrideDcLoadline,
  OUT UINT16                   *VccInAuxImonIccImax,
  OUT UINT32                   *VccInAuxImonSlope,
  OUT UINT16                   *OverrideImonSlope
  );

/**
  Load CPU power management Vr Config block default for specific generation.

  @param[in, out] CpuPowerMgmtVrConfig   Pointer to CPU_POWER_MGMT_VR_CONFIG instance
**/
VOID
EFIAPI
PeiCpuLoadPowerMgmtVrConfigDefaultFru (
  IN OUT CPU_POWER_MGMT_VR_CONFIG    *CpuPowerMgmtVrConfig
  );

/**
  Update the VR override workarounds
  @param[in] ImonSlope  Slope Value from Policy
**/
VOID
VrOverrideWorkaroundFru (
  IN UINT16   ImonSlope
  );

/**
  Override the VR Voltage Limits (VMAX)
  @param[in] VrVoltageLimit  Vmax Value to be updated
**/
VOID
OverrideVrVoltageLimitWa (
  IN UINT16   VoltageLimit
  );

/**
  Print the Vr Config block.

  @param[IN] CpuPowerMgmtVrConfig  - CPU Power Management VR Config Block
**/
VOID
PrintVrOverrides (
  IN CPU_POWER_MGMT_VR_CONFIG  *CpuPowerMgmtVrConfig
  );

/**
  Programs the FIVR RFI settings based on XTAL clock frequency.

  @param[IN] CpuPowerMgmtVrConfig  - CPU Power Management VR Config Block
**/
VOID
UpdateFivrRfiSettings (
  IN CPU_POWER_MGMT_VR_CONFIG *CpuPowerMgmtVrConfig
  );

/**
  Get Vr topologies which contain generation specific Vr address and bit which represents Svid Enabled/Disabled. Transferred as parameter from Fru to IP block.

  @param[IN,OUT] - VrDomainTopology - It takes VR_DOMAIN_TOPOLOGY as parameter and returns same which contains VR address and Svid Enable/Disabled.
**/
VOID
GetCpuVrTopology (
  IN OUT VR_DOMAIN_TOPOLOGY *VrDomainTopology
  );

/**
  Returns the maximum number of voltage regulator domains.

  @retval Maximum Number of VR Domains
**/
UINT16
GetMaxNumVrsFru (
  VOID
  );

/**
  This function uses mailbox communication to control settled time of Richtek VR which is enabled for
  VccSA to meet IMVP8 VR settled time.

  @param[IN] - VrIndex - It represent VR domains like SA,IA,RING,GT.
**/
VOID
VrSettledTimerRt (
  IN UINTN VrIndex
  );

/**
  Return TRUE when the VCC In Demotion is supported in the specific VR.

  @param[in]  VrIndex   Index of VR.

  @retval TRUE     VCC IN Demotion is supported.
  @retval FALSE    VCC IN Demotion is not supported.
 */
BOOLEAN
IsVccInDemotionSupported (
  IN  UINTN  VrIndex
  );

/**
  Return TRUE when the Fast Vmode IccLimit is supported in the specific VR.

  @retval TRUE     IccLimit is supported.
  @retval FALSE    IccLimit is not supported.
 */
BOOLEAN
IsFastVmodeIccLimitSupport (
  VOID
  );

/**
  Get VR VendorId and ProdId

  @param[in,out] VendorId  Get VendorId from Pcode MailBox
  @param[in,out] ProdId    Get ProdId from Pcode MailBox
**/
VOID
GetVrVendorIdProdId (
  IN OUT UINT32 *VendorId,
  IN OUT UINT32 *ProdId
  );

/**
  Set the Slow Slew Rate for IA Domain to Fast/2
**/
VOID
SetIaSlowSlewRate (
  VOID
  );

#endif //_PEI_VR_LIB_H
