/** @file
  VR post initializations.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/CpuMailboxLib.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PciSegmentLib.h>
#include <Register/Cpuid.h>
#include <Library/CpuPolicyLib.h>
#include <Library/HobLib.h>
#include <Library/VoltageRegulatorDomains.h>
#include <VoltageRegulatorCommands.h>
#include <Library/PeiVrLib.h>
#include <CpuDataHob.h>
#include <Library/PeiVrDomainLib.h>
#include <Library/CpuCommonLib.h>

/**
  Print the Vr Config block.

  @param[IN] CpuPowerMgmtVrConfig  - CPU Power Management VR Config Block
**/
VOID
PrintVrOverrides (
  IN CPU_POWER_MGMT_VR_CONFIG *CpuPowerMgmtVrConfig
  )
{
  UINT16      MaxNumVrs;
  UINT32      Index = 0;

  ///
  /// Define Maximum Number of Voltage Regulator Domains.
  ///
  MaxNumVrs = GetMaxNumVrs ();

  DEBUG ((DEBUG_INFO, "------------------ CPU Power Mgmt VR Config ------------------\n"));

  for (Index = 0; Index < MaxNumVrs; Index++) {
    DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VrConfigEnable[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->VrConfigEnable[Index]));
    if (CpuPowerMgmtVrConfig->VrConfigEnable[Index] == 1) {
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : TdcCurrentLimit[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->TdcCurrentLimit[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : AcLoadline[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->AcLoadline[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : DcLoadline[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->DcLoadline[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Psi1Threshold[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Psi1Threshold[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Psi2Threshold[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Psi2Threshold[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Psi3Threshold[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Psi3Threshold[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Psi3Enable[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Psi3Enable[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Psi4Enable[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Psi4Enable[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : ImonSlope[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->ImonSlope[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : ImonOffset[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->ImonOffset[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : IccMax[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->IccMax[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : IccLimit[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->IccLimit[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VrVoltageLimit[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->VrVoltageLimit[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : TdcEnable[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->TdcEnable[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : TdcTimeWindow[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->TdcTimeWindow1[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : TdcLock[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->TdcLock[Index]));
    }
    DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG:: FastPkgCRampDisable for Index = %d : 0x%X\n", Index, CpuPowerMgmtVrConfig->FastPkgCRampDisable[Index]));
    DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG:: SlowSlewRate for Index = %d : 0x%X\n", Index, CpuPowerMgmtVrConfig->SlowSlewRate[Index]));
  }
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : PsysSlope : 0x%X\n", CpuPowerMgmtVrConfig->PsysSlope));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : PsysOffset : 0x%X\n", CpuPowerMgmtVrConfig->PsysOffset1));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : AcousticNoiseMitigation : 0x%X\n", CpuPowerMgmtVrConfig->AcousticNoiseMitigation));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : FivrRfiFrequency : 0x%X\n", CpuPowerMgmtVrConfig->FivrRfiFrequency));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : FivrSpreadSpectrum : 0x%X\n", CpuPowerMgmtVrConfig->FivrSpreadSpectrum));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : EnableMinVoltageOverride : 0x%X\n", CpuPowerMgmtVrConfig->EnableMinVoltageOverride));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : PreWake : 0x%X\n", CpuPowerMgmtVrConfig->PreWake));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : RampUp : 0x%X\n", CpuPowerMgmtVrConfig->RampUp));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : RampDown : 0x%X\n", CpuPowerMgmtVrConfig->RampDown));
  if (CpuPowerMgmtVrConfig->EnableMinVoltageOverride == 1) {
    DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : MinVoltageRuntime : 0x%X\n", CpuPowerMgmtVrConfig->MinVoltageRuntime));
    DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : MinVoltageC8 : 0x%X\n", CpuPowerMgmtVrConfig->MinVoltageC8));
  }
}

///
/// PS1 to PS0 Dynamic cutoff Control
///
typedef union {
  UINT32 Data;
  struct {
    UINT32 CValue              : 12; ///< C value
    UINT32 Reserved1           : 4;  ///< Reserved bit.
    UINT32 MValue              : 12; ///< M value.
    UINT32 Reserved2           : 3;  ///< Reserved bit.
    UINT32 PS1PS0DynamicEnable : 1;  ///< PS1 to PS0 Dynamic cutoff Enable, 1:Enable 0:Disable
  } Fields;
} MAILBOX_DATA_FORMAT_PS1PS0_DYNAMIC_CUTOFF_CONTROL;

///
/// PS2 to PS1 Dynamic cutoff Control
///
typedef union {
  UINT32 Data;
  struct {
    UINT32 CValue              : 12; ///< C value
    UINT32 Reserved1           : 4;  ///< Reserved bit.
    UINT32 MValue              : 12; ///< M value.
    UINT32 Reserved2           : 3;  ///< Reserved bit.
    UINT32 PS2PS1DynamicEnable : 1;  ///< PS1 to PS0 Dynamic cutoff Enable, 1:Enable 0:Disable
  } Fields;
} MAILBOX_DATA_FORMAT_PS2PS1_DYNAMIC_CUTOFF_CONTROL;

///
/// VCCIN Demotion
///
typedef union {
  UINT32 Data;
  struct {
    UINT32 QuiescentPower : 8;  ///< Quiescent Power
    UINT32 Capacitance    : 23; ///< Capacitance
    UINT32 Enable         : 1;  ///< Enable
  } Fields;
} MAILBOX_DATA_FORMAT_QUIESCENT_POWER_AND_PLATFORM_CAP;


///
/// Vsys Demotion
///
typedef union {
  UINT32 Data;
  struct {
    UINT32 VsysData1    : 4;
    UINT32 VsysData2    : 4;
    UINT32 Reserved1    : 24;
  } Fields;
} MAILBOX_DATA_FORMAT_VSYS;


/**
  Programs the VR parameters for the external VR's which support SVID communication.

  @param[IN OUT] SiPolicyPpi      The SI Policy PPI instance
  @param[in]  SelectedCtdpLevel   Ctdp Level Selected in BIOS
**/
VOID
ConfigureSvidVrs (
  IN OUT SI_POLICY_PPI *SiPolicyPpi,
  IN UINT16            SelectedCtdpLevel
  )
{
  EFI_STATUS                  Status;
  UINT32                      MailboxData;
  UINT32                      MailboxStatus;
  UINT32                      MailboxType;
  UINT64                      TempAcLoadline;
  UINT64                      TempDcLoadline;
  UINT8                       TempVrAddress;
  UINT8                       SvidEnabled;
  UINT8                       ConvertedTimeWindow;
  UINT16                      ConvertedPsysOffset;
  UINT16                      ConvertedImonOffset;
  UINTN                       VrIndex;
  CPU_POWER_MGMT_VR_CONFIG    *CpuPowerMgmtVrConfig;
  CPU_TEST_CONFIG             *CpuTestConfig;
  CPU_POWER_MGMT_PSYS_CONFIG  *CpuPowerMgmtPsysConfig;
  VR_DOMAIN_TOPOLOGY          VrDomainTopology;
  CPU_DATA_HOB                CpuDataHob;
  VOID                        *Hob;
  BOOLEAN                     IsTimeWindowInSeconds;
  UINT16                      IccMax[MAX_NUM_VRS];
  UINT16                      TdcCurrentLimit[MAX_NUM_VRS];
  UINT32                      TdcTimeWindow[MAX_NUM_VRS];
  UINT16                      AcLoadline[MAX_NUM_VRS];
  UINT16                      DcLoadline[MAX_NUM_VRS];
  UINT16                      ImonSlope[MAX_NUM_VRS];
  SI_PREMEM_POLICY_PPI        *SiPreMemPolicyPpi;
  OVERCLOCKING_PREMEM_CONFIG  *OverClockingConfig;
  UINT16                      ConvertedVccInAuxImonOffset;
  UINT16                      VccInAuxImonIccImax;
  UINT32                      VccInAuxImonSlope;
  PCODE_MAILBOX_INTERFACE                              MailboxCommand;
  MAILBOX_DATA_FORMAT_PS1PS0_DYNAMIC_CUTOFF_CONTROL    PS1PS0MailboxData;
  MAILBOX_DATA_FORMAT_PS2PS1_DYNAMIC_CUTOFF_CONTROL    PS2PS1MailboxData;
  MAILBOX_DATA_FORMAT_QUIESCENT_POWER_AND_PLATFORM_CAP QuiescentAndPlatformCap;
  MAILBOX_DATA_FORMAT_VSYS                             VsysData;

  DEBUG ((DEBUG_INFO, "VR: Configure SVID Vr's\n"));

  ///
  /// Get RC Policy settings through the SiPolicy PPI
  ///
  Status = PeiServicesLocatePpi (
             &gSiPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPolicyPpi
             );
  if (Status != EFI_SUCCESS) {
    //
    // SI_POLICY_PPI must be installed at this point
    //
    ASSERT (FALSE);
    return;
  }

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtVrConfigGuid, (VOID *)&CpuPowerMgmtVrConfig);
  if (CpuPowerMgmtVrConfig == NULL) {
    ASSERT_EFI_ERROR (Status);
    return;
  }

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuTestConfigGuid, (VOID *)&CpuTestConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtPsysConfigGuid, (VOID *)&CpuPowerMgmtPsysConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gOverclockingPreMemConfigGuid, (VOID *) &OverClockingConfig);
  ASSERT_EFI_ERROR (Status);


  DEBUG ((DEBUG_INFO, "VR: Update VR overrides\n"));
  CopyMem (IccMax, CpuPowerMgmtVrConfig->IccMax, sizeof (IccMax));
  CopyMem (TdcCurrentLimit, CpuPowerMgmtVrConfig->TdcCurrentLimit, sizeof (TdcCurrentLimit));
  CopyMem (TdcTimeWindow, CpuPowerMgmtVrConfig->TdcTimeWindow1, sizeof (TdcTimeWindow));
  CopyMem (AcLoadline, CpuPowerMgmtVrConfig->AcLoadline, sizeof (AcLoadline));
  CopyMem (DcLoadline, CpuPowerMgmtVrConfig->DcLoadline, sizeof (DcLoadline));
  CopyMem (ImonSlope, CpuPowerMgmtVrConfig->ImonSlope, sizeof (ImonSlope));
  VccInAuxImonIccImax = CpuPowerMgmtVrConfig->VccInAuxImonIccImax;
  VccInAuxImonSlope = CpuPowerMgmtVrConfig->VccInAuxImonSlope;

  UpdateVrOverrides (
    CpuPowerMgmtVrConfig,
    SelectedCtdpLevel,
    IccMax,
    TdcCurrentLimit,
    TdcTimeWindow,
    AcLoadline,
    DcLoadline,
    &VccInAuxImonIccImax,
    &VccInAuxImonSlope,
    ImonSlope
    );

  ///
  /// CPU VR MSR mailbox
  ///
  MailboxType = MAILBOX_TYPE_VR_MSR;

  ///
  ///  Configure Platform Level controls
  ///  PSYS Config
  ///
  /// -PsysOffset is defined as S16.7.8 fixed point
  /// -PsysSlope is defined as U16.1.15 fixed point
  /// -Policy Psys offset is defined in 1/1000 increments
  /// -Policy Psys slope is defined in 1/100 increments
  /// -Mailbox ImonOffset = (PlatPolicyPsysOffset * 2^8)/1000
  /// -Mailbox PsysSlope = (PlatPolicyPsysSlope * 2^15)/100
  /// -Adding half of divisor to dividend to account for rounding errors in fixed point arithmetic.
  ///
  if (CpuPowerMgmtVrConfig->PsysOffset1 != 0 || CpuPowerMgmtVrConfig->PsysSlope != 0 ) {
    if (CpuPowerMgmtVrConfig->PsysOffset1 & BIT15) {
      ///
      /// Offset is negative, need to use absolute value to perform mailbox conversion
      /// then convert back to 2's complement
      ///
      ConvertedPsysOffset = (UINT16)(((~(CpuPowerMgmtVrConfig->PsysOffset1 - 1)) * (1 << 8) + 500) / 1000 & VR_PSYS_OFFSET_MASK);
      ConvertedPsysOffset = (UINT16)(~ConvertedPsysOffset + 1);
    } else {
      ConvertedPsysOffset = (UINT16) ((CpuPowerMgmtVrConfig->PsysOffset1 * (1 << 8) + 500) / 1000);
    }
    MailboxData =  (UINT32) (ConvertedPsysOffset |
                   (((CpuPowerMgmtVrConfig->PsysSlope * (1 << 15) + 50)/100) << VR_PSYS_SLOPE_OFFSET));
    MailboxCommand.InterfaceData = 0;
    MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
    MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_PMON_CONFIG;
    MailboxCommand.Fields.Param2 = 0;
    DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_PSYS_CONFIG_CMD\n"));
    DEBUG ((DEBUG_INFO, "(MAILBOX) PsysOffset            = %d (1/1000)\n", CpuPowerMgmtVrConfig->PsysOffset1));
    DEBUG ((DEBUG_INFO, "(MAILBOX) PsysSlope             = %d (1/100)\n", CpuPowerMgmtVrConfig->PsysSlope));
    DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Data    = %d\n", MailboxData));
    Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
    if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
      DEBUG ((DEBUG_ERROR, "VR: Error Writing PSYS Config. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
    }
  }

  ///
  /// PSYS Pmax
  /// -PMax is defined as U16.10.6 fixed point
  /// -Policy Pmax is defined in 1/8 W increments
  /// -Mailbox Pmax = (PlatPolicyPmax * 2^6)/8
  ///
  if (CpuPowerMgmtPsysConfig->PsysPmax != 0) {
    MailboxData =  (UINT32)((CpuPowerMgmtPsysConfig->PsysPmax * (1<<6))/8);
    MailboxCommand.InterfaceData = 0;
    MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
    MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_PMON_PMAX;
    MailboxCommand.Fields.Param2 = 0;
    DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_PSYS_PMAX_CMD\n"));
    DEBUG ((DEBUG_INFO, "(MAILBOX) PsysPmax              = %d (1/8 Watt)\n", CpuPowerMgmtPsysConfig->PsysPmax));
    DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Data    = %d\n", MailboxData));
    Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
    if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
      DEBUG ((DEBUG_ERROR, "VR: Error Writing Psys PMAX. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
    }
  }

  ///
  /// Support Min Voltage override command. Min Voltage Runtime Data[7:0] and Min Voltage for C8 Data[15:8].
  /// - Mailbox Voltage limit defined in U8.1.7 volts in fixed point. Range 4120 to 1.999V
  /// - Policy defined in mV, Range 0 - 1999mV.
  ///
  if (CpuPowerMgmtVrConfig->EnableMinVoltageOverride == 1) {
    MailboxData =  (UINT32) ((CpuPowerMgmtVrConfig->MinVoltageRuntime) * ((1<<7) /1000)        ///< Min Voltage Runtime Data[7:0]
                            | (((CpuPowerMgmtVrConfig->MinVoltageC8) * ((1<<7) /1000)) << 8)); ///< Min Voltage C8 Data[15:8]
    MailboxCommand.InterfaceData = 0;
    MailboxCommand.Fields.Command = 0x59;
    MailboxCommand.Fields.Param1 = 0;
    MailboxCommand.Fields.Param2 = 0;
    DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_VCCIN_MIN_VOLTAGE\n"));
    DEBUG ((DEBUG_INFO, "(MAILBOX) MinVoltageRuntime     = %d mV\n", CpuPowerMgmtVrConfig->MinVoltageRuntime));
    DEBUG ((DEBUG_INFO, "(MAILBOX) MinVoltageC8          = %d mV\n", CpuPowerMgmtVrConfig->MinVoltageC8));
    DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Data    = %d\n", MailboxData));
    Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
    if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
      DEBUG ((DEBUG_ERROR, "VR: Error Writing Min Voltage Override Command. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
    }
  }

  ///
  /// VccIn AUX IccMax Current
  ///
  if (VccInAuxImonIccImax != 0) {
    MailboxData =  (UINT32) VccInAuxImonIccImax;
    MailboxCommand.InterfaceData = 0;
    MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
    MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_VCCINAUX_IMON_IMAX;
    MailboxCommand.Fields.Param2 = 0;
    DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_VCCINAUX_IMON_IMAX_CMD\n"));
    DEBUG ((DEBUG_INFO, "(MAILBOX) VccInAuxImonIccImax   = %d (1/4 Amp)\n", VccInAuxImonIccImax));
    Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
    if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
      DEBUG ((DEBUG_ERROR, "VR: Error Writing VccIn AUX IccMax Current. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
    }
  }

  ///
  /// Vsys
  ///
  if (CpuPowerMgmtVrConfig->EnableVsysCritical != 0) {
    Status                        = EFI_SUCCESS;
    MailboxStatus                 = PCODE_MAILBOX_CC_SUCCESS;
    ///
    /// Set Vsys mode
    ///
    VsysData.Data                 = 0;
    VsysData.Fields.VsysData1     = 8;  // Set Bit3 to 1
    MailboxCommand.InterfaceData  = 0;
    MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
    MailboxCommand.Fields.Param1  = MAILBOX_VR_SUBCMD_SVID_SET_VR;
    MailboxCommand.Fields.Param2  = (MAILBOX_VR_SUBCMD_SVID_SET_VR_VSYS_MODE << 4) | MAILBOX_VR_SUBCMD_SVID_SET_PSYS_VR;
    ///
    /// Write Vsys mode
    ///
    Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, VsysData.Data, &MailboxStatus);
    if (EFI_ERROR (Status) || (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS)) {
      DEBUG ((DEBUG_ERROR, "Mailbox Command Write Vsys mode failed. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
    }

    ///
    /// Set Vsys Critical Threshold
    ///
    VsysData.Data = (CpuPowerMgmtVrConfig->VsysCriticalThreshold * 0xFF) / CpuPowerMgmtVrConfig->VsysFullScale;
    MailboxCommand.InterfaceData  = 0;
    MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
    MailboxCommand.Fields.Param1  = MAILBOX_VR_SUBCMD_SVID_SET_VR;
    MailboxCommand.Fields.Param2  = (MAILBOX_VR_SUBCMD_SVID_SET_VR_CRIT_THRESHOLD << 4) | MAILBOX_VR_SUBCMD_SVID_SET_PSYS_VR;
    ///
    /// Write Vsys Critical Threshold
    ///
    Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, VsysData.Data, &MailboxStatus);
    if (EFI_ERROR (Status) || (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS)) {
      DEBUG ((DEBUG_ERROR, "Mailbox Command Write Vsys Critical Threshold failed. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
    }

    ///
    /// Set Vsys Assertion Deglitch
    ///
    VsysData.Data                 = 0;
    VsysData.Fields.VsysData1     = CpuPowerMgmtVrConfig->VsysAssertionDeglitchExponent;
    VsysData.Fields.VsysData2     = CpuPowerMgmtVrConfig->VsysAssertionDeglitchMantissa;
    MailboxCommand.InterfaceData  = 0;
    MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
    MailboxCommand.Fields.Param1  = MAILBOX_VR_SUBCMD_SVID_SET_VR;
    MailboxCommand.Fields.Param2  = (MAILBOX_VR_SUBCMD_SVID_SET_VR_CONFIG2 << 4) | MAILBOX_VR_SUBCMD_SVID_SET_PSYS_VR;
    ///
    /// Write Vsys Assertion Deglitch
    ///
    Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, VsysData.Data, &MailboxStatus);
    if (EFI_ERROR (Status) || (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS)) {
      DEBUG ((DEBUG_ERROR, "Mailbox Command Write Vsys Assertion Deglitch failed. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
    }

    ///
    /// Set Vsys De-Assertion Deglitch
    ///
    VsysData.Data                 = 0;
    VsysData.Fields.VsysData1     = CpuPowerMgmtVrConfig->VsysDeassertionDeglitchExponent;
    VsysData.Fields.VsysData2     = CpuPowerMgmtVrConfig->VsysDeassertionDeglitchMantissa;
    MailboxCommand.InterfaceData  = 0;
    MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
    MailboxCommand.Fields.Param1  = MAILBOX_VR_SUBCMD_SVID_SET_VR;
    MailboxCommand.Fields.Param2  = (MAILBOX_VR_SUBCMD_SVID_SET_VR_CONFIG1 << 4) | MAILBOX_VR_SUBCMD_SVID_SET_PSYS_VR;
    ///
    /// Write Vsys De-Assertion Deglitch
    ///
    Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, VsysData.Data, &MailboxStatus);
    if (EFI_ERROR (Status) || (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS)) {
      DEBUG ((DEBUG_ERROR, "Mailbox Command Write Vsys De-Assertion Deglitch failed. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
    }
  }


  ///
  ///  Configure VCCIn AUX IMON
  ///
  /// -VccInAuxImonOffset is defined as S16.7.8 fixed point
  /// -VccInAuxImonSlope is defined as U16.1.15 fixed point
  /// -Policy VccInAux Imon offset is defined in 1/1000 increments
  /// -Policy VccInAux Imon slope is defined in 1/100 increments
  /// -Mailbox VccInAuxImonOffset = (VccInAuxImonOffset * 2^8)/1000
  /// -Mailbox VccInAuxImonSlope = (VccInAuxImonSlope * 2^15)/100
  /// -Adding half of divisor to dividend to account for rounding errors in fixed point arithmetic.
  ///
  if (CpuPowerMgmtVrConfig->VccInAuxImonOffset != 0 || VccInAuxImonSlope != 0 ) {
    if (CpuPowerMgmtVrConfig->VccInAuxImonOffset & BIT15) {
      ///
      /// Offset is negative, need to use absolute value to perform mailbox conversion
      /// then convert back to 2's complement
      ///
      ConvertedVccInAuxImonOffset = (UINT16)(((~(CpuPowerMgmtVrConfig->VccInAuxImonOffset - 1)) * (1 << 8) + 500) / 1000 & VR_PSYS_OFFSET_MASK);
      ConvertedVccInAuxImonOffset = (UINT16)(~ConvertedVccInAuxImonOffset + 1);
    } else {
      ConvertedVccInAuxImonOffset = (UINT16) ((CpuPowerMgmtVrConfig->VccInAuxImonOffset * (1 << 8) + 500) / 1000);
    }
    ///
    /// Slope Correction is in U1.15 format
    /// Final Slope = (MAX_ICC / 0xFF) * Slope Correction
    ///
    MailboxData =  (UINT32) (ConvertedVccInAuxImonOffset |
                   (((UINT16)((VccInAuxImonSlope * (1 << 15) + 50)/100) << VR_PSYS_SLOPE_OFFSET)));
    MailboxCommand.InterfaceData = 0;
    MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
    MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_VCCINAUX_IMON_CONFIG;
    MailboxCommand.Fields.Param2 = 0;
    DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_VCCINAUX_IMON_CONFIG_CMD\n"));
    DEBUG ((DEBUG_INFO, "(MAILBOX) VccInAuxImonOffset            = %d (1/1000)\n", CpuPowerMgmtVrConfig->VccInAuxImonOffset));
    DEBUG ((DEBUG_INFO, "(MAILBOX) VccInAuxImonSlope             = %d (1/100)\n", VccInAuxImonSlope));
    DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Data    = %d\n", MailboxData));
    Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
    if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
      DEBUG ((DEBUG_ERROR, "VR: Error Writing VCCINAUX IMON Config. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
    }
  }

  ///
  /// Get CPU VR topology domains which are fru dependent
  ///
  GetCpuVrTopology (&VrDomainTopology);

  ///
  /// Initial cpu data into one hob.
  ///
  ZeroMem (&CpuDataHob, sizeof (CPU_DATA_HOB));

  for (VrIndex = VrDomainTopology.MinVrIndex; VrIndex < VrDomainTopology.MaxVrIndex; VrIndex++) {
    TempVrAddress = VrDomainTopology.VrAddress[VrIndex];
    SvidEnabled = VrDomainTopology.SvidEnabled[VrIndex];

    DEBUG ((DEBUG_INFO, " VR Address for Vr Index %x = 0x%x\n", VrIndex, VrDomainTopology.VrAddress[VrIndex]));
    DEBUG ((DEBUG_INFO, " VR SvidEnabled for Vr Index %x = 0x%x\n", VrIndex, VrDomainTopology.SvidEnabled[VrIndex]));

    DEBUG ((DEBUG_INFO, " Override IccMax[%x] = %d\n", VrIndex, IccMax[VrIndex]));
    DEBUG ((DEBUG_INFO, " Override TdcCurrentLimit[%x] = %d\n", VrIndex, TdcCurrentLimit[VrIndex]));
    DEBUG ((DEBUG_INFO, " Override TdcTimeWindow[%x] = %d\n", VrIndex, TdcTimeWindow[VrIndex]));
    DEBUG ((DEBUG_INFO, " Override AcLoadline[%x] = %d\n", VrIndex, AcLoadline[VrIndex]));
    DEBUG ((DEBUG_INFO, " Override DcLoadline[%x] = %d\n", VrIndex, DcLoadline[VrIndex]));
    DEBUG ((DEBUG_INFO, " Override ImonSlope[%x] = %d\n", VrIndex, ImonSlope[VrIndex]));

    ///
    /// Update Vr Address for CPU Data HOB
    ///
    CpuDataHob.VrAddress[VrIndex] = VrDomainTopology.VrAddress[VrIndex];
    CpuDataHob.SvidEnabled = CpuDataHob.SvidEnabled + (VrDomainTopology.SvidEnabled[VrIndex] << VrIndex);

    if (CpuPowerMgmtVrConfig->VrConfigEnable[VrIndex] == 1 && SvidEnabled == 1) {
      ///
      /// AC/DC Loadline
      ///
      if (AcLoadline[VrIndex] != 0 || DcLoadline[VrIndex] != 0) {
        ///
        ///  Check max AC/DC loadline boundary. Max allowed is 6249 (62.49 mOhm)
        ///
        if (AcLoadline[VrIndex] > AC_DC_LOADLINE_MAX){
          AcLoadline[VrIndex] = AC_DC_LOADLINE_MAX;
        }
        if (DcLoadline[VrIndex] > AC_DC_LOADLINE_MAX){
          DcLoadline[VrIndex] = AC_DC_LOADLINE_MAX;
        }

        ///
        ///  Loadline is 1/100 mOhm units. Mailbox interface requires Loadline in U-4.20 Ohms format.
        ///  After multiplying by 2^20, we must divide the result by 100,000 to convert to Ohms.
        ///  Adding half of divisor to dividend to account for rounding errors in fixed point arithmetic.
        ///
        TempAcLoadline = MultU64x64 (AcLoadline[VrIndex], LShiftU64 (1, 20));
        TempAcLoadline = DivU64x32 (TempAcLoadline + 50000, 100000);
        TempDcLoadline = MultU64x64 (DcLoadline[VrIndex], LShiftU64 (1, 20));
        TempDcLoadline = DivU64x32 (TempDcLoadline + 50000, 100000);

        MailboxData = (UINT32) (TempAcLoadline | (LShiftU64 (TempDcLoadline, DC_LOADLINE_OFFSET)));
        MailboxCommand.InterfaceData = 0;
        MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
        MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_ACDC_LOADLINE;
        MailboxCommand.Fields.Param2 = (TempVrAddress & VR_ADDRESS_MASK);
        DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_ACDC_LOADLINE_CMD\n"));
        DEBUG ((DEBUG_INFO, "(MAILBOX) AcLoadline            = %d (1/100 mOhms)\n", AcLoadline[VrIndex]));
        DEBUG ((DEBUG_INFO, "(MAILBOX) DcLoadline            = %d (1/100 mOhms)\n", DcLoadline[VrIndex]));
        Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
        if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
          DEBUG ((DEBUG_ERROR, "VR: Error Writing AC/DC Loadline. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        }
      }

      ///
      /// PS Cutoff Current
      ///
      MailboxData = 0;
      if (CpuPowerMgmtVrConfig->Psi3Enable[VrIndex] != 0) {
        MailboxData |=  (UINT32) ((~CpuPowerMgmtVrConfig->Psi3Enable[VrIndex] & BIT0) << PSI3_ENABLE_OFFSET);
        DEBUG ((DEBUG_INFO, "(MAILBOX) Psi3Enable            = %d\n", CpuPowerMgmtVrConfig->Psi3Enable[VrIndex]));
      }
      if (CpuPowerMgmtVrConfig->Psi4Enable[VrIndex] != 0) {
        MailboxData |=  (UINT32) ((~CpuPowerMgmtVrConfig->Psi4Enable[VrIndex] & BIT0) << PSI4_ENABLE_OFFSET);
        DEBUG ((DEBUG_INFO, "(MAILBOX) Psi4Enable            = %d\n", CpuPowerMgmtVrConfig->Psi4Enable[VrIndex]));
      }
      if (CpuPowerMgmtVrConfig->Psi1Threshold[VrIndex] != 0) {
        MailboxData |=  (UINT32)(CpuPowerMgmtVrConfig->Psi1Threshold[VrIndex] & PSI_THRESHOLD_MASK);
        DEBUG ((DEBUG_INFO, "(MAILBOX) Psi1Threshold         = %d (1/4 Amp)\n", CpuPowerMgmtVrConfig->Psi1Threshold[VrIndex]));
      }
      if (CpuPowerMgmtVrConfig->Psi2Threshold[VrIndex] != 0) {
        MailboxData |=  (UINT32) ((CpuPowerMgmtVrConfig->Psi2Threshold[VrIndex] & PSI_THRESHOLD_MASK) << PSI2_THRESHOLD_OFFSET);
        DEBUG ((DEBUG_INFO, "(MAILBOX) Psi2Threshold         = %d (1/4 Amp)\n", CpuPowerMgmtVrConfig->Psi2Threshold[VrIndex]));
      }
      if ((CpuPowerMgmtVrConfig->Psi3Threshold[VrIndex] != 0) && (CpuPowerMgmtVrConfig->Psi3Enable[VrIndex] != 0)) {
        MailboxData |=  (UINT32) ((CpuPowerMgmtVrConfig->Psi3Threshold[VrIndex] & PSI_THRESHOLD_MASK) << PSI3_THRESHOLD_OFFSET);
        DEBUG ((DEBUG_INFO, "(MAILBOX) Psi3Threshold         = %d (1/4 Amp)\n", CpuPowerMgmtVrConfig->Psi3Threshold[VrIndex]));
      }
      if (MailboxData != 0) {
        DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_PSI_CUTOFF_CMD\n"));
        MailboxCommand.InterfaceData = 0;
        MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
        MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_PS_CUTOFF;
        MailboxCommand.Fields.Param2 = (TempVrAddress & VR_ADDRESS_MASK);
        Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
        if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
          DEBUG ((DEBUG_ERROR, "VR: Error Writing PS Cutoff Current. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        }
      }
      /// -ImonOffset is defined as S16.7.8 fixed point
      /// -ImonSlope is defined as U16.1.15 fixed point
      /// -Policy Imon offset is defined in 1/1000 increments
      /// -Policy Imon slope is defined in 1/100 increments
      /// -Mailbox ImonOffset = (PlatPolicyImonOffset * 2^8)/1000
      /// -Mailbox ImonSlope = (PlatPolicyImonSlope * 2^15)/100
      /// -Adding half of divisor to dividend to account for rounding errors in fixed point arithmetic.
      ///
      if (CpuPowerMgmtVrConfig->ImonOffset[VrIndex] != 0 || ImonSlope[VrIndex] != 0) {
        if (CpuPowerMgmtVrConfig->ImonOffset[VrIndex] & BIT15) {
          ///
          /// Offset is negative, need to use absolute value to perform mailbox conversion
          /// then convert back to 2's complement
          ///
          ConvertedImonOffset = (UINT16)(((~(CpuPowerMgmtVrConfig->ImonOffset[VrIndex] - 1)) * (1 << 8) + 500) / 1000 & VR_IMON_OFFSET_MASK);
          ConvertedImonOffset = (UINT16)(~ConvertedImonOffset + 1);
        } else {
          ConvertedImonOffset = (UINT16) ((CpuPowerMgmtVrConfig->ImonOffset[VrIndex] * (1 << 8) + 500) / 1000);
        }
        MailboxData =  (UINT32)(ConvertedImonOffset | (((ImonSlope[VrIndex] * (1 << 15) + 50) / 100) << VR_IMON_SLOPE_OFFSET));
        MailboxCommand.InterfaceData = 0;
        MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
        MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_IMON_CONFIG;
        MailboxCommand.Fields.Param2 = (TempVrAddress & VR_ADDRESS_MASK);
        DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_IMON_CONFIG_CMD\n"));
        DEBUG ((DEBUG_INFO, "(MAILBOX) ImonOffset            = %d (1/1000)\n", CpuPowerMgmtVrConfig->ImonOffset[VrIndex]));
        DEBUG ((DEBUG_INFO, "(MAILBOX) ImonSlope             = %d (1/100)\n", ImonSlope[VrIndex]));
        Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
        if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
          DEBUG ((DEBUG_ERROR, "VR: Error Writing IMON Config. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        }
      }

      ///
      /// Icc Max
      /// When Overclocking is enabled, OCMB interface is used to override ICC Max
      ///
      if ((IccMax[VrIndex] != 0) && (OverClockingConfig->OcSupport != 1)) {
        MailboxData = (UINT32)IccMax[VrIndex];
        //
        // Config ICCMAX via PCODE Mailbox
        //
        MailboxCommand.InterfaceData = 0;
        MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
        MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_MAX_ICC;
        MailboxCommand.Fields.Param2 = (TempVrAddress & VR_ADDRESS_MASK);
        DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_VR_ICC_MAX_CMD\n"));
        DEBUG ((DEBUG_INFO, "(MAILBOX) IccMax                = %d (1/4 A)\n", MailboxData));
        Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
        if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
          DEBUG ((DEBUG_ERROR, "VR: Error Writing IccMax. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        }
      }

      ///
      /// Fast Vmode ICC Limit.
      ///
      if (IsFastVmodeIccLimitSupport ()) {
        //
        // Config Fast Vmode ICC Limit via PCODE Mailbox.
        // A value of 0 corresponds to feature disabled (no reactive protection) and
        // default value of ICC Limit should be the same value as the ICC Max value.
        //
        if (CpuPowerMgmtVrConfig->IccLimit[VrIndex] != 0) {
          MailboxData = (UINT32)CpuPowerMgmtVrConfig->IccLimit[VrIndex];
          MailboxCommand.InterfaceData  = 0;
          MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
          MailboxCommand.Fields.Param1  = MAILBOX_VR_SUBCMD_SVID_SET_FAST_VMODE_ICC_LIMIT;
          MailboxCommand.Fields.Param2  = (TempVrAddress & VR_ADDRESS_MASK);
          DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Read Command = 0x%08x\n", MailboxCommand.InterfaceData));
          DEBUG ((DEBUG_INFO, "(MAILBOX) IccLimit              = %d (1/4 A)\n", MailboxData));
          Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
          if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
            DEBUG ((DEBUG_ERROR, "VR: Error Writing IccLimit. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
          }
        }
      }

      ///
      /// VR Voltage Limit
      /// -Mailbox Voltage Limit defined as U16.3.13, Range 0-7.999V
      /// -Policy defined in mV, Range 0-7999mV
      /// -Adding half of divisor to dividend to account for rounding errors in fixed point arithmetic.
      ///
      if (CpuPowerMgmtVrConfig->VrVoltageLimit[VrIndex] != 0) {
        MailboxData =  (UINT32)((CpuPowerMgmtVrConfig->VrVoltageLimit[VrIndex] * (1 << 13) + 500) / 1000) & VR_VOLTAGE_LIMIT_MASK;
        MailboxCommand.InterfaceData = 0;
        MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
        MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_VOLTAGE_LIMIT;
        MailboxCommand.Fields.Param2 = (TempVrAddress & VR_ADDRESS_MASK);
        DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_VR_VOLTAGE_LIMIT_CMD\n"));
        DEBUG ((DEBUG_INFO, "(MAILBOX) VrVoltageLimit        = %d mV\n", CpuPowerMgmtVrConfig->VrVoltageLimit[VrIndex]));
        Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
        if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
          DEBUG ((DEBUG_ERROR, "VR: Error Writing VR Voltage Limit. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        }
      }

      if (CpuPowerMgmtVrConfig->TdcEnable[VrIndex] == 1) {
        ///
        /// VR TDC Settings
        /// -Mailbox TDC Current Limit defined as U15.12.3, Range 0-4095A
        ///    -Policy defined in 1/8 A increments
        /// -Time window mailbox format, in binary, is xxyyyyy, time window = (1+X/4)*pow(2,Y)
        ///    -Set to 1ms default (mailbox value = 0000000b)
        ///

        ///
        /// Ensure Time Window Value is within the supported range.
        ///
        ConvertedTimeWindow = 0;
        if (TdcTimeWindow[VrIndex] != 0) {
          IsTimeWindowInSeconds = (TdcTimeWindow[VrIndex] / 1000) >= 1;
          if (IsTimeWindowInSeconds) {
            ConvertedTimeWindow = GetConvertedTime ((TdcTimeWindow[VrIndex] / 1000), SecondsTimeWindowConvert);
          } else {
            ConvertedTimeWindow = GetConvertedTime (TdcTimeWindow[VrIndex], MilliSecondsTimeWindowConvert);
          }
        }
        DEBUG ((DEBUG_INFO, "(MAILBOX) ConvertedTimeWindow        = %xh \n", ConvertedTimeWindow));
        if (TdcCurrentLimit[VrIndex] != 0) {
          MailboxData = (UINT32)((TdcCurrentLimit[VrIndex] & VR_TDC_CURRENT_LIMIT_MASK) |
                                  ((CpuPowerMgmtVrConfig->TdcEnable[VrIndex] & BIT0) << VR_TDC_ENABLE_OFFSET) |
                                  ((ConvertedTimeWindow & VR_TDC_TIME_WINDOW_MASK) << VR_TDC_TIME_WINDOW_OFFSET) |
                                  ((CpuPowerMgmtVrConfig->TdcLock[VrIndex] & BIT0) << VR_TDC_LOCK_OFFSET));
          if (CpuPowerMgmtVrConfig->Irms[VrIndex] == 1) {
            DEBUG ((DEBUG_INFO, "(MAILBOX) Irms Enabled\n"));
            MailboxData |= (UINT32)((CpuPowerMgmtVrConfig->Irms[VrIndex]) << VR_TDC_IRMS_OFFSET);
          }
          MailboxCommand.InterfaceData = 0;
          MailboxCommand.Fields.Command = MAILBOX_VR_CMD_WRITE_VR_TDC_CONFIG;
          MailboxCommand.Fields.Param1 = (TempVrAddress & VR_ADDRESS_MASK);

          DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_VR_TDC_CONFIG_CMD\n"));
          DEBUG ((DEBUG_INFO, "(MAILBOX) TdcPowerLimit         = %d (1/8A)\n", TdcCurrentLimit[VrIndex]));
          Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
          if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
            DEBUG ((DEBUG_ERROR, "VR: Error Writing VR TDC Config. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
          }
        }
      }

      //
      //  To control settled time of Richtek VR which is enabled for VccSA to meet IMVP8 VR settled time.
      //
      VrSettledTimerRt (VrIndex);

      ///
      /// PS1 to PS0 Dynamic cutoff
      ///
      if (CpuPowerMgmtVrConfig->PS1toPS0DynamicCutoffEnable[VrIndex] == 1) {
        Status                        = EFI_SUCCESS;
        MailboxStatus                 = PCODE_MAILBOX_CC_SUCCESS;
        MailboxCommand.InterfaceData  = 0;
        MailboxCommand.Fields.Param2  = 0;
        PS1PS0MailboxData.Data        = 0;
        PS1PS0MailboxData.Fields.PS1PS0DynamicEnable = 1;
        MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
        MailboxCommand.Fields.Param1  = MAILBOX_VR_SUBCMD_SVID_SET_PS1_PS0_DYNAMIC_CUTOFF;
        MailboxCommand.Fields.Param2  = TempVrAddress;
        PS1PS0MailboxData.Fields.MValue = CpuPowerMgmtVrConfig->PS1toPS0MCoef[VrIndex];
        PS1PS0MailboxData.Fields.CValue = CpuPowerMgmtVrConfig->PS1toPS0CCoef[VrIndex];

        DEBUG ((DEBUG_INFO, "M value = %d, C value = %d\n", PS1PS0MailboxData.Fields.MValue, PS1PS0MailboxData.Fields.CValue ));

        //Set M and C value
        Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, PS1PS0MailboxData.Data, &MailboxStatus);
        if (EFI_ERROR (Status) || (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS)) {
          DEBUG ((DEBUG_ERROR, "Mailbox Command Write failed. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        }
      }

      ///
      /// PS2 to PS1 Dynamic cutoff
      ///
      if (CpuPowerMgmtVrConfig->PS2toPS1DynamicCutoffEnable[VrIndex] == 1) {
        Status                        = EFI_SUCCESS;
        MailboxStatus                 = PCODE_MAILBOX_CC_SUCCESS;
        MailboxCommand.InterfaceData  = 0;
        MailboxCommand.Fields.Param2  = 0;
        PS2PS1MailboxData.Data        = 0;
        PS2PS1MailboxData.Fields.PS2PS1DynamicEnable = 1;
        MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
        MailboxCommand.Fields.Param1  = MAILBOX_VR_SUBCMD_SVID_SET_PS2_PS1_DYNAMIC_CUTOFF;
        MailboxCommand.Fields.Param2  = TempVrAddress;
        PS2PS1MailboxData.Fields.MValue = CpuPowerMgmtVrConfig->PS2toPS1MCoef[VrIndex];
        PS2PS1MailboxData.Fields.CValue = CpuPowerMgmtVrConfig->PS2toPS1CCoef[VrIndex];

        DEBUG ((DEBUG_INFO, "M value = %d, C value = %d\n", PS2PS1MailboxData.Fields.MValue, PS2PS1MailboxData.Fields.CValue ));

        //Set M and C value
        Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, PS2PS1MailboxData.Data, &MailboxStatus);
        if (EFI_ERROR (Status) || (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS)) {
          DEBUG ((DEBUG_ERROR, "Mailbox Command Write failed. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        }
      }

      ///
      /// VCCIN Demotion
      /// VccInDemotionEnable == 0: Disable, 1: Enable (default PCODE setting), 2: Enable with user config
      ///
      if (IsVccInDemotionSupported (VrIndex) && CpuPowerMgmtVrConfig->VccInDemotionEnable[VrIndex] != 1) {
        MailboxStatus                 = PCODE_MAILBOX_CC_SUCCESS;
        MailboxCommand.InterfaceData  = 0;
        MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
        MailboxCommand.Fields.Param1  = MAILBOX_VR_SUBCMD_SVID_SET_QUIESCENT_POWER_AND_PLATFORM_CAP;
        MailboxCommand.Fields.Param2  = TempVrAddress;

        QuiescentAndPlatformCap.Data          = 0;
        //
        // Enable is not a bit to enable VCCIN Demotion, but a bit used by PCODE to know the other fields are valid.
        // BIOS should always set this bit.
        //
        QuiescentAndPlatformCap.Fields.Enable = 1;
        if (CpuPowerMgmtVrConfig->VccInDemotionEnable[VrIndex] == 0) {
          //
          // Disable VCCIN Demotion: setting 0 to capacitance.
          //
          QuiescentAndPlatformCap.Fields.Capacitance = 0;
        } else {
          ASSERT (CpuPowerMgmtVrConfig->VccInDemotionEnable[VrIndex] == 2);
          //
          // Enable VCCIN Demotion using user config.
          //
          QuiescentAndPlatformCap.Fields.Capacitance    = CpuPowerMgmtVrConfig->VccInDemotionCapacitanceInUf[VrIndex];
          QuiescentAndPlatformCap.Fields.QuiescentPower = CpuPowerMgmtVrConfig->VccInDemotionQuiescentPowerInMw[VrIndex];
        }
        DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = MAILBOX_VR_CMD_SVID_VR_HANDLER\n"));
        DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Sub-Command = MAILBOX_VR_SUBCMD_SVID_SET_QUIESCENT_POWER_AND_PLATFORM_CAP\n"));
        DEBUG ((
          DEBUG_INFO, "(MAILBOX) Enable = %d, Capacitance = %d uF, QuiescentPower = %d mW\n",
          QuiescentAndPlatformCap.Fields.Enable, QuiescentAndPlatformCap.Fields.Capacitance,
          QuiescentAndPlatformCap.Fields.QuiescentPower
          ));

        Status = MailboxWrite (MAILBOX_TYPE_VR_MSR, MailboxCommand.InterfaceData, QuiescentAndPlatformCap.Data, &MailboxStatus);
        if (EFI_ERROR (Status) || (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS)) {
          DEBUG ((DEBUG_ERROR, "Mailbox Command Write failed. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        }

        DEBUG_CODE (
          QuiescentAndPlatformCap.Data = 0;
          MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_GET_QUIESCENT_POWER_AND_PLATFORM_CAP;
          Status = MailboxRead (MAILBOX_TYPE_VR_MSR, MailboxCommand.InterfaceData, &QuiescentAndPlatformCap.Data, &MailboxStatus);
          if (EFI_ERROR (Status) || (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS)) {
            DEBUG ((DEBUG_ERROR, "Mailbox Command Read failed. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
          }
          DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Read Command = MAILBOX_VR_CMD_SVID_VR_HANDLER\n"));
          DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Read Sub-Command = MAILBOX_VR_SUBCMD_SVID_GET_QUIESCENT_POWER_AND_PLATFORM_CAP\n"));
          DEBUG ((DEBUG_INFO, "(MAILBOX) Enable = %d, Capacitance = %d uF, QuiescentPower = %d mW\n",
            QuiescentAndPlatformCap.Fields.Enable, QuiescentAndPlatformCap.Fields.Capacitance,
            QuiescentAndPlatformCap.Fields.QuiescentPower
          ));
        );
      }
    }

    ///
    /// Acoustic Noise Mitigation
    ///
    if (CpuPowerMgmtVrConfig->AcousticNoiseMitigation) {

      ///
      /// PreWake, Ramp Up and Ramp Down times programming for Acoustic Noise Mitigation.
      ///
      MailboxCommand.InterfaceData  = 0;
      MailboxCommand.Fields.Command = MAILBOX_VR_CMD_WRITE_ACOUSTIC_MITIGATION_RANGE;
      MailboxData = (CpuPowerMgmtVrConfig->PreWake & NOISE_MIGITATION_RANGE_MASK)
                    | ((CpuPowerMgmtVrConfig->RampUp & NOISE_MIGITATION_RANGE_MASK) << RAMP_UP_OFFSET)
                    | ((CpuPowerMgmtVrConfig->RampDown & NOISE_MIGITATION_RANGE_MASK) << RAMP_DOWN_OFFSET);
      DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = DPA - WRITE_ACOUSTIC_MITIGATION_RANGE\n"));
      DEBUG ((DEBUG_INFO, "(MAILBOX) PreWake               = %d ms\n",CpuPowerMgmtVrConfig->PreWake));
      DEBUG ((DEBUG_INFO, "(MAILBOX) RampUp                = %d ms\n",CpuPowerMgmtVrConfig->RampUp));
      DEBUG ((DEBUG_INFO, "(MAILBOX) RampDown              = %d ms\n",CpuPowerMgmtVrConfig->RampDown));
      Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
      if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
        DEBUG ((DEBUG_ERROR, "Error Writing Acoustic Migitation Range. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
      }

      ///
      /// Set Fast and Slow Slew Rate for Deep Package C States.
      ///
      MailboxCommand.InterfaceData = 0;
      MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
      MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_DISABLE_FAST_PKGC_RAMP;
      MailboxCommand.Fields.Param2 = (TempVrAddress & VR_ADDRESS_MASK);
      MailboxData = CpuPowerMgmtVrConfig->FastPkgCRampDisable[VrIndex];
      DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_SVID_SET_DISABLE_FAST_PKGC_RAMP_CMD\n"));
      Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
      if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
        DEBUG ((DEBUG_ERROR, "VR: Error Writing disable Fast Deep Package C States for VrIndex = %x. EFI_STATUS = %r, Mailbox Status = %X\n", VrIndex , Status, MailboxStatus));
      }

      MailboxCommand.InterfaceData = 0;
      MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
      MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_VR_SLEW_RATE;
      MailboxCommand.Fields.Param2 = (TempVrAddress & VR_ADDRESS_MASK);
      MailboxData = CpuPowerMgmtVrConfig->SlowSlewRate[VrIndex];
      DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_SVID_SET_VR_SLEW_RATE_CMD\n"));
      DEBUG((DEBUG_INFO, "(MAILBOX) SlowSlewRate[%x] = %d (0: Fast/2</b>; 1: Fast/4; 2: Fast/8; 3: Fast/16)\n", VrIndex, CpuPowerMgmtVrConfig->SlowSlewRate[VrIndex]));
      Status = MailboxWrite (MailboxType, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
      if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
        DEBUG ((DEBUG_ERROR, "VR: Error Writing Slow Slew Rate for VrIndex = %x. EFI_STATUS = %r, Mailbox Status = %X\n", VrIndex , Status, MailboxStatus));
      }
    }
  }
  //
  //  To control settled time of Richtek VR which is enabled for VccSA to meet IMVP8 VR settled time.
  //
  VrSettledTimerRt (VrIndex);

  ///
  /// Update Min and Max value for CPU Data HOB
  ///
  CpuDataHob.MaxVrIndex            = VrDomainTopology.MaxVrIndex;
  CpuDataHob.MinVrIndex            = VrDomainTopology.MinVrIndex;

  DEBUG ((DEBUG_INFO, " VR SvidEnabled = 0x%x\n", CpuDataHob.SvidEnabled));
  ///
  /// Build CPU Data HOB
  ///

  Hob = BuildGuidDataHob (
          &gCpuDataHobGuid,
          &CpuDataHob,
          (UINTN) sizeof (CPU_DATA_HOB)
          );
  ASSERT (Hob != NULL);

  ///
  /// Print VR Topology data
  ///
  DEBUG ((DEBUG_INFO, "VR: VR Topology data = 0x%x\n", VrDomainTopology));
  DEBUG ((DEBUG_INFO, "    VR Type 0 = SVID, VR Type 1 = non-SVID\n"));

  ///
  /// RFI Mitigation
  ///
  UpdateFivrRfiSettings (CpuPowerMgmtVrConfig);

}
