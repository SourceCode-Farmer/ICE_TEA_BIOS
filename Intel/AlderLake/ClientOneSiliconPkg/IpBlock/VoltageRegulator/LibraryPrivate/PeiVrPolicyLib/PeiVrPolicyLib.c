/** @file
  This file load VR deafults and config block.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/PeiServicesLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/SiConfigBlockLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/CpuPolicyLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiVrLib.h>
#include <Library/PeiVrDomainLib.h>
#include <Library/PeiCpuInitFruLib.h>
#include <CpuRegs.h>

/**
  Print CPU_POWER_MGMT_VR_CONFIG and serial out.

  @param[in] SiPolicyPpi  Instance of SI_POLICY_PPI
**/
VOID
CpuPowerMgmtVrConfigPrint (
  IN  SI_POLICY_PPI       *SiPolicyPpi
  )
{
  EFI_STATUS  Status;
  CPU_POWER_MGMT_VR_CONFIG  *CpuPowerMgmtVrConfig;
  UINT16                            MaxNumVrs;

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtVrConfigGuid, (VOID *) &CpuPowerMgmtVrConfig);
  ASSERT_EFI_ERROR(Status);

  ///
  /// Define Maximum Number of Voltage Regulator Domains.
  ///
  MaxNumVrs = GetMaxNumVrs ();

  UINT32 Index = 0;
  DEBUG ((DEBUG_INFO, "------------------ CPU Power Mgmt VR Config ------------------\n"));

  for (Index = 0; Index < MaxNumVrs; Index++) {
    DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VrConfigEnable[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->VrConfigEnable[Index]));
    if (CpuPowerMgmtVrConfig->VrConfigEnable[Index] == 1) {
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : TdcCurrentLimit[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->TdcCurrentLimit[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : AcLoadline[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->AcLoadline[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : DcLoadline[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->DcLoadline[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Psi1Threshold[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Psi1Threshold[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Psi2Threshold[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Psi2Threshold[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Psi3Threshold[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Psi3Threshold[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Psi3Enable[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Psi3Enable[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Psi4Enable[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Psi4Enable[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : ImonSlope[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->ImonSlope[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : ImonOffset[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->ImonOffset[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : IccMax[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->IccMax[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : IccLimit[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->IccLimit[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VrVoltageLimit[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->VrVoltageLimit[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : TdcEnable[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->TdcEnable[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : TdcTimeWindow[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->TdcTimeWindow1[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : TdcLock[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->TdcLock[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : Irms[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->Irms[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VccInDemotionEnable[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->VccInDemotionEnable[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VccInDemotionCapacitanceInUf[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->VccInDemotionCapacitanceInUf[Index]));
      DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VccInDemotionQuiescentPowerInMw[%X] : 0x%X\n", Index, CpuPowerMgmtVrConfig->VccInDemotionQuiescentPowerInMw[Index]));
    }
    DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG:: FastPkgCRampDisable for Index = %d : 0x%X\n", Index, CpuPowerMgmtVrConfig->FastPkgCRampDisable[Index]));
    DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG:: SlowSlewRate for Index = %d : 0x%X\n", Index, CpuPowerMgmtVrConfig->SlowSlewRate[Index]));
  }
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : PsysSlope : 0x%X\n", CpuPowerMgmtVrConfig->PsysSlope));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : PsysOffset : 0x%X\n", CpuPowerMgmtVrConfig->PsysOffset1));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : AcousticNoiseMitigation : 0x%X\n", CpuPowerMgmtVrConfig->AcousticNoiseMitigation));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : FivrRfiFrequency : 0x%X\n", CpuPowerMgmtVrConfig->FivrRfiFrequency));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : FivrSpreadSpectrum : 0x%X\n", CpuPowerMgmtVrConfig->FivrSpreadSpectrum));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : EnableMinVoltageOverride : 0x%X\n", CpuPowerMgmtVrConfig->EnableMinVoltageOverride));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : PreWake : 0x%X\n", CpuPowerMgmtVrConfig->PreWake));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : RampUp : 0x%X\n", CpuPowerMgmtVrConfig->RampUp));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : RampDown : 0x%X\n", CpuPowerMgmtVrConfig->RampDown));
  if (CpuPowerMgmtVrConfig->EnableMinVoltageOverride == 1) {
    DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : MinVoltageRuntime : 0x%X\n", CpuPowerMgmtVrConfig->MinVoltageRuntime));
    DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : MinVoltageC8 : 0x%X\n", CpuPowerMgmtVrConfig->MinVoltageC8));
  }
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VccInAuxImonIccImax : 0x%X\n", CpuPowerMgmtVrConfig->VccInAuxImonIccImax));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VccInAuxImonSlope : 0x%X\n", CpuPowerMgmtVrConfig->VccInAuxImonSlope));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VccInAuxImonOffset : 0x%X\n", CpuPowerMgmtVrConfig->VccInAuxImonOffset));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : DlvrSpreadSpectrumPercentage : 0x%X\n", CpuPowerMgmtVrConfig->DlvrSpreadSpectrumPercentage));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : DlvrRfiFrequency : 0x%X\n", CpuPowerMgmtVrConfig->DlvrRfiFrequency));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : DlvrRfiEnable : 0x%X\n", CpuPowerMgmtVrConfig->DlvrRfiEnable));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : RfiMitigation : 0x%X\n", CpuPowerMgmtVrConfig->RfiMitigation));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VrPowerDeliveryDesign : 0x%X\n", CpuPowerMgmtVrConfig->VrPowerDeliveryDesign));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : EnableVsysCritical : 0x%X\n", CpuPowerMgmtVrConfig->EnableVsysCritical));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VsysFullScale : 0x%X\n", CpuPowerMgmtVrConfig->VsysFullScale));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VsysCriticalThreshold : 0x%X\n", CpuPowerMgmtVrConfig->VsysCriticalThreshold));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VsysAssertionDeglitchMantissa : 0x%X\n", CpuPowerMgmtVrConfig->VsysAssertionDeglitchMantissa));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VsysAssertionDeglitchExponent : 0x%X\n", CpuPowerMgmtVrConfig->VsysAssertionDeglitchExponent));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VsysDeassertionDeglitchMantissa : 0x%X\n", CpuPowerMgmtVrConfig->VsysDeassertionDeglitchMantissa));
  DEBUG ((DEBUG_INFO, " CPU_POWER_MGMT_VR_CONFIG : VsysDeassertionDeglitchExponent : 0x%X\n", CpuPowerMgmtVrConfig->VrPowerDeliveryDesign));

}
/**
  Load Config block default

  @param[IN,OUT] ConfigBlockPointer         Pointer to config block

**/
VOID
LoadCpuPowerMgmtVrConfigDefault (
  IN OUT VOID          *ConfigBlockPointer
  )
{
  UINTN                           Index;
  CPU_POWER_MGMT_VR_CONFIG        *CpuPowerMgmtVrConfig;
  UINT16                          MaxNumVrs;

  CpuPowerMgmtVrConfig = ConfigBlockPointer;
  Index                = 0;

  DEBUG ((DEBUG_INFO, "CpuPowerMgmtVrConfig->Header.GuidHob.Name = %g\n", &CpuPowerMgmtVrConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "CpuPowerMgmtVrConfig->Header.GuidHob.Header.HobLength = 0x%x\n", CpuPowerMgmtVrConfig->Header.GuidHob.Header.HobLength));
  ///
  /// Define Maximum Number of Voltage Regulator Domains.
  ///
  MaxNumVrs = GetMaxNumVrs ();

  /*************************************
    CPU VOLTAGE REGULATOR configuration
  **************************************/
  for (Index = 0; Index < MaxNumVrs; Index++) {
    CpuPowerMgmtVrConfig->VrConfigEnable[Index] = CPU_FEATURE_ENABLE;
    CpuPowerMgmtVrConfig->Psi3Enable[Index] = CPU_FEATURE_ENABLE;
    CpuPowerMgmtVrConfig->Psi4Enable[Index] = CPU_FEATURE_ENABLE;
    CpuPowerMgmtVrConfig->PS1toPS0DynamicCutoffEnable[Index] = CPU_FEATURE_DISABLE;
    CpuPowerMgmtVrConfig->PS2toPS1DynamicCutoffEnable[Index] = CPU_FEATURE_DISABLE;
    CpuPowerMgmtVrConfig->PS1toPS0MCoef[Index] = 0x64;
    CpuPowerMgmtVrConfig->PS1toPS0CCoef[Index] = 0x7D0;
    CpuPowerMgmtVrConfig->PS2toPS1MCoef[Index] = 0x64;
    CpuPowerMgmtVrConfig->PS2toPS1CCoef[Index] = 0x1F4;
    //
    // 0: Disable, 1: Enable, 2: User Config
    //
    CpuPowerMgmtVrConfig->VccInDemotionEnable[Index] = 1;
    CpuPowerMgmtVrConfig->TdcTimeWindow1[Index] = 1;
    CpuPowerMgmtVrConfig->TdcEnable[Index] = 1;
    CpuPowerMgmtVrConfig->Psi1Threshold[Index] = 0x50;
    CpuPowerMgmtVrConfig->Psi2Threshold[Index] = 0x14;
    CpuPowerMgmtVrConfig->Psi3Threshold[Index] = 0x4;
  }
  ///
  /// Load CPU power management Vr Config block default for specific generation.
  ///
  PeiCpuLoadPowerMgmtVrConfigDefaultFru (CpuPowerMgmtVrConfig);
}

STATIC COMPONENT_BLOCK_ENTRY mVrIpBlock = {
  &gCpuPowerMgmtVrConfigGuid,
  sizeof (CPU_POWER_MGMT_VR_CONFIG),
  CPU_POWER_MGMT_VR_CONFIG_REVISION,
  LoadCpuPowerMgmtVrConfigDefault
};

/**
  Get VR config block total size.

  @retval Size of config block
**/
UINT16
GetCpuPowerMgmtVrConfigBlockTotalSize (
  VOID
  )
{
  return mVrIpBlock.Size;
}

/**
  Add VR ConfigBlock.

  @param[in] ConfigBlockTableAddress The pointer to config block table

  @retval EFI_SUCCESS                The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES       Insufficient resources to create buffer
**/
EFI_STATUS
AddCpuPowerMgmtVrConfigBlock (
  IN VOID *ConfigBlockTableAddress
  )
{
  EFI_STATUS Status;

  Status = AddComponentConfigBlocks (ConfigBlockTableAddress, &mVrIpBlock, 1);
  return Status;
}
