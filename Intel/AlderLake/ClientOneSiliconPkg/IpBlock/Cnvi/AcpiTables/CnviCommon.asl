/**@file
  CNVi Common ACPI definitions

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <PcieRegs.h>
#include <Register/CnviRegs.h>

Scope (\_SB.PC00) {
  Device(CNVW)
  {
    Name(_ADR, 0x00140003)

    // Reset Type
    //   0: Core Reset
    //   1: Product Reset
    Name (RSTT, 0)

    // Last _PRR Status
    //   0: Non-PLDR executed [Default]
    //   1: Core PLDR Completed successfully
    //   2: Product PLDR Completed successfully
    //   3: Previous PLDR Not Completed
    //   4: Product PLDR timeout
    Name (PRRS, 0)

    //
    // Define a Memory Region that will allow access to the CNVi WiFi PCI Configuration Space
    //
    OperationRegion(CWAR, SystemMemory, Add(\_SB.PC00.GPCB(), 0xA3000), 0x100)
    Field(CWAR, WordAcc, NoLock, Preserve) {
      VDID, 32, // 0x00, VID DID
          ,  1,
      WMSE,  1, // MSE
      WBME,  1, // BME
      Offset(R_PCI_BAR0_OFFSET),
      WBR0, 64, // BAR0
      Offset(R_CNVI_CFG_WIFI_GIO_DEV_CAP),
          , 28,
      WFLR,  1, // Function Level Reset Capable
      Offset(R_CNVI_CFG_WIFI_GIO_DEV_CTRL),
          , 15,
      WIFR,  1, // Init Function Level Reset
      Offset(R_CNVI_CFG_WIFI_PMCSR),
      WPMS, 32,
    }
    Field(CWAR, ByteAcc, NoLock, Preserve) {
      Offset(R_CNVI_CFG_WIFI_PMCSR),
          ,8,
      PMEE,1,
      ,6,
      PMES,1
    }
    Method(_S0W, 0x0, NotSerialized)
    {
      Return (0x3)
    }
    Method(_PRW, 0)
    {
//[-start-210616-Kebin000012-modify]//
#ifdef LCFC_SUPPORT 
      Return (GPRW(0x6D, 3)) // can wakeup from S3 state
#else
      Return (GPRW(0x6D, 4)) // can wakeup from S4 state
#endif 
//[-end-210616-Kebin000012-modify]//
    }

    Method(GPEH, 0) {
      If(LEqual(^VDID,0xFFFFFFFF))
      {
        Return
      }
      If(LEqual(PMES,1)) {
        Notify(\_SB.PC00.CNVW, 0x02)
      }
    }

    Method(_PS0, 0, Serialized) {
      If (LNotEqual (VDID, 0xFFFFFFFF)) {
        // S0i2/3 dynamic switching for CNVi WiFi (Arg0 = 2), active (Arg1 = 1)
        \_SB.S023(2, 1)
      }
    }

    Method(_PS3, 0, Serialized) {
      If (LNotEqual (VDID, 0xFFFFFFFF)) {
        //
        // Check if:
        //  CNVI.PCR.STAT[5:0](CNVI_STATE) = 0xC
        //  CNVI.PCR.STAT[6](CNVI_STATE_VAL) = 1
        //
        Store (PCRR (\PCNV, R_CNVI_PCR_STAT), Local0)
        If (LEqual(And(Local0, 0x7F), 0x4C)) {
          // S0i2/3 dynamic switching for CNVi WiFi (Arg0 = 2), inactive (Arg1 = 0)
          \_SB.S023(2, 0)
        }
      }
    }

    Method(_DSW, 3) {}

    Name(_PRR, Package(){WRST})
  }

  //
  // CNVi is present
  //
  Method(CNIP)
  {
    If (LOr (LNotEqual (\_SB.PC00.CNVW.VDID, 0xFFFFFFFF), LEqual (CRFP, 1))) {
      Return (0x01)
    } Else {
      Return (0x00)
    }
  }

  //
  // Set BT_EN
  //
  Method(SBTE, 0x1, Serialized)
  {
    //
    // Arg0 - Value to BT_EN
    //
    If (LNotEqual (\_SB.GBTP (), 0)) {
      Store(\_SB.GBTP (), Local0)
      \_SB.SGOV (Local0, Arg0)

      // S0i2/3 dynamic switching for CNVi BT
      \_SB.S023 (3, Arg0)
    }
  }

  //
  // Get BT_EN value
  //
  Method(GBTE, 0)
  {
    If (LNotEqual (\_SB.GBTP (), 0)) {
      Store(\_SB.GBTP (), Local0)
      Return (\_SB.GGOV (Local0))
    } Else {
      Return (0)
    }
  }

  //
  // Update BT Audio Offload setting
  //
  Method (AOLX)
  {
    // BT Audio Offload data structure
    Name (AODS, Package () {
      0,    // Revision
      0x12, // DomainType, 0x12:Bluetooth Core
      0     // Audio Offload selection, 0: Disabled, 1:Enabled
      })

    If (LEqual (CBTA, 1)) {
      Store(1, Index (AODS, 2))
    }
    Return (AODS)
  }
}

//
// AOLD (Audio Offload)
//   Arguments:
//     None
//   Return Value:
//     Revision
//     DomainType
//     Audio Offload selection, default value is Disabled
//
If (LAnd (\_SB.PC00.CNIP (), LEqual (CBTC, 1))) {
  If (LOr (LEqual (PCHS, PCH_LP), LOr (LEqual (PCHS, PCH_P), LEqual (PCHS, PCH_M)))) {
    Scope(\_SB.PC00.XHCI.RHUB.HS10)
    {
      Method(AOLD)
      {
        Return (AOLX ())
      }
    }
  } ElseIf (LOr (LEqual (PCHS, PCH_H), LEqual (PCHS, PCH_S))) {
    Scope(\_SB.PC00.XHCI.RHUB.HS14)
    {
      Method(AOLD)
      {
        Return (AOLX ())
      }
    }
  } ElseIf (LEqual (PCHS, PCH_N)) {
    Scope(\_SB.PC00.XHCI.RHUB.HS08)
    {
      Method(AOLD)
      {
        Return (AOLX ())
      }
    }
  }
}


