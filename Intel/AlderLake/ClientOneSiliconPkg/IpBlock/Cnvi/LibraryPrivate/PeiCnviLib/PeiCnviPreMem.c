/** @file
  CNVi initialization in pre-mem phase.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/SiPolicy.h>
#include <Library/PciSegmentLib.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/CnviLib.h>
#include <PcieRegs.h>
#include <Register/SaRegsHostBridge.h>

/**
  Configure CNVi device on Policy callback.

  @param[in] SiPreMemPolicyPpi         The SI PreMem Policy PPI instance
**/
VOID
CnviInitPreMem (
  IN  SI_PREMEM_POLICY_PPI             *SiPreMemPolicyPpi
  )
{
  EFI_STATUS          Status;
  CNVI_PREMEM_CONFIG  *CnviPreMemConfig;
  UINT64              McD0BaseAddress;
  UINT64              MchBar;

  DEBUG ((DEBUG_INFO, "%a() - Start\n", __FUNCTION__));

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCnviPreMemConfigGuid, (VOID *) &CnviPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  if (CnviPreMemConfig->DdrRfim == 0) {
    McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
    MchBar  = PciSegmentRead32 (McD0BaseAddress + R_SA_MCHBAR) & 0xFFFFFFF0;
    MchBar += LShiftU64 ((UINT64) PciSegmentRead32 (McD0BaseAddress + (R_SA_MCHBAR + 4)), 32);
    //
    // Program Mchbar 0x5A40 (DDR_DVFS_RFI_CONFIG_PCU_REG) RFI_DISABLE bit
    //
    MmioOr32 ((UINTN) MchBar + 0x5A40, BIT0);
  }
}
