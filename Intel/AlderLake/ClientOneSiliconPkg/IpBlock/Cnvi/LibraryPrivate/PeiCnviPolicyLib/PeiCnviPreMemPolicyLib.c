/** @file
  This file provides services for CNVi PEI pre-mem policy function

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Library/SiConfigBlockLib.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <CnviConfig.h>

/**
  Print CNVI_PREMEM_CONFIG settings.

  @param[in] SiPreMemPolicyPpi  Instance of SI_PREMEM_POLICY_PPI
**/
VOID
CnviPreMemPrintConfig (
  IN CONST SI_PREMEM_POLICY_PPI *SiPreMemPolicyPpi
  )
{
  EFI_STATUS         Status;
  CNVI_PREMEM_CONFIG *CnviPreMemConfig;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCnviPreMemConfigGuid, (VOID *) &CnviPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "------------------ CNVi PreMem Config ------------------\n"));
  DEBUG ((DEBUG_INFO, "DDR RFI Mitigation = %x\n", CnviPreMemConfig->DdrRfim));
}

/**
  Load Config block default

  @param[in] ConfigBlockPointer  Pointer to config block
**/
VOID
CnviPreMemLoadConfigDefault (
  IN VOID *ConfigBlockPointer
  )
{
  CNVI_PREMEM_CONFIG *CnviPreMemConfig;

  CnviPreMemConfig = ConfigBlockPointer;

  DEBUG ((DEBUG_INFO, "CnviPreMemConfig->Header.GuidHob.Name = %g\n", &CnviPreMemConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "CnviPreMemConfig->Header.GuidHob.Header.HobLength = 0x%x\n", CnviPreMemConfig->Header.GuidHob.Header.HobLength));

  CnviPreMemConfig->DdrRfim = TRUE;  // DDR RFI Mitigation
}

STATIC COMPONENT_BLOCK_ENTRY mCnviPreMemBlock = {
  &gCnviPreMemConfigGuid,
  sizeof (CNVI_PREMEM_CONFIG),
  CNVI_PREMEM_CONFIG_REVISION,
  CnviPreMemLoadConfigDefault
};

/**
  Get CNVi PreMem config block table size.

  @retval Size of config block
**/
UINT16
CnviPreMemGetConfigBlockTotalSize (
  VOID
  )
{
  return mCnviPreMemBlock.Size;
}

/**
  Add CNVi PreMem ConfigBlock.

  @param[in] ConfigBlockTableAddress The pointer to config block table

  @retval EFI_SUCCESS                The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES       Insufficient resources to create buffer
**/
EFI_STATUS
CnviPreMemAddConfigBlock (
  IN VOID *ConfigBlockTableAddress
  )
{
  EFI_STATUS Status;

  Status = AddComponentConfigBlocks (ConfigBlockTableAddress, &mCnviPreMemBlock, 1);
  return Status;
}
