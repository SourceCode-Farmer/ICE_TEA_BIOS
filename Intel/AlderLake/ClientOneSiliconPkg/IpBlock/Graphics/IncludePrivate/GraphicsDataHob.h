/** @file
  The GUID definition for GraphicsDataHob

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _GFX_DATA_HOB_H_
#define _GFX_DATA_HOB_H_

#include <Base.h>

extern EFI_GUID gGraphicsDataHobGuid;
#pragma pack (push,1)

/**
  The GFX_DATA_HOB block describes the expected configuration of the CpuPcie controllers
**/
typedef struct {
  ///
  /// These members describe the configuration of each CPU PCIe root port.
  ///
  EFI_HOB_GUID_TYPE           EfiHobGuidType;                           ///< Offset 0 - 23: GUID Hob type structure for gGraphicsDataHobGuid
  UINT8                       PrimaryDisplayDetection;                  ///< Primary Display - default is IGD. [IGD = 0; PEG = 1; PCIE = 2]
  BOOLEAN                     ExternalGfxScanEnable;                    ///< External Graphics Card scan policy value from BIOS setup. [1 = Enable, 0 = Disable]
  UINT8                       ExtGfxCardPresentOnPegRpBitMap;           ///< External Graphics Card found on CPU PCIE RP bit map. Where each BIT represent corresponding CPU PCIE RP Index.
  UINT8                       ExtGfxCardOnPegRpAsImmediateChildBitMap;  ///< External Graphics Card found on CPU PCIE RP as Immediate Child Device bit map. Where each BIT represent corresponding CPU PCIE RP Index.
  UINT32                      ExtGfxCardPresentOnPcieRpBitMap;          ///< External Graphics Card found on PCH PCIE RP bit map. Where each BIT represent corresponding PCH PCIE RP Index.
  UINT32                      ExtGfxCardOnPcieRpAsImmediateChildBitMap; ///< External Graphics Card found on PCH PCIE RP as Immediate Child Device bit map. Where each BIT represent corresponding PCH PCIE RP Index.
  UINT8                       NumberOfGfxControllerFound;               ///< Number of Graphics Device/Controller found including in IGD, PCIE RP and PEG RP.
  UINT8                       Reserved[2];                              ///< To allign 32 bit boundary.
  UINT8                       LidStatus;                                ///< LFP Display Lid Status (LID_STATUS enum).
  UINT32                      GraphicsConfigPtr;                        ///< Graphics Configuration Ptr.
  UINT32                      VbtSize;                                  ///< Intel Graphics VBT (Video BIOS Table) Size.
} GFX_DATA_HOB;
#pragma pack (pop)
#endif
