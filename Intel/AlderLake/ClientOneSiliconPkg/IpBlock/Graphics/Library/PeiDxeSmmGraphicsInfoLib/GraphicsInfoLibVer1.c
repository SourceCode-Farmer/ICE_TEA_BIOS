/** @file
  Source file for common Graphics Info Lib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/GraphicsInfoLib.h>
#include <Register/IgdRegs.h>
#include <Library/TimerLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Base.h>
#include <Library/PeiGraphicsInitLib.h>
#include <Library/GraphicsInfoFruLib.h>

/**
  This function will check if Bus Initiator and Memory access on 0:2:0 is enabled or not

  @retval TRUE          IGD Bus Initiator Access and Memory Space access is Enabled
  @retval FALSE         IGD Bus Initiator Access and Memory Space access is Disable
**/
BOOLEAN
IgfxCmdRegEnabled (
  VOID
)
{
  UINT16  IgfxCmdRegValue;

  IgfxCmdRegValue = (PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0) + PCI_COMMAND_OFFSET));
  if ((IgfxCmdRegValue & EFI_PCI_COMMAND_BUS_MASTER) && (IgfxCmdRegValue & EFI_PCI_COMMAND_MEMORY_SPACE)) {
    return TRUE;
  }
  return FALSE;
}

/**
  GetIgdBusNumber: Get IGD Bus Number

  @retval PCI bus number for IGD
**/
UINT8
GetIgdBusNumber (
  VOID
  )
{
  return (UINT8) IGD_BUS_NUM;
}

/**
  GetIgdDevNumber: Get IGD Dev Number

  @retval PCI dev number for IGD
**/
UINT8
GetIgdDevNumber (
  VOID
  )
{
  return (UINT8) IGD_DEV_NUM;
}

/**
  GetIgdFunNumber: Get IGD Fun Number

  @retval PCI fun number for IGD
**/
UINT8
GetIgdFuncNumber (
  VOID
  )
{
  return (UINT8) IGD_FUN_NUM;
}

/**
  GetGttMmAdrOffset: Get GTTMMADR Offset value

  @retval PCI Offset for GTTMMADR
**/
UINT8
GetGttMmAdrOffset (
  VOID
  )
{
  return (UINT8) R_SA_IGD_GTTMMADR;
}

/**
  GetGmAdrOffset: Get GMADR Offset value

  @retval PCI Offset for GMADR
**/
UINT8
GetGmAdrOffset (
  VOID
  )
{
  return (UINT8) R_SA_IGD_GMADR;
}

/**
  GetGmAdrOffset: Get graphics aperture Offset value

  @retval PCI Offset for graphics aperture BAR
**/
UINT8
GetIgfxApertureOffset (
  VOID
  )
{
  return (UINT8) R_SA_IGD_GMADR;
}

/**
  GetIgdMsacOffset: Get IGD MSAC Offset value

  @retval PCI Offset for IGD Aperture size.
**/
UINT8
GetIgdMsacOffset (
  VOID
  )
{
  return (UINT8) R_SA_IGD_MSAC_OFFSET;
}

/**
  GetIgdDssmRefClockFreqValue: Read DSSM (Display Strap State) register to get CD Clock reference clock frequence value

  @param[in] GttMmAdr         IGD MMIO Base address value

  @retval IGD CD Clock Reference clock frequency value
**/
UINT32
GetIgdDssmRefClockFreqValue (
  IN  UINTN                GttMmAdr
  )
{
  return (UINT32) ((MmioRead32 ((UINTN) (GttMmAdr + R_SA_GTTMMADR_DSSM_OFFSET)) & B_SA_GTTMMADR_DSSM_REFERENCE_FREQ_MASK) >> B_SA_GTTMMADR_DSSM_REFERENCE_FREQ_OFFSET) ;
}

/**
  "Poll Status" for GT Readiness

  @param[in] Base            - Base address of MMIO
  @param[in] Offset          - MMIO Offset
  @param[in] Mask            - Mask
  @param[in] Result          - Value to wait for

  @retval EFI_SUCCESS          Wait Bit Successfully set
  @retval EFI_TIMEOUT          Timed out
**/
EFI_STATUS
PollGtReady (
  IN       UINT64                       Base,
  IN       UINT32                       Offset,
  IN       UINT32                       Mask,
  IN       UINT32                       Result
  )
{
  UINT32  GtStatus;
  UINT32  StallCount;

  StallCount = 0;

  ///
  /// Register read
  ///
  GtStatus = MmioRead32 ((UINTN) Base + Offset);

  while (((GtStatus & Mask) != Result) && (StallCount < GT_WAIT_TIMEOUT)) {
    ///
    /// 10 microSec wait
    ///
    MicroSecondDelay (10);
    StallCount += 10;

    GtStatus = MmioRead32 ((UINTN) Base + Offset);
  }

  if (StallCount < GT_WAIT_TIMEOUT) {
    return EFI_SUCCESS;
  } else {
    if (!IsSimicsEnvironment()) {
      ASSERT ((StallCount < GT_WAIT_TIMEOUT));
    }
    return EFI_TIMEOUT;
  }
}

/**
  This function will check if Intel Graphics is Enabled or Supported

  @retval TRUE          Graphics Enabled or Supported
  @retval FALSE         Graphics not Enabled or not Supported
**/
BOOLEAN
IsIGfxSupported (
  VOID
)
{
  UINT64                  McD2BaseAddress;

  McD2BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);

  if (PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_WARN, "iGFX is unsupported or disabled!\n"));
    return FALSE;
  }

  return TRUE;
}

/**
  This function will get the Intel Graphics Memory Stolen Size in MB

  @retval GsmSize          Graphics Memory Stolen Size
**/
UINT32
EFIAPI
GetIgdGmsSizeInMb (
  VOID
)
{
  UINT32            GmsSizeSelector;
  UINT32            GmsSize;
  UINT64            McD0BaseAddress;

  ///
  /// Return if Graphics not supported or not enabled
  ///
  if (!IsIGfxSupported ()) {
    return FALSE;
  }

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);

  GmsSizeSelector = PciSegmentRead32 (McD0BaseAddress + R_SA_GGC);
  GmsSizeSelector = (GmsSizeSelector & B_SA_GGC_GMS_MASK) >> N_SA_GGC_GMS_OFFSET;
  ///
  /// Graphics Stolen Size
  /// Graphics Stolen size above 64MB has a granularity of 32MB increments
  /// GMS values below 240 correspond to Sizes 32 * GSMValue
  /// Graphics Stolen size below 64MB has a higher granularity and can be set in 4MB increments
  /// GMS values ranging from 240-254 correspond to sizes 4MB to 60MB (excluding 32MB) which is 4*(GSMValue-239)
  ///
  if (GmsSizeSelector < 240 ) {
    GmsSize = (UINT32) GmsSizeSelector * 32;
  } else {
    GmsSize = 4 * (GmsSizeSelector - 239);
  }

  DEBUG ((DEBUG_INFO, "GmsSize = %x MB\n", GmsSize));

  return ((UINT32) GmsSize * SIZE_1MB);
}

/**
  This function will check if RC6 Context Base is locked or not on IGD

  @param[in] GttMmAdr         IGD MMIO Base address value

  @retval TRUE          RC6 is Locked
  @retval FALSE         If Igfx is not supported OR RC6 is not locked OR it is Gen9 Gfx.
**/
BOOLEAN
IsRc6Locked (
  IN  UINTN     GttMmAdr
)
{
  UINT32                  Rc6CtxVal;

  ///
  /// Return if Graphics not supported or not enabled
  ///
  if (!IsIGfxSupported()) {
    return FALSE;
  }
  ///
  /// RC6 Context Base (GT) B0/D2/F0:R 10h (GTTMMADR), + 0xD48 (RC6CTXBASE), bit[0]
  ///
  Rc6CtxVal = (MmioRead32 (GttMmAdr + 0xD48) & BIT0);

  if (Rc6CtxVal) {
    return TRUE;
  }

  DEBUG ((DEBUG_WARN, "RC6 Context Base is unlocked\n"));
  return FALSE;
}

/**
  This function will check if Doorbell is in Range or not on IGD

  @param[in] GttMmAdr         IGD MMIO Base address value

  @retval TRUE          Doorbell is in Range
  @retval FALSE         If Igfx is not supported OR Doorbell is not in Range OR It is Gen9 Gfx.
**/
BOOLEAN
IsDoorbellRangeValid (
  IN  UINTN     GttMmAdr
)
{
  UINT32            GmsSize;
  UINT32            GmsBase;
  UINT32            DoorbellCtxBase;
  UINT64            McD0BaseAddress;
  UINT32            DsmTop;
  UINT32            Data32;

  ///
  /// Return if Graphics not supported or not enabled
  ///
  if (!IsIGfxSupported()) {
    return FALSE;
  }

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  ///
  /// Get GmsSize and GmsBase Address
  ///
  GmsSize = GetIgdGmsSizeInMb ();
  GmsBase = PciSegmentRead32 (McD0BaseAddress + R_SA_BDSM) & B_SA_BDSM_BDSM_MASK;
  ///
  /// TOLUD DSM Top Base (IOP B0/D0/F0:R BCh (TSEGMB), bits[31:20]
  /// Doorbell Context Base B0/D2/F0:R 10h (GTTMMADR), + 0xDC8 (MDRB_CTXBASE1), bits[31:12]
  ///
  DsmTop = (UINT32)(GmsBase + GmsSize) & 0xFFFFFFFE;
  DoorbellCtxBase = MmioRead32 (GttMmAdr + 0xDC8) & 0xFFFFFFFE;
  Data32 = DoorbellCtxBase + SIZE_4KB;

  if (Data32 == DsmTop) {
    return TRUE;
  }

  DEBUG ((DEBUG_WARN, "DoorBell Range is not Valid\n"));
  return FALSE;
}

/**
  This function will check if RC6 is in Range or not on IGD.

  @param[in] GttMmAdr         IGD MMIO Base address value

  @retval TRUE          RC6 is in Range
  @retval FALSE         RC6 is not in Range OR If Igfx is not supported OR it is a Gen9 Gfx.
**/
BOOLEAN
IsRc6RangeValid (
  IN  UINTN     GttMmAdr
)
{
  UINT32            Rc6CtxBase;
  UINT32            DoorbellCtxBase;
  UINT32            Data32;

  ///
  /// Return if Graphics not supported or not enabled
  ///
  if (!IsIGfxSupported()) {
    return FALSE;
  }
  ///
  /// RC6 Context Base (GT) B0/D2/F0:R 10h (GTTMMADR), + 0xD48 (RC6CTXBASE), bits[31:12]
  /// Doorbell Context Base B0/D2/F0:R 10h (GTTMMADR), + 0xDC8 (MDRB_CTXBASE1), bits[31:12]
  ///
  Rc6CtxBase = MmioRead32 (GttMmAdr + 0xD48) & 0xFFFFFFFE;
  DoorbellCtxBase = MmioRead32 (GttMmAdr + 0xDC8) & 0xFFFFFFFE;
  Data32 = Rc6CtxBase + SIZE_32KB;

  if (Data32 == DoorbellCtxBase) {
    return TRUE;
  }

  DEBUG ((DEBUG_WARN, "RC6 Range is not Valid\n"));
  return FALSE;
}

/**
  This function will check if PAVP is in Range or not on IGD.

  @param[in] GttMmAdr         IGD MMIO Base address value

  @retval TRUE          WOPCM is in Range
  @retval FALSE         WOPCM is not in Range OR If IGfx is not supported OR It is a Gen9 Gfx.
**/
BOOLEAN
IsWoPcmRangeValid (
  IN  UINTN     GttMmAdr
  )
{
  UINT32       PcmBase;
  UINT32       PcmSize;
  UINT32       Data32;
  UINT32       DsmTop;
  UINT32       PcmSizeBits;
  UINT64       McD0BaseAddress;

  if (!IsIGfxSupported()) {
    return FALSE;
  }

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  //
  // PCM Base (display) B0/D2/F0:R 10h (GTTMMADR), + 0x1082C0 (PAVPC0), bits[31:20]
  //
  PcmBase = (MmioRead32 (GttMmAdr + R_SA_GTTMMADR_PAVPC_OFFSET)) & B_SA_PAVPC_PCMBASE_MASK;
  //
  // PCM Size B0/D2/F0:R 10h (GTTMMADR), + 0x1082C0 (PAVPC0), bits[8:7]
  //
  PcmSizeBits = ((MmioRead32 (GttMmAdr + R_SA_GTTMMADR_PAVPC_OFFSET)) & B_SA_PAVPC_WOPCMSZ_MASK) >> N_SA_PAVPC_WOPCMSZ_OFFSET;

  switch (PcmSizeBits) {
    default:
      PcmSize = PAVP_PCM_SIZE_2_MB * SIZE_1MB;
      break;
  }

  Data32 = PcmBase + PcmSize;
  //
  // TOLUD Base (IOP B0/D0/F0:R BCh (TSEGMB), bits[31:20]
  //
  DsmTop = PciSegmentRead32 (McD0BaseAddress + R_SA_BDSM) + GetIgdGmsSizeInMb ();
  DsmTop &= 0xFFFFFFFE;

  if (Data32 == DsmTop) {
    return TRUE;
  }

  DEBUG ((DEBUG_WARN, "WOPCM Range is not Valid\n"));
  return FALSE;
}


/**
  This function will check if WOPCM base is consistent in different memory scopes.

  @param[in] GttMmAdr         IGD MMIO Base address value

  @retval TRUE          WOPCM Base is consistent
  @retval FALSE         WOPCM Base is not consistent OR If Igfx is not supported OR It is Gen9 Gfx.
**/
BOOLEAN
IsWoPcmBaseRegistersConsistent (
  IN  UINTN     GttMmAdr
)
{
  UINT64         McD0BaseAddress;
  UINT64         McD2BaseAddress;
  UINT32         PavpIop;
  UINT32         PavpDev2;
  UINT32         PavpDisplay;

  ///
  /// Return if Graphics not supported or not enabled
  ///
  if (!IsIGfxSupported ()) {
    return FALSE;
  }

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);

  PavpIop     = PciSegmentRead32 (McD0BaseAddress + R_SA_PAVPC);
  PavpDev2    = PciSegmentRead32 (McD2BaseAddress + R_SA_DEV2_PAVPC);
  PavpDisplay = MmioRead32 (GttMmAdr + R_SA_GTTMMADR_PAVPC_OFFSET);

  if ((PavpIop == PavpDev2) && (PavpIop == PavpDisplay)) {
    return TRUE;
  }

  DEBUG ((DEBUG_WARN, "WOPCM Base is not consistent\n"));
  return FALSE;

}

/**
  This function will check if PAVP is locked in different memory scopes.

  @param[in] GttMmAdr         IGD MMIO Base address value

  @retval TRUE          PAVP is locked
  @retval FALSE         PAVP is not locked
**/
BOOLEAN
IsPavpBitLocked (
  IN  UINTN     GttMmAdr
)
{
  UINT64         McD0BaseAddress;
  UINT64         McD2BaseAddress;
  UINT32         PavpIopLock;
  UINT32         PavpDev2Lock;
  UINT32         PavpDisplayLock;

  ///
  /// Return if Graphics not supported or not enabled
  ///
  if (!IsIGfxSupported ()) {
    return FALSE;
  }

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  PavpIopLock     = (PciSegmentRead32 (McD0BaseAddress + R_SA_PAVPC) & B_SA_PAVPC_PAVPLCK_MASK) >> N_SA_PAVPC_PAVPLCK_OFFSET;
  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  PavpDev2Lock    = (PciSegmentRead32 (McD2BaseAddress + R_SA_DEV2_PAVPC) & B_SA_PAVPC_PAVPLCK_MASK) >> N_SA_PAVPC_PAVPLCK_OFFSET;
  PavpDisplayLock = (MmioRead32 (GttMmAdr + R_SA_GTTMMADR_PAVPC_OFFSET) & B_SA_PAVPC_PAVPLCK_MASK) >> N_SA_PAVPC_PAVPLCK_OFFSET;
  if ((PavpIopLock == 1) && (PavpDev2Lock == 1) && (PavpDisplayLock == 1)) {
    return TRUE;
  }

  DEBUG ((DEBUG_WARN, "PAVP is unlocked\n"));
  return FALSE;
}
