/** @file
  PEIM to initialize Early Display.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/PeiGraphicsInitLib.h>
#include <Library/PeiDisplayInitLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PostCodeLib.h>
#include <CpuRegs.h>
#include <Library/TimerLib.h>
#include <Library/CpuPlatformLib.h>
#include <Register/IgdRegs.h>
#include <Library/GpioNativePads.h>
#include <Library/GpioPrivateLib.h>
#include <Library/PchInfoLib.h>
#include <Library/GraphicsInfoFruLib.h>
#include <Library/GraphicsInfoLib.h>
#include <Library/PeiDisplayInitFruLib.h>
#include <Library/CpuMailboxLib.h>
#include <MemInfoHob.h>

/**
  DisplayNativeGpioInit: Initialize the Display Native Gpio

  @param[in] GtPreMemConfig        - GRAPHICS_PEI_PREMEM_CONFIG to access the GtConfig related information

**/
VOID
DisplayNativeGpioInit (
  IN   GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig
  )
{
  UINTN                   GttMmAdr;
  UINT64                  McD2BaseAddress;

  DEBUG ((DEBUG_INFO, "DisplayNativeGpioInit: Begin \n"));
  ///
  /// Check if IGfx is supported or enabled.
  ///
  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  if (PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_INFO, "Exit DisplayNativeGpioInit() since iGFX is unsupported or disabled!\n"));
    return;
  }

  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;
  if (GttMmAdr == 0) {
    GttMmAdr = GtPreMemConfig->GttMmAdr;
    if (GttMmAdr == 0) {
      DEBUG ((DEBUG_WARN, "Temporary GttMmAdr Bar is not initialized - Exit!\n"));
      return;
    }
  }

  //
  // Enable shared and specific pins for eDP/MIPI
  //
  if (GtPreMemConfig->DdiConfiguration.DdiPortAConfig == DdiPortEdp) {
    DEBUG ((DEBUG_INFO, "Configure GPIOs for eDP on DDI-A \n"));
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_AVDD_EN(0), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_BKLTEN(0), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_BKLTCTL(0), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_DDSP_HPD('A'), 0);
  } else if (GtPreMemConfig->DdiConfiguration.DdiPortAConfig == DdiPortMipiDsi) {
    DEBUG ((DEBUG_INFO, "Configure GPIOs for MIPI on DDI-A \n"));
    // configure Gfx MMIO GPIO_CTL_1 (0xC5014 = 0x00000707) to drive the port A DDC clock and data pin outputs to 0.
    MmioWrite32 (GttMmAdr + R_SA_GTTMMADR_PORT_A_GPIO_CTL, 0x00000707);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_AVDD_EN(0), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_BKLTEN(0), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_BKLTCTL(0), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_MIPI_PANEL_RESET(0), 0);
  }

  if (GtPreMemConfig->DdiConfiguration.DdiPortBConfig == DdiPortEdp) {
    DEBUG ((DEBUG_INFO, "Configure GPIOs for eDP on DDI-B \n"));
    MmioAndThenOr32 ((GttMmAdr + R_SA_GTTMMADR_SCHICKEN_1), (UINT32)(~B_SA_GTTMMADR_SECOND_PPS_IO_SELECT), B_SA_GTTMMADR_SECOND_PPS_IO_SELECT);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_AVDD_EN(1), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_BKLTEN(1), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_BKLTCTL(1), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_DDSP_HPD('B'), 0);
  } else if (GtPreMemConfig->DdiConfiguration.DdiPortBConfig == DdiPortMipiDsi) {
    DEBUG ((DEBUG_INFO, "Configure GPIOs for MIPI on DDI-B \n"));
    // Configure display IO pins for second PPS signals. IO pins are muxed between the second PPS/backlight and DDIC GPIO/GMBUS/HPD.
    // Set Gfx MMIO 0xC2000 bit 2 = 1 to switch the mux to allow the second PPS and backlight to be used.
    MmioAndThenOr32 ((GttMmAdr + R_SA_GTTMMADR_SCHICKEN_1), (UINT32)(~B_SA_GTTMMADR_SECOND_PPS_IO_SELECT), B_SA_GTTMMADR_SECOND_PPS_IO_SELECT);
    // configure Gfx MMIO GPIO_CTL_2 (0xC5018 = 0x00000707) to drive the port B DDC clock and data pin outputs to 0.
    MmioWrite32 (GttMmAdr + R_SA_GTTMMADR_PORT_B_GPIO_CTL, 0x00000707);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_AVDD_EN(1), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_BKLTEN(1), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_PANEL_BKLTCTL(1), 0);
    GpioSetNativePadByFunction (GPIO_FUNCTION_MIPI_PANEL_RESET(1), 0);
  }

  ///
  /// Enable DDSP_HPD pins for DP HotPlug
  ///
  if (GtPreMemConfig->DdiConfiguration.DdiPortAHpd) {
    DEBUG ((DEBUG_INFO, "Configure HPD for DDI-A\n"));
    GpioSetNativePadByFunction (GPIO_FUNCTION_DDSP_HPD('A'), 0);
  }
  if (GtPreMemConfig->DdiConfiguration.DdiPortBHpd) {
    DEBUG ((DEBUG_INFO, "Configure HPD for DDI-B\n"));
    GpioSetNativePadByFunction (GPIO_FUNCTION_DDSP_HPD('B'), 0);
  }
  if (GtPreMemConfig->DdiConfiguration.DdiPortCHpd) {
    DEBUG ((DEBUG_INFO, "Configure HPD for DDI-C\n"));
    GpioSetNativePadByFunction (GPIO_FUNCTION_DDSP_HPD('C'), 0);
  }
  if (GtPreMemConfig->DdiConfiguration.DdiPort1Hpd) {
    DEBUG ((DEBUG_INFO, "Configure HPD for DDI-1\n"));
    GpioSetNativePadByFunction (GPIO_FUNCTION_DDSP_HPD(1), 0);
  }
  if (GtPreMemConfig->DdiConfiguration.DdiPort2Hpd) {
    DEBUG ((DEBUG_INFO, "Configure HPD for DDI-2\n"));
    GpioSetNativePadByFunction (GPIO_FUNCTION_DDSP_HPD(2), 0);
  }
  if (GtPreMemConfig->DdiConfiguration.DdiPort3Hpd) {
    DEBUG ((DEBUG_INFO, "Configure HPD for DDI-3\n"));
    GpioSetNativePadByFunction (GPIO_FUNCTION_DDSP_HPD(3), 0);
  }
  if (GtPreMemConfig->DdiConfiguration.DdiPort4Hpd) {
    DEBUG ((DEBUG_INFO, "Configure HPD for DDI-4\n"));
    GpioSetNativePadByFunction (GPIO_FUNCTION_DDSP_HPD(4), 0);
  }

  ///
  /// Enable DDP CTRLCLK and CTRLDATA pins OR TBT RX and TX pins
  ///
  //
  // DDI Port A
  //
  if (GtPreMemConfig->DdiConfiguration.DdiPortADdc == DdiDdcEnable) {
    GpioEnableDpInterface (GpioDdpA);
  }
  //
  // DDI Port B
  //
  if (GtPreMemConfig->DdiConfiguration.DdiPortBDdc == DdiDdcEnable) {
    GpioEnableDpInterface (GpioDdpB);
  }
  //
  // DDI Port C
  //
  if (GtPreMemConfig->DdiConfiguration.DdiPortCDdc == DdiDdcEnable) {
    GpioEnableDpInterface (GpioDdpC);
  }
  //
  // DDI Port 1
  //
  if (GtPreMemConfig->DdiConfiguration.DdiPort1Ddc == DdiDdcEnable) {
    GpioEnableDpInterface (GpioDdp1);
  }
  //
  // DDI Port 2
  //
  if (GtPreMemConfig->DdiConfiguration.DdiPort2Ddc == DdiDdcEnable) {
    GpioEnableDpInterface (GpioDdp2);
  }
  //
  // DDI Port 3
  //
  if (GtPreMemConfig->DdiConfiguration.DdiPort3Ddc == DdiDdcEnable) {
    GpioEnableDpInterface (GpioDdp3);
  }
  //
  // DDI Port 4
  //
  if (GtPreMemConfig->DdiConfiguration.DdiPort4Ddc == DdiDdcEnable) {
    GpioEnableDpInterface (GpioDdp4);
  }

  DEBUG ((DEBUG_INFO, "DisplayNativeGpioInit: End \n"));
}
/**
  DisplayInitPreMem: Initialize the Display in PreMem phase

  @param[in] GtPreMemConfig        - GRAPHICS_PEI_PREMEM_CONFIG to access the GtConfig related information

**/
VOID
DisplayInitPreMem (
  IN   GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig
  )
{
  ///
  /// Initialize Display Native GPIO's
  ///
  DisplayNativeGpioInit (GtPreMemConfig);
  ///
  /// Program Display Work around
  ///
  ProgramDisplayWorkaround (GtPreMemConfig);

}
/**
  Poll Run busy clear

  @param[in] Base    - Base address of MMIO
  @param[in] Timeout - Timeout value in microsecond

  @retval TRUE       - Run Busy bit is clear
  @retval FALSE      - Run Busy bit is still set
**/
BOOLEAN
PollRunBusyClear (
  IN    UINT64           Base,
  IN    UINT32           Timeout
  )
{
  UINT32  Value;
  BOOLEAN Status = FALSE;

  //
  // Make timeout an exact multiple of 10 to avoid infinite loop
  //
  if ((Timeout) % 10 != 0) {
    Timeout = (Timeout) + 10 - ((Timeout) % 10);
  }

  while (Timeout != 0) {
    Value = MmioRead32 ((UINTN) Base + 0x138124);
    if (Value & BIT31) {
      //
      // Wait for 10us and try again.
      //
      DEBUG ((DEBUG_INFO, "Interface register run busy bit is still set. Trying again \n"));
      MicroSecondDelay (MAILBOX_WAITTIME);
      Timeout = Timeout - MAILBOX_WAITTIME;
    } else {
      Status = TRUE;
      break;
    }
  }
  ASSERT ((Timeout != 0));

  return Status;
}

/**
  This function will get value of reference clock from DSSM strap register.

  @param[in] GttMmAdr            - Base Address of IGFX MMIO BAR

  @retval ReferenceFreq          - Returns value of reference frequency found in DSSM strap register.
                                   Unit of freuquency is in KHz.
**/
UINT32
GetDssmReferenceFrequency (
  IN  UINT32                GttMmAdr
)
{
  UINT32 DssmVal;
  UINT32 ReferenceFreq;
  UINT8  ReferenceFreqBits;

  DssmVal           = 0;
  ReferenceFreq     = 0;
  ReferenceFreqBits = 0;

  //
  // Read Reference frequency from DSSM register.
  // BIT31:29 of DSSM register indicates reference frequency
  // For BIT31:29 = 0, reference frequency = 24 MHz.
  // For BIT31:29 = 1, reference frequency = 19.2 MHz.
  // For BIT31:29 = 2, reference frequency = 38.4 MHz.
  //
  DssmVal           = MmioRead32 (GttMmAdr + R_SA_GTTMMADR_DSSM_OFFSET);
  ReferenceFreqBits = (UINT8)((DssmVal & B_SA_GTTMMADR_DSSM_REFERENCE_FREQ_MASK) >> B_SA_GTTMMADR_DSSM_REFERENCE_FREQ_OFFSET);

  switch (ReferenceFreqBits) {
    case 0x0:
      ReferenceFreq = V_SA_CDCLK_PLL_REF_FREQUENCY_24MHZ;
      break;
    case 0x1:
      ReferenceFreq = V_SA_CDCLK_PLL_REF_FREQUENCY_19_2MHZ;
      break;
    case 0x2:
      ReferenceFreq = V_SA_CDCLK_PLL_REF_FREQUENCY_38_4MHZ;
      break;
    default:
     DEBUG ((EFI_D_ERROR,"Invalid PLL Reference clock value during CdClockInit()\n"));
      ASSERT (FALSE);
  }
  return ReferenceFreq;
}

/**
  Program the max Cd Clock supported by the platform

  @param[in] GtConfig            - Instance of GRAPHICS_PEI_CONFIG
  @param[in] GttMmAdr            - Base Address of IGFX MMIO BAR

  @retval EFI_SUCCESS            - CD Clock value programmed.
  @retval EFI_INVALID_PARAMETER  - The input parameter is invalid

**/
EFI_STATUS
ProgramCdClkReg (
  IN       GRAPHICS_PEI_CONFIG          *GtConfig,
  IN       UINT32                       GttMmAdr
  )
{
  UINT32         Data32Or;
  UINT32         ReferenceFreq;

  ReferenceFreq      = 0;

  ///
  /// For Gen12, CDCLK_CTL - GttMmAdr + 0x46000
  /// CdClock 0:  192 Mhz -  [10:0] = 001 0111 1110 = 0x17E
  /// CdClock 1:  307.2Mhz - [10:0] = 010 0110 0100 = 0x264
  /// CdClock 2:  312Mhz   - [10:0] = 010 0110 1110 = 0x26E
  /// CdClock 3:  324Mhz   - [10:0] = 010 1000 0110 = 0x286
  /// CdClock 4:  326.4Mhz - [10:0] = 010 1000 1011 = 0x28B
  /// CdClock 5:  552Mhz   - [10:0] = 100 0100 1110 = 0x44E
  /// CdClock 6:  556.8Mhz - [10:0] = 100 0101 1000 = 0x458
  /// CdClock 7:  648Mhz   - [10:0] = 101 0000 1110 = 0x50E
  /// CdClock 8:  652.8Mhz - [10:0] = 101 0001 1000 = 0x518
  /// CdClock:0xFF Program Max based on reference clock:- For 19.2MHz, 38.4MHz = 652.8 MHz. For 24MHz = 648MHz.
  switch (GtConfig->CdClock) {
  case 0 :
    Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_192;
    break;
  case 1 :
    Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_307_2;
    break;
  case 2 :
    Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_312;
    break;
  case 3 :
    Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_324;
    break;
  case 4 :
    Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_326_4;
    break;
  case 5 :
    Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_552;
    break;
  case 6 :
    Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_556_8;
    break;
  case 7 :
    Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_648;
    break;
  case 8 :
    Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_652_8;
    break;
  case 0xFF:
    ReferenceFreq = GetDssmReferenceFrequency (GttMmAdr);
    if ((ReferenceFreq == V_SA_CDCLK_PLL_REF_FREQUENCY_19_2MHZ) || (ReferenceFreq == V_SA_CDCLK_PLL_REF_FREQUENCY_38_4MHZ)) {
      Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_652_8;
    } else {
      Data32Or = V_SA_CDCLK_CTL_CD_FREQ_DECIMAL_648;
    }
    break;
  default:
    return EFI_INVALID_PARAMETER;
  }
  //
  // Program CDCLK register with user selected value so that GOP can read and initialize CD Clock.
  //
  MmioAndThenOr32 (GttMmAdr + R_SA_GTTMMADR_CDCLK_CTL_OFFSET, 0xFFFFF800, Data32Or);

  DEBUG ((DEBUG_INFO, "ProgramCdClkReg: CdClock 0X%X CDCLK_CTL 0X%X\n", (GtConfig->CdClock), MmioRead32 ((UINTN) (GttMmAdr + R_SA_GTTMMADR_CDCLK_CTL_OFFSET))));

  return EFI_SUCCESS;
}


/**
  Initialize the full CD clock as per Bspec sequence.

  @param[in] GtConfig            - Instance of GRAPHICS_PEI_CONFIG
  @param[in] GtPreMemConfig      - Instance of GRAPHICS_PEI_PREMEM_CONFIG

  @retval EFI_SUCCESS            - CD Clock Init successful.
  @retval EFI_INVALID_PARAMETER  - The input parameter is invalid
  @retval EFI_UNSUPPORTED        - iGfx is not present.
**/
EFI_STATUS
CdClkInit (
  IN  GRAPHICS_PEI_CONFIG             *GtConfig,
  IN  GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig
  )
{
  return EFI_SUCCESS;
}

/**
  InitializeDisplayAudio: Initialize display engine for iDisplay Audio programming.

  This function is called by PCH Init Pre-mem code to program CD clock freq.
  CD clock program steps involve enable PG1 & PG2 and P-code notification.

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_ABORTED             S3 boot - display already initialized
  @retval EFI_UNSUPPORTED         iGfx disabled, iDisplay Audio not present
  @retval EFI_NOT_FOUND           SaPolicy or temporary GTT base address not found
**/
EFI_STATUS
InitializeDisplayAudio (
  VOID
  )
{
  UINT64                      McD2BaseAddress;
  SI_PREMEM_POLICY_PPI        *SiPreMemPolicyPpi;
  GRAPHICS_PEI_PREMEM_CONFIG  *GtPreMemConfig;
  UINTN                       GttMmAdr;
  EFI_STATUS                  Status;
  UINT32                      Msac;
  LARGE_INTEGER               GmAdrValue;
  UINT32                      Data32And;
  UINT32                      Data32Or;

  Msac               = 0;

  DEBUG ((DEBUG_INFO, "InitializeDisplayAudio() Start\n"));

  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  Msac = PciSegmentRead32(McD2BaseAddress + R_SA_IGD_MSAC_OFFSET);

  if (PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_INFO, "iGFX not enabled - iDisplayAudio not supported - Exit!\n"));
    return EFI_UNSUPPORTED;
  }

  ///
  /// Check if GttMmAdr has been already assigned, initialize if not
  ///
  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;
  if (GttMmAdr == 0) {
    ///
    /// Get SA Policy settings through the SaInitConfigBlock PPI
    ///
    Status = PeiServicesLocatePpi (
               &gSiPreMemPolicyPpiGuid,
               0,
               NULL,
               (VOID **) &SiPreMemPolicyPpi
               );
    if (EFI_ERROR (Status) || (SiPreMemPolicyPpi == NULL)) {
      DEBUG ((DEBUG_WARN, "SaPolicy PPI not found - Exit!\n"));
      return EFI_NOT_FOUND;
    }

    Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gGraphicsPeiPreMemConfigGuid, (VOID *) &GtPreMemConfig);
    ASSERT_EFI_ERROR (Status);

    GttMmAdr = GtPreMemConfig->GttMmAdr;
    if (GttMmAdr == 0) {
      DEBUG ((DEBUG_WARN, "Temporary GttMmAdr Bar is not initialized - Exit!\n"));
      return EFI_NOT_FOUND;
    }

    ///
    /// Program and read back GTT Memory Mapped BAR
    ///
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR, (UINT32) (GttMmAdr & 0xFF000000));
    GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;

    PciSegmentAndThenOr32 (McD2BaseAddress + R_SA_IGD_MSAC_OFFSET, (UINT32)~(BIT20 + BIT19 + BIT18 + BIT17 + BIT16), SA_GT_APERTURE_SIZE_256MB << 16);

    GmAdrValue.Data = GetIgfxApertureBar (GtPreMemConfig);
    PciSegmentWrite32 (McD2BaseAddress + GetIgfxApertureOffset (), GmAdrValue.Data32.Low);
    PciSegmentWrite32 (McD2BaseAddress + GetIgfxApertureOffset () + 4, GmAdrValue.Data32.High);
  }

  if (!IgfxCmdRegEnabled()) {
    ///
    /// Enable Bus Initiator and Memory access on 0:2:0 if not enabled
    ///
    PciSegmentOr16 (McD2BaseAddress + PCI_COMMAND_OFFSET, (BIT2 | BIT1));
  }
  //
  // Enable PCH Reset Handshake
  //
  MmioOr32 ((GttMmAdr + R_SA_GTTMMADR_NDE_RSTWRN_OPT_OFFSET), BIT4);
  //
  // Program Display IO analog settings
  //
  Data32And = ~(UINT32) (B_SA_GTTMMADR_AUD_LINK_HYSTERESIS_MASK | B_SA_GTTMMADR_AUD_LINK_RISE_SLEW_MASK |
                         B_SA_GTTMMADR_AUD_LINK_FALL_SLEW_MASK | B_SA_GTTMMADR_AUD_LINK_PU_STRENGTH_MASK | B_SA_GTTMMADR_AUD_LINK_PD_STRENGTH_MASK);
  Data32Or = (UINT32) ((V_SA_GTTMMADR_AUD_LINK_HYSTERESIS << N_SA_AUD_LINK_HYSTERESIS_OFFSET) |
                       (V_SA_GTTMMADR_AUD_LINK_PD_STRENGTH << N_SA_AUD_LINK_PD_STRENGTH_OFFSET) |
                       (V_SA_GTTMMADR_AUD_LINK_PU_STRENGTH << N_SA_AUD_LINK_PU_STRENGTH_OFFSET) |
                       (V_SA_GTTMMADR_AUD_LINK_FALL_SLEW << N_SA_AUD_LINK_FALL_SLEW_OFFSET) |
                       (V_SA_GTTMMADR_AUD_LINK_RISE_SLEW << N_SA_AUD_LINK_RISE_SLEW_OFFSET));
  MmioAndThenOr32 ( GttMmAdr + R_SA_GTTMMADR_AUDIO_PIN_BUF_CTL_OFFSET, Data32And, Data32Or);
  MmioAndThenOr32 ( GttMmAdr + R_SA_GTTMMADR_UTIL_PIN_BUF_CTL_OFFSET, Data32And, Data32Or);
  DEBUG ((DEBUG_INFO, "AUDIO_PIN_BUF_CTL = %x\n", MmioRead32 (GttMmAdr + R_SA_GTTMMADR_AUDIO_PIN_BUF_CTL_OFFSET)));
  DEBUG ((DEBUG_INFO, "UTIL_PIN_BUF_CTL = %x\n", MmioRead32 (GttMmAdr + R_SA_GTTMMADR_UTIL_PIN_BUF_CTL_OFFSET)));

  ///
  /// Program Aperture Size MSAC register based on policy
  ///
  PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_MSAC_OFFSET, Msac);

  DEBUG ((DEBUG_INFO, "InitializeDisplayAudio() End\n"));
  return EFI_SUCCESS;
}

/**
  ConfigureIDispAudioFrequency: Configures iDisplay Audio BCLK frequency and T-Mode

  @param[in] RequestedBclkFrequency     IDisplay Link clock frequency to be set
  @param[in] RequestedTmode             IDisplay Link T-Mode to be set

  @retval EFI_NOT_FOUND                 SA Policy PPI or GT config block not found, cannot initialize GttMmAdr
  @retval EFI_UNSUPPORTED               iDisp link unsupported frequency
  @retval EFI_SUCCESS                   The function completed successfully
**/
EFI_STATUS
ConfigureIDispAudioFrequency (
  IN       HDAUDIO_LINK_FREQUENCY   RequestedBclkFrequency,
  IN       HDAUDIO_IDISP_TMODE      RequestedTmode
  )
{
  return EFI_SUCCESS;
}

/**
  Checks if Inform Gfx MemConfig Supported for GFX ID's

  @retval FALSE  Inform Gfx MemConfig not Supported
  @retval TRUE   Inform Gfx MemConfig Supported
**/
BOOLEAN
EFIAPI
InformGfxMemConfigSupportedId (
  VOID
  )
{
  UINT16     GtDid;
  BOOLEAN    IsSupported = FALSE;

  GtDid = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, PCI_DEVICE_ID_OFFSET));


  switch (GtDid) {
    case V_SA_PCI_DEV_2_GT2_ADL_P_1_ID:
    case V_SA_PCI_DEV_2_GT2_ADL_P_2_ID:
    case V_SA_PCI_DEV_2_GT2_ADL_P_3_ID:
    case V_SA_PCI_DEV_2_GT2_ADL_P_4_ID:
    case V_SA_PCI_DEV_2_GT2_ADL_P_5_ID:
      IsSupported = TRUE;
      break;

    default:
      IsSupported = FALSE;
      break;
  }

  return IsSupported;
}

/**
 Inform Gfx MemConfig
**/
VOID
InformGfxMemConfig (
  VOID
)
{
  MEMORY_INFO_DATA_HOB    *MemInfo;
  EFI_HOB_GUID_TYPE       *GuidHob;
  UINT32                  MailboxStatus;
  UINT32                  MailboxData;
  EFI_STATUS              Status;
  PCODE_MAILBOX_INTERFACE MailboxCommand;
  UINT16                  MemoryBandwidth;

  MemInfo = NULL;
  GuidHob = NULL;
  Status  = EFI_SUCCESS;
  //
  // there is no data associated with the cmd
  //
  MailboxData = 0;
  MemoryBandwidth = 128;

  DEBUG ((DEBUG_INFO, "InformGfxMemConfig Start\n"));

  if (InformGfxMemConfigSupportedId () && IsIGfxSupported ()) {
    GuidHob = GetFirstGuidHob (&gSiMemoryInfoDataGuid);

    if (GuidHob != NULL) {
      MemInfo = (MEMORY_INFO_DATA_HOB *) GET_GUID_HOB_DATA (GuidHob);
      //call Pcode if memory bandwidth less than 128 bit
      if (MemInfo->TotalMemWidth < MemoryBandwidth) {
        DEBUG ((DEBUG_INFO, "InformGfxMemConfig Supported\n"));
        MailboxCommand.InterfaceData  = 0;
        MailboxCommand.Fields.Command = BIOS_MAILBOX_INFORM_GRAPHICS_MEMORY_CONFIG;
        MailboxCommand.Fields.Param1  = SELECT_ALTERNATE_DID2;
        DEBUG ((DEBUG_INFO, "Send Mailbox Command %d to Inform Gfx Mem Config\n", BIOS_MAILBOX_INFORM_GRAPHICS_MEMORY_CONFIG));
        Status = MailboxWrite (MAILBOX_TYPE_PCODE, MailboxCommand.InterfaceData, MailboxData , &MailboxStatus);
        if (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) {
          DEBUG ((DEBUG_INFO, "GFX DID2 Bit 7 is Cleared EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        } else if (MailboxStatus == PCODE_MAILBOX_CC_LOCKED) {
          DEBUG ((DEBUG_ERROR, "Subcommand interface is locked and cannot be used EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        } else if (MailboxStatus == PCODE_MAILBOX_CC_TIMEOUT) {
          DEBUG ((DEBUG_ERROR, "Resource not available EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        } else if (MailboxStatus == PCODE_MAILBOX_CC_ILLEGAL_CMD) {
          DEBUG ((DEBUG_ERROR, "Unlisted subcommand Passes, Illegal instruction EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        } else {
          DEBUG ((DEBUG_INFO, "Mailbox write EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        }
      } else {
        DEBUG ((DEBUG_INFO, "InformGfxMemConfig Memory UnSupported, MemoryBandwidth > 128\n"));
      }
    } else {
      DEBUG ((DEBUG_ERROR, "MemoryInfo Hob is not found\n"));
    }
  } else {
    DEBUG ((DEBUG_INFO, "InformGfxMemConfig UnSupported\n"));
  }
  DEBUG ((DEBUG_INFO, "InformGfxMemConfig End\n"));
}
