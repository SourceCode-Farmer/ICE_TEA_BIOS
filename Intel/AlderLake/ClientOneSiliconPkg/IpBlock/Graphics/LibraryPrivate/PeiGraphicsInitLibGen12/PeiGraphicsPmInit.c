/** @file
  PEIM to initialize IGFX PM

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/PeiServicesLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PciExpressLib.h>
#include <IndustryStandard/Pci22.h>
#include <Library/PeiGraphicsInitLib.h>
#include <Library/PeiDisplayInitLib.h>
#include <Library/PeiDisplayInitFruLib.h>
#include <Library/SaInitLib.h>
#include <Library/TimerLib.h>
#include <CpuRegs.h>
#include <Library/CpuPlatformLib.h>
#include <Library/SaPlatformLib.h>
#include <Register/IgdRegs.h>
#include <Library/GraphicsInfoFruLib.h>
#include <Library/GraphicsInfoLib.h>

///
/// Driver Consumed PPI Prototypes
///
#include <GraphicsConfig.h>
#include <Ppi/SiPolicy.h>

///
/// The maximum subslice count for Gen12.
///
#define MAX_SUBSLICE_COUNT 0x6
#define DSS_ENABLED        0x1

GLOBAL_REMOVE_IF_UNREFERENCED BOOT_SCRIPT_REGISTER_SETTING  gSaGtRC6Registers[] = {
  //
  // BaseAddr  Offset  AndMask  OrMask
  //
  //
  // Render/Video/Blitter Idle Max Count
  //
  {0x0,  0x2054,    0x0,  0xA},
  {0x0,  0x18054,   0x0,  0xA},
  {0x0,  0x1A054,   0x0,  0xA},
  {0x0,  0x22054,   0x0,  0xA},
  {0x0,  0x1C0054,  0x0,  0xA},
  {0x0,  0x1C4054,  0x0,  0xA},
  {0x0,  0x1C8054,  0x0,  0xA},
  {0x0,  0x1D0054,  0x0,  0xA},
  {0x0,  0x1D4054,  0x0,  0xA},
  {0x0,  0x1D8054,  0x0,  0xA},
  {0x0,  0x1E0054,  0x0,  0xA},
  {0x0,  0x1E4054,  0x0,  0xA},
  {0x0,  0x1E8054,  0x0,  0xA},
  {0x0,  0x1F0054,  0x0,  0xA},
  {0x0,  0x1F4054,  0x0,  0xA},
  {0x0,  0x1F8054,  0x0,  0xA},
  {0x0,  0xC3E4,    0x0,  0xA},

  //
  // Enable Idle Messages
  //
  {0x0,  0x2050,    0x0,  0x00010000},
  {0x0,  0x18050,   0x0,  0x00010000},
  {0x0,  0x1A050,   0x0,  0x00010000},
  {0x0,  0x22050,   0x0,  0x00010000},
  {0x0,  0x182050,  0x0,  0x00010000},
  {0x0,  0x1C0050,  0x0,  0x00010000},
  {0x0,  0x1C4050,  0x0,  0x00010000},
  {0x0,  0x1C8050,  0x0,  0x00010000},
  {0x0,  0x1D0050,  0x0,  0x00010000},
  {0x0,  0x1D4050,  0x0,  0x00010000},
  {0x0,  0x1D8050,  0x0,  0x00010000},
  {0x0,  0x1E0050,  0x0,  0x00010000},
  {0x0,  0x1E4050,  0x0,  0x00010000},
  {0x0,  0x1E8050,  0x0,  0x00010000},
  {0x0,  0x1F0050,  0x0,  0x00010000},
  {0x0,  0x1F4050,  0x0,  0x00010000},
  {0x0,  0x1F8050,  0x0,  0x00010000},
};

GLOBAL_REMOVE_IF_UNREFERENCED BOOT_SCRIPT_REGISTER_SETTING  gSaGtSecurityRegisters[] = {
  {0x0,  0x41A0,  0x0,         0x80040003},
  {0x0,  0x41A4,  0x0,         0x800507FC},
  {0x0,  0x41A8,  0x0,         0x800508D3},
  {0x0,  0x41AC,  0x0,         0x800BFFFC},
  {0x0,  0x41B0,  0x0,         0x80138001},
  {0x0,  0x41B4,  0x0,         0x8015FFFC},
  {0x0,  0x41B8,  0x0,         0x80190003},
  {0x0,  0x41BC,  0x0,         0x80197FFC},
  {0x0,  0x41C0,  0x7FFFFFFF,  0x80000000},
  {0x0,  0x41C4,  0x7FFFFFFF,  0x80000000},
  {0x0,  0x41C8,  0x7FFFFFFF,  0x80000000},
  {0x0,  0x41CC,  0x7FFFFFFF,  0x80000000},
  {0x0,  0x41D0,  0x7FFFFFFF,  0x80000000},
  {0x0,  0x41D4,  0x7FFFFFFF,  0x80000000},
  {0x0,  0x41D8,  0x7FFFFFFF,  0x80000000},
  {0x0,  0x41DC,  0x7FFFFFFF,  0x80000000}
};
GLOBAL_REMOVE_IF_UNREFERENCED UINT32 gSpcLock[] = {
  //
  // GTI
  //
  0x24008,
  //
  // Slice
  //
  0x24188,
  0x24190,
  0x2418C,
  //
  // Media Sampler
  //
  0x24A08,
  0x24A0C,
  0x24A10,
  //
  // Media -L
  //
  0x25208,
  0x2520C,
  0x25210,
  //
  // Media - R
  //
  0x25288,
  0x2528C,
  0x25290,
  //
  // HCP (Media - L)
  //
  0x25608,
  0x25610,
  0x2560C,
  //
  // HCP (Media - R)
  //
  0x25688,
  0x25690,
  0x2568C,
  //
  // MFXVDNC (Media - L)
  //
  0x25A08,
  0x25A10,
  0x25A0C,
  //
  // MFXVDNC (Media - R)
  //
  0x25A88,
  0x25A90,
  0x25A8C,
};

GLOBAL_REMOVE_IF_UNREFERENCED BOOT_SCRIPT_REGISTER_SETTING  gSaGtClockGatingRegisters[] = {
  //
  // BaseAddr  Offset  AndMask  OrMask
  //
  //
  //Unslice
  //
  {0x0,  0x9430,  0x0,  0x0},
  {0x0,  0x9434,  0x0,  0x0},
  {0x0,  0x9438,  0x0,  0x0},
  {0x0,  0x9440,  0x0,  0x0},
  {0x0,  0x9444,  0x0,  0x0},
  //
  //Slice
  //
  {0x0,  0x94D0,  0x0,  0x0},
  {0x0,  0x94D4,  0x0,  0x0},
  //
  //Subslice
  //
  {0x0,  0x9520,  0x0,  0x0},
  //
  //VE/VD Boxes
  //
  {0x0,  0x1C3F0C,  0x0,  0x0},
  {0x0,  0x1C7F0C,  0x0,  0x0},
  {0x0,  0x1D3F0C,  0x0,  0x0},
  {0x0,  0x1D7F0C,  0x0,  0x0},
};

/**
  Initialize GT PowerManagement of SystemAgent.

  @param[in] GtConfig            - Instance of GRAPHICS_PEI_CONFIG
  @param[in] GttMmAdr            - Base Address of IGFX MMIO BAR
  @param[in] MchBarBase          - Base Address of MCH_BAR

  @retval EFI_SUCCESS            - GT Power Management initialization complete
  @retval EFI_INVALID_PARAMETER  - The input parameter is invalid
**/
EFI_STATUS
PmInit (
  IN       GRAPHICS_PEI_CONFIG          *GtConfig,
  IN       UINT32                       GttMmAdr,
  IN       UINT32                       MchBarBase
  )
{
  UINT32            RegOffset;
  UINT32            Data32;
  UINT32            Data32Mask;
  UINT32            Result;
  UINT8             LoopCounter;
  UINT32            Data32And;
  UINT32            Data32Or;
  UINT32            Ssid;

  DEBUG ((DEBUG_INFO, "----- GT PMInit() Begin------\n"));

  if ((GttMmAdr == 0) || (MchBarBase == 0) || (GtConfig == NULL)) {
    DEBUG ((DEBUG_WARN, "Invalid parameters for PmInit\n"));
    return EFI_INVALID_PARAMETER;
  }

  ///
  /// 1aa. Enable all GTI-Uncore clock gating
  ///
  RegOffset = 0x9560;
  Data32 = GetClockGating ();

  DEBUG ((DEBUG_INFO, "PmInit 9560 = %x \n", MmioRead32(GttMmAdr + RegOffset)));
  MmioWrite32 (GttMmAdr + RegOffset, Data32);

  if (GtConfig->PmSupport && (!IsDisplayOnlySku())) {
    DEBUG ((DEBUG_INFO, "Starting Register programming for GT PM.\n"));
    //
    // Lock squash steps
    //
    RegOffset = 0x9584;
    Data32Or  = BIT31;
    Data32And = (UINT32) ~(BIT31);
    MmioAndThenOr32 (GttMmAdr + RegOffset, Data32And, Data32Or);
    //
    // Set squash step 1 to 16.667MHz and lock
    //
    RegOffset = 0x9588;
    Data32Or  = 0x80000001;
    Data32And = 0x7FFFFE00;
    MmioAndThenOr32 (GttMmAdr + RegOffset, Data32And, Data32Or);

    ///
    /// 1d. Enable Clock squashing based di/dt mitigation
    ///
    Data32Or = B_SA_GTTMMADR_PROCHOT_0_TEETH_BREAKING_EN | B_SA_GTTMMADR_PROCHOT_0_DIDT_LOCK_CONTROL;
    MmioOr32 (GttMmAdr + R_SA_GTTMMADR_PROCHOT_0_OFFSET, Data32Or);
    //
    // 1g. Set and lock RC1p frequency to 16.667Mhz.
    //
    RegOffset = 0xC14;
    Data32Or  = 0x80000001;
    MmioOr32 (GttMmAdr + RegOffset, Data32Or);
    //
    // 1h. Set RC1p hysteresis to 4096 SBclocks, 10us.
    //
    RegOffset = 0xC1C;
    Data32Or  = 0x00001000;
    MmioOr32 (GttMmAdr + RegOffset, Data32Or);
    //
    // 1i. Enable or disable RC1p.
    //
    RegOffset = 0xC18;
    Data32Or  = 0;
    if (GtConfig -> RC1pFreqEnable == 1) {
      Data32Or = 0x1;
    }
    MmioOr32 (GttMmAdr + RegOffset, Data32Or);

    ///
    /// 1j. Programming Crystal Clock.
    ///
    RegOffset = 0xD00;
    Data32    = MmioRead32 (GttMmAdr + RegOffset);
    ///
    /// Programm [2:1] = 11b if [5:3] is 001 (indicate ref clock is 19.2Mhz)
    /// Programm [2:1] = 10b if [5:3] is 000/010/011 (indicate ref clock is 24Mhz/38.4MHz/25MHz)
    ///
    Data32 &= (BIT3 | BIT4 | BIT5);
    Data32 = (Data32 >>3);
    if (Data32 == 1) {
      Data32Or = 0x6;
    } else if ((Data32 == 0) || (Data32 == 2) || (Data32 == 3)) {
      Data32Or = 0x4;
    }

    Data32Or |= BIT31;
    Data32And = (UINT32) ~(BIT2 | BIT1);
    MmioAndThenOr32 (GttMmAdr + RegOffset, Data32And, Data32Or);

    ///
    /// 1k.DFD_RESTRORE Programming
    ///
    RegOffset = 0xD38;
    Data32    = MmioRead32 (GttMmAdr + RegOffset);
    Data32   |= BIT31;
    MmioWrite32 (GttMmAdr + RegOffset, Data32);
    ///
    /// 1l.Read back 0xD34[0]=0 & 0xD38[31]=1 and boot accordingly
    ///
    if (((MmioRead32 (GttMmAdr + 0xD34) & BIT0) != 0) || ((MmioRead32 (GttMmAdr + 0xD38) & BIT31) != BIT31)) {
      DEBUG ((EFI_D_ERROR,"Not able to program Dfd_Restrore due to some other agent is configuring and locking it before BIOS does.\n"));
      CpuDeadLoop ();
    }

    ///
    /// 2a. Enable Force Wake
    ///
    RegOffset = 0xA188;
    Data32 = 0x00010001;
    MmioWrite32 (GttMmAdr + RegOffset, Data32);

    ///
    /// 2b. Poll to verify Force Wake Acknowledge Bit
    ///
    RegOffset = 0x130044;
    Data32Mask = BIT0;
    Result = 1;
    PollGtReady (GttMmAdr, RegOffset, Data32Mask, Result);
    DEBUG ((DEBUG_INFO, "Poll to verify Force Wake Acknowledge Bit, Result = 1\n"));

    ///
    /// GT Security Resgister programming.
    ///
    for (LoopCounter = 0; LoopCounter < sizeof (gSaGtSecurityRegisters) / sizeof(BOOT_SCRIPT_REGISTER_SETTING); ++LoopCounter) {
      RegOffset    = gSaGtSecurityRegisters[LoopCounter].Offset;
      Data32And    = gSaGtSecurityRegisters[LoopCounter].AndMask;
      Data32Or     = gSaGtSecurityRegisters[LoopCounter].OrMask;

      MmioAndThenOr32 (GttMmAdr + RegOffset, Data32And, Data32Or);
    }

    ///
    /// 5a. GPM Control
    ///
    RegOffset = 0xA180;
    Data32 = 0x81200000;
    MmioWrite32 (GttMmAdr + RegOffset, Data32);

    ///
    /// 5b. ECOBUS Disable Fence writes
    ///
    RegOffset = 0xA194;
    Data32Or = BIT5 | BIT7 | BIT8 | BIT31;
    MmioOr32 (GttMmAdr + RegOffset, Data32Or);

    ///
    /// 5b. Enable DOP clock gating.
    ///
      Data32 = 0xFFFFFFFF;

    RegOffset = 0x9424;
    MmioWrite32 (GttMmAdr + RegOffset, Data32);
    DEBUG ((DEBUG_INFO, "Enabled DOP clock gating \n"));

    ///
    /// 5c. Enable Unit Level Clock Gating
    ///
    for (LoopCounter = 0; LoopCounter < sizeof (gSaGtClockGatingRegisters) / sizeof (BOOT_SCRIPT_REGISTER_SETTING); ++LoopCounter) {
      RegOffset = gSaGtClockGatingRegisters[LoopCounter].Offset;
      Data32And = gSaGtClockGatingRegisters[LoopCounter].AndMask;
      Data32Or = gSaGtClockGatingRegisters[LoopCounter].OrMask;

      MmioAndThenOr32 (GttMmAdr + RegOffset, Data32And, Data32Or);
    }

    ///
    /// 5h. Disable LLC open poll.
    ///
    RegOffset = 0x9044;
    Data32 = 0xC0000000;
    MmioWrite32 (GttMmAdr + RegOffset, Data32);
    ///
    /// 6. RC6 Settings and 7. Enable Idle Messages from all *CS
    ///
    for (LoopCounter = 0; LoopCounter < sizeof (gSaGtRC6Registers) / sizeof (BOOT_SCRIPT_REGISTER_SETTING); ++LoopCounter) {
      RegOffset = gSaGtRC6Registers[LoopCounter].Offset;
      Data32And = gSaGtRC6Registers[LoopCounter].AndMask;
      Data32Or = gSaGtRC6Registers[LoopCounter].OrMask;

      MmioAndThenOr32 (GttMmAdr + RegOffset, Data32And, Data32Or);
    }
    ///
    /// 8a. Program GT Normal Frequency Request
    ///
    Data32 = 0x06000000;
    MmioWrite32 (GttMmAdr + 0xA008, Data32);

    ///
    /// 8b. RP Control.
    ///
    Data32 = 0x00000400;
    MmioWrite32 (GttMmAdr + 0xA024, Data32);

    ///
    /// 8c. Lock all SPC registers.
    ///
    ///
    /// Set slice and subslice steering to valid slices before reading SPC registers
    ///
    //
    // Reading the first enabled Subslice. There are maximum 6 Subslice for Gen12 i.e. DSS0 to DSS5 and hence
    // need to check bit0 to bit5.
    //
      RegOffset = 0x913C;
      Data32 = MmioRead32 (GttMmAdr + RegOffset) & 0x3F;
      for (Ssid = 0; Ssid < MAX_SUBSLICE_COUNT; Ssid++) {
        if ((Data32 & ((UINT32) 0x1 << Ssid)) ==  ((UINT32) DSS_ENABLED << Ssid ))
          break;
      }

      if (Ssid == MAX_SUBSLICE_COUNT)
      {
        DEBUG ((DEBUG_ERROR, "ERROR: None of subslice enabled, treating as fatal error !!!\n"));
        ASSERT(FALSE);
      }

      DEBUG ((DEBUG_INFO, "DSS Enable Read 0x%x Ssid 0x%x\n", MmioRead32 (GttMmAdr + RegOffset), Ssid));
      ///
      /// Sublice Slice 0 disabled, set 0xFDC[subslice select] = 1
      ///
      RegOffset = 0x0FDC;
      Data32And = (UINT32) ~(BIT26 | BIT25 | BIT24);
      Data32Or  = (UINT32) (Ssid << 24);
      MmioAndThenOr32 (GttMmAdr + RegOffset, Data32And, Data32Or);

      Data32Or = 0x80000000;
      for (LoopCounter = 0; LoopCounter < sizeof(gSpcLock) / sizeof(UINT32); ++LoopCounter) {
        RegOffset = gSpcLock[LoopCounter];
        MmioOr32(GttMmAdr + RegOffset, Data32Or);
      }

      ///
      /// Set Lock bit
      ///
    MmioAndThenOr32 (GttMmAdr + R_SA_GTTMMADR_GAMW_ECO_BUS_RW_IA , (UINT32) ~(B_SA_GTTMMADR_CFG_FENCE_LOCK_BIT), (B_SA_GTTMMADR_CFG_FENCE_LOCK_BIT));

    ///
    /// 9. Enabling to enter RC6 state in idle mode.
    ///
    Data32 = 0;
    if (GtConfig->RenderStandby && GtConfig->PmSupport) {
      RegOffset = 0xA094;
      Data32 = 0x00040000;
      MmioWrite32 (GttMmAdr + RegOffset, Data32);
      DEBUG ((DEBUG_INFO, "Entered RC6 state in idle mode\n"));
    }

      ///
      /// 10a. Clear offset 0xA188 [31:0] to clear the force wake enable
      ///
      RegOffset = 0xA188;
      Data32 = 0x00010000;
      MmioWrite32 (GttMmAdr + RegOffset, Data32);

      ///
      /// 10b. Poll until clearing is cleared to verify the force wake acknowledge.
      ///
      RegOffset = 0x130044;
      Data32Mask = BIT0;
      Result = 0;
      PollGtReady (GttMmAdr, RegOffset, Data32Mask, Result);
  }

  DEBUG ((DEBUG_INFO, "GT PM Init Done. Exiting.\n"));
  return EFI_SUCCESS;
}

/**
  Program PSMI registers.

  @param[in] GRAPHICS_PEI_CONFIG             *GtConfig
  @param[in] GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig

  @retval EFI_SUCCESS     - PSMI registers programmed.
**/
EFI_STATUS
ProgramPsmiRegs (
  IN       GRAPHICS_PEI_CONFIG             *GtConfig,
  IN       GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig
  )
{
  UINT64       McD0BaseAddress;
  UINT64       McD2BaseAddress;
  UINT64       PsmiBase;
  UINT32       *PsmiBaseAddr;
  UINT32       *PsmiLimitAddr;
  UINT64       PsmiLimit;
  UINT32       PavpMemSizeInMeg;
  UINT32       PsmiRegionSize;
  UINT32       GraphicsStolenSize;
  UINT32       PavpMemSize;
  UINT32       GttMmAdr;
  UINT32       GSMBase;

  McD0BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  McD2BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);

  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;

  if (GttMmAdr == 0) {
    GttMmAdr = GtPreMemConfig->GttMmAdr;
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR, (UINT32) (GttMmAdr & 0xFFFFFFFF));
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR + 4, 0);
  }

  if (!IgfxCmdRegEnabled()) {
    ///
    /// Enable Bus Initiator and Memory access on 0:2:0
    ///
    PciSegmentOr16 (McD2BaseAddress + PCI_COMMAND_OFFSET, (BIT2 | BIT1));
  }

  ///
  /// If device 0:2:0 (Internal Graphics Device, or GT) is not enabled, skip programming PSMI registers
  ///
  if ((PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) != 0xFFFF) && (GtPreMemConfig->GtPsmiSupport == 1)) {
    DEBUG ((DEBUG_INFO, "Programming PSMI Registers\n"));
    ///
    /// PsmiBase is GSM Base plus GSM Size.
    ///
    GSMBase = PciSegmentRead32 (McD0BaseAddress + R_SA_BDSM) & B_SA_BDSM_BDSM_MASK;

    if (GtPreMemConfig->IgdDvmt50PreAlloc < 240) {
      GraphicsStolenSize = (32 * GtPreMemConfig->IgdDvmt50PreAlloc) << 20;
    } else {
      GraphicsStolenSize = (4 * (GtPreMemConfig->IgdDvmt50PreAlloc - 239)) << 20;
    }
    PsmiBase = (UINT64) GSMBase + (UINT64) GraphicsStolenSize;
    PsmiBaseAddr = (UINT32 *) &PsmiBase;

    ///
    /// Psmi Limit is Psmibase plus Psmi size and subtract PAVP size.
    ///
    PavpMemSizeInMeg = 1 << ((MmioRead32 (GttMmAdr + R_SA_GTTMMADR_PAVPC_OFFSET) & B_SA_PAVPC_WOPCMSZ_MASK) >> 7);
    PavpMemSize = PavpMemSizeInMeg << 20;
    PsmiRegionSize = (32 + 256 * GtPreMemConfig->PsmiRegionSize) << 20;
    PsmiLimit = (UINT64) PsmiBase + (UINT64) PsmiRegionSize - PavpMemSize;
    PsmiLimitAddr = (UINT32 *) &PsmiLimit;
    ///
    /// Program PSMI Base and PSMI Limit and Lock.
    ///
    MmioWrite32 (GttMmAdr + R_SA_GTTMMADR_PSMIBASE_OFFSET + 4, *(PsmiBaseAddr + 1));
    MmioWrite32 (GttMmAdr + R_SA_GTTMMADR_PSMILIMIT_OFFSET + 4, *(PsmiLimitAddr + 1));
  }
  return EFI_SUCCESS;
}

/**
  Initialize PAVP feature of SystemAgent.

  @param[in] GRAPHICS_PEI_CONFIG             *GtConfig
  @param[in] SA_MISC_PEI_CONFIG              *MiscPeiConfig

  @retval EFI_SUCCESS     - PAVP initialization complete
  @retval EFI_UNSUPPORTED - iGFX is not present so PAVP not supported
**/
EFI_STATUS
PavpInit (
  IN       GRAPHICS_PEI_CONFIG             *GtConfig,
  IN       SA_MISC_PEI_CONFIG              *MiscPeiConfig,
  IN       GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig
  )
{

  UINT32       PcmBase;
  UINT64       McD0BaseAddress;
  UINT64       McD2BaseAddress;
  UINT32       Pavpc;
  UINT32       GttMmAdr;
  UINT32       GMSSizeSelector;
  UINT32       GMSSize;

  PcmBase            = 0;
  McD0BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  McD2BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  Pavpc              = PciSegmentRead32 (McD0BaseAddress + R_SA_PAVPC);

  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;
  if (GttMmAdr == 0) {
    GttMmAdr = GtPreMemConfig->GttMmAdr;
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR, (UINT32) (GttMmAdr & 0xFFFFFFFF));
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR + 4, 0);
  }

  if (!IgfxCmdRegEnabled()) {
    ///
    /// Enable Bus Initiator and Memory access on 0:2:0
    ///
    PciSegmentOr16 (McD2BaseAddress + PCI_COMMAND_OFFSET, (BIT2 | BIT1));
  }

  ///
  /// If device 0:2:0 (Internal Graphics Device, or GT) is not enabled, skip PAVP
  ///
  if (PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) != 0xFFFF) {
    GMSSizeSelector = PciSegmentRead32 (McD0BaseAddress + R_SA_GGC);
    GMSSizeSelector = (GMSSizeSelector & B_SA_GGC_GMS_MASK) >> N_SA_GGC_GMS_OFFSET;
    //
    // Graphics Stolen Size
    // Graphics Stolen size above 64MB has a granularity of 32MB increments
    // GMS values below 240 correspond to Sizes 32 * GSMValue
    // Graphics Stolen size below 64MB has a higher granularity and can be set in 4MB increments
    // GMS values ranging from 240-254 correspond to sizes 4MB to 60MB (excluding 32MB) which is 4*(GSMValue-239)
    //
    if (GMSSizeSelector < 240 ) {
      GMSSize = (UINT32) GMSSizeSelector * 32;
    } else {
      GMSSize = 4 * (GMSSizeSelector - 239);
    }
    DEBUG ((DEBUG_INFO, "Initializing PAVP\n"));
    Pavpc &= (UINT32) ~(B_SA_PAVPC_HVYMODSEL_MASK | B_SA_PAVPC_PCMBASE_MASK | B_SA_PAVPC_PAVPE_MASK | B_SA_PAVPC_PCME_MASK);
    Pavpc &= (UINT32) ~(BIT8 | BIT7);
    //
    // Program PCM Base and size = 2MB
    // PCMBase = DSM top - PCM size
    //
    PcmBase = ((UINT32) RShiftU64 ((PciSegmentRead32 (McD0BaseAddress + R_SA_BDSM)), 20)) + GMSSize - PAVP_PCM_SIZE_2_MB;
    Pavpc  |= (UINT32)(BIT7);

    Pavpc |= (UINT32) LShiftU64 (PcmBase, 20);
    if (GtConfig->PavpEnable == 1) {
      Pavpc |= B_SA_PAVPC_PAVPE_MASK;
    }
    Pavpc |= B_SA_PAVPC_PCME_MASK;

    Pavpc &= (UINT32) ~(BIT6);
  }
  ///
  /// Lock PAVPC Register
  ///
  Pavpc |= B_SA_PAVPC_PAVPLCK_MASK;
  PciSegmentWrite32 (McD0BaseAddress + R_SA_PAVPC, Pavpc);

  //
  // Program GT MMIO PAVPC Mirror register
  //
  MmioWrite32 (GttMmAdr + R_SA_GTTMMADR_PAVPC_OFFSET, Pavpc);
  PciSegmentWrite32 (McD2BaseAddress + R_SA_DEV2_PAVPC, Pavpc);
  return EFI_SUCCESS;
}

/**
Initialize GT Power management

  @param[in] GRAPHICS_PEI_PREMEM_CONFIG      GtPreMemConfig
  @param[in] GRAPHICS_PEI_CONFIG             GtConfig

  @retval EFI_SUCCESS          - GT Power management initialization complete
**/
EFI_STATUS
GraphicsPmInit (
  IN       GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig,
  IN       GRAPHICS_PEI_CONFIG             *GtConfig
  )
{
  UINT32                LoGTBaseAddress;
  UINT32                HiGTBaseAddress;
  UINT64                McD2BaseAddress;
  UINT32                GttMmAdr;
  UINT32                MchBarBase;
  UINT32                Msac;
  LARGE_INTEGER         GmAdrValue;

  DEBUG ((DEBUG_INFO, " iGfx Initialization start.\n"));

  GttMmAdr   = 0;
  MchBarBase = 0;
  Msac       = 0;
  MchBarBase = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, R_SA_MCHBAR));
  MchBarBase = MchBarBase & (UINT32) ~BIT0;

  ///
  /// If device 0:2:0 (Internal Graphics Device, or GT) is enabled, then Program GTTMMADR,
  ///
  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  if (PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) != 0xFFFF) {
    ///
    /// Program GT PM Settings if GttMmAdr allocation is Successful
    ///
    GttMmAdr                          = GtPreMemConfig->GttMmAdr;
    LoGTBaseAddress                   = (UINT32) (GttMmAdr & 0xFFFFFFFF);
    HiGTBaseAddress                   = 0;
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR, LoGTBaseAddress);
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR + 4, HiGTBaseAddress);

    Msac = PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_MSAC_OFFSET);
    PciSegmentAndThenOr32 (McD2BaseAddress + R_SA_IGD_MSAC_OFFSET, (UINT32)~(BIT20 + BIT19 + BIT18 + BIT17 + BIT16), SA_GT_APERTURE_SIZE_256MB << 16);

    GmAdrValue.Data = GtPreMemConfig->GmAdr64;
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GMADR, GmAdrValue.Data32.Low);
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GMADR + 4, GmAdrValue.Data32.High);

    if (!IgfxCmdRegEnabled()) {
      ///
      /// Enable Bus Initiator and Memory access on 0:2:0
      ///
      PciSegmentOr16 (McD2BaseAddress + PCI_COMMAND_OFFSET, (BIT2 | BIT1));
    }

    ///
    /// Program GT frequency for GT SKU's only (not applicable on GT0)
    /// Note: User requested frequency takes precedence than DisableTurboGt
    ///
    if (!IsDisplayOnlySku()) {
      if ((GtConfig->DisableTurboGt == 1) && (GtConfig->GtFreqMax == 0xFF)) {
        ///
        /// Read bits[15:8] and limit the GtFrequency to Rp1
        ///
        MmioWrite8 ((MchBarBase + 0x5994), (UINT8)(((MmioRead32 (MchBarBase + 0x5998)) >> 0x8) & 0xFF));
        DEBUG ((DEBUG_INFO, "Disabling Turbo Gt - Programmed to frequency (in units of 50MHz): 0x%x \n", MmioRead8 (MchBarBase + 0x5994)));
      } else if (GtConfig->GtFreqMax != 0xFF) {
        ///
        /// Program user requested GT frequency
        ///
        MmioWrite8 ((MchBarBase + 0x5994), (UINT8) GtConfig->GtFreqMax);
        DEBUG ((DEBUG_INFO, "Max frequency programmed by user in MchBar 0x5994 is (to be multiplied by 50 for MHz): 0x%x  \n", MmioRead8 (MchBarBase + 0x5994)));
      }
    }

    ///
    /// PmInit Initialization
    ///
    DEBUG ((DEBUG_INFO, "Initializing GT PowerManagement\n"));
    PmInit (GtConfig, GttMmAdr, MchBarBase);

    ///
    /// Program CD clock value based on Policy
    ///
    ProgramCdClkReg (GtConfig, GttMmAdr);

    ///
    /// Program Aperture Size MSAC register based on policy
    ///
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_MSAC_OFFSET, Msac);
  }
  DEBUG ((DEBUG_INFO, "iGfx Initialization end.\n"));
  return EFI_SUCCESS;
}

typedef union {
  struct {
    UINT32  Low;
    UINT32  High;
  } Data32;
  UINT64 Data;
} UINT64_STRUCT;

/**
  Mirror System Agent registers.

  @param[in] GtPreMemConfig  Instance of GRAPHICS_PEI_PREMEM_CONFIG

  @retval EFI_SUCCESS     - SA Register Mirroring complete
**/
EFI_STATUS
SaRegisterMirror (
  IN       GRAPHICS_PEI_PREMEM_CONFIG             *GtPreMemConfig
  )
{
  UINT32         GttMmAdr;
  UINT16         Data16;
  UINT32         Data32;
  UINT64_STRUCT  Data64;
  UINT64         McD0BaseAddress;
  UINT64         McD2BaseAddress;

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);

  if (PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_INFO, "\nSkip SA Register Mirror when iGFX is disabled \n"));
    return EFI_SUCCESS;
  }

  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;

  if (GttMmAdr == 0) {
    GttMmAdr = GtPreMemConfig->GttMmAdr;
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR, (UINT32) (GttMmAdr & 0xFFFFFFFF));
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR + 4, 0);
  }

  if (!IgfxCmdRegEnabled()) {
    ///
    /// Enable Bus Initiator and Memory access on 0:2:0
    ///
    PciSegmentOr16 (McD2BaseAddress + PCI_COMMAND_OFFSET, (BIT2 | BIT1));
  }

  // GGC
  DEBUG ((DEBUG_INFO, "Programming GGC\n"));
  Data16 = PciSegmentRead16 (McD0BaseAddress + R_SA_GGC);
  MmioWrite16 (GttMmAdr + R_SA_GTTMMADR_GGC_OFFSET, Data16);
  // TOUUD
  DEBUG ((DEBUG_INFO, "Programming TOUUD\n"));
  Data64.Data32.High = PciSegmentRead32 (McD0BaseAddress + R_SA_TOUUD + 4);
  Data64.Data32.Low  = PciSegmentRead32 (McD0BaseAddress + R_SA_TOUUD);
  MmioWrite64 (GttMmAdr + R_SA_GTTMMADR_TOUUD_OFFSET, Data64.Data);

  // TOLUD
  DEBUG ((DEBUG_INFO, "Programming TOLUD\n"));
  Data32 = PciSegmentRead32 (McD0BaseAddress + R_SA_TOLUD);
  MmioWrite32 (GttMmAdr + R_SA_GTTMMADR_TOLUD_OFFSET, Data32);

  // BDSM
  DEBUG ((DEBUG_INFO, "Programming BDSM\n"));
  Data64.Data = (UINT64) PciSegmentRead32 (McD0BaseAddress + R_SA_BDSM);
  DEBUG ((DEBUG_INFO, "Programming BDSM 0x%lx \n",Data64.Data ));
  MmioWrite64 (GttMmAdr + R_SA_GTTMMADR_BDSM_OFFSET, Data64.Data);

  // BGSM
  DEBUG ((DEBUG_INFO, "Programming BGSM\n"));
  Data64.Data = (UINT64) PciSegmentRead32 (McD0BaseAddress + R_SA_BGSM);
  DEBUG ((DEBUG_INFO, "Programming BGSM 0x%lx \n", Data64.Data));
  MmioWrite64 (GttMmAdr + R_SA_GTTMMADR_BGSM_OFFSET, Data64.Data);

  // TSEG
  DEBUG ((DEBUG_INFO, "Programming TSEG\n"));
  Data64.Data = (UINT64) PciSegmentRead32 (McD0BaseAddress + R_SA_TSEGMB);
  MmioWrite64 (GttMmAdr + R_SA_GTTMMADR_TSEGMB_OFFSET, Data64.Data);

  return EFI_SUCCESS;
}
