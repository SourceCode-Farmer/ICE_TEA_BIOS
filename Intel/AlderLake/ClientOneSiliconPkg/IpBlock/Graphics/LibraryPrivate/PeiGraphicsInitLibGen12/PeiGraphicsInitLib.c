/** @file
  PEIM to initialize IGFX.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/PeiServicesLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchPciBdfLib.h>
#include <IndustryStandard/Pci.h>
#include <Library/PeiGraphicsInitLib.h>
#include <Library/PeiDisplayInitLib.h>
#include <Library/SaInitLib.h>
#include <Library/TimerLib.h>
#include <CpuRegs.h>
#include <Library/CpuPlatformLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PchInfoLib.h>
#include <Library/PeiSaPolicyLib.h>
#include <Ppi/Spi.h>
#include <Library/PciExpressLib.h>
#include <Register/IgdRegs.h>
#include <Library/ThcLib.h>
#include <Library/TccLib.h>
#include <Library/GraphicsInfoFruLib.h>
#include <Library/MsrFruLib.h>

/**
  Check and Force Vdd On based on the H/W Status

  @param[in] GRAPHICS_PEI_PREMEM_CONFIG   GtPreMemConfig

  @retval EFI_NOT_READY                   H/W Power Cycle sequence in progress, Vdd on not enabled
  @retval EFI_SUCCESS                     Vdd On already enabled or enabled successfully

**/
EFI_STATUS
CheckAndForceVddOn (
  IN   GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig
  )
{
  UINTN                   GttMmAdr;
  LARGE_INTEGER           GmAdrValue;
  UINT64                  McD2BaseAddress;
  UINT32                  Msac;
  UINT64                  TimeTillNowinMilliSec;
  UINT64                  DeltaT12TimeMilliSec;
  EFI_STATUS              Status;
  EFI_BOOT_MODE           BootMode;

  ///
  /// Initialize local varibles.
  ///
  GttMmAdr = 0;
  Msac = 0;
  McD2BaseAddress = 0;
  TimeTillNowinMilliSec = 0;
  DeltaT12TimeMilliSec = 0;
  Status = EFI_SUCCESS;

  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  if (PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_INFO, "Returning from CheckAndForceVddOn since IGD is not present\n"));
    return EFI_SUCCESS;
  }

  ///
  /// Get the system Boot mode.
  //
  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

  if ((GtPreMemConfig->PanelPowerEnable == 1) && (BootMode != BOOT_ON_S3_RESUME) && (GtPreMemConfig->InternalGraphics != 0)) {
    Msac = PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_MSAC_OFFSET);
    ///
    /// Check if GttMmAdr has been already assigned, initialize if not
    ///
    GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;
    if (GttMmAdr == 0) {
      GttMmAdr = GtPreMemConfig->GttMmAdr;

      PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR, (UINT32)(GttMmAdr & 0xFF000000));
      GttMmAdr = (PciSegmentRead32(McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;
      PciSegmentAndThenOr32 (McD2BaseAddress + R_SA_IGD_MSAC_OFFSET, (UINT32)~(BIT20 + BIT19 + BIT18 + BIT17 + BIT16), SA_GT_APERTURE_SIZE_256MB << 16);
      GmAdrValue.Data = GtPreMemConfig->GmAdr64;
      PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GMADR, GmAdrValue.Data32.Low);
      PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GMADR + 4, GmAdrValue.Data32.High);
    }

    if (!IgfxCmdRegEnabled()) {
      ///
      /// Enable Bus Initiator and Memory access on 0:2:0
      ///
      PciSegmentOr16 (McD2BaseAddress + PCI_COMMAND_OFFSET, (BIT2 | BIT1));
    }

    ///
    /// Skip if VDD Bit is already set
    ///
    if (((GtPreMemConfig->DdiConfiguration.DdiPortAConfig == DdiPortEdp) && ((MmioRead32(GttMmAdr + R_SA_GTTMMADR_PP_CONTROL) & BIT3) == 0)) || \
        ((GtPreMemConfig->DdiConfiguration.DdiPortBConfig == DdiPortEdp) && ((MmioRead32(GttMmAdr + R_SA_GTTMMADR_PP_CONTROL_2) & BIT3) == 0))
        ) {
      if ((BootMode == BOOT_ASSUMING_NO_CONFIGURATION_CHANGES) || (GtPreMemConfig->OemT12DelayOverride)) {
        DeltaT12TimeMilliSec = (GtPreMemConfig->DeltaT12PowerCycleDelay);
        TimeTillNowinMilliSec =  DivU64x32 (GetTimeInNanoSecond (AsmReadTsc ()), 1000000);
        if ((DeltaT12TimeMilliSec > NO_DELAY) && (DeltaT12TimeMilliSec < MAX_DELAY) && (DeltaT12TimeMilliSec > TimeTillNowinMilliSec)) {
          ///
          /// Add delay based on policy value selected.
          ///
          DEBUG ((DEBUG_INFO, "Policy value based T12 Delay Added : %d ms\n", (DeltaT12TimeMilliSec - TimeTillNowinMilliSec)));
          MicroSecondDelay ((UINTN) (MultU64x32 ((DeltaT12TimeMilliSec - TimeTillNowinMilliSec), 1000)));
        } else if ((DeltaT12TimeMilliSec == MAX_DELAY) && (TOTAL_T12_TIME > TimeTillNowinMilliSec)) {
          ///
          /// Add maximum fixed delay of 500ms as per VESA standard.
          ///
          DEBUG ((DEBUG_INFO, "Fixed T12 Delay added after elapsed time : %d ms\n", (TOTAL_T12_TIME - TimeTillNowinMilliSec)));
          MicroSecondDelay ((UINTN) (MultU64x32 ((TOTAL_T12_TIME - TimeTillNowinMilliSec), 1000)));
        }
      }
      ///
      /// Enable Panel Power - VDD bit
      ///
      if (GtPreMemConfig->DdiConfiguration.DdiPortAConfig == DdiPortEdp) {
        DEBUG ((DEBUG_INFO, "Enable Panel Power Bit for eDP on DDI-A\n"));
        MmioOr32 (GttMmAdr + R_SA_GTTMMADR_PP_CONTROL, (UINT32) BIT3);
      }
      if (GtPreMemConfig->DdiConfiguration.DdiPortBConfig == DdiPortEdp) {
        DEBUG ((DEBUG_INFO, "Enable Panel Power Bit for eDP on DDI-B\n"));
        MmioOr32 (GttMmAdr + R_SA_GTTMMADR_PP_CONTROL_2, (UINT32) BIT3);
      }
    }
    ///
    /// Program Aperture Size MSAC register based on policy
    ///
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_MSAC_OFFSET, Msac);
  }

  return Status;
}

/**
  GraphicsInit: Initialize the IGD if no other external graphics is present

  @param[in] GtPreMemConfig       - GtPreMemConfig to access the GtPreMemConfig related information
  @param[in] PrimaryDisplay       - Primary Display - default is IGD
  @param[in, out] PegMmioLength   - Total IGFX MMIO length

**/
VOID
GraphicsInit (
  IN       GRAPHICS_PEI_PREMEM_CONFIG   *GtPreMemConfig,
  IN       DISPLAY_DEVICE               *PrimaryDisplay,
  IN       UINT32                       *IGfxMmioLength
  )
{
  UINT8                   GMSData;
  BOOLEAN                 IGfxSupported;
  EFI_STATUS              Status;
  UINTN                   GttMmAdr;
  UINT64                  McD0BaseAddress;
  UINT64                  McD2BaseAddress;
  UINT32                  Data32H;
  EFI_BOOT_MODE           BootMode;
  BOOLEAN                 IsMmioLengthInfoRequired;
  UINT32                  Data32;
  UINT32                  Data32Mask;
  LARGE_INTEGER           GmAdrValue;

  DEBUG ((DEBUG_INFO, "iGFX initialization start\n"));

  IsMmioLengthInfoRequired = FALSE;
  *IGfxMmioLength    = 0;
  GttMmAdr           = 0;
  McD0BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  McD2BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);

  ///
  /// Get the boot mode
  ///
  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

  ///
  /// Check if IGfx is supported
  ///
  IGfxSupported = (BOOLEAN) (PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) != 0xFFFF);
  if (!IGfxSupported) {
    DEBUG ((DEBUG_INFO, "iGFX is unsupported or disabled!\n"));
    AdditionalStepsForDisablingIgfx (GtPreMemConfig);
    return;
  }

  ///
  /// Temporarily program GttMmAdr
  ///
  GttMmAdr = GtPreMemConfig->GttMmAdr;
  if (GttMmAdr == 0) {
    DEBUG ((DEBUG_INFO, "Temporary GttMmAdr Bar is not initialized. Returning from GraphicsInit().\n"));
    return;
  }

  ///
  /// Program GttMmAdr
  /// set [23:0] = 0
  ///
  GttMmAdr = (UINT32) (GttMmAdr & 0xFF000000);
  PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR, GttMmAdr);
  GmAdrValue.Data = GtPreMemConfig->GmAdr64;
  PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GMADR, GmAdrValue.Data32.Low);
  PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GMADR + 4, GmAdrValue.Data32.High);
  DEBUG ((DEBUG_INFO, "GTBaseAddress 0X%x:\n", GttMmAdr));
  ///
  /// Read back the programmed GttMmAdr
  ///
  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;

  ///
  /// Enable Bus Initiator and Memory access on 0:2:0
  ///
  PciSegmentOr16 (McD2BaseAddress + PCI_COMMAND_OFFSET, (BIT2 | BIT1));

  ///
  /// If primary display device is IGD or no other display detected then enable IGD
  ///
  if (IGfxSupported &&
      (
        (
          ((*PrimaryDisplay == IGD) || (GtPreMemConfig->PrimaryDisplay == IGD)) &&
          (GtPreMemConfig->InternalGraphics != DISABLED)
          ) || (GtPreMemConfig->InternalGraphics == ENABLED)
        )
      ) {

    DEBUG ((DEBUG_INFO, "IGD enabled.\n"));


    ///
    /// Program GFX Memory
    ///
    GMSData = (UINT8) (GtPreMemConfig->IgdDvmt50PreAlloc);
    ///
    /// Description of GMS D0:F0:R50h[15:8]
    ///
    PciSegmentAnd16 (McD0BaseAddress + R_SA_GGC, (UINT16) ~(B_SA_GGC_GMS_MASK));
    PciSegmentOr16 (McD0BaseAddress + R_SA_GGC, (GMSData & 0xFF) << N_SA_GGC_GMS_OFFSET);
    ///
    /// Program Graphics GTT Memory D0:F0:R50h[7:6]
    ///   00b => 0MB of GTT
    ///   01b => 2MB of GTT
    ///   10b => 4MB of GTT
    ///   11b => 8MB of GTT
    ///
    if (GtPreMemConfig->GttSize != V_SA_GGC_GGMS_DIS) {
      ASSERT (GtPreMemConfig->GttSize <= 3);
      PciSegmentAndThenOr16 (McD0BaseAddress + R_SA_GGC, (UINT16) ~(B_SA_GGC_GGMS_MASK), (GtPreMemConfig->GttSize << N_SA_GGC_GGMS_OFFSET) & B_SA_GGC_GGMS_MASK);
    }
    ///
    /// Set register D2.F0.R 062h [4:0] = `00001b' to set a 256MByte aperture.
    /// This must be done before Device 2 registers are enumerated.
    ///
    ///
    /// Set register D2.F0.R 060h [20:16] = `00001b' to set a 256MByte aperture.
    /// This must be done before Device 2 registers are enumerated.
    ///
    PciSegmentAndThenOr32 (McD2BaseAddress + R_SA_IGD_MSAC_OFFSET, (UINT32)~(BIT20 + BIT19 + BIT18 + BIT17 + BIT16), SA_GT_APERTURE_SIZE_256MB << 16);

    ///
    /// Enable IGD VGA Decode.  This is needed so the Class Code will
    /// be correct for the IGD Device when determining which device
    /// should be primary.  If disabled, IGD will show up as a non VGA device.
    ///
    if ((GtPreMemConfig->PrimaryDisplay != IGD) && (*PrimaryDisplay != IGD)) {
      ///
      /// If IGD is forced to be enabled, but is a secondary display, disable IGD VGA Decode
      ///
      PciSegmentOr16 (McD0BaseAddress + R_SA_GGC, (UINT16) (B_SA_GGC_IVD_MASK | B_SA_GGC_VAMEN_MASK));
      DEBUG ((DEBUG_INFO, "IGD VGA Decode is disabled because it's not a primary display.\n"));
    } else {
      PciSegmentAnd16 (McD0BaseAddress + R_SA_GGC, (UINT16) ~(B_SA_GGC_IVD_MASK | B_SA_GGC_VAMEN_MASK));
    }

    ///
    /// Get Mmio length of iGFX later for dynamic TOLUD support
    ///
    IsMmioLengthInfoRequired = TRUE;

    ///
    /// Copy MSR_PLATFORM_INFO.SAMPLE_PART(FUSE_PROD_PART) bit to CONFIG0 Address D00h, bit 30.
    ///
    Data32 = IsSamplePart () ? BIT30 : 0;
    Data32Mask = BIT30;
    MmioAndThenOr32 (GttMmAdr + 0xD00, Data32Mask,Data32);
    DEBUG ((DEBUG_INFO, "Update CONFIG0 Address D00 : %x\n",MmioRead32 (GttMmAdr + 0xD00)));

    DEBUG((DEBUG_INFO, "Configuring iTouch Source Registers Doorbell and GSA_Touch \n"));

    ///
    /// Configure Doorbell Register 0x10c008 BDF bits[15:0] with Bus Device Function of DoorBell Source THC0
    /// Configure GSA_Touch Register 0x101078 BDF bits[31:16] with Bus Device Function of DoorBell Source THC0
    ///
    Data32H = MmioRead32 (GttMmAdr + R_SA_GTTMMADR_GTDOORBELL_OFFSET);
    Data32H = ((Data32H & 0xFFFF0000) | (PCI_EXPRESS_LIB_ADDRESS (0, ThcDevNumber (0), ThcFuncNumber (0), 0) >> 12));

    Data32 = MmioRead32 (GttMmAdr + R_SA_GTTMMADR_GSA_TOUCH_OFFSET);
    Data32 = ((Data32 & 0x0000FFFF) | (PCI_EXPRESS_LIB_ADDRESS (0, ThcDevNumber (0), ThcFuncNumber (0), 0) << 4));

    MmioWrite32 (GttMmAdr + R_SA_GTTMMADR_GTDOORBELL_OFFSET, Data32H);
    MmioWrite32 (GttMmAdr + R_SA_GTTMMADR_GSA_TOUCH_OFFSET, Data32);

    ///
    /// Configure GSA_Audio Register 0x101074 BDF bits[31:16] with Bus Device Function 0/31/3.
    ///
    Data32 = MmioRead32 (GttMmAdr + R_SA_GTTMMADR_GSA_AUDIO_OFFSET);
    Data32 = ((Data32 & 0x0000FFFF) | (PCI_EXPRESS_LIB_ADDRESS (GSA_AUDIO_BUS, GSA_AUDIO_DEV, GSA_AUDIO_FUN, 0) << 4)); // Program Bus 0, Device 31, Func 3

    MmioWrite32 (GttMmAdr + R_SA_GTTMMADR_GSA_AUDIO_OFFSET, Data32);
  } else {
   AdditionalStepsForDisablingIgfx(GtPreMemConfig);
  }

  ///
  /// Program Aperture Size MSAC register based on policy
  ///
  PciSegmentAndThenOr32 (McD2BaseAddress + R_SA_IGD_MSAC_OFFSET, (UINT32)~(BIT20 + BIT19 + BIT18 + BIT17 + BIT16), (UINT32)GtPreMemConfig->ApertureSize << 16);

  ///
  /// Get Mmio length of iGFX for dynamic TOLUD support
  ///
  if (IsMmioLengthInfoRequired) {
    FindPciDeviceMmioLength (IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, IGfxMmioLength);
  }

#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1

  //
  // Program GT COS Ways and Tags
  //
  if ((GtPreMemConfig->GtClosEnable == 1) && IsTccSku()) {
    // GT Cos Ways
    AsmMsrAndThenOr64 (MSR_GT_COS_WAYS_MASK0, ~((UINT64) B_MSR_GT_COS_WAYS_MASK), V_MSR_GT_COS_WAYS_1_WAYS);
    AsmMsrAndThenOr64 (MSR_GT_COS_WAYS_MASK1, ~((UINT64) B_MSR_GT_COS_WAYS_MASK), V_MSR_GT_COS_WAYS_1_WAYS);
    AsmMsrAndThenOr64 (MSR_GT_COS_WAYS_MASK2, ~((UINT64) B_MSR_GT_COS_WAYS_MASK), V_MSR_GT_COS_WAYS_1_WAYS);
    AsmMsrAndThenOr64 (MSR_GT_COS_WAYS_MASK3, ~((UINT64) B_MSR_GT_COS_WAYS_MASK), V_MSR_GT_COS_WAYS_1_WAYS);

    // GT Cos Tags Ways
    AsmMsrAndThenOr64 (MSR_GT_COS_TAG_WAYS_MASK0, ~((UINT64) B_MSR_GT_COS_TAG_WAYS_MASK), V_MSR_GT_COS_TAG_WAYS_1_WAYS);
    AsmMsrAndThenOr64 (MSR_GT_COS_TAG_WAYS_MASK1, ~((UINT64) B_MSR_GT_COS_TAG_WAYS_MASK), V_MSR_GT_COS_TAG_WAYS_1_WAYS);
    AsmMsrAndThenOr64 (MSR_GT_COS_TAG_WAYS_MASK2, ~((UINT64) B_MSR_GT_COS_TAG_WAYS_MASK), V_MSR_GT_COS_TAG_WAYS_1_WAYS);
    AsmMsrAndThenOr64 (MSR_GT_COS_TAG_WAYS_MASK3, ~((UINT64) B_MSR_GT_COS_TAG_WAYS_MASK), V_MSR_GT_COS_TAG_WAYS_1_WAYS);
  }

#endif

  DEBUG ((DEBUG_INFO, "iGFX initialization end\n"));
}

/**
  This function is to set Gfx Memory map (RC6 base, Doorbell base, Dfdbase etc)

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_NOT_FOUND           SiPreMemPolicyPpi not found
**/
EFI_STATUS
SetGfxMemMap (
  VOID
  )
{
  UINT32            GMSSizeSelector;
  UINT32            GMSSize;
  UINT32            GMSBase;
  UINT32            DoorbellCtxBaseLow;
  UINT32            DoorbellCtxBaseHigh;
  UINT32            Rc6CtxBaseLow;
  UINT32            Rc6CtxBaseHigh;
  UINT64            McD0BaseAddress;
  UINT64            McD2BaseAddress;
  UINT32            GttMmAdr;
  EFI_STATUS        Status;
  SI_PREMEM_POLICY_PPI        *SiPreMemPolicyPpi;
  GRAPHICS_PEI_PREMEM_CONFIG  *GtPreMemConfig;

  DEBUG ((DEBUG_INFO, "Set Gfx Memory Map\n"));

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);

  if (PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_INFO, "\nSkip SetGtMemMap when iGFX is disabled \n"));
    return EFI_SUCCESS;
  }

  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR (Status);
  if ((Status != EFI_SUCCESS) || (SiPreMemPolicyPpi == NULL)) {
    return EFI_NOT_FOUND;
  }

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gGraphicsPeiPreMemConfigGuid, (VOID *) &GtPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;

  if (GttMmAdr == 0) {
    GttMmAdr = GtPreMemConfig->GttMmAdr;
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR, (UINT32) (GttMmAdr & 0xFFFFFFFF));
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR + 4, 0);
  }

  if (!IgfxCmdRegEnabled()) {
    ///
    /// Enable Bus Initiator and Memory access on 0:2:0
    ///
    PciSegmentOr16 (McD2BaseAddress + PCI_COMMAND_OFFSET, (BIT2 | BIT1));
  }

  ///
  /// Calculate Context Base
  ///
  ///
  /// Must set to a physical address within GT stolen Memory WOPCM, at least 32KB from the top.
  /// This range must be coordinated with other uses (like GuC and PAVP);
  /// It is expected that the upper 24KB of stolen memory is the proper location
  /// Also set bit 2:0 to 111b
  ///
  GMSSizeSelector = PciSegmentRead32 (McD0BaseAddress + R_SA_GGC);
  GMSSizeSelector = (GMSSizeSelector & B_SA_GGC_GMS_MASK) >> N_SA_GGC_GMS_OFFSET;
  ///
  /// Graphics Stolen Size
  /// Graphics Stolen size above 64MB has a granularity of 32MB increments
  /// GMS values below 240 correspond to Sizes 32 * GSMValue
  /// Graphics Stolen size below 64MB has a higher granularity and can be set in 4MB increments
  /// GMS values ranging from 240-254 correspond to sizes 4MB to 60MB (excluding 32MB) which is 4*(GSMValue-239)
  ///
  if (GMSSizeSelector < 240 ) {
    GMSSize = (UINT32) GMSSizeSelector * 32;
  } else {
    GMSSize = 4 * (GMSSizeSelector - 239);
  }
  DEBUG ((DEBUG_INFO, "GMSSize: %dMB\n",GMSSize));
  GMSBase = PciSegmentRead32 (McD0BaseAddress + R_SA_BDSM) & B_SA_BDSM_BDSM_MASK;
  DEBUG ((DEBUG_INFO, "GMSBase read from R_SA_BDSM: 0x%x\n",GMSBase));

  ///
  /// The Doorbell Power Context Image lives in WOPCM.
  /// DoorbellCtxBase = DSM top - 4k
  /// RC6CXTBASE =  MDRBCTXBASE (DoorbellCtxBase) - 32KB
  ///
  DoorbellCtxBaseLow = (UINT32)(GMSBase + GMSSize * 0x100000 - 0x1000);
  Rc6CtxBaseLow = DoorbellCtxBaseLow - RC6CTXBASE_SIZE;

  DEBUG ((DEBUG_INFO, "Rc6CtxBaseLow: 0x%x\n", Rc6CtxBaseLow));
  DEBUG ((DEBUG_INFO, "DoorbellCtxBaseLow: 0x%x\n", DoorbellCtxBaseLow));

  ///
  /// Programming Doorbell Context base low. 0xDC8 [31:6] = Base[31:6], 0xDC8[0] = 1
  ///
  DoorbellCtxBaseLow &= 0xFFFFFFC0;
  DoorbellCtxBaseLow |= BIT0;
  DoorbellCtxBaseHigh = 0;
  MmioWrite32 (GttMmAdr + 0xDCC, DoorbellCtxBaseHigh);
  MmioWrite32 (GttMmAdr + 0xDC8, DoorbellCtxBaseLow);

  ///
  /// Programming RC6 Context Base
  ///
  Rc6CtxBaseLow |= BIT0;
  Rc6CtxBaseHigh = 0;
  DEBUG ((DEBUG_INFO, "RC6 Context Base: 0x%x\n", Rc6CtxBaseLow));
  MmioWrite32 (GttMmAdr + 0xD4C, Rc6CtxBaseHigh);
  MmioWrite32 (GttMmAdr + 0xD48, Rc6CtxBaseLow);
  DEBUG ((DEBUG_INFO, "RC6 Context Base Programming done \n"));
  if ((MmioRead32 (GttMmAdr + 0xD48) != Rc6CtxBaseLow) || (MmioRead32(GttMmAdr + 0xD4C) != Rc6CtxBaseHigh) || (MmioRead32(GttMmAdr + 0xDC8) != DoorbellCtxBaseLow) || (MmioRead32(GttMmAdr + 0xDCC) != DoorbellCtxBaseHigh)) {
    DEBUG ((EFI_D_ERROR,"Not able to configure DoorbellCtxBase and Rc6CtxBase due to some other agent is configuring and locking it before BIOS does.\n"));
    CpuDeadLoop ();
  }
  return EFI_SUCCESS;
}

/**
  UpdateAndLockGgcReg:  Program and lock graphic control register

  @param[in] DISPLAY_DEVICE              - Instance of DISPLAY_DEVICE
  @param[in] GRAPHICS_PEI_PREMEM_CONFIG  - Instance of GRAPHICS_PEI_PREMEM_CONFIG to access the GtPreMemConfig related information
**/
VOID
UpdateAndLockGgcReg (
  IN       DISPLAY_DEVICE               *PrimaryDisplay,
  IN       GRAPHICS_PEI_PREMEM_CONFIG   *GtPreMemConfig
)
{
  UINT64                     McD0BaseAddress;
  UINT64                     McD2BaseAddress;
  UINTN                      GttMmAdr;

  McD0BaseAddress   = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  McD2BaseAddress   = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;
  if ((GtPreMemConfig->PrimaryDisplay != IGD) && (*PrimaryDisplay != IGD) && (GtPreMemConfig->InternalGraphics == 1)) {
    ///
    /// If IGD is forced to be enabled, but is a secondary display, disable IGD VGA Decode
    ///
    MmioOr16 (GttMmAdr + R_SA_GTTMMADR_GGC_OFFSET, (UINT16)(B_SA_GGC_IVD_MASK | B_SA_GGC_VAMEN_MASK));
    PciSegmentOr16 (McD0BaseAddress + R_SA_GGC, (UINT16)(B_SA_GGC_IVD_MASK | B_SA_GGC_VAMEN_MASK));
    DEBUG ((DEBUG_INFO, "IGD VGA Decode is disabled because it's not a primary display.\n"));
  }
  //
  // Lock Graphic Control register
  //
  PciSegmentOr32 (McD0BaseAddress + R_SA_GGC, B_SA_GGC_GGCLCK_MASK);
  MmioOr32 (GttMmAdr + R_SA_GTTMMADR_GGC_OFFSET, B_SA_GGC_GGCLCK_MASK);
}
