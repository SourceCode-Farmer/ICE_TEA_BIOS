/** @file
  This file contains VER4 specific GPIO information

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/BaseLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/GpioLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/PchInfoLib.h>
#include <Library/SataSocLib.h>
#include <Library/GpioPrivateLib.h>
#include <Library/GpioNativePads.h>
#include <Library/GpioHelpersLib.h>
#include <Pins/GpioPinsVer4S.h>
#include <Register/GpioRegs.h>
#include <Register/IshRegs.h>

#include "GpioNativePrivateLibInternal.h"

/**
  This function provides GPIO Native Pad for a given native function

  @param[in]  PadFunction            PadFunction for a specific native signal. Please refer to GpioNativePads.h

  @retval     NativePad              GPIO pad with encoded native function
**/
GPIO_NATIVE_PAD
GpioGetNativePadByFunction (
  IN  UINT32                      PadFunction
  )
{
  if (IsPchS()) {
    switch (PadFunction) {
      case GPIO_FUNCTION_SERIAL_IO_UART_RX(0):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C8,  1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_RX(1):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C12, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_RX(2):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C20, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_RX(3):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D20, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_TX(0):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C9,  1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_TX(1):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C13, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_TX(2):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C21, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_TX(3):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D21, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_RTS(0):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C10, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_RTS(1):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C14, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_RTS(2):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C22, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_RTS(3):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D22, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_CTS(0):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C11, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_CTS(1):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C15, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_CTS(2):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C23, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_UART_CTS(3):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D23, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_MOSI (0):  return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I18, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_MOSI (1):  return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I22, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_MOSI (2):  return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R15, 3, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_MOSI (3):  return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D19, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_MISO (0):  return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I17, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_MISO (1):  return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I21, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_MISO (2):  return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R14, 3, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_MISO (3):  return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D18, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CLK (0):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I16, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CLK (1):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I20, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CLK (2):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R13, 3, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CLK (3):   return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D17, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CS (0, 0): return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I15, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CS (0, 1): return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I9,  1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CS (1, 0): return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I19, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CS (1, 1): return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I10, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CS (2, 0): return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R12, 3, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CS (2, 1): return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_B1,  2, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_SPI_CS (3, 0): return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D16, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SCL(0):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C17, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SCL(1):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C19, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SCL(2):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C4,  3, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SCL(3):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C7,  2, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SCL(4):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I12, 2, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SCL(5):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I14, 2, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SDA(0):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C16, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SDA(1):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C18, 1, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SDA(2):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C3,  3, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SDA(3):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C6,  2, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SDA(4):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I11, 2, PadFunction);
      case GPIO_FUNCTION_SERIAL_IO_I2C_SDA(5):    return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I13, 2, PadFunction);
      case GPIO_FUNCTION_ISH_SPI_MOSI(0):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R15, 1, PadFunction);
      case GPIO_FUNCTION_ISH_SPI_MISO(0):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R14, 1, PadFunction);
      case GPIO_FUNCTION_ISH_SPI_CLK(0):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R13, 1, PadFunction);
      case GPIO_FUNCTION_ISH_SPI_CS (0, 0):       return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R12, 1, PadFunction);
      case GPIO_FUNCTION_ISH_GP(0):               return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_B8,  1, PadFunction);
      case GPIO_FUNCTION_ISH_GP(1):               return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_B9,  1, PadFunction);
      case GPIO_FUNCTION_ISH_GP(2):               return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_B10, 1, PadFunction);
      case GPIO_FUNCTION_ISH_GP(3):               return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_B15, 1, PadFunction);
      case GPIO_FUNCTION_ISH_GP(4):               return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_B16, 1, PadFunction);
      case GPIO_FUNCTION_ISH_GP(5):               return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_B17, 1, PadFunction);
      case GPIO_FUNCTION_ISH_GP(6):               return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_B5,  2, PadFunction);
      case GPIO_FUNCTION_ISH_GP(7):               return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_B7,  1, PadFunction);
      case GPIO_FUNCTION_ISH_UART_RX(0):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C3,  1, PadFunction);
      case GPIO_FUNCTION_ISH_UART_RX(1):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C12, 2, PadFunction);
      case GPIO_FUNCTION_ISH_UART_TX(0):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C4,  1, PadFunction);
      case GPIO_FUNCTION_ISH_UART_TX(1):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C13, 2, PadFunction);
      case GPIO_FUNCTION_ISH_UART_RTS(0):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_B1,  1, PadFunction);
      case GPIO_FUNCTION_ISH_UART_RTS(1):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C14, 2, PadFunction);
      case GPIO_FUNCTION_ISH_UART_CTS(0):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_E21, 1, PadFunction);
      case GPIO_FUNCTION_ISH_UART_CTS(1):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C15, 2, PadFunction);
      case GPIO_FUNCTION_ISH_I2C_SCL(0):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H20, 1, PadFunction);
      case GPIO_FUNCTION_ISH_I2C_SCL(1):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H22, 1, PadFunction);
      case GPIO_FUNCTION_ISH_I2C_SCL(2):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C7,  1, PadFunction);
      case GPIO_FUNCTION_ISH_I2C_SDA(0):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H19, 1, PadFunction);
      case GPIO_FUNCTION_ISH_I2C_SDA(1):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H21, 1, PadFunction);
      case GPIO_FUNCTION_ISH_I2C_SDA(2):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_C6,  1, PadFunction);
      case GPIO_FUNCTION_HDA_BCLK:                return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R0,  1, PadFunction);
      case GPIO_FUNCTION_HDA_RSTB:                return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R4,  1, PadFunction);
      case GPIO_FUNCTION_HDA_SYNC:                return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R1,  1, PadFunction);
      case GPIO_FUNCTION_HDA_SDO:                 return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R2,  1, PadFunction);
      case GPIO_FUNCTION_HDA_SDI_0:               return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R3,  1, PadFunction);
      case GPIO_FUNCTION_HDA_SDI_1:               return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R5,  1, PadFunction);
      case GPIO_FUNCTION_DDSP_HPD(1):             return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I1,  1, PadFunction);
      case GPIO_FUNCTION_DDSP_HPD(2):             return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I2,  1, PadFunction);
      case GPIO_FUNCTION_DDSP_HPD(3):             return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I3,  1, PadFunction);
      case GPIO_FUNCTION_DDSP_HPD(4):             return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I4,  1, PadFunction);
      case GPIO_FUNCTION_DDSP_HPD('A'):           return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R9,  1, PadFunction);
      case GPIO_FUNCTION_DDSP_HPD('B'):           return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R10, 1, PadFunction);
      case GPIO_FUNCTION_DDSP_HPD('C'):           return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R11, 1, PadFunction);
      case GPIO_FUNCTION_PANEL_AVDD_EN(0):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_F19, 1, PadFunction);
      case GPIO_FUNCTION_PANEL_AVDD_EN(1):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_R11, 2, PadFunction);
      case GPIO_FUNCTION_PANEL_BKLTEN(0):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_F20, 1, PadFunction);
      case GPIO_FUNCTION_PANEL_BKLTEN(1):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I7,  1, PadFunction);
      case GPIO_FUNCTION_PANEL_BKLTCTL(0):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_F21, 1, PadFunction);
      case GPIO_FUNCTION_PANEL_BKLTCTL(1):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_I8,  1, PadFunction);
      case GPIO_FUNCTION_CNVI_RF_RESET:           return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D5,  2, PadFunction);
      case GPIO_FUNCTION_CNVI_CLKREQ:             return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D6,  3, PadFunction);
      case GPIO_FUNCTION_DMIC_CLKA(0):            return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_S6,  2, PadFunction);
      case GPIO_FUNCTION_DMIC_CLKA(1):            return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_S4,  2, PadFunction);
      case GPIO_FUNCTION_DMIC_CLKB(0):            return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_S2,  2, PadFunction);
      case GPIO_FUNCTION_DMIC_CLKB(1):            return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_S3,  2, PadFunction);
      case GPIO_FUNCTION_DMIC_DATA(0):            return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_S7,  2, PadFunction);
      case GPIO_FUNCTION_DMIC_DATA(1):            return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_S5,  2, PadFunction);
      case GPIO_FUNCTION_THC_SPI_INT(0):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_E20, 2, PadFunction);
      case GPIO_FUNCTION_THC_SPI_INT(1):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D23, 2, PadFunction);
      case GPIO_FUNCTION_THC_CLK_LOOPBACK(0):     return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_SPI1_THC0_CLK_LOOPBK, 2, PadFunction);
      case GPIO_FUNCTION_THC_CLK_LOOPBACK(1):     return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GSPI3_THC1_CLK_LOOPBK, 2, PadFunction);
      case GPIO_FUNCTION_SATA_DEVSLP(0,0):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_E4, 1, PadFunction);
      case GPIO_FUNCTION_SATA_DEVSLP(0,1):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_E5, 1, PadFunction);
      case GPIO_FUNCTION_SATA_DEVSLP(0,2):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_E6, 1, PadFunction);
      case GPIO_FUNCTION_SATA_DEVSLP(0,3):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_F5, 1, PadFunction);
      case GPIO_FUNCTION_SATA_DEVSLP(0,4):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_F6, 1, PadFunction);
      case GPIO_FUNCTION_SATA_DEVSLP(0,5):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_F7, 1, PadFunction);
      case GPIO_FUNCTION_SATA_DEVSLP(0,6):        return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_F9, 1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(0):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D0,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(1):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D1,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(2):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D2,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(3):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D3,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(4):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D11, 1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(5):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D12, 1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(6):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D13, 1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(7):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_D14, 1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(8):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H2,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(9):          return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H3,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(10):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H4,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(11):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H5,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(12):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H6,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(13):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H7,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(14):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H8,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(15):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H9,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(16):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_J8,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(17):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_J9,  1, PadFunction);
      case GPIO_FUNCTION_PCIE_CLKREQ(18):         return GPIO_NATIVE_PAD_DEF (GPIO_VER4_S_GPP_H0,  1, PadFunction);
      default: break;
    }
  }
  return GPIO_PAD_NONE;
}

/**
  This function provides recommended GPIO IO Standby configuration for a given native function

  @param[in]  PadFunction            PadFunction for a specific native signal. Please refer to GpioNativePads.h
  @param[out] StandbyState           IO Standby State for specified native function
  @param[out] StandbyTerm            IO Standby Termination for specified native function

  @retval Status
**/
EFI_STATUS
GpioGetFunctionIoStandbyConfig (
  IN  UINT32                PadFunction,
  OUT GPIO_IOSTANDBY_STATE  *StandbyState,
  OUT GPIO_IOSTANDBY_TERM   *StandbyTerm
  )
{
  //
  // Io Standby feature is not supported on PCH-S
  //
  if (IsPchS ()) {
    return EFI_UNSUPPORTED;
  }

  return EFI_NOT_FOUND;
}


/**
  This function provides SCS SD CARD detect pin

  @retval GpioPin             SD CARD Detect pin
**/
GPIO_PAD
GpioGetScsSdCardDetectPin (
  VOID
  )
{
  ASSERT (FALSE);
  return 0;
}

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSHdaSspInterfaceGpio[][PCH_GPIO_HDA_SSP_NUMBER_OF_PINS] =
{
  { // SSP0/I2S0
    {GPIO_VER4_S_GPP_R0,  GpioPadModeNative2},// I2S0_SCLK
    {GPIO_VER4_S_GPP_R1,  GpioPadModeNative2},// I2S0_SFRM
    {GPIO_VER4_S_GPP_R2,  GpioPadModeNative2},// I2S0_TXD
    {GPIO_VER4_S_GPP_R3,  GpioPadModeNative2} // I2S0_RXD
  },
  { // SSP1/I2S1
    {GPIO_VER4_S_GPP_R8,  GpioPadModeNative2},// I2S1_SCLK
    {GPIO_VER4_S_GPP_R7,  GpioPadModeNative2},// I2S1_SFRM
    {GPIO_VER4_S_GPP_R6,  GpioPadModeNative2},// I2S1_TXD
    {GPIO_VER4_S_GPP_R5,  GpioPadModeNative2} // I2S1_RXD
  },
  { // SSP2/I2S2
    {GPIO_VER4_S_GPP_D8,  GpioPadModeNative1},// I2S2_SCLK
    {GPIO_VER4_S_GPP_D5,  GpioPadModeNative1},// I2S2_SFRM
    {GPIO_VER4_S_GPP_D6,  GpioPadModeNative1},// I2S2_TXD
    {GPIO_VER4_S_GPP_D7,  GpioPadModeNative1} // I2S2_RXD
  }
};

/**
  This function provides SSP/I2S interface pins

  @param[in]  SspInterfaceNumber       SSP/I2S interface

  @param[out] NativePinsTable          Table with pins
**/
VOID
GpioGetHdaSspPins (
  IN  UINT32                      SspInterfaceNumber,
  OUT GPIO_PAD_NATIVE_FUNCTION    **NativePinsTable
  )
{
  if (IsPchS ()) {
    if (SspInterfaceNumber < ARRAY_SIZE (mPchSHdaSspInterfaceGpio)) {
      *NativePinsTable = mPchSHdaSspInterfaceGpio[SspInterfaceNumber];
      return;
    }
  }
  *NativePinsTable = NULL;
  ASSERT (FALSE);
}

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSHdaSspMasterClockGpio[] =
{
    {GPIO_VER4_S_GPP_B11, GpioPadModeNative1} // I2S_MCLK
};

/**
  This function sets HDA SSP Master Clock into native mode

  @param[in]  MclkIndex       MCLK index

  @retval Status
**/
EFI_STATUS
GpioEnableHdaSspMasterClock (
  IN UINT32  MclkIndex
  )
{
  if (GpioOverrideLevel1Enabled ()) {
    return EFI_SUCCESS;
  }

  if (IsPchS ()) {
    if (MclkIndex < ARRAY_SIZE (mPchSHdaSspMasterClockGpio)) {
      return GpioSetPadMode (mPchSHdaSspMasterClockGpio[MclkIndex].Pad, mPchSHdaSspMasterClockGpio[MclkIndex].Mode);
    }
  }

  return EFI_UNSUPPORTED;
}

//
// GPIO pins for HD Audio SoundWire interface [SNDW number][pin: CLK/DATA]
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSHdaSndwGpio[][PCH_GPIO_HDA_SNDW_NUMBER_OF_PINS] =
{
  { // SNDW1
    {GPIO_VER4_S_GPP_S0, GpioPadModeNative1},// SNDW1_CLK
    {GPIO_VER4_S_GPP_S1, GpioPadModeNative1} // SNDW1_DATA
  },
  { // SNDW2
    {GPIO_VER4_S_GPP_S2, GpioPadModeNative1},// SNDW2_CLK
    {GPIO_VER4_S_GPP_S3, GpioPadModeNative1} // SNDW2_DATA
  },
  { // SNDW3
    {GPIO_VER4_S_GPP_S4, GpioPadModeNative1},// SNDW3_CLK
    {GPIO_VER4_S_GPP_S5, GpioPadModeNative1} // SNDW3_DATA
  },
  { // SNDW4
    {GPIO_VER4_S_GPP_S6, GpioPadModeNative1},// SNDW4_CLK
    {GPIO_VER4_S_GPP_S7, GpioPadModeNative1} // SNDW4_DATA
  }
};

/**
  This function provides SNDW interface pins

  @param[in]  SndwInterfaceNumber      SNDWx interface number

  @param[out] NativePinsTable          Table with pins
**/
VOID
GpioGetHdaSndwPins (
  IN  UINT32                      SndwInterfaceNumber,
  OUT GPIO_PAD_NATIVE_FUNCTION    **NativePinsTable
  )
{
  if (IsPchS ()) {
    if (SndwInterfaceNumber < ARRAY_SIZE (mPchSHdaSndwGpio)) {
      *NativePinsTable = mPchSHdaSndwGpio[SndwInterfaceNumber];
      return;
    }
  }
  *NativePinsTable = NULL;
  ASSERT (FALSE);
}

//
// GPIO SPI IO pins for Touch Host Controller [SPI1 or SPI2][pin: IO_0/IO_1/IO_2/IO_3]
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSThcSpiGpioIo[][4] =
{
  {
    {GPIO_VER4_S_GPP_E16,  GpioPadModeNative2}, // SPI1_IO_0
    {GPIO_VER4_S_GPP_E15,  GpioPadModeNative2}, // SPI1_IO_1
    {GPIO_VER4_S_GPP_E17,  GpioPadModeNative2}, // SPI1_IO_2
    {GPIO_VER4_S_GPP_E18,  GpioPadModeNative2}  // SPI1_IO_3
  },
  {
    {GPIO_VER4_S_GPP_D18, GpioPadModeNative2}, // SPI2_IO_0
    {GPIO_VER4_S_GPP_D19, GpioPadModeNative2}, // SPI2_IO_1
    {GPIO_VER4_S_GPP_D20, GpioPadModeNative2}, // SPI2_IO_2
    {GPIO_VER4_S_GPP_D21, GpioPadModeNative2}  // SPI2_IO_3
  }
};

/**
  This function provides SPI IO pin for Touch Host Controller

  @param[in]  SpiIndex                  SPI1 or SPI2 - 0 or 1
  @param[in]  IoIndex                   IoIndex Valid from 0 (SPI_IO_0) to 3 (SPI_IO_3)

  @retval     NativePin                 Native Pin Configuration, 0 if SpiIndex or IoIndex is invalid
**/
GPIO_PAD_NATIVE_FUNCTION
GpioGetThcSpiIo (
  IN  UINT32                      SpiIndex,
  IN  UINT32                      IoIndex
  )
{
  if (IsPchS ()) {
    if (SpiIndex < ARRAY_SIZE (mPchSThcSpiGpioIo) && IoIndex < ARRAY_SIZE (mPchSThcSpiGpioIo[SpiIndex])) {
      return mPchSThcSpiGpioIo[SpiIndex][IoIndex];
    }
  }

  ASSERT (FALSE);
  return (GPIO_PAD_NATIVE_FUNCTION){0};
}

/**
  This function provides SPI ChipSelect pin for Touch Host Controller

  @param[in]  SpiIndex                  SPI1 or SPI2 - 0 or 1

  @retval     NativePin                 Native Pin Configuration, 0 if SpiIndex is invalid
**/
GPIO_PAD_NATIVE_FUNCTION
GpioGetThcSpiCs (
  IN  UINT32                      SpiIndex
  )
{
  if (IsPchS ()) {
    if (SpiIndex == 0) {
      return (GPIO_PAD_NATIVE_FUNCTION) {GPIO_VER4_S_GPP_E13,  GpioPadModeNative2}; // SPI1_CSB
    } else if (SpiIndex == 1) {
      return (GPIO_PAD_NATIVE_FUNCTION) {GPIO_VER4_S_GPP_D16,  GpioPadModeNative2}; // SPI2_CSB
    }
  }

  ASSERT (FALSE);
  return (GPIO_PAD_NATIVE_FUNCTION){0};
}

/**
  This function provides SPI Clock pin for Touch Host Controller

  @param[in]  SpiIndex                  SPI1 or SPI2 - 0 or 1

  @retval     NativePin                 Native Pin Configuration, 0 if SpiIndex is invalid
**/
GPIO_PAD_NATIVE_FUNCTION
GpioGetThcSpiClk (
  IN  UINT32                      SpiIndex
  )
{
  if (IsPchS ()) {
    if (SpiIndex == 0) {
      return (GPIO_PAD_NATIVE_FUNCTION) {GPIO_VER4_S_GPP_E14,  GpioPadModeNative2}; // SPI1_CLK
    } else if (SpiIndex == 1) {
      return (GPIO_PAD_NATIVE_FUNCTION) {GPIO_VER4_S_GPP_D17,  GpioPadModeNative2}; // SPI2_CLK
    }
  }

  ASSERT (FALSE);
  return (GPIO_PAD_NATIVE_FUNCTION){0};
}

/**
  This function provides SPI Reset pin for Touch Host Controller

  @param[in]  SpiIndex                  SPI1 or SPI2 - 0 or 1

  @retval     NativePin                 Native Pin Configuration, 0 if SpiIndex is invalid
**/
GPIO_PAD_NATIVE_FUNCTION
GpioGetThcSpiReset (
  IN  UINT32                      SpiIndex
  )
{
  if (IsPchS ()) {
    if (SpiIndex == 0) {
      return (GPIO_PAD_NATIVE_FUNCTION) {GPIO_VER4_S_GPP_E19,  GpioPadModeNative2}; // SPI1_RESETB
    } else if (SpiIndex == 1) {
      return (GPIO_PAD_NATIVE_FUNCTION) {GPIO_VER4_S_GPP_D22,  GpioPadModeNative2}; // SPI2_RESETB
    }
  }

  ASSERT (FALSE);
  return (GPIO_PAD_NATIVE_FUNCTION){0};
}

//
// GPIO pins for SMBUS
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSSmbusGpio[PCH_GPIO_SMBUS_NUMBER_OF_PINS] =
{
  {GPIO_VER4_S_GPP_C0, GpioPadModeNative1},// SMB_CLK
  {GPIO_VER4_S_GPP_C1, GpioPadModeNative1} // SMB_DATA
};

/**
  This function provides SMBUS interface pins

  @param[out] NativePinsTable          Table with pins
**/
VOID
GpioGetSmbusPins (
  OUT GPIO_PAD_NATIVE_FUNCTION    **NativePinsTable
  )
{
  *NativePinsTable = mPchSSmbusGpio;
  return;
 }

//
// SMBUS Alert pin
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSSmbusAlertGpio = {GPIO_VER4_S_GPP_C2,  GpioPadModeNative1};

/**
  This function sets SMBUS ALERT pin into native mode

  @retval Status
**/
EFI_STATUS
GpioEnableSmbusAlert (
  VOID
  )
{
  GPIO_PAD_NATIVE_FUNCTION SmbusAlertGpio;

  if (GpioOverrideLevel1Enabled ()) {
    return EFI_SUCCESS;
  }

  SmbusAlertGpio = mPchSSmbusAlertGpio;

  return GpioSetPadMode (SmbusAlertGpio.Pad, SmbusAlertGpio.Mode);
}

/**
  This function provides Serial GPIO pins

  @param[in]  SataCtrlIndex       SATA controller index
  @param[out] SgpioPins           SATA Serial GPIO pins
**/
VOID
GpioGetSataSgpioPins (
  IN  UINT32        SataCtrlIndex,
  OUT SGPIO_PINS    *SgpioPins
  )
{
  GPIO_PAD_NATIVE_FUNCTION TempGpioPadNativeFunction = {0, 0};

  SgpioPins->Sclock = TempGpioPadNativeFunction;
  SgpioPins->Sload = TempGpioPadNativeFunction;
  SgpioPins->Sdataout = TempGpioPadNativeFunction;
  ASSERT (FALSE);
  return;
}

//
// SATA reset port to GPIO pin mapping
// SATAGP_x -> GPIO pin y
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSSataGpToGpioMap[] =
{
  {GPIO_VER4_S_GPP_E0,  GpioPadModeNative2},
  {GPIO_VER4_S_GPP_E1,  GpioPadModeNative2},
  {GPIO_VER4_S_GPP_E2,  GpioPadModeNative2},
  {GPIO_VER4_S_GPP_F0,  GpioPadModeNative2},
  {GPIO_VER4_S_GPP_F1,  GpioPadModeNative2},
  {GPIO_VER4_S_GPP_F2,  GpioPadModeNative2},
  {GPIO_VER4_S_GPP_F3,  GpioPadModeNative2},
  {GPIO_VER4_S_GPP_F4,  GpioPadModeNative2}
};

/**
  This function provides SATA GP pin data

  @param[in]  SataCtrlIndex       SATA controller index
  @param[in]  SataPort            SATA port number
  @param[out] NativePin           SATA GP pin
**/
VOID
GpioGetSataGpPin (
  IN  UINT32                    SataCtrlIndex,
  IN  UINTN                     SataPort,
  OUT GPIO_PAD_NATIVE_FUNCTION  *NativePin
  )
{
  if (IsPchS ()) {
    if (SataPort < ARRAY_SIZE (mPchSSataGpToGpioMap)) {
      *NativePin = mPchSSataGpToGpioMap[SataPort];
      return;
    }
  }
  *NativePin = (GPIO_PAD_NATIVE_FUNCTION){0};
  ASSERT (FALSE);
}

//
// SATA LED pin
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSSataLedGpio = {GPIO_VER4_S_GPP_E8, GpioPadModeNative1};

/**
  This function sets SATA LED pin into native mode. SATA LED indicates
  SATA controller activity

  @param[in]  SataCtrlIndex       SATA controller index
  @retval     Status
**/
EFI_STATUS
GpioEnableSataLed (
  IN  UINT32                    SataCtrlIndex
  )
{
  GPIO_PAD_NATIVE_FUNCTION  SataLedGpio;

  if (GpioOverrideLevel1Enabled ()) {
    return EFI_SUCCESS;
  }

  if (IsPchS ()) {
    SataLedGpio = mPchSSataLedGpio;
  } else {
    ASSERT (FALSE);
    return EFI_UNSUPPORTED;
  }

  return GpioSetPadMode (SataLedGpio.Pad, SataLedGpio.Mode);
}

//
// USB2 OC pins
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSUsbOcGpioPins[] =
{
  {GPIO_VER4_S_GPP_E9,  GpioPadModeNative1},// USB_OC_0
  {GPIO_VER4_S_GPP_E10, GpioPadModeNative1},// USB_OC_1
  {GPIO_VER4_S_GPP_E11, GpioPadModeNative1},// USB_OC_2
  {GPIO_VER4_S_GPP_E12, GpioPadModeNative1},// USB_OC_3
  {GPIO_VER4_S_GPP_I11, GpioPadModeNative1},// USB_OC_4
  {GPIO_VER4_S_GPP_I12, GpioPadModeNative1},// USB_OC_5
  {GPIO_VER4_S_GPP_I13, GpioPadModeNative1},// USB_OC_6
  {GPIO_VER4_S_GPP_I14, GpioPadModeNative1} // USB_OC_7
};

/**
  This procedure will set Native Function IOSF-SB Virtual Wire Message Generation bit
  in DW0 of requested GPIO Pad

  @param[in] GPIO_PAD   GpioPad
**/
VOID
GpioSetNafVweBit (
  IN CONST GPIO_PAD PadCfg
  )
{
    DEBUG ((DEBUG_INFO, "GpioSetNafVweBit \n"));
    UINT32   PadCfgRegValue;

    PadCfgRegValue = GpioReadPadCfgReg (PadCfg, 0);
    PadCfgRegValue |= B_PCH_GPIO_NAF_VWE;
    GpioWritePadCfgReg (
      PadCfg,
      0,
      (UINT32)~B_PCH_GPIO_NAF_VWE,
      PadCfgRegValue
      );
}

/**
  This function enables USB OverCurrent pins by setting
  USB2 OCB pins into native mode

  @param[in]  OcPinNumber            USB OC pin number

  @retval Status
**/
EFI_STATUS
GpioEnableUsbOverCurrent (
  IN  UINTN   OcPinNumber
  )
{
  GPIO_PAD_NATIVE_FUNCTION  OcGpio;

  if (GpioOverrideLevel1Enabled ()) {
    return EFI_SUCCESS;
  }

  if (OcPinNumber >= ARRAY_SIZE (mPchSUsbOcGpioPins)) {
    ASSERT(FALSE);
    return EFI_UNSUPPORTED;
  }
  OcGpio = mPchSUsbOcGpioPins[OcPinNumber];

  return GpioSetPadMode (OcGpio.Pad, OcGpio.Mode);
}

//
// PCHHOTB pin
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSPchHotbPin = {GPIO_VER4_S_GPP_B23,  GpioPadModeNative2};

/**
  This function sets PCHHOT pin into native mode

  @retval Status
**/
EFI_STATUS
GpioEnablePchHot (
  VOID
  )
{
  GPIO_PAD_NATIVE_FUNCTION PchHotbPin;

  if (GpioOverrideLevel1Enabled ()) {
    return EFI_SUCCESS;
  }

  PchHotbPin = mPchSPchHotbPin;

  return GpioSetPadMode (PchHotbPin.Pad, PchHotbPin.Mode);
}

//
// VRALERTB pin
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSVrAlertbPin = {GPIO_VER4_S_GPP_B2, GpioPadModeNative1};

/**
  This function sets VRALERTB pin into native mode

  @retval Status
**/
EFI_STATUS
GpioEnableVrAlert (
  VOID
  )
{
  GPIO_PAD_NATIVE_FUNCTION  VrAlertGpio;

  if (GpioOverrideLevel1Enabled ()) {
    return EFI_SUCCESS;
  }

  VrAlertGpio = mPchSVrAlertbPin;

  return GpioSetPadMode (VrAlertGpio.Pad, VrAlertGpio.Mode);
}

//
// CPU_C10_GATE pin
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSCpuC10GatePin = {GPIO_VER4_S_GPP_J1, GpioPadModeNative1};

/**
  This function sets CPU C10 Gate pins into native mode

  @retval Status
**/
EFI_STATUS
GpioEnableCpuC10GatePin (
  VOID
  )
{
  GPIO_PAD_NATIVE_FUNCTION  CpuC10GateGpio;

  if (GpioOverrideLevel1Enabled ()) {
    return EFI_SUCCESS;
  }

  CpuC10GateGpio = mPchSCpuC10GatePin;

  return GpioSetPadMode (CpuC10GateGpio.Pad, CpuC10GateGpio.Mode);
}

//
// CPU GP pins
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSCpuGpPinMap[PCH_GPIO_CPU_GP_NUMBER_OF_PINS] =
{
  {GPIO_VER4_S_GPP_E3, GpioPadModeNative1}, // CPU_GP_0
  {GPIO_VER4_S_GPP_E7, GpioPadModeNative1}, // CPU_GP_1
  {GPIO_VER4_S_GPP_B3, GpioPadModeNative1}, // CPU_GP_2
  {GPIO_VER4_S_GPP_B4, GpioPadModeNative1}  // CPU_GP_3
};

/**
  This function sets CPU GP pins into native mode

  @param[in]  CpuGpPinNum               CPU GP pin number

  @retval Status
**/
EFI_STATUS
GpioEnableCpuGpPin (
  IN  UINT32                            CpuGpPinNum
  )
{
  GPIO_PAD_NATIVE_FUNCTION CpuGpPin;

  if (GpioOverrideLevel1Enabled ()) {
    return EFI_SUCCESS;
  }

  if (CpuGpPinNum >= ARRAY_SIZE (mPchSCpuGpPinMap)) {
    ASSERT (FALSE);
    return EFI_UNSUPPORTED;
  }
  CpuGpPin = mPchSCpuGpPinMap[CpuGpPinNum];

  return GpioSetPadMode (CpuGpPin.Pad, CpuGpPin.Mode);
}

//
// DDP1/2/3/4/A/B/C  CTRLCLK and CTRLDATA pins
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSDdpInterfacePins[][PCH_GPIO_DDP_NUMBER_OF_PINS] =
{
  {// DDP1
    {GPIO_VER4_S_GPP_R16, GpioPadModeNative1},// DDP1_CTRLCLK
    {GPIO_VER4_S_GPP_R17, GpioPadModeNative1} // DDP1_CTRLDATA
  },
  {// DDP2
    {GPIO_VER4_S_GPP_R18, GpioPadModeNative1},// DDP2_CTRLCLK
    {GPIO_VER4_S_GPP_R19, GpioPadModeNative1} // DDP2_CTRLDATA
  },
  {// DDP3
    {GPIO_VER4_S_GPP_R12,  GpioPadModeNative2},// DDP3_CTRLCLK
    {GPIO_VER4_S_GPP_R13, GpioPadModeNative2} // DDP3_CTRLDATA
  },
  {// DDP4
    {GPIO_VER4_S_GPP_R14, GpioPadModeNative2},// DDP4_CTRLCLK
    {GPIO_VER4_S_GPP_R15, GpioPadModeNative2} // DDP4_CTRLDATA
  },
  {// DDPA
    {GPIO_VER4_S_GPP_R20, GpioPadModeNative1},// DDPA_CTRLCLK
    {GPIO_VER4_S_GPP_R21, GpioPadModeNative1} // DDPA_CTRLDATA
  },
  {// DDPB
    {GPIO_VER4_S_GPP_I5, GpioPadModeNative1},// DDPB_CTRLCLK
    {GPIO_VER4_S_GPP_I6, GpioPadModeNative1} // DDPB_CTRLDATA
  },
  {// DDPC
    {GPIO_VER4_S_GPP_I7, GpioPadModeNative2},// DDPC_CTRLCLK
    {GPIO_VER4_S_GPP_I8, GpioPadModeNative2} // DDPC_CTRLDATA
  }
};

/**
  This function provides DDPx interface pins

  @param[in]  DdpInterface   DDPx interface

  @param[out] NativePinsTable          Table with pins
**/
VOID
GpioGetDdpPins (
  IN  GPIO_DDP                    DdpInterface,
  OUT GPIO_PAD_NATIVE_FUNCTION    **NativePinsTable
  )
{
  UINT32  DdpInterfaceIndex;

  switch (DdpInterface) {
    case GpioDdp1:
    case GpioDdp2:
    case GpioDdp3:
    case GpioDdp4:
      if (IsPchS ()) {
        DdpInterfaceIndex = DdpInterface - GpioDdp1;
      } else {
        goto Error;
      }
      break;
    case GpioDdpA:
    case GpioDdpB:
    case GpioDdpC:
      if (IsPchS ()) {
        DdpInterfaceIndex = (DdpInterface - GpioDdpA) + 4;
      } else {
        goto Error;
      }
      break;
    default:
      goto Error;
  }

  if (IsPchS()) {
    if (DdpInterfaceIndex < ARRAY_SIZE (mPchSDdpInterfacePins)) {
      *NativePinsTable = mPchSDdpInterfacePins[DdpInterfaceIndex];
      return;
    }
  }
Error:
  *NativePinsTable = NULL;
  ASSERT(FALSE);
}

/**
  This function provides CNVi BT interface select pin

  @retval GpioPad          GPIO pad for CNVi BT interface select
**/
GPIO_PAD
GpioGetCnviBtIfSelectPin (
  VOID
  )
{
  if (IsPchS ()) {
    return GPIO_VER4_S_VGPIO5;
  } else {
    ASSERT (FALSE);
    return 0;
  }
}

/**
  This function provides CNVi BT Charging pin

  @retval GpioPad          GPIO pad for CNVi BT Charging select
**/
GPIO_PAD
GpioGetCnviBtChargingPin (
  VOID
  )
{
  ASSERT (FALSE);
  return 0;
}

/**
  This function provides CNVi A4WP pin

  @param[out] GpioNativePad       GPIO native pad for CNVi A4WP
**/
VOID
GpioGetCnviA4WpPin (
  OUT GPIO_PAD_NATIVE_FUNCTION  *GpioNativePad
  )
{
  ASSERT (FALSE);
  *GpioNativePad = (GPIO_PAD_NATIVE_FUNCTION){0};
}

/**
  This function provides CNVi BT host wake int pin

  @retval GpioPad          GPIO pad for BT host wake int
**/
GPIO_PAD
GpioGetCnviBtHostWakeIntPin (
  VOID
  )
{
  if (IsPchS ()) {
    return GPIO_VER4_S_VGPIO4;
  } else {
    ASSERT (FALSE);
    return 0;
  }
}

/**
  This function provides CNVi CRF presence strap pin

  @retval GpioPad  GPIO pad for CRF strap pin
**/
GPIO_PAD
GpioGetCnviCrfStrapPin (
  VOID
  )
{
  //
  // CNV_RGI_DT pin
  //
  if (IsPchS ()) {
    return GPIO_VER4_S_GPP_J4;
  } else {
    ASSERT (FALSE);
    return 0;
  }
}

//
// TBT LSX 0/1/2/3  RX and TX pins
//
/**
  This function sets TBT_LSx pin into native mode

  @param[in]  TbtLsxDdiPort     TBT_LSx for DDI Port Number

  @retval     Status
**/
EFI_STATUS
GpioEnableTbtLsxInterface (
  IN GPIO_TBT_LSX  TbtLsxDdiPort
  )
{
  ASSERT(FALSE);
  return EFI_UNSUPPORTED;
}

//
// CNVi Bluetooth UART pads
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD mPchSVCnviBtUartGpioPad[PCH_GPIO_CNVI_UART_NUMBER_OF_PINS] =
{
  GPIO_VER4_S_VGPIO6, // vCNV_BT_UART_TXD
  GPIO_VER4_S_VGPIO7, // vCNV_BT_UART_RXD
  GPIO_VER4_S_VGPIO8, // vCNV_BT_UART_CTS_B
  GPIO_VER4_S_VGPIO9  // vCNV_BT_UART_RTS_B
};

//
// vUART for Bluetooth
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD mPchSVUartForCnviBtGpioPad[PCH_GPIO_CNVI_UART_NUMBER_OF_PINS] =
{
  GPIO_VER4_S_VGPIO18, // vUART0_TXD
  GPIO_VER4_S_VGPIO19, // vUART0_RXD
  GPIO_VER4_S_VGPIO20, // vUART0_CTS_B
  GPIO_VER4_S_VGPIO21  // vUART0_RTS_B
};

/**
  This function provides CNVi BT UART pins

  @param[in]  ConnectionType           CNVi BT UART connection type
  @param[out] VCnviBtUartPad           Table with vCNV_BT_UARTx pads
  @param[out] VCnviBtUartPadMode       vCNV_BT_UARTx pad mode
  @param[out] VUartForCnviBtPad        Table with vUART0 pads
  @param[out] VUartForCnviBtPadMode    vUART0 pad mode
**/
VOID
GpioGetCnviBtUartPins (
  IN  VGPIO_CNVI_BT_UART_CONNECTION_TYPE  ConnectionType,
  OUT GPIO_PAD                            **VCnviBtUartPad,
  OUT GPIO_PAD_MODE                       *VCnviBtUartPadMode,
  OUT GPIO_PAD                            **VUartForCnviBtPad,
  OUT GPIO_PAD_MODE                       *VUartForCnviBtPadMode
  )
{
  if (IsPchS ()) {
    *VCnviBtUartPad = mPchSVCnviBtUartGpioPad;
    *VUartForCnviBtPad = mPchSVUartForCnviBtGpioPad;
  } else {
    ASSERT (FALSE);
    *VCnviBtUartPad = NULL;
    *VUartForCnviBtPad = NULL;
    return;
  }

  switch (ConnectionType) {
    case GpioCnviBtUartToSerialIoUart0:
      *VCnviBtUartPadMode = GpioPadModeNative1;
      *VUartForCnviBtPadMode = GpioPadModeNative1;
      break;
    case GpioCnviBtUartToIshUart0:
      *VCnviBtUartPadMode = GpioPadModeNative2;
      *VUartForCnviBtPadMode = GpioPadModeNative1;
      break;
    case GpioCnviBtUartNotConnected:
    case GpioCnviBtUartToExternalPads:
      *VCnviBtUartPadMode = GpioPadModeGpio;
      *VUartForCnviBtPadMode = GpioPadModeGpio;
      break;
    default:
      ASSERT (FALSE);
      return;
  }
}

/**
  This function provides CNVi BT UART external pads

  @param[out] NativePinsTable          Table with pins
**/
VOID
GpioGetCnviBtUartExternalPins (
  OUT GPIO_PAD_NATIVE_FUNCTION **NativePinsTable
  )
{
  ASSERT (FALSE);
  return;
}

//
// CNVi Bluetooth I2S pads
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD mPchSVCnviBtI2sGpioPad[PCH_GPIO_CNVI_SSP_NUMBER_OF_PINS] =
{
  GPIO_VER4_S_VGPIO30, // vCNV_BT_I2S_BCLK
  GPIO_VER4_S_VGPIO31, // vCNV_BT_I2S_WS_SYNC
  GPIO_VER4_S_VGPIO32, // vCNV_BT_I2S_SDO
  GPIO_VER4_S_VGPIO33  // vCNV_BT_I2S_SDI
};

//
// vSSP for Bluetooth
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD mPchSVSspForCnviBtGpioPad[PCH_GPIO_CNVI_SSP_NUMBER_OF_PINS] =
{
  GPIO_VER4_S_VGPIO34, // vSSP2_SCLK
  GPIO_VER4_S_VGPIO35, // vSSP2_SFRM
  GPIO_VER4_S_VGPIO36, // vSSP2_TXD
  GPIO_VER4_S_VGPIO37  // vSSP2_RXD
};

/**
  This function provides CNVi BT I2S pins

  @param[in]  ConnectionType          CNVi BT I2S connection type
  @param[in]  ConnectionIndex         CNVi BT I2S connection index
  @param[out] VCnviBtI2sPad           Table with vCNV_BT_I2Sx pads
  @param[out] VCnviBtI2sPadMode       vCNV_BT_I2Sx pad mode
  @param[out] VSspForCnviBtPad        Table with vSSP2 pads
  @param[out] VSspForCnviBtPadMode    vSSP2 pad mode
**/
VOID
GpioGetCnviBtI2sPins (
  IN  VGPIO_CNVI_BT_I2S_CONNECTION_TYPE  ConnectionType,
  IN  UINT32                   ConnectionIndex,
  OUT GPIO_PAD                 **VCnviBtI2sPad,
  OUT GPIO_PAD_MODE            *VCnviBtI2sPadMode,
  OUT GPIO_PAD                 **VSspForCnviBtPad,
  OUT GPIO_PAD_MODE            *VSspForCnviBtPadMode
  )
{
  if (ConnectionIndex != 0) {
    ASSERT (FALSE);
    *VCnviBtI2sPad = NULL;
    *VSspForCnviBtPad = NULL;
    return;
  }

  if (IsPchS ()) {
    *VCnviBtI2sPad = mPchSVCnviBtI2sGpioPad;
    *VSspForCnviBtPad = mPchSVSspForCnviBtGpioPad;
  } else {
    ASSERT (FALSE);
    *VCnviBtI2sPad = NULL;
    *VSspForCnviBtPad = NULL;
    return;
  }

  switch (ConnectionType) {
    case GpioCnviBtI2sToSsp0:
      *VCnviBtI2sPadMode = GpioPadModeNative1;
      *VSspForCnviBtPadMode = GpioPadModeNative1;
      break;
    case GpioCnviBtI2sToSsp1:
      *VCnviBtI2sPadMode = GpioPadModeNative2;
      *VSspForCnviBtPadMode = GpioPadModeNative1;
      break;
    case GpioCnviBtI2sToSsp2:
      *VCnviBtI2sPadMode = GpioPadModeNative3;
      *VSspForCnviBtPadMode = GpioPadModeNative1;
      break;
    case GpioCnviBtI2sNotConnected:
    case GpioCnviBtI2sToExternalPads:
      *VCnviBtI2sPadMode = GpioPadModeGpio;
      *VSspForCnviBtPadMode = GpioPadModeGpio;
      break;
    default:
      ASSERT (FALSE);
      return;
  }
}

/**
  This function provides CNVi BT I2S external pads

  @param[out] NativePinsTable          Table with pins
**/
VOID
GpioGetCnviBtI2sExternalPins (
  OUT GPIO_PAD_NATIVE_FUNCTION **NativePinsTable
  )
{
  ASSERT (FALSE);
  return;
}

//
// CNVi MFUART1 pads
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD mPchSVCnviMfUart1GpioPad[PCH_GPIO_CNVI_UART_NUMBER_OF_PINS] =
{
  GPIO_VER4_S_VGPIO10, // vCNV_MFUART1_TXD
  GPIO_VER4_S_VGPIO11, // vCNV_MFUART1_RXD
  GPIO_VER4_S_VGPIO12, // vCNV_MFUART1_CTS_B
  GPIO_VER4_S_VGPIO13  // vCNV_MFUART1_RTS_B
};

//
// vUART for MFUART1
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD mPchSVUartForCnviMfUart1GpioPad[PCH_GPIO_CNVI_UART_NUMBER_OF_PINS] =
{
  GPIO_VER4_S_VGPIO22, // vISH_UART0_TXD
  GPIO_VER4_S_VGPIO23, // vISH_UART0_RXD
  GPIO_VER4_S_VGPIO24, // vISH_UART0_CTS_B
  GPIO_VER4_S_VGPIO25  // vISH_UART0_RTS_B
};

/**
  This function provides CNVi MFUART1 pins

  @param[in]  ConnectionType          CNVi MFUART1 connection type
  @param[out] VCnviBtI2sPad           Table with vCNV_MFUART1x pads
  @param[out] VCnviBtI2sPadMode       vCNV_MFUART1x pad mode
  @param[out] VSspForCnviBtPad        Table with vISH_UART0 pads
  @param[out] VSspForCnviBtPadMode    vISH_UART0 pad mode
**/
VOID
GpioGetCnviMfUart1Pins (
  IN  VGPIO_CNVI_MF_UART1_CONNECTION_TYPE  ConnectionType,
  OUT GPIO_PAD                 **VCnviMfUart1Pad,
  OUT GPIO_PAD_MODE            *VCnviMfUart1PadMode,
  OUT GPIO_PAD                 **VUartForCnviMfUart1Pad,
  OUT GPIO_PAD_MODE            *VUartForCnviMfUart1PadMode
  )
{
  if (IsPchS ()) {
    *VCnviMfUart1Pad = mPchSVCnviMfUart1GpioPad;
    *VUartForCnviMfUart1Pad = mPchSVUartForCnviMfUart1GpioPad;
  } else {
    ASSERT (FALSE);
    *VCnviMfUart1Pad = NULL;
    *VUartForCnviMfUart1Pad = NULL;
    return;
  }

  switch (ConnectionType) {
    case GpioCnviMfUart1ToSerialIoUart2:
      *VCnviMfUart1PadMode = GpioPadModeNative2;
      *VUartForCnviMfUart1PadMode = GpioPadModeNative1;
      break;
    case GpioCnviMfUart1ToIshUart0:
      *VCnviMfUart1PadMode = GpioPadModeNative1;
      *VUartForCnviMfUart1PadMode = GpioPadModeNative1;
      break;
    case GpioCnviMfUart1NotConnected:
    case GpioCnviMfUart1ToExternalPads:
      *VCnviMfUart1PadMode = GpioPadModeGpio;
      *VUartForCnviMfUart1PadMode = GpioPadModeGpio;
      break;
    default:
      ASSERT (FALSE);
      return;
  }
}

/**
  This function provides CNVi MFUART1 external pads

  @param[out] NativePinsTable          Table with pins
**/
VOID
GpioGetCnviMfUart1ExternalPins (
  OUT GPIO_PAD_NATIVE_FUNCTION **NativePinsTable
  )
{
  ASSERT (FALSE);
  return;
}

/**
  This function provides CNVi Bluetooth Enable pad

  @retval GpioPad           CNVi Bluetooth Enable pad
**/
GPIO_PAD
GpioGetCnviBtEnablePin (
  VOID
  )
{
  if (IsPchS ()) {
    return GPIO_VER4_S_VGPIO0;
  } else {
    ASSERT (FALSE);
    return 0;
  }
}

//
// CNVi BRI(Bluetooth Radio Interface) and RGI(Radio Generic Interface) buses from PCH to CRF(Companion RF) module
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSCnviBriRgiGpioPad[PCH_GPIO_CNVI_BRI_RGI_NUMBER_OF_PINS] =
{
  {GPIO_VER4_S_GPP_J2,  GpioPadModeNative1}, // CNV_BRI_DT
  {GPIO_VER4_S_GPP_J3,  GpioPadModeNative1}, // CNV_BRI_RSP
  {GPIO_VER4_S_GPP_J4,  GpioPadModeNative1}, // CNV_RGI_DT
  {GPIO_VER4_S_GPP_J5,  GpioPadModeNative1}  // CNV_RGI_RSP
};

/**
  This function provides CNVi BRI RGI GPIO pads

  @param[out] NativePinsTable          Table with pins
**/
VOID
GpioGetCnvBriRgiPins (
  OUT GPIO_PAD_NATIVE_FUNCTION **NativePinsTable
  )
{
  if (IsPchS ()) {
    *NativePinsTable = mPchSCnviBriRgiGpioPad;
  } else {
    ASSERT (FALSE);
    return;
  }
}


GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSImguClkOutGpioPad[] =
{
  {GPIO_VER4_S_GPP_A14,   GpioPadModeNative1}, // IMGCLKOUT_0
  {GPIO_VER4_S_GPP_B0,    GpioPadModeNative1}  // IMGCLKOUT_1
};

/**
  This function provides IMGCLKOUT pins

  @param[out] NativePinsTable          Table with pins
  @param[out] NoOfNativePins            Number of pins
**/
VOID
GpioGetImgClkOutPins (
  OUT GPIO_PAD_NATIVE_FUNCTION **NativePinsTable,
  OUT UINT32                   *NoOfNativePins
  )
{
  *NativePinsTable = mPchSImguClkOutGpioPad;
  *NoOfNativePins = ARRAY_SIZE (mPchSImguClkOutGpioPad);
}

/**
  This function provides PWRBTN pin

  @retval GpioPad          PWRTBTN pin
**/
GPIO_PAD
GpioGetPwrBtnPin (
  VOID
  )
{
  return GPIO_VER4_S_GPD3;
}

/**
  The function sets VCCIOSEL

  @param[in]  GpioPad             GPIO pad
  @param[in]  VccioSel            Pad voltage

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_UNSUPPORTED         The Pin is owned by others
  @retval EFI_INVALID_PARAMETER   Invalid group or parameter
**/
EFI_STATUS
GpioSetVccLevel (
  IN  GPIO_PAD        GpioPad,
  IN  GPIO_VCCIO_SEL  VccioSel
  )
{
  if (VccioSel >= MaxVccioSel) {
    ASSERT (FALSE);
    return EFI_INVALID_PARAMETER;
  }

  if (!GpioIsPadValid (GpioPad)) {
    ASSERT (FALSE);
    return EFI_INVALID_PARAMETER;
  }

  if (!GpioIsPadHostOwned (GpioPad)) {
    return EFI_UNSUPPORTED;
  }

  GpioWritePadCfgReg (
    GpioPad,
    2,
    (UINT32)~B_GPIO_PCR_VCCIOSEL,
    VccioSel << N_GPIO_PCR_VCCIOSEL
    );
  return EFI_SUCCESS;
}

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD mPchSTypeCSbuGpioPad[] =
{
  GPIO_VER4_S_GPP_J10,  // BSSB_LS_RX
  GPIO_VER4_S_GPP_J11,  // BSSB_LS_TX
};

/**
  SBU (Sideband use) pins are used as auxiliary signals for Type C connector,
  which are hard-wired to BSSB_LS natively for debug function.
  when USB-C is enabled and debug not needed, disable pins (BSSB) used for debug through TypeC connector,
  program SBU pins to high-Z/open circuit per USB-C spec.

  @param[in]  UsbTcPortEnBitmap   USB Type C port enabled bitmap

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_UNSUPPORTED         SBU pads are not supported
  @retval EFI_INVALID_PARAMETER   Invalid input parameter
**/
EFI_STATUS
GpioDisableTypeCSbuDebug (
  IN UINT32           UsbTcPortEnBitmap
  )
{
  GPIO_PAD            *GpioSbuPadTable;
  UINT32              SbuPadNum;
  GPIO_CONFIG         PadConfig;
  UINT32              Index;
  UINT32              PortIndex;

  GpioSbuPadTable   = NULL;
  Index             = 0;
  PortIndex         = 0;
  ZeroMem (&PadConfig, sizeof (GPIO_CONFIG));

  if (HighBitSet32 (UsbTcPortEnBitmap) + 1 > GetPchMaxTypeCPortNum ()) { // check if MSB exceeds max port number
    return EFI_INVALID_PARAMETER;
  }

  if (IsPchS()) {
    GpioSbuPadTable = mPchSTypeCSbuGpioPad;
    SbuPadNum = ARRAY_SIZE (mPchSTypeCSbuGpioPad) / 2;
  } else {
    return EFI_UNSUPPORTED;
  }
  //
  // For USB-C SBU function usage, the pins must be routed to a Type-C connector.
  // BIOS needs to configure the pin to high-Z/open circuit (select GPIO function with TX disable) if debug is disabled,
  // that would guarantees PCH GPIO is not driving to external device.
  // Having 3.3V setting in high-Z/open circuit mode is just a precaution to protect the GPIO in case the external device is driving into PCH.
  //
  for (PortIndex = 0; PortIndex < SbuPadNum ; PortIndex++) {
    if (UsbTcPortEnBitmap & (BIT0 << PortIndex)) {
      for (Index = 0; Index < 2; Index++) {
        PadConfig.PadMode   = GpioPadModeGpio;                // Set GPIO moode
        PadConfig.Direction = GpioDirNone;                    // Set High-Z
        GpioSetVccLevel (GpioSbuPadTable[PortIndex * 2 + Index], GpioVcc3v3); // Set VCCIO to High voltage
        GpioSetPadConfig (GpioSbuPadTable[PortIndex * 2 + Index], &PadConfig);
      }
    }
  }
  return EFI_SUCCESS;
}

/**
  When 2-wire DCI OOB is connected via SBU from Type C port, need set IO Standby state to masked (to operate as if no standby signal asserted)
  to remain connection in low power state.

  @param[in]     DCI connection port ID

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_UNSUPPORTED         SBU pads are not supported
  @retval EFI_INVALID_PARAMETER   Invalid input parameter
**/
EFI_STATUS
Gpio2WireDciOobSetting (
  IN UINT8               DciPortId
  )
{
  GPIO_PAD            *GpioSbuPadTable;
  UINT32              SbuPadNum;
  UINT32              Index;
  UINT32              PortIndex;

  GpioSbuPadTable   = NULL;
  Index             = 0;
  PortIndex         = 0;

  if (DciPortId == 0) {
    return EFI_INVALID_PARAMETER;
  }

  if (IsPchS ())  {
    GpioSbuPadTable = mPchSTypeCSbuGpioPad;
    SbuPadNum = ARRAY_SIZE (mPchSTypeCSbuGpioPad) / 2;
  } else {
    return EFI_UNSUPPORTED;
  }
  //
  // 2-wire DCI OOB port ID ranges from 16 ~ 23.
  // Port ID 16 maps to USBC port 0; Port ID 17 maps to USBC port 1, and so on.
  //
  PortIndex = DciPortId - 0x10;
  if (PortIndex >= SbuPadNum) {
    return EFI_UNSUPPORTED;
  }
  //
  // Set IOStandby state to masked
  //
  for (Index = 0; Index < 2; Index++) {
    GpioConfigurePadIoStandby (GpioSbuPadTable[PortIndex * 2 + Index], GpioIosStateMasked, GpioIosTermDefault);
  }
  return EFI_SUCCESS;
}

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSFiaClkReqVwMsgBus [][20] =
{
  {
    {GPIO_VER4_S_VGPIO_PCIE_0, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_1, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_2, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_3, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_4, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_5, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_6, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_7, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_8, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_9, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_10, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_11, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_12, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_13, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_14, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_15, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_64, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_65, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_66, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_67, GpioPadModeNative1}
  },
  {
    {GPIO_VER4_S_VGPIO_PCIE_16, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_17, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_18, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_19, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_20, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_21, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_22, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_23, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_24, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_25, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_26, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_27, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_28, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_29, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_30, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_31, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_68, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_69, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_70, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_71, GpioPadModeNative1}
  },
  {
    {GPIO_VER4_S_VGPIO_PCIE_32, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_33, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_34, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_35, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_36, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_37, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_38, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_39, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_40, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_41, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_42, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_43, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_44, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_45, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_46, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_47, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_72, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_73, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_74, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_75, GpioPadModeNative1}
  },
  {
    {GPIO_VER4_S_VGPIO_PCIE_48, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_49, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_50, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_51, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_52, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_53, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_54, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_55, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_56, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_57, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_58, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_59, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_60, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_61, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_62, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_63, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_76, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_77, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_78, GpioPadModeNative1},
    {GPIO_VER4_S_VGPIO_PCIE_79, GpioPadModeNative1}
  },
};

/**
  Get the pins used for VW CLKREQ messages between FIA and GPIO controller.

  @param[in]  PciePortIndex       Index of the CPU PCIe root port
  @param[out] VwBusPinsTable      Pointer to table describing VW bus GPIO pins
  @param[out] NoOfNativePins      Size of the table with VW bus GPIO pins

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_UNSUPPORTED         Current GPIO controller doesn't support this functionality
  @retval EFI_INVALID_PARAMETER   Invalid group or parameter
**/
EFI_STATUS
GpioGetCpuPcieVwMsgBusPins (
  IN  UINT32                    PciePortIndex,
  OUT GPIO_PAD_NATIVE_FUNCTION  **VwBusPinsTable,
  OUT UINT32                    *NoOfNativePins
  )
{
  *VwBusPinsTable = NULL;
  *NoOfNativePins = 0;
  if (IsPchS ()) {
    if (PciePortIndex <= ARRAY_SIZE (mPchSFiaClkReqVwMsgBus)) {
      *VwBusPinsTable = mPchSFiaClkReqVwMsgBus[PciePortIndex];
      *NoOfNativePins = ARRAY_SIZE (mPchSFiaClkReqVwMsgBus[PciePortIndex]);
    }
  } else {
    return EFI_UNSUPPORTED;
  }

  if (*VwBusPinsTable != NULL) {
    return EFI_SUCCESS;
  } else {
    return EFI_INVALID_PARAMETER;
  }
}

//
// Timed GPIO pin mapping
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSTimeSyncToGpioMap[] =
{
  {GPIO_VER4_S_GPP_H23, GpioPadModeNative1},
  {GPIO_VER4_S_GPP_I10, GpioPadModeNative2}
};

/**
  This function sets Time Sync Gpio into native mode

  @param[in]  Index       index

  @retval Status
**/
EFI_STATUS
GpioEnableTimeSync (
  IN UINT32  Index
  )
{
  EFI_STATUS                Status;
  GPIO_PAD_NATIVE_FUNCTION  *TimeSyncToGpioMap;
  UINT32                    TimeSyncToGpioMapSize;

  if (GpioOverrideLevel1Enabled ()) {
    return EFI_SUCCESS;
  }

  TimeSyncToGpioMap = mPchSTimeSyncToGpioMap;
  TimeSyncToGpioMapSize = ARRAY_SIZE (mPchSTimeSyncToGpioMap);

  if (Index < TimeSyncToGpioMapSize) {
    Status = GpioSetPadMode (TimeSyncToGpioMap[Index].Pad, TimeSyncToGpioMap[Index].Mode);
    if (EFI_ERROR (Status)) {
      ASSERT_EFI_ERROR (Status);
      return Status;
    }

    Status = GpioConfigurePadIoStandby (
               TimeSyncToGpioMap[Index].Pad,
               TimeSyncToGpioMap[Index].IosState,
               TimeSyncToGpioMap[Index].IosTerm
               );
    if (EFI_ERROR (Status)) {
      ASSERT_EFI_ERROR (Status);
      return Status;
    }
  } else {
    Status = EFI_UNSUPPORTED;
  }

  return Status;
}

//
// Tsn pin mapping
//

/**
  This function sets Tsn into native mode

  @retval Status
**/
EFI_STATUS
GpioEnableTsn (
  VOID
  )
{
  return EFI_SUCCESS;
}

/**
  This function is to be used In GpioLockPads() to override a lock request by SOC code.

  @param[in]  Group               GPIO group
  @param[in]  DwNum               Register number for current group (parameter applicable in accessing whole register).
                                  For group which has less then 32 pads per group DwNum must be 0.
  @param[out] *UnlockCfgPad       DWORD bitmask for pads which are going to be left unlocked
                                  Bit position - PadNumber
                                  Bit value - 0: to be locked, 1: Leave unlocked
  @param[out] *UnlockTxPad        DWORD bitmask for pads which are going to be left unlocked
                                  Bit position - PadNumber
                                  Bit value - 0: to be locked, 1: Leave unlocked

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_INVALID_PARAMETER   Invalid input parameter
**/
EFI_STATUS
GpioUnlockOverride (
  IN  GPIO_GROUP  Group,
  IN  UINT32      DwNum,
  OUT UINT32      *UnlockCfgPad,
  OUT UINT32      *UnlockTxPad
  )
{
  GPIO_PAD            *GpioSbuPadTable;
  UINT32              SbuPadNum;
  UINT32              MaxIndex;
  GPIO_CONFIG         PadConfig;
  UINT32              Index;
  UINT32              SbuUnlockedPads;
  EFI_STATUS          Status;

  if ((UnlockCfgPad == NULL) || (UnlockTxPad == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  MaxIndex = 0;
  // SBU (Sideband use) pins are used as auxiliary signals for Type C connector,
  // which are hard-wired to BSSB_LS natively for debug function.
  // when USB-C needed, don't lock pins that control by PMC at runtime,
  if (IsPchS()) {
    GpioSbuPadTable = mPchSTypeCSbuGpioPad;
    MaxIndex = ARRAY_SIZE (mPchSTypeCSbuGpioPad);
  } else {
    *UnlockCfgPad = 0;
    *UnlockTxPad = 0;
    return EFI_SUCCESS;
  }
  SbuUnlockedPads = 0;

  for (Index = 0; Index < MaxIndex; Index++) {

    SbuPadNum = GpioGetPadNumberFromGpioPad (GpioSbuPadTable[Index]);

    if (Group != GpioGetGroupFromGpioPad (GpioSbuPadTable[Index])) {
      continue;
    }

    if (DwNum != GPIO_GET_DW_NUM (SbuPadNum)) {
      continue;
    }

    Status = GpioGetPadConfig (GpioSbuPadTable[Index], &PadConfig);
    if (EFI_ERROR(Status)) {
      continue;
    }

    //
    // GPIO, Native3, Native4 and Native5 control by PMC at runtime
    //
    if ((PadConfig.PadMode == GpioPadModeNative1) || (PadConfig.PadMode == GpioPadModeNative2)) {
      continue;
    }

    SbuUnlockedPads |= (1 << GPIO_GET_PAD_POSITION (SbuPadNum));
  }

  *UnlockCfgPad = SbuUnlockedPads;
  *UnlockTxPad = SbuUnlockedPads;
  return EFI_SUCCESS;
}
