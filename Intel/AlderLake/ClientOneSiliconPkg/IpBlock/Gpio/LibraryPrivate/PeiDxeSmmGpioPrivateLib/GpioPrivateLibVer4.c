/** @file
  This file contains VER4 specific GPIO information

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi/UefiBaseType.h>
#include <Library/PchInfoLib.h>
#include <Library/GpioLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/GpioNativePads.h>
#include <Library/GpioPrivateLib.h>
#include <Library/DebugLib.h>
#include <Library/TcssInitLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/CpuRegbarAccessLib.h>
#include <Library/IoLib.h>
#include <Library/GpioHelpersLib.h>
#include <Register/GpioRegsVer4.h>
#include <Register/PmcRegsVer4.h>
#include <Register/PchPcrRegs.h>
#include <Register/GpioRegs.h>
#include <Register/IomRegs.h>
#include <Pins/GpioPinsVer4S.h>
#include <GpioNativePrivateLibInternal.h>
#include <CpuSbInfo.h>

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_GROUP_INFO mPchSGpioGroupInfo[] = {
  {PID_GPIOCOM0, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_I_PAD_MAX},
  {PID_GPIOCOM0, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_R_PAD_MAX},
  {PID_GPIOCOM0, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_J_PAD_MAX},
  {PID_GPIOCOM0, R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_VGPIO_PAD_MAX},
  {PID_GPIOCOM0, R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_PAD_OWN,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_HOSTSW_OWN,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_GPI_IS,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_GPI_IE,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_GPI_GPE_STS, R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_GPI_GPE_EN,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_SMI_STS,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_SMI_EN,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_NMI_STS,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_NMI_EN,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_PADCFGLOCK,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_PADCFGLOCKTX,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_PADCFG_OFFSET,  GPIO_VER4_PCH_S_GPIO_VGPIO_0_PAD_MAX},
  {PID_GPIOCOM1, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_B_PAD_MAX},
  {PID_GPIOCOM1, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_G_PAD_MAX},
  {PID_GPIOCOM1, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_H_PAD_MAX},
  {PID_GPIOCOM2, R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_PAD_OWN,      R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_HOSTSW_OWN,      R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_GPI_IS,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_GPI_IE,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_GPI_GPE_STS,     R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_GPI_GPE_EN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_SMI_STS,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_SMI_EN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_NMI_STS,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_NMI_EN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_PADCFGLOCK,      R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_PADCFGLOCKTX,      R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_PADCFG_OFFSET,      GPIO_VER4_PCH_S_GPIO_GPD_PAD_MAX},
  {PID_GPIOCOM3, R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_PAD_OWN,      R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_HOSTSW_OWN,      R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_GPI_IS,    R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_GPI_IE,    R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_GPI_GPE_STS,     R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_GPI_GPE_EN,    R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_SMI_STS,    R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_SMI_EN,    R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_NMI_STS,    R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_NMI_EN,    R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_PADCFGLOCK,      R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_PADCFGLOCKTX,      R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_PADCFG_OFFSET,      GPIO_VER4_PCH_S_GPIO_SPI_PAD_MAX},
  {PID_GPIOCOM3, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_A_PAD_MAX},
  {PID_GPIOCOM3, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_C_PAD_MAX},
  {PID_GPIOCOM3, R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_PAD_OWN,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_HOSTSW_OWN,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_GPI_IS,R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_GPI_GPE_STS, R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_GPI_GPE_EN,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_SMI_STS,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_SMI_EN,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_NMI_STS,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_NMI_EN,R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_PADCFGLOCK,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_PADCFGLOCKTX,  R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_PADCFG_OFFSET,  GPIO_VER4_PCH_S_GPIO_VGPIO_3_PAD_MAX},
  {PID_GPIOCOM4, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_S_PAD_MAX},
  {PID_GPIOCOM4, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_E_PAD_MAX},
  {PID_GPIOCOM4, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_K_PAD_MAX},
  {PID_GPIOCOM4, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_F_PAD_MAX},
  {PID_GPIOCOM5, R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_PAD_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_HOSTSW_OWN,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_GPI_IS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_GPI_IE,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_GPI_GPE_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_GPI_GPE_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_SMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_SMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_NMI_STS,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_NMI_EN,  R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_PADCFGLOCK,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_PADCFGLOCKTX,    R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_PADCFG_OFFSET,    GPIO_VER4_PCH_S_GPIO_GPP_D_PAD_MAX},
  {PID_GPIOCOM5, R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_PAD_OWN,     R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_HOSTSW_OWN,     R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_GPI_IS,   R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_GPI_IE,   R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_GPI_GPE_STS,    R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_GPI_GPE_EN,   R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_SMI_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_SMI_EN,   R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_NMI_STS,   R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_NMI_EN,   R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_PADCFGLOCK,     R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_PADCFGLOCKTX,     R_GPIO_VER4_PCH_S_GPIO_PCR_JTAG_PADCFG_OFFSET,     GPIO_VER4_PCH_S_GPIO_JTAG_PAD_MAX},
  {PID_GPIOCOM5, R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_PAD_OWN,      R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_HOSTSW_OWN,      R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_GPI_IS,    R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_GPI_IE,    R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_GPI_GPE_STS,     R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_GPI_GPE_EN,    R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_SMI_STS,    R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_SMI_EN,    R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_NMI_STS,    R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_NMI_EN,    R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_PADCFGLOCK,      R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_PADCFGLOCKTX,      R_GPIO_VER4_PCH_S_GPIO_PCR_CPU_PADCFG_OFFSET,      GPIO_VER4_PCH_S_GPIO_CPU_PAD_MAX}
};

/**
  This procedure will retrieve address and length of GPIO info table

  @param[out]  GpioGroupInfoTableLength   Length of GPIO group table

  @retval Pointer to GPIO group table

**/
CONST GPIO_GROUP_INFO*
GpioGetGroupInfoTable (
  OUT UINT32              *GpioGroupInfoTableLength
  )
{
  *GpioGroupInfoTableLength = ARRAY_SIZE (mPchSGpioGroupInfo);
  return mPchSGpioGroupInfo;
}

/**
  Get GPIO Chipset ID specific to PCH generation and series

  @retval Chipset ID
**/
UINT32
GpioGetThisChipsetId (
  VOID
  )
{
  return GPIO_VER4_S_CHIPSET_ID;
}

/**
  This internal procedure will check if group is within DeepSleepWell.

  @param[in]  Group               GPIO Group

  @retval GroupWell               TRUE:  This is DSW Group
                                  FALSE: This is not DSW Group
**/
BOOLEAN
GpioIsDswGroup (
  IN  GPIO_GROUP         Group
  )
{
  if ((Group == GPIO_VER4_S_GROUP_GPD)) {
    return TRUE;
  } else {
    return FALSE;
  }
}


/**
  This procedure will perform special handling of GPP_A_12.

  @param[in]  None

  @retval None
**/
VOID
GpioA12SpecialHandling (
  VOID
  )
{
  return;
}

GLOBAL_REMOVE_IF_UNREFERENCED PCH_SBI_PID mGpioComSbiIds[] =
{
  PID_GPIOCOM0, PID_GPIOCOM1, PID_GPIOCOM2, PID_GPIOCOM3, PID_GPIOCOM4, PID_GPIOCOM5
};

/**
  This function provides GPIO Community PortIDs

  @param[out] NativePinsTable                Table with GPIO COMMx SBI PortIDs

  @retval     Number of communities
**/
UINT32
GpioGetComSbiPortIds (
  OUT PCH_SBI_PID    **GpioComSbiIds
  )
{
  *GpioComSbiIds = mGpioComSbiIds;
  return ARRAY_SIZE (mGpioComSbiIds);
}

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_PAD_NATIVE_FUNCTION mPchSIoStandbyStateConfigurationPads[] =
{
  {GPIO_VER4_S_JTAGX,            GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_PRDYB,            GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_PREQB,            GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_CPU_TRSTB,        GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_TRIGGER_IN,       GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_TRIGGER_OUT,      GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_SPI0_IO_2,        GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_SPI0_IO_3,        GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_SPI0_MOSI_IO_0,   GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_SPI0_MISO_IO_1,   GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_SPI0_TPM_CSB,     GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_SPI0_FLASH_0_CSB, GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_SPI0_FLASH_1_CSB, GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_SPI0_CLK,         GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_HDACPU_SDI,       GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_HDACPU_SDO,       GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_HDACPU_SCLK,      GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_PM_SYNC,          GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_CPUPWRGD,         GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_THRMTRIPB,        GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_PLTRST_CPUB,      GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame},
  {GPIO_VER4_S_PM_DOWN,          GpioPadModeHwDefault, GpioIosStateMasked, GpioIosTermSame}
};

/**
  This function provides list of GPIO for which IO Standby State configuration
  has to be set as 'Masked'

  @param[out] GpioPadsList                Table with pads

  @retval      Number of pads
**/
UINT32
GpioGetIoStandbyStateConfigurationPadsList (
  OUT GPIO_PAD_NATIVE_FUNCTION      **GpioPadsList
  )
{
  *GpioPadsList = mPchSIoStandbyStateConfigurationPads;
  return ARRAY_SIZE (mPchSIoStandbyStateConfigurationPads);
}

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_GROUP_TO_GPE_MAPPING mPchSGpioGroupToGpeMapping[] = {
  {GPIO_VER4_S_GROUP_GPP_I, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_I, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_I},
  {GPIO_VER4_S_GROUP_GPP_R, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_R, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_R},
  {GPIO_VER4_S_GROUP_GPP_J, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_J, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_J},
  {GPIO_VER4_S_GROUP_VGPIO, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_VGPIO, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_VGPIO},
  {GPIO_VER4_S_GROUP_GPD,   0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPD,   V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPD},
  {GPIO_VER4_S_GROUP_GPP_D, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_D, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_D},
  {GPIO_VER4_S_GROUP_GPP_S, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_S, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_S},
  {GPIO_VER4_S_GROUP_GPP_E, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_E, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_E},
  {GPIO_VER4_S_GROUP_GPP_K, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_K, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_K},
  {GPIO_VER4_S_GROUP_GPP_F, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_F, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_F},
  {GPIO_VER4_S_GROUP_GPP_A, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_A, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_A},
  {GPIO_VER4_S_GROUP_GPP_C, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_C, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_C},
  {GPIO_VER4_S_GROUP_GPP_B, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_B, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_B},
  {GPIO_VER4_S_GROUP_GPP_G, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_G, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_G},
  {GPIO_VER4_S_GROUP_GPP_H, 0, V_ADL_PCH_S_PMC_PWRM_GPIO_CFG_GPP_H, V_GPIO_VER4_PCH_S_GPIO_PCR_MISCCFG_GPE0_GPP_H}
};

/**
  Get information for GPIO Group required to program GPIO and PMC for desired 1-Tier GPE mapping

  @param[out] GpioGroupToGpeMapping        Table with GPIO Group to GPE mapping
  @param[out] GpioGroupToGpeMappingLength  GPIO Group to GPE mapping table length
**/
VOID
GpioGetGroupToGpeMapping (
  OUT GPIO_GROUP_TO_GPE_MAPPING  **GpioGroupToGpeMapping,
  OUT UINT32                     *GpioGroupToGpeMappingLength
  )
{
  *GpioGroupToGpeMapping = mPchSGpioGroupToGpeMapping;
  *GpioGroupToGpeMappingLength = ARRAY_SIZE (mPchSGpioGroupToGpeMapping);
}

/**
  Check if 0x13 opcode supported for writing to GPIO lock unlock register

  @retval TRUE                It's supported
  @retval FALSE               It's not supported
**/
BOOLEAN
IsGpioLockOpcodeSupported (
  VOID
  )
{
  return FALSE;
}

/**
  This procedure will set GPIO pad to native function based on provided native function
  and platform muxing selection (if needed).

  @param[in]  PadFunction         PadMode for a specific native signal. Please refer to GpioNativePads.h
  @param[in]  PinMux              GPIO Native pin mux platform config.
                                  This argument is optional and needs to be
                                  provided only if feature can be enabled
                                  on multiple pads

  @retval EFI_SUCCESS             The function completed successfully
  @retval EFI_INVALID_PARAMETER   Invalid group or pad number
**/
EFI_STATUS
GpioSetNativePadByFunction (
  IN UINT32  PadFunction,
  IN UINT32  PinMux
  )
{
  GPIO_NATIVE_PAD GpioNativePad;
  EFI_STATUS      Status;

  if (GpioOverrideLevel1Enabled ()) {
    return EFI_SUCCESS;
  }

  GpioNativePad = GpioGetNativePadByFunctionAndPinMux (PadFunction, PinMux);

  if (GpioNativePad == GPIO_PAD_NONE) {
    return EFI_INVALID_PARAMETER;
  }

  Status = GpioSetPadMode (GpioNativePad, GPIO_NATIVE_GET_PAD_MODE (GpioNativePad));
  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }

  Status = GpioConfigurePadIoStandbyByFunction (PadFunction, PinMux);
  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }

  return EFI_SUCCESS;
}

/**
  This function performs basic initialization IOM for AUX Layout in PEI phase after Policy produced at Post-Mem phase.

  For those GPIO pins used for DP Aux orientation control and enabled, BIOS is responsible to -
  1. Configure Pad Mode (PMode) to function# associated with IOM_GPP*_*
     The Pad Mode here could be various per PCH design.
  2. BIOS Provide the following information for the DP Aux orientation bias control for all supported Type-C ports
     to IOM FW
     I. GPIO endpoint IOSF-SB port ID (CPU local port ID)
     II. VW index and data bit position

  @param[in] ConfigTablePtr - Instance pointer of IOM_AUX_ORI_PAD_CONFIG_HEAD

**/
VOID
GpioIomAuxOriSetting (
  IN TCSS_IOM_PEI_CONFIG      *ConfigTablePtr
  )
{
  UINT8                       Index;
  UINT8                       PadIndex;
  IOM_AUX_ORI_BIAS_STRUCTURE  RegSetting;
  UINT32                      PadRegAddr[2];
  UINT32                      PadRegValue;
  UINT16                      RegAddr;
  UINT8                       PchPid;
  GPIO_CONFIG                 PadConfig;

  ///
  /// BIOS communicate the PCH AUX layout with IOM
  ///
  for (Index = 0; Index < MAX_IOM_AUX_BIAS_COUNT; Index++) {

    PadRegAddr[0] = ConfigTablePtr->IomAuxPortPad[Index].GpioPullN;
    PadRegAddr[1] = ConfigTablePtr->IomAuxPortPad[Index].GpioPullP;
    RegSetting.RegValue = 0;

    //
    // GPIO setting is NULL go to check next port config
    //
    if ((PadRegAddr[0] == 0) || (PadRegAddr[1] == 0)) {
      continue;
    }

    ZeroMem (&PadConfig, sizeof (GPIO_CONFIG));

    ///
    /// Up Ping and down ping loop start
    ///
    for (PadIndex = 0; PadIndex < 2; PadIndex++) {

      ///
      /// Configure Pad Mode (PMode) to function# associated with IOM_GPP*_*
      /// The Pad Mode here could be various per PCH design. For ADL, it is Native Function 6
      ///
      PadConfig.PadMode = GpioPadModeNative6;                // Set to IOM mode
      GpioSetPadConfig ((GPIO_PAD) PadRegAddr[PadIndex], &PadConfig);

      ///
      /// 0 is up and 1 is down
      ///
      PadRegValue =  PadRegAddr[PadIndex];

      ///
      /// Update GPIO Family portID bit[7:0]
      ///
      PchPid = GpioGetGpioCommunityPortIdFromGpioPad (PadRegValue);

      //
      //  GPIO PID Translation table
      //         PCH PID  CPU PID
      //  GPCOM0 0x6E     0xB7
      //  GPCOM1 0x6D     0xB8
      //  GPCOM4 0x6A     0xB9
      //  GPCOM5 0x69     0xBA
      //  GPCOM3 0x6B     0xBB
      //
      ///
      /// Translated PCH PID to CPU PID
      ///
      switch (PchPid) {
        case PID_GPIOCOM0:
          RegSetting.Bits.GpioPortId = 0xB7;
          break;
        case PID_GPIOCOM1:
          RegSetting.Bits.GpioPortId = 0xB8;
          break;
        case PID_GPIOCOM4:
          RegSetting.Bits.GpioPortId = 0xB9;
          break;
        case PID_GPIOCOM5:
          RegSetting.Bits.GpioPortId = 0xBA;
          break;
        case PID_GPIOCOM3:
          RegSetting.Bits.GpioPortId = 0xBB;
          break;
        default:
          RegSetting.Bits.GpioPortId = PchPid;
      }

      ///
      /// Update Bit Position bit[10:8]
      ///
      RegSetting.Bits.Bit = (PadRegValue & 0xff) % 8;

      ///
      /// Vw Index update bit[23:16]
      ///
      RegSetting.Bits.VwIndex = (PadRegValue >> 16) & 0x0f;
      switch (RegSetting.Bits.VwIndex) {
        //
        // VW Index 10h - 12h
        //
        case 0: // GPP_A
        case 3: // GPP_D
        case 2: // GPP_C
           RegSetting.Bits.VwIndex = ((PadRegValue & 0xff) / 8) + 0x10;
          break;
        //
        // VW Index 13h - 15h
        //
        case 1: // GPP_B
        case 5: // GPP_F
        case 4: // GPP_E
           RegSetting.Bits.VwIndex = ((PadRegValue & 0xff) / 8) + 0x13;
          break;
        //
        // VW Index 16h - 18h
        //
        case 6: // GPP_G
        case 7: // GPP_H
           RegSetting.Bits.VwIndex = ((PadRegValue & 0xff) / 8) + 0x16;
          break;

        default:
           RegSetting.Bits.VwIndex = 0xFF;
      }

      //
      // Get correspond AUX BIAS CTRL offset
      //
      if (PadIndex == 0) {
        RegAddr = R_IOM_AUX_ORI_BIAS_CTRL_PUP_MMOFFSET_1 + (Index*4);  // Up Pin
      } else {
        RegAddr = R_IOM_AUX_ORI_BIAS_CTRL_PDN_MMOFFSET_1 + (Index*4);  // Down Pin
      }

      //
      // Set register
      //
      CpuRegbarWrite32 (CPU_SB_PID_IOM, RegAddr, RegSetting.RegValue);

      DEBUG ((DEBUG_INFO, "[GpioIomAuxOriSetting] IomAuxOriConfig : Regaddr %08x, RegValue %08x\n",RegAddr, RegSetting.RegValue));

    }
    // Up pin and Down pin Loop End

  }

}
