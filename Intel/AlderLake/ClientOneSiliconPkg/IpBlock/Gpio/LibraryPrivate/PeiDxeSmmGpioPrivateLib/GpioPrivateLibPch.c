/** @file
  This file contains hard/physical/local (not virtual) GPIO information

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020-2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi/UefiBaseType.h>
#include <Library/GpioLib.h>
#include <Library/GpioPrivateLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/PchSbiAccessLib.h>
#include <Register/PchPcrRegs.h>
#include <Register/GpioRegs.h>
#include "GpioNativePrivateLibInternal.h"

/**
  This procedure calculates Pad Configuration Register DW offset

  @param[in] GpioPad                 GPIO pad
  @param[in] DwReg                   Index of the configuration register

  @retval DW Register offset
**/
UINT32
GpioGetGpioPadCfgAddressFromGpioPad (
  IN  GPIO_PAD                GpioPad,
  IN  UINT32                  DwReg
  )
{
  UINT32                 PadCfgRegAddress;
  CONST GPIO_GROUP_INFO  *GpioGroupInfo;
  UINT32                 GpioGroupInfoLength;
  UINT32                 GroupIndex;
  UINT32                 PadNumber;

  GroupIndex = GpioGetGroupIndexFromGpioPad (GpioPad);
  PadNumber = GpioGetPadNumberFromGpioPad (GpioPad);

  GpioGroupInfo = GpioGetGroupInfoTable (&GpioGroupInfoLength);

  //
  // Create Pad Configuration register offset
  //
  PadCfgRegAddress = GpioGroupInfo[GroupIndex].PadCfgOffset + DwReg * 4 + S_GPIO_PCR_PADCFG * PadNumber;

  return PadCfgRegAddress;
}

/**
  This procedure reads GPIO register

  @param[in] GpioGroupInfo           Pointer to GPIO group table info
  @param[in] Register                Register offset

  @retval Register value or "F"s in case of errors
**/
UINT32
GpioRegisterAccessRead32 (
  IN  CONST GPIO_GROUP_INFO   *GpioGroupInfo,
  IN  UINT32                  Register
  )
{
  return MmioRead32 (PCH_PCR_ADDRESS (GpioGroupInfo->Community, Register));
}

/**
  This procedure writes GPIO register

  @param[in] GpioGroupInfo           Pointer to GPIO group table info
  @param[in] Register                Register offset
  @param[in] AndValue                And value
  @param[in] OrValue                 Or value

  @retval EFI_SUCCESS                Operation completed successfully
**/
EFI_STATUS
GpioRegisterAccessAndThenOr32 (
  IN  CONST GPIO_GROUP_INFO   *GpioGroupInfo,
  IN  UINT32                  Register,
  IN  UINT32                  AndValue,
  IN  UINT32                  OrValue
  )
{
  MmioAndThenOr32 (
    PCH_PCR_ADDRESS (GpioGroupInfo->Community, Register),
    AndValue,
    OrValue
    );
  return EFI_SUCCESS;
}

/**
  This procedure will calculate PADCFG register value based on GpioConfig data

  @param[in]  GpioPad                   GPIO Pad
  @param[in]  GpioConfig                GPIO Configuration data
  @param[out] PadCfgDwReg               PADCFG DWx register value
  @param[out] PadCfgDwRegMask           Mask with PADCFG DWx register bits to be modified

  @retval Status
**/
EFI_STATUS
GpioPadCfgRegValueFromGpioConfig (
  IN  GPIO_PAD           GpioPad,
  IN  CONST GPIO_CONFIG  *GpioConfig,
  OUT UINT32             *PadCfgDwReg,
  OUT UINT32             *PadCfgDwRegMask
  )
{
  return GpioPadCfgRegValueFromGpioConfigHardGpio (GpioPad, GpioConfig, PadCfgDwReg, PadCfgDwRegMask);
}

/**
  This internal procedure will get GPIO_CONFIG data from PADCFG registers value

  @param[in]  GpioPad                   GPIO Pad
  @param[in]  PadCfgDwReg               PADCFG DWx register values
  @param[out] GpioData                  GPIO Configuration data

  @retval Status
**/
VOID
GpioConfigFromPadCfgRegValue (
  IN GPIO_PAD      GpioPad,
  IN CONST UINT32  *PadCfgDwReg,
  OUT GPIO_CONFIG  *GpioConfig
  )
{
  GpioConfigFromPadCfgRegValueHardGpio (GpioPad, PadCfgDwReg, GpioConfig);
}

/**
  This procedure returns value of RX State field in Pad Configuration Register
  (current internal RX pad state)

  @param[in] GpioPad              GPIO pad

  @retval Value of RX State
**/
UINT32
GpioGetRxState (
  IN GPIO_PAD   GpioPad
  )
{
  UINT32      PadCfgReg;

  PadCfgReg = GpioReadPadCfgReg (GpioPad, 0);
  return (PadCfgReg & B_GPIO_PCR_RX_STATE) >> N_GPIO_PCR_RX_STATE;
}

/**
  This procedure returns value of TX State field in Pad Configuration Register
  (current internal TX pad state)

  @param[in] GpioPad              GPIO pad

  @retval Value of TX State
**/
UINT32
GpioGetTxState (
  IN GPIO_PAD   GpioPad
  )
{
  UINT32      PadCfgReg;

  PadCfgReg = GpioReadPadCfgReg (GpioPad, 0);
  return (PadCfgReg & B_GPIO_PCR_TX_STATE) >> N_GPIO_PCR_TX_STATE;
}

/**
  This procedure returns of TX State bit mask for Pad Configuration Register

  @param[in] GpioPad              GPIO pad

  @retval TX State bit mask
**/
UINT32
GpioGetTxStateMask (
  IN GPIO_PAD   GpioPad
  )
{
  return (UINT32)~B_GPIO_PCR_TX_STATE;
}

/**
  This procedure returns of TX State OR value shifted to be used for
  Pad Configuration Register programming

  @param[in] GpioPad        GPIO pad
  @param[in] Value          Value to be shifted

  @retval TX State value
**/
UINT32
GpioGetTxStateOrValue (
  IN GPIO_PAD   GpioPad,
  IN UINT32     Value
  )
{
  return Value << N_GPIO_PCR_TX_STATE;
}

/**
  This procedure returns value Interrupt select field in Pad Configuration Register

  @param[in]  GpioPad                   GPIO Pad

  @retval Interrupt select Value
**/
UINT32
GpioGetIntSel (
  IN GPIO_PAD      GpioPad
  )
{
  UINT32  PadCfgReg;

  // Interrupt select located in DW1
  PadCfgReg = GpioReadPadCfgReg (GpioPad, 1);

  return (PadCfgReg & B_GPIO_PCR_INTSEL) >> N_GPIO_PCR_INTSEL;
}

/**
  This procedure checks if a pad is virtual GPIO pad.

  @param[in]  GpioPad                   GPIO Pad

  @retval True - passed pad is virtual GPIO pad
          False - NOT vitrual pad
**/
BOOLEAN
GpioPadIsVirtual (
  IN GPIO_PAD      GpioPad
  )
{
  // There is no virtual (eSPI Virtual Channel) pads for PCH GPIO
  return FALSE;
}

/**
  This procedure programs Pad interrupt level and routning.

  @param[in] GpioPad              GPIO pad
  @param[in] GpioInterruptCfg     Value of Level/Edge
                                  use GPIO_INT_CONFIG as argument

  @retval EFI_SUCCESS       All settings programmed
          EFI_ABORTED       Programmed interrupt level,
                            Interrupt routing left hardware default.
          EFI_UNSUPPORTED   Unsupported settings
**/
EFI_STATUS
GpioProgramPadIntLevelAndRouting (
  IN GPIO_PAD                 GpioPad,
  IN GPIO_INT_CONFIG          GpioInterruptCfg
  )
{
  UINT32      RxLvlEdgeValue;
  UINT32      IntRouteValue;

  if (((GpioInterruptCfg & B_GPIO_INT_CONFIG_INT_TYPE_MASK) >> N_GPIO_INT_CONFIG_INT_TYPE_BIT_POS) != GpioHardwareDefault) {
    RxLvlEdgeValue = ((GpioInterruptCfg & B_GPIO_INT_CONFIG_INT_TYPE_MASK) >> (N_GPIO_INT_CONFIG_INT_TYPE_BIT_POS + 1)) << N_GPIO_PCR_RX_LVL_EDG;

    GpioWritePadCfgReg (
      GpioPad,
      0,
      (UINT32)~B_GPIO_PCR_RX_LVL_EDG,
      RxLvlEdgeValue
      );
  }

  if (((GpioInterruptCfg & B_GPIO_INT_CONFIG_INT_SOURCE_MASK) >> N_GPIO_INT_CONFIG_INT_SOURCE_BIT_POS) != GpioHardwareDefault) {
    IntRouteValue = ((GpioInterruptCfg & B_GPIO_INT_CONFIG_INT_SOURCE_MASK) >> (N_GPIO_INT_CONFIG_INT_SOURCE_BIT_POS + 1)) << N_GPIO_PCR_RX_NMI_ROUTE;

    GpioWritePadCfgReg (
      GpioPad,
      0,
      (UINT32)~(B_GPIO_PCR_RX_NMI_ROUTE | B_GPIO_PCR_RX_SCI_ROUTE | B_GPIO_PCR_RX_SMI_ROUTE | B_GPIO_PCR_RX_APIC_ROUTE),
      IntRouteValue
      );
  } else {
    return EFI_ABORTED;
  }
  return EFI_SUCCESS;
}

/**
  This procedure will write GPIO Lock/LockTx register
  - For PCH SBI message is used.

  @param[in] RegValue             GPIO register (Lock or LockTx) value
  @param[in] RegOffset            GPIO register (Lock or LockTx) base offset
  @param[in] DwNum                Register number for current group.
                                  For group which has less then 32 pads per group DwNum must be 0.
  @param[in] GpioGroupInfo        Pointer to GPIO group table info
  @param[in] GroupIndex           GPIO group index in the GpioGroupInfo table

  @retval EFI_SUCCESS             The function completed successfully
          EFI_UNSUPPORTED         Feature is not supported for this group or pad
**/
EFI_STATUS
GpioInternalWriteLockRegister (
  IN UINT32                 RegValue,
  IN UINT32                 RegOffset,
  IN UINT32                 DwNum,
  IN CONST GPIO_GROUP_INFO  *GpioGroupInfo,
  IN UINT32                 GroupIndex
  )
{
  EFI_STATUS             Status;
  PCH_SBI_OPCODE         Opcode;
  UINT8                  Response;

  //
  // If there are more then 32 pads per group then certain
  // group information would be split into more then one DWord register.
  // PadConfigLock and OutputLock registers when used for group containing more than 32 pads
  // are not placed in a continuous way, e.g:
  // 0x0 - PadConfigLock_DW0
  // 0x4 - OutputLock_DW0
  // 0x8 - PadConfigLock_DW1
  // 0xC - OutputLock_DW1
  //
  RegOffset += DwNum * 0x8;

  if (IsGpioLockOpcodeSupported ()) {
    Opcode = GpioLockUnlock;
  } else {
    Opcode = PrivateControlWrite;
  }

  Status = PchSbiExecutionEx (
             GpioGroupInfo[GroupIndex].Community,
             RegOffset,
             Opcode,
             FALSE,
             0x000F,
             0x0000,
             0x0000,
             &RegValue,
             &Response
             );
  ASSERT_EFI_ERROR (Status);
  return Status;
}