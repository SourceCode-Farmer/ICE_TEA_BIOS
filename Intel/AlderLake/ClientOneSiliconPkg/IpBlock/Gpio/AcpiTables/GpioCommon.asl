/**@file
 GPIO Common file with defines for ACPI GpioLibCommon and GpioXXX.asl (XXX - platform TGL, ADL, ...)

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

//
// Macro to retrieve data from GPIO information data structure (GpioLibObjects*.asl)
//
#define PCH_GPIO_GET_FIELD(GpioGroupIndex,GpioFieldIndex,GpioDataStructure) \
  ( \
    DeRefOf(Index( \
      DeRefOf(Index( \
        GpioDataStructure, \
        GpioGroupIndex)), \
      GpioFieldIndex)) \
  )

#define IS_GPIO_GROUP_MAPPED_TO_GPE_DWX(GroupIndex, GroupDw, GpeDwXGroupIndex, GpeDwXGroupDw) \
  ( \
    LAnd( \
      LEqual(GpeDwXGroupIndex, GroupIndex), \
      LEqual(GpeDwXGroupDw, GroupDw) \
    ) \
  )

//
// Check if group belongs to 2-Tier GPE
//
#define IS_GPIO_GROUP_IN_2_TIER_GPE(GroupIndex, GroupDw) \
  ( \
    LNot( \
      Or( \
        Or( \
          IS_GPIO_GROUP_MAPPED_TO_GPE_DWX(GroupIndex, GroupDw, GEI0, GED0), \
          IS_GPIO_GROUP_MAPPED_TO_GPE_DWX(GroupIndex, GroupDw, GEI1, GED1)), \
        IS_GPIO_GROUP_MAPPED_TO_GPE_DWX(GroupIndex, GroupDw, GEI2, GED2))) \
  )

//
// Get PADCFG_DW0 GpioPad register address
//
#define GET_GPIO_PAD_PADCFG_REG_ADDRESS(GpioGroupIndex,GpioPadNumber) \
  ( \
    Add( \
      GET_PADCFG_REG_ADDRESS(GpioGroupIndex), \
      Multiply( \
        GpioPadNumber, \
        S_GPIO_PCR_PADCFG) \
      ) \
  )

//
// Get GPIO register address
// If group has more DW registers for certain feature
// below macro will return address of the first one.
//
#define GET_PADCFG_REG_ADDRESS(GpioGroupIndex)      GADR(GpioGroupIndex,GPIO_PADCFG_REG_FIELD)
#define GET_HOSTSW_OWN_REG_ADDRESS(GpioGroupIndex)  GADR(GpioGroupIndex,GPIO_HOSTOWN_REG_FIELD)
#define GET_PAD_OWN_REG_ADDRESS(GpioGroupIndex)     GADR(GpioGroupIndex,GPIO_PADOWN_REG_FIELD)
#define GET_GPE_STS_REG_ADDRESS(GpioGroupIndex)     GADR(GpioGroupIndex,GPIO_GPE_STS_REG_FIELD)
#define GET_LOCK_CONFIG_REG_ADDRESS(GpioGroupIndex) GADR(GpioGroupIndex,GPIO_LOCK_CONFIG_REG_FIELD)
#define GET_LOCK_TX_REG_ADDRESS(GpioGroupIndex)     GADR(GpioGroupIndex,GPIO_LOCK_TX_REG_FIELD)

//
// Arg1 possible values for internal GINF function
//
#define GPIO_COM_FIELD               0
#define GPIO_PAD_NUM_FIELD           1
#define GPIO_PADCFG_REG_FIELD        2
#define GPIO_HOSTOWN_REG_FIELD       3
#define GPIO_PADOWN_REG_FIELD        4
#define GPIO_GPE_STS_REG_FIELD       5
#define GPIO_LOCK_CONFIG_REG_FIELD   6
#define GPIO_LOCK_TX_REG_FIELD       7
#define GPIO_DRIVER_PIN_BASE_NUMBER  8