/** @file
  This file contains GPIO name library implementation

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include "GpioLibrary.h"
#include "Library/GpioNativePads.h"
#include <Library/PrintLib.h>

/**
  Generates GPIO group name from GpioPad

  @param[in] GpioPad  GpioPad

  @retval CHAR8*  Pointer to the GPIO group name
**/
CONST
CHAR8*
GpioGetGroupName (
  IN UINT32  GroupIndex
  )
{
  CONST GPIO_GROUP_NAME_INFO*  GroupNameInfo;

  GroupNameInfo = GpioGetGroupNameInfo (GroupIndex);
  if (GroupNameInfo == NULL) {
    return NULL;
  } else {
    return GroupNameInfo->GpioGroupPrefix;
  }
}

/**
  Generates GPIO name from GpioPad

  @param[in]  GpioPad             GpioPad
  @param[out] GpioNameBuffer      Caller allocated buffer of GPIO_NAME_LENGTH_MAX size
  @param[in]  GpioNameBufferSize  Size of the buffer

  @retval CHAR8*  Pointer to the GPIO name
**/
CHAR8*
GpioGetPadName (
  IN GPIO_PAD  GpioPad,
  OUT CHAR8*   GpioNameBuffer,
  IN UINT32    GpioNameBufferSize
  )
{
  UINT32                       GroupIndex;
  UINT32                       PadNumber;
  UINT32                       FirstUniquePadNumber;
  CONST GPIO_GROUP_NAME_INFO*  GroupNameInfo;

  if (GpioNameBuffer == NULL) {
    ASSERT (FALSE);
    return NULL;
  }
  if ((GpioNameBufferSize < GPIO_NAME_LENGTH_MAX) || !GpioIsPadValid (GpioPad)) {
    ASSERT (FALSE);
    *GpioNameBuffer = 0;
    return NULL;
  }

  GroupIndex = GpioGetGroupIndexFromGpioPad (GpioPad);
  PadNumber = GpioGetPadNumberFromGpioPad (GpioPad);
  GroupNameInfo = GpioGetGroupNameInfo (GroupIndex);
  if (GroupNameInfo == NULL) {
    return NULL;
  }

  FirstUniquePadNumber = GpioGetPadNumberFromGpioPad (GroupNameInfo->FirstUniqueGpio);
  if ((PadNumber < FirstUniquePadNumber) || (GroupNameInfo->GroupUniqueNames == NULL)) {
    AsciiSPrint (GpioNameBuffer, GPIO_NAME_LENGTH_MAX, "GPIO_%a%d", GpioGetGroupName (GroupIndex), PadNumber);
  } else {
    if ((PadNumber - FirstUniquePadNumber) < GroupNameInfo->UniqueNamesTableSize) {
      AsciiSPrint (GpioNameBuffer, GPIO_NAME_LENGTH_MAX, "GPIO_%a", GroupNameInfo->GroupUniqueNames[PadNumber - FirstUniquePadNumber]);
    } else {
      AsciiSPrint (GpioNameBuffer, GPIO_NAME_LENGTH_MAX, "GPIO_%08X", GpioPad);
      ASSERT (FALSE);
    }
  }

  return GpioNameBuffer;
}

/**
  This function returns native signal name from PadFunction

  @param[in]  PadFunction             PadFunction for a specific native signal. Please refer to GpioNativePads.h
  @param[out] PadFunctionNameBuffer   Caller allocated buffer of GPIO_NAME_LENGTH_MAX size
  @param[in]  PadFunctionBufferSize   Size of the buffer

  @retval CHAR8*  Pointer to the native signal name
**/
CHAR8*
GpioGetNativeSignalName (
  IN  UINT32   PadFunction,
  OUT CHAR8*   PadFunctionNameBuffer,
  IN UINT32    PadFunctionBufferSize  
  )
{
  CHAR8 *SignalName = NULL;
  UINT32 SignalFunctionIndex;

  if (PadFunctionNameBuffer == NULL) {
    ASSERT (FALSE);
    return NULL;
  }

  if (PadFunctionBufferSize < GPIO_NAME_LENGTH_MAX) {
    ASSERT (FALSE);
    *PadFunctionNameBuffer = 0;
    return NULL;    
  }

  SignalFunctionIndex = GPIO_NATIVE_FUNCTION_GET_VALUE (PadFunction);

  switch (GPIO_NATIVE_FUNCTION_GET_SIGNAL (PadFunction)) {
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_UART_RX):
      SignalName = "GPIO_SERIAL_IO_UART_RX";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_UART_TX):
      SignalName = "GPIO_SERIAL_IO_UART_TX";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_UART_RTS):
      SignalName = "GPIO_SERIAL_IO_UART_RTS";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_UART_CTS):
      SignalName = "GPIO_SERIAL_IO_UART_CTS";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_SPI_MOSI):
      SignalName = "GPIO_SERIAL_IO_SPI_MOSI";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_SPI_MISO):
      SignalName = "GPIO_SERIAL_IO_SPI_MISO";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_SPI_CLK):
      SignalName = "GPIO_SERIAL_IO_SPI_CLK";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_SPI_CS):
      SignalName = "GPIO_SERIAL_IO_SPI_CS";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_GP):
      SignalName = "GPIO_ISH_GP";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_UART_RX):
      SignalName = "GPIO_ISH_UART_RX";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_UART_TX):
      SignalName = "GPIO_ISH_UART_TX";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_UART_RTS):
      SignalName = "GPIO_ISH_UART_RTS";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_UART_CTS):
      SignalName = "GPIO_ISH_UART_CTS";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_SPI_MOSI):
      SignalName = "GPIO_ISH_SPI_MOSI";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_SPI_MISO):
      SignalName = "GPIO_ISH_SPI_MISO";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_SPI_CLK):
      SignalName = "GPIO_ISH_SPI_CLK";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_SPI_CS):
      SignalName = "GPIO_ISH_SPI_CS";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_I2C_SCL):
      SignalName = "GPIO_ISH_I2C_SCL";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_I2C_SDA):
      SignalName = "GPIO_ISH_I2C_SDA";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_THC_SPI_INT):
      SignalName = "GPIO_THC_SPI_INT";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_HDA_BCLK):
      SignalName = "GPIO_HDA_BCLK";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_HDA_RSTB):
      SignalName = "GPIO_HDA_RSTB";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_HDA_SYNC):
      SignalName = "GPIO_HDA_SYNC";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_HDA_SDO):
      SignalName = "GPIO_HDA_SDO";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_HDA_SDI_0):
      SignalName = "GPIO_HDA_SDI_0";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_HDA_SDI_1):
      SignalName = "GPIO_HDA_SDI_1";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_DMIC_DATA):
      SignalName = "GPIO_DMIC_DATA";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_DMIC_CLKA):
      SignalName = "GPIO_DMIC_CLKA";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_DMIC_CLKB):
      SignalName = "GPIO_DMIC_CLKB";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_DDSP_HPD0):
      SignalName = "GPIO_DDSP_HPD0";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_PANEL_AVDD_EN):
      SignalName = "GPIO_PANEL_AVDD_EN";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_PANEL_BKLTEN):
      SignalName = "GPIO_PANEL_BKLTEN";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_PANEL_BKLTCTL):
      SignalName = "GPIO_PANEL_BKLTCTL";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_PANEL_RESET):
      SignalName = "GPIO_PANEL_RESET";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_PANEL_AVEE_EN):
      SignalName = "GPIO_PANEL_AVEE_EN";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_PANEL_VIO_EN):
      SignalName = "GPIO_PANEL_VIO_EN";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_PANEL_HPD):
      SignalName = "GPIO_PANEL_HPD";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_PANEL_TE_EN):
      SignalName = "GPIO_PANEL_TE_EN";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_HDMI_GMBUS_SCL):
      SignalName = "GPIO_HDMI_GMBUS_SCL";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_HDMI_GMBUS_SDA):
      SignalName = "GPIO_HDMI_GMBUS_SDA";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_I3C_SCL):
      SignalName = "GPIO_ISH_I3C_SCL";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_ISH_I3C_SDA):
      SignalName = "GPIO_ISH_I3C_SDA";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_I2C_SCL):
      SignalName = "GPIO_SERIAL_IO_I2C_SCL";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_I2C_SDA):
      SignalName = "GPIO_SERIAL_IO_I2C_SDA";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_CNVI_RF_RESET):
      SignalName = "GPIO_CNVI_RF_RESET";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_CNVI_CLKREQ):
      SignalName = "GPIO_CNVI_CLKREQ";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SD_PWR_EN_B):
      SignalName = "GPIO_SD_PWR_EN_B";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SD_CMD):
      SignalName = "GPIO_SD_CMD";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SD_DATA):
      SignalName = "GPIO_SD_DATA";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SD_CDB):
      SignalName = "GPIO_SD_CDB";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SD_CLK):
      SignalName = "GPIO_SD_CLK";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SD_WP):
      SignalName = "GPIO_SD_WP";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SD_1P8_EN):
      SignalName = "GPIO_SD_1P8_EN";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SD_CLK_FB):
      SignalName = "GPIO_SD_CLK_FB";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_EMMC_CMD):
      SignalName = "GPIO_EMMC_CMD";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_EMMC_DATA):
      SignalName = "GPIO_EMMC_DATA";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_EMMC_RCLK):
      SignalName = "GPIO_EMMC_RCLK";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_EMMC_CLK):
      SignalName = "GPIO_EMMC_CLK";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_EMMC_RESETB):
      SignalName = "GPIO_EMMC_RESETB";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL(GPIO_EMMC_HIP_MON):
      SignalName = "GPIO_EMMC_HIP_MON";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_THC_CLK_LOOPBACK):
      SignalName = "GPIO_THC_CLK_LOOPBACK";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_I3C_SCL):
      SignalName = "GPIO_SERIAL_IO_I3C_SCL";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SERIAL_IO_I3C_SDA):
      SignalName = "GPIO_SERIAL_IO_I3C_SDA";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_MIPI_PANEL_RESET):
      SignalName = "GPIO_MIPI_PANEL_RESET";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_MIPI_SEC_POW_EN_AVEE):
      SignalName = "GPIO_MIPI_SEC_POW_EN_AVEE";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_MIPI_SEC_POW_EN_AVDD):
      SignalName = "GPIO_MIPI_SEC_POW_EN_AVDD";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_THC_WOT):
      SignalName = "GPIO_THC_WOT";
      break;
    case GPIO_NATIVE_FUNCTION_GET_SIGNAL (GPIO_SATA_DEVSLP):
      SignalName = "GPIO_SATA_DEVSLP";
      break;
    default:
      SignalName = "UNKNOWN_SIGNAL";
      break;
  }

  AsciiSPrint (PadFunctionNameBuffer, PadFunctionBufferSize, "%s_%d", SignalName, SignalFunctionIndex);

  return PadFunctionNameBuffer;
}
