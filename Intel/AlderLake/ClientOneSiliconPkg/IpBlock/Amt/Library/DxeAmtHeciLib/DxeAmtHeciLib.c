/** @file
  This is a library for Amt Heci Message functionality.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <PiDxe.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/HobLib.h>
#include <Library/BaseLib.h>
#include <Library/TimerLib.h>
#include <Library/DxeMeLib.h>
#include <Library/RngLib.h>
#include <Protocol/HeciProtocol.h>
#include <AsfMsgs.h>
#include <AmthiMsgs.h>
#include <CoreBiosMsg.h>
#include <AmtConfig.h>
#include <WifiProfileSyncAsfMsgs.h>

//
// ME Client - AMT/ASF
//

//
// Management Control Command
//

/**
  Start ASF Watch Dog Timer.
  The WDT will be started only if AMT WatchDog policy is enabled and corresponding timer value is not zero.

  @param[in] WatchDogType         Which kind of WatchDog, ASF OS WatchDog Timer setting or ASF BIOS WatchDog Timer setting

**/
VOID
AsfStartWatchDog (
  IN  UINT8                       WatchDogType
  )
{
  EFI_STATUS        Status;
  UINT32            Length;
  ASF_START_WDT     AsfStartWdt;
  HECI_PROTOCOL     *Heci;
  UINT32            MeStatus;
  UINT32            MeMode;
  UINT16            WaitTimer;
  EFI_HOB_GUID_TYPE *GuidHob;
  AMT_PEI_CONFIG    *AmtPeiConfig;

  GuidHob = GetFirstGuidHob (&gAmtPolicyHobGuid);
  if (GuidHob == NULL) {
    DEBUG ((DEBUG_INFO, "Get AMT WatchDog policy fail, don't start ASF WDT\n"));
    return;
  }
  AmtPeiConfig = (AMT_PEI_CONFIG *) GET_GUID_HOB_DATA (GuidHob);

  if (WatchDogType == ASF_START_BIOS_WDT) {
    WaitTimer = AmtPeiConfig->WatchDogTimerBios;
  } else {
    WaitTimer = AmtPeiConfig->WatchDogTimerOs;
  }
  if (WaitTimer == 0) {
    DEBUG ((DEBUG_ERROR, "Timeout value is 0, unable to start ASF WDT\n"));
    return;
  }

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Heci protocol does not exist, unable to start ME WDT\n"));
    return;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    DEBUG ((DEBUG_ERROR, "MeMode is %x, unable to start ASF WDT\n", MeMode));
    return;
  }

  ///
  /// Send WDT message when ME is ready.  Do not care about if ME FW INIT is completed.
  ///
  Status = Heci->GetMeStatus (&MeStatus);
  if (EFI_ERROR (Status) || (ME_STATUS_ME_STATE_ONLY (MeStatus) != ME_READY)) {
    DEBUG ((DEBUG_ERROR, "MeStatus is %x, unable to start ASF WDT\n", MeStatus));
    return;
  }

  DEBUG ((DEBUG_INFO, "AsfStartWatchDog () - Starting ASF WDT with timeout %d seconds \n", WaitTimer));

  AsfStartWdt.AsfHeader.Fields.Command           = ASF_MESSAGE_COMMAND_MANAGEMENT_CONTROL;
  AsfStartWdt.AsfHeader.Fields.ByteCount         = ASF_MESSAGE_BYTE_COUNT_MAP (ASF_START_WDT);
  AsfStartWdt.AsfHeader.Fields.SubCommand        = ASF_MESSAGE_SUBCOMMAND_START_WATCH_DOG_TIMER;
  AsfStartWdt.AsfHeader.Fields.VersionNumber     = ASF_VERSION;
  AsfStartWdt.EventSensorType                    = ASF_EVENT_SENSOR_TYPE_WATCHDOG2;
  AsfStartWdt.EventType                          = ASF_EVENT_TYPE_SENSOR_SPECIFIC;
  AsfStartWdt.EventOffset                        = ASF_EVENT_OFFSET_TIMER_EXPIRED;
  AsfStartWdt.EventSeverity                      = ASF_EVENT_SEVERITY_CODE_CRITICAL;
  AsfStartWdt.SensorDevice                       = ASF_SENSOR_DEVICE;
  AsfStartWdt.SensorNumber                       = ASF_SENSOR_NUMBER;
  AsfStartWdt.Entity                             = ASF_ENTITY_UNSPECIFIED;
  AsfStartWdt.EntityInstance                     = ASF_ENTITY_INSTANCE_UNSPECIFIED;
  AsfStartWdt.EventData[0]                       = ASF_WD_EVENT_DATA1;
  if (WatchDogType == ASF_START_BIOS_WDT) {
    AsfStartWdt.EventSourceType                  = ASF_EVENT_SOURCE_TYPE_PLATFORM_FIRMWARE;
    AsfStartWdt.EventData[1]                     = ASF_WD_EVENT_DATA2_BIOS_TIMEOUT;
  } else {
    AsfStartWdt.EventSourceType                  = ASF_EVENT_SOURCE_TYPE_OS;
    AsfStartWdt.EventData[1]                     = ASF_WD_EVENT_DATA2_OS_TIMEOUT;
  }

  AsfStartWdt.TimeoutLow                         = (UINT8) WaitTimer;
  AsfStartWdt.TimeoutHigh                        = (UINT8) (WaitTimer >> 8);
  Length                                         = sizeof (ASF_START_WDT);

  Status = Heci->SendMsg (
                   HECI1_DEVICE,
                   (UINT32 *) &AsfStartWdt,
                   Length,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Unable to start ASF WDT, Status = %r\n", Status));
  }

}

/**
  Stop ASF Watch Dog Timer HECI message.

**/
VOID
AsfStopWatchDog (
  VOID
  )
{
  EFI_STATUS           Status;
  UINT32               Length;
  ASF_STOP_WDT         AsfStopWdt;
  UINT32               MeStatus;
  UINT32               MeMode;
  HECI_PROTOCOL        *Heci;

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Heci protocol does not exist, unable to stop ASF WDT\n"));
    return;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    DEBUG ((DEBUG_ERROR, "MeMode is %x, unable to stop ASF WDT\n", MeMode));
    return;
  }

  ///
  /// Send WDT message when ME is ready. Do not care about if ME FW INIT is completed.
  ///
  Status = Heci->GetMeStatus (&MeStatus);
  if (EFI_ERROR (Status) || (ME_STATUS_ME_STATE_ONLY (MeStatus) != ME_READY)) {
    DEBUG ((DEBUG_ERROR, "MeStatus is %x, unable to stop ASF WDT\n", MeStatus));
    return;
  }

  DEBUG ((DEBUG_INFO, "AsfStopWatchDog () - Stopping ASF WDT...\n"));
  AsfStopWdt.AsfHeader.Fields.Command        = ASF_MESSAGE_COMMAND_MANAGEMENT_CONTROL;
  AsfStopWdt.AsfHeader.Fields.ByteCount      = ASF_MESSAGE_BYTE_COUNT_MAP (ASF_STOP_WDT);
  AsfStopWdt.AsfHeader.Fields.SubCommand     = ASF_MESSAGE_SUBCOMMAND_STOP_WATCH_DOG_TIMER;
  AsfStopWdt.AsfHeader.Fields.VersionNumber  = ASF_VERSION;
  Length                                     = sizeof (ASF_STOP_WDT);

  Status = Heci->SendMsg (
                   HECI1_DEVICE,
                   (UINT32 *) &AsfStopWdt,
                   Length,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Unable to stop ASF WDT, Status = %r\n", Status));
  }
}

/**
  This function is deprecated.
  Use ASF_GetRsePassword to get disk password from the FW

  @param[in,out]   Password            Preallocated buffer to save c string
                                       password to. It has to be at least 32
                                       characters wide.

  @retval EFI_SUCCESS                  Buffer Password contains returned password
  @retval EFI_NOT_FOUND                Either there is no password in AMT memory
                                       or Heci communication failed
  @retval EFI_DEVICE_ERROR             Failed to initialize HECI
  @retval EFI_TIMEOUT                  HECI is not ready for communication
  @retval EFI_UNSUPPORTED              Current ME mode doesn't support send this function
**/
EFI_STATUS
GetRsePassword (
  IN OUT CHAR16                       *Password
  )
{
  return EFI_UNSUPPORTED;
}

/**
  Use GetRsePassword to get disk password from the FW

  @param[in,out]   Password            Preallocated buffer to save string password to.
                                       It has to be at least 32 characters wide.

  @retval EFI_SUCCESS                  Buffer Password contains returned password
  @retval EFI_NOT_FOUND                Either there is no password in AMT memory or Heci communication failed
  @retval EFI_DEVICE_ERROR             Failed to initialize HECI
  @retval EFI_TIMEOUT                  HECI is not ready for communication
  @retval EFI_UNSUPPORTED              Current ME mode doesn't support send this function
**/
EFI_STATUS
GetRsePasswordV2 (
  IN OUT CHAR8                         *Password
  )
{
  EFI_STATUS                 Status;
  HECI_PROTOCOL              *Heci;
  UINT32                     Length;
  UINT32                     RecvLength;
  GET_RSE_PASSWORD_BUFFER    GetRsePassword;
  UINT32                     MeMode;

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  ZeroMem (&GetRsePassword, sizeof (GET_RSE_PASSWORD_BUFFER));
  GetRsePassword.Request.AsfHeader.Fields.Command       = ASF_MESSAGE_COMMAND_ASF_CONFIGURATION;
  GetRsePassword.Request.AsfHeader.Fields.ByteCount     = ASF_MESSAGE_BYTE_COUNT_MAP (GET_RSE_PASSWORD);
  GetRsePassword.Request.AsfHeader.Fields.SubCommand    = ASF_MESSAGE_SUBCOMMAND_GET_RSE_PASSWORD;
  GetRsePassword.Request.AsfHeader.Fields.VersionNumber = ASF_VERSION;
  Length                                                = sizeof (GET_RSE_PASSWORD);
  RecvLength                                            = sizeof (GET_RSE_PASSWORD_RESPONSE);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *) &GetRsePassword,
                   Length,
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get RSE Password status : %r\n", Status));
    return Status;
  }

  if (GetRsePassword.Response.AsfHeader.Fields.Command       != ASF_MESSAGE_COMMAND_ASF_CONFIGURATION ||
      GetRsePassword.Response.AsfHeader.Fields.ByteCount      > ASF_MESSAGE_BYTE_COUNT_MAP (GET_RSE_PASSWORD_RESPONSE) ||
      GetRsePassword.Response.AsfHeader.Fields.SubCommand    != ASF_MESSAGE_SUBCOMMAND_GET_RSE_PASSWORD ||
      GetRsePassword.Response.AsfHeader.Fields.VersionNumber != ASF_VERSION) {
    ZeroMem (&GetRsePassword.Response, sizeof (GET_RSE_PASSWORD_RESPONSE));
    return EFI_NOT_FOUND;
  }

  DEBUG ((DEBUG_INFO, "Received %d long password.\n", GetRsePassword.Response.PasswordLength));

  if (GetRsePassword.Response.PasswordLength > RSE_PASSWORD_MAX_LENGTH || GetRsePassword.Response.PasswordLength == 0) {
    ZeroMem (&GetRsePassword.Response, sizeof (GET_RSE_PASSWORD_RESPONSE));
    return EFI_NOT_FOUND;
  }

  CopyMem (Password, GetRsePassword.Response.Password, GetRsePassword.Response.PasswordLength);
  //
  // Erase temporary passwords from memory
  //
  ZeroMem (&GetRsePassword.Response, sizeof (GET_RSE_PASSWORD_RESPONSE));
  return EFI_SUCCESS;
}

//
// Messaging Command
//

/**
  Send secure erase operation status using PET

  @param[in]    OperationResult   Status of secure erase operation

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_DEVICE_ERROR        HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT             HECI does not return the buffer before timeout
  @retval EFI_BUFFER_TOO_SMALL    Message Buffer is too small for the Acknowledge
**/
EFI_STATUS
SendRsePetAlert (
  IN EFI_STATUS                        OperationResult
  )
{
  EFI_STATUS                           Status;
  PET_ALERT                            RsePetAlert;
  HECI_PROTOCOL                        *Heci;
  UINT32                               Length;
  UINT32                               MeMode;

  DEBUG ((DEBUG_INFO, "SecureErase::SendRsePetAlert (%r)\n", OperationResult));

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  RsePetAlert.AsfHeader.Fields.Command         = ASF_MESSAGE_COMMAND_MESSAGE;
  RsePetAlert.AsfHeader.Fields.ByteCount       = ASF_MESSAGE_BYTE_COUNT_MAP (PET_ALERT);
  RsePetAlert.AsfHeader.Fields.SubCommand      = ASF_MESSAGE_SUBCOMMAND_NORETRANSMIT;
  RsePetAlert.AsfHeader.Fields.VersionNumber   = ASF_VERSION;
  RsePetAlert.EventSensorType                  = ASF_EVENT_SENSOR_TYPE_SYS_FW_ERR_PROG;
  RsePetAlert.EventType                        = ASF_EVENT_TYPE_SENSOR_SPECIFIC;
  RsePetAlert.EventSourceType                  = ASF_EVENT_SOURCE_TYPE_ASF10;
  RsePetAlert.SensorDevice                     = ASF_SENSOR_DEVICE;
  RsePetAlert.Sensornumber                     = ASF_SENSOR_NUMBER;
  RsePetAlert.Entity                           = ASF_ENTITY_BIOS;
  RsePetAlert.EntityInstance                   = 0xFF;
  RsePetAlert.EventData1                       = ASF_RSE_EVENT_DATA0_EVENT_DATA_SET_BY_OEM;
  RsePetAlert.EventData2                       = ASF_RSE_EVENT_DATA1_REMOTE_SECURE_ERASE;
  RsePetAlert.EventData4                       = 0x00;
  RsePetAlert.EventData5                       = 0x00;

  if (EFI_ERROR (OperationResult)) {
    RsePetAlert.EventOffset                    = ASF_EVENT_OFFSET_REMOTE_SECURE_ERASE_FW_ERROR;
    RsePetAlert.EventSeverity                  = ASF_EVENT_SEVERITY_CODE_CRITICAL;
    if (OperationResult == EFI_UNSUPPORTED) {
      RsePetAlert.EventData3                   = ASF_RSE_EVENT_DATA2_UNSUPPORTED;
    } else if (OperationResult == EFI_ACCESS_DENIED) {
      RsePetAlert.EventData3                   = ASF_RSE_EVENT_DATA2_DRIVE_AUTH_FAILURE;
    } else if (OperationResult == EFI_DEVICE_ERROR) {
      RsePetAlert.EventData3                   = ASF_RSE_EVENT_DATA2_GENERAL_FAILURE;
    } else {
      RsePetAlert.EventData3                   = ASF_RSE_EVENT_DATA2_GENERAL_FAILURE;
    }
  } else {
    RsePetAlert.EventOffset                    = ASF_EVENT_OFFSET_REMOTE_SECURE_ERASE_FW_PROGRESS;
    RsePetAlert.EventSeverity                  = ASF_EVENT_SEVERITY_CODE_NONCRITICAL;
    RsePetAlert.EventData3                     = ASF_RSE_EVENT_DATA2_SECURE_ERASE_SUCCESS;
  }

  Length = sizeof (PET_ALERT);
  Status = Heci->SendMsg (
                   HECI1_DEVICE,
                   (UINT32*)&RsePetAlert,
                   Length,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );

  return Status;

}


/**
  Send Remote Platform Erase operation status using PET

  @param[in]    RpeOperationCode    PET Alert Code for the RPE Operation.
  @param[in]    RpeDevice           Device for which the RPE OperationCode is reported.
  @param[in]    RpeOperationStatus  Reports if RPE Operation is in progress or failure.

  @retval EFI_UNSUPPORTED           Current ME mode doesn't support this function
  @retval EFI_SUCCESS               Command succeeded
  @retval EFI_DEVICE_ERROR          HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT               HECI does not return the buffer before timeout
  @retval EFI_BUFFER_TOO_SMALL      Message Buffer is too small for the Acknowledge
**/
EFI_STATUS
SendRpePetAlert (
  IN UINT8                        RpeOperationCode,
  IN UINT8                        RpeDevice,
  IN UINT8                        RpeOperationStatus
  )
{
  EFI_STATUS                           Status;
  PET_ALERT                            RpePetAlert;
  HECI_PROTOCOL                        *Heci;
  UINT32                               Length;
  UINT32                               MeMode;

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  RpePetAlert.AsfHeader.Fields.Command         = ASF_MESSAGE_COMMAND_MESSAGE;
  RpePetAlert.AsfHeader.Fields.ByteCount       = ASF_MESSAGE_BYTE_COUNT_MAP (PET_ALERT);
  RpePetAlert.AsfHeader.Fields.SubCommand      = ASF_MESSAGE_SUBCOMMAND_NORETRANSMIT;
  RpePetAlert.AsfHeader.Fields.VersionNumber   = ASF_VERSION;
  RpePetAlert.EventSensorType                  = ASF_EVENT_SENSOR_TYPE_SYS_FW_ERR_PROG;
  RpePetAlert.EventType                        = ASF_EVENT_TYPE_SENSOR_SPECIFIC;
  RpePetAlert.EventSourceType                  = ASF_EVENT_SOURCE_TYPE_ASF10;
  RpePetAlert.EventSeverity                    = ASF_EVENT_SEVERITY_CODE_NONCRITICAL;
  RpePetAlert.SensorDevice                     = ASF_SENSOR_DEVICE;
  RpePetAlert.Sensornumber                     = ASF_SENSOR_NUMBER;
  RpePetAlert.Entity                           = ASF_ENTITY_BIOS;
  RpePetAlert.EntityInstance                   = 0xFF;
  RpePetAlert.EventData1                       = ASF_RPE_EVENT_DATA0_EVENT_DATA_SET_BY_OEM;
  RpePetAlert.EventData2                       = ASF_RPE_EVENT_DATA2_REMOTE_PLATFORM_ERASE;
  RpePetAlert.EventData5                       = 0x00;

  //
  // Update EventData values for sending RPE Alert to ASF.
  //
  RpePetAlert.EventData3                       = RpeOperationCode;
  RpePetAlert.EventData4                       = RpeDevice;
  RpePetAlert.EventOffset                      = RpeOperationStatus;

  Length = sizeof (PET_ALERT);
  Status = Heci->SendMsg (
                   HECI1_DEVICE,
                   (UINT32*)&RpePetAlert,
                   Length,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );

  return Status;
}

//
// LAN Command
//

/**
  This message is sent to switch active LAN interface.

  @param[in] ActiveInterface      Active Lan Interface
                                    0: As defined in FIT
                                    1: Integrated LAN
                                    2: Discrete LAN

  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_DEVICE_ERROR        HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT             HECI does not return the buffer before timeout
**/
EFI_STATUS
AmtSetActiveLanInterface (
  IN UINT8                        ActiveInterface
  )
{
  EFI_STATUS                          Status;
  HECI_PROTOCOL                       *Heci;
  AMT_SET_ACTIVE_LAN_INTERFACE_BUFFER SetActiveLanInterface;
  UINT32                              MeMode;
  UINT32                              RecvLength;

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  if (MeIsAfterEndOfPost ()) {
    return EFI_UNSUPPORTED;
  }

  SetActiveLanInterface.Request.AsfHeader.Fields.Command       = ASF_MESSAGE_COMMAND_LAN;
  SetActiveLanInterface.Request.AsfHeader.Fields.ByteCount     = ASF_MESSAGE_BYTE_COUNT_MAP (AMT_SET_ACTIVE_LAN_INTERFACE);
  SetActiveLanInterface.Request.AsfHeader.Fields.SubCommand    = ASF_MESSAGE_SUBCOMMAND_SET_ACTIVE_LAN;
  SetActiveLanInterface.Request.AsfHeader.Fields.VersionNumber = ASF_VERSION;
  SetActiveLanInterface.Request.ActiveInterface                = ActiveInterface;
  RecvLength                                                   = sizeof (AMT_SET_ACTIVE_LAN_INTERFACE_RESPONSE);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *) &SetActiveLanInterface,
                   sizeof (AMT_SET_ACTIVE_LAN_INTERFACE),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Set Active Lan Interface failed %r\n", Status));
    return Status;
  }

  if (SetActiveLanInterface.Response.Status != AmtStatusSuccess) {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}

/**
  This message is sent to get active LAN interface.

  @param[out] ActiveInterface     Active Lan Interface
                                    0: As defined in FIT
                                    1: Integrated LAN
                                    2: Discrete LAN

  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_DEVICE_ERROR        HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT             HECI does not return the buffer before timeout
**/
EFI_STATUS
AmtGetActiveLanInterface (
  OUT UINT8                          *ActiveInterface
  )
{
  EFI_STATUS                          Status;
  HECI_PROTOCOL                       *Heci;
  AMT_GET_ACTIVE_LAN_INTERFACE_BUFFER GetActiveLanInterface;
  UINT32                              MeMode;
  UINT32                              RecvLength;

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  if (MeIsAfterEndOfPost ()) {
    return EFI_UNSUPPORTED;
  }

  GetActiveLanInterface.Request.AsfHeader.Fields.Command       = ASF_MESSAGE_COMMAND_LAN;
  GetActiveLanInterface.Request.AsfHeader.Fields.ByteCount     = ASF_MESSAGE_BYTE_COUNT_MAP (AMT_GET_ACTIVE_LAN_INTERFACE);
  GetActiveLanInterface.Request.AsfHeader.Fields.SubCommand    = ASF_MESSAGE_SUBCOMMAND_GET_ACTIVE_LAN;
  GetActiveLanInterface.Request.AsfHeader.Fields.VersionNumber = ASF_VERSION;
  RecvLength                                                   = sizeof (AMT_GET_ACTIVE_LAN_INTERFACE_RESPONSE);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *) &GetActiveLanInterface,
                   sizeof (AMT_GET_ACTIVE_LAN_INTERFACE),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get Active Lan Interface failed %r\n", Status));
    return Status;
  }

  if (GetActiveLanInterface.Response.Status != AmtStatusSuccess) {
    return EFI_DEVICE_ERROR;
  }

  *ActiveInterface = GetActiveLanInterface.Response.ActiveInterface;
  return EFI_SUCCESS;
}

//
// KVM Command
//

/**
  This is used to send KVM request message to Intel ME. When
  Bootoptions indicate that a KVM session is requested then BIOS
  will send this message before any graphical display output to
  ensure that FW is ready for KVM session.

  @param[in] QueryType            0 - Query Request
                                  1 - Cancel Request
  @param[out] ResponseCode        1h - Continue, KVM session established.
                                  2h - Continue, KVM session cancelled.

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_DEVICE_ERROR        HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT             HECI does not return the buffer before timeout
  @retval EFI_BUFFER_TOO_SMALL    Message Buffer is too small for the Acknowledge
**/
EFI_STATUS
AmtQueryKvm (
  IN  UINT32                      QueryType,
  OUT UINT32                      *ResponseCode
  )
{
  EFI_STATUS                      Status;
  HECI_PROTOCOL                   *Heci;
  AMT_QUERY_KVM_BUFFER            QueryKvm;
  UINT32                          RecvLength;
  UINT16                          TimeOut;
  UINT32                          MeMode;

  TimeOut = 0;
  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  QueryKvm.Request.AsfHeader.Data                 = 0;
  QueryKvm.Request.AsfHeader.Fields.Command       = ASF_MESSAGE_COMMAND_KVM;
  QueryKvm.Request.AsfHeader.Fields.ByteCount     = ASF_MESSAGE_BYTE_COUNT_MAP (AMT_QUERY_KVM);
  QueryKvm.Request.AsfHeader.Fields.SubCommand    = ASF_MESSAGE_SUBCOMMAND_KVM_QUERY;
  QueryKvm.Request.AsfHeader.Fields.VersionNumber = ASF_VERSION;
  QueryKvm.Request.QueryType                      = QueryType;
  RecvLength                                      = sizeof (AMT_QUERY_KVM_RESPONSE);

  if (QueryType == QueryRequest) {
    Status = Heci->SendMsg (
                     HECI1_DEVICE,
                     (UINT32 *) &QueryKvm,
                     sizeof (AMT_QUERY_KVM),
                     BIOS_ASF_HOST_ADDR,
                     HECI_ASF_MESSAGE_ADDR
                     );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Send Query KVM failed %r\n", Status));
      return Status;
    }

    //
    // BIOS starts counting up to 8 minutes to wait for Query Request response.
    //
    TimeOut = 0;
    do {
      RecvLength = sizeof (AMT_QUERY_KVM_RESPONSE);
      Status = Heci->ReadMsg (
                       HECI1_DEVICE,
                       NON_BLOCKING,
                       (UINT32 *) &QueryKvm,
                       &RecvLength
                       );

      MicroSecondDelay (KVM_STALL_1_SECOND);
      TimeOut++;
      if (TimeOut > KVM_MAX_WAIT_TIME) {
        DEBUG ((DEBUG_ERROR, "Read Query KVM timeout\n"));
        return EFI_TIMEOUT;
      }
    } while (EFI_ERROR (Status));
  } else {
    //
    // BIOS follows common requirement (wait 5 seconds with max 3 retries) for Cancel Request.
    //
    Status = Heci->SendwAck (
                     HECI1_DEVICE,
                     (UINT32 *) &QueryKvm,
                     sizeof (AMT_QUERY_KVM),
                     &RecvLength,
                     BIOS_ASF_HOST_ADDR,
                     HECI_ASF_MESSAGE_ADDR
                     );
  }

  *ResponseCode = QueryKvm.Response.ResponseCode;

  return Status;
}

//
// MAC Commands
//

/**
  This message is sent by the BIOS in order to pass MAC Address
  which should be used for LAN in a dock.

  @param[in] Enabled              Determines if MAC Passthrough should be used
  @param[in] Mac Address          MAC Address to be used

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_DEVICE_ERROR        HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT             HECI does not return the buffer before timeout
  @retval EFI_BUFFER_TOO_SMALL    Message Buffer is too small for the Acknowledge
**/
EFI_STATUS
AmtSetMacPassthrough (
  IN UINT8                       Enabled,
  IN UINT8                       *MacAddress
  )
{
  EFI_STATUS                     Status;
  HECI_PROTOCOL                  *Heci;
  AMT_SET_MAC_PASSTHROUGH_BUFFER SetMacPassthrough;
  UINT32                         MeMode;
  UINT32                         RecvLength;

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  SetMacPassthrough.Request.AsfHeader.Data                 = 0;
  SetMacPassthrough.Request.AsfHeader.Fields.Command       = ASF_MESSAGE_COMMAND_MAC;
  SetMacPassthrough.Request.AsfHeader.Fields.ByteCount     = ASF_MESSAGE_BYTE_COUNT_MAP (AMT_SET_MAC_PASSTHROUGH);
  SetMacPassthrough.Request.AsfHeader.Fields.SubCommand    = ASF_MESSAGE_SUBCOMMAND_SET_MAC_PASSTHROUGH;
  SetMacPassthrough.Request.AsfHeader.Fields.VersionNumber = ASF_VERSION;

  SetMacPassthrough.Request.Enabled                        = Enabled;
  CopyMem (&SetMacPassthrough.Request.MacAddress, MacAddress, MAC_ADDRESS_SIZE);

  RecvLength = sizeof (AMT_SET_MAC_PASSTHROUGH_RESPONSE);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *)&SetMacPassthrough,
                   sizeof (AMT_SET_MAC_PASSTHROUGH),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  return Status;
}

/**
  Check Amt Reponse message header to see if it was valid

  @param[in] SubCommand   Sub Command for message
  @param[in] AsfHeader    ASF message header to check

  @retval EFI_SUCCESS            Header is valid
  @retval EFI_DEVICE_ERROR       Asf Header is invalid
**/
EFI_STATUS
CheckAmtResponseHeader (
  IN UINT8                                  SubCommand,
  IN ASF_MESSAGE_HEADER                     AsfHeader
  )
{

  if ((AsfHeader.Fields.Command != ASF_MESSAGE_COMMAND_ASF_CONFIGURATION) ||
      (AsfHeader.Fields.SubCommand != SubCommand) ||
      (AsfHeader.Fields.VersionNumber != ASF_VERSION)) {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}

/**
  Decode Amt Status to EFI Status

  @param[in] AmtStatus           AMT Status to decode

  @retval EFI_SUCCESS            Command succeeded
  @retval EFI_INVALID_PARAMETER  Invalid Boot Options State
  @retval EFI_NOT_FOUND          HECI Communication failed
**/
EFI_STATUS
DecodeAmtStatusResponse (
  IN AMT_STATUS                    AmtStatus
  )
{
  EFI_STATUS                       Status;

  switch (AmtStatus) {
    case AmtStatusSuccess:
      Status = EFI_SUCCESS;
      break;
    case AmtStatusInternalError:
    case AmtStatusInvalidMessageLength:
    case AmtStatusInvalidParameter:
      Status = EFI_INVALID_PARAMETER;
      break;
    default:
      Status = EFI_NOT_FOUND;
      break;
  }

  return Status;
}

/**
  Set UEFI Boot Options States that are available for AMT to use by sending ASF
  command through HECI

  @param[in] AmtBootControl      AMT boot control

  @retval EFI_UNSUPPORTED        Current ME mode doesn't support this function
  @retval EFI_SUCCESS            Command succeeded
  @retval EFI_INVALID_PARAMETER  Invalid Boot Options State
  @retval EFI_NOT_FOUND          HECI Communication failed
  @retval EFI_DEVICE_ERROR       ASF Response is invalid
**/
EFI_STATUS
AsfSetUefiBootOptionsState (
  IN AMT_BOOT_CONTROL      AmtBootControl
  )
{
  EFI_STATUS                          Status;
  SET_UEFI_BOOT_OPTIONS_STATE_BUFFER  SetBootOptionsState;
  HECI_PROTOCOL                       *Heci;
  UINT32                              MeMode;
  UINT32                              RecvLength;

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Heci protocol does not exist, unable to set boot options state\n"));
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  ZeroMem (&SetBootOptionsState, sizeof (GET_BOOT_OPTIONS_BUFFER));
  SetBootOptionsState.Request.AsfHeader.Fields.Command       = ASF_MESSAGE_COMMAND_ASF_CONFIGURATION;
  SetBootOptionsState.Request.AsfHeader.Fields.ByteCount     = ASF_MESSAGE_BYTE_COUNT_MAP (SET_UEFI_BOOT_OPTIONS_STATE);
  SetBootOptionsState.Request.AsfHeader.Fields.SubCommand    = ASF_MESSAGE_SUBCOMMAND_SET_UEFI_BOOT_OPTIONS_STATE;
  SetBootOptionsState.Request.AsfHeader.Fields.VersionNumber = ASF_VERSION;
  SetBootOptionsState.Request.AmtBootControl                 = AmtBootControl;
  RecvLength                                                 = sizeof (SET_UEFI_BOOT_OPTIONS_STATE_RESPONSE);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *)&SetBootOptionsState,
                   sizeof (SET_UEFI_BOOT_OPTIONS_STATE),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    return Status;
  }
  Status = CheckAmtResponseHeader (ASF_MESSAGE_SUBCOMMAND_SET_UEFI_BOOT_OPTIONS_STATE, SetBootOptionsState.Response.AsfHeader);
  if (EFI_ERROR (Status)) {
    return EFI_DEVICE_ERROR;
  }

  Status = DecodeAmtStatusResponse (SetBootOptionsState.Response.Status);

  return Status;
}

/**
  Get UEFI Boot Options States that are available for AMT to use by sending ASF
  proper HECI Command.

  @param[out] AmtBootControl     AMT boot control

  @retval EFI_UNSUPPORTED        Current ME mode doesn't support this function
  @retval EFI_SUCCESS            Command succeeded
  @retval EFI_INVALID_PARAMETER  Invalid Boot Options State
  @retval EFI_NOT_FOUND          HECI Communication failed
  @retval EFI_DEVICE_ERROR       ASF Response is invalid
**/
EFI_STATUS
AsfGetUefiBootOptionsState (
    OUT AMT_BOOT_CONTROL      *AmtBootControl
  )
{
  EFI_STATUS                           Status;
  GET_UEFI_BOOT_OPTIONS_STATE_BUFFER   GetUefiBootOptionsState;
  HECI_PROTOCOL                        *Heci;
  UINT32                               MeMode;
  UINT32                               RecvLength;

  if (AmtBootControl == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Heci protocol does not exist, unable to set boot options state\n"));
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  ZeroMem (&GetUefiBootOptionsState, sizeof (GET_UEFI_BOOT_OPTIONS_STATE_BUFFER));
  GetUefiBootOptionsState.Request.AsfHeader.Fields.Command       = ASF_MESSAGE_COMMAND_ASF_CONFIGURATION;
  GetUefiBootOptionsState.Request.AsfHeader.Fields.ByteCount     = ASF_MESSAGE_BYTE_COUNT_MAP (GET_UEFI_BOOT_OPTIONS_STATE);
  GetUefiBootOptionsState.Request.AsfHeader.Fields.SubCommand    = ASF_MESSAGE_SUBCOMMAND_GET_UEFI_BOOT_OPTIONS_STATE;
  GetUefiBootOptionsState.Request.AsfHeader.Fields.VersionNumber = ASF_VERSION;
  RecvLength                                                     = sizeof (GET_UEFI_BOOT_OPTIONS_STATE_RESPONSE);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *)&GetUefiBootOptionsState,
                   sizeof (GET_UEFI_BOOT_OPTIONS_STATE),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = CheckAmtResponseHeader (ASF_MESSAGE_SUBCOMMAND_GET_UEFI_BOOT_OPTIONS_STATE, GetUefiBootOptionsState.Response.AsfHeader);
  if (EFI_ERROR (Status)) {
    return EFI_DEVICE_ERROR;
  }

  *AmtBootControl = GetUefiBootOptionsState.Response.AmtBootControl;

  return Status;
}

/**
  Update UEFI Boot Options available to ASF by sending proper HECI Command

  @param[in] UefiBootOptions      UEFI Boot Options
  @param[in] NumOfUefiBootOptions Number of UEFI Boot Options

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_INVALID_PARAMETER   Invalid Boot Options State
  @retval EFI_NOT_FOUND           HECI Communication failed
  @retval EFI_OUT_OF_RESOURCES    Ran out of resources
  @retval EFI_DEVICE_ERROR        ASF Response is invalid
**/
EFI_STATUS
AsfUpdateUefiBootOptions (
  IN UEFI_BOOT_OPTION      *UefiBootOptions,
  IN UINT16                NumOfUefiBootOptions
  )
{
  EFI_STATUS                       Status;
  UPDATE_UEFI_BOOT_OPTIONS_BUFFER  *UpdateUefiBootOptions;
  HECI_PROTOCOL                    *Heci;
  UINT32                           MeMode;
  UINT32                           Length;
  UINT32                           RecvLength;

  if ((NumOfUefiBootOptions > MAX_UEFI_BOOT_OPTIONS)  || (UefiBootOptions == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Heci protocol does not exist, unable to set boot options state\n"));
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  Length = sizeof (UPDATE_UEFI_BOOT_OPTIONS) + (NumOfUefiBootOptions * sizeof (UEFI_BOOT_OPTION));

  UpdateUefiBootOptions = AllocateZeroPool (Length);
  if (UpdateUefiBootOptions == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  UpdateUefiBootOptions->Request.AsfHeader.Fields.Command       = ASF_MESSAGE_COMMAND_ASF_CONFIGURATION;
  UpdateUefiBootOptions->Request.AsfHeader.Fields.ByteCount     = ASF_MESSAGE_BYTE_COUNT (Length);
  UpdateUefiBootOptions->Request.AsfHeader.Fields.SubCommand    = ASF_MESSAGE_SUBCOMMAND_UPDATE_UEFI_BOOT_OPTIONS;
  UpdateUefiBootOptions->Request.AsfHeader.Fields.VersionNumber = ASF_VERSION;

  UpdateUefiBootOptions->Request.NumOfUefiBootOptions           = NumOfUefiBootOptions;
  CopyMem (UpdateUefiBootOptions->Request.UefiBootOptions, UefiBootOptions, (NumOfUefiBootOptions * sizeof (UEFI_BOOT_OPTION)));

  RecvLength                                                    = sizeof (UPDATE_UEFI_BOOT_OPTIONS_RESPONSE);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *)UpdateUefiBootOptions,
                   Length,
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    FreePool (UpdateUefiBootOptions);
    return Status;
  }

  Status = CheckAmtResponseHeader (ASF_MESSAGE_SUBCOMMAND_UPDATE_UEFI_BOOT_OPTIONS, UpdateUefiBootOptions->Response.AsfHeader);
  if (EFI_ERROR (Status)) {
    FreePool (UpdateUefiBootOptions);
    return EFI_DEVICE_ERROR;
  }

  Status = DecodeAmtStatusResponse (UpdateUefiBootOptions->Response.Status);

  FreePool (UpdateUefiBootOptions);

  return Status;
}

/**
  Get Boot Option with Parameters that being requested from ASF by sending proper
  HECI Command

  @param[out] UefiBootOptionWithParm    UEFI Boot Option with parameters

  @retval EFI_UNSUPPORTED               Current ME mode doesn't support this function
  @retval EFI_SUCCESS                   Command succeeded
  @retval EFI_INVALID_PARAMETER         Invalid Boot Options State
  @retval EFI_NOT_FOUND                 HECI Communication failed
  @retval EFI_DEVICE_ERROR              ASF Response is invalid
**/
EFI_STATUS
AsfGetUefiBootParameters (
  OUT UEFI_BOOT_OPTION_PARAMETER  *UefiBootOptionWithParm
  )
{
  EFI_STATUS                              Status;
  HECI_PROTOCOL                           *Heci;
  UINT32                                  MeMode;
  GET_UEFI_BOOT_OPTION_PARAMETERS_BUFFER  GetUefiBootOptions;
  UINT32                                  RecvLength;
  UINT32                                  Nonce;

  if (UefiBootOptionWithParm == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Heci protocol does not exist, unable to set boot options state\n"));
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  ZeroMem (&GetUefiBootOptions, sizeof (GET_UEFI_BOOT_OPTION_PARAMETERS_BUFFER));
  GetUefiBootOptions.Request.AsfHeader.Fields.Command       = ASF_MESSAGE_COMMAND_ASF_CONFIGURATION;
  GetUefiBootOptions.Request.AsfHeader.Fields.ByteCount     = ASF_MESSAGE_BYTE_COUNT_MAP (GET_UEFI_BOOT_PARAMETERS);
  GetUefiBootOptions.Request.AsfHeader.Fields.SubCommand    = ASF_MESSAGE_SUBCOMMAND_GET_UEFI_BOOT_PARAMETERS;
  GetUefiBootOptions.Request.AsfHeader.Fields.VersionNumber = ASF_VERSION;
  if (!GetRandomNumber32 (&Nonce)) {
    return EFI_NOT_FOUND;
  }
  GetUefiBootOptions.Request.Nonce = Nonce;

  RecvLength = sizeof (GET_UEFI_BOOT_OPTION_PARAMETERS_RESPONSE);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *)&GetUefiBootOptions,
                   sizeof (GET_UEFI_BOOT_PARAMETERS),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = CheckAmtResponseHeader (ASF_MESSAGE_SUBCOMMAND_GET_UEFI_BOOT_PARAMETERS, GetUefiBootOptions.Response.AsfHeader);
  if (EFI_ERROR (Status) || GetUefiBootOptions.Response.Nonce != Nonce) {
    return EFI_DEVICE_ERROR;
  }

  CopyMem (UefiBootOptionWithParm, &GetUefiBootOptions.Response.UefiBootOptionWithParams, sizeof (UEFI_BOOT_OPTION_PARAMETER));

  return Status;
}

/**
  Get Root Certificate Authority(CA) Certificate from ASF at specified Index by sending
  proper HECI Command

  @param[in]   Index             Index
  @param[out]  Cert              Root CA Certificate

  @retval EFI_UNSUPPORTED        Current ME mode doesn't support this function
  @retval EFI_SUCCESS            Command succeeded
  @retval EFI_INVALID_PARAMETER  Invalid Boot Options State
  @retval EFI_NOT_FOUND          HECI Communication failed
  @retval EFI_DEVICE_ERROR       ASF Response is invalid
**/
EFI_STATUS
AsfGetRootCaCertificate (
  IN UINT32                   Index,
  OUT ROOT_CA_CERTIFICATE     *Cert
  )
{
  EFI_STATUS                        Status;
  HECI_PROTOCOL                     *Heci;
  UINT32                            MeMode;
  GET_ROOT_CA_CERTIFICATE_BUFFER    GetRootCaCert;
  UINT32                            RecvLength;
  UINT32                            Nonce;

  if (Cert == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = gBS->LocateProtocol (
                  &gHeciProtocolGuid,
                  NULL,
                  (VOID **) &Heci
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Heci protocol does not exist, unable to set boot options state\n"));
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  GetRootCaCert.Request.AsfHeader.Fields.Command       = ASF_MESSAGE_COMMAND_ASF_CONFIGURATION;
  GetRootCaCert.Request.AsfHeader.Fields.ByteCount     = ASF_MESSAGE_BYTE_COUNT_MAP (GET_ROOT_CA_CERTIFICATE);
  GetRootCaCert.Request.AsfHeader.Fields.SubCommand    = ASF_MESSAGE_SUBCOMMAND_GET_ROOT_CA_CERTIFICATE;
  GetRootCaCert.Request.AsfHeader.Fields.VersionNumber = ASF_VERSION;

  if (!GetRandomNumber32 (&Nonce)) {
    return EFI_NOT_FOUND;
  }
  GetRootCaCert.Request.Nonce = Nonce;
  GetRootCaCert.Request.RootCACertIndex = Index;
  RecvLength = sizeof (GET_ROOT_CA_CERTIFICATE_RESPONSE);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *)&GetRootCaCert,
                   sizeof (GET_ROOT_CA_CERTIFICATE),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = CheckAmtResponseHeader (ASF_MESSAGE_SUBCOMMAND_GET_ROOT_CA_CERTIFICATE, GetRootCaCert.Response.AsfHeader);
  if (EFI_ERROR (Status) || GetRootCaCert.Response.Nonce != Nonce) {
    return EFI_DEVICE_ERROR;
  }

  CopyMem (Cert, &GetRootCaCert.Response.Cert, sizeof (ROOT_CA_CERTIFICATE));

  return Status;
}

/**
  Send One Click Recovery progress or error event using PET

  @param[in] EventOffset          ASF Event Data Codes for OCR Fw Progress/Error Events
  @param[in] EventData3           OCR Additional Event Data 3
  @param[in] EventData4           OCR Additional Event Data 4

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
**/
EFI_STATUS
SendOcrPetEvent (
  IN UINT8              EventOffset,
  IN UINT8              EventData3,
  IN UINT8              EventData4
  )
{
  EFI_STATUS            Status;
  PET_ALERT             OcrPetAlert;
  HECI_PROTOCOL         *Heci;
  UINT32                Length;
  UINT32                MeMode;

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  OcrPetAlert.AsfHeader.Fields.Command         = ASF_MESSAGE_COMMAND_MESSAGE;
  OcrPetAlert.AsfHeader.Fields.ByteCount       = ASF_MESSAGE_BYTE_COUNT_MAP (PET_ALERT);
  OcrPetAlert.AsfHeader.Fields.SubCommand      = ASF_MESSAGE_SUBCOMMAND_NORETRANSMIT;
  OcrPetAlert.AsfHeader.Fields.VersionNumber   = ASF_VERSION;
  OcrPetAlert.EventSensorType                  = ASF_EVENT_SENSOR_TYPE_SYS_FW_ERR_PROG;
  OcrPetAlert.EventType                        = ASF_EVENT_TYPE_SENSOR_SPECIFIC;
  OcrPetAlert.EventSourceType                  = ASF_EVENT_SOURCE_TYPE_ASF10;
  OcrPetAlert.EventSeverity                    = ASF_EVENT_SEVERITY_CODE_NONCRITICAL;
  OcrPetAlert.SensorDevice                     = ASF_SENSOR_DEVICE;
  OcrPetAlert.Sensornumber                     = ASF_SENSOR_NUMBER;
  OcrPetAlert.Entity                           = ASF_ENTITY_BIOS;
  OcrPetAlert.EntityInstance                   = 0xFF;

  OcrPetAlert.EventOffset                      = EventOffset;
  OcrPetAlert.EventData1                       = ASF_OCR_EVENT_DATA1_EVENT_DATA_SET_BY_OEM;
  OcrPetAlert.EventData2                       = ASF_OCR_EVENT_DATA2_EVENT_DATA_REASON;
  OcrPetAlert.EventData3                       = EventData3;
  OcrPetAlert.EventData4                       = EventData4;
  OcrPetAlert.EventData5                       = 0x00;

  Length = sizeof (PET_ALERT);
  Status = Heci->SendMsg (
                   HECI1_DEVICE,
                   (UINT32*)&OcrPetAlert,
                   Length,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );

  return Status;
}

/**
  This function is deprecated.
  Send One Click Recovery status using PET

  @param[in] EventType            OCR Event Type
  @param[in] LoadOptionType       OCR Load Option Type

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
**/
EFI_STATUS
SendOcrPetAlert (
  IN UINT8              EventType,
  IN UINT8              LoadOptionType
  )
{
  if (EventType != ASF_OCR_EVENT_DATA3_CSME_BOOT_OPTION_ADDED) {
    LoadOptionType = 0x00;
  }
  return SendOcrPetEvent(
            ASF_EVENT_OFFSET_ONE_CLICK_RECOVERY_PROGRESS,
            EventType,
            LoadOptionType
            );
}

//
// WiFi profile Command
//

/**
  Get the wireless profile name

  @param[in, out] Profile         Profile data defined in wifi profile protocol

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @return Others                  Error happened
**/
EFI_STATUS
AsfWifiGetProfileName (
  IN OUT WIFI_PROFILE_DATA        *Profile
  )
{
  EFI_STATUS                      Status;
  HECI_PROTOCOL                   *Heci;
  UINT32                          MeMode;
  UINT32                          RecvLength;
  GET_WIFI_PROFILE_NAME_BUFFER    *ProfileNameBuffer;

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Locate HECI protocol failed with status %r\n", Status));
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  // Set up the ASF message commands for profile info transfer
  ProfileNameBuffer = (GET_WIFI_PROFILE_NAME_BUFFER *) AllocateZeroPool (sizeof (GET_WIFI_PROFILE_NAME_BUFFER));
  if (ProfileNameBuffer == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  ProfileNameBuffer->Request.Data                   = 0;
  ProfileNameBuffer->Request.Fields.Command         = ASF_MESSAGE_COMMAND_ASF_CONFIGURATION;
  ProfileNameBuffer->Request.Fields.ByteCount       = ASF_MESSAGE_BYTE_COUNT_MAP (ASF_MESSAGE_HEADER);
  ProfileNameBuffer->Request.Fields.SubCommand      = ASF_MESSAGE_SUBCOMMAND_GET_WIFI_PROFILE_NAME;
  ProfileNameBuffer->Request.Fields.VersionNumber   = ASF_VERSION;
  RecvLength                                        = sizeof (GET_WIFI_PROFILE_NAME_BUFFER);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *) ProfileNameBuffer,
                   sizeof (ASF_MESSAGE_HEADER),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    FreePool (ProfileNameBuffer);
    return Status;
  }
  Profile->Status = ProfileNameBuffer->Response.Status;
  if (!EFI_ERROR (Status) && (ProfileNameBuffer->Response.Status == AmtStatusSuccess)) {
    CopyMem (&Profile->ProfileName, &ProfileNameBuffer->Response.ProfileName, PROFILE_NAME_SIZE);
    FreePool (ProfileNameBuffer);
    return Status;
  }
  DEBUG ((DEBUG_ERROR, "Failed to get profile name with status 0x%X\n", Profile->Status));
  FreePool (ProfileNameBuffer);
  return Status;
}

/**
  Get the wireless profile data

  @param[in, out] Profile         Profile data defined in wifi profile protocol

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @return Others                  Error happened
**/
EFI_STATUS
AsfWifiGetProfileData (
  IN OUT WIFI_PROFILE_DATA        *Profile
  )
{
  EFI_STATUS                      Status;
  HECI_PROTOCOL                   *Heci;
  UINT32                          MeMode;
  UINT32                          RecvLength;
  GET_WIFI_PROFILE_DATA_BUFFER    *ProfileDataBuffer;

 if (Profile->ProfileName == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Locate HECI protocol failed with status %r\n", Status));
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  // Set up the ASF message commands for profile info transfer
  ProfileDataBuffer = (GET_WIFI_PROFILE_DATA_BUFFER *) AllocateZeroPool (sizeof (GET_WIFI_PROFILE_DATA_BUFFER));
  if (ProfileDataBuffer == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  CopyMem (&ProfileDataBuffer->Request.Data, Profile->ProfileName, PROFILE_NAME_SIZE);
  ProfileDataBuffer->Request.AsfHeader.Fields.Command          = ASF_MESSAGE_COMMAND_ASF_CONFIGURATION;
  ProfileDataBuffer->Request.AsfHeader.Fields.ByteCount        = ASF_MESSAGE_BYTE_COUNT_MAP (ASF_PROFILE_MESSAGE_HEADER);
  ProfileDataBuffer->Request.AsfHeader.Fields.SubCommand       = ASF_MESSAGE_SUBCOMMAND_GET_WIFI_PROFILE_DATA;
  ProfileDataBuffer->Request.AsfHeader.Fields.VersionNumber    = ASF_VERSION;
  RecvLength                                                   = sizeof (GET_WIFI_PROFILE_DATA_BUFFER);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *) ProfileDataBuffer,
                   sizeof (ASF_PROFILE_MESSAGE_HEADER),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    FreePool (ProfileDataBuffer);
    return Status;
  }
  if (ProfileDataBuffer->Response.Profile.Status == AmtStatusSuccess) {
    CopyMem (Profile, &ProfileDataBuffer->Response.Profile, sizeof (WIFI_PROFILE_DATA));
    FreePool (ProfileDataBuffer);
    return Status;
  }
  Profile->Status = ProfileDataBuffer->Response.Profile.Status;
  DEBUG ((DEBUG_ERROR, "AsfWifiGetProfileData failed request with status 0x%X\n", Profile->Status));
  FreePool (ProfileDataBuffer);
  return Status;
}


/**
  Get the wireless profile specific 8021X client certificate

  @param[in, out] Profile         Profile data defined in wifi profile protocol

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @return Others                  Error happened
**/
EFI_STATUS
AsfWifiGetClientCertificate (
  IN OUT WIFI_PROFILE             *Profile
  )
{
  EFI_STATUS                      Status;
  HECI_PROTOCOL                   *Heci;
  UINT32                          MeMode;
  UINT32                          RecvLength;
  GET_WIFI_CLIENT_CERT_BUFFER     *CertBuffer;

  if (Profile->ProfileData->ProfileName == NULL) {
    return EFI_UNSUPPORTED;
  }

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Locate protocol failed with status 0x%X\n", Status));
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  // Set up the ASF message commands for profile info transfer
  CertBuffer = (GET_WIFI_CLIENT_CERT_BUFFER *) AllocateZeroPool (sizeof (GET_WIFI_CLIENT_CERT_BUFFER));
  if (CertBuffer == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  CopyMem (&CertBuffer->Request.Data, &Profile->ProfileData->ProfileName, PROFILE_NAME_SIZE);
  CertBuffer->Request.AsfHeader.Fields.Command         = ASF_MESSAGE_COMMAND_ASF_CONFIGURATION;
  CertBuffer->Request.AsfHeader.Fields.ByteCount       = ASF_MESSAGE_BYTE_COUNT_MAP (ASF_PROFILE_MESSAGE_HEADER);
  CertBuffer->Request.AsfHeader.Fields.SubCommand      = ASF_MESSAGE_SUBCOMMAND_GET_WIFI_8021X_CLIENT_CERT;
  CertBuffer->Request.AsfHeader.Fields.VersionNumber   = ASF_VERSION;
  RecvLength                                           = sizeof (GET_WIFI_CLIENT_CERT_BUFFER);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *) CertBuffer,
                   sizeof (ASF_PROFILE_MESSAGE_HEADER),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    return Status;
  }
  if (CertBuffer->Response.Cert.Status == AmtStatusSuccess) {
    CopyMem (Profile->Cert, &CertBuffer->Response.Cert, sizeof (WIFI_8021X_CLIENT_CERT));
    FreePool (CertBuffer);
    return Status;
  }
  Profile->Cert->Status = CertBuffer->Response.Cert.Status;
  DEBUG ((DEBUG_ERROR, "AsfWifiGetClientCertificate failed request with status 0x%X\n", Profile->Cert->Status));
  FreePool (CertBuffer);
  return Status;
}

/**
  Get the wireless profile specific 8021X client key

  @param[in, out] Profile         Profile data defined in wifi profile protocol

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @return Others                  Error happened
**/
EFI_STATUS
AsfWifiGetClientKey (
  IN OUT WIFI_PROFILE             *Profile
  )
{
  EFI_STATUS                      Status;
  HECI_PROTOCOL                   *Heci;
  UINT32                          MeMode;
  UINT32                          RecvLength;
  GET_WIFI_CLIENT_KEY_BUFFER      *KeyBuffer;

 if (Profile->ProfileData->ProfileName == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Locate protocol failed with status %r\n", Status));
    return Status;
  }

  Status = Heci->GetMeMode (&MeMode);
  if (EFI_ERROR (Status) || (MeMode != ME_MODE_NORMAL)) {
    return EFI_UNSUPPORTED;
  }

  // Set up the ASF message commands for profile info transfer
  KeyBuffer = (GET_WIFI_CLIENT_KEY_BUFFER *) AllocateZeroPool (sizeof (GET_WIFI_CLIENT_KEY_BUFFER));
  if (KeyBuffer == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  CopyMem (&KeyBuffer->Request.Data, &Profile->ProfileData->ProfileName, PROFILE_NAME_SIZE);
  KeyBuffer->Request.AsfHeader.Fields.Command          = ASF_MESSAGE_COMMAND_ASF_CONFIGURATION;
  KeyBuffer->Request.AsfHeader.Fields.ByteCount        = ASF_MESSAGE_BYTE_COUNT_MAP (ASF_PROFILE_MESSAGE_HEADER);
  KeyBuffer->Request.AsfHeader.Fields.SubCommand       = ASF_MESSAGE_SUBCOMMAND_GET_WIFI_8021X_CLIENT_KEY;
  KeyBuffer->Request.AsfHeader.Fields.VersionNumber    = ASF_VERSION;
  RecvLength                                           = sizeof (GET_WIFI_CLIENT_KEY_BUFFER);

  Status = Heci->SendwAck (
                   HECI1_DEVICE,
                   (UINT32 *) KeyBuffer,
                   sizeof (ASF_PROFILE_MESSAGE_HEADER),
                   &RecvLength,
                   BIOS_ASF_HOST_ADDR,
                   HECI_ASF_MESSAGE_ADDR
                   );
  if (EFI_ERROR (Status)) {
    return Status;
  }
  if (KeyBuffer->Response.Key.Status == AmtStatusSuccess) {
    CopyMem (Profile->Key, &KeyBuffer->Response.Key, sizeof (WIFI_8021X_CLIENT_KEY));
    FreePool (KeyBuffer);
    return Status;
  }
  Profile->Key->Status = KeyBuffer->Response.Key.Status;
  DEBUG ((DEBUG_ERROR, "AsfWifiGetClientKey failed request with status 0x%X\n", Profile->Key->Status));
  FreePool (KeyBuffer);
  return Status;
}