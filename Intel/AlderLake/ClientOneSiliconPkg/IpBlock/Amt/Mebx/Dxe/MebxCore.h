/** @file
  Include for AMT Bios Extentions Loader

@copyright
  INTEL CONFIDENTIAL
  Copyright 2004 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _MEBX_CORE_H_
#define _MEBX_CORE_H_

#define UNCONFIGURE_TIMEOUT                       60

//
// MEBX Software Class DXE Subclass Progress Code definitions.
//
#define EFI_SW_DXE_MEBX_OPROM_DONE                (EFI_OEM_SPECIFIC | 0x00000000)
#define EFI_SW_DXE_MEBX_RESET_ACTION              (EFI_OEM_SPECIFIC | 0x0000000a)

#define INVOKE_MEBX_BIT                           BIT3

//Reboot reason definitions
#define MEBX_RESET_NO_RESET                       0
#define MEBX_RESET_UNCONFIG_FINISHED              BIT0
#define MEBX_RESET_UNCONFIG_WITHOUT_PASSWORD      BIT1
#define MEBX_RESET_CPU_REPLACEMENT_USER_FEEDBACK  BIT2
#define MEBX_RESET_CPU_REPLACEMENT_RESET_REQUIRED BIT3
#define MEBX_RESET_SOL_SYNC                       BIT4
#define MEBX_RESET_WLAN_PWR_CONFIG_CHANGE         BIT5

/**
  Signal a event for Amt ready to boot.
**/
VOID
EFIAPI
MebxOnReadyToBoot (
  VOID
  );

#endif
