/** @file
  User Consent bitmap conversion and display definitions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2009 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

@par Specification Reference:
**/

#ifndef _MEBX_USER_CONSENT_H_
#define _MEBX_USER_CONSENT_H_


/**
  This function checks if User Consent flow should be executed

  @retval TRUE    User Consent flow should be executed
  @retval FALSE   User Consent flow shouldn't be executed, this includes also a situation where query failed due to HECI error
**/
BOOLEAN
IsUserConsentRequired (
  VOID
  );

/**
  Function reads bitmap from AMT and displays it as USER CONSENT. It continuosly scans keyboard for user input
  and reads HECI to see if it should update the picture on display.

  @retval EFI_SUCCESS         An user aborted user consent flow
  @retval EFI_PROTOCOL_ERROR  Cannot read user input
  @retval EFI_DEVICE_ERROR    User consent flow ended with an error in HECI, KBD or graphics
**/
EFI_STATUS
PerformUserConsent (
  VOID
  );

#endif // _MEBX_USER_CONSENT_H_
