/** @file
  Overclocking common library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/BaseLib.h>
#include <Library/OcCommonLib.h>
#include <OcMailbox.h>

/**
  Converts the input voltage target to the fixed point U12.2.10 Volt format or
  the Binary millivolts representation based on the ConversionType

  @param[in]   InputVoltageTarget     UINT16 voltage input value with fixed point U12.2.10 Volt format
                                      or Binary millivolts representation
  @param[out]  OutputVoltageTarget    A pointer to UINT16 voltage output value
  @param[in]   ConversionType         0:fixed point, 1:Binary millivolts
**/
VOID
ConvertVoltageTarget (
  IN   UINT16  InputVoltageTarget,
  OUT  UINT16  *OutputVoltageTarget,
  IN   UINT8   ConversionType
  )
{
  UINT32 Remainder;
  UINT64 Voltage;
  ///  Fixed point representation:
  ///
  ///  U12.2.10V format
  ///  | | | |
  ///  | | | v
  ///  | | v Exponent
  ///  | v Significand Size
  ///  v Size
  ///  Signed/Unsigned
  ///
  ///  Float Value = Significand x (Base ^ Exponent)
  ///  (Base ^ Exponent) = 2 ^ 10 = 1024
  ///
  Remainder = 0;

  if (InputVoltageTarget == 0) {
    *OutputVoltageTarget = 0;
    return;
  }

  if (ConversionType == CONVERT_TO_FIXED_POINT_VOLTS) {
    ///
    /// Input Voltage is in number of millivolts. Clip the input Voltage
    /// to the max allowed by the fixed point format
    ///
    if (InputVoltageTarget > MAX_TARGET_MV)
      InputVoltageTarget = MAX_TARGET_MV;

    ///
    /// InputTargetVoltage is the significand in mV. Need to convert to Volts
    ///
    Voltage = MultU64x32 (1024, InputVoltageTarget);
    *OutputVoltageTarget = (UINT16) DivU64x32Remainder (Voltage, MILLIVOLTS_PER_VOLT, &Remainder);

    if (Remainder >= 500) {
      *OutputVoltageTarget += 1;
    }
  } else if (ConversionType == CONVERT_TO_BINARY_MILLIVOLT) {
    ///
    /// InputVoltage is specified in fixed point representation, need to
    /// convert to millivolts
    ///
    Voltage = MultU64x32 (MILLIVOLTS_PER_VOLT, InputVoltageTarget);
    *OutputVoltageTarget = (UINT16) DivU64x32Remainder (Voltage, 1024, &Remainder);

    if (Remainder >= 500) {
      *OutputVoltageTarget += 1;
    }
  }
}
