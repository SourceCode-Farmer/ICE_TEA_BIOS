/** @file
  Overclocking early post initialization null library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include <Library/PeiOcLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>

/**
  Initializes pre-memory Overclocking settings in the processor.

  @param[in] SiPolicyPreMemPpi  Instance of SI_PREMEM_POLICY_PPI

  @retval EFI_SUCCESS
**/
EFI_STATUS
CpuOcInitPreMem (
  IN SI_PREMEM_POLICY_PPI *SiPolicyPreMemPpi
  )
{
  return EFI_SUCCESS;
}

/**
  Initializes post-memory Overclocking settings in the processor.

  @param[in] OverclockingtConfig      Pointer to Policy protocol instance
  @param[in] MpServices2Ppi            Pointer to this instance of the MpServices.

  @retval EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
CpuOcInitPostMem (
  IN OVERCLOCKING_PREMEM_CONFIG   *OverClockingConfig,
  IN EDKII_PEI_MP_SERVICES2_PPI   *MpServices2Ppi
  )
{
  return EFI_SUCCESS;
}

/**
  Initializes Overclocking settings in the processor.

  @param[in] *OcConfig                 - OcConfig Instance of OVERCLOCKING_PREMEM_CONFIG
  @param[in] *CpuConfigLibPreMemConfig - CpuConfigLibPreMemConfig Instance of CPU_CONFIG_LIB_PREMEM_CONFIG

  @retval EFI_SUCCESS
**/
EFI_STATUS
SaOcInit (
  IN OVERCLOCKING_PREMEM_CONFIG   *OcConfig,
  IN CPU_CONFIG_LIB_PREMEM_CONFIG *CpuConfigLibPreMemConfig
  )
{
  return EFI_SUCCESS;
}
