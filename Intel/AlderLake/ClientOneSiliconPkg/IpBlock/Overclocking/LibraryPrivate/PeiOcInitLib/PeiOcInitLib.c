/** @file
  Overclocking early post initializations.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include <Uefi.h>
#include <Library/CpuCommonLib.h>
#include <Library/PeiOcLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/CpuMailboxLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PostCodeLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/PeiMeLib.h>
#include <VoltageRegulatorCommands.h>
#include <Register/CommonMsr.h>
#include <Library/BaseOcFruLib.h>
#include <Library/PeiCpuInitFruLib.h>
#include <Library/CpuInfoFruLib.h>
#include <Ppi/Wdt.h>
#include <Register/Cpuid.h>

STATIC CPU_RESET_TYPE               mResetType                  = NO_RESET;
STATIC UINT8                        mNumActiveBigCores          = 0;

#define FIRST_THREAD                                         0
#define SECOND_THREAD                                        1
#define CPUID_NATIVE_MODEL_ID_INFO                           0x1A

typedef struct {
  UINT32           MailboxData;
  UINT32           MailboxStatus;
} PROCESSOR_MAILBOX_DATA;

/**
  Initializes Ring related overclocking settings.

  @param[in] OverClockingConfig      Pointer to CPU Overclocking Policy instance

  @retval EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
CpuOcInitRingSettings (
  IN OVERCLOCKING_PREMEM_CONFIG *OverClockingConfig
  )
{
  EFI_STATUS              Status;
  UINT32                  LibStatus;
  UINT32                  MailboxData;
  UINT32                  CurrentRingDownBin;
  OC_MAILBOX_INTERFACE    MailboxCommand;

  ///
  /// Program Ring Downbin feature if needed
  ///
  MailboxCommand.InterfaceData = 0;
  MailboxCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_GET_MISC_TURBO_CONTROL;
  Status = MailboxRead (MAILBOX_TYPE_OC, MailboxCommand.InterfaceData, &MailboxData, &LibStatus);
  if ((Status == EFI_SUCCESS) && (LibStatus == PCODE_MAILBOX_CC_SUCCESS)) {
    ///
    /// Ring Downbin mailbox command uses reverse encoding. 0 - Enable Ring Downbin, 1 - Disable Ring Downbin
    ///
    CurrentRingDownBin = (~MailboxData & MISC_TURBO_RING_DOWNBIN_MASK) >> MISC_TURBO_RING_DOWNBIN_OFFSET;
    if (OverClockingConfig->RingDownBin != CurrentRingDownBin) {
      DEBUG ((DEBUG_INFO, "(OC) Updating Ring Downbin = %X\n", OverClockingConfig->RingDownBin));
      MailboxData = (~OverClockingConfig->RingDownBin << MISC_TURBO_RING_DOWNBIN_OFFSET) & MISC_TURBO_RING_DOWNBIN_MASK;
      MailboxCommand.InterfaceData = 0;
      MailboxCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_SET_MISC_TURBO_CONTROL;
      MailboxWrite (MAILBOX_TYPE_OC, MailboxCommand.InterfaceData, MailboxData, &LibStatus);
      if (LibStatus != EFI_SUCCESS) {
        DEBUG ((DEBUG_INFO, "(OC) Ring Downbin message failed, mailbox status = %X\n", LibStatus));
      }
    }
  }

  return Status;
}

/**
  Set DisableCoreMask. Disable Core mask is a bitwise indication of which core should be disabled.
  Each bit represents physical core id: This is a common mask for both P-cores and E-cores.
  1 - Disable
  0 - Ignored

  @param[in] DisablePerCoreMask          - Bit 0 - core 0, bit 15 - core 15.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Core Disable update requires a warm reset.
  @retval COLD_RESET        - Core Enable update requires a cold reset.
**/
CPU_RESET_TYPE
EFIAPI
PeiCpuOcSetDisablePerCoreMask (
  IN UINT32            DisablePerCoreMask
  )
{
  CPU_RESET_TYPE                  ResetType;
  OC_MAILBOX_FULL                 DisabledCoreMsg;
  UINT32                          LibStatus;
  EFI_STATUS                      Status;

  ResetType              = NO_RESET;

  //
  // Step 1: BIOS is to query existing disable core mask using MAILBOX_OC_CMD_OC_INTERFACE.MAILBOX_OC_SUBCMD_READ_DISABLED_IA_CORES_MASK.
  //
  ZeroMem (&DisabledCoreMsg, sizeof (DisabledCoreMsg));
  DisabledCoreMsg.Interface.Fields.CommandCompletion = MAILBOX_OC_CMD_OC_INTERFACE;
  DisabledCoreMsg.Interface.Fields.Param1            = MAILBOX_OC_SUBCMD_READ_DISABLED_IA_CORES_MASK;
  Status = MailboxRead (MAILBOX_TYPE_PCODE, DisabledCoreMsg.Interface.InterfaceData, &DisabledCoreMsg.Data, &LibStatus);
  if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
    DEBUG ((DEBUG_ERROR, "BIOS failed to query disabled core mask.\n"));
    return ResetType;
  }
  DEBUG ((DEBUG_INFO, "(OC) Read Milbox data is 0x%x\n", DisabledCoreMsg.Data ));
  //
  // Read the current core disable map and apply new user selection on the existing mask
  //
  DisablePerCoreMask |= DisabledCoreMsg.Data;
  //
  // Check whether the existing core disable mask match the BIOS Setup option or not.
  //
  if (DisablePerCoreMask != DisabledCoreMsg.Data) {
    DEBUG ((
      DEBUG_INFO,
      "(OC) The existing core disable mask value doesn't match the BIOS Setup option = 0x%x / 0x%x\n",
      DisabledCoreMsg.Data,
      DisablePerCoreMask
      ));
    if (DisablePerCoreMask > DisabledCoreMsg.Data) {
      DEBUG ((DEBUG_INFO, "(OC) Disabling Cores, WARM RESET Required.\n"));
      ResetType |= WARM_RESET;
    } else {
      DEBUG ((DEBUG_INFO, "(OC) Enabling Cores, COLD RESET Required.\n"));
      ResetType |= COLD_RESET;
    }
    //
    // Step 2: BIOS is to write disable core mask using MAILBOX_OC_CMD_OC_INTERFACE.MAILBOX_OC_SUBCMD_WRITE_DISABLED_IA_CORES_MASK.
    //
    ZeroMem (&DisabledCoreMsg, sizeof (DisabledCoreMsg));
    DisabledCoreMsg.Interface.Fields.CommandCompletion = MAILBOX_OC_CMD_OC_INTERFACE;
    DisabledCoreMsg.Interface.Fields.Param1            = MAILBOX_OC_SUBCMD_WRITE_DISABLED_IA_CORES_MASK;

    Status = MailboxWrite (MAILBOX_TYPE_PCODE, DisabledCoreMsg.Interface.InterfaceData, DisablePerCoreMask, &LibStatus);
    if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
      DEBUG ((DEBUG_ERROR, "BIOS failed to write disable core mask.\n"));
      return NO_RESET;
    } else {
      DEBUG ((DEBUG_INFO, "BIOS reading core disable mask after write command.\n"));
      ZeroMem (&DisabledCoreMsg, sizeof (DisabledCoreMsg));
      DisabledCoreMsg.Interface.Fields.CommandCompletion = MAILBOX_OC_CMD_OC_INTERFACE;
      DisabledCoreMsg.Interface.Fields.Param1            = MAILBOX_OC_SUBCMD_READ_DISABLED_IA_CORES_MASK;
      Status = MailboxRead (MAILBOX_TYPE_PCODE, DisabledCoreMsg.Interface.InterfaceData, &DisabledCoreMsg.Data, &LibStatus);
      if ((Status == EFI_SUCCESS) || (LibStatus == MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
        DEBUG ((DEBUG_INFO, "(OC) The existing core disable mask value is %x \n", DisabledCoreMsg.Data));
      }
    }
  }
  return ResetType;
}

/**
  Initializes IA/Core related overclocking settings.

  @param[in] OverClockingConfig      Pointer to CPU Overclocking Policy instance
  @param[out] ResetType              Pointer to CPU reset type to indicate which reset should be performed

  @retval EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
CpuOcInitCoreSettings (
  IN OVERCLOCKING_PREMEM_CONFIG *OverClockingConfig,
  OUT CPU_RESET_TYPE *ResetType
  )
{
  EFI_STATUS                          Status;
  AVX_RATIO_ITEM                      CurrentAvxRatioOffset;
  AVX_RATIO_ITEM                      RequestedAvxRatioOffset;
  UINT32                              CurrentPllOffset;
  UINT32                              RequestedPllOffset;
  PCODE_MAILBOX_INTERFACE             MailboxReadCommand;
  PCODE_MAILBOX_INTERFACE             MailboxWriteCommand;
  OC_MAILBOX_INTERFACE                OcMailboxReadCommand;
  OC_MAILBOX_INTERFACE                OcMailboxWriteCommand;
  UINT32                              MailboxData;
  UINT32                              MailboxStatus;
  UINT32                              CurrentTjMaxOffset;
  UINT32                              RequestedTjMaxOffset;
  UINT32                              MiscGlobalConfigData;
  UINT8                               DomainId;
  UINT16                              CurrentPerCoreHtDisMask;
  UINT16                              RequestedPerCoreHtDisMask;
  UINT8                               CoreIndex;
  UINT16                              NumberOfActiveCores;
  MSR_CORE_THREAD_COUNT_REGISTER      MsrCoreThreadCountReg;
  UINT8                               CurrentAvx2VoltageScale;
  UINT8                               RequestedAvx2VoltageScale;
  UINT32                              MiscConfigData;
  UINT32                              TempMiscConfigData;
  BOOLEAN                             SettingsChanged;
  B2PMB_OC_PERSISTENT_OVERRIDES_DATA  OcPersistentData;

  SettingsChanged     = FALSE;
  NumberOfActiveCores = 0;
  MiscConfigData      = 0;
  TempMiscConfigData  = 0;

  ///
  /// Configure the Pll Voltage offsets for each CPU domain
  ///

  //
  // Initialize
  //
  RequestedPllOffset  = 0x00;

  for (DomainId = MAILBOX_OC_DOMAIN_ID_IA_CORE; DomainId <= MAILBOX_OC_DOMAIN_ID_MEMORY_CONTROLLER; DomainId++ ) {
    if (DomainId!= MAILBOX_OC_DOMAIN_ID_RESERVED) {

      MailboxReadCommand.InterfaceData  = 0x00;
      MailboxReadCommand.Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
      MailboxReadCommand.Fields.Param1 = MAILBOX_OC_SUBCMD_READ_PLL_VCC_TRIM_OFFSET;

      MailboxWriteCommand.InterfaceData = 0x00;
      MailboxWriteCommand.Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
      MailboxWriteCommand.Fields.Param1 = MAILBOX_OC_SUBCMD_WRITE_PLL_VCC_TRIM_OFFSET;

      switch (DomainId) {
        case MAILBOX_OC_DOMAIN_ID_IA_CORE:
          RequestedPllOffset = OverClockingConfig->CorePllVoltageOffset;
          MailboxReadCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_IA_CORE;
          MailboxWriteCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_IA_CORE;
        break;

        case MAILBOX_OC_DOMAIN_ID_GT:
          RequestedPllOffset = OverClockingConfig->GtPllVoltageOffset;
          MailboxReadCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_GT;
          MailboxWriteCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_GT;
        break;

        case MAILBOX_OC_DOMAIN_ID_RING:
          RequestedPllOffset = OverClockingConfig->RingPllVoltageOffset;
          MailboxReadCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_RING;
          MailboxWriteCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_RING;
        break;

        case MAILBOX_OC_DOMAIN_ID_SYSTEM_AGENT:
          RequestedPllOffset = OverClockingConfig->SaPllVoltageOffset;
          MailboxReadCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_SYSTEM_AGENT;
          MailboxWriteCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_SYSTEM_AGENT;
        break;

        case MAILBOX_OC_DOMAIN_ID_L2_ATOM:
          RequestedPllOffset = OverClockingConfig->AtomPllVoltageOffset;
          MailboxReadCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_L2_ATOM;
          MailboxWriteCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_L2_ATOM;
        break;

        case MAILBOX_OC_DOMAIN_ID_MEMORY_CONTROLLER:
          RequestedPllOffset = OverClockingConfig->McPllVoltageOffset;
          MailboxReadCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_MEMORY_CONTROLLER;
          MailboxWriteCommand.Fields.Param2 = MAILBOX_OC_DOMAIN_ID_MEMORY_CONTROLLER;
        break;

        default:
        break;
      }

      ///
      /// Read the current PLL voltage offset
      ///
      MailboxRead (MAILBOX_TYPE_PCODE, MailboxReadCommand.InterfaceData, &CurrentPllOffset, &MailboxStatus);
      if (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) {
        ///
        /// If needed, write the new PLL voltage offset
        ///
        if (RequestedPllOffset != CurrentPllOffset) {
          DEBUG ((DEBUG_INFO, "(OC) Current Pll offset for domain %d  = %X\n", DomainId, CurrentPllOffset));
          DEBUG ((DEBUG_INFO, "(OC) Requested Pll offset for domain %d = %X\n",DomainId, RequestedPllOffset));
          MailboxWrite (MAILBOX_TYPE_PCODE, MailboxWriteCommand.InterfaceData, RequestedPllOffset, &MailboxStatus);
          if (MailboxStatus != EFI_SUCCESS) {
            DEBUG ((DEBUG_INFO, "(OC) Pll message failed for Domain %d, mailbox status = %X\n", DomainId, MailboxStatus));
          } else {
            *ResetType |= WARM_RESET;
          }
        }
      }
    }
  }

  ///
  /// Get the current AVX ratio from OC mailbox
  ///
  Status = GetAvxRatioOffset (&CurrentAvxRatioOffset, &MailboxStatus);
  if (MailboxStatus == MAILBOX_OC_COMPLETION_CODE_SUCCESS) {
    DEBUG ((DEBUG_INFO, "(OC) Current AVX2 Ratio Offset   = %X\n", CurrentAvxRatioOffset.Avx2Ratio));
    DEBUG ((DEBUG_INFO, "(OC) Current AVX3 Ratio Offset   = %X\n", CurrentAvxRatioOffset.Avx3Ratio));
    DEBUG ((DEBUG_INFO, "(OC) Requested AVX2 Ratio Offset = %X\n", OverClockingConfig->Avx2RatioOffset));
    RequestedAvxRatioOffset.Avx2Ratio = (UINT8) OverClockingConfig->Avx2RatioOffset;

    if (CompareMem ((VOID *) &RequestedAvxRatioOffset,(VOID *) &CurrentAvxRatioOffset, sizeof (AVX_RATIO_ITEM))) {
      ///
      /// Set the requested AVX ratio to OC mailbox
      ///
      Status = SetAvxRatioOffset (RequestedAvxRatioOffset, &MailboxStatus);
      if ((Status != EFI_SUCCESS) || (MailboxStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
        DEBUG ((DEBUG_ERROR, "(OC) Set AVX ratio offset failed. EFI Status = %X, Library Status = %X\n", Status, MailboxStatus));
      }
    }
  } else {
    DEBUG ((DEBUG_INFO, "(OC) Get AVX Ratio offset failed. EFI Status = %X, Library Status = %X\n", Status, MailboxStatus));
  }

  ///
  ///  Avx Voltage Guardband Scaling
  ///
  if (IsAvxVoltageGuardBandSupport ()) {

    OcMailboxReadCommand.InterfaceData = 0;
    OcMailboxReadCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_GET_AVX_VOLTAGE_GUARDBAND;

    OcMailboxWriteCommand.InterfaceData = 0;
    OcMailboxWriteCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_SET_AVX_VOLTAGE_GUARDBAND;

    Status = MailboxRead (MAILBOX_TYPE_OC, OcMailboxReadCommand.InterfaceData, &MailboxData, &MailboxStatus);
    if ((Status == EFI_SUCCESS) && (MailboxStatus == MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {

      ///
      ///  Mailbox Format for AVX Voltage Scaling Command:
      ///  [15:8] Avx512 Voltage Guardband Scale Factor in U1.7 format
      ///  [7:0]  Avx2 Voltage Guardband Scale Factor in U1.7 format
      ///  AVX guard band scale factor in U1.7 format is "fixed-point representation" (U : unsigned, 1: integer, 7: fraction)
      ///  Scale factor(X) = U1.7 units (N, PCODE) = BIOS setup value (input BIOS inputs= 100 * X (scale factor))
      ///  U1.7 units N = X * 128 (128 is 2^7 = 80h) (which is for PCODE BIOS MB)
      ///  So BIOS will calculate N = ((BIOS inputs * 128) / 100)
      ///  Actually in BIOS is N = ((BIOS inputs * 128 + 50) / 100), where +50 is for rounding errors
      ///
      CurrentAvx2VoltageScale   = MailboxData & 0xFF;
      DEBUG ((DEBUG_INFO, "(OC) Current values of Avx Voltage Guardband Scaling in U1.7 format is CurrentAvx2VoltageScale = %d\n", CurrentAvx2VoltageScale));

      RequestedAvx2VoltageScale   = (UINT32) (((OverClockingConfig->Avx2VoltageScaleFactor   * 128) + 50 ) / 100);

      if (CurrentAvx2VoltageScale != RequestedAvx2VoltageScale) {
        DEBUG ((DEBUG_INFO, "(OC) New requested values of Avx Voltage Guardband Scaling in U1.7 format is RequestedAvx2VoltageScale = %d\n", RequestedAvx2VoltageScale));
        MailboxData = RequestedAvx2VoltageScale;
        Status = MailboxWrite (MAILBOX_TYPE_OC, OcMailboxWriteCommand.InterfaceData, MailboxData, &MailboxStatus);
        if ((Status != EFI_SUCCESS) || (MailboxStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
          DEBUG ((DEBUG_ERROR, "OC: Error Writing Avx Voltage Guardband Scaling. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        } else {
          Status = MailboxRead (MAILBOX_TYPE_OC, MAILBOX_OC_CMD_GET_AVX_VOLTAGE_GUARDBAND, &MailboxData, &MailboxStatus);
          if ((Status == EFI_SUCCESS) && (MailboxStatus == MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
            CurrentAvx2VoltageScale   = MailboxData & 0xFF;
            DEBUG ((DEBUG_INFO, "(OC) Read after write values of Avx Voltage Guardband Scaling is Avx2VoltageScale = %d\n", CurrentAvx2VoltageScale));
          }
        }
      }
    }
  }

  ///
  /// Read the current TjMax Offset
  ///
  CurrentTjMaxOffset = 0;
  MailboxReadCommand.InterfaceData = 0;
  MailboxWriteCommand.InterfaceData = 0;
  ///
  /// Get Tjmax Offset mailbox command
  ///
  OcGetTjMaxOffsetCmd (&MailboxReadCommand, &MailboxWriteCommand);
  MailboxRead (MAILBOX_TYPE_PCODE, MailboxReadCommand.InterfaceData, &CurrentTjMaxOffset, &MailboxStatus);
  if (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) {
    RequestedTjMaxOffset = (UINT8) OverClockingConfig->TjMaxOffset;
    DEBUG ((DEBUG_INFO, "(OC) Current TjMax Offset  = %X\n", CurrentTjMaxOffset));
    //
    // If needed, write the new TjMax Offset. 0 indicates no offset / keep hardware defaults.
    //
    if ((RequestedTjMaxOffset) && (RequestedTjMaxOffset != CurrentTjMaxOffset)) {
      DEBUG ((DEBUG_INFO, "(OC) Current TjMax Offset  = %X\n", CurrentTjMaxOffset));
      DEBUG ((DEBUG_INFO, "(OC) Requested TjMax Offset  = %X\n", RequestedTjMaxOffset));

      //
      // Check if TjMax Offset is within acceptable range of 10 to 63.
      //
      if ((RequestedTjMaxOffset >= 10) && (RequestedTjMaxOffset <= 63)) {
        MailboxWrite (MAILBOX_TYPE_PCODE, MailboxWriteCommand.InterfaceData, RequestedTjMaxOffset, &MailboxStatus);
        if (MailboxStatus != EFI_SUCCESS) {
          DEBUG((DEBUG_ERROR, "(OC) Failed to change TjMax Offset to %X, mailbox status = %X\n", RequestedTjMaxOffset, MailboxStatus));
        } else {
          *ResetType |= WARM_RESET;
          DEBUG ((DEBUG_INFO, "(OC) Reading the TjMax Value set by write command\n"));
          MailboxRead (MAILBOX_TYPE_PCODE, MailboxReadCommand.InterfaceData, &CurrentTjMaxOffset, &MailboxStatus);
          if (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) {
            DEBUG ((DEBUG_INFO, "(OC) Current TjMax Offset  = %X\n", CurrentTjMaxOffset));
          }
        }
      } else {
        DEBUG ((DEBUG_INFO, "(OC) Requested TjMax Offset is out of range and will not be applied. Valid Range 10 to 63.\n"));
      }
    }
  }

  ///
  /// Fivr options are reverse encoded in the config block defintion
  /// We need to convert to the mailbox format before writing
  ///
  MiscGlobalConfigData = (UINT32) (((OverClockingConfig->BclkAdaptiveVoltage << MISC_GLOBAL_BCLK_ADAPTIVE_OFFSET) |
                                    (OverClockingConfig->CoreVfConfigScope << MISC_GLOBAL_PER_CORE_VOLTAGE_OFFSET)
                                    ) & MISC_GLOBAL_CFG_COMMAND_MASK);

  DEBUG ((DEBUG_INFO, "(OC) Set Misc Global Config = %X\n", MiscGlobalConfigData));
  OcMailboxWriteCommand.InterfaceData = 0;
  OcMailboxWriteCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_SET_MISC_GLOBAL_CONFIG;
  Status = MailboxWrite (MAILBOX_TYPE_OC, OcMailboxWriteCommand.InterfaceData, MiscGlobalConfigData, &MailboxStatus);
  if ((Status != EFI_SUCCESS) || (MailboxStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
    DEBUG ((DEBUG_ERROR, "(OC) Set Misc Global Config Failed. EFI Status = %X, Library Status = %X\n", Status, MailboxStatus));
  }

  ///
  /// Overclocking miscellaneous configuration using BIOS command MAILBOX_OC_SUBCMD_WRITE_OC_MISC_CONFIG.
  /// The following CEP/SIRP/ITD Throttle instances will be checked:
  /// IA CEP  -- DATA[2:2] IA_CEP_DISABLE When set - Disables IA CEP
  /// GT CEP  -- DATA[3:3] GT_CEP_DISABLE When set - Disables GT CEP
  /// SA CEP  -- DATA[4:4] SA_CEP_DISABLE When set - Disables SA CEP
  /// IA SIRP -- DATA[5:5] IA_SIRP_DISABLE When set - Disables IA SIRP
  /// ITD Throttle -- DATA[6:6] ITD_THROTTLE_DISABLE When set - Disables Throttling for Inverse Temperature Dependency Management
  /// Note: The data from the config block means to enbale the CEP & SIRP & ITD Throttle, which is opposite to the mailbox write value.
  ///
  if (IsOcCepConfigSupport ()) {
    OcGetCepConfigData (
      !OverClockingConfig->IaCepEnable,
      !OverClockingConfig->GtCepEnable,
      !OverClockingConfig->SaCepEnable,
      &TempMiscConfigData
    );

    DEBUG ((DEBUG_INFO, "(OC) Set OC CEP Config = %X\n", TempMiscConfigData));
    MiscConfigData += TempMiscConfigData;
    TempMiscConfigData = 0;
  }

  if (IsOcSirpConfigSupport ()) {
    OcGetSirpConfigData (
      !OverClockingConfig->IaSirpEnable,
      &TempMiscConfigData
    );

    DEBUG ((DEBUG_INFO, "(OC) Set OC Sirp Config = %X\n", TempMiscConfigData));
    MiscConfigData += TempMiscConfigData;
    TempMiscConfigData = 0;
  }

  if (IsOcItdThrottleConfigSupport ()) {
    OcGetItdThrottleConfigData (
      !OverClockingConfig->ItdThrottleEnable,
      &TempMiscConfigData
    );

    DEBUG ((DEBUG_INFO, "(OC) Set OC Itd Throttle Config = %X\n", TempMiscConfigData));
    MiscConfigData += TempMiscConfigData;
    TempMiscConfigData = 0;
  }

  DEBUG ((DEBUG_INFO, "(OC) Set OC Misc Config = %X\n", MiscConfigData));

  MailboxWriteCommand.InterfaceData = 0;
  MailboxWriteCommand.Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  MailboxWriteCommand.Fields.Param1 = MAILBOX_OC_SUBCMD_WRITE_OC_MISC_CONFIG;
  Status = MailboxWrite (MAILBOX_TYPE_PCODE, MailboxWriteCommand.InterfaceData, MiscConfigData, &MailboxStatus);
  if ((Status != EFI_SUCCESS) || (MailboxStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
    DEBUG ((DEBUG_ERROR, "(OC) Misc Config Failed. EFI Status = %X, Library Status = %X\n", Status, MailboxStatus));
  }

  ///
  /// Program Thermal Velocity Boost (TVB) and Enhanced Thermal Turbo disable (eTVB) features
  ///
  OcMailboxReadCommand.InterfaceData = 0;
  OcMailboxReadCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_GET_MISC_TURBO_CONTROL;
  Status = MailboxRead (MAILBOX_TYPE_OC, OcMailboxReadCommand.InterfaceData, &MailboxData, &MailboxStatus);
  if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
    DEBUG ((DEBUG_INFO, "(OC) MailboxRead data is %X\n", MailboxData));
    ///
    /// TVB Ratio Clipping, TVB Voltage Optimization and eTVB programming.
    /// Mailbox encoding is reversed,0 = enable, 1 = disable for these controls.
    ///
    MailboxData = (UINT32) ((MailboxData & MISC_TURBO_RING_DOWNBIN_MASK) |
                        (((~OverClockingConfig->TvbRatioClipping << MISC_TURBO_TVB_RATIO_OFFSET) & MISC_TURBO_TVB_RATIO_MASK) |
                        ((~OverClockingConfig->TvbVoltageOptimization << MISC_TURBO_TVB_VOLTAGE_OFFSET) & MISC_TURBO_TVB_VOLTAGE_MASK) |
                        ((~OverClockingConfig->Etvb << MISC_TURBO_ETVB_OFFSET) & MISC_TURBO_ETVB_MASK)));
    DEBUG ((DEBUG_INFO, "(OC) Updating Thermal Velocity Boost settings, RatioClipping = %X, VoltageOptimization = %X, eTVB = %X\n", OverClockingConfig->TvbRatioClipping, OverClockingConfig->TvbVoltageOptimization, OverClockingConfig->Etvb));
    OcMailboxWriteCommand.InterfaceData = 0;
    OcMailboxWriteCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_SET_MISC_TURBO_CONTROL;
    MailboxWrite (MAILBOX_TYPE_OC, OcMailboxWriteCommand.InterfaceData, MailboxData, &MailboxStatus);
    if (MailboxStatus != EFI_SUCCESS) {
      DEBUG ((DEBUG_INFO, "(OC) Thermal Velocity Boost message failed, mailbox status = %X\n", MailboxStatus));
    }
  }

  ///
  /// Program per-core disable feature
  ///
  *ResetType |= PeiCpuOcSetDisablePerCoreMask (OverClockingConfig->DisablePerCoreMask);

  ///
  /// Check Program Per-Core HT Disable mask supported or not
  ///
  if (IsPerCoreHtDisableSupport ()) {
    CurrentPerCoreHtDisMask = 0;
    MailboxReadCommand.InterfaceData = 0;
    MailboxWriteCommand.InterfaceData = 0;
    ///
    /// Get PerCore HT Disable mailbox command
    ///
    OcGetPerCoreHtDisableCmd (&MailboxReadCommand, &MailboxWriteCommand);
    MailboxRead (MAILBOX_TYPE_PCODE, MailboxReadCommand.InterfaceData, (UINT32*) &CurrentPerCoreHtDisMask, &MailboxStatus);
    if (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) {
      RequestedPerCoreHtDisMask = OverClockingConfig->PerCoreHtDisable;
      ///
      /// If needed, write the new per core HT disable mask
      ///
      if (RequestedPerCoreHtDisMask != CurrentPerCoreHtDisMask) {
        DEBUG ((DEBUG_INFO, "(OC) Current per-core HT Disable mask = %X\n", CurrentPerCoreHtDisMask));
        DEBUG ((DEBUG_INFO, "(OC) Requested per-core HT Disable mask = %X\n", RequestedPerCoreHtDisMask));
        MailboxWrite (MAILBOX_TYPE_PCODE, MailboxWriteCommand.InterfaceData, (UINT32) RequestedPerCoreHtDisMask, &MailboxStatus);
        if (MailboxStatus != EFI_SUCCESS) {
          DEBUG ((DEBUG_ERROR, "(OC) Per-core HT Disable command failed, mailbox status = %X\n", MailboxStatus));
        } else {
          ///
          /// We need to determine the reset type.
          /// If Disabling a core, warm reset is required
          /// If Enabling a core, a cold reset is required
          /// We will default to warm reset and check for the cold reset condition
          ///
          *ResetType |= WARM_RESET;

          //
          // Read MSR for Active Core and Thread Count.
          //
          MsrCoreThreadCountReg.Uint64 = AsmReadMsr64 (MSR_CORE_THREAD_COUNT);
          NumberOfActiveCores  = (UINT16) MsrCoreThreadCountReg.Bits.Corecount;

          ///
          /// Check if a core HT is being enabled
          ///
          for (CoreIndex = 0 ; CoreIndex < NumberOfActiveCores; CoreIndex++) {
            if ( ((CurrentPerCoreHtDisMask >> CoreIndex) & BIT0) == 1 &&
                 ((RequestedPerCoreHtDisMask >> CoreIndex) & BIT0) == 0) {
              DEBUG ((DEBUG_INFO, "(OC) Cold Reset required for Per-core HT enable flow\n"));
              *ResetType |= COLD_RESET;
            }
          }
        }
      }
    }
  }
  //
  // OC Persistent overrides(B2PMB Command:0x37 and Sub-Commands: 0x2/0x3)
  // BIT 0: Core Ratio Extension Mode
  // BIT 1: Config SA PLL Frequency to be 1600MHz or 3200MHz
  // Bit[3:2] FLL Overclocking Mode, 0x0 = no overclocking, 0x1 = ratio overclocking with nominal (0.5-1x) reference clock frequency,
  // 0x2 = BCLK overclocking with elevated (1-3x) reference clock frequency, 0x3 = BCLK overclocking with extreme elevated (3-5x) reference clock frequency
  // Bit 4: FLL_OC_MODE_EN: Enable FLL Mode override with the value in FllOcMode.
  // BIT 5: Disable Core HW Fixup during TSC copy from PMA to APIC
  //
  MailboxReadCommand.InterfaceData = 0;
  MailboxReadCommand.Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  MailboxReadCommand.Fields.Param1 = MAILBOX_OC_SUBCMD_READ_OC_PERSISTENT_OVERRIDES;

  MailboxWriteCommand.InterfaceData = 0;
  MailboxWriteCommand.Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  MailboxWriteCommand.Fields.Param1 = MAILBOX_OC_SUBCMD_WRITE_OC_PERSISTENT_OVERRIDES;

  Status = MailboxRead (MAILBOX_TYPE_PCODE, MailboxReadCommand.InterfaceData, &OcPersistentData.Data, &MailboxStatus);
  DEBUG ((DEBUG_INFO, "(OC) Read Milbox data is 0x%x\n", OcPersistentData.Data));
  if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
    if (IsCoreRatioExtModeSupported ()){
      //
      // Check the status of FULL_RANGE_MULTIPLIER_UNLOCK_EN bit
      //
      if (OverClockingConfig->CoreRatioExtensionMode != OcPersistentData.Fields.FullRangeMultiplierUnlockEn) {
        DEBUG ((DEBUG_INFO, "(OC) Modifying Core Ratio Extension Mode bit as per the policy \n"));
        OcPersistentData.Fields.FullRangeMultiplierUnlockEn ^= 0x1;  // toggle this bit
        SettingsChanged = TRUE;
      }
    }
    if (IsSaPllFreqOverrideSupported ()){
      //
      // Check the status of SA_PLL_FREQ_OVERRIDE_BIT bit
      //
      if (OverClockingConfig->SaPllFreqOverride != OcPersistentData.Fields.SaPllFrequencyOverride ) {
        DEBUG ((DEBUG_INFO, "(OC) Modifying Sa Pll Frequency Override bit as per the policy \n"));
        OcPersistentData.Fields.SaPllFrequencyOverride ^= 0x1;  // toggle this bit
        SettingsChanged = TRUE;
      }
    }
    if (IsCoreHwFixupDisableSupported ()) {
      //
      // Check the status of TSC_DISABLE_HW_FIXUP_BIT bit
      //
      if (OverClockingConfig->TscDisableHwFixup != OcPersistentData.Fields.TscHwFixUp) {
        DEBUG ((DEBUG_INFO, "(OC) Modifying TSC HW Fixup disable bit as per the policy \n"));
        OcPersistentData.Fields.TscHwFixUp ^= 0x1;  // toggle this bit
        SettingsChanged = TRUE;
      }
    }
    if (IsFllOverclockSupported ()) {
      // FLL Mode settings
      if (OverClockingConfig->FllOcModeEn != OcPersistentData.Fields.FllOcModeEn) {
        DEBUG ((DEBUG_INFO, "(OC) Modifying FLL OC Mode Enable bit as per the policy \n"));
        OcPersistentData.Fields.FllOcModeEn ^= 0x1;
        SettingsChanged = TRUE;
      }
      if (OverClockingConfig->FllOcModeEn) {
        if (OcPersistentData.Fields.FllOverclockingMode != OverClockingConfig->FllOverclockMode) {
          OcPersistentData.Fields.FllOverclockingMode = OverClockingConfig->FllOverclockMode;
          SettingsChanged = TRUE;
          DEBUG ((DEBUG_INFO, "(OC) FLL overcloking Mode is %d \n", OcPersistentData.Fields.FllOverclockingMode));
        }
      }
    }

    if (SettingsChanged == TRUE) {
      DEBUG ((DEBUG_INFO, "(OC) Settings changed, writing data, OcPersistentData is %x \n", OcPersistentData.Data));
      Status = MailboxWrite (MAILBOX_TYPE_PCODE, MailboxWriteCommand.InterfaceData, OcPersistentData.Data, &MailboxStatus);
      if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
        Status = MailboxRead (MAILBOX_TYPE_PCODE, MailboxReadCommand.InterfaceData, &OcPersistentData.Data, &MailboxStatus);
        if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
          DEBUG ((DEBUG_INFO, "(OC) Read after write the Milbox data is 0x%x\n", OcPersistentData.Data));
        }
        // Request will take effect after warm reset.
        DEBUG ((DEBUG_INFO, "(OC) Issuing Warm Reset for the Mode change\n"));
        *ResetType |= WARM_RESET;
      } else {
        DEBUG ((DEBUG_ERROR, "(OC) Mailbox write command failed. MailboxStatus = %x, Mailbox command return status %x \n", MailboxStatus, Status));
      }
    }
  } else {
    DEBUG ((DEBUG_ERROR, "(OC) Mailbox Read command failed. MailboxStatus = %x, Mailbox command return status %x \n", MailboxStatus, Status));
  }
  return Status;
}

/**
  Initializes CPU domain voltage settings for overclocking.

  @param[in] OverClockingConfig      Pointer to CPU Overclocking Policy instance

  @retval EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
CpuOcInitVoltageSettings (
  IN OVERCLOCKING_PREMEM_CONFIG *OverClockingConfig
  )
{
  EFI_STATUS              Status;
  OC_CAPABILITIES_ITEM    OcCaps;
  VOLTAGE_FREQUENCY_ITEM  CurrentVfItem;
  VOLTAGE_FREQUENCY_ITEM  RequestedVfItem;
  UINT32                  LibStatus;
  UINT8                   DomainId;
  UINT8                   MinDomainId;
  UINT8                   MaxDomainId;
  UINT8                   VfPointIndex;
  UINT8                   CoreVfPointCount;
  UINT8                   RingVfPointCount;
  WDT_PPI                 *WdtPei;

  MinDomainId = MAILBOX_OC_DOMAIN_ID_DDR;
  MaxDomainId = MAILBOX_OC_DOMAIN_ID_L2_ATOM;
  Status = EFI_SUCCESS;
  VfPointIndex = 0;
  CoreVfPointCount = 0;
  RingVfPointCount = 0;
  WdtPei = NULL;

  DEBUG((DEBUG_INFO, "(OC) CpuOcInitVoltageSettings Start \n"));
  ///
  /// We will loop on the CPU domains to manage the voltage/frequency settings
  ///
  for (DomainId = MinDomainId; DomainId <= MaxDomainId; DomainId++) {
    ///
    /// Only IA_CORE and RING is valid for CPU Core
    ///
    if ((DomainId == MAILBOX_OC_DOMAIN_ID_IA_CORE) || (DomainId == MAILBOX_OC_DOMAIN_ID_RING) || (DomainId == MAILBOX_OC_DOMAIN_ID_L2_ATOM && IsAtomL2OcSupport ())) {
      ///
      /// Get OC Capabilities of the domain
      ///
      ZeroMem (&OcCaps, sizeof (OcCaps));
      OcCaps.DomainId = DomainId;
      Status = GetOcCapabilities (&OcCaps, &LibStatus);
      if (LibStatus == MAILBOX_OC_COMPLETION_CODE_SUCCESS) {
        ///
        /// If any OC is supported on this domain, then proceed
        ///
        if (OcCaps.RatioOcSupported || OcCaps.VoltageOverridesSupported || OcCaps.VoltageOffsetSupported) {
          //
          // Locate WDT_PPI (ICC WDT PPI)
          //
          Status = PeiServicesLocatePpi (
                     &gWdtPpiGuid,
                     0,
                     NULL,
                     (VOID **) &WdtPei
                     );
          if (EFI_ERROR (Status)) {
            return Status;
          }
          ///
          /// Need to populate the user requested settings from the Policy
          /// to determine if OC changes are desired.
          ///
          if ((DomainId == MAILBOX_OC_DOMAIN_ID_IA_CORE || DomainId == MAILBOX_OC_DOMAIN_ID_RING) && OcCaps.RatioOcSupported == TRUE) {
            ZeroMem (&CurrentVfItem, sizeof (CurrentVfItem));
            CurrentVfItem.DomainId = DomainId;
            ///
            /// Initialize the supported VfPointCount & each VF point ratio.
            ///
            for (VfPointIndex = 0; VfPointIndex < CPU_OC_MAX_VF_POINTS; VfPointIndex++) {
              CurrentVfItem.VfSettings.VfPointIndex = VfPointIndex + 1;
              Status = GetVoltageFrequencyItem (&CurrentVfItem, &LibStatus);
              if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
                break;
              }
              if (DomainId == MAILBOX_OC_DOMAIN_ID_IA_CORE) {
                CoreVfPointCount++;
                ///
                /// Initialize each supported VF point ratio.
                ///
                OverClockingConfig->CoreVfPointRatio[VfPointIndex] = CurrentVfItem.VfSettings.MaxOcRatio;
              } else if (DomainId == MAILBOX_OC_DOMAIN_ID_RING) {
                RingVfPointCount++;
                ///
                /// Initialize each supported VF point ratio.
                ///
                OverClockingConfig->RingVfPointRatio[VfPointIndex] = CurrentVfItem.VfSettings.MaxOcRatio;
              }
            }
            DEBUG ((DEBUG_INFO, "(OC) Core VF Point Count = %d\n", CoreVfPointCount));
            OverClockingConfig->CoreVfPointCount = CoreVfPointCount;

            DEBUG ((DEBUG_INFO, "(OC) Ring VF Point Count = %d\n", RingVfPointCount));
            OverClockingConfig->RingVfPointCount = RingVfPointCount;
          }

          ///
          /// Populate the user requested VfSettings struct
          ///
          ZeroMem (&CurrentVfItem, sizeof (CurrentVfItem));
          CurrentVfItem.DomainId = DomainId;
          ZeroMem (&RequestedVfItem, sizeof (RequestedVfItem));
          RequestedVfItem.DomainId = DomainId;

          if (DomainId == MAILBOX_OC_DOMAIN_ID_IA_CORE) {
            RequestedVfItem.VfSettings.VoltageTargetMode = (UINT8) OverClockingConfig->CoreVoltageMode;
            if (RequestedVfItem.VfSettings.VoltageTargetMode == OC_LIB_OFFSET_ADAPTIVE) {
              RequestedVfItem.VfSettings.VoltageTarget = OverClockingConfig->CoreVoltageAdaptive;
            } else {
              RequestedVfItem.VfSettings.VoltageTarget = OverClockingConfig->CoreVoltageOverride;
            }

            if (OverClockingConfig->CoreVfPointOffsetMode == 0 && OverClockingConfig->CoreVfConfigScope == 0) {
              ///
              /// Get a copy of the current domain VfSettings from the Mailbox Library
              ///
              CurrentVfItem.VfSettings.VfPointIndex = 0;
              Status = GetVoltageFrequencyItem (&CurrentVfItem, &LibStatus);
              if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
                continue;
              }
              RequestedVfItem.VfSettings.MaxOcRatio    = (UINT8) OverClockingConfig->CoreMaxOcRatio;
              RequestedVfItem.VfSettings.VfPointIndex  = 0;
              RequestedVfItem.VfSettings.VoltageOffset = OverClockingConfig->CoreVoltageOffset;

              Status = UpdateVoltageFrequencyItem (&CurrentVfItem, &RequestedVfItem, WdtPei);
            } else if (OverClockingConfig->CoreVfPointOffsetMode == 1) {
              ASSERT (OverClockingConfig->CoreVfPointCount <= CPU_OC_MAX_VF_POINTS);
              for (VfPointIndex = 0; VfPointIndex < OverClockingConfig->CoreVfPointCount; VfPointIndex++) {
                ///
                /// Get a copy of the current domain VfSettings for each Vf point from the Mailbox Library
                ///
                CurrentVfItem.VfSettings.VfPointIndex = VfPointIndex + 1;
                Status = GetVoltageFrequencyItem (&CurrentVfItem, &LibStatus);
                if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
                  continue;
                }

                RequestedVfItem.VfSettings.MaxOcRatio    = CurrentVfItem.VfSettings.MaxOcRatio; ///< For info purpose, not change the current ratio.
                RequestedVfItem.VfSettings.VfPointIndex  = VfPointIndex + 1; ///< Update the VF Point Index.
                RequestedVfItem.VfSettings.VoltageOffset = OverClockingConfig->CoreVfPointOffset[VfPointIndex]; ///< Update the VF point offset.

                Status = UpdateVoltageFrequencyItem (&CurrentVfItem, &RequestedVfItem, WdtPei);
              }
            }
          } else if (DomainId == MAILBOX_OC_DOMAIN_ID_RING) {
            RequestedVfItem.VfSettings.VoltageTargetMode = (UINT8) OverClockingConfig->RingVoltageMode;
            if (RequestedVfItem.VfSettings.VoltageTargetMode == OC_LIB_OFFSET_ADAPTIVE) {
              RequestedVfItem.VfSettings.VoltageTarget = OverClockingConfig->RingVoltageAdaptive;
            } else {
              RequestedVfItem.VfSettings.VoltageTarget = OverClockingConfig->RingVoltageOverride;
            }

            if (OverClockingConfig->RingVfPointOffsetMode == 0) {
              ///
              /// Get a copy of the current domain VfSettings from the Mailbox Library
              ///
              CurrentVfItem.VfSettings.VfPointIndex = 0;
              Status = GetVoltageFrequencyItem (&CurrentVfItem, &LibStatus);
              if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
                continue;
              }
              RequestedVfItem.VfSettings.MaxOcRatio = (UINT8) OverClockingConfig->RingMaxOcRatio;
              RequestedVfItem.VfSettings.VoltageOffset = OverClockingConfig->RingVoltageOffset;
              RequestedVfItem.VfSettings.VfPointIndex  = 0;

              Status = UpdateVoltageFrequencyItem (&CurrentVfItem, &RequestedVfItem, WdtPei);
            } else if (OverClockingConfig->RingVfPointOffsetMode == 1) {
              ASSERT (OverClockingConfig->RingVfPointCount <= CPU_OC_MAX_VF_POINTS);
              for (VfPointIndex = 0; VfPointIndex < OverClockingConfig->RingVfPointCount; VfPointIndex++) {
                ///
                /// Get a copy of the current domain VfSettings for each Vf point from the Mailbox Library
                ///
                CurrentVfItem.VfSettings.VfPointIndex = VfPointIndex + 1;
                Status = GetVoltageFrequencyItem (&CurrentVfItem, &LibStatus);
                if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
                  continue;
                }
                RequestedVfItem.VfSettings.MaxOcRatio    = CurrentVfItem.VfSettings.MaxOcRatio; ///< For info purpose, not change the current ratio.
                RequestedVfItem.VfSettings.VfPointIndex  = VfPointIndex + 1; ///< Update the VF Point Index.
                RequestedVfItem.VfSettings.VoltageOffset = OverClockingConfig->RingVfPointOffset[VfPointIndex]; ///< Update the VF point offset.

                Status = UpdateVoltageFrequencyItem (&CurrentVfItem, &RequestedVfItem, WdtPei);
              }
            }
          } else if (IsAtomL2OcSupport () && DomainId == MAILBOX_OC_DOMAIN_ID_L2_ATOM) {
            ///
            /// Get a copy of the current domain VfSettings from the Mailbox Library
            ///
            CurrentVfItem.VfSettings.VfPointIndex = 0;
            Status = GetVoltageFrequencyItem (&CurrentVfItem, &LibStatus);
            if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
              continue;
            }

            RequestedVfItem.VfSettings.VoltageTargetMode = (UINT8) OverClockingConfig->AtomL2VoltageMode;
            if (RequestedVfItem.VfSettings.VoltageTargetMode == OC_LIB_OFFSET_ADAPTIVE) {
              RequestedVfItem.VfSettings.VoltageTarget = OverClockingConfig->AtomL2VoltageAdaptive;
            } else {
              RequestedVfItem.VfSettings.VoltageTarget = OverClockingConfig->AtomL2VoltageOverride;
            }

            RequestedVfItem.VfSettings.MaxOcRatio = 0; // OC ratio setting is not supported for AtomL2 domain
            RequestedVfItem.VfSettings.VoltageOffset = OverClockingConfig->AtomL2VoltageOffset;

            Status = UpdateVoltageFrequencyItem (&CurrentVfItem, &RequestedVfItem, WdtPei);
          }
        }
      } else {
        DEBUG ((DEBUG_ERROR, "(OC) GetOcCapabilities message failed. Library Status = %X, Domain = %X\n", LibStatus, DomainId));
      }
    }
  }
  return Status;
}

/**
  VR ICCMAX override for supported domains.

  @param[in] OverClockingConfig      Pointer to CPU Overclocking Policy instance

  @retval EFI_SUCCESS
  @retval EFI_UNSUPPORTED      If the command is not supported.
**/
EFI_STATUS
STATIC
VrIccMaxOcOverride  (
    IN OVERCLOCKING_PREMEM_CONFIG *OverClockingConfig
    )
{
  EFI_STATUS                     Status;
  OC_MAILBOX_INTERFACE           OcMailboxReadCommand;
  OC_MAILBOX_INTERFACE           OcMailboxWriteCommand;
  OCMB_VR_TOPOLOGY_DATA          VrMailboxData;
  OCMB_ICCMAX_DATA               IccMaxMailboxData;
  UINT32                         MailboxStatus;
  UINT8                          DomainId;
  UINT16                         RequestedIccMaxValue;
  UINT8                          RequestedIccMaxUnlimitedMode;

  if (!IsIccMaxOcOverrideSupported ()) {
    return EFI_UNSUPPORTED;
  }

  DEBUG ((DEBUG_INFO, "(OC) VrIccMaxOcOverride \n"));

  //
  // Read the VR topology to get the VR address and VR type
  // Param1 and Param2 are zero
  //
  OcMailboxReadCommand.InterfaceData = 0;
  OcMailboxReadCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_GET_VR_TOPOLOGY;

  Status = MailboxRead (MAILBOX_TYPE_OC, OcMailboxReadCommand.InterfaceData, &VrMailboxData.Data, &MailboxStatus);
  if ((Status != EFI_SUCCESS) || (MailboxStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
    DEBUG ((DEBUG_ERROR, "(OC) Mailbox read command failed. MailboxStatus = %x, Mailbox command return status %x \n", MailboxStatus, Status));
    return EFI_UNSUPPORTED;
  }
  DEBUG ((DEBUG_INFO, "(OC) IA VR Address is 0x%x and IA VR Type is %d\n", VrMailboxData.Fields.IaVrAddress, VrMailboxData.Fields.IaVrType));
  DEBUG ((DEBUG_INFO, "(OC) GT VR Address is 0x%x and GT VR Type is %d\n", VrMailboxData.Fields.GtVrAddress, VrMailboxData.Fields.GtVrType));

  //
  // Override VR ICC Max for supported domains
  //
  OcMailboxReadCommand.InterfaceData = 0;
  OcMailboxReadCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_GET_ICCMAX;

  OcMailboxWriteCommand.InterfaceData = 0;
  OcMailboxWriteCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_SET_ICCMAX;

  for (DomainId = MAILBOX_OC_DOMAIN_ID_IA_CORE; DomainId <= MAILBOX_OC_DOMAIN_ID_GT; DomainId++ ) {
    if ((DomainId == MAILBOX_OC_DOMAIN_ID_IA_CORE) && (VrMailboxData.Fields.IaVrType != SVID_VR)) { // IA VR override is not possible
      continue;
    }
    if ((DomainId == MAILBOX_OC_DOMAIN_ID_GT) && (VrMailboxData.Fields.GtVrType != SVID_VR)) { // GT VR override is not possible
      continue;
    }
    IccMaxMailboxData.Data = 0; //reset the value
    switch (DomainId) {
      case MAILBOX_OC_DOMAIN_ID_IA_CORE:
        OcMailboxReadCommand.Fields.Param1  = (UINT8) VrMailboxData.Fields.IaVrAddress;
        OcMailboxWriteCommand.Fields.Param1 = (UINT8) VrMailboxData.Fields.IaVrAddress;
        RequestedIccMaxValue                = OverClockingConfig->IaIccMax;
        RequestedIccMaxUnlimitedMode        = (UINT8) OverClockingConfig->IaIccUnlimitedMode;
      break;

      case MAILBOX_OC_DOMAIN_ID_GT:
        OcMailboxReadCommand.Fields.Param1  = (UINT8) VrMailboxData.Fields.GtVrAddress;
        OcMailboxWriteCommand.Fields.Param1 = (UINT8) VrMailboxData.Fields.GtVrAddress;
        RequestedIccMaxValue                = OverClockingConfig->GtIccMax;
        RequestedIccMaxUnlimitedMode        = (UINT8) OverClockingConfig->GtIccUnlimitedMode;
      break;

      default:
      break;
    }
    //
    // Read current value and unlimited mode
    //
    Status = MailboxRead (MAILBOX_TYPE_OC, OcMailboxReadCommand.InterfaceData, &IccMaxMailboxData.Data, &MailboxStatus);
    if ((Status == EFI_SUCCESS) && (MailboxStatus == MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
      DEBUG ((DEBUG_INFO, "(OC) Current value for DomainId %d ICC Max is %d and mode is %d\n", DomainId, IccMaxMailboxData.Fields.IccMaxValue, IccMaxMailboxData.Fields.UnlimitedIccMaxMode));
      if ((IccMaxMailboxData.Fields.IccMaxValue != RequestedIccMaxValue) ||
          (IccMaxMailboxData.Fields.UnlimitedIccMaxMode != RequestedIccMaxUnlimitedMode)) {
        IccMaxMailboxData.Fields.IccMaxValue         = RequestedIccMaxValue;
        IccMaxMailboxData.Fields.UnlimitedIccMaxMode = RequestedIccMaxUnlimitedMode;
        //
        // Write new ICC value and unlimited mode as per the user selection
        //
        Status = MailboxWrite (MAILBOX_TYPE_OC, OcMailboxWriteCommand.InterfaceData, IccMaxMailboxData.Data, &MailboxStatus);
        if ((Status == EFI_SUCCESS) && (MailboxStatus == MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
          //
          // Read the modified value and unlimited mode to confirm the mailbox write success
          //
          IccMaxMailboxData.Data = 0;
          DEBUG ((DEBUG_INFO, "(OC) Read after write for the ICC Max command for DomainId %d\n", DomainId));
          Status = MailboxRead (MAILBOX_TYPE_OC, OcMailboxReadCommand.InterfaceData, &IccMaxMailboxData.Data, &MailboxStatus);
          if ((Status == EFI_SUCCESS) && (MailboxStatus == MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
            DEBUG ((DEBUG_INFO, "(OC) New value for DomainId %d ICC Max is %d and mode is %d\n", DomainId, IccMaxMailboxData.Fields.IccMaxValue, IccMaxMailboxData.Fields.UnlimitedIccMaxMode));
          }
        } else {
          DEBUG ((DEBUG_ERROR, "(OC) Mailbox write command failed. MailboxStatus = %x, Mailbox command return status %x \n", MailboxStatus, Status));
        }
      } else {
        DEBUG ((DEBUG_INFO, "(OC) ICC values are in sync with the user inputs, no modification required \n"));
      }
    } else {
      DEBUG ((DEBUG_ERROR, "(OC) Failed to read the ICC Max Mailbox command. MailboxStatus = %x, Mailbox command return status %x \n", MailboxStatus, Status));
    }
  }
  return EFI_SUCCESS;
}


/**
  Initializes PVD Ratio Threshold for all domains.

  @param[in] OverClockingConfig      Pointer to CPU Overclocking Policy instance
  @param[out] ResetType              Pointer to CPU reset type to indicate which reset should be performed

  @retval EFI_SUCCESS
  @retval EFI_INVALID_PARAMETER      If user input if out of range.
**/
EFI_STATUS
EFIAPI
PvdRatioInit (
  IN OVERCLOCKING_PREMEM_CONFIG *OverClockingConfig,
  OUT CPU_RESET_TYPE *ResetType
  )
{
  EFI_STATUS               Status;
  PCODE_MAILBOX_INTERFACE  MailboxReadCommand;
  PCODE_MAILBOX_INTERFACE  MailboxWriteCommand;
  PVD_FLL_OVERRIDE         MailboxData;
  UINT32                   MailboxStatus;

  DEBUG ((DEBUG_INFO, "(OC) PvdRatioInit \n"));

  MailboxReadCommand.InterfaceData = 0;
  MailboxReadCommand.Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  MailboxReadCommand.Fields.Param1 = MAILBOX_OC_SUBCMD_READ_PVD_RATIO_THRESHOLD_OVERRIDE;
  MailboxReadCommand.Fields.Param2 = PVD_OVERRIDES;

  MailboxWriteCommand.InterfaceData = 0;
  MailboxWriteCommand.Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  MailboxWriteCommand.Fields.Param1 = MAILBOX_OC_SUBCMD_WRITE_PVD_RATIO_THRESHOLD_OVERRIDE;
  MailboxWriteCommand.Fields.Param2 = PVD_OVERRIDES;

  ///
  /// When zero, PLL selects static ratios as defined by PVD Mode, if the PVD Mode is supported, else default PVD ratio threshold is used.
  /// When non-zero, defines the ratio between VCO and output clock.
  ///
  if (OverClockingConfig->PvdRatioThreshold == 0) {
    if (!IsPvdModeSupported ()) {
      DEBUG ((DEBUG_INFO, "(OC) Auto/Default mode is selected for the Pvd Ratio Threshold \n"));
      return EFI_SUCCESS;
    } else {
      DEBUG ((DEBUG_INFO, "(OC) Selecting Static PVD ratio Specified by the PVD Mode\n"));
      Status = MailboxRead (MAILBOX_TYPE_PCODE, MailboxReadCommand.InterfaceData, &MailboxData.Data, &MailboxStatus);
      if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
        DEBUG ((DEBUG_INFO, "(OC) Current PVD Mode is %d \n", MailboxData.Fields.PvdFllOverclockMode));
        if (MailboxData.Fields.PvdFllOverclockMode != OverClockingConfig->PvdMode) {
          DEBUG ((DEBUG_INFO, "(OC) Modifying PVD Mode as per the policy to %d\n",OverClockingConfig->PvdMode));
          MailboxData.Fields.RatioThreshold = 0;
          MailboxData.Fields.PvdFllOverclockMode = OverClockingConfig->PvdMode;
          Status = MailboxWrite (MAILBOX_TYPE_PCODE, MailboxWriteCommand.InterfaceData, MailboxData.Data, &MailboxStatus);
          if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
            // Request will take effect after warm reset.
            DEBUG ((DEBUG_INFO, "(OC) Issuing Warm Reset for the PVD Mode change\n"));
            *ResetType |= WARM_RESET;
          } else {
            DEBUG ((DEBUG_ERROR, "(OC) Mailbox write command failed. MailboxStatus = %x, Mailbox command return status %x \n", MailboxStatus, Status));
          }
        }
      }
    }
  } else { // For the Non-zero Pvd Ratio Threshold
    Status = MailboxRead (MAILBOX_TYPE_PCODE, MailboxReadCommand.InterfaceData, &MailboxData.Data, &MailboxStatus);
      if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
      //
      // Check the current value of threshold
      //
      DEBUG ((DEBUG_INFO, "(OC) Current PVD Ratio Threshold value is %d \n", MailboxData.Fields.RatioThreshold));
      if (OverClockingConfig->PvdRatioThreshold != MailboxData.Fields.RatioThreshold) {
        DEBUG ((DEBUG_INFO, "(OC) Modifying PVD Ratio Threshold value as per the policy to %d\n",OverClockingConfig->PvdRatioThreshold));
        MailboxData.Fields.RatioThreshold = OverClockingConfig->PvdRatioThreshold;
        Status = MailboxWrite (MAILBOX_TYPE_PCODE, MailboxWriteCommand.InterfaceData, MailboxData.Data, &MailboxStatus);
        if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
          // Request will take effect after warm reset.
          DEBUG ((DEBUG_INFO, "(OC) Issuing Warm Reset for the PVD Ratio change\n"));
          *ResetType |= WARM_RESET;
        } else {
          DEBUG ((DEBUG_ERROR, "(OC) Mailbox write command failed. MailboxStatus = %x, Mailbox command return status %x \n", MailboxStatus, Status));
        }
      }
    } else {
      DEBUG ((DEBUG_ERROR, "(OC) Mailbox Read command failed. MailboxStatus = %x, Mailbox command return status %x \n", MailboxStatus, Status));
    }
  }
  return Status;
}

/**
  Find the Number Of Active Big Cores.

  @param[in] Buffer - A pointer to a buffer used to OC premem config data.
**/
VOID
EFIAPI
GetNumberOfActiveBigCores (
  IN OUT VOID  *Buffer
  )
{
  UINT8                        CoreType;

  CoreType = 0;

  if (IsSecondaryThread ()) {
    return;
  }

  DetectCoreType (&CoreType);
  if (CoreType == CPUID_CORE_TYPE_INTEL_CORE) {
    ++mNumActiveBigCores;
  }
  if (IsBsp ()) {
    DEBUG ((DEBUG_INFO, "(OC) Finding Number Of Active Big Cores...\n"));
  }
  return;
}

/**
  Initializes TVB Ratio Threshold for all Big Cores.

  @param[in] OverClockingConfig      Pointer to CPU Overclocking Policy instance

  @retval EFI_SUCCESS
  @retval EFI_INVALID_PARAMETER      If user input if out of range.
**/
EFI_STATUS
STATIC
ProgramTvbThresholds (
  IN OVERCLOCKING_PREMEM_CONFIG *OverClockingPreMemConfig
  )
{
  EFI_STATUS               Status;
  UINT32                   MailboxStatus;
  UINT8                    CoreIndex;
  OCMB_TVB_DATA            TvbCommand;
  OC_MAILBOX_INTERFACE     OcMailboxReadCommand;
  OC_MAILBOX_INTERFACE     OcMailboxWriteCommand;


  DEBUG ((DEBUG_INFO, "(OC) Program TVB Threshold Values if TVB clipping is disabled\n"));

  if (OverClockingPreMemConfig == NULL) {
    return EFI_INVALID_PARAMETER;
  }
  Status = EFI_SUCCESS;

  OcMailboxReadCommand.InterfaceData = 0;
  OcMailboxReadCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_GET_OC_TVB_CONFIG;

  OcMailboxWriteCommand.InterfaceData = 0;
  OcMailboxWriteCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_SET_OC_TVB_CONFIG;
  ///
  /// Config TVB OCMB if TVB clipping is disabled.
  ///
  if (IsTvbDownBinsTempThresholdSupported () && (OverClockingPreMemConfig->TvbRatioClipping)) {
    TvbCommand.Data           = 0;
    DEBUG ((DEBUG_INFO, "(OC) Configuring TVB parameters for %d number of current active big cores\n",mNumActiveBigCores));
    for (CoreIndex = 1; CoreIndex <= mNumActiveBigCores; ++CoreIndex) {
      OcMailboxReadCommand.Fields.Param1 = CoreIndex;
      Status = MailboxRead (MAILBOX_TYPE_OC, OcMailboxReadCommand.InterfaceData, &TvbCommand.Data, &MailboxStatus);
      if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
        DEBUG ((DEBUG_INFO, "(OC) Current TVB Config for Core Index %d is = %X\n",CoreIndex, TvbCommand.Data));
        if ((TvbCommand.Fields.TvbDownBinsTempThreshold0 != OverClockingPreMemConfig->TvbDownBinsTempThreshold0) ||
            (TvbCommand.Fields.TvbTempThreshold0 != OverClockingPreMemConfig->TvbTempThreshold0) ||
            (TvbCommand.Fields.TvbTempThreshold1 != OverClockingPreMemConfig->TvbTempThreshold1) ||
            (TvbCommand.Fields.TvbDownBinsTempThreshold1 != OverClockingPreMemConfig->TvbDownBinsTempThreshold1)) {
          TvbCommand.Fields.TvbDownBinsTempThreshold0 = OverClockingPreMemConfig->TvbDownBinsTempThreshold0;
          TvbCommand.Fields.TvbTempThreshold0         = OverClockingPreMemConfig->TvbTempThreshold0;
          TvbCommand.Fields.TvbTempThreshold1         = OverClockingPreMemConfig->TvbTempThreshold1;
          TvbCommand.Fields.TvbDownBinsTempThreshold1 = OverClockingPreMemConfig->TvbDownBinsTempThreshold1;
          DEBUG ((DEBUG_INFO, "(OC) Updating TVB Config for Core Index %d is = %X\n",CoreIndex, TvbCommand.Data));
          OcMailboxWriteCommand.Fields.Param1 = CoreIndex;
          MailboxWrite (MAILBOX_TYPE_OC, OcMailboxWriteCommand.InterfaceData, TvbCommand.Data, &MailboxStatus);
          if (MailboxStatus != EFI_SUCCESS) {
            DEBUG ((DEBUG_INFO, "(OC) Thermal Velocity Boost downbin message failed, mailbox status = %X\n", MailboxStatus));
          } else {
            Status = MailboxRead (MAILBOX_TYPE_OC, OcMailboxReadCommand.InterfaceData, &TvbCommand.Data, &MailboxStatus);
            if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
              DEBUG ((DEBUG_INFO, "(OC) Reading back the recently written TVB Config for Core Index %d is = %X\n",CoreIndex, TvbCommand.Data));
            }
          }
        }
      }
    }
  } // End of TVB config
  return Status;
}

/**
  Initializes pre-memory Overclocking settings in the processor.

  @param[in] OverclockingtConfig      Pointer to Policy protocol instance

  @retval EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
CpuOcInitPreMem (
  IN SI_PREMEM_POLICY_PPI *SiPreMemPolicyPpi
  )
{
  EFI_STATUS    Status;
  OVERCLOCKING_PREMEM_CONFIG *OverClockingConfig;
  CPU_RESET_TYPE             ResetType;

  DEBUG((DEBUG_INFO, "(OC) CpuOcInitPreMem Start \n"));
  PostCode (0xC26);

  ResetType = NO_RESET;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gOverclockingPreMemConfigGuid, (VOID *) &OverClockingConfig);
  ASSERT_EFI_ERROR (Status);

  ///
  /// Check for Overclocking support
  ///
  if (OverClockingConfig->OcSupport == 0) {
    DEBUG ((DEBUG_INFO, "(OC) Overclocking is disabled. Bypassing CPU core overclocking flow.\n"));
    return EFI_SUCCESS;
  }

  ///
  /// Configure Ring related settings
  ///
  Status = CpuOcInitRingSettings (OverClockingConfig);

  ///
  /// Configure Core related settings
  ///
  Status = CpuOcInitCoreSettings (OverClockingConfig, &ResetType);

  ///
  /// Configure OC voltage settings
  ///
  Status = CpuOcInitVoltageSettings (OverClockingConfig);

  ///
  /// Configure VR ICCMAX override
  ///
  Status = VrIccMaxOcOverride (OverClockingConfig);

  ///
  /// Configure PVD Ratio Threshold for all domains
  ///
  if (IsPvdRatioThresholdSupported ()) {
    Status = PvdRatioInit (OverClockingConfig, &ResetType);
  }

  ///
  /// Perform reset if required by oc programming flows
  ///
  if (ResetType != NO_RESET) {
    DEBUG ((DEBUG_INFO, "(OC) Perform Reset, type = %X\n", ResetType));
    DEBUG ((DEBUG_INFO, "CpuOcInit Reset \n"));
    PostCode (0xC27);
    PerformWarmOrColdReset (ResetType);
  }

  DEBUG((DEBUG_INFO, "CpuOcInitPreMem End\n"));
  return Status;
}

/**
  Per-Core Voltage & Frequency Settings.

  @param[in] Buffer - A pointer to a buffer used to OC premem config data.
**/
STATIC
VOID
PerCoreVoltageFrequencySetting (
  IN VOID  *Buffer
  )
{
  OVERCLOCKING_PREMEM_CONFIG   *OverClockingConfig;
  VOLTAGE_FREQUENCY_ITEM       CurrentVfItem;
  VOLTAGE_FREQUENCY_ITEM       RequestedVfItem;
  EFI_STATUS                   Status;
  UINT32                       LibStatus;
  STATIC UINT8                 BigCoreIndex = 0;
  STATIC UINT8                 AtomClusterIndex = 0;
  STATIC UINT8                 PreviousApicId = 0xFF;
  UINT8                        ApicId;
  INT16                        RequestedVoltage;;
  UINT8                        CoreType;

  OverClockingConfig = (OVERCLOCKING_PREMEM_CONFIG *) Buffer;
  CoreType = 0;

  ApicId = (UINT8) (GetCpuApicId () >> 3);
  if (PreviousApicId == ApicId) {
    return;
  }
  PreviousApicId = ApicId;

  //
  // Get the current Voltage & Frequency item.
  //
  ZeroMem (&CurrentVfItem, sizeof (CurrentVfItem));
  CurrentVfItem.DomainId = MAILBOX_OC_DOMAIN_ID_IA_CORE;
  CurrentVfItem.VfSettings.VfPointIndex = 0;
  Status = GetVoltageFrequencyItem (&CurrentVfItem, &LibStatus);
  if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
    return;
  }

  DetectCoreType (&CoreType);
  if (CoreType == CPUID_CORE_TYPE_INTEL_CORE) {
    RequestedVoltage = OverClockingConfig->PerCoreVoltageOffset1[BigCoreIndex];
    ++BigCoreIndex;
  } else  {
    RequestedVoltage = OverClockingConfig->PerAtomClusterVoltageOffset[AtomClusterIndex];
    ++AtomClusterIndex;
  }
  //
  // Skip the duplicated setting.
  //
  if (CurrentVfItem.VfSettings.VoltageOffset == RequestedVoltage) {
    return;
  }

  //
  // Populate the user requested Voltage & Frequency item.
  //
  ZeroMem (&RequestedVfItem, sizeof (RequestedVfItem));
  RequestedVfItem.DomainId = MAILBOX_OC_DOMAIN_ID_IA_CORE;
  RequestedVfItem.VfSettings.VoltageTargetMode = (UINT8) OverClockingConfig->CoreVoltageMode;
  if (RequestedVfItem.VfSettings.VoltageTargetMode == OC_LIB_OFFSET_ADAPTIVE) {
    RequestedVfItem.VfSettings.VoltageTarget = OverClockingConfig->CoreVoltageAdaptive;
  } else {
    RequestedVfItem.VfSettings.VoltageTarget = OverClockingConfig->CoreVoltageOverride;
  }
  RequestedVfItem.VfSettings.MaxOcRatio    = (UINT8) OverClockingConfig->CoreMaxOcRatio;
  RequestedVfItem.VfSettings.VfPointIndex  = 0;
  RequestedVfItem.VfSettings.VoltageOffset = RequestedVoltage;

  //
  // Update existing OC Voltage & Frequency item to the requested OC configuration.
  //
  if (IsBsp ()) {
    DEBUG ((DEBUG_INFO, "(OC) Update Per-Core Voltage & Frequency offset...\n"));
  }

  UpdateVoltageFrequencyItem (&CurrentVfItem, &RequestedVfItem, NULL);

  //
  // The setting data will be lost during COLD_RESET.
  // So, here, WARM_RESET for the update of Voltage & Frequency to take effect.
  //
  mResetType |= WARM_RESET;

  return;
}

/**
  Read each processor's ratio value

  @param[out]    Buffer      Pointer to struct PROCESSOR_MAILBOX_DATA
**/
VOID
MailboxReadRatioValue (
  OUT  VOID   *Buffer
  )
{
  PROCESSOR_MAILBOX_DATA       *EachProcessorRatioOverride;
  OC_MAILBOX_INTERFACE         MailboxCommand;

  //
  // Initialize
  //
  EachProcessorRatioOverride = (PROCESSOR_MAILBOX_DATA *) Buffer;

  if (Buffer == NULL) {
    return;
  }

  //
  // Read From CpuMailboxLib
  //
  MailboxCommand.InterfaceData = 0;
  MailboxCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_READ_PER_CORE_RATIO_LIMITS_CMD;
  MailboxRead (
    MAILBOX_TYPE_OC,
    MailboxCommand.InterfaceData,
    (UINT32 *) &EachProcessorRatioOverride->MailboxData,
    (UINT32 *) &EachProcessorRatioOverride->MailboxStatus
    );
}

/**
  Write each processor's ratio value

  @param[out]    Buffer      Pointer to struct PROCESSOR_MAILBOX_DATA
**/
VOID
MailboxWriteRatioValue (
  OUT  VOID   *Buffer
  )
{
  PROCESSOR_MAILBOX_DATA       *EachProcessorRatioOverride;
  MSR_FLEX_RATIO_REGISTER      FlexRatioMsr;
  OC_MAILBOX_INTERFACE         MailboxCommand;

  //
  // Initialize
  //
  EachProcessorRatioOverride = (PROCESSOR_MAILBOX_DATA *) Buffer;

  if (Buffer == NULL) {
    return;
  }

  //
  // Check whether OC lock bit already set
  //
  FlexRatioMsr.Uint64 = AsmReadMsr64 (MSR_FLEX_RATIO);
  if (((UINT8) FlexRatioMsr.Bits.OcLock) != 0) {
    return;
  }

  //
  // Read From CpuMailboxLib
  //
  MailboxCommand.InterfaceData = 0;
  MailboxCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_WRITE_PER_CORE_RATIO_LIMITS_CMD;
  MailboxWrite (
    MAILBOX_TYPE_OC,
    MailboxCommand.InterfaceData,
    EachProcessorRatioOverride->MailboxData,
    (UINT32 *) &EachProcessorRatioOverride->MailboxStatus
    );
}

/**
  Each Core Ratio Value Override.

  @param[in] OverclockingtConfig      Pointer to Policy protocol instance
  @param[in] MpServices2Ppi           Pointer to this instance of the MpServices.

  @retval    EFI_SUCCESS              Big / Small Core Ratio Override successfully
  @retval    Others                   Big / Small Core Ratio Override failed
**/
EFI_STATUS
RatioValueOverride (
  IN  OVERCLOCKING_PREMEM_CONFIG  *OverClockingConfig,
  IN  EDKII_PEI_MP_SERVICES2_PPI  *MpServices2Ppi
  )
{
  CPU_SKU                      CpuSku;
  EFI_STATUS                   Status;
  UINT32                       LogProcNum;
  UINT32                       ProcessorNumber;
  UINT32                       CoreIndex;
  UINT32                       AtomIndex;
  UINT8                        MaxNonTurboLimRatio;
  UINT8                        MaxEfficiencyRatio;
  UINT8                        ApCoreType;
  PROCESSOR_MAILBOX_DATA       EachProcessorRatioOverride;
  EFI_PROCESSOR_INFORMATION    MpContext;


  //
  // Initialize
  //
  Status                = EFI_SUCCESS;
  MaxNonTurboLimRatio   = 0;
  MaxEfficiencyRatio    = 0;
  ZeroMem (&EachProcessorRatioOverride, sizeof (PROCESSOR_MAILBOX_DATA));
  ZeroMem (&MpContext, sizeof (EFI_PROCESSOR_INFORMATION));

  DEBUG ((DEBUG_INFO, "(OC) RatioValueOverride Start \n"));

  CpuSku = GetCpuSku ();
  if ((CpuSku != EnumCpuHalo) && (CpuSku != EnumCpuTrad) && (CpuSku != EnumCpuUlt)) {
    return EFI_SUCCESS;
  }

  if (!OverClockingConfig->PerCoreRatioOverride) {
    return EFI_SUCCESS;
  }

  LogProcNum = GetEnabledThreadCount ();
  CoreIndex = 0;
  AtomIndex = 0;
  GetBusRatio (&MaxNonTurboLimRatio, &MaxEfficiencyRatio);
  for (ProcessorNumber = 0; ProcessorNumber < LogProcNum; ProcessorNumber++) {
    Status = MpServices2Ppi->GetProcessorInfo (
                               MpServices2Ppi,
                               (UINTN) ProcessorNumber,
                               &MpContext
                               );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Processor %d GetProcessorInfo() failed\n", ProcessorNumber));
      return Status;
    }

    if (MpContext.Location.Thread != FIRST_THREAD) {
      // Continue when the processor is not 1st thread of Core, i.e. the 2nd, 3rd or 4th thread of Core.
      continue;
    }

    if ((MpContext.StatusFlag & BIT0) == BIT0) {
      //
      // if MpContext.StatusFlag BIT0 be raised, this processor is BSP
      //
      MailboxReadRatioValue ((VOID *) &EachProcessorRatioOverride);
      if (EachProcessorRatioOverride.MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
        continue;
      }
      ApCoreType = 0;
      DetectCoreType (&ApCoreType);
      if (ApCoreType == CPUID_CORE_TYPE_INTEL_CORE) {
      //
      // BSP is Big Core
      //
        if (OverClockingConfig->PerCoreRatio[CoreIndex] > MaxNonTurboLimRatio) {
          EachProcessorRatioOverride.MailboxData = (UINT32) OverClockingConfig->PerCoreRatio[CoreIndex];
          DEBUG ((DEBUG_INFO, "Setup PerCore Max Ratio[%d] = 0x%02X\n", CoreIndex, OverClockingConfig->PerCoreRatio[CoreIndex]));
          MailboxWriteRatioValue ((VOID *) &EachProcessorRatioOverride);
          DEBUG ((DEBUG_INFO, "MailboxWriteRatioValue = 0x%08X, Status = 0x%08X\n", EachProcessorRatioOverride.MailboxData, EachProcessorRatioOverride.MailboxStatus));
        }
        CoreIndex++;
      } else if (ApCoreType == CPUID_CORE_TYPE_INTEL_ATOM) {
        //
        // BSP is Small Core
        //
        if ((OverClockingConfig->AtomClusterRatio[AtomIndex / ATOM_QUANTITY_IN_CLUSTER] > MaxNonTurboLimRatio) && (AtomIndex % ATOM_QUANTITY_IN_CLUSTER == 0)) {
          EachProcessorRatioOverride.MailboxData = (UINT32) OverClockingConfig->AtomClusterRatio[AtomIndex / ATOM_QUANTITY_IN_CLUSTER];
          DEBUG ((DEBUG_INFO, "Setup AtomClustere Max Ratio[%d] = 0x%02X\n", AtomIndex / ATOM_QUANTITY_IN_CLUSTER, OverClockingConfig->AtomClusterRatio[AtomIndex / ATOM_QUANTITY_IN_CLUSTER]));
          MailboxWriteRatioValue ((VOID *) &EachProcessorRatioOverride);
          DEBUG ((DEBUG_INFO, "MailboxWriteRatioValue = 0x%08X, Status = 0x%08X\n", EachProcessorRatioOverride.MailboxData, EachProcessorRatioOverride.MailboxStatus));
        }
        AtomIndex++;
      }
    } else {
      Status = MpServices2Ppi->StartupThisAP (
                                 MpServices2Ppi,
                                 (EFI_AP_PROCEDURE) MailboxReadRatioValue,
                                 (UINTN) ProcessorNumber,
                                 0,
                                 (VOID *) &EachProcessorRatioOverride
                                 );
      if (EachProcessorRatioOverride.MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
        continue;
      }
      ApCoreType = 0;
      Status = MpServices2Ppi->StartupThisAP (
                                 MpServices2Ppi,
                                 (EFI_AP_PROCEDURE) DetectCoreType,
                                 (UINTN) ProcessorNumber,
                                 0,
                                 (UINT8 *) &ApCoreType
                                 );
      if (ApCoreType == CPUID_CORE_TYPE_INTEL_CORE) {
        //
        // AP is Big Core
        //
        if (OverClockingConfig->PerCoreRatio[CoreIndex] > MaxNonTurboLimRatio) {
          EachProcessorRatioOverride.MailboxData = (UINT32) OverClockingConfig->PerCoreRatio[CoreIndex];
          DEBUG ((DEBUG_INFO, "Setup PerCore Max Ratio[%d] = 0x%02X\n", CoreIndex, OverClockingConfig->PerCoreRatio[CoreIndex]));
          Status = MpServices2Ppi->StartupThisAP (
                                     MpServices2Ppi,
                                     (EFI_AP_PROCEDURE) MailboxWriteRatioValue,
                                     (UINTN) ProcessorNumber,
                                     0,
                                     (VOID *) &EachProcessorRatioOverride
                                     );
          DEBUG ((DEBUG_INFO, "MailboxWriteRatioValue = 0x%08X, Status = 0x%08X\n", EachProcessorRatioOverride.MailboxData, EachProcessorRatioOverride.MailboxStatus));
          if (EFI_ERROR (Status)) {
            return Status;
          }
        }
        CoreIndex++;
      } else if (ApCoreType == CPUID_CORE_TYPE_INTEL_ATOM) {
        //
        // AP is Small Core
        //
        if ((OverClockingConfig->AtomClusterRatio[AtomIndex / ATOM_QUANTITY_IN_CLUSTER] > MaxNonTurboLimRatio) && (AtomIndex % ATOM_QUANTITY_IN_CLUSTER == 0)) {
          EachProcessorRatioOverride.MailboxData = (UINT32) OverClockingConfig->AtomClusterRatio[AtomIndex / ATOM_QUANTITY_IN_CLUSTER];
          DEBUG ((DEBUG_INFO, "Setup AtomCluster Max Ratio[%d] = 0x%02X\n", AtomIndex / ATOM_QUANTITY_IN_CLUSTER, OverClockingConfig->AtomClusterRatio[AtomIndex / ATOM_QUANTITY_IN_CLUSTER]));
          Status = MpServices2Ppi->StartupThisAP (
                                     MpServices2Ppi,
                                     (EFI_AP_PROCEDURE) MailboxWriteRatioValue,
                                     (UINTN) ProcessorNumber,
                                     0,
                                     (VOID *) &EachProcessorRatioOverride
                                     );
          DEBUG ((DEBUG_INFO, "MailboxWriteRatioValue = 0x%08X, Status = 0x%08X\n", EachProcessorRatioOverride.MailboxData, EachProcessorRatioOverride.MailboxStatus));
          if (EFI_ERROR (Status)) {
            return Status;
          }
        }
        AtomIndex++;
      }
    }
  }
  DEBUG ((DEBUG_INFO, "(OC) RatioValueOverride End \n"));

  return EFI_SUCCESS;
}

/**
  Initializes post-memory Overclocking settings in the processor.

  @param[in] OverclockingtConfig      Pointer to Policy protocol instance
  @param[in] MpServices2Ppi            Pointer to this instance of the MpServices.

  @retval EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
CpuOcInitPostMem (
  IN OVERCLOCKING_PREMEM_CONFIG   *OverClockingConfig,
  IN EDKII_PEI_MP_SERVICES2_PPI   *MpServices2Ppi
  )
{
  EFI_STATUS                   Status;
  OC_CAPABILITIES_ITEM         OcCaps;
  UINT32                       LibStatus;
  UINT32                       MiscGlobalConfigData;
  OC_MAILBOX_INTERFACE         MailboxCommand;

  Status = EFI_SUCCESS;
  DEBUG((DEBUG_INFO, "(OC) CpuOcInitPostMem Start \n"));
  if (OverClockingConfig == NULL || MpServices2Ppi == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ///
  /// Check for Overclocking support
  ///
  if (OverClockingConfig->OcSupport == 0) {
    DEBUG ((DEBUG_INFO, "(OC) Overclocking is disabled. Skipping Cpu Oc Init PostMem.\n"));
    return EFI_SUCCESS;
  }

  ///
  /// Find number of active Big cores
  ///
  GetNumberOfActiveBigCores ((VOID *) OverClockingConfig);
  MpServices2Ppi->StartupAllAPs (
                    MpServices2Ppi,
                    (EFI_AP_PROCEDURE) GetNumberOfActiveBigCores,
                    TRUE,
                    0,
                    OverClockingConfig
                    );

  DEBUG ((DEBUG_INFO, "(OC) Total Enabled Big Cores - %d\n", mNumActiveBigCores));

  ///
  /// Program TVB thresholds
  ///
  ProgramTvbThresholds (OverClockingConfig);

  //
  // Per CPU Core OC V/F adjustment if OC is supported and in Legacy Mode.
  //
  if (OverClockingConfig->CoreVfPointOffsetMode == 0 && OverClockingConfig->CoreVfConfigScope == 1) {
    ZeroMem (&OcCaps, sizeof (OcCaps));
    OcCaps.DomainId = MAILBOX_OC_DOMAIN_ID_IA_CORE;
    GetOcCapabilities (&OcCaps, &LibStatus);
    if (LibStatus == MAILBOX_OC_COMPLETION_CODE_SUCCESS && (OcCaps.RatioOcSupported || OcCaps.VoltageOverridesSupported || OcCaps.VoltageOffsetSupported)) {
      mResetType = NO_RESET;

      ///
      /// Program MISC CONFIG (commands 0x14 and 0x15) to allow per-core V/F setting.
      ///
      MailboxCommand.InterfaceData = 0;
      MailboxCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_GET_MISC_GLOBAL_CONFIG;
      Status = MailboxRead (MAILBOX_TYPE_OC, MailboxCommand.InterfaceData, &MiscGlobalConfigData, &LibStatus);
      if ((Status == EFI_SUCCESS) && (LibStatus == PCODE_MAILBOX_CC_SUCCESS) && ((MiscGlobalConfigData & MISC_GLOBAL_PER_CORE_VOLTAGE_MASK) >> MISC_GLOBAL_PER_CORE_VOLTAGE_OFFSET) == 0) {
        MiscGlobalConfigData |= (UINT32) (OverClockingConfig->CoreVfConfigScope << MISC_GLOBAL_PER_CORE_VOLTAGE_OFFSET);
        MailboxCommand.InterfaceData = 0;
        MailboxCommand.Fields.CommandCompletion = MAILBOX_OC_CMD_SET_MISC_GLOBAL_CONFIG;
        MailboxWrite (MAILBOX_TYPE_OC, MailboxCommand.InterfaceData, MiscGlobalConfigData, &LibStatus);
        if (LibStatus != PCODE_MAILBOX_CC_SUCCESS) {
          DEBUG ((DEBUG_INFO, "(OC) Update MISC CONFIG setting failed, mailbox status = %X\n", LibStatus));
          return EFI_NOT_READY;
        }
      }
      //
      // Per-core V/F setting via MP service.
      //
      PerCoreVoltageFrequencySetting (OverClockingConfig);
      MpServices2Ppi->StartupAllAPs (
                        MpServices2Ppi,
                        (EFI_AP_PROCEDURE) PerCoreVoltageFrequencySetting,
                        TRUE,
                        0,
                        OverClockingConfig
                        );
      if (mResetType != NO_RESET) {
        DEBUG ((DEBUG_INFO, "(OC) Reset for per-core OC V/F adjustment...\n"));
        PerformWarmOrColdReset (mResetType);
      }
    }
  }

  ///
  /// Per Core / Atom Core Ratio Override
  ///
  RatioValueOverride (OverClockingConfig, MpServices2Ppi);

  return Status;
}
/**
  This function will perform the BCLK warm reset overclocking flow.

  @param[in] UINT8  BclkSource       BCLK Source
  @param[in] UINT32 BclkFrequency    Required BCLK frequency in 10 KHz unit.

  @retval EFI_SUCCESS          Function successfully executed.

  Appropriate failure code on error.
**/
EFI_STATUS
EFIAPI
PerformBclkOcFlow (
  IN UINT8  BclkSource,
  IN OVERCLOCKING_PREMEM_CONFIG   *OcConfig
  )
{
  EFI_STATUS                Status;
  WDT_PPI                   *WdtPei;
  UINT8                     WarmResetRequired;
  UINT8                     WdtTimeout;
  UINT32                    MailboxStatus;
  UINT32                    RequiredBclkFrequency;
  OC_MAILBOX_INTERFACE      MailboxReadCmd;
  OC_MAILBOX_INTERFACE      MailboxWriteCmd;
  UINT32                    CurrentBclkFrequency;

  Status = EFI_SUCCESS;
  WarmResetRequired = FALSE;
  WdtTimeout = FALSE;
  MailboxStatus = 0;
  RequiredBclkFrequency = 0;
  MailboxReadCmd.InterfaceData = 0;
  MailboxWriteCmd.InterfaceData = 0;

  //
  // Locate WDT_PPI (ICC WDT PPI)
  //
  Status = PeiServicesLocatePpi (
             &gWdtPpiGuid,
             0,
             NULL,
             (VOID **) &WdtPei
             );
  ASSERT_EFI_ERROR (Status);

  //
  // Check for WDT timeout on previous boot
  //
  WdtTimeout = WdtPei->CheckStatus();

  //
  // Check BCLK source
  //
  MailboxReadCmd.Fields.CommandCompletion = MAILBOX_OC_CMD_BCLK_FREQUENCY_CMD;
  DEBUG ((DEBUG_INFO, "(BCLK) Read BCLK CMD = %x\n", MailboxReadCmd));

  Status = MailboxRead (MAILBOX_TYPE_OC, MailboxReadCmd.InterfaceData, &CurrentBclkFrequency, &MailboxStatus);
  if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
    DEBUG ((DEBUG_INFO, "(BCLK) Current BCLK (0x5) (KHz) = %d\n", CurrentBclkFrequency));
  } else {
    DEBUG ((DEBUG_ERROR, "(BCLK) BCLK CMD=0x5 Read fail.....\n"));
  }

  //
  // If no WDT timeout, ramp BCLK, otherwise, continue boot to recover.
  //
  if (WdtTimeout == FALSE) {
    DEBUG ((DEBUG_INFO, "(OC) PerformBclkOcFlow start.\n"));
    if (BclkSource == PCH_BCLK) {
      Status = PeiHeciIccBclkMsg (&WarmResetRequired);
      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_WARN, "(ICC) PeiHeciIccBclkMsg failed with Status = %X\n", Status));
        return Status;
      }
    } else if ((BclkSource == CPU_BCLK) && IsCpuBclkOcFreqSupport () && (!IsSocBclkOcFreqSupport ())) {
      if (OcConfig->CpuBclkOcFrequency != 0) {
        RequiredBclkFrequency = OcConfig->CpuBclkOcFrequency * 10;
        OcGetCpuBclkFreqCmd (&MailboxReadCmd, &MailboxWriteCmd);
        DEBUG ((DEBUG_INFO, "(CPU BCLK) Required CPU BCLK (KHz)= %d\n", RequiredBclkFrequency));
        Status = MailboxRead (MAILBOX_TYPE_OC, MailboxReadCmd.InterfaceData, &CurrentBclkFrequency, &MailboxStatus);
        DEBUG ((DEBUG_INFO, "(BCLK) Current BCLK (KHz) = %d\n", CurrentBclkFrequency));
        if ((RequiredBclkFrequency != CurrentBclkFrequency)) {
          Status = MailboxWrite (MAILBOX_TYPE_OC, MailboxWriteCmd.InterfaceData, RequiredBclkFrequency, &MailboxStatus);
          if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
            WarmResetRequired = 1;
            Status = MailboxRead (MAILBOX_TYPE_OC, MailboxReadCmd.InterfaceData, &CurrentBclkFrequency, &MailboxStatus);
            DEBUG ((DEBUG_INFO, "(BCLK)Reading back BCLK value set by BIOS (KHz) = %d\n", CurrentBclkFrequency));
          }
        }
      }
    } else if (IsCpuBclkOcFreqSupport () && IsSocBclkOcFreqSupport ()) {
      if (OcConfig->CpuBclkOcFrequency != 0) {
        RequiredBclkFrequency = OcConfig->CpuBclkOcFrequency * 10;
        OcGetCpuBclkFreqCmd (&MailboxReadCmd, &MailboxWriteCmd);
        MailboxReadCmd.Fields.Param1 = CPU_BCLK_SELECT;
        MailboxWriteCmd.Fields.Param1 = CPU_BCLK_SELECT;
        DEBUG ((DEBUG_INFO, "(BCLK) Required CPU BCLK (KHz)= %d\n", RequiredBclkFrequency));
        Status = MailboxRead (MAILBOX_TYPE_OC, MailboxReadCmd.InterfaceData, &CurrentBclkFrequency, &MailboxStatus);
        DEBUG ((DEBUG_INFO, "(BCLK) Current CPU BCLK (KHz) = %d\n", CurrentBclkFrequency));
        if (RequiredBclkFrequency != CurrentBclkFrequency) {
          Status = MailboxWrite (MAILBOX_TYPE_OC, MailboxWriteCmd.InterfaceData, RequiredBclkFrequency, &MailboxStatus);
          if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
            WarmResetRequired = 1;
            Status = MailboxRead (MAILBOX_TYPE_OC, MailboxReadCmd.InterfaceData, &CurrentBclkFrequency, &MailboxStatus);
            DEBUG ((DEBUG_INFO, "(BCLK)Reading back CPU BCLK value set by BIOS (KHz) = %d\n", CurrentBclkFrequency));
          }
        }
      }
      if (OcConfig->SocBclkOcFrequency != 0) {
        RequiredBclkFrequency = OcConfig->SocBclkOcFrequency * 10;
        OcGetCpuBclkFreqCmd (&MailboxReadCmd, &MailboxWriteCmd);
        MailboxReadCmd.Fields.Param1  = SOC_BCLK_SELECT;
        MailboxWriteCmd.Fields.Param1 = SOC_BCLK_SELECT;
        DEBUG ((DEBUG_INFO, "(BCLK) Required SOC BCLK (KHz)= %d\n", RequiredBclkFrequency));
        Status = MailboxRead (MAILBOX_TYPE_OC, MailboxReadCmd.InterfaceData, &CurrentBclkFrequency, &MailboxStatus);
        DEBUG ((DEBUG_INFO, "(BCLK) Current SOC BCLK (KHz) = %d\n", CurrentBclkFrequency));
        if (RequiredBclkFrequency != CurrentBclkFrequency) {
          Status = MailboxWrite (MAILBOX_TYPE_OC, MailboxWriteCmd.InterfaceData, RequiredBclkFrequency, &MailboxStatus);
          if ((Status == EFI_SUCCESS) && (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS)) {
            WarmResetRequired = 1;
            Status = MailboxRead (MAILBOX_TYPE_OC, MailboxReadCmd.InterfaceData, &CurrentBclkFrequency, &MailboxStatus);
            DEBUG ((DEBUG_INFO, "(BCLK)Reading back SOC BCLK value set by BIOS (KHz) = %d\n", CurrentBclkFrequency));
          }
        }
      }
    } else {
      return EFI_SUCCESS;
    }
    DEBUG ((DEBUG_INFO, "(OC) PerformBclkOcFlow End.\n"));

    if (WarmResetRequired == 1) {
      DEBUG ((DEBUG_INFO, "(OC) Acknowledged BCLK Ramp reset is required.\n"));

      //
      // Start the Watchdog Timer
      //
      Status = WdtPei->ReloadAndStart (60);
      ASSERT_EFI_ERROR (Status);

      //
      // Warm reset is required for BCLK frequency changes. The BCLK frequency
      // is ramped while the CPU is in reset.
      //
      DEBUG ((DEBUG_INFO, "(OC) Performing Warm Reset for BCLK frequency change.\n"));
      WdtPei->AllowKnownReset ();
      (*GetPeiServicesTablePointer ())->ResetSystem2 (EfiResetWarm, EFI_SUCCESS, 0, NULL);
    }
  }

  DEBUG ((DEBUG_INFO, "(OC) No BCLK warm reset required.\n"));
  return Status;
}

/**
  Initializes Overclocking settings in the processor.

  @param[in] *OcConfig                 - OcConfig Instance of OVERCLOCKING_PREMEM_CONFIG
  @param[in] *CpuConfigLibPreMemConfig - CpuConfigLibPreMemConfig Instance of CPU_CONFIG_LIB_PREMEM_CONFIG

  @retval EFI_SUCCESS
**/
EFI_STATUS
SaOcInit (
  IN OVERCLOCKING_PREMEM_CONFIG   *OcConfig,
  IN CPU_CONFIG_LIB_PREMEM_CONFIG *CpuConfigLibPreMemConfig
  )
{
  EFI_STATUS                Status;
  OC_CAPABILITIES_ITEM      OcCaps;
  VOLTAGE_FREQUENCY_ITEM    CurrentVfItem;
  VOLTAGE_FREQUENCY_ITEM    RequestedVfItem;
  UINT32                    LibStatus;
  UINT8                     DomainId;
  BOOLEAN                   VfUpdateNeeded;
  WDT_PPI                   *gWdtPei;
  UINT32                    CurrentRealtimeMemory;
  PCODE_MAILBOX_INTERFACE   MailboxReadCommand;
  PCODE_MAILBOX_INTERFACE   MailboxWriteCommand;
  UINT32                    MailboxData;

  LibStatus = 0;
  VfUpdateNeeded = FALSE;
  MailboxData = 0;

  DEBUG((DEBUG_INFO, "(OC) SaOcInit Start \n"));

  if (OcConfig->OcSupport == FALSE) {
    ///
    /// Overclocking is disabled
    ///
    DEBUG ((DEBUG_INFO, "(OC) Overclocking is disabled. Bypassing SA overclocking flow.\n"));
    return EFI_SUCCESS;
  }

  Status = EFI_SUCCESS;
  ZeroMem (&CurrentVfItem,sizeof (CurrentVfItem));
  ZeroMem (&RequestedVfItem,sizeof (RequestedVfItem));

  //
  // Locate WDT_PPI (ICC WDT PPI)
  //
  Status = PeiServicesLocatePpi (
             &gWdtPpiGuid,
             0,
             NULL,
             (VOID **) &gWdtPei
             );
  ASSERT_EFI_ERROR (Status);

  ///
  /// Update Realtime Memory Timings if needed
  ///
  CurrentRealtimeMemory = 0;
  DEBUG ((DEBUG_INFO, "(OC) Requested Realtime Memory Timing = %X\n", OcConfig->RealtimeMemoryTiming));
  OcGetRealtimeMemoryTimingCmd (&MailboxReadCommand, &MailboxWriteCommand);
  MailboxRead (MAILBOX_TYPE_PCODE, MailboxReadCommand.InterfaceData, &CurrentRealtimeMemory, &LibStatus);

  if (OcConfig->RealtimeMemoryTiming != ((CurrentRealtimeMemory & OC_MISC_CFG_MEM_REALTIME_CHG_MASK) >> 1)) {
    MailboxData = CurrentRealtimeMemory;
    DEBUG ((DEBUG_INFO, "(PCODE OC) Current Realtime Memory Timing = %X\n", CurrentRealtimeMemory));
    MailboxData &= (UINT32)~OC_MISC_CFG_MEM_REALTIME_CHG_MASK;
    MailboxData |= (OcConfig->RealtimeMemoryTiming << 1);
    MailboxWrite (MAILBOX_TYPE_PCODE, MailboxWriteCommand.InterfaceData, MailboxData, &LibStatus);
    if (LibStatus != EFI_SUCCESS) {
      DEBUG ((DEBUG_INFO, "(PCODE OC) Set Realtime Memory Timing Status failed, mailbox status = %X\n", LibStatus));
    }
  }

  ///
  /// We will loop on the CPU domains to manage the voltage/frequency settings
  ///
  for (DomainId = MAILBOX_OC_DOMAIN_ID_GT; DomainId <= MAILBOX_OC_DOMAIN_ID_SYSTEM_AGENT; DomainId++) {
    ///
    /// Only GT, Uncore, IOA, and IOD are valid for System Agent
    ///
    if (DomainId != MAILBOX_OC_DOMAIN_ID_RING) {
      ///
      /// Get OC Capabilities of the domain
      ///
      ZeroMem (&OcCaps,sizeof (OcCaps));
      OcCaps.DomainId = DomainId;
      Status = GetOcCapabilities (&OcCaps,&LibStatus);

      if (LibStatus == MAILBOX_OC_COMPLETION_CODE_SUCCESS) {
        ///
        /// If any OC is supported on this domain, then proceed
        ///
        if (OcCaps.RatioOcSupported || OcCaps.VoltageOverridesSupported || OcCaps.VoltageOffsetSupported) {
          ///
          /// Need to populate the user requested settings from the policy
          /// to determine if OC changes are desired.
          ///
          ZeroMem (&CurrentVfItem,sizeof (CurrentVfItem));
          CurrentVfItem.DomainId = DomainId;

          ///
          /// Get a copy of the current domain VfSettings from the Mailbox Library
          ///
          Status = GetVoltageFrequencyItem (&CurrentVfItem,&LibStatus);
          if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
            continue;
          }

          ///
          /// Populate the user requested VfSettings struct
          ///
          ZeroMem (&RequestedVfItem,sizeof (RequestedVfItem));
          RequestedVfItem.DomainId = DomainId;
          switch (DomainId) {
            case MAILBOX_OC_DOMAIN_ID_GT:
              RequestedVfItem.VfSettings.MaxOcRatio = (UINT8) OcConfig->GtMaxOcRatio;

              ///
              /// VoltageTarget has 2 uses and we need to update the target based
              /// on the voltagemode requested
              ///
              RequestedVfItem.VfSettings.VoltageTargetMode = (UINT8) OcConfig->GtVoltageMode;
              if (RequestedVfItem.VfSettings.VoltageTargetMode == OC_LIB_OFFSET_ADAPTIVE) {
                RequestedVfItem.VfSettings.VoltageTarget = OcConfig->GtExtraTurboVoltage;
              }
              else {
                RequestedVfItem.VfSettings.VoltageTarget = OcConfig->GtVoltageOverride;
              }
              RequestedVfItem.VfSettings.VoltageOffset = OcConfig->GtVoltageOffset;
              break;

            case MAILBOX_OC_DOMAIN_ID_SYSTEM_AGENT:
              ///
              /// VoltageTarget has 2 uses and we need to update the target based
              /// on the voltagemode requested
              ///
              RequestedVfItem.VfSettings.VoltageTargetMode = (UINT8) OcConfig->SaVoltageMode;
              if (RequestedVfItem.VfSettings.VoltageTargetMode == OC_LIB_OFFSET_ADAPTIVE) {
                RequestedVfItem.VfSettings.VoltageTarget = OcConfig->SaExtraTurboVoltage;
              } else {
                RequestedVfItem.VfSettings.VoltageTarget = OcConfig->SaVoltageOverride;
              }
              RequestedVfItem.VfSettings.VoltageOffset = OcConfig->SaVoltageOffset;
              break;
          }
          VfUpdateNeeded = (BOOLEAN) CompareMem ((VOID*)&RequestedVfItem,(VOID*)&CurrentVfItem,sizeof (VOLTAGE_FREQUENCY_ITEM));

          if (VfUpdateNeeded) {
            VfUpdateNeeded = FALSE;

            ///
            /// Arm watchdog timer for OC changes
            ///
            Status = gWdtPei->ReloadAndStart (WDT_TIMEOUT_BETWEEN_PEI_DXE);

            ///
            /// Need to update the requested voltage/frequency values
            ///
            DEBUG ((DEBUG_INFO, "(OC) Set Voltage Frequency for Domain = %X\n", DomainId));
            DEBUG ((DEBUG_INFO, "(OC) RequestedVfItem.VfSettings.MaxOcRatio     = %X\n", RequestedVfItem.VfSettings.MaxOcRatio));
            DEBUG ((DEBUG_INFO, "(OC) RequestedVfItem.VfSettings.TargetMode     = %X\n", RequestedVfItem.VfSettings.VoltageTargetMode));
            DEBUG ((DEBUG_INFO, "(OC) RequestedVfItem.VfSettings.VoltageTarget  = %X\n", RequestedVfItem.VfSettings.VoltageTarget));
            DEBUG ((DEBUG_INFO, "(OC) RequestedVfItem.VfSettings.VoltageOffset  = %X\n", RequestedVfItem.VfSettings.VoltageOffset));
            DEBUG ((DEBUG_INFO, "(OC) CurrentVfItem.VfSettings.MaxOcRatio       = %X\n", CurrentVfItem.VfSettings.MaxOcRatio));
            DEBUG ((DEBUG_INFO, "(OC) CurrentVfItem.VfSettings.TargetMode       = %X\n", CurrentVfItem.VfSettings.VoltageTargetMode));
            DEBUG ((DEBUG_INFO, "(OC) CurrentVfItem.VfSettings.VoltageTarget    = %X\n", CurrentVfItem.VfSettings.VoltageTarget));
            DEBUG ((DEBUG_INFO, "(OC) CurrentVfItem.VfSettings.VoltageOffset    = %X\n", CurrentVfItem.VfSettings.VoltageOffset));

            Status = SetVoltageFrequencyItem (RequestedVfItem,&LibStatus);
            if ((Status != EFI_SUCCESS) || (LibStatus != MAILBOX_OC_COMPLETION_CODE_SUCCESS)) {
              DEBUG ((DEBUG_WARN, "(OC) Set Voltage Frequency failed. EFI Status = %X, Library Status = %X\n", Status, LibStatus));
            }  else {
              //
              // Warm reset is required for Uncore voltage change. New voltage will be applied in reset.
              //
              (*GetPeiServicesTablePointer ())->ResetSystem2 (EfiResetWarm, EFI_SUCCESS, 0, NULL);
            }
          }
        }
        else {
          DEBUG ((DEBUG_INFO, "(OC) No OC support for this Domain = %X\n", DomainId));
        }
      }
      else {
        DEBUG ((DEBUG_WARN, "(OC) GetOcCapabilities message failed. Library Status = %X, Domain = %X\n", LibStatus, DomainId));
      }
    }
  }

  Status = PerformBclkOcFlow ((UINT8) CpuConfigLibPreMemConfig->BclkSource, OcConfig);
  DEBUG ((DEBUG_INFO, "(ICC) PerformBclkOcFlow status = %X.\n", Status));

  return Status;
}
