/** @file
  Header file for overclocking initializations.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_OC_LIB_H_
#define _PEI_OC_LIB_H_

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/OcCommonLib.h>
#include <Ppi/SiPolicy.h>
#include <Ppi/Wdt.h>
#include <Ppi/MpServices2.h>
#include <OcMailbox.h>
#include <Library/CpuMailboxLib.h>

///
///  High Level Voltage/Frequency data required for setting v/f requests on all CPU domains
///
typedef struct {
  UINT8  MaxOcRatio; ///< Max overclocking ratio limit for given CPU domain id
  UINT8  VoltageTargetMode; ///< Voltage Mode selection; <b>0: Adaptive</b>, 1: Override
  UINT16 VoltageTarget; ///< Voltage target specified in millivolts
  INT16  VoltageOffset; ///< Voltage offset specified in millivolts. Can be positive or negative offset.
  UINT8  VfPointIndex;  ///< Selected VF Point Index.
} VOLTAGE_FREQUENCY_SETTINGS;

///
///  Expanded Voltage Frequency mailbox information in the mailbox command format
///
typedef struct {
  UINT32 MaxOcRatio       : 8;  ///< Max overclocking ratio limit for given CPU domain id
  UINT32 VoltageTargetU12 : 12; ///< Voltage target represented in unsigned fixed point U12.2.10V format
  UINT32 TargetMode       : 1;  ///< Voltage mode selection; <b>0: Adaptive</b>, 1: Override
  UINT32 VoltageOffsetS11 : 11; ///< Voltage offset represented in signed fixed point S11.0.10V format
} VF_MAILBOX_COMMAND_DATA;

///
///  High Level DDR capabilities data used to pass into the overclocking library for final
///  mailbox message creation.
///
typedef struct {
  UINT8   QclkRatio; ///< Qclk Ratio
  BOOLEAN McReferenceClk; ///< Memory Controller Reference Clock 0: 133 MHz, 1: 100 MHz
  UINT8   NumDdrChannels; ///< Number of DDR Channels
} DDR_CAPABILITIES_ITEM;


///
///  High level Voltage/Frequency information used to pass into the overclocking library
///  for the final mailbox message creation.
///
typedef struct {
  VOLTAGE_FREQUENCY_SETTINGS VfSettings; ///< Structure containing the voltage/frequency information
  UINT8                      DomainId; ///< CPU Domain ID
} VOLTAGE_FREQUENCY_ITEM;

///
///  High level OC Capabilities information used to pass into the overclocking library
///  for the final mailbox message creation.
///
typedef struct {
  UINT8   MaxOcRatioLimit;  ///< Max overclocking ratio limit of specified CPU domain
  BOOLEAN RatioOcSupported; ///< Ratio based overclocking support; 0: Not Supported, 1: Supported
  BOOLEAN VoltageOverridesSupported; ///< Voltage override support; 0: Not Supported, 1: Supported
  BOOLEAN VoltageOffsetSupported;    ///< Voltage offset support; 0: Not Supported, 1: Supported
  UINT8   DomainId; ///< CPU Domain ID
} OC_CAPABILITIES_ITEM;

///
/// High level per core ratio limit information used to pass into the overclocking library
/// for the final mailbox message creation.
///
typedef struct {
  UINT8 MaxOcRatioLimit1C; ///< Max 1-Core overclocking ratio limit
  UINT8 MaxOcRatioLimit2C; ///< Max 2-Core overclocking ratio limit
  UINT8 MaxOcRatioLimit3C; ///< Max 3-Core overclocking ratio limit
  UINT8 MaxOcRatioLimit4C; ///< Max 4-Core overclocking ratio limit
  UINT8 MaxOcRatioLimit5C; ///< Max 5-Core overclocking ratio limit
  UINT8 MaxOcRatioLimit6C; ///< Max 6-Core overclocking ratio limit
  UINT8 MaxOcRatioLimit7C; ///< Max 7-Core overclocking ratio limit
  UINT8 MaxOcRatioLimit8C; ///< Max 8-Core overclocking ratio limit
  UINT8 Index; ///< Index selector; 0: 1C to 4C, 1: 5C to 8C
} CORE_RATIO_LIMITS_ITEM;

///
///  High level AVX Ratio Offset information used to pass into the overclocking library
///  for the final mailbox message creation.
///
typedef struct {
  UINT8                      Avx2Ratio; ///< Avx 2 ratio
  UINT8                      Avx3Ratio; ///< Avx 3 ratio
} AVX_RATIO_ITEM;

///
/// PVD and FLL Override command offset information
///
typedef union {
  UINT32 Data;
  struct {
    UINT32 RatioThreshold       :  6; ///< [5:0]  Ratio Threshold value for PVD, Reserved for FLL overrides.
    UINT32 PvdFllOverclockMode  :  2; ///< [7:6]  PVD/FLL Overclock Mode
    UINT32 Reserved             : 24; ///< [31:8] Reserved
  }Fields;
} PVD_FLL_OVERRIDE;

///
/// OCMB VR Topology
///
typedef union {
  UINT32 Data;
  struct {
    UINT32  Reserved1            : 8; /// [07:00] Reserved
    UINT32  IaVrAddress          : 4; /// [11:08] IA VR Address
    UINT32  IaVrType             : 1; /// [12]    IA VR Type
    UINT32  GtVrAddress          : 4; /// [16:13] GT VR Address
    UINT32  GtVrType             : 1; /// [17]    GT VR Type
    UINT32  Reserved2            :14; /// [31:18] Reserved
  } Fields;
} OCMB_VR_TOPOLOGY_DATA;


///
/// OCMB ICC MAX
///
typedef union {
  UINT32 Data;
  struct {
    UINT32  IccMaxValue          :11; /// Sets/Gets the maximum desired current
    UINT32  Reserved             :20; /// Reserved
    UINT32  UnlimitedIccMaxMode  : 1; ///  Unlimited ICCMAX mode
  } Fields;
} OCMB_ICCMAX_DATA;

///
/// OC TVB Config
///
typedef union {
  UINT32 Data;
  struct {
    UINT32   TvbDownBinsTempThreshold0   :8; ///< Down Bins (delta) for Temperature Threshold 0
    UINT32   TvbTempThreshold0           :8; ///< Temperature Threshold 0
    UINT32   TvbTempThreshold1           :8; ///< Temperature Threshold 1
    UINT32   TvbDownBinsTempThreshold1   :8; ///< Down Bins (delta) for Temperature Threshold 1
  } Fields;
} OCMB_TVB_DATA;

///
/// OC PERSISTENT OVERRIDES
///
typedef union {
  UINT32 Data;
  struct {
    UINT32   FullRangeMultiplierUnlockEn      :1; ///< Bit[0] Core Ratio Extension mode. Required for allowing core ratio requests above 85.
    UINT32   SaPllFrequencyOverride           :1; ///< Bit[1] Configure SA PLL to run at 1600MHz (instead of 3200MHz on Desktop). Required for BCLK OC support.
    UINT32   FllOverclockingMode              :2; ///< Bit[3:2] FllOverclockingMode, 0x0 = no overclocking, 0x1 = ratio overclocking with nominal (0.5-1x) reference clock frequency,
                                                  ///< 0x2 = BCLK overclocking with elevated (1-3x) reference clock frequency, 0x3 = BCLK overclocking with extreme elevated (3-5x) reference clock frequency and ratio limited to 63
    UINT32   FllOcModeEn                      :1; ///< Bit[4] Enable FLL Mode override with the value in FLL_OVERCLOCKING_MODE.
    UINT32   TscHwFixUp                       :1; ///< Bit[5] Disable Core HW Fixup during TSC copy from PMA to APIC. Required for BCLK OC support.
    UINT32   Reserved                         :26;///< Bit[31:6] Reserved.
  } Fields;
} B2PMB_OC_PERSISTENT_OVERRIDES_DATA;

///
/// OC Library Function Prototypes
///
/**
  Gets the Voltage and Frequency information for a given CPU domain

  @param[out] *VfSettings
  @param[out] *LibStatus

  @retval EFI_STATUS
**/
EFI_STATUS
EFIAPI GetVoltageFrequencyItem (
  OUT VOLTAGE_FREQUENCY_ITEM *VfSettings,
  OUT UINT32                 *LibStatus
  );

/**
  Sets the Voltage and Frequency information for a given CPU domain

  @param[in]  *VfSettings
  @param[out] *LibStatus

  @retval EFI_STATUS
**/
EFI_STATUS
EFIAPI SetVoltageFrequencyItem (
  IN VOLTAGE_FREQUENCY_ITEM VfSettings,
  OUT UINT32                *LibStatus
  );

/**
  Update existing OC Voltage & Frequency item to the requested OC configuration.

  @param[in]  CurrentVfItem      The current domain VfSettings.
  @param[in]  RequestedVfItem    The requested domain VfSettings.
  @param[in]  WdtPei             watchdog timer for OC changes.

  @retval EFI_STATUS
**/
EFI_STATUS
EFIAPI
UpdateVoltageFrequencyItem (
  IN VOLTAGE_FREQUENCY_ITEM  *CurrentVfItem,
  IN VOLTAGE_FREQUENCY_ITEM  *RequestedVfItem,
  IN WDT_PPI                 *WdtPei           OPTIONAL
  );

/**
  Get the fused P0 ratio and voltage

  @param[out] *FusedP0Voltage in millivolt units
  @param[out] *FusedP0Ratio
  @param[out] *LibStatus

  @retval EFI_STATUS
**/
EFI_STATUS
EFIAPI
GetP0RatioVoltage (
  OUT UINT16 *FusedP0Voltage,
  OUT UINT16 *FusedP0Ratio,
  OUT UINT32 *LibStatus
  );

/**
  Get the overclocking capabilities for a given CPU Domain

  @param[out] *OcCapabilities
  @param[out] *LibStatus

  @retval EFI_STATUS
**/
EFI_STATUS
EFIAPI GetOcCapabilities (
  OUT OC_CAPABILITIES_ITEM *OcCapabilities,
  OUT UINT32               *LibStatus
  );

/**
  Converts the input votlage Offset to the fixed point S11.0.10 Volt format or
  to Binary millivolts representation based on the ConversionType.

  @param[in]  InputVoltageOffset
  @param[out] *OutputVoltageOffset
  @param[in]  ConversionType - 0:fixed point, 1:Signed Binary millivolts
**/
VOID
ConvertVoltageOffset (
  IN INT16  InputVoltageOffset,
  OUT INT16 *OutputVoltageOffset,
  IN UINT8  ConversionType
  );

/**
  Converts the input data to valid mailbox command format based on CommandID

@param[in]  InputData
@param[out] *MailboxData
@param[in]  CommandId
**/
VOID
ConvertToMailboxFormat (
  IN VOID             *InputData,
  OUT OC_MAILBOX_FULL *MailboxData,
  IN UINT32           CommandId
  );

/**
  Gets the AVX Ratio Offset

  @param[out] *AvxRatioOffset - The AVX ratio offset value read from OC mailbox.
  @param[out] *LibStatus - OC mailbox library return code

  @retval EFI_STATUS
**/
EFI_STATUS
EFIAPI
GetAvxRatioOffset (
  OUT AVX_RATIO_ITEM   *AvxRatioOffset,
  OUT UINT32           *LibStatus
  );

/**
  Sets the AVX Ratio Offset

  @param[in] AvxRatioOffset - The AVX ratio offset value to program to OC mailbox.
  @param[out] *LibStatus - OC mailbox library return code

  @retval EFI_STATUS
**/
EFI_STATUS
EFIAPI
SetAvxRatioOffset (
  OUT AVX_RATIO_ITEM   AvxRatioOffset,
  OUT UINT32           *LibStatus
  );

/**
  Initializes pre-memory Overclocking settings in the processor.

  @param[in] *SiPreMemPolicyPpi - SiPreMemPolicyPpi Pointer to Si Policy Ppi instance

  @retval EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
CpuOcInitPreMem (
  IN SI_PREMEM_POLICY_PPI *SiPreMemPolicyPpi
  );

/**
  Initializes post-memory Overclocking settings in the processor.

  @param[in] OverclockingtConfig      Pointer to Policy protocol instance
  @param[in] MpServices2Ppi            Pointer to this instance of the MpServices.

  @retval EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
CpuOcInitPostMem (
  IN OVERCLOCKING_PREMEM_CONFIG   *OverClockingConfig,
  IN EDKII_PEI_MP_SERVICES2_PPI   *MpServices2Ppi
  );

/**
  Initializes Overclocking settings in the processor.

  @param[in] *OcConfig                 - OcConfig Instance of OVERCLOCKING_PREMEM_CONFIG
  @param[in] *CpuConfigLibPreMemConfig - CpuConfigLibPreMemConfig Instance of CPU_CONFIG_LIB_PREMEM_CONFIG

  @retval EFI_SUCCESS
**/
EFI_STATUS
SaOcInit (
  IN OVERCLOCKING_PREMEM_CONFIG   *OcConfig,
  IN CPU_CONFIG_LIB_PREMEM_CONFIG *CpuConfigLibPreMemConfig
  );

#endif
