/** @file
  Initializes TSN mGBE Controller (TSN).

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Ppi/SiPolicy.h>
#include <Register/PchRegs.h>
#include <Register/TsnRegs.h>
#include <Register/PchPcrRegs.h>
#include <Library/IoLib.h>
#include <Library/TsnLib.h>
#include <Library/DebugLib.h>
#include <Library/TimerLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PeiItssLib.h>
#include <Library/PeiHsioLib.h>
#include <Library/PeiTsnInitLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/GpioPrivateLib.h>
#include <Library/SiConfigBlockLib.h>
#include <IndustryStandard/Pci30.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchFiaLib.h>
#include <Library/FiaSocLib.h>
#include <Library/PchPcrLib.h>
#include <Library/ChipsetInitLib.h>

typedef struct {
  UINT32       Offset;
  UINT32       AndData;
  UINT32       OrData;
} TSN_MODPHY_CONFIG;

TSN_MODPHY_CONFIG Tsn_Pll_1G_34Mhz[] = {
  {R_MODPHY1_PCR_LCPLL_DWORD0,    B_MODPHY1_PCR_LCPLL_DWORD0,    V_MODPHY1_PCR_LCPLL_DWORD0_CLK38_4MHZ_1G    },
  {R_MODPHY1_PCR_LCPLL_DWORD2,    B_MODPHY1_PCR_LCPLL_DWORD2,    V_MODPHY1_PCR_LCPLL_DWORD2_CLK38_4MHZ_1G    },
  {R_MODPHY1_PCR_LCPLL_DWORD7,    B_MODPHY1_PCR_LCPLL_DWORD7,    V_MODPHY1_PCR_LCPLL_DWORD7_CLK38_4MHZ_1G    },
  {R_MODPHY1_PCR_LPPLL_DWORD10,   B_MODPHY1_PCR_LPPLL_DWORD10,   V_MODPHY1_PCR_LPPLL_DWORD10_CLK38_4MHZ_1G   },
  {R_MODPHY1_PCR_CMN_ANA_DWORD10, B_MODPHY1_PCR_CMN_ANA_DWORD10, V_MODPHY1_PCR_CMN_ANA_DWORD10_CLK38_4MHZ_1G },
  {R_MODPHY1_PCR_CMN_ANA_DWORD30, B_MODPHY1_PCR_CMN_ANA_DWORD30, V_MODPHY1_PCR_CMN_ANA_DWORD30_CLK38_4MHZ_1G }
};

TSN_MODPHY_CONFIG Tsn_Pll_2_5G_34Mhz[] = {
  {R_MODPHY1_PCR_LCPLL_DWORD0,    B_MODPHY1_PCR_LCPLL_DWORD0,    V_MODPHY1_PCR_LCPLL_DWORD0_CLK38_4MHZ_2_5G    },
  {R_MODPHY1_PCR_LCPLL_DWORD2,    B_MODPHY1_PCR_LCPLL_DWORD2,    V_MODPHY1_PCR_LCPLL_DWORD2_CLK38_4MHZ_2_5G    },
  {R_MODPHY1_PCR_LCPLL_DWORD7,    B_MODPHY1_PCR_LCPLL_DWORD7,    V_MODPHY1_PCR_LCPLL_DWORD7_CLK38_4MHZ_2_5G    },
  {R_MODPHY1_PCR_LPPLL_DWORD10,   B_MODPHY1_PCR_LPPLL_DWORD10,   V_MODPHY1_PCR_LPPLL_DWORD10_CLK38_4MHZ_2_5G   },
  {R_MODPHY1_PCR_CMN_ANA_DWORD10, B_MODPHY1_PCR_CMN_ANA_DWORD10, V_MODPHY1_PCR_CMN_ANA_DWORD10_CLK24MHZ_2_5G   },
  {R_MODPHY1_PCR_CMN_ANA_DWORD30, B_MODPHY1_PCR_CMN_ANA_DWORD30, V_MODPHY1_PCR_CMN_ANA_DWORD30_CLK38_4MHZ_2_5G }
};


TSN_MODPHY_CONFIG Tsn_Pll_1G_24Mhz[] = {
  {R_MODPHY1_PCR_LCPLL_DWORD0,    B_MODPHY1_PCR_LCPLL_DWORD0,    V_MODPHY1_PCR_LCPLL_DWORD0_CLK24MHZ_1G    },
  {R_MODPHY1_PCR_LCPLL_DWORD2,    B_MODPHY1_PCR_LCPLL_DWORD2,    V_MODPHY1_PCR_LCPLL_DWORD2_CLK24MHZ_1G    },
  {R_MODPHY1_PCR_LCPLL_DWORD7,    B_MODPHY1_PCR_LCPLL_DWORD7,    V_MODPHY1_PCR_LCPLL_DWORD7_CLK24MHZ_1G    },
  {R_MODPHY1_PCR_LPPLL_DWORD10,   B_MODPHY1_PCR_LPPLL_DWORD10,   V_MODPHY1_PCR_LPPLL_DWORD10_CLK24MHZ_1G   },
  {R_MODPHY1_PCR_CMN_ANA_DWORD10, B_MODPHY1_PCR_CMN_ANA_DWORD10, V_MODPHY1_PCR_CMN_ANA_DWORD10_CLK24MHZ_1G },
  {R_MODPHY1_PCR_CMN_ANA_DWORD30, B_MODPHY1_PCR_CMN_ANA_DWORD30, V_MODPHY1_PCR_CMN_ANA_DWORD30_CLK24MHZ_1G }
};

TSN_MODPHY_CONFIG Tsn_Pll_2_5G_24Mhz[] = {
  {R_MODPHY1_PCR_LCPLL_DWORD0,    B_MODPHY1_PCR_LCPLL_DWORD0,    V_MODPHY1_PCR_LCPLL_DWORD0_CLK24MHZ_2_5G    },
  {R_MODPHY1_PCR_LCPLL_DWORD2,    B_MODPHY1_PCR_LCPLL_DWORD2,    V_MODPHY1_PCR_LCPLL_DWORD2_CLK24MHZ_2_5G    },
  {R_MODPHY1_PCR_LCPLL_DWORD7,    B_MODPHY1_PCR_LCPLL_DWORD7,    V_MODPHY1_PCR_LCPLL_DWORD7_CLK24MHZ_2_5G    },
  {R_MODPHY1_PCR_LPPLL_DWORD10,   B_MODPHY1_PCR_LPPLL_DWORD10,   V_MODPHY1_PCR_LPPLL_DWORD10_CLK24MHZ_2_5G   },
  {R_MODPHY1_PCR_CMN_ANA_DWORD10, B_MODPHY1_PCR_CMN_ANA_DWORD10, V_MODPHY1_PCR_CMN_ANA_DWORD10_CLK24MHZ_2_5G },
  {R_MODPHY1_PCR_CMN_ANA_DWORD30, B_MODPHY1_PCR_CMN_ANA_DWORD30, V_MODPHY1_PCR_CMN_ANA_DWORD30_CLK24MHZ_2_5G }
};

TSN_MODPHY_CONFIG Tsn_Pll_Common[] = {
  {R_MODPHY1_PCR_LCPLL_DWORD4,    B_MODPHY1_PCR_LCPLL_DWORD4,    V_MODPHY1_PCR_LCPLL_DWORD4_COMMON    },
  {R_MODPHY1_PCR_LCPLL_DWORD6,    B_MODPHY1_PCR_LCPLL_DWORD6,    V_MODPHY1_PCR_LCPLL_DWORD6_COMMON    },
  {R_MODPHY1_PCR_LCPLL_DWORD8,    B_MODPHY1_PCR_LCPLL_DWORD8,    V_MODPHY1_PCR_LCPLL_DWORD8_COMMON    },
  {R_MODPHY1_PCR_LCPLL_DWORD11,   B_MODPHY1_PCR_LCPLL_DWORD11,   V_MODPHY1_PCR_LCPLL_DWORD11_COMMON   },
  {R_MODPHY1_PCR_LCPLL_DWORD12,   B_MODPHY1_PCR_LCPLL_DWORD12,   V_MODPHY1_PCR_LCPLL_DWORD12_COMMON   },
  {R_MODPHY1_PCR_LCPLL_DWORD18,   B_MODPHY1_PCR_LCPLL_DWORD18,   V_MODPHY1_PCR_LCPLL_DWORD18_COMMON   },
  {R_MODPHY1_PCR_LCPLL_DWORD19,   B_MODPHY1_PCR_LCPLL_DWORD19,   V_MODPHY1_PCR_LCPLL_DWORD19_COMMON   },
  {R_MODPHY1_PCR_CMN_DIG_DWORD12, B_MODPHY1_PCR_CMN_DIG_DWORD12, V_MODPHY1_PCR_CMN_DIG_DWORD12_COMMON },
  {R_MODPHY1_PCR_CMN_DIG_DWORD15, B_MODPHY1_PCR_CMN_DIG_DWORD15, V_MODPHY1_PCR_CMN_DIG_DWORD15_COMMON }
};

TSN_MODPHY_CONFIG Tsn_Dlane6_Data[] = {
  {R_MODPHY1_DLANE6_PCR_SET3,                    B_MODPHY1_PCR_SET3,                    V_MODPHY1_PCR_COMMON                         },
  {R_MODPHY1_DLANE6_PCR_DWORD6,                  B_MODPHY1_PCR_DWORD6,                  V_MODPHY1_PCR_DWORD6                         },
  {R_MODPHY1_DLANE6_PCR_2TAP_DEEMPH3P5_RATE1,    B_MODPHY1_PCR_2TAP_DEEMPH3P5_RATE1,    V_MODPHY1_PCR_2TAP_DEEMPH3P5_RATE1_COMMON    },
  {R_MODPHY1_DLANE6_PCR_2TAP_DEEMPH3P5_RATE0,    B_MODPHY1_PCR_2TAP_DEEMPH3P5_RATE0,    V_MODPHY1_PCR_2TAP_DEEMPH3P5_RATE0_COMMON    },
  {R_MODPHY1_DLANE6_PCR_RXPICTRL1,               B_MODPHY1_PCR_RXPICTRL1,               V_MODPHY1_PCR_RXPICTRL1_COMMON               },
  {R_MODPHY1_DLANE6_PCR_RSVD4,                   B_MODPHY1_PCR_RSVD4,                   V_MODPHY1_PCR_RSVD4_COMMON                   },
  {R_MODPHY1_DLANE6_PCR_RSVD9,                   B_MODPHY1_PCR_RSVD9,                   V_MODPHY1_PCR_RSVD9_COMMON                   },
  {R_MODPHY1_DLANE6_PCR_RSVD19,                  B_MODPHY1_PCR_RSVD19,                  V_MODPHY1_PCR_RSVD19_COMMON                  },
  {R_MODPHY1_DLANE6_PCR_REG1C,                   B_MODPHY1_PCR_REG1C,                   V_MODPHY1_PCR_REG1C_COMMON                   },
  {R_MODPHY1_DLANE6_PCR_REG21,                   B_MODPHY1_PCR_REG21,                   V_MODPHY1_PCR_REG21_COMMON                   },
  {R_MODPHY1_DLANE6_PCR_RXUPIFCFGGHALFRATE_0,    B_MODPHY1_PCR_RXUPIFCFGGHALFRATE_0,    V_MODPHY1_PCR_RXUPIFCFGGHALFRATE_0_COMMON    },
  {R_MODPHY1_DLANE6_PCR_RXUPPFCFGGQUARTERRATE_0, B_MODPHY1_PCR_RXUPPFCFGGQUARTERRATE_0, V_MODPHY1_PCR_RXUPPFCFGGQUARTERRATE_0_COMMON },
  {R_MODPHY1_DLANE6_PCR_REG54,                   B_MODPHY1_PCR_REG54,                   V_MODPHY1_PCR_REG54_COMMON                   },
  {R_MODPHY1_DLANE6_PCR_RSVD5E,                  B_MODPHY1_PCR_RSVD5E,                  V_MODPHY1_PCR_RSVD5E_COMMON                  },
  {R_MODPHY1_DLANE6_PCR_RSVD75,                  B_MODPHY1_PCR_RSVD75,                  V_MODPHY1_PCR_RSVD75_COMMON                  },
  {R_MODPHY1_DLANE6_PCR_REG7A,                   B_MODPHY1_PCR_REG7A,                   V_MODPHY1_PCR_REG7A_COMMON                   },
  {R_MODPHY1_DLANE6_PCR_RSVD8B,                  B_MODPHY1_PCR_RSVD8B,                  V_MODPHY1_PCR_RSVD8B_COMMON                  },
  {R_MODPHY1_DLANE6_PCR_RSVD8D,                  B_MODPHY1_PCR_RSVD8D,                  V_MODPHY1_PCR_RSVD8D_COMMON                  },
  {R_MODPHY1_DLANE6_PCR_RSVDEB,                  B_MODPHY1_PCR_RSVDEB,                  V_MODPHY1_PCR_RSVDEB_COMMON                  }
};

TSN_MODPHY_CONFIG Tsn_Dlane7_Data[] = {
  {R_MODPHY1_DLANE7_PCR_SET3,                    B_MODPHY1_PCR_SET3,                    V_MODPHY1_PCR_COMMON                         },
  {R_MODPHY1_DLANE6_PCR_DWORD6,                  B_MODPHY1_PCR_DWORD6,                  V_MODPHY1_PCR_DWORD6                         },
  {R_MODPHY1_DLANE7_PCR_2TAP_DEEMPH3P5_RATE1,    B_MODPHY1_PCR_2TAP_DEEMPH3P5_RATE1,    V_MODPHY1_PCR_2TAP_DEEMPH3P5_RATE1_COMMON    },
  {R_MODPHY1_DLANE7_PCR_2TAP_DEEMPH3P5_RATE0,    B_MODPHY1_PCR_2TAP_DEEMPH3P5_RATE0,    V_MODPHY1_PCR_2TAP_DEEMPH3P5_RATE0_COMMON    },
  {R_MODPHY1_DLANE7_PCR_RXPICTRL1,               B_MODPHY1_PCR_RXPICTRL1,               V_MODPHY1_PCR_RXPICTRL1_COMMON               },
  {R_MODPHY1_DLANE7_PCR_RSVD4,                   B_MODPHY1_PCR_RSVD4,                   V_MODPHY1_PCR_RSVD4_COMMON                   },
  {R_MODPHY1_DLANE7_PCR_RSVD9,                   B_MODPHY1_PCR_RSVD9,                   V_MODPHY1_PCR_RSVD9_COMMON                   },
  {R_MODPHY1_DLANE7_PCR_RSVD19,                  B_MODPHY1_PCR_RSVD19,                  V_MODPHY1_PCR_RSVD19_COMMON                  },
  {R_MODPHY1_DLANE7_PCR_REG1C,                   B_MODPHY1_PCR_REG1C,                   V_MODPHY1_PCR_REG1C_COMMON                   },
  {R_MODPHY1_DLANE7_PCR_REG21,                   B_MODPHY1_PCR_REG21,                   V_MODPHY1_PCR_REG21_COMMON                   },
  {R_MODPHY1_DLANE7_PCR_RXUPIFCFGGHALFRATE_0,    B_MODPHY1_PCR_RXUPIFCFGGHALFRATE_0,    V_MODPHY1_PCR_RXUPIFCFGGHALFRATE_0_COMMON    },
  {R_MODPHY1_DLANE7_PCR_RXUPPFCFGGQUARTERRATE_0, B_MODPHY1_PCR_RXUPPFCFGGQUARTERRATE_0, V_MODPHY1_PCR_RXUPPFCFGGQUARTERRATE_0_COMMON },
  {R_MODPHY1_DLANE7_PCR_REG54,                   B_MODPHY1_PCR_REG54,                   V_MODPHY1_PCR_REG54_COMMON                   },
  {R_MODPHY1_DLANE7_PCR_RSVD5E,                  B_MODPHY1_PCR_RSVD5E,                  V_MODPHY1_PCR_RSVD5E_COMMON                  },
  {R_MODPHY1_DLANE7_PCR_RSVD75,                  B_MODPHY1_PCR_RSVD75,                  V_MODPHY1_PCR_RSVD75_COMMON                  },
  {R_MODPHY1_DLANE7_PCR_REG7A,                   B_MODPHY1_PCR_REG7A,                   V_MODPHY1_PCR_REG7A_COMMON                   },
  {R_MODPHY1_DLANE7_PCR_RSVD8B,                  B_MODPHY1_PCR_RSVD8B,                  V_MODPHY1_PCR_RSVD8B_COMMON                  },
  {R_MODPHY1_DLANE7_PCR_RSVD8D,                  B_MODPHY1_PCR_RSVD8D,                  V_MODPHY1_PCR_RSVD8D_COMMON                  },
  {R_MODPHY1_DLANE7_PCR_RSVDEB,                  B_MODPHY1_PCR_RSVDEB,                  V_MODPHY1_PCR_RSVDEB_COMMON                  }
};

//
// Maximum loop time for Tsn status check
// 4000 * 50 = 200 mSec in total
//
#define TSN_MAX_LOOP_TIME       4000
#define TSN_GMII_DELAY          50

/**
  Modify HSIO lane DWORD and update ChipsetInit SUS Table.

  @param[in] HsioLane   HSIO Lane
  @param[in] Offset     PCR offset
  @param[in] AndMask    Mask to be ANDed with original value.
  @param[in] OrMask     Mask to be ORed with original value.
**/
STATIC
VOID
TsnPchPcrAndThenOr32WithSusWrite (
  IN  PCH_SBI_PID                       Pid,
  IN  UINT32                            Offset,
  IN  UINT32                            AndData,
  IN  UINT32                            OrData
  )
{
  PchPcrAndThenOr32 (Pid, Offset, AndData, OrData);
  HsioChipsetInitSusWrite32 (
    Pid,
    Offset,
    PchPcrRead32 (Pid, Offset),
    AndData,
    OrData
    );
}

/**
  The function programs the TSN PLL registers.

  @param[in]  PortId          PID of the modphy PLL to use
  @param[in]  TablePtr        Pointer to Tsn Modphy Table
  @param[in]  NumberOfEntry   Number of entries to Modphy Table
**/
STATIC
VOID
TsnModPhyProg (
  PCH_SBI_PID             PortId,
  TSN_MODPHY_CONFIG       *TablePtr,
  UINT16                  NumberOfEntry
  )
{
  UINT32                  Index;
  TSN_MODPHY_CONFIG       *TempPtr;

  TempPtr = TablePtr;

  DEBUG ((DEBUG_INFO, "Tsn ModPhy Prog: TableEntry %d\n", NumberOfEntry));

  for (Index = 0; Index < NumberOfEntry; Index ++) {
    TsnPchPcrAndThenOr32WithSusWrite (
      PortId,
      TempPtr->Offset,
      (UINT32) ~(TempPtr->AndData),
      TempPtr->OrData
      );

    // Read back
    DEBUG ((DEBUG_INFO, "Tsn ModPhy Prog: PortId: 0x%02x, Offset: 0x%04x, Value: 0x%08x\n", PortId, TempPtr->Offset, PchPcrRead32(PortId, TempPtr->Offset)));

    TempPtr++;
  }

}

/**
  The function Initialize the Tsn ModPhy registers.

  @param[in]  TsnLinkSpeed      Tsn Link Speed
  @param[in]  LaneNumber        Tsn Lane Number
  @param[in]  SkipPll           SkipPll
**/
STATIC
VOID
TsnModPhyInit (
  UINT8                   TsnLinkSpeed,
  UINT8                   LaneNumber,
  BOOLEAN                 SkipPll
  )
{
  TSN_MODPHY_CONFIG *ModPhyTablePtr;
  UINT16            TableEntry;
  PCH_SBI_PID       PortId = PID_MODPHY1;

  if (!SkipPll) {

    DEBUG ((DEBUG_INFO, "Tsn ModPhy Prog: Configure Lane %d, LinkSpeed %x\n", LaneNumber, TsnLinkSpeed));

    if (IsPchLp () && (LaneNumber == 6 || LaneNumber == 7)) {
      PortId = PID_MODPHY1;
    } else if (IsPchH () && (LaneNumber == 22 || LaneNumber == 23)) {
      PortId = PID_MODPHY2;
    } else {
      DEBUG ((DEBUG_INFO, "Tsn FIA Lane %d not supported\n", LaneNumber));
      return;
    }

    switch (TsnLinkSpeed) {
      case (TSN_LINK_SPEED_2_5G_24MHZ):
        ModPhyTablePtr = &Tsn_Pll_2_5G_24Mhz[0];
        TableEntry = ARRAY_SIZE (Tsn_Pll_2_5G_24Mhz);
        break;

      case (TSN_LINK_SPEED_1G_24MHZ):
        ModPhyTablePtr = &Tsn_Pll_1G_24Mhz[0];
        TableEntry = ARRAY_SIZE (Tsn_Pll_1G_24Mhz);
        break;

      case (TSN_LINK_SPEED_2_5G_34MHZ):
        ModPhyTablePtr = &Tsn_Pll_2_5G_34Mhz[0];
        TableEntry = ARRAY_SIZE (Tsn_Pll_2_5G_34Mhz);
        break;

      case (TSN_LINK_SPEED_1G_34MHZ):
        ModPhyTablePtr = &Tsn_Pll_1G_34Mhz[0];
        TableEntry = ARRAY_SIZE (Tsn_Pll_1G_34Mhz);
        break;

      default:
        ModPhyTablePtr = &Tsn_Pll_2_5G_34Mhz[0];
        TableEntry = ARRAY_SIZE (Tsn_Pll_2_5G_34Mhz); // RefClk 38.4Mhz 2.5G Speed
        break;
    }

    TsnModPhyProg (PortId, ModPhyTablePtr, TableEntry);

    // Program Common PLL
    ModPhyTablePtr = &Tsn_Pll_Common[0];
    TableEntry = ARRAY_SIZE (Tsn_Pll_Common);
    TsnModPhyProg (PortId, ModPhyTablePtr, TableEntry);
  }

  // Program DLane
  if (LaneNumber == 6) {
    ModPhyTablePtr = &Tsn_Dlane6_Data[0];
    TableEntry = ARRAY_SIZE (Tsn_Dlane6_Data);
  } else if ( LaneNumber == 7 ){
    ModPhyTablePtr = &Tsn_Dlane7_Data[0];
    TableEntry = ARRAY_SIZE (Tsn_Dlane7_Data);
  }

  TsnModPhyProg (PortId, ModPhyTablePtr, TableEntry);

}

/**
  The function programs the Tc to DMA channel mapping
**/
STATIC
VOID
TsnTcDmaMapping (
  VOID
  )
{
  // Tc to DMA channel mapping
  PchPcrAndThenOr32 (
    PID_TSN,
    R_TSN_PCR_AXIID_TO_TC0_MAP_0,
    (UINT32) ~B_TSN_PCR_AXIID_TO_TC0_MAP_0,
    V_TSN_PCR_AXIID_TO_TC0_MAP_0
    );
  PchPcrAndThenOr32 (
    PID_TSN,
    R_TSN_PCR_AXIID_TO_TC1_MAP_0,
    (UINT32) ~B_TSN_PCR_AXIID_TO_TC1_MAP_0,
    V_TSN_PCR_AXIID_TO_TC1_MAP_0
    );

  //MSI Tc to DMA channel mapping
  PchPcrAndThenOr32 (
    PID_TSN,
    R_TSN_PCR_MSI_TO_TC0_MAP,
    (UINT32) ~B_TSN_PCR_MSI_TO_TC0_MAP,
    V_TSN_PCR_MSI_TO_TC0_MAP
    );
  PchPcrAndThenOr32 (
    PID_TSN,
    R_TSN_PCR_MSI_TO_TC1_MAP,
    (UINT32) ~B_TSN_PCR_MSI_TO_TC1_MAP,
    V_TSN_PCR_MSI_TO_TC1_MAP
    );
}

/**
  Check for MDIO Phy GMII Busy Status.

  1. Ensure that MDIO_ADDRESS offset 200h [0] = 0b
  2. Poll MDIO_ADDRESS offset 200h [0] up to 200ms

  @param [in] PchTsnBar0   Tsn MMIO space

  @retval EFI_SUCCESS
  @retval EFI_TIMEOUT
**/
EFI_STATUS
PhyGmiiBusyStatus (
  IN      UINT32  PchTsnBar0
  )
{
  UINT32  Count;

  for (Count = 0; Count < TSN_MAX_LOOP_TIME; ++Count) {
    if (MmioRead32 (PchTsnBar0 + R_TSN_MEM_MAC_MDIO_ADDRESS) & B_TSN_MEM_MAC_MDIO_GMII_BUSY) {
      MicroSecondDelay (TSN_GMII_DELAY);
    } else {
      return EFI_SUCCESS;
    }
  }
  DEBUG ((DEBUG_ERROR, "PhyGMIIBusyStatus Timeout in %d micro seconds\n", TSN_MAX_LOOP_TIME * TSN_GMII_DELAY));
  return EFI_TIMEOUT;
}

/**
  Configures Timed Sensitive Network (TSN) Controller Registers and MAC Programming

  @param[in] SiPolicy
  @param[in] PchTsnBase  - Pch Tsn Base Address
**/
STATIC
VOID
PchTsnBiosProg (
  IN  SI_POLICY_PPI   *SiPolicy,
  IN  UINT32          PchTsnBase
  )
{
  EFI_STATUS           Status;
  TSN_CONFIG           *TsnConfig;
  UINT16               GcrValue;
  UINT32               PchTsnBar0;
  UINT32               MacLow;
  UINT32               MacHigh;
  UINT32               MacMdioAddressValue;
  EFI_BOOT_MODE        BootMode;

  DEBUG ((DEBUG_INFO, "PchTsnBiosProg() Start\n"));

  Status = GetConfigBlock ((VOID *) SiPolicy, &gTsnConfigGuid, (VOID *) &TsnConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  if (BootMode != BOOT_ON_S3_RESUME) {
    // Program Temp Bar
    PciSegmentWrite32 (PchTsnBase + R_TSN_CFG_BAR0_LOW,  PcdGet32 (PcdSiliconInitTempMemBaseAddr));
    PciSegmentWrite32 (PchTsnBase + R_TSN_CFG_BAR0_HIGH, 0x0);
    PciSegmentWrite32 (PchTsnBase + R_TSN_CFG_BAR1_LOW,  0x0);
    PciSegmentWrite32 (PchTsnBase + R_TSN_CFG_BAR1_HIGH, 0x0);

    // Enable Memory Space encoding
    PciSegmentOr32 (PchTsnBase + PCI_COMMAND_OFFSET, (EFI_PCI_COMMAND_MEMORY_SPACE));

    // Program MAC address for TSN on PCH
    PchTsnBar0 = PciSegmentRead32 (PchTsnBase + R_TSN_CFG_BAR0_LOW) & ~(B_TSN_CFG_BAR0_MASK);

    DEBUG ((DEBUG_INFO, "PchTsnBar0: 0x%x\n",PchTsnBar0));

    if (PchTsnBar0 != 0xFFFFF000) {
     DEBUG ((DEBUG_INFO, "Start Mac address programming\n"));

      MmioWrite32 (PchTsnBar0 + R_TSN_MEM_MAC_ADDRESS0_LOW, TsnConfig->TsnMacAddressLow);
      //
      // Program MacAddressHigh[15:0]
      // MacAddressHigh[31] Address Enable Bit
      //
      MmioWrite32 (PchTsnBar0 + R_TSN_MEM_MAC_ADDRESS0_HIGH, ((TsnConfig->TsnMacAddressHigh & B_TSN_MEM_MAC_ADDRESS_HIGH_MASK) | B_TSN_MEM_ADDRESS_ENABLE));
      MacLow = MmioRead32 (PchTsnBar0 + R_TSN_MEM_MAC_ADDRESS0_LOW);
      MacHigh = MmioRead32 (PchTsnBar0 + R_TSN_MEM_MAC_ADDRESS0_HIGH);
      DEBUG ((DEBUG_INFO, "Mac Address Low: 0x%x Mac Address High: 0x%x \n", MacLow, MacHigh));
      Status = EFI_SUCCESS;
    } else {
      DEBUG ((DEBUG_INFO, "TSN BAR not found\n"));
      Status = EFI_UNSUPPORTED;
    }

    //
    // Reading Tsn Adhoc Register GCR mgbe_mdio
    //
    MacMdioAddressValue = MmioRead32 (PchTsnBar0 + R_TSN_MEM_MAC_MDIO_ADDRESS);
    MacMdioAddressValue &= (UINT32) ~(B_TSN_MEM_MAC_MDIO_ADDRESS_MASK);
    MacMdioAddressValue |= (V_TSN_MEM_MAC_MDIO_ADHOC_GCR_ADD << N_TSN_MEM_MAC_MDIO_PHYAD) \
                         | (V_TSN_MEM_MAC_MDIO_CLK_TRAIL << N_TSN_MEM_MAC_MDIO_CLK_TRAIL) \
                         | (V_TSN_MEM_MAC_MDIO_CLK_CSR_DIV_10 << N_TSN_MEM_MAC_MDIO_CLK_CSR) \
                         | V_TSN_MEM_MAC_MDIO_GMII_22_PHY_READ | B_TSN_MEM_MAC_MDIO_GMII_BUSY;
    MmioWrite32 (PchTsnBar0 + R_TSN_MEM_MAC_MDIO_ADDRESS, MacMdioAddressValue);

    //
    // Wait for MDIO frame transfer complete before reading MDIO DATA register
    //
    Status = PhyGmiiBusyStatus(PchTsnBar0);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "TSN GMII Busy. Adhoc Register read invalid\n"));
    }
    GcrValue = MmioRead16 (PchTsnBar0 + R_TSN_MEM_MAC_MDIO_DATA);
    DEBUG ((DEBUG_INFO, "TSN MDIO (0x200): 0x%x GCR : 0x%x\n", MacMdioAddressValue, GcrValue));

    //
    // Disable Auto-Negotiation and Clear Adhoc Link Mode Bit
    //
    GcrValue &= (UINT16) ~B_TSN_MEM_MAC_ADHOC_LINK_MODE;
    GcrValue |= B_TSN_MEM_MAC_ADHOC_PHY_AUTONEG_DISABLE;

    switch (TsnConfig->TsnLinkSpeed) {
      case (TSN_LINK_SPEED_2_5G_24MHZ):
        GcrValue |= (UINT16) V_TSN_MEM_MAC_ADHOC_LINK_MODE_2_5G;
        break;
      case (TSN_LINK_SPEED_1G_24MHZ):
        GcrValue |= (UINT16) V_TSN_MEM_MAC_ADHOC_LINK_MODE_1G;
        break;
      case (TSN_LINK_SPEED_2_5G_34MHZ):
        GcrValue |= (UINT16) V_TSN_MEM_MAC_ADHOC_LINK_MODE_2_5G;
        break;
      case (TSN_LINK_SPEED_1G_34MHZ):
        GcrValue |= (UINT16) V_TSN_MEM_MAC_ADHOC_LINK_MODE_1G;
        break;
      default:
        break;
    }

    //
    // Write Link Speed Mode to Global Configuration Register (GCR)
    //
    MmioWrite16 (PchTsnBar0 + R_TSN_MEM_MAC_MDIO_DATA, GcrValue);
    MacMdioAddressValue = MmioRead32 (PchTsnBar0 + R_TSN_MEM_MAC_MDIO_ADDRESS);
    MacMdioAddressValue &= (UINT32) ~(B_TSN_MEM_MAC_MDIO_ADDRESS_MASK);
    MacMdioAddressValue |= (V_TSN_MEM_MAC_MDIO_ADHOC_GCR_ADD << N_TSN_MEM_MAC_MDIO_PHYAD) \
                         | (V_TSN_MEM_MAC_MDIO_CLK_TRAIL << N_TSN_MEM_MAC_MDIO_CLK_TRAIL) \
                         | (V_TSN_MEM_MAC_MDIO_CLK_CSR_DIV_10 << N_TSN_MEM_MAC_MDIO_CLK_CSR) \
                         | V_TSN_MEM_MAC_MDIO_GMII_22_PHY_WRITE | B_TSN_MEM_MAC_MDIO_GMII_BUSY;
    MmioWrite32 (PchTsnBar0 + R_TSN_MEM_MAC_MDIO_ADDRESS, MacMdioAddressValue);
    DEBUG ((DEBUG_INFO, "TSN MDIO (0x200): 0x%x GCR : 0x%x\n", MacMdioAddressValue, GcrValue));

    // Disable Memory Space encoding
    PciSegmentAnd16 (PchTsnBase + PCI_COMMAND_OFFSET, (UINT16) (~EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_BUS_MASTER));
    PciSegmentWrite32 (PchTsnBase + R_TSN_CFG_BAR0_LOW,  0);
  }

  //
  // Configure Multi-Vc via Tc to DMA channel mapping
  //
  if (TsnConfig->MultiVcEnable == 1) {
    TsnTcDmaMapping ();
  }

  DEBUG ((DEBUG_INFO, "PchTsnBiosProg () End\n"));
}

/**
  Configures Timed Sensitive Network (TSN) Controller Interrupt

  @param[in] SiPolicy
**/
STATIC
VOID
TsnConfigureInterrupt (
  IN  SI_POLICY_PPI   *SiPolicy
  )
{
  UINT8     InterruptPin;
  UINT8     Irq;
  UINT32    Data32Or;
  UINT32    Data32And;

  DEBUG ((DEBUG_INFO, "TsnConfigureInterrupt() Start\n"));

  ///
  /// Configure TSN interrupt
  ///
  ItssGetDevIntConfig (
    SiPolicy,
    TsnDevNumber (),
    TsnFuncNumber (),
    &InterruptPin,
    &Irq
    );

  Data32Or =  (UINT32) ((InterruptPin << N_TSN_PCR_PCICFGCTR_IPIN1) |
                        (Irq << N_TSN_PCR_PCICFGCTR_PCI_IRQ));
  Data32And =~(UINT32) (B_TSN_PCR_PCICFGCTR_PCI_IRQ | B_TSN_PCR_PCICFGCTR_ACPI_IRQ | B_TSN_PCR_PCICFGCTR_IPIN1);

  PchPcrAndThenOr32 (PID_TSN, R_TSN_PCR_PCICFGCTRL, Data32And, Data32Or);

  DEBUG ((DEBUG_INFO, "TsnConfigureInterrupt() End\n"));
}

/**
  Initialize the Intel TSN Controller

  @param[in] SiPolicy             Policy
**/
VOID
TsnInit (
  IN  SI_POLICY_PPI    *SiPolicy
  )
{
  EFI_STATUS                      Status;
  TSN_CONFIG                      *TsnConfig;
  UINT32                          PchTsnBase;
  UINT8                           PchTsnLane;
  BOOLEAN                         IsFiaLanePchTsnOwned;
  UINT8                           SkipPll;

  PchTsnLane = 0;

  DEBUG ((DEBUG_INFO, "TsnInit() Start\n"));
  Status = GetConfigBlock ((VOID *) SiPolicy, &gTsnConfigGuid, (VOID *) &TsnConfig);
  ASSERT_EFI_ERROR (Status);

  PchTsnBase = (UINT32)TsnPciCfgBase ();

  //
  // Check if Tsn is available
  //
  if ((PciSegmentRead16 (PchTsnBase + PCI_VENDOR_ID_OFFSET)) == 0xFFFF) {
    DEBUG ((DEBUG_INFO, "Tsn config space not accessible! Tsn not available\n"));
  } else {
    if (TsnConfig->Enable) {
      DEBUG ((DEBUG_INFO, "TSN is Enabled\n"));
      GpioEnableTsn ();
      if(IsPchChipsetInitSyncSupported ()) {
        PchTsnBiosProg (SiPolicy, PchTsnBase);

        //
        // Configure TSN PLL
        //
        IsFiaLanePchTsnOwned  = PchFiaGetTsnLaneNum (0, &PchTsnLane);

        if (TsnConfig->Enable && IsFiaLanePchTsnOwned) {
          SkipPll = 0;

          //
          // Configure TsnModPhy
          //
          TsnModPhyInit (TsnConfig->TsnLinkSpeed, PchTsnLane, SkipPll);
        }
      }
      TsnConfigureInterrupt (SiPolicy);
    } else {
      DEBUG ((DEBUG_INFO, "TSN is Disabled\n"));
    }
  }
  DEBUG ((DEBUG_INFO, "TsnInit() End\n"));
}
