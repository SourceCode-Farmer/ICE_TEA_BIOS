/** @file
  This file provides services for TSN policy function

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Library/SiConfigBlockLib.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <TsnConfig.h>

/**
  Print TSN_CONFIG and serial out.

  @param[in] SiPolicyPpi            Pointer to a SI_POLICY_PPI
**/
VOID
TsnPrintConfig (
  IN SI_POLICY_PPI     *SiPolicyPpi
  )
{
  EFI_STATUS    Status;
  TSN_CONFIG    *TsnConfig;

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gTsnConfigGuid, (VOID *) &TsnConfig);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "------------------ Tsn Config ------------------\n"));
  DEBUG ((DEBUG_INFO, " Enable= %x\n", TsnConfig->Enable));
  DEBUG ((DEBUG_INFO, " TsnLinkSpeed= %x\n", TsnConfig->TsnLinkSpeed));
  DEBUG ((DEBUG_INFO, " TsnMultiVcEnable  = %x\n", TsnConfig->MultiVcEnable));
  DEBUG ((DEBUG_INFO, " TsnMacAddressHigh = %x\n", TsnConfig->TsnMacAddressHigh));
  DEBUG ((DEBUG_INFO, " TsnMacAddressLow  = %x\n", TsnConfig->TsnMacAddressLow));
}

/**
  Load Config block default

  @param[in] ConfigBlockPointer         Pointer to config block
**/
VOID
TsnLoadConfigDefault (
  IN VOID          *ConfigBlockPointer
  )
{
  TSN_CONFIG      *TsnConfig;

  TsnConfig = ConfigBlockPointer;

  DEBUG ((DEBUG_INFO, "TsnConfig->Header.GuidHob.Name = %g\n", &TsnConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "TsnConfig->Header.GuidHob.Header.HobLength = 0x%x\n", TsnConfig->Header.GuidHob.Header.HobLength));

  TsnConfig->Enable = TRUE;
  TsnConfig->TsnLinkSpeed = TSN_LINK_SPEED_2_5G_34MHZ;
  TsnConfig->MultiVcEnable = (UINT8) 0x0;
  TsnConfig->TsnMacAddressHigh = (UINT32) 0x0;
  TsnConfig->TsnMacAddressLow = (UINT32) 0x0;
}

STATIC COMPONENT_BLOCK_ENTRY  mTsnBlocks = {
  &gTsnConfigGuid,
  sizeof (TSN_CONFIG),
  TSN_CONFIG_REVISION,
  TsnLoadConfigDefault
};

/**
  Get Tsn config block table size.

  @retval      Size of config block
**/
UINT16
TsnGetConfigBlockTotalSize (
  VOID
  )
{
  return mTsnBlocks.Size;
}

/**
  Add Tsn ConfigBlock.

  @param[in] ConfigBlockTableAddress    The pointer to config block table

  @retval EFI_SUCCESS                   The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES          Insufficient resources to create buffer
**/
EFI_STATUS
TsnAddConfigBlock (
  IN VOID           *ConfigBlockTableAddress
  )
{
  return AddComponentConfigBlocks (ConfigBlockTableAddress, &mTsnBlocks, 1);
}
