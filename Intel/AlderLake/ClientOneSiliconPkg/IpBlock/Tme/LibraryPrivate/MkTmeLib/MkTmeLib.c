/** @file
  Initilize TME in PEI

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 - 2022 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/PeiServicesLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/TmeLib.h>
#include <Library/MsrFruLib.h>
#include <Library/PmcLib.h>
#include <Library/IoLib.h>
#include <Library/CpuPlatformLib.h>
#include <CpuRegs.h>
#include <CpuDataStruct.h>
#include <Register/Cpuid.h>
#include <Register/CommonMsr.h>
#include <Register/ArchMsr.h>
#include <Register/PmcRegs.h>
#include <Ppi/MpServices2.h>

#define TME_ACTIVATE_RETRY_LIMIT  10

typedef struct {
  UINT64 TmeEnable          :1;
  UINT64 MkTmeMaxKeyIdBits  :4;
  UINT64 MkTmeMaxKey        :15;
  UINT64 TmePolicy          :4;
  UINT64 KeySelect          :1;
  UINT64 SaveKeyForStandby  :1;
  UINT64 MkTmeKeyIdBits     :4;
  UINT64 MkTmeCryptoAlgs    :16;
} TME_CONFIG;

/**
  Configure TME Core Activate MSR across all Cores.

**/
EFI_STATUS
EFIAPI
ConfigureTmeCoreActivateMsr (
  IN  EFI_PEI_SERVICES             **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR    *NotifyDescriptor,
  IN  VOID                         *Ppi
  );

/**
  Notify on MpServicePpi to configure TME Core Activate MSR.
**/
static EFI_PEI_NOTIFY_DESCRIPTOR  mMpServicePpiCallbackNotifyList[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_NOTIFY_DISPATCH | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gEdkiiPeiMpServices2PpiGuid,
    ConfigureTmeCoreActivateMsr
  }
};

/**
  Determines maximum physical address width using CPUID command.

  @retval MaxPhyAddrBits - maximum physical address width that can be used on the system.
**/
UINT32
GetMaxPhyAddrBits ()
{
  CPUID_VIR_PHY_ADDRESS_SIZE_EAX               CpuIdEax;

  AsmCpuidEx (CPUID_VIR_PHY_ADDRESS_SIZE, 0, &CpuIdEax.Uint32, NULL, NULL, NULL);

  return CpuIdEax.Bits.PhysicalAddressBits;
}

/**
  Calculates memory region that shouldn't be encrypted

  @param[in] Base      Base address (must be >= Size)
  @param[in] Size      Size value (must be power of 2)
  @param[out] BaseReg  Contains value to be written to Exclude Base Reg
  @param[out] MaskReg  Contains value to be written to Exclude Mask Reg

  @retval none
**/
VOID
CalculateTmeExcludeBaseMask (
  IN   UINT64                        Base,
  IN   UINT64                        Size,
  OUT  MSR_TME_EXCLUDE_BASE_REGISTER *BaseReg,
  OUT  MSR_TME_EXCLUDE_MASK_REGISTER *MaskReg
  )
{
  UINT64 BitMask = 0;
  UINT32 MaxPhyAddrBits = 0;

  // Set upper 63:MAXPHYS bits to 0
  // MAXPHYS includes keyID bits
  MaxPhyAddrBits = GetMaxPhyAddrBits ();
  BitMask = (LShiftU64 (1, MaxPhyAddrBits) - 1) & ~(0xFFF);
  BaseReg->Uint64 = Base & BitMask;
  Size = ~(Size - 1);
  MaskReg->Uint64 = Size & BitMask;
  MaskReg->Bits.Enable = 1;
}

/**
  Detects Multi-Key Total Memory Encryption capability in TME IP.

  @retval TRUE  - Supported
  @retval FALSE - Not Supported
**/
BOOLEAN
IsTmeCapabilityPresent (
  VOID
  )
{
  ///
  /// Detect TME hardware IP before accessing TME registers
  ///
  if (IsTmeSupported () == FALSE) {
    return FALSE;
  }

  ///
  /// Check if AES-XTS-256 encryption is supported
  ///
  if (MsrIsAesXts256Supported () == FALSE)
  {
    DEBUG ((DEBUG_INFO, "MK-TME: AES-XTS-256 encryption is not supported.\n"));
    return FALSE;
  }

  ///
  /// Read Max supported Keys
  ///
  if ((MsrGetMkTmeMaxKeyidBits () == 0) || (MsrGetMkTmeMaxKey () == 0)) {
    DEBUG ((EFI_D_INFO, "MK-TME: Multiple Keys are not supported.\n"));
    return FALSE;
  }

  return TRUE;
}

/**
  Reports if TME is enabled or not

  @retval TRUE             If TME is enabled
  @retval FALSE            If TME is not enabled
**/
BOOLEAN
IsTmeEnabled (
  VOID
  )
{
  return MsrIsTmeEnabled ();
}

/**
  Reports if Lock bit in TME Activate MSR locked or not

  @retval TRUE             If TME Activate MSR is locked
  @retval FALSE            If TME Activate MSR is not locked
**/
BOOLEAN
IsTmeActivateLockSet (
  VOID
  )
{
  return MsrIsTmeActivateLockSet ();
}

/**
  Set the TME Activate MSR to the specified input parameter

  @param[in] TmeConfig     TME Activate config

  @retval TRUE             If TME Activate MSR is locked
  @retval FALSE            If TME Activate MSR is not locked
**/
BOOLEAN
SetTmeActivateMsr (
  IN TME_CONFIG TmeConfig
  )
{
  MSR_TME_ACTIVATE_REGISTER          TmeValidate;
  EFI_STATUS                         Status;
  UINT8                              Index;

  ///
  /// Configure MSR_TME_ACTIVATE (982H) with TME Enable, Key Select, Save Key, and TME Policy
  /// Lock bit will be set upon successful WRMSR for MSR_TME_ACTIVATE.
  ///   - First SMI will also set the Lock
  ///   - This will also lock MSR_TME_EXCLUDE_MASK and MSR_TME_EXCLUDE_BASE
  ///
  DEBUG ((DEBUG_INFO, "MK-TME: Initialize MSR_TME_ACTIVATE\n"));
  DEBUG ((DEBUG_INFO, " TmeEnable         = 0x%X\n", TmeConfig.TmeEnable));
  DEBUG ((DEBUG_INFO, " KeySelect         = 0x%X\n", TmeConfig.KeySelect));
  DEBUG ((DEBUG_INFO, " SaveKeyForStandby = 0x%X\n", TmeConfig.SaveKeyForStandby));
  DEBUG ((DEBUG_INFO, " TmePolicy         = 0x%X\n", TmeConfig.TmePolicy));
  DEBUG ((DEBUG_INFO, " MkTmeKeyIdBits    = 0x%X\n", TmeConfig.MkTmeKeyIdBits));
  DEBUG ((DEBUG_INFO, " MkTmeCryptoAlgs   = 0x%X\n", TmeConfig.MkTmeCryptoAlgs));

  // TME activate retry limit is 10
  for (Index = 0; Index < TME_ACTIVATE_RETRY_LIMIT; Index++) {
    MsrConfigureTmeActivate ((UINT8) TmeConfig.TmeEnable,
                             (UINT8) TmeConfig.KeySelect,
                             (UINT8) TmeConfig.SaveKeyForStandby,
                             (UINT8) TmeConfig.TmePolicy,
                             (UINT8) TmeConfig.MkTmeKeyIdBits,
                             (UINT16) TmeConfig.MkTmeCryptoAlgs
                            );
    if (MsrIsTmeActivateLockSet ()) {
      TmeValidate.Uint64 = AsmReadMsr64 (MSR_TME_ACTIVATE);
      DEBUG ((DEBUG_INFO, "MK-TME: MSR_TME_ACTIVATE was initialized and locked: %016llx\n", TmeValidate));

      ///
      /// Register for MpServices callback to configure
      /// TME Core Activate MSR across all cores
      ///
      Status = PeiServicesNotifyPpi (mMpServicePpiCallbackNotifyList);
      ASSERT_EFI_ERROR (Status);
      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "MK-TME: Unable to configure TME Core Activate MSR.\n"));
      }

      return TRUE;
    }
  }

  DEBUG ((DEBUG_INFO, "MK-TME: Failed to initialize MSR_TME_ACTIVATE: %016llx\n", TmeValidate));

  return FALSE;
}

/**
  Set TME Core Activate MSR.

**/
EFI_STATUS
EFIAPI
ApSafeSetTmeCoreActivateMsr (
  VOID
  )
{
  ///
  /// write zeroes in the TME Core Activate MSR
  ///
  AsmWriteMsr64 (MSR_CORE_MKTME_ACTIVATION, 0);
  return EFI_SUCCESS;
}

/**
  Configure TME Core Activate MSR across all Cores.

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  MpServices2Ppi PPI.

  @retval EFI_STATUS              The return status from sub function

**/
EFI_STATUS
EFIAPI
ConfigureTmeCoreActivateMsr (
  IN  EFI_PEI_SERVICES             **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR    *NotifyDescriptor,
  IN  VOID                         *Ppi
  )
{
  EFI_STATUS                   Status;
  EDKII_PEI_MP_SERVICES2_PPI   *MpServices2Ppi;

  MpServices2Ppi = NULL;

  ///
  /// Locate MpService Ppi
  ///
  Status = PeiServicesLocatePpi (
             &gEdkiiPeiMpServices2PpiGuid,
             0,
             NULL,
             (VOID **) &MpServices2Ppi
           );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status) || (PeiServices == NULL) || (MpServices2Ppi == NULL)) {
    return EFI_NOT_FOUND;
  }

  ApSafeSetTmeCoreActivateMsr ();
  Status = MpServices2Ppi->StartupAllAPs (
             MpServices2Ppi,
             (EFI_AP_PROCEDURE) ApSafeSetTmeCoreActivateMsr,
             FALSE,
             0,
             NULL
             );
  return Status;
}

/**
  Perform Multi-Key Total Memory Encryption initialization.

  @param[in] TmeEnable      - TME policy enable
  @param[in] TmeExcludeBase - Base physical address to be excluded for TME encryption
  @param[in] TmeExcludeSize - Size of range to be excluded from TME encryption

  @retval VOID - No value to return
**/
VOID
TmeInit (
  IN UINT32 TmeEnable,
  IN UINT64 TmeExcludeBase,
  IN UINT64 TmeExcludeSize
  )
{

  MSR_TME_EXCLUDE_MASK_REGISTER      ExcludeMask;
  MSR_TME_EXCLUDE_BASE_REGISTER      ExcludeBase;
  EFI_BOOT_MODE                      BootMode;
  EFI_STATUS                         Status;
  TME_CONFIG                         TmeConfig;
  UINT32                             RegisterVal32;
  BOOLEAN                            WarmReset;
  BOOLEAN                            IsLocked;

  BootMode  = 0;
  Status    = 0;
  WarmReset = FALSE;
  IsLocked  = FALSE;

  DEBUG ((DEBUG_INFO, "Multi-Key Total Memory Encryption (MK-TME) Initialization\n"));

  ///
  /// Detect MK-TME IP is supported in the hardware
  ///
  if (IsTmeSupported () == FALSE) {
    return;
  }

  ///
  /// Detect MK-TME capabilities are supported in the hardware
  ///
  if (IsTmeCapabilityPresent () == FALSE) {
    DEBUG ((DEBUG_INFO, "MK-TME: Skip MK-TME initialization due to not supported in hardware.\n"));
    ///
    /// Lock MSR before returning
    ///
    MsrLockTmeActivate(0);
    return;
  }

  ///
  /// Detect BIOS policy
  ///
  if (TmeEnable == 0) {
    DEBUG ((DEBUG_INFO, "MK-TME: Policy is disabled in BIOS Setup.\n"));
    DEBUG ((DEBUG_INFO, "MK-TME: Skip MK-TME initialization.\n"));

    ///
    /// Ensure Lock bit of MSR_TME_ACTIVATE (982H) is set
    /// Lock bit will not be set if creation of ephemeral number using DRNG action failed.
    ///
    DEBUG ((DEBUG_INFO, "MK-TME: TME was not enabled. Locking MSR_TME_ACTIVATE.\n"));
    MsrLockTmeActivate(0);
    return;
  }

  ZeroMem (&TmeConfig, sizeof (TME_CONFIG));

  ///
  /// Set TME policy
  ///
  TmeConfig.TmePolicy = V_TME_ACTIVATE_TME_POLICY_AES_XTS_256;

  ///
  /// Read Max supported Keys
  ///
  TmeConfig.MkTmeMaxKeyIdBits = (UINT8) MsrGetMkTmeMaxKeyidBits ();
  TmeConfig.MkTmeMaxKey       = (UINT16) MsrGetMkTmeMaxKey ();

  ///
  /// Configure MSR_TME_EXCLUDE_MASK (983H) with TME Mask and Enable bit
  /// Configure MSR_TME_EXCLUDE_BASE (984H) with TME Base
  ///
  if (TmeExcludeSize != 0 && TmeExcludeBase != 0) {
    CalculateTmeExcludeBaseMask(
      TmeExcludeBase,
      TmeExcludeSize,
      &ExcludeBase,
      &ExcludeMask
    );

    AsmWriteMsr64 (MSR_TME_EXCLUDE_MASK, ExcludeMask.Uint64);
    AsmWriteMsr64 (MSR_TME_EXCLUDE_BASE, ExcludeBase.Uint64);
  }

  ExcludeBase.Uint64 = AsmReadMsr64 (MSR_TME_EXCLUDE_BASE);
  DEBUG ((DEBUG_INFO, "TME_EXCLUDE_BASE (0x984)\n"));
  DEBUG ((DEBUG_INFO, " [BIT 31:12] ExcludeBase0: 0x%X\n", ExcludeBase.Bits.Tmebase0));
  DEBUG ((DEBUG_INFO, " [BIT 63:32] ExcludeBase1: 0x%X\n", ExcludeBase.Bits.Tmebase1));

  ExcludeMask.Uint64 = AsmReadMsr64 (MSR_TME_EXCLUDE_MASK);
  DEBUG ((DEBUG_INFO, "TME_EXCLUDE_MASK (0x983)\n"));
  DEBUG ((DEBUG_INFO, " [BIT 11]    Enable:       0x%X\n", ExcludeMask.Bits.Enable));
  DEBUG ((DEBUG_INFO, " [BIT 31:12] ExcludeMask0: 0x%X\n", ExcludeMask.Bits.Tmemask0));
  DEBUG ((DEBUG_INFO, " [BIT 63:32] ExcludeMask1: 0x%X\n", ExcludeMask.Bits.Tmemask1));

  ///
  /// Set TME Key Select and Save Key for Warm Reset, Standby and Flash Update Mode
  /// Key Select
  ///   - Set for Warm Reset, S3 resume flow and flash update flow to restore
  ///     TME keys from PCH
  ///   - Clear for cold boot to create new TME keys
  /// Save Key
  ///   - Clear for S3 resume flow and flash update flow
  ///   - Set for cold boot to save key into storage
  ///
  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

  ///
  /// Detect Warm Reset boot
  ///
  RegisterVal32 = MmioRead32 ((UINTN) (PmcGetPwrmBase () + R_PMC_PWRM_GEN_PMCON_A));
  if ((((RegisterVal32 & B_PMC_PWRM_GEN_PMCON_A_MEM_SR) != 0) &&
      ((RegisterVal32 & B_PMC_PWRM_GEN_PMCON_A_DISB) != 0))) {
    WarmReset = TRUE;
  }

  if ((BootMode == BOOT_ON_S3_RESUME) ||
      (BootMode == BOOT_ON_FLASH_UPDATE) ||
      (WarmReset == TRUE)) {
    TmeConfig.KeySelect = 1;
    TmeConfig.SaveKeyForStandby = 0;
  } else {
    TmeConfig.KeySelect = 0;
    TmeConfig.SaveKeyForStandby = 1;
  }

  ///
  /// Configure KeyId to max supported
  ///
  TmeConfig.MkTmeKeyIdBits  = TmeConfig.MkTmeMaxKeyIdBits;
  TmeConfig.MkTmeCryptoAlgs = 0x4;  // value 0x4 equates to MSR_TME_ACTIVATE (982H) [BIT50] set

  ///
  /// Set TME enable
  /// TME capability and encryption policy is supported
  ///
  TmeConfig.TmeEnable = 1;

  ///
  /// Call to set TME_ACTIVATE_MSR
  ///
  IsLocked = SetTmeActivateMsr(TmeConfig);

  ///
  /// If TME_ACTIVATE_MSR failed to lock and code tried to restore previous key
  ///
  if (IsLocked == FALSE && TmeConfig.KeySelect == 1){
    ///
    /// Attempt to set TME_ACTIVATE_MSR with new key
    ///
    TmeConfig.KeySelect = 0;
    TmeConfig.SaveKeyForStandby = 1;
    IsLocked = SetTmeActivateMsr(TmeConfig);
  }

  if (IsLocked == FALSE) {
    /// If previous attempts to set TME_ACTIVATE failed, lock TME_ACTIVATE to 0
    MsrLockTmeActivate(0);
  }

  return;
}