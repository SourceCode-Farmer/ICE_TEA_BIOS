/** @file
 Integrated Thunderbolt MailBox ACPI methods

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <TbtMailBoxCmdDefinition.h>

Field(DPME, DWordAcc, NoLock, Preserve)
{
  Offset(R_HOST_DMA_ITBT_NVM_FW_REVISION), // 0xC8
  ,     30,
  IF30,  1, // ITBT FW Version Bit30
  INFR,  1, // ITBT NVM FW Ready
  Offset(FixedPcdGet8 (PcdITbtToPcieRegister)), // 0xEC
  TB2P, 32, // TBT to PCIe
  P2TB, 32  // PCIe to TBT
}

//
// ITBT MailBox Command Method
// Arg0 : MailBox Cmd ID
//
Method(ITMB, 1, Serialized) {
  Or (Arg0, PCIE2TBT_VLD_B, Local0)
  Store (Local0, P2TB)
}

//
// Wait For Command Completed
// Arg0    TimeOut value (unit is 1 millisecond)
//
Method(WFCC, 1, Serialized) {
  WTBS (Arg0)
  Store (0, P2TB)
  WTBC (Arg0)
}

//
// Wait For Command Set
// Arg0    TimeOut value
// Return : 0 - Timeout, non-zero - Command done bit is set within timeout value
//
Method(WTBS, 1, Serialized) {
  Store (Arg0, Local0)
  While (LGreater (Local0, 0)) {
    //
    // Wait for Bit to Set
    //
    If (And (TB2P, TBT2PCIE_DON_R)) {
      Break
    }
    Decrement (Local0)
    Sleep (1)
  }
  Return (Local0)
}

//
// Wait For Command Clear
// Arg0    TimeOut value
//
Method(WTBC, 1, Serialized) {
  Store (Arg0, Local0)
  While (LGreater (Local0, 0)) {
    //
    // Wait for Bit to Clear
    //
    If (LNotEqual (And (TB2P, TBT2PCIE_DON_R), TBT2PCIE_DON_R)) {
      Break
    }
    Decrement (Local0)
    Sleep (1)
  }
}

//
// TCSS ITBT CONNECT_TOPOLOGY MailBox Command Method
//
Method(CNTP, 0, Serialized) {
  Store (0, Local0)
  //
  // Set Force Power if it is not set.
  //
  If (LEqual(DFPE, 0)) {
    Store (0x22, DMAD)
    Store (1, DFPE)
    //
    // Poll the ITBT NVM FW Ready bit with timeout(default is 500ms) before send the TBT MailBox command
    //
    While (LAnd(LEqual (INFR, 0), LLess(Local0, ITFP))) {
      Sleep (1)
      Increment (Local0)
    }
  }
  If (LNotEqual(Local0, ITFP)) {
    ITMB (PCIE2TBT_CONNECT_TOPOLOGY_COMMAND)
  }
}

//
// TCSS ITBT FW Ready BIT Check Method
//
Method (IFWR, 0, Serialized) {
  Return (INFR)
}

//
// TCSS ITBT FW Version BIT30 SET
//
Method (IV30, 0, Serialized) {
  Return (IF30)
}

//
// Name: CMMB
// Description: Send set CM mode mailbox command
// Input: Arg0 - CM mode value (0: FW CM, 1: SW CM)
// Return: 0 = Success, 0xFF = Failure
// Local0: Wait for command done status. 0 = Timeout, 1 = Command done.
// Local1: Set CM mode mail box command status.
//
Method (CMMB, 1, Serialized)
{
  //
  // From TBT FW's view, two modes are provided for CM implementation.
  // - Firmware CM mode  : TBT FW implements CM functions.
  // - Pass Through mode : TBT FW provides USB4 host interface for CM implementation.
  // To implement SW CM, TBT FW need to be switched to Pass Through mode.
  //
  If (LEqual (Arg0, 0))
  {
    //
    // Set FW CM mode and connect topology
    //
    ITMB (PCIE2TBT_FIRMWARE_CM_MODE)
  }
  ElseIf (LEqual(Arg0, 1))
  {
    //
    // Set Pass Through mode for SW CM implementation
    //
    ITMB (PCIE2TBT_PASS_THROUGH_MODE)
  }

  //
  // Wait for set CM mode command done.
  //
  Store (WTBS (SET_CM_TIMEOUT_IN_MS), Local0)
  If (LNotEqual (Local0, 0))
  {
    Store (0, Local1)
  }
  Else{
    ADBG ("Set CM mode command timeout")
    Store (0xFF, Local1)
  }
  Store (0, P2TB)

  Return (Local1)
}

//
// Name: CHCM
// Description: Change CM mode
// Input: Arg0 - CM mode value (0: FW CM, 1: SW CM)
// Return: 0 = Success. 0xFF = Failure
// Local0: Index for polling set force power
// Local1: Change CM mode status
// Local2: Wait for command done status. 0 = Timeout, non-zero = Command done.
//
Method (CHCM, 1, Serialized)
{
  ADBG (Concatenate ("iTBT CHCM - Switch CM mode to ", Arg0))
  If (LGreater (Arg0, 1))
  {
    ADBG ("Invalid CM mode value")
    Return (0xFF)
  }
  Store (0, Local0)
  //
  // Set Force Power if it is not set.
  //
  If (LEqual (DFPE, 0)) {
    ADBG ("Set Force Power")
    Store (0x22, DMAD)
    Store (1, DFPE)
    //
    // Poll the ITBT NVM FW Ready bit with timeout(default is 500ms) before send the TBT MailBox command
    //
    While (LAnd (LEqual (INFR, 0), LLess (Local0, ITFP))) {
      Sleep (1)
      Increment (Local0)
    }
  } Else {
    ADBG ("Force Power has been set")
  }

  If (LNotEqual (Local0, ITFP)) {
    //
    // Send CM mode mailbox command
    //
    Store (CMMB (Arg0), Local1)

    //
    // Send Connect Topology command if CM mode is set to FW CM successfully
    //
    If (LAnd (LEqual (Arg0, 0), LEqual (Local1, 0)))
    {
      //
      // Send Connect Topology command
      //
      ADBG ("Send Connect Topology command")
      ITMB (PCIE2TBT_CONNECT_TOPOLOGY_COMMAND)
      //
      // Wait for command done bit set
      //
      Store (WTBS (ITCT), Local2)
      If (LNotEqual (Local2, 0))
      {
        Store (0, Local1)
      }
      Else{
        ADBG ("Connect topology command timeout!")
        Store (0xFF, Local1)
      }
      Store (0, P2TB)
    }
  }
  Else {
    ADBG ("Force Power Timeout occurred")
    Store (0xFF, Local1)
  }

  ADBG (Concatenate ("iTBT change CM mode status = ", Local1))
  Return (Local1)
}