/**@file
  PEI ITBT Init library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/PeiITbtInitLib.h>
#include <PeiITbtConfig.h>
#include <Library/CpuRegbarAccessLib.h>
#include <Library/PcdLib.h>
#include <Library/RngLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Register/HostDmaRegs.h>

TCSS_DATA_HOB                   *mTcssHob = NULL;

/**
  The Task Table for ITBT
**/
GLOBAL_REMOVE_IF_UNREFERENCED TBT_CALL_TABLE_ENTRY  ITbtCallTable[] = {
  ///
  /// The functions are executed in the following order, as the policy flag dictates.
  /// TbtTask, TbtBootModeFlag, debug_string
  ///
  ///
  /// TO DO: Temporarily blocking the calls to ITbtSetForcePower and ITbtSetCmMode until simics env is fixed
  ///
  {ITbtSetForcePower,         TBT_ALL,               "ITbtSetForcePower"},
  {ITbtPcieTunnelingForUsb4,  TBT_ALL,               "ITbtPcieTunnelingForUsb4"},
  {ITbtSetDmaUuid,            TBT_ALL,               "ITbtDmaUuidUpdate"},
  {ITbtSetDmaSsidSvid,        TBT_ALL,               "ITbtDmaSsidSvidUpdate"},
  {ITbtClearVgaRegisters,     TBT_ALL,               "ITbtClearVgaRegisters"},
  {ITbtSetDmaLtr,             TBT_ALL,               "ITbtSetDmaLtr"},
  {ITbtSetCmMode,             TBT_ALL,               "ITbtSetCmMode"},
  {ITbtUnsetForcePower,       TBT_ALL,               "ITbtUnsetForcePower"},
  {PassITbtPolicyToHob,       (TBT_S4 | TBT_NORMAL), "Pass_ITbt_Policy_To_Hob"},
  {NULL,                      TBT_NULL,              "END_OF_TASK"}
};

/**
  Return the specific ITBT DMA controller is enabled or not

  @param[in]  ControllerIndex  The index of the controller

  @retval  TRUE            The specific ITBT DMA controller is enabled
  @retval  FALSE           The specific ITBT DMA controller is disabled
**/
BOOLEAN
IsITbtDmaControllerEnable (
  IN  UINT8            ControllerIndex
  )
{
  if (ControllerIndex > MAX_HOST_ITBT_DMA_NUMBER) {
    return FALSE;
  }

  if (mTcssHob == NULL) {
    mTcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
    if (mTcssHob == NULL) {
      DEBUG ((DEBUG_ERROR, "TcssHob is NULL\n"));
      if (!IsSimicsEnvironment ()) {
        ASSERT (FALSE);
      }
      return FALSE;
    }
  }

  if (mTcssHob->TcssData.ItbtDmaEn[ControllerIndex] == FALSE) {
    return FALSE;
  }

  return TRUE;
}

/**
  Program iTBT FP Register in order to bring iTBT micro controller up by applying Force Power.

  @param[in]  VOID                PeiTbtConfig

  @retval  EFI_SUCCESS            The function completes successfully
  @retval  EFI_UNSUPPORTED        ITBT is not supported.
**/
EFI_STATUS
EFIAPI
ITbtSetForcePower (
  IN  VOID  *PeiTbtConfig
  )
{
  EFI_STATUS                      Status;
  UINT8                           Index;
  UINTN                           TimeoutIndex;
  UINTN                           ITbtDmaSegment;
  UINTN                           ITbtDmaBus;
  UINTN                           ITbtDmaDev;
  UINTN                           ITbtDmaFunc;
  UINT64                          BaseAddress;
  UINTN                           ForcePowerOnTimeoutInMs;
  UINT32                          RegVal;
  PEI_ITBT_CONFIG                 *PeiITbtConfig;

  PeiITbtConfig = PeiTbtConfig;
  ForcePowerOnTimeoutInMs = (UINTN)PeiITbtConfig->ITbtGenericConfig.ITbtForcePowerOnTimeoutInMs;
  RegVal = 0;
  Status = EFI_SUCCESS;

  DEBUG ((DEBUG_INFO, "[TBT] SetITbtForcePowerEnable call Inside\n"));

  for (Index = 0; Index < MAX_HOST_ITBT_DMA_NUMBER; Index++) {
    if (IsITbtDmaControllerEnable (Index) == FALSE) {
      DEBUG ((DEBUG_INFO, "DMA%d is Disable\n", Index));
      continue;
    }

    Status = GetITbtDmaDevFun (Index, &ITbtDmaSegment, &ITbtDmaBus, &ITbtDmaDev, &ITbtDmaFunc);
    if (Status == EFI_SUCCESS) {
      BaseAddress = PCI_SEGMENT_LIB_ADDRESS ((UINT32)ITbtDmaSegment, (UINT32)ITbtDmaBus, (UINT32)ITbtDmaDev, (UINT32)ITbtDmaFunc, 0);
      PciSegmentAndThenOr32 (
        BaseAddress + R_HOST_DMA_RTD3_FORCEPOWER,
        (UINT32) ~(B_HOST_DMA_DMA_ACTIVE_DELAY_MASK),
        B_HOST_DMA_FORCEPOWER_EN | (V_HOST_DMA_DMA_ACTIVE_DELAY_VALUE << N_HOST_DMA_DMA_ACTIVE_DELAY_OFFSET));

      //
      //  Polling NVM FW version
      //
      Status = EFI_TIMEOUT;
      for (TimeoutIndex = 0; TimeoutIndex < ForcePowerOnTimeoutInMs; TimeoutIndex++) {
        RegVal = PciSegmentRead32 (BaseAddress + R_HOST_DMA_ITBT_NVM_FW_REVISION);
        if ((RegVal & BIT31) == BIT31) {
          Status = EFI_SUCCESS;
          DEBUG ((DEBUG_INFO, "Controller %d (%d:%d:%d:%d) is ready\n", Index, ITbtDmaSegment, ITbtDmaBus, ITbtDmaDev, ITbtDmaFunc));
          break;
        }
        MicroSecondDelay (1000);
      }

      if (Status == EFI_TIMEOUT) {
        DEBUG ((DEBUG_ERROR, "Controller %d (%d:%d:%d:%d) is not ready\n", Index, ITbtDmaSegment, ITbtDmaBus, ITbtDmaDev, ITbtDmaFunc));
        if (!IsSimicsEnvironment ()) {
          ASSERT_EFI_ERROR (Status);
        }
      }
    } else {
      Status = EFI_NOT_FOUND;
      ASSERT_EFI_ERROR (Status);
    }
  }

  DEBUG ((DEBUG_INFO, "[TBT] SetITbtForcePowerEnable call Return\n"));
  return Status;
}

/**
  Send iTBT Connect Topology Command and CM mode command for Integrated Thunderbolt.

  @param[in]  VOID                PeiTbtConfig

  @retval  EFI_SUCCESS            The function completes successfully
  @retval  EFI_DEVICE_ERROR       Connect Topology / CM mode command not executed successfully.
**/
EFI_STATUS
EFIAPI
ITbtSetCmMode (
  IN  VOID  *PeiTbtConfig
  )
{
  EFI_STATUS                      Status;
  UINT8                           Index;
  UINT8                           Usb4CmMode;
  UINT8                           CommandValue;
  UINTN                           ITbtDmaSegment;
  UINTN                           ITbtDmaBus;
  UINTN                           ITbtDmaDev;
  UINTN                           ITbtDmaFunc;
  UINTN                           ConnectTopologyTimeoutInMs;
  BOOLEAN                         ReturnFlag;
  PEI_ITBT_CONFIG                 *PeiITbtConfig;

  PeiITbtConfig = PeiTbtConfig;
  ConnectTopologyTimeoutInMs = (UINTN)PeiITbtConfig->ITbtGenericConfig.ITbtConnectTopologyTimeoutInMs;
  Usb4CmMode = PeiITbtConfig->ITbtGenericConfig.Usb4CmMode;
  Status = EFI_SUCCESS;

  DEBUG ((DEBUG_INFO, "[TBT] ITbtSetCmMode call Inside\n"));

  if (Usb4CmMode == USB4_CM_MODE_FW_CM) {
    CommandValue = PCIE2TBT_FIRMWARE_CM_MODE;
  } else {
    CommandValue = PCIE2TBT_PASS_THROUGH_MODE;
  }
  DEBUG ((DEBUG_INFO, "[TBT] ITbtSetCmMode CommandValue = 0x%x\n", CommandValue));

  //
  // Each Controller will have 2 Root Ports + HIA
  // Based on the root ports enabled, execute Mail Box Communications on corresponding HIA
  //
  for (Index = 0; Index < MAX_HOST_ITBT_DMA_NUMBER; Index++) {
    if (IsITbtDmaControllerEnable (Index) == FALSE) {
      DEBUG ((DEBUG_INFO, "DMA%d is Disable\n", Index));
      continue;
    }

    Status = GetITbtDmaDevFun (Index, &ITbtDmaSegment, &ITbtDmaBus, &ITbtDmaDev, &ITbtDmaFunc);
    ASSERT_EFI_ERROR (Status);
    if (Status == EFI_SUCCESS) {
      //
      // send FW / SW CM mode command
      //
      ReturnFlag = TbtSetPcie2TbtCommand (CommandValue, FixedPcdGet8 (PcdITbtToPcieRegister), FixedPcdGet8 (PcdPcieToITbtRegister), 0, (UINT8)ITbtDmaDev, (UINT8)ITbtDmaFunc, SET_CM_TIMEOUT_IN_MS);
      if (ReturnFlag != TRUE) {
        DEBUG ((DEBUG_ERROR, "Send FW / SW CM mode command, Return with Error Status= EFI_DEVICE_ERROR. DMA Index = %d\n", Index));
        return EFI_DEVICE_ERROR;
      }

      if (Usb4CmMode != USB4_CM_MODE_FW_CM) {
        DEBUG ((DEBUG_INFO, "SW CM mode, skip send CONNECT_TOPOLOGY command. DMA Index = %d\n", Index));
        continue;
      }

      //
      // 1. BIOS send "CONNECT_TOPOLOGY" command to CM with 5 second timeout in Post Mem. CONNECT_TOPOLOGY command is targeted to 0x1F.
      // 2. BIOS dessert iTBT FP bit (0xFC) on each enabled iTBT instance (controller)
      //
      ReturnFlag = TbtSetPcie2TbtCommand (PCIE2TBT_CONNECT_TOPOLOGY_COMMAND, FixedPcdGet8 (PcdITbtToPcieRegister), FixedPcdGet8 (PcdPcieToITbtRegister), 0, (UINT8)ITbtDmaDev, (UINT8)ITbtDmaFunc, ConnectTopologyTimeoutInMs);
      if (ReturnFlag != TRUE) {
        DEBUG ((DEBUG_ERROR, "Send CONNECT_TOPOLOGY command, Return with Error Status= EFI_DEVICE_ERROR. DMA Index = %d\n", Index));
        return EFI_DEVICE_ERROR;
      }
    }
  }

  DEBUG ((DEBUG_INFO, "[TBT] ITbtSetCmMode call Return\n"));
  return Status;
}

/**
  Clear VGA Registers for Integrated Thunderbolt.

  @param[in]  VOID                PeiTbtConfig

  @retval  EFI_SUCCESS            The function completes successfully
  @retval  EFI_UNSUPPORTED        ITBT is not supported.
**/
EFI_STATUS
EFIAPI
ITbtClearVgaRegisters (
  IN  VOID  *PeiTbtConfig
  )
{
  EFI_STATUS                      Status;
  UINT8                           Index;
  UINTN                           RpSegment;
  UINTN                           RpBus;
  UINTN                           RpDev;
  UINTN                           RpFunc;
  PEI_ITBT_CONFIG                 *PeiITbtConfig;

  Status = EFI_SUCCESS;
  PeiITbtConfig = PeiTbtConfig;

  DEBUG ((DEBUG_INFO, "[TBT] ITbtClearVgaRegisters call Inside\n"));

  for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
    if (PeiITbtConfig->ITbtRootPortConfig[Index].ITbtPcieRootPortEn == 1) {
      Status = GetItbtPcieRpInfo ((UINTN)Index, &RpSegment, &RpBus, &RpDev, &RpFunc);
      ASSERT_EFI_ERROR (Status);

      //
      // VGA Enable and VGA 16-bit decode registers of Bridge control register of Root port where
      // Host router resides should be cleaned
      //
      TbtClearVgaRegisters (RpSegment, RpBus, RpDev, RpFunc);
    }
  }

  DEBUG ((DEBUG_INFO, "[TBT] ITbtClearVgaRegisters call Return\n"));
  return Status;
}

/**
  Program LTR Registers for Integrated Thunderbolt.

  @param[in]  VOID                PeiTbtConfig

  @retval  EFI_SUCCESS            The function completes successfully
  @retval  EFI_UNSUPPORTED        ITBT is not supported.
**/
EFI_STATUS
EFIAPI
ITbtSetDmaLtr (
  IN  VOID  *PeiTbtConfig
  )
{
  UINT8                           Index;
  PEI_ITBT_CONFIG                 *PeiITbtConfig;
  UINT16                          ITbtDmaLtrData;

  DEBUG ((DEBUG_INFO, "[TBT] ITbtSetDmaLtr call Inside\n"));
  PeiITbtConfig = PeiTbtConfig;

  if (PeiITbtConfig == NULL) {
    DEBUG ((DEBUG_INFO, "PeiITbtConfig == NULL\n"));
    return EFI_INVALID_PARAMETER;
  }

  //
  // Program ITBT LTR to maximum snoop/non-snoop latency values
  //
  for (Index = 0; Index < MAX_HOST_ITBT_DMA_NUMBER; Index++) {
    if (IsITbtDmaControllerEnable (Index) == FALSE) {
      DEBUG ((DEBUG_INFO, "DMA%d is Disable\n", Index));
      continue;
    }

    ITbtDmaLtrData = PeiITbtConfig->ITbtDmaLtr[Index] & ~BIT15;
    if (ITbtDmaLtrData != 0) {
      ITbtDmaLtrData |= BIT15;
    }
    PciSegmentWrite16 (
      PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM,HOST_DMA_BUS_NUM, HOST_DMA_DEV_NUM, HOST_DMA0_FUN_NUM + Index, R_HOST_DMA_CFG_LTR_CAP),
      ITbtDmaLtrData
    );
  }

  DEBUG ((DEBUG_INFO, "[TBT] ITbtSetDmaLtr call Return\n"));
  return EFI_SUCCESS;
}

/**
  This function Update UUID number to ITBT DMA Host controller.

  @param[in]  VOID                PeiTbtConfig

  @retval  EFI_SUCCESS            The function completes successfully
  @retval  EFI_INVALID_PARAMETER  PeiITbtConfig is invalide.
**/
EFI_STATUS
EFIAPI
ITbtSetDmaUuid (
  IN  VOID  *PeiTbtConfig
  )
{
  UINT8           Index;
  UINT64          RandomNum;
  UINT64          SetUuid;
  UINT16          VendorId;
  PEI_ITBT_CONFIG *PeiITbtConfig;

  DEBUG ((DEBUG_INFO, "[TBT] ITbtSetDmaUuid call Inside\n"));
  PeiITbtConfig = PeiTbtConfig;
  VendorId = 0x8087;
  SetUuid = 0;
  RandomNum = 0;

  if (PeiITbtConfig == NULL) {
    DEBUG ((DEBUG_INFO, "PeiITbtConfig == NULL\n"));
    return EFI_INVALID_PARAMETER;
  }

  for (Index = 0; Index < MAX_HOST_ITBT_DMA_NUMBER; Index++) {

    if (IsITbtDmaControllerEnable (Index) == FALSE) {
      DEBUG ((DEBUG_INFO, "DMA%d is Disable\n", Index));
      continue;
    }

    if (GetRandomNumber64 (&RandomNum) != FALSE) {
      SetUuid = (UINT64) (((UINT64) RandomNum >> 16) | (UINT64) VendorId << 48); // Program VendorID
      SetUuid = (UINT64) (SetUuid & 0xFFFFFFFFFFFFFFF0);
      SetUuid = (UINT64) (SetUuid | (UINT64) Index); // set instance

      //
      // UUID[31:0]
      //
      PciSegmentWrite32 (
        PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, HOST_DMA_BUS_NUM, HOST_DMA_DEV_NUM, HOST_DMA0_FUN_NUM + Index, R_HOST_DMA_UUID_LOW),
        (UINT32) SetUuid
        );

      //
      // UUID[63:32]
      //
      PciSegmentWrite32 (
        PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, HOST_DMA_BUS_NUM, HOST_DMA_DEV_NUM, HOST_DMA0_FUN_NUM + Index, R_HOST_DMA_UUID_HIGH),
        ((UINT32) (SetUuid >> 32))
        );
    } else {
      //
      // write UUID[31:0] to 0
      //
      PciSegmentWrite32 (
        PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, HOST_DMA_BUS_NUM, HOST_DMA_DEV_NUM, HOST_DMA0_FUN_NUM + Index, R_HOST_DMA_UUID_LOW),
        (UINT32) SetUuid
        );

      //
      // write UUID[63:32] to 0
      //
      PciSegmentWrite32 (
        PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, HOST_DMA_BUS_NUM, HOST_DMA_DEV_NUM, HOST_DMA0_FUN_NUM + Index, R_HOST_DMA_UUID_HIGH),
        ((UINT32) (SetUuid >> 32))
        );
    }
  }

  DEBUG ((DEBUG_INFO, "[TBT] ITbtSetDmaUuid call Return\n"));
  return EFI_SUCCESS;
}

/**
  Override default SVID and SSID

  @param[in]      PciBusNum                 Pci bus number
  @param[in]      PciDevNum                 Pci device number
  @param[in]      PciFuncNum                Pci function number
  @param[in,out]  Svid                      Svid value
  @param[in,out]  Ssid                      Ssid value

  @retval  EFI_SUCCESS            get SSID & SVID successfully
  @retval  EFI_UNSUPPORTED        skip program or get fail SSID & SVID.
**/
EFI_STATUS
EFIAPI
OverrideDmaSvidSsid (
  UINT32                 PciBusNum,
  UINT32                 PciDevNum,
  UINT32                 PciFuncNum,
  UINT16                 *Svid,
  UINT16                 *Ssid
  )
{
  UINT16                        CommonSvid;
  UINT16                        CommonSsid;
  UINT16                        Index;
  SI_CONFIG                     *SiConfig;
  EFI_STATUS                    Status;
  SVID_SID_INIT_ENTRY           *SsidTablePtr;
  UINT16                        NumberOfSsidTableEntry;
  SI_POLICY_PPI                 *SiPolicyPpi;

  CommonSvid = DEFAULT_SSVID;
  CommonSsid = DEFAULT_SSDID;

  ///
  /// Get policy settings through the SiSaPolicyPpi
  ///
  Status = PeiServicesLocatePpi (
             &gSiPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPolicyPpi
             );
  if ((Status != EFI_SUCCESS) || (SiPolicyPpi == NULL)) {
    return EFI_UNSUPPORTED;
  }

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gSiConfigGuid, (VOID *) &SiConfig);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "OverrideDmaSvidSsid, get SiConfig fail\n"));
    return EFI_UNSUPPORTED;
  }

  if (SiConfig->SkipSsidProgramming) {
    //
    // Skip all SSID programming
    //
    DEBUG ((DEBUG_INFO, "Skip all SSID programming\n"));
    return EFI_UNSUPPORTED;
  }

  if (SiConfig->CustomizedSvid != 0) {
    CommonSvid = SiConfig->CustomizedSvid;
  }
  if (SiConfig->CustomizedSsid != 0) {
    CommonSsid = SiConfig->CustomizedSsid;
  }

  //
  // Use SiPolicy Table and table counts
  //
  SsidTablePtr = (SVID_SID_INIT_ENTRY*) SiConfig->SsidTablePtr;
  NumberOfSsidTableEntry = SiConfig->NumberOfSsidTableEntry;
  ASSERT (NumberOfSsidTableEntry < SI_MAX_DEVICE_COUNT);

  if (SsidTablePtr != NULL) {
    for (Index = 0; Index < NumberOfSsidTableEntry; Index++) {
      if ((SsidTablePtr[Index].Address.Bits.Bus == PciBusNum) &&
          (SsidTablePtr[Index].Address.Bits.Device == PciDevNum) &&
          (SsidTablePtr[Index].Address.Bits.Function == PciFuncNum)) {
        CommonSvid = SsidTablePtr[Index].SvidSidValue.SubSystemVendorId;
        CommonSsid = SsidTablePtr[Index].SvidSidValue.SubSystemId;
      }
    }
  }

  *Svid = CommonSvid;
  *Ssid = CommonSsid;

  return EFI_SUCCESS;
}

/**
  This function Update SSID & SVID to ITBT DMA Host controller.

  @param[in]  VOID                PeiTbtConfig

  @retval  EFI_SUCCESS            The function completes successfully
  @retval  EFI_INVALID_PARAMETER  PeiITbtConfig is invalide.
**/
EFI_STATUS
EFIAPI
ITbtSetDmaSsidSvid (
  IN  VOID  *PeiTbtConfig
  )
{
  UINT8             Index;
  PEI_ITBT_CONFIG   *PeiITbtConfig;
  UINT16            Svid;
  UINT16            Ssid;
  EFI_STATUS        Status;

  DEBUG ((DEBUG_INFO, "[TBT] ITbtSetDmaSsidSvid call Inside\n"));
  PeiITbtConfig = PeiTbtConfig;

  if (PeiITbtConfig == NULL) {
    DEBUG ((DEBUG_INFO, "PeiITbtConfig == NULL\n"));
    return EFI_INVALID_PARAMETER;
  }

  for (Index = 0; Index < MAX_HOST_ITBT_DMA_NUMBER; Index++) {

    if (IsITbtDmaControllerEnable (Index) == FALSE) {
      DEBUG ((DEBUG_INFO, "DMA%d is Disable\n", Index));
      continue;
    }

    Status = OverrideDmaSvidSsid (HOST_DMA_BUS_NUM, HOST_DMA_DEV_NUM, (UINT32) (HOST_DMA0_FUN_NUM + Index), &Svid, &Ssid);
    if (!EFI_ERROR (Status)) {
      //
      // Program DAM SSID SVID
      //
      TbtSetDmaSsidSvid (PCIE2TBT_SSID_SVID_CFG,
                         FixedPcdGet8 (PcdITbtToPcieRegister),
                         FixedPcdGet8 (PcdPcieToITbtRegister),
                         (UINT8) HOST_DMA_BUS_NUM,
                         (UINT8) HOST_DMA_DEV_NUM,
                         (HOST_DMA0_FUN_NUM + Index),
                         5000,
                         (UINT32) ((Ssid << 16) | Svid)
                         );
    }
  }

  DEBUG ((DEBUG_INFO, "[TBT] ITbtSetDmaSsidSvid call Return\n"));
  return EFI_SUCCESS;
}

/**
  This function pass PEI ITbt Policy to Hob.

  @param[in]  VOID             PeiTbtConfig

  @retval     EFI_SUCCESS      The function completes successfully
  @retval     EFI_UNSUPPORTED  iTBT is not supported.
**/
EFI_STATUS
EFIAPI
PassITbtPolicyToHob (
  IN  VOID  *PeiTbtConfig
)
{
  EFI_STATUS            Status;
  UINT8                 Index;
  PEI_ITBT_CONFIG       *PeiITbtConfig;
  ITBT_INFO_HOB         *ITbtInfoHob;
  UINT8                 EnableDma;
  UINT8                 EnableRp;

  DEBUG ((DEBUG_INFO, "PassITbtPolicyToHob - Start\n"));
  PeiITbtConfig = PeiTbtConfig;

  EnableDma = 0;
  for (Index = 0; Index < MAX_HOST_ITBT_DMA_NUMBER; Index++) {
    if (IsITbtDmaControllerEnable (Index) == TRUE) {
      EnableDma = 1;
      break;
    }
  }

  EnableRp = 0;
  for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
    if (PeiITbtConfig->ITbtRootPortConfig[Index].ITbtPcieRootPortEn == 1) {
      EnableRp = 1;
      break;
    }
  }

  if ((EnableDma == 0) && (EnableRp == 0) ) {
    DEBUG ((DEBUG_INFO, "All DMAs and RPs disabled - ITbtInfoHob is not created!\n"));
    return EFI_UNSUPPORTED;
  }

  ITbtInfoHob = GetFirstGuidHob (&gITbtInfoHobGuid);
  if (ITbtInfoHob == NULL) {
    //
    // Create HOB for ITBT Data
    //
    Status = PeiServicesCreateHob (
               EFI_HOB_TYPE_GUID_EXTENSION,
               sizeof (ITBT_INFO_HOB),
               (VOID **) &ITbtInfoHob
               );
    DEBUG ((DEBUG_INFO, "ITbtInfoHob Created \n"));
    ASSERT_EFI_ERROR (Status);
  }

  if (ITbtInfoHob != NULL) {
    //
    // Initialize the ITBT INFO HOB data.
    //
    ITbtInfoHob->EfiHobGuidType.Name = gITbtInfoHobGuid;

    //
    // Update ITBT Policy
    //
    for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
      ITbtInfoHob->ITbtRootPortConfig[Index].ITbtPcieRootPortEn = PeiITbtConfig->ITbtRootPortConfig[Index].ITbtPcieRootPortEn;
    }
    ITbtInfoHob->ITbtForcePowerOnTimeoutInMs    = PeiITbtConfig->ITbtGenericConfig.ITbtForcePowerOnTimeoutInMs;
    ITbtInfoHob->ITbtConnectTopologyTimeoutInMs = PeiITbtConfig->ITbtGenericConfig.ITbtConnectTopologyTimeoutInMs;
    ITbtInfoHob->Usb4CmMode                     = PeiITbtConfig->ITbtGenericConfig.Usb4CmMode;
  } else {
    return EFI_NOT_FOUND;
  }

  DEBUG ((DEBUG_INFO, "PassITbtPolicyToHob - End\n"));

  return EFI_SUCCESS;
}

/**
  Send iTBT command to ask ITBT controller to enable or disable PCIe tunneling for USB4

  @param[in]  VOID                PeiTbtConfig

  @retval  EFI_SUCCESS            The function completes successfully
  @retval  EFI_DEVICE_ERROR       ITBT Command is not executed successfully.
**/
EFI_STATUS
EFIAPI
ITbtPcieTunnelingForUsb4 (
  IN  VOID  *PeiTbtConfig
  )
{
  EFI_STATUS                      Status;
  UINT8                           Index;
  UINTN                           ITbtDmaSegment;
  UINTN                           ITbtDmaBus;
  UINTN                           ITbtDmaDev;
  UINTN                           ITbtDmaFunc;
  PEI_ITBT_CONFIG                 *PeiITbtConfig;

  PeiITbtConfig = PeiTbtConfig;
  Status = EFI_SUCCESS;

  DEBUG ((DEBUG_INFO, "[TBT] ITbtPcieTunnelingForUsb4 call Inside\n"));

  //
  // Each Controller will have 2 Root Ports + HIA
  // Based on the root ports enabled, execute Mail Box Communications on corresponding HIA
  //
  for (Index = 0; Index < MAX_HOST_ITBT_DMA_NUMBER; Index++) {
    if (IsITbtDmaControllerEnable (Index) == FALSE) {
      DEBUG ((DEBUG_INFO, "DMA%d is Disable\n", Index));
      continue;
    }

    Status = GetITbtDmaDevFun (Index, &ITbtDmaSegment, &ITbtDmaBus, &ITbtDmaDev, &ITbtDmaFunc);
    ASSERT_EFI_ERROR (Status);
    if (Status == EFI_SUCCESS) {
      //
      // 1. BIOS send command to FW CM with 5 second timeout in Post Mem.
      // 2. BIOS dessert iTBT FP bit (0xFC) on each enabled iTBT instance (controller)
      //
      if (PeiITbtConfig->ITbtGenericConfig.ITbtPcieTunnelingForUsb4 == 1) {
        //
        // Enable PCIe Tunnel over USB4 -> set security level to 0
        // It means no security management via TBT3/USB4 Controller
        //
        PciSegmentWrite32 (
          PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, HOST_DMA_BUS_NUM, HOST_DMA_DEV_NUM,HOST_DMA0_FUN_NUM + Index, R_HOST_DMA_ITBT_SECURITY_LEVEL),
          V_HOST_DMA_ITBT_SECURITY_LEVEL0
        );
      } else {
        PciSegmentWrite32 (
          PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, HOST_DMA_BUS_NUM, HOST_DMA_DEV_NUM,HOST_DMA0_FUN_NUM + Index, R_HOST_DMA_ITBT_SECURITY_LEVEL),
          V_HOST_DMA_ITBT_SECURITY_LEVEL5
        );
      }
    }
  }

  DEBUG ((DEBUG_INFO, "[TBT] ITbtPcieTunnelingForUsb4 call Return\n"));
  return Status;
}

/**
  Remove Force Power for iTBT controllers

  @param[in]  VOID                PeiTbtConfig

  @retval  EFI_SUCCESS            The function completes successfully
  @retval  EFI_DEVICE_ERROR       ITBT Command is not executed successfully.
**/
EFI_STATUS
EFIAPI
ITbtUnsetForcePower (
  IN  VOID  *PeiTbtConfig
  )
{
  EFI_STATUS                      Status;
  UINT8                           Index;
  UINTN                           ITbtDmaSegment;
  UINTN                           ITbtDmaBus;
  UINTN                           ITbtDmaDev;
  UINTN                           ITbtDmaFunc;
  UINT64                          BaseAddress;

  Status = EFI_SUCCESS;

  DEBUG ((DEBUG_INFO, "[TBT] ITbtUnsetForcePower call Inside\n"));

  for (Index = 0; Index < MAX_HOST_ITBT_DMA_NUMBER; Index++) {
    if (IsITbtDmaControllerEnable (Index) == FALSE) {
      DEBUG ((DEBUG_INFO, "DMA%d is Disable\n", Index));
      continue;
    }

    Status = GetITbtDmaDevFun (Index, &ITbtDmaSegment, &ITbtDmaBus, &ITbtDmaDev, &ITbtDmaFunc);
    ASSERT_EFI_ERROR (Status);
    if (Status == EFI_SUCCESS) {
      BaseAddress = PCI_SEGMENT_LIB_ADDRESS ((UINT32)ITbtDmaSegment, (UINT32)ITbtDmaBus, (UINT32)ITbtDmaDev, (UINT32)ITbtDmaFunc, 0);
      PciSegmentAnd8 (BaseAddress + R_HOST_DMA_RTD3_FORCEPOWER, (UINT8) (~B_HOST_DMA_FORCEPOWER_EN));
    }
  }

  DEBUG ((DEBUG_INFO, "[TBT] ITbtUnsetForcePower call Return\n"));
  return Status;
}
