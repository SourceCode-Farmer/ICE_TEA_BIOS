/** @file
  Header file for HSIO handle

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#ifndef _HSIO_HANDLE_H_
#define _HSIO_HANDLE_H_

#include <ConfigBlock.h>
#include <HsioConfig.h>
#include <HsioPcieConfig.h>
#include <HsioSataConfig.h>
#include <HsioLane.h>

/**
  Internal HSIO policy options
**/
typedef struct {
  /**
    This is internal switch which allow skip updating Chipset Init table from IP block flow
    EBG is not supporting updating SUS tables.
  **/
  BOOLEAN                    SkipWriteToChipsetInitTable;
  /**
    Specifies the Pcie Pll Spread Spectrum Percentage
    TODO: move this option to HSIO config block
  **/
  UINT8                      PciePllSsc;
} HSIO_PRIVATE_CONFIG;

/**
  HSIO device structure
  Stores all data necessary to initialize HSIO IP block
**/
typedef struct {
  HSIO_LANE                   *Lane;
  USB3_HSIO_CONFIG            *Usb3HsioConfig;
  PCH_HSIO_SATA_PREMEM_CONFIG *SataLanePreMemConfig;
  PCH_HSIO_PCIE_PREMEM_CONFIG *PcieLanePreMemConfig;
  HSIO_PRIVATE_CONFIG         *PrivateConfig;
  PCH_HSIO_CONFIG             *HsioConfig;
} HSIO_HANDLE;

#endif // _HSIO_HANDLE_H_
