/** @file
  PCH Smbus Library

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_SMBUS_LIB_H_
#define _PEI_SMBUS_LIB_H_

#include <SmbusConfig.h>

typedef struct _SMBUS_HANDLE SMBUS_HANDLE;

/**
  Called as a last step in SMBUS controller disable flow.

  @param[in] SmbusHandle  Handle
**/
typedef
VOID
(*SMBUS_DISABLE) (
  IN  SMBUS_HANDLE           *SmbusHandle
  );

/**
Configures GPIO pins for SMBUS Alert

@param[in]  SMBUS_HANDLE     Pointer to SMBUS Handle structure

@retval EFI_STATUS      Status returned by worker function
**/
typedef
EFI_STATUS
(*SMB_ALERT_ENABLE) (
  IN  SMBUS_HANDLE           *SmbusHandle
  );

/**
Configures GPIO pins for SMBUS Interface

@param[in]  SMBUS_HANDLE     Pointer to SMBUS Handle structure

@retval EFI_STATUS      Status returned by worker function
**/
typedef
EFI_STATUS
(*SMB_GPIO_ENABLE) (
  IN  SMBUS_HANDLE           *SmbusHandle
  );

typedef struct {
  SMBUS_DISABLE             SmbusDisable;
  SMB_GPIO_ENABLE           SmbusGpioEnabled;
  SMB_ALERT_ENABLE          SmbusAlertEnable;
} SMBUS_CALLBACKS;
typedef struct {
  UINT8    IntPin;
  UINT8    Irq;
  UINT8    TcoIrq;
  /**
  This option says if SMBUS controller supports
  any Power Management options
  <b>FALSE: NOT Supported</b>, TRUE: Supported
  **/
  BOOLEAN  PowerManagementSupport;
  /**
  This option says if TCO timer build-in SMBUS controller
  supports SMI generation when TCO timer reach 0
  <b>FALSE: NOT Supported</b>, TRUE: Supported
  **/
  BOOLEAN   TcoSmiTimeoutSupport;
  /**
  This option says if the controller supports
  reception of a Host Notify command as a wake event.
  When supported this event is ORed in with the other SMBus wake events
  and is reflected in the SMB_WAK_STS bit of the General Purpose Event 0 Status register.
  <b>FALSE: NOT Supported</b>, TRUE: Supported
  **/
  BOOLEAN   HostNotifyWakeSupport;
} SMBUS_SOC_CONFIG;

//
//  SMBUS Controller Structure
//
typedef struct {
  UINT64                      PciCfgBase;       // PCI configuration base
  UINT8                       Segment;          // PCI Segment
  UINT8                       Bus;              // PCI Bus
  UINT8                       Device;           // PCI Device
  UINT8                       Function;         // PCI Fuction
  UINT32                      SmbusCtrlIndex;   // Smbus Controller Index
  UINT32                      TotalCtrlPortNum; // Total Smbus Ports
  UINT16                      AcpiBase;         // AcpiBase
} SMBUS_CONTROLLER;

struct _SMBUS_HANDLE {
  SMBUS_CONTROLLER         *Controller;         // Describes SMBUS controller location
  SMBUS_SOC_CONFIG         *SocConfig;          // Describes SoC specific config
  PCH_SMBUS_PREMEM_CONFIG  *Config;             // Describes platform config
  SMBUS_CALLBACKS          *Callbacks;          // Set of SoC callbacks
  REGISTER_ACCESS          *SmbusSbAccessMmio;  // P2SB Message type, Smbus controller Private Config Register (PCR) space access
};

/**
  Initialize the Smbus PPI and program the Smbus BAR

  @param[in]  SMBUS_HANDLE        SMBUS_HANDLE instance

  @retval EFI_SUCCESS             The function completes successfully
  @retval EFI_OUT_OF_RESOURCES    Insufficient resources to create database
**/
EFI_STATUS
SmbusPreMemInit (
  IN  SMBUS_HANDLE       *SmbusHandle
  );

/**
  The function performs SMBUS specific programming.

  @param[in] SMBUS_HANDLE       SMBUS_HANDLE instance

**/
VOID
SmbusPostMemConfigure (
  IN  SMBUS_HANDLE           *SmbusHandle
  );
#endif //_PEI_SMBUS_LIB_H_
