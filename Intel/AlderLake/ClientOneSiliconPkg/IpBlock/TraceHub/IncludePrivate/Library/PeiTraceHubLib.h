/** @file
  Header file for CPU and PCH TraceHub Lib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2016 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_TRACEHUB_LIB_H_
#define _PEI_TRACEHUB_LIB_H_

/**
  Create and initialize TraceHub Data HOB

  @param[in] SiPreMemPolicyPpi The SI Pre-Mem Policy PPI instance
**/
VOID
TraceHubDataHobInit (
  IN  SI_PREMEM_POLICY_PPI       *SiPreMemPolicyPpi
  );


/*
  TraceHub configuration at end of PEI
*/
VOID
TraceHubConfigEndOfPei (
  );

/**
  Check if debugger is in use

  @param[in] TraceHubDevice     Specify CPU or PCH trace hub device

  @retval TRUE                  debugger is in use
  @retval FALSE                 debugger is NOT in use
**/
BOOLEAN
IsDebuggerInUse (
  IN TRACE_HUB_DEVICE           TraceHubDevice
  );

/**
  Get TraceHub PCI address

  @param[in] TraceHubDevice     Specified TraceHub device

  @retval UINT64                TraceHub Pci address
**/
UINT64
GetTraceHubPciBase (
  IN   TRACE_HUB_DEVICE     TraceHubDevice
  );

#endif
