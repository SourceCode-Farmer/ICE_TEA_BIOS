/** @file
  This code provides functions for TraceHub.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2016 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Library/BaseLib.h>
#include <Library/PciSegmentLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/SiPolicyLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/MtrrLib.h>
#include <Library/MemoryAllocationLib.h>
#include <PchResetPlatformSpecific.h>
#include <Library/PeiCpuTraceHubLib.h>
#include <Library/PeiTraceHubLib.h>
#include <Library/PsfLib.h>
#include <Library/PsfSocLib.h>
#include <Register/TraceHubRegs.h>
#include <Library/PeiItssLib.h>
#include <Library/SiScheduleResetLib.h>
#include <Library/PchPciBdfLib.h>
#include <TraceHubDataHob.h>
#include <Library/BaseTraceHubInfoFruLib.h>
#include <Library/HobLib.h>
#include <Library/PchSbiAccessLib.h>

UINT32 TraceHubBufferSizeTab[8] = {0, SIZE_1MB, SIZE_8MB, SIZE_64MB, SIZE_128MB, SIZE_256MB, SIZE_512MB, SIZE_1GB};

/**
  Calculate total trace buffer size and adjust it to be power of two to utilize least dynamic MTRR entry.

  @param[in] SiPreMemPolicyPpi The SI Pre-Mem Policy PPI instance

  @retval UINT64                             Total size of trace buffers
**/
STATIC
UINT64
TraceHubCalculateTotalBufferSize (
  IN  SI_PREMEM_POLICY_PPI       *SiPreMemPolicyPpi
  )
{
  EFI_STATUS                   Status;
  UINT64                       TotalTraceBufferSize;
  CPU_TRACE_HUB_PREMEM_CONFIG  *CpuTraceHubPreMemConfig;
  PCH_TRACE_HUB_PREMEM_CONFIG  *PchTraceHubPreMemConfig;

  TotalTraceBufferSize = 0;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gPchTraceHubPreMemConfigGuid, (VOID *) &PchTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuTraceHubPreMemConfigGuid, (VOID *) &CpuTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  if ((CpuTraceHubPreMemConfig->TraceHub.MemReg0Size >= TraceBufferMax) || (CpuTraceHubPreMemConfig->TraceHub.MemReg1Size >= TraceBufferMax) ||
      (PchTraceHubPreMemConfig->TraceHub.MemReg0Size >= TraceBufferMax) || (PchTraceHubPreMemConfig->TraceHub.MemReg1Size >= TraceBufferMax)
      ) {
    DEBUG ((DEBUG_ERROR, "Illegal TraceHub size policy input, should be within the range 0~6, skip allocate trace buffers\n"));
    return 0;
  }

  if (CpuTraceHubPreMemConfig->TraceHub.EnableMode != TraceHubModeDisabled) {
    TotalTraceBufferSize += TraceHubBufferSizeTab[CpuTraceHubPreMemConfig->TraceHub.MemReg0Size] + TraceHubBufferSizeTab[CpuTraceHubPreMemConfig->TraceHub.MemReg1Size];
  }
  if (PchTraceHubPreMemConfig->TraceHub.EnableMode != TraceHubModeDisabled) {
    TotalTraceBufferSize += TraceHubBufferSizeTab[PchTraceHubPreMemConfig->TraceHub.MemReg0Size] + TraceHubBufferSizeTab[PchTraceHubPreMemConfig->TraceHub.MemReg1Size];
  }
  //
  // make the total size to be powoer of 2
  //
  if (TotalTraceBufferSize > GetPowerOfTwo64 (TotalTraceBufferSize)) {
    DEBUG ((DEBUG_INFO, "total TraceHub memory = %x\n is not power of 2", TotalTraceBufferSize));
    TotalTraceBufferSize = 2 * GetPowerOfTwo64 (TotalTraceBufferSize);
  }
  if (TotalTraceBufferSize > 0) {
    DEBUG ((DEBUG_INFO, "required total TraceHub memory = %x\n", TotalTraceBufferSize));
  } else {
    DEBUG ((DEBUG_INFO, "TraceHub memory is not required.\n"));
  }
  return TotalTraceBufferSize;
}

/**
  Create and initialize TraceHub Data HOB

  @param[in] SiPreMemPolicyPpi The SI Pre-Mem Policy PPI instance
**/
VOID
TraceHubDataHobInit (
  IN  SI_PREMEM_POLICY_PPI       *SiPreMemPolicyPpi
  )
{
  EFI_STATUS                     Status;
  CPU_TRACE_HUB_PREMEM_CONFIG    *CpuTraceHubPreMemConfig;
  PCH_TRACE_HUB_PREMEM_CONFIG    *PchTraceHubPreMemConfig;
  TRACEHUB_DATA_HOB              *TraceHubDataHob;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gPchTraceHubPreMemConfigGuid, (VOID *) &PchTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuTraceHubPreMemConfigGuid, (VOID *) &CpuTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  ///
  /// Create HOB for TraceHub Data HOB
  ///
  Status = PeiServicesCreateHob (
             EFI_HOB_TYPE_GUID_EXTENSION,
             sizeof (TRACEHUB_DATA_HOB),
             (VOID **) &TraceHubDataHob
             );
  ASSERT_EFI_ERROR (Status);
  ///
  /// Initialize HOB data
  ///
  TraceHubDataHob->EfiHobGuidType.Name = gTraceHubDataHobGuid;
  DEBUG ((DEBUG_INFO, "TraceHubDataHob->EfiHobGuidType.Name: %g\n", &TraceHubDataHob->EfiHobGuidType.Name));
  ZeroMem (&(TraceHubDataHob->TraceHubMemBase), sizeof (TRACEHUB_DATA_HOB) - sizeof (EFI_HOB_GUID_TYPE));
  TraceHubDataHob->TraceHubMemSize = TraceHubCalculateTotalBufferSize (SiPreMemPolicyPpi);
  TraceHubDataHob->TraceHubMode[CpuTraceHub] = CpuTraceHubPreMemConfig->TraceHub.EnableMode;
  TraceHubDataHob->TraceHubMode[PchTraceHub] = PchTraceHubPreMemConfig->TraceHub.EnableMode;
  DEBUG ((DEBUG_INFO, "TraceHubDataHob @ %X\n", TraceHubDataHob));
  DEBUG ((DEBUG_INFO, "TraceHubDataHobSize - HobHeader: %X\n", sizeof (TRACEHUB_DATA_HOB) - sizeof (EFI_HOB_GUID_TYPE)));
  DEBUG ((DEBUG_INFO, "TraceHubDataHobSize: %X\n", sizeof (TRACEHUB_DATA_HOB)));
}

/**
  Get TraceHub PCI address

  @param[in] TraceHubDevice     Specified TraceHub device

  @retval UINT64                TraceHub Pci address
**/
UINT64
GetTraceHubPciBase (
  IN   TRACE_HUB_DEVICE     TraceHubDevice
  )
{
  if (TraceHubDevice == PchTraceHub) { // PCH TraceHub
    return PchTraceHubPciCfgBase ();
  } else { // CPU TraceHub
    return PCI_SEGMENT_LIB_ADDRESS (
            0,
            0,
            PCI_DEVICE_NUMBER_CPU_TRACE_HUB,
            PCI_FUNCTION_NUMBER_CPU_TRACE_HUB,
            0
            );
  }
}

/**
  Get TraceHub MTB Bar

  @param[in] TraceHubDevice     Specified TraceHub device

  @retval UINT32                TraceHub MTB bar
**/
UINT32
GetTraceHubMtbBar (
  IN   TRACE_HUB_DEVICE     TraceHubDevice
  )
{
  return (PciSegmentRead32 (GetTraceHubPciBase (TraceHubDevice) + R_TRACE_HUB_CFG_CSR_MTB_LBAR) & 0xFFF00000);
}

/**
  Configure PCH tracehub interrup
**/
VOID
ConfigurePchTraceHubInterrupt (
  VOID
  )
{
  SI_POLICY_PPI           *SiPolicy;
  EFI_STATUS              Status;
  UINT8                   InterruptPin;
  UINT64                  TraceHubBaseAddress;

  SiPolicy = NULL;
  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicy);
  ASSERT_EFI_ERROR(Status);

  TraceHubBaseAddress = GetTraceHubPciBase (PchTraceHub);
  if (PciSegmentRead32 (TraceHubBaseAddress) == 0xffffffff) {
    return;
  }

  InterruptPin = ItssGetDevIntPin (SiPolicy, PchTraceHubDevNumber (), PchTraceHubFuncNumber ());
  PciSegmentWrite8 (TraceHubBaseAddress + PCI_INT_PIN_OFFSET, InterruptPin);
}



/**
  Check if debugger is in use

  @param[in] TraceHubDevice    Specified TraceHub device

  @retval TRUE                 debugger is in use
  @retval FALSE                debugger is NOT in use
**/
BOOLEAN
IsDebuggerInUse (
  IN TRACE_HUB_DEVICE          TraceHubDevice
  )
{
  UINT64                       TraceHubPciBase;
  UINT32                       Scrpd0;
  UINT8                        Response;

  Response = 0;
  Scrpd0 = 0;

  TraceHubPciBase = GetTraceHubPciBase (TraceHubDevice);
  if (PciSegmentRead16 (TraceHubPciBase + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_INFO, "%s TraceHub does not exist\n", (TraceHubDevice == CpuTraceHub) ? L"Cpu" : L"Pch"));
    return FALSE;
  }

  switch (TraceHubDevice) {
    case CpuTraceHub:
      PciSegmentWrite8 (TraceHubPciBase + PCI_COMMAND_OFFSET, 0);
      ///
      /// MSE needs to be set until all BARs have been configured otherwise PSF decoding for uninitialized PCI BARs may result in failures
      ///
      PciSegmentWrite32 (TraceHubPciBase + R_TRACE_HUB_CFG_CSR_MTB_LBAR, PcdGet32 (PcdCpuTraceHubMtbBarBase));
      PciSegmentWrite32 (TraceHubPciBase + R_TRACE_HUB_CFG_SW_LBAR, PcdGet32 (PcdCpuTraceHubSwBarBase));
      PciSegmentWrite32 (TraceHubPciBase + R_TRACE_HUB_CFG_RTIT_LBAR, PcdGet32 (PcdCpuTraceHubRtitBarBase));
      PciSegmentOr8 (TraceHubPciBase + PCI_COMMAND_OFFSET, EFI_PCI_COMMAND_MEMORY_SPACE);
      Scrpd0 = MmioRead32 (PcdGet32 (PcdCpuTraceHubMtbBarBase) + R_TRACE_HUB_MEM_CSR_MTB_SCRPD0);
      PciSegmentWrite8 (TraceHubPciBase + PCI_COMMAND_OFFSET, 0);
      break;

    case PchTraceHub:
      //
      // SCRPD0 can be read by SBI for PCH TH, which doesn't need to set all bars.
      //
      PchSbiExecutionEx (
        PID_NPK,
        R_TRACE_HUB_MEM_CSR_MTB_SCRPD0,
        MemoryRead,
        FALSE,
        0x0F,
        0,
        (PchTraceHubDevNumber () << 3) + PchTraceHubFuncNumber (),
        &Scrpd0,
        &Response
        );
        if (Response != SBI_SUCCESSFUL) {
          DEBUG ((DEBUG_ERROR, "SBI Read Pch TraceHub Scrpd0 failed\n"));
          return FALSE;
        }
      break;

    default:
      DEBUG ((DEBUG_ERROR, "Invalid parameter\n"));
      return FALSE;
      break;
  }

  DEBUG ((DEBUG_INFO, "%s TraceHub Scrpd0 = 0x%x\n", ((TraceHubDevice == CpuTraceHub) ? L"Cpu" : L"Pch"), Scrpd0));
  if (Scrpd0 == 0xffffffff) {
    return FALSE;
  }
  if (Scrpd0 & B_TRACE_HUB_MEM_CSR_MTB_SCRPD0_DEBUGGER_IN_USE) {
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
  Configure TraceHub to ACPI mode - hide PCI config space via PSF

  @param[in] CpuTraceHubMode      CPU TraceHub Mode
  @param[in] PchTraceHubMode      PCH TraceHub Mode
**/
STATIC
VOID
ConfigureTraceHubMode (
  TRACE_HUB_ENABLE_MODE       CpuTraceHubMode,
  TRACE_HUB_ENABLE_MODE       PchTraceHubMode
  )
{
  if (IsCpuTraceHubSupport ()) {
    if (CpuTraceHubMode == TraceHubModeHostDebugger) {
      ///
      /// Hide CPU TraceHub Device
      ///
      DEBUG ((DEBUG_INFO, "ConfigureTraceHubMode() hide CPU TraceHub\n"));
      HideCpuTraceHub ();
    }
  }

  if (PchTraceHubMode == TraceHubModeHostDebugger) {
    ///
    /// Hide PCH TraceHub Device
    ///
    DEBUG ((DEBUG_INFO, "ConfigureTraceHubMode() hide PCH TraceHub\n"));
    PsfHideDevice (PsfTraceHubPort ());
  }
}

/**
  Configure TraceHub Msc operational region to corresponding buffer base/size and return next MSC base address

  @param[in] TraceHubDevice         Specified TraceHub device
  @param[in] TraceHubMemBase        TraceHub memory base
  @param[in] *TraceHubConfig        instance of TRACE_HUB_CONFIG

  @retval UINT64                    starting address of next MSC base
**/
STATIC
UINT64
TraceHubConfigMsc (
  IN  TRACE_HUB_DEVICE              TraceHubDevice,
  IN  UINT64                        TraceHubMemBase,
  IN  TRACE_HUB_CONFIG              *TraceHubConfig
  )
{

  UINT32                       TraceHubMtbBar;
  UINT64                       TraceHubPciBase;

  DEBUG ((DEBUG_INFO, "TraceHubConfigMsc\n"));
  DEBUG ((DEBUG_INFO, "TraceHubMemBase = 0x%lx\n", TraceHubMemBase));

  TraceHubMtbBar = GetTraceHubMtbBar (TraceHubDevice);
  //
  // Enable MSE and BME again for the corner case -- booting with trace is activated by tool, MSE and BME are deferred to be set until here
  //
  TraceHubPciBase = GetTraceHubPciBase (TraceHubDevice);
  PciSegmentOr8 (TraceHubPciBase + PCI_COMMAND_OFFSET, EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_BUS_MASTER);
  if (TraceHubConfig->EnableMode != TraceHubModeDisabled) {
    ///
    ///  Program MSC0BAR, MSC0SIZE, MSC1BAR, MAS1SIZE
    ///
    ///  Write the base address and buffer size for Memory Region 0 to the the MSC0BAR and MSC0SIZE registers.
    ///  Write the base address and buffer size for Memory Region 1 to the the MSC1BAR and MSC1SIZE registers.
    ///  Note that BAR and Size are shifted left 12 bits by the hardware to arrive at physical starting address and size.
    ///  BIOS, therefore, should shift-right by 12 bits before writing a value to these registers.
    ///  This also forces the starting address and size to be on 4kB boundaries.
    ///
    if (TraceHubConfig->MemReg0Size != TraceBufferNone) {
      MmioWrite32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC0UBAR, (UINT32)(TraceHubMemBase >> 44));
      MmioWrite32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC0BAR, (UINT32)((TraceHubMemBase >> 12) & 0xFFFFFFFF));
      MmioWrite32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC0SIZE, TraceHubBufferSizeTab[TraceHubConfig->MemReg0Size] >> 12);
      DEBUG ((DEBUG_INFO, "[%x] = 0x%08x\n", TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC0UBAR, MmioRead32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC0UBAR)));
      DEBUG ((DEBUG_INFO, "[%x] = 0x%08x\n", TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC0BAR, MmioRead32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC0BAR)));
      DEBUG ((DEBUG_INFO, "[%x] = 0x%08x\n", TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC0SIZE, MmioRead32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC0SIZE)));
      TraceHubMemBase += TraceHubBufferSizeTab[TraceHubConfig->MemReg0Size];
    }
    if (TraceHubConfig->MemReg1Size != TraceBufferNone) {
      MmioWrite32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC1UBAR, (UINT32)(TraceHubMemBase >> 44));
      MmioWrite32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC1BAR, (UINT32)((TraceHubMemBase >> 12) & 0xFFFFFFFF));
      MmioWrite32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC1SIZE, TraceHubBufferSizeTab[TraceHubConfig->MemReg1Size] >> 12);
      DEBUG ((DEBUG_INFO, "[%x] = 0x%08x\n", TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC1UBAR, MmioRead32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC1UBAR)));
      DEBUG ((DEBUG_INFO, "[%x] = 0x%08x\n", TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC1BAR, MmioRead32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC1BAR)));
      DEBUG ((DEBUG_INFO, "[%x] = 0x%08x\n", TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC1SIZE, MmioRead32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSC1SIZE)));
      TraceHubMemBase += TraceHubBufferSizeTab[TraceHubConfig->MemReg1Size];
    }
    if (IsImrSupport ()) {
      //
      // Set MSU to Root Space 3
      //
      MmioWrite32 (TraceHubMtbBar + R_TRACE_HUB_MEM_MTB_MSUGCTL, V_TRACE_HUB_MEM_MTB_MSUGCTL_MSURS_3);
    }
  }
  return TraceHubMemBase;
}

/*
  TraceHub configuration at end of PEI
*/
VOID
TraceHubConfigEndOfPei (
  )
{
  TRACEHUB_DATA_HOB            *TraceHubDataHob;
  EFI_STATUS                   Status;
  UINT64                       NextMscBase;
  SI_PREMEM_POLICY_PPI         *SiPreMemPolicy;
  CPU_TRACE_HUB_PREMEM_CONFIG  *CpuTraceHubPreMemConfig;
  PCH_TRACE_HUB_PREMEM_CONFIG  *PchTraceHubPreMemConfig;

  DEBUG ((DEBUG_INFO, "TraceHubConfigEndOfPei() Start\n"));
  TraceHubDataHob = NULL;
  NextMscBase = 0;
  //
  // Get Policy settings through the SiPreMemPolicy PPI
  //
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicy);
  ASSERT_EFI_ERROR(Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gCpuTraceHubPreMemConfigGuid, (VOID *) &CpuTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gPchTraceHubPreMemConfigGuid, (VOID *) &PchTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  ConfigurePchTraceHubInterrupt ();

  TraceHubDataHob = (TRACEHUB_DATA_HOB *) GetFirstGuidHob (&gTraceHubDataHobGuid);
  if (TraceHubDataHob != NULL) {
    DEBUG ((DEBUG_INFO, "TraceHub memory base = 0x%lx Size = 0x%lx\n", TraceHubDataHob->TraceHubMemBase, TraceHubDataHob->TraceHubMemSize));
    if ((TraceHubDataHob->TraceHubMemBase > 0) && (TraceHubDataHob->TraceHubMemSize > 0)) {
      //
      // Set TraceHub memory attribute to UC
      //
      Status = MtrrSetMemoryAttribute (TraceHubDataHob->TraceHubMemBase, TraceHubDataHob->TraceHubMemSize, CacheUncacheable);
      if (Status != EFI_SUCCESS) {
        DEBUG ((DEBUG_ERROR, "Set MTRR attribute fail, Status = %r\n", Status));
      }
      //
      // Fill BAR and size to each TraceHub MSC0/1 registers
      //
      NextMscBase = TraceHubDataHob->TraceHubMemBase;
      if (IsCpuTraceHubSupport ()) {
        NextMscBase = TraceHubConfigMsc (CpuTraceHub, NextMscBase, &CpuTraceHubPreMemConfig->TraceHub);
      }
      TraceHubConfigMsc (PchTraceHub, NextMscBase, &PchTraceHubPreMemConfig->TraceHub);
    }
  }
  //
  // Hide TraceHub PCI CFG space for host debugger mode
  //
  ConfigureTraceHubMode (CpuTraceHubPreMemConfig->TraceHub.EnableMode, PchTraceHubPreMemConfig->TraceHub.EnableMode);
  DEBUG ((DEBUG_INFO, "TraceHubConfigEndOfPei() end\n"));
}




