/** @file
  Pei/Dxe/Smm Pch TraceHub Lib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi/UefiBaseType.h>
#include <Library/BaseLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PostCodeLib.h>
#include <Library/PchSbiAccessLib.h>
#include <Library/PsfLib.h>
#include <Library/PsfSocLib.h>
#include <Library/PmcPrivateLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/PeiPchTraceHubLib.h>
#include <TraceHubConfig.h>
#include <IndustryStandard/Pci30.h>
#include <PchReservedResources.h>
#include <Register/UsbRegs.h>
#include <Register/PchRegs.h>
#include <Register/PchPcrRegs.h>
#include <Register/TraceHubRegs.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PeiDciSocLib.h>
#include <Library/DciPrivateLib.h>

VOID
EFIAPI
CpuWriteTraceHubAcpiBaseHelper (
  IN UINT64  TraceHubAcpiBase
  );

/**
  Enable FW BAR MMIO. FW_BAR is not part of PCI standard BAR.
  As such, it requires a separate phantom device for BIOS to configure its MMIO as FW_BAR.
  Anything that goes to this FW_BAR address will be routed by the phantom device to TraceHub.

  Programming steps:
  1. Program FW_BAR to TraceHub PCI Offset 0x70~0x77 as 64-bit base address.
  2. Program FW_BAR to TraceHub phantom device BAR0 and BAR1 as FW_LBAR and FW_UBAR from PSF
  3. Enable MSE of TraceHub phantom device from PSF

  @param[in] AddressHi                   High 32-bits for TraceHub base address.
  @param[in] AddressLo                   Low 32-bits for TraceHub base address.
  @retval EFI_SUCCESS                   Successfully completed.
**/
EFI_STATUS
TraceHubBaseSet (
  UINT32                                AddressHi,
  UINT32                                AddressLo
  )
{
  UINT64                                TraceHubBase;

  //
  // check address valid
  //
  if ((AddressLo & 0x0003FFFF) != 0) {
    DEBUG ((DEBUG_ERROR, "TraceHubBaseSet Error. Invalid Address: %x%x", AddressHi, AddressLo));
    ASSERT (FALSE);
    return EFI_INVALID_PARAMETER;
  }

  TraceHubBase = PchTraceHubPciCfgBase ();

  if (PciSegmentRead16 (TraceHubBase) == 0xFFFF) {
    ASSERT (FALSE);
    return EFI_UNSUPPORTED;
  }
  //
  // Program TraceHub PCI Offset 0x70~0x77 to base address.
  //
  PciSegmentWrite32 (TraceHubBase + R_TRACE_HUB_CFG_FW_LBAR, AddressLo);
  PciSegmentWrite32 (TraceHubBase + R_TRACE_HUB_CFG_FW_UBAR, AddressHi);
  //
  // Program FW_BAR to TraceHub phantom device BAR0 and BAR1 as FW_LBAR and FW_UBAR from PSF
  //
  PsfSetDeviceBarValue (PsfTraceHubAcpiDevPort (), 0, AddressLo);
  PsfSetDeviceBarValue (PsfTraceHubAcpiDevPort (), 1, AddressHi);
  //
  // Enable MSE of TraceHub phantom device from PSF
  //
  PsfEnableDeviceMemSpace (PsfTraceHubAcpiDevPort ());
  //
  // Hide TraceHub phantom Device
  //
  PsfHideDevice (PsfTraceHubAcpiDevPort ());

  return EFI_SUCCESS;
}

/**
  Used to program xHCI DCI MMIO registers via SBI.

  @param[in] Offset          DbC offset
  @param[in] Data            Data

**/
STATIC
VOID
SbiDbcSet (
  UINT16                     Offset,
  UINT32                     Data
  )
{
  UINT8                      Response;

  PchSbiExecutionEx (
    PID_XHCI,
    Offset,
    MemoryWrite,
    FALSE,
    0xf,
    0x5,
    (PchXhciDevNumber () << 3) + PchXhciFuncNumber (),
    &Data,
    &Response
    );
  DEBUG ((DEBUG_INFO, "[0x%x] = 0x%08x\n", Offset, Data));

  if (Response != SBI_SUCCESSFUL) {
    DEBUG ((DEBUG_INFO, "Write [0x%x] Failed!" , Offset));
  }
}

/**
  Read MTB_BAR MMIO offset with SBI, used when MSE is not enabled yet

  @param[in] Offset          offset
  @param[in] Data            Data

**/
STATIC
VOID
SbiMtbRead (
  UINT32                     Offset,
  UINT32                     *Data
  )
{
  UINT8                      Response;

  PchSbiExecutionEx (
    PID_NPK,
    Offset,
    MemoryRead,
    FALSE,
    0x0F,
    0,
    (PchTraceHubDevNumber () << 3) + PchTraceHubFuncNumber (),
    Data,
    &Response
    );
  if (Response != SBI_SUCCESSFUL) {
    DEBUG ((DEBUG_INFO, "Read MTB BAR offset 0x%x Failed!" , Offset));
  }
}

/**
  Write MTB_BAR MMIO offset with SBI, used when MSE is not enabled yet

  @param[in] Offset          offset
  @param[in] Data            Data

**/
STATIC
VOID
SbiMtbWrite (
  UINT32                     Offset,
  UINT32                     Data
  )
{
  UINT8                      Response;

  PchSbiExecutionEx (
    PID_NPK,
    Offset,
    MemoryWrite,
    FALSE,
    0x0F,
    0,
    (PchTraceHubDevNumber () << 3) + PchTraceHubFuncNumber (),
    &Data,
    &Response
    );
  if (Response != SBI_SUCCESSFUL) {
    DEBUG ((DEBUG_INFO, "Write MTB BAR offset 0x%x Failed!" , Offset));
  }
}

/**
  Clears D3HE bit in xHCI PCE register
**/
STATIC
VOID
ClearXhciD3he (
  VOID
  )
{
  PciSegmentAnd16 (PchXhciPciCfgBase () + R_XHCI_CFG_PCE, (UINT16)~B_XHCI_CFG_PCE_D3HE);
}

/**
  Sets D3HE bit in xHCI PCE register
**/
STATIC
VOID
SetXhciD3he (
  VOID
  )
{
  PciSegmentOr16 (PchXhciPciCfgBase () + R_XHCI_CFG_PCE, (UINT16) B_XHCI_CFG_PCE_D3HE);
}

/**
  For xHCI.DbC.Trace does not survive across Sx, that FW agent is responsible to resume tracing back or DbC3 would be suspended.
**/
STATIC
VOID
ConfigureXhciDbcTrace (
  VOID
  )
{
  UINT64                TraceHubBaseAddress;
  UINT32                MtbLBar;
  UINT32                MtbUBar;
  UINT32                Data;

  TraceHubBaseAddress = PchTraceHubPciCfgBase ();

  MtbLBar = PciSegmentRead32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_CSR_MTB_LBAR) & B_TRACE_HUB_CFG_CSR_MTB_RBAL;
  MtbUBar = PciSegmentRead32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_CSR_MTB_UBAR) & B_TRACE_HUB_CFG_CSR_MTB_RBAL;

  //
  // Clearing D3HE prior to all xHCI SB accesses
  //
  ClearXhciD3he ();

  //
  // Set DbC Trace in Payload and Status Base
  //
  SbiDbcSet (R_XHCI_PCR_DCI_DBC_TRACE_IN_PAYLOAD_BP_LOW, MtbLBar + R_TRACE_HUB_MEM_MTB_80000);
  SbiDbcSet (R_XHCI_PCR_DCI_DBC_TRACE_IN_PAYLOAD_BP_HIGH, MtbUBar);

  SbiDbcSet (R_XHCI_PCR_DCI_DBC_TRACE_IN_STATUS_BP_LOW, MtbLBar + R_TRACE_HUB_MEM_MTB_DBCSTSCMD);
  SbiDbcSet (R_XHCI_PCR_DCI_DBC_TRACE_IN_STATUS_BP_HIGH, MtbUBar);

  //
  // Once done with xHCI SB access, set D3HE
  //
  SetXhciD3he ();

  //
  // Set Trace Hub DbC base and offset
  Data = V_PCH_TRACE_HUB_MEM_MTB_DBC_BASE_LO;
  SbiMtbWrite (R_TRACE_HUB_MEM_MTB_DBC_BASE_LO, Data);

  SbiMtbRead (R_TRACE_HUB_MEM_MTB_STREAMCFG2, &Data);
  Data |= (V_TRACE_HUB_MEM_MTB_STREAMCFG2_DBC_OFFSET << N_TRACE_HUB_MEM_MTB_STREAMCFG2_DBC_OFFSET);
  SbiMtbWrite (R_TRACE_HUB_MEM_MTB_STREAMCFG2, Data);
  //
  // Reserve DbC MMIO for Trace Hub - this is done in InstallEfiMemory() in MRC
  //
}


/**
  Return if it need to undo early trace.
  If early trace is activated and SCRPD0[24] is set, assumed early trace is needed. Others assumed does not need.

  @retval TRUE                       Undo early trace
  @retval FALSE                      Early trace is disabled
**/
STATIC
BOOLEAN
IsUndoEarlyTrace (
  VOID
  )
{
  UINT64                  TraceHubBaseAddress;
  UINT32                  Scrpd0;
  UINT8                   Data8;

  TraceHubBaseAddress = PchTraceHubPciCfgBase ();
  Scrpd0 = 0;
  Data8 = 0;

  SbiMtbRead (R_TRACE_HUB_MEM_CSR_MTB_SCRPD0, &Scrpd0);
  Data8 = PciSegmentRead8 (TraceHubBaseAddress + R_TRACE_HUB_CFG_DSC);

  if ((Data8 & B_TRACE_HUB_CFG_DSC_TSACT) && (Data8 & B_TRACE_HUB_CFG_DSC_TRACT)) {
    //
    // Trace is activated
    //
    if (Scrpd0 & B_TRACE_HUB_MEM_CSR_MTB_SCRPD0_DEBUGGER_IN_USE) {
      return FALSE;
    } else {
      return TRUE;
    }
  } else {
    //
    // Trace is not activated, need not undo trace, either.
    //
    return FALSE;
  }
}


/**
  Disable early trace

**/
STATIC
VOID
DisableEarlyTrace (
  VOID
  )
{
  UINT64                  TraceHubBaseAddress;
  UINT32                  Data32;

  TraceHubBaseAddress = PchTraceHubPciCfgBase ();
  Data32 = 0;

  if (!IsDciKeepEarlyTraceSupported ()) {
    //
    // Stop STH and DTF trace storage
    //
    SbiMtbRead (R_TRACE_HUB_MEM_MTB_SCR, &Data32);
    Data32 &= ~(UINT32) (B_TRACE_HUB_MEM_MTB_SCR_STOR_EN_OVR_4 | B_TRACE_HUB_MEM_MTB_SCR_STOR_EN_OVR_5);
    SbiMtbWrite (R_TRACE_HUB_MEM_MTB_SCR, Data32);

    SbiMtbWrite (R_TRACE_HUB_MEM_MTB_CTS_CTRL, 0);
    SbiMtbWrite (R_TRACE_HUB_MEM_MTB_CTS_STS, 0x428);
    SbiMtbWrite (R_TRACE_HUB_MEM_MTB_CTS_CTRL, 0x20001);
    SbiMtbWrite (R_TRACE_HUB_MEM_MTB_CTS_CTRL, 0x00001);
  }
  PciSegmentWrite16 (TraceHubBaseAddress + R_TRACE_HUB_CFG_DSC, 0);
  PciSegmentOr8 (TraceHubBaseAddress + R_TRACE_HUB_CFG_DSC, B_TRACE_HUB_CFG_DSC_FLR);
  DEBUG ((DEBUG_INFO, "Disable early trace complete\n"));
}

/**
  Configure early trace.
  Early trace is activated by default before BIOS.
  Stop early trace if no need which allows s0ix or do nothing, continue tracing.
**/
STATIC
VOID
EarlyTraceConfigure (
  VOID
  )
{
  if (IsDciKeepEarlyTraceSupported ()) {
    if (!IsKeepEarlyTrace ()) {
      DisableEarlyTrace ();
    }
  } else if (IsUndoEarlyTrace ()) {
    DisableEarlyTrace ();
  }
}

/**
  Return timer timeout value per Pch gerneration

  @retval UINT32         Timeout value
**/
STATIC
UINT32
GetTimeOutValue (
  VOID
  )
{
  switch (PchGeneration ()) {
    case ADL_PCH:
      return 0xF4240;

    default:
      return 0x0;
  }
}
/**
  This function performs basic initialization for TraceHub, and return if trace hub needs to be power gated.

  Although controller allows access to a 64bit address resource, PEI phase is a 32-bit environment,
  addresses greater than 4G is not allowed by CPU address space.
  So, the addresses must be limited to below 4G and UBARs should be set to 0.
  If scratchpad0 bit24 is set, which indicates trace hub is used by tool that implies probe is connected and capturing traces.
  With such assumption, BIOS may not power gate trace hub but initialize trace hub as host debugger mode despite what trace hub policy it is.

  @param[in] EnableMode              Trace Hub Enable Mode from policy

  @retval TRUE                       Need to power gate trace hub
  @retval FALSE                      No need to power gate trace hub
**/
BOOLEAN
TraceHubInitialize (
  IN UINT8                           EnableMode
  )
{
  UINT64                  TraceHubBaseAddress;
  UINT32                  Data32;
  UINT32                  MtbUBar;
  UINT32                  MtbLBar;
  UINT32                  SwUBar;
  UINT32                  SwLBar;
  UINT32                  FwUBar;
  UINT32                  FwLBar;
  UINT32                  Scrpd0;

  DEBUG ((DEBUG_INFO, "TraceHubInitialize() - Start\n"));
  Data32 = 0;
  Scrpd0 = 0;
  MtbUBar = 0;
  MtbLBar = PCH_TRACE_HUB_MTB_BASE_ADDRESS;
  SwUBar = 0;
  SwLBar = PCH_TRACE_HUB_SW_BASE_ADDRESS;
  FwUBar = 0;
  FwLBar = PCH_TRACE_HUB_FW_BASE_ADDRESS;
  //
  // Check if Trace Hub Device is present
  //
  TraceHubBaseAddress = PchTraceHubPciCfgBase ();

  if (PciSegmentRead16 (TraceHubBaseAddress) == 0xFFFF) {
    if (EnableMode == TraceHubModeDisabled) {
      DEBUG ((DEBUG_INFO, "TraceHubInitialize() - End. TraceHub device is not present due to TH mode is disabled\n"));
      return FALSE;
    }
    //
    // We are here assuming TH is in PG state, but user opt-in TH enabled, so we un-gate trace hub via PMC IPC1 command.
    // Noted: Trace hub PG state preserves until G3 / global reset.
    // No need to check tool ownership via SCRPD0[24], for it can only be set when TH is active (un-gated).
    //
    DEBUG ((DEBUG_INFO, "TraceHubInitialize - un-gate trace hub due to user opt-in PCH trace hub mode from disabled to enabled.\n"));
    PmcEnableTraceHub ();
  }
  ///
  /// Configure Early Trace
  ///
  EarlyTraceConfigure ();

  SbiMtbRead (R_TRACE_HUB_MEM_CSR_MTB_SCRPD0, &Scrpd0);
  DEBUG ((DEBUG_INFO, "SCRPD0 = 0x%08x\n", Scrpd0));
  ///
  /// Check if STT is disconnected and if user requested disabling of Trace Hub
  ///
  if ((EnableMode == TraceHubModeDisabled) && !(Scrpd0 & B_TRACE_HUB_MEM_CSR_MTB_SCRPD0_DEBUGGER_IN_USE)) {
    ///
    /// Clear MSE
    ///
    PciSegmentWrite8 (TraceHubBaseAddress + PCI_COMMAND_OFFSET, 0);
    DEBUG ((DEBUG_INFO, "TraceHubInitialize() - End. STT disconnected and Trace Hub requested to be disable\n"));
    return TRUE;
  }
  //
  // Set below connfigurations when tool is not in use. When tool is in use, they are controlled by tool.
  //
  if (!(Scrpd0 & B_TRACE_HUB_MEM_CSR_MTB_SCRPD0_DEBUGGER_IN_USE)) {
    ///
    /// Clear LPMEN bit in R_TRACE_HUB_CFG_CSR_MTB_LPP_CTL register
    ///
    SbiMtbRead (R_TRACE_HUB_MEM_CSR_MTB_LPP_CTL, &Data32);
    DEBUG ((DEBUG_INFO, "LPP_CTL old = 0x%08x\n", Data32));
    Data32 &= ~B_TRACE_HUB_MEM_CSR_MTB_LPP_CTL_LPMEN;
    SbiMtbWrite (R_TRACE_HUB_MEM_CSR_MTB_LPP_CTL, Data32);
    DEBUG ((DEBUG_INFO, "LPP_CTL new = 0x%08x\n", Data32));

    ///
    /// Program inbound bus timer timeout value for error handling
    ///
    Data32 = GetTimeOutValue ();
    if (Data32 !=  0) {
      PciSegmentWrite32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_ISTOT, Data32);
      PciSegmentWrite32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_ICTOT, Data32);
    }
    ///
    /// Setup xHCI DbC for Trace Hub tracing, it needs to be ready before setting MSE
    ///
    DEBUG ((DEBUG_INFO, "TraceHubInitialize() - Setup USB3 DbC Tracing\n"));
    ConfigureXhciDbcTrace ();
  }

  ///
  /// Enable TraceHub debug for Sx
  ///
  PmcEnableTraceHubDebugForSx ();

  ///
  /// Program the MTB Base Address Register
  ///
  // Set MTB_LBAR (PCI offset 0x10)
  PciSegmentWrite32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_CSR_MTB_LBAR, MtbLBar);
  //
  // Set MTB_UBAR (PCI offset 0x14)
  //
  PciSegmentWrite32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_CSR_MTB_UBAR, MtbUBar);

  ///
  /// Program the SW Base Address Register with fixed BAR
  ///
  //
  // SW_LBAR (PCI offset 0x18)
  //
  PciSegmentWrite32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_SW_LBAR, SwLBar);
  //
  // SW_UBAR (PCI offset 0x1C)
  //
  PciSegmentWrite32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_SW_UBAR, SwUBar);

  ///
  /// Program the FW BAR and Shadow PCI Device
  ///
  TraceHubBaseSet (FwUBar, FwLBar);

  CpuWriteTraceHubAcpiBaseHelper ((UINT64) (LShiftU64 ((UINT64)FwUBar, 32) + FwLBar));

  ///
  /// Enable MSE and BME when debugger is not in use.
  ///
  if (!(Scrpd0 & B_TRACE_HUB_MEM_CSR_MTB_SCRPD0_DEBUGGER_IN_USE)) {
    PciSegmentOr8 (TraceHubBaseAddress + PCI_COMMAND_OFFSET, EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_BUS_MASTER);
  }

  DEBUG ((DEBUG_INFO, "TraceHubInitialize () Assigned BARs:\n"));
  DEBUG ((DEBUG_INFO, "TraceHubInitialize () FW_LBAR  = 0x%08x\n", PciSegmentRead32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_FW_LBAR) & B_TRACE_HUB_CFG_FW_RBAL));
  DEBUG ((DEBUG_INFO, "TraceHubInitialize () MTB_LBAR = 0x%08x\n", PciSegmentRead32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_CSR_MTB_LBAR) & B_TRACE_HUB_CFG_CSR_MTB_RBAL));
  DEBUG ((DEBUG_INFO, "TraceHubInitialize () SW_LBAR  = 0x%08x\n", PciSegmentRead32 (TraceHubBaseAddress + R_TRACE_HUB_CFG_SW_LBAR) & B_TRACE_HUB_CFG_SW_RBAL));

  ///
  /// Lock power gate control register
  ///
  DEBUG ((DEBUG_INFO, "TraceHubInitialize() Locking HSWPGCR1\n"));
  PmcLockHostSwPgCtrl (PmcGetPwrmBase ());

  DEBUG ((DEBUG_INFO, "TraceHubInitialize () - End\n"));
  return FALSE;
}

/**
  Disable and power gate Pch trace hub

  @retval     EFI_SUCCESS       The function completed successfully.
  @retval     EFI_UNSUPPORTED   The device is not supported
**/
EFI_STATUS
PchTraceHubDisable (
  VOID
  )
{
  UINT64                  TraceHubBaseAddress;
  ///
  /// Check if Trace Hub Device is present
  ///
  TraceHubBaseAddress = PchTraceHubPciCfgBase ();

  if (PciSegmentRead16 (TraceHubBaseAddress) == 0xFFFF) {
    DEBUG ((DEBUG_INFO, "PchTraceHubDisable() TraceHub device is not present\n"));
    return EFI_UNSUPPORTED;
  }
  ///
  /// Disable trace hub phantom device via PSF
  ///
  PsfDisableDevice (PsfTraceHubAcpiDevPort ());
  ///
  /// Disable trace hub via PSF
  ///
  DEBUG ((DEBUG_INFO, "PchTraceHubDisable() disable Trace Hub device via PSF\n"));
  PsfDisableDevice (PsfTraceHubPort ());

  ///
  /// Remove force ACRO_ON
  ///
  PmcDisableTraceHubDebugForSx ();

  ///
  /// Disable Trace Hub
  ///
  PostCode (0xB50);
  DEBUG ((DEBUG_INFO, "PchTraceHubDisable() Power gate Trace Hub device via PMC\n"));
  PmcDisableTraceHub (PmcGetPwrmBase ());
  ///
  /// Lock power gate control register
  ///
  DEBUG ((DEBUG_INFO, "PchTraceHubDisable() Locking HSWPGCR1\n"));
  PmcLockHostSwPgCtrl (PmcGetPwrmBase ());

  return EFI_SUCCESS;
}
