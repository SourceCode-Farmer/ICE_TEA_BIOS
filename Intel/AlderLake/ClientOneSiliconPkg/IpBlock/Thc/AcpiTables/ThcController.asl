/**@file
  Touch Host Controller ACPI

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

  Name(_ADR, THC_ADR)
  Name(RSTL, 0) // Reset SW lock

  // _DSM - Device-Specific Method
  //
  // Arg0:    UUID       Unique function identifier
  // Arg1:    Integer    Revision Level - Will be 2 for HidSpi V1
  // Arg2:    Integer    Function Index (0 = Return Supported Functions)
  // Arg3:    Package    Parameters
  //
  Method (_DSM, 4, Serialized, 0, UnknownObj, {BuffObj, IntObj, IntObj, PkgObj}) {
    If (PCIC(Arg0)) { Return(PCID(Arg0,Arg1,Arg2,Arg3)) }
    If (LEqual (THC_MODE, 0x1)) {
      If(LEqual(Arg0,ToUUID("6E2AC436-0FCF-41AF-A265-B32A220DCFAB"))) {
        //
        // Switch on the function index
        //
        switch(ToInteger(Arg2)) {
          case(0) {
            // Switch on the revision level
            switch(ToInteger(Arg1)) {
              case (2) {
                // HidSpi v1 : Functions 0-6 inclusive are supported (0b01111111)
                Return (Buffer() {0x7F})
              }
              default {
                // Unsupported revision
                Return(Buffer() {0x00})
              }
            }
          } // End case0
          case(1) {
            ADBG ("THC THC_INPUT_REPORT_HEADER_ADDRESS")
            Return (ToInteger (THC_INPUT_REPORT_HEADER_ADDRESS))
          }
          case(2) {
            ADBG ("THC THC_INPUT_REPORT_BODY_ADDRESS")
            Return (ToInteger (THC_INPUT_REPORT_BODY_ADDRESS))
          }
          case(3) {
            ADBG ("THC THC_OUTPUT_REPORT_ADDRESS")
            Return (ToInteger (THC_OUTPUT_REPORT_ADDRESS))
          }
          case(4) {
            ADBG ("THC THC_READ_OPCODE")
            Name(BUF4, Buffer(1) {})
            Store(ToBuffer (THC_READ_OPCODE), Local0)
            Store(DerefOf(Index(Local0, 0)), Index(BUF4,0))
            Return (BUF4)
          }
          case(5) {
            ADBG ("THC THC_WRITE_OPCODE")
            Name(BUF5, Buffer(1) {})
            Store(ToBuffer (THC_WRITE_OPCODE), Local1)
            Store(DerefOf(Index(Local1, 0)), Index(BUF5,0))
            Return (BUF5)
          }
          case(6) {
            /*
            Bit 0-12: Reserved
            Bit 13: SPI Write Mode.
             0b0 - Writes are carried in single SPI mode
             0b1 - Writes are carried out in Multi-SPI mode as specified by bit 14-15
            Bit 14-15: Multi-SPI Mode
             0b00 - Single SPI Mode
             0b01 - Dual SPI Mode
             0b10 - Quad SPI Mode
             0b11 - Reserved
            */
            ADBG ("THC THC_FLAGS")
            Return (ToInteger (THC_FLAGS))
          }
          default {
            // Unsupported function index
            Return (Buffer() {0})
          }
        } //EndSwitch
        //
        // No functions are supported for this UUID.
        //
        Return (Buffer() {0})
      } //EndIfUUID
      If(LEqual(Arg0,ToUUID("300D35B7-AC20-413E-8E9C-92E4DAFD0AFE"))) {
        switch(ToInteger(Arg2)) {
          case(0) {
            // Functions 1-3 inclusive are supported (0b00000111)
            Return (Buffer() {0x7})
          }
          case(1) {
            ADBG ("THC THC_CONNECTION_SPEED")
            /*
             Connection Speed in Hz
             Driver will round down to the nearest Spi frequency that THC supports.
             Max Speed is specific to each Soc design.
            */
            Return (ToInteger (THC_CONNECTION_SPEED))
          }
          case(2) {
            ADBG ("THC THC_LIMIT_PACKET_SIZE")
            /*
            Bit 0: LimitPacketSize
              When set, limits SPI read & write packet size to 64B.
              Otherwise, THC uses Max Soc packet size for SPI Read and Write
              0 - Max Soc Packet Size
              1 - 64 Bytes
            Bit 1-31: Reserved
            */
            Return (ToInteger (THC_LIMIT_PACKET_SIZE))
          }
          case(3) {
            ADBG ("THC THC_PERFORMANCE_LIMITATION")
            /*
            Bit 0-15: Performance Limitation
              Minimum amount of delay the THC/QUICKSPI driver must wait between end of write operation
              and begin of read operation. This value shall be in 10us multiples
              0 - Disabled
              1 - 65535 (0xFFFF) - up to 655350 us
            Bit 16-31: Reserved
            */
            Return (ToInteger (THC_PERFORMANCE_LIMITATION))
          }
          default {
            // Unsupported function index
            Return (Buffer() {0})
          }
        } // End Switch
      } // End UUID
    } // End THC HID Mode
    If(LEqual(Arg0,ToUUID("84005682-5B71-41A4-8D66-8130F787A138"))) {
      switch(ToInteger(Arg2)) {
        case(0) {
          // Function 1/2 are supported (0b00000011)
          Return (Buffer() {0x3})
        }
        case(1) {
            ADBG ("THC THC_ACTIVE_LTR")
          Return (ToInteger (THC_ACTIVE_LTR))
        }
        case(2) {
            ADBG ("THC THC_IDLE_LTR")
          Return (ToInteger (THC_IDLE_LTR))
        }
        default {
          // Unsupported function index
          Return (Buffer() {0})
        }
      } // End Switch
    } // End UUID
    Return(Buffer(){})
  } // End _DSM
  If (LNotEqual (THC_WAKE_INT, 0)) {
    Name (_S0W, 3)
  }

  //
  // Expose THC Resources only when THC Wake is enabled
  //
  If (LNotEqual (THC_WAKE_INT, 0)) {
    Method(_CRS, 0x0, Serialized) {
      Return (TINT (THC_WAKE_INT))
    }
  }
  If (LEqual (THC_MODE, 0x1)) {
    Method(_INI) {
      ADBG ("THC _INI")
      // configure gpio pad in gpio driver mode
      SHPO(THC_RST_PAD, 1)
      // Make sure both pads are in GPIO mode
      SPMV(THC_RST_PAD, 0)
      // Put device in inital reset state
      SPC0(THC_RST_PAD, Or (0x42000200, And(Not(And(THC_RST_TRIGGER,1)),1)))
    }
    Method (_RST, 0, Serialized) {
      ADBG ("THC _RST")
      // Wait until Lock is freed
      // Note: Lock should be not required because Method is serialized
      //       added to avoid race conditions
      While(LEqual(RSTL, 1)) {
        Sleep (10)
      }
      // Acquire Lock
      Store (1, RSTL)
      SGOV(THC_RST_PAD, And(THC_RST_TRIGGER,1))
      Sleep (300)
      SGOV(THC_RST_PAD, And(Not(And(THC_RST_TRIGGER,1)), 1))
      // Release Lock
      Store (0, RSTL)
    }
  }

