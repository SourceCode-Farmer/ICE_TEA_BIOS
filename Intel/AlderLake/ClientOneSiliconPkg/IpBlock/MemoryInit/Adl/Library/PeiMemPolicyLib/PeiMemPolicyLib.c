/** @file
  This file initializes PEI policies: MEMORY_CONFIG_NO_CRC and MEMORY_CONFIGURATION

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/SaPlatformLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/PeiMemPolicyLib.h>
#include "MrcInterface.h"
#include <Library/PeiSaInitFruLib.h>
//
// Need minimum of 48MB during PEI phase for IAG and some buffer for boot.
//
#define  PEI_MIN_MEMORY_SIZE               (10 * 0x800000 + 0x10000000)    // 80MB + 256MB

static COMPONENT_BLOCK_ENTRY  mMrcIpBlocksPreMem[] = {
  { &gMemoryConfigGuid, sizeof (MEMORY_CONFIGURATION), MEMORY_CONFIGURATION_REVISION, LoadMemConfigDefault },
  { &gMemoryConfigNoCrcGuid, sizeof (MEMORY_CONFIG_NO_CRC), MEMORY_CONFIG_NO_CRC_REVISION, LoadMemConfigNoCrcDefault }
};

/**
  Load Config block default

  @param[in] ConfigBlockPointer         Pointer to config block
**/
VOID
LoadMemConfigNoCrcDefault (
  IN VOID    *ConfigBlockPointer
  )
{
  MEMORY_CONFIG_NO_CRC                    *MemConfigNoCrc;

  MemConfigNoCrc = ConfigBlockPointer;
  DEBUG ((DEBUG_INFO, "%aName = %g\n", "MemConfigNoCrc->Header.GuidHob.", &MemConfigNoCrc->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "%aHeader.HobLength = 0x%x\n", "MemConfigNoCrc->Header.GuidHob.", MemConfigNoCrc->Header.GuidHob.Header.HobLength));
  //
  // Allocating memory space for pointer structures inside MemConfigNoCrc
  //
  MemConfigNoCrc->SpdData = (SPD_DATA_BUFFER *) AllocateZeroPool (sizeof (SPD_DATA_BUFFER));
  ASSERT (MemConfigNoCrc->SpdData != NULL);
  if (MemConfigNoCrc->SpdData == NULL) {
    return;
  }

  MemConfigNoCrc->DqDqsMap = (SA_MEMORY_DQDQS_MAPPING *) AllocateZeroPool (sizeof (SA_MEMORY_DQDQS_MAPPING));
  ASSERT (MemConfigNoCrc->DqDqsMap != NULL);
  if (MemConfigNoCrc->DqDqsMap == NULL) {
    return;
  }

  MemConfigNoCrc->RcompData = (SA_MEMORY_RCOMP *) AllocateZeroPool (sizeof (SA_MEMORY_RCOMP));
  ASSERT (MemConfigNoCrc->RcompData != NULL);
  if (MemConfigNoCrc->RcompData == NULL) {
    return;
  }

  //
  // Set PlatformMemory Size
  //
  MemConfigNoCrc->PlatformMemorySize = PEI_MIN_MEMORY_SIZE;
  MemConfigNoCrc->SerialDebugLevel  = 3;  //< Enable MRC debug message

  MemConfigNoCrc->MemTestOnWarmBoot = 0x1;
}

/**
  Load Config block default

  @param[in] ConfigBlockPointer         Pointer to config block
**/
VOID
LoadMemConfigDefault (
  IN VOID *ConfigBlockPointer
  )
{
  MEMORY_CONFIGURATION  *MemConfig;
  UINTN                 Index;

  MemConfig = ConfigBlockPointer;
  DEBUG ((DEBUG_INFO, "%aName = %g\n", "MemConfig->Header.GuidHob.", &MemConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "%aHeader.HobLength = 0x%x\n", "MemConfig->Header.GuidHob.", MemConfig->Header.GuidHob.Header.HobLength));
  // Initialize the Memory Configuration
  MemConfig->EccSupport             = 1;
  MemConfig->ScramblerSupport       = 1;
  MemConfig->PowerDownMode          = 0xFF;
  MemConfig->DisPgCloseIdleTimeout  = 0;
  MemConfig->RankInterleave         = TRUE;
  MemConfig->EnhancedInterleave     = TRUE;
  MemConfig->EnCmdRate              = 7;
  MemConfig->AutoSelfRefreshSupport = TRUE;
  MemConfig->ExtTemperatureSupport  = TRUE;
  MemConfig->Ddr4OneDpc             = 3; // 0 - Disabled; 1 - Enabled on DIMM0 only, 2 - Enabled on DIMM1 only; 3 - Enabled on both DIMMs. (bit [0] - DIMM0, bit [1] - DIMM1)
  MemConfig->EccDftEn               = FALSE;
  MemConfig->Write0                 = TRUE;

  MemConfig->MrcSafeConfig          = FALSE;
  MemConfig->SafeMode               = FALSE;
  MemConfig->ExitOnFailure          = 0x1;

  // Channel Hash Configuration
  MemConfig->ChHashOverride         = FALSE; // Use POR values per memory technology
  MemConfig->ChHashEnable           = TRUE;
  MemConfig->ChHashMask             = 0x830; // Only used if ChHashOverride is TRUE
  MemConfig->ChHashInterleaveBit    = 2;     // Only used if ChHashOverride is TRUE

  MemConfig->ExtendedBankHashing    = TRUE;
  MemConfig->PerBankRefresh         = TRUE;

  // Options for Thermal settings
  MemConfig->EnablePwrDn            = 1;
  MemConfig->EnablePwrDnLpddr       = 1;
  MemConfig->SrefCfgEna             = 1;
  MemConfig->SrefCfgIdleTmr         = 0x200;
  MemConfig->ThrtCkeMinDefeat       = 1;
  MemConfig->ThrtCkeMinTmr          = 0x00;
  MemConfig->ThrtCkeMinDefeatLpddr  = 1;
  MemConfig->ThrtCkeMinTmrLpddr     = 0x00;

  MemConfig->VttTermination         = IsMobileSku ();
  MemConfig->VttCompForVsshi        = 1;

  MemConfig->SpdProfileSelected     = DEFAULT_SPD;
  // MRC training steps
  MemConfig->ECT         = 1;
  MemConfig->ERDMPRTC2D  = 1;
  MemConfig->SOT         = 0;
  MemConfig->RDMPRT      = 0;
  MemConfig->RCVET       = 1;
  MemConfig->JWRL        = 1;
  MemConfig->EWRTC2D     = 1;
  MemConfig->ERDTC2D     = 1;
  MemConfig->WRTC1D      = 1;
  MemConfig->WRVC1D      = 1;
  MemConfig->RDTC1D      = 1;
  MemConfig->RDVC1D      = 1;
  MemConfig->LCT         = 1;
  MemConfig->CMDVC       = 1;
  MemConfig->WRTC2D      = 1;
  MemConfig->RDTC2D      = 1;
  MemConfig->WRVC2D      = 1;
  MemConfig->RDVC2D      = 1;
  MemConfig->RCVENC1D    = 1;
  MemConfig->DIMMODTT    = 1;
  MemConfig->DIMMDFE     = 1;
  MemConfig->EARLYDIMMDFE = 0;
  MemConfig->TXDQSDCC    = 1;
  MemConfig->DRAMDCA     = 1;
  MemConfig->WRDS        = 1;
  MemConfig->RDODTT      = 1;
  MemConfig->RDAPT       = 1;
  MemConfig->RDEQT       = 1;
  MemConfig->PVTTDNLP    = 1;
  MemConfig->RDVREFDC    = 1;
  MemConfig->DIMMRONT    = 0;
  MemConfig->TXTCO       = 1;
  MemConfig->TXTCODQS    = 1;
  MemConfig->WRSRT       = 1;
  MemConfig->WRDSEQT     = 0;
  MemConfig->WRDSUDT     = 1;
  MemConfig->DIMMODTCA   = 0;
  MemConfig->CLKTCO      = 1;
  MemConfig->CMDSR       = 0;
  MemConfig->CMDDSEQ     = 0;
  MemConfig->CMDDRUD     = 0;
  MemConfig->RTL         = 1;
  MemConfig->TAT         = 0;
  MemConfig->ALIASCHK    = 0;
  MemConfig->RMC         = 0;
  MemConfig->RMT         = 0;
  MemConfig->MEMTST      = 0;
  MemConfig->VCCDLLBP    = 0;
  MemConfig->VDDQT       = 0;
  MemConfig->DCC         = 1;
  MemConfig->RMTBIT      = 0;

  MemConfig->MrcFastBoot   = TRUE;
  MemConfig->WeaklockEn    = TRUE;
  MemConfig->RemapEnable   = TRUE;
  MemConfig->BClkFrequency = 100 * 1000 * 1000;

  MemConfig->Vc1ReadMeter         = TRUE;

  MemConfig->MobilePlatform       = IsMobileSku ();
  MemConfig->PciIndex             = 0xCF8;
  MemConfig->PciData              = 0xCFC;

  // SA GV: 0 - Disabled, 1 - FixedPoint1, 2 - FixedPoint2, 3 - FixedPoint3, 4 - FixedPoint4, 5 - Enabled
  if (IsSimicsEnvironment()) {
    MemConfig->SimicsFlag         = 1;
    // Current Simics will fail in MRC training when SAGV enabled so need to by default disable SAGV.
    MemConfig->SaGv               = 0;
  } else {
    MemConfig->SimicsFlag         = 0;
    UpdateMemConfigSaGvDefault (MemConfig);
  }
  MemConfig->Idd3n                = 26;
  MemConfig->Idd3p                = 11;

  MemConfig->RhSelect             = RhRfm;         // Row Hammer: RhDisable / RhRfm / RhPtrr
  MemConfig->McRefreshRate        = RefreshNORMAL; // RefreshNORMAL / Refresh1x / Refresh2x / Refresh4x
  MemConfig->Lfsr0Mask            = OneIn2To11;
  MemConfig->Lfsr1Mask            = OneIn2To11;
  MemConfig->LpddrRttWr           = 0; // Auto
  MemConfig->LpddrRttCa           = 0; // Auto

  MemConfig->RefreshWm            = REFRESH_WM_HIGH;  // Refresh Watermarks: Low / High

  MemConfig->RetrainOnFastFail    = 1; // Restart MRC in Cold mode if SW MemTest fails during Fast flow. 0 = Disabled, 1 = Enabled
  MemConfig->LpDqsOscEn           = 1; // LPDDR4/5 and DDR5 Write DQ/DQS Retraining
  MemConfig->CmdMirror            = 0x00; // BitMask where bits [3:0] are controller 0 Channel [3:0] and [7:4] are Controller 1 Channel [3:0].  0 = No Command Mirror and 1 = Command Mirror.

  MemConfig->PprEnable            = 0; // Post-Package Repair: 0: Disabled, 2: Hard PPR

  // Ibecc
  MemConfig->Ibecc                               = FALSE;
  MemConfig->IbeccOperationMode                  = 2;
  for (Index = 0; Index < MAX_IBECC_REGIONS; Index++) {
    MemConfig->IbeccProtectedRangeEnable [Index] = 0x0;
    MemConfig->IbeccProtectedRangeBase [Index]   = 0x0;
    MemConfig->IbeccProtectedRangeMask [Index]   = 0x03FFFFE0;
  }
  MemConfig->IbeccErrInjControl                  = 0;
  MemConfig->IbeccErrInjAddress                  = 0;
  MemConfig->IbeccErrInjMask                     = 0;
  MemConfig->IbeccErrInjCount                    = 0;
  MemConfig->EccErrInjAddress                    = 0;
  MemConfig->EccErrInjMask                       = 0;
  MemConfig->EccErrInjCount                      = 0;

  MemConfig->MarginLimitL2        = 0x64;

  MemConfig->FreqLimitMixedConfig = 0; // Auto
  MemConfig->FreqLimitMixedConfig_1R1R_8GB = 2000;
  MemConfig->FreqLimitMixedConfig_1R1R_16GB = 2000;
  MemConfig->FreqLimitMixedConfig_1R1R_8GB_16GB = 2000;
  MemConfig->FreqLimitMixedConfig_2R2R = 2000;
  MemConfig->LctCmdEyeWidth = 96;
  MemConfig->Lp5BankMode = 0; // Auto
  // 0 - Mobile
  // 1 - Desktop 1DPC
  // 2 - Desktop 2DPC, Daisy Chain, far DIMM should be populated first
  // 3 - Desktop 2DPC, T-topology with asymmetrical DIMM0/1 routing, has particular DIMM population order
  // 4 - Desktop 2DPC, T-topology with symmetrical DIMM0/1 routing, no particular DIMM population order
  // 5 - ULT Mobile
  MemConfig->UserBd = 0;

  // This item must be provided if UserBd is 2 (btDesktop2DpcDaisyChain) or 3 (btDesktop2DpcTeeTopologyAsymmetrical)
  // 4 bit mask: Bit[0]: MC0 DIMM0, Bit[1]: MC0 DIMM1, Bit[2]: MC1 DIMM0, Bit[3]: MC1 DIMM1.
  // For each MC, the first DIMM to be populated should be set to '1'.
  MemConfig->FirstDimmBitMask     = 0x0A; // Default is standard daisy-chain, Far DIMM is DIMM1 on each MC.
  MemConfig->FirstDimmBitMaskEcc  = 0x0A; // Default is standard daisy-chain, Far ECC DIMM is DIMM1 on each MC.

  MemConfig->SagvSwitchFactorIA         = 30;
  MemConfig->SagvSwitchFactorGT         = 30;
  MemConfig->SagvSwitchFactorIO         = 30;
  MemConfig->SagvSwitchFactorStall      = 30;
  MemConfig->SagvHeuristicsUpControl    = 1;
  MemConfig->SagvHeuristicsDownControl  = 1;
}

/**
  Get Mrc config block table total size.

  @retval      Size of Mrc config block table
**/
UINT16
EFIAPI
MrcGetConfigBlockTotalSizePreMem (
  VOID
  )
{
  return GetComponentConfigBlockTotalSize(&mMrcIpBlocksPreMem[0], sizeof(mMrcIpBlocksPreMem) / sizeof(COMPONENT_BLOCK_ENTRY));
}

/**
  MrcAddConfigBlocksPreMem add all Mrc PEI PreMem config block.

  @param[in] ConfigBlockTableAddress    The pointer to add Mrc config blocks

  @retval EFI_SUCCESS                   The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES          Insufficient resources to create buffer
**/
EFI_STATUS
EFIAPI
MrcAddConfigBlocksPreMem (
  IN VOID           *ConfigBlockTableAddress
  )
{
  EFI_STATUS  Status;
  Status = AddComponentConfigBlocks(ConfigBlockTableAddress, &mMrcIpBlocksPreMem[0], sizeof (mMrcIpBlocksPreMem) / sizeof (COMPONENT_BLOCK_ENTRY));
  if (Status == EFI_SUCCESS) {
    MrcLoadSamplePolicyPreMem(ConfigBlockTableAddress);
  }
  return Status;
}

/**
  This function prints the PEI phase Mrc PreMem policy.

  @param[in] SiPolicyPreMemPpi - Instance of SI_PREMEM_POLICY_PPI
**/
VOID
EFIAPI
MrcPrintPolicyPpiPreMem (
  IN  SI_PREMEM_POLICY_PPI *SiPolicyPreMemPpi
  )
{
  DEBUG_CODE_BEGIN();
  EFI_STATUS                            Status;
  MEMORY_CONFIGURATION                  *MemConfig;
  MEMORY_CONFIG_NO_CRC                  *MemConfigNoCrc;

  //
  // Get requisite IP Config Blocks which needs to be used here
  //
  Status = GetConfigBlock ((VOID *) SiPolicyPreMemPpi, &gMemoryConfigGuid, (VOID *) &MemConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPreMemPpi, &gMemoryConfigNoCrcGuid, (VOID *) &MemConfigNoCrc);
  ASSERT_EFI_ERROR(Status);

  DEBUG((DEBUG_INFO, "------------------------ MEMORY_CONFIGURATION -----------------\n"));
  DEBUG((DEBUG_INFO, " SpdProfileSelected: %d\n tCL: %d\n tRCDtRP: %d\n",   MemConfig->SpdProfileSelected, MemConfig->tCL, MemConfig->tRCDtRP));
  DEBUG((DEBUG_INFO, " tRAS: %d\n tWR: %d\n tRFC: %d\n tRRD: %d\n",         MemConfig->tRAS, MemConfig->tWR, MemConfig->tRFC, MemConfig->tRRD));
  DEBUG((DEBUG_INFO, " tWTR: %d\n tRTP: %d\n tFAW: %d\n tCWL: %d\n",        MemConfig->tWTR, MemConfig->tRTP, MemConfig->tFAW, MemConfig->tCWL));
  DEBUG((DEBUG_INFO, " tREFI: %d\n VddVoltage: %d\n VddqVoltage: %d\n",     MemConfig->tREFI, MemConfig->VddVoltage, MemConfig->VddqVoltage));
  DEBUG((DEBUG_INFO, " VppVoltage: %d\n EccSupport: %d\n EccDftEn: %d\n",   MemConfig->VppVoltage, MemConfig->EccSupport, MemConfig->EccDftEn));
  DEBUG((DEBUG_INFO, " Write0: %d\n UserBd: %d\n FirstDimmBitMask: 0x%X  FirstDimmBitMaskEcc: 0x%X\n", MemConfig->Write0, MemConfig->UserBd, MemConfig->FirstDimmBitMask, MemConfig->FirstDimmBitMaskEcc));
  DEBUG((DEBUG_INFO, " FreqLimitMixedConfig: %d\n",                         MemConfig->FreqLimitMixedConfig));
  DEBUG((DEBUG_INFO, " FreqLimitMixedConfig_1R1R_8GB: %d\n",                MemConfig->FreqLimitMixedConfig_1R1R_8GB));
  DEBUG((DEBUG_INFO, " FreqLimitMixedConfig_1R1R_16GB: %d\n",               MemConfig->FreqLimitMixedConfig_1R1R_16GB));
  DEBUG((DEBUG_INFO, " FreqLimitMixedConfig_1R1R_8GB_16GB: %d\n",           MemConfig->FreqLimitMixedConfig_1R1R_8GB_16GB));
  DEBUG((DEBUG_INFO, " FreqLimitMixedConfig_2R2R: %d\n",                    MemConfig->FreqLimitMixedConfig_2R2R));
  DEBUG((DEBUG_INFO, " LctCmdEyeWidth: %d\n",                               MemConfig->LctCmdEyeWidth));
  DEBUG((DEBUG_INFO, " Lp5BankMode: %d\n",                                  MemConfig->Lp5BankMode));

  DEBUG((DEBUG_INFO, "------------------------ MEMORY_CONFIG_NO_CRC -----------------\n"));
  DEBUG((DEBUG_INFO, " CleanMemory: %d\n MemTestOnWarmBoot: %d\n", MemConfigNoCrc->CleanMemory, MemConfigNoCrc->MemTestOnWarmBoot));
  DEBUG((DEBUG_INFO, " SerialDebugLevel: %d\n PlatformMemorySize: 0x%llX\n", MemConfigNoCrc->SerialDebugLevel, MemConfigNoCrc->PlatformMemorySize));

  DEBUG_CODE_END();
  return;
}
