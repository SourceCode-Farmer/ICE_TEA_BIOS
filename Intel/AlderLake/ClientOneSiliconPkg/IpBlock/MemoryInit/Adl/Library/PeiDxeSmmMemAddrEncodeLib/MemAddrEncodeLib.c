/** @file
  File to support address decoding and encoding

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifdef MRC_MINIBIOS_BUILD
#include "MrcOemPlatform.h"
#include "MemoryUtils.h"
#else // MRC_MINIBIOS_BUILD
#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/IoLib.h>
#include <Library/BaseLib.h>
#endif // MRC_MINIBIOS_BUILD
#include <MrcTypes.h>
#include <Protocol/MemoryAddressEncodeDecode.h>

#define MAXFLDSIZE                          89

/*
 * Convert method from original tool
 * config_base: when original is float, float_num*10+10000, convert it to int.
 */
GLOBAL_REMOVE_IF_UNREFERENCED const INT32 ConfigArrayConst[MAXFLDSIZE][39] = {
{14,0,0,0,14,6,0,16,15,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,13,12,11,10,9,8,7,5,4,3},
{15,17,0,0,14,6,0,16,15,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,13,12,11,10,9,8,7,5,4,3},
{16,0,15,0,14,6,0,17,16,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,13,12,11,10,9,8,7,5,4,3},
{17,17,15,0,14,6,0,18,16,0,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,0,0,13,12,11,10,9,8,7,5,4,3},
{10182,17,15,0,14,6,0,18,16,0,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,0,0,13,12,11,10,9,8,7,5,4,3},
{10181,17,0,0,14,6,0,16,15,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,13,12,11,10,9,8,7,5,4,3},
{19,0,0,0,0,6,0,15,14,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,0,13,12,11,10,9,8,7,5,4,3},
{20,16,0,0,0,6,0,15,14,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,13,12,11,10,9,8,7,5,4,3},
{21,0,14,0,0,6,0,16,15,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,13,12,11,10,9,8,7,5,4,3},
{22,16,14,0,0,6,0,17,15,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,13,12,11,10,9,8,7,5,4,3},
{10232,16,14,0,0,6,0,17,15,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,13,12,11,10,9,8,7,5,4,3},
{10231,16,0,0,0,6,0,15,14,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,13,12,11,10,9,8,7,5,4,3},
{10241,17,0,0,14,6,0,16,15,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,13,12,11,10,9,8,7,5,4,3},
{10242,17,0,0,0,6,0,15,14,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,16,0,0,13,12,11,10,9,8,7,5,4,3},
{10251,17,15,0,14,6,0,18,16,0,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,0,0,13,12,11,10,9,8,7,5,4,3},
{10252,17,14,0,0,6,0,16,15,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,13,12,11,10,9,8,7,5,4,3},
{10262,17,15,0,14,6,0,18,16,0,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,0,0,13,12,11,10,9,8,7,5,4,3},
{10261,17,0,0,0,6,0,15,14,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,16,0,0,13,12,11,10,9,8,7,5,4,3},
{10272,17,14,0,0,6,0,16,15,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,13,12,11,10,9,8,7,5,4,3},
{10271,17,0,0,14,6,0,16,15,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,13,12,11,10,9,8,7,5,4,3},
{10281,0,0,0,14,6,0,16,15,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,13,12,11,10,9,8,7,5,4,3},
{10282,0,0,0,0,6,0,15,14,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,0,13,12,11,10,9,8,7,5,4,3},
{41,6,0,0,0,0,15,14,13,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,12,11,10,9,8,7,5,4,3,2,1},
{42,6,15,0,0,0,16,14,13,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,12,11,10,9,8,7,5,4,3,2,1},
{43,0,0,0,0,0,14,13,12,0,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,0,11,10,9,8,7,6,5,4,3,2,1},
{44,0,14,0,0,0,15,13,12,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,11,10,9,8,7,6,5,4,3,2,1},
{45,0,0,0,0,0,14,13,12,0,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,0,11,10,9,8,7,6,5,4,3,2,1},
{46,6,15,0,16,0,17,14,13,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,12,11,10,9,8,7,5,4,3,2,1},
{47,0,0,0,6,14,0,13,12,0,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,0,0,11,10,9,8,7,5,4,3,2,1},
{48,0,14,0,6,15,0,13,12,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,0,11,10,9,8,7,5,4,3,2,1},
{49,6,0,0,7,15,0,14,13,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,0,12,11,10,9,8,5,4,3,2,1},
{50,6,15,0,7,16,0,14,13,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,12,11,10,9,8,5,4,3,2,1},
{59,6,0,0,0,0,14,13,12,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,0,0,11,10,9,8,7,5,4,3,2,1},
{60,6,14,0,0,0,15,13,12,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,0,11,10,9,8,7,5,4,3,2,1},
{61,0,0,0,0,0,13,12,11,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,0,0,10,9,8,7,6,5,4,3,2,1},
{62,0,13,0,0,0,14,12,11,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,0,0,10,9,8,7,6,5,4,3,2,1},
{63,0,0,0,0,0,13,12,11,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,0,0,10,9,8,7,6,5,4,3,2,1},
{73,6,14,0,15,0,16,13,12,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,11,10,9,8,7,5,4,3,2,1},
{76,0,0,0,14,6,17,16,15,0,0,33,32,31,29,28,27,26,25,24,23,22,21,20,19,18,30,0,0,13,12,11,10,9,8,7,5,4,3},
{77,17,0,0,14,6,18,16,15,0,0,34,33,32,30,29,28,27,26,25,24,23,22,21,20,19,31,0,0,13,12,11,10,9,8,7,5,4,3},
{78,0,15,0,14,6,18,17,16,0,0,34,33,32,30,29,28,27,26,25,24,23,22,21,20,19,31,0,0,13,12,11,10,9,8,7,5,4,3},
{79,17,15,0,14,6,19,18,16,0,0,35,34,33,31,30,29,28,27,26,25,24,23,22,21,20,32,0,0,13,12,11,10,9,8,7,5,4,3},
{10802,17,15,0,14,6,19,18,16,0,0,35,34,33,31,30,29,28,27,26,25,24,23,22,21,20,32,0,0,13,12,11,10,9,8,7,5,4,3},
{10801,17,0,0,14,6,18,16,15,0,0,34,33,32,30,29,28,27,26,25,24,23,22,21,20,19,31,0,0,13,12,11,10,9,8,7,5,4,3},
{81,0,0,0,0,6,16,15,14,0,0,32,31,30,28,27,26,25,24,23,22,21,20,19,18,17,29,0,0,13,12,11,10,9,8,7,5,4,3},
{82,16,0,0,0,6,17,15,14,0,0,33,32,31,29,28,27,26,25,24,23,22,21,20,19,18,30,0,0,13,12,11,10,9,8,7,5,4,3},
{83,0,14,0,0,6,17,16,15,0,0,33,32,31,29,28,27,26,25,24,23,22,21,20,19,18,30,0,0,13,12,11,10,9,8,7,5,4,3},
{84,16,14,0,0,6,18,17,15,0,0,34,33,32,30,29,28,27,26,25,24,23,22,21,20,19,31,0,0,13,12,11,10,9,8,7,5,4,3},
{10852,16,14,0,0,6,18,17,15,0,0,34,33,32,30,29,28,27,26,25,24,23,22,21,20,19,31,0,0,13,12,11,10,9,8,7,5,4,3},
{10851,16,0,0,0,6,17,15,14,0,0,33,32,31,29,28,27,26,25,24,23,22,21,20,19,18,30,0,0,13,12,11,10,9,8,7,5,4,3},
{10861,17,0,0,14,6,18,16,15,0,0,34,33,32,30,29,28,27,26,25,24,23,22,21,20,19,31,0,0,13,12,11,10,9,8,7,5,4,3},
{10862,17,0,0,0,6,16,15,14,0,0,33,32,31,29,28,27,26,25,24,23,22,21,20,19,18,30,0,0,13,12,11,10,9,8,7,5,4,3},
{10871,17,15,0,14,6,19,18,16,0,0,35,34,33,31,30,29,28,27,26,25,24,23,22,21,20,32,0,0,13,12,11,10,9,8,7,5,4,3},
{10872,17,14,0,0,6,18,16,15,0,0,34,33,32,30,29,28,27,26,25,24,23,22,21,20,19,31,0,0,13,12,11,10,9,8,7,5,4,3},
{10882,17,15,0,14,6,19,18,16,0,0,35,34,33,31,30,29,28,27,26,25,24,23,22,21,20,32,0,0,13,12,11,10,9,8,7,5,4,3},
{10881,17,0,0,0,6,16,15,14,0,0,33,32,31,29,28,27,26,25,24,23,22,21,20,19,18,30,0,0,13,12,11,10,9,8,7,5,4,3},
{10892,17,14,0,0,6,18,16,15,0,0,34,33,32,30,29,28,27,26,25,24,23,22,21,20,19,31,0,0,13,12,11,10,9,8,7,5,4,3},
{10891,17,0,0,14,6,18,16,15,0,0,34,33,32,30,29,28,27,26,25,24,23,22,21,20,19,31,0,0,13,12,11,10,9,8,7,5,4,3},
{10901,0,0,0,14,6,17,16,15,0,0,33,32,31,29,28,27,26,25,24,23,22,21,20,19,18,30,0,0,13,12,11,10,9,8,7,5,4,3},
{10902,0,0,0,0,6,16,15,14,0,0,32,31,30,28,27,26,25,24,23,22,21,20,19,18,17,29,0,0,13,12,11,10,9,8,7,5,4,3},
{10911,0,0,14,13,6,0,0,15,0,0,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,0,12,11,10,9,8,7,5,4,3,2},
{10912,0,0,0,13,6,0,0,14,0,0,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,0,0,12,11,10,9,8,7,5,4,3,2},
{10921,0,0,14,13,6,0,16,15,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,12,11,10,9,8,7,5,4,3,2},
{10922,0,0,0,13,6,0,15,14,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,0,12,11,10,9,8,7,5,4,3,2},
{10931,0,15,14,13,6,0,0,16,0,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,12,11,10,9,8,7,5,4,3,2},
{10932,0,14,0,13,6,0,0,15,0,0,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,0,12,11,10,9,8,7,5,4,3,2},
{10941,0,15,14,13,6,0,17,16,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,12,11,10,9,8,7,5,4,3,2},
{10942,0,14,0,13,6,0,16,15,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,12,11,10,9,8,7,5,4,3,2},
{10951,17,0,14,13,6,0,16,15,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,12,11,10,9,8,7,5,4,3,2},
{10952,17,0,0,13,6,0,15,14,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,16,0,0,12,11,10,9,8,7,5,4,3,2},
{10991,17,0,14,13,6,0,0,15,0,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,16,0,0,12,11,10,9,8,7,5,4,3,2},
{10992,17,0,0,13,6,0,0,14,0,0,31,30,29,28,27,26,25,24,23,22,21,20,19,18,16,15,0,0,12,11,10,9,8,7,5,4,3,2},
{10971,17,15,14,13,6,0,19,16,0,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,18,0,0,12,11,10,9,8,7,5,4,3,2},
{10972,17,14,0,13,6,0,16,15,0,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,12,11,10,9,8,7,5,4,3,2},
{11011,17,15,14,13,6,0,0,16,0,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,0,0,12,11,10,9,8,7,5,4,3,2},
{11012,17,14,0,13,6,0,0,15,0,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,16,0,0,12,11,10,9,8,7,5,4,3,2},
{11031,0,0,14,13,6,0,0,15,0,0,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,0,12,11,10,9,8,7,5,4,3,2},
{11032,0,0,0,13,6,0,0,14,0,0,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,0,0,12,11,10,9,8,7,5,4,3,2},
{11041,0,0,14,13,6,0,16,15,0,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,0,0,12,11,10,9,8,7,5,4,3,2},
{11042,0,0,0,13,6,0,15,14,0,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,0,0,12,11,10,9,8,7,5,4,3,2},
};

/*
 * Convert method from original tool
 * config_base: when original is float, float_num*10+10000, convert it to int.
 */
GLOBAL_REMOVE_IF_UNREFERENCED const INT16 EnhConfigArrayConst[MAXFLDSIZE][10] = {
{14,0,0,0,17,13,0,19,18,0},
{15,21,0,0,18,13,0,20,19,0},
{16,0,19,0,18,13,0,21,20,0},
{17,22,20,0,19,13,0,23,21,0},
{182,22,20,0,19,13,0,23,21,0},
{181,22,0,0,19,13,0,21,20,0},
{19,0,0,0,0,13,0,17,16,0},
{20,19,0,0,0,13,0,18,17,0},
{21,0,17,0,0,13,0,19,18,0},
{22,20,18,0,0,13,0,21,19,0},
{232,20,18,0,0,13,0,21,19,0},
{231,20,0,0,0,13,0,19,18,0},
{241,19,0,0,17,13,0,0,18,0},
{242,19,0,0,0,13,0,18,17,0},
{251,21,19,0,18,13,0,22,20,0},
{252,21,18,0,0,13,0,20,19,0},
{262,21,20,0,19,13,0,22,0,0},
{261,21,0,0,0,13,0,20,19,0},
{272,21,18,0,0,13,0,20,19,0},
{271,21,0,0,18,13,0,20,19,0},
{281,0,0,0,17,13,0,19,18,0},
{282,0,0,0,0,13,0,18,17,0},
{50,0,19,0,21,20,0,18,17,0},
{49,0,0,0,19,18,0,17,16,0},
{48,0,18,0,20,19,0,17,16,0},
{47,0,0,0,18,17,0,16,15,0},
{41,0,0,0,0,0,18,17,16,0},
{42,0,19,0,0,0,20,18,17,0},
{43,0,0,0,0,0,17,16,15,0},
{44,0,18,0,0,0,19,17,16,0},
{45,0,0,0,0,0,17,16,15,0},
{46,0,20,0,21,0,22,19,18,0},
{55,0,0,0,0,0,17,16,15,0},
{56,0,18,0,0,0,19,17,16,0},
{57,0,0,0,0,0,18,17,16,0},
{58,0,19,0,0,0,20,18,17,0},
{59,0,0,0,0,0,17,16,15,0},
{60,0,18,0,0,0,19,17,16,0},
{61,0,0,0,0,0,16,15,14,0},
{62,0,17,0,0,0,18,16,15,0},
{63,0,0,0,0,0,16,15,14,0},
{65,0,0,0,0,0,17,16,15,0},
{73,0,19,0,20,0,21,18,17,0},
{74,0,19,0,20,0,21,18,17,0},
{75,0,20,0,21,0,22,19,18,0},
{76,0,0,0,17,13,0,19,18,0},
{77,21,0,0,18,13,0,20,19,0},
{78,0,19,0,18,13,0,21,20,0},
{79,22,20,0,19,13,0,23,21,0},
{10802,22,20,0,19,13,0,23,21,0},
{10801,22,0,0,19,13,0,21,20,0},
{81,0,0,0,0,13,0,17,16,0},
{82,19,0,0,0,13,0,18,17,0},
{83,0,17,0,0,13,0,19,18,0},
{84,20,18,0,0,13,0,21,19,0},
{10852,20,18,0,0,13,0,21,19,0},
{10851,20,0,0,0,13,0,19,18,0},
{10861,19,0,0,17,13,0,0,18,0},
{10862,19,0,0,0,13,0,18,17,0},
{10871,21,19,0,18,13,0,22,20,0},
{10872,21,18,0,0,13,0,20,19,0},
{10882,21,20,0,19,13,0,22,0,0},
{10881,21,0,0,0,13,0,20,19,0},
{10892,21,18,0,0,13,0,20,19,0},
{10891,21,0,0,18,13,0,20,19,0},
{10901,0,0,0,17,13,0,19,18,0},
{10902,0,0,0,0,13,0,18,17,0},
{10911,0,0,17,16,18,0,0,19,0},
{10912,0,0,0,15,18,0,0,16,0},
{10921,0,0,19,17,18,0,21,20,0},
{10922,0,0,0,16,18,0,19,17,0},
{10931,0,20,19,17,18,0,0,21,0},
{10932,0,17,0,16,18,0,0,19,0},
{10941,0,21,20,19,18,0,23,22,0},
{10942,0,19,0,17,18,0,21,20,0},
{10951,23,0,20,19,18,0,22,21,0},
{10952,23,0,0,16,18,0,20,19,0},
{10991,23,0,19,16,18,0,0,20,0},
{10992,23,0,0,15,18,0,0,16,0},
{10971,23,22,21,20,18,0,25,24,0},
{10972,23,19,0,20,18,0,22,21,0},
{11011,23,21,20,19,18,0,0,22,0},
{11012,23,19,0,16,18,0,0,20,0},
{11031,0,0,17,16,18,0,0,19,0},
{11032,0,0,0,15,18,0,0,16,0},
{11041,0,0,19,17,18,0,21,20,0},
{11042,0,0,0,16,18,0,19,17,0},
{11051,23,0,0,0,0,0,0,0,0},
{11052,0,0,0,0,0,0,0,0,0}
};

typedef UINT32       (*MRC_IO_READ_32)              (UINT32 IoAddress);
typedef void         (*MRC_IO_WRITE_32)             (UINT32 IoAddress, UINT32 Value);
typedef UINT32       (*MRC_MMIO_READ_32)            (UINT32 Address);
typedef UINT64       (*MRC_MMIO_READ_64)            (UINT32 Address);
typedef void *       (*MRC_MEMORY_COPY)             (UINT8 *Destination, UINT8 *Source, UINT32 NumBytes);
typedef void *       (*MRC_MEMORY_SET_BYTE)         (UINT8 *Destination, UINT32 NumBytes, UINT8 Value);
typedef UINT64       (*MRC_LEFT_SHIFT_64)           (UINT64 Data, UINT32 NumBits);
typedef UINT64       (*MRC_RIGHT_SHIFT_64)          (UINT64 Data, UINT32 NumBits);
typedef UINT64       (*MRC_MULT_U64_U32)            (UINT64 Multiplicand, UINT32 Multiplier);
typedef UINT64       (*MRC_DIV_U64_U64)             (UINT64 Dividend, UINT64 Divisor, UINT64 *Remainder);

///
/// Function calls that are called external to the MRC.
///   This structure needs to be aligned with SA_FUNCTION_CALLS.  All functions that are
///   not apart of SA_FUNCTION_CALLS need to be at the end of the structure.
///
typedef struct {
  MRC_IO_READ_32              MrcIoRead32;
  MRC_IO_WRITE_32             MrcIoWrite32;
  MRC_MMIO_READ_32            MrcMmioRead32;
  MRC_MMIO_READ_64            MrcMmioRead64;
  MRC_MEMORY_COPY             MrcCopyMem;
  MRC_MEMORY_SET_BYTE         MrcSetMem;
  MRC_LEFT_SHIFT_64           MrcLeftShift64;
  MRC_RIGHT_SHIFT_64          MrcRightShift64;
  MRC_MULT_U64_U32            MrcMultU64x32;
  MRC_DIV_U64_U64             MrcDivU64x64;
} MRC_ADDR_FUNCTION;

#define R_SA_MCHBAR                                (0x48)
#define R_SA_PCIEXBAR                              (0x60)
#define R_SA_TOM                                   (0xa0)
#define R_SA_TOLUD                                 (0xbc)

#ifndef MRC_MINIBIOS_BUILD
#define CMI_REMAP_BASE_REG                                             (0x00012168)
#define CMI_REMAP_LIMIT_REG                                            (0x00012170)
#define CMI_MAD_SLICE_REG                                              (0x00012158)
#define CMI_MEMORY_SLICE_HASH_REG                                      (0x00012160)

#define MC0_CHANNEL_HASH_REG                                           (0x0000D824)
#define MC0_CHANNEL_EHASH_REG                                          (0x0000D828)
#define MC0_MAD_INTER_CHANNEL_REG                                      (0x0000D800)
#define MC0_MAD_INTRA_CH0_REG                                          (0x0000D804)
#define MC0_MAD_INTRA_CH1_REG                                          (0x0000D808)
#define MC0_MAD_DIMM_CH0_REG                                           (0x0000D80C)
#define MC0_MAD_DIMM_CH1_REG                                           (0x0000D810)
#define MC0_CH0_CR_SC_GS_CFG_REG                                       (0x0000E088)
#define MC0_MAD_MC_HASH_REG                                            (0x0000D9B8)

#define MC1_CHANNEL_HASH_REG                                           (0x0001D824)
#define MC1_CHANNEL_EHASH_REG                                          (0x0001D828)
#define MC1_MAD_INTER_CHANNEL_REG                                      (0x0001D800)
#define MC1_MAD_INTRA_CH0_REG                                          (0x0001D804)
#define MC1_MAD_INTRA_CH1_REG                                          (0x0001D808)
#define MC1_MAD_DIMM_CH0_REG                                           (0x0001D80C)
#define MC1_MAD_DIMM_CH1_REG                                           (0x0001D810)
#define MC1_CH0_CR_SC_GS_CFG_REG                                       (0x0001E088)
#define MC1_MAD_MC_HASH_REG                                            (0x0001D9B8)
#endif

// size of the hash mask field in the channel hash register
#define MC_ADDR_CHANNEL_HASH_MASK_SIZE  14

// Bit-Masks used to remove or test register fields
//
#define MC_ADDR_TOLUD_MASK              0xFFF00000
#define MC_ADDR_TOM_MASK                0x0000007FFFF00000ULL
#define MC_ADDR_REMAP_MASK              0x0000007FFFF00000ULL
#define MC_ADDR_CHAN_HASH_ENABLE_MASK   0x10000000
#define DRAM_ADDR_SIZE                      38

#define ADL_MC_HASH_ENABLE_OFFSET                  0
#define ADL_MC_HASH_LSB_OFFSET                     1
#define ADL_MC_HASH_ZONE_OFFSET                    4
#define ADL_MC_HASH_STACKED_OFFSET                 13
#define ADL_MC_HASH_ENABLE_MASK                    (1 << ADL_MC_HASH_ENABLE_OFFSET)
#define ADL_MC_HASH_LSB_MASK                       (7 << ADL_MC_HASH_LSB_OFFSET)
#define ADL_MC_HASH_ZONE_MASK                      (0x1FF << ADL_MC_HASH_ZONE_OFFSET)
#define ADL_MC_HASH_STACKED_MASK                   (1 << ADL_MC_HASH_STACKED_OFFSET)

// Mask used to add 1's to lower bits of REMAP_LIMIT register
#define MC_ADDR_REMAP_LIMIT_LOWER_BITS_MASK 0x00000000000FFFFFULL

#define ONE_BIT_MASK                        1
#define TWO_BIT_MASK                        3
#define THREE_BIT_MASK                      7
#define FOUR_BIT_MASK                       15

#define DIMM_NUM_BITS                       1
#define RANK_NUM_BITS                       1
#define BG_NUM_BITS                         3
#define BANK_NUM_BITS                       3
#define ROW_NUM_BITS                        18
#define COL_NUM_BITS                        12
#define ENHANCED_BITS_SIZE                  DIMM_NUM_BITS + RANK_NUM_BITS + BG_NUM_BITS + BANK_NUM_BITS + 1 // "+1" for column[10:10] of 8Gbx8
#define XaB_NUM_OF_BITS                     4
#define DDR5_EBH_BANK_NUM_OF_BITS           5
#define DDR5_EBH_BG_NUM_OF_BITS             6

// Useful number constants
#define MC_256MB_AS_CL  (1 << 22)
#define MC_512MB_AS_CL  (1 << 23)
#define MC_1GB_AS_CL    (1 << 24)
#define MC_2GB_AS_CL    (1 << 25)
#define MC_4GB_AS_CL    (1 << 26)
#define MC_8GB_AS_CL    (1 << 27)
#define MC_16GB_AS_CL   (1 << 28)
#define MC_32GB_AS_CL   (1 << 29)
#define MC_64GB_AS_CL   (1 << 30)
#define MC_128GB_AS_CL  (1 << 31)

#define CACHE_LINE_ADDR_START_BIT_POS 6

#define DDR_DDR4   0
#define DDR_DDR5   1
#define DDR_LPDDR5 2
#define DDR_LPDDR4 3

#define DDR_x8  0
#define DDR_x16 1
#define DDR_x32 2

#define RANK_SIZE_0GB 0
#define RANK_SIZE_1GB 1
#define RANK_SIZE_2GB 2
#define RANK_SIZE_4GB 4
#define RANK_SIZE_6GB 6
#define RANK_SIZE_8GB 8
#define RANK_SIZE_12GB 12
#define RANK_SIZE_16GB 16
#define RANK_SIZE_32GB 32

// EBH - channel Xor bits - XaB[bank] - bits of channel address participating in Xor
GLOBAL_REMOVE_IF_UNREFERENCED const UINT32   XaB[BANK_NUM_BITS][XaB_NUM_OF_BITS] = { {18, 21,24,27},{19,22,25,28},{20,23,26,29} };
GLOBAL_REMOVE_IF_UNREFERENCED const UINT32   XbB_lp4[BANK_NUM_BITS][XaB_NUM_OF_BITS] = { {17,14,0,0},{16,13,0,0},{15,0,0,0} };
GLOBAL_REMOVE_IF_UNREFERENCED const UINT32   XbB_ddr4[2][BANK_NUM_BITS][XaB_NUM_OF_BITS] = { {{17,14,0,0},{16,0,0,0},{15,0,0,0}}, {{15,12,20,26},{14,13,23,29},{0,0,0,0}} };// differecnt Xor for x8 and x16 - HSD 1305305117
GLOBAL_REMOVE_IF_UNREFERENCED const UINT32   ddr5_ebh_bank[2][2][BANK_NUM_BITS-1][DDR5_EBH_BANK_NUM_OF_BITS] =
                                                {{//x16
                                                                // 1 Rank
                                                                {
                                                                                {15,18,21,24, 27}, // BA0
                                                                                {16,19,22,25, 28}  // BA1
                                                                },
                                                                // 2 Ranks
                                                                {
                                                                                {17,20,23,26,29},  // BA0
                                                                                {18,21,24,27,0}    // BA1
                                                                }
                                                },
                                                {//x8
                                                                // 1 Rank
                                                                {
                                                                                {18,22,26,0,0},
                                                                                {19,23,27,0,0}
                                                                },
                                                                // 2 Ranks
                                                                {
                                                                                {20,24,28,0,0},
                                                                                {21,25,29,0,0}
                                                                }
                                                }};
GLOBAL_REMOVE_IF_UNREFERENCED const UINT32    ddr5_ebh_bg[2][2][BG_NUM_BITS-1][DDR5_EBH_BG_NUM_OF_BITS] =
                                                {{//x16
                                                                // 1 Rank
                                                                {
                                                                                {14,17,20,23,26,29}, // BG1
                                                                                {0,0,0,0,0,0}  // BG2
                                                                },
                                                                // 2 Ranks
                                                                {
                                                                                {16,19,22,25,28,0},  // BG1
                                                                                {0,0,0,0,0,0}    // BG2
                                                                }
                                                },
                                                {//x8
                                                                // 1 Rank
                                                                {
                                                                                {16,20,24,28,0,0},
                                                                                {17,21,25,29,0,0}
                                                                },
                                                                // 2 Ranks
                                                                {
                                                                                {18,22,26,0,0,0},
                                                                                {19,23,27,0,0,0}
                                                                }
                                                }};


// Functions to extract fields from the registers
static
UINT32
get_lpddr5_bg_mode(
  const MRC_ADDR_FUNCTION *MrcCall,
  UINT64 SC_GS_CFG
)
{
  //   00 - 4  Banks x 4 Bank Groups (BG Mode)
  //       01 - 8  Banks
  //       10 - 16 Banks
  //   11 - Reserved (do not use)
  return (UINT32) (MrcCall->MrcRightShift64 (SC_GS_CFG, 47) & 3);
}

static
UINT32
get_ddr_type (
  UINT32 MAD_INTER_CHNL
)
{
  return (UINT32)(MAD_INTER_CHNL & 7); // MAD_INTER_CHNL[1:0]; 0=DDR4; 1=DDR3; 2=LPDDR3; 3=LPDDR4; 4=WIO2
}

static
UINT32
get_enhanced_ch_mode (
  UINT32 MAD_INTER_CHNL
)
{
  return (get_ddr_type(MAD_INTER_CHNL) == 2 || get_ddr_type(MAD_INTER_CHNL) == 3);
}

static
UINT64
get_dimm_l_size_as_cl (
  const MRC_ADDR_FUNCTION *MrcCall,
  UINT32 MAD_DIMM
)
{
  return MrcCall->MrcLeftShift64 ((((UINT64)MAD_DIMM) & 0x007FULL), 23); // MAD_DIMM[ 6:0]*0.5GB
}

static
UINT64
get_dimm_s_size_as_cl (
  const MRC_ADDR_FUNCTION *MrcCall,
  UINT32 MAD_DIMM
)
{
  return MrcCall->MrcLeftShift64 ((((UINT64)MAD_DIMM) & 0x007F0000ULL), 7); // MAD_DIMM[22:16]*0.5GB
}

static
BOOLEAN
get_high_order_intlv_mode (
  UINT32 MAD_INTRA_CHNL
)
{
  return (BOOLEAN)((MAD_INTRA_CHNL >> 24) & 1); // MAD_INTRA_CHNL[24:24]
}

static
BOOLEAN
get_enhanced_intlv_mode (
  UINT32 MAD_INTRA_CHNL
)
{
  return (BOOLEAN)((MAD_INTRA_CHNL >> 8) & 1); // MAD_INTRA_CHNL[8:8]
}

static
UINT32
get_dimm_l_select (
  UINT32 MAD_INTRA_CHNL
)
{
  return (UINT32)(MAD_INTRA_CHNL & 1);
}

static
UINT32
get_dimm_l_number_of_ranks (
  UINT32 MAD_DIMM
)
{
  return 1 + (UINT32)((MAD_DIMM >> 9) & 3);
}

static
UINT32
get_dimm_s_number_of_ranks (
  UINT32 MAD_DIMM
)
{
  return 1 + ((MAD_DIMM >> 26) & 3);
}

// This function determine the device width.
// x8, x16, x32(returns 8, 16, 32)
static
UINT32
get_dimm_l_width (
  UINT32 MAD_DIMM
)
{
  UINT32 width;
  width = (UINT32)((MAD_DIMM >> 7) & 3); // 0-x8, 1-x16, 2-x16

  if (width < 2) {
    return (UINT32)((width + 1) << 3);
  } else {
    return (UINT32)((width + 2) << 3);
  }
}

// x8, x16, x32 (returns 8, 16 or 32)
static
UINT32
get_dimm_s_width (
  UINT32 MAD_DIMM
)
{
  UINT32  width;
  width = (UINT32)((MAD_DIMM >> 24) & 3);
  // 0-x8, 1-x16, 2-x32
  if (width < 2) {
    return (UINT32)((width + 1) << 3);
  } else {
    return (UINT32)((width + 2) << 3);
  }
}

UINT32
get_dimm_l_num_col_bits (
  UINT32 ddr_type
) //  8, 9, 10, 11 or 12
{
  if (ddr_type == 2)
    return 11; // LPDDR5
  else
    return 10; // Supported DDR4/LPDDR4/DDR5 DRAM organizations all have 10 column bits
}

UINT32
get_dimm_s_num_col_bits (
  UINT32 ddr_type
)
{
  if (ddr_type == 2)
    return 11; // LPDDR5
  else
    return 10; // Supported DDR4/LPDDR4/DDR5 DRAM organizations all have 10 column bits
}

static
UINT32
get_ch_hash_lsb_mask_bit (
  UINT32 CHANNEL_HASH
)
{
  return (CHANNEL_HASH >> 24) & 7;
}

static
UINT32
get_ch_hash_mask (
  UINT32 CHANNEL_HASH
)
{
  return (CHANNEL_HASH >> 6) & 0x0003FFF;
}
static
BOOLEAN  get_stacked_mode (
  UINT32 MAD_INTER_CHNL
)
{
  return (BOOLEAN)((MAD_INTER_CHNL >> 8) & 1);
}

static
BOOLEAN
get_stacked_mode_ch1 (
  UINT32 MAD_INTER_CHNL
)
{
  return (BOOLEAN)((MAD_INTER_CHNL >> 9) & 1);
}

static
UINT32
get_stacked_encoding (
  UINT32 MAD_INTER_CHNL
)
{
  return (UINT32)(MAD_INTER_CHNL >> 24) & 7;
}

static
UINT32
get_ch_l_map (
  UINT32 MAD_INTER_CHNL
)
{
  return (UINT32)(MAD_INTER_CHNL >> 4) & 1;
}
static
UINT64
get_ch_s_size_as_cl (
  const MRC_ADDR_FUNCTION *MrcCall,
  UINT32 MAD_INTER_CHNL
)
{
  return MrcCall->MrcLeftShift64 (((UINT64)MAD_INTER_CHNL) & 0x00FF000ULL, 11);
}

static
UINT64 get_ms_s_size_as_cl(
  const MRC_ADDR_FUNCTION *MrcCall,
  UINT32 MAD_SLICE )
{
    return MrcCall->MrcLeftShift64 ((((UINT64) MAD_SLICE) & 0x00FF000ULL), 11);
}

static
UINT32
get_dimm_l_8Gb (
  UINT32 MAD_DIMM
)
{
  return (UINT32)(MAD_DIMM >> 11) & 1;
}

static
UINT32
get_dimm_s_8Gb (
  UINT32 MAD_DIMM
)
{
  return (UINT32)(MAD_DIMM >> 28) & 1;
}

static
UINT32
get_bg0_field_value(
  UINT32 MAD_DIMM
)
{
  return (UINT32)(MAD_DIMM >> 28) & 3;
}

static
BOOLEAN is_dl_device_density_8Gb( UINT32 MAD_DIMM )
{
    return ((MAD_DIMM >> 12) & 1) ? TRUE : FALSE;
}

static
BOOLEAN is_ds_device_density_8Gb( UINT32 MAD_DIMM )
{
    return ((MAD_DIMM >> 11) & 1) ? TRUE : FALSE;
}

static
UINT32
get_ebh_mode (
  UINT32 MAD_DIMM_chx
)
{
  return (UINT32)(MAD_DIMM_chx >> 30) & 3;
}

// Functions to aid in common tasks

// make mask for a certain bits
UINT64
get_x_bit_mask (
  const MRC_ADDR_FUNCTION *MrcCall,
  INT32 num_bit
)
{
  UINT64 x_bit_mask;
  x_bit_mask = 0x00FFFFFFFFULL; // 32 bits mask
  return MrcCall->MrcRightShift64 (x_bit_mask, (INT32)(32 - num_bit));
}

// convert a cache-line address to a system address
static
UINT64
cl_to_sys (
  const MRC_ADDR_FUNCTION *MrcCall,
  UINT64 cache_line
)
{
  return MrcCall->MrcLeftShift64 (cache_line, 6);
}
// Channel conversion functions. For logical channels: 0=L, 1=S.
static
UINT32
logical_to_physical_chan (
  UINT32 MAD_INTER_CHNL,
  UINT32 logical_chan
)
{
  return ((UINT32)(((MAD_INTER_CHNL >> 4) & 1) ^ (logical_chan)));
}

static
UINT32
physical_to_logical_chan (
  UINT32 MAD_INTER_CHNL,
  UINT32 physical_chan
)
{
  return ((((UINT32)((MAD_INTER_CHNL >> 4) & 1)) == physical_chan) ? 0 : 1); // 0=L, 1=S
}

// Function for decode of stacked channel debug feature
static
UINT64
get_stacked_memory_size_as_cl(const MRC_ADDR_FUNCTION *MrcCall, UINT32 MAD_INTER_CHNL )
{
  UINT32 i_stacked_encoding = get_stacked_encoding(MAD_INTER_CHNL);
  return MrcCall->MrcLeftShift64 (0x01000000ULL, i_stacked_encoding); // 1 << 30 + stacked_encoding - 6 for the cachline align
}

static
UINT64
get_remap_size (UINT64 remap_limit, UINT64 remap_base)
{
  return (remap_limit > remap_base) ? (remap_limit + 1 - remap_base) : 0;
}

static
BOOLEAN
is_remap_enabled (UINT64 remap_limit, UINT64 remap_base)
{
  return (get_remap_size(remap_limit, remap_base) == 0) ? FALSE : TRUE;
}

static
UINT64
calculate_gap_top (UINT32 tolud, UINT64 remap_size)
{
  return (tolud + remap_size);
}

// NOTE: b4194941 - REMAP_LIMIT is now inclusive
static BOOLEAN is_Sys_Addr_in_Remap_Region (UINT64 system_address, UINT64 remap_base, UINT64 remap_limit)
{
  return ((system_address >= remap_base) && (system_address <= remap_limit)) ? TRUE : FALSE;
}

static inline BOOLEAN is_Sys_Addr_above_TOM (UINT64 system_address, UINT64 tom)
{
        return (system_address >= tom) ? TRUE : FALSE;
}

static BOOLEAN is_Sys_Addr_in_MMIO_Region (UINT64 system_address, UINT64 tolud, UINT64 gap_limit)
{
  /*
   * Origin codes use (system_address <=  gap_limit). Change it to
   * (system_address <  gap_limit), per
   * corner case
   */
  return ((system_address >= tolud) && (system_address <  gap_limit)) ? TRUE : FALSE;
}

static UINT64 get_cacheline_from_addr (const MRC_ADDR_FUNCTION *MrcCall, UINT64 address)
{
  return MrcCall->MrcRightShift64 (address, 6);
}

static UINT32 get_mc_hash_enable (UINT32 mad_mc_hash)
{
  return ( mad_mc_hash & ADL_MC_HASH_ENABLE_MASK) >> ADL_MC_HASH_ENABLE_OFFSET ;
}

static UINT32 get_mc_hash_lsb (UINT32 mad_mc_hash)
{
  return ( mad_mc_hash & ADL_MC_HASH_LSB_MASK) >> ADL_MC_HASH_LSB_OFFSET ;
}

static UINT32 get_mc_zone1_start (UINT32 mad_mc_hash)
{
  return ( mad_mc_hash & ADL_MC_HASH_ZONE_MASK) >> ADL_MC_HASH_ZONE_OFFSET ;
}

static UINT32 get_mc_stacked_mode (UINT32 mad_mc_hash)
{
  return ( mad_mc_hash & ADL_MC_HASH_STACKED_MASK) >> ADL_MC_HASH_STACKED_OFFSET ;
}

static BOOLEAN is_channel_hash_enabled (UINT32 channel_hash)
{
  return ((channel_hash) & MC_ADDR_CHAN_HASH_ENABLE_MASK) ? TRUE : FALSE;
}
static UINT32 get_channel_hash_mask(UINT32 channel_hash)
{
  return get_ch_hash_mask(channel_hash) | (1 << get_ch_hash_lsb_mask_bit( channel_hash ));
}

static UINT64 get_channel_hash_line (UINT64 cache_line, UINT32 hash_mask)
{
  return (cache_line &  hash_mask);
}

static UINT32 get_select_with_hashen (
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 cache_line)
{
  int i;
  UINT32 chan_select = 0;
  for( i = 0 ; i < MC_ADDR_CHANNEL_HASH_MASK_SIZE ; i++ ){
    chan_select = chan_select ^ ((UINT32)(MrcCall->MrcRightShift64(cache_line, i)));
  }
  chan_select = chan_select & 1;
  return chan_select;
}

static INT64 get_sys_addr_with_hashen(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 cache_line,
    UINT32 hash_mask_bit)
{
    UINT64 chan_line;
    chan_line = cache_line & MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, (hash_mask_bit+7));
    chan_line = MrcCall->MrcRightShift64(chan_line, 1);
    chan_line = chan_line | (cache_line & (~MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, (hash_mask_bit+6))));
    return chan_line;
}

static INT64 get_channel_line_with_hashen(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 cache_line,
    UINT32 hash_mask_bit)
{
  UINT64 chan_line;
  chan_line = cache_line & (MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, (hash_mask_bit+1)));
  chan_line = MrcCall->MrcRightShift64(chan_line, 1);
  chan_line = chan_line | (cache_line & (~MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, hash_mask_bit)));
  return chan_line;
}

static INT64 get_dimm_line_with_hashen(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 chan_line,
    UINT32 hash_lsb_mask_bit,
    UINT32 dimm_select)
{
  UINT64 dimm_line;
  dimm_line = (chan_line & MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, (hash_lsb_mask_bit+1)));
  chan_line &= (~(MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, hash_lsb_mask_bit)));
  chan_line = MrcCall->MrcLeftShift64(chan_line, 1);
  dimm_line = dimm_line | chan_line | dimm_select;
  return dimm_line;
}

// NOTE: ch_s_size is based on cache line and its just not size.
// NICE TO HAVE: Fix Bad Naming convention.
static BOOLEAN is_cache_line_in_zone0 (
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 cache_line,
    UINT64 ch_s_size)
{
  ch_s_size = MrcCall->MrcLeftShift64 (ch_s_size, 1);
  return cache_line < ch_s_size ? TRUE : FALSE;
}
static BOOLEAN is_cache_line_in_zone1 (UINT64 cache_line, UINT64 tom)
{
  return (cache_line < tom) ? TRUE : FALSE;
}

// NOTE: Use address bit-6 for channel selection
static INT64 get_channel_select_with_hashdis(UINT64 cache_line)
{
  // Remap_addr[6:6]
  return (cache_line & 1);
}

static INT64 get_channel_line_with_hashdis(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 cache_line)
{
  // Divide by 2 and get the other half
  return MrcCall->MrcRightShift64(cache_line, 1);
}

static
BOOLEAN
get_dimm_select_with_intrleave_on(UINT32 dimm_select_arr[])
{

  int dimm;
  UINT32 dimm_select = 0;
  for(dimm=0; dimm<DIMM_NUM_BITS; dimm++)
    dimm_select += (dimm_select_arr[dimm] << dimm);

  return (BOOLEAN) dimm_select;
}

static
UINT64
get_dimm_line_with_hashdis(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 chan_line,
    UINT32 dimm_select_arr[])
{
  return (MrcCall->MrcRightShift64(chan_line, 1) & (~( (UINT64) get_x_bit_mask(MrcCall, dimm_select_arr[0]-6) ))) | (chan_line & ( (UINT64) get_x_bit_mask(MrcCall, dimm_select_arr[0]-6) ));
}

static
UINT32
get_physical_dimm_select (UINT32 dimm_select, UINT32 selected_mad_intra_chnl)
{
  return (dimm_select ^ get_dimm_l_select(selected_mad_intra_chnl));
}

static
UINT32
get_ch_width( UINT32 MAD_INTER_CHNL )
{
  UINT32 ch_width = (UINT32) ((MAD_INTER_CHNL >> 27) & 3); // 00-x16, 01-x32, 2-x64
  return (UINT32) (ch_width + 1) << 4;
}
static
UINT32
extended_bank_group_hasher(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT32 sys_addr_hash_bit_pos,
    UINT32 input_bg_value,
    UINT64 dimm_line)
{
  return (input_bg_value ^ (MrcCall->MrcRightShift64(dimm_line, (sys_addr_hash_bit_pos-CACHE_LINE_ADDR_START_BIT_POS)) & 1));
}
static
VOID
addr_decode_map_parse (
    const MRC_ADDR_FUNCTION *MrcCall,
    INT32* config_arr,
    INT32 config_case,
    BOOLEAN second_round
    )
{
  // gets the configuration case we need from the addr_decode_map.csv file
  UINT32 i;
  INT32 curr_cfg;
  for (i = 0; i < MAXFLDSIZE; i++) {
    curr_cfg = ConfigArrayConst[i][0];
    if (second_round) {
      if (curr_cfg == config_case) { // if match the configuration case we are lookink for
        break;
      }
    } else {
      if (config_case <= 10000) {
        if ((curr_cfg >= config_case) && (curr_cfg < (config_case + 1))) { // if almost match the configuration case we are lookink for
          break;
        }
      } else {
        // original is float
        if ((curr_cfg >= config_case) && (curr_cfg < (config_case + 10))) { // if almost match the configuration case we are lookink for
          break;
        }
      }
    }
  }
  //Found a configuration - copying it to the config array
  MrcCall->MrcCopyMem ((UINT8 *)config_arr, (UINT8 *)&ConfigArrayConst[i], sizeof (ConfigArrayConst[i]));

}

static
VOID
enhanced_mode_map_parse (
    const MRC_ADDR_FUNCTION *MrcCall,
    INT32 * enh_config_arr,
    INT32 config_case,
    BOOLEAN second_round
    )
{
  UINT32 i = 0;

  // gets the configuration case we need
  for (i = 0; i < MAXFLDSIZE; i++) {
    if (second_round) {
      if (EnhConfigArrayConst[i][0] == config_case) { // if match the configuration case we are looking for
        break;
      }
    } else {
      if (config_case <= 10000) {
        if ((EnhConfigArrayConst[i][0] >= config_case) && (EnhConfigArrayConst[i][0] < (config_case + 1))) { // if almost match the configuration case we are lookink for
          break;
        }
      } else {
        // original is float
        if ((EnhConfigArrayConst[i][0] >= config_case) && (EnhConfigArrayConst[i][0] < (config_case + 10))) { // if almost match the configuration case we are lookink for
          break;
        }
      }
    }
  }
  //Found a configuration - copying it to the config array
  MrcCall->MrcCopyMem ((UINT8 *)enh_config_arr, (UINT8 *)&EnhConfigArrayConst[i], sizeof (EnhConfigArrayConst[i]));

}

// gives back the configuration case
static
INT32
get_the_config_case_num (
    UINT32 ddr_type,
    UINT32 chnl_width, UINT32 num_of_dimms, UINT32 num_of_ranks,
    UINT32 dimm_x_num_of_ranks, UINT32 dimm_l_width,
    BOOLEAN same_dimm_width_or_one_dimm, BOOLEAN is_dram_density_8Gb,
    BOOLEAN zone0,
    UINT32 lpddr5_bg_mode
    )
{
  INT32 config_case = 0;

  switch(ddr_type)
  {
    case(0) : // DDR4
      {
        if ( same_dimm_width_or_one_dimm )
        {
          if ( dimm_l_width == 8 ) // DDR x8
          {
            if ( num_of_dimms == 1 )
            {
              switch( num_of_ranks )
              {
                case 1  :   config_case = 14; return config_case;
                case 2  :   config_case = 16; return config_case;
              }
            }
            if ( num_of_dimms == 2 )
            {
              switch( num_of_ranks )
              {
                case 2  :   config_case = zone0 ? 15 : 14; return config_case;
                case 3  :   config_case = zone0 ? 18 : ((dimm_x_num_of_ranks == 2) ? 16 : 14); return config_case;
                case 4  :   config_case = zone0 ? 17 : 16; return config_case;
              }
            }
          }
          else // DDR4 x16
          {
            if ( num_of_dimms == 1 )
            {
              switch( num_of_ranks )
              {
                case 1  :   config_case = 19; return config_case;
                case 2  :   config_case = 21; return config_case;
              }
            }
            if ( num_of_dimms == 2 )
            {
              switch( num_of_ranks )
              {
                case 2  :   config_case = zone0 ? 20 : 19; return config_case;
                case 3  :   config_case = zone0 ? 23 : ((dimm_x_num_of_ranks == 2) ? 21 : 19); return config_case;
                case 4  :   config_case = zone0 ? 22 : 21; return config_case;
              }
            }

          }
        }
        else // DDR4 x8 & x16
        {
          switch( num_of_ranks )
          {
            case 2  :   config_case = zone0 ? 24 : ( (dimm_l_width==8) ? 14 : 19); return config_case;
            case 3  :   if ( dimm_x_num_of_ranks == 2 )
                        {
                          if ( dimm_l_width == 16 ) {config_case = zone0 ? 27 : 21;}
                          else {config_case = zone0 ? 26 : 16;}
                        }
                        else
                        {
                          if ( dimm_l_width == 16 ) {config_case = zone0 ? 26 : 19;}
                          else {config_case = zone0 ? 27 : 14;}
                        }
                        return config_case;
            case 4  :   config_case = zone0 ? 25 : ( (dimm_l_width==8) ? 16 : 21 ); return config_case;
          }

        }
        break;
      }
    case(1): // DDR5
      {


        if (dimm_l_width == 8)
        {
          // Incase of 3 ranks in 2 dimms - we will go to the relevant dimm (L/S)
          // and from there will act as 2 dimm 1/2ranks configuration
          if (num_of_dimms == 2 && num_of_ranks == 3) {
            num_of_ranks = dimm_x_num_of_ranks*2;
          }
          if (num_of_dimms == 1)
          {
            switch(num_of_ranks)
            {
              case 1 : config_case = (is_dram_density_8Gb) ? 10911 : 10921; return config_case;
              case 2 : config_case = (is_dram_density_8Gb) ? 10931: 10941; return config_case;
            }
          } else { // 2 dimms
            switch(num_of_ranks)
            {
              case 2 : config_case = (is_dram_density_8Gb) ? (zone0 ? 10991 : 10911) :(zone0 ? 10951 : 10921); return config_case;
              case 4 : config_case = (is_dram_density_8Gb) ? (zone0 ? 11011: 10931) :(zone0 ? 10971 : 10941); return config_case;
            }
          }
        }
        else if (dimm_l_width == 16)
        {

          if (num_of_dimms == 2 && num_of_ranks == 3) {
            num_of_ranks = dimm_x_num_of_ranks*2;
          }

          if ( num_of_dimms == 1 )
          {
            switch( num_of_ranks )
            {
              case 1 : config_case = (is_dram_density_8Gb) ? 10912 : 10922; return config_case;
              case 2 : config_case = (is_dram_density_8Gb) ? 10932: 10942; return config_case;
            }
          } else { // 2 dimms
            switch(num_of_ranks)
            {
              case 2 : config_case = (is_dram_density_8Gb) ? (zone0 ? 10992 : 10912) : (zone0 ? 10952 : 10922); return config_case;
              case 4 : config_case = (is_dram_density_8Gb) ? (zone0 ? 11012 : 10932): (zone0 ? 10972 : 10942); return config_case;
            }
          }
        }
        break;
      }
    case(3) :
      { // LPDDR4
        if (chnl_width == 32)
        {
          if (num_of_dimms == 1) {
            switch( num_of_ranks )
            {
              case 1  :   config_case = 55; return config_case;
              case 2  :   config_case = 56; return config_case;
              case 3  :   config_case = 74; return config_case;
              case 4  :   config_case = 74; return config_case;
            }
          } // End dimms = 1
          if ( num_of_dimms == 2 ){
            switch( num_of_ranks )
            {
              case 2  :   config_case = zone0 ? 57 : 55; return config_case;
              case 4  :   config_case = zone0 ? 58 : 56; return config_case;
              case 6  :   config_case = zone0 ? 75 : 74; return config_case;
              case 8  :   config_case = zone0 ? 75 : 74; return config_case;
            }
          } // end dimms = 2
        }
        else if (chnl_width == 16)
        {
          if (num_of_dimms == 1) {
            switch( num_of_ranks )
            {
              case 1  :   config_case = 61; return config_case;
              case 2  :   config_case = 62; return config_case;
            }
          } // End dimms = 1
          if (num_of_dimms == 2){
            switch (num_of_ranks)
            {
              case 2  :   config_case = 59; return config_case;
              case 4  :   config_case = 60; return config_case;
              case 8  :   config_case = 73; return config_case;
            }
            }
          }
          break;
      } // End LPDDR4
    case(2) :
      { // LPDDR5


        if (num_of_dimms == 1) {
          switch( num_of_ranks )
          {
            case 1  :
              if (lpddr5_bg_mode == 0 || lpddr5_bg_mode == 2 )
              {
                config_case = 47; return config_case;
              }
              else
              {
                config_case =43; return config_case;
              }
            case 2  :
              if (lpddr5_bg_mode == 0 || lpddr5_bg_mode == 2)
              {
                config_case = 48; return config_case;
              }
              else
              {
                config_case = 44; return config_case;
              }
          }
        }
        if (num_of_dimms == 2){
          switch (num_of_ranks)
          {
            case 2	:
              if (lpddr5_bg_mode == 0 || lpddr5_bg_mode == 2)
              {
                config_case = 49; return config_case;
              }
              else
              {
                config_case = 41; return config_case;
              }
            case 4  :
              if (lpddr5_bg_mode == 0 || lpddr5_bg_mode == 2)
              {
                config_case = 50; return config_case;
              }
              else
              {
                config_case = 42; return config_case;
              }
          }
        }
        break;
      } // End LPDDR5
  } // endcase
  return config_case;
}

static
UINT64
adjust_address_for_remap(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT32 tolud,
    UINT64 remap_base,
    UINT64 remap_limit,
    BOOLEAN *p_is_tcm,
    UINT64 remap_addr)
{
  UINT64 gap_limit;


  gap_limit = calculate_gap_top(tolud, get_remap_size(remap_limit, remap_base));

  // check for address falling in remap region
  //
  if(is_Sys_Addr_in_Remap_Region(remap_addr, remap_base, remap_limit)) // b4194941 - REMAP_LIMIT is now inclusive
  {
    // The address hit the remap region, so remap the address from the remap
    // source region (REMAP_BASE to REMAP_LIMIT) to the remap target region
    // (TOLUD to size-of-gap).
    //
    remap_addr -= remap_base;
    remap_addr += ((UINT32) tolud);

  }
  // check for address falling in the ME STOLEN region, when TOM is less than 4GB.
  //    else if ( (remap_addr < tom) && (remap_addr >  gap_limit) && (sys_in_gb(tom) <= 4)  )
  //    {
  //        if (*p_is_tcm)
  //        {
  //            if( 1 )
  //                DEBUG((DEBUG_INFO,"\nDEBUG:   Address was a TCM transaction "
  //                    "that landed in the ME STOLEN region.\n"
  //                    "TCM = FALSE, and should be TRUE"
  //                    "TOM = 0x%010llx (~%.3f GB)\n",
  //                    tom, sys_in_gb(tom) );
  //        }
  //    }
  // check for address falling in MMIO gap created by remap region
  else if (is_Sys_Addr_in_MMIO_Region(remap_addr, tolud, gap_limit)) // b4194941
  {
    // transaction to sys_addr should not have been TCM.
    *p_is_tcm = FALSE;

    return 0;
  }
  else
  { //no remap
  }

  return remap_addr;
}

static
UINT32
calculate_stacked_slice_select(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 remap_line,
    UINT32 MAD_INTER_CHNL)
{
  UINT32 chan_select;

  chan_select = (remap_line < get_stacked_memory_size_as_cl(MrcCall, MAD_INTER_CHNL)) ? 0 : 1;

  if (get_stacked_mode_ch1(MAD_INTER_CHNL)) {
    chan_select ^= 1;
  }
  return chan_select;
}

static
UINT32
calculate_stacked_slice_line_addr(const MRC_ADDR_FUNCTION *MrcCall, UINT64 remap_line, UINT32 MAD_INTER_CHNL)
{
  UINT32 slice_select;
  UINT64 slice_line;

  slice_select = (remap_line < get_stacked_memory_size_as_cl(MrcCall, MAD_INTER_CHNL)) ? 0 : 1;
  slice_line = remap_line;

  // If this is channel 1 in stacked mode, then we need to subtract out the channel size (clear
  // the stacked mode bit)
  //
  if (slice_select == 1) {
    slice_line = slice_line - get_stacked_memory_size_as_cl(MrcCall, MAD_INTER_CHNL);
  }

  return (UINT32)slice_line;
}

static
UINT32 calculate_slice_select(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 remap_line,
    UINT32 CHANNEL_HASH)
{

  UINT32 chan_select;
  UINT32 hash_mask;
  UINT64 hash_line;


  hash_mask = get_channel_hash_mask(CHANNEL_HASH);
  hash_line = (UINT32) get_channel_hash_line(remap_line, hash_mask); // get the bits to XOR for the hash


  // Produce chan_select by XORing together all of the bits of hash_line.
  chan_select = get_select_with_hashen(MrcCall, hash_line);
  return chan_select;
}

static
UINT32 calculate_slice_line_addr(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 remap_line,
    UINT32 CHANNEL_HASH)
{

  UINT64 chan_line;
  UINT32 hash_lsb_mask_bit;


  hash_lsb_mask_bit = get_ch_hash_lsb_mask_bit(CHANNEL_HASH);

  // sys_addr in hash_lsb_mask_bit index, will be remove from sys_addr
  // and the highest bits will shift right in 1 bit.
  chan_line = get_channel_line_with_hashen(MrcCall, remap_line, hash_lsb_mask_bit);
  return (UINT32)chan_line;
}

static
UINT32
calc_rightshift_mod(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 Data,
    UINT32 ShiftBits,
    UINT32 mod)
{
  UINT64 DataDiv;
  Data = MrcCall->MrcRightShift64(Data, ShiftBits);
  DataDiv = MrcCall->MrcDivU64x64(Data, mod, NULL);
  DataDiv = MrcCall->MrcMultU64x32(DataDiv, mod);
  Data = Data - DataDiv;
  return (UINT32) Data;
}

static
UINT32
calc_rightshift_div(
    const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 Data,
    UINT32 ShiftBits,
    UINT32 Div)
{
  Data = MrcCall->MrcRightShift64(Data, ShiftBits);
  Data = MrcCall->MrcDivU64x64(Data, Div, NULL);
  return (UINT32) Data;
}

// This function decode ebh_XaB bit, in 3 cases:
// 1) BG mode - DDR4
// 2) BG mode - LPDDR5
// 3) Bank mode - default configoration
static
VOID
decode_ebh_XaB(
    IN const MRC_ADDR_FUNCTION *MrcCall,
    UINT32 bankBit,
    UINT64 chan_line,
    UINT32 index,
    UINT32 select_arr[],
    UINT32 xor_ebh[],
    UINT32 option)
{
  UINT32 xorBit;
  switch (option)
  {
    //bg mode - ddr4
    case 0:
      DEBUG ((DEBUG_INFO, "\nDEBUG:   bg_select_arr[%0d] = %0d", index, select_arr[index] ));
      DEBUG ((DEBUG_INFO, "\nDEBUG:   XaB2 = "));
      for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
        if (XaB[2][xorBit] == 0) continue;
        (xor_ebh)[xorBit] = (MrcCall->MrcRightShift64(chan_line, XaB[2][xorBit]) & 1);
        (select_arr)[index] = (select_arr)[index] ^ (xor_ebh)[xorBit];

        DEBUG ((DEBUG_INFO, " %0d %a", (xor_ebh)[xorBit], (xorBit == XaB_NUM_OF_BITS-1) ? "" : "^ "));
      }

      break;
      //bg mode - lp5
    case 2:
      for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
        if (XaB[2][xorBit] == 0) continue;
        (xor_ebh)[xorBit] = (MrcCall->MrcRightShift64(chan_line, XaB[2][xorBit]) & 1);
        (select_arr)[0] = (select_arr)[0] ^ (xor_ebh)[xorBit];

        DEBUG ((DEBUG_INFO, " %0d %a", (xor_ebh)[index], (index == XaB_NUM_OF_BITS-1) ? "" : "^ "));
      }
      break;
      //bank mode - default
    case 3:
      DEBUG ((DEBUG_INFO, "\nDEBUG:   XaB%0d = ",bankBit));
      for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
        if (XaB[bankBit][xorBit] == 0) continue;
        xor_ebh[xorBit] = (MrcCall->MrcRightShift64(chan_line, XaB[bankBit][xorBit]) & 1);
        select_arr[bankBit] = select_arr[bankBit] ^ xor_ebh[xorBit];

        DEBUG ((DEBUG_INFO, " %0d %a", xor_ebh[xorBit], (xorBit == XaB_NUM_OF_BITS-1) ? "" : "^ "));
      }
      break;
  }

}

// This function decode ebh_XbB bit cases:
            // 1) LPDDR5 BG mode
            // 2) LPDDR4/LPDDR5 bank mode
UINT32 decode_ebh_XbB(
    IN const MRC_ADDR_FUNCTION *MrcCall,
    UINT32 bankBit,
    UINT64 chan_line,
    UINT32 dlnor)

{
  UINT32 xor_ebh[DDR5_EBH_BG_NUM_OF_BITS];						   // storing temporary results for debug printing
  UINT32 result;
  switch (bankBit)
  {
    case 0:
      xor_ebh[0] = MrcCall->MrcRightShift64(chan_line, XbB_lp4[bankBit][0]) & (XbB_lp4[bankBit][0] == 0 ? 0 : 1);
      xor_ebh[1] = MrcCall->MrcRightShift64(chan_line, XbB_lp4[bankBit][1]) & (XbB_lp4[bankBit][1] == 0 ? 0 : 1) & ( dlnor == 0 ? 1 : 0);
      result =  xor_ebh[0]^xor_ebh[1];

      DEBUG ((DEBUG_INFO, " %0d ^ %0d", xor_ebh[0], xor_ebh[1]));
      return result;
      break;
    case 1:
      xor_ebh[0] = MrcCall->MrcRightShift64(chan_line, XbB_lp4[bankBit][0]) & (XbB_lp4[bankBit][0] == 0 ? 0 : 1) & (dlnor<2 ? 1 : 0);
      xor_ebh[1] = MrcCall->MrcRightShift64(chan_line, XbB_lp4[bankBit][1]) & (XbB_lp4[bankBit][1] == 0 ? 0 : 1) & (dlnor == 0 ? 1 : 0);
      result = xor_ebh[0]^xor_ebh[1];
      DEBUG ((DEBUG_INFO, " %0d ^ %0d", xor_ebh[0], xor_ebh[1]));
      return result;
      break;
    default:
      xor_ebh[0] = MrcCall->MrcRightShift64(chan_line, XbB_lp4[bankBit][0]) & (XbB_lp4[bankBit][0] == 0 ? 0 : 1) & (dlnor<2 ? 1 : 0);
      //if LP5 bg mode
      result = xor_ebh[0];
      return result;
      break;


  }
}

/**
  Address decode function
  Converts system address to DRAM address

  @param[in]      MrcCall            - pointer to MRC_ADDR_FUNCTION struct
  @param[in]      sys_addr           - the 39-bit system address to convert
  @param[in, out] p_is_tcm           - is the transaction to sys_addr "traffic class for the manageability engine"
  @param[in]      TOLUD              - memory register
  @param[in]      REMAP_BASE         - memory register
  @param[in]      REMAP_LIMIT        - memory register
  @param[in]      TOM                - memory register
  @param[in]      CHANNEL_HASH       - memory register
  @param[in]      CHANNEL_EHASH      - memory register
  @param[in]      MAD_INTER_CHNL     - memory register
  @param[in]      MAD_INTRA_CHNL_ch0 - memory register
  @param[in]      MAD_INTRA_CHNL_ch1 - memory register
  @param[in]      MAD_DIMM_ch0       - memory register
  @param[in]      MAD_DIMM_ch1       - memory register
  @param[out]     p_chan             - channel sys_addr decodes to
  @param[out]     p_dimm             - DIMM sys_addr decodes to
  @param[out]     p_rank             - rank sys_addr decodes to
  @param[out]     p_bg               - bank group sys_addr decodes to
  @param[out]     p_bank             - bank sys_addr decodes to
  @param[out]     p_row              - row sys_addr decodes to
  @param[out]     p_col              - column sys_addr decodes to.

  @retval TRUE if successful.

 **/
static
BOOLEAN
cnl_mc_addr_decode(
    IN const MRC_ADDR_FUNCTION *MrcCall,
    IN       UINT64       sys_addr,
    IN OUT   BOOLEAN      *p_is_tcm,
    IN       UINT32       MAD_MC_HASH,
    IN       UINT64       TOM,
    IN       UINT32       CHANNEL_HASH,
    IN       UINT32       CHANNEL_EHASH,
    IN       UINT32       MAD_INTER_CHNL,
    IN       UINT32       MAD_INTRA_CHNL_ch0,
    IN       UINT32       MAD_INTRA_CHNL_ch1,
    IN       UINT32       MAD_DIMM_ch0,
    IN       UINT32       MAD_DIMM_ch1,
    IN       UINT64       SC_GS_CFG,
    IN       UINT32       is_print,
    OUT      UINT32       *p_chan,
    OUT      UINT32       *p_dimm,
    OUT      UINT32       *p_rank,
    OUT      UINT32       *p_bg,
    OUT      UINT32       *p_bank,
    OUT      UINT32       *p_row,
    OUT      UINT32       *p_col,
    OUT      INT32        *p_config_case_num
    )
{
  //Fields in global
  UINT32  ebh_enable;                     // Enhanced Bank Enable
  UINT32  XaB_enable;                     // xor XaB enable
  UINT32  XbB_enable;                     // xor XbB_lp4 enablea
  UINT32  ddr_type;                           // DDR4=0; DDR3=1; LPDDR3=2; LPDDR4=3, WIO2=4;
  UINT32  enhanced_ch_mode;                   // Enabled = 0; Disabled = 1;
  UINT32  bg0_field_value;                // Enabled = 0; Disabled = 1;
  UINT32  bg0_bit_select;                // Enabled = 0; Disabled = 1;
  UINT32  mad_dimm_l[2];                 // Make a MAD_DIMM array for easy access
  UINT32  mad_intra_chnl_l[2];           // Make a MAD_INTRA_CHNL array for easy access
  UINT64  ch_s_size;                     // chanel s_size adjusted to cl address
  UINT32  ch_l_map;                      // Virtual channel L mapping (0 - CH 0; 1 - CH 1)
  UINT32  chan_l_mad_dimm;               // channel L's MAD_DIMM register
  UINT64  tom;                           // Top Of Memory (system address)
  UINT64  tom_in_cl;                     // Top Of Memory (cl address)
  UINT64  calculated_tom;                // TOM calculated from channel sizes
  UINT32  ch_width;                      // New field to determin the Channel Width.
  UINT32  lpddr5_bg_mode;                // get LPDDR5 BG mode

  // NOTE: Variables that are needed for config_case
  UINT64 dimm_l_size;                   // sizes of the DIMMs on the channel in cache-lines
  UINT64 dimm_s_size;
  UINT32 chnl_num_of_dimms;
  UINT32 chnl_num_of_ranks;

  BOOLEAN same_dimm_width_or_one_dimm;
  BOOLEAN high_order_rank_interleave;                  // Not supported in SKL for now.
  BOOLEAN enhanced_interleave_mode;

  // NOTE: Variables needed to retrieve Memory Config
  UINT32 dimm_size;
  UINT32 num_of_ranks;                               // number of ranks on selected DIMM
  UINT32 dimm_width;
  UINT32 D8Gb;                                    // 0-> NOT 8Gb; 1-> 8Gb
  // End of global data

  // These vars are used to get the configuration case
  UINT32 hash_lsb_mask_bit;                // LsbMaskBit field from CHANNEL_HASH register
  UINT32 hash_mask;                        // channel hash mask from CHANNEL_HASH register

  UINT32 slice_zone1_start;                // Zone1 start separate between MC0/1 Interleave zones
  UINT32 slice_stacked_mode;               // Indicated if stacked mode is on (MC wise)
  UINT32 slice_hash_enable;                // Indicating if HASHing is required
  UINT32 slice_high_bits_h_gb = 0;              // Holds system address high bits address[37:30]
  UINT32 slice_high_bits_gb = 0;              // Holds system address high bits address[37:30]
  UINT64 sliced_address;         // Holds the address after slice is being done
  UINT64 sys_addr_mod = 0;;               // Hold the value of the system address after removing the HPA bit

  UINT64 remap_line;             // cache-line address

  UINT32 hash_line;                        // lower bits of remap_line with hash_mask applied

  UINT32 i;                                // loop counter
  UINT32 bankBit;
  UINT32 bgBit;
  UINT32 xorBit;
  UINT32 nor_idx;

  UINT64 chan_line;              // channel address space (cache-line)

  UINT32 chan_select;                      // 0 = Channel L, 1 = Channel S

  UINT32 selected_mad_dimm;           // MAD_DIMM for the selected channel
  UINT32 selected_mad_intra_chnl;     // MAD_INTRA_CHNL for the selected channel

  UINT64 dimm_line=0;            // DIMM address space (cache-line)
  UINT32 dimm_select=0;                    // 0 = DIMM L, 1 = DIMM S
  UINT32 dimm_select_arr[DIMM_NUM_BITS];
  UINT32 rank_select_arr[RANK_NUM_BITS];
  UINT32 bg_select_arr[BG_NUM_BITS];
  UINT32 bank_select_arr[BANK_NUM_BITS];
  UINT32 row_select_arr[ROW_NUM_BITS];
  UINT32 col_select_arr[COL_NUM_BITS];

  UINT32 xor_result;
  UINT32 rank_size;                                   // size of selected DIMM (cache-lines)
  UINT32 dlnor;
  UINT32 width_idx;
  UINT32 nor_g2;                                                                               // DDR4 EBH
  UINT32 nor_1;                                                                                        // DDR4 EBH
  UINT32 xor_ebh[DDR5_EBH_BG_NUM_OF_BITS];                                             // storing temporary results for debug prINT32ing

  // temporary values for bit masking and shifting
  UINT32 rank_bit_shift;

  // virtual arrays to get the inputs from the addr_decode_maps.csv table, according to the configuration
  INT32 config_arr[DRAM_ADDR_SIZE+1];

  // virtual arrays to get the inputs from the enhanced_mode_map.csv table, according to the configuration
  INT32 enh_config_arr[ENHANCED_BITS_SIZE+1];

  INT32 config_case_num;
  UINT32 config_case_num_int_for_switch; // virtual var
  UINT32 converter_step; // virtual var

  // shifters for bit select (from chnl_addr)according to the address decode map
  INT32 dimm_shifter[DIMM_NUM_BITS];
  INT32 rank_shifter[RANK_NUM_BITS];
  INT32 bg_shifter[BG_NUM_BITS];
  INT32 bank_shifter[BANK_NUM_BITS];
  INT32 row_shifter[ROW_NUM_BITS];
  INT32 col_shifter[COL_NUM_BITS];

  INT32 dimm_enhanced_shifter[DIMM_NUM_BITS];
  INT32 rank_enhanced_shifter[RANK_NUM_BITS];
  INT32 bg_enhanced_shifter[BG_NUM_BITS];
  INT32 bank_enhanced_shifter[BANK_NUM_BITS];
  INT32 col_enhanced_shifter[COL_NUM_BITS];

  ebh_enable = 0;                     // Enhanced Bank Enable
  XaB_enable = 0;                     // xor XaB enable
  XbB_enable = 0;                     // xor XbB_lp4 enable

  MrcCall->MrcSetMem ((UINT8 *)mad_dimm_l, sizeof (mad_dimm_l), 0);
  MrcCall->MrcSetMem ((UINT8 *)mad_intra_chnl_l, sizeof (mad_intra_chnl_l), 0);
  MrcCall->MrcSetMem ((UINT8 *)config_arr, sizeof (config_arr), 0);
  MrcCall->MrcSetMem ((UINT8 *)enh_config_arr, sizeof (enh_config_arr), 0);
  MrcCall->MrcSetMem ((UINT8 *)dimm_shifter, sizeof (dimm_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)rank_shifter, sizeof (rank_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)bg_shifter, sizeof (bg_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)bank_shifter, sizeof (bank_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)row_shifter, sizeof (row_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)col_shifter, sizeof (col_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)dimm_enhanced_shifter, sizeof (dimm_enhanced_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)rank_enhanced_shifter, sizeof (rank_enhanced_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)bg_enhanced_shifter, sizeof (bg_enhanced_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)bank_enhanced_shifter, sizeof (bank_enhanced_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)col_enhanced_shifter, sizeof (col_enhanced_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)dimm_select_arr, sizeof (dimm_select_arr), 0);
  MrcCall->MrcSetMem ((UINT8 *)rank_select_arr, sizeof (rank_select_arr), 0);
  MrcCall->MrcSetMem ((UINT8 *)bg_select_arr, sizeof (bg_select_arr), 0);
  MrcCall->MrcSetMem ((UINT8 *)bank_select_arr, sizeof (bank_select_arr), 0);
  MrcCall->MrcSetMem ((UINT8 *)row_select_arr, sizeof (row_select_arr), 0);
  MrcCall->MrcSetMem ((UINT8 *)col_select_arr, sizeof (col_select_arr), 0);
  MrcCall->MrcSetMem ((UINT8 *)xor_ebh, sizeof (xor_ebh), 0);

  dimm_line = 0;
  dimm_select = 0;

  ddr_type = get_ddr_type (MAD_INTER_CHNL);
  enhanced_ch_mode = get_enhanced_ch_mode (MAD_INTER_CHNL);

  ch_width = get_ch_width(MAD_INTER_CHNL);
  lpddr5_bg_mode   = get_lpddr5_bg_mode(MrcCall, SC_GS_CFG);

  DEBUG ((DEBUG_INFO, "enhanced_ch_mode:%u\n", enhanced_ch_mode));

  mad_dimm_l[0]       = MAD_DIMM_ch0;
  mad_dimm_l[1]       = MAD_DIMM_ch1;
  mad_intra_chnl_l[0] = MAD_INTRA_CHNL_ch0;
  mad_intra_chnl_l[1] = MAD_INTRA_CHNL_ch1;

  // Note: Since MC decoder topology can be different than decoder topology
  // (i.e 12GB MC topology is mapped to 16GB in decoder topology)
  // We will have two global variables that hold the information:
  // calculated_tom - for the TOM being calculated from decoder registers (below)
  // actual_tom - the actual dram size.
  // TOM is not directly available in a register. It will be computed by
  // finding the channel index of channel L, then adding the two DIMM
  // capacities of that channel together. Technically, this is only needed to
  // check that the system address is not beyond the amount of memory
  // available.
  ch_s_size = get_ch_s_size_as_cl( MrcCall, MAD_INTER_CHNL );
  ch_l_map  = get_ch_l_map(MAD_INTER_CHNL);
  chan_l_mad_dimm = mad_dimm_l[ch_l_map];
  calculated_tom  = cl_to_sys(MrcCall, get_dimm_l_size_as_cl(MrcCall, chan_l_mad_dimm));
  calculated_tom += cl_to_sys(MrcCall, get_dimm_s_size_as_cl(MrcCall, chan_l_mad_dimm));
  calculated_tom += cl_to_sys(MrcCall, ch_s_size);
  tom = TOM;
  tom_in_cl = MrcCall->MrcRightShift64(tom, 6);

  // determine if we are in EBH mode enable - MAD_DIMM_ch0/ch1 ebh_decode filed must be the same
  ebh_enable = get_ebh_mode(MAD_DIMM_ch0);
  // FIXME: hlalithk (17ww50): Enable Enhanced Bank Hashing for LPDDR5
  if (ebh_enable){
    XaB_enable = ebh_enable & 1;
    XbB_enable =(ebh_enable >> 1) & 1;
    DEBUG((DEBUG_INFO, "\nDEBUG:	EBH mode enabled."));
    DEBUG((DEBUG_INFO, "\nDEBUG:	EBH mode bits = 0x%x",ebh_enable));
    if (ddr_type != 1) {
      DEBUG((DEBUG_INFO, "\nDEBUG:    XaB_enable = %0d", XaB_enable));
      DEBUG((DEBUG_INFO, "\nDEBUG:    XbB_enable= %0d", XbB_enable));
      for ( bankBit = 0; bankBit < BANK_NUM_BITS; bankBit++){
        DEBUG((DEBUG_INFO, "\nDEBUG:	XaB%0d = ",bankBit));
        for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
          if (ddr_type != 1)
            DEBUG((DEBUG_INFO, "0%d %s", XaB[bankBit][xorBit], xorBit == (XaB_NUM_OF_BITS-1) ? "": "^ "));
        }
      }
      if(ddr_type == 0){
        DEBUG((DEBUG_INFO, "\n\nDEBUG:	DDR4 XbB:"));
        for ( bankBit = 0; bankBit < BANK_NUM_BITS; bankBit++){
          DEBUG((DEBUG_INFO, "\nDEBUG:	XbB%0d(x8) = [",bankBit));
          for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
            DEBUG((DEBUG_INFO, "%0d %s", XbB_ddr4[0][bankBit][xorBit], xorBit == (XaB_NUM_OF_BITS-1) ? "]":", "));
          }
          DEBUG((DEBUG_INFO, "\nDEBUG:	XbB%0d(x16) = [",bankBit));
          for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
            DEBUG((DEBUG_INFO, "%0d %s", XbB_ddr4[1][bankBit][xorBit], xorBit == (XaB_NUM_OF_BITS-1) ? "]": ", "));
          }
        }
      } else if (ddr_type == 3 || ddr_type == 2) {
        DEBUG((DEBUG_INFO, "\n\nDEBUG:	LP4/LP5:"));
        for ( bankBit = 0; bankBit < BANK_NUM_BITS; bankBit++){
          DEBUG((DEBUG_INFO, "\nDEBUG:	XbB%0d = [",bankBit));
          for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
            DEBUG((DEBUG_INFO, "%0d %s", XbB_lp4[bankBit][xorBit], xorBit == (XaB_NUM_OF_BITS-1) ? "]":", "));
          }
        }

      }
    } else {
      DEBUG((DEBUG_INFO, "\n\nDEBUG:	DDR5:."));
      for ( width_idx = 0; width_idx < 2; width_idx++){
        for ( nor_idx = 0; nor_idx < 2; nor_idx++){
          for ( bankBit = 0; bankBit < BANK_NUM_BITS-1; bankBit++){
            DEBUG((DEBUG_INFO, "\nDEBUG:	ddr5_ebh_bank[%s][%s][%0d] = {",width_idx ? "x8" : "x16",nor_idx ? "2 Ranks" : "1 Rank", bankBit));
            for(xorBit = 0; xorBit < DDR5_EBH_BANK_NUM_OF_BITS; xorBit++){
              DEBUG((DEBUG_INFO, "%0d %s", ddr5_ebh_bank[width_idx][nor_idx][bankBit][xorBit], xorBit == (DDR5_EBH_BANK_NUM_OF_BITS-1) ? "}":", "));
            }
          }
        }
      }
      for ( width_idx = 0; width_idx < 2; width_idx++){
        for ( nor_idx = 0; nor_idx < 2; nor_idx++){
          for ( bgBit = 0; bgBit < BANK_NUM_BITS-1; bgBit++){
            DEBUG((DEBUG_INFO, "\nDEBUG:	ddr5_ebh_bg[%s][%s][%0d] = {",width_idx ? "x8" : "x16",nor_idx ? "2 Ranks" : "1 Rank", bgBit+1));
            for(xorBit = 0; xorBit < DDR5_EBH_BG_NUM_OF_BITS; xorBit++){
              DEBUG((DEBUG_INFO, "%0d %s", ddr5_ebh_bg[width_idx][nor_idx][bgBit][xorBit], xorBit == (DDR5_EBH_BG_NUM_OF_BITS-1) ? "}":", "));
            }
          }
        }
      }
    }
  }

  DEBUG ((DEBUG_INFO, "\n"));
  /////////////////////////////////////////////////////////////////
  ////        Slice Decoding
  ////////////////////////////////////////////////////////////////

  // MC0 and MC1 are being stacked on each other.
  // Using zone1_start twice bigger than the MC S
  //  +------0.5 TB------+
  //  |                  |
  //  .                  .
  //  .                  .
  //  .                  .
  //  |                  |
  //  |  unmapped        |
  //  |  space above     |
  //  |  DRAM            |
  //  |                  |
  //  +---Both MC Size---+  <-- Top Of Memory = L + S
  //  |                  |
  //  |                  |
  //  |                  |
  //  |      MC L        |
  //  |                  |
  //  |                  |
  //  +--- Stack size ---+  <-- Stack Size = ZONE1_START/2
  //  |                  |
  //  |                  |
  //  |                  |
  //  |      MC S        |
  //  |                  |
  //  |                  |
  //  +--------0x0-------+
  //
  slice_high_bits_h_gb = (UINT32) MrcCall->MrcRightShift64(sys_addr, 29);
  slice_high_bits_gb = slice_high_bits_h_gb >> 1;
  slice_zone1_start = get_mc_zone1_start(MAD_MC_HASH);
  slice_stacked_mode = get_mc_stacked_mode(MAD_MC_HASH);
  slice_hash_enable  = get_mc_hash_enable(MAD_MC_HASH);

  // Zone1
  if (((slice_high_bits_gb >= slice_zone1_start) && slice_hash_enable )|| slice_stacked_mode) {
    DEBUG((DEBUG_INFO, "\nDEBUG:  slice_zone1_start     = %x", slice_zone1_start));
    DEBUG((DEBUG_INFO, "\nDEBUG:  slice_high_bits_h_gb = %x", slice_high_bits_h_gb));

    // We need to calculat in 1 gb granularity for the case of slice_zone1_start = 1GB
    // right shift will give us 0 and address mapping will not affect.
    // So we are calculating in half gb ranularity and devide by 2 at the end
    sliced_address = slice_high_bits_h_gb - slice_zone1_start; // sliced_address := sys_addr[38:30] - zone1_start[8:1]

    sliced_address = (sliced_address << 29) | (sys_addr & 0x1FFFFFFFULL); // sliced_address := sliced_address[8:0]*sys_addr[29:0]
    sys_addr_mod = sliced_address;

    sliced_address = get_cacheline_from_addr(MrcCall, sliced_address);

  } else if (slice_hash_enable){ //Zone0

    DEBUG((DEBUG_INFO, "\nDEBUG:  MC Zone0 Hashing is enabled"));

    hash_lsb_mask_bit = get_mc_hash_lsb(MAD_MC_HASH);


    // sys_addr in hash_lsb_mask_bit index, will be remove from sys_addr
    // and the highest bits will shift right in 1 bit.
    sys_addr_mod   = get_sys_addr_with_hashen(MrcCall, sys_addr, hash_lsb_mask_bit);
    sliced_address = get_cacheline_from_addr(MrcCall, sys_addr_mod);


    DEBUG((DEBUG_INFO, "\nDEBUG:   system_address: 0x%010llx, sliced_address: 0x%010llx, "
          "hash_lsb_mask_bit: 0x%01x",
          sys_addr, sliced_address, hash_lsb_mask_bit));
  } else {
    DEBUG((DEBUG_INFO, "\nDEBUG:  Slice Hash Mode Disabled!"));
    sys_addr_mod = sys_addr;
    sliced_address = get_cacheline_from_addr(MrcCall, sys_addr);
  }
  // Cache-line address
  remap_line = sliced_address;

  DEBUG((DEBUG_INFO, "\nDEBUG:   sliced_address =0x%010llx, sys_addr=0x%09llx, sys_addr_mod=0x%0llx\n", remap_line, sys_addr, sys_addr_mod));

  if( get_stacked_mode(MAD_INTER_CHNL) ) {
    // In stacked mode, check that remapped address is below TOM.
    if (remap_line >= tom_in_cl)
    {
      return FALSE;
    }

  }

  // Channel Interleaving Zones
  //
  // Determine which range/zone the remapped system address falls into
  //
  //  +-----0.5 TB-----+
  //  |                |
  //  .                .
  //  .                .
  //  .                .
  //  |                |
  //  |  unmapped      |
  //  |  space above   |
  //  |  DRAM          |
  //  |                |
  //  +--Zone 1 Limit--+  <-- Top Of Memory = L + S
  //  |                |
  //  |  No Interleave |
  //  |    Channel L   |
  //  |                |
  //  +--Zone 0 Limit--+  <-- 2 * S
  //  |                |
  //  |                |
  //  |                |
  //  |  2-Way Intlv   |
  //  | Channels L & S |
  //  |                |
  //  |                |
  //  |                |
  //  |                |
  //  +-------0x0------+
  //
  if( is_cache_line_in_zone0 (MrcCall, remap_line, ch_s_size) ) // Zone 0
  {
    if( is_channel_hash_enabled(CHANNEL_HASH) )
    {
      chan_select = calculate_slice_select(MrcCall, remap_line, CHANNEL_HASH);
      chan_line = calculate_slice_line_addr(MrcCall, remap_line, CHANNEL_HASH);
    }
    else
    {
      chan_select = (UINT32) get_channel_select_with_hashdis(remap_line);
      chan_line   = get_channel_line_with_hashdis(MrcCall, remap_line);
    }
  }
  else if(remap_line < tom_in_cl) // Zone 1
  {
    chan_select = 0; // Channel L
    chan_line = remap_line - ch_s_size;
  }
  else // address was above memory capacity
  {
    return FALSE;
  }

  // obtain the physical channel index
  *p_chan = logical_to_physical_chan(MAD_INTER_CHNL, chan_select);
  selected_mad_dimm = mad_dimm_l[*p_chan];
  selected_mad_intra_chnl = mad_intra_chnl_l[*p_chan];

  //init_variables_for_determing_cfg_case(selected_mad_dimm, selected_mad_intra_chnl);
  // Find the DIMM sizes on our selected channel. adjust to cache-line granularity
  dimm_l_size = get_dimm_l_size_as_cl(MrcCall, selected_mad_dimm);
  dimm_s_size = get_dimm_s_size_as_cl(MrcCall, selected_mad_dimm);

  // Find number of ranks and dimms in the selected channel
  chnl_num_of_dimms = (dimm_l_size) ? ((dimm_s_size) ? 2 : 1) : 0;
  chnl_num_of_ranks = ((dimm_l_size) ? get_dimm_l_number_of_ranks(selected_mad_dimm) : 0) + ((dimm_s_size) ? get_dimm_s_number_of_ranks(selected_mad_dimm) : 0);

  // Find the dimm width chnl configuration
  same_dimm_width_or_one_dimm =   (dimm_s_size) ?
    ((get_dimm_l_width(selected_mad_dimm)==get_dimm_s_width(selected_mad_dimm)) ?
     TRUE : FALSE) : TRUE;

  // determine if we are doing high order rank interleave
  high_order_rank_interleave = get_high_order_intlv_mode(selected_mad_intra_chnl);

  // determine if we are doing Enhanced Interleave Mode (EIM) (XOR rank, bg & bank bits)
  enhanced_interleave_mode   = get_enhanced_intlv_mode(selected_mad_intra_chnl);


  // In ddr5 - when decoging 2d3r we will map to 2d2r or 2d4r depands on the dimm select
  // But this decode needs to be done only after dimm was selected.
  // In this section the dimm in not selected yet so we are tweeking the number of ranks.
  // Telling the function it is not 3 ranks so it will not map it to 1 dimm.
  config_case_num = get_the_config_case_num(ddr_type, ch_width, chnl_num_of_dimms, chnl_num_of_ranks,
      get_dimm_l_number_of_ranks(selected_mad_dimm),
      get_dimm_l_width(selected_mad_dimm),
      same_dimm_width_or_one_dimm,
      is_dl_device_density_8Gb(selected_mad_dimm),
      TRUE,
      lpddr5_bg_mode);

  if(1)

  config_case_num_int_for_switch = config_case_num;
  // calling the parsing method, to get the address decode map for the current configuration.
  // This is the first call, just to know the DIMM shifters
  addr_decode_map_parse(MrcCall, config_arr, config_case_num, FALSE);
  if ( enhanced_interleave_mode )
    enhanced_mode_map_parse(MrcCall, enh_config_arr, config_case_num, FALSE);

  //dimm_shifters
  for(i=0; i<DIMM_NUM_BITS; i++)
  {
    dimm_shifter[i] = ( config_arr[i+1] - 6 );
    if ( enhanced_interleave_mode )
    {
      dimm_enhanced_shifter[i] = ( enh_config_arr[i+1] - 6 );
    }
  }

  {
    DEBUG((DEBUG_INFO, "\nDEBUG:   high_order_rank_interleave=%s, "
          "enhanced_interleave_mode=%s, ",
          high_order_rank_interleave?L"TRUE":L"FALSE",
          enhanced_interleave_mode?L"TRUE":L"FALSE"));
  }

  // DIMM address calculation
        // DIMM and Rank interleaving enabled
        //  +------32 GB------+
        //  |                 |
        //  .                 .
        //  .                 .
        //  .                 .
        //  |                 |
        //  |  unmapped       |
        //  |  space above    |
        //  |  channel        |
        //  |  capacity       |
        //  |                 |
        //  +-- Zone 1 Limit--+  <-- dimm_l_size + dimm_s_size
        //  |                 |
        //  |  No Interleave  |
        //  |     DIMM L      |
        //  |                 |
        //  +-- Zone 0 Limit--+  <-- dimm_s_size * 2
        //  |                 |
        //  | 2-Way Interleave|
        //  |   DIMMs L & S   |
        //  |                 |
        //  +-------0x0-------+
        //
  if( is_cache_line_in_zone0(MrcCall, chan_line, dimm_s_size) ) // Range 0 limit = 2 * dimm_s_size
  {

          // Determine if the sub channel hash feature is being used
            if( is_channel_hash_enabled(CHANNEL_EHASH) ) // test enable bit
            {
                hash_lsb_mask_bit = get_ch_hash_lsb_mask_bit(CHANNEL_EHASH);
                hash_mask         = get_channel_hash_mask(CHANNEL_EHASH);
                hash_line         = (UINT32)get_channel_hash_line(chan_line, hash_mask); // get the bits to XOR for the hash

                dimm_select       = get_select_with_hashen(MrcCall, hash_line);

                DEBUG((DEBUG_INFO, "\nDEBUG:   chan_line before: 0x%010llx", chan_line));

                // sys_addr in hash_lsb_mask_bit index, will be remove from sys_addr
                // and the highest bits will shift right in 1 bit.
                dimm_line = get_dimm_line_with_hashen(MrcCall, chan_line, hash_lsb_mask_bit, dimm_select);
            }
            else {

              // 2-way DIMM interleave. Channel address is used to select DIMM according to the decode map.
              // In SKL DIMM_NUM_BITS==1. all the for loops are for the case there will be mor than 2 dimms.
              for(i=0; i<DIMM_NUM_BITS; i++)
              {
                // The Dimm shifter can be negative number
                dimm_select_arr[i] = (UINT32) ( MrcCall->MrcRightShift64(chan_line, dimm_shifter[i]) & 1);
                if ( enhanced_interleave_mode )
                {
                  if ( dimm_enhanced_shifter[i] == -6 )
                  {
                  }
                  else
                  {
                    dimm_select_arr[i] = dimm_select_arr[i] ^ ( MrcCall->MrcRightShift64(chan_line, dimm_enhanced_shifter[i]) & 1);
                  }
                }
              }
              // NOTE: dimm_select can be either 0 or 1 since there are only 2
              dimm_select = get_dimm_select_with_intrleave_on (dimm_select_arr);


              // DIMM address is channel address with the interleave bits removed
              dimm_line = get_dimm_line_with_hashdis(MrcCall, chan_line, dimm_select_arr);


              dimm_line=chan_line;
            }
            // get the configuration case
            config_case_num = get_the_config_case_num(ddr_type, ch_width, chnl_num_of_dimms, chnl_num_of_ranks,
                (dimm_select && ddr_type == 1) ? get_dimm_s_number_of_ranks(selected_mad_dimm) : get_dimm_l_number_of_ranks(selected_mad_dimm),
                (dimm_select && ddr_type == 1) ? get_dimm_s_width (selected_mad_dimm) : get_dimm_l_width(selected_mad_dimm),
                same_dimm_width_or_one_dimm,
                (dimm_select && ddr_type == 1) ? is_ds_device_density_8Gb(selected_mad_dimm) : is_dl_device_density_8Gb(selected_mad_dimm),
                TRUE,
                lpddr5_bg_mode);

            config_case_num_int_for_switch = config_case_num;

            // calling the parsing method, to get the address decode map for the current configuration.
            // This is the first call, just to know the DIMM shifters
            addr_decode_map_parse(MrcCall, config_arr, config_case_num, FALSE);
            if ( enhanced_interleave_mode )
              enhanced_mode_map_parse(MrcCall, enh_config_arr, config_case_num, FALSE);
  }
  else if( is_cache_line_in_zone1(chan_line, (dimm_l_size + dimm_s_size)) ) // Range 1 limit
  {

    // get the configuration case
    config_case_num = get_the_config_case_num(ddr_type, ch_width, chnl_num_of_dimms, chnl_num_of_ranks,
        get_dimm_l_number_of_ranks(selected_mad_dimm),
        get_dimm_l_width(selected_mad_dimm),
        same_dimm_width_or_one_dimm,
        is_dl_device_density_8Gb(selected_mad_dimm),
        FALSE,
        lpddr5_bg_mode);


    config_case_num_int_for_switch = config_case_num;

    // calling the parsing method, to get the address decode map for the current configuration.
    // This is the first call, just to know the DIMM shifters
    addr_decode_map_parse(MrcCall, config_arr, config_case_num, FALSE);
    if ( enhanced_interleave_mode )
      enhanced_mode_map_parse(MrcCall, enh_config_arr, config_case_num, FALSE);
    // No DIMM interleave. DIMM is the largest DIMM: DIMM L.
    dimm_select = 0;

    // DIMM address is channel address with DIMM S's contribution removed
    dimm_line = chan_line - dimm_s_size;
  }
  else
  {
    return FALSE;
  }

  // Get the physical DIMM index
  *p_dimm = get_physical_dimm_select (dimm_select, selected_mad_intra_chnl);

  //    init_vars_to_retrieve_memory_config(dimm_select, dimm_s_size, dimm_l_size,
  //                                                                        selected_mad_dimm);
  dimm_size     =      (UINT32) (dimm_select ? dimm_s_size : dimm_l_size);
  num_of_ranks     =                       dimm_select ? get_dimm_s_number_of_ranks(selected_mad_dimm) : get_dimm_l_number_of_ranks(selected_mad_dimm);
  D8Gb          =                       dimm_select ? get_dimm_s_8Gb(selected_mad_dimm)                                      : get_dimm_l_8Gb(selected_mad_dimm);
  dimm_width    =                       dimm_select ? get_dimm_s_width(selected_mad_dimm)                                    : get_dimm_l_width(selected_mad_dimm);
  bg0_field_value =                         get_bg0_field_value(selected_mad_dimm);


  dlnor  = get_dimm_l_number_of_ranks(selected_mad_dimm) - 1;
  nor_g2 = ((dlnor%2 == 1) & (get_dimm_s_size_as_cl(MrcCall, selected_mad_dimm) != 0)) ? 1 : 0;
  nor_1  = ((dlnor == 0) & (get_dimm_s_size_as_cl(MrcCall, selected_mad_dimm) == 0)) ? 1: 0;
  width_idx = dimm_width == 8 ? 1 : 0;

  if(1 && (ebh_enable != 0)) {
  }

  // get the exact configuration case
  switch( config_case_num_int_for_switch )
  {
    case 18 : config_case_num = (num_of_ranks == 2)                    ?   10182  :   10181; break;
    case 80 : config_case_num = (num_of_ranks == 2)                    ?   10802  :   10801; break;
    case 23 : config_case_num = (num_of_ranks == 2)                    ?   10232  :   10231; break;
    case 85 : config_case_num = (num_of_ranks == 2)                    ?   10852  :   10851; break;
    case 24 : config_case_num = (dimm_width == 8)                   ?   10241  :   10242; break;
    case 86 : config_case_num = (dimm_width == 8)                   ?   10861  :   10862; break;
    case 25 : config_case_num = (dimm_width == 8)                   ?   10251  :   10252; break;
    case 87 : config_case_num = (dimm_width == 8)                   ?   10871  :   10872; break;
    case 26 : config_case_num = (dimm_width == 8)                   ?   10262  :   10261; break;
    case 88 : config_case_num = (dimm_width == 8)                   ?   10882  :   10881; break;
    case 27 : config_case_num = (dimm_width == 8)                   ?   10271  :   10272; break;
    case 89 : config_case_num = (dimm_width == 8)                   ?   10891  :   10892; break;
    case 28 : config_case_num = (dimm_width == 8)                   ?   10281  :   10282; break;
    case 90 : config_case_num = (dimm_width == 8)                   ?   10901  :   10902; break;
    case 29 : config_case_num = ((D8Gb == 1) && (dimm_width == 8))  ?   10292  :   10291; break;
  }


  addr_decode_map_parse(MrcCall, config_arr, config_case_num, TRUE);
  if ( enhanced_interleave_mode )
    enhanced_mode_map_parse(MrcCall, enh_config_arr, config_case_num, TRUE);

  converter_step = DIMM_NUM_BITS + 1;
  for(i=0; i<RANK_NUM_BITS; i++)
  {
    rank_shifter[(RANK_NUM_BITS-1)-i] = ( config_arr[i+converter_step] - 6 );
    rank_enhanced_shifter[(RANK_NUM_BITS-1)-i] = ( enh_config_arr[i+converter_step] - 6 );
  }
  converter_step += RANK_NUM_BITS;
  for(i=0; i<BG_NUM_BITS; i++)
  {
    bg_shifter[(BG_NUM_BITS-1)-i] =  ( config_arr[i+converter_step] - 6 );
    bg_enhanced_shifter[(BG_NUM_BITS-1)-i] = ( enh_config_arr[i+converter_step] - 6 );
  }
  converter_step += BG_NUM_BITS;
  for(i=0; i<BANK_NUM_BITS; i++)
  {
    bank_shifter[(BANK_NUM_BITS-1)-i] =  ( config_arr[i+converter_step] - 6 );
    bank_enhanced_shifter[(BANK_NUM_BITS-1)-i] = ( enh_config_arr[i+converter_step] - 6 );
  }
  converter_step += BANK_NUM_BITS;
  for(i=0; i<ROW_NUM_BITS; i++)
  {
    row_shifter[(ROW_NUM_BITS-1)-i] =  ( config_arr[i+converter_step] - 6 );
  }
  converter_step += ROW_NUM_BITS;
  for(i=0; i<COL_NUM_BITS; i++)
  {
    col_shifter[(COL_NUM_BITS-1)-i] = ( config_arr[i+converter_step] - 6 );
    col_enhanced_shifter[i] = -6;
  }
  col_enhanced_shifter[10] = ( enh_config_arr[9] - 6 ); // enhanced mode for column[10:10]

  {
  }

  // DRAM address calculation

  // Now compute Rank, Bank Group, Bank, Row and Col
  if ( (num_of_ranks != 3)) // rank = from the address map
  {
    for(i=0; i<RANK_NUM_BITS; i++)
    {
      if ( rank_shifter[i] == -6 )
      {
        rank_select_arr[i] = 0;
      } else
      {
        rank_select_arr[i] = (UINT32) ( MrcCall->MrcRightShift64(dimm_line, rank_shifter[i]) & 1);
      }
      if ( enhanced_interleave_mode )
      {
        if ( rank_enhanced_shifter[i] == -6 )
        {
          DEBUG((DEBUG_INFO, "")); //Do nothing
        } else {
          DEBUG((DEBUG_INFO, "\nDEBUG:   build rank: rank_enhanced_shifter[%0d]=%d\n",i,rank_enhanced_shifter[i]));
          rank_select_arr[i] = rank_select_arr[i] ^ (MrcCall->MrcRightShift64(dimm_line, rank_enhanced_shifter[i]) & 1);
        }
      }
      DEBUG((DEBUG_INFO, "\nDEBUG:   build rank: rank_select_arr[%0d]=%d\n",i,rank_select_arr[i]));
    }
    *p_rank = 0;
    for(i=0; i<RANK_NUM_BITS; i++)
    {
      *p_rank += (rank_select_arr[i] << i);
    }
  } else // rank = for the case of no rank interleave
  {
    *p_rank = 0;
    rank_size = dimm_size / num_of_ranks;

    // support in legacy mode (not ERM)
    if ( num_of_ranks == 2 )

    {
      switch( dimm_size ) // dimm_size is in cache-lines
      {
        case MC_1GB_AS_CL  : rank_bit_shift = 23; break; // 1GB
        case MC_2GB_AS_CL  : rank_bit_shift = 24; break; // 2GB - 2Gbx16, 2 ranks
        case MC_4GB_AS_CL  : rank_bit_shift = 25; break; // 4GB
        case MC_8GB_AS_CL  : rank_bit_shift = 26; break; // 8GB
        case MC_16GB_AS_CL : rank_bit_shift = 27; break; // 16GB
        case MC_32GB_AS_CL : rank_bit_shift = 28; break; // 32GB - DDR4 max size (using 16Gb)
        default:
                 return FALSE;
      }
      *p_rank = (UINT32) (MrcCall->MrcRightShift64(dimm_line, rank_bit_shift) & ONE_BIT_MASK);
      dimm_line = (UINT32) dimm_line & (~(1 << rank_bit_shift));
    } else {
      // support in ERM
      switch( num_of_ranks )
      {
        case 3:
          *p_rank = (dimm_line < rank_size) ? 0 : ((dimm_line < 2 * rank_size) ? 1 : 2); break;
        case 4:
          *p_rank = (dimm_line < rank_size) ? 0 : ((dimm_line < 2 * rank_size) ? 1 : ((dimm_line < 3 * rank_size) ? 2 : 3)); break;
      }
    }
  }

  // Take care of performance ECO (1303982985)
  if(ddr_type == 0 || ddr_type == 1) {

    bg0_bit_select = 0;
    switch(bg0_field_value)
    {
      case 1: bg0_bit_select = 5; break;
      case 2: bg0_bit_select = 6; break;
    }

    if (bg0_field_value){
      i = col_shifter[bg0_bit_select];
      col_shifter[bg0_bit_select] = bg_shifter[0];
      bg_shifter[0] = i;
    }
  }
  //lpddr5 4X4/16 banks & bg0_field_value==1 -> swap BG[1] with C[6]
  if (ddr_type == 2 && bg0_field_value == 1 &&(lpddr5_bg_mode == 0 || lpddr5_bg_mode == 2) ){

    bg0_bit_select = 6;
    i = col_shifter[bg0_bit_select];
    col_shifter[bg0_bit_select] = bg_shifter[1];
    bg_shifter[1] = i;
  }

  // bg = from the address map
  for(i=0; i<BG_NUM_BITS; i++)
  {

    if ( bg_shifter[i] == -6 )
    {
      bg_select_arr[i] = 0;
    } else
    {
      bg_select_arr[i] = (UINT32) ( MrcCall->MrcRightShift64(dimm_line, bg_shifter[i]) & 1);
    }
    if ( enhanced_interleave_mode )
    {
      DEBUG((DEBUG_INFO,"\nDEBUG:   build bg: bg_enhanced_shifter[%0d]=%d\n",i,bg_enhanced_shifter[i]));

      if ( bg_enhanced_shifter[i] == -6 )
      {
        DEBUG((DEBUG_INFO, "")); //Do nothing
      } else
      {
        bg_select_arr[i] = bg_select_arr[i] ^ (MrcCall->MrcRightShift64(dimm_line, bg_enhanced_shifter[i]) & 1);
        if (i == 0 && (ddr_type == 0 || ddr_type == 1)) {
          DEBUG((DEBUG_INFO, "\nDEBUG:   build bg: after (bg_select_arr[%0d]=%0d)^(dimm[5]=%0lld)\n",i,bg_select_arr[i],MrcCall->MrcRightShift64(dimm_line, 5)&1));
          bg_select_arr[i] = extended_bank_group_hasher(MrcCall, 11, bg_select_arr[i], dimm_line);
        }
      }

      if (ebh_enable) {
        switch (ddr_type) {
          case 0: // DDR4
            {
              if (dimm_width == 8 && i == 1 ){ // EBH on bg1 x8
                if (XaB_enable){
                  decode_ebh_XaB(MrcCall, 0,chan_line,i,bg_select_arr,xor_ebh,0);
                }
                if (XbB_enable) {
                  xor_ebh[0] = (chan_line >> XbB_ddr4[dimm_width == 8 ? 0 : 1][2][0]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][2][0] == 0 ? 0 : 1) & nor_1;
                  bg_select_arr[i] = bg_select_arr[i] ^ xor_ebh[0];
                }
              }
              break;
            }
          case 1: //DDR5
            {
              if (i !=0 ){ // EBH on bg
                for(xorBit = 0; xorBit < DDR5_EBH_BG_NUM_OF_BITS ; xorBit++){
                  xor_ebh[xorBit] = 0;
                  if (ddr5_ebh_bg[width_idx][num_of_ranks-1][i-1][xorBit] == 0) {
                    continue;
                  }
                  xor_ebh[xorBit] = (MrcCall->MrcRightShift64(chan_line, ddr5_ebh_bg[width_idx][num_of_ranks-1][i-1][xorBit]) & 1);
                  bg_select_arr[i] = bg_select_arr[i] ^ xor_ebh[xorBit];
                }
              }
              break;
            }
          case 2:
            {
              if (i == 0 && (lpddr5_bg_mode == 0 || lpddr5_bg_mode == 2)) {
                // lp4 == lp5 ebh decoding.
                // lp4 - 3 bank bits - ba2, ba1 ba0
                // lp5 - (8 banks)   - same
                // lp5 - 4x4/16b     - bg0, ba1, ba0
                if (XaB_enable){
                  decode_ebh_XaB(MrcCall, 2,chan_line,i,bg_select_arr,xor_ebh,2);
                }
                if (XbB_enable) {
                  xor_result = decode_ebh_XbB(MrcCall, 2, chan_line,dlnor);
                  bg_select_arr[0] = bg_select_arr[0] ^ xor_ebh[0];
                  break;

                }
              }
            }
        }
      }

    }
  }
  *p_bg = 0;
  for(i=0; i<BG_NUM_BITS; i++)
  {
    *p_bg += (bg_select_arr[i] << i);
  }

  // bank = from the address map
  for(bankBit=0; bankBit<BANK_NUM_BITS; bankBit++)
  {

    if ( bank_shifter[bankBit] == -6 )
    {
      bank_select_arr[bankBit] = 0;
    } else {
      bank_select_arr[bankBit] = (UINT32) ( MrcCall->MrcRightShift64(dimm_line, bank_shifter[bankBit]) & 1);
    }
    if ( enhanced_interleave_mode )
    {
      if ( bank_enhanced_shifter[bankBit] == -6 )
      {
      } else {
        bank_select_arr[bankBit] = bank_select_arr[bankBit] ^ (MrcCall->MrcRightShift64(dimm_line,  bank_enhanced_shifter[bankBit]) & 1);

        if (ebh_enable){
          switch(ddr_type)
          {
            case 1: //DDR5
              {
                if (bankBit == 2) break;
                for(xorBit = 0; xorBit < DDR5_EBH_BANK_NUM_OF_BITS; xorBit++){
                  xor_ebh[xorBit] = 0;
                  if (ddr5_ebh_bank[width_idx][num_of_ranks-1][bankBit][xorBit]== 0) {
                    continue;
                  }
                  xor_ebh[xorBit] = (MrcCall->MrcRightShift64(chan_line, ddr5_ebh_bank[width_idx][num_of_ranks-1][bankBit][xorBit]) & 1);
                  bank_select_arr[bankBit] = bank_select_arr[bankBit] ^ xor_ebh[xorBit];

                }
                break;
              }
            default: // Not DDR5
              {
                if (XaB_enable){
                  // In DDR4 EBH2 xor with bg1 and not bank2
                  if ((ddr_type == 0) && (bankBit == 2)) continue;
                  decode_ebh_XaB(MrcCall, bankBit,chan_line,i,bank_select_arr,xor_ebh,3);
                }
                if (XbB_enable) {

                  if ((ddr_type == 2) && (lpddr5_bg_mode == 0 || lpddr5_bg_mode == 2) && (bankBit == 2)) continue;

                  else if (ddr_type == 3 || ddr_type == 2)
                  {
                    xor_result = decode_ebh_XbB(MrcCall, bankBit, chan_line,dlnor);
                    bank_select_arr[bankBit] = bank_select_arr[bankBit] ^xor_result;
                  }
                  else if (ddr_type == 0){ //DDR4
                    switch (bankBit)
                    {
                      case 0:
                        xor_ebh[0] = MrcCall->MrcRightShift64(chan_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][0]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][0] == 0 ? 0 : 1) & ((nor_g2 == 0) ? 1 : 0);
                        xor_ebh[1] = MrcCall->MrcRightShift64(chan_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][1]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][1] == 0 ? 0 : 1) & nor_1;
                        xor_ebh[2] = MrcCall->MrcRightShift64(chan_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][2]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][2] == 0 ? 0 : 1) & 1;
                        xor_ebh[3] = MrcCall->MrcRightShift64(chan_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][3]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][3] == 0 ? 0 : 1) & 1;
                        bank_select_arr[bankBit] = bank_select_arr[bankBit] ^xor_ebh[0]^xor_ebh[1]^xor_ebh[2]^xor_ebh[3];
                        break;
                      case 1:
                        xor_ebh[0] = MrcCall->MrcRightShift64(chan_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][0]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][0] == 0 ? 0 : 1) & ((nor_g2 == 0) ? 1 : 0);
                        xor_ebh[1] = MrcCall->MrcRightShift64(chan_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][1]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][1] == 0 ? 0 : 1) & nor_1;
                        xor_ebh[2] = MrcCall->MrcRightShift64(chan_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][2]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][2] == 0 ? 0 : 1) & 1;
                        xor_ebh[3] = MrcCall->MrcRightShift64(chan_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][3]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][3] == 0 ? 0 : 1) & 1;
                        bank_select_arr[bankBit] = bank_select_arr[bankBit] ^xor_ebh[0]^xor_ebh[1]^xor_ebh[2]^xor_ebh[3];
                        break;
                    }
                  }
                }
              }
          }
        }
      }
    }
  }

  *p_bank = 0;
  for(i=0; i<BANK_NUM_BITS; i++)
  {
    *p_bank += (bank_select_arr[i] << i);
  }

  // row = from the address map, depends on DIMM size.
  for(i=0; i<ROW_NUM_BITS; i++)
  {
    if ( row_shifter[i] == -6 )
    {
      row_select_arr[i] = 0;
    } else {

      row_select_arr[i] = (UINT32) ( MrcCall->MrcRightShift64(dimm_line, row_shifter[i]) & 1);
    }
  }

  *p_row = 0;

  for(i=0; i<ROW_NUM_BITS; i++)
  {
    *p_row += (row_select_arr[i] << i);
  }

  // Support in ERM 3 ranks
  // 1 sub channel 3 ranks:
  // Rank_sel_h,Rnk_sel = Modulus3(Zone_address[33:14])
  // Row[16:0], BANK[2] = Div3(Zone_address[33:14])
  // 2 sub channel 3 rank:
  // Rnk_sel_h,Rnk_sel = Modulus3( Zone_address[34:15])
  // Row[16:0], BANK[2] = Div3(Zone_address[34:15])
  if (num_of_ranks == 3){

    // replace bg[1] with the calc rank_h
    *p_bg         = *p_bg   & 1;
    *p_bg         = *p_bg   + (calc_rightshift_mod (MrcCall, dimm_line, (8 +(chnl_num_of_dimms == 2)), 3) & 2);

    // replace rank[0] with the calc rank_l
    *p_rank       = *p_rank & 2;
    *p_rank       = *p_rank + (calc_rightshift_mod (MrcCall, dimm_line, (8 +(chnl_num_of_dimms == 2)), 3) & 1);

    // replace row with cacl row
    *p_row        = 0;
    *p_row        = calc_rightshift_div(MrcCall, dimm_line, (8 +(chnl_num_of_dimms == 2)), 3) >> 1;

    // replace bank[2] with calc ba[2]
    *p_bank       = *p_bank & 3;
    *p_bank       = *p_bank + ((calc_rightshift_div(MrcCall, dimm_line, (8 +(chnl_num_of_dimms == 2)), 3) & 1) << 2);
  }


  // Mask away any row bits too large for the size of DIMM (only the number of row bits changes with DIMM size).
  UINT32 row_lsb = 0;

  if ( ddr_type == 0 ) // DDR4
  {
    row_lsb = (dimm_width == 8) ? 11 : 10; // channel address[16] or [17] per HAS
  } else if(ddr_type == 1) { //DDR5
    if (dimm_select ? is_ds_device_density_8Gb(selected_mad_dimm) : is_dl_device_density_8Gb(selected_mad_dimm)) {
      row_lsb = (dimm_width == 8) ? 10 : 9;
    } else {
      row_lsb = (dimm_width == 8) ? 11 : 10;
    }
  } else if (ddr_type == 2) // LPDDR5
  {
    row_lsb = 9; //Channel address[15] per HAS
  } else if (ddr_type == 3) // LPDDR4
  {
    row_lsb = 8; //Channel address[14] per HAS
  }


  row_lsb = row_lsb + ((num_of_ranks/2));


  *p_row = *p_row & ((UINT32) ((dimm_size >> row_lsb) - 1));


  // col address at the end of the function - it is the same for all the options


  // column is the same for all the options
  //
  for(i=0; i<COL_NUM_BITS; i++)
  {

    if ( col_shifter[i] == -6 )
    {
      col_select_arr[i] = 0;
    } else if ( col_shifter[i] < 0 )
    {
      col_shifter[i] += 6;
      col_select_arr[i] = (UINT32) ( MrcCall->MrcRightShift64(sys_addr_mod, col_shifter[i]) & 1);
    } else
    {
      col_select_arr[i] = (UINT32) ( MrcCall->MrcRightShift64(dimm_line, col_shifter[i]) & 1);
    }
    if ( enhanced_interleave_mode )
    {
      if ( col_enhanced_shifter[i] == -6 )
      {
      }
      else
      {
        col_select_arr[i] = col_select_arr[i] ^ (MrcCall->MrcRightShift64(dimm_line, col_enhanced_shifter[i]) & 1);
      }
    }
  }
  *p_col = 0;
  for(i=0; i<COL_NUM_BITS; i++)
  {
    *p_col += (col_select_arr[i] << i);
  }

  // ERM support: The high bit rank is in bg[1]
  if(num_of_ranks > 2){
    *p_rank = *p_rank | (*p_bg & 2);
    *p_bg  = 0;
  }

  *p_config_case_num = config_case_num;
  return TRUE;

}

static
BOOLEAN
tgl_cmi_slice_to_dram_addr_decode(
    IN const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 cmi_slice_addr,

    // register inputs
    UINT32 MAD_MC_HASH,
    UINT32 CHANNEL_HASH,
    UINT32 CHANNEL_EHASH,
    UINT32 MAD_INTER_CHNL,
    UINT32 MAD_INTRA_CHNL_ch0,
    UINT32 MAD_INTRA_CHNL_ch1,
    UINT32 MAD_DIMM_ch0,
    UINT32 MAD_DIMM_ch1,
    UINT64 SC_GS_CFG,
    UINT64 TOM,

    // printing option
    UINT32 is_print,

    // outputs
    UINT32* p_chan,
    UINT32* p_dimm,
    UINT32* p_rank,
    UINT32* p_bg,
    UINT32* p_bank,
    UINT32* p_row,
    UINT32* p_col)
{
  BOOLEAN p_is_tcm = 0;
  INT32 p_config_num;
  return (BOOLEAN) cnl_mc_addr_decode(
      MrcCall,
      cmi_slice_addr, &p_is_tcm,
      MAD_MC_HASH,TOM, CHANNEL_HASH, CHANNEL_EHASH, MAD_INTER_CHNL,
      MAD_INTRA_CHNL_ch0, MAD_INTRA_CHNL_ch1, MAD_DIMM_ch0, MAD_DIMM_ch1,SC_GS_CFG,
      is_print,
      p_chan, p_dimm, p_rank, p_bg, p_bank, p_row, p_col,&p_config_num);
}

static
BOOLEAN
mc_sys_to_cmi_slice_addr_decode(
    IN const MRC_ADDR_FUNCTION *MrcCall,
    UINT64 sys_addr,
    BOOLEAN* p_is_tcm,
    UINT32 MAD_SLICE,
    UINT32 MEMORY_SLICE_HASH,
    UINT32 TOLUD,
    UINT64 REMAP_BASE,
    UINT64 REMAP_LIMIT,
    UINT64 tom,
    UINT32 is_print,
    UINT32* p_slice,
    UINT64* p_slice_addr)
{
  UINT32 slice_select;
  UINT64 slice_line;
  UINT64 remap_line;

  remap_line = sys_addr;
  if( is_remap_enabled(REMAP_LIMIT, REMAP_BASE) ) // check for remap region being enabled
  {
    remap_line = adjust_address_for_remap(MrcCall, TOLUD, REMAP_BASE, REMAP_LIMIT, p_is_tcm, sys_addr);
    if ((remap_line == 0) && (remap_line != sys_addr))
      return FALSE;
  } else {
  }

  // Cache-line address
  remap_line = get_cacheline_from_addr(MrcCall, remap_line);

  // Check that remapped address is below MC capacity.
  if (is_Sys_Addr_above_TOM(remap_line, tom))
  {
    return FALSE;
  }

  if( get_stacked_mode(MAD_SLICE)) {
    slice_select = calculate_stacked_slice_select(MrcCall, remap_line, MAD_SLICE);
    slice_line = calculate_stacked_slice_line_addr(MrcCall, remap_line, MAD_SLICE);
  } else if( is_cache_line_in_zone0 (MrcCall, remap_line, get_ch_s_size_as_cl(MrcCall, MAD_SLICE)) ) // Zone 0
  {
    if( is_channel_hash_enabled(MEMORY_SLICE_HASH) )
    {
      slice_select = calculate_slice_select(MrcCall, remap_line, MEMORY_SLICE_HASH);
      slice_line = calculate_slice_line_addr(MrcCall, remap_line, MEMORY_SLICE_HASH);
    } else {
      slice_select = (UINT32) get_channel_select_with_hashdis(remap_line);
      slice_line   = get_channel_line_with_hashdis(MrcCall, remap_line);
    }
  } else if(is_cache_line_in_zone1(remap_line, tom)) // Zone 1
  {
    slice_select = 0; // Channel L
    slice_line = remap_line - get_ch_s_size_as_cl(MrcCall, MAD_SLICE);
  } else {
    // address was above memory capacity
    return FALSE;
  }

  // obtain the physical slice index
  *p_slice = logical_to_physical_chan(MAD_SLICE, slice_select);
  *p_slice_addr = MrcCall->MrcLeftShift64(slice_line, 6);
  *p_slice_addr |= sys_addr & 0x3f;

  if( 1 )
  {
  }

  return TRUE;
}

static
UINT64
reverse_adjust_address_for_remap(
    UINT32 TOLUD,
    UINT64 REMAP_BASE,
    UINT64 REMAP_LIMIT,
    UINT64 remap_addr)
{

  UINT64 return_addr = remap_addr;

  UINT64 top_of_remaped_mem = (UINT64) TOLUD;
  top_of_remaped_mem += REMAP_LIMIT;
  top_of_remaped_mem -= REMAP_BASE;
  if( (remap_addr <= top_of_remaped_mem) && (remap_addr >= ((UINT64) TOLUD)) ) // b4194941 - REMAP_LIMIT is now inclusive
  {

    // remap applies. move the address to the remap zone
    return_addr -= ((UINT64) TOLUD);
    return_addr += REMAP_BASE;
  }
  return return_addr;
}

static
BOOLEAN
cmi_slice_addr_to_mc_sys_addr_encode(
    IN const MRC_ADDR_FUNCTION *MrcCall,
    UINT32 slice,
    UINT64 slice_addr,
    UINT32 MAD_SLICE,
    UINT32 MEMORY_SLICE_HASH,
    UINT32 TOLUD,
    UINT64 REMAP_BASE,
    UINT64 REMAP_LIMIT,

    UINT32 is_print,

    UINT64* p_sys_addr,
    BOOLEAN* p_is_tcm)
{

  UINT64 remap_line, remap_addr;

  // map physical slice to L or S
  //
  UINT32 slice_select = physical_to_logical_chan(MAD_SLICE, slice);
  remap_line = MrcCall->MrcRightShift64(slice_addr, 6);

  // Stacked channel mode
  if( get_stacked_mode(MAD_SLICE) ) {

    if (get_stacked_mode_ch1(MAD_SLICE)) {
      slice_select ^= 1;
    }

    // In stacked mode, the channel is chosen based on the bit corresponding to the
    // size of the stacked register.  Bit-wise 'OR' in the channel selection bit into that
    // position.
    //
    remap_line |= (UINT64) (slice_select << (24 + get_stacked_encoding(MAD_SLICE)));
  } else if( is_cache_line_in_zone0 (MrcCall, remap_line, get_ms_s_size_as_cl(MrcCall, MAD_SLICE)) ) // Zone 0
    // Determine which range/zone the remapped system address falls into
  {

    // Determine if the channel hash feature is being used
    if( MEMORY_SLICE_HASH & MC_ADDR_CHAN_HASH_ENABLE_MASK ) // test enable bit
    {
      UINT32 hash_lsb_mask_bit = get_ch_hash_lsb_mask_bit( MEMORY_SLICE_HASH );
      UINT32 hash_mask = get_ch_hash_mask( MEMORY_SLICE_HASH );

      // placing the chan_select bit in hash_lsb_mask_bit
      remap_line = (remap_line & MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, hash_lsb_mask_bit));
      remap_line = MrcCall->MrcLeftShift64(remap_line, 1); // getting the highest bit
      remap_line = remap_line | (remap_line & (~MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, hash_lsb_mask_bit))); // getting the highest bit

      // Get the bits used to produce chan_select, sans the bit athash_lsb_mask_bit
      UINT64 hash_line = ((UINT32) remap_line) & hash_mask;

      // Recreate the value of the bit at hash_lsb_mask_bit by doint the hash
      // XORs.
      //
      // I don't know of a single instruction to do this, so an unrollable
      // loop will be used.
      //
      UINT32 hash_bit = 0;
      UINT32 i;
      for( i = 0 ; i < MC_ADDR_CHANNEL_HASH_MASK_SIZE ; i++ )
        hash_bit = hash_bit ^ (UINT32)MrcCall->MrcRightShift64(hash_line, i);
      hash_bit = hash_bit & 1;

      // Recreate the missing bit by XORing the chan_select (the result of
      // the forward decode).
      // (If X = A ^ B, then A = X ^ B)
      //
      hash_bit = hash_bit ^ slice_select;

      // put the missing bit back into the address
      remap_line = remap_line | (hash_bit << hash_lsb_mask_bit);
    }
    else
    {
      // Without the hash, sys_addr[6:6] determines the channel
      remap_line = MrcCall->MrcLeftShift64(remap_line, 1);
      remap_line |= ((UINT64) slice_select);
    }
  }
  else // Zone 1
  {

    remap_line += (get_ms_s_size_as_cl(MrcCall, MAD_SLICE));
  }

  // work on full address instead of cache-line address;
  remap_addr = MrcCall->MrcLeftShift64(remap_line, 6);


  // Determine if the address is under the remap zone and therefore must be a
  // TCM.  remap_line can't be at or above TOM (Top Of Memory), so no need to
  // check that.  Simply check if the remap_line is between the base and
  // limit.
  //
  *p_is_tcm = (remap_addr <= REMAP_LIMIT) && (remap_addr >= REMAP_BASE); // b4194941 - REMAP_LIMIT is now inclusive


  if( !(*p_is_tcm) && // remap doesn't apply if address was under remap zone.
      (REMAP_LIMIT > REMAP_BASE) ) // remap doesn't apply if remap zone disabled
  {
    remap_addr = reverse_adjust_address_for_remap(TOLUD, REMAP_BASE, REMAP_LIMIT, remap_addr);
  }
  *p_sys_addr = remap_addr;

  // Add back in lower 6 bits
  *p_sys_addr = *p_sys_addr | (slice_addr & 0x3FULL);

  return TRUE;
}

/**
  Address encode function (reverse address decode)
  DRAM address to system address conversion

  @param[in]      MrcCall            - pointer to MRC_ADDR_FUNCTION struct
  @param[in]      chan               - channel addr to encode
  @param[in]      dimm               - DIMM address to encode
  @param[in]      rank               - rank address to encode
  @param[in]      bg                 - bank group address to encode
  @param[in]      bank               - bank address to encode
  @param[in]      row                - row address to encode
  @param[in]      col                - column address to encode. Note: The architecture is limited to half-cache-line
  granularity for burst order. Therefore the last two bits of the column are ignored.
  @param[in]      TOLUD              - memory register
  @param[in]      REMAP_BASE         - memory register
  @param[in]      REMAP_LIMIT        - memory register
  @param[in]      TOM                - memory register
  @param[in]      CHANNEL_HASH       - memory register
  @param[in]      MAD_INTER_CHNL     - memory register
  @param[in]      MAD_INTRA_CHNL_ch0 - memory register
  @param[in]      MAD_INTRA_CHNL_ch1 - memory register
  @param[in]      MAD_DIMM_ch0       - memory register
  @param[in]      MAD_DIMM_ch1       - memory register
  @param[in]      TOM                - memory register
  @param[in, out] mc_select          - At return, (*mc_select != 0) means its caller need add MC hasbit back into address
  @param[out]     p_sys_addr           - the 39-bit system address convert to
  @param[in, out] p_is_tcm           - is the transaction to sys_addr "traffic class for the manageability engine"

  @retval         TRUE  - sys_addr decoded without errors and the address outputs are valid.
  FALSE - An error was encountered while decoding sys_addr.
 **/
static
BOOLEAN
cnl_mc_addr_encode(
    IN const MRC_ADDR_FUNCTION *MrcCall,
    IN       UINT32       chan,
    IN       UINT32       dimm,
    IN       UINT32       rank,
    IN       UINT32       bg,
    IN       UINT32       bank,
    IN       UINT32       row,
    IN       UINT32       col,
    IN       UINT32       MAD_MC_HASH,
    IN       UINT32       CHANNEL_HASH,
    IN       UINT32       CHANNEL_EHASH,
    IN       UINT32       MAD_INTER_CHNL,
    IN       UINT32       MAD_INTRA_CHNL_ch0,
    IN       UINT32       MAD_INTRA_CHNL_ch1,
    IN       UINT32       MAD_DIMM_ch0,
    IN       UINT32       MAD_DIMM_ch1,
    IN       UINT64       SC_GS_CFG,
    IN       UINT64       TOM,
    IN       UINT32       mc_select,
    OUT      UINT64       *p_sys_addr,
    IN OUT   BOOLEAN      *p_is_tcm
    )
{
  //Fields in global
  UINT32  ebh_enable;                     // Enhanced Bank Enable
  UINT32  XaB_enable;                     // xor XaB enable
  UINT32  XbB_enable;                     // xor XbB_lp4 enable
  UINT32  ddr_type;                           // DDR4=0; DDR3=1; LPDDR3=2; LPDDR4=3, WIO2=4;
  UINT32  enhanced_ch_mode;                   // Enabled = 0; Disabled = 1;
  UINT32  bg0_field_value;                // Enabled = 0; Disabled = 1;
  UINT32  bg0_bit_select;                // Enabled = 0; Disabled = 1;
  UINT32  mad_dimm_l[2];                 // Make a MAD_DIMM array for easy access
  UINT32  mad_intra_chnl_l[2];           // Make a MAD_INTRA_CHNL array for easy access
  UINT64  ch_s_size;                     // chanel s_size adjusted to cl address
  UINT32  ch_l_map;                      // Virtual channel L mapping (0 - CH 0; 1 - CH 1)
  UINT32  chan_l_mad_dimm;               // channel L's MAD_DIMM register
  UINT64  tom;                           // Top Of Memory (system address)
  UINT64  calculated_tom;                // TOM calculated from channel sizes
  UINT32  ch_width;                      // New field to determin the Channel Width.
  UINT32  lpddr5_bg_mode;                                // get LPDDR5 BG mode
  // End of global data

  UINT32 num_of_ranks;                     // number of ranks on selected DIMM
  UINT32 dimm_width;
  UINT32 D8Gb;                             // 0-> NOT 8Gb; 1-> 8Gb

  UINT32 hash_lsb_mask_bit;                // LsbMaskBit field from CHANNEL_HASH register
  UINT32 hash_mask;                        // channel hash mask from CHANNEL_HASH register

  UINT32 slice_zone1_start;                // Zone1 start separate between MC0/1 Interleave zones
  UINT32 slice_stacked_mode;               // Indicated if stacked mode is on (MC wise)
  UINT32 slice_hash_enable;                // Indicating if HASHing is required
  UINT32 slice_high_bits_gb= 0;              // Holds system address high bits address[37:30]
  UINT32 slice_high_bits_h_gb= 0;              // Holds system address high bits address[37:30]
  UINT64 sliced_address;         // Holds the address after slice is being done

  UINT64 dimm_select;            // DIMM L=0, S=1
  UINT64 dimm_size;              // size of selected DIMM

  UINT64 bit_above_row;          // one-hot bit to mark the size of the row address

  BOOLEAN high_order_rank_interleave;
  BOOLEAN enhanced_interleave_mode;

  UINT64 dimm_line = 0;              // DIMM address space (cache-line)
  UINT64 chan_line = 0;              // Channel address space (cache-line)

  // sizes of the DIMMs on the channel (cache-line)
  UINT64 dimm_l_size;
  UINT64 dimm_s_size;
  BOOLEAN same_dimm_width_or_one_dimm;
  UINT32 chnl_num_of_dimms;
  UINT32 chnl_num_of_ranks;
  UINT32 dimm_s_num_of_ranks;
  UINT32 num_col_bits;

  UINT32 chan_select;                       // 0 = Channel L, 1 = Channel S

  // address before reverse decoding the remap region
  UINT64 remap_line;              // cache-line
  UINT64 remap_addr;              // full address

  UINT32 hash_line;                         // lower bits of remap_line with hash_mask applied
  UINT32 hash_bit;                          // bit that gets destroyed in forward decode

  UINT32 i;                                 // loop counter
  UINT32 bgBit;
  UINT32 nor_idx;
  UINT32 xorBit;
  UINT32 bankBit;
  UINT32 width_idx;

  UINT32 dlnor;                                                        // large number of ranks - EBH purpose
  UINT32 nor_g2;
  UINT32 nor_1;
  UINT32 bank_no_ebh[BANK_NUM_BITS];       // for later assemble no EBH bank
  UINT32 bg_no_ebh[BG_NUM_BITS];
  UINT32 xor_ebh[DDR5_EBH_BG_NUM_OF_BITS];

  // virtual arrays to get the inputs from the addr_decode_maps.csv table, according to the configuration
  INT32 config_arr[DRAM_ADDR_SIZE+1];

  // virtual arrays to get the inputs from the enhanced_mode_map.csv table, according to the configuration
  INT32 enh_config_arr[ENHANCED_BITS_SIZE+1];


  INT32 config_case_num;
  UINT32 config_case_num_int_for_switch;
  UINT32 converter_step; // virtual var

  UINT64 num_rows_in_dimm_s = 0;
  UINT32 denominator;

  // shifters for bit select (from chnl_addr)according to the address decode map

  INT32 dimm_shifter[DIMM_NUM_BITS];
  INT32 rank_shifter[RANK_NUM_BITS];
  INT32 bg_shifter[BG_NUM_BITS];
  INT32 bank_shifter[BANK_NUM_BITS];
  INT32 row_shifter[ROW_NUM_BITS];
  INT32 col_shifter[COL_NUM_BITS];

  INT32 dimm_enhanced_shifter[DIMM_NUM_BITS];
  INT32 rank_enhanced_shifter[RANK_NUM_BITS];
  INT32 bg_enhanced_shifter[BG_NUM_BITS];
  INT32 bank_enhanced_shifter[BANK_NUM_BITS];
  INT32 col_enhanced_shifter[COL_NUM_BITS];


  BOOLEAN rerun_zone_1        = FALSE;
  BOOLEAN found_the_dimm_zone = FALSE;
  BOOLEAN is_8Gb_device;

  UINT64  Temp;

  //MRC adds it for temporary to save intermediate value
  UINT64  MC_slice_address;

  *p_sys_addr = 0;

  ebh_enable = 0;                     // Enhanced Bank Enable
  XaB_enable = 0;                     // xor XaB enable
  XbB_enable = 0;                     // xor XbB_lp4 enable


  MrcCall->MrcSetMem ((UINT8 *)bank_no_ebh, sizeof (bank_no_ebh), 0);
  MrcCall->MrcSetMem ((UINT8 *)bg_no_ebh, sizeof (bg_no_ebh), 0);
  MrcCall->MrcSetMem ((UINT8 *)xor_ebh, sizeof (xor_ebh), 0);
  MrcCall->MrcSetMem ((UINT8 *)mad_dimm_l, sizeof (mad_dimm_l), 0);
  MrcCall->MrcSetMem ((UINT8 *)mad_intra_chnl_l, sizeof (mad_intra_chnl_l), 0);
  MrcCall->MrcSetMem ((UINT8 *)config_arr, sizeof (config_arr), 0);
  MrcCall->MrcSetMem ((UINT8 *)enh_config_arr, sizeof (enh_config_arr), 0);
  MrcCall->MrcSetMem ((UINT8 *)dimm_shifter, sizeof (dimm_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)rank_shifter, sizeof (rank_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)bg_shifter, sizeof (bg_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)bank_shifter, sizeof (bank_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)row_shifter, sizeof (row_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)col_shifter, sizeof (col_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)dimm_enhanced_shifter, sizeof (dimm_enhanced_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)rank_enhanced_shifter, sizeof (rank_enhanced_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)bg_enhanced_shifter, sizeof (bg_enhanced_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)bank_enhanced_shifter, sizeof (bank_enhanced_shifter), 0);
  MrcCall->MrcSetMem ((UINT8 *)col_enhanced_shifter, sizeof (col_enhanced_shifter), 0);

  ddr_type = get_ddr_type (MAD_INTER_CHNL);
  enhanced_ch_mode = get_enhanced_ch_mode (MAD_INTER_CHNL);

  ch_width = get_ch_width(MAD_INTER_CHNL);
  lpddr5_bg_mode   = get_lpddr5_bg_mode(MrcCall, SC_GS_CFG);

  mad_dimm_l[0]       = MAD_DIMM_ch0;
  mad_dimm_l[1]       = MAD_DIMM_ch1;
  mad_intra_chnl_l[0] = MAD_INTRA_CHNL_ch0;
  mad_intra_chnl_l[1] = MAD_INTRA_CHNL_ch1;

  // Note: Since MC decoder topology can be different than decoder topology
  // (i.e 12GB MC topology is mapped to 16GB in decoder topology)
  // We will have two global variables that hold the information:
  // calculated_tom - for the TOM being calculated from decoder registers (below)
  // actual_tom - the actual dram size.
  // TOM is not directly available in a register. It will be computed by
  // finding the channel index of channel L, then adding the two DIMM
  // capacities of that channel together. Technically, this is only needed to
  // check that the system address is not beyond the amount of memory
  // available.
  ch_s_size = get_ch_s_size_as_cl( MrcCall, MAD_INTER_CHNL );
  ch_l_map  = get_ch_l_map(MAD_INTER_CHNL);
  chan_l_mad_dimm = mad_dimm_l[ch_l_map];
  calculated_tom  = cl_to_sys(MrcCall, get_dimm_l_size_as_cl(MrcCall, chan_l_mad_dimm ));
  calculated_tom += cl_to_sys(MrcCall, get_dimm_s_size_as_cl(MrcCall, chan_l_mad_dimm ));
  calculated_tom += cl_to_sys(MrcCall, ch_s_size);
  tom = TOM;

  dimm_line = 0;
  chan_line = 0;

  // determine if we are in EBH mode enable - MAD_DIMM_ch0/ch1 ebh_decode filed must be the same
  ebh_enable = get_ebh_mode(MAD_DIMM_ch0);
  // FIXME: hlalithk (17ww50): Enable Enhanced Bank Hashing for LPDDR5
  if (ebh_enable){
    XaB_enable = ebh_enable & 1;
    XbB_enable =(ebh_enable >> 1) & 1;
    if (ddr_type != 1) {
      DEBUG((DEBUG_INFO, "\nDEBUG:    XaB_enable = %0d", XaB_enable));
      DEBUG((DEBUG_INFO, "\nDEBUG:    XbB_enable= %0d", XbB_enable));
      for ( bankBit = 0; bankBit < BANK_NUM_BITS; bankBit++){
        DEBUG((DEBUG_INFO, "\nDEBUG:  XaB%0d = ",bankBit));
        for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
          if (ddr_type != 1)
            DEBUG((DEBUG_INFO, "0%d %s", XaB[bankBit][xorBit], xorBit == (XaB_NUM_OF_BITS-1) ? "": "^ "));
        }
      }
      if(ddr_type == 0){
        DEBUG((DEBUG_INFO, "\n\nDEBUG:  DDR4 XbB:"));
        for ( bankBit = 0; bankBit < BANK_NUM_BITS; bankBit++){
          DEBUG((DEBUG_INFO, "\nDEBUG:  XbB%0d(x8) = [",bankBit));
          for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
            DEBUG((DEBUG_INFO, "%0d %s", XbB_ddr4[0][bankBit][xorBit], xorBit == (XaB_NUM_OF_BITS-1) ? "]":", "));
          }
          DEBUG((DEBUG_INFO, "\nDEBUG:  XbB%0d(x16) = [",bankBit));
          for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
            DEBUG((DEBUG_INFO, "%0d %s", XbB_ddr4[1][bankBit][xorBit], xorBit == (XaB_NUM_OF_BITS-1) ? "]": ", "));
          }
        }
      } else if (ddr_type == 3 || ddr_type == 2) {
        DEBUG((DEBUG_INFO, "\n\nDEBUG:  LP4/LP5:"));
        for ( bankBit = 0; bankBit < BANK_NUM_BITS; bankBit++){
          DEBUG((DEBUG_INFO, "\nDEBUG:  XbB%0d = [",bankBit));
          for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
            DEBUG((DEBUG_INFO, "%0d %s", XbB_lp4[bankBit][xorBit], xorBit == (XaB_NUM_OF_BITS-1) ? "]":", "));
          }
        }
      }
    } else {
      DEBUG((DEBUG_INFO, "\n\nDEBUG:  DDR5:."));
      for ( width_idx = 0; width_idx < 2; width_idx++){
        for ( nor_idx = 0; nor_idx < 2; nor_idx++){
          for ( bankBit = 0; bankBit < BANK_NUM_BITS-1; bankBit++){
            DEBUG((DEBUG_INFO, "\nDEBUG:  ddr5_ebh_bank[%s][%s][%0d] = {",width_idx ? "x8" : "x16",nor_idx ? "2 Ranks" : "1 Rank", bankBit));
            for(xorBit = 0; xorBit < DDR5_EBH_BANK_NUM_OF_BITS; xorBit++){
              DEBUG((DEBUG_INFO, "%0d %s", ddr5_ebh_bank[width_idx][nor_idx][bankBit][xorBit], xorBit == (DDR5_EBH_BANK_NUM_OF_BITS-1) ? "}":", "));
            }
          }
        }
      }
      for ( width_idx = 0; width_idx < 2; width_idx++){
        for ( nor_idx = 0; nor_idx < 2; nor_idx++){
          for ( bgBit = 0; bgBit < BANK_NUM_BITS-1; bgBit++){
            DEBUG((DEBUG_INFO, "\nDEBUG:  ddr5_ebh_bg[%s][%s][%0d] = {",width_idx ? "x8" : "x16",nor_idx ? "2 Ranks" : "1 Rank", bgBit+1));
            for(xorBit = 0; xorBit < DDR5_EBH_BG_NUM_OF_BITS; xorBit++){
              DEBUG((DEBUG_INFO, "%0d %s", ddr5_ebh_bg[width_idx][nor_idx][bgBit][xorBit], xorBit == (DDR5_EBH_BG_NUM_OF_BITS-1) ? "}":", "));
            }
          }
        }
      }
    }
  }

  DEBUG ((DEBUG_INFO, "\n"));
  // illegal channel check
  if( chan & ~((UINT32) 1) )
  {
    return FALSE;
  }

  // check for too high of a DIMM index
  if( dimm & ~((UINT32) 1) )
  {
    return FALSE;
  }

  // is it DIMM L or S? L=0, S=1
  dimm_select = (UINT64) (dimm ^ get_dimm_l_select(mad_intra_chnl_l[chan]));


  // get DIMM size
  dimm_size = dimm_select ? get_dimm_s_size_as_cl(MrcCall, mad_dimm_l[chan]) : get_dimm_l_size_as_cl(MrcCall, mad_dimm_l[chan]);

  // check if DIMM slot is populated
  if( dimm_size == 0 )
  {
    return FALSE;
  }

  // check for too high of a rank index
  if( rank & ~((UINT32) 3) )
  {
    return FALSE;
  }

  // get some selected DIMM parameters
  num_of_ranks     =                       dimm_select ? get_dimm_s_number_of_ranks(mad_dimm_l[chan])  :   get_dimm_l_number_of_ranks(mad_dimm_l[chan]);
  dimm_width    =                       dimm_select ? get_dimm_s_width(mad_dimm_l[chan])            :   get_dimm_l_width(mad_dimm_l[chan]);
  D8Gb          =                       dimm_select ? get_dimm_s_8Gb(mad_dimm_l[chan])              :   get_dimm_l_8Gb(mad_dimm_l[chan]);
  bg0_field_value =                     get_bg0_field_value(mad_dimm_l[chan]);
  dlnor         = get_dimm_l_number_of_ranks(mad_dimm_l[chan]) - 1;
  nor_g2 = ((dlnor%2 == 1) & (get_dimm_s_size_as_cl(MrcCall, mad_dimm_l[chan]) != 0)) ? 1 : 0;
  nor_1 = ((dlnor == 0) & (get_dimm_s_size_as_cl(MrcCall, mad_dimm_l[chan]) == 0)) ? 1: 0;
  width_idx = dimm_width == 8 ? 1 : 0;

  DEBUG((DEBUG_INFO, "8Gb:%d\n", D8Gb));

  if(ebh_enable != 0) {
  }

  // check that rank exists on DIMM
  if( rank > num_of_ranks )
  {
    return FALSE;
  }

  // check for too high of a bg index
  if ( (!ddr_type) && (dimm_width==16) ) // DDR4 (DDR4==0 => FALSE) x16 => bg[1]=0
  {
    if( bg & ~((UINT32) 0x1) )
    {
      return FALSE;
    }
  } else if (ddr_type == 1){
    if((dimm_width==16) && (bg & ~((UINT32) 0x3)))
    {
      return FALSE;
    }
  }

  // check for too high of a bank index
  if( bank & ~((UINT32) 0x7) )
  {
    return FALSE;
  }

  // set a bit in a position that is one bit higher than the highest row bit
  num_col_bits = dimm_select ? get_dimm_s_num_col_bits(ddr_type) : get_dimm_l_num_col_bits(ddr_type);

  bit_above_row = MrcCall->MrcRightShift64(dimm_size, (num_col_bits - (2 * (UINT32)enhanced_ch_mode)));

  bit_above_row = MrcCall->MrcDivU64x64(bit_above_row, ((num_of_ranks==3) ? (num_of_ranks+1) : num_of_ranks), NULL);

  // When DDR4 x8, Bg has two bits and the size can be twice bigger.
  bit_above_row = ((ddr_type == 0) && (dimm_width==8))  ? MrcCall->MrcRightShift64(bit_above_row, 1) : bit_above_row;

  // For DDR5, x16 we need to left shift to get the correct out of bound row bit
  bit_above_row = ((ddr_type == 1) && (dimm_width==16)) ? MrcCall->MrcLeftShift64(bit_above_row, 1) : bit_above_row;

  // Check for unexpected high-order row bits
  if( row & ~(((UINT32) bit_above_row) - 1) )
  {
    return FALSE;
  }

  // check for unexpected high-order column bits
  if( col & ~((((UINT32) 1) << num_col_bits) - 1) )
  {
    // check for the special command bits in the column address
    if( ( col & (1<<10)) && (ddr_type == 1) && (num_col_bits < 11) ) // col[10:10] - AP bit
    {
    } else
      //else if( col & (1<<12) && (num_col_bits < 13) ) // col[12:12] - BLOTH bit
      //{
      //    DEBUG((DEBUG_INFO,
      //        "Column bit 12 (the Burst Length On The Fly bit) was set. "
      //        "BLOTH should not be used.\n"
      //        "col = 0x%03x\n",
      //        col));
      //}
    {
    }
    return FALSE;
  }


  //////////////////////////////////////////////////////////////////////////////////////
  // Done with checking. Now reverse decode the address.
  //////////////////////////////////////////////////////////////////////////////////////

  // determine if we are doing high order rank interleave
  high_order_rank_interleave = get_high_order_intlv_mode(mad_intra_chnl_l[chan]);

  // determine if we are doing Enhanced Interleave Mode (EIM) (XOR dimm, rank, bg & bank bits)
  enhanced_interleave_mode   = get_enhanced_intlv_mode(mad_intra_chnl_l[chan]);

  // Find the DIMM sizes on our selected channel. adjust to cache-line granularity
  dimm_l_size = get_dimm_l_size_as_cl(MrcCall, mad_dimm_l[chan]);
  dimm_s_size = get_dimm_s_size_as_cl(MrcCall, mad_dimm_l[chan]);

  // Find number of ranks and dimms in the selected channel
  chnl_num_of_dimms = (dimm_l_size) ? ((dimm_s_size) ? 2 : 1) : 0;
  chnl_num_of_ranks = ((dimm_l_size) ? get_dimm_l_number_of_ranks(mad_dimm_l[chan]) : 0) + ((dimm_s_size) ? get_dimm_s_number_of_ranks(mad_dimm_l[chan]) : 0);

  dimm_s_num_of_ranks = (dimm_s_size) ? get_dimm_s_number_of_ranks(mad_dimm_l[chan]) : 0;

  // Find the dimm width chnl configuration
  same_dimm_width_or_one_dimm =   (dimm_s_size) ?
    ((get_dimm_l_width(mad_dimm_l[chan])==get_dimm_s_width(mad_dimm_l[chan])) ?
     TRUE : FALSE) : TRUE;

  //denominator = dimm_s_num_of_ranks;// *(2^BG_NUM_BITS);
  denominator = (1 << BG_NUM_BITS);

  if  (1)
  {
    DEBUG((DEBUG_INFO, "\nDEBUG: denominator dimm s ranks: %d chnl_num_of_ranks %d BG_NUM_BITS %d denominator %d\n",dimm_s_num_of_ranks,chnl_num_of_ranks,BG_NUM_BITS,denominator));
  }

  if (!dimm_s_size) {
    num_rows_in_dimm_s = 0;
  } else {
    Temp = MrcCall->MrcLeftShift64(dimm_s_size, 6);
    num_rows_in_dimm_s = MrcCall->MrcDivU64x64(Temp, (dimm_s_num_of_ranks*(1<<COL_NUM_BITS)*(64/get_dimm_s_width(mad_dimm_l[chan]))), NULL);
  };

  DEBUG((DEBUG_INFO, "num_rows_in_dimm_s is 0x%09llx\n", num_rows_in_dimm_s));

  is_8Gb_device = dimm_select ? is_ds_device_density_8Gb(mad_dimm_l[chan]) : is_dl_device_density_8Gb(mad_dimm_l[chan]);

  // get the configuration case
  config_case_num = get_the_config_case_num(ddr_type, ch_width, chnl_num_of_dimms, chnl_num_of_ranks,
      (ddr_type == 1) ? num_of_ranks :  get_dimm_l_number_of_ranks(mad_dimm_l[chan]), // Separating ddr5 from other techs - different implemntation
      (ddr_type == 1) ? dimm_width   :  get_dimm_l_width(mad_dimm_l[chan]),
      same_dimm_width_or_one_dimm,
      is_8Gb_device,
      !rerun_zone_1,
      lpddr5_bg_mode );

  while (!found_the_dimm_zone) {
    config_case_num = get_the_config_case_num(ddr_type, ch_width, chnl_num_of_dimms, chnl_num_of_ranks,
        (ddr_type == 1) ? num_of_ranks :  get_dimm_l_number_of_ranks(mad_dimm_l[chan]), // Separating ddr5 from other techs - different implemntation
        (ddr_type == 1) ? dimm_width   :  get_dimm_l_width(mad_dimm_l[chan]),
        same_dimm_width_or_one_dimm,
        is_8Gb_device,
        !rerun_zone_1,
        lpddr5_bg_mode);

    // get the exact configuration case
    config_case_num_int_for_switch = config_case_num;
    switch( config_case_num_int_for_switch )
    {
      case 18 : config_case_num = (num_of_ranks == 2)                    ?   10182  :   10181; break;
      case 80 : config_case_num = (num_of_ranks == 2)                    ?   10802  :   10801; break;
      case 23 : config_case_num = (num_of_ranks == 2)                    ?   10232  :   10231; break;
      case 85 : config_case_num = (num_of_ranks == 2)                    ?   10852  :   10851; break;
      case 24 : config_case_num = (dimm_width == 8)                   ?   10241  :   10242; break;
      case 86 : config_case_num = (dimm_width == 8)                   ?   10861  :   10862; break;
      case 25 : config_case_num = (dimm_width == 8)                   ?   10251  :   10252; break;
      case 87 : config_case_num = (dimm_width == 8)                   ?   10871  :   10872; break;
      case 26 : config_case_num = (dimm_width == 8)                   ?   10262  :   10261; break;
      case 88 : config_case_num = (dimm_width == 8)                   ?   10882  :   10881; break;
      case 27 : config_case_num = (dimm_width == 8)                   ?   10271  :   10272; break;
      case 89 : config_case_num = (dimm_width == 8)                   ?   10891  :   10892; break;
      case 28 : config_case_num = (dimm_width == 8)                   ?   10281  :   10282; break;
      case 90 : config_case_num = (dimm_width == 8)                   ?   10901  :   10902; break;
    }

    if (rerun_zone_1) {
      config_case_num_int_for_switch = config_case_num;
      switch( config_case_num_int_for_switch )
      {
        case 18 : config_case_num = (num_of_ranks == 2)                         ?   16    :   14; break;
        case 80 : config_case_num = (num_of_ranks == 2)                         ?   78    :   76; break;
        case 23 : config_case_num = (num_of_ranks == 2)                         ?   21    :   19; break;
        case 85 : config_case_num = (num_of_ranks == 2)                         ?   83    :   81; break;
      }
    };

    addr_decode_map_parse(MrcCall, config_arr, config_case_num, TRUE);

    if ( enhanced_interleave_mode ) {
      enhanced_mode_map_parse(MrcCall, enh_config_arr, config_case_num, TRUE);
    }

    converter_step = 1;
    for(i=0; i<DIMM_NUM_BITS; i++)
    {
      dimm_shifter[(DIMM_NUM_BITS-1)-i] = ( config_arr[i+converter_step] - 6 );
      dimm_enhanced_shifter[(DIMM_NUM_BITS-1)-i] = ( enh_config_arr[i+converter_step] - 6 );
    }

    converter_step += DIMM_NUM_BITS;
    for(i=0; i<RANK_NUM_BITS; i++)
    {
      rank_shifter[(RANK_NUM_BITS-1)-i] = ( config_arr[i+converter_step] - 6 );
      rank_enhanced_shifter[(RANK_NUM_BITS-1)-i] = ( enh_config_arr[i+converter_step] - 6 );
    }
    converter_step += RANK_NUM_BITS;
    for(i=0; i<BG_NUM_BITS; i++)
    {
      bg_shifter[(BG_NUM_BITS-1)-i] =  ( config_arr[i+converter_step] - 6 );
      bg_enhanced_shifter[(BG_NUM_BITS-1)-i] = ( enh_config_arr[i+converter_step] - 6 );
    }
    converter_step += BG_NUM_BITS;
    for(i=0; i<BANK_NUM_BITS; i++)
    {
      bank_shifter[(BANK_NUM_BITS-1)-i] =  ( config_arr[i+converter_step] - 6 );
      bank_enhanced_shifter[(BANK_NUM_BITS-1)-i] = ( enh_config_arr[i+converter_step] - 6 );
    }
    converter_step += BANK_NUM_BITS;
    for(i=0; i<ROW_NUM_BITS; i++)
    {
      row_shifter[(ROW_NUM_BITS-1)-i] =  ( config_arr[i+converter_step] - 6 );
    }
    converter_step += ROW_NUM_BITS;
    for(i=0; i<COL_NUM_BITS; i++)
    {
      col_shifter[(COL_NUM_BITS-1)-i] = ( config_arr[i+converter_step] - 6 );
      col_enhanced_shifter[i] = -6;
    }
    col_enhanced_shifter[10] = ( enh_config_arr[9] - 6 ); // enhanced mode for column[10:10]

    if (1)
    {
      DEBUG((DEBUG_INFO, "\nDEBUG:   high_order_rank_interleave=%s, "
            "enhanced_interleave_mode=%s, ",
            high_order_rank_interleave?L"TRUE":L"FALSE",
            enhanced_interleave_mode?L"TRUE":L"FALSE"));
    }

    // Take care of performance ECO (1303982985)
    if((ddr_type == 0 || ddr_type == 1) && bg0_field_value != 0){
      bg0_bit_select = 0;
      switch(bg0_field_value)
      {
        case 1: bg0_bit_select = 5; break;
        case 2: bg0_bit_select = 6; break;
      }
      i = col_shifter[bg0_bit_select];
      col_shifter[bg0_bit_select] = bg_shifter[0];
      bg_shifter[0] = i;
    }

    //lpddr5 4X4/16 banks & bg0_field_value==1 -> swap BG[1] with C[6]
    if (ddr_type == 2 && bg0_field_value == 1 &&(lpddr5_bg_mode == 0 || lpddr5_bg_mode == 2) ){

      bg0_bit_select = 6;
      i = col_shifter[bg0_bit_select];
      col_shifter[bg0_bit_select] = bg_shifter[1];
      bg_shifter[1] = i;
    }

    dimm_line = 0;

    // put in the col part of the address:

    for(i=0; i<(COL_NUM_BITS-1); i++)
    {

      if ( col_shifter[i] == -6 )
      {
        DEBUG((DEBUG_INFO, "")); //Do nothing
      } else if ( col_shifter[i] < 0 )
      {
        col_shifter[i] += 6;
        *p_sys_addr = *p_sys_addr | (UINT32) ( ((col >> i) & 1) << col_shifter[i]);
      } else {
        if ( enhanced_interleave_mode && ( col_enhanced_shifter[i] != -6 ) )
        {
          UINT32 Data;
          Data = (UINT32) (MrcCall->MrcRightShift64(dimm_line, col_enhanced_shifter[i]));
          Data &= 1;
          dimm_line = dimm_line ^ (Data << col_shifter[i]);
        } else {
          dimm_line = dimm_line | (UINT32) ( ((col >> i) & 1) << col_shifter[i]);
        }
      }
    }


    // put in the row part of the address
    for(i=0; i<ROW_NUM_BITS; i++)
    {

      if ( row_shifter[i] == -6 )
      {
        DEBUG((DEBUG_INFO, "")); //Do nothing
      } else {
        dimm_line = dimm_line | (UINT32) ( ((row >> i) & 1) << row_shifter[i]);
      }
    }

    if((dimm_line < MrcCall->MrcLeftShift64(dimm_s_size, 1)) && (rerun_zone_1==FALSE)) // Range 0 if DIMM address is less than DIMM S's size.
    {

      // insert dimm_select bit
      for (i=0; i<DIMM_NUM_BITS; i++)
      {
        if ( dimm_shifter[i] == -6 )
        {
          DEBUG((DEBUG_INFO, "")); //Do nothing
        } else {
          Temp = MrcCall->MrcRightShift64(dimm_select, i)&1;
          /* Original codes have bug that dimm_enhanced_shifter[i] might < 0, Fix it */
          if (enhanced_interleave_mode) {
            if (dimm_enhanced_shifter[i] == -6) {
              Temp ^= 0;
            } else
              Temp ^= MrcCall->MrcRightShift64(dimm_line, dimm_enhanced_shifter[i]) & 1;
          } else {
            Temp = MrcCall->MrcLeftShift64(Temp, dimm_shifter[i]);
          }
          dimm_line = dimm_line | Temp;
        }
      }
    }

    // put in the bank part of the address
    for(bankBit=0; bankBit<BANK_NUM_BITS; bankBit++)
    {

      bank_no_ebh[bankBit] = (bank >> bankBit) & 1;

      if ( bank_shifter[bankBit] == -6 )
      {
      } else if ( enhanced_interleave_mode )
      {

        // EBH encoding
        if (ebh_enable){
          switch (ddr_type)
          {
            case (1): // DDR5
              {
                for(xorBit = 0; xorBit < DDR5_EBH_BANK_NUM_OF_BITS; xorBit++){
                  if (ddr5_ebh_bank[width_idx][num_of_ranks-1][bankBit][xorBit] == 0) {
                    DEBUG((DEBUG_INFO, " %0d %s", xor_ebh[xorBit], (xorBit == DDR5_EBH_BANK_NUM_OF_BITS-1) ? "" : "^ "));
                    continue;
                  }
                  xor_ebh[xorBit] = MrcCall->MrcRightShift64(dimm_line, (ddr5_ebh_bank[width_idx][num_of_ranks-1][bankBit][xorBit])) & 1;
                  bank_no_ebh[bankBit] = bank_no_ebh[bankBit] ^ xor_ebh[xorBit];

                }
                break;
              }
            default: // not DDR5
              {
                if (XaB_enable){
                  // In DDR4 x16 EBH2 xor with bg1 and not bank2, in x8 it has no xor EBH2
                  if ((ddr_type == 0) && (bankBit == BANK_NUM_BITS-1)) continue;
                  for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
                    xor_ebh[xorBit] = MrcCall->MrcRightShift64(dimm_line, (XaB[bankBit][xorBit])) & 1;
                    bank_no_ebh[bankBit] = bank_no_ebh[bankBit] ^ xor_ebh[xorBit];

                  }
                }
                if (XbB_enable) {
                  if (ddr_type == 3 || ddr_type == 2){ //LP4
                    switch (bankBit)
                    {
                      case 0:
                        xor_ebh[0] = MrcCall->MrcRightShift64(dimm_line, (XbB_lp4[bankBit][0])) & (XbB_lp4[bankBit][0] == 0 ? 0 : 1) & 1;
                        xor_ebh[1] = MrcCall->MrcRightShift64(dimm_line, (XbB_lp4[bankBit][1])) & (XbB_lp4[bankBit][1] == 0 ? 0 : 1) & ( dlnor == 0 ? 1 : 0);
                        bank_no_ebh[bankBit] = bank_no_ebh[bankBit] ^xor_ebh[0]^xor_ebh[1];

                        break;
                      case 1:
                        xor_ebh[0] = MrcCall->MrcRightShift64(dimm_line, (XbB_lp4[bankBit][0])) & (XbB_lp4[bankBit][0] == 0 ? 0 : 1) & (dlnor<2 ? 1 : 0);
                        xor_ebh[1] = MrcCall->MrcRightShift64(dimm_line, (XbB_lp4[bankBit][1])) & (XbB_lp4[bankBit][1] == 0 ? 0 : 1) & (dlnor == 0 ? 1 : 0);
                        bank_no_ebh[bankBit] = bank_no_ebh[bankBit] ^xor_ebh[0]^xor_ebh[1];
                        break;
                      case 2:
                        xor_ebh[0] = MrcCall->MrcRightShift64(dimm_line, (XbB_lp4[bankBit][0])) & (XbB_lp4[bankBit][0] == 0 ? 0 : 1) & (dlnor<2 ? 1 : 0);
                        bank_no_ebh[bankBit] = bank_no_ebh[bankBit] ^ xor_ebh[0];
                        break;
                    }
                  } else if (ddr_type == 0){ //DDR4
                    switch (bankBit)
                    {
                      case 0:
                        xor_ebh[0] = MrcCall->MrcRightShift64(dimm_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][0]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][0] == 0 ? 0 : 1) & ((nor_g2 == 0) ? 1 : 0);
                        xor_ebh[1] = MrcCall->MrcRightShift64(dimm_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][1]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][1] == 0 ? 0 : 1) & nor_1;
                        xor_ebh[2] = MrcCall->MrcRightShift64(dimm_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][2]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][2] == 0 ? 0 : 1) & 1;
                        xor_ebh[3] = MrcCall->MrcRightShift64(dimm_line, XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][3]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][3] == 0 ? 0 : 1) & 1;
                        bank_no_ebh[bankBit] = bank_no_ebh[bankBit] ^xor_ebh[0]^xor_ebh[1]^xor_ebh[2]^xor_ebh[3];
                        break;
                      case 1:
                        xor_ebh[0] = MrcCall->MrcRightShift64(dimm_line,  XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][0]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][0] == 0 ? 0 : 1) & ((nor_g2 == 0) ? 1 : 0);
                        xor_ebh[1] = MrcCall->MrcRightShift64(dimm_line,  XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][1]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][1] == 0 ? 0 : 1) & nor_1;
                        xor_ebh[2] = MrcCall->MrcRightShift64(dimm_line,  XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][2]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][2] == 0 ? 0 : 1) & 1;
                        xor_ebh[3] = MrcCall->MrcRightShift64(dimm_line,  XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][3]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][bankBit][3] == 0 ? 0 : 1) & 1;
                        bank_no_ebh[bankBit] = bank_no_ebh[bankBit] ^xor_ebh[0]^xor_ebh[1]^xor_ebh[2]^xor_ebh[3];
                        break;
                    }
                  }
                }
              }
          }
        }


        dimm_line = dimm_line | (UINT32) ( ((bank_no_ebh[bankBit] & 1) ^ ((UINT32)(MrcCall->MrcRightShift64(dimm_line,  bank_enhanced_shifter[bankBit]) & 1)) ) << bank_shifter[bankBit]);
      }
      else
      {
        dimm_line = dimm_line | (UINT32) ((bank_no_ebh[bankBit] & 1) << bank_shifter[bankBit]);
      }
    }

    // put in the bg part of the address
    if(ddr_type == 0 || ddr_type == 1 || ((ddr_type == 2) && (lpddr5_bg_mode == 0)) || ((ddr_type == 2) && (lpddr5_bg_mode == 2)) ){
      for(i=0; i<BG_NUM_BITS; i++)
      {
        if ( bg_shifter[i] == -6 )
        {
        }
        else if ( enhanced_interleave_mode )
        {
          bg_no_ebh[i] = (bg >> i) & 1;
          if (ebh_enable)
          {
            switch (ddr_type)
            {
              case (0): // DDR4
                {
                  if (dimm_width == 8 &&  i == 1){ // EBH on bg1 in DDR4
                    if (XaB_enable){

                      for(xorBit = 0; xorBit < XaB_NUM_OF_BITS; xorBit++){
                        if (XaB[2][xorBit] == 0) continue;
                        xor_ebh[xorBit] = (MrcCall->MrcRightShift64(dimm_line,  XaB[2][xorBit]) & 1);
                        bg_no_ebh[i] = bg_no_ebh[i] ^ xor_ebh[xorBit];

                      }
                    }
                    if (XbB_enable) {
                      xor_ebh[0] = MrcCall->MrcRightShift64(dimm_line,  XbB_ddr4[dimm_width == 8 ? 0 : 1][2][0]) & (XbB_ddr4[dimm_width == 8 ? 0 : 1][2][0] == 0 ? 0 : 1) & nor_1;
                      bg_no_ebh[i] = bg_no_ebh[i] ^ xor_ebh[0];
                    }
                    break;
                  }
                }
              case (1): //DDR5
                {
                  if (i == 0) break;
                  for(xorBit = 0; xorBit < DDR5_EBH_BG_NUM_OF_BITS; xorBit++){
                    xor_ebh[xorBit] = 0;
                    if (ddr5_ebh_bg[width_idx][num_of_ranks-1][i-1][xorBit] == 0)
                    {
                      continue;
                    }
                    xor_ebh[xorBit] = (MrcCall->MrcRightShift64(dimm_line,  ddr5_ebh_bg[width_idx][num_of_ranks-1][i-1][xorBit]) & 1);
                    bg_no_ebh[i] = bg_no_ebh[i] ^ xor_ebh[xorBit];

                  }
                }
            }
          }

          if (i == 0 && (ddr_type == 0  || ddr_type == 1 )) {
            bg_no_ebh[i] = extended_bank_group_hasher(MrcCall, 11, bg_no_ebh[i], dimm_line);
          }
          dimm_line = dimm_line | (UINT32) ( (bg_no_ebh[i] ^ ((UINT32)(MrcCall->MrcRightShift64(dimm_line,  bg_enhanced_shifter[i]) & 1))) << bg_shifter[i]);
        }
        else
        {
          dimm_line = dimm_line | (UINT32) ( ((bg >> i) & 1) << bg_shifter[i]);
        }
      }
    }


    // put in the rank part of the address
    for(i=0; i<RANK_NUM_BITS; i++)
    {

      if ( rank_shifter[i] == -6 )
      {
      }
      else if ( enhanced_interleave_mode )
      {
        dimm_line = dimm_line | (UINT32) ( (((rank >> i) & 1) ^ ((UINT32)(MrcCall->MrcRightShift64(dimm_line,  rank_enhanced_shifter[i]) & 1)) ) << rank_shifter[i]);
      }
      else
      {
        dimm_line = dimm_line | (UINT32) ( ((rank >> i) & 1) << rank_shifter[i]);
      }
      // In case of ERM we have to put the high bit of the rank in bg[1]
      if(num_of_ranks >2){

        if ( bg_shifter[i+1] == -6 )
        {
      }
      else if ( enhanced_interleave_mode )
      {
        dimm_line = dimm_line | (UINT32) ( (((rank >> (i+1)) & 1) ^ ((UINT32)(MrcCall->MrcRightShift64(dimm_line,  bg_enhanced_shifter[i+1]) & 1)) ) << bg_shifter[i+1]);
      }
      else
      {
        dimm_line = dimm_line | (UINT32) ( ((rank >> (i+1)) & 1) << bg_shifter[i+1]);
      }

    }
  }

  // put in the col[10:10] part of the address:
  for(i=(COL_NUM_BITS-1); i<COL_NUM_BITS; i++)
  {

    if ( col_shifter[i] == -6 )
    {
    }
    else if ( col_shifter[i] < 0 )
    {
      col_shifter[i] += 6; //azaretsk- check with Elad
      *p_sys_addr = *p_sys_addr | (UINT32) ( ((col >> i) & 1) << col_shifter[i]);
    }
    else
    {
      if ( enhanced_interleave_mode && ( col_enhanced_shifter[i] != -6 ) )
      {
        dimm_line = dimm_line | (UINT32) ( (((col >> i) & 1) ^ ((UINT32)(MrcCall->MrcRightShift64(dimm_line,  col_enhanced_shifter[i]) & 1)) ) << col_shifter[i]);
      }
      else
      {
        dimm_line = dimm_line | (UINT32) ( ((col >> i) & 1) << col_shifter[i]);
      }
    }
  }

  // Support in ERM 3 ranks
  // 1 sub channel 3 ranks:
  // Rank_sel_h,Rnk_sel = Modulus3(Zone_address[33:14])
  // Row[16:0], BANK[2] = Div3(Zone_address[33:14])
  // 2 sub channel 3 rank:
  // Rnk_sel_h,Rnk_sel = Modulus3( Zone_address[34:15])
  // Row[16:0], BANK[2] = Div3(Zone_address[34:15])
  if (num_of_ranks == 3){
    dimm_line = dimm_line & (~(MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, (8 + (chnl_num_of_dimms == 2)))));
    dimm_line = dimm_line | ((3*((row << 1) | ((bank >> 2) & 1)) + ((bg & 2)| (rank & 1))) << (8+ (chnl_num_of_dimms == 2)));
  }


  //
  // DIMM address to channel address
  //

  // Find the DIMM sizes on our selected channel. adjust to cache-line granularity
  dimm_l_size = get_dimm_l_size_as_cl(MrcCall, mad_dimm_l[chan]);
  dimm_s_size = get_dimm_s_size_as_cl(MrcCall, mad_dimm_l[chan]);



  // high_order_rank_interleave causes DIMM interleaving.
  //
  // DIMM and Rank interleaving (or HORI) enabled
  //
  //  +------16 GB------+
  //  |                 |
  //  .                 .
  //  .                 .
  //  .                 .
  //  |                 |
  //  |  unmapped       |
  //  |  space above    |
  //  |  channel        |
  //  |  capacity       |
  //  |                 |
  //  +--Range 1 Limit--+  <-- dimm_l_size + dimm_s_size
  //  |                 |
  //  |  No Interleave  |
  //  |     DIMM L      |
  //  |                 |
  //  +--Range 0 Limit--+  <-- dimm_s_size * 2
  //  |                 |
  //  | 2-Way Interleave|
  //  |   DIMMs L & S   |
  //  |                 |
  //  +-------0x0-------+
  //
  if((dimm_line < MrcCall->MrcLeftShift64(dimm_s_size, 1)) && (rerun_zone_1==FALSE)) // Range 0 if DIMM address is less than DIMM S's size.
  {

    // Determine if the channel hash feature is being used
    if( CHANNEL_EHASH & MC_ADDR_CHAN_HASH_ENABLE_MASK ) // test enable bit
    {
      dimm_line = MrcCall->MrcRightShift64(dimm_line, 1);


      hash_lsb_mask_bit = get_ch_hash_lsb_mask_bit( CHANNEL_EHASH );
      hash_mask = get_ch_hash_mask( CHANNEL_EHASH );

      // placing the chan_select bit in hash_lsb_mask_bit
      chan_line = (dimm_line & MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, hash_lsb_mask_bit)) << 1;
      chan_line = chan_line | (dimm_line & (~(MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, hash_lsb_mask_bit)))); // getting the highest and the lowest bits

      // Get the bits used to produce chan_select, sans the bit athash_lsb_mask_bit
      hash_line = ((UINT32) chan_line) & hash_mask;

      // Recreate the value of the bit at hash_lsb_mask_bit by doint the hash
      // XORs.
      //
      // I don't know of a single instruction to do this, so an unrollable
      // loop will be used.
      //
      hash_bit = 0;
      for( i = 0 ; i < MC_ADDR_CHANNEL_HASH_MASK_SIZE ; i++ )
        hash_bit = hash_bit ^ (hash_line >> i);
      hash_bit = hash_bit & 1;

      // Recreate the missing bit by XORing the chan_select (the result of
      // the forward decode).
      // (If X = A ^ B, then A = X ^ B)
      //
      hash_bit = hash_bit ^ ((UINT32)dimm_select);

      // put the missing bit back into the address
      chan_line = chan_line | (hash_bit << hash_lsb_mask_bit);
    }
    else
    {
      // insert dimm_select bit and convert to chan_line:
      for (i=0; i<DIMM_NUM_BITS; i++)
      {
        if ( dimm_shifter[i] == -6 )
        {
          chan_line = dimm_line;
        }
        else
        {
          Temp = MrcCall->MrcRightShift64(dimm_select, i)&1;
          /* Original codes have bug that dimm_enhanced_shifter[i] might < 0, Fix it */
          if (enhanced_interleave_mode) {
            if (dimm_enhanced_shifter[i] == -6) {
              Temp ^= 0;
            } else
              Temp ^= MrcCall->MrcRightShift64(dimm_line, dimm_enhanced_shifter[i]) & 1;
          } else {
            Temp = MrcCall->MrcLeftShift64(Temp, dimm_shifter[i]);
          }
          chan_line = dimm_line | Temp;
        }
      }
    }
    found_the_dimm_zone = TRUE;
  }
  else // not Range 0, must be Range 1, no interleave
  {
    // Channel address is DIMM L address with DIMM S's contribution from Range 0 added in
    chan_line = dimm_line + dimm_s_size;

    if(rerun_zone_1==FALSE)
    {
      rerun_zone_1=TRUE;
    }
    else
    {
      found_the_dimm_zone = TRUE;

      if (dimm_line > dimm_l_size) {
        return FALSE;
      }
    }
  }
    }

    //
    // Channel address to remaped system address
    //

    // map physical channel to L or S
    //
    chan_select = physical_to_logical_chan(MAD_INTER_CHNL, chan);

    // Determine which range/zone the remapped system address falls into
    //
    //  +-----0.5 TB-----+
    //  |                |
    //  .                .
    //  .                .
    //  .                .
    //  |                |
    //  |  unmapped      |
    //  |  space above   |
    //  |  DRAM          |
    //  |                |
    //  +--Zone 1 Limit--+  <-- Top Of Memory = L + S
    //  |                |
    //  |  No Interleave |
    //  |    Channel L   |
    //  |                |
    //  +--Zone 0 Limit--+  <-- 2 * S
    //  |                |
    //  |  2-Way Intlv   |
    //  | Channels L & S |
    //  |                |
    //  |                |
    //  |                |
    //  |                |
    //  |                |
    //  |                |
    //  +-------0x0------+
    //

    if( chan_line < (ch_s_size) ) // Zone 0
    {

      // Determine if the channel hash feature is being used
      if( CHANNEL_HASH & MC_ADDR_CHAN_HASH_ENABLE_MASK ) // test enable bit
      {
        hash_lsb_mask_bit = get_ch_hash_lsb_mask_bit( CHANNEL_HASH );
        hash_mask = get_ch_hash_mask( CHANNEL_HASH );

        // placing the chan_select bit in hash_lsb_mask_bit
        remap_line = MrcCall->MrcLeftShift64(chan_line & MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, hash_lsb_mask_bit), 1); // getting the highest bit
        remap_line = remap_line | (chan_line & (~MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, hash_lsb_mask_bit))); // getting the highest bit

        // Get the bits used to produce chan_select, sans the bit athash_lsb_mask_bit
        hash_line = ((UINT32) remap_line) & hash_mask;

        // Recreate the value of the bit at hash_lsb_mask_bit by doint the hash
        // XORs.
        //
        // I don't know of a single instruction to do this, so an unrollable
        // loop will be used.
        //
        hash_bit = 0;
        for( i = 0 ; i < MC_ADDR_CHANNEL_HASH_MASK_SIZE ; i++ )
          hash_bit = hash_bit ^ (hash_line >> i);
        hash_bit = hash_bit & 1;

        // Recreate the missing bit by XORing the chan_select (the result of
        // the forward decode).
        // (If X = A ^ B, then A = X ^ B)
        //
        hash_bit = hash_bit ^ chan_select;

        // put the missing bit back into the address
        remap_line = remap_line | (hash_bit << hash_lsb_mask_bit);
      }
      else
      {
        // Without the hash, sys_addr[6:6] determines the channel
        remap_line = MrcCall->MrcLeftShift64(chan_line, 1);
        remap_line |= ((UINT64) chan_select);
      }
    }
    else // Zone 1
    {

      remap_line = chan_line + (ch_s_size);
    }

    // work on full address instead of cache-line address;
    remap_addr = MrcCall->MrcLeftShift64(remap_line, 6);


    // Determine if the address is under the remap zone and therefore must be a
    // TCM.  remap_line can't be at or above TOM (Top Of Memory), so no need to
    // check that.  Simply check if the remap_line is between the base and
    // limit.
    //
    *p_is_tcm = 0;

    //
    // reverse decode the remap region
    //
    *p_sys_addr = remap_addr; // if the remap doesn't apply system address is remap address

    // Restore cache-line chunk order
    if(ch_width == 32){
      *p_sys_addr = *p_sys_addr | (MrcCall->MrcLeftShift64(((UINT64) col), 2) & 0x3FULL);
    }
    else if (ch_width == 16){
      *p_sys_addr = *p_sys_addr | (MrcCall->MrcLeftShift64(((UINT64) col), 1) & 0x3FULL);
    }
    else{
      *p_sys_addr = *p_sys_addr | (MrcCall->MrcLeftShift64(((UINT64) col), 3) & 0x3FULL);
    }

    remap_addr = *p_sys_addr;
    MC_slice_address = remap_addr;

    slice_high_bits_gb = (UINT32) MrcCall->MrcRightShift64(remap_addr, 30);

    slice_high_bits_h_gb = (UINT32) MrcCall->MrcRightShift64(remap_addr, 29);

    slice_zone1_start = get_mc_zone1_start(MAD_MC_HASH);

    slice_stacked_mode = get_mc_stacked_mode(MAD_MC_HASH);
    slice_hash_enable  = get_mc_hash_enable(MAD_MC_HASH);
    if (((slice_high_bits_h_gb >= (slice_zone1_start)) && slice_hash_enable)|| slice_stacked_mode){
      {
        if (slice_stacked_mode) {
        }
        DEBUG((DEBUG_INFO, "\nDEBUG:  slice_high_bits_gb = %x", slice_high_bits_gb));
        DEBUG((DEBUG_INFO, "\nDEBUG:  slice_high_bits_h_gb = %x", slice_high_bits_h_gb));
      }
      sliced_address = ((slice_high_bits_h_gb) + (slice_zone1_start)) ; // sliced_address := sys_addr[38:30] + zone1_start[8:1]

      sliced_address = MrcCall->MrcLeftShift64(sliced_address, 29) | (remap_addr & 0x1FFFFFFFULL); // sliced_address := sliced_address[8:0]*sys_addr[29:0]


      *p_sys_addr = sliced_address;
    }else if (slice_hash_enable){


      hash_lsb_mask_bit = get_mc_hash_lsb(MAD_MC_HASH);


      // placing the chan_select bit in hash_lsb_mask_bit
      sliced_address = MrcCall->MrcLeftShift64((remap_addr & MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, hash_lsb_mask_bit + 6)), 1); // getting the highest bit
      sliced_address = sliced_address | (remap_addr & (~MrcCall->MrcLeftShift64(0xFFFFFFFFFFFFFFFFULL, hash_lsb_mask_bit + 6))); // getting the highest bit


      // Recreate the value of the bit at hash_lsb_mask_bit by doint the hash
      // XORs.
      //
      // I don't know of a single instruction to do this, so an unrollable
      // loop will be used.
      //
      hash_bit = mc_select;


      // put the missing bit back into the address
      sliced_address = sliced_address | (hash_bit << (hash_lsb_mask_bit + 6));


      *p_sys_addr = sliced_address;
    } else {
    }


    // successful reverse address decode

    //rerun_zone_1 = 1;
    if (tom < remap_addr) {
    };

    *p_sys_addr = MC_slice_address;
    return TRUE;
}

/**
  Address decode function
  Converts memory address to DRAM address

  @param[in]      MemoryAddress  - The 39-bit memory address to decode.
  @param[out]     DramAddress    - The DRAM address struct that the memory address decodes to.

  @retval Returns EFI_SUCCESS if successful, EFI_UNSUPPORTED otherwise.
 **/
EFI_STATUS
EFIAPI MrcMemoryAddressDecode (
    IN  UINT64          MemoryAddress,
    OUT DRAM_ADDRESS    *DramAddress
    )
{
  MRC_ADDR_FUNCTION CallTable;
  MRC_ADDR_FUNCTION *MrcCall;
  EFI_STATUS    Status;
  UINT32        MchBarBaseAddress;
  UINT32        PciEBaseAddress;
  UINT32        Offset;
  UINT64        RemapBase;
  UINT64        RemapLimit;
  UINT32        Tolud;
  UINT64        Tom;
  UINT32        ChannelHash;
  UINT32        ChannelEHash;
  UINT32        MadInterChannel;
  UINT32        MadIntraCh0;
  UINT32        MadIntraCh1;
  UINT32        MadDimmCh0;
  UINT32        MadDimmCh1;
  BOOLEAN       IsTcm;
  BOOLEAN       Result;
  UINT64        SC_GS_CFG;
  UINT32        MAD_MC_HASH;
  UINT32        MAD_SLICE;
  UINT32        MEMORY_SLICE_HASH;
  UINT32        p_slice; //Controller number
  UINT64        p_slice_addr;  //Address inputted to Controller

  MrcCall = &CallTable;
  Status  = EFI_UNSUPPORTED;

  //Init Controller to error
  DramAddress->Controller = 0xFF;

  //
  // Set function pointers
  //
#ifdef MRC_MINIBIOS_BUILD
  MrcCall->MrcIoRead32     = (MRC_IO_READ_32) (&MrcOemInPort32);
  MrcCall->MrcIoWrite32    = (MRC_IO_WRITE_32) (&MrcOemOutPort32);
  MrcCall->MrcMmioRead32   = (MRC_MMIO_READ_32) (&MrcOemMmioRead32);
  MrcCall->MrcMmioRead64   = (MRC_MMIO_READ_64) (&SaMmioRead64);
  MrcCall->MrcCopyMem      = (MRC_MEMORY_COPY) (&MrcOemMemoryCpy);
  MrcCall->MrcSetMem       = (MRC_MEMORY_SET_BYTE) (&MrcOemMemorySet);
#else
  MrcCall->MrcIoRead32     = (MRC_IO_READ_32) (&IoRead32);
  MrcCall->MrcIoWrite32    = (MRC_IO_WRITE_32) (&IoWrite32);
  MrcCall->MrcMmioRead32   = (MRC_MMIO_READ_32) (&MmioRead32);
  MrcCall->MrcMmioRead64   = (MRC_MMIO_READ_64) (&MmioRead64);
  MrcCall->MrcCopyMem      = (MRC_MEMORY_COPY) (&CopyMem);
  MrcCall->MrcSetMem       = (MRC_MEMORY_SET_BYTE) (&SetMem);
#endif // MRC_MINIBIOS_BUILD
  MrcCall->MrcMultU64x32   = (MRC_MULT_U64_U32) (&MultU64x32);
  MrcCall->MrcDivU64x64    = (MRC_DIV_U64_U64) (&DivU64x64Remainder);
  MrcCall->MrcLeftShift64  = (MRC_LEFT_SHIFT_64) (&LShiftU64);
  MrcCall->MrcRightShift64 = (MRC_RIGHT_SHIFT_64) (&RShiftU64);

  //
  // Check that MCHBAR is programmed
  //
  MrcCall->MrcIoWrite32 (0xCF8, MRC_BIT31 | R_SA_MCHBAR);
  MchBarBaseAddress = MrcCall->MrcIoRead32 (0xCFC);

  if ((MchBarBaseAddress & MRC_BIT0) == MRC_BIT0) {
    MchBarBaseAddress &= (~MRC_BIT0);
  } else {
    return Status;
  }

  //
  // Check that PCIEXBAR is programmed
  //
  MrcCall->MrcIoWrite32 (0xCF8, MRC_BIT31 | R_SA_PCIEXBAR);
  PciEBaseAddress = MrcCall->MrcIoRead32 (0xCFC);

  if ((PciEBaseAddress & MRC_BIT0) == MRC_BIT0) {
    PciEBaseAddress &= ~(MRC_BIT2 | MRC_BIT1 | MRC_BIT0);
  } else {
    return Status;
  }

  //
  // Collect address configuration from registers
  //
  Offset               = PciEBaseAddress + R_SA_TOLUD;
  Tolud                = MrcCall->MrcMmioRead32 (Offset);
  Tolud                = Tolud & MC_ADDR_TOLUD_MASK;
  Offset               = PciEBaseAddress + R_SA_TOM;
  Tom                  = MrcCall->MrcMmioRead32 (Offset + 4);
  Tom                  = MrcCall->MrcLeftShift64(Tom, 32);
  Tom                  = Tom | MrcCall->MrcMmioRead32 (Offset);
  Tom                  = Tom & MC_ADDR_TOM_MASK;
  RemapBase            = MrcCall->MrcMmioRead64 (MchBarBaseAddress + CMI_REMAP_BASE_REG);
  RemapBase            = RemapBase & MC_ADDR_REMAP_MASK;
  RemapLimit           = MrcCall->MrcMmioRead64 (MchBarBaseAddress + CMI_REMAP_LIMIT_REG);
  RemapLimit           = RemapLimit & MC_ADDR_REMAP_MASK;
  RemapLimit           |= MC_ADDR_REMAP_LIMIT_LOWER_BITS_MASK;
  MAD_SLICE            = MrcCall->MrcMmioRead32 (MchBarBaseAddress + CMI_MAD_SLICE_REG);
  MEMORY_SLICE_HASH    = MrcCall->MrcMmioRead32 (MchBarBaseAddress + CMI_MEMORY_SLICE_HASH_REG);
  IsTcm                = FALSE;
  Result = mc_sys_to_cmi_slice_addr_decode(MrcCall,
      MemoryAddress,
      &IsTcm,
      MAD_SLICE,
      MEMORY_SLICE_HASH,
      Tolud,
      RemapBase,
      RemapLimit,
      Tom,
      0,
      &p_slice,
      &p_slice_addr);
  if (!Result) {
    return EFI_INVALID_PARAMETER;
  }

  if (p_slice == 0) {
    ChannelHash          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_CHANNEL_HASH_REG);
    ChannelEHash         = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_CHANNEL_EHASH_REG);
    MadInterChannel      = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_INTER_CHANNEL_REG);
    MadIntraCh0          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_INTRA_CH0_REG);
    MadIntraCh1          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_INTRA_CH1_REG);
    MadDimmCh0           = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_DIMM_CH0_REG);
    MadDimmCh1           = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_DIMM_CH1_REG);
    MAD_MC_HASH          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_MC_HASH_REG);
    SC_GS_CFG            = MrcCall->MrcMmioRead64 (MchBarBaseAddress + MC0_CH0_CR_SC_GS_CFG_REG);
  } else {
    ChannelHash          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_CHANNEL_HASH_REG);
    ChannelEHash         = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_CHANNEL_EHASH_REG);
    MadInterChannel      = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_INTER_CHANNEL_REG);
    MadIntraCh0          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_INTRA_CH0_REG);
    MadIntraCh1          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_INTRA_CH1_REG);
    MadDimmCh0           = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_DIMM_CH0_REG);
    MadDimmCh1           = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_DIMM_CH1_REG);
    MAD_MC_HASH          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_MC_HASH_REG);
    SC_GS_CFG            = MrcCall->MrcMmioRead64 (MchBarBaseAddress + MC1_CH0_CR_SC_GS_CFG_REG);
  }

  /*
   * On ADL, cnl_mc_addr_decode decodes the full CMI address.
   * Here we need pass full MemoryAddress
   */
  Result = tgl_cmi_slice_to_dram_addr_decode(
      MrcCall,
      MemoryAddress,
      MAD_MC_HASH,
      ChannelHash,
      ChannelEHash,
      MadInterChannel,
      MadIntraCh0,
      MadIntraCh1,
      MadDimmCh0,
      MadDimmCh1,
      SC_GS_CFG,
      Tom,
      0,
      &DramAddress->ChannelNumber,
      &DramAddress->DimmNumber,
      &DramAddress->Rank,
      &DramAddress->BankGroup,
      &DramAddress->Bank,
      &DramAddress->Ras,
      &DramAddress->Cas);

  if (Result) {
    Status = EFI_SUCCESS;
    DramAddress->Socket = 0;
    DramAddress->Controller = p_slice;
  }

  return Status;
}

/**
  Address encode function (reverse address decode)
  DRAM address to memory address conversion

  @param[in]      DramAddress    - The DRAM address that is to be encoded.
  @param[out]     MemoryAddress  - The 39-bit memory address to convert to.

  @retval Returns EFI_SUCCESS if successful, EFI_UNSUPPORTED otherwise.
 **/
EFI_STATUS
EFIAPI MrcMemoryAddressEncode (
    IN  DRAM_ADDRESS *DramAddress,
    OUT UINT64           *MemoryAddress
    )
{
  MRC_ADDR_FUNCTION CallTable;
  MRC_ADDR_FUNCTION *MrcCall;
  EFI_STATUS    Status;
  UINT32        MchBarBaseAddress;
  UINT32        PciEBaseAddress;
  UINT32        Offset;
  BOOLEAN       IsTcm;
  UINT64        RemapBase;
  UINT64        RemapLimit;
  UINT32        Tolud;
  UINT64        Tom;
  UINT32        ChannelHash;
  UINT32        ChannelEHash;
  UINT32        MadInterChannel;
  UINT32        MadIntraCh0;
  UINT32        MadIntraCh1;
  UINT32        MadDimmCh0;
  UINT32        MadDimmCh1;
  UINT64        SC_GS_CFG;
  UINT32        MAD_MC_HASH;
  UINT32        MAD_SLICE;
  UINT32        MEMORY_SLICE_HASH;
  UINT32        mc_select;
  UINT64        CMIAddress;
  BOOLEAN       Result;

  MrcCall = &CallTable;
  Status = EFI_UNSUPPORTED;

  //Init
  *MemoryAddress = 0x0;

  //
  // Set function pointers
  //
#ifdef MRC_MINIBIOS_BUILD
  MrcCall->MrcIoRead32     = (MRC_IO_READ_32) (&MrcOemInPort32);
  MrcCall->MrcIoWrite32    = (MRC_IO_WRITE_32) (&MrcOemOutPort32);
  MrcCall->MrcMmioRead32   = (MRC_MMIO_READ_32) (&MrcOemMmioRead32);
  MrcCall->MrcMmioRead64   = (MRC_MMIO_READ_64) (&SaMmioRead64);
  MrcCall->MrcCopyMem      = (MRC_MEMORY_COPY) (&MrcOemMemoryCpy);
  MrcCall->MrcSetMem       = (MRC_MEMORY_SET_BYTE) (&MrcOemMemorySet);
#else
  MrcCall->MrcIoRead32     = (MRC_IO_READ_32) (&IoRead32);
  MrcCall->MrcIoWrite32    = (MRC_IO_WRITE_32) (&IoWrite32);
  MrcCall->MrcMmioRead32   = (MRC_MMIO_READ_32) (&MmioRead32);
  MrcCall->MrcMmioRead64   = (MRC_MMIO_READ_64) (&MmioRead64);
  MrcCall->MrcCopyMem      = (MRC_MEMORY_COPY) (&CopyMem);
  MrcCall->MrcSetMem       = (MRC_MEMORY_SET_BYTE) (&SetMem);
#endif // MRC_MINIBIOS_BUILD
  MrcCall->MrcMultU64x32   = (MRC_MULT_U64_U32) (&MultU64x32);
  MrcCall->MrcDivU64x64    = (MRC_DIV_U64_U64) (&DivU64x64Remainder);
  MrcCall->MrcLeftShift64  = (MRC_LEFT_SHIFT_64) (&LShiftU64);
  MrcCall->MrcRightShift64 = (MRC_RIGHT_SHIFT_64) (&RShiftU64);

  //
  // Check that MCHBAR is programmed
  //
  MrcCall->MrcIoWrite32 (0xCF8, MRC_BIT31 | R_SA_MCHBAR);
  MchBarBaseAddress = MrcCall->MrcIoRead32 (0xCFC);

  if ((MchBarBaseAddress & MRC_BIT0) == MRC_BIT0) {
    MchBarBaseAddress &= (~MRC_BIT0);
  } else {
    return Status;
  }

  //
  // Check that PCIEXBAR is programmed
  //
  MrcCall->MrcIoWrite32 (0xCF8, MRC_BIT31 | R_SA_PCIEXBAR);
  PciEBaseAddress = MrcCall->MrcIoRead32 (0xCFC);

  if ((PciEBaseAddress & MRC_BIT0) == MRC_BIT0) {
    PciEBaseAddress &= ~(MRC_BIT2 | MRC_BIT1 | MRC_BIT0);
  } else {
    return Status;
  }

  //
  // Collect address configuration from registers
  //
  Offset               = PciEBaseAddress + R_SA_TOLUD;
  Tolud                = MrcCall->MrcMmioRead32 (Offset);
  Tolud                = Tolud & MC_ADDR_TOLUD_MASK;
  Tom                  = MrcCall->MrcMmioRead32 (Offset + 4);
  Tom                  = MrcCall->MrcLeftShift64(Tom, 32);
  Tom                  = Tom | MrcCall->MrcMmioRead32 (Offset);
  Tom                  = Tom & MC_ADDR_TOM_MASK;
  RemapBase            = MrcCall->MrcMmioRead64 (MchBarBaseAddress + CMI_REMAP_BASE_REG);
  RemapBase            = RemapBase & MC_ADDR_REMAP_MASK;
  RemapLimit           = MrcCall->MrcMmioRead64 (MchBarBaseAddress + CMI_REMAP_LIMIT_REG);
  RemapLimit           = RemapLimit & MC_ADDR_REMAP_MASK;
  RemapLimit           |= MC_ADDR_REMAP_LIMIT_LOWER_BITS_MASK;
  MAD_SLICE            = MrcCall->MrcMmioRead32 (MchBarBaseAddress + CMI_MAD_SLICE_REG);
  MEMORY_SLICE_HASH    = MrcCall->MrcMmioRead32 (MchBarBaseAddress + CMI_MEMORY_SLICE_HASH_REG);

  if (DramAddress->Controller == 0) {
    ChannelHash          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_CHANNEL_HASH_REG);
    ChannelEHash         = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_CHANNEL_EHASH_REG);
    MadInterChannel      = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_INTER_CHANNEL_REG);
    MadIntraCh0          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_INTRA_CH0_REG);
    MadIntraCh1          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_INTRA_CH1_REG);
    MadDimmCh0           = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_DIMM_CH0_REG);
    MadDimmCh1           = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_DIMM_CH1_REG);
    MAD_MC_HASH          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC0_MAD_MC_HASH_REG);
    SC_GS_CFG            = MrcCall->MrcMmioRead64 (MchBarBaseAddress + MC0_CH0_CR_SC_GS_CFG_REG);
  } else if (DramAddress->Controller == 1) {
    ChannelHash          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_CHANNEL_HASH_REG);
    ChannelEHash         = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_CHANNEL_EHASH_REG);
    MadInterChannel      = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_INTER_CHANNEL_REG);
    MadIntraCh0          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_INTRA_CH0_REG);
    MadIntraCh1          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_INTRA_CH1_REG);
    MadDimmCh0           = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_DIMM_CH0_REG);
    MadDimmCh1           = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_DIMM_CH1_REG);
    MAD_MC_HASH          = MrcCall->MrcMmioRead32 (MchBarBaseAddress + MC1_MAD_MC_HASH_REG);
    SC_GS_CFG            = MrcCall->MrcMmioRead64 (MchBarBaseAddress + MC1_CH0_CR_SC_GS_CFG_REG);
  } else
    return Status;
  mc_select = 0;

  Result = cnl_mc_addr_encode(
      MrcCall,
      DramAddress->ChannelNumber,
      DramAddress->DimmNumber,
      DramAddress->Rank,
      DramAddress->BankGroup,
      DramAddress->Bank,
      DramAddress->Ras,
      DramAddress->Cas,
      MAD_MC_HASH,
      ChannelHash,
      ChannelEHash,
      MadInterChannel,
      MadIntraCh0,
      MadIntraCh1,
      MadDimmCh0,
      MadDimmCh1,
      SC_GS_CFG,
      Tom,
      mc_select,
      &CMIAddress,
      &IsTcm);
  if (Result) {
    Result = cmi_slice_addr_to_mc_sys_addr_encode(
        MrcCall,
        DramAddress->Controller,
        CMIAddress,
        MAD_SLICE,
        MEMORY_SLICE_HASH,
        Tolud,
        RemapBase,
        RemapLimit,
        0,
        MemoryAddress,
        &IsTcm
        );
  }
  if (Result) {
    Status = EFI_SUCCESS;
  }

  return Status;
}

