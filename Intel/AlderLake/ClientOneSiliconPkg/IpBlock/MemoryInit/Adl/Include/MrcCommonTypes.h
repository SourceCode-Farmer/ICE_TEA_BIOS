/** @file
  This file contains the definitions common to the MRC API and other APIs.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/
#ifndef _MrcCommonTypes_h_
#define _MrcCommonTypes_h_

#ifndef MRC_INT32_MIN
#define MRC_INT32_MIN                       (0x80000000)
#endif

#ifndef MRC_INT32_MAX
#define MRC_INT32_MAX                       (0x7FFFFFFF)
#endif

#ifndef MRC_INT16_MIN
#define MRC_INT16_MIN                       (0x8000)
#endif

#ifndef MRC_INT16_MAX
#define MRC_INT16_MAX                       (0x7FFF)
#endif

#ifndef MRC_INT8_MAX
#define MRC_INT8_MAX                       (0x7F)
#endif

#ifndef MRC_INT8_MIN
#define MRC_INT8_MIN                       (0x80)
#endif

///
/// System boot mode.
///
#ifndef __MRC_BOOT_MODE__
#define __MRC_BOOT_MODE__                 //These values are cloned to MemInfoHob.h manually
typedef enum {
  bmCold,                                 ///< Cold boot
  bmWarm,                                 ///< Warm boot
  bmS3,                                   ///< S3 resume
  bmFast,                                 ///< Fast boot
  MrcBootModeMax,                         ///< MRC_BOOT_MODE enumeration maximum value.
  MrcBootModeDelim = MRC_INT32_MAX            ///< This value ensures the enum size is consistent on both sides of the PPI.
} MRC_BOOT_MODE;
#endif  //__MRC_BOOT_MODE__

///
/// DIMM memory package
/// This enum matches SPD Module Type - SPD byte 3, bits [3:0]
/// Note that DDR3, DDR4, DDR5, and LPDDR have different encoding for some module types
///
typedef enum {
  RDimmMemoryPackage          = 1,
  UDimmMemoryPackage          = 2,
  SoDimmMemoryPackage         = 3,
  LrDimmMemoryPackage         = 4,        // LRDIMM in DDR4 and DDR5
  MiniRDimmMemoryPackage      = 5,
  MiniUDimmMemoryPackage      = 6,
  MiniCDimmMemoryPackage      = 7,
  LpDimmMemoryPackage         = 7,
  SoRDimmEccMemoryPackageDdr4 = 8,
  SoUDimmEccMemoryPackageDdr4 = 9,
  SoCDimmEccMemoryPackage     = 10,
  MemoryDownMemoryPackage     = 11,       // Memory Down in DDR4 and DDR5
  SoDimm16bMemoryPackage      = 12,
  SoDimm32bMemoryPackage      = 13,
  NonDimmMemoryPackage        = 14,       // Non-DIMM solution in LPDDR
  DoubleSize1DpcMemoryPackage = 14,       // Not part of JEDEC spec
  DoubleSize2DpcMemoryPackage = 15,       // Not part of JEDEC spec
  MemoryPackageMax,                       ///< MEMORY_PACKAGE enumeration maximum value.
  MemoryPackageDelim = MRC_INT32_MAX      ///< This value ensures the enum size is consistent on both sides of the PPI.
} MEMORY_PACKAGE;

///
/// Memory training I/O levels.
///
typedef enum {
  DdrLevel   = 0,                         ///< Refers to frontside of DIMM.
  LrbufLevel = 1,                         ///< Refers to data level at backside of LRDIMM or AEP buffer.
  RegALevel  = 2,                         ///< Refers to cmd level at backside of register - side A.
  RegBLevel  = 3,                         ///< Refers to cmd level at backside of register - side B.
  HbmLevel   = 4,                         ///< Refers to HBM bus.
  GsmLtMax,                               ///< GSM_LT enumeration maximum value.
  GsmLtDelim = MRC_INT32_MAX                  ///< This value ensures the enum size is consistent on both sides of the PPI.
} GSM_LT;

///
/// GetSet Group Types.
///
typedef enum {
  RecEnDelay       = 0,         ///< Linear delay (PI ticks), where the positive increment moves the RCVEN sampling window later in time relative to the RX DQS strobes.
  RxDqsDelay       = 1,         ///< Linear delay (PI ticks), where the positive increment moves the RX DQS strobe later in time relative to the RX DQ signal (i.e. toward the hold side of the eye).
  RxDqDelay        = 2,         ///< Linear delay (PI ticks), where the positive increment moves the RX DQ byte/nibble/bitlane later in time relative to the RX DQS signal (i.e.closing the gap between DQ and DQS in the setup side of the eye).
  RxDqsPDelay      = 3,         ///< Linear delay (PI ticks), where the positive increment moves the RX DQS strobe for "even" chunks later in time relative to the RX DQ signal. Even chunks are 0, 2, 4, 6 within the 0 to 7 chunks of an 8 burst length cacheline, for example.
  RxDqsNDelay      = 4,         ///< Linear delay (PI ticks), where the positive increment moves the RX DQS strobe for "odd" chunks later in time relative to the RX DQ signal. Odd chunks are 1, 3, 5, 7 within the 0 to 7 chunks of an 8 burst length cacheline, for example.
  RxVref           = 5,         ///< Linear increment (Vref ticks), where the positive increment moves the byte/nibble/bitlane RX Vref to a higher voltage.
  RxEq             = 6,         ///< RX CTLE setting indicating a set of possible resistances, capacitance, current steering, etc. values, which may be a different set of values per product. The setting combinations are indexed by integer values.
  RxDqBitDelay     = 7,         ///< Linear delay (PI ticks), where the positive increment moves the RX DQ bitlane later in time relative to the RX DQS signal (i.e.closing the gap between DQ and DQS in the setup side of the eye).
  RxVoc            = 8,         ///< Monotonic increment (Sense Amp setting), where the positive increment moves the byte/nibble/bitlane's effective switching point to a lower Vref value.
  RxOdt            = 9,         ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  RxOdtUp          = 10,        ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  RxOdtDn          = 11,        ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  DramDrvStr       = 12,        ///< Drive strength setting resistance setting within a set of possible resistances (or currents), which may be a different set of values per product. Indexed by integer values.
  McOdtDelay       = 13,        ///< Dq Odt Control
  McOdtDuration    = 14,        ///< Dq Odt Control
  SenseAmpDelay    = 15,        ///< This may be used to indicate CmdToDiffAmpEn for SoC's.
  SenseAmpDuration = 16,        ///<
  RoundTripDelay   = 17,        ///< This may be used to indicate CmdToRdDataValid for SoC's.
  RxDqsBitDelay    = 18,        ///< Linear delay (PI ticks), where the positive increment moves the RX DQS within the bitlane later in time relative to the RX DQ signal (i.e.closing the gap between DQ and DQS in the hold side of the eye).
  RxDqDqsDelay     = 19,        ///< Linear delay (PI ticks), where the positive increment moves the RX DQS per strobe later in time relative to the RX DQ signal (i.e. closing the gap between DQS and DQ in the hold side of the eye. The difference between this parameter and RxDqsDelay is that both the DQ and DQS timings may be moved in order to increase the total range of DQDQS timings.
  WrLvlDelay       = 20,        ///< Linear delay (PI ticks), where the positive increment moves both the TX DQS and TX DQ signals later in time relative to all other bus signals.
  TxDqsDelay       = 21,        ///< Linear delay (PI ticks), where the positive increment moves the TX DQS strobe later in time relative to all other bus signals.
  TxDqsDelay90     = 22,        ///< Linear delay (PI ticks), where the positive increment moves the TX DQS strobe later in time relative to all other bus signals, for Gear4 Ph90.
  TxDqDelay        = 23,        ///< Linear delay (PI ticks), where the positive increment moves the TX DQ byte/nibble/bitlane later in time relative to all other bus signals.
  TxDqDelay90      = 24,        ///< Linear delay (PI ticks), where the positive increment moves the TX DQ byte/nibble/bitlane later in time relative to all other bus signals, for Gear4 Ph90.
  TxVref           = 25,        ///< Linear increment (Vref ticks), where the positive increment moves the byte/nibble/bitlane TX Vref to a higher voltage. (Assuming this will abstract away from the range specifics for DDR4, for example.)
  TxEq             = 26,        ///< TX EQ setting indicating a set of possible equalization levels, which may be a different set of values per product. The setting combinations are indexed by integer values.
  TxDqBitDelay     = 27,        ///< Linear delay (PI ticks), where the positive increment moves the TX DQ bitlane later in time relative to all other bus signals.
  TxDqBitDelay90   = 28,        ///< Linear delay (PI ticks), where the positive increment moves the TX DQ bitlane later in time relative to all other bus signals, for Gear4 Ph90.
  TxRon            = 29,        ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  TxRonUp          = 30,        ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  TxRonDn          = 31,        ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  TxSlewRate       = 32,        ///< Monotonic increment, where the positive increment moves the byte/nibble/bitlane's effective slew rate to a higher slope.
  TxImode          = 33,        ///< TX I-Mode Boost setting indicating a set of possible current boost levels, which may be a different set of values per product. The setting combinations are indexed by integer values.
  WrOdt            = 34,        ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  NomOdt           = 35,        ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  ParkOdt          = 36,        ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  TxTco            = 37,        ///< TCO Comp Code for DQ
  TxXtalk          = 38,        ///<
  RxCtleR          = 39,        ///< Rx Equalization Resistance control.
  RxCtleC          = 40,        ///< Rx Equalization Capacitance control.
  RxDqsPBitDelay   = 41,        ///< Linear delay (PI ticks), where the positive increment moves the RX DQS bitlane timing for "even" chunks later in time relative to the RX DQ bitlane signal. Even chunks are 0, 2, 4, 6 within the 0 to 7 chunks of an 8 burst length cacheline, for example.
  RxDqsNBitDelay   = 42,        ///< Linear delay (PI ticks), where the positive increment moves the RX DQS bitlane timing for "odd" chunks later in time relative to the RX DQ bitlane signal. Odd chunks are 1, 3, 5, 7 within the 0 to 7 chunks of an 8 burst length cacheline, for example.
  CmdAll           = 43,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CMD_ALL category later in time relative to all other signals on the bus.
  CmdGrp0          = 44,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CMD_GRP0 category later in time relative to all other signals on the bus.
  CmdGrp1          = 45,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CMD_GRP1 category later in time relative to all other signals on the bus.
  CmdGrp2          = 46,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CMD_GRP2 category later in time relative to all other signals on the bus.
  CtlAll           = 47,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CTL_ALL category later in time relative to all other signals on the bus.
  CtlGrp0          = 48,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CTL_GRP0 category later in time relative to all other signals on the bus.
  CtlGrp1          = 49,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CTL_GRP1 category later in time relative to all other signals on the bus.
  CtlGrp2          = 50,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CTL_GRP2 category later in time relative to all other signals on the bus.
  CtlGrp3          = 51,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CTL_GRP3 category later in time relative to all other signals on the bus.
  CtlGrp4          = 52,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CTL_GRP4 category later in time relative to all other signals on the bus.
  CtlGrp5          = 53,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CTL_GRP5 category later in time relative to all other signals on the bus.
  CmdCtlAll        = 54,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CMD_CTL_ALL category later in time relative to all other signals on the bus.
  CkAll            = 55,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CK_ALL category later in time relative to all other signals on the bus.
  CmdVref          = 56,        ///< Linear increment (Vref ticks), where the positive increment moves the CMD Vref to a higher voltage.
  AlertVref        = 57,        ///< Linear increment (Vref ticks), where the positive increment moves the ALERT Vref to a higher voltage.
  CmdRon           = 58,        ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  CmdGrpPi         = 59,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CMD_GRPx (Specified by Strobe index) category later in time relative to all other signals on the bus.
  CtlGrpPi         = 60,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CTL_GRPx (Specified by Strobe index) category later in time relative to all other signals on the bus.
  ClkGrpPi         = 61,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CLK_GRPx (Specified by Strobe index) category later in time relative to all other signals on the bus.
  ClkGrpG4Pi       = 62,        ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CLK_GRPx (Specified by Strobe index) category later in time relative to all other signals on the bus for Gear4 Ph90 Clock.
  TxCycleDelay     = 63,        ///< Clock Cycle delay, where the positive increment increases the delay
  EridDelay        = 64,        ///< Linear delay (PI ticks), where the positive increment moves the ERID signals later in time relative to the internal sampling clock (i.e.closing the gap between ERID and internal sampling clock in the setup side of the eye). This group is applicable for DDRT DIMMs.
  EridVref         = 65,        ///< Linear increment (Vref ticks), where the positive increment moves the ERID Vref to a higher voltage. This group is applicable for DDRT DIMMs.
  ErrorVref        = 66,        ///< Linear increment (Vref ticks), where the positive increment moves the ERROR Vref to a higher voltage. This group is applicable for DDRT DIMMs.
  ReqVref          = 67,        ///< Linear increment (Vref ticks), where the positive increment moves the REQ Vref to a higher voltage. This group is applicable for DDRT DIMMs.
  RecEnOffset      = 68,        ///< Linear delay (PI ticks), where the positive increment moves the RCVEN sampling window later in time relative to the RX DQS strobes.
  RxDqsOffset      = 69,        ///< Linear delay (PI ticks), where the positive increment moves the RX DQS strobe later in time relative to the RX DQ signal (i.e. toward the hold side of the eye).
  RxVrefOffset     = 70,        ///< Linear increment (Vref ticks), where the positive increment moves the byte/nibble/bitlane RX Vref to a higher voltage.
  TxDqsOffset      = 71,        ///< Linear delay (PI ticks), where the positive increment moves the TX DQS strobe later in time relative to all other bus signals.
  TxDqOffset       = 72,        ///< Linear delay (PI ticks), where the positive increment moves the TX DQ byte/nibble/bitlane later in time relative to all other bus signals.
  RxDqsBitOffset   = 73,        ///< Linear delay (PI ticks), read timing offset between Strobe P/N per lane t for bit[x] of the DQ byte
  RxDqsPiUiOffset  = 74,        ///< Linear delay (PI ticks), Subtracts 1 UI (64 PI Ticks) from Rx DQS_P and DQS_N Picode value to provide more range to find the left eye.
  CkeGrpPi,                     ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the CKE_GRPx (Specified by Strobe index) category later in time relative to all other signals on the bus.
  WckGrpPi,                     ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the WCK_GRPx (Specified by Strobe index) category later in time relative to all other signals on the bus.
  WckGrpPi90,                   ///< Linear delay (PI ticks), where the positive increment moves all signals assigned to the WCK_GRPx (Specified by Strobe index) category later in time relative to all other signals on the bus for Gear4 Ph90 Clock.
  DqsOdtDelay,                  ///< Dqs ODT Control
  DqsOdtDuration,               ///< Dqs ODT Control
  CompRcompOdtUpPerStrobe,      ///< The resistive value of the pull-up block of on die termination
  CompRcompOdtDnPerStrobe,      ///< The resistive value of the pull-dn block of on die termination
  RxDqsEq,                      ///< RX CTLE setting
  RxBiasCtl,                    ///< Control Bias current DAC which determines 1st and 2nd stage amp current.  Iref = 14% * BiasIrefAdj[3:0] where default is (0xA, 0x7) for NPath & PPath.
  RxLpddrMode,                   ///<Rx LP DDR Mode - Rx type selection. 0-LPDDR4x (matched P type) , 1-DDR4 (matched N type), 2-LPDDR5 (unmatched P type), 3-DDR5 (unmatched N type)
  RxBiasVrefSel,                ///< RxBias Vref Selection - used to set the amount of current to use for the reference bias.
  RxBiasTailCtl,                ///< Tail Current Trim Control
  RxDqsAmpOffset,               ///< RX DQS Amplifier Offset Cancellation (Voltage)
  RxDqsUnmatchedAmpOffset,      ///< RX DQS Amplifier Offset Cancellation for the unmatched path (Voltage)
  RxCben,                       ///< PI CB enable settings
  TxRankMuxDelay,               ///< Tx rank mux delay to account for the I/O setting latching time vs. the drive enable
  TxDqsRankMuxDelay,            ///< Tx rank mux delay for DQS to account for the I/O setting latching time vs. the drive enable
  RxRankMuxDelay,               ///< Rx rank mux delay to account for the I/O setting latching time vs. the receive enable
  RxFlybyDelay,                 ///< Per Rank, per Channel value that defines the additive delay on the Receive Enable signal.  (Number of Clocks per step vary by design)
  RxIoTclDelay,                 ///< Per Channel value that defines the additive delay on the Receive Enable signal related to the tCL of the DRAM.  (Number of Clocks per step vary by design) ?CH_SUB
  RoundTripIoComp,              ///< Per Channel offset between Receive Enable is required and when data is ready to go from DDRIO to MC.
  RxFifoRdEnFlybyDelay,         ///< Per Rank, per Channel offset between Read FIFO read enable is required and when data is ready to go from DDRIO to MC. ?CH_SUB
  RxFifoRdEnTclDelay,           ///< Per Channel offset between Read FIFO read enable is required and when data is ready to go from DDRIO to MC. ?CH_SUB
  RxDqDataValidDclkDelay,       ///< Per SubChannel, DCLK delay for DDRIO from Rx FIFO Ready. ?CH_SUB
  RxDqDataValidQclkDelay,       ///< Per SubChannel, QCLK delay for DDRIO from Rx FIFO Ready. ?CH_SUB
  RxRptChDqClkOn,               ///< Force clock ON for the RptCh DqRx logic
  TxRptChDqClkOn,               ///< Force clock ON for the RptCh DqTx logic
  TxDqFifoWrEnTcwlDelay,        ///< Per SubChannel, delay for the Write Enable of TX DQ FIFO. ?CH_SUB
  TxDqFifoRdEnTcwlDelay,       ///< ?CH_SUB
  TxDqFifoRdEnFlybyDelay,       ///< ?CH_SUB
  TxDqsTcoP,                    ///< Provides an unsigned delay control to DqsP-Fall/Rise TcoDelay.  Lower values slows down rise delay and higher values slow down fall delay.
  TxDqsTcoN,                    ///< Provides an unsigned delay control to DqsN-Fall/Rise TcoDelay.  Lower values slows down rise delay and higher values slow down fall delay.
  DefDrvEnLow,                  ///< Defined TX behavior when DrvEn is low: {0: Force On, 1: HiZ, 2: Return to Termination (See CR_RTO), 3: Non-Return to Zero}
  CmdTxEq,                      ///< CA Equalization codes: {4} Controls ConstantZ (1) vs. NonConstantZ (0), {3:0}={0: 0 Static Legs / 12 Eq Legs, ... 12: 12 Static Legs / 0 Eq Legs, 13-15: rsvd}
  CtlTxEq,                      ///< CTL Equalization codes: {4} Controls ConstantZ (1) vs. NonConstantZ (0), {3:0}={0: 0 Static Legs / 12 Eq Legs, ... 12: 12 Static Legs / 0 Eq Legs, 13-15: rsvd}
  TxEqCoeff0,                   ///<Compensation code for static legs when in equalization mode (both PUP and PDN); even bits are segment 0 and odd bits are segment 1
  TxEqCoeff1,                   ///<Compensation code for static legs when in equalization mode (both PUP and PDN); even bits are segment 0 and odd bits are segment 1
  TxEqCoeff2,                   ///<Compensation code for static legs when in equalization mode (both PUP and PDN); even bits are segment 0 and odd bits are segment 1
  RxVrefVttDecap,               ///< Control Rx Vref coupling to VTT.  0: No cap to VTT  1-7 is cap control with 12.5% step size. Remaining cap will be to VSS
  RxVrefVddqDecap,              ///< Control Rx Vref coupling to Vddq.  0: No cap to Vddq  1-7 is cap control with 12.5% step size. Remaining cap will be to VSS
  ForceDfeDisable,              ///< Force DFE Disable - Forces the DFE disabled - overrides the rcven based DFE enable. 0 - DFE enabled. 1 - DFE forced disabled.
  PanicVttDnLp,                 ///< Multiplier to the Vtt Panic Comp Dn Level LPDDR Read code to expand to 8 bits with resolution of 0.5 LSB. Range 0 to 7.5. Final output is 1x LSB
  VttGenStatusSelCount,         ///< Select which comparator is counted. 0:panichi0 1:panichi1 2:paniclo0 3:paniclo1 4:spare 5:(panichi0-paniclo0 6-7:rsvd
  VttGenStatusCount,            ///< 16 bit signed, saturating counter that counts the comparator output. Do not used when the comp FSM is active.
  RxR,                          ///< Matched : 3:2 CTRE Res Control
  RxC,                          ///< Matched : 1:0 CTLE Cap control
  RxTap1,                       ///< Unmatched : Tap 1 Coeffecient control(unsigned voltage with step size ~5mv),
  RxTap2,                       ///< Unmatched : Tap 2 Coeffecient Control(signed with step size ~5mv)
  RxTap3,                       ///< Unmatched : Tap 3 Coeffecient Control(signed with step size ~5mv)
  RxTap4,                       ///< Unmatched : Tap 4 Coeffecient Control(signed with step size ~5mv)
  RxTap1En,                       ///< Unmatched : Tap 1 Coeffecient control(unsigned voltage with step size ~5mv),
  RxTap2En,                       ///< Unmatched : Tap 2 Coeffecient Control(signed with step size ~5mv)
  RxTap3En,                       ///< Unmatched : Tap 3 Coeffecient Control(signed with step size ~5mv)
  RloadDqsDn,                   ///< Sets the strength of the Rload pulldown for the RxDqs P-Path umatched receiver and RxBias Current generation
  DataRxD0PiCb,                 ///< PICB control for secondary rxdqd0 block.  Can match SDLLPICb or use 0 for best bandwidth.
  DataSDllPiCb,                 ///< PICB control for secondary DLL. Higher value means more cap/slower slopes into PI.  Should match value in CBTune1.
  VccDllRxD0PiCb,               ///< PICB control for secondary rxdqd0 block.  Can match SDLLPICb or use 0 for best bandwidth
  VccDllSDllPiCb,               ///< PICB control for secondary DLL.  Higher value means more cap/slower slopes into PI.  Should match value in CBTune1.
  DqsOdtCompOffset,             ///< Offset for DQS OdtUp & OdtDn, decoupled from DQ ODT values for better power control in the system
  DqOdtCompOffset,              ///< 2s Complement offset added to both DQ OdtUp and OdtDn Value. Positive # increases termination and each step is ~5%
  DqSCompOffset,                ///< 2s Complement offset added to DQ Slew Rate Comp Value. Positive # increases slew rate and each step is ~5%
  DqDrvUpCompOffset,            ///< 2s Complement offset added to DQ Drv-Up Comp Value. Positive # increases drive strength and each step is ~5%
  DqDrvDnCompOffset,            ///< 2s Complement offset added to DQ Drv-Down Comp Value. Positive # increases drive strength and each step is ~5%
  RloadCompOffset,              ///< 2s Complement offset added to Rload Comp Code. Positive # increases slew rate and each step is ~5%
  CmdRCompDrvDownOffset,        ///< 2s Complement Cmd offset to pull-down drive strength. ?CH_SUB
  CmdRCompDrvUpOffset,          ///< 2s Complement Cmd offset to pull-up drive strength. ?CH_SUB
  CmdSCompOffset,               ///< 2s Complement Cmd offset to slew rate. Positive # decreases slew rate and each step is ~5% ?CH_SUB
  ClkRCompDrvDownOffset,        ///< 2s Complement Clk offset to pull-down drive strength. ?CH_SUB
  ClkRCompDrvUpOffset,          ///< 2s Complement Clk offset to pull-up drive strength. ?CH_SUB
  ClkSCompOffset,               ///< 2s Complement Clk offset to slew rate. Positive # decreases slew rate and each step is ~5% ?CH_SUB
  CtlRCompDrvDownOffset,        ///< 2s Complement Ctl offset to pull-down drive strength. ?CH_SUB
  CtlRCompDrvUpOffset,          ///< 2s Complement Ctl offset to pull-up drive strength. ?CH_SUB
  CtlSCompOffset,               ///< 2s Complement Ctl offset to slew rate. Positive # decreases slew rate and each step is ~5% ?CH_SUB
  CkeRCompDrvDownOffset,        ///< 2s Complement Cke offset to pull-down drive strength. ?CH_SUB
  CkeRCompDrvUpOffset,          ///< 2s Complement Cke offset to pull-up drive strength. ?CH_SUB
  CkeSCompOffset,               ///< 2s Complement Cke offset to slew rate. Positive # decreases slew rate and each step is ~5% ?CH_SUB
  CompOffsetVssHiFF,            ///< 2s Complement VssHi Feedforward Comp Offset
  CompOffsetAll,                ///< Temporary holder till comps are enumerated.
  VsxHiClkFFOffset,             ///< 2s Complement VsxHi Clk FeedForward Offset
  VsxHiCaFFOffset,              ///< 2s Complement VsxHi Ca FeedForward Offset
  VsxHiCtlFFOffset,             ///< 2s Complement VsxHi Ctl FeedForward Offset
  CmdSlewRate,                  ///< Monotonic increment, where the positive increment adds more delay cells in the Command delay line.
  ClkSlewRate,                  ///< Monotonic increment, where the positive increment adds more delay cells in the Clock delay line.
  ClkRon,                       ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  CtlSlewRate,                  ///< Monotonic increment, where the positive increment adds more delay cells in the Control delay line.
  CtlRon,                       ///< Resistance setting within a set of possible resistances, which may be a different set of values per product. Indexed by integer values.
  DqScompPC,                     ///< Phase or Cycle Lock Type for TxSlewRate
  CmdScompPC,                   ///< Phase or Cycle Lock Type for CmdSlewRate
  CtlScompPC,                   ///< Phase or Cycle Lock Type for CtlSlewRate
  ClkScompPC,                   ///< Phase or Cycle Lock Type for ClkSlewRate
  SCompCodeDq,                  ///< Slew Comp Code for DQ
  SCompCodeCmd,                 ///< Slew Comp Code for Cmd
  SCompCodeCtl,                 ///< Slew Comp Code for Ctl
  SCompCodeClk,                 ///< Slew Comp Code for Clk
  SCompBypassDq,                ///< Bypass DQ Slew delay
  SCompBypassCmd,               ///< Bypass CA Slew delay
  SCompBypassCtl,               ///< Bypass Ctl Slew delay
  SCompBypassClk,               ///< Bypass Clk Slew delay
  WrDSCodeUpCmd,                ///< RcompUp Code for Write Drive Strength-Cmd
  WrDSCodeUpCtl,                ///< RcompUp Code for Write Drive Strength-Ctl
  WrDSCodeUpClk,                ///< RcompUp Code for Write Drive Strength-Clk
  WrDSCodeDnCmd,                ///< RcompDown Code for Write Drive Strength-Cmd
  WrDSCodeDnCtl,                ///< RcompDown Code for Write Drive Strength-Ctl
  WrDSCodeDnClk,                ///< RcompDown Code for Write Drive Strength-Clk
  TcoCompCodeCmd,               ///< TCO Comp Code for Cmd
  TcoCompCodeCtl,               ///< TCO Comp Code for Ctl
  TcoCompCodeClk,               ///< TCO Comp Code for Clk
  DqOdtVrefUp,
  DqOdtVrefDn,
  DqDrvVrefUp,
  DqDrvVrefDn,
  CmdDrvVrefUp,
  CmdDrvVrefDn,
  CtlDrvVrefUp,
  CtlDrvVrefDn,
  ClkDrvVrefUp,
  ClkDrvVrefDn,
  CompRcompOdtUp,               ///< The resistive value of the pull-up block of on die termination
  CompRcompOdtDn,               ///< The resistive value of the pull-dn block of on die termination
  TxDqsBitDelay,                ///< Linear delay (PI ticks), where the positive increment moves the TX DQS bitlane later in time relative to all other bus signals.
  RxVocUnmatched,
  RxPerBitDeskewCal,            ///< Step size of RxPerBit (Byte granularity)
  TxPerBitDeskewCal,            ///< Step size of TxPerBit (Byte granularity)
  CccPerBitDeskewCal,           ///< Step size of CCCPerBit (Byte granularity)
  TxDqDccOffset,
  TxDqsDccOffset,
  RxUnmatchedOffNcal,
  RxMatchedUnmatchedOffPcal,
                                ///< -----------------------------------------------------------------
  EndOfPhyMarker,               ///< Marker for end of phy groups
                                ///< -----------------------------------------------------------------
  GsmIocIoReset,                ///< Used to reset the DDR IO.
  GsmIocWlWakeCyc,              ///< Weak Lock Wake.  Specified in QCLK cycles.  Cycle mapping is project dependent.
  GsmIocWlSleepCyclesAct,       ///< Weak Lock Sleep. Specified in QCLK cycles.  Cycle mapping is project dependent.
  GsmIocWlSleepCyclesLp,        ///< Weak Lock Sleep. Specified in QCLK cycles.  Cycle mapping is project dependent.
  GsmIocForceCmpUpdt,
  GsmIocNoDqInterleave,
  GsmIocScramLpMode,            ///< Set the DDRIO DDRSCRAM to LPDDR4 Mode
  GsmIocScramDdr4Mode,          ///< Set the DDRIO DDRSCRAM to DDR4 Mode
  GsmIocScramDdr5Mode,          ///< Set the DDRIO DDRSCRAM to DDR5 Mode
  GsmIocScramGear1,             ///< Program gear in DDRSCRAM: 1 = Gear1, 0 = Gear2/Gear4
  GsmIocScramGear4,             ///< Program gear in DDRSCRAM: 1 = Gear4, 0 = Gear2/Gear1
  GsmIocVccDllGear1,            ///< Program gear in VccDllDqsDelay.Gear1
  GsmIocVccDllControlBypass_V,           ///< Disable VccDll[0/1] regulator and short the output to VccIoG
  GsmIocVccDllControlSelCode_V,
  GsmIocVccDllControlTarget_V,
  GsmIocVccDllControlOpenLoop_V,
  GsmIocVsxHiControlSelCode_V,
  GsmIocVsxHiControlOpenLoop_V,
  GsmIocCccPiEn,
  GsmIocCccPiEnOverride,
  GsmIocDisClkGate,
  GsmIocDisDataIdlClkGate,      ///< Data Idle Clock Gating Disable switch.
  GsmIocScramLp4Mode,           ///< Set the DDRIO DDRSCRAM to LPDDR4 Mode
  GsmIocScramLp5Mode,           ///< Set the DDRIO DDRSCRAM to LPDDR5 Mode
  GsmIocScramOvrdPeriodicToDvfsComp, ///<Converts PM8 Comp requests to PM9 DFVS Comp
  GsmIocLp5Wck2CkRatio,
  GsmIocChNotPop,               ///< Channel Not Populated Mask.
  GsmIocDisIosfSbClkGate,
  GsmIocEccEn,                  ///< ECC Enable/Disable for the DDRIO ?
  GsmIocWrite0En,
  GsmIocScramWrite0En,
  GsmIocDataOdtStaticEn,
  GsmIocCompOdtStaticDis,
  GsmIocStrobeOdtStaticEn,     ///< 2 bits to control ddrxover cell for mindelay margin improvment [0]: q2q0_sel - controls margin from Qclk to Q0clk[1]: q02pi_sel - controls margin from Q0clk to PIclk
  GsmIocDllMask,
  GsmIocDataOdtMode,
  GsmIocDataDqOdtParkMode,
  GsmIocCompVttOdtEn,
  GsmIocCmdDrvVref200ohm,
  GsmIocVttPanicCompUpMult,
  GsmIocVttPanicCompDnMult,
  GsmIocRxVrefMFC,              ///< RX Vref Metal Finger Cap. ?Vtt vs Vddq
  GsmIocVccDllRxDeskewCal,      ///< Step size of overall Rx instances
  GsmIocVccDllTxDeskewCal,      ///< Step size of overall Tx instances
  GsmIocVccDllCccDeskewCal,     ///< Step size of overall CCC instances
  GsmIocForceRxAmpOn,
  GsmIocTxOn,
  GsmIocRxDisable,
  GsmIocSenseAmpMode,
  GsmIocDqsRFMode,
  GsmIocClkPFallNRiseMode,        ///< Clk P Fall/N Rise
  GsmIocClkPRiseNFallMode,        ///< Clk P Rise/N Fall
  GsmIocClkPFallNRiseFeedback,    ///< Clk P Fall/N Rise Feedback
  GsmIocClkPRiseNFallFeedback,    ///< Clk P Rise/N Fall Feedback
  GsmIocClkPFallNRiseCcc56,       ///< Clk P Fall/N Rise Setting for CCC56 (Perbit for bits 5 and 6)
  GsmIocClkPRiseNFallCcc56,       ///< Clk P Rise/N Fall Setting for CCC56 (Perbit for bits 5 and 6)
  GsmIocClkPFallNRiseCcc78,       ///< Clk P Fall/N Rise Setting for CCC78 (Perbit for bits 7 and 8)
  GsmIocClkPRiseNFallCcc78,       ///< Clk P Rise/N Fall Setting for CCC78 (Perbit for bits 7 and 8)
  GsmIocDdrDqRxSdlBypassEn,       ///< Used to bypass the rxsdl output with value in rxsdlbypasval
  GsmIocCaTrainingMode,
  GsmIocCaParityTrain,            ///< During CA Training, enable parity based error detection for LPDDR4/5 and ddr5.
  GsmIocReadLevelMode,
  GsmIocWriteLevelMode,
  GsmIocReadDqDqsMode,
  GsmIocRxClkStg,               ///< Number of clock cycles to keep clock on in the RX datapath from the fall of RcvEn signal.
  GsmIocDataRxBurstLen,         ///< Configures the burst length of Read at the data partition.
  GsmIocEnDqsNRcvEn,
  GsmIocEnDqsNRcvEnGate,
  GsmIocRxMatchedPathEn,
  GsmIocForceOdtOn,
  GsmIocForceOdtOnWithoutDrvEn,
  GsmIocRxPiPwrDnDis,
  GsmIocTxPiPwrDnDis,
  GsmIocTxDisable,              ///< Ignore Write commands for this data byte
  GsmIocCmdVrefConverge,
  GsmIocCompClkOn,
  GsmIocConstZTxEqEn,
  GsmIocDisableQuickComp,
  GsmIocSinStep,
  GsmIocSinStepAdv,
  GsmIocSdllSegmentDisable,
  GsmIocRXDeskewForceOn,                  ///< Disable all power down modes in Rx Per Bit Deskew
  GsmIocDllWeakLock,
  GsmIocDllWeakLock1,
  GsmIocTxEqEn,
  GsmIocTxEqTapSelect,              ///< Tx Equalization Tap Select - specifies the number of post-cursor taps to use for Tx Equalization
  GsmIocDqSlewDlyByPass,
  GsmIocWlLongDelEn,
  GsmIocCompVddqOdtEn,              ///< Byte ODT mode: 0 Vss, 1:Vddq, 2:Vtt
  GsmIocRptChRepClkOn,              ///< Forces the RPTCH clock on.
  GsmIocCmdAnlgEnGraceCnt,          ///< Grace counter for extending CmdAnalogEn counter.
  GsmIocTxAnlgEnGraceCnt,           ///< Grace conter for extending the TxAnalogEn counter.
  GsmIocTxDqFifoRdEnPerRankDelDis,  ///< Disables the Per Rank delay on TxDq Fifo.
  GsmIocInternalClocksOn,           ///< Force on all the PI enables
  GsmIocDqsMaskPulseCnt,            ///< Controls the number of masked DQS pulses sent by the DDR IO during CMD VREF update in LP4 CBT
  GsmIocDqsMaskValue,               ///< Preamble Mask value is '1 (DDR4 long preamble) or '0 (all other usage)
  GsmIocDqsPulseCnt,                ///< Controls the number of unmasked DQS pulses sent by the DDR IO during CMD VREF update in LP4 CBT
  GsmIocDqOverrideData,             ///< DQ value override to latch CMD VREF to the DRAM during LP4 CBT. This field is used to drive DQ7 High during LP5 ECT.
  GsmIocDqOverrideEn,               ///< Enables the DDR IO to force static DQ value to latch CMD VREF to the DRAM during LP4 CBT. This field is used to drive DQ7 High during LP5 ECT.
  GsmIocRankOverrideEn,             ///< Configures the DDRIO to override the delay values used for the target rank.
  GsmIocRankOverrideVal,            ///< Specified which rank to pull the delay values from.
  GsmIocDataCtlGear1,               ///< Enable Gear1 in DDRDATA: 1 = Gear1, 0 = Gear2/Gear4
  GsmIocDataCtlGear4,               ///< Enable Gear4 in DDRDATA: 1 = Gear4, 0 = Gear2/Gear1
  GsmIocDataWrPreamble,             ///< The number of Write Preambles to send to the DRAM.  Should match MR encoding.
  GsmIocDccTrainingMode,
  GsmIocDccTrainingDone,
  GsmIocDccDrain,
  GsmIocDccActiveClks,
  GsmIocDccActiveBytes,
  GsmIocDccDcoCompEn,
  GsmIocDccClkTrainVal,
  GsmIocDccDataTrainDqsVal,
  GsmIocLdoFFCodeLockData,          ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocLdoFFCodeLockCcc,           ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocDccDcdSampleSelCcc,         ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocStep1PatternCcc,            ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocDccCodePh0IndexCcc,         ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocDccCodePh90IndexCcc,        ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocTxpbddOffsetCcc,            ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocTxpbdPh90incrCcc,           ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocMdllCmnVcdlDccCodeCcc,      ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocDccDcdSampleSelData,        ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocStep1PatternData,           ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocDccCodePh0IndexData,        ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocDccCodePh90IndexData,       ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocTxpbddOffsetData,           ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocTxpbdPh90incrData,          ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocMdllCmnVcdlDccCodeData,     ///< Note: CR contains RW/V field, use WriteNoCache
  GsmIocDccFsmResetData,
  GsmIocDccFsmResetCcc,
  GsmIocDcdRoCalEnData,
  GsmIocDcdRoCalEnCcc,
  GsmIocDcdRoCalStartData,
  GsmIocDcdRoCalStartCcc,
  GsmIocDcdRoCalSampleCountRstData,
  GsmIocDcdRoCalSampleCountRstCcc,
  GsmIocDccStartData,
  GsmIocDccStartCcc,
  GsmIocSrzDcdEnData,
  GsmIocSrzDcdEnCcc,
  GsmIocMdllDcdEnData,
  GsmIocMdllDcdEnCcc,
  GsmIocDcdSampleCountStartData,
  GsmIocDcdSampleCountRstData,
  GsmIocDcdRoLfsrData,

  GsmIocRxSALTailCtrl,
  GsmIocRxVocMode,                  ///< Controls the Sense Amp mode
  GsmIocDataTrainFeedback,          ///< DataTrainFeedback
  GsmIocDdrDqDrvEnOvrdData,
  GsmIocDdrDqDrvEnOvrdModeEn,
  GsmIocDataDqsOdtParkMode,
  GsmIocDataDqsNParkLow,
  GsmIocRxAmpOffsetEn,
  GsmIocBiasPMCtrl,
  GsmIocLocalGateD0tx,
  GsmIocFFCodePiOffset,
  GsmIocFFCodeIdleOffset,
  GsmIocFFCodeWeakOffset,
  GsmIocFFCodeWriteOffset,
  GsmIocFFCodeReadOffset,
  GsmIocFFCodePBDOffset,
  GsmIocFFCodeCCCDistOffset,
  GsmIocDataInvertNibble,
  GsmIocCapCancelCodeIdle,
  GsmIocCapCancelCodePBD,
  GsmIocCapCancelCodeWrite,
  GsmIocCapCancelCodeRead,
  GsmIocCapCancelCodePi,
  GsmIocVssHiFFCodeIdle,
  GsmIocVssHiFFCodeWrite,
  GsmIocVssHiFFCodeRead,
  GsmIocVssHiFFCodePBD,
  GsmIocVssHiFFCodePi,
  GsmIocEnableSpineGate,
  GsmIocDataDccLaneStatusResult,
  GsmIocDataDccSaveFullDcc,
  GsmIocDataDccSkipCRWrite,
  GsmIocDataDccMeasPoint,
  GsmIocDataDccRankEn,
  GsmIocDataDccLaneEn,
  GsmIocDataDccStepSize,
  GsmIocDataDcc2xStep,
  GsmIocCccDccCcc,
  GsmIocCccDccLaneStatusResult,
  GsmIocCccDccStepSize,
  GsmIocCccDcc2xStep,
  GsmIocForceOnRcvEn,
  GsmIocRetrainSwizzleCtlByteSel,
  GsmIocRetrainSwizzleCtlBit,
  GsmIocRetrainSwizzleCtlRetrainEn,
  GsmIocRetrainSwizzleCtlSerialMrr,
  GsmIocRetrainInitPiCode,
  GsmIocRetrainDeltaPiCode,
  GsmIocRetrainRoCount,
  GsmIocRetrainCtlInitTrain,
  GsmIocRetrainCtlDuration,
  GsmIocRetrainCtlResetStatus,
                                    ///< -----------------------------------------------------------------
  EndOfIocMarker,                   ///< End of IO Config Marker
                                    ///< -----------------------------------------------------------------
  GsmMctRCD,
  GsmMctRP,
  GsmMctRPabExt,
  GsmMctRAS,
  GsmMctRDPRE,
  GsmMctPPD,
  GsmMctWRPRE,
  GsmMctFAW,
  GsmMctRRDsg,
  GsmMctRRDdg,
  GsmMctREFSbRd,
  GsmMctLpDeratingExt,
  GsmMctRDRDsg,
  GsmMctRDRDdg,
  GsmMctRDRDdr,
  GsmMctRDRDdd,
  GsmMctRDWRsg,
  GsmMctRDWRdg,
  GsmMctRDWRdr,
  GsmMctRDWRdd,
  GsmMctWRRDsg,
  GsmMctWRRDdg,
  GsmMctWRRDdr,
  GsmMctWRRDdd,
  GsmMctWRWRsg,
  GsmMctWRWRdg,
  GsmMctWRWRdr,
  GsmMctWRWRdd,
  GsmMctCKE,
  GsmMctXP,
  GsmMctXPDLL,
  GsmMctPRPDEN,
  GsmMctRDPDEN,
  GsmMctWRPDEN,
  GsmMctCA2CS,
  GsmMctCSL,
  GsmMctCSH,
  GsmMctOdtRdDuration,
  GsmMctOdtRdDelay,
  GsmMctOdtWrDuration,
  GsmMctOdtWrDelay,
  GsmMctWrEarlyOdt,
  GsmMctCL,
  GsmMctCWL,
  GsmMctAONPD,
  GsmMctCWLAdd,
  GsmMctCWLDec,
  GsmMctXSDLL,
  GsmMctXSR,                    ///< Self Refresh exit to commands not requiring a locked DLL (tXS) in DDR3/4(For SKL, this maps to the tXS_offset) or Self Refresh exit to next valid command delay (tXSR) in LPDDR3/4.
  GsmMctSR,
  GsmMctZQOPER,
  GsmMctMOD,                    ///< Mode Register Set command to next valid command delay.
  GsmMctSrIdle,                 ///< The number of cycles with no transactions in order to enter Self Refresh.
  GsmMctREFI,                   ///< Delay from Start of Refresh to next ACT or Refresh.
  GsmMctRFC,                    ///< Time of refresh.
  GsmMctOrefRi,                 ///< This defines how long the Rank must be idle to do a Opportunistic Refresh.
  GsmMctRefreshHpWm,            ///< tREFI count level that turns refresh priority to high.
  GsmMctRefreshPanicWm,         ///< tREFI count level for panic priority.
  GsmMctHpRefOnMrs,             ///< Enables HP Refresh on MRS flow. intended for SAGV as MC can enter SR while owing refreshes
  GsmMctREFIx9,                 ///< Max time between refreshes per rank.
  GsmMctSrxRefDebits,           ///< Number of Refresh debits to be given on Self refresh exit
  GsmMctZQCSPeriod,
  GsmMctZQCS,
  GsmMctZQCAL,
  GsmMctCPDED,                  ///< Defines the delay required from power down before the command bus can be tri-stated.
  GsmMctCAL,                    ///<
  GsmMctCKCKEH,                 ///< Defines the number of cycles CS must be driven low before asserting CKE for power down exit.
  GsmMctCSCKEH,                 ///< Defines the number of valid clock cycles before asserting CKE for power down exit.
  GsmMctRFCpb,                  ///< Refresh cycle time per bank in DCLKS. How long the per bank refresh command takes to complete.
  GsmMctRefm,                   ///< tRFM same bank value for DDR5
                                ///< -----------------------------------------------------------------
  EndOfMctMarker,               ///< Marks the end of the MC Timing Group.
                                ///< -----------------------------------------------------------------
  GsmMccDramType,               ///< Defines what DRAM technology is being used.
  GsmMccCmdStretch,             ///< Defines the Mode of Command Stretch.
  GsmMccCmdGapRatio,            ///< When using N:1 Command Stretch Mode, this field defines how many back-to-back commands are allowed before a gap is required
  GsmMccAddrMirror,             ///< This field is used to inform the MC which DIMMs are mirrored.
  GsmMccCmdTriStateDis,         ///< Disable Command bus tri-state
  GsmMccCmdTriStateDisTrain,    ///< Disable Command bus tri-state during CADB training
  GsmMccFreqPoint,              ///< Defines the current frequency point the MC is in during training.
  GsmMccEnableOdtMatrix,        ///< Enables custom mapping of ODT assertion on Ranks.
  GsmMccX8Device,
  GsmMccGear2,                  ///< Program gear in MC: 0 = Gear1, 1 = Gear2
  GsmMccGear4,                  ///< Program gear4 in MC
  GsmMccDdr4OneDpc,             ///< ddr4_1dpc performance feature
  GsmMccMultiCycCmd,            ///< Setting this field to 1 allows MC to stretch DDR5 MPC and VREF commands. CS Setup and Hold times are enforced along with CS being asserted(active low) for multiple cycles.
  GsmMccWrite0En,               ///< Enable write0 feature in MC
  GsmMccWCKDiffLowInIdle,
  GsmMccLDimmMap,
  GsmMccEnhancedInterleave,
  GsmMccEccMode,
  GsmMccAddrDecodeDdrType,
  GsmMccLChannelMap,
  GsmMccSChannelSize,
  GsmMccChWidth,
  GsmMccHalfCachelineMode,
  GsmMccLDimmSize,              ///< Defines the L DIMM size in mutiples of X which may vary from project to project.
  GsmMccLDimmDramWidth,         ///< This field defines the L DIMM device width: x4, x8, x16, x32, x64
  GsmMccLDimmRankCnt,
  GsmMccSDimmSize,              ///< Defines the S DIMM size in mutiples of X which may vary from project to project.
  GsmMccSDimmDramWidth,         ///< This field defines the S DIMM device width: x4, x8, x16, x32, x64
  GsmMccSDimmRankCnt,
  GsmMccExtendedBankHashing,    ///< Extended Bank Hashing in Address Decoder
  GsmMccDdr5Device8GbDimmL,     ///< DIMM_L: 0 - DDR5 capacity is more than 8Gb; 1 - DDR5 capacity is 8Gb
  GsmMccDdr5Device8GbDimmS,     ///< DIMM_S: 0 - DDR5 capacity is more than 8Gb; 1 - DDR5 capacity is 8Gb
  GsmMccDdrReset,
  GsmMccEnableRefresh,
  GsmMccEnableSr,               ///< This field is used to enable or disable the Self Refresh feature.
  GsmMccMcInitDoneAck,
  GsmMccMrcDone,
  GsmMccSafeSr,
  GsmMccSaveFreqPoint,          ///< This field communicates with the MC to save the specified frequency point.  Used with FreqIndex in GetSet.
  GsmMccEnableDclk,
  GsmMccPureSrx,                ///< Tell MC to only exit Self Refresh.  Blocks other maintenance FSMs: ZQ, Temp, DQS Oscillator.
  GsmMccLp4FifoRdWr,
  GsmMccIgnoreCke,
  GsmMccMaskCs,
  GsmMccCpgcInOrder,
  GsmMccInOrderIngress,        ///< Setting this bit will result in MC extracing requests from the ingress in an "in order" manner
  GsmMccCadbEnable,
  GsmMccDeselectEnable,
  GsmMccBusRetainOnBubble,      ///< Set this bit to make sure that when CADB can send deselect cycles and N:1 is enabled that CA bus (and CA valid) will be frozen on a cycle where a bubble is enforced.
  GsmMccBlockXarb,
  GsmMccBlockCke,
  GsmMccResetOnCmd,
  GsmMccResetDelay,
  GsmMccRankOccupancy,
  GsmMccMcSrx,
  GsmMccRefInterval,            ///< Refresh Interval period in DCLKS.
  GsmMccRefStaggerEn,           ///< Enables refresh staggering.
  GsmMccRefStaggerMode,         ///< 0 = per DIMM, 1 = per channel.
  GsmMccDisableStolenRefresh,   ///< Disable stolen refresh.
  GsmMccEnRefTypeDisplay,       ///< Displays refresh type on BA address pins.
  GsmMccHashMask,               ///< Bits set for channel XOR function.
  GsmMccLsbMaskBit,             ///< MC channel interleave bit.
  GsmMccHashMode,               ///< Use A6 for channel select or other fields in this register.
  GsmMccDisableCkTristate,      ///< Boolean control to enable/disable Clock Tristating during CKE low.
  GsmMccPbrDis,                 ///< Boolean control for Per Bank Refresh.  1 = Disable, 0 = Enable.
  GsmMccPbrIssueNop,            ///< Boolean if set PBR FSM will issue a NOP command. if cleared PBR FSM will just wait 1 extra clock after PRE command
  GsmMccRefreshAbrRelease,      ///< ABR is activated until tREFI count is below this parameter, when tREFI is above HP water mark
  GsmMccPbrDisOnHot,
  GsmMccOdtOverride,            ///< Override enable
  GsmMccOdtOn,                  ///< Override value
  GsmMccMprTrainDdrOn,
  GsmMccCkeOverride,            ///< Override enable
  GsmMccCkeOn,                  ///< Override value
  GsmMccCsOverride,             ///< Override enable
  GsmMccCsOverrideVal,          ///< Override value
  GsmMccRrdValidTrigger,        ///< Trigger RRDVALID FSM (Roundtrip Matching) per rank
  GsmMccRrdValidOverflow,       ///< RRDVALID FSM Overflow indication per rank
  GsmMccRrdValidValue,          ///< RRDVALID FSM result - gap value, per rank
  GsmMccRrdValidSign,           ///< RRDVALID FSM result - gap sign, per rank
  GsmMccDeswizzleByte,          ///< Data Strobe deswizzle mapping
  GsmMccDeswizzleBit,           ///< Data bits deswizzle mapping
  GsmMccLp5BankMode,            ///< LP5 Bank Mode program
  GsmMccMrrBlnMax,              ///< MRR_BLnMax
                                ///< -----------------------------------------------------------------
  EndOfMccMarker,               ///< End of MC Configuration Group.
                                ///< -----------------------------------------------------------------
  GsmCmiMcOrg,
  GsmCmiHashMask,
  GsmCmiLsbMaskBit,
  GsmCmiStackedMode,
  GsmCmiStackMsMap,
  GsmCmiLMadSliceMap,
  GsmCmiSMadSliceSize,
  GsmCmiStackedMsHash,
  GsmCmiSliceDisable,
  EndOfCmiMarker,               ///< End of CMI Group.
  GsmComplexRcvEn,
  GsmComplexRxBias,             ///< Combines the control of RxBiasRComp and RxBiasIComp into one linear field.
  GsmGtMax,                     ///< GSM_GT enumeration maximum value.
  GsmGtDelim = MRC_INT32_MAX    ///< This value ensures the enum size is consistent on both sides of the PPI.
} GSM_GT;

typedef enum {
  SigRasN   = 0,
  SigCasN   = 1,
  SigWeN    = 2,
  SigBa0    = 3,
  SigBa1    = 4,
  SigBa2    = 5,
  SigA0     = 6,
  SigA1     = 7,
  SigA2     = 8,
  SigA3     = 9,
  SigA4     = 10,
  SigA5     = 11,
  SigA6     = 12,
  SigA7     = 13,
  SigA8     = 14,
  SigA9     = 15,
  SigA10    = 16,
  SigA11    = 17,
  SigA12    = 18,
  SigA13    = 19,
  SigA14    = 20,
  SigA15    = 21,
  SigA16    = 22,
  SigA17    = 23,
  SigCs0N   = 24,
  SigCs1N   = 25,
  SigCs2N   = 26,
  SigCs3N   = 27,
  SigCs4N   = 28,
  SigCs5N   = 29,
  SigCs6N   = 30,
  SigCs7N   = 31,
  SigCs8N   = 32,
  SigCs9N   = 33,
  SigCke0   = 34,
  SigCke1   = 35,
  SigCke2   = 36,
  SigCke3   = 37,
  SigCke4   = 38,
  SigCke5   = 39,
  SigOdt0   = 40,     //could also be used for CA-ODT for LP4
  SigOdt1   = 41,     //could also be used for CA-ODT for LP4
  SigOdt2   = 42,
  SigOdt3   = 43,
  SigOdt4   = 44,
  SigOdt5   = 45,
  SigPar    = 46,
  SigAlertN = 47,
  SigBg0    = 48,
  SigBg1    = 49,
  SigActN   = 50,
  SigCid0   = 51,
  SigCid1   = 52,
  SigCid2   = 53,
  SigCk0    = 54,
  SigCk1    = 55,
  SigCk2    = 56,
  SigCk3    = 57,
  SigCk4    = 58,
  SigCk5    = 59,
  SigGnt0   = 60,
  SigGnt1   = 61,
  SigErid00 = 62,
  SigErid01 = 63,
  SigErid10 = 64,
  SigErid11 = 65,
  SigErr0   = 66,
  SigErr1   = 67,
  SigCa00   = 68,    // First instantiation of the CA bus for a given channel
  SigCa01   = 69,
  SigCa02   = 70,
  SigCa03   = 71,
  SigCa04   = 72,
  SigCa05   = 73,
  SigCa10   = 74,    // Second instantiation of the CA bus for a given channel
  SigCa11   = 75,
  SigCa12   = 76,
  SigCa13   = 77,
  SigCa14   = 78,
  SigCa15   = 79,
  SigCa06,      ///< LPDDR3 has 10 CA pins per bus.
  SigCa07,
  SigCa08,
  SigCa09,
  SigCa16,
  SigCa17,
  SigCa18,
  SigCa19,
  GsmCsnMax,
  GsmCsnDelim = MRC_INT32_MAX
} GSM_CSN;
#endif // _MrcCommonTypes_h_

