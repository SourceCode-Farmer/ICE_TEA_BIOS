#ifndef __MrcMcRegisterStructAdlDxxx_h__
#define __MrcMcRegisterStructAdlDxxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)

typedef MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_STRUCT MC0_CH0_CR_CADB_ACCESS_CONTROL_POLICY_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_READ_POLICY_STRUCT MC0_CH0_CR_CADB_ACCESS_READ_POLICY_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_WRITE_POLICY_STRUCT MC0_CH0_CR_CADB_ACCESS_WRITE_POLICY_STRUCT;
typedef union {
  struct {
    UINT32 START_TEST                              :  1;  // Bits 0:0
    UINT32 STOP_TEST                               :  1;  // Bits 1:1
    UINT32                                         :  30;  // Bits 31:2
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_CH0_CR_CADB_CTL_STRUCT;
typedef union {
  struct {
    UINT32 CMD_DESELECT_START                      :  4;  // Bits 3:0
    UINT32 CMD_DESELECT_STOP                       :  4;  // Bits 7:4
    UINT32 LANE_DESELECT_EN                        :  3;  // Bits 10:8
    UINT32 INITIAL_DSEL_EN                         :  1;  // Bits 11:11
    UINT32 INITIAL_DSEL_SSEQ_EN                    :  1;  // Bits 12:12
    UINT32 CADB_DSEL_THROTTLE_MODE                 :  3;  // Bits 15:13
    UINT32 CADB_SEL_THROTTLE_MODE                  :  3;  // Bits 18:16
    UINT32                                         :  5;  // Bits 23:19
    UINT32 CADB_TO_CPGC_BIND                       :  1;  // Bits 24:24
    UINT32 GLOBAL_START_BIND                       :  1;  // Bits 25:25
    UINT32 GLOBAL_STOP_BIND                        :  1;  // Bits 26:26
    UINT32                                         :  2;  // Bits 28:27
    UINT32 CADB_MODE                               :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_CH0_CR_CADB_CFG_STRUCT;
typedef union {
  struct {
    UINT32 START_DELAY                             :  10;  // Bits 9:0
    UINT32                                         :  6;  // Bits 15:10
    UINT32 STOP_DELAY                              :  10;  // Bits 25:16
    UINT32                                         :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_CH0_CR_CADB_DLY_STRUCT;
typedef union {
  struct {
    UINT32                                         :  21;  // Bits 20:0
    UINT32 MRS_CURR_PTR                            :  3;  // Bits 23:21
    UINT32 TEST_ABORTED                            :  1;  // Bits 24:24
    UINT32                                         :  3;  // Bits 27:25
    UINT32 ALGO_DONE                               :  1;  // Bits 28:28
    UINT32 START_TEST_DELAY_BUSY                   :  1;  // Bits 29:29
    UINT32 TEST_BUSY                               :  1;  // Bits 30:30
    UINT32 TEST_DONE                               :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_CH0_CR_CADB_STATUS_STRUCT;
typedef union {
  struct {
    UINT32 CLOCKRATIO_OVERRIDE                     :  1;  // Bits 0:0
    UINT32 CLOCKRATIO                              :  2;  // Bits 2:1
    UINT32                                         :  1;  // Bits 3:3
    UINT32 MEMTYPE_OVERRIDE                        :  1;  // Bits 4:4
    UINT32 MEMTYPE                                 :  4;  // Bits 8:5
    UINT32                                         :  3;  // Bits 11:9
    UINT32 DSELWON_OVERRIDE                        :  1;  // Bits 12:12
    UINT32 DSELWON                                 :  1;  // Bits 13:13
    UINT32 CONTWON_OVERRIDE                        :  1;  // Bits 14:14
    UINT32 CONTWON                                 :  1;  // Bits 15:15
    UINT32                                         :  1;  // Bits 16:16
    UINT32 CLOCKRATIO_STATUS                       :  2;  // Bits 18:17
    UINT32                                         :  2;  // Bits 20:19
    UINT32 MEMTYPE_STATUS                          :  4;  // Bits 24:21
    UINT32                                         :  7;  // Bits 31:25
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_CH0_CR_CADB_OVERRIDE_STRUCT;
typedef union {
  struct {
    UINT32 STAGR1_POLY                             :  7;  // Bits 6:0
    UINT32                                         :  1;  // Bits 7:7
    UINT32 STAGR2_POLY                             :  7;  // Bits 14:8
    UINT32                                         :  1;  // Bits 15:15
    UINT32 STAGR3_POLY                             :  7;  // Bits 22:16
    UINT32                                         :  9;  // Bits 31:23
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_CFG_0_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_CFG_1_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_CFG_2_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_CFG_3_STRUCT;
typedef union {
  struct {
    UINT32 SAVE_LFSR_SEED_RATE                     :  8;  // Bits 7:0
    UINT32 RELOAD_LFSR_SEED_RATE                   :  5;  // Bits 12:8
    UINT32                                         :  19;  // Bits 31:13
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_STRUCT;
typedef union {
  struct {
    UINT32 CMD                                     :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_1_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_2_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_3_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_LMN_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_0_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_1_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_2_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_3_STRUCT;
typedef union {
  struct {
    UINT32 CS                                      :  4;  // Bits 3:0
    UINT32 CID                                     :  3;  // Bits 6:4
    UINT32 ODT                                     :  4;  // Bits 10:7
    UINT32 CKE                                     :  4;  // Bits 14:11
    UINT32 USE_HALF_CA                             :  1;  // Bits 15:15
    UINT32 VAL                                     :  1;  // Bits 16:16
    UINT32 PAR                                     :  1;  // Bits 17:17
    UINT32 CALow                                   :  14;  // Bits 31:18
    UINT32 CAHigh                                  :  18;  // Bits 49:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_CH0_CR_CADB_BUF_0_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_1_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_2_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_3_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_4_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_5_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_6_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_7_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_8_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_9_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_10_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_11_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_12_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_13_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_14_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH0_CR_CADB_BUF_15_STRUCT;
typedef union {
  struct {
    UINT32 MRS_GAP_SCALE                           :  1;  // Bits 0:0
    UINT32 MRS_GAP                                 :  4;  // Bits 4:1
    UINT32                                         :  3;  // Bits 7:5
    UINT32 MRS_AO_REPEATS                          :  16;  // Bits 23:8
    UINT32                                         :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_CH0_CR_CADB_AO_MRSCFG_STRUCT;
typedef union {
  struct {
    UINT32 WR_SELECT_ENABLE                        :  3;  // Bits 2:0
    UINT32                                         :  5;  // Bits 7:3
    UINT32 RD_SELECT_ENABLE                        :  3;  // Bits 10:8
    UINT32                                         :  5;  // Bits 15:11
    UINT32 ACT_SELECT_ENABLE                       :  3;  // Bits 18:16
    UINT32                                         :  5;  // Bits 23:19
    UINT32 PRE_SELECT_ENABLE                       :  3;  // Bits 26:24
    UINT32                                         :  1;  // Bits 27:27
    UINT32 INITIAL_SEL_SSEQ_EN                     :  1;  // Bits 28:28
    UINT32                                         :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_CH0_CR_CADB_SELCFG_STRUCT;
typedef union {
  struct {
    UINT32 CS_CLOCKS                               :  3;  // Bits 2:0
    UINT32 DSEL_CLOCKS                             :  4;  // Bits 6:3
    UINT32 SETUP_CLOCKS                            :  4;  // Bits 10:7
    UINT32 CS_ACTIVE_POLARITY                      :  1;  // Bits 11:11
    UINT32 MRS_START_PTR                           :  4;  // Bits 15:12
    UINT32 MRS_END_PTR                             :  4;  // Bits 19:16
    UINT32 MRS_GOTO_PTR                            :  4;  // Bits 23:20
    UINT32 MRS_CS_MODE                             :  2;  // Bits 25:24
    UINT32                                         :  6;  // Bits 31:26
    UINT32 MRS_DELAY_CNT                           :  8;  // Bits 39:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_CH0_CR_CADB_MRSCFG_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_STRUCT MC0_CH1_CR_CADB_ACCESS_CONTROL_POLICY_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_READ_POLICY_STRUCT MC0_CH1_CR_CADB_ACCESS_READ_POLICY_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_WRITE_POLICY_STRUCT MC0_CH1_CR_CADB_ACCESS_WRITE_POLICY_STRUCT;

typedef MC0_CH0_CR_CADB_CTL_STRUCT MC0_CH1_CR_CADB_CTL_STRUCT;

typedef MC0_CH0_CR_CADB_CFG_STRUCT MC0_CH1_CR_CADB_CFG_STRUCT;

typedef MC0_CH0_CR_CADB_DLY_STRUCT MC0_CH1_CR_CADB_DLY_STRUCT;

typedef MC0_CH0_CR_CADB_STATUS_STRUCT MC0_CH1_CR_CADB_STATUS_STRUCT;

typedef MC0_CH0_CR_CADB_OVERRIDE_STRUCT MC0_CH1_CR_CADB_OVERRIDE_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_CFG_0_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_CFG_1_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_CFG_2_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_CFG_3_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_PBUF_1_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_PBUF_2_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_PBUF_3_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_LMN_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_POLY_0_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_POLY_1_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_POLY_2_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH1_CR_CADB_DSEL_UNISEQ_POLY_3_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_0_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_1_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_2_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_3_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_4_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_5_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_6_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_7_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_8_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_9_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_10_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_11_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_12_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_13_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_14_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH1_CR_CADB_BUF_15_STRUCT;

typedef MC0_CH0_CR_CADB_AO_MRSCFG_STRUCT MC0_CH1_CR_CADB_AO_MRSCFG_STRUCT;

typedef MC0_CH0_CR_CADB_SELCFG_STRUCT MC0_CH1_CR_CADB_SELCFG_STRUCT;

typedef MC0_CH0_CR_CADB_MRSCFG_STRUCT MC0_CH1_CR_CADB_MRSCFG_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_STRUCT MC0_CH2_CR_CADB_ACCESS_CONTROL_POLICY_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_READ_POLICY_STRUCT MC0_CH2_CR_CADB_ACCESS_READ_POLICY_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_WRITE_POLICY_STRUCT MC0_CH2_CR_CADB_ACCESS_WRITE_POLICY_STRUCT;

typedef MC0_CH0_CR_CADB_CTL_STRUCT MC0_CH2_CR_CADB_CTL_STRUCT;

typedef MC0_CH0_CR_CADB_CFG_STRUCT MC0_CH2_CR_CADB_CFG_STRUCT;

typedef MC0_CH0_CR_CADB_DLY_STRUCT MC0_CH2_CR_CADB_DLY_STRUCT;

typedef MC0_CH0_CR_CADB_STATUS_STRUCT MC0_CH2_CR_CADB_STATUS_STRUCT;

typedef MC0_CH0_CR_CADB_OVERRIDE_STRUCT MC0_CH2_CR_CADB_OVERRIDE_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_CFG_0_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_CFG_1_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_CFG_2_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_CFG_3_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_PBUF_1_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_PBUF_2_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_PBUF_3_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_LMN_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_POLY_0_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_POLY_1_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_POLY_2_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH2_CR_CADB_DSEL_UNISEQ_POLY_3_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_0_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_1_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_2_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_3_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_4_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_5_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_6_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_7_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_8_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_9_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_10_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_11_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_12_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_13_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_14_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH2_CR_CADB_BUF_15_STRUCT;

typedef MC0_CH0_CR_CADB_AO_MRSCFG_STRUCT MC0_CH2_CR_CADB_AO_MRSCFG_STRUCT;

typedef MC0_CH0_CR_CADB_SELCFG_STRUCT MC0_CH2_CR_CADB_SELCFG_STRUCT;

typedef MC0_CH0_CR_CADB_MRSCFG_STRUCT MC0_CH2_CR_CADB_MRSCFG_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_STRUCT MC0_CH3_CR_CADB_ACCESS_CONTROL_POLICY_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_READ_POLICY_STRUCT MC0_CH3_CR_CADB_ACCESS_READ_POLICY_STRUCT;

typedef MC0_CR_CPGC2_ACCESS_WRITE_POLICY_STRUCT MC0_CH3_CR_CADB_ACCESS_WRITE_POLICY_STRUCT;

typedef MC0_CH0_CR_CADB_CTL_STRUCT MC0_CH3_CR_CADB_CTL_STRUCT;

typedef MC0_CH0_CR_CADB_CFG_STRUCT MC0_CH3_CR_CADB_CFG_STRUCT;

typedef MC0_CH0_CR_CADB_DLY_STRUCT MC0_CH3_CR_CADB_DLY_STRUCT;

typedef MC0_CH0_CR_CADB_STATUS_STRUCT MC0_CH3_CR_CADB_STATUS_STRUCT;

typedef MC0_CH0_CR_CADB_OVERRIDE_STRUCT MC0_CH3_CR_CADB_OVERRIDE_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_CFG_0_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_CFG_1_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_CFG_2_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_CFG_3_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_PBUF_1_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_PBUF_2_STRUCT;

typedef MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_PBUF_3_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_LMN_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_POLY_0_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_POLY_1_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_POLY_2_STRUCT;

typedef MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_STRUCT MC0_CH3_CR_CADB_DSEL_UNISEQ_POLY_3_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_0_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_1_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_2_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_3_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_4_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_5_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_6_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_7_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_8_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_9_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_10_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_11_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_12_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_13_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_14_STRUCT;

typedef MC0_CH0_CR_CADB_BUF_0_STRUCT MC0_CH3_CR_CADB_BUF_15_STRUCT;

typedef MC0_CH0_CR_CADB_AO_MRSCFG_STRUCT MC0_CH3_CR_CADB_AO_MRSCFG_STRUCT;

typedef MC0_CH0_CR_CADB_SELCFG_STRUCT MC0_CH3_CR_CADB_SELCFG_STRUCT;

typedef MC0_CH0_CR_CADB_MRSCFG_STRUCT MC0_CH3_CR_CADB_MRSCFG_STRUCT;
typedef union {
  struct {
    UINT32 IBECC_EN                                :  1;  // Bits 0:0
    UINT32                                         :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_IBECC_ACTIVATE_STRUCT;
typedef union {
  struct {
    UINT32 START_ADDR                              :  26;  // Bits 25:0
    UINT32                                         :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_IBECC_STORAGE_ADDR_STRUCT;
typedef union {
  struct {
    UINT32 BASE                                    :  26;  // Bits 25:0
    UINT32                                         :  6;  // Bits 31:26
    UINT32 MASK                                    :  26;  // Bits 57:32
    UINT32                                         :  5;  // Bits 62:58
    UINT32 ENABLE                                  :  1;  // Bits 63:63
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_IBECC_PROTECT_ADDR_RANGE_0_STRUCT;

typedef MC0_IBECC_PROTECT_ADDR_RANGE_0_STRUCT MC0_IBECC_PROTECT_ADDR_RANGE_1_STRUCT;

typedef MC0_IBECC_PROTECT_ADDR_RANGE_0_STRUCT MC0_IBECC_PROTECT_ADDR_RANGE_2_STRUCT;

typedef MC0_IBECC_PROTECT_ADDR_RANGE_0_STRUCT MC0_IBECC_PROTECT_ADDR_RANGE_3_STRUCT;

typedef MC0_IBECC_PROTECT_ADDR_RANGE_0_STRUCT MC0_IBECC_PROTECT_ADDR_RANGE_4_STRUCT;

typedef MC0_IBECC_PROTECT_ADDR_RANGE_0_STRUCT MC0_IBECC_PROTECT_ADDR_RANGE_5_STRUCT;

typedef MC0_IBECC_PROTECT_ADDR_RANGE_0_STRUCT MC0_IBECC_PROTECT_ADDR_RANGE_6_STRUCT;

typedef MC0_IBECC_PROTECT_ADDR_RANGE_0_STRUCT MC0_IBECC_PROTECT_ADDR_RANGE_7_STRUCT;
typedef union {
  struct {
    UINT32 OPERATION_MODE                          :  2;  // Bits 1:0
    UINT32 DISABLE_CORRECTION                      :  1;  // Bits 2:2
    UINT32 DISABLE_UFILL_POISON                    :  1;  // Bits 3:3
    UINT32 DISABLE_PCIE_ERR                        :  1;  // Bits 4:4
    UINT32 DISABLE_MCA_LOG                         :  1;  // Bits 5:5
    UINT32 DISABLE_VC_CACHING                      :  2;  // Bits 7:6
    UINT32 DISABLE_RANGE_CACHING                   :  8;  // Bits 15:8
    UINT32 HIT_ON_READY                            :  1;  // Bits 16:16
    UINT32 DISABLE_CROSS_VC_CHECK                  :  1;  // Bits 17:17
    UINT32 DISABLE_VC1_FF_ALLOCATION               :  1;  // Bits 18:18
    UINT32                                         :  13;  // Bits 31:19
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_IBECC_CONTROL_STRUCT;
typedef union {
  struct {
    UINT32                                         :  6;  // Bits 5:0
    UINT32 ADDRLow                                 :  26;  // Bits 31:6
    UINT32 ADDRHigh                                :  14;  // Bits 45:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_IBECC_INJ_ADDR_COMPARE_STRUCT;

typedef MC0_IBECC_INJ_ADDR_COMPARE_STRUCT MC0_IBECC_INJ_ADDR_MASK_STRUCT;
typedef union {
  struct {
    UINT32 COUNT                                   :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_IBECC_INJ_COUNT_STRUCT;
typedef union {
  struct {
    UINT32 ECC_Inj                                 :  3;  // Bits 2:0
    UINT32                                         :  29;  // Bits 31:3
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_IBECC_INJ_CONTROL_STRUCT;
typedef union {
  struct {
    UINT32                                         :  3;  // Bits 2:0
    UINT32 CERR_OVERFLOW                           :  1;  // Bits 3:3
    UINT32 MERR_OVERFLOW                           :  1;  // Bits 4:4
    UINT32 ERRADDLow                               :  27;  // Bits 31:5
    UINT32 ERRADDHigh                              :  14;  // Bits 45:32
    UINT32 ERRSYND                                 :  16;  // Bits 61:46
    UINT32 CERRSTS                                 :  1;  // Bits 62:62
    UINT32 MERRSTS                                 :  1;  // Bits 63:63
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_IBECC_ERROR_LOG_STRUCT;
typedef union {
  struct {
    UINT32 DEVICE_IDLow                            :  32;  // Bits 31:0
    UINT32 DEVICE_IDHigh                           :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_IBECC_TME_FM_DEVICE_ID_STRUCT;
typedef union {
  struct {
    UINT32                                         :  6;  // Bits 5:0
    UINT32 ERR_ADDRESSLow                          :  26;  // Bits 31:6
    UINT32 ERR_ADDRESSHigh                         :  14;  // Bits 45:32
    UINT32                                         :  12;  // Bits 57:46
    UINT32 ERR_TYPE                                :  4;  // Bits 61:58
    UINT32 PERR_OVERFLOW                           :  1;  // Bits 62:62
    UINT32 PERRSTS                                 :  1;  // Bits 63:63
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_IBECC_TMECC_PARITY_ERROR_LOG_STRUCT;
typedef union {
  struct {
    UINT32 NM_Enable                               :  1;  // Bits 0:0
    UINT32 Encrypt_PMEM                            :  1;  // Bits 1:1
    UINT32                                         :  2;  // Bits 3:2
    UINT32 PMEM_Base                               :  12;  // Bits 15:4
    UINT32 hash_lsb                                :  3;  // Bits 18:16
    UINT32 hash_enabled                            :  1;  // Bits 19:19
    UINT32 PMEM_Limit                              :  12;  // Bits 31:20
    UINT32 Reserved                                :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_IBECC_TME_CONTROL_STRUCT;
typedef union {
  struct {
    UINT32 DDR_TYPE                                :  3;  // Bits 2:0
    UINT32                                         :  1;  // Bits 3:3
    UINT32 CH_L_MAP                                :  1;  // Bits 4:4
    UINT32                                         :  7;  // Bits 11:5
    UINT32 CH_S_SIZE                               :  8;  // Bits 19:12
    UINT32                                         :  7;  // Bits 26:20
    UINT32 CH_WIDTH                                :  2;  // Bits 28:27
    UINT32                                         :  2;  // Bits 30:29
    UINT32 HalfCacheLineMode                       :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MAD_INTER_CHANNEL_STRUCT;
typedef union {
  struct {
    UINT32 DIMM_L_MAP                              :  1;  // Bits 0:0
    UINT32                                         :  7;  // Bits 7:1
    UINT32 EIM                                     :  1;  // Bits 8:8
    UINT32                                         :  3;  // Bits 11:9
    UINT32 ECC                                     :  2;  // Bits 13:12
    UINT32 CRC                                     :  1;  // Bits 14:14
    UINT32                                         :  17;  // Bits 31:15
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MAD_INTRA_CH0_STRUCT;

typedef MC0_MAD_INTRA_CH0_STRUCT MC0_MAD_INTRA_CH1_STRUCT;
typedef union {
  struct {
    UINT32 DIMM_L_SIZE                             :  7;  // Bits 6:0
    UINT32 DLW                                     :  2;  // Bits 8:7
    UINT32 DLNOR                                   :  2;  // Bits 10:9
    UINT32 ddr5_ds_8Gb                             :  1;  // Bits 11:11
    UINT32 ddr5_dl_8Gb                             :  1;  // Bits 12:12
    UINT32                                         :  3;  // Bits 15:13
    UINT32 DIMM_S_SIZE                             :  7;  // Bits 22:16
    UINT32                                         :  1;  // Bits 23:23
    UINT32 DSW                                     :  2;  // Bits 25:24
    UINT32 DSNOR                                   :  2;  // Bits 27:26
    UINT32 BG0_bit_options                         :  2;  // Bits 29:28
    UINT32 Decoder_EBH                             :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MAD_DIMM_CH0_STRUCT;

typedef MC0_MAD_DIMM_CH0_STRUCT MC0_MAD_DIMM_CH1_STRUCT;
typedef union {
  struct {
    UINT32 Spare_RW                                :  12;  // Bits 11:0
    UINT32 VISAByteSel                             :  4;  // Bits 15:12
    UINT32 spare_RW_V                              :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MCDECS_MISC_STRUCT;
typedef union {
  struct {
    UINT32 increase_rcomp                          :  1;  // Bits 0:0
    UINT32 rank2_to_rank1                          :  1;  // Bits 1:1
    UINT32 ovrd_pcu_sr_exit                        :  1;  // Bits 2:2
    UINT32 psmi_freeze_pwm_counters                :  1;  // Bits 3:3
    UINT32 dis_single_ch_sr                        :  1;  // Bits 4:4
    UINT32 dis_other_ch_stolen_ref                 :  1;  // Bits 5:5
    UINT32 ForceSREntry_dft                        :  1;  // Bits 6:6
    UINT32 reserved                                :  2;  // Bits 8:7
    UINT32 ForceSREntry_dft_is_sticky              :  1;  // Bits 9:9
    UINT32 freeze_AFD_on_RTB                       :  1;  // Bits 10:10
    UINT32 iosfsb_keep_ISM_active                  :  1;  // Bits 11:11
    UINT32 ignoreRefBetweenSRX2SRE                 :  1;  // Bits 12:12
    UINT32 override_external_regs_ack_miss_cbit    :  1;  // Bits 13:13
    UINT32 dis_cmi_spec_req_early_valid            :  1;  // Bits 14:14
    UINT32 Dis_EFLOW_fwd_data_hack                 :  1;  // Bits 15:15
    UINT32 freeze_visa_values                      :  1;  // Bits 16:16
    UINT32 freeze_visa_values_on_RTB               :  1;  // Bits 17:17
    UINT32 dis_cmi_ep_power_flows                  :  1;  // Bits 18:18
    UINT32 dis_iosf_sb_error_opcodes               :  1;  // Bits 19:19
    UINT32 dis_iosf_sb_error_bar                   :  1;  // Bits 20:20
    UINT32 dis_iosf_sb_error_range                 :  1;  // Bits 21:21
    UINT32 dis_iosf_sb_error_sai                   :  1;  // Bits 22:22
    UINT32 delay_normal_mode_when_temp_read        :  1;  // Bits 23:23
    UINT32 dis_cmi_spec_rsp_cpl_early_valid        :  1;  // Bits 24:24
    UINT32 force_sb_ep_clk_req                     :  1;  // Bits 25:25
    UINT32 dis_cmi_wr_rsp                          :  1;  // Bits 26:26
    UINT32 dis_iosf_sb_clk_gate                    :  1;  // Bits 27:27
    UINT32 dis_glbdrv_clk_gate                     :  1;  // Bits 28:28
    UINT32 dis_reg_clk_gate                        :  1;  // Bits 29:29
    UINT32                                         :  1;  // Bits 30:30
    UINT32 dis_clk_gate                            :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MCDECS_CBIT_STRUCT;
typedef union {
  struct {
    UINT32                                         :  6;  // Bits 5:0
    UINT32 HASH_MASK                               :  14;  // Bits 19:6
    UINT32                                         :  4;  // Bits 23:20
    UINT32 HASH_LSB_MASK_BIT                       :  3;  // Bits 26:24
    UINT32                                         :  1;  // Bits 27:27
    UINT32 HASH_MODE                               :  1;  // Bits 28:28
    UINT32                                         :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_CHANNEL_HASH_STRUCT;
typedef union {
  struct {
    UINT32                                         :  6;  // Bits 5:0
    UINT32 EHASH_MASK                              :  14;  // Bits 19:6
    UINT32                                         :  4;  // Bits 23:20
    UINT32 EHASH_LSB_MASK_BIT                      :  3;  // Bits 26:24
    UINT32                                         :  1;  // Bits 27:27
    UINT32 EHASH_MODE                              :  1;  // Bits 28:28
    UINT32                                         :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_CHANNEL_EHASH_STRUCT;
typedef union {
  struct {
    UINT32                                         :  1;  // Bits 0:0
    UINT32 ddr_reset                               :  1;  // Bits 1:1
    UINT32                                         :  1;  // Bits 2:2
    UINT32 refresh_enable                          :  1;  // Bits 3:3
    UINT32                                         :  1;  // Bits 4:4
    UINT32 mc_init_done_ack                        :  1;  // Bits 5:5
    UINT32                                         :  1;  // Bits 6:6
    UINT32 mrc_done                                :  1;  // Bits 7:7
    UINT32                                         :  1;  // Bits 8:8
    UINT32 pure_srx                                :  1;  // Bits 9:9
    UINT32                                         :  1;  // Bits 10:10
    UINT32 mrc_save_gv_point_0                     :  1;  // Bits 11:11
    UINT32 mrc_save_gv_point_1                     :  1;  // Bits 12:12
    UINT32 mrc_save_gv_point_2                     :  1;  // Bits 13:13
    UINT32 mrc_save_gv_point_3                     :  1;  // Bits 14:14
    UINT32                                         :  7;  // Bits 21:15
    UINT32 dclk_enable                             :  1;  // Bits 22:22
    UINT32                                         :  1;  // Bits 23:23
    UINT32 override_sr_enable                      :  1;  // Bits 24:24
    UINT32 override_sr_enable_value                :  1;  // Bits 25:25
    UINT32                                         :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MC_INIT_STATE_G_STRUCT;
typedef union {
  struct {
    UINT32 REVISION                                :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MRC_REVISION_STRUCT;
typedef union {
  struct {
    UINT32 CMI_Source_ID0                          :  5;  // Bits 4:0
    UINT32 CMI_Source_ID1                          :  5;  // Bits 9:5
    UINT32 CMI_Source_ID2                          :  5;  // Bits 14:10
    UINT32 CMI_Source_ID3                          :  5;  // Bits 19:15
    UINT32                                         :  12;  // Bits 31:20
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_STRUCT;
typedef union {
  struct {
    UINT32 countLow                                :  32;  // Bits 31:0
    UINT32 countHigh                               :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_PWM_TOTAL_REQCOUNT_STRUCT;

typedef MC0_PWM_TOTAL_REQCOUNT_STRUCT MC0_PWM_PROGRAMMABLE_REQCOUNT_0_STRUCT;

typedef MC0_PWM_TOTAL_REQCOUNT_STRUCT MC0_PWM_PROGRAMMABLE_REQCOUNT_1_STRUCT;

typedef MC0_PWM_TOTAL_REQCOUNT_STRUCT MC0_PWM_RDCAS_COUNT_STRUCT;
typedef union {
  struct {
    UINT32 Idle_timer                              :  16;  // Bits 15:0
    UINT32 SR_Enable                               :  1;  // Bits 16:16
    UINT32 delay_qsync                             :  2;  // Bits 18:17
    UINT32                                         :  13;  // Bits 31:19
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_PM_SREF_CONFIG_STRUCT;
typedef union {
  struct {
    UINT32 VC1_WR_CNFLT                            :  1;  // Bits 0:0
    UINT32 VC1_RD_CNFLT                            :  1;  // Bits 1:1
    UINT32                                         :  30;  // Bits 31:2
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_ATMC_STS_STRUCT;

typedef MC0_PWM_TOTAL_REQCOUNT_STRUCT MC0_READ_OCCUPANCY_COUNT_STRUCT;
typedef union {
  struct {
    UINT32 stall_until_drain                       :  1;  // Bits 0:0
    UINT32 stall_input                             :  1;  // Bits 1:1
    UINT32                                         :  2;  // Bits 3:2
    UINT32 mc_drained                              :  1;  // Bits 4:4
    UINT32                                         :  3;  // Bits 7:5
    UINT32 sr_state                                :  1;  // Bits 8:8
    UINT32                                         :  3;  // Bits 11:9
    UINT32 stall_state                             :  1;  // Bits 12:12
    UINT32                                         :  19;  // Bits 31:13
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_STALL_DRAIN_STRUCT;
typedef union {
  struct {
    UINT32 NonVC1Threshold                         :  4;  // Bits 3:0
    UINT32 VC1RdThreshold                          :  4;  // Bits 7:4
    UINT32 FixedRateEn                             :  1;  // Bits 8:8
    UINT32 HIGH_PRIO_LIM                           :  3;  // Bits 11:9
    UINT32 LOW_PRIO_LIM                            :  3;  // Bits 14:12
    UINT32 spare                                   :  8;  // Bits 22:15
    UINT32                                         :  9;  // Bits 31:23
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_IPC_MC_ARB_STRUCT;

typedef MC0_IPC_MC_ARB_STRUCT MC0_IPC_MC_DEC_ARB_STRUCT;
typedef union {
  struct {
    UINT32 RPQ_count                               :  6;  // Bits 5:0
    UINT32                                         :  2;  // Bits 7:6
    UINT32 WPQ_count                               :  7;  // Bits 14:8
    UINT32                                         :  1;  // Bits 15:15
    UINT32 IPQ_count                               :  5;  // Bits 20:16
    UINT32 WPQ_MinSlotsToReq                       :  4;  // Bits 24:21
    UINT32 IPQ_MinSlotsToReq                       :  3;  // Bits 27:25
    UINT32 RPQ_MinSlotsToReq                       :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_QUEUE_CREDIT_C_STRUCT;
typedef union {
  struct {
    UINT32 AddressLow                              :  32;  // Bits 31:0
    UINT32 AddressHigh                             :  1;  // Bits 32:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_ECC_INJ_ADDR_COMPARE_STRUCT;
typedef union {
  struct {
    UINT32                                         :  20;  // Bits 19:0
    UINT32 REMAPBASELow                            :  12;  // Bits 31:20
    UINT32 REMAPBASEHigh                           :  7;  // Bits 38:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_REMAPBASE_STRUCT;
typedef union {
  struct {
    UINT32                                         :  20;  // Bits 19:0
    UINT32 REMAPLMTLow                             :  12;  // Bits 31:20
    UINT32 REMAPLMTHigh                            :  7;  // Bits 38:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_REMAPLIMIT_STRUCT;

typedef MC0_PWM_TOTAL_REQCOUNT_STRUCT MC0_PWM_WRCAS_COUNT_STRUCT;

typedef MC0_PWM_TOTAL_REQCOUNT_STRUCT MC0_PWM_COMMAND_COUNT_STRUCT;

typedef MC0_PWM_TOTAL_REQCOUNT_STRUCT MC0_PWM_NON_SR_COUNT_STRUCT;
typedef union {
  struct {
    UINT32                                         :  20;  // Bits 19:0
    UINT32 TOLUD                                   :  12;  // Bits 31:20
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_TOLUD_STRUCT;
typedef union {
  struct {
    UINT32 sys_addrLow                             :  32;  // Bits 31:0
    UINT32 sys_addrHigh                            :  1;  // Bits 32:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_STRUCT;

typedef MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_STRUCT MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_1_STRUCT;

typedef MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_STRUCT MC0_GDXC_DDR_SYS_ADD_FILTER_MATCH_0_STRUCT;

typedef MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_STRUCT MC0_GDXC_DDR_SYS_ADD_FILTER_MATCH_1_STRUCT;

typedef MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_STRUCT MC0_GDXC_DDR_SYS_ADD_TRIGGER_MASK_STRUCT;

typedef MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_STRUCT MC0_GDXC_DDR_SYS_ADD_TRIGGER_MATCH_STRUCT;
typedef union {
  struct {
    UINT32 Isoch_time_window                       :  17;  // Bits 16:0
    UINT32 Write_starvation_window                 :  11;  // Bits 27:17
    UINT32 VC1_Read_starvation_en                  :  1;  // Bits 28:28
    UINT32 Write_starvation_in_Isoc_en             :  1;  // Bits 29:29
    UINT32 Read_starvation_in_Isoch_en             :  1;  // Bits 30:30
    UINT32 VC0_counter_disable                     :  1;  // Bits 31:31
    UINT32 Read_starvation_window                  :  11;  // Bits 42:32
    UINT32 VC0_read_count                          :  9;  // Bits 51:43
    UINT32 Force_MCVC1Demote                       :  1;  // Bits 52:52
    UINT32 Disable_MCVC1Demote                     :  1;  // Bits 53:53
    UINT32 MC_Ignore_VC1Demote                     :  1;  // Bits 54:54
    UINT32 Ignore_RGBSync                          :  1;  // Bits 55:55
    UINT32 Force_MC_WPriority                      :  1;  // Bits 56:56
    UINT32 Disable_MC_WPriority                    :  1;  // Bits 57:57
    UINT32 allow_cross_vc_blocking                 :  1;  // Bits 58:58
    UINT32 VC1_block_VC0                           :  1;  // Bits 59:59
    UINT32 VC0_block_VC1                           :  1;  // Bits 60:60
    UINT32 Delay_VC1_on_read_starvation            :  1;  // Bits 61:61
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_SC_QOS_STRUCT;
typedef union {
  struct {
    UINT32 GLB_GRACE_CNT                           :  8;  // Bits 7:0
    UINT32 GLB_DRV_GATE_DIS                        :  1;  // Bits 8:8
    UINT32 qclk_global_driver_override_to_dclk     :  1;  // Bits 9:9
    UINT32                                         :  22;  // Bits 31:10
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_STRUCT;
typedef union {
  struct {
    UINT32 delay_PM_Ack_cycles                     :  10;  // Bits 9:0
    UINT32 R1_Ack_reset_bgf_run                    :  1;  // Bits 10:10
    UINT32 SAGV_Ack_reset_bgf_run                  :  1;  // Bits 11:11
    UINT32 Dclk_en_reset_bgf_run                   :  1;  // Bits 12:12
    UINT32                                         :  3;  // Bits 15:13
    UINT32 PM_REQ_R0_received                      :  1;  // Bits 16:16
    UINT32 PM_REQ_R0_SAGV_received                 :  1;  // Bits 17:17
    UINT32 PM_REQ_R1_received                      :  1;  // Bits 18:18
    UINT32 PM_REQ_R1_deep_sr_received              :  1;  // Bits 19:19
    UINT32 PM_REQ_UnBlock_received                 :  1;  // Bits 20:20
    UINT32 PM_REQ_Acked                            :  1;  // Bits 21:21
    UINT32 MC_State_blocked_R0                     :  1;  // Bits 22:22
    UINT32 MC_State_blocked_R1                     :  1;  // Bits 23:23
    UINT32 Override_DDRPLDrainDone_Cbit            :  1;  // Bits 24:24
    UINT32                                         :  7;  // Bits 31:25
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_PM_CONTROL_STRUCT;
typedef union {
  struct {
    UINT32 CountLow                                :  32;  // Bits 31:0
    UINT32 CountHigh                               :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_PWM_COUNTERS_DURATION_STRUCT;
typedef union {
  struct {
    UINT32 cmi_req_stall_enable                    :  1;  // Bits 0:0
    UINT32 cmi_req_stall_phase                     :  1;  // Bits 1:1
    UINT32 block_scheduler                         :  4;  // Bits 5:2
    UINT32 pwm_clock_enable                        :  1;  // Bits 6:6
    UINT32 Mock_InSR                               :  1;  // Bits 7:7
    UINT32 dis_other_mc_stolen_ref                 :  1;  // Bits 8:8
    UINT32 allow_blockack_on_pending_srx           :  1;  // Bits 9:9
    UINT32 spare                                   :  1;  // Bits 10:10
    UINT32 two_cyc_early_ckstop_dis                :  1;  // Bits 11:11
    UINT32 dis_spid_cmd_clk_gate                   :  1;  // Bits 12:12
    UINT32                                         :  3;  // Bits 15:13
    UINT32 init_complete_override                  :  4;  // Bits 19:16
    UINT32 prevent_sr_on_vc1_high_prio             :  1;  // Bits 20:20
    UINT32 use_initcomplete_ch0_only               :  1;  // Bits 21:21
    UINT32 Mock_InSR_for_MC                        :  1;  // Bits 22:22
    UINT32 Allow_RH_Debt_in_SR                     :  1;  // Bits 23:23
    UINT32                                         :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MCDECS_SECOND_CBIT_STRUCT;
typedef union {
  struct {
    UINT32 AddressLow                              :  32;  // Bits 31:0
    UINT32 AddressHigh                             :  1;  // Bits 32:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_ECC_INJ_ADDR_MASK_STRUCT;
typedef union {
  struct {
    UINT32 RW_Isoch_time_window                    :  17;  // Bits 16:0
    UINT32 RW_Write_starvation_window              :  11;  // Bits 27:17
    UINT32                                         :  4;  // Bits 31:28
    UINT32 RW_Read_starvation_window               :  11;  // Bits 42:32
    UINT32 Isoc_during_demote_period_x8            :  8;  // Bits 50:43
    UINT32 Isoc_during_demote_window               :  8;  // Bits 58:51
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_SC_QOS2_STRUCT;
typedef union {
  struct {
    UINT32 Yellow_Decay_x128                       :  9;  // Bits 8:0
    UINT32 Yellow_Threshold                        :  10;  // Bits 18:9
    UINT32                                         :  13;  // Bits 31:19
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_SC_QOS3_STRUCT;
typedef union {
  struct {
    UINT32 normalmode                              :  1;  // Bits 0:0
    UINT32                                         :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_NORMALMODE_CFG_STRUCT;
typedef union {
  struct {
    UINT32                                         :  24;  // Bits 23:0
    UINT32 CPGC_ACTIVE                             :  1;  // Bits 24:24
    UINT32                                         :  3;  // Bits 27:25
    UINT32 CPGC_ECC_BYTE                           :  3;  // Bits 30:28
    UINT32 Stall_CPGC_CMI_Req                      :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MC_CPGC_CMI_STRUCT;
typedef union {
  struct {
    UINT32 Concat_Row2Col                          :  1;  // Bits 0:0
    UINT32 Col_align                               :  3;  // Bits 3:1
    UINT32 Concat_Bank2Row                         :  1;  // Bits 4:4
    UINT32 Concat_Col2Row                          :  1;  // Bits 5:5
    UINT32 Row_align                               :  3;  // Bits 8:6
    UINT32 Concat_BG2Bank                          :  1;  // Bits 9:9
    UINT32 dis_BankGroup                           :  1;  // Bits 10:10
    UINT32 Rank_as_it                              :  1;  // Bits 11:11
    UINT32 channel_is_CID_Zero                     :  1;  // Bits 12:12
    UINT32 Inverse_channel                         :  1;  // Bits 13:13
    UINT32 Delay_cpl_info                          :  1;  // Bits 14:14
    UINT32 Delay_cpl_data                          :  1;  // Bits 15:15
    UINT32 Reset_CPGC                              :  1;  // Bits 16:16
    UINT32 Lock_On_Active_CPGC_CMI_ISM             :  1;  // Bits 17:17
    UINT32 BG1_mask                                :  1;  // Bits 18:18
    UINT32 Rank1_as_ERM_BG1                        :  1;  // Bits 19:19
    UINT32 in_order_ingress                        :  1;  // Bits 20:20
    UINT32                                         :  11;  // Bits 31:21
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MC_CPGC_MISC_DFT_STRUCT;

typedef MC0_PWM_TOTAL_REQCOUNT_STRUCT MC0_PWM_GLB_DRV_OFF_COUNT_STRUCT;
typedef union {
  struct {
    UINT32 ERR_ADDRESSLow                          :  32;  // Bits 31:0
    UINT32 ERR_ADDRESSHigh                         :  7;  // Bits 38:32
    UINT32 RESERVED                                :  20;  // Bits 58:39
    UINT32 ERR_SRC                                 :  1;  // Bits 59:59
    UINT32 ERR_TYPE                                :  2;  // Bits 61:60
    UINT32 ERR_STS_OVERFLOW                        :  1;  // Bits 62:62
    UINT32 ERR_STS                                 :  1;  // Bits 63:63
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_PARITYERRLOG_STRUCT;
typedef union {
  struct {
    UINT32 DATA_ERR_EN                             :  8;  // Bits 7:0
    UINT32 ADDR_ERR_EN                             :  1;  // Bits 8:8
    UINT32 BE_ERR_EN                               :  2;  // Bits 10:9
    UINT32 RSVD                                    :  5;  // Bits 15:11
    UINT32 ERR_INJ_MASK                            :  5;  // Bits 20:16
    UINT32 RSVD2                                   :  10;  // Bits 30:21
    UINT32 DATA_ERR_INJ_SRC                        :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_PARITY_ERR_INJ_STRUCT;
typedef union {
  struct {
    UINT32 ADDR_PARITY_EN                          :  1;  // Bits 0:0
    UINT32 WBE_PARITY_EN                           :  1;  // Bits 1:1
    UINT32 WDATA_PARITY_EN                         :  2;  // Bits 3:2
    UINT32 RDATA_PARITY_EN                         :  1;  // Bits 4:4
    UINT32 RSVD_0                                  :  3;  // Bits 7:5
    UINT32 DIS_PARITY_PCIE_ERR                     :  1;  // Bits 8:8
    UINT32 DIS_PARITY_LOG                          :  1;  // Bits 9:9
    UINT32 RSVD_1                                  :  21;  // Bits 30:10
    UINT32 PARITY_EN                               :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_PARITY_CONTROL_STRUCT;
typedef union {
  struct {
    UINT32 Hash_enabled                            :  1;  // Bits 0:0
    UINT32 Hash_LSB                                :  3;  // Bits 3:1
    UINT32 Zone1_start                             :  9;  // Bits 12:4
    UINT32 Stacked_Mode                            :  1;  // Bits 13:13
    UINT32                                         :  18;  // Bits 31:14
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_MAD_MC_HASH_STRUCT;
typedef union {
  struct {
    UINT32 global_freeze                           :  1;  // Bits 0:0
    UINT32 Global_reset_ctrl                       :  1;  // Bits 1:1
    UINT32 Global_reset_ctrs                       :  1;  // Bits 2:2
    UINT32                                         :  29;  // Bits 31:3
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_PMON_GLOBAL_CONTROL_STRUCT;
typedef union {
  struct {
    UINT32 frz                                     :  1;  // Bits 0:0
    UINT32                                         :  7;  // Bits 7:1
    UINT32 reset_ctrl                              :  1;  // Bits 8:8
    UINT32 reset_ctrs                              :  1;  // Bits 9:9
    UINT32                                         :  22;  // Bits 31:10
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_PMON_UNIT_CONTROL_STRUCT;
typedef union {
  struct {
    UINT32 ev_sel                                  :  8;  // Bits 7:0
    UINT32 ch_mask                                 :  4;  // Bits 11:8
    UINT32                                         :  5;  // Bits 16:12
    UINT32 rst                                     :  1;  // Bits 17:17
    UINT32 edge_det                                :  1;  // Bits 18:18
    UINT32                                         :  13;  // Bits 31:19
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_PMON_COUNTER_CONTROL_0_STRUCT;

typedef MC0_PMON_COUNTER_CONTROL_0_STRUCT MC0_PMON_COUNTER_CONTROL_1_STRUCT;

typedef MC0_PMON_COUNTER_CONTROL_0_STRUCT MC0_PMON_COUNTER_CONTROL_2_STRUCT;

typedef MC0_PMON_COUNTER_CONTROL_0_STRUCT MC0_PMON_COUNTER_CONTROL_3_STRUCT;

typedef MC0_PMON_COUNTER_CONTROL_0_STRUCT MC0_PMON_COUNTER_CONTROL_4_STRUCT;
typedef union {
  struct {
    UINT32 event_countLow                          :  32;  // Bits 31:0
    UINT32 event_countHigh                         :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MC0_PMON_COUNTER_DATA_0_STRUCT;

typedef MC0_PMON_COUNTER_DATA_0_STRUCT MC0_PMON_COUNTER_DATA_1_STRUCT;

typedef MC0_PMON_COUNTER_DATA_0_STRUCT MC0_PMON_COUNTER_DATA_2_STRUCT;

typedef MC0_PMON_COUNTER_DATA_0_STRUCT MC0_PMON_COUNTER_DATA_3_STRUCT;

typedef MC0_PMON_COUNTER_DATA_0_STRUCT MC0_PMON_COUNTER_DATA_4_STRUCT;
typedef union {
  struct {
    UINT32 EnableOSTelemetry                       :  1;  // Bits 0:0
    UINT32                                         :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_OS_TELEMETRY_CONTROL_STRUCT;
typedef union {
  struct {
    UINT32 RTB_trace_all_rd_bits                   :  1;  // Bits 0:0
    UINT32 RTB_trace_all_wr_bits                   :  1;  // Bits 1:1
    UINT32 RTB_trace_all_BL4_select_high_bits      :  1;  // Bits 2:2
    UINT32                                         :  1;  // Bits 3:3
    UINT32 MCHTrace                                :  1;  // Bits 4:4
    UINT32 SCHTrace                                :  1;  // Bits 5:5
    UINT32 ECC_EN                                  :  1;  // Bits 6:6
    UINT32 ECC_BYTE_replace                        :  3;  // Bits 9:7
    UINT32 RSVD_16_10                              :  7;  // Bits 16:10
    UINT32 Enable_ACC_Trace                        :  1;  // Bits 17:17
    UINT32 Enable_ReadData_Trace                   :  1;  // Bits 18:18
    UINT32 Enable_WriteData_Trace                  :  1;  // Bits 19:19
    UINT32 DDRPL_Activate                          :  1;  // Bits 20:20
    UINT32 DDRPL_GLB_DRV_GATE_DIS                  :  1;  // Bits 21:21
    UINT32 RSVD_23_22                              :  2;  // Bits 23:22
    UINT32 DTF_ENC_CB                              :  1;  // Bits 24:24
    UINT32                                         :  1;  // Bits 25:25
    UINT32 BL4_bypass_trace_read                   :  1;  // Bits 26:26
    UINT32 BL4_bypass_trace_write                  :  1;  // Bits 27:27
    UINT32 Trace_2LM_Tag                           :  1;  // Bits 28:28
    UINT32 RTB_trace_rd_data_high_bits             :  1;  // Bits 29:29
    UINT32 RTB_trace_wr_data_high_bits             :  1;  // Bits 30:30
    UINT32                                         :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_DDRPL_CFG_DTF_STRUCT;
typedef union {
  struct {
    UINT32                                         :  16;  // Bits 15:0
    UINT32 Read_Data_UserDefined_Bits              :  8;  // Bits 23:16
    UINT32 Write_Data_UserDefined_Bits             :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_DDRPL_DEBUG_DTF_STRUCT;
typedef union {
  struct {
    UINT32 Visa_Qclk_Lane_1_out                    :  8;  // Bits 7:0
    UINT32 Visa_Qclk_Lane_0_out                    :  8;  // Bits 15:8
    UINT32 Visa_Dclk_Lane_1_out                    :  8;  // Bits 23:16
    UINT32 Visa_Dclk_Lane_0_out                    :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_DDRPL_VISA_LANES_STRUCT;
typedef union {
  struct {
    UINT32 VisaLane0_Data_Stage_Dclk_tree          :  2;  // Bits 1:0
    UINT32 VisaLane1_Data_Stage_Dclk_tree          :  2;  // Bits 3:2
    UINT32 VisaLane0_Data_Stage_Qclk_tree          :  2;  // Bits 5:4
    UINT32 VisaLane1_Data_Stage_Qclk_tree          :  2;  // Bits 7:6
    UINT32                                         :  24;  // Bits 31:8
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MC0_DDRPL_VISA_CFG_DTF_STRUCT;

#pragma pack(pop)
#endif
