#ifndef __MrcMcRegisterAdl0xxx_h__
#define __MrcMcRegisterAdl0xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define DATA0CH0_CR_DDRDATADQRANK0LANE0_REG                            (0x00000000)

  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycodep_OFF            ( 0)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycodep_WID            ( 8)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycodep_MSK            (0x000000FF)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycodep_MIN            (0)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycodep_MAX            (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycodep_DEF            (0x00000000)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycodep_HSH            (0x08000000)

  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycoden_OFF            ( 8)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycoden_WID            ( 8)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycoden_MSK            (0x0000FF00)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycoden_MIN            (0)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycoden_MAX            (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycoden_DEF            (0x00000000)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycoden_HSH            (0x08100000)

  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_OFF         (16)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_WID         ( 8)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_MSK         (0x00FF0000)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_MIN         (0)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_MAX         (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_DEF         (0x00000000)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_HSH         (0x08200000)

  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew90_OFF       (24)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew90_WID       ( 8)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew90_MSK       (0xFF000000)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew90_MIN       (0)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew90_MAX       (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew90_DEF       (0x00000000)
  #define DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew90_HSH       (0x08300000)

#define DATA0CH0_CR_DDRDATADQRANK0LANE1_REG                            (0x00000004)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK0LANE2_REG                            (0x00000008)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK0LANE3_REG                            (0x0000000C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK0LANE4_REG                            (0x00000010)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK0LANE5_REG                            (0x00000014)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK0LANE6_REG                            (0x00000018)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK0LANE7_REG                            (0x0000001C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK1LANE0_REG                            (0x00000020)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK1LANE1_REG                            (0x00000024)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK1LANE2_REG                            (0x00000028)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK1LANE3_REG                            (0x0000002C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK1LANE4_REG                            (0x00000030)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK1LANE5_REG                            (0x00000034)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK1LANE6_REG                            (0x00000038)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK1LANE7_REG                            (0x0000003C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK2LANE0_REG                            (0x00000040)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK2LANE1_REG                            (0x00000044)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK2LANE2_REG                            (0x00000048)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK2LANE3_REG                            (0x0000004C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK2LANE4_REG                            (0x00000050)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK2LANE5_REG                            (0x00000054)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK2LANE6_REG                            (0x00000058)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK2LANE7_REG                            (0x0000005C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK3LANE0_REG                            (0x00000060)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK3LANE1_REG                            (0x00000064)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK3LANE2_REG                            (0x00000068)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK3LANE3_REG                            (0x0000006C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK3LANE4_REG                            (0x00000070)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK3LANE5_REG                            (0x00000074)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK3LANE6_REG                            (0x00000078)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRDATADQRANK3LANE7_REG                            (0x0000007C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH0_CR_DDRCRDATACONTROL0_REG                              (0x00000080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_Reserved0_OFF                  ( 0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Reserved0_WID                  ( 5)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Reserved0_MSK                  (0x0000001F)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Reserved0_MIN                  (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Reserved0_MAX                  (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Reserved0_DEF                  (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Reserved0_HSH                  (0x05000080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_gear1_OFF                      ( 5)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_gear1_WID                      ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_gear1_MSK                      (0x00000020)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_gear1_MIN                      (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_gear1_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_gear1_DEF                      (0x00000001)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_gear1_HSH                      (0x010A0080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_tx_clock_on_with_txanalogen_OFF ( 6)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_tx_clock_on_with_txanalogen_WID ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_tx_clock_on_with_txanalogen_MSK (0x00000040)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_tx_clock_on_with_txanalogen_MIN (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_tx_clock_on_with_txanalogen_MAX (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_tx_clock_on_with_txanalogen_DEF (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_tx_clock_on_with_txanalogen_HSH (0x010C0080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_local_gate_d0tx_OFF            ( 7)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_local_gate_d0tx_WID            ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_local_gate_d0tx_MSK            (0x00000080)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_local_gate_d0tx_MIN            (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_local_gate_d0tx_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_local_gate_d0tx_DEF            (0x00000001)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_local_gate_d0tx_HSH            (0x010E0080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_Gear4_OFF                      ( 8)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Gear4_WID                      ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Gear4_MSK                      (0x00000100)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Gear4_MIN                      (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Gear4_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Gear4_DEF                      (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_Gear4_HSH                      (0x01100080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_internalclockson_OFF           ( 9)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_internalclockson_WID           ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_internalclockson_MSK           (0x00000200)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_internalclockson_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_internalclockson_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_internalclockson_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_internalclockson_HSH           (0x01120080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_wllongdelen_OFF                (10)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_wllongdelen_WID                ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_wllongdelen_MSK                (0x00000400)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_wllongdelen_MIN                (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_wllongdelen_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_wllongdelen_DEF                (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_wllongdelen_HSH                (0x01140080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_txpion_OFF                     (11)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txpion_WID                     ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txpion_MSK                     (0x00000800)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txpion_MIN                     (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txpion_MAX                     (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txpion_DEF                     (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txpion_HSH                     (0x01160080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_txon_OFF                       (12)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txon_WID                       ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txon_MSK                       (0x00001000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txon_MIN                       (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txon_MAX                       (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txon_DEF                       (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txon_HSH                       (0x01180080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_txdisable_OFF                  (13)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txdisable_WID                  ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txdisable_MSK                  (0x00002000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txdisable_MIN                  (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txdisable_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txdisable_DEF                  (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txdisable_HSH                  (0x011A0080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqodtparkmode_OFF            (14)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqodtparkmode_WID            ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqodtparkmode_MSK            (0x00004000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqodtparkmode_MIN            (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqodtparkmode_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqodtparkmode_DEF            (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqodtparkmode_HSH            (0x011C0080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqsodtparkmode_OFF           (15)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqsodtparkmode_WID           ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqsodtparkmode_MSK           (0x00008000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqsodtparkmode_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqsodtparkmode_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqsodtparkmode_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_endqsodtparkmode_HSH           (0x011E0080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_biaspmctrl_OFF                 (16)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_biaspmctrl_WID                 ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_biaspmctrl_MSK                 (0x00030000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_biaspmctrl_MIN                 (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_biaspmctrl_MAX                 (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACONTROL0_biaspmctrl_DEF                 (0x00000002)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_biaspmctrl_HSH                 (0x02200080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_vrefpmctrl_OFF                 (18)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_vrefpmctrl_WID                 ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_vrefpmctrl_MSK                 (0x000C0000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_vrefpmctrl_MIN                 (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_vrefpmctrl_MAX                 (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACONTROL0_vrefpmctrl_DEF                 (0x00000001)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_vrefpmctrl_HSH                 (0x02240080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxdisable_OFF                  (20)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxdisable_WID                  ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxdisable_MSK                  (0x00100000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxdisable_MIN                  (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxdisable_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxdisable_DEF                  (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxdisable_HSH                  (0x01280080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_forcerxon_OFF                  (21)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_forcerxon_WID                  ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_forcerxon_MSK                  (0x00200000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_forcerxon_MIN                  (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_forcerxon_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_forcerxon_DEF                  (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_forcerxon_HSH                  (0x012A0080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxrankmuxdelay_2ndstg_OFF      (22)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxrankmuxdelay_2ndstg_WID      ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxrankmuxdelay_2ndstg_MSK      (0x00C00000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxrankmuxdelay_2ndstg_MIN      (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxrankmuxdelay_2ndstg_MAX      (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxrankmuxdelay_2ndstg_DEF      (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_rxrankmuxdelay_2ndstg_HSH      (0x022C0080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_datarst_OFF                    (24)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_datarst_WID                    ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_datarst_MSK                    (0x01000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_datarst_MIN                    (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_datarst_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_datarst_DEF                    (0x00000001)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_datarst_HSH                    (0x01300080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtforceqdrven_OFF             (25)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtforceqdrven_WID             ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtforceqdrven_MSK             (0x02000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtforceqdrven_MIN             (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtforceqdrven_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtforceqdrven_DEF             (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtforceqdrven_HSH             (0x01320080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtsampoff_OFF                 (26)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtsampoff_WID                 ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtsampoff_MSK                 (0x04000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtsampoff_MIN                 (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtsampoff_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtsampoff_DEF                 (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_odtsampoff_HSH                 (0x01340080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_ddrcrforceodton_OFF            (27)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_ddrcrforceodton_WID            ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_ddrcrforceodton_MSK            (0x08000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_ddrcrforceodton_MIN            (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_ddrcrforceodton_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL0_ddrcrforceodton_DEF            (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_ddrcrforceodton_HSH            (0x01360080)

  #define DATA0CH0_CR_DDRCRDATACONTROL0_txrankmuxdelay_OFF             (28)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txrankmuxdelay_WID             ( 4)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txrankmuxdelay_MSK             (0xF0000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txrankmuxdelay_MIN             (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txrankmuxdelay_MAX             (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txrankmuxdelay_DEF             (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL0_txrankmuxdelay_HSH             (0x04380080)

#define DATA0CH0_CR_DDRCRDATACONTROL1_REG                              (0x00000084)

  #define DATA0CH0_CR_DDRCRDATACONTROL1_safemodeenable_OFF             ( 0)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_safemodeenable_WID             ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_safemodeenable_MSK             (0x00000001)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_safemodeenable_MIN             (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_safemodeenable_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL1_safemodeenable_DEF             (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_safemodeenable_HSH             (0x01000084)

  #define DATA0CH0_CR_DDRCRDATACONTROL1_rxrankmuxdelay_OFF             ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_rxrankmuxdelay_WID             ( 4)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_rxrankmuxdelay_MSK             (0x0000001E)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_rxrankmuxdelay_MIN             (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_rxrankmuxdelay_MAX             (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACONTROL1_rxrankmuxdelay_DEF             (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_rxrankmuxdelay_HSH             (0x04020084)

  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtdelay_OFF                ( 5)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtdelay_WID                ( 6)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtdelay_MSK                (0x000007E0)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtdelay_MIN                (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtdelay_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtdelay_DEF                (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtdelay_HSH                (0x060A0084)

  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_OFF             (11)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_WID             ( 3)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_MSK             (0x00003800)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_MIN             (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_MAX             (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_DEF             (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_HSH             (0x03160084)

  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtdelay_OFF                 (14)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtdelay_WID                 ( 5)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtdelay_MSK                 (0x0007C000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtdelay_MIN                 (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtdelay_MAX                 (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtdelay_DEF                 (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtdelay_HSH                 (0x051C0084)

  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_OFF              (19)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_WID              ( 4)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_MSK              (0x00780000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_MIN              (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_MAX              (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_DEF              (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_HSH              (0x04260084)

  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampdelay_OFF              (23)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampdelay_WID              ( 5)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampdelay_MSK              (0x0F800000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampdelay_MIN              (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampdelay_MAX              (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampdelay_DEF              (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampdelay_HSH              (0x052E0084)

  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampduration_OFF           (28)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampduration_WID           ( 4)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampduration_MSK           (0xF0000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampduration_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampduration_MAX           (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampduration_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL1_senseampduration_HSH           (0x04380084)

#define DATA0CH0_CR_DDRCRDATACONTROL2_REG                              (0x00000088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_datainvertnibble_OFF           ( 0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_datainvertnibble_WID           ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_datainvertnibble_MSK           (0x00000003)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_datainvertnibble_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_datainvertnibble_MAX           (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACONTROL2_datainvertnibble_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_datainvertnibble_HSH           (0x02000088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_dqsodtcompoffset_OFF           ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dqsodtcompoffset_WID           ( 5)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dqsodtcompoffset_MSK           (0x0000007C)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dqsodtcompoffset_MIN           (-16)              // Manual Edit
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dqsodtcompoffset_MAX           (15) // 0x0000000F // Manual Edit
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dqsodtcompoffset_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dqsodtcompoffset_HSH           (0x85040088)       // Manual Edit

  #define DATA0CH0_CR_DDRCRDATACONTROL2_gear1_A0_OFF                      ( 7)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_gear1_A0_WID                      ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_gear1_A0_MSK                      (0x00000080)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_gear1_A0_MIN                      (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_gear1_A0_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_gear1_A0_DEF                      (0x00000001)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_gear1_A0_HSH                      (0x010E0088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_A0_OFF                 ( 8)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_A0_WID                 ( 3)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_A0_MSK                 (0x00000700)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_A0_MIN                 (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_A0_MAX                 (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_A0_DEF                 (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_A0_HSH                 (0x03100088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_A0_OFF                    (11)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_A0_WID                    ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_A0_MSK                    (0x00000800)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_A0_MIN                    (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_A0_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_A0_DEF                    (0x00000001)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_A0_HSH                    (0x01160088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_A0_OFF           (12)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_A0_WID           ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_A0_MSK           (0x00001000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_A0_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_A0_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_A0_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_A0_HSH           (0x01180088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_A0_OFF (13)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_A0_WID ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_A0_MSK (0x00006000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_A0_MIN (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_A0_MAX (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_A0_DEF (0x00000002)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_A0_HSH (0x021A0088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_A0_OFF               (15)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_A0_WID               ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_A0_MSK               (0x00008000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_A0_MIN               (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_A0_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_A0_DEF               (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_A0_HSH               (0x011E0088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_A0_OFF (16)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_A0_WID ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_A0_MSK (0x00030000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_A0_MIN (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_A0_MAX (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_A0_DEF (0x00000002)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_A0_HSH (0x02200088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_A0_OFF          (18)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_A0_WID          ( 4)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_A0_MSK          (0x003C0000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_A0_MIN          (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_A0_MAX          (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_A0_DEF          (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_A0_HSH          (0x04240088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_A0_OFF                 (22)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_A0_WID                 ( 3)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_A0_MSK                 (0x01C00000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_A0_MIN                 (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_A0_MAX                 (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_A0_DEF                 (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_A0_HSH                 (0x032C0088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_A0_OFF                  (25)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_A0_WID                  ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_A0_MSK                  (0x02000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_A0_MIN                  (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_A0_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_A0_DEF                  (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_A0_HSH                  (0x01320088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_A0_OFF      (26)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_A0_WID      ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_A0_MSK      (0x04000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_A0_MIN      (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_A0_MAX      (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_A0_DEF      (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_A0_HSH      (0x01340088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_A0_OFF                (27)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_A0_WID                ( 5)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_A0_MSK                (0xF8000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_A0_MIN                (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_A0_MAX                (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_A0_DEF                (0x00000011)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_A0_HSH                (0x05360088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_OFF                 ( 7)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_WID                 ( 3)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_MSK                 (0x00000380)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_MIN                 (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_MAX                 (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_DEF                 (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_HSH                 (0x030E0088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_OFF                    (10)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_WID                    ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_MSK                    (0x00000400)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_MIN                    (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_DEF                    (0x00000001)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_dbimode_HSH                    (0x01140088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_OFF           (11)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_WID           ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_MSK           (0x00000800)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdate_ovren_HSH           (0x01160088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_OFF (12)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_WID ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_MSK (0x00003000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_MIN (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_MAX (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_DEF (0x00000002)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_2nd_stage_offset_HSH (0x02180088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_OFF               (14)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_WID               ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_MSK               (0x00004000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_MIN               (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_DEF               (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_disabletxdqs_HSH               (0x011C0088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_OFF (15)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_WID ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_MSK (0x00018000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_MIN (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_MAX (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_DEF (0x00000002)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txrankmuxdelay_2nd_stage_offset_HSH (0x021E0088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_OFF          (17)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_WID          ( 4)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_MSK          (0x001E0000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_MIN          (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_MAX          (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_DEF          (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_HSH          (0x04220088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_OFF                 (21)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_WID                 ( 3)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_MSK                 (0x00E00000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_MIN                 (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_MAX                 (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_DEF                 (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_HSH                 (0x032A0088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_OFF                  (24)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_WID                  ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_MSK                  (0x01000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_MIN                  (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_DEF                  (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_Reserved1_HSH                  (0x01300088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_OFF      (25)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_WID      ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_MSK      (0x02000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_MIN      (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_MAX      (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_DEF      (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_compupdatedone_ovrval_HSH      (0x01320088)

  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_OFF                (26)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_WID                ( 6)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_MSK                (0xFC000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_MIN                (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_DEF                (0x0000002F)
  #define DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_HSH                (0x06340088)

#define DATA0CH0_CR_DDRCRDATACONTROL3_REG                              (0x0000008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_usedefaultrdptrcalc_OFF        ( 0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_usedefaultrdptrcalc_WID        ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_usedefaultrdptrcalc_MSK        (0x00000001)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_usedefaultrdptrcalc_MIN        (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_usedefaultrdptrcalc_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL3_usedefaultrdptrcalc_DEF        (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_usedefaultrdptrcalc_HSH        (0x0100008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_rstnumpre_OFF                  ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rstnumpre_WID                  ( 3)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rstnumpre_MSK                  (0x0000000E)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rstnumpre_MIN                  (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rstnumpre_MAX                  (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rstnumpre_DEF                  (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rstnumpre_HSH                  (0x0302008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxtogglepreamble_OFF           ( 4)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxtogglepreamble_WID           ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxtogglepreamble_MSK           (0x00000030)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxtogglepreamble_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxtogglepreamble_MAX           (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxtogglepreamble_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxtogglepreamble_HSH           (0x0208008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_rcvenwaveshape_OFF             ( 6)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rcvenwaveshape_WID             ( 3)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rcvenwaveshape_MSK             (0x000001C0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rcvenwaveshape_MIN             (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rcvenwaveshape_MAX             (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rcvenwaveshape_DEF             (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rcvenwaveshape_HSH             (0x030C008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxreadpointer_OFF              ( 9)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxreadpointer_WID              ( 5)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxreadpointer_MSK              (0x00003E00)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxreadpointer_MIN              (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxreadpointer_MAX              (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxreadpointer_DEF              (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxreadpointer_HSH              (0x0512008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxvref_OFF                     (14)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxvref_WID                     ( 9)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxvref_MSK                     (0x007FC000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxvref_MIN                     (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxvref_MAX                     (511) // 0x000001FF
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxvref_DEF                     (0x00000100)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxvref_HSH                     (0x091C008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_toggle_OFF   (23)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_toggle_WID   ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_toggle_MSK   (0x00800000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_toggle_MIN   (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_toggle_MAX   (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_toggle_DEF   (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_toggle_HSH   (0x012E008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_enable_OFF   (24)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_enable_WID   ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_enable_MSK   (0x01000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_enable_MIN   (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_enable_MAX   (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_enable_DEF   (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_wr1p5tckpostamble_enable_HSH   (0x0130008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_commandaddrparityodd_OFF       (25)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_commandaddrparityodd_WID       ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_commandaddrparityodd_MSK       (0x02000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_commandaddrparityodd_MIN       (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_commandaddrparityodd_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL3_commandaddrparityodd_DEF       (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_commandaddrparityodd_HSH       (0x0132008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_caparitytrain_OFF              (26)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_caparitytrain_WID              ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_caparitytrain_MSK              (0x04000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_caparitytrain_MIN              (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_caparitytrain_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL3_caparitytrain_DEF              (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_caparitytrain_HSH              (0x0134008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr5_OFF                     (27)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr5_WID                     ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr5_MSK                     (0x08000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr5_MIN                     (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr5_MAX                     (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr5_DEF                     (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr5_HSH                     (0x0136008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr_OFF                      (28)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr_WID                      ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr_MSK                      (0x10000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr_MIN                      (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr_DEF                      (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_lpddr_HSH                      (0x0138008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxpion_OFF                     (29)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxpion_WID                     ( 1)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxpion_MSK                     (0x20000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxpion_MIN                     (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxpion_MAX                     (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxpion_DEF                     (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_rxpion_HSH                     (0x013A008C)

  #define DATA0CH0_CR_DDRCRDATACONTROL3_dram_rddqsoffset_OFF           (30)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_dram_rddqsoffset_WID           ( 2)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_dram_rddqsoffset_MSK           (0xC0000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_dram_rddqsoffset_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_dram_rddqsoffset_MAX           (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACONTROL3_dram_rddqsoffset_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACONTROL3_dram_rddqsoffset_HSH           (0x023C008C)

#define DATA0CH0_CR_AFEMISC_REG                                        (0x00000090)

  #define DATA0CH0_CR_AFEMISC_Reserved2_OFF                            ( 0)
  #define DATA0CH0_CR_AFEMISC_Reserved2_WID                            ( 3)
  #define DATA0CH0_CR_AFEMISC_Reserved2_MSK                            (0x00000007)
  #define DATA0CH0_CR_AFEMISC_Reserved2_MIN                            (0)
  #define DATA0CH0_CR_AFEMISC_Reserved2_MAX                            (7) // 0x00000007
  #define DATA0CH0_CR_AFEMISC_Reserved2_DEF                            (0x00000000)
  #define DATA0CH0_CR_AFEMISC_Reserved2_HSH                            (0x03000090)

  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefctrl_OFF               ( 3)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefctrl_WID               ( 9)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefctrl_MSK               (0x00000FF8)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefctrl_MIN               (0)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefctrl_MAX               (511) // 0x000001FF
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefctrl_DEF               (0x00000100)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefctrl_HSH               (0x09060090)

  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvsxhi_OFF                  (12)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvsxhi_WID                  ( 1)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvsxhi_MSK                  (0x00001000)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvsxhi_MIN                  (0)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvsxhi_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvsxhi_DEF                  (0x00000001)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvsxhi_HSH                  (0x01180090)

  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvddsupply_OFF              (13)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvddsupply_WID              ( 1)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvddsupply_MSK              (0x00002000)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvddsupply_MIN              (0)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvddsupply_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvddsupply_DEF              (0x00000000)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_selvddsupply_HSH              (0x011A0090)

  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefgenenable_OFF          (14)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefgenenable_WID          ( 1)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefgenenable_MSK          (0x00004000)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefgenenable_MIN          (0)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefgenenable_MAX          (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefgenenable_DEF          (0x00000000)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrvrefgenenable_HSH          (0x011C0090)

  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrlevshftbypasspmos_OFF      (15)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrlevshftbypasspmos_WID      ( 1)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrlevshftbypasspmos_MSK      (0x00008000)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrlevshftbypasspmos_MIN      (0)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrlevshftbypasspmos_MAX      (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrlevshftbypasspmos_DEF      (0x00000000)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrlevshftbypasspmos_HSH      (0x011E0090)

  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrenvrefgenweakres_OFF       (16)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrenvrefgenweakres_WID       ( 1)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrenvrefgenweakres_MSK       (0x00010000)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrenvrefgenweakres_MIN       (0)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrenvrefgenweakres_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrenvrefgenweakres_DEF       (0x00000000)
  #define DATA0CH0_CR_AFEMISC_rxvref_cmn_ddrenvrefgenweakres_HSH       (0x01200090)

  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_vrefsel_OFF                   (17)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_vrefsel_WID                   ( 4)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_vrefsel_MSK                   (0x001E0000)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_vrefsel_MIN                   (0)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_vrefsel_MAX                   (15) // 0x0000000F
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_vrefsel_DEF                   (0x00000005)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_vrefsel_HSH                   (0x04220090)

  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_tailctl_OFF                   (21)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_tailctl_WID                   ( 2)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_tailctl_MSK                   (0x00600000)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_tailctl_MIN                   (0)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_tailctl_MAX                   (3) // 0x00000003
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_tailctl_DEF                   (0x00000002)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_tailctl_HSH                   (0x022A0090)

  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_biasicomp_OFF                 (23)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_biasicomp_WID                 ( 4)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_biasicomp_MSK                 (0x07800000)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_biasicomp_MIN                 (0)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_biasicomp_MAX                 (15) // 0x0000000F
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_biasicomp_DEF                 (0x00000008)
  #define DATA0CH0_CR_AFEMISC_rxbias_cmn_biasicomp_HSH                 (0x042E0090)

  #define DATA0CH0_CR_AFEMISC_rxall_cmn_rxlpddrmode_OFF                (27)
  #define DATA0CH0_CR_AFEMISC_rxall_cmn_rxlpddrmode_WID                ( 2)
  #define DATA0CH0_CR_AFEMISC_rxall_cmn_rxlpddrmode_MSK                (0x18000000)
  #define DATA0CH0_CR_AFEMISC_rxall_cmn_rxlpddrmode_MIN                (0)
  #define DATA0CH0_CR_AFEMISC_rxall_cmn_rxlpddrmode_MAX                (3) // 0x00000003
  #define DATA0CH0_CR_AFEMISC_rxall_cmn_rxlpddrmode_DEF                (0x00000001)
  #define DATA0CH0_CR_AFEMISC_rxall_cmn_rxlpddrmode_HSH                (0x02360090)

  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_parkval_OFF                (29)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_parkval_WID                ( 1)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_parkval_MSK                (0x20000000)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_parkval_MIN                (0)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_parkval_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_parkval_DEF                (0x00000000)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_parkval_HSH                (0x013A0090)

  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_iobufact_OFF               (30)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_iobufact_WID               ( 1)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_iobufact_MSK               (0x40000000)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_iobufact_MIN               (0)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_iobufact_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_iobufact_DEF               (0x00000000)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_iobufact_HSH               (0x013C0090)

  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_globalvsshibyp_OFF         (31)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_globalvsshibyp_WID         ( 1)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_globalvsshibyp_MSK         (0x80000000)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_globalvsshibyp_MIN         (0)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_globalvsshibyp_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_globalvsshibyp_DEF         (0x00000000)
  #define DATA0CH0_CR_AFEMISC_afeshared_cmn_globalvsshibyp_HSH         (0x013E0090)

#define DATA0CH0_CR_SRZCTL_REG                                         (0x00000094)

  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdtailctl_OFF              ( 0)
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdtailctl_WID              ( 2)
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdtailctl_MSK              (0x00000003)
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdtailctl_MIN              (0)
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdtailctl_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdtailctl_DEF              (0x00000002)
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdtailctl_HSH              (0x02000094)

  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdselsecondaryclk_OFF      ( 2)
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdselsecondaryclk_WID      ( 1)
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdselsecondaryclk_MSK      (0x00000004)
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdselsecondaryclk_MIN      (0)
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdselsecondaryclk_MAX      (1) // 0x00000001
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdselsecondaryclk_DEF      (0x00000000)
  #define DATA0CH0_CR_SRZCTL_dqsdcdsrz_cmn_dcdselsecondaryclk_HSH      (0x01040094)

  #define DATA0CH0_CR_SRZCTL_dqdcdsrz_cmn_dcdselsecondaryclk_OFF       ( 3)
  #define DATA0CH0_CR_SRZCTL_dqdcdsrz_cmn_dcdselsecondaryclk_WID       ( 1)
  #define DATA0CH0_CR_SRZCTL_dqdcdsrz_cmn_dcdselsecondaryclk_MSK       (0x00000008)
  #define DATA0CH0_CR_SRZCTL_dqdcdsrz_cmn_dcdselsecondaryclk_MIN       (0)
  #define DATA0CH0_CR_SRZCTL_dqdcdsrz_cmn_dcdselsecondaryclk_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_SRZCTL_dqdcdsrz_cmn_dcdselsecondaryclk_DEF       (0x00000000)
  #define DATA0CH0_CR_SRZCTL_dqdcdsrz_cmn_dcdselsecondaryclk_HSH       (0x01060094)

  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear4en_OFF                    ( 4)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear4en_WID                    ( 1)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear4en_MSK                    (0x00000010)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear4en_MIN                    (0)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear4en_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear4en_DEF                    (0x00000000)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear4en_HSH                    (0x01080094)

  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear2en_OFF                    ( 5)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear2en_WID                    ( 1)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear2en_MSK                    (0x00000020)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear2en_MIN                    (0)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear2en_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear2en_DEF                    (0x00000000)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear2en_HSH                    (0x010A0094)

  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear1en_OFF                    ( 6)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear1en_WID                    ( 1)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear1en_MSK                    (0x00000040)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear1en_MIN                    (0)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear1en_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear1en_DEF                    (0x00000001)
  #define DATA0CH0_CR_SRZCTL_dcdsrz_cmn_gear1en_HSH                    (0x010C0094)

  #define DATA0CH0_CR_SRZCTL_cktop_cmn_bonus_OFF                       ( 7)
  #define DATA0CH0_CR_SRZCTL_cktop_cmn_bonus_WID                       ( 8)
  #define DATA0CH0_CR_SRZCTL_cktop_cmn_bonus_MSK                       (0x00007F80)
  #define DATA0CH0_CR_SRZCTL_cktop_cmn_bonus_MIN                       (0)
  #define DATA0CH0_CR_SRZCTL_cktop_cmn_bonus_MAX                       (255) // 0x000000FF
  #define DATA0CH0_CR_SRZCTL_cktop_cmn_bonus_DEF                       (0x00000078)
  #define DATA0CH0_CR_SRZCTL_cktop_cmn_bonus_HSH                       (0x080E0094)

  #define DATA0CH0_CR_SRZCTL_pghub_cmn_bonus_OFF                       (15)
  #define DATA0CH0_CR_SRZCTL_pghub_cmn_bonus_WID                       ( 2)
  #define DATA0CH0_CR_SRZCTL_pghub_cmn_bonus_MSK                       (0x00018000)
  #define DATA0CH0_CR_SRZCTL_pghub_cmn_bonus_MIN                       (0)
  #define DATA0CH0_CR_SRZCTL_pghub_cmn_bonus_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_SRZCTL_pghub_cmn_bonus_DEF                       (0x00000000)
  #define DATA0CH0_CR_SRZCTL_pghub_cmn_bonus_HSH                       (0x021E0094)

  #define DATA0CH0_CR_SRZCTL_dqsbuf_cmn_bonus_OFF                      (17)
  #define DATA0CH0_CR_SRZCTL_dqsbuf_cmn_bonus_WID                      ( 4)
  #define DATA0CH0_CR_SRZCTL_dqsbuf_cmn_bonus_MSK                      (0x001E0000)
  #define DATA0CH0_CR_SRZCTL_dqsbuf_cmn_bonus_MIN                      (0)
  #define DATA0CH0_CR_SRZCTL_dqsbuf_cmn_bonus_MAX                      (15) // 0x0000000F
  #define DATA0CH0_CR_SRZCTL_dqsbuf_cmn_bonus_DEF                      (0x00000002)
  #define DATA0CH0_CR_SRZCTL_dqsbuf_cmn_bonus_HSH                      (0x04220094)

  #define DATA0CH0_CR_SRZCTL_dqbuf_cmn_bonus_OFF                       (21)
  #define DATA0CH0_CR_SRZCTL_dqbuf_cmn_bonus_WID                       ( 2)
  #define DATA0CH0_CR_SRZCTL_dqbuf_cmn_bonus_MSK                       (0x00600000)
  #define DATA0CH0_CR_SRZCTL_dqbuf_cmn_bonus_MIN                       (0)
  #define DATA0CH0_CR_SRZCTL_dqbuf_cmn_bonus_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_SRZCTL_dqbuf_cmn_bonus_DEF                       (0x00000000)
  #define DATA0CH0_CR_SRZCTL_dqbuf_cmn_bonus_HSH                       (0x022A0094)

  #define DATA0CH0_CR_SRZCTL_imod_func_dqstx_cmn_txclkpden_OFF         (23)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqstx_cmn_txclkpden_WID         ( 1)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqstx_cmn_txclkpden_MSK         (0x00800000)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqstx_cmn_txclkpden_MIN         (0)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqstx_cmn_txclkpden_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_SRZCTL_imod_func_dqstx_cmn_txclkpden_DEF         (0x00000000)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqstx_cmn_txclkpden_HSH         (0x012E0094)

  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenvcciog_OFF   (24)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenvcciog_WID   ( 1)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenvcciog_MSK   (0x01000000)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenvcciog_MIN   (0)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenvcciog_MAX   (1) // 0x00000001
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenvcciog_DEF   (0x00000000)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenvcciog_HSH   (0x01300094)

  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqspsal_OFF (25)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqspsal_WID ( 1)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqspsal_MSK (0x02000000)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqspsal_MIN (0)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqspsal_MAX (1) // 0x00000001
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqspsal_DEF (0x00000000)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqspsal_HSH (0x01320094)

  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqsnsal_OFF (26)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqsnsal_WID ( 1)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqsnsal_MSK (0x04000000)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqsnsal_MIN (0)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqsnsal_MAX (1) // 0x00000001
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqsnsal_DEF (0x00000000)
  #define DATA0CH0_CR_SRZCTL_imod_func_dqsdcdsrz_cmn_dcdenrxdqsnsal_HSH (0x01340094)

  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonrise_OFF    (27)
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonrise_WID    ( 2)
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonrise_MSK    (0x18000000)
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonrise_MIN    (0)
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonrise_MAX    (3) // 0x00000003
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonrise_DEF    (0x00000000)
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonrise_HSH    (0x02360094)

  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonfall_OFF    (29)
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonfall_WID    ( 2)
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonfall_MSK    (0x60000000)
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonfall_MIN    (0)
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonfall_MAX    (3) // 0x00000003
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonfall_DEF    (0x00000000)
  #define DATA0CH0_CR_SRZCTL_omod_func_dqstx_cmn_txpfirstonfall_HSH    (0x023A0094)

  #define DATA0CH0_CR_SRZCTL_Reserved3_OFF                             (31)
  #define DATA0CH0_CR_SRZCTL_Reserved3_WID                             ( 1)
  #define DATA0CH0_CR_SRZCTL_Reserved3_MSK                             (0x80000000)
  #define DATA0CH0_CR_SRZCTL_Reserved3_MIN                             (0)
  #define DATA0CH0_CR_SRZCTL_Reserved3_MAX                             (1) // 0x00000001
  #define DATA0CH0_CR_SRZCTL_Reserved3_DEF                             (0x00000000)
  #define DATA0CH0_CR_SRZCTL_Reserved3_HSH                             (0x013E0094)

#define DATA0CH0_CR_DQRXCTL0_REG                                       (0x00000098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap4_OFF                 ( 0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap4_WID                 ( 3)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap4_MSK                 (0x00000007)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap4_MIN                 (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap4_MAX                 (7) // 0x00000007
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap4_DEF                 (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap4_HSH                 (0x03000098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap3_OFF                 ( 3)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap3_WID                 ( 4)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap3_MSK                 (0x00000078)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap3_MIN                 (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap3_MAX                 (15) // 0x0000000F
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap3_DEF                 (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap3_HSH                 (0x04060098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap2_OFF                 ( 7)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap2_WID                 ( 5)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap2_MSK                 (0x00000F80)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap2_MIN                 (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap2_MAX                 (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap2_DEF                 (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap2_HSH                 (0x050E0098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap1_OFF                 (12)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap1_WID                 ( 4)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap1_MSK                 (0x0000F000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap1_MIN                 (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap1_MAX                 (15) // 0x0000000F
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap1_DEF                 (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap1_HSH                 (0x04180098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe3en_OFF                  (16)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe3en_WID                  ( 1)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe3en_MSK                  (0x00010000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe3en_MIN                  (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe3en_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe3en_DEF                  (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe3en_HSH                  (0x01200098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe2en_OFF                  (17)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe2en_WID                  ( 1)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe2en_MSK                  (0x00020000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe2en_MIN                  (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe2en_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe2en_DEF                  (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe2en_HSH                  (0x01220098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe1en_OFF                  (18)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe1en_WID                  ( 1)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe1en_MSK                  (0x00040000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe1en_MIN                  (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe1en_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe1en_DEF                  (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe1en_HSH                  (0x01240098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxtrainmodeen_OFF        (19)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxtrainmodeen_WID        ( 1)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxtrainmodeen_MSK        (0x00080000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxtrainmodeen_MIN        (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxtrainmodeen_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxtrainmodeen_DEF        (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxtrainmodeen_HSH        (0x01260098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxforceen_OFF            (20)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxforceen_WID            ( 1)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxforceen_MSK            (0x00100000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxforceen_MIN            (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxforceen_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxforceen_DEF            (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxpadmuxforceen_HSH            (0x01280098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleres_OFF                 (21)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleres_WID                 ( 2)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleres_MSK                 (0x00600000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleres_MIN                 (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleres_MAX                 (3) // 0x00000003
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleres_DEF                 (0x00000001)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleres_HSH                 (0x022A0098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleeq_OFF                  (23)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleeq_WID                  ( 4)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleeq_MSK                  (0x07800000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleeq_MIN                  (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleeq_MAX                  (15) // 0x0000000F
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleeq_DEF                  (0x00000008)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleeq_HSH                  (0x042E0098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctlecap_OFF                 (27)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctlecap_WID                 ( 2)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctlecap_MSK                 (0x18000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctlecap_MIN                 (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctlecap_MAX                 (3) // 0x00000003
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctlecap_DEF                 (0x00000001)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctlecap_HSH                 (0x02360098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxdiffampen_OFF                (29)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxdiffampen_WID                ( 1)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxdiffampen_MSK                (0x20000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxdiffampen_MIN                (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxdiffampen_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxdiffampen_DEF                (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxdiffampen_HSH                (0x013A0098)

  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmdqcben_OFF                  (30)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmdqcben_WID                  ( 2)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmdqcben_MSK                  (0xC0000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmdqcben_MIN                  (0)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmdqcben_MAX                  (3) // 0x00000003
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmdqcben_DEF                  (0x00000000)
  #define DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmdqcben_HSH                  (0x023C0098)

#define DATA0CH0_CR_DQRXCTL1_REG                                       (0x0000009C)

  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvsshiffstrobestlegen_OFF     ( 0)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvsshiffstrobestlegen_WID     ( 1)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvsshiffstrobestlegen_MSK     (0x00000001)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvsshiffstrobestlegen_MIN     (0)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvsshiffstrobestlegen_MAX     (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvsshiffstrobestlegen_DEF     (0x00000001)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvsshiffstrobestlegen_HSH     (0x0100009C)

  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvttmfc_OFF               ( 1)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvttmfc_WID               ( 3)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvttmfc_MSK               (0x0000000E)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvttmfc_MIN               (0)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvttmfc_MAX               (7) // 0x00000007
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvttmfc_DEF               (0x00000000)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvttmfc_HSH               (0x0302009C)

  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvssmfc_OFF               ( 4)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvssmfc_WID               ( 3)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvssmfc_MSK               (0x00000070)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvssmfc_MIN               (0)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvssmfc_MAX               (7) // 0x00000007
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvssmfc_DEF               (0x00000000)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvssmfc_HSH               (0x0308009C)

  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvdd2gmfc_OFF             ( 7)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvdd2gmfc_WID             ( 3)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvdd2gmfc_MSK             (0x00000380)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvdd2gmfc_MIN             (0)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvdd2gmfc_MAX             (7) // 0x00000007
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvdd2gmfc_DEF             (0x00000007)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvdd2gmfc_HSH             (0x030E009C)

  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmdqbwsel_OFF                 (10)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmdqbwsel_WID                 ( 6)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmdqbwsel_MSK                 (0x0000FC00)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmdqbwsel_MIN                 (0)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmdqbwsel_MAX                 (63) // 0x0000003F
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmdqbwsel_DEF                 (0x00000020)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmdqbwsel_HSH                 (0x0614009C)

  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxtrainreset_OFF               (16)
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxtrainreset_WID               ( 1)
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxtrainreset_MSK               (0x00010000)
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxtrainreset_MIN               (0)
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxtrainreset_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxtrainreset_DEF               (0x00000001)
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxtrainreset_HSH               (0x0120009C)

  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxmvcmres_OFF                  (17)
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxmvcmres_WID                  ( 2)
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxmvcmres_MSK                  (0x00060000)
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxmvcmres_MIN                  (0)
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxmvcmres_MAX                  (3) // 0x00000003
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxmvcmres_DEF                  (0x00000001)
  #define DATA0CH0_CR_DQRXCTL1_dxrx_cmn_rxmvcmres_HSH                  (0x0222009C)

  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxurangesel_OFF                (19)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxurangesel_WID                ( 1)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxurangesel_MSK                (0x00080000)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxurangesel_MIN                (0)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxurangesel_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxurangesel_DEF                (0x00000000)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxurangesel_HSH                (0x0126009C)

  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxupwrmuxsel_OFF               (20)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxupwrmuxsel_WID               ( 1)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxupwrmuxsel_MSK               (0x00100000)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxupwrmuxsel_MIN               (0)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxupwrmuxsel_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxupwrmuxsel_DEF               (0x00000000)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxupwrmuxsel_HSH               (0x0128009C)

  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmpdqdlycode_OFF              (21)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmpdqdlycode_WID              ( 8)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmpdqdlycode_MSK              (0x1FE00000)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmpdqdlycode_MIN              (0)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmpdqdlycode_MAX              (255) // 0x000000FF
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmpdqdlycode_DEF              (0x00000000)
  #define DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxmpdqdlycode_HSH              (0x082A009C)

  #define DATA0CH0_CR_DQRXCTL1_forcedfedisable_OFF                     (29)
  #define DATA0CH0_CR_DQRXCTL1_forcedfedisable_WID                     ( 1)
  #define DATA0CH0_CR_DQRXCTL1_forcedfedisable_MSK                     (0x20000000)
  #define DATA0CH0_CR_DQRXCTL1_forcedfedisable_MIN                     (0)
  #define DATA0CH0_CR_DQRXCTL1_forcedfedisable_MAX                     (1) // 0x00000001
  #define DATA0CH0_CR_DQRXCTL1_forcedfedisable_DEF                     (0x00000000)
  #define DATA0CH0_CR_DQRXCTL1_forcedfedisable_HSH                     (0x013A009C)

  #define DATA0CH0_CR_DQRXCTL1_Reserved4_OFF                           (30)
  #define DATA0CH0_CR_DQRXCTL1_Reserved4_WID                           ( 2)
  #define DATA0CH0_CR_DQRXCTL1_Reserved4_MSK                           (0xC0000000)
  #define DATA0CH0_CR_DQRXCTL1_Reserved4_MIN                           (0)
  #define DATA0CH0_CR_DQRXCTL1_Reserved4_MAX                           (3) // 0x00000003
  #define DATA0CH0_CR_DQRXCTL1_Reserved4_DEF                           (0x00000000)
  #define DATA0CH0_CR_DQRXCTL1_Reserved4_HSH                           (0x023C009C)

#define DATA0CH0_CR_DQSTXRXCTL_REG                                     (0x000000A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsp_OFF         ( 0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsp_WID         ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsp_MSK         (0x00000001)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsp_MIN         (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsp_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsp_DEF         (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsp_HSH         (0x010000A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprnfdelay_OFF          ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprnfdelay_WID          ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprnfdelay_MSK          (0x0000003E)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprnfdelay_MIN          (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprnfdelay_MAX          (31) // 0x0000001F
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprnfdelay_DEF          (0x00000010)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprnfdelay_HSH          (0x050200A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfnrdelay_OFF          ( 6)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfnrdelay_WID          ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfnrdelay_MSK          (0x000007C0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfnrdelay_MIN          (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfnrdelay_MAX          (31) // 0x0000001F
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfnrdelay_DEF          (0x00000010)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfnrdelay_HSH          (0x050C00A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfalldelay_OFF         ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfalldelay_WID         ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfalldelay_MSK         (0x0000003E)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfalldelay_MIN         (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfalldelay_MAX         (31) // 0x0000001F
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfalldelay_DEF         (0x0000001F)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqspfalldelay_HSH         (0x050200A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprisedelay_OFF         ( 6)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprisedelay_WID         ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprisedelay_MSK         (0x000007C0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprisedelay_MIN         (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprisedelay_MAX         (31) // 0x0000001F
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprisedelay_DEF         (0x0000001F)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txdqsprisedelay_HSH         (0x050C00A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsn_OFF         (11)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsn_WID         ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsn_MSK         (0x00000800)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsn_MIN         (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsn_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsn_DEF         (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvttparkendqsn_HSH         (0x011600A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvsshiffstlegen_OFF        (12)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvsshiffstlegen_WID        ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvsshiffstlegen_MSK        (0x00001000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvsshiffstlegen_MIN        (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvsshiffstlegen_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvsshiffstlegen_DEF        (0x00000001)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txvsshiffstlegen_HSH        (0x011800A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodtstatlegendqs_OFF       (13)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodtstatlegendqs_WID       ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodtstatlegendqs_MSK       (0x00002000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodtstatlegendqs_MIN       (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodtstatlegendqs_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodtstatlegendqs_DEF       (0x00000001)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodtstatlegendqs_HSH       (0x011A00A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodten_A0_OFF                 (14)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodten_A0_WID                 ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodten_A0_MSK                 (0x00004000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodten_A0_MIN                 (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodten_A0_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodten_A0_DEF                 (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodten_A0_HSH                 (0x011C00A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_A0_OFF         (15)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_A0_WID         ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_A0_MSK         (0x00008000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_A0_MIN         (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_A0_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_A0_DEF         (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_A0_HSH         (0x011E00A0)

  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_A0_OFF                           (16)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_A0_WID                           ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_A0_MSK                           (0x00010000)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_A0_MIN                           (0)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_A0_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_A0_DEF                           (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_A0_HSH                           (0x012000A0)

  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_A0_OFF                           (17)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_A0_WID                           ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_A0_MSK                           (0x00020000)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_A0_MIN                           (0)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_A0_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_A0_DEF                           (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_A0_HSH                           (0x012200A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_OFF         (14)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_WID         ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_MSK         (0x00004000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_MIN         (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_DEF         (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txidlemodedrven_HSH         (0x011C00A0)

  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_OFF                           (15)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_WID                           ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_MSK                           (0x00008000)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_MIN                           (0)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_DEF                           (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_1_HSH                           (0x011E00A0)

  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_OFF                           (16)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_WID                           ( 2)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_MSK                           (0x00030000)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_MIN                           (0)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_MAX                           (3) // 0x00000003
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_DEF                           (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_bonus_0_HSH                           (0x022000A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxsetailctl_OFF             (18)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxsetailctl_WID             ( 2)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxsetailctl_MSK             (0x000C0000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxsetailctl_MIN             (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxsetailctl_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxsetailctl_DEF             (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxsetailctl_HSH             (0x022400A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmtailctl_OFF              (20)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmtailctl_WID              ( 2)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmtailctl_MSK              (0x00300000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmtailctl_MIN              (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmtailctl_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmtailctl_DEF              (0x00000003)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmtailctl_HSH              (0x022800A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleres_OFF              (22)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleres_WID              ( 2)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleres_MSK              (0x00C00000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleres_MIN              (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleres_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleres_DEF              (0x00000001)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleres_HSH              (0x022C00A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleeq_OFF               (24)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleeq_WID               ( 4)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleeq_MSK               (0x0F000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleeq_MIN               (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleeq_MAX               (15) // 0x0000000F
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleeq_DEF               (0x00000008)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleeq_HSH               (0x043000A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctlecap_OFF              (28)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctlecap_WID              ( 2)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctlecap_MSK              (0x30000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctlecap_MIN              (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctlecap_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctlecap_DEF              (0x00000001)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctlecap_HSH              (0x023800A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxendqsnrcven_OFF           (30)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxendqsnrcven_WID           ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxendqsnrcven_MSK           (0x40000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxendqsnrcven_MIN           (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxendqsnrcven_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxendqsnrcven_DEF           (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxendqsnrcven_HSH           (0x013C00A0)

  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxdiffampen_OFF             (31)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxdiffampen_WID             ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxdiffampen_MSK             (0x80000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxdiffampen_MIN             (0)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxdiffampen_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxdiffampen_DEF             (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxdiffampen_HSH             (0x013E00A0)

#define DATA0CH0_CR_DQSTXRXCTL0_REG                                    (0x000000A4)

  #define DATA0CH0_CR_DQSTXRXCTL0_spare_OFF                            ( 0)
  #define DATA0CH0_CR_DQSTXRXCTL0_spare_WID                            ( 9)
  #define DATA0CH0_CR_DQSTXRXCTL0_spare_MSK                            (0x000001FF)
  #define DATA0CH0_CR_DQSTXRXCTL0_spare_MIN                            (0)
  #define DATA0CH0_CR_DQSTXRXCTL0_spare_MAX                            (511) // 0x000001FF
  #define DATA0CH0_CR_DQSTXRXCTL0_spare_DEF                            (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL0_spare_HSH                            (0x090000A4)


  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnfalldelay_OFF        ( 0)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnfalldelay_WID        ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnfalldelay_MSK        (0x0000001F)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnfalldelay_MIN        (0)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnfalldelay_MAX        (31) // 0x0000001F
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnfalldelay_DEF        (0x0000001F)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnfalldelay_HSH        (0x050000A4)

  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnrisedelay_OFF        ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnrisedelay_WID        ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnrisedelay_MSK        (0x000003E0)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnrisedelay_MIN        (0)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnrisedelay_MAX        (31) // 0x0000001F
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnrisedelay_DEF        (0x0000001F)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqstx_cmn_txdqsnrisedelay_HSH        (0x050A00A4)

  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_Q0_OFF                    (10)
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_Q0_WID                    ( 2)
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_Q0_MSK                    (0x00000C00)
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_Q0_MIN                    (0)
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_Q0_MAX                    (3) // 0x00000003
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_Q0_DEF                    (0x00000003)
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_Q0_HSH                    (0x021400A4)

  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_OFF                    ( 9)
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_WID                    ( 2)
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_MSK                    (0x00000600)
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_MIN                    (0)
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_MAX                    (3) // 0x00000003
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_DEF                    (0x00000003)
  #define DATA0CH0_CR_DQSTXRXCTL0_txdccrangesel_HSH                    (0x021200A4)

  #define DATA0CH0_CR_DQSTXRXCTL0_forcedfedisable_OFF                  (11)
  #define DATA0CH0_CR_DQSTXRXCTL0_forcedfedisable_WID                  ( 1)
  #define DATA0CH0_CR_DQSTXRXCTL0_forcedfedisable_MSK                  (0x00000800)
  #define DATA0CH0_CR_DQSTXRXCTL0_forcedfedisable_MIN                  (0)
  #define DATA0CH0_CR_DQSTXRXCTL0_forcedfedisable_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DQSTXRXCTL0_forcedfedisable_DEF                  (0x00000000)
  #define DATA0CH0_CR_DQSTXRXCTL0_forcedfedisable_HSH                  (0x011600A4)

  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk3_OFF       (12)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk3_WID       ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk3_MSK       (0x0001F000)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk3_MIN       (0)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk3_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk3_DEF       (0x00000010)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk3_HSH       (0x051800A4)

  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk2_OFF       (17)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk2_WID       ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk2_MSK       (0x003E0000)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk2_MIN       (0)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk2_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk2_DEF       (0x00000010)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk2_HSH       (0x052200A4)

  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk1_OFF       (22)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk1_WID       ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk1_MSK       (0x07C00000)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk1_MIN       (0)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk1_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk1_DEF       (0x00000010)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk1_HSH       (0x052C00A4)

  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk0_OFF       (27)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk0_WID       ( 5)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk0_MSK       (0xF8000000)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk0_MIN       (0)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk0_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk0_DEF       (0x00000010)
  #define DATA0CH0_CR_DQSTXRXCTL0_dqsrx_cmn_rxmampoffset_rk0_HSH       (0x053600A4)

#define DATA0CH0_CR_SDLCTL1_REG                                        (0x000000A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdllengthsel_OFF             ( 0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdllengthsel_WID             ( 2)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdllengthsel_MSK             (0x00000003)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdllengthsel_MIN             (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdllengthsel_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdllengthsel_DEF             (0x00000003)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdllengthsel_HSH             (0x020000A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpienable_OFF                 ( 2)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpienable_WID                 ( 1)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpienable_MSK                 (0x00000004)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpienable_MIN                 (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpienable_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpienable_DEF                 (0x00000000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpienable_HSH                 (0x010400A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpicapsel_OFF                 ( 3)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpicapsel_WID                 ( 3)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpicapsel_MSK                 (0x00000038)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpicapsel_MIN                 (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpicapsel_MAX                 (7) // 0x00000007
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpicapsel_DEF                 (0x00000000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxpicapsel_HSH                 (0x030600A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxphsdrvprocsel_OFF            ( 6)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxphsdrvprocsel_WID            ( 5)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxphsdrvprocsel_MSK            (0x000007C0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxphsdrvprocsel_MIN            (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxphsdrvprocsel_MAX            (31) // 0x0000001F
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxphsdrvprocsel_DEF            (0x00000007)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxphsdrvprocsel_HSH            (0x050C00A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_readlevel_OFF                  (11)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_readlevel_WID                  ( 1)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_readlevel_MSK                  (0x00000800)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_readlevel_MIN                  (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_readlevel_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_readlevel_DEF                  (0x00000000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_readlevel_HSH                  (0x011600A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_qualifysdlwithrcven_OFF        (12)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_qualifysdlwithrcven_WID        ( 1)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_qualifysdlwithrcven_MSK        (0x00001000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_qualifysdlwithrcven_MIN        (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_qualifysdlwithrcven_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_qualifysdlwithrcven_DEF        (0x00000001)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_qualifysdlwithrcven_HSH        (0x011800A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_pbddlycodedqenptr_OFF          (13)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_pbddlycodedqenptr_WID          ( 8)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_pbddlycodedqenptr_MSK          (0x001FE000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_pbddlycodedqenptr_MIN          (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_pbddlycodedqenptr_MAX          (255) // 0x000000FF
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_pbddlycodedqenptr_DEF          (0x00000000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_pbddlycodedqenptr_HSH          (0x081A00A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_passrcvenondqsfall_OFF         (21)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_passrcvenondqsfall_WID         ( 1)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_passrcvenondqsfall_MSK         (0x00200000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_passrcvenondqsfall_MIN         (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_passrcvenondqsfall_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_passrcvenondqsfall_DEF         (0x00000000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_passrcvenondqsfall_HSH         (0x012A00A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_burstlen_OFF                   (22)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_burstlen_WID                   ( 3)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_burstlen_MSK                   (0x01C00000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_burstlen_MIN                   (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_burstlen_MAX                   (7) // 0x00000007
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_burstlen_DEF                   (0x00000000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_burstlen_HSH                   (0x032C00A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdlbwsel_OFF                 (25)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdlbwsel_WID                 ( 6)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdlbwsel_MSK                 (0x7E000000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdlbwsel_MIN                 (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdlbwsel_MAX                 (63) // 0x0000003F
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdlbwsel_DEF                 (0x00000020)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_rxsdlbwsel_HSH                 (0x063200A8)

  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_srzclkend0_OFF                 (31)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_srzclkend0_WID                 ( 1)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_srzclkend0_MSK                 (0x80000000)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_srzclkend0_MIN                 (0)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_srzclkend0_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_srzclkend0_DEF                 (0x00000001)
  #define DATA0CH0_CR_SDLCTL1_rxsdl_cmn_srzclkend0_HSH                 (0x013E00A8)

#define DATA0CH0_CR_SDLCTL0_REG                                        (0x000000AC)

  #define DATA0CH0_CR_SDLCTL0_spare_OFF                                ( 0)
  #define DATA0CH0_CR_SDLCTL0_spare_WID                                (17)
  #define DATA0CH0_CR_SDLCTL0_spare_MSK                                (0x0001FFFF)
  #define DATA0CH0_CR_SDLCTL0_spare_MIN                                (0)
  #define DATA0CH0_CR_SDLCTL0_spare_MAX                                (131071) // 0x0001FFFF
  #define DATA0CH0_CR_SDLCTL0_spare_DEF                                (0x00000000)
  #define DATA0CH0_CR_SDLCTL0_spare_HSH                                (0x110000AC)

  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpip_OFF        (17)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpip_WID        ( 4)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpip_MSK        (0x001E0000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpip_MIN        (0)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpip_MAX        (15) // 0x0000000F
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpip_DEF        (0x00000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpip_HSH        (0x042200AC)

  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpin_OFF        (21)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpin_WID        ( 4)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpin_MSK        (0x01E00000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpin_MIN        (0)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpin_MAX        (15) // 0x0000000F
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpin_DEF        (0x00000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverselpin_HSH        (0x042A00AC)

  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverovr_OFF           (25)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverovr_WID           ( 1)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverovr_MSK           (0x02000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverovr_MIN           (0)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverovr_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverovr_DEF           (0x00000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxamp2pixoverovr_HSH           (0x013200AC)

  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_mdlllen2gearratio_OFF          (26)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_mdlllen2gearratio_WID          ( 1)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_mdlllen2gearratio_MSK          (0x04000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_mdlllen2gearratio_MIN          (0)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_mdlllen2gearratio_MAX          (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_mdlllen2gearratio_DEF          (0x00000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_mdlllen2gearratio_HSH          (0x013400AC)

  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_sdlen_OFF                      (27)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_sdlen_WID                      ( 1)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_sdlen_MSK                      (0x08000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_sdlen_MIN                      (0)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_sdlen_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_sdlen_DEF                      (0x00000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_sdlen_HSH                      (0x013600AC)

  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxunmatchclkinv_OFF            (28)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxunmatchclkinv_WID            ( 1)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxunmatchclkinv_MSK            (0x10000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxunmatchclkinv_MIN            (0)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxunmatchclkinv_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxunmatchclkinv_DEF            (0x00000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxunmatchclkinv_HSH            (0x013800AC)

  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxmatchedpathen_OFF            (29)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxmatchedpathen_WID            ( 1)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxmatchedpathen_MSK            (0x20000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxmatchedpathen_MIN            (0)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxmatchedpathen_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxmatchedpathen_DEF            (0x00000001)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxmatchedpathen_HSH            (0x013A00AC)

  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_enabledqsnrcven_OFF            (30)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_enabledqsnrcven_WID            ( 1)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_enabledqsnrcven_MSK            (0x40000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_enabledqsnrcven_MIN            (0)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_enabledqsnrcven_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_enabledqsnrcven_DEF            (0x00000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_enabledqsnrcven_HSH            (0x013C00AC)

  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_srzclkenrcven_OFF              (31)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_srzclkenrcven_WID              ( 1)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_srzclkenrcven_MSK              (0x80000000)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_srzclkenrcven_MIN              (0)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_srzclkenrcven_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_srzclkenrcven_DEF              (0x00000001)
  #define DATA0CH0_CR_SDLCTL0_rxsdl_cmn_srzclkenrcven_HSH              (0x013E00AC)

#define DATA0CH0_CR_MDLLCTL0_REG                                       (0x000000B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turbocaptrim_OFF               ( 0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turbocaptrim_WID               ( 2)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turbocaptrim_MSK               (0x00000003)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turbocaptrim_MIN               (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turbocaptrim_MAX               (3) // 0x00000003
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turbocaptrim_DEF               (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turbocaptrim_HSH               (0x020000B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turboonstartup_OFF             ( 2)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turboonstartup_WID             ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turboonstartup_MSK             (0x00000004)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turboonstartup_MIN             (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turboonstartup_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turboonstartup_DEF             (0x00000001)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_turboonstartup_HSH             (0x010400B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_OFF                ( 3)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_WID                ( 6)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_MSK                (0x000001F8)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_MIN                (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_DEF                (0x00000020)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_HSH                (0x060600B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_parkval_OFF                    ( 9)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_parkval_WID                    ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_parkval_MSK                    (0x00000200)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_parkval_MIN                    (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_parkval_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_parkval_DEF                    (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_parkval_HSH                    (0x011200B0)

  #define DATA0CH0_CR_MDLLCTL0_parkval_ovr_val_OFF                     ( 9)
  #define DATA0CH0_CR_MDLLCTL0_parkval_ovr_val_WID                     ( 1)
  #define DATA0CH0_CR_MDLLCTL0_parkval_ovr_val_MSK                     (0x00000200)
  #define DATA0CH0_CR_MDLLCTL0_parkval_ovr_val_MIN                     (0)
  #define DATA0CH0_CR_MDLLCTL0_parkval_ovr_val_MAX                     (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_parkval_ovr_val_DEF                     (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_parkval_ovr_val_HSH                     (0x011200B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlphgenin_OFF                  (10)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlphgenin_WID                  ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlphgenin_MSK                  (0x00000400)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlphgenin_MIN                  (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlphgenin_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlphgenin_DEF                  (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlphgenin_HSH                  (0x011400B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlrdacholden_OFF               (11)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlrdacholden_WID               ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlrdacholden_MSK               (0x00000800)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlrdacholden_MIN               (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlrdacholden_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlrdacholden_DEF               (0x00000001)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_wlrdacholden_HSH               (0x011600B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_weaklocken_OFF                 (12)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_weaklocken_WID                 ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_weaklocken_MSK                 (0x00001000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_weaklocken_MIN                 (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_weaklocken_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_weaklocken_DEF                 (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_weaklocken_HSH                 (0x011800B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_dllbypassen_OFF                (13)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_dllbypassen_WID                ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_dllbypassen_MSK                (0x00002000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_dllbypassen_MIN                (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_dllbypassen_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_dllbypassen_DEF                (0x00000001)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_dllbypassen_HSH                (0x011A00B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllengthsel_OFF               (14)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllengthsel_WID               ( 2)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllengthsel_MSK               (0x0000C000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllengthsel_MIN               (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllengthsel_MAX               (3) // 0x00000003
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllengthsel_DEF               (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllengthsel_HSH               (0x021C00B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_phdeterr_OFF                   (16)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_phdeterr_WID                   ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_phdeterr_MSK                   (0x00010000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_phdeterr_MIN                   (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_phdeterr_MAX                   (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_phdeterr_DEF                   (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_phdeterr_HSH                   (0x012000B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdlllock_OFF                   (17)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdlllock_WID                   ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdlllock_MSK                   (0x00020000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdlllock_MIN                   (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdlllock_MAX                   (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdlllock_DEF                   (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdlllock_HSH                   (0x012200B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_pien_OFF                       (18)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_pien_WID                       ( 8)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_pien_MSK                       (0x03FC0000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_pien_MIN                       (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_pien_MAX                       (255) // 0x000000FF
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_pien_DEF                       (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_pien_HSH                       (0x082400B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllen_OFF                     (26)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllen_WID                     ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllen_MSK                     (0x04000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllen_MIN                     (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllen_MAX                     (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllen_DEF                     (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_mdllen_HSH                     (0x013400B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlval_OFF                   (27)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlval_WID                   ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlval_MSK                   (0x08000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlval_MIN                   (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlval_MAX                   (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlval_DEF                   (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlval_HSH                   (0x013600B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlerr_OFF                   (28)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlerr_WID                   ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlerr_MSK                   (0x10000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlerr_MIN                   (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlerr_MAX                   (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlerr_DEF                   (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlerr_HSH                   (0x013800B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlen_OFF                    (29)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlen_WID                    ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlen_MSK                    (0x20000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlen_MIN                    (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlen_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlen_DEF                    (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extwlen_HSH                    (0x013A00B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeval_OFF               (30)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeval_WID               ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeval_MSK               (0x40000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeval_MIN               (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeval_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeval_DEF               (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeval_HSH               (0x013C00B0)

  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeen_OFF                (31)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeen_WID                ( 1)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeen_MSK                (0x80000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeen_MIN                (0)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeen_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeen_DEF                (0x00000000)
  #define DATA0CH0_CR_MDLLCTL0_mdll_cmn_extfreezeen_HSH                (0x013E00B0)

#define DATA0CH0_CR_MDLLCTL1_REG                                       (0x000000B4)

  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_picapsel_OFF                   ( 0)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_picapsel_WID                   ( 3)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_picapsel_MSK                   (0x00000007)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_picapsel_MIN                   (0)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_picapsel_MAX                   (7) // 0x00000007
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_picapsel_DEF                   (0x00000000)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_picapsel_HSH                   (0x030000B4)

  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_OFF              ( 3)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_WID              ( 5)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_MSK              (0x000000F8)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_MIN              (0)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_MAX              (31) // 0x0000001F
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_DEF              (0x00000007)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_HSH              (0x050600B4)

  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_openloopbias_OFF               ( 8)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_openloopbias_WID               ( 1)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_openloopbias_MSK               (0x00000100)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_openloopbias_MIN               (0)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_openloopbias_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_openloopbias_DEF               (0x00000000)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_openloopbias_HSH               (0x011000B4)

  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_OFF                ( 9)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_WID                ( 2)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_MSK                (0x00000600)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_MIN                (0)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_MAX                (3) // 0x00000003
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_DEF                (0x00000001)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_HSH                (0x021200B4)

  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_OFF                  (11)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_WID                  ( 6)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_MSK                  (0x0001F800)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_MIN                  (0)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_MAX                  (63) // 0x0000003F
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_DEF                  (0x00000020)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_HSH                  (0x061600B4)

  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldacforcen_OFF              (17)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldacforcen_WID              ( 1)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldacforcen_MSK              (0x00020000)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldacforcen_MIN              (0)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldacforcen_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldacforcen_DEF              (0x00000000)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldacforcen_HSH              (0x012200B4)

  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccode_OFF                (18)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccode_WID                ( 9)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccode_MSK                (0x07FC0000)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccode_MIN                (0)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccode_MAX                (511) // 0x000001FF
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccode_DEF                (0x00000187)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccode_HSH                (0x092400B4)

  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_OFF           (27)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_WID           ( 1)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_MSK           (0x08000000)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_MIN           (0)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_DEF           (0x00000000)
  #define DATA0CH0_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_HSH           (0x013600B4)

  #define DATA0CH0_CR_MDLLCTL1_parkval_ovr_sel_OFF                     (28)
  #define DATA0CH0_CR_MDLLCTL1_parkval_ovr_sel_WID                     ( 1)
  #define DATA0CH0_CR_MDLLCTL1_parkval_ovr_sel_MSK                     (0x10000000)
  #define DATA0CH0_CR_MDLLCTL1_parkval_ovr_sel_MIN                     (0)
  #define DATA0CH0_CR_MDLLCTL1_parkval_ovr_sel_MAX                     (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL1_parkval_ovr_sel_DEF                     (0x00000000)
  #define DATA0CH0_CR_MDLLCTL1_parkval_ovr_sel_HSH                     (0x013800B4)

  #define DATA0CH0_CR_MDLLCTL1_spare_OFF                               (29)
  #define DATA0CH0_CR_MDLLCTL1_spare_WID                               ( 3)
  #define DATA0CH0_CR_MDLLCTL1_spare_MSK                               (0xE0000000)
  #define DATA0CH0_CR_MDLLCTL1_spare_MIN                               (0)
  #define DATA0CH0_CR_MDLLCTL1_spare_MAX                               (7) // 0x00000007
  #define DATA0CH0_CR_MDLLCTL1_spare_DEF                               (0x00000000)
  #define DATA0CH0_CR_MDLLCTL1_spare_HSH                               (0x033A00B4)

#define DATA0CH0_CR_MDLLCTL2_REG                                       (0x000000B8)

  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_OFF              ( 0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_WID              ( 1)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_MSK              (0x00000001)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_MIN              (0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_DEF              (0x00000000)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_HSH              (0x010000B8)

  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dccen_OFF                      ( 1)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dccen_WID                      ( 1)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dccen_MSK                      (0x00000002)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dccen_MIN                      (0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dccen_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dccen_DEF                      (0x00000001)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dccen_HSH                      (0x010200B8)

  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dllldoen_OFF                   ( 2)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dllldoen_WID                   ( 1)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dllldoen_MSK                   (0x00000004)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dllldoen_MIN                   (0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dllldoen_MAX                   (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dllldoen_DEF                   (0x00000000)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_dllldoen_HSH                   (0x010400B8)

  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffreadsegen_OFF             ( 3)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffreadsegen_WID             ( 2)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffreadsegen_MSK             (0x00000018)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffreadsegen_MIN             (0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffreadsegen_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffreadsegen_DEF             (0x00000003)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffreadsegen_HSH             (0x020600B8)

  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_OFF          ( 5)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_WID          ( 5)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_MSK          (0x000003E0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_MIN          (0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_MAX          (31) // 0x0000001F
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_DEF          (0x00000010)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_HSH          (0x050A00B8)

  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vcdldcccode_OFF                (10)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vcdldcccode_WID                ( 4)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vcdldcccode_MSK                (0x00003C00)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vcdldcccode_MIN                (0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vcdldcccode_MAX                (15) // 0x0000000F
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vcdldcccode_DEF                (0x00000008)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_vcdldcccode_HSH                (0x041400B8)

  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_OFF              (14)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_WID              ( 8)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_MSK              (0x003FC000)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_MIN              (0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_MAX              (255) // 0x000000FF
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_DEF              (0x00000080)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_HSH              (0x081C00B8)

  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_OFF                (22)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_WID                ( 6)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_MSK                (0x0FC00000)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_MIN                (0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_DEF                (0x00000020)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_HSH                (0x062C00B8)

  #define DATA0CH0_CR_MDLLCTL2_spare_OFF                               (28)
  #define DATA0CH0_CR_MDLLCTL2_spare_WID                               ( 3)
  #define DATA0CH0_CR_MDLLCTL2_spare_MSK                               (0x70000000)
  #define DATA0CH0_CR_MDLLCTL2_spare_MIN                               (0)
  #define DATA0CH0_CR_MDLLCTL2_spare_MAX                               (7) // 0x00000007
  #define DATA0CH0_CR_MDLLCTL2_spare_DEF                               (0x00000000)
  #define DATA0CH0_CR_MDLLCTL2_spare_HSH                               (0x033800B8)

  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_OFF                (31)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_WID                ( 1)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_MSK                (0x80000000)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_MIN                (0)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_DEF                (0x00000000)
  #define DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_HSH                (0x013E00B8)

#define DATA0CH0_CR_MDLLCTL3_REG                                       (0x000000BC)

  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_OFF               ( 0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_WID               ( 3)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_MSK               (0x00000007)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_MIN               (0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_MAX               (7) // 0x00000007
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_DEF               (0x00000004)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_HSH               (0x030000BC)

  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_OFF        ( 3)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_WID        ( 1)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_MSK        (0x00000008)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_MIN        (0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_DEF        (0x00000000)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_HSH        (0x010600BC)

  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoforceen_OFF                 ( 4)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoforceen_WID                 ( 1)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoforceen_MSK                 (0x00000010)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoforceen_MIN                 (0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoforceen_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoforceen_DEF                 (0x00000000)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoforceen_HSH                 (0x010800BC)

  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_OFF           ( 5)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_WID           ( 5)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_MSK           (0x000003E0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_MIN           (0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_MAX           (31) // 0x0000001F
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_DEF           (0x00000005)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_HSH           (0x050A00BC)

  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_OFF               (10)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_WID               ( 4)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_MSK               (0x00003C00)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_MIN               (0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_MAX               (15) // 0x0000000F
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_DEF               (0x00000008)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_HSH               (0x041400BC)

  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_rloadcomp_OFF                  (14)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_rloadcomp_WID                  ( 6)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_rloadcomp_MSK                  (0x000FC000)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_rloadcomp_MIN                  (0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_rloadcomp_MAX                  (63) // 0x0000003F
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_rloadcomp_DEF                  (0x0000000E)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_rloadcomp_HSH                  (0x061C00BC)

  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_OFF          (20)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_WID          ( 1)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_MSK          (0x00100000)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_MIN          (0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_MAX          (1) // 0x00000001
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_DEF          (0x00000000)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_HSH          (0x012800BC)

  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoffcoderead_OFF              (21)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoffcoderead_WID              ( 7)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoffcoderead_MSK              (0x0FE00000)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoffcoderead_MIN              (0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoffcoderead_MAX              (127) // 0x0000007F
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoffcoderead_DEF              (0x00000040)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_ldoffcoderead_HSH              (0x072A00BC)

  #define DATA0CH0_CR_MDLLCTL3_rxsdl_cmn_rxvcdlcben_OFF                (28)
  #define DATA0CH0_CR_MDLLCTL3_rxsdl_cmn_rxvcdlcben_WID                ( 2)
  #define DATA0CH0_CR_MDLLCTL3_rxsdl_cmn_rxvcdlcben_MSK                (0x30000000)
  #define DATA0CH0_CR_MDLLCTL3_rxsdl_cmn_rxvcdlcben_MIN                (0)
  #define DATA0CH0_CR_MDLLCTL3_rxsdl_cmn_rxvcdlcben_MAX                (3) // 0x00000003
  #define DATA0CH0_CR_MDLLCTL3_rxsdl_cmn_rxvcdlcben_DEF                (0x00000000)
  #define DATA0CH0_CR_MDLLCTL3_rxsdl_cmn_rxvcdlcben_HSH                (0x023800BC)

  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vcdlcben_OFF                   (30)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vcdlcben_WID                   ( 2)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vcdlcben_MSK                   (0xC0000000)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vcdlcben_MIN                   (0)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vcdlcben_MAX                   (3) // 0x00000003
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vcdlcben_DEF                   (0x00000000)
  #define DATA0CH0_CR_MDLLCTL3_mdll_cmn_vcdlcben_HSH                   (0x023C00BC)

#define DATA0CH0_CR_MDLLCTL4_REG                                       (0x000000C0)

  #define DATA0CH0_CR_MDLLCTL4_spare_OFF                               ( 0)
  #define DATA0CH0_CR_MDLLCTL4_spare_WID                               (32)
  #define DATA0CH0_CR_MDLLCTL4_spare_MSK                               (0xFFFFFFFF)
  #define DATA0CH0_CR_MDLLCTL4_spare_MIN                               (0)
  #define DATA0CH0_CR_MDLLCTL4_spare_MAX                               (4294967295) // 0xFFFFFFFF
  #define DATA0CH0_CR_MDLLCTL4_spare_DEF                               (0x00000000)
  #define DATA0CH0_CR_MDLLCTL4_spare_HSH                               (0x200000C0)

#define DATA0CH0_CR_MDLLCTL5_REG                                       (0x000000C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA0CH0_CR_DIGMISCCTRL_REG                                    (0x000000C4)

  #define DATA0CH0_CR_DIGMISCCTRL_dqpirk2rkupdate_delay_OFF            ( 0)
  #define DATA0CH0_CR_DIGMISCCTRL_dqpirk2rkupdate_delay_WID            ( 1)
  #define DATA0CH0_CR_DIGMISCCTRL_dqpirk2rkupdate_delay_MSK            (0x00000001)
  #define DATA0CH0_CR_DIGMISCCTRL_dqpirk2rkupdate_delay_MIN            (0)
  #define DATA0CH0_CR_DIGMISCCTRL_dqpirk2rkupdate_delay_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_DIGMISCCTRL_dqpirk2rkupdate_delay_DEF            (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_dqpirk2rkupdate_delay_HSH            (0x010000C4)

  #define DATA0CH0_CR_DIGMISCCTRL_rxrankmuxdelay_rcvenpicode_OFF       ( 1)
  #define DATA0CH0_CR_DIGMISCCTRL_rxrankmuxdelay_rcvenpicode_WID       ( 2)
  #define DATA0CH0_CR_DIGMISCCTRL_rxrankmuxdelay_rcvenpicode_MSK       (0x00000006)
  #define DATA0CH0_CR_DIGMISCCTRL_rxrankmuxdelay_rcvenpicode_MIN       (0)
  #define DATA0CH0_CR_DIGMISCCTRL_rxrankmuxdelay_rcvenpicode_MAX       (3) // 0x00000003
  #define DATA0CH0_CR_DIGMISCCTRL_rxrankmuxdelay_rcvenpicode_DEF       (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_rxrankmuxdelay_rcvenpicode_HSH       (0x020200C4)

  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqpicode_OFF          ( 3)
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqpicode_WID          ( 2)
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqpicode_MSK          (0x00000018)
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqpicode_MIN          (0)
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqpicode_MAX          (3) // 0x00000003
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqpicode_DEF          (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqpicode_HSH          (0x020600C4)

  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqspicode_OFF         ( 5)
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqspicode_WID         ( 2)
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqspicode_MSK         (0x00000060)
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqspicode_MIN         (0)
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqspicode_MAX         (3) // 0x00000003
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqspicode_DEF         (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_txrankmuxdelay_dqspicode_HSH         (0x020A00C4)

  #define DATA0CH0_CR_DIGMISCCTRL_cfg_rxdqsdelaysaturated_en_OFF       ( 7)
  #define DATA0CH0_CR_DIGMISCCTRL_cfg_rxdqsdelaysaturated_en_WID       ( 1)
  #define DATA0CH0_CR_DIGMISCCTRL_cfg_rxdqsdelaysaturated_en_MSK       (0x00000080)
  #define DATA0CH0_CR_DIGMISCCTRL_cfg_rxdqsdelaysaturated_en_MIN       (0)
  #define DATA0CH0_CR_DIGMISCCTRL_cfg_rxdqsdelaysaturated_en_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_DIGMISCCTRL_cfg_rxdqsdelaysaturated_en_DEF       (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_cfg_rxdqsdelaysaturated_en_HSH       (0x010E00C4)

  #define DATA0CH0_CR_DIGMISCCTRL_RxDqsnDelay_rollover_flag_OFF        ( 8)
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqsnDelay_rollover_flag_WID        ( 1)
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqsnDelay_rollover_flag_MSK        (0x00000100)
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqsnDelay_rollover_flag_MIN        (0)
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqsnDelay_rollover_flag_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqsnDelay_rollover_flag_DEF        (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqsnDelay_rollover_flag_HSH        (0x011000C4)

  #define DATA0CH0_CR_DIGMISCCTRL_RxDqspDelay_rollover_flag_OFF        ( 9)
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqspDelay_rollover_flag_WID        ( 1)
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqspDelay_rollover_flag_MSK        (0x00000200)
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqspDelay_rollover_flag_MIN        (0)
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqspDelay_rollover_flag_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqspDelay_rollover_flag_DEF        (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_RxDqspDelay_rollover_flag_HSH        (0x011200C4)

  #define DATA0CH0_CR_DIGMISCCTRL_dqspirk2rkupdate_delay_OFF           (10)
  #define DATA0CH0_CR_DIGMISCCTRL_dqspirk2rkupdate_delay_WID           ( 1)
  #define DATA0CH0_CR_DIGMISCCTRL_dqspirk2rkupdate_delay_MSK           (0x00000400)
  #define DATA0CH0_CR_DIGMISCCTRL_dqspirk2rkupdate_delay_MIN           (0)
  #define DATA0CH0_CR_DIGMISCCTRL_dqspirk2rkupdate_delay_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DIGMISCCTRL_dqspirk2rkupdate_delay_DEF           (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_dqspirk2rkupdate_delay_HSH           (0x011400C4)

  #define DATA0CH0_CR_DIGMISCCTRL_rcvenpirk2rkupdate_delay_OFF         (11)
  #define DATA0CH0_CR_DIGMISCCTRL_rcvenpirk2rkupdate_delay_WID         ( 1)
  #define DATA0CH0_CR_DIGMISCCTRL_rcvenpirk2rkupdate_delay_MSK         (0x00000800)
  #define DATA0CH0_CR_DIGMISCCTRL_rcvenpirk2rkupdate_delay_MIN         (0)
  #define DATA0CH0_CR_DIGMISCCTRL_rcvenpirk2rkupdate_delay_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_DIGMISCCTRL_rcvenpirk2rkupdate_delay_DEF         (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_rcvenpirk2rkupdate_delay_HSH         (0x011600C4)

  #define DATA0CH0_CR_DIGMISCCTRL_spare_OFF                            (12)
  #define DATA0CH0_CR_DIGMISCCTRL_spare_WID                            ( 2)
  #define DATA0CH0_CR_DIGMISCCTRL_spare_MSK                            (0x00003000)
  #define DATA0CH0_CR_DIGMISCCTRL_spare_MIN                            (0)
  #define DATA0CH0_CR_DIGMISCCTRL_spare_MAX                            (3) // 0x00000003
  #define DATA0CH0_CR_DIGMISCCTRL_spare_DEF                            (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_spare_HSH                            (0x021800C4)

  #define DATA0CH0_CR_DIGMISCCTRL_Reserved5_OFF                        (14)
  #define DATA0CH0_CR_DIGMISCCTRL_Reserved5_WID                        (18)
  #define DATA0CH0_CR_DIGMISCCTRL_Reserved5_MSK                        (0xFFFFC000)
  #define DATA0CH0_CR_DIGMISCCTRL_Reserved5_MIN                        (0)
  #define DATA0CH0_CR_DIGMISCCTRL_Reserved5_MAX                        (262143) // 0x0003FFFF
  #define DATA0CH0_CR_DIGMISCCTRL_Reserved5_DEF                        (0x00000000)
  #define DATA0CH0_CR_DIGMISCCTRL_Reserved5_HSH                        (0x121C00C4)

#define DATA0CH0_CR_AFEMISCCTRL2_REG                                   (0x000000C8)

  #define DATA0CH0_CR_AFEMISCCTRL2_spare_OFF                           ( 0)
  #define DATA0CH0_CR_AFEMISCCTRL2_spare_WID                           (21)
  #define DATA0CH0_CR_AFEMISCCTRL2_spare_MSK                           (0x001FFFFF)
  #define DATA0CH0_CR_AFEMISCCTRL2_spare_MIN                           (0)
  #define DATA0CH0_CR_AFEMISCCTRL2_spare_MAX                           (2097151) // 0x001FFFFF
  #define DATA0CH0_CR_AFEMISCCTRL2_spare_DEF                           (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL2_spare_HSH                           (0x150000C8)

  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakeren_OFF        (21)
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakeren_WID        ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakeren_MSK        (0x00200000)
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakeren_MIN        (0)
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakeren_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakeren_DEF        (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakeren_HSH        (0x012A00C8)

  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakercomp_OFF      (22)
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakercomp_WID      ( 6)
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakercomp_MSK      (0x0FC00000)
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakercomp_MIN      (0)
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakercomp_MAX      (63) // 0x0000003F
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakercomp_DEF      (0x00000020)
  #define DATA0CH0_CR_AFEMISCCTRL2_dxtx_cmn_txvsshileakercomp_HSH      (0x062C00C8)

  #define DATA0CH0_CR_AFEMISCCTRL2_dqrx_cmn_dqphsdrvprocsel_OFF        (28)
  #define DATA0CH0_CR_AFEMISCCTRL2_dqrx_cmn_dqphsdrvprocsel_WID        ( 2)
  #define DATA0CH0_CR_AFEMISCCTRL2_dqrx_cmn_dqphsdrvprocsel_MSK        (0x30000000)
  #define DATA0CH0_CR_AFEMISCCTRL2_dqrx_cmn_dqphsdrvprocsel_MIN        (0)
  #define DATA0CH0_CR_AFEMISCCTRL2_dqrx_cmn_dqphsdrvprocsel_MAX        (3) // 0x00000003
  #define DATA0CH0_CR_AFEMISCCTRL2_dqrx_cmn_dqphsdrvprocsel_DEF        (0x00000003)
  #define DATA0CH0_CR_AFEMISCCTRL2_dqrx_cmn_dqphsdrvprocsel_HSH        (0x023800C8)

  #define DATA0CH0_CR_AFEMISCCTRL2_dcdsrz_cmn_dcdsrzsampclksel_OFF     (30)
  #define DATA0CH0_CR_AFEMISCCTRL2_dcdsrz_cmn_dcdsrzsampclksel_WID     ( 2)
  #define DATA0CH0_CR_AFEMISCCTRL2_dcdsrz_cmn_dcdsrzsampclksel_MSK     (0xC0000000)
  #define DATA0CH0_CR_AFEMISCCTRL2_dcdsrz_cmn_dcdsrzsampclksel_MIN     (0)
  #define DATA0CH0_CR_AFEMISCCTRL2_dcdsrz_cmn_dcdsrzsampclksel_MAX     (3) // 0x00000003
  #define DATA0CH0_CR_AFEMISCCTRL2_dcdsrz_cmn_dcdsrzsampclksel_DEF     (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL2_dcdsrz_cmn_dcdsrzsampclksel_HSH     (0x023C00C8)

#define DATA0CH0_CR_TXPBDOFFSET0_REG                                   (0x000000CC)

  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbd_ph90incr_OFF              ( 0)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbd_ph90incr_WID              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbd_ph90incr_MSK              (0x00000001)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbd_ph90incr_MIN              (0)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbd_ph90incr_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbd_ph90incr_DEF              (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbd_ph90incr_HSH              (0x010000CC)

  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbd_ph90incr_OFF              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbd_ph90incr_WID              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbd_ph90incr_MSK              (0x00000002)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbd_ph90incr_MIN              (0)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbd_ph90incr_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbd_ph90incr_DEF              (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbd_ph90incr_HSH              (0x010200CC)

  #define DATA0CH0_CR_TXPBDOFFSET0_dq4_txpbddoffset_OFF                ( 2)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq4_txpbddoffset_WID                ( 6)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq4_txpbddoffset_MSK                (0x000000FC)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq4_txpbddoffset_MIN                (0)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq4_txpbddoffset_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_TXPBDOFFSET0_dq4_txpbddoffset_DEF                (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq4_txpbddoffset_HSH                (0x060400CC)

  #define DATA0CH0_CR_TXPBDOFFSET0_dq3_txpbddoffset_OFF                ( 8)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq3_txpbddoffset_WID                ( 6)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq3_txpbddoffset_MSK                (0x00003F00)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq3_txpbddoffset_MIN                (0)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq3_txpbddoffset_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_TXPBDOFFSET0_dq3_txpbddoffset_DEF                (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq3_txpbddoffset_HSH                (0x061000CC)

  #define DATA0CH0_CR_TXPBDOFFSET0_dq2_txpbddoffset_OFF                (14)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq2_txpbddoffset_WID                ( 6)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq2_txpbddoffset_MSK                (0x000FC000)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq2_txpbddoffset_MIN                (0)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq2_txpbddoffset_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_TXPBDOFFSET0_dq2_txpbddoffset_DEF                (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq2_txpbddoffset_HSH                (0x061C00CC)

  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbddoffset_OFF                (20)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbddoffset_WID                ( 6)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbddoffset_MSK                (0x03F00000)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbddoffset_MIN                (0)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbddoffset_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbddoffset_DEF                (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbddoffset_HSH                (0x062800CC)

  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbddoffset_OFF                (26)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbddoffset_WID                ( 6)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbddoffset_MSK                (0xFC000000)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbddoffset_MIN                (0)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbddoffset_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbddoffset_DEF                (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbddoffset_HSH                (0x063400CC)

#define DATA0CH0_CR_TXPBDOFFSET1_REG                                   (0x000000D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_Reserved6_OFF                       ( 0)
  #define DATA0CH0_CR_TXPBDOFFSET1_Reserved6_WID                       ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET1_Reserved6_MSK                       (0x00000001)
  #define DATA0CH0_CR_TXPBDOFFSET1_Reserved6_MIN                       (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_Reserved6_MAX                       (1) // 0x00000001
  #define DATA0CH0_CR_TXPBDOFFSET1_Reserved6_DEF                       (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_Reserved6_HSH                       (0x010000D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbd_ph90incr_OFF              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbd_ph90incr_WID              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbd_ph90incr_MSK              (0x00000002)
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbd_ph90incr_MIN              (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbd_ph90incr_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbd_ph90incr_DEF              (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbd_ph90incr_HSH              (0x010200D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbd_ph90incr_OFF              ( 2)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbd_ph90incr_WID              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbd_ph90incr_MSK              (0x00000004)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbd_ph90incr_MIN              (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbd_ph90incr_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbd_ph90incr_DEF              (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbd_ph90incr_HSH              (0x010400D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbd_ph90incr_OFF              ( 3)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbd_ph90incr_WID              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbd_ph90incr_MSK              (0x00000008)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbd_ph90incr_MIN              (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbd_ph90incr_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbd_ph90incr_DEF              (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbd_ph90incr_HSH              (0x010600D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbd_ph90incr_OFF              ( 4)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbd_ph90incr_WID              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbd_ph90incr_MSK              (0x00000010)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbd_ph90incr_MIN              (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbd_ph90incr_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbd_ph90incr_DEF              (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbd_ph90incr_HSH              (0x010800D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dq4_txpbd_ph90incr_OFF              ( 5)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq4_txpbd_ph90incr_WID              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq4_txpbd_ph90incr_MSK              (0x00000020)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq4_txpbd_ph90incr_MIN              (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq4_txpbd_ph90incr_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_TXPBDOFFSET1_dq4_txpbd_ph90incr_DEF              (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq4_txpbd_ph90incr_HSH              (0x010A00D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dq3_txpbd_ph90incr_OFF              ( 6)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq3_txpbd_ph90incr_WID              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq3_txpbd_ph90incr_MSK              (0x00000040)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq3_txpbd_ph90incr_MIN              (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq3_txpbd_ph90incr_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_TXPBDOFFSET1_dq3_txpbd_ph90incr_DEF              (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq3_txpbd_ph90incr_HSH              (0x010C00D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dq2_txpbd_ph90incr_OFF              ( 7)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq2_txpbd_ph90incr_WID              ( 1)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq2_txpbd_ph90incr_MSK              (0x00000080)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq2_txpbd_ph90incr_MIN              (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq2_txpbd_ph90incr_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_TXPBDOFFSET1_dq2_txpbd_ph90incr_DEF              (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq2_txpbd_ph90incr_HSH              (0x010E00D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbddo_OFF                     ( 8)
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbddo_WID                     ( 6)
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbddo_MSK                     (0x00003F00)
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbddo_MIN                     (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbddo_MAX                     (63) // 0x0000003F
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbddo_DEF                     (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbddo_HSH                     (0x061000D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbddoffset_OFF                (14)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbddoffset_WID                ( 6)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbddoffset_MSK                (0x000FC000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbddoffset_MIN                (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbddoffset_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbddoffset_DEF                (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbddoffset_HSH                (0x061C00D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbddoffset_OFF                (20)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbddoffset_WID                ( 6)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbddoffset_MSK                (0x03F00000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbddoffset_MIN                (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbddoffset_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbddoffset_DEF                (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbddoffset_HSH                (0x062800D0)

  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbddoffset_OFF                (26)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbddoffset_WID                ( 6)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbddoffset_MSK                (0xFC000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbddoffset_MIN                (0)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbddoffset_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbddoffset_DEF                (0x00000000)
  #define DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbddoffset_HSH                (0x063400D0)

#define DATA0CH0_CR_COMPCTL0_REG                                       (0x000000D4)

  #define DATA0CH0_CR_COMPCTL0_dxtx_cmn_ennbiasboost_OFF               ( 0)
  #define DATA0CH0_CR_COMPCTL0_dxtx_cmn_ennbiasboost_WID               ( 1)
  #define DATA0CH0_CR_COMPCTL0_dxtx_cmn_ennbiasboost_MSK               (0x00000001)
  #define DATA0CH0_CR_COMPCTL0_dxtx_cmn_ennbiasboost_MIN               (0)
  #define DATA0CH0_CR_COMPCTL0_dxtx_cmn_ennbiasboost_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_COMPCTL0_dxtx_cmn_ennbiasboost_DEF               (0x00000000)
  #define DATA0CH0_CR_COMPCTL0_dxtx_cmn_ennbiasboost_HSH               (0x010000D4)

  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtupdq_OFF             ( 1)
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtupdq_WID             ( 6)
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtupdq_MSK             (0x0000007E)
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtupdq_MIN             (0)
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtupdq_MAX             (63) // 0x0000003F
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtupdq_DEF             (0x00000020)
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtupdq_HSH             (0x060200D4)

  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtdndq_OFF             ( 7)
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtdndq_WID             ( 6)
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtdndq_MSK             (0x00001F80)
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtdndq_MIN             (0)
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtdndq_MAX             (63) // 0x0000003F
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtdndq_DEF             (0x00000020)
  #define DATA0CH0_CR_COMPCTL0_dqtx_cmn_txrcompodtdndq_HSH             (0x060E00D4)

  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtupdqs_OFF           (13)
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtupdqs_WID           ( 6)
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtupdqs_MSK           (0x0007E000)
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtupdqs_MIN           (0)
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtupdqs_MAX           (63) // 0x0000003F
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtupdqs_DEF           (0x00000020)
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtupdqs_HSH           (0x061A00D4)

  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtdndqs_OFF           (19)
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtdndqs_WID           ( 6)
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtdndqs_MSK           (0x01F80000)
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtdndqs_MIN           (0)
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtdndqs_MAX           (63) // 0x0000003F
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtdndqs_DEF           (0x00000020)
  #define DATA0CH0_CR_COMPCTL0_dqstx_cmn_txrcompodtdndqs_HSH           (0x062600D4)

  #define DATA0CH0_CR_COMPCTL0_Reserved7_OFF                           (25)
  #define DATA0CH0_CR_COMPCTL0_Reserved7_WID                           ( 7)
  #define DATA0CH0_CR_COMPCTL0_Reserved7_MSK                           (0xFE000000)
  #define DATA0CH0_CR_COMPCTL0_Reserved7_MIN                           (0)
  #define DATA0CH0_CR_COMPCTL0_Reserved7_MAX                           (127) // 0x0000007F
  #define DATA0CH0_CR_COMPCTL0_Reserved7_DEF                           (0x00000000)
  #define DATA0CH0_CR_COMPCTL0_Reserved7_HSH                           (0x073200D4)

#define DATA0CH0_CR_COMPCTL1_REG                                       (0x000000D8)

  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsp_OFF             ( 0)
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsp_WID             ( 8)
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsp_MSK             (0x000000FF)
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsp_MIN             (0)
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsp_MAX             (255) // 0x000000FF
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsp_DEF             (0x00000010)
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsp_HSH             (0x080000D8)

  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsn_OFF             ( 8)
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsn_WID             ( 8)
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsn_MSK             (0x0000FF00)
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsn_MIN             (0)
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsn_MAX             (255) // 0x000000FF
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsn_DEF             (0x00000010)
  #define DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsn_HSH             (0x081000D8)

  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvup_OFF               (16)
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvup_WID               ( 6)
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvup_MSK               (0x003F0000)
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvup_MIN               (0)
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvup_MAX               (63) // 0x0000003F
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvup_DEF               (0x00000020)
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvup_HSH               (0x062000D8)

  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvdn_OFF               (22)
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvdn_WID               ( 6)
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvdn_MSK               (0x0FC00000)
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvdn_MIN               (0)
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvdn_MAX               (63) // 0x0000003F
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvdn_DEF               (0x00000020)
  #define DATA0CH0_CR_COMPCTL1_dxtx_cmn_txrcompdrvdn_HSH               (0x062C00D8)

  #define DATA0CH0_CR_COMPCTL1_Reserved8_OFF                           (28)
  #define DATA0CH0_CR_COMPCTL1_Reserved8_WID                           ( 4)
  #define DATA0CH0_CR_COMPCTL1_Reserved8_MSK                           (0xF0000000)
  #define DATA0CH0_CR_COMPCTL1_Reserved8_MIN                           (0)
  #define DATA0CH0_CR_COMPCTL1_Reserved8_MAX                           (15) // 0x0000000F
  #define DATA0CH0_CR_COMPCTL1_Reserved8_DEF                           (0x00000000)
  #define DATA0CH0_CR_COMPCTL1_Reserved8_HSH                           (0x043800D8)

#define DATA0CH0_CR_COMPCTL2_REG                                       (0x000000DC)

  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffstrobe_OFF            ( 0)
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffstrobe_WID            ( 6)
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffstrobe_MSK            (0x0000003F)
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffstrobe_MIN            (0)
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffstrobe_MAX            (63) // 0x0000003F
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffstrobe_DEF            (0x00000020)
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffstrobe_HSH            (0x060000DC)

  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffdata_OFF              ( 6)
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffdata_WID              ( 6)
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffdata_MSK              (0x00000FC0)
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffdata_MIN              (0)
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffdata_MAX              (63) // 0x0000003F
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffdata_DEF              (0x00000020)
  #define DATA0CH0_CR_COMPCTL2_dqrx_cmn_rxvsshiffdata_HSH              (0x060C00DC)

  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txvsshiff_OFF                  (12)
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txvsshiff_WID                  ( 6)
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txvsshiff_MSK                  (0x0003F000)
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txvsshiff_MIN                  (0)
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txvsshiff_MAX                  (63) // 0x0000003F
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txvsshiff_DEF                  (0x00000020)
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txvsshiff_HSH                  (0x061800DC)

  #define DATA0CH0_CR_COMPCTL2_rxbias_cmn_rloadcomp_OFF                (18)
  #define DATA0CH0_CR_COMPCTL2_rxbias_cmn_rloadcomp_WID                ( 6)
  #define DATA0CH0_CR_COMPCTL2_rxbias_cmn_rloadcomp_MSK                (0x00FC0000)
  #define DATA0CH0_CR_COMPCTL2_rxbias_cmn_rloadcomp_MIN                (0)
  #define DATA0CH0_CR_COMPCTL2_rxbias_cmn_rloadcomp_MAX                (63) // 0x0000003F
  #define DATA0CH0_CR_COMPCTL2_rxbias_cmn_rloadcomp_DEF                (0x0000000E)
  #define DATA0CH0_CR_COMPCTL2_rxbias_cmn_rloadcomp_HSH                (0x062400DC)

  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txtcocomp_OFF                  (24)
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txtcocomp_WID                  ( 8)
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txtcocomp_MSK                  (0xFF000000)
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txtcocomp_MIN                  (0)
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txtcocomp_MAX                  (255) // 0x000000FF
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txtcocomp_DEF                  (0x00000020)
  #define DATA0CH0_CR_COMPCTL2_dxtx_cmn_txtcocomp_HSH                  (0x083000DC)

#define DATA0CH0_CR_DCCCTL5_REG                                        (0x000000E0)

  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_OFF                    ( 0)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_WID                    ( 8)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_MSK                    (0x000000FF)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_DEF                    (0x00000080)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_HSH                    (0x080000E0)

  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index1_OFF                    ( 8)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index1_WID                    ( 8)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index1_MSK                    (0x0000FF00)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index1_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index1_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index1_DEF                    (0x00000080)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index1_HSH                    (0x081000E0)

  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index2_OFF                    (16)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index2_WID                    ( 8)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index2_MSK                    (0x00FF0000)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index2_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index2_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index2_DEF                    (0x00000080)
  #define DATA0CH0_CR_DCCCTL5_dcccodeph0_index2_HSH                    (0x082000E0)

  #define DATA0CH0_CR_DCCCTL5_initialdcccode_OFF                       (24)
  #define DATA0CH0_CR_DCCCTL5_initialdcccode_WID                       ( 8)
  #define DATA0CH0_CR_DCCCTL5_initialdcccode_MSK                       (0xFF000000)
  #define DATA0CH0_CR_DCCCTL5_initialdcccode_MIN                       (0)
  #define DATA0CH0_CR_DCCCTL5_initialdcccode_MAX                       (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL5_initialdcccode_DEF                       (0x00000080)
  #define DATA0CH0_CR_DCCCTL5_initialdcccode_HSH                       (0x083000E0)

#define DATA0CH0_CR_DCCCTL6_REG                                        (0x000000E4)

  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index3_OFF                    ( 0)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index3_WID                    ( 8)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index3_MSK                    (0x000000FF)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index3_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index3_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index3_DEF                    (0x00000080)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index3_HSH                    (0x080000E4)

  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index4_OFF                    ( 8)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index4_WID                    ( 8)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index4_MSK                    (0x0000FF00)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index4_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index4_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index4_DEF                    (0x00000080)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index4_HSH                    (0x081000E4)

  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index5_OFF                    (16)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index5_WID                    ( 8)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index5_MSK                    (0x00FF0000)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index5_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index5_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index5_DEF                    (0x00000080)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index5_HSH                    (0x082000E4)

  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index6_OFF                    (24)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index6_WID                    ( 8)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index6_MSK                    (0xFF000000)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index6_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index6_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index6_DEF                    (0x00000080)
  #define DATA0CH0_CR_DCCCTL6_dcccodeph0_index6_HSH                    (0x083000E4)

#define DATA0CH0_CR_DCCCTL7_REG                                        (0x000000E8)

  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index7_OFF                    ( 0)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index7_WID                    ( 8)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index7_MSK                    (0x000000FF)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index7_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index7_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index7_DEF                    (0x00000080)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index7_HSH                    (0x080000E8)

  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index8_OFF                    ( 8)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index8_WID                    ( 8)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index8_MSK                    (0x0000FF00)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index8_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index8_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index8_DEF                    (0x00000080)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index8_HSH                    (0x081000E8)

  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index9_OFF                    (16)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index9_WID                    ( 8)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index9_MSK                    (0x00FF0000)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index9_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index9_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index9_DEF                    (0x00000080)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph0_index9_HSH                    (0x082000E8)

  #define DATA0CH0_CR_DCCCTL7_dcccodeph90_index1_OFF                   (24)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph90_index1_WID                   ( 8)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph90_index1_MSK                   (0xFF000000)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph90_index1_MIN                   (0)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph90_index1_MAX                   (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL7_dcccodeph90_index1_DEF                   (0x00000080)
  #define DATA0CH0_CR_DCCCTL7_dcccodeph90_index1_HSH                   (0x083000E8)

#define DATA0CH0_CR_DCCCTL8_REG                                        (0x000000EC)

  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index2_OFF                   ( 0)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index2_WID                   ( 8)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index2_MSK                   (0x000000FF)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index2_MIN                   (0)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index2_MAX                   (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index2_DEF                   (0x00000080)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index2_HSH                   (0x080000EC)

  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index3_OFF                   ( 8)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index3_WID                   ( 8)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index3_MSK                   (0x0000FF00)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index3_MIN                   (0)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index3_MAX                   (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index3_DEF                   (0x00000080)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index3_HSH                   (0x081000EC)

  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index4_OFF                   (16)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index4_WID                   ( 8)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index4_MSK                   (0x00FF0000)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index4_MIN                   (0)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index4_MAX                   (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index4_DEF                   (0x00000080)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index4_HSH                   (0x082000EC)

  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index5_OFF                   (24)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index5_WID                   ( 8)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index5_MSK                   (0xFF000000)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index5_MIN                   (0)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index5_MAX                   (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index5_DEF                   (0x00000080)
  #define DATA0CH0_CR_DCCCTL8_dcccodeph90_index5_HSH                   (0x083000EC)

#define DATA0CH0_CR_DCCCTL9_REG                                        (0x000000F0)

  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index6_OFF                   ( 0)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index6_WID                   ( 8)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index6_MSK                   (0x000000FF)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index6_MIN                   (0)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index6_MAX                   (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index6_DEF                   (0x00000080)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index6_HSH                   (0x080000F0)

  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index7_OFF                   ( 8)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index7_WID                   ( 8)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index7_MSK                   (0x0000FF00)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index7_MIN                   (0)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index7_MAX                   (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index7_DEF                   (0x00000080)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index7_HSH                   (0x081000F0)

  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index8_OFF                   (16)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index8_WID                   ( 8)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index8_MSK                   (0x00FF0000)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index8_MIN                   (0)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index8_MAX                   (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index8_DEF                   (0x00000080)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index8_HSH                   (0x082000F0)

  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index9_OFF                   (24)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index9_WID                   ( 8)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index9_MSK                   (0xFF000000)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index9_MIN                   (0)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index9_MAX                   (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index9_DEF                   (0x00000080)
  #define DATA0CH0_CR_DCCCTL9_dcccodeph90_index9_HSH                   (0x083000F0)

#define DATA0CH0_CR_RXCONTROL0RANK0_REG                                (0x000000F4)

  #define DATA0CH0_CR_RXCONTROL0RANK0_rxrcvenpi_OFF                    ( 0)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxrcvenpi_WID                    (12)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxrcvenpi_MSK                    (0x00000FFF)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxrcvenpi_MIN                    (0)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxrcvenpi_MAX                    (4095) // 0x00000FFF
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxrcvenpi_DEF                    (0x00000FFF)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxrcvenpi_HSH                    (0x0C0000F4)

  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsppicode_OFF              (12)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsppicode_WID              ( 7)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsppicode_MSK              (0x0007F000)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsppicode_MIN              (0)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsppicode_MAX              (127) // 0x0000007F
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsppicode_DEF              (0x00000020)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsppicode_HSH              (0x071800F4)

  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsnpicode_OFF              (19)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsnpicode_WID              ( 7)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsnpicode_MSK              (0x03F80000)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsnpicode_MIN              (0)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsnpicode_MAX              (127) // 0x0000007F
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsnpicode_DEF              (0x00000020)
  #define DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsnpicode_HSH              (0x072600F4)

  #define DATA0CH0_CR_RXCONTROL0RANK0_spare_OFF                        (26)
  #define DATA0CH0_CR_RXCONTROL0RANK0_spare_WID                        ( 6)
  #define DATA0CH0_CR_RXCONTROL0RANK0_spare_MSK                        (0xFC000000)
  #define DATA0CH0_CR_RXCONTROL0RANK0_spare_MIN                        (0)
  #define DATA0CH0_CR_RXCONTROL0RANK0_spare_MAX                        (63) // 0x0000003F
  #define DATA0CH0_CR_RXCONTROL0RANK0_spare_DEF                        (0x00000000)
  #define DATA0CH0_CR_RXCONTROL0RANK0_spare_HSH                        (0x063400F4)

#define DATA0CH0_CR_RXCONTROL0RANK1_REG                                (0x000000F8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA0CH0_CR_RXCONTROL0RANK2_REG                                (0x000000FC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA0CH0_CR_RXCONTROL0RANK3_REG                                (0x00000100)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA0CH0_CR_TXCONTROL0_PH90_REG                                (0x00000104)

  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk1_OFF                ( 0)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk1_WID                ( 8)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk1_MSK                (0x000000FF)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk1_MIN                (0)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk1_MAX                (255) // 0x000000FF
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk1_DEF                (0x00000000)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk1_HSH                (0x08000104)

  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk1_OFF               ( 8)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk1_WID               ( 8)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk1_MSK               (0x0000FF00)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk1_MIN               (0)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk1_MAX               (255) // 0x000000FF
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk1_DEF               (0x00000000)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk1_HSH               (0x08100104)

  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_OFF                (16)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_WID                ( 8)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_MSK                (0x00FF0000)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_MIN                (0)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_MAX                (255) // 0x000000FF
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_DEF                (0x00000000)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_HSH                (0x08200104)

  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk0_OFF               (24)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk0_WID               ( 8)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk0_MSK               (0xFF000000)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk0_MIN               (0)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk0_MAX               (255) // 0x000000FF
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk0_DEF               (0x00000000)
  #define DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk0_HSH               (0x08300104)

#define DATA0CH0_CR_TXCONTROL0RANK0_REG                                (0x00000108)

  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqdelay_OFF                    ( 0)
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqdelay_WID                    (12)
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqdelay_MSK                    (0x00000FFF)
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqdelay_MIN                    (0)
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqdelay_MAX                    (4095) // 0x00000FFF
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqdelay_DEF                    (0x00000FFF)
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqdelay_HSH                    (0x0C000108)

  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqsdelay_OFF                   (12)
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqsdelay_WID                   (12)
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqsdelay_MSK                   (0x00FFF000)
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqsdelay_MIN                   (0)
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqsdelay_MAX                   (4095) // 0x00000FFF
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqsdelay_DEF                   (0x00000FFF)
  #define DATA0CH0_CR_TXCONTROL0RANK0_txdqsdelay_HSH                   (0x0C180108)

  #define DATA0CH0_CR_TXCONTROL0RANK0_spare_OFF                        (24)
  #define DATA0CH0_CR_TXCONTROL0RANK0_spare_WID                        ( 8)
  #define DATA0CH0_CR_TXCONTROL0RANK0_spare_MSK                        (0xFF000000)
  #define DATA0CH0_CR_TXCONTROL0RANK0_spare_MIN                        (0)
  #define DATA0CH0_CR_TXCONTROL0RANK0_spare_MAX                        (255) // 0x000000FF
  #define DATA0CH0_CR_TXCONTROL0RANK0_spare_DEF                        (0x00000000)
  #define DATA0CH0_CR_TXCONTROL0RANK0_spare_HSH                        (0x08300108)

#define DATA0CH0_CR_TXCONTROL0RANK1_REG                                (0x0000010C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA0CH0_CR_TXCONTROL0RANK2_REG                                (0x00000110)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA0CH0_CR_TXCONTROL0RANK3_REG                                (0x00000114)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA0CH0_CR_DDRDATADQSRANKX_REG                                (0x00000118)

  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk0_OFF        ( 0)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk0_WID        ( 8)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk0_MSK        (0x000000FF)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk0_MIN        (0)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk0_MAX        (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk0_DEF        (0x00000000)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk0_HSH        (0x08000118)

  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk1_OFF        ( 8)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk1_WID        ( 8)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk1_MSK        (0x0000FF00)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk1_MIN        (0)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk1_MAX        (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk1_DEF        (0x00000000)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk1_HSH        (0x08100118)

  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk2_OFF        (16)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk2_WID        ( 8)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk2_MSK        (0x00FF0000)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk2_MIN        (0)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk2_MAX        (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk2_DEF        (0x00000000)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk2_HSH        (0x08200118)

  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk3_OFF        (24)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk3_WID        ( 8)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk3_MSK        (0xFF000000)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk3_MIN        (0)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk3_MAX        (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk3_DEF        (0x00000000)
  #define DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk3_HSH        (0x08300118)

#define DATA0CH0_CR_DDRDATADQSPH90RANKX_REG                            (0x0000011C)

  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk0_OFF ( 0)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk0_WID ( 8)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk0_MSK (0x000000FF)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk0_MIN (0)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk0_MAX (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk0_DEF (0x00000000)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk0_HSH (0x0800011C)

  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk1_OFF ( 8)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk1_WID ( 8)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk1_MSK (0x0000FF00)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk1_MIN (0)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk1_MAX (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk1_DEF (0x00000000)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk1_HSH (0x0810011C)

  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk2_OFF (16)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk2_WID ( 8)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk2_MSK (0x00FF0000)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk2_MIN (0)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk2_MAX (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk2_DEF (0x00000000)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk2_HSH (0x0820011C)

  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk3_OFF (24)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk3_WID ( 8)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk3_MSK (0xFF000000)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk3_MIN (0)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk3_MAX (255) // 0x000000FF
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk3_DEF (0x00000000)
  #define DATA0CH0_CR_DDRDATADQSPH90RANKX_txdqsperbitdeskewph90_rk3_HSH (0x0830011C)

#define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00000120)

  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvupcompoffset_OFF        ( 0)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvupcompoffset_WID        ( 6)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvupcompoffset_MSK        (0x0000003F)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvupcompoffset_MIN        (-32)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvupcompoffset_MAX        (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvupcompoffset_DEF        (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvupcompoffset_HSH        (0x86000120)

  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvdowncompoffset_OFF      ( 6)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvdowncompoffset_WID      ( 6)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvdowncompoffset_MSK      (0x00000FC0)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvdowncompoffset_MIN      (-32)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvdowncompoffset_MAX      (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvdowncompoffset_DEF      (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvdowncompoffset_HSH      (0x860C0120)

  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqodtcompoffset_OFF          (12)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqodtcompoffset_WID          ( 5)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqodtcompoffset_MSK          (0x0001F000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqodtcompoffset_MIN          (-16)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqodtcompoffset_MAX          (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqodtcompoffset_DEF          (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqodtcompoffset_HSH          (0x85180120)

  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_vsshiffcompoffset_OFF        (17)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_vsshiffcompoffset_WID        ( 5)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_vsshiffcompoffset_MSK        (0x003E0000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_vsshiffcompoffset_MIN        (-16)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_vsshiffcompoffset_MAX        (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_vsshiffcompoffset_DEF        (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_vsshiffcompoffset_HSH        (0x85220120)

  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqslewratecompoffset_OFF     (22)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqslewratecompoffset_WID     ( 5)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqslewratecompoffset_MSK     (0x07C00000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqslewratecompoffset_MIN     (-16)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqslewratecompoffset_MAX     (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqslewratecompoffset_DEF     (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqslewratecompoffset_HSH     (0x852C0120)

  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_rloadoffset_OFF              (27)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_rloadoffset_WID              ( 5)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_rloadoffset_MSK              (0xF8000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_rloadoffset_MIN              (-16)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_rloadoffset_MAX              (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_rloadoffset_DEF              (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETCOMP_rloadoffset_HSH              (0x85360120)

#define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00000124)

  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rcvenoffset_OFF             ( 0)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rcvenoffset_WID             ( 6)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rcvenoffset_MSK             (0x0000003F)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rcvenoffset_MIN             (-32)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rcvenoffset_MAX             (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rcvenoffset_DEF             (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rcvenoffset_HSH             (0x86000124)

  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqsoffset_OFF             ( 6)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqsoffset_WID             ( 6)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqsoffset_MSK             (0x00000FC0)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqsoffset_MIN             (-32)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqsoffset_MAX             (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqsoffset_DEF             (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqsoffset_HSH             (0x860C0124)

  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqoffset_OFF              (12)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqoffset_WID              ( 6)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqoffset_MSK              (0x0003F000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqoffset_MIN              (-32)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqoffset_MAX              (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqoffset_DEF              (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqoffset_HSH              (0x86180124)

  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqsoffset_OFF             (18)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqsoffset_WID             ( 6)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqsoffset_MSK             (0x00FC0000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqsoffset_MIN             (-32)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqsoffset_MAX             (31) // 0x0000001F
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqsoffset_DEF             (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqsoffset_HSH             (0x86240124)

  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_vrefoffset_OFF              (24)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_vrefoffset_WID              ( 7)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_vrefoffset_MSK              (0x7F000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_vrefoffset_MIN              (-64)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_vrefoffset_MAX              (63) // 0x0000003F
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_vrefoffset_DEF              (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_vrefoffset_HSH              (0x87300124)

  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqspiuioffset_OFF         (31)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqspiuioffset_WID         ( 1)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqspiuioffset_MSK         (0x80000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqspiuioffset_MIN         (0)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqspiuioffset_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqspiuioffset_DEF         (0x00000000)
  #define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqspiuioffset_HSH         (0x013E0124)

#define DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG                               (0x00000128)

  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrdmodeen_OFF             ( 0)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrdmodeen_WID             (11)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrdmodeen_MSK             (0x000007FF)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrdmodeen_MIN             (0)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrdmodeen_MAX             (2047) // 0x000007FF
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrdmodeen_DEF             (0x00000000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrdmodeen_HSH             (0x0B000128)

  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrddata_OFF               (11)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrddata_WID               (11)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrddata_MSK               (0x003FF800)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrddata_MIN               (0)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrddata_MAX               (2047) // 0x000007FF
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrddata_DEF               (0x00000000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrddata_HSH               (0x0B160128)

  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrdqsmaskvalue_OFF           (22)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrdqsmaskvalue_WID           ( 1)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrdqsmaskvalue_MSK           (0x00400000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrdqsmaskvalue_MIN           (0)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrdqsmaskvalue_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrdqsmaskvalue_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrdqsmaskvalue_HSH           (0x012C0128)

  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrnumofpulses_OFF            (23)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrnumofpulses_WID            ( 2)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrnumofpulses_MSK            (0x01800000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrnumofpulses_MIN            (0)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrnumofpulses_MAX            (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrnumofpulses_DEF            (0x00000000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrnumofpulses_HSH            (0x022E0128)

  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrmaskcntpulsenumstart_OFF   (25)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrmaskcntpulsenumstart_WID   ( 4)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrmaskcntpulsenumstart_MSK   (0x1E000000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrmaskcntpulsenumstart_MIN   (0)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrmaskcntpulsenumstart_MAX   (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrmaskcntpulsenumstart_DEF   (0x00000000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrmaskcntpulsenumstart_HSH   (0x04320128)

  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_rcvenmrgining_samplepulse_width_OFF (29)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_rcvenmrgining_samplepulse_width_WID ( 2)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_rcvenmrgining_samplepulse_width_MSK (0x60000000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_rcvenmrgining_samplepulse_width_MIN (0)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_rcvenmrgining_samplepulse_width_MAX (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_rcvenmrgining_samplepulse_width_DEF (0x00000000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_rcvenmrgining_samplepulse_width_HSH (0x023A0128)

  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_dqstx_cmn_txodten_OFF           (31)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_dqstx_cmn_txodten_WID           ( 1)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_dqstx_cmn_txodten_MSK           (0x80000000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_dqstx_cmn_txodten_MIN           (0)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_dqstx_cmn_txodten_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_dqstx_cmn_txodten_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRCMDBUSTRAIN_dqstx_cmn_txodten_HSH           (0x013E0128)

#define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x0000012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq0_OFF ( 0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq0_WID ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq0_MSK (0x00000007)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq0_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq0_MAX (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq0_DEF (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq0_HSH (0x0300012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq1_OFF ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq1_WID ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq1_MSK (0x00000038)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq1_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq1_MAX (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq1_DEF (0x00000001)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq1_HSH (0x0306012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq2_OFF ( 6)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq2_WID ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq2_MSK (0x000001C0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq2_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq2_MAX (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq2_DEF (0x00000002)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq2_HSH (0x030C012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq3_OFF ( 9)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq3_WID ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq3_MSK (0x00000E00)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq3_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq3_MAX (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq3_DEF (0x00000003)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq3_HSH (0x0312012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq4_OFF (12)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq4_WID ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq4_MSK (0x00007000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq4_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq4_MAX (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq4_DEF (0x00000004)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq4_HSH (0x0318012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq5_OFF (15)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq5_WID ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq5_MSK (0x00038000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq5_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq5_MAX (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq5_DEF (0x00000005)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq5_HSH (0x031E012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq6_OFF (18)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq6_WID ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq6_MSK (0x001C0000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq6_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq6_MAX (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq6_DEF (0x00000006)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq6_HSH (0x0324012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq7_OFF (21)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq7_WID ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq7_MSK (0x00E00000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq7_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq7_MAX (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq7_DEF (0x00000007)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq7_HSH (0x032A012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_bytesel_OFF (24)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_bytesel_WID ( 1)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_bytesel_MSK (0x01000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_bytesel_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_bytesel_MAX (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_bytesel_DEF (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_bytesel_HSH (0x0130012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrainen_OFF   (25)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrainen_WID   ( 1)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrainen_MSK   (0x02000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrainen_MIN   (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrainen_MAX   (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrainen_DEF   (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrainen_HSH   (0x0132012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataserialmrr_OFF   (26)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataserialmrr_WID   ( 1)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataserialmrr_MSK   (0x04000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataserialmrr_MIN   (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataserialmrr_MAX   (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataserialmrr_DEF   (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataserialmrr_HSH   (0x0134012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_datainvertnibble_OFF (27)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_datainvertnibble_WID ( 2)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_datainvertnibble_MSK (0x18000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_datainvertnibble_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_datainvertnibble_MAX (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_datainvertnibble_DEF (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_datainvertnibble_HSH (0x0236012C)

  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_spare_OFF           (29)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_spare_WID           ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_spare_MSK           (0xE0000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_spare_MIN           (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_spare_MAX           (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_spare_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_spare_HSH           (0x033A012C)

#define DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG                            (0x00000130)

  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_initpicode_OFF               ( 0)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_initpicode_WID               (10)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_initpicode_MSK               (0x000003FF)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_initpicode_MIN               (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_initpicode_MAX               (1023) // 0x000003FF
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_initpicode_DEF               (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_initpicode_HSH               (0x0A000130)

  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_deltapicode_OFF              (10)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_deltapicode_WID              ( 7)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_deltapicode_MSK              (0x0001FC00)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_deltapicode_MIN              (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_deltapicode_MAX              (127) // 0x0000007F
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_deltapicode_DEF              (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_deltapicode_HSH              (0x07140130)

  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_rocount_OFF                  (17)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_rocount_WID                  (15)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_rocount_MSK                  (0xFFFE0000)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_rocount_MIN                  (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_rocount_MAX                  (32767) // 0x00007FFF
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_rocount_DEF                  (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINRANK3_rocount_HSH                  (0x0F220130)

#define DATA0CH0_CR_DDRCRWRRETRAINRANK2_REG                            (0x00000134)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA0CH0_CR_DDRCRWRRETRAINRANK1_REG                            (0x00000138)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA0CH0_CR_DDRCRWRRETRAINRANK0_REG                            (0x0000013C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00000140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_inittrain_OFF        ( 0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_inittrain_WID        ( 1)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_inittrain_MSK        (0x00000001)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_inittrain_MIN        (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_inittrain_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_inittrain_DEF        (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_inittrain_HSH        (0x01000140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_duration_OFF         ( 1)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_duration_WID         (10)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_duration_MSK         (0x000007FE)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_duration_MIN         (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_duration_MAX         (1023) // 0x000003FF
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_duration_DEF         (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_duration_HSH         (0x0A020140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_resetstatus_OFF      (11)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_resetstatus_WID      ( 1)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_resetstatus_MSK      (0x00000800)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_resetstatus_MIN      (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_resetstatus_MAX      (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_resetstatus_DEF      (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_resetstatus_HSH      (0x01160140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangedelta_OFF (12)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangedelta_WID ( 3)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangedelta_MSK (0x00007000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangedelta_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangedelta_MAX (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangedelta_DEF (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangedelta_HSH (0x03180140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_updonlargechange_OFF (15)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_updonlargechange_WID ( 1)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_updonlargechange_MSK (0x00008000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_updonlargechange_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_updonlargechange_MAX (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_updonlargechange_DEF (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_updonlargechange_HSH (0x011E0140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_stoponlargechange_OFF (16)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_stoponlargechange_WID ( 1)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_stoponlargechange_MSK (0x00010000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_stoponlargechange_MIN (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_stoponlargechange_MAX (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_stoponlargechange_DEF (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_stoponlargechange_HSH (0x01200140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r0status_OFF         (17)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r0status_WID         ( 2)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r0status_MSK         (0x00060000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r0status_MIN         (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r0status_MAX         (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r0status_DEF         (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r0status_HSH         (0x02220140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r1status_OFF         (19)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r1status_WID         ( 2)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r1status_MSK         (0x00180000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r1status_MIN         (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r1status_MAX         (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r1status_DEF         (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r1status_HSH         (0x02260140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r2status_OFF         (21)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r2status_WID         ( 2)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r2status_MSK         (0x00600000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r2status_MIN         (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r2status_MAX         (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r2status_DEF         (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r2status_HSH         (0x022A0140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r3status_OFF         (23)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r3status_WID         ( 2)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r3status_MSK         (0x01800000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r3status_MIN         (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r3status_MAX         (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r3status_DEF         (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_r3status_HSH         (0x022E0140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmstatus_OFF        (25)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmstatus_WID        ( 4)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmstatus_MSK        (0x1E000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmstatus_MIN        (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmstatus_MAX        (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmstatus_DEF        (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmstatus_HSH        (0x04320140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmrank_OFF          (29)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmrank_WID          ( 2)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmrank_MSK          (0x60000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmrank_MIN          (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmrank_MAX          (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmrank_DEF          (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_fsmrank_HSH          (0x023A0140)

  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangetrig_OFF  (31)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangetrig_WID  ( 1)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangetrig_MSK  (0x80000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangetrig_MIN  (0)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangetrig_MAX  (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangetrig_DEF  (0x00000000)
  #define DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_largechangetrig_HSH  (0x013E0140)

#define DATA0CH0_CR_TXCONTROL1_PH90_REG                                (0x00000144)

  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk3_OFF                ( 0)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk3_WID                ( 8)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk3_MSK                (0x000000FF)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk3_MIN                (0)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk3_MAX                (255) // 0x000000FF
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk3_DEF                (0x00000000)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk3_HSH                (0x08000144)

  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk3_OFF               ( 8)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk3_WID               ( 8)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk3_MSK               (0x0000FF00)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk3_MIN               (0)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk3_MAX               (255) // 0x000000FF
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk3_DEF               (0x00000000)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk3_HSH               (0x08100144)

  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk2_OFF                (16)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk2_WID                ( 8)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk2_MSK                (0x00FF0000)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk2_MIN                (0)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk2_MAX                (255) // 0x000000FF
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk2_DEF                (0x00000000)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk2_HSH                (0x08200144)

  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk2_OFF               (24)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk2_WID               ( 8)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk2_MSK               (0xFF000000)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk2_MIN               (0)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk2_MAX               (255) // 0x000000FF
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk2_DEF               (0x00000000)
  #define DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk2_HSH               (0x08300144)

#define DATA0CH0_CR_AFEMISCCTRL1_REG                                   (0x00000148)

  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_rxdqstofifodlysel_OFF      ( 0)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_rxdqstofifodlysel_WID      ( 2)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_rxdqstofifodlysel_MSK      (0x00000003)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_rxdqstofifodlysel_MIN      (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_rxdqstofifodlysel_MAX      (3) // 0x00000003
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_rxdqstofifodlysel_DEF      (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_rxdqstofifodlysel_HSH      (0x02000148)

  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuip_OFF           ( 2)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuip_WID           ( 2)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuip_MSK           (0x0000000C)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuip_MIN           (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuip_MAX           (3) // 0x00000003
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuip_DEF           (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuip_HSH           (0x02040148)

  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuin_OFF           ( 4)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuin_WID           ( 2)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuin_MSK           (0x00000030)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuin_MIN           (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuin_MAX           (3) // 0x00000003
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuin_DEF           (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqrx_cmn_dlydfenumuin_HSH           (0x02080148)

  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrdval_OFF      ( 6)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrdval_WID      ( 2)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrdval_MSK      (0x000000C0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrdval_MIN      (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrdval_MAX      (3) // 0x00000003
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrdval_DEF      (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrdval_HSH      (0x020C0148)

  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrden_OFF       ( 8)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrden_WID       ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrden_MSK       (0x00000100)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrden_MIN       (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrden_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrden_DEF       (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srznumpreovrden_HSH       (0x01100148)

  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrdval_OFF     ( 9)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrdval_WID     ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrdval_MSK     (0x00000200)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrdval_MIN     (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrdval_MAX     (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrdval_DEF     (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrdval_HSH     (0x01120148)

  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrden_OFF      (10)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrden_WID      ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrden_MSK      (0x00000400)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrden_MIN      (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrden_MAX      (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrden_DEF      (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpiovrden_HSH      (0x01140148)

  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrdval_OFF  (11)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrdval_WID  ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrdval_MSK  (0x00000800)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrdval_MIN  (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrdval_MAX  (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrdval_DEF  (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrdval_HSH  (0x01160148)

  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrden_OFF   (12)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrden_WID   ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrden_MSK   (0x00001000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrden_MIN   (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrden_MAX   (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrden_DEF   (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_srzrcvenpimodovrden_HSH   (0x01180148)

  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport1_OFF (13)
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport1_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport1_MSK (0x00002000)
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport1_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport1_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport1_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport1_HSH (0x011A0148)

  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport0_OFF (14)
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport0_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport0_MSK (0x00004000)
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport0_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport0_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport0_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_viewmxdig_cmn_viewdigfromdigport0_HSH (0x011C0148)

  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassval_OFF        (15)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassval_WID        ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassval_MSK        (0x00008000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassval_MIN        (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassval_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassval_DEF        (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassval_HSH        (0x011E0148)

  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassen_OFF         (16)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassen_WID         ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassen_MSK         (0x00010000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassen_MIN         (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassen_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassen_DEF         (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_rxsdlbypassen_HSH         (0x01200148)

  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_parken_OFF                (17)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_parken_WID                ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_parken_MSK                (0x00020000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_parken_MIN                (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_parken_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_parken_DEF                (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_parken_HSH                (0x01220148)

  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_enablepiwhenoff_OFF       (18)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_enablepiwhenoff_WID       ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_enablepiwhenoff_MSK       (0x00040000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_enablepiwhenoff_MIN       (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_enablepiwhenoff_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_enablepiwhenoff_DEF       (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_rxsdl_cmn_enablepiwhenoff_HSH       (0x01240148)

  #define DATA0CH0_CR_AFEMISCCTRL1_pghub_cmn_codeupdatepulse_OFF       (19)
  #define DATA0CH0_CR_AFEMISCCTRL1_pghub_cmn_codeupdatepulse_WID       ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_pghub_cmn_codeupdatepulse_MSK       (0x00080000)
  #define DATA0CH0_CR_AFEMISCCTRL1_pghub_cmn_codeupdatepulse_MIN       (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_pghub_cmn_codeupdatepulse_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_pghub_cmn_codeupdatepulse_DEF       (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_pghub_cmn_codeupdatepulse_HSH       (0x01260148)

  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_tcoslewstatlegen_OFF       (20)
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_tcoslewstatlegen_WID       ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_tcoslewstatlegen_MSK       (0x00100000)
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_tcoslewstatlegen_MIN       (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_tcoslewstatlegen_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_tcoslewstatlegen_DEF       (0x00000001)
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_tcoslewstatlegen_HSH       (0x01280148)

  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_parken_OFF                 (21)
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_parken_WID                 ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_parken_MSK                 (0x00200000)
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_parken_MIN                 (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_parken_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_parken_DEF                 (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_dxtx_cmn_parken_HSH                 (0x012A0148)

  #define DATA0CH0_CR_AFEMISCCTRL1_dqsrx_cmn_rxdiffampdfxen_OFF        (22)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqsrx_cmn_rxdiffampdfxen_WID        ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqsrx_cmn_rxdiffampdfxen_MSK        (0x00400000)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqsrx_cmn_rxdiffampdfxen_MIN        (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqsrx_cmn_rxdiffampdfxen_MAX        (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_dqsrx_cmn_rxdiffampdfxen_DEF        (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_dqsrx_cmn_rxdiffampdfxen_HSH        (0x012C0148)

  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_rxsdl_cmn_rcventrainfb_OFF  (23)
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_rxsdl_cmn_rcventrainfb_WID  ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_rxsdl_cmn_rcventrainfb_MSK  (0x00800000)
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_rxsdl_cmn_rcventrainfb_MIN  (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_rxsdl_cmn_rcventrainfb_MAX  (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_rxsdl_cmn_rcventrainfb_DEF  (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_rxsdl_cmn_rcventrainfb_HSH  (0x012E0148)

  #define DATA0CH0_CR_AFEMISCCTRL1_spare_OFF                           (24)
  #define DATA0CH0_CR_AFEMISCCTRL1_spare_WID                           ( 7)
  #define DATA0CH0_CR_AFEMISCCTRL1_spare_MSK                           (0x7F000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_spare_MIN                           (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_spare_MAX                           (127) // 0x0000007F
  #define DATA0CH0_CR_AFEMISCCTRL1_spare_DEF                           (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_spare_HSH                           (0x07300148)

  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_mdll_cmn_vctldaccompareout_OFF (31)
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_mdll_cmn_vctldaccompareout_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_mdll_cmn_vctldaccompareout_MSK (0x80000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_mdll_cmn_vctldaccompareout_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_mdll_cmn_vctldaccompareout_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_mdll_cmn_vctldaccompareout_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL1_afe2dig_mdll_cmn_vctldaccompareout_HSH (0x013E0148)

#define DATA0CH0_CR_DQXRXAMPCTL_REG                                    (0x0000014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxusalspeed_OFF             ( 0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxusalspeed_WID             ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxusalspeed_MSK             (0x00000003)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxusalspeed_MIN             (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxusalspeed_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxusalspeed_DEF             (0x00000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxusalspeed_HSH             (0x0200014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxmtailctl_OFF              ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxmtailctl_WID              ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxmtailctl_MSK              (0x0000000C)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxmtailctl_MIN              (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxmtailctl_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxmtailctl_DEF              (0x00000002)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq7_rxmtailctl_HSH              (0x0204014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxusalspeed_OFF             ( 4)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxusalspeed_WID             ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxusalspeed_MSK             (0x00000030)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxusalspeed_MIN             (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxusalspeed_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxusalspeed_DEF             (0x00000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxusalspeed_HSH             (0x0208014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxmtailctl_OFF              ( 6)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxmtailctl_WID              ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxmtailctl_MSK              (0x000000C0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxmtailctl_MIN              (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxmtailctl_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxmtailctl_DEF              (0x00000002)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq6_rxmtailctl_HSH              (0x020C014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxusalspeed_OFF             ( 8)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxusalspeed_WID             ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxusalspeed_MSK             (0x00000300)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxusalspeed_MIN             (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxusalspeed_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxusalspeed_DEF             (0x00000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxusalspeed_HSH             (0x0210014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxmtailctl_OFF              (10)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxmtailctl_WID              ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxmtailctl_MSK              (0x00000C00)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxmtailctl_MIN              (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxmtailctl_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxmtailctl_DEF              (0x00000002)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq5_rxmtailctl_HSH              (0x0214014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxusalspeed_OFF             (12)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxusalspeed_WID             ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxusalspeed_MSK             (0x00003000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxusalspeed_MIN             (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxusalspeed_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxusalspeed_DEF             (0x00000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxusalspeed_HSH             (0x0218014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxmtailctl_OFF              (14)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxmtailctl_WID              ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxmtailctl_MSK              (0x0000C000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxmtailctl_MIN              (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxmtailctl_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxmtailctl_DEF              (0x00000002)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq4_rxmtailctl_HSH              (0x021C014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxusalspeed_OFF             (16)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxusalspeed_WID             ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxusalspeed_MSK             (0x00030000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxusalspeed_MIN             (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxusalspeed_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxusalspeed_DEF             (0x00000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxusalspeed_HSH             (0x0220014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxmtailctl_OFF              (18)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxmtailctl_WID              ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxmtailctl_MSK              (0x000C0000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxmtailctl_MIN              (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxmtailctl_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxmtailctl_DEF              (0x00000002)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq3_rxmtailctl_HSH              (0x0224014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxusalspeed_OFF             (20)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxusalspeed_WID             ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxusalspeed_MSK             (0x00300000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxusalspeed_MIN             (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxusalspeed_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxusalspeed_DEF             (0x00000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxusalspeed_HSH             (0x0228014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxmtailctl_OFF              (22)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxmtailctl_WID              ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxmtailctl_MSK              (0x00C00000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxmtailctl_MIN              (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxmtailctl_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxmtailctl_DEF              (0x00000002)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq2_rxmtailctl_HSH              (0x022C014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxusalspeed_OFF             (24)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxusalspeed_WID             ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxusalspeed_MSK             (0x03000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxusalspeed_MIN             (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxusalspeed_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxusalspeed_DEF             (0x00000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxusalspeed_HSH             (0x0230014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxmtailctl_OFF              (26)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxmtailctl_WID              ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxmtailctl_MSK              (0x0C000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxmtailctl_MIN              (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxmtailctl_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxmtailctl_DEF              (0x00000002)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq1_rxmtailctl_HSH              (0x0234014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxusalspeed_OFF             (28)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxusalspeed_WID             ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxusalspeed_MSK             (0x30000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxusalspeed_MIN             (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxusalspeed_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxusalspeed_DEF             (0x00000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxusalspeed_HSH             (0x0238014C)

  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxmtailctl_OFF              (30)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxmtailctl_WID              ( 2)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxmtailctl_MSK              (0xC0000000)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxmtailctl_MIN              (0)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxmtailctl_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxmtailctl_DEF              (0x00000002)
  #define DATA0CH0_CR_DQXRXAMPCTL_dqrx_dq0_rxmtailctl_HSH              (0x023C014C)

#define DATA0CH0_CR_DQ0RXAMPCTL_REG                                    (0x00000150)

  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxupmpoffsetcal_HSH     (0x05000150)

  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxupmpoffsetcal_HSH     (0x050A0150)

  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk3_dq0_rxunoffsetcal_OFF       (10)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk3_dq0_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk3_dq0_rxunoffsetcal_MSK       (0x00007C00)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk3_dq0_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk3_dq0_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk3_dq0_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk3_dq0_rxunoffsetcal_HSH       (0x05140150)

  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk2_dq0_rxunoffsetcal_OFF       (15)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk2_dq0_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk2_dq0_rxunoffsetcal_MSK       (0x000F8000)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk2_dq0_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk2_dq0_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk2_dq0_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk2_dq0_rxunoffsetcal_HSH       (0x051E0150)

  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxunoffsetcal_OFF       (20)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxunoffsetcal_MSK       (0x01F00000)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxunoffsetcal_HSH       (0x05280150)

  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxunoffsetcal_OFF       (25)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxunoffsetcal_MSK       (0x3E000000)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxunoffsetcal_HSH       (0x05320150)

  #define DATA0CH0_CR_DQ0RXAMPCTL_Reserved9_OFF                        (30)
  #define DATA0CH0_CR_DQ0RXAMPCTL_Reserved9_WID                        ( 2)
  #define DATA0CH0_CR_DQ0RXAMPCTL_Reserved9_MSK                        (0xC0000000)
  #define DATA0CH0_CR_DQ0RXAMPCTL_Reserved9_MIN                        (0)
  #define DATA0CH0_CR_DQ0RXAMPCTL_Reserved9_MAX                        (3) // 0x00000003
  #define DATA0CH0_CR_DQ0RXAMPCTL_Reserved9_DEF                        (0x00000000)
  #define DATA0CH0_CR_DQ0RXAMPCTL_Reserved9_HSH                        (0x023C0150)

#define DATA0CH0_CR_DQ1RXAMPCTL_REG                                    (0x00000154)

  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxupmpoffsetcal_HSH     (0x05000154)

  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxupmpoffsetcal_HSH     (0x050A0154)

  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk3_dq1_rxunoffsetcal_OFF       (10)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk3_dq1_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk3_dq1_rxunoffsetcal_MSK       (0x00007C00)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk3_dq1_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk3_dq1_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk3_dq1_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk3_dq1_rxunoffsetcal_HSH       (0x05140154)

  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk2_dq1_rxunoffsetcal_OFF       (15)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk2_dq1_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk2_dq1_rxunoffsetcal_MSK       (0x000F8000)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk2_dq1_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk2_dq1_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk2_dq1_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk2_dq1_rxunoffsetcal_HSH       (0x051E0154)

  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxunoffsetcal_OFF       (20)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxunoffsetcal_MSK       (0x01F00000)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk1_dq1_rxunoffsetcal_HSH       (0x05280154)

  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxunoffsetcal_OFF       (25)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxunoffsetcal_MSK       (0x3E000000)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ1RXAMPCTL_dqrx_rk0_dq1_rxunoffsetcal_HSH       (0x05320154)

  #define DATA0CH0_CR_DQ1RXAMPCTL_Reserved10_OFF                       (30)
  #define DATA0CH0_CR_DQ1RXAMPCTL_Reserved10_WID                       ( 2)
  #define DATA0CH0_CR_DQ1RXAMPCTL_Reserved10_MSK                       (0xC0000000)
  #define DATA0CH0_CR_DQ1RXAMPCTL_Reserved10_MIN                       (0)
  #define DATA0CH0_CR_DQ1RXAMPCTL_Reserved10_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_DQ1RXAMPCTL_Reserved10_DEF                       (0x00000000)
  #define DATA0CH0_CR_DQ1RXAMPCTL_Reserved10_HSH                       (0x023C0154)

#define DATA0CH0_CR_DQ2RXAMPCTL_REG                                    (0x00000158)

  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxupmpoffsetcal_HSH     (0x05000158)

  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxupmpoffsetcal_HSH     (0x050A0158)

  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk3_dq2_rxunoffsetcal_OFF       (10)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk3_dq2_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk3_dq2_rxunoffsetcal_MSK       (0x00007C00)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk3_dq2_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk3_dq2_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk3_dq2_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk3_dq2_rxunoffsetcal_HSH       (0x05140158)

  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk2_dq2_rxunoffsetcal_OFF       (15)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk2_dq2_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk2_dq2_rxunoffsetcal_MSK       (0x000F8000)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk2_dq2_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk2_dq2_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk2_dq2_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk2_dq2_rxunoffsetcal_HSH       (0x051E0158)

  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxunoffsetcal_OFF       (20)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxunoffsetcal_MSK       (0x01F00000)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk1_dq2_rxunoffsetcal_HSH       (0x05280158)

  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxunoffsetcal_OFF       (25)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxunoffsetcal_MSK       (0x3E000000)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ2RXAMPCTL_dqrx_rk0_dq2_rxunoffsetcal_HSH       (0x05320158)

  #define DATA0CH0_CR_DQ2RXAMPCTL_Reserved11_OFF                       (30)
  #define DATA0CH0_CR_DQ2RXAMPCTL_Reserved11_WID                       ( 2)
  #define DATA0CH0_CR_DQ2RXAMPCTL_Reserved11_MSK                       (0xC0000000)
  #define DATA0CH0_CR_DQ2RXAMPCTL_Reserved11_MIN                       (0)
  #define DATA0CH0_CR_DQ2RXAMPCTL_Reserved11_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_DQ2RXAMPCTL_Reserved11_DEF                       (0x00000000)
  #define DATA0CH0_CR_DQ2RXAMPCTL_Reserved11_HSH                       (0x023C0158)

#define DATA0CH0_CR_DQ3RXAMPCTL_REG                                    (0x0000015C)

  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxupmpoffsetcal_HSH     (0x0500015C)

  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxupmpoffsetcal_HSH     (0x050A015C)

  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk3_dq3_rxunoffsetcal_OFF       (10)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk3_dq3_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk3_dq3_rxunoffsetcal_MSK       (0x00007C00)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk3_dq3_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk3_dq3_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk3_dq3_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk3_dq3_rxunoffsetcal_HSH       (0x0514015C)

  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk2_dq3_rxunoffsetcal_OFF       (15)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk2_dq3_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk2_dq3_rxunoffsetcal_MSK       (0x000F8000)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk2_dq3_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk2_dq3_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk2_dq3_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk2_dq3_rxunoffsetcal_HSH       (0x051E015C)

  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxunoffsetcal_OFF       (20)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxunoffsetcal_MSK       (0x01F00000)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk1_dq3_rxunoffsetcal_HSH       (0x0528015C)

  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxunoffsetcal_OFF       (25)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxunoffsetcal_MSK       (0x3E000000)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ3RXAMPCTL_dqrx_rk0_dq3_rxunoffsetcal_HSH       (0x0532015C)

  #define DATA0CH0_CR_DQ3RXAMPCTL_Reserved12_OFF                       (30)
  #define DATA0CH0_CR_DQ3RXAMPCTL_Reserved12_WID                       ( 2)
  #define DATA0CH0_CR_DQ3RXAMPCTL_Reserved12_MSK                       (0xC0000000)
  #define DATA0CH0_CR_DQ3RXAMPCTL_Reserved12_MIN                       (0)
  #define DATA0CH0_CR_DQ3RXAMPCTL_Reserved12_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_DQ3RXAMPCTL_Reserved12_DEF                       (0x00000000)
  #define DATA0CH0_CR_DQ3RXAMPCTL_Reserved12_HSH                       (0x023C015C)

#define DATA0CH0_CR_DQ4RXAMPCTL_REG                                    (0x00000160)

  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxupmpoffsetcal_HSH     (0x05000160)

  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxupmpoffsetcal_HSH     (0x050A0160)

  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk3_dq4_rxunoffsetcal_OFF       (10)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk3_dq4_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk3_dq4_rxunoffsetcal_MSK       (0x00007C00)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk3_dq4_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk3_dq4_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk3_dq4_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk3_dq4_rxunoffsetcal_HSH       (0x05140160)

  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk2_dq4_rxunoffsetcal_OFF       (15)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk2_dq4_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk2_dq4_rxunoffsetcal_MSK       (0x000F8000)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk2_dq4_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk2_dq4_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk2_dq4_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk2_dq4_rxunoffsetcal_HSH       (0x051E0160)

  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxunoffsetcal_OFF       (20)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxunoffsetcal_MSK       (0x01F00000)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk1_dq4_rxunoffsetcal_HSH       (0x05280160)

  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxunoffsetcal_OFF       (25)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxunoffsetcal_MSK       (0x3E000000)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ4RXAMPCTL_dqrx_rk0_dq4_rxunoffsetcal_HSH       (0x05320160)

  #define DATA0CH0_CR_DQ4RXAMPCTL_Reserved13_OFF                       (30)
  #define DATA0CH0_CR_DQ4RXAMPCTL_Reserved13_WID                       ( 2)
  #define DATA0CH0_CR_DQ4RXAMPCTL_Reserved13_MSK                       (0xC0000000)
  #define DATA0CH0_CR_DQ4RXAMPCTL_Reserved13_MIN                       (0)
  #define DATA0CH0_CR_DQ4RXAMPCTL_Reserved13_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_DQ4RXAMPCTL_Reserved13_DEF                       (0x00000000)
  #define DATA0CH0_CR_DQ4RXAMPCTL_Reserved13_HSH                       (0x023C0160)

#define DATA0CH0_CR_DQ5RXAMPCTL_REG                                    (0x00000164)

  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxupmpoffsetcal_HSH     (0x05000164)

  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxupmpoffsetcal_HSH     (0x050A0164)

  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk3_dq5_rxunoffsetcal_OFF       (10)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk3_dq5_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk3_dq5_rxunoffsetcal_MSK       (0x00007C00)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk3_dq5_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk3_dq5_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk3_dq5_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk3_dq5_rxunoffsetcal_HSH       (0x05140164)

  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk2_dq5_rxunoffsetcal_OFF       (15)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk2_dq5_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk2_dq5_rxunoffsetcal_MSK       (0x000F8000)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk2_dq5_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk2_dq5_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk2_dq5_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk2_dq5_rxunoffsetcal_HSH       (0x051E0164)

  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxunoffsetcal_OFF       (20)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxunoffsetcal_MSK       (0x01F00000)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk1_dq5_rxunoffsetcal_HSH       (0x05280164)

  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxunoffsetcal_OFF       (25)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxunoffsetcal_MSK       (0x3E000000)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ5RXAMPCTL_dqrx_rk0_dq5_rxunoffsetcal_HSH       (0x05320164)

  #define DATA0CH0_CR_DQ5RXAMPCTL_Reserved14_OFF                       (30)
  #define DATA0CH0_CR_DQ5RXAMPCTL_Reserved14_WID                       ( 2)
  #define DATA0CH0_CR_DQ5RXAMPCTL_Reserved14_MSK                       (0xC0000000)
  #define DATA0CH0_CR_DQ5RXAMPCTL_Reserved14_MIN                       (0)
  #define DATA0CH0_CR_DQ5RXAMPCTL_Reserved14_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_DQ5RXAMPCTL_Reserved14_DEF                       (0x00000000)
  #define DATA0CH0_CR_DQ5RXAMPCTL_Reserved14_HSH                       (0x023C0164)

#define DATA0CH0_CR_DQ6RXAMPCTL_REG                                    (0x00000168)

  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxupmpoffsetcal_HSH     (0x05000168)

  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxupmpoffsetcal_HSH     (0x050A0168)

  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk3_dq6_rxunoffsetcal_OFF       (10)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk3_dq6_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk3_dq6_rxunoffsetcal_MSK       (0x00007C00)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk3_dq6_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk3_dq6_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk3_dq6_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk3_dq6_rxunoffsetcal_HSH       (0x05140168)

  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk2_dq6_rxunoffsetcal_OFF       (15)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk2_dq6_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk2_dq6_rxunoffsetcal_MSK       (0x000F8000)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk2_dq6_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk2_dq6_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk2_dq6_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk2_dq6_rxunoffsetcal_HSH       (0x051E0168)

  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxunoffsetcal_OFF       (20)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxunoffsetcal_MSK       (0x01F00000)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk1_dq6_rxunoffsetcal_HSH       (0x05280168)

  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxunoffsetcal_OFF       (25)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxunoffsetcal_MSK       (0x3E000000)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ6RXAMPCTL_dqrx_rk0_dq6_rxunoffsetcal_HSH       (0x05320168)

  #define DATA0CH0_CR_DQ6RXAMPCTL_Reserved15_OFF                       (30)
  #define DATA0CH0_CR_DQ6RXAMPCTL_Reserved15_WID                       ( 2)
  #define DATA0CH0_CR_DQ6RXAMPCTL_Reserved15_MSK                       (0xC0000000)
  #define DATA0CH0_CR_DQ6RXAMPCTL_Reserved15_MIN                       (0)
  #define DATA0CH0_CR_DQ6RXAMPCTL_Reserved15_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_DQ6RXAMPCTL_Reserved15_DEF                       (0x00000000)
  #define DATA0CH0_CR_DQ6RXAMPCTL_Reserved15_HSH                       (0x023C0168)

#define DATA0CH0_CR_DQ7RXAMPCTL_REG                                    (0x0000016C)

  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxupmpoffsetcal_HSH     (0x0500016C)

  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxupmpoffsetcal_HSH     (0x050A016C)

  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk3_dq7_rxunoffsetcal_OFF       (10)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk3_dq7_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk3_dq7_rxunoffsetcal_MSK       (0x00007C00)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk3_dq7_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk3_dq7_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk3_dq7_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk3_dq7_rxunoffsetcal_HSH       (0x0514016C)

  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk2_dq7_rxunoffsetcal_OFF       (15)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk2_dq7_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk2_dq7_rxunoffsetcal_MSK       (0x000F8000)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk2_dq7_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk2_dq7_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk2_dq7_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk2_dq7_rxunoffsetcal_HSH       (0x051E016C)

  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxunoffsetcal_OFF       (20)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxunoffsetcal_MSK       (0x01F00000)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk1_dq7_rxunoffsetcal_HSH       (0x0528016C)

  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxunoffsetcal_OFF       (25)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxunoffsetcal_WID       ( 5)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxunoffsetcal_MSK       (0x3E000000)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxunoffsetcal_MIN       (0)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxunoffsetcal_MAX       (31) // 0x0000001F
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxunoffsetcal_DEF       (0x00000010)
  #define DATA0CH0_CR_DQ7RXAMPCTL_dqrx_rk0_dq7_rxunoffsetcal_HSH       (0x0532016C)

  #define DATA0CH0_CR_DQ7RXAMPCTL_Reserved16_OFF                       (30)
  #define DATA0CH0_CR_DQ7RXAMPCTL_Reserved16_WID                       ( 2)
  #define DATA0CH0_CR_DQ7RXAMPCTL_Reserved16_MSK                       (0xC0000000)
  #define DATA0CH0_CR_DQ7RXAMPCTL_Reserved16_MIN                       (0)
  #define DATA0CH0_CR_DQ7RXAMPCTL_Reserved16_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_DQ7RXAMPCTL_Reserved16_DEF                       (0x00000000)
  #define DATA0CH0_CR_DQ7RXAMPCTL_Reserved16_HSH                       (0x023C016C)

#define DATA0CH0_CR_DQRXAMPCTL0_REG                                    (0x00000170)

  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq2_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq2_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq2_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq2_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq2_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq2_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq2_rxupmpoffsetcal_HSH     (0x05000170)

  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq2_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq2_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq2_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq2_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq2_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq2_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq2_rxupmpoffsetcal_HSH     (0x050A0170)

  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq1_rxupmpoffsetcal_OFF     (10)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq1_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq1_rxupmpoffsetcal_MSK     (0x00007C00)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq1_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq1_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq1_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq1_rxupmpoffsetcal_HSH     (0x05140170)

  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq1_rxupmpoffsetcal_OFF     (15)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq1_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq1_rxupmpoffsetcal_MSK     (0x000F8000)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq1_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq1_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq1_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq1_rxupmpoffsetcal_HSH     (0x051E0170)

  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq0_rxupmpoffsetcal_OFF     (20)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq0_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq0_rxupmpoffsetcal_MSK     (0x01F00000)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq0_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq0_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq0_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq0_rxupmpoffsetcal_HSH     (0x05280170)

  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq0_rxupmpoffsetcal_OFF     (25)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq0_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq0_rxupmpoffsetcal_MSK     (0x3E000000)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq0_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq0_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq0_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq0_rxupmpoffsetcal_HSH     (0x05320170)

  #define DATA0CH0_CR_DQRXAMPCTL0_Reserved17_OFF                       (30)
  #define DATA0CH0_CR_DQRXAMPCTL0_Reserved17_WID                       ( 2)
  #define DATA0CH0_CR_DQRXAMPCTL0_Reserved17_MSK                       (0xC0000000)
  #define DATA0CH0_CR_DQRXAMPCTL0_Reserved17_MIN                       (0)
  #define DATA0CH0_CR_DQRXAMPCTL0_Reserved17_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_DQRXAMPCTL0_Reserved17_DEF                       (0x00000000)
  #define DATA0CH0_CR_DQRXAMPCTL0_Reserved17_HSH                       (0x023C0170)

#define DATA0CH0_CR_DQRXAMPCTL1_REG                                    (0x00000174)

  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq5_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq5_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq5_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq5_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq5_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq5_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq5_rxupmpoffsetcal_HSH     (0x05000174)

  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq5_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq5_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq5_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq5_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq5_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq5_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq5_rxupmpoffsetcal_HSH     (0x050A0174)

  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq4_rxupmpoffsetcal_OFF     (10)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq4_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq4_rxupmpoffsetcal_MSK     (0x00007C00)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq4_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq4_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq4_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq4_rxupmpoffsetcal_HSH     (0x05140174)

  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq4_rxupmpoffsetcal_OFF     (15)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq4_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq4_rxupmpoffsetcal_MSK     (0x000F8000)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq4_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq4_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq4_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq4_rxupmpoffsetcal_HSH     (0x051E0174)

  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq3_rxupmpoffsetcal_OFF     (20)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq3_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq3_rxupmpoffsetcal_MSK     (0x01F00000)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq3_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq3_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq3_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk3_dq3_rxupmpoffsetcal_HSH     (0x05280174)

  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq3_rxupmpoffsetcal_OFF     (25)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq3_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq3_rxupmpoffsetcal_MSK     (0x3E000000)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq3_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq3_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq3_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL1_dqrx_rk2_dq3_rxupmpoffsetcal_HSH     (0x05320174)

  #define DATA0CH0_CR_DQRXAMPCTL1_Reserved18_OFF                       (30)
  #define DATA0CH0_CR_DQRXAMPCTL1_Reserved18_WID                       ( 2)
  #define DATA0CH0_CR_DQRXAMPCTL1_Reserved18_MSK                       (0xC0000000)
  #define DATA0CH0_CR_DQRXAMPCTL1_Reserved18_MIN                       (0)
  #define DATA0CH0_CR_DQRXAMPCTL1_Reserved18_MAX                       (3) // 0x00000003
  #define DATA0CH0_CR_DQRXAMPCTL1_Reserved18_DEF                       (0x00000000)
  #define DATA0CH0_CR_DQRXAMPCTL1_Reserved18_HSH                       (0x023C0174)

#define DATA0CH0_CR_DQRXAMPCTL2_REG                                    (0x00000178)

  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq7_rxupmpoffsetcal_OFF     ( 0)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq7_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq7_rxupmpoffsetcal_MSK     (0x0000001F)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq7_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq7_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq7_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq7_rxupmpoffsetcal_HSH     (0x05000178)

  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq7_rxupmpoffsetcal_OFF     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq7_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq7_rxupmpoffsetcal_MSK     (0x000003E0)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq7_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq7_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq7_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq7_rxupmpoffsetcal_HSH     (0x050A0178)

  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq6_rxupmpoffsetcal_OFF     (10)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq6_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq6_rxupmpoffsetcal_MSK     (0x00007C00)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq6_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq6_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq6_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk3_dq6_rxupmpoffsetcal_HSH     (0x05140178)

  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq6_rxupmpoffsetcal_OFF     (15)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq6_rxupmpoffsetcal_WID     ( 5)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq6_rxupmpoffsetcal_MSK     (0x000F8000)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq6_rxupmpoffsetcal_MIN     (0)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq6_rxupmpoffsetcal_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq6_rxupmpoffsetcal_DEF     (0x00000010)
  #define DATA0CH0_CR_DQRXAMPCTL2_dqrx_rk2_dq6_rxupmpoffsetcal_HSH     (0x051E0178)

  #define DATA0CH0_CR_DQRXAMPCTL2_Reserved19_OFF                       (20)
  #define DATA0CH0_CR_DQRXAMPCTL2_Reserved19_WID                       (12)
  #define DATA0CH0_CR_DQRXAMPCTL2_Reserved19_MSK                       (0xFFF00000)
  #define DATA0CH0_CR_DQRXAMPCTL2_Reserved19_MIN                       (0)
  #define DATA0CH0_CR_DQRXAMPCTL2_Reserved19_MAX                       (4095) // 0x00000FFF
  #define DATA0CH0_CR_DQRXAMPCTL2_Reserved19_DEF                       (0x00000000)
  #define DATA0CH0_CR_DQRXAMPCTL2_Reserved19_HSH                       (0x0C280178)

#define DATA0CH0_CR_DQTXCTL0_REG                                       (0x0000017C)

  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvttodten_OFF                 ( 0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvttodten_WID                 ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvttodten_MSK                 (0x00000001)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvttodten_MIN                 (0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvttodten_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvttodten_DEF                 (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvttodten_HSH                 (0x0100017C)

  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvssodten_OFF                 ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvssodten_WID                 ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvssodten_MSK                 (0x00000002)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvssodten_MIN                 (0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvssodten_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvssodten_DEF                 (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvssodten_HSH                 (0x0102017C)

  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvddqodten_OFF                ( 2)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvddqodten_WID                ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvddqodten_MSK                (0x00000004)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvddqodten_MIN                (0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvddqodten_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvddqodten_DEF                (0x00000001)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txvddqodten_HSH                (0x0104017C)

  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpupusevcciog_OFF             ( 3)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpupusevcciog_WID             ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpupusevcciog_MSK             (0x00000008)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpupusevcciog_MIN             (0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpupusevcciog_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpupusevcciog_DEF             (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpupusevcciog_HSH             (0x0106017C)

  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpdnusevcciog_OFF             ( 4)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpdnusevcciog_WID             ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpdnusevcciog_MSK             (0x00000010)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpdnusevcciog_MIN             (0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpdnusevcciog_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpdnusevcciog_DEF             (0x00000001)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txpdnusevcciog_HSH             (0x0108017C)

  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txlocalvsshibyp_OFF            ( 5)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txlocalvsshibyp_WID            ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txlocalvsshibyp_MSK            (0x00000020)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txlocalvsshibyp_MIN            (0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txlocalvsshibyp_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txlocalvsshibyp_DEF            (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txlocalvsshibyp_HSH            (0x010A017C)

  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txvsshiffstlegen_OFF           ( 6)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txvsshiffstlegen_WID           ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txvsshiffstlegen_MSK           (0x00000040)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txvsshiffstlegen_MIN           (0)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txvsshiffstlegen_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txvsshiffstlegen_DEF           (0x00000001)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txvsshiffstlegen_HSH           (0x010C017C)

  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txdyncpadreduxdis_OFF          ( 7)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txdyncpadreduxdis_WID          ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txdyncpadreduxdis_MSK          (0x00000080)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txdyncpadreduxdis_MIN          (0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txdyncpadreduxdis_MAX          (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txdyncpadreduxdis_DEF          (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_txdyncpadreduxdis_HSH          (0x010E017C)

  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_ennmospup_OFF                  ( 8)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_ennmospup_WID                  ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_ennmospup_MSK                  (0x00000100)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_ennmospup_MIN                  (0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_ennmospup_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_ennmospup_DEF                  (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_ennmospup_HSH                  (0x0110017C)

  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txodtstatlegendq_OFF           ( 9)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txodtstatlegendq_WID           ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txodtstatlegendq_MSK           (0x00000200)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txodtstatlegendq_MIN           (0)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txodtstatlegendq_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txodtstatlegendq_DEF           (0x00000001)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txodtstatlegendq_HSH           (0x0112017C)

  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodton_OFF                 (10)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodton_WID                 ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodton_MSK                 (0x00000400)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodton_MIN                 (0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodton_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodton_DEF                 (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodton_HSH                 (0x0114017C)

  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodtoff_OFF                (11)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodtoff_WID                ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodtoff_MSK                (0x00000800)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodtoff_MIN                (0)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodtoff_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodtoff_DEF                (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dxtx_cmn_forceodtoff_HSH                (0x0116017C)

  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrven_OFF            (12)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrven_WID            ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrven_MSK            (0x00001000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrven_MIN            (0)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrven_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrven_DEF            (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrven_HSH            (0x0118017C)

  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrvdata_OFF          (13)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrvdata_WID          ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrvdata_MSK          (0x00002000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrvdata_MIN          (0)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrvdata_MAX          (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrvdata_DEF          (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txidlemodedrvdata_HSH          (0x011A017C)

  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenpreset_OFF               (14)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenpreset_WID               ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenpreset_MSK               (0x00004000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenpreset_MIN               (0)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenpreset_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenpreset_DEF               (0x00000001)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenpreset_HSH               (0x011C017C)

  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenntap_OFF                 (15)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenntap_WID                 ( 2)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenntap_MSK                 (0x00018000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenntap_MIN                 (0)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenntap_MAX                 (3) // 0x00000003
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenntap_DEF                 (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenntap_HSH                 (0x021E017C)

  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqconstz_OFF                 (17)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqconstz_WID                 ( 1)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqconstz_MSK                 (0x00020000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqconstz_MIN                 (0)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqconstz_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqconstz_DEF                 (0x00000001)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqconstz_HSH                 (0x0122017C)

  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff2_OFF             (18)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff2_WID             ( 3)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff2_MSK             (0x001C0000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff2_MIN             (0)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff2_MAX             (7) // 0x00000007
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff2_DEF             (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff2_HSH             (0x0324017C)

  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff1_OFF             (21)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff1_WID             ( 3)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff1_MSK             (0x00E00000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff1_MIN             (0)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff1_MAX             (7) // 0x00000007
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff1_DEF             (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff1_HSH             (0x032A017C)

  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff0_OFF             (24)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff0_WID             ( 3)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff0_MSK             (0x07000000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff0_MIN             (0)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff0_MAX             (7) // 0x00000007
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff0_DEF             (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff0_HSH             (0x0330017C)

  #define DATA0CH0_CR_DQTXCTL0_Reserved20_OFF                          (27)
  #define DATA0CH0_CR_DQTXCTL0_Reserved20_WID                          ( 5)
  #define DATA0CH0_CR_DQTXCTL0_Reserved20_MSK                          (0xF8000000)
  #define DATA0CH0_CR_DQTXCTL0_Reserved20_MIN                          (0)
  #define DATA0CH0_CR_DQTXCTL0_Reserved20_MAX                          (31) // 0x0000001F
  #define DATA0CH0_CR_DQTXCTL0_Reserved20_DEF                          (0x00000000)
  #define DATA0CH0_CR_DQTXCTL0_Reserved20_HSH                          (0x0536017C)

#define DATA0CH0_CR_DCCCTL0_REG                                        (0x00000180)

  #define DATA0CH0_CR_DCCCTL0_dccsettle_dly_OFF                        ( 0)
  #define DATA0CH0_CR_DCCCTL0_dccsettle_dly_WID                        ( 3)
  #define DATA0CH0_CR_DCCCTL0_dccsettle_dly_MSK                        (0x00000007)
  #define DATA0CH0_CR_DCCCTL0_dccsettle_dly_MIN                        (0)
  #define DATA0CH0_CR_DCCCTL0_dccsettle_dly_MAX                        (7) // 0x00000007
  #define DATA0CH0_CR_DCCCTL0_dccsettle_dly_DEF                        (0x00000000)
  #define DATA0CH0_CR_DCCCTL0_dccsettle_dly_HSH                        (0x03000180)

  #define DATA0CH0_CR_DCCCTL0_totalsamplecounts_OFF                    ( 3)
  #define DATA0CH0_CR_DCCCTL0_totalsamplecounts_WID                    (16)
  #define DATA0CH0_CR_DCCCTL0_totalsamplecounts_MSK                    (0x0007FFF8)
  #define DATA0CH0_CR_DCCCTL0_totalsamplecounts_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL0_totalsamplecounts_MAX                    (65535) // 0x0000FFFF
  #define DATA0CH0_CR_DCCCTL0_totalsamplecounts_DEF                    (0x00000000)
  #define DATA0CH0_CR_DCCCTL0_totalsamplecounts_HSH                    (0x10060180)

  #define DATA0CH0_CR_DCCCTL0_macrostep_OFF                            (19)
  #define DATA0CH0_CR_DCCCTL0_macrostep_WID                            ( 8)
  #define DATA0CH0_CR_DCCCTL0_macrostep_MSK                            (0x07F80000)
  #define DATA0CH0_CR_DCCCTL0_macrostep_MIN                            (0)
  #define DATA0CH0_CR_DCCCTL0_macrostep_MAX                            (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL0_macrostep_DEF                            (0x00000008)
  #define DATA0CH0_CR_DCCCTL0_macrostep_HSH                            (0x08260180)

  #define DATA0CH0_CR_DCCCTL0_microstep_OFF                            (27)
  #define DATA0CH0_CR_DCCCTL0_microstep_WID                            ( 4)
  #define DATA0CH0_CR_DCCCTL0_microstep_MSK                            (0x78000000)
  #define DATA0CH0_CR_DCCCTL0_microstep_MIN                            (0)
  #define DATA0CH0_CR_DCCCTL0_microstep_MAX                            (15) // 0x0000000F
  #define DATA0CH0_CR_DCCCTL0_microstep_DEF                            (0x00000001)
  #define DATA0CH0_CR_DCCCTL0_microstep_HSH                            (0x04360180)

  #define DATA0CH0_CR_DCCCTL0_dcdrovcoen_OFF                           (31)
  #define DATA0CH0_CR_DCCCTL0_dcdrovcoen_WID                           ( 1)
  #define DATA0CH0_CR_DCCCTL0_dcdrovcoen_MSK                           (0x80000000)
  #define DATA0CH0_CR_DCCCTL0_dcdrovcoen_MIN                           (0)
  #define DATA0CH0_CR_DCCCTL0_dcdrovcoen_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL0_dcdrovcoen_DEF                           (0x00000000)
  #define DATA0CH0_CR_DCCCTL0_dcdrovcoen_HSH                           (0x013E0180)

#define DATA0CH0_CR_DCCCTL1_REG                                        (0x00000184)

  #define DATA0CH0_CR_DCCCTL1_dcc_high_limit_marker_OFF                ( 0)
  #define DATA0CH0_CR_DCCCTL1_dcc_high_limit_marker_WID                (16)
  #define DATA0CH0_CR_DCCCTL1_dcc_high_limit_marker_MSK                (0x0000FFFF)
  #define DATA0CH0_CR_DCCCTL1_dcc_high_limit_marker_MIN                (0)
  #define DATA0CH0_CR_DCCCTL1_dcc_high_limit_marker_MAX                (65535) // 0x0000FFFF
  #define DATA0CH0_CR_DCCCTL1_dcc_high_limit_marker_DEF                (0x00000000)
  #define DATA0CH0_CR_DCCCTL1_dcc_high_limit_marker_HSH                (0x10000184)

  #define DATA0CH0_CR_DCCCTL1_dcc_low_limit_marker_OFF                 (16)
  #define DATA0CH0_CR_DCCCTL1_dcc_low_limit_marker_WID                 (16)
  #define DATA0CH0_CR_DCCCTL1_dcc_low_limit_marker_MSK                 (0xFFFF0000)
  #define DATA0CH0_CR_DCCCTL1_dcc_low_limit_marker_MIN                 (0)
  #define DATA0CH0_CR_DCCCTL1_dcc_low_limit_marker_MAX                 (65535) // 0x0000FFFF
  #define DATA0CH0_CR_DCCCTL1_dcc_low_limit_marker_DEF                 (0x00000000)
  #define DATA0CH0_CR_DCCCTL1_dcc_low_limit_marker_HSH                 (0x10200184)

#define DATA0CH0_CR_DCCCTL2_REG                                        (0x00000188)

  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountstart_OFF                  ( 0)
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountstart_WID                  ( 1)
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountstart_MSK                  (0x00000001)
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountstart_MIN                  (0)
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountstart_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountstart_DEF                  (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountstart_HSH                  (0x01000188)

  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountrst_b_OFF                  ( 1)
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountrst_b_WID                  ( 1)
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountrst_b_MSK                  (0x00000002)
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountrst_b_MIN                  (0)
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountrst_b_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountrst_b_DEF                  (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_dcdsamplecountrst_b_HSH                  (0x01020188)

  #define DATA0CH0_CR_DCCCTL2_dcdrovcoprog_OFF                         ( 2)
  #define DATA0CH0_CR_DCCCTL2_dcdrovcoprog_WID                         ( 3)
  #define DATA0CH0_CR_DCCCTL2_dcdrovcoprog_MSK                         (0x0000001C)
  #define DATA0CH0_CR_DCCCTL2_dcdrovcoprog_MIN                         (0)
  #define DATA0CH0_CR_DCCCTL2_dcdrovcoprog_MAX                         (7) // 0x00000007
  #define DATA0CH0_CR_DCCCTL2_dcdrovcoprog_DEF                         (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_dcdrovcoprog_HSH                         (0x03040188)

  #define DATA0CH0_CR_DCCCTL2_dcdrolfsr_OFF                            ( 5)
  #define DATA0CH0_CR_DCCCTL2_dcdrolfsr_WID                            (10)
  #define DATA0CH0_CR_DCCCTL2_dcdrolfsr_MSK                            (0x00007FE0)
  #define DATA0CH0_CR_DCCCTL2_dcdrolfsr_MIN                            (0)
  #define DATA0CH0_CR_DCCCTL2_dcdrolfsr_MAX                            (1023) // 0x000003FF
  #define DATA0CH0_CR_DCCCTL2_dcdrolfsr_DEF                            (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_dcdrolfsr_HSH                            (0x0A0A0188)

  #define DATA0CH0_CR_DCCCTL2_srz_dcdenable_OFF                        (15)
  #define DATA0CH0_CR_DCCCTL2_srz_dcdenable_WID                        ( 1)
  #define DATA0CH0_CR_DCCCTL2_srz_dcdenable_MSK                        (0x00008000)
  #define DATA0CH0_CR_DCCCTL2_srz_dcdenable_MIN                        (0)
  #define DATA0CH0_CR_DCCCTL2_srz_dcdenable_MAX                        (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL2_srz_dcdenable_DEF                        (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_srz_dcdenable_HSH                        (0x011E0188)

  #define DATA0CH0_CR_DCCCTL2_mdll_dcdenable_OFF                       (16)
  #define DATA0CH0_CR_DCCCTL2_mdll_dcdenable_WID                       ( 1)
  #define DATA0CH0_CR_DCCCTL2_mdll_dcdenable_MSK                       (0x00010000)
  #define DATA0CH0_CR_DCCCTL2_mdll_dcdenable_MIN                       (0)
  #define DATA0CH0_CR_DCCCTL2_mdll_dcdenable_MAX                       (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL2_mdll_dcdenable_DEF                       (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_mdll_dcdenable_HSH                       (0x01200188)

  #define DATA0CH0_CR_DCCCTL2_Reserved21_OFF                           (17)
  #define DATA0CH0_CR_DCCCTL2_Reserved21_WID                           ( 3)
  #define DATA0CH0_CR_DCCCTL2_Reserved21_MSK                           (0x000E0000)
  #define DATA0CH0_CR_DCCCTL2_Reserved21_MIN                           (0)
  #define DATA0CH0_CR_DCCCTL2_Reserved21_MAX                           (7) // 0x00000007
  #define DATA0CH0_CR_DCCCTL2_Reserved21_DEF                           (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_Reserved21_HSH                           (0x03220188)

  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountstart_OFF             (20)
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountstart_WID             ( 1)
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountstart_MSK             (0x00100000)
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountstart_MIN             (0)
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountstart_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountstart_DEF             (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountstart_HSH             (0x01280188)

  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountrst_b_OFF             (21)
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountrst_b_WID             ( 1)
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountrst_b_MSK             (0x00200000)
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountrst_b_MIN             (0)
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountrst_b_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountrst_b_DEF             (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountrst_b_HSH             (0x012A0188)

  #define DATA0CH0_CR_DCCCTL2_dccfsm_rst_b_OFF                         (22)
  #define DATA0CH0_CR_DCCCTL2_dccfsm_rst_b_WID                         ( 1)
  #define DATA0CH0_CR_DCCCTL2_dccfsm_rst_b_MSK                         (0x00400000)
  #define DATA0CH0_CR_DCCCTL2_dccfsm_rst_b_MIN                         (0)
  #define DATA0CH0_CR_DCCCTL2_dccfsm_rst_b_MAX                         (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL2_dccfsm_rst_b_DEF                         (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_dccfsm_rst_b_HSH                         (0x012C0188)

  #define DATA0CH0_CR_DCCCTL2_compare_dly_OFF                          (23)
  #define DATA0CH0_CR_DCCCTL2_compare_dly_WID                          ( 4)
  #define DATA0CH0_CR_DCCCTL2_compare_dly_MSK                          (0x07800000)
  #define DATA0CH0_CR_DCCCTL2_compare_dly_MIN                          (0)
  #define DATA0CH0_CR_DCCCTL2_compare_dly_MAX                          (15) // 0x0000000F
  #define DATA0CH0_CR_DCCCTL2_compare_dly_DEF                          (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_compare_dly_HSH                          (0x042E0188)

  #define DATA0CH0_CR_DCCCTL2_dcdreset_dly_OFF                         (27)
  #define DATA0CH0_CR_DCCCTL2_dcdreset_dly_WID                         ( 3)
  #define DATA0CH0_CR_DCCCTL2_dcdreset_dly_MSK                         (0x38000000)
  #define DATA0CH0_CR_DCCCTL2_dcdreset_dly_MIN                         (0)
  #define DATA0CH0_CR_DCCCTL2_dcdreset_dly_MAX                         (7) // 0x00000007
  #define DATA0CH0_CR_DCCCTL2_dcdreset_dly_DEF                         (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_dcdreset_dly_HSH                         (0x03360188)

  #define DATA0CH0_CR_DCCCTL2_clknclkpsample_avg_en_OFF                (30)
  #define DATA0CH0_CR_DCCCTL2_clknclkpsample_avg_en_WID                ( 1)
  #define DATA0CH0_CR_DCCCTL2_clknclkpsample_avg_en_MSK                (0x40000000)
  #define DATA0CH0_CR_DCCCTL2_clknclkpsample_avg_en_MIN                (0)
  #define DATA0CH0_CR_DCCCTL2_clknclkpsample_avg_en_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL2_clknclkpsample_avg_en_DEF                (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_clknclkpsample_avg_en_HSH                (0x013C0188)

  #define DATA0CH0_CR_DCCCTL2_threeloop_en_OFF                         (31)
  #define DATA0CH0_CR_DCCCTL2_threeloop_en_WID                         ( 1)
  #define DATA0CH0_CR_DCCCTL2_threeloop_en_MSK                         (0x80000000)
  #define DATA0CH0_CR_DCCCTL2_threeloop_en_MIN                         (0)
  #define DATA0CH0_CR_DCCCTL2_threeloop_en_MAX                         (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL2_threeloop_en_DEF                         (0x00000000)
  #define DATA0CH0_CR_DCCCTL2_threeloop_en_HSH                         (0x013E0188)

#define DATA0CH0_CR_DCCCTL3_REG                                        (0x0000018C)

  #define DATA0CH0_CR_DCCCTL3_dcdrocal_high_freq_marker_OFF            ( 0)
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_high_freq_marker_WID            (16)
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_high_freq_marker_MSK            (0x0000FFFF)
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_high_freq_marker_MIN            (0)
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_high_freq_marker_MAX            (65535) // 0x0000FFFF
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_high_freq_marker_DEF            (0x00000000)
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_high_freq_marker_HSH            (0x1000018C)

  #define DATA0CH0_CR_DCCCTL3_dcdrocal_low_freq_marker_OFF             (16)
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_low_freq_marker_WID             (16)
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_low_freq_marker_MSK             (0xFFFF0000)
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_low_freq_marker_MIN             (0)
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_low_freq_marker_MAX             (65535) // 0x0000FFFF
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_low_freq_marker_DEF             (0x00000000)
  #define DATA0CH0_CR_DCCCTL3_dcdrocal_low_freq_marker_HSH             (0x1020018C)

#define DATA0CH0_CR_DCCCTL10_REG                                       (0x00000190)

  #define DATA0CH0_CR_DCCCTL10_totalrocalsamplecounts_OFF              ( 0)
  #define DATA0CH0_CR_DCCCTL10_totalrocalsamplecounts_WID              (16)
  #define DATA0CH0_CR_DCCCTL10_totalrocalsamplecounts_MSK              (0x0000FFFF)
  #define DATA0CH0_CR_DCCCTL10_totalrocalsamplecounts_MIN              (0)
  #define DATA0CH0_CR_DCCCTL10_totalrocalsamplecounts_MAX              (65535) // 0x0000FFFF
  #define DATA0CH0_CR_DCCCTL10_totalrocalsamplecounts_DEF              (0x00000000)
  #define DATA0CH0_CR_DCCCTL10_totalrocalsamplecounts_HSH              (0x10000190)

  #define DATA0CH0_CR_DCCCTL10_dcdsamplesel_OFF                        (16)
  #define DATA0CH0_CR_DCCCTL10_dcdsamplesel_WID                        ( 4)
  #define DATA0CH0_CR_DCCCTL10_dcdsamplesel_MSK                        (0x000F0000)
  #define DATA0CH0_CR_DCCCTL10_dcdsamplesel_MIN                        (0)
  #define DATA0CH0_CR_DCCCTL10_dcdsamplesel_MAX                        (15) // 0x0000000F
  #define DATA0CH0_CR_DCCCTL10_dcdsamplesel_DEF                        (0x00000000)
  #define DATA0CH0_CR_DCCCTL10_dcdsamplesel_HSH                        (0x04200190)

  #define DATA0CH0_CR_DCCCTL10_step1pattern_OFF                        (20)
  #define DATA0CH0_CR_DCCCTL10_step1pattern_WID                        ( 4)
  #define DATA0CH0_CR_DCCCTL10_step1pattern_MSK                        (0x00F00000)
  #define DATA0CH0_CR_DCCCTL10_step1pattern_MIN                        (0)
  #define DATA0CH0_CR_DCCCTL10_step1pattern_MAX                        (15) // 0x0000000F
  #define DATA0CH0_CR_DCCCTL10_step1pattern_DEF                        (0x00000000)
  #define DATA0CH0_CR_DCCCTL10_step1pattern_HSH                        (0x04280190)

  #define DATA0CH0_CR_DCCCTL10_step2pattern_OFF                        (24)
  #define DATA0CH0_CR_DCCCTL10_step2pattern_WID                        ( 4)
  #define DATA0CH0_CR_DCCCTL10_step2pattern_MSK                        (0x0F000000)
  #define DATA0CH0_CR_DCCCTL10_step2pattern_MIN                        (0)
  #define DATA0CH0_CR_DCCCTL10_step2pattern_MAX                        (15) // 0x0000000F
  #define DATA0CH0_CR_DCCCTL10_step2pattern_DEF                        (0x00000000)
  #define DATA0CH0_CR_DCCCTL10_step2pattern_HSH                        (0x04300190)

  #define DATA0CH0_CR_DCCCTL10_step3pattern_OFF                        (28)
  #define DATA0CH0_CR_DCCCTL10_step3pattern_WID                        ( 4)
  #define DATA0CH0_CR_DCCCTL10_step3pattern_MSK                        (0xF0000000)
  #define DATA0CH0_CR_DCCCTL10_step3pattern_MIN                        (0)
  #define DATA0CH0_CR_DCCCTL10_step3pattern_MAX                        (15) // 0x0000000F
  #define DATA0CH0_CR_DCCCTL10_step3pattern_DEF                        (0x00000000)
  #define DATA0CH0_CR_DCCCTL10_step3pattern_HSH                        (0x04380190)

#define DATA0CH0_CR_DCCCTL12_REG                                       (0x00000194)

  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0overflow_OFF              ( 0)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0overflow_WID              (10)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0overflow_MSK              (0x000003FF)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0overflow_MIN              (0)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0overflow_MAX              (1023) // 0x000003FF
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0overflow_DEF              (0x00000000)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0overflow_HSH              (0x0A000194)

  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph90overflow_OFF             (10)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph90overflow_WID             (10)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph90overflow_MSK             (0x000FFC00)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph90overflow_MIN             (0)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph90overflow_MAX             (1023) // 0x000003FF
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph90overflow_DEF             (0x00000000)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph90overflow_HSH             (0x0A140194)

  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0_status_OFF               (20)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0_status_WID               ( 8)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0_status_MSK               (0x0FF00000)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0_status_MIN               (0)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0_status_MAX               (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0_status_DEF               (0x00000000)
  #define DATA0CH0_CR_DCCCTL12_dccctrlcodeph0_status_HSH               (0x08280194)

  #define DATA0CH0_CR_DCCCTL12_scr_fsm_dccctrlcodestatus_sel_OFF       (28)
  #define DATA0CH0_CR_DCCCTL12_scr_fsm_dccctrlcodestatus_sel_WID       ( 4)
  #define DATA0CH0_CR_DCCCTL12_scr_fsm_dccctrlcodestatus_sel_MSK       (0xF0000000)
  #define DATA0CH0_CR_DCCCTL12_scr_fsm_dccctrlcodestatus_sel_MIN       (0)
  #define DATA0CH0_CR_DCCCTL12_scr_fsm_dccctrlcodestatus_sel_MAX       (15) // 0x0000000F
  #define DATA0CH0_CR_DCCCTL12_scr_fsm_dccctrlcodestatus_sel_DEF       (0x00000000)
  #define DATA0CH0_CR_DCCCTL12_scr_fsm_dccctrlcodestatus_sel_HSH       (0x04380194)

#define DATA0CH0_CR_CLKALIGNCTL0_REG                                   (0x00000198)

  #define DATA0CH0_CR_CLKALIGNCTL0_maxsamplecnt_OFF                    ( 0)
  #define DATA0CH0_CR_CLKALIGNCTL0_maxsamplecnt_WID                    ( 5)
  #define DATA0CH0_CR_CLKALIGNCTL0_maxsamplecnt_MSK                    (0x0000001F)
  #define DATA0CH0_CR_CLKALIGNCTL0_maxsamplecnt_MIN                    (0)
  #define DATA0CH0_CR_CLKALIGNCTL0_maxsamplecnt_MAX                    (31) // 0x0000001F
  #define DATA0CH0_CR_CLKALIGNCTL0_maxsamplecnt_DEF                    (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_maxsamplecnt_HSH                    (0x05000198)

  #define DATA0CH0_CR_CLKALIGNCTL0_samplesize_OFF                      ( 5)
  #define DATA0CH0_CR_CLKALIGNCTL0_samplesize_WID                      ( 5)
  #define DATA0CH0_CR_CLKALIGNCTL0_samplesize_MSK                      (0x000003E0)
  #define DATA0CH0_CR_CLKALIGNCTL0_samplesize_MIN                      (0)
  #define DATA0CH0_CR_CLKALIGNCTL0_samplesize_MAX                      (31) // 0x0000001F
  #define DATA0CH0_CR_CLKALIGNCTL0_samplesize_DEF                      (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_samplesize_HSH                      (0x050A0198)

  #define DATA0CH0_CR_CLKALIGNCTL0_pimicrosteps_OFF                    (10)
  #define DATA0CH0_CR_CLKALIGNCTL0_pimicrosteps_WID                    ( 2)
  #define DATA0CH0_CR_CLKALIGNCTL0_pimicrosteps_MSK                    (0x00000C00)
  #define DATA0CH0_CR_CLKALIGNCTL0_pimicrosteps_MIN                    (0)
  #define DATA0CH0_CR_CLKALIGNCTL0_pimicrosteps_MAX                    (3) // 0x00000003
  #define DATA0CH0_CR_CLKALIGNCTL0_pimicrosteps_DEF                    (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_pimicrosteps_HSH                    (0x02140198)

  #define DATA0CH0_CR_CLKALIGNCTL0_pimacrosteps_OFF                    (12)
  #define DATA0CH0_CR_CLKALIGNCTL0_pimacrosteps_WID                    ( 4)
  #define DATA0CH0_CR_CLKALIGNCTL0_pimacrosteps_MSK                    (0x0000F000)
  #define DATA0CH0_CR_CLKALIGNCTL0_pimacrosteps_MIN                    (0)
  #define DATA0CH0_CR_CLKALIGNCTL0_pimacrosteps_MAX                    (15) // 0x0000000F
  #define DATA0CH0_CR_CLKALIGNCTL0_pimacrosteps_DEF                    (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_pimacrosteps_HSH                    (0x04180198)

  #define DATA0CH0_CR_CLKALIGNCTL0_dllwait_dly_OFF                     (16)
  #define DATA0CH0_CR_CLKALIGNCTL0_dllwait_dly_WID                     ( 4)
  #define DATA0CH0_CR_CLKALIGNCTL0_dllwait_dly_MSK                     (0x000F0000)
  #define DATA0CH0_CR_CLKALIGNCTL0_dllwait_dly_MIN                     (0)
  #define DATA0CH0_CR_CLKALIGNCTL0_dllwait_dly_MAX                     (15) // 0x0000000F
  #define DATA0CH0_CR_CLKALIGNCTL0_dllwait_dly_DEF                     (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_dllwait_dly_HSH                     (0x04200198)

  #define DATA0CH0_CR_CLKALIGNCTL0_swd0piclkpicodesel_OFF              (20)
  #define DATA0CH0_CR_CLKALIGNCTL0_swd0piclkpicodesel_WID              ( 1)
  #define DATA0CH0_CR_CLKALIGNCTL0_swd0piclkpicodesel_MSK              (0x00100000)
  #define DATA0CH0_CR_CLKALIGNCTL0_swd0piclkpicodesel_MIN              (0)
  #define DATA0CH0_CR_CLKALIGNCTL0_swd0piclkpicodesel_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNCTL0_swd0piclkpicodesel_DEF              (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_swd0piclkpicodesel_HSH              (0x01280198)

  #define DATA0CH0_CR_CLKALIGNCTL0_swsample_en_OFF                     (21)
  #define DATA0CH0_CR_CLKALIGNCTL0_swsample_en_WID                     ( 1)
  #define DATA0CH0_CR_CLKALIGNCTL0_swsample_en_MSK                     (0x00200000)
  #define DATA0CH0_CR_CLKALIGNCTL0_swsample_en_MIN                     (0)
  #define DATA0CH0_CR_CLKALIGNCTL0_swsample_en_MAX                     (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNCTL0_swsample_en_DEF                     (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_swsample_en_HSH                     (0x012A0198)

  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignd0piclkpistep_maxreloopcnt_OFF (22)
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignd0piclkpistep_maxreloopcnt_WID ( 8)
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignd0piclkpistep_maxreloopcnt_MSK (0x3FC00000)
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignd0piclkpistep_maxreloopcnt_MIN (0)
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignd0piclkpistep_maxreloopcnt_MAX (255) // 0x000000FF
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignd0piclkpistep_maxreloopcnt_DEF (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignd0piclkpistep_maxreloopcnt_HSH (0x082C0198)

  #define DATA0CH0_CR_CLKALIGNCTL0_start_clkalign_OFF                  (30)
  #define DATA0CH0_CR_CLKALIGNCTL0_start_clkalign_WID                  ( 1)
  #define DATA0CH0_CR_CLKALIGNCTL0_start_clkalign_MSK                  (0x40000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_start_clkalign_MIN                  (0)
  #define DATA0CH0_CR_CLKALIGNCTL0_start_clkalign_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNCTL0_start_clkalign_DEF                  (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_start_clkalign_HSH                  (0x013C0198)

  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignrst_b_OFF                   (31)
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignrst_b_WID                   ( 1)
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignrst_b_MSK                   (0x80000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignrst_b_MIN                   (0)
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignrst_b_MAX                   (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignrst_b_DEF                   (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL0_clkalignrst_b_HSH                   (0x013E0198)

#define DATA0CH0_CR_CLKALIGNCTL1_REG                                   (0x0000019C)

  #define DATA0CH0_CR_CLKALIGNCTL1_swd0piclkpicode_OFF                 ( 0)
  #define DATA0CH0_CR_CLKALIGNCTL1_swd0piclkpicode_WID                 ( 8)
  #define DATA0CH0_CR_CLKALIGNCTL1_swd0piclkpicode_MSK                 (0x000000FF)
  #define DATA0CH0_CR_CLKALIGNCTL1_swd0piclkpicode_MIN                 (0)
  #define DATA0CH0_CR_CLKALIGNCTL1_swd0piclkpicode_MAX                 (255) // 0x000000FF
  #define DATA0CH0_CR_CLKALIGNCTL1_swd0piclkpicode_DEF                 (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL1_swd0piclkpicode_HSH                 (0x0800019C)

  #define DATA0CH0_CR_CLKALIGNCTL1_clkalignd0piclkpicodeoffset_OFF     ( 8)
  #define DATA0CH0_CR_CLKALIGNCTL1_clkalignd0piclkpicodeoffset_WID     ( 8)
  #define DATA0CH0_CR_CLKALIGNCTL1_clkalignd0piclkpicodeoffset_MSK     (0x0000FF00)
  #define DATA0CH0_CR_CLKALIGNCTL1_clkalignd0piclkpicodeoffset_MIN     (0)
  #define DATA0CH0_CR_CLKALIGNCTL1_clkalignd0piclkpicodeoffset_MAX     (255) // 0x000000FF
  #define DATA0CH0_CR_CLKALIGNCTL1_clkalignd0piclkpicodeoffset_DEF     (0x00000004)
  #define DATA0CH0_CR_CLKALIGNCTL1_clkalignd0piclkpicodeoffset_HSH     (0x0810019C)

  #define DATA0CH0_CR_CLKALIGNCTL1_clkaligninitiald0piclkpicode_OFF    (16)
  #define DATA0CH0_CR_CLKALIGNCTL1_clkaligninitiald0piclkpicode_WID    ( 8)
  #define DATA0CH0_CR_CLKALIGNCTL1_clkaligninitiald0piclkpicode_MSK    (0x00FF0000)
  #define DATA0CH0_CR_CLKALIGNCTL1_clkaligninitiald0piclkpicode_MIN    (0)
  #define DATA0CH0_CR_CLKALIGNCTL1_clkaligninitiald0piclkpicode_MAX    (255) // 0x000000FF
  #define DATA0CH0_CR_CLKALIGNCTL1_clkaligninitiald0piclkpicode_DEF    (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL1_clkaligninitiald0piclkpicode_HSH    (0x0820019C)

  #define DATA0CH0_CR_CLKALIGNCTL1_q2d0align_code_OFF                  (24)
  #define DATA0CH0_CR_CLKALIGNCTL1_q2d0align_code_WID                  ( 8)
  #define DATA0CH0_CR_CLKALIGNCTL1_q2d0align_code_MSK                  (0xFF000000)
  #define DATA0CH0_CR_CLKALIGNCTL1_q2d0align_code_MIN                  (0)
  #define DATA0CH0_CR_CLKALIGNCTL1_q2d0align_code_MAX                  (255) // 0x000000FF
  #define DATA0CH0_CR_CLKALIGNCTL1_q2d0align_code_DEF                  (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL1_q2d0align_code_HSH                  (0x0830019C)

#define DATA0CH0_CR_CLKALIGNCTL2_REG                                   (0x000001A0)

  #define DATA0CH0_CR_CLKALIGNCTL2_pdoffset_ovr_OFF                    ( 0)
  #define DATA0CH0_CR_CLKALIGNCTL2_pdoffset_ovr_WID                    ( 1)
  #define DATA0CH0_CR_CLKALIGNCTL2_pdoffset_ovr_MSK                    (0x00000001)
  #define DATA0CH0_CR_CLKALIGNCTL2_pdoffset_ovr_MIN                    (0)
  #define DATA0CH0_CR_CLKALIGNCTL2_pdoffset_ovr_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNCTL2_pdoffset_ovr_DEF                    (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL2_pdoffset_ovr_HSH                    (0x010001A0)

  #define DATA0CH0_CR_CLKALIGNCTL2_txdll_dqd0_pdoffset_OFF             ( 1)
  #define DATA0CH0_CR_CLKALIGNCTL2_txdll_dqd0_pdoffset_WID             ( 8)
  #define DATA0CH0_CR_CLKALIGNCTL2_txdll_dqd0_pdoffset_MSK             (0x000001FE)
  #define DATA0CH0_CR_CLKALIGNCTL2_txdll_dqd0_pdoffset_MIN             (0)
  #define DATA0CH0_CR_CLKALIGNCTL2_txdll_dqd0_pdoffset_MAX             (255) // 0x000000FF
  #define DATA0CH0_CR_CLKALIGNCTL2_txdll_dqd0_pdoffset_DEF             (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL2_txdll_dqd0_pdoffset_HSH             (0x080201A0)

  #define DATA0CH0_CR_CLKALIGNCTL2_d0tosig_offset_OFF                  ( 9)
  #define DATA0CH0_CR_CLKALIGNCTL2_d0tosig_offset_WID                  ( 8)
  #define DATA0CH0_CR_CLKALIGNCTL2_d0tosig_offset_MSK                  (0x0001FE00)
  #define DATA0CH0_CR_CLKALIGNCTL2_d0tosig_offset_MIN                  (0)
  #define DATA0CH0_CR_CLKALIGNCTL2_d0tosig_offset_MAX                  (255) // 0x000000FF
  #define DATA0CH0_CR_CLKALIGNCTL2_d0tosig_offset_DEF                  (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL2_d0tosig_offset_HSH                  (0x081201A0)

  #define DATA0CH0_CR_CLKALIGNCTL2_Reserved22_OFF                      (17)
  #define DATA0CH0_CR_CLKALIGNCTL2_Reserved22_WID                      (15)
  #define DATA0CH0_CR_CLKALIGNCTL2_Reserved22_MSK                      (0xFFFE0000)
  #define DATA0CH0_CR_CLKALIGNCTL2_Reserved22_MIN                      (0)
  #define DATA0CH0_CR_CLKALIGNCTL2_Reserved22_MAX                      (32767) // 0x00007FFF
  #define DATA0CH0_CR_CLKALIGNCTL2_Reserved22_DEF                      (0x00000000)
  #define DATA0CH0_CR_CLKALIGNCTL2_Reserved22_HSH                      (0x0F2201A0)

#define DATA0CH0_CR_DCCCTL4_REG                                        (0x000001A4)

  #define DATA0CH0_CR_DCCCTL4_dcc_bits_en_OFF                          ( 0)
  #define DATA0CH0_CR_DCCCTL4_dcc_bits_en_WID                          (10)
  #define DATA0CH0_CR_DCCCTL4_dcc_bits_en_MSK                          (0x000003FF)
  #define DATA0CH0_CR_DCCCTL4_dcc_bits_en_MIN                          (0)
  #define DATA0CH0_CR_DCCCTL4_dcc_bits_en_MAX                          (1023) // 0x000003FF
  #define DATA0CH0_CR_DCCCTL4_dcc_bits_en_DEF                          (0x00000000)
  #define DATA0CH0_CR_DCCCTL4_dcc_bits_en_HSH                          (0x0A0001A4)

  #define DATA0CH0_CR_DCCCTL4_dcc_step2pattern_enable_OFF              (10)
  #define DATA0CH0_CR_DCCCTL4_dcc_step2pattern_enable_WID              (10)
  #define DATA0CH0_CR_DCCCTL4_dcc_step2pattern_enable_MSK              (0x000FFC00)
  #define DATA0CH0_CR_DCCCTL4_dcc_step2pattern_enable_MIN              (0)
  #define DATA0CH0_CR_DCCCTL4_dcc_step2pattern_enable_MAX              (1023) // 0x000003FF
  #define DATA0CH0_CR_DCCCTL4_dcc_step2pattern_enable_DEF              (0x00000000)
  #define DATA0CH0_CR_DCCCTL4_dcc_step2pattern_enable_HSH              (0x0A1401A4)

  #define DATA0CH0_CR_DCCCTL4_dcc_step3pattern_enable_OFF              (20)
  #define DATA0CH0_CR_DCCCTL4_dcc_step3pattern_enable_WID              (10)
  #define DATA0CH0_CR_DCCCTL4_dcc_step3pattern_enable_MSK              (0x3FF00000)
  #define DATA0CH0_CR_DCCCTL4_dcc_step3pattern_enable_MIN              (0)
  #define DATA0CH0_CR_DCCCTL4_dcc_step3pattern_enable_MAX              (1023) // 0x000003FF
  #define DATA0CH0_CR_DCCCTL4_dcc_step3pattern_enable_DEF              (0x00000000)
  #define DATA0CH0_CR_DCCCTL4_dcc_step3pattern_enable_HSH              (0x0A2801A4)

  #define DATA0CH0_CR_DCCCTL4_dcc_serdata_ovren_OFF                    (30)
  #define DATA0CH0_CR_DCCCTL4_dcc_serdata_ovren_WID                    ( 1)
  #define DATA0CH0_CR_DCCCTL4_dcc_serdata_ovren_MSK                    (0x40000000)
  #define DATA0CH0_CR_DCCCTL4_dcc_serdata_ovren_MIN                    (0)
  #define DATA0CH0_CR_DCCCTL4_dcc_serdata_ovren_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL4_dcc_serdata_ovren_DEF                    (0x00000000)
  #define DATA0CH0_CR_DCCCTL4_dcc_serdata_ovren_HSH                    (0x013C01A4)

  #define DATA0CH0_CR_DCCCTL4_Reserved23_OFF                           (31)
  #define DATA0CH0_CR_DCCCTL4_Reserved23_WID                           ( 1)
  #define DATA0CH0_CR_DCCCTL4_Reserved23_MSK                           (0x80000000)
  #define DATA0CH0_CR_DCCCTL4_Reserved23_MIN                           (0)
  #define DATA0CH0_CR_DCCCTL4_Reserved23_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL4_Reserved23_DEF                           (0x00000000)
  #define DATA0CH0_CR_DCCCTL4_Reserved23_HSH                           (0x013E01A4)

#define DATA0CH0_CR_DDRCRDATACOMPVTT_REG                               (0x000001A8)

  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Vsshiff_rx_offset_OFF           ( 0)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Vsshiff_rx_offset_WID           ( 4)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Vsshiff_rx_offset_MSK           (0x0000000F)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Vsshiff_rx_offset_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Vsshiff_rx_offset_MAX           (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Vsshiff_rx_offset_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Vsshiff_rx_offset_HSH           (0x040001A8)

  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_coderead_offset_OFF         ( 4)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_coderead_offset_WID         ( 4)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_coderead_offset_MSK         (0x000000F0)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_coderead_offset_MIN         (0)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_coderead_offset_MAX         (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_coderead_offset_DEF         (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_coderead_offset_HSH         (0x040801A8)

  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkn_offset_OFF          ( 8)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkn_offset_WID          ( 4)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkn_offset_MSK          (0x00000F00)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkn_offset_MIN          (0)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkn_offset_MAX          (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkn_offset_DEF          (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkn_offset_HSH          (0x041001A8)

  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkp_offset_OFF          (12)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkp_offset_WID          ( 4)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkp_offset_MSK          (0x0000F000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkp_offset_MIN          (0)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkp_offset_MAX          (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkp_offset_DEF          (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dqs_fwdclkp_offset_HSH          (0x041801A8)

  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_vdlllock_offset_OFF         (16)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_vdlllock_offset_WID         ( 4)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_vdlllock_offset_MSK         (0x000F0000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_vdlllock_offset_MIN         (0)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_vdlllock_offset_MAX         (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_vdlllock_offset_DEF         (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_vdlllock_offset_HSH         (0x042001A8)

  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codepi_offset_OFF           (20)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codepi_offset_WID           ( 4)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codepi_offset_MSK           (0x00F00000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codepi_offset_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codepi_offset_MAX           (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codepi_offset_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codepi_offset_HSH           (0x042801A8)

  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codewl_offset_OFF           (24)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codewl_offset_WID           ( 4)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codewl_offset_MSK           (0x0F000000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codewl_offset_MIN           (0)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codewl_offset_MAX           (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codewl_offset_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_codewl_offset_HSH           (0x043001A8)

  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_bwsel_offset_OFF            (28)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_bwsel_offset_WID            ( 4)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_bwsel_offset_MSK            (0xF0000000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_bwsel_offset_MIN            (0)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_bwsel_offset_MAX            (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_bwsel_offset_DEF            (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMPVTT_Dll_bwsel_offset_HSH            (0x043801A8)

#define DATA0CH0_CR_WLCTRL_REG                                         (0x000001AC)

  #define DATA0CH0_CR_WLCTRL_initdlllock_dly_OFF                       ( 0)
  #define DATA0CH0_CR_WLCTRL_initdlllock_dly_WID                       ( 4)
  #define DATA0CH0_CR_WLCTRL_initdlllock_dly_MSK                       (0x0000000F)
  #define DATA0CH0_CR_WLCTRL_initdlllock_dly_MIN                       (0)
  #define DATA0CH0_CR_WLCTRL_initdlllock_dly_MAX                       (15) // 0x0000000F
  #define DATA0CH0_CR_WLCTRL_initdlllock_dly_DEF                       (0x00000004)
  #define DATA0CH0_CR_WLCTRL_initdlllock_dly_HSH                       (0x040001AC)

  #define DATA0CH0_CR_WLCTRL_wlrelock_dly_OFF                          ( 4)
  #define DATA0CH0_CR_WLCTRL_wlrelock_dly_WID                          ( 4)
  #define DATA0CH0_CR_WLCTRL_wlrelock_dly_MSK                          (0x000000F0)
  #define DATA0CH0_CR_WLCTRL_wlrelock_dly_MIN                          (0)
  #define DATA0CH0_CR_WLCTRL_wlrelock_dly_MAX                          (15) // 0x0000000F
  #define DATA0CH0_CR_WLCTRL_wlrelock_dly_DEF                          (0x00000004)
  #define DATA0CH0_CR_WLCTRL_wlrelock_dly_HSH                          (0x040801AC)

  #define DATA0CH0_CR_WLCTRL_wlslowclken_dly_OFF                       ( 0)
  #define DATA0CH0_CR_WLCTRL_wlslowclken_dly_WID                       ( 5)
  #define DATA0CH0_CR_WLCTRL_wlslowclken_dly_MSK                       (0x0000001F)
  #define DATA0CH0_CR_WLCTRL_wlslowclken_dly_MIN                       (0)
  #define DATA0CH0_CR_WLCTRL_wlslowclken_dly_MAX                       (31) // 0x0000001F
  #define DATA0CH0_CR_WLCTRL_wlslowclken_dly_DEF                       (0x00000004)
  #define DATA0CH0_CR_WLCTRL_wlslowclken_dly_HSH                       (0x050001AC)

  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_Q0_OFF                        ( 8)
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_Q0_WID                        ( 5)
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_Q0_MSK                        (0x00001F00)
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_Q0_MIN                        (0)
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_Q0_MAX                        (31) // 0x0000001F
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_Q0_DEF                        (0x0000000C)
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_Q0_HSH                        (0x051001AC)

  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_OFF                        ( 5)
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_WID                        ( 5)
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_MSK                        (0x000003E0)
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_MIN                        (0)
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_MAX                        (31) // 0x0000001F
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_DEF                        (0x0000000C)
  #define DATA0CH0_CR_WLCTRL_weaklocken_dly_HSH                        (0x050A01AC)

  #define DATA0CH0_CR_WLCTRL_spare_OFF                                 (13)
  #define DATA0CH0_CR_WLCTRL_spare_WID                                 ( 1)
  #define DATA0CH0_CR_WLCTRL_spare_MSK                                 (0x00002000)
  #define DATA0CH0_CR_WLCTRL_spare_MIN                                 (0)
  #define DATA0CH0_CR_WLCTRL_spare_MAX                                 (1) // 0x00000001
  #define DATA0CH0_CR_WLCTRL_spare_DEF                                 (0x00000000)
  #define DATA0CH0_CR_WLCTRL_spare_HSH                                 (0x011A01AC)

  #define DATA0CH0_CR_WLCTRL_lp_weaklocken_OFF                         (14)
  #define DATA0CH0_CR_WLCTRL_lp_weaklocken_WID                         ( 1)
  #define DATA0CH0_CR_WLCTRL_lp_weaklocken_MSK                         (0x00004000)
  #define DATA0CH0_CR_WLCTRL_lp_weaklocken_MIN                         (0)
  #define DATA0CH0_CR_WLCTRL_lp_weaklocken_MAX                         (1) // 0x00000001
  #define DATA0CH0_CR_WLCTRL_lp_weaklocken_DEF                         (0x00000000)
  #define DATA0CH0_CR_WLCTRL_lp_weaklocken_HSH                         (0x011C01AC)

  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_sel_OFF                    (15)
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_sel_WID                    ( 1)
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_sel_MSK                    (0x00008000)
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_sel_MIN                    (0)
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_sel_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_sel_DEF                    (0x00000000)
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_sel_HSH                    (0x011E01AC)

  #define DATA0CH0_CR_WLCTRL_wlcomp_samplewait_OFF                     (16)
  #define DATA0CH0_CR_WLCTRL_wlcomp_samplewait_WID                     ( 5)
  #define DATA0CH0_CR_WLCTRL_wlcomp_samplewait_MSK                     (0x001F0000)
  #define DATA0CH0_CR_WLCTRL_wlcomp_samplewait_MIN                     (0)
  #define DATA0CH0_CR_WLCTRL_wlcomp_samplewait_MAX                     (31) // 0x0000001F
  #define DATA0CH0_CR_WLCTRL_wlcomp_samplewait_DEF                     (0x00000006)
  #define DATA0CH0_CR_WLCTRL_wlcomp_samplewait_HSH                     (0x052001AC)

  #define DATA0CH0_CR_WLCTRL_wlcomp_per_stepsize_OFF                   (21)
  #define DATA0CH0_CR_WLCTRL_wlcomp_per_stepsize_WID                   ( 3)
  #define DATA0CH0_CR_WLCTRL_wlcomp_per_stepsize_MSK                   (0x00E00000)
  #define DATA0CH0_CR_WLCTRL_wlcomp_per_stepsize_MIN                   (0)
  #define DATA0CH0_CR_WLCTRL_wlcomp_per_stepsize_MAX                   (7) // 0x00000007
  #define DATA0CH0_CR_WLCTRL_wlcomp_per_stepsize_DEF                   (0x00000003)
  #define DATA0CH0_CR_WLCTRL_wlcomp_per_stepsize_HSH                   (0x032A01AC)

  #define DATA0CH0_CR_WLCTRL_wlcomp_init_stepsize_OFF                  (24)
  #define DATA0CH0_CR_WLCTRL_wlcomp_init_stepsize_WID                  ( 3)
  #define DATA0CH0_CR_WLCTRL_wlcomp_init_stepsize_MSK                  (0x07000000)
  #define DATA0CH0_CR_WLCTRL_wlcomp_init_stepsize_MIN                  (0)
  #define DATA0CH0_CR_WLCTRL_wlcomp_init_stepsize_MAX                  (7) // 0x00000007
  #define DATA0CH0_CR_WLCTRL_wlcomp_init_stepsize_DEF                  (0x00000001)
  #define DATA0CH0_CR_WLCTRL_wlcomp_init_stepsize_HSH                  (0x033001AC)

  #define DATA0CH0_CR_WLCTRL_weaklockcomp_err_OFF                      (27)
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_err_WID                      ( 1)
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_err_MSK                      (0x08000000)
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_err_MIN                      (0)
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_err_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_err_DEF                      (0x00000000)
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_err_HSH                      (0x013601AC)

  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_OFF                        (28)
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_WID                        ( 1)
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_MSK                        (0x10000000)
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_MIN                        (0)
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_MAX                        (1) // 0x00000001
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_DEF                        (0x00000000)
  #define DATA0CH0_CR_WLCTRL_weaklocken_ovr_HSH                        (0x013801AC)

  #define DATA0CH0_CR_WLCTRL_wlcomp_clkgatedis_OFF                     (29)
  #define DATA0CH0_CR_WLCTRL_wlcomp_clkgatedis_WID                     ( 1)
  #define DATA0CH0_CR_WLCTRL_wlcomp_clkgatedis_MSK                     (0x20000000)
  #define DATA0CH0_CR_WLCTRL_wlcomp_clkgatedis_MIN                     (0)
  #define DATA0CH0_CR_WLCTRL_wlcomp_clkgatedis_MAX                     (1) // 0x00000001
  #define DATA0CH0_CR_WLCTRL_wlcomp_clkgatedis_DEF                     (0x00000000)
  #define DATA0CH0_CR_WLCTRL_wlcomp_clkgatedis_HSH                     (0x013A01AC)

  #define DATA0CH0_CR_WLCTRL_weaklocken_OFF                            (30)
  #define DATA0CH0_CR_WLCTRL_weaklocken_WID                            ( 1)
  #define DATA0CH0_CR_WLCTRL_weaklocken_MSK                            (0x40000000)
  #define DATA0CH0_CR_WLCTRL_weaklocken_MIN                            (0)
  #define DATA0CH0_CR_WLCTRL_weaklocken_MAX                            (1) // 0x00000001
  #define DATA0CH0_CR_WLCTRL_weaklocken_DEF                            (0x00000000)
  #define DATA0CH0_CR_WLCTRL_weaklocken_HSH                            (0x013C01AC)

  #define DATA0CH0_CR_WLCTRL_weaklockcomp_en_OFF                       (31)
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_en_WID                       ( 1)
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_en_MSK                       (0x80000000)
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_en_MIN                       (0)
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_en_MAX                       (1) // 0x00000001
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_en_DEF                       (0x00000000)
  #define DATA0CH0_CR_WLCTRL_weaklockcomp_en_HSH                       (0x013E01AC)

#define DATA0CH0_CR_LPMODECTL_REG                                      (0x000001B0)

  #define DATA0CH0_CR_LPMODECTL_spare_OFF                              ( 0)
  #define DATA0CH0_CR_LPMODECTL_spare_WID                              ( 4)
  #define DATA0CH0_CR_LPMODECTL_spare_MSK                              (0x0000000F)
  #define DATA0CH0_CR_LPMODECTL_spare_MIN                              (0)
  #define DATA0CH0_CR_LPMODECTL_spare_MAX                              (15) // 0x0000000F
  #define DATA0CH0_CR_LPMODECTL_spare_DEF                              (0x00000000)
  #define DATA0CH0_CR_LPMODECTL_spare_HSH                              (0x040001B0)

  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dll_to_alldisb_OFF     ( 4)
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dll_to_alldisb_WID     ( 1)
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dll_to_alldisb_MSK     (0x00000010)
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dll_to_alldisb_MIN     (0)
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dll_to_alldisb_MAX     (1) // 0x00000001
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dll_to_alldisb_DEF     (0x00000000)
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dll_to_alldisb_HSH     (0x010801B0)

  #define DATA0CH0_CR_LPMODECTL_scr_vrefen2rxbiasen_timer_val_OFF      ( 5)
  #define DATA0CH0_CR_LPMODECTL_scr_vrefen2rxbiasen_timer_val_WID      ( 8)
  #define DATA0CH0_CR_LPMODECTL_scr_vrefen2rxbiasen_timer_val_MSK      (0x00001FE0)
  #define DATA0CH0_CR_LPMODECTL_scr_vrefen2rxbiasen_timer_val_MIN      (0)
  #define DATA0CH0_CR_LPMODECTL_scr_vrefen2rxbiasen_timer_val_MAX      (255) // 0x000000FF
  #define DATA0CH0_CR_LPMODECTL_scr_vrefen2rxbiasen_timer_val_DEF      (0x00000080)
  #define DATA0CH0_CR_LPMODECTL_scr_vrefen2rxbiasen_timer_val_HSH      (0x080A01B0)

  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrdval_OFF                (13)
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrdval_WID                ( 1)
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrdval_MSK                (0x00002000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrdval_MIN                (0)
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrdval_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrdval_DEF                (0x00000000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrdval_HSH                (0x011A01B0)

  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrden_OFF                 (14)
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrden_WID                 ( 1)
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrden_MSK                 (0x00004000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrden_MIN                 (0)
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrden_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrden_DEF                 (0x00000000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_pien_ovrden_HSH                 (0x011C01B0)

  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrdval_OFF              (15)
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrdval_WID              ( 1)
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrdval_MSK              (0x00008000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrdval_MIN              (0)
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrdval_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrdval_DEF              (0x00000000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrdval_HSH              (0x011E01B0)

  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrden_OFF               (16)
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrden_WID               ( 1)
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrden_MSK               (0x00010000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrden_MIN               (0)
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrden_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrden_DEF               (0x00000000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_mdllen_ovrden_HSH               (0x012001B0)

  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrdval_OFF               (17)
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrdval_WID               ( 1)
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrdval_MSK               (0x00020000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrdval_MIN               (0)
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrdval_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrdval_DEF               (0x00000000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrdval_HSH               (0x012201B0)

  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrden_OFF                (18)
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrden_WID                ( 1)
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrden_MSK                (0x00040000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrden_MIN                (0)
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrden_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrden_DEF                (0x00000000)
  #define DATA0CH0_CR_LPMODECTL_lpmode_ldoen_ovrden_HSH                (0x012401B0)

  #define DATA0CH0_CR_LPMODECTL_scr_mdlldisb2ldodisb_timer_val_OFF     (19)
  #define DATA0CH0_CR_LPMODECTL_scr_mdlldisb2ldodisb_timer_val_WID     ( 3)
  #define DATA0CH0_CR_LPMODECTL_scr_mdlldisb2ldodisb_timer_val_MSK     (0x00380000)
  #define DATA0CH0_CR_LPMODECTL_scr_mdlldisb2ldodisb_timer_val_MIN     (0)
  #define DATA0CH0_CR_LPMODECTL_scr_mdlldisb2ldodisb_timer_val_MAX     (7) // 0x00000007
  #define DATA0CH0_CR_LPMODECTL_scr_mdlldisb2ldodisb_timer_val_DEF     (0x00000001)
  #define DATA0CH0_CR_LPMODECTL_scr_mdlldisb2ldodisb_timer_val_HSH     (0x032601B0)

  #define DATA0CH0_CR_LPMODECTL_scr_pidisb2mdlldsb_timer_val_OFF       (22)
  #define DATA0CH0_CR_LPMODECTL_scr_pidisb2mdlldsb_timer_val_WID       ( 3)
  #define DATA0CH0_CR_LPMODECTL_scr_pidisb2mdlldsb_timer_val_MSK       (0x01C00000)
  #define DATA0CH0_CR_LPMODECTL_scr_pidisb2mdlldsb_timer_val_MIN       (0)
  #define DATA0CH0_CR_LPMODECTL_scr_pidisb2mdlldsb_timer_val_MAX       (7) // 0x00000007
  #define DATA0CH0_CR_LPMODECTL_scr_pidisb2mdlldsb_timer_val_DEF       (0x00000001)
  #define DATA0CH0_CR_LPMODECTL_scr_pidisb2mdlldsb_timer_val_HSH       (0x032C01B0)

  #define DATA0CH0_CR_LPMODECTL_scr_lpmodentry_timer_val_OFF           (25)
  #define DATA0CH0_CR_LPMODECTL_scr_lpmodentry_timer_val_WID           ( 3)
  #define DATA0CH0_CR_LPMODECTL_scr_lpmodentry_timer_val_MSK           (0x0E000000)
  #define DATA0CH0_CR_LPMODECTL_scr_lpmodentry_timer_val_MIN           (0)
  #define DATA0CH0_CR_LPMODECTL_scr_lpmodentry_timer_val_MAX           (7) // 0x00000007
  #define DATA0CH0_CR_LPMODECTL_scr_lpmodentry_timer_val_DEF           (0x00000001)
  #define DATA0CH0_CR_LPMODECTL_scr_lpmodentry_timer_val_HSH           (0x033201B0)

  #define DATA0CH0_CR_LPMODECTL_scr_dllen2pien_timer_val_OFF           (28)
  #define DATA0CH0_CR_LPMODECTL_scr_dllen2pien_timer_val_WID           ( 3)
  #define DATA0CH0_CR_LPMODECTL_scr_dllen2pien_timer_val_MSK           (0x70000000)
  #define DATA0CH0_CR_LPMODECTL_scr_dllen2pien_timer_val_MIN           (0)
  #define DATA0CH0_CR_LPMODECTL_scr_dllen2pien_timer_val_MAX           (7) // 0x00000007
  #define DATA0CH0_CR_LPMODECTL_scr_dllen2pien_timer_val_DEF           (0x00000001)
  #define DATA0CH0_CR_LPMODECTL_scr_dllen2pien_timer_val_HSH           (0x033801B0)

  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dly_OFF                (31)
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dly_WID                ( 1)
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dly_MSK                (0x80000000)
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dly_MIN                (0)
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dly_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dly_DEF                (0x00000000)
  #define DATA0CH0_CR_LPMODECTL_scr_skip_lpmode_dly_HSH                (0x013E01B0)

#define DATA0CH0_CR_DDRCRDATACOMP0_REG                                 (0x000001B4)

  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvUp_OFF                    ( 0)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvUp_WID                    ( 6)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvUp_MSK                    (0x0000003F)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvUp_MIN                    (0)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvUp_MAX                    (63) // 0x0000003F
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvUp_DEF                    (0x00000020)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvUp_HSH                    (0x060001B4)

  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvDown_OFF                  ( 6)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvDown_WID                  ( 6)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvDown_MSK                  (0x00000FC0)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvDown_MIN                  (0)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvDown_MAX                  (63) // 0x0000003F
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvDown_DEF                  (0x00000020)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvDown_HSH                  (0x060C01B4)

  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_OFF                    (12)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_WID                    ( 6)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_MSK                    (0x0003F000)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_MIN                    (0)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_MAX                    (63) // 0x0000003F
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_DEF                    (0x00000020)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_HSH                    (0x061801B4)

  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_OFF                  (18)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_WID                  ( 6)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_MSK                  (0x00FC0000)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_MIN                  (0)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_MAX                  (63) // 0x0000003F
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_DEF                  (0x00000020)
  #define DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_HSH                  (0x062401B4)

  #define DATA0CH0_CR_DDRCRDATACOMP0_VssHiFF_dq_OFF                    (24)
  #define DATA0CH0_CR_DDRCRDATACOMP0_VssHiFF_dq_WID                    ( 6)
  #define DATA0CH0_CR_DDRCRDATACOMP0_VssHiFF_dq_MSK                    (0x3F000000)
  #define DATA0CH0_CR_DDRCRDATACOMP0_VssHiFF_dq_MIN                    (0)
  #define DATA0CH0_CR_DDRCRDATACOMP0_VssHiFF_dq_MAX                    (63) // 0x0000003F
  #define DATA0CH0_CR_DDRCRDATACOMP0_VssHiFF_dq_DEF                    (0x00000020)
  #define DATA0CH0_CR_DDRCRDATACOMP0_VssHiFF_dq_HSH                    (0x063001B4)

  #define DATA0CH0_CR_DDRCRDATACOMP0_Spare_OFF                         (30)
  #define DATA0CH0_CR_DDRCRDATACOMP0_Spare_WID                         ( 2)
  #define DATA0CH0_CR_DDRCRDATACOMP0_Spare_MSK                         (0xC0000000)
  #define DATA0CH0_CR_DDRCRDATACOMP0_Spare_MIN                         (0)
  #define DATA0CH0_CR_DDRCRDATACOMP0_Spare_MAX                         (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACOMP0_Spare_DEF                         (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMP0_Spare_HSH                         (0x023C01B4)

#define DATA0CH0_CR_DDRCRDATACOMP1_REG                                 (0x000001B8)

  #define DATA0CH0_CR_DDRCRDATACOMP1_Vsshiff_rx_OFF                    ( 0)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Vsshiff_rx_WID                    ( 6)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Vsshiff_rx_MSK                    (0x0000003F)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Vsshiff_rx_MIN                    (0)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Vsshiff_rx_MAX                    (63) // 0x0000003F
  #define DATA0CH0_CR_DDRCRDATACOMP1_Vsshiff_rx_DEF                    (0x00000020)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Vsshiff_rx_HSH                    (0x060001B8)

  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkn_OFF                   ( 6)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkn_WID                   ( 7)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkn_MSK                   (0x00001FC0)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkn_MIN                   (0)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkn_MAX                   (127) // 0x0000007F
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkn_DEF                   (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkn_HSH                   (0x070C01B8)

  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkp_OFF                   (13)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkp_WID                   ( 7)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkp_MSK                   (0x000FE000)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkp_MIN                   (0)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkp_MAX                   (127) // 0x0000007F
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkp_DEF                   (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqs_fwdclkp_HSH                   (0x071A01B8)

  #define DATA0CH0_CR_DDRCRDATACOMP1_Dll_coderead_OFF                  (20)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dll_coderead_WID                  ( 7)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dll_coderead_MSK                  (0x07F00000)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dll_coderead_MIN                  (0)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dll_coderead_MAX                  (127) // 0x0000007F
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dll_coderead_DEF                  (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dll_coderead_HSH                  (0x072801B8)

  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsntargetnui_OFF                 (27)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsntargetnui_WID                 ( 3)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsntargetnui_MSK                 (0x38000000)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsntargetnui_MIN                 (0)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsntargetnui_MAX                 (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsntargetnui_DEF                 (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsntargetnui_HSH                 (0x033601B8)

  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsnoffsetnui_OFF                 (30)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsnoffsetnui_WID                 ( 2)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsnoffsetnui_MSK                 (0xC0000000)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsnoffsetnui_MIN                 (0)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsnoffsetnui_MAX                 (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsnoffsetnui_DEF                 (0x00000000)
  #define DATA0CH0_CR_DDRCRDATACOMP1_Dqsnoffsetnui_HSH                 (0x023C01B8)

#define DATA0CH0_CR_VCCDLLCOMPDATA_REG                                 (0x000001BC)

  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codepi_OFF                    ( 0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codepi_WID                    ( 6)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codepi_MSK                    (0x0000003F)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codepi_MIN                    (0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codepi_MAX                    (63) // 0x0000003F
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codepi_DEF                    (0x00000020)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codepi_HSH                    (0x060001BC)

  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codewl_OFF                    ( 6)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codewl_WID                    ( 6)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codewl_MSK                    (0x00000FC0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codewl_MIN                    (0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codewl_MAX                    (63) // 0x0000003F
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codewl_DEF                    (0x00000020)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_codewl_HSH                    (0x060C01BC)

  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_bwsel_OFF                     (12)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_bwsel_WID                     ( 6)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_bwsel_MSK                     (0x0003F000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_bwsel_MIN                     (0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_bwsel_MAX                     (63) // 0x0000003F
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_bwsel_DEF                     (0x00000020)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_bwsel_HSH                     (0x061801BC)

  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_vdlllock_OFF                  (18)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_vdlllock_WID                  ( 8)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_vdlllock_MSK                  (0x03FC0000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_vdlllock_MIN                  (0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_vdlllock_MAX                  (255) // 0x000000FF
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_vdlllock_DEF                  (0x00000080)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Dll_vdlllock_HSH                  (0x082401BC)

  #define DATA0CH0_CR_VCCDLLCOMPDATA_rloaddqs_OFF                      (18)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_rloaddqs_WID                      ( 6)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_rloaddqs_MSK                      (0x00FC0000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_rloaddqs_MIN                      (0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_rloaddqs_MAX                      (63) // 0x0000003F
  #define DATA0CH0_CR_VCCDLLCOMPDATA_rloaddqs_DEF                      (0x00000020)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_rloaddqs_HSH                      (0x062401BC)

  #define DATA0CH0_CR_VCCDLLCOMPDATA_cben_OFF                          (24)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_cben_WID                          ( 2)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_cben_MSK                          (0x03000000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_cben_MIN                          (0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_cben_MAX                          (3) // 0x00000003
  #define DATA0CH0_CR_VCCDLLCOMPDATA_cben_DEF                          (0x00000000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_cben_HSH                          (0x023001BC)

  #define DATA0CH0_CR_VCCDLLCOMPDATA_Offsetnui_OFF                     (26)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Offsetnui_WID                     ( 2)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Offsetnui_MSK                     (0x0C000000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Offsetnui_MIN                     (0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Offsetnui_MAX                     (3) // 0x00000003
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Offsetnui_DEF                     (0x00000000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Offsetnui_HSH                     (0x023401BC)

  #define DATA0CH0_CR_VCCDLLCOMPDATA_Targetnui_OFF                     (28)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Targetnui_WID                     ( 3)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Targetnui_MSK                     (0x70000000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Targetnui_MIN                     (0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Targetnui_MAX                     (7) // 0x00000007
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Targetnui_DEF                     (0x00000000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Targetnui_HSH                     (0x033801BC)

  #define DATA0CH0_CR_VCCDLLCOMPDATA_Spare_OFF                         (31)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Spare_WID                         ( 1)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Spare_MSK                         (0x80000000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Spare_MIN                         (0)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Spare_MAX                         (1) // 0x00000001
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Spare_DEF                         (0x00000000)
  #define DATA0CH0_CR_VCCDLLCOMPDATA_Spare_HSH                         (0x013E01BC)

#define DATA0CH0_CR_DCSOFFSET_REG                                      (0x000001C0)

  #define DATA0CH0_CR_DCSOFFSET_dccoffset_OFF                          ( 0)
  #define DATA0CH0_CR_DCSOFFSET_dccoffset_WID                          ( 8)
  #define DATA0CH0_CR_DCSOFFSET_dccoffset_MSK                          (0x000000FF)
  #define DATA0CH0_CR_DCSOFFSET_dccoffset_MIN                          (0)
  #define DATA0CH0_CR_DCSOFFSET_dccoffset_MAX                          (255) // 0x000000FF
  #define DATA0CH0_CR_DCSOFFSET_dccoffset_DEF                          (0x00000000)
  #define DATA0CH0_CR_DCSOFFSET_dccoffset_HSH                          (0x080001C0)

  #define DATA0CH0_CR_DCSOFFSET_dccoffsetsign_OFF                      ( 8)
  #define DATA0CH0_CR_DCSOFFSET_dccoffsetsign_WID                      ( 1)
  #define DATA0CH0_CR_DCSOFFSET_dccoffsetsign_MSK                      (0x00000100)
  #define DATA0CH0_CR_DCSOFFSET_dccoffsetsign_MIN                      (0)
  #define DATA0CH0_CR_DCSOFFSET_dccoffsetsign_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_DCSOFFSET_dccoffsetsign_DEF                      (0x00000000)
  #define DATA0CH0_CR_DCSOFFSET_dccoffsetsign_HSH                      (0x011001C0)

  #define DATA0CH0_CR_DCSOFFSET_reserved24_OFF                         ( 9)
  #define DATA0CH0_CR_DCSOFFSET_reserved24_WID                         (23)
  #define DATA0CH0_CR_DCSOFFSET_reserved24_MSK                         (0xFFFFFE00)
  #define DATA0CH0_CR_DCSOFFSET_reserved24_MIN                         (0)
  #define DATA0CH0_CR_DCSOFFSET_reserved24_MAX                         (8388607) // 0x007FFFFF
  #define DATA0CH0_CR_DCSOFFSET_reserved24_DEF                         (0x00000000)
  #define DATA0CH0_CR_DCSOFFSET_reserved24_HSH                         (0x171201C0)

#define DATA0CH0_CR_DFXCCMON_REG                                       (0x000001C4)

  #define DATA0CH0_CR_DFXCCMON_ccm_done_OFF                            ( 0)
  #define DATA0CH0_CR_DFXCCMON_ccm_done_WID                            ( 1)
  #define DATA0CH0_CR_DFXCCMON_ccm_done_MSK                            (0x00000001)
  #define DATA0CH0_CR_DFXCCMON_ccm_done_MIN                            (0)
  #define DATA0CH0_CR_DFXCCMON_ccm_done_MAX                            (1) // 0x00000001
  #define DATA0CH0_CR_DFXCCMON_ccm_done_DEF                            (0x00000000)
  #define DATA0CH0_CR_DFXCCMON_ccm_done_HSH                            (0x010001C4)

  #define DATA0CH0_CR_DFXCCMON_ccm_result_OFF                          ( 1)
  #define DATA0CH0_CR_DFXCCMON_ccm_result_WID                          (10)
  #define DATA0CH0_CR_DFXCCMON_ccm_result_MSK                          (0x000007FE)
  #define DATA0CH0_CR_DFXCCMON_ccm_result_MIN                          (0)
  #define DATA0CH0_CR_DFXCCMON_ccm_result_MAX                          (1023) // 0x000003FF
  #define DATA0CH0_CR_DFXCCMON_ccm_result_DEF                          (0x00000000)
  #define DATA0CH0_CR_DFXCCMON_ccm_result_HSH                          (0x0A0201C4)

  #define DATA0CH0_CR_DFXCCMON_ccm_clk_sel_OFF                         (11)
  #define DATA0CH0_CR_DFXCCMON_ccm_clk_sel_WID                         ( 4)
  #define DATA0CH0_CR_DFXCCMON_ccm_clk_sel_MSK                         (0x00007800)
  #define DATA0CH0_CR_DFXCCMON_ccm_clk_sel_MIN                         (0)
  #define DATA0CH0_CR_DFXCCMON_ccm_clk_sel_MAX                         (15) // 0x0000000F
  #define DATA0CH0_CR_DFXCCMON_ccm_clk_sel_DEF                         (0x00000000)
  #define DATA0CH0_CR_DFXCCMON_ccm_clk_sel_HSH                         (0x041601C4)

  #define DATA0CH0_CR_DFXCCMON_spare1_OFF                              (15)
  #define DATA0CH0_CR_DFXCCMON_spare1_WID                              ( 1)
  #define DATA0CH0_CR_DFXCCMON_spare1_MSK                              (0x00008000)
  #define DATA0CH0_CR_DFXCCMON_spare1_MIN                              (0)
  #define DATA0CH0_CR_DFXCCMON_spare1_MAX                              (1) // 0x00000001
  #define DATA0CH0_CR_DFXCCMON_spare1_DEF                              (0x00000000)
  #define DATA0CH0_CR_DFXCCMON_spare1_HSH                              (0x011E01C4)

  #define DATA0CH0_CR_DFXCCMON_ccm_bcnt_ival_OFF                       (16)
  #define DATA0CH0_CR_DFXCCMON_ccm_bcnt_ival_WID                       ( 8)
  #define DATA0CH0_CR_DFXCCMON_ccm_bcnt_ival_MSK                       (0x00FF0000)
  #define DATA0CH0_CR_DFXCCMON_ccm_bcnt_ival_MIN                       (0)
  #define DATA0CH0_CR_DFXCCMON_ccm_bcnt_ival_MAX                       (255) // 0x000000FF
  #define DATA0CH0_CR_DFXCCMON_ccm_bcnt_ival_DEF                       (0x00000000)
  #define DATA0CH0_CR_DFXCCMON_ccm_bcnt_ival_HSH                       (0x082001C4)

  #define DATA0CH0_CR_DFXCCMON_spare2_OFF                              (24)
  #define DATA0CH0_CR_DFXCCMON_spare2_WID                              ( 7)
  #define DATA0CH0_CR_DFXCCMON_spare2_MSK                              (0x7F000000)
  #define DATA0CH0_CR_DFXCCMON_spare2_MIN                              (0)
  #define DATA0CH0_CR_DFXCCMON_spare2_MAX                              (127) // 0x0000007F
  #define DATA0CH0_CR_DFXCCMON_spare2_DEF                              (0x00000000)
  #define DATA0CH0_CR_DFXCCMON_spare2_HSH                              (0x073001C4)

  #define DATA0CH0_CR_DFXCCMON_ccm_start_OFF                           (31)
  #define DATA0CH0_CR_DFXCCMON_ccm_start_WID                           ( 1)
  #define DATA0CH0_CR_DFXCCMON_ccm_start_MSK                           (0x80000000)
  #define DATA0CH0_CR_DFXCCMON_ccm_start_MIN                           (0)
  #define DATA0CH0_CR_DFXCCMON_ccm_start_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_DFXCCMON_ccm_start_DEF                           (0x00000000)
  #define DATA0CH0_CR_DFXCCMON_ccm_start_HSH                           (0x013E01C4)

#define DATA0CH0_CR_AFEMISCCTRL0_REG                                   (0x000001C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrdval_OFF    ( 0)
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrdval_WID    (16)
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrdval_MSK    (0x0000FFFF)
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrdval_MIN    (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrdval_MAX    (65535) // 0x0000FFFF
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrdval_DEF    (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrdval_HSH    (0x100001C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrden_OFF     (16)
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrden_WID     ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrden_MSK     (0x00010000)
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrden_MIN     (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrden_MAX     (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrden_DEF     (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_dqdcdsrz_cmn_txeqdataovrden_HSH     (0x012001C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqsbuf_cmn_bonus_OFF        (17)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqsbuf_cmn_bonus_WID        ( 2)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqsbuf_cmn_bonus_MSK        (0x00060000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqsbuf_cmn_bonus_MIN        (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqsbuf_cmn_bonus_MAX        (3) // 0x00000003
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqsbuf_cmn_bonus_DEF        (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqsbuf_cmn_bonus_HSH        (0x022201C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq7_rxumdataunflopped_OFF (19)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq7_rxumdataunflopped_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq7_rxumdataunflopped_MSK (0x00080000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq7_rxumdataunflopped_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq7_rxumdataunflopped_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq7_rxumdataunflopped_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq7_rxumdataunflopped_HSH (0x012601C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq6_rxumdataunflopped_OFF (20)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq6_rxumdataunflopped_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq6_rxumdataunflopped_MSK (0x00100000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq6_rxumdataunflopped_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq6_rxumdataunflopped_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq6_rxumdataunflopped_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq6_rxumdataunflopped_HSH (0x012801C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq5_rxumdataunflopped_OFF (21)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq5_rxumdataunflopped_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq5_rxumdataunflopped_MSK (0x00200000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq5_rxumdataunflopped_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq5_rxumdataunflopped_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq5_rxumdataunflopped_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq5_rxumdataunflopped_HSH (0x012A01C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq4_rxumdataunflopped_OFF (22)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq4_rxumdataunflopped_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq4_rxumdataunflopped_MSK (0x00400000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq4_rxumdataunflopped_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq4_rxumdataunflopped_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq4_rxumdataunflopped_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq4_rxumdataunflopped_HSH (0x012C01C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq3_rxumdataunflopped_OFF (23)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq3_rxumdataunflopped_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq3_rxumdataunflopped_MSK (0x00800000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq3_rxumdataunflopped_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq3_rxumdataunflopped_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq3_rxumdataunflopped_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq3_rxumdataunflopped_HSH (0x012E01C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq2_rxumdataunflopped_OFF (24)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq2_rxumdataunflopped_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq2_rxumdataunflopped_MSK (0x01000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq2_rxumdataunflopped_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq2_rxumdataunflopped_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq2_rxumdataunflopped_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq2_rxumdataunflopped_HSH (0x013001C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq1_rxumdataunflopped_OFF (25)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq1_rxumdataunflopped_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq1_rxumdataunflopped_MSK (0x02000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq1_rxumdataunflopped_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq1_rxumdataunflopped_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq1_rxumdataunflopped_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq1_rxumdataunflopped_HSH (0x013201C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq0_rxumdataunflopped_OFF (26)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq0_rxumdataunflopped_WID ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq0_rxumdataunflopped_MSK (0x04000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq0_rxumdataunflopped_MIN (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq0_rxumdataunflopped_MAX (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq0_rxumdataunflopped_DEF (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq0_rxumdataunflopped_HSH (0x013401C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_cktop_cmn_bonus_OFF         (27)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_cktop_cmn_bonus_WID         ( 4)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_cktop_cmn_bonus_MSK         (0x78000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_cktop_cmn_bonus_MIN         (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_cktop_cmn_bonus_MAX         (15) // 0x0000000F
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_cktop_cmn_bonus_DEF         (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_afe2dig_cktop_cmn_bonus_HSH         (0x043601C8)

  #define DATA0CH0_CR_AFEMISCCTRL0_Reserved25_OFF                      (31)
  #define DATA0CH0_CR_AFEMISCCTRL0_Reserved25_WID                      ( 1)
  #define DATA0CH0_CR_AFEMISCCTRL0_Reserved25_MSK                      (0x80000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_Reserved25_MIN                      (0)
  #define DATA0CH0_CR_AFEMISCCTRL0_Reserved25_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_AFEMISCCTRL0_Reserved25_DEF                      (0x00000000)
  #define DATA0CH0_CR_AFEMISCCTRL0_Reserved25_HSH                      (0x013E01C8)

#define DATA0CH0_CR_VIEWCTL_REG                                        (0x000001CC)

  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanaen_OFF                 ( 0)
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanaen_WID                 ( 1)
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanaen_MSK                 (0x00000001)
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanaen_MIN                 (0)
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanaen_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanaen_DEF                 (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanaen_HSH                 (0x010001CC)

  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanabiassel_OFF            ( 1)
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanabiassel_WID            ( 3)
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanabiassel_MSK            (0x0000000E)
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanabiassel_MIN            (0)
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanabiassel_MAX            (7) // 0x00000007
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanabiassel_DEF            (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_rxbias_cmn_viewanabiassel_HSH            (0x030201CC)

  #define DATA0CH0_CR_VIEWCTL_rxvref_cmn_viewanaen_OFF                 ( 4)
  #define DATA0CH0_CR_VIEWCTL_rxvref_cmn_viewanaen_WID                 ( 1)
  #define DATA0CH0_CR_VIEWCTL_rxvref_cmn_viewanaen_MSK                 (0x00000010)
  #define DATA0CH0_CR_VIEWCTL_rxvref_cmn_viewanaen_MIN                 (0)
  #define DATA0CH0_CR_VIEWCTL_rxvref_cmn_viewanaen_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_VIEWCTL_rxvref_cmn_viewanaen_DEF                 (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_rxvref_cmn_viewanaen_HSH                 (0x010801CC)

  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_OFF     ( 5)
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_WID     ( 4)
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_MSK     (0x000001E0)
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_MIN     (0)
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_MAX     (15) // 0x0000000F
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_DEF     (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_HSH     (0x040A01CC)

  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_OFF     ( 9)
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_WID     ( 4)
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_MSK     (0x00001E00)
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_MIN     (0)
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_MAX     (15) // 0x0000000F
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_DEF     (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_HSH     (0x041201CC)

  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanasel_OFF             (13)
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanasel_WID             ( 3)
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanasel_MSK             (0x0000E000)
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanasel_MIN             (0)
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanasel_MAX             (7) // 0x00000007
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanasel_DEF             (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanasel_HSH             (0x031A01CC)

  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanaen_OFF              (16)
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanaen_WID              ( 1)
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanaen_MSK              (0x00010000)
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanaen_MIN              (0)
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanaen_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanaen_DEF              (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_viewmxana_cmn_viewanaen_HSH              (0x012001CC)

  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_OFF          (17)
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_WID          ( 4)
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_MSK          (0x001E0000)
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_MIN          (0)
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_MAX          (15) // 0x0000000F
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_DEF          (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_HSH          (0x042201CC)

  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_OFF          (21)
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_WID          ( 4)
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_MSK          (0x01E00000)
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_MIN          (0)
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_MAX          (15) // 0x0000000F
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_DEF          (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_HSH          (0x042A01CC)

  #define DATA0CH0_CR_VIEWCTL_mdll_cmn_viewanaen_OFF                   (25)
  #define DATA0CH0_CR_VIEWCTL_mdll_cmn_viewanaen_WID                   ( 1)
  #define DATA0CH0_CR_VIEWCTL_mdll_cmn_viewanaen_MSK                   (0x02000000)
  #define DATA0CH0_CR_VIEWCTL_mdll_cmn_viewanaen_MIN                   (0)
  #define DATA0CH0_CR_VIEWCTL_mdll_cmn_viewanaen_MAX                   (1) // 0x00000001
  #define DATA0CH0_CR_VIEWCTL_mdll_cmn_viewanaen_DEF                   (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_mdll_cmn_viewanaen_HSH                   (0x013201CC)

  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewsel_OFF                (26)
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewsel_WID                ( 3)
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewsel_MSK                (0x1C000000)
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewsel_MIN                (0)
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewsel_MAX                (7) // 0x00000007
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewsel_DEF                (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewsel_HSH                (0x033401CC)

  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewen_OFF                 (29)
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewen_WID                 ( 1)
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewen_MSK                 (0x20000000)
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewen_MIN                 (0)
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewen_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewen_DEF                 (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_dqsbuf_dqs_anaviewen_HSH                 (0x013A01CC)

  #define DATA0CH0_CR_VIEWCTL_Reserved26_OFF                           (30)
  #define DATA0CH0_CR_VIEWCTL_Reserved26_WID                           ( 2)
  #define DATA0CH0_CR_VIEWCTL_Reserved26_MSK                           (0xC0000000)
  #define DATA0CH0_CR_VIEWCTL_Reserved26_MIN                           (0)
  #define DATA0CH0_CR_VIEWCTL_Reserved26_MAX                           (3) // 0x00000003
  #define DATA0CH0_CR_VIEWCTL_Reserved26_DEF                           (0x00000000)
  #define DATA0CH0_CR_VIEWCTL_Reserved26_HSH                           (0x023C01CC)

#define DATA0CH0_CR_DCCCTL11_REG                                       (0x000001D0)

  #define DATA0CH0_CR_DCCCTL11_fatal_live_OFF                          ( 0)
  #define DATA0CH0_CR_DCCCTL11_fatal_live_WID                          (10)
  #define DATA0CH0_CR_DCCCTL11_fatal_live_MSK                          (0x000003FF)
  #define DATA0CH0_CR_DCCCTL11_fatal_live_MIN                          (0)
  #define DATA0CH0_CR_DCCCTL11_fatal_live_MAX                          (1023) // 0x000003FF
  #define DATA0CH0_CR_DCCCTL11_fatal_live_DEF                          (0x00000000)
  #define DATA0CH0_CR_DCCCTL11_fatal_live_HSH                          (0x0A0001D0)

  #define DATA0CH0_CR_DCCCTL11_fatal_sticky_OFF                        (10)
  #define DATA0CH0_CR_DCCCTL11_fatal_sticky_WID                        (10)
  #define DATA0CH0_CR_DCCCTL11_fatal_sticky_MSK                        (0x000FFC00)
  #define DATA0CH0_CR_DCCCTL11_fatal_sticky_MIN                        (0)
  #define DATA0CH0_CR_DCCCTL11_fatal_sticky_MAX                        (1023) // 0x000003FF
  #define DATA0CH0_CR_DCCCTL11_fatal_sticky_DEF                        (0x00000000)
  #define DATA0CH0_CR_DCCCTL11_fatal_sticky_HSH                        (0x0A1401D0)

  #define DATA0CH0_CR_DCCCTL11_fatal_cal_OFF                           (20)
  #define DATA0CH0_CR_DCCCTL11_fatal_cal_WID                           ( 1)
  #define DATA0CH0_CR_DCCCTL11_fatal_cal_MSK                           (0x00100000)
  #define DATA0CH0_CR_DCCCTL11_fatal_cal_MIN                           (0)
  #define DATA0CH0_CR_DCCCTL11_fatal_cal_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL11_fatal_cal_DEF                           (0x00000000)
  #define DATA0CH0_CR_DCCCTL11_fatal_cal_HSH                           (0x012801D0)

  #define DATA0CH0_CR_DCCCTL11_dcdrocal_en_OFF                         (21)
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_en_WID                         ( 1)
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_en_MSK                         (0x00200000)
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_en_MIN                         (0)
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_en_MAX                         (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_en_DEF                         (0x00000000)
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_en_HSH                         (0x012A01D0)

  #define DATA0CH0_CR_DCCCTL11_dcdrocal_start_OFF                      (22)
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_start_WID                      ( 1)
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_start_MSK                      (0x00400000)
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_start_MIN                      (0)
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_start_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_start_DEF                      (0x00000000)
  #define DATA0CH0_CR_DCCCTL11_dcdrocal_start_HSH                      (0x012C01D0)

  #define DATA0CH0_CR_DCCCTL11_dcc_start_OFF                           (23)
  #define DATA0CH0_CR_DCCCTL11_dcc_start_WID                           ( 1)
  #define DATA0CH0_CR_DCCCTL11_dcc_start_MSK                           (0x00800000)
  #define DATA0CH0_CR_DCCCTL11_dcc_start_MIN                           (0)
  #define DATA0CH0_CR_DCCCTL11_dcc_start_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_DCCCTL11_dcc_start_DEF                           (0x00000000)
  #define DATA0CH0_CR_DCCCTL11_dcc_start_HSH                           (0x012E01D0)

  #define DATA0CH0_CR_DCCCTL11_dccctrlcodeph90_status_OFF              (24)
  #define DATA0CH0_CR_DCCCTL11_dccctrlcodeph90_status_WID              ( 8)
  #define DATA0CH0_CR_DCCCTL11_dccctrlcodeph90_status_MSK              (0xFF000000)
  #define DATA0CH0_CR_DCCCTL11_dccctrlcodeph90_status_MIN              (0)
  #define DATA0CH0_CR_DCCCTL11_dccctrlcodeph90_status_MAX              (255) // 0x000000FF
  #define DATA0CH0_CR_DCCCTL11_dccctrlcodeph90_status_DEF              (0x00000000)
  #define DATA0CH0_CR_DCCCTL11_dccctrlcodeph90_status_HSH              (0x083001D0)

#define DATA0CH0_CR_DCCCTL13_REG                                       (0x000001D4)

  #define DATA0CH0_CR_DCCCTL13_dcdsamplecount_OFF                      ( 0)
  #define DATA0CH0_CR_DCCCTL13_dcdsamplecount_WID                      (16)
  #define DATA0CH0_CR_DCCCTL13_dcdsamplecount_MSK                      (0x0000FFFF)
  #define DATA0CH0_CR_DCCCTL13_dcdsamplecount_MIN                      (0)
  #define DATA0CH0_CR_DCCCTL13_dcdsamplecount_MAX                      (65535) // 0x0000FFFF
  #define DATA0CH0_CR_DCCCTL13_dcdsamplecount_DEF                      (0x00000000)
  #define DATA0CH0_CR_DCCCTL13_dcdsamplecount_HSH                      (0x100001D4)

  #define DATA0CH0_CR_DCCCTL13_dcdrocal_count_OFF                      (16)
  #define DATA0CH0_CR_DCCCTL13_dcdrocal_count_WID                      (16)
  #define DATA0CH0_CR_DCCCTL13_dcdrocal_count_MSK                      (0xFFFF0000)
  #define DATA0CH0_CR_DCCCTL13_dcdrocal_count_MIN                      (0)
  #define DATA0CH0_CR_DCCCTL13_dcdrocal_count_MAX                      (65535) // 0x0000FFFF
  #define DATA0CH0_CR_DCCCTL13_dcdrocal_count_DEF                      (0x00000000)
  #define DATA0CH0_CR_DCCCTL13_dcdrocal_count_HSH                      (0x102001D4)

#define DATA0CH0_CR_PMFSM_REG                                          (0x000001D8)

  #define DATA0CH0_CR_PMFSM_spare_A0_OFF                                  ( 0)
  #define DATA0CH0_CR_PMFSM_spare_A0_WID                                  (25)
  #define DATA0CH0_CR_PMFSM_spare_A0_MSK                                  (0x01FFFFFF)
  #define DATA0CH0_CR_PMFSM_spare_A0_MIN                                  (0)
  #define DATA0CH0_CR_PMFSM_spare_A0_MAX                                  (33554431) // 0x01FFFFFF
  #define DATA0CH0_CR_PMFSM_spare_A0_DEF                                  (0x00000000)
  #define DATA0CH0_CR_PMFSM_spare_A0_HSH                                  (0x190001D8)

  #define DATA0CH0_CR_PMFSM_initdone_status_A0_OFF                        (25)
  #define DATA0CH0_CR_PMFSM_initdone_status_A0_WID                        ( 1)
  #define DATA0CH0_CR_PMFSM_initdone_status_A0_MSK                        (0x02000000)
  #define DATA0CH0_CR_PMFSM_initdone_status_A0_MIN                        (0)
  #define DATA0CH0_CR_PMFSM_initdone_status_A0_MAX                        (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM_initdone_status_A0_DEF                        (0x00000000)
  #define DATA0CH0_CR_PMFSM_initdone_status_A0_HSH                        (0x013201D8)

  #define DATA0CH0_CR_PMFSM_scr_gvblock_ovrride_A0_OFF                    (26)
  #define DATA0CH0_CR_PMFSM_scr_gvblock_ovrride_A0_WID                    ( 1)
  #define DATA0CH0_CR_PMFSM_scr_gvblock_ovrride_A0_MSK                    (0x04000000)
  #define DATA0CH0_CR_PMFSM_scr_gvblock_ovrride_A0_MIN                    (0)
  #define DATA0CH0_CR_PMFSM_scr_gvblock_ovrride_A0_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM_scr_gvblock_ovrride_A0_DEF                    (0x00000000)
  #define DATA0CH0_CR_PMFSM_scr_gvblock_ovrride_A0_HSH                    (0x013401D8)

  #define DATA0CH0_CR_PMFSM_spare_OFF                                  ( 0)
  #define DATA0CH0_CR_PMFSM_spare_WID                                  (21)
  #define DATA0CH0_CR_PMFSM_spare_MSK                                  (0x001FFFFF)
  #define DATA0CH0_CR_PMFSM_spare_MIN                                  (0)
  #define DATA0CH0_CR_PMFSM_spare_MAX                                  (2097151) // 0x001FFFFF
  #define DATA0CH0_CR_PMFSM_spare_DEF                                  (0x00000000)
  #define DATA0CH0_CR_PMFSM_spare_HSH                                  (0x150001D8)

  #define DATA0CH0_CR_PMFSM_current_pmctrlfsm_state_OFF                (21)
  #define DATA0CH0_CR_PMFSM_current_pmctrlfsm_state_WID                ( 4)
  #define DATA0CH0_CR_PMFSM_current_pmctrlfsm_state_MSK                (0x01E00000)
  #define DATA0CH0_CR_PMFSM_current_pmctrlfsm_state_MIN                (0)
  #define DATA0CH0_CR_PMFSM_current_pmctrlfsm_state_MAX                (15) // 0x0000000F
  #define DATA0CH0_CR_PMFSM_current_pmctrlfsm_state_DEF                (0x00000000)
  #define DATA0CH0_CR_PMFSM_current_pmctrlfsm_state_HSH                (0x042A01D8)

  #define DATA0CH0_CR_PMFSM_Vsshipwrgood_OFF                           (25)
  #define DATA0CH0_CR_PMFSM_Vsshipwrgood_WID                           ( 1)
  #define DATA0CH0_CR_PMFSM_Vsshipwrgood_MSK                           (0x02000000)
  #define DATA0CH0_CR_PMFSM_Vsshipwrgood_MIN                           (0)
  #define DATA0CH0_CR_PMFSM_Vsshipwrgood_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM_Vsshipwrgood_DEF                           (0x00000000)
  #define DATA0CH0_CR_PMFSM_Vsshipwrgood_HSH                           (0x013201D8)

  #define DATA0CH0_CR_PMFSM_initdone_status_OFF                        (26)
  #define DATA0CH0_CR_PMFSM_initdone_status_WID                        ( 1)
  #define DATA0CH0_CR_PMFSM_initdone_status_MSK                        (0x04000000)
  #define DATA0CH0_CR_PMFSM_initdone_status_MIN                        (0)
  #define DATA0CH0_CR_PMFSM_initdone_status_MAX                        (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM_initdone_status_DEF                        (0x00000000)
  #define DATA0CH0_CR_PMFSM_initdone_status_HSH                        (0x013401D8)

  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrden_OFF                       (27)
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrden_WID                       ( 1)
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrden_MSK                       (0x08000000)
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrden_MIN                       (0)
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrden_MAX                       (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrden_DEF                       (0x00000000)
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrden_HSH                       (0x013601D8)

  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrdval_OFF                      (28)
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrdval_WID                      ( 4)
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrdval_MSK                      (0xF0000000)
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrdval_MIN                      (0)
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrdval_MAX                      (15) // 0x0000000F
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrdval_DEF                      (0x00000000)
  #define DATA0CH0_CR_PMFSM_pmctrlfsm_ovrdval_HSH                      (0x043801D8)

#define DATA0CH0_CR_PMFSM1_REG                                         (0x000001DC)

  #define DATA0CH0_CR_PMFSM1_pmclkgatedisable_OFF                      ( 0)
  #define DATA0CH0_CR_PMFSM1_pmclkgatedisable_WID                      ( 1)
  #define DATA0CH0_CR_PMFSM1_pmclkgatedisable_MSK                      (0x00000001)
  #define DATA0CH0_CR_PMFSM1_pmclkgatedisable_MIN                      (0)
  #define DATA0CH0_CR_PMFSM1_pmclkgatedisable_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_pmclkgatedisable_DEF                      (0x00000001)
  #define DATA0CH0_CR_PMFSM1_pmclkgatedisable_HSH                      (0x010001DC)

  #define DATA0CH0_CR_PMFSM1_rampenable_ovren_OFF                      ( 1)
  #define DATA0CH0_CR_PMFSM1_rampenable_ovren_WID                      ( 1)
  #define DATA0CH0_CR_PMFSM1_rampenable_ovren_MSK                      (0x00000002)
  #define DATA0CH0_CR_PMFSM1_rampenable_ovren_MIN                      (0)
  #define DATA0CH0_CR_PMFSM1_rampenable_ovren_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_rampenable_ovren_DEF                      (0x00000000)
  #define DATA0CH0_CR_PMFSM1_rampenable_ovren_HSH                      (0x010201DC)

  #define DATA0CH0_CR_PMFSM1_rampenable_ovrval_OFF                     ( 2)
  #define DATA0CH0_CR_PMFSM1_rampenable_ovrval_WID                     ( 1)
  #define DATA0CH0_CR_PMFSM1_rampenable_ovrval_MSK                     (0x00000004)
  #define DATA0CH0_CR_PMFSM1_rampenable_ovrval_MIN                     (0)
  #define DATA0CH0_CR_PMFSM1_rampenable_ovrval_MAX                     (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_rampenable_ovrval_DEF                     (0x00000000)
  #define DATA0CH0_CR_PMFSM1_rampenable_ovrval_HSH                     (0x010401DC)

  #define DATA0CH0_CR_PMFSM1_initdone_ovren_OFF                        ( 3)
  #define DATA0CH0_CR_PMFSM1_initdone_ovren_WID                        ( 1)
  #define DATA0CH0_CR_PMFSM1_initdone_ovren_MSK                        (0x00000008)
  #define DATA0CH0_CR_PMFSM1_initdone_ovren_MIN                        (0)
  #define DATA0CH0_CR_PMFSM1_initdone_ovren_MAX                        (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_initdone_ovren_DEF                        (0x00000000)
  #define DATA0CH0_CR_PMFSM1_initdone_ovren_HSH                        (0x010601DC)

  #define DATA0CH0_CR_PMFSM1_init_done_ovrval_OFF                      ( 4)
  #define DATA0CH0_CR_PMFSM1_init_done_ovrval_WID                      ( 1)
  #define DATA0CH0_CR_PMFSM1_init_done_ovrval_MSK                      (0x00000010)
  #define DATA0CH0_CR_PMFSM1_init_done_ovrval_MIN                      (0)
  #define DATA0CH0_CR_PMFSM1_init_done_ovrval_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_init_done_ovrval_DEF                      (0x00000000)
  #define DATA0CH0_CR_PMFSM1_init_done_ovrval_HSH                      (0x010801DC)

  #define DATA0CH0_CR_PMFSM1_iobufact_ovren_OFF                        ( 5)
  #define DATA0CH0_CR_PMFSM1_iobufact_ovren_WID                        ( 1)
  #define DATA0CH0_CR_PMFSM1_iobufact_ovren_MSK                        (0x00000020)
  #define DATA0CH0_CR_PMFSM1_iobufact_ovren_MIN                        (0)
  #define DATA0CH0_CR_PMFSM1_iobufact_ovren_MAX                        (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_iobufact_ovren_DEF                        (0x00000000)
  #define DATA0CH0_CR_PMFSM1_iobufact_ovren_HSH                        (0x010A01DC)

  #define DATA0CH0_CR_PMFSM1_iobufact_ovrval_OFF                       ( 6)
  #define DATA0CH0_CR_PMFSM1_iobufact_ovrval_WID                       ( 1)
  #define DATA0CH0_CR_PMFSM1_iobufact_ovrval_MSK                       (0x00000040)
  #define DATA0CH0_CR_PMFSM1_iobufact_ovrval_MIN                       (0)
  #define DATA0CH0_CR_PMFSM1_iobufact_ovrval_MAX                       (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_iobufact_ovrval_DEF                       (0x00000000)
  #define DATA0CH0_CR_PMFSM1_iobufact_ovrval_HSH                       (0x010C01DC)

  #define DATA0CH0_CR_PMFSM1_clkalign_ovren_OFF                        ( 7)
  #define DATA0CH0_CR_PMFSM1_clkalign_ovren_WID                        ( 1)
  #define DATA0CH0_CR_PMFSM1_clkalign_ovren_MSK                        (0x00000080)
  #define DATA0CH0_CR_PMFSM1_clkalign_ovren_MIN                        (0)
  #define DATA0CH0_CR_PMFSM1_clkalign_ovren_MAX                        (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_clkalign_ovren_DEF                        (0x00000000)
  #define DATA0CH0_CR_PMFSM1_clkalign_ovren_HSH                        (0x010E01DC)

  #define DATA0CH0_CR_PMFSM1_start_clkalign_ovrval_OFF                 ( 8)
  #define DATA0CH0_CR_PMFSM1_start_clkalign_ovrval_WID                 ( 1)
  #define DATA0CH0_CR_PMFSM1_start_clkalign_ovrval_MSK                 (0x00000100)
  #define DATA0CH0_CR_PMFSM1_start_clkalign_ovrval_MIN                 (0)
  #define DATA0CH0_CR_PMFSM1_start_clkalign_ovrval_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_start_clkalign_ovrval_DEF                 (0x00000000)
  #define DATA0CH0_CR_PMFSM1_start_clkalign_ovrval_HSH                 (0x011001DC)

  #define DATA0CH0_CR_PMFSM1_clkalignrst_ovrval_OFF                    ( 9)
  #define DATA0CH0_CR_PMFSM1_clkalignrst_ovrval_WID                    ( 1)
  #define DATA0CH0_CR_PMFSM1_clkalignrst_ovrval_MSK                    (0x00000200)
  #define DATA0CH0_CR_PMFSM1_clkalignrst_ovrval_MIN                    (0)
  #define DATA0CH0_CR_PMFSM1_clkalignrst_ovrval_MAX                    (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_clkalignrst_ovrval_DEF                    (0x00000000)
  #define DATA0CH0_CR_PMFSM1_clkalignrst_ovrval_HSH                    (0x011201DC)

  #define DATA0CH0_CR_PMFSM1_func_rst_ovren_OFF                        (10)
  #define DATA0CH0_CR_PMFSM1_func_rst_ovren_WID                        ( 1)
  #define DATA0CH0_CR_PMFSM1_func_rst_ovren_MSK                        (0x00000400)
  #define DATA0CH0_CR_PMFSM1_func_rst_ovren_MIN                        (0)
  #define DATA0CH0_CR_PMFSM1_func_rst_ovren_MAX                        (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_func_rst_ovren_DEF                        (0x00000000)
  #define DATA0CH0_CR_PMFSM1_func_rst_ovren_HSH                        (0x011401DC)

  #define DATA0CH0_CR_PMFSM1_func_rst_ovrval_OFF                       (11)
  #define DATA0CH0_CR_PMFSM1_func_rst_ovrval_WID                       ( 1)
  #define DATA0CH0_CR_PMFSM1_func_rst_ovrval_MSK                       (0x00000800)
  #define DATA0CH0_CR_PMFSM1_func_rst_ovrval_MIN                       (0)
  #define DATA0CH0_CR_PMFSM1_func_rst_ovrval_MAX                       (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_func_rst_ovrval_DEF                       (0x00000000)
  #define DATA0CH0_CR_PMFSM1_func_rst_ovrval_HSH                       (0x011601DC)

  #define DATA0CH0_CR_PMFSM1_pien_ovren_OFF                            (12)
  #define DATA0CH0_CR_PMFSM1_pien_ovren_WID                            ( 1)
  #define DATA0CH0_CR_PMFSM1_pien_ovren_MSK                            (0x00001000)
  #define DATA0CH0_CR_PMFSM1_pien_ovren_MIN                            (0)
  #define DATA0CH0_CR_PMFSM1_pien_ovren_MAX                            (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_pien_ovren_DEF                            (0x00000000)
  #define DATA0CH0_CR_PMFSM1_pien_ovren_HSH                            (0x011801DC)

  #define DATA0CH0_CR_PMFSM1_pien_ovrval_OFF                           (13)
  #define DATA0CH0_CR_PMFSM1_pien_ovrval_WID                           ( 1)
  #define DATA0CH0_CR_PMFSM1_pien_ovrval_MSK                           (0x00002000)
  #define DATA0CH0_CR_PMFSM1_pien_ovrval_MIN                           (0)
  #define DATA0CH0_CR_PMFSM1_pien_ovrval_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_pien_ovrval_DEF                           (0x00000000)
  #define DATA0CH0_CR_PMFSM1_pien_ovrval_HSH                           (0x011A01DC)

  #define DATA0CH0_CR_PMFSM1_dllen_ovren_OFF                           (14)
  #define DATA0CH0_CR_PMFSM1_dllen_ovren_WID                           ( 1)
  #define DATA0CH0_CR_PMFSM1_dllen_ovren_MSK                           (0x00004000)
  #define DATA0CH0_CR_PMFSM1_dllen_ovren_MIN                           (0)
  #define DATA0CH0_CR_PMFSM1_dllen_ovren_MAX                           (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_dllen_ovren_DEF                           (0x00000000)
  #define DATA0CH0_CR_PMFSM1_dllen_ovren_HSH                           (0x011C01DC)

  #define DATA0CH0_CR_PMFSM1_dllen_ovrval_OFF                          (15)
  #define DATA0CH0_CR_PMFSM1_dllen_ovrval_WID                          ( 1)
  #define DATA0CH0_CR_PMFSM1_dllen_ovrval_MSK                          (0x00008000)
  #define DATA0CH0_CR_PMFSM1_dllen_ovrval_MIN                          (0)
  #define DATA0CH0_CR_PMFSM1_dllen_ovrval_MAX                          (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_dllen_ovrval_DEF                          (0x00000000)
  #define DATA0CH0_CR_PMFSM1_dllen_ovrval_HSH                          (0x011E01DC)

  #define DATA0CH0_CR_PMFSM1_ldoen_ovrval_OFF                          (16)
  #define DATA0CH0_CR_PMFSM1_ldoen_ovrval_WID                          ( 1)
  #define DATA0CH0_CR_PMFSM1_ldoen_ovrval_MSK                          (0x00010000)
  #define DATA0CH0_CR_PMFSM1_ldoen_ovrval_MIN                          (0)
  #define DATA0CH0_CR_PMFSM1_ldoen_ovrval_MAX                          (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_ldoen_ovrval_DEF                          (0x00000000)
  #define DATA0CH0_CR_PMFSM1_ldoen_ovrval_HSH                          (0x012001DC)

  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovren_OFF                  (17)
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovren_WID                  ( 1)
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovren_MSK                  (0x00020000)
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovren_MIN                  (0)
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovren_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovren_DEF                  (0x00000000)
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovren_HSH                  (0x012201DC)

  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovrval_OFF                 (18)
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovrval_WID                 ( 1)
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovrval_MSK                 (0x00040000)
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovrval_MIN                 (0)
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovrval_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovrval_DEF                 (0x00000000)
  #define DATA0CH0_CR_PMFSM1_Vsshipowergood_ovrval_HSH                 (0x012401DC)

  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovren_OFF               (19)
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovren_WID               ( 1)
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovren_MSK               (0x00080000)
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovren_MIN               (0)
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovren_MAX               (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovren_DEF               (0x00000000)
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovren_HSH               (0x012601DC)

  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovrval_OFF              (20)
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovrval_WID              ( 1)
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovrval_MSK              (0x00100000)
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovrval_MIN              (0)
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovrval_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovrval_DEF              (0x00000000)
  #define DATA0CH0_CR_PMFSM1_clkalign_complete_ovrval_HSH              (0x012801DC)

  #define DATA0CH0_CR_PMFSM1_mdlllock_ovrval_OFF                       (21)
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovrval_WID                       ( 1)
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovrval_MSK                       (0x00200000)
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovrval_MIN                       (0)
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovrval_MAX                       (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovrval_DEF                       (0x00000000)
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovrval_HSH                       (0x012A01DC)

  #define DATA0CH0_CR_PMFSM1_mdlllock_ovren_OFF                        (22)
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovren_WID                        ( 1)
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovren_MSK                        (0x00400000)
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovren_MIN                        (0)
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovren_MAX                        (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovren_DEF                        (0x00000000)
  #define DATA0CH0_CR_PMFSM1_mdlllock_ovren_HSH                        (0x012C01DC)

  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovren_OFF                  (23)
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovren_WID                  ( 1)
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovren_MSK                  (0x00800000)
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovren_MIN                  (0)
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovren_MAX                  (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovren_DEF                  (0x00000000)
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovren_HSH                  (0x012E01DC)

  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovrval_OFF                 (24)
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovrval_WID                 ( 1)
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovrval_MSK                 (0x01000000)
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovrval_MIN                 (0)
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovrval_MAX                 (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovrval_DEF                 (0x00000000)
  #define DATA0CH0_CR_PMFSM1_compupdatedone_ovrval_HSH                 (0x013001DC)

  #define DATA0CH0_CR_PMFSM1_scr_gvblock_ovrride_OFF                   (25)
  #define DATA0CH0_CR_PMFSM1_scr_gvblock_ovrride_WID                   ( 1)
  #define DATA0CH0_CR_PMFSM1_scr_gvblock_ovrride_MSK                   (0x02000000)
  #define DATA0CH0_CR_PMFSM1_scr_gvblock_ovrride_MIN                   (0)
  #define DATA0CH0_CR_PMFSM1_scr_gvblock_ovrride_MAX                   (1) // 0x00000001
  #define DATA0CH0_CR_PMFSM1_scr_gvblock_ovrride_DEF                   (0x00000000)
  #define DATA0CH0_CR_PMFSM1_scr_gvblock_ovrride_HSH                   (0x013201DC)

  #define DATA0CH0_CR_PMFSM1_spare_OFF                                 (26)
  #define DATA0CH0_CR_PMFSM1_spare_WID                                 ( 6)
  #define DATA0CH0_CR_PMFSM1_spare_MSK                                 (0xFC000000)
  #define DATA0CH0_CR_PMFSM1_spare_MIN                                 (0)
  #define DATA0CH0_CR_PMFSM1_spare_MAX                                 (63) // 0x0000003F
  #define DATA0CH0_CR_PMFSM1_spare_DEF                                 (0x00000000)
  #define DATA0CH0_CR_PMFSM1_spare_HSH                                 (0x063401DC)

#define DATA0CH0_CR_CLKALIGNSTATUS_REG                                 (0x000001E0)

  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_OFF     ( 0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_WID     ( 1)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_MSK     (0x00000001)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_MIN     (0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_MAX     (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_DEF     (0x00000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_HSH     (0x010001E0)

  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_complete_level_OFF       ( 1)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_complete_level_WID       ( 1)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_complete_level_MSK       (0x00000002)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_complete_level_MIN       (0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_complete_level_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_complete_level_DEF       (0x00000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalign_complete_level_HSH       (0x010201E0)

  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_err_OFF            ( 2)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_err_WID            ( 1)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_err_MSK            (0x00000004)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_err_MIN            (0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_err_MAX            (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_err_DEF            (0x00000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_err_HSH            (0x010401E0)

  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_one_count_OFF      ( 3)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_one_count_WID      ( 5)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_one_count_MSK      (0x000000F8)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_one_count_MIN      (0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_one_count_MAX      (31) // 0x0000001F
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_one_count_DEF      (0x00000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_one_count_HSH      (0x050601E0)

  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_OFF     ( 8)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_WID     ( 5)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_MSK     (0x00001F00)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_MIN     (0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_MAX     (31) // 0x0000001F
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_DEF     (0x00000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_HSH     (0x051001E0)

  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_OFF    (13)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_WID    ( 1)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_MSK    (0x00002000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_MIN    (0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_MAX    (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_DEF    (0x00000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_HSH    (0x011A01E0)

  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe1_OFF          (14)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe1_WID          ( 1)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe1_MSK          (0x00004000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe1_MIN          (0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe1_MAX          (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe1_DEF          (0x00000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe1_HSH          (0x011C01E0)

  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe0_OFF          (15)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe0_WID          ( 1)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe0_MSK          (0x00008000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe0_MIN          (0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe0_MAX          (1) // 0x00000001
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe0_DEF          (0x00000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_clkalignstatus_safe0_HSH          (0x011E01E0)

  #define DATA0CH0_CR_CLKALIGNSTATUS_ref2xclkpicode_OFF                (16)
  #define DATA0CH0_CR_CLKALIGNSTATUS_ref2xclkpicode_WID                ( 8)
  #define DATA0CH0_CR_CLKALIGNSTATUS_ref2xclkpicode_MSK                (0x00FF0000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_ref2xclkpicode_MIN                (0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_ref2xclkpicode_MAX                (255) // 0x000000FF
  #define DATA0CH0_CR_CLKALIGNSTATUS_ref2xclkpicode_DEF                (0x00000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_ref2xclkpicode_HSH                (0x082001E0)

  #define DATA0CH0_CR_CLKALIGNSTATUS_Reserved27_OFF                    (24)
  #define DATA0CH0_CR_CLKALIGNSTATUS_Reserved27_WID                    ( 8)
  #define DATA0CH0_CR_CLKALIGNSTATUS_Reserved27_MSK                    (0xFF000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_Reserved27_MIN                    (0)
  #define DATA0CH0_CR_CLKALIGNSTATUS_Reserved27_MAX                    (255) // 0x000000FF
  #define DATA0CH0_CR_CLKALIGNSTATUS_Reserved27_DEF                    (0x00000000)
  #define DATA0CH0_CR_CLKALIGNSTATUS_Reserved27_HSH                    (0x083001E0)

#define DATA0CH0_CR_DATATRAINFEEDBACK_REG                              (0x000001E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankovrd_OFF                   ( 0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankovrd_WID                   ( 1)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankovrd_MSK                   (0x00000001)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankovrd_MIN                   (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankovrd_MAX                   (1) // 0x00000001
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankovrd_DEF                   (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankovrd_HSH                   (0x010001E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankvalue_OFF                  ( 1)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankvalue_WID                  ( 2)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankvalue_MSK                  (0x00000006)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankvalue_MIN                  (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankvalue_MAX                  (3) // 0x00000003
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankvalue_DEF                  (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rankvalue_HSH                  (0x020201E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxampoffseten_OFF              ( 3)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxampoffseten_WID              ( 2)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxampoffseten_MSK              (0x00000018)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxampoffseten_MIN              (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxampoffseten_MAX              (3) // 0x00000003
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxampoffseten_DEF              (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxampoffseten_HSH              (0x020601E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxtrainingmode_OFF             ( 5)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxtrainingmode_WID             ( 1)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxtrainingmode_MSK             (0x00000020)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxtrainingmode_MIN             (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxtrainingmode_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxtrainingmode_DEF             (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rxtrainingmode_HSH             (0x010A01E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_wltrainingmode_OFF             ( 6)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_wltrainingmode_WID             ( 2)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_wltrainingmode_MSK             (0x000000C0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_wltrainingmode_MIN             (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_wltrainingmode_MAX             (3) // 0x00000003
  #define DATA0CH0_CR_DATATRAINFEEDBACK_wltrainingmode_DEF             (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_wltrainingmode_HSH             (0x020C01E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_rltrainingmode_OFF             ( 8)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rltrainingmode_WID             ( 1)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rltrainingmode_MSK             (0x00000100)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rltrainingmode_MIN             (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rltrainingmode_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rltrainingmode_DEF             (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_rltrainingmode_HSH             (0x011001E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_senseamptrainingmode_OFF       ( 9)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_senseamptrainingmode_WID       ( 1)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_senseamptrainingmode_MSK       (0x00000200)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_senseamptrainingmode_MIN       (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_senseamptrainingmode_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_DATATRAINFEEDBACK_senseamptrainingmode_DEF       (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_senseamptrainingmode_HSH       (0x011201E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_catrainingmode_OFF             (10)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_catrainingmode_WID             ( 1)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_catrainingmode_MSK             (0x00000400)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_catrainingmode_MIN             (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_catrainingmode_MAX             (1) // 0x00000001
  #define DATA0CH0_CR_DATATRAINFEEDBACK_catrainingmode_DEF             (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_catrainingmode_HSH             (0x011401E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_datatrainfeedback_OFF          (11)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_datatrainfeedback_WID          (10)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_datatrainfeedback_MSK          (0x001FF800)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_datatrainfeedback_MIN          (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_datatrainfeedback_MAX          (1023) // 0x000003FF
  #define DATA0CH0_CR_DATATRAINFEEDBACK_datatrainfeedback_DEF          (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_datatrainfeedback_HSH          (0x0A1601E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_spare_OFF                      (21)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_spare_WID                      ( 1)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_spare_MSK                      (0x00200000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_spare_MIN                      (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_spare_MAX                      (1) // 0x00000001
  #define DATA0CH0_CR_DATATRAINFEEDBACK_spare_DEF                      (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_spare_HSH                      (0x012A01E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrddata_OFF         (22)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrddata_WID         ( 8)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrddata_MSK         (0x3FC00000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrddata_MIN         (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrddata_MAX         (255) // 0x000000FF
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrddata_DEF         (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrddata_HSH         (0x082C01E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrdmodeen_OFF       (30)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrdmodeen_WID       ( 1)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrdmodeen_MSK       (0x40000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrdmodeen_MIN       (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrdmodeen_MAX       (1) // 0x00000001
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrdmodeen_DEF       (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrdmodeen_HSH       (0x013C01E4)

  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqrxsdlbypassen_OFF         (31)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqrxsdlbypassen_WID         ( 1)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqrxsdlbypassen_MSK         (0x80000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqrxsdlbypassen_MIN         (0)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqrxsdlbypassen_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqrxsdlbypassen_DEF         (0x00000000)
  #define DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqrxsdlbypassen_HSH         (0x013E01E4)

#define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG                        (0x000001E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_runtest_OFF              ( 0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_runtest_WID              ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_runtest_MSK              (0x00000001)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_runtest_MIN              (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_runtest_MAX              (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_runtest_DEF              (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_runtest_HSH              (0x010001E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_param_OFF                ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_param_WID                ( 3)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_param_MSK                (0x0000000E)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_param_MIN                (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_param_MAX                (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_param_DEF                (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_param_HSH                (0x030201E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_A0_OFF               ( 4)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_A0_WID               ( 7)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_A0_MSK               (0x000007F0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_A0_MIN               (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_A0_MAX               (127) // 0x0000007F
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_A0_DEF               (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_A0_HSH               (0x070801E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_A0_OFF               (11)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_A0_WID               ( 7)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_A0_MSK               (0x0003F800)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_A0_MIN               (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_A0_MAX               (127) // 0x0000007F
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_A0_DEF               (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_A0_HSH               (0x071601E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved30_A0_OFF           (18)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved30_A0_WID           ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved30_A0_MSK           (0x00040000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved30_A0_MIN           (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved30_A0_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved30_A0_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved30_A0_HSH           (0x012401E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_A0_OFF           (19)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_A0_WID           ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_A0_MSK           (0x00080000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_A0_MIN           (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_A0_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_A0_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_A0_HSH           (0x012601E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_overflow_status_A0_OFF      (21)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_overflow_status_A0_WID      ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_overflow_status_A0_MSK      (0x00200000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_overflow_status_A0_MIN      (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_overflow_status_A0_MAX      (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_overflow_status_A0_DEF      (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_overflow_status_A0_HSH      (0x012A01E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_A0_OFF           (22)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_A0_WID           ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_A0_MSK           (0x00400000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_A0_MIN           (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_A0_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_A0_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_A0_HSH           (0x012C01E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_A0_OFF           (23)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_A0_WID           ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_A0_MSK           (0x00800000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_A0_MIN           (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_A0_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_A0_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_A0_HSH           (0x012E01E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_iolbcycles_A0_OFF           (24)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_iolbcycles_A0_WID           ( 4)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_iolbcycles_A0_MSK           (0x0F000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_iolbcycles_A0_MIN           (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_iolbcycles_A0_MAX           (15) // 0x0000000F
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_iolbcycles_A0_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_iolbcycles_A0_HSH           (0x043001E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_OFF               ( 4)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_WID               ( 8)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_MSK               (0x00000FF0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_MIN               (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_MAX               (255) // 0x000000FF
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_DEF               (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_minval_HSH               (0x080801E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_OFF               (12)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_WID               ( 8)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_MSK               (0x000FF000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_MIN               (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_MAX               (255) // 0x000000FF
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_DEF               (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_maxval_HSH               (0x081801E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_forceonrcven_OFF         (20)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_forceonrcven_WID         ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_forceonrcven_MSK         (0x00100000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_forceonrcven_MIN         (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_forceonrcven_MAX         (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_forceonrcven_DEF         (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_forceonrcven_HSH         (0x012801E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_OFF           (21)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_WID           ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_MSK           (0x00200000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_MIN           (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved29_HSH           (0x012A01E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_settle_time_adj_OFF      (22)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_settle_time_adj_WID      ( 2)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_settle_time_adj_MSK      (0x00C00000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_settle_time_adj_MIN      (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_settle_time_adj_MAX      (3) // 0x00000003
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_settle_time_adj_DEF      (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_settle_time_adj_HSH      (0x022C01E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_OFF           (24)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_WID           ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_MSK           (0x01000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_MIN           (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_calccenter_HSH           (0x013001E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_skip_count_OFF           (25)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_skip_count_WID           ( 3)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_skip_count_MSK           (0x0E000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_skip_count_MIN           (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_skip_count_MAX           (7) // 0x00000007
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_skip_count_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_skip_count_HSH           (0x033201E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_laneresult_OFF           (28)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_laneresult_WID           ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_laneresult_MSK           (0x10000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_laneresult_MIN           (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_laneresult_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_laneresult_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_laneresult_HSH           (0x013801E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data0_OFF                (29)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data0_WID                ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data0_MSK                (0x20000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data0_MIN                (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data0_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data0_DEF                (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data0_HSH                (0x013A01E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data1_OFF                (30)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data1_WID                ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data1_MSK                (0x40000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data1_MIN                (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data1_MAX                (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data1_DEF                (0x00000001)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_data1_HSH                (0x013C01E8)

  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_OFF           (31)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_WID           ( 1)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_MSK           (0x80000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_MIN           (0)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_MAX           (1) // 0x00000001
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_DEF           (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODECONTROL0_Reserved28_HSH           (0x013E01E8)

#define DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x000001EC)

  #define DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_result_OFF              ( 0)
  #define DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_result_WID              (32)
  #define DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_result_MSK              (0xFFFFFFFF)
  #define DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_result_MIN              (0)
  #define DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_result_MAX              (4294967295) // 0xFFFFFFFF
  #define DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_result_DEF              (0x00000000)
  #define DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_result_HSH              (0x200001EC)

#define DATA0CH0_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x000001F0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA0CH0_CR_DCCCTL14_REG                                       (0x000001F4)

  #define DATA0CH0_CR_DCCCTL14_maxdccsteps_OFF                         ( 0)
  #define DATA0CH0_CR_DCCCTL14_maxdccsteps_WID                         ( 7)
  #define DATA0CH0_CR_DCCCTL14_maxdccsteps_MSK                         (0x0000007F)
  #define DATA0CH0_CR_DCCCTL14_maxdccsteps_MIN                         (0)
  #define DATA0CH0_CR_DCCCTL14_maxdccsteps_MAX                         (127) // 0x0000007F
  #define DATA0CH0_CR_DCCCTL14_maxdccsteps_DEF                         (0x00000000)
  #define DATA0CH0_CR_DCCCTL14_maxdccsteps_HSH                         (0x070001F4)

  #define DATA0CH0_CR_DCCCTL14_reserved30_OFF                          ( 7)
  #define DATA0CH0_CR_DCCCTL14_reserved30_WID                          (25)
  #define DATA0CH0_CR_DCCCTL14_reserved30_MSK                          (0xFFFFFF80)
  #define DATA0CH0_CR_DCCCTL14_reserved30_MIN                          (0)
  #define DATA0CH0_CR_DCCCTL14_reserved30_MAX                          (33554431) // 0x01FFFFFF
  #define DATA0CH0_CR_DCCCTL14_reserved30_DEF                          (0x00000000)
  #define DATA0CH0_CR_DCCCTL14_reserved30_HSH                          (0x190E01F4)

#define DATA0CH1_CR_DDRDATADQRANK0LANE0_REG                            (0x00000200)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK0LANE1_REG                            (0x00000204)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK0LANE2_REG                            (0x00000208)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK0LANE3_REG                            (0x0000020C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK0LANE4_REG                            (0x00000210)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK0LANE5_REG                            (0x00000214)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK0LANE6_REG                            (0x00000218)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK0LANE7_REG                            (0x0000021C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK1LANE0_REG                            (0x00000220)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK1LANE1_REG                            (0x00000224)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK1LANE2_REG                            (0x00000228)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK1LANE3_REG                            (0x0000022C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK1LANE4_REG                            (0x00000230)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK1LANE5_REG                            (0x00000234)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK1LANE6_REG                            (0x00000238)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK1LANE7_REG                            (0x0000023C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK2LANE0_REG                            (0x00000240)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK2LANE1_REG                            (0x00000244)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK2LANE2_REG                            (0x00000248)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK2LANE3_REG                            (0x0000024C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK2LANE4_REG                            (0x00000250)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK2LANE5_REG                            (0x00000254)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK2LANE6_REG                            (0x00000258)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK2LANE7_REG                            (0x0000025C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK3LANE0_REG                            (0x00000260)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK3LANE1_REG                            (0x00000264)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK3LANE2_REG                            (0x00000268)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK3LANE3_REG                            (0x0000026C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK3LANE4_REG                            (0x00000270)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK3LANE5_REG                            (0x00000274)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK3LANE6_REG                            (0x00000278)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRDATADQRANK3LANE7_REG                            (0x0000027C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA0CH1_CR_DDRCRDATACONTROL0_REG                              (0x00000280)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA0CH1_CR_DDRCRDATACONTROL1_REG                              (0x00000284)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA0CH1_CR_DDRCRDATACONTROL2_REG                              (0x00000288)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA0CH1_CR_DDRCRDATACONTROL3_REG                              (0x0000028C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA0CH1_CR_AFEMISC_REG                                        (0x00000290)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA0CH1_CR_SRZCTL_REG                                         (0x00000294)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA0CH1_CR_DQRXCTL0_REG                                       (0x00000298)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA0CH1_CR_DQRXCTL1_REG                                       (0x0000029C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA0CH1_CR_DQSTXRXCTL_REG                                     (0x000002A0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA0CH1_CR_DQSTXRXCTL0_REG                                    (0x000002A4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA0CH1_CR_SDLCTL1_REG                                        (0x000002A8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA0CH1_CR_SDLCTL0_REG                                        (0x000002AC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA0CH1_CR_MDLLCTL0_REG                                       (0x000002B0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA0CH1_CR_MDLLCTL1_REG                                       (0x000002B4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA0CH1_CR_MDLLCTL2_REG                                       (0x000002B8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA0CH1_CR_MDLLCTL3_REG                                       (0x000002BC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA0CH1_CR_MDLLCTL4_REG                                       (0x000002C0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA0CH1_CR_MDLLCTL5_REG                                       (0x000002C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA0CH1_CR_DIGMISCCTRL_REG                                    (0x000002C4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA0CH1_CR_AFEMISCCTRL2_REG                                   (0x000002C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA0CH1_CR_TXPBDOFFSET0_REG                                   (0x000002CC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA0CH1_CR_TXPBDOFFSET1_REG                                   (0x000002D0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA0CH1_CR_COMPCTL0_REG                                       (0x000002D4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA0CH1_CR_COMPCTL1_REG                                       (0x000002D8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA0CH1_CR_COMPCTL2_REG                                       (0x000002DC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA0CH1_CR_DCCCTL5_REG                                        (0x000002E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA0CH1_CR_DCCCTL6_REG                                        (0x000002E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA0CH1_CR_DCCCTL7_REG                                        (0x000002E8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA0CH1_CR_DCCCTL8_REG                                        (0x000002EC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA0CH1_CR_DCCCTL9_REG                                        (0x000002F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA0CH1_CR_RXCONTROL0RANK0_REG                                (0x000002F4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA0CH1_CR_RXCONTROL0RANK1_REG                                (0x000002F8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA0CH1_CR_RXCONTROL0RANK2_REG                                (0x000002FC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA0CH1_CR_RXCONTROL0RANK3_REG                                (0x00000300)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA0CH1_CR_TXCONTROL0_PH90_REG                                (0x00000304)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA0CH1_CR_TXCONTROL0RANK0_REG                                (0x00000308)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA0CH1_CR_TXCONTROL0RANK1_REG                                (0x0000030C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA0CH1_CR_TXCONTROL0RANK2_REG                                (0x00000310)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA0CH1_CR_TXCONTROL0RANK3_REG                                (0x00000314)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA0CH1_CR_DDRDATADQSRANKX_REG                                (0x00000318)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA0CH1_CR_DDRDATADQSPH90RANKX_REG                            (0x0000031C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA0CH1_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00000320)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA0CH1_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00000324)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA0CH1_CR_DDRCRCMDBUSTRAIN_REG                               (0x00000328)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA0CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x0000032C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA0CH1_CR_DDRCRWRRETRAINRANK3_REG                            (0x00000330)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA0CH1_CR_DDRCRWRRETRAINRANK2_REG                            (0x00000334)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA0CH1_CR_DDRCRWRRETRAINRANK1_REG                            (0x00000338)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA0CH1_CR_DDRCRWRRETRAINRANK0_REG                            (0x0000033C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA0CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00000340)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA0CH1_CR_TXCONTROL1_PH90_REG                                (0x00000344)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA0CH1_CR_AFEMISCCTRL1_REG                                   (0x00000348)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA0CH1_CR_DQXRXAMPCTL_REG                                    (0x0000034C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA0CH1_CR_DQ0RXAMPCTL_REG                                    (0x00000350)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA0CH1_CR_DQ1RXAMPCTL_REG                                    (0x00000354)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA0CH1_CR_DQ2RXAMPCTL_REG                                    (0x00000358)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA0CH1_CR_DQ3RXAMPCTL_REG                                    (0x0000035C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA0CH1_CR_DQ4RXAMPCTL_REG                                    (0x00000360)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA0CH1_CR_DQ5RXAMPCTL_REG                                    (0x00000364)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA0CH1_CR_DQ6RXAMPCTL_REG                                    (0x00000368)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA0CH1_CR_DQ7RXAMPCTL_REG                                    (0x0000036C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA0CH1_CR_DQRXAMPCTL0_REG                                    (0x00000370)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA0CH1_CR_DQRXAMPCTL1_REG                                    (0x00000374)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA0CH1_CR_DQRXAMPCTL2_REG                                    (0x00000378)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA0CH1_CR_DQTXCTL0_REG                                       (0x0000037C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA0CH1_CR_DCCCTL0_REG                                        (0x00000380)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA0CH1_CR_DCCCTL1_REG                                        (0x00000384)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA0CH1_CR_DCCCTL2_REG                                        (0x00000388)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA0CH1_CR_DCCCTL3_REG                                        (0x0000038C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA0CH1_CR_DCCCTL10_REG                                       (0x00000390)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA0CH1_CR_DCCCTL12_REG                                       (0x00000394)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA0CH1_CR_CLKALIGNCTL0_REG                                   (0x00000398)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA0CH1_CR_CLKALIGNCTL1_REG                                   (0x0000039C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA0CH1_CR_CLKALIGNCTL2_REG                                   (0x000003A0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA0CH1_CR_DCCCTL4_REG                                        (0x000003A4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA0CH1_CR_DDRCRDATACOMPVTT_REG                               (0x000003A8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA0CH1_CR_WLCTRL_REG                                         (0x000003AC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA0CH1_CR_LPMODECTL_REG                                      (0x000003B0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA0CH1_CR_DDRCRDATACOMP0_REG                                 (0x000003B4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA0CH1_CR_DDRCRDATACOMP1_REG                                 (0x000003B8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA0CH1_CR_VCCDLLCOMPDATA_REG                                 (0x000003BC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA0CH1_CR_DCSOFFSET_REG                                      (0x000003C0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA0CH1_CR_DFXCCMON_REG                                       (0x000003C4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA0CH1_CR_AFEMISCCTRL0_REG                                   (0x000003C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA0CH1_CR_VIEWCTL_REG                                        (0x000003CC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA0CH1_CR_DCCCTL11_REG                                       (0x000003D0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA0CH1_CR_DCCCTL13_REG                                       (0x000003D4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA0CH1_CR_PMFSM_REG                                          (0x000003D8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA0CH1_CR_PMFSM1_REG                                         (0x000003DC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA0CH1_CR_CLKALIGNSTATUS_REG                                 (0x000003E0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA0CH1_CR_DATATRAINFEEDBACK_REG                              (0x000003E4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA0CH1_CR_DDRCRMARGINMODECONTROL0_REG                        (0x000003E8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA0CH1_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x000003EC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA0CH1_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x000003F0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA0CH1_CR_DCCCTL14_REG                                       (0x000003F4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA1CH0_CR_DDRDATADQRANK0LANE0_REG                            (0x00000400)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK0LANE1_REG                            (0x00000404)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK0LANE2_REG                            (0x00000408)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK0LANE3_REG                            (0x0000040C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK0LANE4_REG                            (0x00000410)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK0LANE5_REG                            (0x00000414)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK0LANE6_REG                            (0x00000418)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK0LANE7_REG                            (0x0000041C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK1LANE0_REG                            (0x00000420)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK1LANE1_REG                            (0x00000424)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK1LANE2_REG                            (0x00000428)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK1LANE3_REG                            (0x0000042C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK1LANE4_REG                            (0x00000430)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK1LANE5_REG                            (0x00000434)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK1LANE6_REG                            (0x00000438)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK1LANE7_REG                            (0x0000043C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK2LANE0_REG                            (0x00000440)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK2LANE1_REG                            (0x00000444)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK2LANE2_REG                            (0x00000448)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK2LANE3_REG                            (0x0000044C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK2LANE4_REG                            (0x00000450)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK2LANE5_REG                            (0x00000454)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK2LANE6_REG                            (0x00000458)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK2LANE7_REG                            (0x0000045C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK3LANE0_REG                            (0x00000460)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK3LANE1_REG                            (0x00000464)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK3LANE2_REG                            (0x00000468)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK3LANE3_REG                            (0x0000046C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK3LANE4_REG                            (0x00000470)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK3LANE5_REG                            (0x00000474)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK3LANE6_REG                            (0x00000478)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRDATADQRANK3LANE7_REG                            (0x0000047C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH0_CR_DDRCRDATACONTROL0_REG                              (0x00000480)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA1CH0_CR_DDRCRDATACONTROL1_REG                              (0x00000484)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA1CH0_CR_DDRCRDATACONTROL2_REG                              (0x00000488)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA1CH0_CR_DDRCRDATACONTROL3_REG                              (0x0000048C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA1CH0_CR_AFEMISC_REG                                        (0x00000490)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA1CH0_CR_SRZCTL_REG                                         (0x00000494)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA1CH0_CR_DQRXCTL0_REG                                       (0x00000498)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA1CH0_CR_DQRXCTL1_REG                                       (0x0000049C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA1CH0_CR_DQSTXRXCTL_REG                                     (0x000004A0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA1CH0_CR_DQSTXRXCTL0_REG                                    (0x000004A4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA1CH0_CR_SDLCTL1_REG                                        (0x000004A8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA1CH0_CR_SDLCTL0_REG                                        (0x000004AC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA1CH0_CR_MDLLCTL0_REG                                       (0x000004B0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA1CH0_CR_MDLLCTL1_REG                                       (0x000004B4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA1CH0_CR_MDLLCTL2_REG                                       (0x000004B8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA1CH0_CR_MDLLCTL3_REG                                       (0x000004BC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA1CH0_CR_MDLLCTL4_REG                                       (0x000004C0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA1CH0_CR_MDLLCTL5_REG                                       (0x000004C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA1CH0_CR_DIGMISCCTRL_REG                                    (0x000004C4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA1CH0_CR_AFEMISCCTRL2_REG                                   (0x000004C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA1CH0_CR_TXPBDOFFSET0_REG                                   (0x000004CC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA1CH0_CR_TXPBDOFFSET1_REG                                   (0x000004D0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA1CH0_CR_COMPCTL0_REG                                       (0x000004D4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA1CH0_CR_COMPCTL1_REG                                       (0x000004D8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA1CH0_CR_COMPCTL2_REG                                       (0x000004DC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA1CH0_CR_DCCCTL5_REG                                        (0x000004E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA1CH0_CR_DCCCTL6_REG                                        (0x000004E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA1CH0_CR_DCCCTL7_REG                                        (0x000004E8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA1CH0_CR_DCCCTL8_REG                                        (0x000004EC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA1CH0_CR_DCCCTL9_REG                                        (0x000004F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA1CH0_CR_RXCONTROL0RANK0_REG                                (0x000004F4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA1CH0_CR_RXCONTROL0RANK1_REG                                (0x000004F8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA1CH0_CR_RXCONTROL0RANK2_REG                                (0x000004FC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA1CH0_CR_RXCONTROL0RANK3_REG                                (0x00000500)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA1CH0_CR_TXCONTROL0_PH90_REG                                (0x00000504)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA1CH0_CR_TXCONTROL0RANK0_REG                                (0x00000508)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA1CH0_CR_TXCONTROL0RANK1_REG                                (0x0000050C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA1CH0_CR_TXCONTROL0RANK2_REG                                (0x00000510)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA1CH0_CR_TXCONTROL0RANK3_REG                                (0x00000514)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA1CH0_CR_DDRDATADQSRANKX_REG                                (0x00000518)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA1CH0_CR_DDRDATADQSPH90RANKX_REG                            (0x0000051C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA1CH0_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00000520)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA1CH0_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00000524)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA1CH0_CR_DDRCRCMDBUSTRAIN_REG                               (0x00000528)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA1CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x0000052C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA1CH0_CR_DDRCRWRRETRAINRANK3_REG                            (0x00000530)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA1CH0_CR_DDRCRWRRETRAINRANK2_REG                            (0x00000534)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA1CH0_CR_DDRCRWRRETRAINRANK1_REG                            (0x00000538)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA1CH0_CR_DDRCRWRRETRAINRANK0_REG                            (0x0000053C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA1CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00000540)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA1CH0_CR_TXCONTROL1_PH90_REG                                (0x00000544)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA1CH0_CR_AFEMISCCTRL1_REG                                   (0x00000548)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA1CH0_CR_DQXRXAMPCTL_REG                                    (0x0000054C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA1CH0_CR_DQ0RXAMPCTL_REG                                    (0x00000550)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA1CH0_CR_DQ1RXAMPCTL_REG                                    (0x00000554)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA1CH0_CR_DQ2RXAMPCTL_REG                                    (0x00000558)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA1CH0_CR_DQ3RXAMPCTL_REG                                    (0x0000055C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA1CH0_CR_DQ4RXAMPCTL_REG                                    (0x00000560)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA1CH0_CR_DQ5RXAMPCTL_REG                                    (0x00000564)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA1CH0_CR_DQ6RXAMPCTL_REG                                    (0x00000568)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA1CH0_CR_DQ7RXAMPCTL_REG                                    (0x0000056C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA1CH0_CR_DQRXAMPCTL0_REG                                    (0x00000570)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA1CH0_CR_DQRXAMPCTL1_REG                                    (0x00000574)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA1CH0_CR_DQRXAMPCTL2_REG                                    (0x00000578)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA1CH0_CR_DQTXCTL0_REG                                       (0x0000057C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA1CH0_CR_DCCCTL0_REG                                        (0x00000580)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA1CH0_CR_DCCCTL1_REG                                        (0x00000584)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA1CH0_CR_DCCCTL2_REG                                        (0x00000588)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA1CH0_CR_DCCCTL3_REG                                        (0x0000058C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA1CH0_CR_DCCCTL10_REG                                       (0x00000590)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA1CH0_CR_DCCCTL12_REG                                       (0x00000594)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA1CH0_CR_CLKALIGNCTL0_REG                                   (0x00000598)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA1CH0_CR_CLKALIGNCTL1_REG                                   (0x0000059C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA1CH0_CR_CLKALIGNCTL2_REG                                   (0x000005A0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA1CH0_CR_DCCCTL4_REG                                        (0x000005A4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA1CH0_CR_DDRCRDATACOMPVTT_REG                               (0x000005A8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA1CH0_CR_WLCTRL_REG                                         (0x000005AC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA1CH0_CR_LPMODECTL_REG                                      (0x000005B0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA1CH0_CR_DDRCRDATACOMP0_REG                                 (0x000005B4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA1CH0_CR_DDRCRDATACOMP1_REG                                 (0x000005B8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA1CH0_CR_VCCDLLCOMPDATA_REG                                 (0x000005BC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA1CH0_CR_DCSOFFSET_REG                                      (0x000005C0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA1CH0_CR_DFXCCMON_REG                                       (0x000005C4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA1CH0_CR_AFEMISCCTRL0_REG                                   (0x000005C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA1CH0_CR_VIEWCTL_REG                                        (0x000005CC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA1CH0_CR_DCCCTL11_REG                                       (0x000005D0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA1CH0_CR_DCCCTL13_REG                                       (0x000005D4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA1CH0_CR_PMFSM_REG                                          (0x000005D8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA1CH0_CR_PMFSM1_REG                                         (0x000005DC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA1CH0_CR_CLKALIGNSTATUS_REG                                 (0x000005E0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA1CH0_CR_DATATRAINFEEDBACK_REG                              (0x000005E4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA1CH0_CR_DDRCRMARGINMODECONTROL0_REG                        (0x000005E8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA1CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x000005EC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA1CH0_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x000005F0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA1CH0_CR_DCCCTL14_REG                                       (0x000005F4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA1CH1_CR_DDRDATADQRANK0LANE0_REG                            (0x00000600)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK0LANE1_REG                            (0x00000604)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK0LANE2_REG                            (0x00000608)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK0LANE3_REG                            (0x0000060C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK0LANE4_REG                            (0x00000610)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK0LANE5_REG                            (0x00000614)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK0LANE6_REG                            (0x00000618)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK0LANE7_REG                            (0x0000061C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK1LANE0_REG                            (0x00000620)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK1LANE1_REG                            (0x00000624)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK1LANE2_REG                            (0x00000628)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK1LANE3_REG                            (0x0000062C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK1LANE4_REG                            (0x00000630)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK1LANE5_REG                            (0x00000634)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK1LANE6_REG                            (0x00000638)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK1LANE7_REG                            (0x0000063C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK2LANE0_REG                            (0x00000640)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK2LANE1_REG                            (0x00000644)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK2LANE2_REG                            (0x00000648)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK2LANE3_REG                            (0x0000064C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK2LANE4_REG                            (0x00000650)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK2LANE5_REG                            (0x00000654)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK2LANE6_REG                            (0x00000658)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK2LANE7_REG                            (0x0000065C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK3LANE0_REG                            (0x00000660)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK3LANE1_REG                            (0x00000664)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK3LANE2_REG                            (0x00000668)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK3LANE3_REG                            (0x0000066C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK3LANE4_REG                            (0x00000670)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK3LANE5_REG                            (0x00000674)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK3LANE6_REG                            (0x00000678)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRDATADQRANK3LANE7_REG                            (0x0000067C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA1CH1_CR_DDRCRDATACONTROL0_REG                              (0x00000680)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA1CH1_CR_DDRCRDATACONTROL1_REG                              (0x00000684)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA1CH1_CR_DDRCRDATACONTROL2_REG                              (0x00000688)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA1CH1_CR_DDRCRDATACONTROL3_REG                              (0x0000068C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA1CH1_CR_AFEMISC_REG                                        (0x00000690)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA1CH1_CR_SRZCTL_REG                                         (0x00000694)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA1CH1_CR_DQRXCTL0_REG                                       (0x00000698)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA1CH1_CR_DQRXCTL1_REG                                       (0x0000069C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA1CH1_CR_DQSTXRXCTL_REG                                     (0x000006A0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA1CH1_CR_DQSTXRXCTL0_REG                                    (0x000006A4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA1CH1_CR_SDLCTL1_REG                                        (0x000006A8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA1CH1_CR_SDLCTL0_REG                                        (0x000006AC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA1CH1_CR_MDLLCTL0_REG                                       (0x000006B0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA1CH1_CR_MDLLCTL1_REG                                       (0x000006B4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA1CH1_CR_MDLLCTL2_REG                                       (0x000006B8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA1CH1_CR_MDLLCTL3_REG                                       (0x000006BC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA1CH1_CR_MDLLCTL4_REG                                       (0x000006C0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA1CH1_CR_MDLLCTL5_REG                                       (0x000006C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA1CH1_CR_DIGMISCCTRL_REG                                    (0x000006C4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA1CH1_CR_AFEMISCCTRL2_REG                                   (0x000006C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA1CH1_CR_TXPBDOFFSET0_REG                                   (0x000006CC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA1CH1_CR_TXPBDOFFSET1_REG                                   (0x000006D0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA1CH1_CR_COMPCTL0_REG                                       (0x000006D4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA1CH1_CR_COMPCTL1_REG                                       (0x000006D8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA1CH1_CR_COMPCTL2_REG                                       (0x000006DC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA1CH1_CR_DCCCTL5_REG                                        (0x000006E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA1CH1_CR_DCCCTL6_REG                                        (0x000006E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA1CH1_CR_DCCCTL7_REG                                        (0x000006E8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA1CH1_CR_DCCCTL8_REG                                        (0x000006EC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA1CH1_CR_DCCCTL9_REG                                        (0x000006F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA1CH1_CR_RXCONTROL0RANK0_REG                                (0x000006F4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA1CH1_CR_RXCONTROL0RANK1_REG                                (0x000006F8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA1CH1_CR_RXCONTROL0RANK2_REG                                (0x000006FC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA1CH1_CR_RXCONTROL0RANK3_REG                                (0x00000700)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA1CH1_CR_TXCONTROL0_PH90_REG                                (0x00000704)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA1CH1_CR_TXCONTROL0RANK0_REG                                (0x00000708)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA1CH1_CR_TXCONTROL0RANK1_REG                                (0x0000070C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA1CH1_CR_TXCONTROL0RANK2_REG                                (0x00000710)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA1CH1_CR_TXCONTROL0RANK3_REG                                (0x00000714)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA1CH1_CR_DDRDATADQSRANKX_REG                                (0x00000718)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA1CH1_CR_DDRDATADQSPH90RANKX_REG                            (0x0000071C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA1CH1_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00000720)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA1CH1_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00000724)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA1CH1_CR_DDRCRCMDBUSTRAIN_REG                               (0x00000728)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA1CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x0000072C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA1CH1_CR_DDRCRWRRETRAINRANK3_REG                            (0x00000730)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA1CH1_CR_DDRCRWRRETRAINRANK2_REG                            (0x00000734)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA1CH1_CR_DDRCRWRRETRAINRANK1_REG                            (0x00000738)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA1CH1_CR_DDRCRWRRETRAINRANK0_REG                            (0x0000073C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA1CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00000740)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA1CH1_CR_TXCONTROL1_PH90_REG                                (0x00000744)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA1CH1_CR_AFEMISCCTRL1_REG                                   (0x00000748)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA1CH1_CR_DQXRXAMPCTL_REG                                    (0x0000074C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA1CH1_CR_DQ0RXAMPCTL_REG                                    (0x00000750)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA1CH1_CR_DQ1RXAMPCTL_REG                                    (0x00000754)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA1CH1_CR_DQ2RXAMPCTL_REG                                    (0x00000758)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA1CH1_CR_DQ3RXAMPCTL_REG                                    (0x0000075C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA1CH1_CR_DQ4RXAMPCTL_REG                                    (0x00000760)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA1CH1_CR_DQ5RXAMPCTL_REG                                    (0x00000764)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA1CH1_CR_DQ6RXAMPCTL_REG                                    (0x00000768)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA1CH1_CR_DQ7RXAMPCTL_REG                                    (0x0000076C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA1CH1_CR_DQRXAMPCTL0_REG                                    (0x00000770)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA1CH1_CR_DQRXAMPCTL1_REG                                    (0x00000774)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA1CH1_CR_DQRXAMPCTL2_REG                                    (0x00000778)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA1CH1_CR_DQTXCTL0_REG                                       (0x0000077C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA1CH1_CR_DCCCTL0_REG                                        (0x00000780)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA1CH1_CR_DCCCTL1_REG                                        (0x00000784)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA1CH1_CR_DCCCTL2_REG                                        (0x00000788)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA1CH1_CR_DCCCTL3_REG                                        (0x0000078C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA1CH1_CR_DCCCTL10_REG                                       (0x00000790)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA1CH1_CR_DCCCTL12_REG                                       (0x00000794)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA1CH1_CR_CLKALIGNCTL0_REG                                   (0x00000798)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA1CH1_CR_CLKALIGNCTL1_REG                                   (0x0000079C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA1CH1_CR_CLKALIGNCTL2_REG                                   (0x000007A0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA1CH1_CR_DCCCTL4_REG                                        (0x000007A4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA1CH1_CR_DDRCRDATACOMPVTT_REG                               (0x000007A8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA1CH1_CR_WLCTRL_REG                                         (0x000007AC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA1CH1_CR_LPMODECTL_REG                                      (0x000007B0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA1CH1_CR_DDRCRDATACOMP0_REG                                 (0x000007B4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA1CH1_CR_DDRCRDATACOMP1_REG                                 (0x000007B8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA1CH1_CR_VCCDLLCOMPDATA_REG                                 (0x000007BC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA1CH1_CR_DCSOFFSET_REG                                      (0x000007C0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA1CH1_CR_DFXCCMON_REG                                       (0x000007C4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA1CH1_CR_AFEMISCCTRL0_REG                                   (0x000007C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA1CH1_CR_VIEWCTL_REG                                        (0x000007CC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA1CH1_CR_DCCCTL11_REG                                       (0x000007D0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA1CH1_CR_DCCCTL13_REG                                       (0x000007D4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA1CH1_CR_PMFSM_REG                                          (0x000007D8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA1CH1_CR_PMFSM1_REG                                         (0x000007DC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA1CH1_CR_CLKALIGNSTATUS_REG                                 (0x000007E0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA1CH1_CR_DATATRAINFEEDBACK_REG                              (0x000007E4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA1CH1_CR_DDRCRMARGINMODECONTROL0_REG                        (0x000007E8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA1CH1_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x000007EC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA1CH1_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x000007F0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA1CH1_CR_DCCCTL14_REG                                       (0x000007F4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA2CH0_CR_DDRDATADQRANK0LANE0_REG                            (0x00000800)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK0LANE1_REG                            (0x00000804)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK0LANE2_REG                            (0x00000808)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK0LANE3_REG                            (0x0000080C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK0LANE4_REG                            (0x00000810)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK0LANE5_REG                            (0x00000814)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK0LANE6_REG                            (0x00000818)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK0LANE7_REG                            (0x0000081C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK1LANE0_REG                            (0x00000820)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK1LANE1_REG                            (0x00000824)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK1LANE2_REG                            (0x00000828)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK1LANE3_REG                            (0x0000082C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK1LANE4_REG                            (0x00000830)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK1LANE5_REG                            (0x00000834)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK1LANE6_REG                            (0x00000838)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK1LANE7_REG                            (0x0000083C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK2LANE0_REG                            (0x00000840)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK2LANE1_REG                            (0x00000844)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK2LANE2_REG                            (0x00000848)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK2LANE3_REG                            (0x0000084C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK2LANE4_REG                            (0x00000850)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK2LANE5_REG                            (0x00000854)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK2LANE6_REG                            (0x00000858)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK2LANE7_REG                            (0x0000085C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK3LANE0_REG                            (0x00000860)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK3LANE1_REG                            (0x00000864)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK3LANE2_REG                            (0x00000868)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK3LANE3_REG                            (0x0000086C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK3LANE4_REG                            (0x00000870)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK3LANE5_REG                            (0x00000874)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK3LANE6_REG                            (0x00000878)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRDATADQRANK3LANE7_REG                            (0x0000087C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH0_CR_DDRCRDATACONTROL0_REG                              (0x00000880)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA2CH0_CR_DDRCRDATACONTROL1_REG                              (0x00000884)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA2CH0_CR_DDRCRDATACONTROL2_REG                              (0x00000888)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA2CH0_CR_DDRCRDATACONTROL3_REG                              (0x0000088C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA2CH0_CR_AFEMISC_REG                                        (0x00000890)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA2CH0_CR_SRZCTL_REG                                         (0x00000894)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA2CH0_CR_DQRXCTL0_REG                                       (0x00000898)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA2CH0_CR_DQRXCTL1_REG                                       (0x0000089C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA2CH0_CR_DQSTXRXCTL_REG                                     (0x000008A0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA2CH0_CR_DQSTXRXCTL0_REG                                    (0x000008A4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA2CH0_CR_SDLCTL1_REG                                        (0x000008A8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA2CH0_CR_SDLCTL0_REG                                        (0x000008AC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA2CH0_CR_MDLLCTL0_REG                                       (0x000008B0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA2CH0_CR_MDLLCTL1_REG                                       (0x000008B4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA2CH0_CR_MDLLCTL2_REG                                       (0x000008B8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA2CH0_CR_MDLLCTL3_REG                                       (0x000008BC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA2CH0_CR_MDLLCTL4_REG                                       (0x000008C0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA2CH0_CR_MDLLCTL5_REG                                       (0x000008C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA2CH0_CR_DIGMISCCTRL_REG                                    (0x000008C4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA2CH0_CR_AFEMISCCTRL2_REG                                   (0x000008C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA2CH0_CR_TXPBDOFFSET0_REG                                   (0x000008CC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA2CH0_CR_TXPBDOFFSET1_REG                                   (0x000008D0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA2CH0_CR_COMPCTL0_REG                                       (0x000008D4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA2CH0_CR_COMPCTL1_REG                                       (0x000008D8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA2CH0_CR_COMPCTL2_REG                                       (0x000008DC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA2CH0_CR_DCCCTL5_REG                                        (0x000008E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA2CH0_CR_DCCCTL6_REG                                        (0x000008E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA2CH0_CR_DCCCTL7_REG                                        (0x000008E8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA2CH0_CR_DCCCTL8_REG                                        (0x000008EC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA2CH0_CR_DCCCTL9_REG                                        (0x000008F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA2CH0_CR_RXCONTROL0RANK0_REG                                (0x000008F4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA2CH0_CR_RXCONTROL0RANK1_REG                                (0x000008F8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA2CH0_CR_RXCONTROL0RANK2_REG                                (0x000008FC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA2CH0_CR_RXCONTROL0RANK3_REG                                (0x00000900)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA2CH0_CR_TXCONTROL0_PH90_REG                                (0x00000904)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA2CH0_CR_TXCONTROL0RANK0_REG                                (0x00000908)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA2CH0_CR_TXCONTROL0RANK1_REG                                (0x0000090C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA2CH0_CR_TXCONTROL0RANK2_REG                                (0x00000910)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA2CH0_CR_TXCONTROL0RANK3_REG                                (0x00000914)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA2CH0_CR_DDRDATADQSRANKX_REG                                (0x00000918)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA2CH0_CR_DDRDATADQSPH90RANKX_REG                            (0x0000091C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA2CH0_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00000920)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA2CH0_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00000924)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA2CH0_CR_DDRCRCMDBUSTRAIN_REG                               (0x00000928)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA2CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x0000092C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA2CH0_CR_DDRCRWRRETRAINRANK3_REG                            (0x00000930)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA2CH0_CR_DDRCRWRRETRAINRANK2_REG                            (0x00000934)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA2CH0_CR_DDRCRWRRETRAINRANK1_REG                            (0x00000938)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA2CH0_CR_DDRCRWRRETRAINRANK0_REG                            (0x0000093C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA2CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00000940)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA2CH0_CR_TXCONTROL1_PH90_REG                                (0x00000944)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA2CH0_CR_AFEMISCCTRL1_REG                                   (0x00000948)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA2CH0_CR_DQXRXAMPCTL_REG                                    (0x0000094C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA2CH0_CR_DQ0RXAMPCTL_REG                                    (0x00000950)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA2CH0_CR_DQ1RXAMPCTL_REG                                    (0x00000954)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA2CH0_CR_DQ2RXAMPCTL_REG                                    (0x00000958)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA2CH0_CR_DQ3RXAMPCTL_REG                                    (0x0000095C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA2CH0_CR_DQ4RXAMPCTL_REG                                    (0x00000960)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA2CH0_CR_DQ5RXAMPCTL_REG                                    (0x00000964)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA2CH0_CR_DQ6RXAMPCTL_REG                                    (0x00000968)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA2CH0_CR_DQ7RXAMPCTL_REG                                    (0x0000096C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA2CH0_CR_DQRXAMPCTL0_REG                                    (0x00000970)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA2CH0_CR_DQRXAMPCTL1_REG                                    (0x00000974)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA2CH0_CR_DQRXAMPCTL2_REG                                    (0x00000978)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA2CH0_CR_DQTXCTL0_REG                                       (0x0000097C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA2CH0_CR_DCCCTL0_REG                                        (0x00000980)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA2CH0_CR_DCCCTL1_REG                                        (0x00000984)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA2CH0_CR_DCCCTL2_REG                                        (0x00000988)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA2CH0_CR_DCCCTL3_REG                                        (0x0000098C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA2CH0_CR_DCCCTL10_REG                                       (0x00000990)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA2CH0_CR_DCCCTL12_REG                                       (0x00000994)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA2CH0_CR_CLKALIGNCTL0_REG                                   (0x00000998)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA2CH0_CR_CLKALIGNCTL1_REG                                   (0x0000099C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA2CH0_CR_CLKALIGNCTL2_REG                                   (0x000009A0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA2CH0_CR_DCCCTL4_REG                                        (0x000009A4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA2CH0_CR_DDRCRDATACOMPVTT_REG                               (0x000009A8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA2CH0_CR_WLCTRL_REG                                         (0x000009AC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA2CH0_CR_LPMODECTL_REG                                      (0x000009B0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA2CH0_CR_DDRCRDATACOMP0_REG                                 (0x000009B4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA2CH0_CR_DDRCRDATACOMP1_REG                                 (0x000009B8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA2CH0_CR_VCCDLLCOMPDATA_REG                                 (0x000009BC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA2CH0_CR_DCSOFFSET_REG                                      (0x000009C0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA2CH0_CR_DFXCCMON_REG                                       (0x000009C4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA2CH0_CR_AFEMISCCTRL0_REG                                   (0x000009C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA2CH0_CR_VIEWCTL_REG                                        (0x000009CC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA2CH0_CR_DCCCTL11_REG                                       (0x000009D0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA2CH0_CR_DCCCTL13_REG                                       (0x000009D4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA2CH0_CR_PMFSM_REG                                          (0x000009D8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA2CH0_CR_PMFSM1_REG                                         (0x000009DC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA2CH0_CR_CLKALIGNSTATUS_REG                                 (0x000009E0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA2CH0_CR_DATATRAINFEEDBACK_REG                              (0x000009E4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA2CH0_CR_DDRCRMARGINMODECONTROL0_REG                        (0x000009E8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA2CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x000009EC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA2CH0_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x000009F0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA2CH0_CR_DCCCTL14_REG                                       (0x000009F4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA2CH1_CR_DDRDATADQRANK0LANE0_REG                            (0x00000A00)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK0LANE1_REG                            (0x00000A04)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK0LANE2_REG                            (0x00000A08)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK0LANE3_REG                            (0x00000A0C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK0LANE4_REG                            (0x00000A10)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK0LANE5_REG                            (0x00000A14)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK0LANE6_REG                            (0x00000A18)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK0LANE7_REG                            (0x00000A1C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK1LANE0_REG                            (0x00000A20)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK1LANE1_REG                            (0x00000A24)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK1LANE2_REG                            (0x00000A28)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK1LANE3_REG                            (0x00000A2C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK1LANE4_REG                            (0x00000A30)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK1LANE5_REG                            (0x00000A34)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK1LANE6_REG                            (0x00000A38)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK1LANE7_REG                            (0x00000A3C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK2LANE0_REG                            (0x00000A40)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK2LANE1_REG                            (0x00000A44)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK2LANE2_REG                            (0x00000A48)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK2LANE3_REG                            (0x00000A4C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK2LANE4_REG                            (0x00000A50)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK2LANE5_REG                            (0x00000A54)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK2LANE6_REG                            (0x00000A58)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK2LANE7_REG                            (0x00000A5C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK3LANE0_REG                            (0x00000A60)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK3LANE1_REG                            (0x00000A64)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK3LANE2_REG                            (0x00000A68)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK3LANE3_REG                            (0x00000A6C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK3LANE4_REG                            (0x00000A70)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK3LANE5_REG                            (0x00000A74)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK3LANE6_REG                            (0x00000A78)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRDATADQRANK3LANE7_REG                            (0x00000A7C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA2CH1_CR_DDRCRDATACONTROL0_REG                              (0x00000A80)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA2CH1_CR_DDRCRDATACONTROL1_REG                              (0x00000A84)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA2CH1_CR_DDRCRDATACONTROL2_REG                              (0x00000A88)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA2CH1_CR_DDRCRDATACONTROL3_REG                              (0x00000A8C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA2CH1_CR_AFEMISC_REG                                        (0x00000A90)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA2CH1_CR_SRZCTL_REG                                         (0x00000A94)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA2CH1_CR_DQRXCTL0_REG                                       (0x00000A98)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA2CH1_CR_DQRXCTL1_REG                                       (0x00000A9C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA2CH1_CR_DQSTXRXCTL_REG                                     (0x00000AA0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA2CH1_CR_DQSTXRXCTL0_REG                                    (0x00000AA4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA2CH1_CR_SDLCTL1_REG                                        (0x00000AA8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA2CH1_CR_SDLCTL0_REG                                        (0x00000AAC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA2CH1_CR_MDLLCTL0_REG                                       (0x00000AB0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA2CH1_CR_MDLLCTL1_REG                                       (0x00000AB4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA2CH1_CR_MDLLCTL2_REG                                       (0x00000AB8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA2CH1_CR_MDLLCTL3_REG                                       (0x00000ABC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA2CH1_CR_MDLLCTL4_REG                                       (0x00000AC0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA2CH1_CR_MDLLCTL5_REG                                       (0x00000AC4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA2CH1_CR_DIGMISCCTRL_REG                                    (0x00000AC4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA2CH1_CR_AFEMISCCTRL2_REG                                   (0x00000AC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA2CH1_CR_TXPBDOFFSET0_REG                                   (0x00000ACC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA2CH1_CR_TXPBDOFFSET1_REG                                   (0x00000AD0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA2CH1_CR_COMPCTL0_REG                                       (0x00000AD4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA2CH1_CR_COMPCTL1_REG                                       (0x00000AD8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA2CH1_CR_COMPCTL2_REG                                       (0x00000ADC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA2CH1_CR_DCCCTL5_REG                                        (0x00000AE0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA2CH1_CR_DCCCTL6_REG                                        (0x00000AE4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA2CH1_CR_DCCCTL7_REG                                        (0x00000AE8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA2CH1_CR_DCCCTL8_REG                                        (0x00000AEC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA2CH1_CR_DCCCTL9_REG                                        (0x00000AF0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA2CH1_CR_RXCONTROL0RANK0_REG                                (0x00000AF4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA2CH1_CR_RXCONTROL0RANK1_REG                                (0x00000AF8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA2CH1_CR_RXCONTROL0RANK2_REG                                (0x00000AFC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA2CH1_CR_RXCONTROL0RANK3_REG                                (0x00000B00)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA2CH1_CR_TXCONTROL0_PH90_REG                                (0x00000B04)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA2CH1_CR_TXCONTROL0RANK0_REG                                (0x00000B08)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA2CH1_CR_TXCONTROL0RANK1_REG                                (0x00000B0C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA2CH1_CR_TXCONTROL0RANK2_REG                                (0x00000B10)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA2CH1_CR_TXCONTROL0RANK3_REG                                (0x00000B14)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA2CH1_CR_DDRDATADQSRANKX_REG                                (0x00000B18)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA2CH1_CR_DDRDATADQSPH90RANKX_REG                            (0x00000B1C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA2CH1_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00000B20)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA2CH1_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00000B24)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA2CH1_CR_DDRCRCMDBUSTRAIN_REG                               (0x00000B28)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA2CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x00000B2C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA2CH1_CR_DDRCRWRRETRAINRANK3_REG                            (0x00000B30)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA2CH1_CR_DDRCRWRRETRAINRANK2_REG                            (0x00000B34)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA2CH1_CR_DDRCRWRRETRAINRANK1_REG                            (0x00000B38)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA2CH1_CR_DDRCRWRRETRAINRANK0_REG                            (0x00000B3C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA2CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00000B40)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA2CH1_CR_TXCONTROL1_PH90_REG                                (0x00000B44)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA2CH1_CR_AFEMISCCTRL1_REG                                   (0x00000B48)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA2CH1_CR_DQXRXAMPCTL_REG                                    (0x00000B4C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA2CH1_CR_DQ0RXAMPCTL_REG                                    (0x00000B50)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA2CH1_CR_DQ1RXAMPCTL_REG                                    (0x00000B54)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA2CH1_CR_DQ2RXAMPCTL_REG                                    (0x00000B58)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA2CH1_CR_DQ3RXAMPCTL_REG                                    (0x00000B5C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA2CH1_CR_DQ4RXAMPCTL_REG                                    (0x00000B60)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA2CH1_CR_DQ5RXAMPCTL_REG                                    (0x00000B64)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA2CH1_CR_DQ6RXAMPCTL_REG                                    (0x00000B68)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA2CH1_CR_DQ7RXAMPCTL_REG                                    (0x00000B6C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA2CH1_CR_DQRXAMPCTL0_REG                                    (0x00000B70)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA2CH1_CR_DQRXAMPCTL1_REG                                    (0x00000B74)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA2CH1_CR_DQRXAMPCTL2_REG                                    (0x00000B78)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA2CH1_CR_DQTXCTL0_REG                                       (0x00000B7C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA2CH1_CR_DCCCTL0_REG                                        (0x00000B80)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA2CH1_CR_DCCCTL1_REG                                        (0x00000B84)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA2CH1_CR_DCCCTL2_REG                                        (0x00000B88)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA2CH1_CR_DCCCTL3_REG                                        (0x00000B8C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA2CH1_CR_DCCCTL10_REG                                       (0x00000B90)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA2CH1_CR_DCCCTL12_REG                                       (0x00000B94)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA2CH1_CR_CLKALIGNCTL0_REG                                   (0x00000B98)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA2CH1_CR_CLKALIGNCTL1_REG                                   (0x00000B9C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA2CH1_CR_CLKALIGNCTL2_REG                                   (0x00000BA0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA2CH1_CR_DCCCTL4_REG                                        (0x00000BA4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA2CH1_CR_DDRCRDATACOMPVTT_REG                               (0x00000BA8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA2CH1_CR_WLCTRL_REG                                         (0x00000BAC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA2CH1_CR_LPMODECTL_REG                                      (0x00000BB0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA2CH1_CR_DDRCRDATACOMP0_REG                                 (0x00000BB4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA2CH1_CR_DDRCRDATACOMP1_REG                                 (0x00000BB8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA2CH1_CR_VCCDLLCOMPDATA_REG                                 (0x00000BBC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA2CH1_CR_DCSOFFSET_REG                                      (0x00000BC0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA2CH1_CR_DFXCCMON_REG                                       (0x00000BC4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA2CH1_CR_AFEMISCCTRL0_REG                                   (0x00000BC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA2CH1_CR_VIEWCTL_REG                                        (0x00000BCC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA2CH1_CR_DCCCTL11_REG                                       (0x00000BD0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA2CH1_CR_DCCCTL13_REG                                       (0x00000BD4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA2CH1_CR_PMFSM_REG                                          (0x00000BD8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA2CH1_CR_PMFSM1_REG                                         (0x00000BDC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA2CH1_CR_CLKALIGNSTATUS_REG                                 (0x00000BE0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA2CH1_CR_DATATRAINFEEDBACK_REG                              (0x00000BE4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA2CH1_CR_DDRCRMARGINMODECONTROL0_REG                        (0x00000BE8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA2CH1_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x00000BEC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA2CH1_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x00000BF0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA2CH1_CR_DCCCTL14_REG                                       (0x00000BF4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA3CH0_CR_DDRDATADQRANK0LANE0_REG                            (0x00000C00)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK0LANE1_REG                            (0x00000C04)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK0LANE2_REG                            (0x00000C08)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK0LANE3_REG                            (0x00000C0C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK0LANE4_REG                            (0x00000C10)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK0LANE5_REG                            (0x00000C14)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK0LANE6_REG                            (0x00000C18)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK0LANE7_REG                            (0x00000C1C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK1LANE0_REG                            (0x00000C20)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK1LANE1_REG                            (0x00000C24)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK1LANE2_REG                            (0x00000C28)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK1LANE3_REG                            (0x00000C2C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK1LANE4_REG                            (0x00000C30)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK1LANE5_REG                            (0x00000C34)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK1LANE6_REG                            (0x00000C38)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK1LANE7_REG                            (0x00000C3C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK2LANE0_REG                            (0x00000C40)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK2LANE1_REG                            (0x00000C44)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK2LANE2_REG                            (0x00000C48)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK2LANE3_REG                            (0x00000C4C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK2LANE4_REG                            (0x00000C50)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK2LANE5_REG                            (0x00000C54)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK2LANE6_REG                            (0x00000C58)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK2LANE7_REG                            (0x00000C5C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK3LANE0_REG                            (0x00000C60)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK3LANE1_REG                            (0x00000C64)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK3LANE2_REG                            (0x00000C68)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK3LANE3_REG                            (0x00000C6C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK3LANE4_REG                            (0x00000C70)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK3LANE5_REG                            (0x00000C74)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK3LANE6_REG                            (0x00000C78)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRDATADQRANK3LANE7_REG                            (0x00000C7C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH0_CR_DDRCRDATACONTROL0_REG                              (0x00000C80)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA3CH0_CR_DDRCRDATACONTROL1_REG                              (0x00000C84)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA3CH0_CR_DDRCRDATACONTROL2_REG                              (0x00000C88)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA3CH0_CR_DDRCRDATACONTROL3_REG                              (0x00000C8C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA3CH0_CR_AFEMISC_REG                                        (0x00000C90)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA3CH0_CR_SRZCTL_REG                                         (0x00000C94)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA3CH0_CR_DQRXCTL0_REG                                       (0x00000C98)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA3CH0_CR_DQRXCTL1_REG                                       (0x00000C9C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA3CH0_CR_DQSTXRXCTL_REG                                     (0x00000CA0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA3CH0_CR_DQSTXRXCTL0_REG                                    (0x00000CA4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA3CH0_CR_SDLCTL1_REG                                        (0x00000CA8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA3CH0_CR_SDLCTL0_REG                                        (0x00000CAC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA3CH0_CR_MDLLCTL0_REG                                       (0x00000CB0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA3CH0_CR_MDLLCTL1_REG                                       (0x00000CB4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA3CH0_CR_MDLLCTL2_REG                                       (0x00000CB8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA3CH0_CR_MDLLCTL3_REG                                       (0x00000CBC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA3CH0_CR_MDLLCTL4_REG                                       (0x00000CC0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA3CH0_CR_MDLLCTL5_REG                                       (0x00000CC4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA3CH0_CR_DIGMISCCTRL_REG                                    (0x00000CC4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA3CH0_CR_AFEMISCCTRL2_REG                                   (0x00000CC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA3CH0_CR_TXPBDOFFSET0_REG                                   (0x00000CCC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA3CH0_CR_TXPBDOFFSET1_REG                                   (0x00000CD0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA3CH0_CR_COMPCTL0_REG                                       (0x00000CD4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA3CH0_CR_COMPCTL1_REG                                       (0x00000CD8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA3CH0_CR_COMPCTL2_REG                                       (0x00000CDC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA3CH0_CR_DCCCTL5_REG                                        (0x00000CE0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA3CH0_CR_DCCCTL6_REG                                        (0x00000CE4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA3CH0_CR_DCCCTL7_REG                                        (0x00000CE8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA3CH0_CR_DCCCTL8_REG                                        (0x00000CEC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA3CH0_CR_DCCCTL9_REG                                        (0x00000CF0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA3CH0_CR_RXCONTROL0RANK0_REG                                (0x00000CF4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA3CH0_CR_RXCONTROL0RANK1_REG                                (0x00000CF8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA3CH0_CR_RXCONTROL0RANK2_REG                                (0x00000CFC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA3CH0_CR_RXCONTROL0RANK3_REG                                (0x00000D00)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA3CH0_CR_TXCONTROL0_PH90_REG                                (0x00000D04)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA3CH0_CR_TXCONTROL0RANK0_REG                                (0x00000D08)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA3CH0_CR_TXCONTROL0RANK1_REG                                (0x00000D0C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA3CH0_CR_TXCONTROL0RANK2_REG                                (0x00000D10)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA3CH0_CR_TXCONTROL0RANK3_REG                                (0x00000D14)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA3CH0_CR_DDRDATADQSRANKX_REG                                (0x00000D18)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA3CH0_CR_DDRDATADQSPH90RANKX_REG                            (0x00000D1C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA3CH0_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00000D20)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA3CH0_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00000D24)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA3CH0_CR_DDRCRCMDBUSTRAIN_REG                               (0x00000D28)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA3CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x00000D2C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA3CH0_CR_DDRCRWRRETRAINRANK3_REG                            (0x00000D30)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA3CH0_CR_DDRCRWRRETRAINRANK2_REG                            (0x00000D34)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA3CH0_CR_DDRCRWRRETRAINRANK1_REG                            (0x00000D38)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA3CH0_CR_DDRCRWRRETRAINRANK0_REG                            (0x00000D3C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA3CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00000D40)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA3CH0_CR_TXCONTROL1_PH90_REG                                (0x00000D44)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA3CH0_CR_AFEMISCCTRL1_REG                                   (0x00000D48)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA3CH0_CR_DQXRXAMPCTL_REG                                    (0x00000D4C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA3CH0_CR_DQ0RXAMPCTL_REG                                    (0x00000D50)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA3CH0_CR_DQ1RXAMPCTL_REG                                    (0x00000D54)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA3CH0_CR_DQ2RXAMPCTL_REG                                    (0x00000D58)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA3CH0_CR_DQ3RXAMPCTL_REG                                    (0x00000D5C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA3CH0_CR_DQ4RXAMPCTL_REG                                    (0x00000D60)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA3CH0_CR_DQ5RXAMPCTL_REG                                    (0x00000D64)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA3CH0_CR_DQ6RXAMPCTL_REG                                    (0x00000D68)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA3CH0_CR_DQ7RXAMPCTL_REG                                    (0x00000D6C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA3CH0_CR_DQRXAMPCTL0_REG                                    (0x00000D70)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA3CH0_CR_DQRXAMPCTL1_REG                                    (0x00000D74)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA3CH0_CR_DQRXAMPCTL2_REG                                    (0x00000D78)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA3CH0_CR_DQTXCTL0_REG                                       (0x00000D7C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA3CH0_CR_DCCCTL0_REG                                        (0x00000D80)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA3CH0_CR_DCCCTL1_REG                                        (0x00000D84)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA3CH0_CR_DCCCTL2_REG                                        (0x00000D88)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA3CH0_CR_DCCCTL3_REG                                        (0x00000D8C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA3CH0_CR_DCCCTL10_REG                                       (0x00000D90)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA3CH0_CR_DCCCTL12_REG                                       (0x00000D94)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA3CH0_CR_CLKALIGNCTL0_REG                                   (0x00000D98)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA3CH0_CR_CLKALIGNCTL1_REG                                   (0x00000D9C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA3CH0_CR_CLKALIGNCTL2_REG                                   (0x00000DA0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA3CH0_CR_DCCCTL4_REG                                        (0x00000DA4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA3CH0_CR_DDRCRDATACOMPVTT_REG                               (0x00000DA8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA3CH0_CR_WLCTRL_REG                                         (0x00000DAC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA3CH0_CR_LPMODECTL_REG                                      (0x00000DB0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA3CH0_CR_DDRCRDATACOMP0_REG                                 (0x00000DB4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA3CH0_CR_DDRCRDATACOMP1_REG                                 (0x00000DB8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA3CH0_CR_VCCDLLCOMPDATA_REG                                 (0x00000DBC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA3CH0_CR_DCSOFFSET_REG                                      (0x00000DC0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA3CH0_CR_DFXCCMON_REG                                       (0x00000DC4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA3CH0_CR_AFEMISCCTRL0_REG                                   (0x00000DC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA3CH0_CR_VIEWCTL_REG                                        (0x00000DCC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA3CH0_CR_DCCCTL11_REG                                       (0x00000DD0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA3CH0_CR_DCCCTL13_REG                                       (0x00000DD4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA3CH0_CR_PMFSM_REG                                          (0x00000DD8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA3CH0_CR_PMFSM1_REG                                         (0x00000DDC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA3CH0_CR_CLKALIGNSTATUS_REG                                 (0x00000DE0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA3CH0_CR_DATATRAINFEEDBACK_REG                              (0x00000DE4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA3CH0_CR_DDRCRMARGINMODECONTROL0_REG                        (0x00000DE8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA3CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x00000DEC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA3CH0_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x00000DF0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA3CH0_CR_DCCCTL14_REG                                       (0x00000DF4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA3CH1_CR_DDRDATADQRANK0LANE0_REG                            (0x00000E00)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK0LANE1_REG                            (0x00000E04)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK0LANE2_REG                            (0x00000E08)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK0LANE3_REG                            (0x00000E0C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK0LANE4_REG                            (0x00000E10)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK0LANE5_REG                            (0x00000E14)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK0LANE6_REG                            (0x00000E18)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK0LANE7_REG                            (0x00000E1C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK1LANE0_REG                            (0x00000E20)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK1LANE1_REG                            (0x00000E24)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK1LANE2_REG                            (0x00000E28)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK1LANE3_REG                            (0x00000E2C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK1LANE4_REG                            (0x00000E30)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK1LANE5_REG                            (0x00000E34)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK1LANE6_REG                            (0x00000E38)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK1LANE7_REG                            (0x00000E3C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK2LANE0_REG                            (0x00000E40)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK2LANE1_REG                            (0x00000E44)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK2LANE2_REG                            (0x00000E48)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK2LANE3_REG                            (0x00000E4C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK2LANE4_REG                            (0x00000E50)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK2LANE5_REG                            (0x00000E54)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK2LANE6_REG                            (0x00000E58)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK2LANE7_REG                            (0x00000E5C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK3LANE0_REG                            (0x00000E60)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK3LANE1_REG                            (0x00000E64)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK3LANE2_REG                            (0x00000E68)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK3LANE3_REG                            (0x00000E6C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK3LANE4_REG                            (0x00000E70)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK3LANE5_REG                            (0x00000E74)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK3LANE6_REG                            (0x00000E78)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRDATADQRANK3LANE7_REG                            (0x00000E7C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA3CH1_CR_DDRCRDATACONTROL0_REG                              (0x00000E80)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA3CH1_CR_DDRCRDATACONTROL1_REG                              (0x00000E84)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA3CH1_CR_DDRCRDATACONTROL2_REG                              (0x00000E88)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA3CH1_CR_DDRCRDATACONTROL3_REG                              (0x00000E8C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA3CH1_CR_AFEMISC_REG                                        (0x00000E90)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA3CH1_CR_SRZCTL_REG                                         (0x00000E94)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA3CH1_CR_DQRXCTL0_REG                                       (0x00000E98)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA3CH1_CR_DQRXCTL1_REG                                       (0x00000E9C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA3CH1_CR_DQSTXRXCTL_REG                                     (0x00000EA0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA3CH1_CR_DQSTXRXCTL0_REG                                    (0x00000EA4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA3CH1_CR_SDLCTL1_REG                                        (0x00000EA8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA3CH1_CR_SDLCTL0_REG                                        (0x00000EAC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA3CH1_CR_MDLLCTL0_REG                                       (0x00000EB0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA3CH1_CR_MDLLCTL1_REG                                       (0x00000EB4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA3CH1_CR_MDLLCTL2_REG                                       (0x00000EB8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA3CH1_CR_MDLLCTL3_REG                                       (0x00000EBC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA3CH1_CR_MDLLCTL4_REG                                       (0x00000EC0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA3CH1_CR_MDLLCTL5_REG                                       (0x00000EC4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA3CH1_CR_DIGMISCCTRL_REG                                    (0x00000EC4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA3CH1_CR_AFEMISCCTRL2_REG                                   (0x00000EC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA3CH1_CR_TXPBDOFFSET0_REG                                   (0x00000ECC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA3CH1_CR_TXPBDOFFSET1_REG                                   (0x00000ED0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA3CH1_CR_COMPCTL0_REG                                       (0x00000ED4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA3CH1_CR_COMPCTL1_REG                                       (0x00000ED8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA3CH1_CR_COMPCTL2_REG                                       (0x00000EDC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA3CH1_CR_DCCCTL5_REG                                        (0x00000EE0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA3CH1_CR_DCCCTL6_REG                                        (0x00000EE4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA3CH1_CR_DCCCTL7_REG                                        (0x00000EE8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA3CH1_CR_DCCCTL8_REG                                        (0x00000EEC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA3CH1_CR_DCCCTL9_REG                                        (0x00000EF0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA3CH1_CR_RXCONTROL0RANK0_REG                                (0x00000EF4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA3CH1_CR_RXCONTROL0RANK1_REG                                (0x00000EF8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA3CH1_CR_RXCONTROL0RANK2_REG                                (0x00000EFC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA3CH1_CR_RXCONTROL0RANK3_REG                                (0x00000F00)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA3CH1_CR_TXCONTROL0_PH90_REG                                (0x00000F04)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA3CH1_CR_TXCONTROL0RANK0_REG                                (0x00000F08)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA3CH1_CR_TXCONTROL0RANK1_REG                                (0x00000F0C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA3CH1_CR_TXCONTROL0RANK2_REG                                (0x00000F10)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA3CH1_CR_TXCONTROL0RANK3_REG                                (0x00000F14)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA3CH1_CR_DDRDATADQSRANKX_REG                                (0x00000F18)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA3CH1_CR_DDRDATADQSPH90RANKX_REG                            (0x00000F1C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA3CH1_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00000F20)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA3CH1_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00000F24)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA3CH1_CR_DDRCRCMDBUSTRAIN_REG                               (0x00000F28)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA3CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x00000F2C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA3CH1_CR_DDRCRWRRETRAINRANK3_REG                            (0x00000F30)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA3CH1_CR_DDRCRWRRETRAINRANK2_REG                            (0x00000F34)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA3CH1_CR_DDRCRWRRETRAINRANK1_REG                            (0x00000F38)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA3CH1_CR_DDRCRWRRETRAINRANK0_REG                            (0x00000F3C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA3CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00000F40)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA3CH1_CR_TXCONTROL1_PH90_REG                                (0x00000F44)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA3CH1_CR_AFEMISCCTRL1_REG                                   (0x00000F48)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA3CH1_CR_DQXRXAMPCTL_REG                                    (0x00000F4C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA3CH1_CR_DQ0RXAMPCTL_REG                                    (0x00000F50)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA3CH1_CR_DQ1RXAMPCTL_REG                                    (0x00000F54)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA3CH1_CR_DQ2RXAMPCTL_REG                                    (0x00000F58)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA3CH1_CR_DQ3RXAMPCTL_REG                                    (0x00000F5C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA3CH1_CR_DQ4RXAMPCTL_REG                                    (0x00000F60)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA3CH1_CR_DQ5RXAMPCTL_REG                                    (0x00000F64)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA3CH1_CR_DQ6RXAMPCTL_REG                                    (0x00000F68)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA3CH1_CR_DQ7RXAMPCTL_REG                                    (0x00000F6C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA3CH1_CR_DQRXAMPCTL0_REG                                    (0x00000F70)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA3CH1_CR_DQRXAMPCTL1_REG                                    (0x00000F74)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA3CH1_CR_DQRXAMPCTL2_REG                                    (0x00000F78)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA3CH1_CR_DQTXCTL0_REG                                       (0x00000F7C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA3CH1_CR_DCCCTL0_REG                                        (0x00000F80)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA3CH1_CR_DCCCTL1_REG                                        (0x00000F84)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA3CH1_CR_DCCCTL2_REG                                        (0x00000F88)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA3CH1_CR_DCCCTL3_REG                                        (0x00000F8C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA3CH1_CR_DCCCTL10_REG                                       (0x00000F90)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA3CH1_CR_DCCCTL12_REG                                       (0x00000F94)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA3CH1_CR_CLKALIGNCTL0_REG                                   (0x00000F98)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA3CH1_CR_CLKALIGNCTL1_REG                                   (0x00000F9C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA3CH1_CR_CLKALIGNCTL2_REG                                   (0x00000FA0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA3CH1_CR_DCCCTL4_REG                                        (0x00000FA4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA3CH1_CR_DDRCRDATACOMPVTT_REG                               (0x00000FA8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA3CH1_CR_WLCTRL_REG                                         (0x00000FAC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA3CH1_CR_LPMODECTL_REG                                      (0x00000FB0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA3CH1_CR_DDRCRDATACOMP0_REG                                 (0x00000FB4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA3CH1_CR_DDRCRDATACOMP1_REG                                 (0x00000FB8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA3CH1_CR_VCCDLLCOMPDATA_REG                                 (0x00000FBC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA3CH1_CR_DCSOFFSET_REG                                      (0x00000FC0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA3CH1_CR_DFXCCMON_REG                                       (0x00000FC4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA3CH1_CR_AFEMISCCTRL0_REG                                   (0x00000FC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA3CH1_CR_VIEWCTL_REG                                        (0x00000FCC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA3CH1_CR_DCCCTL11_REG                                       (0x00000FD0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA3CH1_CR_DCCCTL13_REG                                       (0x00000FD4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA3CH1_CR_PMFSM_REG                                          (0x00000FD8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA3CH1_CR_PMFSM1_REG                                         (0x00000FDC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA3CH1_CR_CLKALIGNSTATUS_REG                                 (0x00000FE0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA3CH1_CR_DATATRAINFEEDBACK_REG                              (0x00000FE4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA3CH1_CR_DDRCRMARGINMODECONTROL0_REG                        (0x00000FE8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA3CH1_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x00000FEC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA3CH1_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x00000FF0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA3CH1_CR_DCCCTL14_REG                                       (0x00000FF4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG
#pragma pack(pop)
#endif
