/** @file
  The functions in this file implement the memory controller registers that
  are not training specific. After these functions are executed, the
  memory controller will be ready to execute the timing training sequences.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
//
// Include files
//
#include "MrcMcConfiguration.h"
#include "MrcTimingConfiguration.h"
#include "MrcMaintenance.h"
#include "MrcDdrIoOffsets.h"
#include "MrcMemoryApi.h"
#include "MrcLpddr4.h"
#include "MrcDdr4.h"
#include "MrcCommon.h"
#include "Cpgc20.h"
#include "MrcCpgcOffsets.h"
#include "MrcCpgcApi.h"
#include "MrcDdrIoApi.h"
#include "MrcChipApi.h"

#define AV_PHY_INIT 1

///
/// Structs and Types
///

/// CCC Enable Mappings.
/// The numbers after the Ccc union define which CCC instance this mapping should be applied.
typedef union {
  union { // DDR4
    union { // UY
      struct {
        UINT16      : 5; ///< Bits 0:4
        UINT16 Clk0 : 2; ///< Bits 5:6 Clock is a 2 bit enable for P & N
        UINT16      : 9; ///< Bits 7:15
      } Ccc04;
      struct {
        UINT16      : 1;  ///< Bit 0
        UINT16 Odt1 : 1;  ///< Bit 1
        UINT16 Cs1  : 1;  ///< Bit 2
        UINT16 Cs0  : 1;  ///< Bit 3
        UINT16 Odt0 : 1;  ///< Bit 4
        UINT16      : 11; ///< Bits 5:15
      } Ccc15;
      struct {
        UINT16      : 10; ///< Bits 0:9
        UINT16 Cke1 : 1;  ///< Bit  10
        UINT16      : 1;  ///< Bit  11
        UINT16 Cke0 : 1;  ///< Bit  12
        UINT16      : 3;  ///< Bits 13:15
      } Ccc26;
      struct {
        UINT16      : 5; ///< Bits 0:4
        UINT16 Clk1 : 2; ///< Bits 5:6 Clock is a 2 bit enable for P & N
        UINT16      : 9; ///< Bits 7:15
      } Ccc37;
    } UY;
    union { // HS
      struct {
        UINT16 Cke0 : 1; ///< Bit  0
        UINT16      : 1; ///< Bit  1
        UINT16 Cke2 : 1; ///< Bit  2
        UINT16      : 1; ///< Bit  3
        UINT16 Cke3 : 1; ///< Bit  4
        UINT16      : 4; ///< Bits 5:8
        UINT16 Cke1 : 1; ///< Bit  9
        UINT16      : 6; ///< Bits 10:15
      } Ccc02;
      struct {
        UINT16         : 5; ///< Bits 0:4
        UINT16 Clk0or2 : 2; ///< Bits 5:6
        UINT16 Clk1or3 : 2; ///< Bits 7:8
        UINT16         : 7; ///< Bits 9:15
      } Ccc1357;
      struct {
        UINT16      : 1; ///< Bit  0
        UINT16 Odt0 : 1; ///< Bit  1
        UINT16 Cs0  : 1; ///< Bit  2
        UINT16 Odt2 : 1; ///< Bit  3
        UINT16 Cs2  : 1; ///< Bit  4
        UINT16      : 6; ///< Bits 5:10
        UINT16 Odt1 : 1; ///< Bit  11
        UINT16 Cs1  : 1; ///< Bit  12
        UINT16 Odt3 : 1; ///< Bit  13
        UINT16 Cs3  : 1; ///< Bit  14
        UINT16      : 2; ///< Bits 15:16
      } Ccc46;
    } HS;
  } Ddr4;
  union {
    struct {
      UINT16 Cs0    : 1; ///< Bit  0
      UINT16        : 1; ///< Bit  1
      UINT16 Cs1    : 1; ///< Bit  2
      UINT16        : 2; ///< Bits 3:4
      UINT16 Clk3   : 2; ///< Bits 5:6
      UINT16 Clk2   : 2; ///< Bits 7:8
      UINT16 Cs3    : 1; ///< Bit  9
      UINT16        : 6; ///< Bits 10:15
    } Ccc02;
    struct {
      UINT16        : 5; ///< Bits 0:4
      UINT16 Clk0   : 2; ///< Bits 5:6
      UINT16 Clk1   : 2; ///< Bits 7:8
      UINT16        : 2; ///< Bits 9:10
      UINT16 Cs2    : 1; ///< Bit  11
      UINT16        : 3; ///< Bits 12:15
    } Ccc13;
    struct {
      UINT16        : 5; ///< Bits 0:4
      UINT16 Clk3   : 2; ///< Bits 5:6
      UINT16 Clk2   : 2; ///< Bits 7:8
      UINT16        : 2; ///< Bits 9:10
      UINT16 Cs1    : 1; ///< Bit  11
      UINT16        : 2; ///< Bits 12:13
      UINT16 Cs0    : 1; ///< Bit  14
      UINT16        : 1; ///< Bit  15
    } Ccc46;
    struct {
      UINT16        : 5; ///< Bits 0:4
      UINT16 Clk1   : 2; ///< Bits 5:6
      UINT16 Clk0   : 2; ///< Bits 7:8
      UINT16 Cs3    : 1; ///< Bit  9
      UINT16        : 1; ///< Bit  10
      UINT16 Cs2    : 1; ///< Bit  11
      UINT16        : 4; ///< Bits 12:15
    } Ccc57;
  } Ddr5;
  union {
    struct {
      UINT16      : 2; ///< Bits  0:1
      UINT16 Cs1  : 1; ///< Bit   2
      UINT16      : 1; ///< Bit   3
      UINT16 Cs0  : 1; ///< Bit   4
      UINT16 Clk  : 2; ///< Bits  5:6 Clock is a 2 bit enable for P & N
      UINT16 Cke  : 2; ///< Bits  7:8
      UINT16      : 7; ///< Bits  9:15
    } Ccc023467;
    struct {
      UINT16 Cs1  : 1; ///< Bit   0
      UINT16      : 3; ///< Bits  0:3
      UINT16 Cs0  : 1; ///< Bit   4
      UINT16 Clk  : 2; ///< Bits  5:6 Clock is a 2 bit enable for P & N
      UINT16 Cke  : 2; ///< Bits  7:8
      UINT16      : 7; ///< Bits  9:15
    } Ccc15;
  } Lp4;
  union {
    struct {
      UINT16      : 2; ///< Bits  0:1
      UINT16 Cs   : 2; ///< Bits  2:3
      UINT16      : 1; ///< Bit   4
      UINT16 Clk  : 2; ///< Bits  5:6 Clock is a 2 bit enable for P & N
      UINT16 Wck  : 2; ///< Bits  7:8 Clock is a 2 bit enable for P & N
      UINT16      : 7; ///< Bits  9:15
    } Ccc023467;
    struct {
      UINT16 Cs0  : 1; ///< Bits  0
      UINT16      : 2; ///< Bits  1:2
      UINT16 Cs1  : 1; ///< Bit   3
      UINT16      : 1; ///< Bit   4
      UINT16 Clk  : 2; ///< Bits  5:6 Clock is a 2 bit enable for P & N
      UINT16 Wck  : 2; ///< Bits  7:8 Clock is a 2 bit enable for P & N
      UINT16      : 7; ///< Bits  9:15
    } Ccc15;
  } Lp5A;
  union {
    struct {
      UINT16      : 5; ///< Bit   0:4
      UINT16 Clk  : 2; ///< Bits  5:6 Clock is a 2 bit enable for P & N
      UINT16 Wck  : 2; ///< Bits  7:8 Clock is a 2 bit enable for P & N
      UINT16      : 1; ///< Bits  9
      UINT16 Cs   : 2; ///< Bits  10:11
      UINT16      : 4; ///< Bits  12:15
    } Ccc0145;
    struct {
      UINT16      : 5; ///< Bit   0:4
      UINT16 Clk  : 2; ///< Bits  5:6 Clock is a 2 bit enable for P & N
      UINT16 Wck  : 2; ///< Bits  7:8 Clock is a 2 bit enable for P & N
      UINT16 Cs0  : 1; ///< Bits  9
      UINT16      : 1; ///< Bits  10
      UINT16 Cs1  : 1; ///< Bits  11
      UINT16      : 4; ///< Bits  12:15
    } Ccc2367;
  } Lp5D;
  union {
    struct {
      UINT16      : 5; ///< Bits 0:4
      UINT16 Clk  : 2; ///< Bits 5:6 Clock is a 2 bit enable for P & N
      UINT16 Cke  : 2; ///< Bits 7:8
      UINT16 Cs3  : 1; ///< Bit  9
      UINT16 Cs2  : 1; ///< Bit  10
      UINT16 Cs1  : 1; ///< Bit  11
      UINT16 Cs0  : 1; ///< Bit  12
      UINT16      : 3; ///< Bits 13:15
    } Ccc0246;
    struct {
      UINT16 Cs3  : 1; ///< Bit  0
      UINT16      : 4; ///< Bit  1:4
      UINT16 Clk  : 2; ///< Bits 5:6 Clock is a 2 bit enable for P & N
      UINT16 Cke  : 2; ///< Bits 7:8
      UINT16 Cs1  : 1; ///< Bit  9
      UINT16 Cs0  : 1; ///< Bit  10
      UINT16      : 1; ///< Bit  11
      UINT16 Cs2  : 1; ///< Bit  12
      UINT16      : 3; ///< Bits 13:15
    } Ccc13;
    struct {
      UINT16 Cs1  : 1; ///< Bit  0
      UINT16      : 4; ///< Bit  1:4
      UINT16 Clk  : 2; ///< Bits 5:6 Clock is a 2 bit enable for P & N
      UINT16 Cke  : 2; ///< Bits 7:8
      UINT16      : 1; ///< Bit  9
      UINT16 Cs2  : 1; ///< Bit  10
      UINT16 Cs0  : 1; ///< Bit  11
      UINT16      : 1; ///< Bit  12
      UINT16 Cs3  : 1; ///< Bit  13
      UINT16      : 2; ///< Bit  14:15
    } Ccc5;
    struct {
      UINT16      : 5; ///< Bits 0:4
      UINT16 Clk  : 2; ///< Bits 5:6 Clock is a 2 bit enable for P & N
      UINT16 Cke  : 2; ///< Bits 7:8
      UINT16      : 1; ///< Bit  9
      UINT16 Cs2  : 1; ///< Bit  10
      UINT16 Cs0  : 1; ///< Bit  11
      UINT16 Cs1  : 1; ///< Bit  12
      UINT16      : 1; ///< Bit  13
      UINT16 Cs3  : 1; ///< Bit  14
      UINT16      : 1; ///< Bit  15
    } Ccc7;
  } LpHS;
  struct {
    UINT16        : 5; ///< Bit   0:4
    UINT16 Clk    : 2; ///< Bits  5:6 Clock is a 2 bit enable for P & N
    UINT16 CkeWck : 2; ///< Bits  7:8 Clock is a 2 bit enable for P & N
    UINT16        : 7; ///< Bits  9:15
  } CccCommonLp; ///< Clk is common for all technologies.  Cke/Wck are the same across all fubs for Lp4/Lp5 on UY & HS.
  UINT16  Data16;
} CCC_TX_EN_TYPE;

/// Defines
#define MRC_NUM_BYTE_GROUPS    (8)

/// CCC Command TxEn Bits
#define MRC_ALL_CCC_BUF_MSK          (0x7FFF)
#define MRC_UY_LP4_CCC023467_CA_MSK  (0x1E03)
#define MRC_UY_LP4_CCC15_CA_MSK      (0x1E06)
#define MRC_UY_LP5A_CCC023467_CA_MSK (0x1E13)
#define MRC_UY_LP5A_CCC15_CA_MSK     (0x1E16)
#define MRC_UY_LP5D_CCC0145_CA_MSK   (0x121F)
#define MRC_UY_LP5D_CCC2367_CA_MSK   (0x141F)
#define MRC_UY_DDR4_CCC04_CA_MSK     (0x1E1C)
#define MRC_UY_DDR4_CCC15_CA_MSK     (0x1E01)
#define MRC_UY_DDR4_CCC26_CA_MSK     (0xA0F)
#define MRC_UY_DDR4_CCC37_CA_MSK     (0x1F)
#define MRC_HS_DDR4_CCC02_CA_MSK     (0x7C0A)
#define MRC_HS_DDR4_CCC13_CA_MSK     (0x601F)
#define MRC_HS_DDR4_CCC46_CA_MSK     (0x601)
#define MRC_HS_DDR4_CCC57_CA_MSK     (0x7418)
#define MRC_HS_DDR5_CCC02_CA_MSK     (0x7C0A)
#define MRC_HS_DDR5_CCC13_CA_MSK     (0x201F)
#define MRC_HS_DDR5_CCC46_CA_MSK     (0x161F)
#define MRC_HS_DDR5_CCC57_CA_MSK     (0x7410)
#define MRC_HS_LP4_CCC012346_CA_MSK  (0x601E)
#define MRC_HS_LP4_CCC5_CA_MSK       (0x501E)
#define MRC_HS_LP4_CCC7_CA_MSK       (0x221E)
#define MRC_HS_LP5_CCC0246_CA_MSK    (0x601F)
#define MRC_HS_LP5_CCC13_CA_MSK      (0x681E)
#define MRC_HS_LP5_CCC5_CA_MSK       (0x521E)
#define MRC_HS_LP5_CCC7_CA_MSK       (0x221F)

#define PANICV0                      (25)   // mV
#define PANICV1                      (40)   // mV
#define VCCANA_EH                    (1800) // mV
#define MRC_VCCDLL_TARGET            (850)  // mV
#define MRC_VCCBG                    (1000) // mV
#define CA_VOLT_SEL                  (0)
#define CLK_VOLT_SEL                 (0)

///
/// Global Constants
///
const UINT8 CompParamList[] = { RdOdt, WrDS, WrDSCmd, WrDSCtl, WrDSClk };
const UINT8 ByteStagger[] = {1, 4, 1, 5, 2, 6, 3, 7, 8};  // Increased Byte Stagger for byte0
const UINT8 MinCycleStageDelay[] = {46, 70, 70, 46};      // Avoid corner case

/// 8 banks case (x8 or x16):
/// Logical Bank:         0  1  2  3  4  5  6  7
///                       ----------------------
/// Physical Bank:        0  0  1  1  2  2  3  3
/// Physical Bank Group:  0  1  0  1  0  1  0  1
static MRC_BG_BANK_PAIR Ddr4BankMap[MAX_DDR4_x16_BANKS] = {
  {0,0}, {1,0}, {0,1}, {1,1}, {0,2}, {1,2}, {0,3}, {1,3}
};

/**
  This function configures the BCLK RFI frequency for each SAGV point.

  @param[in, out] MrcData - MRC global data.

  @retval VOID
**/
VOID
MrcBclkRfiConfiguration (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MrcDebug          *Debug;
  UINT32              BclkRfiFreqInput;
  UINT32              MailboxCommand;
  UINT32              MailboxData;
  UINT32              MailboxStatus;
  UINT32              MinBclkRfiFreq;
  MrcIntOutput        *MrcIntData;
  const MRC_FUNCTION  *MrcCall;


  MrcIntData        = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));
  Inputs            = &MrcData->Inputs;
  Outputs           = &MrcData->Outputs;
  Debug             = &Outputs->Debug;
  MrcCall           = Inputs->Call.Func;
  BclkRfiFreqInput  = Inputs->BclkRfiFreq[MrcIntData->SaGvPoint];

  // Configure BCLK RFI frequency if needed for this SAGV point
  if (BclkRfiFreqInput != 0) {
    // Get Min BCLK RFI range
    MailboxCommand = CPU_MAILBOX_BCLK_CONFIG_CMD |
                    (CPU_MAILBOX_BCLK_CONFIG_READ_BCLK_RFI_RANGE_SUBCOMMAND << CPU_MAILBOX_CMD_PARAM_1_OFFSET);
    MrcCall->MrcCpuMailboxRead (MAILBOX_TYPE_PCODE, MailboxCommand, &MailboxData, &MailboxStatus);
    if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
      MinBclkRfiFreq = BCLK_DEFAULT;
    } else {
      // Mailbox format is in 7.3 MHz, need to convert to Hz.
      // 1,000,000 / (2^3) = 125,000
      MinBclkRfiFreq = (MailboxData & CPU_MAILBOX_BCLK_CONFIG_READ_BCLK_RFI_RANGE_MIN_MASK) * BCLK_RFI_FREQ_CONVERSION;
    }
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "CPU_MAILBOX_BCLK_CONFIG_CMD %s. MailboxStatus = %Xh\n",
      (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed",
      MailboxStatus
      );

    // Verify BCLK RFI input range
    BclkRfiFreqInput = RANGE(BclkRfiFreqInput, MinBclkRfiFreq, BCLK_DEFAULT);

    // Update BCLK RFI frequency
    MailboxCommand = CPU_MAILBOX_BCLK_CONFIG_CMD |
                    (CPU_MAILBOX_BCLK_CONFIG_SET_BCLK_RFI_FREQ_SUBCOMMAND << CPU_MAILBOX_CMD_PARAM_1_OFFSET) |
                    (MrcIntData->SaGvPoint << CPU_MAILBOX_CMD_PARAM_2_OFFSET);
    // Mailbox format is in 7.3 Mhz, BclkRfiFreq is in Hz.
    // Mailbox format = BclkRfiFreq * (2^3) / 1,000,000.
    // (2^3)/1,000,000 = 1/125,000
    MailboxData = BclkRfiFreqInput / BCLK_RFI_FREQ_CONVERSION;
    MrcCall->MrcCpuMailboxWrite (MAILBOX_TYPE_PCODE, MailboxCommand, MailboxData, &MailboxStatus);
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "CPU_MAILBOX_BCLK_CONFIG_SET_BCLK_RFI_FREQ_SUBCOMMAND %s. MailboxStatus = %Xh\n",
      (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed",
      MailboxStatus
      );
  }
}

/**
  This function locks the DDR frequency requested from SPD or User.
  It will update the frequency related members in the output structure.

  @param[in, out] MrcData - MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcFrequencyLock (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MrcDebug          *Debug;
  MrcChannelOut     *ChannelOut;
  MrcStatus         Status;
  MrcProfile        Profile;
  UINT32            Channel;
  UINT32            Controller;
  UINT32            Offset;
  INT64             GetSetVal;

  Inputs            = &MrcData->Inputs;
  Outputs           = &MrcData->Outputs;
  Debug             = &Outputs->Debug;
  Profile           = Inputs->MemoryProfile;
  // Make sure tCL-tCWL <= 4
  // This is needed to support ODT properly for 2DPC case
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (MrcChannelExist (MrcData, Controller, (UINT8) Channel)) {
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        if (ChannelOut->DimmCount == 2) {
          if ((ChannelOut->Timing[Profile].tCL - ChannelOut->Timing[Profile].tCWL) > 4) {
            ChannelOut->Timing[Profile].tCWL = ChannelOut->Timing[Profile].tCL - 4;
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              "(tCL-tCWL) > 4, Mc%u.Ch%u - tCWL has been updated to %u\n",
              Controller,
              Channel,
              ChannelOut->Timing[Profile].tCWL
              );
          }
        }
      }
    }
  }

  if (MrcReadCR (MrcData, MC_BIOS_DATA_PCU_REG) != 0) {  // Check if PLL is locked
    Offset = (Inputs->A0) ? DDRSCRAM_CR_DDRLASTCR_A0_REG : DDRSCRAM_CR_DDRLASTCR_REG;
    MrcWriteCR (MrcData, Offset, 0);  // Set EnInitComplete to zero in case we repeat phy init in SAGV flow
    MrcWriteCrMulticast (MrcData, DATA_CR_SDLCTL0_REG, 0); // Set rxsdl_cmn_sdlen to 0
  }

  // Configure BCLK RFI frequency
  MrcBclkRfiConfiguration(MrcData);

  Status = McFrequencySet (MrcData, FALSE, MRC_PRINTS_ON);

  // Select the interleaving mode of DQ/DQS pins
  // This must be the first DDR IO register to be programmed
  GetSetVal = (Inputs->DqPinsInterleaved) ? 0 : 1;
  MrcGetSetNoScope (MrcData, GsmIocNoDqInterleave, WriteNoCache | PrintValue, &GetSetVal);

  // Gear configuration originally was inside of McFrequencySet, but we need to modify the flow such that MrcSetGear() is called before
  // McFrequencySet when doing LPDDR frequency switching flow (MrcFrequencySwitch).  This covers the entire subsystem, so we will currently leave
  // this here.
  MrcSetGear (MrcData);

  MrcInternalCheckPoint (MrcData, OemFrequencySetDone, NULL);

  // Save MRC Version into CR
  MrcSetMrcVersion (MrcData);

  return Status;
}

/**
  This function initializes the Memory Controller: Scheduler, Timings, Address Decode,
  Odt Control, and refresh settings.

  @param[in, out] MrcData - MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status.
**/
MrcStatus
MrcMcConfiguration (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcOutput *Outputs;
  MrcDebug  *Debug;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Timing Config\n");
  MrcTimingConfiguration (MrcData);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Refresh Config\n");
  MrcRefreshConfiguration (MrcData);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Scheduler parameters\n");
  MrcSchedulerParametersConfig (MrcData);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Address Decoding Config\n");
  MrcAdConfiguration (MrcData);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Maintenance Configuration\n");
  MrcMaintenanceConfig (MrcData);

  return mrcSuccess;
}

/**
  This function initializes memory subsystem registers that are not specific to MC or DDRIO.

  @param[in, out] MrcData - Pointer to the global MRC data.

  @retval MrcStatus - mrcSuccess if successful or an error status.
**/
MrcStatus
MrcMemorySsInit (
  IN OUT  MrcParameters *const MrcData
  )
{
  ControllerZoneConfiguration (MrcData);

  return mrcSuccess;
}

/**
  PHY init - get the DLL BwSel value per speed.

  @param[in, out] MrcData - Include all MRC global data.

  @retval BwSel value
**/
UINT32
MrcGetDllBwSel (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcOutput     *Outputs;
  MrcFrequency  DdrFrequency;
  BOOLEAN       Gear4;
  UINT32        BwSel;

  Outputs       = &MrcData->Outputs;
  Gear4         = Outputs->Gear4;
  DdrFrequency  = Outputs->Frequency;
  BwSel         = 0;

  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      if (DdrFrequency <= f1067) {
        BwSel = 4;
      } else if (DdrFrequency <= f1600) {
        BwSel = 7;
      } else if (DdrFrequency <= f2400) {
        BwSel = 0xC;
      } else if (DdrFrequency <= f3200) {
        BwSel = 0xF;
      } else if (DdrFrequency <= f4267) {
        BwSel = 0x16;
      }
      break;

    case MRC_DDR_TYPE_DDR4:
      if (DdrFrequency <= f1333) {
        BwSel = 4;
      } else if (DdrFrequency <= f1600) {
        BwSel = 7;
      } else if (DdrFrequency <= f2133) {
        BwSel = 0xA;
      } else if (DdrFrequency <= f2400) {
        BwSel = 0xC;
      } else if (DdrFrequency <= f3200) {
        BwSel = 0xF;
      } else { // @todo_adl Need values for OC cases
        BwSel = 0x16;
      }
      break;

    case MRC_DDR_TYPE_DDR5:
      if (DdrFrequency <= f1100) {
        BwSel = 4;
      } else if (DdrFrequency <= f2133) {
        BwSel = 0xA;
      } else if (DdrFrequency <= f4000) {
        BwSel = 0xF;
      } else if (DdrFrequency <= f6400) {
        BwSel = 0x16;
      } else { // @todo_adl Need values for OC cases
        BwSel = 0x20;
      }
      break;

    case MRC_DDR_TYPE_LPDDR5:
    default:
      if (DdrFrequency <= f1100) {
        BwSel = 4;
      } else if (DdrFrequency <= f1600) {
        BwSel = 7;
      } else if (DdrFrequency <= f2133) {
        BwSel = 0xA;
      } else if (DdrFrequency <= f2400) {
        BwSel =  Gear4 ? 0x4 : 0xC;
      } else if (DdrFrequency <= f3200) {
        BwSel = 0xF;
      } else if (DdrFrequency <= f4000) {
        BwSel = 0xF;
      } else if (DdrFrequency <= f4533) {
        BwSel = 0x16;
      } else if (DdrFrequency <= f4800 && !Gear4) {
        BwSel = 0x1D;
      } else if (DdrFrequency >= f4800 && Gear4) {
        BwSel = 0x1F;
      } else {
        BwSel = 0x16;  // @todo_adl wrong value ?
      }
      break;
  }
  return BwSel;
}

/**
  PHY init - sequence static_DQEARLY in DQ FUB.
  Programs the following registers:
   - DataControl0/1/2/3
   - DataDqRankXLaneY
   - TxPbdOffset0/1

  @param[in, out] MrcData    - Include all MRC global data.
  @param[in]      LockUiOnly - If TRUE, program only registers that depend on LockUI value

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitStaticDqEarly (
  IN OUT MrcParameters *const MrcData,
  IN     BOOLEAN              LockUiOnly
  )
{
  MrcStatus         Status;
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MrcSaveData       *SaveOutputs;
  MrcDebug          *Debug;
  MrcDdrType        DdrType;
  INT64             SenseAmpDurationMax;
  INT64             DqsOdtDurationMax;
  INT64             RxClkStgMax;
  UINT32            i;
  UINT32            Gear1;
  UINT32            ResetNumPre;
  UINT32            NumToggles;
  UINT32            RcvEnWaveShape;
  UINT32            RxTogglePreamble;
  UINT32            UiLock;
  UINT32            RxVrefVal;
  UINT32            ReadDqsOffset;
  BOOLEAN           Ddr5;
  BOOLEAN           Lpddr;
  BOOLEAN           Lpddr4;
  BOOLEAN           Lpddr5;
  BOOLEAN           Gear4;
  BOOLEAN           MatchedRx;
  BOOLEAN           A0;
  BOOLEAN           UlxUlt;
  DATA0CH0_CR_DDRCRDATACONTROL0_STRUCT  DataControl0;
  DATA0CH0_CR_DDRCRDATACONTROL1_STRUCT  DataControl1;
  DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT  DataControl2;
  DATA0CH0_CR_DDRCRDATACONTROL3_STRUCT  DataControl3;

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  SaveOutputs = &MrcData->Save.Data;
  Status      = mrcSuccess;
  DdrType     = Outputs->DdrType;
  Ddr5        = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr4      = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Lpddr       = Outputs->Lpddr;
  Gear4       = Outputs->Gear4;
  Gear1       = !(Outputs->Gear2) && !Gear4;
  UiLock      = SaveOutputs->UiLock;
  ReadDqsOffset = SaveOutputs->ReadDqsOffsetDdr5;
  A0          = Inputs->A0;
  UlxUlt      = Inputs->UlxUlt;

  // @todo_adl  Need SAFE values for DataControl0..3

  if (!LockUiOnly) {
    DataControl0.Data = 0;
    DataControl0.Bits.gear1                 = Gear1;
    DataControl0.Bits.rxrankmuxdelay_2ndstg = 3;
    DataControl0.Bits.vrefpmctrl            = 1;
    DataControl0.Bits.biaspmctrl            = 2;
    DataControl0.Bits.endqsodtparkmode      = 1; // @todo_adl should be 0 for DDR4/5 ?
    DataControl0.Bits.endqodtparkmode       = 0; // @todo_adl should be 1 for Lpddr ?
    DataControl0.Bits.internalclockson      = (A0 || UlxUlt) ? !Lpddr : 0;
    DataControl0.Bits.Gear4                 = Gear4;
    DataControl0.Bits.local_gate_d0tx       = 0;                      // @todo_adl check with DE
    DataControl0.Bits.forcerxon             = (Ddr5 && SaveOutputs->GlobalVsshiBypass) ? 1 : 0;
    MrcWriteCrMulticast (MrcData, DATA_CR_DDRCRDATACONTROL0_REG, DataControl0.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DataControl0: 0x%08X\n", DataControl0.Data);

    MrcGetSetLimits (MrcData, SenseAmpDuration, NULL, &SenseAmpDurationMax, NULL); // SenseAmpDuration and McOdtDuration are the same.
    MrcGetSetLimits (MrcData, DqsOdtDuration,   NULL, &DqsOdtDurationMax,   NULL);

    DataControl1.Data = 0;
    DataControl1.Bits.safemodeenable    = 1;
    DataControl1.Bits.dqodtdelay        = 0;
    DataControl1.Bits.dqodtduration     = (UINT32) SenseAmpDurationMax;
    DataControl1.Bits.dqsodtdelay       = 0;
    DataControl1.Bits.dqsodtduration    = (UINT32) DqsOdtDurationMax;
    DataControl1.Bits.senseampdelay     = (Lpddr5 && Gear4 && (Outputs->Frequency >= f4400)) ? 2 : 0;  // @todo_adl confirm LP5 Gear4 value
    DataControl1.Bits.senseampduration  = (UINT32) SenseAmpDurationMax;
    MrcWriteCrMulticast (MrcData, DATA_CR_DDRCRDATACONTROL1_REG, DataControl1.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DataControl1: 0x%08X\n", DataControl1.Data);

    MrcGetSetLimits (MrcData, GsmIocRxClkStg, NULL, &RxClkStgMax, NULL);

    DataControl2.Data = 0;
    DataControl2.Bits.datainvertnibble      = 0;        // @todo_adl shall we use this feature ? - Mutex w\ Write0 feature
    if (A0) {
      DataControl2.BitsA0.gear1             = Gear1;
      DataControl2.BitsA0.wrpreamble        = MrcGetWpre (MrcData) - 1;
      DataControl2.BitsA0.dbimode           = 1;
      DataControl2.BitsA0.txdqsrankmuxdelay_2nd_stage_offset  = 2;
      DataControl2.BitsA0.txrankmuxdelay_2nd_stage_offset     = 2;
      DataControl2.BitsA0.disabletxdqs      = Lpddr5;
      DataControl2.BitsA0.rxburstlen        = Lpddr4 ? 3 : 1;       // GsmIocDataRxBurstLen
      DataControl2.BitsA0.rxclkstgnum       = (UINT32) RxClkStgMax; // GsmIocRxClkStg
    } else {
      DataControl2.Bits.wrpreamble          = MrcGetWpre (MrcData) - 1;
      DataControl2.Bits.dbimode             = 1;
      DataControl2.Bits.txdqsrankmuxdelay_2nd_stage_offset  = 2;
      DataControl2.Bits.txrankmuxdelay_2nd_stage_offset     = 2;
      DataControl2.Bits.disabletxdqs        = Lpddr5;
      DataControl2.Bits.rxburstlen          = Lpddr4 ? 3 : 1;       // GsmIocDataRxBurstLen
      DataControl2.Bits.rxclkstgnum         = (UINT32) RxClkStgMax; // GsmIocRxClkStg
    }
    MrcWriteCrMulticast (MrcData, DATA_CR_DDRCRDATACONTROL2_REG, DataControl2.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DataControl2: 0x%08X\n", DataControl2.Data);
  }

  MatchedRx = (Outputs->RxMode == MrcRxModeMatchedN) || (Outputs->RxMode == MrcRxModeMatchedP);

  ResetNumPre       = 0;
  RcvEnWaveShape    = 0;
  RxTogglePreamble  = 0;
  if (!MatchedRx) {
    // The number of preamble toggles
    // DDR5: '0010' = 1
    //       '00001010' = 2
    if (Ddr5) {
      if (MrcGetRpre (MrcData) > 3) {
        NumToggles = 2;
      } else {
        NumToggles = 1;
      }
    } else if (Lpddr5) {
      if (MrcGetRpre (MrcData) >= 3) {
        NumToggles = 4;
      } else {
        NumToggles = 2;
      }
    } else {
      NumToggles = (UiLock >= 4) ? 4 : 2;
    }

    ResetNumPre       = NumToggles + 2;
    RxTogglePreamble  = NumToggles - (((UiLock + 1 - ReadDqsOffset * 2) == 1) ? 0 : 1); // <-- depends on UiLock calibration results
    if (Lpddr5) {
      RcvEnWaveShape = Gear1 ? 3 : (Gear4 ? 1 : 2);
    } else { // DDR5
      if (Gear4) {
        RcvEnWaveShape = 1;
      } else {
        if (NumToggles >= 3) {
          RcvEnWaveShape  = Gear1 ? 4 : 3;
        } else {
          RcvEnWaveShape  = Gear1 ? 3 : 2;
        }
      }
    }
  }

  // 0.25 * Vddq for LP4/5  (Vss termination)
  // 0.75 * Vddq for DDR4/5 (Vddq termination)
  // Later in PHY init we will program more precise value according to DimmRon, number of DIMMs / ranks etc.
  RxVrefVal = Lpddr ? (512 / 4) : (512 * 3) / 4;

  if (!LockUiOnly) {
    DataControl3.Data = 0;
    DataControl3.Bits.usedefaultrdptrcalc       = 1;
    DataControl3.Bits.rxvref                    = RxVrefVal;
    DataControl3.Bits.rxreadpointer             = 0;
    DataControl3.Bits.wr1p5tckpostamble_toggle  = 0;
    DataControl3.Bits.wr1p5tckpostamble_enable  = 0;                        // @todo_adl Might need this for DDR5
    DataControl3.Bits.lpddr5                    = Lpddr5;
    DataControl3.Bits.lpddr                     = Lpddr;
    DataControl3.Bits.rxpion                    = 0;
  } else {
    DataControl3.Data = MrcReadCR(MrcData, DATA0CH0_CR_DDRCRDATACONTROL3_REG);
  }
  DataControl3.Bits.rstnumpre                 = ResetNumPre;
  DataControl3.Bits.rxtogglepreamble          = RxTogglePreamble;
  DataControl3.Bits.rcvenwaveshape            = RcvEnWaveShape;
  DataControl3.Bits.dram_rddqsoffset          = ReadDqsOffset;            // Should match DDR5 MR40[2:0], depends on lockui calibration result
  MrcWriteCrMulticast (MrcData, DATA_CR_DDRCRDATACONTROL3_REG, DataControl3.Data);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DataControl3: 0x%08X\n", DataControl3.Data);

  if (!LockUiOnly) {
    for (i = 0; i < MAX_RANK_IN_CHANNEL * MAX_BITS * 4; i = i + 4) {
      MrcWriteCrMulticast (MrcData, (DATA_CR_DDRDATADQRANK0LANE0_REG + i), 0);
    }
    MrcWriteCrMulticast (MrcData, DATA_CR_TXPBDOFFSET0_REG, 0);
    MrcWriteCrMulticast (MrcData, DATA_CR_TXPBDOFFSET1_REG, 0);
  }

  return Status;
}

/**
  PHY init - sequence static_DIG1 in VTT FUB.
  Programs the following registers:
   - DDRVTT_VttGenControl
   - DDRVTT_VttCompOffset
   - DDRVTT_VttCompOffset2
   - DDRVTT_DDREARLY_RSTRDONE

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitVttStaticDig1 (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcStatus         Status;
  MrcOutput         *Outputs;
  MrcInput          *Inputs;
  MrcDdrType        DdrType;
  BOOLEAN           Lpddr4;
  BOOLEAN           Lpddr5;
  BOOLEAN           Ddr4;
  BOOLEAN           A0;
  BOOLEAN           UlxUlt;
  DDRVTT0_CR_DDRCRVTTGENCONTROL_STRUCT  VttGenControl;
  DDRVTT0_CR_DDRCRVTTCOMPOFFSET_STRUCT  VttCompOffset;
  DDRVTT0_CR_DDRCRVTTCOMPOFFSET2_STRUCT VttCompOffset2;
  DDRVTT0_CR_DDREARLY_RSTRDONE_STRUCT   DdrEarlyRestoreDone;

  Status      = mrcSuccess;
  Outputs     = &MrcData->Outputs;
  Inputs      = &MrcData->Inputs;
  DdrType     = Outputs->DdrType;
  Lpddr4      = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4        = (DdrType == MRC_DDR_TYPE_DDR4);
  A0          = MrcData->Inputs.A0;
  UlxUlt      = Inputs->UlxUlt;

  VttGenControl.Data = 0;
  /* TGL code :
  TempVar1 = (382 * VttTargetV);
  TempVar2 = DIVIDEROUND (TempVar1, Vdd2Mv) - 32;
  // Assumes VttTargetV in LPDDR is 150mV based on the system studies.
  VttGenControl.Bits.Target = TempVar2;

  TempVar1 = (382 * PANICV0);
  TempVar2 = DIVIDEROUND (TempVar1, Vdd2Mv);
  VttGenControl.Bits.Panic0 = TempVar2;

  TempVar1 = (382 * PANICV1);
  TempVar2 = DIVIDEROUND (TempVar1, Vdd2Mv);
  VttGenControl.Bits.Panic1 = TempVar2;

  VttGenControl.Bits.Spare1 = SAFE ? 1 : 0;
  // VttGenControl.Bits.DisSensorPwrDn = SAFE ? 1 : 0; // Field no longer exists
  VttGenControl.Bits.EnDacPM = (A0 || SAFE) ? 0 : 2;
// @todo_adl  VttGenControl.Bits.DdrCRForceODTOn = (DataControl0.Bits.OdtForceQDrvEn || DataControl0.Bits.DdrCRForceODTOn) && VttGenControl.Bits.EnVttOdt;
  //This field is programmed in SetVtt function
 // VttGenControl.Bits.EnVttOdt = (DataControl0.OdtMode == 2 * Vtt);   // (DDRControl5.OdtMode == 2 (VTT))
  TempVar1 = (Ddr4 || Ddr5) ? 10 : 16; // MaxMin( RndUp( (DDR4or5?10nS:16nS)/tQCLK/8 )-1, 0, 7)
  TempVar2 = DIVIDEROUND (TempVar1, QclkPs);
  TempVar3 = DIVIDEROUND (TempVar2, 8) - 1;
  VttGenControl.Bits.WakeUpDelay = TempVar3;

  TempVar1 = (60 / RefClkPs) - 1;
  TempVar2 = MrcLog2(TempVar1);
  VttGenControl.Bits.LockTimer = TempVar2;

  VttGenControl.Bits.AckOffset = 1;
  VttGenControl.Bits.EnVttOdt = 1; */

  VttGenControl.Bits.Target = Lpddr4 ? 0x26 : (Lpddr5 ? 0x29 : 0xA0);     // @todo_adl Use TGL formula ?
  VttGenControl.Bits.Panic0 = Ddr4 ? 8 : 9;                               // @todo_adl Use TGL formula ?
  VttGenControl.Bits.Panic1 = Ddr4 ? 13 : 14;                             // @todo_adl Use TGL formula ?
  if (A0) {
    VttGenControl.BitsA0.WakeUpDelay  = 4;                                // @todo_adl Use TGL formula ?
    VttGenControl.BitsA0.LockTimer    = 0;                                // @todo_adl Use TGL formula ?
    VttGenControl.BitsA0.AckOffset    = 0;                                // @todo_adl TGL sets this to 1
    VttGenControl.BitsA0.EnVttOdt     = 1;                                // @todo_adl Only if we use Vtt termination ?
  } else {
    VttGenControl.Bits.WakeUpDelay  = 4;                                  // @todo_adl Use TGL formula ?
    VttGenControl.Bits.LockTimer    = 0;                                  // @todo_adl Use TGL formula ?
    VttGenControl.Bits.AckOffset    = 0;                                  // @todo_adl TGL sets this to 1
    VttGenControl.Bits.EnVttOdt     = UlxUlt ? 0 : 1;
    VttGenControl.Bits.UseFLLDuringDVFS = 1;
  }

  /* TGL code:
  VttCompOffset.Data = MrcReadCR (MrcData, DDRVTT0_CR_DDRCRVTTCOMPOFFSET_REG);
  VttCompOffset2.Data = 0;
  VttCompOffset2.Bits.PanicLo0UsePmos = (VsxHiTargetMv + (-PANICV0) + VttCompOffset.Bits.PanicLo0CompOfst) < 375;
  VttCompOffset2.Bits.PanicLo1UsePmos = (VsxHiTargetMv + (-PANICV1) + VttCompOffset.Bits.PanicLo1CompOfst) < 375;
  VttCompOffset2.Bits.PanicHi0UsePmos = (VsxHiTargetMv + (PANICV0) + VttCompOffset.Bits.PanicHi0CompOfst)  < 375;
  VttCompOffset2.Bits.PanicHi1UsePmos = (VsxHiTargetMv + (PANICV1) + VttCompOffset.Bits.PanicHi1CompOfst)  < 375;
  */

  VttCompOffset.Data = 0;
  VttCompOffset2.Data = 0;    // @todo_adl Use TGL formula ?

  DdrEarlyRestoreDone.Data = 0;
  DdrEarlyRestoreDone.Bits.early_restoredone = 1;

  if (A0) {
    MrcWriteCrMulticast (MrcData, DDRVTT_CR_DDRCRVTTGENCONTROL_A0_REG,  VttGenControl.Data);
    MrcWriteCrMulticast (MrcData, DDRVTT_CR_DDRCRVTTCOMPOFFSET_A0_REG,  VttCompOffset.Data);
    MrcWriteCrMulticast (MrcData, DDRVTT_CR_DDRCRVTTCOMPOFFSET2_A0_REG, VttCompOffset2.Data);
    MrcWriteCrMulticast (MrcData, DDRVTT_CR_DDREARLY_RSTRDONE_A0_REG,   DdrEarlyRestoreDone.Data);
  } else {
    MrcWriteCrMulticast (MrcData, DDRVTT_CR_DDRCRVTTGENCONTROL_REG,   VttGenControl.Data);
    MrcWriteCrMulticast (MrcData, DDRVTT_CR_DDRCRVTTCOMPOFFSET_REG,   VttCompOffset.Data);
    MrcWriteCrMulticast (MrcData, DDRVTT_CR_DDRCRVTTCOMPOFFSET2_REG,  VttCompOffset2.Data);
    MrcWriteCrMulticast (MrcData, DDRVTT_CR_DDREARLY_RSTRDONE_REG,    DdrEarlyRestoreDone.Data);
  }
  return Status;
}

/**
  PHY init - sequence static_compinit in COMP FUB.
  Programs 46 registers in the COMP Fub.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitCompStaticCompInit (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcStatus         Status;
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MrcSaveData       *SaveOutputs;
  MrcDdrType        DdrType;
  BOOLEAN           Lpddr;
  BOOLEAN           Lpddr5;
  BOOLEAN           Ddr5;
  BOOLEAN           Gear2;
  BOOLEAN           Gear4;
  BOOLEAN           EnNmosPup;
  BOOLEAN           LocalVsshiBypass;
  BOOLEAN           GlobalVsshiBypass;
  BOOLEAN           TxPdnUseVcciog;
  BOOLEAN           Matched;
  UINT32            CompVref;
  UINT32            QclkFrequency;
  UINT32            DataRate;
  INT32             SData32;
  UINT32            Data32;
  UINT32            TurboCapTrim;
  UINT32            Vdd2Mv;
  UINT32            ScompDelayTarget;
  UINT32            NumCells;
  UINT32            StageDelay;
  BOOLEAN           PhaseLock;
  BOOLEAN           A0;
  BOOLEAN           Q0Regs;
  DDRPHY_COMP_CR_ALERTRX_STRUCT                   AlertRx;
  DDRPHY_COMP_CR_ALERTTX_STRUCT                   AlertTx;
  DDRPHY_COMP_CR_RCOMPCTRL1_STRUCT                RcompCtrl1;
  DDRPHY_COMP_CR_RCOMPCTRL_STRUCT                 RcompCtrl;
  DDRPHY_COMP_CR_RCOMP_EXTCOMP_STRUCT             RcompExtComp;
  DDRPHY_COMP_CR_RCOMP_RELCOMP_STRUCT             RcompRelComp;
  DDRPHY_COMP_CR_PNCCOMP_CTRL0_STRUCT             PanicCompCtrl0;
  DDRPHY_COMP_CR_PNCCOMP_CTRL1_STRUCT             PanicCompCtrl1;
  DDRPHY_COMP_CR_PNCCOMP_STRUCT                   PanicComp;
  DDRPHY_COMP_CR_SCOMP_STRUCT                     ScompReg;
  DDRPHY_COMP_CR_RXDQSCOMP_RXCODE_STRUCT          RxDqsCompRxCode;
  DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL0_STRUCT       RxDqsCompCmnCtrl0;
  DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL1_STRUCT       RxDqsCompCmnCtrl1;
  DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL1_NEW_STRUCT   RxDqsCompCmnCtrl1New;
  DDRPHY_COMP_CR_RXDQSCOMP_ENABLE_STRUCT          RxDqsCompEnable;
  DDRPHY_COMP_CR_DLLCOMP_LDOCTRL1_STRUCT          DllCompLdoCtrl1;
  DDRPHY_COMP_CR_DLLCOMP_LDOCTRL1_NEW_STRUCT      DllCompLdoCtrl1New;
  DDRPHY_COMP_CR_DLLCOMP_PIDELAY_STRUCT           DllCompPiDelay;
  DDRPHY_COMP_CR_DLLCOMP_PICTRL_STRUCT            DllCompPiCtrl;
  DDRPHY_COMP_CR_DLLCOMP_DLLCTRL_STRUCT           DllCompDllCtrl;
  DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_STRUCT          DllCompVcdlCtrl;
  DDRPHY_COMP_CR_DLLCOMP_VDLLCTRL_STRUCT          DllCompVdllCtrl;
  DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_STRUCT         DllCompSwCapCtrl;
  DDRPHY_COMP_CR_DDREARLYCR_STRUCT                DdrEarlyCr;
  DDRPHY_COMP_CR_VSSHICOMP_CTRL0_STRUCT           VsshiCompCtrl0;
  DDRPHY_COMP_CR_VSSHICOMP_CTRL1_STRUCT           VsshiCompCtrl1;
  DDRPHY_COMP_CR_VSSHICOMP_FFCOMP_STRUCT          VsshiCompFfComp;
  DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_STRUCT        VsxhiOpAmpCh0Ctrl;
  DDRPHY_COMP_CR_VSXHIOPAMPCH1_CTRL_STRUCT        VsxhiOpAmpCh1Ctrl;
  DDRPHY_COMP_CR_COMPDLLWL_STRUCT                 CompDllWl;
  DDRPHY_COMP_CR_TXCONTROL_STRUCT                 TxControl;
  DDRPHY_COMP_CR_VIEWDIGTX_CTRL_STRUCT            ViewDigTxCtrl;
  DDRPHY_COMP_CR_VIEWDIGTX_SEL_STRUCT             ViewDigTxSel;
  DDRPHY_COMP_CR_VIEWANA_CTRL_STRUCT              ViewAnaCtrl;
  DDRPHY_COMP_CR_VIEWANA_CBBSEL_STRUCT            ViewAnaCbbSel;
  DDRPHY_COMP_CR_COMP_IOLVRFFCTL_STRUCT           CompIoLvrFfCtl;
  DDRPHY_COMP_CR_MISCCOMPCODES_STRUCT             MiscCompCodes;
  DDRPHY_COMP_CR_COMPDLLSTRTUP_STRUCT             CompDllStartup;
  DDRPHY_COMP_CR_DDRCRDATACOMP0_STRUCT            DataComp0;
  DDRPHY_COMP_CR_DDRCRCMDCOMP_STRUCT              CmdComp;
  DDRPHY_COMP_CR_DDRCRCLKCOMP_STRUCT              ClkComp;
  DDRPHY_COMP_CR_DDRCRCTLCOMP_STRUCT              CtlComp;
  DDRPHY_COMP_CR_DDRCRCOMPCTL0_STRUCT             CompCtl0;
  DDRPHY_COMP_CR_DDRCRCOMPCTL1_STRUCT             CompCtl1;
  DDRPHY_COMP_CR_DDRCRCOMPCTL2_STRUCT             CompCtl2;
  DDRPHY_COMP_CR_DDRCRCOMPCTL3_STRUCT             CompCtl3;
  DDRPHY_COMP_CR_DDRCRCOMPCTL4_STRUCT             CompCtl4;
  DDRPHY_COMP_CR_SCOMPCTL_STRUCT                  ScompCtl;
  DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_STRUCT       VsshiCompCtrl2;
  DDRPHY_COMP_CR_VCCDDQLDO_DVFSCOMP_CTRL0_STRUCT  VccddqldoDvfscompCtrl0;
  BOOLEAN                                         DtHalo;
  BOOLEAN                                         UlxUlt;
  BOOLEAN                                         Gear1;
  BOOLEAN                                         Ddr4;

  Status      = mrcSuccess;
  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  SaveOutputs = &MrcData->Save.Data;
  DdrType     = Outputs->DdrType;
  Lpddr       = Outputs->Lpddr;
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr5        = (DdrType == MRC_DDR_TYPE_DDR5);
  Gear2       = Outputs->Gear2;
  Gear4       = Outputs->Gear4;
  Gear1       = !Gear2 && !Gear4;
  Vdd2Mv      = Outputs->Vdd2Mv;
  A0          = Inputs->A0;
  Q0Regs      = Inputs->Q0Regs;
  EnNmosPup         = SaveOutputs->EnNmosPup;
  LocalVsshiBypass  = SaveOutputs->LocalVsshiBypass;
  GlobalVsshiBypass = SaveOutputs->GlobalVsshiBypass;
  TxPdnUseVcciog    = SaveOutputs->TxPdnUseVcciog;
  Matched           = (Outputs->RxMode == MrcRxModeMatchedN) || (Outputs->RxMode == MrcRxModeMatchedP);
  DtHalo            = Inputs->DtHalo;
  UlxUlt            = Inputs->UlxUlt;
  Ddr4              = (DdrType == MRC_DDR_TYPE_DDR4);

  DataRate = Outputs->Frequency;
  if (Gear2) {
    QclkFrequency = DataRate / 2;
  } else if (Gear4) {
    QclkFrequency = DataRate / 4;
  } else { // Gear1
    QclkFrequency = DataRate;
  }

  SData32 = 359955 - 112 * QclkFrequency;
  SData32 = DIVIDEROUND (SData32, 100000);     // ROUND(QclkFrequency * (-0.00112) + 3.59955)
  TurboCapTrim = MAX (SData32, 0);             // Protect against negative values

  RcompCtrl1.Data = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_RCOMPCTRL1_REG, RcompCtrl1.Data);

  // DDR4 ALERT_N pin is not used, program values to avoid floating pin
  AlertRx.Data = 0;
  AlertRx.Bits.alertrx_ch0_vrefctl = 0x18;
  AlertRx.Bits.alertrx_ch1_vrefctl = 0x18;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_ALERTRX_REG, AlertRx.Data);

  AlertTx.Data = 0;
  AlertTx.Bits.alerttx_ch0_datain       = 1;
  AlertTx.Bits.alerttx_ch1_datain       = 1;
  AlertTx.Bits.alerttx_ch0_drven        = 1;
  AlertTx.Bits.alerttx_ch1_drven        = 1;
  AlertTx.Bits.alerttx_cmn_drvstatlegen = 1;
  AlertTx.Bits.alerttx_cmn_txrcompdrvup = 0x20;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_ALERTTX_REG, AlertTx.Data);

  RcompCtrl.Data = 0;
  RcompCtrl.Bits.rcomp_cmn_rcompshrtxpupusevcciog = EnNmosPup;
  RcompCtrl.Bits.rcomp_cmn_rcompshrtxpdnusevcciog = TxPdnUseVcciog;
  RcompCtrl.Bits.rcomp_cmn_rcompvddqvsshibyp      = LocalVsshiBypass;
  RcompCtrl.Bits.rcomp_cmn_rcomplocalvsshibyp     = LocalVsshiBypass;
  RcompCtrl.Bits.rcomp_cmn_rcompglobalvsshibyp    = GlobalVsshiBypass;
  RcompCtrl.Bits.rcompexttxdrvusevdd2             = Lpddr;
  RcompCtrl.Bits.swcaprxsampnum_cycles            = 1;
  RcompCtrl.Bits.swcaprxeval_cycles               = 1;
  RcompCtrl.Bits.swcaprxprech_cycles              = 3;
  RcompCtrl.Bits.swcaprxrst_cycles                = 3;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_RCOMPCTRL_REG, RcompCtrl.Data);

  RcompExtComp.Data = 0;
  RcompExtComp.Bits.rcompextvttodten            = 0;      // @todo_adl Only if we want to enable Vtt termination in ADL-P
  RcompExtComp.Bits.rcompextvrefen              = 1;
  RcompExtComp.Bits.rcompextvrefcode            = 0;      // [8 bit] @todo_adl Should be based on RCOMP target ?
  RcompExtComp.Bits.rcomp_cmn_rcompextstatlegen = 1;      // @todo_adl Should be based on RCOMP target ?
  RcompExtComp.Bits.rcompextrxrstb              = 0;
  RcompExtComp.Bits.rcomp_cmn_rcompextennmospup = EnNmosPup;
  RcompExtComp.Bits.rcompextdqpdnen             = 0;
  RcompExtComp.Bits.rcompextdqen                = 0;
  RcompExtComp.Bits.rcompextcodepuplive         = 0;      // [6 bit]
  RcompExtComp.Bits.rcompextcccpdnen            = 0;
  RcompExtComp.Bits.rcompextcccen               = 0;
  RcompExtComp.Bits.rcompextampen               = 0;
  RcompExtComp.Bits.extswcaprxcmpen             = 0;
  RcompExtComp.Bits.rcompextrxrstb              = 0;
  RcompExtComp.Bits.rcompextrxrstb              = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_RCOMP_EXTCOMP_REG, RcompExtComp.Data);

  RcompRelComp.Data = 0;
  RcompRelComp.Bits.rcomprelvrefen              = 1;
  RcompRelComp.Bits.rcomprelvrefcode            = 0;    // [8 bit]
  RcompRelComp.Bits.rcomprelrxrstb              = 0;
  RcompRelComp.Bits.rcomp_cmn_rcomprelennmospup = EnNmosPup;
  RcompRelComp.Bits.rcomprelcodepup             = 0;    // [6 bit]
  RcompRelComp.Bits.rcomprelcodepdnlive         = 0;
  RcompRelComp.Bits.rcomprelampen               = 0;
  RcompRelComp.Bits.rcomprelpupstatlegen        = 1;
  RcompRelComp.Bits.rcomprelpdnstatlegen        = 1;
  RcompRelComp.Bits.relswcaprxcmpen             = 0;

  // spare[0] : rcomp_odtup_extstatlegen    - config used to enable/disable static legs for ODT PUP COMP[br]
  // spare[1] : rcomp_odtdn_relpupstatlegen - config used to enable/disable static legs for ODT PDN COMP[br]
  // spare[2] : rcomp_odtdn_relpdnstatlegen - config used to enable/disable static legs for ODT PDN COMP[br]
  RcompRelComp.Bits.spare                       = 7;    // @todo_adl this is Rx static leg in COMP fub, set to Enabled to match DQ FUBs
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_RCOMP_RELCOMP_REG, RcompRelComp.Data);

  // Vref_pupcode = 192 * Rext / (Rext + Rtarg_pup * NumSeg); Rext= 100 Ohm,
  // 30 Ohm target for CmdVrefUp, this is used in rload calibration
  CompVref = 192 * Inputs->RcompResistor;
  CompVref = UDIVIDEROUND(192 * Inputs->RcompResistor, Inputs->RcompResistor + WRDS_CMD_RLOAD * 2);

  PanicCompCtrl0.Data = 0;
  PanicCompCtrl0.Bits.pnccomp_cmn_vrefvddqvsshibyp    = LocalVsshiBypass;
  PanicCompCtrl0.Bits.vrefusevcciog                   = 1;
  // RloadSegs = RloadTarget(192 - VrefCode) / (VrefCode * RcmdPup) ; RcmpPup = CmdDS * NumOfSeg
  if ((Lpddr5) && (Outputs->Frequency > f4800)) {
    PanicCompCtrl0.Bits.rloadnumsegs                    = UDIVIDEROUND (RLOAD_TARGET_LP5_HIGH_FREQ * (192 - CompVref), (CompVref * WRDS_CMD_RLOAD * 2)); // Cmd has 2 segments
    } else {
    PanicCompCtrl0.Bits.rloadnumsegs                    = UDIVIDEROUND (RLOAD_TARGET * (192 - CompVref), (CompVref * WRDS_CMD_RLOAD * 2)); // Cmd has 2 segments
  }
  PanicCompCtrl0.Bits.localvsshibyp                   = LocalVsshiBypass;
  PanicCompCtrl0.Bits.pnccomp_cmn_globalvsshibyp      = GlobalVsshiBypass;
  PanicCompCtrl0.Bits.swcaprxeval_cycles              = 1;
  PanicCompCtrl0.Bits.swcaprxprech_cycles             = 3;
  PanicCompCtrl0.Bits.swcaprxrst_cycles               = 3;
  PanicCompCtrl0.Bits.twovtcompen                     = !Lpddr;
  PanicCompCtrl0.Bits.ddrmode                         = !Lpddr;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_PNCCOMP_CTRL0_REG, PanicCompCtrl0.Data);

  PanicCompCtrl1.Data = 0;
  PanicCompCtrl1.Bits.pnccomp_cmn_txcccstatlegen      = 1;
  PanicCompCtrl1.Bits.pnccomp_cmn_txcccpupusevcciog   = EnNmosPup;
  PanicCompCtrl1.Bits.pnccomp_cmn_txcccpdnusevcciog   = TxPdnUseVcciog;
  PanicCompCtrl1.Bits.txcccennmospup                  = EnNmosPup;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_PNCCOMP_CTRL1_REG, PanicCompCtrl1.Data);

  PanicComp.Data = 0;
  PanicComp.Bits.pnccomp_code2vt_vrefcode = 0x40;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_PNCCOMP_REG, PanicComp.Data);

  ScompDelayTarget = 120;   // 120ps
  NumCells = UDIVIDEROUND (Outputs->Qclkps, ScompDelayTarget);
  if (NumCells > 15) {
    PhaseLock = TRUE;
    NumCells = UDIVIDEROUND (NumCells, 2);
  } else {
    PhaseLock = FALSE;
  }
  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "SCOMP: NumCells=%u, locked to %s\n", NumCells, PhaseLock ? "Phase" : "Cycle");

  ScompReg.Data = 0;
  ScompReg.Bits.scomp_cmn_slewstatlegen   = 1;
  ScompReg.Bits.scomp_cmn_refclkphasesel  = PhaseLock;    // 0 - cycle lock, 1 - phase lock. Trained in SCOMP init ?
  ScompReg.Bits.cmddlytapselstage         = NumCells;     // Number of delay line cells. Trained in SCOMP init ? dlytapselstage = scomp_lock_period / scomp_delay_cell_target
  ScompReg.Bits.scomp_cmn_refclk2cyclesel = 0;
  ScompReg.Bits.scomp_cmn_compdisfall     = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_SCOMP_REG, ScompReg.Data);

  RxDqsCompRxCode.Data = 0;
  RxDqsCompRxCode.Bits.dllcomp_cmn_picoderxdqsnref = 0; // @todo_adl  What is the formula ? This is set in MrcSetCompLockUI()
  if (DtHalo || (UlxUlt && Lpddr5)) {
    RxDqsCompRxCode.Bits.rxdqscomp_cmn_rxpbddlycoden = 16;
    RxDqsCompRxCode.Bits.rxdqscomp_cmn_rxpbddlycodep = 16;
  }
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_RXCODE_REG, RxDqsCompRxCode.Data);

  RxDqsCompCmnCtrl0.Data = 0;
  RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_dqspcomplockui = 0;    // @todo_adl  What is the formula ? This is set in MrcSetCompLockUI()
  RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_dqsncomplockui = 0;    // @todo_adl  What is the formula ? This is set in MrcSetCompLockUI()
  RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_accoupvrefsel  = 0x80;
  RxDqsCompCmnCtrl0.Bits.dllcomp_cmn_wlrdacholden     = 1;
  if (DtHalo) {
    RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_accoupfwdclkslope = 1;
    RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_accoupselswing = 3;
    RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_accoupvrefsel = 0x16;
  } else if (UlxUlt && Lpddr5) {
    RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_accoupfwdclkslope = 2;
    RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_accoupselswing = 3;
    RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_accoupvrefsel = 6;
  }
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL0_REG, RxDqsCompCmnCtrl0.Data);

  if (!Q0Regs) {
    RxDqsCompCmnCtrl1New.Data = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL1_NEW_REG, RxDqsCompCmnCtrl1New.Data);
  }

  RxDqsCompEnable.Data = 0;
  RxDqsCompEnable.Bits.rxdqscomp_cmn_rxbiasvrefsel  = 0x6;
  RxDqsCompEnable.Bits.rxdqscomp_cmn_rxbiasicomp    = 0x8;    // @todo_adl  What is the formula ?
  RxDqsCompEnable.Bits.rxdqscomp_cmn_rxrloadcomp    = 0xE;    // @todo_adl  What is the formula ?
  RxDqsCompEnable.Bits.rxdqscomp_cmn_rxampoffset    = 0x10;
  RxDqsCompEnable.Bits.rxdqscomp_cmn_rxlpddrmode    = !Lpddr;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_ENABLE_REG, RxDqsCompEnable.Data);

  DllCompLdoCtrl1New.Data = 0;
  DllCompLdoCtrl1New.Bits.dllcomp_cmn_rxdqmindlypat     = Matched ? 0xAA : 0;
  DllCompLdoCtrl1New.Bits.dllcomp_cmn_ldofbdivsel       = 1;
  DllCompLdoCtrl1New.Bits.dllcomp_cmn_vctlcompoffsetcal = 0x10;
  DllCompLdoCtrl1New.Bits.dllcomp_cmn_ldoffreadsegen    = 3;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_LDOCTRL1_NEW_REG, DllCompLdoCtrl1New.Data);

  DllCompPiDelay.Data = 0;
  DllCompPiDelay.Bits.dllcomp_cmn_picoderxdqspref = 0;  // @todo_adl Formula ? This is set in MrcSetCompLockUI()
  DllCompPiDelay.Bits.dllcomp_cmn_picoderxdqsd0   = 0;
  DllCompPiDelay.Bits.dllcomp_cmn_picodemisc      = 0;
  DllCompPiDelay.Bits.dllcomp_cmn_picodedcs       = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_PIDELAY_REG, DllCompPiDelay.Data);

  DllCompPiCtrl.Data = 0;
  DllCompPiCtrl.Bits.dllcomp_cmn_picoderxldoffph1 = 0x40;
  DllCompPiCtrl.Bits.dllcomp_cmn_gear1en          = !Gear2 && !Gear4;
  DllCompPiCtrl.Bits.dllcomp_cmn_gear2en          = Gear2;
  DllCompPiCtrl.Bits.dllcomp_cmn_gear4en          = Gear4;
  DllCompPiCtrl.Bits.dllcomp_cmn_picapsel         = 3;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_PICTRL_REG, DllCompPiCtrl.Data);

  DllCompDllCtrl.Data = 0;
  DllCompDllCtrl.Bits.dllcomp_cmn_turboonstartup  = 1;
  DllCompDllCtrl.Bits.dllcomp_cmn_turbocaptrim    = TurboCapTrim;
  DllCompDllCtrl.Bits.dllcomp_cmn_phsdrvprocsel   = 7;
  DllCompDllCtrl.Bits.dllcomp_cmn_mindlybwsel     = MrcGetDllBwSel (MrcData);
  DllCompDllCtrl.Bits.dllcomp_cmn_mdllengthsel    = Gear2 ? 1 : (Gear4 ? 2 : 0);  // G1: 0, G2: 1, G4: 2
  DllCompDllCtrl.Bits.mdllen                      = 0;    // @todo_adl
  DllCompDllCtrl.Bits.dllcomp_cmn_bonus           = 0x78;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DLLCTRL_REG, DllCompDllCtrl.Data);

  DllCompVcdlCtrl.Data = 0;
  DllCompVcdlCtrl.Bits.vctldaccode                = 0x187;
  DllCompVcdlCtrl.Bits.dllcomp_cmn_vctlcaptrim    = TurboCapTrim;
  DllCompVcdlCtrl.Bits.vcdlbwsel                  = MrcGetDllBwSel (MrcData);
  DllCompVcdlCtrl.Bits.dllcomp_cmn_vcdldcccode    = 8;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG, DllCompVcdlCtrl.Data);

  // dllcomp_cmn_vdllhidaccode      256 * (0.8 / param.VCCDD2)  // in [Volts]
  // dllcomp_cmn_vdlllodaccode      256 * (0.45 / param.VCCDD2)
  // dllcomp_cmn_vdlltargdaccode    256 * ((DdrDataRate > 5200 ? 0.75 : 0.65) / param.VCCDD2)

  DllCompVdllCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VDLLCTRL_REG);
  DllCompVdllCtrl.Bits.dllcomp_cmn_vdllhidaccode    = (256 * (((DataRate >= f4000) || (((Ddr4) && (Gear1)) && (DataRate >= f2667))) ? 850 : 800)) / Vdd2Mv;
  DllCompVdllCtrl.Bits.dllcomp_cmn_vdlllodaccode    = (256 * 450) / Vdd2Mv;
  if ((Lpddr5) && (Outputs->Frequency > f4800)) {
    DllCompVdllCtrl.Bits.dllcomp_cmn_vdlltargdaccode  = (256 * (780)) / Vdd2Mv;
  } else {
    DllCompVdllCtrl.Bits.dllcomp_cmn_vdlltargdaccode  = (256 * (((DataRate >= f4000) || (((Ddr4) && (Gear1)) && (DataRate >= f2667))) ? 750 : 650)) / Vdd2Mv;
  }
  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "dllcomp_cmn_vdlltargdaccode: %u\n", DllCompVdllCtrl.Bits.dllcomp_cmn_vdlltargdaccode);
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VDLLCTRL_REG, DllCompVdllCtrl.Data);

  DllCompSwCapCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_REG);
  // min(0.022 * DdrDataRate + 160, 255) where DdrDataRate is the DDR frequency in MHz
  Data32 = (22 * DataRate) + 160000;
  Data32 = UDIVIDEROUND (Data32, 1000);
  DllCompSwCapCtrl.Bits.ldoffcodelock = MIN (Data32, 255);
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_REG, DllCompSwCapCtrl.Data);
  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "ldoffcodelock: %u\n", DllCompSwCapCtrl.Bits.ldoffcodelock);

  VsshiCompCtrl0.Data = 0;
  VsshiCompCtrl0.Bits.vsshiffleakstlegen                = 1;
  VsshiCompCtrl0.Bits.txvsshiffstlegen                  = 1;
  VsshiCompCtrl0.Bits.rxvsshiffstrobestlegen            = 1;
  VsshiCompCtrl0.Bits.vsshicomp_cmn_ffleakrodivsel      = 1;
  VsshiCompCtrl0.Bits.vsshicomp_cmn_encompleaker        = 1;
  VsshiCompCtrl0.Bits.compffrxendata                    = Ddr5;
  VsshiCompCtrl0.Bits.compennmospup                     = EnNmosPup;
  VsshiCompCtrl0.Bits.vsshicomp_cmn_complocalvsshibyp   = LocalVsshiBypass;
  VsshiCompCtrl0.Bits.compleaken                        = 1;
  VsshiCompCtrl0.Bits.vsshicomp_cmn_compglobalvsshibyp  = GlobalVsshiBypass;
  VsshiCompCtrl0.Bits.vsshicomp_cmn_compvddqvsshibyp    = LocalVsshiBypass;
  VsshiCompCtrl0.Bits.vsshicomp_cmn_comptxpupusevcciog  = EnNmosPup;
  VsshiCompCtrl0.Bits.vsshicomp_cmn_comptxpdnusevcciog  = TxPdnUseVcciog;
  VsshiCompCtrl0.Bits.vsshicomp_cmn_accoupvrefsel       = 0x10;
  VsshiCompCtrl0.Bits.vsshicomp_cmn_accoupselswing      = 3;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSSHICOMP_CTRL0_REG, VsshiCompCtrl0.Data);

  VsshiCompCtrl1.Data = 0;
  VsshiCompCtrl1.Bits.vsshicomp_cmn_ffleakrofreqadj = 0x5;
  VsshiCompCtrl1.Bits.swcaprxrst_cycles             = 0x3;
  VsshiCompCtrl1.Bits.swcaprxprech_cycles           = 0x3;
  VsshiCompCtrl1.Bits.swcaprxeval_cycles            = 0x1;
  VsshiCompCtrl1.Bits.swcaprxsampnum_cycles         = 0x1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSSHICOMP_CTRL1_REG, VsshiCompCtrl1.Data);

  VsshiCompFfComp.Data = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSSHICOMP_FFCOMP_REG, VsshiCompFfComp.Data);  // All zero values

  VsshiCompCtrl2.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_REG);

  VsxhiOpAmpCh0Ctrl.Data = 0;
  VsxhiOpAmpCh0Ctrl.Bits.ch0_biasvrefen                   = !GlobalVsshiBypass;
  VsxhiOpAmpCh0Ctrl.Bits.vsxhiopamp_ch0_opampbiasctl      = 0x2;
  VsxhiOpAmpCh0Ctrl.Bits.vsxhiopamp_ch0_opampbsxhibwctl   = 0x3;
  VsxhiOpAmpCh0Ctrl.Bits.ch0_opampen                      = 0x1;
  VsxhiOpAmpCh0Ctrl.Bits.vsxhiopamp_ch0_opamptargetvref   = VsshiCompCtrl2.Bits.vsshicompcr_leakvrefcode;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_REG, VsxhiOpAmpCh0Ctrl.Data);

  VsxhiOpAmpCh1Ctrl.Data = 0;
  VsxhiOpAmpCh1Ctrl.Bits.vsxhiopamp_cmn_rxrloadcomp       = 0xE;
  VsxhiOpAmpCh1Ctrl.Bits.ch1_biasvrefen                   = !GlobalVsshiBypass;
  VsxhiOpAmpCh1Ctrl.Bits.vsxhiopamp_ch1_opampbiasctl      = 0x2;
  VsxhiOpAmpCh1Ctrl.Bits.vsxhiopamp_ch1_opampbsxhibwctl   = 0x3;
  VsxhiOpAmpCh1Ctrl.Bits.ch1_opampen                      = 0x1;
  VsxhiOpAmpCh1Ctrl.Bits.vsxhiopamp_ch1_opamptargetvref   = VsshiCompCtrl2.Bits.vsshicompcr_leakvrefcode;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH1_CTRL_REG, VsxhiOpAmpCh1Ctrl.Data);

  CompDllWl.Data = 0;
  CompDllWl.Bits.vctl_compperiodicen  = 0x1;
  CompDllWl.Bits.num_samples          = 0;
  CompDllWl.Bits.ch0_pwrgood          = 0x1;
  CompDllWl.Bits.ch1_pwrgood          = 0x1;
  CompDllWl.Bits.wl_step_size         = 0x7;
  CompDllWl.Bits.wl_sample_wait       = 0x6;
  CompDllWl.Bits.vctlcompareen_dly    = 0x6;
  CompDllWl.Bits.vctldaccode          = (((DataRate > f5200) ? 750 : 650) * 4 * 512) / Inputs->VccIomV / 5;
  CompDllWl.Bits.wlcompen             = 0x1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPDLLWL_REG, CompDllWl.Data);

  TxControl.Data = 0;
  TxControl.Bits.dllcomp_cmn_dccrangesel          = 0x3;
  TxControl.Bits.txpbd_cmn_txdccrangesel          = 0x3;
  TxControl.Bits.bonus11                          = 0x5;
  TxControl.Bits.allcomps_cmn_txshrennbiasboost1  = SaveOutputs->EnNBiasBoost;
  TxControl.Bits.txpbd_cmn_dccctrlcodeph0         = 0x80;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_TXCONTROL_REG, TxControl.Data);

  ViewDigTxCtrl.Data = 0;
  ViewDigTxCtrl.Bits.viewdigtx_cmn_txrcompdrvdn   = 0x2F;
  ViewDigTxCtrl.Bits.viewdigtx_cmn_txrcompdrvup   = 0x2F;
  ViewDigTxCtrl.Bits.viewdigtx_cmn_txtcocomp      = 0x20;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VIEWDIGTX_CTRL_REG, ViewDigTxCtrl.Data);

  ViewDigTxSel.Data = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VIEWDIGTX_SEL_REG, ViewDigTxSel.Data);      // All zero values

  ViewAnaCtrl.Data = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VIEWANA_CTRL_REG, ViewAnaCtrl.Data);        // All zero values

  ViewAnaCbbSel.Data = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VIEWANA_CBBSEL_REG, ViewAnaCbbSel.Data);    // All zero values

  CompIoLvrFfCtl.Data = 0;
  CompIoLvrFfCtl.Bits.iolvrseg_cmn_enffleg    = 0x1;
  CompIoLvrFfCtl.Bits.iolvrseg_cmn_enlvrleg   = 0x1;
  CompIoLvrFfCtl.Bits.iolvrseg_cmn_codeffleg  = 0xF;
  CompIoLvrFfCtl.Bits.iolvrseg_cmn_codelvrleg = 0xF;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMP_IOLVRFFCTL_REG, CompIoLvrFfCtl.Data);

  RxDqsCompCmnCtrl1.Data = 0;
  RxDqsCompCmnCtrl1.Bits.rxdqscomp_cmn_rxsdlbwsel       = MrcGetDllBwSel (MrcData);   // Should match primary DLL - DDRCOMP_CR_VCCDLLCOMPDATACCC.Dll_bwsel (See also dqrx_cmn_rxmdqbwsel)
  RxDqsCompCmnCtrl1.Bits.rxdqscomp_cmn_rxphsdrvprocsel  = 0x7;
  RxDqsCompCmnCtrl1.Bits.rxdqscomp_cmn_rxmctlecap       = (Gear4 && Lpddr5) ? 0x3 : 0x1;
  RxDqsCompCmnCtrl1.Bits.rxdqscomp_cmn_rxmctleeq        = (Gear4 && Lpddr5) ? 0xF : 0x8;
  RxDqsCompCmnCtrl1.Bits.rxdqscomp_cmn_rxmctleres       = 0x3;
  RxDqsCompCmnCtrl1.Bits.rxdqscomp_cmn_rxsdllengthsel   = 0x3;
  RxDqsCompCmnCtrl1.Bits.rxdqscomp_cmn_rxtailctl        = (Lpddr5 || Ddr5) ? 3 : 2;
  RxDqsCompCmnCtrl1.Bits.rxdqscomp_cmn_rxpicapsel       = 0x3;
  if (DtHalo || (UlxUlt && Lpddr5)) {
    RxDqsCompCmnCtrl1.Bits.rxdqscomp_cmn_rxdqscompclkdistcben = 0;
  }
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL1_REG, RxDqsCompCmnCtrl1.Data);

  DllCompLdoCtrl1.Data = 0;
  DllCompLdoCtrl1.Bits.dll_pien_timer_val           = A0 ? 0x324 : 0x3CF;
  DllCompLdoCtrl1.Bits.dllcomp_cmn_ldonbiasctrl     = 0x7;
  DllCompLdoCtrl1.Bits.dllcomp_cmn_ldonbiasvrefcode = 0x0B;
  DllCompLdoCtrl1.Bits.dllcomp_cmn_ldopbiasctrl     = 0x3;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_LDOCTRL1_REG, DllCompLdoCtrl1.Data);

  MiscCompCodes.Data = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_MISCCOMPCODES_REG, MiscCompCodes.Data); // All zero values; do we even need to write this ?

  CompDllStartup.Data = 0;
  CompDllStartup.Bits.dll_lock_timer        = 0x1E0;
  CompDllStartup.Bits.dll_disable_timer_val = 0x1;
  CompDllStartup.Bits.dllen_timer_val       = 0xC0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPDLLSTRTUP_REG, CompDllStartup.Data);

  // @todo_adl these are COMP codes, no need to program
  DataComp0.Data = 0;
  DataComp0.Bits.VssHiFF_dq   = 0x20;
  DataComp0.Bits.RcompOdtDown = 0x20;
  DataComp0.Bits.RcompOdtUp   = 0x20;
  DataComp0.Bits.RcompDrvDown = 0x20;
  DataComp0.Bits.RcompDrvUp   = 0x20;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRDATACOMP0_REG, DataComp0.Data);

  CmdComp.Data = 0;
  CmdComp.Bits.VssHiFF_cmd  = 0x20;
  CmdComp.Bits.ScompCmd     = 0x80;
  CmdComp.Bits.RcompDrvDown = 0x20;
  CmdComp.Bits.RcompDrvUp   = 0x20;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCMDCOMP_REG, CmdComp.Data);

  ClkComp.Data = 0;
  ClkComp.Bits.VssHiFF_clk  = 0x20;
  ClkComp.Bits.ScompClk     = 0x20;
  ClkComp.Bits.RcompDrvDown = 0x20;
  ClkComp.Bits.RcompDrvUp   = 0x20;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCLKCOMP_REG, ClkComp.Data);

  CtlComp.Data = 0;
  CtlComp.Bits.CkeCsUp      = 0x0;          // CkeCS up code for LPDDR technology   @todo_adl Should be indeed zero ?
  CtlComp.Bits.VssHiFF_ctl  = 0x20;
  CtlComp.Bits.ScompCtl     = 0x20;
  CtlComp.Bits.RcompDrvDown = 0x20;
  CtlComp.Bits.RcompDrvUp   = 0x20;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCTLCOMP_REG, CtlComp.Data);

  // @todo_Adl program using existing routine
  CompCtl0.Data = 0;
  CompCtl0.Bits.dqvrefup    = Lpddr ? 0x86 : 0x6E;
  CompCtl0.Bits.dqvrefdn    = Lpddr ? 0x57 : 0x40;
  CompCtl0.Bits.dqodtvrefup = Lpddr ? 0x74 : 0x40;
  CompCtl0.Bits.dqodtvrefdn = Lpddr ? 0x57 : 0x40;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPCTL0_REG, CompCtl0.Data);

  CompCtl1.Data = 0;
  CompVref = 120;                                   // 30 Ohm target for CmdVrefUp, this is used in rload calibration
  CompCtl1.Bits.cmdvrefup   = CompVref;
  CompCtl1.Bits.ctlvrefup   = CompVref;
  CompCtl1.Bits.clkvrefup   = CompVref;
  CompCtl1.Bits.ckecsvrefup = Lpddr ? 0x6D : 0;     // vref 8bit coding for CKECSUP with step size of Vccddq/191. Valid range is [0..191] @todo_adl - This is not used in DDR4 ?
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPCTL1_REG, CompCtl1.Data);

  StageDelay = DIVIDECEIL (20000, Outputs->Qclkps);   // 20ns in QCLK units

  CompCtl2.Data = 0;
  CompVref = Lpddr ? 0x57 : 0x50;
  CompCtl2.Bits.cmdvrefdn   = CompVref;
  CompCtl2.Bits.ctlvrefdn   = CompVref;
  CompCtl2.Bits.clkvrefdn   = CompVref;
  if (Q0Regs) {
    CompCtl2.Bits.stage3delay = StageDelay;
  }
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPCTL2_REG, CompCtl2.Data);

  CompCtl3.Data = 0;
  if (A0) {
    CompCtl3.BitsA0.stage3delay     = StageDelay;
    CompCtl3.BitsA0.stage2delay     = StageDelay;
    CompCtl3.BitsA0.codeswitchdelay = 0x40;
  } else if (Q0Regs) {
    CompCtl3.BitsQ0.stage2delay     = StageDelay;
    CompCtl3.BitsQ0.codeswitchdelay = 0x40;
    CompCtl3.BitsQ0.vsxhiopampcr_ch1_globalvsshibyp = GlobalVsshiBypass;
  } else {
    CompCtl3.Bits.stage3delay       = StageDelay;
    CompCtl3.Bits.stage2delay       = StageDelay;
    CompCtl3.Bits.codeswitchdelay   = 0x40;
  }
  CompCtl3.Bits.compinitdelay   = 0x7F;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPCTL3_REG, CompCtl3.Data);

  CompCtl4.Data = 0;
  if (Q0Regs) {
    CompCtl4.BitsQ0.enodt       = 1;      // should be (!envttodt)
    CompCtl4.BitsQ0.envttodt    = 0;      // Enable if we use Vtt termination
    CompCtl4.BitsQ0.stage6delay = StageDelay;
    CompCtl4.BitsQ0.stage5delay = StageDelay;
    CompCtl4.BitsQ0.stage4delay = StageDelay;
  } else {
    CompCtl4.Bits.enodt       = 1;      // should be (!envttodt)
    CompCtl4.Bits.envttodt    = 0;      // Enable if we use Vtt termination
    CompCtl4.Bits.ckecsen     = Lpddr;  // @todo_adl Not needed for DDR4 ?
    CompCtl4.Bits.stage6delay = StageDelay;
    CompCtl4.Bits.stage5delay = StageDelay;
    CompCtl4.Bits.stage4delay = StageDelay;
  }
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPCTL4_REG, CompCtl4.Data);

  ScompCtl.Data = 0;
  ScompCtl.Bits.vsxhiopampcr_ch0_globalvsshibyp = GlobalVsshiBypass;
  if (Q0Regs) {
    ScompCtl.BitsQ0.vsxhiopampcr_chsel            = 3;
  } else {
    ScompCtl.Bits.vsxhiopampcr_ch1_globalvsshibyp = GlobalVsshiBypass;
    ScompCtl.Bits.vsxhiopampcr_chsel              = 3;
  }
  // g1:
  // freq<=1333MHz:h36
  // freq>1333MHz&freq<=1600MHz:h40
  // freq>1600MHz&freq<=1866MHz:h4C
  // freq>1866MHz&freq<=2133MHz:h56
  // freq>2133MHz&freq<=2400MHz:h60
  // freq>2400MHz&freq<=2666MHz:h6B
  // freq>2666MHz&freq<=2933MHz:h76
  // freq>2933MHz&freq<=3200MHz:h7F
  // g2:
  // freq<=2133MHz:h2C
  // freq>2133MHz&freq<=2400MHz:h30
  // freq>2400MHz&freq<=2666MHz:h36
  // freq>2666MHz&freq<=2933MHz:h3B
  // freq>2933MHz&freq<=3200MHz:h40
  // freq>3200MHz&freq<=9200MHz:h7F
  ScompCtl.Bits.vsxhiopampcr_tbias2opamp        = QclkFrequency / 25; // @todo_adl formula ?
  ScompCtl.Bits.vsxhiopampcr_topampen2rdy       = GlobalVsshiBypass ? 0 : (QclkFrequency / 25); // @todo_adl formula ?
  ScompCtl.Bits.clkdlytapselstage               = 0xF;  // @todo_adl formula ?
  ScompCtl.Bits.ctldlytapselstage               = 0xF;  // @todo_adl formula ?
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_SCOMPCTL_REG, ScompCtl.Data);

  DdrEarlyCr.Data = 0;
  DdrEarlyCr.Bits.early_restoredone_comp = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDREARLYCR_REG, DdrEarlyCr.Data);

  if (Q0Regs) {
    VccddqldoDvfscompCtrl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VCCDDQLDO_DVFSCOMP_CTRL0_REG);
    VccddqldoDvfscompCtrl0.Bits.periodiccomp_lvrmuxvccddqgsel = 1;
    VccddqldoDvfscompCtrl0.Bits.dvfscomp_lvrmuxvccddqgsel     = 1;
    VccddqldoDvfscompCtrl0.Bits.vccddq_lvrlsbypass   = (Outputs->VccddqVoltage < VDD_0_60) ? 1 : 0;
    VccddqldoDvfscompCtrl0.Bits.vccddq_lvrtargetcode = ((96 * Outputs->VccddqVoltage) / Vdd2Mv) - 26;
    VccddqldoDvfscompCtrl0.Bits.vccddq_lvrbiasvrefsel  = 0x3;
    VccddqldoDvfscompCtrl0.Bits.vccddq_lvrcodelvrleg   = 0xF;
    VccddqldoDvfscompCtrl0.Bits.vccddq_lvrcoderes2     = 0x4;
    VccddqldoDvfscompCtrl0.Bits.vccddq_lvrcoderes1     = 0x4;
    if (Lpddr) {
      // For now, the VDDQ for LP5, LP4, DDR5, DDR4 are the same for all SAGV points. Set dvfscomp_lvrmuxvccddqgsel = 1 and it will always bypass LVR and use global vddq
      // Note: if there is a VDDQ difference during SAGV switch, need to set dvfscomp_lvrmuxvccddqgsel = 0 and it will enable LVR for early DVFS comp scenarios
      VccddqldoDvfscompCtrl0.Bits.dvfscomp_lvrmuxvccddqgsel = 1;
      VccddqldoDvfscompCtrl0.Bits.vccddq_lvrbypass = (Outputs->VccddqVoltage > VDD_0_90) ? 1 : 0;
    }
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "VCCDDQLDO_DVFSCOMP_CTRL0: 0x%X\n", VccddqldoDvfscompCtrl0.Data);
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_VCCDDQLDO_DVFSCOMP_CTRL0_REG, VccddqldoDvfscompCtrl0.Data);
  }

  return Status;
}

/**
  PHY init - sequence static_periodicdcc in COMP_NEW in FUB.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitStaticPeriodicDcc (
  IN OUT MrcParameters *const MrcData
  )
{
  DCSCTL0_STRUCT    DcsCtl0;
  DCSCTL1_STRUCT    DcsCtl1;
  DCSCTL3_STRUCT    DcsCtl3;
  DCSRUNMODE_STRUCT DcsRunMode;

  DcsCtl0.Data = 0;
  DcsCtl0.Bits.dcs_num_clk          = 1;
  DcsCtl0.Bits.dcs_recycle_mode     = 1;
  DcsCtl0.Bits.dcs_halt_mode        = 1;
  DcsCtl0.Bits.dcs_timer_cfg        = 3;
  DcsCtl0.Bits.dcs_samples          = 2;
  DcsCtl0.Bits.dcs_thresh           = 1;
  DcsCtl0.Bits.dcs_start_dl_val     = 1;
  MrcWriteCR (MrcData, DCSCTL0_REG, DcsCtl0.Data);

  DcsCtl1.Data = 0;
  DcsCtl1.Bits.dcs_mrcinitloop_en     = 1;
  DcsCtl1.Bits.dcs_cnt_load           = 0x3F;
  MrcWriteCR (MrcData, DCSCTL1_REG, DcsCtl1.Data);

  // DCSCTL2 is zero, no need to write

  DcsCtl3.Data = 0;
  DcsCtl3.Bits.dcsfsm_rst_b = 1;
  DcsCtl3.Bits.dcdclocksel  = 1;
  MrcWriteCR (MrcData, DCSCTL3_REG, DcsCtl3.Data);

  // DCSDIGCLKGATE.dcs_dyndcsclken is 1 by default

  DcsRunMode.Data = 0;
  DcsRunMode.Bits.dcs_run_mode = 1;
  MrcWriteCR (MrcData, DCSRUNMODE_REG, DcsRunMode.Data);

  return mrcSuccess;
}

/**
  PHY init - sequence ddrphy_neidengard_init_dcc.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitPeriodicDcc (
  IN OUT MrcParameters *const MrcData
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcDebug          *Debug;
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MrcStatus         Status;
  UINT64            Timeout;
  BOOLEAN           Done;
  DCSINIT_STRUCT    DcsInit;
  DCSCTL0_STRUCT    DcsCtl0;
  DCSCTL3_STRUCT    DcsCtl3;
  DCSSTATUS1_STRUCT DcsStatus1;
  DCSRUNMODE_STRUCT DcsRunMode;
  TXDLLSIGGRPDCCCTL34_STRUCT  DccCtl34;
  TXDLLSIGGRPDCCCTL35_STRUCT  DccCtl35;

  Status  = mrcSuccess;
  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Periodic DCC init\n");

  DcsInit.Data = MrcReadCR (MrcData, DCSINIT_REG);
  DcsInit.Bits.dcs_init = 1;
  MrcWriteCR (MrcData, DCSINIT_REG, DcsInit.Data);

  Timeout = MrcCall->MrcGetCpuTime () + DCC_FSM_TIMEOUT;
  do {
    Done = TRUE;                    // Assume all done
    DcsInit.Data = MrcReadCR (MrcData, DCSINIT_REG);
    if (DcsInit.Bits.dcs_init == 1) {
      Done = FALSE;
    }
  } while (!Done && (MrcCall->MrcGetCpuTime () < Timeout));

  if (!Done) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s dcs_init Timeout!\n", gWarnString);
  }

  DccCtl34.Data = 0;
  DccCtl35.Data = MrcReadCR (MrcData, TXDLLSIGGRPDCCCTL35_REG);
  DccCtl34.Bits.periodic_init_dcccode = DccCtl35.Bits.periodic_initcode_status;
  MrcWriteCR (MrcData, TXDLLSIGGRPDCCCTL34_REG, DccCtl34.Data);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "periodic_init_dcccode: %u\n", DccCtl34.Bits.periodic_init_dcccode);

  DcsStatus1.Data = MrcReadCR (MrcData, DCSSTATUS1_REG);

  DcsCtl0.Data = MrcReadCR (MrcData, DCSCTL0_REG);
  DcsCtl0.Bits.dcs_start_dl_val     = DcsStatus1.Bits.dcs_dl_code;
  DcsCtl0.Bits.dcs_timer_cfg        = 0;
  DcsCtl0.Bits.dcs_recycle_mode     = 1;
  MrcWriteCR (MrcData, DCSCTL0_REG, DcsCtl0.Data);

  // Toggle the FSM reset
  DcsCtl3.Data = MrcReadCR (MrcData, DCSCTL3_REG);
  DcsCtl3.Bits.dcsfsm_rst_b = 0;
  MrcWriteCR (MrcData, DCSCTL3_REG, DcsCtl3.Data);
  MrcWait (MrcData, 30 * MRC_TIMER_1NS);
  DcsCtl3.Bits.dcsfsm_rst_b = 1;
  MrcWriteCR (MrcData, DCSCTL3_REG, DcsCtl3.Data);

  // Switch to Periodic mode
  DcsRunMode.Data = MrcReadCR (MrcData, DCSRUNMODE_REG);
  DcsRunMode.Bits.dcs_run_mode = 2;
  MrcWriteCR (MrcData, DCSRUNMODE_REG, DcsRunMode.Data);

  return Status;
}

/**
  PHY init - compupdone_ovrrideen_duringprecomp / compupdone_ovrridedisable_duringprecomp

  @param[in, out] MrcData - Include all MRC global data
  @param[in]      Value   - Override value (0 or 1)

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitCompUpdateOverride (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               Value
  )
{
  MrcStatus Status;
  CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT   CccGlobalPmFsmOvrd0;
  CCC0_GLOBAL_CR_PM_FSM_OVRD_1_STRUCT   CccGlobalPmFsmOvrd1;
  DATA0CH0_CR_PMFSM1_STRUCT             DataPmFsm1;

  Status = mrcSuccess;

  // Read from the first instance, assume these CR's are the same across all FUBs
  CccGlobalPmFsmOvrd0.Data = MrcReadCR (MrcData, CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG);
  CccGlobalPmFsmOvrd1.Data = MrcReadCR (MrcData, CCC0_GLOBAL_CR_PM_FSM_OVRD_1_REG);
  DataPmFsm1.Data = MrcReadCR (MrcData, DATA0CH0_CR_PMFSM1_REG);

  CccGlobalPmFsmOvrd0.Bits.compupdatedone_ovren  = Value;
  CccGlobalPmFsmOvrd1.Bits.compupdatedone_ovren  = Value;
  DataPmFsm1.Bits.compupdatedone_ovren           = Value;

  // Program all FUBs using multicast
  MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd1.Data);

  return Status;
}

/**
  PHY init - modmem_init_configflow_tagged_seq1

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitSeq1 (
  IN OUT MrcParameters *const MrcData,
  OUT    UINT32               *ClkPiValue,
  OUT    UINT32               *CtlPiValue
  )
{
  MrcStatus         Status;
  MrcStatus         Status2;
  UINT32            Controller;
  UINT32            Channel;
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MrcDebug          *Debug;
  MrcSaveData       *SaveOutputs;
  MrcDdrType        DdrType;
  BOOLEAN           Lpddr;
  BOOLEAN           Lpddr5;
  BOOLEAN           Ddr4;
  BOOLEAN           Ddr5;
  BOOLEAN           Gear1;
  BOOLEAN           Gear2;
  BOOLEAN           Gear4;
  BOOLEAN           Matched;
  BOOLEAN           A0;
  BOOLEAN           UlxUlt;
  BOOLEAN           ScompCmdBypass;
  UINT16            QclkPs;
  UINT32            QclkFrequency;
  UINT32            DataRate;
  INT32             SData32;
  UINT32            Data32;
  UINT32            UiLock;
  UINT32            ReadDqsOffset;
  UINT32            DividerRatio;
  UINT32            Offset;
  UINT32            SampleDiv;
  UINT32            HiBwDiv;
  UINT32            LowBwDiv;
  UINT32            VtOffset;
  UINT32            VtSlope;
  UINT32            Vdd2Mv;
  DDRPHY_COMP_CR_COMPOVERRIDE_STRUCT              CompOverride;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT               FsmSkipCtrl;
  DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_STRUCT         PanicCompCtrl3;
  DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_STRUCT        CompDataComp1;
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_STRUCT     VccDllCompDataCcc;
  DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_STRUCT  DimmVrefControl;
  DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_STRUCT      DimmVrefCh0;
  DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_STRUCT      DimmVrefCh1;
  DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_STRUCT  VrefAdjust2;
  DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_STRUCT       VsshiCompCtrl2;
  DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_STRUCT       VsshiCompCtrl3;
  DDRPHY_COMP_CR_COMPDLLWL_STRUCT                 CompDllWl;
  DDRSCRAM_CR_DDREARLYCR_STRUCT                   ScramDdrEarlyCr;
  CH0CCC_CR_MDLLCTL0_STRUCT                       CccMdllCtl0;
  CH0CCC_CR_MDLLCTL1_STRUCT                       CccMdllCtl1;
  CH0CCC_CR_MDLLCTL2_STRUCT                       CccMdllCtl2;
  CH0CCC_CR_MDLLCTL3_STRUCT                       CccMdllCtl3;
  CH0CCC_CR_COMP3_STRUCT                          CccComp3;
  DATA0CH0_CR_MDLLCTL0_STRUCT                     DataMdllCtl0;
  DATA0CH0_CR_MDLLCTL1_STRUCT                     DataMdllCtl1;
  DATA0CH0_CR_MDLLCTL2_STRUCT                     DataMdllCtl2;
  DATA0CH0_CR_MDLLCTL3_STRUCT                     DataMdllCtl3;
  CCC_GLOBAL_IOLVRCTL_STRUCT                      CccGlobalIoLvrCtl;
  CCC_GLOBAL_CR_DDREARLYCR_STRUCT                 CccGlobalDdrEarlyCr;
  DATA_GLOBAL_CR_IOLVRCTL0_STRUCT                 DataGlobalIoLvrCtl0;
  DATA_GLOBAL_CR_DDREARLYCR_STRUCT                DataGlobalDdrEarlyCr;
  CCC_CR_CLKALIGNCTL1_STRUCT                      CccClkAlignCtl1;
  DATA_CR_CLKALIGNCTL1_STRUCT                     DataClkAlignCtl1;
  DATA_CR_SDLCTL1_STRUCT                          DataSdlCtl1;
  DATA_CR_AFEMISCCTRL1_STRUCT                     AfeMiscCtrl1;
  DATA0CH0_CR_DDRCRDATACOMP1_STRUCT               DdrDataComp1;
  DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT               VccDllCompData;
  CH0CCC_CR_CCCRSTCTL_STRUCT                      CccRstCtl;
  CH0CCC_CR_DDRCRCCCPIDIVIDER_STRUCT              CccPiDivider;
  DATA0CH0_CR_CLKALIGNCTL0_STRUCT                 DataClkAlignCtl0;
  DATA0CH0_CR_CLKALIGNCTL2_STRUCT                 DataClkAlignCtl2;
  CH0CCC_CR_CLKALIGNCTL0_STRUCT                   CccClkAlignCtl0;
  CH0CCC_CR_CLKALIGNCTL2_STRUCT                   CccClkAlignCtl2;
  CCC0_GLOBAL_CR_PMTIMER0_STRUCT                  CccGlobalPmTimer0;
  CCC0_GLOBAL_CR_PMTIMER1_STRUCT                  CccGlobalPmTimer1;
  DATA0_GLOBAL_CR_PMTIMER0_STRUCT                 DataGlobalPmTimer0;
  DATA0_GLOBAL_CR_PMTIMER1_STRUCT                 DataGlobalPmTimer1;
  DDRVCCDLL0_CR_AFE_CTRL1_NEW_STRUCT              VccDllAfeCtrl1New;
  DDRVCCDLL0_CR_AFE_CTRL2_STRUCT                  VccDllAfeCtrl2;
  DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_STRUCT           DllCompDccCtrl;
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_STRUCT    VccDllCompDataCcc1;
  DATA0CH0_CR_DCCCTL14_STRUCT                     DccCtl14;
  CH0CCC_CR_DCCCTL9_STRUCT                        CccDccCtl9;
  DDRPHY_COMP_CR_DCCCTL4_STRUCT                   CompDccCtl4;
  CCC0_GLOBAL_CR_COMPUPDT_DLY0_STRUCT             CompUpdtDly0;
  CCC0_GLOBAL_CR_COMPUPDT_DLY1_STRUCT             CompUpdtDly1;
  MCMISCS_COMPUPDATE_CTRL_STRUCT                  CompUpdateCtrl;

  Status      = mrcSuccess;
  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  SaveOutputs = &MrcData->Save.Data;
  DdrType     = Outputs->DdrType;
  Lpddr       = Outputs->Lpddr;
  Ddr4        = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5        = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Vdd2Mv      = Outputs->Vdd2Mv;
  QclkPs      = Outputs->Qclkps;
  Gear2       = Outputs->Gear2;
  Gear4       = Outputs->Gear4;
  Gear1       = !Gear2 && !Gear4;
  A0          = Inputs->A0;
  UlxUlt      = Inputs->UlxUlt;

  DataRate = Outputs->Frequency;
  if (Gear2) {
    QclkFrequency = DataRate / 2;
  } else if (Gear4) {
    QclkFrequency = DataRate / 4;
  } else { // Gear1
    QclkFrequency = DataRate;
  }
  Matched     = (Outputs->RxMode == MrcRxModeMatchedN) || (Outputs->RxMode == MrcRxModeMatchedP);
  UiLock      = Matched ? 0 : SaveOutputs->UiLock;
  ReadDqsOffset = 0;

  ScompCmdBypass = (!Lpddr && Outputs->Any2Dpc) ? 0 : 1;  // Bypass CA slew rate on LP4/5 and DDR4/5 1DPC

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMrcPhyInitSeq1 started\n");

  Offset = A0 ? DDRSCRAM_CR_DDRLASTCR_A0_REG : DDRSCRAM_CR_DDRLASTCR_REG;
  MrcWriteCR (MrcData, Offset, 0);  // Set EnInitComplete to zero in case we repeat phy init in SAGV flow

  //---------------------------------------------
  // static_alwaysfirst - CCC_GLOBAL
  //---------------------------------------------
  if (!A0) {
    CompUpdtDly0.Data = 0;
    // These are used by the comp FSMs for CA and CS
    CompUpdtDly0.Bits.ca_holddly    = 7;
    CompUpdtDly0.Bits.ca_updtwidth  = 5;
    CompUpdtDly0.Bits.ca_setupdly   = 3;
    CompUpdtDly0.Bits.cs_holddly    = 7;
    CompUpdtDly0.Bits.cs_updtwidth  = 5;
    CompUpdtDly0.Bits.cs_setupdly   = 3;
    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_COMPUPDT_DLY0_REG, CompUpdtDly0.Data);

    CompUpdtDly1.Data = 0;
    // These are used by the comp FSMs for CK
    CompUpdtDly1.Bits.ck_holddly    = 0xd;
    CompUpdtDly1.Bits.ck_updtwidth  = 7;
    CompUpdtDly1.Bits.ck_setupdly   = 3;
    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_COMPUPDT_DLY1_REG, CompUpdtDly1.Data);
  }

  //---------------------------------------------
  // static_alwaysfirst SCRAM - done inside MrcDdrIoPreInit
  //---------------------------------------------

  //---------------------------------------------
  // static_alwaysfirst - CCC
  //---------------------------------------------
  CccRstCtl.Data = 0;
  CccRstCtl.Bits.IamclkP = 5;
  Offset = A0 ? CCC_CR_CCCRSTCTL_A0_REG : CCC_CR_CCCRSTCTL_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccRstCtl.Data);

  //---------------------------------------------
  // static_compinit_mrc - COMP
  //---------------------------------------------
  CompOverride.Data = 0;
  // These are used by the precomp FSMs
  CompOverride.Bits.compclkon     = 1;
  CompOverride.Bits.dllcompclkon  = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG, CompOverride.Data);

  FsmSkipCtrl.Data = 0xFFFFFFFF; // We will enable most of the bits, except the ones below
  FsmSkipCtrl.Bits.En_cmddn200        = Lpddr ? 0 : 1;
  FsmSkipCtrl.Bits.En_ckecsup         = Lpddr ? 1 : 0;
  FsmSkipCtrl.Bits.En_code2vt         = Ddr4  ? 1 : 0;
  FsmSkipCtrl.Bits.En_vsshiffrx       = Ddr5  ? 1 : 0;
  FsmSkipCtrl.Bits.En_vsshiffleakvddq = Lpddr5 ? 0 : 1;
  FsmSkipCtrl.Bits.En_vsshiffcmd      = Lpddr5 ? 0 : 1;
  FsmSkipCtrl.Bits.En_vsshiffctl      = Lpddr5 ? 0 : 1;
  FsmSkipCtrl.Bits.En_vsshiffclk      = Lpddr5 ? 0 : 1;
  FsmSkipCtrl.Bits.En_vsshiffdq       = Lpddr5 ? 0 : 1;
  FsmSkipCtrl.Bits.En_dqsfwdclkp      = Matched ? 0 : 1;
  FsmSkipCtrl.Bits.En_dqsfwdclkn      = FsmSkipCtrl.Bits.En_dqsfwdclkp;
  FsmSkipCtrl.Bits.En_dqsrload        = 0;  // This one will only be used during RLoad calibration
  FsmSkipCtrl.Bits.En_codepi          = 0;  // @todo_adl This one will only be used during ffcodelock / ffcode cal
  FsmSkipCtrl.Bits.En_coderead        = 0;  // @todo_adl This one will only be used during ffcodelock / ffcode cal
  FsmSkipCtrl.Bits.En_scompclk        = 0;
  FsmSkipCtrl.Bits.En_scompctl        = 0;
  FsmSkipCtrl.Bits.En_scompcmd        = !ScompCmdBypass;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrl.Data);

  //---------------------------------------------
  // static_compinit - COMP_NEW
  //---------------------------------------------
  if (!A0) {
    VccDllCompDataCcc1.Data = 0;
    VccDllCompDataCcc1.Bits.rcomp_cmn_odtdnrelpupstatlegen = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_ctldnrelpupstatlegen = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_clkdnrelpupstatlegen = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_dqdnrelpupstatlegen  = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_cmddnrelpupstatlegen = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_odtdnrelpdnstatlegen = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_ctldnrelpdnstatlegen = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_clkdnrelpdnstatlegen = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_dqdnrelpdnstatlegen  = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_cmddnrelpdnstatlegen = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_ckecsupextstatlegen  = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_odtupextstatlegen    = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_ctlupextstatlegen    = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_clkupextstatlegen    = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_dqupextstatlegen     = 1;
    VccDllCompDataCcc1.Bits.rcomp_cmn_cmdupextstatlegen    = 1;
    VccDllCompDataCcc1.Bits.Binfsm4_codedelay              = 0xC0;
    VccDllCompDataCcc1.Bits.Binfsm4_offsetsearch           = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_REG, VccDllCompDataCcc1.Data);
  }

  DllCompDccCtrl.Data = 0;
  DllCompDccCtrl.Bits.ldofbdivsel       = 1;
  DllCompDccCtrl.Bits.dccctrlcode       = 0x80;
  DllCompDccCtrl.Bits.dllcomp_cmn_dccen = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG, DllCompDccCtrl.Data);

  PanicCompCtrl3.Data = 0;

  // Data32 = 100 + Inputs->RcompTarget[WrDSCmd] * 2;
  // Vref_pupcode = 192 * Rext / (Rext + Rtarg_pup * NumSeg); Rext= 100 Ohm,
  // 30 Ohm target for CmdVrefUp, this is used in rload calibration
  Data32                                          = Inputs->RcompResistor + WRDS_CMD_RLOAD * 2;
  PanicCompCtrl3.Bits.pnccomp_rloaddqs_vrefcode   = UDIVIDEROUND (192 * Inputs->RcompResistor, Data32);  // (192 * 100) / (100 + cmdronup*2)
  PanicCompCtrl3.Bits.pnccomp_cmddn200_vrefcode   = 0x44; // @todo_adl Formula ?
  PanicCompCtrl3.Bits.pnccomp_vttpanicdn_vrefcode = 0x48; // @todo_adl Formula ?
  PanicCompCtrl3.Bits.pnccomp_vttpanicup_vrefcode = 0x42; // @todo_adl Formula ?
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_REG, PanicCompCtrl3.Data);

  CompDataComp1.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_REG);
  CompDataComp1.Bits.Dqsnoffsetnui  = 0;
  CompDataComp1.Bits.Dqsntargetnui  = UiLock;
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_REG, CompDataComp1.Data);

  VccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);
  VccDllCompDataCcc.Bits.Dqspoffsetnui  = 0;
  VccDllCompDataCcc.Bits.Dqsptargetnui  = UiLock;
  VccDllCompDataCcc.Bits.Dll_bwsel      = MrcGetDllBwSel (MrcData);
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG, VccDllCompDataCcc.Data);

  DdrDataComp1.Data = 0;
  DdrDataComp1.Bits.Dqsnoffsetnui = 0;
  DdrDataComp1.Bits.Dqsntargetnui = UiLock;
  DdrDataComp1.Bits.Vsshiff_rx    = 0x20;
  MrcWriteCrMulticast (MrcData, DATA_CR_DDRCRDATACOMP1_REG, DdrDataComp1.Data);

  // Dll_xxx fields are updated by COMP FSM, so we can write zeroes here
  VccDllCompData.Data = 0;
  VccDllCompData.Bits.Offsetnui     = 0;
  VccDllCompData.Bits.Targetnui     = UiLock;
  MrcWriteCrMulticast (MrcData, DATA_CR_VCCDLLCOMPDATA_REG, VccDllCompData.Data);

  /*
  TempVar1 = DIVIDEROUND (VccIoMv, 128);
  TempVar2 = 2 * Vdd2Mv;
  TempVar3 = DIVIDEROUND (TempVar2, 382);
  TempVar4 = DIVIDEROUND (TempVar3, TempVar1);
  VtSlope = TempVar4;
  TempVar1 = DIVIDEROUND (Vdd2Mv, 96);
  SignedTempVar = DIVIDEROUND (42, TempVar1);
  VtOffset = RANGE (SignedTempVar, -16, 15);
  */
  VtOffset = 0;
  VtSlope  = 0x10;

  if (QclkFrequency <= 1600) {
    HiBwDiv   = 2; // 16 samples
    LowBwDiv  = 3; // 32 samples
    SampleDiv = 1; // QCLK / 4
  } else {
    HiBwDiv   = 1; // 8 samples
    LowBwDiv  = 2; // 16 samples
    SampleDiv = 2; // QCLK / 8
  }

  DimmVrefControl.Data = 0;
  DimmVrefControl.Bits.hibwdivider    = HiBwDiv;
  DimmVrefControl.Bits.lobwdivider    = LowBwDiv;
  DimmVrefControl.Bits.sampledivider  = MIN (SampleDiv, DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_sampledivider_MAX);
  DimmVrefControl.Bits.hibwenable     = 1;
  DimmVrefControl.Bits.vtoffset       = VtOffset;
  DimmVrefControl.Bits.vtslope        = VtSlope;
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_REG, DimmVrefControl.Data);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DIMMVREF_VREFCONTROL: 0x%X\n", DimmVrefControl.Data);

  DimmVrefCh0.Data = 0;
  DimmVrefCh1.Data = 0;
  Data32 = MID_INT_VREF;
  if (Ddr4) {
    if ((Inputs->MemoryProfile != STD_PROFILE) && (Outputs->Frequency >= f3200)) {
      Data32 -= 10;
    }
    if (MrcRankExist (MrcData, 0, 0, 0)) {    // MC0 DIMM0
      DimmVrefCh0.Bits.enca0vref = 1;
      DimmVrefCh0.Bits.ca0vref   = Data32;
    }
    if (UlxUlt) {
      if (MrcRankExist (MrcData, 1, 0, 0)) {  // MC1 DIMM0
        DimmVrefCh0.Bits.enca1vref = 1;
        DimmVrefCh0.Bits.ca1vref   = Data32;
      }
    } else {
      if (MrcRankExist (MrcData, 0, 0, 2)) {  // MC0 DIMM1 - if we have Rank2, then DIMM1 is enabled
        DimmVrefCh0.Bits.enca1vref = 1;
        DimmVrefCh0.Bits.ca1vref   = Data32;
      }
      if (MrcRankExist (MrcData, 1, 0, 0)) {  // MC1 DIMM0
        DimmVrefCh1.Bits.enca0vref = 1;
        DimmVrefCh1.Bits.ca0vref   = Data32;
      }
      if (MrcRankExist (MrcData, 1, 0, 2)) {  // MC1 DIMM1 - if we have Rank2, then DIMM1 is enabled
        DimmVrefCh1.Bits.enca1vref = 1;
        DimmVrefCh1.Bits.ca1vref   = Data32;
      }
    }
  }
  DimmVrefCh0.Bits.qxcount = MrcFrequencyToRatio (MrcData, QclkFrequency, MRC_REF_CLOCK_133, 0);  // Ratio of QCLK in terms of 133.3333
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "QclkFrequency: %d, qxcount: %d\n", QclkFrequency, DimmVrefCh0.Bits.qxcount);
  DimmVrefCh0.Bits.sagvopenloopen = 1;

  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_REG, DimmVrefCh0.Data);
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_REG, DimmVrefCh1.Data);

  if (Ddr4) {
    ChangeMargin (MrcData, CmdV, Data32, 0, 1, 0, 0, 0, 0, 0, 1, 0);
  }

  /* TGL code
  VrefAdj2.Bits.HiZTimerCtrl  = DDRVREF_CR_DDRCRVREFADJUST2_HiZTimerCtrl_MAX;
  VrefAdj2.Bits.LockOvrd = SAFE ? 0 : 1;
  TempVar1 = (1000 / RefClkPs) - 4; // TBDns
  TempVar2 = MrcLog2(TempVar1);
  VrefAdj2.Bits.LockTimer    = RANGE(TempVar2, 0, 7);
#ifdef CTE_FLAG
  VrefAdj2.Bits.LockTimer = 0;
#endif
  TempVar1 = (Vdd2Mv / 382);
  TempVar2 = VccIoMv / 128;
  TempVar3 = DIVIDEROUND(TempVar1, TempVar2);
  //MaxMin( Rnd(TBD*(VccDD2/382) / (VccIO/128)), 0, 31)
  VrefAdj2.Bits.VtSlopeSAGV = RANGE(TempVar3, 0, 31);
  SignedTempVar1 = (Vdd2Mv / 96);
  // MaxMin( Rnd(TBD mV / (VccDD2/96) ), -16, 15).
  VrefAdj2.Bits.VtOffsetSAGV = RANGE(SignedTempVar1, -16, 15);
  VrefAdj2.Bits.SagvVtCtl   = 1;
  VrefAdj2.Bits.GateICinDVFS = SAFE ? 1 : 0;
  MrcWriteCR (MrcData, DDRVREF_CR_DDRCRVREFADJUST2_REG, VrefAdj2.Data);  */

  VrefAdjust2.Data = 0;
  VrefAdjust2.Bits.hiztimerctrl = 2;    // DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_hiztimerctrl_MAX ?
  VrefAdjust2.Bits.lockovrd     = 1;
  VrefAdjust2.Bits.locktimer    = 0;    // Formula ?
  VrefAdjust2.Bits.vtslopesagv  = 0x10; // Formula ?
  VrefAdjust2.Bits.vtoffsetsagv = 0x10; // Formula ?
  VrefAdjust2.Bits.gateicindvfs = 1;
  VrefAdjust2.Bits.sagvvtctl    = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_REG, VrefAdjust2.Data);

  // param.VCCDD2<=1.05 ? 0x00 ? (192 * min(0.3,(param.VCCDD2 - param.VCCIO)) / param.VCCDD2)
  Data32 = Vdd2Mv - Inputs->VccIomV;
  Data32 = MIN (Data32, 300);
  Data32 = Data32 * 192;
  Data32 = UDIVIDEROUND (Data32, Vdd2Mv);

  VsshiCompCtrl2.Data = 0;
  VsshiCompCtrl2.Bits.vsshicompcr_leakvrefcode  = Data32;
  VsshiCompCtrl2.Bits.vsshicompcr_rxvrefcode    = Data32;
  VsshiCompCtrl2.Bits.vsshicompcr_cmdvrefcode   = Data32;
  VsshiCompCtrl2.Bits.vsshicompcr_dqvrefcode    = Data32;
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_REG, VsshiCompCtrl2.Data);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VSSHICOMP_CTRL2: %08Xh\n", VsshiCompCtrl2.Data);

  VsshiCompCtrl3.Data = 0;
  VsshiCompCtrl3.Bits.vsshicompcr_ctlvrefcode   = Data32;
  VsshiCompCtrl3.Bits.vsshicompcr_clkvrefcode   = Data32;
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_REG, VsshiCompCtrl3.Data);


  //---------------------------------------------
  // static_compinit - COMP
  //---------------------------------------------
  Status |= MrcPhyInitCompStaticCompInit (MrcData);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcPhyInitCompStaticCompInit Done\n");

  //---------------------------------------------
  // static_periodicdcc - COMP_NEW
  //---------------------------------------------
  if (!A0 && Inputs->PeriodicDcc) {
    Status |= MrcPhyInitStaticPeriodicDcc (MrcData);
  }

  CompDllWl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMPDLLWL_REG);
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPDLLWL_REG, CompDllWl.Data);

  //---------------------------------------------
  // static_DLLEARLY - CMD
  //---------------------------------------------
  CccMdllCtl0.Data = 0;
  CccMdllCtl0.Bits.mdll_cmn_mdllen          = 1;                            // @todo_adl Can set to 0 on empty CCC partitions to save power
  CccMdllCtl0.Bits.mdll_cmn_pien            = Gear4 ? 0xFF : 0x5F;          // @todo_adl We can disable some PI's depending on channel population and DRAM type, need table from DE
  CccMdllCtl0.Bits.mdll_cmn_mdllengthsel    = Gear1 ? 0 : (Gear4 ? 2 : 1);  // G1: 0, G2: 1, G4: 2
  CccMdllCtl0.Bits.mdll_cmn_dllbypassen     = 0;
  CccMdllCtl0.Bits.mdll_cmn_wlrdacholden    = 1;
  CccMdllCtl0.Bits.mdll_cmn_turboonstartup  = 1;
  SData32 = 359955 - 112 * QclkFrequency;
  SData32 = DIVIDEROUND (SData32, 100000);                                  // ROUND(QclkFrequency * (-0.00112) + 3.59955)
  CccMdllCtl0.Bits.mdll_cmn_turbocaptrim    = MAX (SData32, 0);             // Protect against negative values
  Offset = A0 ? CCC_CR_MDLLCTL0_A0_REG : CCC_CR_MDLLCTL0_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccMdllCtl0.Data);

  CccMdllCtl1.Data = 0;
  CccMdllCtl1.Bits.mdll_cmn_phsdrvprocsel = 7;
  CccMdllCtl1.Bits.mdll_cmn_picapsel      = 3;
  CccMdllCtl1.Bits.mdll_cmn_vctlcaptrim   = CccMdllCtl0.Bits.mdll_cmn_turbocaptrim;
  if (Gear4 && Lpddr5) {
    CccMdllCtl1.Bits.mdll_cmn_vcdlbwsel  = MrcGetDllBwSel (MrcData);
  }
  Offset = A0 ? CCC_CR_MDLLCTL1_A0_REG : CCC_CR_MDLLCTL1_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccMdllCtl1.Data);

  CccMdllCtl2.Data = 0;
  CccMdllCtl2.Bits.mdll_cmn_dccen             = 1;
  CccMdllCtl2.Bits.mdll_cmn_dllldoen          = 1;    // @todo_adl Can set to 0 on empty CCC partitions to save power
  CccMdllCtl2.Bits.mdll_cmn_vctlcompoffsetcal = 0x10;
  CccMdllCtl2.Bits.mdll_cmn_vcdldcccode       = 8;
  CccMdllCtl2.Bits.mdll_cmn_ldofbdivsel       = 1;
  // min(0.022 * DdrDataRate + 160, 255) where DdrDataRate is the DDR frequency in MHz
  Data32 = (22 * DataRate) + 160000;
  Data32 = UDIVIDEROUND (Data32, 1000);
  CccMdllCtl2.Bits.mdll_cmn_ldoffcodelock     = MIN (Data32, 255);
  Offset = A0 ? CCC_CR_MDLLCTL2_A0_REG : CCC_CR_MDLLCTL2_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccMdllCtl2.Data);

  CccMdllCtl3.Data = 0;
  CccMdllCtl3.Bits.mdll_cmn_ldopbiasctrl      = 3;
  CccMdllCtl3.Bits.mdll_cmn_ldonbiasvrefcode  = 0x0B;
  CccMdllCtl3.Bits.mdll_cmn_ldonbiasctrl      = 7;
  CccMdllCtl3.Bits.mdll_cmn_rloadcomp         = 0x0E;
  Offset = A0 ? CCC_CR_MDLLCTL3_A0_REG : CCC_CR_MDLLCTL3_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccMdllCtl3.Data);

  CccComp3.Data = 0;
  CccComp3.Bits.imod_comp_ccctx_cmn_txtcocompp = 0x1F;
  CccComp3.Bits.imod_comp_ccctx_cmn_txtcocompn = 0x1F;
  if (!A0) {
    CccComp3.Bits.DdrClkSlwDlyBypass = 1;
    CccComp3.Bits.DdrCtlSlwDlyBypass = 1;
    CccComp3.Bits.DdrCaSlwDlyBypass  = ScompCmdBypass;
  }
  Offset = A0 ? CCC_CR_COMP3_A0_REG : CCC_CR_COMP3_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccComp3.Data);

  //---------------------------------------------
  // static_DLLEARLY - DQM
  //---------------------------------------------
  DataMdllCtl2.Data = 0;
  DataMdllCtl2.Bits.mdll_cmn_dccen              = 1;
  DataMdllCtl2.Bits.mdll_cmn_dllldoen           = 1;
  DataMdllCtl2.Bits.mdll_cmn_ldoffreadsegen     = (Lpddr5 && Gear4) ? 1 : 3;
  DataMdllCtl2.Bits.mdll_cmn_vctlcompoffsetcal  = 0x10;
  DataMdllCtl2.Bits.mdll_cmn_vcdldcccode        = 8;
  DataMdllCtl2.Bits.mdll_cmn_ldoffcodelock      = CccMdllCtl2.Bits.mdll_cmn_ldoffcodelock;
  DataMdllCtl2.Bits.mdll_cmn_ldoffcodewl        = 0x20;
  DataMdllCtl2.Bits.mdll_cmn_ldofbdivsel        = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_MDLLCTL2_REG, DataMdllCtl2.Data);

  DataMdllCtl0.Data = 0;
  DataMdllCtl0.Bits.mdll_cmn_turbocaptrim   = CccMdllCtl0.Bits.mdll_cmn_turbocaptrim;
  DataMdllCtl0.Bits.mdll_cmn_turboonstartup = 1;
  DataMdllCtl0.Bits.mdll_cmn_ldoffcodepi    = 0x20;
  DataMdllCtl0.Bits.mdll_cmn_wlrdacholden   = 1;    // @todo_adl Should be set during weaklock enabling at the end of MRC ?
  DataMdllCtl0.Bits.mdll_cmn_dllbypassen    = 0;
  DataMdllCtl0.Bits.mdll_cmn_mdllengthsel   = CccMdllCtl0.Bits.mdll_cmn_mdllengthsel;   // G1: 0, G2: 1, G4: 2
  DataMdllCtl0.Bits.mdll_cmn_pien           = Gear4 ? 0x3F : 0x17;
  DataMdllCtl0.Bits.mdll_cmn_mdllen          = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_MDLLCTL0_REG, DataMdllCtl0.Data);

  DataMdllCtl1.Data = 0;
  DataMdllCtl1.Bits.mdll_cmn_picapsel       = 3;
  DataMdllCtl1.Bits.mdll_cmn_phsdrvprocsel  = 7;
  DataMdllCtl1.Bits.mdll_cmn_vctlcaptrim    = CccMdllCtl0.Bits.mdll_cmn_turbocaptrim;
  DataMdllCtl1.Bits.mdll_cmn_vcdlbwsel      = 0;      // @todo_adl comment says "not used in design", should we touch this ? Formula is not clear
  DataMdllCtl1.Bits.mdll_cmn_vctldaccode    = 0x187;  // @todo_adl Should we program this ?
  MrcWriteCrMulticast (MrcData, DATA_CR_MDLLCTL1_REG, DataMdllCtl1.Data);

  DataMdllCtl3.Data = 0;
  DataMdllCtl3.Bits.mdll_cmn_ldopbiasctrl         = 3;
  DataMdllCtl3.Bits.mdll_cmn_ldonbiasvrefcode     = 0x0B;
  DataMdllCtl3.Bits.mdll_cmn_ldonbiasctrl         = 7;
  DataMdllCtl3.Bits.mdll_cmn_rloadcomp            = 0x1F;
  DataMdllCtl3.Bits.mdll_cmn_ldoffcoderead        = 0x40;
  MrcWriteCrMulticast (MrcData, DATA_CR_MDLLCTL3_REG, DataMdllCtl3.Data);

  //---------------------------------------------
  // static_DQEARLY - DQM
  //---------------------------------------------
  Status |= MrcPhyInitStaticDqEarly (MrcData, FALSE);

  //---------------------------------------------
  // static_DIG_CCCEARLY - CMD
  //---------------------------------------------
  CccPiDivider.Data = 0;
  CccPiDivider.Bits.PiClkDuration     = 7;
  CccPiDivider.Bits.WckHalfPreamble   = Lpddr5 ? 1 : 0;
  CccPiDivider.Bits.PiSyncDivider     = Lpddr5 ? (Gear4 ? 1 : 2) : Gear1;
  CccPiDivider.Bits.Pi4IncPreamble    = (Gear2 ) ? 2 : (Gear4) ? 0 : 1;                                 // DDR4/5: 0, LP4/5: @todo_adl
  CccPiDivider.Bits.Pi4DivEnPreamble  = (Ddr4 || Ddr5 || (Lpddr5 && Gear4)) ? 0 : 1;  // DDR4/5: 0, LP4/5: @todo_adl
  DividerRatio = Gear1 ? 2 : 0;
  CccPiDivider.Bits.Pi4Inc            = Lpddr5 ? 0 : DividerRatio;
  CccPiDivider.Bits.Pi4DivEn          = Lpddr5 ? 0 : Gear1;
  CccPiDivider.Bits.Pi3Inc            = Lpddr5 ? 1 : DividerRatio;
  CccPiDivider.Bits.Pi3DivEn          = Lpddr5 ? 1 : Gear1;
  CccPiDivider.Bits.Pi2Inc            = Lpddr5 ? (Gear4 ? 0 : 1) : DividerRatio;
  CccPiDivider.Bits.Pi2DivEn          = Lpddr5 ? (Gear4 ? 0 : Gear2) : Gear1;
  CccPiDivider.Bits.Pi1Inc            = Lpddr5 ? (Gear4 ? 0 : 1) : DividerRatio;
  CccPiDivider.Bits.Pi1DivEn          = Lpddr5 ? (Gear4 ? 0 : Gear2) : Gear1;
  CccPiDivider.Bits.Pi0Inc            = Lpddr5 ? (Gear4 ? 0 : 2) : DividerRatio;
  CccPiDivider.Bits.Pi0DivEn          = Lpddr5 ? (Gear4 ? 0 : Gear2) : Gear1;
  MrcWriteCrMulticast (MrcData, CCC_CR_DDRCRCCCPIDIVIDER_REG, CccPiDivider.Data);

  //---------------------------------------------
  // static_DIG1 - VTT
  //---------------------------------------------
  Status |= MrcPhyInitVttStaticDig1 (MrcData);

  //---------------------------------------------
  // static_DIG1 - SCRAM
  //---------------------------------------------
  ScramDdrEarlyCr.Data = 0;
  ScramDdrEarlyCr.Bits.early_restoredone = 1;
  Offset = A0 ? DDRSCRAM_CR_DDREARLYCR_A0_REG : DDRSCRAM_CR_DDREARLYCR_REG;
  MrcWriteCR (MrcData, Offset, ScramDdrEarlyCr.Data);

  //---------------------------------------------
  // static_DIG1 - CCC_GLOBAL
  //---------------------------------------------
  CccGlobalPmTimer0.Data = 0;
  CccGlobalPmTimer0.Bits.scr_funcrst_timer_val        = 0x4;
  CccGlobalPmTimer0.Bits.scr_ldoen_timer_val          = 0x2;

  if (A0) {
    CccGlobalPmTimer0.Bits.scr_dll_en_timer_val         = 0xA0;
    CccGlobalPmTimer0.Bits.scr_clkalignrsten_timer_val  = 7;
  } else {
    CccGlobalPmTimer0.Bits.scr_dll_en_timer_val         = UDIVIDEROUND (50000, QclkPs); // 50ns in QCLK units
    Data32 = UDIVIDEROUND (2000, QclkPs);                                               // 2ns in QCLK units
    CccGlobalPmTimer0.Bits.scr_clkalignrsten_timer_val  = MIN (Data32, DATA0_GLOBAL_CR_PMTIMER0_scr_clkalignrsten_timer_val_MAX);
  }
  CccGlobalPmTimer0.Bits.scr_start_clkalign_timer_val = 0x2;
  CccGlobalPmTimer0.Bits.scr_iobufacten_timer_val     = 0x2;
  CccGlobalPmTimer0.Bits.scr_init_done_timer_val      = 0x2;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PMTIMER0_REG, CccGlobalPmTimer0.Data);

  CccGlobalPmTimer1.Data = 0;
  if (A0) {
    CccGlobalPmTimer1.Bits.scr_pien_timer_val             = 0x3C0;
  } else {
    CccGlobalPmTimer1.Bits.scr_pien_timer_val             = UDIVIDEROUND (300000, QclkPs);  // 300ns in QCLK units
  }

  CccGlobalPmTimer1.Bits.scr_iobuf_dis_timer_val          = 0x2;
  CccGlobalPmTimer1.Bits.scr_funcrst_dis_timer_val        = 0x2;
  CccGlobalPmTimer1.Bits.scr_start_clkalign_dis_timer_val = 0x1;
  CccGlobalPmTimer1.Bits.scr_clkalignrst_dis_timer_val    = 0x1;
  CccGlobalPmTimer1.Bits.scr_pi_dis_timer_val             = 0x1;
  CccGlobalPmTimer1.Bits.scr_dll_dis_timer_val            = 0x1;
  CccGlobalPmTimer1.Bits.scr_ldo_dis_timer_val            = 0x1;
  CccGlobalPmTimer1.Bits.scr_initdone_dis_timer_val       = 0x1;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PMTIMER1_REG, CccGlobalPmTimer1.Data);

  CccGlobalIoLvrCtl.Data = 0;
  CccGlobalIoLvrCtl.Bits.iolvrseg_cmn_codeffleg   = 0x0F;
  CccGlobalIoLvrCtl.Bits.iolvrseg_cmn_codelvrleg  = 0x0F;
  CccGlobalIoLvrCtl.Bits.iolvrseg_cmn_enlvrleg    = 1;
  CccGlobalIoLvrCtl.Bits.iolvrseg_cmn_enffleg     = 1;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_IOLVRCTL_REG, CccGlobalIoLvrCtl.Data);

  CccGlobalDdrEarlyCr.Data = 0;
  CccGlobalDdrEarlyCr.Bits.early_restoredone = 1;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_DDREARLYCR_REG, CccGlobalDdrEarlyCr.Data);

  //---------------------------------------------
  // static_DIG1 - DATA_GLOBAL
  //---------------------------------------------
  DataGlobalPmTimer0.Data = 0;
  DataGlobalPmTimer0.Bits.scr_funcrst_timer_val         = 0x6;
  DataGlobalPmTimer0.Bits.scr_ldoen_timer_val           = 0x2;
  DataGlobalPmTimer0.Bits.scr_dll_en_timer_val          = CccGlobalPmTimer0.Bits.scr_dll_en_timer_val;
  DataGlobalPmTimer0.Bits.scr_clkalignrsten_timer_val   = CccGlobalPmTimer0.Bits.scr_clkalignrsten_timer_val;
  DataGlobalPmTimer0.Bits.scr_start_clkalign_timer_val  = 0x2;
  DataGlobalPmTimer0.Bits.scr_iobufacten_timer_val      = 0x2;
  DataGlobalPmTimer0.Bits.scr_init_done_timer_val       = 0x2;
  if (UlxUlt) {
    DataGlobalPmTimer0.Bits.spare                       = 0x1;
  }
  MrcWriteCrMulticast (MrcData, DATA_GLOBAL_CR_PMTIMER0_REG, DataGlobalPmTimer0.Data);

  DataGlobalPmTimer1.Data = 0;
  DataGlobalPmTimer1.Bits.scr_pien_timer_val                = CccGlobalPmTimer1.Bits.scr_pien_timer_val;
  DataGlobalPmTimer1.Bits.scr_iobuf_dis_timer_val           = 0x2;
  DataGlobalPmTimer1.Bits.scr_funcrst_dis_timer_val         = 0x2;
  DataGlobalPmTimer1.Bits.scr_start_clkalign_dis_timer_val  = 0x1;
  DataGlobalPmTimer1.Bits.scr_clkalignrst_dis_timer_val     = 0x1;
  DataGlobalPmTimer1.Bits.scr_pi_dis_timer_val              = 0x1;
  DataGlobalPmTimer1.Bits.scr_dll_dis_timer_val             = 0x1;
  DataGlobalPmTimer1.Bits.scr_ldo_dis_timer_val             = 0x1;
  DataGlobalPmTimer1.Bits.scr_initdone_dis_timer_val        = 0x1;
  MrcWriteCrMulticast (MrcData, DATA_GLOBAL_CR_PMTIMER1_REG, DataGlobalPmTimer1.Data);

  DataGlobalIoLvrCtl0.Data = 0;
  DataGlobalIoLvrCtl0.Bits.iolvrseg_cmn_enffleg     = 1;
  DataGlobalIoLvrCtl0.Bits.iolvrseg_cmn_enlvrleg    = 1;
  DataGlobalIoLvrCtl0.Bits.iolvrseg_cmn_codeffleg   = 0x0F;
  DataGlobalIoLvrCtl0.Bits.iolvrseg_cmn_codelvrleg  = 0x0F;
  MrcWriteCrMulticast (MrcData, DATA_GLOBAL_CR_IOLVRCTL0_REG, DataGlobalIoLvrCtl0.Data);

  DataGlobalDdrEarlyCr.Data = 0;
  DataGlobalDdrEarlyCr.Bits.early_restoredone = 1;
  MrcWriteCrMulticast (MrcData, DATA_GLOBAL_CR_DDREARLYCR_REG, DataGlobalDdrEarlyCr.Data);

  //---------------------------------------------
  // static_CLKALIGNCTLEARLY - DQ
  //---------------------------------------------
  DataClkAlignCtl0.Data = 0;
  DataClkAlignCtl0.Bits.clkalignrst_b = 0x1;
  DataClkAlignCtl0.Bits.clkalignd0piclkpistep_maxreloopcnt = 0x2;
  DataClkAlignCtl0.Bits.dllwait_dly   = 0xC;
  DataClkAlignCtl0.Bits.pimacrosteps  = 0xF;
  DataClkAlignCtl0.Bits.pimicrosteps  = 0x1;
  DataClkAlignCtl0.Bits.samplesize    = 0x10;
  DataClkAlignCtl0.Bits.maxsamplecnt  = 0xA;
  MrcWriteCrMulticast (MrcData, DATA_CR_CLKALIGNCTL0_REG, DataClkAlignCtl0.Data);

  DataClkAlignCtl2.Data = 0;
  DataClkAlignCtl2.Bits.d0tosig_offset = A0 ? 0xC0 : 0xCA;
  if (((Ddr4) && ((Gear1) && (Outputs->Frequency >= f2667))) && (!A0)) {
    DataClkAlignCtl2.Bits.d0tosig_offset += 10;
  }
  MrcWriteCrMulticast (MrcData, DATA_CR_CLKALIGNCTL2_REG, DataClkAlignCtl2.Data);

  //---------------------------------------------
  // static_CLKALIGNCTLEARLY - CMD
  //---------------------------------------------
  CccClkAlignCtl0.Data = 0;
  CccClkAlignCtl0.Bits.clkalignrst_b = 0x1;
  CccClkAlignCtl0.Bits.clkalignd0piclkpistep_maxreloopcnt = 0x2;
  CccClkAlignCtl0.Bits.dllwait_dly   = 0xC;
  CccClkAlignCtl0.Bits.pimacrosteps  = 0xF;
  CccClkAlignCtl0.Bits.pimicrosteps  = 0x1;
  CccClkAlignCtl0.Bits.samplesize    = 0x10;
  CccClkAlignCtl0.Bits.maxsamplecnt  = 0xA;
  MrcWriteCrMulticast (MrcData, CCC_CR_CLKALIGNCTL0_REG, CccClkAlignCtl0.Data);

  CccClkAlignCtl2.Data = 0;
  CccClkAlignCtl2.Bits.d0tosig_offset = A0 ? 0xC0 : 0xE5;
  if (((Ddr4) && ((Gear1) && (Outputs->Frequency >= f2667))) && (!A0)) {
    CccClkAlignCtl2.Bits.d0tosig_offset += 10;
  }
  MrcWriteCrMulticast (MrcData, CCC_CR_CLKALIGNCTL2_REG, CccClkAlignCtl2.Data);

  //---------------------------------------------
  // Individual registers
  //---------------------------------------------
  CccClkAlignCtl1.Data = 0;
  CccClkAlignCtl1.Bits.clkalignd0piclkpicodeoffset = A0 ? 192 : 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_CLKALIGNCTL1_REG, CccClkAlignCtl1.Data);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC:  clkalignd0piclkpicodeoffset: %u\n", CccClkAlignCtl1.Bits.clkalignd0piclkpicodeoffset);

  DataClkAlignCtl1.Data = 0;
  DataClkAlignCtl1.Bits.clkalignd0piclkpicodeoffset = A0 ? 192 : 0;
  MrcWriteCrMulticast (MrcData, DATA_CR_CLKALIGNCTL1_REG, DataClkAlignCtl1.Data);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DATA: clkalignd0piclkpicodeoffset: %u\n", DataClkAlignCtl1.Bits.clkalignd0piclkpicodeoffset);

  if (!A0) {
    DccCtl14.Data = 0;
    DccCtl14.Bits.maxdccsteps = 0x7F;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL14_REG, DccCtl14.Data);

    CccDccCtl9.Data  = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL9_REG);
    CccDccCtl9.Bits.maxdccsteps = 0x7F;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL9_REG, CccDccCtl9.Data);

    CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
    CompDccCtl4.Bits.maxdccsteps = 0x2F;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);
  }

  if (UlxUlt) {
    CompUpdateCtrl.Data = MrcReadCR (MrcData, MCMISCS_COMPUPDATE_CTRL_REG);
    CompUpdateCtrl.Bits.compupdatecntval = 0x40;
    MrcWriteCR (MrcData, MCMISCS_COMPUPDATE_CTRL_REG, CompUpdateCtrl.Data);
  }

  //---------------------------------------------
  // static_CCCEARLY_AFE - CMD and CCC_GLOBAL
  //---------------------------------------------
  Status |= MrcPhyInitStaticCccEarlyAfe (MrcData, ClkPiValue, CtlPiValue);

  //---------------------------------------------
  // static_DQ_AFE_EARLY - DQ
  //---------------------------------------------
  Status |= MrcPhyInitDqStaticDqAfeEarly (MrcData);

  //---------------------------------------------
  // static_DIG2 - VCC
  //---------------------------------------------
  VccDllAfeCtrl1New.Data = 0;
  VccDllAfeCtrl1New.Bits.iolvrtop_cmn_lvroffsetcal = (A0 || UlxUlt) ? 0x10 : 0;
  VccDllAfeCtrl1New.Bits.iolvrtop_cmn_codebiasleg  = 0xFF;
  VccDllAfeCtrl1New.Bits.iolvrtop_cmn_codeffleg    = 0x0F;
  VccDllAfeCtrl1New.Bits.iolvrtop_cmn_codelvrleg   = 0x0F;
  MrcWriteCrMulticast (MrcData, DDRVCCDLL_CR_AFE_CTRL1_NEW_REG, VccDllAfeCtrl1New.Data);

  VccDllAfeCtrl2.Data = 0;
  VccDllAfeCtrl2.Bits.iolvrtop_cmn_coderes1       = 8;
  VccDllAfeCtrl2.Bits.iolvrtop_cmn_coderes2       = 8;
  VccDllAfeCtrl2.Bits.iolvrtop_cmn_enffleg        = 1;
  VccDllAfeCtrl2.Bits.iolvrtop_cmn_enlvrleg       = 1;
  VccDllAfeCtrl2.Bits.iolvrtop_cmn_lvrtargetcode  = 0x15;
  MrcWriteCrMulticast (MrcData, DDRVCCDLL_CR_AFE_CTRL2_REG, VccDllAfeCtrl2.Data);

  //---------------------------------------------
  // compupdone_ovrrideen_duringprecomp
  //---------------------------------------------
  Status |= MrcPhyInitCompUpdateOverride (MrcData, 1);

  //---------------------------------------------
  // Local VssHi Leaker Calibration
  //---------------------------------------------
  //Status |= MrcVssHiLocalLeakCal (MrcData);

  //---------------------------------------------
  // VssHi Offset Cancellation
  //---------------------------------------------
  Status |= MrcVsshiOffsetCancellation (MrcData);

  //---------------------------------------------
  // VssHiFFComp Leaker Ring Oscillator calibration
  //---------------------------------------------
  Status |= MrcVsshiCompLeakerRoCalibration (MrcData);

  //---------------------------------------------
  // PBD step size calibration
  //---------------------------------------------
  Status |= MrcPbdStepSizeCalibration (MrcData);

  //---------------------------------------------
  // Rload Calibration
  //---------------------------------------------
  Status |= MrcRloadCalibration (MrcData);

  //---------------------------------------------
  // LDO BW Sel Calibration
  //---------------------------------------------
  Status |= MrcBwSelCalibration (MrcData, FALSE);
  if (Status != mrcSuccess) {
    return Status;
  }

  //---------------------------------------------
  // RefClk DCC init in COMP Partition
  //---------------------------------------------
  Status |= MrcInitDcc (MrcData, DCD_GROUP_REFCLK, DccFubComp, DccFubComp);

  //---------------------------------------------
  // VCDL DCC init in COMP Partition
  //---------------------------------------------
  Status |= MrcInitDcc (MrcData, DCD_GROUP_VCDL, DccFubComp, DccFubNone);

  //---------------------------------------------
  // LDO BW Sel 2 Calibration
  //---------------------------------------------
  Status |= MrcBwSelCalibration (MrcData, TRUE);
  if (Status != mrcSuccess) {
    return Status;
  }

  //---------------------------------------------
  // RefClk DCC init in all partitions (COMP, DATA, CCC)
  //---------------------------------------------
  Status |= MrcInitDcc (MrcData, DCD_GROUP_REFCLK, DccFubAll, DccFubDataCcc);

  //---------------------------------------------
  // VCDL DCC init in all partitions (COMP, DATA, CCC)
  //---------------------------------------------
  Status |= MrcInitDcc (MrcData, DCD_GROUP_VCDL, DccFubAll, DccFubNone);

  //---------------------------------------------
  // static_FLL - FLL
  //---------------------------------------------
  Status |= MrcPhyInitStaticFll (MrcData);

  Status |= MrcPhyInitFllInitDcc (MrcData);

  //---------------------------------------------
  // LDO FFCode Lock Calibration
  //---------------------------------------------
  Status |= MrcLdoFFCodeLockCalibration (MrcData);

  //---------------------------------------------
  // LDO FFCode Calibration
  //---------------------------------------------
  if (!A0) {
    Status |= MrcLdoFFCodeCalibration (MrcData);
  }

  //---------------------------------------------
  // Force Clock Align Calibration
  //---------------------------------------------
  Status |= MrcForceClkAlignCalibration (MrcData);

  //---------------------------------------------
  // Serializer DCC init in DATA and CCC partitions
  //---------------------------------------------
  if (!Gear4) {
    Status |= MrcInitDcc (MrcData, DCD_GROUP_SRZ, DccFubDataCcc, DccFubNone);
  }

  //---------------------------------------------
  // Periodic DCC init: ddrphy_neidengard_init_dcc
  //---------------------------------------------
  if (!A0 && Inputs->PeriodicDcc) {
    Status |= MrcPhyInitPeriodicDcc (MrcData);
  }


  //---------------------------------------------
  // Lock UI Calibration
  //---------------------------------------------
  if ((!Matched) && (MrcData->Inputs.SimicsFlag == 0)) {
    Status2 = MrcLockUiCalibration (MrcData);
    if (Status2 == mrcUnmatchedRxNotPossible) {
      return Status2;
    }

    if (Ddr5 || Lpddr5) {
      // DDR5/LPDDR5 Read Preamble length depends on UiLock, hence need to recalculate the TAT values after Lock UI calibration
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
            continue;
          }
          SetTcTurnAround (MrcData, Controller, Channel);
        }
      }
    }
  }
  UiLock        = Matched ? 0 : SaveOutputs->UiLock;
  ReadDqsOffset = SaveOutputs->ReadDqsOffsetDdr5;

  //---------------------------------------------
  // compupdone_ovrridedisable_duringprecomp
  //---------------------------------------------
  Status |= MrcPhyInitCompUpdateOverride (MrcData, 0);

  //---------------------------------------------
  // static_compinit_mrc - COMP
  // This sequence is repeated here for the second time, so register values are already populated
  //---------------------------------------------
  CompOverride.Bits.compclkon     = 0;
  CompOverride.Bits.dllcompclkon  = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG, CompOverride.Data);
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrl.Data);

  //---------------------------------------------
  // static_postprecomp - DQ
  //---------------------------------------------
  VccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);

  DataSdlCtl1.Data = 0;
  DataSdlCtl1.Bits.rxsdl_cmn_srzclkend0           = 1;
  DataSdlCtl1.Bits.rxsdl_cmn_rxsdlbwsel           = VccDllCompDataCcc.Bits.Dll_bwsel;    // Should match primary DLL - DDRCOMP_CR_VCCDLLCOMPDATACCC.Dll_bwsel (see also dqrx_cmn_rxmdqbwsel)
  DataSdlCtl1.Bits.rxsdl_cmn_burstlen             = Ddr4 ? 1 : 3;
  DataSdlCtl1.Bits.rxsdl_cmn_passrcvenondqsfall   = Matched ? 0 : (((UiLock - SaveOutputs->CompOffset) + 1 - ReadDqsOffset * 2 == 1) ? 0 : 1); // <-- depends on UiLock calibration results
  DataSdlCtl1.Bits.rxsdl_cmn_qualifysdlwithrcven  = !(DataSdlCtl1.Bits.rxsdl_cmn_passrcvenondqsfall);              // <-- depends on UiLock calibration results
  DataSdlCtl1.Bits.rxsdl_cmn_rxphsdrvprocsel      = 7;
  DataSdlCtl1.Bits.rxsdl_cmn_rxpicapsel           = 3;
  DataSdlCtl1.Bits.rxsdl_cmn_rxpienable           = 1;
  DataSdlCtl1.Bits.rxsdl_cmn_rxsdllengthsel       = 3;
  MrcWriteCrMulticast (MrcData, DATA_CR_SDLCTL1_REG, DataSdlCtl1.Data);

  AfeMiscCtrl1.Data = 0;
  AfeMiscCtrl1.Bits.dxtx_cmn_tcoslewstatlegen     = 1;
//AfeMiscCtrl1.Bits.dqrx_cmn_dlydfenumuip         = 0;
  AfeMiscCtrl1.Bits.dqrx_cmn_dlydfenumuin         = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_AFEMISCCTRL1_REG, AfeMiscCtrl1.Data);

  return Status;
}

/**
  PHY init - sequence static_CCCEARLY_AFE in CMD and CCC_GLOBAL FUBs.

  @param[in, out] MrcData     - Include all MRC global data.
  @param[out]     ClkPiValue  - pointer to CLK PI value, filled by the function
  @param[out]     CtlPiValue  - pointer to CTL PI value, filled by the function

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitStaticCccEarlyAfe (
  IN OUT MrcParameters *const MrcData,
  OUT    UINT32               *ClkPiValue,
  OUT    UINT32               *CtlPiValue
  )
{
  MrcStatus           Status;
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  MrcIntOutput        *IntOutputs;
  MrcSaveData         *SaveOutputs;
  MrcDebug            *Debug;
  MrcDdrType          DdrType;
  MrcFrequency        DdrFrequency;
  MrcIntCmdTimingOut  *IntCmdTiming;
  BOOLEAN             A0;
  BOOLEAN             Lpddr4;
  BOOLEAN             Lpddr5;
  BOOLEAN             Lpddr;
  BOOLEAN             Ddr4;
  BOOLEAN             Ddr5;
  BOOLEAN             Gear1;
  BOOLEAN             Gear2;
  BOOLEAN             Gear4;
  BOOLEAN             EnNmosPup;
  UINT32              ClkPi;
  UINT32              CtlPi;
  UINT32              CmdPi;
  UINT32              WckPi;
  UINT32              SlewRateBypass;
  UINT32              Index;
  UINT32              Controller;
  UINT32              Channel;
  UINT32              Rank;
  UINT32              MaxChannels;
  UINT32              GrpCount;
  UINT32              Offset;
  UINT32              SafeCmdPi;
  UINT32              SafeCtlPi;
  INT64               GetSetVal;
  INT64               GetSetValCtl;
  INT64               GetSetValClk;
  BOOLEAN             DtHalo;
  CH0CCC_CR_COMP2_STRUCT                    CccComp2;
  CH0CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT      CccVoltageUsed;
  CH0CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT     CtlCaCompOffset;
  CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT  VsshiClkCompOffset;
  CH0CCC_CR_CTL0_STRUCT                     CccCtl0;
  CH0CCC_CR_CTL2_STRUCT                     CccCtl2;
  CH0CCC_CR_CTL3_STRUCT                     CccCtl3;
  CH0CCC_CR_TXPBDCODE0_STRUCT               TxPbdCode0;
  CH0CCC_CR_TXPBDCODE1_STRUCT               TxPbdCode1;
  CH0CCC_CR_TXPBDCODE2_STRUCT               TxPbdCode2;
  CH0CCC_CR_TXPBDCODE3_STRUCT               TxPbdCode3;
  CH0CCC_CR_TXPBDCODE4_STRUCT               TxPbdCode4;

  Status      = mrcSuccess;
  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  SaveOutputs = &MrcData->Save.Data;
  IntOutputs  = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  Debug       = &Outputs->Debug;
  DdrType     = Outputs->DdrType;
  Lpddr4      = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4        = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5        = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr       = Outputs->Lpddr;
  Gear2       = Outputs->Gear2;
  Gear4       = Outputs->Gear4;
  Gear1       = !Gear2 && !Gear4;
  A0          = Inputs->A0;
  MaxChannels = Outputs->MaxChannels;
  DdrFrequency = Outputs->Frequency;
  EnNmosPup   = SaveOutputs->EnNmosPup;
  DtHalo      = Inputs->DtHalo;

  // Previous DdrIoInit steps were using CR multicast. Cache may have some other values in it causing register corruption.
  // Invalidate cache here to cause GetSet to do RMW.
  InitializeRegisterCache (MrcData);

  //---------------------------------------------
  // static_CCC_AFE - CMD
  //---------------------------------------------
  CmdPi = DtHalo ? 64 : 0;    // @todo_adl Align UlxUlt DDR4/5 to 64 as well
  if (Lpddr4) {
    CmdPi = Gear4 ? 256 : (Gear2 ? 128 : 32);
  }
  if (Lpddr5) {
    CmdPi = 128;
  }
  CtlPi = CmdPi;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DdrFrequency : %d\n", DdrFrequency);

  if (Ddr4) {
    ClkPi = 64 + (DtHalo ? CtlPi : 0);
  } else if (Ddr5) {
    ClkPi = Gear4 ? 128 : 64 + (DtHalo ? CtlPi : 0);
  } else if (Lpddr4) {
    ClkPi = CtlPi + (Gear4 ? 128 : 64) ;
  } else { // LPDDR5
    ClkPi = 256;
  }

  WckPi = 0;
  if (Lpddr5) {
    WckPi = 128;
  }

  *ClkPiValue = ClkPi;
  *CtlPiValue = CtlPi;
  if (Lpddr5) {
    SafeCmdPi   = MRC_SAFE_LP5_CMD_PI;
    SafeCtlPi   = MRC_SAFE_LP5_CTL_PI;
  } else {
    SafeCmdPi   = ((Inputs->LpFreqSwitch == TRUE) || Gear2) ? MRC_SAFE_LP5_CMD_PI : CmdPi;
    SafeCtlPi   = ((Inputs->LpFreqSwitch == TRUE) || Gear2) ? MRC_SAFE_LP5_CTL_PI : CtlPi;
  }


  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      IntCmdTiming = &IntOutputs->Controller[Controller].CmdTiming[Channel];
      GetSetVal = CmdPi;
      if (!Outputs->LowFreqCsCmd2DSweepDone) {
        SaveOutputs->LowFCmdPiCode[Controller][Channel] = SafeCmdPi;
      }
      GrpCount = (Ddr4) ? MRC_DDR4_CMD_GRP_MAX : ((Ddr5) ? MRC_DDR5_CMD_GRP_MAX : 1); // Number of cmd PI groups: DDR4: 4, DDR5: 2, LP4/5: 1
      for (Index = 0; Index < GrpCount; Index++) {
        MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, Index, CmdGrpPi, WriteToCache | PrintValue, &GetSetVal);
        IntCmdTiming->CmdPiCode[Index] = (UINT16) GetSetVal;
      }
      GetSetValClk = ClkPi;
      GetSetValCtl = CtlPi;
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!Outputs->LowFreqCsCmd2DSweepDone) {
          SaveOutputs->LowFCtlPiCode[Controller][Channel][Rank] = SafeCtlPi;
        }
        MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, CtlGrpPi, WriteToCache | PrintValue, &GetSetValCtl);
        IntCmdTiming->CtlPiCode[Rank] = (UINT16) GetSetValCtl;
        if (!Lpddr) {
          MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, ClkGrpPi, WriteToCache | PrintValue, &GetSetValClk); // Side effect will update PH90 for Gear4
          IntCmdTiming->ClkPiCode[Rank] = (UINT16) GetSetValClk;
          if (Ddr4) {  // No CKE on DDR5
            MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, CkeGrpPi, WriteToCache | PrintValue, &GetSetValCtl);
            IntCmdTiming->CkePiCode[Rank] = (UINT16) GetSetValCtl;
          }
        }
      }
      // Clk/Cke/Wck are per-channel for LPDDR (not per rank)
      if (Lpddr) {
        MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, ClkGrpPi, WriteToCache | PrintValue, &GetSetValClk); // Side effect will update PH90 for Gear4
        IntCmdTiming->ClkPiCode[0] = (UINT16) GetSetValClk;
      }
      if (Lpddr4) {
        MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, CkeGrpPi, WriteToCache | PrintValue, &GetSetValCtl);
        IntCmdTiming->CkePiCode[0] = (UINT16) GetSetValCtl;
      }
      if (Lpddr5) {
        GetSetVal = WckPi;
        MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, WckGrpPi, WriteToCache | PrintValue, &GetSetVal);
        IntCmdTiming->WckPiCode = (UINT16) GetSetVal;
      }
    } // Channel
  } // Controller
  MrcFlushRegisterCachedData (MrcData);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC PI programming done\n");

  SlewRateBypass = MRC_ALL_CCC_BUF_MSK;

  if (A0 && !Lpddr) {
    for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
      DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
      if (Outputs->Controller[Controller].Channel[Channel].DimmCount == 2) { // DDR4/5 2DPC - keep SR bypass disabled on CA pins
        switch (Index) {
          case 0:
          case 2:
            SlewRateBypass = Ddr4 ? MRC_HS_DDR4_CCC02_CA_MSK : MRC_HS_DDR5_CCC02_CA_MSK;
            break;

          case 1:
          case 3:
            SlewRateBypass = Ddr4 ? MRC_HS_DDR4_CCC13_CA_MSK : MRC_HS_DDR5_CCC13_CA_MSK;
            break;

          case 4:
          case 6:
            SlewRateBypass = Ddr4 ? MRC_HS_DDR4_CCC46_CA_MSK : MRC_HS_DDR5_CCC46_CA_MSK;
            break;

          case 5:
          case 7:
          default:
            SlewRateBypass = Ddr4 ? MRC_HS_DDR4_CCC57_CA_MSK : MRC_HS_DDR5_CCC57_CA_MSK;
            break;
        }
        SlewRateBypass = (~SlewRateBypass) & MRC_ALL_CCC_BUF_MSK;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC%d: MC%d C%d ccctx_cmn_txslewdlybypass: 0x%04X\n", Index, Controller, Channel, SlewRateBypass);
      } // 2DPC
    } // CCC index
  } // DDR4/5

  CccComp2.Data = 0;
  if (A0) {
    CccComp2.BitsA0.ccctx_cmn_txslewdlybypass = SlewRateBypass;
    CccComp2.BitsA0.afetx_cmn_txlocalvsshibyp = SaveOutputs->LocalVsshiBypass;
    CccComp2.BitsA0.ccctx_cmn_ennmospup       = EnNmosPup;
    CccComp2.BitsA0.ccctx_cmn_txslewpreupdt   = 1;
  } else {
    CccComp2.Bits.afetx_cmn_txlocalvsshibyp = SaveOutputs->LocalVsshiBypass;
    CccComp2.Bits.ccctx_cmn_ennmospup       = EnNmosPup;
    CccComp2.Bits.ccctx_cmn_txslewpreupdt   = 1;
  }
  CccComp2.Bits.ccctx_cmn_extupdateenclk  = 0;
  CccComp2.Bits.ccctx_cmn_extupdateen     = 1;
  CccComp2.Bits.cccrx_cmn_rxmtailctl      = 1;
  Offset = A0 ? CCC_CR_COMP2_A0_REG : CCC_CR_COMP2_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccComp2.Data);

  CccVoltageUsed.Data = 0;
  CccVoltageUsed.Bits.CsUseCaComp               = Lpddr4;
  CccVoltageUsed.Bits.CkUseCtlComp              = SaveOutputs->GlobalVsshiBypass;
  CccVoltageUsed.Bits.ccctx_cmn_txpupusevcciog  = EnNmosPup;
//CccVoltageUsed.Bits.VssHiBypassVddqMode       = 0;                  // Not used
  CccVoltageUsed.Bits.ccctx_cmn_ennbiasboost    = SaveOutputs->EnNBiasBoost;
//CccVoltageUsed.Bits.rxbias_cmn_biasen         = 0;                  // The actual biasen is controlled by DFx logic. This one is an override enable.
//CccVoltageUsed.Bits.ClkPDPreDrvVccddq         = 0;                  // Not used
//CccVoltageUsed.Bits.CtlPDPreDrvVccddq         = 0;                  // Not used
//CccVoltageUsed.Bits.CaPDPreDrvVccddq          = 0;                  // Not used
  CccVoltageUsed.Bits.CaVoltageSelect           = 1;
  CccVoltageUsed.Bits.ClkVoltageSelect          = 1;
  CccVoltageUsed.Bits.CtlVoltageSelect          = !Lpddr;
  CccVoltageUsed.Bits.ccctx_cmn_txpdnusevcciog  = SaveOutputs->TxPdnUseVcciog;
  Offset = A0 ? CCC_CR_DDRCRCCCVOLTAGEUSED_A0_REG : CCC_CR_DDRCRCCCVOLTAGEUSED_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccVoltageUsed.Data);

  CtlCaCompOffset.Data = 0;
  Offset = A0 ? CCC_CR_DDRCRCTLCACOMPOFFSET_A0_REG : CCC_CR_DDRCRCTLCACOMPOFFSET_REG;
  MrcWriteCrMulticast (MrcData, Offset, CtlCaCompOffset.Data);

  VsshiClkCompOffset.Data = 0;
  Offset = A0 ? CCC_CR_DDRCRVSSHICLKCOMPOFFSET_A0_REG : CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG;
  MrcWriteCrMulticast (MrcData, Offset, VsshiClkCompOffset.Data);

  CccCtl0.Data = 0;
  CccCtl0.Bits.ccctx_cmn_txvsshileakercomp  = SaveOutputs->GlobalVsshiBypass ? 0x3F : 0x20;
  CccCtl0.Bits.ccctx_cmn_txvsshileakeren    = 1;
  CccCtl0.Bits.pghub_cmn_iobufactclk        = 1;
  CccCtl0.Bits.pghub_cmn_iobufact           = 1;
  CccCtl0.Bits.alltx_cmn_tcoslewstatlegen   = 1;
  CccCtl0.Bits.ccctx_rgrp4_gear4enclkdata   = (Lpddr5 || Ddr5) && Gear4;     // 1 if CCCBuf7 and 8 are CK or WCK (in the case of LP5) modes and in G4, else 0
  CccCtl0.Bits.ccctx_rgrp3_gear4enclkdata   = (Lpddr4 || Ddr5) && Gear4;     // 1 if CCCBuf5 and 6 are CK or WCK (in the case of LP5) modes and in G4, else 0
  CccCtl0.Bits.ccctx_ccc8_iamclkp           = 0;
  CccCtl0.Bits.ccctx_ccc7_iamclkp           = 1;
  CccCtl0.Bits.ccctx_ccc6_iamclkp           = 0;
  CccCtl0.Bits.ccctx_ccc5_iamclkp           = 1;
  if (!A0) {
    CccCtl0.Bits.safemodeenable             = 1;
  }
  MrcWriteCrMulticast (MrcData, CCC_CR_CTL0_REG, CccCtl0.Data);

  CccCtl2.Data = 0;
  CccCtl2.Bits.ccctx_cmn_txsegen      = 3;
  CccCtl2.Bits.ccctx_cmn_txeqenpreset = 1;
  CccCtl2.Bits.ccctx_cmn_txeqenntap   = 0;  // @todo_adl CMD TxEq, might be needed for high speed 2DPC
  if (!Inputs->Q0Regs) {
    CccCtl2.Bits.ccctx_cmn_txeqconstz   = 0;
  }
  CccCtl2.Bits.cccrx_cmn_rxmvcmres    = 1;
  CccCtl2.Bits.pghub_cmn_ckes3segsel  = 1;
  MrcWriteCrMulticast (MrcData, CCC_CR_CTL2_REG, CccCtl2.Data);

  CccCtl3.Data = 0;
  CccCtl3.Bits.dcdsrz_cmn_gear4en = Gear4;
  CccCtl3.Bits.dcdsrz_cmn_gear2en = Gear2;
  CccCtl3.Bits.dcdsrz_cmn_gear1en = Gear1;
  CccCtl3.Bits.cktop_cmn_bonus    = 0x78;
  MrcWriteCrMulticast (MrcData, CCC_CR_CTL3_REG, CccCtl3.Data);

  TxPbdCode0.Data = 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_TXPBDCODE0_REG, TxPbdCode0.Data);

  TxPbdCode1.Data = 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_TXPBDCODE1_REG, TxPbdCode1.Data);

  TxPbdCode2.Data = 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_TXPBDCODE2_REG, TxPbdCode2.Data);

  TxPbdCode3.Data = 0;
  TxPbdCode3.Bits.mdll_cmn_dccrangesel    = 3;
  TxPbdCode3.Bits.txpbd_cmn_txdccrangesel = 3;
  MrcWriteCrMulticast (MrcData, CCC_CR_TXPBDCODE3_REG, TxPbdCode3.Data);

  TxPbdCode4.Data = 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_TXPBDCODE4_REG, TxPbdCode4.Data);

  return Status;
}

/**
  PHY init - sequence static_CCC_AFE in CMD and CCC_GLOBAL FUBs.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitStaticCccAfe (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcStatus           Status;
  MrcOutput           *Outputs;
  CH0CCC_CR_SRZBYPASSCTL_STRUCT         SrzBypassCtl;
  CH0CCC_CR_SRZDRVENBYPASSCTL_STRUCT    SrzDrvEnBypassCtl;
  CCC0_GLOBAL_CR_VIEWCTL_STRUCT         CccGlobalViewCtl;
  CCC0_GLOBAL_CR_CCCDRXVREF_STRUCT      CccGlobalCccDrxVref;

  Status      = mrcSuccess;
  Outputs     = &MrcData->Outputs;

  //---------------------------------------------
  // static_CCC_AFE - CMD
  //---------------------------------------------

  SrzBypassCtl.Data = 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_SRZBYPASSCTL_REG, SrzBypassCtl.Data);

  SrzDrvEnBypassCtl.Data = 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_SRZDRVENBYPASSCTL_REG, SrzDrvEnBypassCtl.Data);

  //---------------------------------------------
  // static_CCC_AFE - CCC_GLOBAL
  //---------------------------------------------

  CccGlobalViewCtl.Data = 0;
  CccGlobalViewCtl.Bits.rxbias_cmn_vrefsel_top = 1;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_VIEWCTL_REG, CccGlobalViewCtl.Data);

  CccGlobalCccDrxVref.Data = 0;
  CccGlobalCccDrxVref.Bits.rxbias_cmn_tailctl_top       = 1;
  CccGlobalCccDrxVref.Bits.rxbias_cmn_biasicomp_top     = 8;
  CccGlobalCccDrxVref.Bits.rxall_cmn_rxlpddrmode_top    = Outputs->RxMode; // Encoding matches this CR: 0-LPDDR4x (matched P type) , 1-DDR4 (matched N type), 2-LPDDR5 (unmatched P type), 3-DDR5 (unmatched N type)
  CccGlobalCccDrxVref.Bits.rxvref_cmn_ddrvrefctrl_top0  = 0x100;
  CccGlobalCccDrxVref.Bits.rxvref_cmn_ddrvrefctrl_top1  = 0x100;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_CCCDRXVREF_REG, CccGlobalCccDrxVref.Data);

  return Status;
}

/**
  PHY init - sequence static_DIG2 in DQ FUB.
  Also static_DIG2 in SCRAM FUB.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitDqStaticDig2 (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               ClkPi,
  IN     UINT32               CtlPi
  )
{
  MrcStatus         Status;
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MrcSaveData       *SaveOutputs;
  MrcChannelOut     *ChannelOut;
  MrcControllerOut  *ControllerOut;
  MrcDebug          *Debug;
  MrcDdrType        DdrType;
  MrcFrequency      DdrFrequency;
  MrcTiming         *TimingProfile;
  MrcProfile        Profile;
  BOOLEAN           Lpddr4;
  BOOLEAN           Lpddr5;
  BOOLEAN           Lpddr;
  BOOLEAN           Ddr4;
  BOOLEAN           Ddr5;
  BOOLEAN           Gear1;
  BOOLEAN           Gear2;
  BOOLEAN           Gear4;
  BOOLEAN           UlxUlt;
  BOOLEAN           A0;
  BOOLEAN           J0;
  BOOLEAN           Matched;
  BOOLEAN           Ddr5Cmd2N;
  BOOLEAN           Lp5G42400;
  UINT32            Controller;
  UINT32            Channel;
  UINT32            MaxChannels;
  UINT32            Byte;
  UINT32            PiToQclk;
  UINT32            RecvEnFlyBy;
  INT64             GetSetVal;
  INT64             GetSetDis;
  INT64             GetSetEn;
  INT64             RecEnMax;
  INT64             TxDqsMax;
  INT64             TxDqMax;
  INT64             RcvEnPi[MAX_CONTROLLER][MAX_CHANNEL];
  INT64             RxDqsPPi;
  INT64             RxDqsNPi;
  INT64             TxDqPi;
  INT64             TxDqsPi;
  INT64             TxDqBitPi;
  INT64             TxDqsBitPi;
  INT64             tCL4RcvEn;
  INT64             tCL4RxDqFifoRdEn;
  INT64             RxDataValidDclk;
  INT64             RxDataValidQclk;
  INT64             RxDqFifoRdEnRankDel;
  INT64             RxFlybyDelayVal;
  INT64             tCWL4TxDqFifoWrEn;
  INT64             tCWL4TxDqFifoRdEn;
  INT32             TdqsckMin;
  INT32             tCL;
  INT32             tCWL;
  INT32             RcvEnPiValue;
  UINT16            QclkPs;
  UINT32            CtlDelay;
  UINT32            Safe;
  UINT32            Data32;
  UINT32            NMode;
  UINT32            IpChannel;
  UINT32            Offset;
  UINT32            AddTcwl;
  UINT32            DecTcwl;
  UINT8             CmdMirror;
  DDRSCRAM_CR_DDRSCRAMBLECH0_STRUCT   DdrScramCh0;
  DDRSCRAM_CR_DDRMISCCONTROL1_STRUCT  MiscControl1;
  DDRSCRAM_CR_DDRMISCCONTROL2_STRUCT  MiscControl2;
  DDRSCRAM_CR_DDRMISCCONTROL7_STRUCT  MiscControl7;
  MCMISCS_DDRWCKCONTROL_STRUCT        WckControl;
  MCMISCS_DDRWCKCONTROL1_STRUCT       WckControl1;
  MC0_CH0_CR_SC_WR_DELAY_STRUCT       ScWrDelay;
  MCMISCS_WRITECFGCH01_STRUCT         WriteCfgCh01;
  MC0_CH0_CR_MC2PHY_BGF_CTRL_STRUCT   Mc2phybgfctrl;
  Status      = mrcSuccess;
  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  SaveOutputs = &MrcData->Save.Data;
  Debug       = &Outputs->Debug;
  MaxChannels = Outputs->MaxChannels;
  DdrType     = Outputs->DdrType;
  Lpddr4      = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4        = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5        = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr       = Outputs->Lpddr;
  UlxUlt      = Inputs->UlxUlt;
  A0          = Inputs->A0;
  J0          = Inputs->J0;
  CmdMirror   = Inputs->CmdMirror;
  QclkPs      = Outputs->Qclkps;
  Gear2       = Outputs->Gear2;
  Gear4       = Outputs->Gear4;
  Gear1       = !Gear2 && !Gear4;
  DdrFrequency = Outputs->Frequency;
  Profile     = Inputs->MemoryProfile;
  Safe        = Inputs->SafeMode ? 0xFFFFFFFF : 0;
  NMode       = 0;
  GetSetDis   = 0;
  GetSetEn    = 1;
  Matched     = (Outputs->RxMode == MrcRxModeMatchedN) || (Outputs->RxMode == MrcRxModeMatchedP);
  Ddr5Cmd2N   = Ddr5 && (MrcGetNMode (MrcData) == 2);
  Lp5G42400   = Lpddr5 && Gear4 && (DdrFrequency == f2400);


  // Previous DdrIoInit steps were using CR multicast. Cache may have some other values in it causing register corruption.
  // Invalidate cache here to cause GetSet to do RMW.
  InitializeRegisterCache (MrcData);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Init Data CRs\n");
  if (Lpddr4) {
    TdqsckMin = (Inputs->LoopBackTest) ? 0 : tDQSCK_MIN_LP4;
  } else {
    TdqsckMin = 0;
  }
  TdqsckMin = DIVIDECEIL (TdqsckMin, QclkPs);

  PiToQclk  = Gear4 ? (64 * 4) : 64 * (1 + Gear2);

  if (A0 || UlxUlt) {
    RxDataValidDclk = Gear1 ? 8 : 16; // RTL constant
    RxDataValidQclk = 0;              // RTL constant
  } else {
    RxDataValidDclk = Gear1 ? 8 : 17; // RTL constant
    RxDataValidQclk = Gear1 ? 1 : 0;  // RTL constant
  }
  RxFlybyDelayVal = 0;

  CtlDelay = CtlPi / 64;              // Convert PI to UI's

  RxDqsPPi  = 32;
  RxDqsNPi  = 32;


  // TGL:
  // TxDqsPi = (Gear2 ? (6 + 64) : 6) + CtlPi + (Lpddr5 ? 80 : 0);
  // TxDqsPi += (Ddr5 ? (Gear2 ? 114 : 57): 0);
  //
  // @todo_adl Find out why LP5 needs 32 instead of 96 below
  TxDqsPi = CtlPi + (Gear4 ? 128 : 64);
  TxDqPi  = TxDqsPi + ((Gear2 || Gear4) ? (!Lpddr5 ? 96 : 32) : 32);

  TxDqsBitPi = 0;
  TxDqBitPi  = 0;

  tCL = tCWL = 0;

  //----------------------------------- RX -----------------------------------------
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelOut = &ControllerOut->Channel[Channel];
      TimingProfile = &ChannelOut->Timing[Profile];
      NMode = TimingProfile->NMode;

      tCL = (UINT8) TimingProfile->tCL;
      if (Lpddr5) {
        // Scale to WCK which matches QCLK in the IO.
        tCL *= 4;
      }
      // TGL: RcvEnPi[Controller][Channel] = (Lpddr5 ? 0 : 128) + (Gear2 ? 0 : CtlPi) + (Ddr4 ? 32 : 0);  // Start from CTL PI value
      RcvEnPiValue = 0;
      if (UlxUlt) {
        if (Ddr5) {
          RcvEnPiValue = Matched ? 128 : 64;
          if (Outputs->Frequency == f4400) {
            RcvEnPiValue = Gear4 ? (Matched ? 0x90 : 0xB0) : 0xA0;
          }
        } else if (Ddr4) {
          RcvEnPiValue = Gear2 ? 0 : 256;
        } else if (Lpddr5) {
          RcvEnPiValue = Gear4 ? 128 : 0;
        } else { // LPDDR4
          RcvEnPiValue = CtlPi + (Gear2 ? 0 : (Gear4 ? 160 : 80));
        }
      } else {
        if (Ddr5) {
          RcvEnPiValue = CtlPi + (Matched ? 128 : 64);
        } else if (Ddr4) {
          RcvEnPiValue = CtlPi + (Gear2 ? 128 : 80);  // Start from CTL PI value
        }
      }
      RcvEnPi[Controller][Channel] = RcvEnPiValue;

      Data32 = (tCL * (2 - Gear2)) + TdqsckMin;  // @todo_adl Gear4 here and below
      if (Gear2) {
        Data32 -= 10;
      } else {
        Data32 -= (UlxUlt ? (Ddr5 ? 18 : 15) : 16);
      }
      // Data32 -= (Controller ? 4 : 0); // Adjust delay for MC1
      if (Ddr5) {
        if (Channel == 0) {
          Data32 -= (3 * (2 - Gear2));  // -3 tCK for DDR5 on ch0
        } else {
          Data32 -= (5 * (2 - Gear2));  // -5 tCK for DDR5 on ch1
        }
      }
      if (Ddr4 && Gear2 && !UlxUlt) {
        Data32 -= 2;                                                // DDR4 G2: CL - 12 (assuming zero RecvEn flyby)
        RecvEnFlyBy = ((UINT32) RcvEnPi[Controller][Channel]) >> 7; // Adjust for RecvEn Flyby
        Data32 -= RecvEnFlyBy;
      }
      if (!Gear2) {
        if (!UlxUlt) {
          RcvEnPi[Controller][Channel] += ((Data32 % 2) * 64);   // If odd number of QCLKs, add one 1 QCLK to RcvEnPi
        }
      } else if (Lpddr4) {
        Data32 -= 1;
        if (Gear2) {
          Data32 -= Controller ? 3 : 1;
        }
      } else if (Lpddr5) {
        Data32 -= 9;
      }
      tCL4RcvEn = Data32 / (2 - Gear2); // RxIoTclDelay (tCK)
      if ((Lpddr4 || Ddr4) && UlxUlt) {
        tCL4RcvEn -= 1;
        tCL4RcvEn -= ((Ddr4 && Gear2) ? 1 : 0);
      }

      if (Gear4) {
        tCL4RcvEn = tCL - 22 - (SaveOutputs->UiLock / 2) * Ddr5Cmd2N;
        if (Lpddr4) {
          tCL4RcvEn = tCL - (Controller ? 19 : 20);
          if (DdrFrequency == f2667) {
            tCL4RcvEn = tCL - (Controller ? 23 : 19);
          }
        }
      }
      tCL4RxDqFifoRdEn = tCL4RcvEn;
      if (Ddr5) {
        // If Read DQS Offset feature is used, we need to pull in RecvEn accordingly
        // Keep tCL4RxDqFifoRdEn based on the original tCL4RcvEn
        tCL4RcvEn -= SaveOutputs->ReadDqsOffsetDdr5;
      }

      tCL4RxDqFifoRdEn += MrcRcvEn2RxFifoReadTclDelay (MrcData, Controller, Channel, DdrType, (Gear2 ? 2 : 1));
      if (Gear4) {
        tCL4RxDqFifoRdEn = tCL - 8;
        if (Lpddr5) {
          tCL4RxDqFifoRdEn = tCL - 12; //tcl4rxdqfiforden = cl-2-8-2
          tCL4RcvEn        = tCL - 30;
          RxDataValidDclk  = 16;
        } else if (Lpddr4) {
          RxDataValidDclk = 16;
        }
      }
      RxDqFifoRdEnRankDel = DIVIDECEIL ((UINT32) RcvEnPi[Controller][Channel], PiToQclk);
      if ((Ddr4 || Ddr5) && UlxUlt) {
        RxDqFifoRdEnRankDel = 0;
        RxDqFifoRdEnRankDel += ((Ddr4 && Gear2) ? 2 : 0);
      }
      RxDqFifoRdEnRankDel += CtlDelay; // @todo_adl Should be zero ?


      MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, RecEnDelay,  WriteToCache | PrintValue, &RcvEnPi[Controller][Channel]);
      MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsPDelay, WriteToCache | PrintValue, &RxDqsPPi);
      MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsNDelay, WriteToCache | PrintValue, &RxDqsNPi);

      MrcGetSetMcCh (MrcData, Controller, Channel, RxIoTclDelay,           WriteToCache | PrintValue, &tCL4RcvEn);
      MrcGetSetMcCh (MrcData, Controller, Channel, RxFifoRdEnTclDelay,     WriteToCache | PrintValue, &tCL4RxDqFifoRdEn);
      MrcGetSetMcCh (MrcData, Controller, Channel, RxDqDataValidDclkDelay, WriteToCache | PrintValue, &RxDataValidDclk);
      MrcGetSetMcCh (MrcData, Controller, Channel, RxDqDataValidQclkDelay, WriteToCache | PrintValue, &RxDataValidQclk);
      MrcGetSetMcChRnk (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, RxFlybyDelay,         WriteToCache | PrintValue, &RxFlybyDelayVal);
      MrcGetSetMcChRnk (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, RxFifoRdEnFlybyDelay, WriteToCache | PrintValue, &RxDqFifoRdEnRankDel);

      if (Matched) {
        GetSetVal = 0xF;
        MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxEq,    WriteToCache | PrintValue, &GetSetVal);
        if ((Ddr4) || (Ddr5)) {
          GetSetVal = 0x3;
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxC, WriteToCache | PrintValue, &GetSetVal);
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxR, WriteToCache | PrintValue, &GetSetVal);
        }
        GetSetVal = 0xF;
        MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxDqsEq, WriteToCache | PrintValue, &GetSetVal);
      }

      if (!Matched && SaveOutputs->RxDfe) {
        // Before C0/K0: enable RxDfe only on channels that have at least 2 ranks
        if (((MrcCountBitsEqOne (ChannelOut->ValidRankBitMask) > 1) || !(A0 || Inputs->B0 || J0 || Inputs->Q0 || Inputs->G0)) ||
            (Lpddr5 && (DdrFrequency >= f5600)))  {
          GetSetVal = 0xF;
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxTap1,   WriteToCache | PrintValue, &GetSetVal);
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxTap1En, WriteToCache | PrintValue, &GetSetEn);
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxTap2En, WriteToCache | PrintValue, &GetSetEn);
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxTap3En, WriteToCache | PrintValue, &GetSetEn);
        } else {
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, ForceDfeDisable, WriteNoCache | PrintValue, &GetSetEn);
        }
      }

      /* TGL:
      MrcGetSetBit (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, RxDqsBitDelay,    WriteToCache | GSM_UPDATE_HOST | PrintValue, &RxDqsBitPi);
      MrcGetSetBit (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, RxVoc,           WriteToCache | PrintValue, &GetSetVal);
      MrcGetSetBit (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, RxVocUnmatched,  WriteToCache | PrintValue, &GetSetVal);
      MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsAmpOffset,         WriteToCache | PrintValue, &GetSetVal);
      */
    }
  }

  //----------------------------------- TX -----------------------------------------
  // @todo_adl TxDqsBitDelay in Gear4: need to program DDRDATA_CR_DDRDATADQSPH90RANKX as well
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
// TGL
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      ChannelOut = &ControllerOut->Channel[Channel];
      TimingProfile = &ChannelOut->Timing[Profile];
      tCWL = TimingProfile->tCWL;
      if (Lpddr5) {
        // Scale to WCK which aligns to QCLK in the IO.
        tCWL *= 4;
      }

      ScWrDelay.Data = 0;
      AddTcwl = 0;
      DecTcwl = (UlxUlt) ? 2 : 3;
      DecTcwl = MIN (DecTcwl, (UINT8) tCWL + AddTcwl - 2);
      if (!Lpddr) {
        DecTcwl += (Controller ? 2 : 0); // Adjust delay for MC1
      }

      if (UlxUlt ) {
        if (Ddr5) {
          DecTcwl += 1;
        } else if (Ddr4) {
          DecTcwl += (Gear2 ? 0 : 4);
        } else if (Lpddr5) {
          if (Lp5G42400) {
            DecTcwl += 1;
          } else {
            DecTcwl += (Controller ? 3 : 2);
          }
          //
          //
          while((tCWL/ (Gear4 ? 4 : (Gear2 ?  2 : 1))- DecTcwl + AddTcwl) <= 3) {
            DecTcwl -= 1;
          }
        } else if (Lpddr4) {
          DecTcwl += 1;
        }
      } else {
        if (Ddr5 && (Channel == 1)) {
          DecTcwl += 1;
        }
      }

      if (Ddr5 && Gear4) {
        DecTcwl -= 3; // Gear4 in DDR5 has a limitation of 5
      }
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_WR_DELAY_REG, MC1_CH0_CR_SC_WR_DELAY_REG, Controller, MC0_CH1_CR_SC_WR_DELAY_REG, IpChannel);
      ScWrDelay.Bits.Add_tCWL = AddTcwl;
      ScWrDelay.Bits.Dec_tCWL = DecTcwl;
      MrcWriteCR (MrcData, Offset, ScWrDelay.Data);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d C%d Dec_tCWL: %d\n", Controller, Channel, ScWrDelay.Bits.Dec_tCWL);

      if (Lpddr4 && Gear4) {
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MC2PHY_BGF_CTRL_REG, MC1_CH0_CR_MC2PHY_BGF_CTRL_REG, Controller, MC0_CH1_CR_MC2PHY_BGF_CTRL_REG, IpChannel);
        Mc2phybgfctrl.Data = MrcReadCR (MrcData, Offset);
        Mc2phybgfctrl.Bits.tCWL_offset = (TimingProfile->tCWL - 1) & (MRC_BIT1 | MRC_BIT0);   // Timing->tCWL has 1 extra clock because of tDQSS - subtract it here.
        MrcWriteCR (MrcData, Offset, Mc2phybgfctrl.Data);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d C%d Mc2phybgfctrl: 0x%x\n", Controller, Channel, Mc2phybgfctrl.Data);
      }

      if (!A0 && !J0) {
        GetSetVal = 0;
        MrcGetSetMcCh (MrcData, Controller, Channel, RxRptChDqClkOn, WriteToCache, &GetSetVal);
        MrcGetSetMcCh (MrcData, Controller, Channel, TxRptChDqClkOn, WriteToCache, &GetSetVal);
      }

      MrcGetTxDqFifoDelays (MrcData, Controller, Channel, tCWL, AddTcwl, DecTcwl, &tCWL4TxDqFifoWrEn, &tCWL4TxDqFifoRdEn);
      if (Lpddr5 & Gear4) {
        TxDqPi = 0x120;
      }

      MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, TxDqDelay,   WriteToCache | PrintValue, &TxDqPi);
      MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, TxDqsDelay,  WriteToCache | PrintValue, &TxDqsPi);
      MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, TxDqsBitDelay, WriteToCache | GSM_UPDATE_HOST | PrintValue, &TxDqsBitPi);
      MrcGetSetBit    (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, TxDqBitDelay, WriteToCache | GSM_UPDATE_HOST, &TxDqBitPi);

      MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoWrEnTcwlDelay,           WriteToCache | PrintValue, &tCWL4TxDqFifoWrEn);
      MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay,           WriteToCache | PrintValue, &tCWL4TxDqFifoRdEn);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmIocRptChRepClkOn,             WriteToCache | PrintValue, &GetSetDis);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmIocTxDqFifoRdEnPerRankDelDis, WriteToCache | PrintValue, UlxUlt ? &GetSetEn : &GetSetDis);  // @todo_adl set this on DT as well

      GetSetVal = MCMISCS_WRITECFGCH0_cmdanlgengracecnt_MAX;
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmIocCmdAnlgEnGraceCnt,         WriteToCache | PrintValue, &GetSetVal);

      MrcGetSetLimits (MrcData, GsmIocTxAnlgEnGraceCnt, NULL, &GetSetVal, NULL);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmIocTxAnlgEnGraceCnt,          WriteToCache | PrintValue, &GetSetVal);

      MrcGetSetMcChRnk (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, TxDqFifoRdEnFlybyDelay, WriteToCache | PrintValue, &GetSetDis);

      // TGL:
      // MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, TxEq,        WriteToCache | PrintValue, &TxEqInit);

      if (((ChannelOut->ValidRankBitMask & 0x1) == 0) && (ChannelOut->ValidRankBitMask > 0x3)) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u DIMM1 alone\n", Controller, Channel);
        MrcGetSetLimits (MrcData, RecEnDelay, NULL, &RecEnMax, NULL);
        MrcGetSetLimits (MrcData, TxDqsDelay, NULL, &TxDqsMax, NULL);
        MrcGetSetLimits (MrcData, TxDqDelay,  NULL, &TxDqMax, NULL);
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) { // Not using MAX_SDRAM_IN_DIMM as GetSet won't program unpopulated rank if multicast is used
          MrcGetSetStrobe (MrcData, Controller, Channel, rRank0, Byte, RecEnDelay,  WriteToCache, &RecEnMax);
          MrcGetSetStrobe (MrcData, Controller, Channel, rRank0, Byte, TxDqsDelay,  WriteToCache, &TxDqsMax);
          MrcGetSetStrobe (MrcData, Controller, Channel, rRank0, Byte, TxDqDelay,   WriteToCache, &TxDqMax);
        }
      }
    }
  }
  MrcFlushRegisterCachedData (MrcData);

  MrcWriteCrMulticast (MrcData, DATA_CR_DDRCRDATAOFFSETTRAIN_REG, 0);
  MrcWriteCrMulticast (MrcData, DATA_CR_DDRCRDATAOFFSETCOMP_REG,  0);

  //---------------------------------------------
  // SCRAM fub
  //---------------------------------------------

  for (Channel = 0; Channel < MRC_NUM_CCC_INSTANCES; Channel++) {
    Offset = OFFSET_CALC_CH (DDRSCRAM_CR_DDRSCRAMBLECH0_REG, DDRSCRAM_CR_DDRSCRAMBLECH1_REG, Channel);
    DdrScramCh0.Data = MrcReadCR (MrcData, Offset);
    DdrScramCh0.Bits.scramen = 0;                       // Keep scrambling disabled for training
    if (Lpddr && ((CmdMirror >> Channel) & 0x1)) {
      DdrScramCh0.Bits.ca_mirrored = Outputs->ValidRankMask;
    }
    //DdrScramCh0.Bits.dis_cmdanalogen = Safe ? 0 : 1;
    //DdrScramCh0.Bits.dis_cmdanalogen_at_idle = ?
    MrcWriteCR (MrcData, Offset, DdrScramCh0.Data);
  }

  MiscControl1.Data = MrcReadCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL1_REG);
  MiscControl1.Bits.companddeltadqsupdateclkgatedisable = Safe ? 1 : 0;
  MrcWriteCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL1_REG, MiscControl1.Data);

  MiscControl2.Data = MrcReadCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL2_REG);
  MiscControl2.Bits.rx_analogen_grace_cnt = 0x70;
  MrcWriteCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL2_REG, MiscControl2.Data);

  MiscControl7.Data = 0;
  Data32 = Outputs->BurstLength;    // In tCK
  if (Lpddr5) {
    Data32 *= 4;
  }
  MiscControl7.Bits.rxburstlen = Data32;
  MiscControl7.Bits.txburstlen = Data32;
  MiscControl7.Bits.cmdduration = Ddr4 ? 0 : (Lpddr5 ? 7 : 3);  // 3 for LP4
  if (Lp5G42400) {
    MiscControl7.Bits.cmdduration = 3;
  }

  if (Ddr5) {
    MiscControl7.Bits.cmdduration = (NMode == 1) ? 1 : 2;  // 1 for 1N and 2 for 2N (Gear2 and Gear4)
    if (Gear1) {
      MiscControl7.Bits.cmdduration += 2;
    }
  }
  if (A0) {
    MiscControl7.BitsA0.datainvertnibble = 0;
    Offset = DDRSCRAM_CR_DDRMISCCONTROL7_A0_REG;
  } else {
    MiscControl7.Bits.datainvertnibble = 0;
    Offset = DDRSCRAM_CR_DDRMISCCONTROL7_REG;
  }
  MrcWriteCR (MrcData, Offset, MiscControl7.Data);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDRMISCCONTROL7: 0x%08X\n", MiscControl7.Data);

  // WCK Configuration
  if (Lpddr5) {
    WckControl.Data = MrcReadCR(MrcData, MCMISCS_DDRWCKCONTROL_REG);
    WckControl.Bits.TrainWCkMask = 4;
    WckControl.Bits.TrainWCkBL   = 0xF;
    WckControl.Bits.Lp5Mode = 1;

    Data32 = MrcGetWckPreStatic (MrcData);
    WckControl.Bits.tWckPre = Data32 * ((Gear2) ? 4 : 2);
    WckControl.Bits.tWckHalfRate = Gear4 ? 1 : (2 + Gear1);

    // JEDEC Spec Table 26. WCK2CK Sync AC Parameters for Write operation
    Data32 = MrcGetWckPreWrTotal (MrcData);
    WckControl.Bits.cas2wrwck = Gear4 ? ((tCWL / 2) - (Data32 * 2)): (tCWL + 4 - (Data32 * 4) + (Gear2 ? -1 : 1)); // tCWL was scaled to QCLK/WCK above.

    Data32 = MrcGetWckPreRdTotal (MrcData);
    WckControl.Bits.cas2rdwck = Gear4 ? ((tCL / 2) - (Data32 * 2)) : (tCL + 4 - (Data32 * 4) + (Gear2 ? -1 : 1));  // tCL was scaled to QCLK/WCK above.

    MrcWriteCR (MrcData, MCMISCS_DDRWCKCONTROL_REG, WckControl.Data);
  }

  WckControl1.Data = 0;
  if (Lpddr5) {
    WckControl1.Bits.cas2fswck       = Gear4 ? (MrcGetWckEnlFs (MrcData)) : ((MrcGetWckEnlFs (MrcData) * 4) + (Gear2 ? -1 : 1));
    WckControl1.Bits.wrcmd2dummy     = Gear4 ? 1 : 5;
    WckControl1.Bits.wrdummy2wrdummy = Gear4 ? 3 : 7;
    WckControl1.Bits.rdcmd2dummy     = Gear4 ? 1 : 5;
    WckControl1.Bits.rddummy2rddummy = Gear4 ? 3 : 7;
  }
  if (Ddr5 && (NMode == 2)) {
    WckControl1.Bits.ddr_2n_mode_en = 1;
  }
  MrcWriteCR (MrcData, MCMISCS_DDRWCKCONTROL1_REG, WckControl1.Data);

  if (A0) {
    WriteCfgCh01.Data = 0;
    WriteCfgCh01.Bits.tx_analog_clk_gate_dis_cha = 1;
    WriteCfgCh01.Bits.tx_analog_clk_gate_dis_chb = 1;
    MrcWriteCR (MrcData, MCMISCS_WRITECFGCH01_A0_REG, WriteCfgCh01.Data);
    MrcWriteCR (MrcData, MCMISCS_WRITECFGCH23_A0_REG, WriteCfgCh01.Data);
    MrcWriteCR (MrcData, MCMISCS_WRITECFGCH45_A0_REG, WriteCfgCh01.Data);
    MrcWriteCR (MrcData, MCMISCS_WRITECFGCH67_A0_REG, WriteCfgCh01.Data);
  }

  return Status;
}

/**
  PHY init - sequence static_DQ_AFE_EARLY in DQ FUB.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitDqStaticDqAfeEarly (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcStatus         Status;
  MrcOutput         *Outputs;
  MrcSaveData       *SaveOutputs;
  MrcDebug          *Debug;
  MrcDdrType        DdrType;
  BOOLEAN           Lpddr5;
  BOOLEAN           Lpddr4;
  BOOLEAN           Lpddr;
  BOOLEAN           Ddr5;
  BOOLEAN           Ddr4;
  BOOLEAN           Gear1;
  BOOLEAN           Gear2;
  BOOLEAN           Gear4;
  BOOLEAN           Lp5Gear4;
  BOOLEAN           EnNmosPup;
  BOOLEAN           A0;
  BOOLEAN           Q0Regs;
  MrcInput          *Inputs;
  BOOLEAN           UlxUlt;
  DATA0CH0_CR_AFEMISC_STRUCT      DataAfeMisc;
  DATA0CH0_CR_SRZCTL_STRUCT       DataSrzCtl;
  DATA0CH0_CR_DQRXCTL0_STRUCT     DataDqRxCtl0;
  DATA0CH0_CR_DQRXCTL1_STRUCT     DataDqRxCtl1;
  DATA0CH0_CR_DQSTXRXCTL_STRUCT   DataDqsTxRxCtl;
  DATA0CH0_CR_DQSTXRXCTL0_STRUCT  DataDqsTxRxCtl0;
  DATA0CH0_CR_COMPCTL0_STRUCT     DataCompCtl0;
  DATA0CH0_CR_COMPCTL1_STRUCT     DataCompCtl1;
  DATA0CH0_CR_COMPCTL2_STRUCT     DataCompCtl2;
  DATA0CH0_CR_DQTXCTL0_STRUCT     DataDqTxCtl0;
  DATA0CH0_CR_AFEMISCCTRL2_STRUCT DataAfeMiscCtrl2;

  Status      = mrcSuccess;
  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  SaveOutputs = &MrcData->Save.Data;
  Debug       = &Outputs->Debug;
  DdrType     = Outputs->DdrType;
  Lpddr4      = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4        = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5        = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr       = Outputs->Lpddr;
  Gear2       = Outputs->Gear2;
  Gear4       = Outputs->Gear4;
  Gear1       = !Gear2 && !Gear4;
  Lp5Gear4    = Lpddr5 && Gear4;
  EnNmosPup   = SaveOutputs->EnNmosPup;
  A0          = Inputs->A0;
  Q0Regs      = Inputs->Q0Regs;
  UlxUlt      = Inputs->UlxUlt;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcPhyInitDqStaticDqAfeEarly\n");

  DataAfeMisc.Data = 0;
  DataAfeMisc.Bits.afeshared_cmn_globalvsshibyp = SaveOutputs->GlobalVsshiBypass;
  DataAfeMisc.Bits.afeshared_cmn_iobufact       = 1;
  DataAfeMisc.Bits.rxall_cmn_rxlpddrmode        = Outputs->RxMode; // Encoding matches this CR: 0-LPDDR4x (matched P type) , 1-DDR4 (matched N type), 2-LPDDR5 (unmatched P type), 3-DDR5 (unmatched N type)
  DataAfeMisc.Bits.rxbias_cmn_tailctl           = (Ddr4) ? 3 : 0;
  DataAfeMisc.Bits.rxbias_cmn_biasicomp         = (Ddr5 && UlxUlt && (Outputs->Frequency == f2000)) ? 6  : (Ddr4) ? 0xC : 8;
  DataAfeMisc.Bits.rxbias_cmn_vrefsel           = (Ddr5 && UlxUlt && (Outputs->Frequency == f2000)) ? 5  : (Ddr4) ? 0xF : 6;
  DataAfeMisc.Bits.rxvref_cmn_selvsxhi          = Lpddr5 ? 0 : 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_AFEMISC_REG, DataAfeMisc.Data);

  DataSrzCtl.Data = 0;
  DataSrzCtl.Bits.dcdsrz_cmn_gear1en        = Gear1;
  DataSrzCtl.Bits.dcdsrz_cmn_gear2en        = Gear2;
  DataSrzCtl.Bits.dcdsrz_cmn_gear4en        = Gear4;
  DataSrzCtl.Bits.dqsdcdsrz_cmn_dcdtailctl  = 1;
  DataSrzCtl.Bits.cktop_cmn_bonus           = 0x78;                            // Bit [7] will be updated per UiLock in MrcLockUiCalibration
  DataSrzCtl.Bits.dqsbuf_cmn_bonus          = Ddr5 ? 3 : 2;
  DataSrzCtl.Bits.dqbuf_cmn_bonus           = SaveOutputs->GlobalVsshiBypass;  // Bit [0] of dqbuf_cmn_bonus is set if GlobalVsshiBypass enabled
//DataSrzCtl.Bits.dqbuf_cmn_bonus           = DataSrzCtl.Bits.dqbuf_cmn_bonus | MRC_BIT1; // Bit [1] of dqbuf_cmn_bonus is doubling the RxDFE range
  MrcWriteCrMulticast (MrcData, DATA_CR_SRZCTL_REG, DataSrzCtl.Data);

  DataDqRxCtl0.Data = 0;
  DataDqRxCtl0.Bits.dqrx_cmn_rxmctlecap = Ddr4 ? 3 : 1;
  DataDqRxCtl0.Bits.dqrx_cmn_rxmctleeq  = Ddr4 ? 0xF : 8;
  DataDqRxCtl0.Bits.dqrx_cmn_rxmctleres = Ddr4 ? 1 : 3;
//DataDqRxCtl0.Bits.dqrx_cmn_rxudfetap1 = 0;
  MrcWriteCrMulticast (MrcData, DATA_CR_DQRXCTL0_REG, DataDqRxCtl0.Data);

  DataDqRxCtl1.Data = 0;
  DataDqRxCtl1.Bits.dxrx_cmn_rxmvcmres              = 1;
  DataDqRxCtl1.Bits.dqrx_cmn_rxmdqbwsel             = MrcGetDllBwSel (MrcData); // Rx Matched Path DQ Bandwidth Select - should be same as rxsdlbwsel. Should match primary DLL - DDRCOMP_CR_VCCDLLCOMPDATACCC.Dll_bwsel
  DataDqRxCtl1.Bits.dqrx_cmn_rxvrefvdd2gmfc         = Lpddr ? 0 : 7;
  DataDqRxCtl1.Bits.dqrx_cmn_rxvrefvssmfc           = Lpddr ? 7 : 0;
  DataDqRxCtl1.Bits.dqrx_cmn_rxvrefvttmfc           = 0;
  DataDqRxCtl1.Bits.dqrx_cmn_rxvsshiffstrobestlegen = 1;
  DataDqRxCtl1.Bits.dqrx_cmn_rxmpdqdlycode          = 80;   // This helps seeing the left edge of RdT eye in Matched Rx
  if (Q0Regs) {
    DataDqRxCtl1.Bits.forcedfedisable = !SaveOutputs->RxDfe;
  }
  MrcWriteCrMulticast (MrcData, DATA_CR_DQRXCTL1_REG, DataDqRxCtl1.Data);

  DataDqsTxRxCtl.Data = 0;
  DataDqsTxRxCtl.Bits.dqsrx_cmn_rxdiffampen       = 1;
  DataDqsTxRxCtl.Bits.dqsrx_cmn_rxendqsnrcven     = 0;
  DataDqsTxRxCtl.Bits.dqsrx_cmn_rxmctlecap        = Lp5Gear4 ? 0x3 : 0x1;
  DataDqsTxRxCtl.Bits.dqsrx_cmn_rxmctleeq         = Lp5Gear4 ? 0xF : 0x8;
  DataDqsTxRxCtl.Bits.dqsrx_cmn_rxmctleres        = 3;
  DataDqsTxRxCtl.Bits.dqsrx_cmn_rxmtailctl        = (Ddr5 && UlxUlt && (Outputs->Frequency == f2000)) ? 0 : ((Lpddr4 || Ddr4) ? 2 : 3);
  if (A0) {
    DataDqsTxRxCtl.BitsA0.dqstx_cmn_txodten         = 0;
    DataDqsTxRxCtl.BitsA0.dqstx_cmn_txidlemodedrven = 1;  // @todo_adl
  } else {
    DataDqsTxRxCtl.Bits.dqstx_cmn_txidlemodedrven = 1;    // @todo_adl
    DataDqsTxRxCtl.Bits.dqstx_cmn_txvttparkendqsn = Lpddr;
  }
  DataDqsTxRxCtl.Bits.dqstx_cmn_txodtstatlegendqs = 1;    // @todo_adl cfg.rcomp.txodtstatlegendqs
  if (Q0Regs) {
    DataDqsTxRxCtl.BitsQ0.dqstx_cmn_txdqspfalldelay = 0x1F;
    DataDqsTxRxCtl.BitsQ0.dqstx_cmn_txdqsprisedelay = 0x1F;
  } else {
    DataDqsTxRxCtl.Bits.dqstx_cmn_txdqspfnrdelay    = 0x10;
    DataDqsTxRxCtl.Bits.dqstx_cmn_txdqsprnfdelay    = 0x10;
  }
  DataDqsTxRxCtl.Bits.dqstx_cmn_txvttparkendqsp   = 0;
  DataDqsTxRxCtl.Bits.dqstx_cmn_txvsshiffstlegen  = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_DQSTXRXCTL_REG, DataDqsTxRxCtl.Data);

  DataDqsTxRxCtl0.Data = 0;
  DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk0 = Lp5Gear4 ? 0x14 : 0x10;
  DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk1 = Lp5Gear4 ? 0x14 : 0x10;
  DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk2 = 0x10;
  DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk3 = 0x10;
  if (Q0Regs) {
    DataDqsTxRxCtl0.BitsQ0.dqstx_cmn_txdqsnfalldelay = 0x1F;
    DataDqsTxRxCtl0.BitsQ0.dqstx_cmn_txdqsnrisedelay = 0x1F;
    DataDqsTxRxCtl0.BitsQ0.txdccrangesel             = 3;
  } else {
    DataDqsTxRxCtl0.Bits.txdccrangesel              = 3;
    DataDqsTxRxCtl0.Bits.forcedfedisable            = !SaveOutputs->RxDfe;
  }
  MrcWriteCrMulticast (MrcData, DATA_CR_DQSTXRXCTL0_REG, DataDqsTxRxCtl0.Data);

  DataCompCtl0.Data = 0;
  DataCompCtl0.Bits.dqstx_cmn_txrcompodtdndqs = 0x20;
  DataCompCtl0.Bits.dqstx_cmn_txrcompodtupdqs = 0x20;
  DataCompCtl0.Bits.dqtx_cmn_txrcompodtdndq   = 0x20;
  DataCompCtl0.Bits.dqtx_cmn_txrcompodtupdq   = 0x20;
  DataCompCtl0.Bits.dxtx_cmn_ennbiasboost     = SaveOutputs->EnNBiasBoost;
  MrcWriteCrMulticast (MrcData, DATA_CR_COMPCTL0_REG, DataCompCtl0.Data);

  DataCompCtl1.Data = 0;                                // @todo_adl The whole register is unused ?
  DataCompCtl1.Bits.dxtx_cmn_txrcompdrvdn   = 0x20;
  DataCompCtl1.Bits.dxtx_cmn_txrcompdrvup   = 0x20;
  DataCompCtl1.Bits.dqstx_cmn_txtcocompdqsn = 0x80;
  DataCompCtl1.Bits.dqstx_cmn_txtcocompdqsp = 0x80;
  MrcWriteCrMulticast (MrcData, DATA_CR_COMPCTL1_REG, DataCompCtl1.Data);

  DataCompCtl2.Data = 0;
  DataCompCtl2.Bits.dxtx_cmn_txtcocomp        = 0x1E;
  DataCompCtl2.Bits.rxbias_cmn_rloadcomp      = 0x20;
  DataCompCtl2.Bits.dxtx_cmn_txvsshiff        = 0x20;
  DataCompCtl2.Bits.dqrx_cmn_rxvsshiffdata    = 0x20;
  DataCompCtl2.Bits.dqrx_cmn_rxvsshiffstrobe  = 0x20;
  MrcWriteCrMulticast (MrcData, DATA_CR_COMPCTL2_REG, DataCompCtl2.Data);

  DataDqTxCtl0.Data = 0;
  if (Ddr4) {
    DataDqTxCtl0.Bits.dqtx_cmn_txeqenntap = 2; //Enable 2 taps w 3 coeff per tap
    DataDqTxCtl0.Bits.dqtx_cmn_txeqcompcoeff0 = 0;
    DataDqTxCtl0.Bits.dqtx_cmn_txeqcompcoeff1 = 2;
    DataDqTxCtl0.Bits.dqtx_cmn_txeqcompcoeff2 = 2;
  }
  if (Ddr5) {
    DataDqTxCtl0.Bits.dqtx_cmn_txeqenntap = 2; //Enable 2 taps w 3 coeff per tap
    DataDqTxCtl0.Bits.dqtx_cmn_txeqcompcoeff0 = 0;
    DataDqTxCtl0.Bits.dqtx_cmn_txeqcompcoeff1 = 0;
    DataDqTxCtl0.Bits.dqtx_cmn_txeqcompcoeff2 = 1;
  }
  DataDqTxCtl0.Bits.dqtx_cmn_txeqconstz         = 1;
  DataDqTxCtl0.Bits.dqtx_cmn_txeqenpreset       = 1;
  DataDqTxCtl0.Bits.dqtx_cmn_txidlemodedrvdata  = !Lpddr;
  DataDqTxCtl0.Bits.dqtx_cmn_txidlemodedrven    = A0 ? 0 : (!Lpddr);
  DataDqTxCtl0.Bits.dqtx_cmn_txodtstatlegendq   = 1;      // @todo_adl  cfg.rcomp.txodtstatlegendq
  DataDqTxCtl0.Bits.dxtx_cmn_ennmospup          = EnNmosPup;
  DataDqTxCtl0.Bits.dqtx_cmn_txvsshiffstlegen   = 1;
  DataDqTxCtl0.Bits.dxtx_cmn_txlocalvsshibyp    = SaveOutputs->LocalVsshiBypass;
  DataDqTxCtl0.Bits.dxtx_cmn_txpdnusevcciog     = SaveOutputs->TxPdnUseVcciog;
  DataDqTxCtl0.Bits.dxtx_cmn_txpupusevcciog     = EnNmosPup;
  DataDqTxCtl0.Bits.dxtx_cmn_txvddqodten        = !Lpddr;
  DataDqTxCtl0.Bits.dxtx_cmn_txvssodten         = Lpddr;
  MrcWriteCrMulticast (MrcData, DATA_CR_DQTXCTL0_REG, DataDqTxCtl0.Data);

  DataAfeMiscCtrl2.Data = 0;
  DataAfeMiscCtrl2.Bits.dqrx_cmn_dqphsdrvprocsel = 3;
  DataAfeMiscCtrl2.Bits.dxtx_cmn_txvsshileakercomp = SaveOutputs->GlobalVsshiBypass ? 0x3F : 0;
  MrcWriteCrMulticast (MrcData, DATA_CR_AFEMISCCTRL2_REG, DataAfeMiscCtrl2.Data);

  return Status;
}

/**
  PHY init - sequence static_DQ_AFE in DQ and DATA_GLOBAL FUB's.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitDqStaticDqAfe (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcStatus         Status;
  MrcOutput         *Outputs;
  MrcInput          *Inputs;
  MrcDebug          *Debug;
//  MrcFrequency      DdrFrequency;
  BOOLEAN           Matched;
//  UINT32            Offset;
//  UINT32            Bit;
  DATA0CH0_CR_SDLCTL0_STRUCT      DataSdlCtl0;
  DATA0CH0_CR_DQXRXAMPCTL_STRUCT  DataDqxRxAmpCtl;
//  DATA0CH0_CR_DQRXAMPCTL0_STRUCT  DataDqRxAmpCtl0;
//  DATA0CH0_CR_DQRXAMPCTL1_STRUCT  DataDqRxAmpCtl1;
//  DATA0CH0_CR_DQRXAMPCTL2_STRUCT  DataDqRxAmpCtl2;
//  DATA0CH0_CR_DQ0RXAMPCTL_STRUCT  DataDq0RxAmpCtl;

  Status      = mrcSuccess;
  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  Matched     = (Outputs->RxMode == MrcRxModeMatchedP) || (Outputs->RxMode == MrcRxModeMatchedN);
//  DdrFrequency  = (Lpddr && Inputs->LpFreqSwitch) ? Outputs->HighFrequency : Outputs->Frequency;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcPhyInitDqStaticDqAfe\n");

  // DDRDATA_CR_SDLCTL1 is programmed in Seq1 already

  DataSdlCtl0.Data = 0;
  DataSdlCtl0.Bits.rxsdl_cmn_srzclkenrcven        = 1;
  DataSdlCtl0.Bits.rxsdl_cmn_enabledqsnrcven      = 0;
  DataSdlCtl0.Bits.rxsdl_cmn_rxmatchedpathen      = Matched;
  DataSdlCtl0.Bits.rxsdl_cmn_rxunmatchclkinv      = 0;          // This is set by calibration
  DataSdlCtl0.Bits.rxsdl_cmn_sdlen                = 1;
//DataSdlCtl0.Bits.rxsdl_cmn_mdlllen2gearratio    = 0;
//DataSdlCtl0.Bits.rxsdl_cmn_rxamp2pixoverovr     = 0;
//DataSdlCtl0.Bits.rxsdl_cmn_rxamp2pixoverselpin  = 0;
//DataSdlCtl0.Bits.rxsdl_cmn_rxamp2pixoverselpip  = 0;
  MrcWriteCrMulticast (MrcData, DATA_CR_SDLCTL0_REG, DataSdlCtl0.Data);

  DataDqxRxAmpCtl.Data = 0;
  //if DDR5/UlxUlt and Freq == 2000 keep the value 0
  if(!((Outputs->DdrType == MRC_DDR_TYPE_DDR5) && (Inputs->UlxUlt) && (Outputs->Frequency == f2000))) {
    DataDqxRxAmpCtl.Bits.dqrx_dq0_rxmtailctl  = 2;
    DataDqxRxAmpCtl.Bits.dqrx_dq1_rxmtailctl  = 2;
    DataDqxRxAmpCtl.Bits.dqrx_dq2_rxmtailctl  = 2;
    DataDqxRxAmpCtl.Bits.dqrx_dq3_rxmtailctl  = 2;
    DataDqxRxAmpCtl.Bits.dqrx_dq4_rxmtailctl  = 2;
    DataDqxRxAmpCtl.Bits.dqrx_dq5_rxmtailctl  = 2;
    DataDqxRxAmpCtl.Bits.dqrx_dq6_rxmtailctl  = 2;
    DataDqxRxAmpCtl.Bits.dqrx_dq7_rxmtailctl  = 2;
  }
  DataDqxRxAmpCtl.Bits.dqrx_dq0_rxusalspeed = 1;
  DataDqxRxAmpCtl.Bits.dqrx_dq1_rxusalspeed = 1;
  DataDqxRxAmpCtl.Bits.dqrx_dq2_rxusalspeed = 1;
  DataDqxRxAmpCtl.Bits.dqrx_dq3_rxusalspeed = 1;
  DataDqxRxAmpCtl.Bits.dqrx_dq4_rxusalspeed = 1;
  DataDqxRxAmpCtl.Bits.dqrx_dq5_rxusalspeed = 1;
  DataDqxRxAmpCtl.Bits.dqrx_dq6_rxusalspeed = 1;
  DataDqxRxAmpCtl.Bits.dqrx_dq7_rxusalspeed = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_DQXRXAMPCTL_REG, DataDqxRxAmpCtl.Data);

  /*
  // @todo_adl Default value is good enough ?
  DataDq0RxAmpCtl.Data = 0;
  DataDq0RxAmpCtl.Bits.dqrx_rk0_dq0_rxunoffsetcal   = 0x10;
  DataDq0RxAmpCtl.Bits.dqrx_rk1_dq0_rxunoffsetcal   = 0x10;
  DataDq0RxAmpCtl.Bits.dqrx_rk2_dq0_rxunoffsetcal   = 0x10;
  DataDq0RxAmpCtl.Bits.dqrx_rk3_dq0_rxunoffsetcal   = 0x10;
  DataDq0RxAmpCtl.Bits.dqrx_rk0_dq0_rxupmpoffsetcal = 0x10;
  DataDq0RxAmpCtl.Bits.dqrx_rk1_dq0_rxupmpoffsetcal = 0x10;
  for (Bit = 0; Bit <= 7; Bit++) {
    Offset = OFFSET_CALC_CH (DATA_CR_DQ0RXAMPCTL_REG, DATA_CR_DQ1RXAMPCTL_REG, Bit);
    MrcWriteCrMulticast (MrcData, Offset, DataDq0RxAmpCtl.Data);
  }

  DataDqRxAmpCtl0.Data = 0;
  DataDqRxAmpCtl0.Bits.dqrx_rk2_dq0_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl0.Bits.dqrx_rk3_dq0_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl0.Bits.dqrx_rk2_dq1_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl0.Bits.dqrx_rk3_dq1_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl0.Bits.dqrx_rk2_dq2_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl0.Bits.dqrx_rk3_dq2_rxupmpoffsetcal = 0x10;
  MrcWriteCrMulticast (MrcData, DATA_CR_DQRXAMPCTL0_REG, DataDqRxAmpCtl0.Data);

  DataDqRxAmpCtl1.Data = 0;
  DataDqRxAmpCtl1.Bits.dqrx_rk2_dq3_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl1.Bits.dqrx_rk3_dq3_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl1.Bits.dqrx_rk2_dq4_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl1.Bits.dqrx_rk3_dq4_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl1.Bits.dqrx_rk2_dq5_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl1.Bits.dqrx_rk3_dq5_rxupmpoffsetcal = 0x10;
  MrcWriteCrMulticast (MrcData, DATA_CR_DQRXAMPCTL1_REG, DataDqRxAmpCtl1.Data);

  DataDqRxAmpCtl2.Data = 0;
  DataDqRxAmpCtl2.Bits.dqrx_rk2_dq6_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl2.Bits.dqrx_rk3_dq6_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl2.Bits.dqrx_rk2_dq7_rxupmpoffsetcal = 0x10;
  DataDqRxAmpCtl2.Bits.dqrx_rk3_dq7_rxupmpoffsetcal = 0x10;
  MrcWriteCrMulticast (MrcData, DATA_CR_DQRXAMPCTL2_REG, DataDqRxAmpCtl2.Data);
  */

  // DDRDATAGLB_CR_VTTDRVSEGCTL - default value is good enough ?

  return Status;
}

/**
  PHY init - sequence static_FLL in FLL FUB.

  @param[in]  MrcData - Pointer to global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitStaticFll (
  IN  MrcParameters *const  MrcData
  )
{
  MrcStatus Status;
  MrcOutput *Outputs;
  UINT32    RefClk;
  UINT32    QclkFrequency;
  UINT32    Vdd2Mv;
  UINT32    Data32;
  UINT32    DividedRefClk;
  BOOLEAN   RefClk66;
  FLL_CMD_CFG_REG_STRUCT      FllCmdCfg;
  FLL_STATIC_CFG_0_REG_STRUCT FllStaticCfg0;
  FLL_STATIC_CFG_1_REG_STRUCT FllStaticCfg1;

  Status  = mrcSuccess;
  Outputs = &MrcData->Outputs;
  QclkFrequency = UDIVIDEROUND (1000000, Outputs->Qclkps);
  Vdd2Mv = Outputs->Vdd2Mv;
  RefClk66 = FALSE;

  switch (Outputs->RefClk) {
    case MRC_REF_CLOCK_133:
      RefClk = 133;
      DividedRefClk = 0;
      break;
    case MRC_REF_CLOCK_100:
      RefClk = 100;
      DividedRefClk = 1;
      break;
    case MRC_REF_CLOCK_66:
      RefClk = 66;
      DividedRefClk = 2;
      RefClk66 = TRUE;
      break;
    case MRC_REF_CLOCK_50:
    default:
      RefClk = 50;
      DividedRefClk = 3;
      break;
  }

  FllCmdCfg.Data = MrcReadCR (MrcData, FLL_CMD_CFG_REG_REG);
  FllCmdCfg.Bits.FLL_RATIO = QclkFrequency / RefClk;
  MrcWriteCR (MrcData, FLL_CMD_CFG_REG_REG, FllCmdCfg.Data);

  FllStaticCfg0.Data = MrcReadCR (MrcData, FLL_STATIC_CFG_0_REG_REG);
  FllStaticCfg0.Bits.CAL_THRESH_HI = 0;
  FllStaticCfg0.Bits.CAL_THRESH_LO = 0;
  if (Vdd2Mv == 1200) {
    Data32 = 4;                 // DDR4
  } else if (Vdd2Mv == 1100) {
    Data32 = 7;                 // DDR5 / LP4
  } else {
    Data32 = 9;                 // LP5
  }
  FllStaticCfg0.Bits.LDO_VREFSEL   = Data32;
  FllStaticCfg0.Bits.RUNTIME_CAL   = 0;
  FllStaticCfg0.Bits.FAST_CAL_WINDOW_VAL = RefClk66 ? 0 : 1; // @todo_adl find value for RefClk 50
  FllStaticCfg0.Bits.SLOW_CAL_WINDOW_VAL = RefClk66 ? 2 : 4; // @todo_adl find value for RefClk 50
  MrcWriteCR (MrcData, FLL_STATIC_CFG_0_REG_REG,  FllStaticCfg0.Data);

  FllStaticCfg1.Data = MrcReadCR (MrcData, FLL_STATIC_CFG_1_REG_REG);
  FllStaticCfg1.Bits.REFCLK_DIVIDE_RATIO_SEL  = DividedRefClk;
  FllStaticCfg1.Bits.VSUPPLY_CFG              = (Vdd2Mv >= 1075) ? 1 : 0;
  FllStaticCfg1.Bits.DELAY_FLLENABLE          = 5;
  if (Vdd2Mv == 1200) {
    Data32 = 0;                 // DDR4
  } else if (Vdd2Mv == 1100) {
    Data32 = 1;                 // DDR5 / LP4
  } else {
    Data32 = 2;                 // LP5
  }
  FllStaticCfg1.Bits.VSUPPLY_CFG = Data32;
  MrcWriteCR (MrcData, FLL_STATIC_CFG_1_REG_REG,  FllStaticCfg1.Data);

  MrcWriteCR (MrcData, FLL_STATIC_CFG_2_REG_REG, MrcReadCR (MrcData, FLL_STATIC_CFG_2_REG_REG));
  MrcWriteCR (MrcData, FLL_DEBUG_CFG_REG_REG, 0);
  MrcWriteCR (MrcData, FLL_DYNAMIC_CFG_REG_REG, 0);

  return Status;
}

/**
  PHY init - modmem_init_configflow_tagged_seq2

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitSeq2 (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               ClkPi,
  IN     UINT32               CtlPi
  )
{
  MrcStatus         Status;
  MrcOutput         *Outputs;
  MrcDdrType        DdrType;
  BOOLEAN           Ddr4;
  BOOLEAN           Ddr5;
  CH0CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT CccClkControls;
  MrcStatus         SrzDccStatus;
  SrzDccStatus = mrcSuccess;
  Status       = mrcSuccess;
  Outputs      = &MrcData->Outputs;
  DdrType      = Outputs->DdrType;
  Ddr4         = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5         = (DdrType == MRC_DDR_TYPE_DDR5);

  //---------------------------------------------
  // static_CCC_AFE - CMD / CCC_GLOBAL
  //---------------------------------------------
  Status |= MrcPhyInitStaticCccAfe (MrcData);

  //---------------------------------------------
  // static_DIG2 - CMD
  //---------------------------------------------
  CccClkControls.Data = 0;
  CccClkControls.Bits.RxVref = 0x20;
  CccClkControls.Bits.BlockTrainRst = 1;
  if (Ddr4) {
    CccClkControls.Bits.CaTxEq  = TXEQ_NODEEMPHASIS;
    CccClkControls.Bits.CtlTxEq = TXEQ_NODEEMPHASIS;
    CccClkControls.Bits.ClkTxEq = TXEQ_NODEEMPHASIS;
  } else {
    CccClkControls.Bits.CaTxEq  = (TXEQ_NODEEMPHASIS | TXEQ_CONSTATNTZ);
    CccClkControls.Bits.CtlTxEq = (TXEQ_NODEEMPHASIS | TXEQ_CONSTATNTZ);
    CccClkControls.Bits.ClkTxEq = 0x6;
  }
  CccClkControls.Bits.RTO                   = Ddr5 ? 1 : 0;
  CccClkControls.Bits.DefDrvEnLow           = Ddr4 ? 3 : 2; // Initial value (Ddr4 ? 0x3 : 0x2) but power training can try 0x1 for Ddr4 or weak Rodt.
  MrcWriteCrMulticast (MrcData, CCC_CR_DDRCRCCCCLKCONTROLS_REG, CccClkControls.Data);

  MrcWriteCrMulticast (MrcData, CCC_CR_TXPBDOFFSET_REG, 0);
  MrcWriteCrMulticast (MrcData, CCC_CR_PMFSM_REG, 0);

  //---------------------------------------------
  // static_DIG2 - DQ
  // Also static_DIG2 - SCRAM
  //---------------------------------------------
  Status |= MrcPhyInitDqStaticDig2 (MrcData, ClkPi, CtlPi);

  //---------------------------------------------
  // static_DQ_AFE - DQ and DATA_GLOBAL
  //---------------------------------------------
  Status |= MrcPhyInitDqStaticDqAfe (MrcData);

  if (Outputs->Gear4) {
    SrzDccStatus = MrcInitDcc (MrcData, DCD_GROUP_SRZ, DccFubDataCcc, DccFubNone);
    if (SrzDccStatus == mrcSuccess) {
      SrzDccStatus |= MrcInitDcc (MrcData, DCD_GROUP_SRZ, DccFubDataCcc, DccFubNone);
    }
    Status |= SrzDccStatus;
  }

  // Run software DCC calibration algorithm
  if (MrcData->Inputs.UlxUlt) {
    DccOptimize (MrcData);
  }

  // DDRCCC_CR_CLKALIGNCTL2 - default value is good enough ?

  //---------------------------------------------
  // static_DIG2 - SCRAM - done above in MrcPhyInitDqStaticDig2
  //---------------------------------------------

  //---------------------------------------------
  // static_DIG2 - VTT
  //---------------------------------------------
  // DDRVTT_CR_AFE_CTRL1      - initial values are good enough ?
  // DDRVTT_CR_AFE_CTRL2_NEW  - initial values are good enough ?
  // DDRVTT_CR_AFE_CTRL3_NEW  - initial values are good enough ?

  return Status;
}

/**
  PHY init - modmem_init_configflow_tagged_seq3

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitSeq3 (
  IN OUT MrcParameters *const MrcData
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcStatus   Status;
  MrcInput    *Inputs;
  MrcOutput   *Outputs;
  MrcDebug    *Debug;
  UINT32      Offset;
  UINT32      Controller;
  UINT32      Channel;
  UINT32      FirstController;
  UINT32      FirstChannel;
  UINT32      TransChannel;
  UINT32      TransStrobe;
  UINT32      Rank;
  UINT32      Strobe;
  UINT32      Index;
  UINT32      LdoFFCodeLock;
  UINT64      Timeout;
  BOOLEAN     A0;
  BOOLEAN     Done;
  BOOLEAN     UlxUlt;
  BOOLEAN     Populated;
  UINT32      CccFub;
  UINT32      FubOffset;
  UINT32      ClkComp;
  DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_STRUCT       DllCompSwCapCtrl;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT             FsmSkipCtrl;
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_STRUCT   CompVccDllCompDataCcc;
  CH0CCC_CR_VCCDLLCOMPDATACCC_STRUCT            CmdVccDllCompDataCcc;
  DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT             VccDllCompData;
  CH0CCC_CR_COMP2_STRUCT                        CccComp2;
  CH0CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT          CccClkControls;
  DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT          DataControl2;
  CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT           CccGlobalPmFsmOvrd;
  DATA0CH0_CR_PMFSM1_STRUCT                     DataPmFsm1;
  DATA0CH0_CR_PMFSM_STRUCT                      DataPmFsm;
  CH0CCC_CR_PMFSM_STRUCT                        CccPmFsm;
  DCSCTL3_STRUCT                                DcsCtl3;
  MCMISCS_SPINEGATING_STRUCT                    McMiscSpineGating;

  MrcCall = MrcData->Inputs.Call.Func;
  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status  = mrcSuccess;
  A0      = Inputs->A0;
  UlxUlt  = Inputs->UlxUlt;

  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcPhyInitSeq3\n");

  if (!A0 && MrcData->Inputs.PeriodicDcc) {
    DcsCtl3.Data = MrcReadCR (MrcData, DCSCTL3_REG);
    DcsCtl3.Bits.enperiodicdcc       = 1;
    DcsCtl3.Bits.enperiodicdccondvfs = 1;
    MrcWriteCR (MrcData, DCSCTL3_REG, DcsCtl3.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Periodic DCC Enabled\n");
  }

  FsmSkipCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG);
  if (A0) {
    FsmSkipCtrl.Bits.En_vdlllock    = 0;
    FsmSkipCtrl.Bits.En_pbiascal    = 0;
    FsmSkipCtrl.Bits.En_codewl      = 0;
    FsmSkipCtrl.Bits.En_codepi      = 0;
    FsmSkipCtrl.Bits.En_coderead    = 0;
  } else {
    FsmSkipCtrl.Bits.En_codewl      = 1;
    FsmSkipCtrl.Bits.En_codepi      = 1;
    FsmSkipCtrl.Bits.En_coderead    = 1;
  }

  ClkComp = (A0 || Inputs->B0 || Inputs->J0) ? 0 : 1;
  FsmSkipCtrl.Bits.En_clkup       = ClkComp;
  FsmSkipCtrl.Bits.En_clkdn       = ClkComp;
  FsmSkipCtrl.Bits.En_vsshiffclk  = ClkComp;
  FsmSkipCtrl.Bits.En_scompclk    = ClkComp;

  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrl.Data);

  if (ClkComp == 1) {
    // Set Comp sync enable at the end of Init in Seq3
    CccComp2.Data = MrcReadCR (MrcData, CH0CCC_CR_COMP2_REG);
    CccComp2.Bits.ccctx_rgrp3_compupdtsyncen = 1;
    CccComp2.Bits.ccctx_rgrp4_compupdtsyncen = 1;
    MrcWriteCrMulticast (MrcData, CCC_CR_COMP2_REG, CccComp2.Data);
  }

  if (A0) {
    // Propagate ldoffcodelock to all fubs
    DllCompSwCapCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_REG);
    LdoFFCodeLock = DllCompSwCapCtrl.Bits.ldoffcodelock;

    CompVccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);
    CompVccDllCompDataCcc.Bits.Dll_vdlllock = LdoFFCodeLock;
    MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG, CompVccDllCompDataCcc.Data);

    // Read from CCC0 and broadcast to CCC[0..7]
    CmdVccDllCompDataCcc.Data = MrcReadCR (MrcData, CH0CCC_CR_VCCDLLCOMPDATACCC_REG);
    CmdVccDllCompDataCcc.Bits.Dll_vdlllock = LdoFFCodeLock;
    MrcWriteCrMulticast (MrcData, CCC_CR_VCCDLLCOMPDATACCC_REG, CmdVccDllCompDataCcc.Data);

    // Read from first populated byte and broadcast to all
    Strobe = 0;    // Rank is not used here
    MrcTranslateSystemToIp (MrcData, &FirstController, &FirstChannel, &Rank, &Strobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
    Offset = DATA0CH0_CR_VCCDLLCOMPDATA_REG +
            (DATA0CH1_CR_VCCDLLCOMPDATA_REG - DATA0CH0_CR_VCCDLLCOMPDATA_REG) * FirstChannel +
            (DATA1CH0_CR_VCCDLLCOMPDATA_REG - DATA0CH0_CR_VCCDLLCOMPDATA_REG) * Strobe;
    VccDllCompData.Data = MrcReadCR (MrcData, Offset);
    VccDllCompData.Bits.Dll_vdlllock = LdoFFCodeLock;
    MrcWriteCrMulticast (MrcData, DATA_CR_VCCDLLCOMPDATA_REG, VccDllCompData.Data);

    Offset = CH0CCC_CR_COMP2_A0_REG;
    CccComp2.Data = MrcReadCR (MrcData, Offset);
    CccComp2.Bits.ccctx_cmn_txcompupdten = 1;
    Offset = CCC_CR_COMP2_A0_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccComp2.Data);

    Offset = CH0CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG;
    CccClkControls.Data = MrcReadCR (MrcData, Offset);
    CccClkControls.Bits.IntCkOn = 1;
    Offset = CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccClkControls.Data);

    Offset = DATA0CH0_CR_DDRCRDATACONTROL2_REG +
            (DATA0CH1_CR_DDRCRDATACONTROL2_REG - DATA0CH0_CR_DDRCRDATACONTROL2_REG) * FirstChannel +
            (DATA1CH0_CR_DDRCRDATACONTROL2_REG - DATA0CH0_CR_DDRCRDATACONTROL2_REG) * Strobe;
    DataControl2.Data = MrcReadCR (MrcData, Offset);
    DataControl2.BitsA0.compupdate_ovren = 1;
    MrcWriteCrMulticast (MrcData, DATA_CR_DDRCRDATACONTROL2_REG, DataControl2.Data);

    // And then set those back to zero
    CccComp2.Bits.ccctx_cmn_txcompupdten = 0;
    Offset = CCC_CR_COMP2_A0_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccComp2.Data);

    CccClkControls.Bits.IntCkOn = 0;
    Offset = CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccClkControls.Data);

    DataControl2.BitsA0.compupdate_ovren = 0;
    MrcWriteCrMulticast (MrcData, DATA_CR_DDRCRDATACONTROL2_REG, DataControl2.Data);
  }

  //---------------------------------------------
  // ddrphy_dq_ccc_clkalign_complete_override
  //---------------------------------------------

  // Poll for pmctrlfsm completion
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Polling on PMFSM.initdone_status.. ->\n");
  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT;   // 10 seconds timeout
  do {
    Done = TRUE;                           // Assume all complete
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          TransChannel = Channel;
          TransStrobe  = Strobe;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
          Offset  = DATA0CH0_CR_PMFSM_REG +
                   (DATA0CH1_CR_PMFSM_REG - DATA0CH0_CR_PMFSM_REG) * TransChannel +
                   (DATA1CH0_CR_PMFSM_REG - DATA0CH0_CR_PMFSM_REG) * TransStrobe;
          DataPmFsm.Data = MrcReadCR (MrcData, Offset);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u B%u -> C%u B%u, initdone_status: %u\n", Controller, Channel, Strobe, TransChannel, TransStrobe, DataPmFsm.Bits.initdone_status);
          if (A0) {
            if (DataPmFsm.BitsA0.initdone_status == 0) {
              Done = FALSE;
            }
          } else {
            if (DataPmFsm.Bits.initdone_status == 0) {
              Done = FALSE;
            }
          }
        }
      }
    }
    for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
      DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
      // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC%u -> MC%u C%u\n", Index, Controller, Channel);
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      Offset = OFFSET_CALC_CH (CH2CCC_CR_PMFSM_REG, CH0CCC_CR_PMFSM_REG, UlxUlt ? Index : CccIndexToFub[Index]);
      CccPmFsm.Data  = MrcReadCR (MrcData, Offset);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Polling on CccPmFsm.initdone_status at CCC%u MC%u C%u --> 0x%x, %d\n", Index, Controller, Channel, Offset, CccPmFsm.Bits.initdone_status);
      if (A0) {
        if (CccPmFsm.BitsA0.initdone_status == 0) {
          Done = FALSE;
        }
      } else {
        if (CccPmFsm.Bits.initdone_status == 0) {
          Done = FALSE;
        }
      }
    }
    if (MrcData->Inputs.SimicsFlag == 1) { // No Simics support for this FSM yet
      break;
    }
  } while (!Done && (MrcCall->MrcGetCpuTime () < Timeout));

  FubOffset = CCC1_GLOBAL_CR_PM_FSM_OVRD_0_REG - CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG;
  CccGlobalPmFsmOvrd.Data = 0;
  CccGlobalPmFsmOvrd.Bits.clkalign_ovren           = 1;
  CccGlobalPmFsmOvrd.Bits.clkalign_complete_ovren  = 1;
  CccGlobalPmFsmOvrd.Bits.clkalign_complete_ovrval = 1;
  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    CccFub = UlxUlt ? Index : CccIndexToFub[Index];
    DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
    Populated = MrcChannelExist (MrcData, Controller, Channel);
    CccGlobalPmFsmOvrd.Bits.iobufact_ovren  = !Populated;
    CccGlobalPmFsmOvrd.Bits.iobufact_ovrval = !Populated;
    Offset = ((CccFub % 2) ? CCC0_GLOBAL_CR_PM_FSM_OVRD_1_REG : CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG) + (CccFub / 2) * FubOffset;
    MrcWriteCR (MrcData, Offset, CccGlobalPmFsmOvrd.Data);
  }

  DataPmFsm1.Data = 0;
  DataPmFsm1.Bits.clkalign_ovren            = 1;
  DataPmFsm1.Bits.clkalign_complete_ovren   = 1;
  DataPmFsm1.Bits.clkalign_complete_ovrval  = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);

  // Disable Dynamic Pi Gating, will be enabled after end of MRC Training
  if (UlxUlt) {
    CccClkControls.Data = MrcReadCR (MrcData, CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG);
    CccClkControls.Bits.CaValidPiGateDisable = 1;
    CccClkControls.Bits.CkeIdlePiGateDisable = 1;
    CccClkControls.Bits.ClkGateDisable = 1;
    MrcWriteCrMulticast (MrcData, CCC_CR_DDRCRCCCCLKCONTROLS_REG, CccClkControls.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Disabling Dynamic PiGating in CCC\n");
  }

  // Disable DOP gating on L0 and R0, Will be enabled after MRC Training is done
  if (UlxUlt && (!(Inputs->J0) && !(Inputs->K0) && !(Inputs->Q0))) {
    McMiscSpineGating.Data = MrcReadCR (MrcData, MCMISCS_SPINEGATING_REG);
    McMiscSpineGating.Bits.gracelimitentry  = 7;
    McMiscSpineGating.Bits.enhiphase        = 2;
    McMiscSpineGating.Bits.awakecycles      = 0;
    McMiscSpineGating.Bits.sleepcycles      = 4;
    McMiscSpineGating.Bits.dopgatingdis     = 1;
    McMiscSpineGating.Bits.lpmode2gatingdis = 1;

    McMiscSpineGating.Bits.enablespinegate  = 0;  // Clock Spine Gating Disabled.
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Disable DOP/ClockSpine Gating\n");
    MrcWriteCR(MrcData, MCMISCS_SPINEGATING_REG, McMiscSpineGating.Data);
  }
  //---------------------------------------------
  // static_INITCOMPLETE - SCRAM
  //---------------------------------------------
  // Must be the last register written for basic init (Must be after MC Init).
  Offset = A0 ? DDRSCRAM_CR_DDRLASTCR_A0_REG : DDRSCRAM_CR_DDRLASTCR_REG;
  MrcWriteCR (MrcData, Offset, 1);

  ForceRcomp (MrcData);

  return Status;
}

/**
  This function initializes the Memory Controller IO.
  Occupancy and Scrambler registers are initialized.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcDdrIoPreInit (
  IN OUT MrcParameters *const MrcData
  )
{
  static const UINT8 ChNotPopMap[MRC_NUM_CCC_INSTANCES]    = { 4, 5, 0, 1, 2, 3, 6, 7 };
  MrcInput          *Inputs;
  MrcStatus         Status;
  MrcOutput         *Outputs;
  MrcControllerOut  *ControllerOut;
  MrcDebug          *Debug;
  MrcDdrType        DdrType;
  INT64             GetSetVal;
  INT64             GetSetEn;
//  INT64             GetSetDis;
  UINT32            Offset;
  UINT32            Controller;
  UINT32            Index;
  UINT32            RefClkPs;
  UINT32            Delay;
  UINT32            McIndex;
  UINT32            ChIndex;
  UINT32            TimerXXClk;
  UINT32            KeepXXClkOn;
  UINT16            TxEn;
  UINT8             Gear1;
  UINT8             IoChNotPop;
  UINT8             RankPresent;
  UINT8             Rank0Present;
  UINT8             Rank1Present;
  UINT8             Rank2Present;
  UINT8             Rank3Present;
  UINT8             CccMux;
  BOOLEAN           Lpddr;
  BOOLEAN           Lpddr4;
  BOOLEAN           Lpddr5;
  BOOLEAN           Ddr4;
  BOOLEAN           DqPinsInterleaved;
  BOOLEAN           UlxUlt;
  BOOLEAN           DtHalo;
  BOOLEAN           Safe;
  BOOLEAN           Lp5Descending;
  GSM_GT            Group;
  CCC_TX_EN_TYPE    CccTxEn;
  CH0CCC_CR_DDRCRPINSUSED_STRUCT  CccPinsUsed;
  UINT32                           MailboxCommand;
  UINT32                           MailboxData;
  UINT32                           MailboxStatus;
  DDRPHY_CR_MISCS_CR_AFE_BG_CTRL1  DdrPhyMiscsAfeBgCtrl1;
  UINT32                           VccIomV;
  const MRC_FUNCTION               *MrcCall;

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  Status      = mrcSuccess;
  DdrType     = Outputs->DdrType;
  Lpddr4      = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Ddr4        = (DdrType == MRC_DDR_TYPE_DDR4);
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Lpddr       = Outputs->Lpddr;
  UlxUlt      = Inputs->UlxUlt;
  DtHalo      = Inputs->DtHalo;
  Safe        = (Inputs->SafeMode == 1);
  Gear1       = (Outputs->Gear2 || Outputs->Gear4) ? 0 : 1;
  GetSetEn    = 1;
//  GetSetDis = 0;
  DqPinsInterleaved = (Inputs->DqPinsInterleaved != 0);
  RefClkPs = (Outputs->RefClk == MRC_REF_CLOCK_133) ? 7500 : 10000; // Reference Clock Period in pS

  // ADL-P/M : VccIo = 0.85v or 0.7v (binned)
  MrcCall = Inputs->Call.Func;
  if (UlxUlt) {
    MailboxCommand = CPU_MAILBOX_BIOS_CMD_MRC_CONFIG | (CPU_MAILBOX_BIOS_CMD_MRC_CONFIG_VCCIO_SUBCOMMAND << CPU_MAILBOX_CMD_PARAM_1_OFFSET);
    MrcCall->MrcCpuMailboxRead (MAILBOX_TYPE_PCODE, MailboxCommand, &MailboxData, &MailboxStatus);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CPU_MAILBOX_BIOS_CMD_MRC_CONFIG %s. MailboxStatus = %Xh, MailboxData = %Xh\n", (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed", MailboxStatus, MailboxData);
    if (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) {
      DdrPhyMiscsAfeBgCtrl1.Data = MailboxData;
      // target_voltage = (target_code  + 115) / 192 * 1V
      VccIomV = (DdrPhyMiscsAfeBgCtrl1.Bits.bg_ctrl_lvrtargetcode_iolvr_south + 115) * 1000;
      VccIomV = UDIVIDEROUND (VccIomV, 192);
      Inputs->VccIomV = (UINT16) VccIomV;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VccIo from mailbox command: %d mV\n", Inputs->VccIomV);
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "Failed to get VccIo from mailbox command, using default VccIo: %d mV\n", Inputs->VccIomV);
    }
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    ControllerOut->DeviceId = (UINT16) (MrcPciRead32 (MrcData, HOST_BRIDGE_BUS, HOST_BRIDGE_DEVICE, HOST_BRIDGE_FUNCTION, HOST_BRIDGE_DEVID) >> 16);
    ControllerOut->RevisionId = (UINT8) (MrcPciRead32 (MrcData, HOST_BRIDGE_BUS, HOST_BRIDGE_DEVICE, HOST_BRIDGE_FUNCTION, HOST_BRIDGE_REVID));
  }

  // Select the interleaving mode of DQ/DQS pins
  // This must be the first DDR IO register to be programmed on ULT
  GetSetVal = (DqPinsInterleaved) ? 0 : 1;
  MrcGetSetNoScope (MrcData, GsmIocNoDqInterleave, WriteToCache | PrintValue, &GetSetVal);
  MrcWeaklockEnDis (MrcData, MRC_DISABLE);

  if (Lpddr) {
    MrcGetSetNoScope (MrcData, GsmIocScramLpMode, WriteToCache | PrintValue, &GetSetEn);
  }

  switch (DdrType) {
    case MRC_DDR_TYPE_DDR4:
      Group = GsmIocScramDdr4Mode;
      break;

    case MRC_DDR_TYPE_LPDDR4:
      Group = GsmIocScramLp4Mode;
      break;

    case MRC_DDR_TYPE_DDR5:
      Group = GsmIocScramDdr5Mode;
      break;

    case MRC_DDR_TYPE_LPDDR5:
      Group = GsmIocScramLp5Mode;
      break;

    default:
      Group = GsmGtMax;
      break;
  }
  if (Group == GsmGtMax) {
    Status = mrcUnsupportedTechnology;
  } else {
    MrcGetSetNoScope (MrcData, Group, WriteToCache | PrintValue, &GetSetEn);
  }

  IoChNotPop = 0;
  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    CccTxEn.Data16 = 0;

    DdrIoTranslateCccToMcChannel (MrcData, Index, &McIndex, &ChIndex);
    RankPresent = Outputs->Controller[McIndex].Channel[ChIndex].ValidRankBitMask;
    Rank0Present = (0x1 & RankPresent);
    Rank1Present = ((RankPresent >> 1) & 0x1);
    Rank2Present = ((RankPresent >> 2) & 0x1);
    Rank3Present = ((RankPresent >> 3) & 0x1);
    if (RankPresent == 0) {
      IoChNotPop |= (1 << ((DtHalo && !Lpddr) ? ChNotPopMap[Index] : Index)); // Use 1:1 mapping for LPDDR
    }

    Lp5Descending = (((Inputs->Lp5CccConfig >> Index) & 0x1) != 0); // On ADL-P the CCC index matches Lp5CccConfig bitmask encoding

    // Configure non-common enables:
    // LPDDR - Command, CS
    // DDR   - ODT, CKE, CS
    if (UlxUlt) {
      if (Lpddr4) {
        switch (Index) {
          case 0:
          case 2:
          case 3:
          case 4:
          case 6:
          case 7:
            CccTxEn.Data16 = MRC_UY_LP4_CCC023467_CA_MSK;
            CccTxEn.Lp4.Ccc023467.Cs0 = Rank0Present;
            CccTxEn.Lp4.Ccc023467.Cs1 = Rank1Present;
            break;

          case 1:
          case 5:
            CccTxEn.Data16 = MRC_UY_LP4_CCC15_CA_MSK;
            CccTxEn.Lp4.Ccc15.Cs0 = Rank0Present;
            CccTxEn.Lp4.Ccc15.Cs1 = Rank1Present;
            break;

          default:
            break;
        }
      } else if (Lpddr5) {
        if (Lp5Descending) {
          // Descending Configuration
          switch (Index) {
            case 0:
            case 1:
            case 4:
            case 5:
              CccTxEn.Data16 = MRC_UY_LP5D_CCC0145_CA_MSK;
              CccTxEn.Lp5D.Ccc0145.Cs = RankPresent;
              break;

            case 2:
            case 3:
            case 6:
            case 7:
              CccTxEn.Data16 = MRC_UY_LP5D_CCC2367_CA_MSK;
              CccTxEn.Lp5D.Ccc2367.Cs0 = Rank0Present;
              CccTxEn.Lp5D.Ccc2367.Cs1 = Rank1Present;
              break;

            default:
              // Do nothing.  Will catch 0 in this as a failure below the DdrType Ladder.
              break;
          }
        } else {
          // Ascending Configuration
          switch (Index) {
            case 0:
            case 2:
            case 3:
            case 4:
            case 6:
            case 7:
              CccTxEn.Data16 = MRC_UY_LP5A_CCC023467_CA_MSK;
              CccTxEn.Lp5A.Ccc023467.Cs = RankPresent;
              break;

            case 1:
            case 5:
              CccTxEn.Data16 = MRC_UY_LP5A_CCC15_CA_MSK;
              CccTxEn.Lp5A.Ccc15.Cs0 = Rank0Present;
              CccTxEn.Lp5A.Ccc15.Cs1 = Rank1Present;
              break;

            default:
              // Do nothing.  Will catch 0 in this as a failure below the DdrType Ladder.
              break;
          }
        }
      } else if (Ddr4) {
        // DDR4
        switch (Index % 4) {
          case 0:
            CccTxEn.Data16 = MRC_UY_DDR4_CCC04_CA_MSK;
            if (Rank0Present) {
              CccTxEn.Ddr4.UY.Ccc04.Clk0 = 0x3;
            }
            break;

          case 1:
            CccTxEn.Data16 = MRC_UY_DDR4_CCC15_CA_MSK;
            CccTxEn.Ddr4.UY.Ccc15.Cs0  = Rank0Present;
            CccTxEn.Ddr4.UY.Ccc15.Cs1  = Rank1Present;
            CccTxEn.Ddr4.UY.Ccc15.Odt0 = Rank0Present;
            CccTxEn.Ddr4.UY.Ccc15.Odt1 = Rank1Present;
            break;

          case 2:
            CccTxEn.Data16 = MRC_UY_DDR4_CCC26_CA_MSK;
            CccTxEn.Ddr4.UY.Ccc26.Cke0 = Rank0Present;
            CccTxEn.Ddr4.UY.Ccc26.Cke1 = Rank1Present;
            break;

          case 3:
            CccTxEn.Data16 = MRC_UY_DDR4_CCC37_CA_MSK;
            if (Rank1Present) {
              CccTxEn.Ddr4.UY.Ccc37.Clk1 = 0x3;
            }
            break;
        }
      } else {
          // DDR5 - @adl_todo Need to update per P/M
        switch (Index) {
          case 0:
          case 2:
              CccTxEn.Data16 = MRC_HS_DDR5_CCC02_CA_MSK;
              CccTxEn.Ddr5.Ccc02.Cs0 = Rank0Present;
              CccTxEn.Ddr5.Ccc02.Cs1 = Rank1Present;
              CccTxEn.Ddr5.Ccc02.Cs3 = Rank3Present;
              CccTxEn.Ddr5.Ccc02.Clk2 = (Rank2Present ? 0x3 : 0);
              CccTxEn.Ddr5.Ccc02.Clk3 = (Rank3Present ? 0x3 : 0);
              break;
          case 1:
          case 3:
              CccTxEn.Data16 = MRC_HS_DDR5_CCC13_CA_MSK;
              CccTxEn.Ddr5.Ccc13.Cs2 = Rank2Present;
              CccTxEn.Ddr5.Ccc13.Clk0 = (Rank0Present ? 0x3 : 0);
              CccTxEn.Ddr5.Ccc13.Clk1 = (Rank1Present ? 0x3 : 0);
              break;
          case 4:
          case 6:
              CccTxEn.Data16 = MRC_HS_DDR5_CCC46_CA_MSK;
              CccTxEn.Ddr5.Ccc46.Cs0 = Rank0Present;
              CccTxEn.Ddr5.Ccc46.Cs1 = Rank1Present;
              CccTxEn.Ddr5.Ccc46.Clk2 = (Rank2Present ? 0x3 : 0);
              CccTxEn.Ddr5.Ccc46.Clk3 = (Rank3Present ? 0x3 : 0);
              break;
          case 5:
          case 7:
              CccTxEn.Data16 = MRC_HS_DDR5_CCC57_CA_MSK;
              CccTxEn.Ddr5.Ccc57.Cs2 = Rank2Present;
              CccTxEn.Ddr5.Ccc57.Cs3 = Rank3Present;
              CccTxEn.Ddr5.Ccc57.Clk0 = (Rank0Present ? 0x3 : 0);
              CccTxEn.Ddr5.Ccc57.Clk1 = (Rank1Present ? 0x3 : 0);
              break;
          default:
              // Do nothing.  Will catch 0 in this as a failure below the DdrType Ladder.
              break;
        }
      }
    } else {
      // DT/Halo
      if (Lpddr) {
        if ((Index % 2) == 0) {
          CccTxEn.Data16 = ((Lpddr5) ? MRC_HS_LP5_CCC0246_CA_MSK : MRC_HS_LP4_CCC012346_CA_MSK);
          CccTxEn.LpHS.Ccc0246.Cs0 = Rank0Present;
          CccTxEn.LpHS.Ccc0246.Cs1 = Rank1Present;
          CccTxEn.LpHS.Ccc0246.Cs2 = Rank2Present;
          CccTxEn.LpHS.Ccc0246.Cs3 = Rank3Present;
        } else if (Index < 4) {
          CccTxEn.Data16 = ((Lpddr5) ? MRC_HS_LP5_CCC13_CA_MSK : MRC_HS_LP4_CCC012346_CA_MSK);
          CccTxEn.LpHS.Ccc13.Cs0 = Rank0Present;
          CccTxEn.LpHS.Ccc13.Cs1 = Rank1Present;
          CccTxEn.LpHS.Ccc13.Cs2 = Rank2Present;
          CccTxEn.LpHS.Ccc13.Cs3 = Rank3Present;
        } else if (Index == 5) {
          CccTxEn.Data16 = ((Lpddr5) ? MRC_HS_LP5_CCC5_CA_MSK : MRC_HS_LP4_CCC5_CA_MSK);
          CccTxEn.LpHS.Ccc5.Cs0 = Rank0Present;
          CccTxEn.LpHS.Ccc5.Cs1 = Rank1Present;
          CccTxEn.LpHS.Ccc5.Cs2 = Rank2Present;
          CccTxEn.LpHS.Ccc5.Cs3 = Rank3Present;
        } else {
          CccTxEn.Data16 = ((Lpddr5) ? MRC_HS_LP5_CCC7_CA_MSK : MRC_HS_LP4_CCC7_CA_MSK);
          CccTxEn.LpHS.Ccc7.Cs0 = Rank0Present;
          CccTxEn.LpHS.Ccc7.Cs1 = Rank1Present;
          CccTxEn.LpHS.Ccc7.Cs2 = Rank2Present;
          CccTxEn.LpHS.Ccc7.Cs3 = Rank3Present;
        }
      } else if (Ddr4) { // Matches ADL-S DDR4
        switch (Index) {
          case 0:
          case 2:
            CccTxEn.Data16 = MRC_HS_DDR4_CCC02_CA_MSK;
            CccTxEn.Ddr4.HS.Ccc02.Cke0 = Rank0Present;
            CccTxEn.Ddr4.HS.Ccc02.Cke1 = Rank1Present;
            CccTxEn.Ddr4.HS.Ccc02.Cke2 = Rank2Present;
            CccTxEn.Ddr4.HS.Ccc02.Cke3 = Rank3Present;
            break;

          case 1:
          case 3:
          case 5:
          case 7:
            if (Index < 4) {
              CccTxEn.Data16 = MRC_HS_DDR4_CCC13_CA_MSK;
              CccTxEn.Ddr4.HS.Ccc1357.Clk0or2 = (Rank0Present ? 0x3 : 0);
              CccTxEn.Ddr4.HS.Ccc1357.Clk1or3 = (Rank1Present ? 0x3 : 0);
            } else {
              CccTxEn.Data16 = MRC_HS_DDR4_CCC57_CA_MSK;
              CccTxEn.Ddr4.HS.Ccc1357.Clk0or2 = (Rank2Present ? 0x3 : 0);
              CccTxEn.Ddr4.HS.Ccc1357.Clk1or3 = (Rank3Present ? 0x3 : 0);
            }
            break;

          case 4:
          case 6:
            CccTxEn.Data16 = MRC_HS_DDR4_CCC46_CA_MSK;
            CccTxEn.Ddr4.HS.Ccc46.Cs0 = Rank0Present;
            CccTxEn.Ddr4.HS.Ccc46.Cs1 = Rank1Present;
            CccTxEn.Ddr4.HS.Ccc46.Cs2 = Rank2Present;
            CccTxEn.Ddr4.HS.Ccc46.Cs3 = Rank3Present;
            CccTxEn.Ddr4.HS.Ccc46.Odt0 = Rank0Present;
            CccTxEn.Ddr4.HS.Ccc46.Odt1 = Rank1Present;
            CccTxEn.Ddr4.HS.Ccc46.Odt2 = Rank2Present;
            CccTxEn.Ddr4.HS.Ccc46.Odt3 = Rank3Present;
            break;

            default:
              // Do nothing.  Will catch 0 in this as a failure below the DdrType Ladder.
              break;
        }
      } else {
        // Ddr5
        switch (Index) {
          case 0:
          case 2:
            CccTxEn.Data16 = MRC_HS_DDR5_CCC02_CA_MSK;
            CccTxEn.Ddr5.Ccc02.Cs0 = Rank0Present;
            CccTxEn.Ddr5.Ccc02.Cs1 = Rank1Present;
            CccTxEn.Ddr5.Ccc02.Cs3 = Rank3Present;
            CccTxEn.Ddr5.Ccc02.Clk2 = (Rank2Present ? 0x3 : 0);
            CccTxEn.Ddr5.Ccc02.Clk3 = (Rank3Present ? 0x3 : 0);
            break;

          case 1:
          case 3:
            CccTxEn.Data16 = MRC_HS_DDR5_CCC13_CA_MSK;
            CccTxEn.Ddr5.Ccc13.Cs2 = Rank2Present;
            CccTxEn.Ddr5.Ccc13.Clk0 = (Rank0Present ? 0x3 : 0);
            CccTxEn.Ddr5.Ccc13.Clk1 = (Rank1Present ? 0x3 : 0);
            break;

          case 4:
          case 6:
            CccTxEn.Data16 = MRC_HS_DDR5_CCC46_CA_MSK;
            CccTxEn.Ddr5.Ccc46.Cs0 = Rank0Present;
            CccTxEn.Ddr5.Ccc46.Cs1 = Rank1Present;
            CccTxEn.Ddr5.Ccc46.Clk2 = (Rank2Present ? 0x3 : 0);
            CccTxEn.Ddr5.Ccc46.Clk3 = (Rank3Present ? 0x3 : 0);
            break;

          case 5:
          case 7:
            CccTxEn.Data16 = MRC_HS_DDR5_CCC57_CA_MSK;
            CccTxEn.Ddr5.Ccc57.Cs2 = Rank2Present;
            CccTxEn.Ddr5.Ccc57.Cs3 = Rank3Present;
            CccTxEn.Ddr5.Ccc57.Clk0 = (Rank0Present ? 0x3 : 0);
            CccTxEn.Ddr5.Ccc57.Clk1 = (Rank1Present ? 0x3 : 0);
            break;

            default:
              // Do nothing.  Will catch 0 in this as a failure below the DdrType Ladder.
              break;
        }
      }
    }

    if (CccTxEn.Data16 == 0) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s CCC%d TxEn not initialized properly\n", gErrString, Index);
      if (Inputs->ExitOnFailure) {
        return mrcFail;
      }
    }

    // Handle LP4/5 Clock and CKE/WCK since it is common.
    if (Lpddr) {
      CccTxEn.CccCommonLp.Clk = 0x3;
      CccTxEn.CccCommonLp.CkeWck = (Lpddr4) ? RankPresent : 0x3;
    }

    TxEn = CccTxEn.Data16;
    CccMux = 0;
    switch (DdrType) {
      case MRC_DDR_TYPE_DDR4:
        CccMux = 0;
        break;
      case MRC_DDR_TYPE_LPDDR4:
        CccMux = 1;
        break;

      case MRC_DDR_TYPE_LPDDR5:
        if (UlxUlt) {
          CccMux = Lp5Descending ? 0 : 2;
        } else {
          // DtHalo
          CccMux = 2;
        }
        break;

      case MRC_DDR_TYPE_DDR5:
          CccMux = 3;
        break;

      default:
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %s - %s:%d\n", gErrString, gUnsupportedTechnology, __FILE__, __LINE__);
        return mrcUnsupportedTechnology;
        break;
    }

    KeepXXClkOn = (Ddr4) ? 1 : 0;
    Delay = ((40 + Outputs->RcompTarget[RdOdt]) * 10) / 2; //@todo - Create a variable for DDR Init.
    Delay = Delay * 1000; // Scale to pS
    Delay = DIVIDEROUND (Delay, RefClkPs);
    Delay = MrcLog2 (Delay) - 2;
    TimerXXClk = RANGE (Delay, 0, 7);
    // Note the order of CCC instances is not linear: ch2 is first, ch0 is second etc.
    Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRPINSUSED_REG, CH0CCC_CR_DDRCRPINSUSED_REG, UlxUlt ? Index : CccIndexToFub[Index]);
    CccPinsUsed.Data = 0;
    CccPinsUsed.Bits.TxEn         = Safe ? CH2CCC_CR_DDRCRPINSUSED_TxEn_MAX : TxEn;
    if (UlxUlt) {
      CccPinsUsed.Bits.TxEn       = CH2CCC_CR_DDRCRPINSUSED_TxEn_MAX;  // @todo_adl save power especially on LPDDR4 by turning off unused CCC buffers.
    }
    CccPinsUsed.Bits.CCCMuxSelect = CccMux;
    CccPinsUsed.Bits.AltMuxLp5DEn = Lpddr5 ? Lp5Descending : 0;
    CccPinsUsed.Bits.PiEn         = 0;
    CccPinsUsed.Bits.PiEnOvrd     = 0;
    CccPinsUsed.Bits.Gear1        = Gear1;
    CccPinsUsed.Bits.Gear4        = Outputs->Gear4;
    CccPinsUsed.Bits.KeepXXClkOn  = KeepXXClkOn;
    CccPinsUsed.Bits.TimerXXClk   = TimerXXClk;
    MrcWriteCR (MrcData, Offset, CccPinsUsed.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "0x%04X: CH%dCCC_DDRCRPINSUSED: 0x%08X\n", Offset, Index, CccPinsUsed.Data);
  } // CCC Instance
  GetSetVal = IoChNotPop;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "IoChNotPop: 0x%x\n", IoChNotPop);
  MrcGetSetNoScope (MrcData, GsmIocChNotPop, WriteCached | PrintValue, &GetSetVal);

  GetSetVal = (Outputs->EccSupport) ?  1 : 0;
  MrcGetSetNoScope (MrcData, GsmIocEccEn, WriteToCache | PrintValue, &GetSetVal);

  MrcFlushRegisterCachedData (MrcData);

  return Status;
}

/**
  This function initializes the Memory Controller Phy.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcDdrIoInit (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcInput          *Inputs;
  MrcDebug          *Debug;
  MrcStatus         Status;
  MrcOutput         *Outputs;
  MrcSaveData       *SaveOutputs;
  MrcChannelOut     *ChannelOut;
  MrcControllerOut  *ControllerOut;
  MrcProfile        Profile;
  MrcVddSelect      Vdd;
  MrcVddSelect      DramVdd;
  MrcDdrType        DdrType;
  INT64             GetSetVal;
  INT64             GetSetDis;
  UINT32            Controller;
  UINT32            EffPullUp;
  UINT32            Voh;
  UINT32            PuDeltaV;
  UINT32            ClkPi;
  UINT32            CtlPi;
  UINT8             Channel;
  UINT8             MaxChannels;
  BOOLEAN           Lpddr;
  BOOLEAN           Lpddr4;
  BOOLEAN           Lpddr5;
  CH0CCC_CR_CTL3_STRUCT CccCtl3;
  M_COMP_PCU_STRUCT CrMCompPcu;
  UINT32            Index;
  UINT32            Offset;
  UINT32            CtlRcompDnCode;

  Inputs            = &MrcData->Inputs;
  Outputs           = &MrcData->Outputs;
  Debug             = &Outputs->Debug;
  SaveOutputs       = &MrcData->Save.Data;
  DdrType           = Outputs->DdrType;
  Status            = mrcSuccess;
  Profile           = Inputs->MemoryProfile;
  Lpddr4            = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5            = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Lpddr             = (Lpddr4 || Lpddr5);
  Vdd               = (Lpddr) ? VDD_1_10 : Outputs->VddVoltage[Profile];
  MaxChannels       = Outputs->MaxChannels;
  GetSetDis         = 0;
  ClkPi = 0;
  CtlPi = 0;


  Outputs->ValidMcBitMask = 0;
  Outputs->ValidChBitMask = 0;
  Outputs->ValidRankMask  = 0;
  Outputs->FirstPopController = MAX_CONTROLLER;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      ControllerOut = &Outputs->Controller[Controller];
      Outputs->ValidMcBitMask |= (1 << Controller);
      Outputs->FirstPopController = MIN ((UINT8) Controller, Outputs->FirstPopController);
      ControllerOut->FirstPopCh = MAX_CHANNEL;
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          ChannelOut = &ControllerOut->Channel[Channel];
          // Initialize ValidChBitMask and ValidRankMask used during all training steps
          // Program various Rank Occupancy / RanksUsed bits.

          // Make sure ddr4_1dpc feature is turned off before programming rank occupancy - it might be on from the previous SAGV point
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccDdr4OneDpc, WriteCached | PrintValue, &GetSetDis);

          ControllerOut->ValidChBitMask |= (1 << Channel);
          ControllerOut->FirstPopCh = MIN (Channel, ControllerOut->FirstPopCh);
          Outputs->ValidChBitMask |= (ControllerOut->ValidChBitMask << (Controller * Outputs->MaxChannels));
          Outputs->ValidRankMask  |= ChannelOut->ValidRankBitMask;
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.C%u: ValidRankBitMask=0x%x, Overall ValidRankMask=0x%x\n", Controller, Channel, ChannelOut->ValidRankBitMask, Outputs->ValidRankMask);
        } // MrcChannelExist
      } // Channel
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u: ValidChBitMask=0x%x\n", Controller, ControllerOut->ValidChBitMask);
    } // MrcControllerExists
  } // Controller
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ValidMcBitMask=0x%x, Overall ValidChBitMask=0x%x\n", Outputs->ValidMcBitMask, Outputs->ValidChBitMask);

  // This comes from lockui calibration. Set to 2.5 UI by default.
  SaveOutputs->UiLock = 1;


  if (Lpddr && Inputs->LpFreqSwitch) {
    // Do calibration flow for LowFreq first, and save the calibration results.
    Outputs->Frequency = Outputs->LowFrequency;
    Outputs->Gear2 = Lpddr5 ? 1 : 0; // LowFrequency Gear: Lpddr4 - Gear1, Lpddr5 - Gear2.
    Outputs->Gear4 = 0;
    MrcSetGear (MrcData);
    Status = McFrequencySet (MrcData, FALSE, TRUE);

    Status = MrcPhyInitSeq1 (MrcData, &ClkPi, &CtlPi);
    if (Status == mrcUnmatchedRxNotPossible) {
      // Switch to Matched Rx mode and repeat Seq1
      Outputs->RxMode = (Outputs->RxMode == MrcRxModeUnmatchedN) ? MrcRxModeMatchedN : MrcRxModeMatchedP;
      Status = MrcPhyInitSeq1 (MrcData, &ClkPi, &CtlPi);
    }
    if (Status != mrcSuccess) {
      return Status;
    }
    MrcLpFreqCalibrationSave (MrcData); // Save calibration results for Low Freq

    // @todo_adl Do we need this here ?
    for (Index = (sizeof (CompParamList) / sizeof (CompParamList[0])); Index > 0; Index--) {
      UpdateCompTargetValue (MrcData, CompParamList[Index - 1], Outputs->RcompTarget, TRUE);
    }

    Outputs->Frequency = Outputs->HighFrequency;
    Outputs->Gear2 = (Outputs->HighGear == MrcGear2);
    Outputs->Gear4 = (Outputs->HighGear == MrcGear4);
    MrcSetGear (MrcData);
    Status = McFrequencySet (MrcData, FALSE, TRUE);
  }

  Status = MrcPhyInitSeq1 (MrcData, &ClkPi, &CtlPi);
  if (Status == mrcUnmatchedRxNotPossible) {
    // Switch to Matched Rx mode and repeat Seq1
    Outputs->RxMode = (Outputs->RxMode == MrcRxModeUnmatchedN) ? MrcRxModeMatchedN : MrcRxModeMatchedP;
    Status = MrcPhyInitSeq1 (MrcData, &ClkPi, &CtlPi);
  }
  if (Status != mrcSuccess) {
    return Status;
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcPhyInitSeq1 Done\n");

  Status = MrcPhyInitSeq2 (MrcData, ClkPi, CtlPi);
  if (Status != mrcSuccess) {
    return Status;
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcPhyInitSeq2 Done\n");

  if (Lpddr && Inputs->LpFreqSwitch) {
    MrcLpFreqCalibrationSave (MrcData); // Save calibration results for High Freq
  }


  // Select the DDRIO ODT Mode. This also programs default RxVref.
  // @todo_adl --------------------------------->  MrcSetIoOdtMode (MrcData, Outputs->OdtMode);

  // Configure Strobe
  MrcSetWritePreamble (MrcData);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      // Rx Vref Config for LPDDR
      if (Lpddr) {
        DramVdd = Outputs->VddVoltage[Profile];
        // @todo: This is tied to the Pull Up Calibration selected in LPDDR4 MR.
        Voh =  ((Lpddr5) ? DramVdd / 2 : ((Outputs->Lp4x) ? 360 : 440));  // mV
        PuDeltaV = DramVdd - Voh;
        EffPullUp = PuDeltaV * Outputs->RcompTarget[RdOdt];
        EffPullUp = DIVIDEROUND (EffPullUp, Voh);
        MrcSetIdealRxVref (MrcData, Controller, Channel, EffPullUp, 40, Outputs->RcompTarget[RdOdt], TRUE);
      }
    } // Channel
  } // Controller


  // Initial RxVref (LP4/5 done above)
  if (!Lpddr) {
    MrcSetDefaultRxVref (MrcData, MRC_PRINTS_ON, TRUE);
  }

  CrMCompPcu.Data = 0;
  CrMCompPcu.Bits.COMP_DISABLE  = 1;          // Disable Periodic RComp
  CrMCompPcu.Bits.COMP_INTERVAL = COMP_INT;   // Set periodic comp to happen every 10mS = (10uS * 2^COMP_INT)
  MrcWriteCR (MrcData, M_COMP_PCU_REG, CrMCompPcu.Data);

  // @todo_adl MrcSetupVtt (MrcData, TRUE);

  // Set the DDR voltage in PCU
  MrcSetPcuDdrVoltage (MrcData, Vdd);

  // First RCOMP happens in this function.
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Init COMP Targets\n");
  // Walk backwards here because RdOdt depends on WrDS.
  for (Index = (sizeof (CompParamList) / sizeof (CompParamList[0])); Index > 0; Index--) {
    UpdateCompTargetValue (MrcData, CompParamList[Index - 1], Outputs->RcompTarget, TRUE);
  }

  // Program pghub_cmn_pwrdwncomp according to the CTL Ron down comp code
  MrcGetSetNoScope (MrcData, WrDSCodeDnCtl, ReadUncached, &GetSetVal);
  CtlRcompDnCode = (UINT32) GetSetVal;
  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    Offset = OFFSET_CALC_CH (CH2CCC_CR_CTL3_REG, CH0CCC_CR_CTL3_REG, Index);
    CccCtl3.Data = MrcReadCR (MrcData, Offset);
    CccCtl3.Bits.pghub_cmn_pwrdwncomp = CtlRcompDnCode;
    MrcWriteCR (MrcData, Offset, CccCtl3.Data);
  }

  MrcBlockTrainResetToggle (MrcData, FALSE);
  IoReset (MrcData);
  MrcBlockTrainResetToggle (MrcData, TRUE);

  InitializeRegisterCache (MrcData);    // Phy init uses a mix of direct CR access and GetSet, so invalidate CR cache at the end

  //
  // Differential DCC for CLK, WCK and TxDQS, including Alignment
  //
  RunAllDiffDcc (MrcData);

  return Status;
}

/**
  This function checks for Scomp Training or Bypass mode.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcDdrScomp (
  IN OUT MrcParameters *const MrcData
  )
{
  static const UINT8  ScompParam[4] = {SCompDq, SCompCmd, SCompCtl, SCompClk};
  MrcInput           *Inputs;
  MrcStatus          Status;
  MrcOutput          *Outputs;
  UINT8              ScompIndex;
  UINT8              ScompInitMax;
  BOOLEAN            Lpddr;
  UINT8              ScompBypassBitMask;

  Inputs             = &MrcData->Inputs;
  Outputs            = &MrcData->Outputs;
  Lpddr              = Outputs->Lpddr;
  ScompInitMax       = ARRAY_COUNT (ScompParam);
  Status             = mrcSuccess;
  ScompBypassBitMask = 0;

  for (ScompIndex = 0; ScompIndex < ScompInitMax; ScompIndex++) {
    if (Lpddr && Inputs->ScompBypass[ScompIndex]) {
      ScompBypassBitMask |= (1 << ScompIndex);
    }
  }

  // Skip MrcDdrScompInit if all ScompBypass is set
  if (ScompBypassBitMask < 0xF) {
    Status = MrcDdrScompInit(MrcData, ScompBypassBitMask);
  }
  MrcDdrScompBypass(MrcData, ScompBypassBitMask);

  return Status;
}

/**
This function bypass the Slew Rate Delay Cells of the give Scomp Type.

  @param[in, out] MrcData    - Include all MRC global data.
  @param[in]      ScompIndex - Scomp Type

**/
VOID
MrcDdrScompBypass (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8         ScompBypassBitMask
  )
{
  /* @todo_adl
  static const UINT8  ScompParam[4]  = { SCompDq, SCompCmd, SCompCtl, SCompClk };
  static const GSM_GT SCompBypass[4] = { SCompBypassDq, SCompBypassCmd, SCompBypassCtl, SCompBypassClk };
  static const GSM_GT SCompCells[4]  = {TxSlewRate, CmdSlewRate, CtlSlewRate, ClkSlewRate};
  static const GSM_GT SCompCode[4]   = { SCompCodeDq, SCompCodeCmd, SCompCodeCtl, SCompCodeClk };
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  UINT32            Offset;
  UINT32            Data;
  UINT32            Index;
  UINT32            DdrCaSlwDlyBypass;
  UINT8             ScompIndex;
  UINT8             ScompInitMax;
  INT64             GetSetVal;
  BOOLEAN           Lpddr;
  DDRPHY_COMP_CR_DDRCRCOMPOVR0_STRUCT  DdrCrCompOvr;
  DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT DataControl2;
  CH0CCC_CR_DDRCRPINSUSED_STRUCT       CccPinsUsed;
  DATA0CH0_CR_RCOMPDATA0_STRUCT        CompData0;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  ScompInitMax = ARRAY_COUNT(ScompParam);
  DataControl2.Data = MrcReadCR (MrcData, DATA_CR_DDRCRDATACONTROL2_REG);
  Lpddr = Outputs->Lpddr;
  DdrCrCompOvr.Data = MrcReadCR(MrcData, DDRPHY_COMP_CR_DDRCRCOMPOVR0_REG);

  CompData0.Data = ReadFirstRcompReg (MrcData);

  for (ScompIndex = 0; ScompIndex < ScompInitMax; ScompIndex++) {
    if ((ScompBypassBitMask >> ScompIndex) & 1) {
      // Set Bypass for the given Scomp Type
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Bypass %s\n", CompGlobalOffsetParamStr[ScompParam[ScompIndex]]);
      GetSetVal = 1;
      if (SCompBypass[ScompIndex] == SCompBypassDq) {
        MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, SCompBypass[ScompIndex], WriteToCache, &GetSetVal);
      } else {
        MrcGetSetCcc (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MRC_IGNORE_ARG, MAX_SYS_CHANNEL, SCompBypass[ScompIndex], WriteCached, &GetSetVal);
      }
      // Set SComp Override
      if (DataControl2.Bits.DqSlewDlyByPass || Lpddr) {
        DdrCrCompOvr.Bits.DqSR = 1;
      }
      GetSetVal = 0;
      MrcGetSetNoScope (MrcData, SCompCode[ScompIndex], WriteCached, &GetSetVal);
      // Set Scomp Cells to 0
      MrcGetSetNoScope (MrcData, SCompCells[ScompIndex], WriteCached, &GetSetVal);
    }
  }
  if (DataControl2.Bits.DqSlewDlyByPass) {
    CompData0.Bits.SlewRateComp = 0;
  }
  MrcWriteCrMulticast (MrcData, DATA_CR_RCOMPDATA0_REG, CompData0.Data);
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPOVR0_REG, DdrCrCompOvr.Data);

  DdrCaSlwDlyBypass = (Outputs->DdrType != MRC_DDR_TYPE_DDR4);
  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    Offset = OFFSET_CALC_CH (CH0CCC_CR_DDRCRPINSUSED_REG, CH1CCC_CR_DDRCRPINSUSED_REG, Index);
    CccPinsUsed.Data = MrcReadCR (MrcData, Offset);
    CccPinsUsed.Bits.DdrClkSlwDlyBypass = 1;
    CccPinsUsed.Bits.DdrCtlSlwDlyBypass = 1;//(Outputs->DdrType == MRC_DDR_TYPE_DDR5);
    CccPinsUsed.Bits.DdrCaSlwDlyBypass = DdrCaSlwDlyBypass;
    Data = CccPinsUsed.Data;
    MrcWriteCR (MrcData, Offset, Data);
  }
  */
}

/**
  This function initializes the Slew Rate of DQ, CMD, CTL and CLK buffers in DDRIO.

  @param[in, out] MrcData    - Include all MRC global data.
  @param[in]      ScompIndex - Scomp Type

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcDdrScompInit (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8         ScompBypassBitMask
  )
{
  static const GSM_GT SCompCells[4]           = {TxSlewRate, CmdSlewRate, CtlSlewRate, ClkSlewRate};
  static const GSM_GT SCompPC[4]              = {DqScompPC, CmdScompPC, CtlScompPC, ClkScompPC};
  static const GSM_GT SCompCode[4]            = {SCompCodeDq, SCompCodeCmd, SCompCodeCtl, SCompCodeClk};
  //static const UINT8  DesiredSlewRateUDdr4[4] = {45, 32, 30, 45};  // units of [mV/ps] and multiplied by 10 for integer math precision in DesiredBufferSegmentDelay calculation below.
  static const UINT8  DesiredSlewRateUDdr4[4] = {45, 35, 35, 40};  // units of [mV/ps] and multiplied by 10 for integer math precision in DesiredBufferSegmentDelay calculation below.
  static const UINT8  DesiredSlewRateHDdr4[4] = {45, 30, 30, 50};  // units of [mV/ps] and multiplied by 10 for integer math precision in DesiredBufferSegmentDelay calculation below.
  static const UINT8  DesiredSlewRateLpdr4[4] = {55, 35, 35, 45};  // units of [mV/ps] and multiplied by 10 for integer math precision in DesiredBufferSegmentDelay calculation below.
  static const UINT8  RcompParam[4]           = {WrDS, WrDSCmd, WrDSCtl, WrDSClk};
  static const UINT8  ScompParam[4]           = {SCompDq, SCompCmd, SCompCtl, SCompClk};
#ifdef MRC_DEBUG_PRINT
  static const char   *ScompHeader[4]         = { "Dq", "Cmd", "Ctl", "Clk" };
#endif
  MrcInput          *Inputs;
  MrcDebug          *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput         *Outputs;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *ChannelOut;
  UINT8 const       *DesiredSlewRate;
  MrcVddSelect      Vdd;
  INT64             GetSetVal;
  UINT32            OdtWr;
  UINT32            OdtNom;
  UINT32            DelayCells;
  UINT32            DesiredBufferSegmentDelay[4];
  UINT32            MinChainLength;
  UINT32            MaxChainLength;
  UINT32            VSwing;
  UINT32            VHigh;
  UINT32            VLow;
  UINT32            Voh;
  UINT32            Vtt;
  UINT32            VccIo;
  UINT32            Rtt;
  UINT32            Divisor;
  UINT32            Dividend;
  UINT32            OdtValue;
  UINT32            OdtPark;
  UINT32            Data32;
  UINT32            Controller;
  UINT16            CpuRon;
  UINT8             ScompIndex;
  UINT8             ScompInitMax;
  UINT8             CellIndex;
  UINT8             Max;
  UINT8             Channel;
  UINT8             Rank;
  UINT8             Dimm;
  UINT8             SCompResult[4][SCOMP_CELLS_MAX + 1];  // @todo_adl find the correct definition
  UINT8             SCompCount[4];
  UINT8             NCells[4][MAX_EDGES];
  UINT8             ChannelsPopulated;
  UINT8             DimmSerialResistor;
  UINT8             SegmentsPerBuffer;
  BOOLEAN           Ddr4;
  BOOLEAN           Lpddr4;
  BOOLEAN           Is2DPC;
  BOOLEAN           Is2RDimm;
// @todo_adl DDRPHY_COMP_CR_DDRCRCOMPOVR0_STRUCT DdrCrCompOvr;

  Inputs            = &MrcData->Inputs;
  MrcCall           = Inputs->Call.Func;
  Outputs           = &MrcData->Outputs;
  ControllerOut     = &Outputs->Controller[0];
  Debug             = &Outputs->Debug;
  Ddr4              = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Lpddr4            = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Vdd               = Outputs->VddVoltage[Inputs->MemoryProfile];
  VccIo             = Inputs->VccIomV;
  ScompInitMax      = ARRAY_COUNT (ScompParam);
  Dividend          = 0;

  // Determine slew rate targets according to CPU Sku and DDR type
  if (Inputs->UlxUlt) {
    if (Lpddr4) {
      DesiredSlewRate = DesiredSlewRateLpdr4;
    } else {
      DesiredSlewRate = DesiredSlewRateUDdr4;
    }
  } else {
    // DT/Halo
    DesiredSlewRate = DesiredSlewRateHDdr4;
  }

  for (ScompIndex = 0; ScompIndex < ScompInitMax; ScompIndex++) {
    if (((ScompBypassBitMask >> ScompIndex) & 1) != 1) {
      // Setup of DQ, CMD, CTL, CLK SCompPC
      GetSetVal = 1; // Cycle lock
      MrcGetSetNoScope(MrcData, SCompPC[ScompIndex], WriteCached, &GetSetVal);
    }
  }

  MrcCall->MrcSetMem (SCompResult[0], sizeof (SCompResult), 0);
  MrcCall->MrcSetMem (SCompCount, sizeof (SCompCount), 0);
  MrcCall->MrcSetMem ((UINT8 *) NCells, sizeof (NCells), 0);
  // Gather results for 3-15 ScompCells
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\tScompCode\n\t");
  for (ScompIndex = 0; ScompIndex < ScompInitMax; ScompIndex++) {
    if (((ScompBypassBitMask >> ScompIndex) & 1) != 1) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%s", ScompHeader[ScompIndex]);
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nScompCells\n");
  for (CellIndex = SCOMP_CELLS_MIN; CellIndex <= SCOMP_CELLS_MAX; CellIndex++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%d\t", CellIndex);
    for (ScompIndex = 0; ScompIndex < ScompInitMax; ScompIndex++) {
      if (((ScompBypassBitMask >> ScompIndex) & 1) != 1) {
        // Setup of DQ, CMD, CTL, CLK ScompCells
        GetSetVal = CellIndex;
        MrcGetSetNoScope (MrcData, SCompCells[ScompIndex], WriteCached, &GetSetVal);
      }
    }

    // Force Comp Cycle
    ForceRcomp (MrcData);

    // Read the SCOMP results
    for (ScompIndex = 0; ScompIndex < ScompInitMax; ScompIndex++) {
      if (((ScompBypassBitMask >> ScompIndex) & 1) != 1) {
        MrcGetSetNoScope (MrcData, SCompCode[ScompIndex], ReadUncached, &GetSetVal);
        SCompResult[ScompIndex][CellIndex] = (UINT8) GetSetVal;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", SCompResult[ScompIndex][CellIndex]);
      }
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  }

// @todo_adl  DdrCrCompOvr.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPOVR0_REG);

  // Compare results
  for (ScompIndex = 0; ScompIndex < ScompInitMax; ScompIndex++) {
    if (((ScompBypassBitMask >> ScompIndex) & 1) != 1) {
      if (ScompParam[ScompIndex] != SCompDq) {
        Max = 62;
      } else {
        Max = 29;
      }
      // DQ, CMD, CTL, CLK ScompCells
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s ", CompGlobalOffsetParamStr[ScompParam[ScompIndex]]);
      if (SCompResult[ScompIndex][SCOMP_CELLS_MIN] <= 1) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Setting Slew Rate Compensation Override\n");
// @todo_adl        DdrCrCompOvr.Data |= (1 << (ScompIndex + 10));
      } else {
        for (CellIndex = SCOMP_CELLS_MIN; CellIndex <= SCOMP_CELLS_MAX; CellIndex++) {
          // Find Minimum usable chain length with unsaturated COMP code value, and at least 2 ticks of margin.
          if ((NCells[ScompIndex][0] == 0) && (SCompResult[ScompIndex][CellIndex] >= 2) && (SCompResult[ScompIndex][CellIndex] < Max)) {
            NCells[ScompIndex][0] = CellIndex;
          }
          if (SCompResult[ScompIndex][CellIndex] > Max) {
            //Exceeds upper limit.
            continue;
          }
          NCells[ScompIndex][1] = CellIndex;
          if (CellIndex == SCOMP_CELLS_MIN) {
            // Skip first index for determining Harmonic lock.
            continue;
          }
          if (SCompResult[ScompIndex][CellIndex] > SCompResult[ScompIndex][CellIndex - 1]) {
            // Harmonic lock occurred.
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Harmonic Lock Occured - ");
            NCells[ScompIndex][1] = CellIndex - 1;
            break;
          }
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SCompCellsMin = %d, SCompCellsMax = %d\n", NCells[ScompIndex][0], NCells[ScompIndex][1]);
      }
    }
  }
// @todo_adl  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPOVR0_REG, DdrCrCompOvr.Data);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nScompType\tVSwing\tBufferSegDelay\tSCompPC \tSCompCells\n");
  for (ScompIndex = 0; ScompIndex < ScompInitMax; ScompIndex++) {
    if (((ScompBypassBitMask >> ScompIndex) & 1) != 1) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %s\t", CompGlobalOffsetParamStr[ScompParam[ScompIndex]]);
      // Determine Vtt and Voh based on partition being calculated, Dram Type, and source mux
      if (ScompParam[ScompIndex] == SCompDq) {
        Voh = (Lpddr4 && Inputs->LowSupplyEnData) ? VccIo : Vdd;
        Vtt = (Lpddr4) ? 0 : Vdd;
        SegmentsPerBuffer = 2;
      } else {
        Voh = (Lpddr4 && Inputs->LowSupplyEnCcc && (ScompParam[ScompIndex] != SCompCtl)) ? VccIo : Vdd;
        Vtt = (Lpddr4) ? 0 : Vdd / 2;
        SegmentsPerBuffer = (ScompParam[ScompIndex] == SCompClk) ? 2 : 3;
      }
      if ((NCells[ScompIndex][0] == 0) && (NCells[ScompIndex][1] == 0)) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Slew Rate Compensation Override Set\n");
        GetSetVal = 0;
      } else {
        VSwing = 0;
        MinChainLength = MAX (SCOMP_CELLS_MIN, NCells[ScompIndex][0]);
        MaxChainLength = MIN (SCOMP_CELLS_MAX, NCells[ScompIndex][1]);
        ChannelsPopulated = 0;
        CpuRon = Outputs->RcompTarget[RcompParam[ScompIndex]];
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (MrcChannelExist (MrcData, Controller, Channel)) {
              ChannelOut = &ControllerOut->Channel[Channel];
              ChannelsPopulated++;
              Is2DPC =  (ChannelOut->DimmCount == 2);
              DimmSerialResistor = 15;
              OdtValue = 0;
              if (ScompParam[ScompIndex] == SCompDq) {
                if (!Outputs->DramDqOdtEn) {
                  VSwing += Voh;
                  continue;
                }
              }
              for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank += MAX_RANK_IN_DIMM) {
                if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
                  Dimm = RANK_TO_DIMM_NUMBER (Rank);
                  Is2RDimm = (ChannelOut->Dimm[Dimm].RankInDimm == 1);
                  if ((DimmSerialResistor != 0) && Outputs->Lpddr && (ChannelOut->Dimm[Dimm].ModuleType == NonDimmMemoryPackage)) {
                    DimmSerialResistor = 0;
                  }
                  switch (ScompParam[ScompIndex]) {
                    case SCompDq:
                      OdtWr = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtWr, FALSE, 0, TRUE);
                      if (Ddr4) {
                        OdtNom = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtNom, FALSE, 0, TRUE);
                        OdtPark = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtPark, FALSE, 0, TRUE);
                        OdtPark = (Is2RDimm) ? OdtPark : 0xFFFF;
                        if (Is2DPC) {
                          // OdtValue = OdtWr || (1st Dimm is 2 rank) ? OdtPark : 0xFFFF) || OdtNom || (2nd Dimm is 2 rank) ? OdtPark : 0xFFFF
                          if (Dimm == 0) {
                            Dividend = OdtWr * OdtPark;
                            Divisor = OdtWr + OdtPark;
                            OdtValue = DIVIDEROUND (Dividend, Divisor);
                          } else {
                            Dividend = OdtNom * OdtPark;
                            Divisor = OdtNom + OdtPark;
                            Data32 = DIVIDEROUND (Dividend, Divisor);
                            Dividend = Data32 * OdtValue;
                            Divisor = Data32 + OdtValue;
                            OdtValue = DIVIDEROUND (Dividend, Divisor);
                          }
                        } else {
                          // OdtValue = OdtWr || (Dimm is 2 rank) ? OdtPark : 0xFFFF
                          Dividend = OdtWr * OdtPark;
                          Divisor = OdtWr + OdtPark;
                          OdtValue = DIVIDEROUND (Dividend, Divisor);
                        }
                      } else { // Lpddr3 || Lpddr4
                        OdtValue = OdtWr;
                      }
                      break;

                    case SCompCmd:
                      if (Ddr4) {
                        if (Is2DPC) {
                          Dividend = 55 * 55;
                          Divisor = 55 + 55;
                          OdtValue = DIVIDEROUND (Dividend, Divisor);
                        } else {
                          OdtValue = 55;
                        }
                      } else { // Lpddr4
                        OdtValue = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtCA, FALSE, 0, TRUE);
                      }
                      break;

                    case SCompCtl:
                      if (Ddr4) {
                        if (Is2DPC) {
                          Dividend = 55 * 55;
                          Divisor = 55 + 55;
                          OdtValue = DIVIDEROUND (Dividend, Divisor);
                        } else {
                          OdtValue = 55;
                        }
                      } else { // Lpddr4
                        OdtValue = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtCA, FALSE, 0, TRUE);
                      }
                      break;

                    case SCompClk:
                      if (Ddr4) {
                        if (Is2DPC) {
                          Dividend = 93 * 93;
                          Divisor = 93 + 93;
                          OdtValue = DIVIDEROUND (Dividend, Divisor);
                        } else {
                          OdtValue = 93;
                        }
                      } else { // Lpddr4
                        OdtValue = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtCA, FALSE, 0, TRUE);
                      }
                      break;

                    default:
                      break;
                  } // Switch ScompType
                } // RankExist
              } // For Rank
              Rtt = DimmSerialResistor + OdtValue;
              Divisor = Rtt + CpuRon;
              if (Divisor == 0) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n%s - ODT, CPU Impedance and Dimm Resistor values are zero\n", gErrString, CompGlobalOffsetParamStr[ScompParam[ScompIndex]]);
                return mrcFail;
              }
              //VHigh = Vtt + ((Voh - Vtt) * Rtt / (CpuRon + Rtt));
              //VLow = Vtt * (CpuRon / (CpuRon + Rtt));
              VHigh = Vtt + (((Voh - Vtt) * Rtt) / (Divisor));
              VLow = (Vtt * CpuRon) / Divisor;
              VSwing += (VHigh - VLow);
            } // ChannelExist
          } // For Channel
        } // Controller
        VSwing /= (ChannelsPopulated == 0) ? 1 : ChannelsPopulated;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", VSwing);

        // Derating value is 0.8, but because DesiredSlewRate is multiplied by 10 for precision, value of 8 is used here.
        DesiredBufferSegmentDelay[ScompIndex] = (VSwing * 8) / (DesiredSlewRate[ScompIndex] * SegmentsPerBuffer);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t\t", DesiredBufferSegmentDelay[ScompIndex]);
        if (DesiredBufferSegmentDelay[ScompIndex] == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n%s BufferSegDelay value is zero\n", gErrString);
          return mrcFail;
        }
        DelayCells = DIVIDEROUND (Outputs->Qclkps, DesiredBufferSegmentDelay[ScompIndex]);
        if (DelayCells > (MaxChainLength + 1)) {
          // Calculated value is larger than maximum chain length.
          DelayCells = DIVIDEROUND (Outputs->Qclkps, 2 * DesiredBufferSegmentDelay[ScompIndex]);
          GetSetVal = 0; // Phase Lock
        } else {
          GetSetVal = 1; // Cycle Lock
        }

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\t\t", (GetSetVal == 0) ? "Phase" : "Cycle");
        MrcGetSetNoScope (MrcData, SCompPC[ScompIndex], WriteCached, &GetSetVal);
        DelayCells = RANGE (DelayCells - 1, MinChainLength, MaxChainLength);
        GetSetVal = DelayCells;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\n", DelayCells);
      }
      MrcGetSetNoScope (MrcData, SCompCells[ScompIndex], WriteCached, &GetSetVal);
    }
  }
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");

  return mrcSuccess;
}

/**
  CPGC cannot handle interleaved completions, so we need to disable those during training.
  Use pcode mailbox to control mc_rch_stall_phase.dis_all_cpl_interleave bit

  @param[in] MrcData - Include all MRC global data.
  @param[in] Value   - Set mc_rch_stall_phase.dis_all_cpl_interleave to this value (0 or 1)
**/
VOID
MrcSetMcCplInterleave (
  IN MrcParameters *const MrcData,
  IN UINT32         const Value
  )
{
  MrcStatus                      Status;
  const MRC_FUNCTION             *MrcCall;
  const MrcInput                 *Inputs;
  MrcDebug                       *Debug;
  MrcOutput                      *Outputs;
  UINT32                         Controller;
  UINT32                         MailboxCommand;
  UINT32                         MailboxData;
  UINT32                         MailboxStatus;
  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status  = mrcSuccess;
  MailboxData = 0;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (!MrcControllerExist (MrcData, Controller)) {
      continue;
    }


      MailboxCommand = CPU_MAILBOX_CMD (CPU_MAILBOX_BIOS_CMD_MRC_CR_INTERFACE, CPU_MRC_CR_INTERFACE_SUBCMD_READ_RCH_STALL_PHASE, Controller);
      MrcCall->MrcCpuMailboxRead (MAILBOX_TYPE_PCODE, MailboxCommand, &MailboxData, &MailboxStatus);
      if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s_RCH_STALL_PHASE: Status=0x%x, Data=0x%x\n", "READ", MailboxStatus, MailboxData);
        Status = mrcFail;
      } else {
        // mc_rch_stall_phase.dis_all_cpl_interleave is bit [3]
        MailboxData &= (~MRC_BIT3);
        MailboxData |= ((Value & 1) << 3);
        MailboxCommand = CPU_MAILBOX_CMD (CPU_MAILBOX_BIOS_CMD_MRC_CR_INTERFACE, CPU_MRC_CR_INTERFACE_SUBCMD_WRITE_RCH_STALL_PHASE, Controller);
        MrcCall->MrcCpuMailboxWrite (MAILBOX_TYPE_PCODE, MailboxCommand, MailboxData, &MailboxStatus);
        if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s_RCH_STALL_PHASE: Status=0x%x, Data=0x%x\n", "WRITE", MailboxStatus, MailboxData);
          Status = mrcFail;
        }
      }
  }
  if (Status != mrcSuccess) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Cannot set dis_all_cpl_interleave!\n", gErrString);
  }
}

/**
  This function initializes all the necessary registers for basic training by
  Activating and initializing CPGC Engine
  Enables CKE_On for CPGC usage

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcPreTraining (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput         *Outputs;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *ChannelOut;
  MrcDdrType        DdrType;
  UINT32            Channel;
  UINT32            IpChannel;
  UINT32            SubChannel;
  UINT32            ChannelLoopIncrement;
  UINT32            Controller;
  UINT32            Rank;
  UINT32            RankCount;
  UINT32            Offset;
  UINT8             ChannelMask;
  UINT32            MaskShift;
  BOOLEAN           Ddr4;
  BOOLEAN           Ddr5;
  BOOLEAN           Lpddr;
  BOOLEAN           Lpddr5;
  MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_STRUCT           Cpgc2AddrSize;
  MC0_REQ0_CR_CPGC_SEQ_CFG_A_STRUCT               CpgcSeqCfgA;
  MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_STRUCT  CpgcSeqRankL2PMappingA;
  MC0_CR_CPGC2_CREDIT_CFG_STRUCT                  Cpgc20Credits;
  MC0_CR_CPGC2_V_CHICKEN_STRUCT                   Cpgc2Chicken;
  MC0_CH0_CR_CADB_OVERRIDE_STRUCT                 CadbOverride;

  Outputs       = &MrcData->Outputs;
  DdrType       = Outputs->DdrType;
  Ddr4          = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5          = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5        = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Lpddr         = Outputs->Lpddr;


  // CPGC cannot handle interleaved completions, so disable those during training.
  // This will set mc_rch_stall_phase.dis_all_cpl_interleave = 1
  MrcSetMcCplInterleave (MrcData, 1);

  ChannelLoopIncrement = (Lpddr) ? 2 : 1;
  // @todo - Update for 1 TE per controller.
  MrcSetNormalMode (MrcData, FALSE); // Go to CPGC mode

  // Assign channel to its serial number
  // Activate CPGC Engines on populated channels and subchannels.
  CpgcSeqCfgA.Data = 0;

  Cpgc2AddrSize.Data = 0;
  Cpgc2AddrSize.Bits.Block_Size_Max_Bank  = (Ddr4) ? MAX_DDR4_x16_BANKS - 1 : 0;
  Cpgc2AddrSize.Bits.Region_Size_Max_Bank = Cpgc2AddrSize.Bits.Block_Size_Max_Bank;
  Cpgc2AddrSize.Bits.Block_Size_Bits_Row  = 0;
  Cpgc2AddrSize.Bits.Block_Size_Bits_Col  = (Ddr4) ? 4 : 5;
  Cpgc2AddrSize.Bits.Region_Size_Bits_Row = Cpgc2AddrSize.Bits.Block_Size_Bits_Row;
  Cpgc2AddrSize.Bits.Region_Size_Bits_Col = Cpgc2AddrSize.Bits.Block_Size_Bits_Col;
  Cpgc2AddrSize.Bits.REQUEST_DATA_SIZE    = 1;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (!MrcControllerExist (MrcData, Controller)) {
      continue;
    }
    ControllerOut = &Outputs->Controller[Controller];
    Outputs->McChBitMask |= ControllerOut->ValidChBitMask << (Controller * Outputs->MaxChannels);
    CpgcSeqCfgA.Data = 0;

    Offset = OFFSET_CALC_CH (MC0_CR_CPGC2_V_CHICKEN_REG, MC1_CR_CPGC2_V_CHICKEN_REG, Controller);
    Cpgc2Chicken.Data = MrcReadCR (MrcData, Offset);
    Cpgc2Chicken.Bits.RTN_BYPASS_DISABLE = 1;   // Needed because of two CMF instances in ADL
    MrcWriteCR (MrcData, Offset, Cpgc2Chicken.Data);

    for (Channel = 0; Channel < Outputs->MaxChannels; Channel += ChannelLoopIncrement) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelMask = (Lpddr) ? ((ControllerOut->ValidChBitMask >> Channel) & 0x3) : 0x1;
      MaskShift   = (Ddr5) ? (Channel * Outputs->MaxChannels) : Channel;
      ChannelOut  = &ControllerOut->Channel[Channel];
      IpChannel   = LP_IP_CH (Lpddr, Channel);

      Offset = OFFSET_CALC_CH (MC0_CR_CPGC2_CREDIT_CFG_REG, MC1_CR_CPGC2_CREDIT_CFG_REG, Controller);
      Cpgc20Credits.Data = MrcReadCR (MrcData, Offset);
      Cpgc20Credits.Bits.RD_CPL_CREDITS_INIT = 28;
      MrcWriteCR (MrcData, Offset, Cpgc20Credits.Data);
      // Set credits_config_done in a separate register write
      Cpgc20Credits.Bits.CREDITS_CONFIG_DONE = 1;
      MrcWriteCR (MrcData, Offset, Cpgc20Credits.Data);

      CpgcSeqCfgA.Bits.CHANNEL_ASSIGN      = (ChannelMask << MaskShift);
      ChannelOut->CpgcChAssign             = (UINT8) (Ddr5 ? (1 << Channel) : CpgcSeqCfgA.Bits.CHANNEL_ASSIGN);
      CpgcSeqCfgA.Bits.INITIALIZATION_MODE = CPGC20_ACTIVE_MODE;
      CpgcSeqCfgA.Bits.GLOBAL_START_BIND   = MrcData->Save.Data.CpgcGlobalStart;
      CpgcSeqCfgA.Bits.GLOBAL_STOP_BIND    = CpgcSeqCfgA.Bits.GLOBAL_START_BIND;
      Offset = MrcGetCpgcSeqCfgOffset (MrcData, Controller, Channel / ChannelLoopIncrement);
      MrcWriteCR (MrcData, Offset, CpgcSeqCfgA.Data);

      if (Ddr4 || Ddr5) {
        // Setup Bank Logical to Physical mapping
        // This will allow B2B traffic if we use banks 0 and 1 in CPGC tests, which is good for most data tests.
        // Other tests, like CMD or TAT, can set up a different bank mapping.
        MrcGetSetBankSequence (
          MrcData,
          Controller,
          Channel,
          Ddr4BankMap,
          MAX_DDR4_x16_BANKS,
          MRC_SET
        );
      }
      Offset = OFFSET_CALC_MC_CH (
        MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_REG,
        MC1_REQ0_CR_CPGC2_ADDRESS_SIZE_REG, Controller,
        MC0_REQ1_CR_CPGC2_ADDRESS_SIZE_REG, IpChannel);
      MrcWriteCR64 (MrcData, Offset, Cpgc2AddrSize.Data);

      // Initialize L2P Rank mapping for this CPGC request generator.
      // This is needed because we use CPGC Global Start, and in asymmetrical rank populations we could send traffic to non-existent ranks if default rank mapping is used.
      CpgcSeqRankL2PMappingA.Data = 0;
      RankCount = 0;
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          CpgcSeqRankL2PMappingA.Data |= (Rank << (MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_L2P_RANK0_MAPPING_WID * RankCount));
          RankCount++;
        }
      }
      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG,
                MC1_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG, Controller,
                MC0_REQ1_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG, Channel);
      MrcWriteCR (MrcData, Offset, CpgcSeqRankL2PMappingA.Data);

      if (Lpddr5 && Outputs->Gear4) {
        for (SubChannel = 0; SubChannel < MAX_SUB_CHANNEL; SubChannel++) {
          Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_CADB_OVERRIDE_REG, MC1_CH0_CR_CADB_OVERRIDE_REG, Controller, MC0_CH1_CR_CADB_OVERRIDE_REG, (Channel + SubChannel));
          CadbOverride.Data = MrcReadCR64 (MrcData, Offset);
          CadbOverride.Bits.CLOCKRATIO           = 2;
          CadbOverride.Bits.CLOCKRATIO_OVERRIDE  = 1;
          MrcWriteCR64 (MrcData, Offset, CadbOverride.Data);
        }
      }
    } // Channel
  } // Controller
  if (!Lpddr5) { // @todo_adl  LP5 doesn't have CKE pin, and this causes JEDEC Reset issues
    MrcCkeOnProgramming (MrcData);
  }

  return mrcSuccess;
}

/**
  Print non-LP MRs

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcPrintDdrMrs (
  IN MrcParameters *const MrcData
  )
{
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcChannelOut     *ChannelOut;
  UINT32            Channel;
  UINT32            Controller;
  UINT32            Rank;
  UINT32            RankMod2;
  UINT32            MrIndex;

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;

  if (Outputs->Lpddr) {
    return mrcSuccess;
  }
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      // Dump the MR registers for DDR4
      // LPDDR3/4 Jedec Init is done after Early Command Training
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        RankMod2 = Rank % 2;
        for (MrIndex = mrMR0; MrIndex <= mrMR6; MrIndex++) {
          MRC_DEBUG_MSG (
            Debug,
            MSG_LEVEL_NOTE,
            "MrcSetMR%u  Mc %u Channel %u Rank %u = 0x%X\n",
            MrIndex,
            Controller,
            Channel,
            Rank,
            ChannelOut->Dimm[RANK_TO_DIMM_NUMBER (Rank)].Rank[RankMod2].MR[MrIndex]
          );
        }
      } // Rank
    } // Channel
  } // Controller

  return mrcSuccess;
}

/**
  This function initializes all the necessary registers after main training steps but before LCT.

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess

**/
MrcStatus
MrcPostTraining (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput *Outputs;
  MrcTiming *Timing;
  UINT32    Channel;
  UINT32    Controller;

  Outputs = &MrcData->Outputs;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        Timing = &Outputs->Controller[Controller].Channel[Channel].Timing[MrcData->Inputs.MemoryProfile];
        // Update CmdN timing, Round Trip Latency and tXP
        // OldN = 3N (Gear1) or 2N (Gear2), NewN = 1N or 2N
        UpdateCmdNTiming (
          MrcData,
          Controller,
          Channel,
          Outputs->Gear2 ? (2 * 1) : (2 * 2),
          (Timing->NMode == 2) ? 2 : 0
          );
      }
    }
  }

  return mrcSuccess;
}

/**
  Program PCU_CR_DDR_VOLTAGE register.

  @param[in] MrcData    - Include all MRC global data.
  @param[in] VddVoltage - Current DDR voltage.

**/
void
MrcSetPcuDdrVoltage (
  IN OUT MrcParameters *MrcData,
  IN     MrcVddSelect  VddVoltage
  )
{
  MrcOutput                     *Outputs;
  MrcDebug                      *Debug;
  UINT8                         Data8;
  MrcDdrType                    DdrType;
  DDR_VOLTAGE_PCU_STRUCT        DdrVoltage;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  DdrType = Outputs->DdrType;
  Data8   = 0;
  //
  // Pcode expects the value to follow this chart
  //                       // Load the DDR voltage and use it to calculate the offsets for the weights table
  //                       //  The voltage encoding is as follows:
  //                       //  000   : LPDDR5 1.065V upto 5500 MTs.
  //                       //  001   : LPDDR5 1.115v upto 6400 MTs
  //                       //  010   : DDR5   1.1V
  //                       //  110   : LP4    1.115V
  //                       //  111   : DDR4   1.2V
  //
  switch (VddVoltage) {
    case VDD_1_35:
      Data8 = 1;
      break;

    case VDD_1_20:
      Data8 = 3;
      if (DdrType == MRC_DDR_TYPE_DDR4) {
        Data8 = 7;
      }
      break;

    case VDD_1_10:
      if (DdrType == MRC_DDR_TYPE_DDR5) {
        Data8 = 2;
      } else if (DdrType == MRC_DDR_TYPE_LPDDR4) {
        Data8 = 6;
      } else if (DdrType == MRC_DDR_TYPE_LPDDR5) {
        Data8 = (Outputs->Frequency <= f5400) ? 0 : 1;
      }
      break;

    default:
      Data8 = 0;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PCU_CR_DDR_VOLTAGE = 0x%02X\n", Data8);
  DdrVoltage.Data = 0;
  DdrVoltage.Bits.DDR_VOLTAGE = Data8;
  MrcWriteCR (MrcData, DDR_VOLTAGE_PCU_REG, DdrVoltage.Data);
}

/**
  This function sets up the Vtt termination.

  @param[in] MrcData  - Include all MRC global data.
  @param[in] DebugMsg - Whether to display debug messages

**/
void
MrcSetupVtt (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              DebugMsg
  )
{
  /* @todo_adl
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  INT64             GetSetVal;
  UINT32            Channel;
  UINT32            Controller;
  UINT32            Rank;
  UINT32            Itarget;
  UINT32            IcompUp;
  UINT32            IcompDn;
  UINT32            RonDimm;
  UINT32            TotalRon;
  UINT32            CmdTarget;
  UINT32            CaRonDrvDn;
  UINT32            VttTargetV;
  UINT32            Vdd2Mv;
  UINT32            VccIoMv;
  UINT32            IttcompUp;
  UINT32            TempVar1;
  UINT32            TempVar2;
  UINT32            TempVar3;
  UINT32            TempVar4;
  UINT32            PanicV;
  UINT32            PanicCCCPU;
  UINT16            RodtCpu;
  UINT8             TotalDimms;
  BOOLEAN           CaVoltageSel;
  MrcDebugMsgLevel  DebugLevel;
  DDRPHY_COMP_CR_DDRCRCOMPCTL1_STRUCT       CompCtl1;
  DDRPHY_COMP_CR_DDRCRCOMPCTL0_STRUCT       CompCtl0;
  DDRPHY_COMP_CR_VSSHIPANIC_STRUCT          VssHiPanic;
  DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC_STRUCT   CompVttPanic;
  DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC2_STRUCT  CompVttPanic2;
  DDRVTT0_CR_DDRCRVTTGENCONTROL_STRUCT      VttGenControl;

  Inputs              = &MrcData->Inputs;
  Outputs             = &MrcData->Outputs;
  Debug               = &Outputs->Debug;
  TotalRon            = 0;
  TotalDimms          = 0;
  CaRonDrvDn          = Outputs->RcompTarget[WrDSCmd]; // @todo Check the difference between DrvUp and DrvDn
  Vdd2Mv              = Outputs->Vdd2Mv;
  VccIoMv             = Inputs->VccIomV;
  Itarget             = 10; // Up0=Up1=Dn0=Dn1=10mA

  VssHiPanic.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSSHIPANIC_REG);
  CompCtl0.Data   = MrcReadCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPCTL0_REG);
  VttGenControl.Data = MrcReadCR (MrcData, DDRVTT0_CR_DDRCRVTTGENCONTROL_REG); // All VttGenControl are programmed to same in ddrioint
  VttTargetV = (Outputs->DdrType == MRC_DDR_TYPE_DDR5) ? (Vdd2Mv / 2) : 150;
  PanicCCCPU = VssHiPanic.Bits.PanicCCCPU;
  DebugLevel = (DebugMsg) ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
//  GsmMode    = (DebugMsg) ? PrintValue : 0;

  // Calculate RodtCPU
  // @todo: in power training this might vary between different bytes so need to avg
  RodtCpu = Outputs->RcompTarget[RdOdt];
  // Calculate RonDimm Per Dimm (assuming)
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank += 2) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        TotalDimms++;
        TotalRon += CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmRon, TRUE, INIT_DIMM_RON_34, FALSE);
      } // Rank exist
    } // Channel
  } // Controller

  if (TotalDimms == 0) {
    TotalDimms++;
  }
  RonDimm = TotalRon / TotalDimms;

  MRC_DEBUG_MSG (Debug, DebugLevel, "MrcSetupVtt: RodtCpu=%u ohm, RonDimm=%u ohm\n", RodtCpu, RonDimm);
  MRC_DEBUG_MSG (Debug, DebugLevel, "Itarget=%u uA,\n", Itarget);

  CaVoltageSel    = (CompCtl0.Bits.CaVoltageSelect == 1);

  // Configure VTT Panic Comp Command Ron Vref
  CmdTarget = Outputs->RcompTarget[WrDSCmd];

  // IcompUp and Dn should equal to 10mA
  PanicV = VttGenControl.Bits.Panic0;
  IcompUp = ((VttTargetV - PanicV) / CmdTarget) / (1 + (PanicCCCPU >> 1));

  TempVar1 = CaVoltageSel ? Vdd2Mv : Outputs->VccddqVoltage;
  TempVar2 = TempVar1 - VttTargetV - PanicV;
  IcompDn = TempVar2 / CmdTarget;

  MRC_DEBUG_MSG (Debug, DebugLevel, "IcompDn=%u uA, IcompUp=%u uA\n", IcompDn, IcompUp);

  CompVttPanic.Data = 0;

 //LPDDR Vtt target may be adjusted based on panic count feedback and margin results
  TempVar1 = 191 * (VttTargetV - PanicV);
  //VccIoG is gated version of VccIo
  TempVar2 = DIVIDEROUND (TempVar1, VccIoMv);
  if (TempVar2 > 127) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "Clamping VttPanicUpVref: %d\n",TempVar2);
  }
  CompVttPanic.Bits.VttPanicUpVref = MAX (TempVar2, 127);
  TempVar1 = 191 * (VttTargetV + PanicV);
  TempVar2 = CaVoltageSel ? Vdd2Mv : Outputs->VccddqVoltage;
  CompVttPanic.Bits.VttPanicDnVref = DIVIDEROUND (TempVar1, TempVar2);
  TempVar1 = (VttTargetV - PanicV) / CaRonDrvDn;
  TempVar2 = 1 + (PanicCCCPU >> 1);
  IttcompUp = TempVar1 / TempVar2;
  TempVar3 = (2 * Itarget);
  TempVar4 = DIVIDEROUND (TempVar3, IttcompUp);
  CompVttPanic.Bits.VttPanicCompUp0Mult = TempVar4;
  CompVttPanic.Bits.VttPanicCompUp1Mult = TempVar4;
  TempVar1 = 2 * Itarget;
  TempVar2 = TempVar1 / IcompDn;
  TempVar3 = (1 + VssHiPanic.Bits.PanicPDn2xStep);
  TempVar4 = TempVar2 * TempVar3;
  if (TempVar4 > 15) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "Clamping VttPanicCompDn0Mult: %d\n",TempVar4);
  }
  TempVar4 = RANGE (TempVar4, 0, 15);
  CompVttPanic.Bits.VttPanicCompDn0Mult = TempVar4;
  CompVttPanic.Bits.VttPanicCompDn1Mult = TempVar4;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC_REG, CompVttPanic.Data);

  CompVttPanic2.Data = 0;
  CompVttPanic2.Bits.PanicCCCPU = PanicCCCPU;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC2_REG, CompVttPanic2.Data);

  // GetSet call is to ensure that cache and register are coherent
  GetSetVal = 0;
  MrcGetSetNoScope (MrcData, GsmIocVttPanicCompUpMult, WriteOffsetCached, &GetSetVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocVttPanicCompUpMult, WriteOffsetCached, &GetSetVal);

  CompCtl1.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPCTL1_REG);
  TempVar1 = (350 * 191) / VccIoMv; // Converting mV to ticks
  CompCtl1.Bits.En200ohmVttPncUp = (CompVttPanic.Bits.VttPanicUpVref > TempVar1);
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPCTL1_REG, CompCtl1.Data);

  ForceRcomp (MrcData);
  */
}

