#ifndef __MrcMcRegisterAdlDxxx_h__
#define __MrcMcRegisterAdlDxxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define MC0_CH0_CR_CADB_ACCESS_CONTROL_POLICY_REG                      (0x0000D000)
//Duplicate of MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_REG

#define MC0_CH0_CR_CADB_ACCESS_READ_POLICY_REG                         (0x0000D008)
//Duplicate of MC0_CR_CPGC2_ACCESS_READ_POLICY_REG

#define MC0_CH0_CR_CADB_ACCESS_WRITE_POLICY_REG                        (0x0000D010)
//Duplicate of MC0_CR_CPGC2_ACCESS_WRITE_POLICY_REG

#define MC0_CH0_CR_CADB_CTL_REG                                        (0x0000D018)

  #define MC0_CH0_CR_CADB_CTL_START_TEST_OFF                           ( 0)
  #define MC0_CH0_CR_CADB_CTL_START_TEST_WID                           ( 1)
  #define MC0_CH0_CR_CADB_CTL_START_TEST_MSK                           (0x00000001)
  #define MC0_CH0_CR_CADB_CTL_START_TEST_MIN                           (0)
  #define MC0_CH0_CR_CADB_CTL_START_TEST_MAX                           (1) // 0x00000001
  #define MC0_CH0_CR_CADB_CTL_START_TEST_DEF                           (0x00000000)
  #define MC0_CH0_CR_CADB_CTL_START_TEST_HSH                           (0x0100D018)

  #define MC0_CH0_CR_CADB_CTL_STOP_TEST_OFF                            ( 1)
  #define MC0_CH0_CR_CADB_CTL_STOP_TEST_WID                            ( 1)
  #define MC0_CH0_CR_CADB_CTL_STOP_TEST_MSK                            (0x00000002)
  #define MC0_CH0_CR_CADB_CTL_STOP_TEST_MIN                            (0)
  #define MC0_CH0_CR_CADB_CTL_STOP_TEST_MAX                            (1) // 0x00000001
  #define MC0_CH0_CR_CADB_CTL_STOP_TEST_DEF                            (0x00000000)
  #define MC0_CH0_CR_CADB_CTL_STOP_TEST_HSH                            (0x0102D018)

#define MC0_CH0_CR_CADB_CFG_REG                                        (0x0000D01C)

  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_START_OFF                   ( 0)
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_START_WID                   ( 4)
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_START_MSK                   (0x0000000F)
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_START_MIN                   (0)
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_START_MAX                   (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_START_DEF                   (0x00000000)
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_START_HSH                   (0x0400D01C)

  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_STOP_OFF                    ( 4)
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_STOP_WID                    ( 4)
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_STOP_MSK                    (0x000000F0)
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_STOP_MIN                    (0)
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_STOP_MAX                    (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_STOP_DEF                    (0x00000000)
  #define MC0_CH0_CR_CADB_CFG_CMD_DESELECT_STOP_HSH                    (0x0408D01C)

  #define MC0_CH0_CR_CADB_CFG_LANE_DESELECT_EN_OFF                     ( 8)
  #define MC0_CH0_CR_CADB_CFG_LANE_DESELECT_EN_WID                     ( 3)
  #define MC0_CH0_CR_CADB_CFG_LANE_DESELECT_EN_MSK                     (0x00000700)
  #define MC0_CH0_CR_CADB_CFG_LANE_DESELECT_EN_MIN                     (0)
  #define MC0_CH0_CR_CADB_CFG_LANE_DESELECT_EN_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_CADB_CFG_LANE_DESELECT_EN_DEF                     (0x00000000)
  #define MC0_CH0_CR_CADB_CFG_LANE_DESELECT_EN_HSH                     (0x0310D01C)

  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_EN_OFF                      (11)
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_EN_WID                      ( 1)
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_EN_MSK                      (0x00000800)
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_EN_MIN                      (0)
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_EN_MAX                      (1) // 0x00000001
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_EN_DEF                      (0x00000001)
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_EN_HSH                      (0x0116D01C)

  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_SSEQ_EN_OFF                 (12)
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_SSEQ_EN_WID                 ( 1)
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_SSEQ_EN_MSK                 (0x00001000)
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_SSEQ_EN_MIN                 (0)
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_SSEQ_EN_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_SSEQ_EN_DEF                 (0x00000001)
  #define MC0_CH0_CR_CADB_CFG_INITIAL_DSEL_SSEQ_EN_HSH                 (0x0118D01C)

  #define MC0_CH0_CR_CADB_CFG_CADB_DSEL_THROTTLE_MODE_OFF              (13)
  #define MC0_CH0_CR_CADB_CFG_CADB_DSEL_THROTTLE_MODE_WID              ( 3)
  #define MC0_CH0_CR_CADB_CFG_CADB_DSEL_THROTTLE_MODE_MSK              (0x0000E000)
  #define MC0_CH0_CR_CADB_CFG_CADB_DSEL_THROTTLE_MODE_MIN              (0)
  #define MC0_CH0_CR_CADB_CFG_CADB_DSEL_THROTTLE_MODE_MAX              (7) // 0x00000007
  #define MC0_CH0_CR_CADB_CFG_CADB_DSEL_THROTTLE_MODE_DEF              (0x00000000)
  #define MC0_CH0_CR_CADB_CFG_CADB_DSEL_THROTTLE_MODE_HSH              (0x031AD01C)

  #define MC0_CH0_CR_CADB_CFG_CADB_SEL_THROTTLE_MODE_OFF               (16)
  #define MC0_CH0_CR_CADB_CFG_CADB_SEL_THROTTLE_MODE_WID               ( 3)
  #define MC0_CH0_CR_CADB_CFG_CADB_SEL_THROTTLE_MODE_MSK               (0x00070000)
  #define MC0_CH0_CR_CADB_CFG_CADB_SEL_THROTTLE_MODE_MIN               (0)
  #define MC0_CH0_CR_CADB_CFG_CADB_SEL_THROTTLE_MODE_MAX               (7) // 0x00000007
  #define MC0_CH0_CR_CADB_CFG_CADB_SEL_THROTTLE_MODE_DEF               (0x00000000)
  #define MC0_CH0_CR_CADB_CFG_CADB_SEL_THROTTLE_MODE_HSH               (0x0320D01C)

  #define MC0_CH0_CR_CADB_CFG_CADB_TO_CPGC_BIND_OFF                    (24)
  #define MC0_CH0_CR_CADB_CFG_CADB_TO_CPGC_BIND_WID                    ( 1)
  #define MC0_CH0_CR_CADB_CFG_CADB_TO_CPGC_BIND_MSK                    (0x01000000)
  #define MC0_CH0_CR_CADB_CFG_CADB_TO_CPGC_BIND_MIN                    (0)
  #define MC0_CH0_CR_CADB_CFG_CADB_TO_CPGC_BIND_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_CADB_CFG_CADB_TO_CPGC_BIND_DEF                    (0x00000000)
  #define MC0_CH0_CR_CADB_CFG_CADB_TO_CPGC_BIND_HSH                    (0x0130D01C)

  #define MC0_CH0_CR_CADB_CFG_GLOBAL_START_BIND_OFF                    (25)
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_START_BIND_WID                    ( 1)
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_START_BIND_MSK                    (0x02000000)
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_START_BIND_MIN                    (0)
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_START_BIND_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_START_BIND_DEF                    (0x00000000)
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_START_BIND_HSH                    (0x0132D01C)

  #define MC0_CH0_CR_CADB_CFG_GLOBAL_STOP_BIND_OFF                     (26)
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_STOP_BIND_WID                     ( 1)
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_STOP_BIND_MSK                     (0x04000000)
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_STOP_BIND_MIN                     (0)
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_STOP_BIND_MAX                     (1) // 0x00000001
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_STOP_BIND_DEF                     (0x00000000)
  #define MC0_CH0_CR_CADB_CFG_GLOBAL_STOP_BIND_HSH                     (0x0134D01C)

  #define MC0_CH0_CR_CADB_CFG_CADB_MODE_OFF                            (29)
  #define MC0_CH0_CR_CADB_CFG_CADB_MODE_WID                            ( 3)
  #define MC0_CH0_CR_CADB_CFG_CADB_MODE_MSK                            (0xE0000000)
  #define MC0_CH0_CR_CADB_CFG_CADB_MODE_MIN                            (0)
  #define MC0_CH0_CR_CADB_CFG_CADB_MODE_MAX                            (7) // 0x00000007
  #define MC0_CH0_CR_CADB_CFG_CADB_MODE_DEF                            (0x00000000)
  #define MC0_CH0_CR_CADB_CFG_CADB_MODE_HSH                            (0x033AD01C)

#define MC0_CH0_CR_CADB_DLY_REG                                        (0x0000D020)

  #define MC0_CH0_CR_CADB_DLY_START_DELAY_OFF                          ( 0)
  #define MC0_CH0_CR_CADB_DLY_START_DELAY_WID                          (10)
  #define MC0_CH0_CR_CADB_DLY_START_DELAY_MSK                          (0x000003FF)
  #define MC0_CH0_CR_CADB_DLY_START_DELAY_MIN                          (0)
  #define MC0_CH0_CR_CADB_DLY_START_DELAY_MAX                          (1023) // 0x000003FF
  #define MC0_CH0_CR_CADB_DLY_START_DELAY_DEF                          (0x00000000)
  #define MC0_CH0_CR_CADB_DLY_START_DELAY_HSH                          (0x0A00D020)

  #define MC0_CH0_CR_CADB_DLY_STOP_DELAY_OFF                           (16)
  #define MC0_CH0_CR_CADB_DLY_STOP_DELAY_WID                           (10)
  #define MC0_CH0_CR_CADB_DLY_STOP_DELAY_MSK                           (0x03FF0000)
  #define MC0_CH0_CR_CADB_DLY_STOP_DELAY_MIN                           (0)
  #define MC0_CH0_CR_CADB_DLY_STOP_DELAY_MAX                           (1023) // 0x000003FF
  #define MC0_CH0_CR_CADB_DLY_STOP_DELAY_DEF                           (0x00000000)
  #define MC0_CH0_CR_CADB_DLY_STOP_DELAY_HSH                           (0x0A20D020)

#define MC0_CH0_CR_CADB_STATUS_REG                                     (0x0000D024)

  #define MC0_CH0_CR_CADB_STATUS_MRS_CURR_PTR_OFF                      (21)
  #define MC0_CH0_CR_CADB_STATUS_MRS_CURR_PTR_WID                      ( 3)
  #define MC0_CH0_CR_CADB_STATUS_MRS_CURR_PTR_MSK                      (0x00E00000)
  #define MC0_CH0_CR_CADB_STATUS_MRS_CURR_PTR_MIN                      (0)
  #define MC0_CH0_CR_CADB_STATUS_MRS_CURR_PTR_MAX                      (7) // 0x00000007
  #define MC0_CH0_CR_CADB_STATUS_MRS_CURR_PTR_DEF                      (0x00000000)
  #define MC0_CH0_CR_CADB_STATUS_MRS_CURR_PTR_HSH                      (0x032AD024)

  #define MC0_CH0_CR_CADB_STATUS_TEST_ABORTED_OFF                      (24)
  #define MC0_CH0_CR_CADB_STATUS_TEST_ABORTED_WID                      ( 1)
  #define MC0_CH0_CR_CADB_STATUS_TEST_ABORTED_MSK                      (0x01000000)
  #define MC0_CH0_CR_CADB_STATUS_TEST_ABORTED_MIN                      (0)
  #define MC0_CH0_CR_CADB_STATUS_TEST_ABORTED_MAX                      (1) // 0x00000001
  #define MC0_CH0_CR_CADB_STATUS_TEST_ABORTED_DEF                      (0x00000000)
  #define MC0_CH0_CR_CADB_STATUS_TEST_ABORTED_HSH                      (0x0130D024)

  #define MC0_CH0_CR_CADB_STATUS_ALGO_DONE_OFF                         (28)
  #define MC0_CH0_CR_CADB_STATUS_ALGO_DONE_WID                         ( 1)
  #define MC0_CH0_CR_CADB_STATUS_ALGO_DONE_MSK                         (0x10000000)
  #define MC0_CH0_CR_CADB_STATUS_ALGO_DONE_MIN                         (0)
  #define MC0_CH0_CR_CADB_STATUS_ALGO_DONE_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_CADB_STATUS_ALGO_DONE_DEF                         (0x00000000)
  #define MC0_CH0_CR_CADB_STATUS_ALGO_DONE_HSH                         (0x0138D024)

  #define MC0_CH0_CR_CADB_STATUS_START_TEST_DELAY_BUSY_OFF             (29)
  #define MC0_CH0_CR_CADB_STATUS_START_TEST_DELAY_BUSY_WID             ( 1)
  #define MC0_CH0_CR_CADB_STATUS_START_TEST_DELAY_BUSY_MSK             (0x20000000)
  #define MC0_CH0_CR_CADB_STATUS_START_TEST_DELAY_BUSY_MIN             (0)
  #define MC0_CH0_CR_CADB_STATUS_START_TEST_DELAY_BUSY_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_CADB_STATUS_START_TEST_DELAY_BUSY_DEF             (0x00000000)
  #define MC0_CH0_CR_CADB_STATUS_START_TEST_DELAY_BUSY_HSH             (0x013AD024)

  #define MC0_CH0_CR_CADB_STATUS_TEST_BUSY_OFF                         (30)
  #define MC0_CH0_CR_CADB_STATUS_TEST_BUSY_WID                         ( 1)
  #define MC0_CH0_CR_CADB_STATUS_TEST_BUSY_MSK                         (0x40000000)
  #define MC0_CH0_CR_CADB_STATUS_TEST_BUSY_MIN                         (0)
  #define MC0_CH0_CR_CADB_STATUS_TEST_BUSY_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_CADB_STATUS_TEST_BUSY_DEF                         (0x00000000)
  #define MC0_CH0_CR_CADB_STATUS_TEST_BUSY_HSH                         (0x013CD024)

  #define MC0_CH0_CR_CADB_STATUS_TEST_DONE_OFF                         (31)
  #define MC0_CH0_CR_CADB_STATUS_TEST_DONE_WID                         ( 1)
  #define MC0_CH0_CR_CADB_STATUS_TEST_DONE_MSK                         (0x80000000)
  #define MC0_CH0_CR_CADB_STATUS_TEST_DONE_MIN                         (0)
  #define MC0_CH0_CR_CADB_STATUS_TEST_DONE_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_CADB_STATUS_TEST_DONE_DEF                         (0x00000000)
  #define MC0_CH0_CR_CADB_STATUS_TEST_DONE_HSH                         (0x013ED024)

#define MC0_CH0_CR_CADB_OVERRIDE_REG                                   (0x0000D028)

  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_OVERRIDE_OFF             ( 0)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_OVERRIDE_WID             ( 1)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_OVERRIDE_MSK             (0x00000001)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_OVERRIDE_MIN             (0)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_OVERRIDE_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_OVERRIDE_DEF             (0x00000000)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_OVERRIDE_HSH             (0x4100D028)

  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_OFF                      ( 1)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_WID                      ( 2)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_MSK                      (0x00000006)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_MIN                      (0)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_MAX                      (3) // 0x00000003
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_DEF                      (0x00000000)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_HSH                      (0x4202D028)

  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_OVERRIDE_OFF                ( 4)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_OVERRIDE_WID                ( 1)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_OVERRIDE_MSK                (0x00000010)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_OVERRIDE_MIN                (0)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_OVERRIDE_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_OVERRIDE_DEF                (0x00000000)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_OVERRIDE_HSH                (0x4108D028)

  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_OFF                         ( 5)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_WID                         ( 4)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_MSK                         (0x000001E0)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_MIN                         (0)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_MAX                         (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_DEF                         (0x00000000)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_HSH                         (0x440AD028)

  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_OVERRIDE_OFF                (12)
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_OVERRIDE_WID                ( 1)
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_OVERRIDE_MSK                (0x00001000)
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_OVERRIDE_MIN                (0)
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_OVERRIDE_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_OVERRIDE_DEF                (0x00000000)
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_OVERRIDE_HSH                (0x4118D028)

  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_OFF                         (13)
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_WID                         ( 1)
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_MSK                         (0x00002000)
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_MIN                         (0)
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_DEF                         (0x00000000)
  #define MC0_CH0_CR_CADB_OVERRIDE_DSELWON_HSH                         (0x411AD028)

  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_OVERRIDE_OFF                (14)
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_OVERRIDE_WID                ( 1)
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_OVERRIDE_MSK                (0x00004000)
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_OVERRIDE_MIN                (0)
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_OVERRIDE_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_OVERRIDE_DEF                (0x00000000)
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_OVERRIDE_HSH                (0x411CD028)

  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_OFF                         (15)
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_WID                         ( 1)
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_MSK                         (0x00008000)
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_MIN                         (0)
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_DEF                         (0x00000000)
  #define MC0_CH0_CR_CADB_OVERRIDE_CONTWON_HSH                         (0x411ED028)

  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_STATUS_OFF               (17)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_STATUS_WID               ( 2)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_STATUS_MSK               (0x00060000)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_STATUS_MIN               (0)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_STATUS_MAX               (3) // 0x00000003
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_STATUS_DEF               (0x00000000)
  #define MC0_CH0_CR_CADB_OVERRIDE_CLOCKRATIO_STATUS_HSH               (0x4222D028)

  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_STATUS_OFF                  (21)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_STATUS_WID                  ( 4)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_STATUS_MSK                  (0x01E00000)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_STATUS_MIN                  (0)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_STATUS_MAX                  (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_STATUS_DEF                  (0x00000000)
  #define MC0_CH0_CR_CADB_OVERRIDE_MEMTYPE_STATUS_HSH                  (0x442AD028)

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG                     (0x0000D030)

  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR1_POLY_OFF       ( 0)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR1_POLY_WID       ( 7)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR1_POLY_MSK       (0x0000007F)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR1_POLY_MIN       (0)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR1_POLY_MAX       (127) // 0x0000007F
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR1_POLY_DEF       (0x00000021)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR1_POLY_HSH       (0x4700D030)

  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR2_POLY_OFF       ( 8)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR2_POLY_WID       ( 7)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR2_POLY_MSK       (0x00007F00)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR2_POLY_MIN       (0)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR2_POLY_MAX       (127) // 0x0000007F
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR2_POLY_DEF       (0x00000014)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR2_POLY_HSH       (0x4710D030)

  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR3_POLY_OFF       (16)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR3_POLY_WID       ( 7)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR3_POLY_MSK       (0x007F0000)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR3_POLY_MIN       (0)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR3_POLY_MAX       (127) // 0x0000007F
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR3_POLY_DEF       (0x00000041)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_STAGR3_POLY_HSH       (0x4720D030)

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_CFG_0_REG                          (0x0000D038)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_CFG_1_REG                          (0x0000D039)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_CFG_2_REG                          (0x0000D03A)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_CFG_3_REG                          (0x0000D03B)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG                    (0x0000D03C)

  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_SAVE_LFSR_SEED_RATE_OFF ( 0)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_SAVE_LFSR_SEED_RATE_WID ( 8)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_SAVE_LFSR_SEED_RATE_MSK (0x000000FF)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_SAVE_LFSR_SEED_RATE_MIN (0)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_SAVE_LFSR_SEED_RATE_MAX (255) // 0x000000FF
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_SAVE_LFSR_SEED_RATE_DEF (0x00000000)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_SAVE_LFSR_SEED_RATE_HSH (0x0800D03C)

  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_RELOAD_LFSR_SEED_RATE_OFF ( 8)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_RELOAD_LFSR_SEED_RATE_WID ( 5)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_RELOAD_LFSR_SEED_RATE_MSK (0x00001F00)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_RELOAD_LFSR_SEED_RATE_MIN (0)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_RELOAD_LFSR_SEED_RATE_MAX (31) // 0x0000001F
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_RELOAD_LFSR_SEED_RATE_DEF (0x00000000)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_RELOAD_LFSR_SEED_RATE_HSH (0x0510D03C)

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG                         (0x0000D040)

  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_CMD_OFF                   ( 0)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_CMD_WID                   (32)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_CMD_MSK                   (0xFFFFFFFF)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_CMD_MIN                   (0)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_CMD_MAX                   (4294967295) // 0xFFFFFFFF
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_CMD_DEF                   (0x0000AA55)
  #define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_CMD_HSH                   (0x2000D040)

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_1_REG                         (0x0000D044)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_2_REG                         (0x0000D048)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_3_REG                         (0x0000D04C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_LMN_REG                            (0x0000D050)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_0_REG                         (0x0000D058)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_1_REG                         (0x0000D05C)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_2_REG                         (0x0000D060)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_3_REG                         (0x0000D064)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH0_CR_CADB_BUF_0_REG                                      (0x0000D068)

  #define MC0_CH0_CR_CADB_BUF_0_CS_OFF                                 ( 0)
  #define MC0_CH0_CR_CADB_BUF_0_CS_WID                                 ( 4)
  #define MC0_CH0_CR_CADB_BUF_0_CS_MSK                                 (0x0000000F)
  #define MC0_CH0_CR_CADB_BUF_0_CS_MIN                                 (0)
  #define MC0_CH0_CR_CADB_BUF_0_CS_MAX                                 (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_BUF_0_CS_DEF                                 (0x00000000)
  #define MC0_CH0_CR_CADB_BUF_0_CS_HSH                                 (0x4400D068)

  #define MC0_CH0_CR_CADB_BUF_0_CID_OFF                                ( 4)
  #define MC0_CH0_CR_CADB_BUF_0_CID_WID                                ( 3)
  #define MC0_CH0_CR_CADB_BUF_0_CID_MSK                                (0x00000070)
  #define MC0_CH0_CR_CADB_BUF_0_CID_MIN                                (0)
  #define MC0_CH0_CR_CADB_BUF_0_CID_MAX                                (7) // 0x00000007
  #define MC0_CH0_CR_CADB_BUF_0_CID_DEF                                (0x00000000)
  #define MC0_CH0_CR_CADB_BUF_0_CID_HSH                                (0x4308D068)

  #define MC0_CH0_CR_CADB_BUF_0_ODT_OFF                                ( 7)
  #define MC0_CH0_CR_CADB_BUF_0_ODT_WID                                ( 4)
  #define MC0_CH0_CR_CADB_BUF_0_ODT_MSK                                (0x00000780)
  #define MC0_CH0_CR_CADB_BUF_0_ODT_MIN                                (0)
  #define MC0_CH0_CR_CADB_BUF_0_ODT_MAX                                (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_BUF_0_ODT_DEF                                (0x00000000)
  #define MC0_CH0_CR_CADB_BUF_0_ODT_HSH                                (0x440ED068)

  #define MC0_CH0_CR_CADB_BUF_0_CKE_OFF                                (11)
  #define MC0_CH0_CR_CADB_BUF_0_CKE_WID                                ( 4)
  #define MC0_CH0_CR_CADB_BUF_0_CKE_MSK                                (0x00007800)
  #define MC0_CH0_CR_CADB_BUF_0_CKE_MIN                                (0)
  #define MC0_CH0_CR_CADB_BUF_0_CKE_MAX                                (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_BUF_0_CKE_DEF                                (0x00000000)
  #define MC0_CH0_CR_CADB_BUF_0_CKE_HSH                                (0x4416D068)

  #define MC0_CH0_CR_CADB_BUF_0_USE_HALF_CA_OFF                        (15)
  #define MC0_CH0_CR_CADB_BUF_0_USE_HALF_CA_WID                        ( 1)
  #define MC0_CH0_CR_CADB_BUF_0_USE_HALF_CA_MSK                        (0x00008000)
  #define MC0_CH0_CR_CADB_BUF_0_USE_HALF_CA_MIN                        (0)
  #define MC0_CH0_CR_CADB_BUF_0_USE_HALF_CA_MAX                        (1) // 0x00000001
  #define MC0_CH0_CR_CADB_BUF_0_USE_HALF_CA_DEF                        (0x00000000)
  #define MC0_CH0_CR_CADB_BUF_0_USE_HALF_CA_HSH                        (0x411ED068)

  #define MC0_CH0_CR_CADB_BUF_0_VAL_OFF                                (16)
  #define MC0_CH0_CR_CADB_BUF_0_VAL_WID                                ( 1)
  #define MC0_CH0_CR_CADB_BUF_0_VAL_MSK                                (0x00010000)
  #define MC0_CH0_CR_CADB_BUF_0_VAL_MIN                                (0)
  #define MC0_CH0_CR_CADB_BUF_0_VAL_MAX                                (1) // 0x00000001
  #define MC0_CH0_CR_CADB_BUF_0_VAL_DEF                                (0x00000000)
  #define MC0_CH0_CR_CADB_BUF_0_VAL_HSH                                (0x4120D068)

  #define MC0_CH0_CR_CADB_BUF_0_PAR_OFF                                (17)
  #define MC0_CH0_CR_CADB_BUF_0_PAR_WID                                ( 1)
  #define MC0_CH0_CR_CADB_BUF_0_PAR_MSK                                (0x00020000)
  #define MC0_CH0_CR_CADB_BUF_0_PAR_MIN                                (0)
  #define MC0_CH0_CR_CADB_BUF_0_PAR_MAX                                (1) // 0x00000001
  #define MC0_CH0_CR_CADB_BUF_0_PAR_DEF                                (0x00000000)
  #define MC0_CH0_CR_CADB_BUF_0_PAR_HSH                                (0x4122D068)

  #define MC0_CH0_CR_CADB_BUF_0_CA_OFF                                 (18)
  #define MC0_CH0_CR_CADB_BUF_0_CA_WID                                 (32)
  #define MC0_CH0_CR_CADB_BUF_0_CA_MSK                                 (0x0003FFFFFFFC0000ULL)
  #define MC0_CH0_CR_CADB_BUF_0_CA_MIN                                 (0)
  #define MC0_CH0_CR_CADB_BUF_0_CA_MAX                                 (4294967295) // 0xFFFFFFFF
  #define MC0_CH0_CR_CADB_BUF_0_CA_DEF                                 (0x00000000)
  #define MC0_CH0_CR_CADB_BUF_0_CA_HSH                                 (0x6024D068)

#define MC0_CH0_CR_CADB_BUF_1_REG                                      (0x0000D070)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_2_REG                                      (0x0000D078)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_3_REG                                      (0x0000D080)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_4_REG                                      (0x0000D088)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_5_REG                                      (0x0000D090)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_6_REG                                      (0x0000D098)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_7_REG                                      (0x0000D0A0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_8_REG                                      (0x0000D0A8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_9_REG                                      (0x0000D0B0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_10_REG                                     (0x0000D0B8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_11_REG                                     (0x0000D0C0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_12_REG                                     (0x0000D0C8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_13_REG                                     (0x0000D0D0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_14_REG                                     (0x0000D0D8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_BUF_15_REG                                     (0x0000D0E0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH0_CR_CADB_AO_MRSCFG_REG                                  (0x0000D0E8)

  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_SCALE_OFF                  ( 0)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_SCALE_WID                  ( 1)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_SCALE_MSK                  (0x00000001)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_SCALE_MIN                  (0)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_SCALE_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_SCALE_DEF                  (0x00000000)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_SCALE_HSH                  (0x0100D0E8)

  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_OFF                        ( 1)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_WID                        ( 4)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_MSK                        (0x0000001E)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_MIN                        (0)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_MAX                        (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_DEF                        (0x00000000)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_GAP_HSH                        (0x0402D0E8)

  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_AO_REPEATS_OFF                 ( 8)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_AO_REPEATS_WID                 (16)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_AO_REPEATS_MSK                 (0x00FFFF00)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_AO_REPEATS_MIN                 (0)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_AO_REPEATS_MAX                 (65535) // 0x0000FFFF
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_AO_REPEATS_DEF                 (0x00000000)
  #define MC0_CH0_CR_CADB_AO_MRSCFG_MRS_AO_REPEATS_HSH                 (0x1010D0E8)

#define MC0_CH0_CR_CADB_SELCFG_REG                                     (0x0000D0F0)

  #define MC0_CH0_CR_CADB_SELCFG_WR_SELECT_ENABLE_OFF                  ( 0)
  #define MC0_CH0_CR_CADB_SELCFG_WR_SELECT_ENABLE_WID                  ( 3)
  #define MC0_CH0_CR_CADB_SELCFG_WR_SELECT_ENABLE_MSK                  (0x00000007)
  #define MC0_CH0_CR_CADB_SELCFG_WR_SELECT_ENABLE_MIN                  (0)
  #define MC0_CH0_CR_CADB_SELCFG_WR_SELECT_ENABLE_MAX                  (7) // 0x00000007
  #define MC0_CH0_CR_CADB_SELCFG_WR_SELECT_ENABLE_DEF                  (0x00000000)
  #define MC0_CH0_CR_CADB_SELCFG_WR_SELECT_ENABLE_HSH                  (0x0300D0F0)

  #define MC0_CH0_CR_CADB_SELCFG_RD_SELECT_ENABLE_OFF                  ( 8)
  #define MC0_CH0_CR_CADB_SELCFG_RD_SELECT_ENABLE_WID                  ( 3)
  #define MC0_CH0_CR_CADB_SELCFG_RD_SELECT_ENABLE_MSK                  (0x00000700)
  #define MC0_CH0_CR_CADB_SELCFG_RD_SELECT_ENABLE_MIN                  (0)
  #define MC0_CH0_CR_CADB_SELCFG_RD_SELECT_ENABLE_MAX                  (7) // 0x00000007
  #define MC0_CH0_CR_CADB_SELCFG_RD_SELECT_ENABLE_DEF                  (0x00000000)
  #define MC0_CH0_CR_CADB_SELCFG_RD_SELECT_ENABLE_HSH                  (0x0310D0F0)

  #define MC0_CH0_CR_CADB_SELCFG_ACT_SELECT_ENABLE_OFF                 (16)
  #define MC0_CH0_CR_CADB_SELCFG_ACT_SELECT_ENABLE_WID                 ( 3)
  #define MC0_CH0_CR_CADB_SELCFG_ACT_SELECT_ENABLE_MSK                 (0x00070000)
  #define MC0_CH0_CR_CADB_SELCFG_ACT_SELECT_ENABLE_MIN                 (0)
  #define MC0_CH0_CR_CADB_SELCFG_ACT_SELECT_ENABLE_MAX                 (7) // 0x00000007
  #define MC0_CH0_CR_CADB_SELCFG_ACT_SELECT_ENABLE_DEF                 (0x00000000)
  #define MC0_CH0_CR_CADB_SELCFG_ACT_SELECT_ENABLE_HSH                 (0x0320D0F0)

  #define MC0_CH0_CR_CADB_SELCFG_PRE_SELECT_ENABLE_OFF                 (24)
  #define MC0_CH0_CR_CADB_SELCFG_PRE_SELECT_ENABLE_WID                 ( 3)
  #define MC0_CH0_CR_CADB_SELCFG_PRE_SELECT_ENABLE_MSK                 (0x07000000)
  #define MC0_CH0_CR_CADB_SELCFG_PRE_SELECT_ENABLE_MIN                 (0)
  #define MC0_CH0_CR_CADB_SELCFG_PRE_SELECT_ENABLE_MAX                 (7) // 0x00000007
  #define MC0_CH0_CR_CADB_SELCFG_PRE_SELECT_ENABLE_DEF                 (0x00000000)
  #define MC0_CH0_CR_CADB_SELCFG_PRE_SELECT_ENABLE_HSH                 (0x0330D0F0)

  #define MC0_CH0_CR_CADB_SELCFG_INITIAL_SEL_SSEQ_EN_OFF               (28)
  #define MC0_CH0_CR_CADB_SELCFG_INITIAL_SEL_SSEQ_EN_WID               ( 1)
  #define MC0_CH0_CR_CADB_SELCFG_INITIAL_SEL_SSEQ_EN_MSK               (0x10000000)
  #define MC0_CH0_CR_CADB_SELCFG_INITIAL_SEL_SSEQ_EN_MIN               (0)
  #define MC0_CH0_CR_CADB_SELCFG_INITIAL_SEL_SSEQ_EN_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_CADB_SELCFG_INITIAL_SEL_SSEQ_EN_DEF               (0x00000001)
  #define MC0_CH0_CR_CADB_SELCFG_INITIAL_SEL_SSEQ_EN_HSH               (0x0138D0F0)

#define MC0_CH0_CR_CADB_MRSCFG_REG                                     (0x0000D0F8)

  #define MC0_CH0_CR_CADB_MRSCFG_CS_CLOCKS_OFF                         ( 0)
  #define MC0_CH0_CR_CADB_MRSCFG_CS_CLOCKS_WID                         ( 3)
  #define MC0_CH0_CR_CADB_MRSCFG_CS_CLOCKS_MSK                         (0x00000007)
  #define MC0_CH0_CR_CADB_MRSCFG_CS_CLOCKS_MIN                         (0)
  #define MC0_CH0_CR_CADB_MRSCFG_CS_CLOCKS_MAX                         (7) // 0x00000007
  #define MC0_CH0_CR_CADB_MRSCFG_CS_CLOCKS_DEF                         (0x00000000)
  #define MC0_CH0_CR_CADB_MRSCFG_CS_CLOCKS_HSH                         (0x4300D0F8)

  #define MC0_CH0_CR_CADB_MRSCFG_DSEL_CLOCKS_OFF                       ( 3)
  #define MC0_CH0_CR_CADB_MRSCFG_DSEL_CLOCKS_WID                       ( 4)
  #define MC0_CH0_CR_CADB_MRSCFG_DSEL_CLOCKS_MSK                       (0x00000078)
  #define MC0_CH0_CR_CADB_MRSCFG_DSEL_CLOCKS_MIN                       (0)
  #define MC0_CH0_CR_CADB_MRSCFG_DSEL_CLOCKS_MAX                       (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_MRSCFG_DSEL_CLOCKS_DEF                       (0x00000000)
  #define MC0_CH0_CR_CADB_MRSCFG_DSEL_CLOCKS_HSH                       (0x4406D0F8)

  #define MC0_CH0_CR_CADB_MRSCFG_SETUP_CLOCKS_OFF                      ( 7)
  #define MC0_CH0_CR_CADB_MRSCFG_SETUP_CLOCKS_WID                      ( 4)
  #define MC0_CH0_CR_CADB_MRSCFG_SETUP_CLOCKS_MSK                      (0x00000780)
  #define MC0_CH0_CR_CADB_MRSCFG_SETUP_CLOCKS_MIN                      (0)
  #define MC0_CH0_CR_CADB_MRSCFG_SETUP_CLOCKS_MAX                      (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_MRSCFG_SETUP_CLOCKS_DEF                      (0x00000000)
  #define MC0_CH0_CR_CADB_MRSCFG_SETUP_CLOCKS_HSH                      (0x440ED0F8)

  #define MC0_CH0_CR_CADB_MRSCFG_CS_ACTIVE_POLARITY_OFF                (11)
  #define MC0_CH0_CR_CADB_MRSCFG_CS_ACTIVE_POLARITY_WID                ( 1)
  #define MC0_CH0_CR_CADB_MRSCFG_CS_ACTIVE_POLARITY_MSK                (0x00000800)
  #define MC0_CH0_CR_CADB_MRSCFG_CS_ACTIVE_POLARITY_MIN                (0)
  #define MC0_CH0_CR_CADB_MRSCFG_CS_ACTIVE_POLARITY_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_CADB_MRSCFG_CS_ACTIVE_POLARITY_DEF                (0x00000000)
  #define MC0_CH0_CR_CADB_MRSCFG_CS_ACTIVE_POLARITY_HSH                (0x4116D0F8)

  #define MC0_CH0_CR_CADB_MRSCFG_MRS_START_PTR_OFF                     (12)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_START_PTR_WID                     ( 4)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_START_PTR_MSK                     (0x0000F000)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_START_PTR_MIN                     (0)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_START_PTR_MAX                     (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_START_PTR_DEF                     (0x00000000)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_START_PTR_HSH                     (0x4418D0F8)

  #define MC0_CH0_CR_CADB_MRSCFG_MRS_END_PTR_OFF                       (16)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_END_PTR_WID                       ( 4)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_END_PTR_MSK                       (0x000F0000)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_END_PTR_MIN                       (0)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_END_PTR_MAX                       (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_END_PTR_DEF                       (0x00000000)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_END_PTR_HSH                       (0x4420D0F8)

  #define MC0_CH0_CR_CADB_MRSCFG_MRS_GOTO_PTR_OFF                      (20)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_GOTO_PTR_WID                      ( 4)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_GOTO_PTR_MSK                      (0x00F00000)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_GOTO_PTR_MIN                      (0)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_GOTO_PTR_MAX                      (15) // 0x0000000F
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_GOTO_PTR_DEF                      (0x00000000)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_GOTO_PTR_HSH                      (0x4428D0F8)

  #define MC0_CH0_CR_CADB_MRSCFG_MRS_CS_MODE_OFF                       (24)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_CS_MODE_WID                       ( 2)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_CS_MODE_MSK                       (0x03000000)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_CS_MODE_MIN                       (0)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_CS_MODE_MAX                       (3) // 0x00000003
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_CS_MODE_DEF                       (0x00000000)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_CS_MODE_HSH                       (0x4230D0F8)

  #define MC0_CH0_CR_CADB_MRSCFG_MRS_DELAY_CNT_OFF                     (32)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_DELAY_CNT_WID                     ( 8)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_DELAY_CNT_MSK                     (0x000000FF00000000ULL)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_DELAY_CNT_MIN                     (0)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_DELAY_CNT_MAX                     (255) // 0x000000FF
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_DELAY_CNT_DEF                     (0x00000000)
  #define MC0_CH0_CR_CADB_MRSCFG_MRS_DELAY_CNT_HSH                     (0x4840D0F8)

#define MC0_CH1_CR_CADB_ACCESS_CONTROL_POLICY_REG                      (0x0000D100)
//Duplicate of MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_REG

#define MC0_CH1_CR_CADB_ACCESS_READ_POLICY_REG                         (0x0000D108)
//Duplicate of MC0_CR_CPGC2_ACCESS_READ_POLICY_REG

#define MC0_CH1_CR_CADB_ACCESS_WRITE_POLICY_REG                        (0x0000D110)
//Duplicate of MC0_CR_CPGC2_ACCESS_WRITE_POLICY_REG

#define MC0_CH1_CR_CADB_CTL_REG                                        (0x0000D118)
//Duplicate of MC0_CH0_CR_CADB_CTL_REG

#define MC0_CH1_CR_CADB_CFG_REG                                        (0x0000D11C)
//Duplicate of MC0_CH0_CR_CADB_CFG_REG

#define MC0_CH1_CR_CADB_DLY_REG                                        (0x0000D120)
//Duplicate of MC0_CH0_CR_CADB_DLY_REG

#define MC0_CH1_CR_CADB_STATUS_REG                                     (0x0000D124)
//Duplicate of MC0_CH0_CR_CADB_STATUS_REG

#define MC0_CH1_CR_CADB_OVERRIDE_REG                                   (0x0000D128)
//Duplicate of MC0_CH0_CR_CADB_OVERRIDE_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG                     (0x0000D130)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_CFG_0_REG                          (0x0000D138)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_CFG_1_REG                          (0x0000D139)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_CFG_2_REG                          (0x0000D13A)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_CFG_3_REG                          (0x0000D13B)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG                    (0x0000D13C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_PBUF_0_REG                         (0x0000D140)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_PBUF_1_REG                         (0x0000D144)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_PBUF_2_REG                         (0x0000D148)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_PBUF_3_REG                         (0x0000D14C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_LMN_REG                            (0x0000D150)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_POLY_0_REG                         (0x0000D158)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_POLY_1_REG                         (0x0000D15C)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_POLY_2_REG                         (0x0000D160)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH1_CR_CADB_DSEL_UNISEQ_POLY_3_REG                         (0x0000D164)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH1_CR_CADB_BUF_0_REG                                      (0x0000D168)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_1_REG                                      (0x0000D170)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_2_REG                                      (0x0000D178)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_3_REG                                      (0x0000D180)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_4_REG                                      (0x0000D188)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_5_REG                                      (0x0000D190)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_6_REG                                      (0x0000D198)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_7_REG                                      (0x0000D1A0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_8_REG                                      (0x0000D1A8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_9_REG                                      (0x0000D1B0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_10_REG                                     (0x0000D1B8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_11_REG                                     (0x0000D1C0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_12_REG                                     (0x0000D1C8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_13_REG                                     (0x0000D1D0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_14_REG                                     (0x0000D1D8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_BUF_15_REG                                     (0x0000D1E0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH1_CR_CADB_AO_MRSCFG_REG                                  (0x0000D1E8)
//Duplicate of MC0_CH0_CR_CADB_AO_MRSCFG_REG

#define MC0_CH1_CR_CADB_SELCFG_REG                                     (0x0000D1F0)
//Duplicate of MC0_CH0_CR_CADB_SELCFG_REG

#define MC0_CH1_CR_CADB_MRSCFG_REG                                     (0x0000D1F8)
//Duplicate of MC0_CH0_CR_CADB_MRSCFG_REG

#define MC0_CH2_CR_CADB_ACCESS_CONTROL_POLICY_REG                      (0x0000D200)
//Duplicate of MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_REG

#define MC0_CH2_CR_CADB_ACCESS_READ_POLICY_REG                         (0x0000D208)
//Duplicate of MC0_CR_CPGC2_ACCESS_READ_POLICY_REG

#define MC0_CH2_CR_CADB_ACCESS_WRITE_POLICY_REG                        (0x0000D210)
//Duplicate of MC0_CR_CPGC2_ACCESS_WRITE_POLICY_REG

#define MC0_CH2_CR_CADB_CTL_REG                                        (0x0000D218)
//Duplicate of MC0_CH0_CR_CADB_CTL_REG

#define MC0_CH2_CR_CADB_CFG_REG                                        (0x0000D21C)
//Duplicate of MC0_CH0_CR_CADB_CFG_REG

#define MC0_CH2_CR_CADB_DLY_REG                                        (0x0000D220)
//Duplicate of MC0_CH0_CR_CADB_DLY_REG

#define MC0_CH2_CR_CADB_STATUS_REG                                     (0x0000D224)
//Duplicate of MC0_CH0_CR_CADB_STATUS_REG

#define MC0_CH2_CR_CADB_OVERRIDE_REG                                   (0x0000D228)
//Duplicate of MC0_CH0_CR_CADB_OVERRIDE_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG                     (0x0000D230)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_CFG_0_REG                          (0x0000D238)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_CFG_1_REG                          (0x0000D239)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_CFG_2_REG                          (0x0000D23A)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_CFG_3_REG                          (0x0000D23B)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG                    (0x0000D23C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_PBUF_0_REG                         (0x0000D240)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_PBUF_1_REG                         (0x0000D244)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_PBUF_2_REG                         (0x0000D248)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_PBUF_3_REG                         (0x0000D24C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_LMN_REG                            (0x0000D250)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_POLY_0_REG                         (0x0000D258)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_POLY_1_REG                         (0x0000D25C)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_POLY_2_REG                         (0x0000D260)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH2_CR_CADB_DSEL_UNISEQ_POLY_3_REG                         (0x0000D264)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH2_CR_CADB_BUF_0_REG                                      (0x0000D268)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_1_REG                                      (0x0000D270)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_2_REG                                      (0x0000D278)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_3_REG                                      (0x0000D280)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_4_REG                                      (0x0000D288)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_5_REG                                      (0x0000D290)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_6_REG                                      (0x0000D298)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_7_REG                                      (0x0000D2A0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_8_REG                                      (0x0000D2A8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_9_REG                                      (0x0000D2B0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_10_REG                                     (0x0000D2B8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_11_REG                                     (0x0000D2C0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_12_REG                                     (0x0000D2C8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_13_REG                                     (0x0000D2D0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_14_REG                                     (0x0000D2D8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_BUF_15_REG                                     (0x0000D2E0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH2_CR_CADB_AO_MRSCFG_REG                                  (0x0000D2E8)
//Duplicate of MC0_CH0_CR_CADB_AO_MRSCFG_REG

#define MC0_CH2_CR_CADB_SELCFG_REG                                     (0x0000D2F0)
//Duplicate of MC0_CH0_CR_CADB_SELCFG_REG

#define MC0_CH2_CR_CADB_MRSCFG_REG                                     (0x0000D2F8)
//Duplicate of MC0_CH0_CR_CADB_MRSCFG_REG

#define MC0_CH3_CR_CADB_ACCESS_CONTROL_POLICY_REG                      (0x0000D300)
//Duplicate of MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_REG

#define MC0_CH3_CR_CADB_ACCESS_READ_POLICY_REG                         (0x0000D308)
//Duplicate of MC0_CR_CPGC2_ACCESS_READ_POLICY_REG

#define MC0_CH3_CR_CADB_ACCESS_WRITE_POLICY_REG                        (0x0000D310)
//Duplicate of MC0_CR_CPGC2_ACCESS_WRITE_POLICY_REG

#define MC0_CH3_CR_CADB_CTL_REG                                        (0x0000D318)
//Duplicate of MC0_CH0_CR_CADB_CTL_REG

#define MC0_CH3_CR_CADB_CFG_REG                                        (0x0000D31C)
//Duplicate of MC0_CH0_CR_CADB_CFG_REG

#define MC0_CH3_CR_CADB_DLY_REG                                        (0x0000D320)
//Duplicate of MC0_CH0_CR_CADB_DLY_REG

#define MC0_CH3_CR_CADB_STATUS_REG                                     (0x0000D324)
//Duplicate of MC0_CH0_CR_CADB_STATUS_REG

#define MC0_CH3_CR_CADB_OVERRIDE_REG                                   (0x0000D328)
//Duplicate of MC0_CH0_CR_CADB_OVERRIDE_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG                     (0x0000D330)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_CFG_0_REG                          (0x0000D338)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_CFG_1_REG                          (0x0000D339)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_CFG_2_REG                          (0x0000D33A)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_CFG_3_REG                          (0x0000D33B)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG                    (0x0000D33C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_PBUF_0_REG                         (0x0000D340)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_PBUF_1_REG                         (0x0000D344)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_PBUF_2_REG                         (0x0000D348)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_PBUF_3_REG                         (0x0000D34C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_LMN_REG                            (0x0000D350)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_POLY_0_REG                         (0x0000D358)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_POLY_1_REG                         (0x0000D35C)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_POLY_2_REG                         (0x0000D360)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH3_CR_CADB_DSEL_UNISEQ_POLY_3_REG                         (0x0000D364)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC0_CH3_CR_CADB_BUF_0_REG                                      (0x0000D368)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_1_REG                                      (0x0000D370)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_2_REG                                      (0x0000D378)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_3_REG                                      (0x0000D380)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_4_REG                                      (0x0000D388)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_5_REG                                      (0x0000D390)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_6_REG                                      (0x0000D398)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_7_REG                                      (0x0000D3A0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_8_REG                                      (0x0000D3A8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_9_REG                                      (0x0000D3B0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_10_REG                                     (0x0000D3B8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_11_REG                                     (0x0000D3C0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_12_REG                                     (0x0000D3C8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_13_REG                                     (0x0000D3D0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_14_REG                                     (0x0000D3D8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_BUF_15_REG                                     (0x0000D3E0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC0_CH3_CR_CADB_AO_MRSCFG_REG                                  (0x0000D3E8)
//Duplicate of MC0_CH0_CR_CADB_AO_MRSCFG_REG

#define MC0_CH3_CR_CADB_SELCFG_REG                                     (0x0000D3F0)
//Duplicate of MC0_CH0_CR_CADB_SELCFG_REG

#define MC0_CH3_CR_CADB_MRSCFG_REG                                     (0x0000D3F8)
//Duplicate of MC0_CH0_CR_CADB_MRSCFG_REG

#define MC0_IBECC_ACTIVATE_REG                                         (0x0000D400)

  #define MC0_IBECC_ACTIVATE_IBECC_EN_OFF                              ( 0)
  #define MC0_IBECC_ACTIVATE_IBECC_EN_WID                              ( 1)
  #define MC0_IBECC_ACTIVATE_IBECC_EN_MSK                              (0x00000001)
  #define MC0_IBECC_ACTIVATE_IBECC_EN_MIN                              (0)
  #define MC0_IBECC_ACTIVATE_IBECC_EN_MAX                              (1) // 0x00000001
  #define MC0_IBECC_ACTIVATE_IBECC_EN_DEF                              (0x00000000)
  #define MC0_IBECC_ACTIVATE_IBECC_EN_HSH                              (0x0100D400)

#define MC0_IBECC_STORAGE_ADDR_REG                                     (0x0000D404)

  #define MC0_IBECC_STORAGE_ADDR_START_ADDR_OFF                        ( 0)
  #define MC0_IBECC_STORAGE_ADDR_START_ADDR_WID                        (26)
  #define MC0_IBECC_STORAGE_ADDR_START_ADDR_MSK                        (0x03FFFFFF)
  #define MC0_IBECC_STORAGE_ADDR_START_ADDR_MIN                        (0)
  #define MC0_IBECC_STORAGE_ADDR_START_ADDR_MAX                        (67108863) // 0x03FFFFFF
  #define MC0_IBECC_STORAGE_ADDR_START_ADDR_DEF                        (0x00000000)
  #define MC0_IBECC_STORAGE_ADDR_START_ADDR_HSH                        (0x1A00D404)

#define MC0_IBECC_PROTECT_ADDR_RANGE_0_REG                             (0x0000D408)

  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_BASE_OFF                      ( 0)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_BASE_WID                      (26)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_BASE_MSK                      (0x03FFFFFF)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_BASE_MIN                      (0)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_BASE_MAX                      (67108863) // 0x03FFFFFF
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_BASE_DEF                      (0x00000000)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_BASE_HSH                      (0x5A00D408)

  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_MASK_OFF                      (32)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_MASK_WID                      (26)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_MASK_MSK                      (0x03FFFFFF00000000ULL)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_MASK_MIN                      (0)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_MASK_MAX                      (67108863) // 0x03FFFFFF
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_MASK_DEF                      (0x00000000)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_MASK_HSH                      (0x5A40D408)

  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_ENABLE_OFF                    (63)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_ENABLE_WID                    ( 1)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_ENABLE_MSK                    (0x8000000000000000ULL)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_ENABLE_MIN                    (0)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_ENABLE_MAX                    (1) // 0x00000001
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_ENABLE_DEF                    (0x00000000)
  #define MC0_IBECC_PROTECT_ADDR_RANGE_0_ENABLE_HSH                    (0x417ED408)

#define MC0_IBECC_PROTECT_ADDR_RANGE_1_REG                             (0x0000D410)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC0_IBECC_PROTECT_ADDR_RANGE_2_REG                             (0x0000D418)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC0_IBECC_PROTECT_ADDR_RANGE_3_REG                             (0x0000D420)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC0_IBECC_PROTECT_ADDR_RANGE_4_REG                             (0x0000D428)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC0_IBECC_PROTECT_ADDR_RANGE_5_REG                             (0x0000D430)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC0_IBECC_PROTECT_ADDR_RANGE_6_REG                             (0x0000D438)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC0_IBECC_PROTECT_ADDR_RANGE_7_REG                             (0x0000D440)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC0_IBECC_CONTROL_REG                                          (0x0000D448)

  #define MC0_IBECC_CONTROL_OPERATION_MODE_OFF                         ( 0)
  #define MC0_IBECC_CONTROL_OPERATION_MODE_WID                         ( 2)
  #define MC0_IBECC_CONTROL_OPERATION_MODE_MSK                         (0x00000003)
  #define MC0_IBECC_CONTROL_OPERATION_MODE_MIN                         (0)
  #define MC0_IBECC_CONTROL_OPERATION_MODE_MAX                         (3) // 0x00000003
  #define MC0_IBECC_CONTROL_OPERATION_MODE_DEF                         (0x00000000)
  #define MC0_IBECC_CONTROL_OPERATION_MODE_HSH                         (0x0200D448)

  #define MC0_IBECC_CONTROL_DISABLE_CORRECTION_OFF                     ( 2)
  #define MC0_IBECC_CONTROL_DISABLE_CORRECTION_WID                     ( 1)
  #define MC0_IBECC_CONTROL_DISABLE_CORRECTION_MSK                     (0x00000004)
  #define MC0_IBECC_CONTROL_DISABLE_CORRECTION_MIN                     (0)
  #define MC0_IBECC_CONTROL_DISABLE_CORRECTION_MAX                     (1) // 0x00000001
  #define MC0_IBECC_CONTROL_DISABLE_CORRECTION_DEF                     (0x00000000)
  #define MC0_IBECC_CONTROL_DISABLE_CORRECTION_HSH                     (0x0104D448)

  #define MC0_IBECC_CONTROL_DISABLE_UFILL_POISON_OFF                   ( 3)
  #define MC0_IBECC_CONTROL_DISABLE_UFILL_POISON_WID                   ( 1)
  #define MC0_IBECC_CONTROL_DISABLE_UFILL_POISON_MSK                   (0x00000008)
  #define MC0_IBECC_CONTROL_DISABLE_UFILL_POISON_MIN                   (0)
  #define MC0_IBECC_CONTROL_DISABLE_UFILL_POISON_MAX                   (1) // 0x00000001
  #define MC0_IBECC_CONTROL_DISABLE_UFILL_POISON_DEF                   (0x00000000)
  #define MC0_IBECC_CONTROL_DISABLE_UFILL_POISON_HSH                   (0x0106D448)

  #define MC0_IBECC_CONTROL_DISABLE_PCIE_ERR_OFF                       ( 4)
  #define MC0_IBECC_CONTROL_DISABLE_PCIE_ERR_WID                       ( 1)
  #define MC0_IBECC_CONTROL_DISABLE_PCIE_ERR_MSK                       (0x00000010)
  #define MC0_IBECC_CONTROL_DISABLE_PCIE_ERR_MIN                       (0)
  #define MC0_IBECC_CONTROL_DISABLE_PCIE_ERR_MAX                       (1) // 0x00000001
  #define MC0_IBECC_CONTROL_DISABLE_PCIE_ERR_DEF                       (0x00000000)
  #define MC0_IBECC_CONTROL_DISABLE_PCIE_ERR_HSH                       (0x0108D448)

  #define MC0_IBECC_CONTROL_DISABLE_MCA_LOG_OFF                        ( 5)
  #define MC0_IBECC_CONTROL_DISABLE_MCA_LOG_WID                        ( 1)
  #define MC0_IBECC_CONTROL_DISABLE_MCA_LOG_MSK                        (0x00000020)
  #define MC0_IBECC_CONTROL_DISABLE_MCA_LOG_MIN                        (0)
  #define MC0_IBECC_CONTROL_DISABLE_MCA_LOG_MAX                        (1) // 0x00000001
  #define MC0_IBECC_CONTROL_DISABLE_MCA_LOG_DEF                        (0x00000000)
  #define MC0_IBECC_CONTROL_DISABLE_MCA_LOG_HSH                        (0x010AD448)

  #define MC0_IBECC_CONTROL_DISABLE_VC_CACHING_OFF                     ( 6)
  #define MC0_IBECC_CONTROL_DISABLE_VC_CACHING_WID                     ( 2)
  #define MC0_IBECC_CONTROL_DISABLE_VC_CACHING_MSK                     (0x000000C0)
  #define MC0_IBECC_CONTROL_DISABLE_VC_CACHING_MIN                     (0)
  #define MC0_IBECC_CONTROL_DISABLE_VC_CACHING_MAX                     (3) // 0x00000003
  #define MC0_IBECC_CONTROL_DISABLE_VC_CACHING_DEF                     (0x00000000)
  #define MC0_IBECC_CONTROL_DISABLE_VC_CACHING_HSH                     (0x020CD448)

  #define MC0_IBECC_CONTROL_DISABLE_RANGE_CACHING_OFF                  ( 8)
  #define MC0_IBECC_CONTROL_DISABLE_RANGE_CACHING_WID                  ( 8)
  #define MC0_IBECC_CONTROL_DISABLE_RANGE_CACHING_MSK                  (0x0000FF00)
  #define MC0_IBECC_CONTROL_DISABLE_RANGE_CACHING_MIN                  (0)
  #define MC0_IBECC_CONTROL_DISABLE_RANGE_CACHING_MAX                  (255) // 0x000000FF
  #define MC0_IBECC_CONTROL_DISABLE_RANGE_CACHING_DEF                  (0x00000000)
  #define MC0_IBECC_CONTROL_DISABLE_RANGE_CACHING_HSH                  (0x0810D448)

  #define MC0_IBECC_CONTROL_HIT_ON_READY_OFF                           (16)
  #define MC0_IBECC_CONTROL_HIT_ON_READY_WID                           ( 1)
  #define MC0_IBECC_CONTROL_HIT_ON_READY_MSK                           (0x00010000)
  #define MC0_IBECC_CONTROL_HIT_ON_READY_MIN                           (0)
  #define MC0_IBECC_CONTROL_HIT_ON_READY_MAX                           (1) // 0x00000001
  #define MC0_IBECC_CONTROL_HIT_ON_READY_DEF                           (0x00000000)
  #define MC0_IBECC_CONTROL_HIT_ON_READY_HSH                           (0x0120D448)

  #define MC0_IBECC_CONTROL_DISABLE_CROSS_VC_CHECK_OFF                 (17)
  #define MC0_IBECC_CONTROL_DISABLE_CROSS_VC_CHECK_WID                 ( 1)
  #define MC0_IBECC_CONTROL_DISABLE_CROSS_VC_CHECK_MSK                 (0x00020000)
  #define MC0_IBECC_CONTROL_DISABLE_CROSS_VC_CHECK_MIN                 (0)
  #define MC0_IBECC_CONTROL_DISABLE_CROSS_VC_CHECK_MAX                 (1) // 0x00000001
  #define MC0_IBECC_CONTROL_DISABLE_CROSS_VC_CHECK_DEF                 (0x00000000)
  #define MC0_IBECC_CONTROL_DISABLE_CROSS_VC_CHECK_HSH                 (0x0122D448)

  #define MC0_IBECC_CONTROL_DISABLE_VC1_FF_ALLOCATION_OFF              (18)
  #define MC0_IBECC_CONTROL_DISABLE_VC1_FF_ALLOCATION_WID              ( 1)
  #define MC0_IBECC_CONTROL_DISABLE_VC1_FF_ALLOCATION_MSK              (0x00040000)
  #define MC0_IBECC_CONTROL_DISABLE_VC1_FF_ALLOCATION_MIN              (0)
  #define MC0_IBECC_CONTROL_DISABLE_VC1_FF_ALLOCATION_MAX              (1) // 0x00000001
  #define MC0_IBECC_CONTROL_DISABLE_VC1_FF_ALLOCATION_DEF              (0x00000000)
  #define MC0_IBECC_CONTROL_DISABLE_VC1_FF_ALLOCATION_HSH              (0x0124D448)

#define MC0_IBECC_INJ_ADDR_COMPARE_REG                                 (0x0000D450)

  #define MC0_IBECC_INJ_ADDR_COMPARE_ADDR_OFF                          ( 6)
  #define MC0_IBECC_INJ_ADDR_COMPARE_ADDR_WID                          (40)
  #define MC0_IBECC_INJ_ADDR_COMPARE_ADDR_MSK                          (0x00003FFFFFFFFFC0ULL)
  #define MC0_IBECC_INJ_ADDR_COMPARE_ADDR_MIN                          (0)
  #define MC0_IBECC_INJ_ADDR_COMPARE_ADDR_MAX                          (1099511627775ULL) // 0xFFFFFFFFFF
  #define MC0_IBECC_INJ_ADDR_COMPARE_ADDR_DEF                          (0x00000000)
  #define MC0_IBECC_INJ_ADDR_COMPARE_ADDR_HSH                          (0x680CD450)

#define MC0_IBECC_INJ_ADDR_MASK_REG                                    (0x0000D458)
//Duplicate of MC0_IBECC_INJ_ADDR_COMPARE_REG

#define MC0_IBECC_INJ_COUNT_REG                                        (0x0000D460)

  #define MC0_IBECC_INJ_COUNT_COUNT_OFF                                ( 0)
  #define MC0_IBECC_INJ_COUNT_COUNT_WID                                (32)
  #define MC0_IBECC_INJ_COUNT_COUNT_MSK                                (0xFFFFFFFF)
  #define MC0_IBECC_INJ_COUNT_COUNT_MIN                                (0)
  #define MC0_IBECC_INJ_COUNT_COUNT_MAX                                (4294967295) // 0xFFFFFFFF
  #define MC0_IBECC_INJ_COUNT_COUNT_DEF                                (0x00000000)
  #define MC0_IBECC_INJ_COUNT_COUNT_HSH                                (0x2000D460)

#define MC0_IBECC_INJ_CONTROL_REG                                      (0x0000D464)

  #define MC0_IBECC_INJ_CONTROL_ECC_Inj_OFF                            ( 0)
  #define MC0_IBECC_INJ_CONTROL_ECC_Inj_WID                            ( 3)
  #define MC0_IBECC_INJ_CONTROL_ECC_Inj_MSK                            (0x00000007)
  #define MC0_IBECC_INJ_CONTROL_ECC_Inj_MIN                            (0)
  #define MC0_IBECC_INJ_CONTROL_ECC_Inj_MAX                            (7) // 0x00000007
  #define MC0_IBECC_INJ_CONTROL_ECC_Inj_DEF                            (0x00000000)
  #define MC0_IBECC_INJ_CONTROL_ECC_Inj_HSH                            (0x0300D464)

#define MC0_IBECC_ERROR_LOG_REG                                        (0x0000D468)

  #define MC0_IBECC_ERROR_LOG_CERR_OVERFLOW_OFF                        ( 3)
  #define MC0_IBECC_ERROR_LOG_CERR_OVERFLOW_WID                        ( 1)
  #define MC0_IBECC_ERROR_LOG_CERR_OVERFLOW_MSK                        (0x00000008)
  #define MC0_IBECC_ERROR_LOG_CERR_OVERFLOW_MIN                        (0)
  #define MC0_IBECC_ERROR_LOG_CERR_OVERFLOW_MAX                        (1) // 0x00000001
  #define MC0_IBECC_ERROR_LOG_CERR_OVERFLOW_DEF                        (0x00000000)
  #define MC0_IBECC_ERROR_LOG_CERR_OVERFLOW_HSH                        (0x4106D468)

  #define MC0_IBECC_ERROR_LOG_MERR_OVERFLOW_OFF                        ( 4)
  #define MC0_IBECC_ERROR_LOG_MERR_OVERFLOW_WID                        ( 1)
  #define MC0_IBECC_ERROR_LOG_MERR_OVERFLOW_MSK                        (0x00000010)
  #define MC0_IBECC_ERROR_LOG_MERR_OVERFLOW_MIN                        (0)
  #define MC0_IBECC_ERROR_LOG_MERR_OVERFLOW_MAX                        (1) // 0x00000001
  #define MC0_IBECC_ERROR_LOG_MERR_OVERFLOW_DEF                        (0x00000000)
  #define MC0_IBECC_ERROR_LOG_MERR_OVERFLOW_HSH                        (0x4108D468)

  #define MC0_IBECC_ERROR_LOG_ERRADD_OFF                               ( 5)
  #define MC0_IBECC_ERROR_LOG_ERRADD_WID                               (41)
  #define MC0_IBECC_ERROR_LOG_ERRADD_MSK                               (0x00003FFFFFFFFFE0ULL)
  #define MC0_IBECC_ERROR_LOG_ERRADD_MIN                               (0)
  #define MC0_IBECC_ERROR_LOG_ERRADD_MAX                               (2199023255551ULL) // 0x1FFFFFFFFFF
  #define MC0_IBECC_ERROR_LOG_ERRADD_DEF                               (0x00000000)
  #define MC0_IBECC_ERROR_LOG_ERRADD_HSH                               (0x690AD468)

  #define MC0_IBECC_ERROR_LOG_ERRSYND_OFF                              (46)
  #define MC0_IBECC_ERROR_LOG_ERRSYND_WID                              (16)
  #define MC0_IBECC_ERROR_LOG_ERRSYND_MSK                              (0x3FFFC00000000000ULL)
  #define MC0_IBECC_ERROR_LOG_ERRSYND_MIN                              (0)
  #define MC0_IBECC_ERROR_LOG_ERRSYND_MAX                              (65535) // 0x0000FFFF
  #define MC0_IBECC_ERROR_LOG_ERRSYND_DEF                              (0x00000000)
  #define MC0_IBECC_ERROR_LOG_ERRSYND_HSH                              (0x505CD468)

  #define MC0_IBECC_ERROR_LOG_CERRSTS_OFF                              (62)
  #define MC0_IBECC_ERROR_LOG_CERRSTS_WID                              ( 1)
  #define MC0_IBECC_ERROR_LOG_CERRSTS_MSK                              (0x4000000000000000ULL)
  #define MC0_IBECC_ERROR_LOG_CERRSTS_MIN                              (0)
  #define MC0_IBECC_ERROR_LOG_CERRSTS_MAX                              (1) // 0x00000001
  #define MC0_IBECC_ERROR_LOG_CERRSTS_DEF                              (0x00000000)
  #define MC0_IBECC_ERROR_LOG_CERRSTS_HSH                              (0x417CD468)

  #define MC0_IBECC_ERROR_LOG_MERRSTS_OFF                              (63)
  #define MC0_IBECC_ERROR_LOG_MERRSTS_WID                              ( 1)
  #define MC0_IBECC_ERROR_LOG_MERRSTS_MSK                              (0x8000000000000000ULL)
  #define MC0_IBECC_ERROR_LOG_MERRSTS_MIN                              (0)
  #define MC0_IBECC_ERROR_LOG_MERRSTS_MAX                              (1) // 0x00000001
  #define MC0_IBECC_ERROR_LOG_MERRSTS_DEF                              (0x00000000)
  #define MC0_IBECC_ERROR_LOG_MERRSTS_HSH                              (0x417ED468)

#define MC0_IBECC_TME_FM_DEVICE_ID_REG                                 (0x0000D4E8)

  #define MC0_IBECC_TME_FM_DEVICE_ID_DEVICE_ID_OFF                     ( 0)
  #define MC0_IBECC_TME_FM_DEVICE_ID_DEVICE_ID_WID                     (64)
  #define MC0_IBECC_TME_FM_DEVICE_ID_DEVICE_ID_MSK                     (0xFFFFFFFFFFFFFFFFULL)
  #define MC0_IBECC_TME_FM_DEVICE_ID_DEVICE_ID_MIN                     (0)
  #define MC0_IBECC_TME_FM_DEVICE_ID_DEVICE_ID_MAX                     (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define MC0_IBECC_TME_FM_DEVICE_ID_DEVICE_ID_DEF                     (0x00000000)
  #define MC0_IBECC_TME_FM_DEVICE_ID_DEVICE_ID_HSH                     (0x4000D4E8)

#define MC0_IBECC_TMECC_PARITY_ERROR_LOG_REG                           (0x0000D4F0)

  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_ADDRESS_OFF             ( 6)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_ADDRESS_WID             (40)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_ADDRESS_MSK             (0x00003FFFFFFFFFC0ULL)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_ADDRESS_MIN             (0)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_ADDRESS_MAX             (1099511627775ULL) // 0xFFFFFFFFFF
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_ADDRESS_DEF             (0x00000000)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_ADDRESS_HSH             (0x680CD4F0)

  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_TYPE_OFF                (58)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_TYPE_WID                ( 4)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_TYPE_MSK                (0x3C00000000000000ULL)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_TYPE_MIN                (0)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_TYPE_MAX                (15) // 0x0000000F
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_TYPE_DEF                (0x00000000)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_ERR_TYPE_HSH                (0x4474D4F0)

  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERR_OVERFLOW_OFF           (62)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERR_OVERFLOW_WID           ( 1)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERR_OVERFLOW_MSK           (0x4000000000000000ULL)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERR_OVERFLOW_MIN           (0)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERR_OVERFLOW_MAX           (1) // 0x00000001
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERR_OVERFLOW_DEF           (0x00000000)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERR_OVERFLOW_HSH           (0x417CD4F0)

  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERRSTS_OFF                 (63)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERRSTS_WID                 ( 1)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERRSTS_MSK                 (0x8000000000000000ULL)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERRSTS_MIN                 (0)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERRSTS_MAX                 (1) // 0x00000001
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERRSTS_DEF                 (0x00000000)
  #define MC0_IBECC_TMECC_PARITY_ERROR_LOG_PERRSTS_HSH                 (0x417ED4F0)

#define MC0_IBECC_TME_CONTROL_A0_REG                                   (0x0000D4FC)
#define MC0_IBECC_TME_CONTROL_REG                                      (0x0000D4F8)

  #define MC0_IBECC_TME_CONTROL_NM_Enable_OFF                          ( 0)
  #define MC0_IBECC_TME_CONTROL_NM_Enable_WID                          ( 1)
  #define MC0_IBECC_TME_CONTROL_NM_Enable_MSK                          (0x00000001)
  #define MC0_IBECC_TME_CONTROL_NM_Enable_MIN                          (0)
  #define MC0_IBECC_TME_CONTROL_NM_Enable_MAX                          (1) // 0x00000001
  #define MC0_IBECC_TME_CONTROL_NM_Enable_DEF                          (0x00000000)
  #define MC0_IBECC_TME_CONTROL_NM_Enable_HSH                          (0x4100D4F8)

  #define MC0_IBECC_TME_CONTROL_Encrypt_PMEM_OFF                       ( 1)
  #define MC0_IBECC_TME_CONTROL_Encrypt_PMEM_WID                       ( 1)
  #define MC0_IBECC_TME_CONTROL_Encrypt_PMEM_MSK                       (0x00000002)
  #define MC0_IBECC_TME_CONTROL_Encrypt_PMEM_MIN                       (0)
  #define MC0_IBECC_TME_CONTROL_Encrypt_PMEM_MAX                       (1) // 0x00000001
  #define MC0_IBECC_TME_CONTROL_Encrypt_PMEM_DEF                       (0x00000001)
  #define MC0_IBECC_TME_CONTROL_Encrypt_PMEM_HSH                       (0x4102D4F8)

  #define MC0_IBECC_TME_CONTROL_PMEM_Base_OFF                          ( 4)
  #define MC0_IBECC_TME_CONTROL_PMEM_Base_WID                          (12)
  #define MC0_IBECC_TME_CONTROL_PMEM_Base_MSK                          (0x0000FFF0)
  #define MC0_IBECC_TME_CONTROL_PMEM_Base_MIN                          (0)
  #define MC0_IBECC_TME_CONTROL_PMEM_Base_MAX                          (4095) // 0x00000FFF
  #define MC0_IBECC_TME_CONTROL_PMEM_Base_DEF                          (0x00000000)
  #define MC0_IBECC_TME_CONTROL_PMEM_Base_HSH                          (0x4C08D4F8)

  #define MC0_IBECC_TME_CONTROL_hash_lsb_OFF                           (16)
  #define MC0_IBECC_TME_CONTROL_hash_lsb_WID                           ( 3)
  #define MC0_IBECC_TME_CONTROL_hash_lsb_MSK                           (0x00070000)
  #define MC0_IBECC_TME_CONTROL_hash_lsb_MIN                           (0)
  #define MC0_IBECC_TME_CONTROL_hash_lsb_MAX                           (7) // 0x00000007
  #define MC0_IBECC_TME_CONTROL_hash_lsb_DEF                           (0x00000000)
  #define MC0_IBECC_TME_CONTROL_hash_lsb_HSH                           (0x4320D4F8)

  #define MC0_IBECC_TME_CONTROL_hash_enabled_OFF                       (19)
  #define MC0_IBECC_TME_CONTROL_hash_enabled_WID                       ( 1)
  #define MC0_IBECC_TME_CONTROL_hash_enabled_MSK                       (0x00080000)
  #define MC0_IBECC_TME_CONTROL_hash_enabled_MIN                       (0)
  #define MC0_IBECC_TME_CONTROL_hash_enabled_MAX                       (1) // 0x00000001
  #define MC0_IBECC_TME_CONTROL_hash_enabled_DEF                       (0x00000000)
  #define MC0_IBECC_TME_CONTROL_hash_enabled_HSH                       (0x4126D4F8)

  #define MC0_IBECC_TME_CONTROL_PMEM_Limit_OFF                         (20)
  #define MC0_IBECC_TME_CONTROL_PMEM_Limit_WID                         (12)
  #define MC0_IBECC_TME_CONTROL_PMEM_Limit_MSK                         (0xFFF00000)
  #define MC0_IBECC_TME_CONTROL_PMEM_Limit_MIN                         (0)
  #define MC0_IBECC_TME_CONTROL_PMEM_Limit_MAX                         (4095) // 0x00000FFF
  #define MC0_IBECC_TME_CONTROL_PMEM_Limit_DEF                         (0x00000000)
  #define MC0_IBECC_TME_CONTROL_PMEM_Limit_HSH                         (0x4C28D4F8)

  #define MC0_IBECC_TME_CONTROL_Reserved_OFF                           (32)
  #define MC0_IBECC_TME_CONTROL_Reserved_WID                           (32)
  #define MC0_IBECC_TME_CONTROL_Reserved_MSK                           (0xFFFFFFFF00000000ULL)
  #define MC0_IBECC_TME_CONTROL_Reserved_MIN                           (0)
  #define MC0_IBECC_TME_CONTROL_Reserved_MAX                           (4294967295) // 0xFFFFFFFF
  #define MC0_IBECC_TME_CONTROL_Reserved_DEF                           (0x00000000)
  #define MC0_IBECC_TME_CONTROL_Reserved_HSH                           (0x6040D4F8)

#define MC0_MAD_INTER_CHANNEL_REG                                      (0x0000D800)

  #define MC0_MAD_INTER_CHANNEL_DDR_TYPE_OFF                           ( 0)
  #define MC0_MAD_INTER_CHANNEL_DDR_TYPE_WID                           ( 3)
  #define MC0_MAD_INTER_CHANNEL_DDR_TYPE_MSK                           (0x00000007)
  #define MC0_MAD_INTER_CHANNEL_DDR_TYPE_MIN                           (0)
  #define MC0_MAD_INTER_CHANNEL_DDR_TYPE_MAX                           (7) // 0x00000007
  #define MC0_MAD_INTER_CHANNEL_DDR_TYPE_DEF                           (0x00000000)
  #define MC0_MAD_INTER_CHANNEL_DDR_TYPE_HSH                           (0x0300D800)

  #define MC0_MAD_INTER_CHANNEL_CH_L_MAP_OFF                           ( 4)
  #define MC0_MAD_INTER_CHANNEL_CH_L_MAP_WID                           ( 1)
  #define MC0_MAD_INTER_CHANNEL_CH_L_MAP_MSK                           (0x00000010)
  #define MC0_MAD_INTER_CHANNEL_CH_L_MAP_MIN                           (0)
  #define MC0_MAD_INTER_CHANNEL_CH_L_MAP_MAX                           (1) // 0x00000001
  #define MC0_MAD_INTER_CHANNEL_CH_L_MAP_DEF                           (0x00000000)
  #define MC0_MAD_INTER_CHANNEL_CH_L_MAP_HSH                           (0x0108D800)

  #define MC0_MAD_INTER_CHANNEL_CH_S_SIZE_OFF                          (12)
  #define MC0_MAD_INTER_CHANNEL_CH_S_SIZE_WID                          ( 8)
  #define MC0_MAD_INTER_CHANNEL_CH_S_SIZE_MSK                          (0x000FF000)
  #define MC0_MAD_INTER_CHANNEL_CH_S_SIZE_MIN                          (0)
  #define MC0_MAD_INTER_CHANNEL_CH_S_SIZE_MAX                          (255) // 0x000000FF
  #define MC0_MAD_INTER_CHANNEL_CH_S_SIZE_DEF                          (0x00000000)
  #define MC0_MAD_INTER_CHANNEL_CH_S_SIZE_HSH                          (0x0818D800)

  #define MC0_MAD_INTER_CHANNEL_CH_WIDTH_OFF                           (27)
  #define MC0_MAD_INTER_CHANNEL_CH_WIDTH_WID                           ( 2)
  #define MC0_MAD_INTER_CHANNEL_CH_WIDTH_MSK                           (0x18000000)
  #define MC0_MAD_INTER_CHANNEL_CH_WIDTH_MIN                           (0)
  #define MC0_MAD_INTER_CHANNEL_CH_WIDTH_MAX                           (3) // 0x00000003
  #define MC0_MAD_INTER_CHANNEL_CH_WIDTH_DEF                           (0x00000000)
  #define MC0_MAD_INTER_CHANNEL_CH_WIDTH_HSH                           (0x0236D800)

  #define MC0_MAD_INTER_CHANNEL_HalfCacheLineMode_OFF                  (31)
  #define MC0_MAD_INTER_CHANNEL_HalfCacheLineMode_WID                  ( 1)
  #define MC0_MAD_INTER_CHANNEL_HalfCacheLineMode_MSK                  (0x80000000)
  #define MC0_MAD_INTER_CHANNEL_HalfCacheLineMode_MIN                  (0)
  #define MC0_MAD_INTER_CHANNEL_HalfCacheLineMode_MAX                  (1) // 0x00000001
  #define MC0_MAD_INTER_CHANNEL_HalfCacheLineMode_DEF                  (0x00000000)
  #define MC0_MAD_INTER_CHANNEL_HalfCacheLineMode_HSH                  (0x013ED800)

#define MC0_MAD_INTRA_CH0_REG                                          (0x0000D804)

  #define MC0_MAD_INTRA_CH0_DIMM_L_MAP_OFF                             ( 0)
  #define MC0_MAD_INTRA_CH0_DIMM_L_MAP_WID                             ( 1)
  #define MC0_MAD_INTRA_CH0_DIMM_L_MAP_MSK                             (0x00000001)
  #define MC0_MAD_INTRA_CH0_DIMM_L_MAP_MIN                             (0)
  #define MC0_MAD_INTRA_CH0_DIMM_L_MAP_MAX                             (1) // 0x00000001
  #define MC0_MAD_INTRA_CH0_DIMM_L_MAP_DEF                             (0x00000000)
  #define MC0_MAD_INTRA_CH0_DIMM_L_MAP_HSH                             (0x0100D804)

  #define MC0_MAD_INTRA_CH0_EIM_OFF                                    ( 8)
  #define MC0_MAD_INTRA_CH0_EIM_WID                                    ( 1)
  #define MC0_MAD_INTRA_CH0_EIM_MSK                                    (0x00000100)
  #define MC0_MAD_INTRA_CH0_EIM_MIN                                    (0)
  #define MC0_MAD_INTRA_CH0_EIM_MAX                                    (1) // 0x00000001
  #define MC0_MAD_INTRA_CH0_EIM_DEF                                    (0x00000000)
  #define MC0_MAD_INTRA_CH0_EIM_HSH                                    (0x0110D804)

  #define MC0_MAD_INTRA_CH0_ECC_OFF                                    (12)
  #define MC0_MAD_INTRA_CH0_ECC_WID                                    ( 2)
  #define MC0_MAD_INTRA_CH0_ECC_MSK                                    (0x00003000)
  #define MC0_MAD_INTRA_CH0_ECC_MIN                                    (0)
  #define MC0_MAD_INTRA_CH0_ECC_MAX                                    (3) // 0x00000003
  #define MC0_MAD_INTRA_CH0_ECC_DEF                                    (0x00000000)
  #define MC0_MAD_INTRA_CH0_ECC_HSH                                    (0x0218D804)

  #define MC0_MAD_INTRA_CH0_CRC_OFF                                    (14)
  #define MC0_MAD_INTRA_CH0_CRC_WID                                    ( 1)
  #define MC0_MAD_INTRA_CH0_CRC_MSK                                    (0x00004000)
  #define MC0_MAD_INTRA_CH0_CRC_MIN                                    (0)
  #define MC0_MAD_INTRA_CH0_CRC_MAX                                    (1) // 0x00000001
  #define MC0_MAD_INTRA_CH0_CRC_DEF                                    (0x00000000)
  #define MC0_MAD_INTRA_CH0_CRC_HSH                                    (0x011CD804)

#define MC0_MAD_INTRA_CH1_REG                                          (0x0000D808)
//Duplicate of MC0_MAD_INTRA_CH0_REG

#define MC0_MAD_DIMM_CH0_REG                                           (0x0000D80C)

  #define MC0_MAD_DIMM_CH0_DIMM_L_SIZE_OFF                             ( 0)
  #define MC0_MAD_DIMM_CH0_DIMM_L_SIZE_WID                             ( 7)
  #define MC0_MAD_DIMM_CH0_DIMM_L_SIZE_MSK                             (0x0000007F)
  #define MC0_MAD_DIMM_CH0_DIMM_L_SIZE_MIN                             (0)
  #define MC0_MAD_DIMM_CH0_DIMM_L_SIZE_MAX                             (127) // 0x0000007F
  #define MC0_MAD_DIMM_CH0_DIMM_L_SIZE_DEF                             (0x00000000)
  #define MC0_MAD_DIMM_CH0_DIMM_L_SIZE_HSH                             (0x0700D80C)

  #define MC0_MAD_DIMM_CH0_DLW_OFF                                     ( 7)
  #define MC0_MAD_DIMM_CH0_DLW_WID                                     ( 2)
  #define MC0_MAD_DIMM_CH0_DLW_MSK                                     (0x00000180)
  #define MC0_MAD_DIMM_CH0_DLW_MIN                                     (0)
  #define MC0_MAD_DIMM_CH0_DLW_MAX                                     (3) // 0x00000003
  #define MC0_MAD_DIMM_CH0_DLW_DEF                                     (0x00000000)
  #define MC0_MAD_DIMM_CH0_DLW_HSH                                     (0x020ED80C)

  #define MC0_MAD_DIMM_CH0_DLNOR_OFF                                   ( 9)
  #define MC0_MAD_DIMM_CH0_DLNOR_WID                                   ( 2)
  #define MC0_MAD_DIMM_CH0_DLNOR_MSK                                   (0x00000600)
  #define MC0_MAD_DIMM_CH0_DLNOR_MIN                                   (0)
  #define MC0_MAD_DIMM_CH0_DLNOR_MAX                                   (3) // 0x00000003
  #define MC0_MAD_DIMM_CH0_DLNOR_DEF                                   (0x00000000)
  #define MC0_MAD_DIMM_CH0_DLNOR_HSH                                   (0x0212D80C)

  #define MC0_MAD_DIMM_CH0_ddr5_ds_8Gb_OFF                             (11)
  #define MC0_MAD_DIMM_CH0_ddr5_ds_8Gb_WID                             ( 1)
  #define MC0_MAD_DIMM_CH0_ddr5_ds_8Gb_MSK                             (0x00000800)
  #define MC0_MAD_DIMM_CH0_ddr5_ds_8Gb_MIN                             (0)
  #define MC0_MAD_DIMM_CH0_ddr5_ds_8Gb_MAX                             (1) // 0x00000001
  #define MC0_MAD_DIMM_CH0_ddr5_ds_8Gb_DEF                             (0x00000001)
  #define MC0_MAD_DIMM_CH0_ddr5_ds_8Gb_HSH                             (0x0116D80C)

  #define MC0_MAD_DIMM_CH0_ddr5_dl_8Gb_OFF                             (12)
  #define MC0_MAD_DIMM_CH0_ddr5_dl_8Gb_WID                             ( 1)
  #define MC0_MAD_DIMM_CH0_ddr5_dl_8Gb_MSK                             (0x00001000)
  #define MC0_MAD_DIMM_CH0_ddr5_dl_8Gb_MIN                             (0)
  #define MC0_MAD_DIMM_CH0_ddr5_dl_8Gb_MAX                             (1) // 0x00000001
  #define MC0_MAD_DIMM_CH0_ddr5_dl_8Gb_DEF                             (0x00000001)
  #define MC0_MAD_DIMM_CH0_ddr5_dl_8Gb_HSH                             (0x0118D80C)

  #define MC0_MAD_DIMM_CH0_DIMM_S_SIZE_OFF                             (16)
  #define MC0_MAD_DIMM_CH0_DIMM_S_SIZE_WID                             ( 7)
  #define MC0_MAD_DIMM_CH0_DIMM_S_SIZE_MSK                             (0x007F0000)
  #define MC0_MAD_DIMM_CH0_DIMM_S_SIZE_MIN                             (0)
  #define MC0_MAD_DIMM_CH0_DIMM_S_SIZE_MAX                             (127) // 0x0000007F
  #define MC0_MAD_DIMM_CH0_DIMM_S_SIZE_DEF                             (0x00000000)
  #define MC0_MAD_DIMM_CH0_DIMM_S_SIZE_HSH                             (0x0720D80C)

  #define MC0_MAD_DIMM_CH0_DSW_OFF                                     (24)
  #define MC0_MAD_DIMM_CH0_DSW_WID                                     ( 2)
  #define MC0_MAD_DIMM_CH0_DSW_MSK                                     (0x03000000)
  #define MC0_MAD_DIMM_CH0_DSW_MIN                                     (0)
  #define MC0_MAD_DIMM_CH0_DSW_MAX                                     (3) // 0x00000003
  #define MC0_MAD_DIMM_CH0_DSW_DEF                                     (0x00000000)
  #define MC0_MAD_DIMM_CH0_DSW_HSH                                     (0x0230D80C)

  #define MC0_MAD_DIMM_CH0_DSNOR_OFF                                   (26)
  #define MC0_MAD_DIMM_CH0_DSNOR_WID                                   ( 2)
  #define MC0_MAD_DIMM_CH0_DSNOR_MSK                                   (0x0C000000)
  #define MC0_MAD_DIMM_CH0_DSNOR_MIN                                   (0)
  #define MC0_MAD_DIMM_CH0_DSNOR_MAX                                   (3) // 0x00000003
  #define MC0_MAD_DIMM_CH0_DSNOR_DEF                                   (0x00000000)
  #define MC0_MAD_DIMM_CH0_DSNOR_HSH                                   (0x0234D80C)

  #define MC0_MAD_DIMM_CH0_BG0_bit_options_OFF                         (28)
  #define MC0_MAD_DIMM_CH0_BG0_bit_options_WID                         ( 2)
  #define MC0_MAD_DIMM_CH0_BG0_bit_options_MSK                         (0x30000000)
  #define MC0_MAD_DIMM_CH0_BG0_bit_options_MIN                         (0)
  #define MC0_MAD_DIMM_CH0_BG0_bit_options_MAX                         (3) // 0x00000003
  #define MC0_MAD_DIMM_CH0_BG0_bit_options_DEF                         (0x00000001)
  #define MC0_MAD_DIMM_CH0_BG0_bit_options_HSH                         (0x0238D80C)

  #define MC0_MAD_DIMM_CH0_Decoder_EBH_OFF                             (30)
  #define MC0_MAD_DIMM_CH0_Decoder_EBH_WID                             ( 2)
  #define MC0_MAD_DIMM_CH0_Decoder_EBH_MSK                             (0xC0000000)
  #define MC0_MAD_DIMM_CH0_Decoder_EBH_MIN                             (0)
  #define MC0_MAD_DIMM_CH0_Decoder_EBH_MAX                             (3) // 0x00000003
  #define MC0_MAD_DIMM_CH0_Decoder_EBH_DEF                             (0x00000000)
  #define MC0_MAD_DIMM_CH0_Decoder_EBH_HSH                             (0x023CD80C)

#define MC0_MAD_DIMM_CH1_REG                                           (0x0000D810)
//Duplicate of MC0_MAD_DIMM_CH0_REG
#define MC0_MCDECS_MISC_REG                                            (0x0000D818)

  #define MC0_MCDECS_MISC_Spare_RW_OFF                                 ( 0)
  #define MC0_MCDECS_MISC_Spare_RW_WID                                 (12)
  #define MC0_MCDECS_MISC_Spare_RW_MSK                                 (0x00000FFF)
  #define MC0_MCDECS_MISC_Spare_RW_MIN                                 (0)
  #define MC0_MCDECS_MISC_Spare_RW_MAX                                 (4095) // 0x00000FFF
  #define MC0_MCDECS_MISC_Spare_RW_DEF                                 (0x00000000)
  #define MC0_MCDECS_MISC_Spare_RW_HSH                                 (0x0C00D818)

  #define MC0_MCDECS_MISC_VISAByteSel_OFF                              (12)
  #define MC0_MCDECS_MISC_VISAByteSel_WID                              ( 4)
  #define MC0_MCDECS_MISC_VISAByteSel_MSK                              (0x0000F000)
  #define MC0_MCDECS_MISC_VISAByteSel_MIN                              (0)
  #define MC0_MCDECS_MISC_VISAByteSel_MAX                              (15) // 0x0000000F
  #define MC0_MCDECS_MISC_VISAByteSel_DEF                              (0x00000000)
  #define MC0_MCDECS_MISC_VISAByteSel_HSH                              (0x0418D818)

  #define MC0_MCDECS_MISC_spare_RW_V_OFF                               (16)
  #define MC0_MCDECS_MISC_spare_RW_V_WID                               (16)
  #define MC0_MCDECS_MISC_spare_RW_V_MSK                               (0xFFFF0000)
  #define MC0_MCDECS_MISC_spare_RW_V_MIN                               (0)
  #define MC0_MCDECS_MISC_spare_RW_V_MAX                               (65535) // 0x0000FFFF
  #define MC0_MCDECS_MISC_spare_RW_V_DEF                               (0x00000000)
  #define MC0_MCDECS_MISC_spare_RW_V_HSH                               (0x1020D818)

#define MC0_MCDECS_CBIT_REG                                            (0x0000D81C)

  #define MC0_MCDECS_CBIT_increase_rcomp_OFF                           ( 0)
  #define MC0_MCDECS_CBIT_increase_rcomp_WID                           ( 1)
  #define MC0_MCDECS_CBIT_increase_rcomp_MSK                           (0x00000001)
  #define MC0_MCDECS_CBIT_increase_rcomp_MIN                           (0)
  #define MC0_MCDECS_CBIT_increase_rcomp_MAX                           (1) // 0x00000001
  #define MC0_MCDECS_CBIT_increase_rcomp_DEF                           (0x00000000)
  #define MC0_MCDECS_CBIT_increase_rcomp_HSH                           (0x0100D81C)

  #define MC0_MCDECS_CBIT_rank2_to_rank1_OFF                           ( 1)
  #define MC0_MCDECS_CBIT_rank2_to_rank1_WID                           ( 1)
  #define MC0_MCDECS_CBIT_rank2_to_rank1_MSK                           (0x00000002)
  #define MC0_MCDECS_CBIT_rank2_to_rank1_MIN                           (0)
  #define MC0_MCDECS_CBIT_rank2_to_rank1_MAX                           (1) // 0x00000001
  #define MC0_MCDECS_CBIT_rank2_to_rank1_DEF                           (0x00000000)
  #define MC0_MCDECS_CBIT_rank2_to_rank1_HSH                           (0x0102D81C)

  #define MC0_MCDECS_CBIT_ovrd_pcu_sr_exit_OFF                         ( 2)
  #define MC0_MCDECS_CBIT_ovrd_pcu_sr_exit_WID                         ( 1)
  #define MC0_MCDECS_CBIT_ovrd_pcu_sr_exit_MSK                         (0x00000004)
  #define MC0_MCDECS_CBIT_ovrd_pcu_sr_exit_MIN                         (0)
  #define MC0_MCDECS_CBIT_ovrd_pcu_sr_exit_MAX                         (1) // 0x00000001
  #define MC0_MCDECS_CBIT_ovrd_pcu_sr_exit_DEF                         (0x00000000)
  #define MC0_MCDECS_CBIT_ovrd_pcu_sr_exit_HSH                         (0x0104D81C)

  #define MC0_MCDECS_CBIT_psmi_freeze_pwm_counters_OFF                 ( 3)
  #define MC0_MCDECS_CBIT_psmi_freeze_pwm_counters_WID                 ( 1)
  #define MC0_MCDECS_CBIT_psmi_freeze_pwm_counters_MSK                 (0x00000008)
  #define MC0_MCDECS_CBIT_psmi_freeze_pwm_counters_MIN                 (0)
  #define MC0_MCDECS_CBIT_psmi_freeze_pwm_counters_MAX                 (1) // 0x00000001
  #define MC0_MCDECS_CBIT_psmi_freeze_pwm_counters_DEF                 (0x00000000)
  #define MC0_MCDECS_CBIT_psmi_freeze_pwm_counters_HSH                 (0x0106D81C)

  #define MC0_MCDECS_CBIT_dis_single_ch_sr_OFF                         ( 4)
  #define MC0_MCDECS_CBIT_dis_single_ch_sr_WID                         ( 1)
  #define MC0_MCDECS_CBIT_dis_single_ch_sr_MSK                         (0x00000010)
  #define MC0_MCDECS_CBIT_dis_single_ch_sr_MIN                         (0)
  #define MC0_MCDECS_CBIT_dis_single_ch_sr_MAX                         (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_single_ch_sr_DEF                         (0x00000001)
  #define MC0_MCDECS_CBIT_dis_single_ch_sr_HSH                         (0x0108D81C)

  #define MC0_MCDECS_CBIT_dis_other_ch_stolen_ref_OFF                  ( 5)
  #define MC0_MCDECS_CBIT_dis_other_ch_stolen_ref_WID                  ( 1)
  #define MC0_MCDECS_CBIT_dis_other_ch_stolen_ref_MSK                  (0x00000020)
  #define MC0_MCDECS_CBIT_dis_other_ch_stolen_ref_MIN                  (0)
  #define MC0_MCDECS_CBIT_dis_other_ch_stolen_ref_MAX                  (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_other_ch_stolen_ref_DEF                  (0x00000001)
  #define MC0_MCDECS_CBIT_dis_other_ch_stolen_ref_HSH                  (0x010AD81C)

  #define MC0_MCDECS_CBIT_ForceSREntry_dft_OFF                         ( 6)
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_WID                         ( 1)
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_MSK                         (0x00000040)
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_MIN                         (0)
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_MAX                         (1) // 0x00000001
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_DEF                         (0x00000000)
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_HSH                         (0x010CD81C)

  #define MC0_MCDECS_CBIT_reserved_OFF                                 ( 7)
  #define MC0_MCDECS_CBIT_reserved_WID                                 ( 2)
  #define MC0_MCDECS_CBIT_reserved_MSK                                 (0x00000180)
  #define MC0_MCDECS_CBIT_reserved_MIN                                 (0)
  #define MC0_MCDECS_CBIT_reserved_MAX                                 (3) // 0x00000003
  #define MC0_MCDECS_CBIT_reserved_DEF                                 (0x00000000)
  #define MC0_MCDECS_CBIT_reserved_HSH                                 (0x020ED81C)

  #define MC0_MCDECS_CBIT_ForceSREntry_dft_is_sticky_OFF               ( 9)
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_is_sticky_WID               ( 1)
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_is_sticky_MSK               (0x00000200)
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_is_sticky_MIN               (0)
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_is_sticky_MAX               (1) // 0x00000001
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_is_sticky_DEF               (0x00000001)
  #define MC0_MCDECS_CBIT_ForceSREntry_dft_is_sticky_HSH               (0x0112D81C)

  #define MC0_MCDECS_CBIT_freeze_AFD_on_RTB_OFF                        (10)
  #define MC0_MCDECS_CBIT_freeze_AFD_on_RTB_WID                        ( 1)
  #define MC0_MCDECS_CBIT_freeze_AFD_on_RTB_MSK                        (0x00000400)
  #define MC0_MCDECS_CBIT_freeze_AFD_on_RTB_MIN                        (0)
  #define MC0_MCDECS_CBIT_freeze_AFD_on_RTB_MAX                        (1) // 0x00000001
  #define MC0_MCDECS_CBIT_freeze_AFD_on_RTB_DEF                        (0x00000000)
  #define MC0_MCDECS_CBIT_freeze_AFD_on_RTB_HSH                        (0x0114D81C)

  #define MC0_MCDECS_CBIT_iosfsb_keep_ISM_active_OFF                   (11)
  #define MC0_MCDECS_CBIT_iosfsb_keep_ISM_active_WID                   ( 1)
  #define MC0_MCDECS_CBIT_iosfsb_keep_ISM_active_MSK                   (0x00000800)
  #define MC0_MCDECS_CBIT_iosfsb_keep_ISM_active_MIN                   (0)
  #define MC0_MCDECS_CBIT_iosfsb_keep_ISM_active_MAX                   (1) // 0x00000001
  #define MC0_MCDECS_CBIT_iosfsb_keep_ISM_active_DEF                   (0x00000000)
  #define MC0_MCDECS_CBIT_iosfsb_keep_ISM_active_HSH                   (0x0116D81C)

  #define MC0_MCDECS_CBIT_ignoreRefBetweenSRX2SRE_OFF                  (12)
  #define MC0_MCDECS_CBIT_ignoreRefBetweenSRX2SRE_WID                  ( 1)
  #define MC0_MCDECS_CBIT_ignoreRefBetweenSRX2SRE_MSK                  (0x00001000)
  #define MC0_MCDECS_CBIT_ignoreRefBetweenSRX2SRE_MIN                  (0)
  #define MC0_MCDECS_CBIT_ignoreRefBetweenSRX2SRE_MAX                  (1) // 0x00000001
  #define MC0_MCDECS_CBIT_ignoreRefBetweenSRX2SRE_DEF                  (0x00000000)
  #define MC0_MCDECS_CBIT_ignoreRefBetweenSRX2SRE_HSH                  (0x0118D81C)

  #define MC0_MCDECS_CBIT_override_external_regs_ack_miss_cbit_OFF     (13)
  #define MC0_MCDECS_CBIT_override_external_regs_ack_miss_cbit_WID     ( 1)
  #define MC0_MCDECS_CBIT_override_external_regs_ack_miss_cbit_MSK     (0x00002000)
  #define MC0_MCDECS_CBIT_override_external_regs_ack_miss_cbit_MIN     (0)
  #define MC0_MCDECS_CBIT_override_external_regs_ack_miss_cbit_MAX     (1) // 0x00000001
  #define MC0_MCDECS_CBIT_override_external_regs_ack_miss_cbit_DEF     (0x00000000)
  #define MC0_MCDECS_CBIT_override_external_regs_ack_miss_cbit_HSH     (0x011AD81C)

  #define MC0_MCDECS_CBIT_dis_cmi_spec_req_early_valid_OFF             (14)
  #define MC0_MCDECS_CBIT_dis_cmi_spec_req_early_valid_WID             ( 1)
  #define MC0_MCDECS_CBIT_dis_cmi_spec_req_early_valid_MSK             (0x00004000)
  #define MC0_MCDECS_CBIT_dis_cmi_spec_req_early_valid_MIN             (0)
  #define MC0_MCDECS_CBIT_dis_cmi_spec_req_early_valid_MAX             (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_cmi_spec_req_early_valid_DEF             (0x00000000)
  #define MC0_MCDECS_CBIT_dis_cmi_spec_req_early_valid_HSH             (0x011CD81C)

  #define MC0_MCDECS_CBIT_Dis_EFLOW_fwd_data_hack_OFF                  (15)
  #define MC0_MCDECS_CBIT_Dis_EFLOW_fwd_data_hack_WID                  ( 1)
  #define MC0_MCDECS_CBIT_Dis_EFLOW_fwd_data_hack_MSK                  (0x00008000)
  #define MC0_MCDECS_CBIT_Dis_EFLOW_fwd_data_hack_MIN                  (0)
  #define MC0_MCDECS_CBIT_Dis_EFLOW_fwd_data_hack_MAX                  (1) // 0x00000001
  #define MC0_MCDECS_CBIT_Dis_EFLOW_fwd_data_hack_DEF                  (0x00000000)
  #define MC0_MCDECS_CBIT_Dis_EFLOW_fwd_data_hack_HSH                  (0x011ED81C)

  #define MC0_MCDECS_CBIT_freeze_visa_values_OFF                       (16)
  #define MC0_MCDECS_CBIT_freeze_visa_values_WID                       ( 1)
  #define MC0_MCDECS_CBIT_freeze_visa_values_MSK                       (0x00010000)
  #define MC0_MCDECS_CBIT_freeze_visa_values_MIN                       (0)
  #define MC0_MCDECS_CBIT_freeze_visa_values_MAX                       (1) // 0x00000001
  #define MC0_MCDECS_CBIT_freeze_visa_values_DEF                       (0x00000000)
  #define MC0_MCDECS_CBIT_freeze_visa_values_HSH                       (0x0120D81C)

  #define MC0_MCDECS_CBIT_freeze_visa_values_on_RTB_OFF                (17)
  #define MC0_MCDECS_CBIT_freeze_visa_values_on_RTB_WID                ( 1)
  #define MC0_MCDECS_CBIT_freeze_visa_values_on_RTB_MSK                (0x00020000)
  #define MC0_MCDECS_CBIT_freeze_visa_values_on_RTB_MIN                (0)
  #define MC0_MCDECS_CBIT_freeze_visa_values_on_RTB_MAX                (1) // 0x00000001
  #define MC0_MCDECS_CBIT_freeze_visa_values_on_RTB_DEF                (0x00000000)
  #define MC0_MCDECS_CBIT_freeze_visa_values_on_RTB_HSH                (0x0122D81C)

  #define MC0_MCDECS_CBIT_dis_cmi_ep_power_flows_OFF                   (18)
  #define MC0_MCDECS_CBIT_dis_cmi_ep_power_flows_WID                   ( 1)
  #define MC0_MCDECS_CBIT_dis_cmi_ep_power_flows_MSK                   (0x00040000)
  #define MC0_MCDECS_CBIT_dis_cmi_ep_power_flows_MIN                   (0)
  #define MC0_MCDECS_CBIT_dis_cmi_ep_power_flows_MAX                   (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_cmi_ep_power_flows_DEF                   (0x00000000)
  #define MC0_MCDECS_CBIT_dis_cmi_ep_power_flows_HSH                   (0x0124D81C)

  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_opcodes_OFF                (19)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_opcodes_WID                ( 1)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_opcodes_MSK                (0x00080000)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_opcodes_MIN                (0)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_opcodes_MAX                (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_opcodes_DEF                (0x00000000)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_opcodes_HSH                (0x0126D81C)

  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_bar_OFF                    (20)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_bar_WID                    ( 1)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_bar_MSK                    (0x00100000)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_bar_MIN                    (0)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_bar_MAX                    (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_bar_DEF                    (0x00000000)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_bar_HSH                    (0x0128D81C)

  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_range_OFF                  (21)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_range_WID                  ( 1)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_range_MSK                  (0x00200000)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_range_MIN                  (0)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_range_MAX                  (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_range_DEF                  (0x00000000)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_range_HSH                  (0x012AD81C)

  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_sai_OFF                    (22)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_sai_WID                    ( 1)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_sai_MSK                    (0x00400000)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_sai_MIN                    (0)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_sai_MAX                    (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_sai_DEF                    (0x00000000)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_error_sai_HSH                    (0x012CD81C)

  #define MC0_MCDECS_CBIT_delay_normal_mode_when_temp_read_OFF         (23)
  #define MC0_MCDECS_CBIT_delay_normal_mode_when_temp_read_WID         ( 1)
  #define MC0_MCDECS_CBIT_delay_normal_mode_when_temp_read_MSK         (0x00800000)
  #define MC0_MCDECS_CBIT_delay_normal_mode_when_temp_read_MIN         (0)
  #define MC0_MCDECS_CBIT_delay_normal_mode_when_temp_read_MAX         (1) // 0x00000001
  #define MC0_MCDECS_CBIT_delay_normal_mode_when_temp_read_DEF         (0x00000000)
  #define MC0_MCDECS_CBIT_delay_normal_mode_when_temp_read_HSH         (0x012ED81C)

  #define MC0_MCDECS_CBIT_dis_cmi_spec_rsp_cpl_early_valid_OFF         (24)
  #define MC0_MCDECS_CBIT_dis_cmi_spec_rsp_cpl_early_valid_WID         ( 1)
  #define MC0_MCDECS_CBIT_dis_cmi_spec_rsp_cpl_early_valid_MSK         (0x01000000)
  #define MC0_MCDECS_CBIT_dis_cmi_spec_rsp_cpl_early_valid_MIN         (0)
  #define MC0_MCDECS_CBIT_dis_cmi_spec_rsp_cpl_early_valid_MAX         (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_cmi_spec_rsp_cpl_early_valid_DEF         (0x00000000)
  #define MC0_MCDECS_CBIT_dis_cmi_spec_rsp_cpl_early_valid_HSH         (0x0130D81C)

  #define MC0_MCDECS_CBIT_force_sb_ep_clk_req_OFF                      (25)
  #define MC0_MCDECS_CBIT_force_sb_ep_clk_req_WID                      ( 1)
  #define MC0_MCDECS_CBIT_force_sb_ep_clk_req_MSK                      (0x02000000)
  #define MC0_MCDECS_CBIT_force_sb_ep_clk_req_MIN                      (0)
  #define MC0_MCDECS_CBIT_force_sb_ep_clk_req_MAX                      (1) // 0x00000001
  #define MC0_MCDECS_CBIT_force_sb_ep_clk_req_DEF                      (0x00000000)
  #define MC0_MCDECS_CBIT_force_sb_ep_clk_req_HSH                      (0x0132D81C)

  #define MC0_MCDECS_CBIT_dis_cmi_wr_rsp_OFF                           (26)
  #define MC0_MCDECS_CBIT_dis_cmi_wr_rsp_WID                           ( 1)
  #define MC0_MCDECS_CBIT_dis_cmi_wr_rsp_MSK                           (0x04000000)
  #define MC0_MCDECS_CBIT_dis_cmi_wr_rsp_MIN                           (0)
  #define MC0_MCDECS_CBIT_dis_cmi_wr_rsp_MAX                           (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_cmi_wr_rsp_DEF                           (0x00000000)
  #define MC0_MCDECS_CBIT_dis_cmi_wr_rsp_HSH                           (0x0134D81C)

  #define MC0_MCDECS_CBIT_dis_iosf_sb_clk_gate_OFF                     (27)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_clk_gate_WID                     ( 1)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_clk_gate_MSK                     (0x08000000)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_clk_gate_MIN                     (0)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_clk_gate_MAX                     (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_iosf_sb_clk_gate_DEF                     (0x00000000)
  #define MC0_MCDECS_CBIT_dis_iosf_sb_clk_gate_HSH                     (0x0136D81C)

  #define MC0_MCDECS_CBIT_dis_glbdrv_clk_gate_OFF                      (28)
  #define MC0_MCDECS_CBIT_dis_glbdrv_clk_gate_WID                      ( 1)
  #define MC0_MCDECS_CBIT_dis_glbdrv_clk_gate_MSK                      (0x10000000)
  #define MC0_MCDECS_CBIT_dis_glbdrv_clk_gate_MIN                      (0)
  #define MC0_MCDECS_CBIT_dis_glbdrv_clk_gate_MAX                      (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_glbdrv_clk_gate_DEF                      (0x00000000)
  #define MC0_MCDECS_CBIT_dis_glbdrv_clk_gate_HSH                      (0x0138D81C)

  #define MC0_MCDECS_CBIT_dis_reg_clk_gate_OFF                         (29)
  #define MC0_MCDECS_CBIT_dis_reg_clk_gate_WID                         ( 1)
  #define MC0_MCDECS_CBIT_dis_reg_clk_gate_MSK                         (0x20000000)
  #define MC0_MCDECS_CBIT_dis_reg_clk_gate_MIN                         (0)
  #define MC0_MCDECS_CBIT_dis_reg_clk_gate_MAX                         (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_reg_clk_gate_DEF                         (0x00000000)
  #define MC0_MCDECS_CBIT_dis_reg_clk_gate_HSH                         (0x013AD81C)

  #define MC0_MCDECS_CBIT_dis_clk_gate_OFF                             (31)
  #define MC0_MCDECS_CBIT_dis_clk_gate_WID                             ( 1)
  #define MC0_MCDECS_CBIT_dis_clk_gate_MSK                             (0x80000000)
  #define MC0_MCDECS_CBIT_dis_clk_gate_MIN                             (0)
  #define MC0_MCDECS_CBIT_dis_clk_gate_MAX                             (1) // 0x00000001
  #define MC0_MCDECS_CBIT_dis_clk_gate_DEF                             (0x00000000)
  #define MC0_MCDECS_CBIT_dis_clk_gate_HSH                             (0x013ED81C)

#define MC0_CHANNEL_HASH_REG                                           (0x0000D824)

  #define MC0_CHANNEL_HASH_HASH_MASK_OFF                               ( 6)
  #define MC0_CHANNEL_HASH_HASH_MASK_WID                               (14)
  #define MC0_CHANNEL_HASH_HASH_MASK_MSK                               (0x000FFFC0)
  #define MC0_CHANNEL_HASH_HASH_MASK_MIN                               (0)
  #define MC0_CHANNEL_HASH_HASH_MASK_MAX                               (16383) // 0x00003FFF
  #define MC0_CHANNEL_HASH_HASH_MASK_DEF                               (0x00000000)
  #define MC0_CHANNEL_HASH_HASH_MASK_HSH                               (0x0E0CD824)

  #define MC0_CHANNEL_HASH_HASH_LSB_MASK_BIT_OFF                       (24)
  #define MC0_CHANNEL_HASH_HASH_LSB_MASK_BIT_WID                       ( 3)
  #define MC0_CHANNEL_HASH_HASH_LSB_MASK_BIT_MSK                       (0x07000000)
  #define MC0_CHANNEL_HASH_HASH_LSB_MASK_BIT_MIN                       (0)
  #define MC0_CHANNEL_HASH_HASH_LSB_MASK_BIT_MAX                       (7) // 0x00000007
  #define MC0_CHANNEL_HASH_HASH_LSB_MASK_BIT_DEF                       (0x00000003)
  #define MC0_CHANNEL_HASH_HASH_LSB_MASK_BIT_HSH                       (0x0330D824)

  #define MC0_CHANNEL_HASH_HASH_MODE_OFF                               (28)
  #define MC0_CHANNEL_HASH_HASH_MODE_WID                               ( 1)
  #define MC0_CHANNEL_HASH_HASH_MODE_MSK                               (0x10000000)
  #define MC0_CHANNEL_HASH_HASH_MODE_MIN                               (0)
  #define MC0_CHANNEL_HASH_HASH_MODE_MAX                               (1) // 0x00000001
  #define MC0_CHANNEL_HASH_HASH_MODE_DEF                               (0x00000000)
  #define MC0_CHANNEL_HASH_HASH_MODE_HSH                               (0x0138D824)

#define MC0_CHANNEL_EHASH_REG                                          (0x0000D828)

  #define MC0_CHANNEL_EHASH_EHASH_MASK_OFF                             ( 6)
  #define MC0_CHANNEL_EHASH_EHASH_MASK_WID                             (14)
  #define MC0_CHANNEL_EHASH_EHASH_MASK_MSK                             (0x000FFFC0)
  #define MC0_CHANNEL_EHASH_EHASH_MASK_MIN                             (0)
  #define MC0_CHANNEL_EHASH_EHASH_MASK_MAX                             (16383) // 0x00003FFF
  #define MC0_CHANNEL_EHASH_EHASH_MASK_DEF                             (0x00000000)
  #define MC0_CHANNEL_EHASH_EHASH_MASK_HSH                             (0x0E0CD828)

  #define MC0_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_OFF                     (24)
  #define MC0_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_WID                     ( 3)
  #define MC0_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_MSK                     (0x07000000)
  #define MC0_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_MIN                     (0)
  #define MC0_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_MAX                     (7) // 0x00000007
  #define MC0_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_DEF                     (0x00000000)
  #define MC0_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_HSH                     (0x0330D828)

  #define MC0_CHANNEL_EHASH_EHASH_MODE_OFF                             (28)
  #define MC0_CHANNEL_EHASH_EHASH_MODE_WID                             ( 1)
  #define MC0_CHANNEL_EHASH_EHASH_MODE_MSK                             (0x10000000)
  #define MC0_CHANNEL_EHASH_EHASH_MODE_MIN                             (0)
  #define MC0_CHANNEL_EHASH_EHASH_MODE_MAX                             (1) // 0x00000001
  #define MC0_CHANNEL_EHASH_EHASH_MODE_DEF                             (0x00000000)
  #define MC0_CHANNEL_EHASH_EHASH_MODE_HSH                             (0x0138D828)

#define MC0_MC_INIT_STATE_G_REG                                        (0x0000D830)

  #define MC0_MC_INIT_STATE_G_ddr_reset_OFF                            ( 1)
  #define MC0_MC_INIT_STATE_G_ddr_reset_WID                            ( 1)
  #define MC0_MC_INIT_STATE_G_ddr_reset_MSK                            (0x00000002)
  #define MC0_MC_INIT_STATE_G_ddr_reset_MIN                            (0)
  #define MC0_MC_INIT_STATE_G_ddr_reset_MAX                            (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_ddr_reset_DEF                            (0x00000001)
  #define MC0_MC_INIT_STATE_G_ddr_reset_HSH                            (0x0102D830)

  #define MC0_MC_INIT_STATE_G_refresh_enable_OFF                       ( 3)
  #define MC0_MC_INIT_STATE_G_refresh_enable_WID                       ( 1)
  #define MC0_MC_INIT_STATE_G_refresh_enable_MSK                       (0x00000008)
  #define MC0_MC_INIT_STATE_G_refresh_enable_MIN                       (0)
  #define MC0_MC_INIT_STATE_G_refresh_enable_MAX                       (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_refresh_enable_DEF                       (0x00000000)
  #define MC0_MC_INIT_STATE_G_refresh_enable_HSH                       (0x0106D830)

  #define MC0_MC_INIT_STATE_G_mc_init_done_ack_OFF                     ( 5)
  #define MC0_MC_INIT_STATE_G_mc_init_done_ack_WID                     ( 1)
  #define MC0_MC_INIT_STATE_G_mc_init_done_ack_MSK                     (0x00000020)
  #define MC0_MC_INIT_STATE_G_mc_init_done_ack_MIN                     (0)
  #define MC0_MC_INIT_STATE_G_mc_init_done_ack_MAX                     (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_mc_init_done_ack_DEF                     (0x00000000)
  #define MC0_MC_INIT_STATE_G_mc_init_done_ack_HSH                     (0x010AD830)

  #define MC0_MC_INIT_STATE_G_mrc_done_OFF                             ( 7)
  #define MC0_MC_INIT_STATE_G_mrc_done_WID                             ( 1)
  #define MC0_MC_INIT_STATE_G_mrc_done_MSK                             (0x00000080)
  #define MC0_MC_INIT_STATE_G_mrc_done_MIN                             (0)
  #define MC0_MC_INIT_STATE_G_mrc_done_MAX                             (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_mrc_done_DEF                             (0x00000000)
  #define MC0_MC_INIT_STATE_G_mrc_done_HSH                             (0x010ED830)

  #define MC0_MC_INIT_STATE_G_pure_srx_OFF                             ( 9)
  #define MC0_MC_INIT_STATE_G_pure_srx_WID                             ( 1)
  #define MC0_MC_INIT_STATE_G_pure_srx_MSK                             (0x00000200)
  #define MC0_MC_INIT_STATE_G_pure_srx_MIN                             (0)
  #define MC0_MC_INIT_STATE_G_pure_srx_MAX                             (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_pure_srx_DEF                             (0x00000000)
  #define MC0_MC_INIT_STATE_G_pure_srx_HSH                             (0x0112D830)

  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_0_OFF                  (11)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_0_WID                  ( 1)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_0_MSK                  (0x00000800)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_0_MIN                  (0)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_0_MAX                  (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_0_DEF                  (0x00000000)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_0_HSH                  (0x0116D830)

  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_1_OFF                  (12)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_1_WID                  ( 1)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_1_MSK                  (0x00001000)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_1_MIN                  (0)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_1_MAX                  (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_1_DEF                  (0x00000000)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_1_HSH                  (0x0118D830)

  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_2_OFF                  (13)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_2_WID                  ( 1)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_2_MSK                  (0x00002000)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_2_MIN                  (0)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_2_MAX                  (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_2_DEF                  (0x00000000)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_2_HSH                  (0x011AD830)

  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_3_OFF                  (14)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_3_WID                  ( 1)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_3_MSK                  (0x00004000)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_3_MIN                  (0)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_3_MAX                  (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_3_DEF                  (0x00000000)
  #define MC0_MC_INIT_STATE_G_mrc_save_gv_point_3_HSH                  (0x011CD830)

  #define MC0_MC_INIT_STATE_G_dclk_enable_OFF                          (22)
  #define MC0_MC_INIT_STATE_G_dclk_enable_WID                          ( 1)
  #define MC0_MC_INIT_STATE_G_dclk_enable_MSK                          (0x00400000)
  #define MC0_MC_INIT_STATE_G_dclk_enable_MIN                          (0)
  #define MC0_MC_INIT_STATE_G_dclk_enable_MAX                          (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_dclk_enable_DEF                          (0x00000000)
  #define MC0_MC_INIT_STATE_G_dclk_enable_HSH                          (0x012CD830)

  #define MC0_MC_INIT_STATE_G_override_sr_enable_OFF                   (24)
  #define MC0_MC_INIT_STATE_G_override_sr_enable_WID                   ( 1)
  #define MC0_MC_INIT_STATE_G_override_sr_enable_MSK                   (0x01000000)
  #define MC0_MC_INIT_STATE_G_override_sr_enable_MIN                   (0)
  #define MC0_MC_INIT_STATE_G_override_sr_enable_MAX                   (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_override_sr_enable_DEF                   (0x00000000)
  #define MC0_MC_INIT_STATE_G_override_sr_enable_HSH                   (0x0130D830)

  #define MC0_MC_INIT_STATE_G_override_sr_enable_value_OFF             (25)
  #define MC0_MC_INIT_STATE_G_override_sr_enable_value_WID             ( 1)
  #define MC0_MC_INIT_STATE_G_override_sr_enable_value_MSK             (0x02000000)
  #define MC0_MC_INIT_STATE_G_override_sr_enable_value_MIN             (0)
  #define MC0_MC_INIT_STATE_G_override_sr_enable_value_MAX             (1) // 0x00000001
  #define MC0_MC_INIT_STATE_G_override_sr_enable_value_DEF             (0x00000000)
  #define MC0_MC_INIT_STATE_G_override_sr_enable_value_HSH             (0x0132D830)

#define MC0_MRC_REVISION_REG                                           (0x0000D834)

  #define MC0_MRC_REVISION_REVISION_OFF                                ( 0)
  #define MC0_MRC_REVISION_REVISION_WID                                (32)
  #define MC0_MRC_REVISION_REVISION_MSK                                (0xFFFFFFFF)
  #define MC0_MRC_REVISION_REVISION_MIN                                (0)
  #define MC0_MRC_REVISION_REVISION_MAX                                (4294967295) // 0xFFFFFFFF
  #define MC0_MRC_REVISION_REVISION_DEF                                (0x00000000)
  #define MC0_MRC_REVISION_REVISION_HSH                                (0x2000D834)

#define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_REG                       (0x0000D83C)

  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID0_OFF      ( 0)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID0_WID      ( 5)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID0_MSK      (0x0000001F)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID0_MIN      (0)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID0_MAX      (31) // 0x0000001F
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID0_DEF      (0x00000000)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID0_HSH      (0x0500D83C)

  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID1_OFF      ( 5)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID1_WID      ( 5)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID1_MSK      (0x000003E0)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID1_MIN      (0)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID1_MAX      (31) // 0x0000001F
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID1_DEF      (0x00000001)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID1_HSH      (0x050AD83C)

  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID2_OFF      (10)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID2_WID      ( 5)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID2_MSK      (0x00007C00)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID2_MIN      (0)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID2_MAX      (31) // 0x0000001F
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID2_DEF      (0x00000002)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID2_HSH      (0x0514D83C)

  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID3_OFF      (15)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID3_WID      ( 5)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID3_MSK      (0x000F8000)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID3_MIN      (0)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID3_MAX      (31) // 0x0000001F
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID3_DEF      (0x00000002)
  #define MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_CMI_Source_ID3_HSH      (0x051ED83C)

#define MC0_PWM_TOTAL_REQCOUNT_REG                                     (0x0000D840)

  #define MC0_PWM_TOTAL_REQCOUNT_count_OFF                             ( 0)
  #define MC0_PWM_TOTAL_REQCOUNT_count_WID                             (64)
  #define MC0_PWM_TOTAL_REQCOUNT_count_MSK                             (0xFFFFFFFFFFFFFFFFULL)
  #define MC0_PWM_TOTAL_REQCOUNT_count_MIN                             (0)
  #define MC0_PWM_TOTAL_REQCOUNT_count_MAX                             (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define MC0_PWM_TOTAL_REQCOUNT_count_DEF                             (0x00000000)
  #define MC0_PWM_TOTAL_REQCOUNT_count_HSH                             (0x4000D840)

#define MC0_PWM_PROGRAMMABLE_REQCOUNT_0_REG                            (0x0000D848)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC0_PWM_PROGRAMMABLE_REQCOUNT_1_REG                            (0x0000D850)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC0_PWM_RDCAS_COUNT_REG                                        (0x0000D858)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC0_PM_SREF_CONFIG_REG                                         (0x0000D860)

  #define MC0_PM_SREF_CONFIG_Idle_timer_OFF                            ( 0)
  #define MC0_PM_SREF_CONFIG_Idle_timer_WID                            (16)
  #define MC0_PM_SREF_CONFIG_Idle_timer_MSK                            (0x0000FFFF)
  #define MC0_PM_SREF_CONFIG_Idle_timer_MIN                            (0)
  #define MC0_PM_SREF_CONFIG_Idle_timer_MAX                            (65535) // 0x0000FFFF
  #define MC0_PM_SREF_CONFIG_Idle_timer_DEF                            (0x00000200)
  #define MC0_PM_SREF_CONFIG_Idle_timer_HSH                            (0x1000D860)

  #define MC0_PM_SREF_CONFIG_SR_Enable_OFF                             (16)
  #define MC0_PM_SREF_CONFIG_SR_Enable_WID                             ( 1)
  #define MC0_PM_SREF_CONFIG_SR_Enable_MSK                             (0x00010000)
  #define MC0_PM_SREF_CONFIG_SR_Enable_MIN                             (0)
  #define MC0_PM_SREF_CONFIG_SR_Enable_MAX                             (1) // 0x00000001
  #define MC0_PM_SREF_CONFIG_SR_Enable_DEF                             (0x00000001)
  #define MC0_PM_SREF_CONFIG_SR_Enable_HSH                             (0x0120D860)

  #define MC0_PM_SREF_CONFIG_delay_qsync_OFF                           (17)
  #define MC0_PM_SREF_CONFIG_delay_qsync_WID                           ( 2)
  #define MC0_PM_SREF_CONFIG_delay_qsync_MSK                           (0x00060000)
  #define MC0_PM_SREF_CONFIG_delay_qsync_MIN                           (0)
  #define MC0_PM_SREF_CONFIG_delay_qsync_MAX                           (3) // 0x00000003
  #define MC0_PM_SREF_CONFIG_delay_qsync_DEF                           (0x00000000)
  #define MC0_PM_SREF_CONFIG_delay_qsync_HSH                           (0x0222D860)

#define MC0_ATMC_STS_REG                                               (0x0000D864)

  #define MC0_ATMC_STS_VC1_WR_CNFLT_OFF                                ( 0)
  #define MC0_ATMC_STS_VC1_WR_CNFLT_WID                                ( 1)
  #define MC0_ATMC_STS_VC1_WR_CNFLT_MSK                                (0x00000001)
  #define MC0_ATMC_STS_VC1_WR_CNFLT_MIN                                (0)
  #define MC0_ATMC_STS_VC1_WR_CNFLT_MAX                                (1) // 0x00000001
  #define MC0_ATMC_STS_VC1_WR_CNFLT_DEF                                (0x00000000)
  #define MC0_ATMC_STS_VC1_WR_CNFLT_HSH                                (0x0100D864)

  #define MC0_ATMC_STS_VC1_RD_CNFLT_OFF                                ( 1)
  #define MC0_ATMC_STS_VC1_RD_CNFLT_WID                                ( 1)
  #define MC0_ATMC_STS_VC1_RD_CNFLT_MSK                                (0x00000002)
  #define MC0_ATMC_STS_VC1_RD_CNFLT_MIN                                (0)
  #define MC0_ATMC_STS_VC1_RD_CNFLT_MAX                                (1) // 0x00000001
  #define MC0_ATMC_STS_VC1_RD_CNFLT_DEF                                (0x00000000)
  #define MC0_ATMC_STS_VC1_RD_CNFLT_HSH                                (0x0102D864)

#define MC0_READ_OCCUPANCY_COUNT_REG                                   (0x0000D868)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC0_STALL_DRAIN_REG                                            (0x0000D874)

  #define MC0_STALL_DRAIN_stall_until_drain_OFF                        ( 0)
  #define MC0_STALL_DRAIN_stall_until_drain_WID                        ( 1)
  #define MC0_STALL_DRAIN_stall_until_drain_MSK                        (0x00000001)
  #define MC0_STALL_DRAIN_stall_until_drain_MIN                        (0)
  #define MC0_STALL_DRAIN_stall_until_drain_MAX                        (1) // 0x00000001
  #define MC0_STALL_DRAIN_stall_until_drain_DEF                        (0x00000000)
  #define MC0_STALL_DRAIN_stall_until_drain_HSH                        (0x0100D874)

  #define MC0_STALL_DRAIN_stall_input_OFF                              ( 1)
  #define MC0_STALL_DRAIN_stall_input_WID                              ( 1)
  #define MC0_STALL_DRAIN_stall_input_MSK                              (0x00000002)
  #define MC0_STALL_DRAIN_stall_input_MIN                              (0)
  #define MC0_STALL_DRAIN_stall_input_MAX                              (1) // 0x00000001
  #define MC0_STALL_DRAIN_stall_input_DEF                              (0x00000000)
  #define MC0_STALL_DRAIN_stall_input_HSH                              (0x0102D874)

  #define MC0_STALL_DRAIN_mc_drained_OFF                               ( 4)
  #define MC0_STALL_DRAIN_mc_drained_WID                               ( 1)
  #define MC0_STALL_DRAIN_mc_drained_MSK                               (0x00000010)
  #define MC0_STALL_DRAIN_mc_drained_MIN                               (0)
  #define MC0_STALL_DRAIN_mc_drained_MAX                               (1) // 0x00000001
  #define MC0_STALL_DRAIN_mc_drained_DEF                               (0x00000000)
  #define MC0_STALL_DRAIN_mc_drained_HSH                               (0x0108D874)

  #define MC0_STALL_DRAIN_sr_state_OFF                                 ( 8)
  #define MC0_STALL_DRAIN_sr_state_WID                                 ( 1)
  #define MC0_STALL_DRAIN_sr_state_MSK                                 (0x00000100)
  #define MC0_STALL_DRAIN_sr_state_MIN                                 (0)
  #define MC0_STALL_DRAIN_sr_state_MAX                                 (1) // 0x00000001
  #define MC0_STALL_DRAIN_sr_state_DEF                                 (0x00000000)
  #define MC0_STALL_DRAIN_sr_state_HSH                                 (0x0110D874)

  #define MC0_STALL_DRAIN_stall_state_OFF                              (12)
  #define MC0_STALL_DRAIN_stall_state_WID                              ( 1)
  #define MC0_STALL_DRAIN_stall_state_MSK                              (0x00001000)
  #define MC0_STALL_DRAIN_stall_state_MIN                              (0)
  #define MC0_STALL_DRAIN_stall_state_MAX                              (1) // 0x00000001
  #define MC0_STALL_DRAIN_stall_state_DEF                              (0x00000000)
  #define MC0_STALL_DRAIN_stall_state_HSH                              (0x0118D874)

#define MC0_IPC_MC_ARB_REG                                             (0x0000D878)

  #define MC0_IPC_MC_ARB_NonVC1Threshold_OFF                           ( 0)
  #define MC0_IPC_MC_ARB_NonVC1Threshold_WID                           ( 4)
  #define MC0_IPC_MC_ARB_NonVC1Threshold_MSK                           (0x0000000F)
  #define MC0_IPC_MC_ARB_NonVC1Threshold_MIN                           (0)
  #define MC0_IPC_MC_ARB_NonVC1Threshold_MAX                           (15) // 0x0000000F
  #define MC0_IPC_MC_ARB_NonVC1Threshold_DEF                           (0x00000004)
  #define MC0_IPC_MC_ARB_NonVC1Threshold_HSH                           (0x0400D878)

  #define MC0_IPC_MC_ARB_VC1RdThreshold_OFF                            ( 4)
  #define MC0_IPC_MC_ARB_VC1RdThreshold_WID                            ( 4)
  #define MC0_IPC_MC_ARB_VC1RdThreshold_MSK                            (0x000000F0)
  #define MC0_IPC_MC_ARB_VC1RdThreshold_MIN                            (0)
  #define MC0_IPC_MC_ARB_VC1RdThreshold_MAX                            (15) // 0x0000000F
  #define MC0_IPC_MC_ARB_VC1RdThreshold_DEF                            (0x00000004)
  #define MC0_IPC_MC_ARB_VC1RdThreshold_HSH                            (0x0408D878)

  #define MC0_IPC_MC_ARB_FixedRateEn_OFF                               ( 8)
  #define MC0_IPC_MC_ARB_FixedRateEn_WID                               ( 1)
  #define MC0_IPC_MC_ARB_FixedRateEn_MSK                               (0x00000100)
  #define MC0_IPC_MC_ARB_FixedRateEn_MIN                               (0)
  #define MC0_IPC_MC_ARB_FixedRateEn_MAX                               (1) // 0x00000001
  #define MC0_IPC_MC_ARB_FixedRateEn_DEF                               (0x00000000)
  #define MC0_IPC_MC_ARB_FixedRateEn_HSH                               (0x0110D878)

  #define MC0_IPC_MC_ARB_HIGH_PRIO_LIM_OFF                             ( 9)
  #define MC0_IPC_MC_ARB_HIGH_PRIO_LIM_WID                             ( 3)
  #define MC0_IPC_MC_ARB_HIGH_PRIO_LIM_MSK                             (0x00000E00)
  #define MC0_IPC_MC_ARB_HIGH_PRIO_LIM_MIN                             (0)
  #define MC0_IPC_MC_ARB_HIGH_PRIO_LIM_MAX                             (7) // 0x00000007
  #define MC0_IPC_MC_ARB_HIGH_PRIO_LIM_DEF                             (0x00000004)
  #define MC0_IPC_MC_ARB_HIGH_PRIO_LIM_HSH                             (0x0312D878)

  #define MC0_IPC_MC_ARB_LOW_PRIO_LIM_OFF                              (12)
  #define MC0_IPC_MC_ARB_LOW_PRIO_LIM_WID                              ( 3)
  #define MC0_IPC_MC_ARB_LOW_PRIO_LIM_MSK                              (0x00007000)
  #define MC0_IPC_MC_ARB_LOW_PRIO_LIM_MIN                              (0)
  #define MC0_IPC_MC_ARB_LOW_PRIO_LIM_MAX                              (7) // 0x00000007
  #define MC0_IPC_MC_ARB_LOW_PRIO_LIM_DEF                              (0x00000001)
  #define MC0_IPC_MC_ARB_LOW_PRIO_LIM_HSH                              (0x0318D878)

  #define MC0_IPC_MC_ARB_spare_OFF                                     (15)
  #define MC0_IPC_MC_ARB_spare_WID                                     ( 8)
  #define MC0_IPC_MC_ARB_spare_MSK                                     (0x007F8000)
  #define MC0_IPC_MC_ARB_spare_MIN                                     (0)
  #define MC0_IPC_MC_ARB_spare_MAX                                     (255) // 0x000000FF
  #define MC0_IPC_MC_ARB_spare_DEF                                     (0x00000000)
  #define MC0_IPC_MC_ARB_spare_HSH                                     (0x081ED878)

#define MC0_IPC_MC_DEC_ARB_REG                                         (0x0000D87C)
//Duplicate of MC0_IPC_MC_ARB_REG

#define MC0_QUEUE_CREDIT_C_REG                                         (0x0000D880)

  #define MC0_QUEUE_CREDIT_C_RPQ_count_OFF                             ( 0)
  #define MC0_QUEUE_CREDIT_C_RPQ_count_WID                             ( 6)
  #define MC0_QUEUE_CREDIT_C_RPQ_count_MSK                             (0x0000003F)
  #define MC0_QUEUE_CREDIT_C_RPQ_count_MIN                             (0)
  #define MC0_QUEUE_CREDIT_C_RPQ_count_MAX                             (63) // 0x0000003F
  #define MC0_QUEUE_CREDIT_C_RPQ_count_DEF                             (0x00000020)
  #define MC0_QUEUE_CREDIT_C_RPQ_count_HSH                             (0x0600D880)

  #define MC0_QUEUE_CREDIT_C_WPQ_count_OFF                             ( 8)
  #define MC0_QUEUE_CREDIT_C_WPQ_count_WID                             ( 7)
  #define MC0_QUEUE_CREDIT_C_WPQ_count_MSK                             (0x00007F00)
  #define MC0_QUEUE_CREDIT_C_WPQ_count_MIN                             (0)
  #define MC0_QUEUE_CREDIT_C_WPQ_count_MAX                             (127) // 0x0000007F
  #define MC0_QUEUE_CREDIT_C_WPQ_count_DEF                             (0x00000040)
  #define MC0_QUEUE_CREDIT_C_WPQ_count_HSH                             (0x0710D880)

  #define MC0_QUEUE_CREDIT_C_IPQ_count_OFF                             (16)
  #define MC0_QUEUE_CREDIT_C_IPQ_count_WID                             ( 5)
  #define MC0_QUEUE_CREDIT_C_IPQ_count_MSK                             (0x001F0000)
  #define MC0_QUEUE_CREDIT_C_IPQ_count_MIN                             (0)
  #define MC0_QUEUE_CREDIT_C_IPQ_count_MAX                             (31) // 0x0000001F
  #define MC0_QUEUE_CREDIT_C_IPQ_count_DEF                             (0x00000010)
  #define MC0_QUEUE_CREDIT_C_IPQ_count_HSH                             (0x0520D880)

  #define MC0_QUEUE_CREDIT_C_WPQ_MinSlotsToReq_OFF                     (21)
  #define MC0_QUEUE_CREDIT_C_WPQ_MinSlotsToReq_WID                     ( 4)
  #define MC0_QUEUE_CREDIT_C_WPQ_MinSlotsToReq_MSK                     (0x01E00000)
  #define MC0_QUEUE_CREDIT_C_WPQ_MinSlotsToReq_MIN                     (0)
  #define MC0_QUEUE_CREDIT_C_WPQ_MinSlotsToReq_MAX                     (15) // 0x0000000F
  #define MC0_QUEUE_CREDIT_C_WPQ_MinSlotsToReq_DEF                     (0x00000002)
  #define MC0_QUEUE_CREDIT_C_WPQ_MinSlotsToReq_HSH                     (0x042AD880)

  #define MC0_QUEUE_CREDIT_C_IPQ_MinSlotsToReq_OFF                     (25)
  #define MC0_QUEUE_CREDIT_C_IPQ_MinSlotsToReq_WID                     ( 3)
  #define MC0_QUEUE_CREDIT_C_IPQ_MinSlotsToReq_MSK                     (0x0E000000)
  #define MC0_QUEUE_CREDIT_C_IPQ_MinSlotsToReq_MIN                     (0)
  #define MC0_QUEUE_CREDIT_C_IPQ_MinSlotsToReq_MAX                     (7) // 0x00000007
  #define MC0_QUEUE_CREDIT_C_IPQ_MinSlotsToReq_DEF                     (0x00000002)
  #define MC0_QUEUE_CREDIT_C_IPQ_MinSlotsToReq_HSH                     (0x0332D880)

  #define MC0_QUEUE_CREDIT_C_RPQ_MinSlotsToReq_OFF                     (28)
  #define MC0_QUEUE_CREDIT_C_RPQ_MinSlotsToReq_WID                     ( 4)
  #define MC0_QUEUE_CREDIT_C_RPQ_MinSlotsToReq_MSK                     (0xF0000000)
  #define MC0_QUEUE_CREDIT_C_RPQ_MinSlotsToReq_MIN                     (0)
  #define MC0_QUEUE_CREDIT_C_RPQ_MinSlotsToReq_MAX                     (15) // 0x0000000F
  #define MC0_QUEUE_CREDIT_C_RPQ_MinSlotsToReq_DEF                     (0x00000002)
  #define MC0_QUEUE_CREDIT_C_RPQ_MinSlotsToReq_HSH                     (0x0438D880)

#define MC0_ECC_INJ_ADDR_COMPARE_REG                                   (0x0000D888)

  #define MC0_ECC_INJ_ADDR_COMPARE_Address_OFF                         ( 0)
  #define MC0_ECC_INJ_ADDR_COMPARE_Address_WID                         (33)
  #define MC0_ECC_INJ_ADDR_COMPARE_Address_MSK                         (0x00000001FFFFFFFFULL)
  #define MC0_ECC_INJ_ADDR_COMPARE_Address_MIN                         (0)
  #define MC0_ECC_INJ_ADDR_COMPARE_Address_MAX                         (8589934591ULL) // 0x1FFFFFFFF
  #define MC0_ECC_INJ_ADDR_COMPARE_Address_DEF                         (0x00000000)
  #define MC0_ECC_INJ_ADDR_COMPARE_Address_HSH                         (0x6100D888)

#define MC0_REMAPBASE_REG                                              (0x0000D890)

  #define MC0_REMAPBASE_REMAPBASE_OFF                                  (20)
  #define MC0_REMAPBASE_REMAPBASE_WID                                  (19)
  #define MC0_REMAPBASE_REMAPBASE_MSK                                  (0x0000007FFFF00000ULL)
  #define MC0_REMAPBASE_REMAPBASE_MIN                                  (0)
  #define MC0_REMAPBASE_REMAPBASE_MAX                                  (524287) // 0x0007FFFF
  #define MC0_REMAPBASE_REMAPBASE_DEF                                  (0x0007FFFF)
  #define MC0_REMAPBASE_REMAPBASE_HSH                                  (0x5328D890)

#define MC0_REMAPLIMIT_REG                                             (0x0000D898)

  #define MC0_REMAPLIMIT_REMAPLMT_OFF                                  (20)
  #define MC0_REMAPLIMIT_REMAPLMT_WID                                  (19)
  #define MC0_REMAPLIMIT_REMAPLMT_MSK                                  (0x0000007FFFF00000ULL)
  #define MC0_REMAPLIMIT_REMAPLMT_MIN                                  (0)
  #define MC0_REMAPLIMIT_REMAPLMT_MAX                                  (524287) // 0x0007FFFF
  #define MC0_REMAPLIMIT_REMAPLMT_DEF                                  (0x00000000)
  #define MC0_REMAPLIMIT_REMAPLMT_HSH                                  (0x5328D898)

#define MC0_PWM_WRCAS_COUNT_REG                                        (0x0000D8A0)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC0_PWM_COMMAND_COUNT_REG                                      (0x0000D8A8)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC0_PWM_NON_SR_COUNT_REG                                       (0x0000D8B0)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC0_TOLUD_REG                                                  (0x0000D8BC)

  #define MC0_TOLUD_TOLUD_OFF                                          (20)
  #define MC0_TOLUD_TOLUD_WID                                          (12)
  #define MC0_TOLUD_TOLUD_MSK                                          (0xFFF00000)
  #define MC0_TOLUD_TOLUD_MIN                                          (0)
  #define MC0_TOLUD_TOLUD_MAX                                          (4095) // 0x00000FFF
  #define MC0_TOLUD_TOLUD_DEF                                          (0x00000001)
  #define MC0_TOLUD_TOLUD_HSH                                          (0x0C28D8BC)

#define MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG                         (0x0000D900)

  #define MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_sys_addr_OFF              ( 0)
  #define MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_sys_addr_WID              (33)
  #define MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_sys_addr_MSK              (0x00000001FFFFFFFFULL)
  #define MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_sys_addr_MIN              (0)
  #define MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_sys_addr_MAX              (8589934591ULL) // 0x1FFFFFFFF
  #define MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_sys_addr_DEF              (0x00000000)
  #define MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_sys_addr_HSH              (0x6100D900)

#define MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_1_REG                         (0x0000D908)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC0_GDXC_DDR_SYS_ADD_FILTER_MATCH_0_REG                        (0x0000D910)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC0_GDXC_DDR_SYS_ADD_FILTER_MATCH_1_REG                        (0x0000D918)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC0_GDXC_DDR_SYS_ADD_TRIGGER_MASK_REG                          (0x0000D920)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC0_GDXC_DDR_SYS_ADD_TRIGGER_MATCH_REG                         (0x0000D928)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC0_SC_QOS_REG                                                 (0x0000D930)

  #define MC0_SC_QOS_Isoch_time_window_OFF                             ( 0)
  #define MC0_SC_QOS_Isoch_time_window_WID                             (17)
  #define MC0_SC_QOS_Isoch_time_window_MSK                             (0x0001FFFF)
  #define MC0_SC_QOS_Isoch_time_window_MIN                             (0)
  #define MC0_SC_QOS_Isoch_time_window_MAX                             (131071) // 0x0001FFFF
  #define MC0_SC_QOS_Isoch_time_window_DEF                             (0x000001C2)
  #define MC0_SC_QOS_Isoch_time_window_HSH                             (0x5100D930)

  #define MC0_SC_QOS_Write_starvation_window_OFF                       (17)
  #define MC0_SC_QOS_Write_starvation_window_WID                       (11)
  #define MC0_SC_QOS_Write_starvation_window_MSK                       (0x0FFE0000)
  #define MC0_SC_QOS_Write_starvation_window_MIN                       (0)
  #define MC0_SC_QOS_Write_starvation_window_MAX                       (2047) // 0x000007FF
  #define MC0_SC_QOS_Write_starvation_window_DEF                       (0x0000005A)
  #define MC0_SC_QOS_Write_starvation_window_HSH                       (0x4B22D930)

  #define MC0_SC_QOS_VC1_Read_starvation_en_OFF                        (28)
  #define MC0_SC_QOS_VC1_Read_starvation_en_WID                        ( 1)
  #define MC0_SC_QOS_VC1_Read_starvation_en_MSK                        (0x10000000)
  #define MC0_SC_QOS_VC1_Read_starvation_en_MIN                        (0)
  #define MC0_SC_QOS_VC1_Read_starvation_en_MAX                        (1) // 0x00000001
  #define MC0_SC_QOS_VC1_Read_starvation_en_DEF                        (0x00000001)
  #define MC0_SC_QOS_VC1_Read_starvation_en_HSH                        (0x4138D930)

  #define MC0_SC_QOS_Write_starvation_in_Isoc_en_OFF                   (29)
  #define MC0_SC_QOS_Write_starvation_in_Isoc_en_WID                   ( 1)
  #define MC0_SC_QOS_Write_starvation_in_Isoc_en_MSK                   (0x20000000)
  #define MC0_SC_QOS_Write_starvation_in_Isoc_en_MIN                   (0)
  #define MC0_SC_QOS_Write_starvation_in_Isoc_en_MAX                   (1) // 0x00000001
  #define MC0_SC_QOS_Write_starvation_in_Isoc_en_DEF                   (0x00000001)
  #define MC0_SC_QOS_Write_starvation_in_Isoc_en_HSH                   (0x413AD930)

  #define MC0_SC_QOS_Read_starvation_in_Isoch_en_OFF                   (30)
  #define MC0_SC_QOS_Read_starvation_in_Isoch_en_WID                   ( 1)
  #define MC0_SC_QOS_Read_starvation_in_Isoch_en_MSK                   (0x40000000)
  #define MC0_SC_QOS_Read_starvation_in_Isoch_en_MIN                   (0)
  #define MC0_SC_QOS_Read_starvation_in_Isoch_en_MAX                   (1) // 0x00000001
  #define MC0_SC_QOS_Read_starvation_in_Isoch_en_DEF                   (0x00000001)
  #define MC0_SC_QOS_Read_starvation_in_Isoch_en_HSH                   (0x413CD930)

  #define MC0_SC_QOS_VC0_counter_disable_OFF                           (31)
  #define MC0_SC_QOS_VC0_counter_disable_WID                           ( 1)
  #define MC0_SC_QOS_VC0_counter_disable_MSK                           (0x80000000)
  #define MC0_SC_QOS_VC0_counter_disable_MIN                           (0)
  #define MC0_SC_QOS_VC0_counter_disable_MAX                           (1) // 0x00000001
  #define MC0_SC_QOS_VC0_counter_disable_DEF                           (0x00000000)
  #define MC0_SC_QOS_VC0_counter_disable_HSH                           (0x413ED930)

  #define MC0_SC_QOS_Read_starvation_window_OFF                        (32)
  #define MC0_SC_QOS_Read_starvation_window_WID                        (11)
  #define MC0_SC_QOS_Read_starvation_window_MSK                        (0x000007FF00000000ULL)
  #define MC0_SC_QOS_Read_starvation_window_MIN                        (0)
  #define MC0_SC_QOS_Read_starvation_window_MAX                        (2047) // 0x000007FF
  #define MC0_SC_QOS_Read_starvation_window_DEF                        (0x0000005A)
  #define MC0_SC_QOS_Read_starvation_window_HSH                        (0x4B40D930)

  #define MC0_SC_QOS_VC0_read_count_OFF                                (43)
  #define MC0_SC_QOS_VC0_read_count_WID                                ( 9)
  #define MC0_SC_QOS_VC0_read_count_MSK                                (0x000FF80000000000ULL)
  #define MC0_SC_QOS_VC0_read_count_MIN                                (0)
  #define MC0_SC_QOS_VC0_read_count_MAX                                (511) // 0x000001FF
  #define MC0_SC_QOS_VC0_read_count_DEF                                (0x00000016)
  #define MC0_SC_QOS_VC0_read_count_HSH                                (0x4956D930)

  #define MC0_SC_QOS_Force_MCVC1Demote_OFF                             (52)
  #define MC0_SC_QOS_Force_MCVC1Demote_WID                             ( 1)
  #define MC0_SC_QOS_Force_MCVC1Demote_MSK                             (0x0010000000000000ULL)
  #define MC0_SC_QOS_Force_MCVC1Demote_MIN                             (0)
  #define MC0_SC_QOS_Force_MCVC1Demote_MAX                             (1) // 0x00000001
  #define MC0_SC_QOS_Force_MCVC1Demote_DEF                             (0x00000000)
  #define MC0_SC_QOS_Force_MCVC1Demote_HSH                             (0x4168D930)

  #define MC0_SC_QOS_Disable_MCVC1Demote_OFF                           (53)
  #define MC0_SC_QOS_Disable_MCVC1Demote_WID                           ( 1)
  #define MC0_SC_QOS_Disable_MCVC1Demote_MSK                           (0x0020000000000000ULL)
  #define MC0_SC_QOS_Disable_MCVC1Demote_MIN                           (0)
  #define MC0_SC_QOS_Disable_MCVC1Demote_MAX                           (1) // 0x00000001
  #define MC0_SC_QOS_Disable_MCVC1Demote_DEF                           (0x00000000)
  #define MC0_SC_QOS_Disable_MCVC1Demote_HSH                           (0x416AD930)

  #define MC0_SC_QOS_MC_Ignore_VC1Demote_OFF                           (54)
  #define MC0_SC_QOS_MC_Ignore_VC1Demote_WID                           ( 1)
  #define MC0_SC_QOS_MC_Ignore_VC1Demote_MSK                           (0x0040000000000000ULL)
  #define MC0_SC_QOS_MC_Ignore_VC1Demote_MIN                           (0)
  #define MC0_SC_QOS_MC_Ignore_VC1Demote_MAX                           (1) // 0x00000001
  #define MC0_SC_QOS_MC_Ignore_VC1Demote_DEF                           (0x00000000)
  #define MC0_SC_QOS_MC_Ignore_VC1Demote_HSH                           (0x416CD930)

  #define MC0_SC_QOS_Ignore_RGBSync_OFF                                (55)
  #define MC0_SC_QOS_Ignore_RGBSync_WID                                ( 1)
  #define MC0_SC_QOS_Ignore_RGBSync_MSK                                (0x0080000000000000ULL)
  #define MC0_SC_QOS_Ignore_RGBSync_MIN                                (0)
  #define MC0_SC_QOS_Ignore_RGBSync_MAX                                (1) // 0x00000001
  #define MC0_SC_QOS_Ignore_RGBSync_DEF                                (0x00000000)
  #define MC0_SC_QOS_Ignore_RGBSync_HSH                                (0x416ED930)

  #define MC0_SC_QOS_Force_MC_WPriority_OFF                            (56)
  #define MC0_SC_QOS_Force_MC_WPriority_WID                            ( 1)
  #define MC0_SC_QOS_Force_MC_WPriority_MSK                            (0x0100000000000000ULL)
  #define MC0_SC_QOS_Force_MC_WPriority_MIN                            (0)
  #define MC0_SC_QOS_Force_MC_WPriority_MAX                            (1) // 0x00000001
  #define MC0_SC_QOS_Force_MC_WPriority_DEF                            (0x00000000)
  #define MC0_SC_QOS_Force_MC_WPriority_HSH                            (0x4170D930)

  #define MC0_SC_QOS_Disable_MC_WPriority_OFF                          (57)
  #define MC0_SC_QOS_Disable_MC_WPriority_WID                          ( 1)
  #define MC0_SC_QOS_Disable_MC_WPriority_MSK                          (0x0200000000000000ULL)
  #define MC0_SC_QOS_Disable_MC_WPriority_MIN                          (0)
  #define MC0_SC_QOS_Disable_MC_WPriority_MAX                          (1) // 0x00000001
  #define MC0_SC_QOS_Disable_MC_WPriority_DEF                          (0x00000000)
  #define MC0_SC_QOS_Disable_MC_WPriority_HSH                          (0x4172D930)

  #define MC0_SC_QOS_allow_cross_vc_blocking_OFF                       (58)
  #define MC0_SC_QOS_allow_cross_vc_blocking_WID                       ( 1)
  #define MC0_SC_QOS_allow_cross_vc_blocking_MSK                       (0x0400000000000000ULL)
  #define MC0_SC_QOS_allow_cross_vc_blocking_MIN                       (0)
  #define MC0_SC_QOS_allow_cross_vc_blocking_MAX                       (1) // 0x00000001
  #define MC0_SC_QOS_allow_cross_vc_blocking_DEF                       (0x00000001)
  #define MC0_SC_QOS_allow_cross_vc_blocking_HSH                       (0x4174D930)

  #define MC0_SC_QOS_VC1_block_VC0_OFF                                 (59)
  #define MC0_SC_QOS_VC1_block_VC0_WID                                 ( 1)
  #define MC0_SC_QOS_VC1_block_VC0_MSK                                 (0x0800000000000000ULL)
  #define MC0_SC_QOS_VC1_block_VC0_MIN                                 (0)
  #define MC0_SC_QOS_VC1_block_VC0_MAX                                 (1) // 0x00000001
  #define MC0_SC_QOS_VC1_block_VC0_DEF                                 (0x00000000)
  #define MC0_SC_QOS_VC1_block_VC0_HSH                                 (0x4176D930)

  #define MC0_SC_QOS_VC0_block_VC1_OFF                                 (60)
  #define MC0_SC_QOS_VC0_block_VC1_WID                                 ( 1)
  #define MC0_SC_QOS_VC0_block_VC1_MSK                                 (0x1000000000000000ULL)
  #define MC0_SC_QOS_VC0_block_VC1_MIN                                 (0)
  #define MC0_SC_QOS_VC0_block_VC1_MAX                                 (1) // 0x00000001
  #define MC0_SC_QOS_VC0_block_VC1_DEF                                 (0x00000000)
  #define MC0_SC_QOS_VC0_block_VC1_HSH                                 (0x4178D930)

  #define MC0_SC_QOS_Delay_VC1_on_read_starvation_OFF                  (61)
  #define MC0_SC_QOS_Delay_VC1_on_read_starvation_WID                  ( 1)
  #define MC0_SC_QOS_Delay_VC1_on_read_starvation_MSK                  (0x2000000000000000ULL)
  #define MC0_SC_QOS_Delay_VC1_on_read_starvation_MIN                  (0)
  #define MC0_SC_QOS_Delay_VC1_on_read_starvation_MAX                  (1) // 0x00000001
  #define MC0_SC_QOS_Delay_VC1_on_read_starvation_DEF                  (0x00000000)
  #define MC0_SC_QOS_Delay_VC1_on_read_starvation_HSH                  (0x417AD930)

#define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_REG                         (0x0000D938)

  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_GRACE_CNT_OFF         ( 0)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_GRACE_CNT_WID         ( 8)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_GRACE_CNT_MSK         (0x000000FF)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_GRACE_CNT_MIN         (0)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_GRACE_CNT_MAX         (255) // 0x000000FF
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_GRACE_CNT_DEF         (0x0000000F)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_GRACE_CNT_HSH         (0x0800D938)

  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_DRV_GATE_DIS_OFF      ( 8)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_DRV_GATE_DIS_WID      ( 1)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_DRV_GATE_DIS_MSK      (0x00000100)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_DRV_GATE_DIS_MIN      (0)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_DRV_GATE_DIS_MAX      (1) // 0x00000001
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_DRV_GATE_DIS_DEF      (0x00000000)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_GLB_DRV_GATE_DIS_HSH      (0x0110D938)

  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_qclk_global_driver_override_to_dclk_OFF ( 9)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_qclk_global_driver_override_to_dclk_WID ( 1)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_qclk_global_driver_override_to_dclk_MSK (0x00000200)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_qclk_global_driver_override_to_dclk_MIN (0)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_qclk_global_driver_override_to_dclk_MAX (1) // 0x00000001
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_qclk_global_driver_override_to_dclk_DEF (0x00000000)
  #define MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_qclk_global_driver_override_to_dclk_HSH (0x0112D938)

#define MC0_PM_CONTROL_REG                                             (0x0000D93C)

  #define MC0_PM_CONTROL_delay_PM_Ack_cycles_OFF                       ( 0)
  #define MC0_PM_CONTROL_delay_PM_Ack_cycles_WID                       (10)
  #define MC0_PM_CONTROL_delay_PM_Ack_cycles_MSK                       (0x000003FF)
  #define MC0_PM_CONTROL_delay_PM_Ack_cycles_MIN                       (0)
  #define MC0_PM_CONTROL_delay_PM_Ack_cycles_MAX                       (1023) // 0x000003FF
  #define MC0_PM_CONTROL_delay_PM_Ack_cycles_DEF                       (0x00000010)
  #define MC0_PM_CONTROL_delay_PM_Ack_cycles_HSH                       (0x0A00D93C)

  #define MC0_PM_CONTROL_R1_Ack_reset_bgf_run_OFF                      (10)
  #define MC0_PM_CONTROL_R1_Ack_reset_bgf_run_WID                      ( 1)
  #define MC0_PM_CONTROL_R1_Ack_reset_bgf_run_MSK                      (0x00000400)
  #define MC0_PM_CONTROL_R1_Ack_reset_bgf_run_MIN                      (0)
  #define MC0_PM_CONTROL_R1_Ack_reset_bgf_run_MAX                      (1) // 0x00000001
  #define MC0_PM_CONTROL_R1_Ack_reset_bgf_run_DEF                      (0x00000001)
  #define MC0_PM_CONTROL_R1_Ack_reset_bgf_run_HSH                      (0x0114D93C)

  #define MC0_PM_CONTROL_SAGV_Ack_reset_bgf_run_OFF                    (11)
  #define MC0_PM_CONTROL_SAGV_Ack_reset_bgf_run_WID                    ( 1)
  #define MC0_PM_CONTROL_SAGV_Ack_reset_bgf_run_MSK                    (0x00000800)
  #define MC0_PM_CONTROL_SAGV_Ack_reset_bgf_run_MIN                    (0)
  #define MC0_PM_CONTROL_SAGV_Ack_reset_bgf_run_MAX                    (1) // 0x00000001
  #define MC0_PM_CONTROL_SAGV_Ack_reset_bgf_run_DEF                    (0x00000001)
  #define MC0_PM_CONTROL_SAGV_Ack_reset_bgf_run_HSH                    (0x0116D93C)

  #define MC0_PM_CONTROL_Dclk_en_reset_bgf_run_OFF                     (12)
  #define MC0_PM_CONTROL_Dclk_en_reset_bgf_run_WID                     ( 1)
  #define MC0_PM_CONTROL_Dclk_en_reset_bgf_run_MSK                     (0x00001000)
  #define MC0_PM_CONTROL_Dclk_en_reset_bgf_run_MIN                     (0)
  #define MC0_PM_CONTROL_Dclk_en_reset_bgf_run_MAX                     (1) // 0x00000001
  #define MC0_PM_CONTROL_Dclk_en_reset_bgf_run_DEF                     (0x00000001)
  #define MC0_PM_CONTROL_Dclk_en_reset_bgf_run_HSH                     (0x0118D93C)

  #define MC0_PM_CONTROL_PM_REQ_R0_received_OFF                        (16)
  #define MC0_PM_CONTROL_PM_REQ_R0_received_WID                        ( 1)
  #define MC0_PM_CONTROL_PM_REQ_R0_received_MSK                        (0x00010000)
  #define MC0_PM_CONTROL_PM_REQ_R0_received_MIN                        (0)
  #define MC0_PM_CONTROL_PM_REQ_R0_received_MAX                        (1) // 0x00000001
  #define MC0_PM_CONTROL_PM_REQ_R0_received_DEF                        (0x00000000)
  #define MC0_PM_CONTROL_PM_REQ_R0_received_HSH                        (0x0120D93C)

  #define MC0_PM_CONTROL_PM_REQ_R0_SAGV_received_OFF                   (17)
  #define MC0_PM_CONTROL_PM_REQ_R0_SAGV_received_WID                   ( 1)
  #define MC0_PM_CONTROL_PM_REQ_R0_SAGV_received_MSK                   (0x00020000)
  #define MC0_PM_CONTROL_PM_REQ_R0_SAGV_received_MIN                   (0)
  #define MC0_PM_CONTROL_PM_REQ_R0_SAGV_received_MAX                   (1) // 0x00000001
  #define MC0_PM_CONTROL_PM_REQ_R0_SAGV_received_DEF                   (0x00000000)
  #define MC0_PM_CONTROL_PM_REQ_R0_SAGV_received_HSH                   (0x0122D93C)

  #define MC0_PM_CONTROL_PM_REQ_R1_received_OFF                        (18)
  #define MC0_PM_CONTROL_PM_REQ_R1_received_WID                        ( 1)
  #define MC0_PM_CONTROL_PM_REQ_R1_received_MSK                        (0x00040000)
  #define MC0_PM_CONTROL_PM_REQ_R1_received_MIN                        (0)
  #define MC0_PM_CONTROL_PM_REQ_R1_received_MAX                        (1) // 0x00000001
  #define MC0_PM_CONTROL_PM_REQ_R1_received_DEF                        (0x00000000)
  #define MC0_PM_CONTROL_PM_REQ_R1_received_HSH                        (0x0124D93C)

  #define MC0_PM_CONTROL_PM_REQ_R1_deep_sr_received_OFF                (19)
  #define MC0_PM_CONTROL_PM_REQ_R1_deep_sr_received_WID                ( 1)
  #define MC0_PM_CONTROL_PM_REQ_R1_deep_sr_received_MSK                (0x00080000)
  #define MC0_PM_CONTROL_PM_REQ_R1_deep_sr_received_MIN                (0)
  #define MC0_PM_CONTROL_PM_REQ_R1_deep_sr_received_MAX                (1) // 0x00000001
  #define MC0_PM_CONTROL_PM_REQ_R1_deep_sr_received_DEF                (0x00000000)
  #define MC0_PM_CONTROL_PM_REQ_R1_deep_sr_received_HSH                (0x0126D93C)

  #define MC0_PM_CONTROL_PM_REQ_UnBlock_received_OFF                   (20)
  #define MC0_PM_CONTROL_PM_REQ_UnBlock_received_WID                   ( 1)
  #define MC0_PM_CONTROL_PM_REQ_UnBlock_received_MSK                   (0x00100000)
  #define MC0_PM_CONTROL_PM_REQ_UnBlock_received_MIN                   (0)
  #define MC0_PM_CONTROL_PM_REQ_UnBlock_received_MAX                   (1) // 0x00000001
  #define MC0_PM_CONTROL_PM_REQ_UnBlock_received_DEF                   (0x00000000)
  #define MC0_PM_CONTROL_PM_REQ_UnBlock_received_HSH                   (0x0128D93C)

  #define MC0_PM_CONTROL_PM_REQ_Acked_OFF                              (21)
  #define MC0_PM_CONTROL_PM_REQ_Acked_WID                              ( 1)
  #define MC0_PM_CONTROL_PM_REQ_Acked_MSK                              (0x00200000)
  #define MC0_PM_CONTROL_PM_REQ_Acked_MIN                              (0)
  #define MC0_PM_CONTROL_PM_REQ_Acked_MAX                              (1) // 0x00000001
  #define MC0_PM_CONTROL_PM_REQ_Acked_DEF                              (0x00000000)
  #define MC0_PM_CONTROL_PM_REQ_Acked_HSH                              (0x012AD93C)

  #define MC0_PM_CONTROL_MC_State_blocked_R0_OFF                       (22)
  #define MC0_PM_CONTROL_MC_State_blocked_R0_WID                       ( 1)
  #define MC0_PM_CONTROL_MC_State_blocked_R0_MSK                       (0x00400000)
  #define MC0_PM_CONTROL_MC_State_blocked_R0_MIN                       (0)
  #define MC0_PM_CONTROL_MC_State_blocked_R0_MAX                       (1) // 0x00000001
  #define MC0_PM_CONTROL_MC_State_blocked_R0_DEF                       (0x00000000)
  #define MC0_PM_CONTROL_MC_State_blocked_R0_HSH                       (0x012CD93C)

  #define MC0_PM_CONTROL_MC_State_blocked_R1_OFF                       (23)
  #define MC0_PM_CONTROL_MC_State_blocked_R1_WID                       ( 1)
  #define MC0_PM_CONTROL_MC_State_blocked_R1_MSK                       (0x00800000)
  #define MC0_PM_CONTROL_MC_State_blocked_R1_MIN                       (0)
  #define MC0_PM_CONTROL_MC_State_blocked_R1_MAX                       (1) // 0x00000001
  #define MC0_PM_CONTROL_MC_State_blocked_R1_DEF                       (0x00000001)
  #define MC0_PM_CONTROL_MC_State_blocked_R1_HSH                       (0x012ED93C)

  #define MC0_PM_CONTROL_Override_DDRPLDrainDone_Cbit_OFF              (24)
  #define MC0_PM_CONTROL_Override_DDRPLDrainDone_Cbit_WID              ( 1)
  #define MC0_PM_CONTROL_Override_DDRPLDrainDone_Cbit_MSK              (0x01000000)
  #define MC0_PM_CONTROL_Override_DDRPLDrainDone_Cbit_MIN              (0)
  #define MC0_PM_CONTROL_Override_DDRPLDrainDone_Cbit_MAX              (1) // 0x00000001
  #define MC0_PM_CONTROL_Override_DDRPLDrainDone_Cbit_DEF              (0x00000000)
  #define MC0_PM_CONTROL_Override_DDRPLDrainDone_Cbit_HSH              (0x0130D93C)

#define MC0_PWM_COUNTERS_DURATION_REG                                  (0x0000D948)

  #define MC0_PWM_COUNTERS_DURATION_Count_OFF                          ( 0)
  #define MC0_PWM_COUNTERS_DURATION_Count_WID                          (64)
  #define MC0_PWM_COUNTERS_DURATION_Count_MSK                          (0xFFFFFFFFFFFFFFFFULL)
  #define MC0_PWM_COUNTERS_DURATION_Count_MIN                          (0)
  #define MC0_PWM_COUNTERS_DURATION_Count_MAX                          (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define MC0_PWM_COUNTERS_DURATION_Count_DEF                          (0x00000000)
  #define MC0_PWM_COUNTERS_DURATION_Count_HSH                          (0x4000D948)

#define MC0_MCDECS_SECOND_CBIT_REG                                     (0x0000D954)

  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_enable_OFF              ( 0)
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_enable_WID              ( 1)
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_enable_MSK              (0x00000001)
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_enable_MIN              (0)
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_enable_MAX              (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_enable_DEF              (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_enable_HSH              (0x0100D954)

  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_phase_OFF               ( 1)
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_phase_WID               ( 1)
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_phase_MSK               (0x00000002)
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_phase_MIN               (0)
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_phase_MAX               (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_phase_DEF               (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_cmi_req_stall_phase_HSH               (0x0102D954)

  #define MC0_MCDECS_SECOND_CBIT_block_scheduler_OFF                   ( 2)
  #define MC0_MCDECS_SECOND_CBIT_block_scheduler_WID                   ( 4)
  #define MC0_MCDECS_SECOND_CBIT_block_scheduler_MSK                   (0x0000003C)
  #define MC0_MCDECS_SECOND_CBIT_block_scheduler_MIN                   (0)
  #define MC0_MCDECS_SECOND_CBIT_block_scheduler_MAX                   (15) // 0x0000000F
  #define MC0_MCDECS_SECOND_CBIT_block_scheduler_DEF                   (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_block_scheduler_HSH                   (0x0404D954)

  #define MC0_MCDECS_SECOND_CBIT_pwm_clock_enable_OFF                  ( 6)
  #define MC0_MCDECS_SECOND_CBIT_pwm_clock_enable_WID                  ( 1)
  #define MC0_MCDECS_SECOND_CBIT_pwm_clock_enable_MSK                  (0x00000040)
  #define MC0_MCDECS_SECOND_CBIT_pwm_clock_enable_MIN                  (0)
  #define MC0_MCDECS_SECOND_CBIT_pwm_clock_enable_MAX                  (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_pwm_clock_enable_DEF                  (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_pwm_clock_enable_HSH                  (0x010CD954)

  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_OFF                         ( 7)
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_WID                         ( 1)
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_MSK                         (0x00000080)
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_MIN                         (0)
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_MAX                         (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_DEF                         (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_HSH                         (0x010ED954)

  #define MC0_MCDECS_SECOND_CBIT_dis_other_mc_stolen_ref_OFF           ( 8)
  #define MC0_MCDECS_SECOND_CBIT_dis_other_mc_stolen_ref_WID           ( 1)
  #define MC0_MCDECS_SECOND_CBIT_dis_other_mc_stolen_ref_MSK           (0x00000100)
  #define MC0_MCDECS_SECOND_CBIT_dis_other_mc_stolen_ref_MIN           (0)
  #define MC0_MCDECS_SECOND_CBIT_dis_other_mc_stolen_ref_MAX           (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_dis_other_mc_stolen_ref_DEF           (0x00000001)
  #define MC0_MCDECS_SECOND_CBIT_dis_other_mc_stolen_ref_HSH           (0x0110D954)

  #define MC0_MCDECS_SECOND_CBIT_allow_blockack_on_pending_srx_OFF     ( 9)
  #define MC0_MCDECS_SECOND_CBIT_allow_blockack_on_pending_srx_WID     ( 1)
  #define MC0_MCDECS_SECOND_CBIT_allow_blockack_on_pending_srx_MSK     (0x00000200)
  #define MC0_MCDECS_SECOND_CBIT_allow_blockack_on_pending_srx_MIN     (0)
  #define MC0_MCDECS_SECOND_CBIT_allow_blockack_on_pending_srx_MAX     (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_allow_blockack_on_pending_srx_DEF     (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_allow_blockack_on_pending_srx_HSH     (0x0112D954)

  #define MC0_MCDECS_SECOND_CBIT_spare_OFF                             (10)
  #define MC0_MCDECS_SECOND_CBIT_spare_WID                             ( 1)
  #define MC0_MCDECS_SECOND_CBIT_spare_MSK                             (0x00000400)
  #define MC0_MCDECS_SECOND_CBIT_spare_MIN                             (0)
  #define MC0_MCDECS_SECOND_CBIT_spare_MAX                             (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_spare_DEF                             (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_spare_HSH                             (0x0114D954)

  #define MC0_MCDECS_SECOND_CBIT_two_cyc_early_ckstop_dis_OFF          (11)
  #define MC0_MCDECS_SECOND_CBIT_two_cyc_early_ckstop_dis_WID          ( 1)
  #define MC0_MCDECS_SECOND_CBIT_two_cyc_early_ckstop_dis_MSK          (0x00000800)
  #define MC0_MCDECS_SECOND_CBIT_two_cyc_early_ckstop_dis_MIN          (0)
  #define MC0_MCDECS_SECOND_CBIT_two_cyc_early_ckstop_dis_MAX          (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_two_cyc_early_ckstop_dis_DEF          (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_two_cyc_early_ckstop_dis_HSH          (0x0116D954)

  #define MC0_MCDECS_SECOND_CBIT_dis_spid_cmd_clk_gate_OFF             (12)
  #define MC0_MCDECS_SECOND_CBIT_dis_spid_cmd_clk_gate_WID             ( 1)
  #define MC0_MCDECS_SECOND_CBIT_dis_spid_cmd_clk_gate_MSK             (0x00001000)
  #define MC0_MCDECS_SECOND_CBIT_dis_spid_cmd_clk_gate_MIN             (0)
  #define MC0_MCDECS_SECOND_CBIT_dis_spid_cmd_clk_gate_MAX             (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_dis_spid_cmd_clk_gate_DEF             (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_dis_spid_cmd_clk_gate_HSH             (0x0118D954)

  #define MC0_MCDECS_SECOND_CBIT_init_complete_override_OFF            (16)
  #define MC0_MCDECS_SECOND_CBIT_init_complete_override_WID            ( 4)
  #define MC0_MCDECS_SECOND_CBIT_init_complete_override_MSK            (0x000F0000)
  #define MC0_MCDECS_SECOND_CBIT_init_complete_override_MIN            (0)
  #define MC0_MCDECS_SECOND_CBIT_init_complete_override_MAX            (15) // 0x0000000F
  #define MC0_MCDECS_SECOND_CBIT_init_complete_override_DEF            (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_init_complete_override_HSH            (0x0420D954)

  #define MC0_MCDECS_SECOND_CBIT_prevent_sr_on_vc1_high_prio_OFF       (20)
  #define MC0_MCDECS_SECOND_CBIT_prevent_sr_on_vc1_high_prio_WID       ( 1)
  #define MC0_MCDECS_SECOND_CBIT_prevent_sr_on_vc1_high_prio_MSK       (0x00100000)
  #define MC0_MCDECS_SECOND_CBIT_prevent_sr_on_vc1_high_prio_MIN       (0)
  #define MC0_MCDECS_SECOND_CBIT_prevent_sr_on_vc1_high_prio_MAX       (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_prevent_sr_on_vc1_high_prio_DEF       (0x00000001)
  #define MC0_MCDECS_SECOND_CBIT_prevent_sr_on_vc1_high_prio_HSH       (0x0128D954)

  #define MC0_MCDECS_SECOND_CBIT_use_initcomplete_ch0_only_OFF         (21)
  #define MC0_MCDECS_SECOND_CBIT_use_initcomplete_ch0_only_WID         ( 1)
  #define MC0_MCDECS_SECOND_CBIT_use_initcomplete_ch0_only_MSK         (0x00200000)
  #define MC0_MCDECS_SECOND_CBIT_use_initcomplete_ch0_only_MIN         (0)
  #define MC0_MCDECS_SECOND_CBIT_use_initcomplete_ch0_only_MAX         (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_use_initcomplete_ch0_only_DEF         (0x00000001)
  #define MC0_MCDECS_SECOND_CBIT_use_initcomplete_ch0_only_HSH         (0x012AD954)

  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_for_MC_OFF                  (22)
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_for_MC_WID                  ( 1)
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_for_MC_MSK                  (0x00400000)
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_for_MC_MIN                  (0)
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_for_MC_MAX                  (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_for_MC_DEF                  (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_Mock_InSR_for_MC_HSH                  (0x012CD954)

  #define MC0_MCDECS_SECOND_CBIT_Allow_RH_Debt_in_SR_OFF               (23)
  #define MC0_MCDECS_SECOND_CBIT_Allow_RH_Debt_in_SR_WID               ( 1)
  #define MC0_MCDECS_SECOND_CBIT_Allow_RH_Debt_in_SR_MSK               (0x00800000)
  #define MC0_MCDECS_SECOND_CBIT_Allow_RH_Debt_in_SR_MIN               (0)
  #define MC0_MCDECS_SECOND_CBIT_Allow_RH_Debt_in_SR_MAX               (1) // 0x00000001
  #define MC0_MCDECS_SECOND_CBIT_Allow_RH_Debt_in_SR_DEF               (0x00000000)
  #define MC0_MCDECS_SECOND_CBIT_Allow_RH_Debt_in_SR_HSH               (0x012ED954)

#define MC0_ECC_INJ_ADDR_MASK_REG                                      (0x0000D958)

  #define MC0_ECC_INJ_ADDR_MASK_Address_OFF                            ( 0)
  #define MC0_ECC_INJ_ADDR_MASK_Address_WID                            (33)
  #define MC0_ECC_INJ_ADDR_MASK_Address_MSK                            (0x00000001FFFFFFFFULL)
  #define MC0_ECC_INJ_ADDR_MASK_Address_MIN                            (0)
  #define MC0_ECC_INJ_ADDR_MASK_Address_MAX                            (8589934591ULL) // 0x1FFFFFFFF
  #define MC0_ECC_INJ_ADDR_MASK_Address_DEF                            (0x1FFFFFFFF)
  #define MC0_ECC_INJ_ADDR_MASK_Address_HSH                            (0x6100D958)

#define MC0_SC_QOS2_REG                                                (0x0000D960)

  #define MC0_SC_QOS2_RW_Isoch_time_window_OFF                         ( 0)
  #define MC0_SC_QOS2_RW_Isoch_time_window_WID                         (17)
  #define MC0_SC_QOS2_RW_Isoch_time_window_MSK                         (0x0001FFFF)
  #define MC0_SC_QOS2_RW_Isoch_time_window_MIN                         (0)
  #define MC0_SC_QOS2_RW_Isoch_time_window_MAX                         (131071) // 0x0001FFFF
  #define MC0_SC_QOS2_RW_Isoch_time_window_DEF                         (0x000000E1)
  #define MC0_SC_QOS2_RW_Isoch_time_window_HSH                         (0x5100D960)

  #define MC0_SC_QOS2_RW_Write_starvation_window_OFF                   (17)
  #define MC0_SC_QOS2_RW_Write_starvation_window_WID                   (11)
  #define MC0_SC_QOS2_RW_Write_starvation_window_MSK                   (0x0FFE0000)
  #define MC0_SC_QOS2_RW_Write_starvation_window_MIN                   (0)
  #define MC0_SC_QOS2_RW_Write_starvation_window_MAX                   (2047) // 0x000007FF
  #define MC0_SC_QOS2_RW_Write_starvation_window_DEF                   (0x000001C2)
  #define MC0_SC_QOS2_RW_Write_starvation_window_HSH                   (0x4B22D960)

  #define MC0_SC_QOS2_RW_Read_starvation_window_OFF                    (32)
  #define MC0_SC_QOS2_RW_Read_starvation_window_WID                    (11)
  #define MC0_SC_QOS2_RW_Read_starvation_window_MSK                    (0x000007FF00000000ULL)
  #define MC0_SC_QOS2_RW_Read_starvation_window_MIN                    (0)
  #define MC0_SC_QOS2_RW_Read_starvation_window_MAX                    (2047) // 0x000007FF
  #define MC0_SC_QOS2_RW_Read_starvation_window_DEF                    (0x0000005A)
  #define MC0_SC_QOS2_RW_Read_starvation_window_HSH                    (0x4B40D960)

  #define MC0_SC_QOS2_Isoc_during_demote_period_x8_OFF                 (43)
  #define MC0_SC_QOS2_Isoc_during_demote_period_x8_WID                 ( 8)
  #define MC0_SC_QOS2_Isoc_during_demote_period_x8_MSK                 (0x0007F80000000000ULL)
  #define MC0_SC_QOS2_Isoc_during_demote_period_x8_MIN                 (0)
  #define MC0_SC_QOS2_Isoc_during_demote_period_x8_MAX                 (255) // 0x000000FF
  #define MC0_SC_QOS2_Isoc_during_demote_period_x8_DEF                 (0x00000080)
  #define MC0_SC_QOS2_Isoc_during_demote_period_x8_HSH                 (0x4856D960)

  #define MC0_SC_QOS2_Isoc_during_demote_window_OFF                    (51)
  #define MC0_SC_QOS2_Isoc_during_demote_window_WID                    ( 8)
  #define MC0_SC_QOS2_Isoc_during_demote_window_MSK                    (0x07F8000000000000ULL)
  #define MC0_SC_QOS2_Isoc_during_demote_window_MIN                    (0)
  #define MC0_SC_QOS2_Isoc_during_demote_window_MAX                    (255) // 0x000000FF
  #define MC0_SC_QOS2_Isoc_during_demote_window_DEF                    (0x00000040)
  #define MC0_SC_QOS2_Isoc_during_demote_window_HSH                    (0x4866D960)

#define MC0_SC_QOS3_REG                                                (0x0000D968)

  #define MC0_SC_QOS3_Yellow_Decay_x128_OFF                            ( 0)
  #define MC0_SC_QOS3_Yellow_Decay_x128_WID                            ( 9)
  #define MC0_SC_QOS3_Yellow_Decay_x128_MSK                            (0x000001FF)
  #define MC0_SC_QOS3_Yellow_Decay_x128_MIN                            (0)
  #define MC0_SC_QOS3_Yellow_Decay_x128_MAX                            (511) // 0x000001FF
  #define MC0_SC_QOS3_Yellow_Decay_x128_DEF                            (0x0000007D)
  #define MC0_SC_QOS3_Yellow_Decay_x128_HSH                            (0x0900D968)

  #define MC0_SC_QOS3_Yellow_Threshold_OFF                             ( 9)
  #define MC0_SC_QOS3_Yellow_Threshold_WID                             (10)
  #define MC0_SC_QOS3_Yellow_Threshold_MSK                             (0x0007FE00)
  #define MC0_SC_QOS3_Yellow_Threshold_MIN                             (0)
  #define MC0_SC_QOS3_Yellow_Threshold_MAX                             (1023) // 0x000003FF
  #define MC0_SC_QOS3_Yellow_Threshold_DEF                             (0x000000A0)
  #define MC0_SC_QOS3_Yellow_Threshold_HSH                             (0x0A12D968)

#define MC0_NORMALMODE_CFG_REG                                         (0x0000D96C)

  #define MC0_NORMALMODE_CFG_normalmode_OFF                            ( 0)
  #define MC0_NORMALMODE_CFG_normalmode_WID                            ( 1)
  #define MC0_NORMALMODE_CFG_normalmode_MSK                            (0x00000001)
  #define MC0_NORMALMODE_CFG_normalmode_MIN                            (0)
  #define MC0_NORMALMODE_CFG_normalmode_MAX                            (1) // 0x00000001
  #define MC0_NORMALMODE_CFG_normalmode_DEF                            (0x00000000)
  #define MC0_NORMALMODE_CFG_normalmode_HSH                            (0x0100D96C)

#define MC0_MC_CPGC_CMI_REG                                            (0x0000D970)

  #define MC0_MC_CPGC_CMI_CPGC_ACTIVE_OFF                              (24)
  #define MC0_MC_CPGC_CMI_CPGC_ACTIVE_WID                              ( 1)
  #define MC0_MC_CPGC_CMI_CPGC_ACTIVE_MSK                              (0x01000000)
  #define MC0_MC_CPGC_CMI_CPGC_ACTIVE_MIN                              (0)
  #define MC0_MC_CPGC_CMI_CPGC_ACTIVE_MAX                              (1) // 0x00000001
  #define MC0_MC_CPGC_CMI_CPGC_ACTIVE_DEF                              (0x00000000)
  #define MC0_MC_CPGC_CMI_CPGC_ACTIVE_HSH                              (0x0130D970)

  #define MC0_MC_CPGC_CMI_CPGC_ECC_BYTE_OFF                            (28)
  #define MC0_MC_CPGC_CMI_CPGC_ECC_BYTE_WID                            ( 3)
  #define MC0_MC_CPGC_CMI_CPGC_ECC_BYTE_MSK                            (0x70000000)
  #define MC0_MC_CPGC_CMI_CPGC_ECC_BYTE_MIN                            (0)
  #define MC0_MC_CPGC_CMI_CPGC_ECC_BYTE_MAX                            (7) // 0x00000007
  #define MC0_MC_CPGC_CMI_CPGC_ECC_BYTE_DEF                            (0x00000000)
  #define MC0_MC_CPGC_CMI_CPGC_ECC_BYTE_HSH                            (0x0338D970)

  #define MC0_MC_CPGC_CMI_Stall_CPGC_CMI_Req_OFF                       (31)
  #define MC0_MC_CPGC_CMI_Stall_CPGC_CMI_Req_WID                       ( 1)
  #define MC0_MC_CPGC_CMI_Stall_CPGC_CMI_Req_MSK                       (0x80000000)
  #define MC0_MC_CPGC_CMI_Stall_CPGC_CMI_Req_MIN                       (0)
  #define MC0_MC_CPGC_CMI_Stall_CPGC_CMI_Req_MAX                       (1) // 0x00000001
  #define MC0_MC_CPGC_CMI_Stall_CPGC_CMI_Req_DEF                       (0x00000000)
  #define MC0_MC_CPGC_CMI_Stall_CPGC_CMI_Req_HSH                       (0x013ED970)

#define MC0_MC_CPGC_MISC_DFT_REG                                       (0x0000D974)

  #define MC0_MC_CPGC_MISC_DFT_Concat_Row2Col_OFF                      ( 0)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Row2Col_WID                      ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Row2Col_MSK                      (0x00000001)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Row2Col_MIN                      (0)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Row2Col_MAX                      (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Concat_Row2Col_DEF                      (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Row2Col_HSH                      (0x0100D974)

  #define MC0_MC_CPGC_MISC_DFT_Col_align_OFF                           ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Col_align_WID                           ( 3)
  #define MC0_MC_CPGC_MISC_DFT_Col_align_MSK                           (0x0000000E)
  #define MC0_MC_CPGC_MISC_DFT_Col_align_MIN                           (0)
  #define MC0_MC_CPGC_MISC_DFT_Col_align_MAX                           (7) // 0x00000007
  #define MC0_MC_CPGC_MISC_DFT_Col_align_DEF                           (0x00000003)
  #define MC0_MC_CPGC_MISC_DFT_Col_align_HSH                           (0x0302D974)

  #define MC0_MC_CPGC_MISC_DFT_Concat_Bank2Row_OFF                     ( 4)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Bank2Row_WID                     ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Bank2Row_MSK                     (0x00000010)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Bank2Row_MIN                     (0)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Bank2Row_MAX                     (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Concat_Bank2Row_DEF                     (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Bank2Row_HSH                     (0x0108D974)

  #define MC0_MC_CPGC_MISC_DFT_Concat_Col2Row_OFF                      ( 5)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Col2Row_WID                      ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Col2Row_MSK                      (0x00000020)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Col2Row_MIN                      (0)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Col2Row_MAX                      (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Concat_Col2Row_DEF                      (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Concat_Col2Row_HSH                      (0x010AD974)

  #define MC0_MC_CPGC_MISC_DFT_Row_align_OFF                           ( 6)
  #define MC0_MC_CPGC_MISC_DFT_Row_align_WID                           ( 3)
  #define MC0_MC_CPGC_MISC_DFT_Row_align_MSK                           (0x000001C0)
  #define MC0_MC_CPGC_MISC_DFT_Row_align_MIN                           (0)
  #define MC0_MC_CPGC_MISC_DFT_Row_align_MAX                           (7) // 0x00000007
  #define MC0_MC_CPGC_MISC_DFT_Row_align_DEF                           (0x00000003)
  #define MC0_MC_CPGC_MISC_DFT_Row_align_HSH                           (0x030CD974)

  #define MC0_MC_CPGC_MISC_DFT_Concat_BG2Bank_OFF                      ( 9)
  #define MC0_MC_CPGC_MISC_DFT_Concat_BG2Bank_WID                      ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Concat_BG2Bank_MSK                      (0x00000200)
  #define MC0_MC_CPGC_MISC_DFT_Concat_BG2Bank_MIN                      (0)
  #define MC0_MC_CPGC_MISC_DFT_Concat_BG2Bank_MAX                      (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Concat_BG2Bank_DEF                      (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Concat_BG2Bank_HSH                      (0x0112D974)

  #define MC0_MC_CPGC_MISC_DFT_dis_BankGroup_OFF                       (10)
  #define MC0_MC_CPGC_MISC_DFT_dis_BankGroup_WID                       ( 1)
  #define MC0_MC_CPGC_MISC_DFT_dis_BankGroup_MSK                       (0x00000400)
  #define MC0_MC_CPGC_MISC_DFT_dis_BankGroup_MIN                       (0)
  #define MC0_MC_CPGC_MISC_DFT_dis_BankGroup_MAX                       (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_dis_BankGroup_DEF                       (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_dis_BankGroup_HSH                       (0x0114D974)

  #define MC0_MC_CPGC_MISC_DFT_Rank_as_it_OFF                          (11)
  #define MC0_MC_CPGC_MISC_DFT_Rank_as_it_WID                          ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Rank_as_it_MSK                          (0x00000800)
  #define MC0_MC_CPGC_MISC_DFT_Rank_as_it_MIN                          (0)
  #define MC0_MC_CPGC_MISC_DFT_Rank_as_it_MAX                          (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Rank_as_it_DEF                          (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Rank_as_it_HSH                          (0x0116D974)

  #define MC0_MC_CPGC_MISC_DFT_channel_is_CID_Zero_OFF                 (12)
  #define MC0_MC_CPGC_MISC_DFT_channel_is_CID_Zero_WID                 ( 1)
  #define MC0_MC_CPGC_MISC_DFT_channel_is_CID_Zero_MSK                 (0x00001000)
  #define MC0_MC_CPGC_MISC_DFT_channel_is_CID_Zero_MIN                 (0)
  #define MC0_MC_CPGC_MISC_DFT_channel_is_CID_Zero_MAX                 (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_channel_is_CID_Zero_DEF                 (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_channel_is_CID_Zero_HSH                 (0x0118D974)

  #define MC0_MC_CPGC_MISC_DFT_Inverse_channel_OFF                     (13)
  #define MC0_MC_CPGC_MISC_DFT_Inverse_channel_WID                     ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Inverse_channel_MSK                     (0x00002000)
  #define MC0_MC_CPGC_MISC_DFT_Inverse_channel_MIN                     (0)
  #define MC0_MC_CPGC_MISC_DFT_Inverse_channel_MAX                     (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Inverse_channel_DEF                     (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Inverse_channel_HSH                     (0x011AD974)

  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_info_OFF                      (14)
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_info_WID                      ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_info_MSK                      (0x00004000)
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_info_MIN                      (0)
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_info_MAX                      (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_info_DEF                      (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_info_HSH                      (0x011CD974)

  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_data_OFF                      (15)
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_data_WID                      ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_data_MSK                      (0x00008000)
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_data_MIN                      (0)
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_data_MAX                      (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_data_DEF                      (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Delay_cpl_data_HSH                      (0x011ED974)

  #define MC0_MC_CPGC_MISC_DFT_Reset_CPGC_OFF                          (16)
  #define MC0_MC_CPGC_MISC_DFT_Reset_CPGC_WID                          ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Reset_CPGC_MSK                          (0x00010000)
  #define MC0_MC_CPGC_MISC_DFT_Reset_CPGC_MIN                          (0)
  #define MC0_MC_CPGC_MISC_DFT_Reset_CPGC_MAX                          (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Reset_CPGC_DEF                          (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Reset_CPGC_HSH                          (0x0120D974)

  #define MC0_MC_CPGC_MISC_DFT_Lock_On_Active_CPGC_CMI_ISM_OFF         (17)
  #define MC0_MC_CPGC_MISC_DFT_Lock_On_Active_CPGC_CMI_ISM_WID         ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Lock_On_Active_CPGC_CMI_ISM_MSK         (0x00020000)
  #define MC0_MC_CPGC_MISC_DFT_Lock_On_Active_CPGC_CMI_ISM_MIN         (0)
  #define MC0_MC_CPGC_MISC_DFT_Lock_On_Active_CPGC_CMI_ISM_MAX         (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Lock_On_Active_CPGC_CMI_ISM_DEF         (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Lock_On_Active_CPGC_CMI_ISM_HSH         (0x0122D974)

  #define MC0_MC_CPGC_MISC_DFT_BG1_mask_OFF                            (18)
  #define MC0_MC_CPGC_MISC_DFT_BG1_mask_WID                            ( 1)
  #define MC0_MC_CPGC_MISC_DFT_BG1_mask_MSK                            (0x00040000)
  #define MC0_MC_CPGC_MISC_DFT_BG1_mask_MIN                            (0)
  #define MC0_MC_CPGC_MISC_DFT_BG1_mask_MAX                            (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_BG1_mask_DEF                            (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_BG1_mask_HSH                            (0x0124D974)

  #define MC0_MC_CPGC_MISC_DFT_Rank1_as_ERM_BG1_OFF                    (19)
  #define MC0_MC_CPGC_MISC_DFT_Rank1_as_ERM_BG1_WID                    ( 1)
  #define MC0_MC_CPGC_MISC_DFT_Rank1_as_ERM_BG1_MSK                    (0x00080000)
  #define MC0_MC_CPGC_MISC_DFT_Rank1_as_ERM_BG1_MIN                    (0)
  #define MC0_MC_CPGC_MISC_DFT_Rank1_as_ERM_BG1_MAX                    (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_Rank1_as_ERM_BG1_DEF                    (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_Rank1_as_ERM_BG1_HSH                    (0x0126D974)

  #define MC0_MC_CPGC_MISC_DFT_in_order_ingress_OFF                    (20)
  #define MC0_MC_CPGC_MISC_DFT_in_order_ingress_WID                    ( 1)
  #define MC0_MC_CPGC_MISC_DFT_in_order_ingress_MSK                    (0x00100000)
  #define MC0_MC_CPGC_MISC_DFT_in_order_ingress_MIN                    (0)
  #define MC0_MC_CPGC_MISC_DFT_in_order_ingress_MAX                    (1) // 0x00000001
  #define MC0_MC_CPGC_MISC_DFT_in_order_ingress_DEF                    (0x00000000)
  #define MC0_MC_CPGC_MISC_DFT_in_order_ingress_HSH                    (0x0128D974)

#define MC0_PWM_GLB_DRV_OFF_COUNT_REG                                  (0x0000D978)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC0_PARITYERRLOG_REG                                           (0x0000D9A0)

  #define MC0_PARITYERRLOG_ERR_ADDRESS_OFF                             ( 0)
  #define MC0_PARITYERRLOG_ERR_ADDRESS_WID                             (39)
  #define MC0_PARITYERRLOG_ERR_ADDRESS_MSK                             (0x0000007FFFFFFFFFULL)
  #define MC0_PARITYERRLOG_ERR_ADDRESS_MIN                             (0)
  #define MC0_PARITYERRLOG_ERR_ADDRESS_MAX                             (549755813887ULL) // 0x7FFFFFFFFF
  #define MC0_PARITYERRLOG_ERR_ADDRESS_DEF                             (0x00000000)
  #define MC0_PARITYERRLOG_ERR_ADDRESS_HSH                             (0x6700D9A0)

  #define MC0_PARITYERRLOG_RESERVED_OFF                                (39)
  #define MC0_PARITYERRLOG_RESERVED_WID                                (20)
  #define MC0_PARITYERRLOG_RESERVED_MSK                                (0x07FFFF8000000000ULL)
  #define MC0_PARITYERRLOG_RESERVED_MIN                                (0)
  #define MC0_PARITYERRLOG_RESERVED_MAX                                (1048575) // 0x000FFFFF
  #define MC0_PARITYERRLOG_RESERVED_DEF                                (0x00000000)
  #define MC0_PARITYERRLOG_RESERVED_HSH                                (0x544ED9A0)

  #define MC0_PARITYERRLOG_ERR_SRC_OFF                                 (59)
  #define MC0_PARITYERRLOG_ERR_SRC_WID                                 ( 1)
  #define MC0_PARITYERRLOG_ERR_SRC_MSK                                 (0x0800000000000000ULL)
  #define MC0_PARITYERRLOG_ERR_SRC_MIN                                 (0)
  #define MC0_PARITYERRLOG_ERR_SRC_MAX                                 (1) // 0x00000001
  #define MC0_PARITYERRLOG_ERR_SRC_DEF                                 (0x00000000)
  #define MC0_PARITYERRLOG_ERR_SRC_HSH                                 (0x4176D9A0)

  #define MC0_PARITYERRLOG_ERR_TYPE_OFF                                (60)
  #define MC0_PARITYERRLOG_ERR_TYPE_WID                                ( 2)
  #define MC0_PARITYERRLOG_ERR_TYPE_MSK                                (0x3000000000000000ULL)
  #define MC0_PARITYERRLOG_ERR_TYPE_MIN                                (0)
  #define MC0_PARITYERRLOG_ERR_TYPE_MAX                                (3) // 0x00000003
  #define MC0_PARITYERRLOG_ERR_TYPE_DEF                                (0x00000000)
  #define MC0_PARITYERRLOG_ERR_TYPE_HSH                                (0x4278D9A0)

  #define MC0_PARITYERRLOG_ERR_STS_OVERFLOW_OFF                        (62)
  #define MC0_PARITYERRLOG_ERR_STS_OVERFLOW_WID                        ( 1)
  #define MC0_PARITYERRLOG_ERR_STS_OVERFLOW_MSK                        (0x4000000000000000ULL)
  #define MC0_PARITYERRLOG_ERR_STS_OVERFLOW_MIN                        (0)
  #define MC0_PARITYERRLOG_ERR_STS_OVERFLOW_MAX                        (1) // 0x00000001
  #define MC0_PARITYERRLOG_ERR_STS_OVERFLOW_DEF                        (0x00000000)
  #define MC0_PARITYERRLOG_ERR_STS_OVERFLOW_HSH                        (0x417CD9A0)

  #define MC0_PARITYERRLOG_ERR_STS_OFF                                 (63)
  #define MC0_PARITYERRLOG_ERR_STS_WID                                 ( 1)
  #define MC0_PARITYERRLOG_ERR_STS_MSK                                 (0x8000000000000000ULL)
  #define MC0_PARITYERRLOG_ERR_STS_MIN                                 (0)
  #define MC0_PARITYERRLOG_ERR_STS_MAX                                 (1) // 0x00000001
  #define MC0_PARITYERRLOG_ERR_STS_DEF                                 (0x00000000)
  #define MC0_PARITYERRLOG_ERR_STS_HSH                                 (0x417ED9A0)

#define MC0_PARITY_ERR_INJ_REG                                         (0x0000D9A8)

  #define MC0_PARITY_ERR_INJ_DATA_ERR_EN_OFF                           ( 0)
  #define MC0_PARITY_ERR_INJ_DATA_ERR_EN_WID                           ( 8)
  #define MC0_PARITY_ERR_INJ_DATA_ERR_EN_MSK                           (0x000000FF)
  #define MC0_PARITY_ERR_INJ_DATA_ERR_EN_MIN                           (0)
  #define MC0_PARITY_ERR_INJ_DATA_ERR_EN_MAX                           (255) // 0x000000FF
  #define MC0_PARITY_ERR_INJ_DATA_ERR_EN_DEF                           (0x00000000)
  #define MC0_PARITY_ERR_INJ_DATA_ERR_EN_HSH                           (0x0800D9A8)

  #define MC0_PARITY_ERR_INJ_ADDR_ERR_EN_OFF                           ( 8)
  #define MC0_PARITY_ERR_INJ_ADDR_ERR_EN_WID                           ( 1)
  #define MC0_PARITY_ERR_INJ_ADDR_ERR_EN_MSK                           (0x00000100)
  #define MC0_PARITY_ERR_INJ_ADDR_ERR_EN_MIN                           (0)
  #define MC0_PARITY_ERR_INJ_ADDR_ERR_EN_MAX                           (1) // 0x00000001
  #define MC0_PARITY_ERR_INJ_ADDR_ERR_EN_DEF                           (0x00000000)
  #define MC0_PARITY_ERR_INJ_ADDR_ERR_EN_HSH                           (0x0110D9A8)

  #define MC0_PARITY_ERR_INJ_BE_ERR_EN_OFF                             ( 9)
  #define MC0_PARITY_ERR_INJ_BE_ERR_EN_WID                             ( 2)
  #define MC0_PARITY_ERR_INJ_BE_ERR_EN_MSK                             (0x00000600)
  #define MC0_PARITY_ERR_INJ_BE_ERR_EN_MIN                             (0)
  #define MC0_PARITY_ERR_INJ_BE_ERR_EN_MAX                             (3) // 0x00000003
  #define MC0_PARITY_ERR_INJ_BE_ERR_EN_DEF                             (0x00000000)
  #define MC0_PARITY_ERR_INJ_BE_ERR_EN_HSH                             (0x0212D9A8)

  #define MC0_PARITY_ERR_INJ_RSVD_OFF                                  (11)
  #define MC0_PARITY_ERR_INJ_RSVD_WID                                  ( 5)
  #define MC0_PARITY_ERR_INJ_RSVD_MSK                                  (0x0000F800)
  #define MC0_PARITY_ERR_INJ_RSVD_MIN                                  (0)
  #define MC0_PARITY_ERR_INJ_RSVD_MAX                                  (31) // 0x0000001F
  #define MC0_PARITY_ERR_INJ_RSVD_DEF                                  (0x00000000)
  #define MC0_PARITY_ERR_INJ_RSVD_HSH                                  (0x0516D9A8)

  #define MC0_PARITY_ERR_INJ_ERR_INJ_MASK_OFF                          (16)
  #define MC0_PARITY_ERR_INJ_ERR_INJ_MASK_WID                          ( 5)
  #define MC0_PARITY_ERR_INJ_ERR_INJ_MASK_MSK                          (0x001F0000)
  #define MC0_PARITY_ERR_INJ_ERR_INJ_MASK_MIN                          (0)
  #define MC0_PARITY_ERR_INJ_ERR_INJ_MASK_MAX                          (31) // 0x0000001F
  #define MC0_PARITY_ERR_INJ_ERR_INJ_MASK_DEF                          (0x00000000)
  #define MC0_PARITY_ERR_INJ_ERR_INJ_MASK_HSH                          (0x0520D9A8)

  #define MC0_PARITY_ERR_INJ_RSVD2_OFF                                 (21)
  #define MC0_PARITY_ERR_INJ_RSVD2_WID                                 (10)
  #define MC0_PARITY_ERR_INJ_RSVD2_MSK                                 (0x7FE00000)
  #define MC0_PARITY_ERR_INJ_RSVD2_MIN                                 (0)
  #define MC0_PARITY_ERR_INJ_RSVD2_MAX                                 (1023) // 0x000003FF
  #define MC0_PARITY_ERR_INJ_RSVD2_DEF                                 (0x00000000)
  #define MC0_PARITY_ERR_INJ_RSVD2_HSH                                 (0x0A2AD9A8)

  #define MC0_PARITY_ERR_INJ_DATA_ERR_INJ_SRC_OFF                      (31)
  #define MC0_PARITY_ERR_INJ_DATA_ERR_INJ_SRC_WID                      ( 1)
  #define MC0_PARITY_ERR_INJ_DATA_ERR_INJ_SRC_MSK                      (0x80000000)
  #define MC0_PARITY_ERR_INJ_DATA_ERR_INJ_SRC_MIN                      (0)
  #define MC0_PARITY_ERR_INJ_DATA_ERR_INJ_SRC_MAX                      (1) // 0x00000001
  #define MC0_PARITY_ERR_INJ_DATA_ERR_INJ_SRC_DEF                      (0x00000000)
  #define MC0_PARITY_ERR_INJ_DATA_ERR_INJ_SRC_HSH                      (0x013ED9A8)

#define MC0_PARITY_CONTROL_REG                                         (0x0000D9B4)

  #define MC0_PARITY_CONTROL_ADDR_PARITY_EN_OFF                        ( 0)
  #define MC0_PARITY_CONTROL_ADDR_PARITY_EN_WID                        ( 1)
  #define MC0_PARITY_CONTROL_ADDR_PARITY_EN_MSK                        (0x00000001)
  #define MC0_PARITY_CONTROL_ADDR_PARITY_EN_MIN                        (0)
  #define MC0_PARITY_CONTROL_ADDR_PARITY_EN_MAX                        (1) // 0x00000001
  #define MC0_PARITY_CONTROL_ADDR_PARITY_EN_DEF                        (0x00000001)
  #define MC0_PARITY_CONTROL_ADDR_PARITY_EN_HSH                        (0x0100D9B4)

  #define MC0_PARITY_CONTROL_WBE_PARITY_EN_OFF                         ( 1)
  #define MC0_PARITY_CONTROL_WBE_PARITY_EN_WID                         ( 1)
  #define MC0_PARITY_CONTROL_WBE_PARITY_EN_MSK                         (0x00000002)
  #define MC0_PARITY_CONTROL_WBE_PARITY_EN_MIN                         (0)
  #define MC0_PARITY_CONTROL_WBE_PARITY_EN_MAX                         (1) // 0x00000001
  #define MC0_PARITY_CONTROL_WBE_PARITY_EN_DEF                         (0x00000001)
  #define MC0_PARITY_CONTROL_WBE_PARITY_EN_HSH                         (0x0102D9B4)

  #define MC0_PARITY_CONTROL_WDATA_PARITY_EN_OFF                       ( 2)
  #define MC0_PARITY_CONTROL_WDATA_PARITY_EN_WID                       ( 2)
  #define MC0_PARITY_CONTROL_WDATA_PARITY_EN_MSK                       (0x0000000C)
  #define MC0_PARITY_CONTROL_WDATA_PARITY_EN_MIN                       (0)
  #define MC0_PARITY_CONTROL_WDATA_PARITY_EN_MAX                       (3) // 0x00000003
  #define MC0_PARITY_CONTROL_WDATA_PARITY_EN_DEF                       (0x00000003)
  #define MC0_PARITY_CONTROL_WDATA_PARITY_EN_HSH                       (0x0204D9B4)

  #define MC0_PARITY_CONTROL_RDATA_PARITY_EN_OFF                       ( 4)
  #define MC0_PARITY_CONTROL_RDATA_PARITY_EN_WID                       ( 1)
  #define MC0_PARITY_CONTROL_RDATA_PARITY_EN_MSK                       (0x00000010)
  #define MC0_PARITY_CONTROL_RDATA_PARITY_EN_MIN                       (0)
  #define MC0_PARITY_CONTROL_RDATA_PARITY_EN_MAX                       (1) // 0x00000001
  #define MC0_PARITY_CONTROL_RDATA_PARITY_EN_DEF                       (0x00000001)
  #define MC0_PARITY_CONTROL_RDATA_PARITY_EN_HSH                       (0x0108D9B4)

  #define MC0_PARITY_CONTROL_RSVD_0_OFF                                ( 5)
  #define MC0_PARITY_CONTROL_RSVD_0_WID                                ( 3)
  #define MC0_PARITY_CONTROL_RSVD_0_MSK                                (0x000000E0)
  #define MC0_PARITY_CONTROL_RSVD_0_MIN                                (0)
  #define MC0_PARITY_CONTROL_RSVD_0_MAX                                (7) // 0x00000007
  #define MC0_PARITY_CONTROL_RSVD_0_DEF                                (0x00000000)
  #define MC0_PARITY_CONTROL_RSVD_0_HSH                                (0x030AD9B4)

  #define MC0_PARITY_CONTROL_DIS_PARITY_PCIE_ERR_OFF                   ( 8)
  #define MC0_PARITY_CONTROL_DIS_PARITY_PCIE_ERR_WID                   ( 1)
  #define MC0_PARITY_CONTROL_DIS_PARITY_PCIE_ERR_MSK                   (0x00000100)
  #define MC0_PARITY_CONTROL_DIS_PARITY_PCIE_ERR_MIN                   (0)
  #define MC0_PARITY_CONTROL_DIS_PARITY_PCIE_ERR_MAX                   (1) // 0x00000001
  #define MC0_PARITY_CONTROL_DIS_PARITY_PCIE_ERR_DEF                   (0x00000000)
  #define MC0_PARITY_CONTROL_DIS_PARITY_PCIE_ERR_HSH                   (0x0110D9B4)

  #define MC0_PARITY_CONTROL_DIS_PARITY_LOG_OFF                        ( 9)
  #define MC0_PARITY_CONTROL_DIS_PARITY_LOG_WID                        ( 1)
  #define MC0_PARITY_CONTROL_DIS_PARITY_LOG_MSK                        (0x00000200)
  #define MC0_PARITY_CONTROL_DIS_PARITY_LOG_MIN                        (0)
  #define MC0_PARITY_CONTROL_DIS_PARITY_LOG_MAX                        (1) // 0x00000001
  #define MC0_PARITY_CONTROL_DIS_PARITY_LOG_DEF                        (0x00000000)
  #define MC0_PARITY_CONTROL_DIS_PARITY_LOG_HSH                        (0x0112D9B4)

  #define MC0_PARITY_CONTROL_RSVD_1_OFF                                (10)
  #define MC0_PARITY_CONTROL_RSVD_1_WID                                (21)
  #define MC0_PARITY_CONTROL_RSVD_1_MSK                                (0x7FFFFC00)
  #define MC0_PARITY_CONTROL_RSVD_1_MIN                                (0)
  #define MC0_PARITY_CONTROL_RSVD_1_MAX                                (2097151) // 0x001FFFFF
  #define MC0_PARITY_CONTROL_RSVD_1_DEF                                (0x00000000)
  #define MC0_PARITY_CONTROL_RSVD_1_HSH                                (0x1514D9B4)

  #define MC0_PARITY_CONTROL_PARITY_EN_OFF                             (31)
  #define MC0_PARITY_CONTROL_PARITY_EN_WID                             ( 1)
  #define MC0_PARITY_CONTROL_PARITY_EN_MSK                             (0x80000000)
  #define MC0_PARITY_CONTROL_PARITY_EN_MIN                             (0)
  #define MC0_PARITY_CONTROL_PARITY_EN_MAX                             (1) // 0x00000001
  #define MC0_PARITY_CONTROL_PARITY_EN_DEF                             (0x00000000)
  #define MC0_PARITY_CONTROL_PARITY_EN_HSH                             (0x013ED9B4)

#define MC0_MAD_MC_HASH_REG                                            (0x0000D9B8)

  #define MC0_MAD_MC_HASH_Hash_enabled_OFF                             ( 0)
  #define MC0_MAD_MC_HASH_Hash_enabled_WID                             ( 1)
  #define MC0_MAD_MC_HASH_Hash_enabled_MSK                             (0x00000001)
  #define MC0_MAD_MC_HASH_Hash_enabled_MIN                             (0)
  #define MC0_MAD_MC_HASH_Hash_enabled_MAX                             (1) // 0x00000001
  #define MC0_MAD_MC_HASH_Hash_enabled_DEF                             (0x00000000)
  #define MC0_MAD_MC_HASH_Hash_enabled_HSH                             (0x0100D9B8)

  #define MC0_MAD_MC_HASH_Hash_LSB_OFF                                 ( 1)
  #define MC0_MAD_MC_HASH_Hash_LSB_WID                                 ( 3)
  #define MC0_MAD_MC_HASH_Hash_LSB_MSK                                 (0x0000000E)
  #define MC0_MAD_MC_HASH_Hash_LSB_MIN                                 (0)
  #define MC0_MAD_MC_HASH_Hash_LSB_MAX                                 (7) // 0x00000007
  #define MC0_MAD_MC_HASH_Hash_LSB_DEF                                 (0x00000003)
  #define MC0_MAD_MC_HASH_Hash_LSB_HSH                                 (0x0302D9B8)

  #define MC0_MAD_MC_HASH_Zone1_start_OFF                              ( 4)
  #define MC0_MAD_MC_HASH_Zone1_start_WID                              ( 9)
  #define MC0_MAD_MC_HASH_Zone1_start_MSK                              (0x00001FF0)
  #define MC0_MAD_MC_HASH_Zone1_start_MIN                              (0)
  #define MC0_MAD_MC_HASH_Zone1_start_MAX                              (511) // 0x000001FF
  #define MC0_MAD_MC_HASH_Zone1_start_DEF                              (0x00000000)
  #define MC0_MAD_MC_HASH_Zone1_start_HSH                              (0x0908D9B8)

  #define MC0_MAD_MC_HASH_Stacked_Mode_OFF                             (13)
  #define MC0_MAD_MC_HASH_Stacked_Mode_WID                             ( 1)
  #define MC0_MAD_MC_HASH_Stacked_Mode_MSK                             (0x00002000)
  #define MC0_MAD_MC_HASH_Stacked_Mode_MIN                             (0)
  #define MC0_MAD_MC_HASH_Stacked_Mode_MAX                             (1) // 0x00000001
  #define MC0_MAD_MC_HASH_Stacked_Mode_DEF                             (0x00000000)
  #define MC0_MAD_MC_HASH_Stacked_Mode_HSH                             (0x011AD9B8)

#define MC0_PMON_GLOBAL_CONTROL_REG                                    (0x0000D9C0)

  #define MC0_PMON_GLOBAL_CONTROL_global_freeze_OFF                    ( 0)
  #define MC0_PMON_GLOBAL_CONTROL_global_freeze_WID                    ( 1)
  #define MC0_PMON_GLOBAL_CONTROL_global_freeze_MSK                    (0x00000001)
  #define MC0_PMON_GLOBAL_CONTROL_global_freeze_MIN                    (0)
  #define MC0_PMON_GLOBAL_CONTROL_global_freeze_MAX                    (1) // 0x00000001
  #define MC0_PMON_GLOBAL_CONTROL_global_freeze_DEF                    (0x00000000)
  #define MC0_PMON_GLOBAL_CONTROL_global_freeze_HSH                    (0x0100D9C0)

  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrl_OFF                ( 1)
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrl_WID                ( 1)
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrl_MSK                (0x00000002)
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrl_MIN                (0)
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrl_MAX                (1) // 0x00000001
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrl_DEF                (0x00000000)
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrl_HSH                (0x0102D9C0)

  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrs_OFF                ( 2)
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrs_WID                ( 1)
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrs_MSK                (0x00000004)
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrs_MIN                (0)
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrs_MAX                (1) // 0x00000001
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrs_DEF                (0x00000000)
  #define MC0_PMON_GLOBAL_CONTROL_Global_reset_ctrs_HSH                (0x0104D9C0)

#define MC0_PMON_UNIT_CONTROL_REG                                      (0x0000D9C4)

  #define MC0_PMON_UNIT_CONTROL_frz_OFF                                ( 0)
  #define MC0_PMON_UNIT_CONTROL_frz_WID                                ( 1)
  #define MC0_PMON_UNIT_CONTROL_frz_MSK                                (0x00000001)
  #define MC0_PMON_UNIT_CONTROL_frz_MIN                                (0)
  #define MC0_PMON_UNIT_CONTROL_frz_MAX                                (1) // 0x00000001
  #define MC0_PMON_UNIT_CONTROL_frz_DEF                                (0x00000000)
  #define MC0_PMON_UNIT_CONTROL_frz_HSH                                (0x0100D9C4)

  #define MC0_PMON_UNIT_CONTROL_reset_ctrl_OFF                         ( 8)
  #define MC0_PMON_UNIT_CONTROL_reset_ctrl_WID                         ( 1)
  #define MC0_PMON_UNIT_CONTROL_reset_ctrl_MSK                         (0x00000100)
  #define MC0_PMON_UNIT_CONTROL_reset_ctrl_MIN                         (0)
  #define MC0_PMON_UNIT_CONTROL_reset_ctrl_MAX                         (1) // 0x00000001
  #define MC0_PMON_UNIT_CONTROL_reset_ctrl_DEF                         (0x00000000)
  #define MC0_PMON_UNIT_CONTROL_reset_ctrl_HSH                         (0x0110D9C4)

  #define MC0_PMON_UNIT_CONTROL_reset_ctrs_OFF                         ( 9)
  #define MC0_PMON_UNIT_CONTROL_reset_ctrs_WID                         ( 1)
  #define MC0_PMON_UNIT_CONTROL_reset_ctrs_MSK                         (0x00000200)
  #define MC0_PMON_UNIT_CONTROL_reset_ctrs_MIN                         (0)
  #define MC0_PMON_UNIT_CONTROL_reset_ctrs_MAX                         (1) // 0x00000001
  #define MC0_PMON_UNIT_CONTROL_reset_ctrs_DEF                         (0x00000000)
  #define MC0_PMON_UNIT_CONTROL_reset_ctrs_HSH                         (0x0112D9C4)

#define MC0_PMON_COUNTER_CONTROL_0_REG                                 (0x0000D9D0)

  #define MC0_PMON_COUNTER_CONTROL_0_ev_sel_OFF                        ( 0)
  #define MC0_PMON_COUNTER_CONTROL_0_ev_sel_WID                        ( 8)
  #define MC0_PMON_COUNTER_CONTROL_0_ev_sel_MSK                        (0x000000FF)
  #define MC0_PMON_COUNTER_CONTROL_0_ev_sel_MIN                        (0)
  #define MC0_PMON_COUNTER_CONTROL_0_ev_sel_MAX                        (255) // 0x000000FF
  #define MC0_PMON_COUNTER_CONTROL_0_ev_sel_DEF                        (0x00000000)
  #define MC0_PMON_COUNTER_CONTROL_0_ev_sel_HSH                        (0x0800D9D0)

  #define MC0_PMON_COUNTER_CONTROL_0_ch_mask_OFF                       ( 8)
  #define MC0_PMON_COUNTER_CONTROL_0_ch_mask_WID                       ( 4)
  #define MC0_PMON_COUNTER_CONTROL_0_ch_mask_MSK                       (0x00000F00)
  #define MC0_PMON_COUNTER_CONTROL_0_ch_mask_MIN                       (0)
  #define MC0_PMON_COUNTER_CONTROL_0_ch_mask_MAX                       (15) // 0x0000000F
  #define MC0_PMON_COUNTER_CONTROL_0_ch_mask_DEF                       (0x00000000)
  #define MC0_PMON_COUNTER_CONTROL_0_ch_mask_HSH                       (0x0410D9D0)

  #define MC0_PMON_COUNTER_CONTROL_0_rst_OFF                           (17)
  #define MC0_PMON_COUNTER_CONTROL_0_rst_WID                           ( 1)
  #define MC0_PMON_COUNTER_CONTROL_0_rst_MSK                           (0x00020000)
  #define MC0_PMON_COUNTER_CONTROL_0_rst_MIN                           (0)
  #define MC0_PMON_COUNTER_CONTROL_0_rst_MAX                           (1) // 0x00000001
  #define MC0_PMON_COUNTER_CONTROL_0_rst_DEF                           (0x00000000)
  #define MC0_PMON_COUNTER_CONTROL_0_rst_HSH                           (0x0122D9D0)

  #define MC0_PMON_COUNTER_CONTROL_0_edge_det_OFF                      (18)
  #define MC0_PMON_COUNTER_CONTROL_0_edge_det_WID                      ( 1)
  #define MC0_PMON_COUNTER_CONTROL_0_edge_det_MSK                      (0x00040000)
  #define MC0_PMON_COUNTER_CONTROL_0_edge_det_MIN                      (0)
  #define MC0_PMON_COUNTER_CONTROL_0_edge_det_MAX                      (1) // 0x00000001
  #define MC0_PMON_COUNTER_CONTROL_0_edge_det_DEF                      (0x00000000)
  #define MC0_PMON_COUNTER_CONTROL_0_edge_det_HSH                      (0x0124D9D0)

#define MC0_PMON_COUNTER_CONTROL_1_REG                                 (0x0000D9D4)
//Duplicate of MC0_PMON_COUNTER_CONTROL_0_REG

#define MC0_PMON_COUNTER_CONTROL_2_REG                                 (0x0000D9D8)
//Duplicate of MC0_PMON_COUNTER_CONTROL_0_REG

#define MC0_PMON_COUNTER_CONTROL_3_REG                                 (0x0000D9DC)
//Duplicate of MC0_PMON_COUNTER_CONTROL_0_REG

#define MC0_PMON_COUNTER_CONTROL_4_REG                                 (0x0000D9E0)
//Duplicate of MC0_PMON_COUNTER_CONTROL_0_REG

#define MC0_PMON_COUNTER_DATA_0_REG                                    (0x0000D9E8)

  #define MC0_PMON_COUNTER_DATA_0_event_count_OFF                      ( 0)
  #define MC0_PMON_COUNTER_DATA_0_event_count_WID                      (64)
  #define MC0_PMON_COUNTER_DATA_0_event_count_MSK                      (0xFFFFFFFFFFFFFFFFULL)
  #define MC0_PMON_COUNTER_DATA_0_event_count_MIN                      (0)
  #define MC0_PMON_COUNTER_DATA_0_event_count_MAX                      (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define MC0_PMON_COUNTER_DATA_0_event_count_DEF                      (0x00000000)
  #define MC0_PMON_COUNTER_DATA_0_event_count_HSH                      (0x4000D9E8)

#define MC0_PMON_COUNTER_DATA_1_REG                                    (0x0000D9F0)
//Duplicate of MC0_PMON_COUNTER_DATA_0_REG

#define MC0_PMON_COUNTER_DATA_2_REG                                    (0x0000D9F8)
//Duplicate of MC0_PMON_COUNTER_DATA_0_REG

#define MC0_PMON_COUNTER_DATA_3_REG                                    (0x0000DA00)
//Duplicate of MC0_PMON_COUNTER_DATA_0_REG

#define MC0_PMON_COUNTER_DATA_4_REG                                    (0x0000DA08)
//Duplicate of MC0_PMON_COUNTER_DATA_0_REG

#define MC0_OS_TELEMETRY_CONTROL_REG                                   (0x0000DA10)

  #define MC0_OS_TELEMETRY_CONTROL_EnableOSTelemetry_OFF               ( 0)
  #define MC0_OS_TELEMETRY_CONTROL_EnableOSTelemetry_WID               ( 1)
  #define MC0_OS_TELEMETRY_CONTROL_EnableOSTelemetry_MSK               (0x00000001)
  #define MC0_OS_TELEMETRY_CONTROL_EnableOSTelemetry_MIN               (0)
  #define MC0_OS_TELEMETRY_CONTROL_EnableOSTelemetry_MAX               (1) // 0x00000001
  #define MC0_OS_TELEMETRY_CONTROL_EnableOSTelemetry_DEF               (0x00000000)
  #define MC0_OS_TELEMETRY_CONTROL_EnableOSTelemetry_HSH               (0x0100DA10)

#define MC0_DDRPL_CFG_DTF_REG                                          (0x0000DF00)

  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_rd_bits_OFF                  ( 0)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_rd_bits_WID                  ( 1)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_rd_bits_MSK                  (0x00000001)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_rd_bits_MIN                  (0)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_rd_bits_MAX                  (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_rd_bits_DEF                  (0x00000000)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_rd_bits_HSH                  (0x0100DF00)

  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_wr_bits_OFF                  ( 1)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_wr_bits_WID                  ( 1)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_wr_bits_MSK                  (0x00000002)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_wr_bits_MIN                  (0)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_wr_bits_MAX                  (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_wr_bits_DEF                  (0x00000000)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_wr_bits_HSH                  (0x0102DF00)

  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_BL4_select_high_bits_OFF     ( 2)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_BL4_select_high_bits_WID     ( 1)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_BL4_select_high_bits_MSK     (0x00000004)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_BL4_select_high_bits_MIN     (0)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_BL4_select_high_bits_MAX     (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_BL4_select_high_bits_DEF     (0x00000000)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_all_BL4_select_high_bits_HSH     (0x0104DF00)

  #define MC0_DDRPL_CFG_DTF_MCHTrace_OFF                               ( 4)
  #define MC0_DDRPL_CFG_DTF_MCHTrace_WID                               ( 1)
  #define MC0_DDRPL_CFG_DTF_MCHTrace_MSK                               (0x00000010)
  #define MC0_DDRPL_CFG_DTF_MCHTrace_MIN                               (0)
  #define MC0_DDRPL_CFG_DTF_MCHTrace_MAX                               (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_MCHTrace_DEF                               (0x00000000)
  #define MC0_DDRPL_CFG_DTF_MCHTrace_HSH                               (0x0108DF00)

  #define MC0_DDRPL_CFG_DTF_SCHTrace_OFF                               ( 5)
  #define MC0_DDRPL_CFG_DTF_SCHTrace_WID                               ( 1)
  #define MC0_DDRPL_CFG_DTF_SCHTrace_MSK                               (0x00000020)
  #define MC0_DDRPL_CFG_DTF_SCHTrace_MIN                               (0)
  #define MC0_DDRPL_CFG_DTF_SCHTrace_MAX                               (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_SCHTrace_DEF                               (0x00000000)
  #define MC0_DDRPL_CFG_DTF_SCHTrace_HSH                               (0x010ADF00)

  #define MC0_DDRPL_CFG_DTF_ECC_EN_OFF                                 ( 6)
  #define MC0_DDRPL_CFG_DTF_ECC_EN_WID                                 ( 1)
  #define MC0_DDRPL_CFG_DTF_ECC_EN_MSK                                 (0x00000040)
  #define MC0_DDRPL_CFG_DTF_ECC_EN_MIN                                 (0)
  #define MC0_DDRPL_CFG_DTF_ECC_EN_MAX                                 (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_ECC_EN_DEF                                 (0x00000000)
  #define MC0_DDRPL_CFG_DTF_ECC_EN_HSH                                 (0x010CDF00)

  #define MC0_DDRPL_CFG_DTF_ECC_BYTE_replace_OFF                       ( 7)
  #define MC0_DDRPL_CFG_DTF_ECC_BYTE_replace_WID                       ( 3)
  #define MC0_DDRPL_CFG_DTF_ECC_BYTE_replace_MSK                       (0x00000380)
  #define MC0_DDRPL_CFG_DTF_ECC_BYTE_replace_MIN                       (0)
  #define MC0_DDRPL_CFG_DTF_ECC_BYTE_replace_MAX                       (7) // 0x00000007
  #define MC0_DDRPL_CFG_DTF_ECC_BYTE_replace_DEF                       (0x00000000)
  #define MC0_DDRPL_CFG_DTF_ECC_BYTE_replace_HSH                       (0x030EDF00)

  #define MC0_DDRPL_CFG_DTF_RSVD_16_10_OFF                             (10)
  #define MC0_DDRPL_CFG_DTF_RSVD_16_10_WID                             ( 7)
  #define MC0_DDRPL_CFG_DTF_RSVD_16_10_MSK                             (0x0001FC00)
  #define MC0_DDRPL_CFG_DTF_RSVD_16_10_MIN                             (0)
  #define MC0_DDRPL_CFG_DTF_RSVD_16_10_MAX                             (127) // 0x0000007F
  #define MC0_DDRPL_CFG_DTF_RSVD_16_10_DEF                             (0x00000000)
  #define MC0_DDRPL_CFG_DTF_RSVD_16_10_HSH                             (0x0714DF00)

  #define MC0_DDRPL_CFG_DTF_Enable_ACC_Trace_OFF                       (17)
  #define MC0_DDRPL_CFG_DTF_Enable_ACC_Trace_WID                       ( 1)
  #define MC0_DDRPL_CFG_DTF_Enable_ACC_Trace_MSK                       (0x00020000)
  #define MC0_DDRPL_CFG_DTF_Enable_ACC_Trace_MIN                       (0)
  #define MC0_DDRPL_CFG_DTF_Enable_ACC_Trace_MAX                       (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_Enable_ACC_Trace_DEF                       (0x00000001)
  #define MC0_DDRPL_CFG_DTF_Enable_ACC_Trace_HSH                       (0x0122DF00)

  #define MC0_DDRPL_CFG_DTF_Enable_ReadData_Trace_OFF                  (18)
  #define MC0_DDRPL_CFG_DTF_Enable_ReadData_Trace_WID                  ( 1)
  #define MC0_DDRPL_CFG_DTF_Enable_ReadData_Trace_MSK                  (0x00040000)
  #define MC0_DDRPL_CFG_DTF_Enable_ReadData_Trace_MIN                  (0)
  #define MC0_DDRPL_CFG_DTF_Enable_ReadData_Trace_MAX                  (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_Enable_ReadData_Trace_DEF                  (0x00000001)
  #define MC0_DDRPL_CFG_DTF_Enable_ReadData_Trace_HSH                  (0x0124DF00)

  #define MC0_DDRPL_CFG_DTF_Enable_WriteData_Trace_OFF                 (19)
  #define MC0_DDRPL_CFG_DTF_Enable_WriteData_Trace_WID                 ( 1)
  #define MC0_DDRPL_CFG_DTF_Enable_WriteData_Trace_MSK                 (0x00080000)
  #define MC0_DDRPL_CFG_DTF_Enable_WriteData_Trace_MIN                 (0)
  #define MC0_DDRPL_CFG_DTF_Enable_WriteData_Trace_MAX                 (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_Enable_WriteData_Trace_DEF                 (0x00000001)
  #define MC0_DDRPL_CFG_DTF_Enable_WriteData_Trace_HSH                 (0x0126DF00)

  #define MC0_DDRPL_CFG_DTF_DDRPL_Activate_OFF                         (20)
  #define MC0_DDRPL_CFG_DTF_DDRPL_Activate_WID                         ( 1)
  #define MC0_DDRPL_CFG_DTF_DDRPL_Activate_MSK                         (0x00100000)
  #define MC0_DDRPL_CFG_DTF_DDRPL_Activate_MIN                         (0)
  #define MC0_DDRPL_CFG_DTF_DDRPL_Activate_MAX                         (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_DDRPL_Activate_DEF                         (0x00000000)
  #define MC0_DDRPL_CFG_DTF_DDRPL_Activate_HSH                         (0x0128DF00)

  #define MC0_DDRPL_CFG_DTF_DDRPL_GLB_DRV_GATE_DIS_OFF                 (21)
  #define MC0_DDRPL_CFG_DTF_DDRPL_GLB_DRV_GATE_DIS_WID                 ( 1)
  #define MC0_DDRPL_CFG_DTF_DDRPL_GLB_DRV_GATE_DIS_MSK                 (0x00200000)
  #define MC0_DDRPL_CFG_DTF_DDRPL_GLB_DRV_GATE_DIS_MIN                 (0)
  #define MC0_DDRPL_CFG_DTF_DDRPL_GLB_DRV_GATE_DIS_MAX                 (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_DDRPL_GLB_DRV_GATE_DIS_DEF                 (0x00000000)
  #define MC0_DDRPL_CFG_DTF_DDRPL_GLB_DRV_GATE_DIS_HSH                 (0x012ADF00)

  #define MC0_DDRPL_CFG_DTF_RSVD_23_22_OFF                             (22)
  #define MC0_DDRPL_CFG_DTF_RSVD_23_22_WID                             ( 2)
  #define MC0_DDRPL_CFG_DTF_RSVD_23_22_MSK                             (0x00C00000)
  #define MC0_DDRPL_CFG_DTF_RSVD_23_22_MIN                             (0)
  #define MC0_DDRPL_CFG_DTF_RSVD_23_22_MAX                             (3) // 0x00000003
  #define MC0_DDRPL_CFG_DTF_RSVD_23_22_DEF                             (0x00000002)
  #define MC0_DDRPL_CFG_DTF_RSVD_23_22_HSH                             (0x022CDF00)

  #define MC0_DDRPL_CFG_DTF_DTF_ENC_CB_OFF                             (24)
  #define MC0_DDRPL_CFG_DTF_DTF_ENC_CB_WID                             ( 1)
  #define MC0_DDRPL_CFG_DTF_DTF_ENC_CB_MSK                             (0x01000000)
  #define MC0_DDRPL_CFG_DTF_DTF_ENC_CB_MIN                             (0)
  #define MC0_DDRPL_CFG_DTF_DTF_ENC_CB_MAX                             (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_DTF_ENC_CB_DEF                             (0x00000000)
  #define MC0_DDRPL_CFG_DTF_DTF_ENC_CB_HSH                             (0x0130DF00)

  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_read_OFF                  (26)
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_read_WID                  ( 1)
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_read_MSK                  (0x04000000)
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_read_MIN                  (0)
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_read_MAX                  (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_read_DEF                  (0x00000000)
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_read_HSH                  (0x0134DF00)

  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_write_OFF                 (27)
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_write_WID                 ( 1)
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_write_MSK                 (0x08000000)
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_write_MIN                 (0)
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_write_MAX                 (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_write_DEF                 (0x00000000)
  #define MC0_DDRPL_CFG_DTF_BL4_bypass_trace_write_HSH                 (0x0136DF00)

  #define MC0_DDRPL_CFG_DTF_Trace_2LM_Tag_OFF                          (28)
  #define MC0_DDRPL_CFG_DTF_Trace_2LM_Tag_WID                          ( 1)
  #define MC0_DDRPL_CFG_DTF_Trace_2LM_Tag_MSK                          (0x10000000)
  #define MC0_DDRPL_CFG_DTF_Trace_2LM_Tag_MIN                          (0)
  #define MC0_DDRPL_CFG_DTF_Trace_2LM_Tag_MAX                          (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_Trace_2LM_Tag_DEF                          (0x00000000)
  #define MC0_DDRPL_CFG_DTF_Trace_2LM_Tag_HSH                          (0x0138DF00)

  #define MC0_DDRPL_CFG_DTF_RTB_trace_rd_data_high_bits_OFF            (29)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_rd_data_high_bits_WID            ( 1)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_rd_data_high_bits_MSK            (0x20000000)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_rd_data_high_bits_MIN            (0)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_rd_data_high_bits_MAX            (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_RTB_trace_rd_data_high_bits_DEF            (0x00000000)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_rd_data_high_bits_HSH            (0x013ADF00)

  #define MC0_DDRPL_CFG_DTF_RTB_trace_wr_data_high_bits_OFF            (30)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_wr_data_high_bits_WID            ( 1)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_wr_data_high_bits_MSK            (0x40000000)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_wr_data_high_bits_MIN            (0)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_wr_data_high_bits_MAX            (1) // 0x00000001
  #define MC0_DDRPL_CFG_DTF_RTB_trace_wr_data_high_bits_DEF            (0x00000000)
  #define MC0_DDRPL_CFG_DTF_RTB_trace_wr_data_high_bits_HSH            (0x013CDF00)

#define MC0_DDRPL_DEBUG_DTF_REG                                        (0x0000DF08)

  #define MC0_DDRPL_DEBUG_DTF_Read_Data_UserDefined_Bits_OFF           (16)
  #define MC0_DDRPL_DEBUG_DTF_Read_Data_UserDefined_Bits_WID           ( 8)
  #define MC0_DDRPL_DEBUG_DTF_Read_Data_UserDefined_Bits_MSK           (0x00FF0000)
  #define MC0_DDRPL_DEBUG_DTF_Read_Data_UserDefined_Bits_MIN           (0)
  #define MC0_DDRPL_DEBUG_DTF_Read_Data_UserDefined_Bits_MAX           (255) // 0x000000FF
  #define MC0_DDRPL_DEBUG_DTF_Read_Data_UserDefined_Bits_DEF           (0x00000000)
  #define MC0_DDRPL_DEBUG_DTF_Read_Data_UserDefined_Bits_HSH           (0x0820DF08)

  #define MC0_DDRPL_DEBUG_DTF_Write_Data_UserDefined_Bits_OFF          (24)
  #define MC0_DDRPL_DEBUG_DTF_Write_Data_UserDefined_Bits_WID          ( 8)
  #define MC0_DDRPL_DEBUG_DTF_Write_Data_UserDefined_Bits_MSK          (0xFF000000)
  #define MC0_DDRPL_DEBUG_DTF_Write_Data_UserDefined_Bits_MIN          (0)
  #define MC0_DDRPL_DEBUG_DTF_Write_Data_UserDefined_Bits_MAX          (255) // 0x000000FF
  #define MC0_DDRPL_DEBUG_DTF_Write_Data_UserDefined_Bits_DEF          (0x00000000)
  #define MC0_DDRPL_DEBUG_DTF_Write_Data_UserDefined_Bits_HSH          (0x0830DF08)

#define MC0_DDRPL_VISA_LANES_REG                                       (0x0000DF0C)

  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_1_out_OFF                ( 0)
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_1_out_WID                ( 8)
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_1_out_MSK                (0x000000FF)
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_1_out_MIN                (0)
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_1_out_MAX                (255) // 0x000000FF
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_1_out_DEF                (0x00000000)
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_1_out_HSH                (0x0800DF0C)

  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_0_out_OFF                ( 8)
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_0_out_WID                ( 8)
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_0_out_MSK                (0x0000FF00)
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_0_out_MIN                (0)
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_0_out_MAX                (255) // 0x000000FF
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_0_out_DEF                (0x00000000)
  #define MC0_DDRPL_VISA_LANES_Visa_Qclk_Lane_0_out_HSH                (0x0810DF0C)

  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_1_out_OFF                (16)
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_1_out_WID                ( 8)
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_1_out_MSK                (0x00FF0000)
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_1_out_MIN                (0)
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_1_out_MAX                (255) // 0x000000FF
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_1_out_DEF                (0x00000000)
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_1_out_HSH                (0x0820DF0C)

  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_0_out_OFF                (24)
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_0_out_WID                ( 8)
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_0_out_MSK                (0xFF000000)
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_0_out_MIN                (0)
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_0_out_MAX                (255) // 0x000000FF
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_0_out_DEF                (0x00000000)
  #define MC0_DDRPL_VISA_LANES_Visa_Dclk_Lane_0_out_HSH                (0x0830DF0C)

#define MC0_DDRPL_VISA_CFG_DTF_REG                                     (0x0000DF10)

  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Dclk_tree_OFF    ( 0)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Dclk_tree_WID    ( 2)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Dclk_tree_MSK    (0x00000003)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Dclk_tree_MIN    (0)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Dclk_tree_MAX    (3) // 0x00000003
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Dclk_tree_DEF    (0x00000000)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Dclk_tree_HSH    (0x0200DF10)

  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Dclk_tree_OFF    ( 2)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Dclk_tree_WID    ( 2)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Dclk_tree_MSK    (0x0000000C)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Dclk_tree_MIN    (0)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Dclk_tree_MAX    (3) // 0x00000003
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Dclk_tree_DEF    (0x00000000)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Dclk_tree_HSH    (0x0204DF10)

  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Qclk_tree_OFF    ( 4)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Qclk_tree_WID    ( 2)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Qclk_tree_MSK    (0x00000030)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Qclk_tree_MIN    (0)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Qclk_tree_MAX    (3) // 0x00000003
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Qclk_tree_DEF    (0x00000000)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane0_Data_Stage_Qclk_tree_HSH    (0x0208DF10)

  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Qclk_tree_OFF    ( 6)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Qclk_tree_WID    ( 2)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Qclk_tree_MSK    (0x000000C0)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Qclk_tree_MIN    (0)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Qclk_tree_MAX    (3) // 0x00000003
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Qclk_tree_DEF    (0x00000000)
  #define MC0_DDRPL_VISA_CFG_DTF_VisaLane1_Data_Stage_Qclk_tree_HSH    (0x020CDF10)
#pragma pack(pop)
#endif
