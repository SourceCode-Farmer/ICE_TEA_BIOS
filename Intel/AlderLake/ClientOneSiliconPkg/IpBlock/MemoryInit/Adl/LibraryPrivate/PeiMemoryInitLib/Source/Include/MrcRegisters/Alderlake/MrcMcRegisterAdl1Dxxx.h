#ifndef __MrcMcRegisterAdl1Dxxx_h__
#define __MrcMcRegisterAdl1Dxxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define MC1_CH0_CR_CADB_ACCESS_CONTROL_POLICY_REG                      (0x0001D000)
//Duplicate of MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_REG

#define MC1_CH0_CR_CADB_ACCESS_READ_POLICY_REG                         (0x0001D008)
//Duplicate of MC0_CR_CPGC2_ACCESS_READ_POLICY_REG

#define MC1_CH0_CR_CADB_ACCESS_WRITE_POLICY_REG                        (0x0001D010)
//Duplicate of MC0_CR_CPGC2_ACCESS_WRITE_POLICY_REG

#define MC1_CH0_CR_CADB_CTL_REG                                        (0x0001D018)
//Duplicate of MC0_CH0_CR_CADB_CTL_REG

#define MC1_CH0_CR_CADB_CFG_REG                                        (0x0001D01C)
//Duplicate of MC0_CH0_CR_CADB_CFG_REG

#define MC1_CH0_CR_CADB_DLY_REG                                        (0x0001D020)
//Duplicate of MC0_CH0_CR_CADB_DLY_REG

#define MC1_CH0_CR_CADB_STATUS_REG                                     (0x0001D024)
//Duplicate of MC0_CH0_CR_CADB_STATUS_REG

#define MC1_CH0_CR_CADB_OVERRIDE_REG                                   (0x0001D028)
//Duplicate of MC0_CH0_CR_CADB_OVERRIDE_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG                     (0x0001D030)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_CFG_0_REG                          (0x0001D038)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_CFG_1_REG                          (0x0001D039)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_CFG_2_REG                          (0x0001D03A)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_CFG_3_REG                          (0x0001D03B)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG                    (0x0001D03C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG                         (0x0001D040)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_PBUF_1_REG                         (0x0001D044)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_PBUF_2_REG                         (0x0001D048)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_PBUF_3_REG                         (0x0001D04C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_LMN_REG                            (0x0001D050)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_POLY_0_REG                         (0x0001D058)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_POLY_1_REG                         (0x0001D05C)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_POLY_2_REG                         (0x0001D060)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH0_CR_CADB_DSEL_UNISEQ_POLY_3_REG                         (0x0001D064)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH0_CR_CADB_BUF_0_REG                                      (0x0001D068)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_1_REG                                      (0x0001D070)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_2_REG                                      (0x0001D078)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_3_REG                                      (0x0001D080)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_4_REG                                      (0x0001D088)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_5_REG                                      (0x0001D090)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_6_REG                                      (0x0001D098)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_7_REG                                      (0x0001D0A0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_8_REG                                      (0x0001D0A8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_9_REG                                      (0x0001D0B0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_10_REG                                     (0x0001D0B8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_11_REG                                     (0x0001D0C0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_12_REG                                     (0x0001D0C8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_13_REG                                     (0x0001D0D0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_14_REG                                     (0x0001D0D8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_BUF_15_REG                                     (0x0001D0E0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH0_CR_CADB_AO_MRSCFG_REG                                  (0x0001D0E8)
//Duplicate of MC0_CH0_CR_CADB_AO_MRSCFG_REG

#define MC1_CH0_CR_CADB_SELCFG_REG                                     (0x0001D0F0)
//Duplicate of MC0_CH0_CR_CADB_SELCFG_REG

#define MC1_CH0_CR_CADB_MRSCFG_REG                                     (0x0001D0F8)
//Duplicate of MC0_CH0_CR_CADB_MRSCFG_REG

#define MC1_CH1_CR_CADB_ACCESS_CONTROL_POLICY_REG                      (0x0001D100)
//Duplicate of MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_REG

#define MC1_CH1_CR_CADB_ACCESS_READ_POLICY_REG                         (0x0001D108)
//Duplicate of MC0_CR_CPGC2_ACCESS_READ_POLICY_REG

#define MC1_CH1_CR_CADB_ACCESS_WRITE_POLICY_REG                        (0x0001D110)
//Duplicate of MC0_CR_CPGC2_ACCESS_WRITE_POLICY_REG

#define MC1_CH1_CR_CADB_CTL_REG                                        (0x0001D118)
//Duplicate of MC0_CH0_CR_CADB_CTL_REG

#define MC1_CH1_CR_CADB_CFG_REG                                        (0x0001D11C)
//Duplicate of MC0_CH0_CR_CADB_CFG_REG

#define MC1_CH1_CR_CADB_DLY_REG                                        (0x0001D120)
//Duplicate of MC0_CH0_CR_CADB_DLY_REG

#define MC1_CH1_CR_CADB_STATUS_REG                                     (0x0001D124)
//Duplicate of MC0_CH0_CR_CADB_STATUS_REG

#define MC1_CH1_CR_CADB_OVERRIDE_REG                                   (0x0001D128)
//Duplicate of MC0_CH0_CR_CADB_OVERRIDE_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG                     (0x0001D130)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_CFG_0_REG                          (0x0001D138)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_CFG_1_REG                          (0x0001D139)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_CFG_2_REG                          (0x0001D13A)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_CFG_3_REG                          (0x0001D13B)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG                    (0x0001D13C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_PBUF_0_REG                         (0x0001D140)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_PBUF_1_REG                         (0x0001D144)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_PBUF_2_REG                         (0x0001D148)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_PBUF_3_REG                         (0x0001D14C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_LMN_REG                            (0x0001D150)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_POLY_0_REG                         (0x0001D158)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_POLY_1_REG                         (0x0001D15C)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_POLY_2_REG                         (0x0001D160)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH1_CR_CADB_DSEL_UNISEQ_POLY_3_REG                         (0x0001D164)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH1_CR_CADB_BUF_0_REG                                      (0x0001D168)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_1_REG                                      (0x0001D170)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_2_REG                                      (0x0001D178)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_3_REG                                      (0x0001D180)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_4_REG                                      (0x0001D188)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_5_REG                                      (0x0001D190)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_6_REG                                      (0x0001D198)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_7_REG                                      (0x0001D1A0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_8_REG                                      (0x0001D1A8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_9_REG                                      (0x0001D1B0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_10_REG                                     (0x0001D1B8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_11_REG                                     (0x0001D1C0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_12_REG                                     (0x0001D1C8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_13_REG                                     (0x0001D1D0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_14_REG                                     (0x0001D1D8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_BUF_15_REG                                     (0x0001D1E0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH1_CR_CADB_AO_MRSCFG_REG                                  (0x0001D1E8)
//Duplicate of MC0_CH0_CR_CADB_AO_MRSCFG_REG

#define MC1_CH1_CR_CADB_SELCFG_REG                                     (0x0001D1F0)
//Duplicate of MC0_CH0_CR_CADB_SELCFG_REG

#define MC1_CH1_CR_CADB_MRSCFG_REG                                     (0x0001D1F8)
//Duplicate of MC0_CH0_CR_CADB_MRSCFG_REG

#define MC1_CH2_CR_CADB_ACCESS_CONTROL_POLICY_REG                      (0x0001D200)
//Duplicate of MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_REG

#define MC1_CH2_CR_CADB_ACCESS_READ_POLICY_REG                         (0x0001D208)
//Duplicate of MC0_CR_CPGC2_ACCESS_READ_POLICY_REG

#define MC1_CH2_CR_CADB_ACCESS_WRITE_POLICY_REG                        (0x0001D210)
//Duplicate of MC0_CR_CPGC2_ACCESS_WRITE_POLICY_REG

#define MC1_CH2_CR_CADB_CTL_REG                                        (0x0001D218)
//Duplicate of MC0_CH0_CR_CADB_CTL_REG

#define MC1_CH2_CR_CADB_CFG_REG                                        (0x0001D21C)
//Duplicate of MC0_CH0_CR_CADB_CFG_REG

#define MC1_CH2_CR_CADB_DLY_REG                                        (0x0001D220)
//Duplicate of MC0_CH0_CR_CADB_DLY_REG

#define MC1_CH2_CR_CADB_STATUS_REG                                     (0x0001D224)
//Duplicate of MC0_CH0_CR_CADB_STATUS_REG

#define MC1_CH2_CR_CADB_OVERRIDE_REG                                   (0x0001D228)
//Duplicate of MC0_CH0_CR_CADB_OVERRIDE_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG                     (0x0001D230)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_CFG_0_REG                          (0x0001D238)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_CFG_1_REG                          (0x0001D239)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_CFG_2_REG                          (0x0001D23A)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_CFG_3_REG                          (0x0001D23B)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG                    (0x0001D23C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_PBUF_0_REG                         (0x0001D240)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_PBUF_1_REG                         (0x0001D244)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_PBUF_2_REG                         (0x0001D248)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_PBUF_3_REG                         (0x0001D24C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_LMN_REG                            (0x0001D250)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_POLY_0_REG                         (0x0001D258)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_POLY_1_REG                         (0x0001D25C)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_POLY_2_REG                         (0x0001D260)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH2_CR_CADB_DSEL_UNISEQ_POLY_3_REG                         (0x0001D264)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH2_CR_CADB_BUF_0_REG                                      (0x0001D268)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_1_REG                                      (0x0001D270)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_2_REG                                      (0x0001D278)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_3_REG                                      (0x0001D280)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_4_REG                                      (0x0001D288)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_5_REG                                      (0x0001D290)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_6_REG                                      (0x0001D298)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_7_REG                                      (0x0001D2A0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_8_REG                                      (0x0001D2A8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_9_REG                                      (0x0001D2B0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_10_REG                                     (0x0001D2B8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_11_REG                                     (0x0001D2C0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_12_REG                                     (0x0001D2C8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_13_REG                                     (0x0001D2D0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_14_REG                                     (0x0001D2D8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_BUF_15_REG                                     (0x0001D2E0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH2_CR_CADB_AO_MRSCFG_REG                                  (0x0001D2E8)
//Duplicate of MC0_CH0_CR_CADB_AO_MRSCFG_REG

#define MC1_CH2_CR_CADB_SELCFG_REG                                     (0x0001D2F0)
//Duplicate of MC0_CH0_CR_CADB_SELCFG_REG

#define MC1_CH2_CR_CADB_MRSCFG_REG                                     (0x0001D2F8)
//Duplicate of MC0_CH0_CR_CADB_MRSCFG_REG

#define MC1_CH3_CR_CADB_ACCESS_CONTROL_POLICY_REG                      (0x0001D300)
//Duplicate of MC0_CR_CPGC2_ACCESS_CONTROL_POLICY_REG

#define MC1_CH3_CR_CADB_ACCESS_READ_POLICY_REG                         (0x0001D308)
//Duplicate of MC0_CR_CPGC2_ACCESS_READ_POLICY_REG

#define MC1_CH3_CR_CADB_ACCESS_WRITE_POLICY_REG                        (0x0001D310)
//Duplicate of MC0_CR_CPGC2_ACCESS_WRITE_POLICY_REG

#define MC1_CH3_CR_CADB_CTL_REG                                        (0x0001D318)
//Duplicate of MC0_CH0_CR_CADB_CTL_REG

#define MC1_CH3_CR_CADB_CFG_REG                                        (0x0001D31C)
//Duplicate of MC0_CH0_CR_CADB_CFG_REG

#define MC1_CH3_CR_CADB_DLY_REG                                        (0x0001D320)
//Duplicate of MC0_CH0_CR_CADB_DLY_REG

#define MC1_CH3_CR_CADB_STATUS_REG                                     (0x0001D324)
//Duplicate of MC0_CH0_CR_CADB_STATUS_REG

#define MC1_CH3_CR_CADB_OVERRIDE_REG                                   (0x0001D328)
//Duplicate of MC0_CH0_CR_CADB_OVERRIDE_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG                     (0x0001D330)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_STAGR_POLY_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_CFG_0_REG                          (0x0001D338)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_CFG_1_REG                          (0x0001D339)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_CFG_2_REG                          (0x0001D33A)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_CFG_3_REG                          (0x0001D33B)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_USQ_CFG_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG                    (0x0001D33C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_SAVE_RELOAD_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_PBUF_0_REG                         (0x0001D340)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_PBUF_1_REG                         (0x0001D344)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_PBUF_2_REG                         (0x0001D348)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_PBUF_3_REG                         (0x0001D34C)
//Duplicate of MC0_CH0_CR_CADB_DSEL_UNISEQ_PBUF_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_LMN_REG                            (0x0001D350)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_LMN_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_POLY_0_REG                         (0x0001D358)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_POLY_1_REG                         (0x0001D35C)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_POLY_2_REG                         (0x0001D360)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH3_CR_CADB_DSEL_UNISEQ_POLY_3_REG                         (0x0001D364)
//Duplicate of MC0_CH0_CR_CPGC_DPAT_UNISEQ_POLY_0_REG

#define MC1_CH3_CR_CADB_BUF_0_REG                                      (0x0001D368)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_1_REG                                      (0x0001D370)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_2_REG                                      (0x0001D378)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_3_REG                                      (0x0001D380)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_4_REG                                      (0x0001D388)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_5_REG                                      (0x0001D390)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_6_REG                                      (0x0001D398)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_7_REG                                      (0x0001D3A0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_8_REG                                      (0x0001D3A8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_9_REG                                      (0x0001D3B0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_10_REG                                     (0x0001D3B8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_11_REG                                     (0x0001D3C0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_12_REG                                     (0x0001D3C8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_13_REG                                     (0x0001D3D0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_14_REG                                     (0x0001D3D8)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_BUF_15_REG                                     (0x0001D3E0)
//Duplicate of MC0_CH0_CR_CADB_BUF_0_REG

#define MC1_CH3_CR_CADB_AO_MRSCFG_REG                                  (0x0001D3E8)
//Duplicate of MC0_CH0_CR_CADB_AO_MRSCFG_REG

#define MC1_CH3_CR_CADB_SELCFG_REG                                     (0x0001D3F0)
//Duplicate of MC0_CH0_CR_CADB_SELCFG_REG

#define MC1_CH3_CR_CADB_MRSCFG_REG                                     (0x0001D3F8)
//Duplicate of MC0_CH0_CR_CADB_MRSCFG_REG

#define MC1_IBECC_ACTIVATE_REG                                         (0x0001D400)
//Duplicate of MC0_IBECC_ACTIVATE_REG

#define MC1_IBECC_STORAGE_ADDR_REG                                     (0x0001D404)
//Duplicate of MC0_IBECC_STORAGE_ADDR_REG

#define MC1_IBECC_PROTECT_ADDR_RANGE_0_REG                             (0x0001D408)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC1_IBECC_PROTECT_ADDR_RANGE_1_REG                             (0x0001D410)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC1_IBECC_PROTECT_ADDR_RANGE_2_REG                             (0x0001D418)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC1_IBECC_PROTECT_ADDR_RANGE_3_REG                             (0x0001D420)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC1_IBECC_PROTECT_ADDR_RANGE_4_REG                             (0x0001D428)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC1_IBECC_PROTECT_ADDR_RANGE_5_REG                             (0x0001D430)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC1_IBECC_PROTECT_ADDR_RANGE_6_REG                             (0x0001D438)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC1_IBECC_PROTECT_ADDR_RANGE_7_REG                             (0x0001D440)
//Duplicate of MC0_IBECC_PROTECT_ADDR_RANGE_0_REG

#define MC1_IBECC_CONTROL_REG                                          (0x0001D448)
//Duplicate of MC0_IBECC_CONTROL_REG

#define MC1_IBECC_INJ_ADDR_COMPARE_REG                                 (0x0001D450)
//Duplicate of MC0_IBECC_INJ_ADDR_COMPARE_REG

#define MC1_IBECC_INJ_ADDR_MASK_REG                                    (0x0001D458)
//Duplicate of MC0_IBECC_INJ_ADDR_COMPARE_REG

#define MC1_IBECC_INJ_COUNT_REG                                        (0x0001D460)
//Duplicate of MC0_IBECC_INJ_COUNT_REG

#define MC1_IBECC_INJ_CONTROL_REG                                      (0x0001D464)
//Duplicate of MC0_IBECC_INJ_CONTROL_REG

#define MC1_IBECC_ERROR_LOG_REG                                        (0x0001D468)
//Duplicate of MC0_IBECC_ERROR_LOG_REG

#define MC1_IBECC_TME_FM_DEVICE_ID_REG                                 (0x0001D4E8)
//Duplicate of MC0_IBECC_TME_FM_DEVICE_ID_REG

#define MC1_IBECC_TMECC_PARITY_ERROR_LOG_REG                           (0x0001D4F0)
//Duplicate of MC0_IBECC_TMECC_PARITY_ERROR_LOG_REG

#define MC1_IBECC_TME_CONTROL_A0_REG                                   (0x0001D4FC)
#define MC1_IBECC_TME_CONTROL_REG                                      (0x0001D4F8)
//Duplicate of MC0_IBECC_TME_CONTROL_REG

#define MC1_MAD_INTER_CHANNEL_REG                                      (0x0001D800)
//Duplicate of MC0_MAD_INTER_CHANNEL_REG

#define MC1_MAD_INTRA_CH0_REG                                          (0x0001D804)
//Duplicate of MC0_MAD_INTRA_CH0_REG

#define MC1_MAD_INTRA_CH1_REG                                          (0x0001D808)
//Duplicate of MC0_MAD_INTRA_CH0_REG

#define MC1_MAD_DIMM_CH0_REG                                           (0x0001D80C)
//Duplicate of MC0_MAD_DIMM_CH0_REG

#define MC1_MAD_DIMM_CH1_REG                                           (0x0001D810)
//Duplicate of MC0_MAD_DIMM_CH0_REG
#define MC1_MCDECS_MISC_REG                                            (0x0001D818)
//Duplicate of MC0_MCDECS_MISC_REG

#define MC1_MCDECS_CBIT_REG                                            (0x0001D81C)
//Duplicate of MC0_MCDECS_CBIT_REG

#define MC1_CHANNEL_HASH_REG                                           (0x0001D824)
//Duplicate of MC0_CHANNEL_HASH_REG

#define MC1_CHANNEL_EHASH_REG                                          (0x0001D828)
//Duplicate of MC0_CHANNEL_EHASH_REG

#define MC1_MC_INIT_STATE_G_REG                                        (0x0001D830)
//Duplicate of MC0_MC_INIT_STATE_G_REG

#define MC1_MRC_REVISION_REG                                           (0x0001D834)
//Duplicate of MC0_MRC_REVISION_REG

#define MC1_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_REG                       (0x0001D83C)
//Duplicate of MC0_PWM_PROGRAMMABLE_REQCOUNT_CONFIG_REG

#define MC1_PWM_TOTAL_REQCOUNT_REG                                     (0x0001D840)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC1_PWM_PROGRAMMABLE_REQCOUNT_0_REG                            (0x0001D848)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC1_PWM_PROGRAMMABLE_REQCOUNT_1_REG                            (0x0001D850)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC1_PWM_RDCAS_COUNT_REG                                        (0x0001D858)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC1_PM_SREF_CONFIG_REG                                         (0x0001D860)
//Duplicate of MC0_PM_SREF_CONFIG_REG

#define MC1_ATMC_STS_REG                                               (0x0001D864)
//Duplicate of MC0_ATMC_STS_REG

#define MC1_READ_OCCUPANCY_COUNT_REG                                   (0x0001D868)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC1_STALL_DRAIN_REG                                            (0x0001D874)
//Duplicate of MC0_STALL_DRAIN_REG

#define MC1_IPC_MC_ARB_REG                                             (0x0001D878)
//Duplicate of MC0_IPC_MC_ARB_REG

#define MC1_IPC_MC_DEC_ARB_REG                                         (0x0001D87C)
//Duplicate of MC0_IPC_MC_ARB_REG

#define MC1_QUEUE_CREDIT_C_REG                                         (0x0001D880)
//Duplicate of MC0_QUEUE_CREDIT_C_REG

#define MC1_ECC_INJ_ADDR_COMPARE_REG                                   (0x0001D888)
//Duplicate of MC0_ECC_INJ_ADDR_COMPARE_REG

#define MC1_REMAPBASE_REG                                              (0x0001D890)
//Duplicate of MC0_REMAPBASE_REG

#define MC1_REMAPLIMIT_REG                                             (0x0001D898)
//Duplicate of MC0_REMAPLIMIT_REG

#define MC1_PWM_WRCAS_COUNT_REG                                        (0x0001D8A0)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC1_PWM_COMMAND_COUNT_REG                                      (0x0001D8A8)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC1_PWM_NON_SR_COUNT_REG                                       (0x0001D8B0)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC1_TOLUD_REG                                                  (0x0001D8BC)
//Duplicate of MC0_TOLUD_REG

#define MC1_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG                         (0x0001D900)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC1_GDXC_DDR_SYS_ADD_FILTER_MASK_1_REG                         (0x0001D908)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC1_GDXC_DDR_SYS_ADD_FILTER_MATCH_0_REG                        (0x0001D910)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC1_GDXC_DDR_SYS_ADD_FILTER_MATCH_1_REG                        (0x0001D918)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC1_GDXC_DDR_SYS_ADD_TRIGGER_MASK_REG                          (0x0001D920)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC1_GDXC_DDR_SYS_ADD_TRIGGER_MATCH_REG                         (0x0001D928)
//Duplicate of MC0_GDXC_DDR_SYS_ADD_FILTER_MASK_0_REG

#define MC1_SC_QOS_REG                                                 (0x0001D930)
//Duplicate of MC0_SC_QOS_REG

#define MC1_MCMAINS_GLOBAL_DRIVER_GATE_CFG_REG                         (0x0001D938)
//Duplicate of MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_REG

#define MC1_PM_CONTROL_REG                                             (0x0001D93C)
//Duplicate of MC0_PM_CONTROL_REG

#define MC1_PWM_COUNTERS_DURATION_REG                                  (0x0001D948)
//Duplicate of MC0_PWM_COUNTERS_DURATION_REG

#define MC1_MCDECS_SECOND_CBIT_REG                                     (0x0001D954)
//Duplicate of MC0_MCDECS_SECOND_CBIT_REG

#define MC1_ECC_INJ_ADDR_MASK_REG                                      (0x0001D958)
//Duplicate of MC0_ECC_INJ_ADDR_MASK_REG

#define MC1_SC_QOS2_REG                                                (0x0001D960)
//Duplicate of MC0_SC_QOS2_REG

#define MC1_SC_QOS3_REG                                                (0x0001D968)
//Duplicate of MC0_SC_QOS3_REG

#define MC1_NORMALMODE_CFG_REG                                         (0x0001D96C)
//Duplicate of MC0_NORMALMODE_CFG_REG

#define MC1_MC_CPGC_CMI_REG                                            (0x0001D970)
//Duplicate of MC0_MC_CPGC_CMI_REG

#define MC1_MC_CPGC_MISC_DFT_REG                                       (0x0001D974)
//Duplicate of MC0_MC_CPGC_MISC_DFT_REG

#define MC1_PWM_GLB_DRV_OFF_COUNT_REG                                  (0x0001D978)
//Duplicate of MC0_PWM_TOTAL_REQCOUNT_REG

#define MC1_PARITYERRLOG_REG                                           (0x0001D9A0)
//Duplicate of MC0_PARITYERRLOG_REG

#define MC1_PARITY_ERR_INJ_REG                                         (0x0001D9A8)
//Duplicate of MC0_PARITY_ERR_INJ_REG

#define MC1_PARITY_CONTROL_REG                                         (0x0001D9B4)
//Duplicate of MC0_PARITY_CONTROL_REG

#define MC1_MAD_MC_HASH_REG                                            (0x0001D9B8)
//Duplicate of MC0_MAD_MC_HASH_REG

#define MC1_PMON_GLOBAL_CONTROL_REG                                    (0x0001D9C0)
//Duplicate of MC0_PMON_GLOBAL_CONTROL_REG

#define MC1_PMON_UNIT_CONTROL_REG                                      (0x0001D9C4)
//Duplicate of MC0_PMON_UNIT_CONTROL_REG

#define MC1_PMON_COUNTER_CONTROL_0_REG                                 (0x0001D9D0)
//Duplicate of MC0_PMON_COUNTER_CONTROL_0_REG

#define MC1_PMON_COUNTER_CONTROL_1_REG                                 (0x0001D9D4)
//Duplicate of MC0_PMON_COUNTER_CONTROL_0_REG

#define MC1_PMON_COUNTER_CONTROL_2_REG                                 (0x0001D9D8)
//Duplicate of MC0_PMON_COUNTER_CONTROL_0_REG

#define MC1_PMON_COUNTER_CONTROL_3_REG                                 (0x0001D9DC)
//Duplicate of MC0_PMON_COUNTER_CONTROL_0_REG

#define MC1_PMON_COUNTER_CONTROL_4_REG                                 (0x0001D9E0)
//Duplicate of MC0_PMON_COUNTER_CONTROL_0_REG

#define MC1_PMON_COUNTER_DATA_0_REG                                    (0x0001D9E8)
//Duplicate of MC0_PMON_COUNTER_DATA_0_REG

#define MC1_PMON_COUNTER_DATA_1_REG                                    (0x0001D9F0)
//Duplicate of MC0_PMON_COUNTER_DATA_0_REG

#define MC1_PMON_COUNTER_DATA_2_REG                                    (0x0001D9F8)
//Duplicate of MC0_PMON_COUNTER_DATA_0_REG

#define MC1_PMON_COUNTER_DATA_3_REG                                    (0x0001DA00)
//Duplicate of MC0_PMON_COUNTER_DATA_0_REG

#define MC1_PMON_COUNTER_DATA_4_REG                                    (0x0001DA08)
//Duplicate of MC0_PMON_COUNTER_DATA_0_REG

#define MC1_OS_TELEMETRY_CONTROL_REG                                   (0x0001DA10)
//Duplicate of MC0_OS_TELEMETRY_CONTROL_REG

#define MC1_DDRPL_CFG_DTF_REG                                          (0x0001DF00)
//Duplicate of MC0_DDRPL_CFG_DTF_REG

#define MC1_DDRPL_DEBUG_DTF_REG                                        (0x0001DF08)
//Duplicate of MC0_DDRPL_DEBUG_DTF_REG

#define MC1_DDRPL_VISA_LANES_REG                                       (0x0001DF0C)
//Duplicate of MC0_DDRPL_VISA_LANES_REG

#define MC1_DDRPL_VISA_CFG_DTF_REG                                     (0x0001DF10)
//Duplicate of MC0_DDRPL_VISA_CFG_DTF_REG
#pragma pack(pop)
#endif
