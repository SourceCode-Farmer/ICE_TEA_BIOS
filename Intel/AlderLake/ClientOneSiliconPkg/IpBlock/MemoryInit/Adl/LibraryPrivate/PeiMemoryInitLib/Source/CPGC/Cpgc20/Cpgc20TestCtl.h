/** @file
  This file defines functions for setting up test control
  registers for CPGC 2.0.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2016 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _CPGC_20_TEST_CTL_H_
#define _CPGC_20_TEST_CTL_H_
#include "MrcCpgcApi.h"
#include "Cpgc20.h"

/**
  This function programs the masks that enable error checking on the
  requested cachelines and chunks.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  CachelineMask - Bit Mask of cachelines to enable.
  @param[in]  ChunkMask     - Bit Mask of chunks to enable.

  @retval Nothing
**/
void
Cpgc20SetChunkClErrMsk (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                CachelineMask,
  IN  UINT32                ChunkMask
  );

/**
  This function programs the masks that enable error checking on the
  requested bytes (as per McChBitMask).

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  McMask        - Memory Controller Mask for which Controllers to apply to.
  @param[in]  ChMask        - Channel Mask for which Channels to apply to.
  @param[in]  WordMask      - Which Word (Lower or Upper) to apply ErrMask to.
  @param[in]  ErrMask       - Error Masking Value to apply.

  @retval Nothing
**/
void
Cpgc20SetDataErrMsk (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                WordMask,
  IN  UINT32                ErrMask
  );

/**
  This function programs the masks that enable ecc error checking on the
  requested bytes.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  EccValue      - Error Masking Value to apply.

  @retval Nothing
**/
void
Cpgc20SetEccErrMsk (
  IN  MrcParameters *const MrcData,
  IN  UINT32               EccValue
  );

/**
  This function programs the error conditions to stop the CPGC engine on.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  StopType      - Stop type for CPGC engine.
  @param[in]  NumOfErr      - Number of Stop Type errors to wait on before stopping CPGC engine.

  @retval Nothing
**/
void
Cpgc20SetupTestErrCtl (
  IN  MrcParameters       *const  MrcData,
  IN  MRC_TEST_STOP_TYPE          StopType,
  IN  UINT32                      NumOfErr
  );

/**
  This function will Setup REUT Error Counters to count errors for specified type.

  @param[in]  MrcData         - Pointer to MRC global data.
  @param[in]  CounterPointer  - Specifies in register which counter to setup. Each Channel has 9 counters including ECC.
  @param[in]  ErrControlSel   - Specifies which type of error counter read will be executed.
  @param[in]  CounterSetting  - Specifies in register which Lane/Byte/Chunk to track in specified counter,
                                based on ErrControlSel value.
  @param[in]  CounterScope    - Specifies if the Pointer is used or not.
  @param[in]  CounterUI       - Specifies which UI will be considered when counting errors.
                                  00 - All UI; 01 - Even UI; 10 - Odd UI; 11 - Particular UI (COUNTER_CONTROL_SEL = ErrCounterCtlPerUI)

  @retval mrcWrongInputParameter if CounterSetting is incorrect for the ErrControl selected, otherwise mrcSuccess.
**/
MrcStatus
Cpgc20SetupErrCounterCtl (
  IN MrcParameters *const      MrcData,
  IN UINT8                     CounterPointer,
  IN MRC_ERR_COUNTER_CTL_TYPE  ErrControlSel,
  IN UINT32                    CounterSetting,
  IN UINT8                     CounterScope,
  IN UINT8                     CounterUI
  );

/**
  This function returns the Error status results for specified MRC_ERR_STATUS_TYPE.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Desired Controller.
  @param[in]  Channel     - Desired Channel.
  @param[in]  Param       - Specifies which type of error status read will be executed.
  @param[out] Buffer      - Pointer to buffer which register values will be read into.
                              Error status bits will be returned starting with bit zero.

  @retval Returns mrcWrongInputParameter if Param value is not supported by this function, otherwise mrcSuccess.
**/
MrcStatus
Cpgc20GetErrEccChunkRankByteStatus (
  IN  MrcParameters   *const  MrcData,
  IN  UINT32                  Controller,
  IN  UINT32                  Channel,
  IN  MRC_ERR_STATUS_TYPE     Param,
  OUT UINT64          *const  Buffer
  );

/**
  This function accesses the Sequence loop count.

  @param[in]  MrcData    - Pointer to MRC global data.
  @param[in]  LoopCount  - Pointer to variable to store or set.

  @retval Nothing.
**/
void
Cpgc20SetLoopCount (
  IN      MrcParameters *const  MrcData,
  IN OUT  UINT32  *const        LoopCount
  );

/**
  This function accesses the Logical to Physical Bank Mapping.

  @param[in]  MrcData    - Pointer to MRC global data.
  @param[in]  Controller - Desired Controller.
  @param[in]  Channel    - Desired Channel.
  @param[in]  Banks      - Pointer to buffer to logical-to-physical bank mapping.
  @param[in]  Count      - Length of the Banks buffer.
  @param[in]  IsGet      - TRUE: Get.  FALSE: Set.

  @retval Nothing.
**/
void
Cpgc20GetSetBankMap (
  IN      MrcParameters *const  MrcData,
  IN      UINT32                Controller,
  IN      UINT32                Channel,
  IN OUT  UINT8 *const          Banks,
  IN      UINT32                Count,
  IN      BOOLEAN               IsGet
  );

/**
  This function returns the Bit Group Error status results.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Desired Controller.
  @param[in]  Channel     - Desired Channel.
  @param[out] Status      - Pointer to array where the lane error status values will be stored.

  @retval Nothing.
**/
void
Cpgc20GetBitGroupErrStatus (
  IN  MrcParameters   *const  MrcData,
  IN  UINT32                  Controller,
  IN  UINT32                  Channel,
  OUT UINT8                   *Status
  );

/**
  This function returns the Error Counter status for specified counter.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  Controller    - Desired Controller.
  @param[in]  Channel       - Desired Channel.
  @param[in]  CounterSelect - Desired error counter to read from.
  @param[out] CounterStatus - Pointer to buffer where counter status will be held.
  @param[out] Overflow      - Indicates if counter has reached overflow.

  @retval Nothing.
**/
void
Cpgc20GetErrCounterStatus (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                CounterSelect,
  OUT UINT32        *const  CounterStatus,
  OUT BOOLEAN       *const  Overflow
  );

/**
  This function writes to all enabled CPGC SEQ CTL registers.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  McChBitMask   - Memory Controller Channel Bit mask for which registers should be programmed for.
  @param[in]  CpgcSeqCtl    - Data to be written to all CPGC SEQ CTL registers.

  @retval Nothing.
**/
void
Cpgc20ControlRegWrite (
  IN  MrcParameters *const             MrcData,
  IN  UINT8                            McChBitMask,
  IN  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT  CpgcSeqCtl
  );

/**
  This function aggregates the status of STOP_TEST bit for all enabled CPGC engines.

  @param[in]  MrcData       - Pointer to MRC global data.

  @retval UINT8 of the aggregated value of STOP_TEST.
**/
UINT8
Cpgc20ControlRegStopBitStatus (
  IN  MrcParameters *const  MrcData
  );

/**
  This function writes to all enabled CADB CTL registers.

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  McChBitMask  - Memory Controller Channel Bit mask for which registers should be programmed for.
  @param[in]  CadbControl  - Data to be written to all CADB CTL registers.

  @retval Nothing.
**/
void
Cadb20ControlRegWrite (
  IN  MrcParameters *const                 MrcData,
  IN  UINT8                                McChBitMask,
  IN  MC0_CH0_CR_CADB_CTL_STRUCT           CadbControl
  );

/**
  This function writes to all enabled CADB CFG registers.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  CadbConfig    - Data to be written to all CADB CFG registers.

  @retval Nothing.
**/
void
Cadb20ConfigRegWrite (
  IN  MrcParameters *const                 MrcData,
  IN  MC0_CH0_CR_CADB_CFG_STRUCT           CadbConfig
  );

/**
  This function writes to all enabled CADB_AO_MRSCFG registers.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  CadbMrsConfig - Data to be written to all CADB_AO_MRSCFG registers.

  @retval Nothing.
**/
void
Cadb20MrsConfigRegWrite (
  IN  MrcParameters *const                 MrcData,
  IN  MC0_CH0_CR_CADB_AO_MRSCFG_STRUCT     CadbMrsConfig
  );

/**
  This function checks the value of CADB_STATUS.TEST_DONE bit for all enabled CADB engines.

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  McChBitMask  - Bitmask of participating controllers / channels

  @retval 1 - if all enabled engines are done, otherwise 0
**/
UINT32
Cadb20TestDoneStatus (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 McChBitMask
  );

/**
  This function writes Command Instructions to all enabled CPGC engines.

  @param[in]  MrcData        - Pointer to MRC global data.
  @param[in]  CmdInstruct    - Array of Command Instructions
  @param[in]  NumInstruct    - Number of Instructions

  @retval MrcStatus - mrcSuccess if does not exceed command instruction registers otherwise mrcFail
**/
MrcStatus
Cpgc20CmdInstructWrite (
  IN  MrcParameters *const                            MrcData,
  IN  MC0_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_0_STRUCT  *CmdInstruct,
  IN  UINT8                                           NumInstruct
  );

/**
  This function writes Algorithm Instructions to all enabled CPGC engines.

  @param[in]  MrcData               - Pointer to MRC global data.
  @param[in]  AlgoInstruct          - Array of Algorithm Instructions
  @param[in]  AlgoInstructControl   - Algorithm Instruction Control setting to program based on ValidMask
  @param[in]  AlgoWaitEventControl  - Algorithm Wait Event Control setting to program
  @param[in]  ValidMask             - Bit Mask of which Instructions are valid

  @retval Nothing.
**/
void
Cpgc20AlgoInstructWrite (
  IN  MrcParameters *const                                      MrcData,
  IN  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT          *AlgoInstruct,
  IN  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_STRUCT     AlgoInstructControl,
  IN  MC0_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_STRUCT     AlgoWaitEventControl,
  IN  UINT8                                                     ValidMask
  );

/**
  This function writes Data Instructions to all enabled CPGC engines.

  @param[in]  MrcData        - Pointer to MRC global data.
  @param[in]  DataInstruct   - Array of Data Instructions
  @param[in]  ValidMask      - Bit Mask of which Instructions are valid

  @retval Nothing.
**/
void
Cpgc20DataInstructWrite (
  IN  MrcParameters *const                            MrcData,
  IN  MC0_REQ0_CR_CPGC2_DATA_INSTRUCTION_0_STRUCT     *DataInstruct,
  IN  UINT8                                           ValidMask
  );

/**
  This function sets up Address related registers (ADDRESS_INSTRUCTION, REGION_LOW_ROW, REGION_LOW_COL, ADDRESS_SIZE) to all enabled CPGC engines.

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  AddressOrder      - Address Instruction Order
  @param[in]  AddressDirection  - Address Instruction Direction
  @param[in]  LastValidInstruct - Last Valid Address Instruction
  @param[in]  RowStart          - Starting Row Address
  @param[in]  ColStart          - Starting Column Address
  @param[in]  RowSizeBits       - Memory Region/Block Bits Size for Row
  @param[in]  ColSizeBits       - Memory Region/Block Bits Size for Column
  @param[in]  BankSize          - # of Banks (Base 1 numbering)
  @param[in]  EnCADB            - Set up the address ordering for Command stress

  @retval Nothing.
**/
void
Cpgc20AddressSetup (
  IN  MrcParameters *const            MrcData,
  IN  CPGC20_ADDRESS_INCREMENT_ORDER  AddressOrder,
  IN  CPGC20_ADDRESS_DIRECTION        AddressDirection,
  IN  UINT8                           LastValidInstruct,
  IN  UINT32                          RowStart,
  IN  UINT16                          ColStart,
  IN  UINT8                           RowSizeBits,
  IN  UINT8                           ColSizeBits,
  IN  UINT8                           BankSize,
  IN  UINT8                           EnCADB
  );

/**
  This function sets up ADDRESS_SIZE register per channel according to the Bank/Row/Col size of the given Rank.
  This is used in memory scrubbing.
  The register will be updated on all enabled CPGC engines.

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  Rank              - Rank to work on

**/
void
Cpgc20AddressSetupScrub (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Rank
  );

/**
  This function sets up Address related registers for CMD stress to all enabled CPGC engines.

  @param[in]  MrcData           - Pointer to MRC global data.

  @retval Nothing.
**/
void
Cpgc20AddressSetupCmdStress (
  IN  MrcParameters *const  MrcData
  );

/**
This function sets up Address related registers (ADDRESS_INSTRUCTION, REGION_LOW_ROW, REGION_LOW_COL, ADDRESS_SIZE) to all enabled CPGC engines for PPR mode.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Targeted Controller
  @param[in]  Channel     - Targeted Channel
  @param[in]  RowStart    - Starting Row Address
  @param[in]  ColStart    - Starting Column Address
  @param[in]  BankGroup   - Targeted Bank Group
  @param[in]  BankAddress - Targeted Bank Address

  @retval MrcStatus       - mrcSuccess if no errors encountered
**/
MrcStatus
Cpgc20AddressSetupPPR (
  IN  MrcParameters *const MrcData,
  IN  UINT32 const         McChBitMask,
  IN  UINT32               RowStart,
  IN  UINT16               ColStart,
  IN  UINT8                RowSizeBits,
  IN  UINT8                ColSizeBits,
  IN  UINT32               BlockSizeMaxBank,
  IN  UINT32               BankStart
  );

/**
  This function sets up Base Repeats to all enabled CPGC engines.

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  Burst             - Number of Cachelines (Should be power of 2)
  @param[in]  Ranks             - # of Ranks to test

  @retval Value written to CPGC2_BASE_REPEATS.
**/
UINT32
Cpgc20BaseRepeats (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Burst,
  IN  UINT32                Ranks
  );

/**
  This function sets up Base Repeats for a given CPGC engine (per Channel / SubChannel).
  The function doesn't check for ch/subch presence.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Controller to work on
  @param[in]  Channel     - Channel to work on
  @param[in]  Burst       - Number of Cachelines (Should be power of 2)
  @param[in]  Ranks       - # of Ranks to test

  @retval Value written to CPGC2_BASE_REPEATS.
**/
UINT32
Cpgc20BaseRepeatsMcCh (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Burst,
  IN  UINT32                Ranks
  );

/**
  This function sets up Registers for Basic Mem Test to all enabled CPGC engines.

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  Burst             - Number of CL transactions per algorithm instruction within 1 BlockRepeat (outer loop)
  @param[in]  LoopCount         - Total # of CL transactions
  @param[in]  Ranks             - # of Ranks

  @retval Nothing.
**/
void
Cpgc20BasicMemTest (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Burst,
  IN  UINT32                LoopCount,
  IN  UINT8                 Ranks
  );

/**
  This function adjusts register settings that are based on # of ranks being tested for the channel/subChannel provided.

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  Controller        - Controller to work on
  @param[in]  Channel           - Channel to work on
  @param[in]  OrigRank          - Original # of Ranks
  @param[in]  NewRank           - New # of Ranks

  @retval Nothing.
**/
void
Cpgc20AdjustNumOfRanks (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT8                 OrigRank,
  IN  UINT8                 NewRank
  );

/**
  This function programs the specified value of MaxBanks to the Raster Repo Mode 3 register

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  McChBitMask  - Bitmask of participating controllers / channels
  @param[in]  MaxBanks     - Maximum number of Banks used in Raster Repo Mode 3

  @retval nothing
**/
void
Cpgc20SetRasterRepoMode3MaxBanks (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 McChBitMask,
  IN  UINT8                 MaxBanks
  );

/**
  This function programs the Raster Repo Configuration register based on input values.

  @param[in]  MrcData         - Pointer to MRC global data.
  @param[in]  McChBitMask     - Bitmask of participating controllers / channels
  @param[in]  Mode3Banks      - Specifies which Bank mode to use in Raster Repo Mode 3 usage.
  @param[in]  UpperBanks      - Indicates the base Bank to store when using Raster Repo Mode 3
  @param[in]  StopOnRaster    - If TRUE, the test will stop after status condition is met in Raster Repo
  @param[in]  RasterRepoClear - If set, Raster Repo entries will be Reset (this bit auto-clears)
  @param[in]  RasterRepoMode  - Indicates the mode of operation for Raster Repo

  @retval nothing
**/
void
Cpgc20RasterRepoConfig (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 McChBitMask,
  IN  UINT8         *const  Mode3Banks,
  IN  UINT8         *const  Mode3UpperBanks,
  IN  BOOLEAN       *const  StopOnRaster,
  IN  BOOLEAN       *const  RasterRepoClear,
  IN  UINT8         *const  RasterRepoMode
  );

/**
  This function programs the number of Reads to mask in CPGC test, based on input value.

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  McChBitMask  - Bitmask of participating controllers / channels
  @param[in]  NumMaskReads - Number of Reads to mask

  @retval 1 - if all enabled engines are done, otherwise 0
**/
void
Cpgc20MaskErrFirstNRead (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 McChBitMask,
  IN  UINT8                 NumMaskReads
  );

/**
  This function Reads the Repo Content registers, and returns those values in an input array.

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  Controller   - Controller to work on
  @param[in]  Channel      - Channel to work on
  @param[out] RepoStatus   - Contains the data from Repo Content registers, for 8 normal entries and 2 ECC entries

  @retval nothing
**/
void
Cpgc20ReadRasterRepoContent (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  OUT UINT64                RepoStatus[CPGC20_MAX_RASTER_REPO_CONTENT]
  );

/**
  This function reads the Raster Repo Status register and parses the individual fields into the input pointers.

  @param[in]   MrcData        - Pointer to MRC global data.
  @param[in]   Controller     - Controller to work on
  @param[in]   Channel        - Channel to work on
  @param[out]  BitmapValid    - Optional pointer, if provided it will be updated with indication of valid bits in Repo Status
  @param[out]  SummaryValid   - Optional pointer, if provided it will be updated with indication that test summary in Raster Repo is valid
  @param[out]  FailExcessAll  - Optional pointer, if provided it will be updated with indication of any bank that observed a number of failures that exceeded MAX_BANKS setting
  @param[out]  FailAnyAll     - Optional pointer, if provided it will be updated with indication if any bank failed
  @param[out]  NumErrLogged   - Optional pointer, if provided it will be updated with number of errors logged
  @param[out]  RasterRepoFull - Optional pointer, if provided it will be updated with indication of RasterRepo being full

  @retval nothing
**/
void
Cpgc20ReadRasterRepoStatus (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 Controller,
  IN  UINT8                 Channel,
  OUT UINT8         *const  BitmapValid,
  OUT UINT8         *const  SummaryValid,
  OUT UINT8         *const  FailExcessAll,
  OUT UINT8         *const  FailAnyAll,
  OUT UINT8         *const  NumErrLogged,
  OUT UINT8         *const  RasterRepoFull
  );

/**
  @todo this function may be used with other Raster Repo Modes to further identify DRAM Devices which saw failures

  @param[in]   MrcData        - Pointer to MRC global data.
  @param[in]   Controller     - Controller to work on
  @param[in]   Channel        - Channel to work on
  @param[out]  ErrorSummary   - Pointer to ErrorSummary Array for A/B/C registers

  @retval nothing
**/
void
Cpgc20ReadErrSummary (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 Controller,
  IN  UINT8                 Channel,
  OUT UINT32        *const  ErrorSummary
);

/**
  Setup CPGC command pattern for test

  @param[in]  MrcData            - Pointer to MRC global data.
  @param[in]  Controller         - Controller number
  @param[in]  Channel            - Channel number
  @param[in]  CmdPat             - Command pattern for test
  - PatWrRd
  - PatWr
  - PatRd
  @param[in]  *AddressCarry      - Structure for address carry
  @param[in]  SubSeqWait         - Number of Dclks to stall at the end of a sub-sequence

  @retval N/A
**/
VOID
SetCpgcCmdPat (
  IN MrcParameters   *const MrcData,
  IN UINT8                  Controller,
  IN UINT8                  Channel,
  IN UINT8                  CmdPat,
  IN TAddressCarry          *AddressCarry,
  IN UINT16                 SubSeqWait
);

#endif // _CPGC_20_TEST_CTL_H_
