#ifndef __MrcMcRegisterAdl5xxx_h__
#define __MrcMcRegisterAdl5xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2020 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define CAMARILLO_MAILBOX_DATA0_PCU_REG                                (0x00005810)

  #define CAMARILLO_MAILBOX_DATA0_PCU_DATA_OFF                         ( 0)
  #define CAMARILLO_MAILBOX_DATA0_PCU_DATA_WID                         (32)
  #define CAMARILLO_MAILBOX_DATA0_PCU_DATA_MSK                         (0xFFFFFFFF)
  #define CAMARILLO_MAILBOX_DATA0_PCU_DATA_MIN                         (0)
  #define CAMARILLO_MAILBOX_DATA0_PCU_DATA_MAX                         (4294967295) // 0xFFFFFFFF
  #define CAMARILLO_MAILBOX_DATA0_PCU_DATA_DEF                         (0x00000000)
  #define CAMARILLO_MAILBOX_DATA0_PCU_DATA_HSH                         (0x20005810)

#define CAMARILLO_MAILBOX_DATA1_PCU_REG                                (0x00005814)
//Duplicate of CAMARILLO_MAILBOX_DATA0_PCU_REG

#define CAMARILLO_MAILBOX_INTERFACE_PCU_REG                            (0x00005818)

  #define CAMARILLO_MAILBOX_INTERFACE_PCU_COMMAND_OFF                  ( 0)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_COMMAND_WID                  ( 8)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_COMMAND_MSK                  (0x000000FF)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_COMMAND_MIN                  (0)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_COMMAND_MAX                  (255) // 0x000000FF
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_COMMAND_DEF                  (0x00000000)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_COMMAND_HSH                  (0x08005818)

  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM1_OFF                   ( 8)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM1_WID                   ( 8)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM1_MSK                   (0x0000FF00)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM1_MIN                   (0)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM1_MAX                   (255) // 0x000000FF
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM1_DEF                   (0x00000000)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM1_HSH                   (0x08105818)

  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM2_OFF                   (16)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM2_WID                   (14)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM2_MSK                   (0x3FFF0000)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM2_MIN                   (0)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM2_MAX                   (16383) // 0x00003FFF
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM2_DEF                   (0x00000000)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_PARAM2_HSH                   (0x0E205818)

  #define CAMARILLO_MAILBOX_INTERFACE_PCU_RUN_BUSY_OFF                 (31)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_RUN_BUSY_WID                 ( 1)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_RUN_BUSY_MSK                 (0x80000000)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_RUN_BUSY_MIN                 (0)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_RUN_BUSY_MAX                 (1) // 0x00000001
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_RUN_BUSY_DEF                 (0x00000000)
  #define CAMARILLO_MAILBOX_INTERFACE_PCU_RUN_BUSY_HSH                 (0x013E5818)

#define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_REG                      (0x00005820)

  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_HIGH_TEMP_INT_ENABLE_OFF ( 0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_HIGH_TEMP_INT_ENABLE_WID ( 1)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_HIGH_TEMP_INT_ENABLE_MSK (0x00000001)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_HIGH_TEMP_INT_ENABLE_MIN (0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_HIGH_TEMP_INT_ENABLE_MAX (1) // 0x00000001
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_HIGH_TEMP_INT_ENABLE_DEF (0x00000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_HIGH_TEMP_INT_ENABLE_HSH (0x01005820)

  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_LOW_TEMP_INT_ENABLE_OFF ( 1)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_LOW_TEMP_INT_ENABLE_WID ( 1)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_LOW_TEMP_INT_ENABLE_MSK (0x00000002)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_LOW_TEMP_INT_ENABLE_MIN (0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_LOW_TEMP_INT_ENABLE_MAX (1) // 0x00000001
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_LOW_TEMP_INT_ENABLE_DEF (0x00000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_LOW_TEMP_INT_ENABLE_HSH (0x01025820)

  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_PROCHOT_INT_ENABLE_OFF ( 2)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_PROCHOT_INT_ENABLE_WID ( 1)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_PROCHOT_INT_ENABLE_MSK (0x00000004)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_PROCHOT_INT_ENABLE_MIN (0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_PROCHOT_INT_ENABLE_MAX (1) // 0x00000001
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_PROCHOT_INT_ENABLE_DEF (0x00000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_PROCHOT_INT_ENABLE_HSH (0x01045820)

  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_OUT_OF_SPEC_INT_ENABLE_OFF ( 4)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_OUT_OF_SPEC_INT_ENABLE_WID ( 1)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_OUT_OF_SPEC_INT_ENABLE_MSK (0x00000010)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_OUT_OF_SPEC_INT_ENABLE_MIN (0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_OUT_OF_SPEC_INT_ENABLE_MAX (1) // 0x00000001
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_OUT_OF_SPEC_INT_ENABLE_DEF (0x00000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_OUT_OF_SPEC_INT_ENABLE_HSH (0x01085820)

  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_REL_TEMP_OFF ( 8)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_REL_TEMP_WID ( 7)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_REL_TEMP_MSK (0x00007F00)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_REL_TEMP_MIN (0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_REL_TEMP_MAX (127) // 0x0000007F
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_REL_TEMP_DEF (0x00000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_REL_TEMP_HSH (0x07105820)

  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_INT_ENABLE_OFF (15)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_INT_ENABLE_WID ( 1)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_INT_ENABLE_MSK (0x00008000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_INT_ENABLE_MIN (0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_INT_ENABLE_MAX (1) // 0x00000001
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_INT_ENABLE_DEF (0x00000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_1_INT_ENABLE_HSH (0x011E5820)

  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_REL_TEMP_OFF (16)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_REL_TEMP_WID ( 7)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_REL_TEMP_MSK (0x007F0000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_REL_TEMP_MIN (0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_REL_TEMP_MAX (127) // 0x0000007F
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_REL_TEMP_DEF (0x00000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_REL_TEMP_HSH (0x07205820)

  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_INT_ENABLE_OFF (23)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_INT_ENABLE_WID ( 1)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_INT_ENABLE_MSK (0x00800000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_INT_ENABLE_MIN (0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_INT_ENABLE_MAX (1) // 0x00000001
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_INT_ENABLE_DEF (0x00000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD_2_INT_ENABLE_HSH (0x012E5820)

  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_POWER_INT_ENABLE_OFF   (24)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_POWER_INT_ENABLE_WID   ( 1)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_POWER_INT_ENABLE_MSK   (0x01000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_POWER_INT_ENABLE_MIN   (0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_POWER_INT_ENABLE_MAX   (1) // 0x00000001
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_POWER_INT_ENABLE_DEF   (0x00000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_POWER_INT_ENABLE_HSH   (0x01305820)

  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_TEMPERATURE_AVERAGING_TIME_WINDOW_OFF (25)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_TEMPERATURE_AVERAGING_TIME_WINDOW_WID ( 7)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_TEMPERATURE_AVERAGING_TIME_WINDOW_MSK (0xFE000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_TEMPERATURE_AVERAGING_TIME_WINDOW_MIN (0)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_TEMPERATURE_AVERAGING_TIME_WINDOW_MAX (127) // 0x0000007F
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_TEMPERATURE_AVERAGING_TIME_WINDOW_DEF (0x00000000)
  #define PACKAGE_THERM_CAMARILLO_INTERRUPT_PCU_TEMPERATURE_AVERAGING_TIME_WINDOW_HSH (0x07325820)

#define BIOS_POST_CODE_PCU_REG                                         (0x00005824)

  #define BIOS_POST_CODE_PCU_PostCode_OFF                              ( 0)
  #define BIOS_POST_CODE_PCU_PostCode_WID                              (32)
  #define BIOS_POST_CODE_PCU_PostCode_MSK                              (0xFFFFFFFF)
  #define BIOS_POST_CODE_PCU_PostCode_MIN                              (0)
  #define BIOS_POST_CODE_PCU_PostCode_MAX                              (4294967295) // 0xFFFFFFFF
  #define BIOS_POST_CODE_PCU_PostCode_DEF                              (0x00000000)
  #define BIOS_POST_CODE_PCU_PostCode_HSH                              (0x20005824)

#define DDR_PTM_CTL_PCU_REG                                            (0x00005880)

  #define DDR_PTM_CTL_PCU_OLTM_ENABLE_OFF                              ( 0)
  #define DDR_PTM_CTL_PCU_OLTM_ENABLE_WID                              ( 1)
  #define DDR_PTM_CTL_PCU_OLTM_ENABLE_MSK                              (0x00000001)
  #define DDR_PTM_CTL_PCU_OLTM_ENABLE_MIN                              (0)
  #define DDR_PTM_CTL_PCU_OLTM_ENABLE_MAX                              (1) // 0x00000001
  #define DDR_PTM_CTL_PCU_OLTM_ENABLE_DEF                              (0x00000000)
  #define DDR_PTM_CTL_PCU_OLTM_ENABLE_HSH                              (0x01005880)

  #define DDR_PTM_CTL_PCU_CLTM_ENABLE_OFF                              ( 1)
  #define DDR_PTM_CTL_PCU_CLTM_ENABLE_WID                              ( 1)
  #define DDR_PTM_CTL_PCU_CLTM_ENABLE_MSK                              (0x00000002)
  #define DDR_PTM_CTL_PCU_CLTM_ENABLE_MIN                              (0)
  #define DDR_PTM_CTL_PCU_CLTM_ENABLE_MAX                              (1) // 0x00000001
  #define DDR_PTM_CTL_PCU_CLTM_ENABLE_DEF                              (0x00000000)
  #define DDR_PTM_CTL_PCU_CLTM_ENABLE_HSH                              (0x01025880)

  #define DDR_PTM_CTL_PCU_REFRESH_2X_MODE_OFF                          ( 2)
  #define DDR_PTM_CTL_PCU_REFRESH_2X_MODE_WID                          ( 2)
  #define DDR_PTM_CTL_PCU_REFRESH_2X_MODE_MSK                          (0x0000000C)
  #define DDR_PTM_CTL_PCU_REFRESH_2X_MODE_MIN                          (0)
  #define DDR_PTM_CTL_PCU_REFRESH_2X_MODE_MAX                          (3) // 0x00000003
  #define DDR_PTM_CTL_PCU_REFRESH_2X_MODE_DEF                          (0x00000000)
  #define DDR_PTM_CTL_PCU_REFRESH_2X_MODE_HSH                          (0x02045880)

  #define DDR_PTM_CTL_PCU_EXTTS_ENABLE_OFF                             ( 4)
  #define DDR_PTM_CTL_PCU_EXTTS_ENABLE_WID                             ( 1)
  #define DDR_PTM_CTL_PCU_EXTTS_ENABLE_MSK                             (0x00000010)
  #define DDR_PTM_CTL_PCU_EXTTS_ENABLE_MIN                             (0)
  #define DDR_PTM_CTL_PCU_EXTTS_ENABLE_MAX                             (1) // 0x00000001
  #define DDR_PTM_CTL_PCU_EXTTS_ENABLE_DEF                             (0x00000000)
  #define DDR_PTM_CTL_PCU_EXTTS_ENABLE_HSH                             (0x01085880)

  #define DDR_PTM_CTL_PCU_LOCK_PTM_REGS_PCU_OFF                        ( 5)
  #define DDR_PTM_CTL_PCU_LOCK_PTM_REGS_PCU_WID                        ( 1)
  #define DDR_PTM_CTL_PCU_LOCK_PTM_REGS_PCU_MSK                        (0x00000020)
  #define DDR_PTM_CTL_PCU_LOCK_PTM_REGS_PCU_MIN                        (0)
  #define DDR_PTM_CTL_PCU_LOCK_PTM_REGS_PCU_MAX                        (1) // 0x00000001
  #define DDR_PTM_CTL_PCU_LOCK_PTM_REGS_PCU_DEF                        (0x00000000)
  #define DDR_PTM_CTL_PCU_LOCK_PTM_REGS_PCU_HSH                        (0x010A5880)

  #define DDR_PTM_CTL_PCU_PDWN_CONFIG_CTL_OFF                          ( 6)
  #define DDR_PTM_CTL_PCU_PDWN_CONFIG_CTL_WID                          ( 1)
  #define DDR_PTM_CTL_PCU_PDWN_CONFIG_CTL_MSK                          (0x00000040)
  #define DDR_PTM_CTL_PCU_PDWN_CONFIG_CTL_MIN                          (0)
  #define DDR_PTM_CTL_PCU_PDWN_CONFIG_CTL_MAX                          (1) // 0x00000001
  #define DDR_PTM_CTL_PCU_PDWN_CONFIG_CTL_DEF                          (0x00000000)
  #define DDR_PTM_CTL_PCU_PDWN_CONFIG_CTL_HSH                          (0x010C5880)

  #define DDR_PTM_CTL_PCU_DISABLE_DRAM_TS_OFF                          ( 7)
  #define DDR_PTM_CTL_PCU_DISABLE_DRAM_TS_WID                          ( 1)
  #define DDR_PTM_CTL_PCU_DISABLE_DRAM_TS_MSK                          (0x00000080)
  #define DDR_PTM_CTL_PCU_DISABLE_DRAM_TS_MIN                          (0)
  #define DDR_PTM_CTL_PCU_DISABLE_DRAM_TS_MAX                          (1) // 0x00000001
  #define DDR_PTM_CTL_PCU_DISABLE_DRAM_TS_DEF                          (0x00000000)
  #define DDR_PTM_CTL_PCU_DISABLE_DRAM_TS_HSH                          (0x010E5880)

  #define DDR_PTM_CTL_PCU_DDR4_SKIP_REFRESH_EN_OFF                     ( 8)
  #define DDR_PTM_CTL_PCU_DDR4_SKIP_REFRESH_EN_WID                     ( 1)
  #define DDR_PTM_CTL_PCU_DDR4_SKIP_REFRESH_EN_MSK                     (0x00000100)
  #define DDR_PTM_CTL_PCU_DDR4_SKIP_REFRESH_EN_MIN                     (0)
  #define DDR_PTM_CTL_PCU_DDR4_SKIP_REFRESH_EN_MAX                     (1) // 0x00000001
  #define DDR_PTM_CTL_PCU_DDR4_SKIP_REFRESH_EN_DEF                     (0x00000000)
  #define DDR_PTM_CTL_PCU_DDR4_SKIP_REFRESH_EN_HSH                     (0x01105880)

#define DDR_BANDWIDTH_CONTROL_PCU_REG                                  (0x00005884)

  #define DDR_BANDWIDTH_CONTROL_PCU_BANDWIDTH_THROTTLE_OFF             ( 0)
  #define DDR_BANDWIDTH_CONTROL_PCU_BANDWIDTH_THROTTLE_WID             ( 4)
  #define DDR_BANDWIDTH_CONTROL_PCU_BANDWIDTH_THROTTLE_MSK             (0x0000000F)
  #define DDR_BANDWIDTH_CONTROL_PCU_BANDWIDTH_THROTTLE_MIN             (0)
  #define DDR_BANDWIDTH_CONTROL_PCU_BANDWIDTH_THROTTLE_MAX             (15) // 0x0000000F
  #define DDR_BANDWIDTH_CONTROL_PCU_BANDWIDTH_THROTTLE_DEF             (0x00000000)
  #define DDR_BANDWIDTH_CONTROL_PCU_BANDWIDTH_THROTTLE_HSH             (0x04005884)

#define DDR_THERM_CAMARILLO_INTERRUPT_PCU_REG                          (0x000058A0)

  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_INT_ENABLE_OFF  ( 0)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_INT_ENABLE_WID  ( 1)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_INT_ENABLE_MSK  (0x00000001)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_INT_ENABLE_MIN  (0)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_INT_ENABLE_MAX  (1) // 0x00000001
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_INT_ENABLE_DEF  (0x00000000)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_INT_ENABLE_HSH  (0x010058A0)

  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_INT_ENABLE_OFF  ( 1)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_INT_ENABLE_WID  ( 1)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_INT_ENABLE_MSK  (0x00000002)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_INT_ENABLE_MIN  (0)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_INT_ENABLE_MAX  (1) // 0x00000001
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_INT_ENABLE_DEF  (0x00000000)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_INT_ENABLE_HSH  (0x010258A0)

  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_OFF             ( 8)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_WID             ( 8)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_MSK             (0x0000FF00)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_MIN             (0)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_MAX             (255) // 0x000000FF
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_DEF             (0x00000000)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD1_HSH             (0x081058A0)

  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_OFF             (16)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_WID             ( 8)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_MSK             (0x00FF0000)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_MIN             (0)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_MAX             (255) // 0x000000FF
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_DEF             (0x00000000)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_THRESHOLD2_HSH             (0x082058A0)

  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_TIME_WINDOW_OFF            (24)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_TIME_WINDOW_WID            ( 7)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_TIME_WINDOW_MSK            (0x7F000000)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_TIME_WINDOW_MIN            (0)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_TIME_WINDOW_MAX            (127) // 0x0000007F
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_TIME_WINDOW_DEF            (0x00000000)
  #define DDR_THERM_CAMARILLO_INTERRUPT_PCU_TIME_WINDOW_HSH            (0x073058A0)

#define DDR_VOLTAGE_PCU_REG                                            (0x000058A4)

  #define DDR_VOLTAGE_PCU_DDR_VOLTAGE_OFF                              ( 0)
  #define DDR_VOLTAGE_PCU_DDR_VOLTAGE_WID                              ( 3)
  #define DDR_VOLTAGE_PCU_DDR_VOLTAGE_MSK                              (0x00000007)
  #define DDR_VOLTAGE_PCU_DDR_VOLTAGE_MIN                              (0)
  #define DDR_VOLTAGE_PCU_DDR_VOLTAGE_MAX                              (7) // 0x00000007
  #define DDR_VOLTAGE_PCU_DDR_VOLTAGE_DEF                              (0x00000000)
  #define DDR_VOLTAGE_PCU_DDR_VOLTAGE_HSH                              (0x030058A4)

#define DDR_RAPL_LIMIT_PCU_REG                                         (0x000058E0)

  #define DDR_RAPL_LIMIT_PCU_LIMIT1_POWER_OFF                          ( 0)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_POWER_WID                          (15)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_POWER_MSK                          (0x00007FFF)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_POWER_MIN                          (0)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_POWER_MAX                          (32767) // 0x00007FFF
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_POWER_DEF                          (0x00000000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_POWER_HSH                          (0x4F0058E0)

  #define DDR_RAPL_LIMIT_PCU_LIMIT1_ENABLE_OFF                         (15)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_ENABLE_WID                         ( 1)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_ENABLE_MSK                         (0x00008000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_ENABLE_MIN                         (0)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_ENABLE_MAX                         (1) // 0x00000001
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_ENABLE_DEF                         (0x00000000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_ENABLE_HSH                         (0x411E58E0)

  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_Y_OFF                  (17)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_Y_WID                  ( 5)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_Y_MSK                  (0x003E0000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_Y_MIN                  (0)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_Y_MAX                  (31) // 0x0000001F
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_Y_DEF                  (0x00000000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_Y_HSH                  (0x452258E0)

  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_X_OFF                  (22)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_X_WID                  ( 2)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_X_MSK                  (0x00C00000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_X_MIN                  (0)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_X_MAX                  (3) // 0x00000003
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_X_DEF                  (0x00000000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT1_TIME_WINDOW_X_HSH                  (0x422C58E0)

  #define DDR_RAPL_LIMIT_PCU_LIMIT2_POWER_OFF                          (32)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_POWER_WID                          (15)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_POWER_MSK                          (0x00007FFF00000000ULL)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_POWER_MIN                          (0)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_POWER_MAX                          (32767) // 0x00007FFF
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_POWER_DEF                          (0x00000000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_POWER_HSH                          (0x4F4058E0)

  #define DDR_RAPL_LIMIT_PCU_LIMIT2_ENABLE_OFF                         (47)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_ENABLE_WID                         ( 1)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_ENABLE_MSK                         (0x0000800000000000ULL)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_ENABLE_MIN                         (0)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_ENABLE_MAX                         (1) // 0x00000001
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_ENABLE_DEF                         (0x00000000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_ENABLE_HSH                         (0x415E58E0)

  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_Y_OFF                  (49)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_Y_WID                  ( 5)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_Y_MSK                  (0x003E000000000000ULL)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_Y_MIN                  (0)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_Y_MAX                  (31) // 0x0000001F
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_Y_DEF                  (0x00000000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_Y_HSH                  (0x456258E0)

  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_X_OFF                  (54)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_X_WID                  ( 2)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_X_MSK                  (0x00C0000000000000ULL)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_X_MIN                  (0)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_X_MAX                  (3) // 0x00000003
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_X_DEF                  (0x00000000)
  #define DDR_RAPL_LIMIT_PCU_LIMIT2_TIME_WINDOW_X_HSH                  (0x426C58E0)

  #define DDR_RAPL_LIMIT_PCU_LOCKED_OFF                                (63)
  #define DDR_RAPL_LIMIT_PCU_LOCKED_WID                                ( 1)
  #define DDR_RAPL_LIMIT_PCU_LOCKED_MSK                                (0x8000000000000000ULL)
  #define DDR_RAPL_LIMIT_PCU_LOCKED_MIN                                (0)
  #define DDR_RAPL_LIMIT_PCU_LOCKED_MAX                                (1) // 0x00000001
  #define DDR_RAPL_LIMIT_PCU_LOCKED_DEF                                (0x00000000)
  #define DDR_RAPL_LIMIT_PCU_LOCKED_HSH                                (0x417E58E0)

#define DDR_ENERGY_STATUS_PCU_REG                                      (0x000058E8)

  #define DDR_ENERGY_STATUS_PCU_JOULES_CONSUMED_OFF                    ( 0)
  #define DDR_ENERGY_STATUS_PCU_JOULES_CONSUMED_WID                    (32)
  #define DDR_ENERGY_STATUS_PCU_JOULES_CONSUMED_MSK                    (0xFFFFFFFF)
  #define DDR_ENERGY_STATUS_PCU_JOULES_CONSUMED_MIN                    (0)
  #define DDR_ENERGY_STATUS_PCU_JOULES_CONSUMED_MAX                    (4294967295) // 0xFFFFFFFF
  #define DDR_ENERGY_STATUS_PCU_JOULES_CONSUMED_DEF                    (0x00000000)
  #define DDR_ENERGY_STATUS_PCU_JOULES_CONSUMED_HSH                    (0x200058E8)

#define DDR_RAPL_PERF_STATUS_PCU_REG                                   (0x000058EC)

  #define DDR_RAPL_PERF_STATUS_PCU_DURATION_OFF                        ( 0)
  #define DDR_RAPL_PERF_STATUS_PCU_DURATION_WID                        (32)
  #define DDR_RAPL_PERF_STATUS_PCU_DURATION_MSK                        (0xFFFFFFFF)
  #define DDR_RAPL_PERF_STATUS_PCU_DURATION_MIN                        (0)
  #define DDR_RAPL_PERF_STATUS_PCU_DURATION_MAX                        (4294967295) // 0xFFFFFFFF
  #define DDR_RAPL_PERF_STATUS_PCU_DURATION_DEF                        (0x00000000)
  #define DDR_RAPL_PERF_STATUS_PCU_DURATION_HSH                        (0x200058EC)

#define PACKAGE_RAPL_PERF_STATUS_PCU_REG                               (0x000058F0)

  #define PACKAGE_RAPL_PERF_STATUS_PCU_COUNTS_OFF                      ( 0)
  #define PACKAGE_RAPL_PERF_STATUS_PCU_COUNTS_WID                      (32)
  #define PACKAGE_RAPL_PERF_STATUS_PCU_COUNTS_MSK                      (0xFFFFFFFF)
  #define PACKAGE_RAPL_PERF_STATUS_PCU_COUNTS_MIN                      (0)
  #define PACKAGE_RAPL_PERF_STATUS_PCU_COUNTS_MAX                      (4294967295) // 0xFFFFFFFF
  #define PACKAGE_RAPL_PERF_STATUS_PCU_COUNTS_DEF                      (0x00000000)
  #define PACKAGE_RAPL_PERF_STATUS_PCU_COUNTS_HSH                      (0x200058F0)

#define PRIP_TURBO_PLCY_PCU_REG                                        (0x00005920)

  #define PRIP_TURBO_PLCY_PCU_PRIPTP_OFF                               ( 0)
  #define PRIP_TURBO_PLCY_PCU_PRIPTP_WID                               ( 5)
  #define PRIP_TURBO_PLCY_PCU_PRIPTP_MSK                               (0x0000001F)
  #define PRIP_TURBO_PLCY_PCU_PRIPTP_MIN                               (0)
  #define PRIP_TURBO_PLCY_PCU_PRIPTP_MAX                               (31) // 0x0000001F
  #define PRIP_TURBO_PLCY_PCU_PRIPTP_DEF                               (0x00000000)
  #define PRIP_TURBO_PLCY_PCU_PRIPTP_HSH                               (0x05005920)

#define SECP_TURBO_PLCY_PCU_REG                                        (0x00005924)

  #define SECP_TURBO_PLCY_PCU_SECPTP_OFF                               ( 0)
  #define SECP_TURBO_PLCY_PCU_SECPTP_WID                               ( 5)
  #define SECP_TURBO_PLCY_PCU_SECPTP_MSK                               (0x0000001F)
  #define SECP_TURBO_PLCY_PCU_SECPTP_MIN                               (0)
  #define SECP_TURBO_PLCY_PCU_SECPTP_MAX                               (31) // 0x0000001F
  #define SECP_TURBO_PLCY_PCU_SECPTP_DEF                               (0x00000010)
  #define SECP_TURBO_PLCY_PCU_SECPTP_HSH                               (0x05005924)

#define PRIP_NRG_STTS_PCU_REG                                          (0x00005928)
//Duplicate of CAMARILLO_MAILBOX_DATA0_PCU_REG

#define SECP_NRG_STTS_PCU_REG                                          (0x0000592C)
//Duplicate of CAMARILLO_MAILBOX_DATA0_PCU_REG

#define PACKAGE_POWER_SKU_PCU_REG                                      (0x00005930)

  #define PACKAGE_POWER_SKU_PCU_PKG_TDP_OFF                            ( 0)
  #define PACKAGE_POWER_SKU_PCU_PKG_TDP_WID                            (15)
  #define PACKAGE_POWER_SKU_PCU_PKG_TDP_MSK                            (0x00007FFF)
  #define PACKAGE_POWER_SKU_PCU_PKG_TDP_MIN                            (0)
  #define PACKAGE_POWER_SKU_PCU_PKG_TDP_MAX                            (32767) // 0x00007FFF
  #define PACKAGE_POWER_SKU_PCU_PKG_TDP_DEF                            (0x00000118)
  #define PACKAGE_POWER_SKU_PCU_PKG_TDP_HSH                            (0x4F005930)

  #define PACKAGE_POWER_SKU_PCU_PKG_MIN_PWR_OFF                        (16)
  #define PACKAGE_POWER_SKU_PCU_PKG_MIN_PWR_WID                        (15)
  #define PACKAGE_POWER_SKU_PCU_PKG_MIN_PWR_MSK                        (0x7FFF0000)
  #define PACKAGE_POWER_SKU_PCU_PKG_MIN_PWR_MIN                        (0)
  #define PACKAGE_POWER_SKU_PCU_PKG_MIN_PWR_MAX                        (32767) // 0x00007FFF
  #define PACKAGE_POWER_SKU_PCU_PKG_MIN_PWR_DEF                        (0x00000060)
  #define PACKAGE_POWER_SKU_PCU_PKG_MIN_PWR_HSH                        (0x4F205930)

  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_PWR_OFF                        (32)
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_PWR_WID                        (15)
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_PWR_MSK                        (0x00007FFF00000000ULL)
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_PWR_MIN                        (0)
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_PWR_MAX                        (32767) // 0x00007FFF
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_PWR_DEF                        (0x00000000)
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_PWR_HSH                        (0x4F405930)

  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_WIN_OFF                        (48)
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_WIN_WID                        ( 7)
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_WIN_MSK                        (0x007F000000000000ULL)
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_WIN_MIN                        (0)
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_WIN_MAX                        (127) // 0x0000007F
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_WIN_DEF                        (0x00000012)
  #define PACKAGE_POWER_SKU_PCU_PKG_MAX_WIN_HSH                        (0x47605930)

#define PACKAGE_POWER_SKU_UNIT_PCU_REG                                 (0x00005938)

  #define PACKAGE_POWER_SKU_UNIT_PCU_PWR_UNIT_OFF                      ( 0)
  #define PACKAGE_POWER_SKU_UNIT_PCU_PWR_UNIT_WID                      ( 4)
  #define PACKAGE_POWER_SKU_UNIT_PCU_PWR_UNIT_MSK                      (0x0000000F)
  #define PACKAGE_POWER_SKU_UNIT_PCU_PWR_UNIT_MIN                      (0)
  #define PACKAGE_POWER_SKU_UNIT_PCU_PWR_UNIT_MAX                      (15) // 0x0000000F
  #define PACKAGE_POWER_SKU_UNIT_PCU_PWR_UNIT_DEF                      (0x00000003)
  #define PACKAGE_POWER_SKU_UNIT_PCU_PWR_UNIT_HSH                      (0x04005938)

  #define PACKAGE_POWER_SKU_UNIT_PCU_ENERGY_UNIT_OFF                   ( 8)
  #define PACKAGE_POWER_SKU_UNIT_PCU_ENERGY_UNIT_WID                   ( 5)
  #define PACKAGE_POWER_SKU_UNIT_PCU_ENERGY_UNIT_MSK                   (0x00001F00)
  #define PACKAGE_POWER_SKU_UNIT_PCU_ENERGY_UNIT_MIN                   (0)
  #define PACKAGE_POWER_SKU_UNIT_PCU_ENERGY_UNIT_MAX                   (31) // 0x0000001F
  #define PACKAGE_POWER_SKU_UNIT_PCU_ENERGY_UNIT_DEF                   (0x0000000E)
  #define PACKAGE_POWER_SKU_UNIT_PCU_ENERGY_UNIT_HSH                   (0x05105938)

  #define PACKAGE_POWER_SKU_UNIT_PCU_TIME_UNIT_OFF                     (16)
  #define PACKAGE_POWER_SKU_UNIT_PCU_TIME_UNIT_WID                     ( 4)
  #define PACKAGE_POWER_SKU_UNIT_PCU_TIME_UNIT_MSK                     (0x000F0000)
  #define PACKAGE_POWER_SKU_UNIT_PCU_TIME_UNIT_MIN                     (0)
  #define PACKAGE_POWER_SKU_UNIT_PCU_TIME_UNIT_MAX                     (15) // 0x0000000F
  #define PACKAGE_POWER_SKU_UNIT_PCU_TIME_UNIT_DEF                     (0x0000000A)
  #define PACKAGE_POWER_SKU_UNIT_PCU_TIME_UNIT_HSH                     (0x04205938)

#define PACKAGE_ENERGY_STATUS_PCU_REG                                  (0x0000593C)
//Duplicate of CAMARILLO_MAILBOX_DATA0_PCU_REG

#define PLATFORM_ID_PCU_REG                                            (0x00005950)

  #define PLATFORM_ID_PCU_PLATFORMID_OFF                               (50)
  #define PLATFORM_ID_PCU_PLATFORMID_WID                               ( 3)
  #define PLATFORM_ID_PCU_PLATFORMID_MSK                               (0x001C000000000000ULL)
  #define PLATFORM_ID_PCU_PLATFORMID_MIN                               (0)
  #define PLATFORM_ID_PCU_PLATFORMID_MAX                               (7) // 0x00000007
  #define PLATFORM_ID_PCU_PLATFORMID_DEF                               (0x00000000)
  #define PLATFORM_ID_PCU_PLATFORMID_HSH                               (0x43645950)

#define PLATFORM_INFO_PCU_REG                                          (0x00005958)

  #define PLATFORM_INFO_PCU_MAX_NON_TURBO_LIM_RATIO_OFF                ( 8)
  #define PLATFORM_INFO_PCU_MAX_NON_TURBO_LIM_RATIO_WID                ( 8)
  #define PLATFORM_INFO_PCU_MAX_NON_TURBO_LIM_RATIO_MSK                (0x0000FF00)
  #define PLATFORM_INFO_PCU_MAX_NON_TURBO_LIM_RATIO_MIN                (0)
  #define PLATFORM_INFO_PCU_MAX_NON_TURBO_LIM_RATIO_MAX                (255) // 0x000000FF
  #define PLATFORM_INFO_PCU_MAX_NON_TURBO_LIM_RATIO_DEF                (0x00000000)
  #define PLATFORM_INFO_PCU_MAX_NON_TURBO_LIM_RATIO_HSH                (0x48105958)

  #define PLATFORM_INFO_PCU_SMM_SAVE_CAP_OFF                           (16)
  #define PLATFORM_INFO_PCU_SMM_SAVE_CAP_WID                           ( 1)
  #define PLATFORM_INFO_PCU_SMM_SAVE_CAP_MSK                           (0x00010000)
  #define PLATFORM_INFO_PCU_SMM_SAVE_CAP_MIN                           (0)
  #define PLATFORM_INFO_PCU_SMM_SAVE_CAP_MAX                           (1) // 0x00000001
  #define PLATFORM_INFO_PCU_SMM_SAVE_CAP_DEF                           (0x00000001)
  #define PLATFORM_INFO_PCU_SMM_SAVE_CAP_HSH                           (0x41205958)

  #define PLATFORM_INFO_PCU_PPIN_CAP_OFF                               (23)
  #define PLATFORM_INFO_PCU_PPIN_CAP_WID                               ( 1)
  #define PLATFORM_INFO_PCU_PPIN_CAP_MSK                               (0x00800000)
  #define PLATFORM_INFO_PCU_PPIN_CAP_MIN                               (0)
  #define PLATFORM_INFO_PCU_PPIN_CAP_MAX                               (1) // 0x00000001
  #define PLATFORM_INFO_PCU_PPIN_CAP_DEF                               (0x00000000)
  #define PLATFORM_INFO_PCU_PPIN_CAP_HSH                               (0x412E5958)

  #define PLATFORM_INFO_PCU_OCVOLT_OVRD_AVAIL_OFF                      (24)
  #define PLATFORM_INFO_PCU_OCVOLT_OVRD_AVAIL_WID                      ( 1)
  #define PLATFORM_INFO_PCU_OCVOLT_OVRD_AVAIL_MSK                      (0x01000000)
  #define PLATFORM_INFO_PCU_OCVOLT_OVRD_AVAIL_MIN                      (0)
  #define PLATFORM_INFO_PCU_OCVOLT_OVRD_AVAIL_MAX                      (1) // 0x00000001
  #define PLATFORM_INFO_PCU_OCVOLT_OVRD_AVAIL_DEF                      (0x00000000)
  #define PLATFORM_INFO_PCU_OCVOLT_OVRD_AVAIL_HSH                      (0x41305958)

  #define PLATFORM_INFO_PCU_DCU_16K_MODE_AVAIL_OFF                     (26)
  #define PLATFORM_INFO_PCU_DCU_16K_MODE_AVAIL_WID                     ( 1)
  #define PLATFORM_INFO_PCU_DCU_16K_MODE_AVAIL_MSK                     (0x04000000)
  #define PLATFORM_INFO_PCU_DCU_16K_MODE_AVAIL_MIN                     (0)
  #define PLATFORM_INFO_PCU_DCU_16K_MODE_AVAIL_MAX                     (1) // 0x00000001
  #define PLATFORM_INFO_PCU_DCU_16K_MODE_AVAIL_DEF                     (0x00000000)
  #define PLATFORM_INFO_PCU_DCU_16K_MODE_AVAIL_HSH                     (0x41345958)

  #define PLATFORM_INFO_PCU_SAMPLE_PART_OFF                            (27)
  #define PLATFORM_INFO_PCU_SAMPLE_PART_WID                            ( 1)
  #define PLATFORM_INFO_PCU_SAMPLE_PART_MSK                            (0x08000000)
  #define PLATFORM_INFO_PCU_SAMPLE_PART_MIN                            (0)
  #define PLATFORM_INFO_PCU_SAMPLE_PART_MAX                            (1) // 0x00000001
  #define PLATFORM_INFO_PCU_SAMPLE_PART_DEF                            (0x00000000)
  #define PLATFORM_INFO_PCU_SAMPLE_PART_HSH                            (0x41365958)

  #define PLATFORM_INFO_PCU_PRG_TURBO_RATIO_EN_OFF                     (28)
  #define PLATFORM_INFO_PCU_PRG_TURBO_RATIO_EN_WID                     ( 1)
  #define PLATFORM_INFO_PCU_PRG_TURBO_RATIO_EN_MSK                     (0x10000000)
  #define PLATFORM_INFO_PCU_PRG_TURBO_RATIO_EN_MIN                     (0)
  #define PLATFORM_INFO_PCU_PRG_TURBO_RATIO_EN_MAX                     (1) // 0x00000001
  #define PLATFORM_INFO_PCU_PRG_TURBO_RATIO_EN_DEF                     (0x00000001)
  #define PLATFORM_INFO_PCU_PRG_TURBO_RATIO_EN_HSH                     (0x41385958)

  #define PLATFORM_INFO_PCU_PRG_TDP_LIM_EN_OFF                         (29)
  #define PLATFORM_INFO_PCU_PRG_TDP_LIM_EN_WID                         ( 1)
  #define PLATFORM_INFO_PCU_PRG_TDP_LIM_EN_MSK                         (0x20000000)
  #define PLATFORM_INFO_PCU_PRG_TDP_LIM_EN_MIN                         (0)
  #define PLATFORM_INFO_PCU_PRG_TDP_LIM_EN_MAX                         (1) // 0x00000001
  #define PLATFORM_INFO_PCU_PRG_TDP_LIM_EN_DEF                         (0x00000001)
  #define PLATFORM_INFO_PCU_PRG_TDP_LIM_EN_HSH                         (0x413A5958)

  #define PLATFORM_INFO_PCU_PRG_TJ_OFFSET_EN_OFF                       (30)
  #define PLATFORM_INFO_PCU_PRG_TJ_OFFSET_EN_WID                       ( 1)
  #define PLATFORM_INFO_PCU_PRG_TJ_OFFSET_EN_MSK                       (0x40000000)
  #define PLATFORM_INFO_PCU_PRG_TJ_OFFSET_EN_MIN                       (0)
  #define PLATFORM_INFO_PCU_PRG_TJ_OFFSET_EN_MAX                       (1) // 0x00000001
  #define PLATFORM_INFO_PCU_PRG_TJ_OFFSET_EN_DEF                       (0x00000001)
  #define PLATFORM_INFO_PCU_PRG_TJ_OFFSET_EN_HSH                       (0x413C5958)

  #define PLATFORM_INFO_PCU_CPUID_FAULTING_EN_OFF                      (31)
  #define PLATFORM_INFO_PCU_CPUID_FAULTING_EN_WID                      ( 1)
  #define PLATFORM_INFO_PCU_CPUID_FAULTING_EN_MSK                      (0x80000000)
  #define PLATFORM_INFO_PCU_CPUID_FAULTING_EN_MIN                      (0)
  #define PLATFORM_INFO_PCU_CPUID_FAULTING_EN_MAX                      (1) // 0x00000001
  #define PLATFORM_INFO_PCU_CPUID_FAULTING_EN_DEF                      (0x00000001)
  #define PLATFORM_INFO_PCU_CPUID_FAULTING_EN_HSH                      (0x413E5958)

  #define PLATFORM_INFO_PCU_LPM_SUPPORT_OFF                            (32)
  #define PLATFORM_INFO_PCU_LPM_SUPPORT_WID                            ( 1)
  #define PLATFORM_INFO_PCU_LPM_SUPPORT_MSK                            (0x0000000100000000ULL)
  #define PLATFORM_INFO_PCU_LPM_SUPPORT_MIN                            (0)
  #define PLATFORM_INFO_PCU_LPM_SUPPORT_MAX                            (1) // 0x00000001
  #define PLATFORM_INFO_PCU_LPM_SUPPORT_DEF                            (0x00000000)
  #define PLATFORM_INFO_PCU_LPM_SUPPORT_HSH                            (0x41405958)

  #define PLATFORM_INFO_PCU_CONFIG_TDP_LEVELS_OFF                      (33)
  #define PLATFORM_INFO_PCU_CONFIG_TDP_LEVELS_WID                      ( 2)
  #define PLATFORM_INFO_PCU_CONFIG_TDP_LEVELS_MSK                      (0x0000000600000000ULL)
  #define PLATFORM_INFO_PCU_CONFIG_TDP_LEVELS_MIN                      (0)
  #define PLATFORM_INFO_PCU_CONFIG_TDP_LEVELS_MAX                      (3) // 0x00000003
  #define PLATFORM_INFO_PCU_CONFIG_TDP_LEVELS_DEF                      (0x00000000)
  #define PLATFORM_INFO_PCU_CONFIG_TDP_LEVELS_HSH                      (0x42425958)

  #define PLATFORM_INFO_PCU_PFAT_ENABLE_OFF                            (35)
  #define PLATFORM_INFO_PCU_PFAT_ENABLE_WID                            ( 1)
  #define PLATFORM_INFO_PCU_PFAT_ENABLE_MSK                            (0x0000000800000000ULL)
  #define PLATFORM_INFO_PCU_PFAT_ENABLE_MIN                            (0)
  #define PLATFORM_INFO_PCU_PFAT_ENABLE_MAX                            (1) // 0x00000001
  #define PLATFORM_INFO_PCU_PFAT_ENABLE_DEF                            (0x00000000)
  #define PLATFORM_INFO_PCU_PFAT_ENABLE_HSH                            (0x41465958)

  #define PLATFORM_INFO_PCU_PEG2DMIDIS_EN_OFF                          (36)
  #define PLATFORM_INFO_PCU_PEG2DMIDIS_EN_WID                          ( 1)
  #define PLATFORM_INFO_PCU_PEG2DMIDIS_EN_MSK                          (0x0000001000000000ULL)
  #define PLATFORM_INFO_PCU_PEG2DMIDIS_EN_MIN                          (0)
  #define PLATFORM_INFO_PCU_PEG2DMIDIS_EN_MAX                          (1) // 0x00000001
  #define PLATFORM_INFO_PCU_PEG2DMIDIS_EN_DEF                          (0x00000001)
  #define PLATFORM_INFO_PCU_PEG2DMIDIS_EN_HSH                          (0x41485958)

  #define PLATFORM_INFO_PCU_TIMED_MWAIT_ENABLE_OFF                     (37)
  #define PLATFORM_INFO_PCU_TIMED_MWAIT_ENABLE_WID                     ( 1)
  #define PLATFORM_INFO_PCU_TIMED_MWAIT_ENABLE_MSK                     (0x0000002000000000ULL)
  #define PLATFORM_INFO_PCU_TIMED_MWAIT_ENABLE_MIN                     (0)
  #define PLATFORM_INFO_PCU_TIMED_MWAIT_ENABLE_MAX                     (1) // 0x00000001
  #define PLATFORM_INFO_PCU_TIMED_MWAIT_ENABLE_DEF                     (0x00000000)
  #define PLATFORM_INFO_PCU_TIMED_MWAIT_ENABLE_HSH                     (0x414A5958)

  #define PLATFORM_INFO_PCU_MAX_EFFICIENCY_RATIO_OFF                   (40)
  #define PLATFORM_INFO_PCU_MAX_EFFICIENCY_RATIO_WID                   ( 8)
  #define PLATFORM_INFO_PCU_MAX_EFFICIENCY_RATIO_MSK                   (0x0000FF0000000000ULL)
  #define PLATFORM_INFO_PCU_MAX_EFFICIENCY_RATIO_MIN                   (0)
  #define PLATFORM_INFO_PCU_MAX_EFFICIENCY_RATIO_MAX                   (255) // 0x000000FF
  #define PLATFORM_INFO_PCU_MAX_EFFICIENCY_RATIO_DEF                   (0x00000000)
  #define PLATFORM_INFO_PCU_MAX_EFFICIENCY_RATIO_HSH                   (0x48505958)

  #define PLATFORM_INFO_PCU_MIN_OPERATING_RATIO_OFF                    (48)
  #define PLATFORM_INFO_PCU_MIN_OPERATING_RATIO_WID                    ( 8)
  #define PLATFORM_INFO_PCU_MIN_OPERATING_RATIO_MSK                    (0x00FF000000000000ULL)
  #define PLATFORM_INFO_PCU_MIN_OPERATING_RATIO_MIN                    (0)
  #define PLATFORM_INFO_PCU_MIN_OPERATING_RATIO_MAX                    (255) // 0x000000FF
  #define PLATFORM_INFO_PCU_MIN_OPERATING_RATIO_DEF                    (0x00000008)
  #define PLATFORM_INFO_PCU_MIN_OPERATING_RATIO_HSH                    (0x48605958)

  #define PLATFORM_INFO_PCU_PushPatch_EN_OFF                           (56)
  #define PLATFORM_INFO_PCU_PushPatch_EN_WID                           ( 1)
  #define PLATFORM_INFO_PCU_PushPatch_EN_MSK                           (0x0100000000000000ULL)
  #define PLATFORM_INFO_PCU_PushPatch_EN_MIN                           (0)
  #define PLATFORM_INFO_PCU_PushPatch_EN_MAX                           (1) // 0x00000001
  #define PLATFORM_INFO_PCU_PushPatch_EN_DEF                           (0x00000000)
  #define PLATFORM_INFO_PCU_PushPatch_EN_HSH                           (0x41705958)

  #define PLATFORM_INFO_PCU_EDRAM_ENABLE_OFF                           (57)
  #define PLATFORM_INFO_PCU_EDRAM_ENABLE_WID                           ( 1)
  #define PLATFORM_INFO_PCU_EDRAM_ENABLE_MSK                           (0x0200000000000000ULL)
  #define PLATFORM_INFO_PCU_EDRAM_ENABLE_MIN                           (0)
  #define PLATFORM_INFO_PCU_EDRAM_ENABLE_MAX                           (1) // 0x00000001
  #define PLATFORM_INFO_PCU_EDRAM_ENABLE_DEF                           (0x00000000)
  #define PLATFORM_INFO_PCU_EDRAM_ENABLE_HSH                           (0x41725958)

  #define PLATFORM_INFO_PCU_SXP_2LM_EN_OFF                             (58)
  #define PLATFORM_INFO_PCU_SXP_2LM_EN_WID                             ( 1)
  #define PLATFORM_INFO_PCU_SXP_2LM_EN_MSK                             (0x0400000000000000ULL)
  #define PLATFORM_INFO_PCU_SXP_2LM_EN_MIN                             (0)
  #define PLATFORM_INFO_PCU_SXP_2LM_EN_MAX                             (1) // 0x00000001
  #define PLATFORM_INFO_PCU_SXP_2LM_EN_DEF                             (0x00000000)
  #define PLATFORM_INFO_PCU_SXP_2LM_EN_HSH                             (0x41745958)

  #define PLATFORM_INFO_PCU_SMM_SUPOVR_STATE_LOCK_ENABLE_OFF           (59)
  #define PLATFORM_INFO_PCU_SMM_SUPOVR_STATE_LOCK_ENABLE_WID           ( 1)
  #define PLATFORM_INFO_PCU_SMM_SUPOVR_STATE_LOCK_ENABLE_MSK           (0x0800000000000000ULL)
  #define PLATFORM_INFO_PCU_SMM_SUPOVR_STATE_LOCK_ENABLE_MIN           (0)
  #define PLATFORM_INFO_PCU_SMM_SUPOVR_STATE_LOCK_ENABLE_MAX           (1) // 0x00000001
  #define PLATFORM_INFO_PCU_SMM_SUPOVR_STATE_LOCK_ENABLE_DEF           (0x00000000)
  #define PLATFORM_INFO_PCU_SMM_SUPOVR_STATE_LOCK_ENABLE_HSH           (0x41765958)

  #define PLATFORM_INFO_PCU_TIO_ENABLE_OFF                             (60)
  #define PLATFORM_INFO_PCU_TIO_ENABLE_WID                             ( 1)
  #define PLATFORM_INFO_PCU_TIO_ENABLE_MSK                             (0x1000000000000000ULL)
  #define PLATFORM_INFO_PCU_TIO_ENABLE_MIN                             (0)
  #define PLATFORM_INFO_PCU_TIO_ENABLE_MAX                             (1) // 0x00000001
  #define PLATFORM_INFO_PCU_TIO_ENABLE_DEF                             (0x00000000)
  #define PLATFORM_INFO_PCU_TIO_ENABLE_HSH                             (0x41785958)

#define PACKAGE_TEMPERATURE_PCU_REG                                    (0x00005978)

  #define PACKAGE_TEMPERATURE_PCU_DATA_OFF                             ( 0)
  #define PACKAGE_TEMPERATURE_PCU_DATA_WID                             ( 8)
  #define PACKAGE_TEMPERATURE_PCU_DATA_MSK                             (0x000000FF)
  #define PACKAGE_TEMPERATURE_PCU_DATA_MIN                             (0)
  #define PACKAGE_TEMPERATURE_PCU_DATA_MAX                             (255) // 0x000000FF
  #define PACKAGE_TEMPERATURE_PCU_DATA_DEF                             (0x00000000)
  #define PACKAGE_TEMPERATURE_PCU_DATA_HSH                             (0x08005978)

#define PP0_TEMPERATURE_PCU_REG                                        (0x0000597C)
//Duplicate of PACKAGE_TEMPERATURE_PCU_REG

#define PP1_TEMPERATURE_PCU_REG                                        (0x00005980)
//Duplicate of PACKAGE_TEMPERATURE_PCU_REG

#define DEVICE_SHARED_IDLE_DURATION_PCU_REG                            (0x00005988)

  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_OFF                 ( 0)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_WID                 ( 1)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_MSK                 (0x00000001)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_MIN                 (0)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_MAX                 (1) // 0x00000001
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_DEF                 (0x00000000)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_HSH                 (0x01005988)

  #define DEVICE_SHARED_IDLE_DURATION_PCU_VALID_OFF                    ( 1)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_VALID_WID                    ( 1)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_VALID_MSK                    (0x00000002)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_VALID_MIN                    (0)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_VALID_MAX                    (1) // 0x00000001
  #define DEVICE_SHARED_IDLE_DURATION_PCU_VALID_DEF                    (0x00000000)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_VALID_HSH                    (0x01025988)

  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_BITS_OFF            ( 2)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_BITS_WID            ( 4)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_BITS_MSK            (0x0000003C)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_BITS_MIN            (0)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_BITS_MAX            (15) // 0x0000000F
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_BITS_DEF            (0x00000000)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_RESERVED_BITS_HSH            (0x04045988)

  #define DEVICE_SHARED_IDLE_DURATION_PCU_OD_OFF                       ( 6)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_OD_WID                       ( 1)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_OD_MSK                       (0x00000040)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_OD_MIN                       (0)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_OD_MAX                       (1) // 0x00000001
  #define DEVICE_SHARED_IDLE_DURATION_PCU_OD_DEF                       (0x00000000)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_OD_HSH                       (0x010C5988)

  #define DEVICE_SHARED_IDLE_DURATION_PCU_IM_OFF                       ( 7)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_IM_WID                       ( 1)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_IM_MSK                       (0x00000080)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_IM_MIN                       (0)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_IM_MAX                       (1) // 0x00000001
  #define DEVICE_SHARED_IDLE_DURATION_PCU_IM_DEF                       (0x00000000)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_IM_HSH                       (0x010E5988)

  #define DEVICE_SHARED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_OFF     ( 8)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_WID     (21)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_MSK     (0x1FFFFF00)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_MIN     (0)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_MAX     (2097151) // 0x001FFFFF
  #define DEVICE_SHARED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_DEF     (0x00000000)
  #define DEVICE_SHARED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_HSH     (0x15105988)

#define DEVICE_DEDICATED_IDLE_DURATION_PCU_REG                         (0x0000598C)

  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_USED_OFF                  ( 0)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_USED_WID                  ( 1)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_USED_MSK                  (0x00000001)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_USED_MIN                  (0)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_USED_MAX                  (1) // 0x00000001
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_USED_DEF                  (0x00000000)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_USED_HSH                  (0x0100598C)

  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_VALID_OFF                 ( 1)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_VALID_WID                 ( 1)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_VALID_MSK                 (0x00000002)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_VALID_MIN                 (0)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_VALID_MAX                 (1) // 0x00000001
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_VALID_DEF                 (0x00000000)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_VALID_HSH                 (0x0102598C)

  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_RESERVED_BITS_OFF         ( 2)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_RESERVED_BITS_WID         ( 4)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_RESERVED_BITS_MSK         (0x0000003C)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_RESERVED_BITS_MIN         (0)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_RESERVED_BITS_MAX         (15) // 0x0000000F
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_RESERVED_BITS_DEF         (0x00000000)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_RESERVED_BITS_HSH         (0x0404598C)

  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_OD_OFF                    ( 6)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_OD_WID                    ( 1)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_OD_MSK                    (0x00000040)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_OD_MIN                    (0)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_OD_MAX                    (1) // 0x00000001
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_OD_DEF                    (0x00000000)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_OD_HSH                    (0x010C598C)

  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_IM_OFF                    ( 7)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_IM_WID                    ( 1)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_IM_MSK                    (0x00000080)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_IM_MIN                    (0)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_IM_MAX                    (1) // 0x00000001
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_IM_DEF                    (0x00000000)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_IM_HSH                    (0x010E598C)

  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_OFF  ( 8)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_WID  (21)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_MSK  (0x1FFFFF00)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_MIN  (0)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_MAX  (2097151) // 0x001FFFFF
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_DEF  (0x00000000)
  #define DEVICE_DEDICATED_IDLE_DURATION_PCU_NEXT_DEVICE_ACTIVITY_HSH  (0x1510598C)

#define RP_STATE_LIMITS_PCU_REG                                        (0x00005994)

  #define RP_STATE_LIMITS_PCU_RPSTT_LIM_OFF                            ( 0)
  #define RP_STATE_LIMITS_PCU_RPSTT_LIM_WID                            ( 8)
  #define RP_STATE_LIMITS_PCU_RPSTT_LIM_MSK                            (0x000000FF)
  #define RP_STATE_LIMITS_PCU_RPSTT_LIM_MIN                            (0)
  #define RP_STATE_LIMITS_PCU_RPSTT_LIM_MAX                            (255) // 0x000000FF
  #define RP_STATE_LIMITS_PCU_RPSTT_LIM_DEF                            (0x000000FF)
  #define RP_STATE_LIMITS_PCU_RPSTT_LIM_HSH                            (0x08005994)

#define PACKAGE_RAPL_LIMIT_PCU_REG                                     (0x000059A0)

  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_OFF                     ( 0)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_WID                     (15)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_MSK                     (0x00007FFF)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_MIN                     (0)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_MAX                     (32767) // 0x00007FFF
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_DEF                     (0x00000000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_HSH                     (0x4F0059A0)

  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_EN_OFF                  (15)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_EN_WID                  ( 1)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_EN_MSK                  (0x00008000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_EN_MIN                  (0)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_EN_MAX                  (1) // 0x00000001
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_EN_DEF                  (0x00000000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_EN_HSH                  (0x411E59A0)

  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_1_OFF                    (16)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_1_WID                    ( 1)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_1_MSK                    (0x00010000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_1_MIN                    (0)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_1_MAX                    (1) // 0x00000001
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_1_DEF                    (0x00000000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_1_HSH                    (0x412059A0)

  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_TIME_OFF                (17)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_TIME_WID                ( 7)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_TIME_MSK                (0x00FE0000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_TIME_MIN                (0)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_TIME_MAX                (127) // 0x0000007F
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_TIME_DEF                (0x00000000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_1_TIME_HSH                (0x472259A0)

  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_OFF                     (32)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_WID                     (15)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_MSK                     (0x00007FFF00000000ULL)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_MIN                     (0)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_MAX                     (32767) // 0x00007FFF
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_DEF                     (0x00000000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_HSH                     (0x4F4059A0)

  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_EN_OFF                  (47)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_EN_WID                  ( 1)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_EN_MSK                  (0x0000800000000000ULL)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_EN_MIN                  (0)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_EN_MAX                  (1) // 0x00000001
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_EN_DEF                  (0x00000000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_EN_HSH                  (0x415E59A0)

  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_2_OFF                    (48)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_2_WID                    ( 1)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_2_MSK                    (0x0001000000000000ULL)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_2_MIN                    (0)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_2_MAX                    (1) // 0x00000001
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_2_DEF                    (0x00000000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_CLMP_LIM_2_HSH                    (0x416059A0)

  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_TIME_OFF                (49)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_TIME_WID                ( 7)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_TIME_MSK                (0x00FE000000000000ULL)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_TIME_MIN                (0)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_TIME_MAX                (127) // 0x0000007F
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_TIME_DEF                (0x00000000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_2_TIME_HSH                (0x476259A0)

  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_LOCK_OFF                  (63)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_LOCK_WID                  ( 1)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_LOCK_MSK                  (0x8000000000000000ULL)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_LOCK_MIN                  (0)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_LOCK_MAX                  (1) // 0x00000001
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_LOCK_DEF                  (0x00000000)
  #define PACKAGE_RAPL_LIMIT_PCU_PKG_PWR_LIM_LOCK_HSH                  (0x417E59A0)

#define PRIP_TURBO_PWR_LIM_PCU_REG                                     (0x000059A8)

  #define PRIP_TURBO_PWR_LIM_PCU_IA_PP_PWR_LIM_OFF                     ( 0)
  #define PRIP_TURBO_PWR_LIM_PCU_IA_PP_PWR_LIM_WID                     (15)
  #define PRIP_TURBO_PWR_LIM_PCU_IA_PP_PWR_LIM_MSK                     (0x00007FFF)
  #define PRIP_TURBO_PWR_LIM_PCU_IA_PP_PWR_LIM_MIN                     (0)
  #define PRIP_TURBO_PWR_LIM_PCU_IA_PP_PWR_LIM_MAX                     (32767) // 0x00007FFF
  #define PRIP_TURBO_PWR_LIM_PCU_IA_PP_PWR_LIM_DEF                     (0x00000000)
  #define PRIP_TURBO_PWR_LIM_PCU_IA_PP_PWR_LIM_HSH                     (0x0F0059A8)

  #define PRIP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_OFF                   (15)
  #define PRIP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_WID                   ( 1)
  #define PRIP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_MSK                   (0x00008000)
  #define PRIP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_MIN                   (0)
  #define PRIP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_MAX                   (1) // 0x00000001
  #define PRIP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_DEF                   (0x00000000)
  #define PRIP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_HSH                   (0x011E59A8)

  #define PRIP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_OFF                      (16)
  #define PRIP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_WID                      ( 1)
  #define PRIP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_MSK                      (0x00010000)
  #define PRIP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_MIN                      (0)
  #define PRIP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_MAX                      (1) // 0x00000001
  #define PRIP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_DEF                      (0x00000000)
  #define PRIP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_HSH                      (0x012059A8)

  #define PRIP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_OFF                     (17)
  #define PRIP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_WID                     ( 7)
  #define PRIP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_MSK                     (0x00FE0000)
  #define PRIP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_MIN                     (0)
  #define PRIP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_MAX                     (127) // 0x0000007F
  #define PRIP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_DEF                     (0x00000000)
  #define PRIP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_HSH                     (0x072259A8)

  #define PRIP_TURBO_PWR_LIM_PCU_PP_PWR_LIM_LOCK_OFF                   (31)
  #define PRIP_TURBO_PWR_LIM_PCU_PP_PWR_LIM_LOCK_WID                   ( 1)
  #define PRIP_TURBO_PWR_LIM_PCU_PP_PWR_LIM_LOCK_MSK                   (0x80000000)
  #define PRIP_TURBO_PWR_LIM_PCU_PP_PWR_LIM_LOCK_MIN                   (0)
  #define PRIP_TURBO_PWR_LIM_PCU_PP_PWR_LIM_LOCK_MAX                   (1) // 0x00000001
  #define PRIP_TURBO_PWR_LIM_PCU_PP_PWR_LIM_LOCK_DEF                   (0x00000000)
  #define PRIP_TURBO_PWR_LIM_PCU_PP_PWR_LIM_LOCK_HSH                   (0x013E59A8)

#define SECP_TURBO_PWR_LIM_PCU_REG                                     (0x000059AC)

  #define SECP_TURBO_PWR_LIM_PCU_NON_IA_PP_PWR_LIM_OFF                 ( 0)
  #define SECP_TURBO_PWR_LIM_PCU_NON_IA_PP_PWR_LIM_WID                 (15)
  #define SECP_TURBO_PWR_LIM_PCU_NON_IA_PP_PWR_LIM_MSK                 (0x00007FFF)
  #define SECP_TURBO_PWR_LIM_PCU_NON_IA_PP_PWR_LIM_MIN                 (0)
  #define SECP_TURBO_PWR_LIM_PCU_NON_IA_PP_PWR_LIM_MAX                 (32767) // 0x00007FFF
  #define SECP_TURBO_PWR_LIM_PCU_NON_IA_PP_PWR_LIM_DEF                 (0x00000000)
  #define SECP_TURBO_PWR_LIM_PCU_NON_IA_PP_PWR_LIM_HSH                 (0x0F0059AC)

  #define SECP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_OFF                   (15)
  #define SECP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_WID                   ( 1)
  #define SECP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_MSK                   (0x00008000)
  #define SECP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_MIN                   (0)
  #define SECP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_MAX                   (1) // 0x00000001
  #define SECP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_DEF                   (0x00000000)
  #define SECP_TURBO_PWR_LIM_PCU_PWR_LIM_CTRL_EN_HSH                   (0x011E59AC)

  #define SECP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_OFF                      (16)
  #define SECP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_WID                      ( 1)
  #define SECP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_MSK                      (0x00010000)
  #define SECP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_MIN                      (0)
  #define SECP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_MAX                      (1) // 0x00000001
  #define SECP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_DEF                      (0x00000000)
  #define SECP_TURBO_PWR_LIM_PCU_PP_CLAMP_LIM_HSH                      (0x012059AC)

  #define SECP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_OFF                     (17)
  #define SECP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_WID                     ( 7)
  #define SECP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_MSK                     (0x00FE0000)
  #define SECP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_MIN                     (0)
  #define SECP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_MAX                     (127) // 0x0000007F
  #define SECP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_DEF                     (0x00000000)
  #define SECP_TURBO_PWR_LIM_PCU_CTRL_TIME_WIN_HSH                     (0x072259AC)

  #define SECP_TURBO_PWR_LIM_PCU_SP_PWR_LIM_LOCK_OFF                   (31)
  #define SECP_TURBO_PWR_LIM_PCU_SP_PWR_LIM_LOCK_WID                   ( 1)
  #define SECP_TURBO_PWR_LIM_PCU_SP_PWR_LIM_LOCK_MSK                   (0x80000000)
  #define SECP_TURBO_PWR_LIM_PCU_SP_PWR_LIM_LOCK_MIN                   (0)
  #define SECP_TURBO_PWR_LIM_PCU_SP_PWR_LIM_LOCK_MAX                   (1) // 0x00000001
  #define SECP_TURBO_PWR_LIM_PCU_SP_PWR_LIM_LOCK_DEF                   (0x00000000)
  #define SECP_TURBO_PWR_LIM_PCU_SP_PWR_LIM_LOCK_HSH                   (0x013E59AC)

#define MRC_ODT_POWER_SAVING_PCU_REG                                   (0x000059B8)

  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Rd_OFF                   ( 0)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Rd_WID                   ( 8)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Rd_MSK                   (0x000000FF)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Rd_MIN                   (0)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Rd_MAX                   (255) // 0x000000FF
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Rd_DEF                   (0x00000000)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Rd_HSH                   (0x080059B8)

  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Wt_OFF                   ( 8)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Wt_WID                   ( 8)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Wt_MSK                   (0x0000FF00)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Wt_MIN                   (0)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Wt_MAX                   (255) // 0x000000FF
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Wt_DEF                   (0x00000000)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Wt_HSH                   (0x081059B8)

  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Cmd_OFF                  (16)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Cmd_WID                  ( 8)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Cmd_MSK                  (0x00FF0000)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Cmd_MIN                  (0)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Cmd_MAX                  (255) // 0x000000FF
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Cmd_DEF                  (0x00000000)
  #define MRC_ODT_POWER_SAVING_PCU_MRC_Saving_Cmd_HSH                  (0x082059B8)

  #define MRC_ODT_POWER_SAVING_PCU_RESERVED_OFF                        (24)
  #define MRC_ODT_POWER_SAVING_PCU_RESERVED_WID                        ( 8)
  #define MRC_ODT_POWER_SAVING_PCU_RESERVED_MSK                        (0xFF000000)
  #define MRC_ODT_POWER_SAVING_PCU_RESERVED_MIN                        (0)
  #define MRC_ODT_POWER_SAVING_PCU_RESERVED_MAX                        (255) // 0x000000FF
  #define MRC_ODT_POWER_SAVING_PCU_RESERVED_DEF                        (0x00000000)
  #define MRC_ODT_POWER_SAVING_PCU_RESERVED_HSH                        (0x083059B8)

#define DEVICE_IDLE_DURATION_OVERRIDE_PCU_REG                          (0x000059C8)

  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_OFF               ( 0)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_WID               ( 1)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_MSK               (0x00000001)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_MIN               (0)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_MAX               (1) // 0x00000001
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_DEF               (0x00000000)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_HSH               (0x010059C8)

  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_VALID_OFF                  ( 1)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_VALID_WID                  ( 1)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_VALID_MSK                  (0x00000002)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_VALID_MIN                  (0)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_VALID_MAX                  (1) // 0x00000001
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_VALID_DEF                  (0x00000000)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_VALID_HSH                  (0x010259C8)

  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_BITS_OFF          ( 2)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_BITS_WID          ( 4)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_BITS_MSK          (0x0000003C)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_BITS_MIN          (0)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_BITS_MAX          (15) // 0x0000000F
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_BITS_DEF          (0x00000000)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_RESERVED_BITS_HSH          (0x040459C8)

  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_OD_OFF                     ( 6)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_OD_WID                     ( 1)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_OD_MSK                     (0x00000040)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_OD_MIN                     (0)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_OD_MAX                     (1) // 0x00000001
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_OD_DEF                     (0x00000000)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_OD_HSH                     (0x010C59C8)

  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_IM_OFF                     ( 7)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_IM_WID                     ( 1)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_IM_MSK                     (0x00000080)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_IM_MIN                     (0)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_IM_MAX                     (1) // 0x00000001
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_IM_DEF                     (0x00000000)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_IM_HSH                     (0x010E59C8)

  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_NEXT_DEVICE_ACTIVITY_OFF   ( 8)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_NEXT_DEVICE_ACTIVITY_WID   (21)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_NEXT_DEVICE_ACTIVITY_MSK   (0x1FFFFF00)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_NEXT_DEVICE_ACTIVITY_MIN   (0)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_NEXT_DEVICE_ACTIVITY_MAX   (2097151) // 0x001FFFFF
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_NEXT_DEVICE_ACTIVITY_DEF   (0x00000000)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_NEXT_DEVICE_ACTIVITY_HSH   (0x151059C8)

  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_DISABLE_MDID_EVALUATION_OFF (29)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_DISABLE_MDID_EVALUATION_WID ( 1)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_DISABLE_MDID_EVALUATION_MSK (0x20000000)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_DISABLE_MDID_EVALUATION_MIN (0)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_DISABLE_MDID_EVALUATION_MAX (1) // 0x00000001
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_DISABLE_MDID_EVALUATION_DEF (0x00000000)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_DISABLE_MDID_EVALUATION_HSH (0x013A59C8)

  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_FORCE_MDID_OVERRIDE_OFF    (30)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_FORCE_MDID_OVERRIDE_WID    ( 1)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_FORCE_MDID_OVERRIDE_MSK    (0x40000000)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_FORCE_MDID_OVERRIDE_MIN    (0)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_FORCE_MDID_OVERRIDE_MAX    (1) // 0x00000001
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_FORCE_MDID_OVERRIDE_DEF    (0x00000000)
  #define DEVICE_IDLE_DURATION_OVERRIDE_PCU_FORCE_MDID_OVERRIDE_HSH    (0x013C59C8)

#define CHAP_CONFIG_PCU_REG                                            (0x00005A00)

  #define CHAP_CONFIG_PCU_PECI_CMD_OFF                                 ( 4)
  #define CHAP_CONFIG_PCU_PECI_CMD_WID                                 ( 8)
  #define CHAP_CONFIG_PCU_PECI_CMD_MSK                                 (0x00000FF0)
  #define CHAP_CONFIG_PCU_PECI_CMD_MIN                                 (0)
  #define CHAP_CONFIG_PCU_PECI_CMD_MAX                                 (255) // 0x000000FF
  #define CHAP_CONFIG_PCU_PECI_CMD_DEF                                 (0x00000000)
  #define CHAP_CONFIG_PCU_PECI_CMD_HSH                                 (0x08085A00)

#define FFFC_EMI_CONTROL_PCU_REG                                       (0x00005A08)
//Duplicate of CAMARILLO_MAILBOX_DATA0_PCU_REG

#define FFFC_RFI_CONTROL_PCU_REG                                       (0x00005A0C)
//Duplicate of CAMARILLO_MAILBOX_DATA0_PCU_REG

#define FFFC_RFI_STATUS_PCU_REG                                        (0x00005A10)
//Duplicate of CAMARILLO_MAILBOX_DATA0_PCU_REG

#define FFFC_FAULT_STATUS_PCU_REG                                      (0x00005A14)
//Duplicate of CAMARILLO_MAILBOX_DATA0_PCU_REG

#define FFFC_RFI_CONTROL2_PCU_REG                                      (0x00005A18)
//Duplicate of CAMARILLO_MAILBOX_DATA0_PCU_REG

#define DDR_DVFS_DATA_RATE_PCU_REG                                     (0x00005A30)

  #define DDR_DVFS_DATA_RATE_PCU_POINT_0_OFF                           ( 0)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_0_WID                           (10)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_0_MSK                           (0x000003FF)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_0_MIN                           (0)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_0_MAX                           (1023) // 0x000003FF
  #define DDR_DVFS_DATA_RATE_PCU_POINT_0_DEF                           (0x00000000)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_0_HSH                           (0x4A005A30)

  #define DDR_DVFS_DATA_RATE_PCU_POINT_1_OFF                           (10)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_1_WID                           (10)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_1_MSK                           (0x000FFC00)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_1_MIN                           (0)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_1_MAX                           (1023) // 0x000003FF
  #define DDR_DVFS_DATA_RATE_PCU_POINT_1_DEF                           (0x00000000)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_1_HSH                           (0x4A145A30)

  #define DDR_DVFS_DATA_RATE_PCU_POINT_2_OFF                           (20)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_2_WID                           (10)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_2_MSK                           (0x3FF00000)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_2_MIN                           (0)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_2_MAX                           (1023) // 0x000003FF
  #define DDR_DVFS_DATA_RATE_PCU_POINT_2_DEF                           (0x00000000)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_2_HSH                           (0x4A285A30)

  #define DDR_DVFS_DATA_RATE_PCU_POINT_3_OFF                           (30)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_3_WID                           (10)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_3_MSK                           (0x000000FFC0000000ULL)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_3_MIN                           (0)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_3_MAX                           (1023) // 0x000003FF
  #define DDR_DVFS_DATA_RATE_PCU_POINT_3_DEF                           (0x00000000)
  #define DDR_DVFS_DATA_RATE_PCU_POINT_3_HSH                           (0x4A3C5A30)

  #define DDR_DVFS_DATA_RATE_PCU_RESERVED_OFF                          (40)
  #define DDR_DVFS_DATA_RATE_PCU_RESERVED_WID                          (24)
  #define DDR_DVFS_DATA_RATE_PCU_RESERVED_MSK                          (0xFFFFFF0000000000ULL)
  #define DDR_DVFS_DATA_RATE_PCU_RESERVED_MIN                          (0)
  #define DDR_DVFS_DATA_RATE_PCU_RESERVED_MAX                          (16777215) // 0x00FFFFFF
  #define DDR_DVFS_DATA_RATE_PCU_RESERVED_DEF                          (0x00000000)
  #define DDR_DVFS_DATA_RATE_PCU_RESERVED_HSH                          (0x58505A30)

#define DDR_DVFS_RFI_RESTRICTION_PCU_REG                               (0x00005A38)

  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_BASE_OFF              ( 0)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_BASE_WID              (16)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_BASE_MSK              (0x0000FFFF)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_BASE_MIN              (0)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_BASE_MAX              (65535) // 0x0000FFFF
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_BASE_DEF              (0x00000000)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_BASE_HSH              (0x10005A38)

  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_DELTA_ABOVE_BASE_OFF  (16)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_DELTA_ABOVE_BASE_WID  ( 8)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_DELTA_ABOVE_BASE_MSK  (0x00FF0000)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_DELTA_ABOVE_BASE_MIN  (0)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_DELTA_ABOVE_BASE_MAX  (255) // 0x000000FF
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_DELTA_ABOVE_BASE_DEF  (0x00000000)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_DATA_RATE_DELTA_ABOVE_BASE_HSH  (0x08205A38)

  #define DDR_DVFS_RFI_RESTRICTION_PCU_ERR_CODE_OFF                    (24)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_ERR_CODE_WID                    ( 7)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_ERR_CODE_MSK                    (0x7F000000)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_ERR_CODE_MIN                    (0)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_ERR_CODE_MAX                    (127) // 0x0000007F
  #define DDR_DVFS_RFI_RESTRICTION_PCU_ERR_CODE_DEF                    (0x00000000)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_ERR_CODE_HSH                    (0x07305A38)

  #define DDR_DVFS_RFI_RESTRICTION_PCU_RUN_BUSY_OFF                    (31)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_RUN_BUSY_WID                    ( 1)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_RUN_BUSY_MSK                    (0x80000000)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_RUN_BUSY_MIN                    (0)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_RUN_BUSY_MAX                    (1) // 0x00000001
  #define DDR_DVFS_RFI_RESTRICTION_PCU_RUN_BUSY_DEF                    (0x00000000)
  #define DDR_DVFS_RFI_RESTRICTION_PCU_RUN_BUSY_HSH                    (0x013E5A38)

#define DDR_DVFS_RFI_CONFIG_PCU_REG                                    (0x00005A40)

  #define DDR_DVFS_RFI_CONFIG_PCU_RFI_DISABLE_OFF                      ( 0)
  #define DDR_DVFS_RFI_CONFIG_PCU_RFI_DISABLE_WID                      ( 1)
  #define DDR_DVFS_RFI_CONFIG_PCU_RFI_DISABLE_MSK                      (0x00000001)
  #define DDR_DVFS_RFI_CONFIG_PCU_RFI_DISABLE_MIN                      (0)
  #define DDR_DVFS_RFI_CONFIG_PCU_RFI_DISABLE_MAX                      (1) // 0x00000001
  #define DDR_DVFS_RFI_CONFIG_PCU_RFI_DISABLE_DEF                      (0x00000000)
  #define DDR_DVFS_RFI_CONFIG_PCU_RFI_DISABLE_HSH                      (0x01005A40)

  #define DDR_DVFS_RFI_CONFIG_PCU_RESERVED_OFF                         ( 1)
  #define DDR_DVFS_RFI_CONFIG_PCU_RESERVED_WID                         (31)
  #define DDR_DVFS_RFI_CONFIG_PCU_RESERVED_MSK                         (0xFFFFFFFE)
  #define DDR_DVFS_RFI_CONFIG_PCU_RESERVED_MIN                         (0)
  #define DDR_DVFS_RFI_CONFIG_PCU_RESERVED_MAX                         (2147483647) // 0x7FFFFFFF
  #define DDR_DVFS_RFI_CONFIG_PCU_RESERVED_DEF                         (0x00000000)
  #define DDR_DVFS_RFI_CONFIG_PCU_RESERVED_HSH                         (0x1F025A40)

#define ENERGY_DEBUG_PCU_REG                                           (0x00005B04)

  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP0_VALUE_OFF                  ( 0)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP0_VALUE_WID                  (10)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP0_VALUE_MSK                  (0x000003FF)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP0_VALUE_MIN                  (0)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP0_VALUE_MAX                  (1023) // 0x000003FF
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP0_VALUE_DEF                  (0x00000000)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP0_VALUE_HSH                  (0x0A005B04)

  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP1_VALUE_OFF                  (10)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP1_VALUE_WID                  (10)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP1_VALUE_MSK                  (0x000FFC00)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP1_VALUE_MIN                  (0)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP1_VALUE_MAX                  (1023) // 0x000003FF
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP1_VALUE_DEF                  (0x00000000)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_PP1_VALUE_HSH                  (0x0A145B04)

  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_SA_VALUE_OFF                   (20)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_SA_VALUE_WID                   (10)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_SA_VALUE_MSK                   (0x3FF00000)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_SA_VALUE_MIN                   (0)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_SA_VALUE_MAX                   (1023) // 0x000003FF
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_SA_VALUE_DEF                   (0x00000000)
  #define ENERGY_DEBUG_PCU_DEBUG_ENERGY_SA_VALUE_HSH                   (0x0A285B04)

#define SSKPD_PCU_REG                                                  (0x00005D10)

  #define SSKPD_PCU_SKPD_OFF                                           ( 0)
  #define SSKPD_PCU_SKPD_WID                                           (64)
  #define SSKPD_PCU_SKPD_MSK                                           (0xFFFFFFFFFFFFFFFFULL)
  #define SSKPD_PCU_SKPD_MIN                                           (0)
  #define SSKPD_PCU_SKPD_MAX                                           (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define SSKPD_PCU_SKPD_DEF                                           (0x00000000)
  #define SSKPD_PCU_SKPD_HSH                                           (0x40005D10)

#define C2C3TT_PCU_REG                                                 (0x00005D20)

  #define C2C3TT_PCU_PPDN_INIT_OFF                                     ( 0)
  #define C2C3TT_PCU_PPDN_INIT_WID                                     (12)
  #define C2C3TT_PCU_PPDN_INIT_MSK                                     (0x00000FFF)
  #define C2C3TT_PCU_PPDN_INIT_MIN                                     (0)
  #define C2C3TT_PCU_PPDN_INIT_MAX                                     (4095) // 0x00000FFF
  #define C2C3TT_PCU_PPDN_INIT_DEF                                     (0x00000005)
  #define C2C3TT_PCU_PPDN_INIT_HSH                                     (0x0C005D20)

#define C2_DDR_TT_PCU_REG                                              (0x00005D24)

  #define C2_DDR_TT_PCU_DDR_TIMER_VALUE_OFF                            ( 0)
  #define C2_DDR_TT_PCU_DDR_TIMER_VALUE_WID                            (13)
  #define C2_DDR_TT_PCU_DDR_TIMER_VALUE_MSK                            (0x00001FFF)
  #define C2_DDR_TT_PCU_DDR_TIMER_VALUE_MIN                            (0)
  #define C2_DDR_TT_PCU_DDR_TIMER_VALUE_MAX                            (8191) // 0x00001FFF
  #define C2_DDR_TT_PCU_DDR_TIMER_VALUE_DEF                            (0x000001F4)
  #define C2_DDR_TT_PCU_DDR_TIMER_VALUE_HSH                            (0x0D005D24)

#define PCIE_ILTR_OVRD_PCU_REG                                         (0x00005D30)

  #define PCIE_ILTR_OVRD_PCU_NSTL_OFF                                  ( 0)
  #define PCIE_ILTR_OVRD_PCU_NSTL_WID                                  (10)
  #define PCIE_ILTR_OVRD_PCU_NSTL_MSK                                  (0x000003FF)
  #define PCIE_ILTR_OVRD_PCU_NSTL_MIN                                  (0)
  #define PCIE_ILTR_OVRD_PCU_NSTL_MAX                                  (1023) // 0x000003FF
  #define PCIE_ILTR_OVRD_PCU_NSTL_DEF                                  (0x00000000)
  #define PCIE_ILTR_OVRD_PCU_NSTL_HSH                                  (0x0A005D30)

  #define PCIE_ILTR_OVRD_PCU_MULTIPLIER_OFF                            (10)
  #define PCIE_ILTR_OVRD_PCU_MULTIPLIER_WID                            ( 3)
  #define PCIE_ILTR_OVRD_PCU_MULTIPLIER_MSK                            (0x00001C00)
  #define PCIE_ILTR_OVRD_PCU_MULTIPLIER_MIN                            (0)
  #define PCIE_ILTR_OVRD_PCU_MULTIPLIER_MAX                            (7) // 0x00000007
  #define PCIE_ILTR_OVRD_PCU_MULTIPLIER_DEF                            (0x00000000)
  #define PCIE_ILTR_OVRD_PCU_MULTIPLIER_HSH                            (0x03145D30)

  #define PCIE_ILTR_OVRD_PCU_FORCE_NL_OFF                              (14)
  #define PCIE_ILTR_OVRD_PCU_FORCE_NL_WID                              ( 1)
  #define PCIE_ILTR_OVRD_PCU_FORCE_NL_MSK                              (0x00004000)
  #define PCIE_ILTR_OVRD_PCU_FORCE_NL_MIN                              (0)
  #define PCIE_ILTR_OVRD_PCU_FORCE_NL_MAX                              (1) // 0x00000001
  #define PCIE_ILTR_OVRD_PCU_FORCE_NL_DEF                              (0x00000000)
  #define PCIE_ILTR_OVRD_PCU_FORCE_NL_HSH                              (0x011C5D30)

  #define PCIE_ILTR_OVRD_PCU_NL_V_OFF                                  (15)
  #define PCIE_ILTR_OVRD_PCU_NL_V_WID                                  ( 1)
  #define PCIE_ILTR_OVRD_PCU_NL_V_MSK                                  (0x00008000)
  #define PCIE_ILTR_OVRD_PCU_NL_V_MIN                                  (0)
  #define PCIE_ILTR_OVRD_PCU_NL_V_MAX                                  (1) // 0x00000001
  #define PCIE_ILTR_OVRD_PCU_NL_V_DEF                                  (0x00000000)
  #define PCIE_ILTR_OVRD_PCU_NL_V_HSH                                  (0x011E5D30)

  #define PCIE_ILTR_OVRD_PCU_SXL_OFF                                   (16)
  #define PCIE_ILTR_OVRD_PCU_SXL_WID                                   (10)
  #define PCIE_ILTR_OVRD_PCU_SXL_MSK                                   (0x03FF0000)
  #define PCIE_ILTR_OVRD_PCU_SXL_MIN                                   (0)
  #define PCIE_ILTR_OVRD_PCU_SXL_MAX                                   (1023) // 0x000003FF
  #define PCIE_ILTR_OVRD_PCU_SXL_DEF                                   (0x00000000)
  #define PCIE_ILTR_OVRD_PCU_SXL_HSH                                   (0x0A205D30)

  #define PCIE_ILTR_OVRD_PCU_SXLM_OFF                                  (26)
  #define PCIE_ILTR_OVRD_PCU_SXLM_WID                                  ( 3)
  #define PCIE_ILTR_OVRD_PCU_SXLM_MSK                                  (0x1C000000)
  #define PCIE_ILTR_OVRD_PCU_SXLM_MIN                                  (0)
  #define PCIE_ILTR_OVRD_PCU_SXLM_MAX                                  (7) // 0x00000007
  #define PCIE_ILTR_OVRD_PCU_SXLM_DEF                                  (0x00000000)
  #define PCIE_ILTR_OVRD_PCU_SXLM_HSH                                  (0x03345D30)

  #define PCIE_ILTR_OVRD_PCU_FORCE_SXL_OFF                             (30)
  #define PCIE_ILTR_OVRD_PCU_FORCE_SXL_WID                             ( 1)
  #define PCIE_ILTR_OVRD_PCU_FORCE_SXL_MSK                             (0x40000000)
  #define PCIE_ILTR_OVRD_PCU_FORCE_SXL_MIN                             (0)
  #define PCIE_ILTR_OVRD_PCU_FORCE_SXL_MAX                             (1) // 0x00000001
  #define PCIE_ILTR_OVRD_PCU_FORCE_SXL_DEF                             (0x00000000)
  #define PCIE_ILTR_OVRD_PCU_FORCE_SXL_HSH                             (0x013C5D30)

  #define PCIE_ILTR_OVRD_PCU_SXL_V_OFF                                 (31)
  #define PCIE_ILTR_OVRD_PCU_SXL_V_WID                                 ( 1)
  #define PCIE_ILTR_OVRD_PCU_SXL_V_MSK                                 (0x80000000)
  #define PCIE_ILTR_OVRD_PCU_SXL_V_MIN                                 (0)
  #define PCIE_ILTR_OVRD_PCU_SXL_V_MAX                                 (1) // 0x00000001
  #define PCIE_ILTR_OVRD_PCU_SXL_V_DEF                                 (0x00000000)
  #define PCIE_ILTR_OVRD_PCU_SXL_V_HSH                                 (0x013E5D30)

#define PCIE_ILTR_VAL_PCU_0_REG                                        (0x00005D34)

  #define PCIE_ILTR_VAL_PCU_0_NL_VALUE_OFF                             ( 0)
  #define PCIE_ILTR_VAL_PCU_0_NL_VALUE_WID                             (10)
  #define PCIE_ILTR_VAL_PCU_0_NL_VALUE_MSK                             (0x000003FF)
  #define PCIE_ILTR_VAL_PCU_0_NL_VALUE_MIN                             (0)
  #define PCIE_ILTR_VAL_PCU_0_NL_VALUE_MAX                             (1023) // 0x000003FF
  #define PCIE_ILTR_VAL_PCU_0_NL_VALUE_DEF                             (0x00000000)
  #define PCIE_ILTR_VAL_PCU_0_NL_VALUE_HSH                             (0x0A005D34)

  #define PCIE_ILTR_VAL_PCU_0_NL_SCALE_OFF                             (10)
  #define PCIE_ILTR_VAL_PCU_0_NL_SCALE_WID                             ( 3)
  #define PCIE_ILTR_VAL_PCU_0_NL_SCALE_MSK                             (0x00001C00)
  #define PCIE_ILTR_VAL_PCU_0_NL_SCALE_MIN                             (0)
  #define PCIE_ILTR_VAL_PCU_0_NL_SCALE_MAX                             (7) // 0x00000007
  #define PCIE_ILTR_VAL_PCU_0_NL_SCALE_DEF                             (0x00000000)
  #define PCIE_ILTR_VAL_PCU_0_NL_SCALE_HSH                             (0x03145D34)

  #define PCIE_ILTR_VAL_PCU_0_NL_RESERVED_OFF                          (13)
  #define PCIE_ILTR_VAL_PCU_0_NL_RESERVED_WID                          ( 2)
  #define PCIE_ILTR_VAL_PCU_0_NL_RESERVED_MSK                          (0x00006000)
  #define PCIE_ILTR_VAL_PCU_0_NL_RESERVED_MIN                          (0)
  #define PCIE_ILTR_VAL_PCU_0_NL_RESERVED_MAX                          (3) // 0x00000003
  #define PCIE_ILTR_VAL_PCU_0_NL_RESERVED_DEF                          (0x00000000)
  #define PCIE_ILTR_VAL_PCU_0_NL_RESERVED_HSH                          (0x021A5D34)

  #define PCIE_ILTR_VAL_PCU_0_NL_VALID_OFF                             (15)
  #define PCIE_ILTR_VAL_PCU_0_NL_VALID_WID                             ( 1)
  #define PCIE_ILTR_VAL_PCU_0_NL_VALID_MSK                             (0x00008000)
  #define PCIE_ILTR_VAL_PCU_0_NL_VALID_MIN                             (0)
  #define PCIE_ILTR_VAL_PCU_0_NL_VALID_MAX                             (1) // 0x00000001
  #define PCIE_ILTR_VAL_PCU_0_NL_VALID_DEF                             (0x00000000)
  #define PCIE_ILTR_VAL_PCU_0_NL_VALID_HSH                             (0x011E5D34)

  #define PCIE_ILTR_VAL_PCU_0_SXL_VALUE_OFF                            (16)
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALUE_WID                            (10)
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALUE_MSK                            (0x03FF0000)
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALUE_MIN                            (0)
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALUE_MAX                            (1023) // 0x000003FF
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALUE_DEF                            (0x00000000)
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALUE_HSH                            (0x0A205D34)

  #define PCIE_ILTR_VAL_PCU_0_SXL_SCALE_OFF                            (26)
  #define PCIE_ILTR_VAL_PCU_0_SXL_SCALE_WID                            ( 3)
  #define PCIE_ILTR_VAL_PCU_0_SXL_SCALE_MSK                            (0x1C000000)
  #define PCIE_ILTR_VAL_PCU_0_SXL_SCALE_MIN                            (0)
  #define PCIE_ILTR_VAL_PCU_0_SXL_SCALE_MAX                            (7) // 0x00000007
  #define PCIE_ILTR_VAL_PCU_0_SXL_SCALE_DEF                            (0x00000000)
  #define PCIE_ILTR_VAL_PCU_0_SXL_SCALE_HSH                            (0x03345D34)

  #define PCIE_ILTR_VAL_PCU_0_SXL_RESERVED_OFF                         (29)
  #define PCIE_ILTR_VAL_PCU_0_SXL_RESERVED_WID                         ( 2)
  #define PCIE_ILTR_VAL_PCU_0_SXL_RESERVED_MSK                         (0x60000000)
  #define PCIE_ILTR_VAL_PCU_0_SXL_RESERVED_MIN                         (0)
  #define PCIE_ILTR_VAL_PCU_0_SXL_RESERVED_MAX                         (3) // 0x00000003
  #define PCIE_ILTR_VAL_PCU_0_SXL_RESERVED_DEF                         (0x00000000)
  #define PCIE_ILTR_VAL_PCU_0_SXL_RESERVED_HSH                         (0x023A5D34)

  #define PCIE_ILTR_VAL_PCU_0_SXL_VALID_OFF                            (31)
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALID_WID                            ( 1)
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALID_MSK                            (0x80000000)
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALID_MIN                            (0)
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALID_MAX                            (1) // 0x00000001
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALID_DEF                            (0x00000000)
  #define PCIE_ILTR_VAL_PCU_0_SXL_VALID_HSH                            (0x013E5D34)

#define PCIE_ILTR_VAL_PCU_1_REG                                        (0x00005D38)
//Duplicate of PCIE_ILTR_VAL_PCU_0_REG

#define PCIE_ILTR_VAL_PCU_2_REG                                        (0x00005D3C)
//Duplicate of PCIE_ILTR_VAL_PCU_0_REG

#define PCIE_ILTR_VAL_PCU_3_REG                                        (0x00005D40)
//Duplicate of PCIE_ILTR_VAL_PCU_0_REG

#define BIOS_MAILBOX_DATA_PCU_REG                                      (0x00005DA0)
//Duplicate of CAMARILLO_MAILBOX_DATA0_PCU_REG

#define BIOS_MAILBOX_INTERFACE_PCU_REG                                 (0x00005DA4)

  #define BIOS_MAILBOX_INTERFACE_PCU_COMMAND_OFF                       ( 0)
  #define BIOS_MAILBOX_INTERFACE_PCU_COMMAND_WID                       ( 8)
  #define BIOS_MAILBOX_INTERFACE_PCU_COMMAND_MSK                       (0x000000FF)
  #define BIOS_MAILBOX_INTERFACE_PCU_COMMAND_MIN                       (0)
  #define BIOS_MAILBOX_INTERFACE_PCU_COMMAND_MAX                       (255) // 0x000000FF
  #define BIOS_MAILBOX_INTERFACE_PCU_COMMAND_DEF                       (0x00000000)
  #define BIOS_MAILBOX_INTERFACE_PCU_COMMAND_HSH                       (0x08005DA4)

  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM1_OFF                        ( 8)
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM1_WID                        ( 8)
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM1_MSK                        (0x0000FF00)
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM1_MIN                        (0)
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM1_MAX                        (255) // 0x000000FF
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM1_DEF                        (0x00000000)
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM1_HSH                        (0x08105DA4)

  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM2_OFF                        (16)
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM2_WID                        (13)
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM2_MSK                        (0x1FFF0000)
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM2_MIN                        (0)
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM2_MAX                        (8191) // 0x00001FFF
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM2_DEF                        (0x00000000)
  #define BIOS_MAILBOX_INTERFACE_PCU_PARAM2_HSH                        (0x0D205DA4)

  #define BIOS_MAILBOX_INTERFACE_PCU_RUN_BUSY_OFF                      (31)
  #define BIOS_MAILBOX_INTERFACE_PCU_RUN_BUSY_WID                      ( 1)
  #define BIOS_MAILBOX_INTERFACE_PCU_RUN_BUSY_MSK                      (0x80000000)
  #define BIOS_MAILBOX_INTERFACE_PCU_RUN_BUSY_MIN                      (0)
  #define BIOS_MAILBOX_INTERFACE_PCU_RUN_BUSY_MAX                      (1) // 0x00000001
  #define BIOS_MAILBOX_INTERFACE_PCU_RUN_BUSY_DEF                      (0x00000000)
  #define BIOS_MAILBOX_INTERFACE_PCU_RUN_BUSY_HSH                      (0x013E5DA4)

#define BIOS_RESET_CPL_PCU_REG                                         (0x00005DA8)

  #define BIOS_RESET_CPL_PCU_RST_CPL_OFF                               ( 0)
  #define BIOS_RESET_CPL_PCU_RST_CPL_WID                               ( 1)
  #define BIOS_RESET_CPL_PCU_RST_CPL_MSK                               (0x00000001)
  #define BIOS_RESET_CPL_PCU_RST_CPL_MIN                               (0)
  #define BIOS_RESET_CPL_PCU_RST_CPL_MAX                               (1) // 0x00000001
  #define BIOS_RESET_CPL_PCU_RST_CPL_DEF                               (0x00000000)
  #define BIOS_RESET_CPL_PCU_RST_CPL_HSH                               (0x01005DA8)

  #define BIOS_RESET_CPL_PCU_PCIE_ENUMERATION_DONE_OFF                 ( 1)
  #define BIOS_RESET_CPL_PCU_PCIE_ENUMERATION_DONE_WID                 ( 1)
  #define BIOS_RESET_CPL_PCU_PCIE_ENUMERATION_DONE_MSK                 (0x00000002)
  #define BIOS_RESET_CPL_PCU_PCIE_ENUMERATION_DONE_MIN                 (0)
  #define BIOS_RESET_CPL_PCU_PCIE_ENUMERATION_DONE_MAX                 (1) // 0x00000001
  #define BIOS_RESET_CPL_PCU_PCIE_ENUMERATION_DONE_DEF                 (0x00000000)
  #define BIOS_RESET_CPL_PCU_PCIE_ENUMERATION_DONE_HSH                 (0x01025DA8)

  #define BIOS_RESET_CPL_PCU_C7_ALLOWED_OFF                            ( 2)
  #define BIOS_RESET_CPL_PCU_C7_ALLOWED_WID                            ( 1)
  #define BIOS_RESET_CPL_PCU_C7_ALLOWED_MSK                            (0x00000004)
  #define BIOS_RESET_CPL_PCU_C7_ALLOWED_MIN                            (0)
  #define BIOS_RESET_CPL_PCU_C7_ALLOWED_MAX                            (1) // 0x00000001
  #define BIOS_RESET_CPL_PCU_C7_ALLOWED_DEF                            (0x00000000)
  #define BIOS_RESET_CPL_PCU_C7_ALLOWED_HSH                            (0x01045DA8)

#define MC_BIOS_REQ_PCU_REG                                            (0x00005E00)

  #define MC_BIOS_REQ_PCU_MC_PLL_RATIO_OFF                             ( 0)
  #define MC_BIOS_REQ_PCU_MC_PLL_RATIO_WID                             ( 8)
  #define MC_BIOS_REQ_PCU_MC_PLL_RATIO_MSK                             (0x000000FF)
  #define MC_BIOS_REQ_PCU_MC_PLL_RATIO_MIN                             (0)
  #define MC_BIOS_REQ_PCU_MC_PLL_RATIO_MAX                             (255) // 0x000000FF
  #define MC_BIOS_REQ_PCU_MC_PLL_RATIO_DEF                             (0x00000000)
  #define MC_BIOS_REQ_PCU_MC_PLL_RATIO_HSH                             (0x08005E00)

  #define MC_BIOS_REQ_PCU_MC_PLL_REF_OFF                               ( 8)
  #define MC_BIOS_REQ_PCU_MC_PLL_REF_WID                               ( 4)
  #define MC_BIOS_REQ_PCU_MC_PLL_REF_MSK                               (0x00000F00)
  #define MC_BIOS_REQ_PCU_MC_PLL_REF_MIN                               (0)
  #define MC_BIOS_REQ_PCU_MC_PLL_REF_MAX                               (15) // 0x0000000F
  #define MC_BIOS_REQ_PCU_MC_PLL_REF_DEF                               (0x00000000)
  #define MC_BIOS_REQ_PCU_MC_PLL_REF_HSH                               (0x04105E00)

  #define MC_BIOS_REQ_PCU_GEAR_OFF                                     (12)
  #define MC_BIOS_REQ_PCU_GEAR_WID                                     ( 2)
  #define MC_BIOS_REQ_PCU_GEAR_MSK                                     (0x00003000)
  #define MC_BIOS_REQ_PCU_GEAR_MIN                                     (0)
  #define MC_BIOS_REQ_PCU_GEAR_MAX                                     (3) // 0x00000003
  #define MC_BIOS_REQ_PCU_GEAR_DEF                                     (0x00000000)
  #define MC_BIOS_REQ_PCU_GEAR_HSH                                     (0x02185E00)

  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_VOLTAGE_OFF                      (17)
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_VOLTAGE_WID                      (10)
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_VOLTAGE_MSK                      (0x07FE0000)
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_VOLTAGE_MIN                      (0)
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_VOLTAGE_MAX                      (1023) // 0x000003FF
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_VOLTAGE_DEF                      (0x00000000)
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_VOLTAGE_HSH                      (0x0A225E00)

  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_ICCMAX_OFF                       (27)
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_ICCMAX_WID                       ( 4)
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_ICCMAX_MSK                       (0x78000000)
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_ICCMAX_MIN                       (0)
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_ICCMAX_MAX                       (15) // 0x0000000F
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_ICCMAX_DEF                       (0x00000000)
  #define MC_BIOS_REQ_PCU_REQ_VDDQ_TX_ICCMAX_HSH                       (0x04365E00)

  #define MC_BIOS_REQ_PCU_RUN_BUSY_OFF                                 (31)
  #define MC_BIOS_REQ_PCU_RUN_BUSY_WID                                 ( 1)
  #define MC_BIOS_REQ_PCU_RUN_BUSY_MSK                                 (0x80000000)
  #define MC_BIOS_REQ_PCU_RUN_BUSY_MIN                                 (0)
  #define MC_BIOS_REQ_PCU_RUN_BUSY_MAX                                 (1) // 0x00000001
  #define MC_BIOS_REQ_PCU_RUN_BUSY_DEF                                 (0x00000000)
  #define MC_BIOS_REQ_PCU_RUN_BUSY_HSH                                 (0x013E5E00)

#define MC_BIOS_DATA_PCU_REG                                           (0x00005E04)

  #define MC_BIOS_DATA_PCU_MC_PLL_RATIO_OFF                            ( 0)
  #define MC_BIOS_DATA_PCU_MC_PLL_RATIO_WID                            ( 8)
  #define MC_BIOS_DATA_PCU_MC_PLL_RATIO_MSK                            (0x000000FF)
  #define MC_BIOS_DATA_PCU_MC_PLL_RATIO_MIN                            (0)
  #define MC_BIOS_DATA_PCU_MC_PLL_RATIO_MAX                            (255) // 0x000000FF
  #define MC_BIOS_DATA_PCU_MC_PLL_RATIO_DEF                            (0x00000000)
  #define MC_BIOS_DATA_PCU_MC_PLL_RATIO_HSH                            (0x08005E04)

  #define MC_BIOS_DATA_PCU_MC_PLL_REF_OFF                              ( 8)
  #define MC_BIOS_DATA_PCU_MC_PLL_REF_WID                              ( 4)
  #define MC_BIOS_DATA_PCU_MC_PLL_REF_MSK                              (0x00000F00)
  #define MC_BIOS_DATA_PCU_MC_PLL_REF_MIN                              (0)
  #define MC_BIOS_DATA_PCU_MC_PLL_REF_MAX                              (15) // 0x0000000F
  #define MC_BIOS_DATA_PCU_MC_PLL_REF_DEF                              (0x00000000)
  #define MC_BIOS_DATA_PCU_MC_PLL_REF_HSH                              (0x04105E04)

  #define MC_BIOS_DATA_PCU_GEAR_OFF                                    (12)
  #define MC_BIOS_DATA_PCU_GEAR_WID                                    ( 2)
  #define MC_BIOS_DATA_PCU_GEAR_MSK                                    (0x00003000)
  #define MC_BIOS_DATA_PCU_GEAR_MIN                                    (0)
  #define MC_BIOS_DATA_PCU_GEAR_MAX                                    (3) // 0x00000003
  #define MC_BIOS_DATA_PCU_GEAR_DEF                                    (0x00000000)
  #define MC_BIOS_DATA_PCU_GEAR_HSH                                    (0x02185E04)

  #define MC_BIOS_DATA_PCU_VDDQ_TX_VOLTAGE_OFF                         (17)
  #define MC_BIOS_DATA_PCU_VDDQ_TX_VOLTAGE_WID                         (10)
  #define MC_BIOS_DATA_PCU_VDDQ_TX_VOLTAGE_MSK                         (0x07FE0000)
  #define MC_BIOS_DATA_PCU_VDDQ_TX_VOLTAGE_MIN                         (0)
  #define MC_BIOS_DATA_PCU_VDDQ_TX_VOLTAGE_MAX                         (1023) // 0x000003FF
  #define MC_BIOS_DATA_PCU_VDDQ_TX_VOLTAGE_DEF                         (0x00000000)
  #define MC_BIOS_DATA_PCU_VDDQ_TX_VOLTAGE_HSH                         (0x0A225E04)

  #define MC_BIOS_DATA_PCU_VDDQ_TX_ICCMAX_OFF                          (27)
  #define MC_BIOS_DATA_PCU_VDDQ_TX_ICCMAX_WID                          ( 4)
  #define MC_BIOS_DATA_PCU_VDDQ_TX_ICCMAX_MSK                          (0x78000000)
  #define MC_BIOS_DATA_PCU_VDDQ_TX_ICCMAX_MIN                          (0)
  #define MC_BIOS_DATA_PCU_VDDQ_TX_ICCMAX_MAX                          (15) // 0x0000000F
  #define MC_BIOS_DATA_PCU_VDDQ_TX_ICCMAX_DEF                          (0x00000000)
  #define MC_BIOS_DATA_PCU_VDDQ_TX_ICCMAX_HSH                          (0x04365E04)

#define SAPMCTL_PCU_REG                                                (0x00005F00)

  #define SAPMCTL_PCU_SACG_ENA_OFF                                     ( 0)
  #define SAPMCTL_PCU_SACG_ENA_WID                                     ( 1)
  #define SAPMCTL_PCU_SACG_ENA_MSK                                     (0x00000001)
  #define SAPMCTL_PCU_SACG_ENA_MIN                                     (0)
  #define SAPMCTL_PCU_SACG_ENA_MAX                                     (1) // 0x00000001
  #define SAPMCTL_PCU_SACG_ENA_DEF                                     (0x00000000)
  #define SAPMCTL_PCU_SACG_ENA_HSH                                     (0x01005F00)

  #define SAPMCTL_PCU_MPLL_OFF_ENA_OFF                                 ( 1)
  #define SAPMCTL_PCU_MPLL_OFF_ENA_WID                                 ( 1)
  #define SAPMCTL_PCU_MPLL_OFF_ENA_MSK                                 (0x00000002)
  #define SAPMCTL_PCU_MPLL_OFF_ENA_MIN                                 (0)
  #define SAPMCTL_PCU_MPLL_OFF_ENA_MAX                                 (1) // 0x00000001
  #define SAPMCTL_PCU_MPLL_OFF_ENA_DEF                                 (0x00000001)
  #define SAPMCTL_PCU_MPLL_OFF_ENA_HSH                                 (0x01025F00)

  #define SAPMCTL_PCU_PPLL_OFF_ENA_OFF                                 ( 2)
  #define SAPMCTL_PCU_PPLL_OFF_ENA_WID                                 ( 1)
  #define SAPMCTL_PCU_PPLL_OFF_ENA_MSK                                 (0x00000004)
  #define SAPMCTL_PCU_PPLL_OFF_ENA_MIN                                 (0)
  #define SAPMCTL_PCU_PPLL_OFF_ENA_MAX                                 (1) // 0x00000001
  #define SAPMCTL_PCU_PPLL_OFF_ENA_DEF                                 (0x00000001)
  #define SAPMCTL_PCU_PPLL_OFF_ENA_HSH                                 (0x01045F00)

  #define SAPMCTL_PCU_SACG_SEN_OFF                                     ( 8)
  #define SAPMCTL_PCU_SACG_SEN_WID                                     ( 1)
  #define SAPMCTL_PCU_SACG_SEN_MSK                                     (0x00000100)
  #define SAPMCTL_PCU_SACG_SEN_MIN                                     (0)
  #define SAPMCTL_PCU_SACG_SEN_MAX                                     (1) // 0x00000001
  #define SAPMCTL_PCU_SACG_SEN_DEF                                     (0x00000001)
  #define SAPMCTL_PCU_SACG_SEN_HSH                                     (0x01105F00)

  #define SAPMCTL_PCU_MPLL_OFF_SEN_OFF                                 ( 9)
  #define SAPMCTL_PCU_MPLL_OFF_SEN_WID                                 ( 1)
  #define SAPMCTL_PCU_MPLL_OFF_SEN_MSK                                 (0x00000200)
  #define SAPMCTL_PCU_MPLL_OFF_SEN_MIN                                 (0)
  #define SAPMCTL_PCU_MPLL_OFF_SEN_MAX                                 (1) // 0x00000001
  #define SAPMCTL_PCU_MPLL_OFF_SEN_DEF                                 (0x00000000)
  #define SAPMCTL_PCU_MPLL_OFF_SEN_HSH                                 (0x01125F00)

  #define SAPMCTL_PCU_MDLL_OFF_SEN_OFF                                 (10)
  #define SAPMCTL_PCU_MDLL_OFF_SEN_WID                                 ( 1)
  #define SAPMCTL_PCU_MDLL_OFF_SEN_MSK                                 (0x00000400)
  #define SAPMCTL_PCU_MDLL_OFF_SEN_MIN                                 (0)
  #define SAPMCTL_PCU_MDLL_OFF_SEN_MAX                                 (1) // 0x00000001
  #define SAPMCTL_PCU_MDLL_OFF_SEN_DEF                                 (0x00000000)
  #define SAPMCTL_PCU_MDLL_OFF_SEN_HSH                                 (0x01145F00)

  #define SAPMCTL_PCU_SACG_SREXIT_OFF                                  (11)
  #define SAPMCTL_PCU_SACG_SREXIT_WID                                  ( 1)
  #define SAPMCTL_PCU_SACG_SREXIT_MSK                                  (0x00000800)
  #define SAPMCTL_PCU_SACG_SREXIT_MIN                                  (0)
  #define SAPMCTL_PCU_SACG_SREXIT_MAX                                  (1) // 0x00000001
  #define SAPMCTL_PCU_SACG_SREXIT_DEF                                  (0x00000000)
  #define SAPMCTL_PCU_SACG_SREXIT_HSH                                  (0x01165F00)

  #define SAPMCTL_PCU_NSWAKE_SREXIT_OFF                                (12)
  #define SAPMCTL_PCU_NSWAKE_SREXIT_WID                                ( 1)
  #define SAPMCTL_PCU_NSWAKE_SREXIT_MSK                                (0x00001000)
  #define SAPMCTL_PCU_NSWAKE_SREXIT_MIN                                (0)
  #define SAPMCTL_PCU_NSWAKE_SREXIT_MAX                                (1) // 0x00000001
  #define SAPMCTL_PCU_NSWAKE_SREXIT_DEF                                (0x00000000)
  #define SAPMCTL_PCU_NSWAKE_SREXIT_HSH                                (0x01185F00)

  #define SAPMCTL_PCU_SACG_MPLL_OFF                                    (13)
  #define SAPMCTL_PCU_SACG_MPLL_WID                                    ( 1)
  #define SAPMCTL_PCU_SACG_MPLL_MSK                                    (0x00002000)
  #define SAPMCTL_PCU_SACG_MPLL_MIN                                    (0)
  #define SAPMCTL_PCU_SACG_MPLL_MAX                                    (1) // 0x00000001
  #define SAPMCTL_PCU_SACG_MPLL_DEF                                    (0x00000001)
  #define SAPMCTL_PCU_SACG_MPLL_HSH                                    (0x011A5F00)

  #define SAPMCTL_PCU_MPLL_ON_DE_OFF                                   (14)
  #define SAPMCTL_PCU_MPLL_ON_DE_WID                                   ( 1)
  #define SAPMCTL_PCU_MPLL_ON_DE_MSK                                   (0x00004000)
  #define SAPMCTL_PCU_MPLL_ON_DE_MIN                                   (0)
  #define SAPMCTL_PCU_MPLL_ON_DE_MAX                                   (1) // 0x00000001
  #define SAPMCTL_PCU_MPLL_ON_DE_DEF                                   (0x00000000)
  #define SAPMCTL_PCU_MPLL_ON_DE_HSH                                   (0x011C5F00)

  #define SAPMCTL_PCU_MDLL_ON_DE_OFF                                   (15)
  #define SAPMCTL_PCU_MDLL_ON_DE_WID                                   ( 1)
  #define SAPMCTL_PCU_MDLL_ON_DE_MSK                                   (0x00008000)
  #define SAPMCTL_PCU_MDLL_ON_DE_MIN                                   (0)
  #define SAPMCTL_PCU_MDLL_ON_DE_MAX                                   (1) // 0x00000001
  #define SAPMCTL_PCU_MDLL_ON_DE_DEF                                   (0x00000000)
  #define SAPMCTL_PCU_MDLL_ON_DE_HSH                                   (0x011E5F00)

#define P_COMP_PCU_REG                                                 (0x00005F04)

  #define P_COMP_PCU_COMP_DISABLE_OFF                                  ( 0)
  #define P_COMP_PCU_COMP_DISABLE_WID                                  ( 1)
  #define P_COMP_PCU_COMP_DISABLE_MSK                                  (0x00000001)
  #define P_COMP_PCU_COMP_DISABLE_MIN                                  (0)
  #define P_COMP_PCU_COMP_DISABLE_MAX                                  (1) // 0x00000001
  #define P_COMP_PCU_COMP_DISABLE_DEF                                  (0x00000001)
  #define P_COMP_PCU_COMP_DISABLE_HSH                                  (0x01005F04)

  #define P_COMP_PCU_COMP_INTERVAL_OFF                                 ( 1)
  #define P_COMP_PCU_COMP_INTERVAL_WID                                 ( 4)
  #define P_COMP_PCU_COMP_INTERVAL_MSK                                 (0x0000001E)
  #define P_COMP_PCU_COMP_INTERVAL_MIN                                 (0)
  #define P_COMP_PCU_COMP_INTERVAL_MAX                                 (15) // 0x0000000F
  #define P_COMP_PCU_COMP_INTERVAL_DEF                                 (0x00000008)
  #define P_COMP_PCU_COMP_INTERVAL_HSH                                 (0x04025F04)

  #define P_COMP_PCU_COMP_FORCE_OFF                                    ( 8)
  #define P_COMP_PCU_COMP_FORCE_WID                                    ( 1)
  #define P_COMP_PCU_COMP_FORCE_MSK                                    (0x00000100)
  #define P_COMP_PCU_COMP_FORCE_MIN                                    (0)
  #define P_COMP_PCU_COMP_FORCE_MAX                                    (1) // 0x00000001
  #define P_COMP_PCU_COMP_FORCE_DEF                                    (0x00000000)
  #define P_COMP_PCU_COMP_FORCE_HSH                                    (0x01105F04)

#define M_COMP_PCU_REG                                                 (0x00005F08)

  #define M_COMP_PCU_COMP_DISABLE_OFF                                  ( 0)
  #define M_COMP_PCU_COMP_DISABLE_WID                                  ( 1)
  #define M_COMP_PCU_COMP_DISABLE_MSK                                  (0x00000001)
  #define M_COMP_PCU_COMP_DISABLE_MIN                                  (0)
  #define M_COMP_PCU_COMP_DISABLE_MAX                                  (1) // 0x00000001
  #define M_COMP_PCU_COMP_DISABLE_DEF                                  (0x00000000)
  #define M_COMP_PCU_COMP_DISABLE_HSH                                  (0x01005F08)

  #define M_COMP_PCU_COMP_INTERVAL_OFF                                 ( 1)
  #define M_COMP_PCU_COMP_INTERVAL_WID                                 ( 4)
  #define M_COMP_PCU_COMP_INTERVAL_MSK                                 (0x0000001E)
  #define M_COMP_PCU_COMP_INTERVAL_MIN                                 (0)
  #define M_COMP_PCU_COMP_INTERVAL_MAX                                 (15) // 0x0000000F
  #define M_COMP_PCU_COMP_INTERVAL_DEF                                 (0x0000000D)
  #define M_COMP_PCU_COMP_INTERVAL_HSH                                 (0x04025F08)

  #define M_COMP_PCU_COMP_FORCE_OFF                                    ( 8)
  #define M_COMP_PCU_COMP_FORCE_WID                                    ( 1)
  #define M_COMP_PCU_COMP_FORCE_MSK                                    (0x00000100)
  #define M_COMP_PCU_COMP_FORCE_MIN                                    (0)
  #define M_COMP_PCU_COMP_FORCE_MAX                                    (1) // 0x00000001
  #define M_COMP_PCU_COMP_FORCE_DEF                                    (0x00000000)
  #define M_COMP_PCU_COMP_FORCE_HSH                                    (0x01105F08)

#define FUSA_CONFIG_PCU_REG                                            (0x00005F0C)

  #define FUSA_CONFIG_PCU_UMCNF_CATERR_DIS_OFF                         ( 0)
  #define FUSA_CONFIG_PCU_UMCNF_CATERR_DIS_WID                         ( 1)
  #define FUSA_CONFIG_PCU_UMCNF_CATERR_DIS_MSK                         (0x00000001)
  #define FUSA_CONFIG_PCU_UMCNF_CATERR_DIS_MIN                         (0)
  #define FUSA_CONFIG_PCU_UMCNF_CATERR_DIS_MAX                         (1) // 0x00000001
  #define FUSA_CONFIG_PCU_UMCNF_CATERR_DIS_DEF                         (0x00000000)
  #define FUSA_CONFIG_PCU_UMCNF_CATERR_DIS_HSH                         (0x01005F0C)

#define FUSA_TASK_COMPLETION_COUNTER_PCU_REG                           (0x00005F10)

  #define FUSA_TASK_COMPLETION_COUNTER_PCU_COMPLETION_COUNTER_OFF      ( 0)
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_COMPLETION_COUNTER_WID      (24)
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_COMPLETION_COUNTER_MSK      (0x00FFFFFF)
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_COMPLETION_COUNTER_MIN      (0)
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_COMPLETION_COUNTER_MAX      (16777215) // 0x00FFFFFF
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_COMPLETION_COUNTER_DEF      (0x00000000)
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_COMPLETION_COUNTER_HSH      (0x18005F10)

  #define FUSA_TASK_COMPLETION_COUNTER_PCU_CRC8_OFF                    (24)
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_CRC8_WID                    ( 8)
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_CRC8_MSK                    (0xFF000000)
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_CRC8_MIN                    (0)
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_CRC8_MAX                    (255) // 0x000000FF
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_CRC8_DEF                    (0x00000000)
  #define FUSA_TASK_COMPLETION_COUNTER_PCU_CRC8_HSH                    (0x08305F10)

#define SAPM_PG_STATUS_PCU_REG                                         (0x00005F18)

  #define SAPM_PG_STATUS_PCU_PCIE_PWR_STATUS_OFF                       ( 0)
  #define SAPM_PG_STATUS_PCU_PCIE_PWR_STATUS_WID                       ( 1)
  #define SAPM_PG_STATUS_PCU_PCIE_PWR_STATUS_MSK                       (0x00000001)
  #define SAPM_PG_STATUS_PCU_PCIE_PWR_STATUS_MIN                       (0)
  #define SAPM_PG_STATUS_PCU_PCIE_PWR_STATUS_MAX                       (1) // 0x00000001
  #define SAPM_PG_STATUS_PCU_PCIE_PWR_STATUS_DEF                       (0x00000001)
  #define SAPM_PG_STATUS_PCU_PCIE_PWR_STATUS_HSH                       (0x01005F18)

#define CONFIG_TDP_NOMINAL_PCU_REG                                     (0x00005F3C)

  #define CONFIG_TDP_NOMINAL_PCU_TDP_RATIO_OFF                         ( 0)
  #define CONFIG_TDP_NOMINAL_PCU_TDP_RATIO_WID                         ( 8)
  #define CONFIG_TDP_NOMINAL_PCU_TDP_RATIO_MSK                         (0x000000FF)
  #define CONFIG_TDP_NOMINAL_PCU_TDP_RATIO_MIN                         (0)
  #define CONFIG_TDP_NOMINAL_PCU_TDP_RATIO_MAX                         (255) // 0x000000FF
  #define CONFIG_TDP_NOMINAL_PCU_TDP_RATIO_DEF                         (0x00000000)
  #define CONFIG_TDP_NOMINAL_PCU_TDP_RATIO_HSH                         (0x08005F3C)

#define CONFIG_TDP_LEVEL1_PCU_REG                                      (0x00005F40)

  #define CONFIG_TDP_LEVEL1_PCU_PKG_TDP_OFF                            ( 0)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_TDP_WID                            (15)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_TDP_MSK                            (0x00007FFF)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_TDP_MIN                            (0)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_TDP_MAX                            (32767) // 0x00007FFF
  #define CONFIG_TDP_LEVEL1_PCU_PKG_TDP_DEF                            (0x00000000)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_TDP_HSH                            (0x4F005F40)

  #define CONFIG_TDP_LEVEL1_PCU_TDP_RATIO_OFF                          (16)
  #define CONFIG_TDP_LEVEL1_PCU_TDP_RATIO_WID                          ( 8)
  #define CONFIG_TDP_LEVEL1_PCU_TDP_RATIO_MSK                          (0x00FF0000)
  #define CONFIG_TDP_LEVEL1_PCU_TDP_RATIO_MIN                          (0)
  #define CONFIG_TDP_LEVEL1_PCU_TDP_RATIO_MAX                          (255) // 0x000000FF
  #define CONFIG_TDP_LEVEL1_PCU_TDP_RATIO_DEF                          (0x00000000)
  #define CONFIG_TDP_LEVEL1_PCU_TDP_RATIO_HSH                          (0x48205F40)

  #define CONFIG_TDP_LEVEL1_PCU_PKG_MAX_PWR_OFF                        (32)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MAX_PWR_WID                        (15)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MAX_PWR_MSK                        (0x00007FFF00000000ULL)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MAX_PWR_MIN                        (0)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MAX_PWR_MAX                        (32767) // 0x00007FFF
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MAX_PWR_DEF                        (0x00000000)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MAX_PWR_HSH                        (0x4F405F40)

  #define CONFIG_TDP_LEVEL1_PCU_PKG_MIN_PWR_OFF                        (48)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MIN_PWR_WID                        (15)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MIN_PWR_MSK                        (0x7FFF000000000000ULL)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MIN_PWR_MIN                        (0)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MIN_PWR_MAX                        (32767) // 0x00007FFF
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MIN_PWR_DEF                        (0x00000000)
  #define CONFIG_TDP_LEVEL1_PCU_PKG_MIN_PWR_HSH                        (0x4F605F40)

#define CONFIG_TDP_LEVEL2_PCU_REG                                      (0x00005F48)
//Duplicate of CONFIG_TDP_LEVEL1_PCU_REG

#define CONFIG_TDP_CONTROL_PCU_REG                                     (0x00005F50)

  #define CONFIG_TDP_CONTROL_PCU_TDP_LEVEL_OFF                         ( 0)
  #define CONFIG_TDP_CONTROL_PCU_TDP_LEVEL_WID                         ( 2)
  #define CONFIG_TDP_CONTROL_PCU_TDP_LEVEL_MSK                         (0x00000003)
  #define CONFIG_TDP_CONTROL_PCU_TDP_LEVEL_MIN                         (0)
  #define CONFIG_TDP_CONTROL_PCU_TDP_LEVEL_MAX                         (3) // 0x00000003
  #define CONFIG_TDP_CONTROL_PCU_TDP_LEVEL_DEF                         (0x00000000)
  #define CONFIG_TDP_CONTROL_PCU_TDP_LEVEL_HSH                         (0x02005F50)

  #define CONFIG_TDP_CONTROL_PCU_CONFIG_TDP_LOCK_OFF                   (31)
  #define CONFIG_TDP_CONTROL_PCU_CONFIG_TDP_LOCK_WID                   ( 1)
  #define CONFIG_TDP_CONTROL_PCU_CONFIG_TDP_LOCK_MSK                   (0x80000000)
  #define CONFIG_TDP_CONTROL_PCU_CONFIG_TDP_LOCK_MIN                   (0)
  #define CONFIG_TDP_CONTROL_PCU_CONFIG_TDP_LOCK_MAX                   (1) // 0x00000001
  #define CONFIG_TDP_CONTROL_PCU_CONFIG_TDP_LOCK_DEF                   (0x00000000)
  #define CONFIG_TDP_CONTROL_PCU_CONFIG_TDP_LOCK_HSH                   (0x013E5F50)

#define TURBO_ACTIVATION_RATIO_PCU_REG                                 (0x00005F54)

  #define TURBO_ACTIVATION_RATIO_PCU_MAX_NON_TURBO_RATIO_OFF           ( 0)
  #define TURBO_ACTIVATION_RATIO_PCU_MAX_NON_TURBO_RATIO_WID           ( 8)
  #define TURBO_ACTIVATION_RATIO_PCU_MAX_NON_TURBO_RATIO_MSK           (0x000000FF)
  #define TURBO_ACTIVATION_RATIO_PCU_MAX_NON_TURBO_RATIO_MIN           (0)
  #define TURBO_ACTIVATION_RATIO_PCU_MAX_NON_TURBO_RATIO_MAX           (255) // 0x000000FF
  #define TURBO_ACTIVATION_RATIO_PCU_MAX_NON_TURBO_RATIO_DEF           (0x00000000)
  #define TURBO_ACTIVATION_RATIO_PCU_MAX_NON_TURBO_RATIO_HSH           (0x08005F54)

  #define TURBO_ACTIVATION_RATIO_PCU_TURBO_ACTIVATION_RATIO_LOCK_OFF   (31)
  #define TURBO_ACTIVATION_RATIO_PCU_TURBO_ACTIVATION_RATIO_LOCK_WID   ( 1)
  #define TURBO_ACTIVATION_RATIO_PCU_TURBO_ACTIVATION_RATIO_LOCK_MSK   (0x80000000)
  #define TURBO_ACTIVATION_RATIO_PCU_TURBO_ACTIVATION_RATIO_LOCK_MIN   (0)
  #define TURBO_ACTIVATION_RATIO_PCU_TURBO_ACTIVATION_RATIO_LOCK_MAX   (1) // 0x00000001
  #define TURBO_ACTIVATION_RATIO_PCU_TURBO_ACTIVATION_RATIO_LOCK_DEF   (0x00000000)
  #define TURBO_ACTIVATION_RATIO_PCU_TURBO_ACTIVATION_RATIO_LOCK_HSH   (0x013E5F54)

#define OC_STATUS_PCU_REG                                              (0x00005F58)

  #define OC_STATUS_PCU_MC_TIMING_RUNTIME_OC_ENABLED_OFF               ( 0)
  #define OC_STATUS_PCU_MC_TIMING_RUNTIME_OC_ENABLED_WID               ( 1)
  #define OC_STATUS_PCU_MC_TIMING_RUNTIME_OC_ENABLED_MSK               (0x00000001)
  #define OC_STATUS_PCU_MC_TIMING_RUNTIME_OC_ENABLED_MIN               (0)
  #define OC_STATUS_PCU_MC_TIMING_RUNTIME_OC_ENABLED_MAX               (1) // 0x00000001
  #define OC_STATUS_PCU_MC_TIMING_RUNTIME_OC_ENABLED_DEF               (0x00000000)
  #define OC_STATUS_PCU_MC_TIMING_RUNTIME_OC_ENABLED_HSH               (0x01005F58)

  #define OC_STATUS_PCU_RSVD_0_OFF                                     ( 1)
  #define OC_STATUS_PCU_RSVD_0_WID                                     ( 1)
  #define OC_STATUS_PCU_RSVD_0_MSK                                     (0x00000002)
  #define OC_STATUS_PCU_RSVD_0_MIN                                     (0)
  #define OC_STATUS_PCU_RSVD_0_MAX                                     (1) // 0x00000001
  #define OC_STATUS_PCU_RSVD_0_DEF                                     (0x00000000)
  #define OC_STATUS_PCU_RSVD_0_HSH                                     (0x01025F58)

  #define OC_STATUS_PCU_RSVD_1_OFF                                     ( 2)
  #define OC_STATUS_PCU_RSVD_1_WID                                     ( 1)
  #define OC_STATUS_PCU_RSVD_1_MSK                                     (0x00000004)
  #define OC_STATUS_PCU_RSVD_1_MIN                                     (0)
  #define OC_STATUS_PCU_RSVD_1_MAX                                     (1) // 0x00000001
  #define OC_STATUS_PCU_RSVD_1_DEF                                     (0x00000000)
  #define OC_STATUS_PCU_RSVD_1_HSH                                     (0x01045F58)

  #define OC_STATUS_PCU_RSVD_2_OFF                                     ( 3)
  #define OC_STATUS_PCU_RSVD_2_WID                                     ( 1)
  #define OC_STATUS_PCU_RSVD_2_MSK                                     (0x00000008)
  #define OC_STATUS_PCU_RSVD_2_MIN                                     (0)
  #define OC_STATUS_PCU_RSVD_2_MAX                                     (1) // 0x00000001
  #define OC_STATUS_PCU_RSVD_2_DEF                                     (0x00000000)
  #define OC_STATUS_PCU_RSVD_2_HSH                                     (0x01065F58)
#pragma pack(pop)
#endif
