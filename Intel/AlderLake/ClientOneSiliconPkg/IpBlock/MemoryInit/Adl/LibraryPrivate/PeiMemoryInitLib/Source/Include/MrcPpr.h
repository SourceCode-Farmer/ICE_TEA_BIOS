/** @file
  This file contains functions to find and track DRAM Failing addresses for the
  Post Package Repair feature.

  @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

  @par Specification Reference:
**/

#include "MrcInterface.h"
#include "MrcCpgcApi.h"

/**
Compare most significant channel address bits excluding row bits

@param CurAddr          - First address to compare
@param RowAddr          - Second address to compare

@retval   TRUE for match, otherwise FALSE
**/
BOOLEAN
CurrentAddrMatch (
  ROW_ADDR CurAddr,
  ROW_ADDR RowAddr
  );

  /**
  Returns the CPCG cacheline mask to isolate the per bank group failure.
  A value of 1 means the correspond bank group is enabled for CPGC error check.

  0xFF means check error for all cachelines in other worlds all bank groups.

  In the 8 bank groups interleave case,
  0x01 means check error for the 1st bank group which was programmed in the
  CPGC logical2physical mapping register l2p_bank0_mapping.
  0x80 means check error for the 8th bank group which was programmed in the
  CPGC logical2physical mapping register l2p_bank7_mapping.

  Please be aware that when direction is MT_ADDR_DIR_DN, the order of the
  bank groups is reversed. 0x01 corresponds to the l2p_bank7_mapping while
  0x80 corresponds to the l2p_bank0_mapping

  @param[in] RetryState   - The retry index. Each retry target one bank group.
  @param[in] Direction    - Specifies the test direction.
  @param[in] BgInterleave - The number of interleave bank groups

  @retval cacheline bit mask
  **/
UINT8
GetCachelineMask (
  IN UINT8      RetryState,
  IN UINT8      Direction,
  IN UINT8      BgInterleave
  );

/**
  Gets the default Bank Group interleave value per technology

  @param[in]  MrcData                 - Global MRC data structure
  @param[out] BgInterleave            - Default bank group interleave value for the current DDR memory technology
**/
MrcStatus
GetDefaultBankGroupInterleave (
  IN  MrcParameters *const MrcData,
  OUT UINT8                *BgInterleave
  );

/**
Shifts row fail list to right by number of entries starting at given index

@param Host               - Pointer to sysHost, the system Host (root) structure
@param Socket             - Socket Id
@param Ch                 - Channel number
@param Index              - List index to begin the shift operation
@param NumEntries         - Number of entries to shift

@retval status - MRC_STATUS_SUCCESS / MRC_STATUS_FAILURE
**/
MrcStatus
ShiftFailRangeListRight (
  MrcParameters *const MrcData,
  UINT8                Controller,
  UINT8                Channel,
  UINT32               Index,
  UINT32               NumEntries
  );


  /**
  Shifts row fail list to left by number of entries starting at given index

  @param Host               - Pointer to sysHost, the system Host (root) structure
  @param Socket             - Socket Id
  @param Ch                 - Channel number
  @param Index              - List index to begin the shift operation
  @param NumEntries         - Number of entries to shift

  @retval status - mrcSuccess / mrcFail
  **/
MrcStatus
ShiftFailRangeListLeft (
  MrcParameters *const MrcData,
  UINT8                Controller,
  UINT8                Channel,
  UINT32               Index,
  UINT32               NumEntries
  );

/**
  Combine two adjacent entries if they have the same failing signiture

  @param[in] MrcData        - Global MRC data structure
  @param[in] Controller     - Controller number to compare
  @param[in] Channel        - Channel number to compare
  @param[in] Index          - List index of the entry be combine with its next index entry

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
CombineFailRangeList (
  MrcParameters *const MrcData,
  UINT8                Controller,
  UINT8                Channel,
  UINT32               Index
  );

/**
  Check row failure list and PPR resource list to determine if repairs are required

  @param[in] MrcData              - Global MRC data structure
  @param[in] McChBitMask          - Controller number to compare
  @param[in] DimmRank             - DIMM containing logicalRank
  @param[in] Logical2Physical     - Geometery for logical rank within channel
  @param[in] BaseBits             - Number of bank bits in SW loop

  @retval status - TRUE/FALSE
**/
BOOLEAN
IsPprRepairRequired (
  MrcParameters  *const   MrcData,
  UINT32                  McChBitMask,
  UINT8                   DimmRank[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL],
  UINT8                   Logical2Physical[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL],
  UINT8                   BaseBits
  );

/**
  Updates Row failure list with last failure, coalescing failure ranges where possible

  @param[in] MrcData        - Global MRC data structure
  @param[in] Controller     - Controller number to update
  @param[in] Channel        - Channel number to update
  @param[in] CheckMaskError - Check for previously masked range
  @param[in] NewFail        - New failure information

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
UpdateRowFailList (
  MrcParameters *const MrcData,
  UINT8                Controller,
  UINT8                Channel,
  BOOLEAN              CheckMaskError,
  ROW_FAIL_RANGE       NewFail
  );

/**
  Updates Row failure list with last failure, coalescing failure ranges where possible

  @param[in]  MrcData                 - Global MRC data structure
  @param[in]  McChBitMask             - Memory Controller Channel Bit mask to update
  @param[in]  cpgcErrorStatus         - the failure information per channel
  @param[in]  DimmTemp                - Dimm Temperature on initial failure
  @param[in]  rowBits                 - Number of row bits supported by current logical rank
  @param[in]  baseBits                - Number of bank bits in SW loop
  @param[in]  BgInterleave            - Number of bank groups in HW loop.
  @param[out] TestStatus              - Pass/fail status for the test per channel
  @param[in]  Direction               - Sequential address direction MT_ADDR_DIR_UP, MT_ADDR_DIR_DN
  @param[in]  AmtRetryDisabled        - Indicates when the AMT Retry flag is disabled

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
UpdateRowFailures (
  IN  MrcParameters *const MrcData,
  IN     UINT32            McChBitMask,
  IN     CPGC_ERROR_STATUS_MATS cpgcErrorStatus[MAX_CONTROLLER][MAX_CHANNEL],
  IN     INT16             DimmTemp[MAX_CONTROLLER][MAX_CHANNEL],
  IN     UINT8             rowBits[MAX_CONTROLLER][MAX_CHANNEL],
  IN     UINT32            baseBits,
  OUT    UINT8             TestStatus[MAX_CONTROLLER][MAX_CHANNEL],
  IN     UINT8             Direction,
  IN     BOOLEAN           AmtRetryDisabled
  );

/**
  Searches the failure tracking list for NewFail for each bank address on each single DQ/Device and returns a bit mask of the DQ bits with
  number of row failures greater or equal to the given threshold.


  @param[in] MrcData       - Pointer to global MRC data.
  @param[in] Controller    - 0-based index to controller
  @param[in] Channel       - 0-based index to channel
  @param[in] NewFail       - New failure information
  @param[in] Threshold     - Number of errors per DQ where mask bit is set
  @param[in] DqMaskOver    - Bitmask of DQ having number of row errors in bank greater or equal to threshold

  @retval status - TRUE if any DQ has number of row errors greater or equal to threshold
**/
BOOLEAN
GetDqMaskOverThreshold (
  MrcParameters  *const     MrcData,
  UINT8                     Controller,
  UINT8                     Channel,
  ROW_FAIL_RANGE            NewFail,
  UINT32                    Threshold,
  UINT32                    DqMaskOver[3]
  );

/**
  Executes a step of Advanced Memory test using CPGC engine and logs results

  @param [in]  MrcData             - Pointer to global MRC data.
  @param [in]     McChBitMask      - Memory Controller Channel Bit mask to update
  @param [in,out] *RepairDone      - Flag to indicate the repair was done

  @retval status - MRC_STATUS_SUCCESS/MRC_STATUS_FAILURE
**/
MrcStatus
CpgcMemTestDispositionFailRange (
  IN     MrcParameters *const    MrcData,
  IN     UINT32                  McChBitMask,
  IN OUT UINT8                   *RepairDone
  );

/**
  Check to see if Retry is required after a PPR repair

  @param[in]  MrcData     - Include all MRC global data.
  @param[in]  RepairDone  - Indicator whether PPR repair was done
  @param[in]  RetryCount  - Count of retry attempts

  @retval TRUE if Retry is required; FALSE otherwise
**/
BOOLEAN
IsRetryRequired (
  IN MrcParameters *const MrcData,
  IN UINT8                RepairDone,
  IN UINT32               RetryCount
  );

/**

  Inject error for given Rank/Controller/Channel

  @param[in]  MrcData       - Pointer to global MRC data.
  @param[in]  Rank          - Phyiscal rank index inisde the dimm
  @param[in]  Controller    - 0-based index to controller
  @param[in]  Channel       - 0-based index to channel
  @param[in]  Bank          - bank address number
  @param[in]  Row           - row address
  @param[in]  TestSize      - number of rows to test
  @param[in]  Pattern[16]   - array holding the test pattern
  @param[in]  ErrInjMask16  - Bitmask of DQ lanes to inject error
  @param[in]  Direction     - Sequential address direction MT_ADDR_DIR_UP, MT_ADDR_DIR_DN
  @param[in]  SeqDataInv[2] - Enables pattern inversion per subsequence
  @param[in]  IsUseInvtPat  - Info to indicate whether or not patternQW is inverted by comparing original pattern
  @param[in]  UiShl[16]     - Enables pattern rotation per UI

  @retval MrcStatus - mrcSuccess/mrcFail
**/
MrcStatus
InjectMemtestError (
  IN MrcParameters* const MrcData,
  IN UINT32         Rank,
  IN UINT8          Controller,
  IN UINT8          Channel,
  IN UINT32         Bank,
  IN UINT32         Row,
  IN UINT32         TestSize,
  IN UINT64         Pattern[16],
  IN UINT16         ErrInjMask16,
  IN UINT8          Direction,
  IN UINT8          SeqDataInv[2],
  IN BOOLEAN        IsUseInvtPat,
  IN UINT8          UiShl[16]
  );


/**
Function to run retention test across memory array using Raster Repo Mode 3

@param[in]  MrcData      - Pointer to global MRC data.

@retval MrcStatus - mrcSuccess if no errors found.
**/
MrcStatus
MrcPprEnable (
  IN MrcParameters *const MrcData
  );
