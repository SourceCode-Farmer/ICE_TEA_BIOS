/** @file
Implementation of memory abstraction interface.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
JEDEC
**/
#include "MrcInterface.h"
#include "MrcDdrCommon.h"
#include "MrcChipApi.h"
#include "MrcMemoryApi.h"
#include "MrcDdr3.h"
#include "MrcDdr4.h"
#include "MrcDdr5.h"
#include "MrcLpddr4.h"
#include "MrcLpddr4Registers.h"
#include "MrcLpddr5.h"
#include "MrcMaintenance.h"
#include "MrcCpgcApi.h"
#include "Cpgc20TestCtl.h"
#include "Cpgc20Patterns.h"
#include "MrcPpr.h"

/**
  This function returns the ODTL timing.

  @param[in]  MrcData - Pointer to MRC global data.
  @param[in]  Frequency - Current data rate.
  @param[in]  OdtType   - Selects which command ODT type.
  @param[in]  OdtlParam - Determines if this is for ODT On or Off.

  @retval INT8 - ODT impact to the command timing.
**/
INT8
MrcGetOdtlTiming (
  IN  MrcParameters *const  MrcData,
  IN  MrcFrequency          Frequency,
  IN  LPDDR_ODT_TYPE        OdtType,
  IN  LPDDR_ODTL_PARAM      OdtlParam
  )
{
  INT8 RetVal;
  MRC_LP5_BANKORG  Lp5BankOrg;

  RetVal = 0;
  switch (MrcData->Outputs.DdrType)  {
    case MRC_DDR_TYPE_LPDDR4:
      // Currently LP4 implementation only needs Write ODT
      if (OdtType == LpWrOdt) {
        // LPDDR4 timing. WL Set is statically configured to Set B.
        RetVal = MrcGetOdtlTimingLpddr4 (Frequency, Lp4WlSetB, OdtlParam);
        // ODTLoff LPDDR4 JEDEC Spec table assumes BL=16. Note: for BL32, 8 tCK should be added
        if ((OdtlParam == LpOdtlOff) && ((MrcData->Outputs.BurstLength * 2) == 32)) {
          RetVal += 8;
        }
      }
      break;

    case MRC_DDR_TYPE_LPDDR5:
      Lp5BankOrg = MrcGetBankBgOrg (MrcData, Frequency);
      if (OdtType == LpWrOdt) {
        RetVal = MrcGetWrOdtlLpddr5 (MrcData, Frequency, OdtlParam, Lp5BankOrg);
      } else {
        RetVal = MrcGetNtRdOdtlLpddr5 (Frequency, OdtlParam);
      }
      break;

    default:
      break;
  }

  return RetVal;
}

/**
  This function returns the timing value for Precharge to Precharge delay.

  @param[in]  MrcData - Pointer to MRC global data.

  @retval UINT8 - 0 for unsupported technologies, otherwise the delay in tCK
**/
UINT8
MrcGetPpd (
  IN MrcParameters *const MrcData
  )
{
  UINT8 RetVal;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      RetVal = MRC_LP4_tPPD_ALL_FREQ;
      break;

    case MRC_DDR_TYPE_LPDDR5:
      RetVal = MRC_LP5_tPPD_ALL_FREQ;
      break;

    case MRC_DDR_TYPE_DDR5:
      RetVal = MRC_DDR5_tPPD_ALL_FREQ;
      break;

    default:
      RetVal = 0;
      break;
  }

  return RetVal;
}

/**
  This function returns the timing value for Read Preamble.

  @param[in]  MrcData - Pointer to MRC global data.

  @retval UINT8 - 0 for unsupported technologies, otherwise the delay in tCK.
**/
UINT8
MrcGetRpre (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput      *Outputs;
  MrcSaveData    *SaveOutputs;
  UINT8          RetVal;
  const MrcInput *Inputs;

  Outputs      = &MrcData->Outputs;
  SaveOutputs  = &MrcData->Save.Data;
  Inputs       = &MrcData->Inputs;

  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_LPDDR5:
      if (Outputs->RxMode == MrcRxModeUnmatchedP) {
        if((SaveOutputs->UiLock - SaveOutputs->CompOffset) >= 3) {
          RetVal = TRPRE_LPDDR5_3tCK;
        } else {
          RetVal = TRPRE_LPDDR5_1tCK;
        }
      } else {
        RetVal = TRPRE_LPDDR5_1tCK;
      }
      break;

    case MRC_DDR_TYPE_LPDDR4:
      RetVal = TRPRE_ALL_FREQ_LP4;
      break;

    case MRC_DDR_TYPE_DDR4:
      RetVal = TRPRE_ALL_FREQ;
      break;

    case MRC_DDR_TYPE_DDR5:
      if (Outputs->RxMode == MrcRxModeUnmatchedN) {
        //---- UiLock  RDPRE (tCK) ------
        // 1.5  0       2
        // 2.5  1       2
        // 3.5  2       4
        // 4.5  3       4
        // 5.5  4       4
        // 6.5  5       4
        // 7.5  6       4
        // 8.5  7       4
        if (SaveOutputs->UiLock < 2) {
          RetVal = TRPRE_DDR5_2tCK;
        } else {
          RetVal = TRPRE_DDR5_4tCK;
        }
      } else { // Matched
        //  @todo_adl change for adl_s
        RetVal = ((Inputs->UlxUlt) && (Outputs->Gear4) && (Outputs->Frequency >= f4400)) ? TRPRE_DDR5_2tCK : TRPRE_DDR5_1tCK;
      }
      break;

    default:
      RetVal = 0;
      break;
  }

  return RetVal;
}

/**
  This function returns the timing value for Write Preamble.

  @param[in]  MrcData - Pointer to MRC global data.

  @retval UINT8 - The delay in tCK.
**/
UINT8
MrcGetWpre (
  IN MrcParameters *const MrcData
  )
{
  UINT8 RetVal;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      RetVal = TWPRE_ALL_FREQ_LP4;
      break;

    case MRC_DDR_TYPE_DDR5:
      RetVal = TWPRE_ALL_FREQ_DDR5;
      break;

    default:
      RetVal = TWPRE_ALL_FREQ;
      break;
  }

  return RetVal;
}

/**
  This function looks up the SAGV MR sequence and delays for the memory being initialized.
  The values will be stored in the array pointers passed in sequential order.
  The function will use the Length parameter to ensure it does not overflow the array passed.

  @param[in]  MrcData - Pointer to global MRC data.
  @param[out] MrSeq   - Pointer to array storing MR addresses.
  @param[out] MrDelay - Pointer to array storing MR delay after write.
  @param[in, out] Length - Pointer containing the number of entries available in the array. Will be updated to the
                           number of entries used upon return.
  @param[out] MrPerRank  - Output pointer to an array containing a list of MRs that must be configured on a per-rank basis
                           due to possible unique values per rank. The is terminated using the value mrEndOfSequence

  @retval MrcStatus - mrcSuccess if no errors
  @retval MrcStatus - mrcUnsupportedTechnology if the memory type is not supported.
  @retval MrcStatus - The return value of MrcSagvMrSeq per technology function.
**/
MrcStatus
MrcGetSagvMrSeq (
  IN      MrcParameters *const  MrcData,
  OUT     MrcModeRegister       *MrSeq,
  OUT     GmfTimingIndex        *MrDelay,
  IN OUT  UINT32                *Length,
  OUT     MrcModeRegister       **MrPerRank OPTIONAL
  )
{
  MrcStatus Status;

  Status = mrcSuccess;
  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR5:
      Status = MrcSagvMrSeqLpddr5 (MrcData, MrSeq, MrDelay, Length, MrPerRank);
      break;

    default:
      MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "%s %s\n", gErrString, gUnsupportedTechnology);
      Length = 0;
      Status = mrcUnsupportedTechnology;
      if (MrPerRank != NULL) {
        *MrPerRank = NULL;
      }
      break;
  }

  return Status;
}

/**
  This function looks up the delay timing associated with the input TimingIndex.
  If no timing is associated with the TimingIndex then a value of 0 is returned on
  TimingNckOut pointer.

  @param[in]  MrcData      - Pointer to global MRC data.
  @param[out] TimingIndex  - The delay index to look up
  @param[out] TimingNckOut - Output pointer for the delay timing associated with the input DelayIndex in tCK units

  @retval MrcStatus - mrcSuccess if no errors
**/
MrcStatus
MrcGetGmfDelayTiming (
  IN      MrcParameters *const  MrcData,
  IN      GmfTimingIndex        TimingIndex,
  OUT     UINT16                *TimingNckOut
  )
{
  MrcStatus Status;

  Status = mrcSuccess;
  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR5:
      Status = Lpddr5GmfDelayTimings (MrcData, TimingIndex, TimingNckOut);
      break;

    case MRC_DDR_TYPE_DDR5:
      Status = Ddr5GmfDelayTimings (MrcData, TimingIndex, TimingNckOut);
      break;

    default:
      Status = mrcUnsupportedTechnology;
      MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "%s %s\n", gErrString, gUnsupportedTechnology);
      break;
  }

  return Status;
}

/**
  This function will return the JEDEC parameter tDQS2DQ Min or Max based on the
  populated memory type.

  @param[in]  MrcData - Pointer to MRC global data.
  @param[in]  IsMin   - TRUE returns the minimum value, FALSE returns the maximum value.
  @param[out] Tdqs2dq - Returned parameter value in Femtoseconds.  If the memory technology does not support the parameter, 0 is returned.

  @retval MrcStatus - mrcSuccess if the API supports the memory type; mrcDimmNoSupported if the API does not support the memory type.
**/
MrcStatus
MrcGetTdqs2dq (
  IN  MrcParameters *const  MrcData,
  IN  const BOOLEAN         IsMin,
  OUT UINT32                *Tdqs2dq
  )
{
  MrcStatus Status;
  UINT32    lTdqs2dq;

  Status    = mrcSuccess;
  lTdqs2dq  = 0;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      lTdqs2dq = Lpddr4GetTdqs2dq (IsMin);
      break;

    case MRC_DDR_TYPE_DDR4:
      break;

    default:
      Status  = mrcDimmNotSupport;
      break;
  }

  if (Tdqs2dq != NULL) {
    *Tdqs2dq  = lTdqs2dq;
  }

  return Status;
}

/**
  This function will return the center of the range of the JEDEC timing parameter tDQS2DQ based on the
  populated memory type.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[out] Tdqs2dqCenter - Returned parameter value in Femptoseconds.  If the memory technology does not suppor the parameter, 0 is returned.

  @retval MrcStatus - mrcSuccess if the API supports the memory type; mrcDimmNoSupported if the API does not support the memory type.
**/
MrcStatus
MrcGetTdqs2dqCenter (
  IN  MrcParameters *const  MrcData,
  OUT UINT32                *Tdqs2dqCenter
  )
{
  MrcStatus Status;
  UINT32    lTdqs2dqMax;
  UINT32    lTdqs2dqMin;
  UINT32    lTdqs2dqCenter;

  Status          = mrcSuccess;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      lTdqs2dqMin = Lpddr4GetTdqs2dq (TRUE);
      lTdqs2dqMax = Lpddr4GetTdqs2dq (FALSE);
      break;

    case MRC_DDR_TYPE_DDR5:
      lTdqs2dqMin = Ddr5GetTdqs2dq (MrcData, TRUE);
      lTdqs2dqMax = Ddr5GetTdqs2dq (MrcData, FALSE);
      break;

    case MRC_DDR_TYPE_DDR4:
    default:
      lTdqs2dqMin = 0;
      lTdqs2dqMax = 0;
      Status  = mrcDimmNotSupport;
      break;
  }

  lTdqs2dqCenter = (lTdqs2dqMax + lTdqs2dqMin) / 2;

  if (Tdqs2dqCenter != NULL) {
    *Tdqs2dqCenter  = lTdqs2dqCenter;
  }

  return Status;
}

/**
  This function will return the JEDEC parameter tDQSCK MIN based on the
  populated memory type.

  @param[in]   MrcData   - Pointer to MRC global data.
  @param[out]  TdqsCkPs  - If TdqsCk is not present on the current memory technology, 0 is returned.

  @retval MrcStatus - mrcSuccess if the API supports the memory type; mrcDimmNoSupported if the API does not support the memory type.
**/
MrcStatus
MrcGetTdqsckMin (
  IN  MrcParameters *const  MrcData,
  OUT UINT32                *TdqsCkPs
  )
{
  MrcStatus   Status;
  UINT32      TdqsckMin;

  Status    = mrcSuccess;
  TdqsckMin = 0;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      TdqsckMin = tDQSCK_MIN_LP4;
      break;

    case MRC_DDR_TYPE_DDR4:
    case MRC_DDR_TYPE_LPDDR5:
      // TdqsckMin is initialized to 0.  Break the switch without an error message.
      break;

    default:
      Status = mrcDimmNotSupport;
      break;
  }

  if (TdqsCkPs != NULL) {
    *TdqsCkPs = TdqsckMin;
  }

  return Status;
}

/**
  This function returns tWCKPRE_Static for both Writes and Reads

  @param[in]  MrcData   - Pointer to MRC global data.

  @retval INT8 - Timing in tCK, or 0 if unsupported for the DDR type.
**/
UINT8
MrcGetWckPreStatic (
  IN  MrcParameters *const  MrcData
  )
{
  MrcOutput *Outputs;
  UINT8     RetVal;

  Outputs = &MrcData->Outputs;

  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_LPDDR5:
      RetVal = MrcGetWckPreStaticLpddr5 (Outputs->Frequency);
      break;

    default:
      RetVal = 0;
      break;
  }

  return RetVal;
}

/**
  This function returns tWCKENL_FS for the current DRAM configuration.

  @param[in]  MrcData - Pointer to MRC global data.

  @retval UINT8 - Timing in tCK, or 0 if unsupported.
**/
UINT8
MrcGetWckEnlFs (
  IN  MrcParameters *const  MrcData
  )
{
  MrcOutput *Outputs;
  UINT8     RetVal;

  Outputs = &MrcData->Outputs;

  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_LPDDR5:
      RetVal = MrcGetWckEnlFsLpddr5 (Outputs->Frequency);
      break;

    default:
      RetVal = 0;
      break;
  }

  return RetVal;
}

/**
  This function returns tCKENL_WR for the current DRAM configuration.

  @param[in]  MrcData   - Pointer to MRC global data.

  @retval UINT8 - Timing in tCK, or 0 if unsupported for the DDR type.
**/
UINT8
MrcGetWckPreWrTotal (
  IN  MrcParameters *const  MrcData
  )
{
  MrcOutput *Outputs;
  UINT8     RetVal;

  Outputs = &MrcData->Outputs;

  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_LPDDR5:
      RetVal = MrcGetWckPreWrTotalLpddr5 (Outputs->Frequency);
      break;

    default:
      RetVal = 0;
      break;
  }

  return RetVal;
}

/**
  This function returns tWCKPRE_total_RD for the current DRAM configuration.

  @param[in]  MrcData - Pointer to MRC global data.

  @retval UINT8 - Timing in tCK, or 0 if unsupported for the DDR type.
**/
UINT8
MrcGetWckPreRdTotal (
  IN  MrcParameters *const  MrcData
  )
{
  MrcOutput *Outputs;
  UINT8     RetVal;

  Outputs = &MrcData->Outputs;

  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_LPDDR5:

      RetVal = MrcGetWckPreRdTotalLpddr5 (Outputs->Frequency);
      break;

    default:
      RetVal = 0;
      break;
  }

  return RetVal;
}

/**
  This function determines if the SOC ODT requested is valid for the memory technology.
  If, the requested ODT is not supported, it will return the the closest matching ODT.
  If there is no limitation by the memory technology or the ODT value is supported,
  it will return the requested ODT.

  @param[in]  MrcData - Pointer to the global MRC data
  @param[in]  SocOdt  - The ODT requested by the system

  @retval - Valid ODT value.
**/
UINT16
MrcCheckForSocOdtEnc (
  IN  MrcParameters *const  MrcData,
  IN  UINT16                SocOdt
  )
{
  UINT16        SocOdtRet;
  const BOOLEAN *PuCalSocOdtValid;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      if (MrcData->Outputs.Lp4x) {
        PuCalSocOdtValid = &PuCalSocOdtValidLp4x[Lp4xPuCal0_5][0];
      } else {
        PuCalSocOdtValid = &PuCalSocOdtValidLp4[Lp4PuCal2_5][0];
      }
      SocOdtRet = MrcCheckSocOdtLpddr (MrcData, Lp4RzqValues, LP4_RZQ_NUM_VALUES, PuCalSocOdtValid, SocOdt);
      break;

    case MRC_DDR_TYPE_LPDDR5:
      SocOdtRet = MrcCheckSocOdtLpddr (MrcData, Lp5RzqValues, LP5_RZQ_NUM_VALUES, &PuCalSocOdtValidLp5[0], SocOdt);
      break;

    default:
      SocOdtRet = SocOdt;
  }

  return SocOdtRet;
}

/**
  This function returns the Command/Address bus width per technology type.

  @param[in]  MrcData   - Pointer to global MRC data.
  @param[out] BusWidth  - Return variable for the bus width.  0 is return if the technology is not supported.

  @retval MrcStatus - mrcSuccess if the API supports the memory type; mrcDimmNoSupported if the API does not support the memory type.
**/
MrcStatus
MrcGetCmdBusWidth (
  IN  MrcParameters *const  MrcData,
  OUT UINT8                 *BusWidth
  )
{
  MrcStatus   Status;
  UINT8       lBusWidth;

  Status  = mrcSuccess;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      lBusWidth = 6;
      break;

    case MRC_DDR_TYPE_LPDDR5:
      lBusWidth = 7;
      break;

    case MRC_DDR_TYPE_DDR4:
      lBusWidth = 22;
      break;

    default:
      lBusWidth = 0;
      Status    = mrcDimmNotSupport;
      break;
  }

  if (BusWidth != NULL) {
    *BusWidth = lBusWidth;
  }

  return Status;
}

/**
  This function handles calling the correct JEDEC Reset sequence based on DDR Technology.

  @param[in]  MrcData - Pointer to MRC global data.

  @retval MrcStatus
**/
MrcStatus
MrcJedecReset (
  IN  MrcParameters *MrcData
  )
{
  MrcStatus Status;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
    case MRC_DDR_TYPE_LPDDR5:
      Status = MrcJedecResetLpddr (MrcData);
      break;

    case MRC_DDR_TYPE_DDR4:
      MrcJedecResetDdr4 (MrcData);
      Status = mrcSuccess;
      break;

    case MRC_DDR_TYPE_DDR5:
      Status = MrcJedecResetDdr5 (MrcData);
      break;

    default:
      Status = mrcUnsupportedTechnology;
      break;
  }

  return Status;
}

/**
  This function handles calling the correct JEDEC Init sequence based on DDR Technology.

  @param[in]  MrcData - Pointer to MRC global data.

  @retval MrcStatus
**/
MrcStatus
MrcJedecInit (
  IN  MrcParameters *MrcData
  )
{
  MrcStatus Status;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      Status = MrcJedecInitLpddr4 (MrcData);
      break;

    case MRC_DDR_TYPE_LPDDR5:
      Status = MrcJedecInitLpddr5 (MrcData);
      break;

    case MRC_DDR_TYPE_DDR4:
      Status = MrcJedecInitDdr4 (MrcData);
      break;

    case MRC_DDR_TYPE_DDR5:
      Status = MrcJedecInitDdr5 (MrcData);
      break;

    default:
      Status = mrcUnsupportedTechnology;
      break;
  }

  return Status;
}

/**
  Used to update VrefDQ for DDR4/LPDDR4 or VrefCA for LPDDR4.
  Uses input offset value to increment/decrement current setting.

  @param[in,out] MrcData             - Include all MRC global data.
  @param[in]     Controller          - Selecting which Controller to talk to.
  @param[in]     Channel             - Selecting which Channel to talk to.
  @param[in]     RankMask            - Selecting which Ranks to talk to.
  @param[in]     DeviceMask          - Selecting which Devices to talk to (only valid for DDR4 and adjusting VrefDQ).
  @param[in]     VrefType            - Determines the Vref type to change, only CmdV and TxVref are valid.
  @param[in]     Offset              - Vref offset value.
  @param[in]     UpdateMrcData       - Used to decide if Mrc host must be updated.
  @param[in]     PdaMode             - Selecting to use MRH or old method for MRS (only valid for DDR4 and adjusting VrefDQ).
  @param[in]     IsCachedOffsetParam - Determines if the paramter is an offset (relative to cache) or absolute value.

  @retval Nothing.
**/
void
MrcSetDramVref (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Controller,
  IN     UINT8                Channel,
  IN     UINT8                RankMask,
  IN     UINT16               DeviceMask,
  IN     UINT8                VrefType,
  IN     INT32                Offset,
  IN     BOOLEAN              UpdateMrcData,
  IN     BOOLEAN              PdaMode,
  IN     BOOLEAN              IsCachedOffsetParam
  )
{
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  MrcStatus        Status;

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;

  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      Status = Lpddr4SetDramVref (MrcData, Controller, Channel, RankMask, VrefType, Offset, UpdateMrcData, IsCachedOffsetParam);
      break;

    case MRC_DDR_TYPE_DDR4:
      Status = Ddr4SetDramVref (MrcData, Controller, Channel, RankMask, DeviceMask, VrefType, Offset, UpdateMrcData, PdaMode, IsCachedOffsetParam);
      break;

    case MRC_DDR_TYPE_LPDDR5:
      Status = Lpddr5SetDramVref (MrcData, Controller, Channel, RankMask, VrefType, Offset, UpdateMrcData, IsCachedOffsetParam);
      break;

    case MRC_DDR_TYPE_DDR5:
      Status = Ddr5SetDramVref (MrcData, Controller, Channel, RankMask, DeviceMask, VrefType, Offset, UpdateMrcData, PdaMode, IsCachedOffsetParam);
      break;

    default:
      Status = mrcWrongInputParameter;
      MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_ERROR, "%s ", gUnsupportedTechnology);
      break;
    }

  MRC_DEBUG_ASSERT(Status == mrcSuccess, Debug, "Error Setting DIMM VREF");

}

/**
  Program LPDDR4 FSP-OP and FSP-WR values.
  It will set the ranks specified in the rank mask in all Channels and Controllers populated.
  NOTE:
        Encoding for Vrcg MR field is the same for LPDDR4 and LPDDR5.
          So we only need to pass through the external type defined in MrcMemoryApi.h.
          If this changes, then this function will need to translate between MRC enumeration to MR enumeration.
        FSP points for LPDDR4 and LPDDR5 are integer points.  LPDDR4 supports 2 points, and LPDDR5 supports 3 points.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  RankMask    - Bit mask of Rank's to configure.
  @param[in]  VrcgMode    - VREF Current Generator mode switch.
  @param[in]  FspWePoint  - FSP-WR. Valid Values: LP4 - 0:1; LP5 - 0:2
  @param[in]  FspOpPoint  - FSP-OP. Valid Values: LP4 - 0:1; LP5 - 0:2

  @retval MrcStatus - mrcSuccess if successful, else an error status.
**/
MrcStatus
MrcSetFspVrcg (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 RankMask,
  IN  UINT8                 VrcgMode,
  IN  UINT8                 FspWePoint,
  IN  UINT8                 FspOpPoint
  )
{
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  MrcStatus     Status;
  MrcDdrType    DdrType;
  UINT32        Controller;
  UINT32        Channel;
  UINT32        Address;
  UINT32        MrIndex;
  UINT32        Delay;
  UINT16        *MrPtr;
  UINT8         Rank;
  BOOLEAN       Lpddr5;

  Outputs = &MrcData->Outputs;
  DdrType = Outputs->DdrType;

  Lpddr5  = (DdrType == MRC_DDR_TYPE_LPDDR5);
  if (DdrType == MRC_DDR_TYPE_LPDDR4) {
    Address = mrMR13;
    MrIndex = mrIndexMR13;
  } else if (Lpddr5) {
    Address = mrMR16;
    MrIndex = mrIndexMR16;
  } else {
    return mrcUnsupportedTechnology;
  }
  Status  = mrcSuccess;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if ((!MrcRankExist (MrcData, Controller, Channel, Rank)) || ((RankMask & (1 << Rank)) == 0)) {
          continue;
        }
        MrPtr = ChannelOut->Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR;

        if (Lpddr5) {
          MrcLpddr5SetMr16 (MrcData, FspWePoint, FspOpPoint, MRC_IGNORE_ARG_8, VrcgMode, MRC_IGNORE_ARG_8, &MrPtr[mrIndexMR16]);
        } else {
          MrcLpddr4SetMr13 (MrcData, MRC_IGNORE_ARG_8, MRC_IGNORE_ARG_8, VrcgMode, MRC_IGNORE_ARG_8, FspWePoint, FspOpPoint, &MrPtr[mrIndexMR13]);
        }

        Status = MrcIssueMrw (MrcData, Controller, Channel, Rank, Address, MrPtr[MrIndex], TRUE);
        if (Status != mrcSuccess) {
          return Status;
        }
      } // for Rank
    } // for Channel
  } // for Controller

  if (FspOpPoint != MRC_IGNORE_ARG_8) {
    // When FSP-OP is changed, wait tFC_Long = 250ns
    // This covers for tVRCG_ENABLE/tVRCG_DISABLE as well if needed
    Delay = MRC_LP_tFC_LONG_NS;
    if (Lpddr5) {
      // 4.16.1 Frequency Set Point Update Timing
      // Table 82 - AC Timing Table
      Delay += Outputs->tCKps / 2;
    }
    MrcWait (MrcData, DIVIDECEIL (Delay, MRC_TIMER_1NS));
  } else if (VrcgMode != MRC_IGNORE_ARG_8) {
    // When VRCG mode is changed, wait tVRCG_ENABLE or tVRCG_DISABLE
    if (Lpddr5) {
      Delay = (VrcgMode == LpVrcgNormal) ? MRC_LP5_tVRCG_DISABLE_NS : MRC_LP5_tVRCG_ENABLE_NS;
    } else {
      Delay = (VrcgMode == LpVrcgNormal) ? MRC_LP4_tVRCG_DISABLE_NS : MRC_LP4_tVRCG_ENABLE_NS;
    }
    MrcWait (MrcData, DIVIDECEIL (Delay, MRC_TIMER_1NS));
  }

  return Status;
}

/**
GetDimmParamValue is responsible for performing the concrete get DIMM parameter to value
1. gettig MR name and MR index
2. Read value from cache
3. make dump register from the value read from cache
4. Read the need field from the dump register

**/
MrcStatus
GetDimmParamValue(
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                OptParam,
  IN OUT UINT16*              ParamValue
)
{
  MrcOutput      *Outputs;
  //MrcInput       *Inputs;
  MrcChannelOut  *ChannelOut;
  UINT16         *MrReg;
  UINT16         MrRegVal;
  MrcStatus      Status;
  UINT8          Rank;
  UINT8          Channel;
  UINT8          MrIndex;
  UINT8          MrNum;
  //UINT16         RankValues[MAX_RANK_IN_CHANNEL];

  //override func arguments to return only channel 0 rank 0 data
  Channel = 0;
  Rank = 0;
  //const MRC_FUNCTION  *MrcCall;

  Outputs = &MrcData->Outputs;
  //Inputs = &MrcData->Inputs;
  //MrcCall = Inputs->Call.Func;
  ChannelOut = &Outputs->Controller[0].Channel[Channel];
  //MrcCall->MrcSetMem((UINT8 *)RankValues, sizeof(RankValues), 0);


  //1 getting MR index and name
  Status = GetOptDimmParamMrIndex(MrcData, OptParam, &MrIndex, &MrNum);
  if (Status != mrcSuccess) {
    return Status;
  }
  //2 read MR value from cache
  MrReg = &ChannelOut->Dimm[Rank / 2].Rank[(Rank % 2)].MR[mrMR0];
  MrRegVal = MrReg[MrIndex];

  //3 make reg from struct
  switch (Outputs->DdrType) {

  //case MRC_DDR_TYPE_LPDDR4:
  //  Status = Lpddr4GetDimmParamValue(MrcData, &MrRegVal, OptParam, ParamValue);
  //  break;

  //case MRC_DDR_TYPE_DDR4:
  //  Status = Ddr4GetDimmParamValue(MrcData, &MrRegVal, OptParam, ParamValue);
  //  break;

  case MRC_DDR_TYPE_DDR5:
    Status = Ddr5GetDimmParamValue (MrcData, &MrRegVal, OptParam, ParamValue);
    break;

  default:
    Status = mrcWrongInputParameter;
    MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_ERROR, "%s ", gUnsupportedTechnology);
    break;
  }
  return Status;
}

/**
  SetDimmParamValue is responsible for performing the concrete set DIMM parameter to value, using
  Per-Technology SetDimmParamValue functions.
  Parameters supported: OptDimmRon, OptDimmOdtWr, OptDimmOdtPark, OptDimmOdtNom.
  Function performs the following stages:
  1. Reads cached value.
  2. Programs new value to cache.
  3. Programs MRs via MRH.

  Note Shadow register are defined per Channel. If there is any variation between ranks it will be lost.

  @param[in,out] MrcData    - Include all MRC global data.
  @param[in]     Controller   Controller index to work.
  @param[in]     Channel    - Channel index to work on.
  @param[in]     Ranks      - Rank mask to be applied to.
  @param[in]     OptParam   - Defines the OptParam Offsets.
  @param[in]     ParamValue - The actual values (Typically in Ohms)
  @param[in]     UpdateHost - Decides if MrcData has to be updated

  @retval MrcStatus - mrcWrongInputParameter if unsupported Technology
                    - MrcStatus of the Technology specific SetDimmParamValue functions

**/
MrcStatus
SetDimmParamValue (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Controller,
  IN     UINT8                Channel,
  IN     UINT8                Ranks,
  IN     UINT8                OptParam,
  IN     UINT16               ParamValue,
  IN     UINT8                UpdateHost
  )
{

  MrcOutput      *Outputs;
  MrcInput       *Inputs;
  MrcChannelOut  *ChannelOut;
  UINT16         *MrReg;
  MrcStatus      Status;
  MrcDdrType     DdrType;
  UINT16         MrRegVal;
  UINT8          Rank;
  UINT8          DdrRank;
  UINT8          MrIndex;
  UINT8          MrNum;
  UINT16         RankValues[MAX_RANK_IN_CHANNEL];
  const MRC_FUNCTION  *MrcCall;

  Outputs     = &MrcData->Outputs;
  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  ChannelOut  = &Outputs->Controller[Controller].Channel[Channel];
  DdrType     = Outputs->DdrType;
  MrcCall->MrcSetMem((UINT8 *)RankValues, sizeof(RankValues), 0);

  Status = GetOptDimmParamMrIndex (MrcData, OptParam, &MrIndex, &MrNum);
  if (Status != mrcSuccess) {
    return Status;
  }

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if ((!MrcRankExist (MrcData, Controller, Channel, Rank)) || ((Ranks &  (MRC_BIT0 << Rank)) == 0)) {
      continue;
    }
    // Extract MR value from host struct
    DdrRank = (Outputs->Lpddr) ? dDIMM0 : (Rank / MAX_RANK_IN_DIMM);
    MrReg = &ChannelOut->Dimm[DdrRank].Rank[(Rank % MAX_RANK_IN_DIMM)].MR[mrMR0];
    MrRegVal = MrReg[MrIndex];
    switch (DdrType) {
      case MRC_DDR_TYPE_LPDDR4:
        Status = Lpddr4SetDimmParamValue (MrcData, &MrRegVal, OptParam, ParamValue);
        break;

      case MRC_DDR_TYPE_DDR4:
        Status = Ddr4SetDimmParamValue (MrcData, &MrRegVal, OptParam, ParamValue);
        break;

      case MRC_DDR_TYPE_DDR5:
        Status = Ddr5SetDimmParamValue (MrcData, &MrRegVal, Controller, Channel, Rank, OptParam, ParamValue, UpdateHost);
        break;

      default:
        Status = mrcWrongInputParameter;
        MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "%s ", gUnsupportedTechnology);
        break;
    }
    if (Status != mrcSuccess) {
      // Skip MR Write in case of incorrect MR Set
      return Status;
    }

    // Program the corresponding value to the MR
    RankValues[Rank] = MrRegVal;
    if (UpdateHost && (DdrType != MRC_DDR_TYPE_DDR5)) { // DDR5 is updating host MR value inside Ddr5SetDimmParamValue, and MrRegVal is not updated
      MrReg[MrIndex] = MrRegVal;
    }
  }// Rank

  // Perform MRH operation for LPDDR4 - Send MRW command. For DDR4 MRS Command
  switch (DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
    case MRC_DDR_TYPE_LPDDR5:
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if ((!MrcRankExist (MrcData, Controller, Channel, Rank)) || ((Ranks & (1 << Rank)) == 0)) {
          continue;
        }
        Status = MrcIssueMrw (MrcData, Controller, Channel, Rank, MrNum, RankValues[Rank], TRUE);
        if (Status != mrcSuccess) {
          // Skip next writes upon failure
          return Status;
        }
      } // rank
      break;

    case MRC_DDR_TYPE_DDR4:
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if ((!MrcRankExist (MrcData, Controller, Channel, Rank)) || ((Ranks & (1 << Rank)) == 0)) {
          continue;
        }
        Status = MrcWriteMRS (MrcData, Controller, Channel, 1 << Rank, MrNum, RankValues[Rank]);
        if (Status != mrcSuccess) {
          return Status;  // Skip next writes upon failure
        }
      } // rank
      break;

    case MRC_DDR_TYPE_DDR5:
      // Current implementation performs access from inside Ddr5SetDimmParamValue
      break;

    default:
      Status = mrcWrongInputParameter;
      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "%s ", gUnsupportedTechnology);
      break;
  }

  return Status;
}

/**
  GetOptDimmParamMrIndex using Technology specific GetOptDimmParamMrIndex functions

  @param[in]  MrcData   - Include all MRC global data.
  @param[in]  OptParam  - The Dimm Opt Param (e.g., OptDimmRon, OptDimmOdtWr, OptDimmOdtPark, OptDimmOdtNom)
  @param[out] *MrIndex  - Updated Pointer to the MR index.
  @param[out] *MrNum    - Updated Pointer to the MR number.

  @retval MrcStatus - mrcWrongInputParameter if unsupported Technology
                      Status of the Technology specific functions otherwise
**/

MrcStatus
GetOptDimmParamMrIndex (
  IN MrcParameters *const MrcData,
  IN UINT8                OptDimmParam,
  OUT UINT8               *MrIndex,
  OUT UINT8               *MrNum
  )
{
  MrcOutput  *Outputs;
  MrcDebug   *Debug;
  MrcStatus  Status;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  switch (Outputs->DdrType) {

    case MRC_DDR_TYPE_LPDDR4:
      Status = Lpddr4GetOptDimmParamMrIndex (MrcData, OptDimmParam, MrIndex, MrNum);
      break;

    case MRC_DDR_TYPE_LPDDR5:
      Status = Lpddr5GetOptDimmParamMrIndex (MrcData, OptDimmParam, MrIndex, MrNum);
      break;

    case MRC_DDR_TYPE_DDR4:
      Status = Ddr4GetOptDimmParamMrIndex (MrcData, OptDimmParam, MrIndex, MrNum);
      break;

    case MRC_DDR_TYPE_DDR5:
      Status = Ddr5GetOptDimmParamMrIndex (MrcData, OptDimmParam, MrIndex, MrNum);
      break;

    default:
      Status = mrcWrongInputParameter;
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "%s ", gUnsupportedTechnology);
  }

  return Status;
}

/**
  Get an array and the amount of the corresponding possible DIMM OptParam values available per memory technology.
  Supported DimmOptParams: OptDimmOdtWr, OptDimmOdtNom, OptDimmOdtPark, OptDimmRon

  @param[in]  MrcData             - Include all MRC global data.
  @param[in]  DimmOptParam        - OptDimmOdtWr, OptDimmOdtNom, OptDimmOdtPark, OptDimmRon
  @param[out] DimmOptParamVals    - Pointer to the array of values.
  @param[out] NumDimmOptParamVals - Pointer to the number of values.

  @retval MrcStatus - mrcWrongInputParameter if unsupported Technology
                      Status of the Technology specific functions otherwise
**/
MrcStatus
GetDimmOptParamValues (
  IN MrcParameters  *const MrcData,
  IN UINT8          DimmOptParam,
  OUT UINT16        **DimmOptParamVals,
  OUT UINT8         *NumDimmOptParamVals
  )

{
  MrcOutput  *Outputs;
  MrcStatus  Status;
  Outputs = &MrcData->Outputs;

  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_LPDDR4:
      Status = Lpddr4GetDimmOptParamValues (MrcData, DimmOptParam, DimmOptParamVals, NumDimmOptParamVals);
      break;

    case MRC_DDR_TYPE_LPDDR5:
      Status = Lpddr5GetDimmOptParamValues (MrcData, DimmOptParam, DimmOptParamVals, NumDimmOptParamVals);
      break;

    case MRC_DDR_TYPE_DDR5:
      Status = Ddr5GetDimmOptParamValues (MrcData, DimmOptParam, DimmOptParamVals, NumDimmOptParamVals);
      break;

    default:
    case MRC_DDR_TYPE_DDR4:
      Status = Ddr4GetDimmOptParamValues (MrcData, DimmOptParam, DimmOptParamVals, NumDimmOptParamVals);
      break;
  }

  return Status;
}

/**
  Determine the ODT table for the given Controller / Channel / DIMM.

  @param[in]     MrcData         - Include all the mrc global data.
  @param[in]     Controller      - Controller to work on.
  @param[in]     Channel         - Channel to work on.
  @param[in]     Dimm            - DIMM to work on.
  @param[in out] OdtTable        - ODT structure - or (TOdtValueDdr4 *) or (TOdtValueLpddr *)

  @retval MrcStatus - mrcSuccess             otherwise
**/
MrcStatus
GetOdtTableIndex (
  IN MrcParameters *const MrcData,
  IN const UINT32         Controller,
  IN const UINT32         Channel,
  IN const UINT32         Dimm,
  IN OUT   void           *OdtTable
  )
{
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  void                *OdtTableRet;
  MrcChannelOut       *ChannelOut;
  MrcDimmOut          *DimmOut;
  TOdtIndex           OdtIndex;
  MrcDdrType          DdrType;
  MrcStatus           Status;
  TOdtValueLpddr      OdtLpddr;
  UINT8               RanksInDimm0;
  UINT8               RanksInDimm1;
  UINT8               LpddrRttWr;
  UINT8               LpddrRttCa;
  BOOLEAN             Ddr4;
  BOOLEAN             Ddr5;
  BOOLEAN             Lpddr4;
  BOOLEAN             Lpddr5;

  Inputs       = &MrcData->Inputs;
  Outputs      = &MrcData->Outputs;
  Debug        = &Outputs->Debug;
  MrcCall      = Inputs->Call.Func;
  ChannelOut   = &Outputs->Controller[Controller].Channel[Channel];
  DdrType      = Outputs->DdrType;
  Ddr4         = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5         = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr4       = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5       = (DdrType == MRC_DDR_TYPE_LPDDR5);
  DimmOut      = &ChannelOut->Dimm[dDIMM0];
  RanksInDimm0 = DimmOut[dDIMM0].RankInDimm;
  RanksInDimm1 = DimmOut[dDIMM1].RankInDimm;
  OdtIndex     = oiNotValid;
  OdtTableRet  = NULL;
  LpddrRttWr   = Inputs->LpddrRttWr;
  LpddrRttCa   = Inputs->LpddrRttCa;
  Status = mrcSuccess;

  if (OdtTable == NULL) {
    return mrcFail;
  }

  switch (ChannelOut->DimmCount) {
    case 2: // 2DPC
      if (Lpddr4 || Lpddr5) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s: LPDDR 2DPC Not Supported\n", gErrString);
      } else {
        if ((RanksInDimm0 == 1) && (RanksInDimm1 == 1)) {
          OdtIndex = oi2DPC1R1R;
        } else if ((RanksInDimm0 == 1) && (RanksInDimm1 == 2)) {
          OdtIndex = oi2DPC1R2R;
        } else if ((RanksInDimm0 == 2) && (RanksInDimm1 == 1)) {
          OdtIndex = oi2DPC2R1R;
        } else if ((RanksInDimm0 == 2) && (RanksInDimm1 == 2)) {
          OdtIndex = oi2DPC2R2R;
        } else {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s: Invalid %uDPC rank mode\n", gErrString, 2);
        }
      }
      break;

    case 1: // 1DPC
      if ((RanksInDimm0 == 1) || ((RanksInDimm1 == 1) && (MAX_DIMMS_IN_CHANNEL > 1))) {
        OdtIndex = oi1DPC1R;
      } else if ((RanksInDimm0 == 2) || ((RanksInDimm1 == 2) && (MAX_DIMMS_IN_CHANNEL > 1))) {
        OdtIndex = oi1DPC2R;
      } else {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s: Invalid %uDPC rank mode\n", gErrString, 1);
      }
      break;

    default:
      break;
  }

  if (OdtIndex != oiNotValid) {
    // Retreive the appropriate ODT Table
    if (Ddr4) {
      OdtTableRet = SelectTable_DDR4 (MrcData, Dimm, OdtIndex);
    } else if (Ddr5) {
      OdtTableRet = SelectTable_DDR5 (MrcData, Dimm, OdtIndex);
    } else if (Lpddr4) {
      OdtTableRet = SelectTable_LPDDR4 (MrcData, Dimm, OdtIndex);
    } else if (Lpddr5) {
      OdtTableRet = SelectTable_LPDDR5 (MrcData, Dimm, OdtIndex);
    }

    // Check if the table is valid
    if (OdtTableRet == NULL) {
      return mrcFail;
    }

    // Copy the apropriate data to caller's pointer
    if (Ddr4) {
      MrcCall->MrcCopyMem (OdtTable, OdtTableRet, sizeof(TOdtValueDdr4));
    } else if (Ddr5) {
      MrcCall->MrcCopyMem (OdtTable, OdtTableRet, sizeof(TOdtValueDqDdr5));
    } else if (Lpddr4 || Lpddr5) {
      // Start with the POR table data
      MrcCall->MrcCopyMem ((UINT8 *)&OdtLpddr, (UINT8 *)OdtTableRet, sizeof(TOdtValueLpddr));
      // Fill in user specified data
      if (LpddrRttWr != 0) {
        OdtLpddr.RttWr = LpddrRttWr;
      }
      if (LpddrRttCa != 0) {
        OdtLpddr.RttCa = LpddrRttCa;
      }

      // Valid Rtt value check
      if (OdtEncode (OdtLpddr.RttWr) == -1) {
        MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "%s Invalid %s ODT RttWr Value %u\n", gErrString, gCmdString, OdtLpddr.RttWr);
        Status = mrcWrongInputParameter;
      }
      if (OdtEncode (OdtLpddr.RttCa) == -1) {
        MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "%s Invalid %s ODT RttCa Value %u\n", gErrString, gCmdString, OdtLpddr.RttCa);
        Status = mrcWrongInputParameter;
      }

      // Copy data to caller pointer
      MrcCall->MrcCopyMem ((UINT8 *)OdtTable, (UINT8 *)&OdtLpddr, sizeof(TOdtValueLpddr));
    }

  } else {
    Status = mrcFail;
  }

  return Status;
}

/**
  This function returns the tMRR value.

  @param[in] MrcData    - Include all MRC global data.
  @param[in] tCK        - DCLK period in femtoseconds.
  @param[in] tCL        - CAS Latency in tCK.

  @retval UINT32 - Timing in tCK.
**/
UINT32
tMRRGet (
  IN MrcParameters *const MrcData,
  IN const UINT32         tCK,
  IN const UINT32         tCL
  )
{
  UINT32      tMRR;
  MrcDdrType  DdrType;
  UINT32      BurstLength;

  DdrType     = MrcData->Outputs.DdrType;
  BurstLength = MrcData->Outputs.BurstLength;
  tMRR        = MRC_UINT32_MAX;

  if (DdrType == MRC_DDR_TYPE_DDR5) {
    // max(14ns, 16nCK)
    tMRR = DIVIDECEIL (tMRR_DDR5_FS, tCK);
    tMRR = MAX (tMRR, 16);
  } else if (DdrType == MRC_DDR_TYPE_LPDDR4) {
    tMRR = 8;
  } else if (DdrType == MRC_DDR_TYPE_LPDDR5) {
    // tMRR calculation for Lpddr5
    // tMRR = (RL + (BL/N_max) + RD (tWCKPST / tCK) + 2) * (Wck:Ck Ratio)
    tMRR = tCL + BurstLength + 2;
    tMRR *=4;
  } else {
    // MC register default
    tMRR = MC0_CH0_CR_TC_LPDDR4_MISC_tMRR_DEF;
  }

  return tMRR;
}

/**
  This function returns the tMRRMRW value.
  Minimum delay between MRR to MRW command.

  @param[in] MrcData    - Include all MRC global data.
  @param[in] tCL        - CAS Latency in tCK.

  @retval UINT32 - Timing in tCK.
**/
UINT32
tMRRMRWGet (
  IN MrcParameters *const MrcData,
  IN const UINT32         tCL
  )
{
  MrcOutput   *Outputs;
  MrcDdrType  DdrType;
  UINT32      BurstLength;
  UINT32      TckPs;
  UINT32      tMrrMrw;

  Outputs     = &MrcData->Outputs;
  DdrType     = Outputs->DdrType;
  BurstLength = Outputs->BurstLength;
  TckPs       = Outputs->tCKps;
  tMrrMrw     = MRC_UINT32_MAX;

  tMrrMrw = tCL;
  if (DdrType == MRC_DDR_TYPE_DDR5) {
    tMrrMrw += BurstLength + 1;
  } else if (DdrType == MRC_DDR_TYPE_LPDDR5) {
    tMrrMrw += BurstLength + DIVIDECEIL (MRC_LP5_tWCKDQO_MAX, TckPs) + 2;
    tMrrMrw *= 4;
  } else if (DdrType == MRC_DDR_TYPE_LPDDR4) {
    tMrrMrw += (BurstLength / 2) + DIVIDECEIL (tDQSCK_MAX_LP4, TckPs) + 3;
  } else {
    // MC register default
    tMrrMrw = MC0_CH0_CR_TC_LPDDR4_MISC_tMRRMRW_DEF;
  }

  return tMrrMrw;
}

/**
  This function returns the tOSCO value.

  @param[in] MrcData    - Include all MRC global data.
  @param[in] tCK        - tCK period in femtoseconds.
  @param[in] tCL        - CAS Latency in tCK.

  @retval UINT32 - Timing in tCK.
**/
UINT32
tOSCOGet (
  IN MrcParameters *const MrcData,
  IN const UINT32         tCK,
  IN const UINT32         tCL
  )
{
  MrcOutput   *Outputs;
  MrcDdrType  DdrType;
  UINT32      tOSCO;
  UINT32      MinVal;
  BOOLEAN     Lpddr;
  BOOLEAN     Lpddr5;

  Outputs     = &MrcData->Outputs;
  DdrType     = Outputs->DdrType;
  Lpddr       = Outputs->Lpddr;
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);

  if (Lpddr) {
    tOSCO = DIVIDECEIL (MRC_LP4_tOSCO_FS, tCK);
    MinVal = (Lpddr5) ? 4 : 8;
    tOSCO = MAX (tOSCO, MinVal);
    if (Lpddr5) {
      tOSCO *= 4;
    }
  } else {
    // DDR5 tOSCO = tMPC_Delay = tMRD = max(14ns, 16nCK)
    // DDR4 doesn't use tOSCO
    tOSCO = tMRRGet (MrcData, tCK, tCL);
  }

  return tOSCO;
}

/**
  This function returns the pda_dealy value

  @param[in] MrcData    - Include all MRC global data.

  @retval UINT32 - Timing in ns
**/
UINT32
MrcGetPdaDelay (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput     *Outputs;
  UINT32        TckPs;
  UINT32        BLns;
  UINT32        tpda_delay;

  Outputs = &MrcData->Outputs;
  TckPs  = Outputs->tCKps;
  BLns = DIVIDECEIL ((TckPs * Outputs->BurstLength), 1000);
  tpda_delay = 18 + BLns + 19;
  return tpda_delay;
}

/**
  This executes the necessary ACT and DQ override needed for PPR DRAM sequence, based on provided address

  @param[in] MrcData     - Pointer to MRC global data.
  @param[in] Controller  - Target Controller
  @param[in] Channel     - Target Channel
  @param[in] Rank        - Target Rank
  @param[in] Row         - Target Row for repair
  @param[in] BankGroup   - Target BankGroup for repair
  @param[in] BankAddress - Target BankAddress for repair
  @param[in] ByteMask    - Mask of Bytes for repair

  @retval MrcStatus
**/
MrcStatus
MrcPprActAndDqLow (
  IN MrcParameters* const MrcData,
  IN const UINT32         Controller,
  IN const UINT32         Channel,
  IN const UINT32         Rank,
  IN const UINT32         Row,
  IN const UINT32         BankGroup,
  IN const UINT32         BankAddress,
  IN const UINT16         ByteMask
  )
{
  MrcOutput           *Outputs        = &MrcData->Outputs;
  //MrcDebug            *Debug          = &Outputs->Debug;
  MrcStatus           Status          = mrcSuccess;
  UINT8               McChBitMask     = 0;
  UINT8               Byte;
  INT64               GetSetVal;
  INT64               GetSetDis;
  INT64               GetSetEn;
  INT64               GetSetValSave;
  INT64               tCWL4TxDqFifoRdEn;
  INT64               tCWL4TxDqFifoRdEnSave;
  UINT32              Offset;
  UINT32              GaloisPoly;
  UINT32              IpChannel;
  UINT32              PatternBuffer[3];
  BOOLEAN             Ddr5;
  UINT8               Cke[2];
  UINT8               Cs[2];
  UINT8               RankMask;
  UINT8               RankFeatureEnable = 0;
  MRC_CA_MAP_TYPE     CmdAddr;
  UINT64_STRUCT       CadbPatternChunks[CADB_20_MAX_CHUNKS];
  MRC_PATTERN_CTL     PatCtl;
  UINT8               CadbBufferIndex;
  UINT8               BaseRankMask;
  MC0_CH0_CR_SCHED_SECOND_CBIT_STRUCT            SchedSecondCBit;
  MC0_CH0_CR_SC_GS_CFG_STRUCT                    ScGsCfg;
  MC0_CH0_CR_SC_GS_CFG_STRUCT                    ScGsCfgSave;
  MC0_CH0_CR_CADB_CTL_STRUCT                     CadbCtl;
  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT                CpgcSeqCtl;
  MC0_CR_CPGC2_V_CHICKEN_STRUCT                  Cpgc2Chicken;
  MC0_CR_CPGC2_V_CHICKEN_STRUCT                  Cpgc2ChickenSave;
  MC0_REQ0_CR_CPGC_SEQ_CFG_B_STRUCT              CpgcSeqCfgB;
  MC0_REQ0_CR_CPGC_SEQ_CFG_B_STRUCT              CpgcSeqCfgBSave;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT       AlgoInstruct;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_STRUCT  AlgoInstructControl;
  MC0_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_STRUCT  AlgoWaitEventControl;
  MC0_CH0_CR_CADB_CFG_STRUCT                             CadbCfg;
  MC0_CH0_CR_CADB_AO_MRSCFG_STRUCT                       MrsConfig;
  MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_STRUCT            ReutChMiscRefreshCtrl;
  static const MRC_PG_UNISEQ_TYPE                        UniseqMode[3] = {MrcPgMuxLfsrGalois, MrcPgMuxLfsrGalois, MrcPgMuxLfsrGalois};
  static const MRC_PG_LFSR_TYPE                          CadbLfsrPoly[3] = {MrcLfsr16, MrcLfsr32, MrcLfsr31};

  GetSetEn  = 1;
  GetSetDis = 0;
  Ddr5 = Outputs->DdrType == MRC_DDR_TYPE_DDR5;
  McChBitMask = (1 << ((Controller * Outputs->MaxChannels) + Channel));
  BaseRankMask = (UINT8) ((1 << MAX_RANK_IN_CHANNEL) - 1);

  MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "Repairing Controller %d, Channel%d, Rank%d: McChBitMask = 0x%X\n", Controller, Channel, Rank, McChBitMask);

  if (Ddr5) {
    IpChannel = DDR5_IP_CH (Ddr5, Channel);
    MrcGetSetMc(MrcData, Controller, GsmMccEnableSr, ReadNoCache, &GetSetValSave);  // Save state of Self Refresh
    MrcGetSetMc(MrcData, Controller, GsmMccEnableSr, WriteNoCache, &GetSetDis);     // Disable Self Refresh
    MrcWait (MrcData, MRC_TIMER_1NS);
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockXarb, WriteNoCache, &GetSetEn);  // block XARB
    MrcWait (MrcData, MRC_TIMER_1NS);
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockXarb, WriteNoCache, &GetSetDis);  // Unblock XARB
    MrcWait (MrcData, MRC_TIMER_1NS);

    Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SCHED_SECOND_CBIT_REG, MC1_CH0_CR_SCHED_SECOND_CBIT_REG, Controller, MC0_CH1_CR_SCHED_SECOND_CBIT_REG, Channel);
    SchedSecondCBit.Data = MrcReadCR (MrcData, Offset);
    SchedSecondCBit.Bits.disable_starved_prio_on_new_req = 1;
    MrcWriteCR (MrcData, Offset, SchedSecondCBit.Data);

    MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, ReadNoCache, &tCWL4TxDqFifoRdEnSave);
    tCWL4TxDqFifoRdEn = tCWL4TxDqFifoRdEnSave + 2;
    MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, WriteToCache, &tCWL4TxDqFifoRdEn);
    MrcFlushRegisterCachedData (MrcData);

    // ------------- Config CPGC ----------------------
    SetupIOTestPPR (MrcData, Controller, Channel, Row, BankGroup, BankAddress, &ByteMask);

    GetSetVal = MRC_UINT8_MAX; // Overide all DQs in bytes not included in ByteMask
    for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
      if (((1 << Byte) & ByteMask) == 0) { // If a byte is not present in ByteMask, override DQs to "0xFF"
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDqOverrideData, WriteToCache, &GetSetVal);
      } else {
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDqOverrideData, WriteToCache, &GetSetDis);
      }
      MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDqOverrideEn, WriteToCache, &GetSetVal);
    }
    MrcFlushRegisterCachedData (MrcData);
    MrcSetCpgcBindMode (MrcData, FALSE, FALSE); // Disable Global Start/Stop

    Offset = OFFSET_CALC_CH (MC0_CR_CPGC2_V_CHICKEN_REG, MC1_CR_CPGC2_V_CHICKEN_REG, Controller);
    Cpgc2ChickenSave.Data = MrcReadCR (MrcData, Offset);
    Cpgc2Chicken.Data = Cpgc2ChickenSave.Data;
    Cpgc2Chicken.Bits.XPQ_CH_CREDIT = 16;
    MrcWriteCR (MrcData, Offset, Cpgc2Chicken.Data);

    Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_GS_CFG_REG, MC1_CH0_CR_SC_GS_CFG_REG, Controller, MC0_CH1_CR_SC_GS_CFG_REG, Channel);
    ScGsCfgSave.Data = MrcReadCR64 (MrcData, Offset);
    ScGsCfg.Data = ScGsCfgSave.Data;
    ScGsCfg.Bits.disable_tristate = 0;
    ScGsCfg.Bits.disable_ca_tristate = 0;
    ScGsCfg.Bits.disable_ck_tristate = 0;
    MrcWriteCR64 (MrcData, Offset, ScGsCfg.Data);

    Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC_SEQ_CFG_B_REG, MC1_REQ0_CR_CPGC_SEQ_CFG_B_REG, Controller, MC0_REQ1_CR_CPGC_SEQ_CFG_B_REG, Channel);
    CpgcSeqCfgBSave.Data = MrcReadCR (MrcData, Offset);
    CpgcSeqCfgB.Data = CpgcSeqCfgBSave.Data;
    CpgcSeqCfgB.Bits.START_DELAY = 100;
    MrcWriteCR (MrcData, Offset, CpgcSeqCfgB.Data);

    ReutChMiscRefreshCtrl.Data = 0;
    Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_REG, MC1_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_REG, Controller, MC0_CH1_CR_REUT_CH_MISC_REFRESH_CTRL_REG, Channel);
    MrcWriteCR (MrcData, Offset, ReutChMiscRefreshCtrl.Data);

    AlgoInstruct.Data = 0;
    AlgoInstruct.Bits.Last = 1;
    AlgoInstructControl.Data = 0;
    AlgoInstructControl.Bits.Select_On = 1;
    AlgoInstructControl.Bits.Deselect_On = 1;
    AlgoWaitEventControl.Data = 0;
    Cpgc20AlgoInstructWrite (MrcData, &AlgoInstruct, AlgoInstructControl, AlgoWaitEventControl, 0x1);

    Cadb20UniseqCfg (MrcData, UniseqMode, CadbLfsrPoly);
    PatCtl.DQPat = StaticPattern;
    MrcInitUnisequencer (MrcData, MrcLfsr8, &PatCtl, 0, 3);

    CadbCfg.Data = 0;
    CadbCfg.Bits.CADB_MODE                = 3; // Normal Select Triggered Deselect
    CadbCfg.Bits.INITIAL_DSEL_SSEQ_EN     = 1;
    CadbCfg.Bits.INITIAL_DSEL_EN          = 0;
    CadbCfg.Bits.CADB_TO_CPGC_BIND        = 1;
    CadbCfg.Bits.CADB_SEL_THROTTLE_MODE   = 1;
    CadbCfg.Bits.CADB_DSEL_THROTTLE_MODE  = 1;
    CadbCfg.Bits.LANE_DESELECT_EN         = 0x7;
    CadbCfg.Bits.CMD_DESELECT_START       = 0x5;
    CadbCfg.Bits.CMD_DESELECT_STOP        = 0;
    Cadb20ConfigRegWrite (MrcData, CadbCfg);

    MrsConfig.Data = 0;
    MrsConfig.Bits.MRS_AO_REPEATS = 0x15;
    MrsConfig.Bits.MRS_GAP = 0x9;
    Cadb20MrsConfigRegWrite (MrcData, MrsConfig);

    GaloisPoly = 0x20;
    Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_0_REG, MC1_CH0_CR_CADB_DSEL_UNISEQ_POLY_0_REG, Controller, MC0_CH1_CR_CADB_DSEL_UNISEQ_POLY_0_REG, IpChannel);
    MrcWriteCR (MrcData, Offset, GaloisPoly); // Galois Poly = 0x20
    Offset += MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_1_REG - MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_0_REG;
    MrcWriteCR (MrcData, Offset, GaloisPoly); // Galois Poly = 0x20
    Offset += MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_1_REG - MC0_CH0_CR_CADB_DSEL_UNISEQ_POLY_0_REG;
    MrcWriteCR (MrcData, Offset, GaloisPoly); // Galois Poly = 0x20

    PatternBuffer[0] = 0x2A;
    PatternBuffer[1] = 0xC;
    PatternBuffer[2] = 0x30;
    MrcInitCadbPgMux (MrcData, PatternBuffer, 0, 3);
    Cpgc20BaseRepeats (MrcData, 0, 1);

    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCadbEnable, WriteToCache, &GetSetEn);
    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccDeselectEnable, WriteToCache, &GetSetEn);
    MrcFlushRegisterCachedData (MrcData);

    // Build ACT command
    CadbBufferIndex = 0;
    CmdAddr.Ddr5.Ca = 0;
    CmdAddr.Ddr5.Ca |= (Row & 0xF) << 2;
    CmdAddr.Ddr5.Ca |= (BankGroup & 0x7) << 8;
    CmdAddr.Ddr5.Ca |= (BankAddress & 0x3) << 6;

    RankMask = BaseRankMask;
    RankMask = (UINT8) MrcBitSwap (RankMask, 0, (UINT8) Rank, 1);
    Cs[0] = Cs[1] = (UINT8) RankMask;
    CpgcConvertDdrToCadb (MrcData, &CmdAddr, Cke, Cs, 0, &CadbPatternChunks[CadbBufferIndex++]); // 1st CMD cycle

    CmdAddr.Data32 = 0;
    CmdAddr.Ddr5.Ca |= (Row & 0x3FFF0) >> 4;
    Cs[0] = Cs[1] = (UINT8) BaseRankMask;
    CpgcConvertDdrToCadb (MrcData, &CmdAddr, Cke, Cs, 0, &CadbPatternChunks[CadbBufferIndex++]); // 2nd CMD cycle
    CmdAddr.Data32 = 0xDEADBEEF;
    CpgcConvertDdrToCadb (MrcData, &CmdAddr, Cke, Cs, 0, &CadbPatternChunks[CadbBufferIndex++]); // Deselect
    CadbPatternChunks[CadbBufferIndex - 1].Data32.Low = MrcBitSwap (CadbPatternChunks[2].Data32.Low, 0, 16, 1); // Unset VAL bit
    CadbPatternChunks[CadbBufferIndex - 1].Data32.High = 1;

    // Build WRA command
    CmdAddr.Ddr5.Ca = 0xD; // WRA
    CmdAddr.Ddr5.Ca |= (BankGroup & 0x7) << 8;
    CmdAddr.Ddr5.Ca |= (BankAddress & 0x3) << 6;
    RankMask = BaseRankMask;
    RankMask = (UINT8) MrcBitSwap (RankMask, 0, (UINT8) Rank, 1);
    Cs[0] = Cs[1] = (UINT8) RankMask;
    CpgcConvertDdrToCadb (MrcData, &CmdAddr, Cke, Cs, 0, &CadbPatternChunks[CadbBufferIndex++]); // 1st CMD cycle
    CmdAddr.Data32 = 0;
    CmdAddr.Ddr5.Ca = 0x800; // Not Partial
    Cs[0] = Cs[1] = BaseRankMask;
    CpgcConvertDdrToCadb (MrcData, &CmdAddr, Cke, Cs, 0, &CadbPatternChunks[CadbBufferIndex++]); // 2nd CMD cycle
    CmdAddr.Data32 = 0;
    CpgcConvertDdrToCadb (MrcData, &CmdAddr, Cke, Cs, 0, &CadbPatternChunks[CadbBufferIndex++]); // Deselect
    CadbPatternChunks[CadbBufferIndex - 1].Data32.Low = MrcBitSwap (CadbPatternChunks[2].Data32.Low, 0, 16, 1); // Unset VAL bit
    CadbPatternChunks[CadbBufferIndex - 1].Data32.High = 1;

    MrcSetCadbPgPattern (MrcData, McChBitMask, CadbPatternChunks, CadbBufferIndex, 0);

    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMaskCs,    WriteNoCache, &GetSetEn);
    MrcWait (MrcData, MRC_TIMER_1NS * 2);

    CadbCtl.Data = 0;
    CadbCtl.Bits.START_TEST = 1;
    Cadb20ControlRegWrite (MrcData, McChBitMask, CadbCtl);
    CpgcSeqCtl.Data = 0;
    CpgcSeqCtl.Bits.START_TEST = 1;
    Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

    MrcWait (MrcData, MRC_TIMER_1MS * 2);

    CadbCtl.Bits.START_TEST = 0;
    CadbCtl.Bits.STOP_TEST = 1;
    Cadb20ControlRegWrite (MrcData, McChBitMask, CadbCtl);
    CpgcSeqCtl.Bits.START_TEST = 0;
    CpgcSeqCtl.Bits.STOP_TEST = 1;
    Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

    // Cleanup
    MrcSetCpgcBindMode (MrcData, TRUE, TRUE); // Re-Enable Global Start/Stop

    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCadbEnable, WriteToCache, &GetSetDis);
    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccDeselectEnable, WriteToCache, &GetSetDis);
    MrcFlushRegisterCachedData (MrcData);

    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMaskCs,    WriteNoCache, &GetSetDis);

    MrcGetSetMc(MrcData, Controller, GsmMccEnableSr, WriteNoCache, &GetSetValSave);  // Save state of Self Refresh

    MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, WriteToCache, &tCWL4TxDqFifoRdEnSave);
    MrcFlushRegisterCachedData(MrcData);

    Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SCHED_SECOND_CBIT_REG, MC1_CH0_CR_SCHED_SECOND_CBIT_REG, Controller, MC0_CH1_CR_SCHED_SECOND_CBIT_REG, Channel);
    SchedSecondCBit.Bits.disable_starved_prio_on_new_req = 0;
    MrcWriteCR (MrcData, Offset, SchedSecondCBit.Data);

    Offset = OFFSET_CALC_CH (MC0_CR_CPGC2_V_CHICKEN_REG, MC1_CR_CPGC2_V_CHICKEN_REG, Controller);
    MrcWriteCR (MrcData, Offset, Cpgc2ChickenSave.Data);

    Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC_SEQ_CFG_B_REG, MC1_REQ0_CR_CPGC_SEQ_CFG_B_REG, Controller, MC0_REQ1_CR_CPGC_SEQ_CFG_B_REG, Channel);
    MrcWriteCR (MrcData, Offset, CpgcSeqCfgBSave.Data);

    Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_GS_CFG_REG, MC1_CH0_CR_SC_GS_CFG_REG, Controller, MC0_CH1_CR_SC_GS_CFG_REG, Channel);
    MrcWriteCR64 (MrcData, Offset, ScGsCfgSave.Data);
  } else {
    McChBitMask = SelectReutRanks (MrcData, Controller, Channel, 1 << Rank, FALSE, RankFeatureEnable);

    Status = SetupIOTestPPR (MrcData, Controller, Channel, Row, BankGroup, BankAddress, &ByteMask);

    // Run the test
    RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);
  }

  return Status;
}

/**
  Calculate DqioDuration based on frequency and memory type

  @param[in]  MrcData      - Include all MRC global data
  @param[out] DqioDuration - DqioDuration calculated

  @retval mrcSuccess               - if it success
  @retval mrcUnsupportedTechnology - if the frequency doesn't match
**/
MrcStatus
GetDqioDuration (
  IN     MrcParameters *const MrcData,
  OUT    UINT8               *DqioDuration
  )
{
  MrcStatus   Status;
  MrcDdrType  DdrType;

  DdrType = MrcData->Outputs.DdrType;
  Status  = mrcSuccess;

  if (DdrType == MRC_DDR_TYPE_LPDDR4) {
    Status = Lpddr4GetDqioDuration (MrcData, DqioDuration);
  } else if (DdrType == MRC_DDR_TYPE_LPDDR5) {
    Status = Lpddr5GetDqioDuration (MrcData, DqioDuration);
  } else { // DDR5
    Status = Ddr5GetDqioDuration (MrcData, DqioDuration);
  }
  return Status;
}

/**
This function enables and runs Post Package Repair on detected fail rows.

@param[in]  MrcData      - Pointer to global MRC data.

@retval MrcStatus - mrcSuccess if the DelayType is supported.
**/
MrcStatus
MrcPostPackageRepair (
  IN  MrcParameters *const MrcData,
  IN  UINT32         Controller,
  IN  UINT32         Channel,
  IN  UINT32         Rank,
  IN  UINT32         BankGroup,
  IN  UINT32         BankAddress,
  IN  UINT32         Row,
  IN  UINT16         ByteMask
  )
{
  MrcOutput      *Outputs;
  MrcDdrType     DdrType;
  MrcStatus      Status;
  MRC_LP5_BANKORG BankMode   = MrcLp58Bank; // 8 banks mode

  Outputs = &MrcData->Outputs;
  DdrType = Outputs->DdrType;

  Status      = mrcFail;

  if (DdrType == MRC_DDR_TYPE_DDR4) {
    Status = Ddr4PostPackageRepair (MrcData, Controller, Channel, Rank, BankGroup, BankAddress, Row, ByteMask);
  } else if (DdrType == MRC_DDR_TYPE_DDR5) {
    Status = Ddr5PostPackageRepair (MrcData, Controller, Channel, Rank, BankGroup, BankAddress, Row, ByteMask);
  } else if (DdrType == MRC_DDR_TYPE_LPDDR4) {
    Status = LpDdr4PostPackageRepair (MrcData, Controller, Channel, Rank, BankAddress, Row);
  } else if (DdrType == MRC_DDR_TYPE_LPDDR5) {
    Status = LpDdr5PostPackageRepair (MrcData, Controller, Channel, Rank, BankGroup, BankAddress, Row, BankMode);
  }
  return Status;
}

/**

Get status whether PPR resource is available

@param socket             - Socket
@param Ch                 - Channel number
@param Dimm               - Dimm number
@param Rank               - Rank number
@param SubRank            - SubRank number
@param Nibble             - Nibble number used to identify DRAM
@param BankGroup          - Bank Group number

@retval TRUE if PPR resource available; FALSE if not available

**/
BOOLEAN
GetPprResourceAvailable (
  MrcParameters   *const    MrcData,
  UINT32                    Controller,
  UINT32                    Channel,
  UINT32                    Rank,
  UINT16                    DeviceByte,
  UINT8                     BankGroup,
  UINT8                     BankAddress
  )
{
  MRC_FUNCTION    *MrcCall;
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  MrcInput        *Inputs;
  BOOLEAN         PprResourceAvailable;
  UINT32          MRNumber;
  UINT8           DeviceData;
  UINT64          MprData; // DDR4 only
  UINT8           EccMprData;
  UINT8           MrrResult[4];
  UINT8           PprResource;

  Inputs        = &MrcData->Inputs;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  MrcCall       = Inputs->Call.Func;
  PprResourceAvailable = FALSE;

  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_DDR4:
      MrcDdr4ReadMPR (MrcData, Controller, Channel, Rank, BankGroup, 2, 0, &MprData, &EccMprData); // MPR Page 2, MPR Number 0 for hPPR/sPPR resource check
      if (DeviceByte == MRC_DDR4_ECC_BYTE) {
        DeviceData = EccMprData;
      } else {
        DeviceData = MrcCall->MrcRightShift64 (MprData, (8 * DeviceByte)) & 0xFF;
      }
      if (Inputs->PprEnable == HARD_PPR) {
        if ((DeviceData & DDR4_MPR_PAGE_2_HPPR_OFFSET) != 0) {
          PprResourceAvailable = TRUE;
        } else {
          PprResourceAvailable = FALSE;
        }
      } else { // Soft PPR
        if ((DeviceData & DDR4_MPR_PAGE_2_SPPR_OFFSET) != 0) {
          PprResourceAvailable = TRUE;
        } else {
          PprResourceAvailable = FALSE;
        }
      }
      break;
    case MRC_DDR_TYPE_DDR5:
      if ((BankGroup == 0x0) || (BankGroup == 0x1)) {
        MRNumber = mrMR54;
      } else if ((BankGroup == 0x2) || (BankGroup == 0x3)) {
        MRNumber = mrMR55;
      } else if ((BankGroup == 0x4) || (BankGroup == 0x5)) {
        MRNumber = mrMR56;
      } else if ((BankGroup == 0x6) || (BankGroup == 0x7)) {
        MRNumber = mrMR57;
      } else {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "DDR5 PPR Error, incorrect parameters: BankGroup%d, BankAddress%d\n", BankGroup, BankAddress);
        return mrcWrongInputParameter;
      }

      MrcIssueMrr (MrcData, Controller, Channel, Rank, MRNumber, MrrResult); // Check the correct MR for PPR Resources, based on BA/BG
      PprResource = MrrResult[DeviceByte];
      if (BankGroup & 0x1) { // Odd number bank group
        PprResource = (PprResource & (MRC_BIT4 << BankAddress));
      } else { // Even number bank group
        PprResource = (PprResource & (MRC_BIT0 << BankAddress));
      }
      if (PprResource == 0) { // Resource is not available
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "No PPR Resources available: BankGroup%d, BankAddress%d, Device%d\n", BankGroup, BankAddress, DeviceByte);
        PprResourceAvailable = FALSE;
      } else {
        PprResourceAvailable = TRUE;
      }
      break;
    case MRC_DDR_TYPE_LPDDR4:
      MrcIssueMrr (MrcData, Controller, Channel, Rank, mrMR25, MrrResult);
      DeviceData = MrrResult[0];
      PprResourceAvailable = (DeviceData & (1 << BankAddress)) == 1;
      break;
    case MRC_DDR_TYPE_LPDDR5:
      MrcIssueMrr (MrcData, Controller, Channel, Rank, mrMR29, MrrResult);
      PprResource = MrrResult[0];

      //switch (BankMode) {
      //case Bank8:
      // @todo: Modify LPDDR5 to check MR3 value from host structure, MR3 BG ORG bit needs to be added.
      PprResourceAvailable = ((PprResource & (MRC_BIT0 << BankAddress)) == 1);
      /*  break;
      case Bank16:
        if (BankAddress > 7) { // Banks 0 & 8, 1 & 9, etc. share PPR resource. See JEDEC PPR spec for details
          PprResource = (PprResource & (MRC_BIT0 << (BankAddress - 7)));
        } else {
          PprResource = (PprResource & (MRC_BIT0 << BankAddress));
        }
        break;
      case BankGroups:
        if (BankGroup % 2) { // Even number bank groups
          PprResource = (PprResource & (MRC_BIT0 << BankAddress));
        } else { // Odd number bank groups
          PprResource = (PprResource & (MRC_BIT4 << BankAddress));
        }
        break;
      default:
        return mrcWrongInputParameter;
        break;
      }*/

      break;
    default:
      break;
  }
  return PprResourceAvailable;
}

/**

Returns the number of low order bank group and bank address bits that are not included in the BG interleave

@param[in] MrcData                - Pointer to MrcData
@param[in] McChBitMask            - MC channel bit mask
@param[in] Rank                   - Rank number

@retval BaseBits               - Number of bank bits for software bank loop

**/
UINT32
GetBaseBits (
  IN MrcParameters *const MrcData,
  IN UINT32               McChBitMask,
  IN UINT32               Rank
  )
{
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcDimmOut          *DimmOut;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               BaseBits = 0;
  UINT8               MaxChDdr;

  Outputs = &MrcData->Outputs;

  MaxChDdr = Outputs->MaxChannels;

  if (Outputs->DdrType == MRC_DDR_TYPE_DDR5) { // Assume DDR5 has 32 banks (8 banks per group)
    // BG_Interleave 8
    BaseBits = 5 - (MrcLog2 (8) - 1); // MrcLog2 returns +1 so subtract 1 from the result
  } else if (Outputs->DdrType == MRC_DDR_TYPE_DDR4) {
    // BG_Interleave 2
    BaseBits = 3 - (MrcLog2 (2) - 1);
    MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "BaseBits: DDR4, %d\n", BaseBits);
  } else { // Assume LPDDR has 8 banks
    BaseBits = MrcLog2 (8) - 1; // MrcLog2 returns +1 so subtract 1 from the result
    MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "BaseBits: LP, %d\n", BaseBits);
  }
  // TODO: Remove later if unnecessary

  if (Outputs->DdrType == MRC_DDR_TYPE_DDR5) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChDdr; Channel++) {
        if (MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels) == 0) {
          continue;
        }
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        DimmOut = &ChannelOut->Dimm[RANK_TO_DIMM_NUMBER(Rank)];
        BaseBits = MrcLog2(DimmOut->Banks) - 1;
      }
    }
    MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "BaseBits: DDR5, %d\n", BaseBits);
  }

  return BaseBits;
} // GetBaseBits
