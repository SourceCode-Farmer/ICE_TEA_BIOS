/** @file
  Definition of DDR5 Specific functions, constants, defines,
  and data structures.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
  DDR5 JEDEC Spec
**/
#ifndef _MRC_DDR_5_H_
#define _MRC_DDR_5_H_

#include "MrcChipApi.h"
#include "MrcDdrCommon.h"
#include "MrcDdr5Registers.h"

// DDR5 MPC Command definitions
#define DDR5_MPC_EXIT_CS_TRAINING_MODE    0x00
#define DDR5_MPC_ENTER_CS_TRAINING_MODE   0x01
#define DDR5_MPC_DLL_RESET                0x02
#define DDR5_MPC_ENTER_CA_TRAINING_MODE   0x03
#define DDR5_MPC_ZQCAL_LATCH              0x04
#define DDR5_MPC_ZQCAL_START              0x05
#define DDR5_MPC_STOP_DQS_INTERVAL_OSC    0x06
#define DDR5_MPC_START_DQS_INTERVAL_OSC   0x07
#define DDR5_MPC_SET_2N_COMMAND_TIMING    0x08
#define DDR5_MPC_SET_1N_COMMAND_TIMING    0x09
#define DDR5_MPC_EXIT_PDA_ENUM_PROG_MODE  0x0A
#define DDR5_MPC_ENTER_PDA_ENUM_PROG_MODE 0x0B
#define DDR5_MPC_MANUAL_ECS_OPERATION     0x0C
// 0x0D - 0x1E = Reserved for Future Use
#define DDR5_MPC_APPLY_VREF_RTT           0x1F
// MPC Masks
// These MPC commands must be masked with the data
// or ID codes to be used. Refer to the DDR5 spec
// MPC Opcodes section.

// MPC commands using OP[2:0] to specify MR value
#define DDR5_MPC_GROUP_A_RTT_CK(val)      (0x20 | (0x7 & (val)))
#define DDR5_MPC_GROUP_B_RTT_CK(val)      (0x28 | (0x7 & (val)))
#define DDR5_MPC_GROUP_A_RTT_CS(val)      (0x30 | (0x7 & (val)))
#define DDR5_MPC_GROUP_B_RTT_CS(val)      (0x38 | (0x7 & (val)))
#define DDR5_MPC_GROUP_A_RTT_CA(val)      (0x40 | (0x7 & (val)))
#define DDR5_MPC_GROUP_B_RTT_CA(val)      (0x48 | (0x7 & (val)))
#define DDR5_MPC_SET_DQS_RTT_PARK(val)    (0x50 | (0x7 & (val)))
#define DDR5_MPC_SET_RTT_PARK(val)        (0x58 | (0x7 & (val)))
// MPC commands using OP[3:0] to specify ID value
#define DDR5_MPC_PDA_ENUMERATE_ID(val)    (0x60 | (0xF & (val)))
#define DDR5_MPC_PDA_SELECT_ID(val)       (0x70 | (0xF & (val)))
#define DDR5_MPC_CFG_TDLLK_TCCD_L(val)    (0x80 | (0xF & (val)))

// VREF commands using OP[6:0] to specify VREF calibration setting
#define DDR5_VREFCA(val)                  (0x7F & (val))
#define DDR5_VREFCS(val)                  ((0x7F & (val)) | MRC_BIT7)


///
/// Timings
///

///  Precharge to Precharge Delay for all Frequencies in tCK
#define MRC_DDR5_tPPD_ALL_FREQ (2)

// DDR5 Muti-cycle CS timings (nCK)
// Use CA/CS 22N/6N timing
#define DDR5_tMC_MPC_SETUP_MIN             8 // tMC_MPC_Setup
#define DDR5_tMC_MPC_HOLD_MIN              8 // tMC_MPC_Hold
#define DDR5_tMPC_CS                       6 // tMPC_CS

/// CAS-to-CAS delay for all frequencies in tCK
#define MRC_DDR5_tCCD_ALL_FREQ (8)

/// tRX_DQS2DQ (Units of 100UI for precision)
#define MRC_DDR5_tRX_DQS2DQ_MIN  100
#define MRC_DDR5_tRX_DQS2DQ_3200 300
#define MRC_DDR5_tRX_DQS2DQ_4400 325
#define MRC_DDR5_tRX_DQS2DQ_4800 350

/// tDQSCK (1000nCK)
#define MRC_DDR5_tDQSCK_3200 (240)
#define MRC_DDR5_tDQSCK_3600 (252)
#define MRC_DDR5_tDQSCK_4000 (270)
#define MRC_DDR5_tDQSCK_4400 (286)
#define MRC_DDR5_tDQSCK_4800 (300)

/// tDQSCKi (1000nCK) DQS to CK Variance Window
#define MRC_DDR5_tDQSCKi_3200 (410)
#define MRC_DDR5_tDQSCKi_3600 (430)
#define MRC_DDR5_tDQSCKi_4000 (460)
#define MRC_DDR5_tDQSCKi_4400 (475)
#define MRC_DDR5_tDQSCKi_4800 (490)

/// tDQSS (1000nCK)
#define MRC_DDR5_tDQSS_MIN (375)
#define MRC_DDR5_tDQSS_MAX (375)

// tDQS2DQmin (pS)
#define MRC_DDR5_tDQS2DQ_MIN (200)

/// tXP value for DDR5: max(7.5ns, 8nCK)
#define tXP_DDR5_FS  7500000

/// tCSH_SRexit - Self-Refresh exit CS_n High Pulse width [ps]
#define MRC_DDR5_tCSH_SREXIT_PS   (13000)

/// tCSL_SRexit - Self-Refresh exit CS_n Low Pulse width [nCK]
#define MRC_DDR5_tCSL_SREXIT_1N   (3)
#define MRC_DDR5_tCSL_SREXIT_2N   (6)

/// tRPRE value (nCK)
#define TRPRE_DDR5_1tCK  (1)
#define TRPRE_DDR5_2tCK  (2)
#define TRPRE_DDR5_3tCK  (3)
#define TRPRE_DDR5_4tCK  (4)

/// tWPRE value (nCK)
#define TWPRE_ALL_FREQ_DDR5  (2)

/// tODTon / tODToff values in [nCK]
#define MRC_DDR5_tODT_ON_WR_OFFSET  (-3)
#define MRC_DDR5_tODT_OFF_WR_OFFSET (2)

/// CKE min pulse width/ Power down Entry to Exit (FS)
#define MRC_DDR5_tCKE_MIN           (7500000)
#define MRC_DDR5_tCKE_MIN_NCK       (8)

/// DDR5 CS Training Timings
// Registration of CSTM Entry command to start of training samples time
// Registration of CATM exit to next valid command delay
#define MRC_DDR5_tCSTM_ENTRY_EXIT_DELAY_NS (20)

// Min time between last CS_N pluse and first pulse of MPC Command to exit CSTM
#define MRC_DDR5_tCSTM_MIN_TO_MPC_EXIT_NCK (4)

// Time from sample evaluation to output on DQ bus
#define MRC_DDR5_tCSTM_VALID_NS            (20)

// Time output is available on DQ BUS
#define MRC_DDR5_tCSTM_DQ_WINDOW_NCK       (2)

/// DDR5 CA Training Timings
// Registration of CATM Entry command to start of training samples time
// Registration of CATM exit to next valid command delay
#define MRC_DDR5_tCATM_ENTRY_EXIT_DELAY_NS (20)

// Time from sample evaluation to output on DQ bus
#define MRC_DDR5_tCATM_VALID_NS            (20)

// Time output is available on DQ BUS
#define MRC_DDR5_tCATM_DQ_WINDOW_NCK       (2)

#define MRC_DDR5_CL_30                     (4)

// Write Leveling Pulse Enable - Time from Write Leveling Training Enable MRW to
// when Internal Write Leveling Pulse logic level is valid (NS)
#define MRC_DDR5_tWLPEN_NS                 (15)

// MAX tWLO is 9.5 ns
#define MRC_DDR5_tWLO_MAX_FS               (95 * 100 * 1000)

// MAX tWLOE is 2 ns
#define MRC_DDR5_tWLOE_MAX_FS              (2 * 1000 * 1000)

// DDR5 Write leveling modes
typedef enum {
  Ddr5WrLvlModeDisable,   // MR2[1]: Write Leveling = 0, MR2[7]: Internal Write Timing = 0
  Ddr5WrLvlModeExternal,  // MR2[1]: Write Leveling = 1, MR2[7]: Internal Write Timing = 0
  Ddr5WrLvlModeInternal,  // MR2[1]: Write Leveling = 1, MR2[7]: Internal Write Timing = 1
  Ddr5WrLvlModeFinal      // MR2[1]: Write Leveling = 0, MR2[7]: Internal Write Timing = 1
} Ddr5WrLvlMode;

typedef struct {
  UINT16  RttWr;   ///< Wa - Write ODT on active rank
  UINT16  RttNomRd;
  UINT16  RttNomWr;
  UINT16  RttPark;
  UINT16  RttParkDqs;
} TOdtValueDqDdr5;

typedef struct {
  UINT16  RttCa;
  UINT16  RttCs;
  UINT16  RttCk;
} TOdtValueCccDdr5;

typedef enum {
  DDR5_ODIC_34,     // 0 - RZQ/7 = 34 Ohm
  DDR5_ODIC_40,     // 1 - RZQ/6 = 40 Ohm
  DDR5_ODIC_48,     // 2 - RZQ/5 = 48 Ohm
  DDR5_ODIC_RSVD_1  // 3 - Reserved
} OutputDriverImpedanceControlDdr5;

typedef union {
  struct {
    UINT32 RowBits0_3  : 4;
    UINT32 RowBits4_16 : 13;
    UINT32 RowBit17    : 1;
  }Bits;
  UINT32 Data32;
} Ddr5ActStruct;

/**
  This function is used to get the timing parameter tDQSCK.
  
  @param[in]  MrcData    - Pointer to MRC global data.
  
  @retval UINT32 - The requested parameter in Picoseconds.
**/
UINT32
Ddr5GetTdqsck (
  IN  MrcParameters   *const MrcData
  );

/**
  This function is used to get the timing parameter tDQSCKi.

  @param[in]  MrcData    - Pointer to MRC global data.

  @retval UINT32 - The requested parameter in Picoseconds.
**/
UINT32
Ddr5GetTdqscki (
  IN  MrcParameters   *const MrcData
  );

/**
  This function is used to get the timing parameter tRX_DQS2DQ Min or Max.

  @param[in]  MrcData    - Pointer to MRC global data.
  @param[in]  IsMin      - TRUE returns the minimum value, FALSE returns the maximum value.

  @retval UINT32 - The requested parameter in Femptoseconds.
**/
UINT32
Ddr5GetTdqs2dq (
  IN  MrcParameters   *const MrcData,
  IN  BOOLEAN         IsMin
  );

/**
  Perform JEDEC Init sequence for DDR5.

  @param[in] MrcData - Pointer to MRC global data.

  @retval MrcStatus
**/
MrcStatus
MrcJedecInitDdr5 (
  IN MrcParameters *const MrcData
  );

/**
  This function changes the DDR5 DIMM Voltages using PMIC.
  OEM-specific function is used if implemented and returns SUCCESS;
  otherwise default MRC function is used.

  @param[in, out] MrcData - The MRC "global data" area.
**/
VOID
MrcDdr5VoltageCheckAndSwitch (
  IN OUT MrcParameters      *MrcData
  );

/**
  Perform JEDEC reset sequence for DDR5.

  @param[in] MrcData - Include all MRC global data.

  @retval - mrcSuccess if no errors encountered, mrcFail otherwise
**/
MrcStatus
MrcJedecResetDdr5 (
  IN MrcParameters *const MrcData
  );

/**
  This function issues the PDA Enumeration ID MPC. This will first send a MPC to enter PDA Enumeration Mode.
  It will then iterate through all DRAM devices and send an MPC to assign it an ID number, starting at 0.
  Once the iteration is done, a MPC will be sent to exit PDA Mode. This will wait tMPC Delay between PDA Entry
  and the first PDA Enumerate ID MPC's. This will wait tPDA_DELAY between the PDA Enumerate ID MPC's.

  @param[in]  MrcData       - Pointer to global MRC data

  @retval MrcStatus - mrcTimeout if the FSM does not complete after 1s
  @retval MrcStatus - mrcSuccess if the MPC is sent successfuly
  @retval MrcStatus - mrcFail for unexepected failures
**/
MrcStatus
MrcPdaEnumeration (
  IN MrcParameters *const MrcData
  );

/**
  Enable/Disable DDR5 Write Leveling mode on DRAM

  @param[in] MrcData     - Include all MRC global data.
  @param[in] Rank        - Rank to work on
  @param[in] WriteLvMode - Write leveling mode to enable (Internal/External)
                            Ddr5WrLvlModeDisable,   // MR2[1]: Write Leveling = 0, MR2[7]: Internal Write Timing = 0
                            Ddr5WrLvlModeExternal,  // MR2[1]: Write Leveling = 1, MR2[7]: Internal Write Timing = 0
                            Ddr5WrLvlModeInternal,  // MR2[1]: Write Leveling = 1, MR2[7]: Internal Write Timing = 1
                            Ddr5WrLvlModeFinal      // MR2[1]: Write Leveling = 0, MR2[7]: Internal Write Timing = 1

  @retval - mrcSuccess if no errors encountered, mrcFail otherwise
**/
MrcStatus
MrcDdr5SetDramWrLvlMode (
  IN MrcParameters  *const MrcData,
  IN UINT32                Rank,
  IN Ddr5WrLvlMode   const WrLevelingMode
  );

/**
  Set CAS latency on on DRAM

  @param[in] MrcData     - Include all MRC global data.
  @param[in] CasLatency  - CAS latency value:
                            00000B: 22
                            00001B: 24
                            00010B: 26
                            00011B: 28
                            ...
                            10011B: 60
                            10100B: 62
                            10101B: 64
                            10110B: 66
                            All other encodings reserved.

  @retval - mrcSuccess if no errors encountered, mrcFail otherwise
**/
MrcStatus
MrcDdr5SetCasLatency (
  IN MrcParameters  *const MrcData,
  UINT8                    CasLatency
  );

/**
  This function sets Write Leveling Internal Cycle delay through MR3 command

  @param[in] MrcData          - Include all MRC global data.
  @param[in] Controller       - Index of the requested Controler
  @param[in] Channel          - Index of the requested Channel
  @param[in] Rank             - Current working rank
  @param[in] IntCycleAlignLow - Write Leveling Internal Cycle Alignment (Lower Byte)
  @param[in] IntCycleAlignUp  - Write Leveling Internal Cycle Alignment (Upper Byte)
                                0000B: Disable (Default)
                                0001B: -1 tCK
                                0010B: -2 tCK
                                0011B: -3 tCK
                                0100B: -4 tCK
                                0101B: -5 tCK
                                0110B: -6 tCK
                                0111B: -7 tCK
                                1000B~ 1111B: RFU
  @param[in]  DebugPrint - When TRUE, will print debugging information

  @retval - mrcSuccess if no errors encountered, mrcFail otherwise
**/
MrcStatus
MrcSetMR3_DDR5 (
  IN MrcParameters  *const MrcData,
  IN UINT32                Controller,
  IN UINT32                Channel,
  IN UINT32                Rank,
  IN UINT8                 IntCycleAlignLow,
  IN UINT8                 IntCycleAlignUp,
  IN BOOLEAN               DebugPrint
  );

/**
  This function selects the DQ ODT table according to DIMM/rank population.

  @param[in] MrcData         - Include all the MRC general data.
  @param[in] Dimm            - selected DIMM.
  @param[in] OdtIndex        - selected ODT index.

  @retval TOdtValueDqDdr5 * - Pointer to the relevant table or NULL if the table was not found.
**/
TOdtValueDqDdr5 *
SelectTable_DDR5 (
  IN MrcParameters *const MrcData,
  IN const UINT32         Dimm,
  IN const TOdtIndex      OdtIndex
  );

/**
  This function converts from Ohms to MR ODT encoding.

  @param[in]  OdtValue  - ODT Value in Ohms.

  @retval INT8 - Encoding if valid ODT value.  Else, -1.
**/
INT8
CccOdtEncode_DDR5 (
  IN  UINT32  OdtValue
  );

/**
  This function selects the CCC ODT table according to the Rank population.

  @param[in] MrcData         - Include all the MRC general data.
  @param[in] Controller      - selected Controller.
  @param[in] Channel         - selected Channel.
  @param[in] Rank            - selected Rank.

  @retval TOdtValueCccDdr5 * - Pointer to the relevant table or NULL if the table was not found.
**/
TOdtValueCccDdr5 *
SelectCccTable_DDR5 (
  IN MrcParameters *const MrcData,
  IN const UINT32         Controller,
  IN const UINT32         Channel,
  IN const UINT32         Rank
  );

/**
Used to update TxVref for DDR5.
Uses input offset value to increment/decrement current setting.


@param[in,out] MrcData        - Include all MRC global data.
@param[in]     Controller     - Selecting which Controller to talk to.
@param[in]     Channel        - Selecting which Channel to talk to.
@param[in]     RankMask       - Selecting which Ranks to talk to.
@param[in]     DeviceMask     - Selecting which Devices to talk to (only valid for DDR4 and adjusting VrefDQ).
@param[in]     VrefType       - Determines the Vref type to change, only CmdV and TxVref are valid.
@param[in]     Offset         - Vref offset value.
@param[in]     UpdateMrcData  - Used to decide if Mrc host must be updated.
@param[in]     PdaMode        - Selecting to use MPC or old method for MRS.
@param[in]     IsCachedOffset - Determines if the paramter is an offset (relative to cache) or absolute value.

@retval MrcStatus - mrcWrongInputParameter if unsupported OptParam,  mrcSuccess otherwise
**/
MrcStatus
Ddr5SetDramVref (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Controller,
  IN     UINT8                Channel,
  IN     UINT8                RankMask,
  IN     UINT16               DeviceMask,
  IN     UINT8                VrefType,
  IN     INT32                Offset,
  IN     BOOLEAN              UpdateMrcData,
  IN     BOOLEAN              PdaMode,
  IN     BOOLEAN              IsCachedOffset
);

/**
Set Tx Dimm Vref absolute Value for DDR5.
Use custom MRC range [-62:+63] where zero offset is 66.0%

@param[in, out] MrcData       - Include all MRC global data.
@param[in]      Controller    - Memory Controller Number within the processor (0-based).
@param[in]      Channel       - Selecting which Channel to talk to
@param[in]      Rank          - Selecting which Rank to talk to
@param[in]      Device        - Selecting which Device to talk to (only valid when PDAmode is TRUE)
@param[in]      VrefType       - Determines the Vref type to change, only CmdV and TxVref are valid.
@param[in]      TxVrefOffset  - TxVref Offset to set
@param[in]      UpdateMrcData - update MRC host struct
@param[in]      PdaMode       - Selecting to use MPC or old method for MRS

@retval none
**/
VOID
MrcSetTxVrefDdr5 (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               Controller,
  IN     UINT8                Channel,
  IN     UINT8                Rank,
  IN     UINT8                Device,
  IN     UINT8                VrefType,
  IN     INT32                TxVrefOffset,
  IN     BOOLEAN              UpdateMrcData,
  IN     BOOLEAN              PdaMode
);

/**
Offset to DDR5 VrefDQ Range/Value (MR10)
Offset [-62:+63] - Vref 0 - 125 :: (35% - 97.5%) * 1.1V

@param[in]  MrcData  - Include all MRC global data.
@param[in]  Offset   - Value to be converted to actual VrefDQ Range/Value.

@retval DDR4 VrefDQ Range/Value
**/
UINT8
MrcOffsetToVrefDdr5 (
  IN INT32                Offset
);

/**
DDR5 VrefDQ Range/Value (MR10) to Offset
Vref Range 0 - 125 - Offset [-62:+63] :: (35% - 97.5%) * 1.1V where offset zero is 66.0%

@param[in]  VrefDQ   - VrefDQ Range/Value to be converted back to corresponding Offset value.

@retval Offset
**/
INT32
MrcVrefToOffsetDdr5 (
  IN UINT8                VrefDQ
);

/**
This function Update the Non PDA DDR5 Vref to match the PDA average
across all bytes (per rank/ch).
This will allow the use of Non PDA sweep using GetBerMarginByte

@param[in]     MrcData         - Include all MRC global data.
@param[in]     Controller      - Selecting which Controller to talk to.
@param[in]     Channel         - Selecting which Channel to talk to.
@param[in]     RankMask        - ranks to work on

@retval mrcWrongInputParameter if a bad Param is passed in, otherwise mrcSuccess.
**/

MrcStatus
UpdatePdaCenterDdr5 (
  IN     MrcParameters *const MrcData,
  IN     const UINT8          Controller,
  IN     const UINT8          Channel,
  IN     const UINT8          Ranks,
  IN     UINT8                VrefType
  );

/**
  Get all the data for all Ranks all Devs for specific controller, channel and Mr for DDR5

  example in DDR5 2R  X8 with ecc the data will return as:
  MrPdaData[]= {R0D0,R1D0,R0D1,R1D1,R0D2,R1D2,R0D3,R1D3,R0ECC,R1ECC}

  Important:
  The *NumMrData can different depending on the MR

  @param[in] MrcData      - Pointer to global MRC data.
  @param[in] Controller   - Controller to work on.
  @param[in] Channel      - channel to work on.
  @param[in] MrAddress    - MR Address
  @param[in] MrPdaData    - Array of Data that will be filled
  @param[in] NumMrData    - pointer that will contain # of data that filled in the array.

  @retval MrcStatus - mrcSuccess, otherwise an error status.
  **/
MrcStatus
MrFillPdaMrsDataDdr5 (
  IN      MrcParameters   *MrcData,
  IN      UINT32          Controller,
  IN      UINT32          Channel,
  IN      MrcModeRegister MrAddress,
  IN OUT  UINT8           MrPdaData[MAX_PDA_MR_IN_CHANNEL],
  IN OUT  UINT8           *NumMrData
  );

/**
  This function completes setting up the Generic MRS FSM configuration to enable SAGV during normal operation.

  @param[in] MrcData  - Pointer to global MRC data.
  @param[in] Print    - Boolean control for debug print messages.

  @retval MrcStatus - mrcSuccess, otherwise an error status.
**/
MrcStatus
MrcFinalizeDdr5MrSeq (
  IN  MrcParameters *const MrcData,
  IN  BOOLEAN              Print
  );

/**
This function sets PuulUp/Dn Drive Stregth through MR5 command
@param[in]      MrcData     - Pointer to global MRC data.
@param[in]      Controller       - Index of the requested Controler
@param[in]      Channel          - Index of the requested Channel
@param[in]      Rank             - Current working rank
@param[in]      PuDrvStr    - Pull Down Drive Strength value to set (in Ohms).  Does not support infinity (0xFFFF) as RFU.
@param[in]      PdDrvStr    - Pull Down Drive Strength value to set (in Ohms).  Does not support infinity (0xFFFF) as RFU.

@retval MrcStatus - mrcSuccess if a supported Ron value, else mrcWrongInputParameter.
**/
MrcStatus
MrcSetMR5_DDR5(
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT16                PuDrvStr,
  IN  UINT16                PdDrvStr
);

/**
  Program DQS_RTT_PARK via MPC

  @param[in]      MrcData          - Pointer to global MRC data.
  @param[in]      Controller       - Index of the requested Controler
  @param[in]      Channel          - Index of the requested Channel
  @param[in]      Rank             - Current working rank
  @param[in]      DqsRttPark       - DIMM RttParkDqs value
  @param[in]      DebugPrint       - When TRUE, will print debugging information
  @param[in]      UpdateHost       - When TRUE, will update MRC host struct

  @retval MrcStatus - mrcSuccess if succeeded.
**/
MrcStatus
MrcSetMR33_DDR5 (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT16                DqsRttPark,
  IN  BOOLEAN               DebugPrint,
  IN  BOOLEAN               UpdateHost
);

/**
  Program MR34

  @param[in]      MrcData          - Pointer to global MRC data.
  @param[in]      Controller       - Index of the requested Controler
  @param[in]      Channel          - Index of the requested Channel
  @param[in]      Rank             - Current working rank
  @param[in]      OdtPark          - DIMM RttPark value
  @param[in]      OdtWr            - DIMM RttWr value
  @param[in]      DebugPrint       - When TRUE, will print debugging information
  @param[in]      UpdateHost       - When TRUE, will update MRC host struct

  @retval MrcStatus - mrcSuccess if succeeded.
**/
MrcStatus
MrcSetMR34_DDR5 (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT16                OdtPark,
  IN  UINT16                OdtWr,
  IN  BOOLEAN               DebugPrint,
  IN  BOOLEAN               UpdateHost
);

MrcStatus
MrcSetMR35_DDR5 (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT16                OdtNomWr,
  IN  UINT16                OdtNomRd,
  IN  BOOLEAN               DebugPrint,
  IN  BOOLEAN               UpdateHost
);

/**
Ddr5 Get the MR value and its corresponding index for a given DIMM Opt Param.
Value is set by reference to the corresponding pointers.

@param[in]      MrcData     - Include all MRC global data.
@param[in]      OptParam    - The Dimm Opt Param (e.g., OptDimmRon, OptDimmOdtWr, OptDimmOdtPark, OptDimmOdtNom)
@param[out]     *MrIndex    - Updated Pointer to the MR index.
@param[out]     *MrNum      - Updated Pointer to the MR number.

@retval MrcStatus - mrcWrongInputParameter if unsupported OptParam, mrcSuccess otherwise
**/
MrcStatus
Ddr5GetOptDimmParamMrIndex(
  IN MrcParameters *const MrcData,
  IN UINT8                OptDimmParam,
  OUT UINT8               *MrIndex,
  OUT UINT8               *MrNum
  );

/**
  Ddr5 Set DimmParamValue is responsible for performing the concrete set DIMM parameter to value,
  using Lpddr specific MR set functions.
  Parameters supported: OptDimmRon, OptDimmOdtWr

  @param[in,out]  MrcData    - Include all MRC global data.
  @param[in,out]  MrData     - Pointer to the MR data to update.
  @param[in]      Controller - the controller to work on
  @param[in]      Channel    - The channel to work on
  @param[in]      Rank       - The rank to work on
  @param[in]      OptParam   - The Dimm Opt Param (e.g., OptDimmRon, OptDimmOdtWr, OptDimmOdtPark, OptDimmOdtNom)
  @param[in]      ParamValue - The actual values (Typically in Ohms)
  @param[in]      UpdateHost - Decides if MrcData has to be updated

  @retval MrcStatus - mrcWrongInputParameter if unsupported OptParam, MrcStatus of the MR set functions otherwise
**/
MrcStatus
Ddr5SetDimmParamValue (
  IN OUT MrcParameters *const MrcData,
  IN OUT UINT16        *const MrData,
  IN     UINT32               Controller,
  IN     UINT32               Channel,
  IN     UINT32               Rank,
  IN     UINT8                OptParam,
  IN     UINT16               ParamValue,
  IN     BOOLEAN              UpdateHost
);

/**
Ddr5 Get DimmParamValue is responsible for performing the concrete Get DIMM paramter to value, using
the MRC Cache.
@param[in,out]  MrcData         - Include all MRC global data.
@param[in,out]  MrData          - Pointer to the MR data to update.
@param[in]      OptParam        - The Dimm Opt Param (e.g., OptDimmRon, OptDimmOdtWr, OptDimmOdtPark, OptDimmOdtNom)
@param[in,out]  ParamValue      - The actual values (Typically in Ohms)

@retval MrcStatus - mrcWrongInputParameter if unsupported OptParam
- MrcStatus of the MR set functions otherwise

**/
MrcStatus
Ddr5GetDimmParamValue(
  IN OUT MrcParameters *const MrcData,
  IN OUT UINT16        *const MrData,
  IN     UINT8                OptParam,
  IN OUT UINT16*                ParamValue
);

/**
  DDR5 get available values and the number of possible values of a given DimmOptParam.
  The order of the values returns corresponds to the ascending field values in the MRs as defind by JEDEC

  @param[in]      MrcData               - Include all MRC global data.
  @param[in]      DimmOptParam          - e.g., OptDimmOdtWr, OptDimmOdtNom, OptDimmOdtPark, OptDimmRon
  @param[out]     **DimmOptParamVals    - Reference to the pointer of values.
  @param[out]     *NumDimmOptParamVals  - Reference to the number of values.

  @retval MrcStatus - mrcWrongInputParameter if unsupported OptParam, mrcSuccess otherwise
**/
MrcStatus
Ddr5GetDimmOptParamValues (
  IN MrcParameters *const MrcData,
  IN UINT8                DimmOptParam,
  OUT UINT16              **DimmOptParamVals,
  OUT UINT8               *NumDimmOptParamVals
  );

/**
Enter Post Package Repair (PPR) to attempt to repair detected failed row.

  @param[in] MrcData     - Pointer to MRC global data.
  @param[in] Controller  - Controller for detected fail row
  @param[in] Channel     - Channel for detected fail row
  @param[in] Rank        - Rank for detected fail row
  @param[in] BankAddress - BankAddress for detected fail row
  @param[in] BankGroup   - BankGroup for detected fail row
  @param[in] Row         - Row for detected fail row
  @param[in] ByteMask    - Byte mask to repair for fail row

  @retval MrcStatus
**/

MrcStatus
Ddr5PostPackageRepair (
  IN MrcParameters* const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN UINT32               BankGroup,
  IN UINT32               BankAddress,
  IN UINT32               Row,
  IN UINT16               ByteMask
  );

/**
  Check if the given MR is one of the DFE TAP MR's

  @param[in] MrAddr - MR address.

  @retval TRUE if this is one of the DFE TAP MR's
**/
BOOLEAN
Ddr5IsDimmDfeMr (
  IN UINT8 MrAddr
  );

/**
  Return the DFE TAP of MR according to the MR Address.
  Also check if the MR represents the Bit0 in this TAP.

  @param[in]      MrAddr          - MR address
  @param[in, out] IsDimmDfeMrBit0 - Pointer used to return the Bit0 Indication.

  @retval Tap Index (0-3).
**/
UINT8
Ddr5GetDimmDfeTap (
  IN  UINT8     MrAddr,
  OUT BOOLEAN   *IsDimmDfeMrBit0
  );

/**
  Calculate DqioDuration based on frequency for DDR5

  @param[in]  MrcData      - Include all MRC global data
  @param[out] DqioDuration - DqioDuration calculated

  @retval mrcSuccess               - if it success
  @retval mrcUnsupportedTechnology - if the frequency doesn't match
**/
MrcStatus
Ddr5GetDqioDuration (
  IN     MrcParameters *const MrcData,
  OUT    UINT8               *DqioDuration
  );

/**
  This function returns up to 2 arrays with the Lower/Upper Byte information for each Device within the specified Controller/Channel/Rank

  @param[in]      MrcData           - Include all MRC global data.
  @param[in]      Controller        - Controller to check
  @param[in]      Channel           - Channel to check
  @param[in]      Rank              - Rank to check
  @param[out]     LowerByte         - Array of Lower DRAM Bytes per-Device. Maximum allowed size is MAX_BYTE_IN_DDR5_CHANNEL
  @param[out]     UpperByte         - Optional Array of Upper DRAM Bytes per-Device. Only for use with x16 DRAM devices. Maximum allowed size is MAX_BYTE_IN_DDR5_CHANNEL.

  @retval None
**/
VOID
GetDdr5LowerUpperBytes (
  IN MrcParameters* const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  OUT UINT8               LowerByte[MAX_BYTE_IN_DDR5_CHANNEL],
  OUT UINT8               UpperByte[MAX_BYTE_IN_DDR5_CHANNEL] OPTIONAL
  );

/**
  This function returns the requested DelayType timing in nCK units.

  @param[in]  MrcData      - Pointer to global MRC data.
  @param[in]  DelayType    - Requested delay type
  @param[out] TimingNckOut - Output variable for the requested delay timing in nCK units

  @retval MrcStatus - mrcSuccess if the DelayType is supported.
                      mrcWrongInputParameter if TimingNckOut is NULL.
                      mrcTimingError if the timing value overflows the output pointer.
**/
MrcStatus
Ddr5GmfDelayTimings (
  IN  MrcParameters *const MrcData,
  IN  GmfTimingIndex       DelayType,
  OUT UINT16               *TimingNckOut
  );
#endif // _MRC_DDR_5_H_
