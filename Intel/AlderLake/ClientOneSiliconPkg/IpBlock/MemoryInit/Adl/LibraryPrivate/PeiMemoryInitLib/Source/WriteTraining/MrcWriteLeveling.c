/** @file
  The write leveling flow is the first part of the write training.
  In this stage the memory controller needs to synchronize its DQS sending
  with the clock for each DRAM.  The DRAM can be put in a mode where for a
  write command it responds by sampling the clock using DQS and sending it
  back as the data.  The IO can receive this and tune the DQS alignment so
  it will appear in sync with the clock at the DRAM side.
  The following algorithm is used for the write leveling flow:

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "MrcWriteLeveling.h"
#include "MrcCpgcApi.h"
#include "MrcMemoryApi.h"
#include "MrcLpddr4.h"
#include "MrcDdr4.h"
#include "MrcDdr5.h"
#include "MrcDdr5Registers.h"
#include "MrcChipApi.h"
#include "MrcDdrIoOffsets.h" //@todo remove when setting Write Leveling mode is behind a Memory API
#include "MrcMaintenance.h"

///
/// Local defines - used by this file only
///
// This defines the maximum ADD delay that can be programmed to the register. It may change in the future
#define RTL_CONSTANT        (10)

// Dq/Dqs sweep range: LP4 need larger sweep due to tDQS2DQ variation of 200-800 ps.
// This is calucluated in the function.
#define DQ_SWEEP_START            (-15)
#define DQ_SWEEP_END              (15)
#define DQ_SWEEP_STEP             (5)
#define DQSDQ_SWEEP_STEP          (2)

// Minimum Eye Width for Lp5 Wck Training
#define MIN_WCK_EYE_WIDTH         (20)


/**
  Normalize Dq Fifo Read and Write Enable delays, to make sure the delta is always even for Gear4 DDR5
   1. Check if delta is Odd
   2. See if we have enough headroom in TxDqsDelay
   3. Offset TxDqsDelay by 128 and move the cycle to Read Enable delay

  @param[in, out] MrcData           - Include all MRC global data.
  @retval MrcStatus - If it succeeded, return mrcSuccess
**/
MrcStatus
MrcNormalizeTxGear4(
  IN OUT MrcParameters *const MrcData
)
{

  INT64                         GetSetVal;
  INT64                         GetSetVal2;
  INT64                         TxDqFifoWrEn;
  INT64                         TxDqFifoRdEn;
  UINT8                         Rank;
  UINT8                         Byte;
  UINT8                         Controller;
  UINT8                         Channel;
  BOOLEAN                       Gear4;
  BOOLEAN                       Gear2;
  MrcOutput                     *Outputs;
  MrcDebug                      *Debug;
  BOOLEAN                       ShiftPi;
  UINT8                         Delta;
  UINT8                         ByteMask;
  UINT8                         ValidRankMask;
  UINT8                         RankMask;
  MrcStatus                     Status;

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  Gear4         = Outputs->Gear4;
  Gear2         = Outputs->Gear2;
  ValidRankMask = Outputs->ValidRankMask;
  ShiftPi  = FALSE;
  Delta    = 0;
  ByteMask = 0;
  Status   = mrcSuccess;

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = 1 << Rank;
    if (!(RankMask & ValidRankMask)) {
      continue;
    }
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
          continue;
        }

        MrcGetSetMcCh(MrcData, Controller, Channel, TxDqFifoWrEnTcwlDelay, ReadUncached | PrintValue, &TxDqFifoWrEn);
        MrcGetSetMcCh(MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, ReadUncached | PrintValue, &TxDqFifoRdEn);

        Delta = (UINT8)(TxDqFifoRdEn - TxDqFifoWrEn);
        if (Gear4 && (Delta % 2)) {
          ShiftPi = TRUE;
        }

        ByteMask = 0;
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d:\n", Controller, Channel, Rank);
        // Program the final settings to the DQ bytes and update MrcData
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, ReadUncached, &GetSetVal);
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Original DqsPi B%d:\t\t%d\n", Byte, (UINT32)GetSetVal);
          if (ShiftPi) {
            if (GetSetVal > 128) {
             // MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d: Error Could not Shift offset\n", Controller, Channel, Rank);
              ByteMask |= 1 << Byte;
            }
          }
        }  // Byte

        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Final ByteMask is %x\n", ByteMask);
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (ShiftPi) {
            if (ByteMask == 0xF) {
              GetSetVal = -128;
              Status = MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, WriteOffsetCached | PrintValue, &GetSetVal);
              MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, ReadUncached, &GetSetVal2);
              GetSetVal2 += (Gear2 || Gear4) ? 96 : 32;
              Status = MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Byte, TxDqDelay, ForceWriteCached | PrintValue, &GetSetVal2);
            }
            else {
              GetSetVal = 128;
              Status = MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, WriteOffsetCached | PrintValue, &GetSetVal);
              MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, ReadUncached, &GetSetVal2);
              GetSetVal2 += (Gear2 || Gear4) ? 96 : 32;
              Status = MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Byte, TxDqDelay, ForceWriteCached | PrintValue, &GetSetVal2);
            }
          }
        } // byte

        if (ShiftPi) {
          if (ByteMask == 0xF) {
            TxDqFifoRdEn += 1;
            MrcGetSetMcCh(MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, WriteUncached | PrintValue, &TxDqFifoRdEn);
          } else {
            TxDqFifoRdEn -= 1;
            MrcGetSetMcCh(MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, WriteUncached | PrintValue, &TxDqFifoRdEn);
          }
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d: Shifted Rden to %d, WrEn is %d\n", Controller, TxDqFifoRdEn, TxDqFifoWrEn);
          ShiftPi = FALSE;
        }
      }  // Channel
    }  // Controller
  }  //Rank

  return Status;

}

/**
  Calculate Cycle and PI Delay per channel and per rank
   1. Check if possible to reserve PI's if needed
   2. Program the global min cycle to each channel
   3. Program rank cycle delay
   4. Program byte PI delay + PI reserved as needed

  @param[in, out] MrcData           - Include all MRC global data.
  @param[in]      DqPiReserve       - Number of PIs to reserve
  @param[in]      WrLvlTotPiDly     - Total WL Pi delays array
  @param[in]      WrLvlChMin        - Minimum WL delay per channel array
  @param[in]      WrLvlRankMin      - Minimum WL delay per rank array
  @param[in]      ByteDqOffsetStart - Start passing offset per byte delay array
  @param[in]      ByteDqOffsetEnd   - End passing offset per byte delay array

  @retval MrcStatus - If it succeeded, return mrcSuccess
**/
MrcStatus
MrcNormalizeTxDelay (
  IN OUT MrcParameters *const MrcData,
  IN UINT16                   DqPiReserve,
  IN UINT32                   MaxDec,
  IN UINT32                   WrLvlTotPiDly[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN UINT32                   WrLvlChMin[MAX_CONTROLLER][MAX_CHANNEL],
  IN UINT32                   WrLvlRankMin[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL],
  IN INT16                    ByteDqOffsetStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN INT16                    ByteDqOffsetEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  UINT32                        IpChannel;
  UINT32                        Offset;
  UINT32                        tCWL;
  INT64                         GetSetVal;
  INT64                         TxDqFifoWrEn;
  INT64                         TxDqFifoRdEn;
  UINT32                        AddOneQclk[MAX_CONTROLLER][MAX_CHANNEL];
  INT32                         WrLvlChCycle[MAX_CONTROLLER][MAX_CHANNEL];
  INT32                         WrLvlRankCycle[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  INT32                         MaxChTxDqFifoRdEnDelay[MAX_CONTROLLER][MAX_CHANNEL];
  INT32                         WrLvlChMinReserved;
  INT32                         CurrentOffset;
  INT16                         ByteDqOffset;
  UINT8                         TotMaxAddDelay;
  UINT8                         TotMaxDecDelay;
  UINT8                         ValidRankMask;
  UINT8                         RankMask;
  UINT8                         Rank;
  UINT8                         Byte;
  UINT8                         Controller;
  UINT8                         Channel;
  UINT32                        FirstController;
  UINT32                        FirstChannel;
  BOOLEAN                       Lpddr;
  BOOLEAN                       Lpddr4;
  BOOLEAN                       Gear4;
  BOOLEAN                       Gear2;
  BOOLEAN                       Saturated;
  MrcStatus                     Status;
  MrcStatus                     Status2;
  MrcOutput                     *Outputs;
  const MRC_FUNCTION            *MrcCall;
  const MrcInput                *Inputs;
  MrcDebug                      *Debug;
  MC0_CH0_CR_SC_WR_DELAY_STRUCT ScWrDelay;

  Outputs          = &MrcData->Outputs;
  Inputs           = &MrcData->Inputs;
  Debug            = &Outputs->Debug;
  MrcCall          = Inputs->Call.Func;
  Saturated        = FALSE;
  Gear4            = Outputs->Gear4;
  Gear2            = Outputs->Gear2;
  ValidRankMask    = Outputs->ValidRankMask;
  Lpddr            = Outputs->Lpddr;
  Lpddr4           = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  TotMaxAddDelay   = (Gear4) ? (4 * MAX_ADD_DELAY)     : ((Gear2) ? (2 * MAX_ADD_DELAY)     : MAX_ADD_DELAY);
  TotMaxDecDelay   = (Gear4) ? (4 * MAX_DEC_DELAY + 1) : ((Gear2) ? (2 * MAX_DEC_DELAY + 1) : MAX_DEC_DELAY);
  FirstController  = (MrcControllerExist (MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel     = Outputs->Controller[FirstController].FirstPopCh;

  MrcCall->MrcSetMem ((UINT8 *) AddOneQclk,             sizeof (AddOneQclk),             0);
  MrcCall->MrcSetMem ((UINT8 *) WrLvlChCycle,           sizeof (WrLvlChCycle),           0);
  MrcCall->MrcSetMem ((UINT8 *) WrLvlRankCycle,         sizeof (WrLvlRankCycle),         0);
  MrcCall->MrcSetMem ((UINT8 *) MaxChTxDqFifoRdEnDelay, sizeof (MaxChTxDqFifoRdEnDelay), 0);

  MrcGetSetMcCh (MrcData, FirstController, FirstChannel, GsmMctCWL, ReadFromCache, &GetSetVal);
  tCWL = (UINT32) GetSetVal;
  if (Lpddr4) {
    tCWL++;     // Due to tDQSS on LP4
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }

      // Program per channel delay - Add/Dec
      WrLvlChMinReserved = MAX ((INT32) WrLvlChMin[Controller][Channel] - DqPiReserve, 0);

      ScWrDelay.Data = 0;
      CurrentOffset  = 0;
      WrLvlChCycle[Controller][Channel] = MIN ((WrLvlChMinReserved / 128), (TotMaxAddDelay + TotMaxDecDelay));
      if (!(Gear2 || Gear4)) {
        // Protect against underflow - i.e exceeding MaxDec
        WrLvlChCycle[Controller][Channel] = MAX (TotMaxDecDelay - (INT32) MaxDec, WrLvlChCycle[Controller][Channel]);
      }

      if (WrLvlChCycle[Controller][Channel] > (TotMaxAddDelay + TotMaxDecDelay)) {
        ScWrDelay.Bits.Add_tCWL = MAX_ADD_DELAY;
      } else {
        if (WrLvlChCycle[Controller][Channel] <= TotMaxDecDelay) {
          CurrentOffset = TotMaxDecDelay - WrLvlChCycle[Controller][Channel];
          ScWrDelay.Bits.Dec_tCWL = (Gear4) ? CurrentOffset / 4 : ((Gear2) ? CurrentOffset / 2 : CurrentOffset);
          // Keeping ScWrDelay.Bits.Add_tCWL = 0;
        } else {
          // Keeping ScWrDelay.Bits.Dec_tCWL = 0;
          CurrentOffset = WrLvlChCycle[Controller][Channel] - TotMaxDecDelay;
          ScWrDelay.Bits.Add_tCWL = (Gear4) ? (CurrentOffset + 3) / 4 : ((Gear2) ? (CurrentOffset + 1) / 2 : CurrentOffset);
        }
      }

      if (Gear4) {
        AddOneQclk[Controller][Channel] = 3 - (ABS (CurrentOffset) % 4);
      } else {
        AddOneQclk[Controller][Channel] = (((CurrentOffset % 2) == 0) && (Gear2)) ? 1 : 0;
      }

      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_WR_DELAY_REG, MC1_CH0_CR_SC_WR_DELAY_REG, Controller, MC0_CH1_CR_SC_WR_DELAY_REG, IpChannel);
      MrcWriteCR (MrcData, Offset, ScWrDelay.Data);
    } // for Channel
  } // for Controller

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = 1 << Rank;
    if (!(RankMask & ValidRankMask)) {
      continue;
    }
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_WR_DELAY_REG, MC1_CH0_CR_SC_WR_DELAY_REG, Controller, MC0_CH1_CR_SC_WR_DELAY_REG, IpChannel);
        ScWrDelay.Data = MrcReadCR (MrcData, Offset);
        WrLvlRankCycle[Controller][Channel][Rank] = 0;
        //Grab the longest TxDqFifoRdEnDelay per ch across all ranks
        MaxChTxDqFifoRdEnDelay[Controller][Channel] = MAX (WrLvlRankCycle[Controller][Channel][Rank], MaxChTxDqFifoRdEnDelay[Controller][Channel]);

        GetSetVal = WrLvlRankCycle[Controller][Channel][Rank];
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Rank cycle delay set to %d\n", GetSetVal);
        MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, TxDqFifoRdEnFlybyDelay, WriteToCache, &GetSetVal);
        GetSetVal = 0;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmIocTxDqFifoRdEnPerRankDelDis, WriteToCache, &GetSetVal);

        MrcGetTxDqFifoDelays (MrcData, Controller, Channel, tCWL, ScWrDelay.Bits.Add_tCWL, ScWrDelay.Bits.Dec_tCWL, &TxDqFifoWrEn, &TxDqFifoRdEn);
        if (Gear2) {
          TxDqFifoRdEn += AddOneQclk[Controller][Channel]; // TxDqFifoRdEnTcwlDelay(DCLK)
        }

        if (TxDqFifoRdEn < 0) {
          TxDqFifoRdEn = 0;
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n%s Not enough Tx FIFO separation! TxDqFifoRrEn: %d, TxFifoSeparation: %d\n", gErrString, (UINT32) TxDqFifoRdEn, TX_FIFO_SEPARATION);
        }
        MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoWrEnTcwlDelay, WriteToCache | PrintValue, &TxDqFifoWrEn);
        MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, WriteToCache | PrintValue, &TxDqFifoRdEn);

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d:\tChCyc\tRankCyc\tDqsPi\tDqPi\tTotalPiDelay\n", Controller,Channel, Rank);
        // Program the final settings to the DQ bytes and update MrcData
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          GetSetVal = WrLvlTotPiDly[Controller][Channel][Rank][Byte] - WrLvlChCycle[Controller][Channel] * 128 - WrLvlRankCycle[Controller][Channel][Rank] * 128;
          if (Gear4) {
            // Use Dq/Dqs PI instead of RdEn, to keep constant pointer separation
            GetSetVal = GetSetVal + 128 * AddOneQclk[Controller][Channel];
          }
          Status  = MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay,  WriteToCache, &GetSetVal); // Write same value as DQS; DQ will be adjusted below.
          Status2 = MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, WriteToCache, &GetSetVal);
          if ((Status == mrcParamSaturation) || (Status2 == mrcParamSaturation)) {
            Saturated = TRUE;
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "B%d:\t\t%d\t%d\t%d", Byte, WrLvlChCycle[Controller][Channel], WrLvlRankCycle[Controller][Channel][Rank], (UINT32) GetSetVal);
          if (Lpddr4) {
            // Set DQ to the middle of the passing region
            ByteDqOffset = (ByteDqOffsetEnd[Controller][Channel][Rank][Byte] + ByteDqOffsetStart[Controller][Channel][Rank][Byte]) / 2;
            ChangeMargin (MrcData, WrTLp4, ByteDqOffset, 0, 0, Controller, Channel, Rank, Byte, 0, 1, 0);
          } else {
            GetSetVal = (Gear2 || Gear4) ? 96 : 32;
            Status = MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay, WriteOffsetToCache, &GetSetVal);
            if (Status == mrcParamSaturation) {
              Saturated = TRUE;
            }
          }
#ifdef MRC_DEBUG_PRINT
          MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay, ReadFromCache, &GetSetVal);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%d\t%d\n", (UINT32) GetSetVal, WrLvlTotPiDly[Controller][Channel][Rank][Byte]);
#endif // MRC_DEBUG_PRINT
        } // Byte
      } // for Channel
    } // Controller
  } // for Rank
  MrcFlushRegisterCachedData (MrcData);

  if (Saturated) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n%s, Parameter saturation!\n", gErrString);
    return mrcParamSaturation;
  }

  return mrcSuccess;
}

/**
  This function executes the Jedec Write Leveling Cleanup.
  Center TxDQS-CLK timing

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus - If it succeeded, return mrcSuccess
**/
MrcStatus
WriteLevelingFlyBy (
  IN OUT MrcParameters *const MrcData
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcIntOutput        *IntOutputs;
  MrcControllerOut    *ControllerOut;
  MrcChannelOut       *ChannelOut;
  MrcStatus           Status;
  UINT64              ErrStatus;
  UINT64              EccStatus;
  UINT8               EccByte;
  UINT32              WrLvlTotPiDly[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32              WrLvlRankMin[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT32              WrLvlChMin[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32              Tdqs2dqMinFs;
  UINT32              Tdqs2dqMaxFs;
  UINT32              Tdqs2dqCenterFs;
  UINT32              MaxDec;
  UINT32              MaxAdd;
  UINT32              Data32;
  UINT32              UiLoop;
  INT32               DqSweepStart;
  INT32               DqSweepStop;
  INT32               DqOffset;
  INT32               StartOffset;
  INT32               EndOffset;
  INT32               CurrentOffset;
  UINT16              BytePass[MAX_CONTROLLER][MAX_CHANNEL]; // Bit mask indicating which ch/byte has passed
  UINT16              SavedTxDqsVal[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // Saved TxDqs value
  UINT16              ByteMask;
  UINT16              ValidByteMask;
  UINT16              Result;
  UINT16              SkipMe;
  INT16               ByteDqOffsetStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               ByteDqOffsetEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8               Param;
  UINT8               Channel;
  UINT8               ChannelPlusOne;
  UINT8               Controller;
  UINT8               Rank;
  UINT8               Byte;
  UINT8               McChBitMask;
  UINT8               RankMask;  // RankBitMask for both channels
  UINT8               ValidRankMask;
  UINT8               RankHalf;
  UINT8               RankMod2;
  UINT8               LoopCount;
  UINT8               SdramCount;
  UINT8               MaxChannels;
  INT8                DqSweepStep;
  BOOLEAN             Gear2;
  BOOLEAN             Done;
  BOOLEAN             Lpddr4;
  BOOLEAN             Lpddr;
  INT64               GetSetVal;
  INT64               GetSetEn;
  // Same pattern on all 64 DQ's
  // 4 different CL's:
  // 11111111 00000000 11110000 00001111
  //static UINT32 Pattern[8][2] = {
  //  { 0x0FF000FF, 0x0FF000FF },
  //  { 0x0FF000FF, 0x0FF000FF },
  //  { 0x0FF000FF, 0x0FF000FF },
  //  { 0x0FF000FF, 0x0FF000FF },
  //  { 0x0FF000FF, 0x0FF000FF },
  //  { 0x0FF000FF, 0x0FF000FF },
  //  { 0x0FF000FF, 0x0FF000FF },
  //  { 0x0FF000FF, 0x0FF000FF }
  //};

#ifdef MRC_DEBUG_PRINT
  UINT8               BitGroupErr[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
#endif // MRC_DEBUG_PRINT

  Inputs = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  Lpddr = Outputs->Lpddr;
  MaxChannels = Outputs->MaxChannels;
  Debug = &Outputs->Debug;
  Gear2 = Outputs->Gear2;
  Status = mrcSuccess;
  Done = TRUE;
  SdramCount = Outputs->SdramCount;
  EccStatus = 0;
  EccByte = (Outputs->DdrType == MRC_DDR_TYPE_DDR5) ? MRC_DDR5_ECC_BYTE : MRC_DDR4_ECC_BYTE;

  GetSetEn = 1;
  IntOutputs  = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  MrcCall->MrcSetMem ((UINT8 *) WrLvlRankMin, sizeof (WrLvlRankMin), 0xFF);
  MrcCall->MrcSetMem ((UINT8 *) WrLvlChMin, sizeof (WrLvlChMin), 0xFF);
  MrcCall->MrcSetMem ((UINT8 *) ByteDqOffsetEnd, sizeof (ByteDqOffsetEnd), 0);
  Lpddr4 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  LoopCount = 1;
  //NumIters = 1;

  if (Inputs->LoopBackTest) {
    // Enable DCLK in case JEDEC Reset is skipped
    MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableDclk, WriteNoCache, &GetSetEn);

    // Disable CKE - DRAM enters power down
    GetSetVal = 0;
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccCkeOn, WriteNoCache, &GetSetVal);

    // Set SC_GS_CFG_TRAINING.ignore_cke to send CPGC commands even if CKE is LOW.
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccIgnoreCke, WriteNoCache, &GetSetEn);
    IntOutputs->SkipZq = TRUE;  // We don't need ZQ in SetupIOTest()
  }

  // Check memory used

  McChBitMask = Outputs->McChBitMask;
  ValidRankMask = Outputs->ValidRankMask;
  ValidByteMask = (MRC_BIT0 << SdramCount) - 1; // 0x1FF or 0xFF

  SetupIOTestBasicVA (MrcData, McChBitMask, LoopCount, NSOE, 0, 0, 8, PatWrRd, 0, 8);

  MrcMcTxCycleLimits (MrcData, &MaxAdd, &MaxDec);

  // Determine the Dq/Dqs sweep range during the cycle search.
  if (Lpddr4) {
    Param = WrTLp4;
    MrcGetTdqs2dq (MrcData, TRUE, &Tdqs2dqMinFs);
    MrcGetTdqs2dq (MrcData, FALSE, &Tdqs2dqMaxFs);
    Tdqs2dqMaxFs -= 200000;
    if (Inputs->LoopBackTest && Gear2) {
      Tdqs2dqMinFs -= 100000;
    }
    MrcGetTdqs2dqCenter (MrcData, &Tdqs2dqCenterFs);
    DqSweepStart = -1 * (MrcFemptoSec2PiTick (MrcData,  Tdqs2dqCenterFs - Tdqs2dqMinFs));
    DqSweepStop = MrcFemptoSec2PiTick (MrcData, Tdqs2dqMaxFs - Tdqs2dqCenterFs);
    DqSweepStep = DQSDQ_SWEEP_STEP;
  } else {
    Param = WrT;
    DqSweepStart  = DQ_SWEEP_START;
    DqSweepStop   = DQ_SWEEP_END;
    DqSweepStep   = DQ_SWEEP_STEP;
    if (Inputs->MemoryProfile != STD_PROFILE) {
      DqSweepStep = DQSDQ_SWEEP_STEP;
    }
  }

  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Params : DqSweepStart: %d, DqSweepStop: %d, DQ_SWEEP_STEP: %d\n", DqSweepStart, DqSweepStop, DqSweepStep);
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    // Select Rank for REUT test
    McChBitMask   = 0;
    RankMask    = MRC_BIT0 << Rank;
    RankHalf    = Rank / 2;
    RankMod2    = Rank % 2;

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
        BytePass[Controller][Channel] = 0;
      }
    }
    // Skip if both channels empty
    if (!(RankMask & ValidRankMask)) {
      continue;
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank %d\n", Rank);

    if ((Status = MrcIoTxLimits (MrcData, Rank, MaxAdd, MaxDec, &StartOffset, &EndOffset, SavedTxDqsVal)) != mrcSuccess) {
      return Status;
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Sweep through the cycle offsets until we find a value that passes\n");

    if (RankMask & ValidRankMask) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel                 0                1\nDelay  DqOffset   Byte \t");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          for (Byte = 0; Byte < SdramCount; Byte++) {
            if (MrcByteExist (MrcData, Controller, Channel, Byte)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d ", Byte);
            }
          }
        }
      }
    }

    for (CurrentOffset = StartOffset; CurrentOffset <= EndOffset; CurrentOffset++) {

      for (UiLoop = 0; UiLoop < 4; UiLoop++) {
        if (Inputs->LoopBackTest) {
          MrcCall->MrcSetMem ((UINT8 *) ByteDqOffsetStart, sizeof (ByteDqOffsetStart), 0);  // In loopback mode all bytes should pass on the same cycle, so ignore previous passes
          MrcCall->MrcSetMem ((UINT8 *) ByteDqOffsetEnd, sizeof (ByteDqOffsetEnd), 0);
          MrcCall->MrcSetMem ((UINT8 *) BytePass, sizeof (BytePass), 0);
        }
      // Program new delay offsets to DQ/DQS timing:
      SetWriteCycleDelay (MrcData, Rank, MaxAdd, MaxDec, UiLoop, SavedTxDqsVal, CurrentOffset);

      if (!Lpddr4) {
        // Reset FIFOs & Reset DRAM DLL. Wait 1uS for test to complete
        Status = IoReset (MrcData);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          ControllerOut = &Outputs->Controller[Controller];
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
              ChannelOut = &ControllerOut->Channel[Channel];
              Status = MrcWriteMRS (
                MrcData,
                Controller,
                Channel,
                RankMask,
                mrMR0,
                ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrMR0] | (MRC_BIT0 << 8)
              );
            }
          }
        }
        MrcWait (MrcData, (1 * MRC_TIMER_1US));
      }
      // Run Test sweeping DQ to DQS searching for a Pass.
      for (DqOffset = DqSweepStart; DqOffset <= DqSweepStop; DqOffset += DqSweepStep) {
        // Update Offset
        ChangeMargin (MrcData, Param, DqOffset, 0, 1, 0, 0, Rank, 0, 0, 0, 0);
        // Run Test
        // DQPat = BasicVA, DumArr, ClearErrors = 1, mode = 0
        RunIOTest (MrcData, McChBitMask, StaticPattern, 1, 0);
        // Update results for all ch/bytes
        Done = TRUE;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n% 3d\t% 3d\t       \t", CurrentOffset, DqOffset);
//        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CycleDelay: %3d, FifoRdEnFlyby: %d, FifoRdEn: %2d, FifoWrEn: %2d, Dec: %d, Add: %d, Add1Q: %d ",
//          CycleDelay[0], (UINT32) FifoFlybyDelay, (UINT32) TxDqFifoRdEn, (UINT32) TxDqFifoWrEn, ScWrDelay.Bits.Dec_tCWL, ScWrDelay.Bits.Add_tCWL, ScWrDelay.Bits.Add_1Qclk_delay);
        // Update results for all ch/bytes
#ifdef MRC_DEBUG_PRINT
        MrcCall->MrcSetMem((UINT8 *)BitGroupErr, sizeof (BitGroupErr), 0);
#endif // MRC_DEBUG_PRINT
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          ControllerOut = &Outputs->Controller[Controller];
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
              if (Channel == 0) {
                MRC_DEBUG_MSG (
                  Debug,
                  MSG_LEVEL_NOTE,
                  (SdramCount == MAX_SDRAM_IN_DIMM) ? "                 " : "                "
                );
              }
              continue;
            }
            ChannelOut = &ControllerOut->Channel[Channel];
            ValidByteMask = ChannelOut->ValidByteMask;
            Result = 0;
            if (MrcChannelExist (MrcData, Controller, Channel)) {
              // Read out per byte error results and check for any byte error
              MrcGetMiscErrStatus (MrcData, Controller, Channel, ByteGroupErrStatus, &ErrStatus);
              if (Outputs->EccSupport) { // Read out per byte ecc error results and check for any byte error
                MrcGetMiscErrStatus (MrcData, Controller, Channel, EccLaneErrStatus, &EccStatus);
              }
              Result = (UINT16) (ErrStatus |  MrcCall->MrcLeftShift64 (EccStatus, EccByte));
#ifdef MRC_DEBUG_PRINT
              MrcGetBitGroupErrStatus (MrcData, Controller, Channel, BitGroupErr[Controller][Channel]);
#endif // MRC_DEBUG_PRINT
            }
            SkipMe = (Result & ValidByteMask) | BytePass[Controller][Channel];

            for (Byte = 0; Byte < SdramCount; Byte++) {
              if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
                continue;
              }
              ByteMask = 1 << Byte;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, ((Result & ValidByteMask) & ByteMask) ? "# " : ". ");
              // If this byte has failed or previously passed, nothing to do
              if (SkipMe & ByteMask) {
                if ((Result & ByteMask) == 0) {  // Byte passed, record the current end of the passing region
                  ByteDqOffsetEnd[Controller][Channel][Rank][Byte] = (INT16) DqOffset;
                }
                continue;
              }
              // Pass case
              if ((BytePass[Controller][Channel] & ByteMask) == 0) {  // This is the first pass for this byte, record the start of the passing region
                ByteDqOffsetStart[Controller][Channel][Rank][Byte] = (INT16) DqOffset;
              }
              BytePass[Controller][Channel] |= ByteMask;
              // Adjust delay reference point to MAX_DEC_DELAY so they will be positive delays only
              if (Gear2) {
                WrLvlTotPiDly[Controller][Channel][Rank][Byte] = 128 * (CurrentOffset + 2 * MAX_DEC_DELAY + 1) + SavedTxDqsVal[Controller][Channel][Byte];
              } else if (Outputs->Gear4) {
                WrLvlTotPiDly[Controller][Channel][Rank][Byte] = 128 * (CurrentOffset + 4 * MAX_DEC_DELAY + 1) + SavedTxDqsVal[Controller][Channel][Byte];
              } else {
                WrLvlTotPiDly[Controller][Channel][Rank][Byte] = 128 * (CurrentOffset + MAX_DEC_DELAY) + SavedTxDqsVal[Controller][Channel][Byte];
              }
              if (Inputs->LoopBackTest) {
                WrLvlTotPiDly[Controller][Channel][Rank][Byte] += (32 * UiLoop);
              }
              // Track min pi per ch and rank
              if (WrLvlChMin[Controller][Channel] > WrLvlTotPiDly[Controller][Channel][Rank][Byte]) {
                WrLvlChMin[Controller][Channel] = WrLvlTotPiDly[Controller][Channel][Rank][Byte];
              }
              if (WrLvlRankMin[Controller][Channel][Rank] > WrLvlTotPiDly[Controller][Channel][Rank][Byte]) {
                WrLvlRankMin[Controller][Channel][Rank] = WrLvlTotPiDly[Controller][Channel][Rank][Byte];
              }
              // MRC_WRLVL_FLYBY_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "WrLvlTotPiDly[Channel=%d][Rank=%d][Byte=%d]=%d SavedTxDqsVal[Channel][Byte]=%d\n", Channel, Rank, Byte, WrLvlTotPiDly[Channel][Rank][Byte],SavedTxDqsVal[Channel][Byte]);
            } // for Byte
            // MRC_WRLVL_FLYBY_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "WrLvlRankMin[Channel=%d][Rank=%d]=%d WrLvlChMin[Channel=%d]=%d\n", Channel, Rank, WrLvlRankMin[Channel][Rank], Channel, WrLvlChMin[Channel]);
            if (BytePass[Controller][Channel] != ValidByteMask) {
              Done = FALSE;
            }
          } // for Channel
        } // Controller
#ifdef MRC_DEBUG_PRINT
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            ErrStatus = 0;
            for (Byte = 0; Byte < SdramCount; Byte++) {
              if (Byte < EccByte) {
                ErrStatus |= MrcCall->MrcLeftShift64 (BitGroupErr[Controller][Channel][Byte], Byte * 8);
              }
            }
            if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
              if (Outputs->EccSupport) {
                MRC_DEBUG_MSG (
                  Debug,
                  MSG_LEVEL_NOTE,
                  "0x%02X_",
                  BitGroupErr[Controller][Channel][EccByte]
                );
              }
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_NOTE,
                "%s%016llX ",
                Outputs->EccSupport ? "" : "0x",
                ErrStatus
              );
            }
          }
        } //Controller
#endif  // MRC_DEBUG_PRINT

      } // for DqOffset
       // Jump out of the UI loop if everybody is passing or if it's not a loopback test
      if ((Done == TRUE) || !Inputs->LoopBackTest) {
        break;
      }
    } // for UiLoop

      // Jump out of the for offset loop if everybody is passing
      if (Done == TRUE) {
        break;
      }
    } // for offset
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n");
    // Walk through and find the correct value for each ch/byte
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      ControllerOut = &Outputs->Controller[Controller];
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        ChannelOut = &ControllerOut->Channel[Channel];
        if (BytePass[Controller][Channel] != ChannelOut->ValidByteMask) {
          MRC_DEBUG_MSG (
            Debug,
            MSG_LEVEL_ERROR,
            "Error! Write Leveling CleanUp - Couldn't find a passing value for all bytes on Channel %u Rank %u:\nBytes - ",
            Controller,
            Channel,
            Rank
          );
#ifdef MRC_DEBUG_PRINT
          for (Byte = 0; Byte < SdramCount; Byte++) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, ((BytePass[Controller][Channel] ^ ChannelOut->ValidByteMask) & (1 << Byte)) ? "%d " : "", Byte);
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n");
#endif
          if (Inputs->ExitOnFailure) {
            return mrcWriteLevelingError;
          }
        }
        if (!Lpddr4) {
          // Clean up after Test
          Status = MrcWriteMRS (
            MrcData,
            Controller,
            Channel,
            RankMask,
            mrMR0,
            ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrMR0] | (MRC_BIT0 << 8)
          );
          MrcWait (MrcData, (1 * MRC_TIMER_1US));
        }
      } // for Channel
    } // for Controller
  } // for Rank

  // Aggregate the results for LP
  if (Lpddr) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel += 2) {
        ChannelPlusOne = Channel + 1;
        if ((MrcChannelExist (MrcData, Controller, Channel)) && (MrcChannelExist (MrcData, Controller, ChannelPlusOne))) {
          Data32 = MIN (WrLvlChMin[Controller][Channel], WrLvlChMin[Controller][ChannelPlusOne]);
          WrLvlChMin[Controller][Channel] = WrLvlChMin[Controller][ChannelPlusOne] = Data32;

          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            Data32 = MIN (WrLvlRankMin[Controller][Channel][Rank], WrLvlRankMin[Controller][ChannelPlusOne][Rank]);
            WrLvlRankMin[Controller][Channel][Rank] = WrLvlRankMin[Controller][ChannelPlusOne][Rank] = Data32;
          }
        }
      }
    }
  }

  if ((Status = MrcNormalizeTxDelay (MrcData, 128, MaxDec, WrLvlTotPiDly, WrLvlChMin, WrLvlRankMin, ByteDqOffsetStart, ByteDqOffsetEnd)) != mrcSuccess) {
    return Status;
  }

  if (!Lpddr4) {
    // Clean up after Test
    ChangeMargin (MrcData, Param, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);  // Clean TxDq offset
  }
  Status = IoReset (MrcData);
  return Status;
}

/**
  This function execute the Jedec write leveling training for DDR4 and LP4 configurations
  Align TxDqs to CLK on DRAM side.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus -  if it succedes return mrcSuccess
**/
MrcStatus
MrcJedecWriteLevelingDdr4Lp4 (
  IN OUT MrcParameters *const MrcData
  )
{
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcChannelOut     *ChannelOut;
  MrcDdrType        DdrType;
  MrcStatus         MainStatus;
  INT64             GetSetVal;
  INT64             WriteLevelDis;
  INT64             WriteLevelEn;
  INT64             WlLongDelay;
  INT64             GetSetEn;
  INT64             GetSetDis;
  INT64             TxDqDqsDelayAdj;
  INT64             DataTrainFeedbackField;
  INT64             DisDataIdlClkGateSave;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             MaxChannel;
  UINT8             Rank;
  UINT8             RankHalf;
  UINT8             RankMod2;
  UINT8             Byte;
  UINT8             refbyte;
  UINT8             RankMask;  // RankBitMask for both channels
  UINT8             ValidRankMask;
  UINT8             BiasPMCtrl;
  UINT8             EnDqsOdtParkMode;
  UINT16            *MrReg;
  UINT16            WLStart;
  UINT16            WLStop;
  UINT16            WLDelay;
  UINT8             WLStep;
  UINT32            WaitTime;
  UINT32            CRValue;
  UINT32            DqsToggleTime;
  UINT32            DataTrainFeedback;
  INT32             InitialPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32             InitialPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32             CurrentPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32             CurrentPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32             LargestPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32             LargestPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32            FinalDqs[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32             iWidth;
  INT32             cWidth;
  INT32             lWidth;
  INT32             ByteWidth;
  BOOLEAN           Pass;
  BOOLEAN           RankIsx16;
  BOOLEAN           SavedRestoreMRS;
  BOOLEAN           Lpddr4;
  BOOLEAN           Ddr4;
  BOOLEAN           Gear2;
  UINT16            RttParkSetVal;
  TOdtValueDdr4     Ddr4OdtTableIndex;
  DDR4_MODE_REGISTER_1_STRUCT             Ddr4ModeRegister1;

  Inputs        = &MrcData->Inputs;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  MaxChannel    = Outputs->MaxChannels;
  MainStatus    = mrcSuccess;
  CRValue       = 0;
  ValidRankMask = Outputs->ValidRankMask;
  GetSetEn      = 1;
  GetSetDis     = 0;
  WlLongDelay   = 1;
  BiasPMCtrl    = 0xFF;
  EnDqsOdtParkMode = 0xFF;
  Gear2 = Outputs->Gear2;

  DdrType = Outputs->DdrType;
  Lpddr4 = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Ddr4  = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);

  // Save the flag and force MRS recalculation
  SavedRestoreMRS = Outputs->RestoreMRs;
  Outputs->RestoreMRs = FALSE;

  WriteLevelEn  = MrcWriteLevelDdr4Lpddr4;
  WriteLevelDis = MrcWriteLevelOff;

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocWlLongDelEn, WriteToCache, &WlLongDelay);
  MrcGetSetNoScope (MrcData, GsmIocDisDataIdlClkGate, ReadFromCache, &DisDataIdlClkGateSave);
  MrcGetSetNoScope (MrcData, GsmIocDisDataIdlClkGate, WriteToCache, &GetSetEn);

  for (Controller = 0; (Controller < MAX_CONTROLLER) && (BiasPMCtrl == 0xFF) && (EnDqsOdtParkMode == 0xFF); Controller++) {
    for (Channel = 0; (Channel < MaxChannel) && (BiasPMCtrl == 0xFF) && (EnDqsOdtParkMode == 0xFF); Channel++) {
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (MrcByteExist (MrcData, Controller, Channel, Byte)) {
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocBiasPMCtrl, ReadFromCache, &GetSetVal);
          BiasPMCtrl = (UINT8) GetSetVal;
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataDqsOdtParkMode, ReadFromCache, &GetSetVal);
          EnDqsOdtParkMode  =  (UINT8) GetSetVal;
          break;
        }
      }
    }
  }
  DqsToggleTime = (WlLongDelay || Gear2) ? 4096 : 2048;

  // 0.5UI for Gear 1, 1.5UI for Gear 2
  TxDqDqsDelayAdj = Gear2 ? 96 : 32; // @todo_adl Gear4

  WaitTime = Outputs->UIps * DqsToggleTime;


  // DDR4: Set Qoff bit in MR1 on all ranks
  if (Ddr4) {
    MrcSetMR1_DDR4 (MrcData, 1, INIT_DIMM_RON_34, 0, 0, 1);
  }

  // Enable Rank Mux Override
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideEn, WriteNoCache, &GetSetEn);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = (1 << Rank);
    if (!(RankMask & ValidRankMask)) {
      // Skip if all channels empty
      continue;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank %d\n", Rank);
    RankHalf = Rank / MAX_RANK_IN_DIMM;
    RankMod2 = Rank % MAX_RANK_IN_DIMM;
    // Update Rank Mux Override for the rank under test
    GetSetVal = Rank;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideVal, WriteNoCache, &GetSetVal);

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
          // Enable WL mode in MR2[7]
          if (Lpddr4) {
            CRValue = (ChannelOut->Dimm[dDIMM0].Rank[RankMod2].MR[mrMR2]);
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR2, (CRValue | MRC_BIT7), TRUE);
          } else {
            // Ddr4
            MainStatus = GetOdtTableIndex (MrcData, Controller, Channel, RankHalf, &Ddr4OdtTableIndex);
            if (MainStatus != mrcSuccess) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Error: OdtTableIndex is NULL!\n");
              return mrcFail;
            }
            RttParkSetVal = (Ddr4OdtTableIndex.RttWr == 0xFFFF) ? 0 : Ddr4OdtTableIndex.RttWr; // To get High-Z we must disable Rtt Park (0)
            SetDimmParamValue (MrcData, Controller, Channel, RankMask, OptDimmOdtPark, RttParkSetVal, FALSE);
            // RttWr must be disabled in Write Leveling mode
            SetDimmParamValue (MrcData, Controller, Channel, RankMask, OptDimmOdtWr, 0, FALSE);
            Ddr4ModeRegister1.Data = ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrMR1];
            Ddr4ModeRegister1.Bits.WriteLeveling = 1;
            Ddr4ModeRegister1.Bits.Qoff = 0;
            MrcWriteMRS (MrcData, Controller, Channel, RankMask, mrMR1, (UINT16) Ddr4ModeRegister1.Data);
          }
        }
      }  // for Channel
    } // Controller
    MrcFlushRegisterCachedData (MrcData);
    GetSetVal = 1;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataDqsOdtParkMode, WriteToCache, &GetSetVal);
    GetSetVal = 0;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocBiasPMCtrl, WriteToCache, &GetSetVal);
    MrcFlushRegisterCachedData (MrcData);

    // ******************************************
    // STEP 1 and 2: Find middle of high region
    // ******************************************
    // We will add 64 ticks at the end, so shift the range left by 64.
    //  WLStart = 192 - 64;
    //  WLStop  = 192 - 64 + 128;
    WLStart = 128;
    WLStop = 128 + 128;
    WLStep = 2;

    MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "\n\tCh0\t\t\t\t\t\t\t\t%sCh1\n",
        (Outputs->SdramCount == MAX_SDRAM_IN_DIMM) ? "\t" : ""
    );
    // @todo adjust this to print out 2MC
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "WLDelay%s%s",
      (Outputs->SdramCount == MAX_SDRAM_IN_DIMM) ? "\t0\t1\t2\t3\t4\t5\t6\t7\t8" : "\t0\t1\t2\t3\t4\t5\t6\t7",
      (Outputs->SdramCount == MAX_SDRAM_IN_DIMM) ? "\t0\t1\t2\t3\t4\t5\t6\t7\t8" : "\t0\t1\t2\t3\t4\t5\t6\t7"
    );

    for (WLDelay = WLStart; WLDelay < WLStop; WLDelay += WLStep) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n %d:", WLDelay);

      GetSetVal = WLDelay;
      MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, TxDqsDelay, WriteToCache, &GetSetVal);

      // Set TxPiOn and WLMode
      MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxPiPwrDnDis, WriteToCache, &GetSetEn);
      MrcFlushRegisterCachedData (MrcData); // This needs to be here because WLmode is writenocache so it will set the WLmode before TxDqsDelay and clockPwrdn are disabled.

      MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteNoCache, &GetSetEn);
      MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocWriteLevelMode,     WriteNoCache, &WriteLevelEn);
      if (WlLongDelay || Gear2) {
        MrcWait (MrcData, (WaitTime * MRC_TIMER_1NS) / 1000);   // WaitTime is in [ps]
      }
      IoReset (MrcData);
      MrcWait (MrcData, (WaitTime * MRC_TIMER_1NS) / 1000);     // WaitTime is in [ps]

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        // Update results for all Channels/Bytes
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              "\t\t\t\t\t\t\t\t%s",
              (Outputs->SdramCount == MAX_SDRAM_IN_DIMM) ? "\t" : ""
            );
            continue;
          }
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
              continue;
            }

            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackField);
            DataTrainFeedback = (UINT32) DataTrainFeedbackField;
            Pass = (DataTrainFeedback >= 16);
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%c%d", Pass ? '.' : '#', DataTrainFeedback);
            if (WLDelay == WLStart) {
              if (Pass) {
                InitialPassingStart[Controller][Channel][Byte] = InitialPassingEnd[Controller][Channel][Byte] = WLStart;
                CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = WLStart;
                LargestPassingStart[Controller][Channel][Byte] = LargestPassingEnd[Controller][Channel][Byte] = WLStart;
              } else {
                InitialPassingStart[Controller][Channel][Byte] = InitialPassingEnd[Controller][Channel][Byte] = -WLStep;
                CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = -WLStep;
                LargestPassingStart[Controller][Channel][Byte] = LargestPassingEnd[Controller][Channel][Byte] = -WLStep;
              }
            } else {
              if (Pass) {
                if (InitialPassingEnd[Controller][Channel][Byte] == (WLDelay - WLStep)) {
                  InitialPassingEnd[Controller][Channel][Byte] = WLDelay;
                }

                if (CurrentPassingEnd[Controller][Channel][Byte] == (WLDelay - WLStep)) {
                  CurrentPassingEnd[Controller][Channel][Byte] = WLDelay;
                } else {
                  CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = WLDelay;
                }
                // Special case for last step: Append Initial Passing Region
                // WLDelay should be considered a continuous range that wraps around 0
                if ((WLDelay >= (WLStop - WLStep)) && (InitialPassingStart[Controller][Channel][Byte] == WLStart)) {
                  iWidth = (InitialPassingEnd[Controller][Channel][Byte] - InitialPassingStart[Controller][Channel][Byte]);
                  CurrentPassingEnd[Controller][Channel][Byte] += (WLStep + iWidth);
                }
                // Update Largest variables
                cWidth = CurrentPassingEnd[Controller][Channel][Byte] - CurrentPassingStart[Controller][Channel][Byte];
                lWidth = LargestPassingEnd[Controller][Channel][Byte] - LargestPassingStart[Controller][Channel][Byte];
                if (cWidth > lWidth) {
                  LargestPassingStart[Controller][Channel][Byte] = CurrentPassingStart[Controller][Channel][Byte];
                  LargestPassingEnd[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte];
                }
              }
            }
          } // for Byte
        } // for Channel
      } // Controller
    } // for WLDelay

#ifdef MRC_DEBUG_PRINT
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\tInitPassSt\tInitPassEn\tCurrPassSt\tCurrPassEn\tLargPassSt\tLargPassEn\n");
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d\n", Controller);
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "C%d\n", Channel);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (MrcByteExist (MrcData, Controller, Channel, Byte)) {
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_NOTE,
                "   B%d:\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n",
                Byte,
                InitialPassingStart[Controller][Channel][Byte],
                InitialPassingEnd[Controller][Channel][Byte],
                CurrentPassingStart[Controller][Channel][Byte],
                CurrentPassingEnd[Controller][Channel][Byte],
                LargestPassingStart[Controller][Channel][Byte],
                LargestPassingEnd[Controller][Channel][Byte]
              );
            }
          }
        }
      } // Channel
    } //Controller
#endif // MRC_DEBUG_PRINT

    // Clean up after step
    // Program values for TxDQS
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

          // Check if rank is a x16.  Don't apply to LPDDR parts.
          RankIsx16 = (((ChannelOut->Dimm[RankHalf].SdramWidth == 16) && Ddr4) ? TRUE : FALSE);

          // Clear ODT before MRS (JEDEC Spec)
          GetSetVal = 0;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccOdtOverride, WriteToCache | PrintValue, &GetSetVal);
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccOdtOn, WriteToCache | PrintValue, &GetSetVal);
          MrcFlushRegisterCachedData (MrcData);

          // Restore MR2 values
          if (Lpddr4) {
            CRValue = (ChannelOut->Dimm[dDIMM0].Rank[RankMod2].MR[mrMR2]);
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR2, CRValue, TRUE);
            //@todo - LP Channels
            //if (Outputs->EnhancedChannelMode && (ChannelOut->ValidSubChBitMask == 3)) { // Second subch is populated
            //  MrcIssueMrw (MrcData, Controller, Channel, Rank + 2, mrMR2, CRValue, FALSE, TRUE);
            //}
          }
          if (Ddr4) {
            MrReg = &ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrMR0];
            // Restore Write Leveling mode and Rtt_Nom for this rank, set Qoff.
            Ddr4ModeRegister1.Data = MrReg[mrMR1];
            Ddr4ModeRegister1.Bits.Qoff = 1;
            MrcWriteMRS (MrcData, Controller, Channel, RankMask, mrMR1, Ddr4ModeRegister1.Data);

            // Restore RttWr and RttPark for this rank
            MrcWriteMRS (MrcData, Controller, Channel, RankMask, mrMR2, MrReg[mrMR2]);
            MrcWriteMRS (MrcData, Controller, Channel, RankMask, mrMR5, MrReg[mrMR5]);
          }

          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d:\tLftEdge Width\n", Controller, Channel, Rank);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              continue;
            }
            ByteWidth = LargestPassingEnd[Controller][Channel][Byte] - LargestPassingStart[Controller][Channel][Byte];
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              "  B%d:\t%d\t%d\n",
              Byte,
              LargestPassingStart[Controller][Channel][Byte],
              ByteWidth
            );

              // Check if width is valid
              if ((ByteWidth <= 32) || (ByteWidth >= 96)) {
                MRC_DEBUG_MSG (
                  Debug,
                  MSG_LEVEL_ERROR,
                  "\nERROR! Width region outside expected limits for Mc: %u Channel: %u Rank %u Byte: %u\n",
                  Controller,
                  Channel,
                  Rank,
                  Byte
                );
                if (Inputs->ExitOnFailure) {
                  MainStatus = mrcWriteLevelingError;
                }
              }
            // Align byte pairs if DIMM is x16
            if (Ddr4 && (RankIsx16 && (Byte & MRC_BIT0))) {
              // If odd byte number (1, 3, 5 or 7)
              refbyte = Byte - 1;
              if (LargestPassingStart[Controller][Channel][Byte] > (LargestPassingStart[Controller][Channel][refbyte] + 64)) {
                LargestPassingStart[Controller][Channel][Byte] -= 128;
              }
              if (LargestPassingStart[Controller][Channel][Byte] < (LargestPassingStart[Controller][Channel][refbyte] - 64)) {
                LargestPassingStart[Controller][Channel][Byte] += 128;
              }
            }

            // Add 1 QCLK to DqsPi
//            if (!Gear2) {
//              LargestPassingStart[Controller][Channel][Byte] += 64;
//            }


            // GetSetVal = LargestPassingStart[Channel][Byte];
            // MrcGetSetDdrIoGroupStrobe (MrcData, Channel, Rank, Byte, TxDqsDelay, WriteToCache, &GetSetVal);
            // GetSetVal += 32;
            // MrcGetSetDdrIoGroupStrobe (MrcData, Channel, Rank, Byte, TxDqDelay, WriteToCache, &GetSetVal);
            // Save the results from the current rank into a global array
            FinalDqs[Controller][Channel][Rank][Byte] = (UINT32) LargestPassingStart[Controller][Channel][Byte];
          } // for Byte
          MrcFlushRegisterCachedData (MrcData);
#ifdef MRC_DEBUG_PRINT
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.C%u.R%u:\tDqsPi\tDqPi\n",Controller, Channel, Rank);
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              if (MrcByteExist (MrcData, Controller, Channel, Byte)) {
                // MrcGetSetDdrIoGroupStrobe (MrcData, Channel, Rank, Byte, TxDqsDelay, ReadFromCache, &GetSetVal);
                // MrcGetSetDdrIoGroupStrobe (MrcData, Channel, Rank, Byte, TxDqDelay, ReadFromCache, &GetSetVal2);
                MRC_DEBUG_MSG (
                  Debug,
                  MSG_LEVEL_NOTE,
                  "  B%d:\t%u\t%u\n",
                  Byte,
                  (UINT32) FinalDqs[Controller][Channel][Rank][Byte],     // GetSetVal,
                  (UINT32) FinalDqs[Controller][Channel][Rank][Byte] + (UINT32) TxDqDqsDelayAdj // GetSetVal2
                );
              }
            }
#endif
        } // rank exist
      } // for Channel
    } //Controller

    if (Gear2) {
      MrcWait (MrcData, (WaitTime * MRC_TIMER_1NS) / 1000);
    }

    // Restore EnDQSOdtParkMode
    GetSetVal = EnDqsOdtParkMode;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataDqsOdtParkMode, WriteToCache, &GetSetVal);

    // Exit Write Leveling Mode and Re-enable TxPi Power Down.
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxPiPwrDnDis,       WriteToCache, &GetSetDis);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocWriteLevelMode,     WriteNoCache, &WriteLevelDis);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteNoCache, &GetSetDis);
    MrcFlushRegisterCachedData (MrcData);

    IoReset (MrcData);
  } // for Rank

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = 1 << Rank;
    if (!(RankMask & ValidRankMask)) {
      continue;
    }
    for (Controller = 0 ; Controller < MAX_CONTROLLER ; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (MrcByteExist (MrcData, Controller, Channel, Byte)) {
            GetSetVal = (INT64) FinalDqs[Controller][Channel][Rank][Byte];
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, WriteToCache, &GetSetVal);
            GetSetVal += TxDqDqsDelayAdj;
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay, WriteToCache, &GetSetVal);
          }
        }
      } // Channel
    } // Controller
  } // for Rank
  MrcFlushRegisterCachedData (MrcData);

  // Clean up after Test
  // Restore DqControl2 values.
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocWlLongDelEn, WriteToCache, &GetSetDis);

  // Restore BiasPMCtrl
  GetSetVal = BiasPMCtrl;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocBiasPMCtrl, WriteToCache, &GetSetVal);

  // Restore EnDQSOdtParkMode
  GetSetVal = EnDqsOdtParkMode;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataDqsOdtParkMode, WriteToCache, &GetSetVal);

  // Restore DisDataIdlClkGate value
  MrcGetSetNoScope (MrcData, GsmIocDisDataIdlClkGate, WriteToCache, &DisDataIdlClkGateSave);
  MrcFlushRegisterCachedData (MrcData);

  if (Ddr4) {
    // Disable Qoff on all ranks
    // DLLEnable=1, Dic=0, Al=0, Level=0, Tdqs=0, Qoff=0
    MrcSetMR1_DDR4 (MrcData, 1, INIT_DIMM_RON_34, 0, 0, 0);
  }

  // Disable Rank Mux Override
  MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideEn, WriteNoCache, &GetSetDis);

  // Restore flag
  Outputs->RestoreMRs = SavedRestoreMRS;

  return MainStatus;
}

/**
  This function is a wrapper for WriteLevelingFlyBy executed from the CallTable.
  This allows us to apply workarounds and multi-D implementations of WriteLevelingFlyby.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus -  if it succeeds return mrcSuccess
**/
MrcStatus
MrcWriteLevelingFlyByTraining (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcStatus Status;
  INT64     RdRdsg;
  INT64     RdRddg;
  INT64     RdWrsg;
  INT64     RdWrdg;

  MrcRelaxReadToReadSameRank  (MrcData, TRUE, &RdRdsg, &RdRddg);
  MrcRelaxReadToWriteSameRank (MrcData, TRUE, &RdWrsg, &RdWrdg);
  Status = WriteLevelingFlyBy (MrcData);
  MrcRelaxReadToReadSameRank  (MrcData, FALSE, &RdRdsg, &RdRddg);
  MrcRelaxReadToWriteSameRank (MrcData, FALSE, &RdWrsg, &RdWrdg);

  return Status;
}

/**
  this function execute the Jedec WCK training.
  Center WCK2CK timing

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus -  if it succedes return mrcSuccess
**/
MrcStatus
MrcJedecWCKTraining (
  IN OUT MrcParameters *const MrcData
  )
{
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcIntOutput      *IntOutputs;
  MrcIntCmdTimingOut  *IntCmdTiming;
  MrcChannelOut     *ChannelOut;
  const MRC_FUNCTION  *MrcCall;
  MrcStatus         Status;
  INT64             GetSetVal;
  INT64             WriteLevelEn;
  INT64             WriteLevelDis;
  INT64             GetSetEn;
  INT64             GetSetDis;
  INT64             DataTrainFeedbackField;
  INT64             SaveBiasPmCtrl;
  INT64             SaveInternalClocksOn;
  INT64             SaveForceRxOn;
  UINT32            Controller;
  UINT32            Channel;
  UINT32            Rank;
  UINT32            OrigCccPiDivider[MRC_NUM_CCC_INSTANCES];
  UINT32            OrigWckControl1;
  UINT32            WckPulseWait;
  UINT32            WckPulseWaitDelta;
  UINT16            Qclkps;
  UINT8             MaxChannel;
  UINT8             Byte;
  UINT8             RankMask;  // RankBitMask for all controllers/channels
  UINT8             ValidRankMask;
  UINT16            WckStart;
  UINT16            WckStop;
  UINT16            WckPi;
  UINT16            MaxWckValue;
  UINT8             WckStep;
  UINT32            CRValue;
  UINT32            DataTrainFeedback;
  UINT8             WckStatus[MAX_CONTROLLER][MAX_CHANNEL][CH2CCC_CR_PICODE3_mdll_cmn_pi6code_MAX]; //BitMask of passing Bit0 - Rank0/Byte0, Bit1 - Rank0/Byte1, Bit2 - Rank1/Byte0...
  UINT8             WckStatusPassMask[MAX_CONTROLLER][MAX_CHANNEL]; //BitMask = Bit0 - Rank0/Byte0, Bit1 - Rank0/Byte1, Bit2 - Rank1/Byte0...
  UINT8             PassMask;
  INT32             InitialPassingStart[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             InitialPassingEnd[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             CurrentPassingStart[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             CurrentPassingEnd[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             LargestPassingStart[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             LargestPassingEnd[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             *InitialPassingStartPtr;
  INT32             *InitialPassingEndPtr;
  INT32             *CurrentPassingStartPtr;
  INT32             *CurrentPassingEndPtr;
  INT32             *LargestPassingStartPtr;
  INT32             *LargestPassingEndPtr;
  UINT32            FinalWckPi[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             iWidth;
  INT32             cWidth;
  INT32             lWidth;
  INT32             ChannelWidth;
  UINT32            Index;
  UINT32            CccFub;
  UINT32            Offset;
  BOOLEAN           Pass;
  BOOLEAN           SavedRestoreMRS;
  BOOLEAN           Gear2;
  BOOLEAN           Gear4;
  BOOLEAN           UlxUlt;
  char              *BytesHeader;
  UINT32            SaveCaValidPiGateDisable[MRC_NUM_CCC_INSTANCES];
  MrcModeRegister                       MrAddress;
  MrcModeRegisterIndex                  Mr18ArrayIndex;
  MCMISCS_DDRWCKCONTROL_STRUCT          WckControl;
  MCMISCS_DDRWCKCONTROL_STRUCT          OrigWckControl;
  MCMISCS_DDRWCKCONTROL_STRUCT          WckControlPreIoReset;
  CH0CCC_CR_DDRCRCCCPIDIVIDER_STRUCT    CccPiDivider;
  MCMISCS_DDRWCKCONTROL1_STRUCT         WckControl1;
  CH0CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT  CccClkControls;

  Outputs       = &MrcData->Outputs;
  if (Outputs->DdrType != MRC_DDR_TYPE_LPDDR5) {
    return mrcSuccess;
  }

  Inputs        = &MrcData->Inputs;
  Debug         = &Outputs->Debug;
  MrcCall       = Inputs->Call.Func;
  Status        = mrcSuccess;
  CRValue       = 0;
  ValidRankMask = Outputs->ValidRankMask;
  GetSetEn      = 1;
  GetSetDis     = 0;
  WriteLevelEn  = MrcWriteLevelLpddr5;
  WriteLevelDis = MrcWriteLevelOff;
  MaxChannel    = Outputs->MaxChannels;
  Qclkps        = Outputs->Qclkps;
  WckStart      = 0;
  Gear2         = Outputs->Gear2;
  Gear4         = Outputs->Gear4;
  UlxUlt        = Inputs->UlxUlt;
  IntOutputs    = (MrcIntOutput *) (MrcData->IntOutputs.Internal);

  SaveBiasPmCtrl       = 0xFF;
  SaveInternalClocksOn = 0xFF;
  SaveForceRxOn        = 0xFF;

  SavedRestoreMRS = Outputs->RestoreMRs;
  Outputs->RestoreMRs = FALSE;
  MrcCall->MrcSetMem ((UINT8 *) WckStatus, sizeof (WckStatus), 0);
  MrcCall->MrcSetMem ((UINT8 *) WckStatusPassMask, sizeof (WckStatusPassMask), 0);
  MrcCall->MrcSetMem ((UINT8 *) OrigCccPiDivider, sizeof (OrigCccPiDivider), 0);
  MrcCall->MrcSetMem ((UINT8 *) SaveCaValidPiGateDisable, sizeof (SaveCaValidPiGateDisable), 0);

  MrcGetSetLimits (MrcData, WckGrpPi, NULL, &GetSetVal, NULL);
  MaxWckValue = (UINT16) GetSetVal + 1;

  MrAddress = mrMR18;
  Mr18ArrayIndex = MrcMrAddrToIndex (MrcData, &MrAddress);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Disable WCK Termination to all detected Ranks\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
          CRValue = (ChannelOut->Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR[Mr18ArrayIndex]);
          if ((CRValue & 0x7) != 0) {
            //Disable WCK Termination for all detected Ranks
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR18, (CRValue & ~0x7), TRUE);
          }
          // Setup PassMask for later comparison
          WckStatusPassMask[Controller][Channel] |= (0x3 << (Rank * 2));//Tie 2 to MaxByteInLpChannel?
        }
      }
    }  // for Channel
  } // Controller
  MrcBlockTrainResetToggle (MrcData, FALSE);

  //Sweep +/-0.25 CK
  WckStart = Gear4 ? 128 : 320;

  WckStop  = WckStart + 384;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "WCK Start = 0x%x, WCK Stop = 0x%x\n", WckStart, WckStop);


  WckControl1.Data = MrcReadCR(MrcData, MCMISCS_DDRWCKCONTROL1_REG);
  OrigWckControl1 = WckControl1.Data;
  WckControl1.Bits.TrainWCkEn = 1;
  WckControl1.Bits.TrainWCkSyncRatio = Gear4 ? 1 : 2;
  MrcWriteCR (MrcData, MCMISCS_DDRWCKCONTROL1_REG, WckControl1.Data);

  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    CccFub = UlxUlt ? Index : CccIndexToFub[Index];
    DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
    if (!MrcChannelExist (MrcData, Controller, Channel)) {
      continue;
    }
    Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG, CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG, CccFub);
    CccClkControls.Data = MrcReadCR(MrcData, Offset);
    SaveCaValidPiGateDisable[Index] = CccClkControls.Bits.CaValidPiGateDisable;
    CccClkControls.Bits.CaValidPiGateDisable  = 0;
    MrcWriteCR(MrcData, Offset, CccClkControls.Data);
  }

  // BGF is disabled when Dclk_enable is cleared
  // If PM_CONTROL_Dclk_en_reset_bgf_run bit is set to 0, BGF will continure to run even if dclk_enable is cleared
  // Program PM_CONTROL_Dclk_en_reset_bgf_run to 0
  MrcToggleBgfRun (MrcData, FALSE);
  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableDclk, WriteNoCache, &GetSetDis);
  MrcWait (MrcData, 27 * MRC_TIMER_1NS);
  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableDclk, WriteNoCache, &GetSetEn);
  // Program PM_CONTROL_Dclk_en_reset_bgf_run back to 1
  MrcToggleBgfRun (MrcData, TRUE);

  WckControl.Data = MrcReadCR (MrcData, MCMISCS_DDRWCKCONTROL_REG);
  OrigWckControl.Data = WckControl.Data;
  WckControl.Bits.TrainWCkBL = 12;
  WckControl.Bits.TrainWCkMask = Gear4 ? 3 : 4; // Assuming default as Gear2
  WckControlPreIoReset.Data = WckControl.Data;
  WckControl.Bits.WCkDiffLowInIdle = 1;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (MrcByteExist (MrcData, Controller, Channel, Byte)) {
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocBiasPMCtrl, ReadCached, &SaveBiasPmCtrl);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocForceRxAmpOn, ReadCached, &SaveForceRxOn);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocInternalClocksOn, ReadCached, &SaveInternalClocksOn);
          break;
        }
      }
    }
  }

  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    CccFub = UlxUlt ? Index : CccIndexToFub[Index];
    DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
    if (!MrcChannelExist (MrcData, Controller, Channel)) {
      continue;
    }
    Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRCCCPIDIVIDER_REG, CH0CCC_CR_DDRCRCCCPIDIVIDER_REG, CccFub);
    CccPiDivider.Data = MrcReadCR (MrcData, Offset);
    OrigCccPiDivider[Index] = CccPiDivider.Data;
    CccPiDivider.Bits.Pi4DivEn      = Gear4 ? 0 : 1;
    CccPiDivider.Bits.Pi4Inc        = Gear4 ? 0 : 2;
    CccPiDivider.Bits.ccc_Spare1    = Gear4 ? 1 : 0;
    CccPiDivider.Bits.wck2cktrainen = Gear4 ? 1 : 0;
    MrcWriteCR(MrcData, Offset, CccPiDivider.Data);
  }


  WckStep = 2;

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = (1 << Rank);
    if (!(RankMask & ValidRankMask)) {
      // Skip if all channels empty
      continue;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank %d\n", Rank);

    MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocWriteLevelMode, WriteNoCache, &WriteLevelEn);
    MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteNoCache, &GetSetEn);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocBiasPMCtrl, WriteNoCache, &GetSetDis);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteNoCache, &GetSetDis);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocInternalClocksOn, WriteNoCache, &GetSetEn);

    WckPulseWait = (Qclkps * (1 << WckControl.Bits.TrainWCkBL)) + (5 * Qclkps);
    // We would need to wait (qclk * 4096 ps) for reading response
    WckPulseWaitDelta = Qclkps * 3096;
    WckPulseWaitDelta = DIVIDECEIL(WckPulseWaitDelta, 1000);
    WckPulseWait = DIVIDECEIL (WckPulseWait, 1000);
    // ******************************************
    // Find the rising edge of the high phase.
    // ******************************************
    MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "\n\tMc0Ch0\t\tMc0Ch1\t\tMc0Ch2\t\tMc0Ch3\t\tMc1Ch0\t\tMc1Ch1\t\tMc1Ch2\t\tMc1Ch3\n"
        );
    BytesHeader = "\t0\t1";
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "WckPi%s%s", BytesHeader, BytesHeader);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);

    for (WckPi = WckStart; WckPi < WckStop; WckPi += WckStep) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n %d:", WckPi);

      GetSetVal = WckPi;
      MrcGetSetCcc (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MRC_IGNORE_ARG, 0, WckGrpPi, ForceWriteCached, &GetSetVal);

      Status = IoReset (MrcData);

      MrcWriteCR (MrcData, MCMISCS_DDRWCKCONTROL_REG, WckControl.Data);
      // set WCkDiffLowInIdle shadow copy
      if (!Inputs->A0) {
        GetSetVal = WckControl.Bits.WCkDiffLowInIdle;
        MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccWCKDiffLowInIdle, WriteCached, &GetSetVal);
      }

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
            ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
            // Enable WL mode in MR18[6] and set CKR (WCK to CK freq ratio) to 2:1 MR18[7]..
            //MR18[5] needs to match MR18[7], this is not valid anymore as its reserved as per Spec
            CRValue = (ChannelOut->Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR[Mr18ArrayIndex]);
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR18, (CRValue | 0xC0), TRUE); //@todo make this an memory api usage
          }
        }  // for Channel
      } // Controller

      // Probably do not need to specifically wait tWLMRD which is MAX (14ns, 5tCK)
      MrcWait (MrcData, 27 * MRC_TIMER_1NS);
      WckControl.Bits.TrainWCkPulse = 1;
      MrcWriteCR (MrcData, MCMISCS_DDRWCKCONTROL_REG, WckControl.Data);

//      Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT; // 10 seconds timeout
//      do {
//        WckControl.Data = MrcReadCR (MrcData, MCMISCS_DDRWCKCONTROL_REG);
//      } while (WckControl.Bits.TrainWCkPulse && (MrcCall->MrcGetCpuTime () < Timeout));
//      MrcWait (MrcData, 27 * MRC_TIMER_1NS);
      MrcWait (MrcData, WckPulseWait * MRC_TIMER_1NS);

      WckControl.Bits.TrainWCkPulse = 0;
      MrcWriteCR (MrcData, MCMISCS_DDRWCKCONTROL_REG, WckControl.Data);

      // Update results for all Controllers/Channels/Bytes
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t");
            continue;
          }
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackField);
            DataTrainFeedback = (UINT32) DataTrainFeedbackField;
            Pass = (DataTrainFeedback >= 16);
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%c%d", Pass ? '.' : '#', DataTrainFeedback);
            WckStatus[Controller][Channel][WckPi] |= Pass ? (1 << ((Rank * 2) + Byte)) : 0;
          }
          // Disable WL mode in MR18[6]
          CRValue = (ChannelOut->Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR[Mr18ArrayIndex]);
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR18, CRValue, TRUE);
        } // for Channel
      } // Controller
      MrcWriteCR (MrcData, MCMISCS_DDRWCKCONTROL_REG, WckControlPreIoReset.Data);

      // set WCkDiffLowInIdle shadow copy with same settings
      if (!Inputs->A0) {
        GetSetVal = WckControlPreIoReset.Bits.WCkDiffLowInIdle;
        MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccWCKDiffLowInIdle, WriteCached, &GetSetVal);
      }
    } // for WckPi

    // Clean up after step
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

          // Restore MR18 values
          CRValue = (ChannelOut->Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR[Mr18ArrayIndex]);
          //Disable WCK Termination for current rank to allow subsequent rank
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR18, (CRValue & ~0x7), TRUE);
        } // rank exist
      } // for Channel
    } //Controller

    // Exit Write Leveling Mode and Re-enable TxPi Power Down.
    MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxPiPwrDnDis, WriteToCache, &GetSetDis);
    MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocWriteLevelMode, WriteNoCache, &WriteLevelDis);
    MrcGetSetStrobe(MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteNoCache, &GetSetDis);
    MrcFlushRegisterCachedData (MrcData);

    Status = IoReset (MrcData);
  } // for Rank

  //Aggregate the results within each Channel (Specific to TGL design)
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      PassMask = WckStatusPassMask[Controller][Channel];
      InitialPassingStartPtr = &InitialPassingStart[Controller][Channel];
      InitialPassingEndPtr = &InitialPassingEnd[Controller][Channel];
      CurrentPassingStartPtr = &CurrentPassingStart[Controller][Channel];
      CurrentPassingEndPtr = &CurrentPassingEnd[Controller][Channel];
      LargestPassingStartPtr = &LargestPassingStart[Controller][Channel];
      LargestPassingEndPtr = &LargestPassingEnd[Controller][Channel];

      for (WckPi = WckStart; WckPi < WckStop; WckPi += WckStep) {
        Pass = ((WckStatus[Controller][Channel][WckPi] & PassMask) == PassMask);
        if (WckPi == WckStart) {
          if (Pass) {
            *InitialPassingStartPtr = *InitialPassingEndPtr = WckStart;
            *CurrentPassingStartPtr = *CurrentPassingEndPtr = WckStart;
            *LargestPassingStartPtr = *LargestPassingEndPtr = WckStart;
          } else {
            *InitialPassingStartPtr = *InitialPassingEndPtr = -WckStep;
            *CurrentPassingStartPtr = *CurrentPassingEndPtr = -WckStep;
            *LargestPassingStartPtr = *LargestPassingEndPtr = -WckStep;
          }
        } else {
          if (Pass) {
            if (*InitialPassingEndPtr == (WckPi - WckStep)) {
              *InitialPassingEndPtr = WckPi;
            }

            if (*CurrentPassingEndPtr == (WckPi - WckStep)) {
              *CurrentPassingEndPtr = WckPi;
            } else {
              *CurrentPassingStartPtr = *CurrentPassingEndPtr = WckPi;
            }
            // Special case for last step: Append Initial Passing Region
            // WckPi should be considered a continuous range that wraps around 0
            if ((WckPi >= (WckStop - WckStep)) && (*InitialPassingStartPtr == WckStart)) {
              iWidth = (*InitialPassingEndPtr - *InitialPassingStartPtr);
              *CurrentPassingEndPtr += (WckStep + iWidth);
            }
            // Update Largest variables
            cWidth = *CurrentPassingEndPtr - *CurrentPassingStartPtr;
            lWidth = *LargestPassingEndPtr - *LargestPassingStartPtr;
            if (cWidth > lWidth) {
              *LargestPassingStartPtr = *CurrentPassingStartPtr;
              *LargestPassingEndPtr = *CurrentPassingEndPtr;
            }
          }
        }
      } // for WckPi
    } // for Channel
  } // for Controller

#ifdef MRC_DEBUG_PRINT
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\t\tInitPassSt\tInitPassEn\tCurrPassSt\tCurrPassEn\tLargPassSt\tLargPassEn\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          " Mc%d.C%d:\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n",
          Controller,
          Channel,
          InitialPassingStart[Controller][Channel],
          InitialPassingEnd[Controller][Channel],
          CurrentPassingStart[Controller][Channel],
          CurrentPassingEnd[Controller][Channel],
          LargestPassingStart[Controller][Channel],
          LargestPassingEnd[Controller][Channel]
        );
      }
    } // Channel
  } //Controller
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\tLftEdge Width\n");
#endif // MRC_DEBUG_PRINT

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelWidth = LargestPassingEnd[Controller][Channel] - LargestPassingStart[Controller][Channel];
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        " Mc%d.C%d:\t%d\t%d\n",
        Controller,
        Channel,
        LargestPassingStart[Controller][Channel],
        ChannelWidth
        );
      if (ChannelWidth <= MIN_WCK_EYE_WIDTH) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "No Sufficient Eye width for WCK Training\t Required Minimum Eye Width %d\n", MIN_WCK_EYE_WIDTH);
        return mrcFail;
      }

      FinalWckPi[Controller][Channel] = (UINT32) (LargestPassingStart[Controller][Channel]) % MaxWckValue;
      if ((FinalWckPi[Controller][Channel] >= 256) && Gear2) {
        FinalWckPi[Controller][Channel] = (UINT32) (LargestPassingStart[Controller][Channel] - 256 ) ; //Update from design team, as the new pi settings, shifts by 2clks
      }

#ifdef MRC_DEBUG_PRINT
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\tWckPi\n");
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        " Mc%u.C%u:\t%u\n",
        Controller,
        Channel,
        (UINT32) FinalWckPi[Controller][Channel]
        );
#endif
    } // for Channel
  } //Controller

  for (Controller = 0 ; Controller < MAX_CONTROLLER ; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      IntCmdTiming = &IntOutputs->Controller[Controller].CmdTiming[Channel];
      GetSetVal = (INT64) FinalWckPi[Controller][Channel];
      MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, WckGrpPi, WriteToCache | PrintValue, &GetSetVal);
      IntCmdTiming->WckPiCode = (UINT16) FinalWckPi[Controller][Channel];
    } // Channel
  } // Controller
  MrcFlushRegisterCachedData (MrcData);
  Status = IoReset (MrcData);

  MrcBlockTrainResetToggle (MrcData, TRUE);

  // Clean up after Test
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
          // Restore MR18
          CRValue = (ChannelOut->Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR[Mr18ArrayIndex]);
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR18, CRValue, TRUE);
        }
      }
    }  // for Channel
  } // Controller

  // Restore cached value
  MrcWriteCR (MrcData, MCMISCS_DDRWCKCONTROL_REG, OrigWckControl.Data);
  MrcWriteCR (MrcData, MCMISCS_DDRWCKCONTROL1_REG, OrigWckControl1);

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocBiasPMCtrl,       WriteNoCache, &SaveBiasPmCtrl);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn,     WriteNoCache, &SaveForceRxOn);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocInternalClocksOn, WriteNoCache, &SaveInternalClocksOn);

  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    CccFub = UlxUlt ? Index : CccIndexToFub[Index];
    DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
    if (!MrcChannelExist (MrcData, Controller, Channel)) {
      continue;
    }
    Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRCCCPIDIVIDER_REG, CH0CCC_CR_DDRCRCCCPIDIVIDER_REG, CccFub);
    MrcWriteCR(MrcData, Offset, OrigCccPiDivider[Index]);

    Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG, CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG, CccFub);
    CccClkControls.Data = MrcReadCR(MrcData, Offset);
    CccClkControls.Bits.CaValidPiGateDisable  = SaveCaValidPiGateDisable[Index];
    MrcWriteCR(MrcData, Offset, CccClkControls.Data);
  }

  // Restore flag
  Outputs->RestoreMRs = SavedRestoreMRS;

  return Status;
}

/**
  This function Backups or restores the initial WL settings

  @param[in, out] MrcData            - Include all MRC global data.
  @param[in, out] InitialWlSettings  - pointer to Inital WL settiigs
  @param[in]      BackupRestoreFlag  - Flag to indicate backup or restore

  @retval MrcStatus -  if it succeeds return mrcSuccess
**/
MrcStatus
BackupRestoreWlInitialSettings (
  IN OUT MrcParameters                      *const MrcData,
  IN OUT DDR5_WRLV_INITIAL_SETTINGS_STRUCT  *InitialWlSettings,
  IN BOOLEAN                                BackupRestoreFlag
  )
{
  INT64                       GetSetVal;
  UINT32                      Offset;
  UINT32                      FirstController;
  UINT32                      FirstChannel;
  UINT32                      Controller;
  UINT32                      Channel;
  UINT32                      Rank;
  UINT32                      Strobe;
  UINT8                       tWPRE;
  MrcOutput                   *Outputs;
  MrcDebug                    *Debug;
  DATA0CH0_CR_AFEMISC_STRUCT  DataAfeMisc;
  DATA_CR_SRZCTL_STRUCT       DataSrzCtl;

  Outputs         = &MrcData->Outputs;
  Debug           = &Outputs->Debug;
  FirstController = (MrcControllerExist (MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;

  if (InitialWlSettings == NULL) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s - Missing input/output pointers!\n", gErrString);
    return mrcFail;
  }

  tWPRE = MrcGetWpre (MrcData); // Write Preamble in nCK units (not DDR5_MR8_TWPRE enum)

  // @todo optimize the code - use same getset for backup/restore
  if (BackupRestoreFlag == WRLV_BACKUP_INITIAL_SETTINGS) {
    // Read from first populated byte and broadcast to all
    Controller = FirstController;
    Channel = FirstChannel;
    Strobe = 0;    // Rank is not used here
    MrcTranslateSystemToIp (MrcData, &Controller, &Channel, &Rank, &Strobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
    Offset  = DATA0CH0_CR_AFEMISC_REG +
      (DATA0CH1_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * Channel +
      (DATA1CH0_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * Strobe;

    DataAfeMisc.Data = MrcReadCR (MrcData, Offset);
    InitialWlSettings->RxLpddrMode = (UINT8) DataAfeMisc.Bits.rxall_cmn_rxlpddrmode;
    // Set to zero for JWL Training
    DataAfeMisc.Bits.rxall_cmn_rxlpddrmode = 0;
    MrcWriteCrMulticast (MrcData, DATA_CR_AFEMISC_REG, DataAfeMisc.Data);

    // No GetSet for this register, so use another per-byte PHY CR from above for Channel/Strobe translation
    Offset = DATA0CH0_CR_SRZCTL_REG +
      (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * Channel +
      (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * Strobe;

    DataSrzCtl.Data = MrcReadCR(MrcData, Offset);
    InitialWlSettings->DqsBufCmnBonus = (UINT8) DataSrzCtl.Bits.dqsbuf_cmn_bonus;
    DataSrzCtl.Bits.dqsbuf_cmn_bonus = 2;
    MrcWriteCrMulticast(MrcData, DATA_CR_SRZCTL_REG, DataSrzCtl.Data);

    // Save initial values
    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocBiasPMCtrl,         ReadFromCache, &GetSetVal);
    InitialWlSettings->BiasPMCtrl = (UINT8) GetSetVal;
    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocDataDqsOdtParkMode, ReadFromCache, &GetSetVal);
    InitialWlSettings->EnDqsOdtParkMode = (UINT8) GetSetVal;
    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocDataWrPreamble,     ReadFromCache, &GetSetVal);
    InitialWlSettings->wrpreamble = (UINT8) GetSetVal;
    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocDqsMaskPulseCnt,    ReadFromCache, &GetSetVal);
    InitialWlSettings->DdrCRMaskCntPulseNumStart = (UINT8) GetSetVal;
    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocDqsPulseCnt,        ReadFromCache, &GetSetVal);
    InitialWlSettings->DdrCRNumOfPulses = (UINT8) GetSetVal;
    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocInternalClocksOn,   ReadFromCache, &InitialWlSettings->InternalClocksOn);
    MrcGetSetMcCh (MrcData, FirstController, FirstChannel, GsmMctWRWRsg, ReadFromCache, &GetSetVal);
    InitialWlSettings->tWRWR_sg = (UINT8) GetSetVal;
    MrcGetSetMcCh (MrcData, FirstController, FirstChannel, GsmMctWRWRdg, ReadFromCache, &GetSetVal);
    InitialWlSettings->tWRWR_dg = (UINT8) GetSetVal;

    // Need to save current CL in the DIMM if tWPRE is 4tCK
    if (tWPRE == 4) {
      MrcGetSetMcCh (MrcData, FirstController, FirstChannel, GsmMctCL, ReadCached, &GetSetVal);
      InitialWlSettings->tCL = (UINT8) GetSetVal;
      if (InitialWlSettings->tCL < 30) {
        MrcDdr5SetCasLatency (MrcData, MRC_DDR5_CL_30);
      }
    }
  } else { // WRLV_RESTORE_INITIAL_SETTINGS
    // restore initial settings
    // Read from first populated byte and broadcast to all
    Controller = FirstController;
    Channel = FirstChannel;
    Strobe = 0;    // Rank is not used here
    MrcTranslateSystemToIp (MrcData, &Controller, &Channel, &Rank, &Strobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
    Offset  = DATA0CH0_CR_AFEMISC_REG +
      (DATA0CH1_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * Channel +
      (DATA1CH0_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * Strobe;

    DataAfeMisc.Data = MrcReadCR (MrcData, Offset);
    DataAfeMisc.Bits.rxall_cmn_rxlpddrmode = InitialWlSettings->RxLpddrMode;
    MrcWriteCrMulticast (MrcData, DATA_CR_AFEMISC_REG, DataAfeMisc.Data);

    // No GetSet for this register, so use another per-byte PHY CR from above for Channel/Strobe translation
    Offset = DATA0CH0_CR_SRZCTL_REG +
      (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * Channel +
      (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * Strobe;

    DataSrzCtl.Data = MrcReadCR(MrcData, Offset);
    DataSrzCtl.Bits.dqsbuf_cmn_bonus = InitialWlSettings->DqsBufCmnBonus;
    MrcWriteCrMulticast(MrcData, DATA_CR_SRZCTL_REG, DataSrzCtl.Data);

    GetSetVal = InitialWlSettings->BiasPMCtrl;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocBiasPMCtrl,         WriteToCache, &GetSetVal);
    GetSetVal = InitialWlSettings->EnDqsOdtParkMode;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataDqsOdtParkMode, WriteToCache, &GetSetVal);
    GetSetVal = InitialWlSettings->wrpreamble;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataWrPreamble,     WriteToCache, &GetSetVal);
    GetSetVal = InitialWlSettings->DdrCRMaskCntPulseNumStart;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqsMaskPulseCnt,    WriteToCache, &GetSetVal);
    GetSetVal = InitialWlSettings->DdrCRNumOfPulses;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqsPulseCnt,        WriteToCache, &GetSetVal);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocInternalClocksOn,   WriteToCache, &InitialWlSettings->InternalClocksOn);
    GetSetVal = InitialWlSettings->tWRWR_sg;
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMctWRWRsg, WriteToCache, &GetSetVal);
    GetSetVal = InitialWlSettings->tWRWR_dg;
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMctWRWRdg, WriteToCache, &GetSetVal);

    // Need to restore current CL in the DIMM if tWPRE is 4tCK
    if (tWPRE == 4) {
      MrcDdr5SetCasLatency (MrcData, InitialWlSettings->tCL);
    }

    // Disable feedback propagation to DataTrainFeedback
    GetSetVal = 0;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteToCache, &GetSetVal);

    MrcFlushRegisterCachedData (MrcData);
  }

  return mrcSuccess;
}

/**
  Configures DDRIO registers for DDR5 Write Leveling training

  @param[in, out] MrcData           - Include all MRC global data.
**/
VOID
SetupDdrioDdr5WrLvl (
  IN OUT MrcParameters *const MrcData
  )
{
  INT64 GetSetDis;
  INT64 GetSetEn;
  INT64 GetSetVal;

  GetSetEn  = 1;
  GetSetDis = 0;

  // TGL_MRC_HAS_1.0:
  // Enable DQS_P/N to drive differential low during idle periods (Park Mode, DDRCRDATACONTROL0.endqsodtparkmode = 2'b01)
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataDqsOdtParkMode, WriteToCache, &GetSetEn);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocBiasPMCtrl,         WriteToCache, &GetSetDis);

  // Enable feedback propagation to DataTrainFeedback
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteToCache, &GetSetEn);

  // Enable DDR5 Write Leveling training mode in each data byte (DATATRAINFEEDBACK.wltrainingmode=2'b10)
  // This will force DQS TX on and DQ TX off; force DQ RX on and ODT on.
  GetSetVal = MrcWriteLevelDdr5;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocWriteLevelMode, WriteToCache, &GetSetVal);

  // Mask out xxDQS pulses (2tCK write preamble with 1 functional DQS pulse)
  //   DDRCrDataControl3.WrPreamble = 1
  //   DDRCRCMDBUSTRAIN.DdrCRMaskCntPulseNumStart = 1
  //   DDRCRCMDBUSTRAIN.DdrCRNumOfPulses = 1
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataWrPreamble,  WriteToCache, &GetSetEn);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqsMaskPulseCnt, WriteToCache, &GetSetEn);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqsPulseCnt,     WriteToCache, &GetSetEn);

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocInternalClocksOn, WriteToCache, &GetSetEn);

  MrcFlushRegisterCachedData (MrcData);
}

/**
  Print Write Leveling training results

  @param[in, out] MrcData - Include all MRC global data.
**/
VOID
PrintTotalWlDelay (
  IN MrcParameters *const  MrcData,
  IN UINT32                WrLvlTotPiDly[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
#if 1
#if ((defined MRC_JEDEC_WRLVL_DEBUG) && (MRC_JEDEC_WRLVL_DEBUG == SUPPORT))
  MrcOutput *Outputs;
  MrcDebug  *Debug;
  UINT32 Controller;
  UINT32 Channel;
  UINT32 Rank;
  UINT32 Byte;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  MRC_RCVEN_DEBUG_MSG (Debug, MSG_LEVEL_ALGO, "Total Write Leveling Delay\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            continue;
          }
          MRC_RCVEN_DEBUG_MSG (Debug, MSG_LEVEL_ALGO, "Mc%u.C%u.R%u.B%u: %u\n", Controller, Channel, Rank, Byte, WrLvlTotPiDly[Controller][Channel][Rank][Byte]);
        }
      }
    }
  }
#endif // ((defined MRC_JEDEC_WRLVL_DEBUG) && (MRC_JEDEC_WRLVL_DEBUG == SUPPORT))
#endif // if 0
}

/**
  This function finds the Minimum TX delays per channel and per Rank

  @param[in, out] MrcData       - Include all MRC global data.
  @param[in]      WrLvlTotPiDly - Pointer to total write leveling Pi delays
  @param[out]     WrLvlChMin    - Minimum WL delay per channel array
  @param[out]     WrLvlRankMin  - Minimum WL delay per rank array

  @retval VOID
**/
VOID
MrcFindMinWrLvlDelays (
  IN OUT MrcParameters *const MrcData,
  IN  UINT32           WrLvlTotPiDly[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM],
  OUT UINT32           WrLvlChMin[MAX_CONTROLLER][MAX_CHANNEL],
  OUT UINT32           WrLvlRankMin[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL]
  )
{
  UINT32              Controller;
  UINT32              Channel;
  UINT32              Byte;
  UINT32              Rank;
  MrcOutput           *Outputs;

  Outputs = &MrcData->Outputs;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          WrLvlRankMin[Controller][Channel][Rank] = MIN (WrLvlRankMin[Controller][Channel][Rank], WrLvlTotPiDly[Controller][Channel][Rank][Byte]);
          WrLvlChMin[Controller][Channel]         = MIN (WrLvlChMin[Controller][Channel],         WrLvlTotPiDly[Controller][Channel][Rank][Byte]);
        } // Byte
      } // Rank
    } // Channel
  } // Controller
}

/**
  Adjust the total WL PI delays by the delay provided

  @param[in, out] MrcData         - Include all MRC global data.
  @param[in]      PiAdjust        - Number of PIs to adjust
  @param[in, out] WrLvlTotPiDly   - Total WL Pi delays array
**/
VOID
AdjustWrLvlTotPiDly (
  IN OUT MrcParameters *const MrcData,
  IN INT32                    PiAdjust,
  IN UINT32                   WrLvlTotPiDly[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  UINT32      Controller;
  UINT32      Channel;
  UINT32      Byte;
  UINT32      Rank;
  MrcOutput   *Outputs;

  Outputs     = &MrcData->Outputs;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          WrLvlTotPiDly[Controller][Channel][Rank][Byte] += PiAdjust;
        } // Byte
      } // Rank
    } // Channel
  } // Controller
}

/**
  This function Walks TxDqs Pi Delay forward til feedback is 1.

  @param[in, out]   MrcData         - Include all MRC global data.
  @param[in]        WrLevelingMode  - Current WL mode - external or internal
  @param[in]        PiDelayStart    - PI value to start sweeping
  @param[in]        PiDelayEnd      - End PI sweep value
  @param[in, out]   WrLvlTotPiDly   - Total WL Pi delays array

  @retval MrcStatus -  if it succeeds return mrcSuccess
**/
MrcStatus
MrcWrLvlSweepTxDqs (
  IN OUT MrcParameters *const MrcData,
  IN Ddr5WrLvlMode      const WrLevelingMode,
  IN UINT32                   PiDelayStart,
  IN UINT32                   PiDelayEnd,
  IN UINT8                    StepSize,
  IN UINT32                   WrLvlTotPiDly[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  UINT32              Controller;
  UINT32              Channel;
  UINT32              Rank;
  UINT32              Feedback;
  UINT32              Result[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8               SampleCount;
  UINT8               Byte;
  UINT8               McChBitMask;
  UINT8               RankMask;
  UINT8               ValidRankMask;
  UINT32              PiDelay;
  UINT8               SdramCount;
  UINT8               MaxChannels;
  UINT16              BytePass[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16              ChMask[MAX_CONTROLLER][MAX_CHANNEL];
  INT64               GetSetVal;
  BOOLEAN             Pass;
  BOOLEAN             AllPass;
  Ddr5WrLvlMode       ExitMode;
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcDebug            *Debug;
  const MrcInput      *Inputs;
  const MRC_FUNCTION  *MrcCall;

  Outputs       = &MrcData->Outputs;
  Inputs        = &MrcData->Inputs;
  Debug         = &Outputs->Debug;
  MrcCall       = Inputs->Call.Func;
  ValidRankMask = Outputs->ValidRankMask;
  SdramCount    = Outputs->SdramCount;
  MaxChannels   = Outputs->MaxChannels;
  AllPass       = FALSE;

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = (1 << Rank);
    if (!(RankMask & ValidRankMask)) {
      // Skip if all empty channels
      continue;
    }

    // Enable Write Leveling mode on this rank
    MrcDdr5SetDramWrLvlMode (MrcData, Rank, WrLevelingMode);


    // Clear Tracking Variables
    MrcCall->MrcSetMem ((UINT8 *) BytePass, sizeof (BytePass), 0);

    McChBitMask = SelectReutRanksAll (MrcData, RankMask, FALSE, 0);

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank%u\nChannel\t0\t1\nByte\t", Rank);

    for (Byte = 0; Byte < SdramCount; Byte++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d  ", Byte);
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nTxDqs Offset\n");

    // Walk forward til feedback is 1 with TxDqs PI.
    for (PiDelay = PiDelayStart; PiDelay <= PiDelayEnd; PiDelay += StepSize) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", PiDelay);

      AllPass = TRUE;
      MrcCall->MrcSetMem ((UINT8 *) Result, sizeof (Result), 0);
      for (SampleCount = 0; SampleCount < DDR5_WL_MAX_SAMPLES;  SampleCount++) {
        RunIOTest (MrcData, McChBitMask, StaticPattern, 1, 0);
        MrcWait (MrcData, 20 * MRC_TIMER_1NS);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          if (!MrcControllerExist (MrcData, Controller)) {
            continue;
          }
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              continue;
            }
            for (Byte = 0; Byte < SdramCount; Byte++) {
              if (BytePass[Controller][Channel] & (1 << Byte)) {
                continue;
              }
              Feedback = ReadAfeMiscControl0Reg (MrcData, Controller, Channel, Byte);
              if (Feedback > 0) {
                Result[Controller][Channel][Byte]++;
              }
            } // Byte
          } // Channel
        } // Controller loop
      } // SampleCount


      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (!MrcControllerExist (MrcData, Controller)) {
          continue; // avoid printing end of line when controller is not present
        }
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }

          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
          ChMask[Controller][Channel] = ChannelOut->ValidByteMask;

          for (Byte = 0; Byte < SdramCount; Byte++) {
            // Skip Bytes that are done
            if (BytePass[Controller][Channel] & (1 << Byte)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "    ");
              continue;
            }

            // MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &GetSetVal);
            // DataTrainFeedback = (UINT32) GetSetVal;
            Pass = (Result[Controller][Channel][Byte] >= (DDR5_WL_MAX_SAMPLES / 2));
            if (Pass) {
              BytePass[Controller][Channel] |= (1 << Byte);
              WrLvlTotPiDly[Controller][Channel][Rank][Byte] += PiDelay;
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%c%2d ", Pass ? '.' : '#', Result[Controller][Channel][Byte]);
          } // Byte
          if (BytePass[Controller][Channel] != ChMask[Controller][Channel]) {
            AllPass = FALSE;
            GetSetVal = StepSize;
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, MAX_SDRAM_IN_DIMM, TxDqsDelay, WriteOffsetCached, &GetSetVal);
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
        } // Channel
      } // Controller
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

      if (AllPass == TRUE) {
        break;
      }
    } // PiDelay loop

    // Print the results and Check for Errors
#ifdef MRC_JEDEC_WRLVL_DEBUG
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        MRC_JEDEC_WRLVL_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "BytePass[Mc%d][Ch%d]: 0x%x\n", Controller, Channel, BytePass[Controller][Channel]);
      }
    }
#endif
    // Check if Edge was found for all Bytes in the channels
    if (!AllPass) {
#ifdef MRC_JEDEC_WRLVL_DEBUG
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_ERROR,
        "%s WL Pulse not found for all Bytes with following final results\n",
        gErrString
        );

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }

          for (Byte = 0; Byte < SdramCount; Byte++) {
            // ChMask is 0 for bytes that don't exist, and BytePass is 0 by default.
            // Bytes that don't exist will never be set in BytePass.
            // Thus small code savings skipping ByteExists here.
            if (((BytePass[Controller][Channel] ^ ChMask[Controller][Channel]) & (1 << Byte)) != 0) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s Check byte %u\n", gErrString, Byte);
            }
          }
        }
      }
#endif
      if (Inputs->ExitOnFailure) {
        return mrcWriteLevelingError;
      }
    }
    PrintTotalWlDelay (MrcData, WrLvlTotPiDly);

    // Disable Write Leveling mode on this rank
    if (WrLevelingMode == Ddr5WrLvlModeInternal) {
      ExitMode = Ddr5WrLvlModeFinal;
    } else {
      ExitMode = Ddr5WrLvlModeDisable;
    }
    MrcDdr5SetDramWrLvlMode (MrcData, Rank, ExitMode);
  } // Rank

  return mrcSuccess;
}

/**
  This function runs write leveling training for DDR5
  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus -  if it succeeds return mrcSuccess
**/
MrcStatus
MrcJedecWriteLevelingDdr5 (
  IN OUT MrcParameters *const MrcData
  )
{
  UINT32                      MaxDec;
  UINT32                      MaxAdd;
  UINT32                      Feedback;
  UINT32                      Result[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32                       StartOffset;
  INT32                       EndOffset;
  INT32                       CurrentOffset;
  INT64                       GetSetVal;
  UINT32                      WrLvlTotPiDly[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32                       PassingCycle[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32                      MaxDecDelay;
  UINT32                      WrLvlChMin[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32                      WrLvlRankMin[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT16                      BytePass[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16                      ChMask[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16                      SavedTxDqsVal[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8                       PassIntWlAdj[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8                       PassingIntCycle;
  UINT32                      tWL_ADJ_Start;
  UINT32                      tWL_ADJ_End;
  UINT32                      Controller;
  UINT32                      Channel;
  UINT32                      Rank;
  UINT32                      Byte;
  UINT8                       FineStep;
  UINT8                       RankMask;
  UINT8                       Device;
  UINT8                       McChBitMask;
  UINT8                       ValidRankMask;
  UINT8                       InternalWlCycle;
  UINT8                       UpperByteVal;
  UINT8                       LowerByteVal;
  UINT8                       SdramCount;
  UINT8                       DqsMapCpu2Dram;
  UINT8                       MaxChannels;
  UINT8                       RankHalf;
  UINT8                       SampleCount;
  UINT8                       tWPRE;
  UINT8                       NumDevices;
  BOOLEAN                     Width16;
  BOOLEAN                     Pass;
  BOOLEAN                     AllPass;
  MrcOutput                   *Outputs;
  MrcInput                    *Inputs;
  const MRC_FUNCTION          *MrcCall;
  MrcStatus                   Status;
  MrcChannelOut               *ChannelOut;
  MrcRankOut                  *RankOut;
  MrcChannelIn                *ChannelIn;
  MrcDebug                    *Debug;
  MrcDimmOut                  *DimmOut;
  DDR5_MODE_REGISTER_3_TYPE   MR3;
  DDR5_WRLV_INITIAL_SETTINGS_STRUCT InitialWlSettings;
#ifdef MRC_DEBUG_PRINT
  UINT8                       MrrResult[4];
#endif

  Outputs           = &MrcData->Outputs;
  Inputs            = &MrcData->Inputs;
  Debug             = &Outputs->Debug;
  MrcCall           = Inputs->Call.Func;
  ValidRankMask     = Outputs->ValidRankMask;
  McChBitMask       = Outputs->McChBitMask;
  MaxChannels       = Outputs->MaxChannels;
  SdramCount        = Outputs->SdramCount;
  AllPass           = FALSE;
  FineStep          = 2;


  SetupIOTestBasicVA (MrcData, McChBitMask, 1, NSOE, 0, 0, 1, PatWr, 0, 1);
  MrcSetLoopcount (MrcData, 0);

  // get max sweeping values
  MrcMcTxCycleLimits (MrcData, &MaxAdd, &MaxDec);

  // Since this is the first time we are enabling the TX Path, we should start from TxDqs Pi Code of 0.
  // This simplifies the logic of the search such that we don't have to worry about the original PI.
  GetSetVal = 0;
  MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, TxDqDelay,  WriteToCache, &GetSetVal);
  MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, TxDqsDelay, WriteToCache, &GetSetVal);
  MrcFlushRegisterCachedData (MrcData);

  // Save current settings that we are going to change during this algorithm
  BackupRestoreWlInitialSettings (MrcData, &InitialWlSettings, WRLV_BACKUP_INITIAL_SETTINGS);

  // Setup DDRIO to capture feedback on DQ
  SetupDdrioDdr5WrLvl (MrcData);

  // Relax WR to WR timings
  MrcRelaxWriteToWriteSameRank (MrcData);

  // External Write Leveling Coarse
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nExternal Write Leveling Coarse: Sweep the cycle offsets until pass\n");
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = (1 << Rank);
    if (!(RankMask & ValidRankMask)) {
      // Skip if all channels empty
      continue;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank %d\n", Rank);
    McChBitMask = SelectReutRanksAll (MrcData, RankMask, FALSE, 0);

    // Enable External Write leveling mode on this rank
    Status = MrcDdr5SetDramWrLvlMode (MrcData, Rank, Ddr5WrLvlModeExternal);
    if (Status != mrcSuccess) {
      return Status;
    }
    MrcWait (MrcData, MRC_DDR5_tWLPEN_NS * MRC_TIMER_1NS);

    MrcCall->MrcSetMem ((UINT8 *) SavedTxDqsVal, sizeof (SavedTxDqsVal), 0);

    // Determine the earliest MC can send data to the phy
    if ((Status = MrcIoTxLimits (MrcData, Rank, MaxAdd, MaxDec, &StartOffset, &EndOffset, SavedTxDqsVal)) != mrcSuccess) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n%s failed to get sweep limits\n", gErrString);
      return Status;
    }

    // Sweep through the cycle offsets until we find a value that passes
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nChannel\t0\t1\nByte\t");

    for (Byte = 0; Byte < SdramCount; Byte++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d  ", Byte);
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nCycle\n");
    // Clear Tracking Variable
    MrcCall->MrcSetMem ((UINT8 *) BytePass, sizeof (BytePass), 0);

    // Sweep Channel in clock cycles to find the WL Pulse and break once all Channels are done.
    for (CurrentOffset = StartOffset; CurrentOffset <= EndOffset; CurrentOffset++) {
      AllPass = TRUE;
      // Program new delay offsets to DQ/DQS timing:
      if (SetWriteCycleDelay (MrcData, Rank, MaxAdd, MaxDec, 0, SavedTxDqsVal, CurrentOffset) != mrcSuccess) {
        // continue if we cannot meet minimum TX separation
        continue;
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", CurrentOffset);

      // Clear tracking variable
      MrcCall->MrcSetMem ((UINT8 *) Result, sizeof (Result), 0);
      for (SampleCount = 0; SampleCount < DDR5_WL_MAX_SAMPLES; SampleCount++) {
        IoReset (MrcData);

        // Run Test - sequence of WR commands
        RunIOTest (MrcData, McChBitMask, StaticPattern, 1, 0);
        MrcWait (MrcData, 20 * MRC_TIMER_1NS);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              continue;
            }
            for (Byte = 0; Byte < SdramCount; Byte++) {
              Feedback = ReadAfeMiscControl0Reg (MrcData, Controller, Channel, Byte);
              if (Feedback > 0) {
                Result[Controller][Channel][Byte]++;
              }
            } // Byte
          } // Channel
        } // Controller
      } // SampleCount

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
            continue;
          }
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

          ChMask[Controller][Channel] = ChannelOut->ValidByteMask;

          for (Byte = 0; Byte < SdramCount; Byte++) {
            // Skip Bytes that are done
            if (BytePass[Controller][Channel] & (1 << Byte)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "    ");
              continue;
            }
            //MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &GetSetVal);
            //DataTrainFeedback = (UINT32) GetSetVal;
            Pass = (Result[Controller][Channel][Byte] >= (DDR5_WL_MAX_SAMPLES / 2));
            if (Pass) {
              PassingCycle[Controller][Channel][Byte] = CurrentOffset;
              BytePass[Controller][Channel] |= (1 << Byte);
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%c%2d ", Pass ? '.' : '#', Result[Controller][Channel][Byte]);
          } // Byte
          if (BytePass[Controller][Channel] != ChMask[Controller][Channel]) {
            AllPass = FALSE;
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
        } // Channel
      } // Controller
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

      if (AllPass) {
        break;
      }
    } // CurrentOffset loop

    if (!AllPass) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\nExternal Write Leveling Coarse: Not all bytes passed!\n");
      return mrcWriteLevelingError;
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMove back one clock\n");

    MaxDecDelay = Outputs->Gear4 ? MAX_DEC_DELAY_G4 : (Outputs->Gear2 ? MAX_DEC_DELAY_G2 : MAX_DEC_DELAY_G1);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < SdramCount; Byte++) {
          PassingCycle[Controller][Channel][Byte] -= 1;
          WrLvlTotPiDly[Controller][Channel][Rank][Byte] = TCW_ADJ_PI_STEP_SIZE * (PassingCycle[Controller][Channel][Byte] + MaxDecDelay);
        } // Byte
      } // Channel
    } // Controller

    // Disable Write leveling mode on this rank
    Status = MrcDdr5SetDramWrLvlMode (MrcData, Rank, Ddr5WrLvlModeDisable);
    if (Status != mrcSuccess) {
      return Status;
    }
  } // Rank

  MrcCall->MrcSetMem ((UINT8 *) WrLvlChMin,   sizeof (WrLvlChMin),   0xFF);
  MrcCall->MrcSetMem ((UINT8 *) WrLvlRankMin, sizeof (WrLvlRankMin), 0xFF);
  MrcFindMinWrLvlDelays (MrcData, WrLvlTotPiDly, WrLvlChMin, WrLvlRankMin);

  // Last 2 parameters (ByteDqOffsetStart and ByteDqOffsetEnd) are only required for Lpddr4
  if ((Status = MrcNormalizeTxDelay (MrcData, 0, MaxDec, WrLvlTotPiDly, WrLvlChMin, WrLvlRankMin, NULL, NULL)) != mrcSuccess) {
    return Status;
  }

  // External Write Leveling Fine

  // Walk forward til feedback is 1 with TxDqs PI.
  // Note we sweep up to 256 PI ticks (2 tCK), even though we should see the edge around 128 PI ticks
  // This is because some DRAMs require longer sweep
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nExternal Write Leveling Fine\n");
  if ((Status = MrcWrLvlSweepTxDqs (MrcData, Ddr5WrLvlModeExternal, 0, 256, FineStep, WrLvlTotPiDly)) != mrcSuccess) {
    return Status;
  }

  // Move all MC/CH/Rank/Bytes back by tWL_ADJ_Start (JEDEC Timing based on Write Preamble)
  // Write Preamble |        2tCK        |         3tCK         |       4tCK
  // WL_ADJ_Start   | -0.75tCK  (96 pi)  |  -1.25tCK (160 pi)   | -2.25tck (228 pi)
  // WL_ADJ_End     |  1.25tCK (160 pi)  |   1.75tCK (224 pi)   |  2.75tCK (352 pi)
  tWPRE = MrcGetWpre (MrcData);
  if (tWPRE == 2) {
    tWL_ADJ_Start = 96;
    tWL_ADJ_End = 160;

  } else if (tWPRE == 3) {
    tWL_ADJ_Start = 160;
    tWL_ADJ_End   = 224;
  } else { // tWPRE = 4
    tWL_ADJ_Start = 288;
    tWL_ADJ_End   = 352;
  }

  AdjustWrLvlTotPiDly (MrcData, -1 * tWL_ADJ_Start, WrLvlTotPiDly);

  MrcCall->MrcSetMem ((UINT8 *) WrLvlChMin,   sizeof (WrLvlChMin),   0xFF);
  MrcCall->MrcSetMem ((UINT8 *) WrLvlRankMin, sizeof (WrLvlRankMin), 0xFF);
  MrcFindMinWrLvlDelays (MrcData, WrLvlTotPiDly, WrLvlChMin, WrLvlRankMin);

  // Last 2 parameters (ByteDqOffsetStart and ByteDqOffsetEnd) are only required for Lpddr4
  if ((Status = MrcNormalizeTxDelay (MrcData, 0, MaxDec, WrLvlTotPiDly, WrLvlChMin, WrLvlRankMin, NULL, NULL)) != mrcSuccess) {
    return Status;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nInternal Write Leveling Coarse\n");

  // Internal Write Leveling Coarse

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = (1 << Rank);
    RankHalf = (UINT8) (Rank / 2);
    if (!(RankMask & ValidRankMask)) {
      // Skip if all empty channels
      continue;
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank %d\n", Rank);
    McChBitMask = SelectReutRanksAll (MrcData, RankMask, FALSE, 0);

    // Enable Internal Write Leveling Mode on this rank
    MrcDdr5SetDramWrLvlMode (MrcData, Rank, Ddr5WrLvlModeInternal);

    MrcCall->MrcSetMem ((UINT8 *) BytePass,     sizeof (BytePass),      0);
    MrcCall->MrcSetMem ((UINT8 *) PassIntWlAdj, sizeof (PassIntWlAdj),  0);

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nChannel\t\t0\t\t1\nByte\t\t");

    for (Byte = 0; Byte < SdramCount; Byte++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d  ", Byte);
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nInternalWlCycle\n");

    // Move the Internal WL Pulse until we see a 1 on the feedback
    for (InternalWlCycle = 0; InternalWlCycle < 8; InternalWlCycle++) {

      AllPass = TRUE;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t\t", InternalWlCycle);
      MrcSetMR3_DDR5 (MrcData, MAX_CONTROLLER, MaxChannels, Rank, InternalWlCycle, InternalWlCycle, MRC_PRINTS_OFF);
      MrcCall->MrcSetMem ((UINT8 *) Result, sizeof (Result), 0);

      for (SampleCount = 0; SampleCount < DDR5_WL_MAX_SAMPLES; SampleCount++) {
        RunIOTest (MrcData, McChBitMask, StaticPattern, 1, 0);
        MrcWait (MrcData, 20 * MRC_TIMER_1NS);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              continue;
            }
            for (Byte = 0; Byte < SdramCount; Byte++) {
              Feedback = ReadAfeMiscControl0Reg (MrcData, Controller, Channel, Byte);
              if (Feedback > 0) {
                Result[Controller][Channel][Byte]++;
              }
            } // Byte
          } // Channel
        } // Controller
      } // SampleCount

      // Save per Byte value for when the feedback is 1
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (!MrcControllerExist (MrcData, Controller)) {
          continue; // avoid printing end of line when controller is not present
        }
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

          ChMask[Controller][Channel] = ChannelOut->ValidByteMask;

          for (Byte = 0; Byte < SdramCount; Byte++) {
            // Skip Bytes that are done
            if (BytePass[Controller][Channel] & (1 << Byte)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "    ");
              continue;
            }

            //MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &GetSetVal);
            //DataTrainFeedback = (UINT32) GetSetVal;
            Pass = (Result[Controller][Channel][Byte] >= (DDR5_WL_MAX_SAMPLES / 2));
            if (Pass) {
              PassIntWlAdj[Controller][Channel][Byte] = InternalWlCycle;
              BytePass[Controller][Channel] |= (1 << Byte);
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%c%2d ", Pass ? '.' : '#', Result[Controller][Channel][Byte]);
          } // Byte loop
          if (BytePass[Controller][Channel] != ChMask[Controller][Channel]) {
            AllPass = FALSE;
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t");
        } // Channel
      } // Controller
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

      if (AllPass == TRUE) {
        break;
      }
    } // InternalWlCycle loop

    if (AllPass == FALSE) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\nInternal Write Leveling Coarse: Not all bytes passed!\n");
      return mrcWriteLevelingError;
    }

    // Program Write Leveling Internal Cycle Alignment with PDA.
    // Be sure to group x16
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        ChannelIn  = &Inputs->Controller[Controller].Channel[Channel];
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        DimmOut    = &ChannelOut->Dimm[RankHalf];
        RankOut    = &DimmOut->Rank[Rank % 2];
        Width16    = (DimmOut->SdramWidth == 16);
        UpperByteVal = 0;
        LowerByteVal = 0;

        // Channel Width (ddr5 = 32 Bytes) / SdramWidth (x8 or x16)
        NumDevices = DimmOut->PrimaryBusWidth / DimmOut->SdramWidth;
        if ((DimmOut->SdramWidth == 8) && DimmOut->EccSupport) {
          NumDevices += 1;
        }
        if (Inputs->EnablePda) {
          ChannelOut->Mr3PdaEnabled = TRUE;
          for (Device = 0; Device < NumDevices; Device++) {
            for (Byte = 0; Byte < SdramCount; Byte++) {
              PassingIntCycle = PassIntWlAdj[Controller][Channel][Byte];
              DqsMapCpu2Dram = ChannelIn->DqsMapCpu2Dram[RankHalf][Byte];
              if (Width16) {
                if (DqsMapCpu2Dram == (Device * 2)) {
                  LowerByteVal = PassingIntCycle;
                }
                if (DqsMapCpu2Dram == (Device * 2 + 1)) {
                  UpperByteVal = PassingIntCycle;
                }
              } else { // x8
                if (DqsMapCpu2Dram == Device) {
                  LowerByteVal = PassingIntCycle;
                  break;
                }
              }
            } // Byte

            MrcPdaSelect (MrcData, Controller, Channel, Rank, Device, MRC_PRINTS_OFF);
            MrcSetMR3_DDR5 (MrcData, Controller, Channel, Rank, LowerByteVal, UpperByteVal, MRC_PRINTS_ON);
            MR3.Bits.WriteLevelingInternalCycleUpperByte = UpperByteVal;
            MR3.Bits.WriteLevelingInternalCycleLowerByte = LowerByteVal;
            RankOut->Ddr5PdaMr3[Device] = MR3.Data8;
          } // Device
          // Send PDA command of Index 15 to resume normal rank operation mode for MR/CAVREF/MRC
          MrcPdaSelect (MrcData, Controller, Channel, Rank, 15, MRC_PRINTS_OFF);
        } else {
          // No PDA, use byte0 value for all devices
          LowerByteVal = PassIntWlAdj[Controller][Channel][0];
          MrcSetMR3_DDR5 (MrcData, Controller, Channel, Rank, LowerByteVal, LowerByteVal, MRC_PRINTS_ON);
        }
      } // Channel
    } // Controller

    // Disable Write Leveling Mode on this rank
    MrcDdr5SetDramWrLvlMode (MrcData, Rank, Ddr5WrLvlModeDisable);
  } // Rank loop

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMove back one clock\n");
  AdjustWrLvlTotPiDly (MrcData, -1 * 128, WrLvlTotPiDly); // -= 1 tCK

  MrcCall->MrcSetMem ((UINT8 *) WrLvlChMin,   sizeof (WrLvlChMin),   0xFF);
  MrcCall->MrcSetMem ((UINT8 *) WrLvlRankMin, sizeof (WrLvlRankMin), 0xFF);
  MrcFindMinWrLvlDelays (MrcData, WrLvlTotPiDly, WrLvlChMin, WrLvlRankMin);

  // Last 2 parameters (ByteDqOffsetStart and ByteDqOffsetEnd) are only required for Lpddr4
  if ((Status = MrcNormalizeTxDelay (MrcData, 0, MaxDec, WrLvlTotPiDly, WrLvlChMin, WrLvlRankMin, NULL, NULL)) != mrcSuccess) {
    return Status;
  }


  // Walk forward with PI until we find the edge (Feedback == 1)
  // Note we sweep up to 256 PI ticks (2 tCK), even though we should see the edge around 128 PI ticks
  // This is because some DRAMs require longer sweep
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Internal Write Leveling Fine Step\n");
  if ((Status = MrcWrLvlSweepTxDqs (MrcData, Ddr5WrLvlModeInternal, 0, 256, FineStep, WrLvlTotPiDly)) != mrcSuccess) {
    return Status;
  }

  // Apply the final Adjustment per JEDEC Spec: WL_ADJ_end
  AdjustWrLvlTotPiDly (MrcData, tWL_ADJ_End, WrLvlTotPiDly);

  // Cleanup
  GetSetVal = 0;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocWriteLevelMode, WriteToCache, &GetSetVal);

  // Restore Initial settings
  BackupRestoreWlInitialSettings (MrcData, &InitialWlSettings, WRLV_RESTORE_INITIAL_SETTINGS);

  // Normalize Final MC/Fifo/PI Value
  MrcCall->MrcSetMem ((UINT8 *) WrLvlChMin,   sizeof (WrLvlChMin),   0xFF);
  MrcCall->MrcSetMem ((UINT8 *) WrLvlRankMin, sizeof (WrLvlRankMin), 0xFF);
  MrcFindMinWrLvlDelays (MrcData, WrLvlTotPiDly, WrLvlChMin, WrLvlRankMin);

  // Last 2 parameters (ByteDqOffsetStart and ByteDqOffsetEnd) are only required for Lpddr4
  if ((Status = MrcNormalizeTxDelay (MrcData, 128, MaxDec, WrLvlTotPiDly, WrLvlChMin, WrLvlRankMin, NULL, NULL)) != mrcSuccess) {
    return Status;
  }

  IoReset (MrcData);

#ifdef MRC_DEBUG_PRINT
  // Read out the programmed MR3 value per device
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        MrcIssueMrr (MrcData, Controller, Channel, Rank, 0x03, MrrResult);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u R%u MR3: [0x%02X, 0x%02X, 0x%02X, 0x%02X]\n", Controller, Channel, Rank, MrrResult[0], MrrResult[1], MrrResult[2], MrrResult[3]);
      }
    }
  }
#endif // MRC_DEBUG_PRINT

  if(Outputs->Gear4) {
    Status = MrcNormalizeTxGear4(MrcData);
  }
  return Status;
}

/**
  Execute the JEDEC Write Leveling training.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus -  if it succedes return mrcSuccess
**/
MrcStatus
MrcJedecWriteLevelingTraining (
  IN OUT MrcParameters *const MrcData
  )
{
  BOOLEAN   Ddr5;
  MrcOutput *Outputs;
  MrcStatus Status;

  Outputs = &MrcData->Outputs;
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  if (Ddr5) {
    Status = MrcJedecWriteLevelingDdr5 (MrcData);
  } else {
    Status = MrcJedecWriteLevelingDdr4Lp4 (MrcData);
  }

  return Status;
}
