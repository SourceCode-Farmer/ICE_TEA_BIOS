#ifndef __MrcMcRegisterStructAdl6xxx_h__
#define __MrcMcRegisterStructAdl6xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2020 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)
typedef union {
  struct {
    UINT32                                         :  6;  // Bits 5:0
    UINT32 HASH_MASK                               :  14;  // Bits 19:6
    UINT32                                         :  4;  // Bits 23:20
    UINT32 HASH_LSB_MASK_BIT                       :  3;  // Bits 26:24
    UINT32                                         :  1;  // Bits 27:27
    UINT32 HASH_MODE                               :  1;  // Bits 28:28
    UINT32                                         :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCF_IDP0_CR_CHANNEL_HASH_STRUCT;
typedef union {
  struct {
    UINT32                                         :  6;  // Bits 5:0
    UINT32 EHASH_MASK                              :  14;  // Bits 19:6
    UINT32                                         :  4;  // Bits 23:20
    UINT32 EHASH_LSB_MASK_BIT                      :  3;  // Bits 26:24
    UINT32                                         :  1;  // Bits 27:27
    UINT32 EHASH_MODE                              :  1;  // Bits 28:28
    UINT32 MLMC_IS_BA0                             :  1;  // Bits 29:29
    UINT32                                         :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCF_IDP0_CR_CHANNEL_EHASH_STRUCT;
typedef union {
  struct {
    UINT32 DDR_TYPE                                :  3;  // Bits 2:0
    UINT32 ECHM                                    :  1;  // Bits 3:3
    UINT32 CH_L_MAP                                :  1;  // Bits 4:4
    UINT32                                         :  3;  // Bits 7:5
    UINT32 STKD_MODE                               :  1;  // Bits 8:8
    UINT32 STKD_MODE_CH1                           :  1;  // Bits 9:9
    UINT32                                         :  2;  // Bits 11:10
    UINT32 CH_S_SIZE                               :  8;  // Bits 19:12
    UINT32                                         :  4;  // Bits 23:20
    UINT32 STKD_MODE_CH_BITS                       :  3;  // Bits 26:24
    UINT32                                         :  5;  // Bits 31:27
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCF_IDP0_CR_MAD_INTER_CHANNEL_STRUCT;
typedef union {
  struct {
    UINT32 DIMM_L_MAP                              :  1;  // Bits 0:0
    UINT32                                         :  3;  // Bits 3:1
    UINT32 RI                                      :  1;  // Bits 4:4
    UINT32                                         :  3;  // Bits 7:5
    UINT32 EIM                                     :  1;  // Bits 8:8
    UINT32                                         :  3;  // Bits 11:9
    UINT32 ECC                                     :  2;  // Bits 13:12
    UINT32                                         :  18;  // Bits 31:14
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCF_IDP0_CR_MAD_INTRA_CH0_STRUCT;

typedef CCF_IDP0_CR_MAD_INTRA_CH0_STRUCT CCF_IDP0_CR_MAD_INTRA_CH1_STRUCT;
typedef union {
  struct {
    UINT32 DIMM_L_SIZE                             :  7;  // Bits 6:0
    UINT32 DLW                                     :  2;  // Bits 8:7
    UINT32 DLNOR                                   :  2;  // Bits 10:9
    UINT32 ddr5_device_8Gb                         :  1;  // Bits 11:11
    UINT32                                         :  4;  // Bits 15:12
    UINT32 DIMM_S_SIZE                             :  7;  // Bits 22:16
    UINT32                                         :  1;  // Bits 23:23
    UINT32 DSW                                     :  2;  // Bits 25:24
    UINT32 DSNOR                                   :  2;  // Bits 27:26
    UINT32 BG0_bit_options                         :  2;  // Bits 29:28
    UINT32 Decoder_EBH                             :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCF_IDP0_CR_MAD_DIMM_CH0_STRUCT;

typedef CCF_IDP0_CR_MAD_DIMM_CH0_STRUCT CCF_IDP0_CR_MAD_DIMM_CH1_STRUCT;
typedef union {
  struct {
    UINT32 MARS_Enable                             :  1;  // Bits 0:0
    UINT32                                         :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCF_IDP0_CR_MARS_ENABLE_STRUCT;

typedef CCF_IDP0_CR_CHANNEL_HASH_STRUCT CCF_IDP0_CR_CHANNEL_HASH_MC1_STRUCT;

typedef CCF_IDP0_CR_CHANNEL_EHASH_STRUCT CCF_IDP0_CR_CHANNEL_EHASH_MC1_STRUCT;

typedef CCF_IDP0_CR_MAD_INTER_CHANNEL_STRUCT CCF_IDP0_CR_MAD_INTER_CHANNEL_MC1_STRUCT;

typedef CCF_IDP0_CR_MAD_INTRA_CH0_STRUCT CCF_IDP0_CR_MAD_INTRA_CH0_MC1_STRUCT;

typedef CCF_IDP0_CR_MAD_INTRA_CH0_STRUCT CCF_IDP0_CR_MAD_INTRA_CH1_MC1_STRUCT;

typedef CCF_IDP0_CR_MAD_DIMM_CH0_STRUCT CCF_IDP0_CR_MAD_DIMM_CH0_MC1_STRUCT;

typedef CCF_IDP0_CR_MAD_DIMM_CH0_STRUCT CCF_IDP0_CR_MAD_DIMM_CH1_MC1_STRUCT;

typedef CCF_IDP0_CR_CHANNEL_HASH_STRUCT CCF_IDP0_CR_SLICE_HASH_STRUCT;
typedef union {
  struct {
    UINT32                                         :  4;  // Bits 3:0
    UINT32 MS_L_MAP                                :  1;  // Bits 4:4
    UINT32                                         :  3;  // Bits 7:5
    UINT32 STKD_MODE                               :  1;  // Bits 8:8
    UINT32 STKD_MODE_MS1                           :  1;  // Bits 9:9
    UINT32                                         :  2;  // Bits 11:10
    UINT32 MS_S_SIZE                               :  11;  // Bits 22:12
    UINT32                                         :  1;  // Bits 23:23
    UINT32 STKD_MODE_MS_BITS                       :  4;  // Bits 27:24
    UINT32                                         :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCF_IDP0_CR_MAD_INTER_SLICE_STRUCT;

typedef CCF_IDP0_CR_CHANNEL_HASH_STRUCT CCF_IDP1_CR_CHANNEL_HASH_STRUCT;

typedef CCF_IDP0_CR_CHANNEL_EHASH_STRUCT CCF_IDP1_CR_CHANNEL_EHASH_STRUCT;

typedef CCF_IDP0_CR_MAD_INTER_CHANNEL_STRUCT CCF_IDP1_CR_MAD_INTER_CHANNEL_STRUCT;

typedef CCF_IDP0_CR_MAD_INTRA_CH0_STRUCT CCF_IDP1_CR_MAD_INTRA_CH0_STRUCT;

typedef CCF_IDP0_CR_MAD_INTRA_CH0_STRUCT CCF_IDP1_CR_MAD_INTRA_CH1_STRUCT;

typedef CCF_IDP0_CR_MAD_DIMM_CH0_STRUCT CCF_IDP1_CR_MAD_DIMM_CH0_STRUCT;

typedef CCF_IDP0_CR_MAD_DIMM_CH0_STRUCT CCF_IDP1_CR_MAD_DIMM_CH1_STRUCT;

typedef CCF_IDP0_CR_MARS_ENABLE_STRUCT CCF_IDP1_CR_MARS_ENABLE_STRUCT;

typedef CCF_IDP0_CR_CHANNEL_HASH_STRUCT CCF_IDP1_CR_CHANNEL_HASH_MC1_STRUCT;

typedef CCF_IDP0_CR_CHANNEL_EHASH_STRUCT CCF_IDP1_CR_CHANNEL_EHASH_MC1_STRUCT;

typedef CCF_IDP0_CR_MAD_INTER_CHANNEL_STRUCT CCF_IDP1_CR_MAD_INTER_CHANNEL_MC1_STRUCT;

typedef CCF_IDP0_CR_MAD_INTRA_CH0_STRUCT CCF_IDP1_CR_MAD_INTRA_CH0_MC1_STRUCT;

typedef CCF_IDP0_CR_MAD_INTRA_CH0_STRUCT CCF_IDP1_CR_MAD_INTRA_CH1_MC1_STRUCT;

typedef CCF_IDP0_CR_MAD_DIMM_CH0_STRUCT CCF_IDP1_CR_MAD_DIMM_CH0_MC1_STRUCT;

typedef CCF_IDP0_CR_MAD_DIMM_CH0_STRUCT CCF_IDP1_CR_MAD_DIMM_CH1_MC1_STRUCT;

typedef CCF_IDP0_CR_CHANNEL_HASH_STRUCT CCF_IDP1_CR_SLICE_HASH_STRUCT;

typedef CCF_IDP0_CR_MAD_INTER_SLICE_STRUCT CCF_IDP1_CR_MAD_INTER_SLICE_STRUCT;

#pragma pack(pop)
#endif
