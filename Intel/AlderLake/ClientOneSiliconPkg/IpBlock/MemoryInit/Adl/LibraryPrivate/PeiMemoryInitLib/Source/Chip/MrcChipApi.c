/** @file
  .

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "MrcTypes.h"
#include "MrcInterface.h"
#include "MrcHalRegisterAccess.h"
#include "MrcCommon.h"
#include "MrcChipApi.h"
#include "MrcMemoryApi.h"
#include "MrcDdrIoApiInt.h"
#include "MrcDdr5.h"
#include "MrcDdr5Registers.h"
#include "MrcMaintenance.h"

#define GEN_MRS_FSM_FLAT_TIME_MAX          (127) // BITS[6:0]
#define GEN_MRS_FSM_TIME_SCALAR_x16        (16)
#define GEN_MRS_FSM_TIME_SCALAR_x32        (32)
#define GEN_MRS_FSM_TIME_SCALAR_x64        (64)
#define GEN_MRS_FSM_TIME_SCALAR_BIT_x16    (0x080)
#define GEN_MRS_FSM_TIME_SCALAR_BIT_x32    (0x100)
#define GEN_MRS_FSM_TIME_SCALAR_BIT_x64    (0x180)


/**
  This function configures the Generic MRS FSM (Gmf) timing registers on the
  input Controller McCh based on the pre-defined GmfTimingIndex timing assignments.
  These timings are different per technology and are statically assigned a timing
  register field index.

  @param[in]     MrcData            - Pointer to MRC global data.
  @param[in]     Controller         - Handle to the controller
  @param[in]     McCh               - Handle to channel within the memory controller

  @retval mrcSuccess if successful.
  @retval mrcFail if the timing registers are full and the requested Delay could not be allocated.
**/
MrcStatus
ConfigGenMrsFsmTiming  (
  IN     MrcParameters *MrcData,
  IN     UINT32        Controller,
  IN     UINT32        McCh
  )
{
  MrcStatus Status;
  MrcOutput *Outputs;
  UINT32    Offset;
  UINT16    MrDelay;
  UINT32    TimingSetIdx;
  UINT32    ScaledTimeMax;
  UINT16    TimingEncode;
  BOOLEAN   Lpddr5;
  MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_STRUCT GenMrsFsmTiming;

  Outputs = &MrcData->Outputs;
  Lpddr5 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);

  // Configure all of the Generic MRS FSM Timing registers on the current Controller, McCh
  for (TimingSetIdx = GmfTimingIndex0; TimingSetIdx < GmfTimingIndexMax; TimingSetIdx++) {
    Status = MrcGetGmfDelayTiming (MrcData, TimingSetIdx, &MrDelay);
    if (Status != mrcSuccess) {
      return Status;
    }

    if (Lpddr5) {
      // Need to scale from tCK to WCK
      MrDelay *= 4;
    }
    ScaledTimeMax = (GEN_MRS_FSM_FLAT_TIME_MAX * GEN_MRS_FSM_TIME_SCALAR_x64);
    if (MrDelay > ScaledTimeMax) {
      // Check if the timing value is larger than what the register supports.
      return mrcWrongInputParameter;
    } else if (MrDelay > (GEN_MRS_FSM_FLAT_TIME_MAX * GEN_MRS_FSM_TIME_SCALAR_x32)) {
      // Round to the nearest multiple of the scalar based value and set the bit to enable the scalar
      TimingEncode = ((UINT16) DIVIDECEIL (MrDelay, GEN_MRS_FSM_TIME_SCALAR_x64)) | GEN_MRS_FSM_TIME_SCALAR_BIT_x64;
    } else if (MrDelay > (GEN_MRS_FSM_FLAT_TIME_MAX * GEN_MRS_FSM_TIME_SCALAR_x16)) {
      // Round to the nearest multiple of the scalar based value and set the bit to enable the scalar
      TimingEncode = ((UINT16) DIVIDECEIL (MrDelay, GEN_MRS_FSM_TIME_SCALAR_x32)) | GEN_MRS_FSM_TIME_SCALAR_BIT_x32;
    } else if (MrDelay > GEN_MRS_FSM_FLAT_TIME_MAX) {
      // Round to the nearest multiple of the scalar based value and set the bit to enable the scalar
      TimingEncode = ((UINT16) DIVIDECEIL (MrDelay, GEN_MRS_FSM_TIME_SCALAR_x16)) | GEN_MRS_FSM_TIME_SCALAR_BIT_x16;
    } else {
      TimingEncode = MrDelay;
    }

    // Calculate the register offset
    // 6 entries, 3 in each register
    if (TimingSetIdx < (GmfTimingIndexMax / 2)) {
      Offset = OFFSET_CALC_MC_CH (
        MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG,
        MC1_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG,
        Controller,
        MC0_CH1_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG,
        McCh
        );
    } else {
      Offset = OFFSET_CALC_MC_CH (
        MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_1_REG,
        MC1_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_1_REG,
        Controller,
        MC0_CH1_CR_GENERIC_MRS_FSM_TIMING_STORAGE_1_REG,
        McCh
        );
    }
    GenMrsFsmTiming.Data = MrcReadCR (MrcData, Offset);
    switch (TimingSetIdx % (GmfTimingIndexMax / 2)) {
      case 0:
        GenMrsFsmTiming.Bits.TIMING_FIELD_0 = TimingEncode;
        break;

      case 1:
        GenMrsFsmTiming.Bits.TIMING_FIELD_1 = TimingEncode;
        break;

      case 2:
        GenMrsFsmTiming.Bits.TIMING_FIELD_2 = TimingEncode;
        break;

      default:
        return mrcFail;
    }
    MrcWriteCR (MrcData, Offset, GenMrsFsmTiming.Data);
  } // for TimingSetIdx
  return mrcSuccess;
}

/**
  This function configures the Generic MRS FSM shadow registers as specified by the
  input parameters. This function will handle programming of the control
  registers and will program the appropriate MR value register type:
  GENERIC_MRS_FSM_STORAGE_VALUES

  @param[in]     MrcData       - Pointer to MRC global data.
  @param[in]     Controller    - Handle to the controller
  @param[in]     McCh          - Handle to channel within the memory controller
  @param[in]     GenMrRef      - Pointer to an entry from the MrData array belonging to the ControlRegIdx values to be set.
  @param[in]     MrPerRank     - The DRAM Mode Register requires unique values for each rank.
  @param[in]     ControlRegIdx - The MrData array entry to use when configuring the shadow registers.
  @param[in]     MrData        - Pointer to an array of MR data to configure the MRS FSM with.
  @param[in,out] StorageIdx    - Points to the next available GENERIC_MRS_FSM_STORAGE_VALUES byte.

  @retval mrcSuccess if successful.
  @retval mrcFail if the shadow registers are out of free entries.
**/
MrcStatus
ConfigGenMrsFsmValue  (
  IN     MrcParameters           *MrcData,
  IN     UINT32                  Controller,
  IN     UINT32                  McCh,
  IN     MRC_GEN_MRS_FSM_MR_TYPE *GenMrRef,
  IN     BOOLEAN                 MrPerRank,
  IN     UINT8                   ControlRegIdx,
  IN     MRC_GEN_MRS_FSM_MR_TYPE MrData[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_MR_GEN_FSM],
  IN OUT UINT8                   *StorageIdx
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcDebug  *Debug;
  MrcOutput *Outputs;
  UINT32    ControlRegNum;
  UINT32    Channel;
  UINT32    MaxSubCh;
  UINT32    SubCh;
  UINT32    Offset;
  UINT32    Rank;
//  UINT32    PerRankSwitch;
  UINT8     ShadowFieldIdx;
//  BOOLEAN   Done;
  BOOLEAN   Lpddr;
  MRC_GEN_MRS_FSM_MR_TYPE                               *GenMrPtr;
  MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_STRUCT           GenMrsFsmCtl;
  MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_STRUCT    GenMrsFsmChVal;
  UINT8                                                 MrPdaData[MAX_PDA_MR_IN_CHANNEL];
  UINT8                                                 NumMrData;
  UINT8                                                 mrDataIndex;
  MrcStatus Status;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Lpddr   = Outputs->Lpddr;
  MaxSubCh = (Lpddr) ? MAX_SUB_CHANNEL : 1;
  Status  = mrcSuccess;
  MrcCall = MrcData->Inputs.Call.Func;

  MrcCall->MrcSetMem ((UINT8 *) MrPdaData, sizeof (MrPdaData), 0);
  NumMrData = 0;
  // StorageIdx points to the next available GENERIC_MRS_FSM_STORAGE_VALUES byte.
  // ADL treats the byte fields in GENERIC_MRS_FSM_STORAGE_VALUES registers as an array of bytes.
  // The GENERIC_MRS_FSM_CONTROL.GENERIC_MRS_STORAGE_POINTER must be programmed with a byte index into this array.
  // Example: GENERIC_MRS_FSM_STORAGE_VALUES_27.BYTE_1 corresponds to byte index (27 * 4) + 1 = 109.



  // Configure control register for this MR.
  GenMrsFsmCtl.Data = 0;
  if (GenMrRef->PdaMr) {
    GenMrsFsmCtl.Bits.PER_RANK = 1;
    GenMrsFsmCtl.Bits.PER_DEVICE = 1;
  } else if (MrPerRank) {
    GenMrsFsmCtl.Bits.PER_RANK = 1;
  }
  GenMrsFsmCtl.Bits.COMMAND_TYPE = GenMrRef->CmdType;
  GenMrsFsmCtl.Bits.ADDRESS = GenMrRef->MrAddr;
  GenMrsFsmCtl.Bits.ACTIVE = GenMrRef->Valid;
  GenMrsFsmCtl.Bits.TIMING_VALUE_POINTER = GenMrRef->DelayIndex;
  GenMrsFsmCtl.Bits.GENERIC_MRS_STORAGE_POINTER = *StorageIdx;
  GenMrsFsmCtl.Bits.FSP_CONTROL = (GenMrRef->FspWrToggle ? 1 : (GenMrRef->FspOpToggle ? 2 : 0));

  Offset = MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG +
         ((MC1_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG - MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG) * Controller) +
         ((MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_0_REG - MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG) * McCh) +
         ((MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_1_REG - MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG) * ControlRegIdx);
  MrcWriteCR (MrcData, Offset, GenMrsFsmCtl.Data);

  if (!GenMrRef->PdaMr) {
    // Configure MR Value registers. case of PerRank or Per Ch MR
    for (SubCh = 0; SubCh < MaxSubCh; SubCh++) {
      Channel = (McCh * MaxSubCh) + SubCh;
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }

        GenMrPtr = &MrData[Controller][Channel][Rank][ControlRegIdx];
        if (!GenMrPtr->Valid) {
          continue;
        }

        // Per Rank configurations may span multiple registers, including registers
        // where previous byte indexes are occupied by non per-rank values. To prevent
        // overwriting existing entries, do a read-modify-write for each new value.
        // Crossing register boundaries will be automatically handled by incrementing
        // StorageIdx for each new value written.
        MRC_DEBUG_ASSERT ((*StorageIdx < GEN_MRS_FSM_BYTE_MAX), Debug, "Number of byte storage instances has exceeded the max supported by the MC\n");
        ControlRegNum = (*StorageIdx) / 4;
        Offset = MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG +
               ((MC1_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG - MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG) * Controller) +
               ((MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG - MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG) * McCh) +
               ((MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_1_REG - MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG) * ControlRegNum);
        GenMrsFsmChVal.Data = MrcReadCR (MrcData, Offset);

        // The Registers in a dual rank configuration must be stored in the following order
        // assuming n is the byte address specified in GENERIC_MRS_STORAGE_POINTER.
        // n   = Sub-channel 0, Rank 0
        // n+1 = Sub-channel 0, Rank 1
        // n+2 = Sub-channel 1, Rank 0
        // n+3 = Sub-channel 1, Rank 1
        ShadowFieldIdx = (*StorageIdx) % 4;
        switch (ShadowFieldIdx) {
          case 0:
            GenMrsFsmChVal.Bits.BYTE_0 = GenMrPtr->MrData;
            break;

          case 1:
            GenMrsFsmChVal.Bits.BYTE_1 = GenMrPtr->MrData;
            break;

          case 2:
            GenMrsFsmChVal.Bits.BYTE_2 = GenMrPtr->MrData;
            break;

          case 3:
            GenMrsFsmChVal.Bits.BYTE_3 = GenMrPtr->MrData;
            break;
        }
        MrcWriteCR (MrcData, Offset, GenMrsFsmChVal.Data);
        (*StorageIdx)++;
        // Only a single byte field is needed for non per-rank MrData array entries.
        if (!MrPerRank) {
          break;
        }
      } // Rank
      // Only a single byte field is needed for non per-rank MrData array entries.
      if (!MrPerRank) {
        break;
      }
    } // SubCh
  } else if (GenMrRef->Valid) {
    // Configure MR Value registers. case of PDA or Per Ch MR
    // First Get the data for active Ranks and Dev
    Status = MrFillPdaMrsData (MrcData, Controller, McCh, GenMrRef->MrAddr, MrPdaData,&NumMrData);
    // check bouneries 
    MRC_DEBUG_ASSERT ((Status == mrcSuccess), Debug, "Failed to  MrFillPdaMrsData Controller %d, Channel= %d, MrAddress = %d \n", Controller, McCh, GenMrRef->MrAddr);             
    MRC_DEBUG_ASSERT (((*StorageIdx + NumMrData)< GEN_MRS_FSM_BYTE_MAX), Debug, "Number of byte storage instances has exceeded the max supported by the MC\n");
    
    // configure the MrPdaData  vals in the apprropriete GENERIC_MRS_FSM_STORAGE reg according to *StorageIdx
    for (mrDataIndex=0; mrDataIndex < NumMrData ;) {
      ShadowFieldIdx = (*StorageIdx) % 4;
      ControlRegNum = (*StorageIdx) / 4;
      Offset = MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG +
              ((MC1_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG - MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG) * Controller) +
              ((MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG - MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG) * McCh) +
              ((MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_1_REG - MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG) * ControlRegNum);

      if (ShadowFieldIdx) {
        // Register is partially configured Need Read Modify write
        GenMrsFsmChVal.Data = MrcReadCR (MrcData, Offset);
        while (ShadowFieldIdx && (mrDataIndex < NumMrData)) {
          GenMrsFsmChVal.Data8[ShadowFieldIdx] = MrPdaData[mrDataIndex];
          (*StorageIdx)++;
          mrDataIndex++;
          ShadowFieldIdx = (*StorageIdx) % 4;
        }
      } else {
        // In beginning of the Register, no need to read
        GenMrsFsmChVal.Data = 0;
        while ((ShadowFieldIdx < 4) && (mrDataIndex < NumMrData)) { // fill up to 4 bytes or we finished the data
          GenMrsFsmChVal.Data8[ShadowFieldIdx] = MrPdaData[mrDataIndex];
          (*StorageIdx)++;
          mrDataIndex++;
          ShadowFieldIdx++;
        }
      }
      // MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "GENERIC_MRS_FSM_STORAGE_VALUES: MR%d MC%u C%u StorageIdx: %u ControlRegNum: %u, Offset: 0x%x, data=0x%x\n", GenMrRef->MrAddr, Controller, McCh, *StorageIdx, ControlRegNum, Offset, GenMrsFsmChVal.Data);
      MrcWriteCR (MrcData, Offset, GenMrsFsmChVal.Data);
    }
  } // else if
  return mrcSuccess;
}

/**
  This function checks if the input MrAddr value is present in the MrPerRankLookup array.

  @param[in] MrAddr - MR Address to lookup in MrPerRankLookup
  @param[in] MrPerRankLookup - mrEndOfSequence terminated array specifying which MR addresses
                               require per-rank configuration.

  @retval TRUE if MrAddr value exists in MrPerRankLookup array or MrPerRankLookup is NULL
  @retval FALSE in all other cases
**/
BOOLEAN
IsMrPerRank (
  IN MrcModeRegister       MrAddr,
  IN const MrcModeRegister *MrPerRankLookup
  )
{
  UINT32 Index;
  BOOLEAN MrPerRank;

  if (MrPerRankLookup != NULL) {
    MrPerRank = FALSE;
    for (Index = 0; MrPerRankLookup[Index] != mrEndOfSequence; Index++) {
      if (MrPerRankLookup[Index] == MrAddr) {
        MrPerRank = TRUE;
        break;
      }
    }
    if ((MrAddr >= mrMR129) && (MrAddr <= mrMR252)) { // DDR5 DFE is per rank
      MrPerRank = TRUE;
    }
  } else {
    MrPerRank = TRUE;
  }

  return MrPerRank;
}

/**
  This function configures the Generic MRS FSM shadow registers based on the MrData inputs.
  It will determine if it needs to use the per-rank feature if the MR value differs across ranks.

  @param[in] MrcData - Pointer to MRC global data.
  @param[in] MrData  - Pointer to an array of MR data to configure the MRS FSM with.
  @param[out] MrPerRankLookup - Optional mrEndOfSequence terminated array specifying MR addresses that must
                                be sent per-rank, all other registers use the same data for all ranks on the
                                same channel. If this pointer is NULL, then only MRs with different values in
                                each rank are configued as per-rank Generic MRS FSM entries.

  @retval mrcSuccess if successful.
  @retval mrcFail if MrData pointer is null, the timing or per-rank registers are out of free entries.
**/
MrcStatus
MrcGenMrsFsmConfig (
  IN  MrcParameters *MrcData,
  IN  MRC_GEN_MRS_FSM_MR_TYPE MrData[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_MR_GEN_FSM],
  IN  const MrcModeRegister   *MrPerRankLookup OPTIONAL
  )
{
  const MRC_FUNCTION  *MrcCall;
  const MrcInput      *Inputs;
  MrcDebug  *Debug;
  MrcOutput *Outputs;
  MrcStatus Status;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    McCh;
  UINT32    MaxSubCh;
  UINT32    Offset;
  UINT32    Rank;
  UINT32    SubCh;
  UINT16    InitData;
  UINT8     ControlRegIdx;
  UINT8     ValidControlRegIndex;
  UINT8     StorageIdx;
  UINT8     SaveStorageIdx;
  UINT8     FspOpSwitchIdx;
  BOOLEAN   SagvBreakPoint;
  BOOLEAN   Lpddr;
  BOOLEAN   Ddr5;
  BOOLEAN   MrPerRank;
  BOOLEAN   DoZqCal;
  BOOLEAN   GlobalDevLoopEnable;
  BOOLEAN   EnablePdaSelectAll;
  MRC_GEN_MRS_FSM_MR_TYPE *GenMrPtr;
  MC0_CH0_CR_MRS_FSM_CONTROL_STRUCT MrsFsmCtl;
  BOOLEAN   RestoreStorageIdx;
  BOOLEAN   IsDimmDfeMrBit0;
  UINT8     DfeMrTap;
  UINT8     DfeMrTapStorageIdx[MAX_DFE_TAP_DDR5];

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Lpddr   = Outputs->Lpddr;
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  MaxSubCh = (Lpddr) ? MAX_SUB_CHANNEL : 1;
  Rank    = 0;
  Channel = 0;
  GenMrPtr = NULL;
  SagvBreakPoint = FALSE;
  FspOpSwitchIdx = 0;
  DoZqCal = TRUE;
  GlobalDevLoopEnable = FALSE;
  EnablePdaSelectAll = FALSE;
  ValidControlRegIndex = 0;
  RestoreStorageIdx   = FALSE;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (!MrcControllerExist (MrcData, Controller)) {
      // Skip unpopulated controllers.
      continue;
    }
    // Determine if the the MR value is the same across Sub-Channels and Ranks
    for (McCh = 0; McCh < MAX_CHANNEL_SHARE_REGS; McCh++) {
      // Configure the timing registers for the current MC Channel.
      Status = ConfigGenMrsFsmTiming (
                MrcData,
                Controller,
                McCh
                );
      if (Status != mrcSuccess) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Generic FSM Timing", gErrString);
        if (Status == mrcWrongInputParameter) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Delay Overflow");
        }
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_ERROR,
          " - Mc: %u, IpCh: %u\n",
          Controller, McCh);
        return Status;
      }
      StorageIdx = 0;
      SaveStorageIdx = 0;
      EnablePdaSelectAll = FALSE;
      IsDimmDfeMrBit0    = FALSE;
      MrcCall->MrcSetMem (DfeMrTapStorageIdx, MAX_DFE_TAP_DDR5, 0);
      MRC_MC_IP_DEBUG_MSG (Debug, MSG_LEVEL_ALGO, "Mc%d.IpCh%d\n\tStorageIdx = %d\n", Controller, McCh, StorageIdx);
      ValidControlRegIndex = 0;
      for (ControlRegIdx = 0; ControlRegIdx < MAX_MR_GEN_FSM; ControlRegIdx++) {
        MrPerRank = FALSE;
        InitData  = MRC_UINT16_MAX;
        // Gate SubCh and Rank loops with MrPerRank == FALSE.
        // Once we find that they are different per rank; break out and start the register configuration
        for (SubCh = 0; ((SubCh < MaxSubCh) && (!MrPerRank)); SubCh++) {
          Channel = (McCh * MaxSubCh) + SubCh;
          for (Rank = 0; ((Rank < MAX_RANK_IN_CHANNEL) && (!MrPerRank)); Rank++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              continue;
            }

            GenMrPtr = &MrData[Controller][Channel][Rank][ControlRegIdx];
            if (!GenMrPtr->Valid) {
              continue;
            }

            // Base Case
            if (InitData == MRC_UINT16_MAX) {
              InitData = GenMrPtr->MrData;
            } else if ((MrPerRankLookup == NULL) && (InitData != GenMrPtr->MrData)) {
              // If MrData values differ between ranks then configure as per-rank
              MrPerRank = TRUE;
            } else if ((MrPerRankLookup != NULL) && IsMrPerRank (GenMrPtr->MrAddr, MrPerRankLookup)) {
              MrPerRank = TRUE;
            }

            if ((GenMrPtr->CmdType == GmfCmdMpc) && (GenMrPtr->MrData == DDR5_MPC_ZQCAL_LATCH)) {
              // Turn off automatic ZQCAL when issued as part of the input sequence
              DoZqCal = FALSE;
            }
          } // Rank
        } // SubCh

        // If InitData is not another value besides MRC_UINT16_MAX, then this ControlRegIdx
        // did not have a valid entry in the array for the subch's or ranks.  Skip this index.
        if (InitData == MRC_UINT16_MAX) {
          continue;
        }

        if (GenMrPtr == NULL) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "GenMrPtr is Null\n");
          return mrcFail;
        }
        if ((GenMrPtr->CmdType == GmfCmdMpc) && (GenMrPtr->MrData == DDR5_MPC_PDA_SELECT_ID(0xf)) && !EnablePdaSelectAll) {
          // Ignore PDA Select all if there is no PDA command in the sequence
          continue;
        }

        // In DDR5 DIMM DFE there is MR per bit for each TAP (TAP 1-4)
        // For example Tap1 has {mrMR129, mrMR137, mrMR145, mrMR153, mrMR161, mrMR169, mrMR177, mrMR185} for x8 device
        // All the bits have the same DFE value (it's not trained per bit), but we still need to configure MR's for all the bits.
        // In order to save space in the Generic FSM storage we point to Bit 0 storage for all the bits (0-7)
        if (Ddr5) {
          RestoreStorageIdx = FALSE;
          if (Ddr5IsDimmDfeMr (GenMrPtr->MrAddr)) {
            DfeMrTap = Ddr5GetDimmDfeTap (GenMrPtr->MrAddr, &IsDimmDfeMrBit0);
            if (IsDimmDfeMrBit0) { // First Bit MR in this TAP
              DfeMrTapStorageIdx[DfeMrTap] = StorageIdx;
            } else {
              RestoreStorageIdx = TRUE;
              SaveStorageIdx = StorageIdx;
              StorageIdx = DfeMrTapStorageIdx[DfeMrTap];
            }
          }
        }

        Status = ConfigGenMrsFsmValue (
                  MrcData,
                  Controller,
                  McCh,
                  GenMrPtr,
                  MrPerRank,
                  ValidControlRegIndex,
                  MrData,
                  &StorageIdx
                  );
        if (Status != mrcSuccess) {
          return Status;
        }
        if (Ddr5 && RestoreStorageIdx) {
          StorageIdx = SaveStorageIdx;
        }
        ValidControlRegIndex++;  // Increment the Actual
        if (GenMrPtr->FreqSwitchPoint) {
          SagvBreakPoint = TRUE;
          FspOpSwitchIdx = ControlRegIdx;
        }
        // check if there is at least one PDA Mr to handel in the sequence
        if (GenMrPtr->PdaMr) {
          GlobalDevLoopEnable = TRUE;
          EnablePdaSelectAll  = TRUE;
        }
      } // ControlRegIdx
    } // McCh
  } // Controller

  // Finally configure the MRS_FSM to use the Generic version if this is the first time sending the sequence
  MrsFsmCtl.Data = 0;
  MrsFsmCtl.Bits.GENERIC_MRS_FSM_Enable = 1;
  MrsFsmCtl.Bits.GENERIC_MRS_FSM_Device_Loop_Enable = GlobalDevLoopEnable;
  MrsFsmCtl.Bits.do_PDA_for_ECC = Outputs->EccSupport;
  if (SagvBreakPoint) {
    MrsFsmCtl.Bits.LPDDR4_split_transition = 1;
    MrsFsmCtl.Bits.LPDDR4_switch_FSP       = Lpddr ? 1 : 0;
    MrsFsmCtl.Bits.GENERIC_MRS_FSM_Breakpoint_Address = FspOpSwitchIdx;
    MrsFsmCtl.Bits.do_dq_osc_start = (Inputs->LpDqsOscEn) ? 1 : 0;
    MrsFsmCtl.Bits.GV_auto_enable = 1;
    if (Lpddr) {
      DoZqCal = FALSE;
    }
  }
  if (DoZqCal) {
    MrsFsmCtl.Bits.do_ZQCL = 1;
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      Offset = MC0_BC_CR_MRS_FSM_CONTROL_REG +
             ((MC1_BC_CR_MRS_FSM_CONTROL_REG - MC0_BC_CR_MRS_FSM_CONTROL_REG) * Controller);
      MrcWriteCR64 (MrcData, Offset, MrsFsmCtl.Data);
    }
  }

  return mrcSuccess;
}

/**
  This function executes the MRS FSM and waits for the FSM to complete.
  If the FSM does not complete after 10 seconds, it will return an error message.

  @param[in] MrcData - Pointer to MRC global data.

  @retval mrcFail if the FSM is not idle.
  @retval mrcSuccess otherwise.
**/
MrcStatus
MrcGenMrsFsmRun (
  IN  MrcParameters *MrcData
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcOutput *Outputs;
  UINT64    Timeout;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    IpCh;
  UINT32    Offset;
  UINT8     MaxChannel;
  BOOLEAN   Lpddr;
  BOOLEAN   Flag;
  MC0_CH0_CR_MRS_FSM_RUN_STRUCT MrsFsmRun;

  Outputs = &MrcData->Outputs;
  MrcCall = MrcData->Inputs.Call.Func;
  Lpddr = Outputs->Lpddr;
  MaxChannel = Outputs->MaxChannels;

  // Start all FSM's in parallel
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        IpCh = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MRS_FSM_RUN_REG, MC1_CH0_CR_MRS_FSM_RUN_REG, Controller, MC0_CH1_CR_MRS_FSM_RUN_REG, IpCh);

        MrsFsmRun.Data = 0;
        MrsFsmRun.Bits.Run = 1;
        MrcWriteCR (MrcData, Offset, MrsFsmRun.Data);

        // If we're LPDDR and we are on Channel 0 or 2, increment channel additionally so we move to the next MC Channel.
        if (Lpddr && ((Channel % 2) == 0)) {
          Channel++;
        }
      }
    }
  }

  // Check that all FSM's are done
  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT; // 10 seconds timeout
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        IpCh = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MRS_FSM_RUN_REG, MC1_CH0_CR_MRS_FSM_RUN_REG, Controller, MC0_CH1_CR_MRS_FSM_RUN_REG, IpCh);

        do {
          MrsFsmRun.Data = MrcReadCR (MrcData, Offset);
          Flag = (MrsFsmRun.Bits.Run == 1);
        } while (Flag && (MrcCall->MrcGetCpuTime () < Timeout));
        if (Flag) {
          MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "Generic FSM is not idle\n");
          return mrcFail;
        }

        // If we're LPDDR and we are on Channel 0 or 2, increment channel additionally so we move to the next MC Channel.
        if (Lpddr && ((Channel % 2) == 0)) {
          Channel++;
        }
      }
    }
  }

  return mrcSuccess;
}

/**
  This function cleans the MRS FSM valid control on all Controllers Channels.

  @param[in] MrcData - Pointer to MRC global data.
  @param[in] MrData  - Pointer to an array of MR data to configure the MRS FSM with.
  @param[in] CleanAll - If set to TRUE, MrData values will be ignored and all Control Registers will be cleared

  @retval mrcFail if clean failed.
  @retval mrcSuccess otherwise.
**/
MrcStatus
MrcGenMrsFsmClean (
  IN  MrcParameters *MrcData,
  IN  MRC_GEN_MRS_FSM_MR_TYPE MrData[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_MR_GEN_FSM],
  IN  BOOLEAN       CleanAll
)
{
  MrcOutput *Outputs;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    Rank;
  UINT32    McCh;
  UINT32    Offset;
  UINT8     MaxChannel;
  UINT8     ControlRegIdx;
  UINT8     ValidControlRegIndex;
  BOOLEAN   Lpddr;
  MRC_GEN_MRS_FSM_MR_TYPE                               *GenMrPtr;
  MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_STRUCT           GenMrsFsmCtl;


  Outputs = &MrcData->Outputs;
  Lpddr = Outputs->Lpddr;
  MaxChannel = Outputs->MaxChannels;

  // Run on  all FSM's
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
            McCh = LP_IP_CH (Lpddr, Channel);
            ValidControlRegIndex = 0;
            for (ControlRegIdx = 0; ControlRegIdx < MAX_MR_GEN_FSM; ControlRegIdx++) {
              GenMrPtr = &MrData[Controller][Channel][Rank][ControlRegIdx];
              if (GenMrPtr->Valid || CleanAll) {
                // Clean control register for this MR.
                GenMrsFsmCtl.Data = 0;
                Offset = MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG +
                  ((MC1_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG - MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG) * Controller) +
                  ((MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_0_REG - MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG) * McCh) +
                  ((MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_1_REG - MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG) * ValidControlRegIndex);
                MrcWriteCR (MrcData, Offset, GenMrsFsmCtl.Data);
                ValidControlRegIndex++;
              }
            }
            // The first active rank in the channel is enough , the FSM works per channel
            break;
          }
        }
        // If we're LPDDR and we are on Channel 0 or 2, increment channel additionally so we move to the next MC Channel.
        if (Lpddr && ((Channel % 2) == 0)) {
          Channel++;
        }
      }
    }
  }
    return mrcSuccess;
}

/**
  Executes DDR4 generic MRH command

  @param[in] MrcData      - Pointer to global data
  @param[in] Controller   - Targeted controller
  @param[in] Channel      - Targeted channel
  @param[in] Rank         - Targeted rank
  @param[in] Ca_Bus       - Command bus instruction

  @retval mrcStatus
**/
MrcStatus
MrcDdr4RunGenericMrh (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN Ddr4CaBus     *const Ca_Bus
  )
{
  const MRC_FUNCTION   *MrcCall;
  MrcStatus Status = mrcSuccess;
  UINT32    Offset = 0;
  BOOLEAN   Busy   = FALSE;
  MC0_CH0_CR_MRH_GENERIC_COMMAND_STRUCT MrhGenericCommand;
  MC0_CH0_CR_DDR_MR_COMMAND_STRUCT      DdrMrCommand;

  MrcCall = MrcData->Inputs.Call.Func;
  UINT64 Timeout = MrcCall->MrcGetCpuTime() + MRC_WAIT_TIMEOUT;   // 10 seconds timeout

  MrhGenericCommand.Data = Ca_Bus->Data;            // ca_bus: Precharge banks
  MrhGenericCommand.Bits.two_cyc_command = 0;       // two_cyc_command
  MrhGenericCommand.Bits.signal_mr18_19 = 0;        // signal_mr18_19
  MrhGenericCommand.Bits.Generic_MRH_override = 1;  // Generic_MRH_override: True

  Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR_MR_COMMAND_REG, MC1_CH0_CR_DDR_MR_COMMAND_REG, Controller, MC0_CH1_CR_DDR_MR_COMMAND_REG, Channel);
  //
  // Make sure MRH is not busy
  //
  do {
    DdrMrCommand.Data = MrcReadCR(MrcData, Offset);
    Busy = (DdrMrCommand.Bits.Busy == 1);
} while (Busy && (MrcCall->MrcGetCpuTime() < Timeout));
if (Busy) {
  //MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "Timed out waiting for previous MRH command to finish!\n");
  return mrcDeviceBusy;
}

  Offset = MC0_CH0_CR_MRH_GENERIC_COMMAND_REG +
    ((MC1_CH0_CR_MRH_GENERIC_COMMAND_REG - MC0_CH0_CR_MRH_GENERIC_COMMAND_REG) * Controller) +
    ((MC0_CH1_CR_MRH_GENERIC_COMMAND_REG - MC0_CH0_CR_MRH_GENERIC_COMMAND_REG) * Channel);
  MrcWriteCR(MrcData, Offset, MrhGenericCommand.Data);

  Status = MrcRunMrh (MrcData, Controller, Channel, Rank, 0, 0, 0x2, FALSE);

  MrhGenericCommand.Bits.Generic_MRH_override = 0;  // Generic_MRH_override: False
  MrcWriteCR (MrcData, Offset, MrhGenericCommand.Data);

  return Status;
}

/**
Executes LPDDR4 generic MRH command

  @param[in] MrcData         - Pointer to global data
  @param[in] Controller      - Targeted controller
  @param[in] Channel         - Targeted channel
  @param[in] Rank            - Targeted rank
  @param[in] Ca_Bus          - Command bus instruction
  @param[in] TwoCycleCommand - Determines whether command should be two cycles or not.

  @retval mrsStatus
**/

MrcStatus
MrcRunGenericMrh (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN UINT32 const         Ca_Bus,
  IN BOOLEAN              TwoCycleCommand
  )
{
  const MRC_FUNCTION   *MrcCall;
  MrcStatus Status = mrcSuccess;
  UINT32    Offset = 0;
  BOOLEAN   Busy   = FALSE;
  MC0_CH0_CR_MRH_GENERIC_COMMAND_STRUCT MrhGenericCommand;
  MC0_CH0_CR_DDR_MR_COMMAND_STRUCT      DdrMrCommand;

  MrcCall = MrcData->Inputs.Call.Func;
  UINT64 Timeout = MrcCall->MrcGetCpuTime() + MRC_WAIT_TIMEOUT;   // 10 seconds timeout

  MrhGenericCommand.Data = Ca_Bus;                                      // ca_bus: Precharge banks
  MrhGenericCommand.Bits.two_cyc_command = TwoCycleCommand ? 0x1 : 0x0; // two_cyc_command
  MrhGenericCommand.Bits.signal_mr18_19 = 0;                            // signal_mr18_19
  MrhGenericCommand.Bits.Generic_MRH_override = 1;                      // Generic_MRH_override: True

  Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR_MR_COMMAND_REG, MC1_CH0_CR_DDR_MR_COMMAND_REG, Controller, MC0_CH1_CR_DDR_MR_COMMAND_REG, Channel);
  //
  // Make sure MRH is not busy
  //
  do {
    DdrMrCommand.Data = MrcReadCR(MrcData, Offset);
    Busy = (DdrMrCommand.Bits.Busy == 1);
  } while (Busy && (MrcCall->MrcGetCpuTime() < Timeout));
  if (Busy) {
    //MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "Timed out waiting for previous MRH command to finish!\n");
    return mrcDeviceBusy;
  }

  Offset = MC0_CH0_CR_MRH_GENERIC_COMMAND_REG +
    ((MC1_CH0_CR_MRH_GENERIC_COMMAND_REG - MC0_CH0_CR_MRH_GENERIC_COMMAND_REG) * Controller) +
    ((MC0_CH1_CR_MRH_GENERIC_COMMAND_REG - MC0_CH0_CR_MRH_GENERIC_COMMAND_REG) * Channel);
  MrcWriteCR(MrcData, Offset, MrhGenericCommand.Data);

  Status = MrcRunMrh (MrcData, Controller, Channel, Rank, 0, 0, MRC_MRH_CMD_LP4_MPC, FALSE);

  MrhGenericCommand.Bits.Generic_MRH_override = 0;  // Generic_MRH_override: False
  MrcWriteCR (MrcData, Offset, MrhGenericCommand.Data);

  return Status;
}

UINT32
MrcQclkToTck (
  IN  MrcParameters *const MrcData,
  IN  UINT32               Qclk
  )
{
  MrcOutput *Outputs;
  UINT32    RetVal;
  UINT32    Scalar;
  BOOLEAN   Gear2;

  Outputs = &MrcData->Outputs;
  Gear2 = Outputs->Gear2;

  if (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5) {
    Scalar = Gear2 ? 4 : 8;
  } else {
    Scalar = Gear2 ? 1 : 2;
  }

  RetVal = Qclk / Scalar;

  return RetVal;
}

/**
  Program MC/DDRIO registers to Gear1 / Gear2 / Gear4 mode.
  This only includes Gear mode enable/disable, not other registers that are impacted by gear mode.
  The current Gear mode is set based on Outputs->Gear2 and Outputs->Gear4.

  @param[in] MrcData - The MRC general data.
**/
VOID
MrcSetGear (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput *Outputs;
  INT64     GetSetVal;

  Outputs = &MrcData->Outputs;

  GetSetVal = Outputs->Gear4;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccGear4, WriteToCache, &GetSetVal);

  GetSetVal = (Outputs->Gear2) ? 1 : 0;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccGear2, WriteToCache, &GetSetVal);

  MrcFlushRegisterCachedData(MrcData);

  DdrIoConfigGear (MrcData);
}

UINT32
MrcTckToQclk (
  IN  MrcParameters *const MrcData,
  IN  UINT32               Tck
  )
{
  MrcOutput *Outputs;
  UINT32    RetVal;
  UINT32    Scalar;
  BOOLEAN   Gear2;

  Outputs = &MrcData->Outputs;
  Gear2 = Outputs->Gear2;

  if (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5) {
    Scalar = Gear2 ? 4 : 8;
  } else {
    Scalar = Gear2 ? 1 : 2;
  }

  RetVal = Tck * Scalar;

  return RetVal;
}

/**
  Programming of CCC_CR_DDRCRCCCCLKCONTROLS_BlockTrainRst

  @param[in] MrcData - The MRC global data.
  @param[in] BlockTrainReset - TRUE to BlockTrainReset for most training algos.  FALSE for specific training algos that need PiDivider sync.

**/
VOID
MrcBlockTrainResetToggle (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              BlockTrainReset
)
{
  UINT8   Value;
  UINT8   Index;
  UINT32  Offset;
  CH0CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT  CccClkControls;


  Value = (BlockTrainReset) ? 1 : 0;

  for (Index = 0; Index < MAX_SYS_CHANNEL; Index++) {
    if (MrcData->Inputs.A0) {
      Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG, CH0CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG, Index);
    } else {
      Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG, CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG, Index);
    }
    CccClkControls.Data = MrcReadCR (MrcData, Offset);
    if (CccClkControls.Bits.BlockTrainRst == Value) {
      continue;
    }
    CccClkControls.Bits.BlockTrainRst = Value;
    MrcWriteCR (MrcData, Offset, CccClkControls.Data);
  }
}

/**
  This function configures the DDRCRCMDBUSTRAIN register to values for normal mode.

  @param[in]  MrcData - Pointer to global MRC data.

  @retval none.
**/
VOID
MrcSetWritePreamble (
  IN  MrcParameters *const  MrcData
  )
{
  MrcOutput         *Outputs;
  MrcDdrType        DdrType;
  BOOLEAN           Ddr5;
  INT64             NumMaskedPulses;
  INT64             NumOfPulses;
  INT64             WrPreamble;

  Outputs           = &MrcData->Outputs;
  DdrType           = Outputs->DdrType;
  Ddr5              = (DdrType == MRC_DDR_TYPE_DDR5);

  NumMaskedPulses = (Ddr5 ? 1 : 0);
  NumOfPulses = 3;

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqsMaskPulseCnt, WriteToCache, &NumMaskedPulses);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqsPulseCnt,     WriteToCache, &NumOfPulses);

  if (Ddr5) {
    WrPreamble = 1;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataWrPreamble, WriteToCache, &WrPreamble);
  }
  MrcFlushRegisterCachedData (MrcData);
}

/**
  Returns the currently configured DRAM Command Intput Rate NMode.
  Note: In DDR4 3N is used during training (in Gear1), but this
  function won't report this.

  @param[in] MrcData    - Include all MRC global data.

  @retval 1 = 1N Mode
  @retval 2 = 2N Mode
**/
UINT32
MrcGetNMode (
  IN MrcParameters *const MrcData
  )
{
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MrcChannelOut     *ChannelOut;
  MrcControllerOut  *ControllerOut;
  MrcProfile        Profile;
  UINT32            FirstController;
  UINT32            FirstChannel;
  UINT32            NMode;


  Inputs          = &MrcData->Inputs;
  Outputs         = &MrcData->Outputs;
  Profile         = Inputs->MemoryProfile;
  FirstController = (MrcControllerExist (MrcData, cCONTROLLER0)) ? 0 : 1;
  ControllerOut   = &Outputs->Controller[FirstController];
  FirstChannel    = ControllerOut->FirstPopCh;
  ChannelOut      = &ControllerOut->Channel[FirstChannel];

  // In DDR4 3N is used during training (in Gear1), but this
  // function won't report this.
  NMode = ChannelOut->Timing[Profile].NMode;

  return NMode;
}

/**
  Configure the MC to issue multicycle CS_n MPC commands.
  The DRAM must be configured separately by either setting
  DDR5 MR2.OP[4] = 0 or by resetting the DRAM.

  @param[in] MrcData    - Include all MRC global data.

  @retval None
**/
void
EnableMcMulticycleCs (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput *Outputs;
  UINT32    Gear;
  UINT8     Controller;
  UINT8     Channel;
  UINT32    Offset;
  UINT32    NMode;
  INT64     MultiCycCmd;
  MC0_CH0_CR_TC_MPC_STRUCT TcMpc;

  Outputs           = &MrcData->Outputs;
  Gear              = Outputs->Gear2 ? 2 : 1;
  NMode             = MrcGetNMode (MrcData);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_TC_MPC_REG, MC1_CH0_CR_TC_MPC_REG, Controller, MC0_CH1_CR_TC_MPC_REG, Channel);
        TcMpc.Data = 0;
        TcMpc.Bits.MPC_Setup  = (NMode == 2) ? (DDR5_tMC_MPC_SETUP_MIN + Gear): DDR5_tMC_MPC_SETUP_MIN;
        TcMpc.Bits.MPC_Hold   = DDR5_tMC_MPC_HOLD_MIN;
        TcMpc.Bits.MultiCycCS = DDR5_tMPC_CS;
        MultiCycCmd             = 1;
        MrcWriteCR (MrcData, Offset, TcMpc.Data);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMultiCycCmd, WriteCached, &MultiCycCmd);
      }
    } // Channel
  } // Controller
}

/**
  Configure the MC and DRAM for single cycle CS_n MPC commands.
  An MRW is issued to the DRAM to configure DDR5 MR2[4] = 1.

  @param[in] MrcData    - Include all MRC global data.
  @param[in] McOnly     - TRUE: program MC side only; FALSE: program both MC and DRAM.

  @retval None
**/
void
DisableMcMulticycleCs (
  IN MrcParameters *const MrcData,
  IN const BOOLEAN        McOnly
  )
{
  MrcOutput         *Outputs;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *ChannelOut;
  UINT32            Controller;
  UINT32            Channel;
  UINT32            Rank;
  UINT16            *MrPtr;
  UINT32            MrIndex;
  INT64             MultiCycCmd;
  DDR5_MODE_REGISTER_2_TYPE Mr2;
  UINT32            Dimm;
  UINT32            DimmRank;

  Outputs = &MrcData->Outputs;

  // Set the MC register
  MultiCycCmd = 0;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMultiCycCmd, WriteCached, &MultiCycCmd);

  if (McOnly) { // No need to program the DRAM side
    return;
  }

  MrIndex = mrIndexMR2;

  // Set the DRAM Mode Register
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; (Channel < Outputs->MaxChannels); Channel++) {
      ChannelOut = &ControllerOut->Channel[Channel];
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        Dimm = RANK_TO_DIMM_NUMBER (Rank);
        DimmRank = Rank % MAX_RANK_IN_DIMM;
        MrPtr = ChannelOut->Dimm[Dimm].Rank[DimmRank].MR;
        Mr2.Data8 = (UINT8) MrPtr[MrIndex];
        Mr2.Bits.CsAssertionDuration = 1; // Only a single cycle of CS assertion supported for MPC and VrefCA commands
        MrPtr[MrIndex] = Mr2.Data8;
        // Issue MRW to DDR5 MR2
        MrcIssueMrw (
          MrcData,
          Controller,
          Channel,
          Rank,
          mrMR2,
          Mr2.Data8,
          MRC_PRINTS_ON   // DebugPrint
          );
      }
    }
  }
}

/**
  Read the first RCOMPDATA0 Rcomp register on the first available MC channel.

  @param[in] MrcData      - Pointer to MRC global data.

  @retval UINT32 - The data read from RCOMPDATA0 on the first available MC channel.
**/
UINT32
ReadFirstRcompReg (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcOutput         *Outputs;
  UINT32            Offset;
  UINT32            FirstByte;
  UINT8             FirstController;
  UINT8             FirstChannel;
  DATA0CH0_CR_DDRCRDATACOMP0_STRUCT CompData0;

  Outputs        = &MrcData->Outputs;
  CompData0.Data = 0;

  // Read the first populated RCOMPDATA register
  FirstController = (MrcControllerExist (MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel = Outputs->Controller[FirstController].FirstPopCh;
  FirstByte = MrcFirstByte (MrcData, FirstChannel);
  Offset = DATA0CH0_CR_DDRCRDATACOMP0_REG;
  Offset += (DATA0CH1_CR_DDRCRDATACOMP0_REG - DATA0CH0_CR_DDRCRDATACOMP0_REG) * FirstController +
    (DATA1CH0_CR_DDRCRDATACOMP0_REG - DATA0CH0_CR_DDRCRDATACOMP0_REG) * FirstByte;
  CompData0.Data = MrcReadCR (MrcData, Offset);

  return CompData0.Data;
}

/**
  This function returns the afe2dig_dqrx_dq*_rxumdataunflopped fields from AFEMISCCONTROL0 register
    for the specified Controller, Channel and Byte. All 8 DQ fields are combined and returned together.

  @param[in] MrcData    - Pointer to MRC global data.
  @param[in] Controller - Zero based memory controller number
  @param[in] Channel    - Zero based memory channel number
  @param[in] Byte       - Zero based memory byte number

  @retval afe2dig_dqrx_dq[0...7]_rxumdataunflopped
**/
UINT8
ReadAfeMiscControl0Reg (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Byte
  )
{
  UINT32 ControllerOffset;
  UINT32 ChannelOffset;
  UINT32 ByteOffset;
  UINT32 Rank;
  UINT32 Offset;
  UINT32 RetVal;
  UINT32 DqMask;

  ControllerOffset  = Controller;
  ChannelOffset     = Channel;
  ByteOffset        = Byte;
  
  // Rank is not used
  MrcTranslateSystemToIp (MrcData, &ControllerOffset, &ChannelOffset, &Rank, &ByteOffset, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
  Offset  = DATA0CH0_CR_AFEMISCCTRL0_REG +
    (DATA0CH1_CR_AFEMISCCTRL0_REG - DATA0CH0_CR_AFEMISCCTRL0_REG) * ChannelOffset +
    (DATA1CH0_CR_AFEMISCCTRL0_REG - DATA0CH0_CR_AFEMISCCTRL0_REG) * ByteOffset;

  RetVal = MrcReadCR (MrcData, Offset);

  // DQ7 is LSB, DQ0 is MSB, so for DDR5 ECC we need to look at bits [7:4] only
  DqMask = ((MrcData->Outputs.DdrType == MRC_DDR_TYPE_DDR5) && (Byte == MRC_DDR5_ECC_BYTE)) ? 0xF0 : 0xFF;
  RetVal = (RetVal >> DATA0CH0_CR_AFEMISCCTRL0_afe2dig_dqrx_dq7_rxumdataunflopped_OFF) & DqMask;

  return (UINT8) RetVal;
}

/**
  This funtion finds the maximum possible increment and  decrement values for write leveling trainings

  @param[in] MrcData - Pointer to MRC global data.
  @param[out] MaxAdd  - Max possible increment value for write leveling trainings
  @param[out] MaxDec  - Max possible decrement value for write leveling trainings

  @retval Success/mrcWrongInputParameter
**/
MrcStatus
MrcMcTxCycleLimits (
  IN MrcParameters *const MrcData,
  OUT UINT32              *MaxAdd,
  OUT UINT32              *MaxDec
  )
{
  UINT32              tCWL;
  INT32               TxFifoSeparation;
  INT64               GetSetVal;
  UINT8               FirstChannel;
  UINT8               FirstController;
  BOOLEAN             Lpddr4;
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcInput            *Inputs;
  UINT32              Ddr5Cmd2N;

  Outputs          = &MrcData->Outputs;
  Inputs           = &MrcData->Inputs;
  Debug            = &Outputs->Debug;
  Lpddr4           = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  FirstController  = (MrcControllerExist(MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel     = Outputs->Controller[FirstController].FirstPopCh;
  Ddr5Cmd2N        = ((Outputs->DdrType == MRC_DDR_TYPE_DDR5) && (MrcGetNMode (MrcData) == 2)) ? 1 : 0;

  if ((MaxAdd == NULL) || (MaxDec == NULL)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s - NULL output pointer\n", gErrString);
    return mrcWrongInputParameter;
  }

  *MaxAdd = MAX_ADD_DELAY * Outputs->HighGear + MAX_ADD_RANK_DELAY;
  *MaxDec = 0;

  MrcGetSetMcCh (MrcData, FirstController, FirstChannel, GsmMctCWL, ReadFromCache, &GetSetVal);
  tCWL = (UINT32) GetSetVal;
  if (Lpddr4) {
    tCWL++;     // Due to tDQSS on LP4
  }

  TxFifoSeparation = MrcGetTxFifoSeparation (MrcData);
  // Make sure TxDqFifoRdEnTcwlDelay won't become negative
  if (Outputs->Gear2) {
    *MaxDec = (tCWL - (Lpddr4 ? 1 : 3) + (tCWL % 2) - 2 * Lpddr4 * (tCWL % 2) + ((TxFifoSeparation < 0) ? (TxFifoSeparation + 1) : 0)) / 2;
  } else if (Outputs->Gear4) {
    *MaxDec = (tCWL - 3 + (tCWL % 4) - 3 * Ddr5Cmd2N + ((TxFifoSeparation < 0) ? (TxFifoSeparation + 1) : 0)) / 4;
    if (Lpddr4) {
      *MaxDec = (tCWL - 9 + ((TxFifoSeparation < 0) ? (TxFifoSeparation + 1) : 0)) / 4;
    }
  } else {
    *MaxDec = tCWL - 2 + ((TxFifoSeparation < 0) ? TxFifoSeparation : 0);
  }
  if (Inputs->LoopBackTest) {
    *MaxDec = Outputs->Gear2 ? 4 : 6;   // We use big tCWL, so no reason to start sweep too early
  }
  if (Outputs->Gear4) {
    *MaxDec = MIN (*MaxDec, 5); // there is a hard constraint in MC that we cannot have dec more than 5
  }
  MRC_WRLVL_FLYBY_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s : MaxAdd = %d, MaxDec = %d \n", __FUNCTION__, *MaxAdd, *MaxDec);

  return mrcSuccess;
}


/**
  Find the minimum and maximum PI setting across Tx DQ/DQS on a given Rank, on all channels.
  Determine how far we can use the PI value to shift the Cycle. Min value will use DQS,
  Max value will use DQ (DQ is DQS + 32 (gear1) or DQS + 96 (gear2), and also plus tDQS2DQ for LP4).

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  Rank          - Current working rank
  @param[in]  MaxAdd        - Max possible increment value for write leveling trainings
  @param[in]  MaxDec        - Max possible decrement value for write leveling trainings
  @param[out] StartOffset   - Starting offset
  @param[out] EndOffset     - End of limit
  @param[out] SavedTxDqsVal - Pointer to array where current Tx Dqs timings will be stored

  @retval MrcStatus - If it succeeded, return mrcSuccess
**/

MrcStatus
MrcIoTxLimits (
  IN  MrcParameters *const MrcData,
  IN  UINT32               Rank,
  IN  UINT32               MaxAdd,
  IN  UINT32               MaxDec,
  OUT INT32               *StartOffset,
  OUT INT32               *EndOffset,
  OUT UINT16              SavedTxDqsVal[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  UINT32      Controller;
  UINT32      Channel;
  INT64       GetSetVal;
  UINT16      TxDqsValue;
  UINT16      TxDqMaxVal;
  UINT16      MaxCode;
  UINT16      MinCode;
  UINT8       Byte;
  UINT8       PiCycleDec;
  UINT8       PiCycleAdd;
  MrcOutput   *Outputs;
  MrcDebug    *Debug;

  MaxCode         = 0;
  Outputs         = &MrcData->Outputs;
  Debug           = &Outputs->Debug;

  if ((EndOffset == NULL) || (StartOffset == NULL) || (SavedTxDqsVal == NULL)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s - NULL output pointer\n", gErrString);
    return mrcWrongInputParameter;
  }

  MrcGetSetLimits (MrcData, TxDqDelay, NULL, &GetSetVal, NULL);
  TxDqMaxVal  = (UINT16) GetSetVal;
  MinCode     = TxDqMaxVal;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
        continue;
      }
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay, ReadFromCache, &GetSetVal);
        MaxCode = MAX (MaxCode, (UINT16) GetSetVal);

        MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, ReadFromCache, &GetSetVal);
        TxDqsValue = (UINT16) GetSetVal;
        // Save current TcDqs value
        SavedTxDqsVal[Controller][Channel][Byte] = TxDqsValue;
        MinCode = MIN (MinCode, TxDqsValue);
      }
    }
  } //Controller

  PiCycleDec = (UINT8) (MinCode / 128);
  PiCycleDec = 0;   // We have enough MaxDec range, no need to use PI settings
  PiCycleAdd = (UINT8) ((TxDqMaxVal - MaxCode) / 128);

  if (Outputs->Gear2) {
    PiCycleDec = 0;
    *StartOffset = - 2 * MaxDec;
  } else if (Outputs->Gear4) {
    PiCycleDec = 0;
    *StartOffset = (-4 * MaxDec) - 3;
  } else {
    *StartOffset = -PiCycleDec - MaxDec;  // We can go more negative offsets using PI settings
  }
  *EndOffset   = MaxAdd + PiCycleAdd;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Params : MaxDec: %d, MaxAdd: %d, PiCycleDec: %d, PiCycleAdd: %d\n", MaxDec, MaxAdd, PiCycleDec, PiCycleAdd);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MinCode: %d, MaxCode: %d, Start: %d, End: %d\n", MinCode, MaxCode, *StartOffset, *EndOffset);

  return mrcSuccess;
}

/**
  Programs new delay offsets to DQ/DQS timing

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  Rank          - Current working rank
  @param[in]  MaxAdd        - Max possible increment value for write leveling trainings
  @param[in]  MaxDec        - Max possible decrement value for write leveling trainings
  @param[in]  UiLoop        - The current UiLoop used in loopback mode
  @param[in]  SavedTxDqsVal - array of the initial TX DQS Delays
  @param[in]  TxDelayOffset - Delay to be programmed

  @retval mrcSuccess or mrcTimingError if minimum TX fifo separation is not met
**/

MrcStatus
SetWriteCycleDelay (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Rank,
  IN  UINT32                MaxAdd,
  IN  UINT32                MaxDec,
  IN  UINT32                UiLoop,
  IN  UINT16                SavedTxDqsVal[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN  INT32                 TxDelayOffset
  )
{
  UINT32                        Controller;
  UINT32                        Channel;
  UINT32                        IpChannel;
  UINT32                        Offset;
  UINT32                        tCWL;
  UINT8                         Byte;
  UINT32                        FirstChannel;
  UINT32                        FirstController;
  INT32                         GlobalByteOff;
  INT64                         FifoFlybyDelay;
  INT64                         GetSetVal;
  INT64                         TxDqDqsDelayAdj;
  INT64                         TxDqFifoWrEn;
  INT64                         TxDqFifoRdEn;
  INT32                         CycleDelay[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32                        AddOneQclk[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN                       Lpddr;
  BOOLEAN                       Lpddr4;
  const MRC_FUNCTION            *MrcCall;
  const MrcInput                *Inputs;
  MrcOutput                     *Outputs;
  MrcDebug                      *Debug;
  MC0_CH0_CR_SC_WR_DELAY_STRUCT ScWrDelay;

  Inputs           = &MrcData->Inputs;
  Outputs          = &MrcData->Outputs;
  Debug            = &Outputs->Debug;
  MrcCall          = Inputs->Call.Func;
  Lpddr            = Outputs->Lpddr;
  // 0.5UI for Gear 1, 1.5UI for Gear 2
  TxDqDqsDelayAdj  = (Outputs->Gear2 || Outputs->Gear4) ? 96 : 32;
  Lpddr4           = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  FirstController  = (MrcControllerExist(MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel     = MrcData->Outputs.Controller[FirstController].FirstPopCh;

  MrcCall->MrcSetMem ((UINT8 *) CycleDelay, sizeof (CycleDelay), 0);
  MrcCall->MrcSetMem ((UINT8 *) AddOneQclk, sizeof (AddOneQclk), 0);

  MrcGetSetMcCh (MrcData, FirstController, FirstChannel, GsmMctCWL, ReadFromCache, &GetSetVal);
  tCWL = (UINT32) GetSetVal;
  if (Lpddr4) {
    tCWL++;     // Due to tDQSS on LP4
  }

  // Program new delay offsets to DQ/DQS timing:
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
        continue;
      }
      GlobalByteOff = 0;

      if (Outputs->Gear4) {
        AddOneQclk[Controller][Channel] = 0;
        if (TxDelayOffset > (INT32) MaxAdd) {                 // Will use DQ/DQS PI to go right
          CycleDelay[Controller][Channel] = MAX_ADD_DELAY + MAX_ADD_RANK_DELAY;
          GlobalByteOff = 128 * (TxDelayOffset - MaxAdd);
        } else if (TxDelayOffset < -3 - 4 * (INT32) MaxDec) {     // Will use DQ/DQS PI to go left
          CycleDelay[Controller][Channel] = -1 * MaxDec;
          GlobalByteOff = 128 * (TxDelayOffset + 4 * MaxDec);
        } else {
          if (TxDelayOffset > 4 * MAX_ADD_DELAY) {            // Will use TxDqFifoRdEnFlybyDelay
            CycleDelay[Controller][Channel] = TxDelayOffset - MAX_ADD_DELAY;
          } else {                                            // Will use Dec_tCWL / Add_tCWL
            // Add one QCLK in Gear4 every 4UI's of cycle offset (when CurrentOffset is even)
            // AddOneQclk[Controller][Channel] = 3 - (ABS (TxDelayOffset) % 4);
            GlobalByteOff = 128 * (3 - (ABS (TxDelayOffset) % 4));  // Use Dq/Dqs PI instead of RdEn, to keep constant pointer separation
            if (TxDelayOffset > 0) {
              CycleDelay[Controller][Channel] = (TxDelayOffset + 3) / 4;
            } else {
              CycleDelay[Controller][Channel] = TxDelayOffset / 4;
            }
          }
        }
      } else if (Outputs->Gear2) {
        AddOneQclk[Controller][Channel] = 1;
        if (TxDelayOffset > (INT32) MaxAdd) {                 // Will use DQ/DQS PI to go right
          CycleDelay[Controller][Channel] = MAX_ADD_DELAY + MAX_ADD_RANK_DELAY;
          GlobalByteOff = 128 * (TxDelayOffset - MaxAdd);
        } else if (TxDelayOffset < -1 - 2 * (INT32) MaxDec) {     // Will use DQ/DQS PI to go left
          CycleDelay[Controller][Channel] = -1 * MaxDec;
          GlobalByteOff = 128 * (TxDelayOffset + 2 * MaxDec);
        } else {
          if (TxDelayOffset > 2 * MAX_ADD_DELAY) {            // Will use TxDqFifoRdEnFlybyDelay
            CycleDelay[Controller][Channel] = TxDelayOffset - MAX_ADD_DELAY;
          } else {                                            // Will use Dec_tCWL / Add_tCWL
            AddOneQclk[Controller][Channel] = ((TxDelayOffset % 2) == 0) ? 1 : 0;  // Add one QCLK in Gear2 every other cycle offset (when CurrentOffset is even)
            if (TxDelayOffset > 0) {
              CycleDelay[Controller][Channel] = (TxDelayOffset + 1) / 2;
            } else {
              CycleDelay[Controller][Channel] = TxDelayOffset / 2;
            }
          }
        }
      } else {  // Gear1
        if (TxDelayOffset > (INT32) MaxAdd) {
          CycleDelay[Controller][Channel] = MaxAdd;
          GlobalByteOff = 128 * (TxDelayOffset - MaxAdd);
        } else if (TxDelayOffset < -1 * (INT32) MaxDec) {
          CycleDelay[Controller][Channel] = -1 * MaxDec;
          GlobalByteOff = 128 * (TxDelayOffset + MaxDec);
        } else {
          CycleDelay[Controller][Channel] = TxDelayOffset;
        }
      }

      if (Inputs->LoopBackTest) {
        GlobalByteOff += (32 * UiLoop);
      }

      // Write Tx DQ/DQS Flyby delays
      // Note that we program these also in case GlobalByteOff is zero, because it might have been non-zero in the previous cycle.
      if (GlobalByteOff != 0) {
        // MRC_WRLVL_FLYBY_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nAdd GlobalByteOff = %d to TxDQS Flyby delay: Mc %d Ch %d \n", GlobalByteOff, Controller, Channel);
      }
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        GetSetVal = SavedTxDqsVal[Controller][Channel][Byte] + GlobalByteOff;
        MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, WriteToCache, &GetSetVal);
        GetSetVal += TxDqDqsDelayAdj;
        MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay, WriteToCache, &GetSetVal);
      }
      MrcFlushRegisterCachedData (MrcData);

      // Write SC_WR_DELAY
      FifoFlybyDelay = 0;
      ScWrDelay.Data = 0;
      if (CycleDelay[Controller][Channel] < 0) {
        ScWrDelay.Bits.Dec_tCWL = ABS (CycleDelay[Controller][Channel]);
        ScWrDelay.Bits.Add_tCWL = 0;
      } else {
        if (CycleDelay[Controller][Channel] > MAX_ADD_DELAY) {
          ScWrDelay.Bits.Add_tCWL = MAX_ADD_DELAY;
          FifoFlybyDelay = (CycleDelay[Controller][Channel] - MAX_ADD_DELAY) % MAX_ADD_DELAY; //Max FlyBy Delay
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n%s Add_tCWL is saturated , trying to use FifoFlybyDelay in Gear4\n");
        } else {
          ScWrDelay.Bits.Add_tCWL = CycleDelay[Controller][Channel];
        }
        ScWrDelay.Bits.Dec_tCWL = 0;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_WR_DELAY_REG, MC1_CH0_CR_SC_WR_DELAY_REG, Controller, MC0_CH1_CR_SC_WR_DELAY_REG, IpChannel);
      MrcWriteCR (MrcData, Offset, ScWrDelay.Data);

      MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, TxDqFifoRdEnFlybyDelay, WriteToCache, &FifoFlybyDelay);
      GetSetVal = 0;
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmIocTxDqFifoRdEnPerRankDelDis, WriteToCache, &GetSetVal);

      MrcGetTxDqFifoDelays (MrcData, Controller, Channel, tCWL, ScWrDelay.Bits.Add_tCWL, ScWrDelay.Bits.Dec_tCWL, &TxDqFifoWrEn, &TxDqFifoRdEn);
      TxDqFifoRdEn += AddOneQclk[Controller][Channel]; // TxDqFifoRdEnTcwlDelay(DCLK)

      if (TxDqFifoRdEn < 0) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n%s Not enough Tx FIFO separation! TxDqFifoWrEn: %d, TxFifoSeparation: %d\n", gErrString, (UINT32) TxDqFifoWrEn, MrcGetTxFifoSeparation (MrcData));
        return mrcTimingError;
      }
      MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoWrEnTcwlDelay, WriteToCache, &TxDqFifoWrEn);
      MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, WriteToCache, &TxDqFifoRdEn);
      MrcFlushRegisterCachedData (MrcData);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "GlobalByteOff: %3d, Dec: %d, Add: %d, 1Qclk: %d, WrEn: %d, RdEn: %d\n",
            GlobalByteOff, ScWrDelay.Bits.Dec_tCWL, ScWrDelay.Bits.Add_tCWL, AddOneQclk[Controller][Channel], (UINT32) TxDqFifoWrEn, (UINT32) TxDqFifoRdEn);
    } // Channel
  } // Controller

  return mrcSuccess;
}
/**
  Programming of PM_CONTROL_Dclk_en_reset_bgf_run
  BGF is disabled when Dclk_enable is cleared
  If PM_CONTROL_Dclk_en_reset_bgf_run bit is set to 0, BGF will continue to run even if dclk_enable is cleared

  @param[in] MrcData -    The MRC global data.
  @param[in] SetBgfRun  - FALSE to Keep BgfRun Enabled when EnableDclk is de-asserted.
                          TRUE  to program  PM_CONTROL_Dclk_en_reset_bgf_run = 1 (Negative Logic).

**/
VOID
MrcToggleBgfRun (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              SetBgfRun
)
{
  UINT8       Controller;
  UINT32      Offset;
  BOOLEAN     Value;
  MC0_PM_CONTROL_STRUCT                 PmControl;

  Value = (SetBgfRun) ? 1 : 0;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (!MrcControllerExist (MrcData, Controller)) {
      continue;
    }
    Offset = OFFSET_CALC_CH (MC0_PM_CONTROL_REG, MC1_PM_CONTROL_REG, Controller);
    PmControl.Data= MrcReadCR (MrcData, Offset);
    PmControl.Bits.Dclk_en_reset_bgf_run = Value;
    MrcWriteCR (MrcData, Offset, PmControl.Data);
  }
}
