/** @file
  This file contains functions to find and track DRAM Failing addresses for the
  Post Package Repair feature.

  @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

  @par Specification Reference:
**/

#include "MrcPpr.h"
#include "MrcCommon.h"
#include "MrcCpgcApi.h"
#include "Cpgc20.h"
#include "Cpgc20TestCtl.h"
#include "Cpgc20Patterns.h"
#include "MrcMemoryApi.h"
#include "MrcDdr5.h"

//
// Hooks to test error handling functionality
//
#define INJECT_MEMTEST_FAILURES_0001              0

/**
  Compare most significant channel address bits excluding row bits

  @param CurAddr          - First address to compare
  @param RowAddr          - Second address to compare

  @retval   TRUE for match, otherwise FALSE
**/
BOOLEAN
CurrentAddrMatch (
  ROW_ADDR CurAddr,
  ROW_ADDR RowAddr
  )
{
  // Mask fields that we don't care to match
  RowAddr.Bits.Row = 0;
  CurAddr.Bits.Row = 0;
  RowAddr.Bits.UpperBgMask = 0;
  CurAddr.Bits.UpperBgMask = 0;

  if (CurAddr.Data == RowAddr.Data) {
    return TRUE;
  } else {
    return FALSE;
  }
} //CurrentAddrMatch


/**
  Returns the CPGC cacheline mask to isolate the per bank group failure.
  A value of 1 means the correspond bank group is enabled for CPGC error check.

  0xFF means check error for all cachelines in other worlds all bank groups.

  In the 8 bank groups interleave case,
  0x01 means check error for the 1st bank group which was programmed in the
  CPGC logical2physical mapping register l2p_bank0_mapping.
  0x80 means check error for the 8th bank group which was programmed in the
  CPGC logical2physical mapping register l2p_bank7_mapping.

  Please be aware that when direction is MT_ADDR_DIR_DN, the order of the
  bank groups is reversed. 0x01 corresponds to the l2p_bank7_mapping while
  0x80 corresponds to the l2p_bank0_mapping

  @param[in] RetryState   - The retry index. Each retry target one bank group.
  @param[in] Direction    - Specifies the test direction.
  @param[in] BgInterleave - The number of interleave bank groups

  @retval cacheline bit mask
**/
UINT8
GetCachelineMask (
  IN UINT8      RetryState,
  IN UINT8      Direction,
  IN UINT8      BgInterleave
  )
{
  UINT8 Mask = 0xFF;

  // Mask generated according to the number of bank groups used in BG interleave

  if (BgInterleave == 2) {
    Mask = 0x55;
  } else if (BgInterleave == 8) {
    Mask = 0x01;
  }
  // If none of the above conditions are met, leave Mask as 0xFF

  if (Mask != 0xFF) {
    if ((RetryState > 0) && (RetryState <= BgInterleave)) {
      if (Direction == MT_ADDR_DIR_UP) {
        return (Mask << (RetryState - 1));
      }

      if (Direction == MT_ADDR_DIR_DOWN) {
        return ((Mask << (7)) >> (RetryState - 1));
      }
    }
  } // Mask != 0xFF

  // Default is to check all
  return Mask;
}

/**
  Gets the default Bank Group interleave value per technology

  @param[in]  MrcData                 - Global MRC data structure
  @param[out] BgInterleave            - Default bank group interleave value for the current DDR memory technology
**/
MrcStatus
GetDefaultBankGroupInterleave (
  IN  MrcParameters *const MrcData,
  OUT UINT8                *BgInterleave
  )
{
  MrcOutput* Outputs;

  Outputs = &MrcData->Outputs;
  if (Outputs->DdrType == MRC_DDR_TYPE_DDR4) {
    *BgInterleave = 2;
  } else if (Outputs->DdrType == MRC_DDR_TYPE_DDR5) {
    *BgInterleave = 8;
  } else {
    // LPDDR4/5- no BG interleave
    *BgInterleave = 1;
  }

  return mrcSuccess;
}

/**
  Shifts row fail list to right by number of entries starting at given index

  @param[in] MrcData        - Global MRC data structure
  @param[in] Controller     - Controller number to compare
  @param[in] Channel        - Channel number to compare
  @param[in] Index          - List index to begin the shift operation
  @param[in] NumEntries     - Number of entries to shift

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
ShiftFailRangeListRight (
  MrcParameters *const MrcData,
  UINT8                Controller,
  UINT8                Channel,
  UINT32               Index,
  UINT32               NumEntries
  )
{
  MrcOutput       *Outputs;
  INT32           CurEntry;
  UINT32          FailMax;
  MrcStatus       Status = mrcSuccess;

  Outputs = &MrcData->Outputs;
  FailMax = Outputs->FailMax[Controller][Channel];

  //For last entry in list down to index, move entry up by NumEntries amount
  if (FailMax + NumEntries < MAX_FAIL_RANGE) {
    if (FailMax != 0) {
      for (CurEntry = FailMax - 1; CurEntry >= (INT32) Index; CurEntry--) {
        Outputs->FailRange[Controller][Channel][CurEntry + NumEntries] = Outputs->FailRange[Controller][Channel][CurEntry];
      }
    }
    Outputs->FailMax[Controller][Channel] = FailMax + NumEntries;
  } else {
    Status = mrcFail;
  }
  return Status;
} //ShiftFailRangeListRight

/**
  Shifts row fail list left by number of entries starting at given index

  @param[in] MrcData        - Global MRC data structure
  @param[in] Controller     - Controller number to compare
  @param[in] Channel        - Channel number to compare
  @param[in] Index          - List index to begin the shift operation
  @param[in] NumEntries     - Number of entries to shift

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
ShiftFailRangeListLeft (
  MrcParameters *const MrcData,
  UINT8                Controller,
  UINT8                Channel,
  UINT32               Index,
  UINT32               NumEntries
  )
{
  MrcOutput       *Outputs;
  UINT32          CurEntry;
  UINT32          FailMax;
  MrcStatus       Status = mrcSuccess;

  Outputs = &MrcData->Outputs;
  FailMax = Outputs->FailMax[Controller][Channel];

  if (Index >= NumEntries) {
    //For index up to last entry in list, move entry down by NumEntries amount
    for (CurEntry = Index; CurEntry < FailMax; CurEntry++) {
      Outputs->FailRange[Controller][Channel][CurEntry - NumEntries] = Outputs->FailRange[Controller][Channel][CurEntry];
    }
    Outputs->FailMax[Controller][Channel] = FailMax - NumEntries;
  } else {
    Status = mrcFail;
  }
  return Status;
} // ShiftFailRangeListLeft

/**
  Combine two adjacent entries if they have the same failing signiture

  @param[in] MrcData        - Global MRC data structure
  @param[in] Controller     - Controller number to compare
  @param[in] Channel        - Channel number to compare
  @param[in] Index          - List index of the entry be combine with its next index entry

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
CombineFailRangeList (
  MrcParameters *const MrcData,
  UINT8                Controller,
  UINT8                Channel,
  UINT32               Index
  )
{
  MrcOutput       *Outputs;
  ROW_FAIL_RANGE *FailRangePtr1;
  ROW_FAIL_RANGE *FailRangePtr2;

  Outputs = &MrcData->Outputs;

  if (Index + 1 >= MAX_FAIL_RANGE) {
    return mrcFail;
  }

  FailRangePtr1 = &Outputs->FailRange[Controller][Channel][Index];
  FailRangePtr2 = &Outputs->FailRange[Controller][Channel][Index + 1];

  // Combine list entries if possible
  if (CurrentAddrMatch (FailRangePtr1->Addr, FailRangePtr2->Addr) &&
    (FailRangePtr2->Addr.Bits.Row == FailRangePtr1->Addr.Bits.Row + FailRangePtr1->Size) &&
    (FailRangePtr2->BankGroupMask == FailRangePtr1->BankGroupMask)) {

    FailRangePtr2->Addr.Bits.Row = FailRangePtr1->Addr.Bits.Row;
    FailRangePtr2->Size += FailRangePtr1->Size;

    ShiftFailRangeListLeft (MrcData, Controller, Channel, Index + 1, 1);
    Outputs->FailIndex[Controller][Channel] = Index;
  } else {
    return mrcFail;
  }
  return mrcSuccess;
} // CombineFailRangeList

/**
  Check row failure list and PPR resource list to determine if repairs are required

  @param[in] MrcData              - Global MRC data structure
  @param[in] McChBitMask          - Controller number to compare
  @param[in] DimmRank             - DIMM containing logicalRank
  @param[in] Logical2Physical     - Geometery for logical rank within channel
  @param[in] BaseBits             - Number of bank bits in SW loop

  @retval status - TRUE/FALSE
**/
BOOLEAN
IsPprRepairRequired (
  MrcParameters  *const   MrcData,
  UINT32                  McChBitMask,
  UINT8                   DimmRank[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL],
  UINT8                   Logical2Physical[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL],
  UINT8                   BaseBits
  )
{
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcDimmOut          *DimmOut;
  ROW_FAIL_RANGE      *FailRangePtr;
  UINT32              FailIndex;
  UINT8               Channel;
  UINT8               Controller;
  UINT8               LogicalRank;
  UINT8               Rank;
  UINT8               Bank;
  UINT8               BankGroup;
  UINT8               Device;
  UINT32              DeviceDqMask;
  UINT8               NumDevices;
  UINT8               MaxChDdr;
  UINT8               LowerByte[MAX_BYTE_IN_DDR5_CHANNEL];

  Outputs = &MrcData->Outputs;
  MaxChDdr = Outputs->MaxChannels;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChDdr; Channel++) {
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      // Loop through all row failure ranges
      for (FailIndex = 0; FailIndex < Outputs->FailMax[Controller][Channel]; FailIndex++) {
        FailRangePtr = &Outputs->FailRange[Controller][Channel][FailIndex];

        LogicalRank = (UINT8) FailRangePtr->Addr.Bits.Rank;
        Rank = Logical2Physical[LogicalRank][Controller][Channel];
        Bank = (UINT8) FailRangePtr->Addr.Bits.BankPair;

        DimmOut = &ChannelOut->Dimm[RANK_TO_DIMM_NUMBER (Rank)];
        NumDevices = DimmOut->PrimaryBusWidth / DimmOut->SdramWidth;
        NumDevices += (Outputs->EccSupport) ? 1 : 0;
        NumDevices = MIN (NumDevices, MAX_BYTE_IN_DDR5_CHANNEL); // Ensure Device loop doesn't exceed max array size

        for (BankGroup = 0; BankGroup < 8; BankGroup++) {
          if (FailRangePtr->BankGroupMask & (1 << BankGroup)) {
            //            PprBank = (BankGroup << BaseBits) + Bank;

            // Construct DRAM Mask
            DeviceDqMask = 0xFF;

            GetDdr5LowerUpperBytes (MrcData, Controller, Channel, Rank, LowerByte, NULL);

            for (Device = 0; Device < NumDevices; Device++) {
              if ((FailRangePtr->Mask[LowerByte[Device] / 4] >> ((LowerByte[Device] % 4) * 8)) & DeviceDqMask) {
                if (GetPprResourceAvailable (MrcData, Controller, Channel, Rank, LowerByte[Device], BankGroup, Bank)) {
                  return 1;
                } // GetPprResourceAvailable
              } // FailRangePtr->mask
            } // for Device
          } // if FailRangePtr->BankGroupMask
        } // for BankGroup
      } // for FailIndex
    } // for Channel
  } // for Controller

  return 0;
} // IsPprRepairRequired

/**
  Updates Row failure list with last failure, coalescing failure ranges where possible

  @param[in] MrcData        - Global MRC data structure
  @param[in] Controller     - Controller number to update
  @param[in] Channel        - Channel number to update
  @param[in] CheckMaskError - Check for previously masked range
  @param[in] NewFail        - New failure information

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
UpdateRowFailList (
  MrcParameters *const MrcData,
  UINT8                Controller,
  UINT8                Channel,
  BOOLEAN              CheckMaskError,
  ROW_FAIL_RANGE       NewFail
  )
{
  MrcOutput       *Outputs;
  UINT32 index;
  MrcStatus Status = mrcSuccess;
  ROW_FAIL_RANGE *FailRangePtr;
  ROW_FAIL_RANGE *FailRangePtrNext1;
  ROW_FAIL_RANGE *FailRangePtrNext2;
  ROW_ADDR SearchFail;
  UINT32 SearchSize;
  UINT32 SearchMask[3];
  UINT8  SearchBankGroupMask;
  UINT8 done;

  // Search FailRange list for position to insert new faiure data
  Outputs = &MrcData->Outputs;
  index = 0;
  done = 0;
  while (!done && (index < Outputs->FailMax[Controller][Channel]) && (Status == mrcSuccess)) {

    FailRangePtr = &Outputs->FailRange[Controller][Channel][index];
    FailRangePtrNext1 = &Outputs->FailRange[Controller][Channel][index + 1];
    FailRangePtrNext2 = &Outputs->FailRange[Controller][Channel][index + 2];

    SearchFail = FailRangePtr->Addr;
    SearchSize = FailRangePtr->Size;

    // Check search criteria to move to next entry
    // 1. Non-matching bank, subrank, rank
    // 2. New failure is greater than current entry row range
    // 3. New failure is equal to next entry row
    if (!CurrentAddrMatch (SearchFail, NewFail.Addr) || (SearchFail.Bits.Row + SearchSize < NewFail.Addr.Bits.Row) ||
      ((index + 1 < Outputs->FailMax[Controller][Channel]) && (CurrentAddrMatch (FailRangePtrNext1->Addr, NewFail.Addr)) && (FailRangePtrNext1->Addr.Bits.Row == NewFail.Addr.Bits.Row))) {
      index++;
      continue;
    }

    SearchMask[0] = FailRangePtr->Mask[0];
    SearchMask[1] = FailRangePtr->Mask[1];
    SearchMask[2] = FailRangePtr->Mask[2];
    SearchBankGroupMask = FailRangePtr->BankGroupMask;

    // Append new failure to end of current entry
    // Row address + 1
    // Same DQ mask
    if ((NewFail.Addr.Bits.Row >= SearchFail.Bits.Row) &&
      (NewFail.Addr.Bits.Row < SearchFail.Bits.Row + SearchSize) &&
      ((SearchMask[0] | NewFail.Mask[0]) == SearchMask[0]) &&
      ((SearchMask[1] | NewFail.Mask[1]) == SearchMask[1]) &&
      ((SearchMask[2] | NewFail.Mask[2]) == SearchMask[2])) {

      if (CheckMaskError) {
        // Something went wrong if we got failure on test address that was previously masked
        MRC_DEBUG_ASSERT (FALSE, &Outputs->Debug, "UpdateRowFailures: New failure occurred in previously masked range:  fail addr = 0x%08x, failing bank group = 0x%x error status = 0x%02x%08x%08x, search addr = 0x%08x, size = 0x%08x, mask = 0x%02x%08x%08x\n",
        NewFail.Addr.Data, NewFail.BankGroupMask, NewFail.Mask[2], NewFail.Mask[1], NewFail.Mask[0], SearchFail.Data, SearchSize, SearchMask[2], SearchMask[1], SearchMask[0]);
        return mrcFail;
      } else {
        Outputs->FailIndex[Controller][Channel] = index;

        // Combine list entries (index, index + 1), then (index-1, index) if possible
        CombineFailRangeList (MrcData, Controller, Channel, index);
        if (index > 0) {
          CombineFailRangeList (MrcData, Controller, Channel, index - 1);
        }
        done = 1;
      }

    } else if ((NewFail.Addr.Bits.Row == SearchFail.Bits.Row + SearchSize) && // Add 1 to current entry size
      (NewFail.Mask[0] == SearchMask[0]) &&
      (NewFail.Mask[1] == SearchMask[1]) &&
      (NewFail.Mask[2] == SearchMask[2])) {

      FailRangePtr->Size++;
      FailRangePtr->BankGroupMask |= NewFail.BankGroupMask;
      Outputs->FailIndex[Controller][Channel] = index;
      done = 1;

      // Combine list entries (index, index + 1) if possible
      CombineFailRangeList (MrcData, Controller, Channel, index);
    } else if ((NewFail.Addr.Bits.Row > SearchFail.Bits.Row) && // Split existing entry, so shift by 2 at next entry
      (NewFail.Addr.Bits.Row < SearchFail.Bits.Row + SearchSize - 1)) {

      // Shift list right by 2 at next entry
      if (ShiftFailRangeListRight (MrcData, Controller, Channel, index + 1, 2) == mrcSuccess) {
        // Truncate current entry size;
        FailRangePtr->Size = NewFail.Addr.Bits.Row - SearchFail.Bits.Row;

        // Add new entry + 1 to report new failure added to previous mask;
        FailRangePtrNext1->Size = 1;
        FailRangePtrNext1->Addr = NewFail.Addr;
        FailRangePtrNext1->BankGroupMask = NewFail.BankGroupMask;
        FailRangePtrNext1->Mask[0] = SearchMask[0] | NewFail.Mask[0];
        FailRangePtrNext1->Mask[1] = SearchMask[1] | NewFail.Mask[1];
        FailRangePtrNext1->Mask[2] = SearchMask[2] | NewFail.Mask[2];
        Outputs->FailIndex[Controller][Channel] = index + 1;

        // Add new entry + 2 to report previous extra size
        FailRangePtrNext2->Size = SearchSize - 1 - (NewFail.Addr.Bits.Row - SearchFail.Bits.Row);
        FailRangePtrNext2->Addr = NewFail.Addr;
        FailRangePtrNext2->Addr.Bits.Row = NewFail.Addr.Bits.Row + 1;
        FailRangePtrNext2->BankGroupMask = SearchBankGroupMask;
        FailRangePtrNext2->Mask[0] = SearchMask[0];
        FailRangePtrNext2->Mask[1] = SearchMask[1];
        FailRangePtrNext2->Mask[2] = SearchMask[2];
        done = 1;
      } else {
        Status = mrcFail;
      }
    }
    // Add new entry overlapping end of current entry
    // Same Row address as end of range (or range size = 1)
    // New DQ mask
    else if (NewFail.Addr.Bits.Row == SearchFail.Bits.Row + SearchSize - 1) {

      if (SearchSize > 1) {
        // Shift list right by 1 at next entry
        if (ShiftFailRangeListRight (MrcData, Controller, Channel, index + 1, 1) == mrcSuccess) {

          FailRangePtr->Size--;

          FailRangePtrNext1->Size = 1;
          FailRangePtrNext1->Addr = NewFail.Addr;
          FailRangePtrNext1->BankGroupMask = NewFail.BankGroupMask;
          FailRangePtrNext1->Mask[0] = SearchMask[0] | NewFail.Mask[0];
          FailRangePtrNext1->Mask[1] = SearchMask[1] | NewFail.Mask[1];
          FailRangePtrNext1->Mask[2] = SearchMask[2] | NewFail.Mask[2];
          Outputs->FailIndex[Controller][Channel] = index + 1;
          done = 1;

          // Combine list entries (index + 1, index + 2) if possible
          CombineFailRangeList (MrcData, Controller, Channel, index + 1);
        } else {
          Status = mrcFail;
        }
      } else {
        // Replace current entry
        FailRangePtr->BankGroupMask = SearchBankGroupMask | NewFail.BankGroupMask;
        FailRangePtr->Mask[0] = SearchMask[0] | NewFail.Mask[0];
        FailRangePtr->Mask[1] = SearchMask[1] | NewFail.Mask[1];
        FailRangePtr->Mask[2] = SearchMask[2] | NewFail.Mask[2];
        Outputs->FailIndex[Controller][Channel] = index;
        done = 1;
        // Combine list entries (index, index + 1), then (index-1, index) if possible
        CombineFailRangeList (MrcData, Controller, Channel, index);
        if (index) {
          CombineFailRangeList (MrcData, Controller, Channel, index - 1);
        }
      }
    }
    // Update start of current entry to include fail row
    // Row address - 1
    // Same DQ mask
    else if ((NewFail.Addr.Bits.Row == SearchFail.Bits.Row - 1) &&
      (NewFail.Mask[0] == SearchMask[0]) &&
      (NewFail.Mask[1] == SearchMask[1]) &&
      (NewFail.Mask[2] == SearchMask[2])) {

      FailRangePtr->Size++;
      FailRangePtr->Addr = NewFail.Addr;
      FailRangePtr->BankGroupMask = SearchBankGroupMask | NewFail.BankGroupMask;
      Outputs->FailIndex[Controller][Channel] = index;
      done = 1;
      // Combine list entries (index-1, index) if possible
      if (index) {
        CombineFailRangeList (MrcData, Controller, Channel, index - 1);
      }
    }
    // Add new entry overlapping the start of current entry
    // Same Row address as start of range
    // New DQ mask
    else if (NewFail.Addr.Bits.Row == SearchFail.Bits.Row) {
      if (SearchSize > 1) {
        // Shift list right by 1 at current entry
        if (ShiftFailRangeListRight (MrcData, Controller, Channel, index, 1) == mrcSuccess) {

          FailRangePtr->Size = 1;
          FailRangePtr->Addr = NewFail.Addr;
          FailRangePtr->BankGroupMask = NewFail.BankGroupMask;
          FailRangePtr->Mask[0] = SearchMask[0] | NewFail.Mask[0];
          FailRangePtr->Mask[1] = SearchMask[1] | NewFail.Mask[1];
          FailRangePtr->Mask[2] = SearchMask[2] | NewFail.Mask[2];
          Outputs->FailIndex[Controller][Channel] = index;

          FailRangePtrNext1->Size--;
          FailRangePtrNext1->Addr.Bits.Row++;
          done = 1;

          // Combine list entries (index-1, index) if possible
          CombineFailRangeList (MrcData, Controller, Channel, index - 1);
        } else {
          Status = mrcFail;
        }
      }
    }
    // Add new entry for higher row address with no overlap
    else if (NewFail.Addr.Bits.Row >= SearchFail.Bits.Row + SearchSize) {
      // Shift list right by 1 at next entry
      if (ShiftFailRangeListRight (MrcData, Controller, Channel, index + 1, 1) == mrcSuccess) {
        FailRangePtrNext1->Size = 1;
        FailRangePtrNext1->Addr = NewFail.Addr;
        FailRangePtrNext1->BankGroupMask = NewFail.BankGroupMask;
        FailRangePtrNext1->Mask[0] = NewFail.Mask[0];
        FailRangePtrNext1->Mask[1] = NewFail.Mask[1];
        FailRangePtrNext1->Mask[2] = NewFail.Mask[2];
        Outputs->FailIndex[Controller][Channel] = index + 1;
        done = 1;
      } else {
        Status = mrcFail;
      }
    }
    // Add new entry for lower row address with no overlap
    else {
      // Shift list right by 1 at current entry
      if (ShiftFailRangeListRight (MrcData, Controller, Channel, index, 1) == mrcSuccess) {
        FailRangePtr->Size = 1;
        FailRangePtr->Addr = NewFail.Addr;
        FailRangePtr->BankGroupMask = NewFail.BankGroupMask;
        FailRangePtr->Mask[0] = NewFail.Mask[0];
        FailRangePtr->Mask[1] = NewFail.Mask[1];
        FailRangePtr->Mask[2] = NewFail.Mask[2];
        Outputs->FailIndex[Controller][Channel] = index;
        done = 1;
      } else {
        Status = mrcFail;
      }
    }
  } //while

  // Append entry to end of list
  // Shift list right by 1 at end of list
  if (!done) {
    if (ShiftFailRangeListRight (MrcData, Controller, Channel, index, 1) == mrcSuccess) {
      FailRangePtr = &Outputs->FailRange[Controller][Channel][index];
      FailRangePtr->Size = 1;
      FailRangePtr->Addr = NewFail.Addr;
      FailRangePtr->BankGroupMask = NewFail.BankGroupMask;
      FailRangePtr->Mask[0] = NewFail.Mask[0];
      FailRangePtr->Mask[1] = NewFail.Mask[1];
      FailRangePtr->Mask[2] = NewFail.Mask[2];
      Outputs->FailIndex[Controller][Channel] = index;
    } else {
      Status = mrcFail;
    }
  } // if !done

  return Status;
}  //UpdateRowFailList

/**
  Updates Row failure list with last failure, coalescing failure ranges where possible

  @param[in]  MrcData                 - Global MRC data structure
  @param[in]  McChBitMask             - Memory Controller Channel Bit mask to update
  @param[in]  cpgcErrorStatus         - the failure information per channel
  @param[in]  DimmTemp                - Dimm Temperature on initial failure
  @param[in]  rowBits                 - Number of row bits supported by current logical rank
  @param[in]  baseBits                - Number of bank bits in SW loop
  @param[in]  BgInterleave            - Number of bank groups in HW loop.
  @param[out] TestStatus              - Pass/fail status for the test per channel
  @param[in]  Direction               - Sequential address direction MT_ADDR_DIR_UP, MT_ADDR_DIR_DN
  @param[in]  AmtRetryDisabled        - Indicates when the AMT Retry flag is disabled

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
UpdateRowFailures (
  IN  MrcParameters *const    MrcData,
  IN  UINT32                  McChBitMask,
  IN  CPGC_ERROR_STATUS_MATS  cpgcErrorStatus[MAX_CONTROLLER][MAX_CHANNEL],
  IN  INT16                   DimmTemp[MAX_CONTROLLER][MAX_CHANNEL],
  IN  UINT8                   rowBits[MAX_CONTROLLER][MAX_CHANNEL],
  IN  UINT32                  baseBits,
  OUT UINT8                   TestStatus[MAX_CONTROLLER][MAX_CHANNEL],
  IN  UINT8                   Direction,
  IN  BOOLEAN                 AmtRetryDisabled
  )
{
  MrcOutput       *Outputs;
  UINT32 errorStatusLo;
  UINT32 errorStatusHi;
  UINT32 errorStatusEcc;
  UINT8 Channel;
  UINT8 Controller;
  MrcStatus Status = mrcSuccess;
  ROW_FAIL_RANGE NewFail;
  ROW_FAIL_RANGE LastFail;
  ROW_FAIL_RANGE TempFail;
  UINT8 RetryState;
  UINT32 DqMaskOver[3];
  UINT32 Index;
  UINT32 Row;
  UINT32 OrigRow;
  UINT32 SubChErrBank;
  UINT8 BgInterleave;

  errorStatusEcc = 0;

  Outputs = &MrcData->Outputs;
  RetryState = Outputs->RetryState;
  NewFail.Addr.Data = 0;     // Initialize to Invalid state
  NewFail.BankGroupMask = 0;

  if (GetDefaultBankGroupInterleave(MrcData, &BgInterleave) != mrcSuccess) {
    BgInterleave = 8;
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if ((MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, Outputs->MaxChannels) == 0)) {
        continue;
      }

      errorStatusLo = cpgcErrorStatus[Controller][Channel].cpgcErrDat0S | cpgcErrorStatus[Controller][Channel].cpgcErrDat2S;
      errorStatusHi = cpgcErrorStatus[Controller][Channel].cpgcErrDat1S | cpgcErrorStatus[Controller][Channel].cpgcErrDat3S;

      if (Outputs->EccSupport) {
        errorStatusEcc = cpgcErrorStatus[Controller][Channel].cpgcErrEccS & MRC_UINT8_MAX;  // (ErrorStatusEccSubChB << BYTE_WIDTH) | ErrorStatusEccSubChA;
      } else {
        errorStatusEcc = 0;
      }

      TestStatus[Controller][Channel] = 0;
      if ((DimmTemp[Controller][Channel] / 2 <= AMT_DIMM_TEMP_LIMIT ) &&
        !(errorStatusLo | errorStatusHi | errorStatusEcc)) {
        // Use BG_INTER_8 as an example:
        // retry 0, no fail - set last fail to default, test status = 0
        // retry 1-7, no fail - no last fail update, test status = 0
        // retry 8, no fail - no last fail update, test status = 1
        if (RetryState == 0) {
          NewFail.Addr.Data = 0;     // Set to Invalid state
          NewFail.BankGroupMask = 0;
          NewFail.Mask[0] = 0;
          NewFail.Mask[1] = 0;
          NewFail.Mask[2] = 0;
          NewFail.Size = 0;
          Outputs->LastFail[Controller][Channel] = NewFail;
        } else if (RetryState == BgInterleave) { // BgInterleave = 8, maximum retry state?
          NewFail = Outputs->LastFail[Controller][Channel];
          TestStatus[Controller][Channel] = 1;
        }
        //MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "no error found, RetryState = %d\n", RetryState);
      } else {
        MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Mc%d.C%d: RetryState=%d, errorStatusLo=0x%x, errorStatusHi=0x%x\n", Controller, Channel, RetryState, errorStatusLo, errorStatusHi);

        // New fail address
        NewFail.Addr.Data = 0;
        NewFail.BankGroupMask = 0;

        SubChErrBank = cpgcErrorStatus[Controller][Channel].cpgcErrBank & MRC_UINT16_MAX;
        NewFail.Addr.Bits.Row = cpgcErrorStatus[Controller][Channel].cpgcErrRow;

        NewFail.Addr.Bits.BankPair = SubChErrBank & ((1 << baseBits) - 1);

        NewFail.Addr.Bits.Rank  = cpgcErrorStatus[Controller][Channel].cpgcErrRank;
        NewFail.Addr.Bits.Valid = 1;
        NewFail.Mask[0] = 0;
        NewFail.Mask[1] = 0;
        NewFail.Mask[2] = 0;
        NewFail.Size = 1;

        // Use BG_INTER_8 as an example:
        // retry 0, fail - save last fail, BankGroupMask 0, test status = 1
        // retry 1, fail - update last fail with BankGroupMask bit-0=1, test status = 1
        // retry 2, fail - update last fail with BankGroupMask bit-1=1, test status = 1
        // retry ...
        // retry 8, fail - update last fail with BankGroupMask bit-7=1, test status = 1
        if (RetryState) {
          LastFail = Outputs->LastFail[Controller][Channel];
        } else {
          LastFail = NewFail;
        }
        if (RetryState || AmtRetryDisabled) {
          TempFail = Outputs->LastFail[Controller][Channel];

          // Mask values eventually propagate into DeviceMask; index [0] is low 32 bits, [1] is high 32 bits, [2] is ECC bits
          if (TempFail.Addr.Data == NewFail.Addr.Data) {
            NewFail.Mask[0] = LastFail.Mask[0] | errorStatusLo;
            NewFail.Mask[1] = LastFail.Mask[1] | errorStatusHi;
            NewFail.Mask[2] = LastFail.Mask[2] | errorStatusEcc;
          }
          NewFail.BankGroupMask = LastFail.BankGroupMask | (1 << (SubChErrBank >> baseBits));
        }
        Outputs->LastFail[Controller][Channel] = NewFail;
        TestStatus[Controller][Channel] = 1;
        //MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "    RetryState = %d, SubChErrBank = %d, baseBits = %d\n", RetryState, SubChErrBank, baseBits, NewFail.BankGroupMask);
      }
      // Only update list state after all retries are done
      if ((((RetryState == BgInterleave) && NewFail.BankGroupMask) || AmtRetryDisabled) && TestStatus[Controller][Channel]) {
        if (UpdateRowFailList (MrcData, Controller, Channel, !AmtRetryDisabled, NewFail) != mrcSuccess) {
          Status = mrcFail;
        } else {
          // Call function passing in NewFail and max row threshold; output is a dq mask over threshold.
          // Function will search the current bank and create a DQ mask of the new DQ failures that occur on more than N number of rows in the bank.
          if (GetDqMaskOverThreshold (MrcData, Controller, Channel, NewFail, 8, DqMaskOver)) {
            // Update list to apply DQ mask to whole bank
            OrigRow = NewFail.Addr.Bits.Row;
            for (Index = 0; Index < 3; Index++) {
              NewFail.Mask[Index] = DqMaskOver[Index];
            }
            for (Row = 0; Row < (UINT32)(1 << rowBits[Controller][Channel]); Row++) {
              NewFail.Addr.Bits.Row = Row;
              if (UpdateRowFailList (MrcData, Controller, Channel, FALSE, NewFail) != mrcSuccess) {
                Status = mrcFail;
                break;
              }
            }
            // Final update to restore RowFailIndex
            NewFail.Addr.Bits.Row = OrigRow;
            if (UpdateRowFailList (MrcData, Controller, Channel, FALSE, NewFail) != mrcSuccess) {
              Status = mrcFail;
            }
          } // GetDqMaskOverThreshold
        } // else UpdateRowFailList
      } // RetryState
      if (Status == mrcFail) {
        MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "UpdateRowFailures: FailRange list is Full!\n");
        return Status;
      }
    } // for Channel
  } // for Controller
  return Status;
}  //UpdateRowFailures

/**
  Searches the failure tracking list for NewFail for each bank address on each single DQ/Device and returns a bit mask of the DQ bits with
  number of row failures greater or equal to the given threshold.


  @param[in] MrcData       - Pointer to global MRC data.
  @param[in] Controller    - 0-based index to controller
  @param[in] Channel       - 0-based index to channel
  @param[in] NewFail       - New failure information
  @param[in] Threshold     - Number of errors per DQ where mask bit is set
  @param[in] DqMaskOver    - Bitmask of DQ having number of row errors in bank greater or equal to threshold

  @retval status - TRUE if any DQ has number of row errors greater or equal to threshold
**/
BOOLEAN
GetDqMaskOverThreshold (
  MrcParameters  *const     MrcData,
  UINT8                     Controller,
  UINT8                     Channel,
  ROW_FAIL_RANGE            NewFail,
  UINT32                    Threshold,
  UINT32                    DqMaskOver[3]
  )
{
  MrcOutput       *Outputs;
  UINT32          PerLaneCounters[MAX_BITS * MAX_SDRAM_IN_DIMM];
  ROW_FAIL_RANGE  *FailRangePtr;
  ROW_ADDR        SearchFail;
  UINT32          SearchSize;
  UINT32          Index;
  UINT32          LaneIndex;

  Outputs = &MrcData->Outputs;

  for (Index = 0; Index < (MAX_BITS * MAX_SDRAM_IN_DIMM); Index++) {
    PerLaneCounters[Index] = 0;
  }
  // Search FailRange list for position to insert new faiure data
  for (Index = 0; Index < Outputs->FailMax[Controller][Channel]; Index++) {
    FailRangePtr = &Outputs->FailRange[Controller][Channel][Index];
    SearchFail = FailRangePtr->Addr;
    SearchSize = FailRangePtr->Size;

    // Check search criteria to move to next entry
    // 1. Non-matching bank, subrank, rank, or
    if (CurrentAddrMatch (SearchFail, NewFail.Addr)) {
      // Update counters
      for (LaneIndex = 0; LaneIndex < (MAX_BITS * MAX_SDRAM_IN_DIMM); LaneIndex++) { // 0-71
        if (FailRangePtr->Mask[LaneIndex / 32] & (1 << (LaneIndex % 32))) {
          PerLaneCounters[LaneIndex] += SearchSize;
        }
      }
    }
  }
  // Form new DQ mask
  for (Index = 0; Index < MAX_DQ_MASK_FAIL_RANGE; Index++) {
    DqMaskOver[Index] = NewFail.Mask[Index];
  }
  for (Index = 0; Index < (MAX_BITS * MAX_SDRAM_IN_DIMM); Index++) {
    if (PerLaneCounters[Index] < 8) {
      DqMaskOver[Index / 32] &= ~(1 << (Index % 32));
    }
  }

  // Return Status
  for (Index = 0; Index < MAX_DQ_MASK_FAIL_RANGE; Index++) {
    if (DqMaskOver[Index] != 0) {
      return TRUE;
    }
  }
  return FALSE;
}


/**
  Loop through all controllers and channels to find and print the total row failures per controller, channel

  @param[in]  MrcData      - Pointer to global MRC data.

**/
#ifdef MRC_DEBUG_PRINT
VOID
PrintTotalRowFailures (
  MrcParameters *const    MrcData
  )
{
  MrcOutput           *Outputs;
  UINT8               Channel;
  UINT8               MaxChDdr;
  UINT8               McChBitMask;
  UINT8               Controller;
  UINT8               RowFailureFound = 0;
  UINT32              TotalRowFailures = 0;
  UINT32              RowFailSize;
  UINT32              Index;
  Outputs = &MrcData->Outputs;
  MaxChDdr = Outputs->MaxChannels;
  McChBitMask = Outputs->McChBitMask;

  MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "Summary of row failures:\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChDdr; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChDdr) == 0 || Outputs->FailMax[Controller][Channel] == 0) {
        continue;
      }
      RowFailureFound = 1;
      TotalRowFailures = 0;
      for (Index = 0; Index < MAX_FAIL_RANGE; Index ++) {
        RowFailSize = Outputs->FailRange[Controller][Channel][Index].Size;
        if (RowFailSize <= 8) {
          TotalRowFailures += RowFailSize;
        }
      }
      MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "  Mc%d.C%d: has %d row errors\n", Controller, Channel, TotalRowFailures);
    }
  }
  if (!RowFailureFound) {
    MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "  No row errors found!\n");
  }
}
#endif // MRC_DEBUG_PRINT

/**
  Executes a step of Advanced Memory test using CPGC engine and logs results

  @param [in]     MrcData          - Pointer to global MRC data.
  @param [in]     McChBitMask      - Memory Controller Channel Bit mask to update
  @param [in,out] *RepairDone      - Flag to indicate the repair was done

  @retval status - MRC_STATUS_SUCCESS/MRC_STATUS_FAILURE
**/
MrcStatus
CpgcMemTestDispositionFailRange (
  IN     MrcParameters *const    MrcData,
  IN     UINT32                  McChBitMask,
  IN OUT UINT8                   *RepairDone
  )
{
  MRC_FUNCTION        *MrcCall;
  MrcOutput           *Outputs;
  UINT32              RankChEnMap = 0;
  UINT32              FailIndex;
  ROW_ADDR            CurAddr;
  UINT8               Channel;
  UINT8               Controller;
  UINT8               Bank;
  UINT8               ColumnBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               RowBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               BankBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               RankEnabled[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               DimmRank[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               SubRankCnt[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               Logical2Physical[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               BaseBits;
  UINT8               RowFailuresFound;
  UINT8               Index;
  MrcStatus           RetVal = mrcSuccess;
  BOOLEAN             PprRequired;
  UINT8               MaxChDdr;
  ROW_FAIL_RANGE      *FailRangePtr;
  UINT8               Rank = 0;
  UINT8               BankGroup;
  UINT8               Byte;
  UINT16              ByteMask;
  UINT16              FailRow;
  MrcStatus           Status;
  //PPR_ADDR_MRC_SETUP  NewPprEntry;
  //PPR_ADDR_MRC_SETUP  PprAddrList[MAX_PPR_ADDR_ENTRIES_DDR];
  //UINT8               PprStatus[MAX_PPR_ADDR_ENTRIES_DDR_SPPR];
  BOOLEAN             RepairStatus = FALSE;
  UINT32              BgDqMask[8][3];
  UINT32              BgDqMaskCh[8][MAX_CHANNEL];
  //UINT32              MaxPprValidEntry = 0;
  UINT32              MaxPprAddrListSize;
  //PPR_ADDR_MRC_SETUP  *PprAddrListPtr = NULL;
  //SYS_SETUP           *Setup;
  //UINT8               BuddyByte;
  //UINT8               SubChMSVx4 = GetSubChMaxStrobeValid ();
  ROW_FAIL_RANGE      TempFail;
  UINT32              DqMaskTemp[3];
  UINT32              DqMaskCh[MAX_CHANNEL];
  BOOLEAN             ListUpdate;
  UINT32              DqMaskOver[3];

  MrcCall = MrcData->Inputs.Call.Func;
  Outputs = &MrcData->Outputs;
  MaxChDdr = Outputs->MaxChannels;

  MrcCall->MrcSetMem ((UINT8 *) ColumnBits, sizeof (ColumnBits), 0);
  MrcCall->MrcSetMem ((UINT8 *) RowBits, sizeof (RowBits), 0);
  MrcCall->MrcSetMem ((UINT8 *) BankBits, sizeof (BankBits), 0);
  MrcCall->MrcSetMem ((UINT8 *) RankEnabled, sizeof (RankEnabled), 0);
  MrcCall->MrcSetMem ((UINT8 *) DimmRank, sizeof (DimmRank), 0);
  MrcCall->MrcSetMem ((UINT8 *) SubRankCnt, sizeof (SubRankCnt), 0);
  MrcCall->MrcSetMem ((UINT8 *) Logical2Physical, sizeof (Logical2Physical), 0);
  *RepairDone = 0;

  // Inject error in row fail list for testing purposes
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChDdr; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChDdr) == 0) {
        continue;
      }
      //InjectErrorRowFailList (Host, Socket, Channel);
    }
  }

  //
  // Are there any failures in the list?
  //
  RowFailuresFound = 0;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChDdr; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChDdr) == 0) {
        continue;
      }
      if (Outputs->FailMax[Controller][Channel] > 0) {
        RowFailuresFound = 1;
        break;
      }
    }
  }

  if (RowFailuresFound) {
    // Find geometery for logical rank within channel.
    //SetupIOTestPPR (MrcData, Controller, Channel, Row, BankGroup, BankAddress, ByteMask);
    /*     MemRankGeometry (Socket, ColumnBits, RowBits, BankBits, RankEnabled, DimmRank,
    SubRankCnt, &MaxEnabledRank, &MaxEnabledSubRank, Logical2Physical, TestType);*/

    BaseBits = (UINT8) GetBaseBits (MrcData, McChBitMask, Rank);

    //
    // Set test condition for PPR flow
    //
    PprRequired = IsPprRepairRequired (MrcData, McChBitMask, DimmRank, Logical2Physical, BaseBits);

    // Loop over all ranks and subranks
    for (Rank = 0; Rank <= MAX_RANK_IN_CHANNEL; Rank++) {
      // Skip to next rank if no ranks enabled on any channel
      if (RankChEnMap == 0) {
        continue;
      }
      // Search row fail list and apply PPR repairs as needed
      if (PprRequired) {
        MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "DispositionFailRangesWithPprFlow Starts:  McChBitMask=0x%x\n", McChBitMask);
        // Inspect test results for given logicalRank, logicalSubRank, bank
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChDdr; Channel++) {
            // Was this channel part of the test group?
            if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChDdr) == 0) {
              continue;
            }
            // For each bank pair
            for (Bank = 0; Bank < (1 << BaseBits); Bank++) {
              CurAddr.Data = 0;
              CurAddr.Bits.BankPair = Bank;
              CurAddr.Bits.Rank = Rank;
              //CurAddr.Bits.LogicalRank = LogicalRank;
              CurAddr.Bits.Valid = 1;
              // Loop through all row failure ranges
              for (FailIndex = 0; FailIndex < Outputs->FailMax[Controller][Channel]; FailIndex++) {
                FailRangePtr = &Outputs->FailRange[Controller][Channel][FailIndex];
                // Check for address match, masking row number
                if (CurrentAddrMatch (CurAddr, FailRangePtr->Addr)) {
                  Status = mrcFail;
                  MrcCall->MrcSetMem ((UINT8 *) &BgDqMask, sizeof (BgDqMask), 0);
                  for (BankGroup = 0; BankGroup < 8; BankGroup++) {
                    // If current BG has a failure
                    if ((FailRangePtr->BankGroupMask & (1 << BankGroup)) != 0) {
                     // PprBank = (BankGroup << BaseBits) + Bank;
                      // Initialize DqMask per BG
                      for (Index = 0; Index < MAX_DQ_MASK_FAIL_RANGE; Index++) {
                        BgDqMask[BankGroup][Index] = FailRangePtr->Mask[Index];
                      }

                      // Clear PPR list and status
                      //MrcCall->MrcSetMem ((UINT8 *)&PprAddrList, sizeof (PprAddrList), 0);
                      //MrcCall->MrcSetMem ((UINT8 *)&PprStatus, sizeof (PprStatus), 0);

                      // Construct DRAM Mask
                      ByteMask = 0;

                      // Get dq mask over threshold
                      // Invert then &= with FailRangePtr->mask
                      TempFail = *FailRangePtr;
                      GetDqMaskOverThreshold (MrcData, Controller, Channel, TempFail, 8, DqMaskTemp);
                      for (Index = 0; Index < MAX_DQ_MASK_FAIL_RANGE; Index++) {
                        DqMaskTemp[Index] = ~DqMaskOver[Index] & TempFail.Mask[Index];
                        DqMaskCh[Index] = DqMaskTemp[Index];
                        BgDqMaskCh[BankGroup][Index] = BgDqMask[BankGroup][Index];
                      }

                      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                        // Set Nibble mask bits based on failing DQ bits
                        Index = Byte / 4;
                        if ((DqMaskCh[Index] >> (((Byte * 8) % 32))) & 0xFF) {
                          ByteMask |= 1 << Byte;
                          // @todo: DDR5 Low/High Byte
                          /*                          if ((*ChannelNvList)[Ch].features & X16_PRESENT) {
                          BuddyByte = (*DimmNvList)[Dimm].LogicalX16Mapping.PairLogical[Nibble / 2];
                          if (BuddyByte != BYTE_MASK) {
                          LowNibble = BuddyByte * 2;
                          NibbleMask |= (3 << LowNibble);
                          }
                          }*/
                        }
                      }
                      // If NibbleMask bits are set, then execute a PPR repair flow
                      if (ByteMask) {
                        // Handle Soft PPR or Hard PPR types
                        /*if (Setup->mem.pprType == PPR_TYPE_SOFT) {
                        // Append entries to end of global list for sPPR
                        MaxPprAddrListSize = MAX_PPR_ADDR_ENTRIES_DDR_SPPR - Host->var.mem.NumValidPprEntries;
                        PprAddrListPtr = &Host->var.mem.pprAddrSetup[Host->var.mem.NumValidPprEntries];
                        } else {*/
                        // Initialize local list for hPPR
                        MaxPprAddrListSize = 20;
                        //PprAddrListPtr = PprAddrList;
                        //}

                        // check host->var.mem.pprAddrSetup against MAX_PPR_ADDR_ENTRIES_DDR_SPPR
                        if (FailRangePtr->Size <= MaxPprAddrListSize) {

                          Status = mrcFail;
                          for (FailRow = 0; (FailRow < FailRangePtr->Size) && (Status == mrcSuccess); FailRow++) {
                            // Initialize New PPR entry
                            Status = MrcPostPackageRepair (MrcData, Controller, Channel, Rank, BankGroup, Bank, FailRangePtr->Addr.Bits.Row + FailRow, ByteMask);
                            if (Status == mrcSuccess) {
                              RepairStatus = TRUE;
                            }
                          } // FailRow
                          if (Status == mrcSuccess) {
                            // Do the PPR repairs
                            // Search and Log PprStatus
                            //RepairStatus = LogPostPackageRepairStatus (Socket, PprAddrListPtr, MaxPprValidEntry, PprStatus);
                          } // if (Status == MRC_STATUS_SUCCESS)
                        } else {
                          MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "DispositionFailRangesWithPprFlow: buffer size %d too small for %d row failures\n",
                          MaxPprAddrListSize, FailRangePtr->Size);
                          return mrcFail;
                        } // FailRangePtr->size
                      } // if (ByteMask)

                      if (RepairStatus) {
                        *RepairDone = 1;
                      }
                      if (Status == mrcSuccess) {
                        // Remove the failing DRAM devices from FailRange if PPR flow returns success
                        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                          if (ByteMask & (1 << Byte)) {
                            BgDqMaskCh[BankGroup][Byte / 4] &= ~(MRC_UINT8_MAX << (Byte * BYTE_WIDTH));
                          }
                        }
                        // Clear BG mask if all DRAMs have been repaired
                        // @todo: check number of bits to check for DDR4 here
                        if (BgDqMaskCh[BankGroup][0]) {
                          FailRangePtr->BankGroupMask &= ~(1 << BankGroup);
                        }

                        // Update BgDqMask for both SubCh
                        // @todo: DDR4
                        BgDqMask[BankGroup][0] = BgDqMaskCh[BankGroup][0];
                        BgDqMask[BankGroup][2] &= (MRC_UINT8_MAX << BYTE_WIDTH);
                        BgDqMask[BankGroup][2] |= BgDqMaskCh[BankGroup][2] & MRC_UINT8_MAX;
                      } // if (Status == MRC_STATUS_SUCCESS)
                    } // BankGroupMask
                  } // BankGroup

                    // Update common Dq mask
                  TempFail = *FailRangePtr;
                  ListUpdate = FALSE;
                  for (Index = 0; Index < MAX_DQ_MASK_FAIL_RANGE; Index++) {
                    TempFail.Mask[Index] = BgDqMask[0][Index] | BgDqMask[1][Index];
                    if (TempFail.Mask[Index] != FailRangePtr->Mask[Index]) {
                      FailRangePtr->Mask[Index] = TempFail.Mask[Index];
                      ListUpdate = TRUE;
                    }
                  }
                  if (ListUpdate) {
                    // Remove FailRange entry from FailRangeList
                    if ((TempFail.Mask[0] | TempFail.Mask[1] | TempFail.Mask[2]) == 0) {
                      //RemoveRowFailRange (Host, Socket, Ch, FailIndex); add function?
                    } else {
                      //UpdateRowFailList (Host, Socket, Ch, FALSE, TempFail);
                    }
                    FailIndex = Outputs->FailIndex[Controller][Channel] - 1;  // subtract 1 to account for FailIndex++ at end of for loop
                  }
                } // CurrentAddrMatch
              } // FailIndex
            } // Bank pair
          } // Ch loop
        } // Controller

      } // PPR Required
    } // for Rank
    // Restore test condition for retry
  } // RowFailuresFound

  return RetVal;
} // CpgcMemTestDispositionFailRange

/**
  Check to see if Retry is required after a PPR repair

  @param[in]  MrcData     - Include all MRC global data.
  @param[in]  RepairDone  - Indicator whether PPR repair was done
  @param[in]  RetryCount  - Count of retry attempts

  @retval TRUE if Retry is required; FALSE otherwise
**/
BOOLEAN
IsRetryRequired (
  IN MrcParameters *const MrcData,
  IN UINT8                RepairDone,
  IN UINT32               RetryCount
  )
{
  MrcDebug  *Debug;
  BOOLEAN   RetryRequired;

  Debug = &MrcData->Outputs.Debug;

  if (RetryCount == 0) {
    RetryRequired = TRUE;
  } else if ((RepairDone != 0) && (RetryCount < 10)) {
    RetryRequired = TRUE;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nPPR repair done so retry MemTest!\n");
  } else {
    RetryRequired = FALSE;
  }
  return RetryRequired;
}


/**

  Inject error for given Rank/Controller/Channel

  @param[in]  MrcData       - Pointer to global MRC data.
  @param[in]  Rank          - Phyiscal rank index inisde the dimm
  @param[in]  Controller    - 0-based index to controller
  @param[in]  Channel       - 0-based index to channel
  @param[in]  Bank          - bank address number
  @param[in]  Row           - row address
  @param[in]  TestSize      - number of rows to test
  @param[in]  Pattern[16]   - array holding the test pattern
  @param[in]  ErrInjMask16  - Bitmask of DQ lanes to inject error
  @param[in]  Direction     - Sequential address direction MT_ADDR_DIR_UP, MT_ADDR_DIR_DN
  @param[in]  SeqDataInv[2] - Enables pattern inversion per subsequence
  @param[in]  IsUseInvtPat  - Info to indicate whether or not patternQW is inverted by comparing original pattern
  @param[in]  UiShl[16]     - Enables pattern rotation per UI

  @retval MrcStatus - mrcSuccess/mrcFail
**/
MrcStatus
InjectMemtestError (
  IN MrcParameters* const MrcData,
  IN UINT32         Rank,
  IN UINT8          Controller,
  IN UINT8          Channel,
  IN UINT32         Bank,
  IN UINT32         Row,
  IN UINT32         TestSize,
  IN UINT64         Pattern[16],
  IN UINT16         ErrInjMask16,
  IN UINT8          Direction,
  IN UINT8          SeqDataInv[2],
  IN BOOLEAN        IsUseInvtPat,
  IN UINT8          UiShl[16]
  )
{
  MrcStatus         Status = mrcSuccess;
  MrcOutput         *Outputs;
  MrcDimmOut        *DimmOut;
  UINT32            LocalMcChBitMask;
  UINT8             ColumnBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             RowBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             BankBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             RankMask;
  UINT32            BaseBits;
  UINT32            ChannelLoop;
  UINT32            ControllerLoop;
  UINT32            CurrentRow[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32            TestSizeArray[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32            DeviceMask[MAX_CONTROLLER][MAX_CHANNEL][3];
  UINT8             TestStatus[MAX_CONTROLLER][MAX_CHANNEL];

  Outputs         = &MrcData->Outputs;

  //manually inject error here (only one controller/channel?)
  //hardcode a bunch of dimm settings
  LocalMcChBitMask = 0; // Reset bitmask for new Rank


  RankMask = (1 << Rank);
  LocalMcChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
  for (ControllerLoop = 0; ControllerLoop < MAX_CONTROLLER; ControllerLoop++) {
    for (ChannelLoop = 0; ChannelLoop < Outputs->MaxChannels; ChannelLoop++) {
      TestStatus[ControllerLoop][ChannelLoop] = 0;
      DimmOut = &Outputs->Controller[ControllerLoop].Channel[ChannelLoop].Dimm[RANK_TO_DIMM_NUMBER (Rank)];
      if (MC_CH_MASK_CHECK (LocalMcChBitMask, ControllerLoop, ChannelLoop, Outputs->MaxChannels) == 0) {
        TestSizeArray[ControllerLoop][ChannelLoop] = 0;
        continue;
      } else {
        CurrentRow[ControllerLoop][ChannelLoop] = Row;
        TestSizeArray[ControllerLoop][ChannelLoop] = TestSize;
        // Initialize per-Rank Column Row and Bank sizes
        // The MrcLog2 function returns +1 so we subtract 1
        ColumnBits[Rank][ControllerLoop][ChannelLoop] = MrcLog2 (DimmOut->ColumnSize) - 1;;
        RowBits[Rank][ControllerLoop][ChannelLoop] = 1;
        BankBits[Rank][ControllerLoop][ChannelLoop] = (MrcLog2 (DimmOut->Banks) - 1) + (MrcLog2 (DimmOut->BankGroups) - 1);
      }
    }
  }

  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "ErrInj! Rank[%d]Cont[%d]Channel[%d]:\n\tColumnBits=%d\n\tRowBits=%d\n\tBankBits=%d", Rank, Controller, Channel, ColumnBits[Rank][Controller][Channel], RowBits[Rank][Controller][Channel], BankBits[Rank][Controller][Channel]);

  CurrentRow[Controller][Channel] = Row;
  TestSizeArray[Controller][Channel] = TestSize;
  RankMask = (1 << Rank);
  LocalMcChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
  BaseBits = GetBaseBits (MrcData, LocalMcChBitMask, Rank);
  TestStatus[Controller][Channel] = 0;

  MrcProgramMATSPattern (MrcData, LocalMcChBitMask, Pattern, UiShl, 0xF000);
  Status = CpgcMemTestRowRange (MrcData, LocalMcChBitMask, Rank, ColumnBits, RowBits, BankBits, PatWr,  Direction, Bank, BaseBits,
                                  CurrentRow, TestSizeArray, DeviceMask, TestStatus, SeqDataInv, IsUseInvtPat);

  return Status;
}


/**
Function to run retention test across memory array using Raster Repo Mode 3

@param[in]  MrcData      - Pointer to global MRC data.

@retval MrcStatus - mrcSuccess if no errors found, otherwise mrcFail
**/
MrcStatus
MrcPprEnable (
  IN MrcParameters *const MrcData
  )
{
  const MRC_FUNCTION   *MrcCall;
  MrcOutput            *Outputs;
  MrcInput             *Inputs;
  MrcStatus            Status;
  UINT8                McChBitMask;
  UINT8                Test;
  INT64                GetSetDis;
  BOOLEAN              SeqDataInv[2];
  BOOLEAN              InvertedPassEn;
  UINT8                UiShl[16];
  UINT8                NumCL;
  UINT8                PatternDepth = 8;
  UINT8                RepairDone;
  UINT8                RetryCount;
  UINT64               Value;
  UINT64               BasePattern[2];
  UINT64               Pattern[16];
  UINT64               InvPattern[16];
  UINT64               StartTime;
  UINT64               FinishTime;
  UINT32               ElapsedTime;
  UINT32               TotalTime;
  UINT32               TotalTestTime = 0;
  INT64                GetSetVal;

  Status          = mrcSuccess;
  Outputs         = &MrcData->Outputs;
  Inputs          = &MrcData->Inputs;
  MrcCall         = Inputs->Call.Func;
  GetSetDis       = 0;
  InvertedPassEn  = TRUE;
  McChBitMask     = Outputs->McChBitMask;

  Value = 0x0101010101010101ULL;
  BasePattern[0] = Value;
  SeqDataInv[0] = 0;
  SeqDataInv[1] = 1;

  GetSetVal = 1;
  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableRefresh, WriteNoCache, &GetSetVal);

  for (Test = 0; Test < 8; Test++) {
    MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Begin Test %d ==================\n", Test);

    Pattern[0] = MrcCall->MrcLeftShift64 (BasePattern[0], Test);
    InvPattern[0] = ~(Pattern[0]);
    MrcCall->MrcSetMem ((UINT8 *) UiShl, sizeof (UiShl), 1);
    RepairDone = 1;
    RetryCount = 0;
    NumCL = 1;
    TotalTime = 0;
    while (IsRetryRequired (MrcData, RepairDone, RetryCount++)) {
      //1. Write sliding data pattern to all of memory
      if (Status == mrcSuccess) {
        MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #1 Begin ==================\n");
        StartTime  = MrcCall->MrcGetCpuTime ();
        Status = MrcMemTestMATS (MrcData, McChBitMask, PatWr, SeqDataInv, Pattern, FALSE, UiShl, NumCL, PatternDepth, MT_ADDR_DIR_UP);
        FinishTime = MrcCall->MrcGetCpuTime ();
        ElapsedTime = (UINT32) (FinishTime - StartTime);
        TotalTime += ElapsedTime;
        MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #1 End, ElapsedTime = %dms ==================\n\n", ElapsedTime);
      }

#if INJECT_MEMTEST_FAILURES_0001
      // Rank 0, Controller 0, Ch 0, Bank 3, Row 3, TestSize 1, ErrInjMask16 0xF000
      InjectMemtestError(MrcData, 0, 0, 0, 3, 3, 1, Pattern, 0xF000, MT_ADDR_DIR_UP, SeqDataInv, FALSE, UiShl);
#endif

      //2. Read and compare data pattern
      //   Write inverse pattern

      if (Status == mrcSuccess) {
        MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #2 Begin ==================\n");
        StartTime = MrcCall->MrcGetCpuTime();
        if (InvertedPassEn == TRUE) {
          Status = MrcMemTestMATS (MrcData, McChBitMask, PatRdWr, SeqDataInv, Pattern, FALSE, UiShl, NumCL, PatternDepth, MT_ADDR_DIR_UP);
        } else {
          // Modified step: Use down direction to pressure tWR on upper BG address
          Status = MrcMemTestMATS (MrcData, McChBitMask, PatRdWr, SeqDataInv, Pattern, FALSE, UiShl, NumCL, PatternDepth, MT_ADDR_DIR_DOWN);
        }
        FinishTime = MrcCall->MrcGetCpuTime();
        ElapsedTime = (UINT32)(FinishTime - StartTime);
        TotalTime += ElapsedTime;
        MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #2 End, ElapsedTime = %dms ==================\n\n", ElapsedTime);
      }


      //3. Read and compare inverse pattern
      //   Write original pattern
      StartTime  = MrcCall->MrcGetCpuTime ();
      if (Status == mrcSuccess) {
        MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #3 Begin ==================\n");
        StartTime = MrcCall->MrcGetCpuTime();
        if (InvertedPassEn == TRUE) {
          Status = MrcMemTestMATS (MrcData, McChBitMask, PatRdWr, SeqDataInv, InvPattern, FALSE, UiShl, NumCL, PatternDepth, MT_ADDR_DIR_UP);
        } else {
          // Modified step: Just read and compare the inverse pattern without writting the original pattern
          Status = MrcMemTestMATS (MrcData, McChBitMask, PatRd, SeqDataInv, InvPattern, TRUE, UiShl, NumCL, PatternDepth, MT_ADDR_DIR_UP);
        }
        FinishTime = MrcCall->MrcGetCpuTime();
        ElapsedTime = (UINT32)(FinishTime - StartTime);
        TotalTime += ElapsedTime;
        MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #3 ElapsedTime = %dms ==================\n\n", ElapsedTime);
      }

      // Make another MATS+ pass with inverted data
      if (InvertedPassEn == TRUE) {
        //4. Write inverse pattern to all of memory

        if (Status == mrcSuccess) {
          MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #4 Begin ==================\n");
          StartTime = MrcCall->MrcGetCpuTime();
          Status = MrcMemTestMATS (MrcData, McChBitMask, PatWr, SeqDataInv, InvPattern, FALSE, UiShl, NumCL, PatternDepth, MT_ADDR_DIR_UP);
          FinishTime = MrcCall->MrcGetCpuTime();
          ElapsedTime = (UINT32)(FinishTime - StartTime);
          TotalTime += ElapsedTime;
          MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #4 ElapsedTime = %dms ==================\n\n", ElapsedTime);
        }

        //5. Read and compare inverse pattern
        //   Write original pattern
        StartTime  = MrcCall->MrcGetCpuTime ();
        if (Status == mrcSuccess) {
          StartTime = MrcCall->MrcGetCpuTime();
          MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #5 Begin ==================\n");
          Status = MrcMemTestMATS(MrcData, McChBitMask, PatRdWr, SeqDataInv, InvPattern, FALSE, UiShl, NumCL, PatternDepth, MT_ADDR_DIR_UP);
          FinishTime = MrcCall->MrcGetCpuTime();
          ElapsedTime = (UINT32)(FinishTime - StartTime);
          TotalTime += ElapsedTime;
          MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #5 End, ElapsedTime = %dms ==================\n\n", ElapsedTime);
        }

        //6. Read and compare original pattern
        //   Write inverse pattern
        StartTime  = MrcCall->MrcGetCpuTime ();
        if (Status == mrcSuccess) {
          StartTime = MrcCall->MrcGetCpuTime();
          MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #6 Begin ==================\n");
          Status = MrcMemTestMATS(MrcData, McChBitMask, PatRdWr, SeqDataInv, Pattern, FALSE, UiShl, NumCL, PatternDepth, MT_ADDR_DIR_UP);
          FinishTime = MrcCall->MrcGetCpuTime();
          ElapsedTime = (UINT32)(FinishTime - StartTime);
          TotalTime += ElapsedTime;
          MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== Pattern #6 End, ElapsedTime = %dms ==================\n", ElapsedTime);
        }

      } // InvertedPassEn

      TotalTestTime += TotalTime;
      if (Status == mrcSuccess) {
        CpgcMemTestDispositionFailRange(MrcData, McChBitMask, &RepairDone);
      } else {
        MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_ERROR, "MemTestMATS8: Test #%d Failed!, TotalTime Elapsed = %dms\n\n", Test, TotalTime);
        break;
      }
      MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== End Test #%d, TotalTime Elapsed = %dms ==================\n\n", Test, TotalTime);
    } // while
    if (Status != mrcSuccess) {
      break;
    }
  } // Test

  if (Status == mrcSuccess) {
    MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "================== All Tests Completed!!! TotalTime Elapsed for all Tests: %dms ==================\n", TotalTestTime);
  }
#ifdef MRC_DEBUG_PRINT
  PrintTotalRowFailures(MrcData);
#endif

  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableRefresh, WriteNoCache, &GetSetDis); // Cleanup, disable refreshes
  return Status;
}

