/** @file
  Contains functions that are used outside of the DdrIo Library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _MrcDdrIoApi_h_
#define _MrcDdrIoApi_h_

/// Defines
#define MRC_DDRIO_RX_TO_FIFO_GB_PS    (500)
#define MRC_DDRIO_RX_TO_FIFO_GB_TCK   (4)
#define MRC_NUM_CCC_INSTANCES         (8)
#define MRC_NUM_CCC_GLOBAL_INSTANCES  (4)

#define DCD_GROUP_REFCLK      0
#define DCD_GROUP_VCDL        1
#define DCD_GROUP_SRZ         2
#define DCD_GROUP_SRZ_PH90    3
#define DCD_GROUP_SRZ_QUAD    4
#define DCD_GROUP_MAX         5
#define MAX_DCC_INDEX         (16)

// DCC fub types
typedef enum {
  DccFubNone    = 0,
  DccFubComp    = 1,
  DccFubData    = 2,
  DccFubCcc     = 4,
  DccFubDataCcc = (DccFubData | DccFubCcc),
  DccFubAll     = (DccFubComp | DccFubData | DccFubCcc)
} DccFubMask;

#define DCC_FSM_TIMEOUT  100              // 100 ms

#define DCC_TOTAL_COUNTS      0xC000

// @todo_adl should adjust those for B0
// 49.6 - 50.4 %
#define DCC_MARKER_REFCLK_LO  0xBE76
#define DCC_MARKER_REFCLK_HI  0xC189

// 48 - 52 %
#define DCC_MARKER_VCDL_LO    0xB851
#define DCC_MARKER_VCDL_HI    0xC7AE

// 49.9 - 50.1 %
#define DCC_MARKER_TIGHT_LO   0xBF9D
#define DCC_MARKER_TIGHT_HI   0xC062

//
// DCC Parameters
//
#define ALIGN_NUM_SAMPLES          31    // specifies the number of samples to take for clock alignment, used to filter samples
#define ALIGN_SAMPLE_THRESHOLD     18    // specifies the minimum threshold for alignment direction, determines threshold for early, late, and aligned (neither early nor late)
#define DCC_TARGET                 5000
#define DQS_DCC_THRESHOLD_HI       5010  // DQS High Duty Cycle Threshold requirement
#define DQS_DCC_THRESHOLD_LO       4990  // DQS Low Duty Cycle Threshold requirement
#define DQS_DCC_THRESHOLD_DEFAULT  5000  // Default DQS DCC Threshold
#define CLK_DCC_THRESHOLD_HI       5010  // CLK High Duty Cycle Threshold requirement
#define CLK_DCC_THRESHOLD_LO       4990  // CLK Low Duty Cycle Threshold requirement
#define SW_FSM_DCC_THRESHOLD_HI    5010  // SW FSM High Duty Cycle Threshold requirement
#define SW_FSM_DCC_THRESHOLD_LO    4990  // SW FSM Low Duty Cycle Threshold requirement
#define MAX_HIGH_DITHER_DUTY_CYCLE 5200  // High Duty Cycle Dither Threshold
#define DCC_INITIAL_LOOP_COUNT     0     // Initial DCC loop count
#define DATA_TOTAL_SAMPLE_COUNTS_INDEX          0
#define DATA_STEP1PATTERN_INDEX                 1
#define CCC_STEP1PATTERN_INDEX                  2
#define CH0CCC_CR_COMP2_REG_INDEX               3
#define CCC0_GLOBAL_CR_COMPUPDT_DLY1_REG_INDEX  4
#define CH0CCC_CR_COMP3_REG_INDEX               5
#define CCC_TOTAL_SAMPLE_COUNTS_INDEX           6
#define DCCCTL13_STEP1PATTERN_REG_INDEX         7
#define DATA0CH0_CR_PMFSM1_REG_INDEX            8
#define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG_INDEX  9
#define CCC0_GLOBAL_CR_PM_FSM_OVRD_1_REG_INDEX  10
#define CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG_INDEX 11
#define COMP_TOTAL_SAMPLE_COUNTS_INDEX          12
#define MAX_DCC_SAVED_PARAMETERS                13

#define TOTAL_SAMPLE_COUNTS_VALUE               0xc000
#define DCC_PASS_LIMIT                          2
#define DCC_SATURATE_LIMIT                      0
#define MAX_DCC_TCO_CODE                        0x1f
#define MIN_DCC_TCO_CODE                        0

//
// RX PRE SDL DCC Definitions
//
#define DCC_CORRECTION_LIMIT                    15  // Max number of corrections allowed for DCC loop
#define DCC_PI_SWEEP_RANGE                      128 // Can be in increments of 128 to obtain sweep data in multiples of 2 UI (tDQS_per)
#define RXDQS_VOC_CODE_START                    16
#define RXDQS_VOC_RANGE_MAX                     31
#define RXDQS_VOC_RANGE_MIN                     0
#define MRC_RX_FLYBY_INIT_PRE_SDL_DCC           3

//
// BWSel calibration parameters
//
#define BWSEL_LO_THRESH                         4
#define SW_BWSEL_LO_THRESH                      2
#define VDLL_LO_THRESH                          450
#define VDLL_TARG_STEP                          25
#define VDLL_TARG_GB                            100

typedef enum {
  DccAlignDone  = 0,                    ///< DCC Align
  DccAlignEarly = 1,                    ///< DCC Early.
  DccAlignLate  = 2,                    ///< DCC Late.
} DCC_ALIGN;

typedef enum {
  DccLow          = 0,                  ///< DCC Low
  DccHigh         = 1,                  ///< DCC High.
  DccStatusAlign  = 2,                  ///< DCC Align.
  DccStatusDither = 3,                  ///< DCC Dither.
  DccInit         = 4,                  ///< DCC Init.
} DCC_STATUS;

typedef enum {
  MdllRefclk = 0,                       ///< DCC MdllRefclk
  MdllFbclk  = 1,                       ///< DCC MdllFbclk
  DccDqs     = 2,                       ///< DCC DccDqs
  DccDq0     = 3,                       ///< DCC DccDq0
  DccDq1     = 4,                       ///< DCC DccDq1
  DccDq2     = 5,                       ///< DCC DccDq2
  DccDq3     = 6,                       ///< DCC DccDq3
  DccDq4     = 7,                       ///< DCC DccDq4
  DccDq5     = 8,                       ///< DCC DccDq5
  DccDq6     = 9,                       ///< DCC DccDq6
  DccDq7     = 10,                      ///< DCC DccDq7
  DccCcc0    = 11,                      ///< DCC DccCcc0
  DccCcc1    = 12,                      ///< DCC DccCcc1
  DccCcc2    = 13,                      ///< DCC DccCcc2
  DccCcc3    = 14,                      ///< DCC DccCcc3
  DccCcc4    = 15,                      ///< DCC DccCcc4
  DccCcc5    = 16,                      ///< DCC DccCcc5
  DccCcc6    = 17,                      ///< DCC DccCcc6
  DccCcc7    = 18,                      ///< DCC DccCcc7
  DccCcc8    = 19,                      ///< DCC DccCcc8
  DccCcc9    = 20,                      ///< DCC DccCcc9
  DccCcc10   = 21,                      ///< DCC DccCcc10
  DccCcc11   = 22,                      ///< DCC DccCcc11
  DccCcc12   = 23,                      ///< DCC DccCcc12
  DccCcc13   = 24,                      ///< DCC DccCcc13
  DccCcc14   = 25,                      ///< DCC DccCcc14
} DCC_CLK_TYPES;

typedef enum {
  GroupDccDqs      = 0,                 ///< DQS DCC
  GroupDccClk0     = 1,                 ///< CLK0 DCC
  GroupDccClk1     = 2,                 ///< CLK1 DCC
  GroupDccWck      = 3,                 ///< WCK DCC
} DCC_GROUP;

//
// RX POST SDL Definitions
//
#define RXDQS_DCC_THRESHOLD_HI                  5500  // RXDQS High Duty Cycle Threshold requirement
#define RXDQS_DCC_THRESHOLD_LO                  4500  // RXDQS Low Duty Cycle Threshold requirement

#define DQSRX_CMN_RXDIFFAMPEN_INDEX             0
#define RX_MATCHED_PATH_EN_INDEX                1
#define DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_INDEX  2
#define DQS_FWDCLKP_OFFSET_INDEX                3
#define DQS_FWDCLKP_INDEX                       4
#define RXSDL_CMN_QUALIFYSDLWITHRCVEN_INDEX     5
#define RX_PI_ON_INDEX                          6
#define CKTOP_CMN_BONUS_INDEX                   7

#define MAX_RX_POST_SDL_DCC_SAVED_PARAMETERS    12
#define TOTAL_SAMPLE_COUNTS_VALUE_POST_SDL_DCC  0xc000
#define RX_DCC_DUTY_CYCLE_SUM                   10000 // It's used to check the strobe duty cycle is high or low.

#define DCC_CORRECTION_LIMIT_VCDL_DCC           8     // max number of corrections allowed for RX POST SDL DCC loop
#define DCC_CORRECTION_LIMIT_DQS_VOC            16    // max number of corrections allowed for RX PRE SDL DCC loop
#define PICODE_VCDL_START                       0     // sets the starting PI code for the post-SDL DCC. This specifies the beginning of the delay line to be selected by the RxPI
#define PICODE_VCDL_END                         0x78  // sets the ending PI code for the post-SDL DCC. This specifies the end of the delay line to be selected by the RxPI
#define RXDQS_VOC_CODE_START                    16    // when presdl DCC is selected, this is the starting dcc code
#define RXSDL_DCC_CODE_START                    8     // when postsdl DCC is selected, this is the starting dcc code

// VssHiRegion types
typedef enum {
  VssHiRegionNorth,
  VssHiRegionSouth
} VssHiRegionType;

// VssHiSensePoint types
typedef enum {
  VssHiSensePointNear,
  VssHiSensePointMid,
  VssHiSensePointFar
} VssHiSensePointType;

//
// Software FSM DCC definitions.
//
#define DCC_CORRECTION_LIMIT_SWFSM              (32)
#define DCC_MAX_DITHER_ALLOWED                  (4)
#define DCC_STEP_SIZE_FINE                      (1)
#define DCC_STEP_SIZE_COARSE                    (12)
#define DCC_STEP_SIZE_COARSE_VCDL               (1)
#define DCC_INIT_CODE                           (128)
#define DCC_INIT_CODE_VCDL                      (8)
#define DCC_INIT_CODE_SRZ_QUAD                  (64)
#define MAX_DCC_CHECK_COUNT                     (10)

// Dcc code search direction
typedef enum {
  DccStepDirNone      = 0,      // None
  DccStepDirFineDec   = 1,      // Fine Decrement
  DccStepDirFineInc   = 2,      // Fine Increment
  DccStepDirCoarseDec = 3,      // Coarse Decrement
  DccStepDirCoarseInc = 4,      // Coarse Increment
  DccStepDirDither    = 5,      // Dither
  DccStepDirAlign     = 6,      // Align
} DCC_STEP_DIR;

// Dcc code search state
typedef struct {
  UINT16       Code;
  UINT16       CodePrev;
  UINT32       FbValue;
  UINT32       FbValuePrev;
  UINT16       DitherCount;
  UINT8        DccPrevStatus;            // DCC_STATUS
  UINT8        DccCurStatus;             // DCC_STATUS
  BOOLEAN      Saturated;
  BOOLEAN      Done;
  UINT8        StepSize;
  UINT8        DccStepDir;               // DCC_STEP_DIR
  BOOLEAN      DccResultPass;
} MrcDccCodeSearchState;

// Duty cycle group data
typedef struct {
  UINT32       DutyCycleMin;
  UINT32       DutyCycleMax;
  UINT32       DutyCycleMinLimit;
  UINT32       DutyCycleMaxLimit;
  BOOLEAN      DutyCycleGood;
#ifdef MRC_DEBUG_PRINT
  CHAR8        PartitionMin[7];
  CHAR8        PartitionMax[7];
#endif
  UINT8        DccIndexMin;
  UINT8        DccIndexMax;
  UINT32       DcdGroupMin;
  UINT32       DcdGroupMax;
} DutyCycleGrpData;

// Duty cycle groups.
typedef enum {
  DllRefClk   = 0,
  DllVcdl,
  Ck0,
  CkWck1,
  NonCk,
  DqsDc,
  DqDc,
  CompDc,
  CccDc,
  DataDc,
  DutyCycleMaxGrp,
} DutyCycleGrps;

/// Constants
extern const INT8 RxFifoChDelay[MRC_DDR_TYPE_UNKNOWN][MAX_GEARS][MAX_SYS_CHANNEL];
extern const UINT8 CccIndexToFub[MRC_NUM_CCC_INSTANCES];
extern const UINT8 CccMcIdxMapDdr[MRC_NUM_CCC_INSTANCES];

/// Functions

/**
  PBD step size calibration step (scomp.delay_range)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcPbdStepSizeCalibration (
  IN OUT MrcParameters* const MrcData
  );

/**
  Force Clock Align calibration step (clkphasedetect.qclk2d0align)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcForceClkAlignCalibration (
  IN OUT MrcParameters* const MrcData
  );

/**
  DLL LDO Bandwidth Select calibration step (dllldocomp.dllbwsel_cal)

  @param[in, out] MrcData         - Include all MRC global data.
  @param[in]      DistributeCodes - Distribute results to data/ccc partitions if TRUE.

  @retval mrcSuccess
**/
MrcStatus
MrcBwSelCalibration (
  IN OUT MrcParameters* const MrcData,
  IN     BOOLEAN              DistributeCodes
  );

/**
  Initial Rload calibration step (panicdrv.rload)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcRloadCalibration (
  IN OUT MrcParameters* const MrcData
  );

/**
  DLL LDO FF Code calibration step (dllldocomp.ldoff)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcLdoFFCodeCalibration (
  IN OUT MrcParameters* const MrcData
  );

/**
  DLL LDO FF Code Lock calibration step (dllldocomp.codelock_cal)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcLdoFFCodeLockCalibration (
  IN OUT MrcParameters* const MrcData
  );

/**
  Local VssHi Leaker calibration (buffer.vsshileakercal)

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcVssHiLocalLeakCal (
  IN MrcParameters *const MrcData
  );

/**
  VssHi Offset Cancellation (vsshidrv.offsetcal)
  Cancels out the opamp error for the VssHi LVR. Independent offset cancellation per region (north/south).

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcVsshiOffsetCancellation (
  IN OUT MrcParameters* const MrcData
  );

/**
  VssHiFFComp Leaker Ring Oscillator calibration (vsshicomp.leakerrocal)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcVsshiCompLeakerRoCalibration (
  IN OUT MrcParameters* const MrcData
  );

/**
  Lock UI calibration for Unmatched Rx

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcLockUiCalibration (
  IN OUT MrcParameters *const MrcData
  );

/**
  InitDCC calibration (dllldocomp.dcc_refclk, dllldocomp.dcc_vcdl, dccinit_fsm)

  @param[in, out] MrcData   - Include all MRC global data.
  @param[in]      DcdGroup  - see DCD_GROUP_xxx defines
  @param[in]      FubMask   - Run DCC init in this partition(s)
  @param[in]      RoFubMask - Run DCC RO calibration in this partition(s)

  @retval mrcSuccess
**/
MrcStatus
MrcInitDcc (
  IN OUT MrcParameters* const MrcData,
  IN UINT32                   DcdGroup,
  IN const DccFubMask         FubMask,
  IN const DccFubMask         RoFubMask
  );

/**
  FLL DCC Calibration

  @param[in]  MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcPhyInitFllInitDcc (
  IN MrcParameters *const MrcData
  );

/**
  This function returns the tCL delay separation needed from Receive Enable to the RxFifo Read Enable.
  This covers all internal logic delay in the path.

  @param[in]  MrcData - Pointer to global MRC data.
  @param[in]  Controller  - 0-based index instance to select.
  @param[in]  Channel     - 0-based index instance to select.
  @param[in]  DdrType     - Enumeration of MrcDdrType which DDR type is being enabled.
  @param[in]  GearRatio   - Integer number of the current Gear ratio.
**/
UINT8
MrcRcvEn2RxFifoReadTclDelay (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN MrcDdrType           DdrType,
  IN UINT8                GearRatio
  );

/**
  This routine returns the TX FIFO separation based on technology

  @param[in]  MrcData           - Pointer to MRC global data.

  @retval TxFifoSeparation.
**/

INT32
MrcGetTxFifoSeparation (
  IN OUT MrcParameters *const MrcData
  );

/**
  This routine computes the read and write fifo pointer delays

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  Controller        - Controller to get timings
  @param[in]  Channel           - Controller to get timings
  @param[in]  tCWL              - Write Latency for current channel
  @param[in]  AddTcwl           - Current AddTcwl value
  @param[in]  DecTcwl           - Current DecTcwl value
  @param[out] tCWL4TxDqFifoWrEn - Pointer to the write TX DQ fifo delay
  @param[out] tCWL4TxDqFifoRdEn - Pointer to the read TX DQ fifo delay

  @retval N/A.
**/
VOID
MrcGetTxDqFifoDelays (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32        Controller,
  IN     UINT32        Channel,
  IN     INT32         tCWL,
  IN     UINT32        AddTcwl,
  IN     UINT32        DecTcwl,
     OUT INT64         *tCWL4TxDqFifoWrEn,
     OUT INT64         *tCWL4TxDqFifoRdEn
  );

/**
  Translate from CCC index to system-level Controller and Channel

  @param[in]   MrcData    - Pointer to global MRC data.
  @param[in]   CccIndex   - CCC instance index [0..7]
  @param[out]  Controller - Pointer to Controller value
  @param[out]  Channel    - Pointer to Channel value

**/
VOID
DdrIoTranslateCccToMcChannel (
  IN  MrcParameters *MrcData,
  IN  UINT32        CccIndex,
  OUT UINT32        *Controller,
  OUT UINT32        *Channel
  );

MrcStatus
MrcTranslateSystemToIp (
  IN      MrcParameters *const  MrcData,
  IN OUT  UINT32        *const  Controller,
  IN OUT  UINT32        *const  Channel,
  IN OUT  UINT32        *const  Rank,
  IN OUT  UINT32        *const  Strobe,
  IN      GSM_GT        const   Group
  );

/*
  This function reads result of BwSelCalibration and programs CbEn value to Data and CCC MdllCtrl registers.

  @param[in] MrcData - Include all MRC global data.
*/
VOID
MrcCbEnDistribute (
  IN MrcParameters *const MrcData
);
/**
  This routine runs DCC for CLK and DQS

  @param[in]  MrcData                   - Pointer to MRC global data.

  @retval None

**/
VOID
RunAllDiffDcc (
  IN OUT MrcParameters *const MrcData
  );

/**
  This routine enters Dqs Diff Dcc mode.

  @param[in]  MrcData              - Pointer to MRC global data.
  @param[in]  Enable               - Enable or Disable the DCC mode
  @param[in]  TotalSampleCounts    - Total Sample Counts
  @param[in]  SavedParams          - SavedParams are used to save the register value for restoration.

  @retval None
**/
VOID
EnterDqsDiffDcc (
  IN  MrcParameters *const MrcData,
  IN  BOOLEAN        Enable,
  IN  UINT32         TotalSampleCounts,
  IN  UINT32         SavedParams[MAX_DCC_SAVED_PARAMETERS]
  );

/**
  This routine selects the DCD sample selection for Dqs.

  @param[in]  MrcData                 - Pointer to MRC global data.
  @param[in]  ClockInst               - DCC signal input.

  @retval None

**/
VOID
SelectDccAccDcdData (
  IN OUT MrcParameters *const MrcData,
  IN     DCC_CLK_TYPES        ClockInst
  );

/**
  This routine enters the CLK diff DCC mode.

  @param[in]  MrcData                 - Pointer to MRC global data.
  @param[in]  Enable                  - Enable or Disable the DCC mode
  @param[in]  TotalSampleCounts       - Total Sample Counts
  @param[in]  SelClkInst              - Group input, the value can be GroupDccClk0, or GroupDccClk1 or GroupDccWck.
  @param[in]  SavedParams             - SavedParams are used to save the register value for restoration.

  @retval None

**/
VOID
EnterClkDiffDcc (
  IN MrcParameters *const MrcData,
  IN BOOLEAN        Enable,
  IN UINT32         TotalSampleCounts,
  IN DCC_GROUP      SelClkInst,
  IN UINT32         SavedParams[MAX_DCC_SAVED_PARAMETERS]
  );

/**
  This routine gets the phase detection counts for DQS DCC

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  numSamples                - The number of samples.
  @param[in]  Prise                     - P rise.
  @param[in]  Pfall                     - P fall.
  @param[in]  MinThreshold              - Min Threshold value.

  @retval None

**/
VOID
GetPDSampleDqs (
  IN MrcParameters *const MrcData,
  IN UINT32               numSamples,
  IN DCC_ALIGN            Prise [MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN DCC_ALIGN            Pfall [MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN UINT32               MinThreshold,
  IN BOOLEAN              Print
  );

/**
  This routine gets the phase detection counts for CLK DCC

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  SelClkInst                - CLK input, the value can be GroupDccClk0, GroupDccClk1, or GroupDccWck.
  @param[in]  numSamples                - The number of samples.
  @param[in]  Prise                     - P rise.
  @param[in]  Pfall                     - P fall.
  @param[in]  MinThreshold              - Min Threshold value.

  @retval None

**/
VOID
GetPDSampleClk (
  IN MrcParameters *const MrcData,
  IN UINT32               SelClkInst,
  IN UINT32               NumSamples,
  IN DCC_ALIGN            Prise[MRC_NUM_CCC_INSTANCES],
  IN DCC_ALIGN            Pfall[MRC_NUM_CCC_INSTANCES],
  IN UINT32               MinThreshold
  );

/**
  This routine adjusts the TCO codes for alignment

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  Tcop                      - Tco prise or pfall value.
  @param[in]  Tcon                      - Tco nfall or nrise value.
  @param[in]  Pstatus                   - P rise or P fall status
  @param[in]  AlignOnly                 - Align Only or not.

  @retval Saturate

**/
BOOLEAN
AdjustTcoCodesForAlign (
  IN MrcParameters *const MrcData,
  IN UINT8*               Tcop,
  IN UINT8*               Tcon,
  IN DCC_ALIGN            Pstatus,
  IN BOOLEAN              AlignOnly
  );

/**
  This routine sets the DCC Dqs codes

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TcopRise                  - Tco prise value.
  @param[in]  TcopFall                  - Tco pfall value.
  @param[in]  TconRise                  - Tco nrise value.
  @param[in]  TconFall                  - Tco nfall value.
  @param[in]  AlignOnly                 - Align only or not.
  @param[in]  Controller                - Controller value.
  @param[in]  Channel                   - Channel value.
  @param[in]  Strobe                    - Strobe value.

  @retval None

**/
VOID
SetDiffDccDqsCodes (
  IN MrcParameters *const MrcData,
  IN UINT32               TcopRise,
  IN UINT32               TcopFall,
  IN UINT32               TcoNRise,
  IN UINT32               TcoNFall,
  IN BOOLEAN              AlignOnly,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Strobe,
  IN BOOLEAN              Print
  );

/**
  This routine sets the DCC Clk codes

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TcopRise                  - Tco prise value.
  @param[in]  TcopFall                  - Tco pfall value.
  @param[in]  TconRise                  - Tco nrise value.
  @param[in]  TconFall                  - Tco nfall value.
  @param[in]  SelClkInst                - CLK input, the value can be GroupDccClk0, GroupDccClk1, or GroupDccWck..
  @param[in]  Index                     - CCC partion index.

  @retval None

**/
VOID
SetDiffDccClkCodes (
  IN MrcParameters *const MrcData,
  IN UINT32               TcopRise,
  IN UINT32               TcopFall,
  IN UINT32               TconRise,
  IN UINT32               TconFall,
  IN UINT32               SelClkInst,
  IN UINT32               Index
  );

/**
  This routine saves the CLK TCO codes.

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TcopRise                  - Stroage to save Tco prise value.
  @param[in]  TcopFall                  - Stroage to save Tco pfall value.
  @param[in]  TconRise                  - Stroage to save Tco nrise value.
  @param[in]  TconFall                  - Stroage to save Tco nfall value.

  @retval None

**/
VOID
SaveDiffDccClkCodes (
  IN MrcParameters *const MrcData,
  IN UINT32               TcopRise[MRC_NUM_CCC_INSTANCES][2],
  IN UINT32               TcopFall[MRC_NUM_CCC_INSTANCES][2],
  IN UINT32               TconRise[MRC_NUM_CCC_INSTANCES][2],
  IN UINT32               TconFall[MRC_NUM_CCC_INSTANCES][2]
  );

/**
  This routine runs DQS DCC.

  @param[in]  MrcData                  - Pointer to MRC global data.
  @param[in]  TotalSampleCounts        - Total Sample Counts.
  @param[in]  DutyCycle                - Duty Cycle.
  @param[in]  DcdSampleCount           - Dcd Sample Count.

  @retval None

**/
VOID
RunDccAccData (
  IN MrcParameters *const MrcData,
  IN UINT32               TotalSampleCounts,
  IN UINT32               DutyCycle[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN UINT32               DcdSampleCount[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  );

/**
  This routine runs CLK DCC.

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TotalSampleCounts         - Total Sample Counts.
  @param[in]  SelClkInst                - CLK input, the value can be GroupDccClk0, GroupDccClk1, or GroupDccWck.
  @param[in]  AveDutyCycle              - Average Duty Cyle.
  @param[in]  TotalDcdSampleCount       - Total Dcd Sample Count.

  @retval None

**/
VOID
RunDiffDccAccCmd (
  IN MrcParameters *const MrcData,
  IN UINT32               TotalSampleCounts,
  IN DCC_GROUP      const SelClkInst,
  IN UINT32               AveDutyCycle[MRC_NUM_CCC_INSTANCES],
  IN UINT32               TotalDcdSampleCount[MRC_NUM_CCC_INSTANCES]
  );

/**
  This routine adjusts the TCO codes for DCC

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TcopRise                  - Tco prise value.
  @param[in]  TcopFall                  - Tco pfall value.
  @param[in]  TconRise                  - Tco nrise value.
  @param[in]  TconFall                  - Tco nfall value.
  @param[in]  DccStatus                 - DCC status, it may be 'high', 'low', or 'align.

  @retval Saturate

**/
BOOLEAN
AdjustTcoCodesForDcc (
  IN OUT MrcParameters *const MrcData,
  IN UINT8*                   TcopRise,
  IN UINT8*                   TcopFall,
  IN UINT8*                   TconRise,
  IN UINT8*                   TconFall,
  IN DCC_STATUS               DccStatus
  );

/**
  This routine setups up the data DCC mode.

  @param[in]  MrcData            - Pointer to MRC global data.
  @param[in]  Enable             - Enable or Disable the DCC mode
  @param[in]  TotalSampleCounts  - Total Sample Counts
  @param[in]  Pattern            - The Gear value, it doesn't matter for DCC.
  @param[in]  SavedParams        - SavedParams are used to save the register value for restoration.

  @retval None
**/
VOID
SetupDccAccData (
  IN  MrcParameters *const MrcData,
  IN  BOOLEAN        Enable,
  IN  UINT32         TotalSampleCounts,
  IN  UINT32         Pattern,
  IN  UINT32         SavedParams[MAX_DCC_SAVED_PARAMETERS]
  );

/**
  This routine sets up CLK diff DCC.

  @param[in]  MrcData                 - Pointer to MRC global data.
  @param[in]  Enable                  - Enable or Disable the CLK DCC mode
  @param[in]  TotalSampleCounts       - Total Sample Counts
  @param[in]  Pattern                 - Pattern. The value doesn't matter for DCC.
  @param[in]  DiffClk                 - It's diff CLK DCC or not
  @param[in]  SavedParams             - SavedParams are used to save the register value for restoration.

  @retval None

**/
VOID
SetupDccAccCmd (
  IN OUT MrcParameters *const MrcData,
  IN     BOOLEAN        Enable,
  IN     UINT32         TotalSampleCounts,
  IN     UINT32         Pattern,
  IN     BOOLEAN        DiffClk,
  IN     UINT32         SavedParams[MAX_DCC_SAVED_PARAMETERS]
  );

/**
  This routine selects the DCD sample selection for CLK.

  @param[in]  MrcData                 - Pointer to MRC global data.
  @param[in]  SelClkInst              - DCC signal group input, the value can be GroupDccClk0, GroupDccClk1 or GroupDccWck.

  @retval None

**/
VOID
SelectDccAccDcdCmd (
  IN OUT MrcParameters *const MrcData,
  IN     DCC_CLK_TYPES        ClockInst
  );

/**
  This routine runs CLK DCC.

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TotalSampleCounts         - Total Sample Counts.
  @param[in]  DutyCycle                 - Duty Cycle.
  @param[in]  DcdSampleCount            - Dcd Sample Count.

  @retval None

**/
VOID
RunDccAccCmd (
  IN MrcParameters *const MrcData,
  IN UINT32               TotalSampleCounts,
  IN UINT32               DutyCycle[MRC_NUM_CCC_INSTANCES],
  IN UINT32               DcdSampleCount[MRC_NUM_CCC_INSTANCES]
  );

/**
  Perform Rx PRE SDL DCC Training.
  Runs differential DCC on the DQSP/N Rx.
  Detects duty cycle at the output of the Rx just before the RxSDL input
  Corrects the duty cycle at the output of the Rx regardless of the Rx input differential duty cycle
  For DDR5, duty cycle is corrected at the DQS Rx pad input using the DDR5 DRAM DCA feature
  This procedure should be run per rank for every populated channel and byte

  @param[in]  MrcData - Pointer to MRC global data.

  @retval MrcStatus
**/
MrcStatus
MrcRxPreSdlDccTraining (
  IN OUT MrcParameters *const MrcData
  );

/**
  This routine runs Rx POST SDL DCC training

  @param[in]  MrcData - Pointer to MRC global data.

  @retval     MrcStatus - if succeeded, return mrcSuccess
**/
MrcStatus
MrcRxPostSdlDccTraining (
  IN OUT MrcParameters *const MrcData
  );

/**
  This routine will start the DDR5 DRAM Continuous Burst Output

  @param[in]  MrcData - Pointer to MRC global data.
  @param[in]  Enable  - Enable continous burst mode or not.
  @param[in]  Rank    - Rank number
**/
VOID
StartJedecContBurstModePerRank (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  Enable,
  IN UINT32                   Rank
  );

/**
  This routine enables the RX Dqs Generation

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Enable      - Enable Rx DQS DCD or disable it.
  @param[in]  SavedParams - Storage to save and restore the CSRs.
**/
VOID
RxDqsGenEn (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  Enable,
  IN UINT32                   SavedParams[MAX_RX_POST_SDL_DCC_SAVED_PARAMETERS]
  );

/**
  This routine sets the Rx DQS DCD

  @param[in]  MrcData         - Pointer to MRC global data.
  @param[in]  PadRxEnable     - Pad Rx Enable or not.
  @param[in]  Enable          - Enable Rx DQS DCD or diable it.
  @param[in]  SavedParamsRxDqs - Storage to save and restore the CSRs.
  @param[in]  SaveRxDqsP       - Storage to save and restore the RxDqsP CSRs.
  @param[in]  SaveRxDqsN       - Storage to save and restore the RxDqsN CSRs.

**/
VOID
SetRxDqsDcd (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  PadRxEnable,
  IN BOOLEAN                  Enable,
  IN UINT32                   SavedParamsRxDqs[MAX_RX_POST_SDL_DCC_SAVED_PARAMETERS],
  IN INT64                    SaveRxDqsP[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN INT64                    SaveRxDqsN[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  );

/**
  This routine programs the VCDL DCC code

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Controller number.
  @param[in]  Channel     - Channel number.
  @param[in]  Strobe      - Strobe number.
  @param[in]  DccCode     - DccCode to program.
  @param[in]  Print       - Print debug message or not.
**/
VOID
SetRxVcdlDccCode (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   Controller,
  IN UINT32                   Channel,
  IN UINT32                   Strobe,
  IN UINT32                   DccCode,
  IN BOOLEAN                  Print
  );

/**
  This routine adjusts the VCDL DCC code

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  VcdlDccCode - DCC code input for adjusting.
  @param[in]  Status      - DCC Status, the value can be DccLow, DccHigh or DccAlign.
  @param[in]  DccCode     - DccCode output after adjusting.
  @param[in]  Saturate    - Saturate or not.
**/
VOID
AdjustVcdlDccCode (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                VcdlDccCode,
  IN     DCC_STATUS           Status,
  IN OUT UINT8                *DccCode,
  IN OUT BOOLEAN              *Saturate
  );


/**
  Enable scr_gvblock_ovrride in CCC and DATA fubs.

  @param[in] MrcData     - The Mrc Host data structure
**/
VOID
ScrGvblockOverrideEnable (
  IN MrcParameters  *MrcData
  );

/**
  PHY init - compupdone_ovrrideen_duringprecomp / compupdone_ovrridedisable_duringprecomp

  @param[in, out] MrcData - Include all MRC global data
  @param[in]      Value   - Override value (0 or 1)

  @retval MrcStatus - mrcSuccess if successful or an error status
**/
MrcStatus
MrcPhyInitCompUpdateOverride (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               Value
  );

/**
  This routine adjusts the RxDqs Voc code

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  RxDqsVocCode  - VOC code input for adjusting.
  @param[in]  Status        - DCC Status, the value can be DccLow, DccHigh or DccAlign.
  @param[in]  DccCode       - DccCode output after adjusting.
  @param[in]  Saturate      - Saturate or not.
**/
VOID
AdjustRxDqsVocCode (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                RxDqsVocCode,
  IN     DCC_STATUS           Status,
  IN OUT UINT8                *DccCode,
  IN OUT BOOLEAN              *Saturate
  );

/**
  Run DCC code calibration algorithm for DCD group

  @param[in, out] MrcData        - Include all MRC global data.
  @param[in]      DcdGroup       - DCD_GROUP type.
  @param[in]      CompIndexMask  - COMP DCC Lane mask to run.
  @param[in]      CccIndexMask   - CCC DCC Lane mask to run on all CCC instances.
  @param[in]      DataIndexMask  - DATA DCC Lane mask to run on all DATA instances.

  @retval None
**/
VOID
MrcInitSwFsm (
  IN OUT MrcParameters* const MrcData,
  IN const UINT32             DcdGroup,
  IN UINT16                   CompIndexMask,
  IN UINT16                   CccIndexMask[MAX_CCC_INSTANCES],
  IN UINT16                   DataIndexMask[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  );

/**
  Run DCC code calibration algorithm for DCD Groups/Lanes to optimize duty cycles within Spec Limits

  @param[in, out] MrcData        - Include all MRC global data.

  @retval None
**/
VOID
DccOptimize (
  IN OUT MrcParameters* const MrcData
  );

  /**
  Run DCC for Dqs, and Diff Clk (CLK0, CLK1, WCK)

  @param[in] MrcData           - Include all MRC global data.
  @param[in] TotalSampleCounts - Total Sample Counts.
  @param[in] Group             - Group, can be GroupDccDqs, GroupDccClk0, GroupDccClk1,  GroupDccWck.

  @retval None
**/
VOID
RunDiffClkDcc (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   TotalSampleCounts,
  IN DCC_GROUP          const Group,
  IN BOOLEAN                  DccHiLoOverride,
  IN UINT32                   DqsDutyCyleThresholdHiInput[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN UINT32                   DqsDutyCyleThresholdLoInput[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN BOOLEAN                  Print
  );

#endif //_MrcDdrIoApi_h_
