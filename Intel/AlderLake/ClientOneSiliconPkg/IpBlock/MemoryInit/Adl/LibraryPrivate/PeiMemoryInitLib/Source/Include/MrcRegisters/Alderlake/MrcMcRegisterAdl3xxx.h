#ifndef __MrcMcRegisterAdl3xxx_h__
#define __MrcMcRegisterAdl3xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define DATA6_GLOBAL_CR_DDREARLYCR_REG                                 (0x00003000)
//Duplicate of DDRVTT0_CR_DDREARLY_RSTRDONE_REG

#define DATA6_GLOBAL_CR_IOLVRCTL0_REG                                  (0x00003004)
//Duplicate of DATA0_GLOBAL_CR_IOLVRCTL0_REG

#define DATA6_GLOBAL_CR_PGATE_PGCOMBOCTRL_REG                          (0x00003008)
//Duplicate of DATA0_GLOBAL_CR_PGATE_PGCOMBOCTRL_REG

#define DATA6_GLOBAL_CR_VTTDRVSEGCTL_REG                               (0x0000300C)
//Duplicate of DATA0_GLOBAL_CR_VTTDRVSEGCTL_REG

#define DATA6_GLOBAL_CR_VTTDRVSEGCTL1_REG                              (0x00003010)
//Duplicate of DATA0_GLOBAL_CR_VTTDRVSEGCTL1_REG

#define DATA6_GLOBAL_CR_VIEWCTL_REG                                    (0x00003014)
//Duplicate of DATA0_GLOBAL_CR_VIEWCTL_REG

#define DATA6_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_REG                        (0x00003018)
//Duplicate of DATA0_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_REG

#define DATA6_GLOBAL_CR_PMTIMER0_REG                                   (0x0000301C)
//Duplicate of DATA0_GLOBAL_CR_PMTIMER0_REG

#define DATA6_GLOBAL_CR_PMTIMER1_REG                                   (0x00003020)
//Duplicate of DATA0_GLOBAL_CR_PMTIMER1_REG

#define DATA7_GLOBAL_CR_DDREARLYCR_REG                                 (0x00003080)
//Duplicate of DDRVTT0_CR_DDREARLY_RSTRDONE_REG

#define DATA7_GLOBAL_CR_IOLVRCTL0_REG                                  (0x00003084)
//Duplicate of DATA0_GLOBAL_CR_IOLVRCTL0_REG

#define DATA7_GLOBAL_CR_PGATE_PGCOMBOCTRL_REG                          (0x00003088)
//Duplicate of DATA0_GLOBAL_CR_PGATE_PGCOMBOCTRL_REG

#define DATA7_GLOBAL_CR_VTTDRVSEGCTL_REG                               (0x0000308C)
//Duplicate of DATA0_GLOBAL_CR_VTTDRVSEGCTL_REG

#define DATA7_GLOBAL_CR_VTTDRVSEGCTL1_REG                              (0x00003090)
//Duplicate of DATA0_GLOBAL_CR_VTTDRVSEGCTL1_REG

#define DATA7_GLOBAL_CR_VIEWCTL_REG                                    (0x00003094)
//Duplicate of DATA0_GLOBAL_CR_VIEWCTL_REG

#define DATA7_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_REG                        (0x00003098)
//Duplicate of DATA0_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_REG

#define DATA7_GLOBAL_CR_PMTIMER0_REG                                   (0x0000309C)
//Duplicate of DATA0_GLOBAL_CR_PMTIMER0_REG

#define DATA7_GLOBAL_CR_PMTIMER1_REG                                   (0x000030A0)
//Duplicate of DATA0_GLOBAL_CR_PMTIMER1_REG

#define CCC0_GLOBAL_IOLVRCTL_REG                                       (0x00003100)

  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_lvrbypass_OFF              ( 0)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_lvrbypass_WID              ( 1)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_lvrbypass_MSK              (0x00000001)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_lvrbypass_MIN              (0)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_lvrbypass_MAX              (1) // 0x00000001
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_lvrbypass_DEF              (0x00000000)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_lvrbypass_HSH              (0x01003100)

  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_gndsel_OFF                 ( 1)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_gndsel_WID                 ( 1)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_gndsel_MSK                 (0x00000002)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_gndsel_MIN                 (0)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_gndsel_MAX                 (1) // 0x00000001
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_gndsel_DEF                 (0x00000000)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_gndsel_HSH                 (0x01023100)

  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codelvrleg_OFF             ( 2)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codelvrleg_WID             ( 8)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codelvrleg_MSK             (0x000003FC)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codelvrleg_MIN             (0)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codelvrleg_MAX             (255) // 0x000000FF
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codelvrleg_DEF             (0x0000000F)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codelvrleg_HSH             (0x08043100)

  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codeffleg_OFF              (10)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codeffleg_WID              ( 8)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codeffleg_MSK              (0x0003FC00)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codeffleg_MIN              (0)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codeffleg_MAX              (255) // 0x000000FF
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codeffleg_DEF              (0x0000000F)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_codeffleg_HSH              (0x08143100)

  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enlvrleg_OFF               (18)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enlvrleg_WID               ( 1)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enlvrleg_MSK               (0x00040000)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enlvrleg_MIN               (0)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enlvrleg_MAX               (1) // 0x00000001
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enlvrleg_DEF               (0x00000001)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enlvrleg_HSH               (0x01243100)

  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enffleg_OFF                (19)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enffleg_WID                ( 1)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enffleg_MSK                (0x00080000)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enffleg_MIN                (0)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enffleg_MAX                (1) // 0x00000001
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enffleg_DEF                (0x00000001)
  #define CCC0_GLOBAL_IOLVRCTL_iolvrseg_cmn_enffleg_HSH                (0x01263100)

  #define CCC0_GLOBAL_IOLVRCTL_iolvr_global_bonus_OFF                  (20)
  #define CCC0_GLOBAL_IOLVRCTL_iolvr_global_bonus_WID                  (12)
  #define CCC0_GLOBAL_IOLVRCTL_iolvr_global_bonus_MSK                  (0xFFF00000)
  #define CCC0_GLOBAL_IOLVRCTL_iolvr_global_bonus_MIN                  (0)
  #define CCC0_GLOBAL_IOLVRCTL_iolvr_global_bonus_MAX                  (4095) // 0x00000FFF
  #define CCC0_GLOBAL_IOLVRCTL_iolvr_global_bonus_DEF                  (0x00000000)
  #define CCC0_GLOBAL_IOLVRCTL_iolvr_global_bonus_HSH                  (0x0C283100)

#define CCC0_GLOBAL_CR_VIEWCT_REG                                      (0x00003104)

  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_partobsen_OFF                ( 0)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_partobsen_WID                ( 2)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_partobsen_MSK                (0x00000003)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_partobsen_MIN                (0)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_partobsen_MAX                (3) // 0x00000003
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_partobsen_DEF                (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_partobsen_HSH                (0x02003104)

  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport1_OFF   ( 2)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport1_WID   ( 4)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport1_MSK   (0x0000003C)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport1_MIN   (0)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport1_MAX   (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport1_DEF   (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport1_HSH   (0x04043104)

  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport0_OFF   ( 6)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport0_WID   ( 4)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport0_MSK   (0x000003C0)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport0_MIN   (0)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport0_MAX   (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport0_DEF   (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxdig_cmn_viewdigcbbselport0_HSH   (0x040C3104)

  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanasel_OFF           (10)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanasel_WID           ( 3)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanasel_MSK           (0x00001C00)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanasel_MIN           (0)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanasel_MAX           (7) // 0x00000007
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanasel_DEF           (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanasel_HSH           (0x03143104)

  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanaen_OFF            (13)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanaen_WID            ( 1)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanaen_MSK            (0x00002000)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanaen_MIN            (0)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanaen_MAX            (1) // 0x00000001
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanaen_DEF            (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCT_viewmxana_cmn_viewanaen_HSH            (0x011A3104)

  #define CCC0_GLOBAL_CR_VIEWCT_reserved0_OFF                          (14)
  #define CCC0_GLOBAL_CR_VIEWCT_reserved0_WID                          (18)
  #define CCC0_GLOBAL_CR_VIEWCT_reserved0_MSK                          (0xFFFFC000)
  #define CCC0_GLOBAL_CR_VIEWCT_reserved0_MIN                          (0)
  #define CCC0_GLOBAL_CR_VIEWCT_reserved0_MAX                          (262143) // 0x0003FFFF
  #define CCC0_GLOBAL_CR_VIEWCT_reserved0_DEF                          (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCT_reserved0_HSH                          (0x121C3104)

#define CCC0_GLOBAL_IOLVRSLV_CTRL_REG                                  (0x00003108)

  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved2_OFF                      ( 0)
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved2_WID                      (16)
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved2_MSK                      (0x0000FFFF)
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved2_MIN                      (0)
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved2_MAX                      (65535) // 0x0000FFFF
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved2_DEF                      (0x00000000)
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved2_HSH                      (0x10003108)

  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved1_OFF                      (16)
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved1_WID                      (16)
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved1_MSK                      (0xFFFF0000)
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved1_MIN                      (0)
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved1_MAX                      (65535) // 0x0000FFFF
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved1_DEF                      (0x00000000)
  #define CCC0_GLOBAL_IOLVRSLV_CTRL_reserved1_HSH                      (0x10203108)

#define CCC0_GLOBAL_CR_DDREARLYCR_REG                                  (0x0000310C)
//Duplicate of DDRVTT0_CR_DDREARLY_RSTRDONE_REG

#define CCC0_GLOBAL_CR_VIEWCTL_REG                                     (0x00003110)

  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_top_OFF   ( 0)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_top_WID   ( 4)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_top_MSK   (0x0000000F)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_top_MIN   (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_top_MAX   (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_top_DEF   (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport0sel_top_HSH   (0x04003110)

  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_top_OFF   ( 4)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_top_WID   ( 4)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_top_MSK   (0x000000F0)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_top_MIN   (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_top_MAX   (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_top_DEF   (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewdig_cmn_viewdigport1sel_top_HSH   (0x04083110)

  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_top_OFF ( 8)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_top_WID ( 4)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_top_MSK (0x00000F00)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_top_MIN (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_top_MAX (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_top_DEF (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport0_top_HSH (0x04103110)

  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_top_OFF (12)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_top_WID ( 4)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_top_MSK (0x0000F000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_top_MIN (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_top_MAX (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_top_DEF (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxdig_cmn_viewdigcbbselport1_top_HSH (0x04183110)

  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanabiassel_top_OFF     (16)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanabiassel_top_WID     ( 3)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanabiassel_top_MSK     (0x00070000)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanabiassel_top_MIN     (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanabiassel_top_MAX     (7) // 0x00000007
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanabiassel_top_DEF     (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanabiassel_top_HSH     (0x03203110)

  #define CCC0_GLOBAL_CR_VIEWCTL_mdll_cmn_viewanaen_top_OFF            (19)
  #define CCC0_GLOBAL_CR_VIEWCTL_mdll_cmn_viewanaen_top_WID            ( 1)
  #define CCC0_GLOBAL_CR_VIEWCTL_mdll_cmn_viewanaen_top_MSK            (0x00080000)
  #define CCC0_GLOBAL_CR_VIEWCTL_mdll_cmn_viewanaen_top_MIN            (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_mdll_cmn_viewanaen_top_MAX            (1) // 0x00000001
  #define CCC0_GLOBAL_CR_VIEWCTL_mdll_cmn_viewanaen_top_DEF            (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_mdll_cmn_viewanaen_top_HSH            (0x01263110)

  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanasel_top_OFF      (20)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanasel_top_WID      ( 3)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanasel_top_MSK      (0x00700000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanasel_top_MIN      (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanasel_top_MAX      (7) // 0x00000007
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanasel_top_DEF      (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanasel_top_HSH      (0x03283110)

  #define CCC0_GLOBAL_CR_VIEWCTL_rxvref_cmn_viewanaen_top_OFF          (23)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxvref_cmn_viewanaen_top_WID          ( 1)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxvref_cmn_viewanaen_top_MSK          (0x00800000)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxvref_cmn_viewanaen_top_MIN          (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxvref_cmn_viewanaen_top_MAX          (1) // 0x00000001
  #define CCC0_GLOBAL_CR_VIEWCTL_rxvref_cmn_viewanaen_top_DEF          (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxvref_cmn_viewanaen_top_HSH          (0x012E3110)

  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanaen_top_OFF          (24)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanaen_top_WID          ( 1)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanaen_top_MSK          (0x01000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanaen_top_MIN          (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanaen_top_MAX          (1) // 0x00000001
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanaen_top_DEF          (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_viewanaen_top_HSH          (0x01303110)

  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top1_OFF      (25)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top1_WID      ( 1)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top1_MSK      (0x02000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top1_MIN      (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top1_MAX      (1) // 0x00000001
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top1_DEF      (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top1_HSH      (0x01323110)

  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top0_OFF      (26)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top0_WID      ( 1)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top0_MSK      (0x04000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top0_MIN      (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top0_MAX      (1) // 0x00000001
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top0_DEF      (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_viewmxana_cmn_viewanaen_top0_HSH      (0x01343110)

  #define CCC0_GLOBAL_CR_VIEWCTL_iolvrseg_cmn_viewanaen_top_OFF        (27)
  #define CCC0_GLOBAL_CR_VIEWCTL_iolvrseg_cmn_viewanaen_top_WID        ( 1)
  #define CCC0_GLOBAL_CR_VIEWCTL_iolvrseg_cmn_viewanaen_top_MSK        (0x08000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_iolvrseg_cmn_viewanaen_top_MIN        (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_iolvrseg_cmn_viewanaen_top_MAX        (1) // 0x00000001
  #define CCC0_GLOBAL_CR_VIEWCTL_iolvrseg_cmn_viewanaen_top_DEF        (0x00000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_iolvrseg_cmn_viewanaen_top_HSH        (0x01363110)

  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_vrefsel_top_OFF            (28)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_vrefsel_top_WID            ( 4)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_vrefsel_top_MSK            (0xF0000000)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_vrefsel_top_MIN            (0)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_vrefsel_top_MAX            (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_vrefsel_top_DEF            (0x00000005)
  #define CCC0_GLOBAL_CR_VIEWCTL_rxbias_cmn_vrefsel_top_HSH            (0x04383110)

#define CCC0_GLOBAL_CR_CCCDRXVREF_REG                                  (0x00003114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrenvrefgenweakres_top_OFF ( 0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrenvrefgenweakres_top_WID ( 1)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrenvrefgenweakres_top_MSK (0x00000001)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrenvrefgenweakres_top_MIN (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrenvrefgenweakres_top_MAX (1) // 0x00000001
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrenvrefgenweakres_top_DEF (0x00000000)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrenvrefgenweakres_top_HSH (0x01003114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrlevshftbypasspmos_top_OFF ( 1)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrlevshftbypasspmos_top_WID ( 1)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrlevshftbypasspmos_top_MSK (0x00000002)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrlevshftbypasspmos_top_MIN (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrlevshftbypasspmos_top_MAX (1) // 0x00000001
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrlevshftbypasspmos_top_DEF (0x00000000)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrlevshftbypasspmos_top_HSH (0x01023114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefgenenable_top_OFF ( 2)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefgenenable_top_WID ( 1)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefgenenable_top_MSK (0x00000004)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefgenenable_top_MIN (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefgenenable_top_MAX (1) // 0x00000001
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefgenenable_top_DEF (0x00000000)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefgenenable_top_HSH (0x01043114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvddsupply_top_OFF    ( 3)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvddsupply_top_WID    ( 1)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvddsupply_top_MSK    (0x00000008)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvddsupply_top_MIN    (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvddsupply_top_MAX    (1) // 0x00000001
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvddsupply_top_DEF    (0x00000000)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvddsupply_top_HSH    (0x01063114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvsxhi_top_OFF        ( 4)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvsxhi_top_WID        ( 1)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvsxhi_top_MSK        (0x00000010)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvsxhi_top_MIN        (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvsxhi_top_MAX        (1) // 0x00000001
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvsxhi_top_DEF        (0x00000001)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_selvsxhi_top_HSH        (0x01083114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top1_OFF    ( 5)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top1_WID    ( 9)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top1_MSK    (0x00003FE0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top1_MIN    (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top1_MAX    (511) // 0x000001FF
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top1_DEF    (0x00000100)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top1_HSH    (0x090A3114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top0_OFF    (14)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top0_WID    ( 9)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top0_MSK    (0x007FC000)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top0_MIN    (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top0_MAX    (511) // 0x000001FF
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top0_DEF    (0x00000100)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxvref_cmn_ddrvrefctrl_top0_HSH    (0x091C3114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxall_cmn_rxlpddrmode_top_OFF      (23)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxall_cmn_rxlpddrmode_top_WID      ( 2)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxall_cmn_rxlpddrmode_top_MSK      (0x01800000)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxall_cmn_rxlpddrmode_top_MIN      (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxall_cmn_rxlpddrmode_top_MAX      (3) // 0x00000003
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxall_cmn_rxlpddrmode_top_DEF      (0x00000001)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxall_cmn_rxlpddrmode_top_HSH      (0x022E3114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_cccrxflops_cmn_cccrxreset_top_OFF  (25)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_cccrxflops_cmn_cccrxreset_top_WID  ( 1)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_cccrxflops_cmn_cccrxreset_top_MSK  (0x02000000)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_cccrxflops_cmn_cccrxreset_top_MIN  (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_cccrxflops_cmn_cccrxreset_top_MAX  (1) // 0x00000001
  #define CCC0_GLOBAL_CR_CCCDRXVREF_cccrxflops_cmn_cccrxreset_top_DEF  (0x00000001)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_cccrxflops_cmn_cccrxreset_top_HSH  (0x01323114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_biasicomp_top_OFF       (26)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_biasicomp_top_WID       ( 4)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_biasicomp_top_MSK       (0x3C000000)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_biasicomp_top_MIN       (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_biasicomp_top_MAX       (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_biasicomp_top_DEF       (0x00000008)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_biasicomp_top_HSH       (0x04343114)

  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_tailctl_top_OFF         (30)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_tailctl_top_WID         ( 2)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_tailctl_top_MSK         (0xC0000000)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_tailctl_top_MIN         (0)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_tailctl_top_MAX         (3) // 0x00000003
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_tailctl_top_DEF         (0x00000002)
  #define CCC0_GLOBAL_CR_CCCDRXVREF_rxbias_cmn_tailctl_top_HSH         (0x023C3114)

#define CCC0_GLOBAL_BONUS_0_REG                                        (0x00003118)

  #define CCC0_GLOBAL_BONUS_0_cccglobal_cmn_bonus_0_OFF                ( 0)
  #define CCC0_GLOBAL_BONUS_0_cccglobal_cmn_bonus_0_WID                (32)
  #define CCC0_GLOBAL_BONUS_0_cccglobal_cmn_bonus_0_MSK                (0xFFFFFFFF)
  #define CCC0_GLOBAL_BONUS_0_cccglobal_cmn_bonus_0_MIN                (0)
  #define CCC0_GLOBAL_BONUS_0_cccglobal_cmn_bonus_0_MAX                (4294967295) // 0xFFFFFFFF
  #define CCC0_GLOBAL_BONUS_0_cccglobal_cmn_bonus_0_DEF                (0x00000000)
  #define CCC0_GLOBAL_BONUS_0_cccglobal_cmn_bonus_0_HSH                (0x20003118)

#define CCC0_GLOBAL_BONUS_1_REG                                        (0x0000311C)

  #define CCC0_GLOBAL_BONUS_1_cccglobal_cmn_bonus_1_OFF                ( 0)
  #define CCC0_GLOBAL_BONUS_1_cccglobal_cmn_bonus_1_WID                (32)
  #define CCC0_GLOBAL_BONUS_1_cccglobal_cmn_bonus_1_MSK                (0xFFFFFFFF)
  #define CCC0_GLOBAL_BONUS_1_cccglobal_cmn_bonus_1_MIN                (0)
  #define CCC0_GLOBAL_BONUS_1_cccglobal_cmn_bonus_1_MAX                (4294967295) // 0xFFFFFFFF
  #define CCC0_GLOBAL_BONUS_1_cccglobal_cmn_bonus_1_DEF                (0x00000000)
  #define CCC0_GLOBAL_BONUS_1_cccglobal_cmn_bonus_1_HSH                (0x2000311C)

#define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG                               (0x00003120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pmclkgatedisable_OFF            ( 0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pmclkgatedisable_WID            ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pmclkgatedisable_MSK            (0x00000001)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pmclkgatedisable_MIN            (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pmclkgatedisable_MAX            (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pmclkgatedisable_DEF            (0x00000001)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pmclkgatedisable_HSH            (0x01003120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovren_OFF            ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovren_WID            ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovren_MSK            (0x00000002)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovren_MIN            (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovren_MAX            (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovren_DEF            (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovren_HSH            (0x01023120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovrval_OFF           ( 2)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovrval_WID           ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovrval_MSK           (0x00000004)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovrval_MIN           (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovrval_MAX           (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovrval_DEF           (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_rampenable_ovrval_HSH           (0x01043120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_initdone_ovren_OFF              ( 3)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_initdone_ovren_WID              ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_initdone_ovren_MSK              (0x00000008)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_initdone_ovren_MIN              (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_initdone_ovren_MAX              (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_initdone_ovren_DEF              (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_initdone_ovren_HSH              (0x01063120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_init_done_ovrval_OFF            ( 4)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_init_done_ovrval_WID            ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_init_done_ovrval_MSK            (0x00000010)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_init_done_ovrval_MIN            (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_init_done_ovrval_MAX            (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_init_done_ovrval_DEF            (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_init_done_ovrval_HSH            (0x01083120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovren_OFF              ( 5)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovren_WID              ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovren_MSK              (0x00000020)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovren_MIN              (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovren_MAX              (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovren_DEF              (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovren_HSH              (0x010A3120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovrval_OFF             ( 6)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovrval_WID             ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovrval_MSK             (0x00000040)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovrval_MIN             (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovrval_MAX             (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovrval_DEF             (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_iobufact_ovrval_HSH             (0x010C3120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_ovren_OFF              ( 7)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_ovren_WID              ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_ovren_MSK              (0x00000080)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_ovren_MIN              (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_ovren_MAX              (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_ovren_DEF              (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_ovren_HSH              (0x010E3120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_start_clkalign_ovrval_OFF       ( 8)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_start_clkalign_ovrval_WID       ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_start_clkalign_ovrval_MSK       (0x00000100)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_start_clkalign_ovrval_MIN       (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_start_clkalign_ovrval_MAX       (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_start_clkalign_ovrval_DEF       (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_start_clkalign_ovrval_HSH       (0x01103120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalignrst_ovrval_OFF          ( 9)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalignrst_ovrval_WID          ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalignrst_ovrval_MSK          (0x00000200)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalignrst_ovrval_MIN          (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalignrst_ovrval_MAX          (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalignrst_ovrval_DEF          (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalignrst_ovrval_HSH          (0x01123120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovren_OFF              (10)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovren_WID              ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovren_MSK              (0x00000400)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovren_MIN              (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovren_MAX              (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovren_DEF              (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovren_HSH              (0x01143120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovrval_OFF             (11)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovrval_WID             ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovrval_MSK             (0x00000800)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovrval_MIN             (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovrval_MAX             (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovrval_DEF             (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_func_rst_ovrval_HSH             (0x01163120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovren_OFF                  (12)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovren_WID                  ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovren_MSK                  (0x00001000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovren_MIN                  (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovren_MAX                  (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovren_DEF                  (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovren_HSH                  (0x01183120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovrval_OFF                 (13)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovrval_WID                 ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovrval_MSK                 (0x00002000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovrval_MIN                 (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovrval_MAX                 (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovrval_DEF                 (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_pien_ovrval_HSH                 (0x011A3120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovren_OFF                 (14)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovren_WID                 ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovren_MSK                 (0x00004000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovren_MIN                 (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovren_MAX                 (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovren_DEF                 (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovren_HSH                 (0x011C3120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovrval_OFF                (15)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovrval_WID                ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovrval_MSK                (0x00008000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovrval_MIN                (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovrval_MAX                (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovrval_DEF                (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_dllen_ovrval_HSH                (0x011E3120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_ldoen_ovrval_OFF                (16)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_ldoen_ovrval_WID                ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_ldoen_ovrval_MSK                (0x00010000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_ldoen_ovrval_MIN                (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_ldoen_ovrval_MAX                (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_ldoen_ovrval_DEF                (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_ldoen_ovrval_HSH                (0x01203120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovren_OFF     (17)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovren_WID     ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovren_MSK     (0x00020000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovren_MIN     (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovren_MAX     (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovren_DEF     (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovren_HSH     (0x01223120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovrval_OFF    (18)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovrval_WID    ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovrval_MSK    (0x00040000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovrval_MIN    (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovrval_MAX    (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovrval_DEF    (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_DdrxxClkGoodQnnnH_ovrval_HSH    (0x01243120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovren_OFF        (19)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovren_WID        ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovren_MSK        (0x00080000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovren_MIN        (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovren_MAX        (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovren_DEF        (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovren_HSH        (0x01263120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovrval_OFF       (20)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovrval_WID       ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovrval_MSK       (0x00100000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovrval_MIN       (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovrval_MAX       (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovrval_DEF       (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Vsshipowergood_ovrval_HSH       (0x01283120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovren_OFF     (21)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovren_WID     ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovren_MSK     (0x00200000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovren_MIN     (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovren_MAX     (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovren_DEF     (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovren_HSH     (0x012A3120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovrval_OFF    (22)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovrval_WID    ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovrval_MSK    (0x00400000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovrval_MIN    (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovrval_MAX    (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovrval_DEF    (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_clkalign_complete_ovrval_HSH    (0x012C3120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovren_OFF              (23)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovren_WID              ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovren_MSK              (0x00800000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovren_MIN              (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovren_MAX              (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovren_DEF              (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovren_HSH              (0x012E3120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovrval_OFF             (24)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovrval_WID             ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovrval_MSK             (0x01000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovrval_MIN             (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovrval_MAX             (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovrval_DEF             (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_mdlllock_ovrval_HSH             (0x01303120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovren_OFF        (25)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovren_WID        ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovren_MSK        (0x02000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovren_MIN        (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovren_MAX        (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovren_DEF        (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovren_HSH        (0x01323120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovrval_OFF       (26)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovrval_WID       ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovrval_MSK       (0x04000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovrval_MIN       (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovrval_MAX       (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovrval_DEF       (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_compupdatedone_ovrval_HSH       (0x01343120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_scr_gvblock_ovrride_OFF         (27)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_scr_gvblock_ovrride_WID         ( 1)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_scr_gvblock_ovrride_MSK         (0x08000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_scr_gvblock_ovrride_MIN         (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_scr_gvblock_ovrride_MAX         (1) // 0x00000001
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_scr_gvblock_ovrride_DEF         (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_scr_gvblock_ovrride_HSH         (0x01363120)

  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Spare_OFF                       (28)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Spare_WID                       ( 4)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Spare_MSK                       (0xF0000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Spare_MIN                       (0)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Spare_MAX                       (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Spare_DEF                       (0x00000000)
  #define CCC0_GLOBAL_CR_PM_FSM_OVRD_0_Spare_HSH                       (0x04383120)

#define CCC0_GLOBAL_CR_PM_FSM_OVRD_1_REG                               (0x00003124)
//Duplicate of CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG

#define CCC0_GLOBAL_CR_COMP_OFFSET_0_REG                               (0x00003128)

  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_Spare_OFF                       ( 0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_Spare_WID                       (14)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_Spare_MSK                       (0x00003FFF)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_Spare_MIN                       (0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_Spare_MAX                       (16383) // 0x00003FFF
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_Spare_DEF                       (0x00000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_Spare_HSH                       (0x0E003128)

  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodepi_offset_OFF    (14)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodepi_offset_WID    ( 4)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodepi_offset_MSK    (0x0003C000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodepi_offset_MIN    (0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodepi_offset_MAX    (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodepi_offset_DEF    (0x00000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodepi_offset_HSH    (0x041C3128)

  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodewl_offset_OFF    (18)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodewl_offset_WID    ( 4)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodewl_offset_MSK    (0x003C0000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodewl_offset_MIN    (0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodewl_offset_MAX    (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodewl_offset_DEF    (0x00000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllcodewl_offset_HSH    (0x04243128)

  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllbwsel_offset_OFF     (22)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllbwsel_offset_WID     ( 4)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllbwsel_offset_MSK     (0x03C00000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllbwsel_offset_MIN     (0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllbwsel_offset_MAX     (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllbwsel_offset_DEF     (0x00000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_dllbwsel_offset_HSH     (0x042C3128)

  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_vdlllock_offset_OFF     (26)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_vdlllock_offset_WID     ( 6)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_vdlllock_offset_MSK     (0xFC000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_vdlllock_offset_MIN     (0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_vdlllock_offset_MAX     (63) // 0x0000003F
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_vdlllock_offset_DEF     (0x00000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_0_ccctop0_vdlllock_offset_HSH     (0x06343128)

#define CCC0_GLOBAL_CR_COMP_OFFSET_1_REG                               (0x0000312C)

  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_Spare_OFF                       ( 0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_Spare_WID                       (14)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_Spare_MSK                       (0x00003FFF)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_Spare_MIN                       (0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_Spare_MAX                       (16383) // 0x00003FFF
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_Spare_DEF                       (0x00000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_Spare_HSH                       (0x0E00312C)

  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodepi_offset_OFF    (14)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodepi_offset_WID    ( 4)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodepi_offset_MSK    (0x0003C000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodepi_offset_MIN    (0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodepi_offset_MAX    (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodepi_offset_DEF    (0x00000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodepi_offset_HSH    (0x041C312C)

  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodewl_offset_OFF    (18)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodewl_offset_WID    ( 4)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodewl_offset_MSK    (0x003C0000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodewl_offset_MIN    (0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodewl_offset_MAX    (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodewl_offset_DEF    (0x00000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllcodewl_offset_HSH    (0x0424312C)

  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllbwsel_offset_OFF     (22)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllbwsel_offset_WID     ( 4)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllbwsel_offset_MSK     (0x03C00000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllbwsel_offset_MIN     (0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllbwsel_offset_MAX     (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllbwsel_offset_DEF     (0x00000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_dllbwsel_offset_HSH     (0x042C312C)

  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_vdlllock_offset_OFF     (26)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_vdlllock_offset_WID     ( 6)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_vdlllock_offset_MSK     (0xFC000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_vdlllock_offset_MIN     (0)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_vdlllock_offset_MAX     (63) // 0x0000003F
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_vdlllock_offset_DEF     (0x00000000)
  #define CCC0_GLOBAL_CR_COMP_OFFSET_1_ccctop1_vdlllock_offset_HSH     (0x0634312C)

#define CCC0_GLOBAL_CR_TCOCOMP_0_REG                                   (0x00003130)

  #define CCC0_GLOBAL_CR_TCOCOMP_0_tcocomp_bonus_0_OFF                 ( 0)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_tcocomp_bonus_0_WID                 ( 2)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_tcocomp_bonus_0_MSK                 (0x00000003)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_tcocomp_bonus_0_MIN                 (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_tcocomp_bonus_0_MAX                 (3) // 0x00000003
  #define CCC0_GLOBAL_CR_TCOCOMP_0_tcocomp_bonus_0_DEF                 (0x00000000)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_tcocomp_bonus_0_HSH                 (0x02003130)

  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompp_top0_OFF      ( 2)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompp_top0_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompp_top0_MSK      (0x0000007C)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompp_top0_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompp_top0_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompp_top0_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompp_top0_HSH      (0x05043130)

  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompn_top0_OFF      ( 7)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompn_top0_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompn_top0_MSK      (0x00000F80)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompn_top0_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompn_top0_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompn_top0_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc7_txtcocompn_top0_HSH      (0x050E3130)

  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompp_top0_OFF      (12)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompp_top0_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompp_top0_MSK      (0x0001F000)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompp_top0_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompp_top0_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompp_top0_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompp_top0_HSH      (0x05183130)

  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompn_top0_OFF      (17)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompn_top0_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompn_top0_MSK      (0x003E0000)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompn_top0_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompn_top0_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompn_top0_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc6_txtcocompn_top0_HSH      (0x05223130)

  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompp_top0_OFF      (22)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompp_top0_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompp_top0_MSK      (0x07C00000)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompp_top0_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompp_top0_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompp_top0_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompp_top0_HSH      (0x052C3130)

  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompn_top0_OFF      (27)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompn_top0_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompn_top0_MSK      (0xF8000000)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompn_top0_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompn_top0_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompn_top0_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_0_ccctx_ccc5_txtcocompn_top0_HSH      (0x05363130)

#define CCC0_GLOBAL_CR_TCOCOMP_1_REG                                   (0x00003134)

  #define CCC0_GLOBAL_CR_TCOCOMP_1_tcocomp_bonus_1_OFF                 ( 0)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_tcocomp_bonus_1_WID                 ( 2)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_tcocomp_bonus_1_MSK                 (0x00000003)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_tcocomp_bonus_1_MIN                 (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_tcocomp_bonus_1_MAX                 (3) // 0x00000003
  #define CCC0_GLOBAL_CR_TCOCOMP_1_tcocomp_bonus_1_DEF                 (0x00000000)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_tcocomp_bonus_1_HSH                 (0x02003134)

  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompp_top1_OFF      ( 2)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompp_top1_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompp_top1_MSK      (0x0000007C)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompp_top1_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompp_top1_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompp_top1_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompp_top1_HSH      (0x05043134)

  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompn_top1_OFF      ( 7)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompn_top1_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompn_top1_MSK      (0x00000F80)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompn_top1_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompn_top1_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompn_top1_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc7_txtcocompn_top1_HSH      (0x050E3134)

  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompp_top1_OFF      (12)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompp_top1_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompp_top1_MSK      (0x0001F000)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompp_top1_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompp_top1_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompp_top1_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompp_top1_HSH      (0x05183134)

  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompn_top1_OFF      (17)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompn_top1_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompn_top1_MSK      (0x003E0000)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompn_top1_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompn_top1_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompn_top1_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc6_txtcocompn_top1_HSH      (0x05223134)

  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompp_top1_OFF      (22)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompp_top1_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompp_top1_MSK      (0x07C00000)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompp_top1_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompp_top1_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompp_top1_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompp_top1_HSH      (0x052C3134)

  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompn_top1_OFF      (27)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompn_top1_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompn_top1_MSK      (0xF8000000)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompn_top1_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompn_top1_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompn_top1_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_1_ccctx_ccc5_txtcocompn_top1_HSH      (0x05363134)

#define CCC0_GLOBAL_CR_TCOCOMP_2_REG                                   (0x00003138)

  #define CCC0_GLOBAL_CR_TCOCOMP_2_tcocomp_bonus_2_OFF                 ( 0)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_tcocomp_bonus_2_WID                 (12)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_tcocomp_bonus_2_MSK                 (0x00000FFF)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_tcocomp_bonus_2_MIN                 (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_tcocomp_bonus_2_MAX                 (4095) // 0x00000FFF
  #define CCC0_GLOBAL_CR_TCOCOMP_2_tcocomp_bonus_2_DEF                 (0x00000000)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_tcocomp_bonus_2_HSH                 (0x0C003138)

  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top1_OFF      (12)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top1_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top1_MSK      (0x0001F000)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top1_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top1_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top1_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top1_HSH      (0x05183138)

  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top1_OFF      (17)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top1_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top1_MSK      (0x003E0000)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top1_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top1_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top1_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top1_HSH      (0x05223138)

  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top0_OFF      (22)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top0_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top0_MSK      (0x07C00000)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top0_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top0_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top0_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompp_top0_HSH      (0x052C3138)

  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top0_OFF      (27)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top0_WID      ( 5)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top0_MSK      (0xF8000000)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top0_MIN      (0)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top0_MAX      (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top0_DEF      (0x0000001F)
  #define CCC0_GLOBAL_CR_TCOCOMP_2_ccctx_ccc8_txtcocompn_top0_HSH      (0x05363138)

#define CCC0_GLOBAL_CR_PMTIMER0_REG                                    (0x0000313C)

  #define CCC0_GLOBAL_CR_PMTIMER0_scr_init_done_timer_val_OFF          ( 0)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_init_done_timer_val_WID          ( 4)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_init_done_timer_val_MSK          (0x0000000F)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_init_done_timer_val_MIN          (0)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_init_done_timer_val_MAX          (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_init_done_timer_val_DEF          (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_init_done_timer_val_HSH          (0x0400313C)

  #define CCC0_GLOBAL_CR_PMTIMER0_scr_iobufacten_timer_val_OFF         ( 4)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_iobufacten_timer_val_WID         ( 4)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_iobufacten_timer_val_MSK         (0x000000F0)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_iobufacten_timer_val_MIN         (0)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_iobufacten_timer_val_MAX         (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_iobufacten_timer_val_DEF         (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_iobufacten_timer_val_HSH         (0x0408313C)

  #define CCC0_GLOBAL_CR_PMTIMER0_scr_start_clkalign_timer_val_OFF     ( 8)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_start_clkalign_timer_val_WID     ( 2)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_start_clkalign_timer_val_MSK     (0x00000300)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_start_clkalign_timer_val_MIN     (0)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_start_clkalign_timer_val_MAX     (3) // 0x00000003
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_start_clkalign_timer_val_DEF     (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_start_clkalign_timer_val_HSH     (0x0210313C)

  #define CCC0_GLOBAL_CR_PMTIMER0_scr_clkalignrsten_timer_val_OFF      (10)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_clkalignrsten_timer_val_WID      ( 3)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_clkalignrsten_timer_val_MSK      (0x00001C00)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_clkalignrsten_timer_val_MIN      (0)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_clkalignrsten_timer_val_MAX      (7) // 0x00000007
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_clkalignrsten_timer_val_DEF      (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_clkalignrsten_timer_val_HSH      (0x0314313C)

  #define CCC0_GLOBAL_CR_PMTIMER0_scr_dll_en_timer_val_OFF             (13)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_dll_en_timer_val_WID             (10)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_dll_en_timer_val_MSK             (0x007FE000)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_dll_en_timer_val_MIN             (0)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_dll_en_timer_val_MAX             (1023) // 0x000003FF
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_dll_en_timer_val_A0_DEF          (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_dll_en_timer_val_DEF             (0x00000068)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_dll_en_timer_val_HSH             (0x0A1A313C)

  #define CCC0_GLOBAL_CR_PMTIMER0_scr_ldoen_timer_val_OFF              (23)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_ldoen_timer_val_WID              ( 3)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_ldoen_timer_val_MSK              (0x03800000)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_ldoen_timer_val_MIN              (0)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_ldoen_timer_val_MAX              (7) // 0x00000007
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_ldoen_timer_val_DEF              (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_ldoen_timer_val_HSH              (0x032E313C)

  #define CCC0_GLOBAL_CR_PMTIMER0_scr_funcrst_timer_val_OFF            (26)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_funcrst_timer_val_WID            ( 4)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_funcrst_timer_val_MSK            (0x3C000000)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_funcrst_timer_val_MIN            (0)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_funcrst_timer_val_MAX            (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_funcrst_timer_val_DEF            (0x00000004)
  #define CCC0_GLOBAL_CR_PMTIMER0_scr_funcrst_timer_val_HSH            (0x0434313C)

  #define CCC0_GLOBAL_CR_PMTIMER0_Spare_OFF                            (30)
  #define CCC0_GLOBAL_CR_PMTIMER0_Spare_WID                            ( 2)
  #define CCC0_GLOBAL_CR_PMTIMER0_Spare_MSK                            (0xC0000000)
  #define CCC0_GLOBAL_CR_PMTIMER0_Spare_MIN                            (0)
  #define CCC0_GLOBAL_CR_PMTIMER0_Spare_MAX                            (3) // 0x00000003
  #define CCC0_GLOBAL_CR_PMTIMER0_Spare_DEF                            (0x00000000)
  #define CCC0_GLOBAL_CR_PMTIMER0_Spare_HSH                            (0x023C313C)

#define CCC0_GLOBAL_CR_PMTIMER1_REG                                    (0x00003140)

  #define CCC0_GLOBAL_CR_PMTIMER1_scr_initdone_dis_timer_val_OFF       ( 0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_initdone_dis_timer_val_WID       ( 2)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_initdone_dis_timer_val_MSK       (0x00000003)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_initdone_dis_timer_val_MIN       (0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_initdone_dis_timer_val_MAX       (3) // 0x00000003
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_initdone_dis_timer_val_A0_DEF    (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_initdone_dis_timer_val_DEF       (0x00000001)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_initdone_dis_timer_val_HSH       (0x02003140)

  #define CCC0_GLOBAL_CR_PMTIMER1_scr_ldo_dis_timer_val_OFF            ( 2)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_ldo_dis_timer_val_WID            ( 2)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_ldo_dis_timer_val_MSK            (0x0000000C)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_ldo_dis_timer_val_MIN            (0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_ldo_dis_timer_val_MAX            (3) // 0x00000003
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_ldo_dis_timer_val_A0_DEF         (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_ldo_dis_timer_val_DEF            (0x00000001)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_ldo_dis_timer_val_HSH            (0x02043140)

  #define CCC0_GLOBAL_CR_PMTIMER1_scr_dll_dis_timer_val_OFF            ( 4)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_dll_dis_timer_val_WID            ( 2)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_dll_dis_timer_val_MSK            (0x00000030)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_dll_dis_timer_val_MIN            (0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_dll_dis_timer_val_MAX            (3) // 0x00000003
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_dll_dis_timer_val_A0_DEF         (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_dll_dis_timer_val_DEF            (0x00000001)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_dll_dis_timer_val_HSH            (0x02083140)

  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pi_dis_timer_val_OFF             ( 6)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pi_dis_timer_val_WID             ( 2)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pi_dis_timer_val_MSK             (0x000000C0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pi_dis_timer_val_MIN             (0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pi_dis_timer_val_MAX             (3) // 0x00000003
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pi_dis_timer_val_A0_DEF          (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pi_dis_timer_val_DEF             (0x00000001)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pi_dis_timer_val_HSH             (0x020C3140)

  #define CCC0_GLOBAL_CR_PMTIMER1_scr_clkalignrst_dis_timer_val_OFF    ( 8)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_clkalignrst_dis_timer_val_WID    ( 2)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_clkalignrst_dis_timer_val_MSK    (0x00000300)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_clkalignrst_dis_timer_val_MIN    (0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_clkalignrst_dis_timer_val_MAX    (3) // 0x00000003
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_clkalignrst_dis_timer_val_A0_DEF (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_clkalignrst_dis_timer_val_DEF    (0x00000001)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_clkalignrst_dis_timer_val_HSH    (0x02103140)

  #define CCC0_GLOBAL_CR_PMTIMER1_scr_start_clkalign_dis_timer_val_OFF (10)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_start_clkalign_dis_timer_val_WID ( 2)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_start_clkalign_dis_timer_val_MSK (0x00000C00)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_start_clkalign_dis_timer_val_MIN (0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_start_clkalign_dis_timer_val_MAX (3) // 0x00000003
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_start_clkalign_dis_timer_val_A0_DEF (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_start_clkalign_dis_timer_val_DEF (0x00000001)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_start_clkalign_dis_timer_val_HSH (0x02143140)

  #define CCC0_GLOBAL_CR_PMTIMER1_scr_funcrst_dis_timer_val_OFF        (12)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_funcrst_dis_timer_val_WID        ( 2)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_funcrst_dis_timer_val_MSK        (0x00003000)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_funcrst_dis_timer_val_MIN        (0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_funcrst_dis_timer_val_MAX        (3) // 0x00000003
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_funcrst_dis_timer_val_DEF        (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_funcrst_dis_timer_val_HSH        (0x02183140)

  #define CCC0_GLOBAL_CR_PMTIMER1_scr_iobuf_dis_timer_val_OFF          (14)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_iobuf_dis_timer_val_WID          ( 4)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_iobuf_dis_timer_val_MSK          (0x0003C000)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_iobuf_dis_timer_val_MIN          (0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_iobuf_dis_timer_val_MAX          (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_iobuf_dis_timer_val_DEF          (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_iobuf_dis_timer_val_HSH          (0x041C3140)

  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pien_timer_val_OFF               (18)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pien_timer_val_WID               (11)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pien_timer_val_MSK               (0x1FFC0000)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pien_timer_val_MIN               (0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pien_timer_val_MAX               (2047) // 0x000007FF
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pien_timer_val_A0_DEF            (0x00000002)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pien_timer_val_DEF               (0x000001E0)
  #define CCC0_GLOBAL_CR_PMTIMER1_scr_pien_timer_val_HSH               (0x0B243140)

  #define CCC0_GLOBAL_CR_PMTIMER1_spare_OFF                            (29)
  #define CCC0_GLOBAL_CR_PMTIMER1_spare_WID                            ( 3)
  #define CCC0_GLOBAL_CR_PMTIMER1_spare_MSK                            (0xE0000000)
  #define CCC0_GLOBAL_CR_PMTIMER1_spare_MIN                            (0)
  #define CCC0_GLOBAL_CR_PMTIMER1_spare_MAX                            (7) // 0x00000007
  #define CCC0_GLOBAL_CR_PMTIMER1_spare_DEF                            (0x00000000)
  #define CCC0_GLOBAL_CR_PMTIMER1_spare_HSH                            (0x033A3140)

#define CCC0_GLOBAL_CR_COMPUPDT_DLY0_REG                               (0x00003144)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_setupdly_OFF                 ( 0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_setupdly_WID                 ( 3)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_setupdly_MSK                 (0x00000007)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_setupdly_MIN                 (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_setupdly_MAX                 (7) // 0x00000007
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_setupdly_DEF                 (0x00000004)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_setupdly_HSH                 (0x03003144)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_updtwidth_OFF                ( 3)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_updtwidth_WID                ( 5)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_updtwidth_MSK                (0x000000F8)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_updtwidth_MIN                (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_updtwidth_MAX                (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_updtwidth_DEF                (0x00000014)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_updtwidth_HSH                (0x05063144)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_holddly_OFF                  ( 8)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_holddly_WID                  ( 6)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_holddly_MSK                  (0x00003F00)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_holddly_MIN                  (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_holddly_MAX                  (63) // 0x0000003F
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_holddly_DEF                  (0x0000002C)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_cs_holddly_HSH                  (0x06103144)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_setupdly_OFF                 (14)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_setupdly_WID                 ( 3)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_setupdly_MSK                 (0x0001C000)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_setupdly_MIN                 (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_setupdly_MAX                 (7) // 0x00000007
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_setupdly_DEF                 (0x00000004)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_setupdly_HSH                 (0x031C3144)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_updtwidth_OFF                (17)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_updtwidth_WID                ( 3)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_updtwidth_MSK                (0x000E0000)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_updtwidth_MIN                (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_updtwidth_MAX                (7) // 0x00000007
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_updtwidth_DEF                (0x00000006)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_updtwidth_HSH                (0x03223144)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_holddly_OFF                  (20)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_holddly_WID                  ( 4)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_holddly_MSK                  (0x00F00000)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_holddly_MIN                  (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_holddly_MAX                  (15) // 0x0000000F
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_holddly_DEF                  (0x00000008)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_ca_holddly_HSH                  (0x04283144)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_spare_OFF                       (24)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_spare_WID                       ( 8)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_spare_MSK                       (0xFF000000)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_spare_MIN                       (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_spare_MAX                       (255) // 0x000000FF
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_spare_DEF                       (0x00000000)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY0_spare_HSH                       (0x08303144)

#define CCC0_GLOBAL_CR_COMPUPDT_DLY1_REG                               (0x00003148)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_setupdly_OFF                 ( 0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_setupdly_WID                 ( 3)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_setupdly_MSK                 (0x00000007)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_setupdly_MIN                 (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_setupdly_MAX                 (7) // 0x00000007
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_setupdly_DEF                 (0x00000004)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_setupdly_HSH                 (0x03003148)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_updtwidth_OFF                ( 3)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_updtwidth_WID                ( 5)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_updtwidth_MSK                (0x000000F8)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_updtwidth_MIN                (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_updtwidth_MAX                (31) // 0x0000001F
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_updtwidth_DEF                (0x00000014)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_updtwidth_HSH                (0x05063148)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_holddly_OFF                  ( 8)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_holddly_WID                  ( 6)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_holddly_MSK                  (0x00003F00)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_holddly_MIN                  (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_holddly_MAX                  (63) // 0x0000003F
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_holddly_DEF                  (0x0000001F)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_ck_holddly_HSH                  (0x06103148)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_compupdtovr_sel_OFF             (14)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_compupdtovr_sel_WID             (15)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_compupdtovr_sel_MSK             (0x1FFFC000)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_compupdtovr_sel_MIN             (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_compupdtovr_sel_MAX             (32767) // 0x00007FFF
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_compupdtovr_sel_DEF             (0x00000000)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_compupdtovr_sel_HSH             (0x0F1C3148)

  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_spare_OFF                       (29)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_spare_WID                       ( 3)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_spare_MSK                       (0xE0000000)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_spare_MIN                       (0)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_spare_MAX                       (7) // 0x00000007
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_spare_DEF                       (0x00000000)
  #define CCC0_GLOBAL_CR_COMPUPDT_DLY1_spare_HSH                       (0x033A3148)

#define CCC1_GLOBAL_IOLVRCTL_REG                                       (0x00003180)
//Duplicate of CCC0_GLOBAL_IOLVRCTL_REG

#define CCC1_GLOBAL_CR_VIEWCT_REG                                      (0x00003184)
//Duplicate of CCC0_GLOBAL_CR_VIEWCT_REG

#define CCC1_GLOBAL_IOLVRSLV_CTRL_REG                                  (0x00003188)
//Duplicate of CCC0_GLOBAL_IOLVRSLV_CTRL_REG

#define CCC1_GLOBAL_CR_DDREARLYCR_REG                                  (0x0000318C)
//Duplicate of DDRVTT0_CR_DDREARLY_RSTRDONE_REG

#define CCC1_GLOBAL_CR_VIEWCTL_REG                                     (0x00003190)
//Duplicate of CCC0_GLOBAL_CR_VIEWCTL_REG

#define CCC1_GLOBAL_CR_CCCDRXVREF_REG                                  (0x00003194)
//Duplicate of CCC0_GLOBAL_CR_CCCDRXVREF_REG

#define CCC1_GLOBAL_BONUS_0_REG                                        (0x00003198)
//Duplicate of CCC0_GLOBAL_BONUS_0_REG

#define CCC1_GLOBAL_BONUS_1_REG                                        (0x0000319C)
//Duplicate of CCC0_GLOBAL_BONUS_1_REG

#define CCC1_GLOBAL_CR_PM_FSM_OVRD_0_REG                               (0x000031A0)
//Duplicate of CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG

#define CCC1_GLOBAL_CR_PM_FSM_OVRD_1_REG                               (0x000031A4)
//Duplicate of CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG

#define CCC1_GLOBAL_CR_COMP_OFFSET_0_REG                               (0x000031A8)
//Duplicate of CCC0_GLOBAL_CR_COMP_OFFSET_0_REG

#define CCC1_GLOBAL_CR_COMP_OFFSET_1_REG                               (0x000031AC)
//Duplicate of CCC0_GLOBAL_CR_COMP_OFFSET_1_REG

#define CCC1_GLOBAL_CR_TCOCOMP_0_REG                                   (0x000031B0)
//Duplicate of CCC0_GLOBAL_CR_TCOCOMP_0_REG

#define CCC1_GLOBAL_CR_TCOCOMP_1_REG                                   (0x000031B4)
//Duplicate of CCC0_GLOBAL_CR_TCOCOMP_1_REG

#define CCC1_GLOBAL_CR_TCOCOMP_2_REG                                   (0x000031B8)
//Duplicate of CCC0_GLOBAL_CR_TCOCOMP_2_REG

#define CCC1_GLOBAL_CR_PMTIMER0_REG                                    (0x000031BC)
//Duplicate of CCC0_GLOBAL_CR_PMTIMER0_REG

#define CCC1_GLOBAL_CR_PMTIMER1_REG                                    (0x000031C0)
//Duplicate of CCC0_GLOBAL_CR_PMTIMER1_REG

#define CCC1_GLOBAL_CR_COMPUPDT_DLY0_REG                               (0x000031C4)
//Duplicate of CCC0_GLOBAL_CR_COMPUPDT_DLY0_REG

#define CCC1_GLOBAL_CR_COMPUPDT_DLY1_REG                               (0x000031C8)
//Duplicate of CCC0_GLOBAL_CR_COMPUPDT_DLY1_REG

#define CCC2_GLOBAL_IOLVRCTL_REG                                       (0x00003200)
//Duplicate of CCC0_GLOBAL_IOLVRCTL_REG

#define CCC2_GLOBAL_CR_VIEWCT_REG                                      (0x00003204)
//Duplicate of CCC0_GLOBAL_CR_VIEWCT_REG

#define CCC2_GLOBAL_IOLVRSLV_CTRL_REG                                  (0x00003208)
//Duplicate of CCC0_GLOBAL_IOLVRSLV_CTRL_REG

#define CCC2_GLOBAL_CR_DDREARLYCR_REG                                  (0x0000320C)
//Duplicate of DDRVTT0_CR_DDREARLY_RSTRDONE_REG

#define CCC2_GLOBAL_CR_VIEWCTL_REG                                     (0x00003210)
//Duplicate of CCC0_GLOBAL_CR_VIEWCTL_REG

#define CCC2_GLOBAL_CR_CCCDRXVREF_REG                                  (0x00003214)
//Duplicate of CCC0_GLOBAL_CR_CCCDRXVREF_REG

#define CCC2_GLOBAL_BONUS_0_REG                                        (0x00003218)
//Duplicate of CCC0_GLOBAL_BONUS_0_REG

#define CCC2_GLOBAL_BONUS_1_REG                                        (0x0000321C)
//Duplicate of CCC0_GLOBAL_BONUS_1_REG

#define CCC2_GLOBAL_CR_PM_FSM_OVRD_0_REG                               (0x00003220)
//Duplicate of CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG

#define CCC2_GLOBAL_CR_PM_FSM_OVRD_1_REG                               (0x00003224)
//Duplicate of CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG

#define CCC2_GLOBAL_CR_COMP_OFFSET_0_REG                               (0x00003228)
//Duplicate of CCC0_GLOBAL_CR_COMP_OFFSET_0_REG

#define CCC2_GLOBAL_CR_COMP_OFFSET_1_REG                               (0x0000322C)
//Duplicate of CCC0_GLOBAL_CR_COMP_OFFSET_1_REG

#define CCC2_GLOBAL_CR_TCOCOMP_0_REG                                   (0x00003230)
//Duplicate of CCC0_GLOBAL_CR_TCOCOMP_0_REG

#define CCC2_GLOBAL_CR_TCOCOMP_1_REG                                   (0x00003234)
//Duplicate of CCC0_GLOBAL_CR_TCOCOMP_1_REG

#define CCC2_GLOBAL_CR_TCOCOMP_2_REG                                   (0x00003238)
//Duplicate of CCC0_GLOBAL_CR_TCOCOMP_2_REG

#define CCC2_GLOBAL_CR_PMTIMER0_REG                                    (0x0000323C)
//Duplicate of CCC0_GLOBAL_CR_PMTIMER0_REG

#define CCC2_GLOBAL_CR_PMTIMER1_REG                                    (0x00003240)
//Duplicate of CCC0_GLOBAL_CR_PMTIMER1_REG

#define CCC2_GLOBAL_CR_COMPUPDT_DLY0_REG                               (0x00003244)
//Duplicate of CCC0_GLOBAL_CR_COMPUPDT_DLY0_REG

#define CCC2_GLOBAL_CR_COMPUPDT_DLY1_REG                               (0x00003248)
//Duplicate of CCC0_GLOBAL_CR_COMPUPDT_DLY1_REG

#define CCC3_GLOBAL_IOLVRCTL_REG                                       (0x00003280)
//Duplicate of CCC0_GLOBAL_IOLVRCTL_REG

#define CCC3_GLOBAL_CR_VIEWCT_REG                                      (0x00003284)
//Duplicate of CCC0_GLOBAL_CR_VIEWCT_REG

#define CCC3_GLOBAL_IOLVRSLV_CTRL_REG                                  (0x00003288)
//Duplicate of CCC0_GLOBAL_IOLVRSLV_CTRL_REG

#define CCC3_GLOBAL_CR_DDREARLYCR_REG                                  (0x0000328C)
//Duplicate of DDRVTT0_CR_DDREARLY_RSTRDONE_REG

#define CCC3_GLOBAL_CR_VIEWCTL_REG                                     (0x00003290)
//Duplicate of CCC0_GLOBAL_CR_VIEWCTL_REG

#define CCC3_GLOBAL_CR_CCCDRXVREF_REG                                  (0x00003294)
//Duplicate of CCC0_GLOBAL_CR_CCCDRXVREF_REG

#define CCC3_GLOBAL_BONUS_0_REG                                        (0x00003298)
//Duplicate of CCC0_GLOBAL_BONUS_0_REG

#define CCC3_GLOBAL_BONUS_1_REG                                        (0x0000329C)
//Duplicate of CCC0_GLOBAL_BONUS_1_REG

#define CCC3_GLOBAL_CR_PM_FSM_OVRD_0_REG                               (0x000032A0)
//Duplicate of CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG

#define CCC3_GLOBAL_CR_PM_FSM_OVRD_1_REG                               (0x000032A4)
//Duplicate of CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG

#define CCC3_GLOBAL_CR_COMP_OFFSET_0_REG                               (0x000032A8)
//Duplicate of CCC0_GLOBAL_CR_COMP_OFFSET_0_REG

#define CCC3_GLOBAL_CR_COMP_OFFSET_1_REG                               (0x000032AC)
//Duplicate of CCC0_GLOBAL_CR_COMP_OFFSET_1_REG

#define CCC3_GLOBAL_CR_TCOCOMP_0_REG                                   (0x000032B0)
//Duplicate of CCC0_GLOBAL_CR_TCOCOMP_0_REG

#define CCC3_GLOBAL_CR_TCOCOMP_1_REG                                   (0x000032B4)
//Duplicate of CCC0_GLOBAL_CR_TCOCOMP_1_REG

#define CCC3_GLOBAL_CR_TCOCOMP_2_REG                                   (0x000032B8)
//Duplicate of CCC0_GLOBAL_CR_TCOCOMP_2_REG

#define CCC3_GLOBAL_CR_PMTIMER0_REG                                    (0x000032BC)
//Duplicate of CCC0_GLOBAL_CR_PMTIMER0_REG

#define CCC3_GLOBAL_CR_PMTIMER1_REG                                    (0x000032C0)
//Duplicate of CCC0_GLOBAL_CR_PMTIMER1_REG

#define CCC3_GLOBAL_CR_COMPUPDT_DLY0_REG                               (0x000032C4)
//Duplicate of CCC0_GLOBAL_CR_COMPUPDT_DLY0_REG

#define CCC3_GLOBAL_CR_COMPUPDT_DLY1_REG                               (0x000032C8)
//Duplicate of CCC0_GLOBAL_CR_COMPUPDT_DLY1_REG

#define FLL_CMD_CFG_REG_REG                                            (0x00003300)

  #define FLL_CMD_CFG_REG_FLL_RATIO_OFF                                ( 0)
  #define FLL_CMD_CFG_REG_FLL_RATIO_WID                                ( 8)
  #define FLL_CMD_CFG_REG_FLL_RATIO_MSK                                (0x000000FF)
  #define FLL_CMD_CFG_REG_FLL_RATIO_MIN                                (0)
  #define FLL_CMD_CFG_REG_FLL_RATIO_MAX                                (255) // 0x000000FF
  #define FLL_CMD_CFG_REG_FLL_RATIO_DEF                                (0x00000012)
  #define FLL_CMD_CFG_REG_FLL_RATIO_HSH                                (0x08003300)

  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_OFF                          ( 8)
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_WID                          ( 1)
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_MSK                          (0x00000100)
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_MIN                          (0)
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_MAX                          (1) // 0x00000001
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_DEF                          (0x00000000)
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_HSH                          (0x01103300)

  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_OFF                          ( 9)
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_WID                          ( 1)
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_MSK                          (0x00000200)
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_MIN                          (0)
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_MAX                          (1) // 0x00000001
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_DEF                          (0x00000000)
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_HSH                          (0x01123300)

  #define FLL_CMD_CFG_REG_FLL_ENABLE_OFF                               (10)
  #define FLL_CMD_CFG_REG_FLL_ENABLE_WID                               ( 1)
  #define FLL_CMD_CFG_REG_FLL_ENABLE_MSK                               (0x00000400)
  #define FLL_CMD_CFG_REG_FLL_ENABLE_MIN                               (0)
  #define FLL_CMD_CFG_REG_FLL_ENABLE_MAX                               (1) // 0x00000001
  #define FLL_CMD_CFG_REG_FLL_ENABLE_DEF                               (0x00000000)
  #define FLL_CMD_CFG_REG_FLL_ENABLE_HSH                               (0x01143300)

  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_OFF                           (11)
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_WID                           ( 1)
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_MSK                           (0x00000800)
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_MIN                           (0)
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_MAX                           (1) // 0x00000001
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_DEF                           (0x00000000)
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_HSH                           (0x01163300)

  #define FLL_CMD_CFG_REG_BYPASS_AMPREF_FLT_OFF                        (12)
  #define FLL_CMD_CFG_REG_BYPASS_AMPREF_FLT_WID                        ( 1)
  #define FLL_CMD_CFG_REG_BYPASS_AMPREF_FLT_MSK                        (0x00001000)
  #define FLL_CMD_CFG_REG_BYPASS_AMPREF_FLT_MIN                        (0)
  #define FLL_CMD_CFG_REG_BYPASS_AMPREF_FLT_MAX                        (1) // 0x00000001
  #define FLL_CMD_CFG_REG_BYPASS_AMPREF_FLT_DEF                        (0x00000000)
  #define FLL_CMD_CFG_REG_BYPASS_AMPREF_FLT_HSH                        (0x01183300)

  #define FLL_CMD_CFG_REG_FLLVR_BYPASS_OFF                             (13)
  #define FLL_CMD_CFG_REG_FLLVR_BYPASS_WID                             ( 1)
  #define FLL_CMD_CFG_REG_FLLVR_BYPASS_MSK                             (0x00002000)
  #define FLL_CMD_CFG_REG_FLLVR_BYPASS_MIN                             (0)
  #define FLL_CMD_CFG_REG_FLLVR_BYPASS_MAX                             (1) // 0x00000001
  #define FLL_CMD_CFG_REG_FLLVR_BYPASS_DEF                             (0x00000000)
  #define FLL_CMD_CFG_REG_FLLVR_BYPASS_HSH                             (0x011A3300)

  #define FLL_CMD_CFG_REG_DCO_EN_HR_OFF                                (14)
  #define FLL_CMD_CFG_REG_DCO_EN_HR_WID                                ( 2)
  #define FLL_CMD_CFG_REG_DCO_EN_HR_MSK                                (0x0000C000)
  #define FLL_CMD_CFG_REG_DCO_EN_HR_MIN                                (0)
  #define FLL_CMD_CFG_REG_DCO_EN_HR_MAX                                (3) // 0x00000003
  #define FLL_CMD_CFG_REG_DCO_EN_HR_DEF                                (0x00000000)
  #define FLL_CMD_CFG_REG_DCO_EN_HR_HSH                                (0x021C3300)

  #define FLL_CMD_CFG_REG_DCO_CB_OFF                                   (16)
  #define FLL_CMD_CFG_REG_DCO_CB_WID                                   ( 3)
  #define FLL_CMD_CFG_REG_DCO_CB_MSK                                   (0x00070000)
  #define FLL_CMD_CFG_REG_DCO_CB_MIN                                   (0)
  #define FLL_CMD_CFG_REG_DCO_CB_MAX                                   (7) // 0x00000007
  #define FLL_CMD_CFG_REG_DCO_CB_DEF                                   (0x00000000)
  #define FLL_CMD_CFG_REG_DCO_CB_HSH                                   (0x03203300)

  #define FLL_CMD_CFG_REG_DCO_IREFTUNE_OFF                             (19)
  #define FLL_CMD_CFG_REG_DCO_IREFTUNE_WID                             ( 4)
  #define FLL_CMD_CFG_REG_DCO_IREFTUNE_MSK                             (0x00780000)
  #define FLL_CMD_CFG_REG_DCO_IREFTUNE_MIN                             (0)
  #define FLL_CMD_CFG_REG_DCO_IREFTUNE_MAX                             (15) // 0x0000000F
  #define FLL_CMD_CFG_REG_DCO_IREFTUNE_DEF                             (0x00000004)
  #define FLL_CMD_CFG_REG_DCO_IREFTUNE_HSH                             (0x04263300)

  #define FLL_CMD_CFG_REG_FASTRAMP_EN_OFF                              (23)
  #define FLL_CMD_CFG_REG_FASTRAMP_EN_WID                              ( 1)
  #define FLL_CMD_CFG_REG_FASTRAMP_EN_MSK                              (0x00800000)
  #define FLL_CMD_CFG_REG_FASTRAMP_EN_MIN                              (0)
  #define FLL_CMD_CFG_REG_FASTRAMP_EN_MAX                              (1) // 0x00000001
  #define FLL_CMD_CFG_REG_FASTRAMP_EN_DEF                              (0x00000001)
  #define FLL_CMD_CFG_REG_FASTRAMP_EN_HSH                              (0x012E3300)

  #define FLL_CMD_CFG_REG_LKR_ALWAYS_ON_OFF                            (24)
  #define FLL_CMD_CFG_REG_LKR_ALWAYS_ON_WID                            ( 1)
  #define FLL_CMD_CFG_REG_LKR_ALWAYS_ON_MSK                            (0x01000000)
  #define FLL_CMD_CFG_REG_LKR_ALWAYS_ON_MIN                            (0)
  #define FLL_CMD_CFG_REG_LKR_ALWAYS_ON_MAX                            (1) // 0x00000001
  #define FLL_CMD_CFG_REG_LKR_ALWAYS_ON_DEF                            (0x00000000)
  #define FLL_CMD_CFG_REG_LKR_ALWAYS_ON_HSH                            (0x01303300)

  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_OVRD_EN_OFF                  (25)
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_OVRD_EN_WID                  ( 1)
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_OVRD_EN_MSK                  (0x02000000)
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_OVRD_EN_MIN                  (0)
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_OVRD_EN_MAX                  (1) // 0x00000001
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_OVRD_EN_DEF                  (0x00000000)
  #define FLL_CMD_CFG_REG_FLL_OUT_CLK_REQ_OVRD_EN_HSH                  (0x01323300)

  #define FLL_CMD_CFG_REG_LDO_ENABLE_DLY_SEL_OFF                       (26)
  #define FLL_CMD_CFG_REG_LDO_ENABLE_DLY_SEL_WID                       ( 2)
  #define FLL_CMD_CFG_REG_LDO_ENABLE_DLY_SEL_MSK                       (0x0C000000)
  #define FLL_CMD_CFG_REG_LDO_ENABLE_DLY_SEL_MIN                       (0)
  #define FLL_CMD_CFG_REG_LDO_ENABLE_DLY_SEL_MAX                       (3) // 0x00000003
  #define FLL_CMD_CFG_REG_LDO_ENABLE_DLY_SEL_DEF                       (0x00000002)
  #define FLL_CMD_CFG_REG_LDO_ENABLE_DLY_SEL_HSH                       (0x02343300)

  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_OVRD_EN_OFF                  (28)
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_OVRD_EN_WID                  ( 1)
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_OVRD_EN_MSK                  (0x10000000)
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_OVRD_EN_MIN                  (0)
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_OVRD_EN_MAX                  (1) // 0x00000001
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_OVRD_EN_DEF                  (0x00000000)
  #define FLL_CMD_CFG_REG_FREQ_CHANGE_REQ_OVRD_EN_HSH                  (0x01383300)

  #define FLL_CMD_CFG_REG_LKR_STRENGTH_CFG_OFF                         (29)
  #define FLL_CMD_CFG_REG_LKR_STRENGTH_CFG_WID                         ( 1)
  #define FLL_CMD_CFG_REG_LKR_STRENGTH_CFG_MSK                         (0x20000000)
  #define FLL_CMD_CFG_REG_LKR_STRENGTH_CFG_MIN                         (0)
  #define FLL_CMD_CFG_REG_LKR_STRENGTH_CFG_MAX                         (1) // 0x00000001
  #define FLL_CMD_CFG_REG_LKR_STRENGTH_CFG_DEF                         (0x00000001)
  #define FLL_CMD_CFG_REG_LKR_STRENGTH_CFG_HSH                         (0x013A3300)

  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_OVRD_EN_OFF                   (30)
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_OVRD_EN_WID                   ( 1)
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_OVRD_EN_MSK                   (0x40000000)
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_OVRD_EN_MIN                   (0)
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_OVRD_EN_MAX                   (1) // 0x00000001
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_OVRD_EN_DEF                   (0x00000000)
  #define FLL_CMD_CFG_REG_FLL_LDO_ENABLE_OVRD_EN_HSH                   (0x013C3300)

  #define FLL_CMD_CFG_REG_SELEXTREF_OFF                                (31)
  #define FLL_CMD_CFG_REG_SELEXTREF_WID                                ( 1)
  #define FLL_CMD_CFG_REG_SELEXTREF_MSK                                (0x80000000)
  #define FLL_CMD_CFG_REG_SELEXTREF_MIN                                (0)
  #define FLL_CMD_CFG_REG_SELEXTREF_MAX                                (1) // 0x00000001
  #define FLL_CMD_CFG_REG_SELEXTREF_DEF                                (0x00000000)
  #define FLL_CMD_CFG_REG_SELEXTREF_HSH                                (0x013E3300)

#define FLL_STATIC_CFG_0_REG_REG                                       (0x00003304)

  #define FLL_STATIC_CFG_0_REG_FAST_CAL_WINDOW_VAL_OFF                 ( 0)
  #define FLL_STATIC_CFG_0_REG_FAST_CAL_WINDOW_VAL_WID                 ( 3)
  #define FLL_STATIC_CFG_0_REG_FAST_CAL_WINDOW_VAL_MSK                 (0x00000007)
  #define FLL_STATIC_CFG_0_REG_FAST_CAL_WINDOW_VAL_MIN                 (0)
  #define FLL_STATIC_CFG_0_REG_FAST_CAL_WINDOW_VAL_MAX                 (7) // 0x00000007
  #define FLL_STATIC_CFG_0_REG_FAST_CAL_WINDOW_VAL_DEF                 (0x00000001)
  #define FLL_STATIC_CFG_0_REG_FAST_CAL_WINDOW_VAL_HSH                 (0x03003304)

  #define FLL_STATIC_CFG_0_REG_SLOW_CAL_WINDOW_VAL_OFF                 ( 3)
  #define FLL_STATIC_CFG_0_REG_SLOW_CAL_WINDOW_VAL_WID                 ( 3)
  #define FLL_STATIC_CFG_0_REG_SLOW_CAL_WINDOW_VAL_MSK                 (0x00000038)
  #define FLL_STATIC_CFG_0_REG_SLOW_CAL_WINDOW_VAL_MIN                 (0)
  #define FLL_STATIC_CFG_0_REG_SLOW_CAL_WINDOW_VAL_MAX                 (7) // 0x00000007
  #define FLL_STATIC_CFG_0_REG_SLOW_CAL_WINDOW_VAL_DEF                 (0x00000002)
  #define FLL_STATIC_CFG_0_REG_SLOW_CAL_WINDOW_VAL_HSH                 (0x03063304)

  #define FLL_STATIC_CFG_0_REG_WAIT2FINALLOCK_SEL_OFF                  ( 6)
  #define FLL_STATIC_CFG_0_REG_WAIT2FINALLOCK_SEL_WID                  ( 2)
  #define FLL_STATIC_CFG_0_REG_WAIT2FINALLOCK_SEL_MSK                  (0x000000C0)
  #define FLL_STATIC_CFG_0_REG_WAIT2FINALLOCK_SEL_MIN                  (0)
  #define FLL_STATIC_CFG_0_REG_WAIT2FINALLOCK_SEL_MAX                  (3) // 0x00000003
  #define FLL_STATIC_CFG_0_REG_WAIT2FINALLOCK_SEL_DEF                  (0x00000000)
  #define FLL_STATIC_CFG_0_REG_WAIT2FINALLOCK_SEL_HSH                  (0x020C3304)

  #define FLL_STATIC_CFG_0_REG_SKIP_COARSE_CAL_OFF                     ( 8)
  #define FLL_STATIC_CFG_0_REG_SKIP_COARSE_CAL_WID                     ( 1)
  #define FLL_STATIC_CFG_0_REG_SKIP_COARSE_CAL_MSK                     (0x00000100)
  #define FLL_STATIC_CFG_0_REG_SKIP_COARSE_CAL_MIN                     (0)
  #define FLL_STATIC_CFG_0_REG_SKIP_COARSE_CAL_MAX                     (1) // 0x00000001
  #define FLL_STATIC_CFG_0_REG_SKIP_COARSE_CAL_DEF                     (0x00000000)
  #define FLL_STATIC_CFG_0_REG_SKIP_COARSE_CAL_HSH                     (0x01103304)

  #define FLL_STATIC_CFG_0_REG_FINE_CAL_ENABLE_OFF                     ( 9)
  #define FLL_STATIC_CFG_0_REG_FINE_CAL_ENABLE_WID                     ( 1)
  #define FLL_STATIC_CFG_0_REG_FINE_CAL_ENABLE_MSK                     (0x00000200)
  #define FLL_STATIC_CFG_0_REG_FINE_CAL_ENABLE_MIN                     (0)
  #define FLL_STATIC_CFG_0_REG_FINE_CAL_ENABLE_MAX                     (1) // 0x00000001
  #define FLL_STATIC_CFG_0_REG_FINE_CAL_ENABLE_DEF                     (0x00000001)
  #define FLL_STATIC_CFG_0_REG_FINE_CAL_ENABLE_HSH                     (0x01123304)

  #define FLL_STATIC_CFG_0_REG_RUNTIME_CAL_OFF                         (10)
  #define FLL_STATIC_CFG_0_REG_RUNTIME_CAL_WID                         ( 1)
  #define FLL_STATIC_CFG_0_REG_RUNTIME_CAL_MSK                         (0x00000400)
  #define FLL_STATIC_CFG_0_REG_RUNTIME_CAL_MIN                         (0)
  #define FLL_STATIC_CFG_0_REG_RUNTIME_CAL_MAX                         (1) // 0x00000001
  #define FLL_STATIC_CFG_0_REG_RUNTIME_CAL_DEF                         (0x00000001)
  #define FLL_STATIC_CFG_0_REG_RUNTIME_CAL_HSH                         (0x01143304)

  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_HI_OFF                       (11)
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_HI_WID                       ( 6)
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_HI_MSK                       (0x0001F800)
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_HI_MIN                       (0)
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_HI_MAX                       (63) // 0x0000003F
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_HI_DEF                       (0x00000003)
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_HI_HSH                       (0x06163304)

  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_LO_OFF                       (17)
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_LO_WID                       ( 6)
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_LO_MSK                       (0x007E0000)
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_LO_MIN                       (0)
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_LO_MAX                       (63) // 0x0000003F
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_LO_DEF                       (0x00000003)
  #define FLL_STATIC_CFG_0_REG_CAL_THRESH_LO_HSH                       (0x06223304)

  #define FLL_STATIC_CFG_0_REG_DCO_ON_IN_OFF_STATE_OFF                 (23)
  #define FLL_STATIC_CFG_0_REG_DCO_ON_IN_OFF_STATE_WID                 ( 1)
  #define FLL_STATIC_CFG_0_REG_DCO_ON_IN_OFF_STATE_MSK                 (0x00800000)
  #define FLL_STATIC_CFG_0_REG_DCO_ON_IN_OFF_STATE_MIN                 (0)
  #define FLL_STATIC_CFG_0_REG_DCO_ON_IN_OFF_STATE_MAX                 (1) // 0x00000001
  #define FLL_STATIC_CFG_0_REG_DCO_ON_IN_OFF_STATE_DEF                 (0x00000000)
  #define FLL_STATIC_CFG_0_REG_DCO_ON_IN_OFF_STATE_HSH                 (0x012E3304)

  #define FLL_STATIC_CFG_0_REG_COARSE_CAL_TIME_SEL_OFF                 (24)
  #define FLL_STATIC_CFG_0_REG_COARSE_CAL_TIME_SEL_WID                 ( 3)
  #define FLL_STATIC_CFG_0_REG_COARSE_CAL_TIME_SEL_MSK                 (0x07000000)
  #define FLL_STATIC_CFG_0_REG_COARSE_CAL_TIME_SEL_MIN                 (0)
  #define FLL_STATIC_CFG_0_REG_COARSE_CAL_TIME_SEL_MAX                 (7) // 0x00000007
  #define FLL_STATIC_CFG_0_REG_COARSE_CAL_TIME_SEL_DEF                 (0x00000000)
  #define FLL_STATIC_CFG_0_REG_COARSE_CAL_TIME_SEL_HSH                 (0x03303304)

  #define FLL_STATIC_CFG_0_REG_ISOLATION_MODE_ON_OFF                   (27)
  #define FLL_STATIC_CFG_0_REG_ISOLATION_MODE_ON_WID                   ( 1)
  #define FLL_STATIC_CFG_0_REG_ISOLATION_MODE_ON_MSK                   (0x08000000)
  #define FLL_STATIC_CFG_0_REG_ISOLATION_MODE_ON_MIN                   (0)
  #define FLL_STATIC_CFG_0_REG_ISOLATION_MODE_ON_MAX                   (1) // 0x00000001
  #define FLL_STATIC_CFG_0_REG_ISOLATION_MODE_ON_DEF                   (0x00000000)
  #define FLL_STATIC_CFG_0_REG_ISOLATION_MODE_ON_HSH                   (0x01363304)

  #define FLL_STATIC_CFG_0_REG_LDO_VREFSEL_OFF                         (28)
  #define FLL_STATIC_CFG_0_REG_LDO_VREFSEL_WID                         ( 4)
  #define FLL_STATIC_CFG_0_REG_LDO_VREFSEL_MSK                         (0xF0000000)
  #define FLL_STATIC_CFG_0_REG_LDO_VREFSEL_MIN                         (0)
  #define FLL_STATIC_CFG_0_REG_LDO_VREFSEL_MAX                         (15) // 0x0000000F
  #define FLL_STATIC_CFG_0_REG_LDO_VREFSEL_DEF                         (0x00000004)
  #define FLL_STATIC_CFG_0_REG_LDO_VREFSEL_HSH                         (0x04383304)

#define FLL_STATIC_CFG_1_REG_REG                                       (0x00003308)

  #define FLL_STATIC_CFG_1_REG_COARSECAL_CNTR_EN_OFF                   ( 0)
  #define FLL_STATIC_CFG_1_REG_COARSECAL_CNTR_EN_WID                   ( 4)
  #define FLL_STATIC_CFG_1_REG_COARSECAL_CNTR_EN_MSK                   (0x0000000F)
  #define FLL_STATIC_CFG_1_REG_COARSECAL_CNTR_EN_MIN                   (0)
  #define FLL_STATIC_CFG_1_REG_COARSECAL_CNTR_EN_MAX                   (15) // 0x0000000F
  #define FLL_STATIC_CFG_1_REG_COARSECAL_CNTR_EN_DEF                   (0x0000000F)
  #define FLL_STATIC_CFG_1_REG_COARSECAL_CNTR_EN_HSH                   (0x04003308)

  #define FLL_STATIC_CFG_1_REG_FINECAL_CNTR_EN_OFF                     ( 4)
  #define FLL_STATIC_CFG_1_REG_FINECAL_CNTR_EN_WID                     ( 4)
  #define FLL_STATIC_CFG_1_REG_FINECAL_CNTR_EN_MSK                     (0x000000F0)
  #define FLL_STATIC_CFG_1_REG_FINECAL_CNTR_EN_MIN                     (0)
  #define FLL_STATIC_CFG_1_REG_FINECAL_CNTR_EN_MAX                     (15) // 0x0000000F
  #define FLL_STATIC_CFG_1_REG_FINECAL_CNTR_EN_DEF                     (0x00000002)
  #define FLL_STATIC_CFG_1_REG_FINECAL_CNTR_EN_HSH                     (0x04083308)

  #define FLL_STATIC_CFG_1_REG_DELAY_FLLENABLE_OFF                     ( 8)
  #define FLL_STATIC_CFG_1_REG_DELAY_FLLENABLE_WID                     ( 3)
  #define FLL_STATIC_CFG_1_REG_DELAY_FLLENABLE_MSK                     (0x00000700)
  #define FLL_STATIC_CFG_1_REG_DELAY_FLLENABLE_MIN                     (0)
  #define FLL_STATIC_CFG_1_REG_DELAY_FLLENABLE_MAX                     (7) // 0x00000007
  #define FLL_STATIC_CFG_1_REG_DELAY_FLLENABLE_DEF                     (0x00000003)
  #define FLL_STATIC_CFG_1_REG_DELAY_FLLENABLE_HSH                     (0x03103308)

  #define FLL_STATIC_CFG_1_REG_RCOMPENSATION_CFG_OFF                   (11)
  #define FLL_STATIC_CFG_1_REG_RCOMPENSATION_CFG_WID                   ( 2)
  #define FLL_STATIC_CFG_1_REG_RCOMPENSATION_CFG_MSK                   (0x00001800)
  #define FLL_STATIC_CFG_1_REG_RCOMPENSATION_CFG_MIN                   (0)
  #define FLL_STATIC_CFG_1_REG_RCOMPENSATION_CFG_MAX                   (3) // 0x00000003
  #define FLL_STATIC_CFG_1_REG_RCOMPENSATION_CFG_DEF                   (0x00000003)
  #define FLL_STATIC_CFG_1_REG_RCOMPENSATION_CFG_HSH                   (0x02163308)

  #define FLL_STATIC_CFG_1_REG_REFCLK_DIVIDE_RATIO_SEL_OFF             (13)
  #define FLL_STATIC_CFG_1_REG_REFCLK_DIVIDE_RATIO_SEL_WID             ( 2)
  #define FLL_STATIC_CFG_1_REG_REFCLK_DIVIDE_RATIO_SEL_MSK             (0x00006000)
  #define FLL_STATIC_CFG_1_REG_REFCLK_DIVIDE_RATIO_SEL_MIN             (0)
  #define FLL_STATIC_CFG_1_REG_REFCLK_DIVIDE_RATIO_SEL_MAX             (3) // 0x00000003
  #define FLL_STATIC_CFG_1_REG_REFCLK_DIVIDE_RATIO_SEL_DEF             (0x00000001)
  #define FLL_STATIC_CFG_1_REG_REFCLK_DIVIDE_RATIO_SEL_HSH             (0x021A3308)

  #define FLL_STATIC_CFG_1_REG_COMPUTE_LENGTH_SEL_OFF                  (15)
  #define FLL_STATIC_CFG_1_REG_COMPUTE_LENGTH_SEL_WID                  ( 2)
  #define FLL_STATIC_CFG_1_REG_COMPUTE_LENGTH_SEL_MSK                  (0x00018000)
  #define FLL_STATIC_CFG_1_REG_COMPUTE_LENGTH_SEL_MIN                  (0)
  #define FLL_STATIC_CFG_1_REG_COMPUTE_LENGTH_SEL_MAX                  (3) // 0x00000003
  #define FLL_STATIC_CFG_1_REG_COMPUTE_LENGTH_SEL_DEF                  (0x00000000)
  #define FLL_STATIC_CFG_1_REG_COMPUTE_LENGTH_SEL_HSH                  (0x021E3308)

  #define FLL_STATIC_CFG_1_REG_DAC_SETTLE_LENGTH_SEL_OFF               (17)
  #define FLL_STATIC_CFG_1_REG_DAC_SETTLE_LENGTH_SEL_WID               ( 2)
  #define FLL_STATIC_CFG_1_REG_DAC_SETTLE_LENGTH_SEL_MSK               (0x00060000)
  #define FLL_STATIC_CFG_1_REG_DAC_SETTLE_LENGTH_SEL_MIN               (0)
  #define FLL_STATIC_CFG_1_REG_DAC_SETTLE_LENGTH_SEL_MAX               (3) // 0x00000003
  #define FLL_STATIC_CFG_1_REG_DAC_SETTLE_LENGTH_SEL_DEF               (0x00000000)
  #define FLL_STATIC_CFG_1_REG_DAC_SETTLE_LENGTH_SEL_HSH               (0x02223308)

  #define FLL_STATIC_CFG_1_REG_MISC_CFG_OFF                            (19)
  #define FLL_STATIC_CFG_1_REG_MISC_CFG_WID                            ( 3)
  #define FLL_STATIC_CFG_1_REG_MISC_CFG_MSK                            (0x00380000)
  #define FLL_STATIC_CFG_1_REG_MISC_CFG_MIN                            (0)
  #define FLL_STATIC_CFG_1_REG_MISC_CFG_MAX                            (7) // 0x00000007
  #define FLL_STATIC_CFG_1_REG_MISC_CFG_DEF                            (0x00000000)
  #define FLL_STATIC_CFG_1_REG_MISC_CFG_HSH                            (0x03263308)

  #define FLL_STATIC_CFG_1_REG_VREFSEL_FASTRAMP_OFF                    (22)
  #define FLL_STATIC_CFG_1_REG_VREFSEL_FASTRAMP_WID                    ( 4)
  #define FLL_STATIC_CFG_1_REG_VREFSEL_FASTRAMP_MSK                    (0x03C00000)
  #define FLL_STATIC_CFG_1_REG_VREFSEL_FASTRAMP_MIN                    (0)
  #define FLL_STATIC_CFG_1_REG_VREFSEL_FASTRAMP_MAX                    (15) // 0x0000000F
  #define FLL_STATIC_CFG_1_REG_VREFSEL_FASTRAMP_DEF                    (0x00000000)
  #define FLL_STATIC_CFG_1_REG_VREFSEL_FASTRAMP_HSH                    (0x042C3308)

  #define FLL_STATIC_CFG_1_REG_VSUPPLY_CFG_OFF                         (26)
  #define FLL_STATIC_CFG_1_REG_VSUPPLY_CFG_WID                         ( 2)
  #define FLL_STATIC_CFG_1_REG_VSUPPLY_CFG_MSK                         (0x0C000000)
  #define FLL_STATIC_CFG_1_REG_VSUPPLY_CFG_MIN                         (0)
  #define FLL_STATIC_CFG_1_REG_VSUPPLY_CFG_MAX                         (3) // 0x00000003
  #define FLL_STATIC_CFG_1_REG_VSUPPLY_CFG_DEF                         (0x00000000)
  #define FLL_STATIC_CFG_1_REG_VSUPPLY_CFG_HSH                         (0x02343308)

  #define FLL_STATIC_CFG_1_REG_REFCLK_INPUT_CLKGATE_OVRD_OFF           (28)
  #define FLL_STATIC_CFG_1_REG_REFCLK_INPUT_CLKGATE_OVRD_WID           ( 1)
  #define FLL_STATIC_CFG_1_REG_REFCLK_INPUT_CLKGATE_OVRD_MSK           (0x10000000)
  #define FLL_STATIC_CFG_1_REG_REFCLK_INPUT_CLKGATE_OVRD_MIN           (0)
  #define FLL_STATIC_CFG_1_REG_REFCLK_INPUT_CLKGATE_OVRD_MAX           (1) // 0x00000001
  #define FLL_STATIC_CFG_1_REG_REFCLK_INPUT_CLKGATE_OVRD_DEF           (0x00000000)
  #define FLL_STATIC_CFG_1_REG_REFCLK_INPUT_CLKGATE_OVRD_HSH           (0x01383308)

  #define FLL_STATIC_CFG_1_REG_SEQUENCE_FSM_CLKGATE_OVRD_OFF           (29)
  #define FLL_STATIC_CFG_1_REG_SEQUENCE_FSM_CLKGATE_OVRD_WID           ( 1)
  #define FLL_STATIC_CFG_1_REG_SEQUENCE_FSM_CLKGATE_OVRD_MSK           (0x20000000)
  #define FLL_STATIC_CFG_1_REG_SEQUENCE_FSM_CLKGATE_OVRD_MIN           (0)
  #define FLL_STATIC_CFG_1_REG_SEQUENCE_FSM_CLKGATE_OVRD_MAX           (1) // 0x00000001
  #define FLL_STATIC_CFG_1_REG_SEQUENCE_FSM_CLKGATE_OVRD_DEF           (0x00000000)
  #define FLL_STATIC_CFG_1_REG_SEQUENCE_FSM_CLKGATE_OVRD_HSH           (0x013A3308)

  #define FLL_STATIC_CFG_1_REG_REQACK_FSM_CLKGATE_OVRD_OFF             (30)
  #define FLL_STATIC_CFG_1_REG_REQACK_FSM_CLKGATE_OVRD_WID             ( 1)
  #define FLL_STATIC_CFG_1_REG_REQACK_FSM_CLKGATE_OVRD_MSK             (0x40000000)
  #define FLL_STATIC_CFG_1_REG_REQACK_FSM_CLKGATE_OVRD_MIN             (0)
  #define FLL_STATIC_CFG_1_REG_REQACK_FSM_CLKGATE_OVRD_MAX             (1) // 0x00000001
  #define FLL_STATIC_CFG_1_REG_REQACK_FSM_CLKGATE_OVRD_DEF             (0x00000000)
  #define FLL_STATIC_CFG_1_REG_REQACK_FSM_CLKGATE_OVRD_HSH             (0x013C3308)

  #define FLL_STATIC_CFG_1_REG_DCO_CODE_UPDATE_CLKGATE_OVRD_OFF        (31)
  #define FLL_STATIC_CFG_1_REG_DCO_CODE_UPDATE_CLKGATE_OVRD_WID        ( 1)
  #define FLL_STATIC_CFG_1_REG_DCO_CODE_UPDATE_CLKGATE_OVRD_MSK        (0x80000000)
  #define FLL_STATIC_CFG_1_REG_DCO_CODE_UPDATE_CLKGATE_OVRD_MIN        (0)
  #define FLL_STATIC_CFG_1_REG_DCO_CODE_UPDATE_CLKGATE_OVRD_MAX        (1) // 0x00000001
  #define FLL_STATIC_CFG_1_REG_DCO_CODE_UPDATE_CLKGATE_OVRD_DEF        (0x00000000)
  #define FLL_STATIC_CFG_1_REG_DCO_CODE_UPDATE_CLKGATE_OVRD_HSH        (0x013E3308)

#define FLL_DEBUG_CFG_REG_REG                                          (0x0000330C)

  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_OFF                       ( 0)
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_WID                       ( 1)
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_MSK                       (0x00000001)
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_MIN                       (0)
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_MAX                       (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_DEF                       (0x00000000)
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_HSH                       (0x0100330C)

  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_EN_OFF                    ( 1)
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_EN_WID                    ( 1)
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_EN_MSK                    (0x00000002)
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_EN_MIN                    (0)
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_EN_MAX                    (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_EN_DEF                    (0x00000000)
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_EN_HSH                    (0x0102330C)

  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_EN_OFF                    ( 2)
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_EN_WID                    ( 1)
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_EN_MSK                    (0x00000004)
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_EN_MIN                    (0)
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_EN_MAX                    (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_EN_DEF                    (0x00000000)
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_EN_HSH                    (0x0104330C)

  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_EN_OFF            ( 3)
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_EN_WID            ( 1)
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_EN_MSK            (0x00000008)
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_EN_MIN            (0)
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_EN_MAX            (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_EN_DEF            (0x00000000)
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_EN_HSH            (0x0106330C)

  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_EN_OFF                 ( 4)
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_EN_WID                 ( 1)
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_EN_MSK                 (0x00000010)
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_EN_MIN                 (0)
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_EN_MAX                 (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_EN_DEF                 (0x00000000)
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_EN_HSH                 (0x0108330C)

  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH0_OFF                     ( 5)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH0_WID                     ( 1)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH0_MSK                     (0x00000020)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH0_MIN                     (0)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH0_MAX                     (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH0_DEF                     (0x00000000)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH0_HSH                     (0x010A330C)

  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH1_OFF                     ( 6)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH1_WID                     ( 1)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH1_MSK                     (0x00000040)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH1_MIN                     (0)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH1_MAX                     (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH1_DEF                     (0x00000000)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_DFX_EN_CH1_HSH                     (0x010C330C)

  #define FLL_DEBUG_CFG_REG_VIEWANA_SEL_OFF                            ( 7)
  #define FLL_DEBUG_CFG_REG_VIEWANA_SEL_WID                            ( 4)
  #define FLL_DEBUG_CFG_REG_VIEWANA_SEL_MSK                            (0x00000780)
  #define FLL_DEBUG_CFG_REG_VIEWANA_SEL_MIN                            (0)
  #define FLL_DEBUG_CFG_REG_VIEWANA_SEL_MAX                            (15) // 0x0000000F
  #define FLL_DEBUG_CFG_REG_VIEWANA_SEL_DEF                            (0x00000000)
  #define FLL_DEBUG_CFG_REG_VIEWANA_SEL_HSH                            (0x040E330C)

  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH0_OFF                        (11)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH0_WID                        ( 4)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH0_MSK                        (0x00007800)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH0_MIN                        (0)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH0_MAX                        (15) // 0x0000000F
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH0_DEF                        (0x00000000)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH0_HSH                        (0x0416330C)

  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH1_OFF                        (15)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH1_WID                        ( 4)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH1_MSK                        (0x00078000)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH1_MIN                        (0)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH1_MAX                        (15) // 0x0000000F
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH1_DEF                        (0x00000000)
  #define FLL_DEBUG_CFG_REG_VIEWDIG_SEL_CH1_HSH                        (0x041E330C)

  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_EN_OFF                    (19)
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_EN_WID                    ( 1)
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_EN_MSK                    (0x00080000)
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_EN_MIN                    (0)
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_EN_MAX                    (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_EN_DEF                    (0x00000000)
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_EN_HSH                    (0x0126330C)

  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_TRIGGER_OFF               (20)
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_TRIGGER_WID               ( 1)
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_TRIGGER_MSK               (0x00100000)
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_TRIGGER_MIN               (0)
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_TRIGGER_MAX               (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_TRIGGER_DEF               (0x00000000)
  #define FLL_DEBUG_CFG_REG_SINGLE_STEP_MODE_TRIGGER_HSH               (0x0128330C)

  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_0_OFF                         (21)
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_0_WID                         ( 3)
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_0_MSK                         (0x00E00000)
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_0_MIN                         (0)
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_0_MAX                         (7) // 0x00000007
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_0_DEF                         (0x00000000)
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_0_HSH                         (0x032A330C)

  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_EN_OFF                    (24)
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_EN_WID                    ( 1)
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_EN_MSK                    (0x01000000)
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_EN_MIN                    (0)
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_EN_MAX                    (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_EN_DEF                    (0x00000000)
  #define FLL_DEBUG_CFG_REG_FLL_CORE_EN_OVRD_EN_HSH                    (0x0130330C)

  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_OFF                       (25)
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_WID                       ( 1)
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_MSK                       (0x02000000)
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_MIN                       (0)
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_MAX                       (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_DEF                       (0x00000000)
  #define FLL_DEBUG_CFG_REG_REG_WR_DONE_OVRD_HSH                       (0x0132330C)

  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_OFF                       (26)
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_WID                       ( 1)
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_MSK                       (0x04000000)
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_MIN                       (0)
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_MAX                       (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_DEF                       (0x00000000)
  #define FLL_DEBUG_CFG_REG_HV_PWR_GOOD_OVRD_HSH                       (0x0134330C)

  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_OFF               (27)
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_WID               ( 1)
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_MSK               (0x08000000)
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_MIN               (0)
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_MAX               (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_DEF               (0x00000000)
  #define FLL_DEBUG_CFG_REG_DLY_COUNTERS_BYPASS_OVRD_HSH               (0x0136330C)

  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_OFF                    (28)
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_WID                    ( 1)
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_MSK                    (0x10000000)
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_MIN                    (0)
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_MAX                    (1) // 0x00000001
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_DEF                    (0x00000000)
  #define FLL_DEBUG_CFG_REG_NODELAY_FLL_EN_OVRD_HSH                    (0x0138330C)

  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_1_OFF                         (29)
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_1_WID                         ( 3)
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_1_MSK                         (0xE0000000)
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_1_MIN                         (0)
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_1_MAX                         (7) // 0x00000007
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_1_DEF                         (0x00000000)
  #define FLL_DEBUG_CFG_REG_RESERVED_DBG_1_HSH                         (0x033A330C)

#define FLL_DYNAMIC_CFG_REG_REG                                        (0x00003310)

  #define FLL_DYNAMIC_CFG_REG_VBIAS_CODE_2FLLCORE_OVRD_OFF             ( 0)
  #define FLL_DYNAMIC_CFG_REG_VBIAS_CODE_2FLLCORE_OVRD_WID             (16)
  #define FLL_DYNAMIC_CFG_REG_VBIAS_CODE_2FLLCORE_OVRD_MSK             (0x0000FFFF)
  #define FLL_DYNAMIC_CFG_REG_VBIAS_CODE_2FLLCORE_OVRD_MIN             (0)
  #define FLL_DYNAMIC_CFG_REG_VBIAS_CODE_2FLLCORE_OVRD_MAX             (65535) // 0x0000FFFF
  #define FLL_DYNAMIC_CFG_REG_VBIAS_CODE_2FLLCORE_OVRD_DEF             (0x00000000)
  #define FLL_DYNAMIC_CFG_REG_VBIAS_CODE_2FLLCORE_OVRD_HSH             (0x10003310)

  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_ON_OFF                      (16)
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_ON_WID                      ( 1)
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_ON_MSK                      (0x00010000)
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_ON_MIN                      (0)
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_ON_MAX                      (1) // 0x00000001
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_ON_DEF                      (0x00000000)
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_ON_HSH                      (0x01203310)

  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_OFF_OFF                     (17)
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_OFF_WID                     ( 1)
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_OFF_MSK                     (0x00020000)
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_OFF_MIN                     (0)
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_OFF_MAX                     (1) // 0x00000001
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_OFF_DEF                     (0x00000000)
  #define FLL_DYNAMIC_CFG_REG_FORCE_OUTCLK_OFF_HSH                     (0x01223310)

  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_0_OFF                       (18)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_0_WID                       ( 4)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_0_MSK                       (0x003C0000)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_0_MIN                       (0)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_0_MAX                       (15) // 0x0000000F
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_0_DEF                       (0x00000000)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_0_HSH                       (0x04243310)

  #define FLL_DYNAMIC_CFG_REG_CAL_CODE_VALID_REG_OFF                   (22)
  #define FLL_DYNAMIC_CFG_REG_CAL_CODE_VALID_REG_WID                   ( 1)
  #define FLL_DYNAMIC_CFG_REG_CAL_CODE_VALID_REG_MSK                   (0x00400000)
  #define FLL_DYNAMIC_CFG_REG_CAL_CODE_VALID_REG_MIN                   (0)
  #define FLL_DYNAMIC_CFG_REG_CAL_CODE_VALID_REG_MAX                   (1) // 0x00000001
  #define FLL_DYNAMIC_CFG_REG_CAL_CODE_VALID_REG_DEF                   (0x00000000)
  #define FLL_DYNAMIC_CFG_REG_CAL_CODE_VALID_REG_HSH                   (0x012C3310)

  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_2_OFF                       (23)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_2_WID                       ( 1)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_2_MSK                       (0x00800000)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_2_MIN                       (0)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_2_MAX                       (1) // 0x00000001
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_2_DEF                       (0x00000000)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_2_HSH                       (0x012E3310)

  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_3_OFF                       (24)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_3_WID                       ( 1)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_3_MSK                       (0x01000000)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_3_MIN                       (0)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_3_MAX                       (1) // 0x00000001
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_3_DEF                       (0x00000000)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_3_HSH                       (0x01303310)

  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_1_OFF                       (25)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_1_WID                       ( 7)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_1_MSK                       (0xFE000000)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_1_MIN                       (0)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_1_MAX                       (127) // 0x0000007F
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_1_DEF                       (0x00000000)
  #define FLL_DYNAMIC_CFG_REG_RESERVED_DYN_1_HSH                       (0x07323310)

#define FLL_EXTIP_STAT_REG_REG                                         (0x00003314)

  #define FLL_EXTIP_STAT_REG_FLL_ENABLE_OFF                            ( 0)
  #define FLL_EXTIP_STAT_REG_FLL_ENABLE_WID                            ( 1)
  #define FLL_EXTIP_STAT_REG_FLL_ENABLE_MSK                            (0x00000001)
  #define FLL_EXTIP_STAT_REG_FLL_ENABLE_MIN                            (0)
  #define FLL_EXTIP_STAT_REG_FLL_ENABLE_MAX                            (1) // 0x00000001
  #define FLL_EXTIP_STAT_REG_FLL_ENABLE_DEF                            (0x00000000)
  #define FLL_EXTIP_STAT_REG_FLL_ENABLE_HSH                            (0x01003314)

  #define FLL_EXTIP_STAT_REG_DIST_RESET_B_OFF                          ( 1)
  #define FLL_EXTIP_STAT_REG_DIST_RESET_B_WID                          ( 1)
  #define FLL_EXTIP_STAT_REG_DIST_RESET_B_MSK                          (0x00000002)
  #define FLL_EXTIP_STAT_REG_DIST_RESET_B_MIN                          (0)
  #define FLL_EXTIP_STAT_REG_DIST_RESET_B_MAX                          (1) // 0x00000001
  #define FLL_EXTIP_STAT_REG_DIST_RESET_B_DEF                          (0x00000000)
  #define FLL_EXTIP_STAT_REG_DIST_RESET_B_HSH                          (0x01023314)

  #define FLL_EXTIP_STAT_REG_FLL_EARLY_LOCK_OFF                        ( 2)
  #define FLL_EXTIP_STAT_REG_FLL_EARLY_LOCK_WID                        ( 1)
  #define FLL_EXTIP_STAT_REG_FLL_EARLY_LOCK_MSK                        (0x00000004)
  #define FLL_EXTIP_STAT_REG_FLL_EARLY_LOCK_MIN                        (0)
  #define FLL_EXTIP_STAT_REG_FLL_EARLY_LOCK_MAX                        (1) // 0x00000001
  #define FLL_EXTIP_STAT_REG_FLL_EARLY_LOCK_DEF                        (0x00000000)
  #define FLL_EXTIP_STAT_REG_FLL_EARLY_LOCK_HSH                        (0x01043314)

  #define FLL_EXTIP_STAT_REG_FLL_FINAL_LOCK_OFF                        ( 3)
  #define FLL_EXTIP_STAT_REG_FLL_FINAL_LOCK_WID                        ( 1)
  #define FLL_EXTIP_STAT_REG_FLL_FINAL_LOCK_MSK                        (0x00000008)
  #define FLL_EXTIP_STAT_REG_FLL_FINAL_LOCK_MIN                        (0)
  #define FLL_EXTIP_STAT_REG_FLL_FINAL_LOCK_MAX                        (1) // 0x00000001
  #define FLL_EXTIP_STAT_REG_FLL_FINAL_LOCK_DEF                        (0x00000000)
  #define FLL_EXTIP_STAT_REG_FLL_FINAL_LOCK_HSH                        (0x01063314)

  #define FLL_EXTIP_STAT_REG_RAMP_DONE_OFF                             ( 4)
  #define FLL_EXTIP_STAT_REG_RAMP_DONE_WID                             ( 1)
  #define FLL_EXTIP_STAT_REG_RAMP_DONE_MSK                             (0x00000010)
  #define FLL_EXTIP_STAT_REG_RAMP_DONE_MIN                             (0)
  #define FLL_EXTIP_STAT_REG_RAMP_DONE_MAX                             (1) // 0x00000001
  #define FLL_EXTIP_STAT_REG_RAMP_DONE_DEF                             (0x00000000)
  #define FLL_EXTIP_STAT_REG_RAMP_DONE_HSH                             (0x01083314)

  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_REQ_OFF                       ( 5)
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_REQ_WID                       ( 1)
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_REQ_MSK                       (0x00000020)
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_REQ_MIN                       (0)
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_REQ_MAX                       (1) // 0x00000001
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_REQ_DEF                       (0x00000000)
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_REQ_HSH                       (0x010A3314)

  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_DONE_OFF                      ( 6)
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_DONE_WID                      ( 1)
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_DONE_MSK                      (0x00000040)
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_DONE_MIN                      (0)
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_DONE_MAX                      (1) // 0x00000001
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_DONE_DEF                      (0x00000000)
  #define FLL_EXTIP_STAT_REG_FREQ_CHANGE_DONE_HSH                      (0x010C3314)

  #define FLL_EXTIP_STAT_REG_REG_WRITES_DONE_OFF                       ( 7)
  #define FLL_EXTIP_STAT_REG_REG_WRITES_DONE_WID                       ( 1)
  #define FLL_EXTIP_STAT_REG_REG_WRITES_DONE_MSK                       (0x00000080)
  #define FLL_EXTIP_STAT_REG_REG_WRITES_DONE_MIN                       (0)
  #define FLL_EXTIP_STAT_REG_REG_WRITES_DONE_MAX                       (1) // 0x00000001
  #define FLL_EXTIP_STAT_REG_REG_WRITES_DONE_DEF                       (0x00000000)
  #define FLL_EXTIP_STAT_REG_REG_WRITES_DONE_HSH                       (0x010E3314)

  #define FLL_EXTIP_STAT_REG_VBIAS_CODE_2FLLCORE_OFF                   ( 8)
  #define FLL_EXTIP_STAT_REG_VBIAS_CODE_2FLLCORE_WID                   (16)
  #define FLL_EXTIP_STAT_REG_VBIAS_CODE_2FLLCORE_MSK                   (0x00FFFF00)
  #define FLL_EXTIP_STAT_REG_VBIAS_CODE_2FLLCORE_MIN                   (0)
  #define FLL_EXTIP_STAT_REG_VBIAS_CODE_2FLLCORE_MAX                   (65535) // 0x0000FFFF
  #define FLL_EXTIP_STAT_REG_VBIAS_CODE_2FLLCORE_DEF                   (0x00000000)
  #define FLL_EXTIP_STAT_REG_VBIAS_CODE_2FLLCORE_HSH                   (0x10103314)

  #define FLL_EXTIP_STAT_REG_FLL_RATIO_OFF                             (24)
  #define FLL_EXTIP_STAT_REG_FLL_RATIO_WID                             ( 8)
  #define FLL_EXTIP_STAT_REG_FLL_RATIO_MSK                             (0xFF000000)
  #define FLL_EXTIP_STAT_REG_FLL_RATIO_MIN                             (0)
  #define FLL_EXTIP_STAT_REG_FLL_RATIO_MAX                             (255) // 0x000000FF
  #define FLL_EXTIP_STAT_REG_FLL_RATIO_DEF                             (0x00000000)
  #define FLL_EXTIP_STAT_REG_FLL_RATIO_HSH                             (0x08303314)

#define FLL_DIAG_STAT_REG_REG                                          (0x00003318)

  #define FLL_DIAG_STAT_REG_FLL_REFCLK_REQ_OFF                         ( 0)
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_REQ_WID                         ( 1)
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_REQ_MSK                         (0x00000001)
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_REQ_MIN                         (0)
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_REQ_MAX                         (1) // 0x00000001
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_REQ_DEF                         (0x00000000)
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_REQ_HSH                         (0x01003318)

  #define FLL_DIAG_STAT_REG_FLL_REFCLK_ACK_OFF                         ( 1)
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_ACK_WID                         ( 1)
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_ACK_MSK                         (0x00000002)
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_ACK_MIN                         (0)
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_ACK_MAX                         (1) // 0x00000001
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_ACK_DEF                         (0x00000000)
  #define FLL_DIAG_STAT_REG_FLL_REFCLK_ACK_HSH                         (0x01023318)

  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_REQ_OFF                         ( 2)
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_REQ_WID                         ( 1)
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_REQ_MSK                         (0x00000004)
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_REQ_MIN                         (0)
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_REQ_MAX                         (1) // 0x00000001
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_REQ_DEF                         (0x00000000)
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_REQ_HSH                         (0x01043318)

  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_ACK_OFF                         ( 3)
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_ACK_WID                         ( 1)
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_ACK_MSK                         (0x00000008)
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_ACK_MIN                         (0)
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_ACK_MAX                         (1) // 0x00000001
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_ACK_DEF                         (0x00000000)
  #define FLL_DIAG_STAT_REG_FLL_OUTCLK_ACK_HSH                         (0x01063318)

  #define FLL_DIAG_STAT_REG_FLL_COUNTER_ERR_OFF                        ( 4)
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_ERR_WID                        (12)
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_ERR_MSK                        (0x0000FFF0)
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_ERR_MIN                        (-2048)
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_ERR_MAX                        (2047) // 0x000007FF
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_ERR_DEF                        (0x00000000)
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_ERR_HSH                        (0x8C083318)

  #define FLL_DIAG_STAT_REG_FLL_COUNTER_SUM_OFF                        (16)
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_SUM_WID                        (12)
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_SUM_MSK                        (0x0FFF0000)
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_SUM_MIN                        (0)
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_SUM_MAX                        (4095) // 0x00000FFF
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_SUM_DEF                        (0x00000000)
  #define FLL_DIAG_STAT_REG_FLL_COUNTER_SUM_HSH                        (0x0C203318)

  #define FLL_DIAG_STAT_REG_HV_PWR_GOOD_OFF                            (28)
  #define FLL_DIAG_STAT_REG_HV_PWR_GOOD_WID                            ( 1)
  #define FLL_DIAG_STAT_REG_HV_PWR_GOOD_MSK                            (0x10000000)
  #define FLL_DIAG_STAT_REG_HV_PWR_GOOD_MIN                            (0)
  #define FLL_DIAG_STAT_REG_HV_PWR_GOOD_MAX                            (1) // 0x00000001
  #define FLL_DIAG_STAT_REG_HV_PWR_GOOD_DEF                            (0x00000000)
  #define FLL_DIAG_STAT_REG_HV_PWR_GOOD_HSH                            (0x01383318)

  #define FLL_DIAG_STAT_REG_LDOEN_DLY_CNTR_DONE_OFF                    (29)
  #define FLL_DIAG_STAT_REG_LDOEN_DLY_CNTR_DONE_WID                    ( 1)
  #define FLL_DIAG_STAT_REG_LDOEN_DLY_CNTR_DONE_MSK                    (0x20000000)
  #define FLL_DIAG_STAT_REG_LDOEN_DLY_CNTR_DONE_MIN                    (0)
  #define FLL_DIAG_STAT_REG_LDOEN_DLY_CNTR_DONE_MAX                    (1) // 0x00000001
  #define FLL_DIAG_STAT_REG_LDOEN_DLY_CNTR_DONE_DEF                    (0x00000000)
  #define FLL_DIAG_STAT_REG_LDOEN_DLY_CNTR_DONE_HSH                    (0x013A3318)

  #define FLL_DIAG_STAT_REG_FASTRAMPDONE_CNTR_DONE_OFF                 (30)
  #define FLL_DIAG_STAT_REG_FASTRAMPDONE_CNTR_DONE_WID                 ( 1)
  #define FLL_DIAG_STAT_REG_FASTRAMPDONE_CNTR_DONE_MSK                 (0x40000000)
  #define FLL_DIAG_STAT_REG_FASTRAMPDONE_CNTR_DONE_MIN                 (0)
  #define FLL_DIAG_STAT_REG_FASTRAMPDONE_CNTR_DONE_MAX                 (1) // 0x00000001
  #define FLL_DIAG_STAT_REG_FASTRAMPDONE_CNTR_DONE_DEF                 (0x00000000)
  #define FLL_DIAG_STAT_REG_FASTRAMPDONE_CNTR_DONE_HSH                 (0x013C3318)

  #define FLL_DIAG_STAT_REG_LDO_RAMP_TIMER_DONE_OFF                    (31)
  #define FLL_DIAG_STAT_REG_LDO_RAMP_TIMER_DONE_WID                    ( 1)
  #define FLL_DIAG_STAT_REG_LDO_RAMP_TIMER_DONE_MSK                    (0x80000000)
  #define FLL_DIAG_STAT_REG_LDO_RAMP_TIMER_DONE_MIN                    (0)
  #define FLL_DIAG_STAT_REG_LDO_RAMP_TIMER_DONE_MAX                    (1) // 0x00000001
  #define FLL_DIAG_STAT_REG_LDO_RAMP_TIMER_DONE_DEF                    (0x00000000)
  #define FLL_DIAG_STAT_REG_LDO_RAMP_TIMER_DONE_HSH                    (0x013E3318)

#define FLL_DIAG_STAT_1_REG_REG                                        (0x0000331C)

  #define FLL_DIAG_STAT_1_REG_FLL_COUNTER_OVF_OFF                      ( 0)
  #define FLL_DIAG_STAT_1_REG_FLL_COUNTER_OVF_WID                      (10)
  #define FLL_DIAG_STAT_1_REG_FLL_COUNTER_OVF_MSK                      (0x000003FF)
  #define FLL_DIAG_STAT_1_REG_FLL_COUNTER_OVF_MIN                      (0)
  #define FLL_DIAG_STAT_1_REG_FLL_COUNTER_OVF_MAX                      (1023) // 0x000003FF
  #define FLL_DIAG_STAT_1_REG_FLL_COUNTER_OVF_DEF                      (0x00000000)
  #define FLL_DIAG_STAT_1_REG_FLL_COUNTER_OVF_HSH                      (0x0A00331C)

  #define FLL_DIAG_STAT_1_REG_COUNTER_OVERFLOW_STICKY_OFF              (10)
  #define FLL_DIAG_STAT_1_REG_COUNTER_OVERFLOW_STICKY_WID              ( 1)
  #define FLL_DIAG_STAT_1_REG_COUNTER_OVERFLOW_STICKY_MSK              (0x00000400)
  #define FLL_DIAG_STAT_1_REG_COUNTER_OVERFLOW_STICKY_MIN              (0)
  #define FLL_DIAG_STAT_1_REG_COUNTER_OVERFLOW_STICKY_MAX              (1) // 0x00000001
  #define FLL_DIAG_STAT_1_REG_COUNTER_OVERFLOW_STICKY_DEF              (0x00000000)
  #define FLL_DIAG_STAT_1_REG_COUNTER_OVERFLOW_STICKY_HSH              (0x0114331C)

  #define FLL_DIAG_STAT_1_REG_MEASUREMENT_ENABLE_DLY_VER_OFF           (11)
  #define FLL_DIAG_STAT_1_REG_MEASUREMENT_ENABLE_DLY_VER_WID           ( 1)
  #define FLL_DIAG_STAT_1_REG_MEASUREMENT_ENABLE_DLY_VER_MSK           (0x00000800)
  #define FLL_DIAG_STAT_1_REG_MEASUREMENT_ENABLE_DLY_VER_MIN           (0)
  #define FLL_DIAG_STAT_1_REG_MEASUREMENT_ENABLE_DLY_VER_MAX           (1) // 0x00000001
  #define FLL_DIAG_STAT_1_REG_MEASUREMENT_ENABLE_DLY_VER_DEF           (0x00000000)
  #define FLL_DIAG_STAT_1_REG_MEASUREMENT_ENABLE_DLY_VER_HSH           (0x0116331C)

  #define FLL_DIAG_STAT_1_REG_RESERVED_DIAG_STS_1_OFF                  (12)
  #define FLL_DIAG_STAT_1_REG_RESERVED_DIAG_STS_1_WID                  (20)
  #define FLL_DIAG_STAT_1_REG_RESERVED_DIAG_STS_1_MSK                  (0xFFFFF000)
  #define FLL_DIAG_STAT_1_REG_RESERVED_DIAG_STS_1_MIN                  (0)
  #define FLL_DIAG_STAT_1_REG_RESERVED_DIAG_STS_1_MAX                  (1048575) // 0x000FFFFF
  #define FLL_DIAG_STAT_1_REG_RESERVED_DIAG_STS_1_DEF                  (0x00000000)
  #define FLL_DIAG_STAT_1_REG_RESERVED_DIAG_STS_1_HSH                  (0x1418331C)

#define FLL_STATIC_CFG_2_REG_REG                                       (0x00003320)

  #define FLL_STATIC_CFG_2_REG_LDO_FAST_RAMP_TIME_SEL_OFF              ( 0)
  #define FLL_STATIC_CFG_2_REG_LDO_FAST_RAMP_TIME_SEL_WID              ( 2)
  #define FLL_STATIC_CFG_2_REG_LDO_FAST_RAMP_TIME_SEL_MSK              (0x00000003)
  #define FLL_STATIC_CFG_2_REG_LDO_FAST_RAMP_TIME_SEL_MIN              (0)
  #define FLL_STATIC_CFG_2_REG_LDO_FAST_RAMP_TIME_SEL_MAX              (3) // 0x00000003
  #define FLL_STATIC_CFG_2_REG_LDO_FAST_RAMP_TIME_SEL_DEF              (0x00000001)
  #define FLL_STATIC_CFG_2_REG_LDO_FAST_RAMP_TIME_SEL_HSH              (0x02003320)

  #define FLL_STATIC_CFG_2_REG_RESERVED_STATIC_CFG_2_OFF               ( 2)
  #define FLL_STATIC_CFG_2_REG_RESERVED_STATIC_CFG_2_WID               (30)
  #define FLL_STATIC_CFG_2_REG_RESERVED_STATIC_CFG_2_MSK               (0xFFFFFFFC)
  #define FLL_STATIC_CFG_2_REG_RESERVED_STATIC_CFG_2_MIN               (0)
  #define FLL_STATIC_CFG_2_REG_RESERVED_STATIC_CFG_2_MAX               (1073741823) // 0x3FFFFFFF
  #define FLL_STATIC_CFG_2_REG_RESERVED_STATIC_CFG_2_DEF               (0x00000000)
  #define FLL_STATIC_CFG_2_REG_RESERVED_STATIC_CFG_2_HSH               (0x1E043320)

#define FLL_DIAG_STAT_2_REG_REG                                        (0x00003324)

  #define FLL_DIAG_STAT_2_REG_RESERVED_DIAG_STS_2_OFF                  ( 0)
  #define FLL_DIAG_STAT_2_REG_RESERVED_DIAG_STS_2_WID                  (32)
  #define FLL_DIAG_STAT_2_REG_RESERVED_DIAG_STS_2_MSK                  (0xFFFFFFFF)
  #define FLL_DIAG_STAT_2_REG_RESERVED_DIAG_STS_2_MIN                  (0)
  #define FLL_DIAG_STAT_2_REG_RESERVED_DIAG_STS_2_MAX                  (4294967295) // 0xFFFFFFFF
  #define FLL_DIAG_STAT_2_REG_RESERVED_DIAG_STS_2_DEF                  (0x00000000)
  #define FLL_DIAG_STAT_2_REG_RESERVED_DIAG_STS_2_HSH                  (0x20003324)

#define DATA3_ECC_GLOBAL_CR_DDREARLYCR_REG                             (0x00003380)
//Duplicate of DDRVTT0_CR_DDREARLY_RSTRDONE_REG

#define DATA3_ECC_GLOBAL_CR_IOLVRCTL0_REG                              (0x00003384)
//Duplicate of DATA0_GLOBAL_CR_IOLVRCTL0_REG

#define DATA3_ECC_GLOBAL_CR_PGATE_PGCOMBOCTRL_REG                      (0x00003388)
//Duplicate of DATA0_GLOBAL_CR_PGATE_PGCOMBOCTRL_REG

#define DATA3_ECC_GLOBAL_CR_VTTDRVSEGCTL_REG                           (0x0000338C)
//Duplicate of DATA0_GLOBAL_CR_VTTDRVSEGCTL_REG

#define DATA3_ECC_GLOBAL_CR_VTTDRVSEGCTL1_REG                          (0x00003390)
//Duplicate of DATA0_GLOBAL_CR_VTTDRVSEGCTL1_REG

#define DATA3_ECC_GLOBAL_CR_VIEWCTL_REG                                (0x00003394)
//Duplicate of DATA0_GLOBAL_CR_VIEWCTL_REG

#define DATA3_ECC_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_REG                    (0x00003398)
//Duplicate of DATA0_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_REG

#define DATA3_ECC_GLOBAL_CR_PMTIMER0_REG                               (0x0000339C)
//Duplicate of DATA0_GLOBAL_CR_PMTIMER0_REG

#define DATA3_ECC_GLOBAL_CR_PMTIMER1_REG                               (0x000033A0)
//Duplicate of DATA0_GLOBAL_CR_PMTIMER1_REG

#define DATA4_ECC_GLOBAL_CR_DDREARLYCR_REG                             (0x00003400)
//Duplicate of DDRVTT0_CR_DDREARLY_RSTRDONE_REG

#define DATA4_ECC_GLOBAL_CR_IOLVRCTL0_REG                              (0x00003404)
//Duplicate of DATA0_GLOBAL_CR_IOLVRCTL0_REG

#define DATA4_ECC_GLOBAL_CR_PGATE_PGCOMBOCTRL_REG                      (0x00003408)
//Duplicate of DATA0_GLOBAL_CR_PGATE_PGCOMBOCTRL_REG

#define DATA4_ECC_GLOBAL_CR_VTTDRVSEGCTL_REG                           (0x0000340C)
//Duplicate of DATA0_GLOBAL_CR_VTTDRVSEGCTL_REG

#define DATA4_ECC_GLOBAL_CR_VTTDRVSEGCTL1_REG                          (0x00003410)
//Duplicate of DATA0_GLOBAL_CR_VTTDRVSEGCTL1_REG

#define DATA4_ECC_GLOBAL_CR_VIEWCTL_REG                                (0x00003414)
//Duplicate of DATA0_GLOBAL_CR_VIEWCTL_REG

#define DATA4_ECC_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_REG                    (0x00003418)
//Duplicate of DATA0_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_REG

#define DATA4_ECC_GLOBAL_CR_PMTIMER0_REG                               (0x0000341C)
//Duplicate of DATA0_GLOBAL_CR_PMTIMER0_REG

#define DATA4_ECC_GLOBAL_CR_PMTIMER1_REG                               (0x00003420)
//Duplicate of DATA0_GLOBAL_CR_PMTIMER1_REG

#define CH2CCC_CR_DDRCRPINSUSED_REG                                    (0x00003480)

  #define CH2CCC_CR_DDRCRPINSUSED_TxEn_OFF                             ( 0)
  #define CH2CCC_CR_DDRCRPINSUSED_TxEn_WID                             (15)
  #define CH2CCC_CR_DDRCRPINSUSED_TxEn_MSK                             (0x00007FFF)
  #define CH2CCC_CR_DDRCRPINSUSED_TxEn_MIN                             (0)
  #define CH2CCC_CR_DDRCRPINSUSED_TxEn_MAX                             (32767) // 0x00007FFF
  #define CH2CCC_CR_DDRCRPINSUSED_TxEn_DEF                             (0x00007FFF)
  #define CH2CCC_CR_DDRCRPINSUSED_TxEn_HSH                             (0x0F003480)

  #define CH2CCC_CR_DDRCRPINSUSED_CCCMuxSelect_OFF                     (15)
  #define CH2CCC_CR_DDRCRPINSUSED_CCCMuxSelect_WID                     ( 2)
  #define CH2CCC_CR_DDRCRPINSUSED_CCCMuxSelect_MSK                     (0x00018000)
  #define CH2CCC_CR_DDRCRPINSUSED_CCCMuxSelect_MIN                     (0)
  #define CH2CCC_CR_DDRCRPINSUSED_CCCMuxSelect_MAX                     (3) // 0x00000003
  #define CH2CCC_CR_DDRCRPINSUSED_CCCMuxSelect_DEF                     (0x00000000)
  #define CH2CCC_CR_DDRCRPINSUSED_CCCMuxSelect_HSH                     (0x021E3480)

  #define CH2CCC_CR_DDRCRPINSUSED_PiEn_OFF                             (17)
  #define CH2CCC_CR_DDRCRPINSUSED_PiEn_WID                             ( 5)
  #define CH2CCC_CR_DDRCRPINSUSED_PiEn_MSK                             (0x003E0000)
  #define CH2CCC_CR_DDRCRPINSUSED_PiEn_MIN                             (0)
  #define CH2CCC_CR_DDRCRPINSUSED_PiEn_MAX                             (31) // 0x0000001F
  #define CH2CCC_CR_DDRCRPINSUSED_PiEn_DEF                             (0x0000001F)
  #define CH2CCC_CR_DDRCRPINSUSED_PiEn_HSH                             (0x05223480)

  #define CH2CCC_CR_DDRCRPINSUSED_PiEnOvrd_OFF                         (22)
  #define CH2CCC_CR_DDRCRPINSUSED_PiEnOvrd_WID                         ( 1)
  #define CH2CCC_CR_DDRCRPINSUSED_PiEnOvrd_MSK                         (0x00400000)
  #define CH2CCC_CR_DDRCRPINSUSED_PiEnOvrd_MIN                         (0)
  #define CH2CCC_CR_DDRCRPINSUSED_PiEnOvrd_MAX                         (1) // 0x00000001
  #define CH2CCC_CR_DDRCRPINSUSED_PiEnOvrd_DEF                         (0x00000000)
  #define CH2CCC_CR_DDRCRPINSUSED_PiEnOvrd_HSH                         (0x012C3480)

  #define CH2CCC_CR_DDRCRPINSUSED_DdrCaSlwDlyBypass_A0_OFF                (23)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCaSlwDlyBypass_A0_WID                ( 1)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCaSlwDlyBypass_A0_MSK                (0x00800000)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCaSlwDlyBypass_A0_MIN                (0)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCaSlwDlyBypass_A0_MAX                (1) // 0x00000001
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCaSlwDlyBypass_A0_DEF                (0x00000000)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCaSlwDlyBypass_A0_HSH                (0x012E3480)

  #define CH2CCC_CR_DDRCRPINSUSED_AltMuxLp5DEn_OFF                     (23)
  #define CH2CCC_CR_DDRCRPINSUSED_AltMuxLp5DEn_WID                     ( 1)
  #define CH2CCC_CR_DDRCRPINSUSED_AltMuxLp5DEn_MSK                     (0x00800000)
  #define CH2CCC_CR_DDRCRPINSUSED_AltMuxLp5DEn_MIN                     (0)
  #define CH2CCC_CR_DDRCRPINSUSED_AltMuxLp5DEn_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_DDRCRPINSUSED_AltMuxLp5DEn_DEF                     (0x00000000)
  #define CH2CCC_CR_DDRCRPINSUSED_AltMuxLp5DEn_HSH                     (0x012E3480)

  #define CH2CCC_CR_DDRCRPINSUSED_DdrCtlSlwDlyBypass_A0_OFF               (24)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCtlSlwDlyBypass_A0_WID               ( 1)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCtlSlwDlyBypass_A0_MSK               (0x01000000)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCtlSlwDlyBypass_A0_MIN               (0)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCtlSlwDlyBypass_A0_MAX               (1) // 0x00000001
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCtlSlwDlyBypass_A0_DEF               (0x00000000)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrCtlSlwDlyBypass_A0_HSH               (0x01303480)

  #define CH2CCC_CR_DDRCRPINSUSED_DdrClkSlwDlyBypass_A0_OFF               (25)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrClkSlwDlyBypass_A0_WID               ( 1)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrClkSlwDlyBypass_A0_MSK               (0x02000000)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrClkSlwDlyBypass_A0_MIN               (0)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrClkSlwDlyBypass_A0_MAX               (1) // 0x00000001
  #define CH2CCC_CR_DDRCRPINSUSED_DdrClkSlwDlyBypass_A0_DEF               (0x00000000)
  #define CH2CCC_CR_DDRCRPINSUSED_DdrClkSlwDlyBypass_A0_HSH               (0x01323480)

  #define CH2CCC_CR_DDRCRPINSUSED_Spare_OFF                            (24)
  #define CH2CCC_CR_DDRCRPINSUSED_Spare_WID                            ( 2)
  #define CH2CCC_CR_DDRCRPINSUSED_Spare_MSK                            (0x03000000)
  #define CH2CCC_CR_DDRCRPINSUSED_Spare_MIN                            (0)
  #define CH2CCC_CR_DDRCRPINSUSED_Spare_MAX                            (3) // 0x00000003
  #define CH2CCC_CR_DDRCRPINSUSED_Spare_DEF                            (0x00000000)
  #define CH2CCC_CR_DDRCRPINSUSED_Spare_HSH                            (0x02303480)

  #define CH2CCC_CR_DDRCRPINSUSED_Gear1_OFF                            (26)
  #define CH2CCC_CR_DDRCRPINSUSED_Gear1_WID                            ( 1)
  #define CH2CCC_CR_DDRCRPINSUSED_Gear1_MSK                            (0x04000000)
  #define CH2CCC_CR_DDRCRPINSUSED_Gear1_MIN                            (0)
  #define CH2CCC_CR_DDRCRPINSUSED_Gear1_MAX                            (1) // 0x00000001
  #define CH2CCC_CR_DDRCRPINSUSED_Gear1_DEF                            (0x00000001)
  #define CH2CCC_CR_DDRCRPINSUSED_Gear1_HSH                            (0x01343480)

  #define CH2CCC_CR_DDRCRPINSUSED_TimerXXClk_OFF                       (27)
  #define CH2CCC_CR_DDRCRPINSUSED_TimerXXClk_WID                       ( 3)
  #define CH2CCC_CR_DDRCRPINSUSED_TimerXXClk_MSK                       (0x38000000)
  #define CH2CCC_CR_DDRCRPINSUSED_TimerXXClk_MIN                       (0)
  #define CH2CCC_CR_DDRCRPINSUSED_TimerXXClk_MAX                       (7) // 0x00000007
  #define CH2CCC_CR_DDRCRPINSUSED_TimerXXClk_DEF                       (0x00000000)
  #define CH2CCC_CR_DDRCRPINSUSED_TimerXXClk_HSH                       (0x03363480)

  #define CH2CCC_CR_DDRCRPINSUSED_KeepXXClkOn_OFF                      (30)
  #define CH2CCC_CR_DDRCRPINSUSED_KeepXXClkOn_WID                      ( 1)
  #define CH2CCC_CR_DDRCRPINSUSED_KeepXXClkOn_MSK                      (0x40000000)
  #define CH2CCC_CR_DDRCRPINSUSED_KeepXXClkOn_MIN                      (0)
  #define CH2CCC_CR_DDRCRPINSUSED_KeepXXClkOn_MAX                      (1) // 0x00000001
  #define CH2CCC_CR_DDRCRPINSUSED_KeepXXClkOn_DEF                      (0x00000000)
  #define CH2CCC_CR_DDRCRPINSUSED_KeepXXClkOn_HSH                      (0x013C3480)

  #define CH2CCC_CR_DDRCRPINSUSED_Gear4_OFF                            (31)
  #define CH2CCC_CR_DDRCRPINSUSED_Gear4_WID                            ( 1)
  #define CH2CCC_CR_DDRCRPINSUSED_Gear4_MSK                            (0x80000000)
  #define CH2CCC_CR_DDRCRPINSUSED_Gear4_MIN                            (0)
  #define CH2CCC_CR_DDRCRPINSUSED_Gear4_MAX                            (1) // 0x00000001
  #define CH2CCC_CR_DDRCRPINSUSED_Gear4_DEF                            (0x00000000)
  #define CH2CCC_CR_DDRCRPINSUSED_Gear4_HSH                            (0x013E3480)

#define CH2CCC_CR_CCCRSTCTL_A0_REG                                     (0x000034B8)
#define CH2CCC_CR_CCCRSTCTL_REG                                        (0x00003484)

  #define CH2CCC_CR_CCCRSTCTL_IamclkP_OFF                              ( 0)
  #define CH2CCC_CR_CCCRSTCTL_IamclkP_WID                              ( 4)
  #define CH2CCC_CR_CCCRSTCTL_IamclkP_MSK                              (0x0000000F)
  #define CH2CCC_CR_CCCRSTCTL_IamclkP_MIN                              (0)
  #define CH2CCC_CR_CCCRSTCTL_IamclkP_MAX                              (15) // 0x0000000F
  #define CH2CCC_CR_CCCRSTCTL_IamclkP_DEF                              (0x00000000)
  #define CH2CCC_CR_CCCRSTCTL_IamclkP_HSH                              (0x04003484)

  #define CH2CCC_CR_CCCRSTCTL_ccc_bonus_OFF                            ( 4)
  #define CH2CCC_CR_CCCRSTCTL_ccc_bonus_WID                            (27)
  #define CH2CCC_CR_CCCRSTCTL_ccc_bonus_MSK                            (0x7FFFFFF0)
  #define CH2CCC_CR_CCCRSTCTL_ccc_bonus_MIN                            (0)
  #define CH2CCC_CR_CCCRSTCTL_ccc_bonus_MAX                            (134217727) // 0x07FFFFFF
  #define CH2CCC_CR_CCCRSTCTL_ccc_bonus_DEF                            (0x00000000)
  #define CH2CCC_CR_CCCRSTCTL_ccc_bonus_HSH                            (0x1B083484)

  #define CH2CCC_CR_CCCRSTCTL_cccrst_OFF                               (31)
  #define CH2CCC_CR_CCCRSTCTL_cccrst_WID                               ( 1)
  #define CH2CCC_CR_CCCRSTCTL_cccrst_MSK                               (0x80000000)
  #define CH2CCC_CR_CCCRSTCTL_cccrst_MIN                               (0)
  #define CH2CCC_CR_CCCRSTCTL_cccrst_MAX                               (1) // 0x00000001
  #define CH2CCC_CR_CCCRSTCTL_cccrst_DEF                               (0x00000001)
  #define CH2CCC_CR_CCCRSTCTL_cccrst_HSH                               (0x013E3484)

  #define CH2CCC_CR_CCCRSTCTL_pmrst_override_A0_OFF                       (29)
  #define CH2CCC_CR_CCCRSTCTL_pmrst_override_A0_WID                       ( 1)
  #define CH2CCC_CR_CCCRSTCTL_pmrst_override_A0_MSK                       (0x20000000)
  #define CH2CCC_CR_CCCRSTCTL_pmrst_override_A0_MIN                       (0)
  #define CH2CCC_CR_CCCRSTCTL_pmrst_override_A0_MAX                       (1) // 0x00000001
  #define CH2CCC_CR_CCCRSTCTL_pmrst_override_A0_DEF                       (0x00000000)
  #define CH2CCC_CR_CCCRSTCTL_pmrst_override_A0_HSH                       (0x013A34B8)

  #define CH2CCC_CR_CCCRSTCTL_cfgrst_override_A0_OFF                      (30)
  #define CH2CCC_CR_CCCRSTCTL_cfgrst_override_A0_WID                      ( 1)
  #define CH2CCC_CR_CCCRSTCTL_cfgrst_override_A0_MSK                      (0x40000000)
  #define CH2CCC_CR_CCCRSTCTL_cfgrst_override_A0_MIN                      (0)
  #define CH2CCC_CR_CCCRSTCTL_cfgrst_override_A0_MAX                      (1) // 0x00000001
  #define CH2CCC_CR_CCCRSTCTL_cfgrst_override_A0_DEF                      (0x00000000)
  #define CH2CCC_CR_CCCRSTCTL_cfgrst_override_A0_HSH                      (0x013C34B8)

#define CH2CCC_CR_PICODE0_A0_REG                                       (0x00003484)
#define CH2CCC_CR_PICODE0_REG                                          (0x00003488)

  #define CH2CCC_CR_PICODE0_mdll_cmn_pi0code_OFF                       ( 0)
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi0code_WID                       ( 8)
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi0code_MSK                       (0x000000FF)
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi0code_MIN                       (0)
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi0code_MAX                       (255) // 0x000000FF
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi0code_DEF                       (0x00000000)
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi0code_HSH                       (0x08003488)

  #define CH2CCC_CR_PICODE0_reserved1_OFF                              ( 8)
  #define CH2CCC_CR_PICODE0_reserved1_WID                              ( 8)
  #define CH2CCC_CR_PICODE0_reserved1_MSK                              (0x0000FF00)
  #define CH2CCC_CR_PICODE0_reserved1_MIN                              (0)
  #define CH2CCC_CR_PICODE0_reserved1_MAX                              (255) // 0x000000FF
  #define CH2CCC_CR_PICODE0_reserved1_DEF                              (0x00000000)
  #define CH2CCC_CR_PICODE0_reserved1_HSH                              (0x08103488)

  #define CH2CCC_CR_PICODE0_mdll_cmn_pi1code_OFF                       (16)
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi1code_WID                       (10)
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi1code_MSK                       (0x03FF0000)
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi1code_MIN                       (0)
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi1code_MAX                       (1023) // 0x000003FF
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi1code_DEF                       (0x00000000)
  #define CH2CCC_CR_PICODE0_mdll_cmn_pi1code_HSH                       (0x0A203488)

  #define CH2CCC_CR_PICODE0_reserved0_OFF                              (26)
  #define CH2CCC_CR_PICODE0_reserved0_WID                              ( 6)
  #define CH2CCC_CR_PICODE0_reserved0_MSK                              (0xFC000000)
  #define CH2CCC_CR_PICODE0_reserved0_MIN                              (0)
  #define CH2CCC_CR_PICODE0_reserved0_MAX                              (63) // 0x0000003F
  #define CH2CCC_CR_PICODE0_reserved0_DEF                              (0x00000000)
  #define CH2CCC_CR_PICODE0_reserved0_HSH                              (0x06343488)

#define CH2CCC_CR_PICODE1_A0_REG                                       (0x00003488)
#define CH2CCC_CR_PICODE1_REG                                          (0x0000348C)

  #define CH2CCC_CR_PICODE1_mdll_cmn_pi2code_OFF                       ( 0)
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi2code_WID                       (10)
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi2code_MSK                       (0x000003FF)
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi2code_MIN                       (0)
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi2code_MAX                       (1023) // 0x000003FF
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi2code_DEF                       (0x00000000)
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH                       (0x0A00348C)

  #define CH2CCC_CR_PICODE1_reserved3_OFF                              (10)
  #define CH2CCC_CR_PICODE1_reserved3_WID                              ( 6)
  #define CH2CCC_CR_PICODE1_reserved3_MSK                              (0x0000FC00)
  #define CH2CCC_CR_PICODE1_reserved3_MIN                              (0)
  #define CH2CCC_CR_PICODE1_reserved3_MAX                              (63) // 0x0000003F
  #define CH2CCC_CR_PICODE1_reserved3_DEF                              (0x00000000)
  #define CH2CCC_CR_PICODE1_reserved3_HSH                              (0x0614348C)

  #define CH2CCC_CR_PICODE1_mdll_cmn_pi3code_OFF                       (16)
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi3code_WID                       (10)
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi3code_MSK                       (0x03FF0000)
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi3code_MIN                       (0)
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi3code_MAX                       (1023) // 0x000003FF
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi3code_DEF                       (0x00000000)
  #define CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH                       (0x0A20348C)

  #define CH2CCC_CR_PICODE1_reserved2_OFF                              (26)
  #define CH2CCC_CR_PICODE1_reserved2_WID                              ( 6)
  #define CH2CCC_CR_PICODE1_reserved2_MSK                              (0xFC000000)
  #define CH2CCC_CR_PICODE1_reserved2_MIN                              (0)
  #define CH2CCC_CR_PICODE1_reserved2_MAX                              (63) // 0x0000003F
  #define CH2CCC_CR_PICODE1_reserved2_DEF                              (0x00000000)
  #define CH2CCC_CR_PICODE1_reserved2_HSH                              (0x0634348C)

#define CH2CCC_CR_PICODE2_A0_REG                                       (0x0000348C)
#define CH2CCC_CR_PICODE2_REG                                          (0x00003490)

  #define CH2CCC_CR_PICODE2_mdll_cmn_pi4code_OFF                       ( 0)
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi4code_WID                       (10)
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi4code_MSK                       (0x000003FF)
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi4code_MIN                       (0)
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi4code_MAX                       (1023) // 0x000003FF
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi4code_DEF                       (0x00000000)
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi4code_HSH                       (0x0A003490)

  #define CH2CCC_CR_PICODE2_reserved5_OFF                              (10)
  #define CH2CCC_CR_PICODE2_reserved5_WID                              ( 6)
  #define CH2CCC_CR_PICODE2_reserved5_MSK                              (0x0000FC00)
  #define CH2CCC_CR_PICODE2_reserved5_MIN                              (0)
  #define CH2CCC_CR_PICODE2_reserved5_MAX                              (63) // 0x0000003F
  #define CH2CCC_CR_PICODE2_reserved5_DEF                              (0x00000000)
  #define CH2CCC_CR_PICODE2_reserved5_HSH                              (0x06143490)

  #define CH2CCC_CR_PICODE2_mdll_cmn_pi5code_OFF                       (16)
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi5code_WID                       ( 8)
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi5code_MSK                       (0x00FF0000)
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi5code_MIN                       (0)
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi5code_MAX                       (255) // 0x000000FF
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi5code_DEF                       (0x00000000)
  #define CH2CCC_CR_PICODE2_mdll_cmn_pi5code_HSH                       (0x08203490)

  #define CH2CCC_CR_PICODE2_reserved4_OFF                              (24)
  #define CH2CCC_CR_PICODE2_reserved4_WID                              ( 8)
  #define CH2CCC_CR_PICODE2_reserved4_MSK                              (0xFF000000)
  #define CH2CCC_CR_PICODE2_reserved4_MIN                              (0)
  #define CH2CCC_CR_PICODE2_reserved4_MAX                              (255) // 0x000000FF
  #define CH2CCC_CR_PICODE2_reserved4_DEF                              (0x00000000)
  #define CH2CCC_CR_PICODE2_reserved4_HSH                              (0x08303490)

#define CH2CCC_CR_PICODE3_A0_REG                                       (0x00003490)
#define CH2CCC_CR_PICODE3_REG                                          (0x00003494)

  #define CH2CCC_CR_PICODE3_mdll_cmn_pi6code_OFF                       ( 0)
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi6code_WID                       (10)
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi6code_MSK                       (0x000003FF)
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi6code_MIN                       (0)
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi6code_MAX                       (1023) // 0x000003FF
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi6code_DEF                       (0x00000000)
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi6code_HSH                       (0x0A003494)

  #define CH2CCC_CR_PICODE3_reserved7_OFF                              (10)
  #define CH2CCC_CR_PICODE3_reserved7_WID                              ( 6)
  #define CH2CCC_CR_PICODE3_reserved7_MSK                              (0x0000FC00)
  #define CH2CCC_CR_PICODE3_reserved7_MIN                              (0)
  #define CH2CCC_CR_PICODE3_reserved7_MAX                              (63) // 0x0000003F
  #define CH2CCC_CR_PICODE3_reserved7_DEF                              (0x00000000)
  #define CH2CCC_CR_PICODE3_reserved7_HSH                              (0x06143494)

  #define CH2CCC_CR_PICODE3_mdll_cmn_pi7code_OFF                       (16)
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi7code_WID                       ( 8)
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi7code_MSK                       (0x00FF0000)
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi7code_MIN                       (0)
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi7code_MAX                       (255) // 0x000000FF
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi7code_DEF                       (0x00000000)
  #define CH2CCC_CR_PICODE3_mdll_cmn_pi7code_HSH                       (0x08203494)

  #define CH2CCC_CR_PICODE3_reserved6_OFF                              (24)
  #define CH2CCC_CR_PICODE3_reserved6_WID                              ( 8)
  #define CH2CCC_CR_PICODE3_reserved6_MSK                              (0xFF000000)
  #define CH2CCC_CR_PICODE3_reserved6_MIN                              (0)
  #define CH2CCC_CR_PICODE3_reserved6_MAX                              (255) // 0x000000FF
  #define CH2CCC_CR_PICODE3_reserved6_DEF                              (0x00000000)
  #define CH2CCC_CR_PICODE3_reserved6_HSH                              (0x08303494)

#define CH2CCC_CR_MDLLCTL0_A0_REG                                      (0x00003494)
#define CH2CCC_CR_MDLLCTL0_REG                                         (0x00003498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turbocaptrim_OFF                 ( 0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turbocaptrim_WID                 ( 2)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turbocaptrim_MSK                 (0x00000003)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turbocaptrim_MIN                 (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turbocaptrim_MAX                 (3) // 0x00000003
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turbocaptrim_DEF                 (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turbocaptrim_HSH                 (0x02003498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turboonstartup_OFF               ( 2)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turboonstartup_WID               ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turboonstartup_MSK               (0x00000004)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turboonstartup_MIN               (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turboonstartup_MAX               (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turboonstartup_DEF               (0x00000001)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_turboonstartup_HSH               (0x01043498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_OFF                  ( 3)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_WID                  ( 6)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_MSK                  (0x000001F8)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_MIN                  (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_MAX                  (63) // 0x0000003F
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_DEF                  (0x00000020)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_ldoffcodepi_HSH                  (0x06063498)

  #define CH2CCC_CR_MDLLCTL0_reserved8_OFF                             ( 9)
  #define CH2CCC_CR_MDLLCTL0_reserved8_WID                             ( 1)
  #define CH2CCC_CR_MDLLCTL0_reserved8_MSK                             (0x00000200)
  #define CH2CCC_CR_MDLLCTL0_reserved8_MIN                             (0)
  #define CH2CCC_CR_MDLLCTL0_reserved8_MAX                             (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_reserved8_DEF                             (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_reserved8_HSH                             (0x01123498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlphgenin_OFF                    (10)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlphgenin_WID                    ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlphgenin_MSK                    (0x00000400)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlphgenin_MIN                    (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlphgenin_MAX                    (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlphgenin_DEF                    (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlphgenin_HSH                    (0x01143498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlrdacholden_OFF                 (11)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlrdacholden_WID                 ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlrdacholden_MSK                 (0x00000800)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlrdacholden_MIN                 (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlrdacholden_MAX                 (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlrdacholden_DEF                 (0x00000001)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_wlrdacholden_HSH                 (0x01163498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_weaklocken_OFF                   (12)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_weaklocken_WID                   ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_weaklocken_MSK                   (0x00001000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_weaklocken_MIN                   (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_weaklocken_MAX                   (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_weaklocken_DEF                   (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_weaklocken_HSH                   (0x01183498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_dllbypassen_OFF                  (13)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_dllbypassen_WID                  ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_dllbypassen_MSK                  (0x00002000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_dllbypassen_MIN                  (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_dllbypassen_MAX                  (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_dllbypassen_DEF                  (0x00000001)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_dllbypassen_HSH                  (0x011A3498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllengthsel_OFF                 (14)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllengthsel_WID                 ( 2)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllengthsel_MSK                 (0x0000C000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllengthsel_MIN                 (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllengthsel_MAX                 (3) // 0x00000003
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllengthsel_DEF                 (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllengthsel_HSH                 (0x021C3498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_phdeterr_OFF                     (16)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_phdeterr_WID                     ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_phdeterr_MSK                     (0x00010000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_phdeterr_MIN                     (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_phdeterr_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_phdeterr_DEF                     (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_phdeterr_HSH                     (0x01203498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdlllock_OFF                     (17)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdlllock_WID                     ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdlllock_MSK                     (0x00020000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdlllock_MIN                     (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdlllock_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdlllock_DEF                     (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdlllock_HSH                     (0x01223498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_pien_OFF                         (18)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_pien_WID                         ( 8)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_pien_MSK                         (0x03FC0000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_pien_MIN                         (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_pien_MAX                         (255) // 0x000000FF
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_pien_DEF                         (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_pien_HSH                         (0x08243498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllen_OFF                       (26)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllen_WID                       ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllen_MSK                       (0x04000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllen_MIN                       (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllen_MAX                       (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllen_DEF                       (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_mdllen_HSH                       (0x01343498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlval_OFF                     (27)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlval_WID                     ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlval_MSK                     (0x08000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlval_MIN                     (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlval_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlval_DEF                     (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlval_HSH                     (0x01363498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlerr_OFF                     (28)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlerr_WID                     ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlerr_MSK                     (0x10000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlerr_MIN                     (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlerr_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlerr_DEF                     (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlerr_HSH                     (0x01383498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlen_OFF                      (29)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlen_WID                      ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlen_MSK                      (0x20000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlen_MIN                      (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlen_MAX                      (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlen_DEF                      (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extwlen_HSH                      (0x013A3498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeval_OFF                 (30)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeval_WID                 ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeval_MSK                 (0x40000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeval_MIN                 (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeval_MAX                 (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeval_DEF                 (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeval_HSH                 (0x013C3498)

  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeen_OFF                  (31)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeen_WID                  ( 1)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeen_MSK                  (0x80000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeen_MIN                  (0)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeen_MAX                  (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeen_DEF                  (0x00000000)
  #define CH2CCC_CR_MDLLCTL0_mdll_cmn_extfreezeen_HSH                  (0x013E3498)

#define CH2CCC_CR_MDLLCTL1_A0_REG                                      (0x00003498)
#define CH2CCC_CR_MDLLCTL1_REG                                         (0x0000349C)

  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_picapsel_OFF                     ( 0)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_picapsel_WID                     ( 3)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_picapsel_MSK                     (0x00000007)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_picapsel_MIN                     (0)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_picapsel_MAX                     (7) // 0x00000007
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_picapsel_DEF                     (0x00000000)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_picapsel_HSH                     (0x0300349C)

  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_OFF                ( 3)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_WID                ( 5)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_MSK                (0x000000F8)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_MIN                (0)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_MAX                (31) // 0x0000001F
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_DEF                (0x00000007)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_phsdrvprocsel_HSH                (0x0506349C)

  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_openloopbias_OFF                 ( 8)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_openloopbias_WID                 ( 1)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_openloopbias_MSK                 (0x00000100)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_openloopbias_MIN                 (0)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_openloopbias_MAX                 (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_openloopbias_DEF                 (0x00000000)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_openloopbias_HSH                 (0x0110349C)

  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_OFF                  ( 9)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_WID                  ( 2)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_MSK                  (0x00000600)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_MIN                  (0)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_MAX                  (3) // 0x00000003
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_DEF                  (0x00000001)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctlcaptrim_HSH                  (0x0212349C)

  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_OFF                    (11)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_WID                    ( 6)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_MSK                    (0x0001F800)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_MIN                    (0)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_MAX                    (63) // 0x0000003F
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_DEF                    (0x00000020)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vcdlbwsel_HSH                    (0x0616349C)

  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldacforcen_OFF                (17)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldacforcen_WID                ( 1)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldacforcen_MSK                (0x00020000)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldacforcen_MIN                (0)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldacforcen_MAX                (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldacforcen_DEF                (0x00000000)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldacforcen_HSH                (0x0122349C)

  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccode_OFF                  (18)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccode_WID                  ( 9)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccode_MSK                  (0x07FC0000)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccode_MIN                  (0)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccode_MAX                  (511) // 0x000001FF
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccode_DEF                  (0x00000187)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccode_HSH                  (0x0924349C)

  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_OFF             (27)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_WID             ( 1)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_MSK             (0x08000000)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_MIN             (0)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_MAX             (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_DEF             (0x00000000)
  #define CH2CCC_CR_MDLLCTL1_mdll_cmn_vctldaccompareen_HSH             (0x0136349C)

  #define CH2CCC_CR_MDLLCTL1_Spare_OFF                                 (28)
  #define CH2CCC_CR_MDLLCTL1_Spare_WID                                 ( 4)
  #define CH2CCC_CR_MDLLCTL1_Spare_MSK                                 (0xF0000000)
  #define CH2CCC_CR_MDLLCTL1_Spare_MIN                                 (0)
  #define CH2CCC_CR_MDLLCTL1_Spare_MAX                                 (15) // 0x0000000F
  #define CH2CCC_CR_MDLLCTL1_Spare_DEF                                 (0x00000001)
  #define CH2CCC_CR_MDLLCTL1_Spare_HSH                                 (0x0438349C)

#define CH2CCC_CR_MDLLCTL2_A0_REG                                      (0x0000349C)
#define CH2CCC_CR_MDLLCTL2_REG                                         (0x000034A0)

  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_OFF                ( 0)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_WID                ( 1)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_MSK                (0x00000001)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_MIN                (0)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_MAX                (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_DEF                (0x00000000)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dcdtargclksel_HSH                (0x010034A0)

  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dccen_OFF                        ( 1)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dccen_WID                        ( 1)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dccen_MSK                        (0x00000002)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dccen_MIN                        (0)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dccen_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dccen_DEF                        (0x00000001)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dccen_HSH                        (0x010234A0)

  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dllldoen_OFF                     ( 2)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dllldoen_WID                     ( 1)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dllldoen_MSK                     (0x00000004)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dllldoen_MIN                     (0)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dllldoen_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dllldoen_DEF                     (0x00000000)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_dllldoen_HSH                     (0x010434A0)

  #define CH2CCC_CR_MDLLCTL2_bonus_1_OFF                               ( 3)
  #define CH2CCC_CR_MDLLCTL2_bonus_1_WID                               ( 2)
  #define CH2CCC_CR_MDLLCTL2_bonus_1_MSK                               (0x00000018)
  #define CH2CCC_CR_MDLLCTL2_bonus_1_MIN                               (0)
  #define CH2CCC_CR_MDLLCTL2_bonus_1_MAX                               (3) // 0x00000003
  #define CH2CCC_CR_MDLLCTL2_bonus_1_DEF                               (0x00000000)
  #define CH2CCC_CR_MDLLCTL2_bonus_1_HSH                               (0x020634A0)

  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_OFF            ( 5)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_WID            ( 5)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_MSK            (0x000003E0)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_MIN            (0)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_MAX            (31) // 0x0000001F
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_DEF            (0x00000010)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vctlcompoffsetcal_HSH            (0x050A34A0)

  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vcdldcccode_OFF                  (10)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vcdldcccode_WID                  ( 4)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vcdldcccode_MSK                  (0x00003C00)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vcdldcccode_MIN                  (0)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vcdldcccode_MAX                  (15) // 0x0000000F
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vcdldcccode_DEF                  (0x00000008)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_vcdldcccode_HSH                  (0x041434A0)

  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_OFF                (14)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_WID                ( 8)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_MSK                (0x003FC000)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_MIN                (0)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_MAX                (255) // 0x000000FF
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_DEF                (0x00000080)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_HSH                (0x081C34A0)

  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_OFF                  (22)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_WID                  ( 6)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_MSK                  (0x0FC00000)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_MIN                  (0)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_MAX                  (63) // 0x0000003F
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_DEF                  (0x00000020)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodewl_HSH                  (0x062C34A0)

  #define CH2CCC_CR_MDLLCTL2_spare_OFF                                 (28)
  #define CH2CCC_CR_MDLLCTL2_spare_WID                                 ( 3)
  #define CH2CCC_CR_MDLLCTL2_spare_MSK                                 (0x70000000)
  #define CH2CCC_CR_MDLLCTL2_spare_MIN                                 (0)
  #define CH2CCC_CR_MDLLCTL2_spare_MAX                                 (7) // 0x00000007
  #define CH2CCC_CR_MDLLCTL2_spare_DEF                                 (0x00000000)
  #define CH2CCC_CR_MDLLCTL2_spare_HSH                                 (0x033834A0)

  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_OFF                  (31)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_WID                  ( 1)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_MSK                  (0x80000000)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_MIN                  (0)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_MAX                  (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_DEF                  (0x00000000)
  #define CH2CCC_CR_MDLLCTL2_mdll_cmn_ldofbdivsel_HSH                  (0x013E34A0)

#define CH2CCC_CR_MDLLCTL3_A0_REG                                      (0x000034A0)
#define CH2CCC_CR_MDLLCTL3_REG                                         (0x000034A4)

  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_OFF                 ( 0)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_WID                 ( 3)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_MSK                 (0x00000007)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_MIN                 (0)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_MAX                 (7) // 0x00000007
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_DEF                 (0x00000004)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldopbiasctrl_HSH                 (0x030034A4)

  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_OFF          ( 3)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_WID          ( 1)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_MSK          (0x00000008)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_MIN          (0)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_MAX          (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_DEF          (0x00000000)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldolocalvsshibypass_HSH          (0x010634A4)

  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldoforceen_OFF                   ( 4)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldoforceen_WID                   ( 1)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldoforceen_MSK                   (0x00000010)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldoforceen_MIN                   (0)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldoforceen_MAX                   (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldoforceen_DEF                   (0x00000000)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldoforceen_HSH                   (0x010834A4)

  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_OFF             ( 5)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_WID             ( 5)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_MSK             (0x000003E0)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_MIN             (0)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_MAX             (31) // 0x0000001F
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_DEF             (0x00000005)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasvrefcode_HSH             (0x050A34A4)

  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_OFF                 (10)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_WID                 ( 4)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_MSK                 (0x00003C00)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_MIN                 (0)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_MAX                 (15) // 0x0000000F
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_DEF                 (0x00000008)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_ldonbiasctrl_HSH                 (0x041434A4)

  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_rloadcomp_OFF                    (14)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_rloadcomp_WID                    ( 6)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_rloadcomp_MSK                    (0x000FC000)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_rloadcomp_MIN                    (0)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_rloadcomp_MAX                    (63) // 0x0000003F
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_rloadcomp_DEF                    (0x0000000E)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_rloadcomp_HSH                    (0x061C34A4)

  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_OFF            (20)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_WID            ( 1)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_MSK            (0x00100000)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_MIN            (0)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_MAX            (1) // 0x00000001
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_DEF            (0x00000000)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vctldaccompareout_HSH            (0x012834A4)

  #define CH2CCC_CR_MDLLCTL3_spare_OFF                                 (21)
  #define CH2CCC_CR_MDLLCTL3_spare_WID                                 ( 9)
  #define CH2CCC_CR_MDLLCTL3_spare_MSK                                 (0x3FE00000)
  #define CH2CCC_CR_MDLLCTL3_spare_MIN                                 (0)
  #define CH2CCC_CR_MDLLCTL3_spare_MAX                                 (511) // 0x000001FF
  #define CH2CCC_CR_MDLLCTL3_spare_DEF                                 (0x00000000)
  #define CH2CCC_CR_MDLLCTL3_spare_HSH                                 (0x092A34A4)

  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vcdlcben_OFF                     (30)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vcdlcben_WID                     ( 2)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vcdlcben_MSK                     (0xC0000000)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vcdlcben_MIN                     (0)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vcdlcben_MAX                     (3) // 0x00000003
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vcdlcben_DEF                     (0x00000000)
  #define CH2CCC_CR_MDLLCTL3_mdll_cmn_vcdlcben_HSH                     (0x023C34A4)

#define CH2CCC_CR_COMP2_A0_REG                                         (0x000034A4)
#define CH2CCC_CR_COMP2_REG                                            (0x000034A8)

  #define CH2CCC_CR_COMP2_cccrx_cmn_rxmtailctl_OFF                     ( 0)
  #define CH2CCC_CR_COMP2_cccrx_cmn_rxmtailctl_WID                     ( 2)
  #define CH2CCC_CR_COMP2_cccrx_cmn_rxmtailctl_MSK                     (0x00000003)
  #define CH2CCC_CR_COMP2_cccrx_cmn_rxmtailctl_MIN                     (0)
  #define CH2CCC_CR_COMP2_cccrx_cmn_rxmtailctl_MAX                     (3) // 0x00000003
  #define CH2CCC_CR_COMP2_cccrx_cmn_rxmtailctl_DEF                     (0x00000002)
  #define CH2CCC_CR_COMP2_cccrx_cmn_rxmtailctl_HSH                     (0x020034A8)

  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateen_OFF                    ( 2)
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateen_WID                    ( 1)
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateen_MSK                    (0x00000004)
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateen_MIN                    (0)
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateen_MAX                    (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateen_DEF                    (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateen_HSH                    (0x010434A8)

  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateenclk_OFF                 ( 3)
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateenclk_WID                 ( 1)
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateenclk_MSK                 (0x00000008)
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateenclk_MIN                 (0)
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateenclk_MAX                 (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateenclk_DEF                 (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_cmn_extupdateenclk_HSH                 (0x010634A8)

  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewdlybypass_A0_OFF                ( 4)
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewdlybypass_A0_WID                (15)
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewdlybypass_A0_MSK                (0x0007FFF0)
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewdlybypass_A0_MIN                (0)
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewdlybypass_A0_MAX                (32767) // 0x00007FFF
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewdlybypass_A0_DEF                (0x00007FFF)
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewdlybypass_A0_HSH                (0x0F0834A4)

  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_A0_OFF                  (19)
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_A0_WID                  ( 1)
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_A0_MSK                  (0x00080000)
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_A0_MIN                  (0)
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_A0_MAX                  (1) // 0x00000001
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_A0_DEF                  (0x00000001)
  #define CH0CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_A0_HSH                  (0x012634A4)

  #define CH0CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_A0_OFF               (20)
  #define CH0CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_A0_WID               ( 1)
  #define CH0CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_A0_MSK               (0x00100000)
  #define CH0CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_A0_MIN               (0)
  #define CH0CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_A0_MAX               (1) // 0x00000001
  #define CH0CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_A0_DEF               (0x00000000)
  #define CH0CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_A0_HSH               (0x012834A4)

  #define CH0CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_A0_OFF               (21)
  #define CH0CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_A0_WID               ( 1)
  #define CH0CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_A0_MSK               (0x00200000)
  #define CH0CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_A0_MIN               (0)
  #define CH0CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_A0_MAX               (1) // 0x00000001
  #define CH0CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_A0_DEF               (0x00000000)
  #define CH0CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_A0_HSH               (0x012A34A4)

  #define CH0CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_A0_OFF                 (22)
  #define CH0CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_A0_WID                 ( 1)
  #define CH0CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_A0_MSK                 (0x00400000)
  #define CH0CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_A0_MIN                 (0)
  #define CH0CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_A0_MAX                 (1) // 0x00000001
  #define CH0CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_A0_DEF                 (0x00000000)
  #define CH0CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_A0_HSH                 (0x012C34A4)

  #define CH0CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_A0_OFF                (23)
  #define CH0CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_A0_WID                ( 1)
  #define CH0CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_A0_MSK                (0x00800000)
  #define CH0CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_A0_MIN                (0)
  #define CH0CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_A0_MAX                (1) // 0x00000001
  #define CH0CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_A0_DEF                (0x00000000)
  #define CH0CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_A0_HSH                (0x012E34A4)

  #define CH0CCC_CR_COMP2_ccctx_cmn_ennmospup_A0_OFF                      (24)
  #define CH0CCC_CR_COMP2_ccctx_cmn_ennmospup_A0_WID                      ( 1)
  #define CH0CCC_CR_COMP2_ccctx_cmn_ennmospup_A0_MSK                      (0x01000000)
  #define CH0CCC_CR_COMP2_ccctx_cmn_ennmospup_A0_MIN                      (0)
  #define CH0CCC_CR_COMP2_ccctx_cmn_ennmospup_A0_MAX                      (1) // 0x00000001
  #define CH0CCC_CR_COMP2_ccctx_cmn_ennmospup_A0_DEF                      (0x00000000)
  #define CH0CCC_CR_COMP2_ccctx_cmn_ennmospup_A0_HSH                      (0x013034A4)

  #define CH2CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_OFF                  ( 4)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_WID                  ( 1)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_MSK                  (0x00000010)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_MIN                  (0)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_MAX                  (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_DEF                  (0x00000001)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txslewpreupdt_HSH                  (0x010834A8)

  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_OFF               ( 5)
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_WID               ( 1)
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_MSK               (0x00000020)
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_MIN               (0)
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_MAX               (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_DEF               (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_HSH               (0x010A34A8)

  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_OFF               ( 6)
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_WID               ( 1)
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_MSK               (0x00000040)
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_MIN               (0)
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_MAX               (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_DEF               (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_HSH               (0x010C34A8)

  #define CH2CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_OFF                 ( 7)
  #define CH2CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_WID                 ( 1)
  #define CH2CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_MSK                 (0x00000080)
  #define CH2CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_MIN                 (0)
  #define CH2CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_MAX                 (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_DEF                 (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_cmn_pwrdwncompupdt_HSH                 (0x010E34A8)

  #define CH2CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_OFF                ( 8)
  #define CH2CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_WID                ( 1)
  #define CH2CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_MSK                (0x00000100)
  #define CH2CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_MIN                (0)
  #define CH2CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_MAX                (1) // 0x00000001
  #define CH2CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_DEF                (0x00000000)
  #define CH2CCC_CR_COMP2_afetx_cmn_txlocalvsshibyp_HSH                (0x011034A8)

  #define CH2CCC_CR_COMP2_ccctx_cmn_ennmospup_OFF                      ( 9)
  #define CH2CCC_CR_COMP2_ccctx_cmn_ennmospup_WID                      ( 1)
  #define CH2CCC_CR_COMP2_ccctx_cmn_ennmospup_MSK                      (0x00000200)
  #define CH2CCC_CR_COMP2_ccctx_cmn_ennmospup_MIN                      (0)
  #define CH2CCC_CR_COMP2_ccctx_cmn_ennmospup_MAX                      (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_cmn_ennmospup_DEF                      (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_cmn_ennmospup_HSH                      (0x011234A8)

  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_ovr_sel_OFF       (10)
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_ovr_sel_WID       ( 1)
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_ovr_sel_MSK       (0x00000400)
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_ovr_sel_MIN       (0)
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_ovr_sel_MAX       (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_ovr_sel_DEF       (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_rgrp3_compupdtsyncen_ovr_sel_HSH       (0x011434A8)

  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_ovr_sel_OFF       (11)
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_ovr_sel_WID       ( 1)
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_ovr_sel_MSK       (0x00000800)
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_ovr_sel_MIN       (0)
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_ovr_sel_MAX       (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_ovr_sel_DEF       (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_rgrp4_compupdtsyncen_ovr_sel_HSH       (0x011634A8)

  #define CH2CCC_CR_COMP2_Spare1_OFF                                   (12)
  #define CH2CCC_CR_COMP2_Spare1_WID                                   (15)
  #define CH2CCC_CR_COMP2_Spare1_MSK                                   (0x07FFF000)
  #define CH2CCC_CR_COMP2_Spare1_MIN                                   (0)
  #define CH2CCC_CR_COMP2_Spare1_MAX                                   (32767) // 0x00007FFF
  #define CH2CCC_CR_COMP2_Spare1_DEF                                   (0x00000000)
  #define CH2CCC_CR_COMP2_Spare1_HSH                                   (0x0F1834A8)

  #define CH2CCC_CR_COMP2_ccctx_cmn_txeqcomp_OFF                       (27)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txeqcomp_WID                       ( 3)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txeqcomp_MSK                       (0x38000000)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txeqcomp_MIN                       (0)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txeqcomp_MAX                       (7) // 0x00000007
  #define CH2CCC_CR_COMP2_ccctx_cmn_txeqcomp_DEF                       (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txeqcomp_HSH                       (0x033634A8)

  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdten_OFF                   (30)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdten_WID                   ( 1)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdten_MSK                   (0x40000000)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdten_MIN                   (0)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdten_MAX                   (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdten_DEF                   (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdten_HSH                   (0x013C34A8)

  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdtclken_OFF                (31)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdtclken_WID                ( 1)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdtclken_MSK                (0x80000000)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdtclken_MIN                (0)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdtclken_MAX                (1) // 0x00000001
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdtclken_DEF                (0x00000000)
  #define CH2CCC_CR_COMP2_ccctx_cmn_txcompupdtclken_HSH                (0x013E34A8)

#define CH2CCC_CR_COMP3_A0_REG                                         (0x000034A8)
#define CH2CCC_CR_COMP3_REG                                            (0x000034AC)

  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdten_OFF                     ( 0)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdten_WID                     (15)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdten_MSK                     (0x00007FFF)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdten_MIN                     (0)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdten_MAX                     (32767) // 0x00007FFF
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdten_DEF                     (0x00007FFF)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdten_HSH                     (0x0F0034AC)

  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdtclken_OFF                  (15)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdtclken_WID                  ( 1)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdtclken_MSK                  (0x00008000)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdtclken_MIN                  (0)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdtclken_MAX                  (1) // 0x00000001
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdtclken_DEF                  (0x00000000)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compupdtclken_HSH                  (0x011E34AC)

  #define CH2CCC_CR_COMP3_ccctx_cmn_compsyncreset_OFF                  (16)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compsyncreset_WID                  ( 1)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compsyncreset_MSK                  (0x00010000)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compsyncreset_MIN                  (0)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compsyncreset_MAX                  (1) // 0x00000001
  #define CH2CCC_CR_COMP3_ccctx_cmn_compsyncreset_DEF                  (0x00000001)
  #define CH2CCC_CR_COMP3_ccctx_cmn_compsyncreset_HSH                  (0x012034AC)

  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompp_OFF           (17)
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompp_WID           ( 5)
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompp_MSK           (0x003E0000)
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompp_MIN           (0)
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompp_MAX           (31) // 0x0000001F
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompp_DEF           (0x0000001F)
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompp_HSH           (0x052234AC)

  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompn_OFF           (22)
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompn_WID           ( 5)
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompn_MSK           (0x07C00000)
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompn_MIN           (0)
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompn_MAX           (31) // 0x0000001F
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompn_DEF           (0x0000001F)
  #define CH2CCC_CR_COMP3_imod_comp_ccctx_cmn_txtcocompn_HSH           (0x052C34AC)

  #define CH2CCC_CR_COMP3_DdrCaSlwDlyBypass_OFF                        (27)
  #define CH2CCC_CR_COMP3_DdrCaSlwDlyBypass_WID                        ( 1)
  #define CH2CCC_CR_COMP3_DdrCaSlwDlyBypass_MSK                        (0x08000000)
  #define CH2CCC_CR_COMP3_DdrCaSlwDlyBypass_MIN                        (0)
  #define CH2CCC_CR_COMP3_DdrCaSlwDlyBypass_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_COMP3_DdrCaSlwDlyBypass_DEF                        (0x00000001)
  #define CH2CCC_CR_COMP3_DdrCaSlwDlyBypass_HSH                        (0x013634AC)

  #define CH2CCC_CR_COMP3_DdrCtlSlwDlyBypass_OFF                       (28)
  #define CH2CCC_CR_COMP3_DdrCtlSlwDlyBypass_WID                       ( 1)
  #define CH2CCC_CR_COMP3_DdrCtlSlwDlyBypass_MSK                       (0x10000000)
  #define CH2CCC_CR_COMP3_DdrCtlSlwDlyBypass_MIN                       (0)
  #define CH2CCC_CR_COMP3_DdrCtlSlwDlyBypass_MAX                       (1) // 0x00000001
  #define CH2CCC_CR_COMP3_DdrCtlSlwDlyBypass_DEF                       (0x00000001)
  #define CH2CCC_CR_COMP3_DdrCtlSlwDlyBypass_HSH                       (0x013834AC)

  #define CH2CCC_CR_COMP3_DdrClkSlwDlyBypass_OFF                       (29)
  #define CH2CCC_CR_COMP3_DdrClkSlwDlyBypass_WID                       ( 1)
  #define CH2CCC_CR_COMP3_DdrClkSlwDlyBypass_MSK                       (0x20000000)
  #define CH2CCC_CR_COMP3_DdrClkSlwDlyBypass_MIN                       (0)
  #define CH2CCC_CR_COMP3_DdrClkSlwDlyBypass_MAX                       (1) // 0x00000001
  #define CH2CCC_CR_COMP3_DdrClkSlwDlyBypass_DEF                       (0x00000001)
  #define CH2CCC_CR_COMP3_DdrClkSlwDlyBypass_HSH                       (0x013A34AC)

  #define CH2CCC_CR_COMP3_Spare_OFF                                    (30)
  #define CH2CCC_CR_COMP3_Spare_WID                                    ( 2)
  #define CH2CCC_CR_COMP3_Spare_MSK                                    (0xC0000000)
  #define CH2CCC_CR_COMP3_Spare_MIN                                    (0)
  #define CH2CCC_CR_COMP3_Spare_MAX                                    (3) // 0x00000003
  #define CH2CCC_CR_COMP3_Spare_DEF                                    (0x00000000)
  #define CH2CCC_CR_COMP3_Spare_HSH                                    (0x023C34AC)

#define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_A0_REG                           (0x000034AC)
#define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_REG                              (0x000034B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpdnusevcciog_OFF   ( 0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpdnusevcciog_WID   ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpdnusevcciog_MSK   (0x00000001)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpdnusevcciog_MIN   (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpdnusevcciog_MAX   (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpdnusevcciog_DEF   (0x00000001)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpdnusevcciog_HSH   (0x010034B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlVoltageSelect_OFF           ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlVoltageSelect_WID           ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlVoltageSelect_MSK           (0x00000002)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlVoltageSelect_MIN           (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlVoltageSelect_MAX           (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlVoltageSelect_DEF           (0x00000001)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlVoltageSelect_HSH           (0x010234B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkVoltageSelect_OFF           ( 2)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkVoltageSelect_WID           ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkVoltageSelect_MSK           (0x00000004)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkVoltageSelect_MIN           (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkVoltageSelect_MAX           (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkVoltageSelect_DEF           (0x00000001)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkVoltageSelect_HSH           (0x010434B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaVoltageSelect_OFF            ( 3)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaVoltageSelect_WID            ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaVoltageSelect_MSK            (0x00000008)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaVoltageSelect_MIN            (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaVoltageSelect_MAX            (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaVoltageSelect_DEF            (0x00000001)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaVoltageSelect_HSH            (0x010634B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaPDPreDrvVccddq_OFF           ( 4)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaPDPreDrvVccddq_WID           ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaPDPreDrvVccddq_MSK           (0x00000010)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaPDPreDrvVccddq_MIN           (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaPDPreDrvVccddq_MAX           (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaPDPreDrvVccddq_DEF           (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CaPDPreDrvVccddq_HSH           (0x010834B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlPDPreDrvVccddq_OFF          ( 5)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlPDPreDrvVccddq_WID          ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlPDPreDrvVccddq_MSK          (0x00000020)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlPDPreDrvVccddq_MIN          (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlPDPreDrvVccddq_MAX          (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlPDPreDrvVccddq_DEF          (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CtlPDPreDrvVccddq_HSH          (0x010A34B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkPDPreDrvVccddq_OFF          ( 6)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkPDPreDrvVccddq_WID          ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkPDPreDrvVccddq_MSK          (0x00000040)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkPDPreDrvVccddq_MIN          (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkPDPreDrvVccddq_MAX          (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkPDPreDrvVccddq_DEF          (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ClkPDPreDrvVccddq_HSH          (0x010C34B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txvsshiffstlegen_OFF ( 7)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txvsshiffstlegen_WID ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txvsshiffstlegen_MSK (0x00000080)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txvsshiffstlegen_MIN (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txvsshiffstlegen_MAX (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txvsshiffstlegen_DEF (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txvsshiffstlegen_HSH (0x010E34B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_rxbias_cmn_biasen_OFF          ( 8)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_rxbias_cmn_biasen_WID          ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_rxbias_cmn_biasen_MSK          (0x00000100)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_rxbias_cmn_biasen_MIN          (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_rxbias_cmn_biasen_MAX          (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_rxbias_cmn_biasen_DEF          (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_rxbias_cmn_biasen_HSH          (0x011034B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_ennbiasboost_OFF     ( 9)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_ennbiasboost_WID     ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_ennbiasboost_MSK     (0x00000200)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_ennbiasboost_MIN     (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_ennbiasboost_MAX     (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_ennbiasboost_DEF     (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_ennbiasboost_HSH     (0x011234B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_VssHiBypassVddqMode_OFF        (10)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_VssHiBypassVddqMode_WID        ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_VssHiBypassVddqMode_MSK        (0x00000400)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_VssHiBypassVddqMode_MIN        (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_VssHiBypassVddqMode_MAX        (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_VssHiBypassVddqMode_DEF        (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_VssHiBypassVddqMode_HSH        (0x011434B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpupusevcciog_OFF   (11)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpupusevcciog_WID   ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpupusevcciog_MSK   (0x00000800)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpupusevcciog_MIN   (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpupusevcciog_MAX   (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpupusevcciog_DEF   (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ccctx_cmn_txpupusevcciog_HSH   (0x011634B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CkUseCtlComp_OFF               (12)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CkUseCtlComp_WID               ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CkUseCtlComp_MSK               (0x00001000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CkUseCtlComp_MIN               (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CkUseCtlComp_MAX               (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CkUseCtlComp_DEF               (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CkUseCtlComp_HSH               (0x011834B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CsUseCaComp_OFF                (13)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CsUseCaComp_WID                ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CsUseCaComp_MSK                (0x00002000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CsUseCaComp_MIN                (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CsUseCaComp_MAX                (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CsUseCaComp_DEF                (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_CsUseCaComp_HSH                (0x011A34B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ckecstxeqconstz_OFF            (14)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ckecstxeqconstz_WID            ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ckecstxeqconstz_MSK            (0x00004000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ckecstxeqconstz_MIN            (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ckecstxeqconstz_MAX            (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ckecstxeqconstz_DEF            (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ckecstxeqconstz_HSH            (0x011C34B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ctltxeqconstz_OFF              (15)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ctltxeqconstz_WID              ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ctltxeqconstz_MSK              (0x00008000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ctltxeqconstz_MIN              (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ctltxeqconstz_MAX              (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ctltxeqconstz_DEF              (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_ctltxeqconstz_HSH              (0x011E34B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_clktxeqconstz_OFF              (16)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_clktxeqconstz_WID              ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_clktxeqconstz_MSK              (0x00010000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_clktxeqconstz_MIN              (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_clktxeqconstz_MAX              (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_clktxeqconstz_DEF              (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_clktxeqconstz_HSH              (0x012034B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_catxeqconstz_OFF               (17)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_catxeqconstz_WID               ( 1)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_catxeqconstz_MSK               (0x00020000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_catxeqconstz_MIN               (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_catxeqconstz_MAX               (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_catxeqconstz_DEF               (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_catxeqconstz_HSH               (0x012234B0)

  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_Spare_OFF                      (18)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_Spare_WID                      (14)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_Spare_MSK                      (0xFFFC0000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_Spare_MIN                      (0)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_Spare_MAX                      (16383) // 0x00003FFF
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_Spare_DEF                      (0x00000000)
  #define CH2CCC_CR_DDRCRCCCVOLTAGEUSED_Spare_HSH                      (0x0E2434B0)

#define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_A0_REG                          (0x000034B0)
#define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_REG                             (0x000034B4)

  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlScompOffset_OFF            ( 0)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlScompOffset_WID            ( 4)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlScompOffset_MSK            (0x0000000F)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlScompOffset_MIN            (0)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlScompOffset_MAX            (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlScompOffset_DEF            (0x00000000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlScompOffset_HSH            (0x040034B4)

  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare1_OFF                    ( 4)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare1_WID                    ( 4)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare1_MSK                    (0x000000F0)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare1_MIN                    (0)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare1_MAX                    (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare1_DEF                    (0x00000000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare1_HSH                    (0x040834B4)

  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvUpOffset_OFF       ( 8)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvUpOffset_WID       ( 4)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvUpOffset_MSK       (0x00000F00)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvUpOffset_MIN       (0)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvUpOffset_MAX       (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvUpOffset_DEF       (0x00000000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvUpOffset_HSH       (0x041034B4)

  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvDownOffset_OFF     (12)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvDownOffset_WID     ( 4)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvDownOffset_MSK     (0x0000F000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvDownOffset_MIN     (0)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvDownOffset_MAX     (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvDownOffset_DEF     (0x00000000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvDownOffset_HSH     (0x041834B4)

  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaScompOffset_OFF             (16)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaScompOffset_WID             ( 4)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaScompOffset_MSK             (0x000F0000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaScompOffset_MIN             (0)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaScompOffset_MAX             (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaScompOffset_DEF             (0x00000000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaScompOffset_HSH             (0x042034B4)

  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare2_OFF                    (20)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare2_WID                    ( 4)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare2_MSK                    (0x00F00000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare2_MIN                    (0)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare2_MAX                    (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare2_DEF                    (0x00000000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_Spare2_HSH                    (0x042834B4)

  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvUpOffset_OFF        (24)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvUpOffset_WID        ( 4)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvUpOffset_MSK        (0x0F000000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvUpOffset_MIN        (0)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvUpOffset_MAX        (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvUpOffset_DEF        (0x00000000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvUpOffset_HSH        (0x043034B4)

  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvDownOffset_OFF      (28)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvDownOffset_WID      ( 4)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvDownOffset_MSK      (0xF0000000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvDownOffset_MIN      (0)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvDownOffset_MAX      (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvDownOffset_DEF      (0x00000000)
  #define CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvDownOffset_HSH      (0x043834B4)

#define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_A0_REG                       (0x000034B4)
#define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG                          (0x000034B8)

  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkScompOffset_OFF         ( 0)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkScompOffset_WID         ( 4)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkScompOffset_MSK         (0x0000000F)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkScompOffset_MIN         (0)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkScompOffset_MAX         (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkScompOffset_DEF         (0x00000000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkScompOffset_HSH         (0x040034B8)

  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare1_OFF                 ( 4)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare1_WID                 ( 4)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare1_MSK                 (0x000000F0)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare1_MIN                 (0)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare1_MAX                 (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare1_DEF                 (0x00000000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare1_HSH                 (0x040834B8)

  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvUpOffset_OFF    ( 8)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvUpOffset_WID    ( 4)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvUpOffset_MSK    (0x00000F00)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvUpOffset_MIN    (0)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvUpOffset_MAX    (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvUpOffset_DEF    (0x00000000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvUpOffset_HSH    (0x041034B8)

  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvDownOffset_OFF  (12)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvDownOffset_WID  ( 4)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvDownOffset_MSK  (0x0000F000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvDownOffset_MIN  (0)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvDownOffset_MAX  (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvDownOffset_DEF  (0x00000000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvDownOffset_HSH  (0x041834B8)

  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CtlFFOffset_OFF            (16)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CtlFFOffset_WID            ( 4)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CtlFFOffset_MSK            (0x000F0000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CtlFFOffset_MIN            (0)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CtlFFOffset_MAX            (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CtlFFOffset_DEF            (0x00000000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CtlFFOffset_HSH            (0x042034B8)

  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CaFFOffset_OFF             (20)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CaFFOffset_WID             ( 4)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CaFFOffset_MSK             (0x00F00000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CaFFOffset_MIN             (0)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CaFFOffset_MAX             (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CaFFOffset_DEF             (0x00000000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CaFFOffset_HSH             (0x042834B8)

  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkFFOffset_OFF            (24)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkFFOffset_WID            ( 4)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkFFOffset_MSK            (0x0F000000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkFFOffset_MIN            (0)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkFFOffset_MAX            (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkFFOffset_DEF            (0x00000000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkFFOffset_HSH            (0x043034B8)

  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare_OFF                  (28)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare_WID                  ( 4)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare_MSK                  (0xF0000000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare_MIN                  (0)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare_MAX                  (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare_DEF                  (0x00000000)
  #define CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_Spare_HSH                  (0x043834B8)

#define CH2CCC_CR_CTL0_REG                                             (0x000034BC)

  #define CH2CCC_CR_CTL0_ccctx_ccc5_iamclkp_OFF                        ( 0)
  #define CH2CCC_CR_CTL0_ccctx_ccc5_iamclkp_WID                        ( 1)
  #define CH2CCC_CR_CTL0_ccctx_ccc5_iamclkp_MSK                        (0x00000001)
  #define CH2CCC_CR_CTL0_ccctx_ccc5_iamclkp_MIN                        (0)
  #define CH2CCC_CR_CTL0_ccctx_ccc5_iamclkp_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_CTL0_ccctx_ccc5_iamclkp_DEF                        (0x00000001)
  #define CH2CCC_CR_CTL0_ccctx_ccc5_iamclkp_HSH                        (0x010034BC)

  #define CH2CCC_CR_CTL0_ccctx_ccc6_iamclkp_OFF                        ( 1)
  #define CH2CCC_CR_CTL0_ccctx_ccc6_iamclkp_WID                        ( 1)
  #define CH2CCC_CR_CTL0_ccctx_ccc6_iamclkp_MSK                        (0x00000002)
  #define CH2CCC_CR_CTL0_ccctx_ccc6_iamclkp_MIN                        (0)
  #define CH2CCC_CR_CTL0_ccctx_ccc6_iamclkp_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_CTL0_ccctx_ccc6_iamclkp_DEF                        (0x00000000)
  #define CH2CCC_CR_CTL0_ccctx_ccc6_iamclkp_HSH                        (0x010234BC)

  #define CH2CCC_CR_CTL0_ccctx_ccc7_iamclkp_OFF                        ( 2)
  #define CH2CCC_CR_CTL0_ccctx_ccc7_iamclkp_WID                        ( 1)
  #define CH2CCC_CR_CTL0_ccctx_ccc7_iamclkp_MSK                        (0x00000004)
  #define CH2CCC_CR_CTL0_ccctx_ccc7_iamclkp_MIN                        (0)
  #define CH2CCC_CR_CTL0_ccctx_ccc7_iamclkp_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_CTL0_ccctx_ccc7_iamclkp_DEF                        (0x00000001)
  #define CH2CCC_CR_CTL0_ccctx_ccc7_iamclkp_HSH                        (0x010434BC)

  #define CH2CCC_CR_CTL0_ccctx_ccc8_iamclkp_OFF                        ( 3)
  #define CH2CCC_CR_CTL0_ccctx_ccc8_iamclkp_WID                        ( 1)
  #define CH2CCC_CR_CTL0_ccctx_ccc8_iamclkp_MSK                        (0x00000008)
  #define CH2CCC_CR_CTL0_ccctx_ccc8_iamclkp_MIN                        (0)
  #define CH2CCC_CR_CTL0_ccctx_ccc8_iamclkp_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_CTL0_ccctx_ccc8_iamclkp_DEF                        (0x00000000)
  #define CH2CCC_CR_CTL0_ccctx_ccc8_iamclkp_HSH                        (0x010634BC)

  #define CH2CCC_CR_CTL0_ccctx_rgrp3_gear4enclkdata_OFF                ( 4)
  #define CH2CCC_CR_CTL0_ccctx_rgrp3_gear4enclkdata_WID                ( 1)
  #define CH2CCC_CR_CTL0_ccctx_rgrp3_gear4enclkdata_MSK                (0x00000010)
  #define CH2CCC_CR_CTL0_ccctx_rgrp3_gear4enclkdata_MIN                (0)
  #define CH2CCC_CR_CTL0_ccctx_rgrp3_gear4enclkdata_MAX                (1) // 0x00000001
  #define CH2CCC_CR_CTL0_ccctx_rgrp3_gear4enclkdata_DEF                (0x00000001)
  #define CH2CCC_CR_CTL0_ccctx_rgrp3_gear4enclkdata_HSH                (0x010834BC)

  #define CH2CCC_CR_CTL0_ccctx_rgrp4_gear4enclkdata_OFF                ( 5)
  #define CH2CCC_CR_CTL0_ccctx_rgrp4_gear4enclkdata_WID                ( 1)
  #define CH2CCC_CR_CTL0_ccctx_rgrp4_gear4enclkdata_MSK                (0x00000020)
  #define CH2CCC_CR_CTL0_ccctx_rgrp4_gear4enclkdata_MIN                (0)
  #define CH2CCC_CR_CTL0_ccctx_rgrp4_gear4enclkdata_MAX                (1) // 0x00000001
  #define CH2CCC_CR_CTL0_ccctx_rgrp4_gear4enclkdata_DEF                (0x00000001)
  #define CH2CCC_CR_CTL0_ccctx_rgrp4_gear4enclkdata_HSH                (0x010A34BC)

  #define CH2CCC_CR_CTL0_rxbias_cmn_rloadcomp_OFF                      ( 6)
  #define CH2CCC_CR_CTL0_rxbias_cmn_rloadcomp_WID                      ( 6)
  #define CH2CCC_CR_CTL0_rxbias_cmn_rloadcomp_MSK                      (0x00000FC0)
  #define CH2CCC_CR_CTL0_rxbias_cmn_rloadcomp_MIN                      (0)
  #define CH2CCC_CR_CTL0_rxbias_cmn_rloadcomp_MAX                      (63) // 0x0000003F
  #define CH2CCC_CR_CTL0_rxbias_cmn_rloadcomp_DEF                      (0x0000000E)
  #define CH2CCC_CR_CTL0_rxbias_cmn_rloadcomp_HSH                      (0x060C34BC)

  #define CH2CCC_CR_CTL0_alltx_cmn_tcoslewstatlegen_OFF                (12)
  #define CH2CCC_CR_CTL0_alltx_cmn_tcoslewstatlegen_WID                ( 1)
  #define CH2CCC_CR_CTL0_alltx_cmn_tcoslewstatlegen_MSK                (0x00001000)
  #define CH2CCC_CR_CTL0_alltx_cmn_tcoslewstatlegen_MIN                (0)
  #define CH2CCC_CR_CTL0_alltx_cmn_tcoslewstatlegen_MAX                (1) // 0x00000001
  #define CH2CCC_CR_CTL0_alltx_cmn_tcoslewstatlegen_DEF                (0x00000001)
  #define CH2CCC_CR_CTL0_alltx_cmn_tcoslewstatlegen_HSH                (0x011834BC)

  #define CH2CCC_CR_CTL0_cccrx_cmn_rxdiffampen_OFF                     (13)
  #define CH2CCC_CR_CTL0_cccrx_cmn_rxdiffampen_WID                     ( 1)
  #define CH2CCC_CR_CTL0_cccrx_cmn_rxdiffampen_MSK                     (0x00002000)
  #define CH2CCC_CR_CTL0_cccrx_cmn_rxdiffampen_MIN                     (0)
  #define CH2CCC_CR_CTL0_cccrx_cmn_rxdiffampen_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_CTL0_cccrx_cmn_rxdiffampen_DEF                     (0x00000000)
  #define CH2CCC_CR_CTL0_cccrx_cmn_rxdiffampen_HSH                     (0x011A34BC)

  #define CH2CCC_CR_CTL0_omod_func_cktop_cmn_bonus_OFF                 (14)
  #define CH2CCC_CR_CTL0_omod_func_cktop_cmn_bonus_WID                 ( 4)
  #define CH2CCC_CR_CTL0_omod_func_cktop_cmn_bonus_MSK                 (0x0003C000)
  #define CH2CCC_CR_CTL0_omod_func_cktop_cmn_bonus_MIN                 (0)
  #define CH2CCC_CR_CTL0_omod_func_cktop_cmn_bonus_MAX                 (15) // 0x0000000F
  #define CH2CCC_CR_CTL0_omod_func_cktop_cmn_bonus_DEF                 (0x00000000)
  #define CH2CCC_CR_CTL0_omod_func_cktop_cmn_bonus_HSH                 (0x041C34BC)

  #define CH2CCC_CR_CTL0_omod_func_cccrxflops_cmn_bonus_OFF            (18)
  #define CH2CCC_CR_CTL0_omod_func_cccrxflops_cmn_bonus_WID            ( 1)
  #define CH2CCC_CR_CTL0_omod_func_cccrxflops_cmn_bonus_MSK            (0x00040000)
  #define CH2CCC_CR_CTL0_omod_func_cccrxflops_cmn_bonus_MIN            (0)
  #define CH2CCC_CR_CTL0_omod_func_cccrxflops_cmn_bonus_MAX            (1) // 0x00000001
  #define CH2CCC_CR_CTL0_omod_func_cccrxflops_cmn_bonus_DEF            (0x00000000)
  #define CH2CCC_CR_CTL0_omod_func_cccrxflops_cmn_bonus_HSH            (0x012434BC)

  #define CH2CCC_CR_CTL0_cccrxflops_cmn_bonus_OFF                      (19)
  #define CH2CCC_CR_CTL0_cccrxflops_cmn_bonus_WID                      ( 2)
  #define CH2CCC_CR_CTL0_cccrxflops_cmn_bonus_MSK                      (0x00180000)
  #define CH2CCC_CR_CTL0_cccrxflops_cmn_bonus_MIN                      (0)
  #define CH2CCC_CR_CTL0_cccrxflops_cmn_bonus_MAX                      (3) // 0x00000003
  #define CH2CCC_CR_CTL0_cccrxflops_cmn_bonus_DEF                      (0x00000000)
  #define CH2CCC_CR_CTL0_cccrxflops_cmn_bonus_HSH                      (0x022634BC)

  #define CH2CCC_CR_CTL0_pghub_cmn_iobufact_OFF                        (21)
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufact_WID                        ( 1)
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufact_MSK                        (0x00200000)
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufact_MIN                        (0)
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufact_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufact_DEF                        (0x00000000)
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufact_HSH                        (0x012A34BC)

  #define CH2CCC_CR_CTL0_pghub_cmn_iobufactclk_OFF                     (22)
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufactclk_WID                     ( 1)
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufactclk_MSK                     (0x00400000)
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufactclk_MIN                     (0)
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufactclk_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufactclk_DEF                     (0x00000000)
  #define CH2CCC_CR_CTL0_pghub_cmn_iobufactclk_HSH                     (0x012C34BC)

  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakeren_OFF                 (23)
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakeren_WID                 ( 1)
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakeren_MSK                 (0x00800000)
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakeren_MIN                 (0)
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakeren_MAX                 (1) // 0x00000001
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakeren_DEF                 (0x00000001)
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakeren_HSH                 (0x012E34BC)

  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakercomp_OFF               (24)
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakercomp_WID               ( 6)
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakercomp_MSK               (0x3F000000)
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakercomp_MIN               (0)
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakercomp_MAX               (63) // 0x0000003F
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakercomp_DEF               (0x00000020)
  #define CH2CCC_CR_CTL0_ccctx_cmn_txvsshileakercomp_HSH               (0x063034BC)

  #define CH2CCC_CR_CTL0_safemodeenable_OFF                            (30)
  #define CH2CCC_CR_CTL0_safemodeenable_WID                            ( 1)
  #define CH2CCC_CR_CTL0_safemodeenable_MSK                            (0x40000000)
  #define CH2CCC_CR_CTL0_safemodeenable_MIN                            (0)
  #define CH2CCC_CR_CTL0_safemodeenable_MAX                            (1) // 0x00000001
  #define CH2CCC_CR_CTL0_safemodeenable_DEF                            (0x00000000)
  #define CH2CCC_CR_CTL0_safemodeenable_HSH                            (0x013C34BC)

  #define CH2CCC_CR_CTL0_Spare_OFF                                     (31)
  #define CH2CCC_CR_CTL0_Spare_WID                                     ( 1)
  #define CH2CCC_CR_CTL0_Spare_MSK                                     (0x80000000)
  #define CH2CCC_CR_CTL0_Spare_MIN                                     (0)
  #define CH2CCC_CR_CTL0_Spare_MAX                                     (1) // 0x00000001
  #define CH2CCC_CR_CTL0_Spare_DEF                                     (0x00000000)
  #define CH2CCC_CR_CTL0_Spare_HSH                                     (0x013E34BC)

#define CH2CCC_CR_CTL3_REG                                             (0x000034C0)

  #define CH2CCC_CR_CTL3_cktop_cmn_bonus_OFF                           ( 0)
  #define CH2CCC_CR_CTL3_cktop_cmn_bonus_WID                           ( 8)
  #define CH2CCC_CR_CTL3_cktop_cmn_bonus_MSK                           (0x000000FF)
  #define CH2CCC_CR_CTL3_cktop_cmn_bonus_MIN                           (0)
  #define CH2CCC_CR_CTL3_cktop_cmn_bonus_MAX                           (255) // 0x000000FF
  #define CH2CCC_CR_CTL3_cktop_cmn_bonus_DEF                           (0x00000078)
  #define CH2CCC_CR_CTL3_cktop_cmn_bonus_HSH                           (0x080034C0)

  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdselsecondaryclk_OFF             ( 8)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdselsecondaryclk_WID             ( 1)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdselsecondaryclk_MSK             (0x00000100)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdselsecondaryclk_MIN             (0)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdselsecondaryclk_MAX             (1) // 0x00000001
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdselsecondaryclk_DEF             (0x00000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdselsecondaryclk_HSH             (0x011034C0)

  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear1en_OFF                        ( 9)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear1en_WID                        ( 1)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear1en_MSK                        (0x00000200)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear1en_MIN                        (0)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear1en_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear1en_DEF                        (0x00000001)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear1en_HSH                        (0x011234C0)

  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear2en_OFF                        (10)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear2en_WID                        ( 1)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear2en_MSK                        (0x00000400)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear2en_MIN                        (0)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear2en_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear2en_DEF                        (0x00000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear2en_HSH                        (0x011434C0)

  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear4en_OFF                        (11)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear4en_WID                        ( 1)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear4en_MSK                        (0x00000800)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear4en_MIN                        (0)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear4en_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear4en_DEF                        (0x00000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_gear4en_HSH                        (0x011634C0)

  #define CH2CCC_CR_CTL3_cccbuf_cmn_bonus_OFF                          (12)
  #define CH2CCC_CR_CTL3_cccbuf_cmn_bonus_WID                          ( 2)
  #define CH2CCC_CR_CTL3_cccbuf_cmn_bonus_MSK                          (0x00003000)
  #define CH2CCC_CR_CTL3_cccbuf_cmn_bonus_MIN                          (0)
  #define CH2CCC_CR_CTL3_cccbuf_cmn_bonus_MAX                          (3) // 0x00000003
  #define CH2CCC_CR_CTL3_cccbuf_cmn_bonus_DEF                          (0x00000000)
  #define CH2CCC_CR_CTL3_cccbuf_cmn_bonus_HSH                          (0x021834C0)

  #define CH2CCC_CR_CTL3_pghub_cmn_pwrdwncomp_OFF                      (14)
  #define CH2CCC_CR_CTL3_pghub_cmn_pwrdwncomp_WID                      ( 6)
  #define CH2CCC_CR_CTL3_pghub_cmn_pwrdwncomp_MSK                      (0x000FC000)
  #define CH2CCC_CR_CTL3_pghub_cmn_pwrdwncomp_MIN                      (0)
  #define CH2CCC_CR_CTL3_pghub_cmn_pwrdwncomp_MAX                      (63) // 0x0000003F
  #define CH2CCC_CR_CTL3_pghub_cmn_pwrdwncomp_DEF                      (0x00000000)
  #define CH2CCC_CR_CTL3_pghub_cmn_pwrdwncomp_HSH                      (0x061C34C0)

  #define CH2CCC_CR_CTL3_ccctx_ccc0_dcdsampsecondary_OFF               (20)
  #define CH2CCC_CR_CTL3_ccctx_ccc0_dcdsampsecondary_WID               ( 1)
  #define CH2CCC_CR_CTL3_ccctx_ccc0_dcdsampsecondary_MSK               (0x00100000)
  #define CH2CCC_CR_CTL3_ccctx_ccc0_dcdsampsecondary_MIN               (0)
  #define CH2CCC_CR_CTL3_ccctx_ccc0_dcdsampsecondary_MAX               (1) // 0x00000001
  #define CH2CCC_CR_CTL3_ccctx_ccc0_dcdsampsecondary_DEF               (0x00000000)
  #define CH2CCC_CR_CTL3_ccctx_ccc0_dcdsampsecondary_HSH               (0x012834C0)

  #define CH2CCC_CR_CTL3_Spare4_OFF                                    (21)
  #define CH2CCC_CR_CTL3_Spare4_WID                                    ( 2)
  #define CH2CCC_CR_CTL3_Spare4_MSK                                    (0x00600000)
  #define CH2CCC_CR_CTL3_Spare4_MIN                                    (0)
  #define CH2CCC_CR_CTL3_Spare4_MAX                                    (3) // 0x00000003
  #define CH2CCC_CR_CTL3_Spare4_DEF                                    (0x00000000)
  #define CH2CCC_CR_CTL3_Spare4_HSH                                    (0x022A34C0)

  #define CH2CCC_CR_CTL3_clktx_rgrp4_txclkpden_OFF                     (23)
  #define CH2CCC_CR_CTL3_clktx_rgrp4_txclkpden_WID                     ( 1)
  #define CH2CCC_CR_CTL3_clktx_rgrp4_txclkpden_MSK                     (0x00800000)
  #define CH2CCC_CR_CTL3_clktx_rgrp4_txclkpden_MIN                     (0)
  #define CH2CCC_CR_CTL3_clktx_rgrp4_txclkpden_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_CTL3_clktx_rgrp4_txclkpden_DEF                     (0x00000000)
  #define CH2CCC_CR_CTL3_clktx_rgrp4_txclkpden_HSH                     (0x012E34C0)

  #define CH2CCC_CR_CTL3_clktx_rgrp3_txclkpden_OFF                     (24)
  #define CH2CCC_CR_CTL3_clktx_rgrp3_txclkpden_WID                     ( 1)
  #define CH2CCC_CR_CTL3_clktx_rgrp3_txclkpden_MSK                     (0x01000000)
  #define CH2CCC_CR_CTL3_clktx_rgrp3_txclkpden_MIN                     (0)
  #define CH2CCC_CR_CTL3_clktx_rgrp3_txclkpden_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_CTL3_clktx_rgrp3_txclkpden_DEF                     (0x00000000)
  #define CH2CCC_CR_CTL3_clktx_rgrp3_txclkpden_HSH                     (0x013034C0)

  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdtailctl_OFF                     (25)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdtailctl_WID                     ( 2)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdtailctl_MSK                     (0x06000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdtailctl_MIN                     (0)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdtailctl_MAX                     (3) // 0x00000003
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdtailctl_DEF                     (0x00000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdtailctl_HSH                     (0x023234C0)

  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdsrzsampclksel_OFF               (27)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdsrzsampclksel_WID               ( 2)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdsrzsampclksel_MSK               (0x18000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdsrzsampclksel_MIN               (0)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdsrzsampclksel_MAX               (3) // 0x00000003
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdsrzsampclksel_DEF               (0x00000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdsrzsampclksel_HSH               (0x023634C0)

  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenvcciog_OFF                    (29)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenvcciog_WID                    ( 1)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenvcciog_MSK                    (0x20000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenvcciog_MIN                    (0)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenvcciog_MAX                    (1) // 0x00000001
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenvcciog_DEF                    (0x00000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenvcciog_HSH                    (0x013A34C0)

  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadpsal_OFF                 (30)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadpsal_WID                 ( 1)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadpsal_MSK                 (0x40000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadpsal_MIN                 (0)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadpsal_MAX                 (1) // 0x00000001
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadpsal_DEF                 (0x00000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadpsal_HSH                 (0x013C34C0)

  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadnsal_OFF                 (31)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadnsal_WID                 ( 1)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadnsal_MSK                 (0x80000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadnsal_MIN                 (0)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadnsal_MAX                 (1) // 0x00000001
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadnsal_DEF                 (0x00000000)
  #define CH2CCC_CR_CTL3_dcdsrz_cmn_dcdenrxpadnsal_HSH                 (0x013E34C0)

#define CH2CCC_CR_MDLLCTL4_REG                                         (0x000034C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define CH2CCC_CR_CTL2_REG                                             (0x000034C8)

  #define CH2CCC_CR_CTL2_afeshared_cmn_parkval_OFF                     ( 0)
  #define CH2CCC_CR_CTL2_afeshared_cmn_parkval_WID                     ( 1)
  #define CH2CCC_CR_CTL2_afeshared_cmn_parkval_MSK                     (0x00000001)
  #define CH2CCC_CR_CTL2_afeshared_cmn_parkval_MIN                     (0)
  #define CH2CCC_CR_CTL2_afeshared_cmn_parkval_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_CTL2_afeshared_cmn_parkval_DEF                     (0x00000000)
  #define CH2CCC_CR_CTL2_afeshared_cmn_parkval_HSH                     (0x010034C8)

  #define CH2CCC_CR_CTL2_parkval_ovr_val_OFF                           ( 0)
  #define CH2CCC_CR_CTL2_parkval_ovr_val_WID                           ( 1)
  #define CH2CCC_CR_CTL2_parkval_ovr_val_MSK                           (0x00000001)
  #define CH2CCC_CR_CTL2_parkval_ovr_val_MIN                           (0)
  #define CH2CCC_CR_CTL2_parkval_ovr_val_MAX                           (1) // 0x00000001
  #define CH2CCC_CR_CTL2_parkval_ovr_val_DEF                           (0x00000000)
  #define CH2CCC_CR_CTL2_parkval_ovr_val_HSH                           (0x010034C8)

  #define CH2CCC_CR_CTL2_parkval_ovr_sel_OFF                           ( 1)
  #define CH2CCC_CR_CTL2_parkval_ovr_sel_WID                           ( 1)
  #define CH2CCC_CR_CTL2_parkval_ovr_sel_MSK                           (0x00000002)
  #define CH2CCC_CR_CTL2_parkval_ovr_sel_MIN                           (0)
  #define CH2CCC_CR_CTL2_parkval_ovr_sel_MAX                           (1) // 0x00000001
  #define CH2CCC_CR_CTL2_parkval_ovr_sel_DEF                           (0x00000000)
  #define CH2CCC_CR_CTL2_parkval_ovr_sel_HSH                           (0x010234C8)

  #define CH2CCC_CR_CTL2_reserved9_OFF                                 ( 2)
  #define CH2CCC_CR_CTL2_reserved9_WID                                 ( 2)
  #define CH2CCC_CR_CTL2_reserved9_MSK                                 (0x0000000C)
  #define CH2CCC_CR_CTL2_reserved9_MIN                                 (0)
  #define CH2CCC_CR_CTL2_reserved9_MAX                                 (3) // 0x00000003
  #define CH2CCC_CR_CTL2_reserved9_DEF                                 (0x00000000)
  #define CH2CCC_CR_CTL2_reserved9_HSH                                 (0x020434C8)

  #define CH2CCC_CR_CTL2_cccrx_cmn_rxmvcmres_OFF                       ( 4)
  #define CH2CCC_CR_CTL2_cccrx_cmn_rxmvcmres_WID                       ( 2)
  #define CH2CCC_CR_CTL2_cccrx_cmn_rxmvcmres_MSK                       (0x00000030)
  #define CH2CCC_CR_CTL2_cccrx_cmn_rxmvcmres_MIN                       (0)
  #define CH2CCC_CR_CTL2_cccrx_cmn_rxmvcmres_MAX                       (3) // 0x00000003
  #define CH2CCC_CR_CTL2_cccrx_cmn_rxmvcmres_DEF                       (0x00000001)
  #define CH2CCC_CR_CTL2_cccrx_cmn_rxmvcmres_HSH                       (0x020834C8)

  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqconstz_OFF                      ( 6)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqconstz_WID                      ( 1)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqconstz_MSK                      (0x00000040)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqconstz_MIN                      (0)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqconstz_MAX                      (1) // 0x00000001
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqconstz_DEF                      (0x00000001)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqconstz_HSH                      (0x010C34C8)

  #define CH2CCC_CR_CTL2_bonus_4_OFF                                   ( 6)
  #define CH2CCC_CR_CTL2_bonus_4_WID                                   ( 1)
  #define CH2CCC_CR_CTL2_bonus_4_MSK                                   (0x00000040)
  #define CH2CCC_CR_CTL2_bonus_4_MIN                                   (0)
  #define CH2CCC_CR_CTL2_bonus_4_MAX                                   (1) // 0x00000001
  #define CH2CCC_CR_CTL2_bonus_4_DEF                                   (0x00000001)
  #define CH2CCC_CR_CTL2_bonus_4_HSH                                   (0x010C34C8)

  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenntap_OFF                      ( 7)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenntap_WID                      (15)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenntap_MSK                      (0x003FFF80)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenntap_MIN                      (0)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenntap_MAX                      (32767) // 0x00007FFF
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenntap_DEF                      (0x00000000)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenntap_HSH                      (0x0F0E34C8)

  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenpreset_OFF                    (22)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenpreset_WID                    ( 1)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenpreset_MSK                    (0x00400000)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenpreset_MIN                    (0)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenpreset_MAX                    (1) // 0x00000001
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenpreset_DEF                    (0x00000001)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txeqenpreset_HSH                    (0x012C34C8)

  #define CH2CCC_CR_CTL2_bonus_3_OFF                                   (23)
  #define CH2CCC_CR_CTL2_bonus_3_WID                                   ( 1)
  #define CH2CCC_CR_CTL2_bonus_3_MSK                                   (0x00800000)
  #define CH2CCC_CR_CTL2_bonus_3_MIN                                   (0)
  #define CH2CCC_CR_CTL2_bonus_3_MAX                                   (1) // 0x00000001
  #define CH2CCC_CR_CTL2_bonus_3_DEF                                   (0x00000000)
  #define CH2CCC_CR_CTL2_bonus_3_HSH                                   (0x012E34C8)

  #define CH2CCC_CR_CTL2_bonus_2_OFF                                   (24)
  #define CH2CCC_CR_CTL2_bonus_2_WID                                   ( 1)
  #define CH2CCC_CR_CTL2_bonus_2_MSK                                   (0x01000000)
  #define CH2CCC_CR_CTL2_bonus_2_MIN                                   (0)
  #define CH2CCC_CR_CTL2_bonus_2_MAX                                   (1) // 0x00000001
  #define CH2CCC_CR_CTL2_bonus_2_DEF                                   (0x00000000)
  #define CH2CCC_CR_CTL2_bonus_2_HSH                                   (0x013034C8)

  #define CH2CCC_CR_CTL2_pghub_cmn_ckes3segsel_OFF                     (25)
  #define CH2CCC_CR_CTL2_pghub_cmn_ckes3segsel_WID                     ( 1)
  #define CH2CCC_CR_CTL2_pghub_cmn_ckes3segsel_MSK                     (0x02000000)
  #define CH2CCC_CR_CTL2_pghub_cmn_ckes3segsel_MIN                     (0)
  #define CH2CCC_CR_CTL2_pghub_cmn_ckes3segsel_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_CTL2_pghub_cmn_ckes3segsel_DEF                     (0x00000001)
  #define CH2CCC_CR_CTL2_pghub_cmn_ckes3segsel_HSH                     (0x013234C8)

  #define CH2CCC_CR_CTL2_ccctx_cmn_txsegen_OFF                         (26)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txsegen_WID                         ( 2)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txsegen_MSK                         (0x0C000000)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txsegen_MIN                         (0)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txsegen_MAX                         (3) // 0x00000003
  #define CH2CCC_CR_CTL2_ccctx_cmn_txsegen_DEF                         (0x00000003)
  #define CH2CCC_CR_CTL2_ccctx_cmn_txsegen_HSH                         (0x023434C8)

  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrdval_OFF                (28)
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrdval_WID                ( 1)
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrdval_MSK                (0x10000000)
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrdval_MIN                (0)
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrdval_MAX                (1) // 0x00000001
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrdval_DEF                (0x00000000)
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrdval_HSH                (0x013834C8)

  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrden_OFF                 (29)
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrden_WID                 ( 1)
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrden_MSK                 (0x20000000)
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrden_MIN                 (0)
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrden_MAX                 (1) // 0x00000001
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrden_DEF                 (0x00000000)
  #define CH2CCC_CR_CTL2_txshim_cmn_txeqdataovrden_HSH                 (0x013A34C8)

  #define CH2CCC_CR_CTL2_rxsdl_cmn_parken_OFF                          (30)
  #define CH2CCC_CR_CTL2_rxsdl_cmn_parken_WID                          ( 1)
  #define CH2CCC_CR_CTL2_rxsdl_cmn_parken_MSK                          (0x40000000)
  #define CH2CCC_CR_CTL2_rxsdl_cmn_parken_MIN                          (0)
  #define CH2CCC_CR_CTL2_rxsdl_cmn_parken_MAX                          (1) // 0x00000001
  #define CH2CCC_CR_CTL2_rxsdl_cmn_parken_DEF                          (0x00000000)
  #define CH2CCC_CR_CTL2_rxsdl_cmn_parken_HSH                          (0x013C34C8)

  #define CH2CCC_CR_CTL2_ccctx_cmn_parken_OFF                          (31)
  #define CH2CCC_CR_CTL2_ccctx_cmn_parken_WID                          ( 1)
  #define CH2CCC_CR_CTL2_ccctx_cmn_parken_MSK                          (0x80000000)
  #define CH2CCC_CR_CTL2_ccctx_cmn_parken_MIN                          (0)
  #define CH2CCC_CR_CTL2_ccctx_cmn_parken_MAX                          (1) // 0x00000001
  #define CH2CCC_CR_CTL2_ccctx_cmn_parken_DEF                          (0x00000000)
  #define CH2CCC_CR_CTL2_ccctx_cmn_parken_HSH                          (0x013E34C8)

#define CH2CCC_CR_TXPBDCODE0_REG                                       (0x000034CC)

  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc0_txpbddlycode_OFF             ( 0)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc0_txpbddlycode_WID             ( 8)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc0_txpbddlycode_MSK             (0x000000FF)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc0_txpbddlycode_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc0_txpbddlycode_MAX             (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc0_txpbddlycode_DEF             (0x00000000)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc0_txpbddlycode_HSH             (0x080034CC)

  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc10_txpbddlycode_OFF            ( 8)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc10_txpbddlycode_WID            ( 8)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc10_txpbddlycode_MSK            (0x0000FF00)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc10_txpbddlycode_MIN            (0)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc10_txpbddlycode_MAX            (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc10_txpbddlycode_DEF            (0x00000000)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc10_txpbddlycode_HSH            (0x081034CC)

  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc11_txpbddlycode_OFF            (16)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc11_txpbddlycode_WID            ( 8)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc11_txpbddlycode_MSK            (0x00FF0000)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc11_txpbddlycode_MIN            (0)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc11_txpbddlycode_MAX            (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc11_txpbddlycode_DEF            (0x00000000)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc11_txpbddlycode_HSH            (0x082034CC)

  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc12_txpbddlycode_OFF            (24)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc12_txpbddlycode_WID            ( 8)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc12_txpbddlycode_MSK            (0xFF000000)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc12_txpbddlycode_MIN            (0)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc12_txpbddlycode_MAX            (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc12_txpbddlycode_DEF            (0x00000000)
  #define CH2CCC_CR_TXPBDCODE0_txpbd_ccc12_txpbddlycode_HSH            (0x083034CC)

#define CH2CCC_CR_TXPBDCODE1_REG                                       (0x000034D0)

  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc1_txpbddlycode_OFF             ( 0)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc1_txpbddlycode_WID             ( 8)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc1_txpbddlycode_MSK             (0x000000FF)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc1_txpbddlycode_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc1_txpbddlycode_MAX             (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc1_txpbddlycode_DEF             (0x00000000)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc1_txpbddlycode_HSH             (0x080034D0)

  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc2_txpbddlycode_OFF             ( 8)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc2_txpbddlycode_WID             ( 8)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc2_txpbddlycode_MSK             (0x0000FF00)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc2_txpbddlycode_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc2_txpbddlycode_MAX             (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc2_txpbddlycode_DEF             (0x00000000)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc2_txpbddlycode_HSH             (0x081034D0)

  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc3_txpbddlycode_OFF             (16)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc3_txpbddlycode_WID             ( 8)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc3_txpbddlycode_MSK             (0x00FF0000)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc3_txpbddlycode_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc3_txpbddlycode_MAX             (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc3_txpbddlycode_DEF             (0x00000000)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc3_txpbddlycode_HSH             (0x082034D0)

  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc4_txpbddlycode_OFF             (24)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc4_txpbddlycode_WID             ( 8)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc4_txpbddlycode_MSK             (0xFF000000)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc4_txpbddlycode_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc4_txpbddlycode_MAX             (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc4_txpbddlycode_DEF             (0x00000000)
  #define CH2CCC_CR_TXPBDCODE1_txpbd_ccc4_txpbddlycode_HSH             (0x083034D0)

#define CH2CCC_CR_TXPBDCODE2_REG                                       (0x000034D4)

  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc5_txpbddlycode_OFF             ( 0)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc5_txpbddlycode_WID             ( 8)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc5_txpbddlycode_MSK             (0x000000FF)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc5_txpbddlycode_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc5_txpbddlycode_MAX             (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc5_txpbddlycode_DEF             (0x00000000)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc5_txpbddlycode_HSH             (0x080034D4)

  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc6_txpbddlycode_OFF             ( 8)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc6_txpbddlycode_WID             ( 8)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc6_txpbddlycode_MSK             (0x0000FF00)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc6_txpbddlycode_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc6_txpbddlycode_MAX             (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc6_txpbddlycode_DEF             (0x00000000)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc6_txpbddlycode_HSH             (0x081034D4)

  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc7_txpbddlycode_OFF             (16)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc7_txpbddlycode_WID             ( 8)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc7_txpbddlycode_MSK             (0x00FF0000)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc7_txpbddlycode_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc7_txpbddlycode_MAX             (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc7_txpbddlycode_DEF             (0x00000000)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc7_txpbddlycode_HSH             (0x082034D4)

  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc8_txpbddlycode_OFF             (24)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc8_txpbddlycode_WID             ( 8)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc8_txpbddlycode_MSK             (0xFF000000)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc8_txpbddlycode_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc8_txpbddlycode_MAX             (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc8_txpbddlycode_DEF             (0x00000000)
  #define CH2CCC_CR_TXPBDCODE2_txpbd_ccc8_txpbddlycode_HSH             (0x083034D4)

#define CH2CCC_CR_TXPBDCODE3_REG                                       (0x000034D8)

  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc9_txpbddlycode_OFF             ( 0)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc9_txpbddlycode_WID             ( 8)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc9_txpbddlycode_MSK             (0x000000FF)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc9_txpbddlycode_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc9_txpbddlycode_MAX             (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc9_txpbddlycode_DEF             (0x00000000)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc9_txpbddlycode_HSH             (0x080034D8)

  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc13_txpbddlycode_OFF            ( 8)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc13_txpbddlycode_WID            ( 8)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc13_txpbddlycode_MSK            (0x0000FF00)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc13_txpbddlycode_MIN            (0)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc13_txpbddlycode_MAX            (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc13_txpbddlycode_DEF            (0x00000000)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc13_txpbddlycode_HSH            (0x081034D8)

  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc14_txpbddlycode_OFF            (16)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc14_txpbddlycode_WID            ( 8)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc14_txpbddlycode_MSK            (0x00FF0000)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc14_txpbddlycode_MIN            (0)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc14_txpbddlycode_MAX            (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc14_txpbddlycode_DEF            (0x00000000)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_ccc14_txpbddlycode_HSH            (0x082034D8)

  #define CH2CCC_CR_TXPBDCODE3_txpbd_cmn_txdccrangesel_OFF             (24)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_cmn_txdccrangesel_WID             ( 2)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_cmn_txdccrangesel_MSK             (0x03000000)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_cmn_txdccrangesel_MIN             (0)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_cmn_txdccrangesel_MAX             (3) // 0x00000003
  #define CH2CCC_CR_TXPBDCODE3_txpbd_cmn_txdccrangesel_DEF             (0x00000003)
  #define CH2CCC_CR_TXPBDCODE3_txpbd_cmn_txdccrangesel_HSH             (0x023034D8)

  #define CH2CCC_CR_TXPBDCODE3_mdll_cmn_dccrangesel_OFF                (26)
  #define CH2CCC_CR_TXPBDCODE3_mdll_cmn_dccrangesel_WID                ( 2)
  #define CH2CCC_CR_TXPBDCODE3_mdll_cmn_dccrangesel_MSK                (0x0C000000)
  #define CH2CCC_CR_TXPBDCODE3_mdll_cmn_dccrangesel_MIN                (0)
  #define CH2CCC_CR_TXPBDCODE3_mdll_cmn_dccrangesel_MAX                (3) // 0x00000003
  #define CH2CCC_CR_TXPBDCODE3_mdll_cmn_dccrangesel_DEF                (0x00000003)
  #define CH2CCC_CR_TXPBDCODE3_mdll_cmn_dccrangesel_HSH                (0x023434D8)

  #define CH2CCC_CR_TXPBDCODE3_Spare_OFF                               (28)
  #define CH2CCC_CR_TXPBDCODE3_Spare_WID                               ( 4)
  #define CH2CCC_CR_TXPBDCODE3_Spare_MSK                               (0xF0000000)
  #define CH2CCC_CR_TXPBDCODE3_Spare_MIN                               (0)
  #define CH2CCC_CR_TXPBDCODE3_Spare_MAX                               (15) // 0x0000000F
  #define CH2CCC_CR_TXPBDCODE3_Spare_DEF                               (0x00000000)
  #define CH2CCC_CR_TXPBDCODE3_Spare_HSH                               (0x043834D8)

#define CH2CCC_CR_TXPBDCODE4_REG                                       (0x000034DC)

  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc8_txpbddlycodeph90_OFF         ( 0)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc8_txpbddlycodeph90_WID         ( 8)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc8_txpbddlycodeph90_MSK         (0x000000FF)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc8_txpbddlycodeph90_MIN         (0)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc8_txpbddlycodeph90_MAX         (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc8_txpbddlycodeph90_DEF         (0x00000000)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc8_txpbddlycodeph90_HSH         (0x080034DC)

  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc7_txpbddlycodeph90_OFF         ( 8)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc7_txpbddlycodeph90_WID         ( 8)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc7_txpbddlycodeph90_MSK         (0x0000FF00)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc7_txpbddlycodeph90_MIN         (0)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc7_txpbddlycodeph90_MAX         (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc7_txpbddlycodeph90_DEF         (0x00000000)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc7_txpbddlycodeph90_HSH         (0x081034DC)

  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc6_txpbddlycodeph90_OFF         (16)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc6_txpbddlycodeph90_WID         ( 8)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc6_txpbddlycodeph90_MSK         (0x00FF0000)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc6_txpbddlycodeph90_MIN         (0)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc6_txpbddlycodeph90_MAX         (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc6_txpbddlycodeph90_DEF         (0x00000000)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc6_txpbddlycodeph90_HSH         (0x082034DC)

  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc5_txpbddlycodeph90_OFF         (24)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc5_txpbddlycodeph90_WID         ( 8)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc5_txpbddlycodeph90_MSK         (0xFF000000)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc5_txpbddlycodeph90_MIN         (0)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc5_txpbddlycodeph90_MAX         (255) // 0x000000FF
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc5_txpbddlycodeph90_DEF         (0x00000000)
  #define CH2CCC_CR_TXPBDCODE4_txpbd_ccc5_txpbddlycodeph90_HSH         (0x083034DC)

#define CH2CCC_CR_DCCCTL6_REG                                          (0x000034E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define CH2CCC_CR_DCCCTL7_REG                                          (0x000034E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define CH2CCC_CR_DCCCTL8_REG                                          (0x000034E8)

  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index7_OFF                      ( 0)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index7_WID                      ( 8)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index7_MSK                      (0x000000FF)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index7_MIN                      (0)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index7_MAX                      (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index7_DEF                      (0x00000080)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index7_HSH                      (0x080034E8)

  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index8_OFF                      ( 8)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index8_WID                      ( 8)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index8_MSK                      (0x0000FF00)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index8_MIN                      (0)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index8_MAX                      (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index8_DEF                      (0x00000080)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index8_HSH                      (0x081034E8)

  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index9_OFF                      (16)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index9_WID                      ( 8)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index9_MSK                      (0x00FF0000)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index9_MIN                      (0)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index9_MAX                      (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index9_DEF                      (0x00000080)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index9_HSH                      (0x082034E8)

  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index10_OFF                     (24)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index10_WID                     ( 8)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index10_MSK                     (0xFF000000)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index10_MIN                     (0)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index10_MAX                     (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index10_DEF                     (0x00000080)
  #define CH2CCC_CR_DCCCTL8_dcccodeph0_index10_HSH                     (0x083034E8)

#define CH2CCC_CR_DCCCTL9_REG                                          (0x000034EC)

  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index11_OFF                     ( 0)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index11_WID                     ( 8)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index11_MSK                     (0x000000FF)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index11_MIN                     (0)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index11_MAX                     (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index11_DEF                     (0x00000080)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index11_HSH                     (0x080034EC)

  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index12_OFF                     ( 8)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index12_WID                     ( 8)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index12_MSK                     (0x0000FF00)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index12_MIN                     (0)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index12_MAX                     (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index12_DEF                     (0x00000080)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index12_HSH                     (0x081034EC)

  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index13_OFF                     (16)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index13_WID                     ( 8)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index13_MSK                     (0x00FF0000)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index13_MIN                     (0)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index13_MAX                     (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index13_DEF                     (0x00000080)
  #define CH2CCC_CR_DCCCTL9_dcccodeph0_index13_HSH                     (0x082034EC)

  #define CH2CCC_CR_DCCCTL9_dcdrocal_en_A0_OFF                            (24)
  #define CH2CCC_CR_DCCCTL9_dcdrocal_en_A0_WID                            ( 1)
  #define CH2CCC_CR_DCCCTL9_dcdrocal_en_A0_MSK                            (0x01000000)
  #define CH2CCC_CR_DCCCTL9_dcdrocal_en_A0_MIN                            (0)
  #define CH2CCC_CR_DCCCTL9_dcdrocal_en_A0_MAX                            (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL9_dcdrocal_en_A0_DEF                            (0x00000000)
  #define CH2CCC_CR_DCCCTL9_dcdrocal_en_A0_HSH                            (0x013034EC)

  #define CH2CCC_CR_DCCCTL9_mdll_cmn_enabledcd_A0_OFF                     (25)
  #define CH2CCC_CR_DCCCTL9_mdll_cmn_enabledcd_A0_WID                     ( 1)
  #define CH2CCC_CR_DCCCTL9_mdll_cmn_enabledcd_A0_MSK                     (0x02000000)
  #define CH2CCC_CR_DCCCTL9_mdll_cmn_enabledcd_A0_MIN                     (0)
  #define CH2CCC_CR_DCCCTL9_mdll_cmn_enabledcd_A0_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL9_mdll_cmn_enabledcd_A0_DEF                     (0x00000000)
  #define CH2CCC_CR_DCCCTL9_mdll_cmn_enabledcd_A0_HSH                     (0x013234EC)

  #define CH2CCC_CR_DCCCTL9_dcdsrz_cmn_dcdenable_A0_OFF                   (26)
  #define CH2CCC_CR_DCCCTL9_dcdsrz_cmn_dcdenable_A0_WID                   ( 1)
  #define CH2CCC_CR_DCCCTL9_dcdsrz_cmn_dcdenable_A0_MSK                   (0x04000000)
  #define CH2CCC_CR_DCCCTL9_dcdsrz_cmn_dcdenable_A0_MIN                   (0)
  #define CH2CCC_CR_DCCCTL9_dcdsrz_cmn_dcdenable_A0_MAX                   (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL9_dcdsrz_cmn_dcdenable_A0_DEF                   (0x00000000)
  #define CH2CCC_CR_DCCCTL9_dcdsrz_cmn_dcdenable_A0_HSH                   (0x013434EC)

  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_A0_OFF                           (27)
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_A0_WID                           ( 1)
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_A0_MSK                           (0x08000000)
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_A0_MIN                           (0)
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_A0_MAX                           (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_A0_DEF                           (0x00000000)
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_A0_HSH                           (0x013634EC)

  #define CH2CCC_CR_DCCCTL9_fatal_cal_A0_OFF                              (28)
  #define CH2CCC_CR_DCCCTL9_fatal_cal_A0_WID                              ( 1)
  #define CH2CCC_CR_DCCCTL9_fatal_cal_A0_MSK                              (0x10000000)
  #define CH2CCC_CR_DCCCTL9_fatal_cal_A0_MIN                              (0)
  #define CH2CCC_CR_DCCCTL9_fatal_cal_A0_MAX                              (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL9_fatal_cal_A0_DEF                              (0x00000000)
  #define CH2CCC_CR_DCCCTL9_fatal_cal_A0_HSH                              (0x013834EC)

  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_OFF                           (24)
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_WID                           ( 1)
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_MSK                           (0x01000000)
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_MIN                           (0)
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_MAX                           (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_DEF                           (0x00000000)
  #define CH2CCC_CR_DCCCTL9_dccfsm_rst_b_HSH                           (0x013034EC)

  #define CH2CCC_CR_DCCCTL9_maxdccsteps_OFF                            (25)
  #define CH2CCC_CR_DCCCTL9_maxdccsteps_WID                            ( 7)
  #define CH2CCC_CR_DCCCTL9_maxdccsteps_MSK                            (0xFE000000)
  #define CH2CCC_CR_DCCCTL9_maxdccsteps_MIN                            (0)
  #define CH2CCC_CR_DCCCTL9_maxdccsteps_MAX                            (127) // 0x0000007F
  #define CH2CCC_CR_DCCCTL9_maxdccsteps_DEF                            (0x00000000)
  #define CH2CCC_CR_DCCCTL9_maxdccsteps_HSH                            (0x073234EC)

#define CH2CCC_CR_DCCCTL11_REG                                         (0x000034F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define CH2CCC_CR_DCCCTL20_REG                                         (0x000034F4)

  #define CH2CCC_CR_DCCCTL20_dcdrocal_en_OFF                           ( 0)
  #define CH2CCC_CR_DCCCTL20_dcdrocal_en_WID                           ( 1)
  #define CH2CCC_CR_DCCCTL20_dcdrocal_en_MSK                           (0x00000001)
  #define CH2CCC_CR_DCCCTL20_dcdrocal_en_MIN                           (0)
  #define CH2CCC_CR_DCCCTL20_dcdrocal_en_MAX                           (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_dcdrocal_en_DEF                           (0x00000000)
  #define CH2CCC_CR_DCCCTL20_dcdrocal_en_HSH                           (0x010034F4)

  #define CH2CCC_CR_DCCCTL20_mdll_cmn_enabledcd_OFF                    ( 1)
  #define CH2CCC_CR_DCCCTL20_mdll_cmn_enabledcd_WID                    ( 1)
  #define CH2CCC_CR_DCCCTL20_mdll_cmn_enabledcd_MSK                    (0x00000002)
  #define CH2CCC_CR_DCCCTL20_mdll_cmn_enabledcd_MIN                    (0)
  #define CH2CCC_CR_DCCCTL20_mdll_cmn_enabledcd_MAX                    (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_mdll_cmn_enabledcd_DEF                    (0x00000000)
  #define CH2CCC_CR_DCCCTL20_mdll_cmn_enabledcd_HSH                    (0x010234F4)

  #define CH2CCC_CR_DCCCTL20_dcdsrz_cmn_dcdenable_OFF                  ( 2)
  #define CH2CCC_CR_DCCCTL20_dcdsrz_cmn_dcdenable_WID                  ( 1)
  #define CH2CCC_CR_DCCCTL20_dcdsrz_cmn_dcdenable_MSK                  (0x00000004)
  #define CH2CCC_CR_DCCCTL20_dcdsrz_cmn_dcdenable_MIN                  (0)
  #define CH2CCC_CR_DCCCTL20_dcdsrz_cmn_dcdenable_MAX                  (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_dcdsrz_cmn_dcdenable_DEF                  (0x00000000)
  #define CH2CCC_CR_DCCCTL20_dcdsrz_cmn_dcdenable_HSH                  (0x010434F4)

  #define CH2CCC_CR_DCCCTL20_fatal_cal_OFF                             ( 3)
  #define CH2CCC_CR_DCCCTL20_fatal_cal_WID                             ( 1)
  #define CH2CCC_CR_DCCCTL20_fatal_cal_MSK                             (0x00000008)
  #define CH2CCC_CR_DCCCTL20_fatal_cal_MIN                             (0)
  #define CH2CCC_CR_DCCCTL20_fatal_cal_MAX                             (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_fatal_cal_DEF                             (0x00000000)
  #define CH2CCC_CR_DCCCTL20_fatal_cal_HSH                             (0x010634F4)

  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonrise_OFF             ( 4)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonrise_WID             ( 1)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonrise_MSK             (0x00000010)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonrise_MIN             (0)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonrise_MAX             (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonrise_DEF             (0x00000000)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonrise_HSH             (0x010834F4)

  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonfall_OFF             ( 5)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonfall_WID             ( 1)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonfall_MSK             (0x00000020)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonfall_MIN             (0)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonfall_MAX             (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonfall_DEF             (0x00000000)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc8_txpfirstonfall_HSH             (0x010A34F4)

  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonrise_OFF             ( 6)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonrise_WID             ( 1)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonrise_MSK             (0x00000040)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonrise_MIN             (0)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonrise_MAX             (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonrise_DEF             (0x00000000)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonrise_HSH             (0x010C34F4)

  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonfall_OFF             ( 7)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonfall_WID             ( 1)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonfall_MSK             (0x00000080)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonfall_MIN             (0)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonfall_MAX             (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonfall_DEF             (0x00000000)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc7_txpfirstonfall_HSH             (0x010E34F4)

  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonrise_OFF             ( 8)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonrise_WID             ( 1)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonrise_MSK             (0x00000100)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonrise_MIN             (0)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonrise_MAX             (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonrise_DEF             (0x00000000)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonrise_HSH             (0x011034F4)

  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonfall_OFF             ( 9)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonfall_WID             ( 1)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonfall_MSK             (0x00000200)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonfall_MIN             (0)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonfall_MAX             (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonfall_DEF             (0x00000000)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc6_txpfirstonfall_HSH             (0x011234F4)

  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonrise_OFF             (10)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonrise_WID             ( 1)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonrise_MSK             (0x00000400)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonrise_MIN             (0)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonrise_MAX             (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonrise_DEF             (0x00000000)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonrise_HSH             (0x011434F4)

  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonfall_OFF             (11)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonfall_WID             ( 1)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonfall_MSK             (0x00000800)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonfall_MIN             (0)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonfall_MAX             (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonfall_DEF             (0x00000000)
  #define CH2CCC_CR_DCCCTL20_ccctx_ccc5_txpfirstonfall_HSH             (0x011634F4)

  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountstart_OFF              (12)
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountstart_WID              ( 1)
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountstart_MSK              (0x00001000)
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountstart_MIN              (0)
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountstart_MAX              (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountstart_DEF              (0x00000000)
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountstart_HSH              (0x011834F4)

  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountrst_b_OFF              (13)
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountrst_b_WID              ( 1)
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountrst_b_MSK              (0x00002000)
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountrst_b_MIN              (0)
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountrst_b_MAX              (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountrst_b_DEF              (0x00000000)
  #define CH2CCC_CR_DCCCTL20_dcdrocalsamplecountrst_b_HSH              (0x011A34F4)

  #define CH2CCC_CR_DCCCTL20_dcdrocal_start_OFF                        (14)
  #define CH2CCC_CR_DCCCTL20_dcdrocal_start_WID                        ( 1)
  #define CH2CCC_CR_DCCCTL20_dcdrocal_start_MSK                        (0x00004000)
  #define CH2CCC_CR_DCCCTL20_dcdrocal_start_MIN                        (0)
  #define CH2CCC_CR_DCCCTL20_dcdrocal_start_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_dcdrocal_start_DEF                        (0x00000000)
  #define CH2CCC_CR_DCCCTL20_dcdrocal_start_HSH                        (0x011C34F4)

  #define CH2CCC_CR_DCCCTL20_dcc_start_OFF                             (15)
  #define CH2CCC_CR_DCCCTL20_dcc_start_WID                             ( 1)
  #define CH2CCC_CR_DCCCTL20_dcc_start_MSK                             (0x00008000)
  #define CH2CCC_CR_DCCCTL20_dcc_start_MIN                             (0)
  #define CH2CCC_CR_DCCCTL20_dcc_start_MAX                             (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL20_dcc_start_DEF                             (0x00000000)
  #define CH2CCC_CR_DCCCTL20_dcc_start_HSH                             (0x011E34F4)

  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index15_OFF                    (16)
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index15_WID                    ( 8)
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index15_MSK                    (0x00FF0000)
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index15_MIN                    (0)
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index15_MAX                    (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index15_DEF                    (0x00000080)
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index15_HSH                    (0x082034F4)

  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index14_OFF                    (24)
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index14_WID                    ( 8)
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index14_MSK                    (0xFF000000)
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index14_MIN                    (0)
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index14_MAX                    (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index14_DEF                    (0x00000080)
  #define CH2CCC_CR_DCCCTL20_dcccodeph0_index14_HSH                    (0x083034F4)

#define CH2CCC_CR_DDRCRCCCPIDIVIDER_REG                                (0x00003500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0DivEn_OFF                     ( 0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0DivEn_WID                     ( 1)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0DivEn_MSK                     (0x00000001)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0DivEn_MIN                     (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0DivEn_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0DivEn_DEF                     (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0DivEn_HSH                     (0x01003500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0Inc_OFF                       ( 1)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0Inc_WID                       ( 2)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0Inc_MSK                       (0x00000006)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0Inc_MIN                       (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0Inc_MAX                       (3) // 0x00000003
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0Inc_DEF                       (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi0Inc_HSH                       (0x02023500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1DivEn_OFF                     ( 3)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1DivEn_WID                     ( 1)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1DivEn_MSK                     (0x00000008)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1DivEn_MIN                     (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1DivEn_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1DivEn_DEF                     (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1DivEn_HSH                     (0x01063500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1Inc_OFF                       ( 4)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1Inc_WID                       ( 2)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1Inc_MSK                       (0x00000030)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1Inc_MIN                       (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1Inc_MAX                       (3) // 0x00000003
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1Inc_DEF                       (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi1Inc_HSH                       (0x02083500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2DivEn_OFF                     ( 6)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2DivEn_WID                     ( 1)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2DivEn_MSK                     (0x00000040)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2DivEn_MIN                     (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2DivEn_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2DivEn_DEF                     (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2DivEn_HSH                     (0x010C3500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2Inc_OFF                       ( 7)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2Inc_WID                       ( 2)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2Inc_MSK                       (0x00000180)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2Inc_MIN                       (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2Inc_MAX                       (3) // 0x00000003
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2Inc_DEF                       (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi2Inc_HSH                       (0x020E3500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3DivEn_OFF                     ( 9)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3DivEn_WID                     ( 1)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3DivEn_MSK                     (0x00000200)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3DivEn_MIN                     (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3DivEn_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3DivEn_DEF                     (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3DivEn_HSH                     (0x01123500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3Inc_OFF                       (10)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3Inc_WID                       ( 2)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3Inc_MSK                       (0x00000C00)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3Inc_MIN                       (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3Inc_MAX                       (3) // 0x00000003
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3Inc_DEF                       (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi3Inc_HSH                       (0x02143500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEn_OFF                     (12)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEn_WID                     ( 1)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEn_MSK                     (0x00001000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEn_MIN                     (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEn_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEn_DEF                     (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEn_HSH                     (0x01183500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4Inc_OFF                       (13)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4Inc_WID                       ( 2)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4Inc_MSK                       (0x00006000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4Inc_MIN                       (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4Inc_MAX                       (3) // 0x00000003
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4Inc_DEF                       (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4Inc_HSH                       (0x021A3500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEnPreamble_OFF             (15)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEnPreamble_WID             ( 1)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEnPreamble_MSK             (0x00008000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEnPreamble_MIN             (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEnPreamble_MAX             (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEnPreamble_DEF             (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4DivEnPreamble_HSH             (0x011E3500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4IncPreamble_OFF               (16)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4IncPreamble_WID               ( 2)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4IncPreamble_MSK               (0x00030000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4IncPreamble_MIN               (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4IncPreamble_MAX               (3) // 0x00000003
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4IncPreamble_DEF               (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_Pi4IncPreamble_HSH               (0x02203500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiSyncDivider_OFF                (18)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiSyncDivider_WID                ( 2)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiSyncDivider_MSK                (0x000C0000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiSyncDivider_MIN                (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiSyncDivider_MAX                (3) // 0x00000003
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiSyncDivider_DEF                (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiSyncDivider_HSH                (0x02243500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_WckHalfPreamble_OFF              (20)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_WckHalfPreamble_WID              ( 1)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_WckHalfPreamble_MSK              (0x00100000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_WckHalfPreamble_MIN              (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_WckHalfPreamble_MAX              (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_WckHalfPreamble_DEF              (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_WckHalfPreamble_HSH              (0x01283500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiClkDuration_OFF                (21)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiClkDuration_WID                ( 3)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiClkDuration_MSK                (0x00E00000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiClkDuration_MIN                (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiClkDuration_MAX                (7) // 0x00000007
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiClkDuration_DEF                (0x00000007)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_PiClkDuration_HSH                (0x032A3500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_wck2cktrainen_OFF                (24)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_wck2cktrainen_WID                ( 1)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_wck2cktrainen_MSK                (0x01000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_wck2cktrainen_MIN                (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_wck2cktrainen_MAX                (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_wck2cktrainen_DEF                (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_wck2cktrainen_HSH                (0x01303500)

  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_ccc_Spare1_OFF                   (25)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_ccc_Spare1_WID                   ( 7)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_ccc_Spare1_MSK                   (0xFE000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_ccc_Spare1_MIN                   (0)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_ccc_Spare1_MAX                   (127) // 0x0000007F
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_ccc_Spare1_DEF                   (0x00000000)
  #define CH2CCC_CR_DDRCRCCCPIDIVIDER_ccc_Spare1_HSH                   (0x07323500)

#define CH2CCC_CR_CLKALIGNCTL0_REG                                     (0x00003504)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define CH2CCC_CR_CLKALIGNCTL1_REG                                     (0x00003508)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define CH2CCC_CR_CLKALIGNCTL2_REG                                     (0x0000350C)

  #define CH2CCC_CR_CLKALIGNCTL2_pdoffset_ovr_OFF                      ( 0)
  #define CH2CCC_CR_CLKALIGNCTL2_pdoffset_ovr_WID                      ( 1)
  #define CH2CCC_CR_CLKALIGNCTL2_pdoffset_ovr_MSK                      (0x00000001)
  #define CH2CCC_CR_CLKALIGNCTL2_pdoffset_ovr_MIN                      (0)
  #define CH2CCC_CR_CLKALIGNCTL2_pdoffset_ovr_MAX                      (1) // 0x00000001
  #define CH2CCC_CR_CLKALIGNCTL2_pdoffset_ovr_DEF                      (0x00000000)
  #define CH2CCC_CR_CLKALIGNCTL2_pdoffset_ovr_HSH                      (0x0100350C)

  #define CH2CCC_CR_CLKALIGNCTL2_txdll_cmdd0_pdoffset_OFF              ( 1)
  #define CH2CCC_CR_CLKALIGNCTL2_txdll_cmdd0_pdoffset_WID              ( 8)
  #define CH2CCC_CR_CLKALIGNCTL2_txdll_cmdd0_pdoffset_MSK              (0x000001FE)
  #define CH2CCC_CR_CLKALIGNCTL2_txdll_cmdd0_pdoffset_MIN              (0)
  #define CH2CCC_CR_CLKALIGNCTL2_txdll_cmdd0_pdoffset_MAX              (255) // 0x000000FF
  #define CH2CCC_CR_CLKALIGNCTL2_txdll_cmdd0_pdoffset_DEF              (0x00000000)
  #define CH2CCC_CR_CLKALIGNCTL2_txdll_cmdd0_pdoffset_HSH              (0x0802350C)

  #define CH2CCC_CR_CLKALIGNCTL2_d0tosig_offset_OFF                    ( 9)
  #define CH2CCC_CR_CLKALIGNCTL2_d0tosig_offset_WID                    ( 8)
  #define CH2CCC_CR_CLKALIGNCTL2_d0tosig_offset_MSK                    (0x0001FE00)
  #define CH2CCC_CR_CLKALIGNCTL2_d0tosig_offset_MIN                    (0)
  #define CH2CCC_CR_CLKALIGNCTL2_d0tosig_offset_MAX                    (255) // 0x000000FF
  #define CH2CCC_CR_CLKALIGNCTL2_d0tosig_offset_DEF                    (0x00000000)
  #define CH2CCC_CR_CLKALIGNCTL2_d0tosig_offset_HSH                    (0x0812350C)

  #define CH2CCC_CR_CLKALIGNCTL2_reserved10_OFF                        (17)
  #define CH2CCC_CR_CLKALIGNCTL2_reserved10_WID                        (15)
  #define CH2CCC_CR_CLKALIGNCTL2_reserved10_MSK                        (0xFFFE0000)
  #define CH2CCC_CR_CLKALIGNCTL2_reserved10_MIN                        (0)
  #define CH2CCC_CR_CLKALIGNCTL2_reserved10_MAX                        (32767) // 0x00007FFF
  #define CH2CCC_CR_CLKALIGNCTL2_reserved10_DEF                        (0x00000000)
  #define CH2CCC_CR_CLKALIGNCTL2_reserved10_HSH                        (0x0F22350C)

#define CH2CCC_CR_WLCTRL_A0_REG                                        (0x00003530)
#define CH2CCC_CR_WLCTRL_REG                                           (0x00003510)

  #define CH2CCC_CR_WLCTRL_wlslowclken_dly_OFF                         ( 0)
  #define CH2CCC_CR_WLCTRL_wlslowclken_dly_WID                         ( 5)
  #define CH2CCC_CR_WLCTRL_wlslowclken_dly_MSK                         (0x0000001F)
  #define CH2CCC_CR_WLCTRL_wlslowclken_dly_MIN                         (0)
  #define CH2CCC_CR_WLCTRL_wlslowclken_dly_MAX                         (31) // 0x0000001F
  #define CH2CCC_CR_WLCTRL_wlslowclken_dly_DEF                         (0x00000004)
  #define CH2CCC_CR_WLCTRL_wlslowclken_dly_HSH                         (0x05003510)

  #define CH2CCC_CR_WLCTRL_weaklocken_dly_OFF                          ( 5)
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_WID                          ( 5)
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_MSK                          (0x000003E0)
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_MIN                          (0)
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_MAX                          (31) // 0x0000001F
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_DEF                          (0x00000004)
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_HSH                          (0x050A3510)

  #define CH2CCC_CR_WLCTRL_initdlllock_dly_OFF                         ( 0)
  #define CH2CCC_CR_WLCTRL_initdlllock_dly_WID                         ( 4)
  #define CH2CCC_CR_WLCTRL_initdlllock_dly_MSK                         (0x0000000F)
  #define CH2CCC_CR_WLCTRL_initdlllock_dly_MIN                         (0)
  #define CH2CCC_CR_WLCTRL_initdlllock_dly_MAX                         (15) // 0x0000000F
  #define CH2CCC_CR_WLCTRL_initdlllock_dly_DEF                         (0x00000004)
  #define CH2CCC_CR_WLCTRL_initdlllock_dly_HSH                         (0x04003510)

  #define CH2CCC_CR_WLCTRL_wlrelock_dly_OFF                            ( 4)
  #define CH2CCC_CR_WLCTRL_wlrelock_dly_WID                            ( 4)
  #define CH2CCC_CR_WLCTRL_wlrelock_dly_MSK                            (0x000000F0)
  #define CH2CCC_CR_WLCTRL_wlrelock_dly_MIN                            (0)
  #define CH2CCC_CR_WLCTRL_wlrelock_dly_MAX                            (15) // 0x0000000F
  #define CH2CCC_CR_WLCTRL_wlrelock_dly_DEF                            (0x00000004)
  #define CH2CCC_CR_WLCTRL_wlrelock_dly_HSH                            (0x04083510)

  #define CH2CCC_CR_WLCTRL_weaklocken_dly_Q0_OFF                          ( 8)
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_Q0_WID                          ( 5)
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_Q0_MSK                          (0x00001F00)
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_Q0_MIN                          (0)
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_Q0_MAX                          (31) // 0x0000001F
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_Q0_DEF                          (0x00000004)
  #define CH2CCC_CR_WLCTRL_weaklocken_dly_Q0_HSH                          (0x05103510)

  #define CH2CCC_CR_WLCTRL_spare_OFF                                   (13)
  #define CH2CCC_CR_WLCTRL_spare_WID                                   ( 1)
  #define CH2CCC_CR_WLCTRL_spare_MSK                                   (0x00002000)
  #define CH2CCC_CR_WLCTRL_spare_MIN                                   (0)
  #define CH2CCC_CR_WLCTRL_spare_MAX                                   (1) // 0x00000001
  #define CH2CCC_CR_WLCTRL_spare_DEF                                   (0x00000000)
  #define CH2CCC_CR_WLCTRL_spare_HSH                                   (0x011A3510)

  #define CH2CCC_CR_WLCTRL_lp_weaklocken_OFF                           (14)
  #define CH2CCC_CR_WLCTRL_lp_weaklocken_WID                           ( 1)
  #define CH2CCC_CR_WLCTRL_lp_weaklocken_MSK                           (0x00004000)
  #define CH2CCC_CR_WLCTRL_lp_weaklocken_MIN                           (0)
  #define CH2CCC_CR_WLCTRL_lp_weaklocken_MAX                           (1) // 0x00000001
  #define CH2CCC_CR_WLCTRL_lp_weaklocken_DEF                           (0x00000000)
  #define CH2CCC_CR_WLCTRL_lp_weaklocken_HSH                           (0x011C3510)

  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_sel_OFF                      (15)
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_sel_WID                      ( 1)
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_sel_MSK                      (0x00008000)
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_sel_MIN                      (0)
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_sel_MAX                      (1) // 0x00000001
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_sel_DEF                      (0x00000000)
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_sel_HSH                      (0x011E3510)

  #define CH2CCC_CR_WLCTRL_wlcomp_samplewait_OFF                       (16)
  #define CH2CCC_CR_WLCTRL_wlcomp_samplewait_WID                       ( 5)
  #define CH2CCC_CR_WLCTRL_wlcomp_samplewait_MSK                       (0x001F0000)
  #define CH2CCC_CR_WLCTRL_wlcomp_samplewait_MIN                       (0)
  #define CH2CCC_CR_WLCTRL_wlcomp_samplewait_MAX                       (31) // 0x0000001F
  #define CH2CCC_CR_WLCTRL_wlcomp_samplewait_DEF                       (0x00000006)
  #define CH2CCC_CR_WLCTRL_wlcomp_samplewait_HSH                       (0x05203510)

  #define CH2CCC_CR_WLCTRL_wlcomp_per_stepsize_OFF                     (21)
  #define CH2CCC_CR_WLCTRL_wlcomp_per_stepsize_WID                     ( 3)
  #define CH2CCC_CR_WLCTRL_wlcomp_per_stepsize_MSK                     (0x00E00000)
  #define CH2CCC_CR_WLCTRL_wlcomp_per_stepsize_MIN                     (0)
  #define CH2CCC_CR_WLCTRL_wlcomp_per_stepsize_MAX                     (7) // 0x00000007
  #define CH2CCC_CR_WLCTRL_wlcomp_per_stepsize_DEF                     (0x00000003)
  #define CH2CCC_CR_WLCTRL_wlcomp_per_stepsize_HSH                     (0x032A3510)

  #define CH2CCC_CR_WLCTRL_wlcomp_init_stepsize_OFF                    (24)
  #define CH2CCC_CR_WLCTRL_wlcomp_init_stepsize_WID                    ( 3)
  #define CH2CCC_CR_WLCTRL_wlcomp_init_stepsize_MSK                    (0x07000000)
  #define CH2CCC_CR_WLCTRL_wlcomp_init_stepsize_MIN                    (0)
  #define CH2CCC_CR_WLCTRL_wlcomp_init_stepsize_MAX                    (7) // 0x00000007
  #define CH2CCC_CR_WLCTRL_wlcomp_init_stepsize_DEF                    (0x00000001)
  #define CH2CCC_CR_WLCTRL_wlcomp_init_stepsize_HSH                    (0x03303510)

  #define CH2CCC_CR_WLCTRL_weaklockcomp_err_OFF                        (27)
  #define CH2CCC_CR_WLCTRL_weaklockcomp_err_WID                        ( 1)
  #define CH2CCC_CR_WLCTRL_weaklockcomp_err_MSK                        (0x08000000)
  #define CH2CCC_CR_WLCTRL_weaklockcomp_err_MIN                        (0)
  #define CH2CCC_CR_WLCTRL_weaklockcomp_err_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_WLCTRL_weaklockcomp_err_DEF                        (0x00000000)
  #define CH2CCC_CR_WLCTRL_weaklockcomp_err_HSH                        (0x01363510)

  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_OFF                          (28)
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_WID                          ( 1)
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_MSK                          (0x10000000)
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_MIN                          (0)
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_MAX                          (1) // 0x00000001
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_DEF                          (0x00000000)
  #define CH2CCC_CR_WLCTRL_weaklocken_ovr_HSH                          (0x01383510)

  #define CH2CCC_CR_WLCTRL_wlcomp_clkgatedis_OFF                       (29)
  #define CH2CCC_CR_WLCTRL_wlcomp_clkgatedis_WID                       ( 1)
  #define CH2CCC_CR_WLCTRL_wlcomp_clkgatedis_MSK                       (0x20000000)
  #define CH2CCC_CR_WLCTRL_wlcomp_clkgatedis_MIN                       (0)
  #define CH2CCC_CR_WLCTRL_wlcomp_clkgatedis_MAX                       (1) // 0x00000001
  #define CH2CCC_CR_WLCTRL_wlcomp_clkgatedis_DEF                       (0x00000000)
  #define CH2CCC_CR_WLCTRL_wlcomp_clkgatedis_HSH                       (0x013A3510)

  #define CH2CCC_CR_WLCTRL_weaklocken_OFF                              (30)
  #define CH2CCC_CR_WLCTRL_weaklocken_WID                              ( 1)
  #define CH2CCC_CR_WLCTRL_weaklocken_MSK                              (0x40000000)
  #define CH2CCC_CR_WLCTRL_weaklocken_MIN                              (0)
  #define CH2CCC_CR_WLCTRL_weaklocken_MAX                              (1) // 0x00000001
  #define CH2CCC_CR_WLCTRL_weaklocken_DEF                              (0x00000000)
  #define CH2CCC_CR_WLCTRL_weaklocken_HSH                              (0x013C3510)

  #define CH2CCC_CR_WLCTRL_weaklockcomp_en_OFF                         (31)
  #define CH2CCC_CR_WLCTRL_weaklockcomp_en_WID                         ( 1)
  #define CH2CCC_CR_WLCTRL_weaklockcomp_en_MSK                         (0x80000000)
  #define CH2CCC_CR_WLCTRL_weaklockcomp_en_MIN                         (0)
  #define CH2CCC_CR_WLCTRL_weaklockcomp_en_MAX                         (1) // 0x00000001
  #define CH2CCC_CR_WLCTRL_weaklockcomp_en_DEF                         (0x00000000)
  #define CH2CCC_CR_WLCTRL_weaklockcomp_en_HSH                         (0x013E3510)

#define CH2CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG                           (0x00003510)
#define CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG                              (0x00003514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkGateDisable_OFF             ( 0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkGateDisable_WID             ( 1)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkGateDisable_MSK             (0x00000001)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkGateDisable_MIN             (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkGateDisable_MAX             (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkGateDisable_DEF             (0x00000001)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkGateDisable_HSH             (0x01003514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_DefDrvEnLow_OFF                ( 1)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_DefDrvEnLow_WID                ( 2)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_DefDrvEnLow_MSK                (0x00000006)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_DefDrvEnLow_MIN                (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_DefDrvEnLow_MAX                (3) // 0x00000003
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_DefDrvEnLow_DEF                (0x00000001)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_DefDrvEnLow_HSH                (0x02023514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RTO_OFF                        ( 3)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RTO_WID                        ( 1)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RTO_MSK                        (0x00000008)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RTO_MIN                        (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RTO_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RTO_DEF                        (0x00000000)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RTO_HSH                        (0x01063514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_IntCkOn_OFF                    ( 4)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_IntCkOn_WID                    ( 1)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_IntCkOn_MSK                    (0x00000010)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_IntCkOn_MIN                    (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_IntCkOn_MAX                    (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_IntCkOn_DEF                    (0x00000001)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_IntCkOn_HSH                    (0x01083514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CkeIdlePiGateDisable_OFF       ( 5)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CkeIdlePiGateDisable_WID       ( 1)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CkeIdlePiGateDisable_MSK       (0x00000020)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CkeIdlePiGateDisable_MIN       (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CkeIdlePiGateDisable_MAX       (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CkeIdlePiGateDisable_DEF       (0x00000001)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CkeIdlePiGateDisable_HSH       (0x010A3514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaValidPiGateDisable_OFF       ( 6)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaValidPiGateDisable_WID       ( 1)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaValidPiGateDisable_MSK       (0x00000040)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaValidPiGateDisable_MIN       (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaValidPiGateDisable_MAX       (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaValidPiGateDisable_DEF       (0x00000001)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaValidPiGateDisable_HSH       (0x010C3514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaTxEq_OFF                     ( 7)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaTxEq_WID                     ( 5)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaTxEq_MSK                     (0x00000F80)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaTxEq_MIN                     (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaTxEq_MAX                     (31) // 0x0000001F
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaTxEq_DEF                     (0x00000001)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaTxEq_HSH                     (0x050E3514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlTxEq_OFF                    (12)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlTxEq_WID                    ( 5)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlTxEq_MSK                    (0x0001F000)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlTxEq_MIN                    (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlTxEq_MAX                    (31) // 0x0000001F
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlTxEq_DEF                    (0x00000001)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlTxEq_HSH                    (0x05183514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkTxEq_OFF                    (17)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkTxEq_WID                    ( 5)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkTxEq_MSK                    (0x003E0000)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkTxEq_MIN                    (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkTxEq_MAX                    (31) // 0x0000001F
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkTxEq_DEF                    (0x00000001)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_ClkTxEq_HSH                    (0x05223514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlSRDrv_OFF                   (22)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlSRDrv_WID                   ( 2)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlSRDrv_MSK                   (0x00C00000)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlSRDrv_MIN                   (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlSRDrv_MAX                   (3) // 0x00000003
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlSRDrv_DEF                   (0x00000000)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlSRDrv_HSH                   (0x022C3514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_c3segsel_b_for_cke_OFF         (24)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_c3segsel_b_for_cke_WID         ( 1)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_c3segsel_b_for_cke_MSK         (0x01000000)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_c3segsel_b_for_cke_MIN         (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_c3segsel_b_for_cke_MAX         (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_c3segsel_b_for_cke_DEF         (0x00000001)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_c3segsel_b_for_cke_HSH         (0x01303514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RxVref_OFF                     (25)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RxVref_WID                     ( 6)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RxVref_MSK                     (0x7E000000)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RxVref_MIN                     (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RxVref_MAX                     (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RxVref_DEF                     (0x00000020)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_RxVref_HSH                     (0x06323514)

  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_BlockTrainRst_OFF              (31)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_BlockTrainRst_WID              ( 1)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_BlockTrainRst_MSK              (0x80000000)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_BlockTrainRst_MIN              (0)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_BlockTrainRst_MAX              (1) // 0x00000001
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_BlockTrainRst_DEF              (0x00000000)
  #define CH2CCC_CR_DDRCRCCCCLKCONTROLS_BlockTrainRst_HSH              (0x013E3514)

#define CH2CCC_CR_TXPBDOFFSET_A0_REG                                   (0x0000352C)
#define CH2CCC_CR_TXPBDOFFSET_REG                                      (0x00003518)

  #define CH2CCC_CR_TXPBDOFFSET_Reserved11_OFF                         ( 0)
  #define CH2CCC_CR_TXPBDOFFSET_Reserved11_WID                         ( 4)
  #define CH2CCC_CR_TXPBDOFFSET_Reserved11_MSK                         (0x0000000F)
  #define CH2CCC_CR_TXPBDOFFSET_Reserved11_MIN                         (0)
  #define CH2CCC_CR_TXPBDOFFSET_Reserved11_MAX                         (15) // 0x0000000F
  #define CH2CCC_CR_TXPBDOFFSET_Reserved11_DEF                         (0x00000000)
  #define CH2CCC_CR_TXPBDOFFSET_Reserved11_HSH                         (0x04003518)

  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbd_ph90incr_OFF          ( 4)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbd_ph90incr_WID          ( 1)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbd_ph90incr_MSK          (0x00000010)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbd_ph90incr_MIN          (0)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbd_ph90incr_MAX          (1) // 0x00000001
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbd_ph90incr_DEF          (0x00000000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbd_ph90incr_HSH          (0x01083518)

  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbd_ph90incr_OFF          ( 5)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbd_ph90incr_WID          ( 1)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbd_ph90incr_MSK          (0x00000020)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbd_ph90incr_MIN          (0)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbd_ph90incr_MAX          (1) // 0x00000001
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbd_ph90incr_DEF          (0x00000000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbd_ph90incr_HSH          (0x010A3518)

  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbd_ph90incr_OFF          ( 6)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbd_ph90incr_WID          ( 1)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbd_ph90incr_MSK          (0x00000040)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbd_ph90incr_MIN          (0)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbd_ph90incr_MAX          (1) // 0x00000001
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbd_ph90incr_DEF          (0x00000000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbd_ph90incr_HSH          (0x010C3518)

  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbd_ph90incr_OFF          ( 7)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbd_ph90incr_WID          ( 1)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbd_ph90incr_MSK          (0x00000080)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbd_ph90incr_MIN          (0)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbd_ph90incr_MAX          (1) // 0x00000001
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbd_ph90incr_DEF          (0x00000000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbd_ph90incr_HSH          (0x010E3518)

  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbddoffset_OFF            ( 8)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbddoffset_WID            ( 6)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbddoffset_MSK            (0x00003F00)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbddoffset_MIN            (0)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbddoffset_MAX            (63) // 0x0000003F
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbddoffset_DEF            (0x00000000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbddoffset_HSH            (0x06103518)

  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbddoffset_OFF            (14)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbddoffset_WID            ( 6)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbddoffset_MSK            (0x000FC000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbddoffset_MIN            (0)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbddoffset_MAX            (63) // 0x0000003F
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbddoffset_DEF            (0x00000000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbddoffset_HSH            (0x061C3518)

  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbddoffset_OFF            (20)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbddoffset_WID            ( 6)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbddoffset_MSK            (0x03F00000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbddoffset_MIN            (0)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbddoffset_MAX            (63) // 0x0000003F
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbddoffset_DEF            (0x00000000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbddoffset_HSH            (0x06283518)

  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbddoffset_OFF            (26)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbddoffset_WID            ( 6)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbddoffset_MSK            (0xFC000000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbddoffset_MIN            (0)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbddoffset_MAX            (63) // 0x0000003F
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbddoffset_DEF            (0x00000000)
  #define CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbddoffset_HSH            (0x06343518)

#define CH2CCC_CR_DCCCTL0_A0_REG                                       (0x00003514)
#define CH2CCC_CR_DCCCTL0_REG                                          (0x0000351C)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define CH2CCC_CR_DCCCTL1_A0_REG                                       (0x00003518)
#define CH2CCC_CR_DCCCTL1_REG                                          (0x00003520)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define CH2CCC_CR_DCCCTL2_A0_REG                                       (0x0000351C)
#define CH2CCC_CR_DCCCTL2_REG                                          (0x00003524)

  #define CH2CCC_CR_DCCCTL2_Reserved12_OFF                             ( 0)
  #define CH2CCC_CR_DCCCTL2_Reserved12_WID                             ( 2)
  #define CH2CCC_CR_DCCCTL2_Reserved12_MSK                             (0x00000003)
  #define CH2CCC_CR_DCCCTL2_Reserved12_MIN                             (0)
  #define CH2CCC_CR_DCCCTL2_Reserved12_MAX                             (3) // 0x00000003
  #define CH2CCC_CR_DCCCTL2_Reserved12_DEF                             (0x00000000)
  #define CH2CCC_CR_DCCCTL2_Reserved12_HSH                             (0x02003524)

  #define CH2CCC_CR_DCCCTL2_dcc_clken_ovr_OFF                          ( 2)
  #define CH2CCC_CR_DCCCTL2_dcc_clken_ovr_WID                          ( 1)
  #define CH2CCC_CR_DCCCTL2_dcc_clken_ovr_MSK                          (0x00000004)
  #define CH2CCC_CR_DCCCTL2_dcc_clken_ovr_MIN                          (0)
  #define CH2CCC_CR_DCCCTL2_dcc_clken_ovr_MAX                          (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL2_dcc_clken_ovr_DEF                          (0x00000000)
  #define CH2CCC_CR_DCCCTL2_dcc_clken_ovr_HSH                          (0x01043524)

  #define CH2CCC_CR_DCCCTL2_dcdsamplecountstart_OFF                    ( 3)
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountstart_WID                    ( 1)
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountstart_MSK                    (0x00000008)
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountstart_MIN                    (0)
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountstart_MAX                    (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountstart_DEF                    (0x00000000)
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountstart_HSH                    (0x01063524)

  #define CH2CCC_CR_DCCCTL2_dcdsamplecountrst_b_OFF                    ( 4)
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountrst_b_WID                    ( 1)
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountrst_b_MSK                    (0x00000010)
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountrst_b_MIN                    (0)
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountrst_b_MAX                    (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountrst_b_DEF                    (0x00000000)
  #define CH2CCC_CR_DCCCTL2_dcdsamplecountrst_b_HSH                    (0x01083524)

  #define CH2CCC_CR_DCCCTL2_dcc_serdata_ovren_OFF                      ( 5)
  #define CH2CCC_CR_DCCCTL2_dcc_serdata_ovren_WID                      ( 1)
  #define CH2CCC_CR_DCCCTL2_dcc_serdata_ovren_MSK                      (0x00000020)
  #define CH2CCC_CR_DCCCTL2_dcc_serdata_ovren_MIN                      (0)
  #define CH2CCC_CR_DCCCTL2_dcc_serdata_ovren_MAX                      (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL2_dcc_serdata_ovren_DEF                      (0x00000000)
  #define CH2CCC_CR_DCCCTL2_dcc_serdata_ovren_HSH                      (0x010A3524)

  #define CH2CCC_CR_DCCCTL2_dcdrovcoprog_OFF                           ( 6)
  #define CH2CCC_CR_DCCCTL2_dcdrovcoprog_WID                           ( 3)
  #define CH2CCC_CR_DCCCTL2_dcdrovcoprog_MSK                           (0x000001C0)
  #define CH2CCC_CR_DCCCTL2_dcdrovcoprog_MIN                           (0)
  #define CH2CCC_CR_DCCCTL2_dcdrovcoprog_MAX                           (7) // 0x00000007
  #define CH2CCC_CR_DCCCTL2_dcdrovcoprog_DEF                           (0x00000000)
  #define CH2CCC_CR_DCCCTL2_dcdrovcoprog_HSH                           (0x030C3524)

  #define CH2CCC_CR_DCCCTL2_dcdrolfsr_OFF                              ( 9)
  #define CH2CCC_CR_DCCCTL2_dcdrolfsr_WID                              (10)
  #define CH2CCC_CR_DCCCTL2_dcdrolfsr_MSK                              (0x0007FE00)
  #define CH2CCC_CR_DCCCTL2_dcdrolfsr_MIN                              (0)
  #define CH2CCC_CR_DCCCTL2_dcdrolfsr_MAX                              (1023) // 0x000003FF
  #define CH2CCC_CR_DCCCTL2_dcdrolfsr_DEF                              (0x00000000)
  #define CH2CCC_CR_DCCCTL2_dcdrolfsr_HSH                              (0x0A123524)

  #define CH2CCC_CR_DCCCTL2_scr_fsm_dccctrlcodestatus_sel_OFF          (19)
  #define CH2CCC_CR_DCCCTL2_scr_fsm_dccctrlcodestatus_sel_WID          ( 4)
  #define CH2CCC_CR_DCCCTL2_scr_fsm_dccctrlcodestatus_sel_MSK          (0x00780000)
  #define CH2CCC_CR_DCCCTL2_scr_fsm_dccctrlcodestatus_sel_MIN          (0)
  #define CH2CCC_CR_DCCCTL2_scr_fsm_dccctrlcodestatus_sel_MAX          (15) // 0x0000000F
  #define CH2CCC_CR_DCCCTL2_scr_fsm_dccctrlcodestatus_sel_DEF          (0x00000000)
  #define CH2CCC_CR_DCCCTL2_scr_fsm_dccctrlcodestatus_sel_HSH          (0x04263524)

  #define CH2CCC_CR_DCCCTL2_compare_dly_OFF                            (23)
  #define CH2CCC_CR_DCCCTL2_compare_dly_WID                            ( 4)
  #define CH2CCC_CR_DCCCTL2_compare_dly_MSK                            (0x07800000)
  #define CH2CCC_CR_DCCCTL2_compare_dly_MIN                            (0)
  #define CH2CCC_CR_DCCCTL2_compare_dly_MAX                            (15) // 0x0000000F
  #define CH2CCC_CR_DCCCTL2_compare_dly_DEF                            (0x00000000)
  #define CH2CCC_CR_DCCCTL2_compare_dly_HSH                            (0x042E3524)

  #define CH2CCC_CR_DCCCTL2_dcdreset_dly_OFF                           (27)
  #define CH2CCC_CR_DCCCTL2_dcdreset_dly_WID                           ( 3)
  #define CH2CCC_CR_DCCCTL2_dcdreset_dly_MSK                           (0x38000000)
  #define CH2CCC_CR_DCCCTL2_dcdreset_dly_MIN                           (0)
  #define CH2CCC_CR_DCCCTL2_dcdreset_dly_MAX                           (7) // 0x00000007
  #define CH2CCC_CR_DCCCTL2_dcdreset_dly_DEF                           (0x00000000)
  #define CH2CCC_CR_DCCCTL2_dcdreset_dly_HSH                           (0x03363524)

  #define CH2CCC_CR_DCCCTL2_clknclkpsample_avg_en_OFF                  (30)
  #define CH2CCC_CR_DCCCTL2_clknclkpsample_avg_en_WID                  ( 1)
  #define CH2CCC_CR_DCCCTL2_clknclkpsample_avg_en_MSK                  (0x40000000)
  #define CH2CCC_CR_DCCCTL2_clknclkpsample_avg_en_MIN                  (0)
  #define CH2CCC_CR_DCCCTL2_clknclkpsample_avg_en_MAX                  (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL2_clknclkpsample_avg_en_DEF                  (0x00000000)
  #define CH2CCC_CR_DCCCTL2_clknclkpsample_avg_en_HSH                  (0x013C3524)

  #define CH2CCC_CR_DCCCTL2_threeloop_en_OFF                           (31)
  #define CH2CCC_CR_DCCCTL2_threeloop_en_WID                           ( 1)
  #define CH2CCC_CR_DCCCTL2_threeloop_en_MSK                           (0x80000000)
  #define CH2CCC_CR_DCCCTL2_threeloop_en_MIN                           (0)
  #define CH2CCC_CR_DCCCTL2_threeloop_en_MAX                           (1) // 0x00000001
  #define CH2CCC_CR_DCCCTL2_threeloop_en_DEF                           (0x00000000)
  #define CH2CCC_CR_DCCCTL2_threeloop_en_HSH                           (0x013E3524)

#define CH2CCC_CR_DCCCTL3_A0_REG                                       (0x00003520)
#define CH2CCC_CR_DCCCTL3_REG                                          (0x00003528)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define CH2CCC_CR_DCCCTL5_A0_REG                                       (0x00003524)
#define CH2CCC_CR_DCCCTL5_REG                                          (0x0000352C)

  #define CH2CCC_CR_DCCCTL5_dcc_step3pattern_enable_OFF                ( 0)
  #define CH2CCC_CR_DCCCTL5_dcc_step3pattern_enable_WID                (16)
  #define CH2CCC_CR_DCCCTL5_dcc_step3pattern_enable_MSK                (0x0000FFFF)
  #define CH2CCC_CR_DCCCTL5_dcc_step3pattern_enable_MIN                (0)
  #define CH2CCC_CR_DCCCTL5_dcc_step3pattern_enable_MAX                (65535) // 0x0000FFFF
  #define CH2CCC_CR_DCCCTL5_dcc_step3pattern_enable_DEF                (0x00000000)
  #define CH2CCC_CR_DCCCTL5_dcc_step3pattern_enable_HSH                (0x1000352C)

  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph0_status_OFF                  (16)
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph0_status_WID                  ( 8)
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph0_status_MSK                  (0x00FF0000)
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph0_status_MIN                  (0)
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph0_status_MAX                  (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph0_status_DEF                  (0x00000000)
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph0_status_HSH                  (0x0820352C)

  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph90_status_OFF                 (24)
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph90_status_WID                 ( 8)
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph90_status_MSK                 (0xFF000000)
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph90_status_MIN                 (0)
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph90_status_MAX                 (255) // 0x000000FF
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph90_status_DEF                 (0x00000000)
  #define CH2CCC_CR_DCCCTL5_dccctrlcodeph90_status_HSH                 (0x0830352C)

#define CH2CCC_CR_DCCCTL13_A0_REG                                      (0x00003528)
#define CH2CCC_CR_DCCCTL13_REG                                         (0x00003530)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define CH2CCC_CR_PMFSM_REG                                            (0x00003534)

  #define CH2CCC_CR_PMFSM_spare_OFF                                    ( 0)
  #define CH2CCC_CR_PMFSM_spare_WID                                    (20)
  #define CH2CCC_CR_PMFSM_spare_MSK                                    (0x000FFFFF)
  #define CH2CCC_CR_PMFSM_spare_MIN                                    (0)
  #define CH2CCC_CR_PMFSM_spare_MAX                                    (1048575) // 0x000FFFFF
  #define CH2CCC_CR_PMFSM_spare_DEF                                    (0x00000000)
  #define CH2CCC_CR_PMFSM_spare_HSH                                    (0x14003534)

  #define CH2CCC_CR_PMFSM_DdrxxClkGoodQnnnH_OFF                        (20)
  #define CH2CCC_CR_PMFSM_DdrxxClkGoodQnnnH_WID                        ( 1)
  #define CH2CCC_CR_PMFSM_DdrxxClkGoodQnnnH_MSK                        (0x00100000)
  #define CH2CCC_CR_PMFSM_DdrxxClkGoodQnnnH_MIN                        (0)
  #define CH2CCC_CR_PMFSM_DdrxxClkGoodQnnnH_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_PMFSM_DdrxxClkGoodQnnnH_DEF                        (0x00000000)
  #define CH2CCC_CR_PMFSM_DdrxxClkGoodQnnnH_HSH                        (0x01283534)

  #define CH2CCC_CR_PMFSM_current_pmctrlfsm_state_OFF                  (21)
  #define CH2CCC_CR_PMFSM_current_pmctrlfsm_state_WID                  ( 4)
  #define CH2CCC_CR_PMFSM_current_pmctrlfsm_state_MSK                  (0x01E00000)
  #define CH2CCC_CR_PMFSM_current_pmctrlfsm_state_MIN                  (0)
  #define CH2CCC_CR_PMFSM_current_pmctrlfsm_state_MAX                  (15) // 0x0000000F
  #define CH2CCC_CR_PMFSM_current_pmctrlfsm_state_DEF                  (0x00000000)
  #define CH2CCC_CR_PMFSM_current_pmctrlfsm_state_HSH                  (0x042A3534)

  #define CH2CCC_CR_PMFSM_Vsshipwrgood_OFF                             (25)
  #define CH2CCC_CR_PMFSM_Vsshipwrgood_WID                             ( 1)
  #define CH2CCC_CR_PMFSM_Vsshipwrgood_MSK                             (0x02000000)
  #define CH2CCC_CR_PMFSM_Vsshipwrgood_MIN                             (0)
  #define CH2CCC_CR_PMFSM_Vsshipwrgood_MAX                             (1) // 0x00000001
  #define CH2CCC_CR_PMFSM_Vsshipwrgood_DEF                             (0x00000000)
  #define CH2CCC_CR_PMFSM_Vsshipwrgood_HSH                             (0x01323534)

  #define CH2CCC_CR_PMFSM_initdone_status_OFF                          (26)
  #define CH2CCC_CR_PMFSM_initdone_status_WID                          ( 1)
  #define CH2CCC_CR_PMFSM_initdone_status_MSK                          (0x04000000)
  #define CH2CCC_CR_PMFSM_initdone_status_MIN                          (0)
  #define CH2CCC_CR_PMFSM_initdone_status_MAX                          (1) // 0x00000001
  #define CH2CCC_CR_PMFSM_initdone_status_DEF                          (0x00000000)
  #define CH2CCC_CR_PMFSM_initdone_status_HSH                          (0x01343534)

  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrden_OFF                         (27)
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrden_WID                         ( 1)
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrden_MSK                         (0x08000000)
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrden_MIN                         (0)
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrden_MAX                         (1) // 0x00000001
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrden_DEF                         (0x00000000)
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrden_HSH                         (0x01363534)

  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrdval_OFF                        (28)
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrdval_WID                        ( 4)
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrdval_MSK                        (0xF0000000)
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrdval_MIN                        (0)
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrdval_MAX                        (15) // 0x0000000F
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrdval_DEF                        (0x00000000)
  #define CH2CCC_CR_PMFSM_pmctrlfsm_ovrdval_HSH                        (0x04383534)

#define CH2CCC_CR_DDRCRCMDCOMP_REG                                     (0x00003538)

  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvUp_OFF                        ( 0)
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvUp_WID                        ( 6)
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvUp_MSK                        (0x0000003F)
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvUp_MIN                        (0)
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvUp_MAX                        (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvUp_DEF                        (0x00000020)
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvUp_HSH                        (0x06003538)

  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvDown_OFF                      ( 6)
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvDown_WID                      ( 6)
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvDown_MSK                      (0x00000FC0)
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvDown_MIN                      (0)
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvDown_MAX                      (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvDown_DEF                      (0x00000020)
  #define CH2CCC_CR_DDRCRCMDCOMP_RcompDrvDown_HSH                      (0x060C3538)

  #define CH2CCC_CR_DDRCRCMDCOMP_ScompCmd_OFF                          (12)
  #define CH2CCC_CR_DDRCRCMDCOMP_ScompCmd_WID                          ( 8)
  #define CH2CCC_CR_DDRCRCMDCOMP_ScompCmd_MSK                          (0x000FF000)
  #define CH2CCC_CR_DDRCRCMDCOMP_ScompCmd_MIN                          (0)
  #define CH2CCC_CR_DDRCRCMDCOMP_ScompCmd_MAX                          (255) // 0x000000FF
  #define CH2CCC_CR_DDRCRCMDCOMP_ScompCmd_DEF                          (0x00000000)
  #define CH2CCC_CR_DDRCRCMDCOMP_ScompCmd_HSH                          (0x08183538)

  #define CH2CCC_CR_DDRCRCMDCOMP_VssHiFF_cmd_OFF                       (20)
  #define CH2CCC_CR_DDRCRCMDCOMP_VssHiFF_cmd_WID                       ( 6)
  #define CH2CCC_CR_DDRCRCMDCOMP_VssHiFF_cmd_MSK                       (0x03F00000)
  #define CH2CCC_CR_DDRCRCMDCOMP_VssHiFF_cmd_MIN                       (0)
  #define CH2CCC_CR_DDRCRCMDCOMP_VssHiFF_cmd_MAX                       (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCMDCOMP_VssHiFF_cmd_DEF                       (0x00000020)
  #define CH2CCC_CR_DDRCRCMDCOMP_VssHiFF_cmd_HSH                       (0x06283538)

  #define CH2CCC_CR_DDRCRCMDCOMP_reserved13_OFF                        (26)
  #define CH2CCC_CR_DDRCRCMDCOMP_reserved13_WID                        ( 6)
  #define CH2CCC_CR_DDRCRCMDCOMP_reserved13_MSK                        (0xFC000000)
  #define CH2CCC_CR_DDRCRCMDCOMP_reserved13_MIN                        (0)
  #define CH2CCC_CR_DDRCRCMDCOMP_reserved13_MAX                        (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCMDCOMP_reserved13_DEF                        (0x00000020)
  #define CH2CCC_CR_DDRCRCMDCOMP_reserved13_HSH                        (0x06343538)

#define CH2CCC_CR_DDRCRCLKCOMP_REG                                     (0x0000353C)

  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvUp_OFF                        ( 0)
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvUp_WID                        ( 6)
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvUp_MSK                        (0x0000003F)
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvUp_MIN                        (0)
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvUp_MAX                        (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvUp_DEF                        (0x00000020)
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvUp_HSH                        (0x0600353C)

  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvDown_OFF                      ( 6)
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvDown_WID                      ( 6)
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvDown_MSK                      (0x00000FC0)
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvDown_MIN                      (0)
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvDown_MAX                      (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvDown_DEF                      (0x00000020)
  #define CH2CCC_CR_DDRCRCLKCOMP_RcompDrvDown_HSH                      (0x060C353C)

  #define CH2CCC_CR_DDRCRCLKCOMP_ScompClk_OFF                          (12)
  #define CH2CCC_CR_DDRCRCLKCOMP_ScompClk_WID                          ( 8)
  #define CH2CCC_CR_DDRCRCLKCOMP_ScompClk_MSK                          (0x000FF000)
  #define CH2CCC_CR_DDRCRCLKCOMP_ScompClk_MIN                          (0)
  #define CH2CCC_CR_DDRCRCLKCOMP_ScompClk_MAX                          (255) // 0x000000FF
  #define CH2CCC_CR_DDRCRCLKCOMP_ScompClk_DEF                          (0x00000000)
  #define CH2CCC_CR_DDRCRCLKCOMP_ScompClk_HSH                          (0x0818353C)

  #define CH2CCC_CR_DDRCRCLKCOMP_VssHiFF_ctl_OFF                       (20)
  #define CH2CCC_CR_DDRCRCLKCOMP_VssHiFF_ctl_WID                       ( 6)
  #define CH2CCC_CR_DDRCRCLKCOMP_VssHiFF_ctl_MSK                       (0x03F00000)
  #define CH2CCC_CR_DDRCRCLKCOMP_VssHiFF_ctl_MIN                       (0)
  #define CH2CCC_CR_DDRCRCLKCOMP_VssHiFF_ctl_MAX                       (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCLKCOMP_VssHiFF_ctl_DEF                       (0x00000020)
  #define CH2CCC_CR_DDRCRCLKCOMP_VssHiFF_ctl_HSH                       (0x0628353C)

  #define CH2CCC_CR_DDRCRCLKCOMP_reserved14_OFF                        (26)
  #define CH2CCC_CR_DDRCRCLKCOMP_reserved14_WID                        ( 6)
  #define CH2CCC_CR_DDRCRCLKCOMP_reserved14_MSK                        (0xFC000000)
  #define CH2CCC_CR_DDRCRCLKCOMP_reserved14_MIN                        (0)
  #define CH2CCC_CR_DDRCRCLKCOMP_reserved14_MAX                        (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCLKCOMP_reserved14_DEF                        (0x00000000)
  #define CH2CCC_CR_DDRCRCLKCOMP_reserved14_HSH                        (0x0634353C)

#define CH2CCC_CR_DDRCRCTLCOMP_REG                                     (0x00003540)

  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvUp_OFF                        ( 0)
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvUp_WID                        ( 6)
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvUp_MSK                        (0x0000003F)
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvUp_MIN                        (0)
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvUp_MAX                        (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvUp_DEF                        (0x00000020)
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvUp_HSH                        (0x06003540)

  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvDown_OFF                      ( 6)
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvDown_WID                      ( 6)
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvDown_MSK                      (0x00000FC0)
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvDown_MIN                      (0)
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvDown_MAX                      (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvDown_DEF                      (0x00000020)
  #define CH2CCC_CR_DDRCRCTLCOMP_RcompDrvDown_HSH                      (0x060C3540)

  #define CH2CCC_CR_DDRCRCTLCOMP_ScompCtl_OFF                          (12)
  #define CH2CCC_CR_DDRCRCTLCOMP_ScompCtl_WID                          ( 8)
  #define CH2CCC_CR_DDRCRCTLCOMP_ScompCtl_MSK                          (0x000FF000)
  #define CH2CCC_CR_DDRCRCTLCOMP_ScompCtl_MIN                          (0)
  #define CH2CCC_CR_DDRCRCTLCOMP_ScompCtl_MAX                          (255) // 0x000000FF
  #define CH2CCC_CR_DDRCRCTLCOMP_ScompCtl_DEF                          (0x00000000)
  #define CH2CCC_CR_DDRCRCTLCOMP_ScompCtl_HSH                          (0x08183540)

  #define CH2CCC_CR_DDRCRCTLCOMP_VssHiFF_clk_OFF                       (20)
  #define CH2CCC_CR_DDRCRCTLCOMP_VssHiFF_clk_WID                       ( 6)
  #define CH2CCC_CR_DDRCRCTLCOMP_VssHiFF_clk_MSK                       (0x03F00000)
  #define CH2CCC_CR_DDRCRCTLCOMP_VssHiFF_clk_MIN                       (0)
  #define CH2CCC_CR_DDRCRCTLCOMP_VssHiFF_clk_MAX                       (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCTLCOMP_VssHiFF_clk_DEF                       (0x00000020)
  #define CH2CCC_CR_DDRCRCTLCOMP_VssHiFF_clk_HSH                       (0x06283540)

  #define CH2CCC_CR_DDRCRCTLCOMP_CkeCsUp_OFF                           (26)
  #define CH2CCC_CR_DDRCRCTLCOMP_CkeCsUp_WID                           ( 6)
  #define CH2CCC_CR_DDRCRCTLCOMP_CkeCsUp_MSK                           (0xFC000000)
  #define CH2CCC_CR_DDRCRCTLCOMP_CkeCsUp_MIN                           (0)
  #define CH2CCC_CR_DDRCRCTLCOMP_CkeCsUp_MAX                           (63) // 0x0000003F
  #define CH2CCC_CR_DDRCRCTLCOMP_CkeCsUp_DEF                           (0x00000000)
  #define CH2CCC_CR_DDRCRCTLCOMP_CkeCsUp_HSH                           (0x06343540)

#define CH2CCC_CR_VCCDLLCOMPDATACCC_REG                                (0x00003544)

  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codepi_OFF                   ( 0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codepi_WID                   ( 6)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codepi_MSK                   (0x0000003F)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codepi_MIN                   (0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codepi_MAX                   (63) // 0x0000003F
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codepi_DEF                   (0x00000020)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codepi_HSH                   (0x06003544)

  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codewl_OFF                   ( 6)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codewl_WID                   ( 6)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codewl_MSK                   (0x00000FC0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codewl_MIN                   (0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codewl_MAX                   (63) // 0x0000003F
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codewl_DEF                   (0x00000020)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_codewl_HSH                   (0x060C3544)

  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_bwsel_OFF                    (12)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_bwsel_WID                    ( 6)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_bwsel_MSK                    (0x0003F000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_bwsel_MIN                    (0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_bwsel_MAX                    (63) // 0x0000003F
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_bwsel_DEF                    (0x00000020)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_bwsel_HSH                    (0x06183544)

  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_OFF                 (18)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_WID                 ( 8)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_MSK                 (0x03FC0000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_MIN                 (0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_MAX                 (255) // 0x000000FF
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_DEF                 (0x00000080)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_HSH                 (0x08243544)

  #define CH2CCC_CR_VCCDLLCOMPDATACCC_rloaddqs_OFF                     (18)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_rloaddqs_WID                     ( 6)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_rloaddqs_MSK                     (0x00FC0000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_rloaddqs_MIN                     (0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_rloaddqs_MAX                     (63) // 0x0000003F
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_rloaddqs_DEF                     (0x00000020)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_rloaddqs_HSH                     (0x06243544)

  #define CH2CCC_CR_VCCDLLCOMPDATACCC_cben_OFF                         (24)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_cben_WID                         ( 2)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_cben_MSK                         (0x03000000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_cben_MIN                         (0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_cben_MAX                         (3) // 0x00000003
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_cben_DEF                         (0x00000000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_cben_HSH                         (0x02303544)

  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Offsetnui_OFF                    (26)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Offsetnui_WID                    ( 2)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Offsetnui_MSK                    (0x0C000000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Offsetnui_MIN                    (0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Offsetnui_MAX                    (3) // 0x00000003
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Offsetnui_DEF                    (0x00000000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Offsetnui_HSH                    (0x02343544)

  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Targetnui_OFF                    (28)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Targetnui_WID                    ( 2)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Targetnui_MSK                    (0x30000000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Targetnui_MIN                    (0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Targetnui_MAX                    (3) // 0x00000003
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Targetnui_DEF                    (0x00000000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Targetnui_HSH                    (0x02383544)

  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Spare_OFF                        (30)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Spare_WID                        ( 2)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Spare_MSK                        (0xC0000000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Spare_MIN                        (0)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Spare_MAX                        (3) // 0x00000003
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Spare_DEF                        (0x00000000)
  #define CH2CCC_CR_VCCDLLCOMPDATACCC_Spare_HSH                        (0x023C3544)

#define CH2CCC_CR_DCSOFFSET_REG                                        (0x00003548)

  #define CH2CCC_CR_DCSOFFSET_dccoffset_OFF                            ( 0)
  #define CH2CCC_CR_DCSOFFSET_dccoffset_WID                            ( 8)
  #define CH2CCC_CR_DCSOFFSET_dccoffset_MSK                            (0x000000FF)
  #define CH2CCC_CR_DCSOFFSET_dccoffset_MIN                            (0)
  #define CH2CCC_CR_DCSOFFSET_dccoffset_MAX                            (255) // 0x000000FF
  #define CH2CCC_CR_DCSOFFSET_dccoffset_DEF                            (0x00000000)
  #define CH2CCC_CR_DCSOFFSET_dccoffset_HSH                            (0x08003548)

  #define CH2CCC_CR_DCSOFFSET_dccoffsetsign_OFF                        ( 8)
  #define CH2CCC_CR_DCSOFFSET_dccoffsetsign_WID                        ( 1)
  #define CH2CCC_CR_DCSOFFSET_dccoffsetsign_MSK                        (0x00000100)
  #define CH2CCC_CR_DCSOFFSET_dccoffsetsign_MIN                        (0)
  #define CH2CCC_CR_DCSOFFSET_dccoffsetsign_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_DCSOFFSET_dccoffsetsign_DEF                        (0x00000000)
  #define CH2CCC_CR_DCSOFFSET_dccoffsetsign_HSH                        (0x01103548)

  #define CH2CCC_CR_DCSOFFSET_reserved15_OFF                           ( 9)
  #define CH2CCC_CR_DCSOFFSET_reserved15_WID                           (23)
  #define CH2CCC_CR_DCSOFFSET_reserved15_MSK                           (0xFFFFFE00)
  #define CH2CCC_CR_DCSOFFSET_reserved15_MIN                           (0)
  #define CH2CCC_CR_DCSOFFSET_reserved15_MAX                           (8388607) // 0x007FFFFF
  #define CH2CCC_CR_DCSOFFSET_reserved15_DEF                           (0x00000000)
  #define CH2CCC_CR_DCSOFFSET_reserved15_HSH                           (0x17123548)

#define CH2CCC_CR_DDRCRBSCANDATA_REG                                   (0x0000354C)

  #define CH2CCC_CR_DDRCRBSCANDATA_DdrBscanEnable_OFF                  ( 0)
  #define CH2CCC_CR_DDRCRBSCANDATA_DdrBscanEnable_WID                  ( 1)
  #define CH2CCC_CR_DDRCRBSCANDATA_DdrBscanEnable_MSK                  (0x00000001)
  #define CH2CCC_CR_DDRCRBSCANDATA_DdrBscanEnable_MIN                  (0)
  #define CH2CCC_CR_DDRCRBSCANDATA_DdrBscanEnable_MAX                  (1) // 0x00000001
  #define CH2CCC_CR_DDRCRBSCANDATA_DdrBscanEnable_DEF                  (0x00000000)
  #define CH2CCC_CR_DDRCRBSCANDATA_DdrBscanEnable_HSH                  (0x0100354C)

  #define CH2CCC_CR_DDRCRBSCANDATA_DataValue_OFF                       ( 1)
  #define CH2CCC_CR_DDRCRBSCANDATA_DataValue_WID                       (15)
  #define CH2CCC_CR_DDRCRBSCANDATA_DataValue_MSK                       (0x0000FFFE)
  #define CH2CCC_CR_DDRCRBSCANDATA_DataValue_MIN                       (0)
  #define CH2CCC_CR_DDRCRBSCANDATA_DataValue_MAX                       (32767) // 0x00007FFF
  #define CH2CCC_CR_DDRCRBSCANDATA_DataValue_DEF                       (0x00000000)
  #define CH2CCC_CR_DDRCRBSCANDATA_DataValue_HSH                       (0x0F02354C)

  #define CH2CCC_CR_DDRCRBSCANDATA_BiasRloadVref_OFF                   (16)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasRloadVref_WID                   ( 3)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasRloadVref_MSK                   (0x00070000)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasRloadVref_MIN                   (0)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasRloadVref_MAX                   (7) // 0x00000007
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasRloadVref_DEF                   (0x00000004)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasRloadVref_HSH                   (0x0320354C)

  #define CH2CCC_CR_DDRCRBSCANDATA_BiasIrefAdj_OFF                     (19)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasIrefAdj_WID                     ( 4)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasIrefAdj_MSK                     (0x00780000)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasIrefAdj_MIN                     (0)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasIrefAdj_MAX                     (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasIrefAdj_DEF                     (0x00000008)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasIrefAdj_HSH                     (0x0426354C)

  #define CH2CCC_CR_DDRCRBSCANDATA_BiasCasAdj_OFF                      (23)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasCasAdj_WID                      ( 2)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasCasAdj_MSK                      (0x01800000)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasCasAdj_MIN                      (0)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasCasAdj_MAX                      (3) // 0x00000003
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasCasAdj_DEF                      (0x00000002)
  #define CH2CCC_CR_DDRCRBSCANDATA_BiasCasAdj_HSH                      (0x022E354C)

  #define CH2CCC_CR_DDRCRBSCANDATA_Spare_OFF                           (25)
  #define CH2CCC_CR_DDRCRBSCANDATA_Spare_WID                           ( 7)
  #define CH2CCC_CR_DDRCRBSCANDATA_Spare_MSK                           (0xFE000000)
  #define CH2CCC_CR_DDRCRBSCANDATA_Spare_MIN                           (0)
  #define CH2CCC_CR_DDRCRBSCANDATA_Spare_MAX                           (127) // 0x0000007F
  #define CH2CCC_CR_DDRCRBSCANDATA_Spare_DEF                           (0x00000000)
  #define CH2CCC_CR_DDRCRBSCANDATA_Spare_HSH                           (0x0732354C)

#define CH2CCC_CR_DDRCRMISR_REG                                        (0x00003550)

  #define CH2CCC_CR_DDRCRMISR_enonfuncen_OFF                           ( 0)
  #define CH2CCC_CR_DDRCRMISR_enonfuncen_WID                           ( 1)
  #define CH2CCC_CR_DDRCRMISR_enonfuncen_MSK                           (0x00000001)
  #define CH2CCC_CR_DDRCRMISR_enonfuncen_MIN                           (0)
  #define CH2CCC_CR_DDRCRMISR_enonfuncen_MAX                           (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMISR_enonfuncen_DEF                           (0x00000000)
  #define CH2CCC_CR_DDRCRMISR_enonfuncen_HSH                           (0x01003550)

  #define CH2CCC_CR_DDRCRMISR_startonfuncen_OFF                        ( 1)
  #define CH2CCC_CR_DDRCRMISR_startonfuncen_WID                        ( 1)
  #define CH2CCC_CR_DDRCRMISR_startonfuncen_MSK                        (0x00000002)
  #define CH2CCC_CR_DDRCRMISR_startonfuncen_MIN                        (0)
  #define CH2CCC_CR_DDRCRMISR_startonfuncen_MAX                        (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMISR_startonfuncen_DEF                        (0x00000000)
  #define CH2CCC_CR_DDRCRMISR_startonfuncen_HSH                        (0x01023550)

  #define CH2CCC_CR_DDRCRMISR_enlfsr_OFF                               ( 2)
  #define CH2CCC_CR_DDRCRMISR_enlfsr_WID                               ( 1)
  #define CH2CCC_CR_DDRCRMISR_enlfsr_MSK                               (0x00000004)
  #define CH2CCC_CR_DDRCRMISR_enlfsr_MIN                               (0)
  #define CH2CCC_CR_DDRCRMISR_enlfsr_MAX                               (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMISR_enlfsr_DEF                               (0x00000000)
  #define CH2CCC_CR_DDRCRMISR_enlfsr_HSH                               (0x01043550)

  #define CH2CCC_CR_DDRCRMISR_masknondeta_OFF                          ( 3)
  #define CH2CCC_CR_DDRCRMISR_masknondeta_WID                          ( 1)
  #define CH2CCC_CR_DDRCRMISR_masknondeta_MSK                          (0x00000008)
  #define CH2CCC_CR_DDRCRMISR_masknondeta_MIN                          (0)
  #define CH2CCC_CR_DDRCRMISR_masknondeta_MAX                          (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMISR_masknondeta_DEF                          (0x00000000)
  #define CH2CCC_CR_DDRCRMISR_masknondeta_HSH                          (0x01063550)

  #define CH2CCC_CR_DDRCRMISR_ddrdfxdataovrd_A0_OFF                       ( 4)
  #define CH2CCC_CR_DDRCRMISR_ddrdfxdataovrd_A0_WID                       ( 1)
  #define CH2CCC_CR_DDRCRMISR_ddrdfxdataovrd_A0_MSK                       (0x00000010)
  #define CH2CCC_CR_DDRCRMISR_ddrdfxdataovrd_A0_MIN                       (0)
  #define CH2CCC_CR_DDRCRMISR_ddrdfxdataovrd_A0_MAX                       (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMISR_ddrdfxdataovrd_A0_DEF                       (0x00000000)
  #define CH2CCC_CR_DDRCRMISR_ddrdfxdataovrd_A0_HSH                       (0x01083550)

  #define CH2CCC_CR_DDRCRMISR_Spare_OFF                                ( 4)
  #define CH2CCC_CR_DDRCRMISR_Spare_WID                                ( 1)
  #define CH2CCC_CR_DDRCRMISR_Spare_MSK                                (0x00000010)
  #define CH2CCC_CR_DDRCRMISR_Spare_MIN                                (0)
  #define CH2CCC_CR_DDRCRMISR_Spare_MAX                                (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMISR_Spare_DEF                                (0x00000000)
  #define CH2CCC_CR_DDRCRMISR_Spare_HSH                                (0x01083550)

  #define CH2CCC_CR_DDRCRMISR_encycles_OFF                             ( 5)
  #define CH2CCC_CR_DDRCRMISR_encycles_WID                             ( 3)
  #define CH2CCC_CR_DDRCRMISR_encycles_MSK                             (0x000000E0)
  #define CH2CCC_CR_DDRCRMISR_encycles_MIN                             (0)
  #define CH2CCC_CR_DDRCRMISR_encycles_MAX                             (7) // 0x00000007
  #define CH2CCC_CR_DDRCRMISR_encycles_DEF                             (0x00000000)
  #define CH2CCC_CR_DDRCRMISR_encycles_HSH                             (0x030A3550)

  #define CH2CCC_CR_DDRCRMISR_maskbits_OFF                             ( 8)
  #define CH2CCC_CR_DDRCRMISR_maskbits_WID                             ( 8)
  #define CH2CCC_CR_DDRCRMISR_maskbits_MSK                             (0x0000FF00)
  #define CH2CCC_CR_DDRCRMISR_maskbits_MIN                             (0)
  #define CH2CCC_CR_DDRCRMISR_maskbits_MAX                             (255) // 0x000000FF
  #define CH2CCC_CR_DDRCRMISR_maskbits_DEF                             (0x00000000)
  #define CH2CCC_CR_DDRCRMISR_maskbits_HSH                             (0x08103550)

  #define CH2CCC_CR_DDRCRMISR_misrdata_OFF                             (16)
  #define CH2CCC_CR_DDRCRMISR_misrdata_WID                             (16)
  #define CH2CCC_CR_DDRCRMISR_misrdata_MSK                             (0xFFFF0000)
  #define CH2CCC_CR_DDRCRMISR_misrdata_MIN                             (0)
  #define CH2CCC_CR_DDRCRMISR_misrdata_MAX                             (65535) // 0x0000FFFF
  #define CH2CCC_CR_DDRCRMISR_misrdata_DEF                             (0x00000000)
  #define CH2CCC_CR_DDRCRMISR_misrdata_HSH                             (0x10203550)

#define CH2CCC_CR_DDRCRMARGINMODECONTROL_REG                           (0x00003554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_runtest_OFF                 ( 0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_runtest_WID                 ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_runtest_MSK                 (0x00000001)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_runtest_MIN                 (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_runtest_MAX                 (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_runtest_DEF                 (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_runtest_HSH                 (0x01003554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_param_OFF                   ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_param_WID                   ( 3)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_param_MSK                   (0x0000000E)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_param_MIN                   (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_param_MAX                   (7) // 0x00000007
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_param_DEF                   (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_param_HSH                   (0x03023554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_A0_OFF                  ( 4)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_A0_WID                  ( 8)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_A0_MSK                  (0x00000FF0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_A0_MIN                  (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_A0_MAX                  (255) // 0x000000FF
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_A0_DEF                  (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_A0_HSH                  (0x08083554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_A0_OFF                  (12)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_A0_WID                  ( 8)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_A0_MSK                  (0x000FF000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_A0_MIN                  (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_A0_MAX                  (255) // 0x000000FF
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_A0_DEF                  (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_A0_HSH                  (0x08183554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_A0_OFF           (20)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_A0_WID           ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_A0_MSK           (0x00100000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_A0_MIN           (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_A0_MAX           (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_A0_DEF           (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_A0_HSH           (0x01283554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_CB1_A0_OFF                     (21)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_CB1_A0_WID                     ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_CB1_A0_MSK                     (0x00200000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_CB1_A0_MIN                     (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_CB1_A0_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_CB1_A0_DEF                     (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_CB1_A0_HSH                     (0x012A3554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_A0_OFF              (22)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_A0_WID              ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_A0_MSK              (0x00400000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_A0_MIN              (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_A0_MAX              (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_A0_DEF              (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_A0_HSH              (0x012C3554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolbcycles_A0_OFF              (23)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolbcycles_A0_WID              ( 4)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolbcycles_A0_MSK              (0x07800000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolbcycles_A0_MIN              (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolbcycles_A0_MAX              (15) // 0x0000000F
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolbcycles_A0_DEF              (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolbcycles_A0_HSH              (0x042E3554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_OFF                  ( 4)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_WID                  (10)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_MSK                  (0x00003FF0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_MIN                  (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_MAX                  (1023) // 0x000003FF
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_DEF                  (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_minval_HSH                  (0x0A083554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_OFF                  (14)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_WID                  (10)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_MSK                  (0x00FFC000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_MIN                  (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_MAX                  (1023) // 0x000003FF
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_DEF                  (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_maxval_HSH                  (0x0A1C3554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_OFF           (24)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_WID           ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_MSK           (0x01000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_MIN           (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_MAX           (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_DEF           (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_iolb_cpgcmode_HSH           (0x01303554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_isolation_OFF               (25)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_isolation_WID               ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_isolation_MSK               (0x02000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_isolation_MIN               (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_isolation_MAX               (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_isolation_DEF               (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_isolation_HSH               (0x01323554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_OFF              (26)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_WID              ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_MSK              (0x04000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_MIN              (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_MAX              (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_DEF              (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_calccenter_HSH              (0x01343554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_laneresult_OFF              (27)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_laneresult_WID              ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_laneresult_MSK              (0x08000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_laneresult_MIN              (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_laneresult_MAX              (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_laneresult_DEF              (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_laneresult_HSH              (0x01363554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_OFF                   (28)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_WID                   ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_MSK                   (0x10000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_MIN                   (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_MAX                   (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_DEF                   (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_HSH                   (0x01383554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_OFF                   (29)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_WID                   ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_MSK                   (0x20000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_MIN                   (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_MAX                   (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_DEF                   (0x00000001)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_HSH                   (0x013A3554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_ph1_A0_OFF               (30)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_ph1_A0_WID               ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_ph1_A0_MSK               (0x40000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_ph1_A0_MIN               (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_ph1_A0_MAX               (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_ph1_A0_DEF               (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data0_ph1_A0_HSH               (0x013C3554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_ph1_A0_OFF               (31)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_ph1_A0_WID               ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_ph1_A0_MSK               (0x80000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_ph1_A0_MIN               (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_ph1_A0_MAX               (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_ph1_A0_DEF               (0x00000001)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_data1_ph1_A0_HSH               (0x013E3554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_ddrdfxdataovrd_OFF          (30)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_ddrdfxdataovrd_WID          ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_ddrdfxdataovrd_MSK          (0x40000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_ddrdfxdataovrd_MIN          (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_ddrdfxdataovrd_MAX          (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_ddrdfxdataovrd_DEF          (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_ddrdfxdataovrd_HSH          (0x013C3554)

  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_Spare_OFF                   (31)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_Spare_WID                   ( 1)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_Spare_MSK                   (0x80000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_Spare_MIN                   (0)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_Spare_MAX                   (1) // 0x00000001
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_Spare_DEF                   (0x00000000)
  #define CH2CCC_CR_DDRCRMARGINMODECONTROL_Spare_HSH                   (0x013E3554)

#define CH2CCC_CR_DDRCRMARGINMODEDEBUGMSB_REG                          (0x00003558)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH2CCC_CR_DDRCRMARGINMODEDEBUGLSB_REG                          (0x0000355C)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH2CCC_CR_SRZBYPASSCTL_REG                                     (0x00003560)

  #define CH2CCC_CR_SRZBYPASSCTL_srzdataovrden_OFF                     ( 0)
  #define CH2CCC_CR_SRZBYPASSCTL_srzdataovrden_WID                     ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_srzdataovrden_MSK                     (0x00000001)
  #define CH2CCC_CR_SRZBYPASSCTL_srzdataovrden_MIN                     (0)
  #define CH2CCC_CR_SRZBYPASSCTL_srzdataovrden_MAX                     (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_srzdataovrden_DEF                     (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_srzdataovrden_HSH                     (0x01003560)

  #define CH2CCC_CR_SRZBYPASSCTL_srzdrvenovrden_OFF                    ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_srzdrvenovrden_WID                    ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_srzdrvenovrden_MSK                    (0x00000002)
  #define CH2CCC_CR_SRZBYPASSCTL_srzdrvenovrden_MIN                    (0)
  #define CH2CCC_CR_SRZBYPASSCTL_srzdrvenovrden_MAX                    (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_srzdrvenovrden_DEF                    (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_srzdrvenovrden_HSH                    (0x01023560)

  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrden_OFF                    ( 2)
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrden_WID                    ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrden_MSK                    (0x00000004)
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrden_MIN                    (0)
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrden_MAX                    (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrden_DEF                    (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrden_HSH                    (0x01043560)

  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrdval_OFF                   ( 3)
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrdval_WID                   ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrdval_MSK                   (0x00000008)
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrdval_MIN                   (0)
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrdval_MAX                   (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrdval_DEF                   (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_txeqdataovrdval_HSH                   (0x01063560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc14_srzdataovrdval_OFF              ( 4)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc14_srzdataovrdval_WID              ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc14_srzdataovrdval_MSK              (0x00000010)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc14_srzdataovrdval_MIN              (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc14_srzdataovrdval_MAX              (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc14_srzdataovrdval_DEF              (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc14_srzdataovrdval_HSH              (0x01083560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc13_srzdataovrdval_OFF              ( 5)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc13_srzdataovrdval_WID              ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc13_srzdataovrdval_MSK              (0x00000020)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc13_srzdataovrdval_MIN              (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc13_srzdataovrdval_MAX              (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc13_srzdataovrdval_DEF              (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc13_srzdataovrdval_HSH              (0x010A3560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc12_srzdataovrdval_OFF              ( 6)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc12_srzdataovrdval_WID              ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc12_srzdataovrdval_MSK              (0x00000040)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc12_srzdataovrdval_MIN              (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc12_srzdataovrdval_MAX              (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc12_srzdataovrdval_DEF              (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc12_srzdataovrdval_HSH              (0x010C3560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc11_srzdataovrdval_OFF              ( 7)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc11_srzdataovrdval_WID              ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc11_srzdataovrdval_MSK              (0x00000080)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc11_srzdataovrdval_MIN              (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc11_srzdataovrdval_MAX              (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc11_srzdataovrdval_DEF              (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc11_srzdataovrdval_HSH              (0x010E3560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc10_srzdataovrdval_OFF              ( 8)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc10_srzdataovrdval_WID              ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc10_srzdataovrdval_MSK              (0x00000100)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc10_srzdataovrdval_MIN              (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc10_srzdataovrdval_MAX              (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc10_srzdataovrdval_DEF              (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc10_srzdataovrdval_HSH              (0x01103560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc9_srzdataovrdval_OFF               ( 9)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc9_srzdataovrdval_WID               ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc9_srzdataovrdval_MSK               (0x00000200)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc9_srzdataovrdval_MIN               (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc9_srzdataovrdval_MAX               (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc9_srzdataovrdval_DEF               (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc9_srzdataovrdval_HSH               (0x01123560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc8_srzdataovrdval_OFF               (10)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc8_srzdataovrdval_WID               ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc8_srzdataovrdval_MSK               (0x00000400)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc8_srzdataovrdval_MIN               (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc8_srzdataovrdval_MAX               (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc8_srzdataovrdval_DEF               (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc8_srzdataovrdval_HSH               (0x01143560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc7_srzdataovrdval_OFF               (11)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc7_srzdataovrdval_WID               ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc7_srzdataovrdval_MSK               (0x00000800)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc7_srzdataovrdval_MIN               (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc7_srzdataovrdval_MAX               (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc7_srzdataovrdval_DEF               (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc7_srzdataovrdval_HSH               (0x01163560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc6_srzdataovrdval_OFF               (12)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc6_srzdataovrdval_WID               ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc6_srzdataovrdval_MSK               (0x00001000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc6_srzdataovrdval_MIN               (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc6_srzdataovrdval_MAX               (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc6_srzdataovrdval_DEF               (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc6_srzdataovrdval_HSH               (0x01183560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc5_srzdataovrdval_OFF               (13)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc5_srzdataovrdval_WID               ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc5_srzdataovrdval_MSK               (0x00002000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc5_srzdataovrdval_MIN               (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc5_srzdataovrdval_MAX               (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc5_srzdataovrdval_DEF               (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc5_srzdataovrdval_HSH               (0x011A3560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc4_srzdataovrdval_OFF               (14)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc4_srzdataovrdval_WID               ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc4_srzdataovrdval_MSK               (0x00004000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc4_srzdataovrdval_MIN               (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc4_srzdataovrdval_MAX               (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc4_srzdataovrdval_DEF               (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc4_srzdataovrdval_HSH               (0x011C3560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc3_srzdataovrdval_OFF               (15)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc3_srzdataovrdval_WID               ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc3_srzdataovrdval_MSK               (0x00008000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc3_srzdataovrdval_MIN               (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc3_srzdataovrdval_MAX               (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc3_srzdataovrdval_DEF               (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc3_srzdataovrdval_HSH               (0x011E3560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc2_srzdataovrdval_OFF               (16)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc2_srzdataovrdval_WID               ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc2_srzdataovrdval_MSK               (0x00010000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc2_srzdataovrdval_MIN               (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc2_srzdataovrdval_MAX               (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc2_srzdataovrdval_DEF               (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc2_srzdataovrdval_HSH               (0x01203560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc1_srzdataovrdval_OFF               (17)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc1_srzdataovrdval_WID               ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc1_srzdataovrdval_MSK               (0x00020000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc1_srzdataovrdval_MIN               (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc1_srzdataovrdval_MAX               (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc1_srzdataovrdval_DEF               (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc1_srzdataovrdval_HSH               (0x01223560)

  #define CH2CCC_CR_SRZBYPASSCTL_ccc0_srzdataovrdval_OFF               (18)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc0_srzdataovrdval_WID               ( 1)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc0_srzdataovrdval_MSK               (0x00040000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc0_srzdataovrdval_MIN               (0)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc0_srzdataovrdval_MAX               (1) // 0x00000001
  #define CH2CCC_CR_SRZBYPASSCTL_ccc0_srzdataovrdval_DEF               (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_ccc0_srzdataovrdval_HSH               (0x01243560)

  #define CH2CCC_CR_SRZBYPASSCTL_Spare_OFF                             (19)
  #define CH2CCC_CR_SRZBYPASSCTL_Spare_WID                             (13)
  #define CH2CCC_CR_SRZBYPASSCTL_Spare_MSK                             (0xFFF80000)
  #define CH2CCC_CR_SRZBYPASSCTL_Spare_MIN                             (0)
  #define CH2CCC_CR_SRZBYPASSCTL_Spare_MAX                             (8191) // 0x00001FFF
  #define CH2CCC_CR_SRZBYPASSCTL_Spare_DEF                             (0x00000000)
  #define CH2CCC_CR_SRZBYPASSCTL_Spare_HSH                             (0x0D263560)

#define CH2CCC_CR_SRZDRVENBYPASSCTL_REG                                (0x00003564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrden_OFF         ( 0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrden_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrden_MSK         (0x00000001)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrden_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrden_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrden_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrden_HSH         (0x01003564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrden_OFF         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrden_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrden_MSK         (0x00000002)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrden_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrden_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrden_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrden_HSH         (0x01023564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrden_OFF         ( 2)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrden_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrden_MSK         (0x00000004)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrden_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrden_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrden_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrden_HSH         (0x01043564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrden_OFF         ( 3)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrden_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrden_MSK         (0x00000008)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrden_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrden_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrden_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrden_HSH         (0x01063564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrden_OFF         ( 4)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrden_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrden_MSK         (0x00000010)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrden_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrden_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrden_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrden_HSH         (0x01083564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrden_OFF          ( 5)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrden_WID          ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrden_MSK          (0x00000020)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrden_MIN          (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrden_MAX          (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrden_DEF          (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrden_HSH          (0x010A3564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrden_OFF          ( 6)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrden_WID          ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrden_MSK          (0x00000040)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrden_MIN          (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrden_MAX          (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrden_DEF          (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrden_HSH          (0x010C3564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrden_OFF          ( 7)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrden_WID          ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrden_MSK          (0x00000080)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrden_MIN          (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrden_MAX          (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrden_DEF          (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrden_HSH          (0x010E3564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrden_OFF          ( 8)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrden_WID          ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrden_MSK          (0x00000100)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrden_MIN          (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrden_MAX          (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrden_DEF          (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrden_HSH          (0x01103564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrden_OFF          ( 9)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrden_WID          ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrden_MSK          (0x00000200)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrden_MIN          (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrden_MAX          (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrden_DEF          (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrden_HSH          (0x01123564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrden_OFF          (10)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrden_WID          ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrden_MSK          (0x00000400)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrden_MIN          (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrden_MAX          (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrden_DEF          (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrden_HSH          (0x01143564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrden_OFF          (11)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrden_WID          ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrden_MSK          (0x00000800)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrden_MIN          (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrden_MAX          (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrden_DEF          (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrden_HSH          (0x01163564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrden_OFF          (12)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrden_WID          ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrden_MSK          (0x00001000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrden_MIN          (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrden_MAX          (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrden_DEF          (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrden_HSH          (0x01183564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrden_OFF          (13)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrden_WID          ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrden_MSK          (0x00002000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrden_MIN          (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrden_MAX          (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrden_DEF          (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrden_HSH          (0x011A3564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrden_OFF          (14)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrden_WID          ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrden_MSK          (0x00004000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrden_MIN          (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrden_MAX          (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrden_DEF          (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrden_HSH          (0x011C3564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrdval_OFF        (15)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrdval_WID        ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrdval_MSK        (0x00008000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrdval_MIN        (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrdval_MAX        (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrdval_DEF        (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc14_srzdrvenovrdval_HSH        (0x011E3564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrdval_OFF        (16)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrdval_WID        ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrdval_MSK        (0x00010000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrdval_MIN        (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrdval_MAX        (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrdval_DEF        (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc13_srzdrvenovrdval_HSH        (0x01203564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrdval_OFF        (17)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrdval_WID        ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrdval_MSK        (0x00020000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrdval_MIN        (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrdval_MAX        (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrdval_DEF        (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc12_srzdrvenovrdval_HSH        (0x01223564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrdval_OFF        (18)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrdval_WID        ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrdval_MSK        (0x00040000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrdval_MIN        (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrdval_MAX        (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrdval_DEF        (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc11_srzdrvenovrdval_HSH        (0x01243564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrdval_OFF        (19)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrdval_WID        ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrdval_MSK        (0x00080000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrdval_MIN        (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrdval_MAX        (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrdval_DEF        (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc10_srzdrvenovrdval_HSH        (0x01263564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrdval_OFF         (20)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrdval_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrdval_MSK         (0x00100000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrdval_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrdval_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrdval_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc9_srzdrvenovrdval_HSH         (0x01283564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrdval_OFF         (21)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrdval_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrdval_MSK         (0x00200000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrdval_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrdval_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrdval_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc8_srzdrvenovrdval_HSH         (0x012A3564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrdval_OFF         (22)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrdval_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrdval_MSK         (0x00400000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrdval_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrdval_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrdval_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc7_srzdrvenovrdval_HSH         (0x012C3564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrdval_OFF         (23)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrdval_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrdval_MSK         (0x00800000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrdval_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrdval_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrdval_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc6_srzdrvenovrdval_HSH         (0x012E3564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrdval_OFF         (24)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrdval_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrdval_MSK         (0x01000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrdval_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrdval_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrdval_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc5_srzdrvenovrdval_HSH         (0x01303564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrdval_OFF         (25)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrdval_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrdval_MSK         (0x02000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrdval_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrdval_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrdval_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc4_srzdrvenovrdval_HSH         (0x01323564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrdval_OFF         (26)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrdval_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrdval_MSK         (0x04000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrdval_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrdval_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrdval_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc3_srzdrvenovrdval_HSH         (0x01343564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrdval_OFF         (27)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrdval_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrdval_MSK         (0x08000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrdval_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrdval_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrdval_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc2_srzdrvenovrdval_HSH         (0x01363564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrdval_OFF         (28)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrdval_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrdval_MSK         (0x10000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrdval_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrdval_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrdval_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc1_srzdrvenovrdval_HSH         (0x01383564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrdval_OFF         (29)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrdval_WID         ( 1)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrdval_MSK         (0x20000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrdval_MIN         (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrdval_MAX         (1) // 0x00000001
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrdval_DEF         (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_ccc0_srzdrvenovrdval_HSH         (0x013A3564)

  #define CH2CCC_CR_SRZDRVENBYPASSCTL_reserved16_OFF                   (30)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_reserved16_WID                   ( 2)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_reserved16_MSK                   (0xC0000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_reserved16_MIN                   (0)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_reserved16_MAX                   (3) // 0x00000003
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_reserved16_DEF                   (0x00000000)
  #define CH2CCC_CR_SRZDRVENBYPASSCTL_reserved16_HSH                   (0x023C3564)

#define CH2CCC_CR_DFXCCMON_REG                                         (0x00003568)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define CH2CCC_CR_DCCCTL4_REG                                          (0x0000356C)

  #define CH2CCC_CR_DCCCTL4_dcc_bits_en_OFF                            ( 0)
  #define CH2CCC_CR_DCCCTL4_dcc_bits_en_WID                            (16)
  #define CH2CCC_CR_DCCCTL4_dcc_bits_en_MSK                            (0x0000FFFF)
  #define CH2CCC_CR_DCCCTL4_dcc_bits_en_MIN                            (0)
  #define CH2CCC_CR_DCCCTL4_dcc_bits_en_MAX                            (65535) // 0x0000FFFF
  #define CH2CCC_CR_DCCCTL4_dcc_bits_en_DEF                            (0x00000000)
  #define CH2CCC_CR_DCCCTL4_dcc_bits_en_HSH                            (0x1000356C)

  #define CH2CCC_CR_DCCCTL4_dcc_step2pattern_enable_OFF                (16)
  #define CH2CCC_CR_DCCCTL4_dcc_step2pattern_enable_WID                (16)
  #define CH2CCC_CR_DCCCTL4_dcc_step2pattern_enable_MSK                (0xFFFF0000)
  #define CH2CCC_CR_DCCCTL4_dcc_step2pattern_enable_MIN                (0)
  #define CH2CCC_CR_DCCCTL4_dcc_step2pattern_enable_MAX                (65535) // 0x0000FFFF
  #define CH2CCC_CR_DCCCTL4_dcc_step2pattern_enable_DEF                (0x00000000)
  #define CH2CCC_CR_DCCCTL4_dcc_step2pattern_enable_HSH                (0x1020356C)

#define CH2CCC_CR_DCCCTL17_REG                                         (0x00003570)

  #define CH2CCC_CR_DCCCTL17_fatal_live_OFF                            ( 0)
  #define CH2CCC_CR_DCCCTL17_fatal_live_WID                            (16)
  #define CH2CCC_CR_DCCCTL17_fatal_live_MSK                            (0x0000FFFF)
  #define CH2CCC_CR_DCCCTL17_fatal_live_MIN                            (0)
  #define CH2CCC_CR_DCCCTL17_fatal_live_MAX                            (65535) // 0x0000FFFF
  #define CH2CCC_CR_DCCCTL17_fatal_live_DEF                            (0x00000000)
  #define CH2CCC_CR_DCCCTL17_fatal_live_HSH                            (0x10003570)

  #define CH2CCC_CR_DCCCTL17_fatal_sticky_OFF                          (16)
  #define CH2CCC_CR_DCCCTL17_fatal_sticky_WID                          (16)
  #define CH2CCC_CR_DCCCTL17_fatal_sticky_MSK                          (0xFFFF0000)
  #define CH2CCC_CR_DCCCTL17_fatal_sticky_MIN                          (0)
  #define CH2CCC_CR_DCCCTL17_fatal_sticky_MAX                          (65535) // 0x0000FFFF
  #define CH2CCC_CR_DCCCTL17_fatal_sticky_DEF                          (0x00000000)
  #define CH2CCC_CR_DCCCTL17_fatal_sticky_HSH                          (0x10203570)

#define CH2CCC_CR_DCCCTL18_REG                                         (0x00003574)

  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph0overflow_OFF                ( 0)
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph0overflow_WID                (16)
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph0overflow_MSK                (0x0000FFFF)
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph0overflow_MIN                (0)
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph0overflow_MAX                (65535) // 0x0000FFFF
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph0overflow_DEF                (0x00000000)
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph0overflow_HSH                (0x10003574)

  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph90overflow_OFF               (16)
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph90overflow_WID               (16)
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph90overflow_MSK               (0xFFFF0000)
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph90overflow_MIN               (0)
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph90overflow_MAX               (65535) // 0x0000FFFF
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph90overflow_DEF               (0x00000000)
  #define CH2CCC_CR_DCCCTL18_dccctrlcodeph90overflow_HSH               (0x10203574)

#define CH2CCC_CR_DCCCTL19_REG                                         (0x00003578)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define CH2CCC_CR_CLKALIGNSTATUS_REG                                   (0x0000357C)

  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_OFF       ( 0)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_WID       ( 1)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_MSK       (0x00000001)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_MIN       (0)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_MAX       (1) // 0x00000001
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_DEF       (0x00000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_sample_pd_output_HSH       (0x0100357C)

  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_complete_level_OFF         ( 1)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_complete_level_WID         ( 1)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_complete_level_MSK         (0x00000002)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_complete_level_MIN         (0)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_complete_level_MAX         (1) // 0x00000001
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_complete_level_DEF         (0x00000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalign_complete_level_HSH         (0x0102357C)

  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_err_OFF              ( 2)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_err_WID              ( 1)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_err_MSK              (0x00000004)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_err_MIN              (0)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_err_MAX              (1) // 0x00000001
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_err_DEF              (0x00000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_err_HSH              (0x0104357C)

  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_one_count_OFF        ( 3)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_one_count_WID        ( 5)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_one_count_MSK        (0x000000F8)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_one_count_MIN        (0)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_one_count_MAX        (31) // 0x0000001F
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_one_count_DEF        (0x00000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_one_count_HSH        (0x0506357C)

  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_OFF       ( 8)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_WID       ( 5)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_MSK       (0x00001F00)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_MIN       (0)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_MAX       (31) // 0x0000001F
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_DEF       (0x00000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_zero_count_HSH       (0x0510357C)

  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_OFF      (13)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_WID      ( 1)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_MSK      (0x00002000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_MIN      (0)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_MAX      (1) // 0x00000001
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_DEF      (0x00000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_sample_done_HSH      (0x011A357C)

  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe1_OFF            (14)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe1_WID            ( 1)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe1_MSK            (0x00004000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe1_MIN            (0)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe1_MAX            (1) // 0x00000001
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe1_DEF            (0x00000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe1_HSH            (0x011C357C)

  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe0_OFF            (15)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe0_WID            ( 1)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe0_MSK            (0x00008000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe0_MIN            (0)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe0_MAX            (1) // 0x00000001
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe0_DEF            (0x00000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_clkalignstatus_safe0_HSH            (0x011E357C)

  #define CH2CCC_CR_CLKALIGNSTATUS_ref2xclkpicode_OFF                  (16)
  #define CH2CCC_CR_CLKALIGNSTATUS_ref2xclkpicode_WID                  ( 8)
  #define CH2CCC_CR_CLKALIGNSTATUS_ref2xclkpicode_MSK                  (0x00FF0000)
  #define CH2CCC_CR_CLKALIGNSTATUS_ref2xclkpicode_MIN                  (0)
  #define CH2CCC_CR_CLKALIGNSTATUS_ref2xclkpicode_MAX                  (255) // 0x000000FF
  #define CH2CCC_CR_CLKALIGNSTATUS_ref2xclkpicode_DEF                  (0x00000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_ref2xclkpicode_HSH                  (0x0820357C)

  #define CH2CCC_CR_CLKALIGNSTATUS_reserved17_OFF                      (24)
  #define CH2CCC_CR_CLKALIGNSTATUS_reserved17_WID                      ( 8)
  #define CH2CCC_CR_CLKALIGNSTATUS_reserved17_MSK                      (0xFF000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_reserved17_MIN                      (0)
  #define CH2CCC_CR_CLKALIGNSTATUS_reserved17_MAX                      (255) // 0x000000FF
  #define CH2CCC_CR_CLKALIGNSTATUS_reserved17_DEF                      (0x00000000)
  #define CH2CCC_CR_CLKALIGNSTATUS_reserved17_HSH                      (0x0830357C)

#define CH0CCC_CR_DDRCRPINSUSED_REG                                    (0x00003580)
//Duplicate of CH2CCC_CR_DDRCRPINSUSED_REG

#define CH0CCC_CR_CCCRSTCTL_A0_REG                                     (0x000035B8)
#define CH0CCC_CR_CCCRSTCTL_REG                                        (0x00003584)
//Duplicate of CH2CCC_CR_CCCRSTCTL_REG

#define CH0CCC_CR_PICODE0_A0_REG                                       (0x00003584)
#define CH0CCC_CR_PICODE0_REG                                          (0x00003588)
//Duplicate of CH2CCC_CR_PICODE0_REG

#define CH0CCC_CR_PICODE1_A0_REG                                       (0x00003588)
#define CH0CCC_CR_PICODE1_REG                                          (0x0000358C)
//Duplicate of CH2CCC_CR_PICODE1_REG

#define CH0CCC_CR_PICODE2_A0_REG                                       (0x0000358C)
#define CH0CCC_CR_PICODE2_REG                                          (0x00003590)
//Duplicate of CH2CCC_CR_PICODE2_REG

#define CH0CCC_CR_PICODE3_A0_REG                                       (0x00003590)
#define CH0CCC_CR_PICODE3_REG                                          (0x00003594)
//Duplicate of CH2CCC_CR_PICODE3_REG

#define CH0CCC_CR_MDLLCTL0_A0_REG                                      (0x00003594)
#define CH0CCC_CR_MDLLCTL0_REG                                         (0x00003598)
//Duplicate of CH2CCC_CR_MDLLCTL0_REG

#define CH0CCC_CR_MDLLCTL1_A0_REG                                      (0x00003598)
#define CH0CCC_CR_MDLLCTL1_REG                                         (0x0000359C)
//Duplicate of CH2CCC_CR_MDLLCTL1_REG

#define CH0CCC_CR_MDLLCTL2_A0_REG                                      (0x0000359C)
#define CH0CCC_CR_MDLLCTL2_REG                                         (0x000035A0)
//Duplicate of CH2CCC_CR_MDLLCTL2_REG

#define CH0CCC_CR_MDLLCTL3_A0_REG                                      (0x000035A0)
#define CH0CCC_CR_MDLLCTL3_REG                                         (0x000035A4)
//Duplicate of CH2CCC_CR_MDLLCTL3_REG

#define CH0CCC_CR_COMP2_A0_REG                                         (0x000035A4)
#define CH0CCC_CR_COMP2_REG                                            (0x000035A8)
//Duplicate of CH2CCC_CR_COMP2_REG

#define CH0CCC_CR_COMP3_A0_REG                                         (0x000035A8)
#define CH0CCC_CR_COMP3_REG                                            (0x000035AC)
//Duplicate of CH2CCC_CR_COMP3_REG

#define CH0CCC_CR_DDRCRCCCVOLTAGEUSED_A0_REG                           (0x000035AC)
#define CH0CCC_CR_DDRCRCCCVOLTAGEUSED_REG                              (0x000035B0)
//Duplicate of CH2CCC_CR_DDRCRCCCVOLTAGEUSED_REG

#define CH0CCC_CR_DDRCRCTLCACOMPOFFSET_A0_REG                          (0x000035B0)
#define CH0CCC_CR_DDRCRCTLCACOMPOFFSET_REG                             (0x000035B4)
//Duplicate of CH2CCC_CR_DDRCRCTLCACOMPOFFSET_REG

#define CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_A0_REG                       (0x000035B4)
#define CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG                          (0x000035B8)
//Duplicate of CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG

#define CH0CCC_CR_CTL0_REG                                             (0x000035BC)
//Duplicate of CH2CCC_CR_CTL0_REG

#define CH0CCC_CR_CTL3_REG                                             (0x000035C0)
//Duplicate of CH2CCC_CR_CTL3_REG

#define CH0CCC_CR_MDLLCTL4_REG                                         (0x000035C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define CH0CCC_CR_CTL2_REG                                             (0x000035C8)
//Duplicate of CH2CCC_CR_CTL2_REG

#define CH0CCC_CR_TXPBDCODE0_REG                                       (0x000035CC)
//Duplicate of CH2CCC_CR_TXPBDCODE0_REG

#define CH0CCC_CR_TXPBDCODE1_REG                                       (0x000035D0)
//Duplicate of CH2CCC_CR_TXPBDCODE1_REG

#define CH0CCC_CR_TXPBDCODE2_REG                                       (0x000035D4)
//Duplicate of CH2CCC_CR_TXPBDCODE2_REG

#define CH0CCC_CR_TXPBDCODE3_REG                                       (0x000035D8)
//Duplicate of CH2CCC_CR_TXPBDCODE3_REG

#define CH0CCC_CR_TXPBDCODE4_REG                                       (0x000035DC)
//Duplicate of CH2CCC_CR_TXPBDCODE4_REG

#define CH0CCC_CR_DCCCTL6_REG                                          (0x000035E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define CH0CCC_CR_DCCCTL7_REG                                          (0x000035E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define CH0CCC_CR_DCCCTL8_REG                                          (0x000035E8)
//Duplicate of CH2CCC_CR_DCCCTL8_REG

#define CH0CCC_CR_DCCCTL9_REG                                          (0x000035EC)
//Duplicate of CH2CCC_CR_DCCCTL9_REG

#define CH0CCC_CR_DCCCTL11_REG                                         (0x000035F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define CH0CCC_CR_DCCCTL20_REG                                         (0x000035F4)
//Duplicate of CH2CCC_CR_DCCCTL20_REG

#define CH0CCC_CR_DDRCRCCCPIDIVIDER_REG                                (0x00003600)
//Duplicate of CH2CCC_CR_DDRCRCCCPIDIVIDER_REG

#define CH0CCC_CR_CLKALIGNCTL0_REG                                     (0x00003604)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define CH0CCC_CR_CLKALIGNCTL1_REG                                     (0x00003608)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define CH0CCC_CR_CLKALIGNCTL2_REG                                     (0x0000360C)
//Duplicate of CH2CCC_CR_CLKALIGNCTL2_REG

#define CH0CCC_CR_WLCTRL_A0_REG                                        (0x00003630)
#define CH0CCC_CR_WLCTRL_REG                                           (0x00003610)
//Duplicate of CH2CCC_CR_WLCTRL_REG

#define CH0CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG                           (0x00003610)
#define CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG                              (0x00003614)
//Duplicate of CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG

#define CH0CCC_CR_TXPBDOFFSET_A0_REG                                   (0x0000362C)
#define CH0CCC_CR_TXPBDOFFSET_REG                                      (0x00003618)
//Duplicate of CH2CCC_CR_TXPBDOFFSET_REG

#define CH0CCC_CR_DCCCTL0_A0_REG                                       (0x00003614)
#define CH0CCC_CR_DCCCTL0_REG                                          (0x0000361C)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define CH0CCC_CR_DCCCTL1_A0_REG                                       (0x00003618)
#define CH0CCC_CR_DCCCTL1_REG                                          (0x00003620)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define CH0CCC_CR_DCCCTL2_A0_REG                                       (0x0000361C)
#define CH0CCC_CR_DCCCTL2_REG                                          (0x00003624)
//Duplicate of CH2CCC_CR_DCCCTL2_REG

#define CH0CCC_CR_DCCCTL3_A0_REG                                       (0x00003620)
#define CH0CCC_CR_DCCCTL3_REG                                          (0x00003628)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define CH0CCC_CR_DCCCTL5_A0_REG                                       (0x00003624)
#define CH0CCC_CR_DCCCTL5_REG                                          (0x0000362C)
//Duplicate of CH2CCC_CR_DCCCTL5_REG

#define CH0CCC_CR_DCCCTL13_A0_REG                                      (0x00003628)
#define CH0CCC_CR_DCCCTL13_REG                                         (0x00003630)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define CH0CCC_CR_PMFSM_REG                                            (0x00003634)
//Duplicate of CH2CCC_CR_PMFSM_REG

#define CH0CCC_CR_DDRCRCMDCOMP_REG                                     (0x00003638)
//Duplicate of CH2CCC_CR_DDRCRCMDCOMP_REG

#define CH0CCC_CR_DDRCRCLKCOMP_REG                                     (0x0000363C)
//Duplicate of CH2CCC_CR_DDRCRCLKCOMP_REG

#define CH0CCC_CR_DDRCRCTLCOMP_REG                                     (0x00003640)
//Duplicate of CH2CCC_CR_DDRCRCTLCOMP_REG

#define CH0CCC_CR_VCCDLLCOMPDATACCC_REG                                (0x00003644)
//Duplicate of CH2CCC_CR_VCCDLLCOMPDATACCC_REG

#define CH0CCC_CR_DCSOFFSET_REG                                        (0x00003648)
//Duplicate of CH2CCC_CR_DCSOFFSET_REG

#define CH0CCC_CR_DDRCRBSCANDATA_REG                                   (0x0000364C)
//Duplicate of CH2CCC_CR_DDRCRBSCANDATA_REG

#define CH0CCC_CR_DDRCRMISR_REG                                        (0x00003650)
//Duplicate of CH2CCC_CR_DDRCRMISR_REG

#define CH0CCC_CR_DDRCRMARGINMODECONTROL_REG                           (0x00003654)
//Duplicate of CH2CCC_CR_DDRCRMARGINMODECONTROL_REG

#define CH0CCC_CR_DDRCRMARGINMODEDEBUGMSB_REG                          (0x00003658)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH0CCC_CR_DDRCRMARGINMODEDEBUGLSB_REG                          (0x0000365C)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH0CCC_CR_SRZBYPASSCTL_REG                                     (0x00003660)
//Duplicate of CH2CCC_CR_SRZBYPASSCTL_REG

#define CH0CCC_CR_SRZDRVENBYPASSCTL_REG                                (0x00003664)
//Duplicate of CH2CCC_CR_SRZDRVENBYPASSCTL_REG

#define CH0CCC_CR_DFXCCMON_REG                                         (0x00003668)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define CH0CCC_CR_DCCCTL4_REG                                          (0x0000366C)
//Duplicate of CH2CCC_CR_DCCCTL4_REG

#define CH0CCC_CR_DCCCTL17_REG                                         (0x00003670)
//Duplicate of CH2CCC_CR_DCCCTL17_REG

#define CH0CCC_CR_DCCCTL18_REG                                         (0x00003674)
//Duplicate of CH2CCC_CR_DCCCTL18_REG

#define CH0CCC_CR_DCCCTL19_REG                                         (0x00003678)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define CH0CCC_CR_CLKALIGNSTATUS_REG                                   (0x0000367C)
//Duplicate of CH2CCC_CR_CLKALIGNSTATUS_REG

#define CH3CCC_CR_DDRCRPINSUSED_REG                                    (0x00003680)
//Duplicate of CH2CCC_CR_DDRCRPINSUSED_REG

#define CH3CCC_CR_CCCRSTCTL_A0_REG                                     (0x000036B8)
#define CH3CCC_CR_PICODE0_A0_REG                                       (0x00003684)
#define CH3CCC_CR_PICODE1_A0_REG                                       (0x00003688)
#define CH3CCC_CR_PICODE2_A0_REG                                       (0x0000368C)
#define CH3CCC_CR_PICODE3_A0_REG                                       (0x00003690)
#define CH3CCC_CR_MDLLCTL0_A0_REG                                      (0x00003694)
#define CH3CCC_CR_MDLLCTL1_A0_REG                                      (0x00003698)
#define CH3CCC_CR_MDLLCTL2_A0_REG                                      (0x0000369C)
#define CH3CCC_CR_MDLLCTL3_A0_REG                                      (0x000036A0)
#define CH3CCC_CR_COMP2_A0_REG                                         (0x000036A4)
#define CH3CCC_CR_COMP3_A0_REG                                         (0x000036A8)
#define CH3CCC_CR_DDRCRCCCVOLTAGEUSED_A0_REG                           (0x000036AC)
#define CH3CCC_CR_DDRCRCTLCACOMPOFFSET_A0_REG                          (0x000036B0)
#define CH3CCC_CR_DDRCRVSSHICLKCOMPOFFSET_A0_REG                       (0x000036B4)

#define CH3CCC_CR_CCCRSTCTL_REG                                        (0x00003684)
//Duplicate of CH2CCC_CR_CCCRSTCTL_REG

#define CH3CCC_CR_PICODE0_REG                                          (0x00003688)
//Duplicate of CH2CCC_CR_PICODE0_REG

#define CH3CCC_CR_PICODE1_REG                                          (0x0000368C)
//Duplicate of CH2CCC_CR_PICODE1_REG

#define CH3CCC_CR_PICODE2_REG                                          (0x00003690)
//Duplicate of CH2CCC_CR_PICODE2_REG

#define CH3CCC_CR_PICODE3_REG                                          (0x00003694)
//Duplicate of CH2CCC_CR_PICODE3_REG

#define CH3CCC_CR_MDLLCTL0_REG                                         (0x00003698)
//Duplicate of CH2CCC_CR_MDLLCTL0_REG

#define CH3CCC_CR_MDLLCTL1_REG                                         (0x0000369C)
//Duplicate of CH2CCC_CR_MDLLCTL1_REG

#define CH3CCC_CR_MDLLCTL2_REG                                         (0x000036A0)
//Duplicate of CH2CCC_CR_MDLLCTL2_REG

#define CH3CCC_CR_MDLLCTL3_REG                                         (0x000036A4)
//Duplicate of CH2CCC_CR_MDLLCTL3_REG

#define CH3CCC_CR_COMP2_REG                                            (0x000036A8)
//Duplicate of CH2CCC_CR_COMP2_REG

#define CH3CCC_CR_COMP3_REG                                            (0x000036AC)
//Duplicate of CH2CCC_CR_COMP3_REG

#define CH3CCC_CR_DDRCRCCCVOLTAGEUSED_REG                              (0x000036B0)
//Duplicate of CH2CCC_CR_DDRCRCCCVOLTAGEUSED_REG

#define CH3CCC_CR_DDRCRCTLCACOMPOFFSET_REG                             (0x000036B4)
//Duplicate of CH2CCC_CR_DDRCRCTLCACOMPOFFSET_REG

#define CH3CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG                          (0x000036B8)
//Duplicate of CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG

#define CH3CCC_CR_CTL0_REG                                             (0x000036BC)
//Duplicate of CH2CCC_CR_CTL0_REG

#define CH3CCC_CR_CTL3_REG                                             (0x000036C0)
//Duplicate of CH2CCC_CR_CTL3_REG

#define CH3CCC_CR_MDLLCTL4_REG                                         (0x000036C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define CH3CCC_CR_CTL2_REG                                             (0x000036C8)
//Duplicate of CH2CCC_CR_CTL2_REG

#define CH3CCC_CR_TXPBDCODE0_REG                                       (0x000036CC)
//Duplicate of CH2CCC_CR_TXPBDCODE0_REG

#define CH3CCC_CR_TXPBDCODE1_REG                                       (0x000036D0)
//Duplicate of CH2CCC_CR_TXPBDCODE1_REG

#define CH3CCC_CR_TXPBDCODE2_REG                                       (0x000036D4)
//Duplicate of CH2CCC_CR_TXPBDCODE2_REG

#define CH3CCC_CR_TXPBDCODE3_REG                                       (0x000036D8)
//Duplicate of CH2CCC_CR_TXPBDCODE3_REG

#define CH3CCC_CR_TXPBDCODE4_REG                                       (0x000036DC)
//Duplicate of CH2CCC_CR_TXPBDCODE4_REG

#define CH3CCC_CR_DCCCTL6_REG                                          (0x000036E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define CH3CCC_CR_DCCCTL7_REG                                          (0x000036E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define CH3CCC_CR_DCCCTL8_REG                                          (0x000036E8)
//Duplicate of CH2CCC_CR_DCCCTL8_REG

#define CH3CCC_CR_DCCCTL9_REG                                          (0x000036EC)
//Duplicate of CH2CCC_CR_DCCCTL9_REG

#define CH3CCC_CR_DCCCTL11_REG                                         (0x000036F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define CH3CCC_CR_DCCCTL20_REG                                         (0x000036F4)
//Duplicate of CH2CCC_CR_DCCCTL20_REG

#define CH3CCC_CR_DDRCRCCCPIDIVIDER_REG                                (0x00003700)
//Duplicate of CH2CCC_CR_DDRCRCCCPIDIVIDER_REG

#define CH3CCC_CR_CLKALIGNCTL0_REG                                     (0x00003704)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define CH3CCC_CR_CLKALIGNCTL1_REG                                     (0x00003708)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define CH3CCC_CR_CLKALIGNCTL2_REG                                     (0x0000370C)
//Duplicate of CH2CCC_CR_CLKALIGNCTL2_REG

#define CH3CCC_CR_WLCTRL_A0_REG                                        (0x00003730)
#define CH3CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG                           (0x00003710)
#define CH3CCC_CR_TXPBDOFFSET_A0_REG                                   (0x0000372C)
#define CH3CCC_CR_DCCCTL0_A0_REG                                       (0x00003714)
#define CH3CCC_CR_DCCCTL1_A0_REG                                       (0x00003718)
#define CH3CCC_CR_DCCCTL2_A0_REG                                       (0x0000371C)
#define CH3CCC_CR_DCCCTL3_A0_REG                                       (0x00003720)
#define CH3CCC_CR_DCCCTL5_A0_REG                                       (0x00003724)
#define CH3CCC_CR_DCCCTL13_A0_REG                                      (0x00003728)

#define CH3CCC_CR_WLCTRL_REG                                           (0x00003710)
//Duplicate of CH2CCC_CR_WLCTRL_REG

#define CH3CCC_CR_DDRCRCCCCLKCONTROLS_REG                              (0x00003714)
//Duplicate of CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG

#define CH3CCC_CR_TXPBDOFFSET_REG                                      (0x00003718)
//Duplicate of CH2CCC_CR_TXPBDOFFSET_REG

#define CH3CCC_CR_DCCCTL0_REG                                          (0x0000371C)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define CH3CCC_CR_DCCCTL1_REG                                          (0x00003720)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define CH3CCC_CR_DCCCTL2_REG                                          (0x00003724)
//Duplicate of CH2CCC_CR_DCCCTL2_REG

#define CH3CCC_CR_DCCCTL3_REG                                          (0x00003728)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define CH3CCC_CR_DCCCTL5_REG                                          (0x0000372C)
//Duplicate of CH2CCC_CR_DCCCTL5_REG

#define CH3CCC_CR_DCCCTL13_REG                                         (0x00003730)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define CH3CCC_CR_PMFSM_REG                                            (0x00003734)
//Duplicate of CH2CCC_CR_PMFSM_REG

#define CH3CCC_CR_DDRCRCMDCOMP_REG                                     (0x00003738)
//Duplicate of CH2CCC_CR_DDRCRCMDCOMP_REG

#define CH3CCC_CR_DDRCRCLKCOMP_REG                                     (0x0000373C)
//Duplicate of CH2CCC_CR_DDRCRCLKCOMP_REG

#define CH3CCC_CR_DDRCRCTLCOMP_REG                                     (0x00003740)
//Duplicate of CH2CCC_CR_DDRCRCTLCOMP_REG

#define CH3CCC_CR_VCCDLLCOMPDATACCC_REG                                (0x00003744)
//Duplicate of CH2CCC_CR_VCCDLLCOMPDATACCC_REG

#define CH3CCC_CR_DCSOFFSET_REG                                        (0x00003748)
//Duplicate of CH2CCC_CR_DCSOFFSET_REG

#define CH3CCC_CR_DDRCRBSCANDATA_REG                                   (0x0000374C)
//Duplicate of CH2CCC_CR_DDRCRBSCANDATA_REG

#define CH3CCC_CR_DDRCRMISR_REG                                        (0x00003750)
//Duplicate of CH2CCC_CR_DDRCRMISR_REG

#define CH3CCC_CR_DDRCRMARGINMODECONTROL_REG                           (0x00003754)
//Duplicate of CH2CCC_CR_DDRCRMARGINMODECONTROL_REG

#define CH3CCC_CR_DDRCRMARGINMODEDEBUGMSB_REG                          (0x00003758)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH3CCC_CR_DDRCRMARGINMODEDEBUGLSB_REG                          (0x0000375C)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH3CCC_CR_SRZBYPASSCTL_REG                                     (0x00003760)
//Duplicate of CH2CCC_CR_SRZBYPASSCTL_REG

#define CH3CCC_CR_SRZDRVENBYPASSCTL_REG                                (0x00003764)
//Duplicate of CH2CCC_CR_SRZDRVENBYPASSCTL_REG

#define CH3CCC_CR_DFXCCMON_REG                                         (0x00003768)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define CH3CCC_CR_DCCCTL4_REG                                          (0x0000376C)
//Duplicate of CH2CCC_CR_DCCCTL4_REG

#define CH3CCC_CR_DCCCTL17_REG                                         (0x00003770)
//Duplicate of CH2CCC_CR_DCCCTL17_REG

#define CH3CCC_CR_DCCCTL18_REG                                         (0x00003774)
//Duplicate of CH2CCC_CR_DCCCTL18_REG

#define CH3CCC_CR_DCCCTL19_REG                                         (0x00003778)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define CH3CCC_CR_CLKALIGNSTATUS_REG                                   (0x0000377C)
//Duplicate of CH2CCC_CR_CLKALIGNSTATUS_REG

#define CH1CCC_CR_DDRCRPINSUSED_REG                                    (0x00003780)
//Duplicate of CH2CCC_CR_DDRCRPINSUSED_REG

#define CH1CCC_CR_CCCRSTCTL_A0_REG                                     (0x000037B8)
#define CH1CCC_CR_PICODE0_A0_REG                                       (0x00003784)
#define CH1CCC_CR_PICODE1_A0_REG                                       (0x00003788)
#define CH1CCC_CR_PICODE2_A0_REG                                       (0x0000378C)
#define CH1CCC_CR_PICODE3_A0_REG                                       (0x00003790)
#define CH1CCC_CR_MDLLCTL0_A0_REG                                      (0x00003794)
#define CH1CCC_CR_MDLLCTL1_A0_REG                                      (0x00003798)
#define CH1CCC_CR_MDLLCTL2_A0_REG                                      (0x0000379C)
#define CH1CCC_CR_MDLLCTL3_A0_REG                                      (0x000037A0)
#define CH1CCC_CR_COMP2_A0_REG                                         (0x000037A4)
#define CH1CCC_CR_COMP3_A0_REG                                         (0x000037A8)
#define CH1CCC_CR_DDRCRCCCVOLTAGEUSED_A0_REG                           (0x000037AC)
#define CH1CCC_CR_DDRCRCTLCACOMPOFFSET_A0_REG                          (0x000037B0)
#define CH1CCC_CR_DDRCRVSSHICLKCOMPOFFSET_A0_REG                       (0x000037B4)

#define CH1CCC_CR_CCCRSTCTL_REG                                        (0x00003784)
//Duplicate of CH2CCC_CR_CCCRSTCTL_REG

#define CH1CCC_CR_PICODE0_REG                                          (0x00003788)
//Duplicate of CH2CCC_CR_PICODE0_REG

#define CH1CCC_CR_PICODE1_REG                                          (0x0000378C)
//Duplicate of CH2CCC_CR_PICODE1_REG

#define CH1CCC_CR_PICODE2_REG                                          (0x00003790)
//Duplicate of CH2CCC_CR_PICODE2_REG

#define CH1CCC_CR_PICODE3_REG                                          (0x00003794)
//Duplicate of CH2CCC_CR_PICODE3_REG

#define CH1CCC_CR_MDLLCTL0_REG                                         (0x00003798)
//Duplicate of CH2CCC_CR_MDLLCTL0_REG

#define CH1CCC_CR_MDLLCTL1_REG                                         (0x0000379C)
//Duplicate of CH2CCC_CR_MDLLCTL1_REG

#define CH1CCC_CR_MDLLCTL2_REG                                         (0x000037A0)
//Duplicate of CH2CCC_CR_MDLLCTL2_REG

#define CH1CCC_CR_MDLLCTL3_REG                                         (0x000037A4)
//Duplicate of CH2CCC_CR_MDLLCTL3_REG

#define CH1CCC_CR_COMP2_REG                                            (0x000037A8)
//Duplicate of CH2CCC_CR_COMP2_REG

#define CH1CCC_CR_COMP3_REG                                            (0x000037AC)
//Duplicate of CH2CCC_CR_COMP3_REG

#define CH1CCC_CR_DDRCRCCCVOLTAGEUSED_REG                              (0x000037B0)
//Duplicate of CH2CCC_CR_DDRCRCCCVOLTAGEUSED_REG

#define CH1CCC_CR_DDRCRCTLCACOMPOFFSET_REG                             (0x000037B4)
//Duplicate of CH2CCC_CR_DDRCRCTLCACOMPOFFSET_REG

#define CH1CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG                          (0x000037B8)
//Duplicate of CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG

#define CH1CCC_CR_CTL0_REG                                             (0x000037BC)
//Duplicate of CH2CCC_CR_CTL0_REG

#define CH1CCC_CR_CTL3_REG                                             (0x000037C0)
//Duplicate of CH2CCC_CR_CTL3_REG

#define CH1CCC_CR_MDLLCTL4_REG                                         (0x000037C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define CH1CCC_CR_CTL2_REG                                             (0x000037C8)
//Duplicate of CH2CCC_CR_CTL2_REG

#define CH1CCC_CR_TXPBDCODE0_REG                                       (0x000037CC)
//Duplicate of CH2CCC_CR_TXPBDCODE0_REG

#define CH1CCC_CR_TXPBDCODE1_REG                                       (0x000037D0)
//Duplicate of CH2CCC_CR_TXPBDCODE1_REG

#define CH1CCC_CR_TXPBDCODE2_REG                                       (0x000037D4)
//Duplicate of CH2CCC_CR_TXPBDCODE2_REG

#define CH1CCC_CR_TXPBDCODE3_REG                                       (0x000037D8)
//Duplicate of CH2CCC_CR_TXPBDCODE3_REG

#define CH1CCC_CR_TXPBDCODE4_REG                                       (0x000037DC)
//Duplicate of CH2CCC_CR_TXPBDCODE4_REG

#define CH1CCC_CR_DCCCTL6_REG                                          (0x000037E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define CH1CCC_CR_DCCCTL7_REG                                          (0x000037E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define CH1CCC_CR_DCCCTL8_REG                                          (0x000037E8)
//Duplicate of CH2CCC_CR_DCCCTL8_REG

#define CH1CCC_CR_DCCCTL9_REG                                          (0x000037EC)
//Duplicate of CH2CCC_CR_DCCCTL9_REG

#define CH1CCC_CR_DCCCTL11_REG                                         (0x000037F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define CH1CCC_CR_DCCCTL20_REG                                         (0x000037F4)
//Duplicate of CH2CCC_CR_DCCCTL20_REG

#define CH1CCC_CR_DDRCRCCCPIDIVIDER_REG                                (0x00003800)
//Duplicate of CH2CCC_CR_DDRCRCCCPIDIVIDER_REG

#define CH1CCC_CR_CLKALIGNCTL0_REG                                     (0x00003804)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define CH1CCC_CR_CLKALIGNCTL1_REG                                     (0x00003808)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define CH1CCC_CR_CLKALIGNCTL2_REG                                     (0x0000380C)
//Duplicate of CH2CCC_CR_CLKALIGNCTL2_REG

#define CH1CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG                           (0x00003810)
#define CH1CCC_CR_DCCCTL0_A0_REG                                       (0x00003814)
#define CH1CCC_CR_DCCCTL1_A0_REG                                       (0x00003818)
#define CH1CCC_CR_DCCCTL2_A0_REG                                       (0x0000381C)
#define CH1CCC_CR_DCCCTL3_A0_REG                                       (0x00003820)
#define CH1CCC_CR_DCCCTL5_A0_REG                                       (0x00003824)
#define CH1CCC_CR_DCCCTL13_A0_REG                                      (0x00003828)
#define CH1CCC_CR_TXPBDOFFSET_A0_REG                                   (0x0000382C)
#define CH1CCC_CR_WLCTRL_A0_REG                                        (0x00003830)

#define CH1CCC_CR_WLCTRL_REG                                           (0x00003810)
//Duplicate of CH2CCC_CR_WLCTRL_REG

#define CH1CCC_CR_DDRCRCCCCLKCONTROLS_REG                              (0x00003814)
//Duplicate of CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG

#define CH1CCC_CR_TXPBDOFFSET_REG                                      (0x00003818)
//Duplicate of CH2CCC_CR_TXPBDOFFSET_REG

#define CH1CCC_CR_DCCCTL0_REG                                          (0x0000381C)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define CH1CCC_CR_DCCCTL1_REG                                          (0x00003820)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define CH1CCC_CR_DCCCTL2_REG                                          (0x00003824)
//Duplicate of CH2CCC_CR_DCCCTL2_REG

#define CH1CCC_CR_DCCCTL3_REG                                          (0x00003828)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define CH1CCC_CR_DCCCTL5_REG                                          (0x0000382C)
//Duplicate of CH2CCC_CR_DCCCTL5_REG

#define CH1CCC_CR_DCCCTL13_REG                                         (0x00003830)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define CH1CCC_CR_PMFSM_REG                                            (0x00003834)
//Duplicate of CH2CCC_CR_PMFSM_REG

#define CH1CCC_CR_DDRCRCMDCOMP_REG                                     (0x00003838)
//Duplicate of CH2CCC_CR_DDRCRCMDCOMP_REG

#define CH1CCC_CR_DDRCRCLKCOMP_REG                                     (0x0000383C)
//Duplicate of CH2CCC_CR_DDRCRCLKCOMP_REG

#define CH1CCC_CR_DDRCRCTLCOMP_REG                                     (0x00003840)
//Duplicate of CH2CCC_CR_DDRCRCTLCOMP_REG

#define CH1CCC_CR_VCCDLLCOMPDATACCC_REG                                (0x00003844)
//Duplicate of CH2CCC_CR_VCCDLLCOMPDATACCC_REG

#define CH1CCC_CR_DCSOFFSET_REG                                        (0x00003848)
//Duplicate of CH2CCC_CR_DCSOFFSET_REG

#define CH1CCC_CR_DDRCRBSCANDATA_REG                                   (0x0000384C)
//Duplicate of CH2CCC_CR_DDRCRBSCANDATA_REG

#define CH1CCC_CR_DDRCRMISR_REG                                        (0x00003850)
//Duplicate of CH2CCC_CR_DDRCRMISR_REG

#define CH1CCC_CR_DDRCRMARGINMODECONTROL_REG                           (0x00003854)
//Duplicate of CH2CCC_CR_DDRCRMARGINMODECONTROL_REG

#define CH1CCC_CR_DDRCRMARGINMODEDEBUGMSB_REG                          (0x00003858)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH1CCC_CR_DDRCRMARGINMODEDEBUGLSB_REG                          (0x0000385C)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH1CCC_CR_SRZBYPASSCTL_REG                                     (0x00003860)
//Duplicate of CH2CCC_CR_SRZBYPASSCTL_REG

#define CH1CCC_CR_SRZDRVENBYPASSCTL_REG                                (0x00003864)
//Duplicate of CH2CCC_CR_SRZDRVENBYPASSCTL_REG

#define CH1CCC_CR_DFXCCMON_REG                                         (0x00003868)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define CH1CCC_CR_DCCCTL4_REG                                          (0x0000386C)
//Duplicate of CH2CCC_CR_DCCCTL4_REG

#define CH1CCC_CR_DCCCTL17_REG                                         (0x00003870)
//Duplicate of CH2CCC_CR_DCCCTL17_REG

#define CH1CCC_CR_DCCCTL18_REG                                         (0x00003874)
//Duplicate of CH2CCC_CR_DCCCTL18_REG

#define CH1CCC_CR_DCCCTL19_REG                                         (0x00003878)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define CH1CCC_CR_CLKALIGNSTATUS_REG                                   (0x0000387C)
//Duplicate of CH2CCC_CR_CLKALIGNSTATUS_REG

#define CH5CCC_CR_DDRCRPINSUSED_REG                                    (0x00003880)
//Duplicate of CH2CCC_CR_DDRCRPINSUSED_REG

#define CH5CCC_CR_PICODE0_A0_REG                                       (0x00003884)
#define CH5CCC_CR_PICODE1_A0_REG                                       (0x00003888)
#define CH5CCC_CR_PICODE2_A0_REG                                       (0x0000388C)
#define CH5CCC_CR_PICODE3_A0_REG                                       (0x00003890)
#define CH5CCC_CR_MDLLCTL0_A0_REG                                      (0x00003894)
#define CH5CCC_CR_MDLLCTL1_A0_REG                                      (0x00003898)
#define CH5CCC_CR_MDLLCTL2_A0_REG                                      (0x0000389C)
#define CH5CCC_CR_MDLLCTL3_A0_REG                                      (0x000038A0)
#define CH5CCC_CR_COMP2_A0_REG                                         (0x000038A4)
#define CH5CCC_CR_COMP3_A0_REG                                         (0x000038A8)
#define CH5CCC_CR_DDRCRCCCVOLTAGEUSED_A0_REG                           (0x000038AC)
#define CH5CCC_CR_DDRCRCTLCACOMPOFFSET_A0_REG                          (0x000038B0)
#define CH5CCC_CR_DDRCRVSSHICLKCOMPOFFSET_A0_REG                       (0x000038B4)
#define CH5CCC_CR_CCCRSTCTL_A0_REG                                     (0x000038B8)

#define CH5CCC_CR_CCCRSTCTL_REG                                        (0x00003884)
//Duplicate of CH2CCC_CR_CCCRSTCTL_REG

#define CH5CCC_CR_PICODE0_REG                                          (0x00003888)
//Duplicate of CH2CCC_CR_PICODE0_REG

#define CH5CCC_CR_PICODE1_REG                                          (0x0000388C)
//Duplicate of CH2CCC_CR_PICODE1_REG

#define CH5CCC_CR_PICODE2_REG                                          (0x00003890)
//Duplicate of CH2CCC_CR_PICODE2_REG

#define CH5CCC_CR_PICODE3_REG                                          (0x00003894)
//Duplicate of CH2CCC_CR_PICODE3_REG

#define CH5CCC_CR_MDLLCTL0_REG                                         (0x00003898)
//Duplicate of CH2CCC_CR_MDLLCTL0_REG

#define CH5CCC_CR_MDLLCTL1_REG                                         (0x0000389C)
//Duplicate of CH2CCC_CR_MDLLCTL1_REG

#define CH5CCC_CR_MDLLCTL2_REG                                         (0x000038A0)
//Duplicate of CH2CCC_CR_MDLLCTL2_REG

#define CH5CCC_CR_MDLLCTL3_REG                                         (0x000038A4)
//Duplicate of CH2CCC_CR_MDLLCTL3_REG

#define CH5CCC_CR_COMP2_REG                                            (0x000038A8)
//Duplicate of CH2CCC_CR_COMP2_REG

#define CH5CCC_CR_COMP3_REG                                            (0x000038AC)
//Duplicate of CH2CCC_CR_COMP3_REG

#define CH5CCC_CR_DDRCRCCCVOLTAGEUSED_REG                              (0x000038B0)
//Duplicate of CH2CCC_CR_DDRCRCCCVOLTAGEUSED_REG

#define CH5CCC_CR_DDRCRCTLCACOMPOFFSET_REG                             (0x000038B4)
//Duplicate of CH2CCC_CR_DDRCRCTLCACOMPOFFSET_REG

#define CH5CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG                          (0x000038B8)
//Duplicate of CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG

#define CH5CCC_CR_CTL0_REG                                             (0x000038BC)
//Duplicate of CH2CCC_CR_CTL0_REG

#define CH5CCC_CR_CTL3_REG                                             (0x000038C0)
//Duplicate of CH2CCC_CR_CTL3_REG

#define CH5CCC_CR_MDLLCTL4_REG                                         (0x000038C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define CH5CCC_CR_CTL2_REG                                             (0x000038C8)
//Duplicate of CH2CCC_CR_CTL2_REG

#define CH5CCC_CR_TXPBDCODE0_REG                                       (0x000038CC)
//Duplicate of CH2CCC_CR_TXPBDCODE0_REG

#define CH5CCC_CR_TXPBDCODE1_REG                                       (0x000038D0)
//Duplicate of CH2CCC_CR_TXPBDCODE1_REG

#define CH5CCC_CR_TXPBDCODE2_REG                                       (0x000038D4)
//Duplicate of CH2CCC_CR_TXPBDCODE2_REG

#define CH5CCC_CR_TXPBDCODE3_REG                                       (0x000038D8)
//Duplicate of CH2CCC_CR_TXPBDCODE3_REG

#define CH5CCC_CR_TXPBDCODE4_REG                                       (0x000038DC)
//Duplicate of CH2CCC_CR_TXPBDCODE4_REG

#define CH5CCC_CR_DCCCTL6_REG                                          (0x000038E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define CH5CCC_CR_DCCCTL7_REG                                          (0x000038E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define CH5CCC_CR_DCCCTL8_REG                                          (0x000038E8)
//Duplicate of CH2CCC_CR_DCCCTL8_REG

#define CH5CCC_CR_DCCCTL9_REG                                          (0x000038EC)
//Duplicate of CH2CCC_CR_DCCCTL9_REG

#define CH5CCC_CR_DCCCTL11_REG                                         (0x000038F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define CH5CCC_CR_DCCCTL20_REG                                         (0x000038F4)
//Duplicate of CH2CCC_CR_DCCCTL20_REG

#define CH5CCC_CR_DDRCRCCCPIDIVIDER_REG                                (0x00003900)
//Duplicate of CH2CCC_CR_DDRCRCCCPIDIVIDER_REG

#define CH5CCC_CR_CLKALIGNCTL0_REG                                     (0x00003904)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define CH5CCC_CR_CLKALIGNCTL1_REG                                     (0x00003908)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define CH5CCC_CR_CLKALIGNCTL2_REG                                     (0x0000390C)
//Duplicate of CH2CCC_CR_CLKALIGNCTL2_REG

#define CH5CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG                           (0x00003910)
#define CH5CCC_CR_DCCCTL0_A0_REG                                       (0x00003914)
#define CH5CCC_CR_DCCCTL1_A0_REG                                       (0x00003918)
#define CH5CCC_CR_DCCCTL2_A0_REG                                       (0x0000391C)
#define CH5CCC_CR_DCCCTL3_A0_REG                                       (0x00003920)
#define CH5CCC_CR_DCCCTL5_A0_REG                                       (0x00003924)
#define CH5CCC_CR_DCCCTL13_A0_REG                                      (0x00003928)
#define CH5CCC_CR_TXPBDOFFSET_A0_REG                                   (0x0000392C)
#define CH5CCC_CR_WLCTRL_A0_REG                                        (0x00003930)
#define CH5CCC_CR_WLCTRL_REG                                           (0x00003910)
//Duplicate of CH2CCC_CR_WLCTRL_REG

#define CH5CCC_CR_DDRCRCCCCLKCONTROLS_REG                              (0x00003914)
//Duplicate of CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG

#define CH5CCC_CR_TXPBDOFFSET_REG                                      (0x00003918)
//Duplicate of CH2CCC_CR_TXPBDOFFSET_REG

#define CH5CCC_CR_DCCCTL0_REG                                          (0x0000391C)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define CH5CCC_CR_DCCCTL1_REG                                          (0x00003920)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define CH5CCC_CR_DCCCTL2_REG                                          (0x00003924)
//Duplicate of CH2CCC_CR_DCCCTL2_REG

#define CH5CCC_CR_DCCCTL3_REG                                          (0x00003928)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define CH5CCC_CR_DCCCTL5_REG                                          (0x0000392C)
//Duplicate of CH2CCC_CR_DCCCTL5_REG

#define CH5CCC_CR_DCCCTL13_REG                                         (0x00003930)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define CH5CCC_CR_PMFSM_REG                                            (0x00003934)
//Duplicate of CH2CCC_CR_PMFSM_REG

#define CH5CCC_CR_DDRCRCMDCOMP_REG                                     (0x00003938)
//Duplicate of CH2CCC_CR_DDRCRCMDCOMP_REG

#define CH5CCC_CR_DDRCRCLKCOMP_REG                                     (0x0000393C)
//Duplicate of CH2CCC_CR_DDRCRCLKCOMP_REG

#define CH5CCC_CR_DDRCRCTLCOMP_REG                                     (0x00003940)
//Duplicate of CH2CCC_CR_DDRCRCTLCOMP_REG

#define CH5CCC_CR_VCCDLLCOMPDATACCC_REG                                (0x00003944)
//Duplicate of CH2CCC_CR_VCCDLLCOMPDATACCC_REG

#define CH5CCC_CR_DCSOFFSET_REG                                        (0x00003948)
//Duplicate of CH2CCC_CR_DCSOFFSET_REG

#define CH5CCC_CR_DDRCRBSCANDATA_REG                                   (0x0000394C)
//Duplicate of CH2CCC_CR_DDRCRBSCANDATA_REG

#define CH5CCC_CR_DDRCRMISR_REG                                        (0x00003950)
//Duplicate of CH2CCC_CR_DDRCRMISR_REG

#define CH5CCC_CR_DDRCRMARGINMODECONTROL_REG                           (0x00003954)
//Duplicate of CH2CCC_CR_DDRCRMARGINMODECONTROL_REG

#define CH5CCC_CR_DDRCRMARGINMODEDEBUGMSB_REG                          (0x00003958)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH5CCC_CR_DDRCRMARGINMODEDEBUGLSB_REG                          (0x0000395C)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH5CCC_CR_SRZBYPASSCTL_REG                                     (0x00003960)
//Duplicate of CH2CCC_CR_SRZBYPASSCTL_REG

#define CH5CCC_CR_SRZDRVENBYPASSCTL_REG                                (0x00003964)
//Duplicate of CH2CCC_CR_SRZDRVENBYPASSCTL_REG

#define CH5CCC_CR_DFXCCMON_REG                                         (0x00003968)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define CH5CCC_CR_DCCCTL4_REG                                          (0x0000396C)
//Duplicate of CH2CCC_CR_DCCCTL4_REG

#define CH5CCC_CR_DCCCTL17_REG                                         (0x00003970)
//Duplicate of CH2CCC_CR_DCCCTL17_REG

#define CH5CCC_CR_DCCCTL18_REG                                         (0x00003974)
//Duplicate of CH2CCC_CR_DCCCTL18_REG

#define CH5CCC_CR_DCCCTL19_REG                                         (0x00003978)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define CH5CCC_CR_CLKALIGNSTATUS_REG                                   (0x0000397C)
//Duplicate of CH2CCC_CR_CLKALIGNSTATUS_REG

#define CH7CCC_CR_DDRCRPINSUSED_REG                                    (0x00003980)
//Duplicate of CH2CCC_CR_DDRCRPINSUSED_REG

#define CH7CCC_CR_PICODE0_A0_REG                                       (0x00003984)
#define CH7CCC_CR_PICODE1_A0_REG                                       (0x00003988)
#define CH7CCC_CR_PICODE2_A0_REG                                       (0x0000398C)
#define CH7CCC_CR_PICODE3_A0_REG                                       (0x00003990)
#define CH7CCC_CR_MDLLCTL0_A0_REG                                      (0x00003994)
#define CH7CCC_CR_MDLLCTL1_A0_REG                                      (0x00003998)
#define CH7CCC_CR_MDLLCTL2_A0_REG                                      (0x0000399C)
#define CH7CCC_CR_MDLLCTL3_A0_REG                                      (0x000039A0)
#define CH7CCC_CR_COMP2_A0_REG                                         (0x000039A4)
#define CH7CCC_CR_COMP3_A0_REG                                         (0x000039A8)
#define CH7CCC_CR_DDRCRCCCVOLTAGEUSED_A0_REG                           (0x000039AC)
#define CH7CCC_CR_DDRCRCTLCACOMPOFFSET_A0_REG                          (0x000039B0)
#define CH7CCC_CR_DDRCRVSSHICLKCOMPOFFSET_A0_REG                       (0x000039B4)
#define CH7CCC_CR_CCCRSTCTL_A0_REG                                     (0x000039B8)

#define CH7CCC_CR_CCCRSTCTL_REG                                        (0x00003984)
//Duplicate of CH2CCC_CR_CCCRSTCTL_REG

#define CH7CCC_CR_PICODE0_REG                                          (0x00003988)
//Duplicate of CH2CCC_CR_PICODE0_REG

#define CH7CCC_CR_PICODE1_REG                                          (0x0000398C)
//Duplicate of CH2CCC_CR_PICODE1_REG

#define CH7CCC_CR_PICODE2_REG                                          (0x00003990)
//Duplicate of CH2CCC_CR_PICODE2_REG

#define CH7CCC_CR_PICODE3_REG                                          (0x00003994)
//Duplicate of CH2CCC_CR_PICODE3_REG

#define CH7CCC_CR_MDLLCTL0_REG                                         (0x00003998)
//Duplicate of CH2CCC_CR_MDLLCTL0_REG

#define CH7CCC_CR_MDLLCTL1_REG                                         (0x0000399C)
//Duplicate of CH2CCC_CR_MDLLCTL1_REG

#define CH7CCC_CR_MDLLCTL2_REG                                         (0x000039A0)
//Duplicate of CH2CCC_CR_MDLLCTL2_REG

#define CH7CCC_CR_MDLLCTL3_REG                                         (0x000039A4)
//Duplicate of CH2CCC_CR_MDLLCTL3_REG

#define CH7CCC_CR_COMP2_REG                                            (0x000039A8)
//Duplicate of CH2CCC_CR_COMP2_REG

#define CH7CCC_CR_COMP3_REG                                            (0x000039AC)
//Duplicate of CH2CCC_CR_COMP3_REG

#define CH7CCC_CR_DDRCRCCCVOLTAGEUSED_REG                              (0x000039B0)
//Duplicate of CH2CCC_CR_DDRCRCCCVOLTAGEUSED_REG

#define CH7CCC_CR_DDRCRCTLCACOMPOFFSET_REG                             (0x000039B4)
//Duplicate of CH2CCC_CR_DDRCRCTLCACOMPOFFSET_REG

#define CH7CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG                          (0x000039B8)
//Duplicate of CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG

#define CH7CCC_CR_CTL0_REG                                             (0x000039BC)
//Duplicate of CH2CCC_CR_CTL0_REG

#define CH7CCC_CR_CTL3_REG                                             (0x000039C0)
//Duplicate of CH2CCC_CR_CTL3_REG

#define CH7CCC_CR_MDLLCTL4_REG                                         (0x000039C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define CH7CCC_CR_CTL2_REG                                             (0x000039C8)
//Duplicate of CH2CCC_CR_CTL2_REG

#define CH7CCC_CR_TXPBDCODE0_REG                                       (0x000039CC)
//Duplicate of CH2CCC_CR_TXPBDCODE0_REG

#define CH7CCC_CR_TXPBDCODE1_REG                                       (0x000039D0)
//Duplicate of CH2CCC_CR_TXPBDCODE1_REG

#define CH7CCC_CR_TXPBDCODE2_REG                                       (0x000039D4)
//Duplicate of CH2CCC_CR_TXPBDCODE2_REG

#define CH7CCC_CR_TXPBDCODE3_REG                                       (0x000039D8)
//Duplicate of CH2CCC_CR_TXPBDCODE3_REG

#define CH7CCC_CR_TXPBDCODE4_REG                                       (0x000039DC)
//Duplicate of CH2CCC_CR_TXPBDCODE4_REG

#define CH7CCC_CR_DCCCTL6_REG                                          (0x000039E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define CH7CCC_CR_DCCCTL7_REG                                          (0x000039E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define CH7CCC_CR_DCCCTL8_REG                                          (0x000039E8)
//Duplicate of CH2CCC_CR_DCCCTL8_REG

#define CH7CCC_CR_DCCCTL9_REG                                          (0x000039EC)
//Duplicate of CH2CCC_CR_DCCCTL9_REG

#define CH7CCC_CR_DCCCTL11_REG                                         (0x000039F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define CH7CCC_CR_DCCCTL20_REG                                         (0x000039F4)
//Duplicate of CH2CCC_CR_DCCCTL20_REG

#define CH7CCC_CR_DDRCRCCCPIDIVIDER_REG                                (0x00003A00)
//Duplicate of CH2CCC_CR_DDRCRCCCPIDIVIDER_REG

#define CH7CCC_CR_CLKALIGNCTL0_REG                                     (0x00003A04)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define CH7CCC_CR_CLKALIGNCTL1_REG                                     (0x00003A08)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define CH7CCC_CR_CLKALIGNCTL2_REG                                     (0x00003A0C)
//Duplicate of CH2CCC_CR_CLKALIGNCTL2_REG

#define CH7CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG                           (0x00003A10)
#define CH7CCC_CR_DCCCTL0_A0_REG                                       (0x00003A14)
#define CH7CCC_CR_DCCCTL1_A0_REG                                       (0x00003A18)
#define CH7CCC_CR_DCCCTL2_A0_REG                                       (0x00003A1C)
#define CH7CCC_CR_DCCCTL3_A0_REG                                       (0x00003A20)
#define CH7CCC_CR_DCCCTL5_A0_REG                                       (0x00003A24)
#define CH7CCC_CR_DCCCTL13_A0_REG                                      (0x00003A28)
#define CH7CCC_CR_TXPBDOFFSET_A0_REG                                   (0x00003A2C)
#define CH7CCC_CR_WLCTRL_A0_REG                                        (0x00003A30)

#define CH7CCC_CR_WLCTRL_REG                                           (0x00003A10)
//Duplicate of CH2CCC_CR_WLCTRL_REG

#define CH7CCC_CR_DDRCRCCCCLKCONTROLS_REG                              (0x00003A14)
//Duplicate of CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG

#define CH7CCC_CR_TXPBDOFFSET_REG                                      (0x00003A18)
//Duplicate of CH2CCC_CR_TXPBDOFFSET_REG

#define CH7CCC_CR_DCCCTL0_REG                                          (0x00003A1C)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define CH7CCC_CR_DCCCTL1_REG                                          (0x00003A20)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define CH7CCC_CR_DCCCTL2_REG                                          (0x00003A24)
//Duplicate of CH2CCC_CR_DCCCTL2_REG

#define CH7CCC_CR_DCCCTL3_REG                                          (0x00003A28)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define CH7CCC_CR_DCCCTL5_REG                                          (0x00003A2C)
//Duplicate of CH2CCC_CR_DCCCTL5_REG

#define CH7CCC_CR_DCCCTL13_REG                                         (0x00003A30)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define CH7CCC_CR_PMFSM_REG                                            (0x00003A34)
//Duplicate of CH2CCC_CR_PMFSM_REG

#define CH7CCC_CR_DDRCRCMDCOMP_REG                                     (0x00003A38)
//Duplicate of CH2CCC_CR_DDRCRCMDCOMP_REG

#define CH7CCC_CR_DDRCRCLKCOMP_REG                                     (0x00003A3C)
//Duplicate of CH2CCC_CR_DDRCRCLKCOMP_REG

#define CH7CCC_CR_DDRCRCTLCOMP_REG                                     (0x00003A40)
//Duplicate of CH2CCC_CR_DDRCRCTLCOMP_REG

#define CH7CCC_CR_VCCDLLCOMPDATACCC_REG                                (0x00003A44)
//Duplicate of CH2CCC_CR_VCCDLLCOMPDATACCC_REG

#define CH7CCC_CR_DCSOFFSET_REG                                        (0x00003A48)
//Duplicate of CH2CCC_CR_DCSOFFSET_REG

#define CH7CCC_CR_DDRCRBSCANDATA_REG                                   (0x00003A4C)
//Duplicate of CH2CCC_CR_DDRCRBSCANDATA_REG

#define CH7CCC_CR_DDRCRMISR_REG                                        (0x00003A50)
//Duplicate of CH2CCC_CR_DDRCRMISR_REG

#define CH7CCC_CR_DDRCRMARGINMODECONTROL_REG                           (0x00003A54)
//Duplicate of CH2CCC_CR_DDRCRMARGINMODECONTROL_REG

#define CH7CCC_CR_DDRCRMARGINMODEDEBUGMSB_REG                          (0x00003A58)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH7CCC_CR_DDRCRMARGINMODEDEBUGLSB_REG                          (0x00003A5C)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH7CCC_CR_SRZBYPASSCTL_REG                                     (0x00003A60)
//Duplicate of CH2CCC_CR_SRZBYPASSCTL_REG

#define CH7CCC_CR_SRZDRVENBYPASSCTL_REG                                (0x00003A64)
//Duplicate of CH2CCC_CR_SRZDRVENBYPASSCTL_REG

#define CH7CCC_CR_DFXCCMON_REG                                         (0x00003A68)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define CH7CCC_CR_DCCCTL4_REG                                          (0x00003A6C)
//Duplicate of CH2CCC_CR_DCCCTL4_REG

#define CH7CCC_CR_DCCCTL17_REG                                         (0x00003A70)
//Duplicate of CH2CCC_CR_DCCCTL17_REG

#define CH7CCC_CR_DCCCTL18_REG                                         (0x00003A74)
//Duplicate of CH2CCC_CR_DCCCTL18_REG

#define CH7CCC_CR_DCCCTL19_REG                                         (0x00003A78)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define CH7CCC_CR_CLKALIGNSTATUS_REG                                   (0x00003A7C)
//Duplicate of CH2CCC_CR_CLKALIGNSTATUS_REG

#define CH4CCC_CR_DDRCRPINSUSED_REG                                    (0x00003A80)
//Duplicate of CH2CCC_CR_DDRCRPINSUSED_REG

#define CH4CCC_CR_PICODE0_A0_REG                                       (0x00003A84)
#define CH4CCC_CR_PICODE1_A0_REG                                       (0x00003A88)
#define CH4CCC_CR_PICODE2_A0_REG                                       (0x00003A8C)
#define CH4CCC_CR_PICODE3_A0_REG                                       (0x00003A90)
#define CH4CCC_CR_MDLLCTL0_A0_REG                                      (0x00003A94)
#define CH4CCC_CR_MDLLCTL1_A0_REG                                      (0x00003A98)
#define CH4CCC_CR_MDLLCTL2_A0_REG                                      (0x00003A9C)
#define CH4CCC_CR_MDLLCTL3_A0_REG                                      (0x00003AA0)
#define CH4CCC_CR_COMP2_A0_REG                                         (0x00003AA4)
#define CH4CCC_CR_COMP3_A0_REG                                         (0x00003AA8)
#define CH4CCC_CR_DDRCRCCCVOLTAGEUSED_A0_REG                           (0x00003AAC)
#define CH4CCC_CR_DDRCRCTLCACOMPOFFSET_A0_REG                          (0x00003AB0)
#define CH4CCC_CR_DDRCRVSSHICLKCOMPOFFSET_A0_REG                       (0x00003AB4)
#define CH4CCC_CR_CCCRSTCTL_A0_REG                                     (0x00003AB8)

#define CH4CCC_CR_CCCRSTCTL_REG                                        (0x00003A84)
//Duplicate of CH2CCC_CR_CCCRSTCTL_REG

#define CH4CCC_CR_PICODE0_REG                                          (0x00003A88)
//Duplicate of CH2CCC_CR_PICODE0_REG

#define CH4CCC_CR_PICODE1_REG                                          (0x00003A8C)
//Duplicate of CH2CCC_CR_PICODE1_REG

#define CH4CCC_CR_PICODE2_REG                                          (0x00003A90)
//Duplicate of CH2CCC_CR_PICODE2_REG

#define CH4CCC_CR_PICODE3_REG                                          (0x00003A94)
//Duplicate of CH2CCC_CR_PICODE3_REG

#define CH4CCC_CR_MDLLCTL0_REG                                         (0x00003A98)
//Duplicate of CH2CCC_CR_MDLLCTL0_REG

#define CH4CCC_CR_MDLLCTL1_REG                                         (0x00003A9C)
//Duplicate of CH2CCC_CR_MDLLCTL1_REG

#define CH4CCC_CR_MDLLCTL2_REG                                         (0x00003AA0)
//Duplicate of CH2CCC_CR_MDLLCTL2_REG

#define CH4CCC_CR_MDLLCTL3_REG                                         (0x00003AA4)
//Duplicate of CH2CCC_CR_MDLLCTL3_REG

#define CH4CCC_CR_COMP2_REG                                            (0x00003AA8)
//Duplicate of CH2CCC_CR_COMP2_REG

#define CH4CCC_CR_COMP3_REG                                            (0x00003AAC)
//Duplicate of CH2CCC_CR_COMP3_REG

#define CH4CCC_CR_DDRCRCCCVOLTAGEUSED_REG                              (0x00003AB0)
//Duplicate of CH2CCC_CR_DDRCRCCCVOLTAGEUSED_REG

#define CH4CCC_CR_DDRCRCTLCACOMPOFFSET_REG                             (0x00003AB4)
//Duplicate of CH2CCC_CR_DDRCRCTLCACOMPOFFSET_REG

#define CH4CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG                          (0x00003AB8)
//Duplicate of CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG

#define CH4CCC_CR_CTL0_REG                                             (0x00003ABC)
//Duplicate of CH2CCC_CR_CTL0_REG

#define CH4CCC_CR_CTL3_REG                                             (0x00003AC0)
//Duplicate of CH2CCC_CR_CTL3_REG

#define CH4CCC_CR_MDLLCTL4_REG                                         (0x00003AC4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define CH4CCC_CR_CTL2_REG                                             (0x00003AC8)
//Duplicate of CH2CCC_CR_CTL2_REG

#define CH4CCC_CR_TXPBDCODE0_REG                                       (0x00003ACC)
//Duplicate of CH2CCC_CR_TXPBDCODE0_REG

#define CH4CCC_CR_TXPBDCODE1_REG                                       (0x00003AD0)
//Duplicate of CH2CCC_CR_TXPBDCODE1_REG

#define CH4CCC_CR_TXPBDCODE2_REG                                       (0x00003AD4)
//Duplicate of CH2CCC_CR_TXPBDCODE2_REG

#define CH4CCC_CR_TXPBDCODE3_REG                                       (0x00003AD8)
//Duplicate of CH2CCC_CR_TXPBDCODE3_REG

#define CH4CCC_CR_TXPBDCODE4_REG                                       (0x00003ADC)
//Duplicate of CH2CCC_CR_TXPBDCODE4_REG

#define CH4CCC_CR_DCCCTL6_REG                                          (0x00003AE0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define CH4CCC_CR_DCCCTL7_REG                                          (0x00003AE4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define CH4CCC_CR_DCCCTL8_REG                                          (0x00003AE8)
//Duplicate of CH2CCC_CR_DCCCTL8_REG

#define CH4CCC_CR_DCCCTL9_REG                                          (0x00003AEC)
//Duplicate of CH2CCC_CR_DCCCTL9_REG

#define CH4CCC_CR_DCCCTL11_REG                                         (0x00003AF0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define CH4CCC_CR_DCCCTL20_REG                                         (0x00003AF4)
//Duplicate of CH2CCC_CR_DCCCTL20_REG

#define CH4CCC_CR_DDRCRCCCPIDIVIDER_REG                                (0x00003B00)
//Duplicate of CH2CCC_CR_DDRCRCCCPIDIVIDER_REG

#define CH4CCC_CR_CLKALIGNCTL0_REG                                     (0x00003B04)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define CH4CCC_CR_CLKALIGNCTL1_REG                                     (0x00003B08)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define CH4CCC_CR_CLKALIGNCTL2_REG                                     (0x00003B0C)
//Duplicate of CH2CCC_CR_CLKALIGNCTL2_REG

#define CH4CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG                           (0x00003B10)
#define CH4CCC_CR_DCCCTL0_A0_REG                                       (0x00003B14)
#define CH4CCC_CR_DCCCTL1_A0_REG                                       (0x00003B18)
#define CH4CCC_CR_DCCCTL2_A0_REG                                       (0x00003B1C)
#define CH4CCC_CR_DCCCTL3_A0_REG                                       (0x00003B20)
#define CH4CCC_CR_DCCCTL5_A0_REG                                       (0x00003B24)
#define CH4CCC_CR_DCCCTL13_A0_REG                                      (0x00003B28)
#define CH4CCC_CR_TXPBDOFFSET_A0_REG                                   (0x00003B2C)
#define CH4CCC_CR_WLCTRL_A0_REG                                        (0x00003B30)

#define CH4CCC_CR_WLCTRL_REG                                           (0x00003B10)
//Duplicate of CH2CCC_CR_WLCTRL_REG

#define CH4CCC_CR_DDRCRCCCCLKCONTROLS_REG                              (0x00003B14)
//Duplicate of CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG

#define CH4CCC_CR_TXPBDOFFSET_REG                                      (0x00003B18)
//Duplicate of CH2CCC_CR_TXPBDOFFSET_REG

#define CH4CCC_CR_DCCCTL0_REG                                          (0x00003B1C)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define CH4CCC_CR_DCCCTL1_REG                                          (0x00003B20)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define CH4CCC_CR_DCCCTL2_REG                                          (0x00003B24)
//Duplicate of CH2CCC_CR_DCCCTL2_REG

#define CH4CCC_CR_DCCCTL3_REG                                          (0x00003B28)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define CH4CCC_CR_DCCCTL5_REG                                          (0x00003B2C)
//Duplicate of CH2CCC_CR_DCCCTL5_REG

#define CH4CCC_CR_DCCCTL13_REG                                         (0x00003B30)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define CH4CCC_CR_PMFSM_REG                                            (0x00003B34)
//Duplicate of CH2CCC_CR_PMFSM_REG

#define CH4CCC_CR_DDRCRCMDCOMP_REG                                     (0x00003B38)
//Duplicate of CH2CCC_CR_DDRCRCMDCOMP_REG

#define CH4CCC_CR_DDRCRCLKCOMP_REG                                     (0x00003B3C)
//Duplicate of CH2CCC_CR_DDRCRCLKCOMP_REG

#define CH4CCC_CR_DDRCRCTLCOMP_REG                                     (0x00003B40)
//Duplicate of CH2CCC_CR_DDRCRCTLCOMP_REG

#define CH4CCC_CR_VCCDLLCOMPDATACCC_REG                                (0x00003B44)
//Duplicate of CH2CCC_CR_VCCDLLCOMPDATACCC_REG

#define CH4CCC_CR_DCSOFFSET_REG                                        (0x00003B48)
//Duplicate of CH2CCC_CR_DCSOFFSET_REG

#define CH4CCC_CR_DDRCRBSCANDATA_REG                                   (0x00003B4C)
//Duplicate of CH2CCC_CR_DDRCRBSCANDATA_REG

#define CH4CCC_CR_DDRCRMISR_REG                                        (0x00003B50)
//Duplicate of CH2CCC_CR_DDRCRMISR_REG

#define CH4CCC_CR_DDRCRMARGINMODECONTROL_REG                           (0x00003B54)
//Duplicate of CH2CCC_CR_DDRCRMARGINMODECONTROL_REG

#define CH4CCC_CR_DDRCRMARGINMODEDEBUGMSB_REG                          (0x00003B58)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH4CCC_CR_DDRCRMARGINMODEDEBUGLSB_REG                          (0x00003B5C)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH4CCC_CR_SRZBYPASSCTL_REG                                     (0x00003B60)
//Duplicate of CH2CCC_CR_SRZBYPASSCTL_REG

#define CH4CCC_CR_SRZDRVENBYPASSCTL_REG                                (0x00003B64)
//Duplicate of CH2CCC_CR_SRZDRVENBYPASSCTL_REG

#define CH4CCC_CR_DFXCCMON_REG                                         (0x00003B68)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define CH4CCC_CR_DCCCTL4_REG                                          (0x00003B6C)
//Duplicate of CH2CCC_CR_DCCCTL4_REG

#define CH4CCC_CR_DCCCTL17_REG                                         (0x00003B70)
//Duplicate of CH2CCC_CR_DCCCTL17_REG

#define CH4CCC_CR_DCCCTL18_REG                                         (0x00003B74)
//Duplicate of CH2CCC_CR_DCCCTL18_REG

#define CH4CCC_CR_DCCCTL19_REG                                         (0x00003B78)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define CH4CCC_CR_CLKALIGNSTATUS_REG                                   (0x00003B7C)
//Duplicate of CH2CCC_CR_CLKALIGNSTATUS_REG

#define CH6CCC_CR_DDRCRPINSUSED_REG                                    (0x00003B80)
//Duplicate of CH2CCC_CR_DDRCRPINSUSED_REG

#define CH6CCC_CR_PICODE0_A0_REG                                       (0x00003B84)
#define CH6CCC_CR_PICODE1_A0_REG                                       (0x00003B88)
#define CH6CCC_CR_PICODE2_A0_REG                                       (0x00003B8C)
#define CH6CCC_CR_PICODE3_A0_REG                                       (0x00003B90)
#define CH6CCC_CR_MDLLCTL0_A0_REG                                      (0x00003B94)
#define CH6CCC_CR_MDLLCTL1_A0_REG                                      (0x00003B98)
#define CH6CCC_CR_MDLLCTL2_A0_REG                                      (0x00003B9C)
#define CH6CCC_CR_MDLLCTL3_A0_REG                                      (0x00003BA0)
#define CH6CCC_CR_COMP2_A0_REG                                         (0x00003BA4)
#define CH6CCC_CR_COMP3_A0_REG                                         (0x00003BA8)
#define CH6CCC_CR_DDRCRCCCVOLTAGEUSED_A0_REG                           (0x00003BAC)
#define CH6CCC_CR_DDRCRCTLCACOMPOFFSET_A0_REG                          (0x00003BB0)
#define CH6CCC_CR_DDRCRVSSHICLKCOMPOFFSET_A0_REG                       (0x00003BB4)
#define CH6CCC_CR_CCCRSTCTL_A0_REG                                     (0x00003BB8)

#define CH6CCC_CR_CCCRSTCTL_REG                                        (0x00003B84)
//Duplicate of CH2CCC_CR_CCCRSTCTL_REG

#define CH6CCC_CR_PICODE0_REG                                          (0x00003B88)
//Duplicate of CH2CCC_CR_PICODE0_REG

#define CH6CCC_CR_PICODE1_REG                                          (0x00003B8C)
//Duplicate of CH2CCC_CR_PICODE1_REG

#define CH6CCC_CR_PICODE2_REG                                          (0x00003B90)
//Duplicate of CH2CCC_CR_PICODE2_REG

#define CH6CCC_CR_PICODE3_REG                                          (0x00003B94)
//Duplicate of CH2CCC_CR_PICODE3_REG

#define CH6CCC_CR_MDLLCTL0_REG                                         (0x00003B98)
//Duplicate of CH2CCC_CR_MDLLCTL0_REG

#define CH6CCC_CR_MDLLCTL1_REG                                         (0x00003B9C)
//Duplicate of CH2CCC_CR_MDLLCTL1_REG

#define CH6CCC_CR_MDLLCTL2_REG                                         (0x00003BA0)
//Duplicate of CH2CCC_CR_MDLLCTL2_REG

#define CH6CCC_CR_MDLLCTL3_REG                                         (0x00003BA4)
//Duplicate of CH2CCC_CR_MDLLCTL3_REG

#define CH6CCC_CR_COMP2_REG                                            (0x00003BA8)
//Duplicate of CH2CCC_CR_COMP2_REG

#define CH6CCC_CR_COMP3_REG                                            (0x00003BAC)
//Duplicate of CH2CCC_CR_COMP3_REG

#define CH6CCC_CR_DDRCRCCCVOLTAGEUSED_REG                              (0x00003BB0)
//Duplicate of CH2CCC_CR_DDRCRCCCVOLTAGEUSED_REG

#define CH6CCC_CR_DDRCRCTLCACOMPOFFSET_REG                             (0x00003BB4)
//Duplicate of CH2CCC_CR_DDRCRCTLCACOMPOFFSET_REG

#define CH6CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG                          (0x00003BB8)
//Duplicate of CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG

#define CH6CCC_CR_CTL0_REG                                             (0x00003BBC)
//Duplicate of CH2CCC_CR_CTL0_REG

#define CH6CCC_CR_CTL3_REG                                             (0x00003BC0)
//Duplicate of CH2CCC_CR_CTL3_REG

#define CH6CCC_CR_MDLLCTL4_REG                                         (0x00003BC4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define CH6CCC_CR_CTL2_REG                                             (0x00003BC8)
//Duplicate of CH2CCC_CR_CTL2_REG

#define CH6CCC_CR_TXPBDCODE0_REG                                       (0x00003BCC)
//Duplicate of CH2CCC_CR_TXPBDCODE0_REG

#define CH6CCC_CR_TXPBDCODE1_REG                                       (0x00003BD0)
//Duplicate of CH2CCC_CR_TXPBDCODE1_REG

#define CH6CCC_CR_TXPBDCODE2_REG                                       (0x00003BD4)
//Duplicate of CH2CCC_CR_TXPBDCODE2_REG

#define CH6CCC_CR_TXPBDCODE3_REG                                       (0x00003BD8)
//Duplicate of CH2CCC_CR_TXPBDCODE3_REG

#define CH6CCC_CR_TXPBDCODE4_REG                                       (0x00003BDC)
//Duplicate of CH2CCC_CR_TXPBDCODE4_REG

#define CH6CCC_CR_DCCCTL6_REG                                          (0x00003BE0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define CH6CCC_CR_DCCCTL7_REG                                          (0x00003BE4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define CH6CCC_CR_DCCCTL8_REG                                          (0x00003BE8)
//Duplicate of CH2CCC_CR_DCCCTL8_REG

#define CH6CCC_CR_DCCCTL9_REG                                          (0x00003BEC)
//Duplicate of CH2CCC_CR_DCCCTL9_REG

#define CH6CCC_CR_DCCCTL11_REG                                         (0x00003BF0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define CH6CCC_CR_DCCCTL20_REG                                         (0x00003BF4)
//Duplicate of CH2CCC_CR_DCCCTL20_REG

#define CH6CCC_CR_DDRCRCCCPIDIVIDER_REG                                (0x00003C00)
//Duplicate of CH2CCC_CR_DDRCRCCCPIDIVIDER_REG

#define CH6CCC_CR_CLKALIGNCTL0_REG                                     (0x00003C04)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define CH6CCC_CR_CLKALIGNCTL1_REG                                     (0x00003C08)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define CH6CCC_CR_CLKALIGNCTL2_REG                                     (0x00003C0C)
//Duplicate of CH2CCC_CR_CLKALIGNCTL2_REG

#define CH6CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG                           (0x00003C10)
#define CH6CCC_CR_DCCCTL0_A0_REG                                       (0x00003C14)
#define CH6CCC_CR_DCCCTL1_A0_REG                                       (0x00003C18)
#define CH6CCC_CR_DCCCTL2_A0_REG                                       (0x00003C1C)
#define CH6CCC_CR_DCCCTL3_A0_REG                                       (0x00003C20)
#define CH6CCC_CR_DCCCTL5_A0_REG                                       (0x00003C24)
#define CH6CCC_CR_DCCCTL13_A0_REG                                      (0x00003C28)
#define CH6CCC_CR_TXPBDOFFSET_A0_REG                                   (0x00003C2C)
#define CH6CCC_CR_WLCTRL_A0_REG                                        (0x00003C30)

#define CH6CCC_CR_WLCTRL_REG                                           (0x00003C10)
//Duplicate of CH2CCC_CR_WLCTRL_REG

#define CH6CCC_CR_DDRCRCCCCLKCONTROLS_REG                              (0x00003C14)
//Duplicate of CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG

#define CH6CCC_CR_TXPBDOFFSET_REG                                      (0x00003C18)
//Duplicate of CH2CCC_CR_TXPBDOFFSET_REG

#define CH6CCC_CR_DCCCTL0_REG                                          (0x00003C1C)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define CH6CCC_CR_DCCCTL1_REG                                          (0x00003C20)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define CH6CCC_CR_DCCCTL2_REG                                          (0x00003C24)
//Duplicate of CH2CCC_CR_DCCCTL2_REG

#define CH6CCC_CR_DCCCTL3_REG                                          (0x00003C28)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define CH6CCC_CR_DCCCTL5_REG                                          (0x00003C2C)
//Duplicate of CH2CCC_CR_DCCCTL5_REG

#define CH6CCC_CR_DCCCTL13_REG                                         (0x00003C30)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define CH6CCC_CR_PMFSM_REG                                            (0x00003C34)
//Duplicate of CH2CCC_CR_PMFSM_REG

#define CH6CCC_CR_DDRCRCMDCOMP_REG                                     (0x00003C38)
//Duplicate of CH2CCC_CR_DDRCRCMDCOMP_REG

#define CH6CCC_CR_DDRCRCLKCOMP_REG                                     (0x00003C3C)
//Duplicate of CH2CCC_CR_DDRCRCLKCOMP_REG

#define CH6CCC_CR_DDRCRCTLCOMP_REG                                     (0x00003C40)
//Duplicate of CH2CCC_CR_DDRCRCTLCOMP_REG

#define CH6CCC_CR_VCCDLLCOMPDATACCC_REG                                (0x00003C44)
//Duplicate of CH2CCC_CR_VCCDLLCOMPDATACCC_REG

#define CH6CCC_CR_DCSOFFSET_REG                                        (0x00003C48)
//Duplicate of CH2CCC_CR_DCSOFFSET_REG

#define CH6CCC_CR_DDRCRBSCANDATA_REG                                   (0x00003C4C)
//Duplicate of CH2CCC_CR_DDRCRBSCANDATA_REG

#define CH6CCC_CR_DDRCRMISR_REG                                        (0x00003C50)
//Duplicate of CH2CCC_CR_DDRCRMISR_REG

#define CH6CCC_CR_DDRCRMARGINMODECONTROL_REG                           (0x00003C54)
//Duplicate of CH2CCC_CR_DDRCRMARGINMODECONTROL_REG

#define CH6CCC_CR_DDRCRMARGINMODEDEBUGMSB_REG                          (0x00003C58)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH6CCC_CR_DDRCRMARGINMODEDEBUGLSB_REG                          (0x00003C5C)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define CH6CCC_CR_SRZBYPASSCTL_REG                                     (0x00003C60)
//Duplicate of CH2CCC_CR_SRZBYPASSCTL_REG

#define CH6CCC_CR_SRZDRVENBYPASSCTL_REG                                (0x00003C64)
//Duplicate of CH2CCC_CR_SRZDRVENBYPASSCTL_REG

#define CH6CCC_CR_DFXCCMON_REG                                         (0x00003C68)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define CH6CCC_CR_DCCCTL4_REG                                          (0x00003C6C)
//Duplicate of CH2CCC_CR_DCCCTL4_REG

#define CH6CCC_CR_DCCCTL17_REG                                         (0x00003C70)
//Duplicate of CH2CCC_CR_DCCCTL17_REG

#define CH6CCC_CR_DCCCTL18_REG                                         (0x00003C74)
//Duplicate of CH2CCC_CR_DCCCTL18_REG

#define CH6CCC_CR_DCCCTL19_REG                                         (0x00003C78)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define CH6CCC_CR_CLKALIGNSTATUS_REG                                   (0x00003C7C)
//Duplicate of CH2CCC_CR_CLKALIGNSTATUS_REG

#define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_REG                           (0x00003C80)

  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicup_vrefcode_OFF ( 0)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicup_vrefcode_WID ( 8)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicup_vrefcode_MSK (0x000000FF)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicup_vrefcode_MIN (0)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicup_vrefcode_MAX (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicup_vrefcode_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicup_vrefcode_HSH (0x08003C80)

  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicdn_vrefcode_OFF ( 8)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicdn_vrefcode_WID ( 8)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicdn_vrefcode_MSK (0x0000FF00)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicdn_vrefcode_MIN (0)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicdn_vrefcode_MAX (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicdn_vrefcode_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_vttpanicdn_vrefcode_HSH (0x08103C80)

  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_cmddn200_vrefcode_OFF (16)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_cmddn200_vrefcode_WID ( 8)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_cmddn200_vrefcode_MSK (0x00FF0000)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_cmddn200_vrefcode_MIN (0)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_cmddn200_vrefcode_MAX (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_cmddn200_vrefcode_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_cmddn200_vrefcode_HSH (0x08203C80)

  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_rloaddqs_vrefcode_OFF (24)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_rloaddqs_vrefcode_WID ( 8)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_rloaddqs_vrefcode_MSK (0xFF000000)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_rloaddqs_vrefcode_MIN (0)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_rloaddqs_vrefcode_MAX (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_rloaddqs_vrefcode_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_pnccomp_rloaddqs_vrefcode_HSH (0x08303C80)

#define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_REG                          (0x00003C84)

  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Vsshiff_rx_OFF             ( 0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Vsshiff_rx_WID             ( 6)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Vsshiff_rx_MSK             (0x0000003F)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Vsshiff_rx_MIN             (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Vsshiff_rx_MAX             (63) // 0x0000003F
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Vsshiff_rx_DEF             (0x00000020)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Vsshiff_rx_HSH             (0x06003C84)

  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkn_OFF            ( 6)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkn_WID            ( 7)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkn_MSK            (0x00001FC0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkn_MIN            (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkn_MAX            (127) // 0x0000007F
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkn_DEF            (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkn_HSH            (0x070C3C84)

  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkp_OFF            (13)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkp_WID            ( 7)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkp_MSK            (0x000FE000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkp_MIN            (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkp_MAX            (127) // 0x0000007F
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkp_DEF            (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqs_fwdclkp_HSH            (0x071A3C84)

  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dll_coderead_OFF           (20)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dll_coderead_WID           ( 7)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dll_coderead_MSK           (0x07F00000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dll_coderead_MIN           (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dll_coderead_MAX           (127) // 0x0000007F
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dll_coderead_DEF           (0x00000040)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dll_coderead_HSH           (0x07283C84)

  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsntargetnui_OFF          (27)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsntargetnui_WID          ( 3)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsntargetnui_MSK          (0x38000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsntargetnui_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsntargetnui_MAX          (7) // 0x00000007
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsntargetnui_DEF          (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsntargetnui_HSH          (0x03363C84)

  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsnoffsetnui_OFF          (30)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsnoffsetnui_WID          ( 2)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsnoffsetnui_MSK          (0xC0000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsnoffsetnui_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsnoffsetnui_MAX          (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsnoffsetnui_DEF          (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_Dqsnoffsetnui_HSH          (0x023C3C84)

#define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_REG                        (0x00003C88)

  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn0_OFF          ( 0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn0_WID          ( 8)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn0_MSK          (0x000000FF)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn0_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn0_MAX          (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn0_DEF          (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn0_HSH          (0x08003C88)

  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn1_OFF          ( 8)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn1_WID          ( 8)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn1_MSK          (0x0000FF00)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn1_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn1_MAX          (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn1_DEF          (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttdn1_HSH          (0x08103C88)

  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup0_OFF          (16)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup0_WID          ( 8)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup0_MSK          (0x00FF0000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup0_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup0_MAX          (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup0_DEF          (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup0_HSH          (0x08203C88)

  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup1_OFF          (24)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup1_WID          ( 8)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup1_MSK          (0xFF000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup1_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup1_MAX          (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup1_DEF          (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_panicvttup1_HSH          (0x08303C88)

#define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG                       (0x00003C8C)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codepi_OFF          ( 0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codepi_WID          ( 6)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codepi_MSK          (0x0000003F)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codepi_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codepi_MAX          (63) // 0x0000003F
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codepi_DEF          (0x00000020)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codepi_HSH          (0x06003C8C)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codewl_OFF          ( 6)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codewl_WID          ( 6)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codewl_MSK          (0x00000FC0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codewl_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codewl_MAX          (63) // 0x0000003F
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codewl_DEF          (0x00000020)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_codewl_HSH          (0x060C3C8C)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_bwsel_OFF           (12)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_bwsel_WID           ( 6)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_bwsel_MSK           (0x0003F000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_bwsel_MIN           (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_bwsel_MAX           (63) // 0x0000003F
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_bwsel_DEF           (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_bwsel_HSH           (0x06183C8C)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_OFF        (18)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_WID        ( 8)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_MSK        (0x03FC0000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_MIN        (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_MAX        (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_DEF        (0x00000080)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dll_vdlllock_HSH        (0x08243C8C)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_rloaddqs_OFF            (18)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_rloaddqs_WID            ( 6)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_rloaddqs_MSK            (0x00FC0000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_rloaddqs_MIN            (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_rloaddqs_MAX            (63) // 0x0000003F
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_rloaddqs_DEF            (0x00000020)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_rloaddqs_HSH            (0x06243C8C)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_cben_OFF                (24)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_cben_WID                ( 2)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_cben_MSK                (0x03000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_cben_MIN                (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_cben_MAX                (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_cben_DEF                (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_cben_HSH                (0x02303C8C)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqspoffsetnui_OFF       (26)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqspoffsetnui_WID       ( 2)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqspoffsetnui_MSK       (0x0C000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqspoffsetnui_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqspoffsetnui_MAX       (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqspoffsetnui_DEF       (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqspoffsetnui_HSH       (0x02343C8C)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqsptargetnui_OFF       (28)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqsptargetnui_WID       ( 3)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqsptargetnui_MSK       (0x70000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqsptargetnui_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqsptargetnui_MAX       (7) // 0x00000007
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqsptargetnui_DEF       (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Dqsptargetnui_HSH       (0x03383C8C)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Spare_OFF               (31)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Spare_WID               ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Spare_MSK               (0x80000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Spare_MIN               (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Spare_MAX               (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Spare_DEF               (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_Spare_HSH               (0x013E3C8C)

#define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_REG                    (0x00003C90)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwdivider_OFF      ( 0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwdivider_WID      ( 2)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwdivider_MSK      (0x00000003)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwdivider_MIN      (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwdivider_MAX      (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwdivider_DEF      (0x00000002)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwdivider_HSH      (0x02003C90)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_lobwdivider_OFF      ( 2)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_lobwdivider_WID      ( 2)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_lobwdivider_MSK      (0x0000000C)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_lobwdivider_MIN      (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_lobwdivider_MAX      (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_lobwdivider_DEF      (0x00000002)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_lobwdivider_HSH      (0x02043C90)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_sampledivider_OFF    ( 4)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_sampledivider_WID    ( 2)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_sampledivider_MSK    (0x00000030)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_sampledivider_MIN    (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_sampledivider_MAX    (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_sampledivider_DEF    (0x00000003)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_sampledivider_HSH    (0x02083C90)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_openloop_OFF         ( 6)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_openloop_WID         ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_openloop_MSK         (0x00000040)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_openloop_MIN         (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_openloop_MAX         (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_openloop_DEF         (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_openloop_HSH         (0x010C3C90)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_slowbwerror_OFF      ( 7)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_slowbwerror_WID      ( 2)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_slowbwerror_MSK      (0x00000180)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_slowbwerror_MIN      (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_slowbwerror_MAX      (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_slowbwerror_DEF      (0x00000001)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_slowbwerror_HSH      (0x020E3C90)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwenable_OFF       ( 9)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwenable_WID       ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwenable_MSK       (0x00000200)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwenable_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwenable_MAX       (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwenable_DEF       (0x00000001)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_hibwenable_HSH       (0x01123C90)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtslope_OFF          (10)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtslope_WID          ( 5)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtslope_MSK          (0x00007C00)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtslope_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtslope_MAX          (31) // 0x0000001F
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtslope_DEF          (0x00000010)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtslope_HSH          (0x05143C90)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtoffset_OFF         (15)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtoffset_WID         ( 5)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtoffset_MSK         (0x000F8000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtoffset_MIN         (-16)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtoffset_MAX         (15) // 0x0000000F
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtoffset_DEF         (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_vtoffset_HSH         (0x851E3C90)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_selcode_OFF          (20)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_selcode_WID          ( 4)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_selcode_MSK          (0x00F00000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_selcode_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_selcode_MAX          (15) // 0x0000000F
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_selcode_DEF          (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_selcode_HSH          (0x04283C90)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_outputcode_OFF       (24)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_outputcode_WID       ( 8)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_outputcode_MSK       (0xFF000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_outputcode_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_outputcode_MAX       (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_outputcode_DEF       (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_outputcode_HSH       (0x08303C90)

#define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_REG                        (0x00003C94)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca0vref_OFF              ( 0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca0vref_WID              ( 9)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca0vref_MSK              (0x000001FF)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca0vref_MIN              (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca0vref_MAX              (511) // 0x000001FF
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca0vref_DEF              (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca0vref_HSH              (0x09003C94)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca1vref_OFF              ( 9)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca1vref_WID              ( 9)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca1vref_MSK              (0x0003FE00)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca1vref_MIN              (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca1vref_MAX              (511) // 0x000001FF
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca1vref_DEF              (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca1vref_HSH              (0x09123C94)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_qxcount_OFF              (18)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_qxcount_WID              ( 6)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_qxcount_MSK              (0x00FC0000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_qxcount_MIN              (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_qxcount_MAX              (63) // 0x0000003F
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_qxcount_DEF              (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_qxcount_HSH              (0x06243C94)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_Reserved0_OFF            (24)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_Reserved0_WID            ( 3)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_Reserved0_MSK            (0x07000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_Reserved0_MIN            (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_Reserved0_MAX            (7) // 0x00000007
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_Reserved0_DEF            (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_Reserved0_HSH            (0x03303C94)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca0vref_OFF            (27)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca0vref_WID            ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca0vref_MSK            (0x08000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca0vref_MIN            (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca0vref_MAX            (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca0vref_DEF            (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca0vref_HSH            (0x01363C94)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca1vref_OFF            (28)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca1vref_WID            ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca1vref_MSK            (0x10000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca1vref_MIN            (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca1vref_MAX            (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca1vref_DEF            (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_enca1vref_HSH            (0x01383C94)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvopenloopen_OFF       (29)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvopenloopen_WID       ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvopenloopen_MSK       (0x20000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvopenloopen_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvopenloopen_MAX       (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvopenloopen_DEF       (0x00000001)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvopenloopen_HSH       (0x013A3C94)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvlockcodectl_OFF      (30)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvlockcodectl_WID      ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvlockcodectl_MSK      (0x40000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvlockcodectl_MIN      (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvlockcodectl_MAX      (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvlockcodectl_DEF      (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvlockcodectl_HSH      (0x013C3C94)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvfastbwdisable_OFF    (31)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvfastbwdisable_WID    ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvfastbwdisable_MSK    (0x80000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvfastbwdisable_MIN    (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvfastbwdisable_MAX    (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvfastbwdisable_DEF    (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_sagvfastbwdisable_HSH    (0x013E3C94)

#define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_REG                        (0x00003C98)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca0vref_OFF              ( 0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca0vref_WID              ( 9)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca0vref_MSK              (0x000001FF)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca0vref_MIN              (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca0vref_MAX              (511) // 0x000001FF
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca0vref_DEF              (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca0vref_HSH              (0x09003C98)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca1vref_OFF              ( 9)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca1vref_WID              ( 9)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca1vref_MSK              (0x0003FE00)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca1vref_MIN              (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca1vref_MAX              (511) // 0x000001FF
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca1vref_DEF              (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_ca1vref_HSH              (0x09123C98)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved02_OFF           (18)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved02_WID           ( 9)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved02_MSK           (0x07FC0000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved02_MIN           (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved02_MAX           (511) // 0x000001FF
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved02_DEF           (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved02_HSH           (0x09243C98)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca0vref_OFF            (27)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca0vref_WID            ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca0vref_MSK            (0x08000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca0vref_MIN            (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca0vref_MAX            (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca0vref_DEF            (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca0vref_HSH            (0x01363C98)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca1vref_OFF            (28)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca1vref_WID            ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca1vref_MSK            (0x10000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca1vref_MIN            (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca1vref_MAX            (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca1vref_DEF            (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_enca1vref_HSH            (0x01383C98)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved11_OFF           (29)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved11_WID           ( 3)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved11_MSK           (0xE0000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved11_MIN           (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved11_MAX           (7) // 0x00000007
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved11_DEF           (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_Reserved11_HSH           (0x033A3C98)

#define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_REG                    (0x00003C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_hiztimerctrl_OFF     ( 0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_hiztimerctrl_WID     ( 2)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_hiztimerctrl_MSK     (0x00000003)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_hiztimerctrl_MIN     (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_hiztimerctrl_MAX     (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_hiztimerctrl_DEF     (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_hiztimerctrl_HSH     (0x02003C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca00slowbw_OFF       ( 2)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca00slowbw_WID       ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca00slowbw_MSK       (0x00000004)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca00slowbw_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca00slowbw_MAX       (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca00slowbw_DEF       (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca00slowbw_HSH       (0x01043C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca01slowbw_OFF       ( 3)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca01slowbw_WID       ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca01slowbw_MSK       (0x00000008)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca01slowbw_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca01slowbw_MAX       (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca01slowbw_DEF       (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca01slowbw_HSH       (0x01063C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_lockovrd_OFF         ( 4)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_lockovrd_WID         ( 2)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_lockovrd_MSK         (0x00000030)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_lockovrd_MIN         (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_lockovrd_MAX         (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_lockovrd_DEF         (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_lockovrd_HSH         (0x02083C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_locktimer_OFF        ( 6)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_locktimer_WID        ( 3)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_locktimer_MSK        (0x000001C0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_locktimer_MIN        (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_locktimer_MAX        (7) // 0x00000007
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_locktimer_DEF        (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_locktimer_HSH        (0x030C3C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtslopesagv_OFF      ( 9)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtslopesagv_WID      ( 5)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtslopesagv_MSK      (0x00003E00)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtslopesagv_MIN      (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtslopesagv_MAX      (31) // 0x0000001F
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtslopesagv_DEF      (0x00000010)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtslopesagv_HSH      (0x05123C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtoffsetsagv_OFF     (14)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtoffsetsagv_WID     ( 5)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtoffsetsagv_MSK     (0x0007C000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtoffsetsagv_MIN     (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtoffsetsagv_MAX     (31) // 0x0000001F
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtoffsetsagv_DEF     (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtoffsetsagv_HSH     (0x051C3C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_gateicindvfs_OFF     (19)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_gateicindvfs_WID     ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_gateicindvfs_MSK     (0x00080000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_gateicindvfs_MIN     (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_gateicindvfs_MAX     (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_gateicindvfs_DEF     (0x00000001)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_gateicindvfs_HSH     (0x01263C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca10slowbw_OFF       (20)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca10slowbw_WID       ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca10slowbw_MSK       (0x00100000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca10slowbw_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca10slowbw_MAX       (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca10slowbw_DEF       (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca10slowbw_HSH       (0x01283C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca11slowbw_OFF       (21)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca11slowbw_WID       ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca11slowbw_MSK       (0x00200000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca11slowbw_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca11slowbw_MAX       (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca11slowbw_DEF       (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca11slowbw_HSH       (0x012A3C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_sagvvtctl_OFF        (22)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_sagvvtctl_WID        ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_sagvvtctl_MSK        (0x00400000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_sagvvtctl_MIN        (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_sagvvtctl_MAX        (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_sagvvtctl_DEF        (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_sagvvtctl_HSH        (0x012C3C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrden_OFF     (23)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrden_WID     ( 1)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrden_MSK     (0x00800000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrden_MIN     (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrden_MAX     (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrden_DEF     (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrden_HSH     (0x012E3C9C)

  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrd_OFF       (24)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrd_WID       ( 8)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrd_MSK       (0xFF000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrd_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrd_MAX       (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrd_DEF       (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_vtcompovrd_HSH       (0x08303C9C)

#define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_REG                         (0x00003CA0)

  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_dqvrefcode_OFF ( 0)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_dqvrefcode_WID ( 8)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_dqvrefcode_MSK (0x000000FF)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_dqvrefcode_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_dqvrefcode_MAX (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_dqvrefcode_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_dqvrefcode_HSH (0x08003CA0)

  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_cmdvrefcode_OFF ( 8)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_cmdvrefcode_WID ( 8)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_cmdvrefcode_MSK (0x0000FF00)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_cmdvrefcode_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_cmdvrefcode_MAX (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_cmdvrefcode_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_cmdvrefcode_HSH (0x08103CA0)

  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_rxvrefcode_OFF (16)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_rxvrefcode_WID ( 8)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_rxvrefcode_MSK (0x00FF0000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_rxvrefcode_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_rxvrefcode_MAX (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_rxvrefcode_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_rxvrefcode_HSH (0x08203CA0)

  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_leakvrefcode_OFF (24)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_leakvrefcode_WID ( 8)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_leakvrefcode_MSK (0xFF000000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_leakvrefcode_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_leakvrefcode_MAX (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_leakvrefcode_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_vsshicompcr_leakvrefcode_HSH (0x08303CA0)

#define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_REG                         (0x00003CA4)

  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_clkvrefcode_OFF ( 0)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_clkvrefcode_WID ( 8)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_clkvrefcode_MSK (0x000000FF)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_clkvrefcode_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_clkvrefcode_MAX (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_clkvrefcode_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_clkvrefcode_HSH (0x08003CA4)

  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_ctlvrefcode_OFF ( 8)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_ctlvrefcode_WID ( 8)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_ctlvrefcode_MSK (0x0000FF00)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_ctlvrefcode_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_ctlvrefcode_MAX (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_ctlvrefcode_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_vsshicompcr_ctlvrefcode_HSH (0x08103CA4)

  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_bwsel_lo_threshold_OFF    (16)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_bwsel_lo_threshold_WID    ( 6)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_bwsel_lo_threshold_MSK    (0x003F0000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_bwsel_lo_threshold_MIN    (0)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_bwsel_lo_threshold_MAX    (63) // 0x0000003F
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_bwsel_lo_threshold_DEF    (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_bwsel_lo_threshold_HSH    (0x06203CA4)

  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_Spare_OFF                 (22)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_Spare_WID                 (10)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_Spare_MSK                 (0xFFC00000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_Spare_MIN                 (0)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_Spare_MAX                 (1023) // 0x000003FF
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_Spare_DEF                 (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_Spare_HSH                 (0x0A2C3CA4)

#define DCSCTL0_REG                                                    (0x00003CA8)

  #define DCSCTL0_dcs_start_dl_val_OFF                                 ( 0)
  #define DCSCTL0_dcs_start_dl_val_WID                                 (10)
  #define DCSCTL0_dcs_start_dl_val_MSK                                 (0x000003FF)
  #define DCSCTL0_dcs_start_dl_val_MIN                                 (0)
  #define DCSCTL0_dcs_start_dl_val_MAX                                 (1023) // 0x000003FF
  #define DCSCTL0_dcs_start_dl_val_DEF                                 (0x00000000)
  #define DCSCTL0_dcs_start_dl_val_HSH                                 (0x0A003CA8)

  #define DCSCTL0_dcs_calibrate_en_OFF                                 (10)
  #define DCSCTL0_dcs_calibrate_en_WID                                 ( 1)
  #define DCSCTL0_dcs_calibrate_en_MSK                                 (0x00000400)
  #define DCSCTL0_dcs_calibrate_en_MIN                                 (0)
  #define DCSCTL0_dcs_calibrate_en_MAX                                 (1) // 0x00000001
  #define DCSCTL0_dcs_calibrate_en_DEF                                 (0x00000000)
  #define DCSCTL0_dcs_calibrate_en_HSH                                 (0x01143CA8)

  #define DCSCTL0_dcs_jitter_meas_en_OFF                               (11)
  #define DCSCTL0_dcs_jitter_meas_en_WID                               ( 1)
  #define DCSCTL0_dcs_jitter_meas_en_MSK                               (0x00000800)
  #define DCSCTL0_dcs_jitter_meas_en_MIN                               (0)
  #define DCSCTL0_dcs_jitter_meas_en_MAX                               (1) // 0x00000001
  #define DCSCTL0_dcs_jitter_meas_en_DEF                               (0x00000000)
  #define DCSCTL0_dcs_jitter_meas_en_HSH                               (0x01163CA8)

  #define DCSCTL0_dcs_thresh_OFF                                       (12)
  #define DCSCTL0_dcs_thresh_WID                                       ( 2)
  #define DCSCTL0_dcs_thresh_MSK                                       (0x00003000)
  #define DCSCTL0_dcs_thresh_MIN                                       (0)
  #define DCSCTL0_dcs_thresh_MAX                                       (3) // 0x00000003
  #define DCSCTL0_dcs_thresh_DEF                                       (0x00000000)
  #define DCSCTL0_dcs_thresh_HSH                                       (0x02183CA8)

  #define DCSCTL0_dcs_samples_OFF                                      (14)
  #define DCSCTL0_dcs_samples_WID                                      ( 2)
  #define DCSCTL0_dcs_samples_MSK                                      (0x0000C000)
  #define DCSCTL0_dcs_samples_MIN                                      (0)
  #define DCSCTL0_dcs_samples_MAX                                      (3) // 0x00000003
  #define DCSCTL0_dcs_samples_DEF                                      (0x00000000)
  #define DCSCTL0_dcs_samples_HSH                                      (0x021C3CA8)

  #define DCSCTL0_dcs_cb_tune_OFF                                      (16)
  #define DCSCTL0_dcs_cb_tune_WID                                      ( 2)
  #define DCSCTL0_dcs_cb_tune_MSK                                      (0x00030000)
  #define DCSCTL0_dcs_cb_tune_MIN                                      (0)
  #define DCSCTL0_dcs_cb_tune_MAX                                      (3) // 0x00000003
  #define DCSCTL0_dcs_cb_tune_DEF                                      (0x00000000)
  #define DCSCTL0_dcs_cb_tune_HSH                                      (0x02203CA8)

  #define DCSCTL0_dcs_timer_cfg_OFF                                    (18)
  #define DCSCTL0_dcs_timer_cfg_WID                                    ( 2)
  #define DCSCTL0_dcs_timer_cfg_MSK                                    (0x000C0000)
  #define DCSCTL0_dcs_timer_cfg_MIN                                    (0)
  #define DCSCTL0_dcs_timer_cfg_MAX                                    (3) // 0x00000003
  #define DCSCTL0_dcs_timer_cfg_DEF                                    (0x00000000)
  #define DCSCTL0_dcs_timer_cfg_HSH                                    (0x02243CA8)

  #define DCSCTL0_dcs_halt_mode_OFF                                    (20)
  #define DCSCTL0_dcs_halt_mode_WID                                    ( 2)
  #define DCSCTL0_dcs_halt_mode_MSK                                    (0x00300000)
  #define DCSCTL0_dcs_halt_mode_MIN                                    (0)
  #define DCSCTL0_dcs_halt_mode_MAX                                    (3) // 0x00000003
  #define DCSCTL0_dcs_halt_mode_DEF                                    (0x00000000)
  #define DCSCTL0_dcs_halt_mode_HSH                                    (0x02283CA8)

  #define DCSCTL0_dcs_recycle_mode_OFF                                 (22)
  #define DCSCTL0_dcs_recycle_mode_WID                                 ( 2)
  #define DCSCTL0_dcs_recycle_mode_MSK                                 (0x00C00000)
  #define DCSCTL0_dcs_recycle_mode_MIN                                 (0)
  #define DCSCTL0_dcs_recycle_mode_MAX                                 (3) // 0x00000003
  #define DCSCTL0_dcs_recycle_mode_DEF                                 (0x00000000)
  #define DCSCTL0_dcs_recycle_mode_HSH                                 (0x022C3CA8)

  #define DCSCTL0_dcs_adaptive_filt_en_OFF                             (24)
  #define DCSCTL0_dcs_adaptive_filt_en_WID                             ( 1)
  #define DCSCTL0_dcs_adaptive_filt_en_MSK                             (0x01000000)
  #define DCSCTL0_dcs_adaptive_filt_en_MIN                             (0)
  #define DCSCTL0_dcs_adaptive_filt_en_MAX                             (1) // 0x00000001
  #define DCSCTL0_dcs_adaptive_filt_en_DEF                             (0x00000000)
  #define DCSCTL0_dcs_adaptive_filt_en_HSH                             (0x01303CA8)

  #define DCSCTL0_dcs_clkdivsel_OFF                                    (25)
  #define DCSCTL0_dcs_clkdivsel_WID                                    ( 1)
  #define DCSCTL0_dcs_clkdivsel_MSK                                    (0x02000000)
  #define DCSCTL0_dcs_clkdivsel_MIN                                    (0)
  #define DCSCTL0_dcs_clkdivsel_MAX                                    (1) // 0x00000001
  #define DCSCTL0_dcs_clkdivsel_DEF                                    (0x00000000)
  #define DCSCTL0_dcs_clkdivsel_HSH                                    (0x01323CA8)

  #define DCSCTL0_dcs_num_clk_OFF                                      (26)
  #define DCSCTL0_dcs_num_clk_WID                                      ( 2)
  #define DCSCTL0_dcs_num_clk_MSK                                      (0x0C000000)
  #define DCSCTL0_dcs_num_clk_MIN                                      (0)
  #define DCSCTL0_dcs_num_clk_MAX                                      (3) // 0x00000003
  #define DCSCTL0_dcs_num_clk_DEF                                      (0x00000000)
  #define DCSCTL0_dcs_num_clk_HSH                                      (0x02343CA8)

  #define DCSCTL0_Reserved3_OFF                                        (28)
  #define DCSCTL0_Reserved3_WID                                        ( 4)
  #define DCSCTL0_Reserved3_MSK                                        (0xF0000000)
  #define DCSCTL0_Reserved3_MIN                                        (0)
  #define DCSCTL0_Reserved3_MAX                                        (15) // 0x0000000F
  #define DCSCTL0_Reserved3_DEF                                        (0x00000000)
  #define DCSCTL0_Reserved3_HSH                                        (0x04383CA8)

#define DCSCTL1_REG                                                    (0x00003CAC)

  #define DCSCTL1_dcabypass_OFF                                        ( 0)
  #define DCSCTL1_dcabypass_WID                                        ( 6)
  #define DCSCTL1_dcabypass_MSK                                        (0x0000003F)
  #define DCSCTL1_dcabypass_MIN                                        (0)
  #define DCSCTL1_dcabypass_MAX                                        (63) // 0x0000003F
  #define DCSCTL1_dcabypass_DEF                                        (0x00000000)
  #define DCSCTL1_dcabypass_HSH                                        (0x06003CAC)

  #define DCSCTL1_dcs_cnt_load_OFF                                     ( 6)
  #define DCSCTL1_dcs_cnt_load_WID                                     ( 6)
  #define DCSCTL1_dcs_cnt_load_MSK                                     (0x00000FC0)
  #define DCSCTL1_dcs_cnt_load_MIN                                     (0)
  #define DCSCTL1_dcs_cnt_load_MAX                                     (63) // 0x0000003F
  #define DCSCTL1_dcs_cnt_load_DEF                                     (0x00000000)
  #define DCSCTL1_dcs_cnt_load_HSH                                     (0x060C3CAC)

  #define DCSCTL1_dcs_dcastaticcode_OFF                                (12)
  #define DCSCTL1_dcs_dcastaticcode_WID                                ( 8)
  #define DCSCTL1_dcs_dcastaticcode_MSK                                (0x000FF000)
  #define DCSCTL1_dcs_dcastaticcode_MIN                                (0)
  #define DCSCTL1_dcs_dcastaticcode_MAX                                (255) // 0x000000FF
  #define DCSCTL1_dcs_dcastaticcode_DEF                                (0x00000000)
  #define DCSCTL1_dcs_dcastaticcode_HSH                                (0x08183CAC)

  #define DCSCTL1_dcs_clk_sel_ovrsel_OFF                               (20)
  #define DCSCTL1_dcs_clk_sel_ovrsel_WID                               ( 1)
  #define DCSCTL1_dcs_clk_sel_ovrsel_MSK                               (0x00100000)
  #define DCSCTL1_dcs_clk_sel_ovrsel_MIN                               (0)
  #define DCSCTL1_dcs_clk_sel_ovrsel_MAX                               (1) // 0x00000001
  #define DCSCTL1_dcs_clk_sel_ovrsel_DEF                               (0x00000000)
  #define DCSCTL1_dcs_clk_sel_ovrsel_HSH                               (0x01283CAC)

  #define DCSCTL1_dcs_clk_sel_ovr_OFF                                  (21)
  #define DCSCTL1_dcs_clk_sel_ovr_WID                                  ( 2)
  #define DCSCTL1_dcs_clk_sel_ovr_MSK                                  (0x00600000)
  #define DCSCTL1_dcs_clk_sel_ovr_MIN                                  (0)
  #define DCSCTL1_dcs_clk_sel_ovr_MAX                                  (3) // 0x00000003
  #define DCSCTL1_dcs_clk_sel_ovr_DEF                                  (0x00000000)
  #define DCSCTL1_dcs_clk_sel_ovr_HSH                                  (0x022A3CAC)

  #define DCSCTL1_dcs_mrcinitloop_en_OFF                               (23)
  #define DCSCTL1_dcs_mrcinitloop_en_WID                               ( 1)
  #define DCSCTL1_dcs_mrcinitloop_en_MSK                               (0x00800000)
  #define DCSCTL1_dcs_mrcinitloop_en_MIN                               (0)
  #define DCSCTL1_dcs_mrcinitloop_en_MAX                               (1) // 0x00000001
  #define DCSCTL1_dcs_mrcinitloop_en_DEF                               (0x00000000)
  #define DCSCTL1_dcs_mrcinitloop_en_HSH                               (0x012E3CAC)

  #define DCSCTL1_dcs_exthalt_OFF                                      (24)
  #define DCSCTL1_dcs_exthalt_WID                                      ( 1)
  #define DCSCTL1_dcs_exthalt_MSK                                      (0x01000000)
  #define DCSCTL1_dcs_exthalt_MIN                                      (0)
  #define DCSCTL1_dcs_exthalt_MAX                                      (1) // 0x00000001
  #define DCSCTL1_dcs_exthalt_DEF                                      (0x00000000)
  #define DCSCTL1_dcs_exthalt_HSH                                      (0x01303CAC)

  #define DCSCTL1_dcs_dl_val_ovrsel_OFF                                (25)
  #define DCSCTL1_dcs_dl_val_ovrsel_WID                                ( 1)
  #define DCSCTL1_dcs_dl_val_ovrsel_MSK                                (0x02000000)
  #define DCSCTL1_dcs_dl_val_ovrsel_MIN                                (0)
  #define DCSCTL1_dcs_dl_val_ovrsel_MAX                                (1) // 0x00000001
  #define DCSCTL1_dcs_dl_val_ovrsel_DEF                                (0x00000000)
  #define DCSCTL1_dcs_dl_val_ovrsel_HSH                                (0x01323CAC)

  #define DCSCTL1_dca_fallxriseb_ovrsel_OFF                            (26)
  #define DCSCTL1_dca_fallxriseb_ovrsel_WID                            ( 1)
  #define DCSCTL1_dca_fallxriseb_ovrsel_MSK                            (0x04000000)
  #define DCSCTL1_dca_fallxriseb_ovrsel_MIN                            (0)
  #define DCSCTL1_dca_fallxriseb_ovrsel_MAX                            (1) // 0x00000001
  #define DCSCTL1_dca_fallxriseb_ovrsel_DEF                            (0x00000000)
  #define DCSCTL1_dca_fallxriseb_ovrsel_HSH                            (0x01343CAC)

  #define DCSCTL1_dca_fallxriseb_ovr_OFF                               (27)
  #define DCSCTL1_dca_fallxriseb_ovr_WID                               ( 1)
  #define DCSCTL1_dca_fallxriseb_ovr_MSK                               (0x08000000)
  #define DCSCTL1_dca_fallxriseb_ovr_MIN                               (0)
  #define DCSCTL1_dca_fallxriseb_ovr_MAX                               (1) // 0x00000001
  #define DCSCTL1_dca_fallxriseb_ovr_DEF                               (0x00000000)
  #define DCSCTL1_dca_fallxriseb_ovr_HSH                               (0x01363CAC)

  #define DCSCTL1_Reserved4_OFF                                        (28)
  #define DCSCTL1_Reserved4_WID                                        ( 3)
  #define DCSCTL1_Reserved4_MSK                                        (0x70000000)
  #define DCSCTL1_Reserved4_MIN                                        (0)
  #define DCSCTL1_Reserved4_MAX                                        (7) // 0x00000007
  #define DCSCTL1_Reserved4_DEF                                        (0x00000000)
  #define DCSCTL1_Reserved4_HSH                                        (0x03383CAC)

  #define DCSCTL1_dca_hvm_ovrsel_OFF                                   (31)
  #define DCSCTL1_dca_hvm_ovrsel_WID                                   ( 1)
  #define DCSCTL1_dca_hvm_ovrsel_MSK                                   (0x80000000)
  #define DCSCTL1_dca_hvm_ovrsel_MIN                                   (0)
  #define DCSCTL1_dca_hvm_ovrsel_MAX                                   (1) // 0x00000001
  #define DCSCTL1_dca_hvm_ovrsel_DEF                                   (0x00000000)
  #define DCSCTL1_dca_hvm_ovrsel_HSH                                   (0x013E3CAC)

#define DCSCTL2_REG                                                    (0x00003CB0)

  #define DCSCTL2_dcacode_ovrsel_OFF                                   ( 0)
  #define DCSCTL2_dcacode_ovrsel_WID                                   ( 1)
  #define DCSCTL2_dcacode_ovrsel_MSK                                   (0x00000001)
  #define DCSCTL2_dcacode_ovrsel_MIN                                   (0)
  #define DCSCTL2_dcacode_ovrsel_MAX                                   (1) // 0x00000001
  #define DCSCTL2_dcacode_ovrsel_DEF                                   (0x00000000)
  #define DCSCTL2_dcacode_ovrsel_HSH                                   (0x01003CB0)

  #define DCSCTL2_dcacode_ovr_OFF                                      ( 1)
  #define DCSCTL2_dcacode_ovr_WID                                      ( 8)
  #define DCSCTL2_dcacode_ovr_MSK                                      (0x000001FE)
  #define DCSCTL2_dcacode_ovr_MIN                                      (0)
  #define DCSCTL2_dcacode_ovr_MAX                                      (255) // 0x000000FF
  #define DCSCTL2_dcacode_ovr_DEF                                      (0x00000000)
  #define DCSCTL2_dcacode_ovr_HSH                                      (0x08023CB0)

  #define DCSCTL2_Reserved5_OFF                                        ( 9)
  #define DCSCTL2_Reserved5_WID                                        (23)
  #define DCSCTL2_Reserved5_MSK                                        (0xFFFFFE00)
  #define DCSCTL2_Reserved5_MIN                                        (0)
  #define DCSCTL2_Reserved5_MAX                                        (8388607) // 0x007FFFFF
  #define DCSCTL2_Reserved5_DEF                                        (0x00000000)
  #define DCSCTL2_Reserved5_HSH                                        (0x17123CB0)

#define DCSCTL3_REG                                                    (0x00003CB4)

  #define DCSCTL3_dcddlycben_OFF                                       ( 0)
  #define DCSCTL3_dcddlycben_WID                                       ( 1)
  #define DCSCTL3_dcddlycben_MSK                                       (0x00000001)
  #define DCSCTL3_dcddlycben_MIN                                       (0)
  #define DCSCTL3_dcddlycben_MAX                                       (1) // 0x00000001
  #define DCSCTL3_dcddlycben_DEF                                       (0x00000000)
  #define DCSCTL3_dcddlycben_HSH                                       (0x01003CB4)

  #define DCSCTL3_parkvalue_OFF                                        ( 1)
  #define DCSCTL3_parkvalue_WID                                        ( 1)
  #define DCSCTL3_parkvalue_MSK                                        (0x00000002)
  #define DCSCTL3_parkvalue_MIN                                        (0)
  #define DCSCTL3_parkvalue_MAX                                        (1) // 0x00000001
  #define DCSCTL3_parkvalue_DEF                                        (0x00000000)
  #define DCSCTL3_parkvalue_HSH                                        (0x01023CB4)

  #define DCSCTL3_txdccrangesel_OFF                                    ( 2)
  #define DCSCTL3_txdccrangesel_WID                                    ( 2)
  #define DCSCTL3_txdccrangesel_MSK                                    (0x0000000C)
  #define DCSCTL3_txdccrangesel_MIN                                    (0)
  #define DCSCTL3_txdccrangesel_MAX                                    (3) // 0x00000003
  #define DCSCTL3_txdccrangesel_DEF                                    (0x00000000)
  #define DCSCTL3_txdccrangesel_HSH                                    (0x02043CB4)

  #define DCSCTL3_serializerbypen_OFF                                  ( 4)
  #define DCSCTL3_serializerbypen_WID                                  ( 1)
  #define DCSCTL3_serializerbypen_MSK                                  (0x00000010)
  #define DCSCTL3_serializerbypen_MIN                                  (0)
  #define DCSCTL3_serializerbypen_MAX                                  (1) // 0x00000001
  #define DCSCTL3_serializerbypen_DEF                                  (0x00000000)
  #define DCSCTL3_serializerbypen_HSH                                  (0x01083CB4)

  #define DCSCTL3_enperiodicdcc_OFF                                    ( 5)
  #define DCSCTL3_enperiodicdcc_WID                                    ( 1)
  #define DCSCTL3_enperiodicdcc_MSK                                    (0x00000020)
  #define DCSCTL3_enperiodicdcc_MIN                                    (0)
  #define DCSCTL3_enperiodicdcc_MAX                                    (1) // 0x00000001
  #define DCSCTL3_enperiodicdcc_DEF                                    (0x00000000)
  #define DCSCTL3_enperiodicdcc_HSH                                    (0x010A3CB4)

  #define DCSCTL3_enperiodicdccondvfs_OFF                              ( 6)
  #define DCSCTL3_enperiodicdccondvfs_WID                              ( 1)
  #define DCSCTL3_enperiodicdccondvfs_MSK                              (0x00000040)
  #define DCSCTL3_enperiodicdccondvfs_MIN                              (0)
  #define DCSCTL3_enperiodicdccondvfs_MAX                              (1) // 0x00000001
  #define DCSCTL3_enperiodicdccondvfs_DEF                              (0x00000000)
  #define DCSCTL3_enperiodicdccondvfs_HSH                              (0x010C3CB4)

  #define DCSCTL3_dcdjitmode_sel_OFF                                   ( 7)
  #define DCSCTL3_dcdjitmode_sel_WID                                   ( 1)
  #define DCSCTL3_dcdjitmode_sel_MSK                                   (0x00000080)
  #define DCSCTL3_dcdjitmode_sel_MIN                                   (0)
  #define DCSCTL3_dcdjitmode_sel_MAX                                   (1) // 0x00000001
  #define DCSCTL3_dcdjitmode_sel_DEF                                   (0x00000000)
  #define DCSCTL3_dcdjitmode_sel_HSH                                   (0x010E3CB4)

  #define DCSCTL3_dcdjitmode_OFF                                       ( 8)
  #define DCSCTL3_dcdjitmode_WID                                       ( 1)
  #define DCSCTL3_dcdjitmode_MSK                                       (0x00000100)
  #define DCSCTL3_dcdjitmode_MIN                                       (0)
  #define DCSCTL3_dcdjitmode_MAX                                       (1) // 0x00000001
  #define DCSCTL3_dcdjitmode_DEF                                       (0x00000000)
  #define DCSCTL3_dcdjitmode_HSH                                       (0x01103CB4)

  #define DCSCTL3_dcddlycode_sel_OFF                                   ( 9)
  #define DCSCTL3_dcddlycode_sel_WID                                   ( 1)
  #define DCSCTL3_dcddlycode_sel_MSK                                   (0x00000200)
  #define DCSCTL3_dcddlycode_sel_MIN                                   (0)
  #define DCSCTL3_dcddlycode_sel_MAX                                   (1) // 0x00000001
  #define DCSCTL3_dcddlycode_sel_DEF                                   (0x00000000)
  #define DCSCTL3_dcddlycode_sel_HSH                                   (0x01123CB4)

  #define DCSCTL3_dcddlycode_OFF                                       (10)
  #define DCSCTL3_dcddlycode_WID                                       (10)
  #define DCSCTL3_dcddlycode_MSK                                       (0x000FFC00)
  #define DCSCTL3_dcddlycode_MIN                                       (0)
  #define DCSCTL3_dcddlycode_MAX                                       (1023) // 0x000003FF
  #define DCSCTL3_dcddlycode_DEF                                       (0x00000000)
  #define DCSCTL3_dcddlycode_HSH                                       (0x0A143CB4)

  #define DCSCTL3_dcdclken_sel_OFF                                     (20)
  #define DCSCTL3_dcdclken_sel_WID                                     ( 1)
  #define DCSCTL3_dcdclken_sel_MSK                                     (0x00100000)
  #define DCSCTL3_dcdclken_sel_MIN                                     (0)
  #define DCSCTL3_dcdclken_sel_MAX                                     (1) // 0x00000001
  #define DCSCTL3_dcdclken_sel_DEF                                     (0x00000000)
  #define DCSCTL3_dcdclken_sel_HSH                                     (0x01283CB4)

  #define DCSCTL3_dcdclken_OFF                                         (21)
  #define DCSCTL3_dcdclken_WID                                         ( 1)
  #define DCSCTL3_dcdclken_MSK                                         (0x00200000)
  #define DCSCTL3_dcdclken_MIN                                         (0)
  #define DCSCTL3_dcdclken_MAX                                         (1) // 0x00000001
  #define DCSCTL3_dcdclken_DEF                                         (0x00000000)
  #define DCSCTL3_dcdclken_HSH                                         (0x012A3CB4)

  #define DCSCTL3_dcdcalen_sel_OFF                                     (22)
  #define DCSCTL3_dcdcalen_sel_WID                                     ( 1)
  #define DCSCTL3_dcdcalen_sel_MSK                                     (0x00400000)
  #define DCSCTL3_dcdcalen_sel_MIN                                     (0)
  #define DCSCTL3_dcdcalen_sel_MAX                                     (1) // 0x00000001
  #define DCSCTL3_dcdcalen_sel_DEF                                     (0x00000000)
  #define DCSCTL3_dcdcalen_sel_HSH                                     (0x012C3CB4)

  #define DCSCTL3_dcdcalen_OFF                                         (23)
  #define DCSCTL3_dcdcalen_WID                                         ( 1)
  #define DCSCTL3_dcdcalen_MSK                                         (0x00800000)
  #define DCSCTL3_dcdcalen_MIN                                         (0)
  #define DCSCTL3_dcdcalen_MAX                                         (1) // 0x00000001
  #define DCSCTL3_dcdcalen_DEF                                         (0x00000000)
  #define DCSCTL3_dcdcalen_HSH                                         (0x012E3CB4)

  #define DCSCTL3_bonus_OFF                                            (24)
  #define DCSCTL3_bonus_WID                                            ( 2)
  #define DCSCTL3_bonus_MSK                                            (0x03000000)
  #define DCSCTL3_bonus_MIN                                            (0)
  #define DCSCTL3_bonus_MAX                                            (3) // 0x00000003
  #define DCSCTL3_bonus_DEF                                            (0x00000000)
  #define DCSCTL3_bonus_HSH                                            (0x02303CB4)

  #define DCSCTL3_dcdclocksel_OFF                                      (26)
  #define DCSCTL3_dcdclocksel_WID                                      ( 1)
  #define DCSCTL3_dcdclocksel_MSK                                      (0x04000000)
  #define DCSCTL3_dcdclocksel_MIN                                      (0)
  #define DCSCTL3_dcdclocksel_MAX                                      (1) // 0x00000001
  #define DCSCTL3_dcdclocksel_DEF                                      (0x00000000)
  #define DCSCTL3_dcdclocksel_HSH                                      (0x01343CB4)

  #define DCSCTL3_dcsfsm_rst_b_OFF                                     (27)
  #define DCSCTL3_dcsfsm_rst_b_WID                                     ( 1)
  #define DCSCTL3_dcsfsm_rst_b_MSK                                     (0x08000000)
  #define DCSCTL3_dcsfsm_rst_b_MIN                                     (0)
  #define DCSCTL3_dcsfsm_rst_b_MAX                                     (1) // 0x00000001
  #define DCSCTL3_dcsfsm_rst_b_DEF                                     (0x00000000)
  #define DCSCTL3_dcsfsm_rst_b_HSH                                     (0x01363CB4)

  #define DCSCTL3_reserved6_OFF                                        (28)
  #define DCSCTL3_reserved6_WID                                        ( 4)
  #define DCSCTL3_reserved6_MSK                                        (0xF0000000)
  #define DCSCTL3_reserved6_MIN                                        (0)
  #define DCSCTL3_reserved6_MAX                                        (15) // 0x0000000F
  #define DCSCTL3_reserved6_DEF                                        (0x00000000)
  #define DCSCTL3_reserved6_HSH                                        (0x04383CB4)

#define DCSINIT_REG                                                    (0x00003CB8)

  #define DCSINIT_dcs_init_OFF                                         ( 0)
  #define DCSINIT_dcs_init_WID                                         ( 1)
  #define DCSINIT_dcs_init_MSK                                         (0x00000001)
  #define DCSINIT_dcs_init_MIN                                         (0)
  #define DCSINIT_dcs_init_MAX                                         (1) // 0x00000001
  #define DCSINIT_dcs_init_DEF                                         (0x00000000)
  #define DCSINIT_dcs_init_HSH                                         (0x01003CB8)

  #define DCSINIT_dcs_init_ovrd_OFF                                    ( 1)
  #define DCSINIT_dcs_init_ovrd_WID                                    ( 1)
  #define DCSINIT_dcs_init_ovrd_MSK                                    (0x00000002)
  #define DCSINIT_dcs_init_ovrd_MIN                                    (0)
  #define DCSINIT_dcs_init_ovrd_MAX                                    (1) // 0x00000001
  #define DCSINIT_dcs_init_ovrd_DEF                                    (0x00000000)
  #define DCSINIT_dcs_init_ovrd_HSH                                    (0x01023CB8)

  #define DCSINIT_Reserved7_OFF                                        ( 2)
  #define DCSINIT_Reserved7_WID                                        (30)
  #define DCSINIT_Reserved7_MSK                                        (0xFFFFFFFC)
  #define DCSINIT_Reserved7_MIN                                        (0)
  #define DCSINIT_Reserved7_MAX                                        (1073741823) // 0x3FFFFFFF
  #define DCSINIT_Reserved7_DEF                                        (0x00000000)
  #define DCSINIT_Reserved7_HSH                                        (0x1E043CB8)

#define DCSDIGCLKGATE_REG                                              (0x00003CBC)

  #define DCSDIGCLKGATE_dcs_dyndcsclken_OFF                            ( 0)
  #define DCSDIGCLKGATE_dcs_dyndcsclken_WID                            ( 1)
  #define DCSDIGCLKGATE_dcs_dyndcsclken_MSK                            (0x00000001)
  #define DCSDIGCLKGATE_dcs_dyndcsclken_MIN                            (0)
  #define DCSDIGCLKGATE_dcs_dyndcsclken_MAX                            (1) // 0x00000001
  #define DCSDIGCLKGATE_dcs_dyndcsclken_DEF                            (0x00000001)
  #define DCSDIGCLKGATE_dcs_dyndcsclken_HSH                            (0x01003CBC)

  #define DCSDIGCLKGATE_Reserved8_OFF                                  ( 1)
  #define DCSDIGCLKGATE_Reserved8_WID                                  (31)
  #define DCSDIGCLKGATE_Reserved8_MSK                                  (0xFFFFFFFE)
  #define DCSDIGCLKGATE_Reserved8_MIN                                  (0)
  #define DCSDIGCLKGATE_Reserved8_MAX                                  (2147483647) // 0x7FFFFFFF
  #define DCSDIGCLKGATE_Reserved8_DEF                                  (0x00000000)
  #define DCSDIGCLKGATE_Reserved8_HSH                                  (0x1F023CBC)

#define DCSSTATUS1_REG                                                 (0x00003CC0)

  #define DCSSTATUS1_dcs_hi_OFF                                        ( 0)
  #define DCSSTATUS1_dcs_hi_WID                                        ( 1)
  #define DCSSTATUS1_dcs_hi_MSK                                        (0x00000001)
  #define DCSSTATUS1_dcs_hi_MIN                                        (0)
  #define DCSSTATUS1_dcs_hi_MAX                                        (1) // 0x00000001
  #define DCSSTATUS1_dcs_hi_DEF                                        (0x00000000)
  #define DCSSTATUS1_dcs_hi_HSH                                        (0x01003CC0)

  #define DCSSTATUS1_dcs_lo_OFF                                        ( 1)
  #define DCSSTATUS1_dcs_lo_WID                                        ( 1)
  #define DCSSTATUS1_dcs_lo_MSK                                        (0x00000002)
  #define DCSSTATUS1_dcs_lo_MIN                                        (0)
  #define DCSSTATUS1_dcs_lo_MAX                                        (1) // 0x00000001
  #define DCSSTATUS1_dcs_lo_DEF                                        (0x00000000)
  #define DCSSTATUS1_dcs_lo_HSH                                        (0x01023CC0)

  #define DCSSTATUS1_dcs_dl_code_OFF                                   ( 2)
  #define DCSSTATUS1_dcs_dl_code_WID                                   (10)
  #define DCSSTATUS1_dcs_dl_code_MSK                                   (0x00000FFC)
  #define DCSSTATUS1_dcs_dl_code_MIN                                   (0)
  #define DCSSTATUS1_dcs_dl_code_MAX                                   (1023) // 0x000003FF
  #define DCSSTATUS1_dcs_dl_code_DEF                                   (0x00000000)
  #define DCSSTATUS1_dcs_dl_code_HSH                                   (0x0A043CC0)

  #define DCSSTATUS1_dcs_frozen_OFF                                    (12)
  #define DCSSTATUS1_dcs_frozen_WID                                    ( 1)
  #define DCSSTATUS1_dcs_frozen_MSK                                    (0x00001000)
  #define DCSSTATUS1_dcs_frozen_MIN                                    (0)
  #define DCSSTATUS1_dcs_frozen_MAX                                    (1) // 0x00000001
  #define DCSSTATUS1_dcs_frozen_DEF                                    (0x00000000)
  #define DCSSTATUS1_dcs_frozen_HSH                                    (0x01183CC0)

  #define DCSSTATUS1_dcs_started_OFF                                   (13)
  #define DCSSTATUS1_dcs_started_WID                                   ( 1)
  #define DCSSTATUS1_dcs_started_MSK                                   (0x00002000)
  #define DCSSTATUS1_dcs_started_MIN                                   (0)
  #define DCSSTATUS1_dcs_started_MAX                                   (1) // 0x00000001
  #define DCSSTATUS1_dcs_started_DEF                                   (0x00000000)
  #define DCSSTATUS1_dcs_started_HSH                                   (0x011A3CC0)

  #define DCSSTATUS1_dcs_view0_OFF                                     (14)
  #define DCSSTATUS1_dcs_view0_WID                                     ( 1)
  #define DCSSTATUS1_dcs_view0_MSK                                     (0x00004000)
  #define DCSSTATUS1_dcs_view0_MIN                                     (0)
  #define DCSSTATUS1_dcs_view0_MAX                                     (1) // 0x00000001
  #define DCSSTATUS1_dcs_view0_DEF                                     (0x00000000)
  #define DCSSTATUS1_dcs_view0_HSH                                     (0x011C3CC0)

  #define DCSSTATUS1_dcs_view1_OFF                                     (15)
  #define DCSSTATUS1_dcs_view1_WID                                     ( 1)
  #define DCSSTATUS1_dcs_view1_MSK                                     (0x00008000)
  #define DCSSTATUS1_dcs_view1_MIN                                     (0)
  #define DCSSTATUS1_dcs_view1_MAX                                     (1) // 0x00000001
  #define DCSSTATUS1_dcs_view1_DEF                                     (0x00000000)
  #define DCSSTATUS1_dcs_view1_HSH                                     (0x011E3CC0)

  #define DCSSTATUS1_Reserved9_OFF                                     (16)
  #define DCSSTATUS1_Reserved9_WID                                     (16)
  #define DCSSTATUS1_Reserved9_MSK                                     (0xFFFF0000)
  #define DCSSTATUS1_Reserved9_MIN                                     (0)
  #define DCSSTATUS1_Reserved9_MAX                                     (65535) // 0x0000FFFF
  #define DCSSTATUS1_Reserved9_DEF                                     (0x00000000)
  #define DCSSTATUS1_Reserved9_HSH                                     (0x10203CC0)

#define PLLSTATUS_REG                                                  (0x00003CC4)

  #define PLLSTATUS_plllock_OFF                                        ( 0)
  #define PLLSTATUS_plllock_WID                                        ( 1)
  #define PLLSTATUS_plllock_MSK                                        (0x00000001)
  #define PLLSTATUS_plllock_MIN                                        (0)
  #define PLLSTATUS_plllock_MAX                                        (1) // 0x00000001
  #define PLLSTATUS_plllock_DEF                                        (0x00000000)
  #define PLLSTATUS_plllock_HSH                                        (0x01003CC4)

  #define PLLSTATUS_Reserved10_OFF                                     ( 1)
  #define PLLSTATUS_Reserved10_WID                                     (31)
  #define PLLSTATUS_Reserved10_MSK                                     (0xFFFFFFFE)
  #define PLLSTATUS_Reserved10_MIN                                     (0)
  #define PLLSTATUS_Reserved10_MAX                                     (2147483647) // 0x7FFFFFFF
  #define PLLSTATUS_Reserved10_DEF                                     (0x00000000)
  #define PLLSTATUS_Reserved10_HSH                                     (0x1F023CC4)

#define DCCDIGOBS_REG                                                  (0x00003CC8)

  #define DCCDIGOBS_dcs_viewsel1_OFF                                   ( 0)
  #define DCCDIGOBS_dcs_viewsel1_WID                                   ( 3)
  #define DCCDIGOBS_dcs_viewsel1_MSK                                   (0x00000007)
  #define DCCDIGOBS_dcs_viewsel1_MIN                                   (0)
  #define DCCDIGOBS_dcs_viewsel1_MAX                                   (7) // 0x00000007
  #define DCCDIGOBS_dcs_viewsel1_DEF                                   (0x00000000)
  #define DCCDIGOBS_dcs_viewsel1_HSH                                   (0x03003CC8)

  #define DCCDIGOBS_dcs_viewsel0_OFF                                   ( 3)
  #define DCCDIGOBS_dcs_viewsel0_WID                                   ( 3)
  #define DCCDIGOBS_dcs_viewsel0_MSK                                   (0x00000038)
  #define DCCDIGOBS_dcs_viewsel0_MIN                                   (0)
  #define DCCDIGOBS_dcs_viewsel0_MAX                                   (7) // 0x00000007
  #define DCCDIGOBS_dcs_viewsel0_DEF                                   (0x00000000)
  #define DCCDIGOBS_dcs_viewsel0_HSH                                   (0x03063CC8)

  #define DCCDIGOBS_Reserved12_OFF                                     ( 6)
  #define DCCDIGOBS_Reserved12_WID                                     ( 2)
  #define DCCDIGOBS_Reserved12_MSK                                     (0x000000C0)
  #define DCCDIGOBS_Reserved12_MIN                                     (0)
  #define DCCDIGOBS_Reserved12_MAX                                     (3) // 0x00000003
  #define DCCDIGOBS_Reserved12_DEF                                     (0x00000000)
  #define DCCDIGOBS_Reserved12_HSH                                     (0x020C3CC8)

  #define DCCDIGOBS_dcdcomp_bonus_OFF                                  ( 8)
  #define DCCDIGOBS_dcdcomp_bonus_WID                                  ( 2)
  #define DCCDIGOBS_dcdcomp_bonus_MSK                                  (0x00000300)
  #define DCCDIGOBS_dcdcomp_bonus_MIN                                  (0)
  #define DCCDIGOBS_dcdcomp_bonus_MAX                                  (3) // 0x00000003
  #define DCCDIGOBS_dcdcomp_bonus_DEF                                  (0x00000000)
  #define DCCDIGOBS_dcdcomp_bonus_HSH                                  (0x02103CC8)

  #define DCCDIGOBS_Reserved11_OFF                                     (10)
  #define DCCDIGOBS_Reserved11_WID                                     (22)
  #define DCCDIGOBS_Reserved11_MSK                                     (0xFFFFFC00)
  #define DCCDIGOBS_Reserved11_MIN                                     (0)
  #define DCCDIGOBS_Reserved11_MAX                                     (4194303) // 0x003FFFFF
  #define DCCDIGOBS_Reserved11_DEF                                     (0x00000000)
  #define DCCDIGOBS_Reserved11_HSH                                     (0x16143CC8)

#define DCSRUNMODE_REG                                                 (0x00003CCC)

  #define DCSRUNMODE_dcs_run_mode_OFF                                  ( 0)
  #define DCSRUNMODE_dcs_run_mode_WID                                  ( 2)
  #define DCSRUNMODE_dcs_run_mode_MSK                                  (0x00000003)
  #define DCSRUNMODE_dcs_run_mode_MIN                                  (0)
  #define DCSRUNMODE_dcs_run_mode_MAX                                  (3) // 0x00000003
  #define DCSRUNMODE_dcs_run_mode_DEF                                  (0x00000000)
  #define DCSRUNMODE_dcs_run_mode_HSH                                  (0x02003CCC)

  #define DCSRUNMODE_Reserved13_OFF                                    ( 2)
  #define DCSRUNMODE_Reserved13_WID                                    (30)
  #define DCSRUNMODE_Reserved13_MSK                                    (0xFFFFFFFC)
  #define DCSRUNMODE_Reserved13_MIN                                    (0)
  #define DCSRUNMODE_Reserved13_MAX                                    (1073741823) // 0x3FFFFFFF
  #define DCSRUNMODE_Reserved13_DEF                                    (0x00000000)
  #define DCSRUNMODE_Reserved13_HSH                                    (0x1E043CCC)

#define TXDLLSIGGRPDCCCTL34_REG                                        (0x00003CD0)

  #define TXDLLSIGGRPDCCCTL34_periodic_init_dcccode_OFF                ( 0)
  #define TXDLLSIGGRPDCCCTL34_periodic_init_dcccode_WID                ( 8)
  #define TXDLLSIGGRPDCCCTL34_periodic_init_dcccode_MSK                (0x000000FF)
  #define TXDLLSIGGRPDCCCTL34_periodic_init_dcccode_MIN                (0)
  #define TXDLLSIGGRPDCCCTL34_periodic_init_dcccode_MAX                (255) // 0x000000FF
  #define TXDLLSIGGRPDCCCTL34_periodic_init_dcccode_DEF                (0x00000000)
  #define TXDLLSIGGRPDCCCTL34_periodic_init_dcccode_HSH                (0x08003CD0)

  #define TXDLLSIGGRPDCCCTL34_Reserved14_OFF                           ( 8)
  #define TXDLLSIGGRPDCCCTL34_Reserved14_WID                           (24)
  #define TXDLLSIGGRPDCCCTL34_Reserved14_MSK                           (0xFFFFFF00)
  #define TXDLLSIGGRPDCCCTL34_Reserved14_MIN                           (0)
  #define TXDLLSIGGRPDCCCTL34_Reserved14_MAX                           (16777215) // 0x00FFFFFF
  #define TXDLLSIGGRPDCCCTL34_Reserved14_DEF                           (0x00000000)
  #define TXDLLSIGGRPDCCCTL34_Reserved14_HSH                           (0x18103CD0)

#define TXDLLSIGGRPDCCCTL35_REG                                        (0x00003CD4)

  #define TXDLLSIGGRPDCCCTL35_periodic_percode_status_OFF              ( 0)
  #define TXDLLSIGGRPDCCCTL35_periodic_percode_status_WID              ( 8)
  #define TXDLLSIGGRPDCCCTL35_periodic_percode_status_MSK              (0x000000FF)
  #define TXDLLSIGGRPDCCCTL35_periodic_percode_status_MIN              (0)
  #define TXDLLSIGGRPDCCCTL35_periodic_percode_status_MAX              (255) // 0x000000FF
  #define TXDLLSIGGRPDCCCTL35_periodic_percode_status_DEF              (0x00000000)
  #define TXDLLSIGGRPDCCCTL35_periodic_percode_status_HSH              (0x08003CD4)

  #define TXDLLSIGGRPDCCCTL35_periodic_initcode_status_OFF             ( 8)
  #define TXDLLSIGGRPDCCCTL35_periodic_initcode_status_WID             ( 8)
  #define TXDLLSIGGRPDCCCTL35_periodic_initcode_status_MSK             (0x0000FF00)
  #define TXDLLSIGGRPDCCCTL35_periodic_initcode_status_MIN             (0)
  #define TXDLLSIGGRPDCCCTL35_periodic_initcode_status_MAX             (255) // 0x000000FF
  #define TXDLLSIGGRPDCCCTL35_periodic_initcode_status_DEF             (0x00000000)
  #define TXDLLSIGGRPDCCCTL35_periodic_initcode_status_HSH             (0x08103CD4)

  #define TXDLLSIGGRPDCCCTL35_Reserved15_OFF                           (16)
  #define TXDLLSIGGRPDCCCTL35_Reserved15_WID                           (16)
  #define TXDLLSIGGRPDCCCTL35_Reserved15_MSK                           (0xFFFF0000)
  #define TXDLLSIGGRPDCCCTL35_Reserved15_MIN                           (0)
  #define TXDLLSIGGRPDCCCTL35_Reserved15_MAX                           (65535) // 0x0000FFFF
  #define TXDLLSIGGRPDCCCTL35_Reserved15_DEF                           (0x00000000)
  #define TXDLLSIGGRPDCCCTL35_Reserved15_HSH                           (0x10203CD4)

#define DCSOFFSET_REG                                                  (0x00003CD8)

  #define DCSOFFSET_dccoffset_OFF                                      ( 0)
  #define DCSOFFSET_dccoffset_WID                                      ( 8)
  #define DCSOFFSET_dccoffset_MSK                                      (0x000000FF)
  #define DCSOFFSET_dccoffset_MIN                                      (0)
  #define DCSOFFSET_dccoffset_MAX                                      (255) // 0x000000FF
  #define DCSOFFSET_dccoffset_DEF                                      (0x00000000)
  #define DCSOFFSET_dccoffset_HSH                                      (0x08003CD8)

  #define DCSOFFSET_dccoffsetsign_OFF                                  ( 8)
  #define DCSOFFSET_dccoffsetsign_WID                                  ( 1)
  #define DCSOFFSET_dccoffsetsign_MSK                                  (0x00000100)
  #define DCSOFFSET_dccoffsetsign_MIN                                  (0)
  #define DCSOFFSET_dccoffsetsign_MAX                                  (1) // 0x00000001
  #define DCSOFFSET_dccoffsetsign_DEF                                  (0x00000000)
  #define DCSOFFSET_dccoffsetsign_HSH                                  (0x01103CD8)

  #define DCSOFFSET_reserved16_OFF                                     ( 9)
  #define DCSOFFSET_reserved16_WID                                     (23)
  #define DCSOFFSET_reserved16_MSK                                     (0xFFFFFE00)
  #define DCSOFFSET_reserved16_MIN                                     (0)
  #define DCSOFFSET_reserved16_MAX                                     (8388607) // 0x007FFFFF
  #define DCSOFFSET_reserved16_DEF                                     (0x00000000)
  #define DCSOFFSET_reserved16_HSH                                     (0x17123CD8)

#define DFXDCS_CTL_REG                                                 (0x00003CDC)

  #define DFXDCS_CTL_dcsdfx_offset_OFF                                 ( 0)
  #define DFXDCS_CTL_dcsdfx_offset_WID                                 (10)
  #define DFXDCS_CTL_dcsdfx_offset_MSK                                 (0x000003FF)
  #define DFXDCS_CTL_dcsdfx_offset_MIN                                 (0)
  #define DFXDCS_CTL_dcsdfx_offset_MAX                                 (1023) // 0x000003FF
  #define DFXDCS_CTL_dcsdfx_offset_DEF                                 (0x00000000)
  #define DFXDCS_CTL_dcsdfx_offset_HSH                                 (0x0A003CDC)

  #define DFXDCS_CTL_dcsdfx_limit_OFF                                  (10)
  #define DFXDCS_CTL_dcsdfx_limit_WID                                  (10)
  #define DFXDCS_CTL_dcsdfx_limit_MSK                                  (0x000FFC00)
  #define DFXDCS_CTL_dcsdfx_limit_MIN                                  (0)
  #define DFXDCS_CTL_dcsdfx_limit_MAX                                  (1023) // 0x000003FF
  #define DFXDCS_CTL_dcsdfx_limit_DEF                                  (0x00000000)
  #define DFXDCS_CTL_dcsdfx_limit_HSH                                  (0x0A143CDC)

  #define DFXDCS_CTL_reserved17_OFF                                    (20)
  #define DFXDCS_CTL_reserved17_WID                                    (12)
  #define DFXDCS_CTL_reserved17_MSK                                    (0xFFF00000)
  #define DFXDCS_CTL_reserved17_MIN                                    (0)
  #define DFXDCS_CTL_reserved17_MAX                                    (4095) // 0x00000FFF
  #define DFXDCS_CTL_reserved17_DEF                                    (0x00000000)
  #define DFXDCS_CTL_reserved17_HSH                                    (0x0C283CDC)

#define DFXDCS_RUN_REG                                                 (0x00003CE0)

  #define DFXDCS_RUN_dcsdfx_start_OFF                                  ( 0)
  #define DFXDCS_RUN_dcsdfx_start_WID                                  ( 1)
  #define DFXDCS_RUN_dcsdfx_start_MSK                                  (0x00000001)
  #define DFXDCS_RUN_dcsdfx_start_MIN                                  (0)
  #define DFXDCS_RUN_dcsdfx_start_MAX                                  (1) // 0x00000001
  #define DFXDCS_RUN_dcsdfx_start_DEF                                  (0x00000000)
  #define DFXDCS_RUN_dcsdfx_start_HSH                                  (0x01003CE0)

  #define DFXDCS_RUN_reserved18_OFF                                    ( 1)
  #define DFXDCS_RUN_reserved18_WID                                    (31)
  #define DFXDCS_RUN_reserved18_MSK                                    (0xFFFFFFFE)
  #define DFXDCS_RUN_reserved18_MIN                                    (0)
  #define DFXDCS_RUN_reserved18_MAX                                    (2147483647) // 0x7FFFFFFF
  #define DFXDCS_RUN_reserved18_DEF                                    (0x00000000)
  #define DFXDCS_RUN_reserved18_HSH                                    (0x1F023CE0)

#define DFXDCS_STATUS_REG                                              (0x00003CE4)

  #define DFXDCS_STATUS_dcsdfx_done_OFF                                ( 0)
  #define DFXDCS_STATUS_dcsdfx_done_WID                                ( 1)
  #define DFXDCS_STATUS_dcsdfx_done_MSK                                (0x00000001)
  #define DFXDCS_STATUS_dcsdfx_done_MIN                                (0)
  #define DFXDCS_STATUS_dcsdfx_done_MAX                                (1) // 0x00000001
  #define DFXDCS_STATUS_dcsdfx_done_DEF                                (0x00000000)
  #define DFXDCS_STATUS_dcsdfx_done_HSH                                (0x01003CE4)

  #define DFXDCS_STATUS_dcsdfx_pass_OFF                                ( 1)
  #define DFXDCS_STATUS_dcsdfx_pass_WID                                ( 1)
  #define DFXDCS_STATUS_dcsdfx_pass_MSK                                (0x00000002)
  #define DFXDCS_STATUS_dcsdfx_pass_MIN                                (0)
  #define DFXDCS_STATUS_dcsdfx_pass_MAX                                (1) // 0x00000001
  #define DFXDCS_STATUS_dcsdfx_pass_DEF                                (0x00000000)
  #define DFXDCS_STATUS_dcsdfx_pass_HSH                                (0x01023CE4)

  #define DFXDCS_STATUS_reserved19_OFF                                 ( 2)
  #define DFXDCS_STATUS_reserved19_WID                                 (30)
  #define DFXDCS_STATUS_reserved19_MSK                                 (0xFFFFFFFC)
  #define DFXDCS_STATUS_reserved19_MIN                                 (0)
  #define DFXDCS_STATUS_reserved19_MAX                                 (1073741823) // 0x3FFFFFFF
  #define DFXDCS_STATUS_reserved19_DEF                                 (0x00000000)
  #define DFXDCS_STATUS_reserved19_HSH                                 (0x1E043CE4)

#define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_REG                           (0x00003CE8)

  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_cal_code_src_sel_OFF        ( 0)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_cal_code_src_sel_WID        ( 1)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_cal_code_src_sel_MSK        (0x00000001)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_cal_code_src_sel_MIN        (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_cal_code_src_sel_MAX        (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_cal_code_src_sel_DEF        (0x00000001)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_cal_code_src_sel_HSH        (0x01003CE8)

  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_ratio_src_sel_OFF       ( 1)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_ratio_src_sel_WID       ( 1)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_ratio_src_sel_MSK       (0x00000002)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_ratio_src_sel_MIN       (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_ratio_src_sel_MAX       (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_ratio_src_sel_DEF       (0x00000001)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_ratio_src_sel_HSH       (0x01023CE8)

  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_forceclkreq_OFF             ( 2)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_forceclkreq_WID             ( 1)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_forceclkreq_MSK             (0x00000004)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_forceclkreq_MIN             (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_forceclkreq_MAX             (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_forceclkreq_DEF             (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_forceclkreq_HSH             (0x01043CE8)

  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_enable_src_sel_OFF      ( 3)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_enable_src_sel_WID      ( 1)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_enable_src_sel_MSK      (0x00000008)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_enable_src_sel_MIN      (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_enable_src_sel_MAX      (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_enable_src_sel_DEF      (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fll_enable_src_sel_HSH      (0x01063CE8)

  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwireratio_OFF            ( 4)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwireratio_WID            ( 8)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwireratio_MSK            (0x00000FF0)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwireratio_MIN            (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwireratio_MAX            (255) // 0x000000FF
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwireratio_DEF            (0x00000010)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwireratio_HSH            (0x08083CE8)

  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwirecalcode_OFF          (12)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwirecalcode_WID          (16)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwirecalcode_MSK          (0x0FFFF000)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwirecalcode_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwirecalcode_MAX          (65535) // 0x0000FFFF
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwirecalcode_DEF          (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllwirecalcode_HSH          (0x10183CE8)

  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllcalcodevalid_OFF         (28)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllcalcodevalid_WID         ( 1)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllcalcodevalid_MSK         (0x10000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllcalcodevalid_MIN         (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllcalcodevalid_MAX         (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllcalcodevalid_DEF         (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_fllcalcodevalid_HSH         (0x01383CE8)

  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_spare_OFF                   (29)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_spare_WID                   ( 3)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_spare_MSK                   (0xE0000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_spare_MIN                   (0)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_spare_MAX                   (7) // 0x00000007
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_spare_DEF                   (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_spare_HSH                   (0x033A3CE8)

#define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_REG                       (0x00003CEC)

  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_dimmvref_visa_sel_OFF   ( 0)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_dimmvref_visa_sel_WID   ( 2)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_dimmvref_visa_sel_MSK   (0x00000003)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_dimmvref_visa_sel_MIN   (0)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_dimmvref_visa_sel_MAX   (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_dimmvref_visa_sel_DEF   (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_dimmvref_visa_sel_HSH   (0x02003CEC)

  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapfsm_visa_sel_OFF   ( 2)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapfsm_visa_sel_WID   ( 3)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapfsm_visa_sel_MSK   (0x0000001C)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapfsm_visa_sel_MIN   (0)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapfsm_visa_sel_MAX   (7) // 0x00000007
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapfsm_visa_sel_DEF   (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapfsm_visa_sel_HSH   (0x03043CEC)

  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_binfsm_visa_sel_OFF     ( 5)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_binfsm_visa_sel_WID     ( 3)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_binfsm_visa_sel_MSK     (0x000000E0)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_binfsm_visa_sel_MIN     (0)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_binfsm_visa_sel_MAX     (7) // 0x00000007
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_binfsm_visa_sel_DEF     (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_binfsm_visa_sel_HSH     (0x030A3CEC)

  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapoutput_visa_sel_OFF ( 8)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapoutput_visa_sel_WID ( 4)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapoutput_visa_sel_MSK (0x00000F00)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapoutput_visa_sel_MIN (0)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapoutput_visa_sel_MAX (15) // 0x0000000F
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapoutput_visa_sel_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_swcapoutput_visa_sel_HSH (0x04103CEC)

  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_init_vs_ngdcc_visa_sel_OFF (12)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_init_vs_ngdcc_visa_sel_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_init_vs_ngdcc_visa_sel_MSK (0x00001000)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_init_vs_ngdcc_visa_sel_MIN (0)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_init_vs_ngdcc_visa_sel_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_init_vs_ngdcc_visa_sel_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_init_vs_ngdcc_visa_sel_HSH (0x01183CEC)

  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_reserved20_OFF          (13)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_reserved20_WID          (19)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_reserved20_MSK          (0xFFFFE000)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_reserved20_MIN          (0)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_reserved20_MAX          (524287) // 0x0007FFFF
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_reserved20_DEF          (0x00000000)
  #define DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_reserved20_HSH          (0x131A3CEC)

#define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_REG                      (0x00003CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Dll_rxbwsel_A0_OFF        ( 0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Dll_rxbwsel_A0_WID        ( 6)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Dll_rxbwsel_A0_MSK        (0x0000003F)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Dll_rxbwsel_A0_MIN        (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Dll_rxbwsel_A0_MAX        (63) // 0x0000003F
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Dll_rxbwsel_A0_DEF        (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Dll_rxbwsel_A0_HSH        (0x06003CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_A0_OFF              ( 6)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_A0_WID              (26)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_A0_MSK              (0xFFFFFFC0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_A0_MIN              (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_A0_MAX              (67108863) // 0x03FFFFFF
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_A0_DEF              (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_A0_HSH              (0x1A0C3CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_offsetsearch_OFF ( 0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_offsetsearch_WID ( 2)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_offsetsearch_MSK (0x00000003)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_offsetsearch_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_offsetsearch_MAX (3) // 0x00000003
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_offsetsearch_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_offsetsearch_HSH (0x02003CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_codedelay_OFF  ( 2)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_codedelay_WID  ( 9)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_codedelay_MSK  (0x000007FC)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_codedelay_MIN  (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_codedelay_MAX  (511) // 0x000001FF
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_codedelay_DEF  (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Binfsm4_codedelay_HSH  (0x09043CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmdupextstatlegen_OFF (11)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmdupextstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmdupextstatlegen_MSK (0x00000800)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmdupextstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmdupextstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmdupextstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmdupextstatlegen_HSH (0x01163CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqupextstatlegen_OFF (12)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqupextstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqupextstatlegen_MSK (0x00001000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqupextstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqupextstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqupextstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqupextstatlegen_HSH (0x01183CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkupextstatlegen_OFF (13)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkupextstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkupextstatlegen_MSK (0x00002000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkupextstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkupextstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkupextstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkupextstatlegen_HSH (0x011A3CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctlupextstatlegen_OFF (14)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctlupextstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctlupextstatlegen_MSK (0x00004000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctlupextstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctlupextstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctlupextstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctlupextstatlegen_HSH (0x011C3CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtupextstatlegen_OFF (15)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtupextstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtupextstatlegen_MSK (0x00008000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtupextstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtupextstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtupextstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtupextstatlegen_HSH (0x011E3CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ckecsupextstatlegen_OFF (16)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ckecsupextstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ckecsupextstatlegen_MSK (0x00010000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ckecsupextstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ckecsupextstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ckecsupextstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ckecsupextstatlegen_HSH (0x01203CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpdnstatlegen_OFF (17)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpdnstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpdnstatlegen_MSK (0x00020000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpdnstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpdnstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpdnstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpdnstatlegen_HSH (0x01223CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpdnstatlegen_OFF (18)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpdnstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpdnstatlegen_MSK (0x00040000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpdnstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpdnstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpdnstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpdnstatlegen_HSH (0x01243CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpdnstatlegen_OFF (19)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpdnstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpdnstatlegen_MSK (0x00080000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpdnstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpdnstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpdnstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpdnstatlegen_HSH (0x01263CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpdnstatlegen_OFF (20)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpdnstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpdnstatlegen_MSK (0x00100000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpdnstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpdnstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpdnstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpdnstatlegen_HSH (0x01283CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpdnstatlegen_OFF (21)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpdnstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpdnstatlegen_MSK (0x00200000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpdnstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpdnstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpdnstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpdnstatlegen_HSH (0x012A3CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpupstatlegen_OFF (22)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpupstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpupstatlegen_MSK (0x00400000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpupstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpupstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpupstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_cmddnrelpupstatlegen_HSH (0x012C3CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpupstatlegen_OFF (23)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpupstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpupstatlegen_MSK (0x00800000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpupstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpupstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpupstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_dqdnrelpupstatlegen_HSH (0x012E3CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpupstatlegen_OFF (24)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpupstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpupstatlegen_MSK (0x01000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpupstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpupstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpupstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_clkdnrelpupstatlegen_HSH (0x01303CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpupstatlegen_OFF (25)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpupstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpupstatlegen_MSK (0x02000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpupstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpupstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpupstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_ctldnrelpupstatlegen_HSH (0x01323CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpupstatlegen_OFF (26)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpupstatlegen_WID ( 1)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpupstatlegen_MSK (0x04000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpupstatlegen_MIN (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpupstatlegen_MAX (1) // 0x00000001
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpupstatlegen_DEF (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_rcomp_cmn_odtdnrelpupstatlegen_HSH (0x01343CF0)

  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_OFF              (27)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_WID              ( 5)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_MSK              (0xF8000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_MIN              (0)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_MAX              (31) // 0x0000001F
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_DEF              (0x00000000)
  #define DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_Spare_HSH              (0x05363CF0)

#define DDRSCRAM_CR_DDRSCRAMBLECH0_REG                                 (0x00003E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramen_OFF                       ( 0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramen_WID                       ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramen_MSK                       (0x00000001)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramen_MIN                       (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramen_MAX                       (1) // 0x00000001
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramen_DEF                       (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramen_HSH                       (0x01003E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramkey_OFF                      ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramkey_WID                      (16)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramkey_MSK                      (0x0001FFFE)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramkey_MIN                      (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramkey_MAX                      (65535) // 0x0000FFFF
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramkey_DEF                      (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_scramkey_HSH                      (0x10023E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgateab_OFF                   (17)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgateab_WID                   ( 2)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgateab_MSK                   (0x00060000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgateab_MIN                   (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgateab_MAX                   (3) // 0x00000003
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgateab_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgateab_HSH                   (0x02223E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgatec_OFF                    (19)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgatec_WID                    ( 2)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgatec_MSK                    (0x00180000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgatec_MIN                    (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgatec_MAX                    (3) // 0x00000003
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgatec_DEF                    (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_clockgatec_HSH                    (0x02263E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_enabledbiab_OFF                   (21)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_enabledbiab_WID                   ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_enabledbiab_MSK                   (0x00200000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_enabledbiab_MIN                   (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_enabledbiab_MAX                   (1) // 0x00000001
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_enabledbiab_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_enabledbiab_HSH                   (0x012A3E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_ca_mirrored_OFF                   (22)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_ca_mirrored_WID                   ( 4)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_ca_mirrored_MSK                   (0x03C00000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_ca_mirrored_MIN                   (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_ca_mirrored_MAX                   (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_ca_mirrored_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_ca_mirrored_HSH                   (0x042C3E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_OFF               (26)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_WID               ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_MSK               (0x04000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_MIN               (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_MAX               (1) // 0x00000001
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_DEF               (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_HSH               (0x01343E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_at_idle_OFF       (27)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_at_idle_WID       ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_at_idle_MSK       (0x08000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_at_idle_MIN       (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_at_idle_MAX       (1) // 0x00000001
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_at_idle_DEF       (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_dis_cmdanalogen_at_idle_HSH       (0x01363E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_spare_OFF                         (28)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_spare_WID                         ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_spare_MSK                         (0x10000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_spare_MIN                         (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_spare_MAX                         (1) // 0x00000001
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_spare_DEF                         (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_spare_HSH                         (0x01383E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdistdisable_OFF          (29)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdistdisable_WID          ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdistdisable_MSK          (0x20000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdistdisable_MIN          (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdistdisable_MAX          (1) // 0x00000001
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdistdisable_DEF          (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdistdisable_HSH          (0x013A3E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_earlyrankrdvalidswitch_OFF        (30)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_earlyrankrdvalidswitch_WID        ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_earlyrankrdvalidswitch_MSK        (0x40000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_earlyrankrdvalidswitch_MIN        (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_earlyrankrdvalidswitch_MAX        (1) // 0x00000001
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_earlyrankrdvalidswitch_DEF        (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_earlyrankrdvalidswitch_HSH        (0x013C3E00)

  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdist_OFF                 (31)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdist_WID                 ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdist_MSK                 (0x80000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdist_MIN                 (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdist_MAX                 (1) // 0x00000001
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdist_DEF                 (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH0_forcecompdist_HSH                 (0x013E3E00)

#define DDRSCRAM_CR_DDRSCRAMBLECH1_REG                                 (0x00003E04)

  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramen_OFF                       ( 0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramen_WID                       ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramen_MSK                       (0x00000001)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramen_MIN                       (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramen_MAX                       (1) // 0x00000001
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramen_DEF                       (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramen_HSH                       (0x01003E04)

  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramkey_OFF                      ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramkey_WID                      (16)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramkey_MSK                      (0x0001FFFE)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramkey_MIN                      (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramkey_MAX                      (65535) // 0x0000FFFF
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramkey_DEF                      (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_scramkey_HSH                      (0x10023E04)

  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgateab_OFF                   (17)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgateab_WID                   ( 2)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgateab_MSK                   (0x00060000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgateab_MIN                   (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgateab_MAX                   (3) // 0x00000003
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgateab_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgateab_HSH                   (0x02223E04)

  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgatec_OFF                    (19)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgatec_WID                    ( 2)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgatec_MSK                    (0x00180000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgatec_MIN                    (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgatec_MAX                    (3) // 0x00000003
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgatec_DEF                    (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_clockgatec_HSH                    (0x02263E04)

  #define DDRSCRAM_CR_DDRSCRAMBLECH1_enabledbiab_OFF                   (21)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_enabledbiab_WID                   ( 1)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_enabledbiab_MSK                   (0x00200000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_enabledbiab_MIN                   (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_enabledbiab_MAX                   (1) // 0x00000001
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_enabledbiab_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_enabledbiab_HSH                   (0x012A3E04)

  #define DDRSCRAM_CR_DDRSCRAMBLECH1_ca_mirrored_OFF                   (22)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_ca_mirrored_WID                   ( 4)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_ca_mirrored_MSK                   (0x03C00000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_ca_mirrored_MIN                   (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_ca_mirrored_MAX                   (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_ca_mirrored_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_ca_mirrored_HSH                   (0x042C3E04)

  #define DDRSCRAM_CR_DDRSCRAMBLECH1_spare_OFF                         (26)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_spare_WID                         ( 6)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_spare_MSK                         (0xFC000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_spare_MIN                         (0)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_spare_MAX                         (63) // 0x0000003F
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_spare_DEF                         (0x00000000)
  #define DDRSCRAM_CR_DDRSCRAMBLECH1_spare_HSH                         (0x06343E04)

#define DDRSCRAM_CR_DDRSCRAMBLECH2_REG                                 (0x00003E08)
//Duplicate of DDRSCRAM_CR_DDRSCRAMBLECH1_REG

#define DDRSCRAM_CR_DDRSCRAMBLECH3_REG                                 (0x00003E0C)
//Duplicate of DDRSCRAM_CR_DDRSCRAMBLECH1_REG

#define DDRSCRAM_CR_DDRSCRAMBLECH4_REG                                 (0x00003E10)
//Duplicate of DDRSCRAM_CR_DDRSCRAMBLECH1_REG

#define DDRSCRAM_CR_DDRSCRAMBLECH5_REG                                 (0x00003E14)
//Duplicate of DDRSCRAM_CR_DDRSCRAMBLECH1_REG

#define DDRSCRAM_CR_DDRSCRAMBLECH6_REG                                 (0x00003E18)
//Duplicate of DDRSCRAM_CR_DDRSCRAMBLECH1_REG

#define DDRSCRAM_CR_DDRSCRAMBLECH7_REG                                 (0x00003E1C)
//Duplicate of DDRSCRAM_CR_DDRSCRAMBLECH1_REG

#define DDRSCRAM_CR_DDRMISCCONTROL0_REG                                (0x00003E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_spare0_OFF                       ( 0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_spare0_WID                       ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_spare0_MSK                       (0x00000001)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_spare0_MIN                       (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_spare0_MAX                       (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_spare0_DEF                       (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_spare0_HSH                       (0x01003E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_OvrdPeriodicToDvfsComp_OFF       ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_OvrdPeriodicToDvfsComp_WID       ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_OvrdPeriodicToDvfsComp_MSK       (0x00000002)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_OvrdPeriodicToDvfsComp_MIN       (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_OvrdPeriodicToDvfsComp_MAX       (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_OvrdPeriodicToDvfsComp_DEF       (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_OvrdPeriodicToDvfsComp_HSH       (0x01023E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_eccpresent_OFF                   ( 2)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_eccpresent_WID                   ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_eccpresent_MSK                   (0x00000004)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_eccpresent_MIN                   (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_eccpresent_MAX                   (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_eccpresent_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_eccpresent_HSH                   (0x01043E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_write0_enable_OFF                ( 3)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_write0_enable_WID                ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_write0_enable_MSK                (0x00000008)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_write0_enable_MIN                (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_write0_enable_MAX                (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_write0_enable_DEF                (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_write0_enable_HSH                (0x01063E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1cmdondclkfall_OFF           ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1cmdondclkfall_WID           ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1cmdondclkfall_MSK           (0x00000010)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1cmdondclkfall_MIN           (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1cmdondclkfall_MAX           (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1cmdondclkfall_DEF           (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1cmdondclkfall_HSH           (0x01083E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcecompupdate_OFF              ( 5)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcecompupdate_WID              ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcecompupdate_MSK              (0x00000020)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcecompupdate_MIN              (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcecompupdate_MAX              (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcecompupdate_DEF              (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcecompupdate_HSH              (0x010A3E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_rank_4_mode_OFF                  ( 6)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rank_4_mode_WID                  ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rank_4_mode_MSK                  (0x00000040)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rank_4_mode_MIN                  (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rank_4_mode_MAX                  (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rank_4_mode_DEF                  (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rank_4_mode_HSH                  (0x010C3E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_wck2ck_4_1_OFF                   ( 7)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wck2ck_4_1_WID                   ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wck2ck_4_1_MSK                   (0x00000080)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wck2ck_4_1_MIN                   (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wck2ck_4_1_MAX                   (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wck2ck_4_1_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wck2ck_4_1_HSH                   (0x010E3E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_wrp_d5_wr_0_OFF                  ( 8)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wrp_d5_wr_0_WID                  ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wrp_d5_wr_0_MSK                  (0x00000100)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wrp_d5_wr_0_MIN                  (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wrp_d5_wr_0_MAX                  (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wrp_d5_wr_0_DEF                  (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_wrp_d5_wr_0_HSH                  (0x01103E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr5_mode_OFF                    ( 9)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr5_mode_WID                    ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr5_mode_MSK                    (0x00000200)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr5_mode_MIN                    (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr5_mode_MAX                    (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr5_mode_DEF                    (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr5_mode_HSH                    (0x01123E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddrnochinterleave_OFF            (10)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddrnochinterleave_WID            ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddrnochinterleave_MSK            (0x00000400)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddrnochinterleave_MIN            (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddrnochinterleave_MAX            (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddrnochinterleave_DEF            (0x00000001)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddrnochinterleave_HSH            (0x01143E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr_mode_OFF                   (11)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr_mode_WID                   ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr_mode_MSK                   (0x00000800)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr_mode_MIN                   (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr_mode_MAX                   (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr_mode_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr_mode_HSH                   (0x01163E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1_OFF                        (12)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1_WID                        ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1_MSK                        (0x00001000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1_MIN                        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1_MAX                        (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1_DEF                        (0x00000001)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_gear1_HSH                        (0x01183E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_rcven_extension_OFF              (13)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rcven_extension_WID              ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rcven_extension_MSK              (0x0001E000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rcven_extension_MIN              (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rcven_extension_MAX              (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rcven_extension_DEF              (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_rcven_extension_HSH              (0x041A3E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr4_mode_OFF                    (17)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr4_mode_WID                    ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr4_mode_MSK                    (0x00020000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr4_mode_MIN                    (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr4_mode_MAX                    (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr4_mode_DEF                    (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_ddr4_mode_HSH                    (0x01223E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_clkgatedisable_OFF               (18)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_clkgatedisable_WID               ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_clkgatedisable_MSK               (0x00040000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_clkgatedisable_MIN               (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_clkgatedisable_MAX               (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_clkgatedisable_DEF               (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_clkgatedisable_HSH               (0x01243E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_dataclkgatedisatidle_OFF         (19)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dataclkgatedisatidle_WID         ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dataclkgatedisatidle_MSK         (0x00080000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dataclkgatedisatidle_MIN         (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dataclkgatedisatidle_MAX         (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dataclkgatedisatidle_DEF         (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dataclkgatedisatidle_HSH         (0x01263E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_Gear4_OFF                        (20)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_Gear4_WID                        ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_Gear4_MSK                        (0x00100000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_Gear4_MIN                        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_Gear4_MAX                        (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_Gear4_DEF                        (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_Gear4_HSH                        (0x01283E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr4mode_OFF                   (21)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr4mode_WID                   ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr4mode_MSK                   (0x00200000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr4mode_MIN                   (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr4mode_MAX                   (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr4mode_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_lpddr4mode_HSH                   (0x012A3E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_channel_not_populated_OFF        (22)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_channel_not_populated_WID        ( 8)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_channel_not_populated_MSK        (0x3FC00000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_channel_not_populated_MIN        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_channel_not_populated_MAX        (255) // 0x000000FF
  #define DDRSCRAM_CR_DDRMISCCONTROL0_channel_not_populated_DEF        (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_channel_not_populated_HSH        (0x082C3E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_dis_iosf_sb_clk_gate_OFF         (30)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dis_iosf_sb_clk_gate_WID         ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dis_iosf_sb_clk_gate_MSK         (0x40000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dis_iosf_sb_clk_gate_MIN         (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dis_iosf_sb_clk_gate_MAX         (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dis_iosf_sb_clk_gate_DEF         (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_dis_iosf_sb_clk_gate_HSH         (0x013C3E20)

  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcedeltadqsupdate_OFF          (31)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcedeltadqsupdate_WID          ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcedeltadqsupdate_MSK          (0x80000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcedeltadqsupdate_MIN          (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcedeltadqsupdate_MAX          (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcedeltadqsupdate_DEF          (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL0_forcedeltadqsupdate_HSH          (0x013E3E20)

#define DDRSCRAM_CR_DDRMISCCONTROL1_REG                                (0x00003E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqgndsel_OFF ( 0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqgndsel_WID ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqgndsel_MSK (0x00000001)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqgndsel_MIN (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqgndsel_MAX (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqgndsel_DEF (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqgndsel_HSH (0x01003E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqglben_OFF ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqglben_WID ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqglben_MSK (0x00000002)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqglben_MIN (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqglben_MAX (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqglben_DEF (0x00000001)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatevddq_cmn_vsxhivddqglben_HSH (0x01023E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2gndsel_OFF  ( 2)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2gndsel_WID  ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2gndsel_MSK  (0x00000004)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2gndsel_MIN  (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2gndsel_MAX  (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2gndsel_DEF  (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2gndsel_HSH  (0x01043E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2glben_OFF   ( 3)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2glben_WID   ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2glben_MSK   (0x00000008)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2glben_MIN   (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2glben_MAX   (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2glben_DEF   (0x00000001)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgatehv_cmn_vsxhivdd2glben_HSH   (0x01063E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgate_cmn_vsxhisel_OFF           ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgate_cmn_vsxhisel_WID           ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgate_cmn_vsxhisel_MSK           (0x00000010)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgate_cmn_vsxhisel_MIN           (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgate_cmn_vsxhisel_MAX           (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgate_cmn_vsxhisel_DEF           (0x00000001)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_pgate_cmn_vsxhisel_HSH           (0x01083E24)
  
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs0_OFF             ( 0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs0_WID             ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs0_MSK             (0x0000000F)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs0_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs0_MAX             (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs0_DEF             (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs0_HSH             (0x04003E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_OFF             ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_WID             ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_MSK             (0x000000F0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_MAX             (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_DEF             (0x00000003)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_HSH             (0x04083E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_Q0_OFF             ( 5)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_Q0_WID             ( 3)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_Q0_MSK             (0x000000E0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_Q0_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_Q0_MAX             (7) // 0x00000007
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_Q0_DEF             (0x00000003)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs1_Q0_HSH             (0x030A3E24)
  
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs2_OFF             ( 8)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs2_WID             ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs2_MSK             (0x00000F00)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs2_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs2_MAX             (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs2_DEF             (0x00000001)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs2_HSH             (0x04103E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs3_OFF             (12)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs3_WID             ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs3_MSK             (0x0000F000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs3_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs3_MAX             (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs3_DEF             (0x00000002)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_cs3_HSH             (0x04183E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt0_OFF            (16)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt0_WID            ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt0_MSK            (0x000F0000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt0_MIN            (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt0_MAX            (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt0_DEF            (0x00000004)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt0_HSH            (0x04203E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt1_OFF            (20)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt1_WID            ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt1_MSK            (0x00F00000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt1_MIN            (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt1_MAX            (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt1_DEF            (0x00000006)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_csodtmapping_odt1_HSH            (0x04283E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_OFF                 (24)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_WID                 ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_MSK                 (0x01000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_MIN                 (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_MAX                 (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_DEF                 (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_HSH                 (0x01303E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_quiet_time_OFF      (25)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_quiet_time_WID      ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_quiet_time_MSK      (0x02000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_quiet_time_MIN      (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_quiet_time_MAX      (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_quiet_time_DEF      (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_quiet_time_HSH      (0x01323E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_duration_OFF        (26)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_duration_WID        ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_duration_MSK        (0x3C000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_duration_MIN        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_duration_MAX        (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_duration_DEF        (0x0000000F)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_duration_HSH        (0x04343E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_companddeltadqsupdateclkgatedisable_OFF (30)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_companddeltadqsupdateclkgatedisable_WID ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_companddeltadqsupdateclkgatedisable_MSK (0x40000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_companddeltadqsupdateclkgatedisable_MIN (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_companddeltadqsupdateclkgatedisable_MAX (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL1_companddeltadqsupdateclkgatedisable_DEF (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_companddeltadqsupdateclkgatedisable_HSH (0x013C3E24)

  #define DDRSCRAM_CR_DDRMISCCONTROL1_spare_OFF                        (31)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_spare_WID                        ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_spare_MSK                        (0x80000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_spare_MIN                        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_spare_MAX                        (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL1_spare_DEF                        (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL1_spare_HSH                        (0x013E3E24)

#define DDRSCRAM_CR_DDRMISCCONTROL2_REG                                (0x00003E28)

  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs0_OFF             ( 0)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs0_WID             ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs0_MSK             (0x0000000F)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs0_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs0_MAX             (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs0_DEF             (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs0_HSH             (0x04003E28)

  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs1_OFF             ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs1_WID             ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs1_MSK             (0x000000F0)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs1_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs1_MAX             (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs1_DEF             (0x00000003)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs1_HSH             (0x04083E28)

  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs2_OFF             ( 8)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs2_WID             ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs2_MSK             (0x00000F00)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs2_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs2_MAX             (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs2_DEF             (0x00000001)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs2_HSH             (0x04103E28)

  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs3_OFF             (12)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs3_WID             ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs3_MSK             (0x0000F000)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs3_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs3_MAX             (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs3_DEF             (0x00000002)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_cs3_HSH             (0x04183E28)

  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt0_OFF            (16)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt0_WID            ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt0_MSK            (0x000F0000)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt0_MIN            (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt0_MAX            (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt0_DEF            (0x00000004)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt0_HSH            (0x04203E28)

  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt1_OFF            (20)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt1_WID            ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt1_MSK            (0x00F00000)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt1_MIN            (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt1_MAX            (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt1_DEF            (0x00000006)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_csodtmapping_odt1_HSH            (0x04283E28)

  #define DDRSCRAM_CR_DDRMISCCONTROL2_lpdeltadqstrainmode_OFF          (24)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_lpdeltadqstrainmode_WID          ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_lpdeltadqstrainmode_MSK          (0x01000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_lpdeltadqstrainmode_MIN          (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_lpdeltadqstrainmode_MAX          (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL2_lpdeltadqstrainmode_DEF          (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_lpdeltadqstrainmode_HSH          (0x01303E28)

  #define DDRSCRAM_CR_DDRMISCCONTROL2_rx_analogen_grace_cnt_OFF        (25)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_rx_analogen_grace_cnt_WID        ( 7)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_rx_analogen_grace_cnt_MSK        (0xFE000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_rx_analogen_grace_cnt_MIN        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_rx_analogen_grace_cnt_MAX        (127) // 0x0000007F
  #define DDRSCRAM_CR_DDRMISCCONTROL2_rx_analogen_grace_cnt_DEF        (0x00000064)
  #define DDRSCRAM_CR_DDRMISCCONTROL2_rx_analogen_grace_cnt_HSH        (0x07323E28)

#define MCMISCS_WRITECFGCH0_REG                                        (0x00003E2C)

  #define MCMISCS_WRITECFGCH0_tcwl4txdqfifowren_OFF                    ( 0)
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfifowren_WID                    ( 7)
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfifowren_MSK                    (0x0000007F)
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfifowren_MIN                    (0)
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfifowren_MAX                    (127) // 0x0000007F
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfifowren_DEF                    (0x00000000)
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfifowren_HSH                    (0x07003E2C)

  #define MCMISCS_WRITECFGCH0_tcwl4txdqfiforden_OFF                    ( 7)
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfiforden_WID                    ( 7)
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfiforden_MSK                    (0x00003F80)
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfiforden_MIN                    (0)
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfiforden_MAX                    (127) // 0x0000007F
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfiforden_DEF                    (0x00000000)
  #define MCMISCS_WRITECFGCH0_tcwl4txdqfiforden_HSH                    (0x070E3E2C)

  #define MCMISCS_WRITECFGCH0_spare_A0_OFF                                (14)
  #define MCMISCS_WRITECFGCH0_spare_A0_WID                                ( 5)
  #define MCMISCS_WRITECFGCH0_spare_A0_MSK                                (0x0007C000)
  #define MCMISCS_WRITECFGCH0_spare_A0_MIN                                (0)
  #define MCMISCS_WRITECFGCH0_spare_A0_MAX                                (31) // 0x0000001F
  #define MCMISCS_WRITECFGCH0_spare_A0_DEF                                (0x00000000)
  #define MCMISCS_WRITECFGCH0_spare_A0_HSH                                (0x051C3E2C)

  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_A0_OFF                       (19)
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_A0_WID                       ( 1)
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_A0_MSK                       (0x00080000)
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_A0_MIN                       (0)
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_A0_MAX                       (1) // 0x00000001
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_A0_DEF                       (0x00000001)
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_A0_HSH                       (0x01263E2C)

  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_A0_OFF                       (20)
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_A0_WID                       ( 1)
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_A0_MSK                       (0x00100000)
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_A0_MIN                       (0)
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_A0_MAX                       (1) // 0x00000001
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_A0_DEF                       (0x00000001)
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_A0_HSH                       (0x01283E2C)

  #define MCMISCS_WRITECFGCH0_rptchrepclkon_A0_OFF                        (21)
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_A0_WID                        ( 1)
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_A0_MSK                        (0x00200000)
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_A0_MIN                        (0)
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_A0_MAX                        (1) // 0x00000001
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_A0_DEF                        (0x00000001)
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_A0_HSH                        (0x012A3E2C)

  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_A0_OFF                    (22)
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_A0_WID                    ( 3)
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_A0_MSK                    (0x01C00000)
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_A0_MIN                    (0)
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_A0_MAX                    (7) // 0x00000007
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_A0_DEF                    (0x00000007)
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_A0_HSH                    (0x032C3E2C)

  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_A0_OFF                     (25)
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_A0_WID                     ( 6)
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_A0_MSK                     (0x7E000000)
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_A0_MIN                     (0)
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_A0_MAX                     (63) // 0x0000003F
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_A0_DEF                     (0x0000003F)
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_A0_HSH                     (0x06323E2C)

  #define MCMISCS_WRITECFGCH0_spare_OFF                                (14)
  #define MCMISCS_WRITECFGCH0_spare_WID                                ( 4)
  #define MCMISCS_WRITECFGCH0_spare_MSK                                (0x0003C000)
  #define MCMISCS_WRITECFGCH0_spare_MIN                                (0)
  #define MCMISCS_WRITECFGCH0_spare_MAX                                (15) // 0x0000000F
  #define MCMISCS_WRITECFGCH0_spare_DEF                                (0x00000000)
  #define MCMISCS_WRITECFGCH0_spare_HSH                                (0x041C3E2C)

  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_OFF                       (18)
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_WID                       ( 1)
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_MSK                       (0x00040000)
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_MIN                       (0)
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_MAX                       (1) // 0x00000001
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_DEF                       (0x00000001)
  #define MCMISCS_WRITECFGCH0_rptchdqtxclkon_HSH                       (0x01243E2C)

  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_OFF                       (19)
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_WID                       ( 1)
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_MSK                       (0x00080000)
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_MIN                       (0)
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_MAX                       (1) // 0x00000001
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_DEF                       (0x00000001)
  #define MCMISCS_WRITECFGCH0_rptchdqrxclkon_HSH                       (0x01263E2C)

  #define MCMISCS_WRITECFGCH0_rptchrepclkon_OFF                        (20)
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_WID                        ( 1)
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_MSK                        (0x00100000)
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_MIN                        (0)
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_MAX                        (1) // 0x00000001
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_DEF                        (0x00000001)
  #define MCMISCS_WRITECFGCH0_rptchrepclkon_HSH                        (0x01283E2C)

  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_OFF                    (21)
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_WID                    ( 3)
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_MSK                    (0x00E00000)
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_MIN                    (0)
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_MAX                    (7) // 0x00000007
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_DEF                    (0x00000007)
  #define MCMISCS_WRITECFGCH0_cmdanlgengracecnt_HSH                    (0x032A3E2C)

  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_OFF                     (24)
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_WID                     ( 7)
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_MSK                     (0x7F000000)
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_MIN                     (0)
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_MAX                     (127) // 0x0000007F
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_DEF                     (0x0000003F)
  #define MCMISCS_WRITECFGCH0_txanlgengracecnt_HSH                     (0x07303E2C)

  #define MCMISCS_WRITECFGCH0_txdqfifordenperrankdeldis_OFF            (31)
  #define MCMISCS_WRITECFGCH0_txdqfifordenperrankdeldis_WID            ( 1)
  #define MCMISCS_WRITECFGCH0_txdqfifordenperrankdeldis_MSK            (0x80000000)
  #define MCMISCS_WRITECFGCH0_txdqfifordenperrankdeldis_MIN            (0)
  #define MCMISCS_WRITECFGCH0_txdqfifordenperrankdeldis_MAX            (1) // 0x00000001
  #define MCMISCS_WRITECFGCH0_txdqfifordenperrankdeldis_DEF            (0x00000000)
  #define MCMISCS_WRITECFGCH0_txdqfifordenperrankdeldis_HSH            (0x013E3E2C)

#define MCMISCS_WRITECFGCH1_REG                                        (0x00003E30)
//Duplicate of MCMISCS_WRITECFGCH0_REG

#define MCMISCS_WRITECFGCH2_REG                                        (0x00003E34)
//Duplicate of MCMISCS_WRITECFGCH0_REG

#define MCMISCS_WRITECFGCH3_REG                                        (0x00003E38)
//Duplicate of MCMISCS_WRITECFGCH0_REG

#define MCMISCS_WRITECFGCH4_REG                                        (0x00003E3C)
//Duplicate of MCMISCS_WRITECFGCH0_REG

#define MCMISCS_WRITECFGCH5_REG                                        (0x00003E40)
//Duplicate of MCMISCS_WRITECFGCH0_REG

#define MCMISCS_WRITECFGCH6_REG                                        (0x00003E44)
//Duplicate of MCMISCS_WRITECFGCH0_REG

#define MCMISCS_WRITECFGCH7_REG                                        (0x00003E48)
//Duplicate of MCMISCS_WRITECFGCH0_REG

#define MCMISCS_READCFGCH0_REG                                         (0x00003E4C)

  #define MCMISCS_READCFGCH0_spare_OFF                                 ( 0)
  #define MCMISCS_READCFGCH0_spare_WID                                 (12)
  #define MCMISCS_READCFGCH0_spare_MSK                                 (0x00000FFF)
  #define MCMISCS_READCFGCH0_spare_MIN                                 (0)
  #define MCMISCS_READCFGCH0_spare_MAX                                 (4095) // 0x00000FFF
  #define MCMISCS_READCFGCH0_spare_DEF                                 (0x00000000)
  #define MCMISCS_READCFGCH0_spare_HSH                                 (0x0C003E4C)

  #define MCMISCS_READCFGCH0_tcl4rcven_OFF                             (12)
  #define MCMISCS_READCFGCH0_tcl4rcven_WID                             ( 7)
  #define MCMISCS_READCFGCH0_tcl4rcven_MSK                             (0x0007F000)
  #define MCMISCS_READCFGCH0_tcl4rcven_MIN                             (0)
  #define MCMISCS_READCFGCH0_tcl4rcven_MAX                             (127) // 0x0000007F
  #define MCMISCS_READCFGCH0_tcl4rcven_DEF                             (0x00000000)
  #define MCMISCS_READCFGCH0_tcl4rcven_HSH                             (0x07183E4C)

  #define MCMISCS_READCFGCH0_tcl4rxdqfiforden_OFF                      (19)
  #define MCMISCS_READCFGCH0_tcl4rxdqfiforden_WID                      ( 7)
  #define MCMISCS_READCFGCH0_tcl4rxdqfiforden_MSK                      (0x03F80000)
  #define MCMISCS_READCFGCH0_tcl4rxdqfiforden_MIN                      (0)
  #define MCMISCS_READCFGCH0_tcl4rxdqfiforden_MAX                      (127) // 0x0000007F
  #define MCMISCS_READCFGCH0_tcl4rxdqfiforden_DEF                      (0x00000000)
  #define MCMISCS_READCFGCH0_tcl4rxdqfiforden_HSH                      (0x07263E4C)

  #define MCMISCS_READCFGCH0_rxdqdatavaliddclkdel_OFF                  (26)
  #define MCMISCS_READCFGCH0_rxdqdatavaliddclkdel_WID                  ( 5)
  #define MCMISCS_READCFGCH0_rxdqdatavaliddclkdel_MSK                  (0x7C000000)
  #define MCMISCS_READCFGCH0_rxdqdatavaliddclkdel_MIN                  (0)
  #define MCMISCS_READCFGCH0_rxdqdatavaliddclkdel_MAX                  (31) // 0x0000001F
  #define MCMISCS_READCFGCH0_rxdqdatavaliddclkdel_DEF                  (0x00000001)
  #define MCMISCS_READCFGCH0_rxdqdatavaliddclkdel_HSH                  (0x05343E4C)

  #define MCMISCS_READCFGCH0_rxdqdatavalidqclkdel_OFF                  (31)
  #define MCMISCS_READCFGCH0_rxdqdatavalidqclkdel_WID                  ( 1)
  #define MCMISCS_READCFGCH0_rxdqdatavalidqclkdel_MSK                  (0x80000000)
  #define MCMISCS_READCFGCH0_rxdqdatavalidqclkdel_MIN                  (0)
  #define MCMISCS_READCFGCH0_rxdqdatavalidqclkdel_MAX                  (1) // 0x00000001
  #define MCMISCS_READCFGCH0_rxdqdatavalidqclkdel_DEF                  (0x00000000)
  #define MCMISCS_READCFGCH0_rxdqdatavalidqclkdel_HSH                  (0x013E3E4C)

#define MCMISCS_READCFGCH1_REG                                         (0x00003E50)
//Duplicate of MCMISCS_READCFGCH0_REG

#define MCMISCS_READCFGCH2_REG                                         (0x00003E54)
//Duplicate of MCMISCS_READCFGCH0_REG

#define MCMISCS_READCFGCH3_REG                                         (0x00003E58)
//Duplicate of MCMISCS_READCFGCH0_REG

#define MCMISCS_READCFGCH4_REG                                         (0x00003E5C)
//Duplicate of MCMISCS_READCFGCH0_REG

#define MCMISCS_READCFGCH5_REG                                         (0x00003E60)
//Duplicate of MCMISCS_READCFGCH0_REG

#define MCMISCS_READCFGCH6_REG                                         (0x00003E64)
//Duplicate of MCMISCS_READCFGCH0_REG

#define MCMISCS_READCFGCH7_REG                                         (0x00003E68)
//Duplicate of MCMISCS_READCFGCH0_REG

#define MCMISCS_WRITECFGCH01_A0_REG                                    (0x00003E74)
#define MCMISCS_WRITECFGCH01_REG                                       (0x00003E80)

  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chadel_OFF             ( 0)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chadel_WID             ( 3)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chadel_MSK             (0x00000007)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chadel_MIN             (0)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chadel_MAX             (7) // 0x00000007
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chadel_DEF             (0x00000000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chadel_HSH             (0x03003E80)

  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chadel_OFF             ( 3)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chadel_WID             ( 3)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chadel_MSK             (0x00000038)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chadel_MIN             (0)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chadel_MAX             (7) // 0x00000007
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chadel_DEF             (0x00000000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chadel_HSH             (0x03063E80)

  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chadel_OFF             ( 6)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chadel_WID             ( 3)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chadel_MSK             (0x000001C0)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chadel_MIN             (0)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chadel_MAX             (7) // 0x00000007
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chadel_DEF             (0x00000000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chadel_HSH             (0x030C3E80)

  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chadel_OFF             ( 9)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chadel_WID             ( 3)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chadel_MSK             (0x00000E00)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chadel_MIN             (0)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chadel_MAX             (7) // 0x00000007
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chadel_DEF             (0x00000000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chadel_HSH             (0x03123E80)

  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chbdel_OFF             (12)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chbdel_WID             ( 3)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chbdel_MSK             (0x00007000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chbdel_MIN             (0)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chbdel_MAX             (7) // 0x00000007
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chbdel_DEF             (0x00000000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank0chbdel_HSH             (0x03183E80)

  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chbdel_OFF             (15)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chbdel_WID             ( 3)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chbdel_MSK             (0x00038000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chbdel_MIN             (0)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chbdel_MAX             (7) // 0x00000007
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chbdel_DEF             (0x00000000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank1chbdel_HSH             (0x031E3E80)

  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chbdel_OFF             (18)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chbdel_WID             ( 3)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chbdel_MSK             (0x001C0000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chbdel_MIN             (0)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chbdel_MAX             (7) // 0x00000007
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chbdel_DEF             (0x00000000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank2chbdel_HSH             (0x03243E80)

  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chbdel_OFF             (21)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chbdel_WID             ( 3)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chbdel_MSK             (0x00E00000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chbdel_MIN             (0)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chbdel_MAX             (7) // 0x00000007
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chbdel_DEF             (0x00000000)
  #define MCMISCS_WRITECFGCH01_txdqfifordenrank3chbdel_HSH             (0x032A3E80)

  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_cha_OFF          (24)
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_cha_WID          ( 1)
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_cha_MSK          (0x01000000)
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_cha_MIN          (0)
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_cha_MAX          (1) // 0x00000001
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_cha_DEF          (0x00000000)
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_cha_HSH          (0x01303E80)

  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_chb_OFF          (25)
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_chb_WID          ( 1)
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_chb_MSK          (0x02000000)
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_chb_MIN          (0)
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_chb_MAX          (1) // 0x00000001
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_chb_DEF          (0x00000000)
  #define MCMISCS_WRITECFGCH01_tx_analog_clk_gate_dis_chb_HSH          (0x01323E80)

  #define MCMISCS_WRITECFGCH01_spare_OFF                               (26)
  #define MCMISCS_WRITECFGCH01_spare_WID                               ( 6)
  #define MCMISCS_WRITECFGCH01_spare_MSK                               (0xFC000000)
  #define MCMISCS_WRITECFGCH01_spare_MIN                               (0)
  #define MCMISCS_WRITECFGCH01_spare_MAX                               (63) // 0x0000003F
  #define MCMISCS_WRITECFGCH01_spare_DEF                               (0x00000000)
  #define MCMISCS_WRITECFGCH01_spare_HSH                               (0x06343E80)

#define MCMISCS_WRITECFGCH23_A0_REG                                    (0x00003E80)
#define MCMISCS_WRITECFGCH23_REG                                       (0x00003E84)
//Duplicate of MCMISCS_WRITECFGCH01_REG

#define MCMISCS_WRITECFGCH45_A0_REG                                    (0x00003E84)
#define MCMISCS_WRITECFGCH45_REG                                       (0x00003E88)
//Duplicate of MCMISCS_WRITECFGCH01_REG

#define MCMISCS_WRITECFGCH67_A0_REG                                    (0x00003E88)
#define MCMISCS_WRITECFGCH67_REG                                       (0x00003E8C)
//Duplicate of MCMISCS_WRITECFGCH01_REG

#define MCMISCS_READCFGCH01_A0_REG                                     (0x00003E8C)
#define MCMISCS_READCFGCH01_REG                                        (0x00003E90)

  #define MCMISCS_READCFGCH01_rcvenrank0chadel_OFF                     ( 0)
  #define MCMISCS_READCFGCH01_rcvenrank0chadel_WID                     ( 3)
  #define MCMISCS_READCFGCH01_rcvenrank0chadel_MSK                     (0x00000007)
  #define MCMISCS_READCFGCH01_rcvenrank0chadel_MIN                     (0)
  #define MCMISCS_READCFGCH01_rcvenrank0chadel_MAX                     (7) // 0x00000007
  #define MCMISCS_READCFGCH01_rcvenrank0chadel_DEF                     (0x00000000)
  #define MCMISCS_READCFGCH01_rcvenrank0chadel_HSH                     (0x03003E90)

  #define MCMISCS_READCFGCH01_rcvenrank1chadel_OFF                     ( 3)
  #define MCMISCS_READCFGCH01_rcvenrank1chadel_WID                     ( 3)
  #define MCMISCS_READCFGCH01_rcvenrank1chadel_MSK                     (0x00000038)
  #define MCMISCS_READCFGCH01_rcvenrank1chadel_MIN                     (0)
  #define MCMISCS_READCFGCH01_rcvenrank1chadel_MAX                     (7) // 0x00000007
  #define MCMISCS_READCFGCH01_rcvenrank1chadel_DEF                     (0x00000000)
  #define MCMISCS_READCFGCH01_rcvenrank1chadel_HSH                     (0x03063E90)

  #define MCMISCS_READCFGCH01_rcvenrank2chadel_OFF                     ( 6)
  #define MCMISCS_READCFGCH01_rcvenrank2chadel_WID                     ( 3)
  #define MCMISCS_READCFGCH01_rcvenrank2chadel_MSK                     (0x000001C0)
  #define MCMISCS_READCFGCH01_rcvenrank2chadel_MIN                     (0)
  #define MCMISCS_READCFGCH01_rcvenrank2chadel_MAX                     (7) // 0x00000007
  #define MCMISCS_READCFGCH01_rcvenrank2chadel_DEF                     (0x00000000)
  #define MCMISCS_READCFGCH01_rcvenrank2chadel_HSH                     (0x030C3E90)

  #define MCMISCS_READCFGCH01_rcvenrank3chadel_OFF                     ( 9)
  #define MCMISCS_READCFGCH01_rcvenrank3chadel_WID                     ( 3)
  #define MCMISCS_READCFGCH01_rcvenrank3chadel_MSK                     (0x00000E00)
  #define MCMISCS_READCFGCH01_rcvenrank3chadel_MIN                     (0)
  #define MCMISCS_READCFGCH01_rcvenrank3chadel_MAX                     (7) // 0x00000007
  #define MCMISCS_READCFGCH01_rcvenrank3chadel_DEF                     (0x00000000)
  #define MCMISCS_READCFGCH01_rcvenrank3chadel_HSH                     (0x03123E90)

  #define MCMISCS_READCFGCH01_rcvenrank0chbdel_OFF                     (12)
  #define MCMISCS_READCFGCH01_rcvenrank0chbdel_WID                     ( 3)
  #define MCMISCS_READCFGCH01_rcvenrank0chbdel_MSK                     (0x00007000)
  #define MCMISCS_READCFGCH01_rcvenrank0chbdel_MIN                     (0)
  #define MCMISCS_READCFGCH01_rcvenrank0chbdel_MAX                     (7) // 0x00000007
  #define MCMISCS_READCFGCH01_rcvenrank0chbdel_DEF                     (0x00000000)
  #define MCMISCS_READCFGCH01_rcvenrank0chbdel_HSH                     (0x03183E90)

  #define MCMISCS_READCFGCH01_rcvenrank1chbdel_OFF                     (15)
  #define MCMISCS_READCFGCH01_rcvenrank1chbdel_WID                     ( 3)
  #define MCMISCS_READCFGCH01_rcvenrank1chbdel_MSK                     (0x00038000)
  #define MCMISCS_READCFGCH01_rcvenrank1chbdel_MIN                     (0)
  #define MCMISCS_READCFGCH01_rcvenrank1chbdel_MAX                     (7) // 0x00000007
  #define MCMISCS_READCFGCH01_rcvenrank1chbdel_DEF                     (0x00000000)
  #define MCMISCS_READCFGCH01_rcvenrank1chbdel_HSH                     (0x031E3E90)

  #define MCMISCS_READCFGCH01_rcvenrank2chbdel_OFF                     (18)
  #define MCMISCS_READCFGCH01_rcvenrank2chbdel_WID                     ( 3)
  #define MCMISCS_READCFGCH01_rcvenrank2chbdel_MSK                     (0x001C0000)
  #define MCMISCS_READCFGCH01_rcvenrank2chbdel_MIN                     (0)
  #define MCMISCS_READCFGCH01_rcvenrank2chbdel_MAX                     (7) // 0x00000007
  #define MCMISCS_READCFGCH01_rcvenrank2chbdel_DEF                     (0x00000000)
  #define MCMISCS_READCFGCH01_rcvenrank2chbdel_HSH                     (0x03243E90)

  #define MCMISCS_READCFGCH01_rcvenrank3chbdel_OFF                     (21)
  #define MCMISCS_READCFGCH01_rcvenrank3chbdel_WID                     ( 3)
  #define MCMISCS_READCFGCH01_rcvenrank3chbdel_MSK                     (0x00E00000)
  #define MCMISCS_READCFGCH01_rcvenrank3chbdel_MIN                     (0)
  #define MCMISCS_READCFGCH01_rcvenrank3chbdel_MAX                     (7) // 0x00000007
  #define MCMISCS_READCFGCH01_rcvenrank3chbdel_DEF                     (0x00000000)
  #define MCMISCS_READCFGCH01_rcvenrank3chbdel_HSH                     (0x032A3E90)

  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_cha_OFF           (24)
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_cha_WID           ( 1)
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_cha_MSK           (0x01000000)
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_cha_MIN           (0)
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_cha_MAX           (1) // 0x00000001
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_cha_DEF           (0x00000000)
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_cha_HSH           (0x01303E90)

  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_chb_OFF           (25)
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_chb_WID           ( 1)
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_chb_MSK           (0x02000000)
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_chb_MIN           (0)
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_chb_MAX           (1) // 0x00000001
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_chb_DEF           (0x00000000)
  #define MCMISCS_READCFGCH01_rx_analog_clk_gate_dis_chb_HSH           (0x01323E90)

  #define MCMISCS_READCFGCH01_spare_OFF                                (26)
  #define MCMISCS_READCFGCH01_spare_WID                                ( 6)
  #define MCMISCS_READCFGCH01_spare_MSK                                (0xFC000000)
  #define MCMISCS_READCFGCH01_spare_MIN                                (0)
  #define MCMISCS_READCFGCH01_spare_MAX                                (63) // 0x0000003F
  #define MCMISCS_READCFGCH01_spare_DEF                                (0x00000000)
  #define MCMISCS_READCFGCH01_spare_HSH                                (0x06343E90)

#define MCMISCS_READCFGCH23_A0_REG                                     (0x00003E90)
#define MCMISCS_READCFGCH23_REG                                        (0x00003E94)
//Duplicate of MCMISCS_READCFGCH01_REG

#define MCMISCS_READCFGCH45_A0_REG                                     (0x00003E94)
#define MCMISCS_READCFGCH45_REG                                        (0x00003E98)
//Duplicate of MCMISCS_READCFGCH01_REG

#define MCMISCS_READCFGCH67_A0_REG                                     (0x00003E98)
#define MCMISCS_READCFGCH67_REG                                        (0x00003E9C)
//Duplicate of MCMISCS_READCFGCH01_REG

#define DDRSCRAM_CR_DDRMISCCONTROL7_A0_REG                             (0x00003E9C)
#define DDRSCRAM_CR_DDRMISCCONTROL7_REG                                (0x00003EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_txburstlen_OFF                   ( 0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_txburstlen_WID                   ( 5)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_txburstlen_MSK                   (0x0000001F)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_txburstlen_MIN                   (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_txburstlen_MAX                   (31) // 0x0000001F
  #define DDRSCRAM_CR_DDRMISCCONTROL7_txburstlen_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_txburstlen_HSH                   (0x05003EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_rxburstlen_OFF                   ( 5)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_rxburstlen_WID                   ( 5)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_rxburstlen_MSK                   (0x000003E0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_rxburstlen_MIN                   (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_rxburstlen_MAX                   (31) // 0x0000001F
  #define DDRSCRAM_CR_DDRMISCCONTROL7_rxburstlen_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_rxburstlen_HSH                   (0x050A3EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_cmdduration_OFF                  (10)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_cmdduration_WID                  ( 3)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_cmdduration_MSK                  (0x00001C00)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_cmdduration_MIN                  (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_cmdduration_MAX                  (7) // 0x00000007
  #define DDRSCRAM_CR_DDRMISCCONTROL7_cmdduration_DEF                  (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_cmdduration_HSH                  (0x03143EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_A0_OFF             (13)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_A0_WID             ( 2)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_A0_MSK             (0x00006000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_A0_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_A0_MAX             (3) // 0x00000003
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_A0_DEF             (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_A0_HSH             (0x021A3E9C)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_A0_OFF                   (15)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_A0_WID                   ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_A0_MSK                   (0x00008000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_A0_MIN                   (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_A0_MAX                   (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_A0_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_A0_HSH                   (0x011E3E9C)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_A0_OFF        (16)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_A0_WID        ( 3)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_A0_MSK        (0x00070000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_A0_MIN        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_A0_MAX        (7) // 0x00000007
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_A0_DEF        (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_A0_HSH        (0x03203E9C)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_A0_OFF        (19)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_A0_WID        ( 3)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_A0_MSK        (0x00380000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_A0_MIN        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_A0_MAX        (7) // 0x00000007
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_A0_DEF        (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_A0_HSH        (0x03263E9C)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_A0_OFF       (22)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_A0_WID       ( 2)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_A0_MSK       (0x00C00000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_A0_MIN       (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_A0_MAX       (3) // 0x00000003
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_A0_DEF       (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_A0_HSH       (0x022C3E9C)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_A0_OFF       (24)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_A0_WID       ( 2)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_A0_MSK       (0x03000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_A0_MIN       (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_A0_MAX       (3) // 0x00000003
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_A0_DEF       (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_A0_HSH       (0x02303E9C)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_A0_OFF                 (26)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_A0_WID                 ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_A0_MSK                 (0x04000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_A0_MIN                 (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_A0_MAX                 (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_A0_DEF                 (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_A0_HSH                 (0x01343E9C)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_A0_OFF           (27)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_A0_WID           ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_A0_MSK           (0x08000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_A0_MIN           (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_A0_MAX           (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_A0_DEF           (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_A0_HSH           (0x01363E9C)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_A0_OFF                        (28)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_A0_WID                        ( 4)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_A0_MSK                        (0xF0000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_A0_MIN                        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_A0_MAX                        (15) // 0x0000000F
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_A0_DEF                        (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_A0_HSH                        (0x04383E9C)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_OFF             (13)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_WID             ( 3)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_MSK             (0x0000E000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_MIN             (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_MAX             (7) // 0x00000007
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_DEF             (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_datainvertnibble_HSH             (0x031A3EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_OFF                   (16)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_WID                   ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_MSK                   (0x00010000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_MIN                   (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_MAX                   (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_DEF                   (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_run1stcomp_HSH                   (0x01203EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_OFF        (17)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_WID        ( 3)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_MSK        (0x000E0000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_MIN        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_MAX        (7) // 0x00000007
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_DEF        (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_8to1_HSH        (0x03223EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_OFF        (20)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_WID        ( 3)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_MSK        (0x00700000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_MIN        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_MAX        (7) // 0x00000007
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_DEF        (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_8to1_HSH        (0x03283EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_OFF       (23)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_WID       ( 2)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_MSK       (0x01800000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_MIN       (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_MAX       (3) // 0x00000003
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_DEF       (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel1_32to8_HSH       (0x022E3EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_OFF       (25)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_WID       ( 2)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_MSK       (0x06000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_MIN       (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_MAX       (3) // 0x00000003
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_DEF       (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrcrobsvisasel0_32to8_HSH       (0x02323EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_OFF                 (27)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_WID                 ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_MSK                 (0x08000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_MIN                 (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_MAX                 (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_DEF                 (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_ddrvisaorpll_HSH                 (0x01363EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_OFF           (28)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_WID           ( 1)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_MSK           (0x10000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_MIN           (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_MAX           (1) // 0x00000001
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_DEF           (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_dclkrst_rptr_plus1_HSH           (0x01383EA0)

  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_OFF                        (29)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_WID                        ( 3)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_MSK                        (0xE0000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_MIN                        (0)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_MAX                        (7) // 0x00000007
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_DEF                        (0x00000000)
  #define DDRSCRAM_CR_DDRMISCCONTROL7_spare_HSH                        (0x033A3EA0)

#define MCMISCS_SYNAUTOTRDYSTART_A0_REG                                (0x00003EA0)
#define MCMISCS_SYNAUTOTRDYSTART_REG                                   (0x00003EA4)

  #define MCMISCS_SYNAUTOTRDYSTART_forceautotrdy_OFF                   ( 0)
  #define MCMISCS_SYNAUTOTRDYSTART_forceautotrdy_WID                   ( 1)
  #define MCMISCS_SYNAUTOTRDYSTART_forceautotrdy_MSK                   (0x00000001)
  #define MCMISCS_SYNAUTOTRDYSTART_forceautotrdy_MIN                   (0)
  #define MCMISCS_SYNAUTOTRDYSTART_forceautotrdy_MAX                   (1) // 0x00000001
  #define MCMISCS_SYNAUTOTRDYSTART_forceautotrdy_DEF                   (0x00000000)
  #define MCMISCS_SYNAUTOTRDYSTART_forceautotrdy_HSH                   (0x01003EA4)

  #define MCMISCS_SYNAUTOTRDYSTART_spare_OFF                           ( 1)
  #define MCMISCS_SYNAUTOTRDYSTART_spare_WID                           (31)
  #define MCMISCS_SYNAUTOTRDYSTART_spare_MSK                           (0xFFFFFFFE)
  #define MCMISCS_SYNAUTOTRDYSTART_spare_MIN                           (0)
  #define MCMISCS_SYNAUTOTRDYSTART_spare_MAX                           (2147483647) // 0x7FFFFFFF
  #define MCMISCS_SYNAUTOTRDYSTART_spare_DEF                           (0x00000000)
  #define MCMISCS_SYNAUTOTRDYSTART_spare_HSH                           (0x1F023EA4)

#define MCMISCS_SYNAUTOTRDYEND_A0_REG                                  (0x00003EA4)
#define MCMISCS_SYNAUTOTRDYEND_REG                                     (0x00003EA8)

  #define MCMISCS_SYNAUTOTRDYEND_disableautotrdy_OFF                   ( 0)
  #define MCMISCS_SYNAUTOTRDYEND_disableautotrdy_WID                   ( 1)
  #define MCMISCS_SYNAUTOTRDYEND_disableautotrdy_MSK                   (0x00000001)
  #define MCMISCS_SYNAUTOTRDYEND_disableautotrdy_MIN                   (0)
  #define MCMISCS_SYNAUTOTRDYEND_disableautotrdy_MAX                   (1) // 0x00000001
  #define MCMISCS_SYNAUTOTRDYEND_disableautotrdy_DEF                   (0x00000000)
  #define MCMISCS_SYNAUTOTRDYEND_disableautotrdy_HSH                   (0x01003EA8)

  #define MCMISCS_SYNAUTOTRDYEND_spare_OFF                             ( 1)
  #define MCMISCS_SYNAUTOTRDYEND_spare_WID                             (31)
  #define MCMISCS_SYNAUTOTRDYEND_spare_MSK                             (0xFFFFFFFE)
  #define MCMISCS_SYNAUTOTRDYEND_spare_MIN                             (0)
  #define MCMISCS_SYNAUTOTRDYEND_spare_MAX                             (2147483647) // 0x7FFFFFFF
  #define MCMISCS_SYNAUTOTRDYEND_spare_DEF                             (0x00000000)
  #define MCMISCS_SYNAUTOTRDYEND_spare_HSH                             (0x1F023EA8)

#define MCMISCS_RXDQFIFORDENCH01_A0_REG                                (0x00003EA8)
#define MCMISCS_RXDQFIFORDENCH01_REG                                   (0x00003EAC)

  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_OFF         ( 0)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_WID         ( 4)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_MSK         (0x0000000F)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_MIN         (0)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_MAX         (15) // 0x0000000F
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_DEF         (0x00000000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_HSH         (0x04003EAC)

  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chadel_OFF         ( 4)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chadel_WID         ( 4)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chadel_MSK         (0x000000F0)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chadel_MIN         (0)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chadel_MAX         (15) // 0x0000000F
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chadel_DEF         (0x00000000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chadel_HSH         (0x04083EAC)

  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chadel_OFF         ( 8)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chadel_WID         ( 4)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chadel_MSK         (0x00000F00)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chadel_MIN         (0)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chadel_MAX         (15) // 0x0000000F
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chadel_DEF         (0x00000000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chadel_HSH         (0x04103EAC)

  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chadel_OFF         (12)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chadel_WID         ( 4)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chadel_MSK         (0x0000F000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chadel_MIN         (0)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chadel_MAX         (15) // 0x0000000F
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chadel_DEF         (0x00000000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chadel_HSH         (0x04183EAC)

  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chbdel_OFF         (16)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chbdel_WID         ( 4)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chbdel_MSK         (0x000F0000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chbdel_MIN         (0)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chbdel_MAX         (15) // 0x0000000F
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chbdel_DEF         (0x00000000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chbdel_HSH         (0x04203EAC)

  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chbdel_OFF         (20)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chbdel_WID         ( 4)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chbdel_MSK         (0x00F00000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chbdel_MIN         (0)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chbdel_MAX         (15) // 0x0000000F
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chbdel_DEF         (0x00000000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chbdel_HSH         (0x04283EAC)

  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chbdel_OFF         (24)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chbdel_WID         ( 4)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chbdel_MSK         (0x0F000000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chbdel_MIN         (0)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chbdel_MAX         (15) // 0x0000000F
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chbdel_DEF         (0x00000000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chbdel_HSH         (0x04303EAC)

  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chbdel_OFF         (28)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chbdel_WID         ( 4)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chbdel_MSK         (0xF0000000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chbdel_MIN         (0)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chbdel_MAX         (15) // 0x0000000F
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chbdel_DEF         (0x00000000)
  #define MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chbdel_HSH         (0x04383EAC)

#define MCMISCS_RXDQFIFORDENCH23_A0_REG                                (0x00003EAC)
#define MCMISCS_RXDQFIFORDENCH23_REG                                   (0x00003EB0)
//Duplicate of MCMISCS_RXDQFIFORDENCH01_REG

#define MCMISCS_RXDQFIFORDENCH45_A0_REG                                (0x00003EB0)
#define MCMISCS_RXDQFIFORDENCH45_REG                                   (0x00003EB4)
//Duplicate of MCMISCS_RXDQFIFORDENCH01_REG

#define MCMISCS_RXDQFIFORDENCH67_A0_REG                                (0x00003EB4)
#define MCMISCS_RXDQFIFORDENCH67_REG                                   (0x00003EB8)
//Duplicate of MCMISCS_RXDQFIFORDENCH01_REG

#define MCMISCS_SPINEGATING_A0_REG                                     (0x00003EB8)
#define MCMISCS_SPINEGATING_REG                                        (0x00003EBC)

  #define MCMISCS_SPINEGATING_enablespinegate_OFF                      ( 0)
  #define MCMISCS_SPINEGATING_enablespinegate_WID                      ( 1)
  #define MCMISCS_SPINEGATING_enablespinegate_MSK                      (0x00000001)
  #define MCMISCS_SPINEGATING_enablespinegate_MIN                      (0)
  #define MCMISCS_SPINEGATING_enablespinegate_MAX                      (1) // 0x00000001
  #define MCMISCS_SPINEGATING_enablespinegate_DEF                      (0x00000000)
  #define MCMISCS_SPINEGATING_enablespinegate_HSH                      (0x01003EBC)

  #define MCMISCS_SPINEGATING_sleepcycles_OFF                          ( 1)
  #define MCMISCS_SPINEGATING_sleepcycles_WID                          ( 3)
  #define MCMISCS_SPINEGATING_sleepcycles_MSK                          (0x0000000E)
  #define MCMISCS_SPINEGATING_sleepcycles_MIN                          (0)
  #define MCMISCS_SPINEGATING_sleepcycles_MAX                          (7) // 0x00000007
  #define MCMISCS_SPINEGATING_sleepcycles_DEF                          (0x00000000)
  #define MCMISCS_SPINEGATING_sleepcycles_HSH                          (0x03023EBC)

  #define MCMISCS_SPINEGATING_awakecycles_OFF                          ( 4)
  #define MCMISCS_SPINEGATING_awakecycles_WID                          ( 2)
  #define MCMISCS_SPINEGATING_awakecycles_MSK                          (0x00000030)
  #define MCMISCS_SPINEGATING_awakecycles_MIN                          (0)
  #define MCMISCS_SPINEGATING_awakecycles_MAX                          (3) // 0x00000003
  #define MCMISCS_SPINEGATING_awakecycles_DEF                          (0x00000000)
  #define MCMISCS_SPINEGATING_awakecycles_HSH                          (0x02083EBC)

  #define MCMISCS_SPINEGATING_enhiphase_OFF                            ( 6)
  #define MCMISCS_SPINEGATING_enhiphase_WID                            ( 2)
  #define MCMISCS_SPINEGATING_enhiphase_MSK                            (0x000000C0)
  #define MCMISCS_SPINEGATING_enhiphase_MIN                            (0)
  #define MCMISCS_SPINEGATING_enhiphase_MAX                            (3) // 0x00000003
  #define MCMISCS_SPINEGATING_enhiphase_DEF                            (0x00000000)
  #define MCMISCS_SPINEGATING_enhiphase_HSH                            (0x020C3EBC)

  #define MCMISCS_SPINEGATING_gracelimitentry_OFF                      ( 8)
  #define MCMISCS_SPINEGATING_gracelimitentry_WID                      ( 3)
  #define MCMISCS_SPINEGATING_gracelimitentry_MSK                      (0x00000700)
  #define MCMISCS_SPINEGATING_gracelimitentry_MIN                      (0)
  #define MCMISCS_SPINEGATING_gracelimitentry_MAX                      (7) // 0x00000007
  #define MCMISCS_SPINEGATING_gracelimitentry_DEF                      (0x00000000)
  #define MCMISCS_SPINEGATING_gracelimitentry_HSH                      (0x03103EBC)

  #define MCMISCS_SPINEGATING_spare_OFF                                (11)
  #define MCMISCS_SPINEGATING_spare_WID                                (21)
  #define MCMISCS_SPINEGATING_spare_MSK                                (0xFFFFF800)
  #define MCMISCS_SPINEGATING_spare_MIN                                (0)
  #define MCMISCS_SPINEGATING_spare_MAX                                (2097151) // 0x001FFFFF
  #define MCMISCS_SPINEGATING_spare_DEF                                (0x00000000)
  #define MCMISCS_SPINEGATING_spare_HSH                                (0x15163EBC)

#define MCMISCS_DDRWCKCONTROL_A0_REG                                   (0x00003EBC)
#define MCMISCS_DDRWCKCONTROL_REG                                      (0x00003EC0)

  #define MCMISCS_DDRWCKCONTROL_cas2rdwck_OFF                          ( 0)
  #define MCMISCS_DDRWCKCONTROL_cas2rdwck_WID                          ( 7)
  #define MCMISCS_DDRWCKCONTROL_cas2rdwck_MSK                          (0x0000007F)
  #define MCMISCS_DDRWCKCONTROL_cas2rdwck_MIN                          (0)
  #define MCMISCS_DDRWCKCONTROL_cas2rdwck_MAX                          (127) // 0x0000007F
  #define MCMISCS_DDRWCKCONTROL_cas2rdwck_DEF                          (0x00000000)
  #define MCMISCS_DDRWCKCONTROL_cas2rdwck_HSH                          (0x07003EC0)

  #define MCMISCS_DDRWCKCONTROL_cas2wrwck_OFF                          ( 7)
  #define MCMISCS_DDRWCKCONTROL_cas2wrwck_WID                          ( 7)
  #define MCMISCS_DDRWCKCONTROL_cas2wrwck_MSK                          (0x00003F80)
  #define MCMISCS_DDRWCKCONTROL_cas2wrwck_MIN                          (0)
  #define MCMISCS_DDRWCKCONTROL_cas2wrwck_MAX                          (127) // 0x0000007F
  #define MCMISCS_DDRWCKCONTROL_cas2wrwck_DEF                          (0x00000000)
  #define MCMISCS_DDRWCKCONTROL_cas2wrwck_HSH                          (0x070E3EC0)

  #define MCMISCS_DDRWCKCONTROL_tWckHalfRate_OFF                       (14)
  #define MCMISCS_DDRWCKCONTROL_tWckHalfRate_WID                       ( 3)
  #define MCMISCS_DDRWCKCONTROL_tWckHalfRate_MSK                       (0x0001C000)
  #define MCMISCS_DDRWCKCONTROL_tWckHalfRate_MIN                       (0)
  #define MCMISCS_DDRWCKCONTROL_tWckHalfRate_MAX                       (7) // 0x00000007
  #define MCMISCS_DDRWCKCONTROL_tWckHalfRate_DEF                       (0x00000000)
  #define MCMISCS_DDRWCKCONTROL_tWckHalfRate_HSH                       (0x031C3EC0)

  #define MCMISCS_DDRWCKCONTROL_tWckPre_OFF                            (17)
  #define MCMISCS_DDRWCKCONTROL_tWckPre_WID                            ( 5)
  #define MCMISCS_DDRWCKCONTROL_tWckPre_MSK                            (0x003E0000)
  #define MCMISCS_DDRWCKCONTROL_tWckPre_MIN                            (0)
  #define MCMISCS_DDRWCKCONTROL_tWckPre_MAX                            (31) // 0x0000001F
  #define MCMISCS_DDRWCKCONTROL_tWckPre_DEF                            (0x00000000)
  #define MCMISCS_DDRWCKCONTROL_tWckPre_HSH                            (0x05223EC0)

  #define MCMISCS_DDRWCKCONTROL_TrainWCkPulse_OFF                      (22)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkPulse_WID                      ( 1)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkPulse_MSK                      (0x00400000)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkPulse_MIN                      (0)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkPulse_MAX                      (1) // 0x00000001
  #define MCMISCS_DDRWCKCONTROL_TrainWCkPulse_DEF                      (0x00000000)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkPulse_HSH                      (0x012C3EC0)

  #define MCMISCS_DDRWCKCONTROL_Lp5Mode_OFF                            (23)
  #define MCMISCS_DDRWCKCONTROL_Lp5Mode_WID                            ( 1)
  #define MCMISCS_DDRWCKCONTROL_Lp5Mode_MSK                            (0x00800000)
  #define MCMISCS_DDRWCKCONTROL_Lp5Mode_MIN                            (0)
  #define MCMISCS_DDRWCKCONTROL_Lp5Mode_MAX                            (1) // 0x00000001
  #define MCMISCS_DDRWCKCONTROL_Lp5Mode_DEF                            (0x00000000)
  #define MCMISCS_DDRWCKCONTROL_Lp5Mode_HSH                            (0x012E3EC0)

  #define MCMISCS_DDRWCKCONTROL_WCkDiffLowInIdle_OFF                   (24)
  #define MCMISCS_DDRWCKCONTROL_WCkDiffLowInIdle_WID                   ( 1)
  #define MCMISCS_DDRWCKCONTROL_WCkDiffLowInIdle_MSK                   (0x01000000)
  #define MCMISCS_DDRWCKCONTROL_WCkDiffLowInIdle_MIN                   (0)
  #define MCMISCS_DDRWCKCONTROL_WCkDiffLowInIdle_MAX                   (1) // 0x00000001
  #define MCMISCS_DDRWCKCONTROL_WCkDiffLowInIdle_DEF                   (0x00000000)
  #define MCMISCS_DDRWCKCONTROL_WCkDiffLowInIdle_HSH                   (0x01303EC0)

  #define MCMISCS_DDRWCKCONTROL_TrainWCkBL_OFF                         (25)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkBL_WID                         ( 4)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkBL_MSK                         (0x1E000000)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkBL_MIN                         (0)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkBL_MAX                         (15) // 0x0000000F
  #define MCMISCS_DDRWCKCONTROL_TrainWCkBL_DEF                         (0x00000000)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkBL_HSH                         (0x04323EC0)

  #define MCMISCS_DDRWCKCONTROL_TrainWCkMask_OFF                       (29)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkMask_WID                       ( 3)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkMask_MSK                       (0xE0000000)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkMask_MIN                       (0)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkMask_MAX                       (7) // 0x00000007
  #define MCMISCS_DDRWCKCONTROL_TrainWCkMask_DEF                       (0x00000000)
  #define MCMISCS_DDRWCKCONTROL_TrainWCkMask_HSH                       (0x033A3EC0)

#define MCMISCS_DDRWCKCONTROL1_A0_REG                                  (0x00003EC0)
#define MCMISCS_DDRWCKCONTROL1_REG                                     (0x00003EC4)

  #define MCMISCS_DDRWCKCONTROL1_cas2fswck_OFF                         ( 0)
  #define MCMISCS_DDRWCKCONTROL1_cas2fswck_WID                         ( 7)
  #define MCMISCS_DDRWCKCONTROL1_cas2fswck_MSK                         (0x0000007F)
  #define MCMISCS_DDRWCKCONTROL1_cas2fswck_MIN                         (0)
  #define MCMISCS_DDRWCKCONTROL1_cas2fswck_MAX                         (127) // 0x0000007F
  #define MCMISCS_DDRWCKCONTROL1_cas2fswck_DEF                         (0x00000000)
  #define MCMISCS_DDRWCKCONTROL1_cas2fswck_HSH                         (0x07003EC4)

  #define MCMISCS_DDRWCKCONTROL1_TrainWCkSyncRatio_OFF                 ( 7)
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkSyncRatio_WID                 ( 2)
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkSyncRatio_MSK                 (0x00000180)
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkSyncRatio_MIN                 (0)
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkSyncRatio_MAX                 (3) // 0x00000003
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkSyncRatio_DEF                 (0x00000000)
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkSyncRatio_HSH                 (0x020E3EC4)

  #define MCMISCS_DDRWCKCONTROL1_TrainWCkEn_OFF                        ( 9)
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkEn_WID                        ( 1)
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkEn_MSK                        (0x00000200)
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkEn_MIN                        (0)
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkEn_MAX                        (1) // 0x00000001
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkEn_DEF                        (0x00000000)
  #define MCMISCS_DDRWCKCONTROL1_TrainWCkEn_HSH                        (0x01123EC4)

  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_en_OFF                    (10)
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_en_WID                    ( 1)
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_en_MSK                    (0x00000400)
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_en_MIN                    (0)
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_en_MAX                    (1) // 0x00000001
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_en_DEF                    (0x00000000)
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_en_HSH                    (0x01143EC4)

  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_type_g4_OFF               (11)
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_type_g4_WID               ( 1)
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_type_g4_MSK               (0x00000800)
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_type_g4_MIN               (0)
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_type_g4_MAX               (1) // 0x00000001
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_type_g4_DEF               (0x00000000)
  #define MCMISCS_DDRWCKCONTROL1_ddr_2n_mode_type_g4_HSH               (0x01163EC4)

  #define MCMISCS_DDRWCKCONTROL1_scramCheckEcc_scramClkGateOvrd_OFF    (12)
  #define MCMISCS_DDRWCKCONTROL1_scramCheckEcc_scramClkGateOvrd_WID    ( 2)
  #define MCMISCS_DDRWCKCONTROL1_scramCheckEcc_scramClkGateOvrd_MSK    (0x00003000)
  #define MCMISCS_DDRWCKCONTROL1_scramCheckEcc_scramClkGateOvrd_MIN    (0)
  #define MCMISCS_DDRWCKCONTROL1_scramCheckEcc_scramClkGateOvrd_MAX    (3) // 0x00000003
  #define MCMISCS_DDRWCKCONTROL1_scramCheckEcc_scramClkGateOvrd_DEF    (0x00000000)
  #define MCMISCS_DDRWCKCONTROL1_scramCheckEcc_scramClkGateOvrd_HSH    (0x02183EC4)

  #define MCMISCS_DDRWCKCONTROL1_spare_OFF                             (12)
  #define MCMISCS_DDRWCKCONTROL1_spare_WID                             ( 2)
  #define MCMISCS_DDRWCKCONTROL1_spare_MSK                             (0x00003000)
  #define MCMISCS_DDRWCKCONTROL1_spare_MIN                             (0)
  #define MCMISCS_DDRWCKCONTROL1_spare_MAX                             (3) // 0x00000003
  #define MCMISCS_DDRWCKCONTROL1_spare_DEF                             (0x00000000)
  #define MCMISCS_DDRWCKCONTROL1_spare_HSH                             (0x02183EC4)

  #define MCMISCS_DDRWCKCONTROL1_lp_bank_mode_OFF                      (14)
  #define MCMISCS_DDRWCKCONTROL1_lp_bank_mode_WID                      ( 2)
  #define MCMISCS_DDRWCKCONTROL1_lp_bank_mode_MSK                      (0x0000C000)
  #define MCMISCS_DDRWCKCONTROL1_lp_bank_mode_MIN                      (0)
  #define MCMISCS_DDRWCKCONTROL1_lp_bank_mode_MAX                      (3) // 0x00000003
  #define MCMISCS_DDRWCKCONTROL1_lp_bank_mode_DEF                      (0x00000000)
  #define MCMISCS_DDRWCKCONTROL1_lp_bank_mode_HSH                      (0x021C3EC4)

  #define MCMISCS_DDRWCKCONTROL1_wrcmd2dummy_OFF                       (16)
  #define MCMISCS_DDRWCKCONTROL1_wrcmd2dummy_WID                       ( 4)
  #define MCMISCS_DDRWCKCONTROL1_wrcmd2dummy_MSK                       (0x000F0000)
  #define MCMISCS_DDRWCKCONTROL1_wrcmd2dummy_MIN                       (0)
  #define MCMISCS_DDRWCKCONTROL1_wrcmd2dummy_MAX                       (15) // 0x0000000F
  #define MCMISCS_DDRWCKCONTROL1_wrcmd2dummy_DEF                       (0x00000008)
  #define MCMISCS_DDRWCKCONTROL1_wrcmd2dummy_HSH                       (0x04203EC4)

  #define MCMISCS_DDRWCKCONTROL1_wrdummy2wrdummy_OFF                   (20)
  #define MCMISCS_DDRWCKCONTROL1_wrdummy2wrdummy_WID                   ( 4)
  #define MCMISCS_DDRWCKCONTROL1_wrdummy2wrdummy_MSK                   (0x00F00000)
  #define MCMISCS_DDRWCKCONTROL1_wrdummy2wrdummy_MIN                   (0)
  #define MCMISCS_DDRWCKCONTROL1_wrdummy2wrdummy_MAX                   (15) // 0x0000000F
  #define MCMISCS_DDRWCKCONTROL1_wrdummy2wrdummy_DEF                   (0x00000008)
  #define MCMISCS_DDRWCKCONTROL1_wrdummy2wrdummy_HSH                   (0x04283EC4)

  #define MCMISCS_DDRWCKCONTROL1_rdcmd2dummy_OFF                       (24)
  #define MCMISCS_DDRWCKCONTROL1_rdcmd2dummy_WID                       ( 4)
  #define MCMISCS_DDRWCKCONTROL1_rdcmd2dummy_MSK                       (0x0F000000)
  #define MCMISCS_DDRWCKCONTROL1_rdcmd2dummy_MIN                       (0)
  #define MCMISCS_DDRWCKCONTROL1_rdcmd2dummy_MAX                       (15) // 0x0000000F
  #define MCMISCS_DDRWCKCONTROL1_rdcmd2dummy_DEF                       (0x00000008)
  #define MCMISCS_DDRWCKCONTROL1_rdcmd2dummy_HSH                       (0x04303EC4)

  #define MCMISCS_DDRWCKCONTROL1_rddummy2rddummy_OFF                   (28)
  #define MCMISCS_DDRWCKCONTROL1_rddummy2rddummy_WID                   ( 4)
  #define MCMISCS_DDRWCKCONTROL1_rddummy2rddummy_MSK                   (0xF0000000)
  #define MCMISCS_DDRWCKCONTROL1_rddummy2rddummy_MIN                   (0)
  #define MCMISCS_DDRWCKCONTROL1_rddummy2rddummy_MAX                   (15) // 0x0000000F
  #define MCMISCS_DDRWCKCONTROL1_rddummy2rddummy_DEF                   (0x00000008)
  #define MCMISCS_DDRWCKCONTROL1_rddummy2rddummy_HSH                   (0x04383EC4)

#define DDRSCRAM_CR_DDRLASTCR_A0_REG                                   (0x00003EC4)
#define DDRSCRAM_CR_DDRLASTCR_REG                                      (0x00003EC8)

  #define DDRSCRAM_CR_DDRLASTCR_EnInitComplete_OFF                     ( 0)
  #define DDRSCRAM_CR_DDRLASTCR_EnInitComplete_WID                     ( 1)
  #define DDRSCRAM_CR_DDRLASTCR_EnInitComplete_MSK                     (0x00000001)
  #define DDRSCRAM_CR_DDRLASTCR_EnInitComplete_MIN                     (0)
  #define DDRSCRAM_CR_DDRLASTCR_EnInitComplete_MAX                     (1) // 0x00000001
  #define DDRSCRAM_CR_DDRLASTCR_EnInitComplete_DEF                     (0x00000000)
  #define DDRSCRAM_CR_DDRLASTCR_EnInitComplete_HSH                     (0x01003EC8)

  #define DDRSCRAM_CR_DDRLASTCR_spare_OFF                              ( 1)
  #define DDRSCRAM_CR_DDRLASTCR_spare_WID                              (31)
  #define DDRSCRAM_CR_DDRLASTCR_spare_MSK                              (0xFFFFFFFE)
  #define DDRSCRAM_CR_DDRLASTCR_spare_MIN                              (0)
  #define DDRSCRAM_CR_DDRLASTCR_spare_MAX                              (2147483647) // 0x7FFFFFFF
  #define DDRSCRAM_CR_DDRLASTCR_spare_DEF                              (0x00000000)
  #define DDRSCRAM_CR_DDRLASTCR_spare_HSH                              (0x1F023EC8)

#define DDRMCMISCS_CR_DFXCCMON_A0_REG                                  (0x00003EC8)
#define DDRMCMISCS_CR_DFXCCMON_REG                                     (0x00003ECC)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DDRMCMISCS_CR_DFX_CLKCTRL_A0_REG                               (0x00003ECC)
#define DDRMCMISCS_CR_DFX_CLKCTRL_REG                                  (0x00003ED0)

  #define DDRMCMISCS_CR_DFX_CLKCTRL_dfx_clk_gate_disable_OFF           ( 0)
  #define DDRMCMISCS_CR_DFX_CLKCTRL_dfx_clk_gate_disable_WID           ( 1)
  #define DDRMCMISCS_CR_DFX_CLKCTRL_dfx_clk_gate_disable_MSK           (0x00000001)
  #define DDRMCMISCS_CR_DFX_CLKCTRL_dfx_clk_gate_disable_MIN           (0)
  #define DDRMCMISCS_CR_DFX_CLKCTRL_dfx_clk_gate_disable_MAX           (1) // 0x00000001
  #define DDRMCMISCS_CR_DFX_CLKCTRL_dfx_clk_gate_disable_DEF           (0x00000000)
  #define DDRMCMISCS_CR_DFX_CLKCTRL_dfx_clk_gate_disable_HSH           (0x01003ED0)

  #define DDRMCMISCS_CR_DFX_CLKCTRL_reserved0_OFF                      ( 1)
  #define DDRMCMISCS_CR_DFX_CLKCTRL_reserved0_WID                      (31)
  #define DDRMCMISCS_CR_DFX_CLKCTRL_reserved0_MSK                      (0xFFFFFFFE)
  #define DDRMCMISCS_CR_DFX_CLKCTRL_reserved0_MIN                      (0)
  #define DDRMCMISCS_CR_DFX_CLKCTRL_reserved0_MAX                      (2147483647) // 0x7FFFFFFF
  #define DDRMCMISCS_CR_DFX_CLKCTRL_reserved0_DEF                      (0x00000000)
  #define DDRMCMISCS_CR_DFX_CLKCTRL_reserved0_HSH                      (0x1F023ED0)

#define DDRSCRAM_CR_DDREARLYCR_A0_REG                                  (0x00003ED0)
#define DDRSCRAM_CR_DDREARLYCR_REG                                     (0x00003ED4)
//Duplicate of DDRVTT0_CR_DDREARLY_RSTRDONE_REG

#define MCMISCS_COMPUPDATE_CTRL_REG                                    (0x00003ED8)

  #define MCMISCS_COMPUPDATE_CTRL_spare_OFF                            ( 0)
  #define MCMISCS_COMPUPDATE_CTRL_spare_WID                            ( 6)
  #define MCMISCS_COMPUPDATE_CTRL_spare_MSK                            (0x0000003F)
  #define MCMISCS_COMPUPDATE_CTRL_spare_MIN                            (0)
  #define MCMISCS_COMPUPDATE_CTRL_spare_MAX                            (63) // 0x0000003F
  #define MCMISCS_COMPUPDATE_CTRL_spare_DEF                            (0x00000000)
  #define MCMISCS_COMPUPDATE_CTRL_spare_HSH                            (0x06003ED8)

  #define MCMISCS_COMPUPDATE_CTRL_skipcompupdate_OFF                   ( 6)
  #define MCMISCS_COMPUPDATE_CTRL_skipcompupdate_WID                   (10)
  #define MCMISCS_COMPUPDATE_CTRL_skipcompupdate_MSK                   (0x0000FFC0)
  #define MCMISCS_COMPUPDATE_CTRL_skipcompupdate_MIN                   (0)
  #define MCMISCS_COMPUPDATE_CTRL_skipcompupdate_MAX                   (1023) // 0x000003FF
  #define MCMISCS_COMPUPDATE_CTRL_skipcompupdate_DEF                   (0x00000000)
  #define MCMISCS_COMPUPDATE_CTRL_skipcompupdate_HSH                   (0x0A0C3ED8)

  #define MCMISCS_COMPUPDATE_CTRL_compupdatecntval_OFF                 (16)
  #define MCMISCS_COMPUPDATE_CTRL_compupdatecntval_WID                 ( 8)
  #define MCMISCS_COMPUPDATE_CTRL_compupdatecntval_MSK                 (0x00FF0000)
  #define MCMISCS_COMPUPDATE_CTRL_compupdatecntval_MIN                 (0)
  #define MCMISCS_COMPUPDATE_CTRL_compupdatecntval_MAX                 (255) // 0x000000FF
  #define MCMISCS_COMPUPDATE_CTRL_compupdatecntval_DEF                 (0x00000020)
  #define MCMISCS_COMPUPDATE_CTRL_compupdatecntval_HSH                 (0x08203ED8)

  #define MCMISCS_COMPUPDATE_CTRL_compupdateovrdcntval_OFF             (24)
  #define MCMISCS_COMPUPDATE_CTRL_compupdateovrdcntval_WID             ( 8)
  #define MCMISCS_COMPUPDATE_CTRL_compupdateovrdcntval_MSK             (0xFF000000)
  #define MCMISCS_COMPUPDATE_CTRL_compupdateovrdcntval_MIN             (0)
  #define MCMISCS_COMPUPDATE_CTRL_compupdateovrdcntval_MAX             (255) // 0x000000FF
  #define MCMISCS_COMPUPDATE_CTRL_compupdateovrdcntval_DEF             (0x00000047)
  #define MCMISCS_COMPUPDATE_CTRL_compupdateovrdcntval_HSH             (0x08303ED8)
#pragma pack(pop)
#endif
