/** @file
  This file implements functions for setting up test control
  registers for CPGC 2.0.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2016 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "MrcInterface.h"
#include "McAddress.h"
#include "MrcCommon.h"
#include "MrcCpgcApi.h"
#include "Cpgc20TestCtl.h"
#include "Cpgc20Patterns.h"
#include "Cpgc20.h"
#include "MrcCpgcOffsets.h"

/**
  This function programs the masks that enable error checking on the
  requested cachelines and chunks.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  CachelineMask - Bit Mask of cachelines to enable.
  @param[in]  ChunkMask     - Bit Mask of chunks to enable.

  @retval Nothing
**/
void
Cpgc20SetChunkClErrMsk (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                CachelineMask,
  IN  UINT32                ChunkMask
  )
{
  MrcOutput *Outputs;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    IpChannel;
  UINT32    Offset;
  UINT8     McChMask;
  UINT8     MaxChannel;
  BOOLEAN   Ddr4;
  BOOLEAN   Ddr5;
  MC0_CH0_CR_CPGC_ERR_CTL_STRUCT     Cpgc20ErrCtl;
  MC0_CH0_CR_CPGC_ERR_LNEN_HI_STRUCT Cpgc20ErrChunkMask;

  Outputs    = &MrcData->Outputs;
  Ddr4       = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5       = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  McChMask   = Outputs->McChBitMask;
  MaxChannel = Outputs->MaxChannels;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      IpChannel = DDR5_IP_CH (Ddr5, Channel);
      Offset = MrcGetTestErrCtlOffset (MrcData, Controller, IpChannel);
      Cpgc20ErrCtl.Data = MrcReadCR (MrcData, Offset);
      Cpgc20ErrCtl.Bits.ERRCHK_MASK_CACHELINE = CachelineMask;
      Cpgc20ErrCtl.Bits.ERRCHK_MASK_CHUNK = ChunkMask;
      MrcWriteCR (MrcData, Offset, Cpgc20ErrCtl.Data);

      if (!Ddr4) {    // LP4 / LP5 / DDR5 are using the high offset due to longer burst length
        Offset = MrcGetCpgcErrCheckingHighOffset (MrcData, Controller, IpChannel);
        Cpgc20ErrChunkMask.Data = ChunkMask;
        MrcWriteCR (MrcData, Offset, Cpgc20ErrChunkMask.Data);
      }
    }
  }
}

/**
  This function programs the masks that enable error checking on the
  requested bytes.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  WordMask      - Which Word (Lower or Upper) to apply ErrMask to.
  @param[in]  ErrMask       - Error Masking Value to apply.

  @retval Nothing
**/
void
Cpgc20SetDataErrMsk (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                WordMask,
  IN  UINT32                ErrMask
  )
{
  MrcOutput *Outputs;
  UINT32    Channel;
  UINT32    IpChannel;
  UINT32    Offset;
  UINT32    Controller;
  UINT32    Word;
  UINT32    MaxWords;
  UINT8     McChMask;
  UINT8     MaxChannel;
  BOOLEAN   Ddr4;
  BOOLEAN   Ddr5;

  Outputs    = &MrcData->Outputs;
  Ddr4       = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5       = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  MaxWords   = Ddr4 ? 2 : 1;        // LP4/LP5/DDR5 use low word only
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      IpChannel = DDR5_IP_CH (Ddr5, Channel);
      for (Word = 0; Word < MaxWords; Word++) {
        if (((WordMask >> Word) & 1) != 1) {
          continue;
        }
        Offset = (Word == 0) ? MrcGetCpgcErrCheckingLowOffset (MrcData, Controller, IpChannel) : MrcGetCpgcErrCheckingHighOffset (MrcData, Controller, IpChannel);
        MrcWriteCR (MrcData, Offset, ErrMask);
      }
    }
  }
}

/**
  This function programs the masks that enable ecc error checking on the
  requested bytes.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  EccValue      - Error Masking Value to apply.

  @retval Nothing
**/
void
Cpgc20SetEccErrMsk (
  IN MrcParameters *const MrcData,
  IN UINT32               EccValue
  )
{
  MrcOutput *Outputs;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    IpChannel;
  UINT32    Offset;
  UINT8     McChMask;
  UINT8     MaxChannel;
  BOOLEAN   Ddr5;
  MC0_CH0_CR_CPGC_ERR_XLNEN_STRUCT Cpgc20EccErrMskRankMask;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Ddr5       = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      IpChannel = DDR5_IP_CH (Ddr5, Channel);
      Offset = MrcGetEccErrMskRankErrMskOffset (MrcData, Controller, IpChannel);
      Cpgc20EccErrMskRankMask.Data = MrcReadCR (MrcData, Offset);
      Cpgc20EccErrMskRankMask.Bits.ECC_ERRCHK_MASK = EccValue;
      MrcWriteCR (MrcData, Offset, Cpgc20EccErrMskRankMask.Data);
    }
  }
}

/**
  This function programs the error conditions to stop the CPGC engine on.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  StopType      - Stop type for CPGC engine.
  @param[in]  NumOfErr      - Number of Stop Type errors to wait on before stopping CPGC engine.

  @retval Nothing
**/
void
Cpgc20SetupTestErrCtl (
  IN  MrcParameters       *const  MrcData,
  IN  MRC_TEST_STOP_TYPE          StopType,
  IN  UINT32                      NumOfErr
  )
{
  MrcOutput *Outputs;
  UINT32    Controller;
  UINT32    IpChannel;
  UINT32    Channel;
  UINT8     McChMask;
  UINT8     MaxChannel;
  UINT32    Offset;
  BOOLEAN   Ddr5;
  MC0_CH0_CR_CPGC_ERR_CTL_STRUCT  Cpgc20ErrCtl;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Ddr5       = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      IpChannel = DDR5_IP_CH (Ddr5, Channel);
      Offset = MrcGetTestErrCtlOffset (MrcData, Controller, IpChannel);
      Cpgc20ErrCtl.Data = MrcReadCR (MrcData, Offset);
      if (((UINT32) StopType == Cpgc20ErrCtl.Bits.STOP_ON_ERROR_CTL) && (NumOfErr == Cpgc20ErrCtl.Bits.STOP_ON_N)) {
        // Skipping the write as nothing changed
      } else {
        Cpgc20ErrCtl.Bits.STOP_ON_ERROR_CTL = StopType;
        Cpgc20ErrCtl.Bits.STOP_ON_N = NumOfErr;
        MrcWriteCR (MrcData, Offset, Cpgc20ErrCtl.Data);
      }
    }
  }
}

/**
  This function will Setup REUT Error Counters to count errors for specified type.

  @param[in]  MrcData         - Pointer to MRC global data.
  @param[in]  CounterPointer  - Specifies in register which counter to setup. Each Channel has 9 counters including ECC.
  @param[in]  ErrControlSel   - Specifies which type of error counter read will be executed.
  @param[in]  CounterSetting  - Specifies in register which Lane/Byte/Chunk to track in specified counter,
                                based on ErrControlSel value.
  @param[in]  CounterScope    - Specifies if the Pointer is used or not.
  @param[in]  CounterUI       - Specifies which UI will be considered when counting errors.
                                  00 - All UI; 01 - Even UI; 10 - Odd UI; 11 - Particular UI (COUNTER_CONTROL_SEL = ErrCounterCtlPerUI)

  @retval mrcWrongInputParameter if CounterSetting is incorrect for the ErrControlSel selected, otherwise mrcSuccess.
**/
MrcStatus
Cpgc20SetupErrCounterCtl (
  IN MrcParameters *const      MrcData,
  IN UINT8                     CounterPointer,
  IN MRC_ERR_COUNTER_CTL_TYPE  ErrControlSel,
  IN UINT32                    CounterSetting,
  IN UINT8                     CounterScope,
  IN UINT8                     CounterUI
  )
{
  MrcStatus                            Status;
  MrcOutput                            *Outputs;
  MrcDebug                             *Debug;
  UINT32                               Offset;
  UINT8                                Controller;
  UINT8                                IpChannel;
  UINT8                                Channel;
  UINT8                                McChMask;
  UINT8                                MaxChannel;
  BOOLEAN                              Ddr5;
  MC0_CH0_CR_CPGC_ERR_CNTRCTL_0_STRUCT Cpgc20ErrCounterCtl;

  Status     = mrcSuccess;
  Outputs    = &MrcData->Outputs;
  Debug      = &Outputs->Debug;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Ddr5       = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      IpChannel = DDR5_IP_CH (Ddr5, Channel);
      switch (ErrControlSel) {
        // Setup Error Counter Control for particular lane
        // @todo: This always uses counter 0 for that Channel to count the lane.  This does not allow counting multiple lanes at a time, and should be fixed if feature is needed.
        // @todo: Return Error as this mode isn't supported.
        case ErrCounterCtlPerLane:
          if ((CounterSetting > 72) || (CounterPointer > 8)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Attempted to setup an error counter for invalid lane. Counter: %u, Lane: %u\n", CounterPointer, CounterSetting);
            Status = mrcWrongInputParameter;
          }
          break;

        // Setup Error Counter Control for particular byte
        case ErrCounterCtlPerByte:
          if (((CounterSetting > 8) || (CounterPointer > 8)) && (MrcByteExist (MrcData, Controller, Channel, CounterPointer))) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Attempted to setup an error counter for invalid byte. Counter: %u, Byte: %u\n", CounterPointer, CounterSetting);
            Status = mrcWrongInputParameter;
          }
          break;

        // Setup Error Counter Control for particular nibble - used in Server, probably not used in Client
/**
        case ErrCounterCtlPerNibble:
          if ((CounterSetting > 4) || (CounterPointer > 8)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Attempted to setup an error counter for invalid nibble. Counter: %u, Nibble: %u\n", CounterPointer, CounterSetting);
            Status = mrcWrongInputParameter;
          }
          break;
**/
        // Setup Error Counter Control for particular UI
        case ErrCounterCtlPerUI:
          if (CounterPointer > 8) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Attempted to setup an error counter for invalid chunk. Counter: %u\n", CounterPointer);
            Status = mrcWrongInputParameter;
          }
          // Setup Error Counter Control for All Lanes
          break;
        case ErrCounterCtlAllLanes:
          // In ErrCounterCtlAllLanes, COUNTER_CONTROL_SCOPE = 0. In this case, COUNTER_POINTER is unused and COUNTER_CONTROL_SEL is don't care.
          break;
        default:
          Status = mrcWrongInputParameter;
          break;
      }
      if (Status == mrcSuccess) {
        Offset = (OFFSET_CALC_MC_CH (MC0_CH0_CR_CPGC_ERR_CNTRCTL_0_REG, MC1_CH0_CR_CPGC_ERR_CNTRCTL_0_REG, Controller, MC0_CH1_CR_CPGC_ERR_CNTRCTL_0_REG, IpChannel)) +
               ((MC0_CH0_CR_CPGC_ERR_CNTRCTL_1_REG - MC0_CH0_CR_CPGC_ERR_CNTRCTL_0_REG) * CounterPointer);
        Cpgc20ErrCounterCtl.Data = 0;
        if (Ddr5 && Outputs->EccSupport && (CounterPointer == MRC_DDR5_ECC_BYTE)) {
          // CPGC error counters use byte 8 for ECC byte also in DDR5 case
          Cpgc20ErrCounterCtl.Bits.COUNTER_POINTER     = MRC_DDR4_ECC_BYTE;
        } else {
          Cpgc20ErrCounterCtl.Bits.COUNTER_POINTER     = CounterPointer;
        }
        Cpgc20ErrCounterCtl.Bits.COUNTER_CONTROL_SEL   = ErrControlSel;
        Cpgc20ErrCounterCtl.Bits.COUNTER_CONTROL_SCOPE = CounterScope;
        Cpgc20ErrCounterCtl.Bits.COUNTER_CONTROL_UI    = CounterUI;
        MrcWriteCR (MrcData, Offset, Cpgc20ErrCounterCtl.Data);
      }
    }
  }
  return Status;
}

/**
  This function returns the Error status results for specified MRC_ERR_STATUS_TYPE.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Desired Memory Controller.
  @param[in]  Channel     - Desired Channel.
  @param[in]  Param       - Specifies which type of error status read will be executed.
  @param[out] Buffer      - Pointer to buffer which register values will be read into.
                              Error status bits will be returned starting with bit zero.

  @retval Returns mrcWrongInputParameter if Param value is not supported by this function, otherwise mrcSuccess.
**/
MrcStatus
Cpgc20GetErrEccChunkRankByteStatus (
  IN  MrcParameters   *const  MrcData,
  IN  UINT32                  Controller,
  IN  UINT32                  Channel,
  IN  MRC_ERR_STATUS_TYPE     Param,
  OUT UINT64          *const  Buffer
  )
{
  MrcOutput                                     *Outputs;
  UINT64                                        Value;
  UINT32                                        Offset;
  UINT32                                        Offset1;
  UINT32                                        CRValue;
  UINT32                                        IpChannel;
  UINT8                                         McChMask;
  UINT8                                         MaxChannel;
  BOOLEAN                                       EccSupport;
  BOOLEAN                                       Ddr4;
  BOOLEAN                                       Ddr5;
  MC0_CH0_CR_CPGC_ERR_BYTE_NTH_PAR_STAT_STRUCT  CpgcErrByteNthStatus;
  MC0_CH0_CR_CPGC_ERR_ECC_CHNK_RANK_STAT_STRUCT CpgcErrEccChunkRank;
  MC0_CH0_CR_CPGC_ERR_STAT47_STRUCT             ErrStat47;

  Outputs    = &MrcData->Outputs;
  EccSupport = Outputs->EccSupport;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Ddr4       = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5       = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  Value = 0;
  if (MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) {
    return mrcWrongInputParameter;
  }
  IpChannel = DDR5_IP_CH (Ddr5, Channel);
  if ((Param == ByteGroupErrStatus) || (Param == EccLaneErrStatus) || (Param == WdbRdChunkNumStatus) || (Param == NthErrStatus)) {
    Offset = OFFSET_CALC_MC_CH (
              MC0_CH0_CR_CPGC_ERR_BYTE_NTH_PAR_STAT_REG,
              MC1_CH0_CR_CPGC_ERR_BYTE_NTH_PAR_STAT_REG, Controller,
              MC0_CH1_CR_CPGC_ERR_BYTE_NTH_PAR_STAT_REG, IpChannel);
  } else if ((Param == RankErrStatus) || (Param == ChunkErrStatus)) {
    Offset = OFFSET_CALC_MC_CH (
              MC0_CH0_CR_CPGC_ERR_ECC_CHNK_RANK_STAT_REG,
              MC1_CH0_CR_CPGC_ERR_ECC_CHNK_RANK_STAT_REG, Controller,
              MC0_CH1_CR_CPGC_ERR_ECC_CHNK_RANK_STAT_REG, IpChannel);
  } else {
    Offset = 0;
  }

  CRValue = MrcReadCR (MrcData, Offset);
  CpgcErrByteNthStatus.Data = CRValue;
  CpgcErrEccChunkRank.Data = CRValue;

  Offset1 = OFFSET_CALC_MC_CH (
              MC0_CH0_CR_CPGC_ERR_STAT47_REG,
              MC1_CH0_CR_CPGC_ERR_STAT47_REG, Controller,
              MC0_CH1_CR_CPGC_ERR_STAT47_REG, IpChannel);

  switch (Param) {
    case ByteGroupErrStatus:
      Value = CpgcErrByteNthStatus.Bits.BYTEGRP_ERR_STAT;
      if (EccSupport) {
        Value |= (UINT64) (CpgcErrByteNthStatus.Bits.ECCGRP_ERR_STAT << MC0_CH0_CR_CPGC_ERR_BYTE_NTH_PAR_STAT_ECCGRP_ERR_STAT_OFF);
      }
      break;

    case EccLaneErrStatus:
      if (EccSupport) {
        Value = CpgcErrByteNthStatus.Bits.ECCGRP_ERR_STAT;
      }
      break;

    case RankErrStatus:
      Value = CpgcErrEccChunkRank.Bits.RANK_ERR_STAT;
      break;

    case WdbRdChunkNumStatus:
      Value = CpgcErrByteNthStatus.Bits.RD_CHUNK_NUM_STAT;
      break;

    case NthErrStatus:
      Value = CpgcErrByteNthStatus.Bits.Nth_ERROR;
      break;

    case ChunkErrStatus:
      if (!Ddr4) {
        // For BL16/32 cases the chunk error status is in CPGC_ERR_STAT47.LANE_ERR_STAT_HI
        ErrStat47.Data = MrcReadCR (MrcData, Offset1);
        Value = ErrStat47.Bits.LANE_ERR_STAT_HI;
      } else {
        Value = CpgcErrEccChunkRank.Bits.CHUNK_ERR_STAT;
      }
      break;

    case AlertErrStatus:
//    Value = ReutSubChErrChunkRankByteNthStatus.Bits.Alert_Error_Status;
//    break;

    case NthErrOverflow:
//    Value = ReutSubChErrChunkRankByteNthStatus.Bits.Nth_Error_Overflow;
//    break;

    default:
      return mrcWrongInputParameter;
  }

  MRC_DEBUG_ASSERT (Buffer != NULL, &Outputs->Debug, "%s Null Pointer", gErrString);
  *Buffer = Value;
  return mrcSuccess;
}

/**
  This function accesses the Sequence loop count (per McChBitMask).

  @param[in]  MrcData    - Pointer to MRC global data.
  @param[in]  LoopCount  - Pointer to variable to store or set.

  @retval Nothing.
**/
void
Cpgc20SetLoopCount (
  IN      MrcParameters *const  MrcData,
  IN OUT  UINT32  *const        LoopCount
  )
{
  MrcOutput     *Outputs;
  UINT8         Controller;
  UINT8         Channel;
  UINT8         IpChannel;
  UINT8         McChMask;
  UINT8         MaxChannel;
  UINT32        Offset;
  BOOLEAN       Lpddr;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Lpddr      = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC2_BLOCK_REPEATS_REG,
                MC1_REQ0_CR_CPGC2_BLOCK_REPEATS_REG, Controller,
                MC0_REQ1_CR_CPGC2_BLOCK_REPEATS_REG, IpChannel);
      // MRC_DEBUG_MSG (Outputs->Debug, MSG_LEVEL_ERROR, "Cpgc20SetLoopCount: mc%d ch%d LoopCount = %d\n", Controller, Channel, *LoopCount);
      MrcWriteCR (MrcData, Offset, *LoopCount);
    }
  }
}

/**
  This function accesses the Logical to Physical Bank Mapping.

  @param[in]  MrcData    - Pointer to MRC global data.
  @param[in]  Controller - Desired Controller
  @param[in]  Channel    - Desired Channel
  @param[in]  Banks      - Pointer to buffer to logical-to-physical bank mapping.
  @param[in]  Count      - Length of the Banks buffer.
  @param[in]  IsGet      - TRUE: Get.  FALSE: Set.

  @retval Nothing.
**/
void
Cpgc20GetSetBankMap (
  IN      MrcParameters *const  MrcData,
  IN      UINT32                Controller,
  IN      UINT32                Channel,
  IN OUT  UINT8 *const          Banks,
  IN      UINT32                Count,
  IN      BOOLEAN               IsGet
  )
{
  MrcOutput     *Outputs;
  UINT8         McChMask;
  UINT8         MaxChannel;
  UINT32        IpChannel;
  UINT32        RegData;
  UINT32        RegIndex;
  UINT32        Index;
  UINT32        BanksPerReg;
  UINT32        Offset;
  UINT32        CrIncrement;
  UINT32        FieldOffsetStep;
  UINT32        FieldMask;
  UINT32        FieldOffset;
  BOOLEAN       EndOfRegister;
  BOOLEAN       Lpddr;

  RegData     = 0;
  BanksPerReg = 6;
  Outputs     = &MrcData->Outputs;
  MaxChannel  = Outputs->MaxChannels;
  McChMask    = Outputs->McChBitMask;
  Lpddr       = Outputs->Lpddr;

  if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
    return;
  }
  IpChannel = LP_IP_CH (Lpddr, Channel);
  Offset = OFFSET_CALC_MC_CH (
            MC0_REQ0_CR_CPGC_SEQ_BANK_L2P_MAPPING_A_REG,
            MC1_REQ0_CR_CPGC_SEQ_BANK_L2P_MAPPING_A_REG, Controller,
            MC0_REQ1_CR_CPGC_SEQ_BANK_L2P_MAPPING_A_REG, IpChannel);
  CrIncrement = MC0_REQ0_CR_CPGC_SEQ_BANK_L2P_MAPPING_B_REG - MC0_REQ0_CR_CPGC_SEQ_BANK_L2P_MAPPING_A_REG;
  FieldOffsetStep = MC0_REQ0_CR_CPGC_SEQ_BANK_L2P_MAPPING_A_L2P_BANK1_MAPPING_OFF;
  FieldMask = MC0_REQ0_CR_CPGC_SEQ_BANK_L2P_MAPPING_A_L2P_BANK0_MAPPING_MSK;

  for (Index = 0; Index < Count; Index++) {
    RegIndex      = Index % BanksPerReg;
    FieldOffset   = RegIndex * FieldOffsetStep;
    EndOfRegister = (RegIndex == (BanksPerReg - 1)) || (Index == (Count - 1));
    if (RegIndex == 0) {
      RegData = MrcReadCR (MrcData, Offset);
    }
    if (IsGet) {
      Banks[Index] = (UINT8) ((RegData >> FieldOffset) & FieldMask);
    } else {
      RegData &= ~(FieldMask << FieldOffset);
      RegData |= (Banks[Index] & FieldMask) << FieldOffset;
      // If we're at the end of Banks to write, or we're at the end of the banks in the register,
      // write out the Register data.
      if (EndOfRegister) {
        MrcWriteCR (MrcData, Offset, RegData);
      }
    }
    if (EndOfRegister) {
      // Update the offset to the next register
      Offset += CrIncrement;
    }
  }
}

/**
  This function returns the Bit Group Error status results.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Desired Memory Controller.
  @param[in]  Channel     - Desired Channel.
  @param[out] Status      - Pointer to array where the lane error status values will be stored.

  @retval Nothing.
**/
void
Cpgc20GetBitGroupErrStatus (
  IN  MrcParameters   *const  MrcData,
  IN  UINT32                  Controller,
  IN  UINT32                  Channel,
  OUT UINT8                   *Status
  )
{
  MrcOutput     *Outputs;
  UINT32        ErrDataStatus03;
  UINT32        Offset1;
  UINT32        Offset2;
  UINT32        ErrDataStatus47;
  UINT32        EccByte;
  UINT32        IpChannel;
  UINT8         Byte;
  UINT8         McChMask;
  UINT8         MaxChannel;
  BOOLEAN       Ddr5;
  MC0_CH0_CR_CPGC_ERR_ECC_CHNK_RANK_STAT_STRUCT CpgcErrEccChunkRank;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Ddr5       = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  EccByte    = Ddr5 ? MRC_DDR5_ECC_BYTE : MRC_DDR4_ECC_BYTE;

  if (MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) != 0) {
    IpChannel = DDR5_IP_CH (Ddr5, Channel);
    Offset1 = OFFSET_CALC_MC_CH (
                MC0_CH0_CR_CPGC_ERR_STAT03_REG,
                MC1_CH0_CR_CPGC_ERR_STAT03_REG, Controller,
                MC0_CH1_CR_CPGC_ERR_STAT03_REG, IpChannel);
    Offset2 = OFFSET_CALC_MC_CH (
                MC0_CH0_CR_CPGC_ERR_STAT47_REG,
                MC1_CH0_CR_CPGC_ERR_STAT47_REG, Controller,
                MC0_CH1_CR_CPGC_ERR_STAT47_REG, IpChannel);

    MRC_DEBUG_ASSERT (Status != NULL, &Outputs->Debug, "%s Null Pointer", gErrString);

    ErrDataStatus03 = MrcReadCR (MrcData, Offset1);
    ErrDataStatus47 = MrcReadCR (MrcData, Offset2);
    for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
      if (Byte < 4) {
        Status[Byte] = (ErrDataStatus03 >> (8 * Byte)) & 0xFF;
      } else {
        Status[Byte] = (ErrDataStatus47 >> (8 * (Byte - 4))) & 0xFF;
      }
    }
    if (Outputs->EccSupport) {
      Offset1 = OFFSET_CALC_MC_CH (
                  MC0_CH0_CR_CPGC_ERR_ECC_CHNK_RANK_STAT_REG,
                  MC1_CH0_CR_CPGC_ERR_ECC_CHNK_RANK_STAT_REG, Controller,
                  MC0_CH1_CR_CPGC_ERR_ECC_CHNK_RANK_STAT_REG, IpChannel);
      CpgcErrEccChunkRank.Data = MrcReadCR (MrcData, Offset1);
      Status[EccByte] = (UINT8) CpgcErrEccChunkRank.Bits.ECC_LANE_ERR_STAT;
    }
  }
}
/**
  This function returns the Error Counter status for specified counter.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  Controller    - Desired Memory Controller.
  @param[in]  Channel       - Desired Channel.
  @param[in]  CounterSelect - Desired error counter to read from.
  @param[out] CounterStatus - Pointer to buffer where counter status will be held.
  @param[out] Overflow      - Indicates if counter has reached overflow.

  @retval Nothing.
**/
void
Cpgc20GetErrCounterStatus (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                CounterSelect,
  OUT UINT32        *const  CounterStatus,
  OUT BOOLEAN       *const  Overflow
  )
{
  MrcOutput  *Outputs;
  UINT32     IpChannel;
  UINT32     Offset;
  UINT32     Value;
  BOOLEAN    Ddr5;

  Outputs    = &MrcData->Outputs;
  Ddr5       = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  IpChannel = DDR5_IP_CH (Ddr5, Channel);

  Offset = OFFSET_CALC_MC_CH (
            MC0_CH0_CR_CPGC_ERR_CNTR_0_REG,
            MC1_CH0_CR_CPGC_ERR_CNTR_0_REG, Controller,
            MC0_CH1_CR_CPGC_ERR_CNTR_0_REG, IpChannel);
  Offset += (MC0_CH0_CR_CPGC_ERR_CNTR_1_REG - MC0_CH0_CR_CPGC_ERR_CNTR_0_REG) * CounterSelect;
  Value = MrcReadCR (MrcData, Offset);

  MRC_DEBUG_ASSERT ((CounterStatus != NULL) && (Overflow != NULL), &Outputs->Debug, "%s Null Pointer", gErrString);

  *CounterStatus = Value;

  Offset = OFFSET_CALC_MC_CH (
            MC0_CH0_CR_CPGC_ERR_CNTR_OV_REG,
            MC1_CH0_CR_CPGC_ERR_CNTR_OV_REG, Controller,
            MC0_CH1_CR_CPGC_ERR_CNTR_OV_REG, IpChannel);
  Value = MrcReadCR (MrcData, Offset);

  *Overflow = ((Value >> (CounterSelect % 9)) & 1) == 1;
}

/**
  This function writes to all enabled CPGC SEQ CTL registers.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  McChBitMask   - Memory Controller Channel Bit mask for which registers should be programmed for.
  @param[in]  CpgcSeqCtl    - Data to be written to all CPGC SEQ CTL registers.

  @retval Nothing.
**/
void
Cpgc20ControlRegWrite (
  IN  MrcParameters *const              MrcData,
  IN  UINT8                             McChBitMask,
  IN  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT   CpgcSeqCtl
  )
{
  MrcOutput     *Outputs;
  UINT8         Controller;
  UINT8         Channel;
  UINT8         IpChannel;
  UINT8         MaxChannel;
  UINT32        Offset;
  BOOLEAN       Lpddr;
  BOOLEAN       GlobalStart; 
  BOOLEAN       IssuedStart;
  MC0_REQ0_CR_CPGC_SEQ_STATUS_STRUCT  CpgcStatus;
  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  Lpddr      = Outputs->Lpddr;
  GlobalStart = (CpgcSeqCtl.Bits.START_TEST == 1) && MrcData->Save.Data.CpgcGlobalStart;  // Start all CPGC engines together with a single write
  IssuedStart = FALSE;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      if (CpgcSeqCtl.Bits.START_TEST == 1) {
        Offset = OFFSET_CALC_MC_CH (
          MC0_REQ0_CR_CPGC_SEQ_STATUS_REG,
          MC1_REQ0_CR_CPGC_SEQ_STATUS_REG, Controller,
          MC0_REQ1_CR_CPGC_SEQ_STATUS_REG, IpChannel);
        CpgcStatus.Data = MrcReadCR (MrcData, Offset);
        if (CpgcStatus.Bits.TEST_DONE == 1) {
          // Clear by writing a '1'
          MrcWriteCR (MrcData, Offset, CpgcStatus.Data);
        }
      }
      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC_SEQ_CTL_REG,
                MC1_REQ0_CR_CPGC_SEQ_CTL_REG, Controller,
                MC0_REQ1_CR_CPGC_SEQ_CTL_REG, IpChannel);

      MrcWriteCR (MrcData, Offset, CpgcSeqCtl.Data);
      if (GlobalStart) {
        IssuedStart = TRUE;
        break;
      }
    } // Channel
    if (IssuedStart) {
      break;
    }
  }
}

/**
  This function aggregates the status of STOP_TEST bit for all enabled CPGC engines.

  @param[in]  MrcData       - Pointer to MRC global data.

  @retval UINT8 of the aggregated value of STOP_TEST.
**/
UINT8
Cpgc20ControlRegStopBitStatus (
  IN  MrcParameters *const  MrcData
  )
{
  MrcOutput                       *Outputs;
  UINT8                           Controller;
  UINT8                           Channel;
  UINT8                           IpChannel;
  UINT8                           StopBit;
  UINT8                           McChMask;
  UINT8                           MaxChannel;
  UINT32                          Offset;
  BOOLEAN                         Lpddr;
  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT CpgcSeqCtl;

  Outputs    = &MrcData->Outputs;
  StopBit    = 0;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Lpddr      = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC_SEQ_CTL_REG,
                MC1_REQ0_CR_CPGC_SEQ_CTL_REG, Controller,
                MC0_REQ1_CR_CPGC_SEQ_CTL_REG, IpChannel);
      CpgcSeqCtl.Data = MrcReadCR (MrcData, Offset);
      StopBit |= CpgcSeqCtl.Bits.STOP_TEST;
    }
  }
  return StopBit;
}

/**
  This function writes to all enabled CADB CTL registers.

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  McChBitMask  - Memory Controller Channel Bit mask for which registers should be programmed for.
  @param[in]  CadbControl  - Data to be written to all CADB CTL registers.

  @retval Nothing.
**/
void
Cadb20ControlRegWrite (
  IN  MrcParameters *const                 MrcData,
  IN  UINT8                                McChBitMask,
  IN  MC0_CH0_CR_CADB_CTL_STRUCT           CadbControl
  )
{
  MrcOutput     *Outputs;
  UINT8         Channel;
  UINT8         IpChannel;
  UINT8         Controller;
  UINT32        Offset;
  UINT8         MaxChannel;
  UINT8         McChannel;
  BOOLEAN       Ddr5;
  BOOLEAN       CaParityMode;
  MC0_CH0_CR_CADB_STATUS_STRUCT   CadbStatus;

  Outputs       = &MrcData->Outputs;
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  CaParityMode  = Outputs->CaParityMode;
  MaxChannel    = CaParityMode ? MAX_CHANNEL : Outputs->MaxChannels;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      McChannel = CaParityMode ? (Channel / 2) : Channel;
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, McChannel, Outputs->MaxChannels) == 0) {
        continue;
      }
      IpChannel = CaParityMode ? Channel : DDR5_IP_CH (Ddr5, Channel);
      if (CadbControl.Bits.START_TEST) {
        // Make sure that the previous test was finished
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_CADB_STATUS_REG, MC1_CH0_CR_CADB_STATUS_REG, Controller, MC0_CH1_CR_CADB_STATUS_REG, IpChannel);
        CadbStatus.Data = MrcReadCR (MrcData, Offset);
        if (CadbStatus.Bits.TEST_DONE == 1) {
          // Clear by writing a '1'
          MrcWriteCR (MrcData, Offset, CadbStatus.Data);
          CadbStatus.Data = MrcReadCR (MrcData, Offset);
        }
      }
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_CADB_CTL_REG, MC1_CH0_CR_CADB_CTL_REG, Controller, MC0_CH1_CR_CADB_CTL_REG, IpChannel);
      MrcWriteCR (MrcData, Offset, CadbControl.Data);
    }
  }
}

/**
  This function writes to all enabled CADB CFG registers.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  CadbConfig    - Data to be written to all CADB CFG registers.

  @retval Nothing.
**/
void
Cadb20ConfigRegWrite (
  IN  MrcParameters *const                 MrcData,
  IN  MC0_CH0_CR_CADB_CFG_STRUCT           CadbConfig
  )
{
  MrcOutput     *Outputs;
  UINT8         Controller;
  UINT8         Channel;
  UINT8         IpChannel;
  UINT32        Offset;
  UINT8         McChMask;
  UINT8         MaxChannel;
  UINT8         McChannel;
  BOOLEAN       Ddr5;
  BOOLEAN       CaParityMode;

  Outputs       = &MrcData->Outputs;
  McChMask      = Outputs->McChBitMask;
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  CaParityMode  = Outputs->CaParityMode;
  MaxChannel    = CaParityMode ? MAX_CHANNEL : Outputs->MaxChannels;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      McChannel = CaParityMode ? (Channel / 2) : Channel;
      if (MC_CH_MASK_CHECK (McChMask, Controller, McChannel, Outputs->MaxChannels) == 0) {
        continue;
      }
      IpChannel = CaParityMode ? Channel : DDR5_IP_CH (Ddr5, Channel);
      Offset = OFFSET_CALC_MC_CH (
                MC0_CH0_CR_CADB_CFG_REG,
                MC1_CH0_CR_CADB_CFG_REG, Controller,
                MC0_CH1_CR_CADB_CFG_REG, IpChannel);
      MrcWriteCR (MrcData, Offset, CadbConfig.Data);
    }
  }
}

/**
  This function writes to all enabled CADB_AO_MRSCFG registers.

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  CadbMrsConfig - Data to be written to all CADB_AO_MRSCFG registers.

  @retval Nothing.
**/
void
Cadb20MrsConfigRegWrite (
  IN  MrcParameters *const                 MrcData,
  IN  MC0_CH0_CR_CADB_AO_MRSCFG_STRUCT     CadbMrsConfig
  )
{
  MrcOutput     *Outputs;
  UINT32        Controller;
  UINT32        Channel;
  UINT32        IpChannel;
  UINT32        Offset;
  UINT8         McChMask;
  UINT8         MaxChannel;
  UINT32        McChannel;
  BOOLEAN       Ddr5;
  BOOLEAN       CaParityMode;

  Outputs       = &MrcData->Outputs;
  McChMask      = Outputs->McChBitMask;
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  CaParityMode  = Outputs->CaParityMode;
  MaxChannel    = CaParityMode ? MAX_CHANNEL : Outputs->MaxChannels;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      McChannel = CaParityMode ? (Channel / 2) : Channel;
      if (MC_CH_MASK_CHECK (McChMask, Controller, McChannel, Outputs->MaxChannels) == 0) {
        continue;
      }
      IpChannel = CaParityMode ? Channel : DDR5_IP_CH (Ddr5, Channel);
      Offset = OFFSET_CALC_MC_CH (
        MC0_CH0_CR_CADB_AO_MRSCFG_REG,
        MC1_CH0_CR_CADB_AO_MRSCFG_REG, Controller,
        MC0_CH1_CR_CADB_AO_MRSCFG_REG, IpChannel);
      MrcWriteCR (MrcData, Offset, CadbMrsConfig.Data);
    }
  }
}

/**
  This function checks the value of CADB_STATUS.TEST_DONE bit for all enabled CADB engines.

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  McChBitMask  - Bitmask of participating controllers / channels

  @retval 1 - if all enabled engines are done, otherwise 0
**/
UINT32
Cadb20TestDoneStatus (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 McChBitMask
  )
{
  MrcOutput   *Outputs;
  UINT8       Controller;
  UINT8       Channel;
  UINT8       IpChannel;
  UINT8       MaxChannel;
  UINT8       McChannel;
  UINT32      TestDone;
  UINT32      Offset;
  BOOLEAN     Ddr5;
  BOOLEAN     CaParityMode;
  MC0_CH0_CR_CADB_STATUS_STRUCT   CadbStatus;

  Outputs       = &MrcData->Outputs;
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  CaParityMode  = Outputs->CaParityMode;
  MaxChannel    = CaParityMode ? MAX_CHANNEL : Outputs->MaxChannels;

  TestDone      = 1; // Assume all done
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      McChannel = CaParityMode ? (Channel / 2) : Channel;
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, McChannel, Outputs->MaxChannels) == 0) {
        continue;
      }
      IpChannel = CaParityMode ? Channel : DDR5_IP_CH (Ddr5, Channel);
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_CADB_STATUS_REG, MC1_CH0_CR_CADB_STATUS_REG, Controller, MC0_CH1_CR_CADB_STATUS_REG, IpChannel);
      CadbStatus.Data = MrcReadCR (MrcData, Offset);
      TestDone &= CadbStatus.Bits.TEST_DONE;
    }
  }
  return TestDone;
}

/**
  This function writes Command Instructions to all enabled CPGC engines.

  @param[in]  MrcData        - Pointer to MRC global data.
  @param[in]  CmdInstruct    - Array of Command Instructions
  @param[in]  NumInstruct    - Number of Instructions

  @retval MrcStatus - mrcSuccess if does not exceed command instruction registers otherwise mrcFail
**/
MrcStatus
Cpgc20CmdInstructWrite (
  IN  MrcParameters *const                            MrcData,
  IN  MC0_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_0_STRUCT  *CmdInstruct,
  IN  UINT8                                           NumInstruct
  )
{
  MrcOutput         *Outputs;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             IpChannel;
  UINT8             Index;
  UINT8             McChMask;
  UINT8             MaxChannel;
  UINT32            Offset;
  BOOLEAN           Lpddr;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Lpddr      = Outputs->Lpddr;

  if (NumInstruct > CPGC20_MAX_COMMAND_INSTRUCTION) {
    return mrcFail;
  }
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_0_REG,
                MC1_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_0_REG, Controller,
                MC0_REQ1_CR_CPGC2_COMMAND_INSTRUCTION_0_REG, IpChannel);
      for (Index = 0; Index < NumInstruct; Index++) {
        MrcWriteCR8 (MrcData, Offset, CmdInstruct[Index].Data);
        Offset += MC0_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_1_REG - MC0_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_0_REG;
      }
    }
  }
  return mrcSuccess;
}

/**
  This function writes Algorithm Instructions to all enabled CPGC engines (per Outputs->McChBitMask).

  @param[in]  MrcData               - Pointer to MRC global data.
  @param[in]  AlgoInstruct          - Array of Algorithm Instructions
  @param[in]  AlgoInstructControl   - Algorithm Instruction Control setting to program based on ValidMask
  @param[in]  AlgoWaitEventControl  - Algorithm Wait Event Control setting to program
  @param[in]  ValidMask             - Bit Mask of which Instructions are valid

  @retval Nothing.
**/
void
Cpgc20AlgoInstructWrite (
  IN  MrcParameters *const                                   MrcData,
  IN  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT       *AlgoInstruct,
  IN  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_STRUCT  AlgoInstructControl,
  IN  MC0_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_STRUCT  AlgoWaitEventControl,
  IN  UINT8                                                  ValidMask
  )
{
  MrcOutput     *Outputs;
  UINT32        Controller;
  UINT32        Channel;
  UINT32        IpChannel;
  UINT32        Offset;
  UINT32        CtrlOffset;
  UINT32        Index;
  UINT8         McChMask;
  UINT8         MaxChannel;
  BOOLEAN       Lpddr;
  MC0_REQ0_CR_CPGC2_BASE_CLOCK_CONFIG_STRUCT WaitToStartCfg;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Lpddr      = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
                    MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_REG,
                    MC1_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_REG, Controller,
                    MC0_REQ1_CR_CPGC2_ALGORITHM_INSTRUCTION_0_REG, IpChannel);
      CtrlOffset = OFFSET_CALC_MC_CH (
                    MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_REG,
                    MC1_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_REG, Controller,
                    MC0_REQ1_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_REG, IpChannel);
      for (Index = 0; Index < CPGC20_MAX_ALGORITHM_INSTRUCTION; Index++) {
        if (ValidMask & (MRC_BIT0 << Index)) {
          MrcWriteCR8 (MrcData, Offset, AlgoInstruct[Index].Data);
          MrcWriteCR8 (MrcData, CtrlOffset, AlgoInstructControl.Data);
          Offset += MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_1_REG - MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_REG;
          CtrlOffset += MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_1_REG - MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_REG;
        } else {
          break;
        }
      }
      CtrlOffset = OFFSET_CALC_MC_CH (
                    MC0_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_REG,
                    MC1_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_REG, Controller,
                    MC0_REQ1_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_REG, IpChannel);
      MrcWriteCR (MrcData, CtrlOffset, AlgoWaitEventControl.Data);

      if (AlgoWaitEventControl.Bits.Wait_Clock_Frequency == CPGC20_NATIVE_DUNIT_FREQ) {
        Offset = OFFSET_CALC_MC_CH (
                  MC0_REQ0_CR_CPGC2_BASE_CLOCK_CONFIG_REG,
                  MC1_REQ0_CR_CPGC2_BASE_CLOCK_CONFIG_REG, Controller,
                  MC0_REQ1_CR_CPGC2_BASE_CLOCK_CONFIG_REG, IpChannel);
        WaitToStartCfg.Data = MrcReadCR (MrcData, Offset);
        WaitToStartCfg.Bits.Clock_Freq = 1;
        MrcWriteCR (MrcData, Offset, WaitToStartCfg.Data);
      }
    }
  }
}

/**
  This function writes Algorithm Instructions to all enabled CPGC engines.

  @param[in]  MrcData        - Pointer to MRC global data.
  @param[in]  DataInstruct   - Array of Data Instructions
  @param[in]  ValidMask      - Bit Mask of which Instructions are valid

  @retval Nothing.
**/
void
Cpgc20DataInstructWrite (
  IN  MrcParameters *const                        MrcData,
  IN  MC0_REQ0_CR_CPGC2_DATA_INSTRUCTION_0_STRUCT *DataInstruct,
  IN  UINT8                                       ValidMask
  )
{
  MrcOutput *Outputs;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    IpChannel;
  UINT32    Index;
  UINT32    Offset;
  UINT8     McChMask;
  UINT8     MaxChannel;
  BOOLEAN   Lpddr;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Lpddr      = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC2_DATA_INSTRUCTION_0_REG,
                MC1_REQ0_CR_CPGC2_DATA_INSTRUCTION_0_REG, Controller,
                MC0_REQ1_CR_CPGC2_DATA_INSTRUCTION_0_REG, IpChannel);
      for (Index = 0; Index < CPGC20_MAX_DATA_INSTRUCTION; Index++) {
        if (ValidMask & (MRC_BIT0 << Index)) {
          MrcWriteCR8 (MrcData, Offset, DataInstruct[Index].Data);
          Offset += MC0_REQ0_CR_CPGC2_DATA_INSTRUCTION_1_REG - MC0_REQ0_CR_CPGC2_DATA_INSTRUCTION_0_REG;
        } else {
          break;
        }
      }
    }
  }
}

/**
  This function sets up Address related registers (ADDRESS_INSTRUCTION, REGION_LOW_ROW, REGION_LOW_COL, ADDRESS_SIZE) to all enabled CPGC engines.

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  AddressOrder      - Address Instruction Order
  @param[in]  AddressDirection  - Address Instruction Direction
  @param[in]  LastValidInstruct - Last Valid Address Instruction
  @param[in]  RowStart          - Starting Row Address
  @param[in]  ColStart          - Starting Column Address
  @param[in]  RowSizeBits       - Memory Region/Block Bits Size for Row
  @param[in]  ColSizeBits       - Memory Region/Block Bits Size for Column
  @param[in]  BankSize          - # of Banks (Base 1 numbering)
  @param[in]  EnCADB            - Set up the address ordering for Command stress

  @retval Nothing.
**/
void
Cpgc20AddressSetup (
  IN  MrcParameters *const            MrcData,
  IN  CPGC20_ADDRESS_INCREMENT_ORDER  AddressOrder,
  IN  CPGC20_ADDRESS_DIRECTION        AddressDirection,
  IN  UINT8                           LastValidInstruct,
  IN  UINT32                          RowStart,
  IN  UINT16                          ColStart,
  IN  UINT8                           RowSizeBits,
  IN  UINT8                           ColSizeBits,
  IN  UINT8                           BankSize,
  IN  UINT8                           EnCADB
  )
{
  MrcOutput *Outputs;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    IpChannel;
  UINT32    Index;
  UINT32    Offset;
  UINT8     McChMask;
  UINT8     MaxChannel;
  BOOLEAN   Lpddr;
  MC0_REQ0_CR_CPGC2_ADDRESS_INSTRUCTION_0_STRUCT  Cpgc2AddrInstruct;
  MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_STRUCT           Cpgc2AddrSize;
  MC0_REQ0_CR_CPGC2_REGION_LOW_STRUCT             Cpgc2Low;
//  MrcDebug      *Debug;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Lpddr      = Outputs->Lpddr;
//  Debug = &Outputs->Debug;

//  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AddressOrder: %u, AddressDirection: %u, LastValidInstruct: %u\n", AddressOrder, AddressDirection, LastValidInstruct);
//  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RowStart: %u, RowSizeBits: 0x%x\n", RowStart, RowSizeBits);
//  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ColStart: %u, ColSizeBits: 0x%x\n", ColStart, ColSizeBits);
  Cpgc2AddrInstruct.Data = 0;
  Cpgc2AddrInstruct.Bits.Address_Order = AddressOrder;
  Cpgc2AddrInstruct.Bits.Address_Direction = AddressDirection;

  Cpgc2Low.Data = 0;
  Cpgc2Low.Bits.Low_Row = RowStart;
  Cpgc2Low.Bits.Low_Col = ColStart;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC2_ADDRESS_INSTRUCTION_0_REG,
                MC1_REQ0_CR_CPGC2_ADDRESS_INSTRUCTION_0_REG, Controller,
                MC0_REQ1_CR_CPGC2_ADDRESS_INSTRUCTION_0_REG, IpChannel);
      for (Index = 0; Index < CPGC20_MAX_ADDRESS_INSTRUCTION; Index++) {
        Cpgc2AddrInstruct.Bits.Last = (LastValidInstruct == Index) ? 1 : 0;
        MrcWriteCR8 (MrcData, Offset, Cpgc2AddrInstruct.Data);
        Offset += MC0_REQ0_CR_CPGC2_ADDRESS_INSTRUCTION_1_REG - MC0_REQ0_CR_CPGC2_ADDRESS_INSTRUCTION_0_REG;
        if (LastValidInstruct == Index) {
          break;
        }
      }
      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC2_REGION_LOW_REG,
                MC1_REQ0_CR_CPGC2_REGION_LOW_REG, Controller,
                MC0_REQ1_CR_CPGC2_REGION_LOW_REG, IpChannel);
      MrcWriteCR64 (MrcData, Offset, Cpgc2Low.Data);

      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_REG,
                MC1_REQ0_CR_CPGC2_ADDRESS_SIZE_REG, Controller,
                MC0_REQ1_CR_CPGC2_ADDRESS_SIZE_REG, IpChannel);
      Cpgc2AddrSize.Data = MrcReadCR64 (MrcData, Offset);
      // Row and Col bit sizes in BlockSize and RegionSize registers are exponential. Bank and Rank are linear "+1".
      Cpgc2AddrSize.Bits.Block_Size_Bits_Row  = EnCADB ? 0 : RowSizeBits; // In command stress we want Row to be updated together with Bank, so block size is zero
      Cpgc2AddrSize.Bits.Block_Size_Bits_Col  = EnCADB ? 2 : ColSizeBits; // In command stress we want Bank / Row update every 4 CLs
      Cpgc2AddrSize.Bits.Region_Size_Bits_Row = RowSizeBits;
      Cpgc2AddrSize.Bits.Region_Size_Bits_Col = ColSizeBits;
      Cpgc2AddrSize.Bits.Block_Size_Max_Bank = (BankSize) ? BankSize - 1 : 0;
      Cpgc2AddrSize.Bits.Region_Size_Max_Bank = Cpgc2AddrSize.Bits.Block_Size_Max_Bank;
      MrcWriteCR64 (MrcData, Offset, Cpgc2AddrSize.Data);
    }
  }
}

/**
This function sets up Address related registers (ADDRESS_INSTRUCTION, REGION_LOW_ROW, REGION_LOW_COL, ADDRESS_SIZE) to all enabled CPGC engines.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Targeted Controller
  @param[in]  Channel     - Targeted Channel
  @param[in]  RowStart    - Starting Row Address
  @param[in]  ColStart    - Starting Column Address
  @param[in]  BankGroup   - Targeted Bank Group
  @param[in]  BankAddress - Targeted Bank Address

  @retval MrcStatus       - mrcSuccess if no errors encountered
**/
MrcStatus
Cpgc20AddressSetupPPR (
  IN  MrcParameters *const MrcData,
  IN  UINT32 const         McChBitMask,
  IN  UINT32               RowStart,
  IN  UINT16               ColStart,
  IN  UINT8                RowSizeBits,
  IN  UINT8                ColSizeBits,
  IN  UINT32               BlockSizeMaxBank,
  IN  UINT32               BankStart
  )
{
  MrcOutput           *Outputs;
  UINT32              IpChannel;
  UINT32              Offset;
  UINT32              Controller;
  UINT32              Channel;
  UINT32              LocalMcChMask;
  UINT8               MaxChannel;
  BOOLEAN             Lpddr;
  MC0_REQ0_CR_CPGC2_REGION_LOW_STRUCT     Cpgc2Low;
  MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_STRUCT   Cpgc2AddrSize;

  Outputs         = &MrcData->Outputs;
  MaxChannel      = Outputs->MaxChannels;
  LocalMcChMask   = McChBitMask & Outputs->McChBitMask;
  Lpddr           = Outputs->Lpddr;

  Cpgc2Low.Data = 0;
  Cpgc2Low.Bits.Low_Row  = RowStart;
  Cpgc2Low.Bits.Low_Col  = ColStart;
  Cpgc2Low.Bits.Low_Bank = BankStart;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (LocalMcChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH(
        MC0_REQ0_CR_CPGC2_REGION_LOW_REG,
        MC1_REQ0_CR_CPGC2_REGION_LOW_REG, Controller,
        MC0_REQ1_CR_CPGC2_REGION_LOW_REG, IpChannel);
      MrcWriteCR64 (MrcData, Offset, Cpgc2Low.Data);

      Offset = OFFSET_CALC_MC_CH (
        MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_REG,
        MC1_REQ0_CR_CPGC2_ADDRESS_SIZE_REG, Controller,
        MC0_REQ1_CR_CPGC2_ADDRESS_SIZE_REG, IpChannel);
      Cpgc2AddrSize.Data = MrcReadCR64 (MrcData, Offset);
      // Row and Col bit sizes in BlockSize and RegionSize registers are exponential. Bank and Rank are linear "+1".
      Cpgc2AddrSize.Bits.Block_Size_Bits_Row  = 0;
      Cpgc2AddrSize.Bits.Block_Size_Bits_Col  = ColSizeBits;
      Cpgc2AddrSize.Bits.Region_Size_Bits_Row = RowSizeBits;
      Cpgc2AddrSize.Bits.Region_Size_Bits_Col = ColSizeBits;
      Cpgc2AddrSize.Bits.Block_Size_Max_Bank  = BlockSizeMaxBank;
      Cpgc2AddrSize.Bits.Region_Size_Max_Bank = BlockSizeMaxBank;
      if (BlockSizeMaxBank != 0) {
        Cpgc2AddrSize.Bits.REQUEST_DATA_SIZE = 1; // 64B transactions
      }
      MrcWriteCR64 (MrcData, Offset, Cpgc2AddrSize.Data);
    }
  }

  return mrcSuccess;
}


/**
  This function sets up ADDRESS_SIZE register per channel according to the Bank/Row/Col size of the given Rank.
  This is used in memory scrubbing.
  The register will be updated on all enabled CPGC engines.

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  Rank              - Rank to work on

**/
void
Cpgc20AddressSetupScrub (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Rank
  )
{
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcControllerOut  *ControllerOut;
  MrcDimmOut        *DimmOut;
  UINT8             IpChannel;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             BankCount;
  UINT16            Columns;
  UINT32            Offset;
  UINT8             MaxChannel;
  UINT32            RowSizeBits;
  UINT32            ColSizeBits;
  UINT32            Burst;
  BOOLEAN           Lpddr;
  UINT8             McChMask;
  BOOLEAN           Ddr4;
  BOOLEAN           Ddr5;
  MRC_BG_BANK_PAIR  *BankMapping;
  MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_STRUCT  Cpgc2AddrSize;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  MaxChannel = Outputs->MaxChannels;
  McChMask = Outputs->McChBitMask;
  Lpddr = Outputs->Lpddr;
  Ddr4 = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5 = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      ControllerOut = &Outputs->Controller[Controller];
      // We want to scrub the whole rank, so get the Bank/Row/Col size from this DIMM on this channel
      DimmOut = &ControllerOut->Channel[Channel].Dimm[RANK_TO_DIMM_NUMBER (Rank)];
      BankCount = DimmOut->BankGroups * DimmOut->Banks;
      RowSizeBits = MrcLog2 (DimmOut->RowSize) - 1;
      ColSizeBits = MrcLog2 (DimmOut->ColumnSize) - 1;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Cpgc20AddressSetupScrub: Mc%d Ch%d R%d RowSizeBits = %d, ColSizeBits = %d, BankCount = %d\n", Controller, Channel, Rank, RowSizeBits, ColSizeBits, BankCount);

      // BL=8:  column increment is 2^3 per CL --> 10 - 3 = 7
      // BL=16: column increment is 2^4 per CL --> 10 - 4 = 6
      // BL=32: column increment is 2^5 per CL --> 10 - 5 = 5
      if (Ddr4) {
        ColSizeBits -= 3;
        Columns = DimmOut->ColumnSize / 8;
      } else if (Lpddr) { // LPDDR
        ColSizeBits -= 5;
        Columns = DimmOut->ColumnSize / 32;
      } else { // DDR5
        ColSizeBits -= 4;
        Columns = DimmOut->ColumnSize / 16;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
        MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_REG,
        MC1_REQ0_CR_CPGC2_ADDRESS_SIZE_REG, Controller,
        MC0_REQ1_CR_CPGC2_ADDRESS_SIZE_REG, IpChannel);
      Cpgc2AddrSize.Data = MrcReadCR64 (MrcData, Offset);
      // Row and Col bit sizes in BlockSize and RegionSize registers are exponential. Bank and Rank are linear "+1".
      Cpgc2AddrSize.Bits.Block_Size_Bits_Row = RowSizeBits;
      Cpgc2AddrSize.Bits.Block_Size_Bits_Col = ColSizeBits;
      Cpgc2AddrSize.Bits.Region_Size_Bits_Row = RowSizeBits;
      Cpgc2AddrSize.Bits.Region_Size_Bits_Col = ColSizeBits;
      Cpgc2AddrSize.Bits.Block_Size_Max_Bank = (BankCount) ? BankCount - 1 : 0;
      Cpgc2AddrSize.Bits.Region_Size_Max_Bank = Cpgc2AddrSize.Bits.Block_Size_Max_Bank;
      MrcWriteCR64 (MrcData, Offset, Cpgc2AddrSize.Data);

      // Update BASE_REPEATS to match the required number of Writes
      Burst = DimmOut->RowSize * Columns * BankCount;
      Cpgc20BaseRepeatsMcCh (MrcData, Controller, IpChannel, Burst, 1);

      if (Ddr4 || Ddr5) {
        // Program Bank Logical to Physical mapping, to cover all banks on this DIMM while allowing B2B traffic for maximum speed
        if (Ddr4) {
          BankMapping = (BankCount == 16) ? Ddr4x8BankMapB2B : Ddr4x16BankMapB2B;
        } else { // Ddr5
          if (BankCount == 8) {
            BankMapping = Ddr5_8Gbx16BankMapB2B;
          } else if (BankCount == 16) {
            BankMapping = (DimmOut->DensityIndex == MRC_SPD_SDRAM_DENSITY_8Gb) ? Ddr5_8Gbx8BankMapB2B : Ddr4x8BankMapB2B; // DDR5 16Gb x16 has 4 bank groups / 4 banks, so L2P table is same as DDR4 x8
          } else { // 32 banks
            BankMapping = Ddr5_16Gbx8BankMapB2B;
          }
        }
        MrcGetSetBankSequence (MrcData, Controller, Channel, BankMapping, BankCount, MRC_SET);
      }
    }
  }
}

/**
  This function sets up Address related registers for CMD stress to all enabled CPGC engines.

  @param[in]  MrcData           - Pointer to MRC global data.

  @retval Nothing.
**/
void
Cpgc20AddressSetupCmdStress (
  IN  MrcParameters *const  MrcData
  )
{
  MrcOutput     *Outputs;
  UINT8         Controller;
  UINT8         Channel;
  UINT8         IpChannel;
  UINT8         McChMask;
  UINT8         MaxChannel;
  UINT32        Offset;
  BOOLEAN       Lpddr;
  MC0_REQ0_CR_CPGC2_ADDRESS_PRBS_POLY_STRUCT     AddressPrbsPoly;
  MC0_REQ0_CR_CPGC2_BASE_ADDRESS_CONTROL_STRUCT  BaseAddressControl;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Lpddr      = Outputs->Lpddr;

  AddressPrbsPoly.Data = 0;
  // Keep Address_PRBS_Rank_Poly = 0, we update ranks linearly and disable Rank PRBS
  // Keep Address_PRBS_Bank_Poly = 0, we update banks linearly and disable Bank PRBS
  // @todo How to handle asymmetric configs ? (Rank0 is x8 and Rank2 is x16 - different numer of banks)
  AddressPrbsPoly.Bits.Address_PRBS_Row_Poly    = CPGC_PRBS_14; // LP4 has 14/15/16 row bits, DDR4 has 14/15/16/17 row bits. @todo: Are we missing a stress for row bits 15/16/17 here ? Asymmetric configs are a problem..

  // LP4, LP5, DDR4 and DDR5 have 10 column bits.
  // DDR4: BL = 2^3, 10-3=7
  // DDR5: BL = 2^4, 10-4=6
  // LP4:  BL = 2^5, 10-5=5
  AddressPrbsPoly.Bits.Address_PRBS_Column_Poly = Lpddr ? CPGC_PRBS_5 : ((Outputs->DdrType == MRC_DDR_TYPE_DDR5) ? CPGC_PRBS_6 : CPGC_PRBS_7);

  BaseAddressControl.Data = 0;
  // Keep Addr_Prbs_Rnk_En = 0
  // Keep Addr_Prbs_Bnk_En = 0;
  BaseAddressControl.Bits.Addr_Prbs_Row_En = 1;
  BaseAddressControl.Bits.Addr_Prbs_Col_En = 1;
  BaseAddressControl.Bits.Addr_Prbs_Carry_Mode = 1;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC2_ADDRESS_PRBS_POLY_REG,
                MC1_REQ0_CR_CPGC2_ADDRESS_PRBS_POLY_REG, Controller,
                MC0_REQ1_CR_CPGC2_ADDRESS_PRBS_POLY_REG, IpChannel);
      MrcWriteCR64 (MrcData, Offset, AddressPrbsPoly.Data);

      Offset = OFFSET_CALC_MC_CH (
                MC0_REQ0_CR_CPGC2_BASE_ADDRESS_CONTROL_REG,
                MC1_REQ0_CR_CPGC2_BASE_ADDRESS_CONTROL_REG, Controller,
                MC0_REQ1_CR_CPGC2_BASE_ADDRESS_CONTROL_REG, IpChannel);
      MrcWriteCR (MrcData, Offset, BaseAddressControl.Data);
    }
  }
}

/**
  This function sets up Base Repeats to all enabled CPGC engines.

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  Burst             - Number of Cachelines (Should be power of 2)
  @param[in]  Ranks             - # of Ranks to test

  @retval Value written to CPGC2_BASE_REPEATS.
**/
UINT32
Cpgc20BaseRepeats (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Burst,
  IN  UINT32                Ranks
  )
{
  MrcOutput     *Outputs;
  UINT8         Controller;
  UINT8         Channel;
  UINT8         IpChannel;
  UINT8         McChMask;
  UINT8         MaxChannel;
  UINT32        BaseRepeats;
  BOOLEAN       Lpddr;

  Outputs     = &MrcData->Outputs;
  MaxChannel  = Outputs->MaxChannels;
  McChMask    = Outputs->McChBitMask;
  BaseRepeats = 0;
  Lpddr = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        // For LPDDR4/5, only program register on even channels.
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      BaseRepeats = Cpgc20BaseRepeatsMcCh (MrcData, Controller, IpChannel, Burst, Ranks);
    }
  }
  return BaseRepeats;
}

/**
  This function sets up Base Repeats for a given CPGC engine (per Controller / Channel).
  The function doesn't check for mc/ch presence. Lp Channel taken care of outside of the function in both use cases.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Controller to work on
  @param[in]  Channel     - Channel to work on
  @param[in]  Burst       - Number of Cachelines (Should be power of 2)
  @param[in]  Ranks       - # of Ranks to test

  @retval Value written to CPGC2_BASE_REPEATS.
**/
UINT32
Cpgc20BaseRepeatsMcCh (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Burst,
  IN  UINT32                Ranks
  )
{
  UINT32    Offset;
  UINT32    BaseRepeats;
  MC0_REQ0_CR_CPGC2_BASE_REPEATS_STRUCT  Cpgc2BaseRepeats;

  BaseRepeats = Burst * Ranks;
  BaseRepeats -= (BaseRepeats) ? 1 : 0;
  Cpgc2BaseRepeats.Data = 0;
  Cpgc2BaseRepeats.Bits.Base_Repeats = BaseRepeats;
  Offset = OFFSET_CALC_MC_CH (
            MC0_REQ0_CR_CPGC2_BASE_REPEATS_REG,
            MC1_REQ0_CR_CPGC2_BASE_REPEATS_REG, Controller,
            MC0_REQ1_CR_CPGC2_BASE_REPEATS_REG, Channel);
  MrcWriteCR (MrcData, Offset, Cpgc2BaseRepeats.Data);

  return BaseRepeats;
}

/**
  This function sets up Registers for Basic Mem Test to all enabled CPGC engines.

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  Burst             - Number of CL transactions per algorithm instruction within 1 BlockRepeat (outer loop)
  @param[in]  LoopCount         - Total # of CL transactions
  @param[in]  Ranks             - # of Ranks

  @retval Nothing.
**/
void
Cpgc20BasicMemTest (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Burst,
  IN  UINT32                LoopCount,
  IN  UINT8                 Ranks
  )
{
  MrcOutput  *Outputs;
  UINT8      Byte;
  UINT32     BlockRepeats;
  UINT32     BaseRepeats;
  UINT32     Offset;
  UINT8      Controller;
  UINT8      Channel;
  UINT8      IpChannel;
  UINT8      MaxChannel;
  BOOLEAN    Lpddr;
  MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_STRUCT          Cpgc2AddrSize;

  Outputs      = &MrcData->Outputs;
  MaxChannel   = Outputs->MaxChannels;
  Lpddr        = Outputs->Lpddr;

  BlockRepeats = Burst * Ranks;
  // Divide by 0 prevention
  BlockRepeats = MAX (BlockRepeats, 1);
  BlockRepeats = (LoopCount / BlockRepeats);
  BlockRepeats -= (BlockRepeats) ? 1 : 0;

  MrcSetLoopcount (MrcData, BlockRepeats);
  BaseRepeats = Cpgc20BaseRepeats (MrcData, Burst, Ranks);
  Cpgc20ConfigPgRotation (MrcData, MrcLog2(BaseRepeats + 1));
  for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
    MrcSetupErrCounterCtl (MrcData, Byte, ErrCounterCtlPerByte, 0); //Counts errors per Byte Group, 1 count per UI with an error across all lanes in the Byte Group, all UI
  } // for Byte

  // Update rank count in ADDRESS_SIZE
  // This will be used in SelectReutRanks in order to update BaseRepeats and PG rotation according to the actual number of ranks in the test.
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (Outputs->McChBitMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        // For LPDDR4/5, only program register on even channels.
        continue;
      }
      IpChannel = LP_IP_CH(Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
        MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_REG,
        MC1_REQ0_CR_CPGC2_ADDRESS_SIZE_REG, Controller,
        MC0_REQ1_CR_CPGC2_ADDRESS_SIZE_REG, IpChannel);
      Cpgc2AddrSize.Data = MrcReadCR64 (MrcData, Offset);
      Cpgc2AddrSize.Bits.Block_Size_Max_Rank  = Ranks - 1;
      Cpgc2AddrSize.Bits.Region_Size_Max_Rank = Ranks - 1;
      MrcWriteCR64 (MrcData, Offset, Cpgc2AddrSize.Data);
    }
  }
}

/**
  This function adjusts register settings that are based on # of ranks being tested for the Controller/Channel provided.

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  Controller        - Controller to work on
  @param[in]  Channel           - Channel to work on
  @param[in]  OrigRank          - Original # of Ranks
  @param[in]  NewRank           - New # of Ranks

  @retval Nothing.
**/
void
Cpgc20AdjustNumOfRanks (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT8                 OrigRank,
  IN  UINT8                 NewRank
  )
{
  MrcOutput                              *Outputs;
  UINT8                                  McChMask;
  UINT8                                  MaxChannel;
  UINT32                                 IpChannel;
  UINT32                                 Burst;
  UINT32                                 Offset;
  UINT32                                 BaseRepeats;
  BOOLEAN                                Lpddr;
  MC0_REQ0_CR_CPGC2_BASE_REPEATS_STRUCT  CpgcBaseRepeats;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Lpddr = Outputs->Lpddr;
  if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
    return;
  }
  IpChannel = LP_IP_CH (Lpddr, Channel);

  Offset = OFFSET_CALC_MC_CH (
            MC0_REQ0_CR_CPGC2_BASE_REPEATS_REG,
            MC1_REQ0_CR_CPGC2_BASE_REPEATS_REG, Controller,
            MC0_REQ1_CR_CPGC2_BASE_REPEATS_REG, IpChannel);
  CpgcBaseRepeats.Data = MrcReadCR (MrcData, Offset);
  if (OrigRank == 0) {
    OrigRank = 1;  // Divide by 0 prevention
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "%s Cpgc20AdjustNumOfRanks: OrigRank is zero!", gErrString);
  }
  Burst = (CpgcBaseRepeats.Bits.Base_Repeats + 1) / OrigRank;
  BaseRepeats = Cpgc20BaseRepeatsMcCh (MrcData, Controller, IpChannel, Burst, NewRank);
  Cpgc20ConfigPgRotation (MrcData, MrcLog2 (BaseRepeats + 1));
}

/**
  This function programs the specified value of MaxBanks to the Raster Repo Mode 3 register

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  McChBitMask  - Bitmask of participating controllers / channels
  @param[in]  MaxBanks     - Maximum number of Banks used in Raster Repo Mode 3

  @retval nothing
**/
void
Cpgc20SetRasterRepoMode3MaxBanks (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 McChBitMask,
  IN  UINT8                 MaxBanks
  )
{
  MrcOutput                                 *Outputs;
  //UINT8                                     McChMask;
  UINT8                                     Controller;
  UINT8                                     Channel;
  UINT8                                     MaxChannel;
  UINT32                                    IpChannel;
  UINT32                                    Offset;
  BOOLEAN                                   Ddr5;
  MC0_CH0_CR_CPGC2_RASTER_MODE3_MAX_STRUCT  Mode3Max;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  //McChMask   = Outputs->McChBitMask;
  Ddr5       = Outputs->DdrType == MRC_DDR_TYPE_DDR5;

  Mode3Max.Data = 0;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) == 0) {
        return;
      }
      IpChannel = DDR5_IP_CH (Ddr5, Channel);
      Offset = OFFSET_CALC_MC_CH (
        MC0_CH0_CR_CPGC2_RASTER_MODE3_MAX_REG,
        MC1_CH0_CR_CPGC2_RASTER_MODE3_MAX_REG, Controller,
        MC0_CH1_CR_CPGC2_RASTER_MODE3_MAX_REG, IpChannel);
      Mode3Max.Bits.FAIL_MAX = MaxBanks;
      MrcWriteCR (MrcData, Offset, Mode3Max.Data);
    }
  }
}

/**
  This function programs the Raster Repo Configuration register based on input values.

  @param[in]  MrcData         - Pointer to MRC global data.
  @param[in]  McChBitMask     - Bitmask of participating controllers / channels
  @param[in]  Mode3Banks      - Specifies which Bank mode to use in Raster Repo Mode 3 usage.
  @param[in]  UpperBanks      - Indicates the base Bank to store when using Raster Repo Mode 3
  @param[in]  StopOnRaster    - If TRUE, the test will stop after status condition is met in Raster Repo
  @param[in]  RasterRepoClear - If set, Raster Repo entries will be Reset (this bit auto-clears)
  @param[in]  RasterRepoMode  - Indicates the mode of operation for Raster Repo

  @retval nothing
**/
void
Cpgc20RasterRepoConfig (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 McChBitMask,
  IN  UINT8         *const  Mode3Banks,
  IN  UINT8         *const  Mode3UpperBanks,
  IN  BOOLEAN       *const  StopOnRaster,
  IN  BOOLEAN       *const  RasterRepoClear,
  IN  UINT8         *const  RasterRepoMode
  )
{
  MrcOutput                                   *Outputs;
  UINT8                                       Controller;
  UINT8                                       Channel;
  UINT8                                       MaxChannel;
  UINT32                                      IpChannel;
  UINT32                                      Offset;
  BOOLEAN                                     Ddr5;
  MC0_CH0_CR_CPGC2_RASTER_REPO_CONFIG_STRUCT  RasterRepoConfig;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  //McChMask   = Outputs->McChBitMask;
  Ddr5       = Outputs->DdrType == MRC_DDR_TYPE_DDR5;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      IpChannel = DDR5_IP_CH (Ddr5, Channel);

      Offset = OFFSET_CALC_MC_CH (
        MC0_CH0_CR_CPGC2_RASTER_REPO_CONFIG_REG,
        MC1_CH0_CR_CPGC2_RASTER_REPO_CONFIG_REG, Controller,
        MC0_CH1_CR_CPGC2_RASTER_REPO_CONFIG_REG, IpChannel);

      RasterRepoConfig.Data = MrcReadCR (MrcData, Offset);

      if (Mode3Banks != NULL) {
        RasterRepoConfig.Bits.MODE3_BANKS = *Mode3Banks;
      }
      if (Mode3UpperBanks != NULL) {
        RasterRepoConfig.Bits.UPPER_BANKS = *Mode3UpperBanks;
      }
      if (StopOnRaster != NULL) {
        RasterRepoConfig.Bits.StopOnRaster = *StopOnRaster;
      }
      if (RasterRepoClear != NULL) {
        RasterRepoConfig.Bits.RasterRepoClear = *RasterRepoClear;
      }
      if (RasterRepoMode != NULL) {
        RasterRepoConfig.Bits.RasterRepoMode = *RasterRepoMode;
      }

      MrcWriteCR (MrcData, Offset, RasterRepoConfig.Data);
    }
  }
}

/**
  This function programs the number of Reads to mask in CPGC test, based on input value.

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  McChBitMask  - Bitmask of participating controllers / channels
  @param[in]  NumMaskReads - Number of Reads to mask

  @retval 1 - if all enabled engines are done, otherwise 0
**/
void
Cpgc20MaskErrFirstNRead (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 McChBitMask,
  IN  UINT8                 NumMaskReads
  )
{
  MrcOutput                                         *Outputs;
  UINT8                                             Controller;
  UINT8                                             Channel;
  UINT8                                             MaxChannel;
  UINT32                                            IpChannel;
  UINT32                                            Offset;
  BOOLEAN                                           Ddr5;
  MC0_CH0_CR_CPGC2_MASK_ERRS_FIRST_N_READS_STRUCT   MaskErrsFirstNReads;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  Ddr5       = Outputs->DdrType == MRC_DDR_TYPE_DDR5;

  MaskErrsFirstNReads.Data = 0;
  MaskErrsFirstNReads.Bits.Mask_First_N_Reads = NumMaskReads;


  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) == 0) {
        return;
      }
      IpChannel = DDR5_IP_CH (Ddr5, Channel);

      Offset = OFFSET_CALC_MC_CH (
        MC0_CH0_CR_CPGC2_MASK_ERRS_FIRST_N_READS_REG,
        MC1_CH0_CR_CPGC2_MASK_ERRS_FIRST_N_READS_REG, Controller,
        MC0_CH1_CR_CPGC2_MASK_ERRS_FIRST_N_READS_REG, IpChannel);

      MrcWriteCR (MrcData, Offset, MaskErrsFirstNReads.Data);
    }
  }
}

/**
  This function Reads the Repo Content registers, and returns those values in an input array.

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  Controller   - Controller to work on
  @param[in]  Channel      - Channel to work on
  @param[out] RepoStatus   - Contains the data from Repo Content registers, for 8 normal entries and 2 ECC entries

  @retval nothing
**/
void
Cpgc20ReadRasterRepoContent (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  OUT UINT64                RepoStatus[CPGC20_MAX_RASTER_REPO_CONTENT]
  )
{
  MrcOutput                                       *Outputs;
  UINT8                                           McChMask;
  UINT8                                           MaxChannel;
  UINT32                                          IpChannel;
  UINT32                                          McChOffset;
  UINT32                                          Offset;
  BOOLEAN                                         Ddr5;
  UINT8                                           RepoIndex;
  MC0_CH0_CR_CPGC2_RASTER_REPO_CONTENT_0_STRUCT   RasterRepoContent;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Ddr5       = Outputs->DdrType == MRC_DDR_TYPE_DDR5;

  if (MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) {
    return;
  }
  IpChannel = DDR5_IP_CH (Ddr5, Channel);

  McChOffset = OFFSET_CALC_MC_CH (
    MC0_CH0_CR_CPGC2_RASTER_REPO_CONTENT_0_REG,
    MC1_CH0_CR_CPGC2_RASTER_REPO_CONTENT_0_REG, Controller,
    MC0_CH1_CR_CPGC2_RASTER_REPO_CONTENT_0_REG, IpChannel);
  for (RepoIndex = 0; RepoIndex < CPGC20_NUM_RASTER_REPO_CONTENT_REG; RepoIndex++) {
    Offset = McChOffset + ((MC0_CH0_CR_CPGC2_RASTER_REPO_CONTENT_1_REG - MC0_CH0_CR_CPGC2_RASTER_REPO_CONTENT_0_REG) * RepoIndex);
    RasterRepoContent.Data = MrcReadCR64 (MrcData, Offset);
    if (RepoStatus != NULL) {
      RepoStatus[RepoIndex] = RasterRepoContent.Data;
    }
  }

  McChOffset = OFFSET_CALC_MC_CH (
    MC0_CH0_CR_CPGC2_RASTER_REPO_CONTENT_ECC_0_REG,
    MC1_CH0_CR_CPGC2_RASTER_REPO_CONTENT_ECC_0_REG, Controller,
    MC0_CH1_CR_CPGC2_RASTER_REPO_CONTENT_ECC_0_REG, IpChannel);
  for (RepoIndex = 0; RepoIndex < CPGC20_NUM_RASTER_REPO_CONTENT_ECC; RepoIndex++) {
    Offset = McChOffset + ((MC0_CH0_CR_CPGC2_RASTER_REPO_CONTENT_ECC_1_REG - MC0_CH0_CR_CPGC2_RASTER_REPO_CONTENT_ECC_0_REG) * RepoIndex);
    RasterRepoContent.Data = MrcReadCR64 (MrcData, Offset);
    if (RepoStatus != NULL) {
      RepoStatus[CPGC20_NUM_RASTER_REPO_CONTENT_REG + RepoIndex] = RasterRepoContent.Data;
    }
  }
}

/**
  @todo this function may be used with other Raster Repo Modes to further identify DRAM Devices which saw failures

  @param[in]   MrcData        - Pointer to MRC global data.
  @param[in]   Controller     - Controller to work on
  @param[in]   Channel        - Channel to work on
  @param[out]  ErrorSummary   - Pointer to ErrorSummary Array for A/B/C registers

  @retval nothing
**/
void
Cpgc20ReadErrSummary (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 Controller,
  IN  UINT8                 Channel,
  OUT UINT32        *const  ErrorSummary
  )
{
  MrcOutput                                       *Outputs;
  UINT8                                           McChMask;
  UINT8                                           MaxChannel;
  UINT32                                          IpChannel;
  UINT32                                          Offset;
  BOOLEAN                                         Ddr5;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Ddr5       = Outputs->DdrType == MRC_DDR_TYPE_DDR5;


  if (MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) {
    return;
  }
  IpChannel = DDR5_IP_CH (Ddr5, Channel);

  Offset = OFFSET_CALC_MC_CH (
    MC0_CH0_CR_CPGC2_ERR_SUMMARY_A_REG,
    MC1_CH0_CR_CPGC2_ERR_SUMMARY_A_REG, Controller,
    MC0_CH1_CR_CPGC2_ERR_SUMMARY_A_REG, IpChannel);

  // @todo: ECC?
  ErrorSummary[0]  = MrcReadCR (MrcData, Offset);

  Offset += MC0_CH0_CR_CPGC2_ERR_SUMMARY_B_REG - MC0_CH0_CR_CPGC2_ERR_SUMMARY_A_REG;
  ErrorSummary[1]  = MrcReadCR (MrcData, Offset);

  Offset += MC0_CH0_CR_CPGC2_ERR_SUMMARY_B_REG - MC0_CH0_CR_CPGC2_ERR_SUMMARY_A_REG;
  ErrorSummary[2]  = MrcReadCR (MrcData, Offset);
}

/**
  This function reads the Raster Repo Status register and parses the individual fields into the input pointers.

  @param[in]   MrcData        - Pointer to MRC global data.
  @param[in]   Controller     - Controller to work on
  @param[in]   Channel        - Channel to work on
  @param[out]  BitmapValid    - Optional pointer, if provided it will be updated with indication of valid bits in Repo Status
  @param[out]  SummaryValid   - Optional pointer, if provided it will be updated with indication that test summary in Raster Repo is valid
  @param[out]  FailExcessAll  - Optional pointer, if provided it will be updated with indication of any bank that observed a number of failures that exceeded MAX_BANKS setting
  @param[out]  FailAnyAll     - Optional pointer, if provided it will be updated with indication if any bank failed
  @param[out]  NumErrLogged   - Optional pointer, if provided it will be updated with number of errors logged
  @param[out]  RasterRepoFull - Optional pointer, if provided it will be updated with indication of RasterRepo being full

  @retval nothing
**/
void
Cpgc20ReadRasterRepoStatus (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 Controller,
  IN  UINT8                 Channel,
  OUT UINT8         *const  BitmapValid,
  OUT UINT8         *const  SummaryValid,
  OUT UINT8         *const  FailExcessAll,
  OUT UINT8         *const  FailAnyAll,
  OUT UINT8         *const  NumErrLogged,
  OUT UINT8         *const  RasterRepoFull
  )
{
  MrcOutput                                   *Outputs;
  UINT8                                       McChMask;
  UINT8                                       MaxChannel;
  UINT32                                      IpChannel;
  UINT32                                      Offset;
  BOOLEAN                                     Ddr5;
  //UINT8                                       RepoIndex;
  MC0_CH0_CR_CPGC2_RASTER_REPO_STATUS_STRUCT  RasterRepoStatus;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = Outputs->McChBitMask;
  Ddr5       = Outputs->DdrType == MRC_DDR_TYPE_DDR5;


  if (MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) {
    return;
  }
  IpChannel = DDR5_IP_CH (Ddr5, Channel);

  Offset = OFFSET_CALC_MC_CH (
    MC0_CH0_CR_CPGC2_RASTER_REPO_STATUS_REG,
    MC1_CH0_CR_CPGC2_RASTER_REPO_STATUS_REG, Controller,
    MC0_CH1_CR_CPGC2_RASTER_REPO_STATUS_REG, IpChannel);
  RasterRepoStatus.Data = (UINT8) MrcReadCR (MrcData, Offset);
  
  if (BitmapValid != NULL) {
    *BitmapValid = RasterRepoStatus.Bits.BitmapValid;
  }
  if (SummaryValid != NULL) {
    *SummaryValid = RasterRepoStatus.Bits.SummaryValid;
  }
  if (FailExcessAll != NULL) {
    *FailExcessAll = RasterRepoStatus.Bits.FailExcessAll;
  }
  if (FailAnyAll != NULL) {
    *FailAnyAll = RasterRepoStatus.Bits.FailAnyAll;
  }
  if (NumErrLogged != NULL) {
    *NumErrLogged = RasterRepoStatus.Bits.NumErrLogged;
  }
  if (RasterRepoFull != NULL) {
    *RasterRepoFull = RasterRepoStatus.Bits.RasterRepoFull;
  }
}

/**
Setup CPGC 2 for Standard Write/Read Loopback test pattern

@param[in]  Socket              Socket number
@param[in]  Ch                  Channel number
@param[in]  Subch               Sub channel number
@param[in]  PsCh               Pseudo channel number
@param[in]  SubSeqWait          Number of Dclks to stall at the end of a sub-sequence
@param[in]  IsAddrUp            Set address traverse direction. TRUE - up direction (incremental), FALSE - down direction (decremental)
@param[in]  IsDataInvt          Set data inversion. TRUE - data inversion, FALSE - data no inversion

@retval N/A

**/
VOID
SetCpgcCmdPatRd (
  IN MrcParameters *const   MrcData,
  IN UINT8                  Controller,
  IN UINT8                  Channel,
  IN UINT16                 SubSeqWait,
  IN BOOLEAN                IsAddrUp,
  IN BOOLEAN                IsDataInvt
)
{
  MC0_REQ0_CR_CPGC2_DATA_INSTRUCTION_0_STRUCT            DataInstruction;
  MC0_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_0_STRUCT         CommandInstruction;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT       AlgoInstruction;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_STRUCT  AlgoInstructionControl;
  MC0_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_STRUCT  AlgoWaitEventControl;

  //
  // 1 data instruction
  //
  DataInstruction.Data = 0;
  DataInstruction.Bits.Last = 1;
  DataInstruction.Bits.Background_Mode = 1; // Chunk inversion based on data_background
  DataInstruction.Bits.Data_Background = 0; // No chunk inversion
  Cpgc20DataInstructWrite (MrcData, &DataInstruction, 0x1);

  // 1 algorithm instructions
  // (read)
  AlgoInstruction.Data = 0;
  AlgoInstruction.Bits.Command_Start_Pointer = 0; // Point to read command instruction
  if (IsAddrUp) {
    AlgoInstruction.Bits.Inverse_Direction = 0; // address up, incremental
  } else {
    AlgoInstruction.Bits.Inverse_Direction = 1; // address down, decremental
  }
  if (IsDataInvt) {
    AlgoInstruction.Bits.Invert_Data = 1; // data inversion
  } else {
    AlgoInstruction.Bits.Invert_Data = 0; // data no inversion
  }
  AlgoInstruction.Bits.Last = 1;

  // set wait time between each algorithms
  AlgoInstructionControl.Data = 0;
  AlgoInstructionControl.Bits.Wait_Event_Start = 0; // Do not wait for an event for algo to start
  AlgoInstructionControl.Bits.Wait_Count_Start = 1; // Wait for timer before starting (introduces bubble between bursts)
  AlgoInstructionControl.Bits.Wait_Count_Clear = 1; // Reset the wait timer at the start of this algo instruction
  AlgoInstructionControl.Bits.BE_Train_Err_En = 0; // Don't do byte enable checking
  AlgoInstructionControl.Bits.Select_On = 1; // Enable CADB to replace CMD with CADB
  AlgoInstructionControl.Bits.Deselect_On = 0; // Do not enable CADB to drive deselect traffic for this algo
  AlgoInstructionControl.Bits.Base_Range_Row = 0; // Use base_repeats and not base_col_repeats
  AlgoInstructionControl.Bits.FastY_Init = 0; // Do not override address_direction setting

  AlgoWaitEventControl.Data = 0;
  AlgoWaitEventControl.Bits.Select_Event = 0; // Don't care for Wait_Event_Start = 0
  
  // Disable the event wait timer.
  AlgoWaitEventControl.Bits.Timer = 0x4;
  AlgoWaitEventControl.Bits.Wait_Clock_Frequency = CPGC20_NATIVE_DUNIT_FREQ; // Native CPGC clock
  AlgoWaitEventControl.Bits.Wait_Time = SubSeqWait;
  
  Cpgc20AlgoInstructWrite (MrcData, &AlgoInstruction, AlgoInstructionControl, AlgoWaitEventControl, 0x1); // 1 valid command
  
  // 1 command instructions
  // (read)
  CommandInstruction.Data = 0;
  CommandInstruction.Bits.WriteCmd = 0; // read
  CommandInstruction.Bits.Last = 1;

  Cpgc20CmdInstructWrite (MrcData, &CommandInstruction, 1);
}

/**
Setup CPGC 2 for (rD), (wI) test pattern. It's currently only used by memory test.

@param[in]  Socket              - Socket number
@param[in]  Ch                  - Channel number
@param[in]  Subch               - Sub channel number
@param[in]  Psch                - Pseudo channel number
@param[in]  SubSeqWait          - Number of Dclks to stall after each sub-sequences
@param[in]  IsAddrUp            - Set address traverse direction. TRUE - up direction (incremental), FALSE - down direction (decremental)
@param[in]  IsReadDataInvt      - Set read data inversion. TRUE - data inversion, FALSE - data no inversion
@param[in]  IsWriteDataInvt     - Set write data inversion. TRUE - data inversion, FALSE - data no inversion

@retval N/A

**/
VOID
SetCpgcCmdPatRdWr (
  IN MrcParameters *const   MrcData,
  IN UINT8                  Controller,
  IN UINT8                  Channel,
  IN UINT16                 SubSeqWait,
  IN BOOLEAN                IsAddrUp,
  IN BOOLEAN                IsReadDataInvt,
  IN BOOLEAN                IsWriteDataInvt
)
{
  MC0_REQ0_CR_CPGC2_DATA_INSTRUCTION_0_STRUCT            DataInstruction;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT       AlgoInstruction;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_STRUCT  AlgoInstructionControl;
  MC0_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_STRUCT  AlgoWaitEventControl;
  MC0_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_0_STRUCT         CommandInstruction[3];
  UINT32                                                 Data32;
  UINT32                                                 BaseOffset;
  UINT32                                                 BaseCtlOffset;
  UINT32                                                 InstructionOffset;
  UINT32                                                 InstructionCtlOffset;
  UINT32                                                 Offset;
  UINT32                                                 CtlOffset;
  UINT8                                                  IpChannel;

  IpChannel = LP_IP_CH (MrcData->Outputs.Lpddr, Channel);
  BaseOffset = OFFSET_CALC_MC_CH (
    MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_REG,
    MC1_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_REG, Controller,
    MC0_REQ1_CR_CPGC2_ALGORITHM_INSTRUCTION_0_REG, IpChannel);
  InstructionOffset = MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_1_REG - MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_REG;

  BaseCtlOffset = OFFSET_CALC_MC_CH (
    MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_REG,
    MC1_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_REG, Controller,
    MC0_REQ1_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_REG, IpChannel);
  InstructionCtlOffset = MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_1_REG - MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_REG;

  AlgoInstruction.Data = 0;
  AlgoInstruction.Bits.Command_Start_Pointer = 0; // Point to the first command instruction - read command instruction
  AlgoInstruction.Bits.Last = 0;
  if (IsAddrUp) {
    AlgoInstruction.Bits.Inverse_Direction = 0; // address up, incremental
  } else {
    AlgoInstruction.Bits.Inverse_Direction = 1; // address down, decremental
  }
  if (IsReadDataInvt) {
    AlgoInstruction.Bits.Invert_Data = 1; // data inversion
  } else {
    AlgoInstruction.Bits.Invert_Data = 0; // data no inversion
  }
  MrcWriteCR8 (MrcData, BaseOffset, AlgoInstruction.Data);

  Data32 = MrcReadCR (MrcData, BaseCtlOffset); // Instruction Control 0
  AlgoInstructionControl.Data = Data32 & MRC_UINT8_MAX;
  AlgoInstructionControl.Bits.Wait_Event_Start = 0; // Do not wait for an event for algo to start
  AlgoInstructionControl.Bits.Wait_Count_Start = 0; // Wait for timer before starting (introduces bubble between bursts)
  AlgoInstructionControl.Bits.Wait_Count_Clear = 1; // Reset the wait timer at the start of this algo instruction
  AlgoInstructionControl.Bits.BE_Train_Err_En = 0; // Don't do byte enable checking
  AlgoInstructionControl.Bits.Select_On = 0; // Disable CADB to replace CMD with CADB
  AlgoInstructionControl.Bits.Deselect_On = 0; // Do not enable CADB to drive deselect traffic for this algo
  AlgoInstructionControl.Bits.Base_Range_Row = 0;//0, Use base_repeats and not base_col_repeats. Control base repeats. 1 controls the base cl repeats for nop and 1 nop.
  AlgoInstructionControl.Bits.FastY_Init = 0; // Do not override address_direction setting
  
  // 2. (Wait)) Instruction 1
  Offset = BaseOffset + InstructionOffset; // Instruction 1
  AlgoInstruction.Data = 0;
  AlgoInstruction.Bits.Command_Start_Pointer = 23; // Point to NOP command
  AlgoInstruction.Bits.Last = 0;
  MrcWriteCR8 (MrcData, Offset, AlgoInstruction.Data);

  CtlOffset = BaseCtlOffset + InstructionCtlOffset; // Instruction Ctl 1
  AlgoInstructionControl.Bits.Wait_Count_Start = 1;
  AlgoInstructionControl.Bits.Wait_Count_Clear = 0;
  AlgoInstructionControl.Bits.Base_Range_Row = 1;
  MrcWriteCR8 (MrcData, CtlOffset, AlgoInstructionControl.Data);

  // 3. (Write) Instruction 2
  Offset += InstructionOffset; // Instruction 2
  AlgoInstruction.Data = 0;
  AlgoInstruction.Bits.Command_Start_Pointer = 1; // Point to the first command instruction - read command instruction
  AlgoInstruction.Bits.Last = 0;
  if (IsAddrUp) {
    AlgoInstruction.Bits.Inverse_Direction = 0; // address up, incremental
  } else {
    AlgoInstruction.Bits.Inverse_Direction = 1; // address down, decremental
  }
  if (IsWriteDataInvt) {
    AlgoInstruction.Bits.Invert_Data = 1; // data inversion
  } else {
    AlgoInstruction.Bits.Invert_Data = 0; // data no inversion
  }
  MrcWriteCR8 (MrcData, Offset, AlgoInstruction.Data);

  CtlOffset += InstructionCtlOffset; // Instruction Ctl 2
  AlgoInstructionControl.Bits.Wait_Count_Start = 0;
  AlgoInstructionControl.Bits.Wait_Count_Clear = 1; // Reset the wait timer at the start of this algo instruction
  AlgoInstructionControl.Bits.Base_Range_Row = 0;
  MrcWriteCR8 (MrcData, CtlOffset, AlgoInstructionControl.Data);

  // 4. (Wait)) Instruction 3
  Offset += InstructionOffset; // Instruction 3
  Data32 = 0;
  AlgoInstruction.Data = (UINT8) Data32;
  AlgoInstruction.Bits.Command_Start_Pointer = 23; // Point to NOP command
  AlgoInstruction.Bits.Last = 1;
  MrcWriteCR8 (MrcData, Offset, AlgoInstruction.Data);

  CtlOffset += InstructionCtlOffset; // Instruction Ctl 3
  AlgoInstructionControl.Bits.Wait_Count_Start = 1;
  AlgoInstructionControl.Bits.Wait_Count_Clear = 0;
  AlgoInstructionControl.Bits.Base_Range_Row = 1;
  MrcWriteCR8 (MrcData, CtlOffset, AlgoInstructionControl.Data);

  Offset = OFFSET_CALC_MC_CH (
    MC0_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_REG,
    MC1_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_REG, Controller,
    MC0_REQ1_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_REG, IpChannel);
  
  AlgoWaitEventControl.Data = MrcReadCR (MrcData, Offset);
  AlgoWaitEventControl.Bits.Select_Event = 0; // Don't care for Wait_Event_Start = 0
  AlgoWaitEventControl.Bits.Timer = 0x4; // 2^timer
  AlgoWaitEventControl.Bits.Wait_Clock_Frequency = CPGC20_NATIVE_DUNIT_FREQ; // Native CPGC clock
  AlgoWaitEventControl.Bits.Wait_Time = SubSeqWait;
  MrcWriteCR (MrcData, Offset, AlgoWaitEventControl.Data);

  // 3 command instructions
  // Read - CMD Instruction 0
  Data32 = 0;
  CommandInstruction[0].Data = Data32 & MRC_UINT8_MAX;
  CommandInstruction[0].Bits.WriteCmd = 0; // read
  CommandInstruction[0].Bits.Alternate_Data = 0;
  CommandInstruction[0].Bits.Invert_Data = 0; // rD, no data inversion in this command instruction
  CommandInstruction[0].Bits.Last = 1;

  // Write - CMD Instruction 1
  Data32 = 0;
  CommandInstruction[1].Data = Data32 & MRC_UINT8_MAX;
  CommandInstruction[1].Bits.WriteCmd = 1; // write
  CommandInstruction[1].Bits.Alternate_Data = 0;
  CommandInstruction[1].Bits.Invert_Data = 0; // wD, no data inversion in this command instruction
  CommandInstruction[1].Bits.Last = 1; // algorithm end
  Cpgc20CmdInstructWrite (MrcData, CommandInstruction, 2);

  // Wait - CMD Instruction 23
  Data32 = 0;
  Offset = OFFSET_CALC_MC_CH (
    MC0_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_23_REG,
    MC1_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_23_REG, Controller,
    MC0_REQ1_CR_CPGC2_COMMAND_INSTRUCTION_23_REG, IpChannel);
  CommandInstruction[2].Data = Data32 & MRC_UINT8_MAX;
  CommandInstruction[2].Bits.Last = 1;
  CommandInstruction[2].Bits.Alternate_Data = 1;
  CommandInstruction[2].Bits.Offset = 1;
  MrcWriteCR8 (MrcData, Offset, CommandInstruction[2].Data);

  // Do nothing here.
  DataInstruction.Data = 0;
  DataInstruction.Bits.Last = 1;
  DataInstruction.Bits.Background_Mode = 1; // Chunk inversion based on data_background
  DataInstruction.Bits.Data_Background = 0; // No chunk inversion
  Cpgc20DataInstructWrite (MrcData, &DataInstruction, 0x1);
}

/**
Setup CPGC 2 for Write Only test pattern

@param[in]  Socket              Socket number
@param[in]  Ch                  Channel number
@param[in]  Subch               Sub channel number
@param[in]  Psch                Pseudo channel number
@param[in]  SubSeqWait          Number of Dclks to stall at the end of a sub-sequence
@param[in]  SubSeqWait          Number of Dclks to stall at the end of a sub-sequence
@param[in]  IsAddrUp            Set address traverse direction. TRUE - up direction (incremental), FALSE - down direction (decremental)
@param[in]  IsDataInvt          Set data inversion. TRUE - data inversion, FALSE - data no inversion

@retval N/A

**/
VOID
SetCpgcCmdPatWr (
  IN MrcParameters *const   MrcData,
  IN UINT8                  Controller,
  IN UINT8                  Channel,
  IN UINT16                 SubSeqWait,
  IN BOOLEAN                IsAddrUp,
  IN BOOLEAN                IsDataInvt
  )
{
  MC0_REQ0_CR_CPGC2_DATA_INSTRUCTION_0_STRUCT            DataInstruction;
  MC0_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_0_STRUCT         CommandInstruction;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT       AlgoInstruction;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_STRUCT  AlgoInstructionControl;
  MC0_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_STRUCT  AlgoWaitEventControl;

  // 1 data instruction
  DataInstruction.Data = 0;
  DataInstruction.Bits.Last = 1;
  DataInstruction.Bits.Background_Mode = 0; // Chunk inversion based on data_background
  DataInstruction.Bits.Data_Background = 0; // No chunk inversion
  Cpgc20DataInstructWrite (MrcData, &DataInstruction, 0x1);

  // 1 algorithm instructions
  // (write)
  AlgoInstruction.Data = 0;
  AlgoInstruction.Bits.Command_Start_Pointer = 0; // point to write command instruction
  AlgoInstruction.Bits.Last = 1;
  if (IsAddrUp) {
    AlgoInstruction.Bits.Inverse_Direction = 0; // address up, incremental
  } else {
    AlgoInstruction.Bits.Inverse_Direction = 1; // address down, decremental
  }
  if (IsDataInvt) {
    AlgoInstruction.Bits.Invert_Data = 1; // data inversion
  } else {
    AlgoInstruction.Bits.Invert_Data = 0; // data no inversion
  }

  // set wait time between each algorithms
  AlgoInstructionControl.Data = 0;
  AlgoInstructionControl.Bits.Wait_Count_Clear = 1;
  AlgoInstructionControl.Bits.Wait_Count_Start = 1; // Algo instruction is stalled until the Wait_Time_Current is equal to the Wait_Time.

  AlgoWaitEventControl.Data = 0;
  AlgoWaitEventControl.Bits.Select_Event = 0; // Don't care for Wait_Event_Start = 0
  AlgoWaitEventControl.Bits.Timer = CPGC20_EVENT_WAIT_TIMER_DISABLE; // Disable event wait timer
  AlgoWaitEventControl.Bits.Wait_Clock_Frequency = CPGC20_NATIVE_DUNIT_FREQ; // Native CPGC clock
  AlgoWaitEventControl.Bits.Wait_Time = SubSeqWait;

  Cpgc20AlgoInstructWrite (MrcData, &AlgoInstruction, AlgoInstructionControl, AlgoWaitEventControl, 0x1); // 1 valid instruction

  // 1 command instructions
  // (write)
  CommandInstruction.Data = 0;
  CommandInstruction.Bits.WriteCmd = 1; // Write
  CommandInstruction.Bits.Last = 1;
  Cpgc20CmdInstructWrite (MrcData, &CommandInstruction, 1);
}

/**
  Setup CPGC command pattern for test

  @param[in]  MrcData            - Pointer to MRC global data.
  @param[in]  Controller         - Controller number
  @param[in]  Channel            - Channel number
  @param[in]  CmdPat             - Command pattern for test
    - PatWrRd
    - PatWr
    - PatRd
  @param[in]  *AddressCarry      - Structure for address carry
  @param[in]  SubSeqWait         - Number of Dclks to stall at the end of a sub-sequence

  @retval N/A
**/
VOID
SetCpgcCmdPat (
  IN MrcParameters *const     MrcData,
  IN UINT8                    Controller,
  IN UINT8                    Channel,
  IN UINT8                    CmdPat,
  IN TAddressCarry            *AddressCarry,
  IN UINT16                   SubSeqWait
  )
{
  MrcOutput                                      *Outputs;
  MC0_REQ0_CR_CPGC2_ADDRESS_INSTRUCTION_0_STRUCT AddressInstruction;
  MC0_REQ0_CR_CPGC2_ADDRESS_CONTROL_STRUCT       AddressControl;
  UINT32                                         Offset;
  UINT8                                          IpChannel;
  UINT8                                          Lpddr;
  UINT8                                          MaxChannel;
  UINT8                                          McChMask;
  TAddressCarry                                  AddressCarryLocal;

  Outputs = &MrcData->Outputs;
  Lpddr = Outputs->Lpddr;
  MaxChannel = Outputs->MaxChannels;
  McChMask   = (1 << ((Controller * MaxChannel) + Channel));

  switch (CmdPat) {
  case PatWr:
  case PatWrUp:
    SetCpgcCmdPatWr (MrcData, Controller, Channel, SubSeqWait, TRUE, FALSE);
    break;
  case PatWrDown:
    SetCpgcCmdPatWr (MrcData, Controller, Channel, SubSeqWait, FALSE, FALSE);
    break;
  case PatWrUpInvt:
    SetCpgcCmdPatWr (MrcData, Controller, Channel, SubSeqWait, TRUE, TRUE);
    break;
  case PatWrDownInvt:
    SetCpgcCmdPatWr (MrcData, Controller, Channel, SubSeqWait, FALSE, TRUE);
    break;
  case PatRd:
  case PatRdUp:
    SetCpgcCmdPatRd (MrcData, Controller, Channel, SubSeqWait, TRUE, FALSE);
    break;
  case PatRdDown:
    SetCpgcCmdPatRd (MrcData, Controller, Channel, SubSeqWait, FALSE, FALSE);
    break;
  case PatRdUpInvt:
    SetCpgcCmdPatRd (MrcData, Controller, Channel, SubSeqWait, TRUE, TRUE);
    break;
  case PatRdDownInvt:
    SetCpgcCmdPatRd (MrcData, Controller, Channel, SubSeqWait, FALSE, TRUE);
    break;
  case PatRdWrUp:
    SetCpgcCmdPatRdWr (MrcData, Controller, Channel, SubSeqWait, TRUE, FALSE, FALSE);
    break;
  case PatRdWrDown:
    SetCpgcCmdPatRdWr (MrcData, Controller, Channel, SubSeqWait, FALSE, FALSE, FALSE);
    break;
  case PatRdWrUpInvt:
    SetCpgcCmdPatRdWr (MrcData, Controller, Channel, SubSeqWait, TRUE, TRUE, TRUE);
    break;
  case PatRdWrDownInvt:
    SetCpgcCmdPatRdWr (MrcData, Controller, Channel, SubSeqWait, FALSE, TRUE, TRUE);
    break;
  case PatRdWrUpAlt:
    SetCpgcCmdPatRdWr (MrcData, Controller, Channel, SubSeqWait, TRUE, FALSE, TRUE);
    break;
  case PatRdWrDownAlt:
    SetCpgcCmdPatRdWr (MrcData, Controller, Channel, SubSeqWait, FALSE, FALSE, TRUE);
    break;
  case PatRdWrUpInvtAlt:
    SetCpgcCmdPatRdWr (MrcData, Controller, Channel, SubSeqWait, TRUE, TRUE, FALSE);
    break;
  case PatRdWrDownInvtAlt:
    SetCpgcCmdPatRdWr (MrcData, Controller, Channel, SubSeqWait, FALSE, TRUE, FALSE);
    break;
  default:
    MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "Error: Unknown command pattern %d in SetCpgcCmdPat\n", CmdPat);
    break;
  } // switch cmdpat

  AddressCarryLocal.AddressOrder = AddressCarry->AddressOrder;
  AddressCarryLocal.AddressDirection = AddressCarry->AddressDirection;

  AddressInstruction.Data = 0;
  AddressInstruction.Bits.Last = 1;
  AddressInstruction.Bits.Address_Decode_Enable = 0;
  AddressInstruction.Bits.Address_Direction = AddressCarryLocal.AddressDirection;
  AddressInstruction.Bits.Address_Order = AddressCarryLocal.AddressOrder;

  AddressControl.Data = 0;
  AddressControl.Bits.Address_Decode_Rotate_Repeats = 0; // 1 repeat at address level
  AddressControl.Bits.FastY_Address_Direction = AddressCarryLocal.AddressDirection;
  AddressControl.Bits.FastY_Address_Order = AddressCarryLocal.AddressOrder;
  
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, MaxChannel) == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
        MC0_REQ0_CR_CPGC2_ADDRESS_INSTRUCTION_0_REG,
        MC1_REQ0_CR_CPGC2_ADDRESS_INSTRUCTION_0_REG, Controller,
        MC0_REQ1_CR_CPGC2_ADDRESS_INSTRUCTION_0_REG, IpChannel);
      MrcWriteCR (MrcData, Offset, AddressInstruction.Data);

      Offset = OFFSET_CALC_MC_CH (
        MC0_REQ0_CR_CPGC2_ADDRESS_CONTROL_REG,
        MC1_REQ0_CR_CPGC2_ADDRESS_CONTROL_REG, Controller,
        MC0_REQ1_CR_CPGC2_ADDRESS_CONTROL_REG, IpChannel);
      MrcWriteCR (MrcData, Offset, AddressControl.Data);

    }
  }
}
