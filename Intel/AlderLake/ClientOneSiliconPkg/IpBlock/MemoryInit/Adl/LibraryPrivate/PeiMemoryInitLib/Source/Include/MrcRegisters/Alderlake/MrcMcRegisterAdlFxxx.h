#ifndef __MrcMcRegisterAdlFxxx_h__
#define __MrcMcRegisterAdlFxxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define MC0_BC_CR_TC_PRE_REG                                           (0x0000F000)
//Duplicate of MC0_CH0_CR_TC_PRE_REG

#define MC0_BC_CR_TC_ACT_REG                                           (0x0000F008)
//Duplicate of MC0_CH0_CR_TC_ACT_REG

#define MC0_BC_CR_TC_RDRD_REG                                          (0x0000F00C)
//Duplicate of MC0_CH0_CR_TC_RDRD_REG

#define MC0_BC_CR_TC_RDWR_REG                                          (0x0000F010)
//Duplicate of MC0_CH0_CR_TC_RDWR_REG

#define MC0_BC_CR_TC_WRRD_REG                                          (0x0000F014)
//Duplicate of MC0_CH0_CR_TC_WRRD_REG

#define MC0_BC_CR_TC_WRWR_REG                                          (0x0000F018)
//Duplicate of MC0_CH0_CR_TC_WRWR_REG

#define MC0_BC_CR_PM_ADAPTIVE_CKE_REG                                  (0x0000F01C)
//Duplicate of MC0_CH0_CR_PM_ADAPTIVE_CKE_REG

#define MC0_BC_CR_SC_ROUNDTRIP_LATENCY_REG                             (0x0000F020)
//Duplicate of MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_REG

#define MC0_BC_CR_SCHED_CBIT_REG                                       (0x0000F028)
//Duplicate of MC0_CH0_CR_SCHED_CBIT_REG

#define MC0_BC_CR_SCHED_SECOND_CBIT_REG                                (0x0000F02C)
//Duplicate of MC0_CH0_CR_SCHED_SECOND_CBIT_REG

#define MC0_BC_CR_DFT_MISC_REG                                         (0x0000F030)
//Duplicate of MC0_CH0_CR_DFT_MISC_REG

#define MC0_BC_CR_SC_PCIT_REG                                          (0x0000F034)
//Duplicate of MC0_CH0_CR_SC_PCIT_REG

#define MC0_BC_CR_ECC_DFT_REG                                          (0x0000F038)
//Duplicate of MC0_CH0_CR_ECC_DFT_REG

#define MC0_BC_CR_PM_PDWN_CONFIG_REG                                   (0x0000F040)
//Duplicate of MC0_CH0_CR_PM_PDWN_CONFIG_REG

#define MC0_BC_CR_ECCERRLOG0_REG                                       (0x0000F048)
//Duplicate of MC0_CH0_CR_ECCERRLOG0_REG

#define MC0_BC_CR_ECCERRLOG1_REG                                       (0x0000F04C)
//Duplicate of MC0_CH0_CR_ECCERRLOG1_REG

#define MC0_BC_CR_TC_PWRDN_REG                                         (0x0000F050)
//Duplicate of MC0_CH0_CR_TC_PWRDN_REG

#define MC0_BC_CR_QUEUE_ENTRY_DISABLE_RPQ_REG                          (0x0000F058)

//Duplicate of MC0_CH0_CR_QUEUE_ENTRY_DISABLE_RPQ_REG

#define MC0_BC_CR_QUEUE_ENTRY_DISABLE_IPQ_REG                          (0x0000F05C)
//Duplicate of MC0_CH0_CR_QUEUE_ENTRY_DISABLE_IPQ_REG

#define MC0_BC_CR_QUEUE_ENTRY_DISABLE_WPQ_REG                          (0x0000F060)
//Duplicate of MC0_CH0_CR_QUEUE_ENTRY_DISABLE_WPQ_REG

#define MC0_BC_CR_SC_WDBWM_REG                                         (0x0000F068)
//Duplicate of MC0_CH0_CR_SC_WDBWM_REG

#define MC0_BC_CR_TC_ODT_REG                                           (0x0000F070)
//Duplicate of MC0_CH0_CR_TC_ODT_REG

#define MC0_BC_CR_MCSCHEDS_SPARE_REG                                   (0x0000F078)
//Duplicate of MC0_CH0_CR_MCSCHEDS_SPARE_REG

#define MC0_BC_CR_TC_MPC_REG                                           (0x0000F07C)
//Duplicate of MC0_CH0_CR_TC_MPC_REG

#define MC0_BC_CR_SC_ODT_MATRIX_REG                                    (0x0000F080)
//Duplicate of MC0_CH0_CR_SC_ODT_MATRIX_REG

#define MC0_BC_CR_DFT_BLOCK_REG                                        (0x0000F084)
//Duplicate of MC0_CH0_CR_DFT_BLOCK_REG

#define MC0_BC_CR_SC_GS_CFG_REG                                        (0x0000F088)
//Duplicate of MC0_CH0_CR_SC_GS_CFG_REG

#define MC0_BC_CR_SC_PH_THROTTLING_0_REG                               (0x0000F090)
//Duplicate of MC0_CH0_CR_SC_PH_THROTTLING_0_REG

#define MC0_BC_CR_SC_PH_THROTTLING_1_REG                               (0x0000F094)
//Duplicate of MC0_CH0_CR_SC_PH_THROTTLING_1_REG

#define MC0_BC_CR_SC_PH_THROTTLING_2_REG                               (0x0000F098)
//Duplicate of MC0_CH0_CR_SC_PH_THROTTLING_2_REG

#define MC0_BC_CR_SC_PH_THROTTLING_3_REG                               (0x0000F09C)
//Duplicate of MC0_CH0_CR_SC_PH_THROTTLING_3_REG

#define MC0_BC_CR_SC_WPQ_THRESHOLD_REG                                 (0x0000F0A0)
//Duplicate of MC0_CH0_CR_SC_WPQ_THRESHOLD_REG

#define MC0_BC_CR_SC_PR_CNT_CONFIG_REG                                 (0x0000F0A8)
//Duplicate of MC0_CH0_CR_SC_PR_CNT_CONFIG_REG

#define MC0_BC_CR_REUT_CH_MISC_CKE_CS_CTRL_REG                         (0x0000F0B0)
//Duplicate of MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_REG

#define MC0_BC_CR_REUT_CH_MISC_ODT_CTRL_REG                            (0x0000F0B4)
//Duplicate of MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_REG

#define MC0_BC_CR_SPID_LOW_POWER_CTL_REG                               (0x0000F0B8)
//Duplicate of MC0_CH0_CR_SPID_LOW_POWER_CTL_REG

#define MC0_BC_CR_SC_GS_CFG_TRAINING_REG                               (0x0000F0BC)
//Duplicate of MC0_CH0_CR_SC_GS_CFG_TRAINING_REG

#define MC0_BC_CR_SCHED_THIRD_CBIT_REG                                 (0x0000F0C0)
//Duplicate of MC0_CH0_CR_SCHED_THIRD_CBIT_REG

#define MC0_BC_CR_DEADLOCK_BREAKER_REG                                 (0x0000F0C4)
//Duplicate of MC0_CH0_CR_DEADLOCK_BREAKER_REG

#define MC0_BC_CR_XARB_TC_BUBBLE_INJ_REG                               (0x0000F0C8)
//Duplicate of MC0_CH0_CR_XARB_TC_BUBBLE_INJ_REG

#define MC0_BC_CR_MCSCHEDS_GLOBAL_DRIVER_GATE_CFG_REG                  (0x0000F0D0)
//Duplicate of MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_REG

#define MC0_BC_CR_SC_BLOCKING_RULES_CFG_REG                            (0x0000F0D4)
//Duplicate of MC0_CH0_CR_SC_BLOCKING_RULES_CFG_REG

#define MC0_BC_CR_PWM_DDR_SUBCH0_ACT_COUNTER_REG                       (0x0000F0D8)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_REG

#define MC0_BC_CR_PWM_DDR_SUBCH1_ACT_COUNTER_REG                       (0x0000F0E0)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_REG

#define MC0_BC_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REG             (0x0000F0E8)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REG

#define MC0_BC_CR_PWM_DDR_SUBCH1_REQ_OCCUPANCY_COUNTER_REG             (0x0000F0F0)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REG

#define MC0_BC_CR_WCK_CONFIG_REG                                       (0x0000F0F8)
//Duplicate of MC0_CH0_CR_WCK_CONFIG_REG

#define MC0_BC_CR_XARB_CFG_BUBBLE_INJ_REG                              (0x0000F100)
//Duplicate of MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_REG

#define MC0_BC_CR_TR_RRDVALID_CTRL_REG                                 (0x0000F104)
//Duplicate of MC0_CH0_CR_TR_RRDVALID_CTRL_REG

#define MC0_BC_CR_TR_RRDVALID_DATA_REG                                 (0x0000F108)
//Duplicate of MC0_CH0_CR_TR_RRDVALID_DATA_REG

#define MC0_BC_CR_WMM_READ_CONFIG_REG                                  (0x0000F110)
//Duplicate of MC0_CH0_CR_WMM_READ_CONFIG_REG

#define MC0_BC_CR_MC2PHY_BGF_CTRL_REG                                  (0x0000F114)
//Duplicate of MC0_CH0_CR_MC2PHY_BGF_CTRL_REG

#define MC0_BC_CR_SC_ADAPTIVE_PCIT_REG                                 (0x0000F118)
//Duplicate of MC0_CH0_CR_SC_ADAPTIVE_PCIT_REG

#define MC0_BC_CR_MERGE_REQ_READS_PQ_REG                               (0x0000F120)
//Duplicate of MC0_CH0_CR_MERGE_REQ_READS_PQ_REG
#define MC0_BC_CR_ROWHAMMER_CTL_REG                                    (0x0000F128)
//Duplicate of MC0_CH0_CR_ROWHAMMER_CTL_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG                 (0x0000F200)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_1_REG                 (0x0000F204)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_2_REG                 (0x0000F208)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_3_REG                 (0x0000F20C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_4_REG                 (0x0000F210)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_5_REG                 (0x0000F214)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_6_REG                 (0x0000F218)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_7_REG                 (0x0000F21C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_8_REG                 (0x0000F220)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_9_REG                 (0x0000F224)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_10_REG                (0x0000F228)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_11_REG                (0x0000F22C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_12_REG                (0x0000F230)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_13_REG                (0x0000F234)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_14_REG                (0x0000F238)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_15_REG                (0x0000F23C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_16_REG                (0x0000F240)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_17_REG                (0x0000F244)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_18_REG                (0x0000F248)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_19_REG                (0x0000F24C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_20_REG                (0x0000F250)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_21_REG                (0x0000F254)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_22_REG                (0x0000F258)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_23_REG                (0x0000F25C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_24_REG                (0x0000F260)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_25_REG                (0x0000F264)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_26_REG                (0x0000F268)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_27_REG                (0x0000F26C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_28_REG                (0x0000F270)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_29_REG                (0x0000F274)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_30_REG                (0x0000F278)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_31_REG                (0x0000F27C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_32_REG                (0x0000F280)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_33_REG                (0x0000F284)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_34_REG                (0x0000F288)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_35_REG                (0x0000F28C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_36_REG                (0x0000F290)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_37_REG                (0x0000F294)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_38_REG                (0x0000F298)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_39_REG                (0x0000F29C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_40_REG                (0x0000F2A0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_41_REG                (0x0000F2A4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_42_REG                (0x0000F2A8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_43_REG                (0x0000F2AC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_44_REG                (0x0000F2B0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_45_REG                (0x0000F2B4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_46_REG                (0x0000F2B8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_47_REG                (0x0000F2BC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_48_REG                (0x0000F2C0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_49_REG                (0x0000F2C4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_50_REG                (0x0000F2C8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_51_REG                (0x0000F2CC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_52_REG                (0x0000F2D0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_53_REG                (0x0000F2D4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_54_REG                (0x0000F2D8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_55_REG                (0x0000F2DC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_56_REG                (0x0000F2E0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_57_REG                (0x0000F2E4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_58_REG                (0x0000F2E8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_STORAGE_VALUES_59_REG                (0x0000F2EC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG                 (0x0000F3E0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_TIMING_STORAGE_1_REG                 (0x0000F3E4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG

#define MC0_BC_CR_MNTS_CBIT_REG                                        (0x0000F3EC)
//Duplicate of MC0_CH0_CR_MNTS_CBIT_REG

#define MC0_BC_CR_RH_TRR_LFSR_REG                                      (0x0000F3F0)
//Duplicate of MC0_CH0_CR_RH_TRR_LFSR_REG

#define MC0_BC_CR_WDB_CAPTURE_CTL_REG                                  (0x0000F3F8)
//Duplicate of MC0_CH0_CR_WDB_CAPTURE_CTL_REG

#define MC0_BC_CR_WDB_CAPTURE_STATUS_REG                               (0x0000F3FC)
//Duplicate of MC0_CH0_CR_WDB_CAPTURE_STATUS_REG

#define MC0_BC_CR_RH_TRR_CONTROL_REG                                   (0x0000F400)
//Duplicate of MC0_CH0_CR_RH_TRR_CONTROL_REG

#define MC0_BC_CR_REUT_CH_MISC_REFRESH_CTRL_REG                        (0x0000F404)
//Duplicate of MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_REG

#define MC0_BC_CR_REUT_CH_MISC_ZQ_CTRL_REG                             (0x0000F408)
//Duplicate of MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_REG

#define MC0_BC_CR_TC_REFM_REG                                          (0x0000F40C)
//Duplicate of MC0_CH0_CR_TC_REFM_REG

#define MC0_BC_CR_DDR_MR_PARAMS_REG                                    (0x0000F410)
//Duplicate of MC0_CH0_CR_DDR_MR_PARAMS_REG

#define MC0_BC_CR_DDR_MR_COMMAND_REG                                   (0x0000F414)
//Duplicate of MC0_CH0_CR_DDR_MR_COMMAND_REG

#define MC0_BC_CR_DDR_MR_RESULT_0_REG                                  (0x0000F418)
//Duplicate of MC0_CH0_CR_DDR_MR_RESULT_0_REG

#define MC0_BC_CR_DDR_MR_RESULT_1_REG                                  (0x0000F41C)
//Duplicate of MC0_CH0_CR_DDR_MR_RESULT_1_REG

#define MC0_BC_CR_DDR_MR_RESULT_2_REG                                  (0x0000F420)
//Duplicate of MC0_CH0_CR_DDR_MR_RESULT_2_REG

#define MC0_BC_CR_MR4_RANK_TEMPERATURE_REG                             (0x0000F424)
//Duplicate of MC0_CH0_CR_MR4_RANK_TEMPERATURE_REG

#define MC0_BC_CR_DDR4_MPR_RANK_TEMPERATURE_REG                        (0x0000F428)
//Duplicate of MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_REG

#define MC0_BC_CR_MC_RDB_CREDITS_REG                                   (0x0000F42C)
//Duplicate of MC0_CH0_CR_MC_RDB_CREDITS_REG

#define MC0_BC_CR_DESWIZZLE_LOW_ERM_REG                                (0x0000F430)
//Duplicate of MC0_CH0_CR_DESWIZZLE_LOW_ERM_REG

#define MC0_BC_CR_TC_RFP_REG                                           (0x0000F438)
//Duplicate of MC0_CH0_CR_TC_RFP_REG

#define MC0_BC_CR_TC_RFTP_REG                                          (0x0000F43C)
//Duplicate of MC0_CH0_CR_TC_RFTP_REG

#define MC0_BC_CR_TC_SRFTP_REG                                         (0x0000F440)
//Duplicate of MC0_CH0_CR_TC_SRFTP_REG

#define MC0_BC_CR_MC_REFRESH_STAGGER_REG                               (0x0000F444)
//Duplicate of MC0_CH0_CR_MC_REFRESH_STAGGER_REG

#define MC0_BC_CR_TC_ZQCAL_REG                                         (0x0000F448)
//Duplicate of MC0_CH0_CR_TC_ZQCAL_REG

#define MC0_BC_CR_TC_MR4_SHADDOW_REG                                   (0x0000F450)
//Duplicate of MC0_CH0_CR_TC_MR4_SHADDOW_REG

#define MC0_BC_CR_MC_INIT_STATE_REG                                    (0x0000F454)
//Duplicate of MC0_CH0_CR_MC_INIT_STATE_REG

#define MC0_BC_CR_WDB_VISA_SEL_REG                                     (0x0000F458)
//Duplicate of MC0_CH0_CR_WDB_VISA_SEL_REG

#define MC0_BC_CR_PASR_CTL_REG                                         (0x0000F45C)
//Duplicate of MC0_CH0_CR_PASR_CTL_REG

#define MC0_BC_CR_PM_DIMM_IDLE_ENERGY_REG                              (0x0000F460)
//Duplicate of MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_REG

#define MC0_BC_CR_PM_DIMM_PD_ENERGY_REG                                (0x0000F464)
//Duplicate of MC0_CH0_CR_PM_DIMM_PD_ENERGY_REG

#define MC0_BC_CR_PM_DIMM_ACT_ENERGY_REG                               (0x0000F468)
//Duplicate of MC0_CH0_CR_PM_DIMM_ACT_ENERGY_REG

#define MC0_BC_CR_PM_DIMM_RD_ENERGY_REG                                (0x0000F46C)
//Duplicate of MC0_CH0_CR_PM_DIMM_RD_ENERGY_REG

#define MC0_BC_CR_PM_DIMM_WR_ENERGY_REG                                (0x0000F470)
//Duplicate of MC0_CH0_CR_PM_DIMM_WR_ENERGY_REG

#define MC0_BC_CR_RH_TRR_ADDRESS_REG                                   (0x0000F474)
//Duplicate of MC0_CH0_CR_RH_TRR_ADDRESS_REG

#define MC0_BC_CR_SC_WR_DELAY_REG                                      (0x0000F478)
//Duplicate of MC0_CH0_CR_SC_WR_DELAY_REG

#define MC0_BC_CR_READ_RETURN_DFT_REG                                  (0x0000F47C)
//Duplicate of MC0_CH0_CR_READ_RETURN_DFT_REG

#define MC0_BC_CR_DESWIZZLE_LOW_REG                                    (0x0000F480)
//Duplicate of MC0_CH0_CR_DESWIZZLE_LOW_ERM_REG

#define MC0_BC_CR_SC_PBR_REG                                           (0x0000F488)
//Duplicate of MC0_CH0_CR_SC_PBR_REG

#define MC0_BC_CR_TC_LPDDR4_MISC_REG                                   (0x0000F494)
//Duplicate of MC0_CH0_CR_TC_LPDDR4_MISC_REG

#define MC0_BC_CR_DESWIZZLE_HIGH_ERM_REG                               (0x0000F498)
//Duplicate of MC0_CH0_CR_DESWIZZLE_HIGH_ERM_REG

#define MC0_BC_CR_PM_ALL_RANKS_CKE_LOW_COUNT_REG                       (0x0000F4B0)
//Duplicate of MC0_PWM_COUNTERS_DURATION_REG

#define MC0_BC_CR_DESWIZZLE_HIGH_REG                                   (0x0000F4B8)
//Duplicate of MC0_CH0_CR_DESWIZZLE_HIGH_ERM_REG

#define MC0_BC_CR_TC_SREXITTP_REG                                      (0x0000F4C0)
//Duplicate of MC0_CH0_CR_TC_SREXITTP_REG

#define MC0_BC_CR_DQS_OSCILLATOR_PARAMS_REG                            (0x0000F4C8)
//Duplicate of MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_REG

#define MC0_BC_CR_MRH_GENERIC_COMMAND_REG                              (0x0000F4CC)
//Duplicate of MC0_CH0_CR_MRH_GENERIC_COMMAND_REG

#define MC0_BC_CR_WDB_RD_WR_DFX_DATA_REG                               (0x0000F4D8)
//Duplicate of MC0_CH0_CR_WDB_RD_WR_DFX_DATA_REG

#define MC0_BC_CR_WDB_RD_WR_DFX_CTL_REG                                (0x0000F4E0)
//Duplicate of MC0_CH0_CR_WDB_RD_WR_DFX_CTL_REG

#define MC0_BC_CR_REF_FSM_STATUS_REG                                   (0x0000F4E4)
//Duplicate of MC0_CH0_CR_REF_FSM_STATUS_REG

#define MC0_BC_CR_WDB_MBIST_0_REG                                      (0x0000F4E8)
//Duplicate of MC0_CH0_CR_WDB_MBIST_0_REG

#define MC0_BC_CR_WDB_MBIST_1_REG                                      (0x0000F4EC)
//Duplicate of MC0_CH0_CR_WDB_MBIST_0_REG

#define MC0_BC_CR_RDB_MBIST_REG                                        (0x0000F4F8)
//Duplicate of MC0_CH0_CR_WDB_MBIST_0_REG

#define MC0_BC_CR_ECC_INJECT_COUNT_REG                                 (0x0000F4FC)
//Duplicate of MC0_CH0_CR_ECC_INJECT_COUNT_REG

#define MC0_BC_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_REG                    (0x0000F500)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_REG

#define MC0_BC_CR_PWM_DDR_SUBCH1_RDDATA_COUNTER_REG                    (0x0000F508)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_REG

#define MC0_BC_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_REG                    (0x0000F510)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_REG

#define MC0_BC_CR_PWM_DDR_SUBCH1_WRDATA_COUNTER_REG                    (0x0000F518)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_REG

#define MC0_BC_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG                    (0x0000F520)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_BC_CR_DDR4_MR2_RTT_WR_DIMM1_VALUES_REG                     (0x0000F528)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_BC_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG                     (0x0000F530)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_BC_CR_DDR4_MR6_VREF_DIMM1_VALUES_1_REG                     (0x0000F538)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_BC_CR_DDR4_MR1_ODIC_DIMM1_VALUES_REG                       (0x0000F544)
//Duplicate of MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_REG

#define MC0_BC_CR_DDR4_MR5_RTT_PARK_VALUES_REG                         (0x0000F548)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_BC_CR_DDR4_MR5_RTT_PARK_DIMM1_VALUES_REG                   (0x0000F550)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_BC_CR_DDR4_MR1_RTT_NOM_VALUES_REG                          (0x0000F558)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_BC_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG                      (0x0000F560)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_BC_CR_LPDDR4_DISCRETE_MR_VALUES_1_REG                      (0x0000F568)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_BC_CR_LPDDR4_DISCRETE_MR_VALUES_2_REG                      (0x0000F570)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_BC_CR_LPDDR4_DISCRETE_MR_VALUES_3_REG                      (0x0000F578)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_BC_CR_LPDDR4_DISCRETE_MR_VALUES_4_REG                      (0x0000F580)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_BC_CR_LPDDR4_DISCRETE_MR_VALUES_5_REG                      (0x0000F588)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_BC_CR_LPDDR4_DISCRETE_MR_VALUES_6_REG                      (0x0000F590)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_BC_CR_LPDDR4_DISCRETE_MR_VALUES_7_REG                      (0x0000F598)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_BC_CR_DDR4_MR0_MR1_CONTENT_REG                             (0x0000F5A0)
//Duplicate of MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_REG

#define MC0_BC_CR_DDR4_MR2_MR3_CONTENT_REG                             (0x0000F5A4)
//Duplicate of MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_REG

#define MC0_BC_CR_DDR4_MR4_MR5_CONTENT_REG                             (0x0000F5A8)
//Duplicate of MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_REG

#define MC0_BC_CR_DDR4_MR6_MR7_CONTENT_REG                             (0x0000F5AC)
//Duplicate of MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_REG

#define MC0_BC_CR_DDR4_MR2_RTT_WR_VALUES_REG                           (0x0000F5B0)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_BC_CR_DDR4_MR6_VREF_VALUES_0_REG                           (0x0000F5B8)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_BC_CR_DDR4_MR6_VREF_VALUES_1_REG                           (0x0000F5C0)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_BC_CR_LPDDR_MR_CONTENT_REG                                 (0x0000F5C8)
//Duplicate of MC0_CH0_CR_LPDDR_MR_CONTENT_REG

#define MC0_BC_CR_MRS_FSM_CONTROL_REG                                  (0x0000F5D0)
//Duplicate of MC0_CH0_CR_MRS_FSM_CONTROL_REG

#define MC0_BC_CR_MRS_FSM_RUN_REG                                      (0x0000F5D8)
//Duplicate of MC0_CH0_CR_MRS_FSM_RUN_REG

#define MC0_BC_CR_DDR4_MR1_ODIC_VALUES_REG                             (0x0000F5DC)
//Duplicate of MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_REG

#define MC0_BC_CR_PL_AGENT_CFG_DTF_REG                                 (0x0000F5E0)
//Duplicate of MC0_CH0_CR_PL_AGENT_CFG_DTF_REG

#define MC0_BC_CR_MCMNTS_GLOBAL_DRIVER_GATE_CFG_REG                    (0x0000F5E4)
//Duplicate of MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_REG

#define MC0_BC_CR_DDR4_ECC_DEVICE_VALUES_REG                           (0x0000F5E8)
//Duplicate of MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_REG

#define MC0_BC_CR_DDR4_ECC_DEVICE_DIMM1_VALUES_REG                     (0x0000F5F0)
//Duplicate of MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_REG

#define MC0_BC_CR_MCMNTS_SPARE2_REG                                    (0x0000F5F8)
//Duplicate of MC0_CH0_CR_MCMNTS_SPARE2_REG

#define MC0_BC_CR_MCMNTS_SPARE_REG                                     (0x0000F5FC)
//Duplicate of MC0_CH0_CR_MCMNTS_SPARE_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_0_REG                        (0x0000F600)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_1_REG                        (0x0000F604)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_2_REG                        (0x0000F608)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_3_REG                        (0x0000F60C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_4_REG                        (0x0000F610)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_5_REG                        (0x0000F614)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_6_REG                        (0x0000F618)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_7_REG                        (0x0000F61C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_8_REG                        (0x0000F620)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_9_REG                        (0x0000F624)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_10_REG                       (0x0000F628)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_11_REG                       (0x0000F62C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_12_REG                       (0x0000F630)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_13_REG                       (0x0000F634)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_14_REG                       (0x0000F638)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_15_REG                       (0x0000F63C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_16_REG                       (0x0000F640)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_17_REG                       (0x0000F644)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_18_REG                       (0x0000F648)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_19_REG                       (0x0000F64C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_20_REG                       (0x0000F650)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_21_REG                       (0x0000F654)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_22_REG                       (0x0000F658)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_23_REG                       (0x0000F65C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_24_REG                       (0x0000F660)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_25_REG                       (0x0000F664)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_26_REG                       (0x0000F668)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_27_REG                       (0x0000F66C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_28_REG                       (0x0000F670)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_29_REG                       (0x0000F674)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_30_REG                       (0x0000F678)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_31_REG                       (0x0000F67C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_32_REG                       (0x0000F680)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_33_REG                       (0x0000F684)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_34_REG                       (0x0000F688)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_35_REG                       (0x0000F68C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_36_REG                       (0x0000F690)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_37_REG                       (0x0000F694)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_38_REG                       (0x0000F698)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_39_REG                       (0x0000F69C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_40_REG                       (0x0000F6A0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_41_REG                       (0x0000F6A4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_42_REG                       (0x0000F6A8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_43_REG                       (0x0000F6AC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_44_REG                       (0x0000F6B0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_45_REG                       (0x0000F6B4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_46_REG                       (0x0000F6B8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_47_REG                       (0x0000F6BC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_48_REG                       (0x0000F6C0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_49_REG                       (0x0000F6C4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_50_REG                       (0x0000F6C8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_51_REG                       (0x0000F6CC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_52_REG                       (0x0000F6D0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_53_REG                       (0x0000F6D4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_54_REG                       (0x0000F6D8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_55_REG                       (0x0000F6DC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_56_REG                       (0x0000F6E0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_57_REG                       (0x0000F6E4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_58_REG                       (0x0000F6E8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_59_REG                       (0x0000F6EC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_60_REG                       (0x0000F6F0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_61_REG                       (0x0000F6F4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_62_REG                       (0x0000F6F8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_63_REG                       (0x0000F6FC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_64_REG                       (0x0000F700)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_65_REG                       (0x0000F704)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_66_REG                       (0x0000F708)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_67_REG                       (0x0000F70C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_68_REG                       (0x0000F710)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_69_REG                       (0x0000F714)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_70_REG                       (0x0000F718)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_71_REG                       (0x0000F71C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_72_REG                       (0x0000F720)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_73_REG                       (0x0000F724)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_74_REG                       (0x0000F728)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_75_REG                       (0x0000F72C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_76_REG                       (0x0000F730)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_77_REG                       (0x0000F734)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_78_REG                       (0x0000F738)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_79_REG                       (0x0000F73C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_80_REG                       (0x0000F740)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_81_REG                       (0x0000F744)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_82_REG                       (0x0000F748)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_83_REG                       (0x0000F74C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_84_REG                       (0x0000F750)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_85_REG                       (0x0000F754)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_86_REG                       (0x0000F758)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_87_REG                       (0x0000F75C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_88_REG                       (0x0000F760)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_89_REG                       (0x0000F764)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_90_REG                       (0x0000F768)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_91_REG                       (0x0000F76C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_92_REG                       (0x0000F770)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_93_REG                       (0x0000F774)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_94_REG                       (0x0000F778)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_95_REG                       (0x0000F77C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_96_REG                       (0x0000F780)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_97_REG                       (0x0000F784)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_98_REG                       (0x0000F788)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_99_REG                       (0x0000F78C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_100_REG                      (0x0000F790)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_101_REG                      (0x0000F794)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_102_REG                      (0x0000F798)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_103_REG                      (0x0000F79C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_104_REG                      (0x0000F7A0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_105_REG                      (0x0000F7A4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_106_REG                      (0x0000F7A8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_GENERIC_MRS_FSM_CONTROL_107_REG                      (0x0000F7AC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_BC_CR_MRH_CONFIG_REG                                       (0x0000F7FC)
//Duplicate of MC0_CH0_CR_MRH_CONFIG_REG
#pragma pack(pop)
#endif
