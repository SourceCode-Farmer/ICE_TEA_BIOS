/** @file
  Implementation of the memory controller hardware abstraction layer.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

// Include files
#include "McAddress.h"
#include "MrcInterface.h"
#include "MrcCommon.h"
#include "MrcReadReceiveEnable.h"
#include "MrcDdrIoOffsets.h"
#include "MrcMcOffsets.h"
#include "MrcGears.h"
///
/// Defines
///
#define MAX_CHUNK_SIZE (8)

// MrcCheckComplexOrSideEffect() defines
#define MRC_IS_COMPLEX (TRUE)
#define MRC_IS_SIDE_EFFECT (FALSE)

/// Hash arrays
#define ROUNDTRIP_DELAY_MAX (8)

/// Constants

/// Note that these are declared without "const", because GetSet argument is a pointer to non-const INT64.
/// Actually they are still constants, because they reside in data segment of a BIOS image, which is read-only.
/// And they should only be used for GetSet write operations, not read.
INT64 gGetSetEn   = 1;
INT64 gGetSetDis  = 0;

#ifdef MRC_DEBUG_PRINT
///
/// DDR IO Debug Strings
///
const CHAR8* GsmGtDebugStrings[GsmGtMax + 4] = {
  "RecEnDelay",
  "RxDqsDelay",
  "RxDqDelay",
  "RxDqsPDelay",
  "RxDqsNDelay",
  "RxVref",
  "RxEq",
  "RxDqBitDelay",
  "RxVoc",
  "RxOdt",
  "RxOdtUp",
  "RxOdtDn",
  "DramDrvStr",
  "McOdtDelay",
  "McOdtDuration",
  "SenseAmpDelay",
  "SenseAmpDuration",
  "RoundTripDelay",
  "RxDqsBitDelay",
  "RxDqDqsDelay",
  "WrLvlDelay",
  "TxDqsDelay",
  "TxDqsDelay90",
  "TxDqDelay",
  "TxDqDelay90",
  "TxVref",
  "TxEq",
  "TxDqBitDelay",
  "TxDqBitDelay90",
  "TxRon",
  "TxRonUp",
  "TxRonDn",
  "TxSlewRate",
  "TxImode",
  "WrOdt",
  "NomOdt",
  "ParkOdt",
  "TxTco",
  "TxXtalk",
  "RxCtleR",
  "RxCtleC",
  "RxDqsPBitDelay",
  "RxDqsNBitDelay",
  "CmdAll",
  "CmdGrp0",
  "CmdGrp1",
  "CmdGrp2",
  "CtlAll",
  "CtlGrp0",
  "CtlGrp1",
  "CtlGrp2",
  "CtlGrp3",
  "CtlGrp4",
  "CtlGrp5",
  "CmdCtlAll",
  "CkAll",
  "CmdVref",
  "AlertVref",
  "CmdRon",
  "CmdGrpPi",
  "CtlGrpPi",
  "ClkGrpPi",
  "ClkGrpG4Pi",
  "TxCycleDelay",
  "EridDelay",
  "EridVref",
  "ErrorVref",
  "ReqVref",
  "RecEnOffset",
  "RxDqsOffset",
  "RxVrefOffset",
  "TxDqsOffset",
  "TxDqOffset",
  "RxDqsBitOffset",
  "RxDqsPiUiOffset",
  "CkeGrpPi",
  "WckGrpPi",
  "WckGrpPi90",
  "DqsOdtDelay",
  "DqsOdtDuration",
  "CompRcompOdtUpPerStrobe",
  "CompRcompOdtDnPerStrobe",
  "RxDqsEq",
  "RxBiasCtl",
  "RxLpddrMode",
  "RxBiasVrefSel",
  "RxBiasTailCtl",
  "RxDqsAmpOffset",
  "RxDqsUnmatchedAmpOffset",
  "RxCben",
  "TxRankMuxDelay",
  "TxDqsRankMuxDelay",
  "RxRankMuxDelay",
  "RxFlybyDelay",
  "RxIoTclDelay",
  "RoundTripIoComp",
  "RxFifoRdEnFlybyDelay",
  "RxFifoRdEnTclDelay",
  "RxDqDataValidDclkDelay",
  "RxDqDataValidQclkDelay",
  "RxRptChDqClkOn",
  "TxRptChDqClkOn",
  "TxDqFifoWrEnTcwlDelay",
  "TxDqFifoRdEnTcwlDelay",
  "TxDqFifoRdEnFlybyDelay",
  "TxDqsTcoP",
  "TxDqsTcoN",
  "DefDrvEnLow",
  "CmdTxEq",
  "CtlTxEq",
  "TxEqCoeff0",
  "TxEqCoeff1",
  "TxEqCoeff2",
  "RxVrefVttDecap",
  "RxVrefVddqDecap",
  "ForceDfeDisable",
  "PanicVttDnLp",
  "VttGenStatusSelCount",
  "VttGenStatusCount",
  "RxR",
  "RxC",
  "RxTap1",
  "RxTap2",
  "RxTap3",
  "RxTap4",
  "RxTap1En",
  "RxTap2En",
  "RxTap3En",
  "RloadDqsDn",
  "DataRxD0PiCb",
  "DataSDllPiCb",
  "VccDllRxD0PiCb",
  "VccDllSDllPiCb",
  "DqsOdtCompOffset",
  "DqOdtCompOffset",
  "DqSCompOffset",
  "DqDrvUpCompOffset",
  "DqDrvDnCompOffset",
  "RloadCompOffset",
  "CmdRCompDrvDownOffset",
  "CmdRCompDrvUpOffset",
  "CmdSCompOffset",
  "ClkRCompDrvDownOffset",
  "ClkRCompDrvUpOffset",
  "ClkSCompOffset",
  "CtlRCompDrvDownOffset",
  "CtlRCompDrvUpOffset",
  "CtlSCompOffset",
  "CkeRCompDrvDownOffset",
  "CkeRCompDrvUpOffset",
  "CkeSCompOffset",
  "CompOffsetVssHiFF",
  "CompOffsetAll",
  "VsxHiClkFFOffset",
  "VsxHiCaFFOffset",
  "VsxHiCtlFFOffset",
  "CmdSlewRate",
  "ClkSlewRate",
  "ClkRon",
  "CtlSlewRate",
  "CtlRon",
  "DqScompPC",
  "CmdScompPC",
  "CtlScompPC",
  "ClkScompPC",
  "SCompCodeDq",
  "SCompCodeCmd",
  "SCompCodeCtl",
  "SCompCodeClk",
  "SCompBypassDq",
  "SCompBypassCmd",
  "SCompBypassCtl",
  "SCompBypassClk",
  "WrDSCodeUpCmd",
  "WrDSCodeUpCtl",
  "WrDSCodeUpClk",
  "WrDSCodeDnCmd",
  "WrDSCodeDnCtl",
  "WrDSCodeDnClk",
  "TcoCompCodeCmd",
  "TcoCompCodeCtl",
  "TcoCompCodeClk",
  "DqOdtVrefUp",
  "DqOdtVrefDn",
  "DqDrvVrefUp",
  "DqDrvVrefDn",
  "CmdDrvVrefUp",
  "CmdDrvVrefDn",
  "CtlDrvVrefUp",
  "CtlDrvVrefDn",
  "ClkDrvVrefUp",
  "ClkDrvVrefDn",
  "CompRcompOdtUp",
  "CompRcompOdtDn",
  "TxDqsBitDelay",
  "RxVocUnmatched",
  "RxPerBitDeskewCal",
  "TxPerBitDeskewCal",
  "CccPerBitDeskewCal",
  "TxDqDccOffset",
  "TxDqsDccOffset",
  "RxUnmatchedOffNcal",
  "RxMatchedUnmatchedOffPcal",
  "EndOfPhyMarker",
  "GsmIocIoReset",
  "GsmIocWlWakeCyc",
  "GsmIocWlSleepCyclesAct",
  "GsmIocWlSleepCyclesLp",
  "GsmIocForceCmpUpdt",
  "GsmIocNoDqInterleave",
  "GsmIocScramLpMode",
  "GsmIocScramDdr4Mode",
  "GsmIocScramDdr5Mode",
  "GsmIocScramGear1",
  "GsmIocScramGear4",
  "GsmIocVccDllGear1",
  "GsmIocVccDllControlBypass_V",
  "GsmIocVccDllControlSelCode_V",
  "GsmIocVccDllControlTarget_V",
  "GsmIocVccDllControlOpenLoop_V",
  "GsmIocVsxHiControlSelCode_V",
  "GsmIocVsxHiControlOpenLoop_V",
  "GsmIocCccPiEn",
  "GsmIocCccPiEnOverride",
  "GsmIocDisClkGate",
  "GsmIocDisDataIdlClkGate",
  "GsmIocScramLp4Mode",
  "GsmIocScramLp5Mode",
  "GsmIocScramOvrdPeriodicToDvfsComp",
  "GsmIocLp5Wck2CkRatio",
  "GsmIocChNotPop",
  "GsmIocDisIosfSbClkGate",
  "GsmIocEccEn",
  "GsmIocWrite0En",
  "GsmIocScramWrite0En",
  "GsmIocDataOdtStaticEn",
  "GsmIocCompOdtStaticDis",
  "GsmIocStrobeOdtStaticEn",
  "GsmIocDllMask",
  "GsmIocDataOdtMode",
  "GsmIocDataDqOdtParkMode",
  "GsmIocCompVttOdtEn",
  "GsmIocCmdDrvVref200ohm",
  "GsmIocVttPanicCompUpMult",
  "GsmIocVttPanicCompDnMult",
  "GsmIocRxVrefMFC",
  "GsmIocVccDllRxDeskewCal",
  "GsmIocVccDllTxDeskewCal",
  "GsmIocVccDllCccDeskewCal",
  "GsmIocForceRxAmpOn",
  "GsmIocTxOn",
  "GsmIocRxDisable",
  "GsmIocSenseAmpMode",
  "GsmIocDqsRFMode",
  "GsmIocClkPFallNRiseMode",
  "GsmIocClkPRiseNFallMode",
  "GsmIocClkPFallNRiseFeedback",
  "GsmIocClkPRiseNFallFeedback",
  "GsmIocClkPFallNRiseCcc56",
  "GsmIocClkPRiseNFallCcc56",
  "GsmIocClkPFallNRiseCcc78",
  "GsmIocClkPRiseNFallCcc78",
  "GsmIocDdrDqRxSdlBypassEn",
  "GsmIocCaTrainingMode",
  "GsmIocCaParityTrain",
  "GsmIocReadLevelMode",
  "GsmIocWriteLevelMode",
  "GsmIocReadDqDqsMode",
  "GsmIocRxClkStg",
  "GsmIocDataRxBurstLen",
  "GsmIocEnDqsNRcvEn",
  "GsmIocEnDqsNRcvEnGate",
  "GsmIocRxMatchedPathEn",
  "GsmIocForceOdtOn",
  "GsmIocForceOdtOnWithoutDrvEn",
  "GsmIocRxPiPwrDnDis",
  "GsmIocTxPiPwrDnDis",
  "GsmIocTxDisable",
  "GsmIocCmdVrefConverge",
  "GsmIocCompClkOn",
  "GsmIocConstZTxEqEn",
  "GsmIocDisableQuickComp",
  "GsmIocSinStep",
  "GsmIocSinStepAdv",
  "GsmIocSdllSegmentDisable",
  "GsmIocRXDeskewForceOn",
  "GsmIocDllWeakLock",
  "GsmIocDllWeakLock1",
  "GsmIocTxEqEn",
  "GsmIocTxEqTapSelect",
  "GsmIocDqSlewDlyByPass",
  "GsmIocWlLongDelEn",
  "GsmIocCompVddqOdtEn",
  "GsmIocRptChRepClkOn",
  "GsmIocCmdAnlgEnGraceCnt",
  "GsmIocTxAnlgEnGraceCnt",
  "GsmIocTxDqFifoRdEnPerRankDelDis",
  "GsmIocInternalClocksOn",
  "GsmIocDqsMaskPulseCnt",
  "GsmIocDqsMaskValue",
  "GsmIocDqsPulseCnt",
  "GsmIocDqOverrideData",
  "GsmIocDqOverrideEn",
  "GsmIocRankOverrideEn",
  "GsmIocRankOverrideVal",
  "GsmIocDataCtlGear1",
  "GsmIocDataCtlGear4",
  "GsmIocDataWrPreamble",
  "GsmIocDccTrainingMode",
  "GsmIocDccTrainingDone",
  "GsmIocDccDrain",
  "GsmIocDccActiveClks",
  "GsmIocDccActiveBytes",
  "GsmIocDccDcoCompEn",
  "GsmIocDccClkTrainVal",
  "GsmIocDccDataTrainDqsVal",
  "GsmIocLdoFFCodeLockData",
  "GsmIocLdoFFCodeLockCcc",
  "GsmIocDccDcdSampleSelCcc",
  "GsmIocStep1PatternCcc",
  "GsmIocDccCodePh0IndexCcc",
  "GsmIocDccCodePh90IndexCcc",
  "GsmIocTxpbddOffsetCcc",
  "GsmIocTxpbdPh90incrCcc",
  "GsmIocMdllCmnVcdlDccCodeCcc",
  "GsmIocDccDcdSampleSelData",
  "GsmIocStep1PatternData",
  "GsmIocDccCodePh0IndexData",
  "GsmIocDccCodePh90IndexData",
  "GsmIocTxpbddOffsetData",
  "GsmIocTxpbdPh90incrData",
  "GsmIocMdllCmnVcdlDccCodeData",
  "GsmIocDccFsmResetData",
  "GsmIocDccFsmResetCcc",
  "GsmIocDcdRoCalEnData",
  "GsmIocDcdRoCalEnCcc",
  "GsmIocDcdRoCalStartData",
  "GsmIocDcdRoCalStartCcc",
  "GsmIocDcdRoCalSampleCountRstData",
  "GsmIocDcdRoCalSampleCountRstCcc",
  "GsmIocDccStartData",
  "GsmIocDccStartCcc",
  "GsmIocSrzDcdEnData",
  "GsmIocSrzDcdEnCcc",
  "GsmIocMdllDcdEnData",
  "GsmIocMdllDcdEnCcc",
  "GsmIocDcdSampleCountRstData",
  "GsmIocDcdSampleCountStartData",
  "GsmIocDcdRoLfsrData",
  "GsmIocRxSALTailCtrl",
  "GsmIocRxVocMode",
  "GsmIocDataTrainFeedback",
  "GsmIocDdrDqDrvEnOvrdData",
  "GsmIocDdrDqDrvEnOvrdModeEn",
  "GsmIocDataDqsOdtParkMode",
  "GsmIocDataDqsNParkLow",
  "GsmIocRxAmpOffsetEn",
  "GsmIocBiasPMCtrl",
  "GsmIocLocalGateD0tx",
  "GsmIocFFCodePiOffset",
  "GsmIocFFCodeIdleOffset",
  "GsmIocFFCodeWeakOffset",
  "GsmIocFFCodeWriteOffset",
  "GsmIocFFCodeReadOffset",
  "GsmIocFFCodePBDOffset",
  "GsmIocFFCodeCCCDistOffset",
  "GsmIocDataInvertNibble",
  "GsmIocCapCancelCodeIdle",
  "GsmIocCapCancelCodePBD",
  "GsmIocCapCancelCodeWrite",
  "GsmIocCapCancelCodeRead",
  "GsmIocCapCancelCodePi",
  "GsmIocVssHiFFCodeIdle",
  "GsmIocVssHiFFCodeWrite",
  "GsmIocVssHiFFCodeRead",
  "GsmIocVssHiFFCodePBD",
  "GsmIocVssHiFFCodePi",
  "GsmIocEnableSpineGate",
  "GsmIocDataDccLaneStatusResult",
  "GsmIocDataDccSaveFullDcc",
  "GsmIocDataDccSkipCRWrite",
  "GsmIocDataDccMeasPoint",
  "GsmIocDataDccRankEn",
  "GsmIocDataDccLaneEn",
  "GsmIocDataDccStepSize",
  "GsmIocDataDcc2xStep",
  "GsmIocCccDccCcc",
  "GsmIocCccDccLaneStatusResult",
  "GsmIocCccDccStepSize",
  "GsmIocCccDcc2xStep",
  "GsmIocForceOnRcvEn",
  "GsmIocRetrainSwizzleCtlByteSel",
  "GsmIocRetrainSwizzleCtlBit",
  "GsmIocRetrainSwizzleCtlRetrainEn",
  "GsmIocRetrainSwizzleCtlSerialMrr",
  "GsmIocRetrainInitPiCode",
  "GsmIocRetrainDeltaPiCode",
  "GsmIocRetrainRoCount",
  "GsmIocRetrainCtlInitTrain",
  "GsmIocRetrainCtlDuration",
  "GsmIocRetrainCtlResetStatus",
  "EndOfIocMarker",
  "GsmMctRCD",
  "GsmMctRP",
  "GsmMctRPabExt",
  "GsmMctRAS",
  "GsmMctRDPRE",
  "GsmMctPPD",
  "GsmMctWRPRE",
  "GsmMctFAW",
  "GsmMctRRDsg",
  "GsmMctRRDdg",
  "GsmMctREFSbRd",
  "GsmMctLpDeratingExt",
  "GsmMctRDRDsg",
  "GsmMctRDRDdg",
  "GsmMctRDRDdr",
  "GsmMctRDRDdd",
  "GsmMctRDWRsg",
  "GsmMctRDWRdg",
  "GsmMctRDWRdr",
  "GsmMctRDWRdd",
  "GsmMctWRRDsg",
  "GsmMctWRRDdg",
  "GsmMctWRRDdr",
  "GsmMctWRRDdd",
  "GsmMctWRWRsg",
  "GsmMctWRWRdg",
  "GsmMctWRWRdr",
  "GsmMctWRWRdd",
  "GsmMctCKE",
  "GsmMctXP",
  "GsmMctXPDLL",
  "GsmMctPRPDEN",
  "GsmMctRDPDEN",
  "GsmMctWRPDEN",
  "GsmMctCA2CS",
  "GsmMctCSL",
  "GsmMctCSH",
  "GsmMctOdtRdDuration",
  "GsmMctOdtRdDelay",
  "GsmMctOdtWrDuration",
  "GsmMctOdtWrDelay",
  "GsmMctWrEarlyOdt",
  "GsmMctCL",
  "GsmMctCWL",
  "GsmMctAONPD",
  "GsmMctCWLAdd",
  "GsmMctCWLDec",
  "GsmMctXSDLL",
  "GsmMctXSR",
  "GsmMctSR",
  "GsmMctZQOPER",
  "GsmMctMOD",
  "GsmMctSrIdle",
  "GsmMctREFI",
  "GsmMctRFC",
  "GsmMctOrefRi",
  "GsmMctRefreshHpWm",
  "GsmMctRefreshPanicWm",
  "GsmMctHpRefOnMrs",
  "GsmMctREFIx9",
  "GsmMctSrxRefDebits",
  "GsmMctZQCSPeriod",
  "GsmMctZQCS",
  "GsmMctZQCAL",
  "GsmMctCPDED",
  "GsmMctCAL",
  "GsmMctCKCKEH",
  "GsmMctCSCKEH",
  "GsmMctRFCpb",
  "GsmMctRefm",
  "EndOfMctMarker",
  "GsmMccDramType",
  "GsmMccCmdStretch",
  "GsmMccCmdGapRatio",
  "GsmMccAddrMirror",
  "GsmMccCmdTriStateDis",
  "GsmMccCmdTriStateDisTrain",
  "GsmMccFreqPoint",
  "GsmMccEnableOdtMatrix",
  "GsmMccX8Device",
  "GsmMccGear2",
  "GsmMccGear4",
  "GsmMccDdr4OneDpc",
  "GsmMccMultiCycCmd",
  "GsmMccWrite0En",
  "GsmMccWCKDiffLowInIdle",
  "GsmMccLDimmMap",
  "GsmMccEnhancedInterleave",
  "GsmMccEccMode",
  "GsmMccAddrDecodeDdrType",
  "GsmMccLChannelMap",
  "GsmMccSChannelSize",
  "GsmMccChWidth",
  "GsmMccHalfCachelineMode",
  "GsmMccLDimmSize",
  "GsmMccLDimmDramWidth",
  "GsmMccLDimmRankCnt",
  "GsmMccSDimmSize",
  "GsmMccSDimmDramWidth",
  "GsmMccSDimmRankCnt",
  "GsmMccExtendedBankHashing",
  "GsmMccDdr5Device8GbDimmL",
  "GsmMccDdr5Device8GbDimmS",
  "GsmMccDdrReset",
  "GsmMccEnableRefresh",
  "GsmMccEnableSr",
  "GsmMccMcInitDoneAck",
  "GsmMccMrcDone",
  "GsmMccSafeSr",
  "GsmMccSaveFreqPoint",
  "GsmMccEnableDclk",
  "GsmMccPureSrx",
  "GsmMccLp4FifoRdWr",
  "GsmMccIgnoreCke",
  "GsmMccMaskCs",
  "GsmMccCpgcInOrder",
  "GsmMccInOrderIngress",
  "GsmMccCadbEnable",
  "GsmMccDeselectEnable",
  "GsmMccBusRetainOnBubble",
  "GsmMccBlockXarb",
  "GsmMccBlockCke",
  "GsmMccResetOnCmd",
  "GsmMccResetDelay",
  "GsmMccRankOccupancy",
  "GsmMccMcSrx",
  "GsmMccRefInterval",
  "GsmMccRefStaggerEn",
  "GsmMccRefStaggerMode",
  "GsmMccDisableStolenRefresh",
  "GsmMccEnRefTypeDisplay",
  "GsmMccHashMask",
  "GsmMccLsbMaskBit",
  "GsmMccHashMode",
  "GsmMccDisableCkTristate",
  "GsmMccPbrDis",
  "GsmMccPbrIssueNop",
  "GsmMccRefreshAbrRelease",
  "GsmMccPbrDisOnHot",
  "GsmMccOdtOverride",
  "GsmMccOdtOn",
  "GsmMccMprTrainDdrOn",
  "GsmMccCkeOverride",
  "GsmMccCkeOn",
  "GsmMccCsOverride",
  "GsmMccCsOverrideVal",
  "GsmMccRrdValidTrigger",
  "GsmMccRrdValidOverflow",
  "GsmMccRrdValidValue",
  "GsmMccRrdValidSign",
  "GsmMccDeswizzleByte",
  "GsmMccDeswizzleBit",
  "GsmMccLp5BankMode",
  "GsmMccMrrBlnMax",
  "EndOfMccMarker",
  "GsmCmiMcOrg",
  "GsmCmiHashMask",
  "GsmCmiLsbMaskBit",
  "GsmCmiStackedMode",
  "GsmCmiStackMsMap",
  "GsmCmiLMadSliceMap",
  "GsmCmiSMadSliceSize",
  "GsmCmiStackedMsHash",
  "GsmCmiSliceDisable",
  "EndOfCmiMarker",
  "GsmComplexRcvEn",
  "GsmComplexRxBias",
  "GsmGtMax",
  "x",        // Add 3 dummy strings after GsmGtMax so the protection code doesn't compare against NULL/garbage pointer when a comma is missing above
  "y",        // This supports up to 3 missing commas
  "z"
};
#endif // MCR_DEBUG_PRINT

/**
  This function sets the specified register bit field.

  @param[in] HashIn        - The bit field hash value.
  @param[in] BitFieldValue - The bit field value.
  @param[in] RegisterValue - The register value to write.

  @retval The value written to the register.
**/
UINT32
MrcHalSetBitField32 (
  IN const UINT32 HashIn,
  IN const UINT32 BitfieldValue,
  IN const UINT32 RegisterValue
  )
{
  UINT32 Mask;
  UINT32 BfOffset;
  UINT32 BfWidth;
  MRC_REGISTER_HASH_STRUCT Hash;

  Hash.Data = HashIn;
  BfOffset  = Hash.Bits.BfOffset;
  BfWidth   = Hash.Bits.BfWidth;
  Mask      = ((BfWidth >= 32) ? (0xFFFFFFFFUL) : ((1UL << BfWidth) - 1)) << BfOffset;
  return ((RegisterValue & ~Mask) | ((BitfieldValue << BfOffset) & Mask));
}

/**
  This function sets the specified register bit field.

  @param[in] HashIn        - The bit field hash value.
  @param[in] BitFieldValue - The bit field value.
  @param[in] RegisterValue - The register value to write.

  @retval The value written to the register.
**/
UINT32
MrcHalSetBitFieldS32 (
  IN const UINT32 HashIn,
  IN const INT32  BitfieldValue,
  IN const UINT32 RegisterValue
  )
{
  return (MrcHalSetBitField32 (HashIn, BitfieldValue, RegisterValue));
}

/**
  This function will determine if the group access is signed or unsigned and take care of sign extension.

  @param[in] MrcData       - Include all MRC global data.
  @param[in] HashIn        - The bit field hash value.
  @param[in] RegisterValue - The register value to modify.

  @retval The register bit field value.
**/
INT64
MrcHalGsmGetBitField (
  IN MrcParameters *const MrcData,
  IN const MRC_REGISTER_HASH_STRUCT HashIn,
  IN const UINT64 RegisterValue
  )
{
  UINT64_STRUCT Value;

  Value.Data = RegisterValue;
  if (HashIn.Bits.RegSize == 0) {
    if (HashIn.Bits.BfSign == 0) {
      return (MrcHalGetBitField32 (HashIn.Data, Value.Data32.Low));
    } else {
      return (MrcHalGetBitFieldS32 (HashIn.Data, Value.Data32.Low));
    }
  } else {
    if (HashIn.Bits.BfSign == 0) {
      return (MrcHalGetBitField64 (MrcData, HashIn.Data, Value.Data));
    } else {
      return (MrcHalGetBitFieldS64 (MrcData, HashIn.Data, Value.Data));
    }
  }
}

/**
  This function will determine if the group access is signed or unsigned and take care of sign extension.

  @param[in] MrcData       - Include all MRC global data.
  @param[in] HashIn        - The bit field hash value.
  @param[in] BitFieldValue - The bit field value to modify.
  @param[in] RegisterValue - The register value to modify.

  @retval The updated register.
**/
UINT64
MrcHalGsmSetBitField (
  IN MrcParameters *const MrcData,
  IN const MRC_REGISTER_HASH_STRUCT HashIn,
  IN const INT64  BitFieldValue,
  IN const UINT64 RegisterValue
  )
{
  IN INT64_STRUCT BfValue;
  IN UINT64_STRUCT Value;

  BfValue.Data = BitFieldValue;
  Value.Data = RegisterValue;
  if (HashIn.Bits.RegSize == 0) {
    if (HashIn.Bits.BfSign == 0) {
      return (MrcHalSetBitField32 (HashIn.Data, BfValue.Data32.Low, Value.Data32.Low));
    } else {
      return (MrcHalSetBitFieldS32 (HashIn.Data, BfValue.Data32.Low, Value.Data32.Low));
    }
  } else {
    if (HashIn.Bits.BfSign == 0) {
      return (MrcHalSetBitField64 (MrcData, HashIn.Data, BfValue.Data, Value.Data));
    } else {
      return (MrcHalSetBitFieldS64 (MrcData, HashIn.Data, BfValue.Data, Value.Data));
    }
  }
}

/**
  This function gets the specified register bit field.

  @param[in] HashIn        - The bit field hash value.
  @param[in] RegisterValue - The register value to write.

  @retval The value written to the register.
**/
UINT32
MrcHalGetBitField32 (
  IN const UINT32 HashIn,
  IN const UINT32 RegisterValue
  )
{
  UINT32 BfWidth;
  MRC_REGISTER_HASH_STRUCT Hash;

  Hash.Data = HashIn;
  BfWidth   = Hash.Bits.BfWidth;
  return ((RegisterValue >> Hash.Bits.BfOffset) & ((BfWidth >= 32) ? (0xFFFFFFFFUL) : ((1UL << BfWidth) - 1)));
}

/**
  This function gets the specified register bit field.

  @param[in] HashIn        - The bit field hash value.
  @param[in] RegisterValue - The register value to write.

  @retval The value written to the register.
**/
INT32
MrcHalGetBitFieldS32 (
  IN const UINT32 HashIn,
  IN const UINT32 RegisterValue
  )
{
  UINT32 Value;
  UINT32 BfWidth;
  MRC_REGISTER_HASH_STRUCT Hash;

  Hash.Data = HashIn;
  Value     = MrcHalGetBitField32 (HashIn, RegisterValue);
  BfWidth   = Hash.Bits.BfWidth;
  if (BfWidth < 32) {
    if (Value & (1 << (BfWidth - 1))) {
      Value |= ~((1 << BfWidth) - 1);
    }
  }
  return (Value);
}

/**
  This function sets the specified register bit field.

  @param[in] MrcData       - Include all MRC global data.
  @param[in] HashIn        - The bit field hash value.
  @param[in] BitFieldValue - The bit field value.
  @param[in] RegisterValue - The register value to write.

  @retval The value written to the register.
**/
UINT64
MrcHalSetBitField64 (
  IN MrcParameters *const MrcData,
  IN const UINT32  HashIn,
  IN const UINT64  BitfieldValue,
  IN const UINT64  RegisterValue
  )
{
  MRC_FUNCTION *Func;
  UINT64 Mask;
  UINT32 BfOffset;
  UINT32 BfWidth;
  MRC_REGISTER_HASH_STRUCT Hash;

  Hash.Data = HashIn;
  BfWidth   = Hash.Bits.BfWidth;
  if (BfWidth >= 64) {
    return (BitfieldValue);
  }
  BfOffset = Hash.Bits.BfOffset;
  Func     = MrcData->Inputs.Call.Func;
  Mask     = Func->MrcLeftShift64 ((Func->MrcLeftShift64 (1ULL, BfWidth) - 1), BfOffset);
  return ((RegisterValue & ~Mask) | (Func->MrcLeftShift64 (BitfieldValue, BfOffset) & Mask));
}

/**
  This function sets the specified register bit field.

  @param[in] MrcData       - Include all MRC global data.
  @param[in] HashIn        - The bit field hash value.
  @param[in] BitFieldValue - The bit field value.
  @param[in] RegisterValue - The register value to write.

  @retval The value written to the register.
**/
UINT64
MrcHalSetBitFieldS64 (
  IN MrcParameters *const MrcData,
  IN const UINT32  HashIn,
  IN const INT64   BitfieldValue,
  IN const UINT64  RegisterValue
  )
{
  return (MrcHalSetBitField64 (MrcData, HashIn, BitfieldValue, RegisterValue));
}

/**
  This function gets the specified register bit field.

  @param[in] MrcData       - Include all MRC global data.
  @param[in] HashIn        - The bit field hash value.
  @param[in] RegisterValue - The register value to write.

  @retval The value written to the register.
**/
UINT64
MrcHalGetBitField64 (
  IN MrcParameters *const MrcData,
  IN const UINT32  HashIn,
  IN const UINT64  RegisterValue
  )
{
  MRC_FUNCTION *Func;
  UINT32 BfWidth;
  MRC_REGISTER_HASH_STRUCT Hash;

  Hash.Data = HashIn;
  BfWidth   = Hash.Bits.BfWidth;
  if (BfWidth >= 64) {
    return (RegisterValue);
  }
  Func = MrcData->Inputs.Call.Func;
  return ((Func->MrcRightShift64 (RegisterValue, Hash.Bits.BfOffset)) & (Func->MrcLeftShift64 (1ULL, BfWidth) - 1));
}

/**
  This function gets the specified register bit field.

  @param[in] MrcData       - Include all MRC global data.
  @param[in] HashIn        - The bit field hash value.
  @param[in] RegisterValue - The register value to write.

  @retval The value written to the register.
**/
INT64
MrcHalGetBitFieldS64 (
  IN MrcParameters *const MrcData,
  IN const UINT32  HashIn,
  IN const UINT64  RegisterValue
  )
{
  MRC_FUNCTION *Func;
  UINT64 Value;
  UINT64 Sign;
  UINT32 BfWidth;
  MRC_REGISTER_HASH_STRUCT Hash;

  Value     = MrcHalGetBitField64 (MrcData, HashIn, RegisterValue);
  Hash.Data = HashIn;
  BfWidth   = Hash.Bits.BfWidth;
  if (BfWidth < 64) {
    Func = MrcData->Inputs.Call.Func;
    Sign = Func->MrcLeftShift64 (1ULL, BfWidth - 1);
    if (Value & Sign) {
      Value |= ~(Func->MrcLeftShift64 (1ULL, BfWidth) - 1);
    }
  }
  return (Value);
}

/**
  CPU specific function when encodes Vref (mV) for the GSM_GT RxVref.

  @param[in]  MrcData       - Pointer to global data.
  @param[in]  Vref          - Vref value to encode, in mV.
  @param[out] VrefEncoding  - Register value of the Vref to program.
**/
VOID
MrcEncodeRxVref (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Vref,
  OUT INT64                 *VrefEncoded
  )
{
  MrcOutput *Outputs;
  UINT32  Encoding;
  UINT32  Vdd;

  Outputs = &MrcData->Outputs;

  Vdd = Outputs->Vdd2Mv;

  // Register moves in steps of VDD2/512.  The encoding would be:
  //   Step = Vdd / 512
  //   Encoding = Vref / Step
  //   Encoding = Vref / Vdd / 512
  // To avoid loss of precision, multiply Vref by the step size, then DIVIDEROUND with VDD
  Encoding = Vref * 512;
  Encoding = UDIVIDEROUND (Encoding, Vdd);

  MRC_DEBUG_ASSERT (VrefEncoded != 0, &Outputs->Debug, "%s", gNullPtrErrStr);
  *VrefEncoded = Encoding;
}

/**
  This function is used to translate between MRC Offsets to CR functionality.
  CR is encoded in Sign:Magnitude which creates a non-monotonic response in SenseAmp Vref levels
  when sweeping from 0 - 31.

  @param[in]  SotOffset - Current offset.
  @param[out] Value     - Pointer to return variable to store translated value.
  @param[in]  CrIdx     - TRUE means SotOffset is from the CR encoding.  FALSE means SotOffset is from the MRC encoding.
  @param[in]  Dqs       - Selector on the translation for either Dqs (TRUE) or DQ (FALSE).

  @retval - MrcStatus: mrcFail if inputs are invalid, otherwise mrcSuccess.
**/
MrcStatus
MrcTranslateSotOffset (
  IN  INT64   SotOffset,
  OUT INT64   *Value,
  IN  BOOLEAN CrIdx,
  IN  BOOLEAN Dqs
  )
{
  MrcStatus Status;
  INT64 Result;

  Status = mrcSuccess;
  Result = -1;

  if (CrIdx) {
    if ((SotOffset > 31) || (SotOffset < 0)) {
      Status = mrcFail;
    } else if (SotOffset > 15) {
      // CR Value | HW Value | MRC Idx | Group
      // 16 : 31  |  0 : -15 | 15 :  0 |  DQ
      // 17 : 31  | 15 :  1  | 30 : 16 |  DQS
      //    16    |    0     |    15   |  DQS
      if (Dqs) {
        if (SotOffset == 16) {
          Result = 15;
        } else {
          Result = 31 + 16 - SotOffset;
        }
      } else {
        Result = (31 - SotOffset);
      }
    } else {
      // CR Value | HW Value | MRC Idx | Group
      // 15 :  0  |  15 :  0 | 30 : 15 |  DQ
      // 15 :  0  | -15 :  0 |  0 : 15 |  DQS
      Result = 15;
      if (Dqs) {
        Result -= SotOffset;
      } else {
        Result += SotOffset;
      }
    }
  } else {
    if ((SotOffset < 0) || (SotOffset > 30)) {
      Status = mrcFail;
    } else if (SotOffset > 15) {
      // CR Value | HW Value | MRC Idx | Group
      //  1 : 15  |  1 : 15  | 16 : 30 |  DQ
      // 31 : 17  |  1 : 15  | 16 : 30 |  DQS
      if (Dqs) {
        Result = (31 + 16 - SotOffset);
      } else {
        Result = (SotOffset - 15);
      }
    } else {
      // CR Value | HW Value | MRC Idx | Group
      // 16 : 31  |  0 : -15 | 15 : 0  |  DQ
      //  0 : 15  |  0 : -15 | 15 : 0  |  DQS
      Result = (Dqs) ? 15 : 31;
      Result -= SotOffset;
    }
  }

  if (Value != NULL) {
    *Value = Result;
  } else {
    Status = mrcWrongInputParameter;
  }

  return Status;
}

/**
  This function translates the VOC code from physical to linear

  @param[in]     MrcData - Current offset.
  @param[in]     Group   - The group can be RxUnmatchedOffNcal or RxMatchedUnmatchedOffPcal.
  @param[in out] Value   - Value is input for translation and output for the translation result.

  @retval None.
**/
VOID
PhysicalToLinear(
  IN      MrcParameters *const  MrcData,
  IN      GSM_GT        const   Group,
  IN OUT  INT64         *const  Value
  )
{
  MrcOutput     *Outputs;

  INT64 Result = *Value;
  Outputs = &MrcData->Outputs;

  if ((Group == RxUnmatchedOffNcal) || (Group == RxMatchedUnmatchedOffPcal)) {
    //
    // Translate from physcial to linear
    //
    Result = (*Value > 31) ? 0 : (*Value < 0) ? 31 : (*Value > 15) ? 31 - (*Value) : 16 + (*Value);
  } else {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "Group %d is not supported in PhysicalToLinear\n", Group);
  }
  *Value = Result;
  return;
}

/**
  This function translates the VOC code from linear to physical

  @param[in]     MrcData - Current offset.
  @param[in]     Group   - The group can be RxUnmatchedOffNcal or RxMatchedUnmatchedOffPcal.
  @param[in out] Value   - Value is input for translation and output for the translation result.

  @retval None.
**/
VOID
LinearToPhysical(
  IN      MrcParameters *const  MrcData,
  IN      GSM_GT        const   Group,
  IN OUT  INT64         *const  Value
  )
{
  MrcOutput     *Outputs;

  INT64 Result = *Value;
  Outputs = &MrcData->Outputs;

  if ((Group == RxUnmatchedOffNcal) || (Group == RxMatchedUnmatchedOffPcal)) {
    //
    // Translate from linear to physical
    //
    Result = (*Value > 31) ? 31 : (*Value < 0) ? 0 : (*Value > 15) ? *Value - 16 : 31 - (*Value);
  } else {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "Group %d is not supported in LinearToPhysical\n", Group);
  }
  *Value = Result;
  return;
}

/**
  Variable Max Limits for parameters.

  @param[in]  MrcData  - Pointer to global data.
  @param[in]  Group    - The group to be checked.
  @param[out] LaneMax  - Maximum value allow for Lane parameter.
**/
static
VOID
MrcGetSetParamMaxAdjust (
  IN  MrcParameters *const  MrcData,
  IN  GSM_GT        const   Group,
  OUT UINT32                *LaneMax
  )
{

  MRC_DEBUG_ASSERT (LaneMax != 0, &MrcData->Outputs.Debug, "%s %s", gNullPtrErrStr, __FUNCTION__);
  switch (Group) {
    case GsmIocCccDccCcc:
      *LaneMax = MAX_CCC_PER_BIT;
      break;

    case GsmIocDccCodePh0IndexCcc:
    case GsmIocDccCodePh90IndexCcc:
    case GsmIocTxpbddOffsetCcc:
    case GsmIocTxpbdPh90incrCcc:
      *LaneMax = 16;
      break;

    case GsmIocDccCodePh0IndexData:
    case GsmIocDccCodePh90IndexData:
    case GsmIocTxpbddOffsetData:
    case GsmIocTxpbdPh90incrData:
      *LaneMax = 10;
      break;

    default:
      //Everything else is considered as part of Data set
      *LaneMax = MAX_BITS;
      break;
  }
}

#ifdef MRC_DEBUG_PRINT
/**
  This debug only function checks to ensure the group is supported by GetSet.

  @param[in]  MrcData - Global MRC Data.
  @param[in]  Group   - The group to be accessed.

  @retval mrcSuccess if supported, otherwise mrcWrongInputParameter
**/
static
MrcStatus
MrcCheckGroupSupported (
  IN  MrcParameters *const  MrcData,
  IN  GSM_GT        const   Group
  )
{
  BOOLEAN   IsSupported;

  IsSupported = TRUE;

  switch (Group) {
    case RecEnDelay:
    case RxDqsPDelay:
    case RxDqsNDelay:
    case CompRcompOdtUpPerStrobe:
    case CompRcompOdtDnPerStrobe:
    case RxVref:
    case RxVoc:
    case RxVocUnmatched:
    case RxEq:
    case RxDqsEq:
    case RxDqsBitDelay:
    case RxDqsBitOffset:
    case RxDqsNBitDelay:
    case RxDqsPBitDelay:
    case RxRankMuxDelay:
    case TxDqsDelay:
    case TxDqsDelay90:
    case TxDqDelay:
    case TxDqDelay90:
    case TxEq:
    case TxEqCoeff0:
    case TxEqCoeff1:
    case TxEqCoeff2:
    case DefDrvEnLow:
    case CmdTxEq:
    case CtlTxEq:
    case RxVrefVttDecap:
    case RxVrefVddqDecap:
    case ForceDfeDisable:
    case PanicVttDnLp:
    case VttGenStatusSelCount:
    case VttGenStatusCount:
    case DqsOdtCompOffset:
    case TxRankMuxDelay:
    case TxDqsRankMuxDelay:
    case TxDqBitDelay:
    case TxDqBitDelay90:
    case RxUnmatchedOffNcal:
    case RxMatchedUnmatchedOffPcal:
    case TxDqDccOffset:
    case TxDqsDccOffset:
    case TxDqsBitDelay:
    case RecEnOffset:
    case RxDqsOffset:
    case RxVrefOffset:
    case TxDqsOffset:
    case TxDqOffset:
    case RxDqsPiUiOffset:
    case RoundTripDelay:
    case RxFlybyDelay:
    case RxIoTclDelay:
    case RxFifoRdEnFlybyDelay:
    case RxFifoRdEnTclDelay:
    case RxDqDataValidDclkDelay:
    case RxDqDataValidQclkDelay:
    case RxRptChDqClkOn:
    case TxRptChDqClkOn:
    case TxDqFifoWrEnTcwlDelay:
    case TxDqFifoRdEnTcwlDelay:
    case TxDqFifoRdEnFlybyDelay:
    case SenseAmpDelay:
    case SenseAmpDuration:
    case McOdtDelay:
    case McOdtDuration:
    case DqsOdtDelay:
    case DqsOdtDuration:
    case RxDqsAmpOffset:
    case RxDqsUnmatchedAmpOffset:
    case CmdVref:
    case CmdGrpPi:
    case CtlGrpPi:
    case ClkGrpPi:
    case ClkGrpG4Pi:
    case CkeGrpPi:
    case WckGrpPi:
    case WckGrpPi90:
    case TxSlewRate:
    case DqScompPC:
    case CmdSlewRate:
    case CmdScompPC:
    case CtlSlewRate:
    case CtlScompPC:
    case ClkSlewRate:
    case ClkScompPC:
    case SCompCodeDq:
    case SCompCodeCmd:
    case SCompCodeCtl:
    case SCompCodeClk:
    case SCompBypassDq:
    case SCompBypassCmd:
    case SCompBypassCtl:
    case SCompBypassClk:
    case TxRonUp:
    case WrDSCodeUpCmd:
    case WrDSCodeUpCtl:
    case WrDSCodeUpClk:
    case TxRonDn:
    case WrDSCodeDnCmd:
    case WrDSCodeDnCtl:
    case WrDSCodeDnClk:
    case TxTco:
    case TxDqsTcoP:
    case TxDqsTcoN:
    case TcoCompCodeCmd:
    case TcoCompCodeCtl:
    case TcoCompCodeClk:
    case CompRcompOdtUp:
    case CompRcompOdtDn:
    case ClkRCompDrvDownOffset:
    case ClkRCompDrvUpOffset:
//    case ClkCompOnTheFlyUpdtEn:
    case ClkSCompOffset:
    case CtlSCompOffset:
    case CtlRCompDrvUpOffset:
    case CtlRCompDrvDownOffset:
    case CkeRCompDrvDownOffset:
    case CkeRCompDrvUpOffset:
    case VsxHiClkFFOffset:
    case VsxHiCaFFOffset:
    case VsxHiCtlFFOffset:
    case CompOffsetVssHiFF:
    case CkeSCompOffset:
    case CmdSCompOffset:
    case CmdRCompDrvUpOffset:
    case CmdRCompDrvDownOffset:
    case RxPerBitDeskewCal:
    case TxPerBitDeskewCal:
    case CccPerBitDeskewCal:
    case GsmIocCccPiEn:
    case GsmIocCccPiEnOverride:
    case GsmIocDataOdtStaticEn:
    case GsmIocCompOdtStaticDis:
    case GsmIocStrobeOdtStaticEn:
    case RxR:
    case RxC:
    case RxTap1:
    case RxTap2:
    case RxTap3:
    case RxTap4:
    case RxTap1En:
    case RxTap2En:
    case RxTap3En:
    case RloadDqsDn:
    case RxCben:
    case RxBiasCtl:
    case RxLpddrMode:
    case RxBiasVrefSel:
    case RxBiasTailCtl:
    case DataRxD0PiCb:
    case DataSDllPiCb:
    case VccDllRxD0PiCb:
    case VccDllSDllPiCb:
    case GsmIocDqsMaskPulseCnt:
    case GsmIocDqsMaskValue:
    case GsmIocDqsPulseCnt:
    case GsmIocDqOverrideData:
    case GsmIocDqOverrideEn:
    case GsmIocForceOnRcvEn:
    case GsmIocRankOverrideEn:
    case GsmIocRankOverrideVal:
    case GsmIocDataCtlGear1:
    case GsmIocDataCtlGear4:
    case GsmIocRxSALTailCtrl:
    case GsmIocDataWrPreamble:
    case GsmIocDccTrainingMode:
    case GsmIocDccTrainingDone:
    case GsmIocDccDrain:
    case GsmIocDccActiveClks:
    case GsmIocDccActiveBytes:
    case GsmIocDccDcoCompEn:
    case GsmIocDccClkTrainVal:
    case GsmIocDccDataTrainDqsVal:
    case GsmIocLdoFFCodeLockData:
    case GsmIocLdoFFCodeLockCcc:
    case GsmIocDccDcdSampleSelCcc:
    case GsmIocStep1PatternCcc:
    case GsmIocDccCodePh0IndexCcc:
    case GsmIocDccCodePh90IndexCcc:
    case GsmIocTxpbddOffsetCcc:
    case GsmIocTxpbdPh90incrCcc:
    case GsmIocMdllCmnVcdlDccCodeCcc:
    case GsmIocDccDcdSampleSelData:
    case GsmIocStep1PatternData:
    case GsmIocDccCodePh0IndexData:
    case GsmIocDccCodePh90IndexData:
    case GsmIocTxpbddOffsetData:
    case GsmIocTxpbdPh90incrData:
    case GsmIocMdllCmnVcdlDccCodeData:
    case GsmIocDccFsmResetData:
    case GsmIocDccFsmResetCcc:
    case GsmIocDcdRoCalEnData:
    case GsmIocDcdRoCalEnCcc:
    case GsmIocDcdRoCalStartData:
    case GsmIocDcdRoCalStartCcc:
    case GsmIocDcdRoCalSampleCountRstData:
    case GsmIocDcdRoCalSampleCountRstCcc:
    case GsmIocDccStartData:
    case GsmIocDccStartCcc:
    case GsmIocSrzDcdEnData:
    case GsmIocSrzDcdEnCcc:
    case GsmIocMdllDcdEnData:
    case GsmIocMdllDcdEnCcc:
    case GsmIocDcdSampleCountRstData:
    case GsmIocDcdSampleCountStartData:
    case GsmIocDcdRoLfsrData:
    case GsmIocSdllSegmentDisable:
// @todo_adl    case GsmIocRXDeskewForceOn:
    case GsmIocDllWeakLock:
    case GsmIocDllWeakLock1:
    case GsmIocRxClkStg:
    case GsmIocDataRxBurstLen:
    case GsmIocEnDqsNRcvEn:
    case GsmIocEnDqsNRcvEnGate:
    case GsmIocRxMatchedPathEn:
//    case GsmIocLeakerComp:
//    case GsmIocLongWrPreambleLp4:
//    case GsmIocRxPathBiasRcomp:
    case GsmIocVccDllRxDeskewCal:
    case GsmIocVccDllTxDeskewCal:
    case GsmIocVccDllCccDeskewCal:
    case GsmIocForceRxAmpOn:
//    case GsmIocRxTypeSelect:
    case GsmIocCompVddqOdtEn:
    case GsmIocCompVttOdtEn:
    case GsmIocVttPanicCompUpMult:
    case GsmIocVttPanicCompDnMult:
    case GsmIocRxVrefMFC:
//    case GsmIocVrefPwrDnEn:
    case GsmIocDqSlewDlyByPass:
    case GsmIocWlLongDelEn:
    case GsmIocBiasPMCtrl:
    case GsmIocLocalGateD0tx:
    case GsmIocDataOdtMode:
    case GsmIocDataDqOdtParkMode:
    case GsmIocTxEqEn:
    case GsmIocTxEqTapSelect:
    case GsmIocTxOn:
    case GsmIocRxDisable:
    case GsmIocSenseAmpMode:
    case GsmIocDqsRFMode:
    case GsmIocClkPFallNRiseMode:
    case GsmIocClkPRiseNFallMode:
    case GsmIocClkPFallNRiseFeedback:
    case GsmIocClkPRiseNFallFeedback:
    case GsmIocClkPFallNRiseCcc56:
    case GsmIocClkPRiseNFallCcc56:
    case GsmIocClkPFallNRiseCcc78:
    case GsmIocClkPRiseNFallCcc78:
    case GsmIocDdrDqRxSdlBypassEn:
    case GsmIocCaTrainingMode:
    case GsmIocCaParityTrain:
    case GsmIocReadLevelMode:
    case GsmIocWriteLevelMode:
    case GsmIocReadDqDqsMode:
    case GsmIocForceOdtOn:
    case GsmIocForceOdtOnWithoutDrvEn:
    case GsmIocRxPiPwrDnDis:
    case GsmIocTxPiPwrDnDis:
    case GsmIocInternalClocksOn:
    case GsmIocDataDqsOdtParkMode:
    case GsmIocDataDqsNParkLow:
    case GsmIocTxDisable:
    case GsmIocIoReset:
    case GsmIocCmdVrefConverge:
    case GsmIocNoDqInterleave:
    case GsmIocScramLpMode:
    case GsmIocScramDdr4Mode:
    case GsmIocScramDdr5Mode:
    case GsmIocScramGear1:
    case GsmIocScramGear4:
    case GsmIocVccDllGear1:
    case GsmIocVccDllControlBypass_V:
    case GsmIocVccDllControlSelCode_V:
    case GsmIocVccDllControlTarget_V:
    case GsmIocVccDllControlOpenLoop_V:
    case GsmIocVsxHiControlSelCode_V:
    case GsmIocVsxHiControlOpenLoop_V:
    case GsmIocScramLp4Mode:
    case GsmIocScramLp5Mode:
// @todo_adl    case GsmIocScramOvrdPeriodicToDvfsComp:
    case GsmIocLp5Wck2CkRatio:
    case GsmIocChNotPop:
    case GsmIocDisIosfSbClkGate:
    case GsmIocWlWakeCyc:
    case GsmIocWlSleepCyclesAct:
    case GsmIocWlSleepCyclesLp:
    case GsmIocDisDataIdlClkGate:
    case GsmIocDllMask:
//    case GsmIocWlLatency:
    case GsmIocConstZTxEqEn:
    case GsmIocCompClkOn:
    case GsmIocDisableQuickComp:
    case GsmIocSinStep:
    case GsmIocSinStepAdv:
    case GsmIocForceCmpUpdt:
    case GsmIocRptChRepClkOn:
    case GsmIocCmdAnlgEnGraceCnt:
    case GsmIocTxAnlgEnGraceCnt:
    case GsmIocTxDqFifoRdEnPerRankDelDis:
    case GsmIocRxVocMode:
    case GsmIocDataTrainFeedback:
    case GsmIocDdrDqDrvEnOvrdData:
    case GsmIocDdrDqDrvEnOvrdModeEn:
    case GsmIocRxAmpOffsetEn:
    case GsmIocFFCodePiOffset:
    case GsmIocFFCodeIdleOffset:
    case GsmIocFFCodeWeakOffset:
    case GsmIocFFCodeWriteOffset:
    case GsmIocFFCodeReadOffset:
    case GsmIocFFCodePBDOffset:
    case GsmIocFFCodeCCCDistOffset:
    case GsmIocDataInvertNibble:
    case GsmIocCapCancelCodeIdle:
    case GsmIocCapCancelCodePBD:
    case GsmIocCapCancelCodeWrite:
    case GsmIocCapCancelCodeRead:
    case GsmIocCapCancelCodePi:
    case GsmIocVssHiFFCodeIdle:
    case GsmIocVssHiFFCodeWrite:
    case GsmIocVssHiFFCodeRead:
    case GsmIocVssHiFFCodePBD:
    case GsmIocVssHiFFCodePi:
    case GsmIocEnableSpineGate:
    case GsmIocDataDccLaneStatusResult:
    case GsmIocCccDccLaneStatusResult:
    case GsmIocDataDccSaveFullDcc:
    case GsmIocDataDccSkipCRWrite:
    case GsmIocDataDccMeasPoint:
    case GsmIocDataDccRankEn:
    case GsmIocDataDccLaneEn:
    case GsmIocDataDccStepSize:
    case GsmIocDataDcc2xStep:
    case GsmIocCccDccCcc:
    case GsmIocCccDccStepSize:
    case GsmIocCccDcc2xStep:
    case GsmIocRetrainSwizzleCtlByteSel:
    case GsmIocRetrainSwizzleCtlBit:
    case GsmIocRetrainSwizzleCtlRetrainEn:
    case GsmIocRetrainSwizzleCtlSerialMrr:
    case GsmIocRetrainInitPiCode:
    case GsmIocRetrainDeltaPiCode:
    case GsmIocRetrainRoCount:
    case GsmIocRetrainCtlInitTrain:
    case GsmIocRetrainCtlDuration:
    case GsmIocRetrainCtlResetStatus:
    case DqDrvVrefUp:
    case DqDrvVrefDn:
    case DqOdtVrefUp:
    case DqOdtVrefDn:
    case CmdDrvVrefUp:
    case CmdDrvVrefDn:
    case CtlDrvVrefUp:
    case CtlDrvVrefDn:
    case ClkDrvVrefUp:
    case ClkDrvVrefDn:
    case GsmMctRCD:
    case GsmMctRP:
    case GsmMctRPabExt:
    case GsmMctRAS:
    case GsmMctRDPRE:
    case GsmMctPPD:
    case GsmMctWRPRE:
    case GsmMctFAW:
    case GsmMctRRDsg:
    case GsmMctRRDdg:
    case GsmMctLpDeratingExt:
    case GsmMctRDRDsg:
    case GsmMctRDRDdg:
    case GsmMctRDRDdr:
    case GsmMctRDRDdd:
    case GsmMctRDWRsg:
    case GsmMctRDWRdg:
    case GsmMctRDWRdr:
    case GsmMctRDWRdd:
    case GsmMctWRRDsg:
    case GsmMctWRRDdg:
    case GsmMctWRRDdr:
    case GsmMctWRRDdd:
    case GsmMctWRWRsg:
    case GsmMctWRWRdg:
    case GsmMctWRWRdr:
    case GsmMctWRWRdd:
    case GsmMctCKE:
    case GsmMctXP:
    case GsmMctXPDLL:
    case GsmMctPRPDEN:
    case GsmMctRDPDEN:
    case GsmMctWRPDEN:
    case GsmMctCA2CS:
    case GsmMctCSL:
    case GsmMctCSH:
    case GsmMctXSDLL:
    case GsmMctXSR:
    case GsmMctSR:
    case GsmMctZQOPER:
    case GsmMctMOD:
    case GsmMctZQCAL:
    case GsmMctZQCS:
    case GsmMctZQCSPeriod:
    case GsmMctSrIdle:
    case GsmMctREFI:
    case GsmMctRFC:
    case GsmMctOrefRi:
    case GsmMctRefreshHpWm:
    case GsmMctRefreshPanicWm:
    case GsmMctREFIx9:
    case GsmMctHpRefOnMrs:
    case GsmMctSrxRefDebits:
    case GsmMccLDimmMap:
    case GsmMccEnhancedInterleave:
    case GsmMccEccMode:
    case GsmMccAddrDecodeDdrType:
    case GsmMccLChannelMap:
    case GsmMccSChannelSize:
    case GsmMccChWidth:
    case GsmMccHalfCachelineMode:
    case GsmMccLDimmSize:
    case GsmMccLDimmDramWidth:
    case GsmMccLDimmRankCnt:
    case GsmMccSDimmSize:
    case GsmMccSDimmDramWidth:
    case GsmMccSDimmRankCnt:
    case GsmMccExtendedBankHashing:
    case GsmMccSaveFreqPoint:
    case GsmMccEnableRefresh:
    case GsmMccEnableSr:
    case GsmMccMcInitDoneAck:
    case GsmMccMrcDone:
    case GsmMccEnableDclk:
    case GsmMccPureSrx:
    case GsmMccLp4FifoRdWr:
    case GsmMccIgnoreCke:
    case GsmMccMaskCs:
    case GsmMccCpgcInOrder:
    case GsmMccInOrderIngress:
    case GsmMccCadbEnable:
    case GsmMccDeselectEnable:
    case GsmMccBusRetainOnBubble:
    case GsmMccBlockXarb:
    case GsmMccResetOnCmd:
    case GsmMccResetDelay:
    case GsmMccDramType:
    case GsmMccCmdStretch:
    case GsmMccCmdGapRatio:
    case GsmMccAddrMirror:
    case GsmMccCmdTriStateDis:
    case GsmMccCmdTriStateDisTrain:
    case GsmMccFreqPoint: // This aliases with ddr_probless_low_frequency.
    case GsmMccEnableOdtMatrix:
    case GsmMccX8Device:
    case GsmMccGear2:
    case GsmMccGear4:
    case GsmMccDdr4OneDpc:
    case GsmMccWrite0En:
    case GsmMccMultiCycCmd:
    case GsmMccWCKDiffLowInIdle:
    case GsmMctOdtRdDuration:
    case GsmMctOdtRdDelay:
    case GsmMctWrEarlyOdt:
    case GsmMctOdtWrDuration:
    case GsmMctOdtWrDelay:
    case GsmMctCL:
    case GsmMctCWL:
    case GsmMctCWLAdd:
    case GsmMctCWLDec:
    case GsmMctAONPD:
    case GsmMctCKCKEH:
    case GsmMctCSCKEH:
    case GsmMccRankOccupancy:
    case GsmMccMcSrx:
    case GsmMccRefInterval:
    case GsmMccRefStaggerEn:
    case GsmMccRefStaggerMode:
    case GsmMccDisableStolenRefresh:
    case GsmMccEnRefTypeDisplay:
    case GsmMccHashMask:
    case GsmMccLsbMaskBit:
    case GsmMccHashMode:
    case GsmMctCPDED:
    case GsmMctRFCpb:
    case GsmMctRefm:
    case GsmMccDisableCkTristate:
    case GsmMccPbrDis:
    case GsmMccPbrIssueNop:
    case GsmMccRefreshAbrRelease:
    case GsmMccPbrDisOnHot:
    case GsmMccOdtOverride:
    case GsmMccOdtOn:
    case GsmMccMprTrainDdrOn:
    case GsmMccCkeOverride:
    case GsmMccCkeOn:
    case GsmCmiMcOrg:
    case GsmCmiHashMask:
    case GsmCmiLsbMaskBit:
    case GsmCmiStackedMode:
    case GsmCmiStackMsMap:
    case GsmCmiLMadSliceMap:
    case GsmCmiSMadSliceSize:
    case GsmCmiStackedMsHash:
    case GsmCmiSliceDisable:
    case GsmMccDdr5Device8GbDimmL:
    case GsmMccDdr5Device8GbDimmS:
    case GsmIocEccEn:
    case GsmIocWrite0En:
    case GsmIocScramWrite0En:
    case GsmMccBlockCke:
    case GsmMctREFSbRd:
    case GsmMccCsOverride:
    case GsmMccCsOverrideVal:
    case GsmMccRrdValidTrigger:
    case GsmMccRrdValidOverflow:
    case GsmMccRrdValidValue:
    case GsmMccRrdValidSign:
    case GsmMccDeswizzleByte:
    case GsmMccDeswizzleBit:
    case GsmMccLp5BankMode:
    case GsmMccMrrBlnMax:
      break;

    default:
      IsSupported = FALSE;
      break;
  }

  if (!IsSupported) {
    MRC_DEBUG_MSG (
      &MrcData->Outputs.Debug,
      MSG_LEVEL_ERROR,
      "%s GetSet Group %s(%d) is not supported!\n",
      gErrString,
      GsmGtDebugStrings[Group],
      Group
      );
    return mrcWrongInputParameter;
  }
  return mrcSuccess;
}
#endif

/**
  This function is used to determine if the GSM_GT passed in is a complex parameter.

  @param[in]  MrcData - Pointer to global data structure.
  @param[in]  Group   - GetSet group to check.
  @param[in]  IsComplex - The requested check is to see if the parameter is complex.  Otherwise is there a side effect.

  @retval TRUE if complex or side effect, otherwise FALSE.
**/
BOOLEAN
MrcCheckComplexOrSideEffect (
  IN  MrcParameters *const  MrcData,
  IN  GSM_GT        const   Group,
  IN  BOOLEAN       const   IsComplex
  )
{
  BOOLEAN RetVal;

  RetVal = FALSE;

  if (IsComplex) {
    switch (Group) {
      case GsmIocSenseAmpMode:
      case GsmIocReadDqDqsMode:
      case GsmIocCaTrainingMode:
      case GsmIocWriteLevelMode:
      case GsmIocReadLevelMode:
      case RxDqsBitDelay:
        RetVal = TRUE;
        break;
      // Not a complex parameter
      default:
        break;
    }
  } else {
    // Side Effect
    switch (Group) {
      case RecEnDelay:
      case RecEnOffset:
      case TxDqsDelay:
      case TxDqBitDelay:
      case TxDqDelay:
      case ClkGrpPi:
      case TxDqsOffset:
      case TxDqOffset:
      case WckGrpPi:
        RetVal = TRUE;
        break;

      case GsmIocWlSleepCyclesAct:
        if (MrcData->Inputs.UlxUlt) {
          RetVal = TRUE;
        }
      // No Side Effect.
      default:
        break;
    }
  }

  /*MRC_HAL_DEBUG_MSG (
    &MrcData->Outputs.Debug,
    MSG_LEVEL_HAL,
    "GetSet Group %s(%d) is %s a %s param.\n",
    GsmGtDebugStrings[Group],
    Group,
    (RetVal) ? "" : "not",
    (IsComplex) ? "complex" :  "side effect"
    );*/

  return RetVal;
}

/*
  This function is used to update other Phy register in the channel based on DDR type.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      Dimm        - DIMM Number within the DDR Channel (0-based). Ignored as Rank is rank number in the channel.
  @param[in]      Rank        - Rank number within a channel (0-based).
  @param[in]      Strobe      - If Group is a CMD/CTL/CLK Index type, this is the index for that signal.  Otherwise, Dqs data group within the rank (0-based).
  @param[in]      Bit         - Bit index within the data group (0-based).
  @param[in]      FreqIndex   - Index supporting multiple operating frequencies.
  @param[in]      Level       - DDRIO level to access.
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval Nothing.
**/
void
MrcGetSetUpdatePhyChannels (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Socket,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Dimm,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      UINT32        const   Bit,
  IN      UINT32        const   FreqIndex,
  IN      GSM_LT        const   Level,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN      INT64         *const  Value
  )
{
  UINT8             Ddr4SideEffect[3] = { 1, 2, 3 };
  UINT8             Ddr5SideEffect[1] = { 1 };
  UINT8             SideEffectLoop;
  UINT32            SideEffectController;
  UINT32            SideEffectChannel;
  MrcOutput        *Outputs;
  MrcControllerOut *ControllerOut;

  Outputs = &MrcData->Outputs;
  ControllerOut = &Outputs->Controller[Controller];

  switch (Group) {
    // MCMISCS_READCFGCHx_REG
    case RxIoTclDelay:
    case RxFifoRdEnTclDelay:
    case RxDqDataValidDclkDelay:
    case RxDqDataValidQclkDelay:
      // MCMISCS_WRITECFGCHx_REG
    case RxRptChDqClkOn:
    case TxRptChDqClkOn:
    case TxDqFifoWrEnTcwlDelay:
    case TxDqFifoRdEnTcwlDelay:
    case GsmIocRptChRepClkOn:
    case GsmIocCmdAnlgEnGraceCnt:
    case GsmIocTxAnlgEnGraceCnt:
    case GsmIocTxDqFifoRdEnPerRankDelDis:
      // MCMISCS_READCFGCHxy_REG
    case RxFlybyDelay:
      // MCMISCS_WRITECFGCHxy_REG
    case TxDqFifoRdEnFlybyDelay:
      // MCMISCS_RXDQFIFORDENCHxy_REG
    case RxFifoRdEnFlybyDelay:
    case TcoCompCodeCmd:
    case TcoCompCodeCtl:
    case TcoCompCodeClk:
      switch (Outputs->DdrType) {
        case MRC_DDR_TYPE_DDR4:
          for (SideEffectLoop = 0; SideEffectLoop < sizeof(Ddr4SideEffect); SideEffectLoop++) {
            SideEffectController = 0;
            SideEffectChannel = Controller * MAX_CHANNEL;
            SideEffectChannel += Channel * (MAX_CHANNEL / ControllerOut->ChannelCount) + Ddr4SideEffect[SideEffectLoop];
            GetSet (MrcData, Socket, SideEffectController, SideEffectChannel, Dimm, Rank, Strobe, Bit, FreqIndex, Level, Group, Mode, Value);
          }
          break;
        case MRC_DDR_TYPE_DDR5:
          for (SideEffectLoop = 0; SideEffectLoop < sizeof(Ddr5SideEffect); SideEffectLoop++) {
            SideEffectController = 0;
            SideEffectChannel = Controller * MAX_CHANNEL;
            SideEffectChannel += Channel * (MAX_CHANNEL / Outputs->MaxChannels) + Ddr5SideEffect[SideEffectLoop];
            GetSet (MrcData, Socket, SideEffectController, SideEffectChannel, Dimm, Rank, Strobe, Bit, FreqIndex, Level, Group, Mode, Value);
          }
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
}

/*
  This function perform any side effect action required from the Group that was just set.
  It will inherit the same Mode parameter so it passed it on to the actions executed.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      SubChannel  - DDR SubChannel number within a Channel (0-Based).
  @param[in]      Dimm        - DIMM Number within the DDR Channel (0-based). Ignored as Rank is rank number in the channel.
  @param[in]      Rank        - Rank number within a channel (0-based).
  @param[in]      Strobe      - If Group is a CMD/CTL/CLK Index type, this is the index for that signal.  Otherwise, Dqs data group within the rank (0-based).
  @param[in]      Bit         - Bit index within the data group (0-based).
  @param[in]      FreqIndex   - Index supporting multiple operating frequencies.
  @param[in]      Level       - DDRIO level to access.
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval Nothing.
**/
void
MrcGetSetSideEffect (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Socket,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   SubChannel,
  IN      UINT32        const   Dimm,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      UINT32        const   Bit,
  IN      UINT32        const   FreqIndex,
  IN      GSM_LT        const   Level,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN      INT64         *const  Value
  )
{
  MrcInput  *Inputs;
  MrcOutput *Outputs;
  GSM_GT    WriteGroup;
  GSM_GT    OffsetGroup;
  GSM_GT    PiGroup;
  INT64     GetSetVal;
  INT64     GetSetVal90;
  INT64     GetSetValOffset;
  GSM_GT    WriteGroup90;
  UINT32    LocalRank;
  UINT32    LocalModeWrite;
  UINT32    LocalModeRead;
  UINT32    SearchVal;
  BOOLEAN   Gear2;
  BOOLEAN   Gear4;
  BOOLEAN   Lpddr5;
  UINT32    PrintMode;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Gear2   = Outputs->Gear2;
  Gear4   = Outputs->Gear4;
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  WriteGroup90 = GsmGtMax;
  // Read from cache or register based on the write Mode to the group
  LocalModeRead = GSM_READ_ONLY;
  if (Mode & GSM_READ_CSR) {
    LocalModeRead |= GSM_READ_CSR;
  }
  // Write based on original mode. Clear offset write flag
  LocalModeWrite = Mode;
  LocalModeWrite &= ~GSM_WRITE_OFFSET;

  if ((Mode & GSM_UPDATE_CACHE) == 0) {
    // If caller did not update cache, then read from the CR
    LocalModeRead |= GSM_READ_CSR;
    // If original mode is not using the cache, the side effect write should not use it as well
    LocalModeWrite |= GSM_READ_CSR;
  }
  if (Group == GsmIocWlSleepCyclesAct) {
    MrcGetSet (MrcData, Socket, Controller, Channel, Dimm, 0, Strobe, Bit, FreqIndex, Level, GsmIocWlSleepCyclesLp, Mode, Value);
  }

  if ((Group == RecEnDelay) || (Group == RecEnOffset) || (Group == TxDqsDelay) || (Group == TxDqsOffset) || (Group == TxDqDelay) || (Group == TxDqOffset)) {
    // Find the smallest RecEnDelay/TxDqsDelay/TxDqDelay in the Byte/Channel and update the new [Rx,Tx]RankMuxDelay
    if ((Group == RecEnDelay) || (Group == RecEnOffset)) {
      WriteGroup  = RxRankMuxDelay;
      OffsetGroup = RecEnOffset;
      PiGroup     = RecEnDelay;
    } else if ((Group == TxDqDelay) || (Group == TxDqOffset)) {
      WriteGroup    = TxRankMuxDelay;
      OffsetGroup   = TxDqOffset;
      PiGroup       = TxDqDelay;
      if (Group == TxDqDelay) {
        WriteGroup90  = TxDqDelay90;
      }
    } else { // TxDqsDelay, TxDqsOffset
      WriteGroup    = TxDqsRankMuxDelay;
      OffsetGroup   = TxDqsOffset;
      PiGroup       = TxDqsDelay;
      if (Group == TxDqsDelay) {
        WriteGroup90  = TxDqsDelay90;
      }
    }
    SearchVal = MRC_UINT32_MAX;
    for (LocalRank = 0; LocalRank < MAX_RANK_IN_CHANNEL; LocalRank++) {
      if (MrcRankExist (MrcData, Controller, Channel, LocalRank)) {
        MrcGetSet (MrcData, Socket, Controller, Channel, Dimm, LocalRank, Strobe, Bit, FreqIndex, Level, OffsetGroup, LocalModeRead, &GetSetValOffset);
        MrcGetSet (MrcData, Socket, Controller, Channel, Dimm, LocalRank, Strobe, Bit, FreqIndex, Level, PiGroup,     LocalModeRead, &GetSetVal);
        GetSetVal = GetSetVal + GetSetValOffset;
        SearchVal = MIN (SearchVal, (UINT32) GetSetVal);
      }
    }
    // Align to a QCLK (Gear1) or DCLK (Gear2)
    // @todo_adl check Gear4 case
    SearchVal = SearchVal >> (6 + (Gear2 ? 1 : (Gear4 ? 2 : 0)));
    if ((Lpddr5 || Outputs->Lp4x) && (WriteGroup == RxRankMuxDelay)) {
      SearchVal = (SearchVal <= 1) ? 0 : (SearchVal - 1);
    }
    if (Inputs->DtHalo) {
      SearchVal = (SearchVal <= 2) ? 0 : (SearchVal - 2);
    }
    PrintMode = 0; // PrintValue

    GetSetVal = SearchVal;
    MrcGetSet (MrcData, Socket, Controller, Channel, Dimm, 0, Strobe, Bit, FreqIndex, Level, WriteGroup, LocalModeWrite | PrintMode, &GetSetVal);

    if (Gear4 && (WriteGroup90 != GsmGtMax)) {
      // Update PH90 PI based on the main one; no need to add the offset from DataOffsetTrain, it's being added in HW
      MrcGetSet (MrcData, Socket, Controller, Channel, Dimm, Rank, Strobe, Bit, FreqIndex, Level, PiGroup, LocalModeRead, &GetSetVal);
      GetSetVal90 = (GetSetVal + 64) & DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_MAX;
      MrcGetSet (MrcData, Socket, Controller, Channel, Dimm, Rank, Strobe, Bit, FreqIndex, Level, WriteGroup90, LocalModeWrite, &GetSetVal90);
    }
  }
  if (Gear4 && (Group == ClkGrpPi)) {
    // Update CLK PH90 PI based on the CLK PI
    MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, ClkGrpPi, LocalModeRead, &GetSetVal);
    GetSetVal90 = (GetSetVal + 64) & CH2CCC_CR_PICODE2_mdll_cmn_pi5code_MAX;
    MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, ClkGrpG4Pi, LocalModeWrite, &GetSetVal90);
  }

  if (Gear4 && (Group == WckGrpPi)) {
    // Update WCK PH90 PI based on the WCK PI. It is per channel, hence Rank is ignored
    MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, WckGrpPi, LocalModeRead, &GetSetVal);
    GetSetVal90 = (GetSetVal + 64) & CH2CCC_CR_PICODE3_mdll_cmn_pi7code_MAX;
    MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, WckGrpPi90, LocalModeWrite, &GetSetVal90);
  }

  if (Gear4 && (Group == TxDqBitDelay)) {
    // Update TxDqBit PH90 PI based on the TxDqBit PI
    MrcGetSetBit (MrcData, Controller, Channel, Rank, Strobe, Bit, TxDqBitDelay, LocalModeRead, &GetSetVal);
    // Program these registers to same value
    // phase 90 would affect even fall and odd fall
    // DCC controls even / odd, phase0 90 fall
    MrcGetSetBit (MrcData, Controller, Channel, Rank, Strobe, Bit, TxDqBitDelay90, LocalModeWrite, &GetSetVal);
  }

  if (Group == RxCben) {
    MrcGetSetChStrb(MrcData, Controller, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, DataRxD0PiCb, LocalModeWrite, Value);
    MrcGetSetChStrb(MrcData, Controller, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, DataSDllPiCb, LocalModeWrite, Value);
    MrcGetSetNoScope(MrcData, VccDllRxD0PiCb, LocalModeWrite, Value);
    MrcGetSetNoScope(MrcData, VccDllSDllPiCb, LocalModeWrite, Value);
  }
}

/**
  This function handles accessing and updating complex parameter values.
  This function will adjust the signal for that scope.  It will Not adjust other relative signals.
    Any shared parameter will need to be accounted for outside this function.
  The way each complex parameter is composed is project specific.
  The units of each parameter is specified in the parameter declaration in GSM_GT.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      SubChannel  - DDR SubChannel number within a Channel (0-Based).
  @param[in]      Rank        - Rank number within a channel (0-based).
  @param[in]      Strobe      - If Group is a CMD/CTL/CLK Index type, this is the index for that signal.  Otherwise, Dqs data group within the rank (0-based).
  @param[in]      Bit         - Bit index within the data group (0-based).
  @param[in]      FreqIndex   - Index supporting multiple operating frequencies.
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus: mrcComplexParamDone - no more programming is needed, otherwise continue with GetSet flow.
**/
MrcStatus
MrcGetSetComplexParam (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Socket,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   SubChannel,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      UINT32        const   Bit,
  IN      UINT32        const   FreqIndex,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  MrcStatus Status;
  INT64     GetSetVal;
  UINT32    LocalModeWrite;

  // Write based on original mode. Clear offset write flag
  LocalModeWrite = Mode;
  LocalModeWrite &= ~GSM_WRITE_OFFSET;
  if ((Mode & GSM_UPDATE_CACHE) == 0) {
    // If original mode is not using the cache, the side effect write should not use it as well
    LocalModeWrite |= GSM_READ_CSR;
  }


  switch (Group) {
    case RxDqsBitDelay:
      GetSetVal = *Value;
      MrcGetSetBit (MrcData, Controller, Channel, Rank, Strobe, Bit, RxDqsNBitDelay, Mode, &GetSetVal);
      MrcGetSetBit (MrcData, Controller, Channel, Rank, Strobe, Bit, RxDqsPBitDelay, Mode, &GetSetVal);
      *Value = GetSetVal; // in case of Read
      break;

    default:
      break;
  }

  Status  = mrcSuccess;
  return Status;
}

/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket Index, DIMM index, FreqIndex, and Level.
  Only one setting for the whole controller, so Channel, Rank, Strobe, and Bit index is dropped.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroupController0 (
  IN      MrcParameters *const  MrcData,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroupChannel (MrcData, 0, Group, Mode, Value);
}

/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket Index, DIMM index, FreqIndex, and Level.
  Only one setting for the whole controller, so Channel, Rank, Strobe, and Bit index is dropped.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetMc (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Controller,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSet (
          MrcData,
          MRC_IGNORE_ARG,
          Controller,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          DdrLevel,
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with SOC.
  This function is used to set parameters that do not have any specificity to them.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetNoScope (
  IN      MrcParameters *const  MrcData,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSet (
          MrcData,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          DdrLevel,
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with SOC.
  This function ignores unused parameters in Core MRC to reduce code space.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based)
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetMcCh (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSet (
          MrcData,
          MRC_IGNORE_ARG,
          Controller,
          Channel,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          DdrLevel,
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with SOC.
  This function ignores unused parameters in Core MRC to reduce code space.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      Rank        - Rank within the DDR Channel (0-based).
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetMcChRnk (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Rank,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSet (
          MrcData,
          MRC_IGNORE_ARG,
          Controller,
          Channel,
          MRC_IGNORE_ARG,
          Rank,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          DdrLevel,
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket Index, DIMM index, FreqIndex, and Level.
  This function is used to access indexed Command/Control/Clock groups.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      Channel   - DDR Channel Number within the processor socket (0-based)
  @param[in]      Index     - Group index to Get/Set.
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoCmdGroup (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Index,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroupSocket0Sch (MrcData, Channel, MRC_IGNORE_ARG, MRC_IGNORE_ARG, Index, MRC_IGNORE_ARG, Group, Mode, Value);
}

/**
  Top level function used to interact with SOC.
  This function ignores unused parameters in Core MRC to reduce code space.
  This function is used to access indexed Command/Control/Clock groups.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      Rank        - Rank within the DDR Channel (0-based).
  @param[in]      Index       - Group index to Get/Set.
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetCcc (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Index,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSet (
          MrcData,
          MRC_IGNORE_ARG,
          Controller,
          Channel,
          MRC_IGNORE_ARG,
          Rank,
          Index,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          DdrLevel,
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with SOC.
  This function ignores unused parameters in Core MRC to reduce code space.
  This function is used to access indexed Command/Control/Clock groups using Lane.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      Rank        - Rank within the DDR Channel (0-based).
  @param[in]      Lane        - Lane index to GSM_GT Group.
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetCccLane (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Lane,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSet (
          MrcData,
          MRC_IGNORE_ARG,
          Controller,
          Channel,
          MRC_IGNORE_ARG,
          Rank,
          MRC_IGNORE_ARG,
          Lane,
          MRC_IGNORE_ARG,
          DdrLevel,
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with SOC.
  This function ignores unused parameters in Core MRC to reduce code space.
  This function is used to access individual CCC instance.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      CccIndex  - CCC Instance index [0..7].
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetCccInst (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   CccIndex,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetCccInstLane (
          MrcData,
          CccIndex,
          MRC_IGNORE_ARG,   // Lane is not used
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with SOC.
  This function ignores unused parameters in Core MRC to reduce code space.
  This function is used to access individual CCC instance for Lane-specific groups.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      CccIndex  - CCC Instance index [0..7].
  @param[in]      Lane      - Lane index to GSM_GT Group.
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetCccInstLane (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   CccIndex,
  IN      UINT32        const   Lane,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  MrcStatus Status;
  MrcStatus CurStatus;
  UINT32    Start;
  UINT32    End;
  UINT32    Index;
  Status = mrcSuccess;

  if (CccIndex == MAX_CCC_INSTANCES) {
    Start = 0;
      End   = MAX_CCC_INSTANCES - 1;
  } else {
    Start = End = CccIndex;
  }

  for (Index = Start; Index <= End; ++Index) {
    CurStatus = MrcGetSet (
                  MrcData,
                  MRC_IGNORE_ARG,
                  MRC_IGNORE_ARG,
                  MRC_IGNORE_ARG,
                  MRC_IGNORE_ARG,
                  MRC_IGNORE_ARG,
                  Index,            // CCC Index is passed in as Strobe (0..7 is less than MAX_SDRAM_IN_DIMM, so it won't be treated as multicast)
                  Lane,
                  MRC_IGNORE_ARG,
                  DdrLevel,
                  Group,
                  Mode,
                  Value
                  );
    if (Status == mrcSuccess) {
      Status = CurStatus;
    }
  }
  return Status;
}

/**
  Top level function used to interact with SOC.
  This function ignores unused parameters in Core MRC to reduce code space.
  This function is used to access DDRVCCDLL0 vs DDRVCCDLL1 register offsets.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Index       - Group index to Get/Set.
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetVccDll (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Index,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSet (
          MrcData,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          Index,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          DdrLevel,
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket Index, DIMM index, FreqIndex, and Level.
  The lowest scope of access is Channel, so Rank, Strobe, and Bit index is dropped.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      Channel   - DDR Channel Number within the processor socket (0-based)
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroupChannel (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Channel,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroupStrobe (MrcData, Channel, MRC_IGNORE_ARG, MRC_IGNORE_ARG, Group, Mode, Value);
}
/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket Index, DIMM index, FreqIndex, and Level.
  This function is for those registers that are only per Channel per Rank granularity.
  The lowest scope of access is Rank, so Strobe, and Bit index is dropped.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      Channel   - DDR Channel Number within the processor socket (0-based)
  @param[in]      Rank      - Rank within the DDR Channel (0-based).
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroupChannelRank (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Rank,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroupStrobe (MrcData, Channel, Rank, MRC_IGNORE_ARG, Group, Mode, Value);
}

/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket Index, DIMM index, FreqIndex, and Level.
  This function is for those registers that are only per Strobe per Channel granularity.
  The lowest scope of access is Strobe, so the Bit index is dropped.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      Channel   - DDR Channel Number within the processor socket (0-based)
  @param[in]      Strobe    - Dqs data group within the rank (0-based).
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroupChannelStrobe (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Strobe,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroupStrobe (MrcData, Channel, MRC_IGNORE_ARG, Strobe, Group, Mode, Value);
}

/**
  Top level function used to interact with SOC.
  This function ignores unused parameters in Core MRC to reduce code space.
  This function is for those registers that are only per Strobe per Channel granularity.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      Strobe      - Dqs data group within the rank (0-based).
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetChStrb (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Strobe,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSet (
          MrcData,
          MRC_IGNORE_ARG,
          Controller,
          Channel,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          Strobe,
          MRC_IGNORE_ARG,
          MRC_IGNORE_ARG,
          DdrLevel,
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket Index, DIMM index, FreqIndex, and Level.
  The lowest scope of access is Strobe, so the Bit index is dropped.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      Channel   - DDR Channel Number within the processor socket (0-based)
  @param[in]      Rank      - Rank number within a channel (0-based).
  @param[in]      Strobe    - Dqs data group within the rank (0-based).
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroupStrobe (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroupSocket0 (MrcData, Channel, Rank, Strobe, MRC_IGNORE_ARG, Group, Mode, Value);
}

/**
  Top level function used to interact with SOC.
  This function ignores unused parameters in Core MRC to reduce code space.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      Rank        - Rank number within a channel (0-based).
  @param[in]      Strobe      - Dqs data group within the rank (0-based).
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetStrobe (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSet (
          MrcData,
          MRC_IGNORE_ARG,
          Controller,
          Channel,
          MRC_IGNORE_ARG,
          Rank,
          Strobe,
          MRC_IGNORE_ARG,
          0,
          DdrLevel,
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket Index, DIMM index, FreqIndex, and Level.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      Channel   - DDR Channel Number within the processor socket (0-based)
  @param[in]      Rank      - Rank number within a channel (0-based).
  @param[in]      Strobe    - Dqs data group within the rank (0-based).
  @param[in]      Bit       - Bit index within the data group (0-based).
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroupSocket0 (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      UINT32        const   Bit,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroup (MrcData, 0, Channel, MRC_IGNORE_ARG, MRC_IGNORE_ARG, Rank, Strobe, Bit, 0, DdrLevel, Group, Mode, Value);
}

/**
  Top level function used to interact with SOC.
  This function ignores unused parameters in Core MRC to reduce code space.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel   - DDR Channel Number within the processor socket (0-based)
  @param[in]      Rank      - Rank number within a channel (0-based).
  @param[in]      Strobe    - Dqs data group within the rank (0-based).
  @param[in]      Bit       - Bit index within the data group (0-based).
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetBit (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      UINT32        const   Bit,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSet (
          MrcData,
          MRC_IGNORE_ARG,
          Controller,
          Channel,
          MRC_IGNORE_ARG,
          Rank,
          Strobe,
          Bit,
          MRC_IGNORE_ARG,
          DdrLevel,
          Group,
          Mode,
          Value
          );
}

/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket, Channel, DIMM, Rank, Strobe, Bit, and Level.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      FreqIndex - Index supporting multiple operating frequencies.
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroupFreqIndex (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   FreqIndex,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroup (MrcData, 0, MRC_IGNORE_ARG, MRC_IGNORE_ARG, MRC_IGNORE_ARG, MRC_IGNORE_ARG, MRC_IGNORE_ARG, MRC_IGNORE_ARG, FreqIndex, DdrLevel, Group, Mode, Value);
}

/**
  Top level function used to interact with DDRIO parameters.
    Socket Index, DIMM index, FreqIndex, and Level.

  @param[in]      MrcData   - Pointer to global data structure.
  @param[in]      Channel   - DDR Channel Number within the processor socket (0-based)
  @param[in]      SubChannel  - DDR SubChannel number within a Channel (0-Based).
  @param[in]      Rank      - Rank number within a channel (0-based).
  @param[in]      Strobe    - Dqs data group within the rank (0-based).
  @param[in]      Bit       - Bit index within the data group (0-based).
  @param[in]      Group     - DDRIO group to access.
  @param[in]      Mode      - Bit-field flags controlling Get/Set.
  @param[in,out]  Value     - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroupSocket0Sch (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Channel,
  IN      UINT32        const   SubChannel,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      UINT32        const   Bit,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroup (MrcData, 0, Channel, SubChannel, MRC_IGNORE_ARG, Rank, Strobe, Bit, 0, DdrLevel, Group, Mode, Value);
}

/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket Index, DIMM index, FreqIndex, and Level.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      SubChannel  - DDR SubChannel number within a Channel (0-Based).
  @param[in]      Rank        - Rank number within a channel (0-based).
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroupChSchRnk (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Channel,
  IN      UINT32        const   SubChannel,
  IN      UINT32        const   Rank,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroup (MrcData, 0, Channel, SubChannel, MRC_IGNORE_ARG, Rank, MRC_IGNORE_ARG, MRC_IGNORE_ARG, 0, DdrLevel, Group, Mode, Value);
}

/**
  Top level function used to interact with DDRIO parameters.
  This function ignores unused parameters in Core MRC to reduce code space.
    Socket Index, DIMM index, FreqIndex, and Level.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      SubChannel  - DDR SubChannel number within a Channel (0-Based).
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroupChSch (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Channel,
  IN      UINT32        const   SubChannel,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  return MrcGetSetDdrIoGroup (MrcData, 0, Channel, SubChannel, MRC_IGNORE_ARG, MRC_IGNORE_ARG, MRC_IGNORE_ARG, MRC_IGNORE_ARG, 0, DdrLevel, Group, Mode, Value);
}

/**
  Converting controller, channel, rank, strobe number from System to IP register architecture.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      Rank        - Rank number within a channel (0-based).
  @param[in]      Strobe      - Dqs data group within the rank or the command group (0-based).
  @param[in]      Group       - DDRIO group to access.

  @retval MrcStatus
**/
MrcStatus
MrcTranslateSystemToIp (
  IN      MrcParameters *const  MrcData,
  IN OUT  UINT32        *const  Controller,
  IN OUT  UINT32        *const  Channel,
  IN OUT  UINT32        *const  Rank,
  IN OUT  UINT32        *const  Strobe,
  IN      GSM_GT        const   Group
  )
{
  MrcOutput   *Outputs;
  MrcStatus   Status;
  MrcDebug    *Debug;
  UINT32      TransController;
  UINT32      TransChannel;
  UINT32      TransRank;
  UINT32      TransStrobe;
  BOOLEAN     UlxUlt;
  BOOLEAN     DtHalo;
  MrcDdrType  DdrType;

  static const UINT8 CtlPiCccL2pDdr5[MAX_CONTROLLER][2][MAX_RANK_IN_CHANNEL] = {
    {
      {2, 2, 3, 2}, // Ch0
      {4, 4, 5, 5}  // Ch1
    }, // MC0
    {
      {0, 0, 1, 0}, // Ch0
      {6, 6, 7, 7}  // Ch1
    }  // MC1
  };
  static const UINT8 CmdPiCccL2pDdr5[MAX_CONTROLLER][2][MRC_DDR5_CMD_GRP_MAX] = {
    {
      {2, 3}, // Ch0
      {4, 5}  // Ch1
    }, // MC0
    {
      {0, 1}, // Ch0
      {6, 7}  // Ch1
    }  // MC1
  };
  static const UINT8 ClkPiCccL2pDdr5[MAX_CONTROLLER][2][MAX_RANK_IN_CHANNEL] = {
    {
      {3, 3, 2, 2}, // Ch0
      {5, 5, 4, 4}  // Ch1
    }, // MC0
    {
      {1, 1, 0, 0}, // Ch0
      {7, 7, 6, 6}  // Ch1
    }  // MC1
  };

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  DdrType = Outputs->DdrType;
  Status  = mrcSuccess;
  UlxUlt  = MrcData->Inputs.UlxUlt;
  DtHalo  = MrcData->Inputs.DtHalo;

  // Initialize the translated variables with the input. This gives us a code optimization
  // where we don't have to assign in every Group that doesn't use that scope.
  TransController = *Controller;
  TransChannel    = *Channel;
  TransRank       = *Rank;
  TransStrobe     = *Strobe;

  switch (Group) {
    // MCMISCS_READCFGCHx_REG
    case RxIoTclDelay:
    case RxFifoRdEnTclDelay:
    case RxDqDataValidDclkDelay:
    case RxDqDataValidQclkDelay:
    // MCMISCS_WRITECFGCHx_REG
    case RxRptChDqClkOn:
    case TxRptChDqClkOn:
    case TxDqFifoWrEnTcwlDelay:
    case TxDqFifoRdEnTcwlDelay:
    case GsmIocRptChRepClkOn:
    case GsmIocCmdAnlgEnGraceCnt:
    case GsmIocTxAnlgEnGraceCnt:
    case GsmIocTxDqFifoRdEnPerRankDelDis:
    // MCMISCS_READCFGCHxy_REG
    case RxFlybyDelay:
    // MCMISCS_WRITECFGCHxy_REG
    case TxDqFifoRdEnFlybyDelay:
    // MCMISCS_RXDQFIFORDENCHxy_REG
    case RxFifoRdEnFlybyDelay:
      // For DDR4/5, other PHY channels will be updated inside MrcGetSetUpdatePhyChannels()
      TransChannel = (*Controller * MAX_CHANNEL);
      TransChannel += (*Channel * (MAX_CHANNEL / Outputs->MaxChannels));
      break;

    // CCC_CR_xxx_REG (excluding PI settings)
    case SCompBypassCmd:
    case SCompBypassCtl:
    case SCompBypassClk:
    case CccPerBitDeskewCal:
    case DefDrvEnLow:
    case CmdTxEq:
    case CtlTxEq:
    case GsmIocCccPiEn:
    case GsmIocCccPiEnOverride:
    case GsmIocCccDccCcc:
    case GsmIocLdoFFCodeLockCcc:
    case GsmIocDccDcdSampleSelCcc:
    case GsmIocStep1PatternCcc:
    case GsmIocDccCodePh0IndexCcc:
    case GsmIocDccCodePh90IndexCcc:
    case GsmIocTxpbddOffsetCcc:
    case GsmIocTxpbdPh90incrCcc:
    case GsmIocMdllCmnVcdlDccCodeCcc:
    case GsmIocDccFsmResetCcc:
    case GsmIocDcdRoCalEnCcc:
    case GsmIocDcdRoCalStartCcc:
    case GsmIocDcdRoCalSampleCountRstCcc:
    case GsmIocDccStartCcc:
    case GsmIocSrzDcdEnCcc:
    case GsmIocMdllDcdEnCcc:
      // TransChannel is CCC index - passed in via Strobe
      TransChannel = *Strobe;
      break;

    // CCC_CR_DCCCALCCONTROL
    case GsmIocCccDccStepSize:
    case GsmIocCccDcc2xStep:
    // CCC_CR_DCCLANESTATUSx
    case GsmIocCccDccLaneStatusResult:
    // CCC_CR_DDRCRCCCPERBITDESKEWP[FALL/RISE]N[RISE/FALL]
    case GsmIocClkPFallNRiseMode:
    case GsmIocClkPRiseNFallMode:
    case GsmIocClkPFallNRiseFeedback:
    case GsmIocClkPRiseNFallFeedback:
    case GsmIocClkPFallNRiseCcc56:
    case GsmIocClkPRiseNFallCcc56:
    case GsmIocClkPFallNRiseCcc78:
    case GsmIocClkPRiseNFallCcc78:
    // CCC_CR_DDRCRCTLCACOMPOFFSET_REG
    case CmdSCompOffset:
    case CmdRCompDrvDownOffset:
    case CmdRCompDrvUpOffset:
    case CtlSCompOffset:
    case CtlRCompDrvDownOffset:
    case CtlRCompDrvUpOffset:
    // CCC_CR_DDRCRCCCPIxxx_REG
    case CtlGrpPi:
    case ClkGrpPi:
    case ClkGrpG4Pi:
    case CkeGrpPi:
    case CmdGrpPi:
    case WckGrpPi:
    case WckGrpPi90:
    // TransChannel is CCC index
      if (DdrType == MRC_DDR_TYPE_DDR4) {
        if (Group == CtlGrpPi) {
          // UY Ctrl0 - CH1CCC, Ctrl1 - CH5CCC
          // HS Ctrl0 - CH4CCC, Ctrl1 - CH6CCC
          //TransChannel += 1;
          if (UlxUlt) {
            TransChannel = ((*Controller == 0) ? 1 : 5);
          } else {
            TransChannel = ((*Controller == 0) ? 4 : 6);
          }
        } else if (Group == CkeGrpPi) {
          // UY Ctrl0 - CH2CCC, Ctrl1 - CH6CCC
          // HS Ctrl0 - CH2CCC, Ctrl1 - CH0CCC
          //TransChannel += 2;
          if (*Controller == 1) {
            TransChannel = ((UlxUlt) ? 6 : 0);
          } else {
            TransChannel = 2;
          }
        } else if ((Group == ClkGrpPi) ||
          (Group == ClkGrpG4Pi)        ||
          (Group == GsmIocClkPFallNRiseMode) ||
          (Group == GsmIocClkPRiseNFallMode) ||
          (Group == GsmIocClkPFallNRiseFeedback) ||
          (Group == GsmIocClkPRiseNFallFeedback) ||
          (Group == GsmIocClkPFallNRiseCcc56) ||
          (Group == GsmIocClkPRiseNFallCcc56) ||
          (Group == GsmIocClkPFallNRiseCcc78) ||
          (Group == GsmIocClkPRiseNFallCcc78) ||
          (Group == GsmIocCccDccLaneStatusResult) ||
          (Group == GsmIocCccDcc2xStep) ||
          (Group == GsmIocCccDccStepSize)) {
          // UY Ctrl0.R0 - CH0CCC Ctrl0.R1 - CH3CCC
          // UY Ctrl1.R0 - CH4CCC Ctrl1.R1 - CH7CCC
          // HS Ctrl0.R0 - CH3CCC Ctrl0.R1 - CH3CCC Ctrl0.R2 - CH5CCC Ctrl0.R3 - CH5CCC
          // HS Ctrl1.R0 - CH1CCC Ctrl1.R1 - CH1CCC Ctrl1.R2 - CH7CCC Ctrl1.R3 - CH7CCC
          //TransChannel += (*Rank * 3);
          if (UlxUlt) {
            TransChannel = ((*Rank == 0) ? 0 : 3);
            if (*Controller == 1) {
              TransChannel += 4;
            }
          } else {
            if (*Controller == 0) {
              TransChannel = ((*Rank < 2) ? 3 : 5);
            } else {
              // Controller == 1
              TransChannel = ((*Rank < 2) ? 1 : 7);
            }
          }
        } else {
          // CmdGrpPi CCC Mapping
          // Inner number is CCC instance
          // | Sku     |  UlxUlt |  DtHalo |
          // | PiGroup | 0 1 2 3 | 0 1 2 3 | <-- passed in as Strobe
          // |---------|---------|---------|
          // | Ctrl 0  | 0 1 2 3 | 2 3 4 5 |
          // | Ctrl 1  | 4 5 6 7 | 0 1 6 7 |
          // |-----------------------------|
          if (*Controller == 0) {
            TransChannel = (UlxUlt) ? 0 : 2;
          } else {
            // Controller == 1
            TransChannel = (UlxUlt) ? 4 : 0;
          }
          if (DtHalo && (*Controller == 1) && (*Strobe > 1)) {
            TransChannel += 4;
          }
          TransChannel += *Strobe;
        }
        break;
      } else if (DdrType == MRC_DDR_TYPE_DDR5) {
        if ((*Controller >= MAX_CONTROLLER) || (*Channel >= 2) || ((*Rank != MRC_IGNORE_ARG) && (*Rank >= MAX_RANK_IN_CHANNEL))) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Index out of range\n", gErrString);
          return mrcWrongInputParameter;
        }
        if ((Group == ClkGrpPi) || (Group == ClkGrpG4Pi)) {
          if (UlxUlt) {
            TransChannel = (*Controller * MAX_CHANNEL);
            TransChannel += (*Channel * (MAX_CHANNEL / Outputs->MaxChannels)) + *Rank;
          } else {
            TransChannel = ClkPiCccL2pDdr5[*Controller][*Channel][*Rank];
          }
        } else if (Group == CtlGrpPi) {
          if (UlxUlt) {
            TransChannel = (*Controller == 0) ? 0 : 4;
            TransChannel +=  (*Channel == 1) ? 3 : 0;
          } else {
            TransChannel = CtlPiCccL2pDdr5[*Controller][*Channel][*Rank];
          }
        } else {
          // CmdGrpPi
          if (UlxUlt) {
            TransChannel = (*Controller * MAX_CHANNEL);
            TransChannel += (*Channel * (MAX_CHANNEL / Outputs->MaxChannels)) + *Strobe;
          } else {
            if (*Strobe >= MRC_DDR5_CMD_GRP_MAX) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Index out of range\n", gErrString);
              return mrcWrongInputParameter;
            }
            TransChannel = CmdPiCccL2pDdr5[*Controller][*Channel][*Strobe];
          }
        }
      } else {
        // LPDDR4 / LPDDR5
        TransChannel = (*Controller * MAX_CHANNEL);
        TransChannel += (*Channel * (MAX_CHANNEL / Outputs->MaxChannels));
      }
      break;

    // DDRDATAxCHy_REG
    case GsmIocDqsMaskPulseCnt:
    case GsmIocDqsMaskValue:
    case GsmIocDqsPulseCnt:
    case GsmIocDqOverrideData:
    case GsmIocDqOverrideEn:
    case GsmIocForceOnRcvEn:
    case GsmIocReadDqDqsMode:
    case GsmIocSenseAmpMode:
    case GsmIocDqsRFMode:
    case GsmIocDdrDqRxSdlBypassEn:
    case GsmIocCaTrainingMode:
    case GsmIocCaParityTrain:
    case GsmIocDataTrainFeedback:
    case GsmIocDdrDqDrvEnOvrdData:
    case GsmIocDdrDqDrvEnOvrdModeEn:
    case GsmIocRxAmpOffsetEn:
    case GsmIocFFCodePiOffset:
    case GsmIocFFCodeIdleOffset:
    case GsmIocFFCodeWeakOffset:
    case GsmIocFFCodeWriteOffset:
    case GsmIocFFCodeReadOffset:
    case GsmIocFFCodePBDOffset:
    case GsmIocFFCodeCCCDistOffset:
    case GsmIocDataInvertNibble:
    case GsmIocWriteLevelMode:
    case GsmIocReadLevelMode:
    case GsmIocForceRxAmpOn:
    case GsmIocForceOdtOn:
    case GsmIocForceOdtOnWithoutDrvEn:
    case GsmIocDataOdtStaticEn:
    case GsmIocRxPiPwrDnDis:
    case GsmIocTxPiPwrDnDis:
    case GsmIocTxDisable:
    case GsmIocInternalClocksOn:
    case GsmIocDataDqsNParkLow:
    case GsmIocTxOn:
    case GsmIocRxDisable:
    case GsmIocWlLongDelEn:
    case GsmIocBiasPMCtrl:
    case GsmIocLocalGateD0tx:
    case GsmIocDataOdtMode:
    case GsmIocDataDqsOdtParkMode:
    case GsmIocDataDqOdtParkMode:
    case GsmIocCompVddqOdtEn:
    case GsmIocSdllSegmentDisable:
// @todo_adl    case GsmIocRXDeskewForceOn:
    case GsmIocRankOverrideEn:
    case GsmIocRankOverrideVal:
    case GsmIocDataCtlGear1:
    case GsmIocDataCtlGear4:
    case GsmIocRxSALTailCtrl:
    case GsmIocDataWrPreamble:
    case GsmIocLdoFFCodeLockData:
    case GsmIocDccFsmResetData:
    case GsmIocDcdRoCalEnData:
    case GsmIocDcdRoCalStartData:
    case GsmIocDcdRoCalSampleCountRstData:
    case GsmIocDccStartData:
    case GsmIocSrzDcdEnData:
    case GsmIocMdllDcdEnData:
    case GsmIocDcdSampleCountRstData:
    case GsmIocDcdSampleCountStartData:
    case GsmIocDcdRoLfsrData:
    case GsmIocDqSlewDlyByPass:
    case GsmIocRxVrefMFC:
    case GsmIocTxEqEn:
    case GsmIocTxEqTapSelect:
    case GsmIocConstZTxEqEn:
    case GsmIocRxClkStg:
    case GsmIocDataRxBurstLen:
    case GsmIocEnDqsNRcvEn:
    case GsmIocEnDqsNRcvEnGate:
    case GsmIocRxMatchedPathEn:
    case GsmIocRxVocMode:
    case GsmIocStrobeOdtStaticEn:
    case RxDqsBitDelay:
    case RxDqsBitOffset:
    case RxDqsNBitDelay:
    case RxDqsPBitDelay:
    case TxDqBitDelay:
    case TxDqBitDelay90:
    case RxUnmatchedOffNcal:
    case RxMatchedUnmatchedOffPcal:
    case TxDqDccOffset:
    case TxDqsDccOffset:
    case TxDqsBitDelay:
    case RxVoc:
    case RxVocUnmatched:
    case RxDqsAmpOffset:
    case RxDqsUnmatchedAmpOffset:
    case RxVref:
    case CmdVref:
    case GsmIocCmdVrefConverge:
    case RecEnDelay:
    case RxDqsPDelay:
    case RxDqsNDelay:
    case CompRcompOdtUpPerStrobe :
    case CompRcompOdtDnPerStrobe:
    case RxEq:
    case RxDqsEq:
    case RxPerBitDeskewCal:
    case TxPerBitDeskewCal:
    case SenseAmpDelay:
    case SenseAmpDuration:
    case McOdtDelay:
    case McOdtDuration:
    case DqsOdtDelay:
    case DqsOdtDuration:
    case RxRankMuxDelay:
    case TxRankMuxDelay:
    case TxDqsRankMuxDelay:
    case TxEq:
    case TxEqCoeff0:
    case TxEqCoeff1:
    case TxEqCoeff2:
    case TxDqsDelay:
    case TxDqsDelay90:
    case TxDqDelay:
    case TxDqDelay90:
    case SCompBypassDq:
    case RecEnOffset:
    case RxDqsOffset:
    case RxVrefOffset:
    case TxDqsOffset:
    case TxDqOffset:
    case RxDqsPiUiOffset:
    case CompOffsetAll:
    case TxTco:
    case TxDqsTcoP:
    case TxDqsTcoN:
    case RxTap1:
    case RxTap2:
    case RxTap3:
    case RxTap4:
    case RxTap1En:
    case RxTap2En:
    case RxTap3En:
    case RxC:
    case RxR:
    case RxVrefVttDecap:
    case RxVrefVddqDecap:
    case ForceDfeDisable:
    case DqsOdtCompOffset:
    case DataRxD0PiCb:
    case DataSDllPiCb:
    case RxBiasCtl:
    case RxLpddrMode:
    case RxBiasVrefSel:
    case RxBiasTailCtl:
    case CompOffsetVssHiFF:
    case GsmIocDataDccLaneStatusResult:
    case GsmIocDataDccSaveFullDcc:
    case GsmIocDataDccSkipCRWrite:
    case GsmIocDataDccMeasPoint:
    case GsmIocDataDccRankEn:
    case GsmIocDataDccLaneEn:
    case GsmIocDataDccStepSize:
    case GsmIocDataDcc2xStep:
    case GsmIocRetrainSwizzleCtlByteSel:
    case GsmIocRetrainSwizzleCtlBit:
    case GsmIocRetrainSwizzleCtlRetrainEn:
    case GsmIocRetrainSwizzleCtlSerialMrr:
    case GsmIocRetrainInitPiCode:
    case GsmIocRetrainDeltaPiCode:
    case GsmIocRetrainRoCount:
    case GsmIocRetrainCtlInitTrain:
    case GsmIocRetrainCtlDuration:
    case GsmIocRetrainCtlResetStatus:
    case GsmIocDccDcdSampleSelData:
    case GsmIocStep1PatternData:
    case GsmIocDccCodePh0IndexData:
    case GsmIocDccCodePh90IndexData:
    case GsmIocTxpbddOffsetData:
    case GsmIocTxpbdPh90incrData:
    case GsmIocMdllCmnVcdlDccCodeData:
      //ByteCount will vary based on DdrType
      TransChannel = *Controller;
      if (Outputs->SdramCount == MAX_BYTE_IN_DDR5_CHANNEL) { // DDR5 with ECC
        if (*Strobe == 4) { // ECC Bytes goes to Byte8 and Byte9
          TransStrobe = *Channel + 8;
        } else {
          TransStrobe = (*Channel * (Outputs->SdramCount - 1));
          TransStrobe += *Strobe;
        }
      } else {
        TransStrobe = (*Channel * Outputs->SdramCount);
        TransStrobe += *Strobe;
      }
      break;

    case GsmMccRrdValidTrigger:
    case GsmMccRrdValidOverflow:
    case GsmMccRrdValidValue:
    case GsmMccRrdValidSign:
    case GsmMccDeswizzleByte:
    case GsmMccDeswizzleBit:
      // These belong to MC group, but we need to keep system-level channel for subch indexing (like RoundTripDelay)
      break;

    default:
      if ((Group > EndOfIocMarker) && (Group < EndOfMccMarker)) {
        // This is to cover all MCT and MCC enums
        switch (DdrType) {
          case MRC_DDR_TYPE_LPDDR4:
          case MRC_DDR_TYPE_LPDDR5:
            if (*Channel != MRC_IGNORE_ARG) {
              // Always force writes to this register to the MC channel number for LP.
              TransChannel = *Channel / 2;
              if (*Channel % 2) {
                Status = mrcInstanceDoesNotExist;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "GetSet called with Ch of 1/3 when LPDDR, Group %s(%d), Ch%d\n", GsmGtDebugStrings[Group], Group, *Channel);
              }
            }
            break;

          default:
            // DDR4/DDR5 will use System Channel Number.
            break;
        }
      } else {
        // No translation was done, so it must be a global register
        // Make sure we didn't miss a per-byte register translation
        if (*Strobe != MRC_IGNORE_ARG) {
          MRC_DEBUG_ASSERT (FALSE, Debug, "%s Possible TranslateSystemToIp missing for Group %s(%d), MC%u C%u R%u S%u\n", gErrString, GsmGtDebugStrings[Group], Group, *Controller, *Channel, *Rank, *Strobe);
        }
      }
      break;
  }

  *Controller = TransController;
  *Channel    = TransChannel;
  *Rank       = TransRank;
  *Strobe     = TransStrobe;

  return Status;
}

/**
  Top level function that is System Level Specificity.
  The flow is as follows:
    Check Multicast/Unicast, translate from system to IP register architecture, side effect and update other PHY logic.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]      Controller  - Memory Controller Number within the processor (0-Based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      Dimm        - DIMM Number within the DDR Channel (0-based). Ignored as Rank is rank number in the channel.
  @param[in]      Rank        - Rank number within a channel (0-based).
  @param[in]      Strobe      - Dqs data group within the rank (0-based).
  @param[in]      Lane        - Lane index within the GSM_GT group (0-based).
  @param[in]      FreqIndex   - Index supporting multiple operating frequencies.
  @param[in]      Level       - DDRIO level to access.
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSet (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Socket,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Dimm,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      UINT32        const   Lane,
  IN      UINT32        const   FreqIndex,
  IN      GSM_LT        const   Level,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
)
{
  MrcStatus         Status;
  MrcStatus         CurrentStatus;
  MrcOutput         *Outputs;
  UINT32            SocketLoop;
  UINT32            SocketStart;
  UINT32            SocketEnd;
  UINT32            ControllerLoop;
  UINT32            ControllerStart;
  UINT32            ControllerEnd;
  UINT32            ChannelLoop;
  UINT32            ChannelStart;
  UINT32            ChannelEnd;
  UINT32            RankLoop;
  UINT32            RankStart;
  UINT32            RankEnd;
  UINT32            StrobeLoop;
  UINT32            StrobeStart;
  UINT32            StrobeEnd;
  UINT32            LaneLoop;
  UINT32            LaneStart;
  UINT32            LaneEnd;
  UINT32            LaneMax;
  BOOLEAN           ReadOnly;
  BOOLEAN           Lpddr;
  BOOLEAN           McGroup;
  BOOLEAN           MulticastAccess;
  BOOLEAN           ValidMc;
  BOOLEAN           ValidCh;
  BOOLEAN           ValidRank;
  BOOLEAN           ValidStrobe;
  UINT32            TransController;
  UINT32            TransChannel;
  UINT32            TransRank;
  UINT32            TransStrobe;
#ifdef MRC_DEBUG_PRINT
  MrcDebug      *Debug;
  UINT64        *Ptr;

  Debug = &MrcData->Outputs.Debug;

  // Check that the level is supported
  switch (Level) {
    case DdrLevel:
      break;

    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "GetSet, Level %d is not supported!\n", Level);
      return mrcWrongInputParameter;
      break;
  }

  // Check that GsmGtDebugStrings[] array is consistent with the GSM_GT enums
  Ptr = (UINT64 *) GsmGtDebugStrings[GsmGtMax];
  if ((Ptr == NULL) || (*Ptr != 0x78614d74476d7347)) { // "GsmGtMax"
    MRC_DEBUG_ASSERT (FALSE, Debug, "%s GsmGtDebugStrings[] array is not aligned with GSM_GT enums", gErrString);
  }

  // Check that the Group is supported
  Status = MrcCheckGroupSupported (MrcData, Group);
  if (Status != mrcSuccess) {
    return Status;
  }
#endif // MRC_DEBUG_PRINT

  Outputs = &MrcData->Outputs;
  Lpddr = Outputs->Lpddr;

  ReadOnly = (Mode & GSM_READ_ONLY) == GSM_READ_ONLY;
  Status = mrcSuccess;

  MrcGetSetParamMaxAdjust (MrcData, Group, &LaneMax);

  // Detect and convert all Multicast accesses into unicast.
  MulticastAccess = FALSE;
  if ((Socket != MRC_IGNORE_ARG) && (Socket >= MAX_CPU_SOCKETS)) {
    MulticastAccess = TRUE;
    SocketStart = 0;
    SocketEnd = MAX_CPU_SOCKETS - 1;
  } else {
    SocketStart = Socket;
    SocketEnd = Socket;
  }
  if ((Controller != MRC_IGNORE_ARG) && (Controller >= MAX_CONTROLLER)) {
    MulticastAccess = TRUE;
    ControllerStart = 0;
    ControllerEnd = MAX_CONTROLLER - 1;
  } else {
    ControllerStart = Controller;
    ControllerEnd = Controller;
  }
  if ((Channel != MRC_IGNORE_ARG) && (Channel >= (Outputs->MaxChannels))) {
    MulticastAccess = TRUE;
    ChannelStart = 0;
    ChannelEnd = (UINT32) (Outputs->MaxChannels) - 1;
  } else {
    ChannelStart = Channel;
    ChannelEnd = Channel;
  }
  if ((Rank != MRC_IGNORE_ARG) && (Rank >= MAX_RANK_IN_CHANNEL)) {
    MulticastAccess = TRUE;
    RankStart = 0;
    RankEnd = MAX_RANK_IN_CHANNEL - 1;
  } else {
    RankStart = Rank;
    RankEnd = Rank;
  }
  if ((Strobe != MRC_IGNORE_ARG) && (Strobe >= MAX_SDRAM_IN_DIMM)) {
    MulticastAccess = TRUE;
    StrobeStart = 0;
    StrobeEnd = MrcData->Outputs.SdramCount - 1;
  } else {
    StrobeStart = Strobe;
    StrobeEnd = Strobe;
  }
  if ((Lane != MRC_IGNORE_ARG) && (Lane >= LaneMax)) {
    MulticastAccess = TRUE;
    LaneStart = 0;
    LaneEnd = LaneMax - 1;
  } else {
    LaneStart = Lane;
    LaneEnd = Lane;
  }

  ValidMc     = (Controller != MRC_IGNORE_ARG);
  ValidCh     = (Channel != MRC_IGNORE_ARG);
  ValidRank   = (Rank != MRC_IGNORE_ARG);
  ValidStrobe = (Strobe != MRC_IGNORE_ARG);

  // Check if we are a MC Group.  If so, then we want to skip writes to channel 1/3
  if ((Group > EndOfIocMarker) && (Group < EndOfMccMarker)) {
    McGroup = TRUE;
  } else {
    McGroup = FALSE;
  }

  switch (Group) {
    case GsmMccRrdValidTrigger:
    case GsmMccRrdValidOverflow:
    case GsmMccRrdValidValue:
    case GsmMccRrdValidSign:
      // We are using system-level channel for these items, because the register fields are per subch in LP4/5 (like RoundTripDelay)
      // Hence do not skip channels 1 and 3
      McGroup = FALSE;
      break;
    default:
      break;
  }

  // Break Multicast requests into multiple unicasts if it is not GSM_READ_ONLY.
  if (MulticastAccess) {
    if (ReadOnly) {
      // Ensure no one is trying to read with multicast parameters.
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Attempted to read from a Multicast. Group: %s(%d) Controller: %u, Channel: %u, Rank: %u, Byte: %u\n",
        GsmGtDebugStrings[Group], Group, Controller, Channel, Rank, Strobe);
      return mrcWrongInputParameter;
    }
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "SocketStart: %d\tSocketEnd: %d\n", SocketStart, SocketEnd);
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "ControllerStart: %d\tControllerEnd: %d\n", ControllerStart, ControllerEnd);
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "ChannelStart: %d\tChannelEnd: %d\n", ChannelStart, ChannelEnd);
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "RankStart: %d\tRankEnd: %d\n", RankStart, RankEnd);
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "StrobeStart: %d\tStrobeEnd: %d\n", StrobeStart, StrobeEnd);
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "LaneStart: %d\tLaneEnd: %d\n", LaneStart, LaneEnd);
    for (SocketLoop = SocketStart; SocketLoop <= SocketEnd; SocketLoop++) {
      for (ControllerLoop = ControllerStart; ControllerLoop <= ControllerEnd; ControllerLoop++) {
        if (ValidMc) {
          if (!MrcControllerExist (MrcData, ControllerLoop)) {
            MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "Group %s skipped as %s %d does not exist\n", GsmGtDebugStrings[Group], gControllerStr, ControllerLoop);
            continue;
          }
        }
        for (ChannelLoop = ChannelStart; ChannelLoop <= ChannelEnd; ChannelLoop++) {
          if (ValidMc && ValidCh) {
            if (!MrcChannelExist (MrcData, ControllerLoop, ChannelLoop)) {
              MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "Group %s skipped as %s %d does not exist\n", GsmGtDebugStrings[Group], gChannelStr, ChannelLoop);
              continue;
            } else if (McGroup && (IS_MC_SUB_CH (Lpddr, ChannelLoop))) {
              // Skip calling MrcGetSet for CH 1/3 if we are LPDDR since there isn't any register associated to it.
              continue;
            }
          }
          for (RankLoop = RankStart; RankLoop <= RankEnd; RankLoop++) {
            if (ValidMc && ValidCh && ValidRank) {
              if (!MrcRankExist (MrcData, ControllerLoop, ChannelLoop, RankLoop)) {
                MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "Group %s skipped as %s %d does not exist\n", GsmGtDebugStrings[Group], gRankStr, RankLoop);
                continue;
              }
            }
            for (StrobeLoop = StrobeStart; StrobeLoop <= StrobeEnd; StrobeLoop++) {
              if (ValidMc && ValidCh && ValidRank && ValidStrobe) {
                if (!MrcByteExist (MrcData, ControllerLoop, ChannelLoop, StrobeLoop)) {
                  MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "Group %s skipped as %s %d does not exist\n", GsmGtDebugStrings[Group], gByteStr, StrobeLoop);
                  continue;
                }
              }
              for (LaneLoop = LaneStart; LaneLoop <= LaneEnd; LaneLoop++) {
                CurrentStatus = MrcGetSet (
                                  MrcData,
                                  SocketLoop,
                                  ControllerLoop,
                                  ChannelLoop,
                                  Dimm,
                                  RankLoop,
                                  StrobeLoop,
                                  LaneLoop,
                                  FreqIndex,
                                  Level,
                                  Group,
                                  Mode,
                                  Value
                                  );
                if (Status == mrcSuccess) {
                  Status = CurrentStatus;
                }
              } // BitLoop
            } // StrobeLoop
          } // RankLoop
        } // ChannelLoop
      } // ControllerLoop
    } // SocketLoop
    // We stop here when breaking up multicasts.
    return Status;
  }

  if (MrcCheckComplexOrSideEffect (MrcData, Group, MRC_IS_COMPLEX)) {
    // Complex parameters will stop here.
    Status = MrcGetSetComplexParam (MrcData, Socket, Controller, Channel, 0, Rank, Strobe, Lane, FreqIndex, Group, Mode, Value);
    if (Status == mrcComplexParamDone) {
      return mrcSuccess;
    }
  }

  TransController = Controller;
  TransChannel = Channel;
  TransRank = Rank;
  TransStrobe = Strobe;
  Status = MrcTranslateSystemToIp (MrcData, &TransController, &TransChannel, &TransRank, &TransStrobe, Group);

  if (Status == mrcSuccess) {
    Status = GetSet (MrcData, Socket, TransController, TransChannel, Dimm, TransRank, TransStrobe, Lane, FreqIndex, Level, Group, Mode, Value);

    if (!ReadOnly) {
      // Update other Phy Channels
      MrcGetSetUpdatePhyChannels (MrcData, Socket, Controller, Channel, Dimm, Rank, Strobe, Lane, FreqIndex, Level, Group, Mode, Value);

      // Update MRC Host
      if ((Mode & GSM_UPDATE_HOST) == GSM_UPDATE_HOST) {
        MrcGetSetUpdateHost (MrcData, Controller, Channel, Rank, Strobe, Lane, Group, Value);
      }

      // Check if the field written to has some side effect and resolve it.
      if (MrcCheckComplexOrSideEffect (MrcData, Group, MRC_IS_SIDE_EFFECT)) {
        MrcGetSetSideEffect (MrcData, Socket, Controller, Channel, 0, Dimm, Rank, Strobe, Lane, FreqIndex, Level, Group, Mode, Value);
      }
    }
  }

  return Status;
}

/**
Top level function used to interact with SOC.
The flow is as follows:
  Get the GSM_GT bit shift instruction, CR offset.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      Dimm        - DIMM Number within the DDR Channel (0-based). Ignored as Rank is rank number in the channel.
  @param[in]      Rank        - Rank number within a channel (0-based).
  @param[in]      Strobe      - If Group is a CMD/CTL/CLK Index type, this is the index for that signal.  Otherwise, Dqs data group within the rank (0-based).
  @param[in]      Bit         - Bit index within the data group (0-based).
  @param[in]      FreqIndex   - Index supporting multiple operating frequencies.
  @param[in]      Level       - DDRIO level to access.
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
GetSet (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Socket,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Dimm,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      UINT32        const   Bit,
  IN      UINT32        const   FreqIndex,
  IN      GSM_LT        const   Level,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
)
{
  MrcStatus         Status;
  MRC_RX_MODE_TYPE  RxMode;
  INT64             CurrentVal;
  INT64             UpdateVal;
  INT64             Min;
  INT64             Max;
  UINT64            RegVal;
  UINT64            NewRegVal;
  UINT32            Offset;
  UINT32            Delay;
  BOOLEAN           RegSize;
  BOOLEAN           ReadOnly;
  BOOLEAN           Unmatched;
  BOOLEAN           IsRxVocUnmatched;
  BOOLEAN           IsRxSalVocUnmatched;
  BOOLEAN           RxVocDqs;
  MRC_REGISTER_HASH_STRUCT  HashVal;
#ifdef MRC_DEBUG_PRINT
  MrcDebug      *Debug;
  INT64_STRUCT  PrintVal;
  CHAR8         Str[120];
  CHAR8         *p;

  Debug = &MrcData->Outputs.Debug;
#ifdef MRC_DISABLE_CACHING
  Mode &= ~GSM_CACHE_ONLY;
  Mode |= GSM_READ_CSR;
#endif
#endif // MRC_DEBUG_PRINT

  RxMode = MrcData->Outputs.RxMode;
  Unmatched = (RxMode == MrcRxModeUnmatchedP) || (RxMode == MrcRxModeUnmatchedN);
  RxVocDqs  = (Group == RxDqsUnmatchedAmpOffset);
  IsRxVocUnmatched = Unmatched && ((Group == RxVoc) || (Group == RxVocUnmatched) || RxVocDqs);
  IsRxSalVocUnmatched = Unmatched && ((Group == RxMatchedUnmatchedOffPcal) || (Group == RxUnmatchedOffNcal));
  ReadOnly = (Mode & GSM_READ_ONLY) == GSM_READ_ONLY;
  Status = mrcSuccess;

  // Get CR offset and field hash value
  if (Group < EndOfPhyMarker) {
    Status = MrcGetDdrIoHash (MrcData, Group, Socket, Controller, Channel, Rank, Strobe, Bit, FreqIndex, &HashVal.Data);
  } else if (Group < EndOfIocMarker) {
    Status = MrcGetDdrIoCfgHash (MrcData, Group, Socket, Channel, Rank, Strobe, Bit, FreqIndex, &HashVal.Data);
  } else if (Group < EndOfMctMarker) {
    Status = MrcGetMcTimingHash (MrcData, Group, Socket, Controller, Channel, FreqIndex, &HashVal.Data);
  } else if (Group < EndOfMccMarker) {
    Status = MrcGetMcConfigHash (MrcData, Group, Socket, Controller, Channel, Rank, Strobe, Bit, FreqIndex, &HashVal.Data);
  } else {
    Status = MrcGetCmiHash (MrcData, Controller, Group, Socket, Channel, FreqIndex, &HashVal.Data);
  }
  if (Status != mrcSuccess) {
    return Status;
  }

  Offset = HashVal.Bits.Offset;
  RegSize = (HashVal.Bits.RegSize != 0);
#ifdef MRC_DEBUG_PRINT
  if (Offset > MRC_REGISTER_HASH_Offset_MAX) {
    p = Str;
    p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), "No Offset found for Group %s(%d)", GsmGtDebugStrings[Group], Group) - 1;
    if (Socket != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), " Socket %u,", Socket) - 1;
    }
    if (Controller != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), " Controller %u,", Controller) - 1;
    }
    if (Channel != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), " Channel %u,", Channel) - 1;
    }
    if (Rank != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), " Rank %u,", Rank) - 1;
    }
    if (Strobe != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), " Strobe %u,", Strobe) - 1;
    }
    if (Bit != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), " Bit %u,", Bit) - 1;
    }
    if (FreqIndex != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), " FreqIndex %u", FreqIndex) - 1;
    }
    MRC_DEBUG_ASSERT (Offset < MRC_REGISTER_HASH_Offset_MAX, Debug, "%s\n", Str);
  }
#endif // MRC_DEBUG_PRINT

  // Init data.
  RegVal = MrcCrCacheRead (MrcData, Offset, RegSize, Mode);

  // Get the group value
  CurrentVal = MrcHalGsmGetBitField (MrcData, HashVal, RegVal);

  // Workaround for SenseAmpOffset being non-monotonic in its sweep
  // MRC is passing its offset encoding, so we translate from CR Encoding here.
  if (IsRxVocUnmatched) {
    MrcTranslateSotOffset (CurrentVal, &CurrentVal, TRUE, RxVocDqs);
  } else if (IsRxSalVocUnmatched) {
    PhysicalToLinear(MrcData, Group, (INT64*) &CurrentVal);
  }

  if (ReadOnly) {
    *Value = CurrentVal;
    // UpdatedVal is used for printing below due to W/A around SOT parameter
    UpdateVal = CurrentVal;
  } else {
    CurrentVal = ((Mode & GSM_WRITE_OFFSET) == GSM_WRITE_OFFSET) ? CurrentVal + *Value : *Value;

    // Check value limits.
    MrcGetSetLimits (MrcData, Group, &Min, &Max, &Delay);

    if ((CurrentVal < Min) || (CurrentVal > Max)) {
#ifdef MRC_DEBUG_PRINT
      p = Str;
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), "%s", gWarnString) - 1;
      if (Controller != MRC_IGNORE_ARG) {
        p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), "Mc%u", Controller) - 1;
      }
      if (Channel != MRC_IGNORE_ARG) {
        p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), ".C%u", Channel) - 1;
      }
      if (Rank != MRC_IGNORE_ARG) {
        p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), ".R%u", Rank) - 1;
      }
      if (Strobe != MRC_IGNORE_ARG) {
        p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), ".S%u", Strobe) - 1;
      }
      if (Bit != MRC_IGNORE_ARG) {
        p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), ".B%u,", Bit) - 1;
      }
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), " %s value %lld is out of range [%lld:%lld]. Clamping\n",
                       GsmGtDebugStrings[Group], CurrentVal, Min, Max) - 1;
      if ((Mode & GSM_NO_RANGE_WARN) == 0) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\n", Str);
      }
#endif // MRC_DEBUG_PRINT
      Status = mrcParamSaturation;
    }
#ifdef MRC_DEBUG_PRINT
    if (Delay != 0) {
      MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "Delay for %uus\n", Delay);
    }
#endif
    CurrentVal = RANGE (CurrentVal, Min, Max);

    // Workaround for SenseAmpOffset being non-monotonic in its sweep
    // When we write back to the CR, we need to translate back to its encoding.
    // NewRegVal is in MRC encoding at this point.
    if (IsRxVocUnmatched) {
      MrcTranslateSotOffset (CurrentVal, &UpdateVal, FALSE, RxVocDqs);
    }  else if (IsRxSalVocUnmatched) {
      UpdateVal = CurrentVal;
      LinearToPhysical(MrcData, Group, (INT64*) &UpdateVal);
    } else {
      UpdateVal = CurrentVal;
    }

    // Update CR with new value
    NewRegVal = MrcHalGsmSetBitField (MrcData, HashVal, UpdateVal, RegVal);

    if (((Mode & GSM_FORCE_WRITE) == GSM_FORCE_WRITE) || (RegVal != NewRegVal)) {
      MrcCrCacheWrite (MrcData, Offset, RegSize, Mode, Delay, NewRegVal);
    }
  }

#ifdef MRC_DEBUG_PRINT
  if (Mode & GSM_PRINT_VAL) {
    p = Str;
    PrintVal.Data = UpdateVal;
    *p++ = (ReadOnly) ? 'R' : 'W';
    *p++ = ' ';
    if (Controller != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), "Mc%u", Controller) - 1;
    }
    if (Channel != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), ".C%u", Channel) - 1;
    }
    if (Rank != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), ".R%u", Rank) - 1;
    }
    if (Strobe != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), ".S%u", Strobe) - 1;
    }
    if (Bit != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), ".B%u", Bit) - 1;
    }
    p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), " %s: ", GsmGtDebugStrings[Group]) - 1;
    if (HashVal.Bits.BfWidth > 32) {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), "0x%llx", CurrentVal) - 1;
    } else {
      p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), "%d", PrintVal.Data32.Low) - 1;
      if ((HashVal.Bits.BfSign == 0) && (PrintVal.Data32.Low > 9)) {
        p += MrcSPrintf (MrcData, p, sizeof (Str)-(p - Str), " (0x%x)", PrintVal.Data32.Low) - 1;
      }
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\n", Str);
  }
#endif // MRC_DEBUG_PRINT

  return Status;
}

/**
  Top level function used to interact with DDRIO parameters.
  The flow is as follows:
    Get the GSM_GT bit shift instruction, CR offset, and Multicasting offsets.

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      SubChannel  - DDR SubChannel number within a Channel (0-Based).
  @param[in]      Dimm        - DIMM Number within the DDR Channel (0-based). Ignored as Rank is rank number in the channel.
  @param[in]      Rank        - Rank number within a channel (0-based).
  @param[in]      Strobe      - If Group is a CMD/CTL/CLK Index type, this is the index for that signal.  Otherwise, Dqs data group within the rank (0-based).
  @param[in]      Bit         - Bit index within the data group (0-based).
  @param[in]      FreqIndex   - Index supporting multiple operating frequencies.
  @param[in]      Level       - DDRIO level to access.
  @param[in]      Group       - DDRIO group to access.
  @param[in]      Mode        - Bit-field flags controlling Get/Set.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

  @retval MrcStatus
**/
MrcStatus
MrcGetSetDdrIoGroup (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Socket,
  IN      UINT32        const   Channel,
  IN      UINT32        const   SubChannel,
  IN      UINT32        const   Dimm,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      UINT32        const   Bit,
  IN      UINT32        const   FreqIndex,
  IN      GSM_LT        const   Level,
  IN      GSM_GT        const   Group,
  IN      UINT32                Mode,
  IN OUT  INT64         *const  Value
  )
{
  MrcStatus         Status;
  MrcStatus         CurrentStatus;
  INT64             CurrentVal;
  INT64             Min;
  INT64             Max;
  UINT64            RegVal;
  UINT64            NewRegVal;
  UINT32            Offset;
  UINT32            Delay;
  UINT32            SocketLoop;
  UINT32            SocketStart;
  UINT32            SocketEnd;
  UINT32            ChannelLoop;
  UINT32            ChannelStart;
  UINT32            ChannelEnd;
  UINT32            SubChLoop;
  UINT32            SubChStart;
  UINT32            SubChEnd;
  UINT32            RankLoop;
  UINT32            RankStart;
  UINT32            RankEnd;
  UINT32            StrobeLoop;
  UINT32            StrobeStart;
  UINT32            StrobeEnd;
  UINT32            BitLoop;
  UINT32            BitStart;
  UINT32            BitEnd;
  BOOLEAN           RegSize;
  BOOLEAN           ReadOnly;
  BOOLEAN           MulticastAccess;
  MRC_REGISTER_HASH_STRUCT  HashVal;
#ifdef MRC_DEBUG_PRINT
  MrcDebug      *Debug;
  INT64_STRUCT  PrintVal;
  CHAR8         Str[120];
  CHAR8         *p;
  UINT64        *Ptr;

#ifdef MRC_DISABLE_CACHING
  Mode &= ~GSM_CACHE_ONLY;
  Mode |= GSM_READ_CSR;
#endif
  Debug      = &MrcData->Outputs.Debug;

  // Check that the level is supported
  switch (Level) {
    case DdrLevel:
      break;

    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "GetSet, Level %d is not supported!\n", Level);
      return mrcWrongInputParameter;
      break;
  }

  // Check that GsmGtDebugStrings[] array is consistent with the GSM_GT enums
  Ptr = (UINT64 *) GsmGtDebugStrings[GsmGtMax];
  if ((Ptr == NULL) || (*Ptr != 0x78614d74476d7347)) { // "GsmGtMax"
    MRC_DEBUG_ASSERT (FALSE, Debug, "%s GsmGtDebugStrings[] array is not aligned with GSM_GT enums", gErrString);
  }

  // Check that the Group is supported
  Status = MrcCheckGroupSupported (MrcData, Group);
  if (Status != mrcSuccess) {
    return Status;
  }
#endif // MRC_DEBUG_PRINT

  ReadOnly  = (Mode & GSM_READ_ONLY) == GSM_READ_ONLY;
  Status = mrcSuccess;

  // Detect and convert all Multicast accesses into unicast.
  MulticastAccess = FALSE;
  if ((Socket != MRC_IGNORE_ARG) && (Socket >= MAX_CPU_SOCKETS)) {
    MulticastAccess = TRUE;
    SocketStart = 0;
    SocketEnd = MAX_CPU_SOCKETS - 1;
  } else {
    SocketStart = Socket;
    SocketEnd   = Socket;
  }
  if ((Channel != MRC_IGNORE_ARG) && (Channel >= MAX_CHANNEL)) {
    MulticastAccess = TRUE;
    ChannelStart  = 0;
    ChannelEnd    = MAX_CHANNEL - 1;
  } else {
    ChannelStart  = Channel;
    ChannelEnd    = Channel;
  }
  if ((SubChannel != MRC_IGNORE_ARG) && (SubChannel >= MAX_SUB_CHANNEL)) {
    MulticastAccess = TRUE;
    SubChStart  = 0;
    SubChEnd    = MrcData->Outputs.SubChCount - 1;
  } else {
    SubChStart  = SubChannel;
    SubChEnd    = SubChannel;
  }
  if ((Rank != MRC_IGNORE_ARG) && (Rank >= MAX_RANK_IN_CHANNEL)) {
    MulticastAccess = TRUE;
    RankStart = 0;
    RankEnd   = MAX_RANK_IN_CHANNEL - 1;
  } else {
    RankStart = Rank;
    RankEnd   = Rank;
  }
  if ((Strobe != MRC_IGNORE_ARG) && (Strobe >= MAX_SDRAM_IN_DIMM)) {
    MulticastAccess = TRUE;
    StrobeStart = 0;
    StrobeEnd    = MrcData->Outputs.SdramCount - 1;
  } else {
    StrobeStart = Strobe;
    StrobeEnd   = Strobe;
  }
  if ((Bit != MRC_IGNORE_ARG) && (Bit >= MAX_BITS)) {
    MulticastAccess = TRUE;
    BitStart  = 0;
    BitEnd    = MAX_BITS - 1;
  } else {
    BitStart  = Bit;
    BitEnd    = Bit;
  }

  // Break Multicast requests into multiple unicasts if it is not GSM_READ_ONLY.
  if (MulticastAccess) {
    if (ReadOnly) {
      // Ensure no one is trying to read with multicast parameters.
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Attempted to read from a Multicast. Group: %s(%d) Channel: %u, SubCh: %u, Rank: %u, Byte: %u\n",
        GsmGtDebugStrings[Group], Group, Channel, SubChannel, Rank, Strobe);
      return mrcWrongInputParameter;
    }
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "SocketStart: %d\tSocketEnd: %d\n", SocketStart, SocketEnd);
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "ChannelStart: %d\tChannelEnd: %d\n", ChannelStart, ChannelEnd);
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "SubChStart: %d\tSubChEnd: %d\n", SubChStart, SubChEnd);
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "RankStart: %d\tRankEnd: %d\n", RankStart, RankEnd);
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "StrobeStart: %d\tStrobeEnd: %d\n", StrobeStart, StrobeEnd);
    MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "BitStart: %d\tBitEnd: %d\n", BitStart, BitEnd);
    for (SocketLoop = SocketStart; SocketLoop <= SocketEnd; SocketLoop++) {
      for (ChannelLoop = ChannelStart; ChannelLoop <= ChannelEnd; ChannelLoop++) {
        if ((!MrcChannelExist (MrcData, 0, ChannelLoop)) && (Channel != MRC_IGNORE_ARG)) {
          MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "Group %s skipped as %s %d does not exist\n", GsmGtDebugStrings[Group], gChannelStr, ChannelLoop);
          continue;
        }
        for (SubChLoop = SubChStart; SubChLoop <= SubChEnd; SubChLoop++) {
          for (RankLoop = RankStart; RankLoop <= RankEnd; RankLoop++) {
            if ((!MrcRankExist (MrcData, CONTROLLER_0, ChannelLoop, RankLoop)) && (Rank != MRC_IGNORE_ARG)) {
              MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "Group %s skipped as %s %d does not exist\n", GsmGtDebugStrings[Group], gRankStr, RankLoop);
              continue;
            }
            for (StrobeLoop = StrobeStart; StrobeLoop <= StrobeEnd; StrobeLoop++) {
              if ((!MrcByteInChannelExist (MrcData, ChannelLoop, StrobeLoop)) && (Strobe != MRC_IGNORE_ARG)) {
                MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "Group %s skipped as %s %d does not exist\n", GsmGtDebugStrings[Group], gByteStr, StrobeLoop);
                continue;
              }
              for (BitLoop = BitStart; BitLoop <= BitEnd; BitLoop++) {
                CurrentStatus = MrcGetSetDdrIoGroup (
                                  MrcData,
                                  SocketLoop,
                                  ChannelLoop,
                                  SubChLoop,
                                  Dimm,
                                  RankLoop,
                                  StrobeLoop,
                                  BitLoop,
                                  FreqIndex,
                                  Level,
                                  Group,
                                  Mode,
                                  Value
                                  );
                if (Status == mrcSuccess) {
                  Status = CurrentStatus;
                }
              } // BitLoop
            } // StrobeLoop
          } // RankLoop
        } // SubChLoop
      } // ChannelLoop
    } // SocketLoop
    // We stop here when breaking up multicasts.
    return Status;
  }

  if (MrcCheckComplexOrSideEffect (MrcData, Group, MRC_IS_COMPLEX)) {
    // Complex parameters will stop here.
    Status = MrcGetSetComplexParam (MrcData, Socket, 0, Channel, SubChannel, Rank, Strobe, Bit, FreqIndex, Group, Mode, Value);
    if (Status == mrcComplexParamDone) {
      return mrcSuccess;
    }
  }

  // Get CR offset and field hash value
  if (Group < EndOfPhyMarker) {
    Status = MrcGetDdrIoHash (MrcData, Group, Socket, CONTROLLER_0, Channel, Rank, Strobe, Bit, FreqIndex, &HashVal.Data);
  } else if (Group < EndOfIocMarker) {
    Status = MrcGetDdrIoCfgHash (MrcData, Group, Socket, Channel, Rank, Strobe, Bit, FreqIndex, &HashVal.Data);
  } else if (Group < EndOfMctMarker) {
    Status = MrcGetMcTimingHash (MrcData, Group, Socket, CONTROLLER_0, Channel, FreqIndex, &HashVal.Data);
  } else if (Group < EndOfMccMarker) {
    Status = MrcGetMcConfigHash (MrcData, Group, Socket, CONTROLLER_0, Channel, Rank, Strobe, Bit, FreqIndex, &HashVal.Data);
  } else {
    Status = MrcGetCmiHash (MrcData, CONTROLLER_0, Group, Socket, Channel, FreqIndex, &HashVal.Data);
  }
  if (Status != mrcSuccess) {
    return Status;
  }

  Offset = HashVal.Bits.Offset;
  RegSize = (HashVal.Bits.RegSize != 0);
#ifdef MRC_DEBUG_PRINT
  if (Offset == 0xFFFF) {
    p = Str;
    p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), "No Offset found for Group %s(%d)", GsmGtDebugStrings[Group], Group) - 1;
    if (Socket != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), " Socket %u,", Socket) - 1;
    }
    if (Channel != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), " Channel %u,", Channel) - 1;
    }
    if (SubChannel != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), " SubCh %u,", SubChannel) - 1;
    }
    if (Rank != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), " Rank %u,", Rank) - 1;
    }
    if (Strobe != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), " Strobe %u,", Strobe) - 1;
    }
    if (Bit != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), " Bit %u,", Bit) - 1;
    }
    if (FreqIndex != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), " FreqIndex %u", FreqIndex) - 1;
    }
    MRC_DEBUG_ASSERT (Offset != 0xFFFF, Debug, "%s\n", Str);
  }
#endif // MRC_DEBUG_PRINT

  // Init data.
  RegVal = MrcCrCacheRead (MrcData, Offset, RegSize, Mode);

  // Get the group value
  CurrentVal = MrcHalGsmGetBitField (MrcData, HashVal, RegVal);

  if (ReadOnly) {
    *Value = CurrentVal;
  } else {
    CurrentVal = ((Mode & GSM_WRITE_OFFSET) == GSM_WRITE_OFFSET) ? CurrentVal + *Value : *Value;

    // Check value limits.
    MrcGetSetLimits (MrcData, Group, &Min, &Max, &Delay);

    if ((CurrentVal < Min) || (CurrentVal > Max)) {
#ifdef MRC_DEBUG_PRINT
      p = Str;
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), "%s", gWarnString) - 1;
      if (Channel != MRC_IGNORE_ARG) {
        p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), "C%u", Channel) - 1;
      }
      if (SubChannel != MRC_IGNORE_ARG) {
        p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), ".Sch%u", SubChannel) - 1;
      }
      if (Rank != MRC_IGNORE_ARG) {
        p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), ".R%u", Rank) - 1;
      }
      if (Strobe != MRC_IGNORE_ARG) {
        p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), ".S%u", Strobe) - 1;
      }
      if (Bit != MRC_IGNORE_ARG) {
        p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), ".B%u,", Bit) - 1;
      }
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), " %s value %lld is out of range [%lld:%lld]. Clamping\n",
                       GsmGtDebugStrings[Group], CurrentVal, Min, Max) - 1;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\n", Str);
#endif // MRC_DEBUG_PRINT
      Status = mrcParamSaturation;
    }
#ifdef MRC_DEBUG_PRINT
    if (Delay != 0) {
      MRC_HAL_DEBUG_MSG (Debug, MSG_LEVEL_HAL, "Delay for %uus\n", Delay);
    }
#endif
    CurrentVal = RANGE (CurrentVal, Min, Max);

    // Update CR with new value
    NewRegVal = MrcHalGsmSetBitField (MrcData, HashVal, CurrentVal, RegVal);
    if (((Mode & GSM_FORCE_WRITE) == GSM_FORCE_WRITE) || (RegVal != NewRegVal)) {
      MrcCrCacheWrite (MrcData, Offset, RegSize, Mode, Delay, NewRegVal);
    }
    // Check if the field written to has some side effect and resolve it.
    if (MrcCheckComplexOrSideEffect (MrcData, Group, MRC_IS_SIDE_EFFECT)) {
      MrcGetSetSideEffect (MrcData, Socket, 0, Channel, SubChannel, Dimm, Rank, Strobe, Bit, FreqIndex, Level, Group, Mode, &CurrentVal);
    }
  }

#ifdef MRC_DEBUG_PRINT
  if (Mode & GSM_PRINT_VAL) {
    p = Str;
    PrintVal.Data = CurrentVal;
    *p++ = (ReadOnly) ? 'R' : 'W';
    *p++ = ' ';
    if (Channel != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), "C%u", Channel) - 1;
    }
    if (SubChannel != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), ".Sch%u", SubChannel) - 1;
    }
    if (Rank != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), ".R%u", Rank) - 1;
    }
    if (Strobe != MRC_IGNORE_ARG) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), ".S%u", Strobe) - 1;
    }
    p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), " %s: ", GsmGtDebugStrings[Group]) - 1;
    if (HashVal.Bits.BfWidth > 32) {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), "0x%llx", CurrentVal) - 1;
    } else {
      p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), "%d", PrintVal.Data32.Low) - 1;
      if ((HashVal.Bits.BfSign == 0) && (PrintVal.Data32.Low > 9)) {
        p += MrcSPrintf (MrcData, p, sizeof (Str) - (p - Str), " (0x%x)", PrintVal.Data32.Low) - 1;
      }
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\n", Str);
  }
#endif // MRC_DEBUG_PRINT

  return Status;
}

/**
  Update the hash for the PI setting field based on Gear.

  @param[in]      Gear4    - Gear4 enable
  @param[in]      Gear2    - Gear2 enable
  @param[in, out] HashPtr  - Hash pointer

  @retval none
**/
VOID
MrcGetSetUpdatePiHashPerGear (
  IN     BOOLEAN                  Gear4,
  IN     BOOLEAN                  Gear2,
  IN OUT MRC_REGISTER_HASH_STRUCT *HashPtr
  )
{
  if (Gear2) {
    // In Gear2 the value starts from bit [1]
    HashPtr->Bits.BfOffset += 1;
    HashPtr->Bits.BfWidth  -= 1;
  } else if (!Gear4) { // Gear1
    // In Gear1 the value starts from bit [2]
    HashPtr->Bits.BfOffset += 2;
    HashPtr->Bits.BfWidth  -= 2;
  }
}

/**
  Update the MAX value for the PI setting field based on Gear.

  @param[in]      Gear4    - Gear4 enable
  @param[in]      Gear2    - Gear2 enable
  @param[in, out] Max      - Pointer to the Max value

  @retval none
**/
VOID
MrcGetSetUpdatePiMaxPerGear (
  IN     BOOLEAN  Gear4,
  IN     BOOLEAN  Gear2,
  IN OUT INT64    *Max
  )
{
  if (Gear2) {
    // In Gear2 the value starts from bit [1]
    *Max = ((INT32) (*Max)) / 2;
  } else if (!Gear4) { // Gear1
    // In Gear1 the value starts from bit [2]
    *Max = ((INT32) (*Max)) / 4;
  }
}

/**
  This function returns the register limits.

  @param[in]  MrcData   - Pointer to global data.
  @param[in]  Group     - DDRIO group to access.
  @param[out] MinVal    - Return pointer for Minimum Value supported.
  @param[out] MaxVal    - Return pointer for Maximum Value supported.
  @param[out] WaitTime  - Return pointer for settle time required in microseconds.

  @retval MrcStatus - mrcSuccess if the parameter is found, otherwise mrcFail.
**/
MrcStatus
MrcGetDdrIoGroupLimits (
  IN  MrcParameters *const MrcData,
  IN  GSM_GT  const   Group,
  OUT INT64   *const  MinVal,
  OUT INT64   *const  MaxVal,
  OUT UINT32  *const  WaitTime
  )
{
  MrcStatus Status;
  MrcOutput *Outputs;
  BOOLEAN   Lpddr5;
  BOOLEAN   Gear2;
  BOOLEAN   Gear4;
//  BOOLEAN   Matched;
  MrcDebug  *Debug;
  INT64     Min;
  INT64     Max;
  UINT32    Wait;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status  = mrcSuccess;
  Wait    = 0;
  Min     = MRC_INT64_MIN;
  Max     = MRC_INT64_MAX;
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Gear2   = Outputs->Gear2;
  Gear4   = Outputs->Gear4;
  if (((Outputs->DdrType == MRC_DDR_TYPE_LPDDR4) && (Outputs->Frequency == f1600)) || (Lpddr5 && (Outputs->Frequency == f1100))) {
    Gear2 = FALSE;
  }
//  Matched = (Outputs->RxMode == MrcRxModeMatchedN) || (Outputs->RxMode == MrcRxModeMatchedP);

  switch (Group) {
    case RecEnDelay:
      Min = DATA0CH0_CR_RXCONTROL0RANK0_rxrcvenpi_MIN;
      Max = DATA0CH0_CR_RXCONTROL0RANK0_rxrcvenpi_MAX;
      MrcGetSetUpdatePiMaxPerGear (Gear4, Gear2, &Max);
      break;

    case RxDqsNDelay:
      Min = DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsnpicode_MIN;
      Max = DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsnpicode_MAX;
      break;

    case CompRcompOdtUpPerStrobe:
      Min = DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_MIN;
      Max = DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_MAX;
      break;

    case CompRcompOdtDnPerStrobe:
      Min = DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_MIN;
      Max = DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_MAX;
      break;

    case RxDqsPDelay:
      Min = DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsppicode_MIN;
      Max = DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsppicode_MAX;
      break;

    case RxVref:
      Min = DATA0CH0_CR_DDRCRDATACONTROL3_rxvref_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL3_rxvref_MAX;
      break;

    case RxVoc:
      // For matched we sweep 1-31, for Unmatched we sweep 0 - 30
// @todo_adl      Min = (Matched) ? 1 : DATA0CH0_CR_DDRDATADQRANK0LANE0_RxDQVrefOffset0_MIN;
// @todo_adl      Max = (Matched) ? DATA0CH0_CR_DDRDATADQRANK0LANE0_RxDQVrefOffset0_MAX : 30;
      break;

    case RxVocUnmatched:
// @todo_adl      Min = DATA0CH0_CR_DDRDATADQRANK0LANE0_RxDQVrefOffset1_MIN;
// @todo_adl      Max = 30; //DATA0CH0_CR_DDRDATADQRANK0LANE0_RxDQVrefOffset1_MAX;
      break;

    case RxEq:
      Min = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleeq_MIN;
      Max = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleeq_MAX;
      break;

    case RxR:
      Min = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleres_MIN;
      Max = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleres_MAX;
      break;

    case RxC:
      Min = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctlecap_MIN;
      Max = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctlecap_MAX;
      break;

    case RxDqsEq:
      Min = DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleeq_MIN;
      Max = DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleeq_MAX;
      break;

    case RxTap1En:
    case RxTap2En:
    case RxTap3En:
      Min = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe1en_MIN;
      Max = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe1en_MAX;
      break;
    case RxTap1:
      Min = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap1_MIN;
      Max = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap1_MAX;
      break;

    case RxTap2:
      Min = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap2_MIN;
      Max = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap2_MAX;
      break;

    case RxTap3:
      Min = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap3_MIN;
      Max = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap3_MAX;
      break;

    case RxTap4:
      Min = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap4_MIN;
      Max = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap4_MAX;
      break;

    case RxDqsBitDelay:
    case RxDqsNBitDelay:
    case RxDqsPBitDelay:
      Min = DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycodep_MIN;
      Max = 209;
      break;

    case RxDqsBitOffset:
// @todo_adl      Min = DATA0CH0_CR_DDRDATADQRANK0LANE0_RxDQPerBitDeskewOffset_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRDATADQRANK0LANE0_RxDQPerBitDeskewOffset_MAX;
      break;

    case RxRankMuxDelay:
      Min = DATA0CH0_CR_DDRCRDATACONTROL1_rxrankmuxdelay_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL1_rxrankmuxdelay_MAX;
      break;

    case TxDqsDelay:
      Min = DATA0CH0_CR_TXCONTROL0RANK0_txdqsdelay_MIN;
      Max = DATA0CH0_CR_TXCONTROL0RANK0_txdqsdelay_MAX;
      MrcGetSetUpdatePiMaxPerGear (Gear4, Gear2, &Max);
      break;

    case TxDqsDelay90:
      Min = DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk0_MIN;
      Max = DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk0_MAX;
      break;

    case TxDqDelay:
      Min = DATA0CH0_CR_TXCONTROL0RANK0_txdqdelay_MIN;
      Max = DATA0CH0_CR_TXCONTROL0RANK0_txdqdelay_MAX;
      MrcGetSetUpdatePiMaxPerGear (Gear4, Gear2, &Max);
      break;

    case TxDqDelay90:
      Min = DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_MIN;
      Max = DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_MAX;
      break;

    case TxEq:
    case TxEqCoeff0:
    case TxEqCoeff1:
    case TxEqCoeff2:
      Min = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff0_MIN;
      Max = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff0_MAX;
      break;

    case DefDrvEnLow:
      Min = CH2CCC_CR_DDRCRCCCCLKCONTROLS_DefDrvEnLow_MIN;
      Max = CH2CCC_CR_DDRCRCCCCLKCONTROLS_DefDrvEnLow_MAX;
      break;

    case CmdTxEq:
      Min = CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaTxEq_MIN;
      Max = CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaTxEq_MAX;
      break;

    case CtlTxEq:
      Min = CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlTxEq_MIN;
      Max = CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlTxEq_MAX;
      break;

    case RxVrefVttDecap:
    case RxVrefVddqDecap:
      Min = DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvttmfc_MIN;
      Max = DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvttmfc_MAX;
      break;

    case ForceDfeDisable:
      Min = DATA0CH0_CR_DQRXCTL1_forcedfedisable_MIN;
      Max = DATA0CH0_CR_DQRXCTL1_forcedfedisable_MAX;
      break;

    case PanicVttDnLp:
// @todo_adl      Min = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC2_VttPanicCompDnLpMult_MIN;
// @todo_adl      Max = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC2_VttPanicCompDnLpMult_MAX;
      break;

    case VttGenStatusSelCount:
      Min = DDRVTT0_CR_DDRCRVTTGENSTATUS_SelCount_MIN;
      Max = DDRVTT0_CR_DDRCRVTTGENSTATUS_SelCount_MAX;
      break;

    case VttGenStatusCount:
      Min = DDRVTT0_CR_DDRCRVTTGENSTATUS_Count_MIN;
      Max = DDRVTT0_CR_DDRCRVTTGENSTATUS_Count_MAX;
      break;

    case RloadDqsDn:
// @todo_adl      Min = DDRVCCDLL0_CR_DDRCRVCCDLLCOMPDLL_RloadDqs_MIN;
// @todo_adl      Max = DDRVCCDLL0_CR_DDRCRVCCDLLCOMPDLL_RloadDqs_MAX;
      break;

    case RxCben:
// @todo_adl      Min = DLLDDRDATA0_CR_PITUNE_PiCbEn_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_PITUNE_PiCbEn_MAX;
      break;

    case RxBiasCtl:
      Min = DATA0CH0_CR_AFEMISC_rxbias_cmn_biasicomp_MIN;
      Max = DATA0CH0_CR_AFEMISC_rxbias_cmn_biasicomp_MAX;
      break;

    case RxLpddrMode:
      Min = DATA0CH0_CR_AFEMISC_rxall_cmn_rxlpddrmode_MIN;
      Max = DATA0CH0_CR_AFEMISC_rxall_cmn_rxlpddrmode_MAX;
      break;

    case RxBiasVrefSel:
      Min = DATA0CH0_CR_AFEMISC_rxbias_cmn_vrefsel_MIN;
      Max = DATA0CH0_CR_AFEMISC_rxbias_cmn_vrefsel_MAX;
      break;

    case RxBiasTailCtl:
      Min = DATA0CH0_CR_AFEMISC_rxbias_cmn_tailctl_MIN;
      Max = DATA0CH0_CR_AFEMISC_rxbias_cmn_tailctl_MAX;
      break;

    case DataRxD0PiCb:
// @todo_adl      Min = DATA0CH0_CR_DDRCRDATACONTROL6_rxd0picb_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRCRDATACONTROL6_rxd0picb_MAX;
      break;

    case DataSDllPiCb:
// @todo_adl      Min = DATA0CH0_CR_DDRCRDATACONTROL6_sdll_picb_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRCRDATACONTROL6_sdll_picb_MAX;
      break;

    case VccDllRxD0PiCb:
// @todo_adl      Min = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_RxD0PiCB_MIN;
// @todo_adl      Max = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_RxD0PiCB_MAX;
      break;

    case VccDllSDllPiCb:
// @todo_adl      Min = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_SdllPiCB_MIN;
// @todo_adl      Max = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_SdllPiCB_MAX;
      break;

    case DqsOdtCompOffset:
      Min = DATA0CH0_CR_DDRCRDATACONTROL2_dqsodtcompoffset_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL2_dqsodtcompoffset_MAX;
      break;

    case TxRankMuxDelay:
      Min = DATA0CH0_CR_DDRCRDATACONTROL0_txrankmuxdelay_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL0_txrankmuxdelay_MAX;
      break;

    case TxDqsRankMuxDelay:
      Min = DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_MAX;
      break;

    case TxDqBitDelay:
    case TxDqBitDelay90:
      Min = DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_MIN;
      Max = 209;
      break;

    case RxUnmatchedOffNcal:
      Min = DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxunoffsetcal_MIN;
      Max = DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxunoffsetcal_MAX;
      break;

    case RxMatchedUnmatchedOffPcal:
      Min = DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxupmpoffsetcal_MIN;
      Max = DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxupmpoffsetcal_MAX;
      break;

    case TxDqDccOffset:
// @todo_adl      Min = DATA0CH0_CR_DDRDATADQRANK0LANE0_TxDqDCCOffset_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRDATADQRANK0LANE0_TxDqDCCOffset_MAX;
      break;

    case TxDqsDccOffset:
// @todo_adl      Min = DATA0CH0_CR_DDRDATADQSRANK0_TxDQSDCCOffset_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRDATADQSRANK0_TxDQSDCCOffset_MAX;
      break;

    case TxDqsBitDelay:
      Min = DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk0_MIN;
      Max = 209;
      break;

    case RecEnOffset:
      Min = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rcvenoffset_MIN;
      Max = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rcvenoffset_MAX;
      break;

    case RxDqsOffset:
      Min = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqsoffset_MIN;
      Max = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqsoffset_MAX;
      break;

    case RxVrefOffset:
      Min = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_vrefoffset_MIN;
      Max = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_vrefoffset_MAX;
      break;

    case TxDqsOffset:
      Min = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqsoffset_MIN;
      Max = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqsoffset_MAX;
      break;

    case TxDqOffset:
      Min = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqoffset_MIN;
      Max = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqoffset_MAX;
      break;

    case RxDqsPiUiOffset:
      Min = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqspiuioffset_MIN;
      Max = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqspiuioffset_MAX;
      break;

    case RoundTripDelay:
      Min = MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_0_latency_MIN;
      Max = MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_0_latency_MAX;
      break;

    case RxFlybyDelay:
      Min = MCMISCS_READCFGCH01_rcvenrank0chadel_MIN;
      Max = MCMISCS_READCFGCH01_rcvenrank0chadel_MAX;
      break;

    case RxIoTclDelay:
      Min = MCMISCS_READCFGCH0_tcl4rcven_MIN;
      Max = MCMISCS_READCFGCH0_tcl4rcven_MAX;
      break;

    case RxFifoRdEnFlybyDelay:
      Min = MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_MIN;
      Max = (Gear2 || Gear4) ? (MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_MAX - 1) / 2 : MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_MAX;
      break;

    case RxFifoRdEnTclDelay:
      Min = MCMISCS_READCFGCH0_tcl4rxdqfiforden_MIN;
      Max = MCMISCS_READCFGCH0_tcl4rxdqfiforden_MAX;
      break;

    case RxDqDataValidDclkDelay:
      Min = MCMISCS_READCFGCH0_rxdqdatavaliddclkdel_MIN;
      Max = MCMISCS_READCFGCH0_rxdqdatavaliddclkdel_MAX;
      break;

    // @todo: BOOLEAN case.  If more 1-bit field are added: consolidate.
    case RxDqDataValidQclkDelay:
      Min = MCMISCS_READCFGCH0_rxdqdatavalidqclkdel_MIN;
      Max = MCMISCS_READCFGCH0_rxdqdatavalidqclkdel_MAX;
      break;

    case TxDqFifoWrEnTcwlDelay:
      Min = MCMISCS_WRITECFGCH0_tcwl4txdqfifowren_MIN;
      Max = MCMISCS_WRITECFGCH0_tcwl4txdqfifowren_MAX;
      break;

    case TxDqFifoRdEnTcwlDelay:
      Min = MCMISCS_WRITECFGCH0_tcwl4txdqfiforden_MIN;
      Max = MCMISCS_WRITECFGCH0_tcwl4txdqfiforden_MAX;
      break;

    case TxDqFifoRdEnFlybyDelay:
      Min = MCMISCS_WRITECFGCH01_txdqfifordenrank0chadel_MIN;
      Max = MCMISCS_WRITECFGCH01_txdqfifordenrank0chadel_MAX;
      break;

    case CmdVref:
      Min = DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca0vref_MIN;
      Max = 191;  // DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca0vref_MAX is 511, but max valid value is 191  // @todo_adl - check this with design
      break;

    case SenseAmpDelay:
      Min = DATA0CH0_CR_DDRCRDATACONTROL1_senseampdelay_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL1_senseampdelay_MAX;
      break;

    case SenseAmpDuration:
      Min = DATA0CH0_CR_DDRCRDATACONTROL1_senseampduration_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL1_senseampduration_MAX;
      break;

    case McOdtDelay:
      Min = DATA0CH0_CR_DDRCRDATACONTROL1_dqodtdelay_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL1_dqodtdelay_MAX;
      break;

    case McOdtDuration:
      Min = DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_MAX;
      break;

    case DqsOdtDelay:
      Min = DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtdelay_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtdelay_MAX;
      break;

    case DqsOdtDuration:
      Min = DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_MAX;
      break;

    case RxDqsAmpOffset:
      // Smallest offset starts at 1, not 0.
// @todo_adl      Min = 1; //DATA0CH0_CR_DDRDATADQSRANK0_RxDQSVrefOffset_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRDATADQSRANK0_RxDQSVrefOffset_MAX;
      break;

    case RxDqsUnmatchedAmpOffset:
// @todo_adl      Min = DATA0CH0_CR_DDRDATADQSRANK0_RxDqsUnMatVrefOffset_MIN;
// @todo_adl      Max = 30;  //DATA0CH0_CR_DDRDATADQSRANK0_RxDqsUnMatVrefOffset_MAX

    case CtlGrpPi:
      Min = CH2CCC_CR_PICODE0_mdll_cmn_pi0code_MIN;
      // Max = CH0CCC_CR_DDRCRCCCPICODING0_PiCode0_MAX / ((Gear2 && Lpddr5) ? 1 : 2);  // TGL code
      Max = CH2CCC_CR_PICODE1_mdll_cmn_pi2code_MAX; // CS PI codes are 10 bits
      break;

    case CmdGrpPi:
      Min = CH2CCC_CR_PICODE0_mdll_cmn_pi1code_MIN;
      Max = CH2CCC_CR_PICODE0_mdll_cmn_pi1code_MAX;
      break;

    case ClkGrpPi:
      Min = CH2CCC_CR_PICODE2_mdll_cmn_pi4code_MIN;
      Max = CH2CCC_CR_PICODE2_mdll_cmn_pi4code_MAX;
      break;

    case ClkGrpG4Pi:
      Min = CH2CCC_CR_PICODE2_mdll_cmn_pi5code_MIN;
      Max = CH2CCC_CR_PICODE2_mdll_cmn_pi5code_MAX;
      break;

    case CkeGrpPi:
      Min = CH2CCC_CR_PICODE1_mdll_cmn_pi2code_MIN;
      Max = CH2CCC_CR_PICODE1_mdll_cmn_pi2code_MAX;
      break;

    case WckGrpPi:
      Min = CH2CCC_CR_PICODE3_mdll_cmn_pi6code_MIN;
      Max = CH2CCC_CR_PICODE3_mdll_cmn_pi6code_MAX;
      break;

    case WckGrpPi90:
      Min = CH2CCC_CR_PICODE3_mdll_cmn_pi7code_MIN;
      Max = CH2CCC_CR_PICODE3_mdll_cmn_pi7code_MAX;
      break;

    case TxSlewRate:
    case CmdSlewRate:
    case CtlSlewRate:
    case ClkSlewRate:
// @todo_adl      Min = DDRPHY_COMP_CR_DDRCRCOMPCTL1_DqScompCells_MIN;
// @todo_adl      Max = DDRPHY_COMP_CR_DDRCRCOMPCTL1_DqScompCells_MAX;
      break;

    case DqScompPC:
    case CmdScompPC:
    case CtlScompPC:
    case ClkScompPC:
    case RxRptChDqClkOn:
    case TxRptChDqClkOn:
    case SCompBypassDq:
    case SCompBypassCmd:
    case SCompBypassCtl:
    case SCompBypassClk:
      Min = 0;
      Max = 1;
      break;

    case SCompCodeDq:
    case SCompCodeClk:
// @todo_adl      Min = DATA0CH0_CR_RCOMPDATA0_SlewRateComp_MIN;
// @todo_adl      Max = DATA0CH0_CR_RCOMPDATA0_SlewRateComp_MAX;
      break;

    case SCompCodeCmd:
    case SCompCodeCtl:
    case TxRonUp:
    case WrDSCodeUpCmd:
    case WrDSCodeUpCtl:
    case WrDSCodeUpClk:
    case TxRonDn:
    case WrDSCodeDnCmd:
    case WrDSCodeDnCtl:
    case WrDSCodeDnClk:
      Min = DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvUp_MIN;
      Max = DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvUp_MAX;
      break;

    case TxTco: // This is a 2's complement register, but we set absolute values to it (the sweep thus ends up being non-monotonic)
      Min = DATA0CH0_CR_COMPCTL2_dxtx_cmn_txtcocomp_MIN;
      Max = DATA0CH0_CR_COMPCTL2_dxtx_cmn_txtcocomp_MAX;
      break;

    case TxDqsTcoP:
    case TxDqsTcoN:
      Min = DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsp_MIN;
      Max = DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsp_MAX;
      break;

    // @todo: This is the Comp register.  Not the comp code.  Where is it?
    case TcoCompCodeCmd:
    case TcoCompCodeCtl:
    case TcoCompCodeClk:
// @todo_adl      Min = DDRPHY_COMP_CR_DDRCRCACOMP_TcoComp_MIN;
// @todo_adl      Max = DDRPHY_COMP_CR_DDRCRCACOMP_TcoComp_MAX;
      break;

    case DqOdtVrefUp:
    case DqOdtVrefDn:
    case DqDrvVrefUp:
    case DqDrvVrefDn:
    case CmdDrvVrefUp:
    case CmdDrvVrefDn:
    case CtlDrvVrefUp:
    case CtlDrvVrefDn:
    case ClkDrvVrefUp:
    case ClkDrvVrefDn:
      Min = DDRPHY_COMP_CR_DDRCRCOMPCTL0_dqodtvrefup_MIN;
      Max = 191;
      break;

    case CompRcompOdtUp:
    case CompRcompOdtDn:
      Min = DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_MIN;
      Max = DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_MAX;
      break;

    case CtlSCompOffset:
    case CmdSCompOffset:
    case ClkSCompOffset:
    //@todo: Question - case CkeSCompOffset:
      Min = CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlScompOffset_MIN;
      Max = CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlScompOffset_MAX;
      break;

    case CtlRCompDrvUpOffset:
    case CtlRCompDrvDownOffset:
    case CmdRCompDrvUpOffset:
    case CmdRCompDrvDownOffset:
    //@todo: Question - case CkeRCompDrvDownOffset:
    //@todo: Question - case CkeRCompDrvUpOffset:
    case ClkRCompDrvUpOffset:
    case ClkRCompDrvDownOffset:
      Min = CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvUpOffset_MIN;
      Max = CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvUpOffset_MAX;
      break;

    case VsxHiClkFFOffset:
    case VsxHiCaFFOffset:
    case VsxHiCtlFFOffset:
// @todo_adl      Min = CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkFFOffset_MIN;
// @todo_adl      Max = CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkFFOffset_MAX;
      break;

    case CompOffsetVssHiFF:
// @todo_adl      Min = DATA0CH0_CR_DDRCRDATAOFFSETCOMP_VssHiFFCompOffset_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRCRDATAOFFSETCOMP_VssHiFFCompOffset_MAX;
      break;

    case RxPerBitDeskewCal:
    case TxPerBitDeskewCal:
    case CccPerBitDeskewCal:
// @todo_adl      Min = DATA0CH0_CR_DDRCRDATACONTROL2_TxPBDCalibration_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRCRDATACONTROL2_TxPBDCalibration_MAX;
      break;

    default:
      break;
  }

  if ((Min == MRC_INT64_MIN) && (Max == MRC_INT64_MAX)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Group %s(%d) has no limits defined\n", GsmGtDebugStrings[Group], Group);
    Status = mrcWrongInputParameter;
  }

  // Null guard the pointers
  if (MinVal != NULL) {
    *MinVal = Min;
  }
  if (MaxVal != NULL) {
    *MaxVal = Max;
  }
  if (WaitTime != NULL) {
    *WaitTime = Wait;
  }

  return Status;
}

/**
  This function returns complex group limits.

  @param[in]  MrcData   - Pointer to global data.
  @param[in]  Group     - Complex group to access.
  @param[out] MinVal    - Return pointer for Minimum Value supported.
  @param[out] MaxVal    - Return pointer for Maximum Value supported.
  @param[out] WaitTime  - Return pointer for settle time required in microseconds.

  @retval MrcStatus - mrcSuccess if the parameter is found, otherwise mrcFail.
**/
MrcStatus
MrcGetComplexGroupLimits (
  IN  MrcParameters *const  MrcData,
  IN  UINT32        const   Group,
  OUT INT64         *const  MinVal,
  OUT INT64         *const  MaxVal,
  OUT UINT32        *const  WaitTime
  )
{
  MrcStatus Status;
  MrcDebug  *Debug;
  INT64     Min;
  INT64     Max;
  UINT32    Wait;

  Debug   = &MrcData->Outputs.Debug;
  Status  = mrcSuccess;
  Wait    = 0;
  Min     = MRC_INT64_MIN;
  Max     = MRC_INT64_MAX;

  switch (Group) {
    //case GsmComplexRxBias:
    //  Min = 0;
    //  Max = DDRDATA_CR_DDRCRDATACONTROL1_RxBiasCtl_MAX;
    //break;

    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Group %s(%d) has no limits defined\n", GsmGtDebugStrings[Group], Group);
      Status = mrcWrongInputParameter;
      break;
  }

  // Null guard the pointers
  if (MinVal != NULL) {
    *MinVal = Min;
  }
  if (MaxVal != NULL) {
    *MaxVal = Max;
  }
  if (WaitTime != NULL) {
    *WaitTime = Wait;
  }

  return Status;
}

/**
  This function returns DDRIO Configuration group limits.

  @param[in]  MrcData   - Pointer to global data.
  @param[in]  Group     - DDRIO group to access.
  @param[out] MinVal    - Return pointer for Minimum Value supported.
  @param[out] MaxVal    - Return pointer for Maximum Value supported.
  @param[out] WaitTime  - Return pointer for settle time required in microseconds.

  @retval MrcStatus - mrcSuccess if the parameter is found, otherwise mrcFail.
**/
MrcStatus
MrcGetDdrIoCfgGroupLimits (
  IN  MrcParameters *const  MrcData,
  IN  UINT32        const   Group,
  OUT INT64         *const  MinVal,
  OUT INT64         *const  MaxVal,
  OUT UINT32        *const  WaitTime
  )
{
  MrcStatus Status;
  MrcDebug  *Debug;
  INT64     Min;
  INT64     Max;
  UINT32    Wait;
  BOOLEAN   A0;

  Debug   = &MrcData->Outputs.Debug;
  Status  = mrcSuccess;
  Wait    = 0;
  Min     = MRC_INT64_MIN;
  Max     = MRC_INT64_MAX;
  A0      = MrcData->Inputs.A0;

  switch (Group) {
    case GsmIocWlWakeCyc:
// @todo_adl      Min = DLLDDRDATA0_CR_DLLWEAKLOCK_wl_wakecycles_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DLLWEAKLOCK_wl_wakecycles_MAX;
      break;

    case GsmIocWlSleepCyclesLp:
    case GsmIocWlSleepCyclesAct:
// @todo_adl      Min = DLLDDRDATA0_CR_DLLWEAKLOCK_WL_SleepCyclesAct_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DLLWEAKLOCK_WL_SleepCyclesAct_MAX;
      break;

    case GsmIocChNotPop:
      Min = DDRSCRAM_CR_DDRMISCCONTROL0_channel_not_populated_MIN;
      Max = DDRSCRAM_CR_DDRMISCCONTROL0_channel_not_populated_MAX;
      break;

    case GsmIocRxClkStg:
      Min = DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_MIN;
      Max = A0 ? DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_A0_MAX : DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_MAX;
      break;

    case GsmIocDataRxBurstLen:
      Min = DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_MAX;
      break;

    case GsmIocDllMask:
// @todo_adl      Min = DLLDDRDATA0_CR_DLLWEAKLOCK_DLLMask0_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DLLWEAKLOCK_DLLMask0_MAX;
      break;

    case GsmIocSdllSegmentDisable:
// @todo_adl      Min = DATA0CH0_CR_DDRCRDATACONTROL1_SdllSegmentDisable_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRCRDATACONTROL1_SdllSegmentDisable_MAX;
      break;

    case GsmIocCmdDrvVref200ohm:
// @todo_adl      Min = DDRPHY_COMP_CR_DDRCRCOMPCTL2_Cmd200VrefDn_MIN;
// @todo_adl      Max = DDRPHY_COMP_CR_DDRCRCOMPCTL2_Cmd200VrefDn_MAX;
      break;

    case GsmIocVttPanicCompUpMult:
// @todo_adl      Min = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC_VttPanicCompUp0Mult_MIN;
// @todo_adl      Max = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC_VttPanicCompUp0Mult_MAX;
      break;

    case GsmIocVttPanicCompDnMult:
// @todo_adl      Min = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC_VttPanicCompDn0Mult_MIN;
// @todo_adl      Max = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC_VttPanicCompDn0Mult_MAX;
      break;

    case GsmIocCmdAnlgEnGraceCnt:
      Min = MCMISCS_WRITECFGCH0_cmdanlgengracecnt_MIN;
      Max = MCMISCS_WRITECFGCH0_cmdanlgengracecnt_MAX;
      break;

    case GsmIocTxAnlgEnGraceCnt:
      Min = MCMISCS_WRITECFGCH0_txanlgengracecnt_MIN;
      Max = A0 ? MCMISCS_WRITECFGCH0_txanlgengracecnt_A0_MAX : MCMISCS_WRITECFGCH0_txanlgengracecnt_MAX;
      break;

    case GsmIocDqsMaskPulseCnt:
      Min = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrmaskcntpulsenumstart_MIN;
      Max = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrmaskcntpulsenumstart_MAX;
      break;

    case GsmIocDqsMaskValue:
      Min = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrdqsmaskvalue_MIN;
      Max = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrdqsmaskvalue_MAX;
      break;

    case GsmIocDqsPulseCnt:
      Min = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrnumofpulses_MIN;
      Max = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrnumofpulses_MAX;
      break;

    case GsmIocDqOverrideData:
      Min = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrddata_MIN;
      Max = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrddata_MAX;
      break;

    case GsmIocDqOverrideEn:
      Min = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrdmodeen_MIN;
      Max = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrdmodeen_MAX;
      break;

/*
    case GsmIocRxPathBiasRcomp: // @todo No TGL version found
      Min = DATA0CH0_CR_DDRCRDATACONTROL4_biasrcomp_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL4_biasrcomp_MAX;
      break;
*/
    case GsmIocRankOverrideVal:
      Min = DATA0CH0_CR_DATATRAINFEEDBACK_rankvalue_MIN;
      Max = DATA0CH0_CR_DATATRAINFEEDBACK_rankvalue_MAX;
      break;

    case GsmIocVccDllRxDeskewCal:
    case GsmIocVccDllTxDeskewCal:
    case GsmIocVccDllCccDeskewCal:
// @todo_adl      Min = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_RxDeskewCal_MIN;
// @todo_adl      Max = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_RxDeskewCal_MAX;
      break;

    case GsmIocDccActiveClks:
      //@todo Min = MCMISCS_DCCMAINFSMCONTROL0_ActiveClks_MIN;
      //@todo Max = MCMISCS_DCCMAINFSMCONTROL0_ActiveClks_MAX;
      break;

    case GsmIocDccActiveBytes:
      //@todo Min = MCMISCS_DCCMAINFSMCONTROL0_ActiveBytes_MIN;
      //@todo Max = MCMISCS_DCCMAINFSMCONTROL0_ActiveBytes_MAX;
      break;

    case GsmIocDccClkTrainVal:
      //@todo Min = DDRCLKCH0_CR_DCCCLKTRAINVAL0_Clk0DccVal_MIN;
      //@todo Max = DDRCLKCH0_CR_DCCCLKTRAINVAL0_Clk0DccVal_MAX;
      break;

    case GsmIocDccDataTrainDqsVal:
      //@todo Min = DATA0CH0_CR_DCCDATATRAINVAL1RANK0_DqsDccVal_MIN;
      //@todo Max = DATA0CH0_CR_DCCDATATRAINVAL1RANK0_DqsDccVal_MAX;
      break;

    case GsmIocRxVrefMFC:
// @todo_adl      Min = DATA0CH0_CR_DDRCRDATACONTROL2_RxVrefVttProgMFC_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRCRDATACONTROL2_RxVrefVttProgMFC_MAX;
      break;

    case GsmIocRxVocMode:
// @todo_adl      Min = DATA0CH0_CR_DDRCRDATACONTROL0_RxMode_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRCRDATACONTROL0_RxMode_MAX;
      break;

    case GsmIocDataTrainFeedback:
      Min = DATA0CH0_CR_DATATRAINFEEDBACK_datatrainfeedback_MIN;
      Max = DATA0CH0_CR_DATATRAINFEEDBACK_datatrainfeedback_MAX;
      break;

    case GsmIocDdrDqDrvEnOvrdData:
      Min = DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrddata_MIN;
      Max = DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrddata_MAX;
      break;

    case GsmIocRxAmpOffsetEn:
      Min = DATA0CH0_CR_DATATRAINFEEDBACK_rxampoffseten_MIN;
      Max = DATA0CH0_CR_DATATRAINFEEDBACK_rxampoffseten_MAX;
      break;

    case GsmIocDataWrPreamble:
      Min = DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_MAX;
      break;

    case GsmIocLdoFFCodeLockData:
      Min = DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_MIN;
      Max = DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_MAX;
      break;

    case GsmIocLdoFFCodeLockCcc:
      Min = CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_MIN;
      Max = CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_MAX;
      break;

    case GsmIocDccDcdSampleSelCcc:
    case GsmIocDccDcdSampleSelData:
      Min = DATA0CH0_CR_DCCCTL10_dcdsamplesel_MIN;
      Max = DATA0CH0_CR_DCCCTL10_dcdsamplesel_MAX;
      break;

    case GsmIocStep1PatternCcc:
    case GsmIocStep1PatternData:
      Min = DATA0CH0_CR_DCCCTL10_step1pattern_MIN;
      Max = DATA0CH0_CR_DCCCTL10_step1pattern_MAX;
      break;

    case GsmIocDccCodePh0IndexCcc:
    case GsmIocDccCodePh90IndexCcc:
    case GsmIocDccCodePh0IndexData:
    case GsmIocDccCodePh90IndexData:
      Min = DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_MIN;
      Max = DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_MAX;
      break;

    case GsmIocTxpbddOffsetCcc:
    case GsmIocTxpbddOffsetData:
      Min = DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbddoffset_MIN;
      Max = DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbddoffset_MAX;
      break;

    case GsmIocMdllCmnVcdlDccCodeCcc:
    case GsmIocMdllCmnVcdlDccCodeData:
      Min = DATA0CH0_CR_MDLLCTL2_mdll_cmn_vcdldcccode_MIN;
      Max = DATA0CH0_CR_MDLLCTL2_mdll_cmn_vcdldcccode_MAX;
      break;

    case GsmIocDcdRoLfsrData:
      Min = DATA0CH0_CR_DCCCTL2_dcdrolfsr_MIN;
      Max = DATA0CH0_CR_DCCCTL2_dcdrolfsr_MAX;
      break;

    case GsmIocBiasPMCtrl:
      Min = DATA0CH0_CR_DDRCRDATACONTROL0_biaspmctrl_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL0_biaspmctrl_MAX;
      break;

    case GsmIocDataOdtMode:
// @todo_adl      Min = DATA0CH0_CR_DDRCRDATACONTROL0_OdtMode_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRCRDATACONTROL0_OdtMode_MAX;
      break;

    case GsmIocFFCodePiOffset:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodePIOffset_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodePIOffset_MAX;
      break;

    case GsmIocFFCodeIdleOffset:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeIdleOffset_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeIdleOffset_MAX;
      break;

    case GsmIocFFCodeWeakOffset:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeWeakOffset_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeWeakOffset_MAX;
      break;

    case GsmIocFFCodeWriteOffset:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeWriteOffset_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeWriteOffset_MAX;
      break;

    case GsmIocFFCodeReadOffset:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeReadOffset_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeReadOffset_MAX;
      break;

    case GsmIocFFCodePBDOffset:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodePBDOffset_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodePBDOffset_MAX;
      break;

    case GsmIocFFCodeCCCDistOffset:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeCCCDistOffset_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeCCCDistOffset_MAX;
      break;

    case GsmIocVssHiFFCodeIdle:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodeIdle_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodeIdle_MAX;
      break;

    case GsmIocVssHiFFCodeWrite:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodeWrite_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodeWrite_MAX;
      break;

    case GsmIocVssHiFFCodeRead:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodeRead_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodeRead_MAX;
      break;

    case GsmIocVssHiFFCodePBD:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodePBD_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodePBD_MAX;
      break;

    case GsmIocVssHiFFCodePi:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodePi_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodePi_MAX;
      break;

    case GsmIocDataInvertNibble:
      Min = DATA0CH0_CR_DDRCRDATACONTROL2_datainvertnibble_MIN;
      Max = DATA0CH0_CR_DDRCRDATACONTROL2_datainvertnibble_MAX;
      break;

    case GsmIocCapCancelCodeIdle:
    case GsmIocCapCancelCodeWrite:
    case GsmIocCapCancelCodeRead:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_CapCancelCodeIdle_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_CapCancelCodeIdle_MAX;
      break;

    case GsmIocCapCancelCodePBD:
    case GsmIocCapCancelCodePi:
// @todo_adl      Min = DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_CapCancelCodePBD_MIN;
// @todo_adl      Max = DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_CapCancelCodePBD_MAX;
      break;

    case GsmIocEnableSpineGate:
      Min = MCMISCS_SPINEGATING_enablespinegate_MIN;
      Max = MCMISCS_SPINEGATING_enablespinegate_MAX;
      break;

    case GsmIocWriteLevelMode:
      Min = DATA0CH0_CR_DATATRAINFEEDBACK_wltrainingmode_MIN;
      Max = DATA0CH0_CR_DATATRAINFEEDBACK_wltrainingmode_MAX;
      break;

    case GsmIocVccDllControlTarget_V:
// @todo_adl      Min = DDRVCCDLL0_CR_DDRCRVCCDLLCONTROL_Target_MIN;
// @todo_adl      Max = DDRVCCDLL0_CR_DDRCRVCCDLLCONTROL_Target_MAX;
      break;

    case GsmIocCccPiEn:
      Min = CH2CCC_CR_DDRCRPINSUSED_PiEn_MIN;
      Max = CH2CCC_CR_DDRCRPINSUSED_PiEn_MAX;
      break;

    case GsmIocClkPFallNRiseMode:
    case GsmIocClkPRiseNFallMode:
// @todo_adl      Min = CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_EnFeedback_MIN;
// @todo_adl      Max = CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_EnFeedback_MAX;
      break;

    case GsmIocClkPFallNRiseFeedback:
    case GsmIocClkPRiseNFallFeedback:
// @todo_adl      Min = CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_PfNrFeedback_MIN;
// @todo_adl      Max = CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_PfNrFeedback_MAX;
      break;

    case GsmIocClkPFallNRiseCcc56:
    case GsmIocClkPRiseNFallCcc56:
    case GsmIocClkPFallNRiseCcc78:
    case GsmIocClkPRiseNFallCcc78:
// @todo_adl      Min = CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_CCC56_MIN;
// @todo_adl      Max = CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_CCC56_MAX;
      break;

    case GsmIocDataDccLaneStatusResult:
    case GsmIocCccDccLaneStatusResult:
// @todo_adl      Min = DATA0CH0_CR_DCCLANESTATUS0_Lane0Result_MIN;
// @todo_adl      Max = DATA0CH0_CR_DCCLANESTATUS0_Lane0Result_MAX;
      break;

    case GsmIocDataDccStepSize:
    case GsmIocCccDccStepSize:
// @todo_adl      Min = DATA0CH0_CR_DCCCALCCONTROL_DccStepSize_MIN;
// @todo_adl      Max = DATA0CH0_CR_DCCCALCCONTROL_DccStepSize_MAX;
      break;

    case GsmIocCccDccCcc:
// @todo_adl      Min = CH0CCC_CR_DDRCRCCCPERBITDESKEW0_CCC0_MIN;
// @todo_adl      Max = CH0CCC_CR_DDRCRCCCPERBITDESKEW0_CCC0_MAX;
      break;

    case GsmIocDataDccMeasPoint:
// @todo_adl      Min = DATA0CH0_CR_DCCFSMCONTROL_MeasPoint_MIN;
// @todo_adl      Max = DATA0CH0_CR_DCCFSMCONTROL_MeasPoint_MAX;
      break;

    case GsmIocDataDccRankEn:
// @todo_adl      Min = DATA0CH0_CR_DCCFSMCONTROL_RankEn_MIN;
// @todo_adl      Max = DATA0CH0_CR_DCCFSMCONTROL_RankEn_MAX;
      break;

    case GsmIocDataDccLaneEn:
// @todo_adl      Min = DATA0CH0_CR_DCCFSMCONTROL_LaneEn_MIN;
// @todo_adl      Max = DATA0CH0_CR_DCCFSMCONTROL_LaneEn_MAX;
      break;

    case GsmIocRxSALTailCtrl:
// @todo_adl      Min = DATA0CH0_CR_DDRCRDATACONTROL3_RxSALTailCtrl_MIN;
// @todo_adl      Max = DATA0CH0_CR_DDRCRDATACONTROL3_RxSALTailCtrl_MAX;
      break;

    case GsmIocTxEqTapSelect:
      Min = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenntap_MIN;
      Max = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenntap_MAX;
      break;

    case GsmIocRetrainSwizzleCtlBit:
      Min = DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq0_MIN;
      Max = DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq0_MAX;
      break;

    case GsmIocRetrainInitPiCode:
      Min = DATA0CH0_CR_DDRCRWRRETRAINRANK3_initpicode_MIN;
      Max = DATA0CH0_CR_DDRCRWRRETRAINRANK3_initpicode_MAX;
      break;

    case GsmIocRetrainDeltaPiCode:
      Min = DATA0CH0_CR_DDRCRWRRETRAINRANK3_deltapicode_MIN;
      Max = DATA0CH0_CR_DDRCRWRRETRAINRANK3_deltapicode_MAX;
      break;

    case GsmIocRetrainRoCount:
      Min = DATA0CH0_CR_DDRCRWRRETRAINRANK3_rocount_MIN;
      Max = DATA0CH0_CR_DDRCRWRRETRAINRANK3_rocount_MAX;
      break;

    case GsmIocRetrainCtlDuration:
      Min = DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_duration_MIN;
      Max = DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_duration_MAX;
      break;

      // Binary Max/Min
    case GsmIocLocalGateD0tx:
    case GsmIocDataDccSaveFullDcc:
    case GsmIocDataDccSkipCRWrite:
    case GsmIocDataDcc2xStep:
    case GsmIocCccDcc2xStep:
    case GsmIocCccPiEnOverride:
    case GsmIocIoReset:
    case GsmIocForceCmpUpdt:
    case GsmIocScramLp4Mode:
    case GsmIocScramLp5Mode:
// @todo_adl    case GsmIocScramOvrdPeriodicToDvfsComp:
    case GsmIocLp5Wck2CkRatio:
    case GsmIocScramDdr4Mode:
    case GsmIocScramDdr5Mode:
    case GsmIocScramGear1:
    case GsmIocScramGear4:
    case GsmIocVccDllGear1:
    case GsmIocVccDllControlBypass_V:
    case GsmIocVccDllControlSelCode_V:
    case GsmIocVccDllControlOpenLoop_V:
    case GsmIocVsxHiControlSelCode_V:
    case GsmIocVsxHiControlOpenLoop_V:
    case GsmIocDisDataIdlClkGate:
    case GsmIocDisClkGate:
    case GsmIocDisIosfSbClkGate:
    case GsmIocEccEn:
    case GsmIocWrite0En:
    case GsmIocScramWrite0En:
    case GsmIocScramLpMode:
    case GsmIocNoDqInterleave:
//    case GsmIocVrefPwrDnEn:
    case GsmIocCompVddqOdtEn:
    case GsmIocCompVttOdtEn:
    case GsmIocReadDqDqsMode:
    case GsmIocSenseAmpMode:
    case GsmIocDqsRFMode:
    case GsmIocDdrDqRxSdlBypassEn:
    case GsmIocCaTrainingMode:
    case GsmIocDdrDqDrvEnOvrdModeEn:
    case GsmIocCaParityTrain:
    case GsmIocReadLevelMode:
    case GsmIocForceOdtOn:
    case GsmIocForceOdtOnWithoutDrvEn:
    case GsmIocTxOn:
    case GsmIocRxDisable:
    case GsmIocRxPiPwrDnDis:
    case GsmIocTxPiPwrDnDis:
    case GsmIocInternalClocksOn:
    case GsmIocDataDqsNParkLow:
    case GsmIocTxDisable:
    case GsmIocDataOdtStaticEn:
    case GsmIocCompOdtStaticDis:
    case GsmIocStrobeOdtStaticEn:
//    case GsmIocRxTypeSelect:
//    case GsmIocRxPathBiasSel:
    case GsmIocCmdVrefConverge:
    case GsmIocConstZTxEqEn:
    case GsmIocCompClkOn:
    case GsmIocDisableQuickComp:
    case GsmIocSinStep:
    case GsmIocSinStepAdv:
    case GsmIocDllWeakLock:
    case GsmIocDllWeakLock1:
    case GsmIocTxEqEn:
//    case GsmIocForceBiasOn:
    case GsmIocForceRxAmpOn:
    case GsmIocDqSlewDlyByPass:
    case GsmIocWlLongDelEn:
    case GsmIocTxDqFifoRdEnPerRankDelDis:
    case GsmIocRptChRepClkOn:
    case GsmIocRankOverrideEn:
    case GsmIocDataCtlGear1:
    case GsmIocDataCtlGear4:
    case GsmIocDccDcoCompEn:
    case GsmIocDccTrainingMode:
    case GsmIocDccTrainingDone:
    case GsmIocDccDrain:
// @todo_adl    case GsmIocRXDeskewForceOn:
    case GsmIocDataDqOdtParkMode:
    case GsmIocDataDqsOdtParkMode:
    case GsmIocEnDqsNRcvEn:
    case GsmIocEnDqsNRcvEnGate:
    case GsmIocRxMatchedPathEn:
    case GsmIocForceOnRcvEn:
    case GsmIocRetrainSwizzleCtlByteSel:
    case GsmIocRetrainSwizzleCtlRetrainEn:
    case GsmIocRetrainSwizzleCtlSerialMrr:
    case GsmIocRetrainCtlInitTrain:
    case GsmIocRetrainCtlResetStatus:
    case GsmIocDccFsmResetData:
    case GsmIocDccFsmResetCcc:
    case GsmIocDcdRoCalEnData:
    case GsmIocDcdRoCalEnCcc:
    case GsmIocDcdRoCalStartData:
    case GsmIocDcdRoCalStartCcc:
    case GsmIocDcdRoCalSampleCountRstData:
    case GsmIocDcdRoCalSampleCountRstCcc:
    case GsmIocDccStartData:
    case GsmIocDccStartCcc:
    case GsmIocSrzDcdEnData:
    case GsmIocSrzDcdEnCcc:
    case GsmIocMdllDcdEnData:
    case GsmIocMdllDcdEnCcc:
    case GsmIocDcdSampleCountRstData:
    case GsmIocDcdSampleCountStartData:
    case GsmIocTxpbdPh90incrCcc:
    case GsmIocTxpbdPh90incrData:
      Min = 0;
      Max = 1;
      break;
  }

  if ((Min == MRC_INT64_MIN) && (Max == MRC_INT64_MAX)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Group %s(%d) has no limits defined\n", GsmGtDebugStrings[Group], Group);
    Status = mrcWrongInputParameter;
  }

  // Null guard the pointers
  if (MinVal != NULL) {
    *MinVal = Min;
  }
  if (MaxVal != NULL) {
    *MaxVal = Max;
  }
  if (WaitTime != NULL) {
    *WaitTime = Wait;
  }

  return Status;
}

/**
  This function returns MC Timing group limits.

  @param[in]  MrcData   - Pointer to global data.
  @param[in]  Group     - DDRIO group to access.
  @param[out] MinVal    - Return pointer for Minimum Value supported.
  @param[out] MaxVal    - Return pointer for Maximum Value supported.
  @param[out] WaitTime  - Return pointer for settle time required in microseconds.

  @retval MrcStatus - mrcSuccess if the parameter is found, otherwise mrcFail.
**/
MrcStatus
MrcGetMcTimingGroupLimits (
  IN  MrcParameters *const MrcData,
  IN  GSM_GT  const   Group,
  OUT INT64   *const  MinVal,
  OUT INT64   *const  MaxVal,
  OUT UINT32  *const  WaitTime
  )
{
  MrcStatus      Status;
  MrcDebug       *Debug;
  INT64          Max;
  INT64          Min;
  UINT32         Wait;

  Debug   = &MrcData->Outputs.Debug;
  Status  = mrcSuccess;
  Wait    = 0;
  Min     = MRC_INT64_MIN;
  Max     = MRC_INT64_MAX;

  switch (Group) {
    case GsmMctRCD:
      Min = MC0_CH0_CR_TC_PRE_tRCD_MIN;
      Max = MC0_CH0_CR_TC_PRE_tRCD_MAX;
      break;
    case GsmMctRP:
      Min = MC0_CH0_CR_TC_PRE_tRP_MIN;
      Max = MC0_CH0_CR_TC_PRE_tRP_MAX;
      break;

    case GsmMctRFCpb:
      Min = MC0_CH0_CR_SC_PBR_tRFCpb_MIN;
      Max = MC0_CH0_CR_SC_PBR_tRFCpb_MAX;
      break;

    case GsmMctRefm:
      Min = MC0_CH0_CR_TC_REFM_tRFM_MIN;
      Max = MC0_CH0_CR_TC_REFM_tRFM_MAX;
      break;

    case GsmMctRPabExt:
      Min = MC0_CH0_CR_TC_PRE_tRPab_ext_MIN;
      Max = 8; //MC0_CH0_CR_TC_PRE_tRPab_ext_MAX
      break;

    case GsmMctRAS:
      Min = MC0_CH0_CR_TC_PRE_tRAS_MIN;
      Max = MC0_CH0_CR_TC_PRE_tRAS_MAX;
      break;

    case GsmMctRDPRE:
      Min = MC0_CH0_CR_TC_PRE_tRDPRE_MIN;
      Max = MC0_CH0_CR_TC_PRE_tRDPRE_MAX;
      break;

    case GsmMctPPD:
      Min = MC0_CH0_CR_TC_PRE_tPPD_MIN;
      Max = MC0_CH0_CR_TC_PRE_tPPD_MAX;
      break;

    case GsmMctWRPRE:
      Min = MC0_CH0_CR_TC_PRE_tWRPRE_MIN;
      Max = MC0_CH0_CR_TC_PRE_tWRPRE_MAX;
      break;

    case GsmMctFAW:
      Min = MC0_CH0_CR_TC_ACT_tFAW_MIN;
      Max = MC0_CH0_CR_TC_ACT_tFAW_MAX;
      break;

    case GsmMctRRDsg:
      Min = MC0_CH0_CR_TC_ACT_tRRD_sg_MIN;
      Max = MC0_CH0_CR_TC_ACT_tRRD_sg_MAX;
      break;

    case GsmMctRRDdg:
      Min = MC0_CH0_CR_TC_ACT_tRRD_dg_MIN;
      Max = MC0_CH0_CR_TC_ACT_tRRD_dg_MAX;
      break;

    case GsmMctREFSbRd:
      Min = MC0_CH0_CR_TC_ACT_trefsbrd_MIN;
      Max = MrcData->Inputs.A0 ? MC0_CH0_CR_TC_ACT_trefsbrd_A0_MAX : MC0_CH0_CR_TC_ACT_trefsbrd_MAX;
      break;

    case GsmMctLpDeratingExt:
      Min = MC0_CH0_CR_TC_PRE_derating_ext_MIN;
      Max = MC0_CH0_CR_TC_PRE_derating_ext_MAX;
      break;

    case GsmMctRDRDsg:
      Min = MC0_CH0_CR_TC_RDRD_tRDRD_sg_MIN;
      Max = MC0_CH0_CR_TC_RDRD_tRDRD_sg_MAX;
      break;

    case GsmMctRDRDdg:
      Min = MC0_CH0_CR_TC_RDRD_tRDRD_dg_MIN;
      Max = MC0_CH0_CR_TC_RDRD_tRDRD_dg_MAX;
      break;

    case GsmMctRDRDdr:
      Min = MC0_CH0_CR_TC_RDRD_tRDRD_dr_MIN;
      Max = MC0_CH0_CR_TC_RDRD_tRDRD_dr_MAX;
      break;

    case GsmMctRDRDdd:
      Min = MC0_CH0_CR_TC_RDRD_tRDRD_dd_MIN;
      Max = MC0_CH0_CR_TC_RDRD_tRDRD_dd_MAX;
      break;

    case GsmMctRDWRsg:
      Min = MC0_CH0_CR_TC_RDWR_tRDWR_sg_MIN;
      Max = MC0_CH0_CR_TC_RDWR_tRDWR_sg_MAX;
      break;

    case GsmMctRDWRdg:
      Min = MC0_CH0_CR_TC_RDWR_tRDWR_dg_MIN;
      Max = MC0_CH0_CR_TC_RDWR_tRDWR_dg_MAX;
      break;

    case GsmMctRDWRdr:
      Min = MC0_CH0_CR_TC_RDWR_tRDWR_dr_MIN;
      Max = MC0_CH0_CR_TC_RDWR_tRDWR_dr_MAX;
      break;

    case GsmMctRDWRdd:
      Min = MC0_CH0_CR_TC_RDWR_tRDWR_dd_MIN;
      Max = MC0_CH0_CR_TC_RDWR_tRDWR_dd_MAX;
      break;

    case GsmMctWRRDsg:
      Min = MC0_CH0_CR_TC_WRRD_tWRRD_sg_MIN;
      Max = MC0_CH0_CR_TC_WRRD_tWRRD_sg_MAX;
      break;

    case GsmMctWRRDdg:
      Min = MC0_CH0_CR_TC_WRRD_tWRRD_dg_MIN;
      Max = MC0_CH0_CR_TC_WRRD_tWRRD_dg_MAX;
      break;

    case GsmMctWRRDdr:
      Min = MC0_CH0_CR_TC_WRRD_tWRRD_dr_MIN;
      Max = MC0_CH0_CR_TC_WRRD_tWRRD_dr_MAX;
      break;

    case GsmMctWRRDdd:
      Min = MC0_CH0_CR_TC_WRRD_tWRRD_dd_MIN;
      Max = MC0_CH0_CR_TC_WRRD_tWRRD_dd_MAX;
      break;

    case GsmMctWRWRsg:
      Min = MC0_CH0_CR_TC_WRWR_tWRWR_sg_MIN;
      Max = MC0_CH0_CR_TC_WRWR_tWRWR_sg_MAX;
      break;

    case GsmMctWRWRdg:
      Min = MC0_CH0_CR_TC_WRWR_tWRWR_dg_MIN;
      Max = MC0_CH0_CR_TC_WRWR_tWRWR_dg_MAX;
      break;

    case GsmMctWRWRdr:
      Min = MC0_CH0_CR_TC_WRWR_tWRWR_dr_MIN;
      Max = MC0_CH0_CR_TC_WRWR_tWRWR_dr_MAX;
      break;

    case GsmMctWRWRdd:
      Min = MC0_CH0_CR_TC_WRWR_tWRWR_dd_MIN;
      Max = MC0_CH0_CR_TC_WRWR_tWRWR_dd_MAX;
      break;

    case GsmMctOdtRdDuration:
      Min = MC0_CH0_CR_TC_ODT_ODT_read_duration_MIN;
      Max = MC0_CH0_CR_TC_ODT_ODT_read_duration_MAX;
      break;

    case GsmMctOdtRdDelay:
      Min = MC0_CH0_CR_TC_ODT_ODT_Read_Delay_MIN;
      Max = MC0_CH0_CR_TC_ODT_ODT_Read_Delay_MAX;
      break;

    case GsmMctOdtWrDuration:
      Min = MC0_CH0_CR_TC_ODT_ODT_write_duration_MIN;
      Max = MC0_CH0_CR_TC_ODT_ODT_write_duration_MAX;
      break;

    case GsmMctOdtWrDelay:
      Min = MC0_CH0_CR_TC_ODT_ODT_Write_Delay_MIN;
      Max = MC0_CH0_CR_TC_ODT_ODT_Write_Delay_MAX;
      break;

    case GsmMctCL:
      Min = MC0_CH0_CR_TC_ODT_tCL_MIN;
      Max = MC0_CH0_CR_TC_ODT_tCL_MAX;
      break;

    case GsmMctCWL:
      Min = MC0_CH0_CR_TC_ODT_tCWL_MIN;
      Max = MC0_CH0_CR_TC_ODT_tCWL_MAX;
      break;

    case GsmMctCWLAdd:
      Min = MC0_CH0_CR_SC_WR_DELAY_Add_tCWL_MIN;
      Max = MC0_CH0_CR_SC_WR_DELAY_Add_tCWL_MAX;
      break;

    case GsmMctCWLDec:
      Min = MC0_CH0_CR_SC_WR_DELAY_Dec_tCWL_MIN;
      Max = MC0_CH0_CR_SC_WR_DELAY_Dec_tCWL_MAX;
      break;

    case GsmMctAONPD:
      Min = MC0_CH0_CR_TC_ODT_tAONPD_MIN;
      Max = MC0_CH0_CR_TC_ODT_tAONPD_MAX;
      break;

    case GsmMctCKE:
      Min = MC0_CH0_CR_TC_PWRDN_tCKE_MIN;
      Max = MC0_CH0_CR_TC_PWRDN_tCKE_MAX;
      break;

    case GsmMctXP:
      Min = MC0_CH0_CR_TC_PWRDN_tXP_MIN;
      Max = MC0_CH0_CR_TC_PWRDN_tXP_MAX;
      break;

    case GsmMctXPDLL:
      Min = MC0_CH0_CR_TC_PWRDN_tXPDLL_MIN;
      Max = MC0_CH0_CR_TC_PWRDN_tXPDLL_MAX;
      break;

    case GsmMctPRPDEN:
      Min = MC0_CH0_CR_TC_PWRDN_tPRPDEN_MIN;
      Max = MC0_CH0_CR_TC_PWRDN_tPRPDEN_MAX;
      break;

    case GsmMctRDPDEN:
      Min = MC0_CH0_CR_TC_PWRDN_tRDPDEN_MIN;
      Max = MC0_CH0_CR_TC_PWRDN_tRDPDEN_MAX;
      break;

    case GsmMctWRPDEN:
      Min = MC0_CH0_CR_TC_PWRDN_tWRPDEN_MIN;
      Max = MC0_CH0_CR_TC_PWRDN_tWRPDEN_MAX;
      break;

    case GsmMctCA2CS:
      Min = MC0_CH0_CR_TC_PWRDN_tCA2CS_MIN;
      Max = MC0_CH0_CR_TC_PWRDN_tCA2CS_MAX;
      break;

    case GsmMctCSL:
      Min = MC0_CH0_CR_TC_PWRDN_tCSL_MIN;
      Max = MC0_CH0_CR_TC_PWRDN_tCSL_MAX;
      break;

    case GsmMctCSH:
      Min = MC0_CH0_CR_TC_PWRDN_tCSH_MIN;
      Max = MC0_CH0_CR_TC_PWRDN_tCSH_MAX;
      break;

    case GsmMctXSDLL:
      Min = MC0_CH0_CR_TC_SRFTP_tXSDLL_MIN;
      Max = MC0_CH0_CR_TC_SRFTP_tXSDLL_MAX;
      break;

    case GsmMctXSR:
      Min = MC0_CH0_CR_TC_SREXITTP_tXSR_MIN;
      Max = MC0_CH0_CR_TC_SREXITTP_tXSR_MAX;
      break;

    case GsmMctSR:
      Min = MC0_CH0_CR_TC_SREXITTP_tSR_MIN;
      Max = MC0_CH0_CR_TC_SREXITTP_tSR_MAX;
      break;

    case GsmMctZQOPER:
      Min = MC0_CH0_CR_TC_SRFTP_tZQOPER_MIN;
      Max = MC0_CH0_CR_TC_SRFTP_tZQOPER_MAX;
      break;

    case GsmMctMOD:
      Min = MC0_CH0_CR_TC_SRFTP_tMOD_MIN;
      Max = MC0_CH0_CR_TC_SRFTP_tMOD_MAX;
      break;

    case GsmMctZQCAL:
      Min = MC0_CH0_CR_TC_ZQCAL_tZQCAL_MIN;
      Max = MC0_CH0_CR_TC_ZQCAL_tZQCAL_MAX;
      break;

    case GsmMctZQCS:
      Min = MC0_CH0_CR_TC_ZQCAL_tZQCS_MIN;
      Max = MC0_CH0_CR_TC_ZQCAL_tZQCS_MAX;
      break;

    case GsmMctZQCSPeriod:
      Min = MC0_CH0_CR_TC_ZQCAL_ZQCS_period_MIN;
      Max = MC0_CH0_CR_TC_ZQCAL_ZQCS_period_MAX;
      break;

    case GsmMctCPDED:
      Min = MC0_CH0_CR_SC_GS_CFG_tCPDED_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_tCPDED_MAX;
      break;

    case GsmMctCAL:
      Min = MC0_CH0_CR_SC_GS_CFG_tCAL_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_tCAL_MAX;
      break;

    case GsmMctCKCKEH:
      Min = MC0_CH0_CR_SC_GS_CFG_ck_to_cke_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_ck_to_cke_MAX;
      break;

    case GsmMctCSCKEH:
      Min = MC0_CH0_CR_SC_GS_CFG_cs_to_cke_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_cs_to_cke_MAX;
      break;

    case GsmMctSrIdle:
      Min = MC0_PM_SREF_CONFIG_Idle_timer_MIN;
      Max = MC0_PM_SREF_CONFIG_Idle_timer_MAX;
      break;

    case GsmMctREFI:
      Min = MC0_CH0_CR_TC_RFTP_tREFI_MIN;
      Max = MC0_CH0_CR_TC_RFTP_tREFI_MAX;
      break;

    case GsmMctRFC:
      Min = MC0_CH0_CR_TC_RFTP_tRFC_MIN;
      Max = MC0_CH0_CR_TC_RFTP_tRFC_MAX;
      break;

    case GsmMctOrefRi:
      Min = MC0_CH0_CR_TC_RFP_OREF_RI_MIN;
      Max = MC0_CH0_CR_TC_RFP_OREF_RI_MAX;
      break;

    case GsmMctRefreshHpWm:
      Min = MC0_CH0_CR_TC_RFP_Refresh_HP_WM_MIN;
      Max = MC0_CH0_CR_TC_RFP_Refresh_HP_WM_MAX;
      break;

    case GsmMctRefreshPanicWm:
      Min = MC0_CH0_CR_TC_RFP_Refresh_panic_wm_MIN;
      Max = MC0_CH0_CR_TC_RFP_Refresh_panic_wm_MAX;
      break;

    case GsmMctREFIx9:
      Min = MC0_CH0_CR_TC_RFP_tREFIx9_MIN;
      Max = MC0_CH0_CR_TC_RFP_tREFIx9_MAX;
      break;

    case GsmMctSrxRefDebits:
      Min = MC0_CH0_CR_TC_RFP_SRX_Ref_Debits_MIN;
      Max = MC0_CH0_CR_TC_RFP_SRX_Ref_Debits_MAX;
      break;

      // Group all the Boolean Variables here
    case GsmMctWrEarlyOdt:
    case GsmMctHpRefOnMrs:
      Min = 0;
      Max = 1;
      break;

    default:
      Status = mrcWrongInputParameter;
      break;
  }

  if ((Min == MRC_INT64_MIN) && (Max == MRC_INT64_MAX)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Group %s(%d) has no limits defined\n", GsmGtDebugStrings[Group], Group);
    Status = mrcWrongInputParameter;
  }

  // Null guard the pointers
  if (MinVal != NULL) {
    *MinVal = Min;
  }
  if (MaxVal != NULL) {
    *MaxVal = Max;
  }
  if (WaitTime != NULL) {
    *WaitTime = Wait;
  }

  return Status;
}

/**
  This function returns MC Config register limits.

  @param[in]  MrcData   - Pointer to global data.
  @param[in]  Group     - DDRIO group to access.
  @param[out] MinVal    - Return pointer for Minimum Value supported.
  @param[out] MaxVal    - Return pointer for Maximum Value supported.
  @param[out] WaitTime  - Return pointer for settle time required in microseconds.

  @retval MrcStatus - mrcSuccess if the parameter is found, otherwise mrcFail.
**/
MrcStatus
MrcGetMcConfigGroupLimits (
  IN  MrcParameters *const MrcData,
  IN  GSM_GT  const   Group,
  OUT INT64   *const  MinVal,
  OUT INT64   *const  MaxVal,
  OUT UINT32  *const  WaitTime
  )
{
  MrcStatus      Status;
  MrcDebug       *Debug;
  INT64          Max;
  INT64          Min;
  UINT32         Wait;

  Debug        = &MrcData->Outputs.Debug;
  Status       = mrcSuccess;
  Wait         = 0;
  Min          = MRC_INT64_MIN;
  Max          = MRC_INT64_MAX;

  switch (Group) {
    case GsmMccDramType:
      Min = MC0_CH0_CR_SC_GS_CFG_DRAM_technology_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_DRAM_technology_MAX;
      break;

    case GsmMccCmdStretch:
      Min = MC0_CH0_CR_SC_GS_CFG_CMD_stretch_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_CMD_stretch_MAX;
      break;

    case GsmMccCmdGapRatio:
      Min = MC0_CH0_CR_SC_GS_CFG_N_to_1_ratio_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_N_to_1_ratio_MAX;
      break;

    case GsmMccAddrMirror:
      Min = MC0_CH0_CR_SC_GS_CFG_Address_mirror_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_Address_mirror_MAX;
      break;

    case GsmMccFreqPoint:
      Min = MC0_CH0_CR_SC_GS_CFG_frequency_point_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_frequency_point_MAX;
      break;

    case GsmMccLDimmMap:
      Min = MC0_MAD_INTRA_CH0_DIMM_L_MAP_MIN;
      Max = MC0_MAD_INTRA_CH0_DIMM_L_MAP_MAX;
      break;

    case GsmMccEccMode:
      Min = MC0_MAD_INTRA_CH0_ECC_MIN;
      Max = MC0_MAD_INTRA_CH0_ECC_MAX;
      // Wait 4 usec after enabling the ECC IO, needed by HW
      Wait = 4;
      break;

    case GsmMccAddrDecodeDdrType:
      Min = MC0_MAD_INTER_CHANNEL_DDR_TYPE_MIN;
      Max = MC0_MAD_INTER_CHANNEL_DDR_TYPE_MAX;
      break;

    case GsmMccSChannelSize:
      Min = MC0_MAD_INTER_CHANNEL_CH_S_SIZE_MIN;
      Max = MC0_MAD_INTER_CHANNEL_CH_S_SIZE_MAX;
      break;

    case GsmMccChWidth:
      Min = MC0_MAD_INTER_CHANNEL_CH_WIDTH_MIN;
      Max = MC0_MAD_INTER_CHANNEL_CH_WIDTH_MAX;
      break;

    case GsmMccHalfCachelineMode:
      Min = MC0_MAD_INTER_CHANNEL_HalfCacheLineMode_MIN;
      Max = MC0_MAD_INTER_CHANNEL_HalfCacheLineMode_MAX;
      break;

    case GsmMccLDimmSize:
      Min = MC0_MAD_DIMM_CH0_DIMM_L_SIZE_MIN;
      Max = MC0_MAD_DIMM_CH0_DIMM_L_SIZE_MAX;
      break;

    case GsmMccLDimmDramWidth:
      Min = MC0_MAD_DIMM_CH0_DLW_MIN;
      Max = MC0_MAD_DIMM_CH0_DLW_MAX;
      break;

    case GsmMccLDimmRankCnt:
      Min = MC0_MAD_DIMM_CH0_DLNOR_MIN;
      Max = MC0_MAD_DIMM_CH0_DLNOR_MAX;
      break;

    case GsmMccSDimmSize:
      Min = MC0_MAD_DIMM_CH0_DIMM_S_SIZE_MIN;
      Max = MC0_MAD_DIMM_CH0_DIMM_S_SIZE_MAX;
      break;

    case GsmMccSDimmDramWidth:
      Min = MC0_MAD_DIMM_CH0_DSW_MIN;
      Max = MC0_MAD_DIMM_CH0_DSW_MAX;
      break;

    case GsmMccSDimmRankCnt:
      Min = MC0_MAD_DIMM_CH0_DSNOR_MIN;
      Max = MC0_MAD_DIMM_CH0_DSNOR_MAX;
      break;

    case GsmMccExtendedBankHashing:
      Min = MC0_MAD_DIMM_CH0_Decoder_EBH_MIN;
      Max = MC0_MAD_DIMM_CH0_Decoder_EBH_MAX;
      break;

    case GsmMccResetOnCmd:
      Min = MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_on_command_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_on_command_MAX;
      break;

    case GsmMccResetDelay:
      Min = MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_delay_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_delay_MAX;
      break;

    case GsmMccRankOccupancy:
      Min = MC0_CH0_CR_MC_INIT_STATE_Rank_occupancy_MIN;
      Max = MC0_CH0_CR_MC_INIT_STATE_Rank_occupancy_MAX;
      break;

    case GsmMccRefInterval:
      Min = MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Interval_MIN;
      Max = MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Interval_MAX;
      break;

    case GsmMccRefreshAbrRelease:
      Min = MC0_CH0_CR_SC_PBR_Refresh_ABR_release_MIN;
      Max = MC0_CH0_CR_SC_PBR_Refresh_ABR_release_MAX;
      break;

    case GsmMccHashMask:
      Min = MC0_CHANNEL_HASH_HASH_MASK_MIN;
      Max = MC0_CHANNEL_HASH_HASH_MASK_MAX;
      break;

    case GsmMccLsbMaskBit:
      Min = MC0_CHANNEL_HASH_HASH_LSB_MASK_BIT_MIN;
      Max = MC0_CHANNEL_HASH_HASH_LSB_MASK_BIT_MAX;
      break;

    case GsmMccX8Device:
      Min = MC0_CH0_CR_SC_GS_CFG_x8_device_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_x8_device_MAX;
      break;

    case GsmMccDdr4OneDpc:
      Min = MC0_CH0_CR_SC_GS_CFG_ddr_1dpc_split_ranks_on_subch_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_ddr_1dpc_split_ranks_on_subch_MAX;
      break;

    case GsmMccOdtOverride:
    case GsmMccOdtOn:
      Min = MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_Override_MIN;
      Max = MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_Override_MAX;
      break;

    case GsmMccCkeOverride:
    case GsmMccCkeOn:
    case GsmMccCsOverride:
    case GsmMccCsOverrideVal:
      Min = MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_MIN;
      Max = MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_MAX;
      break;

    case GsmMccRrdValidValue:
      Min = MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_value_MIN;
      Max = MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_value_MAX;
      break;

    case GsmMccDeswizzleByte:
      Min = MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_MIN;
      Max = MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_MAX;
      break;

    case GsmMccDeswizzleBit:
      Min = MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_0_MIN;
      Max = MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_0_MAX;
      break;

    case GsmMccLp5BankMode:
      Min = MC0_CH0_CR_SC_GS_CFG_lpddr5_bg_mode_MIN;
      Max = MC0_CH0_CR_SC_GS_CFG_lpddr5_bg_mode_MAX;
      break;
    // For boolean parameters, we will group them here as their Min/Max is always 0/1
    case GsmMccGear2:
    case GsmMccGear4:
    case GsmMccMultiCycCmd:
    case GsmMccWrite0En:
    case GsmMccWCKDiffLowInIdle:
    case GsmMccCpgcInOrder:
    case GsmMccInOrderIngress:
    case GsmMccCadbEnable:
    case GsmMccDeselectEnable:
    case GsmMccBusRetainOnBubble:
    case GsmMccLp4FifoRdWr:
    case GsmMccIgnoreCke:
    case GsmMccMaskCs:
    case GsmMccBlockXarb:
    case GsmMccBlockCke:
    case GsmMccEnableOdtMatrix:
    case GsmMccCmdTriStateDis:
    case GsmMccCmdTriStateDisTrain:
    case GsmMccEnhancedInterleave:
    case GsmMccLChannelMap:
    case GsmMccSaveFreqPoint:
    case GsmMccEnableRefresh:
    case GsmMccEnableSr:
    case GsmMccMcInitDoneAck:
    case GsmMccMrcDone:
    case GsmMccEnableDclk:
    case GsmMccPureSrx:
    case GsmMccMcSrx:
    case GsmMccHashMode:
    case GsmMccRefStaggerEn:
    case GsmMccRefStaggerMode:
    case GsmMccDisableStolenRefresh:
    case GsmMccEnRefTypeDisplay:
    case GsmMccDisableCkTristate:
    case GsmMccPbrDis:
    case GsmMccPbrIssueNop:
    case GsmMccPbrDisOnHot:
    case GsmMccMprTrainDdrOn:
    case GsmMccRrdValidTrigger:
    case GsmMccRrdValidOverflow:
    case GsmMccRrdValidSign:
    case GsmMccDdr5Device8GbDimmL:
    case GsmMccDdr5Device8GbDimmS:
      Min = 0;
      Max = 1;
      break;

    // These are unused.  Need to finish HAL if these fields are accessed.
    case GsmMccDdrReset:
    case GsmMccSafeSr:
    default:
      break;
  }

  if ((Min == MRC_INT64_MIN) && (Max == MRC_INT64_MAX)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Group %s(%d) has no limits defined\n", GsmGtDebugStrings[Group], Group);
    Status = mrcWrongInputParameter;
  }

  // Null guard the pointers
  if (MinVal != NULL) {
    *MinVal = Min;
  }
  if (MaxVal != NULL) {
    *MaxVal = Max;
  }
  if (WaitTime != NULL) {
    *WaitTime = Wait;
  }

  return Status;
}

/**
  This function returns CMI register limits.

  @param[in]  MrcData   - Pointer to global data.
  @param[in]  Group     - CMI group to access.
  @param[out] MinVal    - Return pointer for Minimum Value supported.
  @param[out] MaxVal    - Return pointer for Maximum Value supported.
  @param[out] WaitTime  - Return pointer for settle time required in microseconds.

  @retval MrcStatus
**/
MrcStatus
MrcGetCmiGroupLimits (
  IN  MrcParameters *const MrcData,
  IN  GSM_GT  const   Group,
  OUT INT64   *const  MinVal,
  OUT INT64   *const  MaxVal,
  OUT UINT32  *const  WaitTime
  )
{
  MrcStatus Status;
  MrcDebug  *Debug;
  INT64     Max;
  INT64     Min;
  UINT32    Wait;

  Debug   = &MrcData->Outputs.Debug;
  Status  = mrcSuccess;
  Wait    = 0;
  Min     = MRC_INT64_MIN;
  Max     = MRC_INT64_MAX;

  switch (Group) {
    case GsmCmiMcOrg:
      Min = CMI_MEMORY_SLICE_HASH_MC_org_MIN;
      Max = CMI_MEMORY_SLICE_HASH_MC_org_MAX;
      break;

    case GsmCmiHashMask:
      Min = CMI_MEMORY_SLICE_HASH_HASH_MASK_MIN;
      Max = CMI_MEMORY_SLICE_HASH_HASH_MASK_MAX;
      break;

    case GsmCmiLsbMaskBit:
      Min = CMI_MEMORY_SLICE_HASH_HASH_LSB_MASK_BIT_MIN;
      Max = CMI_MEMORY_SLICE_HASH_HASH_LSB_MASK_BIT_MAX;
      break;

    case GsmCmiSMadSliceSize:
      Min = CMI_MAD_SLICE_MS_S_SIZE_MIN;
      Max = CMI_MAD_SLICE_MS_S_SIZE_MAX;
      break;

    case GsmCmiStackedMsHash:
      Min = CMI_MAD_SLICE_STKD_MODE_MS_BITS_MIN;
      Max = CMI_MAD_SLICE_STKD_MODE_MS_BITS_MAX;
      break;

    // For boolean parameters, we will group them here as their Min/Max is always 0/1
    case GsmCmiStackedMode:
    case GsmCmiStackMsMap:
    case GsmCmiLMadSliceMap:
    case GsmCmiSliceDisable:
      Min = 0;
      Max = 1;
      break;

    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Group %s(%d) has no limits defined\n", GsmGtDebugStrings[Group], Group);
      Status = mrcWrongInputParameter;
      break;
  }

  // Null guard the pointers
  if (MinVal != NULL) {
    *MinVal = Min;
  }
  if (MaxVal != NULL) {
    *MaxVal = Max;
  }
  if (WaitTime != NULL) {
    *WaitTime = Wait;
  }

  return Status;
}

/**
  This function generates the hash used to execute the Get/Set function.
  The hash consists of: Register Offset, BitField start bit, BitField length.

  @param[in]  MrcData     - Pointer to global data.
  @param[in]  Group       - DDRIO group to access.
  @param[in]  Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Controller  - Memory controller in the processor socket (0-based).
  @param[in]  Channel     - DDR Channel Number within the memory controller (0-based).
  @param[in]  Rank        - Rank number within a channel (0-based).
  @param[in]  Strobe      - Dqs data group within the rank (0-based).
  @param[in]  Bit         - Bit index within the data group (0-based).
  @param[in]  FreqIndex   - Index supporting multiple operating frequencies. (Not used in Client CPU's)
  @param[out] HashVal     - Pointer to return the MMIO access instruction.

  @retval MrcStatus
**/
MrcStatus
MrcGetDdrIoHash (
  IN  MrcParameters *const  MrcData,
  IN  GSM_GT  const   Group,
  IN  UINT32  const   Socket,
  IN  UINT32  const   Controller,
  IN  UINT32  const   Channel,
  IN  UINT32  const   Rank,
  IN  UINT32  const   Strobe,
  IN  UINT32  const   Bit,
  IN  UINT32  const   FreqIndex,
  OUT UINT32  *const  HashVal
  )
{
  MrcDebug   *Debug;
  MrcOutput   *Outputs;
  MrcStatus   Status;
  MrcDdrType  DdrType;
  UINT32      CrOffset;
  UINT32      LogicalIndex;
  UINT32      ChMod2;
  BOOLEAN     DtHalo;
  BOOLEAN     A0;
  BOOLEAN     Ddr4;
  BOOLEAN     Ddr5;
  BOOLEAN     Lpddr4;
  BOOLEAN     Lpddr5;
  BOOLEAN     Gear2;
  BOOLEAN     Gear4;
  UINT32      Lp5CccConfig;
  MRC_REGISTER_HASH_STRUCT *HashPtr;

  static const UINT32 RoundtripDelayHsh[ROUNDTRIP_DELAY_MAX] = {
    MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_0_latency_HSH, MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_1_latency_HSH,
    MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_2_latency_HSH, MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_3_latency_HSH,
    MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_4_latency_HSH, MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_5_latency_HSH,
    MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_6_latency_HSH, MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_7_latency_HSH
  };
  static const UINT32 RxFlybyDelayHsh[2][MAX_RANK_IN_CHANNEL] = {
    {MCMISCS_READCFGCH01_rcvenrank0chadel_HSH, MCMISCS_READCFGCH01_rcvenrank1chadel_HSH,
     MCMISCS_READCFGCH01_rcvenrank2chadel_HSH, MCMISCS_READCFGCH01_rcvenrank3chadel_HSH}, // Channel 0
    {MCMISCS_READCFGCH01_rcvenrank0chbdel_HSH, MCMISCS_READCFGCH01_rcvenrank1chbdel_HSH,
     MCMISCS_READCFGCH01_rcvenrank2chbdel_HSH, MCMISCS_READCFGCH01_rcvenrank3chbdel_HSH}  // Channel 1
  };
  static const UINT32 RxFifoRdyDelay[2][MAX_RANK_IN_CHANNEL] ={
    {MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_HSH, MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chadel_HSH,
     MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chadel_HSH, MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chadel_HSH}, // Channel 0
    {MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chbdel_HSH, MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank1chbdel_HSH,
     MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank2chbdel_HSH, MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank3chbdel_HSH}  // Channel 1
  };
  static const UINT32 TxDqFifoRdEnFlyby[2][MAX_RANK_IN_CHANNEL] = {
    {MCMISCS_WRITECFGCH01_txdqfifordenrank0chadel_HSH, MCMISCS_WRITECFGCH01_txdqfifordenrank1chadel_HSH,
     MCMISCS_WRITECFGCH01_txdqfifordenrank2chadel_HSH, MCMISCS_WRITECFGCH01_txdqfifordenrank3chadel_HSH}, // Channel 0
    {MCMISCS_WRITECFGCH01_txdqfifordenrank0chbdel_HSH, MCMISCS_WRITECFGCH01_txdqfifordenrank1chbdel_HSH,
     MCMISCS_WRITECFGCH01_txdqfifordenrank2chbdel_HSH, MCMISCS_WRITECFGCH01_txdqfifordenrank3chbdel_HSH}  // Channel 1
  };
  static const UINT32 CaVrefHsh[MAX_DIMMS_IN_CHANNEL] = {  // CaVref is per DIMM
    DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca0vref_HSH,
    DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_ca1vref_HSH
  };
  static const UINT32 CkeCccHshDdr4[MAX_RANK_IN_CHANNEL] = {
    CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH, CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH,
    CH2CCC_CR_PICODE3_mdll_cmn_pi6code_HSH, CH2CCC_CR_PICODE2_mdll_cmn_pi4code_HSH
  };
  static const UINT32 CtlCccHshDdr4Hs[MAX_RANK_IN_CHANNEL] = {
    CH2CCC_CR_PICODE3_mdll_cmn_pi6code_HSH, CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH,
    CH2CCC_CR_PICODE2_mdll_cmn_pi4code_HSH, CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH
  };
  static const UINT32 CtlCcc0123HshDdr5Hs[MAX_RANK_IN_CHANNEL] = {
    CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH, CH2CCC_CR_PICODE3_mdll_cmn_pi6code_HSH,
    CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH, CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH
  };
  static const UINT32 CtlCcc4567HshDdr5Hs[MAX_RANK_IN_CHANNEL] = {
    CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH, CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH,
    CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH, CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH
  };

  static const UINT32 TxDqsPerBitDeskewHash[MAX_RANK_IN_CHANNEL] = {
    DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk0_HSH,
    DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk1_HSH,
    DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk2_HSH,
    DATA0CH0_CR_DDRDATADQSRANKX_txdqsperbitdeskew_rk3_HSH
  };

  Debug     = &MrcData->Outputs.Debug;
  Status    = mrcSuccess;
  HashPtr   = (MRC_REGISTER_HASH_STRUCT *) HashVal;
  HashPtr->Data = MRC_UINT32_MAX;
  CrOffset  = MRC_UINT32_MAX;
  Outputs   = &MrcData->Outputs;
  DdrType   = Outputs->DdrType;
  DtHalo    = MrcData->Inputs.DtHalo;
  A0        = MrcData->Inputs.A0;
  Lpddr4    = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5    = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4      = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5      = (DdrType == MRC_DDR_TYPE_DDR5);
  ChMod2    = Channel % 2;
  Gear2     = Outputs->Gear2;
  Gear4     = Outputs->Gear4;
  Lp5CccConfig = MrcData->Inputs.Lp5CccConfig;

  if ((Lpddr4 && (Outputs->Frequency == f1600)) || (Lpddr5 && (Outputs->Frequency == f1100))) {
    Gear2 = FALSE;
  }

  switch (Group) {
    case RecEnDelay:
      HashPtr->Data = DATA0CH0_CR_RXCONTROL0RANK0_rxrcvenpi_HSH;
      MrcGetSetUpdatePiHashPerGear (Gear4, Gear2, HashPtr);
      break;

    case RxDqsNDelay:
      HashPtr->Data = DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsnpicode_HSH;
      break;

    case CompRcompOdtUpPerStrobe:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_HSH;
      break;

    case CompRcompOdtDnPerStrobe:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_HSH;
      break;

    case RxDqsPDelay:
      HashPtr->Data = DATA0CH0_CR_RXCONTROL0RANK0_rxsdldqsppicode_HSH;
      break;

    case RxVref:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL3_rxvref_HSH;
      break;

    case RxVoc:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRDATADQRANK0LANE0_RxDQVrefOffset0_HSH; //VrefOffset1?
      break;

    case RxVocUnmatched:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRDATADQRANK0LANE0_RxDQVrefOffset1_HSH;
      break;

    case RxTap1:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap1_HSH;
      break;

    case RxTap2:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap2_HSH;
      break;

    case RxTap3:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap3_HSH;
      break;

    case RxTap4:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfetap4_HSH;
      break;

    case RxTap1En:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe1en_HSH;
      break;

    case RxTap2En:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe2en_HSH;
      break;

    case RxTap3En:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxudfe3en_HSH;
      break;

    case RxC:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctlecap_HSH;
      break;

    case RxR:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleres_HSH;
      break;

    case RxEq:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL0_dqrx_cmn_rxmctleeq_HSH;
      break;

    case RxDqsEq:
      HashPtr->Data = DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxmctleeq_HSH;
      break;

    case RxRankMuxDelay:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL1_rxrankmuxdelay_HSH;
      break;

    case RxDqsNBitDelay:
      HashPtr->Data = DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycoden_HSH;
      break;

    case RxDqsPBitDelay:
      HashPtr->Data = DATA0CH0_CR_DDRDATADQRANK0LANE0_rxpbddlycodep_HSH;
      break;

    case RxDqsBitOffset:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRDATADQRANK0LANE0_RxDQPerBitDeskewOffset_HSH;
      break;

    case TxDqsDelay:
      HashPtr->Data = DATA0CH0_CR_TXCONTROL0RANK0_txdqsdelay_HSH;
      MrcGetSetUpdatePiHashPerGear (Gear4, Gear2, HashPtr);
      break;

    case TxDqsDelay90:
      if (Rank == 0) {
        HashPtr->Data = DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk0_HSH;
      } else if (Rank == 1) {
        HashPtr->Data = DATA0CH0_CR_TXCONTROL0_PH90_txdqsdelay_rk1_HSH;
      } else if (Rank == 2) {
        HashPtr->Data = DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk2_HSH;
      } else if (Rank == 3) {
        HashPtr->Data = DATA0CH0_CR_TXCONTROL1_PH90_txdqsdelay_rk3_HSH;
      }
      break;

    case TxDqDelay:
      HashPtr->Data = DATA0CH0_CR_TXCONTROL0RANK0_txdqdelay_HSH;
      MrcGetSetUpdatePiHashPerGear (Gear4, Gear2, HashPtr);
      break;

    case TxDqDelay90:
      if (Rank == 0) {
        HashPtr->Data = DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk0_HSH;
      } else if (Rank == 1) {
        HashPtr->Data = DATA0CH0_CR_TXCONTROL0_PH90_txdqdelay_rk1_HSH;
      } else if (Rank == 2) {
        HashPtr->Data = DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk2_HSH;
      } else if (Rank == 3) {
        HashPtr->Data = DATA0CH0_CR_TXCONTROL1_PH90_txdqdelay_rk2_HSH;
      }

      break;

    case TxEqCoeff0:
      HashPtr->Data = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff0_HSH;
      break;

    case TxEqCoeff1:
      HashPtr->Data = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff1_HSH;
      break;

    case TxEqCoeff2:
      HashPtr->Data = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqcompcoeff2_HSH;
      break;

    case DefDrvEnLow:
      HashPtr->Data = CH2CCC_CR_DDRCRCCCCLKCONTROLS_DefDrvEnLow_HSH;
      break;

    case CmdTxEq:
      HashPtr->Data = CH2CCC_CR_DDRCRCCCCLKCONTROLS_CaTxEq_HSH;
      break;

    case CtlTxEq:
      HashPtr->Data = CH2CCC_CR_DDRCRCCCCLKCONTROLS_CtlTxEq_HSH;
      break;

    case RxVrefVttDecap:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvttmfc_HSH;
      break;

    case RxVrefVddqDecap:
      HashPtr->Data = DATA0CH0_CR_DQRXCTL1_dqrx_cmn_rxvrefvdd2gmfc_HSH;
      break;

    case ForceDfeDisable:
      HashPtr->Data = (MrcData->Inputs.Q0Regs) ? DATA0CH0_CR_DQRXCTL1_forcedfedisable_HSH : DATA0CH0_CR_DQSTXRXCTL0_forcedfedisable_HSH;
      break;

    case PanicVttDnLp:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC2_VttPanicCompDnLpMult_HSH;
      break;

    case VttGenStatusSelCount:
      HashPtr->Data = DDRVTT0_CR_DDRCRVTTGENSTATUS_SelCount_HSH;
      break;

    case VttGenStatusCount:
      HashPtr->Data = DDRVTT0_CR_DDRCRVTTGENSTATUS_Count_HSH;
      break;

    case RloadDqsDn:
// @todo_adl      HashPtr->Data = DDRVCCDLL0_CR_DDRCRVCCDLLCOMPDLL_RloadDqs_HSH;
      break;

    case RxCben:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_PITUNE_PiCbEn_HSH;
      break;

    case RxBiasCtl:
      HashPtr->Data = DATA0CH0_CR_AFEMISC_rxbias_cmn_biasicomp_HSH;
      break;

    case RxLpddrMode:
      HashPtr->Data = DATA0CH0_CR_AFEMISC_rxall_cmn_rxlpddrmode_HSH;
      break;

    case RxBiasVrefSel:
      HashPtr->Data = DATA0CH0_CR_AFEMISC_rxbias_cmn_vrefsel_HSH;
      break;

    case RxBiasTailCtl:
      HashPtr->Data = DATA0CH0_CR_AFEMISC_rxbias_cmn_tailctl_HSH;
      break;

    case DataRxD0PiCb:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL6_rxd0picb_HSH;
      break;

    case DataSDllPiCb:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL6_sdll_picb_HSH;
      break;

    case VccDllRxD0PiCb:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_RxD0PiCB_HSH;
      break;

    case VccDllSDllPiCb:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_SdllPiCB_HSH;
      break;

    case DqsOdtCompOffset:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL2_dqsodtcompoffset_HSH;
      break;

    case TxRankMuxDelay:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_txrankmuxdelay_HSH;
      break;

    case TxDqsRankMuxDelay:
      HashPtr->Data = A0 ? DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_A0_HSH : DATA0CH0_CR_DDRCRDATACONTROL2_txdqsrankmuxdelay_HSH;
      break;

    case TxDqBitDelay:
      HashPtr->Data = DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_HSH;
      break;

    case TxDqBitDelay90:
      HashPtr->Data = DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew90_HSH;
      break;

    case TxDqDccOffset:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRDATADQRANK0LANE0_TxDqDCCOffset_HSH;
      break;

    case TxDqsDccOffset:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRDATADQSRANK0_TxDQSDCCOffset_HSH;
      break;

    case TxDqsBitDelay:
      if (Rank >= MAX_RANK_IN_CHANNEL) {
        Status = mrcWrongInputParameter;
      } else {
        HashPtr->Data = TxDqsPerBitDeskewHash[Rank];
      }
      break;

    case RecEnOffset:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rcvenoffset_HSH;
      break;

    case RxDqsOffset:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqsoffset_HSH;
      break;

    case RxVrefOffset:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_vrefoffset_HSH;
      break;

    case TxDqsOffset:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqsoffset_HSH;
      break;

    case TxDqOffset:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_txdqoffset_HSH;
      break;

    case RxDqsPiUiOffset:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_rxdqspiuioffset_HSH;
      break;

    case RoundTripDelay:
      if (Lpddr4 || Lpddr5){
        LogicalIndex = (2 * (Channel % 2)) + (4 * (Rank / 2)) + (Rank % 2);
      } else {
        LogicalIndex = Rank % 4;
      }
      if (LogicalIndex < ROUNDTRIP_DELAY_MAX) {
        HashPtr->Data = RoundtripDelayHsh[LogicalIndex];
      } else {
        Status = mrcWrongInputParameter;
      }
      break;

    case RxFlybyDelay:
      if ((Rank >= MAX_RANK_IN_CHANNEL) || (ChMod2 > (sizeof (RxFlybyDelayHsh) / sizeof (RxFlybyDelayHsh[0])))) {
        Status = mrcWrongInputParameter;
      } else {
        HashPtr->Data = RxFlybyDelayHsh[ChMod2][Rank];
      }
      break;

    case RxIoTclDelay:
      HashPtr->Data = MCMISCS_READCFGCH0_tcl4rcven_HSH;
      break;

    case RxFifoRdEnFlybyDelay:
      if ((Rank >= MAX_RANK_IN_CHANNEL) || (ChMod2 > (sizeof (RxFifoRdyDelay) / sizeof (RxFifoRdyDelay[0])))) {
        Status = mrcWrongInputParameter;
      } else {
        HashPtr->Data = RxFifoRdyDelay[ChMod2][Rank];
        if (Gear2 || Gear4) {
          // In Gear2 the value in  tCK is in bits [3:1], and bit [0] should be 0
          // In Gear4 the value in 2tCK is in bits [3:1], and bit [0] should be 0
          // In Gear1 we program the value as is, it has UI resolution
          HashPtr->Bits.BfOffset += 1;
          HashPtr->Bits.BfWidth -= 1;
        }
      }
      break;

    case RxFifoRdEnTclDelay:
      HashPtr->Data = MCMISCS_READCFGCH0_tcl4rxdqfiforden_HSH;
      break;

    case RxDqDataValidDclkDelay:
      HashPtr->Data = MCMISCS_READCFGCH0_rxdqdatavaliddclkdel_HSH;
      break;

    case RxDqDataValidQclkDelay:
      HashPtr->Data = MCMISCS_READCFGCH0_rxdqdatavalidqclkdel_HSH;
      break;

    case RxRptChDqClkOn:
      HashPtr->Data = A0? MCMISCS_WRITECFGCH0_rptchdqrxclkon_A0_HSH : MCMISCS_WRITECFGCH0_rptchdqrxclkon_HSH;
      break;

    case TxRptChDqClkOn:
      HashPtr->Data = A0? MCMISCS_WRITECFGCH0_rptchdqtxclkon_A0_HSH : MCMISCS_WRITECFGCH0_rptchdqtxclkon_HSH;
      break;

    case TxDqFifoWrEnTcwlDelay:
      HashPtr->Data = MCMISCS_WRITECFGCH0_tcwl4txdqfifowren_HSH;
      break;

    case TxDqFifoRdEnTcwlDelay:
      HashPtr->Data = MCMISCS_WRITECFGCH0_tcwl4txdqfiforden_HSH;
      break;

    case TxDqFifoRdEnFlybyDelay:
      if ((Rank >= MAX_RANK_IN_CHANNEL) || (ChMod2 > (sizeof (RxFlybyDelayHsh) / sizeof (RxFlybyDelayHsh[0])))) {
        Status = mrcWrongInputParameter;
      } else {
        HashPtr->Data = TxDqFifoRdEnFlyby[ChMod2][Rank];
      }
      break;

    case SenseAmpDelay:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL1_senseampdelay_HSH;
      break;

    case SenseAmpDuration:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL1_senseampduration_HSH;
      break;

    case McOdtDelay:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL1_dqodtdelay_HSH;
      break;

    case McOdtDuration:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_HSH;
      break;

    case DqsOdtDelay:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtdelay_HSH;
      break;

    case DqsOdtDuration:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_HSH;
      break;

    case RxDqsAmpOffset:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRDATADQSRANK0_RxDQSVrefOffset_HSH;
      break;

    case RxDqsUnmatchedAmpOffset:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRDATADQSRANK0_RxDqsUnMatVrefOffset_HSH;
      break;

    case CmdVref:
      if (Rank < MAX_RANK_IN_CHANNEL) {
        // The CH0 and CH1 hash is identical, just the register offset differs
        HashPtr->Data = CaVrefHsh[MrcData->Inputs.UlxUlt ? Controller : RANK_TO_DIMM_NUMBER (Rank)];
      } else {
        Status = mrcWrongInputParameter;
      }
      break;

    case CmdGrpPi:
      HashPtr->Data = CH2CCC_CR_PICODE0_mdll_cmn_pi1code_HSH;
      MrcGetSetUpdatePiHashPerGear (Gear4, Gear2, HashPtr);
      break;

    case ClkGrpPi:
      if (DtHalo && Ddr4 && ((Rank % 2) == 1)) {
        HashPtr->Data = CH2CCC_CR_PICODE3_mdll_cmn_pi6code_HSH; // DDR4 CLK1 and CLK3
      } else {
        HashPtr->Data = CH2CCC_CR_PICODE2_mdll_cmn_pi4code_HSH; // DDR4 CLK0 and CLK2, and other DDR types
      }
      MrcGetSetUpdatePiHashPerGear (Gear4, Gear2, HashPtr);
      break;

    case ClkGrpG4Pi:
      HashPtr->Data = CH2CCC_CR_PICODE2_mdll_cmn_pi5code_HSH; // CLK PH90
      break;

    case CtlGrpPi:
      if (Ddr4) {
        if (DtHalo) {
          HashPtr->Data = CtlCccHshDdr4Hs[Rank];
        } else {
          HashPtr->Data = (Rank == 0 ) ? CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH : CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH;
        }
      } else if (Ddr5) {
        if (DtHalo) {
          HashPtr->Data = ((Channel < 4) ? CtlCcc0123HshDdr5Hs[Rank] : CtlCcc4567HshDdr5Hs[Rank]);
        } else {
          HashPtr->Data = (Rank == 0) ? CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH : CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH;
        }
      } else {
        // Lpddr
        if (Lpddr4 || (Lpddr5 && (Lp5CccConfig & (1 << Channel)))) { // LP4 and LP5 Descending use the same mapping
          HashPtr->Data = (Rank == 0) ? CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH : CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH;
        } else { // LP5 Ascending CCC
          HashPtr->Data = (Rank == 0) ? CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH : CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH;
        }
      }
      MrcGetSetUpdatePiHashPerGear (Gear4, Gear2, HashPtr);
      break;

    case CkeGrpPi:
      if (Ddr4) {
        if (DtHalo) {
          HashPtr->Data = CkeCccHshDdr4[Rank];
        } else {
          HashPtr->Data = (Rank == 0 ) ? CH2CCC_CR_PICODE1_mdll_cmn_pi3code_HSH : CH2CCC_CR_PICODE1_mdll_cmn_pi2code_HSH;
        }
      } else if (Lpddr4) {
        HashPtr->Data = CH2CCC_CR_PICODE3_mdll_cmn_pi6code_HSH;
      } else {
        Status = mrcWrongInputParameter;
      }
      MrcGetSetUpdatePiHashPerGear (Gear4, Gear2, HashPtr);
      break;

    case WckGrpPi:
      HashPtr->Data = CH2CCC_CR_PICODE3_mdll_cmn_pi6code_HSH;
      MrcGetSetUpdatePiHashPerGear (Gear4, Gear2, HashPtr);
      break;

    case WckGrpPi90:
      HashPtr->Data = CH2CCC_CR_PICODE3_mdll_cmn_pi7code_HSH;
      MrcGetSetUpdatePiHashPerGear (Gear4, Gear2, HashPtr);
      break;

    case TxSlewRate:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_DqScompCells_HSH;
      break;

    case DqScompPC:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_DqScompPC_HSH;
      break;

    case CmdSlewRate:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_CmdScompCells_HSH;
      break;

    case CmdScompPC:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_CmdScompPC_HSH;
      break;

    case CtlSlewRate:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_CtlScompCells_HSH;
      break;

    case CtlScompPC:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_CtlScompPC_HSH;
      break;

    case ClkSlewRate:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_ClkScompCells_HSH;
      break;

    case ClkScompPC:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_ClkScompPC_HSH;
      break;

    case SCompBypassDq:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL2_DqSlewDlyByPass_HSH;
      break;

    case SCompBypassCmd:
      HashPtr->Data = A0 ? CH2CCC_CR_DDRCRPINSUSED_DdrCaSlwDlyBypass_A0_HSH  : CH2CCC_CR_COMP3_DdrCaSlwDlyBypass_HSH;
      break;

    case SCompBypassCtl:
      HashPtr->Data = A0 ? CH2CCC_CR_DDRCRPINSUSED_DdrCtlSlwDlyBypass_A0_HSH : CH2CCC_CR_COMP3_DdrCtlSlwDlyBypass_HSH;
      break;

    case SCompBypassClk:
      HashPtr->Data = A0 ? CH2CCC_CR_DDRCRPINSUSED_DdrClkSlwDlyBypass_A0_HSH : CH2CCC_CR_COMP3_DdrClkSlwDlyBypass_HSH;
      break;

    case SCompCodeDq:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_RCOMPDATA0_SlewRateComp_HSH;
      break;

    case SCompCodeCtl: // Same CR Layout
    case SCompCodeClk: // Same CR Layout
    case SCompCodeCmd:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCACOMP_Scomp_HSH;
      break;

    case TxRonUp:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvUp_HSH;
      break;

    case WrDSCodeUpCtl: // Same CR Layout
    case WrDSCodeUpClk: // Same CR Layout
    case WrDSCodeUpCmd:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCMDCOMP_RcompDrvUp_HSH;
      break;

    case TxRonDn:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACOMP0_RcompDrvDown_HSH;
      break;

    case WrDSCodeDnCtl: // Same CR layout
    case WrDSCodeDnClk: // Same CR layout
    case WrDSCodeDnCmd:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCMDCOMP_RcompDrvDown_HSH;
      break;

    case TxTco:
      HashPtr->Data = DATA0CH0_CR_COMPCTL2_dxtx_cmn_txtcocomp_HSH;
      break;

    case TxDqsTcoP:
      HashPtr->Data = DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsp_HSH;
      break;

    case TxDqsTcoN:
      HashPtr->Data = DATA0CH0_CR_COMPCTL1_dqstx_cmn_txtcocompdqsn_HSH;
      break;

    /* @todo_adl
    case TcoCompCodeCtl: // Same CR layout
    case TcoCompCodeClk: // Same CR layout
    case TcoCompCodeCmd:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCACOMP_TcoComp_HSH;
      break;
      */
    case DqOdtVrefUp:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL0_dqodtvrefup_HSH;
      break;

    case DqOdtVrefDn:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL0_dqodtvrefdn_HSH;
      break;

    case DqDrvVrefUp:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL0_dqvrefup_HSH;
      break;

    case DqDrvVrefDn:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL0_dqvrefdn_HSH;
      break;

    case CmdDrvVrefUp:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_cmdvrefup_HSH;
      break;

    case CmdDrvVrefDn:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL2_cmdvrefdn_HSH;
      break;

    case CtlDrvVrefUp:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_ctlvrefup_HSH;
      break;

    case CtlDrvVrefDn:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL2_ctlvrefdn_HSH;
      break;

    case ClkDrvVrefUp:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_clkvrefup_HSH;
      break;

    case ClkDrvVrefDn:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL2_clkvrefdn_HSH;
      break;

    case CompRcompOdtUp:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtUp_HSH;
      break;

    case CompRcompOdtDn:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACOMP0_RcompOdtDown_HSH;
      break;

    case CtlSCompOffset:
      HashPtr->Data = CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlScompOffset_HSH;
      break;

    case CkeSCompOffset:
// @todo_adl
      break;

    case CompOffsetVssHiFF:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATAOFFSETCOMP_VssHiFFCompOffset_HSH;
      break;

    case CtlRCompDrvUpOffset:
      HashPtr->Data = CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvUpOffset_HSH;
      break;

    case CtlRCompDrvDownOffset:
      HashPtr->Data = CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CtlRcompDrvDownOffset_HSH;
      break;

    case CkeRCompDrvUpOffset:
    case CkeRCompDrvDownOffset:
// @todo_adl
      break;

    case CmdSCompOffset:
      HashPtr->Data = CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaScompOffset_HSH;
      break;

    case CmdRCompDrvUpOffset:
      HashPtr->Data = CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvUpOffset_HSH;
      break;

    case CmdRCompDrvDownOffset:
      HashPtr->Data = CH2CCC_CR_DDRCRCTLCACOMPOFFSET_CaRcompDrvDownOffset_HSH;
      break;

    case ClkRCompDrvDownOffset:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvDownOffset_HSH;
      break;

    case ClkRCompDrvUpOffset:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkRcompDrvUpOffset_HSH;
      break;

    case ClkSCompOffset:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkScompOffset_HSH;
      break;

    case VsxHiClkFFOffset:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_ClkFFOffset_HSH;
      break;

    case VsxHiCaFFOffset:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CaFFOffset_HSH;
      break;

    case VsxHiCtlFFOffset:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_CtlFFOffset_HSH;
      break;

    case RxPerBitDeskewCal:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL3_RxPBDCalibration_HSH;
      break;

    case TxPerBitDeskewCal:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL2_TxPBDCalibration_HSH;
      break;

    case CccPerBitDeskewCal:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRCCCPERBITDESKEW0_deskewcal_HSH;
      break;

    case RxUnmatchedOffNcal:
      switch (Rank) {
        case 0:
          HashPtr->Data = DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxunoffsetcal_HSH;
          break;
        case 1:
          HashPtr->Data = DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxunoffsetcal_HSH;
          break;
        case 2:
          HashPtr->Data = DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk2_dq0_rxunoffsetcal_HSH;
          break;
        case 3:
          HashPtr->Data = DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk3_dq0_rxunoffsetcal_HSH;
          break;
        default:
          Status = mrcWrongInputParameter;
          break;
      }
      break;

    case RxMatchedUnmatchedOffPcal:
      switch (Rank) {
        case 0:
          HashPtr->Data = DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk0_dq0_rxupmpoffsetcal_HSH;
          break;
        case 1:
          HashPtr->Data = DATA0CH0_CR_DQ0RXAMPCTL_dqrx_rk1_dq0_rxupmpoffsetcal_HSH;
          break;
        case 2:
          switch (Bit) {
            case 0:
            case 3:
              HashPtr->Data = DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq0_rxupmpoffsetcal_HSH;
              break;
            case 1:
            case 4:
            case 6:
              HashPtr->Data = DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq1_rxupmpoffsetcal_HSH;
              break;
            case 2:
            case 5:
            case 7:
              HashPtr->Data = DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk2_dq2_rxupmpoffsetcal_HSH;
              break;
            default:
              Status = mrcWrongInputParameter;
              break;
          }
          break;
        case 3:
          switch (Bit) {
            case 0:
            case 3:
              HashPtr->Data = DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq0_rxupmpoffsetcal_HSH;
              break;
            case 1:
            case 4:
            case 6:
              HashPtr->Data = DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq1_rxupmpoffsetcal_HSH;
              break;
            case 2:
            case 5:
            case 7:
              HashPtr->Data = DATA0CH0_CR_DQRXAMPCTL0_dqrx_rk3_dq2_rxupmpoffsetcal_HSH;
              break;
            default:
              Status = mrcWrongInputParameter;
              break;
          }
          break;
        default:
          Status = mrcWrongInputParameter;
          break;
      }
      break;

    // These are unused.  Need to finish HAL if these fields are accessed.
    case TxRon:
    case CmdRon:
    case ClkRon:
    case CtlRon:

    default:
      Status = mrcWrongInputParameter;
      break;
  }

  if (HashPtr->Data == MRC_UINT32_MAX) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Hash is undefined. Group %s(%d) Channel %u Rank %u Strobe %u, Lane %u\n", gErrString, GsmGtDebugStrings[Group], Group, Channel, Rank, Strobe, Bit);
    Status = mrcWrongInputParameter;
  }
  if (Status == mrcSuccess) {
    CrOffset = MrcGetDdrIoRegOffset (MrcData, Group, Socket, Controller, Channel, Rank, Strobe, Bit, FreqIndex);
  }

  HashPtr->Bits.Offset = CrOffset;

  return Status;
}

/**
  This function generates the hash used to execute the Get/Set function.
  The hash consists of: Register Offset, BitField start bit, BitField length.

  @param[in]  MrcData     - Pointer to global data.
  @param[in]  Group       - DDRIO group to access.
  @param[in]  Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Channel     - DDR Channel Number within the memory controller (0-based).
  @param[in]  Rank        - Rank number within a channel (0-based).
  @param[in]  Strobe      - Dqs data group within the rank (0-based).
  @param[in]  Lane        - Lane index within the group (0-based).
  @param[in]  FreqIndex   - Index supporting multiple operating frequencies. (Not used in Client CPU's)
  @param[out] HashVal     - Pointer to return the MMIO access instruction.

  @retval MrcStatus
**/
MrcStatus
MrcGetDdrIoCfgHash (
  IN  MrcParameters   *const  MrcData,
  IN  GSM_GT          const   Group,
  IN  UINT32          const   Socket,
  IN  UINT32          const   Channel,
  IN  UINT32          const   Rank,
  IN  UINT32          const   Strobe,
  IN  UINT32          const   Lane,
  IN  UINT32          const   FreqIndex,
  OUT UINT32          *const  HashVal
  )
{
  MrcStatus Status;
  MrcDebug  *Debug;
  MrcInput  *Inputs;
  UINT32    CrOffset;
  MRC_REGISTER_HASH_STRUCT *HashPtr;
  BOOLEAN    UlxUlt;
  BOOLEAN    A0;

/* @todo_adl
  static const UINT32 DccLaneStatus[MAX_BITS] = {
    DATA0CH0_CR_DCCLANESTATUS0_Lane0Result_HSH, DATA0CH0_CR_DCCLANESTATUS0_Lane1Result_HSH,
    DATA0CH0_CR_DCCLANESTATUS0_Lane2Result_HSH, DATA0CH0_CR_DCCLANESTATUS1_Lane3Result_HSH,
    DATA0CH0_CR_DCCLANESTATUS1_Lane4Result_HSH, DATA0CH0_CR_DCCLANESTATUS1_Lane5Result_HSH,
    DATA0CH0_CR_DCCLANESTATUS2_Lane6Result_HSH, DATA0CH0_CR_DCCLANESTATUS2_Lane7Result_HSH
  };
  static const UINT32 CccPerBitDeskew[MAX_CCC_PER_BIT] = {
    CH0CCC_CR_DDRCRCCCPERBITDESKEW0_CCC0_HSH, CH0CCC_CR_DDRCRCCCPERBITDESKEW0_CCC1_HSH,
    CH0CCC_CR_DDRCRCCCPERBITDESKEW0_CCC2_HSH, CH0CCC_CR_DDRCRCCCPERBITDESKEW0_CCC3_HSH,
    CH0CCC_CR_DDRCRCCCPERBITDESKEW0_CCC4_HSH, CH0CCC_CR_DDRCRCCCPERBITDESKEW1_CCC5_HSH,
    CH0CCC_CR_DDRCRCCCPERBITDESKEW1_CCC6_HSH, CH0CCC_CR_DDRCRCCCPERBITDESKEW1_CCC7_HSH,
    CH0CCC_CR_DDRCRCCCPERBITDESKEW1_CCC8_HSH, CH0CCC_CR_DDRCRCCCPERBITDESKEW1_CCC9_HSH,
    CH0CCC_CR_DDRCRCCCPERBITDESKEW2_CCC10_HSH, CH0CCC_CR_DDRCRCCCPERBITDESKEW2_CCC11_HSH,
    CH0CCC_CR_DDRCRCCCPERBITDESKEW2_CCC12_HSH
  }; */
  static const UINT32 CaVrefConvergeHsh[MAX_CHANNEL][MAX_DIMMS_IN_CHANNEL] = { // CaVref is per DIMM
    {
      DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca00slowbw_HSH,
      DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca01slowbw_HSH
    },
    {
      DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca10slowbw_HSH,
      DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca11slowbw_HSH
    }
  };
  static const UINT32 DataRetrainDqHsh[MAX_BITS] = {
    DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq0_HSH, DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq1_HSH,
    DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq2_HSH, DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq3_HSH,
    DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq4_HSH, DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq5_HSH,
    DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq6_HSH, DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_dq7_HSH
  };
  static const UINT32 DccCodePh0IndexCccHsh[] = {
    // Index [0..6] have same register layout in DATA and CCC
    DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_HSH,
    DATA0CH0_CR_DCCCTL5_dcccodeph0_index1_HSH,
    DATA0CH0_CR_DCCCTL5_dcccodeph0_index2_HSH,
    DATA0CH0_CR_DCCCTL6_dcccodeph0_index3_HSH,
    DATA0CH0_CR_DCCCTL6_dcccodeph0_index4_HSH,
    DATA0CH0_CR_DCCCTL6_dcccodeph0_index5_HSH,
    DATA0CH0_CR_DCCCTL6_dcccodeph0_index6_HSH,
    CH2CCC_CR_DCCCTL8_dcccodeph0_index7_HSH,
    CH2CCC_CR_DCCCTL8_dcccodeph0_index8_HSH,
    CH2CCC_CR_DCCCTL8_dcccodeph0_index9_HSH,
    CH2CCC_CR_DCCCTL8_dcccodeph0_index10_HSH,
    CH2CCC_CR_DCCCTL9_dcccodeph0_index11_HSH,
    CH2CCC_CR_DCCCTL9_dcccodeph0_index12_HSH,
    CH2CCC_CR_DCCCTL9_dcccodeph0_index13_HSH,
    CH2CCC_CR_DCCCTL20_dcccodeph0_index14_HSH,
    CH2CCC_CR_DCCCTL20_dcccodeph0_index15_HSH
  };
  static const UINT32 DccCodePh90IndexCccHsh[] = {
    // DATA and CCC have same register layout
    DATA0CH0_CR_DCCCTL9_dcccodeph90_index6_HSH,
    DATA0CH0_CR_DCCCTL9_dcccodeph90_index7_HSH,
    DATA0CH0_CR_DCCCTL9_dcccodeph90_index8_HSH,
    DATA0CH0_CR_DCCCTL9_dcccodeph90_index9_HSH
  };
  static const UINT32 DccCodeTxPbdIndexCccHsh[] = {
    CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbddoffset_HSH,
    CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbddoffset_HSH,
    CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbddoffset_HSH,
    CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbddoffset_HSH
  };
  static const UINT32 DccCodeTxPbdPh90IncrIndexCccHsh[] = {
    CH2CCC_CR_TXPBDOFFSET_txpbd_ccc5_txpbd_ph90incr_HSH,
    CH2CCC_CR_TXPBDOFFSET_txpbd_ccc6_txpbd_ph90incr_HSH,
    CH2CCC_CR_TXPBDOFFSET_txpbd_ccc7_txpbd_ph90incr_HSH,
    CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbd_ph90incr_HSH
  };
  static const UINT32 DccCodePh0IndexDataHsh[] = {
    DATA0CH0_CR_DCCCTL5_dcccodeph0_index0_HSH,
    DATA0CH0_CR_DCCCTL5_dcccodeph0_index1_HSH,
    DATA0CH0_CR_DCCCTL5_dcccodeph0_index2_HSH,
    DATA0CH0_CR_DCCCTL6_dcccodeph0_index3_HSH,
    DATA0CH0_CR_DCCCTL6_dcccodeph0_index4_HSH,
    DATA0CH0_CR_DCCCTL6_dcccodeph0_index5_HSH,
    DATA0CH0_CR_DCCCTL6_dcccodeph0_index6_HSH,
    DATA0CH0_CR_DCCCTL7_dcccodeph0_index7_HSH,
    DATA0CH0_CR_DCCCTL7_dcccodeph0_index8_HSH,
    DATA0CH0_CR_DCCCTL7_dcccodeph0_index9_HSH
  };
  static const UINT32 DccCodePh90IndexDataHsh[] = {
    DATA0CH0_CR_DCCCTL7_dcccodeph90_index1_HSH,
    DATA0CH0_CR_DCCCTL8_dcccodeph90_index2_HSH,
    DATA0CH0_CR_DCCCTL8_dcccodeph90_index3_HSH,
    DATA0CH0_CR_DCCCTL8_dcccodeph90_index4_HSH,
    DATA0CH0_CR_DCCCTL8_dcccodeph90_index5_HSH,
    DATA0CH0_CR_DCCCTL9_dcccodeph90_index6_HSH,
    DATA0CH0_CR_DCCCTL9_dcccodeph90_index7_HSH,
    DATA0CH0_CR_DCCCTL9_dcccodeph90_index8_HSH,
    DATA0CH0_CR_DCCCTL9_dcccodeph90_index9_HSH
  };
  static const UINT32 DccCodeTxPbdIndexDataHsh[] = {
    DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbddo_HSH,
    DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbddoffset_HSH,
    DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbddoffset_HSH,
    DATA0CH0_CR_TXPBDOFFSET0_dq2_txpbddoffset_HSH,
    DATA0CH0_CR_TXPBDOFFSET0_dq3_txpbddoffset_HSH,
    DATA0CH0_CR_TXPBDOFFSET0_dq4_txpbddoffset_HSH,
    DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbddoffset_HSH,
    DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbddoffset_HSH,
    DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbddoffset_HSH
  };
  static const UINT32 DccCodeTxPbdPh90IncrIndexDataHsh[] = {
    DATA0CH0_CR_TXPBDOFFSET1_dqs_txpbd_ph90incr_HSH,
    DATA0CH0_CR_TXPBDOFFSET0_dq0_txpbd_ph90incr_HSH,
    DATA0CH0_CR_TXPBDOFFSET0_dq1_txpbd_ph90incr_HSH,
    DATA0CH0_CR_TXPBDOFFSET1_dq2_txpbd_ph90incr_HSH,
    DATA0CH0_CR_TXPBDOFFSET1_dq3_txpbd_ph90incr_HSH,
    DATA0CH0_CR_TXPBDOFFSET1_dq4_txpbd_ph90incr_HSH,
    DATA0CH0_CR_TXPBDOFFSET1_dq5_txpbd_ph90incr_HSH,
    DATA0CH0_CR_TXPBDOFFSET1_dq6_txpbd_ph90incr_HSH,
    DATA0CH0_CR_TXPBDOFFSET1_dq7_txpbd_ph90incr_HSH
  };
  Debug     = &MrcData->Outputs.Debug;
  Inputs    = &MrcData->Inputs;
  Status    = mrcSuccess;
  HashPtr   = (MRC_REGISTER_HASH_STRUCT *) HashVal;
  HashPtr->Data = MRC_UINT32_MAX;
  CrOffset  = MRC_UINT32_MAX;
  UlxUlt    = Inputs->UlxUlt;
  A0        = Inputs->A0;

  switch (Group) {
    case GsmIocWlWakeCyc:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DLLWEAKLOCK_wl_wakecycles_HSH;
      break;

    case GsmIocWlSleepCyclesAct:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DLLWEAKLOCK_WL_SleepCyclesAct_HSH;
      break;

    case GsmIocWlSleepCyclesLp:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DLLWEAKLOCK_WL_SleepCyclesLP_HSH;
      break;

    case GsmIocForceCmpUpdt:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_forcecompupdate_HSH;
      break;
/*
    case GsmIocWlLatency: // @todo No TGL version found
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_WeakLock_Latency_HSH;
      break;
*/
    case GsmIocNoDqInterleave:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_ddrnochinterleave_HSH;
      break;

    case GsmIocScramLpMode:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_lpddr_mode_HSH;
      break;

    case GsmIocScramDdr4Mode:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_ddr4_mode_HSH;
      break;

    case GsmIocScramDdr5Mode:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_ddr5_mode_HSH;
      break;

    case GsmIocScramGear1:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_gear1_HSH;
      break;

    case GsmIocScramGear4:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_Gear4_HSH;
      break;

    case GsmIocDisClkGate:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_clkgatedisable_HSH;
      break;

    case GsmIocDisDataIdlClkGate:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_dataclkgatedisatidle_HSH;
      break;

    case GsmIocScramLp4Mode:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_lpddr4mode_HSH;
      break;

    case GsmIocScramLp5Mode:
      HashPtr->Data = MCMISCS_DDRWCKCONTROL_Lp5Mode_HSH;
      break;

    case GsmIocScramOvrdPeriodicToDvfsComp:
// @todo_adl      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_OvrdPeriodicToDvfsComp_HSH;
      break;

    case GsmIocLp5Wck2CkRatio:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_wck2ck_4_1_HSH;
      break;

    case GsmIocChNotPop:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_channel_not_populated_HSH;
      break;

    case GsmIocDisIosfSbClkGate:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_dis_iosf_sb_clk_gate_HSH;
      break;

    case GsmIocEccEn:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_eccpresent_HSH;
      break;

    case GsmIocWrite0En:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_write0_enable_HSH;
      break;

    case GsmIocScramWrite0En:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL0_wrp_d5_wr_0_HSH;
      break;

    case GsmIocVccDllGear1:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_VCCDLLDQSDELAY_Gear1_HSH;
      break;

    case GsmIocVccDllControlBypass_V:
// @todo_adl      HashPtr->Data = DDRVCCDLL0_CR_DDRCRVCCDLLCONTROL_Bypass_HSH;
      break;

    case GsmIocVccDllControlSelCode_V:
// @todo_adl      HashPtr->Data = DDRVCCDLL0_CR_DDRCRVCCDLLCONTROL_SelCode_HSH;
      break;

    case GsmIocVccDllControlTarget_V:
// @todo_adl      HashPtr->Data = DDRVCCDLL0_CR_DDRCRVCCDLLCONTROL_Target_HSH;
      break;

    case GsmIocVccDllControlOpenLoop_V:
// @todo_adl      HashPtr->Data = DDRVCCDLL0_CR_DDRCRVCCDLLCONTROL_OpenLoop_HSH;
      break;

    case GsmIocVsxHiControlSelCode_V:
// @todo_adl      HashPtr->Data = DDRVSSHIAFEA_CR_DDRCRVSSHICONTROL_SelCode_HSH;
      break;

    case GsmIocVsxHiControlOpenLoop_V:
// @todo_adl      HashPtr->Data = DDRVSSHIAFEA_CR_DDRCRVSSHICONTROL_OpenLoop_HSH;
      break;

    case GsmIocCccPiEn:
      HashPtr->Data = CH2CCC_CR_DDRCRPINSUSED_PiEn_HSH;
      break;

    case GsmIocCccPiEnOverride:
      HashPtr->Data = CH2CCC_CR_DDRCRPINSUSED_PiEnOvrd_HSH;
      break;

    case GsmIocClkPFallNRiseMode:
      // Bit field position is the same for TCO1 and TCO2.
// @todo_adl      HashPtr->Data = (UlxUlt) ? CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_EnFeedback_HSH : CH2CCC_CR_DDRCRPERBITTCO1_EnClkRFFeedback_P0_HSH;
      break;

    case GsmIocClkPRiseNFallMode:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRCCCPERBITDESKEWPRISENFALL_EnFeedback_HSH;
      break;

    case GsmIocClkPFallNRiseFeedback:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_PfNrFeedback_HSH;
      break;

    case GsmIocClkPRiseNFallFeedback:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRCCCPERBITDESKEWPRISENFALL_PrNfFeedback_HSH;
      break;

    case GsmIocClkPFallNRiseCcc56:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_CCC56_HSH;
      break;

    case GsmIocClkPRiseNFallCcc56:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRCCCPERBITDESKEWPRISENFALL_CCC56_HSH;
      break;

    case GsmIocClkPFallNRiseCcc78:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_CCC78_HSH;
      break;

    case GsmIocClkPRiseNFallCcc78:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DDRCRCCCPERBITDESKEWPRISENFALL_CCC78_HSH;
      break;

    case GsmIocDataOdtStaticEn:
      HashPtr->Data = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txodtstatlegendq_HSH;
      break;

    case GsmIocStrobeOdtStaticEn:
      HashPtr->Data = DATA0CH0_CR_DQSTXRXCTL_dqstx_cmn_txodtstatlegendqs_HSH;
      break;

    case GsmIocDqsMaskPulseCnt:
      HashPtr->Data = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrmaskcntpulsenumstart_HSH;
      break;

    case GsmIocDqsMaskValue:
      HashPtr->Data = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrdqsmaskvalue_HSH;
      break;

    case GsmIocDqsPulseCnt:
      HashPtr->Data = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrcrnumofpulses_HSH;
      break;

    case GsmIocDqOverrideData:
      HashPtr->Data = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrddata_HSH;
      break;

    case GsmIocDqOverrideEn:
      HashPtr->Data = DATA0CH0_CR_DDRCRCMDBUSTRAIN_ddrdqovrdmodeen_HSH;
      break;

    case GsmIocForceOnRcvEn:
      HashPtr->Data = DATA0CH0_CR_DDRCRMARGINMODECONTROL0_forceonrcven_HSH;
      break;

    case GsmIocRankOverrideEn:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_rankovrd_HSH;
      break;

    case GsmIocRankOverrideVal:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_rankvalue_HSH;
      break;

    case GsmIocDataCtlGear1:
      HashPtr->Data = A0 ? DATA0CH0_CR_DDRCRDATACONTROL2_gear1_A0_HSH : DATA0CH0_CR_DDRCRDATACONTROL0_gear1_HSH;
      break;

    case GsmIocDataCtlGear4:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_Gear4_HSH;
      break;

    case GsmIocRxSALTailCtrl:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL3_RxSALTailCtrl_HSH;
      break;

    case GsmIocDataWrPreamble:
      HashPtr->Data = A0 ? DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_A0_HSH : DATA0CH0_CR_DDRCRDATACONTROL2_wrpreamble_HSH;
      break;

    case GsmIocLdoFFCodeLockData:
      HashPtr->Data = DATA0CH0_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_HSH;
      break;

    case GsmIocDccFsmResetData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL2_dccfsm_rst_b_HSH;
      break;

    case GsmIocLdoFFCodeLockCcc:
      HashPtr->Data = CH2CCC_CR_MDLLCTL2_mdll_cmn_ldoffcodelock_HSH;
      break;

    case GsmIocDccDcdSampleSelCcc:                   // DATA and CCC have same register layout
    case GsmIocDccDcdSampleSelData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL10_dcdsamplesel_HSH;
      break;

    case GsmIocStep1PatternCcc:                      // DATA and CCC have same register layout
    case GsmIocStep1PatternData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL10_step1pattern_HSH;
      break;

    case GsmIocDccCodePh0IndexCcc:
      if (Lane <= ARRAY_COUNT (DccCodePh0IndexCccHsh)) {
        HashPtr->Data = DccCodePh0IndexCccHsh[Lane];
      }
      break;

    case GsmIocDccCodePh90IndexCcc:                // Lane is Index [6..9]
      if ((Lane >= 6) && (Lane <= 9)) {
        HashPtr->Data = DccCodePh90IndexCccHsh[Lane - 6];
      }
      break;

    case GsmIocTxpbddOffsetCcc:                   // Lane is Index [6..9]
      if ((Lane >= 6) && (Lane <= 9)) {
        HashPtr->Data = DccCodeTxPbdIndexCccHsh[Lane - 6];
      }
      break;

    case GsmIocTxpbdPh90incrCcc:                  // Lane is Index [6..9]
      if ((Lane >= 6) && (Lane <= 9)) {
        HashPtr->Data = DccCodeTxPbdPh90IncrIndexCccHsh[Lane - 6];
      }
      break;

    case GsmIocMdllCmnVcdlDccCodeCcc:
      HashPtr->Data = CH2CCC_CR_MDLLCTL2_mdll_cmn_vcdldcccode_HSH;
      break;

    case GsmIocDccCodePh0IndexData:
      if (Lane <= ARRAY_COUNT (DccCodePh0IndexDataHsh)) {
        HashPtr->Data = DccCodePh0IndexDataHsh[Lane];
      }
      break;

    case GsmIocDccCodePh90IndexData:                // Lane is Index [1..9]
      if ((Lane >= 1) && (Lane <= 9)) {
        HashPtr->Data = DccCodePh90IndexDataHsh[Lane - 1];
      }
      break;

    case GsmIocTxpbddOffsetData:                   // Lane is Index [1..9]
      if ((Lane >= 1) && (Lane <= 9)) {
        HashPtr->Data = DccCodeTxPbdIndexDataHsh[Lane - 1];
      }
      break;

    case GsmIocTxpbdPh90incrData:                  // Lane is Index [1..9]
      if ((Lane >= 1) && (Lane <= 9)) {
        HashPtr->Data = DccCodeTxPbdPh90IncrIndexDataHsh[Lane - 1];
      }
      break;

    case GsmIocMdllCmnVcdlDccCodeData:
      HashPtr->Data = DATA0CH0_CR_MDLLCTL2_mdll_cmn_vcdldcccode_HSH;
      break;

    case GsmIocDccFsmResetCcc:
      HashPtr->Data = CH2CCC_CR_DCCCTL9_dccfsm_rst_b_HSH;
      break;

    case GsmIocDcdRoCalEnData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL11_dcdrocal_en_HSH;
      break;

    case GsmIocDcdRoCalEnCcc:
      HashPtr->Data = CH2CCC_CR_DCCCTL20_dcdrocal_en_HSH;
      break;

    case GsmIocDcdRoCalStartData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL11_dcdrocal_start_HSH;
      break;

    case GsmIocDcdRoCalStartCcc:
      HashPtr->Data = CH2CCC_CR_DCCCTL20_dcdrocal_start_HSH;
      break;

    case GsmIocDcdRoCalSampleCountRstData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL2_dcdrocalsamplecountrst_b_HSH;
      break;

    case GsmIocDcdRoCalSampleCountRstCcc:
      HashPtr->Data = CH2CCC_CR_DCCCTL20_dcdrocalsamplecountrst_b_HSH;
      break;

    case GsmIocDccStartData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL11_dcc_start_HSH;
      break;

    case GsmIocDccStartCcc:
      HashPtr->Data = CH2CCC_CR_DCCCTL20_dcc_start_HSH;
      break;

    case GsmIocSrzDcdEnData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL2_srz_dcdenable_HSH;
      break;

    case GsmIocSrzDcdEnCcc:
      HashPtr->Data = CH2CCC_CR_DCCCTL20_dcdsrz_cmn_dcdenable_HSH;
      break;

    case GsmIocMdllDcdEnData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL2_mdll_dcdenable_HSH;
      break;

    case GsmIocMdllDcdEnCcc:
      HashPtr->Data = CH2CCC_CR_DCCCTL20_mdll_cmn_enabledcd_HSH;
      break;

    case GsmIocDcdSampleCountRstData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL2_dcdsamplecountrst_b_HSH;
      break;

    case GsmIocDcdSampleCountStartData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL2_dcdsamplecountstart_HSH;
      break;

    case GsmIocDcdRoLfsrData:
      HashPtr->Data = DATA0CH0_CR_DCCCTL2_dcdrolfsr_HSH;
      break;

    case GsmIocTxOn:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_txon_HSH;
      break;

    case GsmIocRxDisable:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_rxdisable_HSH;
      break;

    case GsmIocDataDccLaneStatusResult:
    case GsmIocCccDccLaneStatusResult:
// @todo_adl      HashPtr->Data = DccLaneStatus[Lane];
      break;

    case GsmIocDataDccSaveFullDcc:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DCCFSMCONTROL_SaveFullDcc_HSH;
      break;

    case GsmIocDataDccSkipCRWrite:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DCCFSMCONTROL_SkipCRWrite_HSH;
      break;

    case GsmIocDataDccMeasPoint:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DCCFSMCONTROL_MeasPoint_HSH;
      break;

    case GsmIocDataDccRankEn:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DCCFSMCONTROL_RankEn_HSH;
      break;

    case GsmIocDataDccLaneEn:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DCCFSMCONTROL_LaneEn_HSH;
      break;

    case GsmIocDataDccStepSize:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DCCCALCCONTROL_DccStepSize_HSH;
      break;

    case GsmIocDataDcc2xStep:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DCCCALCCONTROL_Dcc2xStep_HSH;
      break;

    case GsmIocCccDccCcc:
// @todo_adl      HashPtr->Data = CccPerBitDeskew[Lane];
      break;

    case GsmIocCccDccStepSize:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DCCCALCCONTROL_DccStepSize_HSH;
      break;

    case GsmIocCccDcc2xStep:
// @todo_adl      HashPtr->Data = CH0CCC_CR_DCCCALCCONTROL_Dcc2xStep_HSH;
      break;

    case GsmIocSenseAmpMode:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_senseamptrainingmode_HSH;
      break;

    case GsmIocDqsRFMode:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_DqsRFTrainingMode_HSH;
      break;

    case GsmIocDdrDqRxSdlBypassEn:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqrxsdlbypassen_HSH;
      break;

    case GsmIocCaTrainingMode:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_catrainingmode_HSH;
      break;

    case GsmIocCaParityTrain:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL3_caparitytrain_HSH;
      break;

    case GsmIocDataTrainFeedback:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_datatrainfeedback_HSH;
      break;

    case GsmIocDdrDqDrvEnOvrdData:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrddata_HSH;
      break;

    case GsmIocDdrDqDrvEnOvrdModeEn:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_ddrdqdrvenovrdmodeen_HSH;
      break;

    case GsmIocRxAmpOffsetEn:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_rxampoffseten_HSH;
      break;

    case GsmIocReadLevelMode:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_rltrainingmode_HSH;
      break;

    case GsmIocWriteLevelMode:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_wltrainingmode_HSH;
      break;

    case GsmIocReadDqDqsMode:
      HashPtr->Data = DATA0CH0_CR_DATATRAINFEEDBACK_rxtrainingmode_HSH;
      break;

    case GsmIocForceOdtOn:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_odtforceqdrven_HSH;
      break;

    case GsmIocForceOdtOnWithoutDrvEn:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_ddrcrforceodton_HSH;
      break;

    case GsmIocRxPiPwrDnDis:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL3_rxpion_HSH;
      break;

    case GsmIocTxPiPwrDnDis:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_txpion_HSH;
      break;

    case GsmIocInternalClocksOn:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_internalclockson_HSH;
      break;

    case GsmIocDataDqsOdtParkMode:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_endqsodtparkmode_HSH;
      break;

    case GsmIocDataDqsNParkLow:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_DqsNParkLowVoh_HSH;
      break;

    case GsmIocTxDisable:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_txdisable_HSH;
      break;

    case GsmIocDllMask:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DLLWEAKLOCK_DLLMask0_HSH;
      break;

    case GsmIocSdllSegmentDisable:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL1_SdllSegmentDisable_HSH;
      break;

    case GsmIocRXDeskewForceOn:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL1_RXDeskewForceOn_HSH;
      break;

    case GsmIocCmdDrvVref200ohm:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL2_Cmd200VrefDn_HSH;
      break;

    case GsmIocDllWeakLock:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DLLWEAKLOCK_DllWeakLock0_HSH;
      break;

    case GsmIocDllWeakLock1:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DLLWEAKLOCK_DllWeakLock1_HSH;
      break;

    case GsmIocRxClkStg:
      HashPtr->Data = A0 ? DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_A0_HSH : DATA0CH0_CR_DDRCRDATACONTROL2_rxclkstgnum_HSH;
      break;

    case GsmIocDataRxBurstLen:
      HashPtr->Data = A0 ? DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_A0_HSH : DATA0CH0_CR_DDRCRDATACONTROL2_rxburstlen_HSH;
      break;

    case GsmIocEnDqsNRcvEn:
      HashPtr->Data = DATA0CH0_CR_DQSTXRXCTL_dqsrx_cmn_rxendqsnrcven_HSH;
      break;

    case GsmIocEnDqsNRcvEnGate:
      HashPtr->Data = DATA0CH0_CR_SDLCTL0_rxsdl_cmn_enabledqsnrcven_HSH;
      break;

    case GsmIocRxMatchedPathEn:
      HashPtr->Data = DATA0CH0_CR_SDLCTL0_rxsdl_cmn_rxmatchedpathen_HSH;
      break;

    case GsmIocRxVrefMFC:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL2_RxVrefVttProgMFC_HSH;
      break;
/*
    case GsmIocVrefPwrDnEn: // @todo No TGL version found
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL2_EnableVrefPwrDn_HSH;
      break;
*/
    case GsmIocConstZTxEqEn:
      HashPtr->Data = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqconstz_HSH;
      break;

    case GsmIocForceRxAmpOn:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_forcerxon_HSH;
      break;

    case GsmIocDqSlewDlyByPass:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_txdqslewdlybypass_HSH;
      break;

    case GsmIocWlLongDelEn:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_wllongdelen_HSH;
      break;

    case GsmIocBiasPMCtrl:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_biaspmctrl_HSH;
      break;

    case GsmIocLocalGateD0tx:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_local_gate_d0tx_HSH;
      break;

    case GsmIocDataOdtMode:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_OdtMode_HSH;
      break;

    case GsmIocFFCodeIdleOffset:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeIdleOffset_HSH;
      break;

    case GsmIocFFCodeWeakOffset:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeWeakOffset_HSH;
      break;

    case GsmIocFFCodePiOffset:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodePIOffset_HSH;
      break;

    case GsmIocFFCodeCCCDistOffset:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeCCCDistOffset_HSH;
      break;

    case GsmIocFFCodePBDOffset:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodePBDOffset_HSH;
      break;

    case GsmIocFFCodeWriteOffset:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeWriteOffset_HSH;
      break;

    case GsmIocFFCodeReadOffset:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_FFCodeReadOffset_HSH;
      break;

    case GsmIocCapCancelCodeIdle:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_CapCancelCodeIdle_HSH;
      break;

    case GsmIocCapCancelCodePBD:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_CapCancelCodePBD_HSH;
      break;

    case GsmIocCapCancelCodeWrite:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_CapCancelCodeWrite_HSH;
      break;

    case GsmIocCapCancelCodeRead:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_CapCancelCodeRead_HSH;
      break;

    case GsmIocCapCancelCodePi:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_CapCancelCodePi_HSH;
      break;

    case GsmIocVssHiFFCodeIdle:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodeIdle_HSH;
      break;

    case GsmIocVssHiFFCodeWrite:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodeWrite_HSH;
      break;

    case GsmIocVssHiFFCodeRead:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodeRead_HSH;
      break;

    case GsmIocVssHiFFCodePBD:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodePBD_HSH;
      break;

    case GsmIocVssHiFFCodePi:
// @todo_adl      HashPtr->Data = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_VsxHiFFCodePi_HSH;
      break;

    case GsmIocDataInvertNibble:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL2_datainvertnibble_HSH;
      break;

    case GsmIocEnableSpineGate:
      HashPtr->Data = MCMISCS_SPINEGATING_enablespinegate_HSH;
      break;

    case GsmIocDataDqOdtParkMode:
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_endqodtparkmode_HSH;
      break;

    case GsmIocTxEqEn:
      HashPtr->Data = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenpreset_HSH;
      break;

    case GsmIocTxEqTapSelect:
      HashPtr->Data = DATA0CH0_CR_DQTXCTL0_dqtx_cmn_txeqenntap_HSH;
      break;

    case GsmIocVccDllRxDeskewCal:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_RxDeskewCal_HSH;
      break;

    case GsmIocVccDllTxDeskewCal:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_TxDeskewCal_HSH;
      break;

    case GsmIocVccDllCccDeskewCal:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_CCCDeskewCal_HSH;
      break;
/*
    case GsmIocRxTypeSelect: // @todo No TGL version found
      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL4_rxsel_HSH;
      break;
*/

    case GsmIocIoReset:
      HashPtr->Data = DDRSCRAM_CR_DDRMISCCONTROL1_io_train_rst_HSH;
      break;

    case GsmIocCmdVrefConverge:
      if (UlxUlt) {
        if (Channel == 0) {
          HashPtr->Data = DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca00slowbw_HSH;
        } else {
          HashPtr->Data = DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_ca01slowbw_HSH;
        }
      } else if (Rank < MAX_RANK_IN_CHANNEL) {
        HashPtr->Data = CaVrefConvergeHsh[Channel][RANK_TO_DIMM_NUMBER (Rank)];
      } else {
        Status = mrcWrongInputParameter;
      }
      break;

    case GsmIocCompClkOn:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL4_compclkon_HSH;
      break;

    case GsmIocDisableQuickComp:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_DisableQuickComp_HSH;
      break;

    case GsmIocSinStep:
      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL4_sinstep_HSH;
      break;

    case GsmIocSinStepAdv:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_SinStepAdv_HSH;
      break;

    case GsmIocCompOdtStaticDis:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL1_DisableOdtStatic_HSH;
      break;

    case GsmIocCompVddqOdtEn:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL0_EnVddqOdt_HSH;
      break;

    case GsmIocCompVttOdtEn:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPCTL0_EnVttOdt_HSH;
      break;

    case GsmIocVttPanicCompUpMult:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC_VttPanicCompUp0Mult_HSH;
      break;

    case GsmIocVttPanicCompDnMult:
// @todo_adl      HashPtr->Data = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC_VttPanicCompDn0Mult_HSH;
      break;

    case GsmIocRptChRepClkOn:
      HashPtr->Data = A0 ? MCMISCS_WRITECFGCH0_rptchrepclkon_A0_HSH : MCMISCS_WRITECFGCH0_rptchrepclkon_HSH;
      break;

    case GsmIocCmdAnlgEnGraceCnt:
      HashPtr->Data = A0 ? MCMISCS_WRITECFGCH0_cmdanlgengracecnt_A0_HSH : MCMISCS_WRITECFGCH0_cmdanlgengracecnt_HSH;
      break;

    case GsmIocTxAnlgEnGraceCnt:
      HashPtr->Data = A0 ? MCMISCS_WRITECFGCH0_txanlgengracecnt_A0_HSH : MCMISCS_WRITECFGCH0_txanlgengracecnt_HSH;
      break;

    case GsmIocTxDqFifoRdEnPerRankDelDis:
      HashPtr->Data = MCMISCS_WRITECFGCH0_txdqfifordenperrankdeldis_HSH;
      break;

    case GsmIocRetrainSwizzleCtlBit:
      HashPtr->Data = DataRetrainDqHsh[Lane];
      break;

    case GsmIocRetrainSwizzleCtlByteSel:
      HashPtr->Data = DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrain_bytesel_HSH;
      break;

    case GsmIocRetrainSwizzleCtlRetrainEn:
      HashPtr->Data = DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataretrainen_HSH;
      break;

    case GsmIocRetrainSwizzleCtlSerialMrr:
      HashPtr->Data = DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_dataserialmrr_HSH;
      break;

    case GsmIocRetrainInitPiCode:
      HashPtr->Data = DATA0CH0_CR_DDRCRWRRETRAINRANK3_initpicode_HSH;
      break;

    case GsmIocRetrainDeltaPiCode:
      HashPtr->Data = DATA0CH0_CR_DDRCRWRRETRAINRANK3_deltapicode_HSH;
      break;

    case GsmIocRetrainRoCount:
      HashPtr->Data = DATA0CH0_CR_DDRCRWRRETRAINRANK3_rocount_HSH;
      break;

    case GsmIocRetrainCtlInitTrain:
      HashPtr->Data = DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_inittrain_HSH;
      break;

    case GsmIocRetrainCtlDuration:
      HashPtr->Data = DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_duration_HSH;
      break;

    case GsmIocRetrainCtlResetStatus:
      HashPtr->Data = DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_resetstatus_HSH;
      break;

    case GsmIocRxVocMode:
// @todo_adl      HashPtr->Data = DATA0CH0_CR_DDRCRDATACONTROL0_RxMode_HSH;
      break;

    //@todo case GsmIocDccTrainingMode:
    //@todo   HashPtr->Data = MCMISCS_DCCCONTROL0_TrainingMode_HSH;
    //@todo   break;

    //@todo case GsmIocDccTrainingDone:
    //@todo   HashPtr->Data = MCMISCS_DCCCONTROL0_TrainingDone_HSH;
    //@todo   break;

    //@todo case GsmIocDccDrain:
    //@todo   HashPtr->Data = MCMISCS_DCCCONTROL0_Drain_HSH;
    //@todo   break;

    //@todo case GsmIocDccActiveClks:
    //@todo   HashPtr->Data = MCMISCS_DCCMAINFSMCONTROL0_ActiveClks_HSH;
    //@todo   break;

    //@todo case GsmIocDccActiveBytes:
    //@todo   HashPtr->Data = MCMISCS_DCCMAINFSMCONTROL0_ActiveBytes_HSH;
    //@todo   break;

    //@todo case GsmIocDccDcoCompEn:
    //@todo   HashPtr->Data = MCMISCS_DCCMAINFSMCONTROL0_DcoCompEn_HSH;
    //@todo   break;

    //@todo case GsmIocDccClkTrainVal:
    //@todo   // Rank is MRC_IGNORE_ARG, Strobe is the one that is valid
    //@todo   if (Strobe == 0) {
    //@todo     HashPtr->Data = DDRCLKCH0_CR_DCCCLKTRAINVAL0_Clk0DccVal_HSH;
    //@todo   } else if (Strobe == 1) {
    //@todo     HashPtr->Data = DDRCLKCH0_CR_DCCCLKTRAINVAL0_Clk1DccVal_HSH;
    //@todo   } else if (Strobe == 2) {
    //@todo     //@todo H/S sku code
    //@todo     Status = mrcWrongInputParameter;
    //@todo   } else {
    //@todo     //@todo H/S sku code
    //@todo     Status = mrcWrongInputParameter;
    //@todo   }
    //@todo   break;

    //@todo case GsmIocDccDataTrainDqsVal:
    //@todo   HashPtr->Data = DATA0CH0_CR_DCCDATATRAINVAL1RANK0_DqsDccVal_HSH;
    //@todo   break;

    // These are unused.  Need to finish HAL if these fields are accessed.
    default:
      Status = mrcWrongInputParameter;
      break;
  }

  if (HashPtr->Data == MRC_UINT32_MAX) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Hash is undefined. Group %s(%d) Channel %u Rank %u Strobe %u Lane %u\n", gErrString, GsmGtDebugStrings[Group], Group, Channel, Rank, Strobe, Lane);
    Status = mrcWrongInputParameter;
  }
  if (Status == mrcSuccess) {
    CrOffset = MrcGetDdrIoConfigOffsets (MrcData, Group, Socket, Channel, Rank, Strobe, Lane, FreqIndex);
  }

  HashPtr->Bits.Offset  = CrOffset;

  return Status;
}

/**
  This function generates the hash used to execute the Get/Set function.
  The hash consists of: Register Offset, BitField start bit, BitField length.

  @param[in]  MrcData   - Pointer to global data.
  @param[in]  Group     - MC Timing group to access.
  @param[in]  Socket    - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Channel   - DDR Channel Number within the processor socket (0-based).
  @param[out] HashVal   - Pointer to return the MMIO access instruction.

  @retval MrcStatus
**/
MrcStatus
MrcGetMcTimingHash (
  IN  MrcParameters   *const  MrcData,
  IN  GSM_GT          const   Group,
  IN  UINT32          const   Socket,
  IN  UINT32          const   Controller,
  IN  UINT32          const   Channel,
  IN  UINT32          const   FreqIndex,
  OUT UINT32          *const  HashVal
  )
{
  MrcStatus Status;
  MrcDebug  *Debug;
  MRC_REGISTER_HASH_STRUCT *HashPtr;
  UINT32    CrOffset;

  Debug    = &MrcData->Outputs.Debug;
  Status   = mrcSuccess;
  HashPtr  = (MRC_REGISTER_HASH_STRUCT *) HashVal;
  CrOffset = MRC_UINT32_MAX;

  switch (Group) {
    case GsmMctRCD:
      HashPtr->Data = MC0_CH0_CR_TC_PRE_tRCD_HSH;
      break;
    case GsmMctRP:
      HashPtr->Data = MC0_CH0_CR_TC_PRE_tRP_HSH;
      break;

    case GsmMctRFCpb:
      HashPtr->Data = MC0_CH0_CR_SC_PBR_tRFCpb_HSH;
      break;

    case GsmMctRefm:
      HashPtr->Data = MC0_CH0_CR_TC_REFM_tRFM_HSH;
      break;

    case GsmMctRPabExt:
      HashPtr->Data = MC0_CH0_CR_TC_PRE_tRPab_ext_HSH;
      break;

    case GsmMctRAS:
      HashPtr->Data = MC0_CH0_CR_TC_PRE_tRAS_HSH;
      break;

    case GsmMctRDPRE:
      HashPtr->Data = MC0_CH0_CR_TC_PRE_tRDPRE_HSH;
      break;

    case GsmMctPPD:
      HashPtr->Data = MC0_CH0_CR_TC_PRE_tPPD_HSH;
      break;

    case GsmMctWRPRE:
      HashPtr->Data = MC0_CH0_CR_TC_PRE_tWRPRE_HSH;
      break;

    case GsmMctFAW:
      HashPtr->Data = MC0_CH0_CR_TC_ACT_tFAW_HSH;
      break;

    case GsmMctRRDsg:
      HashPtr->Data = MC0_CH0_CR_TC_ACT_tRRD_sg_HSH;
      break;

    case GsmMctRRDdg:
      HashPtr->Data = MC0_CH0_CR_TC_ACT_tRRD_dg_HSH;
      break;

    case GsmMctREFSbRd:
      HashPtr->Data =  MrcData->Inputs.A0 ? MC0_CH0_CR_TC_ACT_trefsbrd_A0_HSH : MC0_CH0_CR_TC_ACT_trefsbrd_HSH;
      break;

    case GsmMctLpDeratingExt:
      HashPtr->Data = MC0_CH0_CR_TC_PRE_derating_ext_HSH;
      break;

    case GsmMctRDRDsg:
      HashPtr->Data = MC0_CH0_CR_TC_RDRD_tRDRD_sg_HSH;
      break;

    case GsmMctRDRDdg:
      HashPtr->Data = MC0_CH0_CR_TC_RDRD_tRDRD_dg_HSH;
      break;

    case GsmMctRDRDdr:
      HashPtr->Data = MC0_CH0_CR_TC_RDRD_tRDRD_dr_HSH;
      break;

    case GsmMctRDRDdd:
      HashPtr->Data = MC0_CH0_CR_TC_RDRD_tRDRD_dd_HSH;
      break;

    case GsmMctRDWRsg:
      HashPtr->Data = MC0_CH0_CR_TC_RDWR_tRDWR_sg_HSH;
      break;

    case GsmMctRDWRdg:
      HashPtr->Data = MC0_CH0_CR_TC_RDWR_tRDWR_dg_HSH;
      break;

    case GsmMctRDWRdr:
      HashPtr->Data = MC0_CH0_CR_TC_RDWR_tRDWR_dr_HSH;
      break;

    case GsmMctRDWRdd:
      HashPtr->Data = MC0_CH0_CR_TC_RDWR_tRDWR_dd_HSH;
      break;

    case GsmMctWRRDsg:
      HashPtr->Data = MC0_CH0_CR_TC_WRRD_tWRRD_sg_HSH;
      break;

    case GsmMctWRRDdg:
      HashPtr->Data = MC0_CH0_CR_TC_WRRD_tWRRD_dg_HSH;
      break;

    case GsmMctWRRDdr:
      HashPtr->Data = MC0_CH0_CR_TC_WRRD_tWRRD_dr_HSH;
      break;

    case GsmMctWRRDdd:
      HashPtr->Data = MC0_CH0_CR_TC_WRRD_tWRRD_dd_HSH;
      break;

    case GsmMctWRWRsg:
      HashPtr->Data = MC0_CH0_CR_TC_WRWR_tWRWR_sg_HSH;
      break;

    case GsmMctWRWRdg:
      HashPtr->Data = MC0_CH0_CR_TC_WRWR_tWRWR_dg_HSH;
      break;

    case GsmMctWRWRdr:
      HashPtr->Data = MC0_CH0_CR_TC_WRWR_tWRWR_dr_HSH;
      break;

    case GsmMctWRWRdd:
      HashPtr->Data = MC0_CH0_CR_TC_WRWR_tWRWR_dd_HSH;
      break;

    case GsmMctOdtRdDuration:
      HashPtr->Data = MC0_CH0_CR_TC_ODT_ODT_read_duration_HSH;
      break;

    case GsmMctOdtRdDelay:
      HashPtr->Data = MC0_CH0_CR_TC_ODT_ODT_Read_Delay_HSH;
      break;

    case GsmMctWrEarlyOdt:
      HashPtr->Data = MC0_CH0_CR_TC_ODT_Write_Early_ODT_HSH;
      break;

    case GsmMctOdtWrDuration:
      HashPtr->Data = MC0_CH0_CR_TC_ODT_ODT_write_duration_HSH;
      break;

    case GsmMctOdtWrDelay:
      HashPtr->Data = MC0_CH0_CR_TC_ODT_ODT_Write_Delay_HSH;
      break;

    case GsmMctCL:
      HashPtr->Data = MC0_CH0_CR_TC_ODT_tCL_HSH;
      break;

    case GsmMctCWL:
      HashPtr->Data = MC0_CH0_CR_TC_ODT_tCWL_HSH;
      break;

    case GsmMctCWLAdd:
      HashPtr->Data = MC0_CH0_CR_SC_WR_DELAY_Add_tCWL_HSH;
      break;

    case GsmMctCWLDec:
      HashPtr->Data = MC0_CH0_CR_SC_WR_DELAY_Dec_tCWL_HSH;
      break;

    case GsmMctAONPD:
      HashPtr->Data = MC0_CH0_CR_TC_ODT_tAONPD_HSH;
      break;

    case GsmMctCKE:
      HashPtr->Data = MC0_CH0_CR_TC_PWRDN_tCKE_HSH;
      break;

    case GsmMctXP:
      HashPtr->Data = MC0_CH0_CR_TC_PWRDN_tXP_HSH;
      break;

    case GsmMctXPDLL:
      HashPtr->Data = MC0_CH0_CR_TC_PWRDN_tXPDLL_HSH;
      break;

    case GsmMctPRPDEN:
      HashPtr->Data = MC0_CH0_CR_TC_PWRDN_tPRPDEN_HSH;
      break;

    case GsmMctRDPDEN:
      HashPtr->Data = MC0_CH0_CR_TC_PWRDN_tRDPDEN_HSH;
      break;

    case GsmMctWRPDEN:
      HashPtr->Data = MC0_CH0_CR_TC_PWRDN_tWRPDEN_HSH;
      break;

    case GsmMctCA2CS:
      HashPtr->Data = MC0_CH0_CR_TC_PWRDN_tCA2CS_HSH;
      break;

    case GsmMctCSL:
      HashPtr->Data = MC0_CH0_CR_TC_PWRDN_tCSL_HSH;
      break;

    case GsmMctCSH:
      HashPtr->Data = MC0_CH0_CR_TC_PWRDN_tCSH_HSH;
      break;

    case GsmMctXSDLL:
      HashPtr->Data = MC0_CH0_CR_TC_SRFTP_tXSDLL_HSH;
      break;

    case GsmMctXSR:
      HashPtr->Data = MC0_CH0_CR_TC_SREXITTP_tXSR_HSH;
      break;

    case GsmMctSR:
      HashPtr->Data = MC0_CH0_CR_TC_SREXITTP_tSR_HSH;
      break;

    case GsmMctZQOPER:
      HashPtr->Data = MC0_CH0_CR_TC_SRFTP_tZQOPER_HSH;
      break;

    case GsmMctMOD:
      HashPtr->Data = MC0_CH0_CR_TC_SRFTP_tMOD_HSH;
      break;

    case GsmMctZQCAL:
      HashPtr->Data = MC0_CH0_CR_TC_ZQCAL_tZQCAL_HSH;
      break;

    case GsmMctZQCS:
      HashPtr->Data = MC0_CH0_CR_TC_ZQCAL_tZQCS_HSH;
      break;

    case GsmMctZQCSPeriod:
      HashPtr->Data = MC0_CH0_CR_TC_ZQCAL_ZQCS_period_HSH;
      break;

    case GsmMctCPDED:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_tCPDED_HSH;
      break;

    case GsmMctCAL:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_tCAL_HSH;
      break;

    case GsmMctCKCKEH:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_ck_to_cke_HSH;
      break;

    case GsmMctCSCKEH:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_cs_to_cke_HSH;
      break;

    case GsmMctSrIdle:
      HashPtr->Data = MC0_PM_SREF_CONFIG_Idle_timer_HSH;
      break;

    case GsmMctREFI:
      HashPtr->Data = MC0_CH0_CR_TC_RFTP_tREFI_HSH;
      break;

    case GsmMctHpRefOnMrs:
      HashPtr->Data = MC0_CH0_CR_TC_RFP_HPRefOnMRS_HSH;
      break;

    case GsmMctRFC:
      HashPtr->Data = MC0_CH0_CR_TC_RFTP_tRFC_HSH;
      break;

    case GsmMctOrefRi:
      HashPtr->Data = MC0_CH0_CR_TC_RFP_OREF_RI_HSH;
      break;

    case GsmMctRefreshHpWm:
      HashPtr->Data = MC0_CH0_CR_TC_RFP_Refresh_HP_WM_HSH;
      break;

    case GsmMctRefreshPanicWm:
      HashPtr->Data = MC0_CH0_CR_TC_RFP_Refresh_panic_wm_HSH;
      break;

    case GsmMctREFIx9:
      HashPtr->Data = MC0_CH0_CR_TC_RFP_tREFIx9_HSH;
      break;

    case GsmMctSrxRefDebits:
      HashPtr->Data = MC0_CH0_CR_TC_RFP_SRX_Ref_Debits_HSH;
      break;

    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Hash undefined. Group %s(%d) Channel %u\n", GsmGtDebugStrings[Group], Group,Channel);
      HashPtr->Data = MRC_UINT32_MAX;
      Status = mrcWrongInputParameter;
      break;
  }

  if (Status == mrcSuccess) {
    CrOffset = MrcGetMcTimingRegOffset (MrcData, Group, Socket, Controller, Channel, FreqIndex);
  }

  HashPtr->Bits.Offset = CrOffset;

  return Status;
}

/**
  This function returns the hash used to execute the Get/Set function.
  The hash consists of: Register Offset, BitField start bit, BitField length.

  @param[in]  MrcData     - Pointer to global data.
  @param[in]  Group       - MC Timing group to access.
  @param[in]  Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Controller  - Controller number within the processor socket (0-based).
  @param[in]  Channel     - DDR Channel Number within the controller (0-based).
  @param[in]  Rank        - Rank number within a channel (0-based).
  @param[in]  Strobe     - Strobe number within a channel (0-based).
  @param[in]  Bit        - Bit number within a Byte (0-based).
  @param[in]  FreqIndex   - Index supporting multiple operating frequencies. (Not used in Client CPU's)
  @param[out] HashVal     - Pointer to return the MMIO access instruction.

  @retval MrcStatus
**/
MrcStatus
MrcGetMcConfigHash (
  IN  MrcParameters   *const  MrcData,
  IN  GSM_GT          const   Group,
  IN  UINT32          const   Socket,
  IN  UINT32          const   Controller,
  IN  UINT32          const   Channel,
  IN  UINT32          const   Rank,
  IN  UINT32          const   Strobe,
  IN  UINT32          const   Bit,
  IN  UINT32          const   FreqIndex,
  OUT UINT32          *const  HashVal
  )
{
  MrcStatus Status;
  MrcDebug  *Debug;
  MRC_REGISTER_HASH_STRUCT *HashPtr;
  UINT32    CrOffset;
  UINT32    RrdValidIndex;
  MrcOutput *Outputs;
  BOOLEAN   Lpddr;
  static const UINT32 RrdValidTriggerHsh[ROUNDTRIP_DELAY_MAX] = {
    MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_trigger_HSH, MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_trigger_HSH,
    MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_trigger_HSH, MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_trigger_HSH,
    MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_trigger_HSH, MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_trigger_HSH,
    MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_trigger_HSH, MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_trigger_HSH
  };
  static const UINT32 RrdValidOverflowHsh[ROUNDTRIP_DELAY_MAX] = {
    MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_overflow_HSH, MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_overflow_HSH,
    MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_overflow_HSH, MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_overflow_HSH,
    MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_overflow_HSH, MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_overflow_HSH,
    MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_overflow_HSH, MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_overflow_HSH
  };
  static const UINT32 RrdValidValueHsh[ROUNDTRIP_DELAY_MAX] = {
    MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_value_HSH, MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_value_HSH,
    MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_value_HSH, MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_value_HSH,
    MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_value_HSH, MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_value_HSH,
    MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_value_HSH, MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_value_HSH
  };
  static const UINT32 RrdValidSignHsh[ROUNDTRIP_DELAY_MAX] = {
    MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_sign_HSH, MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_sign_HSH,
    MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_sign_HSH, MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_sign_HSH,
    MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_sign_HSH, MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_sign_HSH,
    MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_sign_HSH, MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_sign_HSH
  };
  static const UINT32 DeswizzleBitHsh[4][5] = { // 4 Bytes, 5 Bits per Byte
    { MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_0_HSH,   MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_1_HSH,
      MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_2_HSH,   MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_3_HSH,  MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_4_HSH },
    { MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_8_HSH,   MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_9_HSH,
      MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_10_HSH,  MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_11_HSH,  MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_12_HSH },
    { MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_16_HSH, MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_17_HSH,
      MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_18_HSH, MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_19_HSH, MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_20_HSH },
    { MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_24_HSH, MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_25_HSH,
      MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_26_HSH, MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_27_HSH, MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_28_HSH }
  };

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  Status        = mrcSuccess;
  HashPtr       = (MRC_REGISTER_HASH_STRUCT *) HashVal;
  HashPtr->Data = MRC_UINT32_MAX;
  CrOffset      = MRC_UINT32_MAX;
  Lpddr         = Outputs->Lpddr;

  if (Lpddr) {
    // Note: Channel is a system-level channel here, because the register fields are per subch in LP4/5 (like RoundTripDelay)
    RrdValidIndex = (2 * (Channel % 2)) + (4 * (Rank / 2)) + (Rank % 2);
  } else {
    RrdValidIndex = Rank % 4;
  }

  switch (Group) {
    case GsmMccDramType:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_DRAM_technology_HSH;
      break;

    case GsmMccCmdStretch:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_CMD_stretch_HSH;
      break;

    case GsmMccCmdGapRatio:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_N_to_1_ratio_HSH;
      break;

    case GsmMccAddrMirror:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_Address_mirror_HSH;
      break;

    case GsmMccCmdTriStateDis:
      HashPtr->Data = (MrcData->Inputs.A0) ? MC0_CH0_CR_SC_GS_CFG_disable_tristate_A0_HSH : MC0_CH0_CR_SC_GS_CFG_disable_tristate_HSH;
      break;

    case GsmMccCmdTriStateDisTrain:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_disable_tristate_HSH;
      break;

    case GsmMccFreqPoint:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_frequency_point_HSH;
      break;

    case GsmMccEnableOdtMatrix:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_enable_odt_matrix_HSH;
      break;

    case GsmMccX8Device:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_x8_device_HSH;
      break;

    case GsmMccGear2:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_gear2_HSH;
      break;

    case GsmMccGear4:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_gear4_HSH;
      break;

    case GsmMccDdr4OneDpc:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_ddr_1dpc_split_ranks_on_subch_HSH;
      break;

    case GsmMccWrite0En:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_write0_enable_HSH;
      break;

    case GsmMccLp5BankMode:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_lpddr5_bg_mode_HSH;
      break;
    case GsmMccWCKDiffLowInIdle:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_WCKDiffLowInIdle_HSH;
      break;

    case GsmMccMultiCycCmd:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_MultiCycCmd_HSH;
      break;

    case GsmMccLDimmMap:
      HashPtr->Data = MC0_MAD_INTRA_CH0_DIMM_L_MAP_HSH;
      break;

    case GsmMccEnhancedInterleave:
      HashPtr->Data = MC0_MAD_INTRA_CH0_EIM_HSH;
      break;

    case GsmMccEccMode:
      HashPtr->Data = MC0_MAD_INTRA_CH0_ECC_HSH;
      break;

    case GsmMccAddrDecodeDdrType:
      HashPtr->Data = MC0_MAD_INTER_CHANNEL_DDR_TYPE_HSH;
      break;

    case GsmMccLChannelMap:
      HashPtr->Data = MC0_MAD_INTER_CHANNEL_CH_L_MAP_HSH;
      break;

    case GsmMccSChannelSize:
      HashPtr->Data = MC0_MAD_INTER_CHANNEL_CH_S_SIZE_HSH;
      break;

    case GsmMccChWidth:
      HashPtr->Data = MC0_MAD_INTER_CHANNEL_CH_WIDTH_HSH;
      break;

    case GsmMccHalfCachelineMode:
      HashPtr->Data = MC0_MAD_INTER_CHANNEL_HalfCacheLineMode_HSH;
      break;

    case GsmMccLDimmSize:
      HashPtr->Data = MC0_MAD_DIMM_CH0_DIMM_L_SIZE_HSH;
      break;

    case GsmMccLDimmDramWidth:
      HashPtr->Data = MC0_MAD_DIMM_CH0_DLW_HSH;
      break;

    case GsmMccLDimmRankCnt:
      HashPtr->Data = MC0_MAD_DIMM_CH0_DLNOR_HSH;
      break;

    case GsmMccSDimmSize:
      HashPtr->Data = MC0_MAD_DIMM_CH0_DIMM_S_SIZE_HSH;
      break;

    case GsmMccSDimmDramWidth:
      HashPtr->Data = MC0_MAD_DIMM_CH0_DSW_HSH;
      break;

    case GsmMccSDimmRankCnt:
      HashPtr->Data = MC0_MAD_DIMM_CH0_DSNOR_HSH;
      break;

    case GsmMccExtendedBankHashing:
      HashPtr->Data = MC0_MAD_DIMM_CH0_Decoder_EBH_HSH;
      break;

    case GsmMccDdr5Device8GbDimmL:
      HashPtr->Data = MC0_MAD_DIMM_CH0_ddr5_dl_8Gb_HSH;
      break;

    case GsmMccDdr5Device8GbDimmS:
      HashPtr->Data = MC0_MAD_DIMM_CH0_ddr5_ds_8Gb_HSH;
      break;

    case GsmMccSaveFreqPoint:
      switch (FreqIndex) {
        case 0:
          HashPtr->Data = MC0_MC_INIT_STATE_G_mrc_save_gv_point_0_HSH;
          break;

        case 1:
          HashPtr->Data = MC0_MC_INIT_STATE_G_mrc_save_gv_point_1_HSH;
          break;

        case 2:
          HashPtr->Data = MC0_MC_INIT_STATE_G_mrc_save_gv_point_2_HSH;
          break;

        case 3:
          HashPtr->Data = MC0_MC_INIT_STATE_G_mrc_save_gv_point_3_HSH;
          break;

        default:
          Status = mrcWrongInputParameter;
          break;
      }
      break;

    case GsmMccEnableSr:
      HashPtr->Data = MC0_PM_SREF_CONFIG_SR_Enable_HSH;
      break;

    case GsmMccEnableRefresh:
      HashPtr->Data = MC0_MC_INIT_STATE_G_refresh_enable_HSH;
      break;

    case GsmMccMcInitDoneAck:
      HashPtr->Data = MC0_MC_INIT_STATE_G_mc_init_done_ack_HSH;
      break;

    case GsmMccMrcDone:
      HashPtr->Data = MC0_MC_INIT_STATE_G_mrc_done_HSH;
      break;

    case GsmMccEnableDclk:
      HashPtr->Data = MC0_MC_INIT_STATE_G_dclk_enable_HSH;
      break;

    case GsmMccPureSrx:
      HashPtr->Data = MC0_MC_INIT_STATE_G_pure_srx_HSH;
      break;

    case GsmMccLp4FifoRdWr:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_lp4_fifo_rd_wr_HSH;
      break;

    case GsmMccIgnoreCke:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_ignore_cke_HSH;
      break;

    case GsmMccMaskCs:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_mask_cs_HSH;
      break;

    case GsmMccCpgcInOrder:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_cpgc_in_order_HSH;
      break;

    case GsmMccInOrderIngress:
      HashPtr->Data = MC0_MC_CPGC_MISC_DFT_in_order_ingress_HSH;
      break;

    case GsmMccCadbEnable:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_cadb_enable_HSH;
      break;

    case GsmMccDeselectEnable:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_deselect_enable_HSH;
      break;

    case GsmMccBusRetainOnBubble:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_bus_retain_on_n_to_1_bubble_HSH;
      break;

    case GsmMccBlockXarb:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_xarb_HSH;
      break;

    case GsmMccBlockCke:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_cke_HSH;
      break;

    case GsmMccResetOnCmd:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_on_command_HSH;
      break;

    case GsmMccResetDelay:
      HashPtr->Data = MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_delay_HSH;
      break;

    case GsmMccRankOccupancy:
      HashPtr->Data = MC0_CH0_CR_MC_INIT_STATE_Rank_occupancy_HSH;
      break;

    case GsmMccRefInterval:
      HashPtr->Data = MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Interval_HSH;
      break;

    case GsmMccRefreshAbrRelease:
      HashPtr->Data = MC0_CH0_CR_SC_PBR_Refresh_ABR_release_HSH;
      break;

    case GsmMccRefStaggerEn:
      HashPtr->Data = MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_En_HSH;
      break;

    case GsmMccRefStaggerMode:
      HashPtr->Data = MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_Mode_HSH;
      break;

    case GsmMccDisableStolenRefresh:
      HashPtr->Data = MC0_CH0_CR_MC_REFRESH_STAGGER_Disable_Stolen_Refresh_HSH;
      break;

    case GsmMccEnRefTypeDisplay:
      HashPtr->Data = MC0_CH0_CR_MC_REFRESH_STAGGER_En_Ref_Type_Display_HSH;
      break;

    case GsmMccHashMask:
      HashPtr->Data = MC0_CHANNEL_HASH_HASH_MASK_HSH;
      break;

    case GsmMccLsbMaskBit:
      HashPtr->Data = MC0_CHANNEL_HASH_HASH_LSB_MASK_BIT_HSH;
      break;

    case GsmMccHashMode:
      HashPtr->Data = MC0_CHANNEL_HASH_HASH_MODE_HSH;
      break;

    case GsmMccDisableCkTristate:
      HashPtr->Data = MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ck_tristate_HSH;
      break;

    case GsmMccPbrDis:
      HashPtr->Data = MC0_CH0_CR_SC_PBR_PBR_Disable_HSH;
      break;

    case GsmMccPbrIssueNop:
      HashPtr->Data = MC0_CH0_CR_SC_PBR_PBR_Issue_NOP_HSH;
      break;

    case GsmMccPbrDisOnHot:
      HashPtr->Data = MC0_CH0_CR_SC_PBR_PBR_Disable_on_hot_HSH;
      break;

    case GsmMccOdtOverride:
      HashPtr->Data = MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_Override_HSH;
      break;

    case GsmMccOdtOn:
      HashPtr->Data = MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_On_HSH;
      break;

    case GsmMccMprTrainDdrOn:
      HashPtr->Data = MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_MPR_Train_DDR_On_HSH;
      break;

    case GsmMccCkeOverride:
      HashPtr->Data = MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_HSH;
      break;

    case GsmMccCkeOn:
      HashPtr->Data = MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_On_HSH;
      break;

    case GsmMccCsOverride:
      HashPtr->Data = MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_0_HSH;
      break;

    case GsmMccCsOverrideVal:
      HashPtr->Data = MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_0_HSH;
      break;

    case GsmMccRrdValidTrigger:
      if (RrdValidIndex < ROUNDTRIP_DELAY_MAX) {
        HashPtr->Data = RrdValidTriggerHsh[RrdValidIndex];
      }
      break;

    case GsmMccRrdValidOverflow:
      if (RrdValidIndex < ROUNDTRIP_DELAY_MAX) {
        HashPtr->Data = RrdValidOverflowHsh[RrdValidIndex];
      }
      break;

    case GsmMccRrdValidValue:
      if (RrdValidIndex < ROUNDTRIP_DELAY_MAX) {
        HashPtr->Data = RrdValidValueHsh[RrdValidIndex];
      }
      break;

    case GsmMccRrdValidSign:
      if (RrdValidIndex < ROUNDTRIP_DELAY_MAX) {
        HashPtr->Data = RrdValidSignHsh[RrdValidIndex];
      }
      break;

    case GsmMccDeswizzleByte:
      // Note: Channel is a system-level channel here, because we need the per-subch resolution for LP4/5
      if (Lpddr) {
        if ((Channel == 0) || (Channel == 2)) {
          HashPtr->Data = (Strobe == 0) ? MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_HSH : MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_1_HSH;
        } else {
          HashPtr->Data = (Strobe == 0) ? MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_0_HSH : MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_1_HSH;
        }
      } else {
        if (Strobe < 2) {
          HashPtr->Data = (Strobe == 0) ? MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_HSH : MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_1_HSH;
        } else if (Strobe < 4) {
          HashPtr->Data = (Strobe == 2) ? MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_0_HSH : MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_1_HSH;
        } else {
          Status = mrcWrongInputParameter;
        }
      }
      break;

    case GsmMccDeswizzleBit:
      if ((Strobe < 4) && (Bit < 5)) {
        HashPtr->Data = DeswizzleBitHsh[Strobe][Bit];
      } else {
        Status = mrcWrongInputParameter;
      }
      break;

      // These are unused.  Need to finish HAL if these fields are accessed.
    case GsmMccDdrReset:
    case GsmMccSafeSr:

    default:
      break;
  }

  if (HashPtr->Data == MRC_UINT32_MAX) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Hash undefined. Group %s(%d) Channel %u\n", GsmGtDebugStrings[Group], Group, Channel);
    Status = mrcWrongInputParameter;
  }

  if (Status == mrcSuccess) {
    CrOffset = MrcGetMcConfigRegOffset (MrcData, Group, Socket, Controller, Channel, Rank, Strobe, Bit, FreqIndex);
  }

  HashPtr->Bits.Offset = CrOffset;

  return Status;
}

/**
  This function returns the hash used to execute the Get/Set function.
  The hash consists of: Register Offset, BitField start bit, BitField length.

  @param[in]  MrcData    - Pointer to global data.
  @param[in]  Controller - Controller number within the processor socket (0-based).
  @param[in]  Group      - CMI group to access.
  @param[in]  Socket     - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Channel    - DDR Channel Number within the system (0-based).
  @param[in]  FreqIndex  - Index supporting multiple operating frequencies. (Not used in Client CPU's)
  @param[out] HashVal    - Pointer to return the MMIO access instruction.

  @retval MrcStatus
**/
MrcStatus
MrcGetCmiHash (
  IN  MrcParameters   *const  MrcData,
  IN  UINT32          const   Controller,
  IN  GSM_GT          const   Group,
  IN  UINT32          const   Socket,
  IN  UINT32          const   Channel,
  IN  UINT32          const   FreqIndex,
  OUT UINT32          *const  HashVal
  )
{
  MrcStatus Status;
  MrcDebug  *Debug;
  MRC_REGISTER_HASH_STRUCT *HashPtr;
  UINT32 CrOffset;

  Debug   = &MrcData->Outputs.Debug;
  Status  = mrcSuccess;
  HashPtr = (MRC_REGISTER_HASH_STRUCT *) HashVal;
  CrOffset      = MRC_UINT32_MAX;
  HashPtr->Data = MRC_UINT32_MAX;

  switch (Group) {
    case GsmCmiMcOrg:
      HashPtr->Data = CMI_MEMORY_SLICE_HASH_MC_org_HSH;
      break;

    case GsmCmiHashMask:
      HashPtr->Data = CMI_MEMORY_SLICE_HASH_HASH_MASK_HSH;
      break;

    case GsmCmiLsbMaskBit:
      HashPtr->Data = CMI_MEMORY_SLICE_HASH_HASH_LSB_MASK_BIT_HSH;
      break;

    case GsmCmiStackedMode:
      HashPtr->Data = CMI_MAD_SLICE_STKD_MODE_HSH;
      break;

    case GsmCmiStackMsMap:
      HashPtr->Data = CMI_MAD_SLICE_STKD_MODE_MS1_HSH;
      break;

    case GsmCmiLMadSliceMap:
      HashPtr->Data = CMI_MAD_SLICE_MS_L_MAP_HSH;
      break;

    case GsmCmiSMadSliceSize:
      HashPtr->Data = CMI_MAD_SLICE_MS_S_SIZE_HSH;
      break;

    case GsmCmiStackedMsHash:
      HashPtr->Data = CMI_MAD_SLICE_STKD_MODE_MS_BITS_HSH;
      break;

    case GsmCmiSliceDisable:
      HashPtr->Data = (Controller == 0) ? CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_0_DISABLE_HSH : CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_1_DISABLE_HSH;
      break;

    default:
      break;
  }

  if (HashPtr->Data == MRC_UINT32_MAX) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Hash undefined. Group %s(%d) Channel %u\n", GsmGtDebugStrings[Group], Group, Channel);
    Status = mrcWrongInputParameter;
  }
  if (Status == mrcSuccess) {
    CrOffset = MrcGetCmiRegOffset (MrcData, Group, Socket, Channel, FreqIndex);
  }

  HashPtr->Bits.Offset = CrOffset;

  return Status;
}

///
/// Public Functions
///
/**
  This function is the interface for the core of the MRC to get the limits of a
  specific GSM_GT parameter.

  @param[in]  MrcData   - Pointer to global data.
  @param[in]  Group     - DDRIO group to access.
  @param[out] MinVal    - Return pointer for Minimum Value supported.
  @param[out] MaxVal    - Return pointer for Maximum Value supported.
  @param[out] WaitTime  - Return pointer for settle time required in microseconds.

  @retval MrcStatus - mrcSuccess if the parameter is found, otherwise mrcFail.
**/
MrcStatus
MrcGetSetLimits (
  IN  MrcParameters *const MrcData,
  IN  GSM_GT  const   Group,
  OUT INT64   *const  MinVal,
  OUT INT64   *const  MaxVal,
  OUT UINT32  *const  WaitTime
  )
{
  MrcStatus Status;

  Status = mrcSuccess;

  if (Group < EndOfPhyMarker) {
    MrcGetDdrIoGroupLimits (MrcData, Group, MinVal, MaxVal, WaitTime);
  } else if (Group < EndOfIocMarker) {
    MrcGetDdrIoCfgGroupLimits (MrcData, Group, MinVal, MaxVal, WaitTime);
  } else if (Group < EndOfMctMarker) {
    MrcGetMcTimingGroupLimits (MrcData, Group, MinVal, MaxVal, WaitTime);
  } else if (Group < EndOfMccMarker) {
    MrcGetMcConfigGroupLimits (MrcData, Group, MinVal, MaxVal, WaitTime);
  } else if (Group < EndOfCmiMarker){
    MrcGetCmiGroupLimits (MrcData, Group, MinVal, MaxVal, WaitTime);
  } else {
    MrcGetComplexGroupLimits (MrcData, Group, MinVal, MaxVal, WaitTime);
  }

  return Status;
}

#ifdef MRC_DEBUG_PRINT
/**
  This function outputs the specified group values to the debug print device.

  @param[in] MrcData   - Pointer to global data.
  @param[in] Group     - DDRIO group to access.
  @param[in] Socket    - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in] Controller- DDR Memory Controller Number within the processor socket (0-based).
  @param[in] Channel   - DDR Channel Number within the memory controller (0-based).
  @param[in] Rank      - Rank number within a channel (0-based).
  @param[in] Strobe    - Dqs data group within the rank (0-based).
  @param[in] Bit       - Bit index within the data group (0-based).
  @param[in] FreqIndex - Index supporting multiple operating frequencies. (Not used in Client CPU's)

  @retval MrcStatus
**/
MrcStatus
MrcPrintDdrIoGroup (
  IN MrcParameters *const  MrcData,
  IN UINT32  const Socket,
  IN UINT32  const Controller,
  IN UINT32  const Channel,
  IN UINT32  const Rank,
  IN UINT32  const Strobe,
  IN UINT32  const Bit,
  IN UINT32  const FreqIndex,
  IN GSM_GT  const Group
  )
{
  MrcOutput     *Outputs;
  MrcDebug      *Debug;
  INT64_STRUCT  Value;
  UINT32        ControllerIndex;
  UINT32        ControllerMax;
  UINT32        ControllerStart;
  UINT32        ChannelIndex;
  UINT32        ChannelMax;
  UINT32        ChannelStart;
  UINT32        RankIndex;
  UINT32        RankMax;
  UINT32        RankStart;
  UINT32        Byte;
  UINT32        ByteMax;
  UINT32        ByteStart;
  UINT32        BitIndex;
  UINT32        BitMax;
  UINT32        BitStart;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  if ((Strobe >= MAX_SDRAM_IN_DIMM) && (Strobe != MRC_IGNORE_ARG)) {
    ByteMax   = Outputs->SdramCount;
    ByteStart = 0;
  } else {
    ByteMax   = Strobe + 1;
    ByteStart = Strobe;
  }
  if ((Bit >= MAX_BITS) && (Strobe != MRC_IGNORE_ARG)) {
    BitMax    = MAX_BITS;
    BitStart  = 0;
  } else {
    BitMax    = Bit + 1;
    BitStart  = Bit;
  }
  if ((Channel >= MAX_CHANNEL) && (Channel != MRC_IGNORE_ARG)) {
    ChannelMax    = Outputs->MaxChannels;
    ChannelStart  = 0;
  } else {
    ChannelMax    = Channel + 1;
    ChannelStart  = Channel;
  }
  if ((Rank >= MAX_RANK_IN_CHANNEL) && (Rank != MRC_IGNORE_ARG)) {
    RankMax   = MAX_RANK_IN_CHANNEL;
    RankStart = 0;
  } else {
    RankMax   = Rank + 1;
    RankStart = Rank;
  }
  if ((Controller >= MAX_CONTROLLER) && (Controller != MRC_IGNORE_ARG)) {
    ControllerMax    = MAX_CONTROLLER;
    ControllerStart  = 0;
  } else {
    ControllerMax    = Controller + 1;
    ControllerStart  = Controller;
  }

  switch (Group) {
    case RecEnDelay:
      for (ControllerIndex = ControllerStart; ControllerIndex < ControllerMax; ControllerIndex++) {
        for (ChannelIndex = ChannelStart; ChannelIndex < ChannelMax; ChannelIndex++) {
          for (RankIndex = RankStart; RankIndex < RankMax; RankIndex++) {
            if (!MrcRankExist (MrcData, ControllerIndex, ChannelIndex, RankIndex)) {
              continue;
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RecEnDelay Mc %u Channel %u Rank %u:\n", ControllerIndex, ChannelIndex, RankIndex);
            for (Byte = ByteStart; Byte < ByteMax; Byte++) {
              if (MrcByteExist (MrcData, ControllerIndex, ChannelIndex, Byte)) {
                MrcGetSetStrobe (MrcData, ControllerIndex, ChannelIndex, RankIndex, Byte, RecEnDelay, GSM_READ_ONLY, &Value.Data);
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " B%u: %d\n", Byte, Value.Data32.Low);
              }
            } // Byte
          } // Rank
        } // Channel
      } // Controller
      break;

    case RxDqsNBitDelay:
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "RxDqsNBitDelay Mc %u Channel %u Rank %u Byte %u:", Controller, Channel, Rank, Strobe);
      for (BitIndex = BitStart; BitIndex < BitMax; BitIndex++) {
        MrcGetSetBit(MrcData, Controller, Channel, Rank, Strobe, BitIndex, RxDqsNBitDelay, GSM_READ_ONLY, &Value.Data);
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, " %02X", Value.Data32.Low);
      } // Bit
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");
      break;

    case RxDqsPBitDelay:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RxDqsPBitDelay Mc %u Channel %u Rank %u Byte %u:", Controller, Channel, Rank, Strobe);
      for (BitIndex = BitStart; BitIndex < BitMax; BitIndex++) {
        MrcGetSetBit (MrcData, Controller, Channel, Rank, Strobe, BitIndex, RxDqsPBitDelay, GSM_READ_ONLY, &Value.Data);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %02X", Value.Data32.Low);
      } // Bit
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
      break;

    default:
      break;
  }
  return (mrcSuccess);
}
#endif // MRC_DEBUG_PRINT

/**
  Update MRC Host Data Structure when GSM_UPDATE_HOST is set
  Currently only used for RxDqsBitDelay and TxDqBitDelay

  @param[in]      MrcData     - Pointer to global data structure.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]      Rank        - Rank number within a channel (0-based).
  @param[in]      Strobe      - If Group is a CMD/CTL/CLK Index type, this is the index for that signal.  Otherwise, Dqs data group within the rank (0-based).
  @param[in]      Bit         - Bit index within the data group (0-based).
  @param[in]      Group       - DDRIO group to access.
  @param[in,out]  Value       - Pointer to value for Get/Set to operate on.  Can be offset or absolute value based on mode.

**/
void
MrcGetSetUpdateHost (
  IN      MrcParameters *const  MrcData,
  IN      UINT32        const   Controller,
  IN      UINT32        const   Channel,
  IN      UINT32        const   Rank,
  IN      UINT32        const   Strobe,
  IN      UINT32        const   Bit,
  IN      GSM_GT        const   Group,
  IN OUT  INT64         *const  Value
  )
{
  MrcOutput          *Outputs;
  MrcControllerOut   *ControllerOut;
  MrcChannelOut      *ChannelOut;

  Outputs = &MrcData->Outputs;
  ControllerOut = &Outputs->Controller[Controller];
  ChannelOut = &ControllerOut->Channel[Channel];

  switch (Group) {
    //case RxDqsBitDelay: // @todo_adl RxDqsBitDelay became complex parameter
    //  ChannelOut->RxDqPb[Rank][Strobe][Bit].Center = (UINT8) *Value;
    //  break;

    case TxDqBitDelay:
      ChannelOut->TxDqPb[Rank][Strobe][Bit].Center = (UINT8) *Value;

    default:
      break;
  }

}

