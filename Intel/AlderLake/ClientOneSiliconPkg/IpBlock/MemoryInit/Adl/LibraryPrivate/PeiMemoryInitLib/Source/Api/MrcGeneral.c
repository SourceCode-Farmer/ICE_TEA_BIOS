/** @file
  This file contains all the MRC general API to the MRC wrapper.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include "MrcTypes.h"
#include "MrcApi.h"
#include "MrcGeneral.h"
#include "MrcMemoryApi.h"
#include "MrcDdr3.h"
#include "MrcDdr4.h"
#include "MrcDdr5.h"
#include "MrcLpddr4.h"
#include "MrcLpddr5.h"
#include "MrcStartMemoryConfiguration.h"
#include "MrcTimingConfiguration.h"
#include "MrcSchedulerParameters.h"
#include "MrcCpgcApi.h"
#include "MrcGears.h"
#include "MrcChipApi.h"
#include "MrcMaintenance.h"
#include "MrcMcConfiguration.h"

const MrcVersionWithSignature cVersion = {
  0x5F52455643524D5FULL,    // SIGNATURE_64 ('_','M','R','C','V','E','R','_');
  #include "MrcVersion.h"
};

// Number of Scrambler Groups
#define MRC_NUM_SCRAM (8)

// RCOMP target values for { RdOdt, WrDS, WrDSCmd, WrDSCtl, WrDSClk } - per DDR Type
const UINT16 RcompTargetLpddr4[MAX_RCOMP_TARGETS]      = { 80, 40, 40, 40, 30 };
const UINT16 RcompTargetLpddr4Lb[MAX_RCOMP_TARGETS]    = { 80,  5, 20, 20, 20 };   // Loopback mode
const UINT16 RcompTargetLpddr4x[MAX_RCOMP_TARGETS]     = { 40, 30, 30, 30, 30 };
const UINT16 RcompTargetLpddr5[MAX_RCOMP_TARGETS]      = { 48, 30, 20, 20, 20 };
const UINT16 RcompTargetLpddr5LowVddq[MAX_RCOMP_TARGETS] = { 48, 30, 20, 20, 20 };
const UINT16 RcompTargetUDdr4[MAX_RCOMP_TARGETS]       = { 50, 20, 25, 25, 25 };
const UINT16 RcompTargetSDdr4[MAX_RCOMP_TARGETS]       = { 50, 20, 25, 25, 25 };
const UINT16 RcompTargetSDdr4Sn641A2[MAX_RCOMP_TARGETS]= { 50, 25, 25, 25, 25 };
const UINT16 RcompTargetDdr4Lb[MAX_RCOMP_TARGETS]      = { 100, 10, 32, 33, 28 };  // Loopback mode
const UINT16 RcompTargetSDdr5[MAX_RCOMP_TARGETS]       = { 50, 16, 30, 30, 27 };
const UINT16 RcompTargetSDdr5TwoDpc[MAX_RCOMP_TARGETS] = { 50, 20, 16, 30, 27 };

const UINT16 RcompTargetDdr4_OC[MAX_RCOMP_TARGETS]            = { 30, 20, 20, 20, 20 };
const UINT16 RcompTargetDdr4_ExtremeOC[MAX_RCOMP_TARGETS]     = { 30, 15, 20, 20, 20 };

const MrcFrequency SagvFreqPorA0[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS] = {
//     0,     1,     2,     3
  {f2133, f2400, f2667, f2667}, // DDR4
  {f2000, f2000, f3200, f3200}, // DDR5
  {f2200, f3200, f4400, f5400}, // LPDDR5
  {f2133, f3200, f4267, f2933}  // LPDDR4
};
const UINT8 SaGvGearPorA0[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS] = {
//     0,     1,     2,     3
  {    2,     2,     2,     2}, // DDR4
  {    2,     2,     2,     2}, // DDR5
  {    2,     2,     2,     2}, // LPDDR5
  {    2,     2,     2,     1}  // LPDDR4
};

const MrcFrequency SagvFreqPor[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS] = {
//     0,     1,     2,     3
  {f2133, f2667, f2933, f3200}, // DDR4
  {f2000, f3600, f4400, f4800}, // DDR5
  {f2200, f3200, f4400, f5400}, // LPDDR5
  {f2133, f3200, f4267, f2933}  // LPDDR4
};
const UINT8 SaGvGearPor[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS] = {
//     0,     1,     2,     3
  {    2,     1,     1,     1}, // DDR4
  {    2,     2,     2,     2}, // DDR5
  {    2,     2,     2,     2}, // LPDDR5
  {    2,     2,     2,     1}  // LPDDR4
};

// POR SAGV Frequency Points for ADL-P L,R step and beyond
const MrcFrequency SagvFreqPorU[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS] = {
  //   0,     1,     2,     3
  {f2133, f2933, f3200, f2667}, // DDR4
  {f2000, f4400, f4800, f4800}, // DDR5
  {f2400, f4800, f5200, f5200}, // LPDDR5
  {f2667, f3733, f4267, f4267}  // LPDDR4
};

// POR SAGV Frequency Points for ADL-P LP5 Type3 board
const MrcFrequency SagvFreqPorLp5T3[MAX_SAGV_POINTS] = {
  //   0,     1,     2,     3
   f2400, f4400, f4800, f4800   // LPDDR5
};

// POR SAGV Frequency Points for ADL-P DDR5 for 45W package
const MrcFrequency SagvFreqPorU45WDdr5[MAX_SAGV_POINTS] = {
  //  0,     1,     2,     3
  f2000, f3600, f4400, f4800
};

// POR SAGV Gears for ADL-P L,R step and beyond
const UINT8 SaGvGearPorU[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS] = {
  //   0,     1,     2,     3
  {    2,     2,     2,     1}, // DDR4
  {    2,     4,     4,     2}, // DDR5
  {    4,     4,     4,     2}, // LPDDR5
  {    4,     4,     4,     2}  // LPDDR4
};

// POR SAGV Gears for ADL-P DDR5 for 45W package
const UINT8 SaGvGearPorU45WDdr5[MAX_SAGV_POINTS] = {
  //  0,     1,     2,     3
      2,     2,     2,     2
};

// POR SAGV Frequency Points for ADL-P J Step Only
const MrcFrequency SagvFreqPorJ0[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS] = {
  //   0,     1,     2,     3
  {f2133, f2667, f2933, f3200}, // DDR4
  {f2000, f3600, f4400, f4800}, // DDR5
  {f2200, f3200, f3600, f4400}, // LPDDR5
  {f2133, f3200, f3733, f4267}  // LPDDR4
};
// POR SAGV Gears for ADL-P J step only
const UINT8 SaGvGearPorJ0[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS] = {
  //   0,     1,     2,     3
  {    2,     2,     2,     2}, // DDR4
  {    2,     2,     2,     2}, // DDR5
  {    2,     2,     2,     2}, // LPDDR5
  {    2,     2,     2,     2}  // LPDDR4
};

// POR SAGV Frequency Points for ADL-P Q,K Step
const MrcFrequency SagvFreqPorQ0K0[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS] = {
  //   0,     1,     2,     3
  {f2133, f2933, f3200, f2667}, // DDR4
  {f2000, f3600, f4400, f4800}, // DDR5
  {f2400, f3600, f4800, f5200}, // LPDDR5
  {f2667, f3200, f3733, f4267}  // LPDDR4
};

// POR SAGV Frequency Points for ADL-P LP5 Type3 board (Q0/K0 steppings)
const MrcFrequency SagvFreqPorLp5T3Q0K0[2][MAX_SAGV_POINTS] = {
  {f2400, f3600, f4400, f4800}, // LPDDR5 1R
  {f2400, f3600, f4000, f4400}  // LPDDR5 2R
};

// POR SAGV Gears for ADL-P Q,K Step
const UINT8 SaGvGearPorQ0K0[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS] = {
  //   0,     1,     2,     3
  {    2,     2,     2,     1}, // DDR4
  {    2,     2,     2,     2}, // DDR5
  {    4,     2,     2,     2}, // LPDDR5
  {    4,     2,     2,     2}  // LPDDR4
};



const UINT8 WeakLockEnTcwlTclLimits[MAX_MRC_DDR_TYPE - 1][MAX_GEARS][2] = {
  // Gear1,      Gear2,      Gear4
  //{tCWL,tCL}
  {{  6,  9 }, {  9, 14 }, {  0,  0 }}, // DDR4
  {{  6,  9 }, {  9, 14 }, { 26, 30 }}, // DDR5
  {{ 11, 12 }, { 16, 22 }, { 27, 34 }}, // LPDDR5
  {{  6,  6 }, { 10, 12 }, { 21, 18 }}  // LPDDR4
};

#ifdef MRC_DEBUG_PRINT
extern const char CcdString[];
const char TrainEnString[]  = "TrainingEnables";
const char TrainEn2String[] = "TrainingEnables2";
const char ThermEnString[]  = "ThermalEnables";
const char PrintBorder[]    = "*************************************\n";
#endif // MRC_DEBUG_PRINT

// This table is used for LPDDR MR5 decoding
struct {
  UINT8   VendorId;
  UINT16  JedecId;
  char    *VendorName;
} DramVendorList [] = {
  { 1,    0xCE00, "Samsung" },
  { 3,    0xFE02, "Elpida"  },
  { 6,    0xAD00, "Hynix"   },
  { 0xFF, 0x2C00, "Micron"  },
};

// This is used for DDR5 Dimm mixed Populaion
#define MAX_DIMMS_IN_SOCKET                  4
#define DDR5_DIMM_MIXED_CONFIG_1R1R_8GB      1
#define DDR5_DIMM_MIXED_CONFIG_1R1R_16GB     2
#define DDR5_DIMM_MIXED_CONFIG_1R1R_8GB_16GB 3
#define DDR5_DIMM_MIXED_CONFIG_1R1R_OTHERS   4
#define DDR5_DIMM_MIXED_CONFIG_2R1R          5
#define DDR5_DIMM_MIXED_CONFIG_1R2R          6
#define DDR5_DIMM_MIXED_CONFIG_2R2R          7
#define DDR5_DIMM_MIXED_CONFIG_2R_OTHERS     8
#define DIMM_SIZE_8GB                        8192
#define DIMM_SIZE_16GB                       16384
/**
  Enable/Disable DLL WeakLock if needed.
  Note: We don't enable it in McConfig because CKE is still low during that step.

  @param[in] MrcData - The MRC general data.
  @param[in]  Enable - BOOLEAN control to enable (if TRUE), or disable (if FALSE) WeakLock.

  @retval None
**/
void
MrcWeaklockEnDis (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              Enable
  )
{
  const MrcInput    *Inputs;
  MrcOutput         *Outputs;
  MrcDebug          *Debug;
  MrcChannelOut     *ChannelOut;
  const MrcTiming   *Timing;
  MrcDdrType        DdrType;
  UINT32            FirstController;
  UINT32            FirstChannel;
  UINT16            tCL;
  UINT16            tCWL;
  UINT8             tCwlLimit;
  UINT8             tClLimit;
  UINT8             Gear;
  BOOLEAN           WeakLockEn;
  BOOLEAN           WeakLockCccDis;
  BOOLEAN           WeakLockDataDis;
  BOOLEAN           A0;
  BOOLEAN           B0;
  BOOLEAN           Q0Regs;
  BOOLEAN           UlxUlt;
  BOOLEAN           DtHalo;
  BOOLEAN           Ddr4;
  BOOLEAN           Lpddr5;
  BOOLEAN           Gear1;
  BOOLEAN           Gear2;
  BOOLEAN           Gear4;
  DATA0CH0_CR_WLCTRL_STRUCT  DataWlCtrl;
  CH0CCC_CR_WLCTRL_STRUCT    CccWlCtrl;

  Inputs          = &MrcData->Inputs;
  Outputs         = &MrcData->Outputs;
  Debug           = &Outputs->Debug;
  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;
  ChannelOut      = &Outputs->Controller[FirstController].Channel[FirstChannel];
  Timing          = &ChannelOut->Timing[Inputs->MemoryProfile];
  UlxUlt          = Inputs->UlxUlt;
  DtHalo          = Inputs->DtHalo;
  A0              = Inputs->A0;
  B0              = Inputs->B0;
  Q0Regs          = Inputs->Q0Regs;
  DdrType         = Outputs->DdrType;
  Ddr4            = (DdrType == MRC_DDR_TYPE_DDR4);
  Lpddr5          = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Gear2           = Outputs->Gear2;
  Gear4           = Outputs->Gear4;
  Gear1           = !Gear2 && !Gear4;
  Gear            = Gear1 ? 0 : (Gear2 ? 1 : 2);

  if (DdrType >= MRC_DDR_TYPE_UNKNOWN) {
    return;
  }

  tCWL = Timing->tCWL;
  tCL  = Timing->tCL;
  if (Lpddr5) { // Convert to WCK units
    tCWL *= 4;
    tCL  *= 4;
  }

  tCwlLimit   = WeakLockEnTcwlTclLimits[DdrType][Gear][0];
  tClLimit    = WeakLockEnTcwlTclLimits[DdrType][Gear][1];
  WeakLockEn  = ((tCWL > tCwlLimit) && (tCL > tClLimit));

  WeakLockDataDis = ((Outputs->Lpddr && DtHalo) || A0);
  WeakLockEn      = (!WeakLockDataDis && WeakLockEn && Enable);

  DataWlCtrl.Data = 0;
  if (Q0Regs) {
    DataWlCtrl.BitsQ0.weaklocken_dly    = 2;
    DataWlCtrl.BitsQ0.initdlllock_dly   = 4;
    DataWlCtrl.BitsQ0.wlrelock_dly      = 4;
  } else {
    DataWlCtrl.Bits.weaklocken_dly      = 12;
  }
  DataWlCtrl.Bits.weaklocken            = WeakLockEn;
  DataWlCtrl.Bits.weaklockcomp_en       = WeakLockEn;
  DataWlCtrl.Bits.wlcomp_init_stepsize  = 3;
  DataWlCtrl.Bits.wlcomp_per_stepsize   = 1;
  DataWlCtrl.Bits.wlcomp_samplewait     = 6;
  if (WeakLockEn) {
    DataWlCtrl.Bits.lp_weaklocken       = B0 ? 0 : 1;
  } else {
    DataWlCtrl.Bits.lp_weaklocken       = 0;
  }
  if(UlxUlt) {
    DataWlCtrl.Bits.wlcomp_clkgatedis   = 1;
  }
  MrcWriteCrMulticast (MrcData, DATA_CR_WLCTRL_REG, DataWlCtrl.Data);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DATA_WLCTRL=0x%08X\n", DataWlCtrl.Data);

  if (UlxUlt && !Inputs->J0 && !Inputs->Q0) {  // Currently only enabled on K0
    WeakLockCccDis = (Ddr4 && UlxUlt);
    WeakLockEn     = (!WeakLockCccDis && Enable);
    CccWlCtrl.Data = 0;
    if (Q0Regs) {
      CccWlCtrl.BitsQ0.weaklocken_dly     = 2;
      CccWlCtrl.BitsQ0.initdlllock_dly    = 4;
      CccWlCtrl.BitsQ0.wlrelock_dly       = 4;
    } else {
      CccWlCtrl.Bits.weaklocken_dly       = 4;
      CccWlCtrl.Bits.wlslowclken_dly      = 4;
    }
    CccWlCtrl.Bits.weaklockcomp_en      = WeakLockEn;
    CccWlCtrl.Bits.weaklocken           = WeakLockEn;
    CccWlCtrl.Bits.wlcomp_init_stepsize = 3;
    CccWlCtrl.Bits.wlcomp_per_stepsize  = 1;
    CccWlCtrl.Bits.wlcomp_samplewait    = 6;
    CccWlCtrl.Bits.lp_weaklocken        = WeakLockEn;
    CccWlCtrl.Bits.wlcomp_clkgatedis    = 1;

    MrcWriteCrMulticast (MrcData, CCC_CR_WLCTRL_REG, CccWlCtrl.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC_WLCTRL=0x%08X\n", CccWlCtrl.Data);
  }
}

/**
  This function returns the current configuration of Frequency and Gear ratio based on the
  current SAGV point, DDR type, and SAGV input parameters: SaGvFreq, SaGvGear.

  It will update FreqOut and GearOut with the result.

  @param[in]  MrcData   - Pointer to global MRC data.
  @param[in]  SaGvPoint - Current operating SAGV point.
  @param[out] FreqOut   - Pointer to return the SAGV point Frequency.
  @param[out] GearOut   - Pointer to return the SAGV point Gear.
**/
VOID
MrcGetSagvConfig (
  IN  MrcParameters *const  MrcData,
  IN  MrcSaGvPoint          SaGvPoint,
  OUT MrcFrequency          *FreqOut,
  OUT BOOLEAN               *GearOut
  )
{
  MrcInput    *Inputs;
  MrcOutput   *Outputs;
  MrcDdrType  DdrType;
  UINT8       Gear;
  BOOLEAN     UlxUlt;
  BOOLEAN     A0;
  BOOLEAN     Q0;
  BOOLEAN     J0;
  BOOLEAN     K0;
  BOOLEAN     L0;
#ifdef MRC_DEBUG_PRINT
  MrcDebug    *Debug;
#endif

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  DdrType = Outputs->DdrType;
  UlxUlt  = (Inputs->UlxUlt);
  A0      = Inputs->A0;
  Q0      = Inputs->Q0;
  J0      = Inputs->J0;
  K0      = Inputs->K0;
  L0      = Inputs->L0;
#ifdef MRC_DEBUG_PRINT
  Debug   = &Outputs->Debug;
#endif

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SAGV point %u\n", SaGvPoint);
  if (FreqOut == NULL) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %s is NULL\n", &gErrString, "FreqOut");
  } else {
    if (Inputs->SaGvFreq[SaGvPoint]) {
      *FreqOut = Inputs->SaGvFreq[SaGvPoint];
    } else {
      if (DdrType < MRC_DDR_TYPE_UNKNOWN) {
        if (UlxUlt) {
          *FreqOut = (Q0 || K0) ? SagvFreqPorQ0K0[DdrType][SaGvPoint] : (J0 ? SagvFreqPorJ0[DdrType][SaGvPoint] : SagvFreqPorU[DdrType][SaGvPoint]);
          if ((Inputs->BoardType == btUlxUltType3) && (DdrType == MRC_DDR_TYPE_LPDDR5)) {
            if (Q0 || K0) {
              *FreqOut = (Outputs->Any2Ranks) ? SagvFreqPorLp5T3Q0K0[1][SaGvPoint] : SagvFreqPorLp5T3Q0K0[0][SaGvPoint];
            } else {
              *FreqOut = SagvFreqPorLp5T3[SaGvPoint];
            }
          }
          if ((Inputs->CpuPackageTdp == CPU_TDP_45_WATTS) && (DdrType == MRC_DDR_TYPE_DDR5) && L0) {
            *FreqOut = SagvFreqPorU45WDdr5[SaGvPoint];
          }
        } else {
          *FreqOut = A0 ? SagvFreqPorA0[DdrType][SaGvPoint] : SagvFreqPor[DdrType][SaGvPoint];
        }
        MRC_DEBUG_ASSERT (*FreqOut != 0, Debug, "Invalid SAGV %s: Point %d %s: %d\n", "Freq", SaGvPoint, "Freq", *FreqOut);
      }
    }
  }

  if (GearOut == NULL) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %s is NULL\n", &gErrString, "GearOut");
  } else {
    if (Inputs->SaGvGear[SaGvPoint]) {
      *GearOut = (Inputs->SaGvGear[SaGvPoint] == 2);
       Outputs->Gear4 = (Inputs->SaGvGear[SaGvPoint] == 4);
    } else {
      if (DdrType < MRC_DDR_TYPE_UNKNOWN) {
        if (UlxUlt) {
          Gear = (Q0 || K0) ? SaGvGearPorQ0K0[DdrType][SaGvPoint] : (J0 ? SaGvGearPorJ0[DdrType][SaGvPoint] : SaGvGearPorU[DdrType][SaGvPoint]);
          if ((Inputs->CpuPackageTdp == CPU_TDP_45_WATTS) && (DdrType == MRC_DDR_TYPE_DDR5) && L0) {
            Gear = SaGvGearPorU45WDdr5 [SaGvPoint];
          }
        } else {
          Gear = A0 ? SaGvGearPorA0[DdrType][SaGvPoint] : SaGvGearPor[DdrType][SaGvPoint];
        }
        *GearOut = (Gear == 2);
        Outputs->Gear4 = (Gear == 4);
        MRC_DEBUG_ASSERT (Gear != 0, Debug, "Invalid SAGV %s: Point %d %s: %d\n", "Gear", SaGvPoint, "Gear", Gear);
      }
    }
  }
}

/**
  Read LPDDR information from MR5 and MR8 and print to the debug log.
  Also update the Manufacturer's ID in the SPD table, for BIOS Setup and SMBIOS table.

  @param[in] MrcData - include all the MRC general data.

  @retval none
**/
void
ShowLpddrInfo (
  IN  MrcParameters *const MrcData
  )
{
  MrcInput        *Inputs;
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  MrcIntOutput    *MrcIntData;
  MrcSpd          *SpdIn;
  UINT32          Controller;
  UINT32          Channel;
  UINT32          Rank;
  UINT8           MrrResult[4];
  UINT32          MrAddr;
  UINT32          Device;
  UINT32          MaxDevices;
  UINT32          Index;
  BOOLEAN         VendorFound;
  UINT16          JedecId;
#ifdef MRC_DEBUG_PRINT
  UINT8           LpddrDeviceWidth;
#endif

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  MrcIntData = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));
  Debug   = &Outputs->Debug;
  VendorFound = FALSE;
  Index = 0;

  if (Outputs->Lpddr) {
    MaxDevices = (Outputs->LpByteMode) ? 2 : 1;
  } else {
    MaxDevices = 4;
  }
  if (Inputs->BootMode != bmCold) {
    // Full deswizzle table is not present on non-cold flows, so cannot parse MR read.
    return;
  }
  // LPDDR: Read MR5 and MR8
  if (Outputs->Lpddr) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }

          // MR5 - Manufacturer ID
          MrAddr = 5;
          MrcIssueMrr (MrcData, Controller, Channel, Rank, MrAddr, MrrResult);
          for (Device = 0; Device < MaxDevices; Device++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tDevice[%u]= 0x%02X", Device, MrrResult[Device]);
            VendorFound = FALSE;
            for (Index = 0; Index < sizeof (DramVendorList) / sizeof (DramVendorList[0]); Index++) {
              if (DramVendorList[Index].VendorId == MrrResult[Device]) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %s\n", DramVendorList[Index].VendorName);
                VendorFound = TRUE;
                break;
              }
            }
            if (!VendorFound) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " Unknown\n");
            }
          }

          if (VendorFound) {
            // Patch SPD data with vendor ID code.
            // This is consumed by BIOS Setup and SMBIOS Type 17 table creation code.
            // If SAGV enabled, only do this on the last pass: MrcSaGvPoint4.
            if ((Inputs->SaGv != MrcSaGvEnabled) || (MrcIntData->SaGvPoint == MrcSaGvPoint4)) {
              JedecId = DramVendorList[Index].JedecId;
              SpdIn = &Inputs->Controller[Controller].Channel[Channel].Dimm[dDIMM0].Spd.Data;
              SpdIn->Lpddr.ManufactureInfo.ModuleId.IdCode.Data = JedecId;
              SpdIn->Lpddr.ManufactureInfo.DramIdCode.Data      = JedecId;
            }
          }

#ifdef MRC_DEBUG_PRINT
          // MR8 - I/O Width, Density, Type
          // I/O Width OP[7:6]
          // 00B: x16
          // 01B: x8
          // All Others: Reserved
          MrAddr = 8;
          MrcIssueMrr (MrcData, Controller, Channel, Rank, MrAddr, MrrResult);
          for (Device = 0; Device < MaxDevices; Device++) {
            LpddrDeviceWidth = ((MRC_BIT7 | MRC_BIT6) & MrrResult[Device]) >> 6;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tDevice[%u]= 0x%02X - %s\n", Device, MrrResult[Device],
              (LpddrDeviceWidth == 0x1) ? "x8" : ((LpddrDeviceWidth ==  0x0) ? "x16" : "Unknown"));
          }
#endif // #ifdef MRC_DEBUG_PRINT
        } // for Rank
      } // for Channel
    } // for Controller
  }
}

/**
  This function Dimm Population Info for DDR5.

  @param[in, out] MrcData - Include all MRC global data.

  @retval Returns DDR5 DIMM MIXED CONFIG.
**/
UINT8
GetDDR5DimmPopInfo(
  IN  MrcParameters *const MrcData
  )
{
  struct {
    UINT8   RankInDimm;   // The number of ranks in this DIMM.
    UINT32  DimmCapacity; // DIMM size in MBytes.
  } DimmPopulationInfo[MAX_DIMMS_IN_SOCKET] = {
      { 0, 0 },           // MC0  Dimm0
      { 0, 0 },           // MC0  Dimm1
      { 0, 0 },           // MC1  Dimm0
      { 0, 0 },           // MC1  Dimm1
    };
  MrcDebug                      *Debug;
  MrcOutput                     *Outputs;
  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  for (int i = 0; i < MAX_DIMMS_IN_SOCKET; i++ ) {
    DimmPopulationInfo[i].DimmCapacity = Outputs->Controller[i / 2].Channel[0].Dimm[i % 2].DimmCapacity + Outputs->Controller[i / 2].Channel[1].Dimm[i % 2].DimmCapacity;
    DimmPopulationInfo[i].RankInDimm = Outputs->Controller[i / 2].Channel[0].Dimm[i % 2].RankInDimm;
    MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info: Dimm%d DimmCapacity=%u RankInDimm=%u\n", i, DimmPopulationInfo[i].DimmCapacity, DimmPopulationInfo[i].RankInDimm);
  }

  if (!Outputs->Any2Ranks) {
    //1R1R Dimm Mixed
    if (((DimmPopulationInfo[0].DimmCapacity == DIMM_SIZE_8GB) && (DimmPopulationInfo[1].DimmCapacity == DIMM_SIZE_8GB)) || ((DimmPopulationInfo[2].DimmCapacity == DIMM_SIZE_8GB) && (DimmPopulationInfo[3].DimmCapacity == DIMM_SIZE_8GB))) {
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: DDR5_DIMM_MIXED_CONFIG_1R1R_8GB\n");
      return DDR5_DIMM_MIXED_CONFIG_1R1R_8GB;
    } else if (((DimmPopulationInfo[0].DimmCapacity == DIMM_SIZE_16GB) && (DimmPopulationInfo[1].DimmCapacity == DIMM_SIZE_16GB)) || ((DimmPopulationInfo[2].DimmCapacity == DIMM_SIZE_16GB) && (DimmPopulationInfo[3].DimmCapacity == DIMM_SIZE_16GB))) {
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: DDR5_DIMM_MIXED_CONFIG_1R1R_16G\n");
      return DDR5_DIMM_MIXED_CONFIG_1R1R_16GB;
    } else if (((DimmPopulationInfo[0].DimmCapacity + DimmPopulationInfo[1].DimmCapacity) == (DIMM_SIZE_8GB + DIMM_SIZE_16GB)) || ((DimmPopulationInfo[2].DimmCapacity + DimmPopulationInfo[3].DimmCapacity) == (DIMM_SIZE_8GB + DIMM_SIZE_16GB))) {
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: DDR5_DIMM_MIXED_CONFIG_1R1R_8GB_16GB\n");
      return DDR5_DIMM_MIXED_CONFIG_1R1R_8GB_16GB;
    } else {
      //Other Config
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: DDR5_DIMM_MIXED_CONFIG_1R1R_OTHERS!!! Limit Outputs->FreqMax = f2000\n");
      return DDR5_DIMM_MIXED_CONFIG_1R1R_OTHERS;
    }
  } else {
    //2R2R Dimm Mixed, 1R2R Dimm Mixed or 2R1R Dimm Mixed
      if ( (DimmPopulationInfo[0].RankInDimm == 2 && DimmPopulationInfo[1].RankInDimm == 1) || (DimmPopulationInfo[2].RankInDimm == 2 && DimmPopulationInfo[3].RankInDimm == 1) ) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5:DDR5_DIMM_MIXED_CONFIG_2R1R\n");
        return DDR5_DIMM_MIXED_CONFIG_2R1R;
      } else if ( (DimmPopulationInfo[0].RankInDimm == 1 && DimmPopulationInfo[1].RankInDimm == 2) || (DimmPopulationInfo[2].RankInDimm == 1 && DimmPopulationInfo[3].RankInDimm == 2) ){
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5:DDR5_DIMM_MIXED_CONFIG_1R2R\n");
        return DDR5_DIMM_MIXED_CONFIG_1R2R;
      } else if ( (DimmPopulationInfo[0].RankInDimm == 2 && DimmPopulationInfo[1].RankInDimm == 2) || (DimmPopulationInfo[2].RankInDimm == 2 && DimmPopulationInfo[3].RankInDimm == 2) ) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5:DDR5_DIMM_MIXED_CONFIG_2R2R\n");
        return DDR5_DIMM_MIXED_CONFIG_2R2R;
    } else {
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: DDR5_DIMM_MIXED_CONFIG_2R_OTHERS!!! Limit Outputs->FreqMax = f2000 !!!\n");
      return DDR5_DIMM_MIXED_CONFIG_2R_OTHERS;
    }
  }
}

/**
  This function performs final MC/DDRIO configuration after all training steps are done, but before Normal mode.
  - Enable Scrambler
  - Enable ddr4_1dpc feature
  - Enable ECC
  - Enable Weaklock
  - Enable CMD tri-state
  - Enable LPDDR4/X DqsN Park mode

  @param[in, out] MrcData - Include all MRC global data.

  @retval Returns mrcSuccess or failure if DRAM width doesn't match.
**/
MrcStatus
MrcMcActivate (
  IN     MrcParameters *const MrcData
  )
{
  const MRC_FUNCTION  *MrcCall;
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcIntOutput        *MrcIntData;
  // MrcChannelOut       *ChannelOut;
  MrcDdrType          DdrType;
  MrcStatus           Status;
  INT64               GetSetVal;
  INT64               GetSetDis;
  UINT32              Offset;
  UINT32              Data32;
  UINT32              GeneratedSeed;
  UINT32              i;
  UINT8               Controller;
  UINT8               Channel;
  UINT32              IpChannel;
#if 0
  UINT8               Byte;
  UINT8               Rank;
  UINT8               MaxRcvEn;
  UINT8               RcvEnDrift;
  UINT8               RcvEnTurnOff;
  UINT8               RcvEn;
  UINT16              MaxRdDataValid;
  UINT16              RdDataValid;
  INT8                OdtTurnOff;
  INT64               OdtDelay;
  INT64               OdtDuration;
  INT64               RxClkStgNum;
  INT64               RxFifoRdEnRank;
  INT64               LpddrLongOdtEn;
  INT64               OdtSampExtEn;
  INT32               Temp;
  UINT32              SubCh;
  BOOLEAN             Ratio900To1000;
  BOOLEAN             Ratio800To900;
  BOOLEAN             Ratio700To800;
  UINT32              Ratio;
  DDRSCRAM_CR_DDRMISCCONTROL2_STRUCT  MiscControl2;
  DLLDDRDATA0_CR_DDRCRVCCDLLFFCONTROL_STRUCT      VccDllFFControl;
#endif
  BOOLEAN             Lpddr;
  BOOLEAN             Lpddr5;
  BOOLEAN             Ddr5;
  BOOLEAN             UlxUlt;
  DDRSCRAM_CR_DDRSCRAMBLECH0_STRUCT       DdrScramble;
  MC0_CH0_CR_MC_INIT_STATE_STRUCT         McInitState;
  MC0_CH0_CR_WCK_CONFIG_STRUCT            WckConfig;
  CH0CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT    CccClkControls;
  MCMISCS_SPINEGATING_STRUCT              McMiscSpineGating;
  DDRSCRAM_CR_DDRLASTCR_STRUCT            LastCr;
  DDRSCRAM_CR_DDRMISCCONTROL0_STRUCT      MiscControl0;
  //DDRSCRAM_CR_DDRMISCCONTROL7_STRUCT MiscControl7;
  //UINT8               DataInvertNibble;
  //BOOLEAN             Vtt;
  //BOOLEAN             Vddq;

  MrcIntData    = ((MrcIntOutput *)(MrcData->IntOutputs.Internal));
  Inputs        = &MrcData->Inputs;
  MrcCall       = Inputs->Call.Func;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  DdrType       = Outputs->DdrType;
  GeneratedSeed = 0;
  GetSetDis     = 0;
  Lpddr         = Outputs->Lpddr;
  Lpddr5        = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr5          = (DdrType == MRC_DDR_TYPE_DDR5);
  UlxUlt        = Inputs->UlxUlt;
  //Vtt           = (Outputs->OdtMode == MrcOdtModeVtt);
  //Vddq          = (Outputs->OdtMode == MrcOdtModeVddq);

  MrcBeforeNormalModeTestMenu (MrcData);
  // Oem hook before normal mode configuration starts
  MrcInternalCheckPoint (MrcData, OemBeforeNormalMode, NULL);

  MrcPrintDimmOdtValues (MrcData);  // Print DIMM ODT table

  if (!Lpddr) {
    MrcSetMrShadows (MrcData);
  }

  if (Ddr5) {
    // Use multicycle MPC command only during boot.
    // After boot, configure DRAM and MC for 1tCK MPC
    DisableMcMulticycleCs (MrcData, FALSE);

    // Capture the final state of the MR's for the Generic MRS FSM - for Fast flow and for SAGV
    MrcFinalizeMrSeq (MrcData, MRC_PRINTS_ON);
  }

  // Read LPDDR MR5 and MR8 info
  ShowLpddrInfo (MrcData);

  // Program DllWeaklock bit after training, when CKE is high
  if (Inputs->WeaklockEn) {
    MrcWeaklockEnDis (MrcData, MRC_ENABLE);
  }

  if (!Inputs->SafeMode) {
    GetSetVal = 0;
    MrcGetSetNoScope (MrcData, GsmIocDisDataIdlClkGate, WriteCached, &GetSetVal);
  }

  if (UlxUlt) {
    GetSetVal = 0;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocInternalClocksOn, WriteCached | PrintValue, &GetSetVal);
  }
  // Configure DDR4_1DPC performance feature
  MrcConfigureDdr4OneDpc (MrcData);

  // Configure Power Down CR
  MrcPowerDownConfig (MrcData);

  // Enable Scrambling
  if (Inputs->ScramblerSupport && !(Inputs->J0 && Outputs->Gear4 && Lpddr5)) {
    for (i = 0; i < MRC_NUM_SCRAM; i++) {
      Offset = OFFSET_CALC_CH (DDRSCRAM_CR_DDRSCRAMBLECH0_REG, DDRSCRAM_CR_DDRSCRAMBLECH1_REG, i);
      DdrScramble.Data = MrcReadCR (MrcData, Offset);
      DdrScramble.Bits.scramen  = 1;
      DdrScramble.Bits.clockgateab = 3;
      DdrScramble.Bits.clockgatec  = 3;
      DdrScramble.Bits.enabledbiab = 1;
      if (MrcIntData->ScramKey == 0) {
        // Only generate ScramKey once for all SAGV points
        MrcCall->MrcGetRandomNumber (&GeneratedSeed);
        if (GeneratedSeed == 0) {
          GeneratedSeed = 0x1234;
        }
        MrcIntData->ScramKey = GeneratedSeed;
      }
      DdrScramble.Bits.scramkey = MrcIntData->ScramKey;
      MrcWriteCR (MrcData, Offset, DdrScramble.Data);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDRSCRAMBLECH%u: 0x%08X\n", i, DdrScramble.Data);
    }
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (!(MrcControllerExist (MrcData, Controller))) {
      continue;
    }
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!(MrcChannelExist (MrcData, Controller, Channel))) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      // Enable the command tri state at the end of the training.
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdTriStateDis, WriteCached | PrintValue, &GetSetDis);
      // Set the MC to ECC mode for all channels if needed.
      if (Outputs->EccSupport == TRUE) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ECC support\n");
        GetSetVal = emBothActive;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccEccMode, WriteNoCache | PrintValue, &GetSetVal);
      }
      // Tell MC that we are in FSP-OP = 1
      if (Lpddr) {
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MC_INIT_STATE_REG, MC1_CH0_CR_MC_INIT_STATE_REG, Controller, MC0_CH1_CR_MC_INIT_STATE_REG, IpChannel);
        McInitState.Data = MrcReadCR (MrcData, Offset);
        McInitState.Bits.LPDDR4_current_FSP = 1;
        MrcWriteCR (MrcData, Offset, McInitState.Data);
      }

      // Program 1x Refresh per technology during cold path.
      if (DdrType == MRC_DDR_TYPE_DDR4) {
        Data32 = RANK_TEMPERATURE_1XREF_DDR4;
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_REG, MC1_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_REG, Controller, MC0_CH1_CR_DDR4_MPR_RANK_TEMPERATURE_REG, IpChannel);
      } else { // LP4/LP5/DDR5
        Data32 = (DdrType == MRC_DDR_TYPE_LPDDR4) ? RANK_TEMPERATURE_1XREF_LPDDR4 : (Ddr5 ? RANK_TEMPERATURE_1XREF_DDR5 : RANK_TEMPERATURE_1XREF_LPDDR5);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MR4_RANK_TEMPERATURE_REG, MC1_CH0_CR_MR4_RANK_TEMPERATURE_REG, Controller, MC0_CH1_CR_MR4_RANK_TEMPERATURE_REG, IpChannel);
      }
      MrcWriteCR (MrcData, Offset, Data32);
    } // for Channel
  } // for Controller

  if (Lpddr5 && (Outputs->Any2Ranks)) {
    // Perform Jedec Reset to enable WCK Mode changes
    Status = MrcResetSequence (MrcData);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ResetSequence Status 0x%x\n", Status);

    // Need one more FinalizeMrSeq to update SAGV info in MRS FSM after JEDEC Reset
    MrcFinalizeMrSeq (MrcData, MRC_PRINTS_ON);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcFinalizeMrSeq Status 0x%x\n", Status);

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!(MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        // ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH(MC0_CH0_CR_WCK_CONFIG_REG, MC1_CH0_CR_WCK_CONFIG_REG, Controller, MC0_CH1_CR_WCK_CONFIG_REG, IpChannel);
        WckConfig.Data = MrcReadCR64 (MrcData, Offset);
        WckConfig.Bits.LP5_WCK_MODE = 1; // 2R: Manual
        if (WckConfig.Bits.LP5_WCK_MODE == 2) { // WCK_DYNAMIC Mode
          WckConfig.Bits.tWCKPST = Outputs->HighGear * 2;
          WckConfig.Bits.tCASSTOP_ADDITIONAL_GAP = Outputs->HighGear * 2;
        }
        MrcWriteCR64 (MrcData, Offset, WckConfig.Data);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u: WckConfig = 0x%016llx\n", Controller, Channel, WckConfig.Data);
      }
    }
  }

  UpdateSampOdtTiming (MrcData, (Inputs->SafeMode) ? 10 : 3);

//@todo ICL
#if 0
  // Program RxClkStgNum
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      // Read from Byte 0 as these should be the same across all bytes.
      MaxRcvEn   = 0;
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MrcGetSetDdrIoGroupStrobe (MrcData, Channel, Rank, Byte, RecEnDelay, ReadFromCache, &GetSetVal);
          Temp = (INT32) GetSetVal;
          Temp = Temp / 64;
          RcvEn = (UINT8) Temp;
          MaxRcvEn = MAX (MaxRcvEn, RcvEn);
        }
      }
      RcvEnDrift   = (Lpddr) ? (UINT8) ((tDQSCK_DRIFT + Outputs->Qclkps - 1) / Outputs->Qclkps) : 1;
      RcvEnTurnOff = MaxRcvEn + (5 - 6) + 1 + 7 + 3 + 3 + 2 + (2 * RcvEnDrift);
      if (LpddrLongOdtEn == 1) {
        RcvEnTurnOff++;
      }

      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, Byte, McOdtDelay,     ReadFromCache, &OdtDelay);
        MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, Byte, McOdtDuration,  ReadFromCache, &OdtDuration);
        MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, Byte, GsmIocRxClkStg, ReadFromCache, &RxClkStgNum);

        OdtTurnOff = (INT8) (OdtDelay + OdtDuration + 14);
        OdtTurnOff = MIN (OdtTurnOff, DATA0CH0_CR_DDRCRDATACONTROL2_RxClkStgNum_MAX);

        RxClkStgNum = MAX (17, OdtTurnOff);
        MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, Byte, GsmIocRxClkStg, WriteCached, &RxClkStgNum);
      }
    }
  }
#endif

// @todo TGL
#if 0
  // Calculate the DDRIO RdDataValid per channel/rank and save the max value.
  // This is based on the longest RxFIFO timing in the system.
  MaxRdDataValid = MrcIntData->MaxRdDataValid;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      MrcGetSetMcCh (MrcData, Controller, Channel, RxFifoRdEnTclDelay,      ReadFromCache, &GetSetVal);
      RdDataValid = (UINT16) GetSetVal;
      MrcGetSetMcCh (MrcData, Controller, Channel, RxDqDataValidDclkDelay,  ReadFromCache, &GetSetVal);
      RdDataValid += (UINT16) GetSetVal;

      RxFifoRdEnRank = 0;
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, RxFifoRdEnFlybyDelay, ReadFromCache, &GetSetVal);
        RxFifoRdEnRank = MAX (GetSetVal, RxFifoRdEnRank);
      } // Rank
      RdDataValid += (UINT16) RxFifoRdEnRank;
      MrcIntData->MaxRdDataValid = MAX (MaxRdDataValid, RdDataValid);
    } // for Channel
  } // for Controller

  // If we're at the last point, or SAGV is not enabled, program the RX Grace counter
  if ((MrcIntData->SaGvPoint == MrcSaGvPointHigh) || (MrcData->Inputs.SaGv != MrcSaGvEnabled)) {
    MiscControl2.Data = MrcReadCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL2_REG);
    MiscControl2.Bits.rx_analogen_grace_cnt = MrcIntData->MaxRdDataValid;
    MrcWriteCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL2_REG, MiscControl2.Data);
  }
#endif
  //MrcDccFsmFinalize (MrcData);

  // Enable Power Features
  if (UlxUlt && (!(Inputs->J0) && !(Inputs->K0) && !(Inputs->Q0))) {
    McMiscSpineGating.Data = MrcReadCR (MrcData, MCMISCS_SPINEGATING_REG);
    LastCr.Data = MrcReadCR (MrcData, DDRSCRAM_CR_DDRLASTCR_REG);
    MiscControl0.Data = MrcReadCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL0_REG);

    // Enable Static Spine gating
    LastCr.Bits.StaticSouthSpineGateEn = 0;
    if(MiscControl0.Bits.ddrnochinterleave == 1) {
      if (!(MrcControllerExist (MrcData, cCONTROLLER0))) {
        LastCr.Bits.StaticSouthSpineGateEn = 1;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Static Spine Gating Enabled\n");
      }
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Interleaved Channel Topolgy, Static Spine Gating Disabled\n");
    }
    MrcWriteCR(MrcData, DDRSCRAM_CR_DDRLASTCR_REG, LastCr.Data);

    // Disable DOP gating for all technologies.
    McMiscSpineGating.Bits.dopgatingdis = 1;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "dopgatingdis: %u\n", McMiscSpineGating.Bits.dopgatingdis);
    MrcWriteCR (MrcData, MCMISCS_SPINEGATING_REG, McMiscSpineGating.Data);
  }

  // Enabling Dynamic Clock Pi Gating for all technologies
  if (UlxUlt && (!(Inputs->J0) && !(Inputs->K0) && !(Inputs->Q0))) {
    CccClkControls.Data = MrcReadCR (MrcData, CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG);
    if (!Lpddr5) {
      // No CA/CKE PI gating for LP5
      CccClkControls.Bits.CaValidPiGateDisable = 0;
      CccClkControls.Bits.CkeIdlePiGateDisable = 0;
    }
    CccClkControls.Bits.ClkGateDisable = 0;
    MrcWriteCrMulticast (MrcData, CCC_CR_DDRCRCCCCLKCONTROLS_REG, CccClkControls.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Enabling Dynamic PiGating in CCC\n");
  }
  MrcFlushRegisterCachedData (MrcData);

  // Oem hook when normal mode configuration is done
  MrcInternalCheckPoint (MrcData, OemAfterNormalMode, NULL);

  // Enable Self Refresh
  GetSetVal = 1;
  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableSr, WriteNoCache, &GetSetVal);

  return mrcSuccess;
}

/**
  Program MC/CPGC engines to either Normal mode of operation
  or CPGC training mode.

  @param[in] MrcData    - The MRC general data.
  @param[in] NormalMode - TRUE for Normal mode, FALSE for CPGC mode

  @retval Always returns mrcSuccess.
**/
void
MrcSetNormalMode (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              NormalMode
  )
{
  MRC_FUNCTION  *MrcCall;
  MrcOutput   *Outputs;
  INT64       GetSetVal;
  INT64       CmdStretch;
  UINT32      Channel;
  UINT32      IpChannel;
  UINT32      Controller;
  UINT32      Offset;
  UINT32      SchedThirdCbitOrig[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN     Lpddr;
  MC0_NORMALMODE_CFG_STRUCT             NormalModeCfg;
  MC0_MC_CPGC_CMI_STRUCT                McCpgcCmi;
  MC0_CH0_CR_MERGE_REQ_READS_PQ_STRUCT  MergeReqReadsPq;
  MC0_CH0_CR_SCHED_THIRD_CBIT_STRUCT    SchedThirdCbit;
  BOOLEAN                               A0;
  MC0_PM_CONTROL_STRUCT                 PmControl;
  MC0_MC_INIT_STATE_G_STRUCT            McInitStateG;
  MC0_CH0_CR_MC_INIT_STATE_STRUCT       McInitState;
  MC0_MCDECS_SECOND_CBIT_STRUCT         McdecsSecondCbit;

  A0 = MrcData->Inputs.A0;
  MrcCall = MrcData->Inputs.Call.Func;
  Outputs = &MrcData->Outputs;
  Lpddr   = Outputs->Lpddr;

  MrcCall->MrcSetMem ((UINT8 *) SchedThirdCbitOrig, sizeof (SchedThirdCbitOrig), 0);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel) && (!IS_MC_SUB_CH (Lpddr, Channel))) {
        IpChannel = LP_IP_CH (Lpddr, Channel);
        // Set dis_scheds_clk_gate during transition to CPGC mode
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SCHED_THIRD_CBIT_REG, MC1_CH0_CR_SCHED_THIRD_CBIT_REG, Controller, MC0_CH1_CR_SCHED_THIRD_CBIT_REG, IpChannel);
        SchedThirdCbit.Data = MrcReadCR (MrcData, Offset);
        SchedThirdCbitOrig[Controller][Channel] = SchedThirdCbit.Data;
        SchedThirdCbit.Bits.dis_scheds_clk_gate = 1;
        MrcWriteCR (MrcData, Offset, SchedThirdCbit.Data);
      }
    }
  }

  if (NormalMode) {
    MrcSetCpgcInitMode (MrcData, !NormalMode);   // Disable CPGC engines before clearing CPGC_CMI.CPGC_ACTIVE
  }

  GetSetVal = 1;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockXarb, WriteNoCache, &GetSetVal);  // Block XARB when changing normalmode and cadb_enable
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockCke,  WriteNoCache, &GetSetVal);

  NormalModeCfg.Data = 0;
  NormalModeCfg.Bits.normalmode = NormalMode;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      Offset = OFFSET_CALC_CH (MC0_MC_CPGC_CMI_REG, MC1_MC_CPGC_CMI_REG, Controller);
      McCpgcCmi.Data = MrcReadCR (MrcData, Offset);
      McCpgcCmi.Bits.CPGC_ACTIVE = NormalMode ? 0 : 1;
      MrcWriteCR (MrcData, Offset, McCpgcCmi.Data);
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel) && (!IS_MC_SUB_CH (Lpddr, Channel))) {
          // Set bus_retain_on_n_to_1_bubble when N:1 is used and we are in Training mode; otherwise clear it.
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdStretch, ReadFromCache, &CmdStretch);
          GetSetVal = (!NormalMode && (CmdStretch == 3)) ? 1 : 0;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccBusRetainOnBubble, WriteNoCache, &GetSetVal);
          IpChannel = LP_IP_CH (Lpddr, Channel);
          Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MERGE_REQ_READS_PQ_REG, MC1_CH0_CR_MERGE_REQ_READS_PQ_REG, Controller, MC0_CH1_CR_MERGE_REQ_READS_PQ_REG, IpChannel);
          MergeReqReadsPq.Data = 0; // enable_rpq_req_merge needs to be disabled when cpgc_in_order is used
          if (NormalMode) {
            if (MrcData->Inputs.A0) {
              MergeReqReadsPq.BitsA0.disp_srcid                 = MC0_CH0_CR_MERGE_REQ_READS_PQ_disp_srcid_A0_DEF;
            }
            MergeReqReadsPq.Bits.enable_ipq_req_merge           = MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_ipq_req_merge_DEF;
            MergeReqReadsPq.Bits.enable_rpq_req_merge           = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5) ? 0 : MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_rpq_req_merge_DEF; // Disabling enable_rpq_req_merge bit for Lpddr5
            MergeReqReadsPq.Bits.enable_merge_req_vc1_low_prio  = (MrcData->Inputs.Ibecc) ? 0 : MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_merge_req_vc1_low_prio_DEF;
          }
          MrcWriteCR (MrcData, Offset, MergeReqReadsPq.Data);
        }
      }
      Offset = OFFSET_CALC_CH (MC0_NORMALMODE_CFG_REG, MC1_NORMALMODE_CFG_REG, Controller);
      MrcWriteCR (MrcData, Offset, NormalModeCfg.Data);
    }
  }

  if (NormalMode) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      if (!MrcControllerExist (MrcData, Controller)) {
        Offset = OFFSET_CALC_CH (MC0_PM_CONTROL_REG, MC1_PM_CONTROL_REG, Controller);
        PmControl.Data = MrcReadCR (MrcData, Offset);
        PmControl.Bits.Override_DDRPLDrainDone_Cbit = 1;
        MrcWriteCR (MrcData, Offset, PmControl.Data);

        if (A0) {
          Offset = OFFSET_CALC_CH (MC0_MC_INIT_STATE_G_REG, MC1_MC_INIT_STATE_G_REG, Controller);
          McInitStateG.Data = MrcReadCR (MrcData, Offset);
          McInitStateG.Bits.dclk_enable = 1;
          McInitStateG.Bits.refresh_enable = 1;
          MrcWriteCR (MrcData, Offset, McInitStateG.Data);

          // Set the Rank_occupancy to one for ch0
          Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MC_INIT_STATE_REG, MC1_CH0_CR_MC_INIT_STATE_REG, Controller, MC0_CH1_CR_MC_INIT_STATE_REG, 0);
          McInitState.Data = MrcReadCR (MrcData, Offset);
          McInitState.Bits.Rank_occupancy = 1;
          MrcWriteCR (MrcData, Offset, McInitState.Data);
        }

        Offset = OFFSET_CALC_CH (MC0_MCDECS_SECOND_CBIT_REG, MC1_MCDECS_SECOND_CBIT_REG, Controller);
        McdecsSecondCbit.Data = MrcReadCR (MrcData, Offset);
        McdecsSecondCbit.Bits.Mock_InSR = 1;
        if (!A0) {
          McdecsSecondCbit.Bits.Mock_InSR_for_MC = 1;
        }
        MrcWriteCR (MrcData, Offset, McdecsSecondCbit.Data);

        Offset = OFFSET_CALC_CH (MC0_NORMALMODE_CFG_REG, MC1_NORMALMODE_CFG_REG, Controller);
        MrcWriteCR (MrcData, Offset, NormalModeCfg.Data);
      }
    }
  }
  GetSetVal = NormalMode ? 0 : 1;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccCpgcInOrder,     WriteNoCache, &GetSetVal);
  MrcGetSetMc   (MrcData, MAX_CONTROLLER,              GsmMccInOrderIngress,  WriteNoCache, &GetSetVal);

  GetSetVal = 0;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockXarb, WriteNoCache, &GetSetVal);  // Unblock XARB
  // This CR bit is used to block CKE pin from toggling (no power up / power down)
  // Notes: we must always use this CR and block the scheduler when enabling/disabling "NormalMode"
  // or when enabling Probeless to make sure there are no commands in flight.
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockCke,  WriteNoCache, &GetSetVal);  // Unblock CKE

  if (!NormalMode) {
    MrcSetCpgcInitMode (MrcData, !NormalMode);   // Enable CPGC engines after setting CPGC_CMI.CPGC_ACTIVE
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel) && (!IS_MC_SUB_CH (Lpddr, Channel))) {
        IpChannel = LP_IP_CH (Lpddr, Channel);
        // Restore dis_scheds_clk_gate
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SCHED_THIRD_CBIT_REG, MC1_CH0_CR_SCHED_THIRD_CBIT_REG, Controller, MC0_CH1_CR_SCHED_THIRD_CBIT_REG, IpChannel);
        MrcWriteCR (MrcData, Offset, SchedThirdCbitOrig[Controller][Channel]);
      }
    }
  }
}

/**
  This function enables Normal Mode and configures the Power Down Modes.
  We also have special flow here for SAGV in S3/Warm boot modes.

  @param[in] MrcData - The MRC general data.

  @retval Always returns mrcSuccess.
**/
MrcStatus
MrcNormalMode (
  IN MrcParameters *const MrcData
  )
{
  const MRC_FUNCTION  *MrcCall;
  const MrcInput      *Inputs;
  MRC_BOOT_MODE       BootMode;
  INT64               GetSetVal;
  BOOLEAN             SaGvAndSelfRefresh;
  UINT32              Controller;
  BOOLEAN             MemInSr;
  BOOLEAN             Lpddr;
  UINT8               Channel;
  UINT32              IpChannel;
  UINT32              Offset;
  UINT32              MrParams[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32              SecondCbitKeep[MAX_CONTROLLER][MAX_CHANNEL];
  MC0_CH0_CR_SCHED_SECOND_CBIT_STRUCT       SchedSecondCbit;
  MC0_CH0_CR_DDR_MR_PARAMS_STRUCT           DdrMrParams;
  UINT64                  Timeout;
  BOOLEAN                 Flag;
  MC0_STALL_DRAIN_STRUCT  StallDrain;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  BootMode  = Inputs->BootMode;
  MemInSr   = (BootMode == bmWarm) || (BootMode == bmS3);
  Lpddr   = MrcData->Outputs.Lpddr;

  // Check if SAGV is enabled and memory is in Self-Refresh right now (Warm reset or S3 resume)
  SaGvAndSelfRefresh = (Inputs->SaGv == MrcSaGvEnabled) && (Inputs->BootMode != bmCold) && (Inputs->BootMode != bmFast);

  MrcCall->MrcSetMem ((UINT8 *) SecondCbitKeep, sizeof (SecondCbitKeep), 0);
  MrcCall->MrcSetMem ((UINT8 *) MrParams, sizeof (MrParams), 0);

  if (MemInSr) {
    // Disable MR4 reads before going to Normal Mode (which will exit SelfRefresh):
    // 1. SCHED_SECOND_CBIT.dis_srx_mr4 = 1
    // 2. SCHED_SECOND_CBIT.dis_SRX_MRS_MR4 = 1
    // 3. DDR_MR_PARAMS.MR4_PERIOD = 0
    // 4. DDR_MR_PARAMS.DDR4_TS_readout_en = 0
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SCHED_SECOND_CBIT_REG, MC1_CH0_CR_SCHED_SECOND_CBIT_REG, Controller, MC0_CH1_CR_SCHED_SECOND_CBIT_REG, IpChannel);
        SchedSecondCbit.Data = MrcReadCR (MrcData, Offset);
        SecondCbitKeep[Controller][IpChannel] = SchedSecondCbit.Data;     // Backup the current value
        SchedSecondCbit.Bits.dis_srx_mr4 = 1;
        SchedSecondCbit.Bits.dis_SRX_MRS_MR4 = 1;
        MrcWriteCR (MrcData, Offset, SchedSecondCbit.Data);

        Offset = OFFSET_CALC_MC_CH(MC0_CH0_CR_DDR_MR_PARAMS_REG, MC1_CH0_CR_DDR_MR_PARAMS_REG, Controller, MC0_CH1_CR_DDR_MR_PARAMS_REG, IpChannel);
        DdrMrParams.Data = MrcReadCR (MrcData, Offset);
        MrParams[Controller][IpChannel] = DdrMrParams.Data;               // Backup the current value
        DdrMrParams.Bits.MR4_PERIOD = 0;
        DdrMrParams.Bits.DDR4_TS_readout_en = 0;
        MrcWriteCR (MrcData, Offset, DdrMrParams.Data);
      } // Channel
    } // Controller
  } // MemInSr
  MrcSetNormalMode (MrcData, TRUE);

  Flag = FALSE;
  // Poll until STALL_DRAIN_STRUCT.sr_state becomes zero (DDR is not in self-refresh)
  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT; // 10 seconds timeout
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    Flag = FALSE;
    if (MrcControllerExist (MrcData, Controller)) {
      Offset = OFFSET_CALC_CH (MC0_STALL_DRAIN_REG, MC1_STALL_DRAIN_REG, Controller);
      do {
        StallDrain.Data = MrcReadCR (MrcData, Offset);
        Flag            = (StallDrain.Bits.sr_state == 1);
        if (Inputs->SimicsFlag == 1) {
          Flag = FALSE;
        }
      } while (Flag && (MrcCall->MrcGetCpuTime () < Timeout));
    }
  }

  if (Flag) {
    return mrcFail;
  }

  // Issue ZQ on both channels
  MrcIssueZQ (MrcData, MRC_ZQ_INIT);

  if (SaGvAndSelfRefresh) {
    // Program DRAM MRs to match the High point
    MrcProgramMrsFsm (MrcData);
  }

  // Restore MR4 settings
  if (MemInSr) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SCHED_SECOND_CBIT_REG, MC1_CH0_CR_SCHED_SECOND_CBIT_REG, Controller, MC0_CH1_CR_SCHED_SECOND_CBIT_REG, IpChannel);
        MrcWriteCR (MrcData, Offset, SecondCbitKeep[Controller][IpChannel]);

        Offset = OFFSET_CALC_MC_CH(MC0_CH0_CR_DDR_MR_PARAMS_REG, MC1_CH0_CR_DDR_MR_PARAMS_REG, Controller, MC0_CH1_CR_DDR_MR_PARAMS_REG, IpChannel);
        MrcWriteCR (MrcData, Offset, MrParams[Controller][IpChannel]);
      } // Channel
    } // Controller
  } // MemInSr

  // Ensure that pure_srx must be cleared so for FSM's to work.
  GetSetVal = 0;
  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccPureSrx, WriteNoCache, &GetSetVal);

  return mrcSuccess;
}

/**
  Clear Delta DQS before switching SA GV point

  @param[in] MrcData - include all the MRC general data.
void
MrcClearDeltaDqs (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput                         *Inputs;
  const MRC_FUNCTION                     *MrcCall;
  MrcOutput                              *Outputs;
  MrcDebug                               *Debug;
  UINT32                                 Offset;
  UINT32                                 Timeout;
  UINT8                                  Rank;
  BOOLEAN                                Busy;
  MCMISCS_DELTADQSCOMMON0_STRUCT         DeltaDqsCommon0;
  DDRDATA0CH0_CR_DELTADQSRANK0_STRUCT    DeltaDqsRank;
  DDRSCRAM_CR_DDRMISCCONTROL1_STRUCT     DdrMiscControl1;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;

  Offset = MCMISCS_DELTADQSCOMMON0_REG;
  DeltaDqsCommon0.Data = MrcReadCR (MrcData, Offset);
  if (DeltaDqsCommon0.Bits.Lp4DeltaDQSTrainMode == 1) {
    Timeout = (UINT32) MrcCall->MrcGetCpuTime () + 10000; // 10 seconds timeout
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Clear Lp4DeltaDQSTrainMode\n");
    DeltaDqsCommon0.Bits.Lp4DeltaDQSTrainMode = 0;
    MrcWriteCR (MrcData, Offset, DeltaDqsCommon0.Data);
#ifdef CTE_FLAG
    MrcWait (MrcData, 25 * MRC_TIMER_1NS);
#else
    MrcWait (MrcData, 5 * MRC_TIMER_1US);
#endif
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Clear DeltaDQS registers\n");
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      Offset = DDRDATA_CR_DELTADQSRANK0_REG +
               ((DDRDATA_CR_DELTADQSRANK1_REG - DDRDATA_CR_DELTADQSRANK0_REG) * Rank);
      if (Rank == 3) {
        Offset += (DDRDATA_CR_DELTADQSRANK1_REG - DDRDATA_CR_DELTADQSRANK0_REG);
      }
      DeltaDqsRank.Data = 0;
      MrcWriteCR (MrcData, Offset, DeltaDqsRank.Data);
    } // for Rank
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Force DeltaDQS update\n");
    Offset = DDRSCRAM_CR_DDRMISCCONTROL1_REG;
    DdrMiscControl1.Data = MrcReadCR (MrcData, Offset);
    DdrMiscControl1.Bits.ForceDeltaDQSUpdate = 1;
    MrcWriteCR (MrcData, Offset, DdrMiscControl1.Data);
    // Wait for DeltaDQS Update to complete
    // Poll on register
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Waiting for DeltaDQS Update to complete\n");
    do {
      DdrMiscControl1.Data = MrcReadCR (MrcData, Offset);
      Busy = (DdrMiscControl1.Bits.ForceDeltaDQSUpdate == 1);
    } while (Busy && ((UINT32) MrcCall->MrcGetCpuTime () < Timeout));
    if (Busy) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s DeltaDQS Update did not to complete\n", gErrString);
    }
  }
}
**/
MrcStatus
MrcEarlyOverrides (
  IN  MrcParameters *const  MrcData
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcInput           *Inputs;
  MrcDebug           *Debug;
  UINT32             Data32;
  TrainingStepsEn    *TrainingEnables;
  TrainingStepsEn2   *TrainingEnables2;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Debug   = &MrcData->Outputs.Debug;

  Inputs->IsVrefCAPerDimm = TRUE;

  TrainingEnables  = &Inputs->TrainingEnables;
  TrainingEnables2 = &Inputs->TrainingEnables2;

  // Detect if we are running under Simics
  Data32 = MrcCall->MrcMmioRead32 (Inputs->PciEBaseAddress + 0xFC);
  if ((Data32 & 0xFF) == 0x04) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Simics detected (0x%08X)\n", Data32);
    Inputs->SimicsFlag = 1;
    if (Data32 & MRC_BIT16) {  // Disable most of the MRC logging
      Debug->Level = MSG_LEVEL_TIME;
    }
  }

  if (Inputs->LoopBackTest) {
    MrcCall->MrcSetMem ((UINT8 *) TrainingEnables,  sizeof (TrainingStepsEn),  0);
    MrcCall->MrcSetMem ((UINT8 *) TrainingEnables2, sizeof (TrainingStepsEn2), 0);
    TrainingEnables->JWRL   = 1;
    TrainingEnables->WRTC1D = 1;
    TrainingEnables->RDTC1D = 1;
    TrainingEnables->RMT    = 1;
    Inputs->RmtPerTask      = 0;
    Inputs->ForceSingleRank = 1;
  }
  return mrcSuccess;
}

/**
  This function will override MRC Inputs based on current safe configuration.
  Called if MrcSafeConfig is TRUE.
  These overrides don't depend on DDR type because we don't know it yet.
  Overrides that depend on DDR type will be done during SPD Processing in MrcMcCapabilityPreSpd() below in this file.

  @param[in]  MrcData - Pointer to global MRC data.

  @retval - mrcSuccess
**/
MrcStatus
MrcSafeMode (
  IN  MrcParameters *const  MrcData
  )
{
  MrcInput    *Inputs;

  Inputs  = &MrcData->Inputs;

  // Inputs->WeaklockEn        = 0;
  // Inputs->TatDelta          = 0;
  // Inputs->ScramblerSupport  = 1;
  // Inputs->SrefCfgEna        = 0;
  // Inputs->LpDqsOscEn        = 0;
  // Inputs->Ddr4OneDpc        = 0;  // DDR4 1DPC performance feature: 0 - Disabled; 1 - Enabled on DIMM0 only, 2 - Enabled on DIMM1 only; 3 - Enabled on both DIMMs. (bit [0] - DIMM0, bit [1] - DIMM1)

  // We also disable periodic temperature reads in DDR_MR_PARAMS - see ChannelAddressDecodeConfiguration()
  // MC lp_mode is controlled via Inputs->SafeMode in ConfigSpidLowPowerCtl()

  Inputs->TrainingEnables2.TXTCO      = 0;
  Inputs->TrainingEnables2.CLKTCO     = 0;
  Inputs->TrainingEnables2.CMDSR      = 0;
  Inputs->TrainingEnables2.CMDDSEQ    = 0;
  Inputs->TrainingEnables2.DIMMODTCA  = 0;
  Inputs->TrainingEnables2.TXTCODQS   = 0;
  Inputs->TrainingEnables2.CMDDRUD    = 0;
  Inputs->TrainingEnables2.VCCDLLBP   = 0;
  Inputs->TrainingEnables2.PVTTDNLP   = 0;
  Inputs->TrainingEnables2.RDVREFDC   = 0;
  Inputs->TrainingEnables2.VDDQT      = 0;

  Inputs->TrainingEnables.RCVENC1D    = 0;
  Inputs->TrainingEnables.WRSRT       = 0;
  Inputs->TrainingEnables.TAT         = 0;
  Inputs->TrainingEnables.MEMTST      = 0;
  Inputs->TrainingEnables.ALIASCHK    = 0;
  Inputs->TrainingEnables.RMC         = 0;
  Inputs->TrainingEnables.WRDSUDT     = 0;

  return mrcSuccess;
}

#define MICRON_DRAM_ID  (0x2C80)

/**
  Find the vendor-specific swizzling configuration

  @param[in]  MrcSpdData   - Pointer to Spd data
  @param[in]  DimmOut      - Pointer to DimmOut
  @param[in]  DimmMap      - Dimm Bit Map of the dimm
  @param[in][out] RhTrrCtl - Pointer to RH_TRR_CONTROL
**/
VOID
MrcGetVendorSwizzling (
  IN MrcSpd                                *Spd,
  IN MrcDimmOut                            *DimmOut,
  IN UINT8                                  DimmMap,
  IN OUT MC0_CH0_CR_RH_TRR_CONTROL_STRUCT  *RhTrrCtl
  )
{
  UINT8 DensityIndex;

  DensityIndex = DimmOut->DensityIndex;

  // Search is based on Density and DieRev.
  // 2 Dimm must be configured with same value, so we OR 0x3
  switch (DimmOut->DdrType) {
    case MRC_DDR_TYPE_DDR4:
      if (!((Spd->Ddr4.ManufactureInfo.DramIdCode.Data == MICRON_DRAM_ID) ||
            (Spd->Ddr4.ManufactureInfo.ModuleId.IdCode.Data == MICRON_DRAM_ID))) {
        return;
      }

      if ((Spd->Ddr4.ManufactureInfo.DramStepping == 'E') && (DensityIndex == MRC_SPD_SDRAM_DENSITY_16Gb)) {
        RhTrrCtl->Bits.MA3_Swizzling |= DimmMap;
      }
      if ((Spd->Ddr4.ManufactureInfo.DramStepping == 'R') && (DensityIndex == MRC_SPD_SDRAM_DENSITY_8Gb)) {
        RhTrrCtl->Bits.Micron_R_Swizzling |= DimmMap;
      }
      if ((Spd->Ddr4.ManufactureInfo.DramStepping == 'F') && (DensityIndex == MRC_SPD_SDRAM_DENSITY_16Gb)) {
        RhTrrCtl->Bits.Micron_F_Swizzling |= DimmMap;
      }
      break;

    case MRC_DDR_TYPE_DDR5:
      if (!((Spd->Ddr5.ManufactureInfo.DramIdCode.Data == MICRON_DRAM_ID) ||
            (Spd->Ddr5.ManufactureInfo.ModuleId.IdCode.Data == MICRON_DRAM_ID))) {
        return;
      }

      // Don't check DramStepping for DDR5, partially because DramStepping is often 0 in SPD
      if (DensityIndex == MRC_SPD_SDRAM_DENSITY_16Gb) {
        RhTrrCtl->Bits.MA3_Swizzling |= DimmMap;
      }
      break;

    case MRC_DDR_TYPE_LPDDR4:
      if (!((Spd->Lpddr.ManufactureInfo.DramIdCode.Data == MICRON_DRAM_ID) ||
            (Spd->Lpddr.ManufactureInfo.ModuleId.IdCode.Data == MICRON_DRAM_ID))) {
        return;
      }

      // Don't check DramStepping for LPDDR, partially because DramStepping is often 0 in SPD
      if ((DensityIndex == MRC_SPD_LPDDR_SDRAM_DENSITY_12Gb) ||
          (DensityIndex == MRC_SPD_SDRAM_DENSITY_8Gb)  ||
          (DensityIndex == MRC_SPD_SDRAM_DENSITY_16Gb)) {
        RhTrrCtl->Bits.MA3_Swizzling |= DimmMap;
      }
      break;

    case MRC_DDR_TYPE_LPDDR5:
      if (!((Spd->Lpddr.ManufactureInfo.DramIdCode.Data == MICRON_DRAM_ID) ||
            (Spd->Lpddr.ManufactureInfo.ModuleId.IdCode.Data == MICRON_DRAM_ID))) {
        return;
      }

      // Don't check DramStepping for LPDDR, partially because DramStepping is often 0 in SPD
      if (DensityIndex == MRC_SPD_SDRAM_DENSITY_8Gb) {
        RhTrrCtl->Bits.MA3_Swizzling |= DimmMap;
      }

    default:
      break;
  }
}

/**
  Configure Row Hammer pTRR on the given controller / channel

   @param[in]  MrcData      - Pointer to global MRC data.
   @param[in]  Controller   - MC index
   @param[in]  Channel      - Channel index
   @param[in]  DimmMap      - Present dimms of the Channel

   @retval - TRUE if pTRR is configured
**/
BOOLEAN
MrcRhConfigPTRR (
  IN MrcParameters *const MrcData,
  IN UINT32             Controller,
  IN UINT32             Channel,
  IN UINT8              DimmMap
  )
{
  MrcInput                          *Inputs;
  MrcOutput                         *Outputs;
  const MRC_FUNCTION                *MrcCall;
  MrcDebug                          *Debug;
  MC0_CH0_CR_RH_TRR_CONTROL_STRUCT  RhTrrCtl;
  MC0_CH0_CR_RH_TRR_LFSR_STRUCT     RhTrrLfsr;
  UINT32                            Data32;
  UINT8                             Dimm;
  UINT32                            IpChannel;
  UINT32                            Offset;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  MrcCall = Inputs->Call.Func;
  Debug   = &Outputs->Debug;
  IpChannel = LP_IP_CH (Outputs->Lpddr, Channel);

  if (Inputs->A0 || Inputs->B0 || Inputs->J0) {
    return FALSE;
  }

  MrcCall->MrcGetRandomNumber (&Data32);
  RhTrrLfsr.Bits.LFSR_0 = Data32;
  MrcCall->MrcGetRandomNumber (&Data32);
  RhTrrLfsr.Bits.LFSR_1 = Data32;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u: RH_TRR_LFSR=0x%llx\n", Controller, IpChannel, RhTrrLfsr.Data);
  Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_RH_TRR_LFSR_REG, MC1_CH0_CR_RH_TRR_LFSR_REG, Controller, MC0_CH1_CR_RH_TRR_LFSR_REG, IpChannel);
  MrcWriteCR64 (MrcData, Offset, RhTrrLfsr.Data);

  RhTrrCtl.Data = 0;
  if (Outputs->DdrType == MRC_DDR_TYPE_DDR5) {
    //Default value is 4 for DDR5 and 0 for others
    RhTrrCtl.Bits.BlockState_Delay = 4;
  }
  RhTrrCtl.Bits.TRR_Dimm_Enabled = 0x3;  // Both DIMMs should be configured to the same value
  RhTrrCtl.Bits.LFSR_0_MASK = Inputs->Lfsr0Mask;
  RhTrrCtl.Bits.LFSR_1_MASK = Inputs->Lfsr1Mask;

  for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
    if (DimmMap & (1 << Dimm)) {
      MrcGetVendorSwizzling (
        &Inputs->Controller[Controller].Channel[Channel].Dimm[Dimm].Spd.Data,
        &Outputs->Controller[Controller].Channel[Channel].Dimm[Dimm],
        1 << Dimm,
        &RhTrrCtl
      );
    }
  }

  if (Inputs->RHMASwizzlingOverride) {
    RhTrrCtl.Bits.MA0_Swizzling = Inputs->RHMA0Swizzling;
    RhTrrCtl.Bits.MA3_Swizzling = Inputs->RHMA3Swizzling;
  }
  if (Inputs->RHMicronSwizzlingOverride) {
    RhTrrCtl.Bits.Micron_R_Swizzling = Inputs->RHMicronRSwizzling;
    RhTrrCtl.Bits.Micron_F_Swizzling = Inputs->RHMicronFSwizzling;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u: RH_TRR_CONTROL: 0x%x\n", Controller, IpChannel, RhTrrCtl.Data);
  Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_RH_TRR_CONTROL_REG, MC1_CH0_CR_RH_TRR_CONTROL_REG, Controller, MC0_CH1_CR_RH_TRR_CONTROL_REG, IpChannel);
  MrcWriteCR (MrcData, Offset, RhTrrCtl.Data);

  return TRUE;
}

/**
  Check if RFM is required by DRAM. If it requires, save 4 parameters into vars

   @param[in]  MrcData        - Pointer to global MRC data.
   @param[in]  Controller     - MC number
   @param[in]  Channel        - Channel number
   @param[in]  Dimm           - Dimm number
   @param[out] RAAIMT         - RFM RAAIMT
   @param[out] RAAMMT         - RFM RAAMMT
   @param[out] REF_SUB        - RFM REF_SUB
   @param[out] NORMAL_REF_SUB - RFM NORMAL_REF_SUB

   @retval - TRUE means RFM is required and RAAIMT and other outputs save the parameter values
**/
BOOLEAN
MrcRhCheckRFMRequired (
  IN MrcParameters *const MrcData,
  IN UINT32            Controller,
  IN UINT32            Channel,
  IN UINT32            Dimm,
  OUT UINT32           *RAAIMT,
  OUT UINT32           *RAAMMT,
  OUT UINT32           *REF_SUB,
  OUT UINT32           *NORMAL_REF_SUB
  )
{
  MrcInput                      *Inputs;
  MrcOutput                     *Outputs;
  MrcDebug                      *Debug;
  MrcDdrType                    DdrType;
  UINT32                        Rank;
  UINT32                        RankInDimm;
  UINT32                        RankRAAIMT;
  UINT32                        RankRAA;
  UINT8                         MrrResult[4];
  LPDDR4_MODE_REGISTER_0_TYPE   Lpddr4MR0;
  LPDDR4_MODE_REGISTER_24_TYPE  Lpddr4MR24;
  LPDDR4_MODE_REGISTER_25_TYPE  Lpddr4MR25;
  LPDDR5_MODE_REGISTER_27_TYPE  Lpddr5MR27;
  LPDDR5_MODE_REGISTER_57_TYPE  Lpddr5MR57;
  DDR5_MODE_REGISTER_58_TYPE    Ddr5MR58;
  DDR5_MODE_REGISTER_59_TYPE    Ddr5MR59;
  UINT8                         RfmRequired;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  DdrType = Outputs->DdrType;
  RfmRequired = 0;

  *RAAIMT         = MRC_UINT32_MAX;
  *RAAMMT         = MRC_UINT32_MAX;
  *REF_SUB        = MRC_UINT32_MAX;
  *NORMAL_REF_SUB = MRC_UINT32_MAX;

  if ((DdrType == MRC_DDR_TYPE_DDR4) || Inputs->A0 || (Inputs->RhSelect != RhRfm)) {
    return FALSE;
  }

  for (RankInDimm = 0; RankInDimm < MAX_RANK_IN_DIMM; RankInDimm++) {
    Rank = Dimm * MAX_RANK_IN_DIMM + RankInDimm;
    if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
      continue;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u Dimm%u R%u: ", Controller, Channel, Dimm, Rank);
    // Just check Device 0's MR registers
    switch (DdrType) {
      case MRC_DDR_TYPE_LPDDR4:
        MrcIssueMrr (MrcData, Controller, Channel, Rank, mrMR0, MrrResult);
        Lpddr4MR0.Data8 = MrrResult[0];
        if (Lpddr4MR0.Bits.RFMSupport == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RFM is not supported\n");
          continue;
        }
        MrcIssueMrr (MrcData, Controller, Channel, Rank, mrMR24, MrrResult);
        Lpddr4MR24.Data8 = MrrResult[0];
        if (Lpddr4MR24.Bits.RFM == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RFM is not required\n");
          continue;
        }
        if (Lpddr4MR24.Bits.RAAIMT == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Lpddr4 MR24.RAAIMT bad value: %u\n", Lpddr4MR24.Bits.RAAIMT);
          continue;
        }
        RankRAAIMT = Lpddr4MR24.Bits.RAAIMT * 8;

        MrcIssueMrr (MrcData, Controller, Channel, Rank, mrMR25, MrrResult);
        Lpddr4MR25.Data8 = MrrResult[0];
        switch (Lpddr4MR25.Bits.RAADEC) {
          case 0:
            RankRAA = RankRAAIMT;
            break;
          case 1:
            RankRAA = UDIVIDEROUND (RankRAAIMT * 15, 10);
            break;
          case 2:
            RankRAA = RankRAAIMT * 2;
            break;
          default:
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Lpddr4MR25.RAADEC bad value\n");
            continue;
            break;
        }
        *REF_SUB = MIN (*REF_SUB, RankRAA);
        RankRAA = (Lpddr4MR24.Bits.RAAMMT + 1) * 2 * RankRAAIMT;
        *RAAMMT = MIN (*RAAMMT, RankRAA);
        *RAAIMT = MIN (*RAAIMT, RankRAAIMT);
        *NORMAL_REF_SUB = MIN (*NORMAL_REF_SUB, RankRAAIMT);
        RfmRequired++;
        break;

      case MRC_DDR_TYPE_LPDDR5:
        MrcIssueMrr (MrcData, Controller, Channel, Rank, mrMR27, MrrResult);
        Lpddr5MR27.Data8 = MrrResult[0];
        if (Lpddr5MR27.Bits.RFM == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RFM is not supported\n");
          continue;
        }

        if (Lpddr5MR27.Bits.RAAIMT == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Lpddr5 MR27.RAAIMT bad value: %u\n", Lpddr5MR27.Bits.RAAIMT);
          continue;
        }
        RankRAAIMT = Lpddr5MR27.Bits.RAAIMT * 8;
        *RAAIMT = MIN (*RAAIMT, RankRAAIMT);
        RankRAA = (Lpddr5MR27.Bits.RAAMULT + 1) * 2 * RankRAAIMT;
        *RAAMMT = MIN (*RAAMMT, RankRAA);
        *NORMAL_REF_SUB = MIN (*NORMAL_REF_SUB, RankRAAIMT);

        MrcIssueMrr (MrcData, Controller, Channel, Rank, mrMR57, MrrResult);
        Lpddr5MR57.Data8 = MrrResult[0];
        switch (Lpddr5MR57.Bits.RAADEC) {
          case 0:
            RankRAA = RankRAAIMT;
            break;
          case 1:
            RankRAA = UDIVIDEROUND (RankRAAIMT * 15, 10);
            break;
          case 2:
            RankRAA = RankRAAIMT * 2;
            break;
          case 3:
            RankRAA = RankRAAIMT * 4;
            break;
        }
        *REF_SUB = MIN (*REF_SUB, RankRAA);
        RfmRequired++;
        break;

      case MRC_DDR_TYPE_DDR5:
        MrcIssueMrr (MrcData, Controller, Channel, Rank, mrMR58, MrrResult);
        Ddr5MR58.Data8 = MrrResult[0];
        if (Ddr5MR58.Bits.RfmRequired == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RFM is not required\n");
          continue;
        }

        if ((Ddr5MR58.Bits.Raaimt <= 3) || (Ddr5MR58.Bits.Raaimt >= 11)) {
          // Raaimt: 0~3, 11~15 are RFU
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Ddr5 MR58.Raaimt bad value: %u\n", Ddr5MR58.Bits.Raaimt);
          continue;
        }

        if ((Ddr5MR58.Bits.Raammt <= 2) || (Ddr5MR58.Bits.Raammt >= 7)) {
          // Raammt: 0~2, 7 are RFU
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Ddr5 MR58.Raammt bad value: %u\n", Ddr5MR58.Bits.Raammt);
          continue;
        }

        if (Inputs->FineGranularityRefresh) {
          // FGR
          RankRAAIMT = ((UINT32) Ddr5MR58.Bits.Raaimt) * 4;
          RankRAA = RankRAAIMT * Ddr5MR58.Bits.Raammt * 2;
        } else {
          // Normal
          RankRAAIMT = ((UINT32) Ddr5MR58.Bits.Raaimt) * 8;
          RankRAA = RankRAAIMT * Ddr5MR58.Bits.Raammt;
        }

        MrcIssueMrr (MrcData, Controller, Channel, Rank, mrMR59, MrrResult);
        Ddr5MR59.Data8 = MrrResult[0];
        switch (Ddr5MR59.Bits.RfmRaaCounter) {
          case 0:
            *NORMAL_REF_SUB = MIN (*NORMAL_REF_SUB, RankRAAIMT);
            break;
          case 1:
            *NORMAL_REF_SUB = MIN (*NORMAL_REF_SUB, RankRAAIMT / 2);
            break;
          default:
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Ddr5 MR59.RfmRaaCounter bad value: %u\n", Ddr5MR59.Bits.RfmRaaCounter);
            continue;
            break;
        }

        *RAAIMT = MIN (*RAAIMT, RankRAAIMT);
        *RAAMMT = MIN (*RAAMMT, RankRAA);
        *REF_SUB = MIN (*REF_SUB, RankRAAIMT);
        RfmRequired++;
        break;
      default:
        break;
    } // DdrType
  } // Rank

  return (BOOLEAN) (RfmRequired > 0);
}

/**
  Program Row Hammer mitigation if enabled

  @param[in] MrcData - The MRC general data.

  @retval Returns MrcStatus
**/
MrcStatus
MrcRhPrevention (
  IN MrcParameters *const MrcData
  )
{
  const MrcInput                    *Inputs;
  MrcDebug                          *Debug;
  MrcOutput                         *Outputs;
  UINT32                            Controller;
  UINT32                            Channel;
  UINT32                            IpChannel;
  UINT32                            WmChannel;
  UINT8                             Dimm;
  UINT8                             DimmMax;
  UINT32                            Offset;
  BOOLEAN                           Lpddr;
  UINT8                             DimmPresentMap;
  UINT8                             DimmRFMRequiredMap;
  UINT32                            RAAIMT;
  UINT32                            RAAIMTOneDimm;
  UINT32                            RAAMMT;
  UINT32                            RAAMMTOneDimm;
  UINT32                            REF_SUB;
  UINT32                            REF_SUBOneDimm;
  UINT32                            NORMAL_REF_SUB;
  UINT32                            NORMAL_REF_SUBOneDimm;
  MC0_CH0_CR_ROWHAMMER_CTL_STRUCT   RowHammerCtl;
  BOOLEAN                           RfmRequired;
  BOOLEAN                           PTRRConfigured;
  BOOLEAN                           NeedChangeRefreshWm;
  INT64                             PanicWm;
  INT64                             HpWm;


  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Lpddr   = Outputs->Lpddr;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RH mode: %s\n", (Inputs->RhSelect == RhDisable) ? "Disabled" : ((Inputs->RhSelect == RhRfm) ? "RFM" : "pTRR"));

  DimmPresentMap      = 0;
  DimmRFMRequiredMap  = 0;
  RAAIMT          = MRC_UINT32_MAX;
  RAAMMT          = MRC_UINT32_MAX;
  REF_SUB         = MRC_UINT32_MAX;
  NORMAL_REF_SUB  = MRC_UINT32_MAX;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!IS_MC_SUB_CH (Lpddr, Channel)) {
        DimmPresentMap      = 0;
        DimmRFMRequiredMap  = 0;
        RAAIMT          = MRC_UINT32_MAX;
        RAAMMT          = MRC_UINT32_MAX;
        REF_SUB         = MRC_UINT32_MAX;
        NORMAL_REF_SUB  = MRC_UINT32_MAX;
      }

      // Use DimmMax to deal with a special case of LPDDR.
      // DIMM0 is subch0 and DIMM1 is subch1 in case of LPDDR.
      // LPDDR board might just install memory device at
      // subchannel 0, 2, 4, 6 and not install memory at 1,3,5,7.
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        DimmMax = MAX_DIMMS_IN_CHANNEL;
      } else {
        DimmMax = 0;
      }

      for (Dimm = 0; Dimm < DimmMax; Dimm++) {
        if (Outputs->Controller[Controller].Channel[Channel].Dimm[Dimm].Status != DIMM_PRESENT) {
          continue;
        }

        if (!Lpddr) {
          DimmPresentMap |= (1 << Dimm);
        } else if (!IS_MC_SUB_CH (Lpddr, Channel)) {
          DimmPresentMap |= (1 << dDIMM0);
        } else {
          DimmPresentMap |= (1 << dDIMM1);
        }

        RfmRequired = MrcRhCheckRFMRequired (MrcData, Controller, Channel, Dimm, &RAAIMTOneDimm, &RAAMMTOneDimm, &REF_SUBOneDimm, &NORMAL_REF_SUBOneDimm);
        if (!RfmRequired) {
          continue;
        }

        if (!Lpddr) {
          DimmRFMRequiredMap |= (1 << Dimm);
        } else if (!IS_MC_SUB_CH (Lpddr, Channel)) {
          DimmRFMRequiredMap |= (1 << dDIMM0);
        } else {
          DimmRFMRequiredMap |= (1 << dDIMM1);
        }

        RAAIMT  = MIN (RAAIMT, RAAIMTOneDimm);
        RAAMMT  = MIN (RAAMMT, RAAMMTOneDimm);
        REF_SUB = MIN (REF_SUB, REF_SUBOneDimm);
        NORMAL_REF_SUB = MIN (NORMAL_REF_SUB, NORMAL_REF_SUBOneDimm);
      } // for Dimm

      if ((DimmPresentMap == 0) || (Lpddr && !IS_MC_SUB_CH (Lpddr, Channel))) {
         //  MC registers are per big ch0/1 only
         //  As for LPDDR, we need check sub channel both 0 and 1,
         //  or both 2 and 3, before going ahead to configure RHCTL or PTRR.
         continue;
      }

      // Configure Hardware RHP
      RowHammerCtl.Data = 0;
      if ((Inputs->RhSelect == RhRfm) && (DimmPresentMap == DimmRFMRequiredMap)) {
        // Configure RFM when all DIMMs in the channel support RFM
        if (DimmRFMRequiredMap & (1 << dDIMM0)) {
          RowHammerCtl.Bits.RH_ENABLED_DIMM0 = 1;
        }
        if (DimmRFMRequiredMap & (1 << dDIMM1)) {
          RowHammerCtl.Bits.RH_ENABLED_DIMM1 = 1;
        }

        if (DimmRFMRequiredMap) {
          RowHammerCtl.Bits.RH_HIGH_WM      = RAAMMT;
          RowHammerCtl.Bits.RH_LOW_WM       = RAAIMT;
          RowHammerCtl.Bits.NORMAL_REF_SUB  = NORMAL_REF_SUB;
          RowHammerCtl.Bits.REFM_SUB        = REF_SUB;
          RowHammerCtl.Bits.REFm_EN         = 1;

          // Disable accelerated refresh
          RowHammerCtl.Bits.RH_ACC_REF_EN         = 0;
          RowHammerCtl.Bits.RH_ACC_REF_RATE       = 0;
          RowHammerCtl.Bits.IGNORE_HIGH_WM_BLOCK  = 0;
          RowHammerCtl.Bits.REFm_Disable_on_hot   = 0;
        }
      } else {
        DimmRFMRequiredMap = 0;
      }

      IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH(MC0_CH0_CR_ROWHAMMER_CTL_REG, MC1_CH0_CR_ROWHAMMER_CTL_REG, Controller, MC0_CH1_CR_ROWHAMMER_CTL_REG, IpChannel);
      // Write register even when RFM is disabled as the register might be not 0
      MrcWriteCR64 (MrcData, Offset, RowHammerCtl.Data);
      MRC_DEBUG_MSG (Debug,
          MSG_LEVEL_NOTE,
          "MC%u C%u: Dimm0 RFM %s, Dimm1 RFM %s\n",
          Controller,
          IpChannel,
          DimmRFMRequiredMap & (1 << dDIMM0) ? "on" : "off",
          DimmRFMRequiredMap & (1 << dDIMM1) ? "on" : "off");
      MRC_DEBUG_MSG (Debug,
          MSG_LEVEL_NOTE,
          "RowHammerCtl: 0x%016llx RAAIMT: 0x%x, RAAMMT: 0x%x, NORMAL_REF_SUB: 0x%x, REF_SUB: 0x%x\n",
          RowHammerCtl.Data,
          RAAIMT,
          RAAMMT,
          NORMAL_REF_SUB,
          REF_SUB
          );

      PTRRConfigured = FALSE;
      if ((Inputs->RhSelect != RhDisable) && (DimmRFMRequiredMap == 0)) {
        // Configure pTRR in RFM mode if RFM is not supported, or if user chooses pTRR mode
        PTRRConfigured = MrcRhConfigPTRR (MrcData, Controller, Channel, DimmPresentMap);
      }

      NeedChangeRefreshWm = PTRRConfigured;
      if (NeedChangeRefreshWm) {
        // Wm optimization based on testing results
        PanicWm = MIN (7, Inputs->RefreshPanicWm);
        HpWm = MIN (6, Inputs->RefreshHpWm);
         //  LPDDR4/LPDDR5:
         //    When execution comes here, Channel is an odd number(1 or 3).
         //    As GetSet accepts only even channel number(0 or 2) for LPDDR,
         //    we have to convert Channel to an even number.
         //  DDR4/DDR5:
         //    Just use the Channel number.
        if (!Lpddr) {
          WmChannel = Channel;
        } else {
          // In LPDDR, we have to use even channel number
          WmChannel = Channel - 1;
        }
        // Set REFRESH_PANIC_WM and REFRESH_HP_WM to support watermark finer granularity.
        MrcGetSetMcCh (MrcData, Controller, WmChannel, GsmMctRefreshPanicWm,    WriteToCache | PrintValue, &PanicWm);
        MrcGetSetMcCh (MrcData, Controller, WmChannel, GsmMctRefreshHpWm,       WriteToCache | PrintValue, &HpWm);
      }


    } // for Channel
  }  // for Controler
  MrcFlushRegisterCachedData (MrcData);
  return mrcSuccess;
}

/**
  SA GV flow for the cold boot

  @param[in] MrcData - include all the MRC general data.

  @retval mrcStatus if succeeded.
**/
MrcStatus
MrcSaGvSwitch (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcIntOutput        *MrcIntData;
  MrcOutput           *Outputs;
  MrcSaveData         *SaveData;
  MrcStatus           Status;
  MrcSaGvPoint        SaGvPointIndex;
  MrcSaGvPoint        SaGvPointStart;
  MrcSaGvPoint        SaGvPointMax;
  INT64               GetSetVal;
  UINT64              Timeout;
  UINT32              FreqIndex;
  BOOLEAN             Busy;
  MRC_GEN_MRS_FSM_MR_TYPE MrData[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_MR_GEN_FSM];

  Inputs   = &MrcData->Inputs;
  Outputs  = &MrcData->Outputs;
  MrcCall  = Inputs->Call.Func;
  SaveData = &MrcData->Save.Data;
  Debug    = &Outputs->Debug;
  Status   = mrcSuccess;
  MrcIntData  = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));

  // At this point the MC is in Normal mode with Refreshes enabled
  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT; // 10 seconds timeout

  // Initialize SaGvPoint Start/Max variables for default SaGv
  SaGvPointStart = MrcIntData->SaGvPoint;
  SaGvPointMax   = MrcIntData->SaGvPoint;
  if (SaveData->IsXmpSagvEnabled) {
    // If Dynamic Memory Boost or Realtime Memory Frequency are enabled, save Point 1/2 together
    if (MrcIntData->SaGvPoint == MrcSaGvPoint1) {
      SaGvPointMax   = MrcSaGvPoint2;
    } else if (MrcIntData->SaGvPoint == MrcSaGvPoint4) {
      // SaGv Point 4 is trained, and saved as Points 3/4
      SaGvPointStart = MrcSaGvPoint3;
      SaGvPointMax   = MrcSaGvPoint4;
    }
  }

  for (SaGvPointIndex = SaGvPointStart; SaGvPointIndex <= SaGvPointMax; SaGvPointIndex++) {
    // Save the current point
    GetSetVal = 1;
    FreqIndex = SaGvPointIndex;
    MrcGetSetDdrIoGroupFreqIndex (MrcData, FreqIndex, GsmMccSaveFreqPoint, WriteNoCache, &GetSetVal);
    // Poll for acknowledgment
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Waiting for SAGV %d point save acknowledge\n", FreqIndex);
    do {
      MrcGetSetDdrIoGroupFreqIndex (MrcData, FreqIndex, GsmMccSaveFreqPoint, ReadNoCache, &GetSetVal);
      Busy = (GetSetVal == 1);
    } while (Busy && (MrcCall->MrcGetCpuTime () < Timeout));

    if (Busy) {
      return mrcDeviceBusy;
    }
  }

  if ((Outputs->DdrType == MRC_DDR_TYPE_DDR5) && (Inputs->SaGv == MrcSaGvEnabled) && (MrcIntData->SaGvPoint != MrcSaGvPoint4)) {
    MrcCall->MrcSetMem ((UINT8 *) MrData, sizeof (MrData), 0);
    MrcGenMrsFsmClean (MrcData, MrData, TRUE); // Clean Generic MRS FSM register entries for next SAGV Point
  }

  return Status;
}

/**
  SA GV flow for the Fixed mode.

  @param[in] MrcData  - include all the MRC general data.
  @param[in] SaGvMode - The SAGV mode to be fixed to.

  @retval mrcStatus if succeeded.
**/
MrcStatus
MrcSetSaGvFixed (
  IN  MrcParameters *const MrcData,
  IN  MrcSaGv              SaGvMode
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcStatus           Status;
  UINT32              Data32;
  UINT32              MailboxStatus;
  MrcSaGv             SaGv;

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  MrcCall     = Inputs->Call.Func;
  Debug       = &Outputs->Debug;
  Status      = mrcSuccess;
  SaGv        = Inputs->SaGv;

  // Set the fixed point via CPU Mailbox
  // Assumption here that MrcSaGv values for FixedLow, FixedMed,
  // and FixedHigh match the CPU_MAILBOX command encoding.
  if (SaGv == MrcSaGvDisabled) {
    // In BIOS option "SAGV Disable", need to keep both Qclk and PSF0 policies in the highest point, i.e. use BIOS mailbox SAGV_CONFIG_SET_POLICY to write following values:
    // SAGV_BIOS_QCLK_POLICY = 4
    // SAGV_BIOS_PSF0_POLICY = 4
    // For reference:
    // encoding of heuristics policies
    // %SAGV_POLICY_DYNAMIC                             0   //this policy means we can switch between high and low according to our heuristics
    // %SAGV_POLICY_FIXED_0                             1
    // %SAGV_POLICY_FIXED_1                             2
    // %SAGV_POLICY_FIXED_2                             3
    // %SAGV_POLICY_FIXED_3                             4
    // QCLK control policy DATA[3:0]
    // PSF0 control policy DATA[7:4]
    Data32 = MrcSaGvFixed4 | (MrcSaGvFixed3 << 4);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SAGV is disabled. Set the QCLK and PSF0 policies in the highest point. Value 0x%x\n", Data32);
  } else {
    Data32 = SaGvMode;
  }
  MrcCall->MrcCpuMailboxWrite (MAILBOX_TYPE_PCODE, CPU_MAILBOX_CMD_SAGV_SET_POLICY, Data32, &MailboxStatus);
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "CPU_MAILBOX_CMD_SAGV_SET_POLICY %s. MailboxStatus = %Xh\n",
    (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed",
    MailboxStatus
    );

  return Status;
}

/**
  SA GV flow: configure SAGV switch factor and heuristics.

  @param[in] MrcData  - include all the MRC general data.

  @retval mrcStatus if succeeded.
**/
MrcStatus
MrcSetSaGvThreshold (
  IN  MrcParameters *const MrcData
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  UINT32              Data32;
  UINT32              MailboxStatus;
  UINT32              MailboxCommand;

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  MrcCall     = Inputs->Call.Func;
  Debug       = &Outputs->Debug;

  MailboxCommand =  CPU_MAILBOX_CMD_SAGV_CONFIG_HANDLER | (CPU_MAILBOX_CMD_SAGV_CONFIG_HEURISTICS_SUBCOMMAND << CPU_MAILBOX_CMD_PARAM_1_OFFSET) | (CPU_MAILBOX_CMD_SAGV_CONFIG_HEURISTICS_DOWN << CPU_MAILBOX_CMD_PARAM_2_OFFSET);
  Data32 = Inputs->SagvHeuristicsDownControl;
  MrcCall->MrcCpuMailboxWrite (MAILBOX_TYPE_PCODE, MailboxCommand, Data32, &MailboxStatus);
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "CPU_MAILBOX_CMD_SAGV_CONFIG_HANDLER SAGV_CONFIG_HEURISTICS_DOWN %s. MailboxStatus = %Xh\n",
    (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed",
    MailboxStatus
    );

  MailboxCommand =  CPU_MAILBOX_CMD_SAGV_CONFIG_HANDLER | (CPU_MAILBOX_CMD_SAGV_CONFIG_HEURISTICS_SUBCOMMAND << CPU_MAILBOX_CMD_PARAM_1_OFFSET) | (CPU_MAILBOX_CMD_SAGV_CONFIG_HEURISTICS_UP << CPU_MAILBOX_CMD_PARAM_2_OFFSET);
  Data32 = Inputs->SagvHeuristicsUpControl;
  MrcCall->MrcCpuMailboxWrite (MAILBOX_TYPE_PCODE, MailboxCommand, Data32, &MailboxStatus);
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "CPU_MAILBOX_CMD_SAGV_CONFIG_HANDLER SAGV_CONFIG_HEURISTICS_UP %s. MailboxStatus = %Xh\n",
    (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed",
    MailboxStatus
    );

  Data32 = Inputs->SagvSwitchFactorIA;
  Data32 <<= 0x8; // Use format U8.8
  MailboxCommand =  CPU_MAILBOX_CMD_SAGV_CONFIG_HANDLER | (CPU_MAILBOX_CMD_SAGV_CONFIG_HEURISTICS_SUBCOMMAND << CPU_MAILBOX_CMD_PARAM_1_OFFSET) | (CPU_MAILBOX_CMD_SAGV_CONFIG_FACTOR_IA_DDR_BW << CPU_MAILBOX_CMD_PARAM_2_OFFSET);
  MrcCall->MrcCpuMailboxWrite (MAILBOX_TYPE_PCODE, MailboxCommand, Data32, &MailboxStatus);
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "CPU_MAILBOX_CMD_SAGV_CONFIG_FACTOR_IA_DDR_BW  %s. MailboxStatus = %Xh\n",
    (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed",
    MailboxStatus
    );

  Data32 = Inputs->SagvSwitchFactorGT;
  Data32 <<= 0x8; // Use format U8.8
  MailboxCommand =  CPU_MAILBOX_CMD_SAGV_CONFIG_HANDLER | (CPU_MAILBOX_CMD_SAGV_CONFIG_HEURISTICS_SUBCOMMAND << CPU_MAILBOX_CMD_PARAM_1_OFFSET) | (CPU_MAILBOX_CMD_SAGV_CONFIG_FACTOR_GT_DDR_BW << CPU_MAILBOX_CMD_PARAM_2_OFFSET);
  MrcCall->MrcCpuMailboxWrite (MAILBOX_TYPE_PCODE, MailboxCommand, Data32, &MailboxStatus);
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "CPU_MAILBOX_CMD_SAGV_CONFIG_FACTOR_GT_DDR_BW  %s. MailboxStatus = %Xh\n",
    (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed",
    MailboxStatus
    );

  Data32 = Inputs->SagvSwitchFactorIO;
  Data32 <<= 0x8; // Use format U8.8
  MailboxCommand =  CPU_MAILBOX_CMD_SAGV_CONFIG_HANDLER | (CPU_MAILBOX_CMD_SAGV_CONFIG_HEURISTICS_SUBCOMMAND << CPU_MAILBOX_CMD_PARAM_1_OFFSET) | (CPU_MAILBOX_CMD_SAGV_CONFIG_FACTOR_IO_DDR_BW << CPU_MAILBOX_CMD_PARAM_2_OFFSET);
  MrcCall->MrcCpuMailboxWrite (MAILBOX_TYPE_PCODE, MailboxCommand, Data32, &MailboxStatus);
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "CPU_MAILBOX_CMD_SAGV_CONFIG_FACTOR_IO_DDR_BW  %s. MailboxStatus = %Xh\n",
    (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed",
    MailboxStatus
    );

  Data32 = Inputs->SagvSwitchFactorStall;
  Data32 <<= 0x8; // Use format U8.8
  MailboxCommand =  CPU_MAILBOX_CMD_SAGV_CONFIG_HANDLER | (CPU_MAILBOX_CMD_SAGV_CONFIG_HEURISTICS_SUBCOMMAND << CPU_MAILBOX_CMD_PARAM_1_OFFSET) | (CPU_MAILBOX_CMD_SAGV_CONFIG_FACTOR_IAGT_STALL << CPU_MAILBOX_CMD_PARAM_2_OFFSET);
  MrcCall->MrcCpuMailboxWrite (MAILBOX_TYPE_PCODE, MailboxCommand, Data32, &MailboxStatus);
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "CPU_MAILBOX_CMD_SAGV_CONFIG_FACTOR_IAGT_STALL  %s. MailboxStatus = %Xh\n",
    (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed",
    MailboxStatus
    );

  return mrcSuccess;
}

/**
  Energy Performance Gain.

  @param[in]  MrcData - Pointer to the MRC global data structure

  @retval - Status.
**/
MrcStatus
MrcEnergyPerfGain (
  IN MrcParameters *const MrcData
  )
{
  MrcStatus           Status;
  //const MrcInput      *Inputs;
  //MrcIntOutput        *MrcIntData;
  //MrcCpuModel         CpuModel;
  /*
  UINT32              MailboxStatus;
  UINT8               Index;
  UINT16              MilliWatt[2];
  UINT16              Data[2];
  */

  //Inputs   = &MrcData->Inputs;
  //MrcIntData  = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));

  //CpuModel    = Inputs->CpuModel;

  Status  = mrcSuccess;

  //if ((Inputs->SaGv != MrcSaGvEnabled) || (MrcIntData->SaGvPoint == MrcSaGvPointHigh)) {
    /*
    // If enabled, set up EPG.
    if (Inputs->EpgEnable == 1) {
      for (Index = 0; Index < 2; Index++) {
        // Calculate the 3 parameters (mW):  (Idd3x * Vdd * number of DIMMs present in the system) / 1000.
        MilliWatt[Index] = (UINT16) ((((Index == 0) ? (Inputs->Idd3n) : (Inputs->Idd3p))
            * (Outputs->VddVoltage[Inputs->MemoryProfile]) * (Outputs->Controller[0].Channel[0].DimmCount
            + Outputs->Controller[0].Channel[1].DimmCount)) / 1000);
        // Convert to fixed point 8.8 value.  Integer8.8 = (milliwatts / 1000) * 2^8
        Data[Index] = MilliWatt[Index] * 256 / 1000;
        // Write to mailbox register.
        if (MrcCall->MrcCpuMailboxWrite != NULL) {
          MrcCall->MrcCpuMailboxWrite (MAILBOX_TYPE_PCODE,
            ((Index == 0) ? SET_EPG_BIOS_POWER_OVERHEAD_0_CMD : SET_EPG_BIOS_POWER_OVERHEAD_1_CMD),
            Data[Index], &MailboxStatus);
          MRC_DEBUG_MSG (
            Debug,
            MSG_LEVEL_NOTE,
            "SET_EPG_BIOS_POWER_OVERHEAD_%d_CMD %s. Value = %08Xh. MailboxStatus = %Xh\n",
            Index,
            (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) ? "success" : "failed",
            Data[Index],
            MailboxStatus);
        }  // Write to mailbox register.
      }  // for loop
    }  // EpgEnable
    */
  //}
  return Status;
}

/**
  this function is the last function that call from the MRC core.
    the function set DISB and set the MRC_Done.

  @param[in] MrcData - include all the MRC general data.

  @retval Always returns mrcSuccess.
**/
MrcStatus
MrcDone (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput      *Inputs;
  const MRC_FUNCTION  *MrcCall;
  MrcDebug            *Debug;
  MrcSaGv             SaGv;
  INT64               GetSetVal;
  UINT64              Timeout;
  UINT32              Controller;
  BOOLEAN             Flag;
  M_COMP_PCU_STRUCT   MCompPcu;

  Inputs  = &MrcData->Inputs;
  Debug   = &MrcData->Outputs.Debug;
  MrcCall = Inputs->Call.Func;
  SaGv    = Inputs->SaGv;

  // This is disabled during training, and re-enabled at the end of MRC
  // This will set mc_rch_stall_phase.dis_all_cpl_interleave = 0
  MrcSetMcCplInterleave (MrcData, 0);

  // scr_gvblock_ovrride should be enabled at the end of last SAGV point training.
  ScrGvblockOverrideEnable (MrcData);

  //if SAGV mode is fixed to one mode
  if ((SaGv != MrcSaGvDisabled) && (SaGv != MrcSaGvEnabled)) {
    MrcSetSaGvFixed (MrcData, SaGv);
  } else if (Inputs->RealtimeMemoryFrequency && (SaGv == MrcSaGvEnabled)) {
    //if using Realtime Memory Frequency, use fixed low to start, and it can be toggled between fixed low and fixed high during runtime.
    MrcSetSaGvFixed (MrcData, MrcSaGvFixed1);
  } else if (SaGv == MrcSaGvDisabled) {
    MrcSetSaGvFixed (MrcData, MrcSaGvFixed4);
  } else if (Inputs->DynamicMemoryBoost && (SaGv == MrcSaGvEnabled)) {
    //Currently, we configure threshold only when DMB is running
    MrcSetSaGvThreshold(MrcData);
  }

  GetSetVal = 1;
  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableRefresh, WriteNoCache, &GetSetVal);

  // Enable Periodic Comp with periodic interval = 10uS*2^COMP_INT
  MCompPcu.Data = 0;
  MCompPcu.Bits.COMP_INTERVAL = COMP_INT;
  MrcWriteCR (MrcData, M_COMP_PCU_REG, MCompPcu.Data);

  // lock the MC and memory map registers.
  McRegistersLock (MrcData);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller) || (Controller == 0)) {
      MrcGetSetMc (MrcData, Controller, GsmMccMrcDone, WriteNoCache, &GetSetVal);
    }
  }

  // Wait for mc_init_done_ack
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Waiting for mc_init_done Acknowledge\n");

  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT; // 10 seconds timeout
  do {
    Flag = 0;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      if (MrcControllerExist (MrcData, Controller)) {
        MrcGetSetMc (MrcData, Controller, GsmMccMcInitDoneAck, ReadNoCache, &GetSetVal);
      }
      Flag |= (GetSetVal == 0);
      if (Inputs->SimicsFlag) {
        Flag = 0;
      }
    }
  } while (Flag && (MrcCall->MrcGetCpuTime () < Timeout));
  if (Flag) {
    return mrcFail;
  }

//  MrcWriteCR64 (MrcData, SSKPD_PCU_REG, 1); // We use this register to indicate "MRC done"


  return mrcSuccess;
}

/**
  Check for symmetric dimm/channel/mc population

  @param[in] *MrcData - Pointer to the MRC Debug structure.

  @retval Returns TRUE if symmetric
  @retval Returns FALSE if not symmetric
**/
BOOLEAN
MrcIsSymmetric (
  IN MrcParameters *MrcData
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcInput           *Inputs;
  MrcOutput          *Outputs;
  MrcDebug           *Debug;
  UINT8              Controller;
  INT64              SDimmSize[MAX_CONTROLLER][MAX_CHANNEL];
  INT64              LDimmSize[MAX_CONTROLLER][MAX_CHANNEL];
  INT64              ChannelSize[MAX_CONTROLLER][MAX_CHANNEL];
  INT64              ControllerSize[MAX_CONTROLLER];
  UINT8              MaxChannels;
  UINT8              Channel;
  BOOLEAN            DimmSymmetrical;
  BOOLEAN            ChannelSymmetrical;
  BOOLEAN            ControllerSymmetrical;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  MaxChannels = Outputs->MaxChannels;
  MrcCall->MrcSetMem ((UINT8 *) SDimmSize, sizeof (SDimmSize), 0);
  MrcCall->MrcSetMem ((UINT8 *) LDimmSize, sizeof (LDimmSize), 0);
  MrcCall->MrcSetMem ((UINT8 *) ChannelSize, sizeof (ChannelSize), 0);
  MrcCall->MrcSetMem ((UINT8 *) ControllerSize, sizeof (ControllerSize), 0);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccSDimmSize, ReadUncached, &SDimmSize[Controller][Channel]);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccLDimmSize, ReadUncached, &LDimmSize[Controller][Channel]);
        ChannelSize[Controller][Channel] = SDimmSize[Controller][Channel] + LDimmSize[Controller][Channel];
        ControllerSize[Controller] += ChannelSize[Controller][Channel];
      }
    }
  }

  DimmSymmetrical = (((LDimmSize[0][0] == SDimmSize[0][0]) || (SDimmSize[0][0] == 0)) &&
                     ((LDimmSize[0][1] == SDimmSize[0][1]) || (SDimmSize[0][1] == 0)) &&
                     ((LDimmSize[1][0] == SDimmSize[1][0]) || (SDimmSize[1][0] == 0)) &&
                     ((LDimmSize[1][1] == SDimmSize[1][1]) || (SDimmSize[1][1] == 0)));

  ChannelSymmetrical = (((ChannelSize[0][0] == ChannelSize[0][1]) || (ChannelSize[0][0] == 0) || (ChannelSize[0][1] == 0)) &&
                        ((ChannelSize[1][0] == ChannelSize[1][1]) || (ChannelSize[1][0] == 0) || (ChannelSize[1][1] == 0)));

  ControllerSymmetrical = ((ControllerSize[0] == ControllerSize[1]) || (ControllerSize[0] == 0) || (ControllerSize[1] == 0));
  // Symmetric only if all of the below is true:
  // - DIMM (sub-channel) sizes are equal, or only one exists
  // - Channel sizes are equal, or only one exists
  // - MC/Slice sizes are equal, or only one exists
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Symmetrical config check: DIMM: %s, Channel: %s, Slice: %s\n",
    DimmSymmetrical ? "yes" : "no",
    ChannelSymmetrical ? "yes" : "no",
    ControllerSymmetrical ? "yes" : "no"
    );
  if (!(DimmSymmetrical && ChannelSymmetrical && ControllerSymmetrical)) {
    return FALSE;
  }

  return TRUE;
}

/**
  Enables IBECC if supported by CPU, and if dimm/channel/mc population is symmetric

  @param[in] *MrcData - Pointer to the MRC Debug structure.

  @retval Returns mrcSuccess
**/
MrcStatus
MrcIbecc (
  IN MrcParameters *MrcData
  )
{
  MrcDebug                              *Debug;
  const MrcInput                        *Inputs;
  MrcOutput                             *Outputs;
  MrcMemoryMap                          *MemoryMap;
  UINT32                                Offset;
  UINT8                                 Controller;
  UINT8                                 IbeccRange;
  MC0_IBECC_CONTROL_STRUCT              IbeccControl;
  MC0_IBECC_ACTIVATE_STRUCT             IbeccActivate;
  MC0_IBECC_STORAGE_ADDR_STRUCT         IbeccStorageAddr;
  MC0_IBECC_PROTECT_ADDR_RANGE_0_STRUCT IbeccAddressRange;
  MC0_IBECC_TME_CONTROL_STRUCT          IbeccTmeControl;
  CMI_MEMORY_SLICE_HASH_STRUCT          CmiMemorySliceHash;
  MC0_IBECC_INJ_CONTROL_STRUCT          IbeccInjControl;
  MC0_ECC_INJ_ADDR_COMPARE_STRUCT       EccInjAddrCompare;
  MC0_ECC_INJ_ADDR_MASK_STRUCT          EccInjAddrMask;
  MC0_IBECC_INJ_COUNT_STRUCT            IbeccInjCount;

  Inputs    = &MrcData->Inputs;
  Outputs   = &MrcData->Outputs;
  Debug     = &Outputs->Debug;
  MemoryMap = &Outputs->MemoryMapData;

  if (Inputs->DtHalo && Outputs->EccSupport && Inputs->EccDftEn && !Inputs->Write0) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcIbecc Inject Ecc\n");
    MrcWriteCR64 (MrcData, MC1_ECC_INJ_ADDR_COMPARE_REG, Inputs->EccErrInjAddress);
    MrcWriteCR64 (MrcData, MC1_ECC_INJ_ADDR_MASK_REG, Inputs->EccErrInjMask);
    MrcWriteCR (MrcData, MC1_BC_CR_ECC_INJECT_COUNT_REG, Inputs->EccErrInjCount);
  }

  if (!MrcIsSymmetric (MrcData) || !(Inputs->Ibecc)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "IBECC disabled\n");
    return mrcSuccess;
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    Offset = OFFSET_CALC_CH (MC0_IBECC_CONTROL_REG, MC1_IBECC_CONTROL_REG, Controller);
    IbeccControl.Data = MrcReadCR (MrcData, Offset);
    IbeccControl.Bits.OPERATION_MODE = Inputs->IbeccOperationMode;
    MrcWriteCR (MrcData, Offset, IbeccControl.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u MODE:%u\n", Controller, IbeccControl.Bits.OPERATION_MODE);

    if ((Inputs->IbeccOperationMode == IbeccModeProtectedRanges) || (Inputs->IbeccOperationMode == IbeccModeProtectedAll)) {
      // Program EDSR
      Offset = OFFSET_CALC_CH (MC0_IBECC_STORAGE_ADDR_REG, MC1_IBECC_STORAGE_ADDR_REG, Controller);
      IbeccStorageAddr.Data = MrcReadCR (MrcData, Offset);
      IbeccStorageAddr.Bits.START_ADDR = MemoryMap->TomMinusMe;
      MrcWriteCR (MrcData, Offset, IbeccStorageAddr.Data);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "START_ADDR:%X\n", IbeccStorageAddr.Bits.START_ADDR);
    }

      if (Inputs->IbeccOperationMode == IbeccModeProtectedAll) {
        Offset = OFFSET_CALC_CH (MC0_IBECC_PROTECT_ADDR_RANGE_0_REG, MC1_IBECC_PROTECT_ADDR_RANGE_0_REG, Controller);
        IbeccAddressRange.Data = MrcReadCR64 (MrcData, Offset);
        IbeccAddressRange.Bits.BASE = 0;
        IbeccAddressRange.Bits.MASK = 0;
        IbeccAddressRange.Bits.ENABLE = MRC_ENABLE;
        MrcWriteCR64 (MrcData, Offset, IbeccAddressRange.Data);
      }

    if (Inputs->IbeccOperationMode == IbeccModeProtectedRanges) {
      for (IbeccRange = 0; IbeccRange < MAX_IBECC_REGIONS; IbeccRange++) {
        if (Inputs->IbeccProtectedRangeEnable[IbeccRange] == MRC_ENABLE) {
          Offset = OFFSET_CALC_CH (MC0_IBECC_PROTECT_ADDR_RANGE_0_REG, MC1_IBECC_PROTECT_ADDR_RANGE_0_REG, Controller);
          Offset += ((MC0_IBECC_PROTECT_ADDR_RANGE_1_REG - MC0_IBECC_PROTECT_ADDR_RANGE_0_REG) * IbeccRange);
          IbeccAddressRange.Data = MrcReadCR64 (MrcData, Offset);
          IbeccAddressRange.Bits.BASE = Inputs->IbeccProtectedRangeBase[IbeccRange];
          IbeccAddressRange.Bits.MASK = Inputs->IbeccProtectedRangeMask[IbeccRange];
          IbeccAddressRange.Bits.ENABLE = MRC_ENABLE;
          MrcWriteCR64 (MrcData, Offset, IbeccAddressRange.Data);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ProtectedRange[%u]:BASE:%X MASK:%X\n", IbeccRange, IbeccAddressRange.Bits.BASE, IbeccAddressRange.Bits.MASK);
        }
      }
    }

    CmiMemorySliceHash.Data = MrcReadCR64 (MrcData, CMI_MEMORY_SLICE_HASH_REG);
    Offset = OFFSET_CALC_CH (MC0_IBECC_TME_CONTROL_REG, MC1_IBECC_TME_CONTROL_REG, Controller);
    IbeccTmeControl.Data = MrcReadCR64 (MrcData, Offset);
    IbeccTmeControl.Bits.hash_lsb = CmiMemorySliceHash.Bits.HASH_LSB_MASK_BIT;
    IbeccTmeControl.Bits.hash_enabled = (CmiMemorySliceHash.Bits.HASH_MASK == 1) ? 0 : 1;
    MrcWriteCR64 (MrcData, Offset, IbeccTmeControl.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "TME_CONTROL:hash_lsb %x hash_enabled %x\n", IbeccTmeControl.Bits.hash_lsb, IbeccTmeControl.Bits.hash_enabled);

    Offset = OFFSET_CALC_CH (MC0_IBECC_ACTIVATE_REG, MC1_IBECC_ACTIVATE_REG, Controller);
    IbeccActivate.Data = MrcReadCR (MrcData, Offset);
    IbeccActivate.Bits.IBECC_EN = MRC_ENABLE;
    MrcWriteCR (MrcData, Offset, IbeccActivate.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "IBECC_EN:%d\n", IbeccActivate.Bits.IBECC_EN);

    if (Inputs->IbeccErrInjControl != IbeccErrInjNoErrorInjection) {
      if ((Inputs->IbeccErrInjControl == IbeccErrInjCorrAddressMatch) || (Inputs->IbeccErrInjControl == IbeccErrInjUncorrAddressMatch)) {
        EccInjAddrCompare.Data = Inputs->IbeccErrInjAddress;
        Offset = OFFSET_CALC_CH (MC0_IBECC_INJ_ADDR_COMPARE_REG, MC1_IBECC_INJ_ADDR_COMPARE_REG, Controller);
        MrcWriteCR64 (MrcData, Offset, EccInjAddrCompare.Data);

        EccInjAddrMask.Data = Inputs->IbeccErrInjMask;
        Offset = OFFSET_CALC_CH (MC0_IBECC_INJ_ADDR_MASK_REG, MC1_IBECC_INJ_ADDR_MASK_REG, Controller);
        MrcWriteCR64 (MrcData, Offset, EccInjAddrMask.Data);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "IBERR INJ Addr:%llX Mask:%llX\n", EccInjAddrCompare.Data, EccInjAddrMask.Data);
      }

      if ((Inputs->IbeccErrInjControl == IbeccErrInjCorrCountInsertion) || (Inputs->IbeccErrInjControl == IbeccErrInjUncorrCountInsertion)) {
        IbeccInjCount.Data = 0;
        IbeccInjCount.Bits.COUNT = Inputs->IbeccErrInjCount;
        Offset = OFFSET_CALC_CH (MC0_IBECC_INJ_COUNT_REG, MC1_IBECC_INJ_COUNT_REG, Controller);
        MrcWriteCR (MrcData, Offset, IbeccInjCount.Data);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "IBERR INJ Count:%u\n", IbeccInjCount.Bits.COUNT);
      }

      IbeccInjControl.Data = 0;
      IbeccInjControl.Bits.ECC_Inj = Inputs->IbeccErrInjControl;
      Offset = OFFSET_CALC_CH (MC0_IBECC_INJ_CONTROL_REG, MC1_IBECC_INJ_CONTROL_REG, Controller);
      MrcWriteCR (MrcData, Offset, IbeccInjControl.Data);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "IBECC_INJ:%u\n", IbeccInjControl.Bits.ECC_Inj);
    }

  } // Controller

  return mrcSuccess;
}

/**
  Print the MRC version to the MRC output device.

  @param[in] Debug   - Pointer to the MRC Debug structure.
  @param[in] Version - The MRC version.

  @retval Nothing.
**/
void
MrcVersionPrint (
  IN MrcParameters     *MrcData,
  IN const MrcVersion  *Version
  )
{
#ifdef MRC_DEBUG_PRINT
  MrcDebug *Debug;

  Debug    = &MrcData->Outputs.Debug;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "**********************************************************************\n");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "** Copyright (c) 2011- 2020 Intel Corporation. All rights reserved. **\n");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "** Alderlake memory detection and initialization code.              **\n");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "** Major version number is:   %2u                                    **\n", Version->Major);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "** Minor version number is:   %2u                                    **\n", Version->Minor);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "** Rev version number is:     %2u                                    **\n", Version->Rev);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "** Build number is:           %2u                                    **\n", Version->Build);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "**********************************************************************\n");
#endif
  return;
}

/**
  This function return the MRC version.

  @param[in] MrcData - Include all MRC global data.
  @param[out] Version - Location to store the MRC version.

  @retval Nothing.
**/
void
MrcVersionGet (
  IN  const MrcParameters *const MrcData,
  OUT MrcVersion *const Version
  )
{
  const MrcInput     *Inputs;
  const MRC_FUNCTION *MrcCall;

  if (Version != NULL) {
    Inputs  = &MrcData->Inputs;
    MrcCall = Inputs->Call.Func;
    MrcCall->MrcCopyMem ((UINT8 *) Version, (UINT8 *) &cVersion.Version, sizeof (MrcVersion));
  }
}

/**
  This function set the MRC version to MRC_REVISION register.
  Can be called as soon as CMI PLL is locked and MC registers are available.

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess.
**/
MrcStatus
MrcSetMrcVersion (
  IN     MrcParameters *const MrcData
  )
{
  MrcVersion const         *Version;
  MC0_MRC_REVISION_STRUCT  MrcRevision;

  Version = &MrcData->Outputs.Version;
  MrcRevision.Data = (((UINT32) Version->Major) << 24) |
                     (((UINT32) Version->Minor) << 16) |
                     (((UINT32) Version->Rev)   << 8)  |
                     (((UINT32) Version->Build));

  if (MrcControllerExist (MrcData, 0)) {
    MrcWriteCR (MrcData, MC0_MRC_REVISION_REG, MrcRevision.Data);
  } else {
    MrcWriteCR (MrcData, MC1_MRC_REVISION_REG, MrcRevision.Data);
  }
  return mrcSuccess;
}

/**
  This function locks the memory controller and memory map registers.

  @param[in] MrcData - Include all MRC global data.

  @retval Nothing.
**/
void
McRegistersLock (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput                                 *Inputs;
  const MRC_FUNCTION                             *MrcCall;
  MrcDebug                                       *Debug;
  MrcSaveData                                    *SaveData;
  UINT32                                         Offset;
  UINT32                                         PciEBaseAddress;
  TOM_0_0_0_PCI_STRUCT                           Tom;
  TOLUD_0_0_0_PCI_STRUCT                         Tolud;
  TOUUD_0_0_0_PCI_STRUCT                         Touud;
  TSEGMB_0_0_0_PCI_STRUCT                        Tsegmb;
  BDSM_0_0_0_PCI_STRUCT                          Bdsm;
  BGSM_0_0_0_PCI_STRUCT                          Bgsm;
  DPR_0_0_0_PCI_STRUCT                           Dpr;

  Debug   = &MrcData->Outputs.Debug;
  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  SaveData  = &MrcData->Save.Data;
  PciEBaseAddress = Inputs->PciEBaseAddress;

  // Lock PRMRR.  Convert from MB to Address.
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcSetLockPrmrr: 0x%x\n", MrcCall->MrcSetLockPrmrr);
  MrcCall->MrcSetLockPrmrr (MrcCall->MrcLeftShift64 (MrcData->Outputs.MemoryMapData.PrmrrBase, 20), Inputs->PrmrrSize << 20);
  // Lock TME.
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcTmeInit: 0x%x\n", MrcCall->MrcTmeInit);
  MrcCall->MrcTmeInit ((UINT32)(SaveData->TmeEnable), Inputs->TmeExcludeBase, Inputs->TmeExcludeSize);
  // Lock the memory map registers.
  // Lock TOM.
  Offset        = PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, TOM_0_0_0_PCI_REG);
  Tom.Data32[0] = MrcCall->MrcMmioRead32 (Offset);
  Tom.Bits.LOCK = 1;
  MrcCall->MrcMmioWrite32 (Offset, Tom.Data32[0]);

  // Lock TOLUD.
  Offset          = PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, TOLUD_0_0_0_PCI_REG);
  Tolud.Data      = MrcCall->MrcMmioRead32 (Offset);
  Tolud.Bits.LOCK = 1;
  MrcCall->MrcMmioWrite32 (Offset, Tolud.Data);

  // Lock TOUUD.
  Offset          = PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, TOUUD_0_0_0_PCI_REG);
  Touud.Data32[0] = MrcCall->MrcMmioRead32 (Offset);
  Touud.Bits.LOCK = 1;
  MrcCall->MrcMmioWrite32 (Offset, Touud.Data32[0]);

  // Lock DPR register
  Offset        = PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, DPR_0_0_0_PCI_REG);
  Dpr.Data      = MrcCall->MrcMmioRead32 (Offset);
  Dpr.Bits.LOCK = 0;
  MrcCall->MrcMmioWrite32 (Offset, Dpr.Data);

  // Lock TSEG.
  Offset         = PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, TSEGMB_0_0_0_PCI_REG);
  Tsegmb.Data    = MrcCall->MrcMmioRead32 (Offset);
  Tsegmb.Bits.LOCK = 1;
  MrcCall->MrcMmioWrite32 (Offset, Tsegmb.Data);

  // Lock BDSM.
  Offset         = PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, BDSM_0_0_0_PCI_REG);
  Bdsm.Data      = MrcCall->MrcMmioRead32 (Offset);
  Bdsm.Bits.LOCK = 1;
  MrcCall->MrcMmioWrite32 (Offset, Bdsm.Data);

  // Lock BGSM.
  Offset         = PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, BGSM_0_0_0_PCI_REG);
  Bgsm.Data      = MrcCall->MrcMmioRead32 (Offset);
  Bgsm.Bits.LOCK = 1;
  MrcCall->MrcMmioWrite32 (Offset, Bgsm.Data);

  // Lock the Ia/Gt exclusion
  MrcCall->MrcSetIaGtImrExclusionLock ();

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMemory map registers are locked\n");

  return;
}

/**
  This function returns the recommended MRC boot mode.

  @param[in] MrcData - The global host structure

  @retval bmWarm if we are in self refresh and the DISB bit is set, otherwise returns bmCold.
**/
MRC_BOOT_MODE
MrcGetBootMode (
  IN MrcParameters * const MrcData
  )
{
  MRC_BOOT_MODE BootMode;
  MrcInput     *Inputs;
  MRC_FUNCTION *MrcCall;
  UINT32       RegisterVal32;

  Inputs    = &MrcData->Inputs;
  MrcCall   = Inputs->Call.Func;

//@todo ICL PCH may have changed this
  RegisterVal32 = MrcCall->MrcMmioRead32((UINTN)PCH_PWRM_BASE_ADDRESS + R_PCH_PWRM_GEN_PMCON_A);
  if (((RegisterVal32 & B_PCH_PWRM_GEN_PMCON_A_MEM_SR_MRC) != 0) && ((RegisterVal32 & B_PCH_PWRM_GEN_PMCON_A_DISB_MRC) != 0))  {
    BootMode = bmWarm;
  } else {
    BootMode = bmCold;
  }

  return BootMode;
}

/**
  This function sets the DISB bit in General PM Configuration.

  @param[in] MrcData - The global host structure

  @retval Nothing.
**/
void
MrcSetDISB (
  IN MrcParameters *const MrcData
  )
{
  MrcInput      *Inputs;
  MRC_FUNCTION  *MrcCall;
  UINT32        RegOffset;
  UINT8         RegisterVal;
  UINT8         Rw1cBits;

  Inputs    = &MrcData->Inputs;
  MrcCall   = Inputs->Call.Func;

  RegOffset = PCH_PWRM_BASE_ADDRESS + R_PCH_PWRM_GEN_PMCON_A;
  // The bits we care about are between Bit 16-23.  There are many RW/1C bits which we would like to avoid.
  // Thus we will read/write a UINT8 at the register offset +2 Bytes.
  RegOffset += 2;

  RegisterVal = MrcCall->MrcMmioRead8 (RegOffset);

  // Bit 23 is DISB.  So we want to set this.  Bit 23 of the register is bit 7 at RegOffset+2
  RegisterVal |=  MRC_BIT7;

  // Bit 18 and Bit 16 are RW/1C.  So we will set these bits to 0 before writing the register.
  // Bit 18 is Bit 2 of the Byte.
  // Bit 16 is Bit 0 of the Byte.
  Rw1cBits = MRC_BIT0 | MRC_BIT2;
  // Invert the mask so RW/1C bits are 0 and the rest are 1's.  Then AND this with the value to be written.
  Rw1cBits = ~Rw1cBits;
  RegisterVal &= Rw1cBits;

  MrcCall->MrcMmioWrite8 (RegOffset, RegisterVal);
}

/**
  This function resets the DISB bit in General PM Configuration.

  @param[in] MrcData - The global host structure

  @retval Nothing.
**/
void
MrcResetDISB (
  IN MrcParameters *const MrcData
  )
{
  MrcInput      *Inputs;
  MRC_FUNCTION  *MrcCall;
  UINT32        RegOffset;
  UINT8         RegisterVal;
  UINT8         Rw1cBits;

  Inputs    = &MrcData->Inputs;
  MrcCall   = Inputs->Call.Func;

  RegOffset = PCH_PWRM_BASE_ADDRESS + R_PCH_PWRM_GEN_PMCON_A;
  // The bits we care about are between Bit 16-23.  There are many RW/1C bits which we would like to avoid.
  // Thus we will read/write a UINT8 at the register offset +2 Bytes.
  RegOffset += 2;

  RegisterVal = MrcCall->MrcMmioRead8 (RegOffset);

  // Bit 18 and Bit 16 are RW/1C.  So we will set these bits to 0 before writing the register.
  // Bit 18 is Bit 2 of the Byte.
  // Bit 16 is Bit 0 of the Byte.
  Rw1cBits = MRC_BIT0 | MRC_BIT2;
  // Bit 23 is DISB.  So we want to clear this.  Bit 23 of the register is bit 7 at RegOffset+2
  Rw1cBits |=  MRC_BIT7;
  // Invert the mask so RW/1C bits and DISB are 0 and the rest are 1's.  Then AND this with the value to be written.
  Rw1cBits = ~Rw1cBits;

  RegisterVal &= Rw1cBits;

  MrcCall->MrcMmioWrite8 (RegOffset, RegisterVal);
}

/**
  This function reads the CAPID0 register and sets the memory controller's capability.

  @param[in, out] MrcData - All the MRC global data.

  @retval Returns mrcSuccess if the memory controller's capability has been determined, otherwise returns mrcFail.
**/
MrcStatus
MrcMcCapability (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcInput                  *Inputs;
  MrcDebug                  *Debug;
  const MRC_FUNCTION        *MrcCall;
  MrcSaveData               *Save;
  MrcOutput                 *Outputs;
  MrcControllerOut          *ControllerOut;
  MrcChannelOut             *ChannelOut;
  MrcDimmOut                *DimmOut;
  MrcDdrType                DdrType;
  MrcProfile                Profile;
  UINT8                     MaxChannel;
  UINT32                    DimmCount;
  UINT32                    Max;
  UINT32                    Size;
  UINT32                    ChannelNum;
  UINT32                    DimmNum;
  UINT32                    ChDimmCount;
  UINT32                    Offset;
  UINT16                    NModeMinimum;
  UINT8                     Controller;
  UINT8                     Channel;
  UINT8                     Dimm;
  BOOLEAN                   Cmd2N;
  BOOLEAN                   UlxUlt;
  BOOLEAN                   Lpddr4;
  BOOLEAN                   Lpddr5;
  BOOLEAN                   Ddr4;
  BOOLEAN                   Ddr5;
  BOOLEAN                   EccSupport;
  BOOLEAN                   IgnoreNonEccDimm;
  const char                *StrDdrType;
  CAPID0_A_0_0_0_PCI_STRUCT Capid0A;
  CAPID0_B_0_0_0_PCI_STRUCT Capid0B;
  CAPID0_C_0_0_0_PCI_STRUCT Capid0C;
  CAPID0_E_0_0_0_PCI_STRUCT Capid0E;
  DEVEN_0_0_0_PCI_STRUCT    Deven;

  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  Outputs     = &MrcData->Outputs;
  Save        = &MrcData->Save.Data;
  Debug       = &Outputs->Debug;
  ChDimmCount = MAX_DIMMS_IN_CHANNEL;
  Profile     = Inputs->MemoryProfile;
  DdrType     = Outputs->DdrType;
  UlxUlt      = (Inputs->UlxUlt);
  MaxChannel  = Outputs->MaxChannels;
  Ddr4        = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5        = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr4      = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);

  // Obtain the capabilities of the memory controller.
  Offset       = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_A_0_0_0_PCI_REG);
  Capid0A.Data = MrcCall->MrcMmioRead32 (Offset);

  Offset       = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_B_0_0_0_PCI_REG);
  Capid0B.Data = MrcCall->MrcMmioRead32 (Offset);

  Offset       = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_C_0_0_0_PCI_REG);
  Capid0C.Data = MrcCall->MrcMmioRead32 (Offset);

  Offset       = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_E_0_0_0_PCI_REG);
  Capid0E.Data = MrcCall->MrcMmioRead32 (Offset);

  Save->McCapId.A = Capid0A.Data;
  Save->McCapId.B = Capid0B.Data;
  Save->McCapId.C = Capid0C.Data;
  Save->McCapId.E = Capid0E.Data;

  Offset     = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, DEVEN_0_0_0_PCI_REG);
  Deven.Data = MrcCall->MrcMmioRead32 (Offset);

  // Check that current DDR type is allowed on this CPU
  StrDdrType = NULL;
  if (Lpddr4 && (Capid0C.Bits.LPDDR4_EN == 0)) {
    StrDdrType = gDdrTypeStr[MRC_DDR_TYPE_LPDDR4];
  } else if (Lpddr5 && (Capid0E.Bits.LPDDR5_EN == 0)) {
    StrDdrType = gDdrTypeStr[MRC_DDR_TYPE_LPDDR5];
  } else if (Ddr4 && (Capid0C.Bits.DDR4_EN == 0)) {
    StrDdrType = gDdrTypeStr[MRC_DDR_TYPE_DDR4];
  } else if (Ddr5 && (Capid0E.Bits.DDR5_EN == 0)) {
    StrDdrType = gDdrTypeStr[MRC_DDR_TYPE_DDR5];
  }

  if (StrDdrType != NULL) {
    // MRC detected a memory technology and CAPID value shows the memory tech is not supported by this CPU.
    // e.g. LPDDR4 memory detected but CPU only supports DDR4.
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "ERROR: %s is not supported on this CPU\n", StrDdrType);
    return mrcDimmNotSupport;
  }

  // Determine Vccddq Limit
  Outputs->VccddqLimit = (UINT16) Capid0E.Bits.VDDQ_VOLTAGE_MAX;

  Outputs->IsOverclockCapable = (Capid0A.Bits.DDR_OVERCLOCK > 0) ? TRUE : FALSE;

  if (!(Lpddr4 || Lpddr5 || Ddr4 || Ddr5)) {
    // MRC detected memory technology besides what is supported by CPU.
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "ERROR: CPU only supports %s or %s or %s or %s; none of which has been detected!\n",
      gDdrTypeStr[MRC_DDR_TYPE_LPDDR4], gDdrTypeStr[MRC_DDR_TYPE_LPDDR5], gDdrTypeStr[MRC_DDR_TYPE_DDR4], gDdrTypeStr[MRC_DDR_TYPE_DDR5]);
    return mrcDimmNotSupport;
  }

  // Determine if the internal graphics engine is supported.
  if ((Capid0A.Bits.IGD == 0) && (Deven.Bits.D2EN > 0)) {
    Outputs->GraphicsStolenSize = Inputs->GraphicsStolenSize;
    Outputs->GraphicsGttSize    = Inputs->GraphicsGttSize;
    Outputs->GtPsmiRegionSize   = Inputs->GtPsmiRegionSize;
  } else {
    Outputs->GraphicsStolenSize = 0;
    Outputs->GraphicsGttSize    = 0;
    Outputs->GtPsmiRegionSize   = 0;
  }

  //
  // PSMI Handler Size to allocate the memory
  //
  Outputs->PsmiHandlerSize = Inputs->PsmiHandlerSize;

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "Memory allocated for IGD = %uMB and for GTT = %uMB.\n",
    Outputs->GraphicsStolenSize,
    Outputs->GraphicsGttSize
    );

  // Determine the maximum size of memory per channel, based on CAPID
  switch (Capid0A.Bits.DDRSZ) {
    case tcs64GB:
      Outputs->MrcTotalChannelLimit = (64 * 1024);
      break;

    case tcs8GB:
      Outputs->MrcTotalChannelLimit = (8 * 1024);
      break;

    case tcs4GB:
      Outputs->MrcTotalChannelLimit = (4 * 1024);
      break;

    case tcs2GB:
    default:
      Outputs->MrcTotalChannelLimit = (2 * 1024);
      break;
  }

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "Maximum size of memory allowed on a channel = %uMB.\n",
    Outputs->MrcTotalChannelLimit
    );

  // Determine how many dimms are supported per channel on this memory controller,
  // based on fuse and how many channels have DIMMs installed.
  DimmCount     = (Capid0A.Bits.DDPCD == 0) ? MAX_DIMMS_IN_CHANNEL : 1;

  if ((Inputs->Force1Dpc == TRUE) || UlxUlt) {
    // Only 1DPC is supported on ULX / ULT platform
    DimmCount = 1;
  }

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "Number of channels supported = %u\nNumber of DIMMs per channel supported = %u\n",
    MaxChannel,
    DimmCount
    );

  // Determine the minimum NMode supported on this memory controller.
  NModeMinimum = (Capid0A.Bits.D1NM == 0) ? 1 : 2;

  if (Ddr5) {
    NModeMinimum = 2; // Use 2N for DDR5
  }

  // Determine the ECC capability of the memory controller.
  IgnoreNonEccDimm = (Capid0A.Bits.FDEE == 0) ? FALSE : TRUE;

  // Set EccSupport flag to TRUE if we must NOT ignore ECC DIMMs
  if (IgnoreNonEccDimm == TRUE) {
    Outputs->EccSupport = TRUE;
    EccSupport = TRUE; // FDEE has presedence over ECCDIS
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ALL DIMMs MUST be ECC capable\n");
  } else {
    EccSupport = ((Capid0A.Bits.ECCDIS > 0) || (Outputs->EccSupport == FALSE)) ? FALSE : TRUE;
  }

  // Now copy ECC and NMode information to the channel and DIMM results.
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      ControllerOut = &Outputs->Controller[Controller];
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          ChannelOut = &ControllerOut->Channel[Channel];
          Cmd2N = (NModeMinimum == 2);
          if ((Profile == STD_PROFILE) && !Outputs->Gear2) {
            // Okay to use Outputs->Frequency here as we don't have frequency switching in DDR4,
            // and LPDDR4 doesn't support stretched commands.
            if ((Ddr4 && (Outputs->Frequency > f2133)) ||
                (Ddr4 && !UlxUlt && (Outputs->Frequency >= f1333))
               ) {
              Cmd2N = TRUE;
            }
          }
          if (Cmd2N) {
            ChannelOut->Timing[Profile].NMode = MAX (2, ChannelOut->Timing[Profile].NMode);
          }
          for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
            DimmOut = &ChannelOut->Dimm[Dimm];
            if (DimmOut->Status == DIMM_PRESENT) {
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_NOTE,
                "  %s %u/%u/%u NMode = %u\n",
                CcdString,
                Controller,
                Channel,
                Dimm,
                ChannelOut->Timing[Profile].NMode
              );
              if (EccSupport == TRUE) {
                if (DimmOut->EccSupport == FALSE) {
                  if (IgnoreNonEccDimm == TRUE) {
                    DimmOut->Status = DIMM_DISABLED;
                    MRC_DEBUG_MSG (
                      Debug,
                      MSG_LEVEL_NOTE,
                      "  %s %u/%u/%u Disabling non-ECC capable DIMM\n",
                      CcdString,
                      Controller,
                      Channel,
                      Dimm
                    );
                  } else {
                    Outputs->EccSupport = FALSE; // Final ECCSupport must be disabled if one DIMM is NOT capable
                  }
                }
              } else {
                DimmOut->EccSupport = FALSE;
                Outputs->EccSupport = FALSE; // Final ECCSupport must be disabled if ECCDIS is set
              }
            }
          }
        }
      }
    }
  }

  // Make sure we have the same NMode limit on both channels
  Cmd2N = FALSE;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (ControllerOut->Channel[Channel].Timing[Profile].NMode == 2) {
        Cmd2N = TRUE;
        break;
      }
    }
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      ControllerOut->Channel[Channel].Timing[Profile].NMode = (Cmd2N) ? 2 : 1;
    }
  }

  // Update Final SdramCount
  // SdramCount = Number of bytes lanes per channel
  // DDR4  x64 bit bus / x8 bits per bytelane = 8 bytelanes
  // DDR5  x32 bit bus / x8 bits per bytelane = 4 bytelanes
  // LPDDR x16 bit bus / x8 bits per bytelane = 2 bytelanes
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      ControllerOut = &Outputs->Controller[Controller];
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        ChannelOut = &ControllerOut->Channel[Channel];
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
            DimmOut = &ChannelOut->Dimm[Dimm];
            if (DimmOut->Status == DIMM_PRESENT) {
              Outputs->SdramCount = (DimmOut->PrimaryBusWidth / 8);
              break;
            }
          }
        }
      }
    }
  }
  if (Outputs->EccSupport == TRUE) {
    Outputs->SdramCount++;
  }

  // Determine the size of memory in each channel.
  // Also determine the channel with the largest amount.
  Max = ChannelNum = Outputs->MemoryMapData.TotalPhysicalMemorySize = 0;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      ControllerOut = &Outputs->Controller[Controller];
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        ChannelOut = &ControllerOut->Channel[Channel];
        Size        = 0;
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
            DimmOut = &ChannelOut->Dimm[Dimm];
            if (DimmOut->Status == DIMM_PRESENT) {
              Size += DimmOut->DimmCapacity;
            }
          }

          ChannelOut->Capacity = Size;
          if (Size > Max) {
            Max         = Size;
            ChannelNum  = Channel;
            ChDimmCount = ChannelOut->DimmCount;
          } else if ((Size == Max) && (DimmCount == 1)) {
            // Choose channel with least amount of DIMMs if 2DPC is disabled
            if (ChannelOut->DimmCount < ChDimmCount) {
              ChDimmCount = ChannelOut->DimmCount;
              ChannelNum  = Channel;
            }
          }
        } // ChannelExist

        Outputs->MemoryMapData.TotalPhysicalMemorySize += ChannelOut->Capacity;
        // Program ValidByteMask here
        ChannelOut->ValidByteMask = (1 << Outputs->SdramCount) - 1;
      }
    }
  }

  if (DimmCount == 1) {
    // Determine which DIMMs are supported on this memory controller.
    // If fused for one DIMM per channel, we pick the DIMM in a channel with the most memory.
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      if (MrcControllerExist (MrcData, Controller)) {
        ControllerOut = &Outputs->Controller[Controller];
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          ChannelOut = &ControllerOut->Channel[Channel];
          Max                   = Size = DimmNum = 0;
          if (ChannelOut->Status == CHANNEL_PRESENT) {
            for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
              DimmOut = &ChannelOut->Dimm[Dimm];
              if (DimmOut->Status == DIMM_PRESENT) {
                Size = DimmOut->DimmCapacity;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "C%uD%uDimmCapacity = 0x%x\n", Channel, Dimm, DimmOut->DimmCapacity);
                if (Size > Max) {
                  Max     = Size;
                  DimmNum = Dimm;
                }
              }
            }

            for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
              DimmOut = &ChannelOut->Dimm[Dimm];
              if ((DimmOut->Status == DIMM_PRESENT) && (Dimm != DimmNum)) {
                DimmOut->Status = DIMM_DISABLED;
              }
            }

            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              "Controller configured to one DIMM per channel, we've selected channel %u, Dimm %u.\n",
              Channel,
              DimmNum
              );
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ValidRankBitMask = 0x%x\n", ChannelOut->ValidRankBitMask);
          }
        }
      }
    }
  }

  Inputs->Ibecc = (Inputs->Ibecc == TRUE) && (Capid0E.Bits.IBECC_DIS == 0);

  return mrcSuccess;
}

/**
  This function reads the CAPID0 register and sets the following values
  according to memory controller's capability and user input:
    Outputs->RefClk
    Outputs->FreqMax
    Outputs->Capable100
    Outputs->MemoryClockMax

  @param[in, out] MrcData - All the MRC global data.

  @retval Always returns mrcSuccess.
**/
MrcStatus
MrcMcCapabilityPreSpd (
  IN OUT MrcParameters *const MrcData
  )
{
  const MrcInput                *Inputs;
  const MRC_FUNCTION            *MrcCall;
  MrcDebug                      *Debug;
  MrcOutput                     *Outputs;
  MrcSaveData                   *SaveData;
  MrcIntOutput                  *IntOutputs;
  MrcControllerOut              *ControllerOut;
  MrcChannelOut                 *ChannelOut;
  MrcDimmOut                    *DimmOut;
  MrcFrequency                  FreqMax;
  MrcFrequency                  FreqMaxAtGear1;
  MrcRefClkSelect               RefClk;
  MrcClockRatio                 MaxRatio;
  MrcClockRatio                 MaxRatioAtGear1;
  MrcDdrType                    DdrType;
  BOOLEAN                       RefClk100En;
  UINT32                        MaxFreqCap;
  UINT32                        MaxFreqCapAtGear1;
  UINT32                        Offset;
  UINT32                        Divisor;
  UINT32                        MaxFreq;
  UINT32                        MaxFreqAtGear1;
  UINT32                        FrequencyMultiplier;
  BOOLEAN                       Lpddr;
  BOOLEAN                       Lpddr5;
  BOOLEAN                       Ddr4;
  BOOLEAN                       Ddr5;
  BOOLEAN                       OverclockCapable;
  BOOLEAN                       MrcSafe;
  BOOLEAN                       DtHalo;
  CAPID0_A_0_0_0_PCI_STRUCT     Capid0A;
  CAPID0_B_0_0_0_PCI_STRUCT     Capid0B;
  CAPID0_C_0_0_0_PCI_STRUCT     Capid0C;
  CAPID0_E_0_0_0_PCI_STRUCT     Capid0E;
  CAPID0_F_0_0_0_PCI_STRUCT     Capid0F;
  BOOLEAN                       UlxUlt;
  UINT32                        Controller;
  UINT32                        Channel;
  UINT32                        Dimm;
  UINT8                         PopulatedDimmMask;
  UINT8                         ExpectedDimmMask;
  BOOLEAN                       OneDpcPopulated;
  BOOLEAN                       Invalid1DpcPopulation;
  BOOLEAN                       Q0;
  BOOLEAN                       K0;
  UINT8                         DDR5DimmPopInfo;

  Inputs    = &MrcData->Inputs;
  MrcCall   = Inputs->Call.Func;
  Outputs   = &MrcData->Outputs;
  SaveData  = &MrcData->Save.Data;
  Debug     = &Outputs->Debug;
  IntOutputs = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));

  DdrType = Outputs->DdrType;
  Lpddr   = Outputs->Lpddr;
  Lpddr5  = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4    = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5    = (DdrType == MRC_DDR_TYPE_DDR5);
  MrcSafe = (Inputs->MrcSafeConfig == 1);
  DtHalo  = Inputs->DtHalo;
  UlxUlt  = (Inputs->UlxUlt);
  Q0      = Inputs->Q0;
  K0      = Inputs->K0;

  // Obtain the capabilities of the memory controller.
  Offset       = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_A_0_0_0_PCI_REG);
  Capid0A.Data = MrcCall->MrcMmioRead32 (Offset);

  Offset       = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_B_0_0_0_PCI_REG);
  Capid0B.Data = MrcCall->MrcMmioRead32 (Offset);

  Offset       = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_C_0_0_0_PCI_REG);
  Capid0C.Data = MrcCall->MrcMmioRead32 (Offset);

  Offset       = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_E_0_0_0_PCI_REG);
  Capid0E.Data = MrcCall->MrcMmioRead32 (Offset);

  Offset       = CAPID0_F_0_0_0_PCI_REG;
  Capid0F.Data = MrcReadCR (MrcData, Offset);

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "CAPID0_A: %08Xh\nCAPID0_B: %08Xh\nCAPID0_C: %08Xh\nCAPID0_E: %08Xh\nCAPID0_F: %08Xh\n",
    Capid0A.Data,
    Capid0B.Data,
    Capid0C.Data,
    Capid0E.Data,
    Capid0F.Data
    );

  // Determine the maximum memory frequency supported and the memory reference clock.
  switch (DdrType) {
    case MRC_DDR_TYPE_LPDDR5:
      MaxFreqCap = Capid0E.Bits.MAX_DATA_RATE_LPDDR5;
      break;

    case MRC_DDR_TYPE_LPDDR4:
      MaxFreqCap = Capid0C.Bits.MAX_DATA_RATE_LPDDR4;
      break;

    case MRC_DDR_TYPE_DDR5:
      MaxFreqCap = Capid0E.Bits.MAX_DATA_RATE_DDR5;
      break;

    default:
    case MRC_DDR_TYPE_DDR4:
      MaxFreqCap = Capid0C.Bits.MAX_DATA_RATE_DDR4;
      break;
  }

  FrequencyMultiplier = (Lpddr5 || Ddr5) ? 400000 : 266666;

  MaxFreq = MaxFreqCap * FrequencyMultiplier;
  MaxFreq = UDIVIDEROUND (MaxFreq, 1000);

  MaxFreqCapAtGear1 = Capid0F.Bits.Max_Data_Rate_At_GEAR1;  // Units of 33.3 MHz
  MaxFreqAtGear1 = MaxFreqCapAtGear1 * 33333;
  MaxFreqAtGear1 = UDIVIDEROUND (MaxFreqAtGear1, 1000);

#ifdef MRC_DEBUG_PRINT
  // Print DMFC decoding. 0 means Unlimited
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DMFC: %u\nMax_Data_Rate_At_GEAR1: %u\n", MaxFreq, MaxFreqAtGear1);
#endif

  OverclockCapable = (Capid0A.Bits.DDR_OVERCLOCK > 0) ? TRUE : FALSE;
  RefClk100En      = (BOOLEAN) Capid0B.Bits.PLL_REF100_CFG;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_OVERCLOCK: %d, PLL_REF100_CFG: %d\n", OverclockCapable, RefClk100En);

  // Determine correct RefClk
  RefClk  = Inputs->RefClk;
  if (!RefClk100En) {
    RefClk = MRC_REF_CLOCK_133;
    if (Lpddr5 || Ddr5) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "LPDDR5/DDR5 need RefClk100 but it is disabled\n");
      return mrcFail;
    }
  } else {
    Outputs->Capable100 = TRUE;
    if (Lpddr5 || Ddr5) {
      // Force to RefClk100
      RefClk = MRC_REF_CLOCK_100;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RefClk set to 100 due to LPDDR5/DDR5\n");
    } else if (OverclockCapable && (Inputs->MemoryProfile == STD_PROFILE)) {
      // If we are using standard memory profile, DIMMS should run at RefClk 133.
      RefClk = MRC_REF_CLOCK_133;
    }
    // Otherwise, we keep what was requested via the input parameter
  }
  Outputs->RefClk = RefClk;

  // Now use the RefClk to determine the Max Ratio/Freq
  // Start by using FreqMax to determine what is the max frequency is based on the RefClk we found.
  // If OverclockCapable is set, this will be our limit.
  if (OverclockCapable) {
    FreqMax = (RefClk == MRC_REF_CLOCK_100) ? MAX_FREQ_OC_100 : MAX_FREQ_OC_133;
    FreqMaxAtGear1 = MAX_FREQ_OC_100 ;
  } else {
    FreqMax = MaxFreq;
    FreqMaxAtGear1 = MaxFreqAtGear1;
  }
  Outputs->FreqMax = ((Inputs->FreqMax > fNoInit) && (Inputs->FreqMax < fInvalid)) ? Inputs->FreqMax : FreqMax;

  // Do not limit FreqMax in Auto case if we are in Overclocking mode
  if ((MrcSafe || Lpddr) && (Inputs->FreqMax == fNoInit) && (Inputs->MemoryProfile == STD_PROFILE)) {
    switch (Outputs->DdrType) {
      case MRC_DDR_TYPE_DDR4:
        if (Inputs->SimicsFlag == 1) {
          Outputs->FreqMax = f1333;
        } else {
          Outputs->FreqMax = (Inputs->A0) ? f2667 : f3200;
        }
        break;

      case MRC_DDR_TYPE_LPDDR4:
        if (Inputs->SimicsFlag == 1) {
          Outputs->FreqMax = f1600;
        } else {
          Outputs->FreqMax = f4267;
        }
        break;

      case MRC_DDR_TYPE_LPDDR5:
        // Limit the Type3 board max freq to 1R-5200, 2R-4800.
        if (Inputs->BoardType == btUlxUltType3) {
          if (Q0 || K0) {
            Outputs->FreqMax = (Outputs->Any2Ranks) ? f4400 : f4800;
          } else {
            Outputs->FreqMax = f4800;
          }
        } else {
          Outputs->FreqMax = f5200;
        }
        break;

      default:
      case MRC_DDR_TYPE_DDR5:
        // Limit the 2DPC board max freq as 4400.
        // If there is a 2DPC population in the 2DPC board, the max freq will be further limited below to 4000, otherwise, it will keep as 4400.
        // 1DPC population may also be reduced from 4400 if a wrong slot is populated.
        Outputs->FreqMax = (Inputs->BoardType == btDesktop1Dpc) ? f4800 : f4400;
        // Limit the DDR5 DT board max freq according to the following table:
        //  BoardType        1R      2R
        //  =========       ====    ====
        //  1DPC            4800    4800
        //  2DPC (1DIMM)    4400    4400
        //  2DPC (2DIMM)    4000    3600
        if ((Inputs->BoardType == btDesktop2DpcDaisyChain)  ||
            (Inputs->BoardType == btDesktop2DpcTeeTopology) ||
            (Inputs->BoardType == btDesktop2DpcTeeTopologyAsymmetrical)) {
          // 2DPC board
          if (Outputs->Any2Dpc) {
            if (Outputs->Any2Ranks) {
              Outputs->FreqMax = f3600;  // 2DPC 2R2R, 2R1R, 1R2R
            } else {
              Outputs->FreqMax = f4000;  // 2DPC 1R1R
            }
          } else {
            Outputs->FreqMax = f4400;    // 2DPC 1R0R or 2R0R
          }
        } else { // 1DPC board
          Outputs->FreqMax = f4800;
        }

        if (Inputs->Ddr5DoubleSize2Dpc) {
          Outputs->FreqMax = f3600;
        }

        if (UlxUlt) {
          Outputs->FreqMax = f4800;
        }

        break;
    } // DdrType

    if (!Lpddr && ((Inputs->BoardType == btDesktop2DpcDaisyChain) || (Inputs->BoardType == btDesktop2DpcTeeTopologyAsymmetrical))) {
      // These 2DPC boards have DIMM population rules that MRC must enforce.
      // If there is a 1DPC populated on any channel, and it doesn't match FirstDimmBitMask - MRC will reduce speed by 1 bin.
      Invalid1DpcPopulation = FALSE;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        ControllerOut = &Outputs->Controller[Controller];
        PopulatedDimmMask = 0;
        OneDpcPopulated = FALSE;
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          ChannelOut  = &ControllerOut->Channel[Channel];
          if (ChannelOut->DimmCount == 1) { // 1DPC populated on a 2DPC board
            OneDpcPopulated = TRUE;
            for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
              DimmOut = &ChannelOut->Dimm[Dimm];
              if (DimmOut->Status == DIMM_PRESENT) {
                PopulatedDimmMask |= (1 << Dimm);  // 2-bit mask, for this MC only
              }
            }
          }
        } // Channel
        if (OneDpcPopulated) {  // 1DPC populated on this MC, check against population rules mask
          if (Outputs->EccSupport == FALSE) {
            // The logis is that if one bit in ExpectedDimmMask is 1, the according bit in PopulatedDimmMask can be 1 or 0.
            // The logis is that if one bit in ExpectedDimmMask is 0, the according bit in PopulatedDimmMask needs to be 0.
            ExpectedDimmMask = (Inputs->FirstDimmBitMask >> (Controller * MAX_DIMMS_IN_CHANNEL)) & 3;
            if (((PopulatedDimmMask & MRC_BIT0) > (ExpectedDimmMask & MRC_BIT0)) ||
                (((PopulatedDimmMask & MRC_BIT1) > (ExpectedDimmMask & MRC_BIT1)))) {
              Invalid1DpcPopulation = TRUE;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,
                             "MC%u: 1DPC on 2DPC board, ExpectedDimmMask=%u, PopulatedDimmMask=%u, reduce speed\n", Controller, ExpectedDimmMask,
                             PopulatedDimmMask);
            }
          } else {
            // The logis is that if one bit in ExpectedDimmMask is 1, the according bit in PopulatedDimmMask can be 1 or 0.
            // The logis is that if one bit in ExpectedDimmMask is 0, the according bit in PopulatedDimmMask needs to be 0.
            ExpectedDimmMask = (Inputs->FirstDimmBitMaskEcc >> (Controller * MAX_DIMMS_IN_CHANNEL)) & 3;
            if (((PopulatedDimmMask & MRC_BIT0) > (ExpectedDimmMask & MRC_BIT0)) ||
                (((PopulatedDimmMask & MRC_BIT1) > (ExpectedDimmMask & MRC_BIT1)))) {
              Invalid1DpcPopulation = TRUE;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,
                             "MC%u: 1DPC on 2DPC board, ExpectedDimmMask=%u, PopulatedDimmMask=%u, reduce speed\n", Controller, ExpectedDimmMask,
                             PopulatedDimmMask);
            }
          }
        }
      } // Controller
      if (Invalid1DpcPopulation) {
        if (Inputs->FreqLimitMixedConfig == 0) { // Auto
          Outputs->FreqMax = Ddr4 ? f2933 : f4000;
        } else {
          Outputs->FreqMax = Inputs->FreqLimitMixedConfig;
        }
        // If the final resolved speed is less than POR 1DPC speed (meaning it was reduced), set MemorySpeedReducedWrongDimmSlot = TRUE
        if (Outputs->FreqMax < (UINT32) (Ddr4 ? f3200 : f4400)) {
          SaveData->MemorySpeedReducedWrongDimmSlot = TRUE;
        }
      }
    } // 2DPC board with population rules
  }

  if (!OverclockCapable) {
    // Use the ratio limit from CAPID to determine the MaxFreq
    MaxRatio = (MrcClockRatio) ((MaxFreqCap == 0) ? CAPID0_C_0_0_0_PCI_MAX_DATA_RATE_DDR4_MAX : MaxFreqCap);
    MaxRatioAtGear1 = (MrcClockRatio) ((MaxFreqCapAtGear1 == 0) ? 32 : MaxFreqCapAtGear1);                  // Unlimited = 3200 (using refclk 100)
    // CAPID is using 200/266, while MC_BIOS_REQ is using 100/133, hence double the CAPID value
    if (Lpddr5 || Ddr5) {
      MaxRatio *= 4;
    } else {
      MaxRatio *= 2; //DDR4/LP4
    }

    // DDR4/LP4 CAPID is fused base off of 133 ref clk.  So first we will calculate the frequency max, then we will limit
    // it to 100 RefClk if it is selected.  This is done below because we need to know the Gear ratio as well.
    // DDR5/LP5 CAPID is fused base off of 100 ref clk ,So first we will calculate the frequency max
    // @ gear1 CAPID is fused base off of 100 ref clk, So first we will calculate the frequency max, then we will limit  according to real refClk if This is done below because we need to know the Gear ratio as well.
    FreqMax  = MrcRatioToFrequency (MrcData, MaxRatio, (Lpddr5 || Ddr5) ? MRC_REF_CLOCK_100 :MRC_REF_CLOCK_133, BCLK_DEFAULT);
    FreqMaxAtGear1  = MrcRatioToFrequency (MrcData, MaxRatioAtGear1,  MRC_REF_CLOCK_100, BCLK_DEFAULT);
  }
  // Now check if the CPU is the limiter of the frequency, and apply that limit over user/DIMM request.
  if ((FreqMax < Outputs->FreqMax) || (Outputs->FreqMax == fNoInit)) {
    Outputs->FreqMax  = FreqMax;
  }

  if ((Inputs->SaGv != MrcSaGvDisabled) && (!Inputs->DynamicMemoryBoost) && (!Inputs->RealtimeMemoryFrequency)) {
    MrcGetSagvConfig (MrcData, IntOutputs->SaGvPoint, &FreqMax, &Outputs->Gear2);
    if (DdrType == MRC_DDR_TYPE_DDR5 && (Inputs->BoardType == btDesktop2DpcDaisyChain || Inputs->BoardType == btDesktop2DpcTeeTopology || Inputs->BoardType == btDesktop2DpcTeeTopologyAsymmetrical)
        && ((Inputs->SaGvFreq[IntOutputs->SaGvPoint]) == 0)) {
        //DDR5 and 2DPC board
        if (IntOutputs->SaGvPoint == MrcSaGvPoint3) {
          if (Outputs->Any2Dpc) {
            if (Outputs->Any2Ranks) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "2DPC 2R2R\n");
              FreqMax = f3200; // Set the max freq as 3200 for SAGV point 3 if the config includes 2DPC 2R2R.
            } else {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "2DPC 1R1R\n");
              FreqMax = f3600; // Set the max freq as 3600 for SAGV point 3 if the config includes 2DPC 1R1R.
            }
          } else {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "1DPC in a 2DPC board\n");
            FreqMax = f4000; // Set the max freq as 4000 for SAGV point 3 if the config is 1DPC in a 2DPC board.
          }
        } else if (IntOutputs->SaGvPoint == MrcSaGvPoint2) {
          if ((Outputs->Any2Dpc) && (Outputs->Any2Ranks)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "2DPC 2R2R for GV point 2\n");
            FreqMax = f3200; // Set the max freq as 3200 for SAGV point 2 if the config includes 2DPC 2R2R.
          }
        }
    }
    Outputs->FreqMax = MIN (FreqMax, Outputs->FreqMax);
  } else {
    // Set Gear by the input parameter if not auto, otherwise default to G1 up to 2133 MHz.
    if (Inputs->GearRatio) {
      Outputs->Gear2 = (Inputs->GearRatio == 2) ? 1 : 0;
      Outputs->Gear4 = (Inputs->GearRatio == 4) ? 1 : 0;
    } else {
      Outputs->Gear2 = (Outputs->FreqMax >= 2133) ? 1 : 0;
      if (Ddr4 && DtHalo) {
        if (!Inputs->A0 && (Outputs->FreqMax <= 3200)) {
          Outputs->Gear2 = 0; // On DDR4 use Gear1 up to 3200
        }
      }
      if (Ddr5) {
        Outputs->Gear2 = 1; // No Gear1 in DDR5
      }
    }
  }
  Outputs->HighGear = (Outputs->Gear4) ? 4 : ((Outputs->Gear2) ? 2 : 1);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "HighGear: %u\n", Outputs->HighGear);

  // Apply the CAPID limitation for Gear1
  if (!Outputs->Gear2 && !Outputs->Gear4) {
    if (RefClk == MRC_REF_CLOCK_133) {
      // FreqMaxAtGear1 at this point is still in terms of the refClk100.
      // Convert to to 133 by calculating the ratio in refClk133 and then calculate the correct frequency
      MaxRatioAtGear1 = (MrcClockRatio) (FreqMaxAtGear1 / 133);
      FreqMaxAtGear1  = MrcRatioToFrequency (MrcData, MaxRatioAtGear1,  MRC_REF_CLOCK_133, BCLK_DEFAULT);
    }
#ifdef MRC_DEBUG_PRINT
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "FreqMaxAtGear1: %u\n", FreqMaxAtGear1);
#endif
    Outputs->FreqMax = MIN (Outputs->FreqMax, FreqMaxAtGear1);
  }

  if (Outputs->IsMixedMemory) {
    MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info Enter: Outputs->FreqMax=%u \n", Outputs->FreqMax);
    if (Ddr5) {
      DDR5DimmPopInfo = GetDDR5DimmPopInfo(MrcData);
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info: DDR5DimmPopInfo=%u \n", DDR5DimmPopInfo);
      if ((Inputs->FreqLimitMixedConfig_1R1R_8GB || Inputs->FreqLimitMixedConfig_1R1R_16GB || Inputs->FreqLimitMixedConfig_1R1R_8GB_16GB || Inputs->FreqLimitMixedConfig_2R2R)) {
        switch (DDR5DimmPopInfo) {
          case DDR5_DIMM_MIXED_CONFIG_1R1R_8GB:
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: FreqLimitMixedConfig_1R1R_8GB = %u\n", Inputs->FreqLimitMixedConfig_1R1R_8GB);
            Outputs->FreqMax = ( Outputs->FreqMax > Inputs->FreqLimitMixedConfig_1R1R_8GB &&  Inputs->FreqLimitMixedConfig_1R1R_8GB ) ? Inputs->FreqLimitMixedConfig_1R1R_8GB : Outputs->FreqMax;
            break;
          case DDR5_DIMM_MIXED_CONFIG_1R1R_16GB:
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: FreqLimitMixedConfig_1R1R_16GB = %u\n", Inputs->FreqLimitMixedConfig_1R1R_16GB);
            Outputs->FreqMax = ( Outputs->FreqMax > Inputs->FreqLimitMixedConfig_1R1R_16GB && Inputs->FreqLimitMixedConfig_1R1R_16GB ) ? Inputs->FreqLimitMixedConfig_1R1R_16GB : Outputs->FreqMax;
            break;
          case DDR5_DIMM_MIXED_CONFIG_1R1R_8GB_16GB:
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: FreqLimitMixedConfig_1R1R_8GB_16GB = %u\n", Inputs->FreqLimitMixedConfig_1R1R_8GB_16GB);
            Outputs->FreqMax = ( Outputs->FreqMax > Inputs->FreqLimitMixedConfig_1R1R_8GB_16GB && Inputs->FreqLimitMixedConfig_1R1R_8GB_16GB) ? Inputs->FreqLimitMixedConfig_1R1R_8GB_16GB : Outputs->FreqMax;
            break;
          case DDR5_DIMM_MIXED_CONFIG_1R1R_OTHERS:
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: This 1R1R others config!!! Limit Outputs->FreqMax = f2000\n");
            Outputs->FreqMax = f2000;
            break;
          case DDR5_DIMM_MIXED_CONFIG_2R2R:
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: FreqLimitMixedConfig_2R2R = %u\n", Inputs->FreqLimitMixedConfig_2R2R);
            Outputs->FreqMax = ( Outputs->FreqMax > Inputs->FreqLimitMixedConfig_2R2R && Inputs->FreqLimitMixedConfig_2R2R ) ? Inputs->FreqLimitMixedConfig_2R2R : Outputs->FreqMax;
            break;
          case DDR5_DIMM_MIXED_CONFIG_1R2R:
          case DDR5_DIMM_MIXED_CONFIG_2R1R:
          case DDR5_DIMM_MIXED_CONFIG_2R_OTHERS:
          default:
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: This 1R2R, 2R1R or Other 2R config!!! Limit Outputs->FreqMax = f2000 !!!\n");
            Outputs->FreqMax = f2000;
            break;
        }
      } else {
          if ((Outputs->Any2Dpc) && (Outputs->Any2Ranks) && (Outputs->FreqMax > f2000)) {
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: Mixed DIMM config including 2R and set Outputs->FreqMax = f2000\n");
            Outputs->FreqMax = f2000; // Set the max freq as 3200 for mixed dimm configuration including 2R.
          } else {
            //Auto mode  POR frequency
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR5: Automode \n");
          }
      }
    }

    if (Ddr4) {
      if (Inputs->FreqLimitMixedConfig > 0) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR4: limit max freq to FreqLimitMixedConfig = %d\n", Inputs->FreqLimitMixedConfig);
        Outputs->FreqMax = Outputs->FreqMax > Inputs->FreqLimitMixedConfig ? Inputs->FreqLimitMixedConfig : Outputs->FreqMax;
      } else {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info DDR4: FreqLimitMixedConfig=Auto \n");
        Outputs->FreqMax = ( Outputs->FreqMax > f2933 ) ? f2933 : Outputs->FreqMax;
      }
    }
    MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mixed DIMM Info Exit: Outputs->FreqMax=%u \n", Outputs->FreqMax);
  }

  if (RefClk == MRC_REF_CLOCK_100) {
    // FreqMax at this point is still in terms of the 133.  Convert to to 100
    // If we have a gear ratio enabled, the frequency max has to be a ratio of the Gear ratio.
    // I.E.  If Gear2 FreqMax must be an even ratio of 200/266
    Divisor = 100 * Outputs->HighGear;
    Outputs->FreqMax /= Divisor;
    Outputs->FreqMax *= Divisor;
  }
  Outputs->MemoryClockMax = ConvertFreq2Clock (MrcData, Outputs->FreqMax);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Outputs->MemoryClockMax: %u, Outputs->FreqMax : %u, Gear%d\n", Outputs->MemoryClockMax, Outputs->FreqMax, Outputs->HighGear);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "The maximum memory frequency allowed is %u, tCK=%ufs\n", Outputs->FreqMax, Outputs->MemoryClockMax);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%uMHz reference clock is selected\n", (Outputs->RefClk == MRC_REF_CLOCK_133) ? 133 : 100);

  return mrcSuccess;
}

/**
  This function reads the input data structure and sets the appropriate overrides in the output structure.

  @param[in, out] MrcData - All the MRC global data.

  @retval Returns mrcSuccess if the timing overrides have been conpleted.
**/
MrcStatus
MrcSetOverrides (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcInput      *Inputs;
  MrcOutput     *Outputs;
  MrcSaveData   *SaveOutputs;
  MrcDebug      *Debug;
  const UINT16  *RcompTarget;
  MrcDdrType    DdrType;
  MrcFrequency  DdrFreq;
  MrcStatus     Status;
  MrcFrequency  LowFreq;
  UINT16        ReqRdOdt;
  UINT16        ValidRdOdt;
  UINT8         Index;
  UINT8         NewMode;
  UINT8         RankMask;
  UINT8         NumRanks;
  BOOLEAN       lDramDqOdtEn;
  BOOLEAN       UlxUlt;
  MrcFrequency  DdrFrequency;
  BOOLEAN       Lpddr;
  BOOLEAN       Lpddr4;
  BOOLEAN       Lpddr5;
  BOOLEAN       Ddr4;
  BOOLEAN       Ddr5;

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  SaveOutputs = &MrcData->Save.Data;
  Debug       = &Outputs->Debug;
  DdrType     = Outputs->DdrType;
  UlxUlt      = (Inputs->UlxUlt);
  RankMask    = Outputs->ValidRankMask;
  Status      = mrcSuccess;
  RcompTarget = NULL;

  Outputs->EccSupport      = Inputs->EccSupport != 0;
  Outputs->VoltageDone     = FALSE;
  Lpddr   = Outputs->Lpddr;
  Lpddr4  = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5  = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4    = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5    = (DdrType == MRC_DDR_TYPE_DDR5);
  DdrFrequency = Outputs->Frequency;
  if (Lpddr5) {
    // Disabled LPFrequency Switch for LP5.
    Inputs->LpFreqSwitch = FALSE;
  }

  Inputs->WeaklockEn = 0;

// Assign Vccdq voltage to match DRAM Vddq.
  if (Ddr4) {
    Outputs->VccddqVoltage = Outputs->Vdd2Mv;
  } else if (Ddr5) {
    Outputs->VccddqVoltage = Outputs->Vdd2Mv;
  } else if (Lpddr4) {
    Outputs->VccddqVoltage = VDD_0_90;
  } else { //Lpddr5
    if ((DdrFrequency <= 2400) && !(Inputs->J0)) {
      Outputs->VccddqVoltage = VDD_0_80;
    } else if (DdrFrequency > 5300) {
      NumRanks = MrcCountBitsEqOne(RankMask);
      if (NumRanks > 1) {
        Outputs->VccddqVoltage = VDD_1_00;
      } else {
        Outputs->VccddqVoltage = VDD_0_80;
      }
    } else {
      Outputs->VccddqVoltage = VDD_0_80;
    }
  }
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "VccddqVoltage: %u\n", Outputs->VccddqVoltage);

  if (Inputs->DramDqOdt == MrcAuto) {
    switch (DdrType) {
      case MRC_DDR_TYPE_LPDDR4:
        // LPDDR4 we do frequency switching.  If we have this enabled, we need to check HighFrequency.
        // Otherwise, we use Frequency
        DdrFreq = (Inputs->LpFreqSwitch) ? Outputs->HighFrequency : Outputs->Frequency;
        if (DdrFreq == f1600) {
          lDramDqOdtEn = FALSE;
        } else {
          lDramDqOdtEn = TRUE;
        }
        break;

      case MRC_DDR_TYPE_DDR4:
      case MRC_DDR_TYPE_LPDDR5:
      default:  // For non-supported DRAM types, enable DQ ODT
        lDramDqOdtEn = TRUE;
        break;
    }
  } else {
    lDramDqOdtEn = (Inputs->DramDqOdt == MrcEnable) ? TRUE : FALSE;
  }
  Outputs->DramDqOdtEn  = lDramDqOdtEn;
  // Configure LowFrequency
  switch (DdrType) {
    case MRC_DDR_TYPE_LPDDR5:
      LowFreq = f2200;
      break;

    case MRC_DDR_TYPE_LPDDR4:
      LowFreq = f1600;
      break;

    default:
      LowFreq = fInvalid;
      break;
  }
  if (LowFreq != fInvalid) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "LPDDR Low Frequency: %d\n", LowFreq);
  }
  Outputs->LowFrequency = LowFreq;

  // Configure RxPath
  if ((Inputs->RxMode != 0xFF) && (Inputs->RxMode < MrcRxModeMax)) {
    NewMode = Inputs->RxMode;
  } else {
    switch (DdrType) {
      case MRC_DDR_TYPE_LPDDR4:
        NewMode = MrcRxModeMatchedP;
        break;

      case MRC_DDR_TYPE_LPDDR5:
        NewMode = MrcRxModeUnmatchedP;
        break;

      case MRC_DDR_TYPE_DDR5:
        NewMode = MrcRxModeMatchedN;
        break;

      case MRC_DDR_TYPE_DDR4:
      default:
        NewMode = MrcRxModeMatchedN;
        break;
    }
  }
  Outputs->RxMode = NewMode;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RxMode: %s\n", gIoRxModeStr[NewMode]);

  // If RcompResistors are not zero, then user is overriding default termination
  if (Inputs->RcompResistor == 0) {
    Inputs->RcompResistor = 100; // All platform designs default to 100 Ohm.
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Updating Rcomp Resistors: %u\n", Inputs->RcompResistor);
  }

  // Determine RcompTargets based on CPU type and DDR Type
  switch (DdrType) {
    case MRC_DDR_TYPE_DDR4:
      RcompTarget = (UlxUlt) ? RcompTargetUDdr4 : (Outputs->IsDdr4Pn641A2) ? RcompTargetSDdr4Sn641A2 : RcompTargetSDdr4;
      if (Inputs->MemoryProfile != STD_PROFILE) {
          if (Outputs->Frequency >= f4000) {
            RcompTarget = RcompTargetDdr4_ExtremeOC;
          } else if (Outputs->Frequency >= f3200) {
            RcompTarget = RcompTargetDdr4_OC;
          }
      }
      if (Inputs->LoopBackTest) {
        RcompTarget = RcompTargetDdr4Lb;
      }
      break;

    case MRC_DDR_TYPE_LPDDR5:
      RcompTarget = RcompTargetLpddr5;
      if (Outputs->VccddqVoltage <= VDD_0_60) {
        RcompTarget = RcompTargetLpddr5LowVddq;
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, " Using RcompTargetLp5LowVddq \n");
      }
      break;

    case MRC_DDR_TYPE_LPDDR4:
      RcompTarget = (Outputs->Lp4x) ? RcompTargetLpddr4x : RcompTargetLpddr4;
      if (Inputs->LoopBackTest) {
        RcompTarget = RcompTargetLpddr4Lb;
      }
      break;

    case MRC_DDR_TYPE_DDR5:
      RcompTarget = Outputs->Any2Dpc ? RcompTargetSDdr5TwoDpc : RcompTargetSDdr5;
      break;

    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %s: %d\n", gErrString, gUnsupportedTechnology, DdrType);
      Status = mrcFail;
      break;
  }
#ifdef MRC_DEBUG_PRINT
  if ((Inputs->RcompTarget[0] == 0) || (Inputs->RcompTarget[1] == 0) || (Inputs->RcompTarget[2] == 0) ||
      (Inputs->RcompTarget[3] == 0) || (Inputs->RcompTarget[4] == 0)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Updating Rcomp Targets:\n");
  }
#endif // MRC_DEBUG_PRINT
  for (Index = 0; Index < MAX_RCOMP_TARGETS; Index++) {
    if ((Inputs->RcompTarget[Index] == 0) && (RcompTarget != NULL)) {
      Outputs->RcompTarget[Index] = RcompTarget[Index];
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " RcompTarget[%s]: %u\n", GlobalCompOffsetStr[Index], Outputs->RcompTarget[Index]);
    } else {
      Outputs->RcompTarget[Index] = Inputs->RcompTarget[Index];
    }
  }

  // Determine the IO ODT Termination mode:
  //  Technology  DT/HALO(2 DPC)  DT/HALO(1 DPC)  ULX/ULT
  //-----------------------------------------------------
  //  LP5           VSS             VSS             VSS
  //  Lp4           VSS             VSS             VSS
  //  Ddr4          VDDq            VDDq            VTT
  if (Outputs->OdtMode == MrcOdtModeDefault) {
    switch (DdrType) {
      case MRC_DDR_TYPE_DDR4:
      case MRC_DDR_TYPE_DDR5:
        if (UlxUlt) {
          Outputs->OdtMode = MrcOdtModeVtt;
        } else {
          // DT/HALO
          Outputs->OdtMode = MrcOdtModeVddq;
        }
        if (Inputs->SafeMode) {
          Outputs->OdtMode = MrcOdtModeVddq;
        }
        break;

      case MRC_DDR_TYPE_LPDDR4:
      case MRC_DDR_TYPE_LPDDR5:
        Outputs->OdtMode = MrcOdtModeVss;
        break;

      default:
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %s: %d\n", gErrString, gUnsupportedTechnology, DdrType);
        Status = mrcFail;
        break;
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDRIO ODT Mode: %s\n", gIoOdtModeStr[Outputs->OdtMode]);

  // If we are LPDDR4, we need to check that the request CPU ODT matches one of the MR values for SOC_ODT.
  ReqRdOdt  = Outputs->RcompTarget[RdOdt];
  ValidRdOdt = MrcCheckForSocOdtEnc (MrcData, ReqRdOdt);
  if (ValidRdOdt != ReqRdOdt) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "RdOdt target of %d is not supported by the memory.  Updated to %d.  Please Update RcompTarget[RdOdt] to a correct value\n", ReqRdOdt, ValidRdOdt);
    Outputs->RcompTarget[RdOdt] = ValidRdOdt;
  }


  // If we have ECT disabled, set the ECT done flag so Reset Flows will behave normally for DDR5 and LPDDR systems.
  if ((Inputs->TrainingEnables.ECT == 0) || (Inputs->SimicsFlag == 1)) {
    Outputs->EctDone = TRUE;
    Inputs->LpDqsOscEn = FALSE;
  }

  SaveOutputs->Ddr5HpMode         = 0;
  SaveOutputs->GlobalVsshiBypass  = Lpddr5;
  if (Inputs->A0 || Inputs->B0 || Inputs->J0 || Inputs->Q0 || Inputs->G0) {
    SaveOutputs->Ddr5HpMode        = Ddr5;
    SaveOutputs->GlobalVsshiBypass = (SaveOutputs->GlobalVsshiBypass || Ddr5);
  }
  SaveOutputs->EnNBiasBoost       = Ddr5 ? !(SaveOutputs->Ddr5HpMode) : 0;
  SaveOutputs->LocalVsshiBypass   = Lpddr || SaveOutputs->Ddr5HpMode;
  SaveOutputs->EnNmosPup          = (Outputs->VccddqVoltage <= VDD_0_60) ? 1 : 0;
  SaveOutputs->TxPdnUseVcciog     = (Ddr4 || Ddr5 || SaveOutputs->EnNmosPup) && !(SaveOutputs->Ddr5HpMode);

  SaveOutputs->CpgcGlobalStart    = TRUE;  // Start all CPGC engines together

  if (Inputs->A0 || Inputs->B0 || Inputs->J0 || Inputs->Q0 || Inputs->G0) {
    // Disable Rx DFE at 4400 and below, on higher frequencies it will be enabled on channels that are populated with more than one rank
    SaveOutputs->RxDfe = (DdrFrequency >= f4800) ? TRUE : FALSE;
  } else {
    // Enable RxDFE at 3200 and above on C0/K0 and later
    SaveOutputs->RxDfe = (DdrFrequency >= f3200) ? TRUE : FALSE;
  }

  if (Inputs->LpMode == 0) {          // 0=Auto, 1=Enabled, 2=Disabled
    Inputs->LpMode = (UlxUlt)? 1 : 2;  // Enabled by default on mobile, Disabled by default on Desktop
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "LocalVsshiBypass: %u\nGlobalVsshiBypass: %u\n",  SaveOutputs->LocalVsshiBypass, SaveOutputs->GlobalVsshiBypass);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EnNmosPup: %u\nTxPdnUseVcciog: %u\nRxDfe: %u\n", SaveOutputs->EnNmosPup, SaveOutputs->TxPdnUseVcciog, SaveOutputs->RxDfe);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EnNBiasBoost: %u\nCpgcGlobalStart: %u\nLpMode: %u\n", SaveOutputs->EnNBiasBoost, SaveOutputs->CpgcGlobalStart, Inputs->LpMode);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "LpDdrDqDqsReTraining (LpDqsOscEn): %u\n", Inputs->LpDqsOscEn);

  if (Lpddr) {
    Inputs->TrainingEnables.WRDSEQT = 0;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "WRDSEQT: %u\n", Inputs->TrainingEnables.WRDSEQT);
  }
  if (UlxUlt) {
    Inputs->MsHashMask = (Lpddr) ? 0x2094 : 0x2098;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MsHashMask: %d\n", Inputs->MsHashMask);
  }

  return Status;
}

/**
  This function get the current value of the sticky scratchpad register.

  @param[in] MrcData - include all the MRC data.

  @retval The current value of the sticky scratchpad register.
**/
UINT64
MrcWmRegGet (
  IN     MrcParameters *const MrcData
  )
{
  return (MrcReadCR64 (MrcData, SSKPD_PCU_REG));
}

/**
  This function Set a newvalue of the sticky scratchpad register by set new givin Bit(s)

  @param[in] MrcData   - include all the MRC data.
  @param[in] SskpdBits - Bit(s) Need to Set

**/
void
MrcWmRegSetBits (
  IN     MrcParameters *const MrcData,
  IN     UINT64        SskpdBits
)
{
  UINT64 Sskpd;

  Sskpd = MrcReadCR64 (MrcData, SSKPD_PCU_REG);
  MrcWriteCR64(MrcData, SSKPD_PCU_REG, (Sskpd | SskpdBits));
}

/**
  This function sets DRAM to enable Write0 if it is supported by the device

  @param[in] MrcData - The global host structure

  @retval Returns True if feature is enabled, otherwise - False.
**/
BOOLEAN
MrcLpddr5Write0En (
  IN MrcParameters *const MrcData
  )
{
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  MrcChannelOut   *ChannelOut;
  UINT32          Controller;
  UINT32          Channel;
  UINT32          Rank;
  UINT8           MrrResult[4];
  UINT32          Device;
  UINT32          MaxDeviceNumber;
  UINT32          Loop;
  LPDDR5_MODE_REGISTER_21_TYPE    Mr21;
  MrcModeRegisterIndex            MrIndex;
  MrcModeRegister                 MrAddr;

  Outputs         = &MrcData->Outputs;
  Debug           = &Outputs->Debug;
  MaxDeviceNumber = (Outputs->LpByteMode) ? 2 : 1; // In ByteMode we have two x8 devices per channel
  MrAddr          = mrMR21;
  MrIndex         = MrcMrAddrToIndex (MrcData, &MrAddr);

  for (Loop = 0; Loop < 2; Loop++) {
    // Loop 0 - check if all devices support Write0
    // Loop 1 - enable Write0 on all devices
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          MrcIssueMrr (MrcData, Controller, Channel, Rank, MrAddr, MrrResult);
          if (Loop == 0) {
            for (Device = 0; Device < MaxDeviceNumber; Device++) {
              Mr21.Data8 = MrrResult[Device];
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tMC%u C%u R%u Device[%u] = 0x%02X\n", Controller, Channel, Rank, Device, MrrResult[Device]);
              if (Mr21.Bits.Wxfs != 1) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Write0 feature is not supported for device[%u]\n", Device);
                return FALSE;
              }
            }  // for Device
          } else { // Loop 1 - enable Write0
            Mr21.Data8 = MrrResult[0];  // Can use MR21 from Device 0
            Mr21.Bits.Wxfe = 1;
            MrcIssueMrw (MrcData, Controller, Channel, Rank, MrAddr, Mr21.Data8, TRUE);
            ChannelOut->Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR[MrIndex] = Mr21.Data8;  // Update MR21 in the host struct, for non-cold boots and runtime SAGV
          }
        }  // for Rank
      }  // for Channel
    }  // for Controller
  }  // for Loop

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Write0 feature enabled on all devices\n");

  return TRUE;
}

/**
  This function enables Write0 feature on LP5/DDR5

  @param[in] MrcData - The global host structure

  @retval mrcSuccess.
**/
MrcStatus
MrcWrite0 (
  IN MrcParameters *const MrcData
  )
{
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  MrcSaveData     *SaveData;
  INT64           GetSetEn;
  BOOLEAN         Lpddr5;

  Outputs   = &MrcData->Outputs;
  Debug     = &Outputs->Debug;
  SaveData  = &MrcData->Save.Data;
  GetSetEn  = 1;
  Lpddr5    = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);

  // @todo_adl - Disable DataInvertNibble if enabled (Mutex w\ Write0 feature)

  if (Outputs->EccSupport || SaveData->TmeEnable) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Write0 feature is Disabled when ECC or TME is on\n");
    return mrcSuccess;
  }

  if (Lpddr5) {
    if (!MrcLpddr5Write0En (MrcData)) {
      return mrcSuccess;
    }
  }

  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccWrite0En, WriteToCache | PrintValue, &GetSetEn);
  MrcGetSetNoScope (MrcData, GsmIocWrite0En,      WriteToCache | PrintValue, &GetSetEn);
  MrcGetSetNoScope (MrcData, GsmIocScramWrite0En, WriteToCache | PrintValue, &GetSetEn);
  MrcFlushRegisterCachedData (MrcData);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Write0 feature is enabled\n");

  return mrcSuccess;
}

/**
  This function Set a newvalue of the sticky scratchpad register by clear givin Bit(s)

  @param[in] MrcData   - include all the MRC data.
  @param[in] SskpdBits - Bit(s) Need to Clear

**/
void
MrcWmRegClrBits(
  IN     MrcParameters *const MrcData,
  IN     UINT64        SskpdBits
  )
{
  UINT64 Sskpd;

  Sskpd = MrcReadCR64 (MrcData, SSKPD_PCU_REG);
  MrcWriteCR64(MrcData, SSKPD_PCU_REG, Sskpd & (~SskpdBits));
}

/**
  This function fills in the MRS FSM to finalize the SAGV configuration for normal operation.

  @param[in] MrcData - The global host structure

  @retval mrcSuccess.
**/
MrcStatus
MrcSaGvFinal (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput          *Inputs;
  MrcDebug                *Debug;
  MrcOutput               *Outputs;
  MrcControllerOut        *ControllerOut;
  MrcChannelOut           *ChannelOut;
  const MrcRankOut        *RankOut;
  MrcDdrType              DdrType;
  MRC_FUNCTION            *MrcCall;
  UINT32                  Controller;
  UINT32                  Channel;
  UINT32                  IpChannel;
  UINT32                  Rank;
  UINT32                  Dimm;
  UINT32                  SubCh;
  UINT32                  Offset;
  UINT8                   VrefCode;
  UINT8                   Byte;
  UINT8                   RankMod2;
  BOOLEAN                 FsmSaved;
  BOOLEAN                 SaGv;
  BOOLEAN                 Lpddr4;

  MC0_CH0_CR_LPDDR_MR_CONTENT_STRUCT      LpddrMrContent;
  MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_STRUCT  Mr0Mr1Content;
  MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_STRUCT  Mr2Mr3Content;
  MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_STRUCT  Mr4Mr5Content;
  MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_STRUCT  Mr6Mr7Content;
  MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_STRUCT  Mr6VrefValues;
  MC0_CH0_CR_MRS_FSM_CONTROL_STRUCT MrsFsmControl;
  MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_STRUCT Lpddr4DiscreteMrValue;
#ifdef MRC_DEBUG_PRINT
  UINT8                   Mr;
  static const UINT8      Lpddr4MrIndex[8]   = {mrIndexMR3, mrIndexMR11, mrIndexMR12, mrIndexMR14, mrIndexMR1, mrIndexMR2, mrIndexMR22, mrIndexMR23};
  static const UINT8      Lpddr4MrAddress[8] = {mrMR3, mrMR11, mrMR12, mrMR14, mrMR1, mrMR2, mrMR22, mrMR23};
#endif

  Inputs        = &MrcData->Inputs;
  MrcCall       = MrcData->Inputs.Call.Func;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  DdrType       = Outputs->DdrType;
  Lpddr4        = (DdrType == MRC_DDR_TYPE_LPDDR4);
  SaGv          = (Inputs->SaGv == MrcSaGvEnabled);

  if (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5) {
    // For DDR5 it is called from MrcMcActivate(), hence only doing LP5 here.
    return MrcFinalizeMrSeq (MrcData, MRC_PRINTS_ON);
  }

  MrsFsmControl.Data = 0;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelOut = &ControllerOut->Channel[Channel];
      FsmSaved = FALSE;
      // In the MC, Channel 1/3 are treated as ranks 2/3.  Convert to MC IP layout:
      // Channel 0/2 -> IpChannel 0/1 SubCh 0
      // Channel 1/3 -> IpChannel 0/1 SubCh 1
      IpChannel = LP_IP_CH (Lpddr4, Channel);
      SubCh     = Channel % 2;
      if (Lpddr4) {
        for (Rank = 0; Rank < MAX_RANK_IN_DIMM; Rank++) {
          RankOut = &ChannelOut->Dimm[dDIMM0].Rank[Rank];
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
#ifdef MRC_DEBUG_PRINT
            for (Mr = 0; Mr < ARRAY_COUNT(Lpddr4MrIndex); Mr++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.C%u.R%u.MR[%u] = 0x%04X\n", Controller, Channel, Rank, Lpddr4MrAddress[Mr], RankOut->MR[Lpddr4MrIndex[Mr]]);
            }
#endif
            // The following configuration is per rank configurations.
            Lpddr4DiscreteMrValue.Data = 0;
            Lpddr4DiscreteMrValue.Bits.PDDS     = (RankOut->MR[mrIndexMR3]  >> 3) & 0x7;
            Lpddr4DiscreteMrValue.Bits.DQ_ODT   =  RankOut->MR[mrIndexMR11]       & 0x7;
            Lpddr4DiscreteMrValue.Bits.CA_ODT   = (RankOut->MR[mrIndexMR11] >> 4) & 0x7;
            Lpddr4DiscreteMrValue.Bits.CA_VREF  =  RankOut->MR[mrIndexMR12]       & 0x7F;
            Lpddr4DiscreteMrValue.Bits.DQ_VREF  =  RankOut->MR[mrIndexMR14]       & 0x7F;
            Lpddr4DiscreteMrValue.Bits.CODT     =  RankOut->MR[mrIndexMR22]       & 0x3F;
            Offset = OFFSET_CALC4 (
                      MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG, MC1_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG, Controller,
                      MC0_CH1_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG, IpChannel,
                      MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_2_REG, SubCh,
                      MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_1_REG, Rank
                      );
            MrcWriteCR64 (MrcData, Offset, Lpddr4DiscreteMrValue.Data);
            // Choose to use the System Definition for the register mapping instead of IpChannel
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.C%u.R%u: LPDDR4_DISCRETE_MR_VALUES = 0x%016llX\n", Controller, Channel, Rank, Lpddr4DiscreteMrValue.Data);

            // The following items are per MC channel (Common to both Ch 0/1 and 2/3 respectively)
            // Cannot support Ch 1 or 3 only configurations.
            if (!FsmSaved && (!IS_MC_SUB_CH (Lpddr4, Channel))) {
              LpddrMrContent.Data = 0;
              LpddrMrContent.Bits.MR1  = RankOut->MR[mrIndexMR1];
              LpddrMrContent.Bits.MR2  = RankOut->MR[mrIndexMR2];
              LpddrMrContent.Bits.MR3  = RankOut->MR[mrIndexMR3];
              LpddrMrContent.Bits.MR11 = RankOut->MR[mrIndexMR11];
              LpddrMrContent.Bits.MR12 = RankOut->MR[mrIndexMR12];
              LpddrMrContent.Bits.MR13 = RankOut->MR[mrIndexMR13];
              LpddrMrContent.Bits.MR22 = RankOut->MR[mrIndexMR22];
              LpddrMrContent.Bits.MR23 = RankOut->MR[mrIndexMR23];
              Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_LPDDR_MR_CONTENT_REG, MC1_CH0_CR_LPDDR_MR_CONTENT_REG, Controller, MC0_CH1_CR_LPDDR_MR_CONTENT_REG, IpChannel);
              MrcWriteCR64 (MrcData, Offset, LpddrMrContent.Data);
              // Choose to use the System Definition for the register mapping instead of IpChannel
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.C%u: LPDDR_MR_CONTENT = 0x%016llX\n", Controller, Channel, LpddrMrContent.Data);

              MrsFsmControl.Bits.LPDDR_Restore_MR = 0x1FF;
              MrsFsmControl.Bits.LPDDR4_split_transition = 1;
              MrsFsmControl.Bits.GV_auto_enable = 1;
              MrsFsmControl.Bits.Clear_VRCG = 1;
              MrsFsmControl.Bits.LPDDR4_switch_FSP = 1;
              MrsFsmControl.Bits.do_dq_osc_start = (Inputs->LpDqsOscEn) ? 1 : 0;
              MrsFsmControl.Bits.tVREFDQ = DIVIDECEIL (MRC_LP4_tFC_LONG_NS * 1000, Outputs->tCKps);  // tVREFDQ is in tCK units
              FsmSaved = TRUE;
            } // !FsmSaved && !IS_MC_SUB_CH
          } // RankExist
        } // For Rank
      } else {
        // DDR4
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
            RankMod2 = Rank % 2;
            Dimm = RANK_TO_DIMM_NUMBER (Rank);
            RankOut = &ChannelOut->Dimm[Dimm].Rank[RankMod2];

            // Per-device registers restore
            Mr6VrefValues.Data = 0;
            for (Byte = 0; Byte < 8; Byte++) { // Only 8 bytes, no place for ECC byte
              VrefCode = RankOut->DdrPdaVrefDq[Byte] & 0x7F;
              Mr6VrefValues.Data |= MrcCall->MrcLeftShift64 ((UINT64) VrefCode, Byte * MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte1_OFF);
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_NOTE,
                "Mc%u.C%u.R%d.S%u: Vref offset = %d (0 = 820mV) Code = 0x%X \n",
                Controller,
                Channel,
                Rank,
                Byte,
                MrcVrefDqToOffsetDdr4 (VrefCode),
                VrefCode
              );
            }
            Offset = OFFSET_CALC4 (
                      MC0_CH0_CR_DDR4_MR6_VREF_VALUES_0_REG, MC1_CH0_CR_DDR4_MR6_VREF_VALUES_0_REG, Controller,
                      MC0_CH1_CR_DDR4_MR6_VREF_VALUES_0_REG, Channel,
                      MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG, Dimm,
                      MC0_CH0_CR_DDR4_MR6_VREF_VALUES_1_REG, RankMod2
                      );
            MrcWriteCR64 (MrcData, Offset, Mr6VrefValues.Data);
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              "Mc%u.C%u.D%u,R%u: DDR4_MR6_VREF_VALUES = 0x%08X%08X\n",
              Controller,
              Channel,
              Dimm,
              Rank,
              Mr6VrefValues.Data32[1],
              Mr6VrefValues.Data32[0]
              );

            if (!FsmSaved) {
              Mr0Mr1Content.Data = 0;
              Mr2Mr3Content.Data = 0;
              Mr4Mr5Content.Data = 0;
              Mr6Mr7Content.Data = 0;
              Mr0Mr1Content.Bits.MR0 = RankOut->MR[0];
              Mr0Mr1Content.Bits.MR1 = RankOut->MR[1];
              Mr2Mr3Content.Bits.MR2 = RankOut->MR[2];
              Mr2Mr3Content.Bits.MR3 = RankOut->MR[3];
              MrsFsmControl.Bits.DDR4_Restore_MR = 0xF; // Restore MR0..MR3

              Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_REG, MC1_CH0_CR_DDR4_MR0_MR1_CONTENT_REG, Controller, MC0_CH1_CR_DDR4_MR0_MR1_CONTENT_REG, Channel);
              MrcWriteCR (MrcData, Offset, Mr0Mr1Content.Data);

              Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_REG, MC1_CH0_CR_DDR4_MR2_MR3_CONTENT_REG, Controller, MC0_CH1_CR_DDR4_MR2_MR3_CONTENT_REG, Channel);
              MrcWriteCR (MrcData, Offset, Mr2Mr3Content.Data);
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_NOTE,
                "Mc%u.C%u: MR0_MR1_CONTENT = 0x%08X, MR2_MR3_CONTENT = 0x%08X\n",
                Controller,
                Channel,
                Mr0Mr1Content.Data,
                Mr2Mr3Content.Data
                );

              Mr4Mr5Content.Bits.MR4 = RankOut->MR[4];
              Mr4Mr5Content.Bits.MR5 = RankOut->MR[5];
              Mr6Mr7Content.Bits.MR6 = RankOut->MR[6];
              MrsFsmControl.Bits.DDR4_Restore_MR = 0x7F; // Restore MR0..MR6
              MrsFsmControl.Bits.DDR4_Restore_MR6_Per_Device = 1;
              MrsFsmControl.Bits.vref_time_per_byte = 1;
              MrsFsmControl.Bits.tVREFDQ = DIVIDECEIL (tVREF_DQ_PS_DDR4, Outputs->Dclkps);
              if (ChannelOut->Dimm[1].Status == DIMM_PRESENT) {
                MrsFsmControl.Bits.TwoDPC_support = 1;
              }

              Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_REG, MC1_CH0_CR_DDR4_MR4_MR5_CONTENT_REG, Controller, MC0_CH1_CR_DDR4_MR4_MR5_CONTENT_REG, Channel);
              MrcWriteCR (MrcData, Offset, Mr4Mr5Content.Data);

              Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_REG, MC1_CH0_CR_DDR4_MR6_MR7_CONTENT_REG, Controller, MC0_CH1_CR_DDR4_MR6_MR7_CONTENT_REG, Channel);
              MrcWriteCR (MrcData, Offset, Mr6Mr7Content.Data);
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_NOTE,
                "Mc%u.C%u: MR4_MR5_CONTENT = 0x%08X, MR6_MR7_CONTENT = 0x%08X\n",
                Controller,
                Channel,
                Mr4Mr5Content.Data,
                Mr6Mr7Content.Data
                );
              FsmSaved = TRUE;
            } // !FsmSaved
          } // RankExist
        } // Rank
      } // DDR4

      if (SaGv && (!IS_MC_SUB_CH (Lpddr4, Channel))) {
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MRS_FSM_CONTROL_REG, MC1_CH0_CR_MRS_FSM_CONTROL_REG, Controller, MC0_CH1_CR_MRS_FSM_CONTROL_REG, IpChannel);
        MrcWriteCR64 (MrcData, Offset, MrsFsmControl.Data);
        // Choose to use the System Definition for the register mapping instead of IpChannel
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.C%u: MRS_FSM_CONTROL = 0x%016llX\n", Controller, Channel, MrsFsmControl.Data);
      }
    } // for Channel
  } // Controller
#ifdef MRC_DEBUG_PRINT
  if (DdrType == MRC_DDR_TYPE_DDR4) {
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      if (((1 << Rank) & Outputs->ValidRankMask) == 0) {
        // Skip if this rank is not present on any of the channels
        continue;
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Rank %d\tMc0\tMc1", Rank);
      RankMod2 = Rank % 2;
      for (Mr = 0; Mr <= 6; Mr++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMR[%d]:", Mr);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          ControllerOut = &Outputs->Controller[Controller];
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            ChannelOut = &ControllerOut->Channel[Channel];
            RankOut = &ChannelOut->Dimm[RANK_TO_DIMM_NUMBER (Rank)].Rank[RankMod2];
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              if (Controller == 0) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
              }
              continue;
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t0x%04X", RankOut->MR[Mr]);
          } // for Channel
        } // for Controller
      } // for Mr
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
    } // for Rank
  } // if DDR4
#endif

  return mrcSuccess;
}

/**
  Gets pointers to functions inside of core.

  @param[in]  MrcData                     - All the MRC global data.
  @param[out] CallChannelExist            - Pointer to the function MrcChannelExist
  @param[out] CallPrintf                  - Pointer to the function MrcPrintf
  @param[out] CallChangeMargin            - Pointer to the function ChangeMargin
  @param[out] CallSignExtend              - Pointer to the function MrcSignExtend
  @param[out] CallShiftPIforCmdTraining   - Pointer to the function ShiftPIforCmdTraining
  @param[out] CallMrcUpdateVref           - Pointer to the function MrcUpdateVref

  @retval Returns mrcSuccess if the function succeeds.
**/
MrcStatus
MrcGetCoreFunction (
  IN const MrcParameters *const MrcData,
  OUT UINTN                    *CallChannelExist,
  OUT UINTN                    *CallPrintf,
  OUT UINTN                    *CallChangeMargin,
  OUT UINTN                    *CallSignExtend,
  OUT UINTN                    *CallShiftPIforCmdTraining,
  OUT UINTN                    *CallMrcUpdateVref
  )
{
  *CallChannelExist            = (UINTN) &MrcChannelExist;
  *CallPrintf                  = (UINTN) &MrcPrintf;
  *CallChangeMargin            = (UINTN) &ChangeMargin;
  *CallSignExtend              = (UINTN) &MrcSE;
  *CallShiftPIforCmdTraining   = (UINTN) &ShiftPIforCmdTraining;
  *CallMrcUpdateVref           = (UINTN) &MrcUpdateVref;
  return (mrcSuccess);
}

#ifdef MRC_DEBUG_PRINT
/**
  Print the input parameters to the debug message output port.

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcPrintInputParameters (
  IN MrcParameters *const MrcData
  )
{
  MrcDebug                *Debug;
  const MrcInput          *Inputs;
  const MrcControllerIn   *ControllerIn;
  const MrcChannelIn      *ChannelIn;
  const MrcDimmIn         *DimmIn;
  const MrcTiming         *Timing;
  const TrainingStepsEn   *TrainingSteps;
  const TrainingStepsEn2  *TrainingSteps2;
  const UINT8             *Buffer;
  UINT16                  Line;
  UINT16                  Address;
  UINT16                  Offset;
  UINT8                   Controller;
  UINT8                   Channel;
  UINT8                   Dimm;
  UINT32                  Index;
  CHAR8                   HexDump[16 * 3 + 16 + 1];
  CHAR8                   *p;
  UINT8                   Data8;

  Inputs  = &MrcData->Inputs;
  Debug   = &MrcData->Outputs.Debug;

  // The following are system level definitions. All memory controllers in the system are set to these values.
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Debug.Stream: %Xh\n", Debug->Stream);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Debug.Level: %Xh\n", Debug->Level);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "HeapBase: %08Xh\n", Inputs->HeapBase.DataN);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "HeapSize: %u\n", Inputs->HeapSize);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SerialBuffer: %08Xh\n", Inputs->SerialBuffer.DataN);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SerialBufferSize: %u\nFreqMax: %u\n", Inputs->SerialBufferSize, Inputs->FreqMax);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "FreqLimitMixedConfig: %u\nFirstDimmBitMask: 0x%X \nFirstDimmBitMaskEcc: 0x%X\n", Inputs->FreqLimitMixedConfig, Inputs->FirstDimmBitMask, Inputs->FirstDimmBitMaskEcc);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "FreqLimitMixedConfig_1R1R_8GB: %u\n", Inputs->FreqLimitMixedConfig_1R1R_8GB);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "FreqLimitMixedConfig_1R1R_16GB: %u\n", Inputs->FreqLimitMixedConfig_1R1R_16GB);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "FreqLimitMixedConfig_1R1R_8GB_16GB: %u\n", Inputs->FreqLimitMixedConfig_1R1R_8GB_16GB);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "FreqLimitMixedConfig_2R2R: %u\n", Inputs->FreqLimitMixedConfig_2R2R);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "GearRatio: %u\nRatio: %u\n", Inputs->GearRatio, Inputs->Ratio);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RefClk: %uMHz\nBClk: %uHz\n", (Inputs->RefClk == MRC_REF_CLOCK_100) ? 100 : 133, Inputs->BClkFrequency);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DynamicMemoryBoost: %u\nRealtimeMemoryFrequency: %u\n", Inputs->DynamicMemoryBoost, Inputs->RealtimeMemoryFrequency);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SagvSwitchFactorIA: %u\nSagvSwitchFactorGT: %u\nSagvSwitchFactorIO: %u\nSagvSwitchFactorStall: %u\nSagvHeuristicsUpControl: %u\nSagvHeuristicsDownControl: %u\n", Inputs->SagvSwitchFactorIA, Inputs->SagvSwitchFactorGT, Inputs->SagvSwitchFactorIO, Inputs->SagvSwitchFactorStall, Inputs->SagvHeuristicsUpControl, Inputs->SagvHeuristicsDownControl);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MemoryProfile: %u\nLp5CccConfig: %Xh\n", Inputs->MemoryProfile, Inputs->Lp5CccConfig);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "BoardType: %u\nCmdRanksTerminated: %Xh\n", Inputs->BoardType, Inputs->CmdRanksTerminated);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DqPinsInterleaved: %u\nSeparateCkeDelayDdr4: %u\n", Inputs->DqPinsInterleaved, Inputs->SeparateCkeDelayDdr4);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CpuModel: %Xh\nCpuStepping: %Xh\n", Inputs->CpuModel, Inputs->CpuStepping);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CpuPackageTdp: %dW\nGraphicsStolenSize: %Xh\n", (Inputs->CpuPackageTdp / 100), Inputs->GraphicsStolenSize);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "GraphicsGttSize: %Xh\n", Inputs->GraphicsGttSize);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Iteration: %Xh\nDebugValue: %Xh\n", Inputs->Iteration, Inputs->DebugValue);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VddVoltage: %u mV\nVddqVoltage: %u mV\nVppVoltage: %u mV\n", Inputs->VddVoltage, Inputs->VddqVoltage, Inputs->VppVoltage);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "BootMode: %Xh\n", Inputs->BootMode);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "TxtFlag: %Xh\nSimicsFlag: %Xh\n", Inputs->TxtFlag, Inputs->SimicsFlag);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MobilePlatform: %Xh\nEccDftEn: %u\nWrite0: %u\n", Inputs->MobilePlatform, Inputs->EccDftEn, Inputs->Write0);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EccSupport: %Xh\nCleanMemory: 0x%X\n", Inputs->EccSupport, Inputs->CleanMemory);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Ibecc: %u\nIbeccOperationMode: %u\n", Inputs->Ibecc, Inputs->IbeccOperationMode);
  for (Index = 0; Index < MAX_IBECC_REGIONS; Index++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "IbeccProtectedRangeEnable[%u]: %u, Base: 0x%X, Mask: 0x%X\n", Index, Inputs->IbeccProtectedRangeEnable[Index], Inputs->IbeccProtectedRangeBase[Index], Inputs->IbeccProtectedRangeMask[Index]);
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "IbeccErrInjControl: %u\nIbeccErrInjAddress: 0x%llX\n", Inputs->IbeccErrInjControl, Inputs->IbeccErrInjAddress);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "IbeccErrInjMask: 0x%llX\nIbeccErrInjCount: %u\n", Inputs->IbeccErrInjMask, Inputs->IbeccErrInjCount);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EccErrInjAddress 0x%llx\nEccErrInjMask: 0x%llX\nEccErrInjCount: %u\n", Inputs->EccErrInjAddress, Inputs->EccErrInjMask, Inputs->EccErrInjCount);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ScramblerSupport: %Xh\n", Inputs->ScramblerSupport);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RemapEnable: %Xh\n", Inputs->RemapEnable);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PowerDownMode: %Xh\n", Inputs->PowerDownMode);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PwdwnIdleCounter: %Xh\nDisPgCloseIdleTimeout: %Xh\n", Inputs->PwdwnIdleCounter, Inputs->DisPgCloseIdleTimeout);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RankInterleave: %Xh\n", Inputs->RankInterleave);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EnhancedInterleave: %Xh\n", Inputs->EnhancedInterleave);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "WeaklockEn: %Xh\n", Inputs->WeaklockEn);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EnCmdRate: %Xh\nLctCmdEyeWidth: %u\n", Inputs->EnCmdRate, Inputs->LctCmdEyeWidth);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Lp5BankMode: %u\n", Inputs->Lp5BankMode);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "BaseAddresses\n");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  PciE: %Xh\n", Inputs->PciEBaseAddress);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  MchBar: %Xh\n", Inputs->MchBarBaseAddress);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  Smbus: %Xh\n", Inputs->SmbusBaseAddress);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MeStolenSize: %Xh\n", Inputs->MeStolenSize);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MmioSize: %Xh\n", Inputs->MmioSize);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "TsegSize: %Xh\n", Inputs->TsegSize);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DprSize: %Xh\n", Inputs->DprSize);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PrmrrSize: %Xh\n", Inputs->PrmrrSize);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VddSettleWaitTime: %Xh\n", Inputs->VddSettleWaitTime);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VccIomV: %d\n", Inputs->VccIomV);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RmtPerTask: %d\nTrainTrace: %d\n", Inputs->RmtPerTask, Inputs->TrainTrace);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MarginLimitCheck: %d\nMarginLimitL2: %d\n", Inputs->MarginLimitCheck, Inputs->MarginLimitL2);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AutoSelfRefreshSupport: %u\n", Inputs->AutoSelfRefreshSupport);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ExtTemperatureSupport: %u\n", Inputs->ExtTemperatureSupport);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RhSelect: %u\nLfsr0Mask: %u\nLfsr1Mask: %u\n", Inputs->RhSelect, Inputs->Lfsr0Mask, Inputs->Lfsr1Mask);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "McRefreshRate: %u\nRefreshWm: %u\n", Inputs->McRefreshRate, Inputs->RefreshWm);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RetrainOnFastFail: %u\nLpDdrDqDqsReTraining (LpDqsOscEn) : %Xh\n", Inputs->RetrainOnFastFail, Inputs->LpDqsOscEn);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "NewFeatureEnable1: %u\nNewFeatureEnable2: %u\n", Inputs->NewFeatureEnable1, Inputs->NewFeatureEnable2);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcSafeConfig: %u\nSafeMode: %u\n", Inputs->MrcSafeConfig, Inputs->SafeMode);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ReuseAdlSDdr5Board: %u\n", Inputs->ReuseAdlSDdr5Board);
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "FirstDimmBitMask: 0x%x\n", Inputs->FirstDimmBitMask);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SBGACpu: %u\n", Inputs->SBGACpu);
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "FirstDimmBitMaskEcc: 0x%x\n", Inputs->FirstDimmBitMaskEcc);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ChHashOverride: %u\nChHashEnable: %u\n", Inputs->ChHashOverride, Inputs->ChHashEnable);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ChHashMask: %Xh\n", Inputs->ChHashMask);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ChHashInterleaveBit: %Xh\nExtendedBankHashing: %u\nPerBankRefresh: %u\n", Inputs->ChHashInterleaveBit, Inputs->ExtendedBankHashing, Inputs->PerBankRefresh);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ProbelessTrace: %u\n", Inputs->ProbelessTrace);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Force1Dpc: %u\nEnablePda: %u\n", Inputs->Force1Dpc, Inputs->EnablePda);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DisableChannel:\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tM[%u]C[%u]:%u\n", Controller, Channel, Inputs->DisableChannel[Controller][Channel]);
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ForceSingleRank: %u\n", Inputs->ForceSingleRank);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VttCompForVsshi: %u\nCmdMirror: 0x%x\n", Inputs->VttCompForVsshi, Inputs->CmdMirror);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Ddr4OneDpc: %u\nRxMode: %u\n", Inputs->Ddr4OneDpc, Inputs->RxMode);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "LpFreqSwitch: %u\n", Inputs->LpFreqSwitch);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PowerTrainingMode: %s optimization\n", Inputs->PowerTrainingMode ? "margin" : "power");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "BdatEnable: %u\nBdatTestType: %u\n", Inputs->BdatEnable, Inputs->BdatTestType);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PprEnable: %u\nPeriodicDcc: %u\nLpMode: %u\n", Inputs->PprEnable, Inputs->PeriodicDcc, Inputs->LpMode);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s", PrintBorder);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "*****    MRC TRAINING STEPS     *****\n");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s", PrintBorder);
  TrainingSteps = &Inputs->TrainingEnables;
  TrainingSteps2 = &Inputs->TrainingEnables2;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "JedecReset: %u\n%s.SOT: %u\n", Inputs->JedecReset, TrainEnString, TrainingSteps->SOT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.ERDMPRTC2D: %u\n", TrainEnString, TrainingSteps->ERDMPRTC2D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.ECT: %u\n", TrainEnString, TrainingSteps->ECT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RCVET: %u\n", TrainEnString, TrainingSteps->RCVET);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RDMPRT: %u\n", TrainEnString, TrainingSteps->RDMPRT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.JWRL: %u\n", TrainEnString, TrainingSteps->JWRL);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.EWRTC2D: %u\n", TrainEnString, TrainingSteps->EWRTC2D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.ERDTC2D: %u\n", TrainEnString, TrainingSteps->ERDTC2D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.WRTC1D: %u\n", TrainEnString, TrainingSteps->WRTC1D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.WRVC1D: %u\n", TrainEnString, TrainingSteps->WRVC1D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RDTC1D: %u\n", TrainEnString, TrainingSteps->RDTC1D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s2.DIMMODTCAT: %u\n%s.DIMMODTT: %u\n", TrainEnString, TrainingSteps2->DIMMODTCA, TrainEnString, TrainingSteps->DIMMODTT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.DIMMRONT: %u\n%s.DIMMDFE: %u EARLYDIMMDFE: %u\n", TrainEnString, TrainingSteps->DIMMRONT, TrainEn2String, TrainingSteps2->DIMMDFE, TrainingSteps2->EARLYDIMMDFE);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.TXDQSDCC: %u\n", TrainEn2String, TrainingSteps2->TXDQSDCC);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.DRAMDCA: %u\n", TrainEn2String, TrainingSteps2->DRAMDCA);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.WRDS: %u\n", TrainEn2String, TrainingSteps2->WRDS);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.WRDSEQT: %u\n", TrainEnString, TrainingSteps->WRDSEQT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.WRDSUDT: %u\n", TrainEnString, TrainingSteps->WRDSUDT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.WRSRT: %u\n", TrainEnString, TrainingSteps->WRSRT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RDODTT: %u\n", TrainEnString, TrainingSteps->RDODTT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RDEQT: %u\n", TrainEnString, TrainingSteps->RDEQT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RDAPT: %u\n", TrainEnString, TrainingSteps->RDAPT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.CMDVC: %u\n", TrainEnString, TrainingSteps->CMDVC);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.WRTC2D: %u\n", TrainEnString, TrainingSteps->WRTC2D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RDTC2D: %u\n", TrainEnString, TrainingSteps->RDTC2D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.WRVC2D: %u\n", TrainEnString, TrainingSteps->WRVC2D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RDVC2D: %u\n", TrainEnString, TrainingSteps->RDVC2D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.LCT: %u\n", TrainEnString, TrainingSteps->LCT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RTL: %u\n", TrainEnString, TrainingSteps->RTL);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.TAT: %u\n", TrainEnString, TrainingSteps->TAT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RMT: %u\n", TrainEnString, TrainingSteps->RMT);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.MEMTST: %u\n", TrainEnString, TrainingSteps->MEMTST);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.ALIASCHK: %u\n", TrainEnString, TrainingSteps->ALIASCHK);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RCVENC1D: %u\n", TrainEnString, TrainingSteps->RCVENC1D);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RMC: %u\n%s.DCC: %u\n", TrainEnString, TrainingSteps->RMC, TrainEn2String, TrainingSteps2->DCC);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RDVC1D: %u\n%s.TXTCO: %u\n%s.CLKTCO: %u\n", TrainEn2String, TrainingSteps2->RDVC1D,
                 TrainEn2String, TrainingSteps2->TXTCO, TrainEn2String, TrainingSteps2->CLKTCO);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.CMDSR: %u\n%s.CMDDSEQ: %u\n%s.DIMMODTCA: %u\n%s.TXTCODQS: %u\n", TrainEn2String, TrainingSteps2->CMDSR,
                 TrainEn2String, TrainingSteps2->CMDDSEQ, TrainEn2String, TrainingSteps2->DIMMODTCA, TrainEn2String, TrainingSteps2->TXTCODQS);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.CMDDRUD: %u\n%s.VCCDLLBP: %u\n%s.PVTTDNLP: %u\n", TrainEn2String, TrainingSteps2->CMDDRUD,
                 TrainEn2String, TrainingSteps2->VCCDLLBP, TrainEn2String, TrainingSteps2->PVTTDNLP);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.RDVREFDC: %u\n%s.VDDQT: %u\n%s.RMTBIT: %u\n", TrainEn2String, TrainingSteps2->RDVREFDC, TrainEn2String, TrainingSteps2->VDDQT,
                 TrainEn2String, TrainingSteps2->RMTBIT);
  // SAGV Inputs
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SAGV Inputs\nPoints\tFreq\tGears\n");
  for (Index = 0; Index < MAX_SAGV_POINTS; Index++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%u\t%u\t%u\n", Index, Inputs->SaGvFreq[Index], Inputs->SaGvGear[Index]);
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s", PrintBorder);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "*****      MRC TIMING DATA      *****\n");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s", PrintBorder);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerIn = &Inputs->Controller[Controller];
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Controller[%u] ChannelCount: %Xh\n", Controller, ControllerIn->ChannelCount);
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      ChannelIn = &ControllerIn->Channel[Channel];
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel[%u].Status: %Xh\n", Channel, ChannelIn->Status);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel[%u].DimmCount: %Xh\n", Channel, ChannelIn->DimmCount);

      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        DimmIn = &ChannelIn->Dimm[Dimm];
        Timing = &DimmIn->Timing;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u Status: %Xh\n", CcdString, Controller, Channel, Dimm, DimmIn->Status);
        if (Inputs->MemoryProfile == CUSTOM_PROFILE1) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tCK    : %u\n", CcdString, Controller, Channel, Dimm, Timing->tCK);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u NMode  : %u\n", CcdString, Controller, Channel, Dimm, Timing->NMode);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tCL    : %u\n", CcdString, Controller, Channel, Dimm, Timing->tCL);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tCWL   : %u\n", CcdString, Controller, Channel, Dimm, Timing->tCWL);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tFAW   : %u\n", CcdString, Controller, Channel, Dimm, Timing->tFAW);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tRAS   : %u\n", CcdString, Controller, Channel, Dimm, Timing->tRAS);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tRCDtRP: %u\n", CcdString, Controller, Channel, Dimm, Timing->tRCDtRP);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tREFI  : %u\n", CcdString, Controller, Channel, Dimm, Timing->tREFI);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tRFC   : %u\n", CcdString, Controller, Channel, Dimm, Timing->tRFC);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tRFCpb : %u\n", CcdString, Controller, Channel, Dimm, Timing->tRFCpb);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tRFC2  : %u\n", CcdString, Controller, Channel, Dimm, Timing->tRFC2);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tRFC4  : %u\n", CcdString, Controller, Channel, Dimm, Timing->tRFC4);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tRRD   : %u\n", CcdString, Controller, Channel, Dimm, Timing->tRRD);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tRRD_L : %u\n", CcdString, Controller, Channel, Dimm, Timing->tRRD_L);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tRRD_S : %u\n", CcdString, Controller, Channel, Dimm, Timing->tRRD_S);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tRTP   : %u\n", CcdString, Controller, Channel, Dimm, Timing->tRTP);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tWR    : %u\n", CcdString, Controller, Channel, Dimm, Timing->tWR);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tWTR   : %u\n", CcdString, Controller, Channel, Dimm, Timing->tWTR);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tWTR_L : %u\n", CcdString, Controller, Channel, Dimm, Timing->tWTR_L);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tWTR_S : %u\n", CcdString, Controller, Channel, Dimm, Timing->tWTR_S);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u tCCD_L : %u\n", CcdString, Controller, Channel, Dimm, Timing->tCCD_L);
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s %u/%u/%u SpdAddress: %Xh\n", CcdString, Controller, Channel, Dimm, DimmIn->SpdAddress);
        Buffer = (UINT8 *) &DimmIn->Spd.Data;
        if (Buffer[2] == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " empty\n");
          continue;
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SPD:           00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F\n");
        for (Line = 0; Line < (sizeof (MrcSpd) / 16); Line++) {
          Address = Line * 16;
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " % 4Xh(% 5u): ", Address, Address);
          p = HexDump;
          for (Offset = 0; Offset < 16; Offset++) {
            p += MrcSPrintf (MrcData, p, sizeof (HexDump) - (p - HexDump), "%02X ", Buffer[Address + Offset]) - 1;
          }
          for (Offset = 0; Offset < 16; Offset++) {
            Data8 = Buffer[Address + Offset];
            *p++ = isprint (Data8) ? Data8 : '.';
          }
          *p = '\0';
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\n", HexDump);
        }
      }
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s", PrintBorder);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "*****    THERMAL OVERWRITE    *******\n");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s", PrintBorder);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.EnablePwrDn     : %Xh\n",   ThermEnString, Inputs->EnablePwrDn);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.EnablePwrDnLpddr: %Xh\n",   ThermEnString, Inputs->EnablePwrDnLpddr);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s.Refresh2X       : %Xh\n",   ThermEnString, Inputs->Refresh2X);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SrefCfgEna      : %Xh\n",      Inputs->SrefCfgEna);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SrefCfgIdleTmr  : %Xh\n",      Inputs->SrefCfgIdleTmr);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ThrtCkeMinDefeat: %Xh\n",      Inputs->ThrtCkeMinDefeat);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ThrtCkeMinTmr   : %Xh\n",      Inputs->ThrtCkeMinTmr);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ThrtCkeMinDefeatLpddr: %Xh\n", Inputs->ThrtCkeMinDefeatLpddr);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ThrtCkeMinTmrLpddr   : %Xh\n", Inputs->ThrtCkeMinTmrLpddr);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RcompResistor: %u\n",  Inputs->RcompResistor);
  for (Index = 0; Index < MAX_RCOMP_TARGETS; Index++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RcompTarget[%s]: %u\n", GlobalCompOffsetStr[Index], Inputs->RcompTarget[Index]);
  }

  return mrcSuccess;
}

/**
  Print the specified memory to the serial message debug port.

  @param[in] Debug - Serial message debug structure.
  @param[in] Start - The starting address to dump.
  @param[in] Size  - The amount of data in bytes to dump.

  @retval Nothing.
**/
void
MrcPrintMemory (
  IN MrcDebug *const    Debug,
  IN const UINT8 *const Start,
  IN const UINT32       Size
  )
{
  const UINT8  *Address;
  const UINT8  *End;
  UINT32       Line;
  UINT32       Offset;
  union {
    UINT64     Data64[2];
    UINT32     Data32[4];
    UINT16     Data16[8];
    UINT8      Data8[16];
  } Buffer;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "          ");
  for (Offset = 0; Offset < 16; Offset++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%02X ", ((UINTN) Start + Offset) % 16);
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  End = Start + Size;
  for (Line = 0; Line < ((Size / 16) + 1); Line++) {
    Address = Start + (Line * 16);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "% 8X: ", Address);
    for (Offset = 0; Offset < 16; Offset++) {
      Buffer.Data8[Offset] = ((Address + Offset) < End) ? Address[Offset] : 0;
    }
    for (Offset = 0; Offset < 16; Offset++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, ((Address + Offset) < End) ? "%02X " : "   ", Buffer.Data8[Offset]);
    }
    for (Offset = 0; (Offset < 16) && ((Address + Offset) < End); Offset++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%c", isprint (Buffer.Data8[Offset]) ? Buffer.Data8[Offset] : '.');
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  }
  return;
}
#endif


/**
  Sets CpuModel and CpuStepping in MrcData based on CpuModelStep.

  @param[out] MrcData     - The Mrc Host data structure
  @param[in]  CpuModel    - The CPU Family Model.
  @param[in]  CpuStepping - The CPU Stepping.

  @retval mrcSuccess if the model and stepping is found.  Otherwise mrcFail
**/
MrcStatus
MrcSetCpuInformation (
  OUT MrcParameters  *MrcData,
  IN  MrcCpuModel    CpuModel,
  IN  MrcCpuStepping CpuStepping
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcInput            *Inputs;
  MrcDebug            *Debug;
  MrcStatus           Status;
  char                *Step;
  UINT64              Revision;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Debug   = &MrcData->Outputs.Debug;
  Status  = mrcFail;

  Inputs->CpuFamily   = cfMax;
  Inputs->TestEngine  = MrcTeCpgc20;
  Step = NULL;

  //MRC host structure is Byte set to 0.  So Inputs->[DtHalo,Mobile] is FALSE by default
  switch (CpuModel) {
    case cmADL_DT_HALO:
      Inputs->CpuModel  = cmADL_DT_HALO;
      Inputs->CpuFamily = cfAdl;
      Inputs->DtHalo    = TRUE;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AlderLake-S:");

      switch (CpuStepping) {
        case csAdlA0:
          Inputs->CpuStepping = csAdlA0;
          Inputs->A0 = TRUE;
          Step = "A0";
          break;

        case csAdlB0:
          Inputs->CpuStepping = csAdlB0;
          Inputs->B0 = TRUE;
          Step = "B0";
          break;

        case csAdlG0:
          Inputs->CpuStepping = csAdlG0;
          Inputs->G0 = TRUE;
          Inputs->Q0Regs = TRUE;  // G0 has the same register changes as Q0
          Step = "G0";
          break;

        case csAdlH0:
          Inputs->CpuStepping = csAdlH0;
          Inputs->H0 = TRUE;
          Inputs->Q0Regs = TRUE;  // H0 has the same register changes as Q0
          Step = "H0";
          break;

        case csAdlC0:
          Inputs->CpuStepping = csAdlC0;
          Inputs->C0 = TRUE;
          Inputs->Q0Regs = TRUE;  // C0 has the same register changes as Q0
          Step = "C0";
          break;

        default:
          Inputs->CpuStepping = csAdlDtHaloLast;
          break;
      }
      Status = mrcSuccess;
      break;

    case cmADL_ULX_ULT:
      Inputs->CpuModel  = cmADL_ULX_ULT;
      Inputs->CpuFamily = cfAdl;
      Inputs->UlxUlt    = TRUE;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AlderLake-P/M:");

      switch (CpuStepping) {
        case csAdlJ0:
          Inputs->CpuStepping = csAdlJ0;
          Inputs->J0 = TRUE;
          Step = "J0";
          break;

        case csAdlQ0:
          Inputs->CpuStepping = csAdlQ0;
          Inputs->Q0 = TRUE;
          Inputs->Q0Regs = TRUE;
          Step = "Q0";
          break;

        case csAdlK0:
          Inputs->CpuStepping = csAdlK0;
          Inputs->K0 = TRUE;
          Inputs->Q0Regs = TRUE;  // C0 has the same register changes as Q0
          Step = "K0";
          break;

        case csAdlL0:
          Inputs->CpuStepping = csAdlL0;
          Inputs->L0 = TRUE;
          Inputs->Q0Regs = TRUE;  // L0 has the same register changes as Q0
          Step = "L0";
          break;

        case csAdlR0:
          Inputs->CpuStepping = csAdlR0;
          Inputs->R0 = TRUE;
          Inputs->Q0Regs = TRUE;  // R0 has the same register changes as Q0
          Step = "R0";
          break;

        default:
          Inputs->CpuStepping = csAdlUlxUltLast;
          break;
      }
      Status = mrcSuccess;
      break;
  }

  if (Status == mrcSuccess) {
    if (Step != NULL) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " Stepping %s\n", Step);
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "\nWARNING: Unknown CPU stepping, using MRC for last known step = %Xh\n", Inputs->CpuStepping);
    }
  }

  Revision = MrcCall->MrcReadMsr64 (MRC_MSR_IA32_BIOS_SIGN_ID);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Microcode Update: 0x%08X\n", (UINT32) (MrcCall->MrcRightShift64 (Revision, 32)));

  return Status;
}



