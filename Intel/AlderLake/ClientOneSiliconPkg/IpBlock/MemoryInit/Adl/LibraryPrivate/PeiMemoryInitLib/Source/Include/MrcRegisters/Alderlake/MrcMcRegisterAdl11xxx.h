#ifndef __MrcMcRegisterAdl11xxx_h__
#define __MrcMcRegisterAdl11xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2020 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_REG                          (0x00011000)

  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_SAI_MASK_OFF               ( 0)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_SAI_MASK_WID               (64)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_SAI_MASK_MSK               (0xFFFFFFFFFFFFFFFFULL)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_SAI_MASK_MIN               (0)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_SAI_MASK_MAX               (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_SAI_MASK_DEF               (0x40001000208)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_SAI_MASK_HSH               (0x40011000)

#define CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_REG                         (0x00011008)

  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_SAI_MASK_OFF              ( 0)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_SAI_MASK_WID              (64)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_SAI_MASK_MSK              (0xFFFFFFFFFFFFFFFFULL)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_SAI_MASK_MIN              (0)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_SAI_MASK_MAX              (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_SAI_MASK_DEF              (0x4000100021A)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_SAI_MASK_HSH              (0x40011008)

#define CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_REG                         (0x00011010)

  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_SAI_MASK_OFF              ( 0)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_SAI_MASK_WID              (64)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_SAI_MASK_MSK              (0xFFFFFFFFFFFFFFFFULL)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_SAI_MASK_MIN              (0)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_SAI_MASK_MAX              (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_SAI_MASK_DEF              (0xFFFFFFFFFFFFFFFF)
  #define CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_SAI_MASK_HSH              (0x40011010)

#define CMF_0_P_U_CODE_PG_ACCESS_POLICY_CP_REG                         (0x00011018)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_REG

#define CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_REG                        (0x00011020)

  #define CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_SAI_MASK_OFF             ( 0)
  #define CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_SAI_MASK_WID             (64)
  #define CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_SAI_MASK_MSK             (0xFFFFFFFFFFFFFFFFULL)
  #define CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_SAI_MASK_MIN             (0)
  #define CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_SAI_MASK_MAX             (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_SAI_MASK_DEF             (0x4000100020A)
  #define CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_SAI_MASK_HSH             (0x40011020)

#define CMF_0_P_U_CODE_PG_ACCESS_POLICY_RAC_REG                        (0x00011028)
//Duplicate of CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_REG

#define CMF_0_OS_PG_ACCESS_POLICY_CP_REG                               (0x00011030)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_REG

#define CMF_0_OS_PG_ACCESS_POLICY_WAC_REG                              (0x00011038)

  #define CMF_0_OS_PG_ACCESS_POLICY_WAC_SAI_MASK_OFF                   ( 0)
  #define CMF_0_OS_PG_ACCESS_POLICY_WAC_SAI_MASK_WID                   (64)
  #define CMF_0_OS_PG_ACCESS_POLICY_WAC_SAI_MASK_MSK                   (0xFFFFFFFFFFFFFFFFULL)
  #define CMF_0_OS_PG_ACCESS_POLICY_WAC_SAI_MASK_MIN                   (0)
  #define CMF_0_OS_PG_ACCESS_POLICY_WAC_SAI_MASK_MAX                   (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define CMF_0_OS_PG_ACCESS_POLICY_WAC_SAI_MASK_DEF                   (0x4000100021B)
  #define CMF_0_OS_PG_ACCESS_POLICY_WAC_SAI_MASK_HSH                   (0x40011038)

#define CMF_0_OS_PG_ACCESS_POLICY_RAC_REG                              (0x00011040)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_REG

#define CMI_0_PRIO_THRESHOLD_REG                                       (0x00011048)

  #define CMI_0_PRIO_THRESHOLD_NON_VC1_PRIO_THRESHOLD_OFF              ( 0)
  #define CMI_0_PRIO_THRESHOLD_NON_VC1_PRIO_THRESHOLD_WID              (16)
  #define CMI_0_PRIO_THRESHOLD_NON_VC1_PRIO_THRESHOLD_MSK              (0x0000FFFF)
  #define CMI_0_PRIO_THRESHOLD_NON_VC1_PRIO_THRESHOLD_MIN              (0)
  #define CMI_0_PRIO_THRESHOLD_NON_VC1_PRIO_THRESHOLD_MAX              (65535) // 0x0000FFFF
  #define CMI_0_PRIO_THRESHOLD_NON_VC1_PRIO_THRESHOLD_DEF              (0x00000008)
  #define CMI_0_PRIO_THRESHOLD_NON_VC1_PRIO_THRESHOLD_HSH              (0x10011048)

  #define CMI_0_PRIO_THRESHOLD_VC1_RD_PRIO_THRESHOLD_OFF               (16)
  #define CMI_0_PRIO_THRESHOLD_VC1_RD_PRIO_THRESHOLD_WID               (16)
  #define CMI_0_PRIO_THRESHOLD_VC1_RD_PRIO_THRESHOLD_MSK               (0xFFFF0000)
  #define CMI_0_PRIO_THRESHOLD_VC1_RD_PRIO_THRESHOLD_MIN               (0)
  #define CMI_0_PRIO_THRESHOLD_VC1_RD_PRIO_THRESHOLD_MAX               (65535) // 0x0000FFFF
  #define CMI_0_PRIO_THRESHOLD_VC1_RD_PRIO_THRESHOLD_DEF               (0x00000008)
  #define CMI_0_PRIO_THRESHOLD_VC1_RD_PRIO_THRESHOLD_HSH               (0x10211048)

#define CMI_0_PRIO_LIM_REG                                             (0x0001104C)

  #define CMI_0_PRIO_LIM_LOW_PRIORITY_LIM_OFF                          ( 0)
  #define CMI_0_PRIO_LIM_LOW_PRIORITY_LIM_WID                          (16)
  #define CMI_0_PRIO_LIM_LOW_PRIORITY_LIM_MSK                          (0x0000FFFF)
  #define CMI_0_PRIO_LIM_LOW_PRIORITY_LIM_MIN                          (0)
  #define CMI_0_PRIO_LIM_LOW_PRIORITY_LIM_MAX                          (65535) // 0x0000FFFF
  #define CMI_0_PRIO_LIM_LOW_PRIORITY_LIM_DEF                          (0x00000001)
  #define CMI_0_PRIO_LIM_LOW_PRIORITY_LIM_HSH                          (0x1001104C)

  #define CMI_0_PRIO_LIM_HIGH_PRIORITY_LIM_OFF                         (16)
  #define CMI_0_PRIO_LIM_HIGH_PRIORITY_LIM_WID                         (16)
  #define CMI_0_PRIO_LIM_HIGH_PRIORITY_LIM_MSK                         (0xFFFF0000)
  #define CMI_0_PRIO_LIM_HIGH_PRIORITY_LIM_MIN                         (0)
  #define CMI_0_PRIO_LIM_HIGH_PRIORITY_LIM_MAX                         (65535) // 0x0000FFFF
  #define CMI_0_PRIO_LIM_HIGH_PRIORITY_LIM_DEF                         (0x00000004)
  #define CMI_0_PRIO_LIM_HIGH_PRIORITY_LIM_HSH                         (0x1021104C)

#define CMF_0_TOPOLOGY_GLOBAL_CFG_0_REG                                (0x00011050)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_2LM_1LM_OFF                  ( 0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_2LM_1LM_WID                  ( 1)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_2LM_1LM_MSK                  (0x00000001)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_2LM_1LM_MIN                  (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_2LM_1LM_MAX                  (1) // 0x00000001
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_2LM_1LM_DEF                  (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_2LM_1LM_HSH                  (0x01011050)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_TMECC_ON_OFF                 ( 1)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_TMECC_ON_WID                 ( 1)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_TMECC_ON_MSK                 (0x00000002)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_TMECC_ON_MIN                 (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_TMECC_ON_MAX                 (1) // 0x00000001
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_TMECC_ON_DEF                 (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SEL_TMECC_ON_HSH                 (0x01031050)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_0_OFF                   ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_0_WID                   ( 6)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_0_MSK                   (0x000000FC)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_0_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_0_MAX                   (63) // 0x0000003F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_0_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_0_HSH                   (0x06051050)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_0_DISABLE_OFF              ( 8)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_0_DISABLE_WID              ( 1)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_0_DISABLE_MSK              (0x00000100)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_0_DISABLE_MIN              (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_0_DISABLE_MAX              (1) // 0x00000001
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_0_DISABLE_DEF              (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_0_DISABLE_HSH              (0x01111050)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_1_DISABLE_OFF              ( 9)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_1_DISABLE_WID              ( 1)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_1_DISABLE_MSK              (0x00000200)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_1_DISABLE_MIN              (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_1_DISABLE_MAX              (1) // 0x00000001
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_1_DISABLE_DEF              (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_SLICE_1_DISABLE_HSH              (0x01131050)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_1_OFF                   (10)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_1_WID                   (22)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_1_MSK                   (0xFFFFFC00)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_1_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_1_MAX                   (4194303) // 0x003FFFFF
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_1_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_0_Reserved_1_HSH                   (0x16151050)

#define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REG                                (0x00011054)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_0_OFF                   ( 0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_0_WID                   (10)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_0_MSK                   (0x000003FF)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_0_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_0_MAX                   (1023) // 0x000003FF
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_0_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_0_HSH                   (0x0A011054)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_AGENT_WR_RSP_OFF                 (10)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_AGENT_WR_RSP_WID                 ( 5)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_AGENT_WR_RSP_MSK                 (0x00007C00)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_AGENT_WR_RSP_MIN                 (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_AGENT_WR_RSP_MAX                 (31) // 0x0000001F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_AGENT_WR_RSP_DEF                 (0x00000003)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_AGENT_WR_RSP_HSH                 (0x05151054)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_1_OFF                   (15)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_1_WID                   ( 5)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_1_MSK                   (0x000F8000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_1_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_1_MAX                   (31) // 0x0000001F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_1_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_1_HSH                   (0x051F1054)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_1_OFF        (20)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_1_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_1_MSK        (0x00300000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_1_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_1_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_1_DEF        (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_1_HSH        (0x02291054)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_0_OFF        (22)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_0_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_0_MSK        (0x00C00000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_0_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_0_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_0_DEF        (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_REQUESTOR_MUX_DEMUX_0_HSH        (0x022D1054)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_1_OFF        (24)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_1_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_1_MSK        (0x03000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_1_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_1_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_1_DEF        (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_1_HSH        (0x02311054)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_0_OFF        (26)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_0_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_0_MSK        (0x0C000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_0_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_0_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_0_DEF        (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_RESPONDER_MUX_DEMUX_0_HSH        (0x02351054)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_2_OFF                   (28)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_2_WID                   ( 4)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_2_MSK                   (0xF0000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_2_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_2_MAX                   (15) // 0x0000000F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_2_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_1_Reserved_2_HSH                   (0x04391054)

#define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REG                                (0x00011058)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_0_OFF                   ( 0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_0_WID                   (10)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_0_MSK                   (0x000003FF)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_0_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_0_MAX                   (1023) // 0x000003FF
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_0_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_0_HSH                   (0x0A011058)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_AGENT_WR_RSP_OFF                 (10)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_AGENT_WR_RSP_WID                 ( 5)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_AGENT_WR_RSP_MSK                 (0x00007C00)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_AGENT_WR_RSP_MIN                 (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_AGENT_WR_RSP_MAX                 (31) // 0x0000001F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_AGENT_WR_RSP_DEF                 (0x0000000F)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_AGENT_WR_RSP_HSH                 (0x05151058)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_1_OFF                   (15)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_1_WID                   ( 5)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_1_MSK                   (0x000F8000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_1_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_1_MAX                   (31) // 0x0000001F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_1_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_1_HSH                   (0x051F1058)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_1_OFF        (20)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_1_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_1_MSK        (0x00300000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_1_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_1_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_1_DEF        (0x00000001)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_1_HSH        (0x02291058)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_0_OFF        (22)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_0_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_0_MSK        (0x00C00000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_0_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_0_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_0_DEF        (0x00000001)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_REQUESTOR_MUX_DEMUX_0_HSH        (0x022D1058)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_1_OFF        (24)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_1_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_1_MSK        (0x03000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_1_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_1_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_1_DEF        (0x00000001)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_1_HSH        (0x02311058)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_0_OFF        (26)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_0_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_0_MSK        (0x0C000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_0_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_0_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_0_DEF        (0x00000001)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_RESPONDER_MUX_DEMUX_0_HSH        (0x02351058)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_2_OFF                   (28)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_2_WID                   ( 4)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_2_MSK                   (0xF0000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_2_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_2_MAX                   (15) // 0x0000000F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_2_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_2_Reserved_2_HSH                   (0x04391058)

#define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REG                                (0x0001105C)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_0_OFF                   ( 0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_0_WID                   (10)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_0_MSK                   (0x000003FF)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_0_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_0_MAX                   (1023) // 0x000003FF
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_0_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_0_HSH                   (0x0A01105C)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_AGENT_WR_RSP_OFF                 (10)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_AGENT_WR_RSP_WID                 ( 5)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_AGENT_WR_RSP_MSK                 (0x00007C00)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_AGENT_WR_RSP_MIN                 (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_AGENT_WR_RSP_MAX                 (31) // 0x0000001F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_AGENT_WR_RSP_DEF                 (0x0000000F)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_AGENT_WR_RSP_HSH                 (0x0515105C)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_1_OFF                   (15)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_1_WID                   ( 5)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_1_MSK                   (0x000F8000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_1_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_1_MAX                   (31) // 0x0000001F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_1_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_1_HSH                   (0x051F105C)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_1_OFF        (20)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_1_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_1_MSK        (0x00300000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_1_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_1_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_1_DEF        (0x00000002)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_1_HSH        (0x0229105C)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_0_OFF        (22)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_0_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_0_MSK        (0x00C00000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_0_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_0_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_0_DEF        (0x00000002)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_REQUESTOR_MUX_DEMUX_0_HSH        (0x022D105C)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_1_OFF        (24)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_1_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_1_MSK        (0x03000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_1_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_1_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_1_DEF        (0x00000002)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_1_HSH        (0x0231105C)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_0_OFF        (26)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_0_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_0_MSK        (0x0C000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_0_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_0_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_0_DEF        (0x00000002)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_RESPONDER_MUX_DEMUX_0_HSH        (0x0235105C)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_2_OFF                   (28)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_2_WID                   ( 4)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_2_MSK                   (0xF0000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_2_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_2_MAX                   (15) // 0x0000000F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_2_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_3_Reserved_2_HSH                   (0x0439105C)

#define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REG                                (0x00011060)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_0_OFF                   ( 0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_0_WID                   (10)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_0_MSK                   (0x000003FF)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_0_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_0_MAX                   (1023) // 0x000003FF
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_0_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_0_HSH                   (0x0A011060)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_AGENT_WR_RSP_OFF                 (10)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_AGENT_WR_RSP_WID                 ( 5)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_AGENT_WR_RSP_MSK                 (0x00007C00)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_AGENT_WR_RSP_MIN                 (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_AGENT_WR_RSP_MAX                 (31) // 0x0000001F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_AGENT_WR_RSP_DEF                 (0x0000000F)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_AGENT_WR_RSP_HSH                 (0x05151060)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_1_OFF                   (15)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_1_WID                   ( 5)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_1_MSK                   (0x000F8000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_1_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_1_MAX                   (31) // 0x0000001F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_1_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_1_HSH                   (0x051F1060)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_1_OFF        (20)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_1_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_1_MSK        (0x00300000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_1_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_1_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_1_DEF        (0x00000003)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_1_HSH        (0x02291060)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_0_OFF        (22)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_0_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_0_MSK        (0x00C00000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_0_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_0_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_0_DEF        (0x00000003)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_REQUESTOR_MUX_DEMUX_0_HSH        (0x022D1060)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_1_OFF        (24)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_1_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_1_MSK        (0x03000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_1_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_1_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_1_DEF        (0x00000003)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_1_HSH        (0x02311060)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_0_OFF        (26)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_0_WID        ( 2)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_0_MSK        (0x0C000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_0_MIN        (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_0_MAX        (3) // 0x00000003
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_0_DEF        (0x00000003)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_RESPONDER_MUX_DEMUX_0_HSH        (0x02351060)

  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_2_OFF                   (28)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_2_WID                   ( 4)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_2_MSK                   (0xF0000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_2_MIN                   (0)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_2_MAX                   (15) // 0x0000000F
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_2_DEF                   (0x00000000)
  #define CMF_0_TOPOLOGY_GLOBAL_CFG_4_Reserved_2_HSH                   (0x04391060)

#define CMF_0_GLOBAL_CFG_1_REG                                         (0x00011068)

  #define CMF_0_GLOBAL_CFG_1_ISM_IDLE_TIME_OFF                         ( 0)
  #define CMF_0_GLOBAL_CFG_1_ISM_IDLE_TIME_WID                         ( 8)
  #define CMF_0_GLOBAL_CFG_1_ISM_IDLE_TIME_MSK                         (0x000000FF)
  #define CMF_0_GLOBAL_CFG_1_ISM_IDLE_TIME_MIN                         (0)
  #define CMF_0_GLOBAL_CFG_1_ISM_IDLE_TIME_MAX                         (255) // 0x000000FF
  #define CMF_0_GLOBAL_CFG_1_ISM_IDLE_TIME_DEF                         (0x00000010)
  #define CMF_0_GLOBAL_CFG_1_ISM_IDLE_TIME_HSH                         (0x08011068)

  #define CMF_0_GLOBAL_CFG_1_FIXED_WINDOW_OFF                          ( 8)
  #define CMF_0_GLOBAL_CFG_1_FIXED_WINDOW_WID                          ( 1)
  #define CMF_0_GLOBAL_CFG_1_FIXED_WINDOW_MSK                          (0x00000100)
  #define CMF_0_GLOBAL_CFG_1_FIXED_WINDOW_MIN                          (0)
  #define CMF_0_GLOBAL_CFG_1_FIXED_WINDOW_MAX                          (1) // 0x00000001
  #define CMF_0_GLOBAL_CFG_1_FIXED_WINDOW_DEF                          (0x00000000)
  #define CMF_0_GLOBAL_CFG_1_FIXED_WINDOW_HSH                          (0x01111068)

  #define CMF_0_GLOBAL_CFG_1_CLK_GATE_EN_OFF                           ( 9)
  #define CMF_0_GLOBAL_CFG_1_CLK_GATE_EN_WID                           ( 1)
  #define CMF_0_GLOBAL_CFG_1_CLK_GATE_EN_MSK                           (0x00000200)
  #define CMF_0_GLOBAL_CFG_1_CLK_GATE_EN_MIN                           (0)
  #define CMF_0_GLOBAL_CFG_1_CLK_GATE_EN_MAX                           (1) // 0x00000001
  #define CMF_0_GLOBAL_CFG_1_CLK_GATE_EN_DEF                           (0x00000001)
  #define CMF_0_GLOBAL_CFG_1_CLK_GATE_EN_HSH                           (0x01131068)

  #define CMF_0_GLOBAL_CFG_1_FORCE_ISM_ACTIVE_OFF                      (10)
  #define CMF_0_GLOBAL_CFG_1_FORCE_ISM_ACTIVE_WID                      ( 1)
  #define CMF_0_GLOBAL_CFG_1_FORCE_ISM_ACTIVE_MSK                      (0x00000400)
  #define CMF_0_GLOBAL_CFG_1_FORCE_ISM_ACTIVE_MIN                      (0)
  #define CMF_0_GLOBAL_CFG_1_FORCE_ISM_ACTIVE_MAX                      (1) // 0x00000001
  #define CMF_0_GLOBAL_CFG_1_FORCE_ISM_ACTIVE_DEF                      (0x00000000)
  #define CMF_0_GLOBAL_CFG_1_FORCE_ISM_ACTIVE_HSH                      (0x01151068)

  #define CMF_0_GLOBAL_CFG_1_BYPASS_EN_OFF                             (11)
  #define CMF_0_GLOBAL_CFG_1_BYPASS_EN_WID                             ( 1)
  #define CMF_0_GLOBAL_CFG_1_BYPASS_EN_MSK                             (0x00000800)
  #define CMF_0_GLOBAL_CFG_1_BYPASS_EN_MIN                             (0)
  #define CMF_0_GLOBAL_CFG_1_BYPASS_EN_MAX                             (1) // 0x00000001
  #define CMF_0_GLOBAL_CFG_1_BYPASS_EN_DEF                             (0x00000001)
  #define CMF_0_GLOBAL_CFG_1_BYPASS_EN_HSH                             (0x01171068)

  #define CMF_0_GLOBAL_CFG_1_DIS_PERF_COUNTERS_OFF                     (12)
  #define CMF_0_GLOBAL_CFG_1_DIS_PERF_COUNTERS_WID                     ( 1)
  #define CMF_0_GLOBAL_CFG_1_DIS_PERF_COUNTERS_MSK                     (0x00001000)
  #define CMF_0_GLOBAL_CFG_1_DIS_PERF_COUNTERS_MIN                     (0)
  #define CMF_0_GLOBAL_CFG_1_DIS_PERF_COUNTERS_MAX                     (1) // 0x00000001
  #define CMF_0_GLOBAL_CFG_1_DIS_PERF_COUNTERS_DEF                     (0x00000000)
  #define CMF_0_GLOBAL_CFG_1_DIS_PERF_COUNTERS_HSH                     (0x01191068)

  #define CMF_0_GLOBAL_CFG_1_DIS_GLBDRV_GATING_OFF                     (13)
  #define CMF_0_GLOBAL_CFG_1_DIS_GLBDRV_GATING_WID                     ( 1)
  #define CMF_0_GLOBAL_CFG_1_DIS_GLBDRV_GATING_MSK                     (0x00002000)
  #define CMF_0_GLOBAL_CFG_1_DIS_GLBDRV_GATING_MIN                     (0)
  #define CMF_0_GLOBAL_CFG_1_DIS_GLBDRV_GATING_MAX                     (1) // 0x00000001
  #define CMF_0_GLOBAL_CFG_1_DIS_GLBDRV_GATING_DEF                     (0x00000000)
  #define CMF_0_GLOBAL_CFG_1_DIS_GLBDRV_GATING_HSH                     (0x011B1068)

  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_PRE_VALID_OFF                (14)
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_PRE_VALID_WID                ( 1)
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_PRE_VALID_MSK                (0x00004000)
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_PRE_VALID_MIN                (0)
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_PRE_VALID_MAX                (1) // 0x00000001
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_PRE_VALID_DEF                (0x00000000)
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_PRE_VALID_HSH                (0x011D1068)

  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_BYPASS_PRE_VALID_OFF         (15)
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_BYPASS_PRE_VALID_WID         ( 1)
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_BYPASS_PRE_VALID_MSK         (0x00008000)
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_BYPASS_PRE_VALID_MIN         (0)
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_BYPASS_PRE_VALID_MAX         (1) // 0x00000001
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_BYPASS_PRE_VALID_DEF         (0x00000000)
  #define CMF_0_GLOBAL_CFG_1_DIS_RSP_PORT_BYPASS_PRE_VALID_HSH         (0x011F1068)

  #define CMF_0_GLOBAL_CFG_1_RESERVED_OFF                              (16)
  #define CMF_0_GLOBAL_CFG_1_RESERVED_WID                              (16)
  #define CMF_0_GLOBAL_CFG_1_RESERVED_MSK                              (0xFFFF0000)
  #define CMF_0_GLOBAL_CFG_1_RESERVED_MIN                              (0)
  #define CMF_0_GLOBAL_CFG_1_RESERVED_MAX                              (65535) // 0x0000FFFF
  #define CMF_0_GLOBAL_CFG_1_RESERVED_DEF                              (0x00000000)
  #define CMF_0_GLOBAL_CFG_1_RESERVED_HSH                              (0x10211068)

#define CMI_0_IOSF_SB_EP_CTRL_REG                                      (0x0001106C)

  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEDEF_OFF                  ( 0)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEDEF_WID                  ( 1)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEDEF_MSK                  (0x00000001)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEDEF_MIN                  (0)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEDEF_MAX                  (1) // 0x00000001
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEDEF_DEF                  (0x00000000)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEDEF_HSH                  (0x0101106C)

  #define CMI_0_IOSF_SB_EP_CTRL_RSVD0_OFF                              ( 1)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD0_WID                              ( 7)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD0_MSK                              (0x000000FE)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD0_MIN                              (0)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD0_MAX                              (127) // 0x0000007F
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD0_DEF                              (0x00000000)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD0_HSH                              (0x0703106C)

  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEN_OFF                    ( 8)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEN_WID                    ( 1)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEN_MSK                    (0x00000100)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEN_MIN                    (0)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEN_MAX                    (1) // 0x00000001
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEN_DEF                    (0x00000001)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_CLKGATEN_HSH                    (0x0111106C)

  #define CMI_0_IOSF_SB_EP_CTRL_RSVD1_OFF                              ( 9)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD1_WID                              ( 7)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD1_MSK                              (0x0000FE00)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD1_MIN                              (0)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD1_MAX                              (127) // 0x0000007F
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD1_DEF                              (0x00000000)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD1_HSH                              (0x0713106C)

  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_IDLECNT_OFF                     (16)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_IDLECNT_WID                     ( 8)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_IDLECNT_MSK                     (0x00FF0000)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_IDLECNT_MIN                     (0)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_IDLECNT_MAX                     (255) // 0x000000FF
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_IDLECNT_DEF                     (0x00000010)
  #define CMI_0_IOSF_SB_EP_CTRL_CGCTRL_IDLECNT_HSH                     (0x0821106C)

  #define CMI_0_IOSF_SB_EP_CTRL_RSVD2_OFF                              (24)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD2_WID                              ( 8)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD2_MSK                              (0xFF000000)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD2_MIN                              (0)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD2_MAX                              (255) // 0x000000FF
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD2_DEF                              (0x00000000)
  #define CMI_0_IOSF_SB_EP_CTRL_RSVD2_HSH                              (0x0831106C)

#define CMI_0_CLK_GATE_EN_0_REG                                        (0x00011070)

  #define CMI_0_CLK_GATE_EN_0_CLK_GATE_EN_OFF                          ( 0)
  #define CMI_0_CLK_GATE_EN_0_CLK_GATE_EN_WID                          (32)
  #define CMI_0_CLK_GATE_EN_0_CLK_GATE_EN_MSK                          (0xFFFFFFFF)
  #define CMI_0_CLK_GATE_EN_0_CLK_GATE_EN_MIN                          (0)
  #define CMI_0_CLK_GATE_EN_0_CLK_GATE_EN_MAX                          (4294967295) // 0xFFFFFFFF
  #define CMI_0_CLK_GATE_EN_0_CLK_GATE_EN_DEF                          (0xFFFFFFFF)
  #define CMI_0_CLK_GATE_EN_0_CLK_GATE_EN_HSH                          (0x20011070)

#define CMI_0_CLK_GATE_EN_1_REG                                        (0x00011074)
//Duplicate of CMI_0_CLK_GATE_EN_0_REG

#define CMF_0_RSVD0_REG                                                (0x00011078)

  #define CMF_0_RSVD0_RESERVED_OFF                                     ( 0)
  #define CMF_0_RSVD0_RESERVED_WID                                     ( 6)
  #define CMF_0_RSVD0_RESERVED_MSK                                     (0x0000003F)
  #define CMF_0_RSVD0_RESERVED_MIN                                     (0)
  #define CMF_0_RSVD0_RESERVED_MAX                                     (63) // 0x0000003F
  #define CMF_0_RSVD0_RESERVED_DEF                                     (0x00000000)
  #define CMF_0_RSVD0_RESERVED_HSH                                     (0x06011078)

  #define CMF_0_RSVD0_vc1wr_window_OFF                                 ( 6)
  #define CMF_0_RSVD0_vc1wr_window_WID                                 ( 8)
  #define CMF_0_RSVD0_vc1wr_window_MSK                                 (0x00003FC0)
  #define CMF_0_RSVD0_vc1wr_window_MIN                                 (0)
  #define CMF_0_RSVD0_vc1wr_window_MAX                                 (255) // 0x000000FF
  #define CMF_0_RSVD0_vc1wr_window_DEF                                 (0x0000000F)
  #define CMF_0_RSVD0_vc1wr_window_HSH                                 (0x080D1078)

  #define CMF_0_RSVD0_update_vc1wr_window_OFF                          (14)
  #define CMF_0_RSVD0_update_vc1wr_window_WID                          ( 1)
  #define CMF_0_RSVD0_update_vc1wr_window_MSK                          (0x00004000)
  #define CMF_0_RSVD0_update_vc1wr_window_MIN                          (0)
  #define CMF_0_RSVD0_update_vc1wr_window_MAX                          (1) // 0x00000001
  #define CMF_0_RSVD0_update_vc1wr_window_DEF                          (0x00000000)
  #define CMF_0_RSVD0_update_vc1wr_window_HSH                          (0x011D1078)

  #define CMF_0_RSVD0_vc1wr_window_fix_disable_OFF                     (15)
  #define CMF_0_RSVD0_vc1wr_window_fix_disable_WID                     ( 1)
  #define CMF_0_RSVD0_vc1wr_window_fix_disable_MSK                     (0x00008000)
  #define CMF_0_RSVD0_vc1wr_window_fix_disable_MIN                     (0)
  #define CMF_0_RSVD0_vc1wr_window_fix_disable_MAX                     (1) // 0x00000001
  #define CMF_0_RSVD0_vc1wr_window_fix_disable_DEF                     (0x00000000)
  #define CMF_0_RSVD0_vc1wr_window_fix_disable_HSH                     (0x011F1078)

  #define CMF_0_RSVD0_vc0wr_const_prio_fix_en_OFF                      (16)
  #define CMF_0_RSVD0_vc0wr_const_prio_fix_en_WID                      ( 1)
  #define CMF_0_RSVD0_vc0wr_const_prio_fix_en_MSK                      (0x00010000)
  #define CMF_0_RSVD0_vc0wr_const_prio_fix_en_MIN                      (0)
  #define CMF_0_RSVD0_vc0wr_const_prio_fix_en_MAX                      (1) // 0x00000001
  #define CMF_0_RSVD0_vc0wr_const_prio_fix_en_DEF                      (0x00000000)
  #define CMF_0_RSVD0_vc0wr_const_prio_fix_en_HSH                      (0x01211078)

  #define CMF_0_RSVD0_gldv_gating_disable_OFF                          (17)
  #define CMF_0_RSVD0_gldv_gating_disable_WID                          ( 1)
  #define CMF_0_RSVD0_gldv_gating_disable_MSK                          (0x00020000)
  #define CMF_0_RSVD0_gldv_gating_disable_MIN                          (0)
  #define CMF_0_RSVD0_gldv_gating_disable_MAX                          (1) // 0x00000001
  #define CMF_0_RSVD0_gldv_gating_disable_DEF                          (0x00000000)
  #define CMF_0_RSVD0_gldv_gating_disable_HSH                          (0x01231078)

  #define CMF_0_RSVD0_iop_ep_req_ch0_rd_park_size_OFF                  (18)
  #define CMF_0_RSVD0_iop_ep_req_ch0_rd_park_size_WID                  ( 4)
  #define CMF_0_RSVD0_iop_ep_req_ch0_rd_park_size_MSK                  (0x003C0000)
  #define CMF_0_RSVD0_iop_ep_req_ch0_rd_park_size_MIN                  (0)
  #define CMF_0_RSVD0_iop_ep_req_ch0_rd_park_size_MAX                  (15) // 0x0000000F
  #define CMF_0_RSVD0_iop_ep_req_ch0_rd_park_size_DEF                  (0x00000000)
  #define CMF_0_RSVD0_iop_ep_req_ch0_rd_park_size_HSH                  (0x04251078)

  #define CMF_0_RSVD0_iop_ep_req_ch0_wr_park_size_OFF                  (22)
  #define CMF_0_RSVD0_iop_ep_req_ch0_wr_park_size_WID                  ( 4)
  #define CMF_0_RSVD0_iop_ep_req_ch0_wr_park_size_MSK                  (0x03C00000)
  #define CMF_0_RSVD0_iop_ep_req_ch0_wr_park_size_MIN                  (0)
  #define CMF_0_RSVD0_iop_ep_req_ch0_wr_park_size_MAX                  (15) // 0x0000000F
  #define CMF_0_RSVD0_iop_ep_req_ch0_wr_park_size_DEF                  (0x00000000)
  #define CMF_0_RSVD0_iop_ep_req_ch0_wr_park_size_HSH                  (0x042D1078)

  #define CMF_0_RSVD0_RESERVED1_OFF                                    (26)
  #define CMF_0_RSVD0_RESERVED1_WID                                    ( 6)
  #define CMF_0_RSVD0_RESERVED1_MSK                                    (0xFC000000)
  #define CMF_0_RSVD0_RESERVED1_MIN                                    (0)
  #define CMF_0_RSVD0_RESERVED1_MAX                                    (63) // 0x0000003F
  #define CMF_0_RSVD0_RESERVED1_DEF                                    (0x00000000)
  #define CMF_0_RSVD0_RESERVED1_HSH                                    (0x06351078)

#define CMF_0_RSVD1_REG                                                (0x0001107C)

  #define CMF_0_RSVD1_RESERVED_OFF                                     ( 0)
  #define CMF_0_RSVD1_RESERVED_WID                                     (32)
  #define CMF_0_RSVD1_RESERVED_MSK                                     (0xFFFFFFFF)
  #define CMF_0_RSVD1_RESERVED_MIN                                     (0)
  #define CMF_0_RSVD1_RESERVED_MAX                                     (4294967295) // 0xFFFFFFFF
  #define CMF_0_RSVD1_RESERVED_DEF                                     (0xFFFFFFFF)
  #define CMF_0_RSVD1_RESERVED_HSH                                     (0x2001107C)

#define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG                (0x00011080)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_PARK_SIZE_OFF ( 0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_PARK_SIZE_WID ( 4)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_PARK_SIZE_MSK (0x0000000F)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_PARK_SIZE_MIN (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_PARK_SIZE_MAX (15) // 0x0000000F
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_PARK_SIZE_DEF (0x00000003)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_PARK_SIZE_HSH (0x04011080)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED0_OFF    ( 4)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED0_WID    ( 4)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED0_MSK    (0x000000F0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED0_MIN    (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED0_MAX    (15) // 0x0000000F
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED0_DEF    (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED0_HSH    (0x04091080)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_PARK_SIZE_OFF ( 8)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_PARK_SIZE_WID ( 4)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_PARK_SIZE_MSK (0x00000F00)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_PARK_SIZE_MIN (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_PARK_SIZE_MAX (15) // 0x0000000F
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_PARK_SIZE_DEF (0x00000003)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_PARK_SIZE_HSH (0x04111080)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED1_OFF    (12)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED1_WID    ( 4)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED1_MSK    (0x0000F000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED1_MIN    (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED1_MAX    (15) // 0x0000000F
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED1_DEF    (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_RESERVED1_HSH    (0x04191080)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_MAX_CREDIT_OFF (16)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_MAX_CREDIT_WID ( 8)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_MAX_CREDIT_MSK (0x00FF0000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_MAX_CREDIT_MIN (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_MAX_CREDIT_MAX (255) // 0x000000FF
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_MAX_CREDIT_DEF (0x00000002)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_WR_MAX_CREDIT_HSH (0x08211080)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_MAX_CREDIT_OFF (24)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_MAX_CREDIT_WID ( 8)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_MAX_CREDIT_MSK (0xFF000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_MAX_CREDIT_MIN (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_MAX_CREDIT_MAX (255) // 0x000000FF
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_MAX_CREDIT_DEF (0x00000003)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REQ_RD_MAX_CREDIT_HSH (0x08311080)

#define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_1_REG                (0x00011084)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG                (0x00011088)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_PARK_SIZE_OFF ( 0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_PARK_SIZE_WID ( 4)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_PARK_SIZE_MSK (0x0000000F)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_PARK_SIZE_MIN (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_PARK_SIZE_MAX (15) // 0x0000000F
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_PARK_SIZE_DEF (0x00000003)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_PARK_SIZE_HSH (0x04011088)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED0_OFF    ( 4)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED0_WID    ( 4)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED0_MSK    (0x000000F0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED0_MIN    (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED0_MAX    (15) // 0x0000000F
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED0_DEF    (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED0_HSH    (0x04091088)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_PARK_SIZE_OFF ( 8)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_PARK_SIZE_WID ( 4)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_PARK_SIZE_MSK (0x00000F00)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_PARK_SIZE_MIN (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_PARK_SIZE_MAX (15) // 0x0000000F
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_PARK_SIZE_DEF (0x00000003)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_PARK_SIZE_HSH (0x04111088)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED1_OFF    (12)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED1_WID    ( 4)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED1_MSK    (0x0000F000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED1_MIN    (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED1_MAX    (15) // 0x0000000F
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED1_DEF    (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_RESERVED1_HSH    (0x04191088)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_MAX_CREDIT_OFF (16)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_MAX_CREDIT_WID ( 8)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_MAX_CREDIT_MSK (0x00FF0000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_MAX_CREDIT_MIN (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_MAX_CREDIT_MAX (255) // 0x000000FF
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_MAX_CREDIT_DEF (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_WR_MAX_CREDIT_HSH (0x08211088)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_MAX_CREDIT_OFF (24)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_MAX_CREDIT_WID ( 8)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_MAX_CREDIT_MSK (0xFF000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_MAX_CREDIT_MIN (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_MAX_CREDIT_MAX (255) // 0x000000FF
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_MAX_CREDIT_DEF (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REQ_RD_MAX_CREDIT_HSH (0x08311088)

#define CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_3_REG                (0x0001108C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REG                          (0x00011090)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_WR_QUEUEDEPTH_OFF      ( 0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_WR_QUEUEDEPTH_WID      ( 8)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_WR_QUEUEDEPTH_MSK      (0x000000FF)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_WR_QUEUEDEPTH_MIN      (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_WR_QUEUEDEPTH_MAX      (255) // 0x000000FF
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_WR_QUEUEDEPTH_DEF      (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_WR_QUEUEDEPTH_HSH      (0x08011090)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_RD_QUEUEDEPTH_OFF      ( 8)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_RD_QUEUEDEPTH_WID      ( 8)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_RD_QUEUEDEPTH_MSK      (0x0000FF00)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_RD_QUEUEDEPTH_MIN      (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_RD_QUEUEDEPTH_MAX      (255) // 0x000000FF
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_RD_QUEUEDEPTH_DEF      (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REQ_RD_QUEUEDEPTH_HSH      (0x08111090)

  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_RESERVED1_OFF              (16)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_RESERVED1_WID              (16)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_RESERVED1_MSK              (0xFFFF0000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_RESERVED1_MIN              (0)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_RESERVED1_MAX              (65535) // 0x0000FFFF
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_RESERVED1_DEF              (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_RESERVED1_HSH              (0x10211090)

#define CMI_0_REQUEST_PORT_0_STALL_REG                                 (0x00011094)

  #define CMI_0_REQUEST_PORT_0_STALL_REQ_STALL_OFF                     ( 0)
  #define CMI_0_REQUEST_PORT_0_STALL_REQ_STALL_WID                     ( 1)
  #define CMI_0_REQUEST_PORT_0_STALL_REQ_STALL_MSK                     (0x00000001)
  #define CMI_0_REQUEST_PORT_0_STALL_REQ_STALL_MIN                     (0)
  #define CMI_0_REQUEST_PORT_0_STALL_REQ_STALL_MAX                     (1) // 0x00000001
  #define CMI_0_REQUEST_PORT_0_STALL_REQ_STALL_DEF                     (0x00000000)
  #define CMI_0_REQUEST_PORT_0_STALL_REQ_STALL_HSH                     (0x01011094)

  #define CMI_0_REQUEST_PORT_0_STALL_RSVD0_OFF                         ( 1)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD0_WID                         ( 4)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD0_MSK                         (0x0000001E)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD0_MIN                         (0)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD0_MAX                         (15) // 0x0000000F
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD0_DEF                         (0x00000000)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD0_HSH                         (0x04031094)

  #define CMI_0_REQUEST_PORT_0_STALL_RSVD1_OFF                         ( 5)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD1_WID                         ( 3)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD1_MSK                         (0x000000E0)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD1_MIN                         (0)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD1_MAX                         (7) // 0x00000007
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD1_DEF                         (0x00000000)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD1_HSH                         (0x030B1094)

  #define CMI_0_REQUEST_PORT_0_STALL_UNSTALL_COUNTER_VALUE_OFF         ( 8)
  #define CMI_0_REQUEST_PORT_0_STALL_UNSTALL_COUNTER_VALUE_WID         (16)
  #define CMI_0_REQUEST_PORT_0_STALL_UNSTALL_COUNTER_VALUE_MSK         (0x00FFFF00)
  #define CMI_0_REQUEST_PORT_0_STALL_UNSTALL_COUNTER_VALUE_MIN         (0)
  #define CMI_0_REQUEST_PORT_0_STALL_UNSTALL_COUNTER_VALUE_MAX         (65535) // 0x0000FFFF
  #define CMI_0_REQUEST_PORT_0_STALL_UNSTALL_COUNTER_VALUE_DEF         (0x00000000)
  #define CMI_0_REQUEST_PORT_0_STALL_UNSTALL_COUNTER_VALUE_HSH         (0x10111094)

  #define CMI_0_REQUEST_PORT_0_STALL_RSVD2_OFF                         (24)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD2_WID                         ( 8)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD2_MSK                         (0xFF000000)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD2_MIN                         (0)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD2_MAX                         (255) // 0x000000FF
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD2_DEF                         (0x00000000)
  #define CMI_0_REQUEST_PORT_0_STALL_RSVD2_HSH                         (0x08311094)

#define CMI_0_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_0_REG                (0x00011098)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_0_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_1_REG                (0x0001109C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_0_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_2_REG                (0x000110A0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_0_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_3_REG                (0x000110A4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_0_REQUEST_PORT_1_REQ_CONFIG_1_REG                          (0x000110A8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REG

#define CMI_0_REQUEST_PORT_1_STALL_REG                                 (0x000110AC)
//Duplicate of CMI_0_REQUEST_PORT_0_STALL_REG

#define CMI_0_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_0_REG                (0x000110B0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_0_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_1_REG                (0x000110B4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_0_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_2_REG                (0x000110B8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_0_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_3_REG                (0x000110BC)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_0_REQUEST_PORT_2_REQ_CONFIG_1_REG                          (0x000110C0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REG

#define CMI_0_REQUEST_PORT_2_STALL_REG                                 (0x000110C4)
//Duplicate of CMI_0_REQUEST_PORT_0_STALL_REG

#define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG                (0x000110C8)

  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_WR_RSP_PARK_SIZE_OFF ( 0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_WR_RSP_PARK_SIZE_WID ( 4)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_WR_RSP_PARK_SIZE_MSK (0x0000000F)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_WR_RSP_PARK_SIZE_MIN (0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_WR_RSP_PARK_SIZE_MAX (15) // 0x0000000F
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_WR_RSP_PARK_SIZE_DEF (0x00000003)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_WR_RSP_PARK_SIZE_HSH (0x040110C8)

  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED0_OFF    ( 4)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED0_WID    ( 4)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED0_MSK    (0x000000F0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED0_MIN    (0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED0_MAX    (15) // 0x0000000F
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED0_DEF    (0x00000000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED0_HSH    (0x040910C8)

  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_PARK_SIZE_OFF ( 8)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_PARK_SIZE_WID ( 4)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_PARK_SIZE_MSK (0x00000F00)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_PARK_SIZE_MIN (0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_PARK_SIZE_MAX (15) // 0x0000000F
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_PARK_SIZE_DEF (0x00000003)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_PARK_SIZE_HSH (0x041110C8)

  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED1_OFF    (12)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED1_WID    ( 4)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED1_MSK    (0x0000F000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED1_MIN    (0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED1_MAX    (15) // 0x0000000F
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED1_DEF    (0x00000000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RESERVED1_HSH    (0x041910C8)

  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RSP_MAX_CREDIT_OFF (16)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RSP_MAX_CREDIT_WID ( 8)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RSP_MAX_CREDIT_MSK (0x00FF0000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RSP_MAX_CREDIT_MIN (0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RSP_MAX_CREDIT_MAX (255) // 0x000000FF
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RSP_MAX_CREDIT_DEF (0x00000000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RSP_MAX_CREDIT_HSH (0x082110C8)

  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_MAX_CREDIT_OFF (24)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_MAX_CREDIT_WID ( 8)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_MAX_CREDIT_MSK (0xFF000000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_MAX_CREDIT_MIN (0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_MAX_CREDIT_MAX (255) // 0x000000FF
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_MAX_CREDIT_DEF (0x00000000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_RD_CPL_MAX_CREDIT_HSH (0x083110C8)

#define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_1_REG                (0x000110CC)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_2_REG                (0x000110D0)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_3_REG                (0x000110D4)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REG                          (0x000110D8)

  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RD_CPL_QUEUEDEPTH_OFF      ( 0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RD_CPL_QUEUEDEPTH_WID      ( 8)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RD_CPL_QUEUEDEPTH_MSK      (0x000000FF)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RD_CPL_QUEUEDEPTH_MIN      (0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RD_CPL_QUEUEDEPTH_MAX      (255) // 0x000000FF
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RD_CPL_QUEUEDEPTH_DEF      (0x00000002)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RD_CPL_QUEUEDEPTH_HSH      (0x080110D8)

  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RSP_QUEUEDEPTH_OFF         ( 8)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RSP_QUEUEDEPTH_WID         ( 8)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RSP_QUEUEDEPTH_MSK         (0x0000FF00)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RSP_QUEUEDEPTH_MIN         (0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RSP_QUEUEDEPTH_MAX         (255) // 0x000000FF
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RSP_QUEUEDEPTH_DEF         (0x00000003)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RSP_QUEUEDEPTH_HSH         (0x081110D8)

  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REQ_FAST_WAKE_OFF          (16)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REQ_FAST_WAKE_WID          ( 1)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REQ_FAST_WAKE_MSK          (0x00010000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REQ_FAST_WAKE_MIN          (0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REQ_FAST_WAKE_MAX          (1) // 0x00000001
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REQ_FAST_WAKE_DEF          (0x00000000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REQ_FAST_WAKE_HSH          (0x012110D8)

  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RESERVED1_OFF              (17)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RESERVED1_WID              (15)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RESERVED1_MSK              (0xFFFE0000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RESERVED1_MIN              (0)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RESERVED1_MAX              (32767) // 0x00007FFF
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RESERVED1_DEF              (0x00000000)
  #define CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_RESERVED1_HSH              (0x0F2310D8)

#define CMI_0_RESPOND_PORT_0_STALL_REG                                 (0x000110DC)

  #define CMI_0_RESPOND_PORT_0_STALL_RSVD0_OFF                         ( 0)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD0_WID                         ( 4)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD0_MSK                         (0x0000000F)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD0_MIN                         (0)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD0_MAX                         (15) // 0x0000000F
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD0_DEF                         (0x00000000)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD0_HSH                         (0x040110DC)

  #define CMI_0_RESPOND_PORT_0_STALL_RSP_RD_CPL_STALL_OFF              ( 4)
  #define CMI_0_RESPOND_PORT_0_STALL_RSP_RD_CPL_STALL_WID              ( 1)
  #define CMI_0_RESPOND_PORT_0_STALL_RSP_RD_CPL_STALL_MSK              (0x00000010)
  #define CMI_0_RESPOND_PORT_0_STALL_RSP_RD_CPL_STALL_MIN              (0)
  #define CMI_0_RESPOND_PORT_0_STALL_RSP_RD_CPL_STALL_MAX              (1) // 0x00000001
  #define CMI_0_RESPOND_PORT_0_STALL_RSP_RD_CPL_STALL_DEF              (0x00000000)
  #define CMI_0_RESPOND_PORT_0_STALL_RSP_RD_CPL_STALL_HSH              (0x010910DC)

  #define CMI_0_RESPOND_PORT_0_STALL_RSVD1_OFF                         ( 5)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD1_WID                         ( 3)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD1_MSK                         (0x000000E0)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD1_MIN                         (0)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD1_MAX                         (7) // 0x00000007
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD1_DEF                         (0x00000000)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD1_HSH                         (0x030B10DC)

  #define CMI_0_RESPOND_PORT_0_STALL_UNSTALL_COUNTER_VALUE_OFF         ( 8)
  #define CMI_0_RESPOND_PORT_0_STALL_UNSTALL_COUNTER_VALUE_WID         (16)
  #define CMI_0_RESPOND_PORT_0_STALL_UNSTALL_COUNTER_VALUE_MSK         (0x00FFFF00)
  #define CMI_0_RESPOND_PORT_0_STALL_UNSTALL_COUNTER_VALUE_MIN         (0)
  #define CMI_0_RESPOND_PORT_0_STALL_UNSTALL_COUNTER_VALUE_MAX         (65535) // 0x0000FFFF
  #define CMI_0_RESPOND_PORT_0_STALL_UNSTALL_COUNTER_VALUE_DEF         (0x00000000)
  #define CMI_0_RESPOND_PORT_0_STALL_UNSTALL_COUNTER_VALUE_HSH         (0x101110DC)

  #define CMI_0_RESPOND_PORT_0_STALL_RSVD2_OFF                         (24)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD2_WID                         ( 8)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD2_MSK                         (0xFF000000)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD2_MIN                         (0)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD2_MAX                         (255) // 0x000000FF
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD2_DEF                         (0x00000000)
  #define CMI_0_RESPOND_PORT_0_STALL_RSVD2_HSH                         (0x083110DC)

#define CMI_0_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_0_REG                (0x000110E0)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_1_REG                (0x000110E4)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_2_REG                (0x000110E8)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_3_REG                (0x000110EC)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_CPL_CONFIG_1_REG                          (0x000110F0)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REG

#define CMI_0_RESPOND_PORT_1_STALL_REG                                 (0x000110F4)
//Duplicate of CMI_0_RESPOND_PORT_0_STALL_REG

#define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG                  (0x00011100)

  #define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_READ_COUNTER_OFF   ( 0)
  #define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_READ_COUNTER_WID   (64)
  #define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_READ_COUNTER_MSK   (0xFFFFFFFFFFFFFFFFULL)
  #define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_READ_COUNTER_MIN   (0)
  #define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_READ_COUNTER_MAX   (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_READ_COUNTER_DEF   (0x00000000)
  #define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_READ_COUNTER_HSH   (0x40011100)

#define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_1_REG                  (0x00011108)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_2_REG                  (0x00011110)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_3_REG                  (0x00011118)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG                  (0x00011120)

  #define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_WRITE_COUNTER_OFF  ( 0)
  #define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_WRITE_COUNTER_WID  (64)
  #define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_WRITE_COUNTER_MSK  (0xFFFFFFFFFFFFFFFFULL)
  #define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_WRITE_COUNTER_MIN  (0)
  #define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_WRITE_COUNTER_MAX  (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_WRITE_COUNTER_DEF  (0x00000000)
  #define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_WRITE_COUNTER_HSH  (0x40011120)

#define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_1_REG                  (0x00011128)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_2_REG                  (0x00011130)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_3_REG                  (0x00011138)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_RD_COUNTER_CHANNEL_0_REG                  (0x00011140)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_RD_COUNTER_CHANNEL_1_REG                  (0x00011148)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_RD_COUNTER_CHANNEL_2_REG                  (0x00011150)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_RD_COUNTER_CHANNEL_3_REG                  (0x00011158)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_WR_COUNTER_CHANNEL_0_REG                  (0x00011160)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_WR_COUNTER_CHANNEL_1_REG                  (0x00011168)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_WR_COUNTER_CHANNEL_2_REG                  (0x00011170)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_0_RESPOND_PORT_1_WR_COUNTER_CHANNEL_3_REG                  (0x00011178)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_0_REQ_L3_ARB_CONFIG_REG                                    (0x00011180)

  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_HiPri_Park_Size_OFF         ( 0)
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_HiPri_Park_Size_WID         ( 4)
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_HiPri_Park_Size_MSK         (0x0000000F)
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_HiPri_Park_Size_MIN         (0)
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_HiPri_Park_Size_MAX         (15) // 0x0000000F
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_HiPri_Park_Size_DEF         (0x00000008)
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_HiPri_Park_Size_HSH         (0x04011180)

  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_LoPri_Park_Size_OFF         ( 4)
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_LoPri_Park_Size_WID         ( 4)
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_LoPri_Park_Size_MSK         (0x000000F0)
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_LoPri_Park_Size_MIN         (0)
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_LoPri_Park_Size_MAX         (15) // 0x0000000F
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_LoPri_Park_Size_DEF         (0x00000002)
  #define CMI_0_REQ_L3_ARB_CONFIG_NonVC1Wr_LoPri_Park_Size_HSH         (0x04091180)

  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_HiPri_Park_Size_OFF            ( 8)
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_HiPri_Park_Size_WID            ( 4)
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_HiPri_Park_Size_MSK            (0x00000F00)
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_HiPri_Park_Size_MIN            (0)
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_HiPri_Park_Size_MAX            (15) // 0x0000000F
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_HiPri_Park_Size_DEF            (0x00000008)
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_HiPri_Park_Size_HSH            (0x04111180)

  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_LoPri_Park_Size_OFF            (12)
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_LoPri_Park_Size_WID            ( 4)
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_LoPri_Park_Size_MSK            (0x0000F000)
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_LoPri_Park_Size_MIN            (0)
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_LoPri_Park_Size_MAX            (15) // 0x0000000F
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_LoPri_Park_Size_DEF            (0x00000002)
  #define CMI_0_REQ_L3_ARB_CONFIG_VC1Wr_LoPri_Park_Size_HSH            (0x04191180)

  #define CMI_0_REQ_L3_ARB_CONFIG_Reserved_OFF                         (16)
  #define CMI_0_REQ_L3_ARB_CONFIG_Reserved_WID                         (16)
  #define CMI_0_REQ_L3_ARB_CONFIG_Reserved_MSK                         (0xFFFF0000)
  #define CMI_0_REQ_L3_ARB_CONFIG_Reserved_MIN                         (0)
  #define CMI_0_REQ_L3_ARB_CONFIG_Reserved_MAX                         (65535) // 0x0000FFFF
  #define CMI_0_REQ_L3_ARB_CONFIG_Reserved_DEF                         (0x00000000)
  #define CMI_0_REQ_L3_ARB_CONFIG_Reserved_HSH                         (0x10211180)

#define CMI_0_PARITY_CONTROL_REG                                       (0x00011184)

  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_CHK_MASK_OFF                ( 0)
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_CHK_MASK_WID                ( 8)
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_CHK_MASK_MSK                (0x000000FF)
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_CHK_MASK_MIN                (0)
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_CHK_MASK_MAX                (255) // 0x000000FF
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_CHK_MASK_DEF                (0x00000000)
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_CHK_MASK_HSH                (0x08011184)

  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_GEN_MASK_OFF                ( 8)
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_GEN_MASK_WID                ( 8)
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_GEN_MASK_MSK                (0x0000FF00)
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_GEN_MASK_MIN                (0)
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_GEN_MASK_MAX                (255) // 0x000000FF
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_GEN_MASK_DEF                (0x000000FF)
  #define CMI_0_PARITY_CONTROL_ADDR_PARITY_GEN_MASK_HSH                (0x08111184)

  #define CMI_0_PARITY_CONTROL_DIS_PARITY_PCH_EVENT_OFF                (16)
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_PCH_EVENT_WID                ( 1)
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_PCH_EVENT_MSK                (0x00010000)
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_PCH_EVENT_MIN                (0)
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_PCH_EVENT_MAX                (1) // 0x00000001
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_PCH_EVENT_DEF                (0x00000000)
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_PCH_EVENT_HSH                (0x01211184)

  #define CMI_0_PARITY_CONTROL_DIS_PARITY_LOG_OFF                      (17)
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_LOG_WID                      ( 1)
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_LOG_MSK                      (0x00020000)
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_LOG_MIN                      (0)
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_LOG_MAX                      (1) // 0x00000001
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_LOG_DEF                      (0x00000000)
  #define CMI_0_PARITY_CONTROL_DIS_PARITY_LOG_HSH                      (0x01231184)

  #define CMI_0_PARITY_CONTROL_DIS_MULTIPLE_PCH_MSG_OFF                (18)
  #define CMI_0_PARITY_CONTROL_DIS_MULTIPLE_PCH_MSG_WID                ( 1)
  #define CMI_0_PARITY_CONTROL_DIS_MULTIPLE_PCH_MSG_MSK                (0x00040000)
  #define CMI_0_PARITY_CONTROL_DIS_MULTIPLE_PCH_MSG_MIN                (0)
  #define CMI_0_PARITY_CONTROL_DIS_MULTIPLE_PCH_MSG_MAX                (1) // 0x00000001
  #define CMI_0_PARITY_CONTROL_DIS_MULTIPLE_PCH_MSG_DEF                (0x00000000)
  #define CMI_0_PARITY_CONTROL_DIS_MULTIPLE_PCH_MSG_HSH                (0x01251184)

  #define CMI_0_PARITY_CONTROL_RESERVED_OFF                            (19)
  #define CMI_0_PARITY_CONTROL_RESERVED_WID                            (12)
  #define CMI_0_PARITY_CONTROL_RESERVED_MSK                            (0x7FF80000)
  #define CMI_0_PARITY_CONTROL_RESERVED_MIN                            (0)
  #define CMI_0_PARITY_CONTROL_RESERVED_MAX                            (4095) // 0x00000FFF
  #define CMI_0_PARITY_CONTROL_RESERVED_DEF                            (0x00000000)
  #define CMI_0_PARITY_CONTROL_RESERVED_HSH                            (0x0C271184)

  #define CMI_0_PARITY_CONTROL_PARITY_EN_OFF                           (31)
  #define CMI_0_PARITY_CONTROL_PARITY_EN_WID                           ( 1)
  #define CMI_0_PARITY_CONTROL_PARITY_EN_MSK                           (0x80000000)
  #define CMI_0_PARITY_CONTROL_PARITY_EN_MIN                           (0)
  #define CMI_0_PARITY_CONTROL_PARITY_EN_MAX                           (1) // 0x00000001
  #define CMI_0_PARITY_CONTROL_PARITY_EN_DEF                           (0x00000000)
  #define CMI_0_PARITY_CONTROL_PARITY_EN_HSH                           (0x013F1184)

#define CMI_0_PARITY_ERR_LOG_REG                                       (0x00011188)

  #define CMI_0_PARITY_ERR_LOG_Reserved_0_OFF                          ( 0)
  #define CMI_0_PARITY_ERR_LOG_Reserved_0_WID                          ( 6)
  #define CMI_0_PARITY_ERR_LOG_Reserved_0_MSK                          (0x0000003F)
  #define CMI_0_PARITY_ERR_LOG_Reserved_0_MIN                          (0)
  #define CMI_0_PARITY_ERR_LOG_Reserved_0_MAX                          (63) // 0x0000003F
  #define CMI_0_PARITY_ERR_LOG_Reserved_0_DEF                          (0x00000000)
  #define CMI_0_PARITY_ERR_LOG_Reserved_0_HSH                          (0x46011188)

  #define CMI_0_PARITY_ERR_LOG_ERR_ADDRESS_OFF                         ( 6)
  #define CMI_0_PARITY_ERR_LOG_ERR_ADDRESS_WID                         (40)
  #define CMI_0_PARITY_ERR_LOG_ERR_ADDRESS_MSK                         (0x00003FFFFFFFFFC0ULL)
  #define CMI_0_PARITY_ERR_LOG_ERR_ADDRESS_MIN                         (0)
  #define CMI_0_PARITY_ERR_LOG_ERR_ADDRESS_MAX                         (1099511627775ULL) // 0xFFFFFFFFFF
  #define CMI_0_PARITY_ERR_LOG_ERR_ADDRESS_DEF                         (0x00000000)
  #define CMI_0_PARITY_ERR_LOG_ERR_ADDRESS_HSH                         (0x680D1188)

  #define CMI_0_PARITY_ERR_LOG_Reserved_1_OFF                          (46)
  #define CMI_0_PARITY_ERR_LOG_Reserved_1_WID                          ( 2)
  #define CMI_0_PARITY_ERR_LOG_Reserved_1_MSK                          (0x0000C00000000000ULL)
  #define CMI_0_PARITY_ERR_LOG_Reserved_1_MIN                          (0)
  #define CMI_0_PARITY_ERR_LOG_Reserved_1_MAX                          (3) // 0x00000003
  #define CMI_0_PARITY_ERR_LOG_Reserved_1_DEF                          (0x00000000)
  #define CMI_0_PARITY_ERR_LOG_Reserved_1_HSH                          (0x425D1188)

  #define CMI_0_PARITY_ERR_LOG_ERR_SRC_OFF                             (48)
  #define CMI_0_PARITY_ERR_LOG_ERR_SRC_WID                             ( 3)
  #define CMI_0_PARITY_ERR_LOG_ERR_SRC_MSK                             (0x0007000000000000ULL)
  #define CMI_0_PARITY_ERR_LOG_ERR_SRC_MIN                             (0)
  #define CMI_0_PARITY_ERR_LOG_ERR_SRC_MAX                             (7) // 0x00000007
  #define CMI_0_PARITY_ERR_LOG_ERR_SRC_DEF                             (0x00000000)
  #define CMI_0_PARITY_ERR_LOG_ERR_SRC_HSH                             (0x43611188)

  #define CMI_0_PARITY_ERR_LOG_Reserved_2_OFF                          (51)
  #define CMI_0_PARITY_ERR_LOG_Reserved_2_WID                          (12)
  #define CMI_0_PARITY_ERR_LOG_Reserved_2_MSK                          (0x7FF8000000000000ULL)
  #define CMI_0_PARITY_ERR_LOG_Reserved_2_MIN                          (0)
  #define CMI_0_PARITY_ERR_LOG_Reserved_2_MAX                          (4095) // 0x00000FFF
  #define CMI_0_PARITY_ERR_LOG_Reserved_2_DEF                          (0x00000000)
  #define CMI_0_PARITY_ERR_LOG_Reserved_2_HSH                          (0x4C671188)

  #define CMI_0_PARITY_ERR_LOG_ERR_STS_OFF                             (63)
  #define CMI_0_PARITY_ERR_LOG_ERR_STS_WID                             ( 1)
  #define CMI_0_PARITY_ERR_LOG_ERR_STS_MSK                             (0x8000000000000000ULL)
  #define CMI_0_PARITY_ERR_LOG_ERR_STS_MIN                             (0)
  #define CMI_0_PARITY_ERR_LOG_ERR_STS_MAX                             (1) // 0x00000001
  #define CMI_0_PARITY_ERR_LOG_ERR_STS_DEF                             (0x00000000)
  #define CMI_0_PARITY_ERR_LOG_ERR_STS_HSH                             (0x417F1188)

#define CMI_0_PARITY_ERR_INJ_REG                                       (0x00011190)

  #define CMI_0_PARITY_ERR_INJ_Reserved_0_OFF                          ( 0)
  #define CMI_0_PARITY_ERR_INJ_Reserved_0_WID                          ( 8)
  #define CMI_0_PARITY_ERR_INJ_Reserved_0_MSK                          (0x000000FF)
  #define CMI_0_PARITY_ERR_INJ_Reserved_0_MIN                          (0)
  #define CMI_0_PARITY_ERR_INJ_Reserved_0_MAX                          (255) // 0x000000FF
  #define CMI_0_PARITY_ERR_INJ_Reserved_0_DEF                          (0x00000000)
  #define CMI_0_PARITY_ERR_INJ_Reserved_0_HSH                          (0x08011190)

  #define CMI_0_PARITY_ERR_INJ_ADDR_ERR_EN_OFF                         ( 8)
  #define CMI_0_PARITY_ERR_INJ_ADDR_ERR_EN_WID                         ( 1)
  #define CMI_0_PARITY_ERR_INJ_ADDR_ERR_EN_MSK                         (0x00000100)
  #define CMI_0_PARITY_ERR_INJ_ADDR_ERR_EN_MIN                         (0)
  #define CMI_0_PARITY_ERR_INJ_ADDR_ERR_EN_MAX                         (1) // 0x00000001
  #define CMI_0_PARITY_ERR_INJ_ADDR_ERR_EN_DEF                         (0x00000000)
  #define CMI_0_PARITY_ERR_INJ_ADDR_ERR_EN_HSH                         (0x01111190)

  #define CMI_0_PARITY_ERR_INJ_Reserved_1_OFF                          ( 9)
  #define CMI_0_PARITY_ERR_INJ_Reserved_1_WID                          ( 1)
  #define CMI_0_PARITY_ERR_INJ_Reserved_1_MSK                          (0x00000200)
  #define CMI_0_PARITY_ERR_INJ_Reserved_1_MIN                          (0)
  #define CMI_0_PARITY_ERR_INJ_Reserved_1_MAX                          (1) // 0x00000001
  #define CMI_0_PARITY_ERR_INJ_Reserved_1_DEF                          (0x00000000)
  #define CMI_0_PARITY_ERR_INJ_Reserved_1_HSH                          (0x01131190)

  #define CMI_0_PARITY_ERR_INJ_Reserved_2_OFF                          (10)
  #define CMI_0_PARITY_ERR_INJ_Reserved_2_WID                          ( 6)
  #define CMI_0_PARITY_ERR_INJ_Reserved_2_MSK                          (0x0000FC00)
  #define CMI_0_PARITY_ERR_INJ_Reserved_2_MIN                          (0)
  #define CMI_0_PARITY_ERR_INJ_Reserved_2_MAX                          (63) // 0x0000003F
  #define CMI_0_PARITY_ERR_INJ_Reserved_2_DEF                          (0x00000000)
  #define CMI_0_PARITY_ERR_INJ_Reserved_2_HSH                          (0x06151190)

  #define CMI_0_PARITY_ERR_INJ_ERR_SRC_MASK_OFF                        (16)
  #define CMI_0_PARITY_ERR_INJ_ERR_SRC_MASK_WID                        ( 8)
  #define CMI_0_PARITY_ERR_INJ_ERR_SRC_MASK_MSK                        (0x00FF0000)
  #define CMI_0_PARITY_ERR_INJ_ERR_SRC_MASK_MIN                        (0)
  #define CMI_0_PARITY_ERR_INJ_ERR_SRC_MASK_MAX                        (255) // 0x000000FF
  #define CMI_0_PARITY_ERR_INJ_ERR_SRC_MASK_DEF                        (0x00000000)
  #define CMI_0_PARITY_ERR_INJ_ERR_SRC_MASK_HSH                        (0x08211190)

  #define CMI_0_PARITY_ERR_INJ_Reserved_3_OFF                          (24)
  #define CMI_0_PARITY_ERR_INJ_Reserved_3_WID                          ( 8)
  #define CMI_0_PARITY_ERR_INJ_Reserved_3_MSK                          (0xFF000000)
  #define CMI_0_PARITY_ERR_INJ_Reserved_3_MIN                          (0)
  #define CMI_0_PARITY_ERR_INJ_Reserved_3_MAX                          (255) // 0x000000FF
  #define CMI_0_PARITY_ERR_INJ_Reserved_3_DEF                          (0x00000000)
  #define CMI_0_PARITY_ERR_INJ_Reserved_3_HSH                          (0x08311190)

#define CMI_0_REQUEST_PORT_0_RESERVED_0_REG                            (0x00011320)

  #define CMI_0_REQUEST_PORT_0_RESERVED_0_RESERVED_OFF                 ( 0)
  #define CMI_0_REQUEST_PORT_0_RESERVED_0_RESERVED_WID                 (32)
  #define CMI_0_REQUEST_PORT_0_RESERVED_0_RESERVED_MSK                 (0xFFFFFFFF)
  #define CMI_0_REQUEST_PORT_0_RESERVED_0_RESERVED_MIN                 (0)
  #define CMI_0_REQUEST_PORT_0_RESERVED_0_RESERVED_MAX                 (4294967295) // 0xFFFFFFFF
  #define CMI_0_REQUEST_PORT_0_RESERVED_0_RESERVED_DEF                 (0x00000000)
  #define CMI_0_REQUEST_PORT_0_RESERVED_0_RESERVED_HSH                 (0x20011320)

#define CMI_0_REQUEST_PORT_0_RESERVED_1_REG                            (0x00011324)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_REQUEST_PORT_0_RESERVED_2_REG                            (0x00011328)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG                  (0x0001132C)

  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_VAL_OFF  ( 0)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_VAL_WID  (16)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_VAL_MSK  (0x0000FFFF)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_VAL_MIN  (0)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_VAL_MAX  (65535) // 0x0000FFFF
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_VAL_DEF  (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_VAL_HSH  (0x1001132C)

  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_CLR_OFF            (16)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_CLR_WID            ( 1)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_CLR_MSK            (0x00010000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_CLR_MIN            (0)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_CLR_MAX            (1) // 0x00000001
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_CLR_DEF            (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_CLR_HSH            (0x0121132C)

  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_ENABLE_OFF         (17)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_ENABLE_WID         ( 1)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_ENABLE_MSK         (0x00020000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_ENABLE_MIN         (0)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_ENABLE_MAX         (1) // 0x00000001
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_ENABLE_DEF         (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_ENABLE_HSH         (0x0123132C)

  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_FUNC_OV_OFF        (18)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_FUNC_OV_WID        ( 1)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_FUNC_OV_MSK        (0x00040000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_FUNC_OV_MIN        (0)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_FUNC_OV_MAX        (1) // 0x00000001
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_FUNC_OV_DEF        (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_FUNC_OV_HSH        (0x0125132C)

  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_EVENT_COUNT_OFF    (19)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_EVENT_COUNT_WID    (12)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_EVENT_COUNT_MSK    (0x7FF80000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_EVENT_COUNT_MIN    (0)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_EVENT_COUNT_MAX    (4095) // 0x00000FFF
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_EVENT_COUNT_DEF    (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_EVENT_COUNT_HSH    (0x0C27132C)

  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_TYPE_OFF (31)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_TYPE_WID ( 1)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_TYPE_MSK (0x80000000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_TYPE_MIN (0)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_TYPE_MAX (1) // 0x00000001
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_TYPE_DEF (0x00000000)
  #define CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_SIGNATURE_TYPE_HSH (0x013F132C)

#define CMI_0_REQUEST_PORT_0_RD_CPL_OUT_DFX_INSTR_DBG_REG              (0x00011330)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_0_RSP_OUT_DFX_INSTR_DBG_REG                 (0x00011334)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_1_RESERVED_0_REG                            (0x00011338)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_REQUEST_PORT_1_RESERVED_1_REG                            (0x0001133C)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_REQUEST_PORT_1_RESERVED_2_REG                            (0x00011340)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_REQUEST_PORT_1_REQ_IN_DFX_INSTR_DBG_REG                  (0x00011344)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_1_RD_CPL_OUT_DFX_INSTR_DBG_REG              (0x00011348)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_1_RSP_OUT_DFX_INSTR_DBG_REG                 (0x0001134C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_2_RESERVED_0_REG                            (0x00011350)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_REQUEST_PORT_2_RESERVED_1_REG                            (0x00011354)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_REQUEST_PORT_2_RESERVED_2_REG                            (0x00011358)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_REQUEST_PORT_2_REQ_IN_DFX_INSTR_DBG_REG                  (0x0001135C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_2_RD_CPL_OUT_DFX_INSTR_DBG_REG              (0x00011360)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_2_RSP_OUT_DFX_INSTR_DBG_REG                 (0x00011364)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_RESPOND_PORT_0_RESERVED_0_REG                            (0x00011368)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_RESPOND_PORT_0_RESERVED_1_REG                            (0x0001136C)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_RESPOND_PORT_0_RESERVED_2_REG                            (0x00011370)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_RESPOND_PORT_0_REQ_OUT_DFX_INSTR_DBG_REG                 (0x00011374)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_RESPOND_PORT_0_RD_CPL_IN_DFX_INSTR_DBG_REG               (0x00011378)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_RESPOND_PORT_0_RSP_IN_DFX_INSTR_DBG_REG                  (0x0001137C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_RESPOND_PORT_1_RESERVED_0_REG                            (0x00011380)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_RESPOND_PORT_1_RESERVED_1_REG                            (0x00011384)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_RESPOND_PORT_1_RESERVED_2_REG                            (0x00011388)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_0_RESPOND_PORT_1_REQ_OUT_DFX_INSTR_DBG_REG                 (0x0001138C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_RESPOND_PORT_1_RD_CPL_IN_DFX_INSTR_DBG_REG               (0x00011390)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_RESPOND_PORT_1_RSP_IN_DFX_INSTR_DBG_REG                  (0x00011394)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_0_REQ_DATA_IN_DFX_INSTR_DBG_REG             (0x00011420)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_0_RD_CPL_DATA_OUT_DFX_INSTR_DBG_REG         (0x00011424)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_1_REQ_DATA_IN_DFX_INSTR_DBG_REG             (0x00011428)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_1_RD_CPL_DATA_OUT_DFX_INSTR_DBG_REG         (0x0001142C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_2_REQ_DATA_IN_DFX_INSTR_DBG_REG             (0x00011430)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_REQUEST_PORT_2_RD_CPL_DATA_OUT_DFX_INSTR_DBG_REG         (0x00011434)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_RESPOND_PORT_0_REQ_DATA_OUT_DFX_INSTR_DBG_REG            (0x00011438)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_RESPOND_PORT_0_RD_CPL_DATA_IN_DFX_INSTR_DBG_REG          (0x0001143C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_RESPOND_PORT_1_REQ_DATA_OUT_DFX_INSTR_DBG_REG            (0x00011440)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_RESPOND_PORT_1_RD_CPL_DATA_IN_DFX_INSTR_DBG_REG          (0x00011444)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_0_DFX_OBSERVE_REG                                          (0x00011448)

  #define CMI_0_DFX_OBSERVE_INSTR_DBG_MODE_OFF                         ( 0)
  #define CMI_0_DFX_OBSERVE_INSTR_DBG_MODE_WID                         ( 1)
  #define CMI_0_DFX_OBSERVE_INSTR_DBG_MODE_MSK                         (0x00000001)
  #define CMI_0_DFX_OBSERVE_INSTR_DBG_MODE_MIN                         (0)
  #define CMI_0_DFX_OBSERVE_INSTR_DBG_MODE_MAX                         (1) // 0x00000001
  #define CMI_0_DFX_OBSERVE_INSTR_DBG_MODE_DEF                         (0x00000000)
  #define CMI_0_DFX_OBSERVE_INSTR_DBG_MODE_HSH                         (0x01011448)

  #define CMI_0_DFX_OBSERVE_DBG_VALID_COUNT_OFF                        ( 1)
  #define CMI_0_DFX_OBSERVE_DBG_VALID_COUNT_WID                        (18)
  #define CMI_0_DFX_OBSERVE_DBG_VALID_COUNT_MSK                        (0x0007FFFE)
  #define CMI_0_DFX_OBSERVE_DBG_VALID_COUNT_MIN                        (0)
  #define CMI_0_DFX_OBSERVE_DBG_VALID_COUNT_MAX                        (262143) // 0x0003FFFF
  #define CMI_0_DFX_OBSERVE_DBG_VALID_COUNT_DEF                        (0x00000000)
  #define CMI_0_DFX_OBSERVE_DBG_VALID_COUNT_HSH                        (0x12031448)

  #define CMI_0_DFX_OBSERVE_DBG_CYCLE_COUNT_OFF                        (19)
  #define CMI_0_DFX_OBSERVE_DBG_CYCLE_COUNT_WID                        (13)
  #define CMI_0_DFX_OBSERVE_DBG_CYCLE_COUNT_MSK                        (0xFFF80000)
  #define CMI_0_DFX_OBSERVE_DBG_CYCLE_COUNT_MIN                        (0)
  #define CMI_0_DFX_OBSERVE_DBG_CYCLE_COUNT_MAX                        (8191) // 0x00001FFF
  #define CMI_0_DFX_OBSERVE_DBG_CYCLE_COUNT_DEF                        (0x00000000)
  #define CMI_0_DFX_OBSERVE_DBG_CYCLE_COUNT_HSH                        (0x0D271448)

#define CMF_0_GLOBAL_STATUS_REG                                        (0x00011450)

  #define CMF_0_GLOBAL_STATUS_MUX_DEMUX_STS_OFF                        ( 0)
  #define CMF_0_GLOBAL_STATUS_MUX_DEMUX_STS_WID                        ( 1)
  #define CMF_0_GLOBAL_STATUS_MUX_DEMUX_STS_MSK                        (0x00000001)
  #define CMF_0_GLOBAL_STATUS_MUX_DEMUX_STS_MIN                        (0)
  #define CMF_0_GLOBAL_STATUS_MUX_DEMUX_STS_MAX                        (1) // 0x00000001
  #define CMF_0_GLOBAL_STATUS_MUX_DEMUX_STS_DEF                        (0x00000001)
  #define CMF_0_GLOBAL_STATUS_MUX_DEMUX_STS_HSH                        (0x01011450)

  #define CMF_0_GLOBAL_STATUS_RESERVED_OFF                             ( 1)
  #define CMF_0_GLOBAL_STATUS_RESERVED_WID                             (31)
  #define CMF_0_GLOBAL_STATUS_RESERVED_MSK                             (0xFFFFFFFE)
  #define CMF_0_GLOBAL_STATUS_RESERVED_MIN                             (0)
  #define CMF_0_GLOBAL_STATUS_RESERVED_MAX                             (2147483647) // 0x7FFFFFFF
  #define CMF_0_GLOBAL_STATUS_RESERVED_DEF                             (0x00000000)
  #define CMF_0_GLOBAL_STATUS_RESERVED_HSH                             (0x1F031450)

#define CMF_0_GLOBAL_CFG_REG                                           (0x00011454)

  #define CMF_0_GLOBAL_CFG_CREDITS_CONFIG_DONE_OFF                     ( 0)
  #define CMF_0_GLOBAL_CFG_CREDITS_CONFIG_DONE_WID                     ( 1)
  #define CMF_0_GLOBAL_CFG_CREDITS_CONFIG_DONE_MSK                     (0x00000001)
  #define CMF_0_GLOBAL_CFG_CREDITS_CONFIG_DONE_MIN                     (0)
  #define CMF_0_GLOBAL_CFG_CREDITS_CONFIG_DONE_MAX                     (1) // 0x00000001
  #define CMF_0_GLOBAL_CFG_CREDITS_CONFIG_DONE_DEF                     (0x00000000)
  #define CMF_0_GLOBAL_CFG_CREDITS_CONFIG_DONE_HSH                     (0x01011454)

  #define CMF_0_GLOBAL_CFG_RESERVED_0_OFF                              ( 1)
  #define CMF_0_GLOBAL_CFG_RESERVED_0_WID                              (31)
  #define CMF_0_GLOBAL_CFG_RESERVED_0_MSK                              (0xFFFFFFFE)
  #define CMF_0_GLOBAL_CFG_RESERVED_0_MIN                              (0)
  #define CMF_0_GLOBAL_CFG_RESERVED_0_MAX                              (2147483647) // 0x7FFFFFFF
  #define CMF_0_GLOBAL_CFG_RESERVED_0_DEF                              (0x00000000)
  #define CMF_0_GLOBAL_CFG_RESERVED_0_HSH                              (0x1F031454)

#define CMF_1_LT_SA_W_PG_ACCESS_POLICY_CP_REG                          (0x00011800)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_REG

#define CMF_1_LT_SA_W_PG_ACCESS_POLICY_WAC_REG                         (0x00011808)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_REG

#define CMF_1_LT_SA_W_PG_ACCESS_POLICY_RAC_REG                         (0x00011810)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_REG

#define CMF_1_P_U_CODE_PG_ACCESS_POLICY_CP_REG                         (0x00011818)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_REG

#define CMF_1_P_U_CODE_PG_ACCESS_POLICY_WAC_REG                        (0x00011820)
//Duplicate of CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_REG

#define CMF_1_P_U_CODE_PG_ACCESS_POLICY_RAC_REG                        (0x00011828)
//Duplicate of CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_REG

#define CMF_1_OS_PG_ACCESS_POLICY_CP_REG                               (0x00011830)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_REG

#define CMF_1_OS_PG_ACCESS_POLICY_WAC_REG                              (0x00011838)
//Duplicate of CMF_0_OS_PG_ACCESS_POLICY_WAC_REG

#define CMF_1_OS_PG_ACCESS_POLICY_RAC_REG                              (0x00011840)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_REG

#define CMI_1_PRIO_THRESHOLD_REG                                       (0x00011848)
//Duplicate of CMI_0_PRIO_THRESHOLD_REG

#define CMI_1_PRIO_LIM_REG                                             (0x0001184C)
//Duplicate of CMI_0_PRIO_LIM_REG

#define CMF_1_TOPOLOGY_GLOBAL_CFG_0_REG                                (0x00011850)
//Duplicate of CMF_0_TOPOLOGY_GLOBAL_CFG_0_REG

#define CMF_1_TOPOLOGY_GLOBAL_CFG_1_REG                                (0x00011854)
//Duplicate of CMF_0_TOPOLOGY_GLOBAL_CFG_1_REG

#define CMF_1_TOPOLOGY_GLOBAL_CFG_2_REG                                (0x00011858)
//Duplicate of CMF_0_TOPOLOGY_GLOBAL_CFG_2_REG

#define CMF_1_TOPOLOGY_GLOBAL_CFG_3_REG                                (0x0001185C)
//Duplicate of CMF_0_TOPOLOGY_GLOBAL_CFG_3_REG

#define CMF_1_TOPOLOGY_GLOBAL_CFG_4_REG                                (0x00011860)
//Duplicate of CMF_0_TOPOLOGY_GLOBAL_CFG_4_REG

#define CMF_1_GLOBAL_CFG_1_REG                                         (0x00011868)
//Duplicate of CMF_0_GLOBAL_CFG_1_REG

#define CMI_1_IOSF_SB_EP_CTRL_REG                                      (0x0001186C)
//Duplicate of CMI_0_IOSF_SB_EP_CTRL_REG

#define CMI_1_CLK_GATE_EN_0_REG                                        (0x00011870)
//Duplicate of CMI_0_CLK_GATE_EN_0_REG

#define CMI_1_CLK_GATE_EN_1_REG                                        (0x00011874)
//Duplicate of CMI_0_CLK_GATE_EN_0_REG

#define CMF_1_RSVD0_REG                                                (0x00011878)
//Duplicate of CMF_0_RSVD0_REG

#define CMF_1_RSVD1_REG                                                (0x0001187C)
//Duplicate of CMF_0_RSVD1_REG

#define CMI_1_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG                (0x00011880)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_1_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_1_REG                (0x00011884)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_1_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG                (0x00011888)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_1_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_3_REG                (0x0001188C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_1_REQUEST_PORT_0_REQ_CONFIG_1_REG                          (0x00011890)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REG

#define CMI_1_REQUEST_PORT_0_STALL_REG                                 (0x00011894)
//Duplicate of CMI_0_REQUEST_PORT_0_STALL_REG

#define CMI_1_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_0_REG                (0x00011898)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_1_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_1_REG                (0x0001189C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_1_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_2_REG                (0x000118A0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_1_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_3_REG                (0x000118A4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_1_REQUEST_PORT_1_REQ_CONFIG_1_REG                          (0x000118A8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REG

#define CMI_1_REQUEST_PORT_1_STALL_REG                                 (0x000118AC)
//Duplicate of CMI_0_REQUEST_PORT_0_STALL_REG

#define CMI_1_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_0_REG                (0x000118B0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_1_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_1_REG                (0x000118B4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_1_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_2_REG                (0x000118B8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_1_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_3_REG                (0x000118BC)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_1_REQUEST_PORT_2_REQ_CONFIG_1_REG                          (0x000118C0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REG

#define CMI_1_REQUEST_PORT_2_STALL_REG                                 (0x000118C4)
//Duplicate of CMI_0_REQUEST_PORT_0_STALL_REG

#define CMI_1_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG                (0x000118C8)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_1_REG                (0x000118CC)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_2_REG                (0x000118D0)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_3_REG                (0x000118D4)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_CPL_CONFIG_1_REG                          (0x000118D8)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REG

#define CMI_1_RESPOND_PORT_0_STALL_REG                                 (0x000118DC)
//Duplicate of CMI_0_RESPOND_PORT_0_STALL_REG

#define CMI_1_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_0_REG                (0x000118E0)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_1_REG                (0x000118E4)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_2_REG                (0x000118E8)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_3_REG                (0x000118EC)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_CPL_CONFIG_1_REG                          (0x000118F0)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REG

#define CMI_1_RESPOND_PORT_1_STALL_REG                                 (0x000118F4)
//Duplicate of CMI_0_RESPOND_PORT_0_STALL_REG

#define CMI_1_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG                  (0x00011900)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_RD_COUNTER_CHANNEL_1_REG                  (0x00011908)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_RD_COUNTER_CHANNEL_2_REG                  (0x00011910)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_RD_COUNTER_CHANNEL_3_REG                  (0x00011918)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG                  (0x00011920)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_WR_COUNTER_CHANNEL_1_REG                  (0x00011928)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_WR_COUNTER_CHANNEL_2_REG                  (0x00011930)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_0_WR_COUNTER_CHANNEL_3_REG                  (0x00011938)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_RD_COUNTER_CHANNEL_0_REG                  (0x00011940)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_RD_COUNTER_CHANNEL_1_REG                  (0x00011948)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_RD_COUNTER_CHANNEL_2_REG                  (0x00011950)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_RD_COUNTER_CHANNEL_3_REG                  (0x00011958)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_WR_COUNTER_CHANNEL_0_REG                  (0x00011960)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_WR_COUNTER_CHANNEL_1_REG                  (0x00011968)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_WR_COUNTER_CHANNEL_2_REG                  (0x00011970)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_1_RESPOND_PORT_1_WR_COUNTER_CHANNEL_3_REG                  (0x00011978)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_1_REQ_L3_ARB_CONFIG_REG                                    (0x00011980)
//Duplicate of CMI_0_REQ_L3_ARB_CONFIG_REG

#define CMI_1_PARITY_CONTROL_REG                                       (0x00011984)
//Duplicate of CMI_0_PARITY_CONTROL_REG

#define CMI_1_PARITY_ERR_LOG_REG                                       (0x00011988)
//Duplicate of CMI_0_PARITY_ERR_LOG_REG

#define CMI_1_PARITY_ERR_INJ_REG                                       (0x00011990)
//Duplicate of CMI_0_PARITY_ERR_INJ_REG

#define CMI_1_REQUEST_PORT_0_RESERVED_0_REG                            (0x00011B20)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_REQUEST_PORT_0_RESERVED_1_REG                            (0x00011B24)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_REQUEST_PORT_0_RESERVED_2_REG                            (0x00011B28)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG                  (0x00011B2C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_0_RD_CPL_OUT_DFX_INSTR_DBG_REG              (0x00011B30)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_0_RSP_OUT_DFX_INSTR_DBG_REG                 (0x00011B34)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_1_RESERVED_0_REG                            (0x00011B38)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_REQUEST_PORT_1_RESERVED_1_REG                            (0x00011B3C)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_REQUEST_PORT_1_RESERVED_2_REG                            (0x00011B40)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_REQUEST_PORT_1_REQ_IN_DFX_INSTR_DBG_REG                  (0x00011B44)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_1_RD_CPL_OUT_DFX_INSTR_DBG_REG              (0x00011B48)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_1_RSP_OUT_DFX_INSTR_DBG_REG                 (0x00011B4C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_2_RESERVED_0_REG                            (0x00011B50)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_REQUEST_PORT_2_RESERVED_1_REG                            (0x00011B54)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_REQUEST_PORT_2_RESERVED_2_REG                            (0x00011B58)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_REQUEST_PORT_2_REQ_IN_DFX_INSTR_DBG_REG                  (0x00011B5C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_2_RD_CPL_OUT_DFX_INSTR_DBG_REG              (0x00011B60)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_2_RSP_OUT_DFX_INSTR_DBG_REG                 (0x00011B64)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_RESPOND_PORT_0_RESERVED_0_REG                            (0x00011B68)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_RESPOND_PORT_0_RESERVED_1_REG                            (0x00011B6C)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_RESPOND_PORT_0_RESERVED_2_REG                            (0x00011B70)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_RESPOND_PORT_0_REQ_OUT_DFX_INSTR_DBG_REG                 (0x00011B74)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_RESPOND_PORT_0_RD_CPL_IN_DFX_INSTR_DBG_REG               (0x00011B78)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_RESPOND_PORT_0_RSP_IN_DFX_INSTR_DBG_REG                  (0x00011B7C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_RESPOND_PORT_1_RESERVED_0_REG                            (0x00011B80)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_RESPOND_PORT_1_RESERVED_1_REG                            (0x00011B84)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_RESPOND_PORT_1_RESERVED_2_REG                            (0x00011B88)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_1_RESPOND_PORT_1_REQ_OUT_DFX_INSTR_DBG_REG                 (0x00011B8C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_RESPOND_PORT_1_RD_CPL_IN_DFX_INSTR_DBG_REG               (0x00011B90)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_RESPOND_PORT_1_RSP_IN_DFX_INSTR_DBG_REG                  (0x00011B94)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_0_REQ_DATA_IN_DFX_INSTR_DBG_REG             (0x00011C20)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_0_RD_CPL_DATA_OUT_DFX_INSTR_DBG_REG         (0x00011C24)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_1_REQ_DATA_IN_DFX_INSTR_DBG_REG             (0x00011C28)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_1_RD_CPL_DATA_OUT_DFX_INSTR_DBG_REG         (0x00011C2C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_2_REQ_DATA_IN_DFX_INSTR_DBG_REG             (0x00011C30)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_REQUEST_PORT_2_RD_CPL_DATA_OUT_DFX_INSTR_DBG_REG         (0x00011C34)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_RESPOND_PORT_0_REQ_DATA_OUT_DFX_INSTR_DBG_REG            (0x00011C38)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_RESPOND_PORT_0_RD_CPL_DATA_IN_DFX_INSTR_DBG_REG          (0x00011C3C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_RESPOND_PORT_1_REQ_DATA_OUT_DFX_INSTR_DBG_REG            (0x00011C40)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_RESPOND_PORT_1_RD_CPL_DATA_IN_DFX_INSTR_DBG_REG          (0x00011C44)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_1_DFX_OBSERVE_REG                                          (0x00011C48)
//Duplicate of CMI_0_DFX_OBSERVE_REG

#define CMF_1_GLOBAL_STATUS_REG                                        (0x00011C50)
//Duplicate of CMF_0_GLOBAL_STATUS_REG

#define CMF_1_GLOBAL_CFG_REG                                           (0x00011C54)
//Duplicate of CMF_0_GLOBAL_CFG_REG
#pragma pack(pop)
#endif
